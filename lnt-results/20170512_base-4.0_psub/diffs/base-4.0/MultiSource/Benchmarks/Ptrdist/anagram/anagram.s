	.text
	.file	"anagram.bc"
	.globl	Fatal
	.p2align	4, 0x90
	.type	Fatal,@function
Fatal:                                  # @Fatal
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	%esi, %ecx
	movq	%rdi, %rdx
	movq	stderr(%rip), %rdi
	xorl	%eax, %eax
	movq	%rdx, %rsi
	movl	%ecx, %edx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	Fatal, .Lfunc_end0-Fatal
	.cfi_endproc

	.globl	ReadDict
	.p2align	4, 0x90
	.type	ReadDict,@function
ReadDict:                               # @ReadDict
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 208
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	leaq	8(%rsp), %rdx
	movl	$1, %edi
	movq	%r14, %rsi
	callq	__xstat
	testl	%eax, %eax
	jne	.LBB1_12
# BB#1:
	movq	56(%rsp), %rbp
	addq	$52000, %rbp            # imm = 0xCB20
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, pchDictionary(%rip)
	testq	%rbx, %rbx
	je	.LBB1_13
# BB#2:
	movl	$.L.str.2, %esi
	movq	%r14, %rdi
	callq	fopen
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB1_14
# BB#3:                                 # %.preheader
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%r12, %rdi
	callq	feof
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jne	.LBB1_10
# BB#4:                                 # %.lr.ph.preheader
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_6 Depth 2
	movq	%rbx, %r13
	leaq	3(%r13), %rbx
	xorl	%r14d, %r14d
	jmp	.LBB1_6
	.p2align	4, 0x90
.LBB1_8:                                #   in Loop: Header=BB1_6 Depth=2
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movslq	%ebp, %rcx
	movzwl	(%rax,%rcx,2), %eax
	shrl	$10, %eax
	andl	$1, %eax
	addl	%eax, %r14d
	movb	%bpl, -1(%rbx)
	incq	%rbx
.LBB1_6:                                #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r12, %rdi
	callq	fgetc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	je	.LBB1_9
# BB#7:                                 #   in Loop: Header=BB1_6 Depth=2
	cmpl	$10, %ebp
	jne	.LBB1_8
.LBB1_9:                                #   in Loop: Header=BB1_5 Depth=1
	movb	$0, -1(%rbx)
	movl	%ebx, %eax
	subl	%r13d, %eax
	movb	%al, (%r13)
	movb	%r14b, 1(%r13)
	incl	%r15d
	movq	%r12, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB1_5
.LBB1_10:                               # %._crit_edge
	movq	%r12, %rdi
	callq	fclose
	movb	$0, (%rbx)
	movq	stderr(%rip), %rdi
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	callq	fprintf
	cmpl	$26000, %r15d           # imm = 0x6590
	jae	.LBB1_15
# BB#11:
	incq	%rbx
	movq	stderr(%rip), %rdi
	movq	(%rsp), %rdx            # 8-byte Reload
	subq	%rbx, %rdx
	addq	pchDictionary(%rip), %rdx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	callq	fprintf
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_12:
	movl	$.L.str, %edi
	xorl	%esi, %esi
	callq	Fatal
.LBB1_13:
	movl	$.L.str.1, %edi
	xorl	%esi, %esi
	callq	Fatal
.LBB1_14:
	movl	$.L.str.3, %edi
	xorl	%esi, %esi
	callq	Fatal
.LBB1_15:
	movl	$.L.str.5, %edi
	xorl	%esi, %esi
	callq	Fatal
.Lfunc_end1:
	.size	ReadDict, .Lfunc_end1-ReadDict
	.cfi_endproc

	.globl	BuildMask
	.p2align	4, 0x90
	.type	BuildMask,@function
BuildMask:                              # @BuildMask
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	xorl	%r14d, %r14d
	movl	$alPhrase, %edi
	xorl	%esi, %esi
	movl	$416, %edx              # imm = 0x1A0
	callq	memset
	xorps	%xmm0, %xmm0
	movaps	%xmm0, aqMainMask(%rip)
	movaps	%xmm0, aqMainSign(%rip)
	jmp	.LBB2_1
	.p2align	4, 0x90
.LBB2_7:                                #   in Loop: Header=BB2_1 Depth=1
	movl	%ebp, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB2_9
# BB#8:                                 #   in Loop: Header=BB2_1 Depth=1
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movl	(%rax,%rbp,4), %ebp
.LBB2_9:                                # %tolower.exit
                                        #   in Loop: Header=BB2_1 Depth=1
	movslq	%ebp, %rax
	shlq	$4, %rax
	incl	alPhrase-1552(%rax)
	incl	%r14d
.LBB2_1:                                # %.sink.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_2 Depth 2
	movl	%r14d, cchPhraseLength(%rip)
	.p2align	4, 0x90
.LBB2_2:                                #   Parent Loop BB2_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB2_3
# BB#6:                                 #   in Loop: Header=BB2_2 Depth=2
	incq	%rbx
	callq	__ctype_b_loc
	movq	(%rax), %rax
	testb	$4, 1(%rax,%rbp,2)
	je	.LBB2_2
	jmp	.LBB2_7
.LBB2_3:                                # %.preheader.preheader
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB2_4:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_11 Depth 2
	movq	%rax, %rsi
	shlq	$4, %rsi
	movl	alPhrase(%rsi), %edi
	testq	%rdi, %rdi
	je	.LBB2_5
# BB#10:                                # %.lr.ph
                                        #   in Loop: Header=BB2_4 Depth=1
	movl	$0, auGlobalFrequency(,%rax,4)
	movl	$1, %ebp
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB2_11:                               #   Parent Loop BB2_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebp
	addq	%rbx, %rbx
	cmpq	%rbx, %rdi
	jae	.LBB2_11
# BB#12:                                # %._crit_edge
                                        #   in Loop: Header=BB2_4 Depth=1
	leal	(%rcx,%rbp), %edx
	cmpl	$65, %edx
	jb	.LBB2_14
# BB#13:                                #   in Loop: Header=BB2_4 Depth=1
	incl	%r8d
	xorl	%ecx, %ecx
	cmpl	$2, %r8d
	jae	.LBB2_17
.LBB2_14:                               #   in Loop: Header=BB2_4 Depth=1
	leal	-1(%rbx), %r9d
	shlq	%cl, %rbx
	movl	%r8d, %edx
	orq	%rbx, aqMainSign(,%rdx,8)
	shlq	%cl, %rdi
	orq	%rdi, aqMainMask(,%rdx,8)
	movl	%r9d, alPhrase+8(%rsi)
	movl	%ecx, alPhrase+4(%rsi)
	movl	%r8d, alPhrase+12(%rsi)
	addl	%ecx, %ebp
	movl	%ebp, %ecx
	jmp	.LBB2_15
	.p2align	4, 0x90
.LBB2_5:                                #   in Loop: Header=BB2_4 Depth=1
	movl	$-1, auGlobalFrequency(,%rax,4)
.LBB2_15:                               #   in Loop: Header=BB2_4 Depth=1
	incq	%rax
	cmpq	$26, %rax
	jl	.LBB2_4
# BB#16:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB2_17:
	movl	$.L.str.7, %edi
	xorl	%esi, %esi
	callq	Fatal
.Lfunc_end2:
	.size	BuildMask, .Lfunc_end2-BuildMask
	.cfi_endproc

	.globl	NewWord
	.p2align	4, 0x90
	.type	NewWord,@function
NewWord:                                # @NewWord
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 16
	movl	$32, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB3_2
# BB#1:
	popq	%rcx
	retq
.LBB3_2:
	movl	cpwCand(%rip), %esi
	movl	$.L.str.8, %edi
	callq	Fatal
.Lfunc_end3:
	.size	NewWord, .Lfunc_end3-NewWord
	.cfi_endproc

	.globl	wprint
	.p2align	4, 0x90
	.type	wprint,@function
wprint:                                 # @wprint
	.cfi_startproc
# BB#0:
	movq	%rdi, %rcx
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	wprint, .Lfunc_end4-wprint
	.cfi_endproc

	.globl	NextWord
	.p2align	4, 0x90
	.type	NextWord,@function
NextWord:                               # @NextWord
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	cpwCand(%rip), %ebx
	cmpq	$5000, %rbx             # imm = 0x1388
	jae	.LBB5_5
# BB#1:
	leal	1(%rbx), %ebp
	movl	%ebp, cpwCand(%rip)
	movq	apwCand(,%rbx,8), %rax
	testq	%rax, %rax
	jne	.LBB5_4
# BB#2:
	movl	$32, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB5_6
# BB#3:                                 # %NewWord.exit
	movq	%rax, apwCand(,%rbx,8)
.LBB5_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB5_5:
	movl	$.L.str.10, %edi
	xorl	%esi, %esi
	callq	Fatal
.LBB5_6:
	movl	$.L.str.8, %edi
	movl	%ebp, %esi
	callq	Fatal
.Lfunc_end5:
	.size	NextWord, .Lfunc_end5-NextWord
	.cfi_endproc

	.globl	BuildWord
	.p2align	4, 0x90
	.type	BuildWord,@function
BuildWord:                              # @BuildWord
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 80
.Lcfi31:
	.cfi_offset %rbx, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 10(%rsp)
	movdqa	%xmm0, (%rsp)
	xorl	%r15d, %r15d
	movq	%r14, %rbp
	jmp	.LBB6_1
.LBB6_6:                                #   in Loop: Header=BB6_1 Depth=1
	incl	%r15d
	.p2align	4, 0x90
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	movsbq	(%rbp), %rbx
	testq	%rbx, %rbx
	je	.LBB6_7
# BB#2:                                 #   in Loop: Header=BB6_1 Depth=1
	incq	%rbp
	callq	__ctype_b_loc
	movq	(%rax), %rax
	testb	$4, 1(%rax,%rbx,2)
	je	.LBB6_1
# BB#3:                                 #   in Loop: Header=BB6_1 Depth=1
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB6_5
# BB#4:                                 #   in Loop: Header=BB6_1 Depth=1
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movl	(%rax,%rbx,4), %ebx
.LBB6_5:                                # %tolower.exit
                                        #   in Loop: Header=BB6_1 Depth=1
	movslq	%ebx, %rax
	movzbl	-97(%rsp,%rax), %ecx
	incb	%cl
	movb	%cl, -97(%rsp,%rax)
	leaq	-97(%rax), %rax
	movzbl	%cl, %ecx
	shlq	$4, %rax
	cmpl	alPhrase(%rax), %ecx
	jbe	.LBB6_6
	jmp	.LBB6_13
.LBB6_7:                                # %.preheader
	movd	(%rsp), %xmm1           # xmm1 = mem[0],zero,zero,zero
	pxor	%xmm0, %xmm0
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	paddd	auGlobalFrequency(%rip), %xmm1
	movdqa	%xmm1, auGlobalFrequency(%rip)
	movd	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	paddd	auGlobalFrequency+16(%rip), %xmm1
	movdqa	%xmm1, auGlobalFrequency+16(%rip)
	movd	8(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	paddd	auGlobalFrequency+32(%rip), %xmm1
	movdqa	%xmm1, auGlobalFrequency+32(%rip)
	movd	12(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	paddd	auGlobalFrequency+48(%rip), %xmm1
	movdqa	%xmm1, auGlobalFrequency+48(%rip)
	movd	16(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	paddd	auGlobalFrequency+64(%rip), %xmm1
	movdqa	%xmm1, auGlobalFrequency+64(%rip)
	movd	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	paddd	auGlobalFrequency+80(%rip), %xmm1
	movdqa	%xmm1, auGlobalFrequency+80(%rip)
	movzbl	24(%rsp), %eax
	addl	%eax, auGlobalFrequency+96(%rip)
	movzbl	25(%rsp), %eax
	addl	%eax, auGlobalFrequency+100(%rip)
	movl	cpwCand(%rip), %ebx
	cmpq	$5000, %rbx             # imm = 0x1388
	jae	.LBB6_14
# BB#8:
	leal	1(%rbx), %ebp
	movl	%ebp, cpwCand(%rip)
	movq	apwCand(,%rbx,8), %rax
	testq	%rax, %rax
	jne	.LBB6_11
# BB#9:
	movl	$32, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB6_15
# BB#10:                                # %NewWord.exit.i
	movq	%rax, apwCand(,%rbx,8)
.LBB6_11:                               # %NextWord.exit
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%rax)
	movq	%r14, 16(%rax)
	movl	%r15d, 24(%rax)
	leaq	1(%rsp), %rdx
	movq	$-416, %rsi             # imm = 0xFE60
	.p2align	4, 0x90
.LBB6_12:                               # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rdx), %edi
	movzbl	alPhrase+420(%rsi), %ecx
	shlq	%cl, %rdi
	movl	alPhrase+428(%rsi), %ecx
	orq	%rdi, (%rax,%rcx,8)
	movzbl	(%rdx), %edi
	movzbl	alPhrase+436(%rsi), %ecx
	shlq	%cl, %rdi
	movl	alPhrase+444(%rsi), %ecx
	orq	%rdi, (%rax,%rcx,8)
	addq	$2, %rdx
	addq	$32, %rsi
	jne	.LBB6_12
.LBB6_13:                               # %.loopexit
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_14:
	movl	$.L.str.10, %edi
	xorl	%esi, %esi
	callq	Fatal
.LBB6_15:
	movl	$.L.str.8, %edi
	movl	%ebp, %esi
	callq	Fatal
.Lfunc_end6:
	.size	BuildWord, .Lfunc_end6-BuildWord
	.cfi_endproc

	.globl	AddWords
	.p2align	4, 0x90
	.type	AddWords,@function
AddWords:                               # @AddWords
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 16
.Lcfi36:
	.cfi_offset %rbx, -16
	movq	pchDictionary(%rip), %rbx
	movl	$0, cpwCand(%rip)
	movb	(%rbx), %al
	testb	%al, %al
	je	.LBB7_1
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	1(%rbx), %ecx
	movl	cchMinLength(%rip), %esi
	cmpl	%esi, %ecx
	jge	.LBB7_4
# BB#3:                                 # %.lr.ph._crit_edge
                                        #   in Loop: Header=BB7_2 Depth=1
	movl	cchPhraseLength(%rip), %edx
	cmpl	%edx, %ecx
	je	.LBB7_6
	jmp	.LBB7_7
	.p2align	4, 0x90
.LBB7_4:                                #   in Loop: Header=BB7_2 Depth=1
	addl	%ecx, %esi
	movl	cchPhraseLength(%rip), %edx
	cmpl	%edx, %esi
	jle	.LBB7_6
# BB#5:                                 #   in Loop: Header=BB7_2 Depth=1
	cmpl	%edx, %ecx
	jne	.LBB7_7
.LBB7_6:                                #   in Loop: Header=BB7_2 Depth=1
	leaq	2(%rbx), %rdi
	callq	BuildWord
	movzbl	(%rbx), %eax
.LBB7_7:                                #   in Loop: Header=BB7_2 Depth=1
	movsbq	%al, %rcx
	movzbl	(%rbx,%rcx), %eax
	leaq	(%rbx,%rcx), %rbx
	testb	%al, %al
	jne	.LBB7_2
# BB#8:                                 # %._crit_edge.loopexit
	movl	cpwCand(%rip), %edx
	jmp	.LBB7_9
.LBB7_1:
	xorl	%edx, %edx
.LBB7_9:                                # %._crit_edge
	movq	stderr(%rip), %rdi
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	popq	%rbx
	jmp	fprintf                 # TAILCALL
.Lfunc_end7:
	.size	AddWords, .Lfunc_end7-AddWords
	.cfi_endproc

	.globl	DumpCandidates
	.p2align	4, 0x90
	.type	DumpCandidates,@function
DumpCandidates:                         # @DumpCandidates
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 32
.Lcfi40:
	.cfi_offset %rbx, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	cmpl	$0, cpwCand(%rip)
	je	.LBB8_3
# BB#1:                                 # %.lr.ph.preheader
	movl	$10, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %eax
	movq	apwCand(,%rax,8), %rax
	movq	16(%rax), %rsi
	movl	%ebp, %eax
	andl	$3, %eax
	cmpl	$3, %eax
	movl	$32, %edx
	cmovel	%ebx, %edx
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	printf
	incl	%ebp
	cmpl	cpwCand(%rip), %ebp
	jb	.LBB8_2
.LBB8_3:                                # %._crit_edge
	movl	$10, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	putchar                 # TAILCALL
.Lfunc_end8:
	.size	DumpCandidates, .Lfunc_end8-DumpCandidates
	.cfi_endproc

	.globl	DumpWords
	.p2align	4, 0x90
	.type	DumpWords,@function
DumpWords:                              # @DumpWords
	.cfi_startproc
# BB#0:
	movl	DumpWords.X(%rip), %eax
	incl	%eax
	andl	$1023, %eax             # imm = 0x3FF
	movl	%eax, DumpWords.X(%rip)
	je	.LBB9_1
# BB#5:
	retq
.LBB9_1:                                # %.preheader
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 16
.Lcfi43:
	.cfi_offset %rbx, -16
	cmpl	$0, cpwLast(%rip)
	jle	.LBB9_4
# BB#2:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	apwSol(,%rbx,8), %rax
	movq	16(%rax), %rsi
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	movslq	cpwLast(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB9_3
.LBB9_4:                                # %._crit_edge
	movl	$10, %edi
	popq	%rbx
	jmp	putchar                 # TAILCALL
.Lfunc_end9:
	.size	DumpWords, .Lfunc_end9-DumpWords
	.cfi_endproc

	.globl	FindAnagram
	.p2align	4, 0x90
	.type	FindAnagram,@function
FindAnagram:                            # @FindAnagram
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi50:
	.cfi_def_cfa_offset 96
.Lcfi51:
	.cfi_offset %rbx, -56
.Lcfi52:
	.cfi_offset %r12, -48
.Lcfi53:
	.cfi_offset %r13, -40
.Lcfi54:
	.cfi_offset %r14, -32
.Lcfi55:
	.cfi_offset %r15, -24
.Lcfi56:
	.cfi_offset %rbp, -16
	movq	%rdi, %r9
	movl	cpwCand(%rip), %eax
	movslq	%edx, %rcx
	leal	-1(%rcx), %ebp
	leaq	achByFrequency(%rcx), %rdx
	.p2align	4, 0x90
.LBB10_1:                               # =>This Inner Loop Header: Depth=1
	movsbq	(%rdx), %rdi
	shlq	$4, %rdi
	movl	alPhrase+8(%rdi), %ebx
	movzbl	alPhrase+4(%rdi), %ecx
	shll	%cl, %ebx
	movl	alPhrase+12(%rdi), %r13d
	incl	%ebp
	incq	%rdx
	testq	(%r9,%r13,8), %rbx
	je	.LBB10_1
# BB#2:                                 # %.preheader
	leaq	apwCand(,%rax,8), %r15
	cmpq	%rsi, %r15
	jbe	.LBB10_12
# BB#3:                                 # %.lr.ph.lr.ph
	movq	%rsp, %rdi
                                        # implicit-def: %R14
.LBB10_7:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_4 Depth 2
                                        #     Child Loop BB10_18 Depth 2
	movq	(%r9), %r10
	movq	aqMainSign(%rip), %rcx
	movq	aqMainSign+8(%rip), %r8
	.p2align	4, 0x90
.LBB10_4:                               #   Parent Loop BB10_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %r12
	movq	%r10, %rax
	subq	(%r12), %rax
	testq	%rax, %rcx
	jne	.LBB10_5
# BB#8:                                 #   in Loop: Header=BB10_4 Depth=2
	movq	8(%r9), %r14
	subq	8(%r12), %r14
	testq	%r14, %r8
	jne	.LBB10_5
# BB#9:                                 #   in Loop: Header=BB10_4 Depth=2
	testq	(%r12,%r13,8), %rbx
	jne	.LBB10_13
# BB#10:                                #   in Loop: Header=BB10_4 Depth=2
	movq	-8(%r15), %rdx
	movq	%rdx, (%rsi)
	movq	%r12, -8(%r15)
	leaq	-8(%r15), %r15
	cmpq	%r15, %rsi
	jb	.LBB10_4
	jmp	.LBB10_11
	.p2align	4, 0x90
.LBB10_5:                               #   in Loop: Header=BB10_7 Depth=1
	movq	%rax, (%rsp)
	movq	%r14, 8(%rsp)
	jmp	.LBB10_6
	.p2align	4, 0x90
.LBB10_13:                              #   in Loop: Header=BB10_7 Depth=1
	movq	%rax, (%rsp)
	movq	%r14, 8(%rsp)
	movslq	cpwLast(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, cpwLast(%rip)
	movq	%r12, apwSol(,%rax,8)
	movl	cchPhraseLength(%rip), %ecx
	subl	24(%r12), %ecx
	movl	%ecx, cchPhraseLength(%rip)
	je	.LBB10_15
# BB#14:                                #   in Loop: Header=BB10_7 Depth=1
	movl	cpwCand(%rip), %eax
	leaq	apwCand(,%rax,8), %r15
	movl	%ebp, %edx
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%r9, 24(%rsp)           # 8-byte Spill
	callq	FindAnagram
	movq	%rsp, %rdi
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB10_20
.LBB10_15:                              #   in Loop: Header=BB10_7 Depth=1
	movl	DumpWords.X(%rip), %ecx
	incl	%ecx
	andl	$1023, %ecx             # imm = 0x3FF
	movl	%ecx, DumpWords.X(%rip)
	jne	.LBB10_20
# BB#16:                                # %.preheader.i
                                        #   in Loop: Header=BB10_7 Depth=1
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	testl	%eax, %eax
	js	.LBB10_19
# BB#17:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB10_7 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB10_18:                              # %.lr.ph.i
                                        #   Parent Loop BB10_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	apwSol(,%rax,8), %rax
	movq	16(%rax), %rsi
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	printf
	incq	16(%rsp)                # 8-byte Folded Spill
	movslq	cpwLast(%rip), %rax
	cmpq	%rax, 16(%rsp)          # 8-byte Folded Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	jl	.LBB10_18
.LBB10_19:                              # %._crit_edge.i
                                        #   in Loop: Header=BB10_7 Depth=1
	movl	$10, %edi
	callq	putchar
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	%rsp, %rdi
	.p2align	4, 0x90
.LBB10_20:                              # %DumpWords.exit
                                        #   in Loop: Header=BB10_7 Depth=1
	movl	24(%r12), %eax
	addl	%eax, cchPhraseLength(%rip)
	decl	cpwLast(%rip)
.LBB10_6:                               # %.outer.backedge
                                        #   in Loop: Header=BB10_7 Depth=1
	addq	$8, %rsi
	cmpq	%r15, %rsi
	jb	.LBB10_7
	jmp	.LBB10_12
.LBB10_11:                              # %._crit_edge
	movq	%rax, (%rsp)
	movq	%r14, 8(%rsp)
.LBB10_12:                              # %.outer._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	FindAnagram, .Lfunc_end10-FindAnagram
	.cfi_endproc

	.globl	CompareFrequency
	.p2align	4, 0x90
	.type	CompareFrequency,@function
CompareFrequency:                       # @CompareFrequency
	.cfi_startproc
# BB#0:
	movsbq	(%rdi), %rcx
	movsbq	(%rsi), %rdx
	movl	auGlobalFrequency(,%rdx,4), %esi
	movl	$-1, %eax
	cmpl	%esi, auGlobalFrequency(,%rcx,4)
	jb	.LBB11_4
# BB#1:
	movl	$1, %eax
	ja	.LBB11_4
# BB#2:
	cmpb	%dl, %cl
	movl	$-1, %eax
	jl	.LBB11_4
# BB#3:
	setg	%al
	movzbl	%al, %eax
.LBB11_4:
	retq
.Lfunc_end11:
	.size	CompareFrequency, .Lfunc_end11-CompareFrequency
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI12_0:
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	9                       # 0x9
	.byte	10                      # 0xa
	.byte	11                      # 0xb
	.byte	12                      # 0xc
	.byte	13                      # 0xd
	.byte	14                      # 0xe
	.byte	15                      # 0xf
	.text
	.globl	SortCandidates
	.p2align	4, 0x90
	.type	SortCandidates,@function
SortCandidates:                         # @SortCandidates
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 16
.Lcfi58:
	.cfi_offset %rbx, -16
	movaps	.LCPI12_0(%rip), %xmm0  # xmm0 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
	movaps	%xmm0, achByFrequency(%rip)
	movabsq	$1663540288323457296, %rax # imm = 0x1716151413121110
	movq	%rax, achByFrequency+16(%rip)
	movw	$6424, achByFrequency+24(%rip) # imm = 0x1918
	movl	$achByFrequency, %edi
	movl	$26, %esi
	movl	$1, %edx
	movl	$CompareFrequency, %ecx
	callq	qsort
	movq	stderr(%rip), %rcx
	movl	$.L.str.14, %edi
	movl	$24, %esi
	movl	$1, %edx
	callq	fwrite
	movq	$-26, %rbx
	.p2align	4, 0x90
.LBB12_1:                               # =>This Inner Loop Header: Depth=1
	movsbl	achByFrequency+26(%rbx), %edi
	addl	$97, %edi
	movq	stderr(%rip), %rsi
	callq	fputc
	incq	%rbx
	jne	.LBB12_1
# BB#2:
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	popq	%rbx
	jmp	fputc                   # TAILCALL
.Lfunc_end12:
	.size	SortCandidates, .Lfunc_end12-SortCandidates
	.cfi_endproc

	.globl	GetPhrase
	.p2align	4, 0x90
	.type	GetPhrase,@function
GetPhrase:                              # @GetPhrase
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi61:
	.cfi_def_cfa_offset 32
.Lcfi62:
	.cfi_offset %rbx, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	cmpl	$0, fInteractive(%rip)
	je	.LBB13_2
# BB#1:
	movl	$62, %edi
	callq	putchar
.LBB13_2:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stdin(%rip), %rdx
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	fgets
	testq	%rax, %rax
	je	.LBB13_4
# BB#3:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB13_4:
	xorl	%edi, %edi
	callq	exit
.Lfunc_end13:
	.size	GetPhrase, .Lfunc_end13-GetPhrase
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi66:
	.cfi_def_cfa_offset 32
.Lcfi67:
	.cfi_offset %rbx, -24
.Lcfi68:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %eax
	andl	$-2, %eax
	cmpl	$2, %eax
	jne	.LBB14_27
# BB#1:
	cmpl	$3, %edi
	jne	.LBB14_3
# BB#2:
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, cchMinLength(%rip)
.LBB14_3:
	movl	$1, %edi
	callq	isatty
	movl	%eax, fInteractive(%rip)
	movq	8(%rbx), %rdi
	callq	ReadDict
	movl	$10, %ebp
	cmpl	$0, fInteractive(%rip)
	jne	.LBB14_5
	jmp	.LBB14_6
	.p2align	4, 0x90
.LBB14_8:                               #   in Loop: Header=BB14_6 Depth=1
	movl	$achPhrase, %edi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rcx
	movl	%ecx, cchMinLength(%rip)
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
.LBB14_4:                               # %.backedge
                                        #   in Loop: Header=BB14_6 Depth=1
	cmpl	$0, fInteractive(%rip)
	je	.LBB14_6
.LBB14_5:
	movl	$62, %edi
	callq	putchar
.LBB14_6:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_16 Depth 2
                                        #     Child Loop BB14_12 Depth 2
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stdin(%rip), %rdx
	movl	$achPhrase, %edi
	movl	$255, %esi
	callq	fgets
	testq	%rax, %rax
	je	.LBB14_28
# BB#7:                                 # %GetPhrase.exit
                                        #   in Loop: Header=BB14_6 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movsbq	achPhrase(%rip), %rax
	testb	$8, 1(%rcx,%rax,2)
	jne	.LBB14_8
# BB#9:                                 #   in Loop: Header=BB14_6 Depth=1
	cmpb	$63, %al
	jne	.LBB14_14
# BB#10:                                #   in Loop: Header=BB14_6 Depth=1
	cmpl	$0, cpwCand(%rip)
	je	.LBB14_13
# BB#11:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB14_6 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB14_12:                              # %.lr.ph.i
                                        #   Parent Loop BB14_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %eax
	movq	apwCand(,%rax,8), %rax
	movq	16(%rax), %rsi
	movl	%ebx, %eax
	andl	$3, %eax
	cmpl	$3, %eax
	movl	$32, %edx
	cmovel	%ebp, %edx
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	printf
	incl	%ebx
	cmpl	cpwCand(%rip), %ebx
	jb	.LBB14_12
.LBB14_13:                              # %DumpCandidates.exit
                                        #   in Loop: Header=BB14_6 Depth=1
	movl	$10, %edi
	callq	putchar
	cmpl	$0, fInteractive(%rip)
	jne	.LBB14_5
	jmp	.LBB14_6
	.p2align	4, 0x90
.LBB14_14:                              #   in Loop: Header=BB14_6 Depth=1
	movl	$achPhrase, %edi
	callq	BuildMask
	movq	pchDictionary(%rip), %rbx
	movl	$0, cpwCand(%rip)
	movb	(%rbx), %al
	testb	%al, %al
	je	.LBB14_15
	.p2align	4, 0x90
.LBB14_16:                              # %.lr.ph.i8
                                        #   Parent Loop BB14_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	1(%rbx), %ecx
	movl	cchMinLength(%rip), %esi
	cmpl	%esi, %ecx
	jge	.LBB14_18
# BB#17:                                # %.lr.ph._crit_edge.i
                                        #   in Loop: Header=BB14_16 Depth=2
	movl	cchPhraseLength(%rip), %edx
	cmpl	%edx, %ecx
	je	.LBB14_20
	jmp	.LBB14_21
	.p2align	4, 0x90
.LBB14_18:                              #   in Loop: Header=BB14_16 Depth=2
	addl	%ecx, %esi
	movl	cchPhraseLength(%rip), %edx
	cmpl	%edx, %esi
	jle	.LBB14_20
# BB#19:                                #   in Loop: Header=BB14_16 Depth=2
	cmpl	%edx, %ecx
	jne	.LBB14_21
.LBB14_20:                              #   in Loop: Header=BB14_16 Depth=2
	leaq	2(%rbx), %rdi
	callq	BuildWord
	movzbl	(%rbx), %eax
.LBB14_21:                              #   in Loop: Header=BB14_16 Depth=2
	movsbq	%al, %rcx
	movzbl	(%rbx,%rcx), %eax
	leaq	(%rbx,%rcx), %rbx
	testb	%al, %al
	jne	.LBB14_16
# BB#22:                                # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB14_6 Depth=1
	movl	cpwCand(%rip), %edx
.LBB14_23:                              # %AddWords.exit
                                        #   in Loop: Header=BB14_6 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	callq	fprintf
	cmpl	$0, cpwCand(%rip)
	je	.LBB14_4
# BB#24:                                # %AddWords.exit
                                        #   in Loop: Header=BB14_6 Depth=1
	movl	cchPhraseLength(%rip), %eax
	testl	%eax, %eax
	je	.LBB14_4
# BB#25:                                #   in Loop: Header=BB14_6 Depth=1
	movl	$0, cpwLast(%rip)
	callq	SortCandidates
	movl	$jbAnagram, %edi
	callq	_setjmp
	testl	%eax, %eax
	jne	.LBB14_4
# BB#26:                                #   in Loop: Header=BB14_6 Depth=1
	movl	$aqMainMask, %edi
	movl	$apwCand, %esi
	xorl	%edx, %edx
	callq	FindAnagram
	cmpl	$0, fInteractive(%rip)
	jne	.LBB14_5
	jmp	.LBB14_6
.LBB14_15:                              #   in Loop: Header=BB14_6 Depth=1
	xorl	%edx, %edx
	jmp	.LBB14_23
.LBB14_28:
	xorl	%edi, %edi
	callq	exit
.LBB14_27:
	movl	$.L.str.16, %edi
	xorl	%esi, %esi
	callq	Fatal
.Lfunc_end14:
	.size	main, .Lfunc_end14-main
	.cfi_endproc

	.type	cchMinLength,@object    # @cchMinLength
	.data
	.globl	cchMinLength
	.p2align	2
cchMinLength:
	.long	3                       # 0x3
	.size	cchMinLength, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Cannot stat dictionary\n"
	.size	.L.str, 24

	.type	pchDictionary,@object   # @pchDictionary
	.comm	pchDictionary,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Unable to allocate memory for dictionary\n"
	.size	.L.str.1, 42

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"r"
	.size	.L.str.2, 2

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Cannot open dictionary\n"
	.size	.L.str.3, 24

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"main dictionary has %u entries\n"
	.size	.L.str.4, 32

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Dictionary too large; increase MAXWORDS\n"
	.size	.L.str.5, 41

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%lu bytes wasted\n"
	.size	.L.str.6, 18

	.type	alPhrase,@object        # @alPhrase
	.comm	alPhrase,416,16
	.type	aqMainMask,@object      # @aqMainMask
	.comm	aqMainMask,16,16
	.type	aqMainSign,@object      # @aqMainSign
	.comm	aqMainSign,16,16
	.type	cchPhraseLength,@object # @cchPhraseLength
	.comm	cchPhraseLength,4,4
	.type	auGlobalFrequency,@object # @auGlobalFrequency
	.comm	auGlobalFrequency,104,16
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"MAX_QUADS not large enough\n"
	.size	.L.str.7, 28

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Out of memory after %d candidates\n"
	.size	.L.str.8, 35

	.type	cpwCand,@object         # @cpwCand
	.comm	cpwCand,4,4
	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%s "
	.size	.L.str.9, 4

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Too many candidates\n"
	.size	.L.str.10, 21

	.type	apwCand,@object         # @apwCand
	.comm	apwCand,40000,16
	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%d candidates\n"
	.size	.L.str.11, 15

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"%15s%c"
	.size	.L.str.12, 7

	.type	DumpWords.X,@object     # @DumpWords.X
	.local	DumpWords.X
	.comm	DumpWords.X,4,4
	.type	cpwLast,@object         # @cpwLast
	.comm	cpwLast,4,4
	.type	apwSol,@object          # @apwSol
	.comm	apwSol,408,16
	.type	achByFrequency,@object  # @achByFrequency
	.comm	achByFrequency,26,16
	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Order of search will be "
	.size	.L.str.14, 25

	.type	fInteractive,@object    # @fInteractive
	.comm	fInteractive,4,4
	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Usage: anagram dictionary [length]\n"
	.size	.L.str.16, 36

	.type	achPhrase,@object       # @achPhrase
	.comm	achPhrase,255,16
	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"New length: %d\n"
	.size	.L.str.17, 16

	.type	jbAnagram,@object       # @jbAnagram
	.comm	jbAnagram,200,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
