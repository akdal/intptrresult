	.text
	.file	"libclamav_sis.bc"
	.globl	cli_scansis
	.p2align	4, 0x90
	.type	cli_scansis,@function
cli_scansis:                            # @cli_scansis
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 288
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movl	%edi, %ebp
	movb	$0, 15(%rsp)
	leaq	88(%rsp), %rdx
	movl	$1, %edi
	movl	%ebp, %esi
	callq	__fxstat
	cmpl	$-1, %eax
	je	.LBB0_4
# BB#1:
	movq	136(%rsp), %r12
	cmpq	$67, %r12
	jg	.LBB0_5
# BB#2:
	xorl	%ebp, %ebp
	movl	$.L.str.1, %edi
.LBB0_3:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB0_59
.LBB0_4:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-123, %ebp
	jmp	.LBB0_59
.LBB0_5:
	cmpq	$134217728, %r12        # imm = 0x8000000
	ja	.LBB0_11
# BB#6:
	xorl	%edi, %edi
	movl	$1, %edx
	movl	$2, %ecx
	xorl	%r9d, %r9d
	movq	%r12, %rsi
	movl	%ebp, %r8d
	callq	mmap
	movq	%rax, %r14
	cmpq	$-1, %r14
	je	.LBB0_12
# BB#7:
	xorl	%ebp, %ebp
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpl	$268436505, 8(%r14)     # imm = 0x10000419
	jne	.LBB0_13
# BB#8:
	movl	4(%r14), %esi
	movzwl	18(%r14), %r13d
	movzwl	20(%r14), %r10d
	movzwl	22(%r14), %r8d
	movzwl	24(%r14), %r9d
	movzwl	36(%r14), %r11d
	movzwl	38(%r14), %edx
	movzwl	40(%r14), %ecx
	movzwl	42(%r14), %eax
	movl	48(%r14), %ebp
	movl	52(%r14), %edi
	movl	56(%r14), %ebx
	cmpl	$268450321, %esi        # imm = 0x10003A11
	jg	.LBB0_15
# BB#9:
	cmpl	$268435565, %esi        # imm = 0x1000006D
	je	.LBB0_18
# BB#10:
	cmpl	$268450254, %esi        # imm = 0x100039CE
	je	.LBB0_17
	jmp	.LBB0_37
.LBB0_11:
	xorl	%ebp, %ebp
	movl	$.L.str.4, %edi
	movl	$134217728, %esi        # imm = 0x8000000
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB0_59
.LBB0_12:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-114, %ebp
	jmp	.LBB0_59
.LBB0_13:
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_14:
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	munmap
	jmp	.LBB0_59
.LBB0_15:
	cmpl	$268450322, %esi        # imm = 0x10003A12
	je	.LBB0_19
# BB#16:
	cmpl	$268450360, %esi        # imm = 0x10003A38
	jne	.LBB0_37
.LBB0_17:
	xorl	%ebp, %ebp
	movl	$.L.str.8, %edi
	jmp	.LBB0_3
.LBB0_18:
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movl	%r10d, 48(%rsp)         # 4-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movl	%r9d, 68(%rsp)          # 4-byte Spill
	movl	%ebx, 72(%rsp)          # 4-byte Spill
	movl	%r8d, 76(%rsp)          # 4-byte Spill
	movl	%r11d, 80(%rsp)         # 4-byte Spill
	movw	%dx, 54(%rsp)           # 2-byte Spill
	movl	%ecx, 84(%rsp)          # 4-byte Spill
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$3, 44(%rsp)            # 4-byte Folded Spill
	jmp	.LBB0_20
.LBB0_19:
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movl	%r10d, 48(%rsp)         # 4-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movl	%r9d, 68(%rsp)          # 4-byte Spill
	movl	%ebx, 72(%rsp)          # 4-byte Spill
	movl	%r8d, 76(%rsp)          # 4-byte Spill
	movl	%r11d, 80(%rsp)         # 4-byte Spill
	movw	%dx, 54(%rsp)           # 2-byte Spill
	movl	%ecx, 84(%rsp)          # 4-byte Spill
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$6, 44(%rsp)            # 4-byte Folded Spill
.LBB0_20:
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	cli_dbgmsg
	leal	-1(%r13), %eax
	movzwl	%ax, %eax
	cmpl	$98, %eax
	ja	.LBB0_38
# BB#21:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	%r12, %rax
	jae	.LBB0_55
# BB#22:
	leaq	(%r13,%r13), %rbx
	leal	(%rbx,%rax), %eax
	cmpl	136(%rsp), %eax
	jae	.LBB0_55
# BB#23:
	movq	%rbx, %rdi
	callq	cli_malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_39
# BB#24:
	movq	%r14, %rsi
	addq	16(%rsp), %rsi          # 8-byte Folded Reload
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	1(%r13,%r13,2), %rdi
	movl	$1, %esi
	callq	cli_calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_40
# BB#25:                                # %.lr.ph243
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%r13, 56(%rsp)          # 8-byte Spill
	leal	-1(%r13), %ebp
	xorl	%r13d, %r13d
.LBB0_26:                               # =>This Inner Loop Header: Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movzwl	(%rax,%r13,2), %eax
	movl	%eax, %ecx
	shrl	%ecx
	imull	$2675, %ecx, %ecx       # imm = 0xA73
	shrl	$17, %ecx
	imull	$98, %ecx, %ecx
	subl	%ecx, %eax
	movzwl	%ax, %eax
	movq	langcodes(,%rax,8), %rsi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	strncat
	cmpq	%r13, %rbp
	je	.LBB0_28
# BB#27:                                #   in Loop: Header=BB0_26 Depth=1
	movq	%rbx, %rdi
	callq	strlen
	movw	$32, (%rbx,%rax)
.LBB0_28:                               #   in Loop: Header=BB0_26 Depth=1
	incq	%r13
	cmpq	%r13, 56(%rsp)          # 8-byte Folded Reload
	jne	.LBB0_26
# BB#29:                                # %._crit_edge244
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%rbx, %rdi
	callq	free
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movl	68(%rsp), %esi          # 4-byte Reload
	testw	%si, %si
	je	.LBB0_31
# BB#30:
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_31:
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	movl	76(%rsp), %esi          # 4-byte Reload
	callq	cli_dbgmsg
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	movl	72(%rsp), %esi          # 4-byte Reload
	callq	cli_dbgmsg
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	80(%rsp), %eax          # 4-byte Reload
	testb	$1, %al
	movq	24(%rsp), %rbx          # 8-byte Reload
	je	.LBB0_33
# BB#32:
	movl	%eax, %ebp
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%ebp, %eax
.LBB0_33:
	testb	$2, %al
	je	.LBB0_35
# BB#34:
	movl	%eax, %ebp
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%ebp, %eax
.LBB0_35:
	movl	%eax, %r13d
	testb	$8, %al
	jne	.LBB0_41
# BB#36:
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movb	$1, %bpl
	testb	$16, %r13b
	jne	.LBB0_42
	jmp	.LBB0_43
.LBB0_37:
	xorl	%ebp, %ebp
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB0_14
.LBB0_38:
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	cli_errmsg
	jmp	.LBB0_57
.LBB0_39:
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	munmap
	movl	$-114, %ebp
	jmp	.LBB0_59
.LBB0_40:
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	munmap
	movq	%rbp, %rdi
	callq	free
	movl	$-114, %ebp
	jmp	.LBB0_59
.LBB0_41:
	xorl	%ebp, %ebp
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testb	$16, %r13b
	je	.LBB0_43
.LBB0_42:
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_43:
	movzwl	54(%rsp), %eax          # 2-byte Folded Reload
	cmpl	$5, %eax
	ja	.LBB0_46
# BB#44:
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_45:
	movl	$.L.str.25, %edi
	jmp	.LBB0_52
.LBB0_46:
	movl	$.L.str.31, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB0_53
.LBB0_47:
	movl	$.L.str.26, %edi
	jmp	.LBB0_52
.LBB0_48:
	movl	$.L.str.27, %edi
	jmp	.LBB0_52
.LBB0_49:
	movl	$.L.str.28, %edi
	jmp	.LBB0_52
.LBB0_50:
	movl	$.L.str.29, %edi
	jmp	.LBB0_52
.LBB0_51:
	movl	$.L.str.30, %edi
.LBB0_52:
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_53:
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	movl	84(%rsp), %esi          # 4-byte Reload
	callq	cli_dbgmsg
	movl	$.L.str.33, %edi
	xorl	%eax, %eax
	movl	40(%rsp), %esi          # 4-byte Reload
	callq	cli_dbgmsg
	cmpl	$6, 44(%rsp)            # 4-byte Folded Reload
	jne	.LBB0_61
# BB#54:
	cmpq	$100, %r12
	ja	.LBB0_60
.LBB0_55:
	movl	$.L.str.11, %edi
.LBB0_56:
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB0_57:
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	munmap
.LBB0_58:
	movl	$-124, %ebp
.LBB0_59:
	movl	%ebp, %eax
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_60:
	movl	80(%r14), %esi
	movl	$.L.str.34, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_61:
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.LBB0_66
# BB#62:
	movl	4(%rax), %esi
	testl	%esi, %esi
	je	.LBB0_66
# BB#63:
	cmpl	%esi, 48(%rsp)          # 4-byte Folded Reload
	jbe	.LBB0_66
# BB#64:
	xorl	%ebp, %ebp
	movl	$.L.str.35, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testb	$1, 41(%r15)
	je	.LBB0_59
# BB#65:
	movq	(%r15), %rax
	movq	$.L.str.36, (%rax)
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	munmap
	movl	$1, %ebp
	jmp	.LBB0_59
.LBB0_66:
	movl	$.L.str.37, %edi
	xorl	%eax, %eax
	movl	48(%rsp), %esi          # 4-byte Reload
	callq	cli_dbgmsg
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_dbgmsg
	cmpq	%r12, %rbx
	jae	.LBB0_92
# BB#67:
	movl	%ebp, %ebx
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_93
# BB#68:
	movl	$448, %esi              # imm = 0x1C0
	movq	%rbp, %rdi
	callq	mkdir
	cmpl	$-1, %eax
	je	.LBB0_93
# BB#69:                                # %.preheader
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movl	%ebx, 40(%rsp)          # 4-byte Spill
	cmpw	$0, 48(%rsp)            # 2-byte Folded Reload
	je	.LBB0_115
# BB#70:                                # %.lr.ph
	cmpl	$6, 44(%rsp)            # 4-byte Folded Reload
	jne	.LBB0_94
# BB#71:                                # %.lr.ph.split.us.preheader
	movq	56(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%rax,2), %eax
	leal	36(,%rax,4), %eax
	movl	%eax, 44(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB0_72:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	leal	4(%rbp), %r13d
	cmpq	%r12, %r13
	jae	.LBB0_118
# BB#73:                                #   in Loop: Header=BB0_72 Depth=1
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movl	%ebp, %ebx
	movl	(%r14,%rbx), %eax
	cmpq	$6, %rax
	ja	.LBB0_78
# BB#74:                                #   in Loop: Header=BB0_72 Depth=1
	jmpq	*.LJTI0_2(,%rax,8)
.LBB0_75:                               #   in Loop: Header=BB0_72 Depth=1
	movl	$.L.str.48, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	24(%rsp), %rax          # 8-byte Reload
	addl	$8, %eax
	movq	%rax, %r13
	cmpq	%r12, %rax
	jae	.LBB0_120
# BB#76:                                #   in Loop: Header=BB0_72 Depth=1
	movl	4(%r14,%rbx), %ebp
	movl	$.L.str.49, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	cmpq	%r12, %rbp
	jae	.LBB0_123
# BB#77:                                #   in Loop: Header=BB0_72 Depth=1
	addl	%ebp, %r13d
	jmp	.LBB0_79
.LBB0_78:                               #   in Loop: Header=BB0_72 Depth=1
	movl	$.L.str.53, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB0_79:                               #   in Loop: Header=BB0_72 Depth=1
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	jmp	.LBB0_91
.LBB0_80:                               #   in Loop: Header=BB0_72 Depth=1
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	136(%rsp), %esi
	movzbl	40(%rsp), %r8d          # 1-byte Folded Reload
	movl	$1, %ecx
	movq	%r14, %rdi
	movl	%r13d, %edx
	leaq	15(%rsp), %r9
	pushq	%r15
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	24(%rsp)                # 8-byte Folded Reload
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	sis_extract_simple
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB0_124
# BB#81:                                #   in Loop: Header=BB0_72 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	addl	$48, %eax
	movl	%eax, %r13d
	jmp	.LBB0_91
.LBB0_82:                               #   in Loop: Header=BB0_72 Depth=1
	movl	$.L.str.44, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	136(%rsp), %esi
	movzbl	40(%rsp), %r8d          # 1-byte Folded Reload
	movq	%r14, %rdi
	movl	%r13d, %edx
	movq	56(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	leaq	15(%rsp), %r9
	pushq	%r15
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	24(%rsp)                # 8-byte Folded Reload
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	callq	sis_extract_simple
	addq	$16, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB0_124
# BB#83:                                #   in Loop: Header=BB0_72 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	addl	44(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, %r13d
	jmp	.LBB0_91
.LBB0_84:                               #   in Loop: Header=BB0_72 Depth=1
	movl	$.L.str.45, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	8(%rax), %eax
	cmpq	%r12, %rax
	jae	.LBB0_120
# BB#85:                                #   in Loop: Header=BB0_72 Depth=1
	movl	4(%r14,%rbx), %ebp
	movl	$.L.str.46, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	cmpl	$128, %ebp
	movq	24(%rsp), %rcx          # 8-byte Reload
	ja	.LBB0_127
# BB#86:                                #   in Loop: Header=BB0_72 Depth=1
	imull	56(%rsp), %ebp          # 4-byte Folded Reload
	leal	(%rcx,%rbp,8), %eax
	cmpq	%r12, %rax
	jae	.LBB0_127
# BB#87:                                #   in Loop: Header=BB0_72 Depth=1
	shll	$3, %ebp
	leal	24(%rcx,%rbp), %r13d
	jmp	.LBB0_91
.LBB0_88:                               #   in Loop: Header=BB0_72 Depth=1
	movl	$.L.str.51, %edi
	jmp	.LBB0_90
.LBB0_89:                               #   in Loop: Header=BB0_72 Depth=1
	movl	$.L.str.52, %edi
.LBB0_90:                               #   in Loop: Header=BB0_72 Depth=1
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_91:                               #   in Loop: Header=BB0_72 Depth=1
	movl	32(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	48(%rsp), %eax          # 4-byte Folded Reload
	movl	%r13d, %ebp
	jl	.LBB0_72
	jmp	.LBB0_115
.LBB0_92:
	movl	$.L.str.39, %edi
	jmp	.LBB0_56
.LBB0_93:
	testq	%rbp, %rbp
	movl	$.L.str.41, %esi
	cmovneq	%rbp, %rsi
	movl	$.L.str.40, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	munmap
	movl	$-118, %ebp
	jmp	.LBB0_59
.LBB0_94:                               # %.lr.ph.split.preheader
	movq	56(%rsp), %rax          # 8-byte Reload
	leal	32(,%rax,4), %eax
	movl	%eax, 44(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB0_95:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	leal	4(%rbp), %r13d
	cmpq	%r12, %r13
	jae	.LBB0_118
# BB#96:                                #   in Loop: Header=BB0_95 Depth=1
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movl	%ebp, %ebx
	movl	(%r14,%rbx), %eax
	cmpq	$6, %rax
	ja	.LBB0_101
# BB#97:                                #   in Loop: Header=BB0_95 Depth=1
	jmpq	*.LJTI0_1(,%rax,8)
.LBB0_98:                               #   in Loop: Header=BB0_95 Depth=1
	movl	$.L.str.48, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	24(%rsp), %rax          # 8-byte Reload
	addl	$8, %eax
	movq	%rax, %r13
	cmpq	%r12, %rax
	jae	.LBB0_120
# BB#99:                                #   in Loop: Header=BB0_95 Depth=1
	movl	4(%r14,%rbx), %ebp
	movl	$.L.str.49, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	cmpq	%r12, %rbp
	jae	.LBB0_123
# BB#100:                               #   in Loop: Header=BB0_95 Depth=1
	addl	%ebp, %r13d
	jmp	.LBB0_102
.LBB0_101:                              #   in Loop: Header=BB0_95 Depth=1
	movl	$.L.str.53, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB0_102:                              #   in Loop: Header=BB0_95 Depth=1
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	jmp	.LBB0_114
.LBB0_103:                              #   in Loop: Header=BB0_95 Depth=1
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	136(%rsp), %esi
	movzbl	40(%rsp), %r8d          # 1-byte Folded Reload
	movl	$1, %ecx
	movq	%r14, %rdi
	movl	%r13d, %edx
	leaq	15(%rsp), %r9
	pushq	%r15
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	24(%rsp)                # 8-byte Folded Reload
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	callq	sis_extract_simple
	addq	$16, %rsp
.Lcfi21:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB0_124
# BB#104:                               #   in Loop: Header=BB0_95 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	addl	$36, %eax
	movl	%eax, %r13d
	jmp	.LBB0_114
.LBB0_105:                              #   in Loop: Header=BB0_95 Depth=1
	movl	$.L.str.44, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	136(%rsp), %esi
	movzbl	40(%rsp), %r8d          # 1-byte Folded Reload
	movq	%r14, %rdi
	movl	%r13d, %edx
	movq	56(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	leaq	15(%rsp), %r9
	pushq	%r15
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	24(%rsp)                # 8-byte Folded Reload
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	callq	sis_extract_simple
	addq	$16, %rsp
.Lcfi24:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB0_124
# BB#106:                               #   in Loop: Header=BB0_95 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	addl	44(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, %r13d
	jmp	.LBB0_114
.LBB0_107:                              #   in Loop: Header=BB0_95 Depth=1
	movl	$.L.str.45, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	8(%rax), %eax
	cmpq	%r12, %rax
	jae	.LBB0_120
# BB#108:                               #   in Loop: Header=BB0_95 Depth=1
	movl	4(%r14,%rbx), %ebp
	movl	$.L.str.46, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	cmpl	$128, %ebp
	movq	24(%rsp), %rcx          # 8-byte Reload
	ja	.LBB0_127
# BB#109:                               #   in Loop: Header=BB0_95 Depth=1
	imull	56(%rsp), %ebp          # 4-byte Folded Reload
	leal	(%rcx,%rbp,8), %eax
	cmpq	%r12, %rax
	jae	.LBB0_127
# BB#110:                               #   in Loop: Header=BB0_95 Depth=1
	shll	$3, %ebp
	leal	24(%rcx,%rbp), %r13d
	jmp	.LBB0_114
.LBB0_111:                              #   in Loop: Header=BB0_95 Depth=1
	movl	$.L.str.51, %edi
	jmp	.LBB0_113
.LBB0_112:                              #   in Loop: Header=BB0_95 Depth=1
	movl	$.L.str.52, %edi
.LBB0_113:                              #   in Loop: Header=BB0_95 Depth=1
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_114:                              #   in Loop: Header=BB0_95 Depth=1
	movl	32(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	48(%rsp), %eax          # 4-byte Folded Reload
	movl	%r13d, %ebp
	jl	.LBB0_95
.LBB0_115:                              # %._crit_edge
	movl	$.L.str.54, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	cli_scandir
	movl	%eax, %ebp
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB0_117
# BB#116:
	movq	%rbx, %rdi
	callq	cli_rmdirs
.LBB0_117:
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB0_14
.LBB0_118:                              # %.us-lcssa.us
	movl	$.L.str.39, %edi
.LBB0_119:                              # %.us-lcssa.us
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB0_120:                              # %.us-lcssa.us
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	munmap
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB0_122
# BB#121:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	cli_rmdirs
.LBB0_122:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	jmp	.LBB0_58
.LBB0_123:                              # %.us-lcssa237.us
	movl	$.L.str.50, %edi
	jmp	.LBB0_119
.LBB0_124:                              # %.us-lcssa241.us
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	munmap
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB0_126
# BB#125:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	cli_rmdirs
.LBB0_126:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	jmp	.LBB0_59
.LBB0_127:                              # %.us-lcssa239.us
	movl	$.L.str.47, %edi
	jmp	.LBB0_119
.Lfunc_end0:
	.size	cli_scansis, .Lfunc_end0-cli_scansis
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_45
	.quad	.LBB0_47
	.quad	.LBB0_48
	.quad	.LBB0_49
	.quad	.LBB0_50
	.quad	.LBB0_51
.LJTI0_1:
	.quad	.LBB0_103
	.quad	.LBB0_105
	.quad	.LBB0_107
	.quad	.LBB0_98
	.quad	.LBB0_98
	.quad	.LBB0_111
	.quad	.LBB0_112
.LJTI0_2:
	.quad	.LBB0_80
	.quad	.LBB0_82
	.quad	.LBB0_84
	.quad	.LBB0_75
	.quad	.LBB0_75
	.quad	.LBB0_88
	.quad	.LBB0_89

	.text
	.p2align	4, 0x90
	.type	sis_extract_simple,@function
sis_extract_simple:                     # @sis_extract_simple
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi31:
	.cfi_def_cfa_offset 288
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movl	%r8d, %r14d
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%esi, %r12d
	movq	%rdi, %rbx
	movq	$0, 32(%rsp)
	leal	24(%rdx,%rcx,8), %eax
	cmpl	%r12d, %eax
	jae	.LBB1_4
# BB#1:
	movl	%edx, %edx
	leaq	(%rbx,%rdx), %r15
	movl	(%rbx,%rdx), %eax
	cmpq	$5, %rax
	movq	%r9, 80(%rsp)           # 8-byte Spill
	ja	.LBB1_8
# BB#2:
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_3:
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	$.L.str.150, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.151, %r13d
	jmp	.LBB1_9
.LBB1_4:
	movl	$.L.str.149, %edi
.LBB1_5:
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB1_6:
	movl	$-124, %ebp
.LBB1_7:
	movl	%ebp, %eax
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_8:
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	movl	$.L.str.166, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB1_9:
	movb	$1, %al
	movl	%eax, 16(%rsp)          # 4-byte Spill
	jmp	.LBB1_31
.LBB1_10:
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	movl	$.L.str.152, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.153, %r13d
	jmp	.LBB1_31
.LBB1_11:
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	movl	$.L.str.154, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.155, %r13d
	jmp	.LBB1_31
.LBB1_12:
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	$.L.str.156, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	4(%r15), %ebp
	cmpb	$2, %bpl
	je	.LBB1_19
# BB#13:
	cmpb	$1, %bpl
	je	.LBB1_20
# BB#14:
	testb	%bpl, %bpl
	jne	.LBB1_22
# BB#15:
	movl	$.L.str.158, %edi
	jmp	.LBB1_21
.LBB1_16:
	xorl	%ebp, %ebp
	movl	$.L.str.164, %edi
	jmp	.LBB1_18
.LBB1_17:
	xorl	%ebp, %ebp
	movl	$.L.str.165, %edi
.LBB1_18:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB1_7
.LBB1_19:
	movl	$.L.str.160, %edi
	jmp	.LBB1_21
.LBB1_20:
	movl	$.L.str.159, %edi
.LBB1_21:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	andw	$-256, %bp
	jne	.LBB1_23
	jmp	.LBB1_26
.LBB1_22:
	movl	$.L.str.161, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_warnmsg
	andw	$-256, %bp
	je	.LBB1_26
.LBB1_23:
	movzwl	%bp, %eax
	cmpl	$512, %eax              # imm = 0x200
	je	.LBB1_27
# BB#24:
	movb	$1, %cl
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	cmpl	$256, %eax              # imm = 0x100
	jne	.LBB1_29
# BB#25:
	movl	$.L.str.162, %edi
	jmp	.LBB1_28
.LBB1_26:
	movb	$1, %al
	movl	%eax, 16(%rsp)          # 4-byte Spill
	jmp	.LBB1_30
.LBB1_27:
	movb	$1, %al
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	$.L.str.163, %edi
.LBB1_28:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB1_30
.LBB1_29:
	movl	4(%r15), %esi
	movl	$.L.str.161, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB1_30:
	movl	$.L.str.157, %r13d
.LBB1_31:
	movl	8(%r15), %ebp
	cmpl	$513, %ebp              # imm = 0x201
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	movq	%r13, 56(%rsp)          # 8-byte Spill
	jb	.LBB1_33
# BB#32:
	movq	%r15, 72(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	movl	$.L.str.167, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	jne	.LBB1_47
	jmp	.LBB1_49
.LBB1_33:
	movl	12(%r15), %r13d
	cmpl	%r12d, %r13d
	jae	.LBB1_43
# BB#34:
	leal	(%r13,%rbp), %eax
	cmpl	%r12d, %eax
	jae	.LBB1_43
# BB#35:
	movq	%r15, 72(%rsp)          # 8-byte Spill
	testl	%ebp, %ebp
	je	.LBB1_44
# BB#36:
	movl	%ebp, %eax
	andl	$1, %eax
	jne	.LBB1_44
# BB#37:
	movl	%ebp, %edi
	shrl	%edi
	incl	%edi
	movl	$1, %esi
	callq	cli_calloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB1_45
# BB#38:                                # %.lr.ph.i.preheader
	addq	64(%rsp), %r13          # 8-byte Folded Reload
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_39:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rcx), %edx
	movzbl	(%r13,%rdx), %ebx
	shlb	$4, %bl
	movl	%eax, %esi
	movb	%bl, (%r15,%rsi)
	movl	%ecx, %edx
	addb	(%r13,%rdx), %bl
	cmpb	$37, %bl
	movb	$95, %dl
	je	.LBB1_41
# BB#40:                                # %.lr.ph.i
                                        #   in Loop: Header=BB1_39 Depth=1
	movl	%ebx, %edx
.LBB1_41:                               # %.lr.ph.i
                                        #   in Loop: Header=BB1_39 Depth=1
	movb	%dl, (%r15,%rsi)
	addl	$2, %ecx
	incl	%eax
	cmpl	%ebp, %ecx
	jb	.LBB1_39
# BB#42:                                # %sis_utf16_decode.exit
	movl	$.L.str.169, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	jmp	.LBB1_46
.LBB1_43:
	movl	$.L.str.168, %edi
	jmp	.LBB1_5
.LBB1_44:
	movl	$.L.str.193, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
.LBB1_45:
	xorl	%r15d, %r15d
	movl	$.L.str.170, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB1_46:
	movq	56(%rsp), %r13          # 8-byte Reload
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	je	.LBB1_49
.LBB1_47:
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	16(%rax), %r13d
	cmpl	$513, %r13d             # imm = 0x201
	jb	.LBB1_50
# BB#48:
	xorl	%ebp, %ebp
	movl	$.L.str.171, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB1_65
.LBB1_49:
	xorl	%ebp, %ebp
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB1_70
	jmp	.LBB1_66
.LBB1_50:
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	movl	20(%rax), %r14d
	cmpl	%r12d, %r14d
	jae	.LBB1_60
# BB#51:
	leal	(%r14,%r13), %eax
	cmpl	%r12d, %eax
	jae	.LBB1_60
# BB#52:
	testl	%r13d, %r13d
	je	.LBB1_62
# BB#53:
	movl	%r13d, %eax
	andl	$1, %eax
	jne	.LBB1_62
# BB#54:
	movl	%r13d, %edi
	shrl	%edi
	incl	%edi
	movl	$1, %esi
	callq	cli_calloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB1_63
# BB#55:                                # %.lr.ph.i12.preheader
	addq	64(%rsp), %r14          # 8-byte Folded Reload
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_56:                               # %.lr.ph.i12
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rcx), %edx
	movzbl	(%r14,%rdx), %ebx
	shlb	$4, %bl
	movl	%eax, %esi
	movb	%bl, (%rbp,%rsi)
	movl	%ecx, %edx
	addb	(%r14,%rdx), %bl
	cmpb	$37, %bl
	movb	$95, %dl
	je	.LBB1_58
# BB#57:                                # %.lr.ph.i12
                                        #   in Loop: Header=BB1_56 Depth=1
	movl	%ebx, %edx
.LBB1_58:                               # %.lr.ph.i12
                                        #   in Loop: Header=BB1_56 Depth=1
	movb	%dl, (%rbp,%rsi)
	addl	$2, %ecx
	incl	%eax
	cmpl	%r13d, %ecx
	jb	.LBB1_56
# BB#59:                                # %sis_utf16_decode.exit14
	movl	$.L.str.173, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	jmp	.LBB1_64
.LBB1_60:
	movl	$.L.str.172, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-124, %ebp
	testq	%r15, %r15
	je	.LBB1_7
# BB#61:
	movq	%r15, %rdi
	callq	free
	jmp	.LBB1_7
.LBB1_62:
	movl	$.L.str.193, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	cli_dbgmsg
.LBB1_63:
	xorl	%ebp, %ebp
	movl	$.L.str.174, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB1_64:
	movl	12(%rsp), %r14d         # 4-byte Reload
.LBB1_65:
	movq	56(%rsp), %r13          # 8-byte Reload
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB1_70
.LBB1_66:
	testq	%r15, %r15
	je	.LBB1_68
# BB#67:
	movq	%r15, %rdi
	callq	free
.LBB1_68:
	testq	%rbp, %rbp
	je	.LBB1_70
# BB#69:
	movq	%rbp, %rdi
	callq	free
.LBB1_70:
	movq	288(%rsp), %rbx
	testq	%r13, %r13
	je	.LBB1_73
# BB#71:
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%r13, %rdi
	callq	strlen
	leaq	2(%rbp,%rax), %rdi
	callq	cli_malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB1_116
# BB#72:
	movl	$.L.str.175, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	movq	%r13, %rcx
	callq	sprintf
	jmp	.LBB1_74
.LBB1_73:
	movq	%rbx, %rdi
	callq	cli_strdup
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB1_116
.LBB1_74:
	leaq	88(%rsp), %rdx
	movl	$1, %edi
	movq	%rbp, %rsi
	callq	__xstat
	cmpl	$-1, %eax
	jne	.LBB1_76
# BB#75:
	movl	$448, %esi              # imm = 0x1C0
	movq	%rbp, %rdi
	callq	mkdir
	cmpl	$-1, %eax
	je	.LBB1_117
.LBB1_76:                               # %.preheader
	movq	48(%rsp), %rdx          # 8-byte Reload
	testw	%dx, %dx
	movq	80(%rsp), %rbx          # 8-byte Reload
	je	.LBB1_114
# BB#77:                                # %.lr.ph
	leal	(,%rdx,8), %eax
	movl	%edx, %ecx
	shll	$2, %ecx
	movl	%eax, %eax
	movzwl	%dx, %edx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rsi          # 8-byte Reload
	addq	%rsi, %rax
	movq	64(%rsp), %rdx          # 8-byte Reload
	leaq	24(%rdx,%rax), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	addq	%rsi, %rcx
	leaq	24(%rdx,%rcx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_78:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r15,4), %r13d
	cmpl	%r12d, %r13d
	jne	.LBB1_80
# BB#79:                                #   in Loop: Header=BB1_78 Depth=1
	movl	$.L.str.176, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movb	$1, (%rbx)
	jmp	.LBB1_113
	.p2align	4, 0x90
.LBB1_80:                               #   in Loop: Header=BB1_78 Depth=1
	cmpl	%r12d, %r13d
	jbe	.LBB1_83
# BB#81:                                #   in Loop: Header=BB1_78 Depth=1
	cmpb	$0, (%rbx)
	je	.LBB1_118
# BB#82:                                #   in Loop: Header=BB1_78 Depth=1
	movl	$.L.str.178, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB1_113
	.p2align	4, 0x90
.LBB1_83:                               #   in Loop: Header=BB1_78 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	24(%rax,%r15,4), %r14d
	cmpl	%r12d, %r14d
	jae	.LBB1_119
# BB#84:                                #   in Loop: Header=BB1_78 Depth=1
	leal	(%r13,%r14), %eax
	cmpl	%r12d, %eax
	ja	.LBB1_119
# BB#85:                                #   in Loop: Header=BB1_78 Depth=1
	movq	%rbp, %rdi
	callq	cli_gentemp
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB1_121
# BB#86:                                #   in Loop: Header=BB1_78 Depth=1
	cmpb	$0, 12(%rsp)            # 1-byte Folded Reload
	je	.LBB1_101
# BB#87:                                #   in Loop: Header=BB1_78 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r15,4), %eax
	testq	%rax, %rax
	movq	%rax, 32(%rsp)
	je	.LBB1_102
# BB#88:                                #   in Loop: Header=BB1_78 Depth=1
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$.L.str.181, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	cli_dbgmsg
	movl	32(%rsp), %esi
	movl	$.L.str.182, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	296(%rsp), %rax
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.LBB1_92
# BB#89:                                #   in Loop: Header=BB1_78 Depth=1
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB1_92
# BB#90:                                #   in Loop: Header=BB1_78 Depth=1
	movq	32(%rsp), %rsi
	cmpq	%rdx, %rsi
	jbe	.LBB1_92
# BB#91:                                #   in Loop: Header=BB1_78 Depth=1
	movl	$.L.str.183, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movq	296(%rsp), %rax
	testb	$1, 41(%rax)
	jne	.LBB1_132
.LBB1_92:                               #   in Loop: Header=BB1_78 Depth=1
	movq	32(%rsp), %rdi
	leaq	(%r14,%r14,2), %rax
	cmpq	%rax, %rdi
	jbe	.LBB1_96
# BB#93:                                #   in Loop: Header=BB1_78 Depth=1
	movq	296(%rsp), %rcx
	movq	32(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB1_97
# BB#94:                                #   in Loop: Header=BB1_78 Depth=1
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB1_97
# BB#95:                                #   in Loop: Header=BB1_78 Depth=1
	cmpq	%rcx, %rdi
	jbe	.LBB1_97
.LBB1_96:                               #   in Loop: Header=BB1_78 Depth=1
	movq	%rax, 32(%rsp)
	movq	%rax, %rdi
.LBB1_97:                               #   in Loop: Header=BB1_78 Depth=1
	callq	cli_malloc
	testq	%rax, %rax
	je	.LBB1_128
# BB#98:                                #   in Loop: Header=BB1_78 Depth=1
	addq	64(%rsp), %r13          # 8-byte Folded Reload
	movq	%rax, %rbp
	movq	%rax, %rdi
	leaq	32(%rsp), %rsi
	movq	%r13, %rdx
	movq	%r14, %rcx
	callq	uncompress
	testl	%eax, %eax
	jne	.LBB1_129
# BB#99:                                #   in Loop: Header=BB1_78 Depth=1
	movq	32(%rsp), %rsi
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, %rsi
	jne	.LBB1_103
# BB#100:                               #   in Loop: Header=BB1_78 Depth=1
	movl	%eax, %r14d
	jmp	.LBB1_104
.LBB1_101:                              #   in Loop: Header=BB1_78 Depth=1
	addq	64(%rsp), %r13          # 8-byte Folded Reload
	jmp	.LBB1_105
.LBB1_102:                              #   in Loop: Header=BB1_78 Depth=1
	movl	$.L.str.180, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	callq	free
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB1_113
.LBB1_103:                              #   in Loop: Header=BB1_78 Depth=1
	movl	$.L.str.187, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movl	32(%rsp), %r14d
.LBB1_104:                              #   in Loop: Header=BB1_78 Depth=1
	movq	%rbp, %r13
.LBB1_105:                              #   in Loop: Header=BB1_78 Depth=1
	movl	$577, %esi              # imm = 0x241
	movl	$384, %edx              # imm = 0x180
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	open
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	je	.LBB1_122
# BB#106:                               #   in Loop: Header=BB1_78 Depth=1
	movl	%ebp, %edi
	movq	%r13, %rsi
	movl	%r14d, %edx
	callq	cli_writen
	cmpl	%r14d, %eax
	jne	.LBB1_123
# BB#107:                               #   in Loop: Header=BB1_78 Depth=1
	movl	12(%rsp), %r14d         # 4-byte Reload
	testb	%r14b, %r14b
	je	.LBB1_109
# BB#108:                               #   in Loop: Header=BB1_78 Depth=1
	movl	$.L.str.190, %edi
	jmp	.LBB1_110
.LBB1_109:                              #   in Loop: Header=BB1_78 Depth=1
	movl	$.L.str.191, %edi
.LBB1_110:                              #   in Loop: Header=BB1_78 Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	movl	%ebp, %edi
	callq	close
	cmpl	$-1, %eax
	je	.LBB1_126
# BB#111:                               #   in Loop: Header=BB1_78 Depth=1
	movq	%rbx, %rdi
	callq	free
	testb	%r14b, %r14b
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB1_113
# BB#112:                               #   in Loop: Header=BB1_78 Depth=1
	movq	%r13, %rdi
	callq	free
	.p2align	4, 0x90
.LBB1_113:                              #   in Loop: Header=BB1_78 Depth=1
	incq	%r15
	cmpq	56(%rsp), %r15          # 8-byte Folded Reload
	jl	.LBB1_78
.LBB1_114:                              # %._crit_edge
	movq	%rbp, %rdi
	callq	free
	xorl	%ebp, %ebp
	jmp	.LBB1_7
.LBB1_116:
	movl	$-114, %ebp
	jmp	.LBB1_7
.LBB1_117:
	movq	%rbp, %rdi
	jmp	.LBB1_131
.LBB1_118:
	movl	$.L.str.177, %edi
	jmp	.LBB1_120
.LBB1_119:
	movl	$.L.str.179, %edi
.LBB1_120:
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	%rbp, %rdi
	callq	free
	jmp	.LBB1_6
.LBB1_121:
	movq	%rbp, %rdi
	callq	free
	movl	$-114, %ebp
	jmp	.LBB1_7
.LBB1_122:
	movl	$.L.str.188, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_errmsg
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%rbx, %rdi
	callq	free
	movl	$-123, %ebp
	cmpb	$0, 12(%rsp)            # 1-byte Folded Reload
	jne	.LBB1_127
	jmp	.LBB1_7
.LBB1_123:
	movl	$.L.str.189, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	movq	%rbx, %rdx
	callq	cli_errmsg
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%rbx, %rdi
	callq	free
	cmpb	$0, 12(%rsp)            # 1-byte Folded Reload
	je	.LBB1_125
# BB#124:
	movq	%r13, %rdi
	callq	free
.LBB1_125:
	movl	%ebp, %edi
	callq	close
	movl	$-123, %ebp
	jmp	.LBB1_7
.LBB1_126:
	movl	$.L.str.192, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_errmsg
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%rbx, %rdi
	callq	free
	movl	$-123, %ebp
	testb	%r14b, %r14b
	je	.LBB1_7
.LBB1_127:
	movq	%r13, %rdi
	callq	free
	jmp	.LBB1_7
.LBB1_128:
	movl	$.L.str.185, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB1_130
.LBB1_129:
	movl	$.L.str.186, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rbp, %rdi
	callq	free
.LBB1_130:
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%rbx, %rdi
.LBB1_131:
	callq	free
	movl	$-123, %ebp
	jmp	.LBB1_7
.LBB1_132:
	movq	296(%rsp), %rax
	movq	(%rax), %rax
	movq	$.L.str.184, (%rax)
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%rbx, %rdi
	callq	free
	movl	$1, %ebp
	jmp	.LBB1_7
.Lfunc_end1:
	.size	sis_extract_simple, .Lfunc_end1-sis_extract_simple
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_3
	.quad	.LBB1_10
	.quad	.LBB1_11
	.quad	.LBB1_12
	.quad	.LBB1_16
	.quad	.LBB1_17

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"SIS: fstat() failed\n"
	.size	.L.str, 21

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"SIS: Broken or not a SIS file (too small)\n"
	.size	.L.str.1, 43

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"SIS: mmap() failed\n"
	.size	.L.str.2, 20

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"SIS: mmap'ed file\n"
	.size	.L.str.3, 19

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"SIS: File too large (> %d)\n"
	.size	.L.str.4, 28

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"SIS: Not a SIS file\n"
	.size	.L.str.5, 21

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"SIS: EPOC release 3, 4 or 5\n"
	.size	.L.str.6, 29

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"SIS: EPOC release 6\n"
	.size	.L.str.7, 21

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"SIS: Application(?)\n"
	.size	.L.str.8, 21

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"SIS: Unknown value of UID 2 (EPOC release == 0x%x) -> not a real SIS file??\n"
	.size	.L.str.9, 77

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"SIS: Number of languages: %d\n"
	.size	.L.str.10, 30

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"SIS: Broken file structure (language records)\n"
	.size	.L.str.11, 47

	.type	langcodes,@object       # @langcodes
	.section	.rodata,"a",@progbits
	.p2align	4
langcodes:
	.quad	.L.str.41
	.quad	.L.str.55
	.quad	.L.str.56
	.quad	.L.str.57
	.quad	.L.str.58
	.quad	.L.str.59
	.quad	.L.str.60
	.quad	.L.str.61
	.quad	.L.str.62
	.quad	.L.str.63
	.quad	.L.str.64
	.quad	.L.str.65
	.quad	.L.str.66
	.quad	.L.str.67
	.quad	.L.str.68
	.quad	.L.str.69
	.quad	.L.str.70
	.quad	.L.str.71
	.quad	.L.str.72
	.quad	.L.str.73
	.quad	.L.str.74
	.quad	.L.str.75
	.quad	.L.str.76
	.quad	.L.str.77
	.quad	.L.str.78
	.quad	.L.str.79
	.quad	.L.str.80
	.quad	.L.str.81
	.quad	.L.str.82
	.quad	.L.str.83
	.quad	.L.str.84
	.quad	.L.str.85
	.quad	.L.str.86
	.quad	.L.str.87
	.quad	.L.str.88
	.quad	.L.str.89
	.quad	.L.str.90
	.quad	.L.str.91
	.quad	.L.str.92
	.quad	.L.str.93
	.quad	.L.str.94
	.quad	.L.str.95
	.quad	.L.str.75
	.quad	.L.str.96
	.quad	.L.str.97
	.quad	.L.str.98
	.quad	.L.str.99
	.quad	.L.str.100
	.quad	.L.str.65
	.quad	.L.str.101
	.quad	.L.str.102
	.quad	.L.str.103
	.quad	.L.str.104
	.quad	.L.str.105
	.quad	.L.str.106
	.quad	.L.str.107
	.quad	.L.str.108
	.quad	.L.str.109
	.quad	.L.str.110
	.quad	.L.str.111
	.quad	.L.str.112
	.quad	.L.str.113
	.quad	.L.str.114
	.quad	.L.str.115
	.quad	.L.str.116
	.quad	.L.str.117
	.quad	.L.str.118
	.quad	.L.str.119
	.quad	.L.str.120
	.quad	.L.str.121
	.quad	.L.str.122
	.quad	.L.str.123
	.quad	.L.str.124
	.quad	.L.str.125
	.quad	.L.str.126
	.quad	.L.str.127
	.quad	.L.str.128
	.quad	.L.str.129
	.quad	.L.str.130
	.quad	.L.str.131
	.quad	.L.str.132
	.quad	.L.str.133
	.quad	.L.str.134
	.quad	.L.str.135
	.quad	.L.str.136
	.quad	.L.str.137
	.quad	.L.str.138
	.quad	.L.str.139
	.quad	.L.str.140
	.quad	.L.str.141
	.quad	.L.str.142
	.quad	.L.str.143
	.quad	.L.str.144
	.quad	.L.str.145
	.quad	.L.str.41
	.quad	.L.str.146
	.quad	.L.str.147
	.quad	.L.str.148
	.size	langcodes, 784

	.type	.L.str.13,@object       # @.str.13
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.13:
	.asciz	"SIS: Supported languages: %s\n"
	.size	.L.str.13, 30

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"SIS: Incorrect number of languages (%d)\n"
	.size	.L.str.14, 41

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"SIS: Offset of languages records: %d\n"
	.size	.L.str.15, 38

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"SIS: Installation language: %d\n"
	.size	.L.str.16, 32

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"SIS: Number of requisites: %d\n"
	.size	.L.str.17, 31

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"SIS: Offset of requisites records: %d\n"
	.size	.L.str.18, 39

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"SIS: Options:\n"
	.size	.L.str.19, 15

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"SIS:    * File is in Unicode format\n"
	.size	.L.str.20, 37

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"SIS:    * File is distributable\n"
	.size	.L.str.21, 33

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"SIS:    * Archived files are not compressed\n"
	.size	.L.str.22, 45

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"SIS:    * Archived files are compressed\n"
	.size	.L.str.23, 41

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"SIS:    * File installation shuts down all applications\n"
	.size	.L.str.24, 57

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"SIS: Type: Contains an application\n"
	.size	.L.str.25, 36

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"SIS: Type: Contains a shared/system component\n"
	.size	.L.str.26, 47

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"SIS: Type: Contains an optional (selectable) component\n"
	.size	.L.str.27, 56

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"SIS: Type: Configures an existing application or service\n"
	.size	.L.str.28, 58

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"SIS: Type: Patches an existing component\n"
	.size	.L.str.29, 42

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"SIS: Type: Upgrades an existing component\n"
	.size	.L.str.30, 43

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"SIS: Unknown value of type\n"
	.size	.L.str.31, 28

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"SIS: Major version: %d\n"
	.size	.L.str.32, 24

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"SIS: Minor version: %d\n"
	.size	.L.str.33, 24

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"SIS: Maximum space required: %d\n"
	.size	.L.str.34, 33

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"SIS: Files limit reached (max: %d)\n"
	.size	.L.str.35, 36

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"SIS.ExceededFilesLimit"
	.size	.L.str.36, 23

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"SIS: Number of files: %d\n"
	.size	.L.str.37, 26

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"SIS: Offset of files records: %d\n"
	.size	.L.str.38, 34

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"SIS: Broken file structure (frecord)\n"
	.size	.L.str.39, 38

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"SIS: Can't create temporary directory %s\n"
	.size	.L.str.40, 42

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.zero	1
	.size	.L.str.41, 1

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"SIS: -----\n"
	.size	.L.str.42, 12

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"SIS: Simple file record\n"
	.size	.L.str.43, 25

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"SIS: Multiple languages file record\n"
	.size	.L.str.44, 37

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"SIS: Options record\n"
	.size	.L.str.45, 21

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"SIS: Number of options: %d\n"
	.size	.L.str.46, 28

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"SIS: Incorrect number of options\n"
	.size	.L.str.47, 34

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"SIS: If/ElseIf record\n"
	.size	.L.str.48, 23

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"SIS: Size of conditional expression: %d\n"
	.size	.L.str.49, 41

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"SIS: Incorrect size of conditional expression\n"
	.size	.L.str.50, 47

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"SIS: Else record\n"
	.size	.L.str.51, 18

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"SIS: EndIf record\n"
	.size	.L.str.52, 19

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"SIS: Unknown file record type\n"
	.size	.L.str.53, 31

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"SIS:  ****** Scanning extracted files ******\n"
	.size	.L.str.54, 46

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"EN"
	.size	.L.str.55, 3

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"FR"
	.size	.L.str.56, 3

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"GE"
	.size	.L.str.57, 3

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"SP"
	.size	.L.str.58, 3

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"IT"
	.size	.L.str.59, 3

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"SW"
	.size	.L.str.60, 3

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"DA"
	.size	.L.str.61, 3

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"NO"
	.size	.L.str.62, 3

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"FI"
	.size	.L.str.63, 3

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"AM"
	.size	.L.str.64, 3

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"SF"
	.size	.L.str.65, 3

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"SG"
	.size	.L.str.66, 3

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"PO"
	.size	.L.str.67, 3

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"TU"
	.size	.L.str.68, 3

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"IC"
	.size	.L.str.69, 3

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"RU"
	.size	.L.str.70, 3

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"HU"
	.size	.L.str.71, 3

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"DU"
	.size	.L.str.72, 3

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"BL"
	.size	.L.str.73, 3

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"AU"
	.size	.L.str.74, 3

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"BG"
	.size	.L.str.75, 3

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"AS"
	.size	.L.str.76, 3

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"NZ"
	.size	.L.str.77, 3

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"IF"
	.size	.L.str.78, 3

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"CS"
	.size	.L.str.79, 3

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"SK"
	.size	.L.str.80, 3

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"PL"
	.size	.L.str.81, 3

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"SL"
	.size	.L.str.82, 3

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"TC"
	.size	.L.str.83, 3

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"HK"
	.size	.L.str.84, 3

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"ZH"
	.size	.L.str.85, 3

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"JA"
	.size	.L.str.86, 3

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"TH"
	.size	.L.str.87, 3

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"AF"
	.size	.L.str.88, 3

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"SQ"
	.size	.L.str.89, 3

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"AH"
	.size	.L.str.90, 3

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"AR"
	.size	.L.str.91, 3

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"HY"
	.size	.L.str.92, 3

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"TL"
	.size	.L.str.93, 3

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"BE"
	.size	.L.str.94, 3

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"BN"
	.size	.L.str.95, 3

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"MY"
	.size	.L.str.96, 3

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"CA"
	.size	.L.str.97, 3

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"HR"
	.size	.L.str.98, 3

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"CE"
	.size	.L.str.99, 3

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"IE"
	.size	.L.str.100, 3

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"ET"
	.size	.L.str.101, 3

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"FA"
	.size	.L.str.102, 3

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"CF"
	.size	.L.str.103, 3

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"GD"
	.size	.L.str.104, 3

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"KA"
	.size	.L.str.105, 3

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"EL"
	.size	.L.str.106, 3

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"CG"
	.size	.L.str.107, 3

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"GU"
	.size	.L.str.108, 3

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"HE"
	.size	.L.str.109, 3

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"HI"
	.size	.L.str.110, 3

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"IN"
	.size	.L.str.111, 3

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"GA"
	.size	.L.str.112, 3

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"SZ"
	.size	.L.str.113, 3

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"KN"
	.size	.L.str.114, 3

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"KK"
	.size	.L.str.115, 3

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"KM"
	.size	.L.str.116, 3

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"KO"
	.size	.L.str.117, 3

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"LO"
	.size	.L.str.118, 3

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"LV"
	.size	.L.str.119, 3

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"LT"
	.size	.L.str.120, 3

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"MK"
	.size	.L.str.121, 3

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"MS"
	.size	.L.str.122, 3

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"ML"
	.size	.L.str.123, 3

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"MR"
	.size	.L.str.124, 3

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"MO"
	.size	.L.str.125, 3

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"MN"
	.size	.L.str.126, 3

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"NN"
	.size	.L.str.127, 3

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"BP"
	.size	.L.str.128, 3

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"PA"
	.size	.L.str.129, 3

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"RO"
	.size	.L.str.130, 3

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"SR"
	.size	.L.str.131, 3

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"SI"
	.size	.L.str.132, 3

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"SO"
	.size	.L.str.133, 3

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"OS"
	.size	.L.str.134, 3

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"LS"
	.size	.L.str.135, 3

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	"SH"
	.size	.L.str.136, 3

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"FS"
	.size	.L.str.137, 3

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"TA"
	.size	.L.str.138, 3

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"TE"
	.size	.L.str.139, 3

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"BO"
	.size	.L.str.140, 3

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"TI"
	.size	.L.str.141, 3

	.type	.L.str.142,@object      # @.str.142
.L.str.142:
	.asciz	"CT"
	.size	.L.str.142, 3

	.type	.L.str.143,@object      # @.str.143
.L.str.143:
	.asciz	"TK"
	.size	.L.str.143, 3

	.type	.L.str.144,@object      # @.str.144
.L.str.144:
	.asciz	"UK"
	.size	.L.str.144, 3

	.type	.L.str.145,@object      # @.str.145
.L.str.145:
	.asciz	"UR"
	.size	.L.str.145, 3

	.type	.L.str.146,@object      # @.str.146
.L.str.146:
	.asciz	"VI"
	.size	.L.str.146, 3

	.type	.L.str.147,@object      # @.str.147
.L.str.147:
	.asciz	"CY"
	.size	.L.str.147, 3

	.type	.L.str.148,@object      # @.str.148
.L.str.148:
	.asciz	"ZU"
	.size	.L.str.148, 3

	.type	.L.str.149,@object      # @.str.149
.L.str.149:
	.asciz	"SIS: sis_extract_simple: Broken file record\n"
	.size	.L.str.149, 45

	.type	.L.str.150,@object      # @.str.150
.L.str.150:
	.asciz	"SIS: File type: Standard file\n"
	.size	.L.str.150, 31

	.type	.L.str.151,@object      # @.str.151
.L.str.151:
	.asciz	"standard"
	.size	.L.str.151, 9

	.type	.L.str.152,@object      # @.str.152
.L.str.152:
	.asciz	"SIS: File type: Text file\n"
	.size	.L.str.152, 27

	.type	.L.str.153,@object      # @.str.153
.L.str.153:
	.asciz	"text"
	.size	.L.str.153, 5

	.type	.L.str.154,@object      # @.str.154
.L.str.154:
	.asciz	"SIS: File type: Component file\n"
	.size	.L.str.154, 32

	.type	.L.str.155,@object      # @.str.155
.L.str.155:
	.asciz	"component"
	.size	.L.str.155, 10

	.type	.L.str.156,@object      # @.str.156
.L.str.156:
	.asciz	"SIS: File type: Run file\n"
	.size	.L.str.156, 26

	.type	.L.str.157,@object      # @.str.157
.L.str.157:
	.asciz	"run"
	.size	.L.str.157, 4

	.type	.L.str.158,@object      # @.str.158
.L.str.158:
	.asciz	"SIS:    * During installation only\n"
	.size	.L.str.158, 36

	.type	.L.str.159,@object      # @.str.159
.L.str.159:
	.asciz	"SIS:    * During removal only\n"
	.size	.L.str.159, 31

	.type	.L.str.160,@object      # @.str.160
.L.str.160:
	.asciz	"SIS:    * During installation and removal\n"
	.size	.L.str.160, 43

	.type	.L.str.161,@object      # @.str.161
.L.str.161:
	.asciz	"SIS: sis_extract_simple: Unknown value in file details (0x%x)\n"
	.size	.L.str.161, 63

	.type	.L.str.162,@object      # @.str.162
.L.str.162:
	.asciz	"SIS:    * Ends when installation finished\n"
	.size	.L.str.162, 43

	.type	.L.str.163,@object      # @.str.163
.L.str.163:
	.asciz	"SIS:    * Waits until closed before continuing\n"
	.size	.L.str.163, 48

	.type	.L.str.164,@object      # @.str.164
.L.str.164:
	.asciz	"SIS: File type: Null file\n"
	.size	.L.str.164, 27

	.type	.L.str.165,@object      # @.str.165
.L.str.165:
	.asciz	"SIS: File type: MIME file\n"
	.size	.L.str.165, 27

	.type	.L.str.166,@object      # @.str.166
.L.str.166:
	.asciz	"SIS: Unknown file type in file record\n"
	.size	.L.str.166, 39

	.type	.L.str.167,@object      # @.str.167
.L.str.167:
	.asciz	"SIS: sis_extract_simple: Source name too long and will not be decoded\n"
	.size	.L.str.167, 71

	.type	.L.str.168,@object      # @.str.168
.L.str.168:
	.asciz	"SIS: sis_extract_simple: Broken source name data\n"
	.size	.L.str.168, 50

	.type	.L.str.169,@object      # @.str.169
.L.str.169:
	.asciz	"SIS: Source name: %s\n"
	.size	.L.str.169, 22

	.type	.L.str.170,@object      # @.str.170
.L.str.170:
	.asciz	"SIS: Source name not decoded\n"
	.size	.L.str.170, 30

	.type	.L.str.171,@object      # @.str.171
.L.str.171:
	.asciz	"SIS: sis_extract_simple: Destination name too long and will not be decoded\n"
	.size	.L.str.171, 76

	.type	.L.str.172,@object      # @.str.172
.L.str.172:
	.asciz	"SIS: sis_extract_simple: Broken destination name data\n"
	.size	.L.str.172, 55

	.type	.L.str.173,@object      # @.str.173
.L.str.173:
	.asciz	"SIS: Destination name: %s\n"
	.size	.L.str.173, 27

	.type	.L.str.174,@object      # @.str.174
.L.str.174:
	.asciz	"SIS: Destination name not decoded\n"
	.size	.L.str.174, 35

	.type	.L.str.175,@object      # @.str.175
.L.str.175:
	.asciz	"%s/%s"
	.size	.L.str.175, 6

	.type	.L.str.176,@object      # @.str.176
.L.str.176:
	.asciz	"SIS: Null file (installation record)\n"
	.size	.L.str.176, 38

	.type	.L.str.177,@object      # @.str.177
.L.str.177:
	.asciz	"SIS: sis_extract_simple: Broken file data (fileoff)\n"
	.size	.L.str.177, 53

	.type	.L.str.178,@object      # @.str.178
.L.str.178:
	.asciz	"SIS: Null file (installation track)\n"
	.size	.L.str.178, 37

	.type	.L.str.179,@object      # @.str.179
.L.str.179:
	.asciz	"SIS: sis_extract_simple: Broken file data (filelen, fileoff)\n"
	.size	.L.str.179, 62

	.type	.L.str.180,@object      # @.str.180
.L.str.180:
	.asciz	"SIS: Empty file, skipping\n"
	.size	.L.str.180, 27

	.type	.L.str.181,@object      # @.str.181
.L.str.181:
	.asciz	"SIS: Compressed size: %u\n"
	.size	.L.str.181, 26

	.type	.L.str.182,@object      # @.str.182
.L.str.182:
	.asciz	"SIS: Original size: %u\n"
	.size	.L.str.182, 24

	.type	.L.str.183,@object      # @.str.183
.L.str.183:
	.asciz	"SIS: Size exceeded (%u, max: %lu)\n"
	.size	.L.str.183, 35

	.type	.L.str.184,@object      # @.str.184
.L.str.184:
	.asciz	"SIS.ExceededFileSize"
	.size	.L.str.184, 21

	.type	.L.str.185,@object      # @.str.185
.L.str.185:
	.asciz	"SIS: sis_extract_simple: Can't allocate decompression buffer\n"
	.size	.L.str.185, 62

	.type	.L.str.186,@object      # @.str.186
.L.str.186:
	.asciz	"SIS: sis_extract_simple: File decompression failed\n"
	.size	.L.str.186, 52

	.type	.L.str.187,@object      # @.str.187
.L.str.187:
	.asciz	"SIS: WARNING: Real original size: %u\n"
	.size	.L.str.187, 38

	.type	.L.str.188,@object      # @.str.188
.L.str.188:
	.asciz	"SIS: sis_extract_simple: Can't create new file %s\n"
	.size	.L.str.188, 51

	.type	.L.str.189,@object      # @.str.189
.L.str.189:
	.asciz	"SIS: sis_extract_simple: Can't write %d bytes to %s\n"
	.size	.L.str.189, 53

	.type	.L.str.190,@object      # @.str.190
.L.str.190:
	.asciz	"SIS: File decompressed into %s\n"
	.size	.L.str.190, 32

	.type	.L.str.191,@object      # @.str.191
.L.str.191:
	.asciz	"SIS: File saved into %s\n"
	.size	.L.str.191, 25

	.type	.L.str.192,@object      # @.str.192
.L.str.192:
	.asciz	"SIS: sis_extract_simple: Can't close descriptor %d\n"
	.size	.L.str.192, 52

	.type	.L.str.193,@object      # @.str.193
.L.str.193:
	.asciz	"SIS: sis_utf16_decode: Broken filename (length == %d)\n"
	.size	.L.str.193, 55


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
