	.text
	.file	"covariance.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4652007308841189376     # double 1000
.LCPI6_1:
	.quad	4608083138725491507     # double 1.2
.LCPI6_3:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_2:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 80
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8000000, %edx          # imm = 0x7A1200
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_66
# BB#1:
	movq	8(%rsp), %r14
	testq	%r14, %r14
	je	.LBB6_66
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8000000, %edx          # imm = 0x7A1200
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_66
# BB#3:                                 # %polybench_alloc_data.exit
	movq	8(%rsp), %r15
	testq	%r15, %r15
	je	.LBB6_66
# BB#4:                                 # %polybench_alloc_data.exit37
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8000000, %edx          # imm = 0x7A1200
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_66
# BB#5:                                 # %polybench_alloc_data.exit37
	movq	8(%rsp), %r12
	testq	%r12, %r12
	je	.LBB6_66
# BB#6:                                 # %polybench_alloc_data.exit39
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8000, %edx             # imm = 0x1F40
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_66
# BB#7:                                 # %polybench_alloc_data.exit39
	movq	8(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB6_66
# BB#8:                                 # %polybench_alloc_data.exit41
	leaq	8(%r14), %rax
	xorl	%ecx, %ecx
	movsd	.LCPI6_0(%rip), %xmm1   # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB6_9:                                # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_10 Depth 2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movq	%rax, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_10:                               #   Parent Loop BB6_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%esi, %xmm2
	mulsd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, -8(%rdx)
	movl	%esi, %edi
	orl	$1, %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, (%rdx)
	addq	$2, %rsi
	addq	$16, %rdx
	cmpq	$1000, %rsi             # imm = 0x3E8
	jne	.LBB6_10
# BB#11:                                #   in Loop: Header=BB6_9 Depth=1
	incq	%rcx
	addq	$8000, %rax             # imm = 0x1F40
	cmpq	$1000, %rcx             # imm = 0x3E8
	jne	.LBB6_9
# BB#12:                                # %init_array.exit
	xorl	%eax, %eax
	movsd	.LCPI6_1(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB6_13:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_14 Depth 2
	movq	$0, (%rbx,%rax,8)
	xorpd	%xmm2, %xmm2
	movq	%rcx, %rdx
	movl	$1000, %esi             # imm = 0x3E8
	.p2align	4, 0x90
.LBB6_14:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rdx), %xmm2
	movsd	%xmm2, (%rbx,%rax,8)
	addsd	8000(%rdx), %xmm2
	movsd	%xmm2, (%rbx,%rax,8)
	addsd	16000(%rdx), %xmm2
	movsd	%xmm2, (%rbx,%rax,8)
	addsd	24000(%rdx), %xmm2
	movsd	%xmm2, (%rbx,%rax,8)
	addsd	32000(%rdx), %xmm2
	movsd	%xmm2, (%rbx,%rax,8)
	addq	$40000, %rdx            # imm = 0x9C40
	addq	$-5, %rsi
	jne	.LBB6_14
# BB#15:                                #   in Loop: Header=BB6_13 Depth=1
	divsd	%xmm0, %xmm2
	movsd	%xmm2, (%rbx,%rax,8)
	incq	%rax
	addq	$8, %rcx
	cmpq	$1000, %rax             # imm = 0x3E8
	jne	.LBB6_13
# BB#16:                                # %.preheader2.i.preheader
	leaq	8000(%rbx), %r8
	leaq	48(%r14), %rcx
	leaq	24(%r14), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_17:                               # %.preheader2.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_20 Depth 2
                                        #     Child Loop BB6_22 Depth 2
	imulq	$8000, %rsi, %rax       # imm = 0x1F40
	leaq	(%r14,%rax), %rdi
	cmpq	%r8, %rdi
	jae	.LBB6_19
# BB#18:                                # %.preheader2.i
                                        #   in Loop: Header=BB6_17 Depth=1
	leaq	8000(%r14,%rax), %rax
	cmpq	%rax, %rbx
	jae	.LBB6_19
# BB#21:                                # %scalar.ph.preheader
                                        #   in Loop: Header=BB6_17 Depth=1
	movq	%rdx, %rax
	movl	$3, %edi
	.p2align	4, 0x90
.LBB6_22:                               # %scalar.ph
                                        #   Parent Loop BB6_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-24(%rax), %xmm2        # xmm2 = mem[0],zero
	subsd	-24(%rbx,%rdi,8), %xmm2
	movsd	%xmm2, -24(%rax)
	movsd	-16(%rax), %xmm2        # xmm2 = mem[0],zero
	subsd	-16(%rbx,%rdi,8), %xmm2
	movsd	%xmm2, -16(%rax)
	movsd	-8(%rax), %xmm2         # xmm2 = mem[0],zero
	subsd	-8(%rbx,%rdi,8), %xmm2
	movsd	%xmm2, -8(%rax)
	movsd	(%rax), %xmm2           # xmm2 = mem[0],zero
	subsd	(%rbx,%rdi,8), %xmm2
	movsd	%xmm2, (%rax)
	addq	$4, %rdi
	addq	$32, %rax
	cmpq	$1003, %rdi             # imm = 0x3EB
	jne	.LBB6_22
	jmp	.LBB6_23
	.p2align	4, 0x90
.LBB6_19:                               # %vector.body.preheader
                                        #   in Loop: Header=BB6_17 Depth=1
	movq	%rcx, %rdi
	movl	$6, %ebp
	.p2align	4, 0x90
.LBB6_20:                               # %vector.body
                                        #   Parent Loop BB6_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-48(%rbx,%rbp,8), %xmm2
	movupd	-32(%rbx,%rbp,8), %xmm3
	movupd	-48(%rdi), %xmm4
	movupd	-32(%rdi), %xmm5
	subpd	%xmm2, %xmm4
	subpd	%xmm3, %xmm5
	movupd	%xmm4, -48(%rdi)
	movupd	%xmm5, -32(%rdi)
	movupd	-16(%rbx,%rbp,8), %xmm2
	movupd	(%rbx,%rbp,8), %xmm3
	movupd	-16(%rdi), %xmm4
	movupd	(%rdi), %xmm5
	subpd	%xmm2, %xmm4
	subpd	%xmm3, %xmm5
	movupd	%xmm4, -16(%rdi)
	movupd	%xmm5, (%rdi)
	addq	$8, %rbp
	addq	$64, %rdi
	cmpq	$1006, %rbp             # imm = 0x3EE
	jne	.LBB6_20
.LBB6_23:                               # %middle.block
                                        #   in Loop: Header=BB6_17 Depth=1
	incq	%rsi
	addq	$8000, %rcx             # imm = 0x1F40
	addq	$8000, %rdx             # imm = 0x1F40
	cmpq	$1000, %rsi             # imm = 0x3E8
	jne	.LBB6_17
# BB#24:                                # %.lr.ph.i.preheader
	xorl	%r10d, %r10d
	movq	%r14, %r9
	.p2align	4, 0x90
.LBB6_25:                               # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_26 Depth 2
                                        #       Child Loop BB6_27 Depth 3
	movq	%r9, %rdx
	movq	%r10, %rdi
	.p2align	4, 0x90
.LBB6_26:                               #   Parent Loop BB6_25 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_27 Depth 3
	imulq	$8000, %r10, %rax       # imm = 0x1F40
	addq	%r15, %rax
	leaq	(%rax,%rdi,8), %rbp
	movq	$0, (%rax,%rdi,8)
	xorpd	%xmm2, %xmm2
	movl	$1000, %eax             # imm = 0x3E8
	movq	%rdx, %rsi
	movq	%r9, %rcx
	.p2align	4, 0x90
.LBB6_27:                               #   Parent Loop BB6_25 Depth=1
                                        #     Parent Loop BB6_26 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rcx), %xmm3           # xmm3 = mem[0],zero
	mulsd	(%rsi), %xmm3
	addsd	%xmm2, %xmm3
	movsd	%xmm3, (%rbp)
	movsd	8000(%rcx), %xmm2       # xmm2 = mem[0],zero
	mulsd	8000(%rsi), %xmm2
	addsd	%xmm3, %xmm2
	movsd	%xmm2, (%rbp)
	addq	$16000, %rcx            # imm = 0x3E80
	addq	$16000, %rsi            # imm = 0x3E80
	addq	$-2, %rax
	jne	.LBB6_27
# BB#28:                                #   in Loop: Header=BB6_26 Depth=2
	imulq	$8000, %rdi, %rax       # imm = 0x1F40
	addq	%r15, %rax
	movsd	%xmm2, (%rax,%r10,8)
	incq	%rdi
	addq	$8, %rdx
	cmpq	$1000, %rdi             # imm = 0x3E8
	jne	.LBB6_26
# BB#29:                                # %._crit_edge.i
                                        #   in Loop: Header=BB6_25 Depth=1
	incq	%r10
	addq	$8, %r9
	cmpq	$1000, %r10             # imm = 0x3E8
	jne	.LBB6_25
# BB#30:                                # %.preheader.i46.preheader
	leaq	8(%r14), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB6_31:                               # %.preheader.i46
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_32 Depth 2
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edx, %xmm2
	movq	%rcx, %rax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_32:                               #   Parent Loop BB6_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%esi, %xmm3
	mulsd	%xmm2, %xmm3
	divsd	%xmm1, %xmm3
	movsd	%xmm3, -8(%rax)
	movl	%esi, %edi
	orl	$1, %edi
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%edi, %xmm3
	mulsd	%xmm2, %xmm3
	divsd	%xmm1, %xmm3
	movsd	%xmm3, (%rax)
	addq	$2, %rsi
	addq	$16, %rax
	cmpq	$1000, %rsi             # imm = 0x3E8
	jne	.LBB6_32
# BB#33:                                #   in Loop: Header=BB6_31 Depth=1
	incq	%rdx
	addq	$8000, %rcx             # imm = 0x1F40
	cmpq	$1000, %rdx             # imm = 0x3E8
	jne	.LBB6_31
# BB#34:                                # %init_array.exit52.preheader
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB6_35:                               # %init_array.exit52
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_36 Depth 2
	movq	$0, (%rbx,%rcx,8)
	xorpd	%xmm1, %xmm1
	movq	%rdx, %rax
	movl	$1000, %esi             # imm = 0x3E8
	.p2align	4, 0x90
.LBB6_36:                               #   Parent Loop BB6_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rax), %xmm1
	movsd	%xmm1, (%rbx,%rcx,8)
	addsd	8000(%rax), %xmm1
	movsd	%xmm1, (%rbx,%rcx,8)
	addsd	16000(%rax), %xmm1
	movsd	%xmm1, (%rbx,%rcx,8)
	addsd	24000(%rax), %xmm1
	movsd	%xmm1, (%rbx,%rcx,8)
	addsd	32000(%rax), %xmm1
	movsd	%xmm1, (%rbx,%rcx,8)
	addq	$40000, %rax            # imm = 0x9C40
	addq	$-5, %rsi
	jne	.LBB6_36
# BB#37:                                #   in Loop: Header=BB6_35 Depth=1
	divsd	%xmm0, %xmm1
	movsd	%xmm1, (%rbx,%rcx,8)
	incq	%rcx
	addq	$8, %rdx
	cmpq	$1000, %rcx             # imm = 0x3E8
	jne	.LBB6_35
# BB#38:                                # %.preheader2.i60.preheader
	leaq	48(%r14), %rcx
	leaq	24(%r14), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_39:                               # %.preheader2.i60
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_42 Depth 2
                                        #     Child Loop BB6_44 Depth 2
	imulq	$8000, %rsi, %rax       # imm = 0x1F40
	leaq	(%r14,%rax), %rdi
	cmpq	%r8, %rdi
	jae	.LBB6_41
# BB#40:                                # %.preheader2.i60
                                        #   in Loop: Header=BB6_39 Depth=1
	leaq	8000(%r14,%rax), %rax
	cmpq	%rax, %rbx
	jae	.LBB6_41
# BB#43:                                # %scalar.ph134.preheader
                                        #   in Loop: Header=BB6_39 Depth=1
	movq	%rdx, %rax
	movl	$3, %edi
	.p2align	4, 0x90
.LBB6_44:                               # %scalar.ph134
                                        #   Parent Loop BB6_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-24(%rax), %xmm0        # xmm0 = mem[0],zero
	subsd	-24(%rbx,%rdi,8), %xmm0
	movsd	%xmm0, -24(%rax)
	movsd	-16(%rax), %xmm0        # xmm0 = mem[0],zero
	subsd	-16(%rbx,%rdi,8), %xmm0
	movsd	%xmm0, -16(%rax)
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	subsd	-8(%rbx,%rdi,8), %xmm0
	movsd	%xmm0, -8(%rax)
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	subsd	(%rbx,%rdi,8), %xmm0
	movsd	%xmm0, (%rax)
	addq	$4, %rdi
	addq	$32, %rax
	cmpq	$1003, %rdi             # imm = 0x3EB
	jne	.LBB6_44
	jmp	.LBB6_45
	.p2align	4, 0x90
.LBB6_41:                               # %vector.body132.preheader
                                        #   in Loop: Header=BB6_39 Depth=1
	movq	%rcx, %rdi
	movl	$6, %ebp
	.p2align	4, 0x90
.LBB6_42:                               # %vector.body132
                                        #   Parent Loop BB6_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-48(%rbx,%rbp,8), %xmm0
	movupd	-32(%rbx,%rbp,8), %xmm1
	movupd	-48(%rdi), %xmm2
	movupd	-32(%rdi), %xmm3
	subpd	%xmm0, %xmm2
	subpd	%xmm1, %xmm3
	movupd	%xmm2, -48(%rdi)
	movupd	%xmm3, -32(%rdi)
	movupd	-16(%rbx,%rbp,8), %xmm0
	movupd	(%rbx,%rbp,8), %xmm1
	movupd	-16(%rdi), %xmm2
	movupd	(%rdi), %xmm3
	subpd	%xmm0, %xmm2
	subpd	%xmm1, %xmm3
	movupd	%xmm2, -16(%rdi)
	movupd	%xmm3, (%rdi)
	addq	$8, %rbp
	addq	$64, %rdi
	cmpq	$1006, %rbp             # imm = 0x3EE
	jne	.LBB6_42
.LBB6_45:                               # %middle.block133
                                        #   in Loop: Header=BB6_39 Depth=1
	incq	%rsi
	addq	$8000, %rcx             # imm = 0x1F40
	addq	$8000, %rdx             # imm = 0x1F40
	cmpq	$1000, %rsi             # imm = 0x3E8
	jne	.LBB6_39
# BB#46:                                # %.lr.ph.i67.preheader
	xorl	%r9d, %r9d
	movq	%r14, %r8
	.p2align	4, 0x90
.LBB6_47:                               # %.lr.ph.i67
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_48 Depth 2
                                        #       Child Loop BB6_49 Depth 3
	movq	%r8, %rcx
	movq	%r9, %rsi
	.p2align	4, 0x90
.LBB6_48:                               #   Parent Loop BB6_47 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_49 Depth 3
	imulq	$8000, %r9, %rax        # imm = 0x1F40
	addq	%r12, %rax
	leaq	(%rax,%rsi,8), %rdi
	movq	$0, (%rax,%rsi,8)
	xorpd	%xmm0, %xmm0
	movl	$1000, %ebp             # imm = 0x3E8
	movq	%rcx, %rdx
	movq	%r8, %rax
	.p2align	4, 0x90
.LBB6_49:                               #   Parent Loop BB6_47 Depth=1
                                        #     Parent Loop BB6_48 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	mulsd	(%rdx), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdi)
	movsd	8000(%rax), %xmm0       # xmm0 = mem[0],zero
	mulsd	8000(%rdx), %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, (%rdi)
	addq	$16000, %rax            # imm = 0x3E80
	addq	$16000, %rdx            # imm = 0x3E80
	addq	$-2, %rbp
	jne	.LBB6_49
# BB#50:                                #   in Loop: Header=BB6_48 Depth=2
	imulq	$8000, %rsi, %rax       # imm = 0x1F40
	addq	%r12, %rax
	movsd	%xmm0, (%rax,%r9,8)
	incq	%rsi
	addq	$8, %rcx
	cmpq	$1000, %rsi             # imm = 0x3E8
	jne	.LBB6_48
# BB#51:                                # %._crit_edge.i76
                                        #   in Loop: Header=BB6_47 Depth=1
	incq	%r9
	addq	$8, %r8
	cmpq	$1000, %r9              # imm = 0x3E8
	jne	.LBB6_47
# BB#52:                                # %.preheader.i78.preheader
	xorl	%edx, %edx
	movl	$1, %eax
	movapd	.LCPI6_2(%rip), %xmm2   # xmm2 = [nan,nan]
	movsd	.LCPI6_3(%rip), %xmm3   # xmm3 = mem[0],zero
.LBB6_53:                               # %.preheader.i78
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_54 Depth 2
	movq	%rax, %rsi
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB6_54:                               #   Parent Loop BB6_53 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%r15,%rsi,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%r12,%rsi,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_55
# BB#57:                                #   in Loop: Header=BB6_54 Depth=2
	movsd	(%r15,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r12,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_56
# BB#58:                                #   in Loop: Header=BB6_54 Depth=2
	addq	$2, %rsi
	leaq	2(%rcx), %rdi
	incq	%rcx
	cmpq	$1000, %rcx             # imm = 0x3E8
	movq	%rdi, %rcx
	jl	.LBB6_54
# BB#59:                                #   in Loop: Header=BB6_53 Depth=1
	incq	%rdx
	addq	$1000, %rax             # imm = 0x3E8
	cmpq	$1000, %rdx             # imm = 0x3E8
	jl	.LBB6_53
# BB#60:                                # %check_FP.exit
	movl	$16001, %edi            # imm = 0x3E81
	callq	malloc
	movq	%rax, %r13
	movb	$0, 16000(%r13)
	xorl	%eax, %eax
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB6_61:                               # %.preheader.i82
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_62 Depth 2
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rbp, %rsi
	movl	$15, %ecx
	.p2align	4, 0x90
.LBB6_62:                               #   Parent Loop BB6_61 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rdx
	movl	%edx, %eax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -15(%r13,%rcx)
	movb	%al, -14(%r13,%rcx)
	movq	%rdx, %rax
	shrq	$8, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -13(%r13,%rcx)
	movb	%al, -12(%r13,%rcx)
	movq	%rdx, %rax
	shrq	$16, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -11(%r13,%rcx)
	movb	%al, -10(%r13,%rcx)
	movl	%edx, %eax
	shrl	$24, %eax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -9(%r13,%rcx)
	movb	%al, -8(%r13,%rcx)
	movq	%rdx, %rax
	shrq	$32, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -7(%r13,%rcx)
	movb	%al, -6(%r13,%rcx)
	movq	%rdx, %rax
	shrq	$40, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -5(%r13,%rcx)
	movb	%al, -4(%r13,%rcx)
	movq	%rdx, %rax
	shrq	$48, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -3(%r13,%rcx)
	movb	%al, -2(%r13,%rcx)
	shrq	$56, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, -1(%r13,%rcx)
	movb	%dl, (%r13,%rcx)
	addq	$16, %rcx
	addq	$8, %rsi
	cmpq	$16015, %rcx            # imm = 0x3E8F
	jne	.LBB6_62
# BB#63:                                #   in Loop: Header=BB6_61 Depth=1
	movq	stderr(%rip), %rsi
	movq	%r13, %rdi
	callq	fputs
	movq	16(%rsp), %rax          # 8-byte Reload
	incq	%rax
	addq	$8000, %rbp             # imm = 0x1F40
	cmpq	$1000, %rax             # imm = 0x3E8
	jne	.LBB6_61
# BB#64:                                # %print_array.exit
	movq	%r13, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	xorl	%eax, %eax
	jmp	.LBB6_65
.LBB6_55:                               # %check_FP.exit.threadsplit
	decq	%rcx
.LBB6_56:                               # %check_FP.exit.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_3(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %r8d
	movl	%ecx, %r9d
	callq	fprintf
	movl	$1, %eax
.LBB6_65:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_66:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A[%d][%d] = %lf and B[%d][%d] = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 76


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
