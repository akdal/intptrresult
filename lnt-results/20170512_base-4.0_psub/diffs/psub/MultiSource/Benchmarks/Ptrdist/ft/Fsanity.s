	.text
	.file	"Fsanity.bc"
	.globl	SanityCheck1
	.p2align	4, 0x90
	.type	SanityCheck1,@function
SanityCheck1:                           # @SanityCheck1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB0_5
# BB#1:                                 # %.preheader.preheader
	movq	%r15, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_2:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	callq	LessThan
	testl	%eax, %eax
	jne	.LBB0_6
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	(%rbx), %rsi
	movq	16(%rbx), %rdi
	callq	SanityCheck1
	testl	%eax, %eax
	je	.LBB0_6
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	24(%rbx), %rbx
	cmpq	%r15, %rbx
	jne	.LBB0_2
.LBB0_5:
	movl	$1, %ebp
.LBB0_6:                                # %.loopexit
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	SanityCheck1, .Lfunc_end0-SanityCheck1
	.cfi_endproc

	.globl	SanityCheck2
	.p2align	4, 0x90
	.type	SanityCheck2,@function
SanityCheck2:                           # @SanityCheck2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -32
.Lcfi13:
	.cfi_offset %r14, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB1_8
# BB#1:                                 # %.preheader23.preheader
	movq	%r14, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_2:                                # %.preheader23
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_4 Depth 2
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_7
# BB#3:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	xorl	%eax, %eax
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB1_4:                                # %.preheader
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	40(%rcx), %edx
	leal	1(%rax,%rdx), %eax
	movq	24(%rcx), %rcx
	cmpq	%rdi, %rcx
	jne	.LBB1_4
# BB#5:                                 #   in Loop: Header=BB1_2 Depth=1
	cmpl	40(%rbx), %eax
	jne	.LBB1_9
# BB#6:                                 #   in Loop: Header=BB1_2 Depth=1
	callq	SanityCheck2
	testl	%eax, %eax
	je	.LBB1_9
.LBB1_7:                                #   in Loop: Header=BB1_2 Depth=1
	movq	24(%rbx), %rbx
	cmpq	%r14, %rbx
	jne	.LBB1_2
.LBB1_8:
	movl	$1, %ebp
.LBB1_9:                                # %.loopexit
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	SanityCheck2, .Lfunc_end1-SanityCheck2
	.cfi_endproc

	.globl	SanityCheck3
	.p2align	4, 0x90
	.type	SanityCheck3,@function
SanityCheck3:                           # @SanityCheck3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 64
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r12
	testq	%r12, %r12
	jne	.LBB2_2
# BB#1:
	movl	$1, %r15d
	testl	%r14d, %r14d
	je	.LBB2_6
.LBB2_2:                                # %.preheader.preheader
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB2_3:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	40(%rbp), %ebx
	movq	16(%rbp), %rdi
	movl	%ebx, %esi
	callq	SanityCheck3
	testl	%eax, %eax
	je	.LBB2_6
# BB#4:                                 #   in Loop: Header=BB2_3 Depth=1
	leal	1(%r13,%rbx), %r13d
	movq	24(%rbp), %rbp
	cmpq	%r12, %rbp
	jne	.LBB2_3
# BB#5:
	xorl	%r15d, %r15d
	cmpl	%r14d, %r13d
	sete	%r15b
.LBB2_6:                                # %.loopexit
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	SanityCheck3, .Lfunc_end2-SanityCheck3
	.cfi_endproc

	.globl	PrettyPrint
	.p2align	4, 0x90
	.type	PrettyPrint,@function
PrettyPrint:                            # @PrettyPrint
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 32
.Lcfi31:
	.cfi_offset %rbx, -24
.Lcfi32:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB3_4
# BB#1:
	movl	$40, %edi
	callq	putchar
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movl	40(%rbx), %esi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movq	16(%rbx), %rdi
	callq	PrettyPrint
	movq	24(%rbx), %rbx
	cmpq	%r14, %rbx
	jne	.LBB3_2
# BB#3:
	movl	$41, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	putchar                 # TAILCALL
.LBB3_4:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	printf                  # TAILCALL
.Lfunc_end3:
	.size	PrettyPrint, .Lfunc_end3-PrettyPrint
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" nil "
	.size	.L.str, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"[%u] "
	.size	.L.str.2, 6


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
