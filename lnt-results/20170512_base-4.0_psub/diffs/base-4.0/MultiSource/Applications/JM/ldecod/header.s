	.text
	.file	"header.bc"
	.globl	CeilLog2
	.p2align	4, 0x90
	.type	CeilLog2,@function
CeilLog2:                               # @CeilLog2
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	decl	%edi
	je	.LBB0_2
	.p2align	4, 0x90
.LBB0_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	shrl	%edi
	incl	%eax
	testl	%edi, %edi
	jne	.LBB0_1
.LBB0_2:                                # %._crit_edge
	retq
.Lfunc_end0:
	.size	CeilLog2, .Lfunc_end0-CeilLog2
	.cfi_endproc

	.globl	FirstPartOfSliceHeader
	.p2align	4, 0x90
	.type	FirstPartOfSliceHeader,@function
FirstPartOfSliceHeader:                 # @FirstPartOfSliceHeader
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	img(%rip), %rax
	movq	5592(%rax), %r14
	movslq	28(%r14), %rax
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	movq	40(%r14), %rcx
	movslq	assignSE2partition(%rax), %rax
	imulq	$56, %rax, %rax
	movq	(%rcx,%rax), %rbx
	movl	8(%rbx), %eax
	movl	%eax, UsedBits(%rip)
	movl	$.L.str, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movl	%eax, 20(%r14)
	movl	$.L.str.1, %edi
	movq	%rbx, %rsi
	callq	ue_v
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-5(%rax), %ecx
	cmpl	$4, %eax
	cmovlel	%eax, %ecx
	movl	%ecx, 12(%r14)
	movq	img(%rip), %rax
	movl	%ecx, 44(%rax)
	movl	$.L.str.2, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movl	%eax, 148(%r14)
	movl	UsedBits(%rip), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	FirstPartOfSliceHeader, .Lfunc_end1-FirstPartOfSliceHeader
	.cfi_endproc

	.globl	RestOfSliceHeader
	.p2align	4, 0x90
	.type	RestOfSliceHeader,@function
RestOfSliceHeader:                      # @RestOfSliceHeader
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi11:
	.cfi_def_cfa_offset 80
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rax
	movq	5592(%rax), %rbp
	movslq	28(%rbp), %rax
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	movq	40(%rbp), %rcx
	movslq	assignSE2partition(%rax), %rax
	imulq	$56, %rax, %rax
	movq	(%rcx,%rax), %rbx
	movq	active_sps(%rip), %rax
	movl	1008(%rax), %edi
	addl	$4, %edi
	movl	$.L.str.3, %esi
	movq	%rbx, %rdx
	callq	u_v
	movq	img(%rip), %rcx
	movl	%eax, 5676(%rcx)
	cmpl	$0, 5804(%rcx)
	je	.LBB2_2
# BB#1:
	movl	%eax, 5660(%rcx)
	movl	$0, 6056(%rcx)
.LBB2_2:
	movq	active_sps(%rip), %rax
	cmpl	$0, 2076(%rax)
	je	.LBB2_4
# BB#3:
	movl	$0, 5584(%rcx)
	movl	$0, 5680(%rcx)
.LBB2_7:
	xorl	%eax, %eax
	jmp	.LBB2_8
.LBB2_4:
	movl	$.L.str.4, %edi
	movq	%rbx, %rsi
	callq	u_1
	movq	img(%rip), %rcx
	movl	%eax, 5680(%rcx)
	testl	%eax, %eax
	je	.LBB2_6
# BB#5:
	movl	$.L.str.5, %edi
	movq	%rbx, %rsi
	callq	u_1
	movq	img(%rip), %rcx
	movl	%eax, 5684(%rcx)
	cmpl	$1, %eax
	movl	$1, %eax
	sbbl	$-1, %eax
	movl	%eax, 5584(%rcx)
.LBB2_8:
	movl	%eax, 16(%rbp)
	movq	active_sps(%rip), %rax
	cmpl	$0, 2080(%rax)
	je	.LBB2_9
# BB#10:
	cmpl	$0, 5680(%rcx)
	sete	%dl
	jmp	.LBB2_11
.LBB2_9:
	xorl	%edx, %edx
.LBB2_11:
	movzbl	%dl, %edx
	movl	%edx, 5624(%rcx)
	cmpl	$0, 5804(%rcx)
	je	.LBB2_13
# BB#12:
	movl	$.L.str.6, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movq	img(%rip), %rcx
	movl	%eax, 5812(%rcx)
	movq	active_sps(%rip), %rax
.LBB2_13:
	movl	1012(%rax), %edx
	testl	%edx, %edx
	jne	.LBB2_18
# BB#14:
	movl	1016(%rax), %edi
	addl	$4, %edi
	movl	$.L.str.7, %esi
	movq	%rbx, %rdx
	callq	u_v
	movq	img(%rip), %rcx
	movl	%eax, 5688(%rcx)
	movq	active_pps(%rip), %rdx
	xorl	%eax, %eax
	cmpl	$1, 984(%rdx)
	jne	.LBB2_17
# BB#15:
	cmpl	$0, 5680(%rcx)
	jne	.LBB2_17
# BB#16:
	movl	$.L.str.8, %edi
	movq	%rbx, %rsi
	callq	se_v
	movq	img(%rip), %rcx
.LBB2_17:
	movl	%eax, 5692(%rcx)
	movq	active_sps(%rip), %rax
	movl	1012(%rax), %edx
.LBB2_18:
	cmpl	$1, %edx
	jne	.LBB2_24
# BB#19:
	cmpl	$0, 1020(%rax)
	je	.LBB2_20
# BB#23:
	movq	$0, 5696(%rcx)
	jmp	.LBB2_24
.LBB2_20:
	movl	$.L.str.9, %edi
	movq	%rbx, %rsi
	callq	se_v
	movq	img(%rip), %rcx
	movl	%eax, 5696(%rcx)
	movq	active_pps(%rip), %rax
	cmpl	$1, 984(%rax)
	jne	.LBB2_24
# BB#21:
	cmpl	$0, 5680(%rcx)
	jne	.LBB2_24
# BB#22:
	movl	$.L.str.10, %edi
	movq	%rbx, %rsi
	callq	se_v
	movq	img(%rip), %rcx
	movl	%eax, 5700(%rcx)
.LBB2_24:
	movq	active_pps(%rip), %rax
	cmpl	$0, 1152(%rax)
	je	.LBB2_26
# BB#25:
	movl	$.L.str.11, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movq	img(%rip), %rcx
	movl	%eax, 5652(%rcx)
.LBB2_26:
	movl	44(%rcx), %eax
	cmpl	$1, %eax
	jne	.LBB2_28
# BB#27:
	movl	$.L.str.12, %edi
	movq	%rbx, %rsi
	callq	u_1
	movq	img(%rip), %rcx
	movl	%eax, 40(%rcx)
	movl	44(%rcx), %eax
.LBB2_28:
	movq	active_pps(%rip), %rdx
	movl	1112(%rdx), %esi
	incl	%esi
	movl	%esi, 5640(%rcx)
	movl	1116(%rdx), %edx
	incl	%edx
	movl	%edx, 5644(%rcx)
	cmpl	$3, %eax
	ja	.LBB2_34
# BB#29:
	cmpl	$2, %eax
	je	.LBB2_34
# BB#30:
	movl	$.L.str.13, %edi
	movq	%rbx, %rsi
	callq	u_1
	testl	%eax, %eax
	je	.LBB2_31
# BB#32:
	movl	$.L.str.14, %edi
	movq	%rbx, %rsi
	callq	ue_v
	incl	%eax
	movq	img(%rip), %rcx
	movl	%eax, 5640(%rcx)
	cmpl	$1, 44(%rcx)
	jne	.LBB2_34
# BB#33:
	movl	$.L.str.15, %edi
	movq	%rbx, %rsi
	callq	ue_v
	incl	%eax
	movq	img(%rip), %rcx
	movl	%eax, 5644(%rcx)
	jmp	.LBB2_34
.LBB2_31:                               # %._crit_edge
	movq	img(%rip), %rcx
.LBB2_34:
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rbx, (%rsp)            # 8-byte Spill
	cmpl	$1, 44(%rcx)
	je	.LBB2_36
# BB#35:
	movl	$0, 5644(%rcx)
.LBB2_36:
	movq	5592(%rcx), %r15
	movslq	28(%r15), %rax
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	movq	40(%r15), %rcx
	movslq	assignSE2partition(%rax), %rax
	imulq	$56, %rax, %rax
	movq	(%rcx,%rax), %r12
	movq	%r15, %rdi
	callq	alloc_ref_pic_list_reordering_buffer
	movq	img(%rip), %rax
	movl	44(%rax), %eax
	cmpl	$2, %eax
	je	.LBB2_46
# BB#37:
	cmpl	$4, %eax
	je	.LBB2_46
# BB#38:
	movl	$.L.str.58, %edi
	movq	%r12, %rsi
	callq	u_1
	movl	%eax, 64(%r15)
	testl	%eax, %eax
	je	.LBB2_46
# BB#39:                                # %.preheader63.i
	leaq	80(%r15), %rbx
	leaq	88(%r15), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB2_40:                               # =>This Inner Loop Header: Depth=1
	movl	$.L.str.59, %edi
	movq	%r12, %rsi
	callq	ue_v
	movl	%eax, %ebp
	movq	72(%r15), %rax
	movl	%ebp, (%rax,%r14)
	movl	%ebp, %eax
	orl	$1, %eax
	cmpl	$1, %eax
	jne	.LBB2_42
# BB#41:                                #   in Loop: Header=BB2_40 Depth=1
	movl	$.L.str.60, %edi
	movq	%rbx, %r13
	jmp	.LBB2_44
	.p2align	4, 0x90
.LBB2_42:                               #   in Loop: Header=BB2_40 Depth=1
	cmpl	$2, %ebp
	jne	.LBB2_45
# BB#43:                                #   in Loop: Header=BB2_40 Depth=1
	movl	$.L.str.61, %edi
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB2_44:                               # %.sink.split.i
                                        #   in Loop: Header=BB2_40 Depth=1
	movq	%r12, %rsi
	callq	ue_v
	movq	(%r13), %rcx
	movl	%eax, (%rcx,%r14)
.LBB2_45:                               #   in Loop: Header=BB2_40 Depth=1
	addq	$4, %r14
	cmpl	$3, %ebp
	jne	.LBB2_40
.LBB2_46:                               # %.loopexit64.i
	movq	img(%rip), %rax
	cmpl	$1, 44(%rax)
	jne	.LBB2_55
# BB#47:
	movl	$.L.str.62, %edi
	movq	%r12, %rsi
	callq	u_1
	movl	%eax, 96(%r15)
	testl	%eax, %eax
	je	.LBB2_55
# BB#48:                                # %.preheader.i
	leaq	112(%r15), %r14
	leaq	120(%r15), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_49:                               # =>This Inner Loop Header: Depth=1
	movl	$.L.str.63, %edi
	movq	%r12, %rsi
	callq	ue_v
	movl	%eax, %ebp
	movq	104(%r15), %rax
	movl	%ebp, (%rax,%rbx)
	movl	%ebp, %eax
	orl	$1, %eax
	cmpl	$1, %eax
	jne	.LBB2_51
# BB#50:                                #   in Loop: Header=BB2_49 Depth=1
	movl	$.L.str.64, %edi
	movq	%r14, %r13
	jmp	.LBB2_53
	.p2align	4, 0x90
.LBB2_51:                               #   in Loop: Header=BB2_49 Depth=1
	cmpl	$2, %ebp
	jne	.LBB2_54
# BB#52:                                #   in Loop: Header=BB2_49 Depth=1
	movl	$.L.str.65, %edi
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB2_53:                               # %.sink.split8.i
                                        #   in Loop: Header=BB2_49 Depth=1
	movq	%r12, %rsi
	callq	ue_v
	movq	(%r13), %rcx
	movl	%eax, (%rcx,%rbx)
.LBB2_54:                               #   in Loop: Header=BB2_49 Depth=1
	addq	$4, %rbx
	cmpl	$3, %ebp
	jne	.LBB2_49
.LBB2_55:                               # %.loopexit.i
	movq	img(%rip), %rdx
	cmpl	$0, 5652(%rdx)
	je	.LBB2_57
# BB#56:
	movq	80(%r15), %rax
	movl	(%rax), %eax
	incl	%eax
	movl	%eax, redundant_slice_ref_idx(%rip)
.LBB2_57:                               # %ref_pic_list_reordering.exit
	movq	active_pps(%rip), %rax
	movl	1120(%rax), %ecx
	testl	%ecx, %ecx
	movq	16(%rsp), %r15          # 8-byte Reload
	je	.LBB2_60
# BB#58:
	movl	12(%r15), %esi
	testl	%esi, %esi
	je	.LBB2_62
# BB#59:
	cmpl	$3, %esi
	jne	.LBB2_60
.LBB2_62:                               # %.thread
	movl	$1, 5800(%rdx)
	movq	(%rsp), %r14            # 8-byte Reload
	jmp	.LBB2_65
.LBB2_60:
	cmpl	$0, 1124(%rax)
	je	.LBB2_61
# BB#63:
	cmpl	$1, 12(%r15)
	sete	%sil
	jmp	.LBB2_64
.LBB2_61:
	xorl	%esi, %esi
.LBB2_64:
	movq	(%rsp), %r14            # 8-byte Reload
	testl	%ecx, %ecx
	movzbl	%sil, %ecx
	movl	%ecx, 5800(%rdx)
	je	.LBB2_67
.LBB2_65:
	movl	44(%rdx), %ecx
	testl	%ecx, %ecx
	je	.LBB2_69
# BB#66:
	cmpl	$3, %ecx
	je	.LBB2_69
.LBB2_67:
	cmpl	$1, 1124(%rax)
	jne	.LBB2_95
# BB#68:
	cmpl	$1, 44(%rdx)
	jne	.LBB2_95
.LBB2_69:
	movq	5592(%rdx), %rax
	movslq	28(%rax), %rcx
	leaq	(%rcx,%rcx,4), %rcx
	shlq	$4, %rcx
	movq	40(%rax), %rax
	movslq	assignSE2partition(%rcx), %rcx
	imulq	$56, %rcx, %rcx
	movq	(%rax,%rcx), %rbx
	movl	$.L.str.66, %edi
	movq	%rbx, %rsi
	callq	ue_v
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	img(%rip), %rdi
	movl	%eax, 5760(%rdi)
	leal	-1(%rax), %ecx
	movl	$1, %ebp
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	testl	%eax, %eax
	cmovel	%eax, %edx
	movl	%edx, 5792(%rdi)
	movq	active_sps(%rip), %rax
	cmpl	$0, 32(%rax)
	je	.LBB2_71
# BB#70:
	movl	$.L.str.67, %edi
	movq	%rbx, %rsi
	callq	ue_v
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	img(%rip), %rdi
	movl	%eax, 5764(%rdi)
	leal	-1(%rax), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	testl	%eax, %eax
	cmovel	%eax, %ebp
	movl	%ebp, 5796(%rdi)
.LBB2_71:
	callq	reset_wp_params
	movq	img(%rip), %rdx
	cmpl	$0, 5640(%rdx)
	jle	.LBB2_82
# BB#72:                                # %.lr.ph111.i.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_73:                               # %.lr.ph111.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.68, %edi
	movq	%rbx, %rsi
	callq	u_1
	testl	%eax, %eax
	je	.LBB2_75
# BB#74:                                #   in Loop: Header=BB2_73 Depth=1
	movl	$.L.str.69, %edi
	movq	%rbx, %rsi
	callq	se_v
	movq	img(%rip), %rcx
	movq	5768(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movl	%eax, (%rcx)
	movl	$.L.str.70, %edi
	movq	%rbx, %rsi
	callq	se_v
	movq	img(%rip), %rdx
	jmp	.LBB2_76
	.p2align	4, 0x90
.LBB2_75:                               #   in Loop: Header=BB2_73 Depth=1
	movq	img(%rip), %rdx
	movzbl	5760(%rdx), %ecx
	movl	$1, %eax
	shll	%cl, %eax
	movq	5768(%rdx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movl	%eax, (%rcx)
	xorl	%eax, %eax
.LBB2_76:                               #   in Loop: Header=BB2_73 Depth=1
	movq	5776(%rdx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movl	%eax, (%rcx)
	movq	active_sps(%rip), %rax
	cmpl	$0, 32(%rax)
	je	.LBB2_81
# BB#77:                                #   in Loop: Header=BB2_73 Depth=1
	movl	$.L.str.71, %edi
	movq	%rbx, %rsi
	callq	u_1
	testl	%eax, %eax
	je	.LBB2_79
# BB#78:                                # %.split109.preheader.i
                                        #   in Loop: Header=BB2_73 Depth=1
	movl	$.L.str.72, %edi
	movq	%rbx, %rsi
	callq	se_v
	movq	img(%rip), %rcx
	movq	5768(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movl	%eax, 4(%rcx)
	movl	$.L.str.73, %edi
	movq	%rbx, %rsi
	callq	se_v
	movq	img(%rip), %rcx
	movq	5776(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movl	%eax, 4(%rcx)
	movl	$.L.str.72, %edi
	movq	%rbx, %rsi
	callq	se_v
	movq	img(%rip), %rcx
	movq	5768(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movl	%eax, 8(%rcx)
	movl	$.L.str.73, %edi
	movq	%rbx, %rsi
	callq	se_v
	movq	img(%rip), %rdx
	movq	5776(%rdx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rsi
	jmp	.LBB2_80
	.p2align	4, 0x90
.LBB2_79:                               # %.split109.us.preheader.i
                                        #   in Loop: Header=BB2_73 Depth=1
	movq	img(%rip), %rdx
	movzbl	5764(%rdx), %ecx
	movl	$1, %eax
	shll	%cl, %eax
	movq	5768(%rdx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rdi
	movl	%eax, 4(%rdi)
	movq	5776(%rdx), %rax
	movq	(%rax), %rax
	movq	(%rax,%rbp,8), %rsi
	movl	$0, 4(%rsi)
	movzbl	5764(%rdx), %ecx
	movl	$1, %eax
	shll	%cl, %eax
	movl	%eax, 8(%rdi)
	xorl	%eax, %eax
.LBB2_80:                               # %.loopexit105.sink.split.i
                                        #   in Loop: Header=BB2_73 Depth=1
	movl	%eax, 8(%rsi)
.LBB2_81:                               # %.loopexit105.i
                                        #   in Loop: Header=BB2_73 Depth=1
	incq	%rbp
	movslq	5640(%rdx), %rax
	cmpq	%rax, %rbp
	jl	.LBB2_73
.LBB2_82:                               # %._crit_edge.i
	cmpl	$1, 44(%rdx)
	jne	.LBB2_95
# BB#83:
	movq	active_pps(%rip), %rax
	cmpl	$1, 1124(%rax)
	jne	.LBB2_95
# BB#84:                                # %.preheader.i66
	cmpl	$0, 5644(%rdx)
	jle	.LBB2_95
# BB#85:                                # %.lr.ph.i.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_86:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.74, %edi
	movq	%rbx, %rsi
	callq	u_1
	testl	%eax, %eax
	je	.LBB2_88
# BB#87:                                #   in Loop: Header=BB2_86 Depth=1
	movl	$.L.str.75, %edi
	movq	%rbx, %rsi
	callq	se_v
	movq	img(%rip), %rcx
	movq	5768(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movl	%eax, (%rcx)
	movl	$.L.str.76, %edi
	movq	%rbx, %rsi
	callq	se_v
	movq	img(%rip), %rdx
	jmp	.LBB2_89
	.p2align	4, 0x90
.LBB2_88:                               #   in Loop: Header=BB2_86 Depth=1
	movq	img(%rip), %rdx
	movzbl	5760(%rdx), %ecx
	movl	$1, %eax
	shll	%cl, %eax
	movq	5768(%rdx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movl	%eax, (%rcx)
	xorl	%eax, %eax
.LBB2_89:                               #   in Loop: Header=BB2_86 Depth=1
	movq	5776(%rdx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movl	%eax, (%rcx)
	movq	active_sps(%rip), %rax
	cmpl	$0, 32(%rax)
	je	.LBB2_94
# BB#90:                                #   in Loop: Header=BB2_86 Depth=1
	movl	$.L.str.77, %edi
	movq	%rbx, %rsi
	callq	u_1
	testl	%eax, %eax
	je	.LBB2_92
# BB#91:                                # %.split.preheader.i
                                        #   in Loop: Header=BB2_86 Depth=1
	movl	$.L.str.78, %edi
	movq	%rbx, %rsi
	callq	se_v
	movq	img(%rip), %rcx
	movq	5768(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movl	%eax, 4(%rcx)
	movl	$.L.str.79, %edi
	movq	%rbx, %rsi
	callq	se_v
	movq	img(%rip), %rcx
	movq	5776(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movl	%eax, 4(%rcx)
	movl	$.L.str.78, %edi
	movq	%rbx, %rsi
	callq	se_v
	movq	img(%rip), %rcx
	movq	5768(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movl	%eax, 8(%rcx)
	movl	$.L.str.79, %edi
	movq	%rbx, %rsi
	callq	se_v
	movq	img(%rip), %rdx
	movq	5776(%rdx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rsi
	jmp	.LBB2_93
	.p2align	4, 0x90
.LBB2_92:                               # %.split.us.preheader.i
                                        #   in Loop: Header=BB2_86 Depth=1
	movq	img(%rip), %rdx
	movzbl	5764(%rdx), %ecx
	movl	$1, %eax
	shll	%cl, %eax
	movq	5768(%rdx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rdi
	movl	%eax, 4(%rdi)
	movq	5776(%rdx), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbp,8), %rsi
	movl	$0, 4(%rsi)
	movzbl	5764(%rdx), %ecx
	movl	$1, %eax
	shll	%cl, %eax
	movl	%eax, 8(%rdi)
	xorl	%eax, %eax
.LBB2_93:                               # %.loopexit.sink.split.i
                                        #   in Loop: Header=BB2_86 Depth=1
	movl	%eax, 8(%rsi)
.LBB2_94:                               # %.loopexit.i69
                                        #   in Loop: Header=BB2_86 Depth=1
	incq	%rbp
	movslq	5644(%rdx), %rax
	cmpq	%rax, %rbp
	jl	.LBB2_86
.LBB2_95:                               # %pred_weight_table.exit
	cmpl	$0, 5808(%rdx)
	je	.LBB2_97
# BB#96:
	movq	%r14, %rdi
	callq	dec_ref_pic_marking
.LBB2_97:
	movq	active_pps(%rip), %rax
	xorl	%ebp, %ebp
	cmpl	$0, 12(%rax)
	movl	$0, %eax
	je	.LBB2_101
# BB#98:
	movq	img(%rip), %rax
	movl	44(%rax), %ecx
	cmpl	$2, %ecx
	movl	$0, %eax
	je	.LBB2_101
# BB#99:
	cmpl	$4, %ecx
	movl	$0, %eax
	je	.LBB2_101
# BB#100:
	movl	$.L.str.16, %edi
	movq	%r14, %rsi
	callq	ue_v
.LBB2_101:
	movq	img(%rip), %rcx
	movl	%eax, 5868(%rcx)
	movl	$.L.str.17, %edi
	movq	%r14, %rsi
	callq	se_v
	movl	%eax, %ebx
	movq	active_pps(%rip), %rax
	movl	1128(%rax), %eax
	leal	26(%rbx,%rax), %ecx
	movq	img(%rip), %rax
	movl	%ecx, 28(%rax)
	movl	%ecx, 4(%r15)
	cmpl	$51, %ecx
	jg	.LBB2_103
# BB#102:
	subl	5884(%rax), %ebp
	cmpl	%ebp, %ecx
	jge	.LBB2_104
.LBB2_103:
	movl	$.L.str.18, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	img(%rip), %rax
.LBB2_104:
	movl	%ebx, 8(%r15)
	movl	44(%rax), %eax
	leal	-3(%rax), %ecx
	cmpl	$1, %ecx
	ja	.LBB2_109
# BB#105:
	cmpl	$3, %eax
	jne	.LBB2_107
# BB#106:
	movl	$.L.str.19, %edi
	movq	%r14, %rsi
	callq	u_1
	movq	img(%rip), %rcx
	movl	%eax, 36(%rcx)
.LBB2_107:
	movl	$.L.str.20, %edi
	movq	%r14, %rsi
	callq	se_v
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	active_pps(%rip), %rcx
	movl	1132(%rcx), %ecx
	leal	26(%rax,%rcx), %eax
	movq	img(%rip), %rcx
	movl	%eax, 32(%rcx)
	cmpl	$52, %eax
	jb	.LBB2_109
# BB#108:
	movl	$.L.str.21, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
.LBB2_109:
	movq	active_pps(%rip), %rax
	cmpl	$0, 1144(%rax)
	je	.LBB2_113
# BB#110:
	movl	$.L.str.22, %edi
	movq	%r14, %rsi
	callq	ue_v
	movl	%eax, 136(%r15)
	cmpl	$1, %eax
	jne	.LBB2_111
# BB#112:
	movq	$0, 140(%r15)
	jmp	.LBB2_114
.LBB2_113:
	movl	$0, 144(%r15)
	movq	$0, 136(%r15)
	jmp	.LBB2_114
.LBB2_111:
	movl	$.L.str.23, %edi
	movq	%r14, %rsi
	callq	se_v
	addl	%eax, %eax
	movl	%eax, 140(%r15)
	movl	$.L.str.24, %edi
	movq	%r14, %rsi
	callq	se_v
	addl	%eax, %eax
	movl	%eax, 144(%r15)
.LBB2_114:
	movq	active_pps(%rip), %rcx
	cmpl	$0, 988(%rcx)
	je	.LBB2_120
# BB#115:
	movl	992(%rcx), %eax
	addl	$-3, %eax
	cmpl	$2, %eax
	ja	.LBB2_120
# BB#116:
	movq	active_sps(%rip), %rdx
	movl	2068(%rdx), %eax
	movl	2072(%rdx), %edx
	incl	%edx
	incl	%eax
	imull	%edx, %eax
	movl	1096(%rcx), %ecx
	incl	%ecx
	xorl	%edi, %edi
	xorl	%edx, %edx
	divl	%ecx
	xorl	%ecx, %ecx
	testl	%edx, %edx
	setne	%cl
	addl	%eax, %ecx
	je	.LBB2_119
# BB#117:                               # %.lr.ph.i70.preheader
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_118:                              # %.lr.ph.i70
                                        # =>This Inner Loop Header: Depth=1
	shrl	%ecx
	incl	%edi
	testl	%ecx, %ecx
	jne	.LBB2_118
.LBB2_119:                              # %CeilLog2.exit
	movl	$.L.str.25, %esi
	movq	%r14, %rdx
	callq	u_v
	movq	img(%rip), %rcx
	movl	%eax, 5648(%rcx)
.LBB2_120:
	movq	img(%rip), %rsi
	movl	5680(%rsi), %edi
	movl	5828(%rsi), %ecx
	incl	%edi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%edi
	movl	%eax, 5832(%rsi)
	movl	5820(%rsi), %edx
	imull	%edx, %eax
	movl	%eax, 5836(%rsi)
	imull	%edx, %ecx
	movl	%ecx, 5840(%rsi)
	movl	UsedBits(%rip), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_6:
	movl	$0, 5584(%rcx)
	movl	$0, 5684(%rcx)
	jmp	.LBB2_7
.Lfunc_end2:
	.size	RestOfSliceHeader, .Lfunc_end2-RestOfSliceHeader
	.cfi_endproc

	.globl	dec_ref_pic_marking
	.p2align	4, 0x90
	.type	dec_ref_pic_marking,@function
dec_ref_pic_marking:                    # @dec_ref_pic_marking
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -32
.Lcfi22:
	.cfi_offset %r14, -24
.Lcfi23:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	img(%rip), %rax
	movq	5632(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB3_3
# BB#1:                                 # %.lr.ph.preheader
	addq	$5632, %rax             # imm = 0x1600
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rdi), %rax
	movq	%rax, (%rcx)
	callq	free
	movq	img(%rip), %rax
	leaq	5632(%rax), %rcx
	movq	5632(%rax), %rdi
	testq	%rdi, %rdi
	jne	.LBB3_2
.LBB3_3:                                # %._crit_edge
	cmpl	$0, 5804(%rax)
	je	.LBB3_5
# BB#4:
	movl	$.L.str.26, %edi
	movq	%r14, %rsi
	callq	u_1
	movq	img(%rip), %rcx
	movl	%eax, 5848(%rcx)
	movl	$.L.str.27, %edi
	movq	%r14, %rsi
	callq	u_1
	movq	img(%rip), %rcx
	movl	%eax, 5852(%rcx)
	jmp	.LBB3_18
.LBB3_5:
	movl	$.L.str.28, %edi
	movq	%r14, %rsi
	callq	u_1
	movq	img(%rip), %rcx
	movl	%eax, 5856(%rcx)
	testl	%eax, %eax
	je	.LBB3_18
	.p2align	4, 0x90
.LBB3_6:                                # %.preheader35
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_15 Depth 2
	movl	$1, %edi
	movl	$32, %esi
	callq	calloc
	movq	%rax, %r15
	movl	$.L.str.29, %edi
	movq	%r14, %rsi
	callq	ue_v
	movl	%eax, %ebx
	movl	%ebx, (%r15)
	orl	$2, %eax
	cmpl	$3, %eax
	jne	.LBB3_8
# BB#7:                                 #   in Loop: Header=BB3_6 Depth=1
	movl	$.L.str.30, %edi
	movq	%r14, %rsi
	callq	ue_v
	movl	%eax, 4(%r15)
.LBB3_8:                                #   in Loop: Header=BB3_6 Depth=1
	leal	-2(%rbx), %eax
	cmpl	$4, %eax
	ja	.LBB3_13
# BB#9:                                 #   in Loop: Header=BB3_6 Depth=1
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_11:                               #   in Loop: Header=BB3_6 Depth=1
	movl	$.L.str.32, %edi
	movq	%r14, %rsi
	callq	ue_v
	movl	%eax, 12(%r15)
	cmpl	$4, %ebx
	jne	.LBB3_13
.LBB3_12:                               #   in Loop: Header=BB3_6 Depth=1
	movl	$.L.str.33, %edi
	movq	%r14, %rsi
	callq	ue_v
	movl	%eax, 16(%r15)
	jmp	.LBB3_13
	.p2align	4, 0x90
.LBB3_10:                               #   in Loop: Header=BB3_6 Depth=1
	movl	$.L.str.31, %edi
	movq	%r14, %rsi
	callq	ue_v
	movl	%eax, 8(%r15)
.LBB3_13:                               #   in Loop: Header=BB3_6 Depth=1
	movq	img(%rip), %rax
	movq	5632(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB3_14
	.p2align	4, 0x90
.LBB3_15:                               # %.preheader
                                        #   Parent Loop BB3_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	24(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB3_15
# BB#16:                                #   in Loop: Header=BB3_6 Depth=1
	addq	$24, %rax
	jmp	.LBB3_17
	.p2align	4, 0x90
.LBB3_14:                               #   in Loop: Header=BB3_6 Depth=1
	addq	$5632, %rax             # imm = 0x1600
.LBB3_17:                               # %.loopexit
                                        #   in Loop: Header=BB3_6 Depth=1
	movq	%r15, (%rax)
	testl	%ebx, %ebx
	jne	.LBB3_6
.LBB3_18:                               # %.loopexit36
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	dec_ref_pic_marking, .Lfunc_end3-dec_ref_pic_marking
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_10
	.quad	.LBB3_11
	.quad	.LBB3_12
	.quad	.LBB3_13
	.quad	.LBB3_11

	.text
	.globl	decode_poc
	.p2align	4, 0x90
	.type	decode_poc,@function
decode_poc:                             # @decode_poc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 16
.Lcfi25:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	active_sps(%rip), %rsi
	movl	1012(%rsi), %edx
	movl	1016(%rsi), %ecx
	addl	$4, %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	cmpl	$2, %edx
	je	.LBB4_11
# BB#1:
	cmpl	$1, %edx
	je	.LBB4_14
# BB#2:
	testl	%edx, %edx
	jne	.LBB4_77
# BB#3:
	cmpl	$0, 5804(%rbx)
	je	.LBB4_18
# BB#4:
	movl	$0, 5708(%rbx)
	xorl	%edx, %edx
.LBB4_5:                                # %.sink.split
	movl	%edx, 5712(%rbx)
.LBB4_6:
	movl	5688(%rbx), %ecx
	movl	%edx, %esi
	subl	%ecx, %esi
	jbe	.LBB4_8
# BB#7:
	movl	%eax, %edi
	shrl	%edi
	cmpl	%edi, %esi
	jae	.LBB4_25
.LBB4_8:
	movl	%ecx, %esi
	subl	%edx, %esi
	jbe	.LBB4_17
# BB#9:
	movl	%eax, %edx
	shrl	%edx
	cmpl	%edx, %esi
	jbe	.LBB4_17
# BB#10:
	movl	5708(%rbx), %edx
	subl	%eax, %edx
	movl	%edx, %eax
	jmp	.LBB4_26
.LBB4_11:
	cmpl	$0, 5804(%rbx)
	je	.LBB4_21
# BB#12:
	movl	$0, 5740(%rbx)
	movq	$0, 5664(%rbx)
	movl	$0, 5672(%rbx)
	movl	$0, 5752(%rbx)
	cmpl	$0, 5676(%rbx)
	je	.LBB4_30
# BB#13:
	movl	$.L.str.34, %edi
	movl	$-1020, %esi            # imm = 0xFC04
	callq	error
	movl	5676(%rbx), %ecx
	movl	5740(%rbx), %eax
	jmp	.LBB4_76
.LBB4_14:
	cmpl	$0, 5804(%rbx)
	je	.LBB4_23
# BB#15:
	movl	$0, 5740(%rbx)
	movl	$0, 5696(%rbx)
	cmpl	$0, 5676(%rbx)
	je	.LBB4_46
# BB#16:
	movl	$.L.str.34, %edi
	movl	$-1020, %esi            # imm = 0xFC04
	callq	error
	movq	active_sps(%rip), %rsi
	jmp	.LBB4_46
.LBB4_17:
	movl	5708(%rbx), %eax
	jmp	.LBB4_26
.LBB4_18:
	cmpl	$0, 5860(%rbx)
	je	.LBB4_36
# BB#19:
	xorl	%edx, %edx
	cmpl	$0, 5864(%rbx)
	movl	$0, 5708(%rbx)
	jne	.LBB4_5
# BB#20:
	movl	5664(%rbx), %edx
	jmp	.LBB4_5
.LBB4_21:
	cmpl	$0, 5860(%rbx)
	je	.LBB4_37
# BB#22:
	movl	$0, 5736(%rbx)
	movl	$0, 5756(%rbx)
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.LBB4_38
.LBB4_23:
	cmpl	$0, 5860(%rbx)
	je	.LBB4_43
# BB#24:
	movl	$0, 5756(%rbx)
	movl	$0, 5736(%rbx)
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	cmpl	%ecx, 5676(%rbx)
	jb	.LBB4_44
	jmp	.LBB4_45
.LBB4_25:
	addl	5708(%rbx), %eax
.LBB4_26:
	movl	%eax, 5716(%rbx)
	cmpl	$0, 5680(%rbx)
	je	.LBB4_29
# BB#27:
	leal	(%rcx,%rax), %edx
	cmpl	$0, 5684(%rbx)
	je	.LBB4_31
# BB#28:
	movl	%edx, 5668(%rbx)
	jmp	.LBB4_32
.LBB4_29:
	leal	(%rcx,%rax), %esi
	movl	%esi, 5664(%rbx)
	movl	5692(%rbx), %edi
	leal	(%rdi,%rsi), %edx
	movl	%edx, 5668(%rbx)
	testl	%edi, %edi
	cmovgl	%esi, %edx
	movl	%edx, 5672(%rbx)
	jmp	.LBB4_32
.LBB4_30:
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	jmp	.LBB4_76
.LBB4_31:
	movl	%edx, 5664(%rbx)
.LBB4_32:
	movl	%edx, 5752(%rbx)
	movl	%edx, 5672(%rbx)
	movl	5676(%rbx), %edx
	cmpl	5736(%rbx), %edx
	je	.LBB4_34
# BB#33:
	movl	%edx, 5736(%rbx)
.LBB4_34:
	cmpl	$0, 5808(%rbx)
	je	.LBB4_77
# BB#35:
	movl	%ecx, 5712(%rbx)
	movl	%eax, 5708(%rbx)
	popq	%rbx
	retq
.LBB4_36:                               # %._crit_edge196
	movl	5712(%rbx), %edx
	jmp	.LBB4_6
.LBB4_37:                               # %._crit_edge
	movl	5736(%rbx), %edx
	movl	5756(%rbx), %eax
.LBB4_38:
	movl	5676(%rbx), %ecx
	cmpl	%edx, %ecx
	jae	.LBB4_40
# BB#39:
	addl	5816(%rbx), %eax
.LBB4_40:
	movl	%eax, 5740(%rbx)
	leal	(%rcx,%rax), %edx
	movl	%edx, 5720(%rbx)
	addl	%edx, %edx
	cmpl	$1, 5808(%rbx)
	sbbl	$0, %edx
	movl	%edx, 5752(%rbx)
	cmpl	$0, 5680(%rbx)
	je	.LBB4_74
# BB#41:
	cmpl	$0, 5684(%rbx)
	movl	%edx, 5672(%rbx)
	je	.LBB4_75
# BB#42:
	movl	%edx, 5668(%rbx)
	jmp	.LBB4_76
.LBB4_43:                               # %._crit_edge190
	movl	5736(%rbx), %ecx
	movl	5756(%rbx), %eax
	cmpl	%ecx, 5676(%rbx)
	jae	.LBB4_45
.LBB4_44:
	addl	5816(%rbx), %eax
.LBB4_45:
	movl	%eax, 5740(%rbx)
.LBB4_46:
	movslq	1032(%rsi), %r10
	testq	%r10, %r10
	je	.LBB4_48
# BB#47:
	movl	5676(%rbx), %eax
	addl	5740(%rbx), %eax
	jmp	.LBB4_49
.LBB4_48:
	xorl	%eax, %eax
.LBB4_49:
	movl	%eax, 5720(%rbx)
	movl	5808(%rbx), %r8d
	testl	%eax, %eax
	je	.LBB4_52
# BB#50:
	testl	%r8d, %r8d
	jne	.LBB4_52
# BB#51:
	decl	%eax
	movl	%eax, 5720(%rbx)
.LBB4_52:
	movl	$0, 5744(%rbx)
	testl	%r10d, %r10d
	jle	.LBB4_57
# BB#53:                                # %.lr.ph181.preheader
	leaq	-1(%r10), %r9
	movq	%r10, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB4_55
	.p2align	4, 0x90
.LBB4_54:                               # %.lr.ph181.prol
                                        # =>This Inner Loop Header: Depth=1
	addl	1036(%rsi,%rdx,4), %ecx
	movl	%ecx, 5744(%rbx)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB4_54
.LBB4_55:                               # %.lr.ph181.prol.loopexit
	cmpq	$3, %r9
	jb	.LBB4_58
	.p2align	4, 0x90
.LBB4_56:                               # %.lr.ph181
                                        # =>This Inner Loop Header: Depth=1
	addl	1036(%rsi,%rdx,4), %ecx
	movl	%ecx, 5744(%rbx)
	addl	1040(%rsi,%rdx,4), %ecx
	movl	%ecx, 5744(%rbx)
	addl	1044(%rsi,%rdx,4), %ecx
	movl	%ecx, 5744(%rbx)
	addl	1048(%rsi,%rdx,4), %ecx
	movl	%ecx, 5744(%rbx)
	addq	$4, %rdx
	cmpq	%r10, %rdx
	jl	.LBB4_56
	jmp	.LBB4_58
.LBB4_57:
	xorl	%ecx, %ecx
.LBB4_58:                               # %.loopexit178
	testl	%eax, %eax
	je	.LBB4_65
# BB#59:
	decl	%eax
	xorl	%edx, %edx
	divl	%r10d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%eax, 5728(%rbx)
	movl	%edx, 5732(%rbx)
	imull	%eax, %ecx
	movl	%ecx, 5724(%rbx)
	testl	%edx, %edx
	js	.LBB4_66
# BB#60:                                # %.lr.ph.preheader
	leal	1(%rdx), %edi
	xorl	%eax, %eax
	andq	$3, %rdi
	je	.LBB4_62
	.p2align	4, 0x90
.LBB4_61:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	addl	1036(%rsi,%rax,4), %ecx
	movl	%ecx, 5724(%rbx)
	incq	%rax
	cmpq	%rax, %rdi
	jne	.LBB4_61
.LBB4_62:                               # %.lr.ph.prol.loopexit
	cmpl	$3, %edx
	jb	.LBB4_66
# BB#63:                                # %.lr.ph.preheader.new
	movslq	%edx, %rdx
	decq	%rax
	.p2align	4, 0x90
.LBB4_64:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	addl	1040(%rsi,%rax,4), %ecx
	movl	%ecx, 5724(%rbx)
	addl	1044(%rsi,%rax,4), %ecx
	movl	%ecx, 5724(%rbx)
	addl	1048(%rsi,%rax,4), %ecx
	movl	%ecx, 5724(%rbx)
	addl	1052(%rsi,%rax,4), %ecx
	movl	%ecx, 5724(%rbx)
	addq	$4, %rax
	cmpq	%rdx, %rax
	jl	.LBB4_64
	jmp	.LBB4_66
.LBB4_65:
	movl	$0, 5724(%rbx)
	xorl	%ecx, %ecx
.LBB4_66:                               # %.loopexit
	testl	%r8d, %r8d
	jne	.LBB4_68
# BB#67:
	addl	1024(%rsi), %ecx
	movl	%ecx, 5724(%rbx)
.LBB4_68:
	cmpl	$0, 5680(%rbx)
	je	.LBB4_71
# BB#69:
	movl	5696(%rbx), %eax
	cmpl	$0, 5684(%rbx)
	je	.LBB4_72
# BB#70:
	addl	1028(%rsi), %ecx
	addl	%eax, %ecx
	movl	%ecx, 5668(%rbx)
	movl	%ecx, %eax
	jmp	.LBB4_73
.LBB4_71:
	addl	5696(%rbx), %ecx
	movl	%ecx, 5664(%rbx)
	movl	1028(%rsi), %eax
	addl	%ecx, %eax
	addl	5700(%rbx), %eax
	movl	%eax, 5668(%rbx)
	cmpl	%eax, %ecx
	cmovlel	%ecx, %eax
	movl	%eax, 5672(%rbx)
	jmp	.LBB4_73
.LBB4_72:
	addl	%ecx, %eax
	movl	%eax, 5664(%rbx)
.LBB4_73:
	movl	%eax, 5752(%rbx)
	movl	%eax, 5672(%rbx)
	movl	5676(%rbx), %eax
	movl	%eax, 5736(%rbx)
	movl	5740(%rbx), %eax
	movl	%eax, 5756(%rbx)
	popq	%rbx
	retq
.LBB4_74:
	movl	%edx, 5672(%rbx)
	movl	%edx, 5668(%rbx)
.LBB4_75:
	movl	%edx, 5664(%rbx)
.LBB4_76:
	movl	%ecx, 5736(%rbx)
	movl	%eax, 5756(%rbx)
.LBB4_77:
	popq	%rbx
	retq
.Lfunc_end4:
	.size	decode_poc, .Lfunc_end4-decode_poc
	.cfi_endproc

	.globl	dumppoc
	.p2align	4, 0x90
	.type	dumppoc,@function
dumppoc:                                # @dumppoc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 16
.Lcfi27:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$.Lstr, %edi
	callq	puts
	movl	5664(%rbx), %esi
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	callq	printf
	movl	5668(%rbx), %esi
	movl	$.L.str.37, %edi
	xorl	%eax, %eax
	callq	printf
	movl	5676(%rbx), %esi
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	callq	printf
	movl	5680(%rbx), %esi
	movl	$.L.str.39, %edi
	xorl	%eax, %eax
	callq	printf
	movl	5684(%rbx), %esi
	movl	$.L.str.40, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.1, %edi
	callq	puts
	movq	active_sps(%rip), %rax
	movl	1008(%rax), %esi
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	callq	printf
	movq	active_sps(%rip), %rax
	movl	1016(%rax), %esi
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	callq	printf
	movq	active_sps(%rip), %rax
	movl	1012(%rax), %esi
	movl	$.L.str.44, %edi
	xorl	%eax, %eax
	callq	printf
	movq	active_sps(%rip), %rax
	movl	1032(%rax), %esi
	movl	$.L.str.45, %edi
	xorl	%eax, %eax
	callq	printf
	movq	active_sps(%rip), %rax
	movl	1020(%rax), %esi
	movl	$.L.str.46, %edi
	xorl	%eax, %eax
	callq	printf
	movq	active_sps(%rip), %rax
	movl	1024(%rax), %esi
	movl	$.L.str.47, %edi
	xorl	%eax, %eax
	callq	printf
	movq	active_sps(%rip), %rax
	movl	1028(%rax), %esi
	movl	$.L.str.48, %edi
	xorl	%eax, %eax
	callq	printf
	movq	active_sps(%rip), %rax
	movl	1036(%rax), %esi
	movl	$.L.str.49, %edi
	xorl	%eax, %eax
	callq	printf
	movq	active_sps(%rip), %rax
	movl	1040(%rax), %esi
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.2, %edi
	callq	puts
	movq	active_pps(%rip), %rax
	movl	984(%rax), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movl	5696(%rbx), %esi
	movl	$.L.str.53, %edi
	xorl	%eax, %eax
	callq	printf
	movl	5700(%rbx), %esi
	movl	$.L.str.54, %edi
	xorl	%eax, %eax
	callq	printf
	movl	5704(%rbx), %esi
	movl	$.L.str.55, %edi
	xorl	%eax, %eax
	callq	printf
	movl	5804(%rbx), %esi
	movl	$.L.str.56, %edi
	xorl	%eax, %eax
	callq	printf
	movl	5816(%rbx), %esi
	movl	$.L.str.57, %edi
	xorl	%eax, %eax
	callq	printf
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	dumppoc, .Lfunc_end5-dumppoc
	.cfi_endproc

	.globl	picture_order
	.p2align	4, 0x90
	.type	picture_order,@function
picture_order:                          # @picture_order
	.cfi_startproc
# BB#0:
	cmpl	$0, 5680(%rdi)
	je	.LBB6_1
# BB#2:
	cmpl	$0, 5684(%rdi)
	je	.LBB6_3
# BB#4:
	addq	$5668, %rdi             # imm = 0x1624
	movl	(%rdi), %eax
	retq
.LBB6_1:
	addq	$5672, %rdi             # imm = 0x1628
	movl	(%rdi), %eax
	retq
.LBB6_3:
	addq	$5664, %rdi             # imm = 0x1620
	movl	(%rdi), %eax
	retq
.Lfunc_end6:
	.size	picture_order, .Lfunc_end6-picture_order
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"SH: first_mb_in_slice"
	.size	.L.str, 22

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"SH: slice_type"
	.size	.L.str.1, 15

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"SH: pic_parameter_set_id"
	.size	.L.str.2, 25

	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"SH: frame_num"
	.size	.L.str.3, 14

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"SH: field_pic_flag"
	.size	.L.str.4, 19

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"SH: bottom_field_flag"
	.size	.L.str.5, 22

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"SH: idr_pic_id"
	.size	.L.str.6, 15

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"SH: pic_order_cnt_lsb"
	.size	.L.str.7, 22

	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"SH: delta_pic_order_cnt_bottom"
	.size	.L.str.8, 31

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"SH: delta_pic_order_cnt[0]"
	.size	.L.str.9, 27

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"SH: delta_pic_order_cnt[1]"
	.size	.L.str.10, 27

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"SH: redundant_pic_cnt"
	.size	.L.str.11, 22

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"SH: direct_spatial_mv_pred_flag"
	.size	.L.str.12, 32

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"SH: num_ref_idx_override_flag"
	.size	.L.str.13, 30

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"SH: num_ref_idx_l0_active_minus1"
	.size	.L.str.14, 33

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"SH: num_ref_idx_l1_active_minus1"
	.size	.L.str.15, 33

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"SH: cabac_init_idc"
	.size	.L.str.16, 19

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"SH: slice_qp_delta"
	.size	.L.str.17, 19

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"slice_qp_delta makes slice_qp_y out of range"
	.size	.L.str.18, 45

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"SH: sp_for_switch_flag"
	.size	.L.str.19, 23

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"SH: slice_qs_delta"
	.size	.L.str.20, 19

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"slice_qs_delta makes slice_qs_y out of range"
	.size	.L.str.21, 45

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"SH: disable_deblocking_filter_idc"
	.size	.L.str.22, 34

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"SH: slice_alpha_c0_offset_div2"
	.size	.L.str.23, 31

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"SH: slice_beta_offset_div2"
	.size	.L.str.24, 27

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"SH: slice_group_change_cycle"
	.size	.L.str.25, 29

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"SH: no_output_of_prior_pics_flag"
	.size	.L.str.26, 33

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"SH: long_term_reference_flag"
	.size	.L.str.27, 29

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"SH: adaptive_ref_pic_buffering_flag"
	.size	.L.str.28, 36

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"SH: memory_management_control_operation"
	.size	.L.str.29, 40

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"SH: difference_of_pic_nums_minus1"
	.size	.L.str.30, 34

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"SH: long_term_pic_num"
	.size	.L.str.31, 22

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"SH: long_term_frame_idx"
	.size	.L.str.32, 24

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"SH: max_long_term_pic_idx_plus1"
	.size	.L.str.33, 32

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"frame_num not equal to zero in IDR picture"
	.size	.L.str.34, 43

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"toppoc                                %d\n"
	.size	.L.str.36, 42

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"bottompoc                             %d\n"
	.size	.L.str.37, 42

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"frame_num                             %d\n"
	.size	.L.str.38, 42

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"field_pic_flag                        %d\n"
	.size	.L.str.39, 42

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"bottom_field_flag                     %d\n"
	.size	.L.str.40, 42

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"log2_max_frame_num_minus4             %d\n"
	.size	.L.str.42, 42

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"log2_max_pic_order_cnt_lsb_minus4     %d\n"
	.size	.L.str.43, 42

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"pic_order_cnt_type                    %d\n"
	.size	.L.str.44, 42

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"num_ref_frames_in_pic_order_cnt_cycle %d\n"
	.size	.L.str.45, 42

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"delta_pic_order_always_zero_flag      %d\n"
	.size	.L.str.46, 42

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"offset_for_non_ref_pic                %d\n"
	.size	.L.str.47, 42

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"offset_for_top_to_bottom_field        %d\n"
	.size	.L.str.48, 42

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"offset_for_ref_frame[0]               %d\n"
	.size	.L.str.49, 42

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"offset_for_ref_frame[1]               %d\n"
	.size	.L.str.50, 42

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"pic_order_present_flag                %d\n"
	.size	.L.str.52, 42

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"delta_pic_order_cnt[0]                %d\n"
	.size	.L.str.53, 42

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"delta_pic_order_cnt[1]                %d\n"
	.size	.L.str.54, 42

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"delta_pic_order_cnt[2]                %d\n"
	.size	.L.str.55, 42

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"idr_flag                              %d\n"
	.size	.L.str.56, 42

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"MaxFrameNum                           %d\n"
	.size	.L.str.57, 42

	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8
	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"SH: ref_pic_list_reordering_flag_l0"
	.size	.L.str.58, 36

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"SH: reordering_of_pic_nums_idc_l0"
	.size	.L.str.59, 34

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"SH: abs_diff_pic_num_minus1_l0"
	.size	.L.str.60, 31

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"SH: long_term_pic_idx_l0"
	.size	.L.str.61, 25

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"SH: ref_pic_list_reordering_flag_l1"
	.size	.L.str.62, 36

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"SH: reordering_of_pic_nums_idc_l1"
	.size	.L.str.63, 34

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"SH: abs_diff_pic_num_minus1_l1"
	.size	.L.str.64, 31

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"SH: long_term_pic_idx_l1"
	.size	.L.str.65, 25

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"SH: luma_log2_weight_denom"
	.size	.L.str.66, 27

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"SH: chroma_log2_weight_denom"
	.size	.L.str.67, 29

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"SH: luma_weight_flag_l0"
	.size	.L.str.68, 24

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"SH: luma_weight_l0"
	.size	.L.str.69, 19

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"SH: luma_offset_l0"
	.size	.L.str.70, 19

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"SH: chroma_weight_flag_l0"
	.size	.L.str.71, 26

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"SH: chroma_weight_l0"
	.size	.L.str.72, 21

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"SH: chroma_offset_l0"
	.size	.L.str.73, 21

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"SH: luma_weight_flag_l1"
	.size	.L.str.74, 24

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"SH: luma_weight_l1"
	.size	.L.str.75, 19

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"SH: luma_offset_l1"
	.size	.L.str.76, 19

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"SH: chroma_weight_flag_l1"
	.size	.L.str.77, 26

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"SH: chroma_weight_l1"
	.size	.L.str.78, 21

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"SH: chroma_offset_l1"
	.size	.L.str.79, 21

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"\nPOC locals..."
	.size	.Lstr, 15

	.type	.Lstr.1,@object         # @str.1
.Lstr.1:
	.asciz	"POC SPS"
	.size	.Lstr.1, 8

	.type	.Lstr.2,@object         # @str.2
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.2:
	.asciz	"POC in SLice Header"
	.size	.Lstr.2, 20


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
