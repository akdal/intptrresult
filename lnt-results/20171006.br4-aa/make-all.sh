branch=br4-aa
date=1006
for i in 2 3 4 ; do
python2 ../../lnt/parse.py \
  /home/juneyoung.lee/inttoptr/remote-result/freeze${i}/lntresult.${date}.${branch}/mysandbox-copyforrsync/mysandbox cc${i}.csv rt${i}.csv
echo "-----"

for k in cc rt ; do
python2 ../../lnt/columns-get.py ../20171001.base-5.0.insts-opt-noprov-5.0/${k}${i}.csv 0 4 ${k}${i}.csv 1 4 ${k}${i}.${branch}.csv
done

for k in cc rt ; do
  for j in 1 5 ;  do
    python2 ../../lnt/sort.py ${k}${i}.${branch}.csv ${k}${i}.${branch}.sorted.${j}per.csv ${j}
  done
  python2 ../../lnt/filter.py ${k}${i}.${branch}.csv ./machine${i}/benchasmdiffs.txt ${k}${i}.${branch}.filtered.csv
  python2 ../../lnt/sort.py ${k}${i}.${branch}.filtered.csv ${k}${i}.${branch}.filtered.sorted.5per.csv 5
done
done

python2 ../../lnt/maketable.py . rt 2,3,4 ${branch} filtered.sorted.5per runtime.csv
python2 ../../lnt/maketable.py . cc 2,3,4 ${branch} filtered.sorted.5per compilationtime.csv
