	.text
	.file	"jidctfst.bc"
	.globl	jpeg_idct_ifast
	.p2align	4, 0x90
	.type	jpeg_idct_ifast,@function
jpeg_idct_ifast:                        # @jpeg_idct_ifast
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 224
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r8d, -128(%rsp)        # 4-byte Spill
	movq	%rcx, -112(%rsp)        # 8-byte Spill
	movq	408(%rdi), %rax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	movq	88(%rsi), %r15
	movl	$9, %r12d
	leaq	-96(%rsp), %rax
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movswl	16(%rdx), %r8d
	movswl	32(%rdx), %ebp
	movl	%ebp, %ecx
	orl	%r8d, %ecx
	movswl	48(%rdx), %r13d
	movswl	64(%rdx), %ebx
	movl	%r13d, %edi
	orl	%ebx, %edi
	orl	%ecx, %edi
	movswl	80(%rdx), %r14d
	movswl	96(%rdx), %ecx
	movl	%r14d, %esi
	orl	%ecx, %esi
	orl	%edi, %esi
	movswl	112(%rdx), %r9d
	movl	%r9d, %edi
	orw	%si, %di
	je	.LBB0_2
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	movswl	(%rdx), %r10d
	imull	(%r15), %r10d
	imull	64(%r15), %ebp
	imull	128(%r15), %ebx
	imull	192(%r15), %ecx
	leal	(%rbx,%r10), %r11d
	subl	%ebx, %r10d
	leal	(%rcx,%rbp), %esi
	subl	%ecx, %ebp
	movslq	%ebp, %rcx
	imulq	$362, %rcx, %rcx        # imm = 0x16A
	shrq	$8, %rcx
	subl	%esi, %ecx
	movl	%r12d, -124(%rsp)       # 4-byte Spill
	leal	(%rsi,%r11), %r12d
	subl	%esi, %r11d
	leal	(%rcx,%r10), %esi
	movq	%rsi, -104(%rsp)        # 8-byte Spill
	subl	%ecx, %r10d
	imull	32(%r15), %r8d
	imull	96(%r15), %r13d
	imull	160(%r15), %r14d
	imull	224(%r15), %r9d
	leal	(%r14,%r13), %ecx
	subl	%r13d, %r14d
	leal	(%r9,%r8), %esi
	subl	%r9d, %r8d
	leal	(%rsi,%rcx), %r9d
	subl	%ecx, %esi
	movslq	%esi, %rcx
	imulq	$362, %rcx, %rbp        # imm = 0x16A
	shrq	$8, %rbp
	leal	(%r8,%r14), %ecx
	movslq	%ecx, %rcx
	imulq	$473, %rcx, %rcx        # imm = 0x1D9
	shrq	$8, %rcx
	movslq	%r8d, %rsi
	imulq	$277, %rsi, %rbx        # imm = 0x115
	shrq	$8, %rbx
	subl	%ecx, %ebx
	movslq	%r14d, %rsi
	imulq	$-669, %rsi, %rdi       # imm = 0xFD63
	shrq	$8, %rdi
	subl	%r9d, %edi
	addl	%ecx, %edi
	subl	%edi, %ebp
	addl	%ebp, %ebx
	leal	(%r9,%r12), %ecx
	movl	%ecx, (%rax)
	subl	%r9d, %r12d
	movl	%r12d, 224(%rax)
	movl	-124(%rsp), %r12d       # 4-byte Reload
	movq	-104(%rsp), %rcx        # 8-byte Reload
	leal	(%rdi,%rcx), %esi
	subl	%edi, %ecx
	movq	%rcx, %rdi
	leal	(%rbp,%r10), %ecx
	subl	%ebp, %r10d
	leal	(%rbx,%r11), %ebp
	subl	%ebx, %r11d
	movl	$24, %ebx
	jmp	.LBB0_4
	.p2align	4, 0x90
.LBB0_2:                                #   in Loop: Header=BB0_1 Depth=1
	movswl	(%rdx), %esi
	imull	(%r15), %esi
	movl	%esi, (%rax)
	movl	%esi, 96(%rax)
	movl	%esi, %edi
	movl	%esi, %ecx
	movl	%esi, %r10d
	movl	%esi, %ebp
	movl	$56, %ebx
	movl	%esi, %r11d
.LBB0_4:                                #   in Loop: Header=BB0_1 Depth=1
	movl	%esi, 32(%rax)
	movl	%edi, 192(%rax)
	movl	%ecx, 64(%rax)
	movl	%r10d, 160(%rax)
	movl	%ebp, 128(%rax)
	movl	%r11d, (%rax,%rbx,4)
	addq	$4, %rax
	addq	$4, %r15
	addq	$2, %rdx
	decl	%r12d
	cmpl	$1, %r12d
	jg	.LBB0_1
# BB#5:                                 # %.preheader
	movl	-128(%rsp), %r9d        # 4-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_6:                                # =>This Inner Loop Header: Depth=1
	movq	-112(%rsp), %rax        # 8-byte Reload
	movq	(%rax,%rsi), %r10
	addq	%r9, %r10
	movl	-92(%rsp,%rsi,4), %r15d
	movl	-88(%rsp,%rsi,4), %eax
	movl	%eax, %edx
	orl	%r15d, %edx
	movl	-84(%rsp,%rsi,4), %r14d
	movl	-80(%rsp,%rsi,4), %ecx
	movl	%r14d, %ebp
	orl	%ecx, %ebp
	orl	%edx, %ebp
	movl	-76(%rsp,%rsi,4), %r8d
	movl	-72(%rsp,%rsi,4), %ebx
	movl	%r8d, %edi
	orl	%ebx, %edi
	orl	%ebp, %edi
	movl	-68(%rsp,%rsi,4), %ebp
	movl	-96(%rsp,%rsi,4), %edx
	orl	%ebp, %edi
	je	.LBB0_7
# BB#8:                                 #   in Loop: Header=BB0_6 Depth=1
	leal	(%rdx,%rcx), %r11d
	subl	%ecx, %edx
	leal	(%rbx,%rax), %ecx
	subl	%ebx, %eax
	cltq
	imulq	$362, %rax, %rdi        # imm = 0x16A
	shrq	$8, %rdi
	subl	%ecx, %edi
	leal	(%r11,%rcx), %eax
	subl	%ecx, %r11d
	leal	(%rdi,%rdx), %ebx
	subl	%edi, %edx
	leal	(%r8,%r14), %ecx
	subl	%r14d, %r8d
	leal	(%rbp,%r15), %edi
	subl	%ebp, %r15d
	leal	(%rdi,%rcx), %r12d
	subl	%ecx, %edi
	movslq	%edi, %rcx
	imulq	$362, %rcx, %r14        # imm = 0x16A
	shrq	$8, %r14
	leal	(%r15,%r8), %ecx
	movslq	%ecx, %rcx
	imulq	$473, %rcx, %rcx        # imm = 0x1D9
	shrq	$8, %rcx
	movslq	%r15d, %rdi
	imulq	$277, %rdi, %r15        # imm = 0x115
	shrq	$8, %r15
	subl	%ecx, %r15d
	movslq	%r8d, %rdi
	imulq	$-669, %rdi, %rdi       # imm = 0xFD63
	shrq	$8, %rdi
	subl	%r12d, %edi
	addl	%ecx, %edi
	subl	%edi, %r14d
	addl	%r14d, %r15d
	leal	(%rax,%r12), %ecx
	shrl	$5, %ecx
	andl	$1023, %ecx             # imm = 0x3FF
	movq	-120(%rsp), %rbp        # 8-byte Reload
	movzbl	128(%rbp,%rcx), %ecx
	movb	%cl, (%r10)
	subl	%r12d, %eax
	shrl	$5, %eax
	andl	$1023, %eax             # imm = 0x3FF
	movzbl	128(%rbp,%rax), %eax
	movb	%al, 7(%r10)
	leal	(%rdi,%rbx), %eax
	shrl	$5, %eax
	andl	$1023, %eax             # imm = 0x3FF
	movzbl	128(%rbp,%rax), %eax
	movb	%al, 1(%r10)
	subl	%edi, %ebx
	shrl	$5, %ebx
	andl	$1023, %ebx             # imm = 0x3FF
	movzbl	128(%rbp,%rbx), %eax
	movb	%al, 6(%r10)
	leal	(%r14,%rdx), %eax
	shrl	$5, %eax
	andl	$1023, %eax             # imm = 0x3FF
	movzbl	128(%rbp,%rax), %eax
	movb	%al, 2(%r10)
	subl	%r14d, %edx
	shrl	$5, %edx
	andl	$1023, %edx             # imm = 0x3FF
	movzbl	128(%rbp,%rdx), %eax
	movb	%al, 5(%r10)
	leal	(%r15,%r11), %eax
	shrl	$5, %eax
	andl	$1023, %eax             # imm = 0x3FF
	movzbl	128(%rbp,%rax), %eax
	movb	%al, 4(%r10)
	subl	%r15d, %r11d
	shrl	$5, %r11d
	andl	$1023, %r11d            # imm = 0x3FF
	movb	128(%rbp,%r11), %al
	movl	$3, %ecx
	jmp	.LBB0_9
	.p2align	4, 0x90
.LBB0_7:                                #   in Loop: Header=BB0_6 Depth=1
	shrl	$5, %edx
	andl	$1023, %edx             # imm = 0x3FF
	movq	-120(%rsp), %rax        # 8-byte Reload
	movzbl	128(%rax,%rdx), %eax
	movb	%al, (%r10)
	imull	$16843009, %eax, %ecx   # imm = 0x1010101
	movw	%cx, 5(%r10)
	movl	%ecx, 1(%r10)
	movl	$7, %ecx
.LBB0_9:                                #   in Loop: Header=BB0_6 Depth=1
	movb	%al, (%r10,%rcx)
	addq	$8, %rsi
	cmpq	$64, %rsi
	jne	.LBB0_6
# BB#10:
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	jpeg_idct_ifast, .Lfunc_end0-jpeg_idct_ifast
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
