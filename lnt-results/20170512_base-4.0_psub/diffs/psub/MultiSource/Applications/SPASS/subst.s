	.text
	.file	"subst.bc"
	.globl	subst_Add
	.p2align	4, 0x90
	.type	subst_Add,@function
subst_Add:                              # @subst_Add
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movl	%edi, %ebp
	movl	$24, %edi
	callq	memory_Malloc
	movq	%rbx, (%rax)
	movl	%ebp, 8(%rax)
	movq	%r14, 16(%rax)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	subst_Add, .Lfunc_end0-subst_Add
	.cfi_endproc

	.globl	subst_Delete
	.p2align	4, 0x90
	.type	subst_Delete,@function
subst_Delete:                           # @subst_Delete
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB1_4
	.p2align	4, 0x90
.LBB1_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %r14
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_3
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	callq	term_Delete
.LBB1_3:                                #   in Loop: Header=BB1_1 Depth=1
	movq	memory_ARRAY+192(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+192(%rip), %rax
	movq	%rbx, (%rax)
	testq	%r14, %r14
	movq	%r14, %rbx
	jne	.LBB1_1
.LBB1_4:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	subst_Delete, .Lfunc_end1-subst_Delete
	.cfi_endproc

	.globl	subst_Free
	.p2align	4, 0x90
	.type	subst_Free,@function
subst_Free:                             # @subst_Free
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB2_2
	.p2align	4, 0x90
.LBB2_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rax
	movq	memory_ARRAY+192(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY+192(%rip), %rcx
	movq	%rdi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rdi
	jne	.LBB2_1
.LBB2_2:                                # %._crit_edge
	retq
.Lfunc_end2:
	.size	subst_Free, .Lfunc_end2-subst_Free
	.cfi_endproc

	.globl	subst_Term
	.p2align	4, 0x90
	.type	subst_Term,@function
subst_Term:                             # @subst_Term
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testq	%rsi, %rsi
	jne	.LBB3_2
	jmp	.LBB3_4
	.p2align	4, 0x90
.LBB3_5:                                #   in Loop: Header=BB3_2 Depth=1
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB3_4
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%edi, 8(%rsi)
	jne	.LBB3_5
# BB#3:
	movq	16(%rsi), %rax
.LBB3_4:                                # %.loopexit
	retq
.Lfunc_end3:
	.size	subst_Term, .Lfunc_end3-subst_Term
	.cfi_endproc

	.globl	subst_Apply
	.p2align	4, 0x90
	.type	subst_Apply,@function
subst_Apply:                            # @subst_Apply
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB4_1
# BB#2:
	jmp	subst_ApplyIntern       # TAILCALL
.LBB4_1:
	movq	%rsi, %rax
	retq
.Lfunc_end4:
	.size	subst_Apply, .Lfunc_end4-subst_Apply
	.cfi_endproc

	.p2align	4, 0x90
	.type	subst_ApplyIntern,@function
subst_ApplyIntern:                      # @subst_ApplyIntern
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -32
.Lcfi15:
	.cfi_offset %r14, -24
.Lcfi16:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB5_5
# BB#1:
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.LBB5_5
# BB#2:                                 # %.lr.ph.i.preheader
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%eax, 8(%rcx)
	je	.LBB5_8
# BB#4:                                 #   in Loop: Header=BB5_3 Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB5_3
	jmp	.LBB5_5
.LBB5_8:                                # %subst_Term.exit
	movq	16(%rcx), %rbx
	testq	%rbx, %rbx
	je	.LBB5_5
# BB#9:
	movq	16(%rbx), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, %r15
	movl	(%rbx), %eax
	movl	%eax, (%r14)
	movq	16(%r14), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	%r15, 16(%r14)
	jmp	.LBB5_10
.LBB5_5:                                # %subst_Term.exit.thread
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB5_10
	.p2align	4, 0x90
.LBB5_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	callq	subst_ApplyIntern
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB5_7
.LBB5_10:                               # %.loopexit
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	subst_ApplyIntern, .Lfunc_end5-subst_ApplyIntern
	.cfi_endproc

	.globl	subst_Merge
	.p2align	4, 0x90
	.type	subst_Merge,@function
subst_Merge:                            # @subst_Merge
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi20:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 64
.Lcfi24:
	.cfi_offset %rbx, -56
.Lcfi25:
	.cfi_offset %r12, -48
.Lcfi26:
	.cfi_offset %r13, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	testq	%r13, %r13
	je	.LBB6_8
# BB#1:                                 # %.preheader.preheader
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB6_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_4 Depth 2
	testq	%r14, %r14
	je	.LBB6_6
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB6_2 Depth=1
	xorl	%ebp, %ebp
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB6_4:                                #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	8(%r13), %edi
	movq	16(%r13), %rsi
	leaq	16(%rbx), %rdx
	callq	term_SubstituteVariable
	testl	%eax, %eax
	cmovnel	%r12d, %ebp
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB6_4
# BB#5:                                 # %._crit_edge
                                        #   in Loop: Header=BB6_2 Depth=1
	testl	%ebp, %ebp
	jne	.LBB6_7
.LBB6_6:                                # %._crit_edge.thread
                                        #   in Loop: Header=BB6_2 Depth=1
	movl	8(%r13), %ebx
	movq	16(%r13), %rdi
	callq	term_Copy
	movq	%rax, %r15
	movl	$24, %edi
	callq	memory_Malloc
	movq	%r14, (%rax)
	movl	%ebx, 8(%rax)
	movq	%r15, 16(%rax)
	movq	%rax, %r14
.LBB6_7:                                #   in Loop: Header=BB6_2 Depth=1
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB6_2
.LBB6_8:                                # %._crit_edge23
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	subst_Merge, .Lfunc_end6-subst_Merge
	.cfi_endproc

	.globl	subst_Compose
	.p2align	4, 0x90
	.type	subst_Compose,@function
subst_Compose:                          # @subst_Compose
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi36:
	.cfi_def_cfa_offset 64
.Lcfi37:
	.cfi_offset %rbx, -56
.Lcfi38:
	.cfi_offset %r12, -48
.Lcfi39:
	.cfi_offset %r13, -40
.Lcfi40:
	.cfi_offset %r14, -32
.Lcfi41:
	.cfi_offset %r15, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	testq	%r13, %r13
	je	.LBB7_1
# BB#2:                                 # %.preheader.lr.ph
	testq	%r14, %r14
	je	.LBB7_11
# BB#3:                                 # %.preheader.preheader
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB7_4:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_5 Depth 2
                                        #     Child Loop BB7_7 Depth 2
	movl	8(%r13), %ebp
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB7_5:                                #   Parent Loop BB7_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r13), %rsi
	leaq	16(%rbx), %rdx
	movl	%ebp, %edi
	callq	term_SubstituteVariable
	movq	(%rbx), %rbx
	movl	8(%r13), %ebp
	testq	%rbx, %rbx
	jne	.LBB7_5
# BB#6:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB7_7:                                # %.lr.ph.i
                                        #   Parent Loop BB7_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ebp, 8(%rax)
	je	.LBB7_10
# BB#8:                                 #   in Loop: Header=BB7_7 Depth=2
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB7_7
# BB#9:                                 # %.loopexit
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	16(%r13), %rdi
	callq	term_Copy
	movq	%rax, %r12
	movl	$24, %edi
	callq	memory_Malloc
	movq	%r15, (%rax)
	movl	%ebp, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, %r15
.LBB7_10:                               # %subst_BindVar.exit
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB7_4
	jmp	.LBB7_13
.LBB7_1:
	xorl	%r15d, %r15d
	testq	%r14, %r14
	jne	.LBB7_15
	jmp	.LBB7_14
.LBB7_11:                               # %subst_BindVar.exit.us.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB7_12:                               # %subst_BindVar.exit.us
                                        # =>This Inner Loop Header: Depth=1
	movl	8(%r13), %ebx
	movq	16(%r13), %rdi
	callq	term_Copy
	movq	%rax, %r12
	movl	$24, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbp, (%r15)
	movl	%ebx, 8(%r15)
	movq	%r12, 16(%r15)
	movq	(%r13), %r13
	testq	%r13, %r13
	movq	%r15, %rbp
	jne	.LBB7_12
.LBB7_13:                               # %._crit_edge33
	testq	%r14, %r14
	je	.LBB7_14
.LBB7_15:
	testq	%r15, %r15
	je	.LBB7_19
# BB#16:                                # %.preheader.i.preheader
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB7_17:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB7_17
# BB#18:
	movq	%r15, (%rax)
	jmp	.LBB7_19
.LBB7_14:
	movq	%r15, %r14
.LBB7_19:                               # %subst_NUnion.exit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	subst_Compose, .Lfunc_end7-subst_Compose
	.cfi_endproc

	.globl	subst_BindVar
	.p2align	4, 0x90
	.type	subst_BindVar,@function
subst_BindVar:                          # @subst_BindVar
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testq	%rsi, %rsi
	jne	.LBB8_2
	jmp	.LBB8_4
	.p2align	4, 0x90
.LBB8_5:                                #   in Loop: Header=BB8_2 Depth=1
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB8_4
.LBB8_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%edi, 8(%rsi)
	jne	.LBB8_5
# BB#3:
	movl	$1, %eax
.LBB8_4:                                # %._crit_edge
	retq
.Lfunc_end8:
	.size	subst_BindVar, .Lfunc_end8-subst_BindVar
	.cfi_endproc

	.globl	subst_Copy
	.p2align	4, 0x90
	.type	subst_Copy,@function
subst_Copy:                             # @subst_Copy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 48
.Lcfi48:
	.cfi_offset %rbx, -48
.Lcfi49:
	.cfi_offset %r12, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	je	.LBB9_6
# BB#1:                                 # %.lr.ph.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB9_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	8(%rbx), %ebp
	movq	16(%rbx), %rdi
	callq	term_Copy
	movq	%rax, %r15
	movl	$24, %edi
	callq	memory_Malloc
	testq	%r14, %r14
	movq	$0, (%rax)
	movl	%ebp, 8(%rax)
	movq	%r15, 16(%rax)
	je	.LBB9_3
# BB#4:                                 #   in Loop: Header=BB9_2 Depth=1
	movq	%rax, (%r12)
	jmp	.LBB9_5
	.p2align	4, 0x90
.LBB9_3:                                #   in Loop: Header=BB9_2 Depth=1
	movq	%rax, %r14
.LBB9_5:                                #   in Loop: Header=BB9_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %r12
	jne	.LBB9_2
.LBB9_6:                                # %._crit_edge
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	subst_Copy, .Lfunc_end9-subst_Copy
	.cfi_endproc

	.globl	subst_MatchTops
	.p2align	4, 0x90
	.type	subst_MatchTops,@function
subst_MatchTops:                        # @subst_MatchTops
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testq	%rsi, %rsi
	jne	.LBB10_2
	jmp	.LBB10_5
	.p2align	4, 0x90
.LBB10_6:                               #   in Loop: Header=BB10_2 Depth=1
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB10_5
.LBB10_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	8(%rsi), %rcx
	shlq	$5, %rcx
	movq	8(%rdi,%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB10_6
# BB#3:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	16(%rsi), %rdx
	movl	(%rcx), %ecx
	cmpl	(%rdx), %ecx
	jne	.LBB10_6
# BB#4:
	movl	$1, %eax
.LBB10_5:                               # %._crit_edge
	retq
.Lfunc_end10:
	.size	subst_MatchTops, .Lfunc_end10-subst_MatchTops
	.cfi_endproc

	.globl	subst_Unify
	.p2align	4, 0x90
	.type	subst_Unify,@function
subst_Unify:                            # @subst_Unify
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 32
.Lcfi56:
	.cfi_offset %rbx, -32
.Lcfi57:
	.cfi_offset %r14, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movl	$1, %r14d
	testq	%rbx, %rbx
	jne	.LBB11_2
	jmp	.LBB11_5
	.p2align	4, 0x90
.LBB11_8:                               #   in Loop: Header=BB11_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB11_5
.LBB11_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	8(%rbx), %rsi
	movq	%rsi, %rcx
	shlq	$5, %rcx
	movq	8(%rbp,%rcx), %r8
	movq	16(%rbx), %rax
	testq	%r8, %r8
	je	.LBB11_3
# BB#6:                                 #   in Loop: Header=BB11_2 Depth=1
	movq	16(%rbp,%rcx), %rcx
	movq	%rbp, %rdi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	unify_UnifyAllOC
	testl	%eax, %eax
	jne	.LBB11_8
	jmp	.LBB11_4
	.p2align	4, 0x90
.LBB11_3:                               #   in Loop: Header=BB11_2 Depth=1
	movq	%rbp, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%rbp, %rdx
	movq	%rax, %rcx
	callq	unify_OccurCheck
	testl	%eax, %eax
	jne	.LBB11_4
# BB#7:                                 #   in Loop: Header=BB11_2 Depth=1
	movq	16(%rbx), %rax
	movslq	8(%rbx), %rcx
	shlq	$5, %rcx
	leaq	(%rbp,%rcx), %rdx
	movq	%rdx, cont_CURRENTBINDING(%rip)
	movq	%rax, 8(%rbp,%rcx)
	movq	%rbp, 16(%rbp,%rcx)
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, 24(%rbp,%rcx)
	movq	%rdx, cont_LASTBINDING(%rip)
	incl	cont_BINDINGS(%rip)
	jmp	.LBB11_8
.LBB11_4:
	xorl	%r14d, %r14d
.LBB11_5:                               # %._crit_edge
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end11:
	.size	subst_Unify, .Lfunc_end11-subst_Unify
	.cfi_endproc

	.globl	subst_IsShallow
	.p2align	4, 0x90
	.type	subst_IsShallow,@function
subst_IsShallow:                        # @subst_IsShallow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 32
.Lcfi62:
	.cfi_offset %rbx, -32
.Lcfi63:
	.cfi_offset %r14, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %r14d
	testq	%rbx, %rbx
	jne	.LBB12_2
	jmp	.LBB12_9
	.p2align	4, 0x90
.LBB12_11:                              # %.thread
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB12_9
.LBB12_2:                               # %.lr.ph36
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_6 Depth 2
	movq	16(%rbx), %rbp
	cmpl	$0, (%rbp)
	jg	.LBB12_11
# BB#3:                                 #   in Loop: Header=BB12_2 Depth=1
	movq	%rbp, %rdi
	callq	term_IsGround
	testl	%eax, %eax
	jne	.LBB12_11
# BB#4:                                 #   in Loop: Header=BB12_2 Depth=1
	movq	16(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB12_6
	jmp	.LBB12_11
	.p2align	4, 0x90
.LBB12_10:                              #   in Loop: Header=BB12_6 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB12_11
.LBB12_6:                               # %.lr.ph
                                        #   Parent Loop BB12_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rdi
	cmpl	$0, (%rdi)
	jg	.LBB12_10
# BB#7:                                 #   in Loop: Header=BB12_6 Depth=2
	callq	term_IsGround
	testl	%eax, %eax
	jne	.LBB12_10
# BB#8:
	xorl	%r14d, %r14d
.LBB12_9:                               # %.loopexit
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end12:
	.size	subst_IsShallow, .Lfunc_end12-subst_IsShallow
	.cfi_endproc

	.globl	subst_Match
	.p2align	4, 0x90
	.type	subst_Match,@function
subst_Match:                            # @subst_Match
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 32
.Lcfi68:
	.cfi_offset %rbx, -32
.Lcfi69:
	.cfi_offset %r14, -24
.Lcfi70:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB13_5
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	8(%rbx), %rax
	shlq	$5, %rax
	movq	8(%r14,%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB13_6
# BB#3:                                 #   in Loop: Header=BB13_2 Depth=1
	movq	16(%rbx), %rsi
	movq	%r14, %rdi
	callq	unify_Match
	testl	%eax, %eax
	je	.LBB13_6
# BB#4:                                 #   in Loop: Header=BB13_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB13_2
.LBB13_5:
	movl	$1, %ebp
.LBB13_6:                               # %._crit_edge
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end13:
	.size	subst_Match, .Lfunc_end13-subst_Match
	.cfi_endproc

	.globl	subst_MatchReverse
	.p2align	4, 0x90
	.type	subst_MatchReverse,@function
subst_MatchReverse:                     # @subst_MatchReverse
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 32
.Lcfi74:
	.cfi_offset %rbx, -32
.Lcfi75:
	.cfi_offset %r14, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movl	$1, %r14d
	testq	%rbx, %rbx
	jne	.LBB14_2
	jmp	.LBB14_5
	.p2align	4, 0x90
.LBB14_8:                               #   in Loop: Header=BB14_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB14_5
.LBB14_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	8(%rbx), %rdx
	movq	%rdx, %rax
	shlq	$5, %rax
	movq	8(%rbp,%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB14_3
# BB#6:                                 #   in Loop: Header=BB14_2 Depth=1
	movq	16(%rbx), %rsi
	movq	16(%rbp,%rax), %rdx
	movq	%rbp, %rdi
	callq	unify_MatchReverse
	testl	%eax, %eax
	jne	.LBB14_8
	jmp	.LBB14_4
	.p2align	4, 0x90
.LBB14_3:                               #   in Loop: Header=BB14_2 Depth=1
	addl	$-2001, %edx            # imm = 0xF82F
	cmpl	$999, %edx              # imm = 0x3E7
	ja	.LBB14_4
# BB#7:                                 #   in Loop: Header=BB14_2 Depth=1
	leaq	8(%rbp,%rax), %rcx
	movq	cont_INSTANCECONTEXT(%rip), %rdx
	movq	16(%rbx), %rsi
	leaq	(%rbp,%rax), %rdi
	movq	%rdi, cont_CURRENTBINDING(%rip)
	movq	%rsi, (%rcx)
	movq	%rdx, 16(%rbp,%rax)
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, 24(%rbp,%rax)
	movq	%rdi, cont_LASTBINDING(%rip)
	incl	cont_BINDINGS(%rip)
	jmp	.LBB14_8
.LBB14_4:
	xorl	%r14d, %r14d
.LBB14_5:                               # %._crit_edge
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end14:
	.size	subst_MatchReverse, .Lfunc_end14-subst_MatchReverse
	.cfi_endproc

	.globl	subst_Variation
	.p2align	4, 0x90
	.type	subst_Variation,@function
subst_Variation:                        # @subst_Variation
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 32
.Lcfi80:
	.cfi_offset %rbx, -32
.Lcfi81:
	.cfi_offset %r14, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB15_5
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB15_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	8(%rbx), %rax
	shlq	$5, %rax
	movq	8(%r14,%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB15_6
# BB#3:                                 #   in Loop: Header=BB15_2 Depth=1
	movq	16(%rbx), %rsi
	movq	%r14, %rdi
	callq	unify_Variation
	testl	%eax, %eax
	je	.LBB15_6
# BB#4:                                 #   in Loop: Header=BB15_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB15_2
.LBB15_5:
	movl	$1, %ebp
.LBB15_6:                               # %._crit_edge
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end15:
	.size	subst_Variation, .Lfunc_end15-subst_Variation
	.cfi_endproc

	.globl	subst_ComGen
	.p2align	4, 0x90
	.type	subst_ComGen,@function
subst_ComGen:                           # @subst_ComGen
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi86:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi87:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi89:
	.cfi_def_cfa_offset 96
.Lcfi90:
	.cfi_offset %rbx, -56
.Lcfi91:
	.cfi_offset %r12, -48
.Lcfi92:
	.cfi_offset %r13, -40
.Lcfi93:
	.cfi_offset %r14, -32
.Lcfi94:
	.cfi_offset %r15, -24
.Lcfi95:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	$0, (%rcx)
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	$0, (%rdx)
	xorl	%r13d, %r13d
	movq	%r12, 32(%rsp)          # 8-byte Spill
	jmp	.LBB16_1
.LBB16_6:                               #   in Loop: Header=BB16_1 Depth=1
	movq	%rax, %rdi
	callq	term_Copy
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	%rax, %r13
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	(%rbp), %r15
	movl	$24, %edi
	callq	memory_Malloc
	movq	%r15, (%rax)
	movl	%r14d, 8(%rax)
	movq	%r13, 16(%rax)
	movq	%rax, (%rbp)
	movslq	8(%rbx), %rbp
	movq	%rbp, %rax
	shlq	$5, %rax
	movq	8(%r12,%rax), %rdi
	callq	term_Copy
	movq	%rax, %r14
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	(%r15), %r13
	movl	$24, %edi
	callq	memory_Malloc
	movq	%r13, (%rax)
	movq	24(%rsp), %r13          # 8-byte Reload
	movl	%ebp, 8(%rax)
	movq	%r14, 16(%rax)
	movq	%rax, (%r15)
	jmp	.LBB16_9
	.p2align	4, 0x90
.LBB16_1:                               # =>This Inner Loop Header: Depth=1
	movslq	8(%rbx), %rbp
	movq	%rbp, %rax
	shlq	$5, %rax
	movq	8(%r12,%rax), %rdi
	movq	16(%rbx), %rsi
	testq	%rdi, %rdi
	je	.LBB16_2
# BB#3:                                 #   in Loop: Header=BB16_1 Depth=1
	callq	term_Equal
	testl	%eax, %eax
	movl	8(%rbx), %r14d
	je	.LBB16_5
# BB#4:                                 #   in Loop: Header=BB16_1 Depth=1
	movq	16(%rbx), %rdi
	callq	term_Copy
	jmp	.LBB16_8
	.p2align	4, 0x90
.LBB16_2:                               #   in Loop: Header=BB16_1 Depth=1
	movq	%rsi, %rdi
	callq	term_Copy
	movq	%rax, %r14
	movq	%r13, %r15
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	(%r12), %r13
	movl	$24, %edi
	callq	memory_Malloc
	movq	%r13, (%rax)
	movq	%r15, %r13
	movl	%ebp, 8(%rax)
	movq	%r14, 16(%rax)
	movq	%rax, (%r12)
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB16_9
	.p2align	4, 0x90
.LBB16_5:                               #   in Loop: Header=BB16_1 Depth=1
	movslq	%r14d, %rax
	shlq	$5, %rax
	movq	8(%r12,%rax), %rdx
	movl	(%rdx), %ecx
	movq	16(%rbx), %rax
	cmpl	(%rax), %ecx
	jne	.LBB16_6
# BB#7:                                 #   in Loop: Header=BB16_1 Depth=1
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, %r8
	callq	unify_ComGenLinear
.LBB16_8:                               #   in Loop: Header=BB16_1 Depth=1
	movq	%rax, %rbp
	movl	$24, %edi
	callq	memory_Malloc
	movq	%r13, (%rax)
	movl	%r14d, 8(%rax)
	movq	%rbp, 16(%rax)
	movq	%rax, %r13
.LBB16_9:                               #   in Loop: Header=BB16_1 Depth=1
	movslq	8(%rbx), %rax
	shlq	$5, %rax
	movq	$0, 8(%r12,%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB16_1
# BB#10:
	movq	%r13, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	subst_ComGen, .Lfunc_end16-subst_ComGen
	.cfi_endproc

	.globl	subst_CloseVariables
	.p2align	4, 0x90
	.type	subst_CloseVariables,@function
subst_CloseVariables:                   # @subst_CloseVariables
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB17_3
	.p2align	4, 0x90
.LBB17_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	8(%rsi), %rax
	shlq	$5, %rax
	movq	$0, 8(%rdi,%rax)
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB17_1
.LBB17_3:                               # %._crit_edge
	retq
.Lfunc_end17:
	.size	subst_CloseVariables, .Lfunc_end17-subst_CloseVariables
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI18_0:
	.zero	16
	.text
	.globl	subst_CloseOpenVariables
	.p2align	4, 0x90
	.type	subst_CloseOpenVariables,@function
subst_CloseOpenVariables:               # @subst_CloseOpenVariables
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 32
.Lcfi99:
	.cfi_offset %rbx, -32
.Lcfi100:
	.cfi_offset %r14, -24
.Lcfi101:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	jmp	.LBB18_1
	.p2align	4, 0x90
.LBB18_4:                               #   in Loop: Header=BB18_1 Depth=1
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rax
	movq	%rax, cont_LASTBINDING(%rip)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	decl	cont_BINDINGS(%rip)
.LBB18_1:                               # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB18_5
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB18_1 Depth=1
	movq	8(%rcx), %rdi
	testq	%rdi, %rdi
	je	.LBB18_4
# BB#3:                                 #   in Loop: Header=BB18_1 Depth=1
	movl	(%rcx), %ebp
	callq	term_Copy
	movq	%rax, %rbx
	movl	$24, %edi
	callq	memory_Malloc
	movq	%r14, (%rax)
	movl	%ebp, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rax, %r14
	jmp	.LBB18_4
.LBB18_5:                               # %._crit_edge
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end18:
	.size	subst_CloseOpenVariables, .Lfunc_end18-subst_CloseOpenVariables
	.cfi_endproc

	.globl	subst_ExtractUnifier
	.p2align	4, 0x90
	.type	subst_ExtractUnifier,@function
subst_ExtractUnifier:                   # @subst_ExtractUnifier
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi102:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi103:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi105:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi106:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi107:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi108:
	.cfi_def_cfa_offset 80
.Lcfi109:
	.cfi_offset %rbx, -56
.Lcfi110:
	.cfi_offset %r12, -48
.Lcfi111:
	.cfi_offset %r13, -40
.Lcfi112:
	.cfi_offset %r14, -32
.Lcfi113:
	.cfi_offset %r15, -24
.Lcfi114:
	.cfi_offset %rbp, -16
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	$0, (%r15)
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	$0, (%rcx)
	movq	cont_LASTBINDING(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB19_2
	jmp	.LBB19_6
	.p2align	4, 0x90
.LBB19_5:                               #   in Loop: Header=BB19_2 Depth=1
	movq	24(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB19_6
.LBB19_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %r14d
	movslq	%r14d, %rax
	shlq	$5, %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax), %rcx
	cmpq	%rbp, %rcx
	movq	%r15, %r13
	je	.LBB19_4
# BB#3:                                 #   in Loop: Header=BB19_2 Depth=1
	addq	(%rsp), %rax            # 8-byte Folded Reload
	cmpq	%rbp, %rax
	movq	8(%rsp), %r13           # 8-byte Reload
	jne	.LBB19_5
.LBB19_4:                               # %.sink.split
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	8(%rbp), %rsi
	movq	16(%rbp), %rdi
	callq	cont_CopyAndApplyBindings
	movq	%rax, %rbx
	movq	(%r13), %r12
	movl	$24, %edi
	callq	memory_Malloc
	movq	%r12, (%rax)
	movl	%r14d, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, (%r13)
	jmp	.LBB19_5
.LBB19_6:                               # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	subst_ExtractUnifier, .Lfunc_end19-subst_ExtractUnifier
	.cfi_endproc

	.globl	subst_ExtractUnifierCom
	.p2align	4, 0x90
	.type	subst_ExtractUnifierCom,@function
subst_ExtractUnifierCom:                # @subst_ExtractUnifierCom
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi117:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi118:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi119:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi120:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi121:
	.cfi_def_cfa_offset 64
.Lcfi122:
	.cfi_offset %rbx, -56
.Lcfi123:
	.cfi_offset %r12, -48
.Lcfi124:
	.cfi_offset %r13, -40
.Lcfi125:
	.cfi_offset %r14, -32
.Lcfi126:
	.cfi_offset %r15, -24
.Lcfi127:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	$0, (%r15)
	movq	cont_LASTBINDING(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB20_3
	.p2align	4, 0x90
.LBB20_1:                               # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %r13d
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	cont_CopyAndApplyBindingsCom
	movq	%rax, %r12
	movq	(%r15), %rbp
	movl	$24, %edi
	callq	memory_Malloc
	movq	%rbp, (%rax)
	movl	%r13d, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, (%r15)
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB20_1
.LBB20_3:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	subst_ExtractUnifierCom, .Lfunc_end20-subst_ExtractUnifierCom
	.cfi_endproc

	.globl	subst_ExtractMatcher
	.p2align	4, 0x90
	.type	subst_ExtractMatcher,@function
subst_ExtractMatcher:                   # @subst_ExtractMatcher
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi128:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi129:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi130:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi131:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi132:
	.cfi_def_cfa_offset 48
.Lcfi133:
	.cfi_offset %rbx, -40
.Lcfi134:
	.cfi_offset %r14, -32
.Lcfi135:
	.cfi_offset %r15, -24
.Lcfi136:
	.cfi_offset %rbp, -16
	movq	cont_LASTBINDING(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB21_1
# BB#2:                                 # %.lr.ph.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB21_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ebp
	movq	8(%rbx), %r15
	movl	$24, %edi
	callq	memory_Malloc
	movq	%r14, (%rax)
	movl	%ebp, 8(%rax)
	movq	%r15, 16(%rax)
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %r14
	jne	.LBB21_3
	jmp	.LBB21_4
.LBB21_1:
	xorl	%eax, %eax
.LBB21_4:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	subst_ExtractMatcher, .Lfunc_end21-subst_ExtractMatcher
	.cfi_endproc

	.globl	subst_Print
	.p2align	4, 0x90
	.type	subst_Print,@function
subst_Print:                            # @subst_Print
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi137:
	.cfi_def_cfa_offset 16
.Lcfi138:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	stdout(%rip), %rcx
	movl	$.L.str, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	testq	%rbx, %rbx
	jne	.LBB22_2
	jmp	.LBB22_6
	.p2align	4, 0x90
.LBB22_5:                               #   in Loop: Header=BB22_2 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB22_6
.LBB22_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	8(%rbx), %edi
	callq	symbol_Print
	cmpq	$0, 16(%rbx)
	je	.LBB22_4
# BB#3:                                 #   in Loop: Header=BB22_2 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
	movq	16(%rbx), %rdi
	callq	term_PrintPrefix
.LBB22_4:                               #   in Loop: Header=BB22_2 Depth=1
	cmpq	$0, (%rbx)
	jne	.LBB22_5
.LBB22_6:                               # %._crit_edge
	movq	stdout(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$2, %esi
	movl	$1, %edx
	popq	%rbx
	jmp	fwrite                  # TAILCALL
.Lfunc_end22:
	.size	subst_Print, .Lfunc_end22-subst_Print
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"{ "
	.size	.L.str, 3

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	" -> "
	.size	.L.str.1, 5

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"; "
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	" }"
	.size	.L.str.3, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
