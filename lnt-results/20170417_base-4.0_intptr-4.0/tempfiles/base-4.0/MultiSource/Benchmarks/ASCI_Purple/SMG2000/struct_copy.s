	.text
	.file	"struct_copy.bc"
	.globl	hypre_StructCopy
	.p2align	4, 0x90
	.type	hypre_StructCopy,@function
hypre_StructCopy:                       # @hypre_StructCopy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 208
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	movq	%rsi, 136(%rsp)         # 8-byte Spill
	movq	8(%rsi), %rax
	movq	8(%rax), %rcx
	cmpl	$0, 8(%rcx)
	jle	.LBB0_20
# BB#1:                                 # %.lr.ph316
	xorl	%esi, %esi
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_6 Depth 2
                                        #       Child Loop BB0_8 Depth 3
                                        #         Child Loop BB0_29 Depth 4
                                        #         Child Loop BB0_12 Depth 4
                                        #         Child Loop BB0_15 Depth 4
	movq	(%rcx), %rcx
	leaq	(,%rsi,8), %rax
	leaq	(%rax,%rax,2), %rbp
	movq	%rcx, %r13
	leaq	(%rcx,%rbp), %rdi
	movq	128(%rsp), %rdx         # 8-byte Reload
	movq	16(%rdx), %rax
	movq	24(%rdx), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	(%rax), %r15
	movq	136(%rsp), %rcx         # 8-byte Reload
	movq	16(%rcx), %rax
	movq	24(%rcx), %rbx
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	(%rax), %r12
	movq	40(%rdx), %rax
	movslq	(%rax,%rsi,4), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	40(%rcx), %rax
	movq	%rsi, 144(%rsp)         # 8-byte Spill
	movslq	(%rax,%rsi,4), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	108(%rsp), %rsi
	callq	hypre_BoxGetSize
	movl	(%r12,%rbp), %esi
	movl	4(%r12,%rbp), %edi
	movl	12(%r12,%rbp), %eax
	movl	%eax, %edx
	subl	%esi, %edx
	incl	%edx
	cmpl	%esi, %eax
	movl	16(%r12,%rbp), %eax
	movl	$0, %ecx
	cmovsl	%ecx, %edx
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movl	%eax, %r9d
	subl	%edi, %r9d
	incl	%r9d
	movl	%edi, (%rsp)            # 4-byte Spill
	cmpl	%edi, %eax
	movl	(%r15,%rbp), %ebx
	movl	4(%r15,%rbp), %edi
	movl	12(%r15,%rbp), %eax
	cmovsl	%ecx, %r9d
	movl	%eax, %edx
	subl	%ebx, %edx
	incl	%edx
	cmpl	%ebx, %eax
	movl	16(%r15,%rbp), %eax
	cmovsl	%ecx, %edx
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movl	%eax, %r14d
	subl	%edi, %r14d
	incl	%r14d
	cmpl	%edi, %eax
	cmovsl	%ecx, %r14d
	movl	108(%rsp), %eax
	movl	112(%rsp), %ecx
	movl	116(%rsp), %edx
	cmpl	%eax, %ecx
	movq	%rax, 48(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	cmovgel	%ecx, %eax
	cmpl	%eax, %edx
	movl	%edx, 20(%rsp)          # 4-byte Spill
	cmovgel	%edx, %eax
	testl	%eax, %eax
	jle	.LBB0_19
# BB#3:                                 # %.lr.ph312
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_19
# BB#4:                                 # %.lr.ph312
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, 72(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_19
# BB#5:                                 # %.preheader278.us.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %rax
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	movl	%edi, 56(%rsp)          # 4-byte Spill
	movl	%esi, %edi
	movl	8(%rax,%rbp), %r8d
	movl	%r8d, %r13d
	subl	8(%r12,%rbp), %r13d
	subl	8(%r15,%rbp), %r8d
	movl	(%rax,%rbp), %r12d
	movl	4(%rax,%rbp), %r15d
	movl	12(%rsp), %r10d         # 4-byte Reload
	movl	%r10d, %ebp
	movq	48(%rsp), %rax          # 8-byte Reload
	subl	%eax, %ebp
	movl	16(%rsp), %r11d         # 4-byte Reload
	movl	%r11d, %edx
	subl	%eax, %edx
	movq	72(%rsp), %rcx          # 8-byte Reload
	leal	-1(%rcx), %esi
	imull	%esi, %ebp
	imull	%esi, %edx
	movl	%r12d, %esi
	subl	%edi, %esi
	movl	%r15d, %edi
	subl	(%rsp), %edi            # 4-byte Folded Reload
	imull	%r9d, %r13d
	addl	%edi, %r13d
	imull	%r11d, %r13d
	addl	%esi, %r13d
	movl	%r13d, 4(%rsp)          # 4-byte Spill
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	leaq	(%rdi,%rsi,8), %rdi
	subl	8(%rsp), %r12d          # 4-byte Folded Reload
	movq	64(%rsp), %r13          # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%r13,8), %rbx
	subl	56(%rsp), %r15d         # 4-byte Folded Reload
	movl	%r14d, %esi
	subl	%ecx, %esi
	imull	%r10d, %esi
	movl	%esi, 96(%rsp)          # 4-byte Spill
	subl	%ecx, %r9d
	imull	%r11d, %r9d
	movl	%r9d, 92(%rsp)          # 4-byte Spill
	addl	%r10d, %ebp
	subl	%eax, %ebp
	movl	%ebp, 104(%rsp)         # 4-byte Spill
	addl	%r11d, %edx
	subl	%eax, %edx
	movl	%edx, 100(%rsp)         # 4-byte Spill
	imull	%r14d, %r8d
	leal	-1(%rax), %eax
	addl	%r15d, %r8d
	movl	%r10d, %edx
	imull	%ecx, %edx
	movl	%edx, 88(%rsp)          # 4-byte Spill
	imull	%r10d, %r8d
	addl	%r12d, %r8d
	movl	%r8d, (%rsp)            # 4-byte Spill
	imull	%ecx, %r11d
	movl	%r11d, 84(%rsp)         # 4-byte Spill
	addq	%rax, %r13
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	8(%rcx,%r13,8), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	24(%rsp), %rcx          # 8-byte Reload
	addq	%rax, %rcx
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	1(%rax), %r14
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax,%rcx,8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r14, %r8
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	andq	%rax, %r8
	leaq	-4(%r8), %rax
	shrq	$2, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_6:                                # %.preheader278.us
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_8 Depth 3
                                        #         Child Loop BB0_29 Depth 4
                                        #         Child Loop BB0_12 Depth 4
                                        #         Child Loop BB0_15 Depth 4
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	movl	100(%rsp), %eax         # 4-byte Reload
	movl	104(%rsp), %ecx         # 4-byte Reload
	jle	.LBB0_18
# BB#7:                                 # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB0_6 Depth=2
	movl	%edx, 8(%rsp)           # 4-byte Spill
	xorl	%r9d, %r9d
	movl	4(%rsp), %ebp           # 4-byte Reload
	movl	(%rsp), %r11d           # 4-byte Reload
	jmp	.LBB0_8
.LBB0_24:                               # %vector.body.preheader
                                        #   in Loop: Header=BB0_8 Depth=3
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_25
# BB#26:                                # %vector.body.prol
                                        #   in Loop: Header=BB0_8 Depth=3
	movups	(%rdi,%r10,8), %xmm0
	movups	16(%rdi,%r10,8), %xmm1
	movups	%xmm0, (%rbx,%r12,8)
	movups	%xmm1, 16(%rbx,%r12,8)
	movl	$4, %eax
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_28
	jmp	.LBB0_30
.LBB0_25:                               #   in Loop: Header=BB0_8 Depth=3
	xorl	%eax, %eax
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB0_30
.LBB0_28:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB0_8 Depth=3
	leaq	(%rdi,%r10,8), %rcx
	leaq	(%rbx,%r12,8), %rsi
	.p2align	4, 0x90
.LBB0_29:                               # %vector.body
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	(%rcx,%rax,8), %xmm0
	movups	16(%rcx,%rax,8), %xmm1
	movups	%xmm0, (%rsi,%rax,8)
	movups	%xmm1, 16(%rsi,%rax,8)
	movups	32(%rcx,%rax,8), %xmm0
	movups	48(%rcx,%rax,8), %xmm1
	movups	%xmm0, 32(%rsi,%rax,8)
	movups	%xmm1, 48(%rsi,%rax,8)
	addq	$8, %rax
	cmpq	%rax, %r8
	jne	.LBB0_29
.LBB0_30:                               # %middle.block
                                        #   in Loop: Header=BB0_8 Depth=3
	cmpq	%r8, %r14
	je	.LBB0_16
# BB#31:                                #   in Loop: Header=BB0_8 Depth=3
	addq	%r8, %r12
	addq	%r8, %r10
	movl	%r8d, %r13d
	jmp	.LBB0_10
	.p2align	4, 0x90
.LBB0_8:                                # %.preheader.us.us
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_29 Depth 4
                                        #         Child Loop BB0_12 Depth 4
                                        #         Child Loop BB0_15 Depth 4
	movslq	%r11d, %r10
	movslq	%ebp, %r12
	cmpq	$3, %r14
	jbe	.LBB0_9
# BB#21:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_8 Depth=3
	testq	%r8, %r8
	je	.LBB0_9
# BB#22:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_8 Depth=3
	movl	16(%rsp), %eax          # 4-byte Reload
	imull	%r9d, %eax
	addl	4(%rsp), %eax           # 4-byte Folded Reload
	cltq
	leaq	(%rbx,%rax,8), %rsi
	movl	12(%rsp), %ecx          # 4-byte Reload
	imull	%r9d, %ecx
	addl	(%rsp), %ecx            # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	cmpq	%rdx, %rsi
	jae	.LBB0_24
# BB#23:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_8 Depth=3
	movq	64(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,8), %rax
	leaq	(%rdi,%rcx,8), %rcx
	cmpq	%rax, %rcx
	jae	.LBB0_24
	.p2align	4, 0x90
.LBB0_9:                                #   in Loop: Header=BB0_8 Depth=3
	xorl	%r13d, %r13d
.LBB0_10:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_8 Depth=3
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r15d
	subl	%r13d, %r15d
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%eax, %esi
	subl	%r13d, %esi
	andl	$3, %r15d
	je	.LBB0_13
# BB#11:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB0_8 Depth=3
	negl	%r15d
	.p2align	4, 0x90
.LBB0_12:                               # %scalar.ph.prol
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rdi,%r10,8), %rax
	movq	%rax, (%rbx,%r12,8)
	incq	%r10
	incq	%r12
	incl	%r13d
	incl	%r15d
	jne	.LBB0_12
.LBB0_13:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB0_8 Depth=3
	cmpl	$3, %esi
	jb	.LBB0_16
# BB#14:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB0_8 Depth=3
	leaq	24(%rbx,%r12,8), %rsi
	leaq	24(%rdi,%r10,8), %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%r13d, %ecx
	.p2align	4, 0x90
.LBB0_15:                               # %scalar.ph
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	-24(%rax), %rdx
	movq	%rdx, -24(%rsi)
	movq	-16(%rax), %rdx
	movq	%rdx, -16(%rsi)
	movq	-8(%rax), %rdx
	movq	%rdx, -8(%rsi)
	movq	(%rax), %rdx
	movq	%rdx, (%rsi)
	addq	$32, %rsi
	addq	$32, %rax
	addl	$-4, %ecx
	jne	.LBB0_15
.LBB0_16:                               # %._crit_edge.us.us
                                        #   in Loop: Header=BB0_8 Depth=3
	addl	12(%rsp), %r11d         # 4-byte Folded Reload
	addl	16(%rsp), %ebp          # 4-byte Folded Reload
	incl	%r9d
	cmpl	72(%rsp), %r9d          # 4-byte Folded Reload
	jne	.LBB0_8
# BB#17:                                #   in Loop: Header=BB0_6 Depth=2
	movl	84(%rsp), %eax          # 4-byte Reload
	movl	88(%rsp), %ecx          # 4-byte Reload
	movl	8(%rsp), %edx           # 4-byte Reload
.LBB0_18:                               # %._crit_edge286.us
                                        #   in Loop: Header=BB0_6 Depth=2
	addl	(%rsp), %ecx            # 4-byte Folded Reload
	addl	4(%rsp), %eax           # 4-byte Folded Reload
	addl	96(%rsp), %ecx          # 4-byte Folded Reload
	addl	92(%rsp), %eax          # 4-byte Folded Reload
	incl	%edx
	cmpl	20(%rsp), %edx          # 4-byte Folded Reload
	movl	%ecx, (%rsp)            # 4-byte Spill
	movl	%eax, 4(%rsp)           # 4-byte Spill
	jne	.LBB0_6
.LBB0_19:                               # %._crit_edge313
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	144(%rsp), %rsi         # 8-byte Reload
	incq	%rsi
	movq	120(%rsp), %rcx         # 8-byte Reload
	movslq	8(%rcx), %rax
	cmpq	%rax, %rsi
	jl	.LBB0_2
.LBB0_20:                               # %._crit_edge317
	xorl	%eax, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_StructCopy, .Lfunc_end0-hypre_StructCopy
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
