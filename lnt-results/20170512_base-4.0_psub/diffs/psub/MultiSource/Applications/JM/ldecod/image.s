	.text
	.file	"image.bc"
	.globl	MbAffPostProc
	.p2align	4, 0x90
	.type	MbAffPostProc,@function
MbAffPostProc:                          # @MbAffPostProc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1128, %rsp             # imm = 0x468
.Lcfi6:
	.cfi_def_cfa_offset 1184
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	dec_picture(%rip), %rbp
	cmpl	$0, 316912(%rbp)
	jle	.LBB0_63
# BB#1:                                 # %.lr.ph91.preheader
	movq	316920(%rbp), %r13
	movq	316928(%rbp), %r15
	leaq	8(%r13), %r14
	leaq	160(%rsp), %r12
	xorl	%ebx, %ebx
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%r15, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph91
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
                                        #     Child Loop BB0_6 Depth 2
                                        #     Child Loop BB0_11 Depth 2
                                        #       Child Loop BB0_19 Depth 3
                                        #       Child Loop BB0_22 Depth 3
                                        #     Child Loop BB0_27 Depth 2
                                        #       Child Loop BB0_31 Depth 3
                                        #     Child Loop BB0_36 Depth 2
                                        #       Child Loop BB0_44 Depth 3
                                        #       Child Loop BB0_47 Depth 3
                                        #     Child Loop BB0_52 Depth 2
                                        #       Child Loop BB0_56 Depth 3
	movq	316936(%rbp), %rax
	cmpb	$0, (%rax,%rbx)
	je	.LBB0_62
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	xorl	%ecx, %ecx
	movl	%ebx, %edi
	leaq	60(%rsp), %rsi
	leaq	56(%rsp), %rdx
	callq	get_mb_pos
	movslq	60(%rsp), %rax
	movslq	56(%rsp), %rcx
	leaq	(%r13,%rcx,8), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_4:                                # %.preheader75
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rsi,8), %rdi
	movzwl	(%rdi,%rax,2), %ebp
	movw	%bp, 96(%rsp,%rsi,2)
	movzwl	2(%rdi,%rax,2), %ebp
	movw	%bp, 160(%rsp,%rsi,2)
	movzwl	4(%rdi,%rax,2), %ebp
	movw	%bp, 224(%rsp,%rsi,2)
	movzwl	6(%rdi,%rax,2), %ebp
	movw	%bp, 288(%rsp,%rsi,2)
	movzwl	8(%rdi,%rax,2), %ebp
	movw	%bp, 352(%rsp,%rsi,2)
	movzwl	10(%rdi,%rax,2), %ebp
	movw	%bp, 416(%rsp,%rsi,2)
	movzwl	12(%rdi,%rax,2), %ebp
	movw	%bp, 480(%rsp,%rsi,2)
	movzwl	14(%rdi,%rax,2), %ebp
	movw	%bp, 544(%rsp,%rsi,2)
	movzwl	16(%rdi,%rax,2), %ebp
	movw	%bp, 608(%rsp,%rsi,2)
	movzwl	18(%rdi,%rax,2), %ebp
	movw	%bp, 672(%rsp,%rsi,2)
	movzwl	20(%rdi,%rax,2), %ebp
	movw	%bp, 736(%rsp,%rsi,2)
	movzwl	22(%rdi,%rax,2), %ebp
	movw	%bp, 800(%rsp,%rsi,2)
	movzwl	24(%rdi,%rax,2), %ebp
	movw	%bp, 864(%rsp,%rsi,2)
	movzwl	26(%rdi,%rax,2), %ebp
	movw	%bp, 928(%rsp,%rsi,2)
	movzwl	28(%rdi,%rax,2), %ebp
	movw	%bp, 992(%rsp,%rsi,2)
	movzwl	30(%rdi,%rax,2), %edi
	movw	%di, 1056(%rsp,%rsi,2)
	incq	%rsi
	cmpq	$32, %rsi
	jne	.LBB0_4
# BB#5:                                 # %.preheader74.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	leaq	(%r14,%rcx,8), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_6:                                # %.preheader74
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rcx,%rdx,8), %rdi
	movq	(%rcx,%rdx,8), %rsi
	movzwl	96(%rsp,%rdx), %ebp
	movw	%bp, (%rdi,%rax,2)
	movzwl	128(%rsp,%rdx), %ebp
	movw	%bp, (%rsi,%rax,2)
	movzwl	160(%rsp,%rdx), %ebp
	movw	%bp, 2(%rdi,%rax,2)
	movzwl	192(%rsp,%rdx), %ebp
	movw	%bp, 2(%rsi,%rax,2)
	movzwl	224(%rsp,%rdx), %ebp
	movw	%bp, 4(%rdi,%rax,2)
	movzwl	256(%rsp,%rdx), %ebp
	movw	%bp, 4(%rsi,%rax,2)
	movzwl	288(%rsp,%rdx), %ebp
	movw	%bp, 6(%rdi,%rax,2)
	movzwl	320(%rsp,%rdx), %ebp
	movw	%bp, 6(%rsi,%rax,2)
	movzwl	352(%rsp,%rdx), %ebp
	movw	%bp, 8(%rdi,%rax,2)
	movzwl	384(%rsp,%rdx), %ebp
	movw	%bp, 8(%rsi,%rax,2)
	movzwl	416(%rsp,%rdx), %ebp
	movw	%bp, 10(%rdi,%rax,2)
	movzwl	448(%rsp,%rdx), %ebp
	movw	%bp, 10(%rsi,%rax,2)
	movzwl	480(%rsp,%rdx), %ebp
	movw	%bp, 12(%rdi,%rax,2)
	movzwl	512(%rsp,%rdx), %ebp
	movw	%bp, 12(%rsi,%rax,2)
	movzwl	544(%rsp,%rdx), %ebp
	movw	%bp, 14(%rdi,%rax,2)
	movzwl	576(%rsp,%rdx), %ebp
	movw	%bp, 14(%rsi,%rax,2)
	movzwl	608(%rsp,%rdx), %ebp
	movw	%bp, 16(%rdi,%rax,2)
	movzwl	640(%rsp,%rdx), %ebp
	movw	%bp, 16(%rsi,%rax,2)
	movzwl	672(%rsp,%rdx), %ebp
	movw	%bp, 18(%rdi,%rax,2)
	movzwl	704(%rsp,%rdx), %ebp
	movw	%bp, 18(%rsi,%rax,2)
	movzwl	736(%rsp,%rdx), %ebp
	movw	%bp, 20(%rdi,%rax,2)
	movzwl	768(%rsp,%rdx), %ebp
	movw	%bp, 20(%rsi,%rax,2)
	movzwl	800(%rsp,%rdx), %ebp
	movw	%bp, 22(%rdi,%rax,2)
	movzwl	832(%rsp,%rdx), %ebp
	movw	%bp, 22(%rsi,%rax,2)
	movzwl	864(%rsp,%rdx), %ebp
	movw	%bp, 24(%rdi,%rax,2)
	movzwl	896(%rsp,%rdx), %ebp
	movw	%bp, 24(%rsi,%rax,2)
	movzwl	928(%rsp,%rdx), %ebp
	movw	%bp, 26(%rdi,%rax,2)
	movzwl	960(%rsp,%rdx), %ebp
	movw	%bp, 26(%rsi,%rax,2)
	movzwl	992(%rsp,%rdx), %ebp
	movw	%bp, 28(%rdi,%rax,2)
	movzwl	1024(%rsp,%rdx), %ebp
	movw	%bp, 28(%rsi,%rax,2)
	movzwl	1056(%rsp,%rdx), %ebp
	movw	%bp, 30(%rdi,%rax,2)
	movzwl	1088(%rsp,%rdx), %edi
	movw	%di, 30(%rsi,%rax,2)
	addq	$2, %rdx
	cmpq	$32, %rdx
	jne	.LBB0_6
# BB#7:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	dec_picture(%rip), %rbp
	cmpl	$0, 317044(%rbp)
	je	.LBB0_62
# BB#8:                                 # %.preheader73
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	60(%rsp), %ecx
	movq	img(%rip), %rdi
	movl	5932(%rdi), %r8d
	movl	$16, %eax
	xorl	%edx, %edx
	idivl	%r8d
	movl	%eax, %esi
	movl	%ecx, %eax
	cltd
	idivl	%esi
	movl	%eax, %r9d
	movl	%r9d, 60(%rsp)
	movl	56(%rsp), %esi
	movslq	5936(%rdi), %rcx
	movl	$16, %eax
	xorl	%edx, %edx
	idivl	%ecx
	movl	%eax, %edi
	movl	%esi, %eax
	cltd
	idivl	%edi
	movq	%rcx, %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	testq	%rcx, %rcx
	movl	%eax, 56(%rsp)
	jle	.LBB0_62
# BB#9:                                 # %.preheader73
                                        #   in Loop: Header=BB0_2 Depth=1
	testl	%r8d, %r8d
	jle	.LBB0_62
# BB#10:                                # %.preheader71.us.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movl	%r8d, 52(%rsp)          # 4-byte Spill
	movslq	%r8d, %r8
	movslq	%r9d, %r13
	movslq	%eax, %r11
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%rax,%rax), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	(%r15), %r9
	leaq	(%r8,%r13), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%r8, %rax
	andq	$-8, %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	96(%rsp), %rdi
	leaq	544(%rsp), %r15
	xorl	%ebx, %ebx
	jmp	.LBB0_11
.LBB0_18:                               # %vector.body146.preheader
                                        #   in Loop: Header=BB0_11 Depth=2
	movq	%r12, %rdx
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	%r15, %rsi
	.p2align	4, 0x90
.LBB0_19:                               # %vector.body146
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	(%rcx), %xmm0
	movd	%xmm0, %r12d
	movw	%r12w, -448(%rsi)
	pextrw	$1, %xmm0, %eax
	movw	%ax, -384(%rsi)
	pextrw	$2, %xmm0, %eax
	movw	%ax, -320(%rsi)
	pextrw	$3, %xmm0, %eax
	movw	%ax, -256(%rsi)
	pextrw	$4, %xmm0, %eax
	movw	%ax, -192(%rsi)
	pextrw	$5, %xmm0, %eax
	movw	%ax, -128(%rsi)
	pextrw	$6, %xmm0, %eax
	movw	%ax, -64(%rsi)
	pextrw	$7, %xmm0, %eax
	movw	%ax, (%rsi)
	addq	$512, %rsi              # imm = 0x200
	addq	$16, %rcx
	addq	$-8, %rbp
	jne	.LBB0_19
# BB#20:                                # %middle.block147
                                        #   in Loop: Header=BB0_11 Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, %r8
	movq	%rax, %rcx
	movq	%rdx, %r12
	je	.LBB0_23
	jmp	.LBB0_21
	.p2align	4, 0x90
.LBB0_11:                               # %.preheader71.us
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_19 Depth 3
                                        #       Child Loop BB0_22 Depth 3
	movq	%r8, %rcx
	shlq	$6, %rcx
	leaq	(%r11,%rbx), %rsi
	cmpl	$8, %r8d
	leaq	96(%rsp,%rcx), %r10
	movq	(%r9,%rsi,8), %r14
	jae	.LBB0_13
# BB#12:                                #   in Loop: Header=BB0_11 Depth=2
	xorl	%ecx, %ecx
	jmp	.LBB0_21
	.p2align	4, 0x90
.LBB0_13:                               # %min.iters.checked150
                                        #   in Loop: Header=BB0_11 Depth=2
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	je	.LBB0_17
# BB#14:                                # %vector.memcheck167
                                        #   in Loop: Header=BB0_11 Depth=2
	leaq	96(%rsp,%rbx,2), %rsi
	leaq	(%r14,%r13,2), %rcx
	movq	72(%rsp), %rax          # 8-byte Reload
	leaq	(%r14,%rax,2), %rbp
	cmpq	%rbp, %rsi
	jae	.LBB0_18
# BB#15:                                # %vector.memcheck167
                                        #   in Loop: Header=BB0_11 Depth=2
	leaq	-62(%r10,%rbx,2), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB0_18
.LBB0_17:                               #   in Loop: Header=BB0_11 Depth=2
	xorl	%ecx, %ecx
.LBB0_21:                               # %scalar.ph148.preheader
                                        #   in Loop: Header=BB0_11 Depth=2
	movq	%rcx, %rsi
	shlq	$6, %rsi
	addq	%rdi, %rsi
	leaq	(%r14,%r13,2), %rbp
	.p2align	4, 0x90
.LBB0_22:                               # %scalar.ph148
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	(%rbp,%rcx,2), %eax
	movw	%ax, (%rsi)
	incq	%rcx
	addq	$64, %rsi
	cmpq	%r8, %rcx
	jl	.LBB0_22
.LBB0_23:                               # %._crit_edge.us
                                        #   in Loop: Header=BB0_11 Depth=2
	incq	%rbx
	addq	$2, %r15
	addq	$2, %rdi
	cmpq	88(%rsp), %rbx          # 8-byte Folded Reload
	jl	.LBB0_11
# BB#24:                                # %.preheader72
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB0_59
# BB#25:                                # %.preheader72
                                        #   in Loop: Header=BB0_2 Depth=1
	testl	%r8d, %r8d
	movq	8(%rsp), %r15           # 8-byte Reload
	jle	.LBB0_60
# BB#26:                                # %.preheader.us.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	(%r15), %r9
	leaq	-1(%r8), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	52(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r15d
	andl	$1, %r15d
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_27:                               # %.preheader.us
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_31 Depth 3
	testq	%r15, %r15
	leaq	(%r11,%rbp,2), %rax
	movq	(%r9,%rax,8), %rbx
	leaq	1(%r11,%rbp,2), %rax
	movq	(%r9,%rax,8), %rcx
	jne	.LBB0_29
# BB#28:                                #   in Loop: Header=BB0_27 Depth=2
	xorl	%edi, %edi
	cmpq	$0, 80(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_30
	jmp	.LBB0_32
	.p2align	4, 0x90
.LBB0_29:                               #   in Loop: Header=BB0_27 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	leal	(%rax,%rbp), %eax
	cltq
	movzwl	96(%rsp,%rbp,2), %esi
	movw	%si, (%rbx,%r13,2)
	movzwl	96(%rsp,%rax,2), %eax
	movw	%ax, (%rcx,%r13,2)
	movl	$1, %edi
	cmpq	$0, 80(%rsp)            # 8-byte Folded Reload
	je	.LBB0_32
.LBB0_30:                               # %.preheader.us.new
                                        #   in Loop: Header=BB0_27 Depth=2
	movslq	%r14d, %rax
	leaq	2(%rcx,%r13,2), %rsi
	leaq	2(%rbx,%r13,2), %rbx
	movq	%rdi, %rcx
	shlq	$6, %rcx
	addq	%r12, %rcx
	.p2align	4, 0x90
.LBB0_31:                               #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_27 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	-64(%rcx,%rbp,2), %edx
	movw	%dx, -2(%rbx,%rdi,2)
	movzwl	-64(%rcx,%rax,2), %edx
	movw	%dx, -2(%rsi,%rdi,2)
	movzwl	(%rcx,%rbp,2), %edx
	movw	%dx, (%rbx,%rdi,2)
	movzwl	(%rcx,%rax,2), %edx
	movw	%dx, (%rsi,%rdi,2)
	addq	$2, %rdi
	subq	$-128, %rcx
	cmpq	%r8, %rdi
	jl	.LBB0_31
.LBB0_32:                               # %._crit_edge85.us
                                        #   in Loop: Header=BB0_27 Depth=2
	incq	%rbp
	incl	%r14d
	cmpq	(%rsp), %rbp            # 8-byte Folded Reload
	jl	.LBB0_27
# BB#33:                                # %._crit_edge87
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB0_58
# BB#34:                                # %._crit_edge87
                                        #   in Loop: Header=BB0_2 Depth=1
	testl	%r8d, %r8d
	jle	.LBB0_58
# BB#35:                                # %.preheader71.us.preheader.1
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	8(%rax), %rbx
	movq	%r8, %r12
	andq	$-8, %r12
	leaq	96(%rsp), %rsi
	leaq	544(%rsp), %rbp
	xorl	%r9d, %r9d
	movl	52(%rsp), %r14d         # 4-byte Reload
	jmp	.LBB0_36
.LBB0_43:                               # %vector.body.preheader
                                        #   in Loop: Header=BB0_36 Depth=2
	movq	%rbx, %rdx
	movq	64(%rsp), %r14          # 8-byte Reload
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB0_44:                               # %vector.body
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_36 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	(%rcx), %xmm0
	movd	%xmm0, %eax
	movw	%ax, -448(%rbx)
	pextrw	$1, %xmm0, %eax
	movw	%ax, -384(%rbx)
	pextrw	$2, %xmm0, %eax
	movw	%ax, -320(%rbx)
	pextrw	$3, %xmm0, %eax
	movw	%ax, -256(%rbx)
	pextrw	$4, %xmm0, %eax
	movw	%ax, -192(%rbx)
	pextrw	$5, %xmm0, %eax
	movw	%ax, -128(%rbx)
	pextrw	$6, %xmm0, %eax
	movw	%ax, -64(%rbx)
	pextrw	$7, %xmm0, %eax
	movw	%ax, (%rbx)
	addq	$512, %rbx              # imm = 0x200
	addq	$16, %rcx
	addq	$-8, %r14
	jne	.LBB0_44
# BB#45:                                # %middle.block
                                        #   in Loop: Header=BB0_36 Depth=2
	movq	%r12, %rax
	cmpq	%rax, %r8
	movq	%rax, %rcx
	movl	52(%rsp), %r14d         # 4-byte Reload
	movq	%rdx, %rbx
	je	.LBB0_48
	jmp	.LBB0_46
	.p2align	4, 0x90
.LBB0_36:                               # %.preheader71.us.1
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_44 Depth 3
                                        #       Child Loop BB0_47 Depth 3
	leaq	(%r11,%r9), %rax
	cmpl	$8, %r14d
	movq	(%rbx,%rax,8), %rdi
	jae	.LBB0_38
# BB#37:                                #   in Loop: Header=BB0_36 Depth=2
	xorl	%ecx, %ecx
	jmp	.LBB0_46
	.p2align	4, 0x90
.LBB0_38:                               # %min.iters.checked
                                        #   in Loop: Header=BB0_36 Depth=2
	testq	%r12, %r12
	je	.LBB0_42
# BB#39:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_36 Depth=2
	leaq	96(%rsp,%r9,2), %rax
	leaq	(%rdi,%r13,2), %rcx
	movq	72(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdi,%rdx,2), %rdx
	cmpq	%rdx, %rax
	jae	.LBB0_43
# BB#40:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_36 Depth=2
	leaq	-62(%r10,%r9,2), %rax
	cmpq	%rax, %rcx
	jae	.LBB0_43
.LBB0_42:                               #   in Loop: Header=BB0_36 Depth=2
	xorl	%ecx, %ecx
.LBB0_46:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_36 Depth=2
	movq	%rcx, %rax
	shlq	$6, %rax
	addq	%rsi, %rax
	leaq	(%rdi,%r13,2), %rdi
	.p2align	4, 0x90
.LBB0_47:                               # %scalar.ph
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_36 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	(%rdi,%rcx,2), %edx
	movw	%dx, (%rax)
	incq	%rcx
	addq	$64, %rax
	cmpq	%r8, %rcx
	jl	.LBB0_47
.LBB0_48:                               # %._crit_edge.us.1
                                        #   in Loop: Header=BB0_36 Depth=2
	incq	%r9
	addq	$2, %rbp
	addq	$2, %rsi
	cmpq	88(%rsp), %r9           # 8-byte Folded Reload
	jl	.LBB0_36
# BB#49:                                # %.preheader72.1
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	leaq	160(%rsp), %r12
	jle	.LBB0_58
# BB#50:                                # %.preheader72.1
                                        #   in Loop: Header=BB0_2 Depth=1
	testl	%r8d, %r8d
	jle	.LBB0_58
# BB#51:                                # %.preheader.us.preheader.1
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	8(%rax), %r14
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, %r10d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_52:                               # %.preheader.us.1
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_56 Depth 3
	testq	%r15, %r15
	leaq	(%r11,%rbp,2), %rax
	movq	(%r14,%rax,8), %r9
	leaq	1(%r11,%rbp,2), %rax
	movq	(%r14,%rax,8), %rax
	jne	.LBB0_54
# BB#53:                                #   in Loop: Header=BB0_52 Depth=2
	xorl	%ebx, %ebx
	cmpq	$0, 80(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_55
	jmp	.LBB0_57
	.p2align	4, 0x90
.LBB0_54:                               #   in Loop: Header=BB0_52 Depth=2
	movq	(%rsp), %rcx            # 8-byte Reload
	leal	(%rcx,%rbp), %ecx
	movslq	%ecx, %rcx
	movzwl	96(%rsp,%rbp,2), %esi
	movw	%si, (%r9,%r13,2)
	movzwl	96(%rsp,%rcx,2), %ecx
	movw	%cx, (%rax,%r13,2)
	movl	$1, %ebx
	cmpq	$0, 80(%rsp)            # 8-byte Folded Reload
	je	.LBB0_57
.LBB0_55:                               # %.preheader.us.1.new
                                        #   in Loop: Header=BB0_52 Depth=2
	movslq	%r10d, %rsi
	movq	%rbx, %rcx
	shlq	$6, %rcx
	addq	%r12, %rcx
	leaq	2(%rax,%r13,2), %rax
	leaq	2(%r9,%r13,2), %rdi
	.p2align	4, 0x90
.LBB0_56:                               #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_52 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	-64(%rcx,%rbp,2), %edx
	movw	%dx, -2(%rdi,%rbx,2)
	movzwl	-64(%rcx,%rsi,2), %edx
	movw	%dx, -2(%rax,%rbx,2)
	movzwl	(%rcx,%rbp,2), %edx
	movw	%dx, (%rdi,%rbx,2)
	movzwl	(%rcx,%rsi,2), %edx
	movw	%dx, (%rax,%rbx,2)
	addq	$2, %rbx
	subq	$-128, %rcx
	cmpq	%r8, %rbx
	jl	.LBB0_56
.LBB0_57:                               # %._crit_edge85.us.1
                                        #   in Loop: Header=BB0_52 Depth=2
	incq	%rbp
	incl	%r10d
	cmpq	(%rsp), %rbp            # 8-byte Folded Reload
	jl	.LBB0_52
.LBB0_58:                               #   in Loop: Header=BB0_2 Depth=1
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %r15           # 8-byte Reload
	jmp	.LBB0_62
.LBB0_59:                               #   in Loop: Header=BB0_2 Depth=1
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	8(%rsp), %r15           # 8-byte Reload
	jmp	.LBB0_61
.LBB0_60:                               #   in Loop: Header=BB0_2 Depth=1
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB0_61:                               # %.loopexit
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_62:                               # %.loopexit
                                        #   in Loop: Header=BB0_2 Depth=1
	addq	$2, %rbx
	movslq	316912(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_2
.LBB0_63:                               # %._crit_edge92
	addq	$1128, %rsp             # imm = 0x468
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	MbAffPostProc, .Lfunc_end0-MbAffPostProc
	.cfi_endproc

	.globl	decode_one_frame
	.p2align	4, 0x90
	.type	decode_one_frame,@function
decode_one_frame:                       # @decode_one_frame
	.cfi_startproc
# BB#0:                                 # %.lr.ph
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	5592(%rbx), %r15
	movl	$0, 12(%rbx)
	movl	$-4711, 4(%rbx)         # imm = 0xED99
	movl	$-8888, 32(%r15)        # imm = 0xDD48
	movl	$0, 8(%rbx)
	movl	$1, 5580(%rbx)
	movl	$Is_primary_correct, %r12d
	.p2align	4, 0x90
.LBB1_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_5 Depth 2
                                        #     Child Loop BB1_9 Depth 2
	callq	read_new_slice
	movl	%eax, %ebp
	movq	img(%rip), %rax
	cmpl	$0, 5652(%rax)
	je	.LBB1_2
# BB#18:                                #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, 44(%rax)
	je	.LBB1_21
# BB#19:                                #   in Loop: Header=BB1_1 Depth=1
	movslq	redundant_slice_ref_idx(%rip), %rax
	cmpl	$0, ref_flag(,%rax,4)
	jne	.LBB1_21
# BB#20:                                #   in Loop: Header=BB1_1 Depth=1
	movl	$0, Is_redundant_correct(%rip)
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_2:                                #   in Loop: Header=BB1_1 Depth=1
	movl	$1, Is_redundant_correct(%rip)
	movl	$1, Is_primary_correct(%rip)
	cmpl	$2, 44(%rax)
	je	.LBB1_21
# BB#3:                                 # %.preheader.i
                                        #   in Loop: Header=BB1_1 Depth=1
	movslq	5640(%rax), %rax
	testq	%rax, %rax
	jle	.LBB1_21
# BB#4:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB1_1 Depth=1
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$3, %rsi
	je	.LBB1_8
	.p2align	4, 0x90
.LBB1_5:                                #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, ref_flag(,%rcx,4)
	jne	.LBB1_7
# BB#6:                                 #   in Loop: Header=BB1_5 Depth=2
	movl	$0, Is_primary_correct(%rip)
.LBB1_7:                                #   in Loop: Header=BB1_5 Depth=2
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB1_5
.LBB1_8:                                # %.prol.loopexit
                                        #   in Loop: Header=BB1_1 Depth=1
	cmpq	$3, %rdx
	jb	.LBB1_21
	.p2align	4, 0x90
.LBB1_9:                                #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, ref_flag(,%rcx,4)
	jne	.LBB1_11
# BB#10:                                #   in Loop: Header=BB1_9 Depth=2
	movl	$0, Is_primary_correct(%rip)
.LBB1_11:                               #   in Loop: Header=BB1_9 Depth=2
	cmpl	$0, ref_flag+4(,%rcx,4)
	jne	.LBB1_13
# BB#12:                                #   in Loop: Header=BB1_9 Depth=2
	movl	$0, Is_primary_correct(%rip)
.LBB1_13:                               #   in Loop: Header=BB1_9 Depth=2
	cmpl	$0, ref_flag+8(,%rcx,4)
	jne	.LBB1_15
# BB#14:                                #   in Loop: Header=BB1_9 Depth=2
	movl	$0, Is_primary_correct(%rip)
.LBB1_15:                               #   in Loop: Header=BB1_9 Depth=2
	cmpl	$0, ref_flag+12(,%rcx,4)
	jne	.LBB1_17
# BB#16:                                #   in Loop: Header=BB1_9 Depth=2
	movl	$0, Is_primary_correct(%rip)
.LBB1_17:                               #   in Loop: Header=BB1_9 Depth=2
	addq	$4, %rcx
	cmpq	%rcx, %rax
	jne	.LBB1_9
	.p2align	4, 0x90
.LBB1_21:                               # %Error_tracking.exit
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	5652(%rbx), %ecx
	movl	5676(%rbx), %eax
	movl	previous_frame_num(%rip), %edx
	cmpl	%edx, %eax
	jne	.LBB1_25
# BB#22:                                #   in Loop: Header=BB1_1 Depth=1
	cmpl	$1, %ebp
	je	.LBB1_25
# BB#23:                                #   in Loop: Header=BB1_1 Depth=1
	testl	%ecx, %ecx
	je	.LBB1_25
# BB#24:                                #   in Loop: Header=BB1_1 Depth=1
	movl	Is_primary_correct(%rip), %esi
	testl	%esi, %esi
	jne	.LBB1_41
	.p2align	4, 0x90
.LBB1_25:                               # %Error_tracking.exit._crit_edge
                                        #   in Loop: Header=BB1_1 Depth=1
	cmpl	%edx, %eax
	setne	%dl
	testl	%ecx, %ecx
	je	.LBB1_27
# BB#26:                                # %Error_tracking.exit._crit_edge
                                        #   in Loop: Header=BB1_1 Depth=1
	testb	%dl, %dl
	je	.LBB1_28
.LBB1_27:                               # %.preheader.preheader
                                        #   in Loop: Header=BB1_1 Depth=1
	movaps	ref_flag+48(%rip), %xmm0
	movups	%xmm0, ref_flag+52(%rip)
	movaps	ref_flag+32(%rip), %xmm0
	movups	%xmm0, ref_flag+36(%rip)
	movaps	ref_flag+16(%rip), %xmm0
	movups	%xmm0, ref_flag+20(%rip)
	movaps	ref_flag(%rip), %xmm0
	movups	%xmm0, ref_flag+4(%rip)
.LBB1_28:                               # %.loopexit
                                        #   in Loop: Header=BB1_1 Depth=1
	testl	%ecx, %ecx
	movl	$Is_redundant_correct, %ecx
	cmoveq	%r12, %rcx
	movl	(%rcx), %ecx
	movl	%ecx, ref_flag(%rip)
	movl	%eax, previous_frame_num(%rip)
	cmpl	$1, %ebp
	je	.LBB1_29
# BB#30:                                #   in Loop: Header=BB1_1 Depth=1
	movq	5592(%rbx), %r13
	movq	active_pps(%rip), %rax
	cmpl	$0, 12(%rax)
	je	.LBB1_32
# BB#31:                                #   in Loop: Header=BB1_1 Depth=1
	movq	%rbx, %rdi
	callq	init_contexts
	xorl	%eax, %eax
	callq	cabac_new_slice
	movq	active_pps(%rip), %rax
.LBB1_32:                               #   in Loop: Header=BB1_1 Depth=1
	cmpl	$0, 1124(%rax)
	je	.LBB1_34
# BB#33:                                #   in Loop: Header=BB1_1 Depth=1
	cmpl	$1, 44(%rbx)
	je	.LBB1_36
.LBB1_34:                               #   in Loop: Header=BB1_1 Depth=1
	cmpl	$0, 1120(%rax)
	je	.LBB1_37
# BB#35:                                #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, 44(%rbx)
	je	.LBB1_37
.LBB1_36:                               #   in Loop: Header=BB1_1 Depth=1
	movq	%rbx, %rdi
	callq	fill_wp_params
.LBB1_37:                               #   in Loop: Header=BB1_1 Depth=1
	orl	$1, %ebp
	cmpl	$3, %ebp
	jne	.LBB1_40
# BB#38:                                #   in Loop: Header=BB1_1 Depth=1
	cmpl	$0, (%r13)
	jne	.LBB1_40
# BB#39:                                #   in Loop: Header=BB1_1 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	decode_one_slice
	.p2align	4, 0x90
.LBB1_40:                               # %decode_slice.exit
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	$0, 5580(%rbx)
	incl	12(%rbx)
.LBB1_41:                               # %.backedge
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	32(%r15), %eax
	decl	%eax
	cmpl	$1, %eax
	ja	.LBB1_1
# BB#42:                                # %.critedge
	callq	exit_picture
	movl	$2, %eax
	jmp	.LBB1_43
.LBB1_29:
	callq	exit_picture
	movl	$1, %eax
.LBB1_43:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	decode_one_frame, .Lfunc_end1-decode_one_frame
	.cfi_endproc

	.globl	Error_tracking
	.p2align	4, 0x90
	.type	Error_tracking,@function
Error_tracking:                         # @Error_tracking
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	cmpl	$0, 5652(%rax)
	je	.LBB2_1
# BB#17:
	cmpl	$2, 44(%rax)
	je	.LBB2_20
# BB#18:
	movslq	redundant_slice_ref_idx(%rip), %rax
	cmpl	$0, ref_flag(,%rax,4)
	jne	.LBB2_20
# BB#19:
	movl	$0, Is_redundant_correct(%rip)
	retq
.LBB2_1:
	movl	$1, Is_redundant_correct(%rip)
	movl	$1, Is_primary_correct(%rip)
	cmpl	$2, 44(%rax)
	je	.LBB2_20
# BB#2:                                 # %.preheader
	movslq	5640(%rax), %rax
	testq	%rax, %rax
	jle	.LBB2_20
# BB#3:                                 # %.lr.ph
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$3, %rsi
	je	.LBB2_7
	.p2align	4, 0x90
.LBB2_4:                                # =>This Inner Loop Header: Depth=1
	cmpl	$0, ref_flag(,%rcx,4)
	jne	.LBB2_6
# BB#5:                                 #   in Loop: Header=BB2_4 Depth=1
	movl	$0, Is_primary_correct(%rip)
.LBB2_6:                                #   in Loop: Header=BB2_4 Depth=1
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB2_4
.LBB2_7:                                # %.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB2_20
	.p2align	4, 0x90
.LBB2_8:                                # =>This Inner Loop Header: Depth=1
	cmpl	$0, ref_flag(,%rcx,4)
	jne	.LBB2_10
# BB#9:                                 #   in Loop: Header=BB2_8 Depth=1
	movl	$0, Is_primary_correct(%rip)
.LBB2_10:                               #   in Loop: Header=BB2_8 Depth=1
	cmpl	$0, ref_flag+4(,%rcx,4)
	jne	.LBB2_12
# BB#11:                                #   in Loop: Header=BB2_8 Depth=1
	movl	$0, Is_primary_correct(%rip)
.LBB2_12:                               #   in Loop: Header=BB2_8 Depth=1
	cmpl	$0, ref_flag+8(,%rcx,4)
	jne	.LBB2_14
# BB#13:                                #   in Loop: Header=BB2_8 Depth=1
	movl	$0, Is_primary_correct(%rip)
.LBB2_14:                               #   in Loop: Header=BB2_8 Depth=1
	cmpl	$0, ref_flag+12(,%rcx,4)
	jne	.LBB2_16
# BB#15:                                #   in Loop: Header=BB2_8 Depth=1
	movl	$0, Is_primary_correct(%rip)
.LBB2_16:                               #   in Loop: Header=BB2_8 Depth=1
	addq	$4, %rcx
	cmpq	%rax, %rcx
	jl	.LBB2_8
.LBB2_20:                               # %.loopexit
	retq
.Lfunc_end2:
	.size	Error_tracking, .Lfunc_end2-Error_tracking
	.cfi_endproc

	.globl	decode_slice
	.p2align	4, 0x90
	.type	decode_slice,@function
decode_slice:                           # @decode_slice
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 48
.Lcfi31:
	.cfi_offset %rbx, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	5592(%rbx), %r15
	movq	active_pps(%rip), %rax
	cmpl	$0, 12(%rax)
	je	.LBB3_2
# BB#1:
	movq	%rbx, %rdi
	callq	init_contexts
	xorl	%eax, %eax
	callq	cabac_new_slice
	movq	active_pps(%rip), %rax
.LBB3_2:
	cmpl	$0, 1124(%rax)
	je	.LBB3_4
# BB#3:
	cmpl	$1, 44(%rbx)
	je	.LBB3_6
.LBB3_4:
	cmpl	$0, 1120(%rax)
	je	.LBB3_7
# BB#5:
	cmpl	$2, 44(%rbx)
	je	.LBB3_7
.LBB3_6:
	movq	%rbx, %rdi
	callq	fill_wp_params
.LBB3_7:
	orl	$1, %ebp
	cmpl	$3, %ebp
	jne	.LBB3_9
# BB#8:
	cmpl	$0, (%r15)
	je	.LBB3_10
.LBB3_9:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_10:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	decode_one_slice        # TAILCALL
.Lfunc_end3:
	.size	decode_slice, .Lfunc_end3-decode_slice
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.text
	.globl	buf2img
	.p2align	4, 0x90
	.type	buf2img,@function
buf2img:                                # @buf2img
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi41:
	.cfi_def_cfa_offset 208
.Lcfi42:
	.cfi_offset %rbx, -56
.Lcfi43:
	.cfi_offset %r12, -48
.Lcfi44:
	.cfi_offset %r13, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movl	%ecx, %r12d
	movl	%edx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdi, %rbp
	cmpl	$3, %ebx
	jb	.LBB4_2
# BB#1:
	movl	$.L.str, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
.LBB4_2:
	xorl	%eax, %eax
	callq	testEndian
	testl	%eax, %eax
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	je	.LBB4_24
# BB#3:
	cmpl	$1, %ebx
	je	.LBB4_52
# BB#4:
	cmpl	$2, %ebx
	je	.LBB4_71
# BB#5:
	cmpl	$4, %ebx
	jne	.LBB4_23
# BB#6:                                 # %.preheader111
	testl	%r12d, %r12d
	jle	.LBB4_23
# BB#7:                                 # %.preheader111
	testl	%r15d, %r15d
	jle	.LBB4_23
# BB#8:                                 # %.preheader110.us.preheader
	movl	%r15d, %r13d
	movl	%r12d, %r9d
	leal	(,%r15,4), %r12d
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%rax,%r13,4), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	-1(%r13), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%r15d, %eax
	andl	$3, %eax
	movq	%r13, %r14
	movq	%rax, 32(%rsp)          # 8-byte Spill
	subq	%rax, %r14
	movl	$1, %r11d
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movdqa	.LCPI4_0(%rip), %xmm1   # xmm1 = [0,255,0,0,0,0,0,0,0,255,0,0,0,0,0,0]
	xorl	%r10d, %r10d
	xorl	%ecx, %ecx
	jmp	.LBB4_12
.LBB4_9:                                # %vector.body.preheader
                                        #   in Loop: Header=BB4_12 Depth=1
	movq	%r14, %rax
	movl	%r10d, %edx
	movq	%r8, %rbx
	.p2align	4, 0x90
.LBB4_10:                               # %vector.body
                                        #   Parent Loop BB4_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rdx
	movdqu	(%rdi,%rdx), %xmm2
	movdqa	%xmm2, %xmm3
	punpckhdq	%xmm0, %xmm3    # xmm3 = xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movdqa	%xmm2, %xmm4
	psrlq	$8, %xmm4
	movdqa	%xmm3, %xmm5
	psrlq	$8, %xmm5
	pand	%xmm1, %xmm5
	pand	%xmm1, %xmm4
	psrlq	$24, %xmm3
	psrlq	$24, %xmm2
	por	%xmm4, %xmm2
	por	%xmm5, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshuflw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3,4,5,6,7]
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshuflw	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3,4,5,6,7]
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	movq	%xmm2, (%rbx)
	addq	$8, %rbx
	addl	$16, %edx
	addq	$-4, %rax
	jne	.LBB4_10
# BB#11:                                # %middle.block
                                        #   in Loop: Header=BB4_12 Depth=1
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	movq	%r14, %rax
	movl	%r14d, %edx
	jne	.LBB4_17
	jmp	.LBB4_22
	.p2align	4, 0x90
.LBB4_12:                               # %.preheader110.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_10 Depth 2
                                        #     Child Loop BB4_21 Depth 2
	cmpl	$4, %r15d
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rcx,8), %r8
	jb	.LBB4_16
# BB#13:                                # %min.iters.checked
                                        #   in Loop: Header=BB4_12 Depth=1
	testq	%r14, %r14
	je	.LBB4_16
# BB#14:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_12 Depth=1
	movl	%r12d, %eax
	imull	%ecx, %eax
	cltq
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax), %rdx
	cmpq	%rdx, %r8
	movq	(%rsp), %rdi            # 8-byte Reload
	jae	.LBB4_9
# BB#15:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_12 Depth=1
	addq	%rdi, %rax
	leaq	(%r8,%r13,2), %rdx
	cmpq	%rdx, %rax
	jae	.LBB4_9
	.p2align	4, 0x90
.LBB4_16:                               #   in Loop: Header=BB4_12 Depth=1
	xorl	%eax, %eax
	xorl	%edx, %edx
.LBB4_17:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB4_12 Depth=1
	movl	%r13d, %ebx
	subl	%eax, %ebx
	testb	$1, %bl
	jne	.LBB4_19
# BB#18:                                #   in Loop: Header=BB4_12 Depth=1
	movq	%rax, %rbx
	cmpq	%rax, 8(%rsp)           # 8-byte Folded Reload
	jne	.LBB4_20
	jmp	.LBB4_22
	.p2align	4, 0x90
.LBB4_19:                               # %scalar.ph.prol
                                        #   in Loop: Header=BB4_12 Depth=1
	movl	%ecx, %ebx
	imull	%r15d, %ebx
	addl	%edx, %ebx
	movslq	%ebx, %rbx
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	(%rdi,%rbx,4), %ebx
	movq	%r9, %rdi
	movl	%ebx, %r9d
	shrl	$8, %r9d
	andl	$65280, %r9d            # imm = 0xFF00
	shrq	$24, %rbx
	orl	%r9d, %ebx
	movq	%rdi, %r9
	movw	%bx, (%r8,%rax,2)
	leaq	1(%rax), %rbx
	incl	%edx
	cmpq	%rax, 8(%rsp)           # 8-byte Folded Reload
	je	.LBB4_22
.LBB4_20:                               # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB4_12 Depth=1
	movq	%r13, %rax
	subq	%rbx, %rax
	leaq	2(%r8,%rbx,2), %rbx
	leal	(%rdx,%r11), %edi
	shll	$2, %edi
	movslq	%edi, %r8
	movq	(%rsp), %rdi            # 8-byte Reload
	addq	%rdi, %r8
	addl	%esi, %edx
	shll	$2, %edx
	movslq	%edx, %rdx
	addq	%rdi, %rdx
	.p2align	4, 0x90
.LBB4_21:                               # %scalar.ph
                                        #   Parent Loop BB4_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %edi
	movl	%edi, %ebp
	shrl	$8, %ebp
	andl	$65280, %ebp            # imm = 0xFF00
	shrq	$24, %rdi
	orl	%ebp, %edi
	movw	%di, -2(%rbx)
	movl	(%r8), %edi
	movl	%edi, %ebp
	shrl	$8, %ebp
	andl	$65280, %ebp            # imm = 0xFF00
	shrq	$24, %rdi
	orl	%ebp, %edi
	movw	%di, (%rbx)
	addq	$4, %rbx
	addq	$8, %r8
	addq	$8, %rdx
	addq	$-2, %rax
	jne	.LBB4_21
.LBB4_22:                               # %._crit_edge134.us
                                        #   in Loop: Header=BB4_12 Depth=1
	incq	%rcx
	addl	%r12d, %r10d
	addl	%r15d, %r11d
	addl	%r15d, %esi
	cmpq	%r9, %rcx
	jne	.LBB4_12
.LBB4_23:                               # %.loopexit112
	movl	$.L.str.1, %edi
	movl	$500, %esi              # imm = 0x1F4
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	error                   # TAILCALL
.LBB4_24:
	testl	%r12d, %r12d
	setg	%cl
	testl	%r15d, %r15d
	setg	%al
	andb	%cl, %al
	cmpl	$1, %ebx
	jne	.LBB4_42
# BB#25:                                # %.preheader101
	testb	%al, %al
	je	.LBB4_88
# BB#26:                                # %.preheader.us.preheader
	leal	-1(%r15), %r11d
	incq	%r11
	movl	%r15d, %r14d
	movl	%r12d, %r10d
	leaq	-1(%r14), %r9
	movl	%r15d, %r8d
	andl	$15, %r8d
	movq	%r14, %r12
	subq	%r8, %r12
	xorl	%r13d, %r13d
	pxor	%xmm0, %xmm0
	movq	(%rsp), %rsi            # 8-byte Reload
	jmp	.LBB4_30
.LBB4_27:                               # %vector.body279.preheader
                                        #   in Loop: Header=BB4_30 Depth=1
	leaq	(%rsi,%r12), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_28:                               # %vector.body279
                                        #   Parent Loop BB4_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi,%rax), %xmm1      # xmm1 = mem[0],zero
	movq	8(%rsi,%rax), %xmm2     # xmm2 = mem[0],zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	movdqu	%xmm1, (%rdx,%rax,2)
	movdqu	%xmm2, 16(%rdx,%rax,2)
	addq	$16, %rax
	cmpq	%rax, %r12
	jne	.LBB4_28
# BB#29:                                # %middle.block280
                                        #   in Loop: Header=BB4_30 Depth=1
	testl	%r8d, %r8d
	movq	%r12, %rbp
	jne	.LBB4_35
	jmp	.LBB4_41
	.p2align	4, 0x90
.LBB4_30:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_28 Depth 2
                                        #     Child Loop BB4_37 Depth 2
                                        #     Child Loop BB4_40 Depth 2
	cmpl	$16, %r15d
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r13,8), %rdx
	jb	.LBB4_34
# BB#31:                                # %min.iters.checked283
                                        #   in Loop: Header=BB4_30 Depth=1
	testq	%r12, %r12
	je	.LBB4_34
# BB#32:                                # %vector.memcheck297
                                        #   in Loop: Header=BB4_30 Depth=1
	movq	%r11, %rax
	imulq	%r13, %rax
	leaq	(%r14,%rax), %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	addq	%rdi, %rcx
	cmpq	%rcx, %rdx
	jae	.LBB4_27
# BB#33:                                # %vector.memcheck297
                                        #   in Loop: Header=BB4_30 Depth=1
	addq	%rdi, %rax
	leaq	(%rdx,%r14,2), %rcx
	cmpq	%rcx, %rax
	jae	.LBB4_27
	.p2align	4, 0x90
.LBB4_34:                               #   in Loop: Header=BB4_30 Depth=1
	xorl	%ebp, %ebp
	movq	%rsi, %rdi
.LBB4_35:                               # %scalar.ph281.preheader
                                        #   in Loop: Header=BB4_30 Depth=1
	movl	%r14d, %eax
	subl	%ebp, %eax
	movq	%r9, %rcx
	subq	%rbp, %rcx
	andq	$3, %rax
	je	.LBB4_38
# BB#36:                                # %scalar.ph281.prol.preheader
                                        #   in Loop: Header=BB4_30 Depth=1
	negq	%rax
	.p2align	4, 0x90
.LBB4_37:                               # %scalar.ph281.prol
                                        #   Parent Loop BB4_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi), %ebx
	incq	%rdi
	movw	%bx, (%rdx,%rbp,2)
	incq	%rbp
	incq	%rax
	jne	.LBB4_37
.LBB4_38:                               # %scalar.ph281.prol.loopexit
                                        #   in Loop: Header=BB4_30 Depth=1
	cmpq	$3, %rcx
	jb	.LBB4_41
# BB#39:                                # %scalar.ph281.preheader.new
                                        #   in Loop: Header=BB4_30 Depth=1
	movq	%r14, %rax
	subq	%rbp, %rax
	leaq	6(%rdx,%rbp,2), %rcx
	.p2align	4, 0x90
.LBB4_40:                               # %scalar.ph281
                                        #   Parent Loop BB4_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi), %edx
	movw	%dx, -6(%rcx)
	movzbl	1(%rdi), %edx
	movw	%dx, -4(%rcx)
	movzbl	2(%rdi), %edx
	movw	%dx, -2(%rcx)
	movzbl	3(%rdi), %edx
	movw	%dx, (%rcx)
	addq	$8, %rcx
	addq	$4, %rdi
	addq	$-4, %rax
	jne	.LBB4_40
.LBB4_41:                               # %._crit_edge.us
                                        #   in Loop: Header=BB4_30 Depth=1
	addq	%r11, %rsi
	incq	%r13
	cmpq	%r10, %r13
	jne	.LBB4_30
	jmp	.LBB4_88
.LBB4_42:                               # %.preheader102
	testb	%al, %al
	je	.LBB4_88
# BB#43:                                # %.lr.ph122.split.us.preheader
	movslq	%ebx, %rsi
	movslq	%r15d, %rax
	movl	%r15d, %ecx
	movl	%r12d, %edx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	leaq	-1(%rcx), %rdx
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	%ecx, %r15d
	andl	$3, %r15d
	movq	%rsi, %rcx
	movq	%rax, 112(%rsp)         # 8-byte Spill
	imulq	%rax, %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leaq	(,%rsi,4), %rax
	movl	$3, %ecx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movl	$2, %ecx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	$1, %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	(%rsp), %rbp            # 8-byte Reload
	xorl	%r13d, %r13d
	movq	%r15, 88(%rsp)          # 8-byte Spill
	movq	%rax, 128(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_44:                               # %.lr.ph122.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_46 Depth 2
                                        #     Child Loop BB4_50 Depth 2
	testq	%r15, %r15
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	je	.LBB4_47
# BB#45:                                # %.prol.preheader
                                        #   in Loop: Header=BB4_44 Depth=1
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	.p2align	4, 0x90
.LBB4_46:                               #   Parent Loop BB4_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%r13,8), %rdi
	movw	$0, (%rdi,%rbx)
	addq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	memcpy
	movq	8(%rsp), %rdx           # 8-byte Reload
	incq	%r12
	addq	%rdx, %rbp
	addq	$2, %rbx
	cmpq	%r12, %r15
	jne	.LBB4_46
	jmp	.LBB4_48
	.p2align	4, 0x90
.LBB4_47:                               #   in Loop: Header=BB4_44 Depth=1
	xorl	%r12d, %r12d
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB4_48:                               # %.prol.loopexit
                                        #   in Loop: Header=BB4_44 Depth=1
	cmpq	$3, 96(%rsp)            # 8-byte Folded Reload
	movq	%r14, %rbx
	jb	.LBB4_51
# BB#49:                                # %.lr.ph122.split.us.new
                                        #   in Loop: Header=BB4_44 Depth=1
	movq	72(%rsp), %rcx          # 8-byte Reload
	subq	%r12, %rcx
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%r12,%rax), %rax
	movq	8(%rsp), %rdx           # 8-byte Reload
	imulq	%rdx, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	6(%r12,%r12), %r15
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%r12,%rax), %rax
	imulq	%rdx, %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%r12,%rax), %rax
	imulq	%rdx, %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	addq	40(%rsp), %r12          # 8-byte Folded Reload
	imulq	%rdx, %r12
	movq	(%rsp), %r14            # 8-byte Reload
	.p2align	4, 0x90
.LBB4_50:                               #   Parent Loop BB4_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	(%rbx,%r13,8), %rax
	leaq	-6(%rax,%r15), %rdi
	movw	$0, -6(%rax,%r15)
	movq	136(%rsp), %rax         # 8-byte Reload
	leaq	(%r14,%rax), %rsi
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rdx
	callq	memcpy
	movq	(%rbx,%r13,8), %rax
	leaq	-4(%rax,%r15), %rdi
	movw	$0, -4(%rax,%r15)
	leaq	(%r14,%r12), %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	(%rbx,%r13,8), %rax
	leaq	-2(%rax,%r15), %rdi
	movw	$0, -2(%rax,%r15)
	movq	144(%rsp), %rax         # 8-byte Reload
	leaq	(%r14,%rax), %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	(%rbx,%r13,8), %rdi
	movw	$0, (%rdi,%r15)
	addq	%r15, %rdi
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%r14,%rax), %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	128(%rsp), %rdi         # 8-byte Reload
	addq	%rdi, %r14
	addq	$8, %r15
	addq	$-4, %rcx
	jne	.LBB4_50
.LBB4_51:                               # %._crit_edge120.us
                                        #   in Loop: Header=BB4_44 Depth=1
	incq	%r13
	movq	120(%rsp), %rbp         # 8-byte Reload
	addq	80(%rsp), %rbp          # 8-byte Folded Reload
	movq	112(%rsp), %rax         # 8-byte Reload
	addq	%rax, 64(%rsp)          # 8-byte Folded Spill
	addq	%rax, 56(%rsp)          # 8-byte Folded Spill
	addq	%rax, 48(%rsp)          # 8-byte Folded Spill
	addq	%rax, 40(%rsp)          # 8-byte Folded Spill
	cmpq	104(%rsp), %r13         # 8-byte Folded Reload
	movq	88(%rsp), %r15          # 8-byte Reload
	jne	.LBB4_44
	jmp	.LBB4_88
.LBB4_52:                               # %.preheader105
	testl	%r12d, %r12d
	jle	.LBB4_88
# BB#53:                                # %.preheader105
	testl	%r15d, %r15d
	jle	.LBB4_88
# BB#54:                                # %.preheader104.us.preheader
	movslq	%r15d, %r14
	movl	%r15d, %ebx
	movl	%r12d, %r11d
	leaq	-1(%rbx), %r10
	movl	%r15d, %eax
	andl	$15, %eax
	movq	%rbx, %r9
	movq	%rax, 8(%rsp)           # 8-byte Spill
	subq	%rax, %r9
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	8(%rax), %r12
	xorl	%r13d, %r13d
	pxor	%xmm0, %xmm0
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB4_55:                               # %.preheader104.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_63 Depth 2
                                        #     Child Loop BB4_67 Depth 2
                                        #     Child Loop BB4_69 Depth 2
	cmpl	$16, %r15d
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r13,8), %rsi
	jae	.LBB4_57
# BB#56:                                #   in Loop: Header=BB4_55 Depth=1
	xorl	%edx, %edx
	jmp	.LBB4_65
	.p2align	4, 0x90
.LBB4_57:                               # %min.iters.checked253
                                        #   in Loop: Header=BB4_55 Depth=1
	testq	%r9, %r9
	je	.LBB4_61
# BB#58:                                # %vector.memcheck267
                                        #   in Loop: Header=BB4_55 Depth=1
	movq	%r14, %rax
	imulq	%r13, %rax
	leaq	(%rbx,%rax), %rcx
	movq	(%rsp), %rdx            # 8-byte Reload
	addq	%rdx, %rcx
	cmpq	%rcx, %rsi
	jae	.LBB4_62
# BB#59:                                # %vector.memcheck267
                                        #   in Loop: Header=BB4_55 Depth=1
	addq	%rdx, %rax
	leaq	(%rsi,%rbx,2), %rcx
	cmpq	%rcx, %rax
	jae	.LBB4_62
.LBB4_61:                               #   in Loop: Header=BB4_55 Depth=1
	xorl	%edx, %edx
	jmp	.LBB4_65
.LBB4_62:                               # %vector.body249.preheader
                                        #   in Loop: Header=BB4_55 Depth=1
	leaq	16(%rsi), %rax
	movq	%r9, %rcx
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB4_63:                               # %vector.body249
                                        #   Parent Loop BB4_55 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	movq	(%rdx), %xmm2           # xmm2 = mem[0],zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	movdqu	%xmm1, -16(%rax)
	movdqu	%xmm2, (%rax)
	addq	$16, %rdx
	addq	$32, %rax
	addq	$-16, %rcx
	jne	.LBB4_63
# BB#64:                                # %middle.block250
                                        #   in Loop: Header=BB4_55 Depth=1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movq	%r9, %rdx
	je	.LBB4_70
	.p2align	4, 0x90
.LBB4_65:                               # %scalar.ph251.preheader
                                        #   in Loop: Header=BB4_55 Depth=1
	movl	%ebx, %ecx
	subl	%edx, %ecx
	movq	%r10, %rax
	subq	%rdx, %rax
	andq	$3, %rcx
	je	.LBB4_68
# BB#66:                                # %scalar.ph251.prol.preheader
                                        #   in Loop: Header=BB4_55 Depth=1
	leaq	(%rbp,%rdx), %rdi
	negq	%rcx
	.p2align	4, 0x90
.LBB4_67:                               # %scalar.ph251.prol
                                        #   Parent Loop BB4_55 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi), %r8d
	movw	%r8w, (%rsi,%rdx,2)
	incq	%rdx
	incq	%rdi
	incq	%rcx
	jne	.LBB4_67
.LBB4_68:                               # %scalar.ph251.prol.loopexit
                                        #   in Loop: Header=BB4_55 Depth=1
	cmpq	$3, %rax
	jb	.LBB4_70
	.p2align	4, 0x90
.LBB4_69:                               # %scalar.ph251
                                        #   Parent Loop BB4_55 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbp,%rdx), %eax
	movw	%ax, (%rsi,%rdx,2)
	movzbl	1(%rbp,%rdx), %eax
	movw	%ax, 2(%rsi,%rdx,2)
	movzbl	2(%rbp,%rdx), %eax
	movw	%ax, 4(%rsi,%rdx,2)
	movzbl	3(%rbp,%rdx), %eax
	movw	%ax, 6(%rsi,%rdx,2)
	addq	$4, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB4_69
.LBB4_70:                               # %._crit_edge125.us
                                        #   in Loop: Header=BB4_55 Depth=1
	incq	%r13
	addq	%r14, %r12
	addq	%r14, %rbp
	cmpq	%r11, %r13
	jne	.LBB4_55
	jmp	.LBB4_88
.LBB4_71:                               # %.preheader108
	testl	%r12d, %r12d
	jle	.LBB4_88
# BB#72:                                # %.preheader108
	testl	%r15d, %r15d
	jle	.LBB4_88
# BB#73:                                # %.preheader107.us.preheader
	movl	%r15d, %r13d
	movl	%r12d, %r11d
	leal	(%r15,%r15), %r12d
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%rax,%r13,2), %r8
	leaq	-1(%r13), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%r15d, %eax
	andl	$15, %eax
	movq	%r13, %r14
	movq	%rax, 24(%rsp)          # 8-byte Spill
	subq	%rax, %r14
	movl	$1, %r10d
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	jmp	.LBB4_77
.LBB4_74:                               # %vector.body210.preheader
                                        #   in Loop: Header=BB4_77 Depth=1
	movq	%r8, %rbp
	leaq	16(%rbx), %rax
	movq	%r14, %rdx
	movl	%r9d, %r8d
	.p2align	4, 0x90
.LBB4_75:                               # %vector.body210
                                        #   Parent Loop BB4_77 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%r8d, %r8
	movdqu	(%rdi,%r8), %xmm1
	movdqu	16(%rdi,%r8), %xmm2
	movdqa	%xmm1, %xmm3
	punpckhbw	%xmm0, %xmm3    # xmm3 = xmm3[8],xmm0[8],xmm3[9],xmm0[9],xmm3[10],xmm0[10],xmm3[11],xmm0[11],xmm3[12],xmm0[12],xmm3[13],xmm0[13],xmm3[14],xmm0[14],xmm3[15],xmm0[15]
	pshuflw	$177, %xmm3, %xmm3      # xmm3 = xmm3[1,0,3,2,4,5,6,7]
	pshufhw	$177, %xmm3, %xmm3      # xmm3 = xmm3[0,1,2,3,5,4,7,6]
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	pshuflw	$177, %xmm1, %xmm1      # xmm1 = xmm1[1,0,3,2,4,5,6,7]
	pshufhw	$177, %xmm1, %xmm1      # xmm1 = xmm1[0,1,2,3,5,4,7,6]
	packuswb	%xmm3, %xmm1
	movdqa	%xmm2, %xmm3
	punpckhbw	%xmm0, %xmm3    # xmm3 = xmm3[8],xmm0[8],xmm3[9],xmm0[9],xmm3[10],xmm0[10],xmm3[11],xmm0[11],xmm3[12],xmm0[12],xmm3[13],xmm0[13],xmm3[14],xmm0[14],xmm3[15],xmm0[15]
	pshuflw	$177, %xmm3, %xmm3      # xmm3 = xmm3[1,0,3,2,4,5,6,7]
	pshufhw	$177, %xmm3, %xmm3      # xmm3 = xmm3[0,1,2,3,5,4,7,6]
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	pshuflw	$177, %xmm2, %xmm2      # xmm2 = xmm2[1,0,3,2,4,5,6,7]
	pshufhw	$177, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,5,4,7,6]
	packuswb	%xmm3, %xmm2
	movdqu	%xmm1, -16(%rax)
	movdqu	%xmm2, (%rax)
	addq	$32, %rax
	addl	$32, %r8d
	addq	$-16, %rdx
	jne	.LBB4_75
# BB#76:                                # %middle.block211
                                        #   in Loop: Header=BB4_77 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movq	%r14, %rax
	movl	%r14d, %edx
	movq	%rbp, %r8
	jne	.LBB4_82
	jmp	.LBB4_87
	.p2align	4, 0x90
.LBB4_77:                               # %.preheader107.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_75 Depth 2
                                        #     Child Loop BB4_86 Depth 2
	cmpl	$16, %r15d
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rcx,8), %rbx
	jb	.LBB4_81
# BB#78:                                # %min.iters.checked214
                                        #   in Loop: Header=BB4_77 Depth=1
	testq	%r14, %r14
	je	.LBB4_81
# BB#79:                                # %vector.memcheck229
                                        #   in Loop: Header=BB4_77 Depth=1
	movl	%r12d, %eax
	imull	%ecx, %eax
	cltq
	leaq	(%r8,%rax), %rdx
	cmpq	%rdx, %rbx
	movq	(%rsp), %rdi            # 8-byte Reload
	jae	.LBB4_74
# BB#80:                                # %vector.memcheck229
                                        #   in Loop: Header=BB4_77 Depth=1
	addq	%rdi, %rax
	leaq	(%rbx,%r13,2), %rdx
	cmpq	%rdx, %rax
	jae	.LBB4_74
	.p2align	4, 0x90
.LBB4_81:                               #   in Loop: Header=BB4_77 Depth=1
	xorl	%eax, %eax
	xorl	%edx, %edx
.LBB4_82:                               # %scalar.ph212.preheader
                                        #   in Loop: Header=BB4_77 Depth=1
	movl	%r13d, %edi
	subl	%eax, %edi
	testb	$1, %dil
	jne	.LBB4_84
# BB#83:                                #   in Loop: Header=BB4_77 Depth=1
	movq	%rax, %rdi
	cmpq	%rax, 8(%rsp)           # 8-byte Folded Reload
	jne	.LBB4_85
	jmp	.LBB4_87
	.p2align	4, 0x90
.LBB4_84:                               # %scalar.ph212.prol
                                        #   in Loop: Header=BB4_77 Depth=1
	movl	%ecx, %edi
	imull	%r15d, %edi
	addl	%edx, %edi
	movslq	%edi, %rdi
	movq	(%rsp), %rbp            # 8-byte Reload
	movzwl	(%rbp,%rdi,2), %edi
	rolw	$8, %di
	movw	%di, (%rbx,%rax,2)
	leaq	1(%rax), %rdi
	incl	%edx
	cmpq	%rax, 8(%rsp)           # 8-byte Folded Reload
	je	.LBB4_87
.LBB4_85:                               # %scalar.ph212.preheader.new
                                        #   in Loop: Header=BB4_77 Depth=1
	movq	%r13, %rax
	subq	%rdi, %rax
	leaq	2(%rbx,%rdi,2), %rbx
	leal	(%rdx,%r10), %edi
	addl	%edi, %edi
	movslq	%edi, %rdi
	movq	(%rsp), %rbp            # 8-byte Reload
	addq	%rbp, %rdi
	addl	%esi, %edx
	addl	%edx, %edx
	movslq	%edx, %rdx
	addq	%rbp, %rdx
	.p2align	4, 0x90
.LBB4_86:                               # %scalar.ph212
                                        #   Parent Loop BB4_77 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rdx), %ebp
	rolw	$8, %bp
	movw	%bp, -2(%rbx)
	movzwl	(%rdi), %ebp
	rolw	$8, %bp
	movw	%bp, (%rbx)
	addq	$4, %rbx
	addq	$4, %rdi
	addq	$4, %rdx
	addq	$-2, %rax
	jne	.LBB4_86
.LBB4_87:                               # %._crit_edge129.us
                                        #   in Loop: Header=BB4_77 Depth=1
	incq	%rcx
	addl	%r12d, %r9d
	addl	%r15d, %r10d
	addl	%r15d, %esi
	cmpq	%r11, %rcx
	jne	.LBB4_77
.LBB4_88:                               # %.loopexit
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	buf2img, .Lfunc_end4-buf2img
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4621819117588971520     # double 10
	.text
	.globl	find_snr
	.p2align	4, 0x90
	.type	find_snr,@function
find_snr:                               # @find_snr
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi54:
	.cfi_def_cfa_offset 160
.Lcfi55:
	.cfi_offset %rbx, -56
.Lcfi56:
	.cfi_offset %r12, -48
.Lcfi57:
	.cfi_offset %r13, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movl	%edx, 48(%rsp)          # 4-byte Spill
	movq	%rsi, %r13
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	img(%rip), %r12
	movl	5872(%r12), %r15d
	movl	5900(%r12), %eax
	movl	%eax, 56(%rsp)          # 4-byte Spill
	movl	%r15d, %edi
	sarl	$31, %edi
	shrl	$29, %edi
	movl	5904(%r12), %eax
	movl	%eax, 60(%rsp)          # 4-byte Spill
	movq	active_sps(%rip), %rax
	movl	2160(%rax), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movq	.Lexit_picture.yuv_types+16(%rip), %rax
	movq	%rax, 96(%rsp)
	movapd	.Lexit_picture.yuv_types(%rip), %xmm0
	movapd	%xmm0, 80(%rsp)
	movl	4(%r13), %eax
	movq	input(%rip), %r10
	movl	3008(%r10), %r8d
	cltd
	idivl	%r8d
	movl	%eax, %r9d
	movl	317052(%r13), %ebp
	xorl	%eax, %eax
	testl	%ebp, %ebp
	movl	$0, %ebx
	movl	$0, %esi
	movl	$0, %edx
	movl	$0, %ecx
	je	.LBB5_2
# BB#1:
	movslq	317044(%r13), %rsi
	movl	find_snr.SubWidthC(,%rsi,4), %edx
	movl	317056(%r13), %ecx
	imull	%edx, %ecx
	imull	317060(%r13), %edx
	movl	$2, %ebx
	subl	317048(%r13), %ebx
	imull	find_snr.SubHeightC(,%rsi,4), %ebx
	movl	317064(%r13), %esi
	imull	%ebx, %esi
	imull	317068(%r13), %ebx
.LBB5_2:
	addl	%edi, %r15d
	movl	316864(%r13), %r14d
	movl	316868(%r13), %r11d
	movl	%r14d, %edi
	subl	%ecx, %edi
	movl	%edi, (%rsp)            # 4-byte Spill
	movl	%r11d, %ecx
	subl	%esi, %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	testl	%ebp, %ebp
	movl	$0, %ebp
	movl	$0, %ecx
	movl	$0, %edi
	je	.LBB5_4
# BB#3:
	movl	317056(%r13), %edi
	movl	317060(%r13), %ecx
	movl	$2, %eax
	subl	317048(%r13), %eax
	movl	317064(%r13), %ebp
	imull	%eax, %ebp
	imull	317068(%r13), %eax
.LBB5_4:
	sarl	$3, %r15d
	subl	%edx, (%rsp)            # 4-byte Folded Spill
	subl	%ebx, 4(%rsp)           # 4-byte Folded Spill
	cmpl	$0, 317044(%r13)
	je	.LBB5_6
.LBB5_5:
	addl	%ecx, %edi
	movl	316872(%r13), %edx
	movl	316876(%r13), %esi
	subl	%edi, %edx
	addl	%eax, %ebp
	subl	%ebp, %esi
	jmp	.LBB5_8
.LBB5_6:
	cmpl	$0, 3012(%r10)
	je	.LBB5_5
# BB#7:
	movl	%r14d, %eax
	shrl	$31, %eax
	addl	%r14d, %eax
	movl	%eax, %edx
	sarl	%edx
	movl	%r11d, %esi
	shrl	$31, %esi
	addl	%r11d, %esi
	sarl	%esi
.LBB5_8:
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movslq	4(%rsp), %rax           # 4-byte Folded Reload
	movl	(%rsp), %ebp            # 4-byte Reload
	movslq	%ebp, %rcx
	imulq	%rax, %rcx
	movslq	%esi, %rax
	movslq	%edx, %rdx
	imulq	%rax, %rdx
	leaq	(%rcx,%rdx,2), %rsi
	movslq	%r15d, %rdi
	movl	6000(%r12), %ecx
	testl	%r9d, %r9d
	jne	.LBB5_12
# BB#9:
	testl	%ecx, %ecx
	je	.LBB5_11
# BB#10:
	movl	6060(%r12), %eax
	imull	(%r12), %eax
	cltd
	idivl	%r8d
	movl	%eax, 5996(%r12)
	jmp	.LBB5_12
.LBB5_11:
	xorl	%ecx, %ecx
.LBB5_12:                               # %._crit_edge
	imulq	%rdi, %rsi
	movl	5996(%r12), %ebx
	addl	%r9d, %ebx
	cmpl	%ebx, %ecx
	cmovll	%ebx, %ecx
	movl	%ecx, 6000(%r12)
	movl	%ebx, frame_no(%rip)
	movl	%ebp, (%rsp)            # 4-byte Spill
	movl	%ebp, %eax
	imull	%r15d, %eax
	imull	4(%rsp), %eax           # 4-byte Folded Reload
	movslq	%eax, %r14
	movq	%r14, %rdi
	movq	%rsi, %rbp
	callq	malloc
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.LBB5_14
# BB#13:
	movl	$.L.str.2, %edi
	callq	no_mem_exit
	movl	frame_no(%rip), %ebx
.LBB5_14:
	movslq	%ebx, %rsi
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	imulq	%rbp, %rsi
	xorl	%edx, %edx
	movl	48(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %edi
	callq	lseek64
	cmpq	$-1, %rax
	je	.LBB5_57
# BB#15:
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jne	.LBB5_17
# BB#16:
	movabsq	$6148914691236517206, %rcx # imm = 0x5555555555555556
	movq	16(%rsp), %rax          # 8-byte Reload
	imulq	%rcx
	movq	%rdx, %rax
	shrq	$63, %rax
	leaq	(%rax,%rdx), %rsi
	movl	$1, %edx
	movl	%ebx, %edi
	callq	lseek64
.LBB5_17:
	movl	%ebx, %edi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	read
	movq	imgY_ref(%rip), %rdi
	movq	%r12, %rsi
	movl	(%rsp), %ebp            # 4-byte Reload
	movl	%ebp, %edx
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%r15d, %r8d
	callq	buf2img
	cmpl	$0, 317044(%r13)
	je	.LBB5_20
# BB#18:                                # %.preheader242
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %eax
	imull	%r15d, %eax
	movl	%ebx, %r14d
	movl	8(%rsp), %ebx           # 4-byte Reload
	imull	%ebx, %eax
	movslq	%eax, %rdx
	movl	%r14d, %edi
	movq	%r12, %rsi
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	callq	read
	movq	imgUV_ref(%rip), %rax
	movq	(%rax), %rdi
	movq	%r12, %rsi
	movl	%ebp, %edx
	movl	%ebx, %ecx
	movl	%r15d, %r8d
	callq	buf2img
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB5_21
# BB#19:                                # %.loopexit243.thread303
	movl	%r14d, %edi
	movq	%r12, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	read
	movq	imgUV_ref(%rip), %rax
	movq	8(%rax), %rdi
	movq	%r12, %rsi
	movl	%ebp, %edx
	movl	%ebx, %ecx
	movl	%r15d, %r8d
	callq	buf2img
	movl	(%rsp), %ebp            # 4-byte Reload
	jmp	.LBB5_23
.LBB5_20:                               # %.loopexit243
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jne	.LBB5_23
	jmp	.LBB5_22
.LBB5_21:                               # %.loopexit243.thread
	movq	16(%rsp), %rsi          # 8-byte Reload
	negq	%rsi
	movl	$1, %edx
	movl	%r14d, %edi
	callq	lseek64
	movl	%r14d, %edi
	movq	%r12, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	read
	movq	imgUV_ref(%rip), %rax
	movq	8(%rax), %rdi
	movq	%r12, %rsi
	movl	%ebp, %edx
	movl	%ebx, %ecx
	movl	%r15d, %r8d
	callq	buf2img
	movl	(%rsp), %ebp            # 4-byte Reload
	movl	%r14d, %ebx
.LBB5_22:
	movq	16(%rsp), %rax          # 8-byte Reload
	addq	%rax, %rax
	movabsq	$6148914691236517206, %rcx # imm = 0x5555555555555556
	imulq	%rcx
	movq	%rdx, %rax
	shrq	$63, %rax
	leaq	(%rax,%rdx), %rsi
	movl	$1, %edx
	movl	%ebx, %edi
	callq	lseek64
.LBB5_23:
	movq	%r12, %rdi
	callq	free
	movq	img(%rip), %rax
	movq	5552(%rax), %rax
	movl	$0, (%rax)
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB5_33
# BB#24:                                # %.preheader241.lr.ph
	testl	%ebp, %ebp
	jle	.LBB5_33
# BB#25:                                # %.preheader241.us.preheader
	movq	imgY_ref(%rip), %r9
	movq	img(%rip), %rax
	movq	5552(%rax), %rdx
	movq	316920(%r13), %r10
	movl	%ebp, %r12d
	movl	4(%rsp), %r11d          # 4-byte Reload
	movl	%r12d, %r14d
	andl	$1, %r14d
	xorl	%r15d, %r15d
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB5_26:                               # %.preheader241.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_30 Depth 2
	testq	%r14, %r14
	movq	(%r10,%r15,8), %rsi
	movq	(%r9,%r15,8), %rbp
	jne	.LBB5_28
# BB#27:                                #   in Loop: Header=BB5_26 Depth=1
	xorl	%edi, %edi
	cmpl	$1, (%rsp)              # 4-byte Folded Reload
	jne	.LBB5_29
	jmp	.LBB5_31
	.p2align	4, 0x90
.LBB5_28:                               #   in Loop: Header=BB5_26 Depth=1
	movzwl	(%rsi), %ecx
	movzwl	(%rbp), %edi
	subq	%rdi, %rcx
	movslq	(%rdx,%rcx,4), %rcx
	addq	%rcx, %r8
	movl	$1, %edi
	cmpl	$1, (%rsp)              # 4-byte Folded Reload
	je	.LBB5_31
.LBB5_29:                               # %.preheader241.us.new
                                        #   in Loop: Header=BB5_26 Depth=1
	movq	%r12, %rcx
	subq	%rdi, %rcx
	leaq	2(%rsi,%rdi,2), %rsi
	leaq	2(%rbp,%rdi,2), %rbp
	.p2align	4, 0x90
.LBB5_30:                               #   Parent Loop BB5_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	-2(%rsi), %edi
	movzwl	-2(%rbp), %ebx
	subq	%rbx, %rdi
	movslq	(%rdx,%rdi,4), %rdi
	addq	%r8, %rdi
	movzwl	(%rsi), %eax
	movzwl	(%rbp), %ebx
	subq	%rbx, %rax
	movslq	(%rdx,%rax,4), %r8
	addq	%rdi, %r8
	addq	$4, %rsi
	addq	$4, %rbp
	addq	$-2, %rcx
	jne	.LBB5_30
.LBB5_31:                               # %._crit_edge256.us
                                        #   in Loop: Header=BB5_26 Depth=1
	incq	%r15
	cmpq	%r11, %r15
	jne	.LBB5_26
	jmp	.LBB5_34
.LBB5_33:
	xorl	%r8d, %r8d
.LBB5_34:                               # %._crit_edge260
	xorl	%ebx, %ebx
	cmpl	$0, 317044(%r13)
	movq	%r13, 40(%rsp)          # 8-byte Spill
	je	.LBB5_43
# BB#35:                                # %._crit_edge260
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movl	$0, %ebp
	jle	.LBB5_44
# BB#36:                                # %.preheader.lr.ph
	movl	12(%rsp), %esi          # 4-byte Reload
	testl	%esi, %esi
	jle	.LBB5_42
# BB#37:                                # %.preheader.us.preheader
	movq	imgUV_ref(%rip), %rdx
	movq	img(%rip), %rcx
	movq	5552(%rcx), %r11
	movq	(%rdx), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	8(%rdx), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	316928(%rax), %rdx
	movq	(%rdx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	8(%rdx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%esi, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	8(%rsp), %r15d          # 4-byte Reload
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_38:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_39 Depth 2
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r12,8), %rsi
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r12,8), %rdi
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r12,8), %rdx
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r12,8), %r13
	movq	72(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB5_39:                               #   Parent Loop BB5_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rsi), %r9d
	movzwl	(%rdi), %r10d
	subq	%r10, %r9
	movslq	(%r11,%r9,4), %rcx
	addq	%rcx, %rbp
	movzwl	(%rdx), %ecx
	movzwl	(%r13), %eax
	subq	%rax, %rcx
	movslq	(%r11,%rcx,4), %rax
	addq	%rax, %rbx
	addq	$2, %rsi
	addq	$2, %rdi
	addq	$2, %rdx
	addq	$2, %r13
	decq	%r14
	jne	.LBB5_39
# BB#40:                                # %._crit_edge.us
                                        #   in Loop: Header=BB5_38 Depth=1
	incq	%r12
	cmpq	%r15, %r12
	jne	.LBB5_38
	jmp	.LBB5_44
.LBB5_42:
	xorl	%ebx, %ebx
.LBB5_43:                               # %.loopexit
	xorl	%ebp, %ebp
.LBB5_44:                               # %.loopexit
	xorpd	%xmm1, %xmm1
	testq	%r8, %r8
	xorpd	%xmm0, %xmm0
	je	.LBB5_46
# BB#45:
	movl	56(%rsp), %eax          # 4-byte Reload
	imull	%eax, %eax
	movl	%eax, %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	movl	(%rsp), %eax            # 4-byte Reload
	cvtsi2sdl	%eax, %xmm2
	movl	4(%rsp), %eax           # 4-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm2, %xmm0
	xorps	%xmm2, %xmm2
	cvtsi2sdq	%r8, %xmm2
	divsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm0
	callq	log10
	xorpd	%xmm1, %xmm1
	mulsd	.LCPI5_0(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
.LBB5_46:
	movl	60(%rsp), %r15d         # 4-byte Reload
	imull	%r15d, %r15d
	movq	64(%rsp), %r14          # 8-byte Reload
	movss	%xmm0, 4(%r14)
	testq	%rbp, %rbp
	je	.LBB5_48
# BB#47:
	movl	%r15d, %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	movl	12(%rsp), %eax          # 4-byte Reload
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movl	8(%rsp), %eax           # 4-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm2, %xmm0
	xorps	%xmm2, %xmm2
	cvtsi2sdq	%rbp, %xmm2
	divsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm0
	callq	log10
	mulsd	.LCPI5_0(%rip), %xmm0
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm0, %xmm1
.LBB5_48:
	movss	%xmm1, 8(%r14)
	testq	%rbx, %rbx
	je	.LBB5_50
# BB#49:
	movl	%r15d, %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	movl	12(%rsp), %eax          # 4-byte Reload
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movl	8(%rsp), %eax           # 4-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm2, %xmm0
	xorps	%xmm2, %xmm2
	cvtsi2sdq	%rbx, %xmm2
	divsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm0
	callq	log10
	mulsd	.LCPI5_0(%rip), %xmm0
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm0, %xmm2
	jmp	.LBB5_51
.LBB5_50:
	xorpd	%xmm2, %xmm2
.LBB5_51:
	movq	40(%rsp), %rsi          # 8-byte Reload
	movss	%xmm2, 12(%r14)
	movq	img(%rip), %rax
	cmpl	$0, (%rax)
	je	.LBB5_53
# BB#52:
	movl	(%r14), %eax
	cvtsi2ssl	%eax, %xmm3
	movss	28(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	movss	4(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	incl	%eax
	cvtsi2ssl	%eax, %xmm4
	divss	%xmm4, %xmm1
	movss	%xmm1, 28(%r14)
	movss	32(%r14), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	movss	8(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm5
	divss	%xmm4, %xmm5
	movss	%xmm5, 32(%r14)
	mulss	36(%r14), %xmm3
	addq	$36, %r14
	addss	%xmm2, %xmm3
	divss	%xmm4, %xmm3
	jmp	.LBB5_54
.LBB5_53:
	movl	4(%r14), %eax
	movl	%eax, 16(%r14)
	movl	%eax, 28(%r14)
	movl	8(%r14), %ecx
	movl	%ecx, 20(%r14)
	movl	%ecx, 32(%r14)
	movss	%xmm2, 24(%r14)
	movd	%eax, %xmm0
	movd	%ecx, %xmm1
	addq	$36, %r14
	movaps	%xmm2, %xmm3
.LBB5_54:
	movss	%xmm3, (%r14)
	cmpl	$0, 317096(%rsi)
	je	.LBB5_56
# BB#55:
	movq	stdout(%rip), %rdi
	movl	frame_no(%rip), %edx
	movl	16(%rsi), %ecx
	movl	316832(%rsi), %r8d
	movl	317072(%rsi), %r9d
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	cvtss2sd	%xmm2, %xmm2
	movslq	317044(%rsi), %rax
	leaq	(%rax,%rax,2), %rax
	leaq	80(%rsp,%rax,2), %rbp
	movl	$.L.str.4, %esi
	movb	$3, %al
	pushq	$0
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$16, %rsp
.Lcfi63:
	.cfi_adjust_cfa_offset -16
.LBB5_56:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_57:
	movq	stderr(%rip), %rdi
	movl	frame_no(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	%r12, %rdi
	callq	free
	jmp	.LBB5_56
.Lfunc_end5:
	.size	find_snr, .Lfunc_end5-find_snr
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI6_1:
	.long	128                     # 0x80
	.long	128                     # 0x80
	.long	128                     # 0x80
	.long	128                     # 0x80
	.text
	.globl	get_block
	.p2align	4, 0x90
	.type	get_block,@function
get_block:                              # @get_block
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi68:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 56
	subq	$520, %rsp              # imm = 0x208
.Lcfi70:
	.cfi_def_cfa_offset 576
.Lcfi71:
	.cfi_offset %rbx, -56
.Lcfi72:
	.cfi_offset %r12, -48
.Lcfi73:
	.cfi_offset %r13, -40
.Lcfi74:
	.cfi_offset %r14, -32
.Lcfi75:
	.cfi_offset %r15, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movq	%r9, %r13
	movslq	%edi, %rax
	movq	(%rsi,%rax,8), %rax
	cmpq	no_reference_picture(%rip), %rax
	jne	.LBB6_3
# BB#1:
	movq	%r8, %rdi
	movl	5672(%rdi), %esi
	cmpl	6104(%rdi), %esi
	jge	.LBB6_3
# BB#2:                                 # %.preheader
	movl	$.Lstr, %edi
	callq	puts
	movdqa	.LCPI6_1(%rip), %xmm0   # xmm0 = [128,128,128,128]
	movdqu	%xmm0, (%r13)
	movdqu	%xmm0, 16(%r13)
	movdqu	%xmm0, 32(%r13)
	movdqu	%xmm0, 48(%r13)
	jmp	.LBB6_43
.LBB6_3:
	movq	316920(%rax), %r11
	movq	%r11, get_block.cur_imgY(%rip)
	movl	%edx, %r10d
	andl	$3, %r10d
	movl	%ecx, %ebp
	andl	$3, %ebp
	movl	%edx, %ebx
	subl	%r10d, %ebx
	sarl	$2, %ebx
	movl	%ecx, %r15d
	subl	%ebp, %r15d
	movq	dec_picture(%rip), %rax
	movl	316880(%rax), %r14d
	movq	316936(%rax), %rsi
	movl	4(%r8), %edi
	cmpb	$0, (%rsi,%rdi)
	je	.LBB6_4
# BB#5:
	movl	316868(%rax), %r12d
	sarl	%r12d
	decl	%r12d
	jmp	.LBB6_6
.LBB6_4:
	movl	316884(%rax), %r12d
.LBB6_6:
	sarl	$2, %r15d
	movl	%ecx, %eax
	orl	%edx, %eax
	testb	$3, %al
	je	.LBB6_7
# BB#8:
	testl	%ebp, %ebp
	movl	%r14d, 120(%rsp)        # 4-byte Spill
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movq	%r8, 40(%rsp)           # 8-byte Spill
	je	.LBB6_9
# BB#13:
	movl	%edx, %eax
	andb	$3, %al
	movl	%r12d, 88(%rsp)         # 4-byte Spill
	movq	%r11, 112(%rsp)         # 8-byte Spill
	je	.LBB6_24
# BB#14:
	cmpb	$2, %al
	jne	.LBB6_28
# BB#15:                                # %.preheader481
	movl	%ecx, 132(%rsp)         # 4-byte Spill
	movq	%rbp, 160(%rsp)         # 8-byte Spill
	movslq	%r15d, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movslq	%ebx, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	192(%rsp), %r13
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_16:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_17 Depth 2
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rax), %rbp
	movl	$4294967294, %eax       # imm = 0xFFFFFFFE
	leal	(%rbp,%rax), %ecx
	cmpq	$2, %rbp
	cmovlel	%r8d, %ecx
	cmpl	%r14d, %ecx
	cmovgl	%r14d, %ecx
	leal	1(%rbp,%rax), %eax
	cmpq	$1, %rbp
	cmovlel	%r8d, %eax
	cmpl	%r14d, %eax
	cmovgl	%r14d, %eax
	leal	1(%rbp), %edi
	testq	%rbp, %rbp
	movl	$0, %ebx
	cmovgl	%ebp, %ebx
	cmovsl	%r8d, %edi
	cmpl	%r14d, %ebx
	cmovgl	%r14d, %ebx
	cmpl	%r14d, %edi
	cmovgl	%r14d, %edi
	leal	2(%rbp), %esi
	cmpq	$-2, %rbp
	cmovlel	%r8d, %esi
	cmpl	%r14d, %esi
	cmovgl	%r14d, %esi
	leal	3(%rbp), %edx
	cmpq	$-3, %rbp
	cmovlel	%r8d, %edx
	cmpl	%r14d, %edx
	cmovgl	%r14d, %edx
	movl	%ecx, 48(%rsp)          # 4-byte Spill
	movslq	%ecx, %rbp
	movl	%edx, 104(%rsp)         # 4-byte Spill
	movslq	%edx, %r8
	movl	%eax, 56(%rsp)          # 4-byte Spill
	movslq	%eax, %r9
	movl	%esi, 80(%rsp)          # 4-byte Spill
	movslq	%esi, %r10
	movl	%ebx, 72(%rsp)          # 4-byte Spill
	movslq	%ebx, %r12
	movl	%edi, 64(%rsp)          # 4-byte Spill
	movslq	%edi, %r11
	movq	$-2, %r14
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	88(%rsp), %edi          # 4-byte Reload
	movq	112(%rsp), %rbx         # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_17:                               #   Parent Loop BB6_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rcx,%r14), %rsi
	testq	%rsi, %rsi
	movl	$0, %eax
	cmovlel	%eax, %esi
	cmpl	%edi, %esi
	cmovgl	%edi, %esi
	movslq	%esi, %rsi
	movq	(%rbx,%rsi,8), %r15
	movzwl	(%r15,%rbp,2), %esi
	movzwl	(%r15,%r8,2), %edx
	addl	%esi, %edx
	movzwl	(%r15,%r9,2), %esi
	movzwl	(%r15,%r10,2), %eax
	addl	%esi, %eax
	imull	$-5, %eax, %eax
	addl	%edx, %eax
	movzwl	(%r15,%r12,2), %edx
	movzwl	(%r15,%r11,2), %esi
	addl	%edx, %esi
	leal	(%rsi,%rsi,4), %edx
	leal	(%rax,%rdx,4), %eax
	movl	%eax, (%r13)
	addq	$36, %r13
	incq	%r14
	cmpq	$7, %r14
	jne	.LBB6_17
# BB#18:                                #   in Loop: Header=BB6_16 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	movq	(%rsp), %r13            # 8-byte Reload
	addq	$4, %r13
	cmpq	$4, %rcx
	movl	120(%rsp), %r14d        # 4-byte Reload
	movl	$0, %r8d
	jne	.LBB6_16
# BB#19:                                # %.preheader480
	movq	%r15, get_block.cur_lineY(%rip)
	movl	48(%rsp), %eax          # 4-byte Reload
	movl	%eax, get_block.ipos_m2(%rip)
	movl	104(%rsp), %eax         # 4-byte Reload
	movl	%eax, get_block.ipos_p3(%rip)
	movl	56(%rsp), %eax          # 4-byte Reload
	movl	%eax, get_block.ipos_m1(%rip)
	movl	80(%rsp), %eax          # 4-byte Reload
	movl	%eax, get_block.ipos_p2(%rip)
	movl	72(%rsp), %eax          # 4-byte Reload
	movl	%eax, get_block.ipos(%rip)
	movl	64(%rsp), %eax          # 4-byte Reload
	movl	%eax, get_block.ipos_p1(%rip)
	movl	196(%rsp), %r8d
	movl	200(%rsp), %ebx
	movl	232(%rsp), %r14d
	movl	340(%rsp), %r13d
	movl	268(%rsp), %edi
	movl	236(%rsp), %r9d
	movl	344(%rsp), %ecx
	movl	272(%rsp), %eax
	movl	204(%rsp), %ebp
	movl	240(%rsp), %r11d
	movl	348(%rsp), %r12d
	leaq	384(%rsp), %rdx
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB6_20:                               # %.loopexit479648
                                        # =>This Inner Loop Header: Depth=1
	movl	%r14d, %r15d
	movl	%r15d, (%rsp)           # 4-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movl	%r9d, %esi
	movl	%esi, 120(%rsp)         # 4-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%r11d, 88(%rsp)         # 4-byte Spill
	movl	-48(%rdx), %eax
	addl	-156(%rdx), %eax
	imull	$-5, %eax, %r9d
	movl	-192(%rdx), %eax
	addl	-12(%rdx), %eax
	addl	%r9d, %eax
	movl	-84(%rdx), %edi
	addl	-120(%rdx), %edi
	leal	(%rdi,%rdi,4), %edi
	leal	512(%rax,%rdi,4), %eax
	movq	40(%rsp), %r14          # 8-byte Reload
	movl	5900(%r14), %edi
	sarl	$10, %eax
	movl	$0, %esi
	cmovsl	%esi, %eax
	cmpl	%edi, %eax
	cmovgl	%edi, %eax
	movq	8(%rsp), %r9            # 8-byte Reload
	movl	%eax, (%r9,%r10)
	movl	-80(%rdx), %esi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movl	-8(%rdx), %edi
	movl	%edi, 16(%rsp)          # 4-byte Spill
	addl	%r15d, %r13d
	imull	$-5, %r13d, %eax
	addl	%edi, %r8d
	addl	%eax, %r8d
	movq	56(%rsp), %r13          # 8-byte Reload
	leal	(%rsi,%r13), %eax
	leal	(%rax,%rax,4), %eax
	leal	512(%r8,%rax,4), %eax
	movl	5900(%r14), %edi
	sarl	$10, %eax
	movl	$0, %esi
	cmovsl	%esi, %eax
	cmpl	%edi, %eax
	cmovgl	%edi, %eax
	movl	%eax, 4(%r9,%r10)
	movl	-76(%rdx), %edi
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movl	-4(%rdx), %r8d
	movl	120(%rsp), %r15d        # 4-byte Reload
	addl	%r15d, %ecx
	imull	$-5, %ecx, %ecx
	addl	%r8d, %ebx
	addl	%ecx, %ebx
	movq	32(%rsp), %rax          # 8-byte Reload
	leal	(%rdi,%rax), %ecx
	leal	(%rcx,%rcx,4), %ecx
	leal	512(%rbx,%rcx,4), %ecx
	movl	5900(%r14), %edi
	sarl	$10, %ecx
	cmovsl	%esi, %ecx
	cmpl	%edi, %ecx
	cmovgl	%edi, %ecx
	movl	%ecx, 8(%r9,%r10)
	movl	-108(%rdx), %r11d
	movl	(%rdx), %ecx
	movl	88(%rsp), %ebx          # 4-byte Reload
	addl	%ebx, %r12d
	imull	$-5, %r12d, %esi
	addl	%ecx, %ebp
	addl	%esi, %ebp
	movl	-72(%rdx), %esi
	addl	%r11d, %esi
	leal	(%rsi,%rsi,4), %esi
	leal	512(%rbp,%rsi,4), %esi
	movl	5900(%r14), %edi
	sarl	$10, %esi
	movl	$0, %ebp
	cmovsl	%ebp, %esi
	cmpl	%edi, %esi
	cmovgl	%edi, %esi
	movq	%r13, %r14
	movl	%esi, 12(%r9,%r10)
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %r9
	addq	$36, %rdx
	addq	$16, %r10
	cmpq	$64, %r10
	movl	%ecx, %r12d
	movl	%ebx, %ebp
	movl	%r8d, %ecx
	movl	%r15d, %ebx
	movl	16(%rsp), %r13d         # 4-byte Reload
	movl	(%rsp), %eax            # 4-byte Reload
	movl	%eax, %r8d
	movq	48(%rsp), %rax          # 8-byte Reload
	jne	.LBB6_20
# BB#21:
	movl	$3, get_block.jpos_m2(%rip)
	movl	$8, get_block.jpos_p3(%rip)
	movl	$4, get_block.jpos_m1(%rip)
	movl	$7, get_block.jpos_p2(%rip)
	movl	$5, get_block.jpos(%rip)
	movl	$6, get_block.jpos_p1(%rip)
	testb	$1, 132(%rsp)           # 1-byte Folded Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	je	.LBB6_43
# BB#22:                                # %.preheader477
	movq	160(%rsp), %rax         # 8-byte Reload
	shrl	%eax
	orl	$2, %eax
	leaq	(%rax,%rax,8), %rax
	leaq	204(%rsp,%rax,4), %rax
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB6_23:                               # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rdx), %esi
	movl	5900(%rcx), %edi
	movl	-12(%rax), %ebp
	addl	$16, %ebp
	sarl	$5, %ebp
	cmovsl	%r8d, %ebp
	cmpl	%edi, %ebp
	cmovgl	%edi, %ebp
	leal	1(%rsi,%rbp), %esi
	sarl	%esi
	movl	%esi, (%rbx,%rdx)
	movl	4(%rbx,%rdx), %esi
	movl	5900(%rcx), %edi
	movl	-8(%rax), %ebp
	addl	$16, %ebp
	sarl	$5, %ebp
	cmovsl	%r8d, %ebp
	cmpl	%edi, %ebp
	cmovgl	%edi, %ebp
	leal	1(%rsi,%rbp), %esi
	sarl	%esi
	movl	%esi, 4(%rbx,%rdx)
	movl	8(%rbx,%rdx), %esi
	movl	5900(%rcx), %edi
	movl	-4(%rax), %ebp
	addl	$16, %ebp
	sarl	$5, %ebp
	cmovsl	%r8d, %ebp
	cmpl	%edi, %ebp
	cmovgl	%edi, %ebp
	leal	1(%rsi,%rbp), %esi
	sarl	%esi
	movl	%esi, 8(%rbx,%rdx)
	movl	12(%rbx,%rdx), %esi
	movl	5900(%rcx), %edi
	movl	(%rax), %ebp
	addl	$16, %ebp
	sarl	$5, %ebp
	cmovsl	%r8d, %ebp
	cmpl	%edi, %ebp
	cmovgl	%edi, %ebp
	leal	1(%rsi,%rbp), %esi
	sarl	%esi
	movl	%esi, 12(%rbx,%rdx)
	addq	$16, %rdx
	addq	$36, %rax
	cmpq	$64, %rdx
	jne	.LBB6_23
	jmp	.LBB6_43
.LBB6_7:                                # %.preheader461
	xorl	%r8d, %r8d
	testl	%ebx, %ebx
	movl	$0, %edx
	cmovnsl	%ebx, %edx
	cmpl	%r14d, %edx
	cmovgl	%r14d, %edx
	movslq	%edx, %r10
	movl	%ebx, %edi
	incl	%edi
	cmovsl	%r8d, %edi
	cmpl	%r14d, %edi
	cmovgl	%r14d, %edi
	movslq	%edi, %rdi
	movl	%ebx, %ebp
	addl	$2, %ebp
	cmovsl	%r8d, %ebp
	cmpl	%r14d, %ebp
	cmovgl	%r14d, %ebp
	movslq	%ebp, %rbp
	addl	$3, %ebx
	cmovsl	%r8d, %ebx
	cmpl	%r14d, %ebx
	cmovgl	%r14d, %ebx
	movslq	%ebx, %rdx
	leal	1(%r15), %ebx
	testl	%r15d, %r15d
	movl	$0, %ecx
	cmovnsl	%r15d, %ecx
	cmovsl	%r8d, %ebx
	cmpl	%r12d, %ecx
	cmovgl	%r12d, %ecx
	movslq	%ecx, %rcx
	movq	(%r11,%rcx,8), %rcx
	pinsrw	$0, (%rcx,%r10,2), %xmm1
	pinsrw	$1, (%rcx,%rdi,2), %xmm1
	pinsrw	$2, (%rcx,%rbp,2), %xmm1
	pinsrw	$3, (%rcx,%rdx,2), %xmm1
	pxor	%xmm0, %xmm0
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movdqu	%xmm1, (%r13)
	cmpl	%r12d, %ebx
	cmovgl	%r12d, %ebx
	movslq	%ebx, %rcx
	movq	(%r11,%rcx,8), %rcx
	pinsrw	$0, (%rcx,%r10,2), %xmm1
	pinsrw	$1, (%rcx,%rdi,2), %xmm1
	pinsrw	$2, (%rcx,%rbp,2), %xmm1
	pinsrw	$3, (%rcx,%rdx,2), %xmm1
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movdqu	%xmm1, 16(%r13)
	leal	2(%r15), %ecx
	cmpl	$-2, %r15d
	cmovlel	%r8d, %ecx
	cmpl	%r12d, %ecx
	cmovgl	%r12d, %ecx
	movslq	%ecx, %rcx
	movq	(%r11,%rcx,8), %rcx
	pinsrw	$0, (%rcx,%r10,2), %xmm1
	pinsrw	$1, (%rcx,%rdi,2), %xmm1
	pinsrw	$2, (%rcx,%rbp,2), %xmm1
	pinsrw	$3, (%rcx,%rdx,2), %xmm1
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movdqu	%xmm1, 32(%r13)
	leal	3(%r15), %ecx
	cmpl	$-3, %r15d
	cmovlel	%r8d, %ecx
	cmpl	%r12d, %ecx
	cmovgl	%r12d, %ecx
	movslq	%ecx, %rax
	movq	(%r11,%rax,8), %rax
	pinsrw	$0, (%rax,%r10,2), %xmm1
	pinsrw	$1, (%rax,%rdi,2), %xmm1
	pinsrw	$2, (%rax,%rbp,2), %xmm1
	pinsrw	$3, (%rax,%rdx,2), %xmm1
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movdqu	%xmm1, 48(%r13)
	movq	%rax, get_block.cur_lineY(%rip)
	jmp	.LBB6_43
.LBB6_9:                                # %.preheader465
	movl	%edx, 96(%rsp)          # 4-byte Spill
	movq	%r10, 136(%rsp)         # 8-byte Spill
	leal	1(%r15), %ecx
	xorl	%edi, %edi
	testl	%r15d, %r15d
	movl	$0, %edx
	cmovnsl	%r15d, %edx
	cmovsl	%edi, %ecx
	cmpl	%r12d, %edx
	cmovgl	%r12d, %edx
	movslq	%edx, %rax
	movslq	%ebx, %rdx
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	(%r11,%rax,8), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	cmpl	%r12d, %ecx
	cmovgl	%r12d, %ecx
	movslq	%ecx, %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movq	(%r11,%rax,8), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leal	2(%r15), %ecx
	cmpl	$-2, %r15d
	cmovlel	%edi, %ecx
	cmpl	%r12d, %ecx
	cmovgl	%r12d, %ecx
	movslq	%ecx, %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movq	(%r11,%rax,8), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leal	3(%r15), %ecx
	cmpl	$-3, %r15d
	cmovlel	%edi, %ecx
	cmpl	%r12d, %ecx
	cmovgl	%r12d, %ecx
	movslq	%ecx, %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	(%r11,%rax,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	-2(%rdx), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	decq	%rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	xorl	%r10d, %r10d
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_10:                               # =>This Inner Loop Header: Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r10), %r12
	leaq	2(%rax,%r10), %rdx
	cmpq	$2, %rdx
	movl	$0, %esi
	cmovlel	%esi, %r12d
	cmpl	%r14d, %r12d
	cmovgl	%r14d, %r12d
	movq	56(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r10), %r13d
	cmpq	$1, %rdx
	cmovlel	%esi, %r13d
	cmpl	%r14d, %r13d
	cmovgl	%r14d, %r13d
	testq	%rdx, %rdx
	movl	%edx, %r15d
	cmovlel	%esi, %r15d
	leal	1(%rbx,%r10), %ecx
	cmovsl	%esi, %ecx
	xorl	%esi, %esi
	cmpl	%r14d, %r15d
	cmovgl	%r14d, %r15d
	cmpl	%r14d, %ecx
	cmovgl	%r14d, %ecx
	cmpq	$-2, %rdx
	leal	2(%rbx,%r10), %ebp
	cmovlel	%esi, %ebp
	cmpl	%r14d, %ebp
	cmovgl	%r14d, %ebp
	cmpq	$-3, %rdx
	leal	3(%rbx,%r10), %r8d
	cmovlel	%esi, %r8d
	cmpl	%r14d, %r8d
	cmovgl	%r14d, %r8d
	movslq	%r12d, %r14
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movslq	%r8d, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movslq	%r13d, %rsi
	movq	%rsi, (%rsp)            # 8-byte Spill
	movslq	%ebp, %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movslq	%r15d, %rdi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movslq	%ecx, %rdx
	movq	64(%rsp), %rbx          # 8-byte Reload
	movzwl	(%rbx,%rsi,2), %r9d
	movzwl	(%rbx,%rax,2), %esi
	addl	%r9d, %esi
	movzwl	(%rbx,%rdi,2), %r11d
	movzwl	(%rbx,%rdx,2), %r9d
	addl	%r11d, %r9d
	movzwl	(%rbx,%r14,2), %r14d
	movq	16(%rsp), %rax          # 8-byte Reload
	movzwl	(%rbx,%rax,2), %r11d
	addl	%r14d, %r11d
	imull	$-5, %esi, %esi
	leal	(%r9,%r9,4), %edi
	addl	%esi, %r11d
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	5900(%rax), %esi
	leal	16(%r11,%rdi,4), %edi
	sarl	$5, %edi
	movl	$0, %eax
	cmovsl	%eax, %edi
	cmpl	%esi, %edi
	cmovgl	%esi, %edi
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%edi, (%rax,%r10,4)
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	(%rsp), %rax            # 8-byte Reload
	movzwl	(%rbx,%rax,2), %esi
	movq	88(%rsp), %rax          # 8-byte Reload
	movzwl	(%rbx,%rax,2), %edi
	addl	%esi, %edi
	movq	24(%rsp), %rax          # 8-byte Reload
	movzwl	(%rbx,%rax,2), %r9d
	movq	%rdx, %r14
	movzwl	(%rbx,%r14,2), %esi
	addl	%r9d, %esi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movzwl	(%rbx,%rdx,2), %r9d
	movq	16(%rsp), %r11          # 8-byte Reload
	movzwl	(%rbx,%r11,2), %ebx
	addl	%r9d, %ebx
	imull	$-5, %edi, %edi
	leal	(%rsi,%rsi,4), %esi
	addl	%edi, %ebx
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	5900(%rdi), %edi
	leal	16(%rbx,%rsi,4), %esi
	sarl	$5, %esi
	movl	$0, %ebx
	cmovsl	%ebx, %esi
	cmpl	%edi, %esi
	cmovgl	%edi, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%esi, 16(%rdi,%r10,4)
	movq	104(%rsp), %r11         # 8-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	movzwl	(%r11,%rsi,2), %esi
	movq	88(%rsp), %rdi          # 8-byte Reload
	movzwl	(%r11,%rdi,2), %edi
	addl	%esi, %edi
	movzwl	(%r11,%rax,2), %esi
	movzwl	(%r11,%r14,2), %ebx
	addl	%esi, %ebx
	movzwl	(%r11,%rdx,2), %r9d
	movq	16(%rsp), %rax          # 8-byte Reload
	movzwl	(%r11,%rax,2), %esi
	addl	%r9d, %esi
	imull	$-5, %edi, %edi
	addl	%edi, %esi
	leal	(%rbx,%rbx,4), %edi
	leal	16(%rsi,%rdi,4), %esi
	movq	40(%rsp), %r11          # 8-byte Reload
	movl	5900(%r11), %edi
	sarl	$5, %esi
	movl	$0, %edx
	cmovsl	%edx, %esi
	cmpl	%edi, %esi
	cmovgl	%edi, %esi
	movq	8(%rsp), %r9            # 8-byte Reload
	movl	%esi, 32(%r9,%r10,4)
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	(%rsp), %rdx            # 8-byte Reload
	movzwl	(%rbx,%rdx,2), %esi
	movq	88(%rsp), %rdx          # 8-byte Reload
	movzwl	(%rbx,%rdx,2), %edi
	addl	%esi, %edi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movzwl	(%rbx,%rdx,2), %edx
	movzwl	(%rbx,%r14,2), %esi
	movl	120(%rsp), %r14d        # 4-byte Reload
	addl	%edx, %esi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movzwl	(%rbx,%rdx,2), %edx
	movzwl	(%rbx,%rax,2), %ebx
	addl	%edx, %ebx
	imull	$-5, %edi, %edx
	addl	%edx, %ebx
	leal	(%rsi,%rsi,4), %edx
	leal	16(%rbx,%rdx,4), %edx
	movl	5900(%r11), %esi
	sarl	$5, %edx
	movl	$0, %eax
	cmovsl	%eax, %edx
	cmpl	%esi, %edx
	cmovgl	%esi, %edx
	movl	%edx, 48(%r9,%r10,4)
	movq	72(%rsp), %rbx          # 8-byte Reload
	incq	%r10
	cmpq	$4, %r10
	jne	.LBB6_10
# BB#11:
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, get_block.cur_lineY(%rip)
	movl	%r12d, get_block.ipos_m2(%rip)
	movl	%r8d, get_block.ipos_p3(%rip)
	movl	%r13d, get_block.ipos_m1(%rip)
	movl	%ebp, get_block.ipos_p2(%rip)
	movl	%r15d, get_block.ipos(%rip)
	movl	%ecx, get_block.ipos_p1(%rip)
	testb	$1, 96(%rsp)            # 1-byte Folded Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	je	.LBB6_43
# BB#12:                                # %.preheader463
	movq	get_block.cur_imgY(%rip), %rax
	movq	136(%rsp), %rdi         # 8-byte Reload
	shrl	%edi
	leal	1(%rdi), %ecx
	movl	%edi, %edx
	orl	$2, %edx
	leal	3(%rdi), %esi
	movd	%ebx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movd	%esi, %xmm1
	movd	%ecx, %xmm2
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movd	%edi, %xmm1
	movd	%edx, %xmm3
	punpckldq	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	paddd	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	movdqa	%xmm1, %xmm2
	pcmpgtd	%xmm0, %xmm2
	pand	%xmm1, %xmm2
	movd	%r14d, %xmm1
	pshufd	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movdqa	%xmm1, %xmm3
	pcmpgtd	%xmm2, %xmm3
	pand	%xmm3, %xmm2
	pandn	%xmm1, %xmm3
	por	%xmm2, %xmm3
	pshufd	$78, %xmm3, %xmm1       # xmm1 = xmm3[2,3,0,1]
	movdqa	%xmm1, %xmm2
	psrad	$31, %xmm2
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movdqa	%xmm3, %xmm2
	psrad	$31, %xmm2
	punpckldq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movq	168(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rbp
	movd	%xmm3, %rdx
	pshufd	$78, %xmm3, %xmm2       # xmm2 = xmm3[2,3,0,1]
	movd	%xmm2, %rsi
	movd	%xmm1, %rcx
	movdqu	(%r8), %xmm2
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	movd	%xmm1, %rdi
	pinsrw	$0, (%rbp,%rdx,2), %xmm3
	pinsrw	$1, (%rbp,%rsi,2), %xmm3
	pinsrw	$2, (%rbp,%rcx,2), %xmm3
	pinsrw	$3, (%rbp,%rdi,2), %xmm3
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	movdqa	.LCPI6_0(%rip), %xmm1   # xmm1 = [1,1,1,1]
	paddd	%xmm1, %xmm2
	paddd	%xmm3, %xmm2
	psrad	$1, %xmm2
	movdqu	%xmm2, (%r8)
	movq	176(%rsp), %rbp         # 8-byte Reload
	movq	(%rax,%rbp,8), %rbp
	movdqu	16(%r8), %xmm2
	pinsrw	$0, (%rbp,%rdx,2), %xmm3
	pinsrw	$1, (%rbp,%rsi,2), %xmm3
	pinsrw	$2, (%rbp,%rcx,2), %xmm3
	pinsrw	$3, (%rbp,%rdi,2), %xmm3
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	paddd	%xmm1, %xmm2
	paddd	%xmm3, %xmm2
	psrad	$1, %xmm2
	movdqu	%xmm2, 16(%r8)
	movq	184(%rsp), %rbp         # 8-byte Reload
	movq	(%rax,%rbp,8), %rbp
	movdqu	32(%r8), %xmm2
	pinsrw	$0, (%rbp,%rdx,2), %xmm3
	pinsrw	$1, (%rbp,%rsi,2), %xmm3
	pinsrw	$2, (%rbp,%rcx,2), %xmm3
	pinsrw	$3, (%rbp,%rdi,2), %xmm3
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	paddd	%xmm1, %xmm2
	paddd	%xmm3, %xmm2
	psrad	$1, %xmm2
	movdqu	%xmm2, 32(%r8)
	movq	144(%rsp), %rbp         # 8-byte Reload
	movq	(%rax,%rbp,8), %rax
	movdqu	48(%r8), %xmm2
	pinsrw	$0, (%rax,%rdx,2), %xmm3
	pinsrw	$1, (%rax,%rsi,2), %xmm3
	pinsrw	$2, (%rax,%rcx,2), %xmm3
	pinsrw	$3, (%rax,%rdi,2), %xmm3
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	paddd	%xmm1, %xmm2
	paddd	%xmm3, %xmm2
	psrad	$1, %xmm2
	movdqu	%xmm2, 48(%r8)
	movq	%rax, get_block.cur_lineY(%rip)
	jmp	.LBB6_43
.LBB6_24:                               # %.preheader476
	movl	%ecx, 132(%rsp)         # 4-byte Spill
	movq	%rbp, 160(%rsp)         # 8-byte Spill
	leal	1(%rbx), %ecx
	xorl	%eax, %eax
	testl	%ebx, %ebx
	movl	$0, %edx
	cmovnsl	%ebx, %edx
	cmovsl	%eax, %ecx
	cmpl	%r14d, %edx
	cmovgl	%r14d, %edx
	movslq	%edx, %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movslq	%r15d, %rdx
	cmpl	%r14d, %ecx
	cmovgl	%r14d, %ecx
	movslq	%ecx, %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leal	2(%rbx), %ecx
	cmpl	$-2, %ebx
	cmovlel	%eax, %ecx
	cmpl	%r14d, %ecx
	cmovgl	%r14d, %ecx
	movslq	%ecx, %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	leal	3(%rbx), %ecx
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	cmpl	$-3, %ebx
	cmovlel	%eax, %ecx
	cmpl	%r14d, %ecx
	cmovgl	%r14d, %ecx
	movslq	%ecx, %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	leaq	12(%r13), %rbx
	leaq	-2(%rdx), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	decq	%rdx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	movq	%r15, 152(%rsp)         # 8-byte Spill
	movl	%r15d, %eax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB6_25:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, (%rsp)            # 8-byte Spill
	movq	184(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r10), %rbp
	leaq	2(%rax,%r10), %rdx
	cmpq	$2, %rdx
	movl	$0, %edi
	cmovlel	%edi, %ebp
	cmpl	%r12d, %ebp
	cmovgl	%r12d, %ebp
	movq	104(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r10), %r11d
	cmpq	$1, %rdx
	cmovlel	%edi, %r11d
	cmpl	%r12d, %r11d
	cmovgl	%r12d, %r11d
	testq	%rdx, %rdx
	movl	%edx, %r9d
	cmovlel	%edi, %r9d
	movq	176(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rsi
	leal	1(%rsi,%r10), %eax
	cmovsl	%edi, %eax
	cmpl	%r12d, %r9d
	cmovgl	%r12d, %r9d
	cmpl	%r12d, %eax
	cmovgl	%r12d, %eax
	cmpq	$-2, %rdx
	leal	2(%rsi,%r10), %r13d
	cmovlel	%edi, %r13d
	cmpl	%r12d, %r13d
	cmovgl	%r12d, %r13d
	cmpq	$-3, %rdx
	leal	3(%rsi,%r10), %r15d
	cmovlel	%edi, %r15d
	cmpl	%r12d, %r15d
	cmovgl	%r12d, %r15d
	movq	%rbp, 168(%rsp)         # 8-byte Spill
	movslq	%ebp, %rdx
	movq	112(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi,%rdx,8), %r12
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movslq	%r15d, %rdx
	movq	(%rdi,%rdx,8), %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movslq	%r11d, %rdx
	movq	(%rdi,%rdx,8), %rbp
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movslq	%r13d, %rdx
	movq	(%rdi,%rdx,8), %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movslq	%r9d, %rsi
	movq	%r10, %r14
	movq	(%rdi,%rsi,8), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movslq	%eax, %rsi
	movq	(%rdi,%rsi,8), %rsi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	80(%rsp), %rdi          # 8-byte Reload
	movzwl	(%rbp,%rdi,2), %r10d
	movzwl	(%rdx,%rdi,2), %ebp
	addl	%r10d, %ebp
	movzwl	(%rcx,%rdi,2), %edx
	movq	%r14, %r10
	movzwl	(%rsi,%rdi,2), %r14d
	addl	%edx, %r14d
	movzwl	(%r12,%rdi,2), %esi
	movq	%r8, %rcx
	movq	32(%rsp), %rdx          # 8-byte Reload
	movzwl	(%rdx,%rdi,2), %r12d
	addl	%esi, %r12d
	imull	$-5, %ebp, %esi
	leal	(%r14,%r14,4), %ebp
	addl	%esi, %r12d
	movl	5900(%rcx), %esi
	leal	16(%r12,%rbp,4), %ebp
	sarl	$5, %ebp
	movl	$0, %edx
	cmovsl	%edx, %ebp
	cmpl	%esi, %ebp
	cmovgl	%esi, %ebp
	movl	%ebp, -12(%rbx)
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	movzwl	(%rdx,%rbx,2), %esi
	movq	64(%rsp), %r14          # 8-byte Reload
	movzwl	(%r14,%rbx,2), %ebp
	addl	%esi, %ebp
	movq	48(%rsp), %r8           # 8-byte Reload
	movzwl	(%r8,%rbx,2), %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movzwl	(%rdi,%rbx,2), %edi
	addl	%esi, %edi
	movq	24(%rsp), %rcx          # 8-byte Reload
	movzwl	(%rcx,%rbx,2), %esi
	movq	32(%rsp), %r12          # 8-byte Reload
	movzwl	(%r12,%rbx,2), %ebx
	addl	%esi, %ebx
	imull	$-5, %ebp, %esi
	leal	(%rdi,%rdi,4), %edi
	addl	%esi, %ebx
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	5900(%rcx), %esi
	leal	16(%rbx,%rdi,4), %edi
	sarl	$5, %edi
	movl	$0, %ebx
	cmovsl	%ebx, %edi
	cmpl	%esi, %edi
	cmovgl	%esi, %edi
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%edi, -8(%rsi)
	movq	144(%rsp), %rbp         # 8-byte Reload
	movzwl	(%rdx,%rbp,2), %esi
	movzwl	(%r14,%rbp,2), %edi
	addl	%esi, %edi
	movzwl	(%r8,%rbp,2), %esi
	movq	16(%rsp), %rcx          # 8-byte Reload
	movzwl	(%rcx,%rbp,2), %ebx
	addl	%esi, %ebx
	movq	24(%rsp), %r8           # 8-byte Reload
	movzwl	(%r8,%rbp,2), %esi
	movzwl	(%r12,%rbp,2), %ebp
	addl	%esi, %ebp
	imull	$-5, %edi, %esi
	addl	%esi, %ebp
	leal	(%rbx,%rbx,4), %esi
	leal	16(%rbp,%rsi,4), %esi
	movq	40(%rsp), %r14          # 8-byte Reload
	movl	5900(%r14), %edi
	sarl	$5, %esi
	movl	$0, %ebp
	cmovsl	%ebp, %esi
	cmpl	%edi, %esi
	cmovgl	%edi, %esi
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	%esi, -4(%rdx)
	movq	136(%rsp), %rbx         # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	movzwl	(%rdx,%rbx,2), %esi
	movq	64(%rsp), %rdx          # 8-byte Reload
	movzwl	(%rdx,%rbx,2), %edx
	addl	%esi, %edx
	movq	48(%rsp), %rsi          # 8-byte Reload
	movzwl	(%rsi,%rbx,2), %esi
	movzwl	(%rcx,%rbx,2), %edi
	addl	%esi, %edi
	movzwl	(%r8,%rbx,2), %esi
	movzwl	(%r12,%rbx,2), %ebx
	movl	88(%rsp), %r12d         # 4-byte Reload
	addl	%esi, %ebx
	imull	$-5, %edx, %edx
	addl	%edx, %ebx
	leal	(%rdi,%rdi,4), %edx
	leal	16(%rbx,%rdx,4), %edx
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%r14, %r8
	movl	5900(%r14), %esi
	sarl	$5, %edx
	cmovsl	%ebp, %edx
	cmpl	%esi, %edx
	cmovgl	%esi, %edx
	movl	%edx, (%rbx)
	incq	%r10
	addq	$16, %rbx
	cmpq	$4, %r10
	jne	.LBB6_25
# BB#26:
	movq	168(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, get_block.jpos_m2(%rip)
	movl	%r15d, get_block.jpos_p3(%rip)
	movl	%r11d, get_block.jpos_m1(%rip)
	movl	%r13d, get_block.jpos_p2(%rip)
	movl	%r9d, get_block.jpos(%rip)
	movl	%eax, get_block.jpos_p1(%rip)
	testb	$1, 132(%rsp)           # 1-byte Folded Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	120(%rsp), %ecx         # 4-byte Reload
	movq	72(%rsp), %rsi          # 8-byte Reload
	je	.LBB6_43
# BB#27:                                # %.preheader474
	movq	get_block.cur_imgY(%rip), %r9
	movq	160(%rsp), %rax         # 8-byte Reload
	shrl	%eax
	movq	152(%rsp), %r14         # 8-byte Reload
	leal	(%r14,%rax), %r11d
	xorl	%r8d, %r8d
	testl	%esi, %esi
	movl	$0, %edx
	cmovnsl	%esi, %edx
	cmpl	%ecx, %edx
	cmovgl	%ecx, %edx
	movslq	%edx, %r10
	movl	%esi, %edx
	incl	%edx
	cmovsl	%r8d, %edx
	cmpl	%ecx, %edx
	cmovgl	%ecx, %edx
	movslq	%edx, %rbx
	movl	%esi, %edx
	addl	$2, %edx
	cmovsl	%r8d, %edx
	cmpl	%ecx, %edx
	cmovgl	%ecx, %edx
	movslq	%edx, %rdx
	addl	$3, %esi
	cmovsl	%r8d, %esi
	cmpl	%ecx, %esi
	cmovgl	%ecx, %esi
	movslq	%esi, %rsi
	testl	%r11d, %r11d
	movl	$0, %ecx
	cmovnsl	%r11d, %ecx
	leal	1(%r14,%rax), %edi
	cmovsl	%r8d, %edi
	cmpl	%r12d, %ecx
	cmovgl	%r12d, %ecx
	movslq	%ecx, %rcx
	movq	(%r9,%rcx,8), %rcx
	movdqu	(%rbp), %xmm2
	pinsrw	$0, (%rcx,%r10,2), %xmm3
	pinsrw	$1, (%rcx,%rbx,2), %xmm3
	pinsrw	$2, (%rcx,%rdx,2), %xmm3
	pinsrw	$3, (%rcx,%rsi,2), %xmm3
	pxor	%xmm0, %xmm0
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	movdqa	.LCPI6_0(%rip), %xmm1   # xmm1 = [1,1,1,1]
	paddd	%xmm1, %xmm2
	paddd	%xmm3, %xmm2
	psrad	$1, %xmm2
	movdqu	%xmm2, (%rbp)
	cmpl	%r12d, %edi
	cmovgl	%r12d, %edi
	movslq	%edi, %rcx
	movq	(%r9,%rcx,8), %rcx
	movdqu	16(%rbp), %xmm2
	pinsrw	$0, (%rcx,%r10,2), %xmm3
	pinsrw	$1, (%rcx,%rbx,2), %xmm3
	pinsrw	$2, (%rcx,%rdx,2), %xmm3
	pinsrw	$3, (%rcx,%rsi,2), %xmm3
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	paddd	%xmm1, %xmm2
	paddd	%xmm3, %xmm2
	psrad	$1, %xmm2
	movdqu	%xmm2, 16(%rbp)
	leal	2(%r14,%rax), %ecx
	cmpl	$-2, %r11d
	cmovlel	%r8d, %ecx
	cmpl	%r12d, %ecx
	cmovgl	%r12d, %ecx
	movslq	%ecx, %rcx
	movq	(%r9,%rcx,8), %rcx
	movdqu	32(%rbp), %xmm2
	pinsrw	$0, (%rcx,%r10,2), %xmm3
	pinsrw	$1, (%rcx,%rbx,2), %xmm3
	pinsrw	$2, (%rcx,%rdx,2), %xmm3
	pinsrw	$3, (%rcx,%rsi,2), %xmm3
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	paddd	%xmm1, %xmm2
	paddd	%xmm3, %xmm2
	psrad	$1, %xmm2
	movdqu	%xmm2, 32(%rbp)
	leal	3(%r14,%rax), %ecx
	cmpl	$-3, %r11d
	cmovlel	%r8d, %ecx
	cmpl	%r12d, %ecx
	cmovgl	%r12d, %ecx
	movslq	%ecx, %rax
	movq	(%r9,%rax,8), %rax
	movdqu	48(%rbp), %xmm2
	pinsrw	$0, (%rax,%r10,2), %xmm3
	pinsrw	$1, (%rax,%rbx,2), %xmm3
	pinsrw	$2, (%rax,%rdx,2), %xmm3
	pinsrw	$3, (%rax,%rsi,2), %xmm3
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	paddd	%xmm1, %xmm2
	paddd	%xmm3, %xmm2
	psrad	$1, %xmm2
	movdqu	%xmm2, 48(%rbp)
	movq	%rax, get_block.cur_lineY(%rip)
	jmp	.LBB6_43
.LBB6_28:
	movq	%r10, 136(%rsp)         # 8-byte Spill
	cmpl	$2, %ebp
	jne	.LBB6_29
# BB#34:                                # %.preheader470
	movl	%edx, 96(%rsp)          # 4-byte Spill
	movslq	%ebx, %rcx
	movslq	%r15d, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	192(%rsp), %r13
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB6_35:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_36 Depth 2
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	leaq	(%rdx,%rax), %rdx
	movl	$4294967294, %esi       # imm = 0xFFFFFFFE
	leal	(%rdx,%rsi), %eax
	cmpq	$2, %rdx
	cmovlel	%ebx, %eax
	cmpl	%r12d, %eax
	cmovgl	%r12d, %eax
	leal	1(%rdx,%rsi), %r9d
	cmpq	$1, %rdx
	cmovlel	%ebx, %r9d
	cmpl	%r12d, %r9d
	cmovgl	%r12d, %r9d
	leal	1(%rdx), %edi
	testq	%rdx, %rdx
	movl	$0, %ebp
	cmovgl	%edx, %ebp
	cmovsl	%ebx, %edi
	cmpl	%r12d, %ebp
	cmovgl	%r12d, %ebp
	cmpl	%r12d, %edi
	cmovgl	%r12d, %edi
	xorl	%r8d, %r8d
	leal	2(%rdx), %ebx
	cmpq	$-2, %rdx
	cmovlel	%r8d, %ebx
	cmpl	%r12d, %ebx
	cmovgl	%r12d, %ebx
	leal	3(%rdx), %esi
	cmpq	$-3, %rdx
	cmovlel	%r8d, %esi
	cmpl	%r12d, %esi
	cmovgl	%r12d, %esi
	movl	%eax, 24(%rsp)          # 4-byte Spill
	cltq
	movq	(%r11,%rax,8), %r8
	movl	%esi, 80(%rsp)          # 4-byte Spill
	movslq	%esi, %rax
	movq	(%r11,%rax,8), %r15
	movl	%r9d, 48(%rsp)          # 4-byte Spill
	movslq	%r9d, %rax
	movq	(%r11,%rax,8), %r9
	movl	%ebx, 72(%rsp)          # 4-byte Spill
	movslq	%ebx, %rax
	movl	$0, %ebx
	movq	(%r11,%rax,8), %r10
	movl	%ebp, 64(%rsp)          # 4-byte Spill
	movslq	%ebp, %rax
	movq	(%r11,%rax,8), %rdx
	movl	%edi, 56(%rsp)          # 4-byte Spill
	movslq	%edi, %rax
	movq	(%r11,%rax,8), %r11
	movq	$-2, %r14
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	120(%rsp), %ebp         # 4-byte Reload
	.p2align	4, 0x90
.LBB6_36:                               #   Parent Loop BB6_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rcx,%r14), %rax
	testq	%rax, %rax
	cmovlel	%ebx, %eax
	cmpl	%ebp, %eax
	cmovgl	%ebp, %eax
	cltq
	movzwl	(%r8,%rax,2), %esi
	movzwl	(%r15,%rax,2), %r12d
	addl	%esi, %r12d
	movzwl	(%r9,%rax,2), %esi
	movzwl	(%r10,%rax,2), %edi
	addl	%esi, %edi
	imull	$-5, %edi, %esi
	addl	%r12d, %esi
	movzwl	(%rdx,%rax,2), %edi
	movzwl	(%r11,%rax,2), %eax
	addl	%edi, %eax
	leal	(%rax,%rax,4), %eax
	leal	(%rsi,%rax,4), %eax
	movl	%eax, (%r13)
	addq	$4, %r13
	incq	%r14
	cmpq	$7, %r14
	jne	.LBB6_36
# BB#37:                                #   in Loop: Header=BB6_35 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	movq	(%rsp), %r13            # 8-byte Reload
	addq	$36, %r13
	cmpq	$4, %rdx
	movl	88(%rsp), %r12d         # 4-byte Reload
	movq	112(%rsp), %r11         # 8-byte Reload
	jne	.LBB6_35
# BB#38:                                # %.preheader469
	movl	24(%rsp), %eax          # 4-byte Reload
	movl	%eax, get_block.jpos_m2(%rip)
	movl	80(%rsp), %eax          # 4-byte Reload
	movl	%eax, get_block.jpos_p3(%rip)
	movl	48(%rsp), %eax          # 4-byte Reload
	movl	%eax, get_block.jpos_m1(%rip)
	movl	72(%rsp), %eax          # 4-byte Reload
	movl	%eax, get_block.jpos_p2(%rip)
	movl	64(%rsp), %eax          # 4-byte Reload
	movl	%eax, get_block.jpos(%rip)
	movl	56(%rsp), %eax          # 4-byte Reload
	movl	%eax, get_block.jpos_p1(%rip)
	leaq	224(%rsp), %rax
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	40(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_39:                               # %.preheader468
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rax), %r9d
	movl	-16(%rax), %r10d
	movl	-28(%rax), %edi
	movl	-24(%rax), %ebx
	leal	(%r10,%rdi), %ecx
	imull	$-5, %ecx, %r11d
	movl	-20(%rax), %esi
	leal	(%rsi,%rbx), %ebp
	leal	(%rbp,%rbp,4), %r14d
	movl	5900(%r12), %ecx
	movl	-32(%rax), %ebp
	addl	%r9d, %ebp
	addl	%r11d, %ebp
	leal	512(%rbp,%r14,4), %ebp
	sarl	$10, %ebp
	cmovsl	%r8d, %ebp
	cmpl	%ecx, %ebp
	cmovgl	%ecx, %ebp
	movl	%ebp, (%r15,%rdx)
	movl	-8(%rax), %ecx
	leal	(%r9,%rbx), %ebp
	imull	$-5, %ebp, %r11d
	leal	(%r10,%rsi), %ebp
	leal	(%rbp,%rbp,4), %r14d
	movl	5900(%r12), %ebp
	addl	%ecx, %edi
	addl	%r11d, %edi
	leal	512(%rdi,%r14,4), %edi
	sarl	$10, %edi
	cmovsl	%r8d, %edi
	cmpl	%ebp, %edi
	cmovgl	%ebp, %edi
	movl	%edi, 4(%r15,%rdx)
	movl	-4(%rax), %edi
	leal	(%rcx,%rsi), %ebp
	imull	$-5, %ebp, %r11d
	leal	(%r9,%r10), %ebp
	leal	(%rbp,%rbp,4), %r14d
	movl	5900(%r12), %ebp
	addl	%edi, %ebx
	addl	%r11d, %ebx
	leal	512(%rbx,%r14,4), %ebx
	sarl	$10, %ebx
	cmovsl	%r8d, %ebx
	cmpl	%ebp, %ebx
	cmovgl	%ebp, %ebx
	movl	%ebx, 8(%r15,%rdx)
	addl	%r10d, %edi
	imull	$-5, %edi, %edi
	addl	%r9d, %ecx
	leal	(%rcx,%rcx,4), %ecx
	movl	5900(%r12), %ebp
	addl	(%rax), %esi
	addl	%edi, %esi
	leal	512(%rsi,%rcx,4), %ecx
	sarl	$10, %ecx
	cmovsl	%r8d, %ecx
	cmpl	%ebp, %ecx
	cmovgl	%ebp, %ecx
	movl	%ecx, 12(%r15,%rdx)
	addq	$36, %rax
	addq	$16, %rdx
	cmpq	$64, %rdx
	jne	.LBB6_39
# BB#40:
	testb	$1, 96(%rsp)            # 1-byte Folded Reload
	je	.LBB6_43
# BB#41:                                # %.preheader466
	movq	136(%rsp), %rcx         # 8-byte Reload
	shrl	%ecx
	movl	%ecx, %eax
	orl	$2, %eax
	movl	%ecx, %edx
	orl	$4, %edx
	leaq	192(%rsp,%rax,4), %r8
	addq	$12, %r15
	leaq	212(%rsp,%rcx,4), %rcx
	leaq	192(%rsp,%rdx,4), %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB6_42:                               # =>This Inner Loop Header: Depth=1
	movl	-12(%r15), %ebp
	movl	5900(%r12), %ebx
	movl	(%r8,%rdi), %eax
	addl	$16, %eax
	sarl	$5, %eax
	cmovsl	%esi, %eax
	cmpl	%ebx, %eax
	cmovgl	%ebx, %eax
	leal	1(%rbp,%rax), %eax
	sarl	%eax
	movl	%eax, -12(%r15)
	movl	-8(%r15), %eax
	movl	5900(%r12), %ebp
	movl	-8(%rcx,%rdi), %ebx
	addl	$16, %ebx
	sarl	$5, %ebx
	cmovsl	%esi, %ebx
	cmpl	%ebp, %ebx
	cmovgl	%ebp, %ebx
	leal	1(%rax,%rbx), %eax
	sarl	%eax
	movl	%eax, -8(%r15)
	movl	-4(%r15), %eax
	movl	5900(%r12), %ebp
	movl	(%rdx,%rdi), %ebx
	addl	$16, %ebx
	sarl	$5, %ebx
	cmovsl	%esi, %ebx
	cmpl	%ebp, %ebx
	cmovgl	%ebp, %ebx
	leal	1(%rax,%rbx), %eax
	sarl	%eax
	movl	%eax, -4(%r15)
	movl	(%r15), %eax
	movl	5900(%r12), %ebp
	movl	(%rcx,%rdi), %ebx
	addl	$16, %ebx
	sarl	$5, %ebx
	cmovsl	%esi, %ebx
	cmpl	%ebp, %ebx
	cmovgl	%ebp, %ebx
	leal	1(%rax,%rbx), %eax
	sarl	%eax
	movl	%eax, (%r15)
	addq	$36, %rdi
	addq	$16, %r15
	cmpq	$144, %rdi
	jne	.LBB6_42
	jmp	.LBB6_43
.LBB6_29:                               # %.preheader473
	xorl	%ecx, %ecx
	cmpl	$1, %ebp
	setne	%cl
	xorl	%r8d, %r8d
	leal	1(%rcx,%r15), %eax
	leal	2(%rcx,%r15), %edx
	leal	3(%rcx,%r15), %ebp
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	%r15, 152(%rsp)         # 8-byte Spill
	addl	%r15d, %ecx
	movl	$0, %edi
	cmovnsl	%ecx, %edi
	cmovsl	%r8d, %eax
	cmpl	%r12d, %edi
	cmovgl	%r12d, %edi
	movslq	%edi, %rdi
	movslq	%ebx, %rsi
	movq	(%r11,%rdi,8), %rdi
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	cmpl	%r12d, %eax
	cmovgl	%r12d, %eax
	cltq
	movq	(%r11,%rax,8), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	cmpl	$-2, %ecx
	cmovlel	%r8d, %edx
	cmpl	%r12d, %edx
	cmovgl	%r12d, %edx
	movslq	%edx, %rax
	movq	(%r11,%rax,8), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	cmpl	$-3, %ecx
	cmovlel	%r8d, %ebp
	cmpl	%r12d, %ebp
	cmovgl	%r12d, %ebp
	movslq	%ebp, %rax
	movq	(%r11,%rax,8), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	-2(%rsi), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	decq	%rsi
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	xorl	%r13d, %r13d
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_30:                               # =>This Inner Loop Header: Depth=1
	movq	144(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r13), %r11
	leaq	2(%rax,%r13), %rcx
	cmpq	$2, %rcx
	movl	$0, %esi
	cmovlel	%esi, %r11d
	cmpl	%r14d, %r11d
	cmovgl	%r14d, %r11d
	movq	112(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r13), %ebx
	cmpq	$1, %rcx
	cmovlel	%esi, %ebx
	cmpl	%r14d, %ebx
	cmovgl	%r14d, %ebx
	testq	%rcx, %rcx
	movl	%ecx, %edx
	cmovlel	%esi, %edx
	movq	72(%rsp), %rax          # 8-byte Reload
	leal	1(%rax,%r13), %r10d
	cmovsl	%esi, %r10d
	cmpl	%r14d, %edx
	cmovgl	%r14d, %edx
	cmpl	%r14d, %r10d
	cmovgl	%r14d, %r10d
	cmpq	$-2, %rcx
	leal	2(%rax,%r13), %edi
	cmovlel	%esi, %edi
	cmpl	%r14d, %edi
	cmovgl	%r14d, %edi
	cmpq	$-3, %rcx
	leal	3(%rax,%r13), %r9d
	cmovlel	%esi, %r9d
	cmpl	%r14d, %r9d
	cmovgl	%r14d, %r9d
	movslq	%r11d, %r14
	movslq	%r9d, %r15
	movslq	%ebx, %rsi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movslq	%edi, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movslq	%edx, %r8
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movslq	%r10d, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	80(%rsp), %rbp          # 8-byte Reload
	movzwl	(%rbp,%rsi,2), %r12d
	movzwl	(%rbp,%rax,2), %esi
	addl	%r12d, %esi
	movzwl	(%rbp,%r8,2), %r12d
	movzwl	(%rbp,%rcx,2), %eax
	addl	%r12d, %eax
	movzwl	(%rbp,%r14,2), %ecx
	movq	%r14, %r12
	movzwl	(%rbp,%r15,2), %r14d
	addl	%ecx, %r14d
	imull	$-5, %esi, %ecx
	leal	(%rax,%rax,4), %eax
	addl	%ecx, %r14d
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	5900(%rcx), %ecx
	leal	16(%r14,%rax,4), %eax
	sarl	$5, %eax
	movl	$0, %esi
	cmovsl	%esi, %eax
	cmpl	%ecx, %eax
	cmovgl	%ecx, %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%eax, (%rcx,%r13,4)
	movq	104(%rsp), %rbp         # 8-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	movzwl	(%rbp,%r8,2), %eax
	movq	(%rsp), %rcx            # 8-byte Reload
	movzwl	(%rbp,%rcx,2), %ecx
	addl	%eax, %ecx
	movq	32(%rsp), %r14          # 8-byte Reload
	movzwl	(%rbp,%r14,2), %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
	movzwl	(%rbp,%rsi,2), %esi
	addl	%eax, %esi
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movzwl	(%rbp,%r12,2), %eax
	movl	%eax, 56(%rsp)          # 4-byte Spill
	movzwl	(%rbp,%r15,2), %eax
	addl	56(%rsp), %eax          # 4-byte Folded Reload
	imull	$-5, %ecx, %ecx
	leal	(%rsi,%rsi,4), %esi
	addl	%ecx, %eax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	5900(%rcx), %ecx
	leal	16(%rax,%rsi,4), %eax
	sarl	$5, %eax
	movl	$0, %esi
	cmovsl	%esi, %eax
	cmpl	%ecx, %eax
	cmovgl	%ecx, %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%eax, 16(%rcx,%r13,4)
	movq	96(%rsp), %rbp          # 8-byte Reload
	movzwl	(%rbp,%r8,2), %eax
	movq	(%rsp), %rcx            # 8-byte Reload
	movzwl	(%rbp,%rcx,2), %ecx
	addl	%eax, %ecx
	movzwl	(%rbp,%r14,2), %eax
	movq	16(%rsp), %r8           # 8-byte Reload
	movzwl	(%rbp,%r8,2), %esi
	addl	%eax, %esi
	movzwl	(%rbp,%r12,2), %r14d
	movzwl	(%rbp,%r15,2), %eax
	addl	%r14d, %eax
	imull	$-5, %ecx, %ecx
	addl	%ecx, %eax
	leal	(%rsi,%rsi,4), %ecx
	leal	16(%rax,%rcx,4), %eax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	5900(%rcx), %ecx
	sarl	$5, %eax
	movl	$0, %esi
	cmovsl	%esi, %eax
	cmpl	%ecx, %eax
	cmovgl	%ecx, %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%eax, 32(%rcx,%r13,4)
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	movzwl	(%rbp,%rax,2), %eax
	movl	120(%rsp), %r14d        # 4-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	movzwl	(%rbp,%rcx,2), %ecx
	addl	%eax, %ecx
	movq	32(%rsp), %rax          # 8-byte Reload
	movzwl	(%rbp,%rax,2), %eax
	movq	8(%rsp), %r12           # 8-byte Reload
	movzwl	(%rbp,%r8,2), %esi
	addl	%eax, %esi
	movq	48(%rsp), %rax          # 8-byte Reload
	movzwl	(%rbp,%rax,2), %r8d
	movzwl	(%rbp,%r15,2), %eax
	addl	%r8d, %eax
	imull	$-5, %ecx, %ecx
	addl	%ecx, %eax
	leal	(%rsi,%rsi,4), %ecx
	movq	40(%rsp), %rsi          # 8-byte Reload
	leal	16(%rax,%rcx,4), %eax
	movl	5900(%rsi), %ecx
	sarl	$5, %eax
	movl	$0, %ebp
	cmovsl	%ebp, %eax
	cmpl	%ecx, %eax
	cmovgl	%ecx, %eax
	movl	%eax, 48(%r12,%r13,4)
	incq	%r13
	cmpq	$4, %r13
	jne	.LBB6_30
# BB#31:                                # %.preheader471
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, get_block.cur_lineY(%rip)
	movl	%r11d, get_block.ipos_m2(%rip)
	movl	%r9d, get_block.ipos_p3(%rip)
	movl	%ebx, get_block.ipos_m1(%rip)
	movl	%edi, get_block.ipos_p2(%rip)
	movl	%edx, get_block.ipos(%rip)
	movl	%r10d, get_block.ipos_p1(%rip)
	xorl	%ecx, %ecx
	cmpl	$1, 136(%rsp)           # 4-byte Folded Reload
	setne	%cl
	xorl	%ebp, %ebp
	movq	72(%rsp), %rdi          # 8-byte Reload
	leal	1(%rcx,%rdi), %eax
	leal	2(%rcx,%rdi), %edx
	leal	3(%rcx,%rdi), %esi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	addl	%edi, %ecx
	movq	get_block.cur_imgY(%rip), %rdi
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movl	$0, %edi
	cmovnsl	%ecx, %edi
	cmovsl	%ebp, %eax
	cmpl	%r14d, %edi
	cmovgl	%r14d, %edi
	movslq	%edi, %rdi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movq	152(%rsp), %rdi         # 8-byte Reload
	movslq	%edi, %rbx
	cmpl	%r14d, %eax
	cmovgl	%r14d, %eax
	cltq
	movq	%rax, 64(%rsp)          # 8-byte Spill
	cmpl	$-2, %ecx
	cmovlel	%ebp, %edx
	cmpl	%r14d, %edx
	cmovgl	%r14d, %edx
	movslq	%edx, %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	cmpl	$-3, %ecx
	cmovlel	%ebp, %esi
	cmpl	%r14d, %esi
	cmovgl	%r14d, %esi
	movslq	%esi, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	addq	$12, %r12
	leaq	-2(%rbx), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	decq	%rbx
	movq	%rbx, 120(%rsp)         # 8-byte Spill
	movl	%edi, %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_32:                               # =>This Inner Loop Header: Depth=1
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	80(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rcx), %rbp
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	2(%rax,%rcx), %rcx
	cmpq	$2, %rcx
	movl	$0, %edx
	cmovlel	%edx, %ebp
	movl	88(%rsp), %esi          # 4-byte Reload
	cmpl	%esi, %ebp
	cmovgl	%esi, %ebp
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	leal	(%rax,%rdi), %edi
	cmpq	$1, %rcx
	cmovlel	%edx, %edi
	cmpl	%esi, %edi
	cmovgl	%esi, %edi
	testq	%rcx, %rcx
	movl	%ecx, %r10d
	cmovlel	%edx, %r10d
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	leal	1(%rax,%rbx), %r15d
	cmovsl	%edx, %r15d
	cmpl	%esi, %r10d
	cmovgl	%esi, %r10d
	cmpl	%esi, %r15d
	cmovgl	%esi, %r15d
	cmpq	$-2, %rcx
	movq	8(%rsp), %rbx           # 8-byte Reload
	leal	2(%rax,%rbx), %r9d
	cmovlel	%edx, %r9d
	cmpl	%esi, %r9d
	cmovgl	%esi, %r9d
	cmpq	$-3, %rcx
	movq	8(%rsp), %rcx           # 8-byte Reload
	leal	3(%rax,%rcx), %r14d
	cmovlel	%edx, %r14d
	cmpl	%esi, %r14d
	cmovgl	%esi, %r14d
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	movslq	%ebp, %rax
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx,%rax,8), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movslq	%r14d, %rax
	movq	(%rdx,%rax,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movslq	%edi, %rax
	movq	(%rdx,%rax,8), %rsi
	movslq	%r9d, %rax
	movq	(%rdx,%rax,8), %rbp
	movq	%rbp, (%rsp)            # 8-byte Spill
	movslq	%r10d, %rax
	movq	(%rdx,%rax,8), %r8
	movslq	%r15d, %rax
	movq	(%rdx,%rax,8), %r11
	movq	56(%rsp), %rdx          # 8-byte Reload
	movzwl	(%rsi,%rdx,2), %eax
	movq	%rsi, %r13
	movzwl	(%rbp,%rdx,2), %esi
	addl	%eax, %esi
	movzwl	(%r8,%rdx,2), %eax
	movzwl	(%r11,%rdx,2), %ebx
	movq	%r11, %rbp
	addl	%eax, %ebx
	movzwl	(%rcx,%rdx,2), %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movzwl	(%rcx,%rdx,2), %edx
	addl	%eax, %edx
	imull	$-5, %esi, %eax
	leal	(%rbx,%rbx,4), %esi
	addl	%eax, %edx
	movl	-12(%r12), %eax
	leal	16(%rdx,%rsi,4), %edx
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	5900(%rsi), %esi
	sarl	$5, %edx
	movl	$0, %ebx
	cmovsl	%ebx, %edx
	cmpl	%esi, %edx
	cmovgl	%esi, %edx
	leal	1(%rax,%rdx), %eax
	sarl	%eax
	movl	%eax, -12(%r12)
	movq	64(%rsp), %rbx          # 8-byte Reload
	movzwl	(%r13,%rbx,2), %eax
	movq	%r13, %r11
	movq	%r11, 32(%rsp)          # 8-byte Spill
	movq	(%rsp), %rdx            # 8-byte Reload
	movzwl	(%rdx,%rbx,2), %edx
	addl	%eax, %edx
	movzwl	(%r8,%rbx,2), %eax
	movzwl	(%rbp,%rbx,2), %esi
	addl	%eax, %esi
	movq	16(%rsp), %r13          # 8-byte Reload
	movzwl	(%r13,%rbx,2), %eax
	movzwl	(%rcx,%rbx,2), %ebx
	addl	%eax, %ebx
	imull	$-5, %edx, %eax
	leal	(%rsi,%rsi,4), %edx
	addl	%eax, %ebx
	movl	-8(%r12), %eax
	leal	16(%rbx,%rdx,4), %edx
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	5900(%rsi), %esi
	sarl	$5, %edx
	movl	$0, %ebx
	cmovsl	%ebx, %edx
	cmpl	%esi, %edx
	cmovgl	%esi, %edx
	leal	1(%rax,%rdx), %eax
	sarl	%eax
	movl	%eax, -8(%r12)
	movq	112(%rsp), %rbx         # 8-byte Reload
	movzwl	(%r11,%rbx,2), %eax
	movq	(%rsp), %rdx            # 8-byte Reload
	movzwl	(%rdx,%rbx,2), %edx
	addl	%eax, %edx
	movzwl	(%r8,%rbx,2), %eax
	movq	%rbp, %r11
	movzwl	(%r11,%rbx,2), %esi
	addl	%eax, %esi
	movzwl	(%r13,%rbx,2), %eax
	movzwl	(%rcx,%rbx,2), %ebx
	movq	%rcx, %rbp
	addl	%eax, %ebx
	imull	$-5, %edx, %eax
	addl	%eax, %ebx
	leal	(%rsi,%rsi,4), %eax
	leal	16(%rbx,%rax,4), %eax
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	5900(%rdx), %edx
	sarl	$5, %eax
	movl	$0, %ebx
	cmovsl	%ebx, %eax
	cmpl	%edx, %eax
	cmovgl	%edx, %eax
	movl	-4(%r12), %edx
	leal	1(%rdx,%rax), %eax
	sarl	%eax
	movl	%eax, -4(%r12)
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	movzwl	(%rax,%rsi,2), %eax
	movq	(%rsp), %rcx            # 8-byte Reload
	movzwl	(%rcx,%rsi,2), %edx
	addl	%eax, %edx
	movzwl	(%r8,%rsi,2), %eax
	movzwl	(%r11,%rsi,2), %ecx
	addl	%eax, %ecx
	movzwl	(%r13,%rsi,2), %eax
	movzwl	(%rbp,%rsi,2), %esi
	addl	%eax, %esi
	imull	$-5, %edx, %eax
	addl	%eax, %esi
	leal	(%rcx,%rcx,4), %eax
	leal	16(%rsi,%rax,4), %eax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	5900(%rcx), %ecx
	sarl	$5, %eax
	cmovsl	%ebx, %eax
	cmpl	%ecx, %eax
	cmovgl	%ecx, %eax
	movl	(%r12), %ecx
	leal	1(%rcx,%rax), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	sarl	%eax
	movl	%eax, (%r12)
	incq	%rcx
	addq	$16, %r12
	cmpq	$4, %rcx
	jne	.LBB6_32
# BB#33:                                # %.loopexit472
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	%eax, get_block.jpos_m2(%rip)
	movl	%r14d, get_block.jpos_p3(%rip)
	movl	%edi, get_block.jpos_m1(%rip)
	movl	%r9d, get_block.jpos_p2(%rip)
	movl	%r10d, get_block.jpos(%rip)
	movl	%r15d, get_block.jpos_p1(%rip)
.LBB6_43:                               # %.loopexit
	addq	$520, %rsp              # imm = 0x208
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	get_block, .Lfunc_end6-get_block
	.cfi_endproc

	.globl	reorder_lists
	.p2align	4, 0x90
	.type	reorder_lists,@function
reorder_lists:                          # @reorder_lists
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi79:
	.cfi_def_cfa_offset 32
.Lcfi80:
	.cfi_offset %rbx, -24
.Lcfi81:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	cmpl	$2, %ebp
	je	.LBB7_16
# BB#1:
	cmpl	$4, %ebp
	je	.LBB7_16
# BB#2:
	cmpl	$0, 64(%rbx)
	je	.LBB7_4
# BB#3:
	movq	listX(%rip), %rdi
	movq	img(%rip), %rax
	movl	5640(%rax), %edx
	decl	%edx
	movq	72(%rbx), %rcx
	movq	80(%rbx), %r8
	movq	88(%rbx), %r9
	movl	$listXsize, %esi
	callq	reorder_ref_pic_list
.LBB7_4:
	movq	no_reference_picture(%rip), %rax
	movq	listX(%rip), %rcx
	movq	img(%rip), %rdx
	movslq	5640(%rdx), %rdx
	cmpq	-8(%rcx,%rdx,8), %rax
	jne	.LBB7_8
# BB#5:
	cmpl	$0, non_conforming_stream(%rip)
	je	.LBB7_7
# BB#6:
	movl	$.Lstr.2, %edi
	callq	puts
	jmp	.LBB7_8
.LBB7_7:
	movl	$.L.str.7, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
.LBB7_8:
	movq	img(%rip), %rax
	movl	5640(%rax), %ecx
	movl	%ecx, listXsize(%rip)
	cmpl	$1, %ebp
	jne	.LBB7_16
# BB#9:
	cmpl	$0, 96(%rbx)
	je	.LBB7_11
# BB#10:
	movq	listX+8(%rip), %rdi
	movl	5644(%rax), %edx
	decl	%edx
	movq	104(%rbx), %rcx
	movq	112(%rbx), %r8
	movq	120(%rbx), %r9
	movl	$listXsize+4, %esi
	callq	reorder_ref_pic_list
	movq	img(%rip), %rax
.LBB7_11:
	movq	no_reference_picture(%rip), %rcx
	movq	listX+8(%rip), %rdx
	movslq	5644(%rax), %rax
	cmpq	-8(%rdx,%rax,8), %rcx
	jne	.LBB7_15
# BB#12:
	cmpl	$0, non_conforming_stream(%rip)
	je	.LBB7_14
# BB#13:
	movl	$.Lstr.1, %edi
	callq	puts
	jmp	.LBB7_15
.LBB7_14:
	movl	$.L.str.9, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
.LBB7_15:
	movq	img(%rip), %rax
	movl	5644(%rax), %eax
	movl	%eax, listXsize+4(%rip)
.LBB7_16:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	free_ref_pic_list_reordering_buffer # TAILCALL
.Lfunc_end7:
	.size	reorder_lists, .Lfunc_end7-reorder_lists
	.cfi_endproc

	.globl	set_ref_pic_num
	.p2align	4, 0x90
	.type	set_ref_pic_num,@function
set_ref_pic_num:                        # @set_ref_pic_num
	.cfi_startproc
# BB#0:
	movq	img(%rip), %r9
	movslq	12(%r9), %r8
	movslq	listXsize(%rip), %r10
	testq	%r10, %r10
	jle	.LBB8_3
# BB#1:                                 # %.lr.ph71
	movq	listX(%rip), %r11
	movq	dec_picture(%rip), %rax
	imulq	$1584, %r8, %rdi        # imm = 0x630
	leaq	237624(%rax,%rdi), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%r11,%rax,8), %rcx
	movl	4(%rcx), %edx
	xorl	%esi, %esi
	cmpl	$2, (%rcx)
	sete	%sil
	leal	(%rsi,%rdx,2), %edx
	movslq	%edx, %rdx
	movq	%rdx, -237600(%rdi,%rax,8)
	movslq	16(%rcx), %rdx
	addq	%rdx, %rdx
	movq	%rdx, -158400(%rdi,%rax,8)
	movslq	8(%rcx), %rdx
	addq	%rdx, %rdx
	movq	%rdx, -79200(%rdi,%rax,8)
	movl	12(%rcx), %ecx
	leal	1(%rcx,%rcx), %ecx
	movslq	%ecx, %rcx
	movq	%rcx, (%rdi,%rax,8)
	incq	%rax
	cmpq	%r10, %rax
	jl	.LBB8_2
.LBB8_3:                                # %.preheader63
	movslq	listXsize+4(%rip), %r10
	testq	%r10, %r10
	jle	.LBB8_6
# BB#4:                                 # %.lr.ph67
	movq	listX+8(%rip), %r11
	movq	dec_picture(%rip), %rax
	imulq	$1584, %r8, %rdi        # imm = 0x630
	leaq	237888(%rax,%rdi), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_5:                                # =>This Inner Loop Header: Depth=1
	movq	(%r11,%rax,8), %rcx
	movl	4(%rcx), %edx
	xorl	%esi, %esi
	cmpl	$2, (%rcx)
	sete	%sil
	leal	(%rsi,%rdx,2), %edx
	movslq	%edx, %rdx
	movq	%rdx, -237600(%rdi,%rax,8)
	movslq	16(%rcx), %rdx
	addq	%rdx, %rdx
	movq	%rdx, -158400(%rdi,%rax,8)
	movslq	8(%rcx), %rdx
	addq	%rdx, %rdx
	movq	%rdx, -79200(%rdi,%rax,8)
	movl	12(%rcx), %ecx
	leal	1(%rcx,%rcx), %ecx
	movslq	%ecx, %rcx
	movq	%rcx, (%rdi,%rax,8)
	incq	%rax
	cmpq	%r10, %rax
	jl	.LBB8_5
.LBB8_6:                                # %._crit_edge68
	movq	active_sps(%rip), %rax
	cmpl	$0, 2076(%rax)
	jne	.LBB8_20
# BB#7:
	cmpl	$0, 5584(%r9)
	jne	.LBB8_20
# BB#8:                                 # %.preheader
	movq	dec_picture(%rip), %r9
	movslq	listXsize+8(%rip), %r10
	testq	%r10, %r10
	jle	.LBB8_11
# BB#9:                                 # %.lr.ph
	movq	listX+16(%rip), %r11
	imulq	$1584, %r8, %rax        # imm = 0x630
	leaq	238152(%r9,%rax), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_10:                               # =>This Inner Loop Header: Depth=1
	movq	(%r11,%rax,8), %rcx
	movl	4(%rcx), %edx
	xorl	%esi, %esi
	cmpl	$2, (%rcx)
	sete	%sil
	leal	(%rsi,%rdx,2), %edx
	movslq	%edx, %rdx
	movq	%rdx, -237600(%rdi,%rax,8)
	movslq	16(%rcx), %rdx
	addq	%rdx, %rdx
	movq	%rdx, -158400(%rdi,%rax,8)
	movslq	8(%rcx), %rdx
	addq	%rdx, %rdx
	movq	%rdx, -79200(%rdi,%rax,8)
	movl	12(%rcx), %ecx
	leal	1(%rcx,%rcx), %ecx
	movslq	%ecx, %rcx
	movq	%rcx, (%rdi,%rax,8)
	incq	%rax
	cmpq	%r10, %rax
	jl	.LBB8_10
.LBB8_11:                               # %._crit_edge
	movslq	listXsize+12(%rip), %r10
	testq	%r10, %r10
	jle	.LBB8_14
# BB#12:                                # %.lr.ph.1
	movq	listX+24(%rip), %r11
	imulq	$1584, %r8, %rax        # imm = 0x630
	leaq	238416(%r9,%rax), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_13:                               # =>This Inner Loop Header: Depth=1
	movq	(%r11,%rax,8), %rcx
	movl	4(%rcx), %edx
	xorl	%esi, %esi
	cmpl	$2, (%rcx)
	sete	%sil
	leal	(%rsi,%rdx,2), %edx
	movslq	%edx, %rdx
	movq	%rdx, -237600(%rdi,%rax,8)
	movslq	16(%rcx), %rdx
	addq	%rdx, %rdx
	movq	%rdx, -158400(%rdi,%rax,8)
	movslq	8(%rcx), %rdx
	addq	%rdx, %rdx
	movq	%rdx, -79200(%rdi,%rax,8)
	movl	12(%rcx), %ecx
	leal	1(%rcx,%rcx), %ecx
	movslq	%ecx, %rcx
	movq	%rcx, (%rdi,%rax,8)
	incq	%rax
	cmpq	%r10, %rax
	jl	.LBB8_13
.LBB8_14:                               # %._crit_edge.1
	movslq	listXsize+16(%rip), %r10
	testq	%r10, %r10
	jle	.LBB8_17
# BB#15:                                # %.lr.ph.2
	movq	listX+32(%rip), %r11
	imulq	$1584, %r8, %rax        # imm = 0x630
	leaq	238680(%r9,%rax), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_16:                               # =>This Inner Loop Header: Depth=1
	movq	(%r11,%rax,8), %rcx
	movl	4(%rcx), %edx
	xorl	%esi, %esi
	cmpl	$2, (%rcx)
	sete	%sil
	leal	(%rsi,%rdx,2), %edx
	movslq	%edx, %rdx
	movq	%rdx, -237600(%rdi,%rax,8)
	movslq	16(%rcx), %rdx
	addq	%rdx, %rdx
	movq	%rdx, -158400(%rdi,%rax,8)
	movslq	8(%rcx), %rdx
	addq	%rdx, %rdx
	movq	%rdx, -79200(%rdi,%rax,8)
	movl	12(%rcx), %ecx
	leal	1(%rcx,%rcx), %ecx
	movslq	%ecx, %rcx
	movq	%rcx, (%rdi,%rax,8)
	incq	%rax
	cmpq	%r10, %rax
	jl	.LBB8_16
.LBB8_17:                               # %._crit_edge.2
	movslq	listXsize+20(%rip), %r10
	testq	%r10, %r10
	jle	.LBB8_20
# BB#18:                                # %.lr.ph.3
	movq	listX+40(%rip), %r11
	imulq	$1584, %r8, %rax        # imm = 0x630
	leaq	238944(%r9,%rax), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_19:                               # =>This Inner Loop Header: Depth=1
	movq	(%r11,%rcx,8), %rdi
	movl	4(%rdi), %edx
	xorl	%esi, %esi
	cmpl	$2, (%rdi)
	sete	%sil
	leal	(%rsi,%rdx,2), %edx
	movslq	%edx, %rdx
	movq	%rdx, -237600(%rax,%rcx,8)
	movslq	16(%rdi), %rdx
	addq	%rdx, %rdx
	movq	%rdx, -158400(%rax,%rcx,8)
	movslq	8(%rdi), %rdx
	addq	%rdx, %rdx
	movq	%rdx, -79200(%rax,%rcx,8)
	movl	12(%rdi), %edx
	leal	1(%rdx,%rdx), %edx
	movslq	%edx, %rdx
	movq	%rdx, (%rax,%rcx,8)
	incq	%rcx
	cmpq	%r10, %rcx
	jl	.LBB8_19
.LBB8_20:                               # %.loopexit
	retq
.Lfunc_end8:
	.size	set_ref_pic_num, .Lfunc_end8-set_ref_pic_num
	.cfi_endproc

	.globl	read_new_slice
	.p2align	4, 0x90
	.type	read_new_slice,@function
read_new_slice:                         # @read_new_slice
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi85:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi88:
	.cfi_def_cfa_offset 64
.Lcfi89:
	.cfi_offset %rbx, -56
.Lcfi90:
	.cfi_offset %r12, -48
.Lcfi91:
	.cfi_offset %r13, -40
.Lcfi92:
	.cfi_offset %r14, -32
.Lcfi93:
	.cfi_offset %r15, -24
.Lcfi94:
	.cfi_offset %rbp, -16
	movl	$8000000, %edi          # imm = 0x7A1200
	xorl	%eax, %eax
	callq	AllocNALU
	movq	%rax, %rbx
	movq	img(%rip), %rax
	movq	5592(%rax), %r13
	leaq	4(%rsp), %rbp
	movl	$.L.str.11, %r14d
	jmp	.LBB9_1
.LBB9_71:                               #   in Loop: Header=BB9_1 Depth=1
	movl	4(%rbx), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.3, %edi
	callq	puts
	.p2align	4, 0x90
.LBB9_1:                                # %.backedge
                                        # =>This Inner Loop Header: Depth=1
	movq	bits(%rip), %rdi
	callq	ftell
	movq	input(%rip), %rax
	cmpl	$0, 3000(%rax)
	je	.LBB9_2
# BB#3:                                 #   in Loop: Header=BB9_1 Depth=1
	movq	%rbx, %rdi
	callq	GetRTPNALU
	jmp	.LBB9_4
	.p2align	4, 0x90
.LBB9_2:                                #   in Loop: Header=BB9_1 Depth=1
	movq	%rbx, %rdi
	callq	GetAnnexbNALU
.LBB9_4:                                #   in Loop: Header=BB9_1 Depth=1
	movl	%eax, 4(%rsp)
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	CheckZeroByteNonVCL
	movq	%rbx, %rdi
	callq	NALUtoRBSP
	movl	4(%rsp), %eax
	testl	%eax, %eax
	jns	.LBB9_6
# BB#5:                                 #   in Loop: Header=BB9_1 Depth=1
	movq	input(%rip), %rax
	cmpl	$0, 3000(%rax)
	movl	$.L.str.12, %esi
	cmoveq	%r14, %rsi
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	printf
	movl	4(%rsp), %eax
.LBB9_6:                                #   in Loop: Header=BB9_1 Depth=1
	testl	%eax, %eax
	je	.LBB9_7
# BB#8:                                 #   in Loop: Header=BB9_1 Depth=1
	cmpl	$0, 20(%rbx)
	je	.LBB9_10
# BB#9:                                 #   in Loop: Header=BB9_1 Depth=1
	movl	$.Lstr.10, %edi
	callq	puts
.LBB9_10:                               #   in Loop: Header=BB9_1 Depth=1
	movl	12(%rbx), %esi
	leal	-1(%rsi), %eax
	cmpl	$11, %eax
	ja	.LBB9_72
# BB#11:                                #   in Loop: Header=BB9_1 Depth=1
	jmpq	*.LJTI9_0(,%rax,8)
.LBB9_12:                               #   in Loop: Header=BB9_1 Depth=1
	movq	img(%rip), %rax
	cmpl	$0, 6088(%rax)
	movl	6092(%rax), %ecx
	jne	.LBB9_14
# BB#13:                                #   in Loop: Header=BB9_1 Depth=1
	cmpl	$5, %esi
	je	.LBB9_14
# BB#19:                                #   in Loop: Header=BB9_1 Depth=1
	testl	%ecx, %ecx
	je	.LBB9_1
	jmp	.LBB9_20
.LBB9_72:                               #   in Loop: Header=BB9_1 Depth=1
	movl	4(%rbx), %edx
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	jmp	.LBB9_1
.LBB9_66:                               #   in Loop: Header=BB9_1 Depth=1
	movl	$.Lstr.5, %edi
	callq	puts
	jmp	.LBB9_1
.LBB9_67:                               #   in Loop: Header=BB9_1 Depth=1
	movl	$.Lstr.4, %edi
	callq	puts
	jmp	.LBB9_1
.LBB9_68:                               #   in Loop: Header=BB9_1 Depth=1
	movl	4(%rbx), %esi
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	callq	printf
	movq	24(%rbx), %rdi
	movl	4(%rbx), %esi
	movq	img(%rip), %rdx
	callq	InterpretSEIMessage
	jmp	.LBB9_1
.LBB9_70:                               #   in Loop: Header=BB9_1 Depth=1
	movq	%rbx, %rdi
	callq	ProcessSPS
	jmp	.LBB9_1
.LBB9_69:                               #   in Loop: Header=BB9_1 Depth=1
	movq	%rbx, %rdi
	callq	ProcessPPS
	jmp	.LBB9_1
.LBB9_7:
	movq	%rbx, %rdi
	callq	FreeNALU
	movl	$1, %r12d
	jmp	.LBB9_46
.LBB9_32:
	movq	img(%rip), %rax
	movl	$0, 5804(%rax)
	movl	16(%rbx), %ecx
	movl	%ecx, 5808(%rax)
	movl	$1, 28(%r13)
	movl	$3, 24(%r13)
	movl	$0, (%r13)
	movq	40(%r13), %rax
	movq	(%rax), %rbp
	movl	$0, 24(%rbp)
	movl	$0, (%rbp)
	movl	$0, 8(%rbp)
	movq	16(%rbp), %rdi
	movq	24(%rbx), %rsi
	incq	%rsi
	movl	4(%rbx), %edx
	decl	%edx
	callq	memcpy
	movq	16(%rbp), %rdi
	movl	4(%rbx), %esi
	decl	%esi
	callq	RBSPtoSODB
	movl	%eax, 12(%rbp)
	movl	%eax, 4(%rbp)
	xorl	%eax, %eax
	callq	FirstPartOfSliceHeader
	movl	148(%r13), %edi
	callq	UseParameterSet
	xorl	%eax, %eax
	callq	RestOfSliceHeader
	movq	active_pps(%rip), %rdi
	movq	active_sps(%rip), %rsi
	callq	FmoInit
	callq	is_new_picture
	testl	%eax, %eax
	je	.LBB9_33
# BB#34:
	movq	img(%rip), %rdi
	movq	input(%rip), %rsi
	callq	init_picture
	leaq	4(%rsp), %rsi
	movq	%rbx, %rdi
	callq	CheckZeroByteVCL
	movl	$2, %r12d
	jmp	.LBB9_35
.LBB9_33:
	movl	$3, %r12d
.LBB9_35:
	movq	img(%rip), %rax
	movl	44(%rax), %edi
	movq	5592(%rax), %rax
	movl	16(%rax), %esi
	callq	init_lists
	movq	img(%rip), %rax
	movl	44(%rax), %edi
	movq	5592(%rax), %rsi
	callq	reorder_lists
	movq	img(%rip), %rax
	cmpl	$0, 5584(%rax)
	jne	.LBB9_37
# BB#36:
	xorl	%eax, %eax
	callq	init_mbaff_lists
	movq	img(%rip), %rax
.LBB9_37:
	cmpl	$0, 5624(%rax)
	setne	%cl
	movl	20(%r13), %edx
	shll	%cl, %edx
	movl	%edx, 4(%rax)
	movl	$.L.str.16, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movl	%eax, %r14d
	movq	active_pps(%rip), %rax
	cmpl	$0, 12(%rax)
	je	.LBB9_39
# BB#38:
	movl	$.L.str.17, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
.LBB9_39:
	movq	bits(%rip), %rdi
	callq	ftell
	movq	%rax, %r15
	movq	input(%rip), %rax
	cmpl	$0, 3000(%rax)
	je	.LBB9_40
# BB#41:
	movq	%rbx, %rdi
	callq	GetRTPNALU
	jmp	.LBB9_42
.LBB9_40:
	movq	%rbx, %rdi
	callq	GetAnnexbNALU
.LBB9_42:
	movl	%eax, 4(%rsp)
	leaq	4(%rsp), %rsi
	movq	%rbx, %rdi
	callq	CheckZeroByteNonVCL
	movq	%rbx, %rdi
	callq	NALUtoRBSP
	movl	4(%rsp), %eax
	testl	%eax, %eax
	jns	.LBB9_44
# BB#43:
	movq	input(%rip), %rax
	cmpl	$0, 3000(%rax)
	movl	$.L.str.11, %eax
	movl	$.L.str.12, %esi
	cmoveq	%rax, %rsi
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	printf
	movl	4(%rsp), %eax
.LBB9_44:
	testl	%eax, %eax
	je	.LBB9_45
# BB#47:
	movl	12(%rbx), %eax
	cmpl	$3, %eax
	jne	.LBB9_59
# BB#48:
	movq	40(%r13), %rax
	movq	56(%rax), %rbp
	movl	$0, 24(%rbp)
	movl	$0, (%rbp)
	movl	$0, 8(%rbp)
	movq	16(%rbp), %rdi
	movq	24(%rbx), %rsi
	incq	%rsi
	movl	4(%rbx), %edx
	decl	%edx
	callq	memcpy
	movq	16(%rbp), %rdi
	movl	4(%rbx), %esi
	decl	%esi
	callq	RBSPtoSODB
	movl	%eax, 12(%rbp)
	movl	%eax, 4(%rbp)
	movl	$.L.str.18, %edi
	movq	%rbp, %rsi
	callq	ue_v
	cmpl	%r14d, %eax
	je	.LBB9_50
# BB#49:
	movl	$.Lstr.7, %edi
	callq	puts
.LBB9_50:
	movq	active_pps(%rip), %rax
	cmpl	$0, 1152(%rax)
	je	.LBB9_52
# BB#51:
	movl	$.L.str.20, %edi
	movq	%rbp, %rsi
	callq	ue_v
.LBB9_52:
	movq	bits(%rip), %rdi
	callq	ftell
	movq	%rax, %r15
	movq	input(%rip), %rax
	cmpl	$0, 3000(%rax)
	je	.LBB9_53
# BB#54:
	movq	%rbx, %rdi
	callq	GetRTPNALU
	jmp	.LBB9_55
.LBB9_14:
	testl	%ecx, %ecx
	jne	.LBB9_18
# BB#15:
	xorl	%ecx, %ecx
	cmpl	$5, %esi
	je	.LBB9_17
# BB#16:
	movl	$.Lstr.9, %edi
	callq	puts
	movl	$1, %ecx
	movq	img(%rip), %rax
.LBB9_17:
	movl	%ecx, non_conforming_stream(%rip)
.LBB9_18:                               # %.thread
	movl	$1, 6092(%rax)
.LBB9_20:                               # %.loopexit
	xorl	%ecx, %ecx
	cmpl	$5, 12(%rbx)
	sete	%cl
	movl	%ecx, 5804(%rax)
	movl	16(%rbx), %ecx
	movl	%ecx, 5808(%rax)
	movl	$0, 28(%r13)
	movl	$1, 24(%r13)
	movl	$0, (%r13)
	movq	40(%r13), %rax
	movq	(%rax), %r14
	movl	$0, 24(%r14)
	movl	$0, (%r14)
	movl	$0, 8(%r14)
	movq	16(%r14), %rdi
	movq	24(%rbx), %rsi
	incq	%rsi
	movl	4(%rbx), %edx
	decl	%edx
	callq	memcpy
	movq	16(%r14), %rdi
	movl	4(%rbx), %esi
	decl	%esi
	callq	RBSPtoSODB
	movl	%eax, 12(%r14)
	movl	%eax, 4(%r14)
	xorl	%eax, %eax
	callq	FirstPartOfSliceHeader
	movl	148(%r13), %edi
	callq	UseParameterSet
	xorl	%eax, %eax
	callq	RestOfSliceHeader
	movq	active_pps(%rip), %rdi
	movq	active_sps(%rip), %rsi
	callq	FmoInit
	movq	active_pps(%rip), %rdi
	movq	active_sps(%rip), %rsi
	callq	AssignQuantParam
	movq	img(%rip), %rbp
	cmpl	$0, 5652(%rbp)
	je	.LBB9_24
# BB#21:                                # %.loopexit
	movl	Is_primary_correct(%rip), %eax
	testl	%eax, %eax
	jne	.LBB9_24
# BB#22:                                # %.loopexit
	movl	Is_redundant_correct(%rip), %eax
	testl	%eax, %eax
	je	.LBB9_24
# BB#23:
	movl	44(%rbp), %eax
	movq	dec_picture(%rip), %rcx
	movl	%eax, 317024(%rcx)
.LBB9_24:
	callq	is_new_picture
	testl	%eax, %eax
	je	.LBB9_25
# BB#26:
	movq	input(%rip), %rsi
	movq	%rbp, %rdi
	callq	init_picture
	leaq	4(%rsp), %rsi
	movq	%rbx, %rdi
	callq	CheckZeroByteVCL
	movl	$2, %r12d
	movq	img(%rip), %rbp
	jmp	.LBB9_27
.LBB9_25:
	movl	$3, %r12d
.LBB9_27:
	movl	44(%rbp), %edi
	movq	5592(%rbp), %rax
	movl	16(%rax), %esi
	callq	init_lists
	movq	img(%rip), %rax
	movl	44(%rax), %edi
	movq	5592(%rax), %rsi
	callq	reorder_lists
	movq	img(%rip), %rax
	cmpl	$0, 5584(%rax)
	jne	.LBB9_29
# BB#28:
	xorl	%eax, %eax
	callq	init_mbaff_lists
	movq	img(%rip), %rax
.LBB9_29:
	cmpl	$0, 5624(%rax)
	setne	%cl
	movl	20(%r13), %edx
	shll	%cl, %edx
	movl	%edx, 4(%rax)
	movq	active_pps(%rip), %rcx
	cmpl	$0, 12(%rcx)
	je	.LBB9_31
# BB#30:
	movl	8(%r14), %ecx
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	sarl	$3, %edx
	andl	$7, %ecx
	cmpl	$1, %ecx
	sbbl	$-1, %edx
	movq	40(%r13), %rdi
	addq	$8, %rdi
	movq	16(%r14), %rsi
	movl	44(%rax), %r8d
	movq	%r14, %rcx
	callq	arideco_start_decoding
.LBB9_31:
	movq	%rbx, %rdi
	callq	FreeNALU
	movq	img(%rip), %rax
	movl	$0, 6088(%rax)
	jmp	.LBB9_46
.LBB9_53:
	movq	%rbx, %rdi
	callq	GetAnnexbNALU
.LBB9_55:
	movl	%eax, 4(%rsp)
	leaq	4(%rsp), %rsi
	movq	%rbx, %rdi
	callq	CheckZeroByteNonVCL
	movq	%rbx, %rdi
	callq	NALUtoRBSP
	movl	4(%rsp), %eax
	testl	%eax, %eax
	jns	.LBB9_57
# BB#56:
	movq	input(%rip), %rax
	cmpl	$0, 3000(%rax)
	movl	$.L.str.11, %eax
	movl	$.L.str.12, %esi
	cmoveq	%rax, %rsi
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	printf
	movl	4(%rsp), %eax
.LBB9_57:
	testl	%eax, %eax
	je	.LBB9_45
# BB#58:                                # %thread-pre-split
	movl	12(%rbx), %eax
.LBB9_59:
	cmpl	$4, %eax
	jne	.LBB9_64
# BB#60:
	movq	40(%r13), %rax
	movq	112(%rax), %rbp
	movl	$0, 24(%rbp)
	movl	$0, (%rbp)
	movl	$0, 8(%rbp)
	movq	16(%rbp), %rdi
	movq	24(%rbx), %rsi
	incq	%rsi
	movl	4(%rbx), %edx
	decl	%edx
	callq	memcpy
	movq	16(%rbp), %rdi
	movl	4(%rbx), %esi
	decl	%esi
	callq	RBSPtoSODB
	movl	%eax, 12(%rbp)
	movl	%eax, 4(%rbp)
	movl	$.L.str.21, %edi
	movq	%rbp, %rsi
	callq	ue_v
	cmpl	%r14d, %eax
	je	.LBB9_62
# BB#61:
	movl	$.Lstr.6, %edi
	callq	puts
.LBB9_62:
	movq	active_pps(%rip), %rax
	cmpl	$0, 1152(%rax)
	je	.LBB9_64
# BB#63:
	movl	$.L.str.23, %edi
	movq	%rbp, %rsi
	callq	ue_v
.LBB9_64:
	movl	12(%rbx), %eax
	addl	$-3, %eax
	cmpl	$2, %eax
	jb	.LBB9_45
# BB#65:
	movq	bits(%rip), %rdi
	xorl	%edx, %edx
	movq	%r15, %rsi
	callq	fseek
.LBB9_45:
	movq	%rbx, %rdi
	callq	FreeNALU
.LBB9_46:
	movl	%r12d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	read_new_slice, .Lfunc_end9-read_new_slice
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI9_0:
	.quad	.LBB9_12
	.quad	.LBB9_32
	.quad	.LBB9_66
	.quad	.LBB9_67
	.quad	.LBB9_12
	.quad	.LBB9_68
	.quad	.LBB9_70
	.quad	.LBB9_69
	.quad	.LBB9_1
	.quad	.LBB9_1
	.quad	.LBB9_1
	.quad	.LBB9_71

	.text
	.globl	init_picture
	.p2align	4, 0x90
	.type	init_picture,@function
init_picture:                           # @init_picture
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 32
.Lcfi98:
	.cfi_offset %rbx, -32
.Lcfi99:
	.cfi_offset %r14, -24
.Lcfi100:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	5592(%rbx), %r15
	cmpq	$0, dec_picture(%rip)
	je	.LBB10_2
# BB#1:
	callq	exit_picture
.LBB10_2:
	movl	6088(%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB10_4
# BB#3:
	movl	6096(%rbx), %eax
	addl	5676(%rbx), %eax
	xorl	%edx, %edx
	divl	5816(%rbx)
	movl	%edx, 6100(%rbx)
.LBB10_4:
	cmpl	$0, 5804(%rbx)
	je	.LBB10_6
# BB#5:
	movl	5676(%rbx), %eax
	movl	%eax, 6100(%rbx)
.LBB10_6:
	testl	%ecx, %ecx
	jne	.LBB10_17
# BB#7:
	movl	5660(%rbx), %eax
	movl	5676(%rbx), %ecx
	cmpl	%eax, %ecx
	je	.LBB10_17
# BB#8:
	incl	%eax
	xorl	%edx, %edx
	divl	5816(%rbx)
	cmpl	%edx, %ecx
	je	.LBB10_17
# BB#9:
	movq	active_sps(%rip), %rax
	cmpl	$0, 2064(%rax)
	je	.LBB10_10
# BB#15:
	cmpl	$0, 6068(%rbx)
	jne	.LBB10_17
.LBB10_16:
	movq	%rbx, %rdi
	callq	fill_frame_num_gap
.LBB10_17:
	cmpl	$0, 5808(%rbx)
	je	.LBB10_19
# BB#18:
	movl	5676(%rbx), %eax
	movl	%eax, 5660(%rbx)
.LBB10_19:                              # %._crit_edge144
	movq	%rbx, %rdi
	callq	decode_poc
	movl	6100(%rbx), %eax
	cmpl	5676(%rbx), %eax
	jne	.LBB10_22
# BB#20:
	cmpl	$2147483647, 6104(%rbx) # imm = 0x7FFFFFFF
	jne	.LBB10_22
# BB#21:
	movl	5672(%rbx), %eax
	movl	%eax, 6104(%rbx)
.LBB10_22:
	cmpl	$0, 5808(%rbx)
	je	.LBB10_24
# BB#23:
	movl	5672(%rbx), %eax
	movl	%eax, 6056(%rbx)
.LBB10_24:
	movl	5584(%rbx), %edi
	cmpl	$1, %edi
	ja	.LBB10_26
# BB#25:
	leaq	6024(%rbx), %rdi
	callq	ftime
	leaq	6008(%rbx), %rdi
	callq	time
	movl	5584(%rbx), %edi
.LBB10_26:
	movl	48(%rbx), %esi
	movl	52(%rbx), %edx
	movl	56(%rbx), %ecx
	movl	64(%rbx), %r8d
	callq	alloc_storable_picture
	movq	%rax, dec_picture(%rip)
	movl	5664(%rbx), %ecx
	movl	%ecx, 8(%rax)
	movl	5668(%rbx), %ecx
	movl	%ecx, 12(%rax)
	movl	5672(%rbx), %ecx
	movl	%ecx, 16(%rax)
	movl	28(%rbx), %ecx
	movl	%ecx, 317072(%rax)
	movl	8(%r15), %ecx
	movl	%ecx, 317084(%rax)
	movq	active_pps(%rip), %rcx
	movl	1136(%rcx), %edx
	movl	%edx, 317076(%rax)
	movl	1140(%rcx), %ecx
	movl	%ecx, 317080(%rax)
	movq	erc_errorVar(%rip), %rdi
	movl	5836(%rbx), %esi
	movl	316864(%rax), %ecx
	movl	%esi, %edx
	callq	ercReset
	movl	$0, erc_mvperMB(%rip)
	movl	5584(%rbx), %eax
	testl	%eax, %eax
	je	.LBB10_31
# BB#27:
	cmpl	$2, %eax
	je	.LBB10_30
# BB#28:
	cmpl	$1, %eax
	jne	.LBB10_32
# BB#29:
	movl	5664(%rbx), %eax
	movq	dec_picture(%rip), %rcx
	movl	%eax, 4(%rcx)
	shll	(%rbx)
	jmp	.LBB10_33
.LBB10_31:
	movl	5672(%rbx), %eax
	movq	dec_picture(%rip), %rcx
	movl	%eax, 4(%rcx)
	jmp	.LBB10_33
.LBB10_30:
	movl	5668(%rbx), %eax
	movq	dec_picture(%rip), %rcx
	movl	%eax, 4(%rcx)
	movl	(%rbx), %eax
	leal	1(%rax,%rax), %eax
	movl	%eax, (%rbx)
	jmp	.LBB10_33
.LBB10_32:
	movl	$.L.str.31, %edi
	movl	$235, %esi
	callq	error
.LBB10_33:
	movl	$0, 12(%rbx)
	cmpl	$5, 44(%rbx)
	jl	.LBB10_35
# BB#34:
	movl	$1, %edi
	callq	set_ec_flag
	movl	$0, 44(%rbx)
.LBB10_35:                              # %.preheader125
	movl	5836(%rbx), %esi
	testl	%esi, %esi
	jle	.LBB10_50
# BB#36:                                # %.preheader124.lr.ph
	movl	5924(%rbx), %ecx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB10_37:                              # %.preheader124
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_39 Depth 2
                                        #     Child Loop BB10_42 Depth 2
                                        #     Child Loop BB10_45 Depth 2
                                        #     Child Loop BB10_48 Depth 2
	cmpl	$-3, %ecx
	jl	.LBB10_49
# BB#38:                                # %.lr.ph130
                                        #   in Loop: Header=BB10_37 Depth=1
	movq	5560(%rbx), %rcx
	movq	(%rcx,%rax,8), %rcx
	movq	(%rcx), %rdx
	movq	$-1, %rsi
	.p2align	4, 0x90
.LBB10_39:                              #   Parent Loop BB10_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$-1, 4(%rdx,%rsi,4)
	movslq	5924(%rbx), %rcx
	leaq	3(%rcx), %rdi
	incq	%rsi
	cmpq	%rdi, %rsi
	jl	.LBB10_39
# BB#40:                                # %._crit_edge131
                                        #   in Loop: Header=BB10_37 Depth=1
	cmpl	$-4, %ecx
	jle	.LBB10_49
# BB#41:                                # %.lr.ph130.1
                                        #   in Loop: Header=BB10_37 Depth=1
	movq	5560(%rbx), %rcx
	movq	(%rcx,%rax,8), %rcx
	movq	8(%rcx), %rdx
	movq	$-1, %rsi
	.p2align	4, 0x90
.LBB10_42:                              #   Parent Loop BB10_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$-1, 4(%rdx,%rsi,4)
	movslq	5924(%rbx), %rcx
	leaq	3(%rcx), %rdi
	incq	%rsi
	cmpq	%rdi, %rsi
	jl	.LBB10_42
# BB#43:                                # %._crit_edge131.1
                                        #   in Loop: Header=BB10_37 Depth=1
	cmpl	$-3, %ecx
	jl	.LBB10_49
# BB#44:                                # %.lr.ph130.2
                                        #   in Loop: Header=BB10_37 Depth=1
	movq	5560(%rbx), %rcx
	movq	(%rcx,%rax,8), %rcx
	movq	16(%rcx), %rdx
	movq	$-1, %rsi
	.p2align	4, 0x90
.LBB10_45:                              #   Parent Loop BB10_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$-1, 4(%rdx,%rsi,4)
	movslq	5924(%rbx), %rcx
	leaq	3(%rcx), %rdi
	incq	%rsi
	cmpq	%rdi, %rsi
	jl	.LBB10_45
# BB#46:                                # %._crit_edge131.2
                                        #   in Loop: Header=BB10_37 Depth=1
	cmpl	$-3, %ecx
	jl	.LBB10_49
# BB#47:                                # %.lr.ph130.3
                                        #   in Loop: Header=BB10_37 Depth=1
	movq	5560(%rbx), %rcx
	movq	(%rcx,%rax,8), %rcx
	movq	24(%rcx), %rdx
	movq	$-1, %rsi
	.p2align	4, 0x90
.LBB10_48:                              #   Parent Loop BB10_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$-1, 4(%rdx,%rsi,4)
	movslq	5924(%rbx), %rcx
	leaq	3(%rcx), %rdi
	incq	%rsi
	cmpq	%rdi, %rsi
	jl	.LBB10_48
	.p2align	4, 0x90
.LBB10_49:                              # %._crit_edge131.3
                                        #   in Loop: Header=BB10_37 Depth=1
	incq	%rax
	movslq	5836(%rbx), %rsi
	cmpq	%rsi, %rax
	jl	.LBB10_37
.LBB10_50:                              # %._crit_edge134
	movq	active_pps(%rip), %rax
	cmpl	$0, 1148(%rax)
	je	.LBB10_54
# BB#51:                                # %.preheader122
	testl	%esi, %esi
	jle	.LBB10_62
# BB#52:                                # %.lr.ph128
	movq	16(%rbx), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB10_53:                              # =>This Inner Loop Header: Depth=1
	movl	$1, (%rax,%rcx,4)
	incq	%rcx
	movslq	5836(%rbx), %rsi
	cmpq	%rsi, %rcx
	jl	.LBB10_53
.LBB10_54:                              # %.preheader
	testl	%esi, %esi
	jle	.LBB10_62
# BB#55:                                # %.lr.ph
	movq	5600(%rbx), %r8
	movslq	%esi, %rax
	leaq	-1(%rax), %rsi
	movq	%rax, %rdi
	andq	$3, %rdi
	je	.LBB10_56
# BB#57:                                # %.prol.preheader
	leaq	336(%r8), %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB10_58:                              # =>This Inner Loop Header: Depth=1
	movl	$-1, -324(%rdx)
	movl	$1, (%rdx)
	incq	%rcx
	addq	$408, %rdx              # imm = 0x198
	cmpq	%rcx, %rdi
	jne	.LBB10_58
	jmp	.LBB10_59
.LBB10_56:
	xorl	%ecx, %ecx
.LBB10_59:                              # %.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB10_62
# BB#60:                                # %.lr.ph.new
	imulq	$408, %rcx, %rdx        # imm = 0x198
	leaq	1560(%r8,%rdx), %rdx
	.p2align	4, 0x90
.LBB10_61:                              # =>This Inner Loop Header: Depth=1
	movl	$-1, -1548(%rdx)
	movl	$1, -1224(%rdx)
	movl	$-1, -1140(%rdx)
	movl	$1, -816(%rdx)
	movl	$-1, -732(%rdx)
	movl	$1, -408(%rdx)
	movl	$-1, -324(%rdx)
	movl	$1, (%rdx)
	addq	$4, %rcx
	addq	$1632, %rdx             # imm = 0x660
	cmpq	%rax, %rcx
	jl	.LBB10_61
.LBB10_62:                              # %._crit_edge
	xorps	%xmm0, %xmm0
	movups	%xmm0, 84(%rbx)
	movups	%xmm0, 68(%rbx)
	movl	44(%rbx), %ecx
	movq	dec_picture(%rip), %rax
	movl	%ecx, 317024(%rax)
	xorl	%ecx, %ecx
	cmpl	$0, 5808(%rbx)
	setne	%cl
	movl	%ecx, 316848(%rax)
	movl	5804(%rbx), %ecx
	movl	%ecx, 317028(%rax)
	movl	5848(%rbx), %ecx
	movl	%ecx, 317032(%rax)
	movl	5852(%rbx), %ecx
	movl	%ecx, 317036(%rax)
	movl	5856(%rbx), %ecx
	movl	%ecx, 317040(%rax)
	movq	5632(%rbx), %rcx
	movq	%rcx, 317088(%rax)
	movq	$0, 5632(%rbx)
	movl	5624(%rbx), %ecx
	movl	%ecx, 316904(%rax)
	movl	5820(%rbx), %edx
	movl	%edx, 316908(%rax)
	testl	%ecx, %ecx
	movl	$get_mb_block_pos_mbaff, %ecx
	movl	$get_mb_block_pos_normal, %edx
	cmovneq	%rcx, %rdx
	movq	%rdx, get_mb_block_pos(%rip)
	movl	$getAffNeighbour, %ecx
	movl	$getNonAffNeighbour, %edx
	cmovneq	%rcx, %rdx
	movq	%rdx, getNeighbour(%rip)
	movl	5676(%rbx), %ecx
	movl	%ecx, 316832(%rax)
	movl	%ecx, 316824(%rax)
	xorl	%edx, %edx
	cmpl	6100(%rbx), %ecx
	sete	%dl
	movl	%edx, 316828(%rax)
	xorl	%ecx, %ecx
	cmpl	$0, 5584(%rbx)
	sete	%cl
	movl	%ecx, 316900(%rax)
	movq	active_sps(%rip), %rcx
	movl	32(%rcx), %edx
	movl	%edx, 317044(%rax)
	movl	2076(%rcx), %edx
	movl	%edx, 317048(%rax)
	movl	2088(%rcx), %edx
	movl	%edx, 317052(%rax)
	testl	%edx, %edx
	je	.LBB10_64
# BB#63:
	movups	2092(%rcx), %xmm0
	movups	%xmm0, 317056(%rax)
.LBB10_64:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB10_10:
	movl	3020(%r14), %eax
	testl	%eax, %eax
	je	.LBB10_14
# BB#11:
	cmpl	%edx, %ecx
	jae	.LBB10_13
# BB#12:
	movl	$1, 6068(%rbx)
	movl	$1, 6080(%rbx)
	movq	%rbx, %rdi
	callq	conceal_lost_frames
	movl	3020(%r14), %eax
	movl	%eax, 6068(%rbx)
	cmpl	$0, 6068(%rbx)
	jne	.LBB10_17
	jmp	.LBB10_16
.LBB10_14:
	movl	$.L.str.30, %edi
	movl	$100, %esi
	callq	error
	cmpl	$0, 6068(%rbx)
	jne	.LBB10_17
	jmp	.LBB10_16
.LBB10_13:
	movl	%eax, 6068(%rbx)
	movl	$0, 6080(%rbx)
	movq	%rbx, %rdi
	callq	conceal_lost_frames
	cmpl	$0, 6068(%rbx)
	jne	.LBB10_17
	jmp	.LBB10_16
.Lfunc_end10:
	.size	init_picture, .Lfunc_end10-init_picture
	.cfi_endproc

	.globl	exit_picture
	.p2align	4, 0x90
	.type	exit_picture,@function
exit_picture:                           # @exit_picture
	.cfi_startproc
# BB#0:
	movq	dec_picture(%rip), %rsi
	testq	%rsi, %rsi
	je	.LBB11_44
# BB#1:
	pushq	%rbp
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi104:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi105:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi107:
	.cfi_def_cfa_offset 112
.Lcfi108:
	.cfi_offset %rbx, -56
.Lcfi109:
	.cfi_offset %r12, -48
.Lcfi110:
	.cfi_offset %r13, -40
.Lcfi111:
	.cfi_offset %r14, -32
.Lcfi112:
	.cfi_offset %r15, -24
.Lcfi113:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rdi
	callq	DeblockPicture
	movq	dec_picture(%rip), %rax
	cmpl	$0, 316904(%rax)
	je	.LBB11_3
# BB#2:
	callq	MbAffPostProc
	movq	dec_picture(%rip), %rax
.LBB11_3:
	movq	316920(%rax), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, 16(%rsp)
	cmpl	$0, 317044(%rax)
	je	.LBB11_5
# BB#4:
	movq	316928(%rax), %rcx
	movq	(%rcx), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, 24(%rsp)
	movq	8(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, 32(%rsp)
.LBB11_5:
	cmpl	$0, 316904(%rax)
	jne	.LBB11_21
# BB#6:
	movq	erc_errorVar(%rip), %rcx
	xorl	%ebx, %ebx
	xorl	%edi, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	ercStartSegment
	movq	dec_picture(%rip), %rax
	movl	316912(%rax), %edi
	cmpl	$2, %edi
	movl	$0, %r14d
	jb	.LBB11_14
# BB#7:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB11_8:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rbx), %r15d
	movq	img(%rip), %rcx
	movq	5600(%rcx), %rcx
	imulq	$408, %r15, %rdx        # imm = 0x198
	movl	336(%rcx,%rdx), %edx
	movl	%ebx, %ebp
	imulq	$408, %rbp, %rbx        # imm = 0x198
	cmpl	336(%rcx,%rbx), %edx
	je	.LBB11_13
# BB#9:                                 #   in Loop: Header=BB11_8 Depth=1
	movq	erc_errorVar(%rip), %rcx
	xorl	%edx, %edx
	movl	%ebp, %edi
	movl	%r14d, %esi
	callq	ercStopSegment
	movq	img(%rip), %rax
	movq	5600(%rax), %rax
	cmpl	$0, 336(%rax,%rbx)
	movq	dec_picture(%rip), %rax
	movl	316864(%rax), %edi
	movq	erc_errorVar(%rip), %rsi
	je	.LBB11_11
# BB#10:                                #   in Loop: Header=BB11_8 Depth=1
	callq	ercMarkCurrSegmentLost
	jmp	.LBB11_12
.LBB11_11:                              #   in Loop: Header=BB11_8 Depth=1
	callq	ercMarkCurrSegmentOK
.LBB11_12:                              #   in Loop: Header=BB11_8 Depth=1
	incl	%r14d
	movq	erc_errorVar(%rip), %rcx
	xorl	%edx, %edx
	movl	%r15d, %edi
	movl	%r14d, %esi
	callq	ercStartSegment
	movq	dec_picture(%rip), %rax
.LBB11_13:                              #   in Loop: Header=BB11_8 Depth=1
	movl	316912(%rax), %edi
	leal	1(%rbp), %ebx
	addl	$2, %ebp
	cmpl	%edi, %ebp
	jb	.LBB11_8
.LBB11_14:                              # %._crit_edge
	decl	%edi
	movq	erc_errorVar(%rip), %rcx
	xorl	%edx, %edx
	movl	%r14d, %esi
	callq	ercStopSegment
	movq	img(%rip), %rax
	movq	5600(%rax), %rax
	movl	%ebx, %ecx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	cmpl	$0, 336(%rax,%rcx)
	movq	dec_picture(%rip), %rax
	movl	316864(%rax), %edi
	movq	erc_errorVar(%rip), %rsi
	je	.LBB11_16
# BB#15:
	callq	ercMarkCurrSegmentLost
	jmp	.LBB11_17
.LBB11_16:
	callq	ercMarkCurrSegmentOK
.LBB11_17:
	movq	dec_picture(%rip), %rdi
	movl	erc_mvperMB(%rip), %eax
	xorl	%edx, %edx
	divl	316912(%rdi)
	movl	%eax, erc_mvperMB(%rip)
	movq	img(%rip), %rax
	movq	%rax, erc_img(%rip)
	movl	317024(%rdi), %eax
	cmpl	$4, %eax
	je	.LBB11_19
# BB#18:
	cmpl	$2, %eax
	jne	.LBB11_20
.LBB11_19:
	movl	316864(%rdi), %esi
	movl	316868(%rdi), %edx
	movq	erc_errorVar(%rip), %rcx
	leaq	16(%rsp), %rdi
	callq	ercConcealIntraFrame
	jmp	.LBB11_21
.LBB11_20:
	movq	erc_object_list(%rip), %rsi
	movl	316864(%rdi), %edx
	movl	316868(%rdi), %ecx
	movq	erc_errorVar(%rip), %r8
	movl	317044(%rdi), %r9d
	leaq	16(%rsp), %rdi
	callq	ercConcealInterFrame
.LBB11_21:
	movq	img(%rip), %rax
	cmpl	$0, 5584(%rax)
	je	.LBB11_23
# BB#22:
	movl	(%rax), %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	movl	%edx, (%rax)
.LBB11_23:
	movq	dec_picture(%rip), %rdi
	movl	(%rdi), %r14d
	movl	16(%rdi), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	317024(%rdi), %ebx
	movl	316848(%rdi), %r15d
	movl	317072(%rdi), %r12d
	movl	316832(%rdi), %r13d
	movslq	317044(%rdi), %rbp
	callq	store_picture_in_dpb
	movq	$0, dec_picture(%rip)
	movq	img(%rip), %rdi
	cmpl	$0, 5860(%rdi)
	je	.LBB11_25
# BB#24:
	movl	$0, 5660(%rdi)
.LBB11_25:
	orl	$2, %r14d
	cmpl	$2, %r14d
	jne	.LBB11_43
# BB#26:
	addq	$6040, %rdi             # imm = 0x1798
	callq	ftime
	movq	img(%rip), %rdi
	addq	$6016, %rdi             # imm = 0x1780
	callq	time
	leaq	(%rbp,%rbp,2), %rax
	leaq	.Lexit_picture.yuv_types(%rax,%rax), %rbp
	movq	%rbp, %rdi
	callq	strlen
	leaq	1(%rax), %rdx
	leaq	46(%rsp), %r14
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	memcpy
	movq	input(%rip), %rax
	cmpl	$0, 3016(%rax)
	je	.LBB11_27
# BB#36:
	movq	stdout(%rip), %rdi
	movq	snr(%rip), %rax
	movl	(%rax), %edx
	movl	$.L.str.38, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB11_37:
	movq	stdout(%rip), %rdi
	callq	fflush
	testl	%r15d, %r15d
	jne	.LBB11_40
# BB#38:                                # %switch.early.test
	cmpl	$4, %ebx
	ja	.LBB11_41
.LBB11_39:                              # %switch.early.test
	movl	$21, %eax
	btl	%ebx, %eax
	jae	.LBB11_41
.LBB11_40:
	movq	img(%rip), %rdi
	incl	(%rdi)
.LBB11_42:
	movq	snr(%rip), %rax
	incl	(%rax)
	incl	g_nFrame(%rip)
.LBB11_43:
	movl	$-4712, 4(%rdi)         # imm = 0xED98
	movl	$0, 12(%rdi)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
.LBB11_44:
	retq
.LBB11_27:
	cmpl	$4, %ebx
	ja	.LBB11_33
# BB#28:
	movl	%ebx, %eax
	jmpq	*.LJTI11_0(,%rax,8)
.LBB11_30:
	movq	stdout(%rip), %rdi
	movl	frame_no(%rip), %edx
	movq	snr(%rip), %rax
	movss	4(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	12(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movl	$.L.str.4, %esi
	movb	$3, %al
	movl	%r13d, %r8d
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%r12d, %r9d
	pushq	$0
.Lcfi114:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi115:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$16, %rsp
.Lcfi116:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB11_37
.LBB11_33:
	testl	%r15d, %r15d
	movq	stdout(%rip), %rdi
	movl	frame_no(%rip), %edx
	movq	snr(%rip), %rax
	movss	4(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	12(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	je	.LBB11_35
# BB#34:                                # %.thread
	movl	$.L.str.36, %esi
	movb	$3, %al
	movl	%r13d, %r8d
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%r12d, %r9d
	pushq	$0
.Lcfi117:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi118:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$16, %rsp
.Lcfi119:
	.cfi_adjust_cfa_offset -16
	movq	stdout(%rip), %rdi
	callq	fflush
	jmp	.LBB11_40
.LBB11_29:
	movq	stdout(%rip), %rdi
	movl	frame_no(%rip), %edx
	movq	snr(%rip), %rax
	movss	4(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	12(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movl	$.L.str.33, %esi
	movb	$3, %al
	movl	%r13d, %r8d
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%r12d, %r9d
	pushq	$0
.Lcfi120:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi121:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$16, %rsp
.Lcfi122:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB11_37
.LBB11_31:
	movq	stdout(%rip), %rdi
	movl	frame_no(%rip), %edx
	movq	snr(%rip), %rax
	movss	4(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	12(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movl	$.L.str.34, %esi
	movb	$3, %al
	movl	%r13d, %r8d
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%r12d, %r9d
	pushq	$0
.Lcfi123:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi124:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$16, %rsp
.Lcfi125:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB11_37
.LBB11_32:
	movq	stdout(%rip), %rdi
	movl	frame_no(%rip), %edx
	movq	snr(%rip), %rax
	movss	4(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	12(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movl	$.L.str.35, %esi
	movb	$3, %al
	movl	%r13d, %r8d
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%r12d, %r9d
	pushq	$0
.Lcfi126:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi127:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$16, %rsp
.Lcfi128:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB11_37
.LBB11_35:                              # %.thread68
	movl	$.L.str.37, %esi
	movb	$3, %al
	movl	%r13d, %r8d
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%r12d, %r9d
	pushq	$0
.Lcfi129:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi130:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$16, %rsp
.Lcfi131:
	.cfi_adjust_cfa_offset -16
	movq	stdout(%rip), %rdi
	callq	fflush
	cmpl	$4, %ebx
	jbe	.LBB11_39
.LBB11_41:
	incl	Bframe_ctr(%rip)
	movq	img(%rip), %rdi
	jmp	.LBB11_42
.Lfunc_end11:
	.size	exit_picture, .Lfunc_end11-exit_picture
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI11_0:
	.quad	.LBB11_30
	.quad	.LBB11_33
	.quad	.LBB11_29
	.quad	.LBB11_31
	.quad	.LBB11_32

	.text
	.globl	frame_postprocessing
	.p2align	4, 0x90
	.type	frame_postprocessing,@function
frame_postprocessing:                   # @frame_postprocessing
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end12:
	.size	frame_postprocessing, .Lfunc_end12-frame_postprocessing
	.cfi_endproc

	.globl	field_postprocessing
	.p2align	4, 0x90
	.type	field_postprocessing,@function
field_postprocessing:                   # @field_postprocessing
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movl	%ecx, (%rdi)
	retq
.Lfunc_end13:
	.size	field_postprocessing, .Lfunc_end13-field_postprocessing
	.cfi_endproc

	.globl	ercWriteMBMODEandMV
	.p2align	4, 0x90
	.type	ercWriteMBMODEandMV,@function
ercWriteMBMODEandMV:                    # @ercWriteMBMODEandMV
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi132:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi133:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi134:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi135:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi136:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi137:
	.cfi_def_cfa_offset 56
.Lcfi138:
	.cfi_offset %rbx, -56
.Lcfi139:
	.cfi_offset %r12, -48
.Lcfi140:
	.cfi_offset %r13, -40
.Lcfi141:
	.cfi_offset %r14, -32
.Lcfi142:
	.cfi_offset %r15, -24
.Lcfi143:
	.cfi_offset %rbp, -16
	movslq	4(%rdi), %rcx
	movq	dec_picture(%rip), %r9
	movl	316864(%r9), %esi
	sarl	$4, %esi
	movl	%ecx, %eax
	cltd
	idivl	%esi
	movl	%eax, %r8d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	5600(%rdi), %rbx
	movq	erc_object_list(%rip), %rsi
	leal	(,%rcx,4), %ebp
	movslq	%ebp, %rbp
	leaq	(%rbp,%rbp,2), %rbp
	cmpl	$1, 44(%rdi)
	movq	%r9, -16(%rsp)          # 8-byte Spill
	jne	.LBB14_1
# BB#17:                                # %.preheader
	shll	$2, %edx
	shll	$2, %r8d
	imulq	$408, %rcx, %rcx        # imm = 0x198
	leaq	40(%rbx,%rcx), %r14
	leaq	20(%rsi,%rbp,8), %rsi
	xorl	%edi, %edi
	movl	%r8d, -28(%rsp)         # 4-byte Spill
	movq	%rdx, -24(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB14_18:                              # =>This Inner Loop Header: Depth=1
	cmpl	$10, (%r14)
	jne	.LBB14_20
# BB#19:                                # %.thread
                                        #   in Loop: Header=BB14_18 Depth=1
	movb	$2, -20(%rsi)
	jmp	.LBB14_21
	.p2align	4, 0x90
.LBB14_20:                              #   in Loop: Header=BB14_18 Depth=1
	cmpb	$11, 288(%r14,%rdi)
	sete	%al
	addb	$5, %al
	movb	%al, -20(%rsi)
	cmpb	$11, 288(%r14,%rdi)
	jne	.LBB14_22
.LBB14_21:                              #   in Loop: Header=BB14_18 Depth=1
	movq	$0, -8(%rsi)
	xorl	%ecx, %ecx
	jmp	.LBB14_23
	.p2align	4, 0x90
.LBB14_22:                              #   in Loop: Header=BB14_18 Depth=1
	movl	%edi, %eax
	shrl	$31, %eax
	addl	%edi, %eax
	andl	$-2, %eax
	movl	%edi, %ecx
	subl	%eax, %ecx
	leal	(%rdx,%rcx,2), %ecx
	addl	%r8d, %eax
	movq	316952(%r9), %r8
	movq	316976(%r9), %rbx
	movq	(%r8), %rbp
	movslq	%eax, %r10
	movq	(%rbp,%r10,8), %rax
	movslq	%ecx, %r13
	movzbl	(%rax,%r13), %eax
	shrb	$7, %al
	movzbl	%al, %r11d
	movq	(%rbx,%r11,8), %rax
	movq	(%rax,%r10,8), %rdx
	movq	(%rdx,%r13,8), %r15
	movswl	(%r15), %r9d
	movl	%r13d, %ebp
	orl	$1, %ebp
	movslq	%ebp, %rbp
	movq	(%rdx,%rbp,8), %r12
	movswl	(%r12), %ecx
	movl	%r10d, %edx
	orl	$1, %edx
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rax
	movq	(%rax,%r13,8), %rdx
	movq	(%rax,%rbp,8), %rax
	movswl	(%rdx), %ebp
	addl	%r9d, %ecx
	movswl	(%rax), %ebx
	addl	%ebp, %ecx
	leal	(%rcx,%rbx), %ebp
	leal	2(%rbx,%rcx), %r9d
	movl	%r9d, %ebx
	sarl	$31, %ebx
	shrl	$30, %ebx
	leal	2(%rbx,%rbp), %ebp
	sarl	$2, %ebp
	movl	%ebp, -8(%rsi)
	movswl	2(%r15), %ebx
	movswl	2(%r12), %ecx
	addl	%ebx, %ecx
	movswl	2(%rdx), %edx
	movswl	2(%rax), %eax
	addl	%edx, %ecx
	leal	(%rcx,%rax), %edx
	leal	2(%rax,%rcx), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	leal	2(%rcx,%rdx), %ebx
	sarl	$2, %ebx
	movl	%ebp, %edx
	negl	%edx
	cmpl	$-3, %r9d
	movq	-16(%rsp), %r9          # 8-byte Reload
	cmovgel	%ebp, %edx
	movl	%ebx, %ecx
	negl	%ecx
	cmpl	$-3, %eax
	movl	%ebx, -4(%rsi)
	cmovgel	%ebx, %ecx
	addl	erc_mvperMB(%rip), %edx
	addl	%ecx, %edx
	movl	%edx, erc_mvperMB(%rip)
	movq	(%r8,%r11,8), %rax
	movl	-28(%rsp), %r8d         # 4-byte Reload
	movq	(%rax,%r10,8), %rax
	movq	-24(%rsp), %rdx         # 8-byte Reload
	movsbl	(%rax,%r13), %ecx
.LBB14_23:                              #   in Loop: Header=BB14_18 Depth=1
	movl	%ecx, (%rsi)
	incq	%rdi
	addq	$24, %rsi
	cmpq	$4, %rdi
	jne	.LBB14_18
	jmp	.LBB14_24
.LBB14_1:                               # %.preheader150
	leaq	(%rsi,%rbp,8), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	imulq	$408, %rcx, %rcx        # imm = 0x198
	leaq	40(%rbx,%rcx), %r14
	shll	$2, %edx
	shll	$2, %r8d
	leaq	12(%rsi,%rbp,8), %rdi
	xorl	%esi, %esi
	movl	%r8d, -28(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB14_2:                               # =>This Inner Loop Header: Depth=1
	movb	$2, %bpl
	cmpl	$10, (%r14)
	je	.LBB14_9
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=1
	movzbl	288(%r14,%rsi), %ecx
	testb	%cl, %cl
	je	.LBB14_8
# BB#4:                                 #   in Loop: Header=BB14_2 Depth=1
	movb	$6, %bpl
	cmpb	$11, %cl
	je	.LBB14_9
# BB#5:                                 #   in Loop: Header=BB14_2 Depth=1
	cmpb	$1, %cl
	je	.LBB14_7
# BB#6:                                 #   in Loop: Header=BB14_2 Depth=1
	movb	$5, %cl
.LBB14_7:                               #   in Loop: Header=BB14_2 Depth=1
	movb	%cl, %bpl
	jmp	.LBB14_9
.LBB14_8:                               # %.fold.split
                                        #   in Loop: Header=BB14_2 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB14_9:                               # %._crit_edge
                                        #   in Loop: Header=BB14_2 Depth=1
	movb	%bpl, -12(%rdi)
	movzbl	288(%r14,%rsi), %ebx
	cmpb	$11, %bl
	je	.LBB14_11
# BB#10:                                # %._crit_edge
                                        #   in Loop: Header=BB14_2 Depth=1
	testb	%bl, %bl
	jne	.LBB14_12
.LBB14_11:                              #   in Loop: Header=BB14_2 Depth=1
	movq	$0, (%rdi)
	xorl	%ecx, %ecx
	movq	%rdi, %rbx
	jmp	.LBB14_16
	.p2align	4, 0x90
.LBB14_12:                              #   in Loop: Header=BB14_2 Depth=1
	movl	%esi, %ebp
	shrl	$31, %ebp
	addl	%esi, %ebp
	andl	$-2, %ebp
	movl	%esi, %ecx
	subl	%ebp, %ecx
	leal	(%rdx,%rcx,2), %r11d
	addl	%r8d, %ebp
	addb	$-5, %bl
	movq	316976(%r9), %rcx
	movq	(%rcx), %r12
	movslq	%ebp, %r10
	movq	(%r12,%r10,8), %r13
	movslq	%r11d, %r11
	movq	(%r13,%r11,8), %r15
	movswl	(%r15), %ecx
	cmpb	$2, %bl
	ja	.LBB14_14
# BB#13:                                #   in Loop: Header=BB14_2 Depth=1
	leaq	(%rsi,%rsi,2), %rbx
	movq	-8(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbx,8), %rbx
	movl	%r11d, %r8d
	orl	$1, %r8d
	movslq	%r8d, %r8
	movq	(%r13,%r8,8), %r13
	movswl	(%r13), %r9d
	orl	$1, %ebp
	movslq	%ebp, %rbp
	movq	(%r12,%rbp,8), %rbp
	movq	(%rbp,%r11,8), %r12
	movq	%r10, -24(%rsp)         # 8-byte Spill
	movswl	(%r12), %r10d
	movq	(%rbp,%r8,8), %r8
	movswl	(%r8), %eax
	addl	%r9d, %ecx
	addl	%r10d, %ecx
	leal	(%rcx,%rax), %ebp
	leal	2(%rax,%rcx), %eax
	sarl	$31, %eax
	shrl	$30, %eax
	leal	2(%rax,%rbp), %ecx
	sarl	$2, %ecx
	addq	$12, %rbx
	movl	%ecx, (%rdi)
	movswl	2(%r15), %r9d
	movswl	2(%r13), %ebp
	movswl	2(%r12), %r10d
	movswl	2(%r8), %eax
	movl	-28(%rsp), %r8d         # 4-byte Reload
	addl	%r9d, %ebp
	movq	-16(%rsp), %r9          # 8-byte Reload
	addl	%r10d, %ebp
	leal	(%rbp,%rax), %r10d
	leal	2(%rax,%rbp), %eax
	sarl	$31, %eax
	shrl	$30, %eax
	leal	2(%rax,%r10), %ebp
	movq	-24(%rsp), %r10         # 8-byte Reload
	sarl	$2, %ebp
	jmp	.LBB14_15
.LBB14_14:                              #   in Loop: Header=BB14_2 Depth=1
	movl	%ecx, (%rdi)
	movswl	2(%r15), %ebp
	movq	%rdi, %rbx
.LBB14_15:                              #   in Loop: Header=BB14_2 Depth=1
	movl	%ebp, 4(%rdi)
	movl	%ebp, %eax
	negl	%eax
	cmovll	%ebp, %eax
	movl	%ecx, %ebp
	negl	%ebp
	cmovll	%ecx, %ebp
	addl	erc_mvperMB(%rip), %eax
	addl	%ebp, %eax
	movl	%eax, erc_mvperMB(%rip)
	movq	316952(%r9), %rax
	movq	(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movsbl	(%rax,%r11), %ecx
.LBB14_16:                              #   in Loop: Header=BB14_2 Depth=1
	movl	%ecx, 8(%rbx)
	incq	%rsi
	addq	$24, %rdi
	cmpq	$4, %rsi
	jne	.LBB14_2
.LBB14_24:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	ercWriteMBMODEandMV, .Lfunc_end14-ercWriteMBMODEandMV
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI15_0:
	.long	2147483647              # 0x7fffffff
	.long	2147483647              # 0x7fffffff
	.long	4294967295              # 0xffffffff
	.long	2147483647              # 0x7fffffff
	.text
	.globl	init_old_slice
	.p2align	4, 0x90
	.type	init_old_slice,@function
init_old_slice:                         # @init_old_slice
	.cfi_startproc
# BB#0:
	movl	$0, old_slice(%rip)
	movl	$2147483647, old_slice+40(%rip) # imm = 0x7FFFFFFF
	movl	$0, old_slice+32(%rip)
	movaps	.LCPI15_0(%rip), %xmm0  # xmm0 = [2147483647,2147483647,4294967295,2147483647]
	movups	%xmm0, old_slice+8(%rip)
	movabsq	$9223372034707292159, %rax # imm = 0x7FFFFFFF7FFFFFFF
	movq	%rax, old_slice+24(%rip)
	retq
.Lfunc_end15:
	.size	init_old_slice, .Lfunc_end15-init_old_slice
	.cfi_endproc

	.globl	exit_slice
	.p2align	4, 0x90
	.type	exit_slice,@function
exit_slice:                             # @exit_slice
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	movq	5592(%rax), %rcx
	movl	148(%rcx), %ecx
	movl	%ecx, old_slice+40(%rip)
	movl	5676(%rax), %ecx
	movl	%ecx, old_slice+8(%rip)
	movl	5680(%rax), %ecx
	movl	%ecx, old_slice(%rip)
	testl	%ecx, %ecx
	je	.LBB16_2
# BB#1:
	movl	5684(%rax), %ecx
	movl	%ecx, old_slice+4(%rip)
.LBB16_2:
	movl	5808(%rax), %ecx
	movl	%ecx, old_slice+12(%rip)
	movl	5804(%rax), %ecx
	movl	%ecx, old_slice+32(%rip)
	testl	%ecx, %ecx
	je	.LBB16_4
# BB#3:
	movl	5812(%rax), %ecx
	movl	%ecx, old_slice+36(%rip)
.LBB16_4:
	movq	active_sps(%rip), %rcx
	movl	1012(%rcx), %ecx
	cmpl	$1, %ecx
	je	.LBB16_7
# BB#5:
	testl	%ecx, %ecx
	jne	.LBB16_9
# BB#6:                                 # %.thread
	movl	5688(%rax), %ecx
	movl	%ecx, old_slice+16(%rip)
	addq	$5692, %rax             # imm = 0x163C
	movl	$old_slice+20, %ecx
	jmp	.LBB16_8
.LBB16_7:
	movl	5696(%rax), %ecx
	movl	%ecx, old_slice+24(%rip)
	addq	$5700, %rax             # imm = 0x1644
	movl	$old_slice+28, %ecx
.LBB16_8:                               # %.sink.split
	movl	(%rax), %eax
	movl	%eax, (%rcx)
.LBB16_9:
	retq
.Lfunc_end16:
	.size	exit_slice, .Lfunc_end16-exit_slice
	.cfi_endproc

	.globl	is_new_picture
	.p2align	4, 0x90
	.type	is_new_picture,@function
is_new_picture:                         # @is_new_picture
	.cfi_startproc
# BB#0:
	cmpq	$0, dec_picture(%rip)
	sete	%r8b
	movl	old_slice+40(%rip), %edx
	movq	img(%rip), %rcx
	movq	5592(%rcx), %rsi
	cmpl	148(%rsi), %edx
	setne	%r9b
	movl	old_slice+8(%rip), %edx
	cmpl	5676(%rcx), %edx
	setne	%al
	orb	%r8b, %al
	movl	old_slice(%rip), %edi
	movl	5680(%rcx), %esi
	cmpl	%esi, %edi
	setne	%dl
	orb	%al, %dl
	orb	%r9b, %dl
	movzbl	%dl, %eax
	testl	%edi, %edi
	je	.LBB17_3
# BB#1:
	testl	%esi, %esi
	je	.LBB17_3
# BB#2:
	movl	old_slice+4(%rip), %eax
	cmpl	5684(%rcx), %eax
	setne	%al
	orb	%al, %dl
	movzbl	%dl, %eax
.LBB17_3:
	movl	old_slice+12(%rip), %esi
	movl	5808(%rcx), %edx
	cmpl	%edx, %esi
	jne	.LBB17_5
# BB#4:
	xorl	%edx, %edx
	jmp	.LBB17_6
.LBB17_5:
	testl	%esi, %esi
	sete	%sil
	testl	%edx, %edx
	sete	%dl
	orb	%sil, %dl
.LBB17_6:
	movzbl	%dl, %esi
	orl	%eax, %esi
	movl	old_slice+32(%rip), %edi
	movl	5804(%rcx), %edx
	xorl	%eax, %eax
	cmpl	%edx, %edi
	setne	%al
	orl	%esi, %eax
	testl	%edi, %edi
	je	.LBB17_9
# BB#7:
	testl	%edx, %edx
	je	.LBB17_9
# BB#8:
	movl	old_slice+36(%rip), %edx
	xorl	%esi, %esi
	cmpl	5812(%rcx), %edx
	setne	%sil
	orl	%esi, %eax
.LBB17_9:
	movq	active_sps(%rip), %rdx
	movl	1012(%rdx), %edx
	cmpl	$1, %edx
	je	.LBB17_12
# BB#10:
	testl	%edx, %edx
	jne	.LBB17_14
# BB#11:                                # %.thread
	movl	old_slice+16(%rip), %esi
	xorl	%edx, %edx
	cmpl	5688(%rcx), %esi
	setne	%dl
	addq	$5692, %rcx             # imm = 0x163C
	movl	$old_slice+20, %esi
	jmp	.LBB17_13
.LBB17_12:
	movl	old_slice+24(%rip), %esi
	xorl	%edx, %edx
	cmpl	5696(%rcx), %esi
	setne	%dl
	addq	$5700, %rcx             # imm = 0x1644
	movl	$old_slice+28, %esi
.LBB17_13:                              # %.sink.split
	orl	%eax, %edx
	movl	(%rsi), %esi
	xorl	%eax, %eax
	cmpl	(%rcx), %esi
	setne	%al
	orl	%edx, %eax
.LBB17_14:
	retq
.Lfunc_end17:
	.size	is_new_picture, .Lfunc_end17-is_new_picture
	.cfi_endproc

	.globl	decode_one_slice
	.p2align	4, 0x90
	.type	decode_one_slice,@function
decode_one_slice:                       # @decode_one_slice
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi144:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi145:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi146:
	.cfi_def_cfa_offset 32
.Lcfi147:
	.cfi_offset %rbx, -24
.Lcfi148:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$-1, 5576(%rbx)
	callq	set_ref_pic_num
	cmpl	$1, 44(%rbx)
	jne	.LBB18_2
# BB#1:
	movq	Co_located(%rip), %rdi
	movl	$listX, %esi
	callq	compute_colocated
	.p2align	4, 0x90
.LBB18_2:                               # =>This Inner Loop Header: Depth=1
	movl	4(%rbx), %esi
	movq	%rbx, %rdi
	callq	start_macroblock
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	read_one_macroblock
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	decode_one_macroblock
	cmpl	$0, 5624(%rbx)
	je	.LBB18_5
# BB#3:                                 #   in Loop: Header=BB18_2 Depth=1
	movq	dec_picture(%rip), %rax
	movq	316936(%rax), %rax
	movl	4(%rbx), %ecx
	cmpb	$0, (%rax,%rcx)
	je	.LBB18_5
# BB#4:                                 #   in Loop: Header=BB18_2 Depth=1
	sarl	5640(%rbx)
	sarl	5644(%rbx)
.LBB18_5:                               #   in Loop: Header=BB18_2 Depth=1
	movq	%rbx, %rdi
	callq	ercWriteMBMODEandMV
	cmpl	$0, 5624(%rbx)
	je	.LBB18_6
# BB#7:                                 #   in Loop: Header=BB18_2 Depth=1
	movzbl	4(%rbx), %eax
	andb	$1, %al
	jmp	.LBB18_8
	.p2align	4, 0x90
.LBB18_6:                               #   in Loop: Header=BB18_2 Depth=1
	movb	$1, %al
.LBB18_8:                               #   in Loop: Header=BB18_2 Depth=1
	movzbl	%al, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	exit_macroblock
	testl	%eax, %eax
	je	.LBB18_2
# BB#9:
	movq	img(%rip), %rax
	movq	5592(%rax), %rcx
	movl	148(%rcx), %ecx
	movl	%ecx, old_slice+40(%rip)
	movl	5676(%rax), %ecx
	movl	%ecx, old_slice+8(%rip)
	movl	5680(%rax), %ecx
	movl	%ecx, old_slice(%rip)
	testl	%ecx, %ecx
	je	.LBB18_11
# BB#10:
	movl	5684(%rax), %ecx
	movl	%ecx, old_slice+4(%rip)
.LBB18_11:
	movl	5808(%rax), %ecx
	movl	%ecx, old_slice+12(%rip)
	movl	5804(%rax), %ecx
	movl	%ecx, old_slice+32(%rip)
	testl	%ecx, %ecx
	je	.LBB18_13
# BB#12:
	movl	5812(%rax), %ecx
	movl	%ecx, old_slice+36(%rip)
.LBB18_13:
	movq	active_sps(%rip), %rcx
	movl	1012(%rcx), %ecx
	cmpl	$1, %ecx
	je	.LBB18_16
# BB#14:
	testl	%ecx, %ecx
	jne	.LBB18_18
# BB#15:                                # %.thread.i
	movl	5688(%rax), %ecx
	movl	%ecx, old_slice+16(%rip)
	addq	$5692, %rax             # imm = 0x163C
	movl	$old_slice+20, %ecx
	jmp	.LBB18_17
.LBB18_16:
	movl	5696(%rax), %ecx
	movl	%ecx, old_slice+24(%rip)
	addq	$5700, %rax             # imm = 0x1644
	movl	$old_slice+28, %ecx
.LBB18_17:                              # %.sink.split.i
	movl	(%rax), %eax
	movl	%eax, (%rcx)
.LBB18_18:                              # %exit_slice.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end18:
	.size	decode_one_slice, .Lfunc_end18-decode_one_slice
	.cfi_endproc

	.globl	fill_wp_params
	.p2align	4, 0x90
	.type	fill_wp_params,@function
fill_wp_params:                         # @fill_wp_params
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi149:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi150:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi151:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi152:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi153:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi154:
	.cfi_def_cfa_offset 56
.Lcfi155:
	.cfi_offset %rbx, -56
.Lcfi156:
	.cfi_offset %r12, -48
.Lcfi157:
	.cfi_offset %r13, -40
.Lcfi158:
	.cfi_offset %r14, -32
.Lcfi159:
	.cfi_offset %r15, -24
.Lcfi160:
	.cfi_offset %rbp, -16
	movl	44(%rdi), %r9d
	movl	5640(%rdi), %eax
	movq	%rax, -64(%rsp)         # 8-byte Spill
	movl	5644(%rdi), %eax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	cmpl	$1, %r9d
	jne	.LBB19_5
# BB#1:
	movq	active_pps(%rip), %rax
	cmpl	$2, 1124(%rax)
	jne	.LBB19_5
# BB#2:
	movabsq	$21474836485, %rax      # imm = 0x500000005
	movq	%rax, 5760(%rdi)
	movabsq	$68719476752, %rax      # imm = 0x1000000010
	movq	%rax, 5792(%rdi)
	movq	5768(%rdi), %rax
	movq	5776(%rdi), %rcx
	movq	(%rax), %r8
	movq	8(%rax), %r10
	movq	(%rcx), %r11
	movq	8(%rcx), %r14
	movl	$5, %ecx
	xorl	%eax, %eax
	jmp	.LBB19_3
	.p2align	4, 0x90
.LBB19_4:                               # %.preheader261..preheader261_crit_edge
                                        #   in Loop: Header=BB19_3 Depth=1
	incq	%rax
	movl	5760(%rdi), %ecx
.LBB19_3:                               # %.preheader261
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r8,%rax,8), %r15
	movq	(%r10,%rax,8), %rsi
	movq	(%r11,%rax,8), %rbp
	movq	(%r14,%rax,8), %rbx
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	movl	%edx, (%r15)
	movl	%edx, (%rsi)
	movl	$0, (%rbp)
	movl	$0, (%rbx)
	movzbl	5764(%rdi), %ecx
	movl	$1, %edx
	shll	%cl, %edx
	movl	%edx, 4(%r15)
	movl	%edx, 4(%rsi)
	movl	$0, 4(%rbp)
	movl	$0, 4(%rbx)
	movzbl	5764(%rdi), %ecx
	movl	$1, %edx
	shll	%cl, %edx
	movl	%edx, 8(%r15)
	movl	%edx, 8(%rsi)
	movl	$0, 8(%rbp)
	movl	$0, 8(%rbx)
	cmpq	$31, %rax
	jne	.LBB19_4
.LBB19_5:                               # %.loopexit
	cmpl	$1, %r9d
	jne	.LBB19_43
# BB#6:                                 # %.preheader260
	movq	%rdi, -72(%rsp)         # 8-byte Spill
	cmpl	$0, -64(%rsp)           # 4-byte Folded Reload
	jle	.LBB19_23
# BB#7:                                 # %.preheader259.lr.ph
	movq	active_pps(%rip), %r13
	movq	listX+8(%rip), %r15
	movq	listX(%rip), %r12
	xorl	%r11d, %r11d
	movl	$-128, %r9d
	movl	$127, %r10d
	movl	$1023, %r14d            # imm = 0x3FF
	movq	-72(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB19_8:                               # %.preheader259
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_10 Depth 2
                                        #       Child Loop BB19_11 Depth 3
	cmpl	$0, -56(%rsp)           # 4-byte Folded Reload
	jle	.LBB19_22
# BB#9:                                 # %.preheader258.lr.ph
                                        #   in Loop: Header=BB19_8 Depth=1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB19_10:                              # %.preheader258
                                        #   Parent Loop BB19_8 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB19_11 Depth 3
	xorl	%ebp, %ebp
	jmp	.LBB19_11
.LBB19_18:                              #   in Loop: Header=BB19_11 Depth=3
	movl	5752(%rdi), %ebx
	subl	%eax, %ebx
	cmpl	$-129, %ebx
	cmovlel	%r9d, %ebx
	cmpl	$128, %ebx
	cmovgel	%r10d, %ebx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	movl	%edx, %eax
	negl	%eax
	cmpl	$-1, %ecx
	cmovgel	%edx, %eax
	addl	$16384, %eax            # imm = 0x4000
	cltd
	idivl	%ecx
	imull	%ebx, %eax
	addl	$32, %eax
	sarl	$6, %eax
	cmpl	$-1025, %eax            # imm = 0xFBFF
	movl	$-1024, %ecx            # imm = 0xFC00
	cmovlel	%ecx, %eax
	cmpl	$1024, %eax             # imm = 0x400
	cmovgel	%r14d, %eax
	sarl	$2, %eax
	movq	5784(%rdi), %rdx
	movq	8(%rdx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movl	%eax, (%rcx,%rbp)
	movl	$64, %ebx
	subl	%eax, %ebx
	movq	(%rdx), %rax
	movq	(%rax,%r11,8), %rax
	movq	(%rax,%rsi,8), %rax
	movl	%ebx, (%rax,%rbp)
	movl	(%rcx,%rbp), %edx
	addl	$64, %edx
	cmpl	$193, %edx
	jb	.LBB19_20
# BB#19:                                #   in Loop: Header=BB19_11 Depth=3
	movl	$32, (%rax,%rbp)
	movl	$32, (%rcx,%rbp)
	movq	-72(%rsp), %rax         # 8-byte Reload
	movq	5776(%rax), %rax
	movq	(%rax), %rcx
	movq	(%rcx,%r11,8), %rcx
	movl	$0, (%rcx,%rbp)
	movq	8(%rax), %rax
	movq	(%rax,%rsi,8), %rax
	movl	$0, (%rax,%rbp)
	jmp	.LBB19_20
	.p2align	4, 0x90
.LBB19_11:                              #   Parent Loop BB19_8 Depth=1
                                        #     Parent Loop BB19_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	1124(%r13), %eax
	cmpl	$2, %eax
	je	.LBB19_14
# BB#12:                                #   in Loop: Header=BB19_11 Depth=3
	cmpl	$1, %eax
	jne	.LBB19_20
# BB#13:                                #   in Loop: Header=BB19_11 Depth=3
	movq	-72(%rsp), %rcx         # 8-byte Reload
	movq	5768(%rcx), %rax
	movq	5784(%rcx), %rcx
	movq	(%rax), %rdx
	movq	(%rdx,%r11,8), %rdx
	movl	(%rdx,%rbp), %edx
	movq	(%rcx), %rbx
	movq	(%rbx,%r11,8), %rbx
	movq	(%rbx,%rsi,8), %rbx
	movl	%edx, (%rbx,%rbp)
	movq	8(%rax), %rax
	movq	(%rax,%rsi,8), %rax
	movl	(%rax,%rbp), %eax
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movl	%eax, (%rcx,%rbp)
	jmp	.LBB19_20
	.p2align	4, 0x90
.LBB19_14:                              #   in Loop: Header=BB19_11 Depth=3
	movq	(%r15,%rsi,8), %r8
	movl	4(%r8), %ecx
	movq	(%r12,%r11,8), %rdx
	movl	4(%rdx), %eax
	subl	%eax, %ecx
	cmpl	$-129, %ecx
	cmovlel	%r9d, %ecx
	cmpl	$128, %ecx
	cmovgel	%r10d, %ecx
	testl	%ecx, %ecx
	je	.LBB19_17
# BB#15:                                #   in Loop: Header=BB19_11 Depth=3
	cmpl	$0, 316844(%r8)
	jne	.LBB19_17
# BB#16:                                #   in Loop: Header=BB19_11 Depth=3
	cmpl	$0, 316844(%rdx)
	je	.LBB19_18
	.p2align	4, 0x90
.LBB19_17:                              #   in Loop: Header=BB19_11 Depth=3
	movq	-72(%rsp), %rax         # 8-byte Reload
	movq	5784(%rax), %rax
	movq	(%rax), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movl	$32, (%rcx,%rbp)
	movq	8(%rax), %rax
	movq	(%rax,%r11,8), %rax
	movq	(%rax,%rsi,8), %rax
	movl	$32, (%rax,%rbp)
.LBB19_20:                              #   in Loop: Header=BB19_11 Depth=3
	addq	$4, %rbp
	cmpq	$12, %rbp
	jne	.LBB19_11
# BB#21:                                #   in Loop: Header=BB19_10 Depth=2
	incq	%rsi
	cmpq	-56(%rsp), %rsi         # 8-byte Folded Reload
	jne	.LBB19_10
.LBB19_22:                              # %._crit_edge269
                                        #   in Loop: Header=BB19_8 Depth=1
	incq	%r11
	cmpq	-64(%rsp), %r11         # 8-byte Folded Reload
	jne	.LBB19_8
.LBB19_23:                              # %._crit_edge271
	cmpl	$0, -64(%rsp)           # 4-byte Folded Reload
	setle	%al
	movq	-72(%rsp), %r15         # 8-byte Reload
	cmpl	$0, 5624(%r15)
	je	.LBB19_43
# BB#24:                                # %._crit_edge271
	testb	%al, %al
	jne	.LBB19_43
# BB#25:                                # %.preheader256.lr.ph
	movslq	-64(%rsp), %rax         # 4-byte Folded Reload
	movslq	-56(%rsp), %rcx         # 4-byte Folded Reload
	addq	%rax, %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	addq	%rcx, %rcx
	movq	%rcx, -40(%rsp)         # 8-byte Spill
	movq	active_pps(%rip), %rdi
	leaq	5664(%r15), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	leaq	5668(%r15), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB19_26:                              # %.preheader256
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_28 Depth 2
                                        #       Child Loop BB19_29 Depth 3
                                        #         Child Loop BB19_30 Depth 4
	cmpl	$0, -56(%rsp)           # 4-byte Folded Reload
	jle	.LBB19_42
# BB#27:                                # %.preheader255.lr.ph
                                        #   in Loop: Header=BB19_26 Depth=1
	movq	5776(%r15), %rbp
	movl	%ebx, %eax
	shrl	$31, %eax
	addl	%ebx, %eax
	sarl	%eax
	movslq	%eax, %rcx
	movq	(%rbp), %rax
	movq	8(%rbp), %rdx
	movq	%rdx, -32(%rsp)         # 8-byte Spill
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %r10
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB19_28:                              # %.preheader255
                                        #   Parent Loop BB19_26 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB19_29 Depth 3
                                        #         Child Loop BB19_30 Depth 4
	movl	%r9d, %eax
	shrl	$31, %eax
	addl	%r9d, %eax
	sarl	%eax
	movslq	%eax, %rcx
	movq	-32(%rsp), %rax         # 8-byte Reload
	movq	%rcx, -64(%rsp)         # 8-byte Spill
	movq	(%rax,%rcx,8), %r12
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB19_29:                              # %.preheader
                                        #   Parent Loop BB19_26 Depth=1
                                        #     Parent Loop BB19_28 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB19_30 Depth 4
	movl	$2, %r14d
	jmp	.LBB19_30
.LBB19_37:                              #   in Loop: Header=BB19_30 Depth=4
	cmpq	$2, %r14
	movq	-24(%rsp), %rdx         # 8-byte Reload
	cmoveq	-16(%rsp), %rdx         # 8-byte Folded Reload
	movl	(%rdx), %esi
	subl	%eax, %esi
	cmpl	$-129, %esi
	movl	$-128, %eax
	cmovlel	%eax, %esi
	cmpl	$128, %esi
	movl	$127, %eax
	cmovgel	%eax, %esi
	movl	%r8d, %edx
	shrl	$31, %edx
	addl	%r8d, %edx
	sarl	%edx
	movl	%edx, %eax
	negl	%eax
	cmpl	$-1, %r8d
	cmovgel	%edx, %eax
	addl	$16384, %eax            # imm = 0x4000
	cltd
	idivl	%r8d
	imull	%esi, %eax
	addl	$32, %eax
	sarl	$6, %eax
	cmpl	$-1025, %eax            # imm = 0xFBFF
	movl	$-1024, %edx            # imm = 0xFC00
	cmovlel	%edx, %eax
	cmpl	$1024, %eax             # imm = 0x400
	movl	$1023, %edx             # imm = 0x3FF
	cmovgel	%edx, %eax
	sarl	$2, %eax
	movq	-72(%rsp), %r15         # 8-byte Reload
	movq	5784(%r15), %rsi
	movq	8(%rsi,%r14,8), %rdx
	movq	(%rdx,%rbx,8), %rdx
	movq	(%rdx,%r9,8), %r8
	movl	%eax, (%r8,%r11,4)
	movl	$64, %edx
	subl	%eax, %edx
	movq	(%rsi,%r14,8), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax,%r9,8), %rax
	movl	%edx, (%rax,%r11,4)
	movl	(%r8,%r11,4), %edx
	addl	$64, %edx
	cmpl	$193, %edx
	jb	.LBB19_39
# BB#38:                                #   in Loop: Header=BB19_30 Depth=4
	movl	$32, (%r8,%r11,4)
	movl	$32, (%rax,%r11,4)
	movl	$0, (%r13,%r11,4)
	movl	$0, (%rcx,%r11,4)
	jmp	.LBB19_39
	.p2align	4, 0x90
.LBB19_30:                              #   Parent Loop BB19_26 Depth=1
                                        #     Parent Loop BB19_28 Depth=2
                                        #       Parent Loop BB19_29 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	(%r10,%r11,4), %eax
	movq	(%rbp,%r14,8), %rcx
	movq	(%rcx,%rbx,8), %r13
	movl	%eax, (%r13,%r11,4)
	movl	(%r12,%r11,4), %eax
	movq	8(%rbp,%r14,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movl	%eax, (%rcx,%r11,4)
	movl	1124(%rdi), %eax
	cmpl	$2, %eax
	je	.LBB19_33
# BB#31:                                #   in Loop: Header=BB19_30 Depth=4
	cmpl	$1, %eax
	jne	.LBB19_39
# BB#32:                                #   in Loop: Header=BB19_30 Depth=4
	movq	5768(%r15), %rax
	movq	5784(%r15), %rcx
	movq	(%rax), %rdx
	movq	-8(%rsp), %rsi          # 8-byte Reload
	movq	(%rdx,%rsi,8), %rdx
	movl	(%rdx,%r11,4), %edx
	movq	(%rcx,%r14,8), %rsi
	movq	(%rsi,%rbx,8), %rsi
	movq	(%rsi,%r9,8), %rsi
	movl	%edx, (%rsi,%r11,4)
	movq	8(%rax), %rax
	movq	-64(%rsp), %rdx         # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movl	(%rax,%r11,4), %eax
	movq	8(%rcx,%r14,8), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movl	%eax, (%rcx,%r11,4)
	jmp	.LBB19_39
	.p2align	4, 0x90
.LBB19_33:                              #   in Loop: Header=BB19_30 Depth=4
	movq	listX+8(,%r14,8), %rax
	movq	(%rax,%r9,8), %r15
	movl	4(%r15), %r8d
	movq	listX(,%r14,8), %rax
	movq	(%rax,%rbx,8), %rdx
	movl	4(%rdx), %eax
	subl	%eax, %r8d
	cmpl	$-129, %r8d
	movl	$-128, %esi
	cmovlel	%esi, %r8d
	cmpl	$128, %r8d
	movl	$127, %esi
	cmovgel	%esi, %r8d
	testl	%r8d, %r8d
	je	.LBB19_36
# BB#34:                                #   in Loop: Header=BB19_30 Depth=4
	cmpl	$0, 316844(%r15)
	jne	.LBB19_36
# BB#35:                                #   in Loop: Header=BB19_30 Depth=4
	cmpl	$0, 316844(%rdx)
	je	.LBB19_37
	.p2align	4, 0x90
.LBB19_36:                              #   in Loop: Header=BB19_30 Depth=4
	movq	-72(%rsp), %r15         # 8-byte Reload
	movq	5784(%r15), %rax
	movq	(%rax,%r14,8), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movl	$32, (%rcx,%r11,4)
	movq	8(%rax,%r14,8), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax,%r9,8), %rax
	movl	$32, (%rax,%r11,4)
.LBB19_39:                              #   in Loop: Header=BB19_30 Depth=4
	addq	$2, %r14
	cmpq	$6, %r14
	jl	.LBB19_30
# BB#40:                                #   in Loop: Header=BB19_29 Depth=3
	incq	%r11
	cmpq	$3, %r11
	jne	.LBB19_29
# BB#41:                                #   in Loop: Header=BB19_28 Depth=2
	incq	%r9
	cmpq	-40(%rsp), %r9          # 8-byte Folded Reload
	jl	.LBB19_28
.LBB19_42:                              # %._crit_edge
                                        #   in Loop: Header=BB19_26 Depth=1
	incq	%rbx
	cmpq	-48(%rsp), %rbx         # 8-byte Folded Reload
	jl	.LBB19_26
.LBB19_43:                              # %.critedge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	fill_wp_params, .Lfunc_end19-fill_wp_params
	.cfi_endproc

	.globl	reset_wp_params
	.p2align	4, 0x90
	.type	reset_wp_params,@function
reset_wp_params:                        # @reset_wp_params
	.cfi_startproc
# BB#0:
	movq	5768(%rdi), %rcx
	movq	(%rcx), %r8
	movq	8(%rcx), %r9
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB20_1:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r8,%rsi,8), %r10
	movq	(%r9,%rsi,8), %rdx
	movzbl	5760(%rdi), %ecx
	movl	$1, %eax
	shll	%cl, %eax
	movl	%eax, (%r10)
	movl	%eax, (%rdx)
	movzbl	5764(%rdi), %ecx
	movl	$1, %eax
	shll	%cl, %eax
	movl	%eax, 4(%r10)
	movl	%eax, 4(%rdx)
	movzbl	5764(%rdi), %ecx
	movl	$1, %eax
	shll	%cl, %eax
	movl	%eax, 8(%r10)
	movl	%eax, 8(%rdx)
	incq	%rsi
	cmpq	$32, %rsi
	jne	.LBB20_1
# BB#2:
	retq
.Lfunc_end20:
	.size	reset_wp_params, .Lfunc_end20-reset_wp_params
	.cfi_endproc

	.type	dec_picture,@object     # @dec_picture
	.comm	dec_picture,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Source picture has higher bit depth than imgpel data type. Please recompile with larger data type for imgpel."
	.size	.L.str, 110

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"reading only from formats of 8, 16 or 32 bit allowed on big endian architecture"
	.size	.L.str.1, 80

	.type	find_snr.SubWidthC,@object # @find_snr.SubWidthC
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
find_snr.SubWidthC:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.size	find_snr.SubWidthC, 16

	.type	find_snr.SubHeightC,@object # @find_snr.SubHeightC
	.p2align	4
find_snr.SubHeightC:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.size	find_snr.SubHeightC, 16

	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"find_snr: buf"
	.size	.L.str.2, 14

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Error in seeking frame number: %d\n"
	.size	.L.str.3, 35

	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%04d(P)  %8d %5d %5d %7.4f %7.4f %7.4f  %s %5d\n"
	.size	.L.str.4, 48

	.type	get_block.cur_imgY,@object # @get_block.cur_imgY
	.local	get_block.cur_imgY
	.comm	get_block.cur_imgY,8,8
	.type	get_block.cur_lineY,@object # @get_block.cur_lineY
	.local	get_block.cur_lineY
	.comm	get_block.cur_lineY,8,8
	.type	get_block.jpos_m2,@object # @get_block.jpos_m2
	.local	get_block.jpos_m2
	.comm	get_block.jpos_m2,4,4
	.type	get_block.jpos_m1,@object # @get_block.jpos_m1
	.local	get_block.jpos_m1
	.comm	get_block.jpos_m1,4,4
	.type	get_block.jpos,@object  # @get_block.jpos
	.local	get_block.jpos
	.comm	get_block.jpos,4,4
	.type	get_block.jpos_p1,@object # @get_block.jpos_p1
	.local	get_block.jpos_p1
	.comm	get_block.jpos_p1,4,4
	.type	get_block.jpos_p2,@object # @get_block.jpos_p2
	.local	get_block.jpos_p2
	.comm	get_block.jpos_p2,4,4
	.type	get_block.jpos_p3,@object # @get_block.jpos_p3
	.local	get_block.jpos_p3
	.comm	get_block.jpos_p3,4,4
	.type	get_block.ipos_m2,@object # @get_block.ipos_m2
	.local	get_block.ipos_m2
	.comm	get_block.ipos_m2,4,4
	.type	get_block.ipos_m1,@object # @get_block.ipos_m1
	.local	get_block.ipos_m1
	.comm	get_block.ipos_m1,4,4
	.type	get_block.ipos,@object  # @get_block.ipos
	.local	get_block.ipos
	.comm	get_block.ipos,4,4
	.type	get_block.ipos_p1,@object # @get_block.ipos_p1
	.local	get_block.ipos_p1
	.comm	get_block.ipos_p1,4,4
	.type	get_block.ipos_p2,@object # @get_block.ipos_p2
	.local	get_block.ipos_p2
	.comm	get_block.ipos_p2,4,4
	.type	get_block.ipos_p3,@object # @get_block.ipos_p3
	.local	get_block.ipos_p3
	.comm	get_block.ipos_p3,4,4
	.type	non_conforming_stream,@object # @non_conforming_stream
	.comm	non_conforming_stream,4,4
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"RefPicList0[ num_ref_idx_l0_active_minus1 ] is equal to 'no reference picture', invalid bitstream"
	.size	.L.str.7, 98

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"RefPicList1[ num_ref_idx_l1_active_minus1 ] is equal to 'no reference picture', invalid bitstream"
	.size	.L.str.9, 98

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Error while getting the NALU in file format %s, exit\n"
	.size	.L.str.10, 54

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Annex B"
	.size	.L.str.11, 8

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"RTP"
	.size	.L.str.12, 4

	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"NALU: DP_A slice_id"
	.size	.L.str.16, 20

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"received data partition with CABAC, this is not allowed"
	.size	.L.str.17, 56

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"NALU: DP_B slice_id"
	.size	.L.str.18, 20

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"NALU: DP_B redudant_pic_cnt"
	.size	.L.str.20, 28

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"NALU: DP_C slice_id"
	.size	.L.str.21, 20

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"NALU:SLICE_C redudand_pic_cnt"
	.size	.L.str.23, 30

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"read_new_slice: Found NALU_TYPE_SEI, len %d\n"
	.size	.L.str.26, 45

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"read_new_slice: Found NALU_TYPE_FILL, len %d\n"
	.size	.L.str.27, 46

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"Found NALU type %d, len %d undefined, ignore NALU, moving on\n"
	.size	.L.str.29, 62

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"An unintentional loss of pictures occurs! Exit\n"
	.size	.L.str.30, 48

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"img->structure not initialized"
	.size	.L.str.31, 31

	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	.Lexit_picture.yuv_types,@object # @exit_picture.yuv_types
	.section	.rodata,"a",@progbits
	.p2align	4
.Lexit_picture.yuv_types:
	.asciz	"4:0:0"
	.asciz	"4:2:0"
	.asciz	"4:2:2"
	.asciz	"4:4:4"
	.size	.Lexit_picture.yuv_types, 24

	.type	.L.str.33,@object       # @.str.33
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.33:
	.asciz	"%04d(I)  %8d %5d %5d %7.4f %7.4f %7.4f  %s %5d\n"
	.size	.L.str.33, 48

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"%04d(SP) %8d %5d %5d %7.4f %7.4f %7.4f  %s %5d\n"
	.size	.L.str.34, 48

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"%04d(SI) %8d %5d %5d %7.4f %7.4f %7.4f  %s %5d\n"
	.size	.L.str.35, 48

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"%04d(RB) %8d %5d %5d %7.4f %7.4f %7.4f  %s %5d\n"
	.size	.L.str.36, 48

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"%04d(B)  %8d %5d %5d %7.4f %7.4f %7.4f  %s %5d\n"
	.size	.L.str.37, 48

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"Completed Decoding frame %05d.\r"
	.size	.L.str.38, 32

	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	old_slice,@object       # @old_slice
	.comm	old_slice,44,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8
	.type	last_out_fs,@object     # @last_out_fs
	.comm	last_out_fs,8,8
	.type	pocs_in_dpb,@object     # @pocs_in_dpb
	.comm	pocs_in_dpb,400,16
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"list[ref_frame] is equal to 'no reference picture' before RAP"
	.size	.Lstr, 62

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"RefPicList1[ num_ref_idx_l1_active_minus1 ] is equal to 'no reference picture'"
	.size	.Lstr.1, 79

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"RefPicList0[ num_ref_idx_l0_active_minus1 ] is equal to 'no reference picture'"
	.size	.Lstr.2, 79

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"Skipping these filling bits, proceeding w/ next NALU"
	.size	.Lstr.3, 53

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"found data partition C without matching DP A, discarding"
	.size	.Lstr.4, 57

	.type	.Lstr.5,@object         # @str.5
	.p2align	4
.Lstr.5:
	.asciz	"found data partition B without matching DP A, discarding"
	.size	.Lstr.5, 57

	.type	.Lstr.6,@object         # @str.6
	.p2align	4
.Lstr.6:
	.asciz	"got a data partition C which does not match DP_A"
	.size	.Lstr.6, 49

	.type	.Lstr.7,@object         # @str.7
	.p2align	4
.Lstr.7:
	.asciz	"got a data partition B which does not match DP_A"
	.size	.Lstr.7, 49

	.type	.Lstr.9,@object         # @str.9
	.p2align	4
.Lstr.9:
	.asciz	"Warning: Decoding does not start with an IDR picture."
	.size	.Lstr.9, 54

	.type	.Lstr.10,@object        # @str.10
	.p2align	4
.Lstr.10:
	.asciz	"Found NALU w/ forbidden_bit set, bit error?  Let's try..."
	.size	.Lstr.10, 58


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
