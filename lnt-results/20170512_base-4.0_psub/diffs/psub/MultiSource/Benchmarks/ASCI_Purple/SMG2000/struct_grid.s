	.text
	.file	"struct_grid.bc"
	.globl	hypre_StructGridCreate
	.p2align	4, 0x90
	.type	hypre_StructGridCreate,@function
hypre_StructGridCreate:                 # @hypre_StructGridCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %r15d
	movl	%edi, %ebx
	movl	$72, %edi
	callq	hypre_MAlloc
	movq	%rax, %rbp
	movl	%ebx, (%rbp)
	movl	%r15d, 4(%rbp)
	xorl	%edi, %edi
	callq	hypre_BoxArrayCreate
	movq	%rax, 8(%rbp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbp)
	movl	$2, 32(%rbp)
	movups	%xmm0, 52(%rbp)
	movups	%xmm0, 40(%rbp)
	movl	$1, 68(%rbp)
	movq	%rbp, (%r14)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_StructGridCreate, .Lfunc_end0-hypre_StructGridCreate
	.cfi_endproc

	.globl	hypre_StructGridRef
	.p2align	4, 0x90
	.type	hypre_StructGridRef,@function
hypre_StructGridRef:                    # @hypre_StructGridRef
	.cfi_startproc
# BB#0:
	incl	68(%rdi)
	movq	%rdi, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	hypre_StructGridRef, .Lfunc_end1-hypre_StructGridRef
	.cfi_endproc

	.globl	hypre_StructGridDestroy
	.p2align	4, 0x90
	.type	hypre_StructGridDestroy,@function
hypre_StructGridDestroy:                # @hypre_StructGridDestroy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB2_3
# BB#1:
	decl	68(%rbx)
	jne	.LBB2_3
# BB#2:
	movq	40(%rbx), %rdi
	callq	hypre_BoxDestroy
	movq	24(%rbx), %rdi
	callq	hypre_BoxNeighborsDestroy
	movq	16(%rbx), %rdi
	callq	hypre_Free
	movq	$0, 16(%rbx)
	movq	8(%rbx), %rdi
	callq	hypre_BoxArrayDestroy
	movq	%rbx, %rdi
	callq	hypre_Free
.LBB2_3:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end2:
	.size	hypre_StructGridDestroy, .Lfunc_end2-hypre_StructGridDestroy
	.cfi_endproc

	.globl	hypre_StructGridSetHoodInfo
	.p2align	4, 0x90
	.type	hypre_StructGridSetHoodInfo,@function
hypre_StructGridSetHoodInfo:            # @hypre_StructGridSetHoodInfo
	.cfi_startproc
# BB#0:
	movl	%esi, 32(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	hypre_StructGridSetHoodInfo, .Lfunc_end3-hypre_StructGridSetHoodInfo
	.cfi_endproc

	.globl	hypre_StructGridSetPeriodic
	.p2align	4, 0x90
	.type	hypre_StructGridSetPeriodic,@function
hypre_StructGridSetPeriodic:            # @hypre_StructGridSetPeriodic
	.cfi_startproc
# BB#0:
	movl	(%rsi), %eax
	movl	%eax, 56(%rdi)
	movl	4(%rsi), %eax
	movl	%eax, 60(%rdi)
	movl	8(%rsi), %eax
	movl	%eax, 64(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end4:
	.size	hypre_StructGridSetPeriodic, .Lfunc_end4-hypre_StructGridSetPeriodic
	.cfi_endproc

	.globl	hypre_StructGridSetExtents
	.p2align	4, 0x90
	.type	hypre_StructGridSetExtents,@function
hypre_StructGridSetExtents:             # @hypre_StructGridSetExtents
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 48
.Lcfi16:
	.cfi_offset %rbx, -40
.Lcfi17:
	.cfi_offset %r12, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %r15
	callq	hypre_BoxCreate
	movq	%rax, %rbx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	hypre_BoxSetExtents
	movq	8(%r15), %rsi
	movq	%rbx, %rdi
	callq	hypre_AppendBox
	movq	%rbx, %rdi
	callq	hypre_BoxDestroy
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	hypre_StructGridSetExtents, .Lfunc_end5-hypre_StructGridSetExtents
	.cfi_endproc

	.globl	hypre_StructGridSetBoxes
	.p2align	4, 0x90
	.type	hypre_StructGridSetBoxes,@function
hypre_StructGridSetBoxes:               # @hypre_StructGridSetBoxes
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 32
.Lcfi23:
	.cfi_offset %rbx, -24
.Lcfi24:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	callq	hypre_Free
	movq	%r14, 8(%rbx)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	hypre_StructGridSetBoxes, .Lfunc_end6-hypre_StructGridSetBoxes
	.cfi_endproc

	.globl	hypre_StructGridSetHood
	.p2align	4, 0x90
	.type	hypre_StructGridSetHood,@function
hypre_StructGridSetHood:                # @hypre_StructGridSetHood
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi31:
	.cfi_def_cfa_offset 96
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movl	%r14d, %edi
	callq	hypre_BoxArrayCreate
	movq	%rax, %r12
	leal	(,%r14,4), %edi
	callq	hypre_MAlloc
	movq	%rax, %r15
	testl	%r14d, %r14d
	jle	.LBB7_3
# BB#1:                                 # %.lr.ph
	movq	(%r13), %rsi
	movq	(%r12), %rax
	movslq	12(%rsp), %rdi          # 4-byte Folded Reload
	movl	%r14d, %ecx
	addq	$20, %rax
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rdi,4), %rdx
	leaq	(%rdi,%rdi,2), %rdi
	leaq	20(%rsi,%rdi,8), %rsi
	movq	%r15, %rdi
	.p2align	4, 0x90
.LBB7_2:                                # =>This Inner Loop Header: Depth=1
	movl	-20(%rsi), %ebp
	movl	%ebp, -20(%rax)
	movl	-16(%rsi), %ebp
	movl	%ebp, -16(%rax)
	movl	-12(%rsi), %ebp
	movl	%ebp, -12(%rax)
	movl	-8(%rsi), %ebp
	movl	%ebp, -8(%rax)
	movl	-4(%rsi), %ebp
	movl	%ebp, -4(%rax)
	movl	(%rsi), %ebp
	movl	%ebp, (%rax)
	movl	(%rdx), %ebp
	movl	%ebp, (%rdi)
	addq	$4, %rdi
	addq	$24, %rax
	addq	$4, %rdx
	addq	$24, %rsi
	decq	%rcx
	jne	.LBB7_2
.LBB7_3:                                # %._crit_edge
	movq	8(%rbx), %rdi
	callq	hypre_Free
	movq	$0, 8(%rbx)
	movq	16(%rbx), %rdi
	callq	hypre_Free
	movq	%r12, 8(%rbx)
	movq	%r15, 16(%rbx)
	leaq	32(%rsp), %rax
	movq	%rax, (%rsp)
	movq	%r13, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%r14d, %r8d
	movl	96(%rsp), %r9d
	callq	hypre_BoxNeighborsCreate
	movq	32(%rsp), %rax
	movq	%rax, 24(%rbx)
	movq	40(%rbx), %rdi
	callq	hypre_BoxDestroy
	movq	104(%rsp), %rax
	movq	%rax, 40(%rbx)
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	hypre_StructGridSetHood, .Lfunc_end7-hypre_StructGridSetHood
	.cfi_endproc

	.globl	hypre_StructGridAssemble
	.p2align	4, 0x90
	.type	hypre_StructGridAssemble,@function
hypre_StructGridAssemble:               # @hypre_StructGridAssemble
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 112
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	8(%r12), %r14
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB8_2
# BB#1:                                 # %._crit_edge195
	leaq	8(%r14), %r15
	jmp	.LBB8_33
.LBB8_2:
	movl	(%r12), %edi
	movl	4(%r12), %r15d
	leaq	24(%rsp), %rdx
	leaq	40(%rsp), %rcx
	leaq	20(%rsp), %r8
	movq	%r14, %rsi
	callq	hypre_GatherAllBoxes
	movl	8(%r14), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	callq	hypre_BoxCreate
	testl	%r15d, %r15d
	jle	.LBB8_10
# BB#3:                                 # %.lr.ph170
	movq	24(%rsp), %r8
	movq	(%r8), %r9
	leaq	36(%r9), %r10
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB8_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_7 Depth 2
	movl	(%r9,%r11,4), %ebp
	movl	12(%r9,%r11,4), %ebx
	movslq	8(%r8), %r13
	testq	%r13, %r13
	jle	.LBB8_8
# BB#5:                                 # %.lr.ph164
                                        #   in Loop: Header=BB8_4 Depth=1
	cmpl	$1, %r13d
	je	.LBB8_8
# BB#6:                                 # %._crit_edge190.preheader
                                        #   in Loop: Header=BB8_4 Depth=1
	movq	%r10, %rsi
	movl	$1, %edx
	.p2align	4, 0x90
.LBB8_7:                                # %._crit_edge190
                                        #   Parent Loop BB8_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-12(%rsi), %edi
	movl	(%rsi), %ecx
	cmpl	%edi, %ebp
	cmovgl	%edi, %ebp
	cmpl	%ecx, %ebx
	cmovll	%ecx, %ebx
	incq	%rdx
	addq	$24, %rsi
	cmpq	%r13, %rdx
	jl	.LBB8_7
.LBB8_8:                                # %._crit_edge165
                                        #   in Loop: Header=BB8_4 Depth=1
	movl	%ebp, (%rax,%r11,4)
	movl	%ebx, 12(%rax,%r11,4)
	incq	%r11
	addq	$4, %r10
	cmpq	%r15, %r11
	jne	.LBB8_4
# BB#9:                                 # %.preheader
	cmpl	$2, %r15d
	jg	.LBB8_22
.LBB8_10:                               # %.lr.ph158.preheader
	movslq	%r15d, %rcx
	movl	$3, %edx
	subq	%rcx, %rdx
	cmpq	$1, %rdx
	jbe	.LBB8_21
# BB#11:                                # %min.iters.checked
	movq	%rdx, %r9
	andq	$-2, %r9
	movq	%rdx, %rsi
	andq	$-2, %rsi
	je	.LBB8_21
# BB#12:                                # %vector.body.preheader
	leaq	-2(%rsi), %r8
	movl	%r8d, %ebp
	shrl	%ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB8_13
# BB#14:                                # %vector.body.prol.preheader
	leaq	12(%rax,%rcx,4), %rdi
	negq	%rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_15:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, -12(%rdi,%rbx,4)
	movq	$0, (%rdi,%rbx,4)
	addq	$2, %rbx
	incq	%rbp
	jne	.LBB8_15
	jmp	.LBB8_16
.LBB8_13:
	xorl	%ebx, %ebx
.LBB8_16:                               # %vector.body.prol.loopexit
	cmpq	$6, %r8
	jb	.LBB8_19
# BB#17:                                # %vector.body.preheader.new
	movq	%rsi, %rdi
	subq	%rbx, %rdi
	addq	%rcx, %rbx
	leaq	36(%rax,%rbx,4), %rbp
	.p2align	4, 0x90
.LBB8_18:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, -36(%rbp)
	movq	$0, -24(%rbp)
	movq	$0, -28(%rbp)
	movq	$0, -16(%rbp)
	movq	$0, -20(%rbp)
	movq	$0, -8(%rbp)
	movq	$0, -12(%rbp)
	movq	$0, (%rbp)
	addq	$32, %rbp
	addq	$-8, %rdi
	jne	.LBB8_18
.LBB8_19:                               # %middle.block
	cmpq	%rsi, %rdx
	je	.LBB8_22
# BB#20:
	addq	%r9, %rcx
	.p2align	4, 0x90
.LBB8_21:                               # %.lr.ph158
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, (%rax,%rcx,4)
	movl	$0, 12(%rax,%rcx,4)
	incq	%rcx
	cmpq	$3, %rcx
	jne	.LBB8_21
.LBB8_22:                               # %._crit_edge159
	movq	%rax, 40(%r12)
	movq	24(%rsp), %rcx
	movslq	8(%rcx), %r8
	testq	%r8, %r8
	jle	.LBB8_23
# BB#24:                                # %.lr.ph154
	movq	(%rcx), %rcx
	addq	$20, %rcx
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB8_25:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %eax
	movl	-12(%rcx), %ebx
	movl	%eax, %ebp
	subl	%ebx, %ebp
	incl	%ebp
	cmpl	%ebx, %eax
	movl	-8(%rcx), %r10d
	movl	-4(%rcx), %eax
	movl	-16(%rcx), %esi
	cmovsl	%r9d, %ebp
	movl	%eax, %ebx
	subl	%esi, %ebx
	incl	%ebx
	cmpl	%esi, %eax
	movl	-20(%rcx), %eax
	cmovsl	%r9d, %ebx
	movl	%r10d, %esi
	subl	%eax, %esi
	incl	%esi
	cmpl	%eax, %r10d
	cmovsl	%r9d, %esi
	imull	%ebx, %esi
	imull	%ebp, %esi
	addl	%esi, %edx
	incq	%rdi
	addq	$24, %rcx
	cmpq	%r8, %rdi
	jl	.LBB8_25
	jmp	.LBB8_26
.LBB8_23:
	xorl	%edx, %edx
.LBB8_26:                               # %._crit_edge155
	leaq	8(%r14), %r15
	movl	%edx, 52(%r12)
	leaq	24(%rsp), %rsi
	leaq	40(%rsp), %rdx
	leaq	20(%rsp), %rcx
	leaq	36(%rsp), %r8
	movq	%r12, %rdi
	callq	hypre_StructGridPeriodicAllBoxes
	movq	24(%rsp), %rbx
	movl	8(%rbx), %edi
	shll	$2, %edi
	callq	hypre_MAlloc
	movq	%rax, %rbp
	cmpl	$0, 8(%rbx)
	jle	.LBB8_29
# BB#27:                                # %.lr.ph148.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_28:                               # %.lr.ph148
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, (%rbp,%rax,4)
	incq	%rax
	movslq	8(%rbx), %rcx
	cmpq	%rcx, %rax
	jl	.LBB8_28
.LBB8_29:                               # %._crit_edge149
	movq	40(%rsp), %rsi
	movl	20(%rsp), %r13d
	movl	36(%rsp), %r9d
	leaq	48(%rsp), %rax
	movq	%rax, (%rsp)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	movl	%r13d, %ecx
	movl	32(%rsp), %r8d          # 4-byte Reload
	callq	hypre_BoxNeighborsCreate
	movq	48(%rsp), %rax
	movq	%rax, 24(%r12)
	movl	(%r15), %edi
	shll	$2, %edi
	callq	hypre_MAlloc
	cmpl	$0, (%r15)
	jle	.LBB8_32
# BB#30:                                # %.lr.ph144
	movslq	%r13d, %rcx
	leaq	(%rbp,%rcx,4), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB8_31:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rdx,4), %esi
	movl	%esi, (%rax,%rdx,4)
	incq	%rdx
	movslq	(%r15), %rsi
	cmpq	%rsi, %rdx
	jl	.LBB8_31
.LBB8_32:                               # %._crit_edge145
	movq	%rax, 16(%r12)
	movq	24(%r12), %rdi
.LBB8_33:
	movl	32(%r12), %esi
	movl	$1, %edx
	callq	hypre_BoxNeighborsAssemble
	movslq	(%r15), %r8
	testq	%r8, %r8
	jle	.LBB8_34
# BB#35:                                # %.lr.ph
	movq	(%r14), %rcx
	addq	$20, %rcx
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB8_36:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %eax
	movl	-12(%rcx), %ebx
	movl	%eax, %ebp
	subl	%ebx, %ebp
	incl	%ebp
	cmpl	%ebx, %eax
	movl	-8(%rcx), %r10d
	movl	-4(%rcx), %eax
	movl	-16(%rcx), %esi
	cmovsl	%r9d, %ebp
	movl	%eax, %ebx
	subl	%esi, %ebx
	incl	%ebx
	cmpl	%esi, %eax
	movl	-20(%rcx), %eax
	cmovsl	%r9d, %ebx
	movl	%r10d, %esi
	subl	%eax, %esi
	incl	%esi
	cmpl	%eax, %r10d
	cmovsl	%r9d, %esi
	imull	%ebx, %esi
	imull	%ebp, %esi
	addl	%esi, %edx
	incq	%rdi
	addq	$24, %rcx
	cmpq	%r8, %rdi
	jl	.LBB8_36
	jmp	.LBB8_37
.LBB8_34:
	xorl	%edx, %edx
.LBB8_37:                               # %._crit_edge
	movl	%edx, 48(%r12)
	xorl	%eax, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	hypre_StructGridAssemble, .Lfunc_end8-hypre_StructGridAssemble
	.cfi_endproc

	.globl	hypre_GatherAllBoxes
	.p2align	4, 0x90
	.type	hypre_GatherAllBoxes,@function
hypre_GatherAllBoxes:                   # @hypre_GatherAllBoxes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi57:
	.cfi_def_cfa_offset 176
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movq	%r8, 112(%rsp)          # 8-byte Spill
	movq	%rcx, %rbp
	movq	%rdx, %rbx
	movq	%rsi, %r12
	movl	%edi, %r15d
	leaq	12(%rsp), %rsi
	callq	hypre_MPI_Comm_size
	leaq	20(%rsp), %rsi
	movl	%r15d, %edi
	callq	hypre_MPI_Comm_rank
	movl	8(%r12), %eax
	leal	(,%rax,8), %ecx
	subl	%eax, %ecx
	movl	%ecx, 16(%rsp)
	movl	12(%rsp), %edi
	shll	$2, %edi
	callq	hypre_MAlloc
	movq	%rax, %r13
	movl	12(%rsp), %edi
	shll	$2, %edi
	callq	hypre_MAlloc
	movq	%rax, %r14
	subq	$8, %rsp
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	leaq	24(%rsp), %rdi
	movl	$1, %esi
	movl	$1, %edx
	movl	$1, %r8d
	movl	$1, %r9d
	movq	%r13, %rcx
	pushq	%r15
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	callq	hypre_MPI_Allgather
	addq	$16, %rsp
.Lcfi66:
	.cfi_adjust_cfa_offset -16
	movl	$0, (%r14)
	movl	(%r13), %eax
	cmpl	$2, 12(%rsp)
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	jl	.LBB9_1
# BB#2:                                 # %.lr.ph154.preheader
	xorl	%ecx, %ecx
	movl	$1, %edx
	movl	%eax, %ebx
	.p2align	4, 0x90
.LBB9_3:                                # %.lr.ph154
                                        # =>This Inner Loop Header: Depth=1
	addl	%eax, %ecx
	movl	%ecx, (%r14,%rdx,4)
	movl	(%r13,%rdx,4), %eax
	addl	%eax, %ebx
	incq	%rdx
	movslq	12(%rsp), %rsi
	cmpq	%rsi, %rdx
	jl	.LBB9_3
	jmp	.LBB9_4
.LBB9_1:
	movl	%eax, %ebx
.LBB9_4:                                # %._crit_edge155
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movl	16(%rsp), %edi
	shll	$2, %edi
	callq	hypre_MAlloc
	movq	%rax, %rbp
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	leal	(,%rbx,4), %edi
	callq	hypre_MAlloc
	movq	%rbp, %rdi
	movq	%rax, %r14
	cmpl	$0, 8(%r12)
	jle	.LBB9_7
# BB#5:                                 # %.lr.ph149
	movq	(%r12), %rax
	addq	$20, %rax
	movl	$5, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB9_6:                                # =>This Inner Loop Header: Depth=1
	leal	-5(%rcx), %esi
	movl	20(%rsp), %ebp
	movslq	%esi, %rsi
	movl	%ebp, (%rdi,%rsi,4)
	leal	-4(%rcx), %ebp
	movl	-20(%rax), %ebx
	movslq	%ebp, %rbp
	movl	%ebx, (%rdi,%rbp,4)
	movl	-8(%rax), %ebp
	movl	%ebp, 8(%rdi,%rsi,4)
	leal	-2(%rcx), %ebp
	movl	-16(%rax), %ebx
	movslq	%ebp, %rbp
	movl	%ebx, (%rdi,%rbp,4)
	movl	-4(%rax), %ebp
	movl	%ebp, 16(%rdi,%rsi,4)
	movl	-12(%rax), %ebp
	movslq	%ecx, %rcx
	movl	%ebp, (%rdi,%rcx,4)
	movl	(%rax), %ebp
	movl	%ebp, 24(%rdi,%rsi,4)
	incq	%rdx
	movslq	8(%r12), %rsi
	addq	$24, %rax
	addl	$7, %ecx
	cmpq	%rsi, %rdx
	jl	.LBB9_6
.LBB9_7:                                # %._crit_edge150
	movl	16(%rsp), %esi
	movl	$1, %edx
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	movq	%r14, %rcx
	movq	%r13, 88(%rsp)          # 8-byte Spill
	movq	%r13, %r8
	movq	32(%rsp), %r9           # 8-byte Reload
	pushq	%r15
.Lcfi67:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi68:
	.cfi_adjust_cfa_offset 8
	callq	hypre_MPI_Allgatherv
	addq	$16, %rsp
.Lcfi69:
	.cfi_adjust_cfa_offset -16
	movq	48(%rsp), %r15          # 8-byte Reload
	movslq	%r15d, %rax
	imulq	$-1840700269, %rax, %rbx # imm = 0x92492493
	shrq	$32, %rbx
	addl	%r15d, %ebx
	movl	%ebx, %eax
	shrl	$31, %eax
	sarl	$2, %ebx
	addl	%eax, %ebx
	movl	%ebx, %edi
	callq	hypre_BoxArrayCreate
	movq	%rax, 40(%rsp)          # 8-byte Spill
	shll	$2, %ebx
	movl	%ebx, %edi
	callq	hypre_MAlloc
	movq	%rax, 24(%rsp)          # 8-byte Spill
	callq	hypre_BoxCreate
	movq	%rax, %r13
	testl	%r15d, %r15d
	jle	.LBB9_8
# BB#9:                                 # %.lr.ph
	xorl	%ebx, %ebx
	movl	$-1, 8(%rsp)            # 4-byte Folded Spill
	movl	$5, %r12d
	xorl	%r15d, %r15d
	movq	24(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB9_10:                               # =>This Inner Loop Header: Depth=1
	movslq	%r15d, %rax
	movl	(%r14,%rax,4), %ecx
	movl	%ecx, (%rbp,%rbx,4)
	movl	4(%r14,%rax,4), %ecx
	movl	%ecx, 68(%rsp)
	movl	8(%r14,%rax,4), %ecx
	movl	%ecx, 56(%rsp)
	leal	3(%rax), %ecx
	movslq	%ecx, %rcx
	movl	(%r14,%rcx,4), %ecx
	movl	%ecx, 72(%rsp)
	movl	16(%r14,%rax,4), %ecx
	movl	%ecx, 60(%rsp)
	leal	5(%rax), %ecx
	movslq	%ecx, %rcx
	movl	(%r14,%rcx,4), %ecx
	movl	%ecx, 76(%rsp)
	movl	24(%r14,%rax,4), %ecx
	movl	%ecx, 64(%rsp)
	leal	7(%rax), %r15d
	movq	%r13, %rdi
	leaq	68(%rsp), %rsi
	leaq	56(%rsp), %rdx
	callq	hypre_BoxSetExtents
	movl	(%r13), %eax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rcx
	movl	%eax, -20(%rcx,%r12,4)
	movl	4(%r13), %eax
	movl	%eax, -16(%rcx,%r12,4)
	movl	8(%r13), %eax
	movl	%eax, -12(%rcx,%r12,4)
	movl	12(%r13), %eax
	movl	%eax, -8(%rcx,%r12,4)
	movl	16(%r13), %eax
	movl	%eax, -4(%rcx,%r12,4)
	movl	20(%r13), %eax
	movl	%eax, (%rcx,%r12,4)
	movl	8(%rsp), %ecx           # 4-byte Reload
	testl	%ecx, %ecx
	jns	.LBB9_12
# BB#11:                                #   in Loop: Header=BB9_10 Depth=1
	movl	(%rbp,%rbx,4), %eax
	cmpl	20(%rsp), %eax
	cmovel	%ebx, %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
.LBB9_12:                               #   in Loop: Header=BB9_10 Depth=1
	incq	%rbx
	addq	$6, %r12
	cmpl	48(%rsp), %r15d         # 4-byte Folded Reload
	jl	.LBB9_10
	jmp	.LBB9_13
.LBB9_8:
	movl	$-1, 8(%rsp)            # 4-byte Folded Spill
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB9_13:                               # %._crit_edge
	movq	%r13, %rdi
	callq	hypre_BoxDestroy
	movq	80(%rsp), %rdi          # 8-byte Reload
	callq	hypre_Free
	movq	%r14, %rdi
	callq	hypre_Free
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	hypre_Free
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	hypre_Free
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	%rbp, (%rax)
	movq	112(%rsp), %rax         # 8-byte Reload
	movl	8(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, (%rax)
	xorl	%eax, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	hypre_GatherAllBoxes, .Lfunc_end9-hypre_GatherAllBoxes
	.cfi_endproc

	.globl	hypre_StructGridPeriodicAllBoxes
	.p2align	4, 0x90
	.type	hypre_StructGridPeriodicAllBoxes,@function
hypre_StructGridPeriodicAllBoxes:       # @hypre_StructGridPeriodicAllBoxes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi76:
	.cfi_def_cfa_offset 288
.Lcfi77:
	.cfi_offset %rbx, -56
.Lcfi78:
	.cfi_offset %r12, -48
.Lcfi79:
	.cfi_offset %r13, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movl	56(%rdi), %ebx
	movl	60(%rdi), %eax
	movl	64(%rdi), %r14d
	xorl	%r12d, %r12d
	testl	%ebx, %ebx
	setne	%r11b
	xorl	%edi, %edi
	testl	%eax, %eax
	setne	%r9b
	xorl	%r13d, %r13d
	testl	%r14d, %r14d
	setne	%r10b
	movl	%eax, %ebp
	orl	%ebx, %ebp
	orl	%r14d, %ebp
	je	.LBB10_1
# BB#2:
	movq	%r8, 176(%rsp)          # 8-byte Spill
	movb	%r11b, %r12b
	movb	%r9b, %dil
	movq	%rdi, 136(%rsp)         # 8-byte Spill
	movb	%r10b, %r13b
	movq	%rsi, 152(%rsp)         # 8-byte Spill
	movq	(%rsi), %rsi
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	movq	(%rdx), %rdx
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	movl	(%rcx), %ecx
	movl	%ecx, 44(%rsp)          # 4-byte Spill
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movslq	8(%rsi), %rsi
	leal	1(%r12,%r12), %edx
	leal	1(%rdi,%rdi), %ecx
	imull	%edx, %ecx
	leal	1(%r13,%r13), %r15d
	movq	%rsi, 128(%rsp)         # 8-byte Spill
	imull	%esi, %r15d
	imull	%ecx, %r15d
	movl	%r15d, %edi
	movl	%eax, %ebp
	callq	hypre_BoxArrayCreate
	movq	%rax, 64(%rsp)          # 8-byte Spill
	shll	$2, %r15d
	movl	%r15d, %edi
	movq	136(%rsp), %r15         # 8-byte Reload
	callq	hypre_MAlloc
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%ebx, 88(%rsp)          # 4-byte Spill
	movl	%ebx, %eax
	negl	%eax
	sbbl	%eax, %eax
	movl	%eax, 84(%rsp)          # 4-byte Spill
	movl	%ebp, 104(%rsp)         # 4-byte Spill
	movl	%ebp, %eax
	negl	%eax
	sbbl	%eax, %eax
	movl	%eax, 96(%rsp)          # 4-byte Spill
	movl	%r14d, %eax
	negl	%eax
	sbbl	%eax, %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	testl	%r14d, %r14d
	setne	%al
	movl	$1, %ecx
	subl	%eax, %ecx
	movl	%ecx, 100(%rsp)         # 4-byte Spill
	movl	$2, %ecx
	subl	%eax, %ecx
	movl	%ecx, 92(%rsp)          # 4-byte Spill
	xorl	%r11d, %r11d
                                        # implicit-def: %EAX
	movl	%eax, 36(%rsp)          # 4-byte Spill
	xorl	%ebx, %ebx
	movl	$0, 40(%rsp)            # 4-byte Folded Spill
	movq	%r12, 184(%rsp)         # 8-byte Spill
	movq	%r13, %rdx
	movl	%r14d, 108(%rsp)        # 4-byte Spill
	movq	%rdx, 200(%rsp)         # 8-byte Spill
	jmp	.LBB10_3
	.p2align	4, 0x90
.LBB10_23:                              # %._crit_edge263
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpl	44(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB10_3
# BB#24:                                #   in Loop: Header=BB10_3 Depth=1
	movl	44(%rsp), %eax          # 4-byte Reload
	movl	16(%rsp), %ecx          # 4-byte Reload
	subl	%ecx, %eax
	subl	12(%rsp), %eax          # 4-byte Folded Reload
	addl	%ebx, %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB10_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_5 Depth 2
                                        #     Child Loop BB10_10 Depth 2
                                        #       Child Loop BB10_12 Depth 3
                                        #         Child Loop BB10_15 Depth 4
                                        #           Child Loop BB10_17 Depth 5
	movl	%ebx, %esi
	cmpl	128(%rsp), %r11d        # 4-byte Folded Reload
	jge	.LBB10_25
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB10_3 Depth=1
	movl	%r11d, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movslq	%eax, %r13
	movl	%esi, 16(%rsp)          # 4-byte Spill
	movslq	%esi, %rsi
	movq	120(%rsp), %r9          # 8-byte Reload
	leaq	(%r9,%r13,4), %rbp
	leaq	(%r13,%r13,2), %rax
	leaq	20(,%rax,8), %rax
	movq	56(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rsi,4), %r8
	leaq	(%rsi,%rsi,2), %rsi
	leaq	20(,%rsi,8), %rsi
	xorl	%ebx, %ebx
	movq	128(%rsp), %r10         # 8-byte Reload
	movl	84(%rsp), %r11d         # 4-byte Reload
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB10_5:                               #   Parent Loop BB10_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp,%rbx,4), %edi
	cmpl	(%r9,%r13,4), %edi
	jne	.LBB10_7
# BB#6:                                 #   in Loop: Header=BB10_5 Depth=2
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rdi
	movl	-20(%rdi,%rax), %ebp
	movq	%rbx, %rcx
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %rbx
	movl	%ebp, -20(%rbx,%rsi)
	movl	-16(%rdi,%rax), %ebp
	movl	%ebp, -16(%rbx,%rsi)
	movl	-12(%rdi,%rax), %ebp
	movl	%ebp, -12(%rbx,%rsi)
	movl	-8(%rdi,%rax), %ebp
	movl	%ebp, -8(%rbx,%rsi)
	movl	-4(%rdi,%rax), %ebp
	movl	%ebp, -4(%rbx,%rsi)
	movq	144(%rsp), %rbp         # 8-byte Reload
	movl	(%rdi,%rax), %edi
	movl	%edi, (%rbx,%rsi)
	movq	%rcx, %rbx
	movl	(%rbp,%rbx,4), %edi
	movl	%edi, (%r8,%rbx,4)
	addq	$24, %rax
	leaq	1(%r13,%rbx), %rdi
	incq	%rbx
	addq	$24, %rsi
	cmpq	%r10, %rdi
	jl	.LBB10_5
.LBB10_7:                               # %.._crit_edge_crit_edge
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%rbx), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	addl	16(%rsp), %ebx          # 4-byte Folded Reload
	cmpl	%r12d, %r11d
	jle	.LBB10_9
# BB#8:                                 #   in Loop: Header=BB10_3 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r11d
	jmp	.LBB10_23
	.p2align	4, 0x90
.LBB10_9:                               # %.lr.ph262
                                        #   in Loop: Header=BB10_3 Depth=1
	movl	12(%rsp), %ecx          # 4-byte Reload
	movslq	%ecx, %rsi
	leal	-1(%rsi), %eax
	subl	24(%rsp), %eax          # 4-byte Folded Reload
	incl	%eax
	movl	%eax, 112(%rsp)         # 4-byte Spill
	leaq	(%r13,%r13), %rax
	leaq	(%rax,%rax,2), %rax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	subq	%r13, %rsi
	movq	%rsi, 216(%rsp)         # 8-byte Spill
	movl	%r11d, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	%ecx, %r11d
	.p2align	4, 0x90
.LBB10_10:                              #   Parent Loop BB10_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_12 Depth 3
                                        #         Child Loop BB10_15 Depth 4
                                        #           Child Loop BB10_17 Depth 5
	movl	96(%rsp), %eax          # 4-byte Reload
	cmpl	%r15d, %eax
	jg	.LBB10_22
# BB#11:                                # %.lr.ph253
                                        #   in Loop: Header=BB10_10 Depth=2
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %r13d
	imull	88(%rsp), %r13d         # 4-byte Folded Reload
	movl	%eax, %edi
	.p2align	4, 0x90
.LBB10_12:                              #   Parent Loop BB10_3 Depth=1
                                        #     Parent Loop BB10_10 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB10_15 Depth 4
                                        #           Child Loop BB10_17 Depth 5
	cmpl	%edx, 20(%rsp)          # 4-byte Folded Reload
	movq	%rdi, 192(%rsp)         # 8-byte Spill
	jg	.LBB10_21
# BB#13:                                # %.lr.ph245
                                        #   in Loop: Header=BB10_12 Depth=3
	movl	%edi, %ecx
	orl	48(%rsp), %ecx          # 4-byte Folded Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	cmpl	12(%rsp), %esi          # 4-byte Folded Reload
	jge	.LBB10_20
# BB#14:                                # %.lr.ph245.split.us.preheader
                                        #   in Loop: Header=BB10_12 Depth=3
	movl	%edi, %r12d
	imull	104(%rsp), %r12d        # 4-byte Folded Reload
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r15d
	movl	%ecx, 116(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB10_15:                              # %.lr.ph245.split.us
                                        #   Parent Loop BB10_3 Depth=1
                                        #     Parent Loop BB10_10 Depth=2
                                        #       Parent Loop BB10_12 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB10_17 Depth 5
	movl	%ecx, %eax
	orl	%r15d, %eax
	je	.LBB10_19
# BB#16:                                # %.lr.ph238.us
                                        #   in Loop: Header=BB10_15 Depth=4
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movl	%r15d, %r11d
	imull	%r14d, %r11d
	movq	%rbx, 224(%rsp)         # 8-byte Spill
	movslq	%ebx, %rdx
	movq	208(%rsp), %rcx         # 8-byte Reload
	leaq	20(%rax,%rcx,4), %rsi
	leaq	(%rdx,%rdx,2), %rcx
	shlq	$3, %rcx
	movq	64(%rsp), %rax          # 8-byte Reload
	addq	(%rax), %rcx
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx,4), %rax
	movq	216(%rsp), %rdx         # 8-byte Reload
	.p2align	4, 0x90
.LBB10_17:                              #   Parent Loop BB10_3 Depth=1
                                        #     Parent Loop BB10_10 Depth=2
                                        #       Parent Loop BB10_12 Depth=3
                                        #         Parent Loop BB10_15 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movl	-20(%rsi), %ebx
	movl	%ebx, (%rcx)
	movl	-16(%rsi), %edi
	movl	%edi, 4(%rcx)
	movl	-12(%rsi), %r10d
	movl	%r10d, 8(%rcx)
	movl	-8(%rsi), %r14d
	movl	%r14d, 12(%rcx)
	movl	-4(%rsi), %r8d
	movl	%r8d, 16(%rcx)
	addl	%r13d, %ebx
	movl	(%rsi), %r9d
	addl	%r11d, %r9d
	movl	%ebx, (%rcx)
	addl	%r12d, %edi
	movl	%edi, 4(%rcx)
	addl	%r11d, %r10d
	movl	%r10d, 8(%rcx)
	addl	%r13d, %r14d
	movl	%r14d, 12(%rcx)
	addl	%r12d, %r8d
	movl	%r8d, 16(%rcx)
	movl	%r9d, 20(%rcx)
	movl	(%rbp), %edi
	movl	%edi, (%rax)
	addq	$24, %rsi
	addq	$4, %rbp
	addq	$24, %rcx
	addq	$4, %rax
	decq	%rdx
	jne	.LBB10_17
# BB#18:                                # %..loopexit_crit_edge.us.loopexit
                                        #   in Loop: Header=BB10_15 Depth=4
	movq	224(%rsp), %rbx         # 8-byte Reload
	addl	112(%rsp), %ebx         # 4-byte Folded Reload
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r11d
	movl	108(%rsp), %r14d        # 4-byte Reload
	movq	200(%rsp), %rdx         # 8-byte Reload
	movq	144(%rsp), %rbp         # 8-byte Reload
	movl	116(%rsp), %ecx         # 4-byte Reload
.LBB10_19:                              # %..loopexit_crit_edge.us
                                        #   in Loop: Header=BB10_15 Depth=4
	cmpl	%edx, %r15d
	leal	1(%r15), %eax
	movl	%eax, %r15d
	jl	.LBB10_15
	jmp	.LBB10_21
	.p2align	4, 0x90
.LBB10_20:                              # %.lr.ph245.split
                                        #   in Loop: Header=BB10_12 Depth=3
	movl	%ecx, %eax
	movl	20(%rsp), %r8d          # 4-byte Reload
	orl	%r8d, %eax
	cmovnel	%esi, %r11d
	cmpl	%edx, %r8d
	jge	.LBB10_21
# BB#27:                                # %.lr.ph245.split.1
                                        #   in Loop: Header=BB10_12 Depth=3
	movl	%ecx, %eax
	movl	100(%rsp), %edi         # 4-byte Reload
	orl	%edi, %eax
	cmovnel	%esi, %r11d
	cmpl	%edx, %edi
	jge	.LBB10_21
# BB#28:                                # %.lr.ph245.split.2
                                        #   in Loop: Header=BB10_12 Depth=3
	orl	92(%rsp), %ecx          # 4-byte Folded Reload
	cmovnel	%esi, %r11d
	.p2align	4, 0x90
.LBB10_21:                              # %._crit_edge246
                                        #   in Loop: Header=BB10_12 Depth=3
	movq	136(%rsp), %r15         # 8-byte Reload
	movq	192(%rsp), %rax         # 8-byte Reload
	cmpl	%r15d, %eax
	leal	1(%rax), %eax
	movl	%eax, %edi
	jl	.LBB10_12
.LBB10_22:                              # %._crit_edge254
                                        #   in Loop: Header=BB10_10 Depth=2
	movq	184(%rsp), %r12         # 8-byte Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	cmpl	%r12d, %eax
	leal	1(%rax), %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 48(%rsp)          # 8-byte Spill
	jl	.LBB10_10
	jmp	.LBB10_23
.LBB10_25:
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	hypre_BoxArraySetSize
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	hypre_BoxArrayDestroy
	movq	120(%rsp), %rdi         # 8-byte Reload
	callq	hypre_Free
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	%rbx, (%rax)
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	168(%rsp), %rax         # 8-byte Reload
	movl	36(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, (%rax)
	movq	176(%rsp), %r8          # 8-byte Reload
	movl	40(%rsp), %eax          # 4-byte Reload
	jmp	.LBB10_26
.LBB10_1:
	xorl	%eax, %eax
.LBB10_26:
	movl	%eax, (%r8)
	xorl	%eax, %eax
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	hypre_StructGridPeriodicAllBoxes, .Lfunc_end10-hypre_StructGridPeriodicAllBoxes
	.cfi_endproc

	.globl	hypre_StructGridPrint
	.p2align	4, 0x90
	.type	hypre_StructGridPrint,@function
hypre_StructGridPrint:                  # @hypre_StructGridPrint
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi85:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 48
.Lcfi88:
	.cfi_offset %rbx, -48
.Lcfi89:
	.cfi_offset %r12, -40
.Lcfi90:
	.cfi_offset %r13, -32
.Lcfi91:
	.cfi_offset %r14, -24
.Lcfi92:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	4(%rbx), %edx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	8(%rbx), %r15
	movl	8(%r15), %edx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	cmpl	$0, 8(%r15)
	jle	.LBB11_3
# BB#1:                                 # %.lr.ph
	movl	$20, %ebx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB11_2:                               # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movl	-20(%rax,%rbx), %ecx
	movl	-16(%rax,%rbx), %r8d
	movl	-12(%rax,%rbx), %r9d
	movl	-8(%rax,%rbx), %r10d
	movl	-4(%rax,%rbx), %r11d
	movl	(%rax,%rbx), %r13d
	subq	$8, %rsp
.Lcfi93:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.1, %esi
	movl	$0, %eax
	movq	%r14, %rdi
	movl	%r12d, %edx
	pushq	%r13
.Lcfi94:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi95:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi96:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$32, %rsp
.Lcfi97:
	.cfi_adjust_cfa_offset -32
	incq	%r12
	movslq	8(%r15), %rax
	addq	$24, %rbx
	cmpq	%rax, %r12
	jl	.LBB11_2
.LBB11_3:                               # %._crit_edge
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	hypre_StructGridPrint, .Lfunc_end11-hypre_StructGridPrint
	.cfi_endproc

	.globl	hypre_StructGridRead
	.p2align	4, 0x90
	.type	hypre_StructGridRead,@function
hypre_StructGridRead:                   # @hypre_StructGridRead
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi100:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi101:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi102:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi104:
	.cfi_def_cfa_offset 112
.Lcfi105:
	.cfi_offset %rbx, -56
.Lcfi106:
	.cfi_offset %r12, -48
.Lcfi107:
	.cfi_offset %r13, -40
.Lcfi108:
	.cfi_offset %r14, -32
.Lcfi109:
	.cfi_offset %r15, -24
.Lcfi110:
	.cfi_offset %rbp, -16
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %rbx
	movl	%edi, %r14d
	leaq	12(%rsp), %rdx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movl	12(%rsp), %ebp
	movl	$72, %edi
	callq	hypre_MAlloc
	movq	%rax, %r15
	movl	%r14d, (%r15)
	movl	%ebp, 4(%r15)
	xorl	%edi, %edi
	callq	hypre_BoxArrayCreate
	movq	%rax, 8(%r15)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r15)
	movl	$2, 32(%r15)
	movups	%xmm0, 52(%r15)
	movups	%xmm0, 40(%r15)
	movl	$1, 68(%r15)
	leaq	8(%rsp), %rdx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	cmpl	$0, 8(%rsp)
	jle	.LBB12_3
# BB#1:                                 # %.lr.ph
	leaq	44(%rsp), %r12
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB12_2:                               # =>This Inner Loop Header: Depth=1
	subq	$8, %rsp
.Lcfi111:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.1, %esi
	movl	$0, %eax
	movq	%rbx, %rdi
	leaq	36(%rsp), %rdx
	movq	%r12, %rcx
	leaq	56(%rsp), %r8
	leaq	60(%rsp), %r9
	leaq	48(%rsp), %rbp
	pushq	%rbp
.Lcfi112:
	.cfi_adjust_cfa_offset 8
	leaq	52(%rsp), %rbp
	pushq	%rbp
.Lcfi113:
	.cfi_adjust_cfa_offset 8
	leaq	56(%rsp), %rbp
	pushq	%rbp
.Lcfi114:
	.cfi_adjust_cfa_offset 8
	callq	fscanf
	addq	$32, %rsp
.Lcfi115:
	.cfi_adjust_cfa_offset -32
	callq	hypre_BoxCreate
	movq	%rax, %r13
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	hypre_BoxSetExtents
	movq	8(%r15), %rsi
	movq	%r13, %rdi
	callq	hypre_AppendBox
	movq	%r13, %rdi
	callq	hypre_BoxDestroy
	incl	%r14d
	cmpl	8(%rsp), %r14d
	jl	.LBB12_2
.LBB12_3:                               # %._crit_edge
	movq	%r15, %rdi
	callq	hypre_StructGridAssemble
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%r15, (%rax)
	xorl	%eax, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	hypre_StructGridRead, .Lfunc_end12-hypre_StructGridRead
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d\n"
	.size	.L.str, 4

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%d:  (%d, %d, %d)  x  (%d, %d, %d)\n"
	.size	.L.str.1, 36


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
