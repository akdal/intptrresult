	.text
	.file	"z40.bc"
	.globl	FilterInit
	.p2align	4, 0x90
	.type	FilterInit,@function
FilterInit:                             # @FilterInit
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$0, filter_count(%rip)
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_1
# BB#2:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_3
.LBB0_1:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_3:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, filter_active(%rip)
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movl	$.L.str.1, %esi
	callq	MakeWord
	movq	FilterInSym(%rip), %rcx
	movq	%rax, 56(%rcx)
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movl	$.L.str.2, %esi
	callq	MakeWord
	movq	FilterOutSym(%rip), %rcx
	movq	%rax, 56(%rcx)
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movl	$.L.str.3, %esi
	callq	MakeWord
	movq	FilterErrSym(%rip), %rcx
	movq	%rax, 56(%rcx)
	movq	FilterInSym(%rip), %rax
	movq	56(%rax), %rax
	movq	%rax, filter_in_filename(%rip)
	movq	FilterOutSym(%rip), %rax
	movq	56(%rax), %rax
	movq	%rax, filter_out_filename(%rip)
	popq	%rax
	retq
.Lfunc_end0:
	.size	FilterInit, .Lfunc_end0-FilterInit
	.cfi_endproc

	.globl	FilterCreate
	.p2align	4, 0x90
	.type	FilterCreate,@function
FilterCreate:                           # @FilterCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$2056, %rsp             # imm = 0x808
.Lcfi7:
	.cfi_def_cfa_offset 2112
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movl	%edi, %r12d
	movzbl	zz_lengths+57(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_1
# BB#2:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_3
.LBB1_1:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB1_3:
	movb	$57, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzwl	2(%r15), %eax
	movw	%ax, 34(%rbx)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	4(%r15), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%rbx), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%rbx)
	andl	4(%r15), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbx)
	movl	filter_count(%rip), %ecx
	incl	%ecx
	movl	%ecx, filter_count(%rip)
	movq	%rsp, %rbp
	movl	$.L.str.4, %esi
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sprintf
	movl	$.L.str.5, %esi
	movq	%rbp, %rdi
	callq	fopen
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.LBB1_5
# BB#4:
	movq	%rsp, %r9
	movl	$40, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	callq	Error
.LBB1_5:
	movq	%rsp, %rsi
	movl	$11, %edi
	movq	%r15, %rdx
	callq	MakeWord
	movq	%rax, %rbp
	movl	%r12d, %eax
	shll	$12, %eax
	andl	$4190208, %eax          # imm = 0x3FF000
	movl	$-4190209, %ecx         # imm = 0xFFC00FFF
	andl	40(%rbp), %ecx
	orl	%eax, %ecx
	movl	%ecx, 40(%rbp)
	movq	%r14, 48(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_6
# BB#7:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_8
.LBB1_6:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_8:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_11
# BB#9:
	testq	%rax, %rax
	je	.LBB1_11
# BB#10:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_11:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_14
# BB#12:
	testq	%rax, %rax
	je	.LBB1_14
# BB#13:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_14:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_15
# BB#16:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_17
.LBB1_15:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_17:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	filter_active(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_20
# BB#18:
	testq	%rcx, %rcx
	je	.LBB1_20
# BB#19:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_20:
	testq	%rbp, %rbp
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB1_23
# BB#21:
	testq	%rax, %rax
	je	.LBB1_23
# BB#22:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_23:
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movl	%r12d, %esi
	movq	%r15, %rdx
	callq	LexScanVerbatim
	movq	%r13, %rdi
	callq	fclose
	movl	filter_count(%rip), %ecx
	movq	%rsp, %rbp
	movl	$.L.str.4, %esi
	movl	$.L.str.2, %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sprintf
	movl	$11, %edi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	callq	MakeWord
	movq	%rax, %rbp
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_24
# BB#25:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_26
.LBB1_24:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_26:
	testq	%rbx, %rbx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB1_29
# BB#27:
	testq	%rax, %rax
	je	.LBB1_29
# BB#28:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_29:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_32
# BB#30:
	testq	%rax, %rax
	je	.LBB1_32
# BB#31:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_32:
	movzwl	41(%r14), %eax
	testb	$1, %ah
	je	.LBB1_34
# BB#33:
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	PushScope
.LBB1_34:
	callq	GetScopeSnapshot
	movq	%rax, %rbp
	movzwl	41(%r14), %eax
	testb	$1, %ah
	je	.LBB1_36
# BB#35:
	callq	PopScope
.LBB1_36:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_37
# BB#38:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_39
.LBB1_37:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_39:
	testq	%rbx, %rbx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB1_42
# BB#40:
	testq	%rax, %rax
	je	.LBB1_42
# BB#41:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_42:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_45
# BB#43:
	testq	%rax, %rax
	je	.LBB1_45
# BB#44:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_45:
	movq	%rbx, %rax
	addq	$2056, %rsp             # imm = 0x808
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	FilterCreate, .Lfunc_end1-FilterCreate
	.cfi_endproc

	.globl	FilterSetFileNames
	.p2align	4, 0x90
	.type	FilterSetFileNames,@function
FilterSetFileNames:                     # @FilterSetFileNames
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -24
.Lcfi18:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	cmpb	$57, 32(%r14)
	je	.LBB2_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.7, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_2:
	movq	8(%r14), %rbx
	cmpq	%r14, %rbx
	leaq	8(%r14), %r14
	jne	.LBB2_4
# BB#3:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.7, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.9, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%r14), %rbx
	.p2align	4, 0x90
.LBB2_4:                                # =>This Inner Loop Header: Depth=1
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB2_4
# BB#5:
	cmpb	$11, %al
	je	.LBB2_7
# BB#6:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.7, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.10, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_7:                                # %.loopexit12
	movq	FilterInSym(%rip), %rax
	movq	%rbx, 56(%rax)
	movq	(%r14), %rax
	movq	8(%rax), %rbx
	.p2align	4, 0x90
.LBB2_8:                                # =>This Inner Loop Header: Depth=1
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB2_8
# BB#9:
	cmpb	$11, %al
	je	.LBB2_11
# BB#10:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.7, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.11, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_11:                               # %.loopexit
	movq	FilterOutSym(%rip), %rax
	movq	%rbx, 56(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	FilterSetFileNames, .Lfunc_end2-FilterSetFileNames
	.cfi_endproc

	.globl	FilterExecute
	.p2align	4, 0x90
	.type	FilterExecute,@function
FilterExecute:                          # @FilterExecute
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$2072, %rsp             # imm = 0x818
.Lcfi25:
	.cfi_def_cfa_offset 2128
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	cmpb	$57, 32(%r15)
	je	.LBB3_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.7, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_2:
	cmpb	$82, 32(%rbx)
	je	.LBB3_4
# BB#3:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.7, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.13, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_4:
	leaq	32(%r15), %r13
	movq	filter_in_filename(%rip), %rax
	movq	FilterInSym(%rip), %rcx
	movq	%rax, 56(%rcx)
	cmpl	$0, SafeExecution(%rip)
	je	.LBB3_6
# BB#5:
	movl	$40, %edi
	movl	$2, %esi
	movl	$.L.str.14, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r13, %r8
	movq	%r14, %r9
	callq	Error
	movl	$11, %edi
	movl	$.L.str.15, %esi
	movq	%r13, %rdx
	callq	MakeWord
	movq	%rax, %rbx
	jmp	.LBB3_18
.LBB3_6:
	movq	%r14, %rdi
	callq	system
	movl	%eax, %r12d
	movl	$.L.str.3, %edi
	movl	$.L.str.16, %esi
	callq	fopen
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB3_13
# BB#7:                                 # %.preheader
	leaq	16(%rsp), %rdi
	movl	$2048, %esi             # imm = 0x800
	movq	%rbp, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB3_12
# BB#8:                                 # %.lr.ph
	leaq	16(%rsp), %rbx
	.p2align	4, 0x90
.LBB3_9:                                # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	strlen
	cmpb	$10, 15(%rsp,%rax)
	jne	.LBB3_11
# BB#10:                                #   in Loop: Header=BB3_9 Depth=1
	movb	$0, 15(%rsp,%rax)
.LBB3_11:                               #   in Loop: Header=BB3_9 Depth=1
	movl	$40, %edi
	movl	$3, %esi
	movl	$.L.str.17, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r13, %r8
	movq	%rbx, %r9
	callq	Error
	movl	$2048, %esi             # imm = 0x800
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fgets
	testq	%rax, %rax
	jne	.LBB3_9
.LBB3_12:                               # %._crit_edge
	movq	%rbp, %rdi
	callq	fclose
	movl	$.L.str.3, %edi
	callq	remove
.LBB3_13:
	testl	%r12d, %r12d
	je	.LBB3_15
# BB#14:
	movl	$40, %edi
	movl	$4, %esi
	movl	$.L.str.18, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r13, %r8
	movq	%r14, %r9
	callq	Error
.LBB3_15:
	movq	(%r15), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB3_16:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rbp
	leaq	16(%rbp), %rax
	cmpb	$0, 32(%rbp)
	je	.LBB3_16
# BB#17:
	movq	%rbp, %rdi
	callq	LoadScopeSnapshot
	movq	FilterOutSym(%rip), %rax
	movq	56(%rax), %rdi
	addq	$64, %rdi
	movl	$.L.str.15, %esi
	movl	$10, %ecx
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	callq	DefineFile
	movzwl	%ax, %edi
	xorl	%esi, %esi
	movl	$10, %edx
	movl	$1, %ecx
	xorl	%r8d, %r8d
	callq	LexPush
	movq	FilterOutSym(%rip), %r9
	movl	$104, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r13, %rsi
	callq	NewToken
	movq	%rax, 8(%rsp)
	leaq	8(%rsp), %rdi
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	Parse
	movq	%rax, %rbx
	callq	LexPop
	movq	%rbp, %rdi
	callq	ClearScopeSnapshot
	movq	FilterOutSym(%rip), %rax
	movq	56(%rax), %rdi
	addq	$64, %rdi
	callq	remove
	movq	filter_out_filename(%rip), %rax
	movq	FilterOutSym(%rip), %rcx
	movq	%rax, 56(%rcx)
.LBB3_18:
	movq	%rbx, %rax
	addq	$2072, %rsp             # imm = 0x818
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	FilterExecute, .Lfunc_end3-FilterExecute
	.cfi_endproc

	.globl	FilterWrite
	.p2align	4, 0x90
	.type	FilterWrite,@function
FilterWrite:                            # @FilterWrite
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 64
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpb	$57, 32(%r12)
	je	.LBB4_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.7, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.19, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB4_2:
	movq	8(%r12), %r14
	addq	$32, %r12
	.p2align	4, 0x90
.LBB4_3:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %r14
	cmpb	$0, 32(%r14)
	je	.LBB4_3
# BB#4:
	leaq	64(%r14), %r13
	movl	$.L.str.16, %esi
	movq	%r13, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB4_6
# BB#5:
	movl	$40, %edi
	movl	$5, %esi
	movl	$.L.str.20, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r12, %r8
	movq	%r13, %r9
	callq	Error
.LBB4_6:
	testl	$4190208, 40(%r14)      # imm = 0x3FF000
	je	.LBB4_12
# BB#7:
	movl	$.L.str.21, %edi
	movl	$6, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movl	$10, %edi
	movq	%r15, %rsi
	callq	fputc
	movq	%rbp, %r12
.LBB4_8:                                # %.sink.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_9 Depth 2
	incl	(%r12)
	.p2align	4, 0x90
.LBB4_9:                                #   Parent Loop BB4_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	je	.LBB4_11
# BB#10:                                #   in Loop: Header=BB4_9 Depth=2
	movl	%ebp, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	cmpl	$10, %ebp
	jne	.LBB4_9
	jmp	.LBB4_8
.LBB4_11:
	movl	$.L.str.23, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movl	$32, %edi
	movq	%r15, %rsi
	callq	fputc
	movq	48(%r14), %rdi
	callq	SymName
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	fputs
	jmp	.LBB4_17
.LBB4_12:
	movl	$123, %edi
	movq	%r15, %rsi
	callq	fputc
	movl	$10, %edi
	movq	%r15, %rsi
	callq	fputc
	movq	%rbp, %r12
.LBB4_13:                               # %.sink.split3
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_14 Depth 2
	incl	(%r12)
	.p2align	4, 0x90
.LBB4_14:                               #   Parent Loop BB4_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	je	.LBB4_16
# BB#15:                                #   in Loop: Header=BB4_14 Depth=2
	movl	%ebp, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	cmpl	$10, %ebp
	jne	.LBB4_14
	jmp	.LBB4_13
.LBB4_16:
	movl	$125, %edi
	movq	%r15, %rsi
	callq	fputc
.LBB4_17:
	movl	$10, %edi
	movq	%r15, %rsi
	callq	fputc
	incl	(%r12)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fclose                  # TAILCALL
.Lfunc_end4:
	.size	FilterWrite, .Lfunc_end4-FilterWrite
	.cfi_endproc

	.globl	FilterScavenge
	.p2align	4, 0x90
	.type	FilterScavenge,@function
FilterScavenge:                         # @FilterScavenge
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi47:
	.cfi_def_cfa_offset 32
.Lcfi48:
	.cfi_offset %rbx, -24
.Lcfi49:
	.cfi_offset %r14, -16
	movq	filter_active(%rip), %rax
	movq	8(%rax), %r14
	cmpq	%rax, %r14
	je	.LBB5_23
# BB#1:                                 # %.lr.ph
	testl	%edi, %edi
	je	.LBB5_2
	.p2align	4, 0x90
.LBB5_13:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_14 Depth 2
	movq	%r14, %rbx
	leaq	16(%rbx), %rax
	.p2align	4, 0x90
.LBB5_14:                               #   Parent Loop BB5_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB5_14
# BB#15:                                #   in Loop: Header=BB5_13 Depth=1
	movq	8(%rbx), %r14
	addq	$64, %rdi
	callq	remove
	movq	%rbx, xx_link(%rip)
	movq	%rbx, zz_hold(%rip)
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB5_16
# BB#17:                                #   in Loop: Header=BB5_13 Depth=1
	movq	%rax, zz_res(%rip)
	movq	16(%rbx), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%rbx), %rcx
	movq	%rax, 24(%rcx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	jmp	.LBB5_18
	.p2align	4, 0x90
.LBB5_16:                               #   in Loop: Header=BB5_13 Depth=1
	xorl	%eax, %eax
.LBB5_18:                               #   in Loop: Header=BB5_13 Depth=1
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_hold(%rip)
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB5_20
# BB#19:                                #   in Loop: Header=BB5_13 Depth=1
	movq	%rax, zz_res(%rip)
	movq	(%rbx), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rbx
.LBB5_20:                               #   in Loop: Header=BB5_13 Depth=1
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbx), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB5_22
# BB#21:                                #   in Loop: Header=BB5_13 Depth=1
	callq	DisposeObject
.LBB5_22:                               # %.backedge
                                        #   in Loop: Header=BB5_13 Depth=1
	cmpq	filter_active(%rip), %r14
	jne	.LBB5_13
	jmp	.LBB5_23
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_3 Depth 2
	movq	%r14, %rbx
	leaq	16(%rbx), %rax
	.p2align	4, 0x90
.LBB5_3:                                #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB5_3
# BB#4:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	8(%rbx), %r14
	movq	24(%rdi), %rax
	cmpq	16(%rdi), %rax
	jne	.LBB5_12
# BB#5:                                 #   in Loop: Header=BB5_2 Depth=1
	addq	$64, %rdi
	callq	remove
	movq	%rbx, xx_link(%rip)
	movq	%rbx, zz_hold(%rip)
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB5_6
# BB#7:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	%rax, zz_res(%rip)
	movq	16(%rbx), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%rbx), %rcx
	movq	%rax, 24(%rcx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	jmp	.LBB5_8
.LBB5_6:                                #   in Loop: Header=BB5_2 Depth=1
	xorl	%eax, %eax
.LBB5_8:                                #   in Loop: Header=BB5_2 Depth=1
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_hold(%rip)
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB5_10
# BB#9:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	%rax, zz_res(%rip)
	movq	(%rbx), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rbx
.LBB5_10:                               #   in Loop: Header=BB5_2 Depth=1
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbx), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB5_12
# BB#11:                                #   in Loop: Header=BB5_2 Depth=1
	callq	DisposeObject
	.p2align	4, 0x90
.LBB5_12:                               # %.backedge.us
                                        #   in Loop: Header=BB5_2 Depth=1
	cmpq	filter_active(%rip), %r14
	jne	.LBB5_2
.LBB5_23:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	FilterScavenge, .Lfunc_end5-FilterScavenge
	.cfi_endproc

	.type	filter_count,@object    # @filter_count
	.local	filter_count
	.comm	filter_count,4,4
	.type	filter_active,@object   # @filter_active
	.local	filter_active
	.comm	filter_active,8,8
	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"louti"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"lout"
	.size	.L.str.2, 5

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"lout.err"
	.size	.L.str.3, 9

	.type	filter_in_filename,@object # @filter_in_filename
	.local	filter_in_filename
	.comm	filter_in_filename,8,8
	.type	filter_out_filename,@object # @filter_out_filename
	.local	filter_out_filename
	.comm	filter_out_filename,8,8
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%s%d"
	.size	.L.str.4, 5

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"w"
	.size	.L.str.5, 2

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"cannot open temporary filter file %s"
	.size	.L.str.6, 37

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"assert failed in %s"
	.size	.L.str.7, 20

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"FilterSetFileNames: type(x)!"
	.size	.L.str.8, 29

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"FilterSetFileNames: x has no children!"
	.size	.L.str.9, 39

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"FilterSetFileNames: type(y)!"
	.size	.L.str.10, 29

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"FilterSetFileNames: type(y) (2)!"
	.size	.L.str.11, 33

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"FilterExecute: type(x)!"
	.size	.L.str.12, 24

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"FilterExecute: type(env)!"
	.size	.L.str.13, 26

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"safe execution prohibiting command: %s"
	.size	.L.str.14, 39

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.zero	1
	.size	.L.str.15, 1

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"r"
	.size	.L.str.16, 2

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"%s"
	.size	.L.str.17, 3

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"failure (non-zero status) of filter: %s"
	.size	.L.str.18, 40

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"FilterWrite: type(x)!"
	.size	.L.str.19, 22

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"cannot read filter temporary file %s"
	.size	.L.str.20, 37

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"@Begin"
	.size	.L.str.21, 7

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"@End"
	.size	.L.str.23, 5


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
