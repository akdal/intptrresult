	.text
	.file	"BitlDecoder.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	15                      # 0xf
.LCPI0_1:
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
.LCPI0_2:
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
.LCPI0_3:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI0_4:
	.long	170                     # 0xaa
	.long	170                     # 0xaa
	.long	170                     # 0xaa
	.long	170                     # 0xaa
.LCPI0_5:
	.long	85                      # 0x55
	.long	85                      # 0x55
	.long	85                      # 0x55
	.long	85                      # 0x55
.LCPI0_6:
	.long	204                     # 0xcc
	.long	204                     # 0xcc
	.long	204                     # 0xcc
	.long	204                     # 0xcc
.LCPI0_7:
	.long	51                      # 0x33
	.long	51                      # 0x33
	.long	51                      # 0x33
	.long	51                      # 0x33
.LCPI0_8:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
.LCPI0_9:
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_BitlDecoder.ii,@function
_GLOBAL__sub_I_BitlDecoder.ii:          # @_GLOBAL__sub_I_BitlDecoder.ii
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	movdqa	.LCPI0_0(%rip), %xmm14  # xmm14 = [12,13,14,15]
	movdqa	.LCPI0_1(%rip), %xmm7   # xmm7 = [8,9,10,11]
	movdqa	.LCPI0_2(%rip), %xmm5   # xmm5 = [4,5,6,7]
	movdqa	.LCPI0_3(%rip), %xmm6   # xmm6 = [0,1,2,3]
	movq	$-256, %rax
	movdqa	.LCPI0_4(%rip), %xmm8   # xmm8 = [170,170,170,170]
	movdqa	.LCPI0_5(%rip), %xmm9   # xmm9 = [85,85,85,85]
	movdqa	.LCPI0_6(%rip), %xmm10  # xmm10 = [204,204,204,204]
	movdqa	.LCPI0_7(%rip), %xmm11  # xmm11 = [51,51,51,51]
	movdqa	.LCPI0_8(%rip), %xmm12  # xmm12 = [255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0]
	movdqa	.LCPI0_9(%rip), %xmm13  # xmm13 = [16,16,16,16]
	.p2align	4, 0x90
.LBB0_1:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm6, %xmm2
	paddd	%xmm2, %xmm2
	movdqa	%xmm5, %xmm1
	paddd	%xmm1, %xmm1
	movdqa	%xmm7, %xmm0
	paddd	%xmm0, %xmm0
	movdqa	%xmm14, %xmm4
	paddd	%xmm4, %xmm4
	pand	%xmm8, %xmm4
	pand	%xmm8, %xmm0
	pand	%xmm8, %xmm1
	pand	%xmm8, %xmm2
	movdqa	%xmm6, %xmm3
	psrld	$1, %xmm3
	pand	%xmm9, %xmm3
	por	%xmm2, %xmm3
	movdqa	%xmm5, %xmm2
	psrld	$1, %xmm2
	pand	%xmm9, %xmm2
	por	%xmm1, %xmm2
	movdqa	%xmm7, %xmm1
	psrld	$1, %xmm1
	pand	%xmm9, %xmm1
	por	%xmm0, %xmm1
	movdqa	%xmm14, %xmm0
	psrld	$1, %xmm0
	pand	%xmm9, %xmm0
	por	%xmm4, %xmm0
	movdqa	%xmm0, %xmm4
	pslld	$2, %xmm4
	pand	%xmm10, %xmm4
	psrld	$2, %xmm0
	pand	%xmm11, %xmm0
	por	%xmm4, %xmm0
	movdqa	%xmm1, %xmm4
	pslld	$2, %xmm4
	pand	%xmm10, %xmm4
	psrld	$2, %xmm1
	pand	%xmm11, %xmm1
	por	%xmm4, %xmm1
	movdqa	%xmm2, %xmm4
	pslld	$2, %xmm4
	pand	%xmm10, %xmm4
	psrld	$2, %xmm2
	pand	%xmm11, %xmm2
	por	%xmm4, %xmm2
	movdqa	%xmm3, %xmm4
	pslld	$2, %xmm4
	pand	%xmm10, %xmm4
	psrld	$2, %xmm3
	pand	%xmm11, %xmm3
	por	%xmm4, %xmm3
	movdqa	%xmm0, %xmm4
	pslld	$4, %xmm4
	psrld	$4, %xmm0
	por	%xmm4, %xmm0
	movdqa	%xmm1, %xmm4
	pslld	$4, %xmm4
	psrld	$4, %xmm1
	por	%xmm4, %xmm1
	movdqa	%xmm2, %xmm4
	pslld	$4, %xmm4
	psrld	$4, %xmm2
	por	%xmm4, %xmm2
	movdqa	%xmm3, %xmm4
	pslld	$4, %xmm4
	psrld	$4, %xmm3
	por	%xmm4, %xmm3
	pand	%xmm12, %xmm3
	pand	%xmm12, %xmm2
	packuswb	%xmm2, %xmm3
	pand	%xmm12, %xmm1
	pand	%xmm12, %xmm0
	packuswb	%xmm0, %xmm1
	packuswb	%xmm1, %xmm3
	movdqa	%xmm3, _ZN5NBitl12kInvertTableE+256(%rax)
	paddd	%xmm13, %xmm6
	paddd	%xmm13, %xmm5
	paddd	%xmm13, %xmm7
	paddd	%xmm13, %xmm14
	addq	$16, %rax
	jne	.LBB0_1
# BB#2:                                 # %__cxx_global_var_init.exit
	retq
.Lfunc_end0:
	.size	_GLOBAL__sub_I_BitlDecoder.ii, .Lfunc_end0-_GLOBAL__sub_I_BitlDecoder.ii
	.cfi_endproc

	.type	_ZN5NBitl12kInvertTableE,@object # @_ZN5NBitl12kInvertTableE
	.bss
	.globl	_ZN5NBitl12kInvertTableE
	.p2align	4
_ZN5NBitl12kInvertTableE:
	.zero	256
	.size	_ZN5NBitl12kInvertTableE, 256

	.type	_ZN5NBitl26g_InverterTableInitializerE,@object # @_ZN5NBitl26g_InverterTableInitializerE
	.globl	_ZN5NBitl26g_InverterTableInitializerE
_ZN5NBitl26g_InverterTableInitializerE:
	.zero	1
	.size	_ZN5NBitl26g_InverterTableInitializerE, 1

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_BitlDecoder.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
