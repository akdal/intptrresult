	.text
	.file	"pair.bc"
	.globl	set_pair
	.p2align	4, 0x90
	.type	set_pair,@function
set_pair:                               # @set_pair
	.cfi_startproc
# BB#0:
	movl	$1, %esi
	jmp	set_pair1               # TAILCALL
.Lfunc_end0:
	.size	set_pair, .Lfunc_end0-set_pair
	.cfi_endproc

	.globl	set_pair1
	.p2align	4, 0x90
	.type	set_pair1,@function
set_pair1:                              # @set_pair1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1080, %rsp             # imm = 0x438
.Lcfi6:
	.cfi_def_cfa_offset 1136
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	48(%rbx), %rbp
	movl	%esi, 16(%rsp)          # 4-byte Spill
	testl	%esi, %esi
	je	.LBB1_2
# BB#1:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	makeup_labels
.LBB1_2:
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movslq	cube+8(%rip), %rbx
	leaq	(,%rbx,4), %rdi
	callq	malloc
	movq	%rax, %r12
	testq	%rbx, %rbx
	jle	.LBB1_4
# BB#3:                                 # %.lr.ph259
	decl	%ebx
	leaq	4(,%rbx,4), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	memset
.LBB1_4:                                # %.preheader228
	movl	(%rbp), %eax
	testl	%eax, %eax
	jle	.LBB1_13
# BB#5:                                 # %.lr.ph254
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_6:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rcx
	movslq	(%rcx,%rbx,4), %rcx
	testq	%rcx, %rcx
	jle	.LBB1_11
# BB#7:                                 #   in Loop: Header=BB1_6 Depth=1
	movl	cube+8(%rip), %edx
	cmpl	%edx, %ecx
	jg	.LBB1_11
# BB#8:                                 #   in Loop: Header=BB1_6 Depth=1
	movq	16(%rbp), %rsi
	movslq	(%rsi,%rbx,4), %rsi
	testq	%rsi, %rsi
	jle	.LBB1_11
# BB#9:                                 #   in Loop: Header=BB1_6 Depth=1
	cmpl	%edx, %esi
	jg	.LBB1_11
# BB#10:                                #   in Loop: Header=BB1_6 Depth=1
	movl	$1, -4(%r12,%rcx,4)
	movl	$1, -4(%r12,%rsi,4)
	jmp	.LBB1_12
	.p2align	4, 0x90
.LBB1_11:                               #   in Loop: Header=BB1_6 Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	fatal
	movl	(%rbp), %eax
.LBB1_12:                               #   in Loop: Header=BB1_6 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB1_6
.LBB1_13:                               # %._crit_edge255
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rdi
	movq	%rbp, %rsi
	callq	pairvar
	movq	%rax, %rcx
	movl	cube+8(%rip), %eax
	testl	%eax, %eax
	movq	%rbp, (%rsp)            # 8-byte Spill
	jle	.LBB1_26
# BB#14:                                # %.lr.ph.i.preheader
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
                                        # implicit-def: %R14D
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_15:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%r12,%rbx,4)
	je	.LBB1_19
# BB#16:                                #   in Loop: Header=BB1_15 Depth=1
	testl	%ebp, %ebp
	je	.LBB1_18
# BB#17:                                #   in Loop: Header=BB1_15 Depth=1
	movq	cube+32(%rip), %rdx
	addl	(%rdx,%rbx,4), %r13d
	jmp	.LBB1_22
	.p2align	4, 0x90
.LBB1_19:                               #   in Loop: Header=BB1_15 Depth=1
	testl	%ebp, %ebp
	je	.LBB1_20
# BB#21:                                #   in Loop: Header=BB1_15 Depth=1
	movl	%r14d, %esi
	subl	%r15d, %esi
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%r13d, %edx
	callq	sf_delcol
	movq	%rax, %rcx
	addl	%r13d, %r15d
	movl	cube+8(%rip), %eax
	jmp	.LBB1_22
	.p2align	4, 0x90
.LBB1_18:                               #   in Loop: Header=BB1_15 Depth=1
	movq	cube+16(%rip), %rdx
	movl	(%rdx,%rbx,4), %r14d
	movq	cube+32(%rip), %rdx
	movl	(%rdx,%rbx,4), %r13d
	movl	$1, %ebp
	jmp	.LBB1_22
.LBB1_20:                               #   in Loop: Header=BB1_15 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_22:                               #   in Loop: Header=BB1_15 Depth=1
	incq	%rbx
	movslq	%eax, %rdx
	cmpq	%rdx, %rbx
	jl	.LBB1_15
# BB#23:                                # %._crit_edge.i
	testl	%ebp, %ebp
	je	.LBB1_25
# BB#24:
	subl	%r15d, %r14d
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%r14d, %esi
	movl	%r13d, %edx
	callq	sf_delcol
	movq	%rax, %rcx
.LBB1_25:                               # %delvar.exit
	movq	(%rsp), %rbp            # 8-byte Reload
.LBB1_26:                               # %delvar.exit
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rcx, (%rax)
	movq	16(%rax), %rdi
	movq	%rbp, %rsi
	callq	pairvar
	movq	%rax, %rcx
	movl	cube+8(%rip), %eax
	testl	%eax, %eax
	jle	.LBB1_39
# BB#27:                                # %.lr.ph.i197.preheader
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
                                        # implicit-def: %R14D
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_28:                               # %.lr.ph.i197
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%r12,%rbx,4)
	je	.LBB1_32
# BB#29:                                #   in Loop: Header=BB1_28 Depth=1
	testl	%ebp, %ebp
	je	.LBB1_31
# BB#30:                                #   in Loop: Header=BB1_28 Depth=1
	movq	cube+32(%rip), %rdx
	addl	(%rdx,%rbx,4), %r13d
	jmp	.LBB1_35
	.p2align	4, 0x90
.LBB1_32:                               #   in Loop: Header=BB1_28 Depth=1
	testl	%ebp, %ebp
	je	.LBB1_33
# BB#34:                                #   in Loop: Header=BB1_28 Depth=1
	movl	%r14d, %esi
	subl	%r15d, %esi
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%r13d, %edx
	callq	sf_delcol
	movq	%rax, %rcx
	addl	%r13d, %r15d
	movl	cube+8(%rip), %eax
	jmp	.LBB1_35
	.p2align	4, 0x90
.LBB1_31:                               #   in Loop: Header=BB1_28 Depth=1
	movq	cube+16(%rip), %rdx
	movl	(%rdx,%rbx,4), %r14d
	movq	cube+32(%rip), %rdx
	movl	(%rdx,%rbx,4), %r13d
	movl	$1, %ebp
	jmp	.LBB1_35
.LBB1_33:                               #   in Loop: Header=BB1_28 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_35:                               #   in Loop: Header=BB1_28 Depth=1
	incq	%rbx
	movslq	%eax, %rdx
	cmpq	%rdx, %rbx
	jl	.LBB1_28
# BB#36:                                # %._crit_edge.i205
	testl	%ebp, %ebp
	je	.LBB1_38
# BB#37:
	subl	%r15d, %r14d
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%r14d, %esi
	movl	%r13d, %edx
	callq	sf_delcol
	movq	%rax, %rcx
.LBB1_38:                               # %delvar.exit207
	movq	(%rsp), %rbp            # 8-byte Reload
.LBB1_39:                               # %delvar.exit207
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rcx, 16(%rax)
	movq	8(%rax), %rdi
	movq	%rbp, %rsi
	callq	pairvar
	movq	%rax, %rdx
	movl	cube+8(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB1_52
# BB#40:                                # %.lr.ph.i214.preheader
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
                                        # implicit-def: %R14D
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_41:                               # %.lr.ph.i214
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%r12,%rbx,4)
	je	.LBB1_45
# BB#42:                                #   in Loop: Header=BB1_41 Depth=1
	testl	%ebp, %ebp
	je	.LBB1_44
# BB#43:                                #   in Loop: Header=BB1_41 Depth=1
	movq	cube+32(%rip), %rax
	addl	(%rax,%rbx,4), %r13d
	jmp	.LBB1_48
	.p2align	4, 0x90
.LBB1_45:                               #   in Loop: Header=BB1_41 Depth=1
	testl	%ebp, %ebp
	je	.LBB1_46
# BB#47:                                #   in Loop: Header=BB1_41 Depth=1
	movl	%r14d, %esi
	subl	%r15d, %esi
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%rdx, %rdi
	movl	%r13d, %edx
	callq	sf_delcol
	movq	%rax, %rdx
	addl	%r13d, %r15d
	movl	cube+8(%rip), %ecx
	jmp	.LBB1_48
	.p2align	4, 0x90
.LBB1_44:                               #   in Loop: Header=BB1_41 Depth=1
	movq	cube+16(%rip), %rax
	movl	(%rax,%rbx,4), %r14d
	movq	cube+32(%rip), %rax
	movl	(%rax,%rbx,4), %r13d
	movl	$1, %ebp
	jmp	.LBB1_48
.LBB1_46:                               #   in Loop: Header=BB1_41 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_48:                               #   in Loop: Header=BB1_41 Depth=1
	incq	%rbx
	movslq	%ecx, %rax
	cmpq	%rax, %rbx
	jl	.LBB1_41
# BB#49:                                # %._crit_edge.i222
	testl	%ebp, %ebp
	je	.LBB1_51
# BB#50:
	subl	%r15d, %r14d
	xorl	%eax, %eax
	movq	%rdx, %rdi
	movl	%r14d, %esi
	movl	%r13d, %edx
	callq	sf_delcol
	movq	%rax, %rdx
	movl	cube+8(%rip), %ecx
.LBB1_51:                               # %delvar.exit224
	movq	(%rsp), %rbp            # 8-byte Reload
.LBB1_52:                               # %delvar.exit224
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rdx, 8(%rax)
	movslq	cube(%rip), %rsi
	movl	cube+4(%rip), %r15d
	movq	cube+16(%rip), %rax
	movslq	%ecx, %r14
	movslq	(%rax,%r14,4), %rax
	testl	%r14d, %r14d
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movq	%rax, 48(%rsp)          # 8-byte Spill
	jle	.LBB1_53
# BB#54:                                # %.lr.ph251.preheader
	movl	%ecx, %eax
	cmpl	$8, %ecx
	jb	.LBB1_55
# BB#56:                                # %min.iters.checked
	movl	%ecx, %edx
	andl	$7, %edx
	movq	%rax, %r8
	subq	%rdx, %r8
	je	.LBB1_55
# BB#57:                                # %vector.body.preheader
	leaq	16(%r12), %rsi
	pxor	%xmm1, %xmm1
	movq	%r8, %rdi
	pxor	%xmm2, %xmm2
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB1_58:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rsi), %xmm3
	movdqu	(%rsi), %xmm4
	pcmpeqd	%xmm1, %xmm3
	psrld	$31, %xmm3
	pcmpeqd	%xmm1, %xmm4
	psrld	$31, %xmm4
	paddd	%xmm3, %xmm2
	paddd	%xmm4, %xmm0
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB1_58
# BB#59:                                # %middle.block
	paddd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %ebx
	testl	%edx, %edx
	jne	.LBB1_60
	jmp	.LBB1_62
.LBB1_55:
	xorl	%r8d, %r8d
	xorl	%ebx, %ebx
.LBB1_60:                               # %.lr.ph251.preheader302
	leaq	(%r12,%r8,4), %rdx
	subq	%r8, %rax
	.p2align	4, 0x90
.LBB1_61:                               # %.lr.ph251
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$1, (%rdx)
	adcl	$0, %ebx
	addq	$4, %rdx
	decq	%rax
	jne	.LBB1_61
	jmp	.LBB1_62
.LBB1_53:
	xorl	%ebx, %ebx
.LBB1_62:                               # %._crit_edge252
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	subl	%ecx, %r15d
	leal	(%rbx,%r15), %r13d
	addl	(%rbp), %r13d
	movslq	%r13d, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %r12
	movl	(%rbp), %eax
	testl	%eax, %eax
	jle	.LBB1_65
# BB#63:                                # %.lr.ph248.preheader
	movslq	%ebx, %rax
	leaq	(%r12,%rax,4), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_64:                               # %.lr.ph248
                                        # =>This Inner Loop Header: Depth=1
	movl	$4, (%rcx,%rdx,4)
	incq	%rdx
	movslq	(%rbp), %rax
	cmpq	%rax, %rdx
	jl	.LBB1_64
.LBB1_65:                               # %.preheader227
	testl	%r15d, %r15d
	jle	.LBB1_73
# BB#66:                                # %.lr.ph244
	movq	cube+32(%rip), %rdx
	movl	(%rdx,%r14,4), %ecx
	addl	%ebx, %eax
	cltq
	movl	%ecx, (%r12,%rax,4)
	cmpl	$1, %r15d
	je	.LBB1_73
# BB#67:                                # %._crit_edge286.preheader
	movl	%r15d, %r8d
	testb	$1, %r8b
	jne	.LBB1_68
# BB#69:                                # %._crit_edge286.prol
	movl	(%rbp), %ecx
	movl	4(%rdx,%r14,4), %esi
	leal	1(%rbx,%rcx), %ecx
	movslq	%ecx, %rcx
	movl	%esi, (%r12,%rcx,4)
	movl	$2, %ecx
	cmpl	$2, %r15d
	jne	.LBB1_71
	jmp	.LBB1_73
.LBB1_68:
	movl	$1, %ecx
	cmpl	$2, %r15d
	je	.LBB1_73
.LBB1_71:                               # %._crit_edge286.preheader.new
	movl	%ebx, %esi
	leaq	4(%rdx,%r14,4), %rdx
	.p2align	4, 0x90
.LBB1_72:                               # %._crit_edge286
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rdx,%rcx,4), %edi
	leal	(%rsi,%rcx), %eax
	movq	(%rsp), %rbp            # 8-byte Reload
	movl	(%rbp), %ebp
	addl	%eax, %ebp
	movslq	%ebp, %rbp
	movl	%edi, (%r12,%rbp,4)
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	(%rdi), %edi
	movl	(%rdx,%rcx,4), %ebp
	leal	1(%rdi,%rax), %eax
	cltq
	movl	%ebp, (%r12,%rax,4)
	addq	$2, %rcx
	cmpq	%rcx, %r8
	jne	.LBB1_72
.LBB1_73:                               # %._crit_edge245
	xorl	%eax, %eax
	callq	setdown_cube
	movq	cube+32(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_75
# BB#74:
	callq	free
	movq	$0, cube+32(%rip)
.LBB1_75:
	movl	%r13d, cube+4(%rip)
	movl	%ebx, cube+8(%rip)
	movq	%r12, cube+32(%rip)
	xorl	%eax, %eax
	callq	cube_setup
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	je	.LBB1_98
# BB#76:
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	56(%r15), %r12
	movslq	cube(%rip), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 56(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.LBB1_80
# BB#77:                                # %.lr.ph242
	movq	%r12, 40(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	xorl	%ecx, %ecx
	movq	8(%rsp), %r14           # 8-byte Reload
	.p2align	4, 0x90
.LBB1_78:                               # =>This Inner Loop Header: Depth=1
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	cube+8(%rip), %ebp
	addl	%ebp, %ebp
	addq	%r12, %rbp
	movq	(%rsp), %rax            # 8-byte Reload
	movq	8(%rax), %rax
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	16(%rcx), %rcx
	movl	(%rax,%r12), %eax
	leal	-2(%rax,%rax), %edx
	leal	-1(%rax,%rax), %eax
	cltq
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	(%rsi,%rax,8), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	(%rcx,%r12), %eax
	leal	-2(%rax,%rax), %ecx
	leal	-1(%rax,%rax), %eax
	cltq
	movq	(%rsi,%rax,8), %rbx
	movslq	%edx, %rax
	movq	(%rsi,%rax,8), %r13
	movslq	%ecx, %rax
	movq	(%rsi,%rax,8), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	leaq	80(%rsp), %r15
	movq	%r15, %rdi
	movq	%r13, %rdx
	callq	sprintf
	movq	%r15, %rdi
	callq	util_strsav
	movq	56(%r14), %rcx
	movslq	%ebp, %rbp
	movq	%rax, (%rcx,%rbp,8)
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r13, %rdx
	movq	%rbx, %r13
	movq	%r13, %rcx
	callq	sprintf
	movq	%r15, %rdi
	callq	util_strsav
	movq	56(%r14), %rcx
	leal	1(%rbp), %edx
	movslq	%edx, %rdx
	movq	%rax, (%rcx,%rdx,8)
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	movq	64(%rsp), %rcx          # 8-byte Reload
	callq	sprintf
	movq	%r15, %rdi
	callq	util_strsav
	movq	56(%r14), %rcx
	leal	2(%rbp), %edx
	movslq	%edx, %rdx
	movq	%rax, (%rcx,%rdx,8)
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %rdx
	movq	%r13, %rcx
	callq	sprintf
	movq	%r15, %rdi
	callq	util_strsav
	movq	56(%r14), %rcx
	addl	$3, %ebp
	movslq	%ebp, %rdx
	movq	%rax, (%rcx,%rdx,8)
	movq	16(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	movq	(%rsp), %rax            # 8-byte Reload
	movslq	(%rax), %rax
	addq	$4, %r12
	cmpq	%rax, %rcx
	jl	.LBB1_78
# BB#79:
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	40(%rsp), %r12          # 8-byte Reload
.LBB1_80:                               # %.preheader226
	movl	28(%rsp), %ecx          # 4-byte Reload
	testl	%ecx, %ecx
	jle	.LBB1_85
# BB#81:                                # %.lr.ph238.preheader
	movl	%ecx, %ecx
	xorl	%edx, %edx
	movabsq	$8589934592, %r8        # imm = 0x200000000
	xorl	%edi, %edi
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB1_82:                               # %.lr.ph238
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%r14,%rdi,2)
	jne	.LBB1_84
# BB#83:                                #   in Loop: Header=BB1_82 Depth=1
	movq	%rdx, %rbx
	sarq	$29, %rbx
	movq	(%r12,%rbx), %r10
	movq	56(%r15), %rbp
	leal	(%r9,%r9), %esi
	movslq	%esi, %rsi
	movq	%r10, (%rbp,%rsi,8)
	leal	1(%rdi), %esi
	movslq	%esi, %rsi
	movq	(%r12,%rsi,8), %r10
	movq	56(%r15), %r11
	leal	1(%r9,%r9), %ebp
	movslq	%ebp, %rbp
	movq	%r10, (%r11,%rbp,8)
	movq	$0, (%r12,%rsi,8)
	movq	$0, (%r12,%rbx)
	incl	%r9d
.LBB1_84:                               #   in Loop: Header=BB1_82 Depth=1
	addq	$2, %rdi
	addq	%r8, %rdx
	decq	%rcx
	jne	.LBB1_82
.LBB1_85:                               # %._crit_edge239
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	cmpl	%ebx, %edi
	jge	.LBB1_91
# BB#86:                                # %.lr.ph235
	shll	$2, %eax
	movl	cube+8(%rip), %edx
	addl	%edx, %edx
	movl	%ebx, %ecx
	subl	%edi, %ecx
	leaq	-1(%rbx), %rsi
	testb	$1, %cl
	movq	%rdi, %rcx
	je	.LBB1_88
# BB#87:
	movq	(%r12,%rdi,8), %r8
	movq	56(%r15), %rcx
	leal	(%rdx,%rax), %ebp
	movslq	%ebp, %rbp
	movq	%r8, (%rcx,%rbp,8)
	movq	$0, (%r12,%rdi,8)
	leaq	1(%rdi), %rcx
.LBB1_88:                               # %.prol.loopexit
	cmpq	%rdi, %rsi
	je	.LBB1_91
# BB#89:                                # %.lr.ph235.new
	addl	%edx, %eax
	subl	%edi, %eax
	.p2align	4, 0x90
.LBB1_90:                               # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rcx,8), %rdx
	movq	56(%r15), %rsi
	leaq	(%rax,%rcx), %rdi
	movslq	%edi, %rdi
	movq	%rdx, (%rsi,%rdi,8)
	movq	$0, (%r12,%rcx,8)
	movq	8(%r12,%rcx,8), %rdx
	movq	56(%r15), %rsi
	incl	%edi
	movslq	%edi, %rdi
	movq	%rdx, (%rsi,%rdi,8)
	movq	$0, 8(%r12,%rcx,8)
	addq	$2, %rcx
	cmpq	%rcx, %rbx
	jne	.LBB1_90
.LBB1_91:                               # %.preheader225
	testl	%ebx, %ebx
	jle	.LBB1_96
# BB#92:                                # %.lr.ph231.preheader
	movl	%ebx, %ebx
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB1_93:                               # %.lr.ph231
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_95
# BB#94:                                #   in Loop: Header=BB1_93 Depth=1
	callq	free
	movq	$0, (%rbp)
.LBB1_95:                               #   in Loop: Header=BB1_93 Depth=1
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB1_93
	jmp	.LBB1_97
.LBB1_96:                               # %._crit_edge232
	testq	%r12, %r12
	je	.LBB1_98
.LBB1_97:                               # %._crit_edge232.thread
	movq	%r12, %rdi
	callq	free
.LBB1_98:                               # %.preheader
	movq	(%rsp), %rsi            # 8-byte Reload
	cmpl	$0, (%rsi)
	jle	.LBB1_101
# BB#99:                                # %.lr.ph
	xorl	%eax, %eax
	movq	cube+112(%rip), %rcx
	.p2align	4, 0x90
.LBB1_100:                              # =>This Inner Loop Header: Depth=1
	movl	cube+8(%rip), %edx
	addl	%eax, %edx
	movslq	%edx, %rdx
	movl	$0, (%rcx,%rdx,4)
	incl	%eax
	cmpl	(%rsi), %eax
	jl	.LBB1_100
.LBB1_101:                              # %._crit_edge
	testq	%r14, %r14
	je	.LBB1_103
# BB#102:
	movq	%r14, %rdi
	callq	free
.LBB1_103:
	addq	$1080, %rsp             # imm = 0x438
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	set_pair1, .Lfunc_end1-set_pair1
	.cfi_endproc

	.globl	pairvar
	.p2align	4, 0x90
	.type	pairvar,@function
pairvar:                                # @pairvar
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	cube+16(%rip), %rax
	movslq	cube+4(%rip), %rcx
	movl	-4(%rax,%rcx,4), %esi
	movl	(%r15), %edx
	shll	$2, %edx
	negl	%edx
	xorl	%eax, %eax
	movl	%esi, 4(%rsp)           # 4-byte Spill
	callq	sf_delcol
	movslq	(%rax), %rdx
	movslq	12(%rax), %rcx
	imulq	%rdx, %rcx
	testl	%ecx, %ecx
	jle	.LBB2_16
# BB#1:                                 # %.preheader.lr.ph
	movq	24(%rax), %r12
	leaq	(%r12,%rcx,4), %r8
	movq	cube+16(%rip), %r11
	movl	(%r15), %ecx
	.p2align	4, 0x90
.LBB2_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_4 Depth 2
	testl	%ecx, %ecx
	jle	.LBB2_15
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	8(%r15), %r9
	movq	16(%r15), %r10
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, %r13d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_4:                                #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%r9,%rbx,4), %rcx
	movl	-4(%r11,%rcx,4), %edi
	movslq	(%r10,%rbx,4), %rcx
	movl	-4(%r11,%rcx,4), %esi
	leal	1(%rsi), %ecx
	movl	%ecx, %edx
	sarl	$5, %edx
	movslq	%edx, %rdx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	andl	4(%r12,%rdx,4), %ebp
	movl	%esi, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %r14
	movl	$1, %edx
	movl	%esi, %ecx
	shll	%cl, %edx
	andl	4(%r12,%r14,4), %edx
	movl	%edi, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movl	4(%r12,%rcx,4), %ecx
	btl	%edi, %ecx
	jae	.LBB2_9
# BB#5:                                 #   in Loop: Header=BB2_4 Depth=2
	testl	%edx, %edx
	je	.LBB2_7
# BB#6:                                 #   in Loop: Header=BB2_4 Depth=2
	leal	3(%r13), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%esi, 4(%r12,%rcx,4)
.LBB2_7:                                #   in Loop: Header=BB2_4 Depth=2
	testl	%ebp, %ebp
	je	.LBB2_9
# BB#8:                                 #   in Loop: Header=BB2_4 Depth=2
	leal	2(%r13), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%esi, 4(%r12,%rcx,4)
.LBB2_9:                                #   in Loop: Header=BB2_4 Depth=2
	incl	%edi
	movl	%edi, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movl	4(%r12,%rcx,4), %ecx
	btl	%edi, %ecx
	jae	.LBB2_14
# BB#10:                                #   in Loop: Header=BB2_4 Depth=2
	testl	%edx, %edx
	je	.LBB2_12
# BB#11:                                #   in Loop: Header=BB2_4 Depth=2
	leal	1(%r13), %ecx
	movl	$1, %edx
	shll	%cl, %edx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%edx, 4(%r12,%rcx,4)
.LBB2_12:                               #   in Loop: Header=BB2_4 Depth=2
	testl	%ebp, %ebp
	je	.LBB2_14
# BB#13:                                #   in Loop: Header=BB2_4 Depth=2
	movl	$1, %edx
	movl	%r13d, %ecx
	shll	%cl, %edx
	movl	%r13d, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%edx, 4(%r12,%rcx,4)
.LBB2_14:                               #   in Loop: Header=BB2_4 Depth=2
	incq	%rbx
	movslq	(%r15), %rcx
	addl	$4, %r13d
	cmpq	%rcx, %rbx
	jl	.LBB2_4
.LBB2_15:                               # %._crit_edge
                                        #   in Loop: Header=BB2_2 Depth=1
	movslq	(%rax), %rdx
	leaq	(%r12,%rdx,4), %r12
	cmpq	%r8, %r12
	jb	.LBB2_2
.LBB2_16:                               # %._crit_edge68
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	pairvar, .Lfunc_end2-pairvar
	.cfi_endproc

	.globl	delvar
	.p2align	4, 0x90
	.type	delvar,@function
delvar:                                 # @delvar
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 64
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movl	cube+8(%rip), %eax
	testl	%eax, %eax
	jle	.LBB3_11
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
                                        # implicit-def: %R14D
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%r12,%rbx,4)
	je	.LBB3_6
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	testl	%ebp, %ebp
	je	.LBB3_5
# BB#4:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	cube+32(%rip), %rcx
	addl	(%rcx,%rbx,4), %r13d
	jmp	.LBB3_9
	.p2align	4, 0x90
.LBB3_6:                                #   in Loop: Header=BB3_2 Depth=1
	testl	%ebp, %ebp
	je	.LBB3_7
# BB#8:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	%r14d, %esi
	subl	%r15d, %esi
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movl	%r13d, %edx
	callq	sf_delcol
	movq	%rax, %rdi
	addl	%r13d, %r15d
	movl	cube+8(%rip), %eax
	jmp	.LBB3_9
	.p2align	4, 0x90
.LBB3_5:                                #   in Loop: Header=BB3_2 Depth=1
	movq	cube+16(%rip), %rcx
	movl	(%rcx,%rbx,4), %r14d
	movq	cube+32(%rip), %rcx
	movl	(%rcx,%rbx,4), %r13d
	movl	$1, %ebp
	jmp	.LBB3_9
.LBB3_7:                                #   in Loop: Header=BB3_2 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_9:                                #   in Loop: Header=BB3_2 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB3_2
# BB#10:                                # %._crit_edge
	testl	%ebp, %ebp
	je	.LBB3_11
# BB#12:
	subl	%r15d, %r14d
	xorl	%eax, %eax
	movl	%r14d, %esi
	movl	%r13d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	sf_delcol               # TAILCALL
.LBB3_11:                               # %._crit_edge.thread
	movq	%rdi, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	delvar, .Lfunc_end3-delvar
	.cfi_endproc

	.globl	find_optimal_pairing
	.p2align	4, 0x90
	.type	find_optimal_pairing,@function
find_optimal_pairing:                   # @find_optimal_pairing
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 48
.Lcfi44:
	.cfi_offset %rbx, -48
.Lcfi45:
	.cfi_offset %r12, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	find_pairing_cost
	movq	%rax, %r12
	cmpl	$0, summary(%rip)
	je	.LBB4_10
# BB#1:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, cube+8(%rip)
	jle	.LBB4_4
# BB#2:                                 # %.lr.ph51.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph51
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
	cmpl	cube+8(%rip), %ebp
	jl	.LBB4_3
.LBB4_4:                                # %._crit_edge52
	movl	$10, %edi
	callq	putchar
	cmpl	$0, cube+8(%rip)
	jle	.LBB4_12
# BB#5:                                 # %.lr.ph48.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_6:                                # %.lr.ph48
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_8 Depth 2
	leaq	1(%rbp), %r15
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	printf
	cmpl	$0, cube+8(%rip)
	jle	.LBB4_9
# BB#7:                                 # %.lr.ph44
                                        #   in Loop: Header=BB4_6 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_8:                                #   Parent Loop BB4_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%rbp,8), %rax
	movl	(%rax,%rbx,4), %esi
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	movslq	cube+8(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB4_8
.LBB4_9:                                # %._crit_edge45
                                        #   in Loop: Header=BB4_6 Depth=1
	movl	$10, %edi
	callq	putchar
	movslq	cube+8(%rip), %rax
	cmpq	%rax, %r15
	movq	%r15, %rbp
	jl	.LBB4_6
	jmp	.LBB4_11
.LBB4_10:                               # %thread-pre-split
	movl	cube+8(%rip), %eax
.LBB4_11:                               # %.loopexit
	cmpl	$14, %eax
	jg	.LBB4_13
.LBB4_12:                               # %.loopexit.thread
	movq	%r12, %rdi
	callq	pair_best_cost
	leaq	48(%r14), %r15
	movq	%rax, 48(%r14)
	jmp	.LBB4_14
.LBB4_13:
	leaq	48(%r14), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	greedy_best_cost
.LBB4_14:
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%r15), %r15
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, (%r15)
	jle	.LBB4_17
# BB#15:                                # %.lr.ph.i
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_16:                               # =>This Inner Loop Header: Depth=1
	movq	8(%r15), %rax
	movq	16(%r15), %rcx
	movl	(%rax,%rbp,4), %esi
	movl	(%rcx,%rbp,4), %edx
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbp
	movslq	(%r15), %rax
	cmpq	%rax, %rbp
	jl	.LBB4_16
.LBB4_17:                               # %print_pair.exit
	movl	$10, %edi
	callq	putchar
	movl	cube+8(%rip), %eax
	testl	%eax, %eax
	jle	.LBB4_22
# BB#18:                                # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_19:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbp,8), %rdi
	testq	%rdi, %rdi
	je	.LBB4_21
# BB#20:                                #   in Loop: Header=BB4_19 Depth=1
	callq	free
	movq	$0, (%r12,%rbp,8)
	movl	cube+8(%rip), %eax
.LBB4_21:                               #   in Loop: Header=BB4_19 Depth=1
	incq	%rbp
	movslq	%eax, %rcx
	cmpq	%rcx, %rbp
	jl	.LBB4_19
	jmp	.LBB4_23
.LBB4_22:                               # %._crit_edge
	testq	%r12, %r12
	je	.LBB4_24
.LBB4_23:                               # %._crit_edge.thread
	movq	%r12, %rdi
	callq	free
.LBB4_24:
	movl	$1, %esi
	movq	%r14, %rdi
	callq	set_pair1
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r15
	movq	(%r14), %rdi
	movq	8(%r14), %rsi
	movq	16(%r14), %rdx
	xorl	%eax, %eax
	callq	espresso
	movq	%rax, %rbx
	movq	%rbx, (%r14)
	cmpl	$0, summary(%rip)
	je	.LBB4_25
# BB#26:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r15, %rcx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	print_trace             # TAILCALL
.LBB4_25:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	find_optimal_pairing, .Lfunc_end4-find_optimal_pairing
	.cfi_endproc

	.globl	find_pairing_cost
	.p2align	4, 0x90
	.type	find_pairing_cost,@function
find_pairing_cost:                      # @find_pairing_cost
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi55:
	.cfi_def_cfa_offset 128
.Lcfi56:
	.cfi_offset %rbx, -56
.Lcfi57:
	.cfi_offset %r12, -48
.Lcfi58:
	.cfi_offset %r13, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movq	%rdi, %rbx
	movslq	cube+8(%rip), %rbp
	leaq	(,%rbp,8), %rdi
	callq	malloc
	movq	%rax, %r12
	testq	%rbp, %rbp
	jle	.LBB5_9
# BB#1:                                 # %.lr.ph173.preheader
	leaq	(,%rbp,4), %r14
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph173
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, (%r12,%r15,8)
	incq	%r15
	cmpq	%rbp, %r15
	jl	.LBB5_2
# BB#3:                                 # %.preheader136
	testl	%ebp, %ebp
	jle	.LBB5_9
# BB#4:                                 # %.preheader.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB5_5:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_7 Depth 2
	testl	%ebp, %ebp
	jle	.LBB5_8
# BB#6:                                 # %.lr.ph167
                                        #   in Loop: Header=BB5_5 Depth=1
	movq	(%r12,%rax,8), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_7:                                #   Parent Loop BB5_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$0, (%rcx,%rdx,4)
	incq	%rdx
	movslq	cube+8(%rip), %rbp
	cmpq	%rbp, %rdx
	jl	.LBB5_7
.LBB5_8:                                # %._crit_edge168
                                        #   in Loop: Header=BB5_5 Depth=1
	incq	%rax
	movslq	%ebp, %rcx
	cmpq	%rcx, %rax
	jl	.LBB5_5
.LBB5_9:                                # %._crit_edge170
	movl	$24, %edi
	callq	malloc
	movq	%rax, %r14
	movl	$4, %edi
	callq	malloc
	movq	%rax, 8(%r14)
	movl	$4, %edi
	callq	malloc
	movq	%rax, 16(%r14)
	movq	%r14, 48(%rbx)
	movl	$1, (%r14)
	cmpl	$2, %ebp
	jl	.LBB5_51
# BB#10:                                # %.lr.ph164
	movl	$1, %r13d
	xorl	%edx, %edx
                                        # implicit-def: %RAX
	movq	%rax, 40(%rsp)          # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 48(%rsp)          # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 8(%rsp)           # 8-byte Spill
                                        # implicit-def: %EAX
	movl	%eax, (%rsp)            # 4-byte Spill
                                        # implicit-def: %R14
                                        # implicit-def: %R15D
                                        # implicit-def: %EAX
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	%r12, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB5_12:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_14 Depth 2
                                        #       Child Loop BB5_26 Depth 3
                                        #       Child Loop BB5_29 Depth 3
                                        #       Child Loop BB5_31 Depth 3
	movq	%rdx, %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	incq	%rdx
	movslq	%ebp, %rax
	cmpq	%rax, %rdx
	jge	.LBB5_11
# BB#13:                                # %.lr.ph148
                                        #   in Loop: Header=BB5_12 Depth=1
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%r13, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB5_14:                               #   Parent Loop BB5_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_26 Depth 3
                                        #       Child Loop BB5_29 Depth 3
                                        #       Child Loop BB5_31 Depth 3
	movl	4(%rsp), %ebp           # 4-byte Reload
	testl	%ebp, %ebp
	jle	.LBB5_33
# BB#15:                                #   in Loop: Header=BB5_14 Depth=2
	movq	(%rbx), %rdi
	xorl	%eax, %eax
	callq	sf_save
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	8(%rbx), %rdi
	xorl	%eax, %eax
	callq	sf_save
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	16(%rbx), %rdi
	xorl	%eax, %eax
	callq	sf_save
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	cube+8(%rip), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movslq	cube+4(%rip), %r15
	leaq	(,%r15,4), %rdi
	callq	malloc
	movq	%rax, %r14
	testq	%r15, %r15
	jle	.LBB5_32
# BB#16:                                # %.lr.ph
                                        #   in Loop: Header=BB5_14 Depth=2
	movq	cube+32(%rip), %rax
	cmpl	$8, %r15d
	jae	.LBB5_18
# BB#17:                                #   in Loop: Header=BB5_14 Depth=2
	xorl	%ecx, %ecx
	jmp	.LBB5_31
.LBB5_18:                               # %min.iters.checked
                                        #   in Loop: Header=BB5_14 Depth=2
	movq	%r15, %rcx
	andq	$-8, %rcx
	je	.LBB5_19
# BB#20:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_14 Depth=2
	leaq	(%rax,%r15,4), %rdx
	cmpq	%rdx, %r14
	jae	.LBB5_23
# BB#21:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_14 Depth=2
	leaq	(%r14,%r15,4), %rdx
	cmpq	%rdx, %rax
	jae	.LBB5_23
# BB#22:                                #   in Loop: Header=BB5_14 Depth=2
	xorl	%ecx, %ecx
	jmp	.LBB5_31
.LBB5_19:                               #   in Loop: Header=BB5_14 Depth=2
	xorl	%ecx, %ecx
	jmp	.LBB5_31
.LBB5_23:                               # %vector.body.preheader
                                        #   in Loop: Header=BB5_14 Depth=2
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB5_24
# BB#25:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB5_14 Depth=2
	negq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_26:                               # %vector.body.prol
                                        #   Parent Loop BB5_12 Depth=1
                                        #     Parent Loop BB5_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	(%rax,%rdi,4), %xmm0
	movups	16(%rax,%rdi,4), %xmm1
	movups	%xmm0, (%r14,%rdi,4)
	movups	%xmm1, 16(%r14,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB5_26
	jmp	.LBB5_27
.LBB5_24:                               #   in Loop: Header=BB5_14 Depth=2
	xorl	%edi, %edi
.LBB5_27:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB5_14 Depth=2
	cmpq	$24, %rdx
	jb	.LBB5_30
# BB#28:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB5_14 Depth=2
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%r14,%rdi,4), %rsi
	leaq	112(%rax,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB5_29:                               # %vector.body
                                        #   Parent Loop BB5_12 Depth=1
                                        #     Parent Loop BB5_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB5_29
.LBB5_30:                               # %middle.block
                                        #   in Loop: Header=BB5_14 Depth=2
	cmpq	%rcx, %r15
	je	.LBB5_32
	.p2align	4, 0x90
.LBB5_31:                               # %scalar.ph
                                        #   Parent Loop BB5_12 Depth=1
                                        #     Parent Loop BB5_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rax,%rcx,4), %edx
	movl	%edx, (%r14,%rcx,4)
	incq	%rcx
	cmpq	%r15, %rcx
	jl	.LBB5_31
.LBB5_32:                               # %._crit_edge
                                        #   in Loop: Header=BB5_14 Depth=2
	movq	48(%rbx), %rax
	movq	8(%rax), %rcx
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	%edx, (%rcx)
	leal	1(%r13), %ecx
	movq	16(%rax), %rax
	movl	%ecx, (%rax)
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	set_pair1
.LBB5_33:                               #   in Loop: Header=BB5_14 Depth=2
	cmpl	$3, %ebp
	ja	.LBB5_44
# BB#34:                                #   in Loop: Header=BB5_14 Depth=2
	movl	%ebp, %eax
	jmpq	*.LJTI5_0(,%rax,8)
.LBB5_39:                               #   in Loop: Header=BB5_14 Depth=2
	movl	cube(%rip), %ebp
	movl	$2, %eax
	cmpl	$33, %ebp
	jl	.LBB5_41
# BB#40:                                #   in Loop: Header=BB5_14 Depth=2
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	addl	$2, %eax
.LBB5_41:                               #   in Loop: Header=BB5_14 Depth=2
	movslq	%eax, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, %r12
	movq	cube+72(%rip), %rax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rsi
	movq	(%rax,%r13,8), %rdx
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	set_or
	movq	(%rbx), %rdi
	xorl	%eax, %eax
	callq	sf_save
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r12, %rsi
	callq	dist_merge
	movq	%rax, %rcx
	movq	(%rbx), %rax
	movl	12(%rax), %eax
	subl	12(%rcx), %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sf_free
	testq	%r12, %r12
	je	.LBB5_43
# BB#42:                                #   in Loop: Header=BB5_14 Depth=2
	movq	%r12, %rdi
	callq	free
.LBB5_43:                               #   in Loop: Header=BB5_14 Depth=2
	movq	24(%rsp), %r12          # 8-byte Reload
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB5_44
	.p2align	4, 0x90
.LBB5_38:                               #   in Loop: Header=BB5_14 Depth=2
	movq	(%rbx), %rdi
	movq	8(%rbx), %rsi
	xorl	%eax, %eax
	callq	reduce
	movq	%rax, %rcx
	movq	%rcx, (%rbx)
	movq	16(%rbx), %rsi
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	expand
	movq	%rax, %rcx
	movq	%rcx, (%rbx)
	movq	8(%rbx), %rsi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	irredundant
	jmp	.LBB5_37
	.p2align	4, 0x90
.LBB5_36:                               #   in Loop: Header=BB5_14 Depth=2
	movq	(%rbx), %rdi
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	xorl	%eax, %eax
	callq	espresso
	jmp	.LBB5_37
	.p2align	4, 0x90
.LBB5_35:                               #   in Loop: Header=BB5_14 Depth=2
	movq	(%rbx), %rdi
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	minimize_exact
.LBB5_37:                               #   in Loop: Header=BB5_14 Depth=2
	movq	%rax, (%rbx)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	12(%rcx), %ecx
	subl	12(%rax), %ecx
	movl	%ecx, (%rsp)            # 4-byte Spill
.LBB5_44:                               #   in Loop: Header=BB5_14 Depth=2
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%r12,%rax,8), %rax
	movl	(%rsp), %ecx            # 4-byte Reload
	movl	%ecx, (%rax,%r13,4)
	testl	%ebp, %ebp
	jle	.LBB5_48
# BB#45:                                #   in Loop: Header=BB5_14 Depth=2
	xorl	%eax, %eax
	callq	setdown_cube
	movq	cube+32(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB5_47
# BB#46:                                #   in Loop: Header=BB5_14 Depth=2
	callq	free
	movq	$0, cube+32(%rip)
.LBB5_47:                               #   in Loop: Header=BB5_14 Depth=2
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, cube+8(%rip)
	movl	%r15d, cube+4(%rip)
	movq	%r14, cube+32(%rip)
	xorl	%eax, %eax
	callq	cube_setup
	movq	(%rbx), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	8(%rbx), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	16(%rbx), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, (%rbx)
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rbx)
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rbx)
.LBB5_48:                               #   in Loop: Header=BB5_14 Depth=2
	incq	%r13
	movslq	cube+8(%rip), %rbp
	cmpq	%rbp, %r13
	jl	.LBB5_14
# BB#49:                                #   in Loop: Header=BB5_12 Depth=1
	movq	64(%rsp), %r13          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
.LBB5_11:                               # %.loopexit
                                        #   in Loop: Header=BB5_12 Depth=1
	leal	-1(%rbp), %eax
	cltq
	incq	%r13
	cmpq	%rax, %rdx
	jl	.LBB5_12
# BB#50:                                # %._crit_edge165.loopexit
	movq	48(%rbx), %r14
.LBB5_51:                               # %._crit_edge165
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB5_53
# BB#52:
	callq	free
	movq	$0, 8(%r14)
.LBB5_53:
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB5_55
# BB#54:
	callq	free
.LBB5_55:                               # %pair_free.exit
	movq	%r14, %rdi
	callq	free
	movq	$0, 48(%rbx)
	movq	%r12, %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	find_pairing_cost, .Lfunc_end5-find_pairing_cost
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI5_0:
	.quad	.LBB5_39
	.quad	.LBB5_38
	.quad	.LBB5_36
	.quad	.LBB5_35

	.text
	.globl	print_pair
	.p2align	4, 0x90
	.type	print_pair,@function
print_pair:                             # @print_pair
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 32
.Lcfi65:
	.cfi_offset %rbx, -24
.Lcfi66:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, (%r14)
	jle	.LBB6_3
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	movq	8(%r14), %rax
	movq	16(%r14), %rcx
	movl	(%rax,%rbx,4), %esi
	movl	(%rcx,%rbx,4), %edx
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	movslq	(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB6_2
.LBB6_3:                                # %._crit_edge
	movl	$10, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	putchar                 # TAILCALL
.Lfunc_end6:
	.size	print_pair, .Lfunc_end6-print_pair
	.cfi_endproc

	.globl	greedy_best_cost
	.p2align	4, 0x90
	.type	greedy_best_cost,@function
greedy_best_cost:                       # @greedy_best_cost
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi70:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi71:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi73:
	.cfi_def_cfa_offset 80
.Lcfi74:
	.cfi_offset %rbx, -56
.Lcfi75:
	.cfi_offset %r12, -48
.Lcfi76:
	.cfi_offset %r13, -40
.Lcfi77:
	.cfi_offset %r14, -32
.Lcfi78:
	.cfi_offset %r15, -24
.Lcfi79:
	.cfi_offset %rbp, -16
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r13
	movslq	cube+8(%rip), %r14
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	$0, (%rbx)
	leaq	(,%r14,4), %rbp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, 8(%rbx)
	movq	%rbp, %rdi
	callq	malloc
	cmpq	$33, %r14
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	%rax, 16(%rbx)
	jge	.LBB7_2
# BB#1:
	movl	$8, %edi
	jmp	.LBB7_3
.LBB7_2:
	leal	-1(%r14), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB7_3:
	callq	malloc
	movq	%rax, %rcx
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%r14d, %esi
	callq	set_fill
	movq	%rax, %rbp
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	set_ord
	cmpl	$2, %eax
	jl	.LBB7_15
# BB#4:                                 # %.preheader68.lr.ph
	xorl	%r12d, %r12d
                                        # implicit-def: %R15D
                                        # implicit-def: %R14D
	.p2align	4, 0x90
.LBB7_5:                                # %.preheader68
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_7 Depth 2
                                        #       Child Loop BB7_9 Depth 3
                                        #         Child Loop BB7_10 Depth 4
	movslq	cube+8(%rip), %rcx
	movl	$-1, %r8d
	testq	%rcx, %rcx
	jle	.LBB7_14
# BB#6:                                 # %.lr.ph
                                        #   in Loop: Header=BB7_5 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB7_7:                                #   Parent Loop BB7_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_9 Depth 3
                                        #         Child Loop BB7_10 Depth 4
	movl	%edx, %esi
	sarl	$5, %esi
	movslq	%esi, %rsi
	movl	4(%rbp,%rsi,4), %esi
	btl	%edx, %esi
	jae	.LBB7_13
# BB#8:                                 # %.preheader
                                        #   in Loop: Header=BB7_7 Depth=2
	movl	%edx, %esi
	jmp	.LBB7_9
	.p2align	4, 0x90
.LBB7_12:                               #   in Loop: Header=BB7_9 Depth=3
	movq	(%r13,%rdx,8), %rax
	movl	(%rax,%rdi,4), %eax
	cmpl	%r8d, %eax
	cmovgl	%edx, %r14d
	cmovgl	%esi, %r15d
	cmovgel	%eax, %r8d
.LBB7_9:                                # %.outer
                                        #   Parent Loop BB7_5 Depth=1
                                        #     Parent Loop BB7_7 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB7_10 Depth 4
	leal	1(%rsi), %edi
	movslq	%edi, %rbx
	.p2align	4, 0x90
.LBB7_10:                               #   Parent Loop BB7_5 Depth=1
                                        #     Parent Loop BB7_7 Depth=2
                                        #       Parent Loop BB7_9 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rbx, %rdi
	cmpl	%ecx, %edi
	jge	.LBB7_13
# BB#11:                                #   in Loop: Header=BB7_10 Depth=4
	incl	%esi
	movl	%edi, %ebx
	sarl	$5, %ebx
	movslq	%ebx, %rbx
	movl	4(%rbp,%rbx,4), %eax
	leaq	1(%rdi), %rbx
	btl	%edi, %eax
	jae	.LBB7_10
	jmp	.LBB7_12
	.p2align	4, 0x90
.LBB7_13:                               # %.loopexit
                                        #   in Loop: Header=BB7_7 Depth=2
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB7_7
.LBB7_14:                               # %._crit_edge
                                        #   in Loop: Header=BB7_5 Depth=1
	leal	1(%r14), %eax
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	8(%rsi), %rcx
	movslq	(%rsi), %rdx
	movl	%eax, (%rcx,%rdx,4)
	leal	1(%r15), %eax
	movq	16(%rsi), %rcx
	movslq	(%rsi), %rdx
	movl	%eax, (%rcx,%rdx,4)
	incl	(%rsi)
	movl	$-2, %eax
	movl	%r14d, %ecx
	roll	%cl, %eax
	movl	%r14d, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	andl	%eax, 4(%rbp,%rcx,4)
	movl	$-2, %eax
	movl	%r15d, %ecx
	roll	%cl, %eax
	movl	%r15d, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	andl	%eax, 4(%rbp,%rcx,4)
	addl	%r8d, %r12d
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	set_ord
	cmpl	$1, %eax
	jg	.LBB7_5
	jmp	.LBB7_16
.LBB7_15:                               # %._crit_edge80
	testq	%rbp, %rbp
	je	.LBB7_17
.LBB7_16:                               # %._crit_edge80.thread
	movq	%rbp, %rdi
	callq	free
.LBB7_17:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, (%rax)
	movl	%r12d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	greedy_best_cost, .Lfunc_end7-greedy_best_cost
	.cfi_endproc

	.globl	pair_best_cost
	.p2align	4, 0x90
	.type	pair_best_cost,@function
pair_best_cost:                         # @pair_best_cost
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 32
.Lcfi83:
	.cfi_offset %rbx, -32
.Lcfi84:
	.cfi_offset %r14, -24
.Lcfi85:
	.cfi_offset %r15, -16
	movl	$-1, best_cost(%rip)
	movq	$0, best_pair(%rip)
	movq	%rdi, cost_array(%rip)
	movslq	cube+8(%rip), %r14
	movl	$24, %edi
	callq	malloc
	movq	%rax, %r15
	movl	$0, (%r15)
	leaq	(,%r14,4), %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 8(%r15)
	movq	%rbx, %rdi
	callq	malloc
	cmpq	$33, %r14
	movq	%rax, 16(%r15)
	jge	.LBB8_2
# BB#1:
	movl	$8, %edi
	jmp	.LBB8_3
.LBB8_2:
	leal	-1(%r14), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB8_3:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%r14d, %esi
	callq	set_fill
	movq	%rax, %r14
	movl	cube+8(%rip), %esi
	movl	$find_best_cost, %ecx
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	generate_all_pairs
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB8_5
# BB#4:
	callq	free
	movq	$0, 8(%r15)
.LBB8_5:
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB8_7
# BB#6:
	callq	free
.LBB8_7:                                # %pair_free.exit
	movq	%r15, %rdi
	callq	free
	testq	%r14, %r14
	je	.LBB8_9
# BB#8:
	movq	%r14, %rdi
	callq	free
.LBB8_9:
	movq	best_pair(%rip), %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	pair_best_cost, .Lfunc_end8-pair_best_cost
	.cfi_endproc

	.globl	find_best_cost
	.p2align	4, 0x90
	.type	find_best_cost,@function
find_best_cost:                         # @find_best_cost
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi86:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi87:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi89:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi90:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi92:
	.cfi_def_cfa_offset 80
.Lcfi93:
	.cfi_offset %rbx, -56
.Lcfi94:
	.cfi_offset %r12, -48
.Lcfi95:
	.cfi_offset %r13, -40
.Lcfi96:
	.cfi_offset %r14, -32
.Lcfi97:
	.cfi_offset %r15, -24
.Lcfi98:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movslq	(%r14), %r13
	testq	%r13, %r13
	jle	.LBB9_1
# BB#2:                                 # %.lr.ph
	movq	cost_array(%rip), %rax
	movq	8(%r14), %rcx
	movq	16(%r14), %rdx
	testb	$1, %r13b
	jne	.LBB9_4
# BB#3:
	xorl	%esi, %esi
	xorl	%ebp, %ebp
	cmpl	$1, %r13d
	jne	.LBB9_6
	jmp	.LBB9_7
.LBB9_1:
	xorl	%ebp, %ebp
	cmpl	best_cost(%rip), %ebp
	jg	.LBB9_8
	jmp	.LBB9_27
.LBB9_4:
	movslq	(%rcx), %rsi
	movq	-8(%rax,%rsi,8), %rsi
	movslq	(%rdx), %rdi
	movl	-4(%rsi,%rdi,4), %ebp
	movl	$1, %esi
	cmpl	$1, %r13d
	je	.LBB9_7
	.p2align	4, 0x90
.LBB9_6:                                # =>This Inner Loop Header: Depth=1
	movslq	(%rcx,%rsi,4), %rdi
	movq	-8(%rax,%rdi,8), %rdi
	movslq	(%rdx,%rsi,4), %rbx
	addl	-4(%rdi,%rbx,4), %ebp
	movslq	4(%rcx,%rsi,4), %rdi
	movq	-8(%rax,%rdi,8), %rdi
	movslq	4(%rdx,%rsi,4), %rbx
	addl	-4(%rdi,%rbx,4), %ebp
	addq	$2, %rsi
	cmpq	%r13, %rsi
	jl	.LBB9_6
.LBB9_7:                                # %._crit_edge
	cmpl	best_cost(%rip), %ebp
	jle	.LBB9_27
.LBB9_8:
	movl	%ebp, best_cost(%rip)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	leaq	(,%r13,4), %r12
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, %r15
	movq	%r15, 8(%rbx)
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, 16(%rbx)
	movl	%r13d, (%rbx)
	testl	%r13d, %r13d
	jle	.LBB9_26
# BB#9:                                 # %.lr.ph.i
	movq	8(%r14), %rcx
	movq	16(%r14), %rdx
	cmpl	$8, %r13d
	jae	.LBB9_11
# BB#10:
	xorl	%edi, %edi
	jmp	.LBB9_25
.LBB9_11:                               # %min.iters.checked
	movq	%r13, %rsi
	andq	$-8, %rsi
	je	.LBB9_12
# BB#13:                                # %vector.memcheck
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	leaq	(%r15,%r13,4), %rdi
	leaq	(%rax,%r13,4), %r11
	leaq	(%rcx,%r13,4), %r9
	leaq	(%rdx,%r13,4), %r10
	movq	%r10, 8(%rsp)           # 8-byte Spill
	cmpq	%r11, %r15
	sbbb	%bl, %bl
	cmpq	%rdi, %rax
	sbbb	%r8b, %r8b
	andb	%bl, %r8b
	cmpq	%r9, %r15
	sbbb	%r12b, %r12b
	cmpq	%rdi, %rcx
	sbbb	%bl, %bl
	movb	%bl, 7(%rsp)            # 1-byte Spill
	cmpq	%r10, %r15
	sbbb	%r10b, %r10b
	cmpq	%rdi, %rdx
	sbbb	%bl, %bl
	movb	%bl, 6(%rsp)            # 1-byte Spill
	cmpq	%r9, %rax
	sbbb	%r9b, %r9b
	cmpq	%r11, %rcx
	sbbb	%bl, %bl
	movb	%bl, 5(%rsp)            # 1-byte Spill
	cmpq	8(%rsp), %rax           # 8-byte Folded Reload
	sbbb	%bl, %bl
	movb	%bl, 8(%rsp)            # 1-byte Spill
	cmpq	%r11, %rdx
	sbbb	%r11b, %r11b
	xorl	%edi, %edi
	testb	$1, %r8b
	jne	.LBB9_14
# BB#15:                                # %vector.memcheck
	andb	7(%rsp), %r12b          # 1-byte Folded Reload
	andb	$1, %r12b
	movq	16(%rsp), %rbx          # 8-byte Reload
	jne	.LBB9_25
# BB#16:                                # %vector.memcheck
	andb	6(%rsp), %r10b          # 1-byte Folded Reload
	andb	$1, %r10b
	jne	.LBB9_25
# BB#17:                                # %vector.memcheck
	andb	5(%rsp), %r9b           # 1-byte Folded Reload
	andb	$1, %r9b
	jne	.LBB9_25
# BB#18:                                # %vector.memcheck
	movb	8(%rsp), %bl            # 1-byte Reload
	andb	%r11b, %bl
	andb	$1, %bl
	movq	16(%rsp), %rbx          # 8-byte Reload
	jne	.LBB9_25
# BB#19:                                # %vector.body.preheader
	leaq	-8(%rsi), %rdi
	movq	%rdi, %r8
	shrq	$3, %r8
	btl	$3, %edi
	jb	.LBB9_20
# BB#21:                                # %vector.body.prol
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	%xmm0, (%r15)
	movups	%xmm1, 16(%r15)
	movups	(%rdx), %xmm0
	movups	16(%rdx), %xmm1
	movups	%xmm0, (%rax)
	movups	%xmm1, 16(%rax)
	movl	$8, %edi
	testq	%r8, %r8
	jne	.LBB9_23
	jmp	.LBB9_24
.LBB9_12:
	xorl	%edi, %edi
	jmp	.LBB9_25
.LBB9_20:
	xorl	%edi, %edi
	testq	%r8, %r8
	je	.LBB9_24
	.p2align	4, 0x90
.LBB9_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rcx,%rdi,4), %xmm0
	movups	16(%rcx,%rdi,4), %xmm1
	movups	%xmm0, (%r15,%rdi,4)
	movups	%xmm1, 16(%r15,%rdi,4)
	movups	(%rdx,%rdi,4), %xmm0
	movups	16(%rdx,%rdi,4), %xmm1
	movups	%xmm0, (%rax,%rdi,4)
	movups	%xmm1, 16(%rax,%rdi,4)
	movups	32(%rcx,%rdi,4), %xmm0
	movups	48(%rcx,%rdi,4), %xmm1
	movups	%xmm0, 32(%r15,%rdi,4)
	movups	%xmm1, 48(%r15,%rdi,4)
	movups	32(%rdx,%rdi,4), %xmm0
	movups	48(%rdx,%rdi,4), %xmm1
	movups	%xmm0, 32(%rax,%rdi,4)
	movups	%xmm1, 48(%rax,%rdi,4)
	addq	$16, %rdi
	cmpq	%rdi, %rsi
	jne	.LBB9_23
.LBB9_24:                               # %middle.block
	cmpq	%rsi, %r13
	movq	%rsi, %rdi
	je	.LBB9_26
	.p2align	4, 0x90
.LBB9_25:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rdi,4), %esi
	movl	%esi, (%r15,%rdi,4)
	movl	(%rdx,%rdi,4), %esi
	movl	%esi, (%rax,%rdi,4)
	incq	%rdi
	cmpq	%r13, %rdi
	jl	.LBB9_25
.LBB9_26:                               # %pair_save.exit
	movq	%rbx, best_pair(%rip)
.LBB9_27:
	movl	debug(%rip), %eax
	testb	$8, %ah
	je	.LBB9_33
# BB#28:
	movl	trace(%rip), %eax
	testl	%eax, %eax
	je	.LBB9_33
# BB#29:
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, (%r14)
	jle	.LBB9_32
# BB#30:                                # %.lr.ph.i19
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB9_31:                               # =>This Inner Loop Header: Depth=1
	movq	8(%r14), %rax
	movq	16(%r14), %rcx
	movl	(%rax,%rbp,4), %esi
	movl	(%rcx,%rbp,4), %edx
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbp
	movslq	(%r14), %rax
	cmpq	%rax, %rbp
	jl	.LBB9_31
.LBB9_32:                               # %print_pair.exit
	movl	$10, %edi
	callq	putchar
.LBB9_33:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_14:
	movq	16(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB9_25
.Lfunc_end9:
	.size	find_best_cost, .Lfunc_end9-find_best_cost
	.cfi_endproc

	.globl	pair_all
	.p2align	4, 0x90
	.type	pair_all,@function
pair_all:                               # @pair_all
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi101:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi103:
	.cfi_def_cfa_offset 48
.Lcfi104:
	.cfi_offset %rbx, -40
.Lcfi105:
	.cfi_offset %r12, -32
.Lcfi106:
	.cfi_offset %r14, -24
.Lcfi107:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	%r14, global_PLA(%rip)
	movl	%esi, pair_minim_strategy(%rip)
	movq	(%r14), %rax
	movl	12(%rax), %eax
	incl	%eax
	movl	%eax, best_cost(%rip)
	movq	$0, best_pair(%rip)
	movq	$0, best_phase(%rip)
	movq	$0, best_R(%rip)
	movq	$0, best_D(%rip)
	movq	$0, best_F(%rip)
	movslq	cube+8(%rip), %r15
	movl	$24, %edi
	callq	malloc
	movq	%rax, %r12
	movl	$0, (%r12)
	leaq	(,%r15,4), %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 8(%r12)
	movq	%rbx, %rdi
	callq	malloc
	cmpq	$33, %r15
	movq	%rax, 16(%r12)
	jge	.LBB10_2
# BB#1:
	movl	$8, %edi
	jmp	.LBB10_3
.LBB10_2:
	leal	-1(%r15), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB10_3:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%r15d, %esi
	callq	set_clear
	movq	%rax, %rcx
	movl	cube+8(%rip), %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	set_fill
	movq	%rax, %r15
	movl	cube+8(%rip), %esi
	movl	$minimize_pair, %ecx
	movq	%r12, %rdi
	movq	%r15, %rdx
	callq	generate_all_pairs
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_5
# BB#4:
	callq	free
	movq	$0, 8(%r12)
.LBB10_5:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_7
# BB#6:
	callq	free
.LBB10_7:                               # %pair_free.exit
	movq	%r12, %rdi
	callq	free
	testq	%r15, %r15
	je	.LBB10_9
# BB#8:
	movq	%r15, %rdi
	callq	free
.LBB10_9:
	movq	best_pair(%rip), %rax
	movq	%rax, 48(%r14)
	movq	best_phase(%rip), %rax
	movq	%rax, 40(%r14)
	movl	$1, %esi
	movq	%r14, %rdi
	callq	set_pair1
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	printf
	movq	48(%r14), %r15
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, (%r15)
	jle	.LBB10_12
# BB#10:                                # %.lr.ph.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB10_11:                              # =>This Inner Loop Header: Depth=1
	movq	8(%r15), %rax
	movq	16(%r15), %rcx
	movl	(%rax,%rbx,4), %esi
	movl	(%rcx,%rbx,4), %edx
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	movslq	(%r15), %rax
	cmpq	%rax, %rbx
	jl	.LBB10_11
.LBB10_12:                              # %print_pair.exit
	movl	$10, %edi
	callq	putchar
	movq	(%r14), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	8(%r14), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	16(%r14), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	best_F(%rip), %rax
	movq	%rax, (%r14)
	movq	best_D(%rip), %rax
	movq	%rax, 8(%r14)
	movq	best_R(%rip), %rax
	movq	%rax, 16(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end10:
	.size	pair_all, .Lfunc_end10-pair_all
	.cfi_endproc

	.globl	minimize_pair
	.p2align	4, 0x90
	.type	minimize_pair,@function
minimize_pair:                          # @minimize_pair
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi109:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi111:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi112:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi114:
	.cfi_def_cfa_offset 128
.Lcfi115:
	.cfi_offset %rbx, -56
.Lcfi116:
	.cfi_offset %r12, -48
.Lcfi117:
	.cfi_offset %r13, -40
.Lcfi118:
	.cfi_offset %r14, -32
.Lcfi119:
	.cfi_offset %r15, -24
.Lcfi120:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	global_PLA(%rip), %rax
	movq	(%rax), %rdi
	xorl	%eax, %eax
	callq	sf_save
	movq	%rax, %r15
	movq	global_PLA(%rip), %rax
	movq	8(%rax), %rdi
	xorl	%eax, %eax
	callq	sf_save
	movq	%rax, %r14
	movq	global_PLA(%rip), %rax
	movq	16(%rax), %rdi
	xorl	%eax, %eax
	callq	sf_save
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	cube+8(%rip), %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movslq	cube+4(%rip), %r12
	leaq	(,%r12,4), %rdi
	callq	malloc
	movq	%rax, %r13
	testq	%r12, %r12
	jle	.LBB11_17
# BB#1:                                 # %.lr.ph
	movq	cube+32(%rip), %rax
	cmpl	$8, %r12d
	jae	.LBB11_3
# BB#2:
	xorl	%ecx, %ecx
	jmp	.LBB11_16
.LBB11_3:                               # %min.iters.checked
	movq	%r12, %rcx
	andq	$-8, %rcx
	je	.LBB11_4
# BB#5:                                 # %vector.memcheck
	leaq	(%rax,%r12,4), %rdx
	cmpq	%rdx, %r13
	jae	.LBB11_8
# BB#6:                                 # %vector.memcheck
	leaq	(%r13,%r12,4), %rdx
	cmpq	%rdx, %rax
	jae	.LBB11_8
# BB#7:
	xorl	%ecx, %ecx
	jmp	.LBB11_16
.LBB11_4:
	xorl	%ecx, %ecx
	jmp	.LBB11_16
.LBB11_8:                               # %vector.body.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB11_9
# BB#10:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB11_11:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rax,%rdi,4), %xmm0
	movups	16(%rax,%rdi,4), %xmm1
	movups	%xmm0, (%r13,%rdi,4)
	movups	%xmm1, 16(%r13,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB11_11
	jmp	.LBB11_12
.LBB11_9:
	xorl	%edi, %edi
.LBB11_12:                              # %vector.body.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB11_15
# BB#13:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%r13,%rdi,4), %rsi
	leaq	112(%rax,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB11_14:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB11_14
.LBB11_15:                              # %middle.block
	cmpq	%rcx, %r12
	je	.LBB11_17
	.p2align	4, 0x90
.LBB11_16:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rcx,4), %edx
	movl	%edx, (%r13,%rcx,4)
	incq	%rcx
	cmpq	%r12, %rcx
	jl	.LBB11_16
.LBB11_17:                              # %._crit_edge
	movq	global_PLA(%rip), %rdi
	movq	%rbx, 48(%rdi)
	xorl	%esi, %esi
	callq	set_pair1
	cmpl	$0, summary(%rip)
	je	.LBB11_22
# BB#18:
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, (%rbx)
	jle	.LBB11_21
# BB#19:                                # %.lr.ph.i
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_20:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	movq	16(%rbx), %rcx
	movl	(%rax,%rbp,4), %esi
	movl	(%rcx,%rbp,4), %edx
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbp
	movslq	(%rbx), %rax
	cmpq	%rax, %rbp
	jl	.LBB11_20
.LBB11_21:                              # %print_pair.exit
	movl	$10, %edi
	callq	putchar
.LBB11_22:
	movl	pair_minim_strategy(%rip), %eax
	testl	%eax, %eax
	movq	%r14, 64(%rsp)          # 8-byte Spill
	je	.LBB11_30
# BB#23:
	cmpl	$1, %eax
	je	.LBB11_28
# BB#24:
	cmpl	$2, %eax
	jne	.LBB11_33
# BB#25:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbp
	movq	global_PLA(%rip), %rdi
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	phase_assignment
	cmpl	$0, summary(%rip)
	je	.LBB11_33
# BB#26:
	movq	global_PLA(%rip), %rax
	movq	(%rax), %r14
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%rbp, %rcx
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rcx, %rdx
	callq	print_trace
	cmpl	$0, summary(%rip)
	je	.LBB11_33
# BB#27:
	movq	global_PLA(%rip), %rax
	movq	40(%rax), %rdi
	xorl	%eax, %eax
	callq	pc1
	movq	%rax, %rcx
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	jmp	.LBB11_33
.LBB11_30:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r14
	movq	global_PLA(%rip), %rax
	movq	(%rax), %rdi
	movq	8(%rax), %rsi
	movq	16(%rax), %rdx
	xorl	%eax, %eax
	callq	espresso
	movq	%rax, %rbp
	movq	global_PLA(%rip), %rax
	movq	%rbp, (%rax)
	cmpl	$0, summary(%rip)
	je	.LBB11_33
# BB#31:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r14, %rcx
	movl	$.L.str.6, %esi
	jmp	.LBB11_32
.LBB11_28:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r14
	movq	global_PLA(%rip), %rax
	movq	(%rax), %rdi
	movq	8(%rax), %rsi
	movq	16(%rax), %rdx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	minimize_exact
	movq	%rax, %rbp
	movq	global_PLA(%rip), %rax
	movq	%rbp, (%rax)
	cmpl	$0, summary(%rip)
	je	.LBB11_33
# BB#29:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r14, %rcx
	movl	$.L.str.12, %esi
.LBB11_32:                              # %.thread
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB11_33:                              # %.thread
	movq	global_PLA(%rip), %rcx
	movq	(%rcx), %rax
	movl	12(%rax), %eax
	cmpl	best_cost(%rip), %eax
	jge	.LBB11_68
# BB#34:
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movl	%eax, best_cost(%rip)
	movslq	(%rbx), %rbp
	movl	$24, %edi
	callq	malloc
	movq	%rax, %r12
	leaq	(,%rbp,4), %r15
	movq	%r15, %rdi
	callq	malloc
	movq	%rax, %r14
	movq	%r14, 8(%r12)
	movq	%r15, %rdi
	callq	malloc
	testq	%rbp, %rbp
	movq	%rax, 16(%r12)
	movl	%ebp, (%r12)
	jle	.LBB11_55
# BB#35:                                # %.lr.ph.i38
	movq	8(%rbx), %rcx
	movq	16(%rbx), %rdx
	cmpl	$8, %ebp
	jae	.LBB11_37
# BB#36:
	xorl	%edi, %edi
	jmp	.LBB11_54
.LBB11_37:                              # %min.iters.checked50
	movq	%rbp, %rsi
	andq	$-8, %rsi
	je	.LBB11_38
# BB#39:                                # %vector.memcheck80
	movq	%r12, 16(%rsp)          # 8-byte Spill
	leaq	(%r14,%rbp,4), %rdi
	leaq	(%rax,%rbp,4), %r10
	leaq	(%rcx,%rbp,4), %r9
	leaq	(%rdx,%rbp,4), %r12
	cmpq	%r10, %r14
	sbbb	%bl, %bl
	cmpq	%rdi, %rax
	sbbb	%r8b, %r8b
	andb	%bl, %r8b
	cmpq	%r9, %r14
	sbbb	%r15b, %r15b
	cmpq	%rdi, %rcx
	sbbb	%bl, %bl
	movb	%bl, 15(%rsp)           # 1-byte Spill
	cmpq	%r12, %r14
	sbbb	%r11b, %r11b
	cmpq	%rdi, %rdx
	sbbb	%bl, %bl
	movb	%bl, 14(%rsp)           # 1-byte Spill
	cmpq	%r9, %rax
	sbbb	%r9b, %r9b
	cmpq	%r10, %rcx
	sbbb	%bl, %bl
	movb	%bl, 13(%rsp)           # 1-byte Spill
	cmpq	%r12, %rax
	sbbb	%bl, %bl
	cmpq	%r10, %rdx
	sbbb	%r10b, %r10b
	xorl	%edi, %edi
	testb	$1, %r8b
	jne	.LBB11_40
# BB#41:                                # %vector.memcheck80
	andb	15(%rsp), %r15b         # 1-byte Folded Reload
	andb	$1, %r15b
	jne	.LBB11_42
# BB#43:                                # %vector.memcheck80
	andb	14(%rsp), %r11b         # 1-byte Folded Reload
	andb	$1, %r11b
	jne	.LBB11_44
# BB#45:                                # %vector.memcheck80
	andb	13(%rsp), %r9b          # 1-byte Folded Reload
	andb	$1, %r9b
	jne	.LBB11_46
# BB#47:                                # %vector.memcheck80
	andb	%r10b, %bl
	andb	$1, %bl
	movq	16(%rsp), %r12          # 8-byte Reload
	jne	.LBB11_54
# BB#48:                                # %vector.body46.preheader
	leaq	-8(%rsi), %rdi
	movq	%rdi, %rbx
	shrq	$3, %rbx
	btl	$3, %edi
	jb	.LBB11_49
# BB#50:                                # %vector.body46.prol
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	%xmm0, (%r14)
	movups	%xmm1, 16(%r14)
	movups	(%rdx), %xmm0
	movups	16(%rdx), %xmm1
	movups	%xmm0, (%rax)
	movups	%xmm1, 16(%rax)
	movl	$8, %edi
	testq	%rbx, %rbx
	jne	.LBB11_52
	jmp	.LBB11_53
.LBB11_38:
	xorl	%edi, %edi
	jmp	.LBB11_54
.LBB11_49:
	xorl	%edi, %edi
	testq	%rbx, %rbx
	je	.LBB11_53
	.p2align	4, 0x90
.LBB11_52:                              # %vector.body46
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rcx,%rdi,4), %xmm0
	movups	16(%rcx,%rdi,4), %xmm1
	movups	%xmm0, (%r14,%rdi,4)
	movups	%xmm1, 16(%r14,%rdi,4)
	movups	(%rdx,%rdi,4), %xmm0
	movups	16(%rdx,%rdi,4), %xmm1
	movups	%xmm0, (%rax,%rdi,4)
	movups	%xmm1, 16(%rax,%rdi,4)
	movups	32(%rcx,%rdi,4), %xmm0
	movups	48(%rcx,%rdi,4), %xmm1
	movups	%xmm0, 32(%r14,%rdi,4)
	movups	%xmm1, 48(%r14,%rdi,4)
	movups	32(%rdx,%rdi,4), %xmm0
	movups	48(%rdx,%rdi,4), %xmm1
	movups	%xmm0, 32(%rax,%rdi,4)
	movups	%xmm1, 48(%rax,%rdi,4)
	addq	$16, %rdi
	cmpq	%rdi, %rsi
	jne	.LBB11_52
.LBB11_53:                              # %middle.block47
	cmpq	%rsi, %rbp
	movq	%rsi, %rdi
	jne	.LBB11_54
	jmp	.LBB11_55
.LBB11_46:
	movq	16(%rsp), %r12          # 8-byte Reload
	jmp	.LBB11_54
.LBB11_44:
	movq	16(%rsp), %r12          # 8-byte Reload
	jmp	.LBB11_54
.LBB11_40:
	movq	16(%rsp), %r12          # 8-byte Reload
	jmp	.LBB11_54
.LBB11_42:
	movq	16(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB11_54:                              # %scalar.ph48
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rdi,4), %esi
	movl	%esi, (%r14,%rdi,4)
	movl	(%rdx,%rdi,4), %esi
	movl	%esi, (%rax,%rdi,4)
	incq	%rdi
	cmpq	%rbp, %rdi
	jl	.LBB11_54
.LBB11_55:                              # %pair_save.exit
	movq	%r12, best_pair(%rip)
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.LBB11_56
# BB#57:
	movl	(%rax), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	cmpl	$33, %eax
	movq	40(%rsp), %r12          # 8-byte Reload
	jae	.LBB11_59
# BB#58:
	movl	$8, %edi
	jmp	.LBB11_60
.LBB11_56:
	xorl	%eax, %eax
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	40(%rsp), %r12          # 8-byte Reload
	jmp	.LBB11_61
.LBB11_59:
	decl	%eax
	shrl	$5, %eax
	leal	8(,%rax,4), %edi
.LBB11_60:
	movq	24(%rsp), %r15          # 8-byte Reload
	callq	malloc
	movq	%rax, %rcx
	movq	40(%rbx), %rsi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	set_copy
.LBB11_61:
	movq	%rax, best_phase(%rip)
	movq	best_F(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB11_63
# BB#62:
	xorl	%eax, %eax
	callq	sf_free
.LBB11_63:
	movq	best_D(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB11_65
# BB#64:
	xorl	%eax, %eax
	callq	sf_free
.LBB11_65:
	movq	best_R(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB11_67
# BB#66:
	xorl	%eax, %eax
	callq	sf_free
.LBB11_67:
	movq	global_PLA(%rip), %rax
	movq	(%rax), %rdi
	xorl	%eax, %eax
	callq	sf_save
	movq	%rax, best_F(%rip)
	movq	global_PLA(%rip), %rax
	movq	8(%rax), %rdi
	xorl	%eax, %eax
	callq	sf_save
	movq	%rax, best_D(%rip)
	movq	global_PLA(%rip), %rax
	movq	16(%rax), %rdi
	xorl	%eax, %eax
	callq	sf_save
	movq	%rax, best_R(%rip)
.LBB11_68:
	xorl	%eax, %eax
	callq	setdown_cube
	movq	cube+32(%rip), %rdi
	testq	%rdi, %rdi
	movq	64(%rsp), %rbx          # 8-byte Reload
	je	.LBB11_70
# BB#69:
	callq	free
	movq	$0, cube+32(%rip)
.LBB11_70:
	movl	36(%rsp), %eax          # 4-byte Reload
	movl	%eax, cube+8(%rip)
	movl	%r12d, cube+4(%rip)
	movq	%r13, cube+32(%rip)
	xorl	%eax, %eax
	callq	cube_setup
	movq	global_PLA(%rip), %rax
	movq	(%rax), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	global_PLA(%rip), %rax
	movq	8(%rax), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	global_PLA(%rip), %rax
	movq	16(%rax), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	global_PLA(%rip), %rax
	movq	%r15, (%rax)
	movq	%rbx, 8(%rax)
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 16(%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%rax)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	minimize_pair, .Lfunc_end11-minimize_pair
	.cfi_endproc

	.globl	generate_all_pairs
	.p2align	4, 0x90
	.type	generate_all_pairs,@function
generate_all_pairs:                     # @generate_all_pairs
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi121:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi122:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi123:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi124:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi125:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi127:
	.cfi_def_cfa_offset 128
.Lcfi128:
	.cfi_offset %rbx, -56
.Lcfi129:
	.cfi_offset %r12, -48
.Lcfi130:
	.cfi_offset %r13, -40
.Lcfi131:
	.cfi_offset %r14, -32
.Lcfi132:
	.cfi_offset %r15, -24
.Lcfi133:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %rbx
	movl	%esi, %ebp
	movq	%rdi, %r14
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	set_ord
	cmpl	$1, %eax
	jg	.LBB12_2
# BB#1:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	*%r15
	jmp	.LBB12_41
.LBB12_2:
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movl	$24, %edi
	callq	malloc
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%rax, %r15
	movl	%ebp, 16(%rsp)          # 4-byte Spill
	movslq	%ebp, %rbx
	shlq	$2, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %rbp
	movq	%rbp, 8(%r15)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 16(%r15)
	movslq	(%r14), %rcx
	testq	%rcx, %rcx
	movq	%r15, 56(%rsp)          # 8-byte Spill
	movl	%ecx, (%r15)
	movq	32(%rsp), %r15          # 8-byte Reload
	jle	.LBB12_20
# BB#3:                                 # %.lr.ph.i
	movq	8(%r14), %rdx
	movq	16(%r14), %rsi
	cmpl	$8, %ecx
	jae	.LBB12_5
# BB#4:
	xorl	%edi, %edi
	jmp	.LBB12_19
.LBB12_5:                               # %min.iters.checked
	movq	%rcx, %r13
	andq	$-8, %r13
	je	.LBB12_6
# BB#7:                                 # %vector.memcheck
	leaq	(%rbp,%rcx,4), %rdi
	leaq	(%rax,%rcx,4), %r11
	leaq	(%rdx,%rcx,4), %r8
	leaq	(%rsi,%rcx,4), %r15
	cmpq	%r11, %rbp
	sbbb	%r9b, %r9b
	cmpq	%rdi, %rax
	sbbb	%r12b, %r12b
	andb	%r9b, %r12b
	cmpq	%r8, %rbp
	sbbb	%r14b, %r14b
	cmpq	%rdi, %rdx
	sbbb	%bl, %bl
	movb	%bl, 24(%rsp)           # 1-byte Spill
	cmpq	%r15, %rbp
	sbbb	%r10b, %r10b
	cmpq	%rdi, %rsi
	sbbb	%bl, %bl
	movb	%bl, 12(%rsp)           # 1-byte Spill
	cmpq	%r8, %rax
	sbbb	%r9b, %r9b
	cmpq	%r11, %rdx
	sbbb	%bl, %bl
	movb	%bl, 20(%rsp)           # 1-byte Spill
	cmpq	%r15, %rax
	sbbb	%r8b, %r8b
	cmpq	%r11, %rsi
	sbbb	%r11b, %r11b
	xorl	%edi, %edi
	testb	$1, %r12b
	jne	.LBB12_8
# BB#9:                                 # %vector.memcheck
	andb	24(%rsp), %r14b         # 1-byte Folded Reload
	andb	$1, %r14b
	movq	32(%rsp), %r15          # 8-byte Reload
	jne	.LBB12_19
# BB#10:                                # %vector.memcheck
	andb	12(%rsp), %r10b         # 1-byte Folded Reload
	andb	$1, %r10b
	jne	.LBB12_19
# BB#11:                                # %vector.memcheck
	andb	20(%rsp), %r9b          # 1-byte Folded Reload
	andb	$1, %r9b
	jne	.LBB12_19
# BB#12:                                # %vector.memcheck
	andb	%r11b, %r8b
	andb	$1, %r8b
	jne	.LBB12_19
# BB#13:                                # %vector.body.preheader
	leaq	-8(%r13), %rdi
	movq	%rdi, %r8
	shrq	$3, %r8
	btl	$3, %edi
	jb	.LBB12_14
# BB#15:                                # %vector.body.prol
	movups	(%rdx), %xmm0
	movups	16(%rdx), %xmm1
	movups	%xmm0, (%rbp)
	movups	%xmm1, 16(%rbp)
	movups	(%rsi), %xmm0
	movups	16(%rsi), %xmm1
	movups	%xmm0, (%rax)
	movups	%xmm1, 16(%rax)
	movl	$8, %edi
	testq	%r8, %r8
	jne	.LBB12_17
	jmp	.LBB12_18
.LBB12_6:
	xorl	%edi, %edi
	jmp	.LBB12_19
.LBB12_14:
	xorl	%edi, %edi
	testq	%r8, %r8
	je	.LBB12_18
	.p2align	4, 0x90
.LBB12_17:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdx,%rdi,4), %xmm0
	movups	16(%rdx,%rdi,4), %xmm1
	movups	%xmm0, (%rbp,%rdi,4)
	movups	%xmm1, 16(%rbp,%rdi,4)
	movups	(%rsi,%rdi,4), %xmm0
	movups	16(%rsi,%rdi,4), %xmm1
	movups	%xmm0, (%rax,%rdi,4)
	movups	%xmm1, 16(%rax,%rdi,4)
	movups	32(%rdx,%rdi,4), %xmm0
	movups	48(%rdx,%rdi,4), %xmm1
	movups	%xmm0, 32(%rbp,%rdi,4)
	movups	%xmm1, 48(%rbp,%rdi,4)
	movups	32(%rsi,%rdi,4), %xmm0
	movups	48(%rsi,%rdi,4), %xmm1
	movups	%xmm0, 32(%rax,%rdi,4)
	movups	%xmm1, 48(%rax,%rdi,4)
	addq	$16, %rdi
	cmpq	%rdi, %r13
	jne	.LBB12_17
.LBB12_18:                              # %middle.block
	cmpq	%r13, %rcx
	movq	%r13, %rdi
	je	.LBB12_20
	.p2align	4, 0x90
.LBB12_19:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdx,%rdi,4), %ebx
	movl	%ebx, (%rbp,%rdi,4)
	movl	(%rsi,%rdi,4), %ebx
	movl	%ebx, (%rax,%rdi,4)
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB12_19
.LBB12_20:                              # %pair_save.exit
	movl	(%r15), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	cmpl	$33, %eax
	jae	.LBB12_22
# BB#21:
	movl	$8, %edi
	jmp	.LBB12_23
.LBB12_22:
	decl	%eax
	shrl	$5, %eax
	leal	8(,%rax,4), %edi
.LBB12_23:
	callq	malloc
	movq	%rax, %rcx
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r15, %rsi
	callq	set_copy
	movq	%rax, %r8
	movl	16(%rsp), %edx          # 4-byte Reload
	testl	%edx, %edx
	jle	.LBB12_27
# BB#24:                                # %.lr.ph67.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_25:                              # %.lr.ph67
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %eax
	sarl	$5, %eax
	cltq
	movl	4(%r15,%rax,4), %eax
	btl	%ebx, %eax
	jb	.LBB12_27
# BB#26:                                #   in Loop: Header=BB12_25 Depth=1
	incl	%ebx
	cmpl	%edx, %ebx
	jl	.LBB12_25
.LBB12_27:                              # %._crit_edge68
	leal	1(%rbx), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	cmpl	%edx, %eax
	movq	%r8, 24(%rsp)           # 8-byte Spill
	jge	.LBB12_33
# BB#28:                                # %.lr.ph
	movl	%ebx, %ecx
	andl	$31, %ecx
	movl	$1, %eax
	shll	%cl, %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	$-2, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	roll	%cl, %eax
	movl	%eax, 44(%rsp)          # 4-byte Spill
	movl	%ebx, %eax
	sarl	$5, %eax
	incl	%eax
	cltq
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	12(%rsp), %r14d         # 4-byte Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB12_29:                              # =>This Inner Loop Header: Depth=1
	movl	%r14d, %eax
	sarl	$5, %eax
	movslq	%eax, %r12
	movl	4(%r15,%r12,4), %eax
	movl	$1, %r13d
	movl	%r14d, %ecx
	shll	%cl, %r13d
	andb	$31, %cl
	movzbl	%cl, %ecx
	btl	%ecx, %eax
	jae	.LBB12_30
# BB#31:                                #   in Loop: Header=BB12_29 Depth=1
	incq	%r12
	movl	44(%rsp), %eax          # 4-byte Reload
	andl	%eax, (%r8,%rbp,4)
	movl	%r13d, %eax
	notl	%eax
	andl	%eax, (%r8,%r12,4)
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	8(%r15), %rax
	movslq	(%r15), %rcx
	movl	12(%rsp), %edx          # 4-byte Reload
	movl	%edx, (%rax,%rcx,4)
	incl	%r14d
	movq	16(%r15), %rax
	movslq	(%r15), %rcx
	movl	%r14d, (%rax,%rcx,4)
	incl	(%r15)
	movq	%r15, %rdi
	movl	16(%rsp), %esi          # 4-byte Reload
	movq	%r8, %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	generate_all_pairs
	movq	24(%rsp), %r8           # 8-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	decl	(%r15)
	movq	32(%rsp), %r15          # 8-byte Reload
	movl	20(%rsp), %eax          # 4-byte Reload
	orl	%eax, (%r8,%rbp,4)
	orl	%r13d, (%r8,%r12,4)
	cmpl	%edx, %r14d
	jne	.LBB12_29
	jmp	.LBB12_33
	.p2align	4, 0x90
.LBB12_30:                              # %._crit_edge71
                                        #   in Loop: Header=BB12_29 Depth=1
	incl	%r14d
	cmpl	%edx, %r14d
	jne	.LBB12_29
.LBB12_33:                              # %._crit_edge
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	set_ord
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	andl	$-2, %ecx
	subl	%ecx, %eax
	cmpl	$1, %eax
	movq	56(%rsp), %r14          # 8-byte Reload
	jne	.LBB12_35
# BB#34:
	movl	$-2, %eax
	movl	%ebx, %ecx
	roll	%cl, %eax
	sarl	$5, %ebx
	movslq	%ebx, %rcx
	movq	24(%rsp), %rdx          # 8-byte Reload
	andl	%eax, 4(%rdx,%rcx,4)
	movq	%r14, %rdi
	movl	16(%rsp), %esi          # 4-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	generate_all_pairs
.LBB12_35:
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB12_37
# BB#36:
	callq	free
	movq	$0, 8(%r14)
.LBB12_37:
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB12_39
# BB#38:
	callq	free
.LBB12_39:                              # %pair_free.exit
	movq	%r14, %rdi
	callq	free
	movq	24(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB12_41
# BB#40:
	callq	free
.LBB12_41:
	xorl	%eax, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_8:
	movq	32(%rsp), %r15          # 8-byte Reload
	jmp	.LBB12_19
.Lfunc_end12:
	.size	generate_all_pairs, .Lfunc_end12-generate_all_pairs
	.cfi_endproc

	.globl	pair_new
	.p2align	4, 0x90
	.type	pair_new,@function
pair_new:                               # @pair_new
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi134:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi136:
	.cfi_def_cfa_offset 32
.Lcfi137:
	.cfi_offset %rbx, -24
.Lcfi138:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	$0, (%rbx)
	movslq	%ebp, %rbp
	shlq	$2, %rbp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, 8(%rbx)
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, 16(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end13:
	.size	pair_new, .Lfunc_end13-pair_new
	.cfi_endproc

	.globl	pair_save
	.p2align	4, 0x90
	.type	pair_save,@function
pair_save:                              # @pair_save
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi139:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi140:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi141:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi142:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi143:
	.cfi_def_cfa_offset 48
.Lcfi144:
	.cfi_offset %rbx, -40
.Lcfi145:
	.cfi_offset %r12, -32
.Lcfi146:
	.cfi_offset %r14, -24
.Lcfi147:
	.cfi_offset %r15, -16
	movl	%esi, %ebx
	movq	%rdi, %r15
	movl	$24, %edi
	callq	malloc
	movq	%rax, %r14
	movslq	%ebx, %r12
	shlq	$2, %r12
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, 8(%r14)
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, 16(%r14)
	movl	(%r15), %ecx
	movl	%ecx, (%r14)
	testl	%ecx, %ecx
	jle	.LBB14_3
# BB#1:                                 # %.lr.ph
	movq	8(%r15), %rcx
	movq	16(%r15), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB14_2:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edi
	movl	%edi, (%rbx,%rsi,4)
	movl	(%rdx,%rsi,4), %edi
	movl	%edi, (%rax,%rsi,4)
	incq	%rsi
	movslq	(%r15), %rdi
	cmpq	%rdi, %rsi
	jl	.LBB14_2
.LBB14_3:                               # %._crit_edge
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	pair_save, .Lfunc_end14-pair_save
	.cfi_endproc

	.globl	pair_free
	.p2align	4, 0x90
	.type	pair_free,@function
pair_free:                              # @pair_free
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi148:
	.cfi_def_cfa_offset 16
.Lcfi149:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_2
# BB#1:
	callq	free
	movq	$0, 8(%rbx)
.LBB15_2:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_4
# BB#3:
	callq	free
.LBB15_4:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end15:
	.size	pair_free, .Lfunc_end15-pair_free
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"can only pair binary-valued variables"
	.size	.L.str, 38

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%s+%s"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"    "
	.size	.L.str.2, 5

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%3d "
	.size	.L.str.3, 5

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"# "
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"ESPRESSO  "
	.size	.L.str.6, 11

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"pair is"
	.size	.L.str.7, 8

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	" (%d %d)"
	.size	.L.str.8, 9

	.type	best_cost,@object       # @best_cost
	.local	best_cost
	.comm	best_cost,4,4
	.type	best_pair,@object       # @best_pair
	.local	best_pair
	.comm	best_pair,8,8
	.type	cost_array,@object      # @cost_array
	.local	cost_array
	.comm	cost_array,8,8
	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"cost is %d "
	.size	.L.str.9, 12

	.type	global_PLA,@object      # @global_PLA
	.local	global_PLA
	.comm	global_PLA,8,8
	.type	pair_minim_strategy,@object # @pair_minim_strategy
	.local	pair_minim_strategy
	.comm	pair_minim_strategy,4,4
	.type	best_phase,@object      # @best_phase
	.local	best_phase
	.comm	best_phase,8,8
	.type	best_R,@object          # @best_R
	.local	best_R
	.comm	best_R,8,8
	.type	best_D,@object          # @best_D
	.local	best_D
	.comm	best_D,8,8
	.type	best_F,@object          # @best_F
	.local	best_F
	.comm	best_F,8,8
	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"OPO       "
	.size	.L.str.10, 11

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"# phase is %s\n"
	.size	.L.str.11, 15

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"EXACT     "
	.size	.L.str.12, 11


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
