	.text
	.file	"dynprog.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_1:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 176
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$500000, %edx           # imm = 0x7A120
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_44
# BB#1:
	movq	(%rsp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_44
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$10000, %edx            # imm = 0x2710
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_44
# BB#3:                                 # %polybench_alloc_data.exit
	movq	(%rsp), %r12
	testq	%r12, %r12
	je	.LBB6_44
# BB#4:                                 # %polybench_alloc_data.exit29
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$10000, %edx            # imm = 0x2710
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_44
# BB#5:                                 # %polybench_alloc_data.exit29
	movq	(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB6_44
# BB#6:                                 # %polybench_alloc_data.exit31
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_7:                                # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_8 Depth 2
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_8:                                #   Parent Loop BB6_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %edx
	imull	%eax, %edx
	movl	%edx, %esi
	shrl	$31, %esi
	addl	%edx, %esi
	andl	$-2, %esi
	subl	%esi, %edx
	imulq	$200, %rax, %rsi
	leaq	(%r12,%rsi), %rdi
	movl	%edx, (%rdi,%rcx,4)
	movl	%eax, %edx
	subl	%ecx, %edx
	movslq	%edx, %rdx
	imulq	$1374389535, %rdx, %rdx # imm = 0x51EB851F
	movq	%rdx, %rdi
	shrq	$63, %rdi
	sarq	$36, %rdx
	addl	%edi, %edx
	addq	%rbx, %rsi
	movl	%edx, (%rsi,%rcx,4)
	incq	%rcx
	cmpq	$50, %rcx
	jne	.LBB6_8
# BB#9:                                 #   in Loop: Header=BB6_7 Depth=1
	incq	%rax
	cmpq	$50, %rax
	jne	.LBB6_7
# BB#10:                                # %init_array.exit
	leaq	204(%r12), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	200(%rax), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB6_11:                               # %.preheader2.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_13 Depth 2
                                        #       Child Loop BB6_14 Depth 3
                                        #         Child Loop BB6_20 Depth 4
	movl	%eax, 48(%rsp)          # 4-byte Spill
	xorl	%esi, %esi
	movl	$10000, %edx            # imm = 0x2710
	movq	%r12, %rdi
	callq	memset
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	40(%rsp), %r9           # 8-byte Reload
	movq	%r12, 16(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB6_13:                               # %.lr.ph9.i
                                        #   Parent Loop BB6_11 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_14 Depth 3
                                        #         Child Loop BB6_20 Depth 4
	movq	%rbp, %rdi
	imulq	$10204, %rdi, %rcx      # imm = 0x27DC
	addq	$200, %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leaq	1(%rdi), %rbp
	imulq	$200, %rdi, %rcx
	leaq	(%r12,%rcx), %rdx
	leaq	(%rdx,%r14,4), %rsi
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	leaq	1(%r14), %rsi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	%r8, 72(%rsp)           # 8-byte Spill
	movq	%r9, 64(%rsp)           # 8-byte Spill
	xorl	%r12d, %r12d
	movq	%r14, %rsi
	.p2align	4, 0x90
.LBB6_14:                               #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_13 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB6_20 Depth 4
	imulq	$10000, %rdi, %rax      # imm = 0x2710
	addq	8(%rsp), %rax           # 8-byte Folded Reload
	imulq	$200, %rsi, %r11
	addq	%rax, %r11
	movl	$0, (%r11,%rdi,4)
	cmpq	%rsi, %rbp
	jge	.LBB6_21
# BB#15:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB6_14 Depth=3
	imulq	$200, %r12, %rax
	addq	96(%rsp), %rax          # 8-byte Folded Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	(%rbx,%rax), %r13d
	testb	$1, %r12b
	jne	.LBB6_17
# BB#16:                                #   in Loop: Header=BB6_14 Depth=3
	movq	%r14, %r10
	cmpq	$1, %r12
	jne	.LBB6_19
	jmp	.LBB6_21
	.p2align	4, 0x90
.LBB6_17:                               # %.lr.ph.i.prol
                                        #   in Loop: Header=BB6_14 Depth=3
	movq	88(%rsp), %rax          # 8-byte Reload
	addl	(%rax), %r13d
	imulq	$200, %r14, %rax
	addq	24(%rsp), %rax          # 8-byte Folded Reload
	addl	(%rax,%rsi,4), %r13d
	movl	%r13d, (%r11,%r14,4)
	movq	80(%rsp), %r10          # 8-byte Reload
	cmpq	$1, %r12
	je	.LBB6_21
.LBB6_19:                               # %.lr.ph.i.preheader.new
                                        #   in Loop: Header=BB6_14 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r10,4), %r15
	imulq	$200, %r10, %rbx
	addq	%r9, %rbx
	leaq	(%r8,%r10,4), %rax
	.p2align	4, 0x90
.LBB6_20:                               # %.lr.ph.i
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_13 Depth=2
                                        #       Parent Loop BB6_14 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	addl	(%r15), %r13d
	addl	-200(%rbx), %r13d
	movl	%r13d, (%rax)
	addl	4(%r15), %r13d
	addl	(%rbx), %r13d
	movl	%r13d, 4(%rax)
	addq	$2, %r10
	addq	$8, %r15
	addq	$400, %rbx              # imm = 0x190
	addq	$8, %rax
	cmpq	%r10, %rsi
	jne	.LBB6_20
.LBB6_21:                               # %._crit_edge.i
                                        #   in Loop: Header=BB6_14 Depth=3
	movq	104(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rcx), %rax
	movl	(%rax,%rsi,4), %eax
	addl	-4(%r11,%rsi,4), %eax
	movl	%eax, (%rdx,%rsi,4)
	incq	%rsi
	incq	%r12
	addq	$4, %r9
	addq	$200, %r8
	cmpq	$50, %rsi
	jne	.LBB6_14
# BB#12:                                # %.loopexit.i
                                        #   in Loop: Header=BB6_13 Depth=2
	incq	%r14
	addq	$200, 16(%rsp)          # 8-byte Folded Spill
	movq	64(%rsp), %r9           # 8-byte Reload
	addq	$4, %r9
	movq	72(%rsp), %r8           # 8-byte Reload
	addq	$10200, %r8             # imm = 0x27D8
	cmpq	$49, %rbp
	movq	24(%rsp), %r12          # 8-byte Reload
	jne	.LBB6_13
# BB#22:                                #   in Loop: Header=BB6_11 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	addl	196(%r12), %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	48(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	$10000, %eax            # imm = 0x2710
	jne	.LBB6_11
# BB#23:                                # %.preheader.i34.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_24:                               # %.preheader.i34
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_25 Depth 2
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_25:                               #   Parent Loop BB6_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %edx
	imull	%eax, %edx
	movl	%edx, %esi
	shrl	$31, %esi
	addl	%edx, %esi
	andl	$-2, %esi
	subl	%esi, %edx
	imulq	$200, %rax, %rsi
	leaq	(%r12,%rsi), %rdi
	movl	%edx, (%rdi,%rcx,4)
	movl	%eax, %edx
	subl	%ecx, %edx
	movslq	%edx, %rdx
	imulq	$1374389535, %rdx, %rdx # imm = 0x51EB851F
	movq	%rdx, %rdi
	shrq	$63, %rdi
	sarq	$36, %rdx
	addl	%edi, %edx
	addq	%rbx, %rsi
	movl	%edx, (%rsi,%rcx,4)
	incq	%rcx
	cmpq	$50, %rcx
	jne	.LBB6_25
# BB#26:                                #   in Loop: Header=BB6_24 Depth=1
	incq	%rax
	cmpq	$50, %rax
	jne	.LBB6_24
# BB#27:                                # %.preheader2.i43.preheader
	leaq	204(%r12), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	200(%rax), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_28:                               # %.preheader2.i43
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_30 Depth 2
                                        #       Child Loop BB6_31 Depth 3
                                        #         Child Loop BB6_37 Depth 4
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	movl	$10000, %edx            # imm = 0x2710
	movq	%r12, %rdi
	callq	memset
	movq	112(%rsp), %r8          # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%r12, 16(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	movl	$1, %r9d
	.p2align	4, 0x90
.LBB6_30:                               # %.lr.ph9.i50
                                        #   Parent Loop BB6_28 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_31 Depth 3
                                        #         Child Loop BB6_37 Depth 4
	movq	%r15, %rdi
	imulq	$10204, %rdi, %rcx      # imm = 0x27DC
	addq	$200, %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leaq	1(%rdi), %r15
	imulq	$200, %rdi, %rcx
	leaq	(%r12,%rcx), %rdx
	leaq	(%rdx,%r9,4), %rbp
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	leaq	1(%r9), %rbp
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	movq	%r8, 72(%rsp)           # 8-byte Spill
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movq	%rsi, %rax
	xorl	%r13d, %r13d
	movq	%r9, %rsi
	.p2align	4, 0x90
.LBB6_31:                               #   Parent Loop BB6_28 Depth=1
                                        #     Parent Loop BB6_30 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB6_37 Depth 4
	imulq	$10000, %rdi, %rbp      # imm = 0x2710
	addq	8(%rsp), %rbp           # 8-byte Folded Reload
	imulq	$200, %rsi, %r11
	addq	%rbp, %r11
	movl	$0, (%r11,%rdi,4)
	cmpq	%rsi, %r15
	jge	.LBB6_38
# BB#32:                                # %.lr.ph.i55.preheader
                                        #   in Loop: Header=BB6_31 Depth=3
	imulq	$200, %r13, %rbp
	addq	96(%rsp), %rbp          # 8-byte Folded Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	(%rbx,%rbp), %r12d
	testb	$1, %r13b
	jne	.LBB6_34
# BB#33:                                #   in Loop: Header=BB6_31 Depth=3
	movq	%r9, %r10
	cmpq	$1, %r13
	jne	.LBB6_36
	jmp	.LBB6_38
	.p2align	4, 0x90
.LBB6_34:                               # %.lr.ph.i55.prol
                                        #   in Loop: Header=BB6_31 Depth=3
	movq	88(%rsp), %rbp          # 8-byte Reload
	addl	(%rbp), %r12d
	imulq	$200, %r9, %rbp
	addq	24(%rsp), %rbp          # 8-byte Folded Reload
	addl	(%rbp,%rsi,4), %r12d
	movl	%r12d, (%r11,%r9,4)
	movq	80(%rsp), %r10          # 8-byte Reload
	cmpq	$1, %r13
	je	.LBB6_38
.LBB6_36:                               # %.lr.ph.i55.preheader.new
                                        #   in Loop: Header=BB6_31 Depth=3
	movq	16(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%r10,4), %rbp
	imulq	$200, %r10, %rbx
	addq	%rax, %rbx
	leaq	(%r8,%r10,4), %r14
	.p2align	4, 0x90
.LBB6_37:                               # %.lr.ph.i55
                                        #   Parent Loop BB6_28 Depth=1
                                        #     Parent Loop BB6_30 Depth=2
                                        #       Parent Loop BB6_31 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	addl	(%rbp), %r12d
	addl	-200(%rbx), %r12d
	movl	%r12d, (%r14)
	addl	4(%rbp), %r12d
	addl	(%rbx), %r12d
	movl	%r12d, 4(%r14)
	addq	$2, %r10
	addq	$8, %rbp
	addq	$400, %rbx              # imm = 0x190
	addq	$8, %r14
	cmpq	%r10, %rsi
	jne	.LBB6_37
.LBB6_38:                               # %._crit_edge.i58
                                        #   in Loop: Header=BB6_31 Depth=3
	movq	104(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rcx), %rbp
	movl	(%rbp,%rsi,4), %ebp
	addl	-4(%r11,%rsi,4), %ebp
	movl	%ebp, (%rdx,%rsi,4)
	incq	%rsi
	incq	%r13
	addq	$4, %rax
	addq	$200, %r8
	cmpq	$50, %rsi
	jne	.LBB6_31
# BB#29:                                # %.loopexit.i46
                                        #   in Loop: Header=BB6_30 Depth=2
	incq	%r9
	addq	$200, 16(%rsp)          # 8-byte Folded Spill
	movq	64(%rsp), %rsi          # 8-byte Reload
	addq	$4, %rsi
	movq	72(%rsp), %r8           # 8-byte Reload
	addq	$10200, %r8             # imm = 0x27D8
	cmpq	$49, %r15
	movq	24(%rsp), %r12          # 8-byte Reload
	jne	.LBB6_30
# BB#39:                                #   in Loop: Header=BB6_28 Depth=1
	movq	48(%rsp), %rdx          # 8-byte Reload
	addl	196(%r12), %edx
	movl	40(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	$10000, %eax            # imm = 0x2710
	jne	.LBB6_28
# BB#40:                                # %kernel_dynprog.exit60
	movq	56(%rsp), %rax          # 8-byte Reload
	cvtsi2sdl	%eax, %xmm0
	cvtsi2sdl	%edx, %xmm1
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	andpd	.LCPI6_0(%rip), %xmm2
	movq	stderr(%rip), %rdi
	ucomisd	.LCPI6_1(%rip), %xmm2
	jbe	.LBB6_42
# BB#41:                                # %check_FP.exit.thread
	movsd	.LCPI6_1(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	callq	fprintf
	movl	$1, %ebp
	jmp	.LBB6_43
.LBB6_42:                               # %check_FP.exit
	xorl	%ebp, %ebp
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	free
	movq	%r12, %rdi
	callq	free
	movq	104(%rsp), %rdi         # 8-byte Reload
	callq	free
.LBB6_43:
	movl	%ebp, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_44:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A = %lf and B = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 60

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%d "
	.size	.L.str.3, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
