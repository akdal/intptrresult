	.text
	.file	"move_gen.bc"
	.globl	move_generator
	.p2align	4, 0x90
	.type	move_generator,@function
move_generator:                         # @move_generator
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	andl	$1, %esi
	movl	g_board_size(,%rsi,4), %r9d
	testl	%r9d, %r9d
	jle	.LBB0_1
# BB#3:                                 # %.lr.ph47.preheader
	addq	$8, %rdi
	xorl	%r10d, %r10d
	movq	%rsi, %r8
	shlq	$7, %r8
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph47
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_8 Depth 2
	movl	g_board+8(%r8,%r10,4), %ecx
	andl	g_board(%r8,%r10,4), %ecx
	movl	g_board+4(%r8,%r10,4), %edx
	incq	%r10
	movl	%edx, %ebx
	shrl	%ebx
	orl	%edx, %ebx
	movl	%ecx, %r11d
	shrl	%r11d
	andl	%ecx, %r11d
	orl	%ebx, %r11d
	cmpl	$-1, %r11d
	je	.LBB0_5
# BB#7:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_6 Depth=1
	notl	%r11d
	movslq	%eax, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%rdi,%rcx,4), %rbx
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph
                                        #   Parent Loop BB0_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r11d, %edx
	negl	%edx
	andl	%r11d, %edx
	xorl	%edx, %r11d
	movl	%edx, %ecx
	andl	$65535, %ecx            # imm = 0xFFFF
	je	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_8 Depth=2
	movl	%ecx, %ecx
	movl	lastbit16(,%rcx,4), %r9d
	jmp	.LBB0_12
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_8 Depth=2
	movl	$100, %r9d
	cmpl	$65536, %edx            # imm = 0x10000
	jb	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_8 Depth=2
	shrl	$16, %edx
	movl	lastbit16(,%rdx,4), %r9d
	addl	$16, %r9d
	.p2align	4, 0x90
.LBB0_12:                               # %lastbit32.exit
                                        #   in Loop: Header=BB0_8 Depth=2
	movl	%r9d, -4(%rbx)
	movl	%r10d, -8(%rbx)
	movl	$0, (%rbx)
	addq	$12, %rbx
	incl	%eax
	testl	%r11d, %r11d
	jne	.LBB0_8
# BB#4:                                 # %.loopexit.loopexit
                                        #   in Loop: Header=BB0_6 Depth=1
	movl	g_board_size(,%rsi,4), %r9d
.LBB0_5:                                # %.loopexit
                                        #   in Loop: Header=BB0_6 Depth=1
	movslq	%r9d, %rcx
	cmpq	%rcx, %r10
	jl	.LBB0_6
	jmp	.LBB0_2
.LBB0_1:
	xorl	%eax, %eax
.LBB0_2:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end0:
	.size	move_generator, .Lfunc_end0-move_generator
	.cfi_endproc

	.globl	move_generator_stage1
	.p2align	4, 0x90
	.type	move_generator_stage1,@function
move_generator_stage1:                  # @move_generator_stage1
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	andl	$1, %esi
	movl	g_board_size(,%rsi,4), %r9d
	testl	%r9d, %r9d
	jle	.LBB1_1
# BB#3:                                 # %.lr.ph47.preheader
	addq	$8, %rdi
	xorl	%r10d, %r10d
	movq	%rsi, %r8
	shlq	$7, %r8
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph47
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_8 Depth 2
	movl	g_board+8(%r8,%r10,4), %ecx
	andl	g_board(%r8,%r10,4), %ecx
	movl	g_board+4(%r8,%r10,4), %r11d
	incq	%r10
	movl	%ecx, %edx
	shrl	%edx
	orl	%r11d, %ecx
	shrl	%r11d
	orl	%ecx, %r11d
	orl	%edx, %r11d
	cmpl	$-1, %r11d
	je	.LBB1_5
# BB#7:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_6 Depth=1
	notl	%r11d
	movslq	%eax, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%rdi,%rcx,4), %rbx
	.p2align	4, 0x90
.LBB1_8:                                # %.lr.ph
                                        #   Parent Loop BB1_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r11d, %edx
	negl	%edx
	andl	%r11d, %edx
	xorl	%edx, %r11d
	movl	%edx, %ecx
	andl	$65535, %ecx            # imm = 0xFFFF
	je	.LBB1_10
# BB#9:                                 #   in Loop: Header=BB1_8 Depth=2
	movl	%ecx, %ecx
	movl	lastbit16(,%rcx,4), %r9d
	jmp	.LBB1_12
	.p2align	4, 0x90
.LBB1_10:                               #   in Loop: Header=BB1_8 Depth=2
	movl	$100, %r9d
	cmpl	$65536, %edx            # imm = 0x10000
	jb	.LBB1_12
# BB#11:                                #   in Loop: Header=BB1_8 Depth=2
	shrl	$16, %edx
	movl	lastbit16(,%rdx,4), %r9d
	addl	$16, %r9d
	.p2align	4, 0x90
.LBB1_12:                               # %lastbit32.exit
                                        #   in Loop: Header=BB1_8 Depth=2
	movl	%r9d, -4(%rbx)
	movl	%r10d, -8(%rbx)
	movl	$0, (%rbx)
	addq	$12, %rbx
	incl	%eax
	testl	%r11d, %r11d
	jne	.LBB1_8
# BB#4:                                 # %.loopexit.loopexit
                                        #   in Loop: Header=BB1_6 Depth=1
	movl	g_board_size(,%rsi,4), %r9d
.LBB1_5:                                # %.loopexit
                                        #   in Loop: Header=BB1_6 Depth=1
	movslq	%r9d, %rcx
	cmpq	%rcx, %r10
	jl	.LBB1_6
	jmp	.LBB1_2
.LBB1_1:
	xorl	%eax, %eax
.LBB1_2:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end1:
	.size	move_generator_stage1, .Lfunc_end1-move_generator_stage1
	.cfi_endproc

	.globl	move_generator_stage2
	.p2align	4, 0x90
	.type	move_generator_stage2,@function
move_generator_stage2:                  # @move_generator_stage2
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 16
.Lcfi5:
	.cfi_offset %rbx, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	andl	$1, %edx
	movl	g_board_size(,%rdx,4), %r9d
	testl	%r9d, %r9d
	jle	.LBB2_11
# BB#1:                                 # %.lr.ph45.preheader
	addq	$8, %rdi
	xorl	%r10d, %r10d
	movq	%rdx, %r8
	shlq	$7, %r8
	.p2align	4, 0x90
.LBB2_4:                                # %.lr.ph45
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_6 Depth 2
	movl	g_board+8(%r8,%r10,4), %eax
	andl	g_board(%r8,%r10,4), %eax
	movl	g_board+4(%r8,%r10,4), %ecx
	incq	%r10
	movl	%ecx, %r11d
	shrl	%r11d
	orl	%ecx, %r11d
	movl	%eax, %ecx
	shrl	%ecx
	xorl	%eax, %ecx
	notl	%r11d
	andl	%ecx, %r11d
	je	.LBB2_3
# BB#5:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_4 Depth=1
	movslq	%esi, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,4), %rbx
	.p2align	4, 0x90
.LBB2_6:                                # %.lr.ph
                                        #   Parent Loop BB2_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r11d, %ecx
	negl	%ecx
	andl	%r11d, %ecx
	xorl	%ecx, %r11d
	movl	%ecx, %eax
	andl	$65535, %eax            # imm = 0xFFFF
	je	.LBB2_8
# BB#7:                                 #   in Loop: Header=BB2_6 Depth=2
	movl	%eax, %eax
	movl	lastbit16(,%rax,4), %r9d
	jmp	.LBB2_10
	.p2align	4, 0x90
.LBB2_8:                                #   in Loop: Header=BB2_6 Depth=2
	movl	$100, %r9d
	cmpl	$65536, %ecx            # imm = 0x10000
	jb	.LBB2_10
# BB#9:                                 #   in Loop: Header=BB2_6 Depth=2
	shrl	$16, %ecx
	movl	lastbit16(,%rcx,4), %r9d
	addl	$16, %r9d
	.p2align	4, 0x90
.LBB2_10:                               # %lastbit32.exit
                                        #   in Loop: Header=BB2_6 Depth=2
	movl	%r9d, -4(%rbx)
	movl	%r10d, -8(%rbx)
	movl	$0, (%rbx)
	addq	$12, %rbx
	incl	%esi
	testl	%r11d, %r11d
	jne	.LBB2_6
# BB#2:                                 # %.loopexit.loopexit
                                        #   in Loop: Header=BB2_4 Depth=1
	movl	g_board_size(,%rdx,4), %r9d
.LBB2_3:                                # %.loopexit
                                        #   in Loop: Header=BB2_4 Depth=1
	movslq	%r9d, %rax
	cmpq	%rax, %r10
	jl	.LBB2_4
.LBB2_11:                               # %._crit_edge
	movl	%esi, %eax
	popq	%rbx
	retq
.Lfunc_end2:
	.size	move_generator_stage2, .Lfunc_end2-move_generator_stage2
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
