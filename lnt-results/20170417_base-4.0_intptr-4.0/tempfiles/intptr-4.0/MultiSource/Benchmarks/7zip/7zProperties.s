	.text
	.file	"7zProperties.bc"
	.globl	_ZN8NArchive3N7z8CHandler10FillPopIDsEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandler10FillPopIDsEv,@function
_ZN8NArchive3N7z8CHandler10FillPopIDsEv: # @_ZN8NArchive3N7z8CHandler10FillPopIDsEv
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 80
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r13, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	leaq	880(%r15), %r14
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	664(%r15), %rsi
	movq	%rsp, %rdi
	callq	_ZN13CRecordVectorIyEC2ERKS0_
	movslq	12(%rsp), %r12
	testq	%r12, %r12
	jle	.LBB0_82
# BB#1:                                 # %.lr.ph.i
	movq	16(%rsp), %rax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	cmpq	$14, (%rax,%rsi,8)
	je	.LBB0_3
# BB#5:                                 #   in Loop: Header=BB0_2 Depth=1
	incq	%rsi
	cmpq	%r12, %rsi
	jl	.LBB0_2
	jmp	.LBB0_6
.LBB0_3:
	movq	(%rsp), %rax
.Ltmp0:
	movq	%rsp, %rdi
	movl	$1, %edx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	*16(%rax)
.Ltmp1:
# BB#4:                                 # %._ZN8NArchive3N7zL13RemoveOneItemER13CRecordVectorIyEj.exit_crit_edge
	movl	12(%rsp), %r12d
.LBB0_6:                                # %_ZN8NArchive3N7zL13RemoveOneItemER13CRecordVectorIyEj.exit
	testl	%r12d, %r12d
	jle	.LBB0_82
# BB#7:                                 # %.lr.ph.i2
	movq	16(%rsp), %rax
	movslq	%r12d, %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_8:                                # =>This Inner Loop Header: Depth=1
	cmpq	$15, (%rax,%rsi,8)
	je	.LBB0_9
# BB#11:                                #   in Loop: Header=BB0_8 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB0_8
	jmp	.LBB0_12
.LBB0_9:
	movq	(%rsp), %rax
.Ltmp2:
	movq	%rsp, %rdi
	movl	$1, %edx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	*16(%rax)
.Ltmp3:
# BB#10:                                # %._ZN8NArchive3N7zL13RemoveOneItemER13CRecordVectorIyEj.exit6_crit_edge
	movl	12(%rsp), %r12d
.LBB0_12:                               # %_ZN8NArchive3N7zL13RemoveOneItemER13CRecordVectorIyEj.exit6
	testl	%r12d, %r12d
	jle	.LBB0_82
# BB#13:                                # %.lr.ph.i7
	movq	16(%rsp), %rax
	movslq	%r12d, %rcx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_14:                               # =>This Inner Loop Header: Depth=1
	cmpq	$17, (%rax,%rbx,8)
	je	.LBB0_15
# BB#18:                                #   in Loop: Header=BB0_14 Depth=1
	incq	%rbx
	cmpq	%rcx, %rbx
	jl	.LBB0_14
	jmp	.LBB0_19
.LBB0_15:
.Ltmp4:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp5:
# BB#16:                                # %.noexc10
	movq	896(%r15), %rax
	movslq	892(%r15), %rcx
	movq	$17, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 892(%r15)
	movq	(%rsp), %rax
.Ltmp6:
	movq	%rsp, %rdi
	movl	$1, %edx
	movl	%ebx, %esi
	callq	*16(%rax)
.Ltmp7:
# BB#17:                                # %.noexc10._ZN8NArchive3N7zL11CopyOneItemER13CRecordVectorIyES3_j.exit_crit_edge
	movl	12(%rsp), %r12d
.LBB0_19:                               # %_ZN8NArchive3N7zL11CopyOneItemER13CRecordVectorIyES3_j.exit
	testl	%r12d, %r12d
	jle	.LBB0_82
# BB#20:                                # %.lr.ph.i12
	movq	16(%rsp), %rax
	movslq	%r12d, %rcx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_21:                               # =>This Inner Loop Header: Depth=1
	cmpq	$16, (%rax,%rbx,8)
	je	.LBB0_22
# BB#25:                                #   in Loop: Header=BB0_21 Depth=1
	incq	%rbx
	cmpq	%rcx, %rbx
	jl	.LBB0_21
	jmp	.LBB0_26
.LBB0_22:
.Ltmp8:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp9:
# BB#23:                                # %.noexc15
	movq	896(%r15), %rax
	movslq	892(%r15), %rcx
	movq	$16, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 892(%r15)
	movq	(%rsp), %rax
.Ltmp10:
	movq	%rsp, %rdi
	movl	$1, %edx
	movl	%ebx, %esi
	callq	*16(%rax)
.Ltmp11:
# BB#24:                                # %.noexc15._ZN8NArchive3N7zL11CopyOneItemER13CRecordVectorIyES3_j.exit17_crit_edge
	movl	12(%rsp), %r12d
.LBB0_26:                               # %_ZN8NArchive3N7zL11CopyOneItemER13CRecordVectorIyES3_j.exit17
	testl	%r12d, %r12d
	jle	.LBB0_82
# BB#27:                                # %.lr.ph.i18
	movq	16(%rsp), %rax
	movslq	%r12d, %rcx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_28:                               # =>This Inner Loop Header: Depth=1
	cmpq	$9, (%rax,%rbx,8)
	je	.LBB0_29
# BB#32:                                #   in Loop: Header=BB0_28 Depth=1
	incq	%rbx
	cmpq	%rcx, %rbx
	jl	.LBB0_28
	jmp	.LBB0_33
.LBB0_29:
.Ltmp12:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp13:
# BB#30:                                # %.noexc21
	movq	896(%r15), %rax
	movslq	892(%r15), %rcx
	movq	$9, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 892(%r15)
	movq	(%rsp), %rax
.Ltmp14:
	movq	%rsp, %rdi
	movl	$1, %edx
	movl	%ebx, %esi
	callq	*16(%rax)
.Ltmp15:
# BB#31:                                # %.noexc21._ZN8NArchive3N7zL11CopyOneItemER13CRecordVectorIyES3_j.exit23_crit_edge
	movl	12(%rsp), %r12d
.LBB0_33:                               # %_ZN8NArchive3N7zL11CopyOneItemER13CRecordVectorIyES3_j.exit23
	testl	%r12d, %r12d
	jle	.LBB0_82
# BB#34:                                # %.lr.ph.i24
	movq	16(%rsp), %rax
	movslq	%r12d, %rcx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_35:                               # =>This Inner Loop Header: Depth=1
	cmpq	$6, (%rax,%rbx,8)
	je	.LBB0_36
# BB#39:                                #   in Loop: Header=BB0_35 Depth=1
	incq	%rbx
	cmpq	%rcx, %rbx
	jl	.LBB0_35
	jmp	.LBB0_40
.LBB0_36:
.Ltmp16:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp17:
# BB#37:                                # %.noexc27
	movq	896(%r15), %rax
	movslq	892(%r15), %rcx
	movq	$6, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 892(%r15)
	movq	(%rsp), %rax
.Ltmp18:
	movq	%rsp, %rdi
	movl	$1, %edx
	movl	%ebx, %esi
	callq	*16(%rax)
.Ltmp19:
# BB#38:                                # %.noexc27._ZN8NArchive3N7zL11CopyOneItemER13CRecordVectorIyES3_j.exit29_crit_edge
	movl	12(%rsp), %r12d
.LBB0_40:                               # %_ZN8NArchive3N7zL11CopyOneItemER13CRecordVectorIyES3_j.exit29
	testl	%r12d, %r12d
	jle	.LBB0_82
# BB#41:                                # %.lr.ph.i30
	movq	16(%rsp), %rax
	movslq	%r12d, %rcx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_42:                               # =>This Inner Loop Header: Depth=1
	cmpq	$18, (%rax,%rbx,8)
	je	.LBB0_43
# BB#46:                                #   in Loop: Header=BB0_42 Depth=1
	incq	%rbx
	cmpq	%rcx, %rbx
	jl	.LBB0_42
	jmp	.LBB0_47
.LBB0_43:
.Ltmp20:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp21:
# BB#44:                                # %.noexc33
	movq	896(%r15), %rax
	movslq	892(%r15), %rcx
	movq	$18, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 892(%r15)
	movq	(%rsp), %rax
.Ltmp22:
	movq	%rsp, %rdi
	movl	$1, %edx
	movl	%ebx, %esi
	callq	*16(%rax)
.Ltmp23:
# BB#45:                                # %.noexc33._ZN8NArchive3N7zL11CopyOneItemER13CRecordVectorIyES3_j.exit35_crit_edge
	movl	12(%rsp), %r12d
.LBB0_47:                               # %_ZN8NArchive3N7zL11CopyOneItemER13CRecordVectorIyES3_j.exit35
	testl	%r12d, %r12d
	jle	.LBB0_82
# BB#48:                                # %.lr.ph.i36
	movq	16(%rsp), %rax
	movslq	%r12d, %rcx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_49:                               # =>This Inner Loop Header: Depth=1
	cmpq	$20, (%rax,%rbx,8)
	je	.LBB0_50
# BB#53:                                #   in Loop: Header=BB0_49 Depth=1
	incq	%rbx
	cmpq	%rcx, %rbx
	jl	.LBB0_49
	jmp	.LBB0_54
.LBB0_50:
.Ltmp24:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp25:
# BB#51:                                # %.noexc39
	movq	896(%r15), %rax
	movslq	892(%r15), %rcx
	movq	$20, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 892(%r15)
	movq	(%rsp), %rax
.Ltmp26:
	movq	%rsp, %rdi
	movl	$1, %edx
	movl	%ebx, %esi
	callq	*16(%rax)
.Ltmp27:
# BB#52:                                # %.noexc39._ZN8NArchive3N7zL11CopyOneItemER13CRecordVectorIyES3_j.exit41_crit_edge
	movl	12(%rsp), %r12d
.LBB0_54:                               # %_ZN8NArchive3N7zL11CopyOneItemER13CRecordVectorIyES3_j.exit41
	testl	%r12d, %r12d
	jle	.LBB0_82
# BB#55:                                # %.lr.ph.i42
	movq	16(%rsp), %rax
	movslq	%r12d, %rcx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_56:                               # =>This Inner Loop Header: Depth=1
	cmpq	$19, (%rax,%rbx,8)
	je	.LBB0_57
# BB#60:                                #   in Loop: Header=BB0_56 Depth=1
	incq	%rbx
	cmpq	%rcx, %rbx
	jl	.LBB0_56
	jmp	.LBB0_61
.LBB0_57:
.Ltmp28:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp29:
# BB#58:                                # %.noexc45
	movq	896(%r15), %rax
	movslq	892(%r15), %rcx
	movq	$19, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 892(%r15)
	movq	(%rsp), %rax
.Ltmp30:
	movq	%rsp, %rdi
	movl	$1, %edx
	movl	%ebx, %esi
	callq	*16(%rax)
.Ltmp31:
# BB#59:                                # %.noexc45._ZN8NArchive3N7zL11CopyOneItemER13CRecordVectorIyES3_j.exit47_crit_edge
	movl	12(%rsp), %r12d
.LBB0_61:                               # %_ZN8NArchive3N7zL11CopyOneItemER13CRecordVectorIyES3_j.exit47
	testl	%r12d, %r12d
	jle	.LBB0_82
# BB#62:                                # %.lr.ph.i48
	movq	16(%rsp), %rax
	movslq	%r12d, %rcx
	xorl	%ebx, %ebx
.LBB0_63:                               # =>This Inner Loop Header: Depth=1
	cmpq	$21, (%rax,%rbx,8)
	je	.LBB0_64
# BB#67:                                #   in Loop: Header=BB0_63 Depth=1
	incq	%rbx
	cmpq	%rcx, %rbx
	jl	.LBB0_63
	jmp	.LBB0_68
.LBB0_64:
.Ltmp32:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp33:
# BB#65:                                # %.noexc51
	movq	896(%r15), %rax
	movslq	892(%r15), %rcx
	movq	$21, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 892(%r15)
	movq	(%rsp), %rax
.Ltmp34:
	movq	%rsp, %rdi
	movl	$1, %edx
	movl	%ebx, %esi
	callq	*16(%rax)
.Ltmp35:
# BB#66:                                # %.noexc51._ZN8NArchive3N7zL11CopyOneItemER13CRecordVectorIyES3_j.exit53_crit_edge
	movl	12(%rsp), %r12d
.LBB0_68:                               # %_ZN8NArchive3N7zL11CopyOneItemER13CRecordVectorIyES3_j.exit53
	testl	%r12d, %r12d
	jle	.LBB0_82
# BB#69:                                # %.lr.ph.i54
	movq	16(%rsp), %rax
	movslq	%r12d, %rcx
	xorl	%ebx, %ebx
.LBB0_70:                               # =>This Inner Loop Header: Depth=1
	cmpq	$10, (%rax,%rbx,8)
	je	.LBB0_71
# BB#74:                                #   in Loop: Header=BB0_70 Depth=1
	incq	%rbx
	cmpq	%rcx, %rbx
	jl	.LBB0_70
	jmp	.LBB0_75
.LBB0_71:
.Ltmp36:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp37:
# BB#72:                                # %.noexc57
	movq	896(%r15), %rax
	movslq	892(%r15), %rcx
	movq	$10, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 892(%r15)
	movq	(%rsp), %rax
.Ltmp38:
	movq	%rsp, %rdi
	movl	$1, %edx
	movl	%ebx, %esi
	callq	*16(%rax)
.Ltmp39:
# BB#73:                                # %.noexc57._ZN8NArchive3N7zL11CopyOneItemER13CRecordVectorIyES3_j.exit59_crit_edge
	movl	12(%rsp), %r12d
.LBB0_75:                               # %_ZN8NArchive3N7zL11CopyOneItemER13CRecordVectorIyES3_j.exit59
	testl	%r12d, %r12d
	jle	.LBB0_82
# BB#76:                                # %.lr.ph.i60
	movq	16(%rsp), %rax
	movslq	%r12d, %rcx
	xorl	%ebx, %ebx
.LBB0_77:                               # =>This Inner Loop Header: Depth=1
	cmpq	$22, (%rax,%rbx,8)
	je	.LBB0_78
# BB#81:                                #   in Loop: Header=BB0_77 Depth=1
	incq	%rbx
	cmpq	%rcx, %rbx
	jl	.LBB0_77
	jmp	.LBB0_82
.LBB0_78:
.Ltmp40:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp41:
# BB#79:                                # %.noexc63
	movq	896(%r15), %rax
	movslq	892(%r15), %rcx
	movq	$22, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 892(%r15)
	movq	(%rsp), %rax
.Ltmp42:
	movq	%rsp, %rdi
	movl	$1, %edx
	movl	%ebx, %esi
	callq	*16(%rax)
.Ltmp43:
# BB#80:                                # %.noexc63._ZN8NArchive3N7zL11CopyOneItemER13CRecordVectorIyES3_j.exit65thread-pre-split_crit_edge
	movl	12(%rsp), %r12d
.LBB0_82:                               # %_ZN8NArchive3N7zL11CopyOneItemER13CRecordVectorIyES3_j.exit65
	movl	892(%r15), %esi
	addl	%r12d, %esi
.Ltmp44:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp45:
# BB#83:                                # %.noexc69
	testl	%r12d, %r12d
	jle	.LBB0_87
# BB#84:                                # %.lr.ph.i66
	movl	%r12d, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_85:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rax
	movq	(%rax,%rbx,8), %r13
.Ltmp46:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp47:
# BB#86:                                # %.noexc70
                                        #   in Loop: Header=BB0_85 Depth=1
	movq	896(%r15), %rax
	movslq	892(%r15), %rcx
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 892(%r15)
	incq	%rbx
	cmpq	%rbx, %r12
	jne	.LBB0_85
.LBB0_87:                               # %_ZN13CRecordVectorIyEpLERKS0_.exit
.Ltmp49:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp50:
# BB#88:
	movq	896(%r15), %rax
	movslq	892(%r15), %rcx
	movq	$97, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 892(%r15)
.Ltmp51:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp52:
# BB#89:
	movq	896(%r15), %rax
	movslq	892(%r15), %rcx
	movq	$98, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 892(%r15)
.Ltmp53:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp54:
# BB#90:
	movq	896(%r15), %rax
	movslq	892(%r15), %rcx
	testq	%rcx, %rcx
	movq	$99, (%rax,%rcx,8)
	leaq	1(%rcx), %rcx
	movl	%ecx, 892(%r15)
	js	.LBB0_95
# BB#91:                                # %.lr.ph.i76
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_92:                               # =>This Inner Loop Header: Depth=1
	cmpq	$20, (%rax,%rsi,8)
	je	.LBB0_93
# BB#94:                                #   in Loop: Header=BB0_92 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB0_92
	jmp	.LBB0_95
.LBB0_93:
	movq	(%r14), %rax
.Ltmp55:
	movl	$1, %edx
	movq	%r14, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	*16(%rax)
.Ltmp56:
.LBB0_95:                               # %.loopexit.i
.Ltmp57:
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector13InsertOneItemEi
.Ltmp58:
# BB#96:
	movq	896(%r15), %rax
	movq	$20, (%rax)
	movslq	892(%r15), %rcx
	testq	%rcx, %rcx
	jle	.LBB0_101
# BB#97:                                # %.lr.ph.i84
	movl	$1, %esi
	.p2align	4, 0x90
.LBB0_98:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rcx, %rsi
	jge	.LBB0_101
# BB#99:                                # %._crit_edge
                                        #   in Loop: Header=BB0_98 Depth=1
	cmpq	$6, (%rax,%rsi,8)
	leaq	1(%rsi), %rsi
	jne	.LBB0_98
# BB#100:
	decl	%esi
	movq	(%r14), %rax
.Ltmp59:
	movl	$1, %edx
	movq	%r14, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	*16(%rax)
.Ltmp60:
.LBB0_101:                              # %.loopexit.i88
.Ltmp61:
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector13InsertOneItemEi
.Ltmp62:
# BB#102:
	movq	896(%r15), %rax
	movq	$6, (%rax)
	movslq	892(%r15), %rcx
	testq	%rcx, %rcx
	jle	.LBB0_107
# BB#103:                               # %.lr.ph.i95
	movl	$1, %esi
	.p2align	4, 0x90
.LBB0_104:                              # =>This Inner Loop Header: Depth=1
	cmpq	%rcx, %rsi
	jge	.LBB0_107
# BB#105:                               # %._crit_edge170
                                        #   in Loop: Header=BB0_104 Depth=1
	cmpq	$9, (%rax,%rsi,8)
	leaq	1(%rsi), %rsi
	jne	.LBB0_104
# BB#106:
	decl	%esi
	movq	(%r14), %rax
.Ltmp63:
	movl	$1, %edx
	movq	%r14, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	*16(%rax)
.Ltmp64:
.LBB0_107:                              # %.loopexit.i99
.Ltmp65:
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector13InsertOneItemEi
.Ltmp66:
# BB#108:
	movq	896(%r15), %rax
	movq	$9, (%rax)
	movslq	892(%r15), %rcx
	testq	%rcx, %rcx
	jle	.LBB0_113
# BB#109:                               # %.lr.ph.i106
	movl	$1, %esi
	.p2align	4, 0x90
.LBB0_110:                              # =>This Inner Loop Header: Depth=1
	cmpq	%rcx, %rsi
	jge	.LBB0_113
# BB#111:                               # %._crit_edge173
                                        #   in Loop: Header=BB0_110 Depth=1
	cmpq	$17, (%rax,%rsi,8)
	leaq	1(%rsi), %rsi
	jne	.LBB0_110
# BB#112:
	decl	%esi
	movq	(%r14), %rax
.Ltmp67:
	movl	$1, %edx
	movq	%r14, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	*16(%rax)
.Ltmp68:
.LBB0_113:                              # %.loopexit.i110
.Ltmp69:
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector13InsertOneItemEi
.Ltmp70:
# BB#114:
	movq	896(%r15), %rax
	movq	$17, (%rax)
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB0_116:                              # %.loopexit.split-lp
.Ltmp71:
	jmp	.LBB0_117
.LBB0_115:                              # %.loopexit
.Ltmp48:
.LBB0_117:
	movq	%rax, %rbx
.Ltmp72:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp73:
# BB#118:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_119:
.Ltmp74:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN8NArchive3N7z8CHandler10FillPopIDsEv, .Lfunc_end0-_ZN8NArchive3N7z8CHandler10FillPopIDsEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp45-.Ltmp0          #   Call between .Ltmp0 and .Ltmp45
	.long	.Ltmp71-.Lfunc_begin0   #     jumps to .Ltmp71
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin0   #     jumps to .Ltmp48
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp70-.Ltmp49         #   Call between .Ltmp49 and .Ltmp70
	.long	.Ltmp71-.Lfunc_begin0   #     jumps to .Ltmp71
	.byte	0                       #   On action: cleanup
	.long	.Ltmp70-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp72-.Ltmp70         #   Call between .Ltmp70 and .Ltmp72
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp72-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp73-.Ltmp72         #   Call between .Ltmp72 and .Ltmp73
	.long	.Ltmp74-.Lfunc_begin0   #     jumps to .Ltmp74
	.byte	1                       #   On action: 1
	.long	.Ltmp73-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Lfunc_end0-.Ltmp73     #   Call between .Ltmp73 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIyEC2ERKS0_,"axG",@progbits,_ZN13CRecordVectorIyEC2ERKS0_,comdat
	.weak	_ZN13CRecordVectorIyEC2ERKS0_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyEC2ERKS0_,@function
_ZN13CRecordVectorIyEC2ERKS0_:          # @_ZN13CRecordVectorIyEC2ERKS0_
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 48
.Lcfi16:
	.cfi_offset %rbx, -48
.Lcfi17:
	.cfi_offset %r12, -40
.Lcfi18:
	.cfi_offset %r13, -32
.Lcfi19:
	.cfi_offset %r14, -24
.Lcfi20:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$8, 24(%r12)
	movq	$_ZTV13CRecordVectorIyE+16, (%r12)
.Ltmp75:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp76:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp77:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp78:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB1_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_4:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %r13
.Ltmp80:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp81:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB1_4
.LBB1_6:                                # %_ZN13CRecordVectorIyEaSERKS0_.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB1_8:                                # %.loopexit.split-lp
.Ltmp79:
	jmp	.LBB1_9
.LBB1_7:                                # %.loopexit
.Ltmp82:
.LBB1_9:
	movq	%rax, %r14
.Ltmp83:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp84:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB1_11:
.Ltmp85:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end1:
	.size	_ZN13CRecordVectorIyEC2ERKS0_, .Lfunc_end1-_ZN13CRecordVectorIyEC2ERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp75-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp78-.Ltmp75         #   Call between .Ltmp75 and .Ltmp78
	.long	.Ltmp79-.Lfunc_begin1   #     jumps to .Ltmp79
	.byte	0                       #   On action: cleanup
	.long	.Ltmp80-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp81-.Ltmp80         #   Call between .Ltmp80 and .Ltmp81
	.long	.Ltmp82-.Lfunc_begin1   #     jumps to .Ltmp82
	.byte	0                       #   On action: cleanup
	.long	.Ltmp83-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp84-.Ltmp83         #   Call between .Ltmp83 and .Ltmp84
	.long	.Ltmp85-.Lfunc_begin1   #     jumps to .Ltmp85
	.byte	1                       #   On action: 1
	.long	.Ltmp84-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end1-.Ltmp84     #   Call between .Ltmp84 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.text
	.globl	_ZN8NArchive3N7z8CHandler21GetNumberOfPropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandler21GetNumberOfPropertiesEPj,@function
_ZN8NArchive3N7z8CHandler21GetNumberOfPropertiesEPj: # @_ZN8NArchive3N7z8CHandler21GetNumberOfPropertiesEPj
	.cfi_startproc
# BB#0:
	movl	892(%rdi), %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	_ZN8NArchive3N7z8CHandler21GetNumberOfPropertiesEPj, .Lfunc_end3-_ZN8NArchive3N7z8CHandler21GetNumberOfPropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive3N7z8CHandler15GetPropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandler15GetPropertyInfoEjPPwPjPt,@function
_ZN8NArchive3N7z8CHandler15GetPropertyInfoEjPPwPjPt: # @_ZN8NArchive3N7z8CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	%esi, 892(%rdi)
	jle	.LBB4_27
# BB#1:
	movq	896(%rdi), %rdi
	movslq	%esi, %rsi
	movq	(%rdi,%rsi,8), %rdi
	xorl	%esi, %esi
	cmpq	%rdi, _ZN8NArchive3N7z8kPropMapE(%rip)
	je	.LBB4_26
# BB#2:
	cmpq	%rdi, _ZN8NArchive3N7z8kPropMapE+24(%rip)
	jne	.LBB4_4
# BB#3:
	movl	$1, %esi
	jmp	.LBB4_26
.LBB4_4:
	cmpq	%rdi, _ZN8NArchive3N7z8kPropMapE+48(%rip)
	jne	.LBB4_6
# BB#5:
	movl	$2, %esi
	jmp	.LBB4_26
.LBB4_6:
	cmpq	%rdi, _ZN8NArchive3N7z8kPropMapE+72(%rip)
	jne	.LBB4_8
# BB#7:
	movl	$3, %esi
	jmp	.LBB4_26
.LBB4_8:
	cmpq	%rdi, _ZN8NArchive3N7z8kPropMapE+96(%rip)
	jne	.LBB4_10
# BB#9:
	movl	$4, %esi
	jmp	.LBB4_26
.LBB4_10:
	cmpq	%rdi, _ZN8NArchive3N7z8kPropMapE+120(%rip)
	jne	.LBB4_12
# BB#11:
	movl	$5, %esi
	jmp	.LBB4_26
.LBB4_12:
	cmpq	%rdi, _ZN8NArchive3N7z8kPropMapE+144(%rip)
	jne	.LBB4_14
# BB#13:
	movl	$6, %esi
	jmp	.LBB4_26
.LBB4_14:
	cmpq	%rdi, _ZN8NArchive3N7z8kPropMapE+168(%rip)
	jne	.LBB4_16
# BB#15:
	movl	$7, %esi
	jmp	.LBB4_26
.LBB4_16:
	cmpq	%rdi, _ZN8NArchive3N7z8kPropMapE+192(%rip)
	jne	.LBB4_18
# BB#17:
	movl	$8, %esi
	jmp	.LBB4_26
.LBB4_18:
	cmpq	%rdi, _ZN8NArchive3N7z8kPropMapE+216(%rip)
	jne	.LBB4_20
# BB#19:
	movl	$9, %esi
	jmp	.LBB4_26
.LBB4_20:
	cmpq	%rdi, _ZN8NArchive3N7z8kPropMapE+240(%rip)
	jne	.LBB4_22
# BB#21:
	movl	$10, %esi
	jmp	.LBB4_26
.LBB4_22:
	cmpq	%rdi, _ZN8NArchive3N7z8kPropMapE+264(%rip)
	jne	.LBB4_24
# BB#23:
	movl	$11, %esi
	jmp	.LBB4_26
.LBB4_24:
	cmpq	%rdi, _ZN8NArchive3N7z8kPropMapE+288(%rip)
	jne	.LBB4_27
# BB#25:
	movl	$12, %esi
.LBB4_26:                               # %select.unfold
	shlq	$3, %rsi
	movl	_ZN8NArchive3N7z8kPropMapE+16(%rsi,%rsi,2), %eax
	movl	%eax, (%rcx)
	movzwl	_ZN8NArchive3N7z8kPropMapE+20(%rsi,%rsi,2), %eax
	movw	%ax, (%r8)
	movq	$0, (%rdx)
	xorl	%eax, %eax
.LBB4_27:                               # %_ZN8NArchive3N7zL13FindPropInMapEy.exit
	retq
.Lfunc_end4:
	.size	_ZN8NArchive3N7z8CHandler15GetPropertyInfoEjPPwPjPt, .Lfunc_end4-_ZN8NArchive3N7z8CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIyED0Ev,"axG",@progbits,_ZN13CRecordVectorIyED0Ev,comdat
	.weak	_ZN13CRecordVectorIyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyED0Ev,@function
_ZN13CRecordVectorIyED0Ev:              # @_ZN13CRecordVectorIyED0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -24
.Lcfi25:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp86:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp87:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB5_2:
.Ltmp88:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZN13CRecordVectorIyED0Ev, .Lfunc_end5-_ZN13CRecordVectorIyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp86-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp87-.Ltmp86         #   Call between .Ltmp86 and .Ltmp87
	.long	.Ltmp88-.Lfunc_begin2   #     jumps to .Ltmp88
	.byte	0                       #   On action: cleanup
	.long	.Ltmp87-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end5-.Ltmp87     #   Call between .Ltmp87 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZN8NArchive3N7z8kPropMapE,@object # @_ZN8NArchive3N7z8kPropMapE
	.data
	.globl	_ZN8NArchive3N7z8kPropMapE
	.p2align	4
_ZN8NArchive3N7z8kPropMapE:
	.quad	17                      # 0x11
	.quad	0
	.long	3                       # 0x3
	.short	8                       # 0x8
	.zero	2
	.quad	9                       # 0x9
	.quad	0
	.long	7                       # 0x7
	.short	21                      # 0x15
	.zero	2
	.quad	6                       # 0x6
	.quad	0
	.long	8                       # 0x8
	.short	21                      # 0x15
	.zero	2
	.quad	18                      # 0x12
	.quad	0
	.long	10                      # 0xa
	.short	64                      # 0x40
	.zero	2
	.quad	20                      # 0x14
	.quad	0
	.long	12                      # 0xc
	.short	64                      # 0x40
	.zero	2
	.quad	19                      # 0x13
	.quad	0
	.long	11                      # 0xb
	.short	64                      # 0x40
	.zero	2
	.quad	21                      # 0x15
	.quad	0
	.long	9                       # 0x9
	.short	19                      # 0x13
	.zero	2
	.quad	24                      # 0x18
	.quad	0
	.long	29                      # 0x1d
	.short	19                      # 0x13
	.zero	2
	.quad	10                      # 0xa
	.quad	0
	.long	19                      # 0x13
	.short	19                      # 0x13
	.zero	2
	.quad	16                      # 0x10
	.quad	0
	.long	21                      # 0x15
	.short	11                      # 0xb
	.zero	2
	.quad	97                      # 0x61
	.quad	0
	.long	15                      # 0xf
	.short	11                      # 0xb
	.zero	2
	.quad	98                      # 0x62
	.quad	0
	.long	22                      # 0x16
	.short	8                       # 0x8
	.zero	2
	.quad	99                      # 0x63
	.quad	0
	.long	27                      # 0x1b
	.short	19                      # 0x13
	.zero	2
	.size	_ZN8NArchive3N7z8kPropMapE, 312

	.type	_ZTV13CRecordVectorIyE,@object # @_ZTV13CRecordVectorIyE
	.section	.rodata._ZTV13CRecordVectorIyE,"aG",@progbits,_ZTV13CRecordVectorIyE,comdat
	.weak	_ZTV13CRecordVectorIyE
	.p2align	3
_ZTV13CRecordVectorIyE:
	.quad	0
	.quad	_ZTI13CRecordVectorIyE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIyED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIyE, 40

	.type	_ZTS13CRecordVectorIyE,@object # @_ZTS13CRecordVectorIyE
	.section	.rodata._ZTS13CRecordVectorIyE,"aG",@progbits,_ZTS13CRecordVectorIyE,comdat
	.weak	_ZTS13CRecordVectorIyE
	.p2align	4
_ZTS13CRecordVectorIyE:
	.asciz	"13CRecordVectorIyE"
	.size	_ZTS13CRecordVectorIyE, 19

	.type	_ZTI13CRecordVectorIyE,@object # @_ZTI13CRecordVectorIyE
	.section	.rodata._ZTI13CRecordVectorIyE,"aG",@progbits,_ZTI13CRecordVectorIyE,comdat
	.weak	_ZTI13CRecordVectorIyE
	.p2align	4
_ZTI13CRecordVectorIyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIyE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIyE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
