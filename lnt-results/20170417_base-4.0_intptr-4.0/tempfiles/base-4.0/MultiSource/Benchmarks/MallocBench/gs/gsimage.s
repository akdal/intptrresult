	.text
	.file	"gsimage.bc"
	.globl	gs_image_init
	.p2align	4, 0x90
	.type	gs_image_init,@function
gs_image_init:                          # @gs_image_init
	.cfi_startproc
# BB#0:
	movq	%rsi, %r10
	movl	$-21, %eax
	cmpb	$0, 436(%r10)
	jne	.LBB0_6
# BB#1:
	decl	%r8d
	movl	$-15, %eax
	cmpl	$7, %r8d
	ja	.LBB0_6
# BB#2:                                 # %switch.hole_check
	movl	$139, %esi
	btl	%r8d, %esi
	jae	.LBB0_6
# BB#3:                                 # %switch.lookup
	addl	$4, %r9d
	cmpl	$8, %r9d
	ja	.LBB0_6
# BB#4:                                 # %switch.hole_check24
	movl	$419, %esi              # imm = 0x1A3
	btl	%r9d, %esi
	jae	.LBB0_6
# BB#5:                                 # %switch.lookup25
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%r8d, %rax
	movl	.Lswitch.table(,%rax,4), %r11d
	movslq	%r9d, %rsi
	movl	.Lswitch.table.1(,%rsi,4), %r8d
	movl	.Lswitch.table.2(,%rsi,4), %r9d
	movq	448(%r10), %rax
	movl	%edx, %esi
	movl	%ecx, %edx
	movl	%r11d, %ecx
	pushq	16(%rax)
.Lcfi1:
	.cfi_adjust_cfa_offset 8
	pushq	24(%rax)
.Lcfi2:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi3:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rsp)
.Lcfi4:
	.cfi_adjust_cfa_offset 8
	callq	image_init
	addq	$32, %rsp
.Lcfi5:
	.cfi_adjust_cfa_offset -32
	popq	%rcx
.LBB0_6:
	retq
.Lfunc_end0:
	.size	gs_image_init, .Lfunc_end0-gs_image_init
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	1166016512              # float 4096
	.text
	.globl	image_init
	.p2align	4, 0x90
	.type	image_init,@function
image_init:                             # @image_init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 176
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movl	%r8d, %r14d
	movl	%ecx, %ebx
	movl	%edx, %r15d
	movl	%esi, %r12d
	movq	%rdi, %r13
	testl	%r12d, %r12d
	movl	$-23, %eax
	jle	.LBB1_50
# BB#1:
	testl	%r15d, %r15d
	js	.LBB1_50
# BB#2:
	testl	%r15d, %r15d
	je	.LBB1_49
# BB#3:
	movq	176(%rsp), %rdi
	leaq	24(%rsp), %rsi
	callq	gs_matrix_invert
	testl	%eax, %eax
	js	.LBB1_50
# BB#4:
	movq	184(%rsp), %rax
	leaq	24(%rax), %rsi
	leaq	24(%rsp), %rdi
	movq	%rdi, %rdx
	callq	gs_matrix_multiply
	testl	%eax, %eax
	js	.LBB1_50
# BB#5:
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	movl	%r14d, %ebx
	leal	8(%r12), %r14d
	imull	%ebx, %r14d
	movl	$1, %edi
	movl	$.L.str, %edx
	movl	%r14d, %esi
	callq	gs_malloc
	testq	%rax, %rax
	je	.LBB1_6
# BB#7:
	movl	%r12d, (%r13)
	movl	%r15d, 4(%r13)
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, 8(%r13)
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	movl	%ebx, 12(%r13)
	movl	8(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, 16(%r13)
	movss	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	cvttss2si	%xmm1, %r11
	movq	%r11, 24(%r13)
	movss	72(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	cvttss2si	%xmm1, %rdi
	movq	%rdi, 48(%r13)
	movq	40(%rsp), %rcx
	movq	56(%rsp), %rdx
	movd	%edx, %xmm2
	orq	%rcx, %rdx
	movabsq	$9223372036854775807, %r8 # imm = 0x7FFFFFFFFFFFFFFF
	xorl	%esi, %esi
	andq	%rdx, %r8
	setne	%sil
	movl	%esi, 100(%r13)
	movd	%ecx, %xmm1
	je	.LBB1_8
# BB#9:
	movdqa	%xmm1, %xmm3
	mulss	%xmm0, %xmm3
	cvttss2si	%xmm3, %rcx
	movdqa	%xmm2, %xmm3
	mulss	%xmm0, %xmm3
	cvttss2si	%xmm3, %rdx
	jmp	.LBB1_10
.LBB1_6:
	movl	$-25, %eax
	jmp	.LBB1_50
.LBB1_8:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.LBB1_10:
	movq	184(%rsp), %rsi
	movq	%rsi, %r10
	movq	%rcx, 32(%r13)
	movq	%rdx, 40(%r13)
	movss	88(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	cvttss2si	%xmm3, %rsi
	movq	%rsi, 160(%r13)
	movss	104(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	cvttss2si	%xmm3, %r9
	movq	%r9, 168(%r13)
	movq	%r10, 72(%r13)
	movq	%rax, 80(%r13)
	movl	%r14d, 88(%r13)
	movslq	%r12d, %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	4(%rsp), %ecx           # 4-byte Reload
	shlq	%cl, %rdx
	movl	12(%rsp), %ebx          # 4-byte Reload
	movslq	%ebx, %rax
	imulq	%rdx, %rax
	movslq	8(%rsp), %rbp           # 4-byte Folded Reload
	xorl	%edx, %edx
	divq	%rbp
	addq	$7, %rax
	shrq	$3, %rax
	movl	%eax, 92(%r13)
	cmpl	$1, %ebx
	jne	.LBB1_19
# BB#11:
	cmpl	$1, %ecx
	je	.LBB1_17
# BB#12:
	cmpl	$2, %ecx
	je	.LBB1_16
# BB#13:
	cmpl	$3, %ecx
	jne	.LBB1_18
# BB#14:
	leaq	680(%r13), %rax
	movl	$65, %ecx
	.p2align	4, 0x90
.LBB1_15:                               # =>This Inner Loop Header: Depth=1
	movl	$-1, -384(%rax)
	movl	$-1, -416(%rax)
	movl	$-1, -448(%rax)
	movl	$-1, -480(%rax)
	movl	$-1, -256(%rax)
	movl	$-1, -288(%rax)
	movl	$-1, -320(%rax)
	movl	$-1, -352(%rax)
	movl	$-1, -128(%rax)
	movl	$-1, -160(%rax)
	movl	$-1, -192(%rax)
	movl	$-1, -224(%rax)
	movl	$-1, (%rax)
	movl	$-1, -32(%rax)
	movl	$-1, -64(%rax)
	movl	$-1, -96(%rax)
	addl	$-4, %ecx
	addq	$512, %rax              # imm = 0x200
	cmpl	$1, %ecx
	jg	.LBB1_15
	jmp	.LBB1_18
.LBB1_16:
	movl	$-1, 7816(%r13)
	movl	$-1, 7272(%r13)
	movl	$-1, 6728(%r13)
	movl	$-1, 6184(%r13)
	movl	$-1, 5096(%r13)
	movl	$-1, 4552(%r13)
	movl	$-1, 4008(%r13)
	movl	$-1, 3464(%r13)
	movl	$-1, 2376(%r13)
	movl	$-1, 1832(%r13)
	movl	$-1, 1288(%r13)
	movl	$-1, 744(%r13)
.LBB1_17:
	movl	$-1, 5640(%r13)
	movl	$-1, 2920(%r13)
.LBB1_18:                               # %.loopexit
	movq	192(%rsp), %rax
	movq	%rax, 184(%r13)
	movl	$0, 200(%r13)
	movq	200(%rsp), %rax
	movq	%rax, 8344(%r13)
	movl	$0, 8360(%r13)
.LBB1_19:
	movq	%rsi, %rcx
	movq	264(%r10), %rax
	movq	56(%rax), %rdx
	movq	64(%rax), %r12
	movq	72(%rax), %rbx
	movq	80(%rax), %r14
	movq	16(%rsp), %rsi          # 8-byte Reload
	imulq	%rsi, %r11
	movslq	%r15d, %r10
	imulq	%r10, %rdi
	testq	%r8, %r8
	je	.LBB1_20
# BB#21:
	mulss	%xmm0, %xmm2
	cvttss2si	%xmm2, %rbp
	imulq	%r10, %rbp
	addq	%r11, %rbp
	mulss	%xmm0, %xmm1
	cvttss2si	%xmm1, %rax
	imulq	%rsi, %rax
	addq	%rax, %rdi
	jmp	.LBB1_22
.LBB1_20:
	movq	%r11, %rbp
.LBB1_22:
	testq	%rbp, %rbp
	movq	184(%rsp), %rsi
	movq	%rsi, %r10
	movq	%rcx, %rsi
	js	.LBB1_23
# BB#26:
	cmpq	%rdx, %rsi
	jl	.LBB1_25
# BB#27:
	addq	%rsi, %rbp
	cmpq	%rbx, %rbp
	jle	.LBB1_28
	jmp	.LBB1_25
.LBB1_23:
	cmpq	%rbx, %rsi
	jg	.LBB1_25
# BB#24:
	addq	%rsi, %rbp
	cmpq	%rdx, %rbp
	jl	.LBB1_25
.LBB1_28:
	testq	%rdi, %rdi
	js	.LBB1_33
# BB#29:
	cmpq	%r12, %r9
	jge	.LBB1_30
.LBB1_25:                               # %.thread
	movl	$0, 96(%r13)
	movb	$1, %al
.LBB1_31:
	movl	8(%rsp), %edx           # 4-byte Reload
	movl	4(%rsp), %edi           # 4-byte Reload
	movl	12(%rsp), %ebp          # 4-byte Reload
.LBB1_40:
	movzbl	%al, %ecx
	movl	%ecx, 104(%r13)
	cmpb	$0, 437(%r10)
	je	.LBB1_42
# BB#41:
	movl	$image_render_skip, %eax
	jmp	.LBB1_47
.LBB1_42:
	cmpl	$1, %ebp
	jle	.LBB1_44
# BB#43:
	movl	$image_render_color, %eax
	jmp	.LBB1_47
.LBB1_33:
	addq	%r9, %rdi
	cmpq	%r14, %r9
	setg	%dl
	setle	%al
	cmpq	%r12, %rdi
	setl	%cl
	setge	%bl
	andb	%al, %bl
	testq	%r8, %r8
	movzbl	%bl, %eax
	movl	%eax, 96(%r13)
	movb	$1, %al
	jne	.LBB1_31
# BB#34:
	orb	%cl, %dl
	jmp	.LBB1_35
.LBB1_44:
	testl	%edi, %edi
	setne	%cl
	orb	%al, %cl
	jne	.LBB1_46
# BB#45:
	leaq	2048(%rsi,%r11), %rax
	sarq	$12, %rax
	sarq	$12, %rsi
	subq	%rsi, %rax
	cmpq	16(%rsp), %rax          # 8-byte Folded Reload
	jne	.LBB1_46
# BB#51:
	movq	$image_render_direct, 64(%r13)
	movl	$image_unpack_3, %eax
	jmp	.LBB1_48
.LBB1_30:
	addq	%r9, %rdi
	xorl	%eax, %eax
	cmpq	%r14, %rdi
	setle	%al
	setg	%cl
	testq	%r8, %r8
	movl	%eax, 96(%r13)
	movb	$1, %al
	jne	.LBB1_31
# BB#32:
	testb	%cl, %cl
.LBB1_35:
	movl	8(%rsp), %edx           # 4-byte Reload
	movl	4(%rsp), %edi           # 4-byte Reload
	movl	12(%rsp), %ebp          # 4-byte Reload
	jne	.LBB1_40
# BB#36:
	cmpq	$-1, 192(%rsp)
	je	.LBB1_39
# BB#37:
	cmpq	$-1, 200(%rsp)
	je	.LBB1_39
# BB#38:
	xorl	%eax, %eax
	jmp	.LBB1_40
.LBB1_46:
	movl	$image_render_mono, %eax
.LBB1_47:                               # %.sink.split
	movq	%rax, 64(%r13)
	cmpl	$1, %edx
	movslq	%edi, %rax
	movl	$image_init.procs, %ecx
	movl	$image_init.spread_procs, %edx
	cmoveq	%rcx, %rdx
	movq	(%rdx,%rax,8), %rax
.LBB1_48:
	movq	%rax, 56(%r13)
	movl	$0, 144(%r13)
	movl	$0, 152(%r13)
	movl	$0, 176(%r13)
.LBB1_49:
	xorl	%eax, %eax
.LBB1_50:
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_39:
	movq	312(%r10), %rax
	cmpl	$0, 16(%rax)
	setne	%al
	jmp	.LBB1_40
.Lfunc_end1:
	.size	image_init, .Lfunc_end1-image_init
	.cfi_endproc

	.globl	gs_imagemask_init
	.p2align	4, 0x90
	.type	gs_imagemask_init,@function
gs_imagemask_init:                      # @gs_imagemask_init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 64
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movl	%r8d, %ebp
	movl	%ecx, %r15d
	movl	%edx, %r12d
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movq	304(%rbx), %rdi
	movq	312(%rbx), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	gx_color_render
	movq	312(%rbx), %rax
	movq	(%rax), %rcx
	cmpl	$1, %ebp
	sbbq	%rax, %rax
	movq	%rax, %rbp
	notq	%rbp
	orq	%rcx, %rbp
	orq	%rcx, %rax
	movl	$0, %ecx
	movl	$1, %r8d
	movl	$1, %r9d
	movq	%r13, %rdi
	movl	%r12d, %esi
	movl	%r15d, %edx
	pushq	%rax
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	callq	image_init
	addq	$40, %rsp
.Lcfi36:
	.cfi_adjust_cfa_offset -32
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	gs_imagemask_init, .Lfunc_end2-gs_imagemask_init
	.cfi_endproc

	.globl	image_unpack_0
	.p2align	4, 0x90
	.type	image_unpack_0,@function
image_unpack_0:                         # @image_unpack_0
	.cfi_startproc
# BB#0:
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	testl	%ecx, %ecx
	je	.LBB3_6
# BB#1:                                 # %.lr.ph.preheader
	testb	$1, %cl
	jne	.LBB3_3
# BB#2:
	movl	%ecx, %eax
	cmpl	$1, %ecx
	jne	.LBB3_5
	jmp	.LBB3_6
.LBB3_3:                                # %.lr.ph.prol
	leal	-1(%rcx), %eax
	movzbl	(%rdx), %r8d
	incq	%rdx
	movq	%r8, %rdi
	shrq	$4, %rdi
	movl	map_4_to_32(,%rdi,4), %edi
	movl	%edi, (%rsi)
	andl	$15, %r8d
	movl	map_4_to_32(,%r8,4), %edi
	movl	%edi, 4(%rsi)
	addq	$8, %rsi
	cmpl	$1, %ecx
	je	.LBB3_6
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdx), %ecx
	movq	%rcx, %rdi
	shrq	$4, %rdi
	movl	map_4_to_32(,%rdi,4), %edi
	movl	%edi, (%rsi)
	andl	$15, %ecx
	movl	map_4_to_32(,%rcx,4), %ecx
	movl	%ecx, 4(%rsi)
	movzbl	1(%rdx), %ecx
	movq	%rcx, %rdi
	shrq	$4, %rdi
	andl	$15, %ecx
	addq	$2, %rdx
	addl	$-2, %eax
	movl	map_4_to_32(,%rdi,4), %edi
	movl	%edi, 8(%rsi)
	movl	map_4_to_32(,%rcx,4), %ecx
	movl	%ecx, 12(%rsi)
	leaq	16(%rsi), %rsi
	jne	.LBB3_5
.LBB3_6:                                # %._crit_edge
	retq
.Lfunc_end3:
	.size	image_unpack_0, .Lfunc_end3-image_unpack_0
	.cfi_endproc

	.globl	image_unpack_1
	.p2align	4, 0x90
	.type	image_unpack_1,@function
image_unpack_1:                         # @image_unpack_1
	.cfi_startproc
# BB#0:
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	testl	%ecx, %ecx
	je	.LBB4_6
# BB#1:                                 # %.lr.ph.preheader
	testb	$1, %cl
	jne	.LBB4_3
# BB#2:
	movl	%ecx, %eax
	cmpl	$1, %ecx
	jne	.LBB4_5
	jmp	.LBB4_6
.LBB4_3:                                # %.lr.ph.prol
	leal	-1(%rcx), %eax
	movzbl	(%rdx), %r8d
	incq	%rdx
	movq	%r8, %rdi
	shrq	$4, %rdi
	movzwl	map_4_to_16(%rdi,%rdi), %edi
	movw	%di, (%rsi)
	andl	$15, %r8d
	movzwl	map_4_to_16(%r8,%r8), %edi
	movw	%di, 2(%rsi)
	addq	$4, %rsi
	cmpl	$1, %ecx
	je	.LBB4_6
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdx), %ecx
	movq	%rcx, %rdi
	shrq	$4, %rdi
	movzwl	map_4_to_16(%rdi,%rdi), %edi
	movw	%di, (%rsi)
	andl	$15, %ecx
	movzwl	map_4_to_16(%rcx,%rcx), %ecx
	movw	%cx, 2(%rsi)
	movzbl	1(%rdx), %ecx
	movq	%rcx, %rdi
	shrq	$4, %rdi
	andl	$15, %ecx
	addq	$2, %rdx
	addl	$-2, %eax
	movzwl	map_4_to_16(%rdi,%rdi), %edi
	movw	%di, 4(%rsi)
	movzwl	map_4_to_16(%rcx,%rcx), %ecx
	movw	%cx, 6(%rsi)
	leaq	8(%rsi), %rsi
	jne	.LBB4_5
.LBB4_6:                                # %._crit_edge
	retq
.Lfunc_end4:
	.size	image_unpack_1, .Lfunc_end4-image_unpack_1
	.cfi_endproc

	.globl	image_unpack_2
	.p2align	4, 0x90
	.type	image_unpack_2,@function
image_unpack_2:                         # @image_unpack_2
	.cfi_startproc
# BB#0:
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	testl	%ecx, %ecx
	je	.LBB5_6
# BB#1:                                 # %.lr.ph
	movslq	16(%rdi), %r11
	testb	$1, %cl
	jne	.LBB5_3
# BB#2:
	movl	%ecx, %r10d
	cmpl	$1, %ecx
	jne	.LBB5_5
	jmp	.LBB5_6
.LBB5_3:
	leal	-1(%rcx), %r10d
	movb	(%rdx), %r8b
	incq	%rdx
	movl	%r8d, %r9d
	andb	$-16, %r9b
	movl	%r8d, %eax
	shrb	$4, %al
	orb	%r9b, %al
	movb	%al, (%rsi)
	movl	%r8d, %eax
	andb	$15, %al
	shlb	$4, %r8b
	orb	%al, %r8b
	movb	%r8b, (%rsi,%r11)
	addq	%r11, %rsi
	addq	%r11, %rsi
	cmpl	$1, %ecx
	je	.LBB5_6
	.p2align	4, 0x90
.LBB5_5:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rdx), %ecx
	movl	%ecx, %r8d
	andb	$-16, %r8b
	movl	%ecx, %eax
	shrb	$4, %al
	orb	%r8b, %al
	movb	%al, (%rsi)
	movl	%ecx, %eax
	andb	$15, %al
	shlb	$4, %cl
	orb	%al, %cl
	movb	%cl, (%rsi,%r11)
	addq	%r11, %rsi
	movzbl	1(%rdx), %eax
	movl	%eax, %r8d
	andb	$-16, %r8b
	movl	%eax, %ecx
	shrb	$4, %cl
	orb	%r8b, %cl
	leaq	(%rsi,%r11), %rdi
	movl	%eax, %r8d
	andb	$15, %r8b
	shlb	$4, %al
	orb	%r8b, %al
	movb	%cl, (%r11,%rsi)
	leaq	(%rdi,%r11), %rsi
	addq	$2, %rdx
	addq	%r11, %rsi
	addl	$-2, %r10d
	movb	%al, (%r11,%rdi)
	jne	.LBB5_5
.LBB5_6:                                # %._crit_edge
	retq
.Lfunc_end5:
	.size	image_unpack_2, .Lfunc_end5-image_unpack_2
	.cfi_endproc

	.globl	image_unpack_3
	.p2align	4, 0x90
	.type	image_unpack_3,@function
image_unpack_3:                         # @image_unpack_3
	.cfi_startproc
# BB#0:
	cmpq	%rsi, %rdx
	je	.LBB6_1
# BB#2:
	movl	%ecx, %eax
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rax, %rdx
	jmp	memcpy                  # TAILCALL
.LBB6_1:
	retq
.Lfunc_end6:
	.size	image_unpack_3, .Lfunc_end6-image_unpack_3
	.cfi_endproc

	.globl	image_unpack_0_spread
	.p2align	4, 0x90
	.type	image_unpack_0_spread,@function
image_unpack_0_spread:                  # @image_unpack_0_spread
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 56
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movq	%rsi, %r15
	testl	%r12d, %r12d
	je	.LBB7_3
# BB#1:                                 # %.lr.ph
	movslq	16(%rdi), %rdi
	.p2align	4, 0x90
.LBB7_2:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rdx), %r8d
	incq	%rdx
	movl	%r8d, %eax
	sarb	$7, %al
	movb	%al, -33(%rsp)          # 1-byte Spill
	movl	%r8d, %r9d
	shrb	$6, %r9b
	andb	$1, %r9b
	negb	%r9b
	leaq	(%r15,%rdi), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movl	%r8d, %r11d
	shrb	$5, %r11b
	andb	$1, %r11b
	negb	%r11b
	leaq	(%rax,%rdi), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movl	%r8d, %ebp
	shrb	$4, %bpl
	andb	$1, %bpl
	negb	%bpl
	leaq	(%rax,%rdi), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movl	%r8d, %r13d
	shrb	$3, %r13b
	andb	$1, %r13b
	negb	%r13b
	leaq	(%rax,%rdi), %rcx
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	movl	%r8d, %eax
	shrb	$2, %al
	andb	$1, %al
	negb	%al
	leaq	(%rcx,%rdi), %r14
	movl	%r8d, %ebx
	shrb	%bl
	andb	$1, %bl
	negb	%bl
	leaq	(%r14,%rdi), %r10
	andb	$1, %r8b
	negb	%r8b
	movzbl	-33(%rsp), %ecx         # 1-byte Folded Reload
	movb	%cl, (%r15)
	movb	%r9b, (%r15,%rdi)
	leaq	(%r10,%rdi), %r15
	addq	%rdi, %r15
	decl	%r12d
	movq	-8(%rsp), %rsi          # 8-byte Reload
	movb	%r11b, (%rdi,%rsi)
	movq	-16(%rsp), %rcx         # 8-byte Reload
	movb	%bpl, (%rdi,%rcx)
	movq	-24(%rsp), %rcx         # 8-byte Reload
	movb	%r13b, (%rdi,%rcx)
	movq	-32(%rsp), %rcx         # 8-byte Reload
	movb	%al, (%rdi,%rcx)
	movb	%bl, (%rdi,%r14)
	movb	%r8b, (%rdi,%r10)
	jne	.LBB7_2
.LBB7_3:                                # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	image_unpack_0_spread, .Lfunc_end7-image_unpack_0_spread
	.cfi_endproc

	.globl	image_unpack_1_spread
	.p2align	4, 0x90
	.type	image_unpack_1_spread,@function
image_unpack_1_spread:                  # @image_unpack_1_spread
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 24
.Lcfi51:
	.cfi_offset %rbx, -24
.Lcfi52:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	testl	%ecx, %ecx
	je	.LBB8_6
# BB#1:                                 # %.lr.ph
	movslq	16(%rdi), %rax
	testb	$1, %cl
	jne	.LBB8_3
# BB#2:
	movl	%ecx, %r9d
	cmpl	$1, %ecx
	jne	.LBB8_5
	jmp	.LBB8_6
.LBB8_3:
	leal	-1(%rcx), %r9d
	movzbl	(%rdx), %r8d
	incq	%rdx
	movq	%r8, %rdi
	shrq	$4, %rdi
	movzwl	map_4_to_16(%rdi,%rdi), %ebx
	movb	%bh, (%rsi)  # NOREX
	movb	%bl, (%rsi,%rax)
	addq	%rax, %rsi
	andl	$15, %r8d
	movzwl	map_4_to_16(%r8,%r8), %ebx
	movb	%bh, (%rax,%rsi)  # NOREX
	addq	%rax, %rsi
	movb	%bl, (%rax,%rsi)
	addq	%rax, %rsi
	addq	%rax, %rsi
	cmpl	$1, %ecx
	je	.LBB8_6
	.p2align	4, 0x90
.LBB8_5:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rdx), %ecx
	movq	%rcx, %rdi
	shrq	$4, %rdi
	movzwl	map_4_to_16(%rdi,%rdi), %ebx
	movb	%bh, (%rsi)  # NOREX
	movb	%bl, (%rsi,%rax)
	addq	%rax, %rsi
	andl	$15, %ecx
	movzwl	map_4_to_16(%rcx,%rcx), %ecx
	movb	%ch, (%rax,%rsi)  # NOREX
	addq	%rax, %rsi
	movb	%cl, (%rax,%rsi)
	addq	%rax, %rsi
	movzbl	1(%rdx), %ebx
	movq	%rbx, %rdi
	shrq	$4, %rdi
	leaq	(%rsi,%rax), %r10
	leaq	(%r10,%rax), %rbp
	andl	$15, %ebx
	leaq	(%rbp,%rax), %r8
	movzwl	map_4_to_16(%rdi,%rdi), %ecx
	movb	%ch, (%rax,%rsi)  # NOREX
	leaq	(%r8,%rax), %rsi
	addq	$2, %rdx
	addq	%rax, %rsi
	addl	$-2, %r9d
	movb	%cl, (%rax,%r10)
	movzwl	map_4_to_16(%rbx,%rbx), %ecx
	movb	%ch, (%rax,%rbp)  # NOREX
	movb	%cl, (%rax,%r8)
	jne	.LBB8_5
.LBB8_6:                                # %._crit_edge
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end8:
	.size	image_unpack_1_spread, .Lfunc_end8-image_unpack_1_spread
	.cfi_endproc

	.globl	image_unpack_3_spread
	.p2align	4, 0x90
	.type	image_unpack_3_spread,@function
image_unpack_3_spread:                  # @image_unpack_3_spread
	.cfi_startproc
# BB#0:
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	testl	%ecx, %ecx
	je	.LBB9_6
# BB#1:                                 # %.lr.ph
	movslq	16(%rdi), %rdi
	leal	-1(%rcx), %r8d
	movl	%ecx, %eax
	andl	$7, %eax
	je	.LBB9_4
# BB#2:                                 # %.prol.preheader
	negl	%eax
	.p2align	4, 0x90
.LBB9_3:                                # =>This Inner Loop Header: Depth=1
	decl	%ecx
	movzbl	(%rdx), %r9d
	incq	%rdx
	movb	%r9b, (%rsi)
	addq	%rdi, %rsi
	incl	%eax
	jne	.LBB9_3
.LBB9_4:                                # %.prol.loopexit
	cmpl	$7, %r8d
	jb	.LBB9_6
	.p2align	4, 0x90
.LBB9_5:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rdx), %eax
	movb	%al, (%rsi)
	movzbl	1(%rdx), %eax
	movb	%al, (%rsi,%rdi)
	addq	%rdi, %rsi
	movzbl	2(%rdx), %eax
	movb	%al, (%rdi,%rsi)
	addq	%rdi, %rsi
	movzbl	3(%rdx), %eax
	movb	%al, (%rdi,%rsi)
	addq	%rdi, %rsi
	movzbl	4(%rdx), %eax
	movb	%al, (%rdi,%rsi)
	addq	%rdi, %rsi
	movzbl	5(%rdx), %eax
	movb	%al, (%rdi,%rsi)
	addq	%rdi, %rsi
	movzbl	6(%rdx), %eax
	movb	%al, (%rdi,%rsi)
	addq	%rdi, %rsi
	movzbl	7(%rdx), %eax
	movb	%al, (%rdi,%rsi)
	leaq	(%rsi,%rdi), %rsi
	addq	%rdi, %rsi
	addl	$-8, %ecx
	leaq	8(%rdx), %rdx
	jne	.LBB9_5
.LBB9_6:                                # %._crit_edge
	retq
.Lfunc_end9:
	.size	image_unpack_3_spread, .Lfunc_end9-image_unpack_3_spread
	.cfi_endproc

	.globl	image_render_skip
	.p2align	4, 0x90
	.type	image_render_skip,@function
image_render_skip:                      # @image_render_skip
	.cfi_startproc
# BB#0:
	movl	%ecx, %eax
	retq
.Lfunc_end10:
	.size	image_render_skip, .Lfunc_end10-image_render_skip
	.cfi_endproc

	.globl	image_render_color
	.p2align	4, 0x90
	.type	image_render_color,@function
image_render_color:                     # @image_render_color
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 56
	subq	$360, %rsp              # imm = 0x168
.Lcfi59:
	.cfi_def_cfa_offset 416
.Lcfi60:
	.cfi_offset %rbx, -56
.Lcfi61:
	.cfi_offset %r12, -48
.Lcfi62:
	.cfi_offset %r13, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	72(%rdi), %r14
	movq	24(%rdi), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	32(%rdi), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	40(%rdi), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	48(%rdi), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	160(%rdi), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movslq	12(%rdi), %rsi
	movl	%edx, %eax
	leaq	(%r15,%rax), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	%rsi, %rdx
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	subq	%rsi, %rcx
	cmpl	$0, 100(%rdi)
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	movq	168(%rdi), %r13
	movb	(%rcx), %cl
	notb	%cl
	movb	%cl, (%r15,%rax)
                                        # implicit-def: %EAX
	movl	%eax, 12(%rsp)          # 4-byte Spill
                                        # implicit-def: %EBP
                                        # implicit-def: %EBX
	jne	.LBB11_2
# BB#1:
	leaq	2048(%r13), %rbx
	shrq	$12, %rbx
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	2048(%rax,%r13), %rax
	shrq	$12, %rax
	xorl	%ebp, %ebp
	subl	%ebx, %eax
	cmovsl	%eax, %ebp
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
.LBB11_2:                               # %.lr.ph
	movw	$0, 20(%rsp)
	movl	$0, 16(%rsp)
	leaq	16(%rsp), %r12
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	gx_color_from_rgb
	leaq	184(%rsp), %rcx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rcx, %r12
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	gx_color_render
	addl	%ebx, %ebp
	movl	%ebp, 84(%rsp)          # 4-byte Spill
	movq	%r14, 64(%rsp)          # 8-byte Spill
	leaq	8(%r14), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	152(%rsp), %r9
	xorl	%eax, %eax
	movq	%r13, 56(%rsp)          # 8-byte Spill
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	%r8, %rbx
	.p2align	4, 0x90
.LBB11_3:                               # =>This Inner Loop Header: Depth=1
	cmpl	$4, 144(%rsp)           # 4-byte Folded Reload
	jne	.LBB11_5
# BB#4:                                 #   in Loop: Header=BB11_3 Depth=1
	movzbl	3(%r15), %edi
	xorl	$255, %edi
	movzbl	(%r15), %ecx
	xorl	$255, %ecx
	imull	%edi, %ecx
	movl	$2155905153, %ebp       # imm = 0x80808081
	imulq	%rbp, %rcx
	shrq	$39, %rcx
	movzbl	1(%r15), %edx
	xorl	$255, %edx
	imull	%edi, %edx
	imulq	%rbp, %rdx
	shrq	$39, %rdx
	movzbl	2(%r15), %esi
	xorl	$255, %esi
	imull	%edi, %esi
	imulq	%rbp, %rsi
	shrq	$39, %rsi
	movl	$4, %edi
	jmp	.LBB11_6
	.p2align	4, 0x90
.LBB11_5:                               #   in Loop: Header=BB11_3 Depth=1
	movb	(%r15), %cl
	movb	1(%r15), %dl
	movb	2(%r15), %sil
	movl	$3, %edi
.LBB11_6:                               #   in Loop: Header=BB11_3 Depth=1
	addq	%rdi, %r15
	movzbl	%sil, %esi
	movq	%rsi, %rdi
	shlq	$16, %rdi
	movzbl	%dl, %ebp
	movq	%rbp, %rdx
	shlq	$8, %rdx
	orq	%rdi, %rdx
	movzbl	%cl, %ecx
	leaq	(%rcx,%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.LBB11_8
# BB#7:                                 #   in Loop: Header=BB11_3 Depth=1
	movq	%rax, %rcx
	movq	%r9, %rbp
	movq	%r12, %r14
	jmp	.LBB11_21
	.p2align	4, 0x90
.LBB11_8:                               #   in Loop: Header=BB11_3 Depth=1
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movl	%ecx, %eax
	shll	$8, %eax
	orl	%ecx, %eax
	movw	%ax, 16(%rsp)
	movl	%ebp, %eax
	shll	$8, %eax
	orl	%ebp, %eax
	movw	%ax, 18(%rsp)
	movl	%esi, %eax
	shll	$8, %eax
	orl	%esi, %eax
	movw	%ax, 20(%rsp)
	xorl	%eax, %eax
	movq	%r13, %r12
	movq	%r9, %r13
	leaq	16(%rsp), %r14
	movq	%r14, %rdi
	movq	%r8, %rbp
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	callq	gx_color_from_rgb
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r13, %r14
	movq	%r12, %r13
	movq	%r14, %rsi
	movq	64(%rsp), %rdx          # 8-byte Reload
	callq	gx_color_render
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	%rbp, %r8
	movq	184(%rsp), %rax
	cmpq	152(%rsp), %rax
	jne	.LBB11_15
# BB#9:                                 #   in Loop: Header=BB11_3 Depth=1
	movl	200(%rsp), %eax
	cmpl	168(%rsp), %eax
	jne	.LBB11_15
# BB#10:                                #   in Loop: Header=BB11_3 Depth=1
	testl	%eax, %eax
	je	.LBB11_14
# BB#11:                                #   in Loop: Header=BB11_3 Depth=1
	cmpq	72(%rsp), %r15          # 8-byte Folded Reload
	ja	.LBB11_15
# BB#12:                                #   in Loop: Header=BB11_3 Depth=1
	movq	160(%rsp), %rax
	cmpq	%rax, 192(%rsp)
	je	.LBB11_13
	jmp	.LBB11_15
.LBB11_14:                              #   in Loop: Header=BB11_3 Depth=1
	cmpq	72(%rsp), %r15          # 8-byte Folded Reload
	jbe	.LBB11_13
	.p2align	4, 0x90
.LBB11_15:                              #   in Loop: Header=BB11_3 Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 104(%rax)
	je	.LBB11_19
# BB#16:                                #   in Loop: Header=BB11_3 Depth=1
	movq	%r14, 120(%rsp)         # 8-byte Spill
	leaq	216(%rsp), %rbp
	movq	%rbp, %rdi
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	%r8, %r12
	movq	%rbx, %r14
	callq	gx_path_init
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r14), %r9
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r13), %rax
	movq	%rax, (%rsp)
	movq	%rbp, %rdi
	movq	%r12, %rsi
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	%r14, %rcx
	movq	%r13, %r8
	callq	gx_path_add_pgram
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB11_18
# BB#17:                                # %.thread
                                        #   in Loop: Header=BB11_3 Depth=1
	movl	$-1, %ecx
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	leaq	216(%rsp), %rbp
	movq	%rbp, %rdi
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	movq	64(%rsp), %rdx          # 8-byte Reload
	callq	gx_fill_path
	movq	%rbp, %rdi
	callq	gx_path_release
	movq	%r14, %r8
	movq	%r13, 56(%rsp)          # 8-byte Spill
	movq	%rbx, %rbp
	movq	%r14, %rbx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	120(%rsp), %r14         # 8-byte Reload
	jmp	.LBB11_21
.LBB11_19:                              #   in Loop: Header=BB11_3 Depth=1
	addq	$2048, %r8              # imm = 0x800
	shrq	$12, %r8
	leaq	2048(%rbx), %rax
	shrq	$12, %rax
	movl	%eax, %ecx
	subl	%r8d, %ecx
	cmovsl	%eax, %r8d
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	xorl	%eax, %eax
	movl	%r8d, %edi
	movl	84(%rsp), %esi          # 4-byte Reload
	movl	12(%rsp), %ecx          # 4-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %r8
	movq	64(%rsp), %r9           # 8-byte Reload
	callq	gz_fill_rectangle
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %r8
	movq	%r13, 56(%rsp)          # 8-byte Spill
	jmp	.LBB11_20
.LBB11_13:                              #   in Loop: Header=BB11_3 Depth=1
	movq	%r14, %rbp
	movq	40(%rsp), %r14          # 8-byte Reload
.LBB11_20:                              # %.thread204
                                        #   in Loop: Header=BB11_3 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
.LBB11_21:                              # %.thread204
                                        #   in Loop: Header=BB11_3 Depth=1
	addq	136(%rsp), %rbx         # 8-byte Folded Reload
	addq	128(%rsp), %r13         # 8-byte Folded Reload
	cmpq	72(%rsp), %r15          # 8-byte Folded Reload
	movq	%r14, %r12
	movq	%rbp, %r9
	movq	%rcx, %rax
	jbe	.LBB11_3
# BB#22:
	movl	$1, %ebp
	jmp	.LBB11_23
.LBB11_18:
	leaq	216(%rsp), %rdi
	callq	gx_path_release
.LBB11_23:                              # %.loopexit
	movl	%ebp, %eax
	addq	$360, %rsp              # imm = 0x168
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	image_render_color, .Lfunc_end11-image_render_color
	.cfi_endproc

	.globl	image_render_direct
	.p2align	4, 0x90
	.type	image_render_direct,@function
image_render_direct:                    # @image_render_direct
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi69:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi70:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi72:
	.cfi_def_cfa_offset 96
.Lcfi73:
	.cfi_offset %rbx, -56
.Lcfi74:
	.cfi_offset %r12, -48
.Lcfi75:
	.cfi_offset %r13, -40
.Lcfi76:
	.cfi_offset %r14, -32
.Lcfi77:
	.cfi_offset %r15, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movl	%edx, %r13d
	movq	72(%rdi), %rdx
	movq	160(%rdi), %r8
	movq	168(%rdi), %r9
	movq	48(%rdi), %rbx
	addq	%r9, %rbx
	shrq	$12, %r8
	shrq	$12, %r9
	shrq	$12, %rbx
	movl	%ebx, %ecx
	subl	%r9d, %ecx
	movq	448(%rdx), %rdx
	movq	(%rdx), %rbp
	movq	8(%rbp), %rdx
	movq	72(%rdx), %r10
	movq	184(%rdi), %r15
	movq	8344(%rdi), %rax
	cmpl	$1, %ecx
	jne	.LBB12_2
# BB#1:
	leal	7(%r13), %ecx
	shrl	$3, %ecx
	xorl	%edx, %edx
	movq	%rbp, %rdi
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	%rax
.Lcfi79:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi80:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi81:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi82:
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$32, %rsp
.Lcfi83:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB12_6
.LBB12_2:
	movl	%ecx, %r12d
	negl	%r12d
	cmovll	%ecx, %r12d
	testl	%ecx, %ecx
	cmovsl	%ebx, %r9d
	movl	$1, %r14d
	testl	%r12d, %r12d
	jle	.LBB12_6
# BB#3:                                 # %.lr.ph
	leal	7(%r13), %ecx
	shrl	$3, %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB12_4:                               # =>This Inner Loop Header: Depth=1
	movq	%r9, 32(%rsp)           # 8-byte Spill
	xorl	%edx, %edx
	movq	%rbp, %rdi
	movl	12(%rsp), %ecx          # 4-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	%rax
.Lcfi84:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi85:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi86:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi87:
	.cfi_adjust_cfa_offset 8
	movq	%r10, %r14
	movq	%rax, %rbx
	callq	*%r14
	movq	%rbx, %rax
	movq	64(%rsp), %r9           # 8-byte Reload
	movq	%r14, %r10
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
	addq	$32, %rsp
.Lcfi88:
	.cfi_adjust_cfa_offset -32
	incl	%r9d
	decl	%r12d
	jne	.LBB12_4
# BB#5:
	movl	$1, %r14d
.LBB12_6:                               # %.loopexit
	movl	%r14d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	image_render_direct, .Lfunc_end12-image_render_direct
	.cfi_endproc

	.globl	image_render_mono
	.p2align	4, 0x90
	.type	image_render_mono,@function
image_render_mono:                      # @image_render_mono
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi92:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi93:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 56
	subq	$376, %rsp              # imm = 0x178
.Lcfi95:
	.cfi_def_cfa_offset 432
.Lcfi96:
	.cfi_offset %rbx, -56
.Lcfi97:
	.cfi_offset %r12, -48
.Lcfi98:
	.cfi_offset %r13, -40
.Lcfi99:
	.cfi_offset %r14, -32
.Lcfi100:
	.cfi_offset %r15, -24
.Lcfi101:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	184(%rdi), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	8344(%rdi), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	72(%rdi), %rdx
	movq	24(%rdi), %rsi
	movq	32(%rdi), %r8
	movl	100(%rdi), %ebp
	movq	264(%rdx), %rax
	movq	448(%rdx), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	movq	8(%rcx), %rcx
	movq	56(%rcx), %rcx
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	movq	16(%rax), %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	24(%rax), %r11
	movq	32(%rax), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	40(%rax), %rcx
	movl	%ebp, 84(%rsp)          # 4-byte Spill
	testl	%ebp, %ebp
	movq	40(%rdi), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movq	48(%rdi), %rbp
	movq	160(%rdi), %r10
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	movq	168(%rdi), %r9
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	312(%rdx), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movzbl	(%r14), %r12d
	je	.LBB13_2
# BB#1:
                                        # implicit-def: %EAX
	movl	%eax, 20(%rsp)          # 4-byte Spill
                                        # implicit-def: %EAX
                                        # implicit-def: %EDX
	movq	%rdx, 72(%rsp)          # 8-byte Spill
                                        # implicit-def: %RDX
	movq	%r9, %r13
.LBB13_11:
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movq	%r11, 152(%rsp)         # 8-byte Spill
	leal	-1(%rbx), %ecx
	movb	(%r14,%rcx), %cl
	notb	%cl
	movl	%ebx, %edx
	movb	%cl, (%r14,%rdx)
	movw	$257, 32(%rsp)          # imm = 0x101
	testl	%ebx, %ebx
	js	.LBB13_66
# BB#12:                                # %.lr.ph
	movq	%rbp, 184(%rsp)         # 8-byte Spill
	movq	72(%rsp), %rcx          # 8-byte Reload
	addl	%eax, %ecx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	incl	%ebx
	movl	$-2, %r11d
	movq	%r10, %rbp
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movq	%r8, 56(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB13_13:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%r14), %eax
	cmpl	%r12d, %eax
	je	.LBB13_65
# BB#14:                                #   in Loop: Header=BB13_13 Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 104(%rax)
	je	.LBB13_49
# BB#15:                                #   in Loop: Header=BB13_13 Depth=1
	cmpl	%r11d, %r12d
	je	.LBB13_25
# BB#16:                                #   in Loop: Header=BB13_13 Depth=1
	testl	%r12d, %r12d
	je	.LBB13_17
# BB#19:                                #   in Loop: Header=BB13_13 Depth=1
	cmpq	$-1, 128(%rsp)          # 8-byte Folded Reload
	jne	.LBB13_21
# BB#20:                                #   in Loop: Header=BB13_13 Depth=1
	movl	$255, %r11d
	cmpl	$255, %r12d
	je	.LBB13_64
	jmp	.LBB13_21
.LBB13_49:                              #   in Loop: Header=BB13_13 Depth=1
	addq	$2048, %r10             # imm = 0x800
	shrq	$12, %r10
	leaq	2048(%rbp), %r15
	shrq	$12, %r15
	movl	%r15d, %ecx
	subl	%r10d, %ecx
	jle	.LBB13_51
# BB#50:                                #   in Loop: Header=BB13_13 Depth=1
	movl	%r10d, %r15d
	jmp	.LBB13_53
.LBB13_51:                              #   in Loop: Header=BB13_13 Depth=1
	testl	%ecx, %ecx
	je	.LBB13_64
# BB#52:                                #   in Loop: Header=BB13_13 Depth=1
	negl	%ecx
.LBB13_53:                              #   in Loop: Header=BB13_13 Depth=1
	testl	%r12d, %r12d
	movq	136(%rsp), %r9          # 8-byte Reload
	je	.LBB13_56
# BB#54:                                #   in Loop: Header=BB13_13 Depth=1
	cmpl	$255, %r12d
	jne	.LBB13_58
# BB#55:                                #   in Loop: Header=BB13_13 Depth=1
	movq	128(%rsp), %r9          # 8-byte Reload
.LBB13_56:                              #   in Loop: Header=BB13_13 Depth=1
	cmpq	$-1, %r9
	je	.LBB13_64
# BB#57:                                #   in Loop: Header=BB13_13 Depth=1
	movq	176(%rsp), %rdi         # 8-byte Reload
	movl	%r15d, %esi
	movq	72(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r11d, %r15d
	movl	20(%rsp), %r8d          # 4-byte Reload
	callq	*168(%rsp)              # 8-byte Folded Reload
	movl	%r15d, %r11d
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB13_64
.LBB13_17:                              #   in Loop: Header=BB13_13 Depth=1
	cmpq	$-1, 136(%rsp)          # 8-byte Folded Reload
	je	.LBB13_18
.LBB13_21:                              #   in Loop: Header=BB13_13 Depth=1
	movslq	%r12d, %rax
	shlq	$5, %rax
	movq	112(%rsp), %rcx         # 8-byte Reload
	leaq	184(%rcx,%rax), %rdx
	cmpl	$0, 200(%rcx,%rax)
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	js	.LBB13_22
# BB#23:                                #   in Loop: Header=BB13_13 Depth=1
	xorl	%eax, %eax
	movq	%rdx, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%r10, %r15
	movq	%r9, 8(%rsp)            # 8-byte Spill
	callq	gx_color_load
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	%r15, %r10
	jmp	.LBB13_24
.LBB13_58:                              #   in Loop: Header=BB13_13 Depth=1
	cmpl	%r11d, %r12d
	je	.LBB13_63
# BB#59:                                #   in Loop: Header=BB13_13 Depth=1
	movslq	%r12d, %rax
	shlq	$5, %rax
	movq	112(%rsp), %rsi         # 8-byte Reload
	leaq	184(%rsi,%rax), %rdx
	cmpl	$0, 200(%rsi,%rax)
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	js	.LBB13_60
# BB#61:                                #   in Loop: Header=BB13_13 Depth=1
	xorl	%eax, %eax
	movq	%rdx, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	gx_color_load
	jmp	.LBB13_62
.LBB13_22:                              #   in Loop: Header=BB13_13 Depth=1
	movl	%r12d, %eax
	shll	$8, %eax
	addl	%r12d, %eax
	movw	%ax, 28(%rsp)
	movw	%ax, 26(%rsp)
	movw	%ax, 24(%rsp)
	movw	%ax, 30(%rsp)
	xorl	%eax, %eax
	leaq	24(%rsp), %rdi
	movq	%rdx, %rsi
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	%r10, 8(%rsp)           # 8-byte Spill
	movq	%r9, %r15
	callq	gx_color_render
	movq	%r15, %r9
	movq	8(%rsp), %r10           # 8-byte Reload
.LBB13_24:                              #   in Loop: Header=BB13_13 Depth=1
	movq	56(%rsp), %r8           # 8-byte Reload
	movl	%r12d, %r11d
.LBB13_25:                              #   in Loop: Header=BB13_13 Depth=1
	leaq	232(%rsp), %r12
	movq	%r12, %rdi
	cmpl	$0, 84(%rsp)            # 4-byte Folded Reload
	je	.LBB13_26
# BB#34:                                #   in Loop: Header=BB13_13 Depth=1
	movl	%r11d, 52(%rsp)         # 4-byte Spill
	movq	120(%rsp), %rsi         # 8-byte Reload
	movq	%r10, %r15
	movq	%rbx, %r12
	movq	%rdi, %rbx
	movq	%r9, 8(%rsp)            # 8-byte Spill
	callq	gx_path_init
	movq	192(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbp), %r9
	movq	184(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r13), %rax
	movq	%rax, (%rsp)
	movq	%rbx, %rdi
	movq	%r12, %rbx
	movq	%r15, %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rbp, %rcx
	movq	%r13, %r8
.LBB13_35:                              #   in Loop: Header=BB13_13 Depth=1
	callq	gx_path_add_pgram
	movl	%eax, %r15d
	testl	%r15d, %r15d
	js	.LBB13_36
# BB#37:                                #   in Loop: Header=BB13_13 Depth=1
	cmpl	$0, 84(%rsp)            # 4-byte Folded Reload
	leaq	232(%rsp), %r15
	je	.LBB13_46
# BB#38:                                #   in Loop: Header=BB13_13 Depth=1
	movq	%r15, %rdi
	leaq	200(%rsp), %rsi
	callq	gx_path_bbox
	movq	216(%rsp), %rcx
	movl	$5, %eax
	cmpq	104(%rsp), %rcx         # 8-byte Folded Reload
	jle	.LBB13_44
# BB#39:                                #   in Loop: Header=BB13_13 Depth=1
	movq	200(%rsp), %rdx
	cmpq	96(%rsp), %rdx          # 8-byte Folded Reload
	jge	.LBB13_44
# BB#40:                                #   in Loop: Header=BB13_13 Depth=1
	addq	$2048, %rdx             # imm = 0x800
	shrq	$12, %rdx
	addq	$2048, %rcx             # imm = 0x800
	shrq	$12, %rcx
	cmpl	%ecx, %edx
	je	.LBB13_44
# BB#41:                                #   in Loop: Header=BB13_13 Depth=1
	movq	224(%rsp), %rcx
	cmpq	152(%rsp), %rcx         # 8-byte Folded Reload
	jle	.LBB13_44
# BB#42:                                #   in Loop: Header=BB13_13 Depth=1
	movq	208(%rsp), %rdx
	cmpq	144(%rsp), %rdx         # 8-byte Folded Reload
	jge	.LBB13_44
# BB#43:                                #   in Loop: Header=BB13_13 Depth=1
	addq	$2048, %rdx             # imm = 0x800
	shrq	$12, %rdx
	addq	$2048, %rcx             # imm = 0x800
	shrq	$12, %rcx
	xorl	%eax, %eax
	cmpl	%ecx, %edx
	sete	%al
	leal	(%rax,%rax,4), %eax
.LBB13_44:                              #   in Loop: Header=BB13_13 Depth=1
	movl	%eax, %ecx
	andb	$7, %cl
	cmpb	$5, %cl
	je	.LBB13_47
# BB#45:                                #   in Loop: Header=BB13_13 Depth=1
	testb	%cl, %cl
	jne	.LBB13_48
.LBB13_46:                              #   in Loop: Header=BB13_13 Depth=1
	movl	$-1, %ecx
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	88(%rsp), %rsi          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	callq	gx_fill_path
.LBB13_47:                              #   in Loop: Header=BB13_13 Depth=1
	movq	%r15, %rdi
	callq	gx_path_release
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsp), %r8           # 8-byte Reload
	movl	52(%rsp), %r11d         # 4-byte Reload
	jmp	.LBB13_64
.LBB13_26:                              #   in Loop: Header=BB13_13 Depth=1
	leaq	2048(%rbp), %r15
	andq	$-4096, %r15            # imm = 0xF000
	addq	$2048, %r10             # imm = 0x800
	andq	$-4096, %r10            # imm = 0xF000
	cmpq	%r15, %r10
	jne	.LBB13_28
# BB#27:                                #   in Loop: Header=BB13_13 Depth=1
	movq	64(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB13_64
.LBB13_28:                              #   in Loop: Header=BB13_13 Depth=1
	movq	64(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	js	.LBB13_31
# BB#29:                                #   in Loop: Header=BB13_13 Depth=1
	cmpq	104(%rsp), %r15         # 8-byte Folded Reload
	jle	.LBB13_64
# BB#30:                                #   in Loop: Header=BB13_13 Depth=1
	cmpq	96(%rsp), %r10          # 8-byte Folded Reload
	jl	.LBB13_33
	jmp	.LBB13_64
.LBB13_48:                              #   in Loop: Header=BB13_13 Depth=1
	testl	%eax, %eax
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsp), %r8           # 8-byte Reload
	movl	52(%rsp), %r11d         # 4-byte Reload
	je	.LBB13_64
	jmp	.LBB13_66
.LBB13_18:                              #   in Loop: Header=BB13_13 Depth=1
	xorl	%r11d, %r11d
	jmp	.LBB13_64
.LBB13_31:                              #   in Loop: Header=BB13_13 Depth=1
	cmpq	96(%rsp), %r15          # 8-byte Folded Reload
	jge	.LBB13_64
# BB#32:                                #   in Loop: Header=BB13_13 Depth=1
	cmpq	104(%rsp), %r10         # 8-byte Folded Reload
	jle	.LBB13_64
.LBB13_33:                              #   in Loop: Header=BB13_13 Depth=1
	movl	%r11d, 52(%rsp)         # 4-byte Spill
	movq	120(%rsp), %rsi         # 8-byte Reload
	movq	%r10, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r12
	callq	gx_path_init
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movq	%r12, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	%r13, %r8
	movq	%r15, %r9
	jmp	.LBB13_35
.LBB13_60:                              #   in Loop: Header=BB13_13 Depth=1
	movl	%r12d, %eax
	shll	$8, %eax
	addl	%r12d, %eax
	movw	%ax, 28(%rsp)
	movw	%ax, 26(%rsp)
	movw	%ax, 24(%rsp)
	movw	%ax, 30(%rsp)
	xorl	%eax, %eax
	leaq	24(%rsp), %rdi
	movq	%rdx, %rsi
	movq	40(%rsp), %rdx          # 8-byte Reload
	callq	gx_color_render
.LBB13_62:                              #   in Loop: Header=BB13_13 Depth=1
	movl	8(%rsp), %ecx           # 4-byte Reload
	movl	%r12d, %r11d
.LBB13_63:                              #   in Loop: Header=BB13_13 Depth=1
	xorl	%eax, %eax
	movl	%r15d, %edi
	movq	72(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%ecx, %edx
	movl	%r11d, %r15d
	movl	20(%rsp), %ecx          # 4-byte Reload
	movq	88(%rsp), %r8           # 8-byte Reload
	movq	40(%rsp), %r9           # 8-byte Reload
	callq	gz_fill_rectangle
	movl	%r15d, %r11d
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB13_64:                              #   in Loop: Header=BB13_13 Depth=1
	movzbl	(%r14), %r12d
	movq	%rbp, %r10
	movq	%r13, %r9
.LBB13_65:                              #   in Loop: Header=BB13_13 Depth=1
	incq	%r14
	addq	%rsi, %rbp
	addq	%r8, %r13
	decl	%ebx
	testl	%ebx, %ebx
	jg	.LBB13_13
.LBB13_66:
	movl	$1, %r15d
.LBB13_67:                              # %.loopexit
	movl	%r15d, %eax
	addq	$376, %rsp              # imm = 0x178
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_2:
	leaq	2048(%rbp,%r9), %rdx
	leaq	2048(%r9), %r13
	movq	%r13, %rdi
	shrq	$12, %rdi
	movq	%rdx, %rax
	shrq	$12, %rax
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	subl	%edi, %eax
	jle	.LBB13_4
# BB#3:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 20(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
.LBB13_6:
	andq	$-4096, %rdx            # imm = 0xF000
	andq	$-4096, %r13            # imm = 0xF000
	movl	$1, %r15d
	testq	%rbp, %rbp
	jle	.LBB13_9
# BB#7:
	cmpq	%r11, %rdx
	jle	.LBB13_67
# BB#8:
	cmpq	%rcx, %r13
	jl	.LBB13_11
	jmp	.LBB13_67
.LBB13_4:
	movl	$1, %r15d
	testl	%eax, %eax
	jns	.LBB13_67
# BB#5:
	movl	%eax, %edi
	negl	%edi
	movl	%edi, 20(%rsp)          # 4-byte Spill
	jmp	.LBB13_6
.LBB13_9:
	cmpq	%r11, %r13
	jle	.LBB13_67
# BB#10:
	cmpq	%rcx, %rdx
	jl	.LBB13_11
	jmp	.LBB13_67
.LBB13_36:                              # %.thread277
	leaq	232(%rsp), %rdi
	callq	gx_path_release
	jmp	.LBB13_67
.Lfunc_end13:
	.size	image_render_mono, .Lfunc_end13-image_render_mono
	.cfi_endproc

	.globl	gs_image_next
	.p2align	4, 0x90
	.type	gs_image_next,@function
gs_image_next:                          # @gs_image_next
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi102:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi103:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi105:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi106:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi107:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi108:
	.cfi_def_cfa_offset 80
.Lcfi109:
	.cfi_offset %rbx, -56
.Lcfi110:
	.cfi_offset %r12, -48
.Lcfi111:
	.cfi_offset %r13, -40
.Lcfi112:
	.cfi_offset %r14, -32
.Lcfi113:
	.cfi_offset %r15, -24
.Lcfi114:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rdi, %rbx
	movl	(%rbx), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	92(%rbx), %r14d
	movl	152(%rbx), %edx
	movslq	144(%rbx), %rax
	testq	%rax, %rax
	je	.LBB14_1
# BB#2:
	movl	$-23, %ebp
	cmpl	%r15d, 148(%rbx)
	je	.LBB14_3
	jmp	.LBB14_19
.LBB14_1:
	movl	%r15d, 148(%rbx)
.LBB14_3:
	movq	%rsi, 112(%rbx,%rax,8)
	movl	144(%rbx), %eax
	incl	%eax
	movl	%eax, 144(%rbx)
	xorl	%ebp, %ebp
	cmpl	16(%rbx), %eax
	jne	.LBB14_19
# BB#4:
	movl	$0, 144(%rbx)
	testl	%r15d, %r15d
	je	.LBB14_18
# BB#5:                                 # %.lr.ph93
	xorl	%ecx, %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movl	%r14d, 16(%rsp)         # 4-byte Spill
	jmp	.LBB14_6
	.p2align	4, 0x90
.LBB14_20:                              # %.thread.backedge._crit_edge
                                        #   in Loop: Header=BB14_6 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	addl	%ebp, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	16(%rbx), %eax
.LBB14_6:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_8 Depth 2
	movl	%r14d, %ebp
	subl	%edx, %ebp
	cmpl	%ebp, %r15d
	cmovbl	%r15d, %ebp
	movl	$3, %ecx
	subl	8(%rbx), %ecx
	movl	%edx, 20(%rsp)          # 4-byte Spill
	movl	%edx, %r13d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r13d
	testl	%eax, %eax
	jle	.LBB14_9
# BB#7:                                 # %.lr.ph
                                        #   in Loop: Header=BB14_6 Depth=1
	imull	%eax, %r13d
	addq	80(%rbx), %r13
	movl	(%rsp), %r12d           # 4-byte Reload
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB14_8:                               #   Parent Loop BB14_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r13,%r14), %rsi
	movq	112(%rbx,%r14,8), %rdx
	addq	%r12, %rdx
	movq	%rbx, %rdi
	movl	%ebp, %ecx
	callq	*56(%rbx)
	incq	%r14
	movslq	16(%rbx), %rax
	cmpq	%rax, %r14
	jl	.LBB14_8
.LBB14_9:                               # %._crit_edge
                                        #   in Loop: Header=BB14_6 Depth=1
	movl	20(%rsp), %edx          # 4-byte Reload
	addl	%ebp, %edx
	movl	16(%rsp), %r14d         # 4-byte Reload
	cmpl	%r14d, %edx
	jne	.LBB14_16
# BB#10:                                #   in Loop: Header=BB14_6 Depth=1
	movq	80(%rbx), %rsi
	movl	12(%rbx), %edx
	imull	12(%rsp), %edx          # 4-byte Folded Reload
	movl	$1, %ecx
	movq	%rbx, %rdi
	callq	*64(%rbx)
	testl	%eax, %eax
	js	.LBB14_11
# BB#13:                                #   in Loop: Header=BB14_6 Depth=1
	movl	176(%rbx), %eax
	incl	%eax
	movl	%eax, 176(%rbx)
	cmpl	4(%rbx), %eax
	je	.LBB14_14
# BB#15:                                #   in Loop: Header=BB14_6 Depth=1
	movdqu	40(%rbx), %xmm0
	movdqu	160(%rbx), %xmm1
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 160(%rbx)
	xorl	%edx, %edx
.LBB14_16:                              # %.thread.backedge
                                        #   in Loop: Header=BB14_6 Depth=1
	subl	%ebp, %r15d
	jne	.LBB14_20
# BB#17:
	xorl	%ebp, %ebp
.LBB14_18:                              # %.thread._crit_edge
	movl	%edx, 152(%rbx)
	jmp	.LBB14_19
.LBB14_11:
	movl	%eax, %ebp
	jmp	.LBB14_12
.LBB14_14:
	movl	$1, %ebp
.LBB14_12:
	movq	80(%rbx), %rdi
	movl	88(%rbx), %esi
	movl	$1, %edx
	movl	$.L.str, %ecx
	callq	gs_free
.LBB14_19:
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	gs_image_next, .Lfunc_end14-gs_image_next
	.cfi_endproc

	.type	gs_image_enum_sizeof,@object # @gs_image_enum_sizeof
	.data
	.globl	gs_image_enum_sizeof
	.p2align	2
gs_image_enum_sizeof:
	.long	8376                    # 0x20b8
	.size	gs_image_enum_sizeof, 4

	.type	map_4_to_32,@object     # @map_4_to_32
	.globl	map_4_to_32
	.p2align	4
map_4_to_32:
	.long	0                       # 0x0
	.long	4278190080              # 0xff000000
	.long	16711680                # 0xff0000
	.long	4294901760              # 0xffff0000
	.long	65280                   # 0xff00
	.long	4278255360              # 0xff00ff00
	.long	16776960                # 0xffff00
	.long	4294967040              # 0xffffff00
	.long	255                     # 0xff
	.long	4278190335              # 0xff0000ff
	.long	16711935                # 0xff00ff
	.long	4294902015              # 0xffff00ff
	.long	65535                   # 0xffff
	.long	4278255615              # 0xff00ffff
	.long	16777215                # 0xffffff
	.long	4294967295              # 0xffffffff
	.size	map_4_to_32, 64

	.type	map_4_to_16,@object     # @map_4_to_16
	.globl	map_4_to_16
	.p2align	4
map_4_to_16:
	.short	0                       # 0x0
	.short	21760                   # 0x5500
	.short	43520                   # 0xaa00
	.short	65280                   # 0xff00
	.short	85                      # 0x55
	.short	21845                   # 0x5555
	.short	43605                   # 0xaa55
	.short	65365                   # 0xff55
	.short	170                     # 0xaa
	.short	21930                   # 0x55aa
	.short	43690                   # 0xaaaa
	.short	65450                   # 0xffaa
	.short	255                     # 0xff
	.short	22015                   # 0x55ff
	.short	43775                   # 0xaaff
	.short	65535                   # 0xffff
	.size	map_4_to_16, 32

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"image buffer"
	.size	.L.str, 13

	.type	image_init.procs,@object # @image_init.procs
	.section	.rodata,"a",@progbits
	.p2align	4
image_init.procs:
	.quad	image_unpack_0
	.quad	image_unpack_1
	.quad	image_unpack_2
	.quad	image_unpack_3
	.size	image_init.procs, 32

	.type	image_init.spread_procs,@object # @image_init.spread_procs
	.p2align	4
image_init.spread_procs:
	.quad	image_unpack_0_spread
	.quad	image_unpack_1_spread
	.quad	image_unpack_2
	.quad	image_unpack_3_spread
	.size	image_init.spread_procs, 32

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
.Lswitch.table:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.size	.Lswitch.table, 32

	.type	.Lswitch.table.1,@object # @switch.table.1
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table.1:
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	4                       # 0x4
	.size	.Lswitch.table.1, 36

	.type	.Lswitch.table.2,@object # @switch.table.2
	.p2align	4
.Lswitch.table.2:
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.size	.Lswitch.table.2, 36


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
