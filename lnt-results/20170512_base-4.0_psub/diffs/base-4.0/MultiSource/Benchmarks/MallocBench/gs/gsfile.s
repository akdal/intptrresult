	.text
	.file	"gsfile.bc"
	.globl	gs_writeppmfile
	.p2align	4, 0x90
	.type	gs_writeppmfile,@function
gs_writeppmfile:                        # @gs_writeppmfile
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbp
	callq	mem_bytes_per_scan_line
	movl	%eax, %ebx
	movl	28(%rbp), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movl	48(%rbp), %ebp
	leal	(%rbx,%rbx,2), %edi
	movl	$1, %esi
	movl	$.L.str, %edx
	movl	%edi, 16(%rsp)          # 4-byte Spill
	callq	gs_malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_1
# BB#2:
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movl	$-23, %r14d
	leal	-1(%rbp), %eax
	cmpl	$31, %eax
	ja	.LBB0_37
# BB#3:
	movl	$.L.str.1, %esi
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_4:
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 40(%rax)
	movl	$.L.str.2, %eax
	movl	$.L.str.3, %esi
	cmovneq	%rax, %rsi
	jmp	.LBB0_7
.LBB0_1:
	movl	$-25, %r14d
	jmp	.LBB0_38
.LBB0_5:
	movl	$.L.str.4, %esi
	jmp	.LBB0_7
.LBB0_6:
	movl	$.L.str.5, %esi
.LBB0_7:
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	24(%rax), %edx
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	20(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %ecx
	callq	fprintf
	testl	%ebx, %ebx
	jle	.LBB0_37
# BB#8:                                 # %.lr.ph128
	movq	40(%rsp), %r14          # 8-byte Reload
	movslq	%r14d, %rax
	leaq	(%r15,%rax), %r12
	cmpl	$8, %ebp
	movq	%r13, 56(%rsp)          # 8-byte Spill
	jne	.LBB0_24
# BB#9:                                 # %.lr.ph128.split.us.preheader
	movq	%r15, 8(%rsp)           # 8-byte Spill
	leaq	(%r12,%rax), %rcx
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rax), %r15
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_10:                               # %.lr.ph128.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_13 Depth 2
                                        #     Child Loop BB0_18 Depth 2
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	movl	%r13d, %esi
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rdx
	movl	%r14d, %ecx
	callq	mem_copy_scan_lines
	cmpl	$0, 40(%rbp)
	movq	192(%rbp), %rbp
	je	.LBB0_17
# BB#11:                                #   in Loop: Header=BB0_10 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	testl	%r14d, %r14d
	movl	16(%rsp), %ecx          # 4-byte Reload
	jle	.LBB0_20
# BB#12:                                # %.lr.ph122.us.preheader
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_13:                               # %.lr.ph122.us
                                        #   Parent Loop BB0_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx), %edx
	incq	%rcx
	leaq	(%rdx,%rdx,2), %rdx
	movzbl	(%rbp,%rdx), %ebx
	movb	%bl, (%rax)
	movzbl	1(%rbp,%rdx), %ebx
	movb	%bl, 1(%rax)
	movzbl	2(%rbp,%rdx), %edx
	movb	%dl, 2(%rax)
	addq	$3, %rax
	cmpq	%r15, %rcx
	jb	.LBB0_13
# BB#14:                                #   in Loop: Header=BB0_10 Depth=1
	movl	16(%rsp), %ecx          # 4-byte Reload
	jmp	.LBB0_20
	.p2align	4, 0x90
.LBB0_17:                               # %.preheader.us
                                        #   in Loop: Header=BB0_10 Depth=1
	testl	%r14d, %r14d
	movl	%r14d, %ecx
	jle	.LBB0_20
	.p2align	4, 0x90
.LBB0_18:                               # %.lr.ph125.us
                                        #   Parent Loop BB0_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbx), %ecx
	leaq	(%rcx,%rcx,2), %rcx
	movzbl	(%rbp,%rcx), %ecx
	movb	%cl, (%rbx)
	incq	%rbx
	cmpq	%r12, %rbx
	jb	.LBB0_18
# BB#19:                                #   in Loop: Header=BB0_10 Depth=1
	movl	%r14d, %ecx
.LBB0_20:                               # %.loopexit.us
                                        #   in Loop: Header=BB0_10 Depth=1
	movslq	%ecx, %r14
	movl	$1, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r14, %rdx
	movq	56(%rsp), %rcx          # 8-byte Reload
	callq	fwrite
	cmpq	%r14, %rax
	jb	.LBB0_21
# BB#15:                                #   in Loop: Header=BB0_10 Depth=1
	incl	%r13d
	cmpl	20(%rsp), %r13d         # 4-byte Folded Reload
	movq	40(%rsp), %r14          # 8-byte Reload
	jl	.LBB0_10
# BB#16:
	xorl	%r14d, %r14d
	movq	8(%rsp), %r15           # 8-byte Reload
	jmp	.LBB0_37
.LBB0_24:                               # %.lr.ph128.split.preheader
	leaq	4(%r15), %rax
	cmpq	%rax, %r12
	cmovaq	%r12, %rax
	movq	%r15, %rcx
	notq	%rcx
	addq	%rax, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	shrq	$2, %rcx
	leaq	(%rcx,%rcx,2), %rax
	leaq	3(%r15,%rax), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	incl	%ecx
	andl	$3, %ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	negq	%rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_25:                               # %.lr.ph128.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_29 Depth 2
                                        #     Child Loop BB0_32 Depth 2
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%r14d, %esi
	movq	%r15, %rdx
	movq	40(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %ecx
	callq	mem_copy_scan_lines
	cmpl	$32, %ebp
	movl	%ebx, %edx
	jne	.LBB0_35
# BB#26:                                #   in Loop: Header=BB0_25 Depth=1
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	movq	%r15, %rdx
	jle	.LBB0_34
# BB#27:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_25 Depth=1
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	movq	%r15, %rax
	movq	%r15, %rcx
	je	.LBB0_31
# BB#28:                                # %.lr.ph.prol.preheader
                                        #   in Loop: Header=BB0_25 Depth=1
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	%r15, %rax
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB0_29:                               # %.lr.ph.prol
                                        #   Parent Loop BB0_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	1(%rcx), %ebx
	movb	%bl, (%rax)
	movzbl	2(%rcx), %ebx
	movb	%bl, 1(%rax)
	movzbl	3(%rcx), %ebx
	addq	$4, %rcx
	movb	%bl, 2(%rax)
	addq	$3, %rax
	incq	%rdx
	jne	.LBB0_29
# BB#30:                                #   in Loop: Header=BB0_25 Depth=1
	movq	56(%rsp), %r13          # 8-byte Reload
.LBB0_31:                               # %.lr.ph.prol.loopexit
                                        #   in Loop: Header=BB0_25 Depth=1
	cmpq	$12, 24(%rsp)           # 8-byte Folded Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	jb	.LBB0_34
	.p2align	4, 0x90
.LBB0_32:                               # %.lr.ph
                                        #   Parent Loop BB0_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	1(%rcx), %edx
	movb	%dl, (%rax)
	movzbl	2(%rcx), %edx
	movb	%dl, 1(%rax)
	movzbl	3(%rcx), %edx
	movb	%dl, 2(%rax)
	movzbl	5(%rcx), %edx
	movb	%dl, 3(%rax)
	movzbl	6(%rcx), %edx
	movb	%dl, 4(%rax)
	movzbl	7(%rcx), %edx
	movb	%dl, 5(%rax)
	movzbl	9(%rcx), %edx
	movb	%dl, 6(%rax)
	movzbl	10(%rcx), %edx
	movb	%dl, 7(%rax)
	movzbl	11(%rcx), %edx
	movb	%dl, 8(%rax)
	movzbl	13(%rcx), %edx
	movb	%dl, 9(%rax)
	movzbl	14(%rcx), %edx
	movb	%dl, 10(%rax)
	movzbl	15(%rcx), %edx
	addq	$16, %rcx
	movb	%dl, 11(%rax)
	addq	$12, %rax
	cmpq	%r12, %rcx
	jb	.LBB0_32
# BB#33:                                #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
.LBB0_34:                               # %._crit_edge
                                        #   in Loop: Header=BB0_25 Depth=1
	subl	%r15d, %edx
.LBB0_35:                               #   in Loop: Header=BB0_25 Depth=1
	movq	%rbp, %rbx
	movslq	%edx, %rbp
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rbp, %rdx
	movq	%r13, %rcx
	callq	fwrite
	cmpq	%rbp, %rax
	jb	.LBB0_36
# BB#22:                                #   in Loop: Header=BB0_25 Depth=1
	incl	%r14d
	cmpl	20(%rsp), %r14d         # 4-byte Folded Reload
	movq	%rbx, %rbp
	jl	.LBB0_25
# BB#23:
	xorl	%r14d, %r14d
	jmp	.LBB0_37
.LBB0_21:
	movl	$-12, %r14d
	movq	8(%rsp), %r15           # 8-byte Reload
	jmp	.LBB0_37
.LBB0_36:
	movl	$-12, %r14d
.LBB0_37:                               # %.loopexit116
	movl	$1, %edx
	movl	$.L.str, %ecx
	movq	%r15, %rdi
	movl	16(%rsp), %esi          # 4-byte Reload
	callq	gs_free
.LBB0_38:
	movl	%r14d, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	gs_writeppmfile, .Lfunc_end0-gs_writeppmfile
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_7
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_4
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_5
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_37
	.quad	.LBB0_6

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"ppm file buffer"
	.size	.L.str, 16

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"P4\n# Ghostscript 1 bit mono image dump\n%d %d\n"
	.size	.L.str.1, 46

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"P6\n# Ghostscript 8 bit mapped color image dump\n%d %d\n255\n"
	.size	.L.str.2, 58

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"P5\n# Ghostscript 8 bit gray scale image dump\n%d %d\n255\n"
	.size	.L.str.3, 56

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"P6\n# Ghostscript 24 bit color image dump\n%d %d\n255\n"
	.size	.L.str.4, 52

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"P6\n# Ghostscript 32 bit color image dump\n%d %d\n255\n"
	.size	.L.str.5, 52


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
