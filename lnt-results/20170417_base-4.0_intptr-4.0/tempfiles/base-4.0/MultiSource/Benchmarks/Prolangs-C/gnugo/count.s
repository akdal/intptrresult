	.text
	.file	"count.bc"
	.globl	count
	.p2align	4, 0x90
	.type	count,@function
count:                                  # @count
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movl	%edi, %r12d
	movslq	%r12d, %rax
	leal	1(%rax), %ecx
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	movslq	%ecx, %rcx
	leal	-1(%rax), %edx
	movl	%edx, 24(%rsp)          # 4-byte Spill
	movslq	%edx, %rdx
	movslq	%esi, %rsi
	imulq	$19, %rax, %rbp
	addq	%rsi, %rbp
	imulq	$19, %rcx, %rcx
	addq	%rsi, %rcx
	imulq	$19, %rdx, %rdx
	addq	%rsi, %rdx
	movl	%esi, %r14d
	leaq	-1(%r14), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB0_1:                                # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	testl	%r12d, %r12d
	movb	$0, ml(%rbp,%r15)
	je	.LBB0_9
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	movzbl	p(%rdx,%r15), %eax
	testl	%eax, %eax
	je	.LBB0_3
.LBB0_5:                                #   in Loop: Header=BB0_1 Depth=1
	cmpl	%ebx, %eax
	jne	.LBB0_8
# BB#6:                                 #   in Loop: Header=BB0_1 Depth=1
	cmpb	$0, ml(%rdx,%r15)
	je	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_1 Depth=1
	leal	(%r14,%r15), %esi
	movl	24(%rsp), %edi          # 4-byte Reload
	movl	%ebx, %edx
	callq	count
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	$18, %r12d
	jne	.LBB0_9
	jmp	.LBB0_15
	.p2align	4, 0x90
.LBB0_3:                                #   in Loop: Header=BB0_1 Depth=1
	cmpb	$0, ml(%rdx,%r15)
	je	.LBB0_5
# BB#4:                                 #   in Loop: Header=BB0_1 Depth=1
	incl	lib(%rip)
	movb	$0, ml(%rdx,%r15)
.LBB0_8:                                #   in Loop: Header=BB0_1 Depth=1
	cmpl	$18, %r12d
	je	.LBB0_15
.LBB0_9:                                #   in Loop: Header=BB0_1 Depth=1
	movzbl	p(%rcx,%r15), %eax
	testl	%eax, %eax
	je	.LBB0_10
.LBB0_12:                               #   in Loop: Header=BB0_1 Depth=1
	cmpl	%ebx, %eax
	jne	.LBB0_15
# BB#13:                                #   in Loop: Header=BB0_1 Depth=1
	cmpb	$0, ml(%rcx,%r15)
	je	.LBB0_15
# BB#14:                                #   in Loop: Header=BB0_1 Depth=1
	leal	(%r14,%r15), %esi
	movl	28(%rsp), %edi          # 4-byte Reload
	movl	%ebx, %edx
	callq	count
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB0_15
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_1 Depth=1
	cmpb	$0, ml(%rcx,%r15)
	je	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_1 Depth=1
	incl	lib(%rip)
	movb	$0, ml(%rcx,%r15)
	.p2align	4, 0x90
.LBB0_15:                               #   in Loop: Header=BB0_1 Depth=1
	leaq	(%r14,%r15), %rax
	testl	%eax, %eax
	je	.LBB0_23
# BB#16:                                #   in Loop: Header=BB0_1 Depth=1
	leaq	-18(%r14,%r15), %r13
	movzbl	p-1(%rbp,%r15), %eax
	testl	%eax, %eax
	je	.LBB0_17
.LBB0_19:                               #   in Loop: Header=BB0_1 Depth=1
	cmpl	%ebx, %eax
	jne	.LBB0_22
# BB#20:                                #   in Loop: Header=BB0_1 Depth=1
	cmpb	$0, ml-1(%rbp,%r15)
	je	.LBB0_22
# BB#21:                                #   in Loop: Header=BB0_1 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r15), %esi
	movl	%r12d, %edi
	movl	%ebx, %edx
	callq	count
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	testl	%r13d, %r13d
	jne	.LBB0_23
	jmp	.LBB0_28
	.p2align	4, 0x90
.LBB0_17:                               #   in Loop: Header=BB0_1 Depth=1
	cmpb	$0, ml-1(%rbp,%r15)
	je	.LBB0_19
# BB#18:                                #   in Loop: Header=BB0_1 Depth=1
	incl	lib(%rip)
	movb	$0, ml-1(%rbp,%r15)
.LBB0_22:                               #   in Loop: Header=BB0_1 Depth=1
	testl	%r13d, %r13d
	je	.LBB0_28
.LBB0_23:                               #   in Loop: Header=BB0_1 Depth=1
	movzbl	p+1(%rbp,%r15), %eax
	testl	%eax, %eax
	jne	.LBB0_26
# BB#24:                                #   in Loop: Header=BB0_1 Depth=1
	cmpb	$0, ml+1(%rbp,%r15)
	jne	.LBB0_25
.LBB0_26:                               #   in Loop: Header=BB0_1 Depth=1
	cmpl	%ebx, %eax
	jne	.LBB0_28
# BB#27:                                #   in Loop: Header=BB0_1 Depth=1
	cmpb	$0, ml+1(%rbp,%r15)
	leaq	1(%r15), %r15
	jne	.LBB0_1
	jmp	.LBB0_28
.LBB0_25:
	incl	lib(%rip)
	movb	$0, ml+1(%r15,%rbp)
.LBB0_28:                               # %.loopexit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	count, .Lfunc_end0-count
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
