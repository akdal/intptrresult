	.text
	.file	"erc_api.bc"
	.globl	ercInit
	.p2align	4, 0x90
	.type	ercInit,@function
ercInit:                                # @ercInit
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebx
	movl	%edi, %ebp
	movq	erc_errorVar(%rip), %rdi
	callq	ercClose
	imull	%ebx, %ebp
	sarl	$6, %ebp
	movslq	%ebp, %rdi
	movl	$24, %esi
	callq	calloc
	movq	%rax, erc_object_list(%rip)
	testq	%rax, %rax
	jne	.LBB0_2
# BB#1:
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB0_2:
	movl	$72, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB0_4
# BB#3:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB0_4:                                # %ercOpen.exit
	testq	%rbx, %rbx
	movl	$0, (%rbx)
	movq	$0, 48(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	movups	%xmm0, 8(%rbx)
	movl	$0, 40(%rbx)
	movl	$1, 64(%rbx)
	movq	%rbx, erc_errorVar(%rip)
	je	.LBB0_6
# BB#5:
	movl	%r14d, 64(%rbx)
.LBB0_6:                                # %ercSetErrorConcealment.exit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	ercInit, .Lfunc_end0-ercInit
	.cfi_endproc

	.globl	ercClose
	.p2align	4, 0x90
	.type	ercClose,@function
ercClose:                               # @ercClose
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB1_4
# BB#1:
	cmpq	$0, 8(%rbx)
	je	.LBB1_3
# BB#2:
	movq	32(%rbx), %rdi
	callq	free
	movq	8(%rbx), %rdi
	callq	free
	movq	16(%rbx), %rdi
	callq	free
	movq	24(%rbx), %rdi
	callq	free
	movq	48(%rbx), %rdi
	callq	free
.LBB1_3:
	movq	%rbx, %rdi
	callq	free
.LBB1_4:
	movq	erc_object_list(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_6
# BB#5:
	callq	free
	movq	$0, erc_object_list(%rip)
.LBB1_6:
	popq	%rbx
	retq
.Lfunc_end1:
	.size	ercClose, .Lfunc_end1-ercClose
	.cfi_endproc

	.globl	ercOpen
	.p2align	4, 0x90
	.type	ercOpen,@function
ercOpen:                                # @ercOpen
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 16
.Lcfi9:
	.cfi_offset %rbx, -16
	movl	$72, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB2_2
# BB#1:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB2_2:
	movl	$0, (%rbx)
	movq	$0, 48(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	movups	%xmm0, 8(%rbx)
	movl	$0, 40(%rbx)
	movl	$1, 64(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end2:
	.size	ercOpen, .Lfunc_end2-ercOpen
	.cfi_endproc

	.globl	ercSetErrorConcealment
	.p2align	4, 0x90
	.type	ercSetErrorConcealment,@function
ercSetErrorConcealment:                 # @ercSetErrorConcealment
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB3_2
# BB#1:
	movl	%esi, 64(%rdi)
.LBB3_2:
	retq
.Lfunc_end3:
	.size	ercSetErrorConcealment, .Lfunc_end3-ercSetErrorConcealment
	.cfi_endproc

	.globl	ercReset
	.p2align	4, 0x90
	.type	ercReset,@function
ercReset:                               # @ercReset
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 64
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %ebx
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB4_31
# BB#1:
	cmpl	$0, 64(%r14)
	je	.LBB4_31
# BB#2:
	cmpl	%ebx, (%r14)
	je	.LBB4_5
# BB#3:
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB4_5
# BB#4:
	callq	free
	movq	$0, 8(%r14)
	movq	48(%r14), %rdi
	callq	free
	movq	$0, 48(%r14)
	movq	16(%r14), %rdi
	callq	free
	movq	$0, 16(%r14)
	movq	24(%r14), %rdi
	callq	free
	movq	$0, 24(%r14)
	movq	32(%r14), %rdi
	callq	free
	movq	$0, 32(%r14)
.LBB4_5:                                # %._crit_edge96
	movq	8(%r14), %rax
	testq	%rax, %rax
	je	.LBB4_6
# BB#17:
	movq	48(%r14), %rdi
	movq	%rax, 48(%r14)
	movq	%rdi, 8(%r14)
	leal	(,%rbx,4), %eax
	movslq	%eax, %r12
	shlq	$2, %r12
	movslq	%ebx, %rbp
	shlq	$2, %rbp
	leaq	4(%r14), %r13
	jmp	.LBB4_18
.LBB4_6:
	movslq	%r15d, %rax
	shlq	$2, %rax
	leaq	(%rax,%rax,2), %rbp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, 32(%r14)
	testq	%rax, %rax
	jne	.LBB4_8
# BB#7:
	movl	$.L.str.2, %edi
	callq	no_mem_exit
	movq	32(%r14), %rax
.LBB4_8:
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rbp, %rdx
	callq	memset
	movl	%r15d, 4(%r14)
	leal	(,%rbx,4), %eax
	movslq	%eax, %r12
	shlq	$2, %r12
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	jne	.LBB4_10
# BB#9:
	movl	$.L.str.3, %edi
	callq	no_mem_exit
.LBB4_10:
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, 48(%r14)
	testq	%rax, %rax
	jne	.LBB4_12
# BB#11:
	movl	$.L.str.4, %edi
	callq	no_mem_exit
.LBB4_12:
	movslq	%ebx, %rbp
	shlq	$2, %rbp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, 16(%r14)
	testq	%rax, %rax
	jne	.LBB4_14
# BB#13:
	movl	$.L.str.5, %edi
	callq	no_mem_exit
.LBB4_14:
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, 24(%r14)
	testq	%rax, %rax
	jne	.LBB4_16
# BB#15:
	movl	$.L.str.6, %edi
	callq	no_mem_exit
.LBB4_16:
	leaq	4(%r14), %r13
	movl	%ebx, (%r14)
	movq	8(%r14), %rdi
.LBB4_18:
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	16(%r14), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	24(%r14), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	cmpl	%r15d, 4(%r14)
	jne	.LBB4_20
# BB#19:                                # %._crit_edge95
	movslq	%r15d, %rax
	shlq	$2, %rax
	leaq	(%rax,%rax,2), %rbp
	jmp	.LBB4_23
.LBB4_20:
	movq	32(%r14), %rdi
	callq	free
	movslq	%r15d, %rax
	shlq	$2, %rax
	leaq	(%rax,%rax,2), %rbp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, 32(%r14)
	testq	%rax, %rax
	jne	.LBB4_22
# BB#21:
	movl	$.L.str.2, %edi
	callq	no_mem_exit
.LBB4_22:
	movl	%r15d, (%r13)
.LBB4_23:
	movq	32(%r14), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movslq	4(%r14), %rax
	testq	%rax, %rax
	jle	.LBB4_30
# BB#24:                                # %.lr.ph
	movq	32(%r14), %rdx
	decl	%ebx
	testb	$1, %al
	jne	.LBB4_26
# BB#25:
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	jne	.LBB4_28
	jmp	.LBB4_30
.LBB4_26:
	movl	$1, 8(%rdx)
	movl	$0, (%rdx)
	movl	%ebx, 4(%rdx)
	movl	$1, %ecx
	cmpl	$1, %eax
	je	.LBB4_30
.LBB4_28:                               # %.lr.ph.new
	leaq	(%rcx,%rcx,2), %rsi
	leaq	20(%rdx,%rsi,4), %rdx
	.p2align	4, 0x90
.LBB4_29:                               # =>This Inner Loop Header: Depth=1
	movl	$1, -12(%rdx)
	movl	$0, -20(%rdx)
	movl	%ebx, -16(%rdx)
	movl	$1, (%rdx)
	movl	$0, -8(%rdx)
	movl	%ebx, -4(%rdx)
	addq	$2, %rcx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	jl	.LBB4_29
.LBB4_30:                               # %._crit_edge
	movl	$0, 40(%r14)
	movl	$0, 60(%r14)
.LBB4_31:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	ercReset, .Lfunc_end4-ercReset
	.cfi_endproc

	.globl	ercStartSegment
	.p2align	4, 0x90
	.type	ercStartSegment,@function
ercStartSegment:                        # @ercStartSegment
	.cfi_startproc
# BB#0:
	testq	%rcx, %rcx
	je	.LBB5_3
# BB#1:
	cmpl	$0, 64(%rcx)
	je	.LBB5_3
# BB#2:
	movl	$0, 56(%rcx)
	movq	32(%rcx), %rax
	movslq	%esi, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movl	$0, 8(%rax,%rcx,4)
	movl	%edi, (%rax,%rcx,4)
.LBB5_3:
	retq
.Lfunc_end5:
	.size	ercStartSegment, .Lfunc_end5-ercStartSegment
	.cfi_endproc

	.globl	ercStopSegment
	.p2align	4, 0x90
	.type	ercStopSegment,@function
ercStopSegment:                         # @ercStopSegment
	.cfi_startproc
# BB#0:
	testq	%rcx, %rcx
	je	.LBB6_3
# BB#1:
	cmpl	$0, 64(%rcx)
	je	.LBB6_3
# BB#2:
	movq	32(%rcx), %rax
	movslq	%esi, %rdx
	leaq	(%rdx,%rdx,2), %rdx
	movl	%edi, 4(%rax,%rdx,4)
	incl	40(%rcx)
.LBB6_3:
	retq
.Lfunc_end6:
	.size	ercStopSegment, .Lfunc_end6-ercStopSegment
	.cfi_endproc

	.globl	ercMarkCurrSegmentLost
	.p2align	4, 0x90
	.type	ercMarkCurrSegmentLost,@function
ercMarkCurrSegmentLost:                 # @ercMarkCurrSegmentLost
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB7_8
# BB#1:
	cmpl	$0, 64(%rsi)
	je	.LBB7_8
# BB#2:
	movslq	40(%rsi), %rax
	decq	%rax
	cmpl	$0, 56(%rsi)
	jne	.LBB7_4
# BB#3:
	incl	60(%rsi)
	movl	$1, 56(%rsi)
.LBB7_4:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 48
.Lcfi28:
	.cfi_offset %rbx, -48
.Lcfi29:
	.cfi_offset %r12, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	32(%rsi), %r8
	leaq	(%rax,%rax,2), %r9
	movl	(%r8,%r9,4), %r10d
	cmpl	4(%r8,%r9,4), %r10d
	jg	.LBB7_7
# BB#5:                                 # %.lr.ph
	movslq	%r10d, %rcx
	leaq	4(%r8,%r9,4), %r11
	movq	8(%rsi), %rbx
	movq	16(%rsi), %r14
	movl	%edi, %r12d
	sarl	$4, %r12d
	sarl	$3, %edi
	movq	24(%rsi), %r15
	decq	%rcx
	.p2align	4, 0x90
.LBB7_6:                                # =>This Inner Loop Header: Depth=1
	movl	%r10d, %eax
	cltd
	idivl	%r12d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rax,%rax), %esi
	imull	%edi, %esi
	leal	(%rsi,%rdx,2), %ebp
	movslq	%ebp, %rbp
	movl	$1, (%rbx,%rbp,4)
	leal	1(%rsi,%rdx,2), %esi
	movslq	%esi, %rsi
	movl	$1, (%rbx,%rsi,4)
	leal	1(%rax,%rax), %eax
	imull	%edi, %eax
	leal	(%rax,%rdx,2), %eax
	cltq
	movl	$1, (%rbx,%rax,4)
	movl	$1, 4(%rbx,%rax,4)
	movl	$1, 4(%r14,%rcx,4)
	movl	$1, 4(%r15,%rcx,4)
	movslq	(%r11), %rax
	incq	%rcx
	incl	%r10d
	cmpq	%rax, %rcx
	jl	.LBB7_6
.LBB7_7:                                # %._crit_edge
	movl	$1, 8(%r8,%r9,4)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
.LBB7_8:
	retq
.Lfunc_end7:
	.size	ercMarkCurrSegmentLost, .Lfunc_end7-ercMarkCurrSegmentLost
	.cfi_endproc

	.globl	ercMarkCurrSegmentOK
	.p2align	4, 0x90
	.type	ercMarkCurrSegmentOK,@function
ercMarkCurrSegmentOK:                   # @ercMarkCurrSegmentOK
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB8_6
# BB#1:
	cmpl	$0, 64(%rsi)
	je	.LBB8_6
# BB#2:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 48
.Lcfi38:
	.cfi_offset %rbx, -48
.Lcfi39:
	.cfi_offset %r12, -40
.Lcfi40:
	.cfi_offset %r14, -32
.Lcfi41:
	.cfi_offset %r15, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movslq	40(%rsi), %rax
	decq	%rax
	movq	32(%rsi), %r8
	leaq	(%rax,%rax,2), %r9
	movl	(%r8,%r9,4), %r10d
	cmpl	4(%r8,%r9,4), %r10d
	jg	.LBB8_5
# BB#3:                                 # %.lr.ph
	movslq	%r10d, %rcx
	leaq	4(%r8,%r9,4), %r11
	movq	8(%rsi), %rbx
	movq	16(%rsi), %r14
	movl	%edi, %r12d
	sarl	$4, %r12d
	sarl	$3, %edi
	movq	24(%rsi), %r15
	decq	%rcx
	.p2align	4, 0x90
.LBB8_4:                                # =>This Inner Loop Header: Depth=1
	movl	%r10d, %eax
	cltd
	idivl	%r12d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rax,%rax), %esi
	imull	%edi, %esi
	leal	(%rsi,%rdx,2), %ebp
	movslq	%ebp, %rbp
	movl	$3, (%rbx,%rbp,4)
	leal	1(%rsi,%rdx,2), %esi
	movslq	%esi, %rsi
	movl	$3, (%rbx,%rsi,4)
	leal	1(%rax,%rax), %eax
	imull	%edi, %eax
	leal	(%rax,%rdx,2), %eax
	cltq
	movl	$3, (%rbx,%rax,4)
	movl	$3, 4(%rbx,%rax,4)
	movl	$3, 4(%r14,%rcx,4)
	movl	$3, 4(%r15,%rcx,4)
	movslq	(%r11), %rax
	incq	%rcx
	incl	%r10d
	cmpq	%rax, %rcx
	jl	.LBB8_4
.LBB8_5:                                # %._crit_edge
	movl	$0, 8(%r8,%r9,4)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
.LBB8_6:
	retq
.Lfunc_end8:
	.size	ercMarkCurrSegmentOK, .Lfunc_end8-ercMarkCurrSegmentOK
	.cfi_endproc

	.globl	ercMarkCurrMBConcealed
	.p2align	4, 0x90
	.type	ercMarkCurrMBConcealed,@function
ercMarkCurrMBConcealed:                 # @ercMarkCurrMBConcealed
	.cfi_startproc
# BB#0:
	movl	%edx, %r8d
	testq	%rcx, %rcx
	je	.LBB9_10
# BB#1:
	cmpl	$0, 64(%rcx)
	je	.LBB9_10
# BB#2:
	xorl	%eax, %eax
	testl	%esi, %esi
	cmovnsl	%esi, %eax
	shrl	$31, %esi
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	je	.LBB9_6
# BB#3:
	cmpl	$1, %eax
	je	.LBB9_8
# BB#4:
	cmpl	$2, %eax
	jne	.LBB9_10
# BB#5:                                 # %._crit_edge
	movslq	%edi, %rax
	jmp	.LBB9_9
.LBB9_6:
	movq	8(%rcx), %r9
	movl	%r8d, %r10d
	sarl	$4, %r10d
	movl	%edi, %eax
	cltd
	idivl	%r10d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rax,%rax), %r10d
	sarl	$3, %r8d
	imull	%r8d, %r10d
	leal	(%r10,%rdx,2), %r11d
	movslq	%r11d, %r11
	movl	$2, (%r9,%r11,4)
	leal	1(%r10,%rdx,2), %r10d
	movslq	%r10d, %r10
	movl	$2, (%r9,%r10,4)
	leal	1(%rax,%rax), %eax
	imull	%r8d, %eax
	leal	(%rax,%rdx,2), %eax
	cltq
	movabsq	$8589934594, %rdx       # imm = 0x200000002
	movq	%rdx, (%r9,%rax,4)
	testl	%esi, %esi
	je	.LBB9_10
# BB#7:                                 # %.thread
	movq	16(%rcx), %rdx
	movslq	%edi, %rax
	movl	$2, (%rdx,%rax,4)
	jmp	.LBB9_9
.LBB9_8:
	movq	16(%rcx), %rdx
	movslq	%edi, %rax
	movl	$2, (%rdx,%rax,4)
	testl	%esi, %esi
	je	.LBB9_10
.LBB9_9:
	movq	24(%rcx), %rcx
	movl	$2, (%rcx,%rax,4)
.LBB9_10:
	retq
.Lfunc_end9:
	.size	ercMarkCurrMBConcealed, .Lfunc_end9-ercMarkCurrMBConcealed
	.cfi_endproc

	.type	erc_object_list,@object # @erc_object_list
	.bss
	.globl	erc_object_list
	.p2align	3
erc_object_list:
	.quad	0
	.size	erc_object_list, 8

	.type	erc_errorVar,@object    # @erc_errorVar
	.globl	erc_errorVar
	.p2align	3
erc_errorVar:
	.quad	0
	.size	erc_errorVar, 8

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"ercInit: erc_object_list"
	.size	.L.str, 25

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"ercOpen: errorVar"
	.size	.L.str.1, 18

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"ercReset: errorVar->segments"
	.size	.L.str.2, 29

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"ercReset: errorVar->yCondition"
	.size	.L.str.3, 31

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"ercReset: errorVar->prevFrameYCondition"
	.size	.L.str.4, 40

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"ercReset: errorVar->uCondition"
	.size	.L.str.5, 31

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"ercReset: errorVar->vCondition"
	.size	.L.str.6, 31

	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8
	.type	last_out_fs,@object     # @last_out_fs
	.comm	last_out_fs,8,8
	.type	pocs_in_dpb,@object     # @pocs_in_dpb
	.comm	pocs_in_dpb,400,16
	.type	erc_recfr,@object       # @erc_recfr
	.comm	erc_recfr,24,8
	.type	erc_mvperMB,@object     # @erc_mvperMB
	.comm	erc_mvperMB,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
