	.text
	.file	"vbrquantize.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4604930618986332160     # double 0.75
.LCPI0_1:
	.quad	-4616189618054758400    # double -1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_2:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	calc_sfb_ave_noise
	.p2align	4, 0x90
	.type	calc_sfb_ave_noise,@function
calc_sfb_ave_noise:                     # @calc_sfb_ave_noise
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movl	%edx, %r15d
	movq	%rsi, %r13
	movq	%rdi, %r12
	movsd	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	callq	pow
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movl	%r14d, %eax
	imull	%r15d, %eax
	testl	%eax, %eax
	jle	.LBB0_1
# BB#2:                                 # %.lr.ph.preheader
	movslq	%r15d, %r15
	movslq	%eax, %rbx
	xorpd	%xmm1, %xmm1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movsd	(%r13,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	divsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	callq	floor
	cvttsd2si	%xmm0, %eax
	cmpl	$8206, %eax             # imm = 0x200E
	jg	.LBB0_4
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=1
	movsd	(%r12,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	andpd	.LCPI0_2(%rip), %xmm1
	movslq	%eax, %rcx
	movsd	pow43(,%rcx,8), %xmm2   # xmm2 = mem[0],zero
	movsd	(%rsp), %xmm3           # 8-byte Reload
                                        # xmm3 = mem[0],zero
	mulsd	%xmm3, %xmm2
	movapd	%xmm1, %xmm0
	subsd	%xmm2, %xmm0
	cmpl	$8206, %eax             # imm = 0x200E
	je	.LBB0_8
# BB#6:                                 #   in Loop: Header=BB0_3 Depth=1
	movsd	pow43+8(,%rcx,8), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm3, %xmm2
	subsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm2
	movapd	.LCPI0_2(%rip), %xmm3   # xmm3 = [nan,nan]
	movapd	%xmm3, %xmm4
	andpd	%xmm4, %xmm2
	movapd	%xmm0, %xmm3
	andpd	%xmm4, %xmm3
	ucomisd	%xmm2, %xmm3
	jbe	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_3 Depth=1
	movapd	%xmm1, %xmm0
.LBB0_8:                                #   in Loop: Header=BB0_3 Depth=1
	mulsd	%xmm0, %xmm0
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	addq	%r15, %rbp
	cmpq	%rbx, %rbp
	jl	.LBB0_3
	jmp	.LBB0_9
.LBB0_1:
	xorpd	%xmm1, %xmm1
.LBB0_9:                                # %._crit_edge
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r14d, %xmm0
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	jmp	.LBB0_10
.LBB0_4:
	movsd	.LCPI0_1(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB0_10:                               # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	calc_sfb_ave_noise, .Lfunc_end0-calc_sfb_ave_noise
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4666723172467343360     # double 1.0E+4
.LCPI1_1:
	.quad	-4606619468846596096    # double -4.5
.LCPI1_2:
	.quad	-4596908582150078464    # double -20.5
.LCPI1_3:
	.quad	-4593038301220306944    # double -36.5
.LCPI1_4:
	.quad	4604930618986332160     # double 0.75
.LCPI1_5:
	.quad	4620693217682128896     # double 8
.LCPI1_6:
	.quad	-4602678819172646912    # double -8
.LCPI1_7:
	.quad	4616189618054758400     # double 4
.LCPI1_8:
	.quad	-4607182418800017408    # double -4
.LCPI1_9:
	.quad	4611686018427387904     # double 2
.LCPI1_10:
	.quad	-4611686018427387904    # double -2
.LCPI1_11:
	.quad	4607182418800017408     # double 1
.LCPI1_12:
	.quad	-4616189618054758400    # double -1
.LCPI1_13:
	.quad	4602678819172646912     # double 0.5
.LCPI1_14:
	.quad	-4620693217682128896    # double -0.5
.LCPI1_15:
	.quad	4629700416936869888     # double 32
.LCPI1_17:
	.quad	4576918229304087675     # double 0.01
.LCPI1_18:
	.quad	-4625196817309499392    # double -0.25
.LCPI1_19:
	.quad	0                       # double 0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_16:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	find_scalefac
	.p2align	4, 0x90
	.type	find_scalefac,@function
find_scalefac:                          # @find_scalefac
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 192
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebp
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r14
	cvtsi2sdl	%ebp, %xmm2
	imull	%edx, %ebp
	movslq	%edx, %r12
	movslq	%ebp, %r13
	testl	%r13d, %r13d
	movsd	%xmm2, 128(%rsp)        # 8-byte Spill
	jle	.LBB1_28
# BB#1:                                 # %.split.us.preheader
	movl	%ebp, 120(%rsp)         # 4-byte Spill
	movsd	.LCPI1_2(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI1_15(%rip), %xmm2  # xmm2 = mem[0],zero
	xorl	%ebp, %ebp
	movapd	%xmm0, 48(%rsp)         # 16-byte Spill
	jmp	.LBB1_2
	.p2align	4, 0x90
.LBB1_9:                                #   in Loop: Header=BB1_2 Depth=1
	ucomisd	40(%rsp), %xmm5         # 8-byte Folded Reload
	jbe	.LBB1_10
# BB#11:                                #   in Loop: Header=BB1_2 Depth=1
	movapd	48(%rsp), %xmm2         # 16-byte Reload
	movapd	%xmm2, %xmm1
	cmpeqsd	.LCPI1_0(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movapd	96(%rsp), %xmm3         # 16-byte Reload
	andpd	%xmm3, %xmm0
	andnpd	%xmm2, %xmm1
	orpd	%xmm0, %xmm1
	movsd	88(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	subsd	%xmm2, %xmm3
	movapd	%xmm3, %xmm0
	movapd	%xmm1, 48(%rsp)         # 16-byte Spill
	jmp	.LBB1_13
.LBB1_10:                               #   in Loop: Header=BB1_2 Depth=1
	movapd	96(%rsp), %xmm1         # 16-byte Reload
	movapd	%xmm1, %xmm0
	movsd	88(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	addsd	%xmm2, %xmm0
	movapd	%xmm1, 48(%rsp)         # 16-byte Spill
	jmp	.LBB1_13
	.p2align	4, 0x90
.LBB1_2:                                # %.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_3 Depth 2
	mulsd	.LCPI1_13(%rip), %xmm2
	movsd	%xmm2, 88(%rsp)         # 8-byte Spill
	movapd	%xmm1, 96(%rsp)         # 16-byte Spill
	movapd	%xmm1, %xmm0
	callq	exp2
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	.LCPI1_4(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	xorl	%r15d, %r15d
	xorpd	%xmm5, %xmm5
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph.i.us
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	%xmm5, 64(%rsp)         # 8-byte Spill
	movsd	(%rbx,%r15,8), %xmm0    # xmm0 = mem[0],zero
	divsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	callq	floor
	movsd	64(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	cvttsd2si	%xmm0, %eax
	cmpl	$8206, %eax             # imm = 0x200E
	jg	.LBB1_12
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=2
	movsd	(%r14,%r15,8), %xmm1    # xmm1 = mem[0],zero
	andpd	.LCPI1_16(%rip), %xmm1
	movslq	%eax, %rcx
	movsd	pow43(,%rcx,8), %xmm2   # xmm2 = mem[0],zero
	movsd	(%rsp), %xmm3           # 8-byte Reload
                                        # xmm3 = mem[0],zero
	mulsd	%xmm3, %xmm2
	movapd	%xmm1, %xmm0
	subsd	%xmm2, %xmm0
	cmpl	$8206, %eax             # imm = 0x200E
	je	.LBB1_7
# BB#5:                                 #   in Loop: Header=BB1_3 Depth=2
	movsd	pow43+8(,%rcx,8), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm3, %xmm2
	subsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm2
	movapd	.LCPI1_16(%rip), %xmm3  # xmm3 = [nan,nan]
	movapd	%xmm3, %xmm4
	andpd	%xmm4, %xmm2
	movapd	%xmm0, %xmm3
	andpd	%xmm4, %xmm3
	ucomisd	%xmm2, %xmm3
	jbe	.LBB1_7
# BB#6:                                 #   in Loop: Header=BB1_3 Depth=2
	movapd	%xmm1, %xmm0
.LBB1_7:                                #   in Loop: Header=BB1_3 Depth=2
	mulsd	%xmm0, %xmm0
	addsd	%xmm0, %xmm5
	addq	%r12, %r15
	cmpq	%r13, %r15
	jl	.LBB1_3
# BB#8:                                 # %calc_sfb_ave_noise.exit.loopexit.us
                                        #   in Loop: Header=BB1_2 Depth=1
	divsd	128(%rsp), %xmm5        # 8-byte Folded Reload
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm5, %xmm0
	jbe	.LBB1_9
.LBB1_12:                               # %calc_sfb_ave_noise.exit.thread.us
                                        #   in Loop: Header=BB1_2 Depth=1
	movsd	88(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movapd	96(%rsp), %xmm0         # 16-byte Reload
	addsd	%xmm2, %xmm0
.LBB1_13:                               #   in Loop: Header=BB1_2 Depth=1
	incl	%ebp
	cmpl	$7, %ebp
	movapd	%xmm0, %xmm1
	jne	.LBB1_2
# BB#14:                                # %.us-lcssa.us.loopexit
	addsd	%xmm2, %xmm2
	movl	120(%rsp), %ebp         # 4-byte Reload
	jmp	.LBB1_15
.LBB1_28:                               # %.split
	xorpd	%xmm3, %xmm3
	xorpd	%xmm1, %xmm1
	divsd	%xmm2, %xmm1
	movsd	.LCPI1_1(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm3
	movsd	%xmm1, 64(%rsp)         # 8-byte Spill
	jbe	.LBB1_30
# BB#29:
	movsd	.LCPI1_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, (%rsp)           # 16-byte Spill
	jmp	.LBB1_32
.LBB1_30:
	movsd	.LCPI1_2(%rip), %xmm3   # xmm3 = mem[0],zero
	movapd	%xmm3, (%rsp)           # 16-byte Spill
	ucomisd	40(%rsp), %xmm1         # 8-byte Folded Reload
	jbe	.LBB1_32
# BB#31:
	movsd	.LCPI1_3(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB1_32:                               # %.split.1140
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	callq	exp2
	movsd	.LCPI1_4(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	movsd	64(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	xorpd	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	jbe	.LBB1_33
# BB#43:                                # %calc_sfb_ave_noise.exit.thread.1
	movapd	16(%rsp), %xmm0         # 16-byte Reload
	addsd	.LCPI1_5(%rip), %xmm0
	movapd	(%rsp), %xmm1           # 16-byte Reload
	movapd	%xmm1, 16(%rsp)         # 16-byte Spill
	jmp	.LBB1_44
.LBB1_33:
	ucomisd	40(%rsp), %xmm0         # 8-byte Folded Reload
	jbe	.LBB1_34
# BB#42:
	movsd	.LCPI1_0(%rip), %xmm3   # xmm3 = mem[0],zero
	movapd	(%rsp), %xmm0           # 16-byte Reload
	cmpeqsd	%xmm0, %xmm3
	movapd	%xmm3, %xmm1
	movapd	16(%rsp), %xmm2         # 16-byte Reload
	andpd	%xmm2, %xmm1
	andnpd	%xmm0, %xmm3
	orpd	%xmm1, %xmm3
	addsd	.LCPI1_6(%rip), %xmm2
	movapd	%xmm2, %xmm0
	movapd	%xmm3, 16(%rsp)         # 16-byte Spill
	jmp	.LBB1_44
.LBB1_34:
	movsd	.LCPI1_5(%rip), %xmm0   # xmm0 = mem[0],zero
	addsd	16(%rsp), %xmm0         # 16-byte Folded Reload
.LBB1_44:                               # %.split.2141
	movapd	%xmm0, (%rsp)           # 16-byte Spill
	callq	exp2
	movsd	.LCPI1_4(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	xorpd	%xmm0, %xmm0
	movsd	64(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB1_45
# BB#48:                                # %calc_sfb_ave_noise.exit.thread.2
	movapd	(%rsp), %xmm1           # 16-byte Reload
	addsd	.LCPI1_7(%rip), %xmm1
	movapd	16(%rsp), %xmm0         # 16-byte Reload
	jmp	.LBB1_49
.LBB1_45:
	ucomisd	40(%rsp), %xmm1         # 8-byte Folded Reload
	movapd	16(%rsp), %xmm2         # 16-byte Reload
	jbe	.LBB1_46
# BB#47:
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	cmpeqsd	%xmm2, %xmm0
	movapd	%xmm0, %xmm1
	movapd	(%rsp), %xmm3           # 16-byte Reload
	andpd	%xmm3, %xmm1
	andnpd	%xmm2, %xmm0
	orpd	%xmm1, %xmm0
	addsd	.LCPI1_8(%rip), %xmm3
	movapd	%xmm3, %xmm1
.LBB1_49:                               # %.split.3142
	movapd	%xmm0, (%rsp)           # 16-byte Spill
	jmp	.LBB1_50
.LBB1_46:
	movsd	.LCPI1_7(%rip), %xmm1   # xmm1 = mem[0],zero
	addsd	(%rsp), %xmm1           # 16-byte Folded Reload
.LBB1_50:                               # %.split.3142
	movapd	%xmm1, 16(%rsp)         # 16-byte Spill
	movapd	%xmm1, %xmm0
	callq	exp2
	movsd	.LCPI1_4(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	xorpd	%xmm0, %xmm0
	movsd	64(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB1_51
# BB#54:                                # %calc_sfb_ave_noise.exit.thread.3
	movapd	16(%rsp), %xmm1         # 16-byte Reload
	addsd	.LCPI1_9(%rip), %xmm1
	movapd	(%rsp), %xmm0           # 16-byte Reload
	jmp	.LBB1_55
.LBB1_51:
	ucomisd	40(%rsp), %xmm1         # 8-byte Folded Reload
	jbe	.LBB1_52
# BB#53:
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	(%rsp), %xmm2           # 16-byte Reload
	cmpeqsd	%xmm2, %xmm0
	movapd	%xmm0, %xmm1
	movapd	16(%rsp), %xmm3         # 16-byte Reload
	andpd	%xmm3, %xmm1
	andnpd	%xmm2, %xmm0
	orpd	%xmm1, %xmm0
	addsd	.LCPI1_10(%rip), %xmm3
	movapd	%xmm3, %xmm1
.LBB1_55:                               # %.split.4143
	movapd	%xmm0, 16(%rsp)         # 16-byte Spill
	jmp	.LBB1_56
.LBB1_52:
	movsd	.LCPI1_9(%rip), %xmm1   # xmm1 = mem[0],zero
	addsd	16(%rsp), %xmm1         # 16-byte Folded Reload
.LBB1_56:                               # %.split.4143
	movapd	%xmm1, (%rsp)           # 16-byte Spill
	movapd	%xmm1, %xmm0
	callq	exp2
	movsd	.LCPI1_4(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	xorpd	%xmm0, %xmm0
	movsd	64(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB1_57
# BB#60:                                # %calc_sfb_ave_noise.exit.thread.4
	movapd	(%rsp), %xmm0           # 16-byte Reload
	addsd	.LCPI1_11(%rip), %xmm0
	movapd	16(%rsp), %xmm1         # 16-byte Reload
	movapd	%xmm1, (%rsp)           # 16-byte Spill
	jmp	.LBB1_61
.LBB1_57:
	ucomisd	40(%rsp), %xmm1         # 8-byte Folded Reload
	jbe	.LBB1_58
# BB#59:
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	16(%rsp), %xmm2         # 16-byte Reload
	cmpeqsd	%xmm2, %xmm0
	movapd	%xmm0, %xmm1
	movapd	(%rsp), %xmm3           # 16-byte Reload
	andpd	%xmm3, %xmm1
	andnpd	%xmm2, %xmm0
	orpd	%xmm1, %xmm0
	addsd	.LCPI1_12(%rip), %xmm3
	movapd	%xmm3, %xmm1
	movapd	%xmm0, (%rsp)           # 16-byte Spill
	movapd	%xmm1, %xmm0
	jmp	.LBB1_61
.LBB1_58:
	movsd	.LCPI1_11(%rip), %xmm0  # xmm0 = mem[0],zero
	addsd	(%rsp), %xmm0           # 16-byte Folded Reload
.LBB1_61:                               # %.split.5144
	movapd	%xmm0, 48(%rsp)         # 16-byte Spill
	callq	exp2
	movsd	.LCPI1_4(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	xorpd	%xmm0, %xmm0
	movsd	64(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB1_62
# BB#65:                                # %calc_sfb_ave_noise.exit.thread.5
	movapd	48(%rsp), %xmm1         # 16-byte Reload
	addsd	.LCPI1_13(%rip), %xmm1
	movapd	(%rsp), %xmm0           # 16-byte Reload
	jmp	.LBB1_66
.LBB1_62:
	ucomisd	40(%rsp), %xmm1         # 8-byte Folded Reload
	jbe	.LBB1_63
# BB#64:
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	(%rsp), %xmm3           # 16-byte Reload
	cmpeqsd	%xmm3, %xmm0
	movapd	%xmm0, %xmm1
	movapd	48(%rsp), %xmm2         # 16-byte Reload
	andpd	%xmm2, %xmm1
	andnpd	%xmm3, %xmm0
	orpd	%xmm1, %xmm0
	addsd	.LCPI1_14(%rip), %xmm2
	movapd	%xmm2, %xmm1
.LBB1_66:                               # %.split.6145
	movapd	%xmm0, 48(%rsp)         # 16-byte Spill
	jmp	.LBB1_67
.LBB1_63:
	movsd	.LCPI1_13(%rip), %xmm1  # xmm1 = mem[0],zero
	addsd	48(%rsp), %xmm1         # 16-byte Folded Reload
.LBB1_67:                               # %.split.6145
	movapd	%xmm1, 16(%rsp)         # 16-byte Spill
	movapd	%xmm1, %xmm0
	callq	exp2
	movsd	.LCPI1_4(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	xorpd	%xmm0, %xmm0
	movsd	64(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	movsd	.LCPI1_13(%rip), %xmm2  # xmm2 = mem[0],zero
	jbe	.LBB1_68
.LBB1_15:                               # %.us-lcssa.us
	movapd	48(%rsp), %xmm0         # 16-byte Reload
.LBB1_16:                               # %.us-lcssa.us
	movsd	.LCPI1_4(%rip), %xmm3   # xmm3 = mem[0],zero
	addsd	%xmm0, %xmm3
	movsd	.LCPI1_17(%rip), %xmm1  # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 120(%rsp)        # 8-byte Spill
	ucomisd	%xmm1, %xmm3
	movapd	%xmm3, %xmm1
	jbe	.LBB1_40
# BB#17:                                # %.lr.ph
	movapd	%xmm0, 48(%rsp)         # 16-byte Spill
	addsd	%xmm0, %xmm2
	testl	%ebp, %ebp
	movsd	%xmm2, 88(%rsp)         # 8-byte Spill
	jle	.LBB1_35
# BB#18:
	movsd	.LCPI1_18(%rip), %xmm3  # xmm3 = mem[0],zero
	.p2align	4, 0x90
.LBB1_19:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_20 Depth 2
	movapd	%xmm1, %xmm0
	subsd	%xmm2, %xmm0
	andpd	.LCPI1_16(%rip), %xmm0
	movapd	%xmm1, %xmm2
	addsd	%xmm3, %xmm1
	cmpltsd	.LCPI1_17(%rip), %xmm0
	andpd	%xmm0, %xmm1
	andnpd	%xmm2, %xmm0
	orpd	%xmm1, %xmm0
	movapd	%xmm0, 96(%rsp)         # 16-byte Spill
	callq	exp2
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	.LCPI1_4(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	xorl	%ebp, %ebp
	xorpd	%xmm5, %xmm5
	.p2align	4, 0x90
.LBB1_20:                               # %.lr.ph.i91.us
                                        #   Parent Loop BB1_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	%xmm5, 64(%rsp)         # 8-byte Spill
	movsd	(%rbx,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	divsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	callq	floor
	movsd	64(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	cvttsd2si	%xmm0, %eax
	cmpl	$8206, %eax             # imm = 0x200E
	jg	.LBB1_27
# BB#21:                                #   in Loop: Header=BB1_20 Depth=2
	movsd	(%r14,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	andpd	.LCPI1_16(%rip), %xmm1
	movslq	%eax, %rcx
	movsd	pow43(,%rcx,8), %xmm2   # xmm2 = mem[0],zero
	movsd	(%rsp), %xmm3           # 8-byte Reload
                                        # xmm3 = mem[0],zero
	mulsd	%xmm3, %xmm2
	movapd	%xmm1, %xmm0
	subsd	%xmm2, %xmm0
	cmpl	$8206, %eax             # imm = 0x200E
	je	.LBB1_24
# BB#22:                                #   in Loop: Header=BB1_20 Depth=2
	movsd	pow43+8(,%rcx,8), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm3, %xmm2
	subsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm2
	movapd	.LCPI1_16(%rip), %xmm3  # xmm3 = [nan,nan]
	movapd	%xmm3, %xmm4
	andpd	%xmm4, %xmm2
	movapd	%xmm0, %xmm3
	andpd	%xmm4, %xmm3
	ucomisd	%xmm2, %xmm3
	jbe	.LBB1_24
# BB#23:                                #   in Loop: Header=BB1_20 Depth=2
	movapd	%xmm1, %xmm0
.LBB1_24:                               #   in Loop: Header=BB1_20 Depth=2
	mulsd	%xmm0, %xmm0
	addsd	%xmm0, %xmm5
	addq	%r12, %rbp
	cmpq	%r13, %rbp
	jl	.LBB1_20
# BB#25:                                # %calc_sfb_ave_noise.exit97.loopexit.us
                                        #   in Loop: Header=BB1_19 Depth=1
	divsd	128(%rsp), %xmm5        # 8-byte Folded Reload
	ucomisd	.LCPI1_19, %xmm5
	jbe	.LBB1_27
# BB#26:                                # %calc_sfb_ave_noise.exit97.loopexit.us
                                        #   in Loop: Header=BB1_19 Depth=1
	movsd	40(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	ucomisd	%xmm5, %xmm0
	jae	.LBB1_41
.LBB1_27:                               # %calc_sfb_ave_noise.exit97.thread.us
                                        #   in Loop: Header=BB1_19 Depth=1
	movapd	96(%rsp), %xmm1         # 16-byte Reload
	movsd	.LCPI1_18(%rip), %xmm3  # xmm3 = mem[0],zero
	addsd	%xmm3, %xmm1
	ucomisd	120(%rsp), %xmm1        # 8-byte Folded Reload
	movsd	88(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	ja	.LBB1_19
	jmp	.LBB1_39
.LBB1_68:
	ucomisd	40(%rsp), %xmm1         # 8-byte Folded Reload
	jbe	.LBB1_69
# BB#70:
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	48(%rsp), %xmm1         # 16-byte Reload
	cmpeqsd	%xmm1, %xmm0
	movapd	16(%rsp), %xmm3         # 16-byte Reload
	andpd	%xmm0, %xmm3
	andnpd	%xmm1, %xmm0
	orpd	%xmm3, %xmm0
	jmp	.LBB1_16
.LBB1_35:                               # %.lr.ph.split.preheader
	xorpd	%xmm0, %xmm0
	movapd	%xmm1, %xmm3
	xorpd	%xmm1, %xmm1
	divsd	128(%rsp), %xmm1        # 8-byte Folded Reload
	ucomisd	%xmm0, %xmm1
	setbe	%al
	movsd	40(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	movapd	%xmm3, %xmm1
	setb	%bl
	orb	%al, %bl
	movsd	.LCPI1_18(%rip), %xmm3  # xmm3 = mem[0],zero
	.p2align	4, 0x90
.LBB1_36:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm1, %xmm0
	subsd	%xmm2, %xmm0
	andpd	.LCPI1_16(%rip), %xmm0
	movapd	%xmm1, %xmm2
	addsd	%xmm3, %xmm2
	cmpltsd	.LCPI1_17(%rip), %xmm0
	andpd	%xmm0, %xmm2
	andnpd	%xmm1, %xmm0
	orpd	%xmm2, %xmm0
	movapd	%xmm0, 64(%rsp)         # 16-byte Spill
	callq	exp2
	movsd	.LCPI1_4(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	testb	%bl, %bl
	je	.LBB1_37
# BB#38:                                # %calc_sfb_ave_noise.exit97.thread
                                        #   in Loop: Header=BB1_36 Depth=1
	movsd	.LCPI1_18(%rip), %xmm3  # xmm3 = mem[0],zero
	movapd	64(%rsp), %xmm0         # 16-byte Reload
	addsd	%xmm3, %xmm0
	ucomisd	120(%rsp), %xmm0        # 8-byte Folded Reload
	movapd	%xmm0, %xmm1
	movsd	88(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	ja	.LBB1_36
.LBB1_39:
	movapd	48(%rsp), %xmm0         # 16-byte Reload
	jmp	.LBB1_40
.LBB1_69:
	movapd	16(%rsp), %xmm0         # 16-byte Reload
	jmp	.LBB1_16
.LBB1_37:
	movapd	64(%rsp), %xmm0         # 16-byte Reload
.LBB1_40:                               # %._crit_edge
	movapd	%xmm0, 96(%rsp)         # 16-byte Spill
.LBB1_41:                               # %._crit_edge
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	find_scalefac, .Lfunc_end1-find_scalefac
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	3
.LCPI2_0:
	.quad	4607182418800017408     # double 1
	.quad	4611686018427387904     # double 2
.LCPI2_1:
	.quad	4619567317775286272     # double 7
	.quad	4624633867356078080     # double 15
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_2:
	.quad	4604930618986332160     # double 0.75
.LCPI2_3:
	.quad	4547007122018943789     # double 1.0E-4
	.text
	.globl	compute_scalefacs_short
	.p2align	4, 0x90
	.type	compute_scalefacs_short,@function
compute_scalefacs_short:                # @compute_scalefacs_short
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 32
	subq	$320, %rsp              # imm = 0x140
.Lcfi29:
	.cfi_def_cfa_offset 352
.Lcfi30:
	.cfi_offset %rbx, -32
.Lcfi31:
	.cfi_offset %r14, -24
.Lcfi32:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rdi, %rax
	xorl	%ebx, %ebx
	cmpl	$0, 68(%rsi)
	sete	%bl
	leaq	32(%rsp), %rdi
	movl	$288, %edx              # imm = 0x120
	movq	%rax, %rsi
	callq	memcpy
	movsd	.LCPI2_0(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	leaq	48(%rsp), %rbx
	addq	$8, %r14
	xorpd	%xmm0, %xmm0
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB2_1:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	xorl	%eax, %eax
	cmpq	$6, %r15
	setl	%al
	movsd	.LCPI2_1(,%rax,8), %xmm1 # xmm1 = mem[0],zero
	movsd	16(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	divsd	%xmm2, %xmm1
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	movsd	-16(%rbx), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movapd	%xmm2, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	.LCPI2_2(%rip), %xmm0   # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
	movsd	.LCPI2_3(%rip), %xmm1   # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	movl	%eax, -8(%r14)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	24(%rsp), %xmm0         # 8-byte Folded Reload
	maxsd	(%rsp), %xmm0           # 8-byte Folded Reload
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	-8(%rbx), %xmm0         # xmm0 = mem[0],zero
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	.LCPI2_2(%rip), %xmm0   # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
	addsd	.LCPI2_3(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	movl	%eax, -4(%r14)
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	24(%rsp), %xmm0         # 8-byte Folded Reload
	maxsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	.LCPI2_2(%rip), %xmm0   # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
	addsd	.LCPI2_3(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	movl	%eax, (%r14)
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	maxsd	(%rsp), %xmm0           # 8-byte Folded Reload
	incq	%r15
	addq	$24, %rbx
	addq	$12, %r14
	cmpq	$12, %r15
	jne	.LBB2_1
# BB#2:
	addq	$320, %rsp              # imm = 0x140
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	compute_scalefacs_short, .Lfunc_end2-compute_scalefacs_short
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4604930618986332160     # double 0.75
.LCPI3_1:
	.quad	4547007122018943789     # double 1.0E-4
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	3
.LCPI3_2:
	.quad	4619567317775286272     # double 7
	.quad	4624633867356078080     # double 15
	.text
	.globl	compute_scalefacs_long
	.p2align	4, 0x90
	.type	compute_scalefacs_long,@function
compute_scalefacs_long:                 # @compute_scalefacs_long
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 40
	subq	$200, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 240
.Lcfi38:
	.cfi_offset %rbx, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rax
	cmpl	$1, 68(%r14)
	movl	$1, %ebp
	adcl	$0, %ebp
	leaq	32(%rsp), %rdi
	movl	$168, %edx
	movq	%rax, %rsi
	callq	memcpy
	movl	$0, 64(%r14)
	movl	pretab+44(%rip), %eax
	cltd
	idivl	%ebp
	cvtsi2sdl	%eax, %xmm1
	addsd	120(%rsp), %xmm1
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm1
	ja	.LBB3_1
# BB#4:
	movl	pretab+48(%rip), %eax
	cltd
	idivl	%ebp
	cvtsi2sdl	%eax, %xmm2
	addsd	128(%rsp), %xmm2
	ucomisd	%xmm0, %xmm2
	ja	.LBB3_1
# BB#5:
	movl	pretab+52(%rip), %eax
	cltd
	idivl	%ebp
	cvtsi2sdl	%eax, %xmm3
	addsd	136(%rsp), %xmm3
	xorpd	%xmm5, %xmm5
	ucomisd	%xmm5, %xmm3
	ja	.LBB3_1
# BB#6:
	movl	pretab+56(%rip), %eax
	cltd
	idivl	%ebp
	cvtsi2sdl	%eax, %xmm4
	addsd	144(%rsp), %xmm4
	ucomisd	%xmm5, %xmm4
	ja	.LBB3_1
# BB#7:
	movl	pretab+60(%rip), %eax
	cltd
	idivl	%ebp
	cvtsi2sdl	%eax, %xmm6
	addsd	152(%rsp), %xmm6
	xorps	%xmm7, %xmm7
	ucomisd	%xmm7, %xmm6
	ja	.LBB3_1
# BB#8:
	movsd	160(%rsp), %xmm12       # xmm12 = mem[0],zero
	movl	pretab+64(%rip), %eax
	cltd
	idivl	%ebp
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%eax, %xmm5
	addsd	%xmm12, %xmm5
	ucomisd	%xmm7, %xmm5
	ja	.LBB3_1
# BB#9:
	movsd	168(%rsp), %xmm11       # xmm11 = mem[0],zero
	movl	pretab+68(%rip), %eax
	cltd
	idivl	%ebp
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%eax, %xmm5
	addsd	%xmm11, %xmm5
	xorps	%xmm7, %xmm7
	ucomisd	%xmm7, %xmm5
	ja	.LBB3_1
# BB#10:
	movsd	176(%rsp), %xmm8        # xmm8 = mem[0],zero
	movl	pretab+72(%rip), %eax
	cltd
	idivl	%ebp
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%eax, %xmm5
	addsd	%xmm8, %xmm5
	ucomisd	%xmm7, %xmm5
	ja	.LBB3_1
# BB#11:
	movsd	184(%rsp), %xmm9        # xmm9 = mem[0],zero
	movl	pretab+76(%rip), %eax
	cltd
	idivl	%ebp
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%eax, %xmm5
	addsd	%xmm9, %xmm5
	xorps	%xmm7, %xmm7
	ucomisd	%xmm7, %xmm5
	ja	.LBB3_1
# BB#12:
	movsd	192(%rsp), %xmm10       # xmm10 = mem[0],zero
	movl	pretab+80(%rip), %eax
	cltd
	idivl	%ebp
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%eax, %xmm5
	addsd	%xmm10, %xmm5
	ucomisd	%xmm7, %xmm5
	ja	.LBB3_1
# BB#13:                                # %.thread.preheader.loopexit48
	movl	$1, 64(%r14)
	movsd	%xmm1, 120(%rsp)
	movsd	%xmm2, 128(%rsp)
	movsd	%xmm3, 136(%rsp)
	movsd	%xmm4, 144(%rsp)
	movsd	%xmm6, 152(%rsp)
	movl	pretab+64(%rip), %eax
	cltd
	idivl	%ebp
	cvtsi2sdl	%eax, %xmm1
	addsd	%xmm1, %xmm12
	movsd	%xmm12, 160(%rsp)
	movl	pretab+68(%rip), %eax
	cltd
	idivl	%ebp
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	addsd	%xmm1, %xmm11
	movsd	%xmm11, 168(%rsp)
	movl	pretab+72(%rip), %eax
	cltd
	idivl	%ebp
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	addsd	%xmm1, %xmm8
	movsd	%xmm8, 176(%rsp)
	movl	pretab+76(%rip), %eax
	cltd
	idivl	%ebp
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	addsd	%xmm1, %xmm9
	movsd	%xmm9, 184(%rsp)
	movl	pretab+80(%rip), %eax
	cltd
	idivl	%ebp
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	addsd	%xmm1, %xmm10
	movsd	%xmm10, 192(%rsp)
.LBB3_1:                                # %.thread.preheader
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebp, %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_2:                                # %.thread
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	xorl	%ebx, %ebx
	cmpq	$11, %rbp
	setl	%bl
	movsd	32(%rsp,%rbp,8), %xmm0  # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
	addsd	.LCPI3_1(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	movl	%eax, (%r15,%rbp,4)
	movsd	.LCPI3_2(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	divsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	addsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	maxsd	24(%rsp), %xmm0         # 8-byte Folded Reload
	incq	%rbp
	cmpq	$21, %rbp
	jne	.LBB3_2
# BB#3:
	addq	$200, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	compute_scalefacs_long, .Lfunc_end3-compute_scalefacs_long
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4621819117588971520     # double 10
.LCPI4_1:
	.quad	4638637247447433216     # double 127
.LCPI4_3:
	.quad	4616189618054758400     # double 4
.LCPI4_4:
	.quad	4641592734702895104     # double 210
.LCPI4_5:
	.quad	4602678819172646912     # double 0.5
.LCPI4_7:
	.quad	-4611686018427387904    # double -2
.LCPI4_8:
	.quad	4604930618986332160     # double 0.75
.LCPI4_9:
	.quad	4547007122018943789     # double 1.0E-4
.LCPI4_11:
	.quad	0                       # double 0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_2:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
.LCPI4_6:
	.quad	4615063718147915776     # double 3.5
	.quad	4620130267728707584     # double 7.5
.LCPI4_10:
	.quad	4619567317775286272     # double 7
	.quad	4624633867356078080     # double 15
	.text
	.globl	VBR_iteration_loop_new
	.p2align	4, 0x90
	.type	VBR_iteration_loop_new,@function
VBR_iteration_loop_new:                 # @VBR_iteration_loop_new
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 56
	subq	$7480, %rsp             # imm = 0x1D38
.Lcfi48:
	.cfi_def_cfa_offset 7536
.Lcfi49:
	.cfi_offset %rbx, -56
.Lcfi50:
	.cfi_offset %r12, -48
.Lcfi51:
	.cfi_offset %r13, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movq	%r8, 96(%rsp)           # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	movq	7536(%rsp), %rdx
	movq	%r9, 104(%rsp)          # 8-byte Spill
	movq	%r9, %rsi
	callq	iteration_init
	movl	92(%rbx), %eax
	leal	-10(%rax,%rax), %eax
	cvtsi2sdl	%eax, %xmm1
	divsd	.LCPI4_0(%rip), %xmm1
	ucomisd	.LCPI4_1(%rip), %xmm1
	ja	.LBB4_1
.LBB4_2:                                # %cdce.end
	movl	$1065353216, masking_lower(%rip) # imm = 0x3F800000
	cmpl	$0, 200(%rbx)
	jle	.LBB4_39
# BB#3:                                 # %.lr.ph185
	movq	7544(%rsp), %rcx
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	16(%rax), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	96(%rcx), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	5520(%rsp), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_8 Depth 2
                                        #       Child Loop BB4_9 Depth 3
                                        #       Child Loop BB4_24 Depth 3
                                        #       Child Loop BB4_26 Depth 3
                                        #       Child Loop BB4_32 Depth 3
                                        #       Child Loop BB4_35 Depth 3
	cmpl	$0, convert_mdct(%rip)
	je	.LBB4_6
# BB#5:                                 #   in Loop: Header=BB4_4 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rax,8), %rdi
	shlq	$10, %rdi
	addq	48(%rsp), %rdi          # 8-byte Folded Reload
	movq	%rdi, %rsi
	callq	ms_convert
.LBB4_6:                                # %.preheader172
                                        #   in Loop: Header=BB4_4 Depth=1
	cmpl	$0, 204(%rbx)
	jle	.LBB4_38
# BB#7:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB4_4 Depth=1
	movq	64(%rsp), %r12          # 8-byte Reload
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	72(%rsp), %rcx          # 8-byte Reload
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB4_8:                                # %.lr.ph
                                        #   Parent Loop BB4_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_9 Depth 3
                                        #       Child Loop BB4_24 Depth 3
                                        #       Child Loop BB4_26 Depth 3
                                        #       Child Loop BB4_32 Depth 3
                                        #       Child Loop BB4_35 Depth 3
	imulq	$240, 40(%rsp), %r13    # 8-byte Folded Reload
	addq	104(%rsp), %r13         # 8-byte Folded Reload
	imulq	$120, %r15, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	72(%rax,%r13), %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	%rcx, %rbx
	movl	$2, %ebp
	.p2align	4, 0x90
.LBB4_9:                                #   Parent Loop BB4_4 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-16(%rbx), %xmm1        # xmm1 = mem[0],zero
	andpd	.LCPI4_2(%rip), %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB4_11
# BB#10:                                # %call.sqrt
                                        #   in Loop: Header=BB4_9 Depth=3
	movapd	%xmm1, %xmm0
	movapd	%xmm1, 16(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	16(%rsp), %xmm1         # 16-byte Reload
.LBB4_11:                               # %.split
                                        #   in Loop: Header=BB4_9 Depth=3
	mulsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB4_13
# BB#12:                                # %call.sqrt305
                                        #   in Loop: Header=BB4_9 Depth=3
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB4_13:                               # %.split.split
                                        #   in Loop: Header=BB4_9 Depth=3
	movsd	%xmm1, 896(%rsp,%rbp,8)
	movsd	-8(%rbx), %xmm1         # xmm1 = mem[0],zero
	andpd	.LCPI4_2(%rip), %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB4_15
# BB#14:                                # %call.sqrt306
                                        #   in Loop: Header=BB4_9 Depth=3
	movapd	%xmm1, %xmm0
	movapd	%xmm1, 16(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	16(%rsp), %xmm1         # 16-byte Reload
.LBB4_15:                               # %.split.split.split
                                        #   in Loop: Header=BB4_9 Depth=3
	mulsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB4_17
# BB#16:                                # %call.sqrt307
                                        #   in Loop: Header=BB4_9 Depth=3
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB4_17:                               # %.split.split.split.split
                                        #   in Loop: Header=BB4_9 Depth=3
	movsd	%xmm1, 904(%rsp,%rbp,8)
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	andpd	.LCPI4_2(%rip), %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB4_19
# BB#18:                                # %call.sqrt308
                                        #   in Loop: Header=BB4_9 Depth=3
	movapd	%xmm1, %xmm0
	movapd	%xmm1, 16(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	16(%rsp), %xmm1         # 16-byte Reload
.LBB4_19:                               # %.split.split.split.split.split
                                        #   in Loop: Header=BB4_9 Depth=3
	mulsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB4_21
# BB#20:                                # %call.sqrt309
                                        #   in Loop: Header=BB4_9 Depth=3
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB4_21:                               # %.split.split.split.split.split.split
                                        #   in Loop: Header=BB4_9 Depth=3
	movsd	%xmm1, 912(%rsp,%rbp,8)
	addq	$3, %rbp
	addq	$24, %rbx
	cmpq	$578, %rbp              # imm = 0x242
	jne	.LBB4_9
# BB#22:                                #   in Loop: Header=BB4_8 Depth=2
	movq	%r13, 120(%rsp)         # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	48(%rax,%r13), %rcx
	movq	40(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rsi,8), %rax
	shlq	$10, %rax
	addq	48(%rsp), %rax          # 8-byte Folded Reload
	leaq	(%r15,%r15,8), %r13
	shlq	$9, %r13
	addq	%rax, %r13
	imulq	$1952, %rsi, %rax       # imm = 0x7A0
	addq	96(%rsp), %rax          # 8-byte Folded Reload
	imulq	$976, %r15, %rdx        # imm = 0x3D0
	addq	%rax, %rdx
	movq	%r15, 88(%rsp)          # 8-byte Spill
	imulq	$488, %r15, %rax        # imm = 0x1E8
	imulq	$976, %rsi, %rsi        # imm = 0x3D0
	leaq	5520(%rsp,%rsi), %r8
	addq	%rax, %r8
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	movq	%rcx, %r15
	callq	calc_xmin
	cmpl	$2, (%rsp)              # 4-byte Folded Reload
	jne	.LBB4_23
# BB#25:                                # %.preheader166.preheader
                                        #   in Loop: Header=BB4_8 Depth=2
	xorpd	%xmm0, %xmm0
	movl	$48, %ebx
	movl	$scalefac_band+96, %ebp
	.p2align	4, 0x90
.LBB4_26:                               # %.preheader166
                                        #   Parent Loop BB4_4 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movapd	%xmm0, 16(%rsp)         # 16-byte Spill
	movslq	-4(%rbp), %rax
	movl	(%rbp), %r8d
	subl	%eax, %r8d
	shlq	$3, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%r13,%rax), %rdi
	leaq	912(%rsp,%rax), %rsi
	movss	masking_lower(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	mulsd	-16(%r14,%rbx,4), %xmm0
	movl	$3, %edx
	callq	find_scalefac
	movsd	%xmm0, 112(%rsp,%rbx,4)
	maxsd	16(%rsp), %xmm0         # 16-byte Folded Reload
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movslq	-4(%rbp), %rax
	movl	(%rbp), %r8d
	subl	%eax, %r8d
	leaq	(%rax,%rax,2), %rax
	leaq	8(%r13,%rax,8), %rdi
	leaq	920(%rsp,%rax,8), %rsi
	movss	masking_lower(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	mulsd	-8(%r14,%rbx,4), %xmm0
	movl	$3, %edx
	callq	find_scalefac
	movsd	%xmm0, 120(%rsp,%rbx,4)
	maxsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movslq	-4(%rbp), %rax
	movl	(%rbp), %r8d
	subl	%eax, %r8d
	leaq	(%rax,%rax,2), %rax
	leaq	16(%r13,%rax,8), %rdi
	leaq	928(%rsp,%rax,8), %rsi
	movss	masking_lower(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	mulsd	(%r14,%rbx,4), %xmm0
	movl	$3, %edx
	callq	find_scalefac
	movsd	%xmm0, 128(%rsp,%rbx,4)
	maxsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	addq	$4, %rbp
	addq	$6, %rbx
	cmpq	$120, %rbx
	jne	.LBB4_26
	jmp	.LBB4_27
	.p2align	4, 0x90
.LBB4_23:                               # %.preheader170.preheader
                                        #   in Loop: Header=BB4_8 Depth=2
	xorpd	%xmm1, %xmm1
	movq	%r14, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_24:                               # %.preheader170
                                        #   Parent Loop BB4_4 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movslq	scalefac_band(%rbp), %rax
	movl	scalefac_band+4(%rbp), %r8d
	subl	%eax, %r8d
	leaq	(%r13,%rax,8), %rdi
	leaq	912(%rsp,%rax,8), %rsi
	movss	masking_lower(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	mulsd	(%rbx), %xmm0
	movl	$1, %edx
	callq	find_scalefac
	movsd	%xmm0, 128(%rsp,%rbp,2)
	maxsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	addq	$4, %rbp
	addq	$8, %rbx
	cmpq	$84, %rbp
	movapd	%xmm0, %xmm1
	jne	.LBB4_24
.LBB4_27:                               # %.loopexit
                                        #   in Loop: Header=BB4_8 Depth=2
	movapd	%xmm0, 16(%rsp)         # 16-byte Spill
	mulsd	.LCPI4_3(%rip), %xmm0
	addsd	.LCPI4_4(%rip), %xmm0
	addsd	.LCPI4_5(%rip), %xmm0
	callq	floor
	cmpl	$2, (%rsp)              # 4-byte Folded Reload
	cvttsd2si	%xmm0, %rax
	movq	120(%rsp), %r13         # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	%eax, 60(%rbp,%r13)
	jne	.LBB4_28
# BB#31:                                # %.preheader
                                        #   in Loop: Header=BB4_8 Depth=2
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	movaps	%xmm2, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movapd	304(%rsp), %xmm1
	subpd	%xmm0, %xmm1
	movapd	%xmm1, 304(%rsp)
	movsd	320(%rsp), %xmm1        # xmm1 = mem[0],zero
	subsd	%xmm2, %xmm1
	movsd	%xmm1, 320(%rsp)
	movupd	328(%rsp), %xmm1
	subpd	%xmm0, %xmm1
	movupd	%xmm1, 328(%rsp)
	movsd	344(%rsp), %xmm1        # xmm1 = mem[0],zero
	subsd	%xmm2, %xmm1
	movsd	%xmm1, 344(%rsp)
	movapd	352(%rsp), %xmm1
	subpd	%xmm0, %xmm1
	movapd	%xmm1, 352(%rsp)
	movsd	368(%rsp), %xmm1        # xmm1 = mem[0],zero
	subsd	%xmm2, %xmm1
	movsd	%xmm1, 368(%rsp)
	movupd	376(%rsp), %xmm1
	subpd	%xmm0, %xmm1
	movupd	%xmm1, 376(%rsp)
	movsd	392(%rsp), %xmm1        # xmm1 = mem[0],zero
	subsd	%xmm2, %xmm1
	movsd	%xmm1, 392(%rsp)
	movapd	400(%rsp), %xmm1
	subpd	%xmm0, %xmm1
	movapd	%xmm1, 400(%rsp)
	movsd	416(%rsp), %xmm1        # xmm1 = mem[0],zero
	subsd	%xmm2, %xmm1
	movsd	%xmm1, 416(%rsp)
	movupd	424(%rsp), %xmm1
	subpd	%xmm0, %xmm1
	movupd	%xmm1, 424(%rsp)
	movsd	440(%rsp), %xmm1        # xmm1 = mem[0],zero
	subsd	%xmm2, %xmm1
	movsd	%xmm1, 440(%rsp)
	movapd	448(%rsp), %xmm1
	subpd	%xmm0, %xmm1
	movapd	%xmm1, 448(%rsp)
	movsd	464(%rsp), %xmm1        # xmm1 = mem[0],zero
	subsd	%xmm2, %xmm1
	movsd	%xmm1, 464(%rsp)
	movupd	472(%rsp), %xmm1
	subpd	%xmm0, %xmm1
	movupd	%xmm1, 472(%rsp)
	movsd	488(%rsp), %xmm1        # xmm1 = mem[0],zero
	subsd	%xmm2, %xmm1
	movsd	%xmm1, 488(%rsp)
	movapd	496(%rsp), %xmm1
	subpd	%xmm0, %xmm1
	movapd	%xmm1, 496(%rsp)
	movsd	512(%rsp), %xmm1        # xmm1 = mem[0],zero
	subsd	%xmm2, %xmm1
	movsd	%xmm1, 512(%rsp)
	movupd	520(%rsp), %xmm1
	subpd	%xmm0, %xmm1
	movupd	%xmm1, 520(%rsp)
	movsd	536(%rsp), %xmm1        # xmm1 = mem[0],zero
	subsd	%xmm2, %xmm1
	movsd	%xmm1, 536(%rsp)
	movapd	544(%rsp), %xmm1
	subpd	%xmm0, %xmm1
	movapd	%xmm1, 544(%rsp)
	movsd	560(%rsp), %xmm1        # xmm1 = mem[0],zero
	subsd	%xmm2, %xmm1
	movsd	%xmm1, 560(%rsp)
	movupd	568(%rsp), %xmm1
	subpd	%xmm0, %xmm1
	movupd	%xmm1, 568(%rsp)
	movsd	584(%rsp), %xmm0        # xmm0 = mem[0],zero
	subsd	%xmm2, %xmm0
	movsd	%xmm0, 584(%rsp)
	leaq	116(%rbp,%r13), %r15
	movl	$0, 116(%rbp,%r13)
	movl	$288, %edx              # imm = 0x120
	leaq	624(%rsp), %rdi
	leaq	304(%rsp), %rsi
	callq	memcpy
	xorpd	%xmm0, %xmm0
	leaq	640(%rsp), %rbp
	movq	%r12, %rbx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB4_32:                               # %.preheader.i
                                        #   Parent Loop BB4_4 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	xorl	%eax, %eax
	cmpq	$6, %r13
	setl	%al
	movsd	.LCPI4_6(,%rax,8), %xmm0 # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	-16(%rbp), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	mulsd	.LCPI4_7(%rip), %xmm0
	movsd	.LCPI4_8(%rip), %xmm1   # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm0
	movsd	.LCPI4_9(%rip), %xmm1   # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	movl	%eax, -8(%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	maxsd	(%rsp), %xmm0           # 8-byte Folded Reload
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	-8(%rbp), %xmm1         # xmm1 = mem[0],zero
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	addsd	%xmm1, %xmm1
	movsd	.LCPI4_8(%rip), %xmm0   # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
	addsd	.LCPI4_9(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	movl	%eax, -4(%rbx)
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	maxsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	(%rbp), %xmm1           # xmm1 = mem[0],zero
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	addsd	%xmm1, %xmm1
	movsd	.LCPI4_8(%rip), %xmm0   # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
	addsd	.LCPI4_9(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	movl	%eax, (%rbx)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	maxsd	(%rsp), %xmm0           # 8-byte Folded Reload
	incq	%r13
	addq	$12, %rbx
	addq	$24, %rbp
	cmpq	$12, %r13
	jne	.LBB4_32
# BB#33:                                # %compute_scalefacs_short.exit
                                        #   in Loop: Header=BB4_8 Depth=2
	ucomisd	.LCPI4_11, %xmm0
	jbe	.LBB4_37
# BB#34:                                #   in Loop: Header=BB4_8 Depth=2
	movl	$1, (%r15)
	movl	$288, %edx              # imm = 0x120
	leaq	624(%rsp), %rdi
	leaq	304(%rsp), %rsi
	callq	memcpy
	xorpd	%xmm0, %xmm0
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_35:                               # %.preheader.i164
                                        #   Parent Loop BB4_4 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	xorl	%eax, %eax
	cmpq	$6, %rbp
	setl	%al
	movsd	.LCPI4_10(,%rax,8), %xmm0 # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	624(%rsp,%rbx,2), %xmm1 # xmm1 = mem[0],zero
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movsd	.LCPI4_8(%rip), %xmm0   # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
	movsd	.LCPI4_9(%rip), %xmm1   # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	movl	%eax, -8(%r12,%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	maxsd	(%rsp), %xmm0           # 8-byte Folded Reload
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	632(%rsp,%rbx,2), %xmm1 # xmm1 = mem[0],zero
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	movsd	.LCPI4_8(%rip), %xmm0   # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
	addsd	.LCPI4_9(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	movl	%eax, -4(%r12,%rbx)
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	maxsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	640(%rsp,%rbx,2), %xmm1 # xmm1 = mem[0],zero
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movsd	.LCPI4_8(%rip), %xmm0   # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
	addsd	.LCPI4_9(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	movl	%eax, (%r12,%rbx)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	maxsd	(%rsp), %xmm0           # 8-byte Folded Reload
	incq	%rbp
	addq	$12, %rbx
	cmpq	$144, %rbx
	jne	.LBB4_35
# BB#36:                                # %compute_scalefacs_short.exit165
                                        #   in Loop: Header=BB4_8 Depth=2
	ucomisd	.LCPI4_11, %xmm0
	jbe	.LBB4_37
	jmp	.LBB4_40
	.p2align	4, 0x90
.LBB4_28:                               # %.preheader168.preheader
                                        #   in Loop: Header=BB4_8 Depth=2
	movapd	128(%rsp), %xmm1
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	movaps	%xmm2, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	subpd	%xmm0, %xmm1
	movapd	%xmm1, 128(%rsp)
	movapd	144(%rsp), %xmm1
	subpd	%xmm0, %xmm1
	movapd	%xmm1, 144(%rsp)
	movapd	160(%rsp), %xmm1
	subpd	%xmm0, %xmm1
	movapd	%xmm1, 160(%rsp)
	movapd	176(%rsp), %xmm1
	subpd	%xmm0, %xmm1
	movapd	%xmm1, 176(%rsp)
	movapd	192(%rsp), %xmm1
	subpd	%xmm0, %xmm1
	movapd	%xmm1, 192(%rsp)
	movapd	208(%rsp), %xmm1
	subpd	%xmm0, %xmm1
	movapd	%xmm1, 208(%rsp)
	movapd	224(%rsp), %xmm1
	subpd	%xmm0, %xmm1
	movapd	%xmm1, 224(%rsp)
	movapd	240(%rsp), %xmm1
	subpd	%xmm0, %xmm1
	movapd	%xmm1, 240(%rsp)
	movapd	256(%rsp), %xmm1
	subpd	%xmm0, %xmm1
	movapd	%xmm1, 256(%rsp)
	movapd	272(%rsp), %xmm1
	subpd	%xmm0, %xmm1
	movapd	%xmm1, 272(%rsp)
	movsd	288(%rsp), %xmm0        # xmm0 = mem[0],zero
	subsd	%xmm2, %xmm0
	movsd	%xmm0, 288(%rsp)
	movl	$0, 116(%rbp,%r13)
	imulq	$488, 40(%rsp), %rax    # 8-byte Folded Reload
                                        # imm = 0x1E8
	addq	7544(%rsp), %rax
	imulq	$244, 88(%rsp), %rbx    # 8-byte Folded Reload
	addq	%rax, %rbx
	leaq	128(%rsp), %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	compute_scalefacs_long
	ucomisd	.LCPI4_11, %xmm0
	jbe	.LBB4_37
# BB#29:                                #   in Loop: Header=BB4_8 Depth=2
	leaq	116(%rbp,%r13), %rax
	movl	$1, (%rax)
	leaq	128(%rsp), %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	compute_scalefacs_long
	ucomisd	.LCPI4_11, %xmm0
	ja	.LBB4_30
.LBB4_37:                               #   in Loop: Header=BB4_8 Depth=2
	movq	88(%rsp), %r15          # 8-byte Reload
	incq	%r15
	movq	80(%rsp), %rbx          # 8-byte Reload
	movslq	204(%rbx), %rax
	movq	112(%rsp), %rcx         # 8-byte Reload
	addq	$4608, %rcx             # imm = 0x1200
	addq	$488, %r14              # imm = 0x1E8
	addq	$244, %r12
	cmpq	%rax, %r15
	jl	.LBB4_8
.LBB4_38:                               # %._crit_edge
                                        #   in Loop: Header=BB4_4 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rdx
	incq	%rdx
	movslq	200(%rbx), %rax
	addq	$9216, 72(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x2400
	addq	$976, 56(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x3D0
	addq	$488, 64(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x1E8
	movq	%rdx, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	cmpq	%rax, %rdx
	jl	.LBB4_4
.LBB4_39:                               # %._crit_edge186
	addq	$7480, %rsp             # imm = 0x1D38
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_1:                                # %cdce.call
	movsd	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	pow
	jmp	.LBB4_2
.LBB4_30:
	movl	$32, %edi
	callq	exit
.LBB4_40:
	movl	$32, %edi
	callq	exit
.Lfunc_end4:
	.size	VBR_iteration_loop_new, .Lfunc_end4-VBR_iteration_loop_new
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
