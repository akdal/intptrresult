	.text
	.file	"btSliderConstraint.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1065353216              # float 1
	.long	3212836864              # float -1
	.long	0                       # float 0
	.long	0                       # float 0
.LCPI0_1:
	.long	1065353216              # float 1
	.long	1060320051              # float 0.699999988
	.long	0                       # float 0
	.long	1065353216              # float 1
.LCPI0_2:
	.long	1065353216              # float 1
	.long	1060320051              # float 0.699999988
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.text
	.globl	_ZN18btSliderConstraint10initParamsEv
	.p2align	4, 0x90
	.type	_ZN18btSliderConstraint10initParamsEv,@function
_ZN18btSliderConstraint10initParamsEv:  # @_ZN18btSliderConstraint10initParamsEv
	.cfi_startproc
# BB#0:
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [1.000000e+00,-1.000000e+00,0.000000e+00,0.000000e+00]
	movups	%xmm0, 232(%rdi)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [1.000000e+00,7.000000e-01,0.000000e+00,1.000000e+00]
	movups	%xmm0, 248(%rdi)
	movl	$1060320051, 264(%rdi)  # imm = 0x3F333333
	movl	$0, 268(%rdi)
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [1.000000e+00,7.000000e-01,1.000000e+00,1.000000e+00]
	movups	%xmm0, 296(%rdi)
	movl	$1060320051, 312(%rdi)  # imm = 0x3F333333
	movl	$1065353216, 316(%rdi)  # imm = 0x3F800000
	movups	%xmm0, 272(%rdi)
	movl	$1060320051, 288(%rdi)  # imm = 0x3F333333
	movl	$1065353216, 292(%rdi)  # imm = 0x3F800000
	movb	$0, 1116(%rdi)
	movq	$0, 1136(%rdi)
	movl	$0, 1144(%rdi)
	movq	$0, 1125(%rdi)
	movq	$0, 1120(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN18btSliderConstraint10initParamsEv, .Lfunc_end0-_ZN18btSliderConstraint10initParamsEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	1065353216              # float 1
	.long	3212836864              # float -1
	.long	0                       # float 0
	.long	0                       # float 0
.LCPI1_1:
	.long	1065353216              # float 1
	.long	1060320051              # float 0.699999988
	.long	0                       # float 0
	.long	1065353216              # float 1
.LCPI1_2:
	.long	1065353216              # float 1
	.long	1060320051              # float 0.699999988
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.text
	.globl	_ZN18btSliderConstraintC2Ev
	.p2align	4, 0x90
	.type	_ZN18btSliderConstraintC2Ev,@function
_ZN18btSliderConstraintC2Ev:            # @_ZN18btSliderConstraintC2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$7, %esi
	callq	_ZN17btTypedConstraintC2E21btTypedConstraintType
	movq	$_ZTV18btSliderConstraint+16, (%rbx)
	movb	$0, 96(%rbx)
	movb	$1, 228(%rbx)
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [1.000000e+00,-1.000000e+00,0.000000e+00,0.000000e+00]
	movups	%xmm0, 232(%rbx)
	movaps	.LCPI1_1(%rip), %xmm0   # xmm0 = [1.000000e+00,7.000000e-01,0.000000e+00,1.000000e+00]
	movups	%xmm0, 248(%rbx)
	movl	$1060320051, 264(%rbx)  # imm = 0x3F333333
	movl	$0, 268(%rbx)
	movl	$1065353216, 296(%rbx)  # imm = 0x3F800000
	movl	$1060320051, 300(%rbx)  # imm = 0x3F333333
	movl	$1065353216, 304(%rbx)  # imm = 0x3F800000
	movl	$1065353216, 308(%rbx)  # imm = 0x3F800000
	movl	$1060320051, 312(%rbx)  # imm = 0x3F333333
	movl	$1065353216, 316(%rbx)  # imm = 0x3F800000
	movaps	.LCPI1_2(%rip), %xmm0   # xmm0 = [1.000000e+00,7.000000e-01,1.000000e+00,1.000000e+00]
	movups	%xmm0, 272(%rbx)
	movl	$1060320051, 288(%rbx)  # imm = 0x3F333333
	movl	$1065353216, 292(%rbx)  # imm = 0x3F800000
	movb	$0, 1116(%rbx)
	movq	$0, 1136(%rbx)
	movl	$0, 1144(%rbx)
	movq	$0, 1125(%rbx)
	movq	$0, 1120(%rbx)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZN18btSliderConstraintC2Ev, .Lfunc_end1-_ZN18btSliderConstraintC2Ev
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	1065353216              # float 1
	.long	3212836864              # float -1
	.long	0                       # float 0
	.long	0                       # float 0
.LCPI3_1:
	.long	1065353216              # float 1
	.long	1060320051              # float 0.699999988
	.long	0                       # float 0
	.long	1065353216              # float 1
.LCPI3_2:
	.long	1065353216              # float 1
	.long	1060320051              # float 0.699999988
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.text
	.globl	_ZN18btSliderConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
	.p2align	4, 0x90
	.type	_ZN18btSliderConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b,@function
_ZN18btSliderConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b: # @_ZN18btSliderConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 48
.Lcfi7:
	.cfi_offset %rbx, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movq	%r8, %r15
	movq	%rcx, %rbp
	movq	%rdx, %rax
	movq	%rsi, %rcx
	movq	%rdi, %rbx
	movl	$7, %esi
	movq	%rcx, %rdx
	movq	%rax, %rcx
	callq	_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_
	movq	$_ZTV18btSliderConstraint+16, (%rbx)
	movb	$0, 96(%rbx)
	movups	(%rbp), %xmm0
	movups	%xmm0, 100(%rbx)
	movups	16(%rbp), %xmm0
	movups	%xmm0, 116(%rbx)
	movups	32(%rbp), %xmm0
	movups	%xmm0, 132(%rbx)
	movups	48(%rbp), %xmm0
	movups	%xmm0, 148(%rbx)
	movups	(%r15), %xmm0
	movups	%xmm0, 164(%rbx)
	movups	16(%r15), %xmm0
	movups	%xmm0, 180(%rbx)
	movups	32(%r15), %xmm0
	movups	%xmm0, 196(%rbx)
	movups	48(%r15), %xmm0
	movups	%xmm0, 212(%rbx)
	movb	%r14b, 228(%rbx)
	movaps	.LCPI3_0(%rip), %xmm0   # xmm0 = [1.000000e+00,-1.000000e+00,0.000000e+00,0.000000e+00]
	movups	%xmm0, 232(%rbx)
	movaps	.LCPI3_1(%rip), %xmm0   # xmm0 = [1.000000e+00,7.000000e-01,0.000000e+00,1.000000e+00]
	movups	%xmm0, 248(%rbx)
	movl	$1060320051, 264(%rbx)  # imm = 0x3F333333
	movl	$0, 268(%rbx)
	movl	$1065353216, 296(%rbx)  # imm = 0x3F800000
	movl	$1060320051, 300(%rbx)  # imm = 0x3F333333
	movl	$1065353216, 304(%rbx)  # imm = 0x3F800000
	movl	$1065353216, 308(%rbx)  # imm = 0x3F800000
	movl	$1060320051, 312(%rbx)  # imm = 0x3F333333
	movl	$1065353216, 316(%rbx)  # imm = 0x3F800000
	movaps	.LCPI3_2(%rip), %xmm0   # xmm0 = [1.000000e+00,7.000000e-01,1.000000e+00,1.000000e+00]
	movups	%xmm0, 272(%rbx)
	movl	$1060320051, 288(%rbx)  # imm = 0x3F333333
	movl	$1065353216, 292(%rbx)  # imm = 0x3F800000
	movb	$0, 1116(%rbx)
	movq	$0, 1136(%rbx)
	movl	$0, 1144(%rbx)
	movq	$0, 1125(%rbx)
	movq	$0, 1120(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZN18btSliderConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b, .Lfunc_end3-_ZN18btSliderConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
	.cfi_endproc

	.section	.text._ZN11btRigidBodyD2Ev,"axG",@progbits,_ZN11btRigidBodyD2Ev,comdat
	.weak	_ZN11btRigidBodyD2Ev
	.p2align	4, 0x90
	.type	_ZN11btRigidBodyD2Ev,@function
_ZN11btRigidBodyD2Ev:                   # @_ZN11btRigidBodyD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV11btRigidBody+16, (%rbx)
	movq	536(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_4
# BB#1:
	cmpb	$0, 544(%rbx)
	je	.LBB4_3
# BB#2:
.Ltmp0:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp1:
.LBB4_3:                                # %.noexc
	movq	$0, 536(%rbx)
.LBB4_4:
	movb	$1, 544(%rbx)
	movq	$0, 536(%rbx)
	movq	$0, 524(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17btCollisionObjectD2Ev # TAILCALL
.LBB4_5:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_ZN17btCollisionObjectD2Ev
.Ltmp4:
# BB#6:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB4_7:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN11btRigidBodyD2Ev, .Lfunc_end4-_ZN11btRigidBodyD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end4-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	1065353216              # float 1
	.long	3212836864              # float -1
	.long	0                       # float 0
	.long	0                       # float 0
.LCPI5_1:
	.long	1065353216              # float 1
	.long	1060320051              # float 0.699999988
	.long	0                       # float 0
	.long	1065353216              # float 1
.LCPI5_2:
	.long	1065353216              # float 1
	.long	1060320051              # float 0.699999988
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.text
	.globl	_ZN18btSliderConstraintC2ER11btRigidBodyRK11btTransformb
	.p2align	4, 0x90
	.type	_ZN18btSliderConstraintC2ER11btRigidBodyRK11btTransformb,@function
_ZN18btSliderConstraintC2ER11btRigidBodyRK11btTransformb: # @_ZN18btSliderConstraintC2ER11btRigidBodyRK11btTransformb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %rbp
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movl	$7, %esi
	movl	$_ZL7s_fixed, %edx
	movq	%rax, %rcx
	callq	_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_
	movq	$_ZTV18btSliderConstraint+16, (%rbx)
	movb	$0, 96(%rbx)
	movups	(%rbp), %xmm0
	movups	%xmm0, 164(%rbx)
	movups	16(%rbp), %xmm0
	movups	%xmm0, 180(%rbx)
	movups	32(%rbp), %xmm0
	movups	%xmm0, 196(%rbx)
	movups	48(%rbp), %xmm0
	movups	%xmm0, 212(%rbx)
	movb	%r14b, 228(%rbx)
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [1.000000e+00,-1.000000e+00,0.000000e+00,0.000000e+00]
	movups	%xmm0, 232(%rbx)
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,7.000000e-01,0.000000e+00,1.000000e+00]
	movups	%xmm0, 248(%rbx)
	movl	$1060320051, 264(%rbx)  # imm = 0x3F333333
	movl	$0, 268(%rbx)
	movl	$1065353216, 296(%rbx)  # imm = 0x3F800000
	movl	$1060320051, 300(%rbx)  # imm = 0x3F333333
	movl	$1065353216, 304(%rbx)  # imm = 0x3F800000
	movl	$1065353216, 308(%rbx)  # imm = 0x3F800000
	movl	$1060320051, 312(%rbx)  # imm = 0x3F333333
	movl	$1065353216, 316(%rbx)  # imm = 0x3F800000
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,7.000000e-01,1.000000e+00,1.000000e+00]
	movups	%xmm0, 272(%rbx)
	movl	$1060320051, 288(%rbx)  # imm = 0x3F333333
	movl	$1065353216, 292(%rbx)  # imm = 0x3F800000
	movb	$0, 1116(%rbx)
	movq	$0, 1136(%rbx)
	movl	$0, 1144(%rbx)
	movq	$0, 1125(%rbx)
	movq	$0, 1120(%rbx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN18btSliderConstraintC2ER11btRigidBodyRK11btTransformb, .Lfunc_end5-_ZN18btSliderConstraintC2ER11btRigidBodyRK11btTransformb
	.cfi_endproc

	.globl	_ZN18btSliderConstraint13buildJacobianEv
	.p2align	4, 0x90
	.type	_ZN18btSliderConstraint13buildJacobianEv,@function
_ZN18btSliderConstraint13buildJacobianEv: # @_ZN18btSliderConstraint13buildJacobianEv
	.cfi_startproc
# BB#0:
	cmpb	$0, 96(%rdi)
	je	.LBB6_3
# BB#1:
	cmpb	$0, 228(%rdi)
	movq	24(%rdi), %rax
	movq	32(%rdi), %rdx
	je	.LBB6_2
# BB#4:
	leaq	164(%rdi), %r8
	leaq	100(%rdi), %rcx
	movq	%rax, %rsi
	jmp	_ZN18btSliderConstraint16buildJacobianIntER11btRigidBodyS1_RK11btTransformS4_ # TAILCALL
.LBB6_3:
	retq
.LBB6_2:
	leaq	100(%rdi), %r8
	leaq	164(%rdi), %rcx
	movq	%rdx, %rsi
	movq	%rax, %rdx
	jmp	_ZN18btSliderConstraint16buildJacobianIntER11btRigidBodyS1_RK11btTransformS4_ # TAILCALL
.Lfunc_end6:
	.size	_ZN18btSliderConstraint13buildJacobianEv, .Lfunc_end6-_ZN18btSliderConstraint13buildJacobianEv
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI7_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI7_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI7_2:
	.zero	16
	.text
	.globl	_ZN18btSliderConstraint16buildJacobianIntER11btRigidBodyS1_RK11btTransformS4_
	.p2align	4, 0x90
	.type	_ZN18btSliderConstraint16buildJacobianIntER11btRigidBodyS1_RK11btTransformS4_,@function
_ZN18btSliderConstraint16buildJacobianIntER11btRigidBodyS1_RK11btTransformS4_: # @_ZN18btSliderConstraint16buildJacobianIntER11btRigidBodyS1_RK11btTransformS4_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	subq	$248, %rsp
.Lcfi28:
	.cfi_def_cfa_offset 304
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movss	8(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	(%rcx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm1, %xmm3
	movaps	%xmm3, 192(%rsp)        # 16-byte Spill
	movss	16(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm2, %xmm9
	movaps	%xmm9, 224(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm1
	movss	32(%rcx), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movss	16(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	mulss	%xmm2, %xmm0
	movaps	%xmm2, %xmm7
	movaps	%xmm7, 208(%rsp)        # 16-byte Spill
	addss	%xmm1, %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movaps	%xmm3, %xmm0
	mulss	%xmm10, %xmm0
	movss	20(%rcx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm1
	mulss	%xmm4, %xmm1
	addss	%xmm0, %xmm1
	movss	36(%rcx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movss	8(%rcx), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm8, %xmm0
	movss	24(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm3
	mulss	%xmm2, %xmm3
	addss	%xmm0, %xmm3
	movss	40(%rcx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm9, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movss	24(%r15), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	mulss	%xmm11, %xmm0
	movss	28(%r15), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	mulss	%xmm7, %xmm3
	movaps	%xmm7, 176(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm3
	movss	32(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm15
	mulss	%xmm1, %xmm15
	movaps	%xmm1, 160(%rsp)        # 16-byte Spill
	addss	%xmm3, %xmm15
	movaps	%xmm10, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm4, %xmm3
	mulss	%xmm7, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm12, %xmm13
	mulss	%xmm1, %xmm13
	addss	%xmm3, %xmm13
	movaps	%xmm8, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm7, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm9, %xmm7
	mulss	%xmm1, %xmm7
	addss	%xmm3, %xmm7
	movss	40(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	movss	44(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm6, %xmm5
	movss	48(%r15), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm14
	addss	%xmm5, %xmm14
	mulss	%xmm0, %xmm10
	mulss	%xmm3, %xmm4
	addss	%xmm10, %xmm4
	mulss	%xmm6, %xmm12
	addss	%xmm4, %xmm12
	mulss	%xmm0, %xmm8
	mulss	%xmm3, %xmm2
	addss	%xmm8, %xmm2
	mulss	%xmm6, %xmm9
	addss	%xmm2, %xmm9
	movaps	192(%rsp), %xmm2        # 16-byte Reload
	unpcklps	%xmm11, %xmm2   # xmm2 = xmm2[0],xmm11[0],xmm2[1],xmm11[1]
	movss	48(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	movaps	224(%rsp), %xmm4        # 16-byte Reload
	unpcklps	176(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	52(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm4, %xmm2
	addps	%xmm1, %xmm2
	movss	56(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	208(%rsp), %xmm4        # 16-byte Reload
	unpcklps	160(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	mulss	%xmm1, %xmm6
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	addps	%xmm2, %xmm1
	movsd	56(%r15), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	addss	%xmm3, %xmm0
	addss	%xmm6, %xmm0
	addss	64(%r15), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 844(%rbx)
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	movss	%xmm0, 848(%rbx)
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	movss	%xmm0, 852(%rbx)
	movl	$0, 856(%rbx)
	movss	%xmm15, 860(%rbx)
	movss	%xmm13, 864(%rbx)
	movss	%xmm7, 868(%rbx)
	movl	$0, 872(%rbx)
	movss	%xmm14, 876(%rbx)
	movss	%xmm12, 880(%rbx)
	movss	%xmm9, 884(%rbx)
	movl	$0, 888(%rbx)
	movlps	%xmm2, 892(%rbx)
	movlps	%xmm1, 900(%rbx)
	movss	8(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	(%r8), %xmm6            # xmm6 = mem[0],zero,zero,zero
	movss	4(%r8), %xmm10          # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm1, %xmm3
	movaps	%xmm3, 192(%rsp)        # 16-byte Spill
	movss	16(%r8), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm2, %xmm7
	movaps	%xmm7, 224(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm1
	movss	32(%r8), %xmm14         # xmm14 = mem[0],zero,zero,zero
	movss	16(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	mulss	%xmm2, %xmm0
	movaps	%xmm2, %xmm8
	movaps	%xmm8, 208(%rsp)        # 16-byte Spill
	addss	%xmm1, %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movaps	%xmm3, %xmm0
	mulss	%xmm10, %xmm0
	movss	20(%r8), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm1
	mulss	%xmm4, %xmm1
	addss	%xmm0, %xmm1
	movss	36(%r8), %xmm12         # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm0
	mulss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movss	8(%r8), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm9, %xmm0
	movss	24(%r8), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm3
	mulss	%xmm2, %xmm3
	addss	%xmm0, %xmm3
	movss	40(%r8), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm0
	mulss	%xmm7, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movss	24(%r14), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	mulss	%xmm11, %xmm0
	movss	28(%r14), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	mulss	%xmm8, %xmm3
	movaps	%xmm8, 176(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm3
	movss	32(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm15
	mulss	%xmm1, %xmm15
	movaps	%xmm1, 160(%rsp)        # 16-byte Spill
	addss	%xmm3, %xmm15
	movaps	%xmm10, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm4, %xmm3
	mulss	%xmm8, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm12, %xmm13
	mulss	%xmm1, %xmm13
	addss	%xmm3, %xmm13
	movaps	%xmm9, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm8, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm7, %xmm8
	mulss	%xmm1, %xmm8
	addss	%xmm3, %xmm8
	movss	40(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	movss	44(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm6, %xmm5
	movss	48(%r14), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm14
	addss	%xmm5, %xmm14
	mulss	%xmm0, %xmm10
	mulss	%xmm3, %xmm4
	addss	%xmm10, %xmm4
	mulss	%xmm6, %xmm12
	addss	%xmm4, %xmm12
	mulss	%xmm0, %xmm9
	mulss	%xmm3, %xmm2
	addss	%xmm9, %xmm2
	mulss	%xmm6, %xmm7
	addss	%xmm2, %xmm7
	movaps	192(%rsp), %xmm2        # 16-byte Reload
	unpcklps	%xmm11, %xmm2   # xmm2 = xmm2[0],xmm11[0],xmm2[1],xmm11[1]
	movss	48(%r8), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	movaps	224(%rsp), %xmm4        # 16-byte Reload
	unpcklps	176(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	52(%r8), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm4, %xmm2
	addps	%xmm1, %xmm2
	movaps	208(%rsp), %xmm4        # 16-byte Reload
	unpcklps	160(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	56(%r8), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm6
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	addps	%xmm2, %xmm1
	movsd	56(%r14), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	addss	%xmm3, %xmm0
	addss	%xmm6, %xmm0
	addss	64(%r14), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 908(%rbx)
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	movss	%xmm0, 912(%rbx)
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	movss	%xmm0, 916(%rbx)
	movl	$0, 920(%rbx)
	movss	%xmm15, 924(%rbx)
	movss	%xmm13, 928(%rbx)
	movss	%xmm8, 932(%rbx)
	movl	$0, 936(%rbx)
	movss	%xmm14, 940(%rbx)
	movss	%xmm12, 944(%rbx)
	movss	%xmm7, 948(%rbx)
	movl	$0, 952(%rbx)
	movlps	%xmm2, 956(%rbx)
	movlps	%xmm1, 964(%rbx)
	movups	892(%rbx), %xmm0
	movups	%xmm0, 988(%rbx)
	movups	956(%rbx), %xmm0
	movups	%xmm0, 1004(%rbx)
	movss	844(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	860(%rbx), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm4
	unpcklps	%xmm10, %xmm4   # xmm4 = xmm4[0],xmm10[0],xmm4[1],xmm10[1]
	movss	876(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movlps	%xmm4, 972(%rbx)
	movlps	%xmm2, 980(%rbx)
	movsd	1004(%rbx), %xmm7       # xmm7 = mem[0],zero
	movsd	988(%rbx), %xmm8        # xmm8 = mem[0],zero
	movaps	%xmm7, %xmm0
	subps	%xmm8, %xmm0
	movss	996(%rbx), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movss	1012(%rbx), %xmm1       # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm3
	subss	%xmm9, %xmm3
	xorps	%xmm6, %xmm6
	movss	%xmm3, %xmm6            # xmm6 = xmm3[0],xmm6[1,2,3]
	movlps	%xmm0, 1036(%rbx)
	movlps	%xmm6, 1044(%rbx)
	movaps	%xmm0, %xmm6
	mulss	%xmm5, %xmm0
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	mulss	%xmm10, %xmm6
	addss	%xmm0, %xmm6
	mulss	%xmm2, %xmm3
	addss	%xmm6, %xmm3
	mulss	%xmm3, %xmm2
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	addps	%xmm8, %xmm3
	addss	%xmm9, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm3, 1020(%rbx)
	movlps	%xmm0, 1028(%rbx)
	movsd	56(%r15), %xmm0         # xmm0 = mem[0],zero
	subps	%xmm0, %xmm3
	subss	64(%r15), %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm3, 1068(%rbx)
	movlps	%xmm0, 1076(%rbx)
	movsd	56(%r14), %xmm0         # xmm0 = mem[0],zero
	subps	%xmm0, %xmm7
	xorps	%xmm0, %xmm0
	subss	64(%r14), %xmm1
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movlps	%xmm7, 1084(%rbx)
	leaq	1068(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	1084(%rbx), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movlps	%xmm0, 1092(%rbx)
	leaq	428(%r15), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	428(%r14), %r12
	movq	$-252, %r13
	movl	$263, %ebp              # imm = 0x107
	.p2align	4, 0x90
.LBB7_1:                                # =>This Inner Loop Header: Depth=1
	movss	-192(%rbx,%rbp,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movss	-208(%rbx,%rbp,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movss	-176(%rbx,%rbp,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movlps	%xmm1, 16(%rsp)
	movlps	%xmm0, 24(%rsp)
	leaq	576(%rbx,%r13), %rdi
	movl	8(%r15), %eax
	movl	%eax, 112(%rsp)
	movl	24(%r15), %eax
	movl	%eax, 116(%rsp)
	movl	40(%r15), %eax
	movl	%eax, 120(%rsp)
	movl	$0, 124(%rsp)
	movl	12(%r15), %eax
	movl	%eax, 128(%rsp)
	movl	28(%r15), %eax
	movl	%eax, 132(%rsp)
	movl	44(%r15), %eax
	movl	%eax, 136(%rsp)
	movl	$0, 140(%rsp)
	movl	16(%r15), %eax
	movl	%eax, 144(%rsp)
	movl	32(%r15), %eax
	movl	%eax, 148(%rsp)
	movl	48(%r15), %eax
	movl	%eax, 152(%rsp)
	movl	$0, 156(%rsp)
	movl	8(%r14), %eax
	movl	%eax, 64(%rsp)
	movl	24(%r14), %eax
	movl	%eax, 68(%rsp)
	movl	40(%r14), %eax
	movl	%eax, 72(%rsp)
	movl	$0, 76(%rsp)
	movl	12(%r14), %eax
	movl	%eax, 80(%rsp)
	movl	28(%r14), %eax
	movl	%eax, 84(%rsp)
	movl	44(%r14), %eax
	movl	%eax, 88(%rsp)
	movl	$0, 92(%rsp)
	movl	16(%r14), %eax
	movl	%eax, 96(%rsp)
	movl	32(%r14), %eax
	movl	%eax, 100(%rsp)
	movl	48(%r14), %eax
	movl	%eax, 104(%rsp)
	movl	$0, 108(%rsp)
	movss	360(%r15), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	360(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	leaq	112(%rsp), %rsi
	leaq	64(%rsp), %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	leaq	16(%rsp), %r9
	pushq	%r12
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rsp)                # 8-byte Folded Reload
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	callq	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	movss	.LCPI7_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	addq	$16, %rsp
.Lcfi37:
	.cfi_adjust_cfa_offset -16
	divss	656(%rbx,%r13), %xmm0
	movss	%xmm0, -476(%rbx,%rbp,4)
	movss	1036(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movss	1040(%rbx), %xmm1       # xmm1 = mem[0],zero,zero,zero
	mulss	16(%rsp), %xmm0
	mulss	20(%rsp), %xmm1
	addss	%xmm0, %xmm1
	movss	1044(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	mulss	24(%rsp), %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rbx,%rbp,4)
	incq	%rbp
	addq	$84, %r13
	jne	.LBB7_1
# BB#2:
	movb	$0, 320(%rbx)
	movl	1052(%rbx), %eax
	movl	%eax, 1100(%rbx)
	movss	232(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	236(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm2
	jb	.LBB7_8
# BB#3:
	movd	%eax, %xmm1
	ucomiss	%xmm2, %xmm1
	jbe	.LBB7_6
# BB#4:
	subss	%xmm2, %xmm1
	jmp	.LBB7_5
.LBB7_6:
	ucomiss	%xmm1, %xmm0
	jbe	.LBB7_8
# BB#7:
	subss	%xmm0, %xmm1
.LBB7_5:                                # %_ZN18btSliderConstraint13testLinLimitsEv.exit.preheader
	movss	%xmm1, 1052(%rbx)
	movb	$1, 320(%rbx)
	jmp	.LBB7_9
.LBB7_8:
	movl	$0, 1052(%rbx)
.LBB7_9:                                # %_ZN18btSliderConstraint13testLinLimitsEv.exit.preheader
	movq	$-252, %rax
	movl	$219, %ecx
	movaps	.LCPI7_1(%rip), %xmm9   # xmm9 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB7_10:                               # %_ZN18btSliderConstraint13testLinLimitsEv.exit
                                        # =>This Inner Loop Header: Depth=1
	movss	-32(%rbx,%rcx,4), %xmm14 # xmm14 = mem[0],zero,zero,zero
	movss	-16(%rbx,%rcx,4), %xmm8 # xmm8 = mem[0],zero,zero,zero
	movss	(%rbx,%rcx,4), %xmm15   # xmm15 = mem[0],zero,zero,zero
	movsd	8(%r15), %xmm7          # xmm7 = mem[0],zero
	movsd	24(%r15), %xmm2         # xmm2 = mem[0],zero
	movsd	40(%r15), %xmm6         # xmm6 = mem[0],zero
	movsd	8(%r14), %xmm13         # xmm13 = mem[0],zero
	movsd	24(%r14), %xmm12        # xmm12 = mem[0],zero
	movsd	40(%r14), %xmm11        # xmm11 = mem[0],zero
	movss	16(%r14), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	16(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm0
	movss	32(%r15), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm5
	movaps	%xmm14, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm7, %xmm1
	movaps	%xmm8, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm2, %xmm4
	movss	48(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm2
	addps	%xmm1, %xmm4
	movaps	%xmm15, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm6, %xmm7
	movss	32(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addps	%xmm4, %xmm7
	movss	48(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm3
	movups	%xmm3, 840(%rbx,%rax)
	addss	%xmm0, %xmm5
	addss	%xmm2, %xmm5
	xorps	%xmm0, %xmm0
	movss	%xmm5, %xmm0            # xmm0 = xmm5[0],xmm0[1,2,3]
	movlps	%xmm7, 856(%rbx,%rax)
	movlps	%xmm0, 864(%rbx,%rax)
	movaps	%xmm14, %xmm6
	xorps	%xmm9, %xmm6
	pshufd	$224, %xmm6, %xmm0      # xmm0 = xmm6[0,0,2,3]
	mulps	%xmm13, %xmm0
	movaps	%xmm8, %xmm2
	xorps	%xmm9, %xmm2
	pshufd	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm12, %xmm2
	mulss	%xmm8, %xmm1
	mulss	%xmm15, %xmm4
	addps	%xmm0, %xmm2
	movaps	%xmm15, %xmm0
	xorps	%xmm9, %xmm0
	pshufd	$224, %xmm0, %xmm12     # xmm12 = xmm0[0,0,2,3]
	mulps	%xmm11, %xmm12
	addps	%xmm2, %xmm12
	mulss	%xmm10, %xmm6
	subss	%xmm1, %xmm6
	subss	%xmm4, %xmm6
	xorps	%xmm1, %xmm1
	movss	%xmm6, %xmm1            # xmm1 = xmm6[0],xmm1[1,2,3]
	movlps	%xmm12, 872(%rbx,%rax)
	movlps	%xmm1, 880(%rbx,%rax)
	movsd	428(%r15), %xmm1        # xmm1 = mem[0],zero
	mulps	%xmm7, %xmm1
	movss	436(%r15), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	xorps	%xmm4, %xmm4
	movss	%xmm2, %xmm4            # xmm4 = xmm2[0],xmm4[1,2,3]
	movlps	%xmm1, 888(%rbx,%rax)
	movlps	%xmm4, 896(%rbx,%rax)
	movsd	428(%r14), %xmm4        # xmm4 = mem[0],zero
	mulps	%xmm12, %xmm4
	movss	436(%r14), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm3
	xorps	%xmm0, %xmm0
	movss	%xmm3, %xmm0            # xmm0 = xmm3[0],xmm0[1,2,3]
	movlps	%xmm4, 904(%rbx,%rax)
	movlps	%xmm0, 912(%rbx,%rax)
	movaps	%xmm1, %xmm0
	mulss	%xmm7, %xmm1
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	860(%rbx,%rax), %xmm0
	addss	%xmm1, %xmm0
	mulss	%xmm5, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm4, %xmm0
	mulss	%xmm12, %xmm4
	shufps	$229, %xmm12, %xmm12    # xmm12 = xmm12[1,1,2,3]
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	%xmm12, %xmm0
	addss	%xmm4, %xmm0
	mulss	%xmm6, %xmm3
	addss	%xmm0, %xmm3
	addss	%xmm2, %xmm3
	movss	%xmm3, 920(%rbx,%rax)
	incq	%rcx
	addq	$84, %rax
	jne	.LBB7_10
# BB#11:
	unpcklps	%xmm8, %xmm14   # xmm14 = xmm14[0],xmm8[0],xmm14[1],xmm8[1]
	xorps	%xmm0, %xmm0
	movss	%xmm15, %xmm0           # xmm0 = xmm15[0],xmm0[1,2,3]
	movlps	%xmm14, 16(%rsp)
	movlps	%xmm0, 24(%rsp)
	movq	%rbx, %rdi
	callq	_ZN18btSliderConstraint13testAngLimitsEv
	movss	844(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	860(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	876(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	280(%r15), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	movss	296(%r15), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	addss	%xmm3, %xmm5
	movss	312(%r15), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm5, %xmm4
	movss	284(%r15), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	movss	300(%r15), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	addss	%xmm3, %xmm5
	movss	316(%r15), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	addss	%xmm5, %xmm6
	movss	288(%r15), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	movss	304(%r15), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	addss	%xmm3, %xmm5
	movss	320(%r15), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm5, %xmm3
	mulss	%xmm2, %xmm4
	mulss	%xmm1, %xmm6
	addss	%xmm4, %xmm6
	mulss	%xmm0, %xmm3
	addss	%xmm6, %xmm3
	movss	280(%r14), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	movss	296(%r14), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	addss	%xmm4, %xmm5
	movss	312(%r14), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm5, %xmm4
	movss	284(%r14), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	movss	300(%r14), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm6
	addss	%xmm5, %xmm6
	movss	316(%r14), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	addss	%xmm6, %xmm5
	movss	288(%r14), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm6
	movss	304(%r14), %xmm7        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	addss	%xmm6, %xmm7
	movss	320(%r14), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	addss	%xmm7, %xmm6
	mulss	%xmm2, %xmm4
	mulss	%xmm1, %xmm5
	addss	%xmm4, %xmm5
	mulss	%xmm0, %xmm6
	addss	%xmm5, %xmm6
	addss	%xmm3, %xmm6
	movss	.LCPI7_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm6, %xmm0
	movss	%xmm0, 1112(%rbx)
	movl	$0, 1128(%rbx)
	movl	$0, 1144(%rbx)
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_ZN18btSliderConstraint16buildJacobianIntER11btRigidBodyS1_RK11btTransformS4_, .Lfunc_end7-_ZN18btSliderConstraint16buildJacobianIntER11btRigidBodyS1_RK11btTransformS4_
	.cfi_endproc

	.section	.text._ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f,"axG",@progbits,_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f,comdat
	.weak	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	.p2align	4, 0x90
	.type	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f,@function
_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f: # @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	.cfi_startproc
# BB#0:
	movq	16(%rsp), %r10
	movq	8(%rsp), %rax
	movups	(%r9), %xmm2
	movups	%xmm2, (%rdi)
	movss	(%rcx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm11         # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm7
	mulss	%xmm11, %xmm7
	movss	8(%rcx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	(%rdi), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	4(%rdi), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm4
	mulss	%xmm10, %xmm4
	subss	%xmm4, %xmm7
	mulss	%xmm9, %xmm5
	movaps	%xmm11, %xmm4
	mulss	%xmm2, %xmm4
	subss	%xmm4, %xmm5
	mulss	%xmm10, %xmm2
	mulss	%xmm9, %xmm6
	subss	%xmm6, %xmm2
	movaps	%xmm7, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	16(%rsi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1]
	mulps	%xmm4, %xmm6
	movss	20(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	movaps	%xmm5, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	addps	%xmm6, %xmm4
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	24(%rsi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm12         # xmm12 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm12   # xmm12 = xmm12[0],xmm8[0],xmm12[1],xmm8[1]
	mulps	%xmm3, %xmm12
	addps	%xmm4, %xmm12
	mulss	32(%rsi), %xmm7
	mulss	36(%rsi), %xmm5
	addss	%xmm7, %xmm5
	mulss	40(%rsi), %xmm2
	addss	%xmm5, %xmm2
	xorps	%xmm8, %xmm8
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movlps	%xmm12, 16(%rdi)
	movlps	%xmm3, 24(%rdi)
	movss	(%r8), %xmm3            # xmm3 = mem[0],zero,zero,zero
	movss	4(%r8), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	mulss	%xmm11, %xmm5
	movss	8(%r8), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm6
	mulss	%xmm10, %xmm6
	subss	%xmm5, %xmm6
	mulss	%xmm9, %xmm7
	mulss	%xmm3, %xmm11
	subss	%xmm7, %xmm11
	mulss	%xmm10, %xmm3
	mulss	%xmm9, %xmm4
	subss	%xmm3, %xmm4
	movss	16(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%rdx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm9          # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm6, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	movss	20(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm9    # xmm9 = xmm9[0],xmm5[0],xmm9[1],xmm5[1]
	movaps	%xmm11, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm9, %xmm7
	addps	%xmm3, %xmm7
	movaps	%xmm4, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	24(%rdx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm9, %xmm5    # xmm5 = xmm5[0],xmm9[0],xmm5[1],xmm9[1]
	mulps	%xmm3, %xmm5
	addps	%xmm7, %xmm5
	mulss	32(%rdx), %xmm6
	mulss	36(%rdx), %xmm11
	addss	%xmm6, %xmm11
	mulss	40(%rdx), %xmm4
	addss	%xmm11, %xmm4
	xorps	%xmm3, %xmm3
	movss	%xmm4, %xmm3            # xmm3 = xmm4[0],xmm3[1,2,3]
	movlps	%xmm5, 32(%rdi)
	movlps	%xmm3, 40(%rdi)
	movsd	(%rax), %xmm9           # xmm9 = mem[0],zero
	mulps	%xmm12, %xmm9
	mulss	8(%rax), %xmm2
	xorps	%xmm6, %xmm6
	movss	%xmm2, %xmm6            # xmm6 = xmm2[0],xmm6[1,2,3]
	movlps	%xmm9, 48(%rdi)
	movlps	%xmm6, 56(%rdi)
	movsd	(%r10), %xmm6           # xmm6 = mem[0],zero
	mulps	%xmm5, %xmm6
	mulss	8(%r10), %xmm4
	movaps	%xmm6, %xmm7
	movlps	%xmm6, 64(%rdi)
	mulss	%xmm5, %xmm6
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	movss	%xmm4, %xmm8            # xmm8 = xmm4[0],xmm8[1,2,3]
	movlps	%xmm8, 72(%rdi)
	movss	16(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm3
	shufps	$229, %xmm9, %xmm9      # xmm9 = xmm9[1,1,2,3]
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	mulss	20(%rdi), %xmm9
	addss	%xmm3, %xmm9
	mulss	24(%rdi), %xmm2
	addss	%xmm9, %xmm2
	addss	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm5, %xmm7
	addss	%xmm6, %xmm7
	mulss	40(%rdi), %xmm4
	addss	%xmm7, %xmm4
	addss	%xmm2, %xmm4
	movss	%xmm4, 80(%rdi)
	retq
.Lfunc_end8:
	.size	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f, .Lfunc_end8-_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	.cfi_endproc

	.text
	.globl	_ZN18btSliderConstraint13testLinLimitsEv
	.p2align	4, 0x90
	.type	_ZN18btSliderConstraint13testLinLimitsEv,@function
_ZN18btSliderConstraint13testLinLimitsEv: # @_ZN18btSliderConstraint13testLinLimitsEv
	.cfi_startproc
# BB#0:
	movb	$0, 320(%rdi)
	movl	1052(%rdi), %eax
	movl	%eax, 1100(%rdi)
	movss	232(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	236(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm2
	jb	.LBB9_6
# BB#1:
	movd	%eax, %xmm1
	ucomiss	%xmm2, %xmm1
	jbe	.LBB9_4
# BB#2:
	subss	%xmm2, %xmm1
	jmp	.LBB9_3
.LBB9_4:
	ucomiss	%xmm1, %xmm0
	jbe	.LBB9_6
# BB#5:
	subss	%xmm0, %xmm1
.LBB9_3:
	movss	%xmm1, 1052(%rdi)
	movb	$1, 320(%rdi)
	retq
.LBB9_6:
	movl	$0, 1052(%rdi)
	retq
.Lfunc_end9:
	.size	_ZN18btSliderConstraint13testLinLimitsEv, .Lfunc_end9-_ZN18btSliderConstraint13testLinLimitsEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI10_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI10_4:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI10_1:
	.long	1061752795              # float 0.785398185
.LCPI10_2:
	.long	1075235812              # float 2.3561945
.LCPI10_3:
	.long	3209236443              # float -0.785398185
	.text
	.globl	_ZN18btSliderConstraint13testAngLimitsEv
	.p2align	4, 0x90
	.type	_ZN18btSliderConstraint13testAngLimitsEv,@function
_ZN18btSliderConstraint13testAngLimitsEv: # @_ZN18btSliderConstraint13testAngLimitsEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 16
.Lcfi39:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$0, 1108(%rbx)
	movb	$0, 321(%rbx)
	movss	240(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	244(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jb	.LBB10_7
# BB#1:
	movss	912(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	928(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	944(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	852(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movss	868(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm6
	addss	%xmm3, %xmm6
	movss	884(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	addss	%xmm6, %xmm3
	mulss	848(%rbx), %xmm0
	mulss	864(%rbx), %xmm5
	addss	%xmm0, %xmm5
	mulss	880(%rbx), %xmm4
	addss	%xmm5, %xmm4
	movaps	.LCPI10_0(%rip), %xmm5  # xmm5 = [nan,nan,nan,nan]
	andps	%xmm3, %xmm5
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm4
	jae	.LBB10_2
# BB#3:
	movaps	%xmm5, %xmm0
	addss	%xmm4, %xmm0
	subss	%xmm4, %xmm5
	divss	%xmm5, %xmm0
	movss	.LCPI10_2(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	jmp	.LBB10_4
.LBB10_2:
	movaps	%xmm4, %xmm0
	subss	%xmm5, %xmm0
	addss	%xmm4, %xmm5
	divss	%xmm5, %xmm0
	movss	.LCPI10_1(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
.LBB10_4:                               # %_Z11btAtan2Fastff.exit
	mulss	.LCPI10_3(%rip), %xmm0
	addss	%xmm4, %xmm0
	xorps	%xmm4, %xmm4
	cmpltss	%xmm4, %xmm3
	movaps	%xmm3, %xmm4
	andnps	%xmm0, %xmm4
	xorps	.LCPI10_4(%rip), %xmm0
	andps	%xmm3, %xmm0
	orps	%xmm4, %xmm0
	callq	_Z21btAdjustAngleToLimitsfff
	movss	%xmm0, 1104(%rbx)
	movss	240(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	ja	.LBB10_6
# BB#5:
	movss	244(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jbe	.LBB10_7
.LBB10_6:                               # %.sink.split
	subss	%xmm1, %xmm0
	movss	%xmm0, 1108(%rbx)
	movb	$1, 321(%rbx)
.LBB10_7:
	popq	%rbx
	retq
.Lfunc_end10:
	.size	_ZN18btSliderConstraint13testAngLimitsEv, .Lfunc_end10-_ZN18btSliderConstraint13testAngLimitsEv
	.cfi_endproc

	.globl	_ZN18btSliderConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.p2align	4, 0x90
	.type	_ZN18btSliderConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E,@function
_ZN18btSliderConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E: # @_ZN18btSliderConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 32
.Lcfi43:
	.cfi_offset %rbx, -24
.Lcfi44:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpb	$0, 96(%rbx)
	je	.LBB11_2
# BB#1:
	movq	$0, (%r14)
	jmp	.LBB11_11
.LBB11_2:
	movabsq	$8589934596, %rax       # imm = 0x200000004
	movq	%rax, (%r14)
	movq	24(%rbx), %rsi
	movq	32(%rbx), %rdx
	addq	$8, %rsi
	addq	$8, %rdx
	movq	%rbx, %rdi
	callq	_ZN18btSliderConstraint19calculateTransformsERK11btTransformS2_
	movb	$0, 320(%rbx)
	movl	1052(%rbx), %eax
	movl	%eax, 1100(%rbx)
	movss	232(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	236(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	jb	.LBB11_5
# BB#3:
	movd	%eax, %xmm1
	ucomiss	%xmm0, %xmm1
	ja	.LBB11_6
# BB#4:
	ucomiss	%xmm1, %xmm2
	movaps	%xmm2, %xmm0
	jbe	.LBB11_5
.LBB11_6:                               # %_ZN18btSliderConstraint13testLinLimitsEv.exit.thread.sink.split
	subss	%xmm0, %xmm1
	movss	%xmm1, 1052(%rbx)
	movb	$1, 320(%rbx)
	jmp	.LBB11_7
.LBB11_5:
	movl	$0, 1052(%rbx)
	cmpb	$0, 1116(%rbx)
	je	.LBB11_8
.LBB11_7:                               # %_ZN18btSliderConstraint13testLinLimitsEv.exit.thread
	incl	(%r14)
	decl	4(%r14)
.LBB11_8:
	movq	%rbx, %rdi
	callq	_ZN18btSliderConstraint13testAngLimitsEv
	cmpb	$0, 321(%rbx)
	jne	.LBB11_10
# BB#9:
	cmpb	$0, 1132(%rbx)
	je	.LBB11_11
.LBB11_10:
	incl	(%r14)
	decl	4(%r14)
.LBB11_11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end11:
	.size	_ZN18btSliderConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E, .Lfunc_end11-_ZN18btSliderConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.cfi_endproc

	.globl	_ZN18btSliderConstraint19calculateTransformsERK11btTransformS2_
	.p2align	4, 0x90
	.type	_ZN18btSliderConstraint19calculateTransformsERK11btTransformS2_,@function
_ZN18btSliderConstraint19calculateTransformsERK11btTransformS2_: # @_ZN18btSliderConstraint19calculateTransformsERK11btTransformS2_
	.cfi_startproc
# BB#0:
	subq	$88, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 96
	cmpb	$0, 228(%rdi)
	je	.LBB12_1
.LBB12_2:
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	100(%rdi), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	104(%rdi), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm1, %xmm3
	movaps	%xmm3, -48(%rsp)        # 16-byte Spill
	movss	116(%rdi), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm2, %xmm9
	movaps	%xmm9, -16(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm1
	movss	132(%rdi), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	mulss	%xmm2, %xmm0
	movaps	%xmm2, %xmm7
	movaps	%xmm7, -32(%rsp)        # 16-byte Spill
	addss	%xmm1, %xmm0
	movss	%xmm0, -112(%rsp)       # 4-byte Spill
	movaps	%xmm3, %xmm0
	mulss	%xmm10, %xmm0
	movss	120(%rdi), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm1
	mulss	%xmm4, %xmm1
	addss	%xmm0, %xmm1
	movss	136(%rdi), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, -128(%rsp)       # 16-byte Spill
	movss	108(%rdi), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm8, %xmm0
	movss	124(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm3
	mulss	%xmm2, %xmm3
	addss	%xmm0, %xmm3
	movss	140(%rdi), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm9, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm0, -64(%rsp)        # 16-byte Spill
	movss	16(%rsi), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	mulss	%xmm11, %xmm0
	movss	20(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	mulss	%xmm7, %xmm3
	movaps	%xmm7, -80(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm3
	movss	24(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm15
	mulss	%xmm1, %xmm15
	movaps	%xmm1, -96(%rsp)        # 16-byte Spill
	addss	%xmm3, %xmm15
	movaps	%xmm10, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm4, %xmm3
	mulss	%xmm7, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm12, %xmm13
	mulss	%xmm1, %xmm13
	addss	%xmm3, %xmm13
	movaps	%xmm8, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm7, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm9, %xmm7
	mulss	%xmm1, %xmm7
	addss	%xmm3, %xmm7
	movss	32(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	movss	36(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm6, %xmm5
	movss	40(%rsi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm14
	addss	%xmm5, %xmm14
	mulss	%xmm0, %xmm10
	mulss	%xmm3, %xmm4
	addss	%xmm10, %xmm4
	mulss	%xmm6, %xmm12
	addss	%xmm4, %xmm12
	mulss	%xmm0, %xmm8
	mulss	%xmm3, %xmm2
	addss	%xmm8, %xmm2
	mulss	%xmm6, %xmm9
	addss	%xmm2, %xmm9
	movaps	-48(%rsp), %xmm2        # 16-byte Reload
	unpcklps	%xmm11, %xmm2   # xmm2 = xmm2[0],xmm11[0],xmm2[1],xmm11[1]
	movss	148(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	movaps	-16(%rsp), %xmm4        # 16-byte Reload
	unpcklps	-80(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	152(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm4, %xmm2
	addps	%xmm1, %xmm2
	movss	156(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movaps	-32(%rsp), %xmm4        # 16-byte Reload
	unpcklps	-96(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	mulss	%xmm1, %xmm6
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	addps	%xmm2, %xmm1
	movsd	48(%rsi), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	addss	%xmm3, %xmm0
	addss	%xmm6, %xmm0
	addss	56(%rsi), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movss	-112(%rsp), %xmm0       # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 844(%rdi)
	movaps	-128(%rsp), %xmm0       # 16-byte Reload
	movss	%xmm0, 848(%rdi)
	movaps	-64(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 852(%rdi)
	movl	$0, 856(%rdi)
	movss	%xmm15, 860(%rdi)
	movss	%xmm13, 864(%rdi)
	movss	%xmm7, 868(%rdi)
	movl	$0, 872(%rdi)
	movss	%xmm14, 876(%rdi)
	movss	%xmm12, 880(%rdi)
	movss	%xmm9, 884(%rdi)
	movl	$0, 888(%rdi)
	movlps	%xmm2, 892(%rdi)
	movlps	%xmm1, 900(%rdi)
	movss	(%rdx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	164(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	168(%rdi), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm11
	movss	180(%rdi), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	196(%rdi), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	mulss	%xmm6, %xmm1
	movaps	%xmm1, -80(%rsp)        # 16-byte Spill
	movaps	%xmm2, %xmm1
	mulss	%xmm8, %xmm1
	movaps	%xmm1, -96(%rsp)        # 16-byte Spill
	movss	200(%rdi), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	movaps	%xmm1, -112(%rsp)       # 16-byte Spill
	movss	24(%rdx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	mulss	%xmm10, %xmm1
	movaps	%xmm1, 64(%rsp)         # 16-byte Spill
	movss	32(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	movss	36(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm3
	mulss	%xmm5, %xmm3
	movaps	%xmm5, %xmm15
	addss	%xmm0, %xmm3
	movss	40(%rdx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm4
	addss	%xmm3, %xmm4
	movss	%xmm4, -16(%rsp)        # 4-byte Spill
	movaps	%xmm9, %xmm0
	mulss	%xmm10, %xmm0
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movss	204(%rdi), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	mulss	%xmm10, %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movaps	%xmm8, %xmm0
	movaps	%xmm0, -128(%rsp)       # 16-byte Spill
	mulss	%xmm1, %xmm8
	movss	184(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm10   # xmm10 = xmm10[0],xmm0[0],xmm10[1],xmm0[1]
	movaps	%xmm6, %xmm3
	movaps	%xmm6, %xmm12
	unpcklps	%xmm7, %xmm6    # xmm6 = xmm6[0],xmm7[0],xmm6[1],xmm7[1]
	movss	188(%rdi), %xmm14       # xmm14 = mem[0],zero,zero,zero
	unpcklps	%xmm14, %xmm7   # xmm7 = xmm7[0],xmm14[0],xmm7[1],xmm14[1]
	movaps	%xmm7, (%rsp)           # 16-byte Spill
	movaps	%xmm14, %xmm7
	unpcklps	%xmm0, %xmm14   # xmm14 = xmm14[0],xmm0[0],xmm14[1],xmm0[1]
	movss	%xmm15, -64(%rsp)       # 4-byte Spill
	mulss	%xmm15, %xmm0
	addss	%xmm8, %xmm0
	mulss	%xmm9, %xmm3
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	mulss	%xmm13, %xmm9
	addss	%xmm0, %xmm9
	movss	%xmm9, -32(%rsp)        # 4-byte Spill
	movss	172(%rdi), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	mulss	%xmm4, %xmm3
	movaps	%xmm4, %xmm0
	mulss	%xmm1, %xmm4
	mulss	%xmm15, %xmm7
	addss	%xmm4, %xmm7
	mulss	%xmm5, %xmm12
	mulss	%xmm13, %xmm5
	addss	%xmm7, %xmm5
	movss	%xmm5, -48(%rsp)        # 4-byte Spill
	movaps	%xmm11, %xmm5
	mulss	%xmm2, %xmm5
	movss	212(%rdi), %xmm8        # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm2
	movss	4(%rdx), %xmm11         # xmm11 = mem[0],zero,zero,zero
	movss	216(%rdi), %xmm15       # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm9
	mulss	%xmm15, %xmm9
	addss	%xmm2, %xmm9
	movss	16(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	-112(%rsp), %xmm2       # 16-byte Reload
	mulss	%xmm4, %xmm2
	movaps	%xmm2, -112(%rsp)       # 16-byte Spill
	movaps	-128(%rsp), %xmm2       # 16-byte Reload
	mulss	%xmm4, %xmm2
	movaps	%xmm2, -128(%rsp)       # 16-byte Spill
	mulss	%xmm4, %xmm0
	mulss	%xmm8, %xmm4
	movss	20(%rdx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm2
	mulss	%xmm15, %xmm2
	addss	%xmm4, %xmm2
	unpcklps	%xmm10, %xmm6   # xmm6 = xmm6[0],xmm10[0],xmm6[1],xmm10[1]
	unpcklps	(%rsp), %xmm14  # 16-byte Folded Reload
                                        # xmm14 = xmm14[0],mem[0],xmm14[1],mem[1]
	movss	220(%rdi), %xmm10       # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm13
	shufps	$0, %xmm11, %xmm10      # xmm10 = xmm10[0,0],xmm11[0,0]
	unpcklps	%xmm7, %xmm11   # xmm11 = xmm11[0],xmm7[0],xmm11[1],xmm7[1]
	unpcklps	%xmm7, %xmm7    # xmm7 = xmm7[0,0,1,1]
	unpcklps	%xmm7, %xmm11   # xmm11 = xmm11[0],xmm7[0],xmm11[1],xmm7[1]
	mulps	%xmm14, %xmm11
	mulps	%xmm6, %xmm10
	movaps	-112(%rsp), %xmm4       # 16-byte Reload
	unpcklps	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	unpcklps	-128(%rsp), %xmm3 # 16-byte Folded Reload
                                        # xmm3 = xmm3[0],mem[0],xmm3[1],mem[1]
	unpcklps	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	unpcklps	-96(%rsp), %xmm2 # 16-byte Folded Reload
                                        # xmm2 = xmm2[0],mem[0],xmm2[1],mem[1]
	unpcklps	%xmm5, %xmm9    # xmm9 = xmm9[0],xmm5[0],xmm9[1],xmm5[1]
	unpcklps	%xmm2, %xmm9    # xmm9 = xmm9[0],xmm2[0],xmm9[1],xmm2[1]
	addps	%xmm10, %xmm9
	addps	%xmm11, %xmm3
	movaps	64(%rsp), %xmm0         # 16-byte Reload
	unpcklps	32(%rsp), %xmm0 # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	unpcklps	48(%rsp), %xmm12 # 16-byte Folded Reload
                                        # xmm12 = xmm12[0],mem[0],xmm12[1],mem[1]
	unpcklps	%xmm0, %xmm12   # xmm12 = xmm12[0],xmm0[0],xmm12[1],xmm0[1]
	movss	52(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	16(%rsp), %xmm0 # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	movss	48(%rdx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	-80(%rsp), %xmm6 # 16-byte Folded Reload
                                        # xmm6 = xmm6[0],mem[0],xmm6[1],mem[1]
	unpcklps	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	movss	-64(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addps	%xmm3, %xmm12
	addps	%xmm9, %xmm6
	mulss	%xmm8, %xmm1
	mulss	%xmm15, %xmm0
	addss	%xmm1, %xmm0
	leaq	892(%rdi), %rax
	jmp	.LBB12_4
.LBB12_1:
	cmpb	$0, 96(%rdi)
	je	.LBB12_2
# BB#3:
	movss	(%rdx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	164(%rdi), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	168(%rdi), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	mulss	%xmm3, %xmm0
	movss	180(%rdi), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm2
	mulss	%xmm5, %xmm2
	addss	%xmm0, %xmm2
	movss	196(%rdi), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	mulss	%xmm1, %xmm0
	addss	%xmm2, %xmm0
	movss	%xmm0, -112(%rsp)       # 4-byte Spill
	movaps	%xmm3, %xmm0
	movaps	%xmm3, %xmm8
	movaps	%xmm8, -48(%rsp)        # 16-byte Spill
	mulss	%xmm9, %xmm0
	movss	184(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	movaps	%xmm5, -16(%rsp)        # 16-byte Spill
	mulss	%xmm2, %xmm3
	addss	%xmm0, %xmm3
	movss	200(%rdi), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	movaps	%xmm1, %xmm11
	movaps	%xmm11, -32(%rsp)       # 16-byte Spill
	mulss	%xmm12, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm0, -128(%rsp)       # 16-byte Spill
	movss	172(%rdi), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm3
	mulss	%xmm7, %xmm3
	movss	188(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	addss	%xmm3, %xmm5
	movss	204(%rdi), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm3
	mulss	%xmm10, %xmm3
	addss	%xmm5, %xmm3
	movaps	%xmm3, -64(%rsp)        # 16-byte Spill
	movss	16(%rdx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm3
	mulss	%xmm11, %xmm3
	movss	20(%rdx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	mulss	%xmm8, %xmm5
	movaps	%xmm8, -80(%rsp)        # 16-byte Spill
	addss	%xmm3, %xmm5
	movss	24(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm15
	mulss	%xmm0, %xmm15
	movaps	%xmm0, -96(%rsp)        # 16-byte Spill
	addss	%xmm5, %xmm15
	movaps	%xmm9, %xmm3
	mulss	%xmm11, %xmm3
	movaps	%xmm2, %xmm5
	mulss	%xmm8, %xmm5
	addss	%xmm3, %xmm5
	movaps	%xmm12, %xmm13
	mulss	%xmm0, %xmm13
	addss	%xmm5, %xmm13
	movaps	%xmm7, %xmm3
	mulss	%xmm11, %xmm3
	movaps	%xmm1, %xmm5
	mulss	%xmm8, %xmm5
	addss	%xmm3, %xmm5
	movaps	%xmm10, %xmm8
	mulss	%xmm0, %xmm8
	addss	%xmm5, %xmm8
	movss	32(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm6
	movss	36(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	addss	%xmm6, %xmm4
	movss	40(%rdx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm14
	addss	%xmm4, %xmm14
	mulss	%xmm5, %xmm9
	mulss	%xmm3, %xmm2
	addss	%xmm9, %xmm2
	mulss	%xmm6, %xmm12
	addss	%xmm2, %xmm12
	mulss	%xmm5, %xmm7
	mulss	%xmm3, %xmm1
	addss	%xmm7, %xmm1
	mulss	%xmm6, %xmm10
	addss	%xmm1, %xmm10
	movaps	-48(%rsp), %xmm1        # 16-byte Reload
	unpcklps	%xmm11, %xmm1   # xmm1 = xmm1[0],xmm11[0],xmm1[1],xmm11[1]
	movss	212(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm1, %xmm0
	movaps	-16(%rsp), %xmm2        # 16-byte Reload
	unpcklps	-80(%rsp), %xmm2 # 16-byte Folded Reload
                                        # xmm2 = xmm2[0],mem[0],xmm2[1],mem[1]
	movss	216(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	addps	%xmm0, %xmm1
	movss	220(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	-32(%rsp), %xmm2        # 16-byte Reload
	unpcklps	-96(%rsp), %xmm2 # 16-byte Folded Reload
                                        # xmm2 = xmm2[0],mem[0],xmm2[1],mem[1]
	mulss	%xmm0, %xmm6
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	addps	%xmm1, %xmm0
	movsd	48(%rdx), %xmm1         # xmm1 = mem[0],zero
	addps	%xmm0, %xmm1
	addss	%xmm3, %xmm5
	addss	%xmm6, %xmm5
	addss	56(%rdx), %xmm5
	xorps	%xmm0, %xmm0
	movss	%xmm5, %xmm0            # xmm0 = xmm5[0],xmm0[1,2,3]
	movss	-112(%rsp), %xmm2       # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 844(%rdi)
	movaps	-128(%rsp), %xmm2       # 16-byte Reload
	movss	%xmm2, 848(%rdi)
	movaps	-64(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 852(%rdi)
	movl	$0, 856(%rdi)
	movss	%xmm15, 860(%rdi)
	movss	%xmm13, 864(%rdi)
	movss	%xmm8, 868(%rdi)
	movl	$0, 872(%rdi)
	movss	%xmm14, 876(%rdi)
	movss	%xmm12, 880(%rdi)
	movss	%xmm10, 884(%rdi)
	movl	$0, 888(%rdi)
	movlps	%xmm1, 892(%rdi)
	movlps	%xmm0, 900(%rdi)
	movss	(%rsi), %xmm11          # xmm11 = mem[0],zero,zero,zero
	movss	100(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	104(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm10
	movss	116(%rdi), %xmm15       # xmm15 = mem[0],zero,zero,zero
	movss	132(%rdi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm1
	mulss	%xmm9, %xmm1
	movaps	%xmm1, -80(%rsp)        # 16-byte Spill
	movaps	%xmm11, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm1, -96(%rsp)        # 16-byte Spill
	movss	136(%rdi), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	movaps	%xmm1, -112(%rsp)       # 16-byte Spill
	movss	24(%rsi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm1
	mulss	%xmm8, %xmm1
	movaps	%xmm1, 64(%rsp)         # 16-byte Spill
	movss	32(%rsi), %xmm14        # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm0
	movss	36(%rsi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm1
	mulss	%xmm6, %xmm1
	movaps	%xmm6, %xmm7
	addss	%xmm0, %xmm1
	movss	40(%rsi), %xmm13        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm3
	addss	%xmm1, %xmm3
	movss	%xmm3, -16(%rsp)        # 4-byte Spill
	movaps	%xmm4, %xmm0
	mulss	%xmm8, %xmm0
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movss	140(%rdi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm8, %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movaps	%xmm2, %xmm0
	movaps	%xmm0, -128(%rsp)       # 16-byte Spill
	mulss	%xmm14, %xmm2
	movss	120(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm8    # xmm8 = xmm8[0],xmm0[0],xmm8[1],xmm0[1]
	movaps	%xmm9, %xmm5
	movaps	%xmm9, %xmm12
	unpcklps	%xmm15, %xmm9   # xmm9 = xmm9[0],xmm15[0],xmm9[1],xmm15[1]
	movss	124(%rdi), %xmm6        # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm15   # xmm15 = xmm15[0],xmm6[0],xmm15[1],xmm6[1]
	movaps	%xmm6, %xmm1
	unpcklps	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	movss	%xmm7, -64(%rsp)        # 4-byte Spill
	mulss	%xmm7, %xmm0
	addss	%xmm2, %xmm0
	mulss	%xmm4, %xmm5
	movaps	%xmm5, 16(%rsp)         # 16-byte Spill
	mulss	%xmm13, %xmm4
	addss	%xmm0, %xmm4
	movss	%xmm4, -32(%rsp)        # 4-byte Spill
	movss	108(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm2
	mulss	%xmm0, %xmm2
	movaps	%xmm0, %xmm5
	mulss	%xmm14, %xmm0
	mulss	%xmm7, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm3, %xmm12
	mulss	%xmm13, %xmm3
	addss	%xmm1, %xmm3
	movss	%xmm3, -48(%rsp)        # 4-byte Spill
	mulss	%xmm11, %xmm10
	movaps	%xmm10, (%rsp)          # 16-byte Spill
	movss	148(%rdi), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm11
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	152(%rdi), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	mulss	%xmm10, %xmm0
	addss	%xmm11, %xmm0
	movss	16(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	-112(%rsp), %xmm3       # 16-byte Reload
	mulss	%xmm7, %xmm3
	movaps	%xmm3, -112(%rsp)       # 16-byte Spill
	movaps	-128(%rsp), %xmm3       # 16-byte Reload
	mulss	%xmm7, %xmm3
	movaps	%xmm3, -128(%rsp)       # 16-byte Spill
	mulss	%xmm7, %xmm5
	mulss	%xmm4, %xmm7
	movss	20(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm11
	mulss	%xmm10, %xmm11
	addss	%xmm7, %xmm11
	unpcklps	%xmm8, %xmm9    # xmm9 = xmm9[0],xmm8[0],xmm9[1],xmm8[1]
	unpcklps	%xmm15, %xmm6   # xmm6 = xmm6[0],xmm15[0],xmm6[1],xmm15[1]
	movss	156(%rdi), %xmm7        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm13
	shufps	$0, %xmm1, %xmm7        # xmm7 = xmm7[0,0],xmm1[0,0]
	unpcklps	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	unpcklps	%xmm3, %xmm3    # xmm3 = xmm3[0,0,1,1]
	unpcklps	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	mulps	%xmm6, %xmm1
	mulps	%xmm9, %xmm7
	movaps	-112(%rsp), %xmm3       # 16-byte Reload
	unpcklps	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	unpcklps	-128(%rsp), %xmm2 # 16-byte Folded Reload
                                        # xmm2 = xmm2[0],mem[0],xmm2[1],mem[1]
	unpcklps	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	unpcklps	-96(%rsp), %xmm11 # 16-byte Folded Reload
                                        # xmm11 = xmm11[0],mem[0],xmm11[1],mem[1]
	unpcklps	(%rsp), %xmm0   # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	unpcklps	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1]
	addps	%xmm7, %xmm0
	addps	%xmm1, %xmm2
	movaps	64(%rsp), %xmm1         # 16-byte Reload
	unpcklps	32(%rsp), %xmm1 # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	unpcklps	48(%rsp), %xmm12 # 16-byte Folded Reload
                                        # xmm12 = xmm12[0],mem[0],xmm12[1],mem[1]
	unpcklps	%xmm1, %xmm12   # xmm12 = xmm12[0],xmm1[0],xmm12[1],xmm1[1]
	movss	52(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	16(%rsp), %xmm1 # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	movss	48(%rsi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	-80(%rsp), %xmm6 # 16-byte Folded Reload
                                        # xmm6 = xmm6[0],mem[0],xmm6[1],mem[1]
	unpcklps	%xmm1, %xmm6    # xmm6 = xmm6[0],xmm1[0],xmm6[1],xmm1[1]
	addps	%xmm2, %xmm12
	addps	%xmm0, %xmm6
	movss	-64(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm14
	mulss	%xmm10, %xmm0
	addss	%xmm14, %xmm0
	leaq	892(%rdi), %rax
	movq	%rsi, %rdx
.LBB12_4:
	addss	%xmm0, %xmm13
	addss	56(%rdx), %xmm13
	xorps	%xmm0, %xmm0
	movss	%xmm13, %xmm0           # xmm0 = xmm13[0],xmm0[1,2,3]
	movaps	%xmm6, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	movss	%xmm1, 908(%rdi)
	movaps	%xmm6, %xmm1
	shufps	$231, %xmm1, %xmm1      # xmm1 = xmm1[3,1,2,3]
	movss	%xmm1, 912(%rdi)
	movss	%xmm12, 916(%rdi)
	movl	$0, 920(%rdi)
	movaps	%xmm12, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	%xmm1, 924(%rdi)
	movaps	%xmm12, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	movss	%xmm1, 928(%rdi)
	shufps	$231, %xmm12, %xmm12    # xmm12 = xmm12[3,1,2,3]
	movss	%xmm12, 932(%rdi)
	movl	$0, 936(%rdi)
	movss	-16(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 940(%rdi)
	movss	-32(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 944(%rdi)
	movss	-48(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 948(%rdi)
	movl	$0, 952(%rdi)
	movlps	%xmm6, 956(%rdi)
	movlps	%xmm0, 964(%rdi)
	leaq	988(%rdi), %rcx
	movups	(%rax), %xmm0
	movups	%xmm0, 988(%rdi)
	leaq	1004(%rdi), %rax
	movups	956(%rdi), %xmm0
	movups	%xmm0, 1004(%rdi)
	movss	844(%rdi), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movss	860(%rdi), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm3
	unpcklps	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	movss	876(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movlps	%xmm3, 972(%rdi)
	movlps	%xmm2, 980(%rdi)
	cmpb	$0, 228(%rdi)
	jne	.LBB12_7
# BB#5:
	cmpb	$0, 96(%rdi)
	je	.LBB12_6
.LBB12_7:
	movq	%rax, %rdx
.LBB12_8:
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	subps	%xmm0, %xmm1
	movss	8(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	subss	8(%rcx), %xmm0
	movaps	%xmm1, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	xorps	%xmm7, %xmm7
	xorps	%xmm6, %xmm6
	movss	%xmm0, %xmm6            # xmm6 = xmm0[0],xmm6[1,2,3]
	movlps	%xmm1, 1036(%rdi)
	movlps	%xmm6, 1044(%rdi)
	mulss	%xmm1, %xmm8
	mulss	%xmm4, %xmm5
	addss	%xmm8, %xmm5
	movaps	%xmm2, %xmm6
	mulss	%xmm0, %xmm6
	addss	%xmm5, %xmm6
	movaps	%xmm6, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm5, %xmm3
	mulss	%xmm6, %xmm2
	movsd	988(%rdi), %xmm5        # xmm5 = mem[0],zero
	addps	%xmm3, %xmm5
	addss	996(%rdi), %xmm2
	movss	%xmm2, %xmm7            # xmm7 = xmm2[0],xmm7[1,2,3]
	movlps	%xmm5, 1020(%rdi)
	movlps	%xmm7, 1028(%rdi)
	movss	%xmm6, 1052(%rdi)
	movss	848(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	movss	864(%rdi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	addss	%xmm2, %xmm3
	movss	880(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, 1056(%rdi)
	movss	852(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	movss	868(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	addss	%xmm2, %xmm1
	mulss	884(%rdi), %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, 1060(%rdi)
	addq	$88, %rsp
	retq
.LBB12_6:
	movq	%rcx, %rdx
	movq	%rax, %rcx
	jmp	.LBB12_8
.Lfunc_end12:
	.size	_ZN18btSliderConstraint19calculateTransformsERK11btTransformS2_, .Lfunc_end12-_ZN18btSliderConstraint19calculateTransformsERK11btTransformS2_
	.cfi_endproc

	.globl	_ZN18btSliderConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E
	.p2align	4, 0x90
	.type	_ZN18btSliderConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E,@function
_ZN18btSliderConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E: # @_ZN18btSliderConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E
	.cfi_startproc
# BB#0:
	movq	$6, (%rsi)
	retq
.Lfunc_end13:
	.size	_ZN18btSliderConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E, .Lfunc_end13-_ZN18btSliderConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E
	.cfi_endproc

	.globl	_ZN18btSliderConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.p2align	4, 0x90
	.type	_ZN18btSliderConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E,@function
_ZN18btSliderConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E: # @_ZN18btSliderConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rax
	movq	32(%rdi), %r9
	movss	360(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	leaq	8(%rax), %rdx
	leaq	328(%rax), %r8
	movss	360(%r9), %xmm1         # xmm1 = mem[0],zero,zero,zero
	leaq	8(%r9), %rcx
	leaq	328(%r9), %r9
	jmp	_ZN18btSliderConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_ff # TAILCALL
.Lfunc_end14:
	.size	_ZN18btSliderConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E, .Lfunc_end14-_ZN18btSliderConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI15_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI15_7:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI15_1:
	.long	1056964608              # float 0.5
.LCPI15_2:
	.long	1065185444              # float 0.990000009
.LCPI15_3:
	.long	1008981770              # float 0.00999999977
.LCPI15_5:
	.long	1065353216              # float 1
.LCPI15_6:
	.long	2139095039              # float 3.40282347E+38
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	2
.LCPI15_4:
	.long	3212836864              # float -1
	.long	1065353216              # float 1
	.text
	.globl	_ZN18btSliderConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_ff
	.p2align	4, 0x90
	.type	_ZN18btSliderConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_ff,@function
_ZN18btSliderConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_ff: # @_ZN18btSliderConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_ff
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi52:
	.cfi_def_cfa_offset 176
.Lcfi53:
	.cfi_offset %rbx, -56
.Lcfi54:
	.cfi_offset %r12, -48
.Lcfi55:
	.cfi_offset %r13, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movss	%xmm1, 80(%rsp)         # 4-byte Spill
	movss	%xmm0, 64(%rsp)         # 4-byte Spill
	movq	%r9, %r15
	movq	%r8, %r12
	movq	%rcx, %r13
	movq	%rdx, %rbp
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	%rbp, %rsi
	movq	%r13, %rdx
	callq	_ZN18btSliderConstraint19calculateTransformsERK11btTransformS2_
	movb	$0, 320(%rbx)
	movl	1052(%rbx), %eax
	movl	%eax, 1100(%rbx)
	movss	232(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	236(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm2
	jae	.LBB15_1
.LBB15_6:
	movl	$0, 1052(%rbx)
	jmp	.LBB15_7
.LBB15_1:
	movd	%eax, %xmm1
	ucomiss	%xmm2, %xmm1
	jbe	.LBB15_4
# BB#2:
	subss	%xmm2, %xmm1
	jmp	.LBB15_3
.LBB15_4:
	ucomiss	%xmm1, %xmm0
	jbe	.LBB15_6
# BB#5:
	subss	%xmm0, %xmm1
.LBB15_3:                               # %_ZN18btSliderConstraint13testLinLimitsEv.exit
	movss	%xmm1, 1052(%rbx)
	movb	$1, 320(%rbx)
.LBB15_7:                               # %_ZN18btSliderConstraint13testLinLimitsEv.exit
	movq	%rbx, %rdi
	callq	_ZN18btSliderConstraint13testAngLimitsEv
	movslq	40(%r14), %rax
	movb	228(%rbx), %cl
	movss	844(%rbx), %xmm15       # xmm15 = mem[0],zero,zero,zero
	movss	860(%rbx), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movss	876(%rbx), %xmm13       # xmm13 = mem[0],zero,zero,zero
	movss	848(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	864(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movss	880(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movlps	%xmm2, 32(%rsp)
	movlps	%xmm3, 40(%rsp)
	movss	852(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	868(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movss	884(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movlps	%xmm4, 16(%rsp)
	movlps	%xmm3, 24(%rsp)
	movq	16(%r14), %rdx
	movss	%xmm0, (%rdx)
	movss	%xmm1, 4(%rdx)
	movl	40(%rsp), %esi
	movl	%esi, 8(%rdx)
	movss	%xmm2, (%rdx,%rax,4)
	movl	20(%rsp), %esi
	movl	%esi, 4(%rdx,%rax,4)
	movl	24(%rsp), %esi
	movl	%esi, 8(%rdx,%rax,4)
	movss	32(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	.LCPI15_0(%rip), %xmm1  # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm4, %xmm0
	xorps	%xmm1, %xmm0
	movq	32(%r14), %rdx
	movss	%xmm0, (%rdx)
	movss	36(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	xorps	%xmm1, %xmm0
	movss	%xmm0, 4(%rdx)
	movss	40(%rsp), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	xorps	%xmm1, %xmm0
	movss	%xmm0, 8(%rdx)
	movss	16(%rsp), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm0
	xorps	%xmm1, %xmm0
	movss	%xmm0, (%rdx,%rax,4)
	movss	20(%rsp), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm0
	xorps	%xmm1, %xmm0
	movss	%xmm0, 4(%rdx,%rax,4)
	movss	24(%rsp), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm0
	xorps	%xmm1, %xmm0
	movss	%xmm0, 8(%rdx,%rax,4)
	movss	908(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	924(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	940(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm7
	mulss	%xmm2, %xmm7
	movaps	%xmm13, %xmm0
	mulss	%xmm3, %xmm0
	subss	%xmm0, %xmm7
	movaps	%xmm13, %xmm0
	mulss	%xmm1, %xmm0
	mulss	%xmm15, %xmm2
	subss	%xmm2, %xmm0
	mulss	%xmm15, %xmm3
	mulss	%xmm14, %xmm1
	subss	%xmm1, %xmm3
	movaps	%xmm4, %xmm1
	mulss	%xmm7, %xmm1
	movaps	%xmm0, %xmm2
	mulss	%xmm5, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm3, %xmm1
	mulss	%xmm6, %xmm1
	addss	%xmm2, %xmm1
	movss	(%r14), %xmm2           # xmm2 = mem[0],zero,zero,zero
	mulss	4(%r14), %xmm2
	mulss	308(%rbx), %xmm2
	mulss	%xmm2, %xmm1
	movq	48(%r14), %rdx
	movss	%xmm1, (%rdx)
	mulss	%xmm8, %xmm7
	mulss	%xmm9, %xmm0
	addss	%xmm7, %xmm0
	mulss	%xmm10, %xmm3
	addss	%xmm0, %xmm3
	mulss	%xmm2, %xmm3
	movss	%xmm3, (%rdx,%rax,4)
	movss	48(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	52(%rbp), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	56(%rbp), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	48(%r13), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movss	52(%r13), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	56(%r13), %xmm12        # xmm12 = mem[0],zero,zero,zero
	leaq	(%rax,%rax), %rdx
	movss	80(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	64(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm3
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm3
	jbe	.LBB15_8
# BB#9:                                 # %select.true.sink
	divss	%xmm3, %xmm2
	jmp	.LBB15_10
.LBB15_8:
	movss	.LCPI15_1(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
.LBB15_10:                              # %select.end
	movss	.LCPI15_2(%rip), %xmm7  # xmm7 = mem[0],zero,zero,zero
	minss	%xmm2, %xmm7
	movss	.LCPI15_3(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	maxss	%xmm7, %xmm3
	subss	%xmm1, %xmm11
	subss	%xmm10, %xmm8
	subss	%xmm9, %xmm12
	movaps	%xmm8, %xmm7
	mulss	%xmm6, %xmm7
	movaps	%xmm12, %xmm1
	mulss	%xmm5, %xmm1
	subss	%xmm1, %xmm7
	movaps	%xmm12, %xmm1
	mulss	%xmm4, %xmm1
	mulss	%xmm11, %xmm6
	subss	%xmm6, %xmm1
	mulss	%xmm11, %xmm5
	mulss	%xmm8, %xmm4
	subss	%xmm4, %xmm5
	movq	16(%r14), %rsi
	movaps	%xmm3, %xmm4
	mulss	%xmm7, %xmm4
	leaq	(,%rdx,4), %rbp
	movss	%xmm4, (%rsi,%rdx,4)
	movaps	%xmm3, %xmm4
	mulss	%xmm1, %xmm4
	orq	$4, %rbp
	movss	%xmm4, (%rsi,%rbp)
	movaps	%xmm3, %xmm4
	mulss	%xmm5, %xmm4
	movss	%xmm4, 8(%rsi,%rdx,4)
	movzbl	%cl, %ecx
	movss	.LCPI15_4(,%rcx,4), %xmm10 # xmm10 = mem[0],zero,zero,zero
	movss	.LCPI15_5(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm4
	movq	32(%r14), %rdi
	mulss	%xmm4, %xmm7
	movss	%xmm7, (%rdi,%rdx,4)
	mulss	%xmm4, %xmm1
	movss	%xmm1, (%rdi,%rbp)
	mulss	%xmm4, %xmm5
	movss	%xmm5, 8(%rdi,%rdx,4)
	movss	24(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm6
	mulss	%xmm5, %xmm6
	movss	16(%rsp), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm7
	mulss	%xmm1, %xmm7
	subss	%xmm7, %xmm6
	movaps	%xmm12, %xmm7
	mulss	%xmm9, %xmm7
	mulss	%xmm11, %xmm5
	subss	%xmm5, %xmm7
	mulss	%xmm11, %xmm1
	movaps	%xmm8, %xmm5
	mulss	%xmm9, %xmm5
	subss	%xmm5, %xmm1
	movaps	%xmm3, %xmm5
	mulss	%xmm6, %xmm5
	shlq	$2, %rax
	leaq	(%rax,%rax,2), %rcx
	movss	%xmm5, (%rsi,%rcx)
	movaps	%xmm3, %xmm5
	mulss	%xmm7, %xmm5
	movss	%xmm5, 4(%rsi,%rcx)
	movaps	%xmm3, %xmm5
	mulss	%xmm1, %xmm5
	movss	%xmm5, 8(%rsi,%rcx)
	mulss	%xmm4, %xmm6
	movss	%xmm6, (%rdi,%rcx)
	mulss	%xmm4, %xmm7
	movss	%xmm7, 4(%rdi,%rcx)
	mulss	%xmm4, %xmm1
	movss	%xmm1, 8(%rdi,%rcx)
	movq	8(%r14), %rax
	movl	40(%rsp), %esi
	movl	%esi, 8(%rax,%rdx,4)
	movq	32(%rsp), %rsi
	movq	%rsi, (%rax,%rdx,4)
	movq	8(%r14), %rax
	movl	24(%rsp), %esi
	movl	%esi, 8(%rax,%rcx)
	movq	16(%rsp), %rsi
	movq	%rsi, (%rax,%rcx)
	movss	956(%rbx), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	960(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	subss	892(%rbx), %xmm7
	subss	896(%rbx), %xmm6
	movss	964(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	subss	900(%rbx), %xmm1
	movss	(%r14), %xmm5           # xmm5 = mem[0],zero,zero,zero
	mulss	4(%r14), %xmm5
	mulss	296(%rbx), %xmm5
	movss	32(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	movss	36(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	addss	%xmm0, %xmm2
	movss	40(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	addss	%xmm2, %xmm0
	mulss	%xmm5, %xmm0
	movq	48(%r14), %r8
	movss	%xmm0, (%r8,%rdx,4)
	mulss	%xmm9, %xmm7
	mulss	20(%rsp), %xmm6
	addss	%xmm7, %xmm6
	mulss	24(%rsp), %xmm1
	addss	%xmm6, %xmm1
	mulss	%xmm5, %xmm1
	movss	%xmm1, (%r8,%rcx)
	cmpb	$0, 320(%rbx)
	je	.LBB15_11
# BB#12:
	movss	1052(%rbx), %xmm2       # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm2
	xorps	%xmm0, %xmm0
	xorl	%r13d, %r13d
	ucomiss	%xmm0, %xmm2
	seta	%r13b
	incl	%r13d
	jmp	.LBB15_13
.LBB15_11:
	xorl	%r13d, %r13d
	xorps	%xmm2, %xmm2
.LBB15_13:
	movzbl	1116(%rbx), %ebp
	movl	%ebp, %eax
	orl	%r13d, %eax
	je	.LBB15_14
# BB#15:
	movq	%r15, 48(%rsp)          # 8-byte Spill
	testl	%r13d, %r13d
	setne	%r9b
	movslq	40(%r14), %rax
	leaq	(,%rax,4), %r15
	movq	8(%r14), %rcx
	shlq	$4, %rax
	movss	%xmm15, (%rcx,%rax)
	leal	1(%r15), %edx
	movslq	%edx, %rsi
	movss	%xmm14, (%rcx,%rsi,4)
	leal	2(%r15), %edx
	movslq	%edx, %rdi
	movss	%xmm13, (%rcx,%rdi,4)
	movaps	%xmm13, %xmm0
	mulss	%xmm8, %xmm0
	movaps	%xmm14, %xmm1
	mulss	%xmm12, %xmm1
	subss	%xmm1, %xmm0
	mulss	%xmm15, %xmm12
	movaps	%xmm13, %xmm1
	mulss	%xmm11, %xmm1
	subss	%xmm1, %xmm12
	mulss	%xmm14, %xmm11
	mulss	%xmm15, %xmm8
	subss	%xmm8, %xmm11
	movaps	%xmm3, %xmm1
	mulss	%xmm0, %xmm1
	movq	16(%r14), %rcx
	movss	%xmm1, (%rcx,%rax)
	movaps	%xmm3, %xmm1
	mulss	%xmm12, %xmm1
	movss	%xmm1, (%rcx,%rsi,4)
	mulss	%xmm11, %xmm3
	movss	%xmm3, (%rcx,%rdi,4)
	mulss	%xmm4, %xmm0
	movq	32(%r14), %rcx
	movss	%xmm0, (%rcx,%rax)
	mulss	%xmm4, %xmm12
	movss	%xmm12, (%rcx,%rsi,4)
	mulss	%xmm4, %xmm11
	movss	%xmm11, (%rcx,%rdi,4)
	movss	232(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	236(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm1
	setnp	%cl
	sete	%dl
	movl	$0, (%r8,%rax)
	movq	64(%r14), %rdi
	movl	$0, (%rdi,%rax)
	movq	72(%r14), %rsi
	movl	$0, (%rsi,%rax)
	testb	%bpl, %bpl
	je	.LBB15_18
# BB#16:
	andb	%cl, %dl
	andb	%dl, %r9b
	jne	.LBB15_18
# BB#17:
	movq	56(%r14), %rax
	movl	$0, 16(%rax)
	movss	%xmm3, 56(%rsp)         # 4-byte Spill
	movss	1120(%rbx), %xmm3       # xmm3 = mem[0],zero,zero,zero
	movss	1100(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movss	%xmm1, 60(%rsp)         # 4-byte Spill
	movss	232(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm2, 8(%rsp)          # 4-byte Spill
	movss	236(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	(%r14), %xmm4           # xmm4 = mem[0],zero,zero,zero
	mulss	4(%r14), %xmm4
	movq	%rbx, %rdi
	movaps	%xmm13, 80(%rsp)        # 16-byte Spill
	movaps	%xmm14, 64(%rsp)        # 16-byte Spill
	movaps	%xmm15, 96(%rsp)        # 16-byte Spill
	movss	%xmm10, 12(%rsp)        # 4-byte Spill
	callq	_ZN17btTypedConstraint14getMotorFactorEfffff
	movss	56(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm10        # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movaps	96(%rsp), %xmm15        # 16-byte Reload
	movaps	64(%rsp), %xmm14        # 16-byte Reload
	movaps	80(%rsp), %xmm13        # 16-byte Reload
	mulss	%xmm10, %xmm0
	mulss	1120(%rbx), %xmm0
	movq	48(%r14), %r8
	movss	(%r8,%r15,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, (%r8,%r15,4)
	movss	1124(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	mulss	(%r14), %xmm0
	movq	64(%r14), %rdi
	movss	(%rdi,%r15,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, (%rdi,%r15,4)
	movss	60(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	1124(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	mulss	(%r14), %xmm0
	movq	72(%r14), %rsi
	addss	(%rsi,%r15,4), %xmm0
	movss	%xmm0, (%rsi,%r15,4)
.LBB15_18:
	movl	$5, %ecx
	testl	%r13d, %r13d
	je	.LBB15_33
# BB#19:
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	4(%r14), %xmm0
	mulss	%xmm0, %xmm2
	addss	(%r8,%r15,4), %xmm2
	movss	%xmm2, (%r8,%r15,4)
	movq	56(%r14), %rax
	movl	$0, (%rax,%r15,4)
	ucomiss	%xmm3, %xmm1
	jne	.LBB15_21
	jp	.LBB15_21
# BB#20:
	movl	$-8388609, (%rdi,%r15,4) # imm = 0xFF7FFFFF
	jmp	.LBB15_24
.LBB15_14:
	movl	$4, %ecx
	cmpb	$0, 321(%rbx)
	jne	.LBB15_35
	jmp	.LBB15_34
.LBB15_21:
	cmpl	$1, %r13d
	jne	.LBB15_23
# BB#22:
	movl	$-8388609, (%rdi,%r15,4) # imm = 0xFF7FFFFF
	xorps	%xmm0, %xmm0
	jmp	.LBB15_25
.LBB15_23:
	movl	$0, (%rdi,%r15,4)
.LBB15_24:
	movss	.LCPI15_6(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
.LBB15_25:
	movss	%xmm0, (%rsi,%r15,4)
	movss	.LCPI15_5(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	subss	280(%rbx), %xmm0
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	je	.LBB15_32
# BB#26:
	andps	.LCPI15_7(%rip), %xmm0
	movss	(%r12), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm1
	movss	4(%r12), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm2
	addss	%xmm1, %xmm2
	movss	8(%r12), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm1
	addss	%xmm2, %xmm1
	movq	48(%rsp), %r12          # 8-byte Reload
	movss	(%r12), %xmm2           # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm2
	movss	4(%r12), %xmm3          # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm3
	addss	%xmm2, %xmm3
	movss	8(%r12), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm2
	addss	%xmm3, %xmm2
	subss	%xmm2, %xmm1
	mulss	%xmm1, %xmm10
	xorps	%xmm1, %xmm1
	cmpl	$1, %r13d
	jne	.LBB15_29
# BB#27:
	ucomiss	%xmm10, %xmm1
	jbe	.LBB15_32
# BB#28:
	mulss	%xmm10, %xmm0
	xorps	.LCPI15_0(%rip), %xmm0
	ucomiss	(%r8,%r15,4), %xmm0
	ja	.LBB15_31
	jmp	.LBB15_32
.LBB15_29:
	ucomiss	%xmm1, %xmm10
	jbe	.LBB15_32
# BB#30:
	mulss	%xmm10, %xmm0
	xorps	.LCPI15_0(%rip), %xmm0
	movss	(%r8,%r15,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB15_32
.LBB15_31:
	movss	%xmm0, (%r8,%r15,4)
.LBB15_32:
	movss	272(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	(%r8,%r15,4), %xmm0
	movss	%xmm0, (%r8,%r15,4)
.LBB15_33:
	cmpb	$0, 321(%rbx)
	je	.LBB15_34
.LBB15_35:
	movss	1108(%rbx), %xmm2       # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	xorl	%r15d, %r15d
	ucomiss	%xmm0, %xmm2
	setbe	%r15b
	incl	%r15d
	jmp	.LBB15_36
.LBB15_34:
	xorps	%xmm2, %xmm2
	xorl	%r15d, %r15d
.LBB15_36:
	movzbl	1132(%rbx), %eax
	movl	%eax, %edx
	orl	%r15d, %edx
	je	.LBB15_55
# BB#37:
	testl	%r15d, %r15d
	setne	%sil
	movslq	40(%r14), %rdx
	movslq	%ecx, %rbp
	imulq	%rdx, %rbp
	movq	16(%r14), %rcx
	movss	%xmm15, (%rcx,%rbp,4)
	movslq	%ebp, %rdx
	movss	%xmm14, 4(%rcx,%rdx,4)
	movss	%xmm13, 8(%rcx,%rdx,4)
	movaps	%xmm15, %xmm0
	movaps	.LCPI15_0(%rip), %xmm1  # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm1, %xmm0
	movq	32(%r14), %rcx
	movss	%xmm0, (%rcx,%rbp,4)
	movaps	%xmm14, %xmm0
	xorps	%xmm1, %xmm0
	movss	%xmm0, 4(%rcx,%rdx,4)
	xorps	%xmm13, %xmm1
	movss	%xmm1, 8(%rcx,%rdx,4)
	movss	240(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	244(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm1
	setnp	%dl
	sete	%cl
	testb	%al, %al
	je	.LBB15_40
# BB#38:
	andb	%dl, %cl
	andb	%cl, %sil
	jne	.LBB15_40
# BB#39:
	movq	56(%r14), %rax
	movl	$0, (%rax,%rbp,4)
	movss	1104(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movss	240(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm2, 48(%rsp)         # 4-byte Spill
	movss	244(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	%xmm3, 8(%rsp)          # 4-byte Spill
	movss	1136(%rbx), %xmm3       # xmm3 = mem[0],zero,zero,zero
	movss	(%r14), %xmm4           # xmm4 = mem[0],zero,zero,zero
	mulss	4(%r14), %xmm4
	movq	%rbx, %rdi
	movaps	%xmm13, 80(%rsp)        # 16-byte Spill
	movaps	%xmm14, 64(%rsp)        # 16-byte Spill
	movaps	%xmm15, 96(%rsp)        # 16-byte Spill
	callq	_ZN17btTypedConstraint14getMotorFactorEfffff
	movss	8(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	48(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	96(%rsp), %xmm15        # 16-byte Reload
	movaps	64(%rsp), %xmm14        # 16-byte Reload
	movaps	80(%rsp), %xmm13        # 16-byte Reload
	mulss	1136(%rbx), %xmm0
	movq	48(%r14), %r8
	movss	%xmm0, (%r8,%rbp,4)
	movss	1140(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	mulss	(%r14), %xmm0
	xorps	.LCPI15_0(%rip), %xmm0
	movq	64(%r14), %rax
	movss	%xmm0, (%rax,%rbp,4)
	movss	1140(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	mulss	(%r14), %xmm0
	movq	72(%r14), %rax
	movss	%xmm0, (%rax,%rbp,4)
.LBB15_40:
	testl	%r15d, %r15d
	je	.LBB15_55
# BB#41:
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	4(%r14), %xmm0
	mulss	%xmm0, %xmm2
	addss	(%r8,%rbp,4), %xmm2
	movss	%xmm2, (%r8,%rbp,4)
	movq	56(%r14), %rax
	movl	$0, (%rax,%rbp,4)
	ucomiss	%xmm3, %xmm1
	jne	.LBB15_43
	jp	.LBB15_43
# BB#42:
	movq	64(%r14), %rax
	movl	$-8388609, (%rax,%rbp,4) # imm = 0xFF7FFFFF
	movss	.LCPI15_6(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB15_46
.LBB15_43:
	movq	64(%r14), %rax
	cmpl	$1, %r15d
	jne	.LBB15_45
# BB#44:
	movl	$0, (%rax,%rbp,4)
	movss	.LCPI15_6(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB15_46
.LBB15_45:
	movl	$-8388609, (%rax,%rbp,4) # imm = 0xFF7FFFFF
	xorps	%xmm0, %xmm0
.LBB15_46:
	movq	72(%r14), %rax
	movss	%xmm0, (%rax,%rbp,4)
	movss	.LCPI15_5(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	subss	292(%rbx), %xmm1
	xorps	%xmm0, %xmm0
	movaps	%xmm1, %xmm4
	ucomiss	%xmm0, %xmm1
	je	.LBB15_54
# BB#47:
	andps	.LCPI15_7(%rip), %xmm4
	movq	24(%rbx), %rax
	movq	32(%rbx), %rcx
	shufps	$224, %xmm15, %xmm15    # xmm15 = xmm15[0,0,2,3]
	movss	344(%rcx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	348(%rcx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	344(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	348(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	mulps	%xmm15, %xmm2
	shufps	$224, %xmm14, %xmm14    # xmm14 = xmm14[0,0,2,3]
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	mulps	%xmm14, %xmm3
	addps	%xmm2, %xmm3
	shufps	$224, %xmm13, %xmm13    # xmm13 = xmm13[0,0,2,3]
	movss	352(%rcx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	352(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	mulps	%xmm13, %xmm0
	addps	%xmm3, %xmm0
	movaps	%xmm0, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	subss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	cmpl	$1, %r15d
	jne	.LBB15_51
# BB#48:
	ucomiss	%xmm0, %xmm1
	jbe	.LBB15_54
# BB#49:
	mulss	%xmm0, %xmm4
	xorps	.LCPI15_0(%rip), %xmm4
	movaps	%xmm4, %xmm0
	ucomiss	(%r8,%rbp,4), %xmm4
	jbe	.LBB15_54
# BB#50:
	movss	%xmm0, (%r8,%rbp,4)
	jmp	.LBB15_54
.LBB15_51:
	ucomiss	%xmm1, %xmm0
	jbe	.LBB15_54
# BB#52:
	mulss	%xmm0, %xmm4
	xorps	.LCPI15_0(%rip), %xmm4
	movss	(%r8,%rbp,4), %xmm0     # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	ucomiss	%xmm4, %xmm0
	jbe	.LBB15_54
# BB#53:
	movss	%xmm1, (%r8,%rbp,4)
.LBB15_54:
	movss	284(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	(%r8,%rbp,4), %xmm0
	movss	%xmm0, (%r8,%rbp,4)
.LBB15_55:
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	_ZN18btSliderConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_ff, .Lfunc_end15-_ZN18btSliderConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_ff
	.cfi_endproc

	.globl	_ZN18btSliderConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.p2align	4, 0x90
	.type	_ZN18btSliderConstraint23solveConstraintObsoleteER12btSolverBodyS1_f,@function
_ZN18btSliderConstraint23solveConstraintObsoleteER12btSolverBodyS1_f: # @_ZN18btSliderConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.cfi_startproc
# BB#0:
	movq	%rdx, %r8
	movq	%rsi, %rax
	cmpb	$0, 96(%rdi)
	je	.LBB16_3
# BB#1:
	movss	%xmm0, 840(%rdi)
	cmpb	$0, 228(%rdi)
	movq	24(%rdi), %r9
	movq	32(%rdi), %rcx
	je	.LBB16_2
# BB#4:
	movq	%r9, %rsi
	movq	%rax, %rdx
	jmp	_ZN18btSliderConstraint18solveConstraintIntER11btRigidBodyR12btSolverBodyS1_S3_ # TAILCALL
.LBB16_3:
	retq
.LBB16_2:
	movq	%rcx, %rsi
	movq	%r8, %rdx
	movq	%r9, %rcx
	movq	%rax, %r8
	jmp	_ZN18btSliderConstraint18solveConstraintIntER11btRigidBodyR12btSolverBodyS1_S3_ # TAILCALL
.Lfunc_end16:
	.size	_ZN18btSliderConstraint23solveConstraintObsoleteER12btSolverBodyS1_f, .Lfunc_end16-_ZN18btSliderConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI17_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI17_1:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI17_2:
	.long	2147483648              # float -0
.LCPI17_3:
	.long	925353388               # float 9.99999974E-6
.LCPI17_4:
	.long	1065353216              # float 1
	.text
	.globl	_ZN18btSliderConstraint18solveConstraintIntER11btRigidBodyR12btSolverBodyS1_S3_
	.p2align	4, 0x90
	.type	_ZN18btSliderConstraint18solveConstraintIntER11btRigidBodyR12btSolverBodyS1_S3_,@function
_ZN18btSliderConstraint18solveConstraintIntER11btRigidBodyR12btSolverBodyS1_S3_: # @_ZN18btSliderConstraint18solveConstraintIntER11btRigidBodyR12btSolverBodyS1_S3_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi65:
	.cfi_def_cfa_offset 352
.Lcfi66:
	.cfi_offset %rbx, -56
.Lcfi67:
	.cfi_offset %r12, -48
.Lcfi68:
	.cfi_offset %r13, -40
.Lcfi69:
	.cfi_offset %r14, -32
.Lcfi70:
	.cfi_offset %r15, -24
.Lcfi71:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rcx, %r14
	movq	%rdx, %r13
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	72(%r13), %rax
	testq	%rax, %rax
	je	.LBB17_1
# BB#2:
	movsd	328(%rax), %xmm0        # xmm0 = mem[0],zero
	movsd	(%r13), %xmm1           # xmm1 = mem[0],zero
	addps	%xmm0, %xmm1
	movss	336(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	344(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	addss	8(%r13), %xmm2
	addss	16(%r13), %xmm0
	movsd	348(%rax), %xmm3        # xmm3 = mem[0],zero
	movsd	20(%r13), %xmm4         # xmm4 = mem[0],zero
	addps	%xmm3, %xmm4
	movss	1068(%rbx), %xmm3       # xmm3 = mem[0],zero,zero,zero
	movss	1076(%rbx), %xmm12      # xmm12 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm12   # xmm12 = xmm12[0],xmm3[0],xmm12[1],xmm3[1]
	mulps	%xmm4, %xmm12
	movaps	%xmm4, %xmm5
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	movaps	%xmm0, %xmm6
	shufps	$0, %xmm5, %xmm6        # xmm6 = xmm6[0,0],xmm5[0,0]
	shufps	$226, %xmm5, %xmm6      # xmm6 = xmm6[2,0],xmm5[2,3]
	movsd	1072(%rbx), %xmm5       # xmm5 = mem[0],zero
	mulps	%xmm6, %xmm5
	subps	%xmm5, %xmm12
	mulss	1072(%rbx), %xmm0
	mulss	%xmm3, %xmm4
	subss	%xmm4, %xmm0
	addps	%xmm1, %xmm12
	addss	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movaps	%xmm1, 48(%rsp)         # 16-byte Spill
	jmp	.LBB17_3
.LBB17_1:
	xorps	%xmm12, %xmm12
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
.LBB17_3:                               # %_ZNK12btSolverBody31getVelocityInLocalPointObsoleteERK9btVector3RS0_.exit
	movq	72(%r15), %rax
	testq	%rax, %rax
	je	.LBB17_4
# BB#5:
	movsd	328(%rax), %xmm0        # xmm0 = mem[0],zero
	movsd	(%r15), %xmm2           # xmm2 = mem[0],zero
	addps	%xmm0, %xmm2
	movss	336(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	344(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	addss	8(%r15), %xmm3
	addss	16(%r15), %xmm1
	movsd	348(%rax), %xmm0        # xmm0 = mem[0],zero
	movsd	20(%r15), %xmm4         # xmm4 = mem[0],zero
	addps	%xmm0, %xmm4
	movss	1084(%rbx), %xmm5       # xmm5 = mem[0],zero,zero,zero
	movss	1092(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm0    # xmm0 = xmm0[0],xmm5[0],xmm0[1],xmm5[1]
	mulps	%xmm4, %xmm0
	movaps	%xmm4, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movaps	%xmm1, %xmm7
	shufps	$0, %xmm6, %xmm7        # xmm7 = xmm7[0,0],xmm6[0,0]
	shufps	$226, %xmm6, %xmm7      # xmm7 = xmm7[2,0],xmm6[2,3]
	movsd	1088(%rbx), %xmm6       # xmm6 = mem[0],zero
	mulps	%xmm7, %xmm6
	subps	%xmm6, %xmm0
	mulss	1088(%rbx), %xmm1
	mulss	%xmm5, %xmm4
	subss	%xmm4, %xmm1
	addps	%xmm2, %xmm0
	addss	%xmm3, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	jmp	.LBB17_6
.LBB17_4:                               # %_ZNK12btSolverBody31getVelocityInLocalPointObsoleteERK9btVector3RS0_.exit._ZNK12btSolverBody31getVelocityInLocalPointObsoleteERK9btVector3RS0_.exit255_crit_edge
	xorps	%xmm0, %xmm0
	xorps	%xmm2, %xmm2
.LBB17_6:                               # %_ZNK12btSolverBody31getVelocityInLocalPointObsoleteERK9btVector3RS0_.exit255
	movaps	%xmm12, %xmm1
	subss	%xmm0, %xmm1
	movaps	%xmm1, 128(%rsp)        # 16-byte Spill
	shufps	$229, %xmm12, %xmm12    # xmm12 = xmm12[1,1,2,3]
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	%xmm0, %xmm12
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	subss	%xmm2, %xmm0
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	leaq	296(%rbx), %r9
	leaq	300(%rbx), %r11
	leaq	304(%rbx), %r10
	leaq	248(%rbx), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	272(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	252(%rbx), %rdi
	leaq	276(%rbx), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leaq	256(%rbx), %rsi
	leaq	280(%rbx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	$-63, %rbp
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB17_7:                               # =>This Inner Loop Header: Depth=1
	movss	576(%rbx,%rbp,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movaps	128(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm1, %xmm0
	movss	580(%rbx,%rbp,4), %xmm10 # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm3
	mulss	%xmm10, %xmm3
	addss	%xmm0, %xmm3
	movss	584(%rbx,%rbp,4), %xmm7 # xmm7 = mem[0],zero,zero,zero
	movaps	48(%rsp), %xmm13        # 16-byte Reload
	mulss	%xmm7, %xmm13
	addss	%xmm3, %xmm13
	movss	1052(%rbx,%r8), %xmm6   # xmm6 = mem[0],zero,zero,zero
	testq	%r8, %r8
	movq	%r11, %rcx
	movq	%r9, %rax
	movq	%r10, %rdx
	jne	.LBB17_9
# BB#8:                                 #   in Loop: Header=BB17_7 Depth=1
	cmpb	$0, 320(%rbx)
	movq	24(%rsp), %rax          # 8-byte Reload
	cmovneq	16(%rsp), %rax          # 8-byte Folded Reload
	movq	%rdi, %rcx
	cmovneq	112(%rsp), %rcx         # 8-byte Folded Reload
	movq	%rsi, %rdx
	cmovneq	32(%rsp), %rdx          # 8-byte Folded Reload
.LBB17_9:                               #   in Loop: Header=BB17_7 Depth=1
	testq	%r8, %r8
	mulss	(%rcx), %xmm6
	divss	840(%rbx), %xmm6
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm0
	subss	%xmm0, %xmm6
	mulss	(%rax), %xmm6
	mulss	576(%rbx,%r8), %xmm6
	movss	1068(%rbx), %xmm5       # xmm5 = mem[0],zero,zero,zero
	movss	1072(%rbx), %xmm4       # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm4, %xmm0
	movss	1076(%rbx), %xmm3       # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm2
	mulss	%xmm3, %xmm2
	subss	%xmm2, %xmm0
	mulss	%xmm1, %xmm3
	movaps	%xmm7, %xmm2
	mulss	%xmm5, %xmm2
	subss	%xmm2, %xmm3
	mulss	%xmm10, %xmm5
	mulss	%xmm1, %xmm4
	subss	%xmm4, %xmm5
	movss	1088(%rbx), %xmm2       # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm14
	mulss	%xmm2, %xmm14
	movss	1092(%rbx), %xmm11      # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm4
	mulss	%xmm11, %xmm4
	subss	%xmm4, %xmm14
	mulss	%xmm1, %xmm11
	movss	1084(%rbx), %xmm15      # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm4
	mulss	%xmm15, %xmm4
	subss	%xmm4, %xmm11
	mulss	%xmm10, %xmm15
	mulss	%xmm1, %xmm2
	subss	%xmm2, %xmm15
	movss	360(%r12), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	mulss	%xmm2, %xmm10
	mulss	%xmm2, %xmm7
	movss	280(%r12), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movss	284(%r12), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	addss	%xmm2, %xmm4
	movss	288(%r12), %xmm8        # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm8
	addss	%xmm4, %xmm8
	movss	296(%r12), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movss	300(%r12), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	addss	%xmm2, %xmm4
	movss	304(%r12), %xmm9        # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm9
	addss	%xmm4, %xmm9
	mulss	312(%r12), %xmm0
	mulss	316(%r12), %xmm3
	addss	%xmm0, %xmm3
	mulss	320(%r12), %xmm5
	addss	%xmm3, %xmm5
	mulss	%xmm6, %xmm1
	mulss	%xmm6, %xmm10
	mulss	%xmm6, %xmm7
	addss	(%r13), %xmm1
	movss	%xmm1, (%r13)
	addss	4(%r13), %xmm10
	movss	%xmm10, 4(%r13)
	addss	8(%r13), %xmm7
	movss	%xmm7, 8(%r13)
	movss	32(%r13), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm0
	movss	36(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm1
	movss	40(%r13), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	mulss	%xmm8, %xmm0
	mulss	%xmm9, %xmm1
	mulss	%xmm5, %xmm2
	addss	16(%r13), %xmm0
	movss	%xmm0, 16(%r13)
	addss	20(%r13), %xmm1
	movss	%xmm1, 20(%r13)
	addss	24(%r13), %xmm2
	movss	%xmm2, 24(%r13)
	movss	360(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	576(%rbx,%rbp,4), %xmm2 # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movss	580(%rbx,%rbp,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	mulss	584(%rbx,%rbp,4), %xmm0
	movaps	%xmm14, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	296(%r14), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	280(%r14), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	284(%r14), %xmm7        # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	movaps	%xmm11, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	300(%r14), %xmm4        # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm7    # xmm7 = xmm7[0],xmm4[0],xmm7[1],xmm4[1]
	mulps	%xmm3, %xmm7
	addps	%xmm5, %xmm7
	movaps	%xmm15, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	304(%r14), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	288(%r14), %xmm3        # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	mulps	%xmm4, %xmm3
	addps	%xmm7, %xmm3
	mulss	312(%r14), %xmm14
	mulss	316(%r14), %xmm11
	addss	%xmm14, %xmm11
	mulss	320(%r14), %xmm15
	addss	%xmm11, %xmm15
	movaps	%xmm6, %xmm4
	xorps	.LCPI17_0(%rip), %xmm4
	mulss	%xmm6, %xmm2
	mulss	%xmm6, %xmm1
	mulss	%xmm6, %xmm0
	movss	(%r15), %xmm5           # xmm5 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm5
	movss	%xmm5, (%r15)
	movss	4(%r15), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm2
	movss	%xmm2, 4(%r15)
	movss	8(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 8(%r15)
	movsd	32(%r15), %xmm0         # xmm0 = mem[0],zero
	pshufd	$224, %xmm4, %xmm1      # xmm1 = xmm4[0,0,2,3]
	mulps	%xmm0, %xmm1
	mulss	40(%r15), %xmm6
	mulps	%xmm3, %xmm1
	mulss	%xmm15, %xmm6
	movsd	16(%r15), %xmm0         # xmm0 = mem[0],zero
	addps	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	%xmm0, 16(%r15)
	movss	%xmm1, 20(%r15)
	movss	24(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm6, %xmm1
	movss	%xmm1, 24(%r15)
	jne	.LBB17_13
# BB#10:                                #   in Loop: Header=BB17_7 Depth=1
	movzbl	1116(%rbx), %eax
	testb	%al, %al
	je	.LBB17_13
# BB#11:                                #   in Loop: Header=BB17_7 Depth=1
	movss	1124(%rbx), %xmm2       # xmm2 = mem[0],zero,zero,zero
	movss	1128(%rbx), %xmm3       # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm2
	jbe	.LBB17_13
# BB#12:                                #   in Loop: Header=BB17_7 Depth=1
	addss	1120(%rbx), %xmm13
	mulss	576(%rbx,%r8), %xmm13
	movss	.LCPI17_2(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	cmpltss	%xmm13, %xmm0
	andps	.LCPI17_1(%rip), %xmm13
	addss	%xmm3, %xmm13
	minss	%xmm13, %xmm2
	movaps	%xmm2, %xmm10
	subss	%xmm3, %xmm10
	movaps	%xmm1, %xmm13
	shufps	$0, %xmm13, %xmm13      # xmm13 = xmm13[0,0,0,0]
	movaps	%xmm0, %xmm1
	andnps	%xmm10, %xmm1
	xorps	%xmm13, %xmm10
	andps	%xmm0, %xmm10
	orps	%xmm1, %xmm10
	movss	%xmm2, 1128(%rbx)
	movss	576(%rbx,%rbp,4), %xmm6 # xmm6 = mem[0],zero,zero,zero
	movss	580(%rbx,%rbp,4), %xmm5 # xmm5 = mem[0],zero,zero,zero
	movss	584(%rbx,%rbp,4), %xmm7 # xmm7 = mem[0],zero,zero,zero
	movss	1068(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movss	1072(%rbx), %xmm4       # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm1
	mulss	%xmm4, %xmm1
	movss	1076(%rbx), %xmm3       # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm2
	mulss	%xmm3, %xmm2
	subss	%xmm2, %xmm1
	mulss	%xmm6, %xmm3
	movaps	%xmm7, %xmm2
	mulss	%xmm0, %xmm2
	subss	%xmm2, %xmm3
	mulss	%xmm5, %xmm0
	mulss	%xmm6, %xmm4
	subss	%xmm4, %xmm0
	movss	1088(%rbx), %xmm2       # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm14
	mulss	%xmm2, %xmm14
	movss	1092(%rbx), %xmm8       # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm4
	mulss	%xmm8, %xmm4
	subss	%xmm4, %xmm14
	mulss	%xmm6, %xmm8
	movss	1084(%rbx), %xmm15      # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm4
	mulss	%xmm15, %xmm4
	subss	%xmm4, %xmm8
	mulss	%xmm5, %xmm15
	mulss	%xmm6, %xmm2
	subss	%xmm2, %xmm15
	movss	360(%r12), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm6
	mulss	%xmm2, %xmm5
	mulss	%xmm2, %xmm7
	movss	280(%r12), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	movss	284(%r12), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	addss	%xmm2, %xmm4
	movss	288(%r12), %xmm9        # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm9
	addss	%xmm4, %xmm9
	movss	296(%r12), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	movss	300(%r12), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	addss	%xmm2, %xmm4
	movss	304(%r12), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	addss	%xmm4, %xmm2
	mulss	312(%r12), %xmm1
	mulss	316(%r12), %xmm3
	addss	%xmm1, %xmm3
	mulss	320(%r12), %xmm0
	addss	%xmm3, %xmm0
	mulss	%xmm10, %xmm6
	mulss	%xmm10, %xmm5
	mulss	%xmm10, %xmm7
	addss	(%r13), %xmm6
	movss	%xmm6, (%r13)
	addss	4(%r13), %xmm5
	movss	%xmm5, 4(%r13)
	addss	8(%r13), %xmm7
	movss	%xmm7, 8(%r13)
	movss	32(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm1
	movss	36(%r13), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm3
	movss	40(%r13), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	mulss	%xmm9, %xmm1
	mulss	%xmm2, %xmm3
	mulss	%xmm0, %xmm4
	addss	16(%r13), %xmm1
	movss	%xmm1, 16(%r13)
	addss	20(%r13), %xmm3
	movss	%xmm3, 20(%r13)
	addss	24(%r13), %xmm4
	movss	%xmm4, 24(%r13)
	movss	360(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	576(%rbx,%rbp,4), %xmm2 # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movss	580(%rbx,%rbp,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	mulss	584(%rbx,%rbp,4), %xmm0
	movaps	%xmm14, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	296(%r14), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	280(%r14), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	284(%r14), %xmm6        # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	movaps	%xmm8, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	300(%r14), %xmm4        # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm3, %xmm6
	addps	%xmm5, %xmm6
	movaps	%xmm15, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	304(%r14), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	288(%r14), %xmm3        # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	mulps	%xmm4, %xmm3
	addps	%xmm6, %xmm3
	mulss	312(%r14), %xmm14
	mulss	316(%r14), %xmm8
	addss	%xmm14, %xmm8
	mulss	320(%r14), %xmm15
	addss	%xmm8, %xmm15
	xorps	%xmm10, %xmm13
	mulss	%xmm10, %xmm2
	mulss	%xmm10, %xmm1
	mulss	%xmm10, %xmm0
	movss	(%r15), %xmm4           # xmm4 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm4
	movss	%xmm4, (%r15)
	movss	4(%r15), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm2
	movss	%xmm2, 4(%r15)
	movss	8(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 8(%r15)
	movsd	32(%r15), %xmm0         # xmm0 = mem[0],zero
	pshufd	$224, %xmm13, %xmm1     # xmm1 = xmm13[0,0,2,3]
	mulps	%xmm0, %xmm1
	movss	40(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm2
	mulps	%xmm3, %xmm1
	mulss	%xmm15, %xmm2
	movsd	16(%r15), %xmm0         # xmm0 = mem[0],zero
	addps	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	%xmm0, 16(%r15)
	movss	%xmm1, 20(%r15)
	movss	24(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm1
	movss	%xmm1, 24(%r15)
.LBB17_13:                              #   in Loop: Header=BB17_7 Depth=1
	addq	$4, %r8
	addq	$21, %rbp
	cmpq	$12, %r8
	jne	.LBB17_7
# BB#14:
	movss	844(%rbx), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movss	860(%rbx), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movss	876(%rbx), %xmm13       # xmm13 = mem[0],zero,zero,zero
	movss	908(%rbx), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movss	924(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 24(%rsp)         # 4-byte Spill
	movss	940(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 16(%rsp)         # 4-byte Spill
	movq	72(%r13), %rax
	xorps	%xmm7, %xmm7
	testq	%rax, %rax
	xorps	%xmm3, %xmm3
	xorps	%xmm8, %xmm8
	je	.LBB17_16
# BB#15:
	movsd	344(%rax), %xmm2        # xmm2 = mem[0],zero
	movsd	16(%r13), %xmm3         # xmm3 = mem[0],zero
	addps	%xmm2, %xmm3
	movss	352(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	addss	24(%r13), %xmm2
	xorps	%xmm8, %xmm8
	movss	%xmm2, %xmm8            # xmm8 = xmm2[0],xmm8[1,2,3]
.LBB17_16:                              # %_ZNK12btSolverBody18getAngularVelocityER9btVector3.exit322
	movq	72(%r15), %rax
	testq	%rax, %rax
	xorps	%xmm6, %xmm6
	je	.LBB17_18
# BB#17:
	movsd	344(%rax), %xmm7        # xmm7 = mem[0],zero
	addps	%xmm0, %xmm7
	addss	352(%rax), %xmm1
	xorps	%xmm6, %xmm6
	movss	%xmm1, %xmm6            # xmm6 = xmm1[0],xmm6[1,2,3]
.LBB17_18:                              # %_ZNK12btSolverBody18getAngularVelocityER9btVector3.exit
	movaps	%xmm9, %xmm0
	mulss	%xmm3, %xmm0
	movaps	%xmm3, %xmm11
	shufps	$229, %xmm11, %xmm11    # xmm11 = xmm11[1,1,2,3]
	movaps	%xmm14, %xmm1
	mulss	%xmm11, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm13, %xmm2
	mulss	%xmm8, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm9, %xmm4
	mulss	%xmm2, %xmm4
	movaps	%xmm3, %xmm10
	movaps	%xmm14, %xmm3
	mulss	%xmm2, %xmm3
	mulss	%xmm13, %xmm2
	movaps	%xmm12, %xmm0
	movss	%xmm9, 128(%rsp)        # 4-byte Spill
	movaps	%xmm7, %xmm9
	mulss	%xmm9, %xmm0
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	movss	24(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm1
	mulss	%xmm7, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm6, %xmm0
	movss	%xmm13, 48(%rsp)        # 4-byte Spill
	movss	16(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm5
	mulss	%xmm0, %xmm5
	addss	%xmm1, %xmm5
	movss	%xmm12, 32(%rsp)        # 4-byte Spill
	movaps	%xmm12, %xmm1
	mulss	%xmm5, %xmm1
	movaps	%xmm15, %xmm6
	movaps	%xmm9, %xmm15
	movss	128(%rsp), %xmm9        # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm6
	mulss	%xmm13, %xmm5
	movaps	%xmm14, %xmm12
	movss	48(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm14
	movaps	%xmm10, 256(%rsp)       # 16-byte Spill
	movss	%xmm4, 100(%rsp)        # 4-byte Spill
	subss	%xmm4, %xmm10
	movaps	%xmm11, 224(%rsp)       # 16-byte Spill
	movss	%xmm3, 104(%rsp)        # 4-byte Spill
	subss	%xmm3, %xmm11
	movaps	%xmm8, 240(%rsp)        # 16-byte Spill
	movss	%xmm2, 108(%rsp)        # 4-byte Spill
	subss	%xmm2, %xmm8
	movaps	%xmm15, %xmm0
	movss	%xmm1, 92(%rsp)         # 4-byte Spill
	subss	%xmm1, %xmm0
	movaps	%xmm7, 208(%rsp)        # 16-byte Spill
	movaps	%xmm7, %xmm1
	movss	%xmm6, 88(%rsp)         # 4-byte Spill
	subss	%xmm6, %xmm1
	subss	%xmm0, %xmm10
	movaps	%xmm14, %xmm0
	movss	%xmm5, 96(%rsp)         # 4-byte Spill
	subss	%xmm5, %xmm0
	subss	%xmm1, %xmm11
	subss	%xmm0, %xmm8
	movaps	%xmm10, 160(%rsp)       # 16-byte Spill
	movaps	%xmm10, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm11, 176(%rsp)       # 16-byte Spill
	movaps	%xmm11, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm8, 192(%rsp)        # 16-byte Spill
	mulss	%xmm8, %xmm8
	addss	%xmm1, %xmm8
	xorps	%xmm0, %xmm0
	sqrtss	%xmm8, %xmm0
	ucomiss	%xmm0, %xmm0
	movss	%xmm12, 12(%rsp)        # 4-byte Spill
	movaps	%xmm15, 272(%rsp)       # 16-byte Spill
	movaps	%xmm14, 112(%rsp)       # 16-byte Spill
	jnp	.LBB17_20
# BB#19:                                # %call.sqrt
	movaps	%xmm8, %xmm0
	movaps	%xmm8, 144(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	144(%rsp), %xmm8        # 16-byte Reload
	movaps	112(%rsp), %xmm14       # 16-byte Reload
	movss	128(%rsp), %xmm9        # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movss	48(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
.LBB17_20:                              # %_ZNK12btSolverBody18getAngularVelocityER9btVector3.exit.split
	xorps	%xmm1, %xmm1
	movss	%xmm1, 144(%rsp)        # 4-byte Spill
	ucomiss	.LCPI17_3(%rip), %xmm0
	jbe	.LBB17_24
# BB#21:
	xorps	%xmm0, %xmm0
	sqrtss	%xmm8, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB17_23
# BB#22:                                # %call.sqrt952
	movaps	%xmm8, %xmm0
	callq	sqrtf
	movss	128(%rsp), %xmm9        # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movss	48(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
.LBB17_23:                              # %.split
	movss	.LCPI17_4(%rip), %xmm10 # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm1
	divss	%xmm0, %xmm1
	movaps	160(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm1, %xmm0
	movaps	176(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm1, %xmm2
	mulss	192(%rsp), %xmm1        # 16-byte Folded Reload
	movss	280(%r12), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movss	296(%r12), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	movss	312(%r12), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	addss	%xmm5, %xmm4
	movss	284(%r12), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movss	300(%r12), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	movss	316(%r12), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm6
	addss	%xmm5, %xmm6
	movss	288(%r12), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movss	304(%r12), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	movss	320(%r12), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	addss	%xmm5, %xmm3
	mulss	%xmm0, %xmm4
	mulss	%xmm2, %xmm6
	addss	%xmm4, %xmm6
	mulss	%xmm1, %xmm3
	addss	%xmm6, %xmm3
	movss	280(%r14), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	296(%r14), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm4, %xmm5
	movss	312(%r14), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	addss	%xmm5, %xmm4
	movss	284(%r14), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	movss	300(%r14), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm6
	addss	%xmm5, %xmm6
	movss	316(%r14), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	addss	%xmm6, %xmm5
	movss	288(%r14), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	movss	304(%r14), %xmm7        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm7
	addss	%xmm6, %xmm7
	movss	320(%r14), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm6
	addss	%xmm7, %xmm6
	mulss	%xmm0, %xmm4
	mulss	%xmm2, %xmm5
	addss	%xmm4, %xmm5
	mulss	%xmm1, %xmm6
	addss	%xmm5, %xmm6
	addss	%xmm3, %xmm6
	divss	%xmm6, %xmm10
	mulss	316(%rbx), %xmm10
	mulss	308(%rbx), %xmm10
	movss	%xmm10, 144(%rsp)       # 4-byte Spill
	movaps	112(%rsp), %xmm14       # 16-byte Reload
.LBB17_24:
	movaps	%xmm12, %xmm4
	movss	16(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	movaps	%xmm13, %xmm0
	movss	24(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm0
	subss	%xmm0, %xmm4
	movaps	%xmm13, %xmm11
	movss	32(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm11
	mulss	%xmm9, %xmm1
	subss	%xmm1, %xmm11
	mulss	%xmm9, %xmm5
	mulss	%xmm12, %xmm0
	subss	%xmm0, %xmm5
	movss	.LCPI17_4(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	divss	840(%rbx), %xmm2
	mulss	%xmm2, %xmm4
	mulss	%xmm2, %xmm11
	mulss	%xmm5, %xmm2
	movaps	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm11, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm2, %xmm5
	mulss	%xmm5, %xmm5
	addss	%xmm1, %xmm5
	xorps	%xmm0, %xmm0
	sqrtss	%xmm5, %xmm0
	ucomiss	%xmm0, %xmm0
	movss	%xmm2, 24(%rsp)         # 4-byte Spill
	movss	%xmm4, 16(%rsp)         # 4-byte Spill
	movss	%xmm11, 32(%rsp)        # 4-byte Spill
	jnp	.LBB17_26
# BB#25:                                # %call.sqrt954
	movaps	%xmm5, %xmm0
	movss	%xmm5, 44(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	44(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI17_4(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movaps	112(%rsp), %xmm14       # 16-byte Reload
	movss	12(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
.LBB17_26:                              # %.split953
	ucomiss	.LCPI17_3(%rip), %xmm0
	jbe	.LBB17_27
# BB#28:
	xorps	%xmm0, %xmm0
	sqrtss	%xmm5, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB17_30
# BB#29:                                # %call.sqrt956
	movaps	%xmm5, %xmm0
	callq	sqrtf
	movss	32(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI17_4(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
.LBB17_30:                              # %.split955
	movaps	%xmm3, %xmm5
	divss	%xmm0, %xmm5
	movaps	%xmm4, %xmm0
	mulss	%xmm5, %xmm0
	mulss	%xmm5, %xmm11
	mulss	%xmm2, %xmm5
	movss	280(%r12), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 44(%rsp)         # 4-byte Spill
	movss	284(%r12), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	%xmm4, 72(%rsp)         # 4-byte Spill
	mulss	%xmm0, %xmm2
	movss	296(%r12), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 84(%rsp)         # 4-byte Spill
	mulss	%xmm11, %xmm1
	addss	%xmm2, %xmm1
	movss	312(%r12), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	%xmm6, 80(%rsp)         # 4-byte Spill
	movss	312(%r14), %xmm13       # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm13
	movaps	%xmm5, %xmm2
	mulss	%xmm6, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm3, %xmm8
	movaps	%xmm0, %xmm3
	mulss	%xmm0, %xmm2
	movss	280(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	296(%r14), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm6
	addss	%xmm1, %xmm6
	addss	%xmm6, %xmm13
	movss	284(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 76(%rsp)         # 4-byte Spill
	movss	288(%r14), %xmm15       # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm15
	mulss	%xmm0, %xmm13
	mulss	%xmm4, %xmm0
	movss	300(%r12), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm4
	mulss	%xmm1, %xmm4
	addss	%xmm0, %xmm4
	movss	288(%r12), %xmm9        # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm3
	movss	304(%r12), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm14
	mulss	%xmm10, %xmm14
	addss	%xmm3, %xmm14
	movaps	%xmm5, %xmm6
	movss	320(%r12), %xmm12       # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm6
	addss	%xmm14, %xmm6
	movss	304(%r14), %xmm7        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm7
	addss	%xmm15, %xmm7
	movss	320(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm0
	addss	%xmm7, %xmm0
	mulss	%xmm5, %xmm6
	movss	316(%r14), %xmm7        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm7
	mulss	%xmm5, %xmm0
	movss	316(%r12), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm4, %xmm5
	mulss	%xmm11, %xmm5
	addss	%xmm2, %xmm5
	addss	%xmm5, %xmm6
	movss	84(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	movss	300(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm2
	addss	76(%rsp), %xmm2         # 4-byte Folded Reload
	addss	%xmm2, %xmm7
	mulss	%xmm11, %xmm7
	movss	72(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	addss	%xmm13, %xmm7
	addss	%xmm7, %xmm0
	movss	44(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm0
	divss	%xmm0, %xmm8
	movss	80(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	312(%rbx), %xmm8
	mulss	308(%rbx), %xmm8
	movss	12(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movaps	112(%rsp), %xmm14       # 16-byte Reload
	jmp	.LBB17_31
.LBB17_27:                              # %._crit_edge
	movss	280(%r12), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	284(%r12), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	288(%r12), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movss	296(%r12), %xmm15       # xmm15 = mem[0],zero,zero,zero
	movss	300(%r12), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	304(%r12), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movss	312(%r12), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	316(%r12), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm8
	movss	320(%r12), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm13
	xorps	%xmm8, %xmm8
.LBB17_31:
	movaps	160(%rsp), %xmm6        # 16-byte Reload
	mulss	%xmm6, %xmm7
	movaps	176(%rsp), %xmm4        # 16-byte Reload
	mulss	%xmm4, %xmm5
	addss	%xmm7, %xmm5
	movaps	192(%rsp), %xmm7        # 16-byte Reload
	mulss	%xmm7, %xmm9
	addss	%xmm5, %xmm9
	mulss	%xmm6, %xmm15
	mulss	%xmm4, %xmm1
	addss	%xmm15, %xmm1
	mulss	%xmm7, %xmm10
	addss	%xmm1, %xmm10
	mulss	%xmm6, %xmm0
	mulss	%xmm4, %xmm3
	addss	%xmm0, %xmm3
	mulss	%xmm7, %xmm12
	addss	%xmm3, %xmm12
	movss	144(%rsp), %xmm11       # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	movss	.LCPI17_2(%rip), %xmm5  # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm0
	movss	(%r13), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r13)
	movss	4(%r13), %xmm1          # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 4(%r13)
	addss	8(%r13), %xmm0
	movss	%xmm0, 8(%r13)
	movss	32(%r13), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm0
	mulss	%xmm9, %xmm0
	movss	36(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm1
	mulss	%xmm10, %xmm1
	movss	40(%r13), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm2
	mulss	%xmm12, %xmm2
	movss	16(%r13), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm3
	movss	%xmm3, 16(%r13)
	movss	20(%r13), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm0
	movss	%xmm0, 20(%r13)
	movss	24(%r13), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm0
	movss	%xmm0, 24(%r13)
	movss	280(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm0
	movss	284(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	addss	%xmm0, %xmm2
	movss	288(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm1
	addss	%xmm2, %xmm1
	movss	296(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm0
	movss	300(%r14), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	addss	%xmm0, %xmm3
	movss	304(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	addss	%xmm3, %xmm2
	mulss	312(%r14), %xmm6
	mulss	316(%r14), %xmm4
	addss	%xmm6, %xmm4
	mulss	320(%r14), %xmm7
	addss	%xmm4, %xmm7
	xorps	%xmm0, %xmm0
	movaps	%xmm11, %xmm3
	mulss	%xmm0, %xmm3
	movss	(%r15), %xmm4           # xmm4 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm4
	movss	%xmm4, (%r15)
	movss	4(%r15), %xmm4          # xmm4 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm4
	movss	%xmm4, 4(%r15)
	addss	8(%r15), %xmm3
	movss	%xmm3, 8(%r15)
	movss	32(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm3
	mulss	%xmm1, %xmm3
	movss	36(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm1
	mulss	%xmm2, %xmm1
	mulss	40(%r15), %xmm11
	mulss	%xmm7, %xmm11
	addss	16(%r15), %xmm3
	movss	%xmm3, 16(%r15)
	addss	20(%r15), %xmm1
	movss	%xmm1, 20(%r15)
	addss	24(%r15), %xmm11
	movss	%xmm11, 24(%r15)
	movss	280(%r12), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm1
	movss	284(%r12), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	addss	%xmm1, %xmm2
	movss	288(%r12), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm1
	addss	%xmm2, %xmm1
	movss	296(%r12), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm2
	movss	300(%r12), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm3
	addss	%xmm2, %xmm3
	movss	304(%r12), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	addss	%xmm3, %xmm2
	movss	312(%r12), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm3
	movss	316(%r12), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm4
	addss	%xmm3, %xmm4
	movss	320(%r12), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm3
	addss	%xmm4, %xmm3
	mulss	%xmm8, %xmm0
	movss	(%r13), %xmm4           # xmm4 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm4
	movss	%xmm4, (%r13)
	movss	4(%r13), %xmm4          # xmm4 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm4
	movss	%xmm4, 4(%r13)
	addss	8(%r13), %xmm0
	movss	%xmm0, 8(%r13)
	movss	32(%r13), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm0
	mulss	%xmm1, %xmm0
	movss	36(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm1
	mulss	%xmm2, %xmm1
	movss	40(%r13), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm2
	mulss	%xmm3, %xmm2
	addss	16(%r13), %xmm0
	movss	%xmm0, 16(%r13)
	addss	20(%r13), %xmm1
	movss	%xmm1, 20(%r13)
	addss	24(%r13), %xmm2
	movss	%xmm2, 24(%r13)
	movss	280(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm0
	movss	284(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm1
	addss	%xmm0, %xmm1
	movss	288(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm0
	addss	%xmm1, %xmm0
	movss	296(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm1
	movss	300(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	addss	%xmm1, %xmm2
	movss	304(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm1
	addss	%xmm2, %xmm1
	mulss	312(%r14), %xmm9
	mulss	316(%r14), %xmm7
	addss	%xmm9, %xmm7
	mulss	320(%r14), %xmm6
	addss	%xmm7, %xmm6
	movaps	%xmm8, %xmm2
	mulss	%xmm5, %xmm2
	movss	(%r15), %xmm3           # xmm3 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm3
	movss	%xmm3, (%r15)
	movss	4(%r15), %xmm3          # xmm3 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm3
	movss	%xmm3, 4(%r15)
	addss	8(%r15), %xmm2
	movss	%xmm2, 8(%r15)
	movss	32(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm2
	mulss	%xmm0, %xmm2
	movss	36(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm0
	mulss	%xmm1, %xmm0
	mulss	40(%r15), %xmm8
	mulss	%xmm6, %xmm8
	movss	16(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm1
	movss	%xmm1, 16(%r15)
	movss	20(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 20(%r15)
	movss	24(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm8, %xmm0
	movss	%xmm0, 24(%r15)
	movaps	272(%rsp), %xmm1        # 16-byte Reload
	subss	256(%rsp), %xmm1        # 16-byte Folded Reload
	movaps	208(%rsp), %xmm0        # 16-byte Reload
	subss	224(%rsp), %xmm0        # 16-byte Folded Reload
	subss	240(%rsp), %xmm14       # 16-byte Folded Reload
	cmpb	$0, 321(%rbx)
	movss	128(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	mulss	%xmm13, %xmm0
	addss	%xmm1, %xmm0
	movss	48(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm14
	addss	%xmm0, %xmm14
	movss	1108(%rbx), %xmm1       # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm9
	movaps	%xmm3, %xmm8
	movaps	%xmm13, %xmm10
	je	.LBB17_33
# BB#32:
	mulss	292(%rbx), %xmm14
	mulss	288(%rbx), %xmm1
	divss	840(%rbx), %xmm1
	addss	%xmm14, %xmm1
	leaq	284(%rbx), %rax
	jmp	.LBB17_34
.LBB17_33:
	mulss	268(%rbx), %xmm14
	mulss	264(%rbx), %xmm1
	divss	840(%rbx), %xmm1
	addss	%xmm14, %xmm1
	leaq	260(%rbx), %rax
.LBB17_34:
	movss	1112(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	mulss	(%rax), %xmm0
	mulss	%xmm1, %xmm0
	movss	280(%r12), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm1
	movss	284(%r12), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm2
	addss	%xmm1, %xmm2
	movss	288(%r12), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm1
	addss	%xmm2, %xmm1
	movss	296(%r12), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm2
	movss	300(%r12), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm3
	addss	%xmm2, %xmm3
	movss	304(%r12), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm2
	addss	%xmm3, %xmm2
	movss	312(%r12), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm3
	movss	316(%r12), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	addss	%xmm3, %xmm4
	movss	320(%r12), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm3
	addss	%xmm4, %xmm3
	xorps	%xmm4, %xmm4
	mulss	%xmm0, %xmm4
	movss	(%r13), %xmm5           # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, (%r13)
	movss	4(%r13), %xmm5          # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, 4(%r13)
	addss	8(%r13), %xmm4
	movss	%xmm4, 8(%r13)
	movss	32(%r13), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	36(%r13), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	movss	40(%r13), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	mulss	%xmm1, %xmm4
	mulss	%xmm2, %xmm5
	mulss	%xmm3, %xmm6
	addss	16(%r13), %xmm4
	movss	%xmm4, 16(%r13)
	addss	20(%r13), %xmm5
	movss	%xmm5, 20(%r13)
	addss	24(%r13), %xmm6
	movss	%xmm6, 24(%r13)
	movss	280(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm1
	movss	284(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm2
	addss	%xmm1, %xmm2
	movss	288(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm1
	addss	%xmm2, %xmm1
	movss	296(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm2
	movss	300(%r14), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm3
	addss	%xmm2, %xmm3
	movss	304(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm2
	addss	%xmm3, %xmm2
	movss	312(%r14), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm3
	movss	316(%r14), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	addss	%xmm3, %xmm4
	movss	320(%r14), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm3
	addss	%xmm4, %xmm3
	movss	.LCPI17_2(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	(%r15), %xmm5           # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, (%r15)
	movss	4(%r15), %xmm5          # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, 4(%r15)
	addss	8(%r15), %xmm4
	movss	%xmm4, 8(%r15)
	movss	32(%r15), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	36(%r15), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	mulss	40(%r15), %xmm0
	mulss	%xmm1, %xmm4
	mulss	%xmm2, %xmm5
	mulss	%xmm3, %xmm0
	movss	16(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm1
	movss	%xmm1, 16(%r15)
	movss	20(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm1
	movss	%xmm1, 20(%r15)
	movss	24(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 24(%r15)
	cmpb	$0, 1132(%rbx)
	je	.LBB17_37
# BB#35:
	movss	1140(%rbx), %xmm2       # xmm2 = mem[0],zero,zero,zero
	movss	1144(%rbx), %xmm1       # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jbe	.LBB17_37
# BB#36:
	movss	100(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	subss	92(%rsp), %xmm4         # 4-byte Folded Reload
	movss	104(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	subss	88(%rsp), %xmm0         # 4-byte Folded Reload
	movss	108(%rsp), %xmm3        # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	subss	96(%rsp), %xmm3         # 4-byte Folded Reload
	mulss	%xmm9, %xmm4
	mulss	%xmm10, %xmm0
	addss	%xmm4, %xmm0
	mulss	%xmm8, %xmm3
	addss	%xmm0, %xmm3
	movss	1136(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm0
	mulss	1112(%rbx), %xmm0
	movaps	.LCPI17_1(%rip), %xmm3  # xmm3 = [nan,nan,nan,nan]
	andps	%xmm0, %xmm3
	addss	%xmm1, %xmm3
	minss	%xmm3, %xmm2
	movaps	%xmm2, %xmm3
	subss	%xmm1, %xmm3
	movss	.LCPI17_2(%rip), %xmm7  # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm4
	shufps	$0, %xmm4, %xmm4        # xmm4 = xmm4[0,0,0,0]
	xorps	%xmm3, %xmm4
	xorps	%xmm1, %xmm1
	cmpltss	%xmm1, %xmm0
	movaps	%xmm0, %xmm5
	andnps	%xmm3, %xmm5
	andps	%xmm4, %xmm0
	orps	%xmm5, %xmm0
	movss	%xmm2, 1144(%rbx)
	movss	280(%r12), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm2
	movss	284(%r12), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm3
	addss	%xmm2, %xmm3
	movss	288(%r12), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm2
	addss	%xmm3, %xmm2
	movss	296(%r12), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm3
	movss	300(%r12), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	addss	%xmm3, %xmm4
	movss	304(%r12), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm3
	addss	%xmm4, %xmm3
	movss	312(%r12), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm4
	movss	316(%r12), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm5
	addss	%xmm4, %xmm5
	movss	320(%r12), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm4
	addss	%xmm5, %xmm4
	mulss	%xmm0, %xmm1
	movss	(%r13), %xmm5           # xmm5 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm5
	movss	%xmm5, (%r13)
	movss	4(%r13), %xmm5          # xmm5 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm5
	movss	%xmm5, 4(%r13)
	addss	8(%r13), %xmm1
	movss	%xmm1, 8(%r13)
	movss	32(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	36(%r13), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	movss	40(%r13), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	mulss	%xmm2, %xmm1
	mulss	%xmm3, %xmm5
	mulss	%xmm4, %xmm6
	addss	16(%r13), %xmm1
	movss	%xmm1, 16(%r13)
	addss	20(%r13), %xmm5
	movss	%xmm5, 20(%r13)
	addss	24(%r13), %xmm6
	movss	%xmm6, 24(%r13)
	movss	280(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm1
	movss	284(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm2
	addss	%xmm1, %xmm2
	movss	288(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm1
	addss	%xmm2, %xmm1
	movss	296(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm2
	movss	300(%r14), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm3
	addss	%xmm2, %xmm3
	movss	304(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm2
	addss	%xmm3, %xmm2
	mulss	312(%r14), %xmm9
	mulss	316(%r14), %xmm10
	addss	%xmm9, %xmm10
	mulss	320(%r14), %xmm8
	addss	%xmm10, %xmm8
	movaps	%xmm0, %xmm3
	mulss	%xmm7, %xmm3
	movss	(%r15), %xmm4           # xmm4 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm4
	movss	%xmm4, (%r15)
	movss	4(%r15), %xmm4          # xmm4 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm4
	movss	%xmm4, 4(%r15)
	addss	8(%r15), %xmm3
	movss	%xmm3, 8(%r15)
	movss	32(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movss	36(%r15), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	40(%r15), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	mulss	%xmm1, %xmm3
	mulss	%xmm2, %xmm4
	mulss	%xmm8, %xmm5
	movss	16(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm0
	movss	%xmm0, 16(%r15)
	movss	20(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm0
	movss	%xmm0, 20(%r15)
	movss	24(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm0
	movss	%xmm0, 24(%r15)
.LBB17_37:
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	_ZN18btSliderConstraint18solveConstraintIntER11btRigidBodyR12btSolverBodyS1_S3_, .Lfunc_end17-_ZN18btSliderConstraint18solveConstraintIntER11btRigidBodyR12btSolverBodyS1_S3_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI18_0:
	.long	1086918619              # float 6.28318548
.LCPI18_1:
	.long	3226013659              # float -3.14159274
.LCPI18_2:
	.long	1078530011              # float 3.14159274
.LCPI18_3:
	.long	3234402267              # float -6.28318548
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI18_4:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.text._Z21btAdjustAngleToLimitsfff,"axG",@progbits,_Z21btAdjustAngleToLimitsfff,comdat
	.weak	_Z21btAdjustAngleToLimitsfff
	.p2align	4, 0x90
	.type	_Z21btAdjustAngleToLimitsfff,@function
_Z21btAdjustAngleToLimitsfff:           # @_Z21btAdjustAngleToLimitsfff
	.cfi_startproc
# BB#0:
	subq	$40, %rsp
.Lcfi72:
	.cfi_def_cfa_offset 48
	movaps	%xmm1, %xmm3
	ucomiss	%xmm2, %xmm3
	jae	.LBB18_21
# BB#1:
	ucomiss	%xmm0, %xmm3
	jbe	.LBB18_11
# BB#2:
	movss	%xmm2, (%rsp)           # 4-byte Spill
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	subss	%xmm0, %xmm3
	movss	.LCPI18_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	callq	fmodf
	movss	.LCPI18_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB18_4
# BB#3:
	addss	.LCPI18_0(%rip), %xmm0
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	movss	(%rsp), %xmm2           # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	jmp	.LBB18_6
.LBB18_11:
	ucomiss	%xmm2, %xmm0
	jbe	.LBB18_21
# BB#12:
	movss	%xmm3, (%rsp)           # 4-byte Spill
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	subss	%xmm2, %xmm0
	movss	.LCPI18_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	callq	fmodf
	movaps	%xmm0, %xmm1
	movss	.LCPI18_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jbe	.LBB18_14
# BB#13:
	addss	.LCPI18_0(%rip), %xmm1
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	movss	(%rsp), %xmm2           # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	jmp	.LBB18_16
.LBB18_4:
	ucomiss	.LCPI18_2(%rip), %xmm0
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	movss	(%rsp), %xmm2           # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	jbe	.LBB18_6
# BB#5:
	addss	.LCPI18_3(%rip), %xmm0
.LBB18_6:                               # %_Z16btNormalizeAnglef.exit
	movaps	%xmm0, (%rsp)           # 16-byte Spill
	subss	%xmm1, %xmm2
	movss	.LCPI18_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm0
	callq	fmodf
	movss	.LCPI18_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB18_8
# BB#7:
	addss	.LCPI18_0(%rip), %xmm0
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	movaps	(%rsp), %xmm2           # 16-byte Reload
	jmp	.LBB18_10
.LBB18_8:
	ucomiss	.LCPI18_2(%rip), %xmm0
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	movaps	(%rsp), %xmm2           # 16-byte Reload
	jbe	.LBB18_10
# BB#9:
	addss	.LCPI18_3(%rip), %xmm0
.LBB18_10:                              # %_Z16btNormalizeAnglef.exit30
	andps	.LCPI18_4(%rip), %xmm0
	cmpltss	%xmm0, %xmm2
	movaps	%xmm2, %xmm0
	andps	%xmm1, %xmm0
	addss	.LCPI18_0(%rip), %xmm1
	andnps	%xmm1, %xmm2
	orps	%xmm0, %xmm2
	movaps	%xmm2, %xmm0
	addq	$40, %rsp
	retq
.LBB18_14:
	ucomiss	.LCPI18_2(%rip), %xmm1
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	movss	(%rsp), %xmm2           # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	jbe	.LBB18_16
# BB#15:
	addss	.LCPI18_3(%rip), %xmm1
.LBB18_16:                              # %_Z16btNormalizeAnglef.exit32
	movss	%xmm1, (%rsp)           # 4-byte Spill
	subss	%xmm2, %xmm0
	movss	.LCPI18_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	callq	fmodf
	movss	.LCPI18_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB18_18
# BB#17:
	addss	.LCPI18_0(%rip), %xmm0
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	movss	(%rsp), %xmm3           # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	jmp	.LBB18_20
.LBB18_18:
	ucomiss	.LCPI18_2(%rip), %xmm0
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	movss	(%rsp), %xmm3           # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	jbe	.LBB18_20
# BB#19:
	addss	.LCPI18_3(%rip), %xmm0
.LBB18_20:                              # %_Z16btNormalizeAnglef.exit34
	andps	.LCPI18_4(%rip), %xmm0
	movss	.LCPI18_3(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm1
	cmpltss	%xmm3, %xmm0
	andps	%xmm0, %xmm1
	andnps	%xmm2, %xmm0
	orps	%xmm1, %xmm0
.LBB18_21:
	addq	$40, %rsp
	retq
.Lfunc_end18:
	.size	_Z21btAdjustAngleToLimitsfff, .Lfunc_end18-_Z21btAdjustAngleToLimitsfff
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI19_0:
	.long	1056964608              # float 0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI19_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN18btSliderConstraint11getAncorInAEv
	.p2align	4, 0x90
	.type	_ZN18btSliderConstraint11getAncorInAEv,@function
_ZN18btSliderConstraint11getAncorInAEv: # @_ZN18btSliderConstraint11getAncorInAEv
	.cfi_startproc
# BB#0:
	movss	232(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	addss	236(%rdi), %xmm2
	mulss	.LCPI19_0(%rip), %xmm2
	movss	972(%rdi), %xmm14       # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm14
	movss	976(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	mulss	980(%rdi), %xmm2
	addss	988(%rdi), %xmm14
	addss	992(%rdi), %xmm1
	addss	996(%rdi), %xmm2
	movq	24(%rdi), %rax
	movsd	8(%rax), %xmm13         # xmm13 = mem[0],zero
	movsd	24(%rax), %xmm12        # xmm12 = mem[0],zero
	movsd	40(%rax), %xmm11        # xmm11 = mem[0],zero
	movss	16(%rax), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	32(%rax), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	48(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movd	56(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movdqa	.LCPI19_1(%rip), %xmm7  # xmm7 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm7, %xmm4
	movd	60(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movdqa	%xmm0, %xmm5
	pxor	%xmm7, %xmm5
	movd	64(%rax), %xmm6         # xmm6 = mem[0],zero,zero,zero
	pxor	%xmm6, %xmm7
	pshufd	$224, %xmm4, %xmm3      # xmm3 = xmm4[0,0,2,3]
	mulps	%xmm13, %xmm3
	pshufd	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm12, %xmm5
	addps	%xmm3, %xmm5
	pshufd	$224, %xmm7, %xmm3      # xmm3 = xmm7[0,0,2,3]
	mulps	%xmm11, %xmm3
	addps	%xmm5, %xmm3
	mulss	%xmm10, %xmm4
	mulss	%xmm9, %xmm0
	subss	%xmm0, %xmm4
	mulss	%xmm8, %xmm6
	subss	%xmm6, %xmm4
	movaps	%xmm14, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm13, %xmm0
	movaps	%xmm1, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm12, %xmm5
	addps	%xmm0, %xmm5
	movaps	%xmm2, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm11, %xmm0
	addps	%xmm5, %xmm0
	addps	%xmm3, %xmm0
	mulss	%xmm10, %xmm14
	mulss	%xmm9, %xmm1
	addss	%xmm14, %xmm1
	mulss	%xmm8, %xmm2
	addss	%xmm1, %xmm2
	addss	%xmm4, %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	retq
.Lfunc_end19:
	.size	_ZN18btSliderConstraint11getAncorInAEv, .Lfunc_end19-_ZN18btSliderConstraint11getAncorInAEv
	.cfi_endproc

	.globl	_ZN18btSliderConstraint11getAncorInBEv
	.p2align	4, 0x90
	.type	_ZN18btSliderConstraint11getAncorInBEv,@function
_ZN18btSliderConstraint11getAncorInBEv: # @_ZN18btSliderConstraint11getAncorInBEv
	.cfi_startproc
# BB#0:
	movsd	212(%rdi), %xmm0        # xmm0 = mem[0],zero
	movsd	220(%rdi), %xmm1        # xmm1 = mem[0],zero
	retq
.Lfunc_end20:
	.size	_ZN18btSliderConstraint11getAncorInBEv, .Lfunc_end20-_ZN18btSliderConstraint11getAncorInBEv
	.cfi_endproc

	.section	.text._ZN17btTypedConstraintD2Ev,"axG",@progbits,_ZN17btTypedConstraintD2Ev,comdat
	.weak	_ZN17btTypedConstraintD2Ev
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraintD2Ev,@function
_ZN17btTypedConstraintD2Ev:             # @_ZN17btTypedConstraintD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end21:
	.size	_ZN17btTypedConstraintD2Ev, .Lfunc_end21-_ZN17btTypedConstraintD2Ev
	.cfi_endproc

	.section	.text._ZN18btSliderConstraintD0Ev,"axG",@progbits,_ZN18btSliderConstraintD0Ev,comdat
	.weak	_ZN18btSliderConstraintD0Ev
	.p2align	4, 0x90
	.type	_ZN18btSliderConstraintD0Ev,@function
_ZN18btSliderConstraintD0Ev:            # @_ZN18btSliderConstraintD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end22:
	.size	_ZN18btSliderConstraintD0Ev, .Lfunc_end22-_ZN18btSliderConstraintD0Ev
	.cfi_endproc

	.section	.text._ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,"axG",@progbits,_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,comdat
	.weak	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,@function
_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif: # @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end23:
	.size	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif, .Lfunc_end23-_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_btSliderConstraint.ii,@function
_GLOBAL__sub_I_btSliderConstraint.ii:   # @_GLOBAL__sub_I_btSliderConstraint.ii
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi73:
	.cfi_def_cfa_offset 32
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movq	%rsp, %rcx
	movl	$_ZL7s_fixed, %edi
	xorps	%xmm0, %xmm0
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	_ZN11btRigidBodyC1EfP13btMotionStateP16btCollisionShapeRK9btVector3
	movl	$_ZN11btRigidBodyD2Ev, %edi
	movl	$_ZL7s_fixed, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	addq	$24, %rsp
	retq
.Lfunc_end24:
	.size	_GLOBAL__sub_I_btSliderConstraint.ii, .Lfunc_end24-_GLOBAL__sub_I_btSliderConstraint.ii
	.cfi_endproc

	.type	_ZTV18btSliderConstraint,@object # @_ZTV18btSliderConstraint
	.section	.rodata,"a",@progbits
	.globl	_ZTV18btSliderConstraint
	.p2align	3
_ZTV18btSliderConstraint:
	.quad	0
	.quad	_ZTI18btSliderConstraint
	.quad	_ZN17btTypedConstraintD2Ev
	.quad	_ZN18btSliderConstraintD0Ev
	.quad	_ZN18btSliderConstraint13buildJacobianEv
	.quad	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.quad	_ZN18btSliderConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.quad	_ZN18btSliderConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.quad	_ZN18btSliderConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.size	_ZTV18btSliderConstraint, 72

	.type	_ZL7s_fixed,@object     # @_ZL7s_fixed
	.local	_ZL7s_fixed
	.comm	_ZL7s_fixed,568,8
	.type	_ZTS18btSliderConstraint,@object # @_ZTS18btSliderConstraint
	.globl	_ZTS18btSliderConstraint
	.p2align	4
_ZTS18btSliderConstraint:
	.asciz	"18btSliderConstraint"
	.size	_ZTS18btSliderConstraint, 21

	.type	_ZTS17btTypedConstraint,@object # @_ZTS17btTypedConstraint
	.section	.rodata._ZTS17btTypedConstraint,"aG",@progbits,_ZTS17btTypedConstraint,comdat
	.weak	_ZTS17btTypedConstraint
	.p2align	4
_ZTS17btTypedConstraint:
	.asciz	"17btTypedConstraint"
	.size	_ZTS17btTypedConstraint, 20

	.type	_ZTS13btTypedObject,@object # @_ZTS13btTypedObject
	.section	.rodata._ZTS13btTypedObject,"aG",@progbits,_ZTS13btTypedObject,comdat
	.weak	_ZTS13btTypedObject
_ZTS13btTypedObject:
	.asciz	"13btTypedObject"
	.size	_ZTS13btTypedObject, 16

	.type	_ZTI13btTypedObject,@object # @_ZTI13btTypedObject
	.section	.rodata._ZTI13btTypedObject,"aG",@progbits,_ZTI13btTypedObject,comdat
	.weak	_ZTI13btTypedObject
	.p2align	3
_ZTI13btTypedObject:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13btTypedObject
	.size	_ZTI13btTypedObject, 16

	.type	_ZTI17btTypedConstraint,@object # @_ZTI17btTypedConstraint
	.section	.rodata._ZTI17btTypedConstraint,"aG",@progbits,_ZTI17btTypedConstraint,comdat
	.weak	_ZTI17btTypedConstraint
	.p2align	4
_ZTI17btTypedConstraint:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS17btTypedConstraint
	.long	0                       # 0x0
	.long	1                       # 0x1
	.quad	_ZTI13btTypedObject
	.quad	2050                    # 0x802
	.size	_ZTI17btTypedConstraint, 40

	.type	_ZTI18btSliderConstraint,@object # @_ZTI18btSliderConstraint
	.section	.rodata,"a",@progbits
	.globl	_ZTI18btSliderConstraint
	.p2align	4
_ZTI18btSliderConstraint:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18btSliderConstraint
	.quad	_ZTI17btTypedConstraint
	.size	_ZTI18btSliderConstraint, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_btSliderConstraint.ii

	.globl	_ZN18btSliderConstraintC1Ev
	.type	_ZN18btSliderConstraintC1Ev,@function
_ZN18btSliderConstraintC1Ev = _ZN18btSliderConstraintC2Ev
	.globl	_ZN18btSliderConstraintC1ER11btRigidBodyS1_RK11btTransformS4_b
	.type	_ZN18btSliderConstraintC1ER11btRigidBodyS1_RK11btTransformS4_b,@function
_ZN18btSliderConstraintC1ER11btRigidBodyS1_RK11btTransformS4_b = _ZN18btSliderConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
	.globl	_ZN18btSliderConstraintC1ER11btRigidBodyRK11btTransformb
	.type	_ZN18btSliderConstraintC1ER11btRigidBodyRK11btTransformb,@function
_ZN18btSliderConstraintC1ER11btRigidBodyRK11btTransformb = _ZN18btSliderConstraintC2ER11btRigidBodyRK11btTransformb
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
