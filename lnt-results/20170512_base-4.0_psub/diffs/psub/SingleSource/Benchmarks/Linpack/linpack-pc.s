	.text
	.file	"linpack-pc.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	1232348160              # float 1.0E+6
	.text
	.globl	second
	.p2align	4, 0x90
	.type	second,@function
second:                                 # @second
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	callq	clock
	cvtsi2ssq	%rax, %xmm0
	divss	.LCPI0_0(%rip), %xmm0
	popq	%rax
	retq
.Lfunc_end0:
	.size	second, .Lfunc_end0-second
	.cfi_endproc

	.globl	what_date
	.p2align	4, 0x90
	.type	what_date,@function
what_date:                              # @what_date
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	what_date, .Lfunc_end1-what_date
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4693973835154019669     # double 666666.66666666663
.LCPI2_1:
	.quad	4671226772094713856     # double 2.0E+4
.LCPI2_2:
	.quad	4544132024016830464     # double 6.103515625E-5
.LCPI2_8:
	.quad	4503599627370496000     # double 1.1920928955078125E-7
.LCPI2_9:
	.quad	4696837146684686336     # double 1.0E+6
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_3:
	.long	1232348160              # float 1.0E+6
.LCPI2_6:
	.long	872415232               # float 1.1920929E-7
.LCPI2_7:
	.long	3212836864              # float -1
.LCPI2_10:
	.long	1137180672              # float 400
.LCPI2_11:
	.long	1084227584              # float 5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_4:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI2_5:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 112
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movb	$1, main.lda(%rip)
	movb	$1, main.ldaa(%rip)
	movb	$1, main.n(%rip)
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$7, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$7, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$53, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	xorl	%ebx, %ebx
	movl	$.L.str.5, %esi
	movl	$.L.str, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	callq	fprintf
	movb	main.n(%rip), %r15b
	testb	%r15b, %r15b
	movl	$100, %r14d
	cmovel	%ebx, %r14d
	xorpd	%xmm0, %xmm0
	jne	.LBB2_2
# BB#1:
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	jne	.LBB2_3
	jmp	.LBB2_4
.LBB2_2:
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	je	.LBB2_4
.LBB2_3:
	movsd	.LCPI2_1(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
.LBB2_4:
	je	.LBB2_20
# BB#5:                                 # %.preheader53.lr.ph.i
	movb	main.lda(%rip), %cl
	xorl	%r8d, %r8d
	testb	%cl, %cl
	movl	$201, %r12d
	cmoveq	%r8, %r12
	movl	%r14d, %ebx
	leaq	(,%r12,4), %r13
	movl	$1325, %edi             # imm = 0x52D
	movl	$main.a+4, %ecx
	movsd	.LCPI2_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB2_6:                                # %.preheader53.us.i.new
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_7 Depth 2
	movq	%rcx, %rdx
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB2_7:                                #   Parent Loop BB2_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imull	$3125, %edi, %ebp       # imm = 0xC35
	movl	%ebp, %edi
	sarl	$31, %edi
	shrl	$16, %edi
	addl	%ebp, %edi
	andl	$-65536, %edi           # imm = 0xFFFF0000
	movl	%ebp, %eax
	subl	%edi, %eax
	negl	%edi
	leal	-32768(%rbp,%rdi), %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, -4(%rdx)
	imull	$3125, %eax, %ebp       # imm = 0xC35
	movl	%ebp, %eax
	sarl	$31, %eax
	shrl	$16, %eax
	addl	%ebp, %eax
	andl	$-65536, %eax           # imm = 0xFFFF0000
	movl	%ebp, %edi
	subl	%eax, %edi
	negl	%eax
	leal	-32768(%rbp,%rax), %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	%xmm0, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, (%rdx)
	addq	$8, %rdx
	addq	$-2, %rsi
	jne	.LBB2_7
# BB#8:                                 # %._crit_edge62.us.i
                                        #   in Loop: Header=BB2_6 Depth=1
	incq	%r8
	addq	%r13, %rcx
	cmpq	%rbx, %r8
	jne	.LBB2_6
# BB#9:                                 # %.preheader.us.preheader.i
	leal	-1(%r14), %eax
	leaq	4(,%rax,4), %rdx
	xorl	%ebp, %ebp
	movl	$main.b, %edi
	xorl	%esi, %esi
	callq	memset
	andl	$4, %r14d
	movq	%rbx, %rax
	subq	%r14, %rax
	movl	$main.a+16, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_10:                               # %.preheader.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_13 Depth 2
                                        #     Child Loop BB2_18 Depth 2
	testb	%r15b, %r15b
	je	.LBB2_16
# BB#11:                                # %min.iters.checked
                                        #   in Loop: Header=BB2_10 Depth=1
	testq	%rax, %rax
	je	.LBB2_16
# BB#12:                                # %vector.body.preheader
                                        #   in Loop: Header=BB2_10 Depth=1
	movq	%rcx, %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_13:                               # %vector.body
                                        #   Parent Loop BB2_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	addps	main.b(,%rdi,4), %xmm0
	addps	main.b+16(,%rdi,4), %xmm1
	movaps	%xmm0, main.b(,%rdi,4)
	movaps	%xmm1, main.b+16(,%rdi,4)
	addq	$8, %rdi
	addq	$32, %rsi
	cmpq	%rdi, %rax
	jne	.LBB2_13
# BB#14:                                # %middle.block
                                        #   in Loop: Header=BB2_10 Depth=1
	testl	%r14d, %r14d
	movq	%rax, %rsi
	jne	.LBB2_17
	jmp	.LBB2_19
	.p2align	4, 0x90
.LBB2_16:                               #   in Loop: Header=BB2_10 Depth=1
	xorl	%esi, %esi
.LBB2_17:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB2_10 Depth=1
	leaq	(%rsi,%rbp), %rdi
	leaq	main.a(,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB2_18:                               # %scalar.ph
                                        #   Parent Loop BB2_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	main.b(,%rsi,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	(%rdi), %xmm0
	movss	%xmm0, main.b(,%rsi,4)
	incq	%rsi
	addq	$4, %rdi
	cmpq	%rsi, %rbx
	jne	.LBB2_18
.LBB2_19:                               # %._crit_edge.us.i
                                        #   in Loop: Header=BB2_10 Depth=1
	incq	%rdx
	addq	%r13, %rcx
	addq	%r12, %rbp
	cmpq	%rbx, %rdx
	jne	.LBB2_10
.LBB2_20:                               # %matgen.exit
	callq	clock
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	divss	.LCPI2_3(%rip), %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movb	main.lda(%rip), %al
	xorl	%ebp, %ebp
	testb	%al, %al
	movl	$201, %r14d
	movl	$0, %esi
	cmovnel	%r14d, %esi
	movb	main.n(%rip), %al
	testb	%al, %al
	movl	$100, %ebx
	movl	$0, %edx
	cmovnel	%ebx, %edx
	movl	$main.a, %edi
	movl	$main.ipvt, %ecx
	movl	$main.info, %r8d
	callq	dgefa
	callq	clock
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	divss	.LCPI2_3(%rip), %xmm0
	subss	8(%rsp), %xmm0          # 4-byte Folded Reload
	movss	%xmm0, atime.0(%rip)
	callq	clock
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	divss	.LCPI2_3(%rip), %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movb	main.lda(%rip), %al
	testb	%al, %al
	cmovel	%ebp, %r14d
	movb	main.n(%rip), %al
	testb	%al, %al
	movl	$0, %edx
	cmovnel	%ebx, %edx
	movl	$main.a, %edi
	movl	$main.ipvt, %ecx
	movl	$main.b, %r8d
	xorl	%r9d, %r9d
	movl	%r14d, %esi
	callq	dgesl
	callq	clock
	cvtsi2ssq	%rax, %xmm4
	divss	.LCPI2_3(%rip), %xmm4
	subss	8(%rsp), %xmm4          # 4-byte Folded Reload
	movss	%xmm4, atime.1(%rip)
	movss	atime.0(%rip), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movl	$0, main.i(%rip)
	movb	main.n(%rip), %r12b
	testb	%r12b, %r12b
	cmovel	%ebp, %ebx
	je	.LBB2_41
# BB#21:                                # %vector.body499
	movaps	main.b(%rip), %xmm0
	movaps	main.b+16(%rip), %xmm1
	movaps	%xmm0, main.x(%rip)
	movaps	%xmm1, main.x+16(%rip)
	movaps	main.b+32(%rip), %xmm0
	movaps	main.b+48(%rip), %xmm1
	movaps	%xmm0, main.x+32(%rip)
	movaps	%xmm1, main.x+48(%rip)
	movaps	main.b+64(%rip), %xmm0
	movaps	main.b+80(%rip), %xmm1
	movaps	%xmm0, main.x+64(%rip)
	movaps	%xmm1, main.x+80(%rip)
	movaps	main.b+96(%rip), %xmm0
	movaps	main.b+112(%rip), %xmm1
	movaps	%xmm0, main.x+96(%rip)
	movaps	%xmm1, main.x+112(%rip)
	movaps	main.b+128(%rip), %xmm0
	movaps	main.b+144(%rip), %xmm1
	movaps	%xmm0, main.x+128(%rip)
	movaps	%xmm1, main.x+144(%rip)
	movaps	main.b+160(%rip), %xmm0
	movaps	main.b+176(%rip), %xmm1
	movaps	%xmm0, main.x+160(%rip)
	movaps	%xmm1, main.x+176(%rip)
	movaps	main.b+192(%rip), %xmm0
	movaps	main.b+208(%rip), %xmm1
	movaps	%xmm0, main.x+192(%rip)
	movaps	%xmm1, main.x+208(%rip)
	movaps	main.b+224(%rip), %xmm0
	movaps	main.b+240(%rip), %xmm1
	movaps	%xmm0, main.x+224(%rip)
	movaps	%xmm1, main.x+240(%rip)
	movaps	main.b+256(%rip), %xmm0
	movaps	main.b+272(%rip), %xmm1
	movaps	%xmm0, main.x+256(%rip)
	movaps	%xmm1, main.x+272(%rip)
	movaps	main.b+288(%rip), %xmm0
	movaps	main.b+304(%rip), %xmm1
	movaps	%xmm0, main.x+288(%rip)
	movaps	%xmm1, main.x+304(%rip)
	movaps	main.b+320(%rip), %xmm0
	movaps	main.b+336(%rip), %xmm1
	movaps	%xmm0, main.x+320(%rip)
	movaps	%xmm1, main.x+336(%rip)
	movaps	main.b+352(%rip), %xmm0
	movaps	main.b+368(%rip), %xmm1
	movaps	%xmm0, main.x+352(%rip)
	movaps	%xmm1, main.x+368(%rip)
	movl	$96, %eax
	.p2align	4, 0x90
.LBB2_22:                               # %.lr.ph426
                                        # =>This Inner Loop Header: Depth=1
	movl	main.b(,%rax,4), %ecx
	movl	%ecx, main.x(,%rax,4)
	incq	%rax
	cmpl	%ebx, %eax
	jl	.LBB2_22
# BB#23:                                # %.preheader53.lr.ph.i160
	movl	%eax, main.i(%rip)
	movb	main.lda(%rip), %al
	xorl	%r8d, %r8d
	movb	%al, 28(%rsp)           # 1-byte Spill
	testb	%al, %al
	movl	$201, %r13d
	cmoveq	%r8, %r13
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movl	%ebx, %ebx
	leaq	(,%r13,4), %r15
	movl	$main.a, %ecx
	movl	$1325, %edx             # imm = 0x52D
	xorps	%xmm2, %xmm2
	movsd	.LCPI2_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB2_24:                               # %.preheader53.us.i163
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_25 Depth 2
	movq	%rbx, %rsi
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB2_25:                               #   Parent Loop BB2_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm2, %xmm1
	imull	$3125, %edx, %eax       # imm = 0xC35
	movl	%eax, %ebp
	sarl	$31, %ebp
	shrl	$16, %ebp
	addl	%eax, %ebp
	andl	$-65536, %ebp           # imm = 0xFFFF0000
	movl	%eax, %edx
	subl	%ebp, %edx
	negl	%ebp
	leal	-32768(%rax,%rbp), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	mulsd	%xmm0, %xmm2
	cvtsd2ss	%xmm2, %xmm2
	movss	%xmm2, (%rdi)
	maxss	%xmm1, %xmm2
	addq	$4, %rdi
	decq	%rsi
	jne	.LBB2_25
# BB#26:                                # %._crit_edge62.us.i172
                                        #   in Loop: Header=BB2_24 Depth=1
	incq	%r8
	addq	%r15, %rcx
	cmpq	%rbx, %r8
	jne	.LBB2_24
# BB#27:                                # %.preheader.us.preheader.i175
	movss	%xmm2, 40(%rsp)         # 4-byte Spill
	movq	32(%rsp), %rbp          # 8-byte Reload
	leal	-1(%rbp), %eax
	leaq	4(,%rax,4), %rdx
	xorl	%r14d, %r14d
	movl	$main.b, %edi
	xorl	%esi, %esi
	movss	%xmm3, 8(%rsp)          # 4-byte Spill
	movss	%xmm4, 48(%rsp)         # 4-byte Spill
	callq	memset
	movss	48(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movl	%ebp, %r8d
	andl	$4, %r8d
	movq	%rbx, %rcx
	subq	%r8, %rcx
	movl	$main.a+16, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_28:                               # %.preheader.us.i177
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_31 Depth 2
                                        #     Child Loop BB2_36 Depth 2
	testb	%r12b, %r12b
	je	.LBB2_34
# BB#29:                                # %min.iters.checked522
                                        #   in Loop: Header=BB2_28 Depth=1
	testq	%rcx, %rcx
	je	.LBB2_34
# BB#30:                                # %vector.body518.preheader
                                        #   in Loop: Header=BB2_28 Depth=1
	movq	%rdx, %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_31:                               # %vector.body518
                                        #   Parent Loop BB2_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	addps	main.b(,%rax,4), %xmm0
	addps	main.b+16(,%rax,4), %xmm1
	movaps	%xmm0, main.b(,%rax,4)
	movaps	%xmm1, main.b+16(,%rax,4)
	addq	$8, %rax
	addq	$32, %rdi
	cmpq	%rax, %rcx
	jne	.LBB2_31
# BB#32:                                # %middle.block519
                                        #   in Loop: Header=BB2_28 Depth=1
	testl	%r8d, %r8d
	movq	%rcx, %rdi
	jne	.LBB2_35
	jmp	.LBB2_37
	.p2align	4, 0x90
.LBB2_34:                               #   in Loop: Header=BB2_28 Depth=1
	xorl	%edi, %edi
.LBB2_35:                               # %scalar.ph520.preheader
                                        #   in Loop: Header=BB2_28 Depth=1
	leaq	(%rdi,%r14), %rax
	leaq	main.a(,%rax,4), %rax
	.p2align	4, 0x90
.LBB2_36:                               # %scalar.ph520
                                        #   Parent Loop BB2_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	main.b(,%rdi,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, main.b(,%rdi,4)
	incq	%rdi
	addq	$4, %rax
	cmpq	%rdi, %rbx
	jne	.LBB2_36
.LBB2_37:                               # %._crit_edge.us.i183
                                        #   in Loop: Header=BB2_28 Depth=1
	incq	%rsi
	addq	%r15, %rdx
	addq	%r13, %r14
	cmpq	%rbx, %rsi
	jne	.LBB2_28
# BB#38:                                # %matgen.exit184.preheader
	movl	$0, main.i(%rip)
	testb	%r12b, %r12b
	movq	32(%rsp), %rbx          # 8-byte Reload
	movb	28(%rsp), %al           # 1-byte Reload
	je	.LBB2_43
# BB#39:                                # %vector.body539
	movaps	.LCPI2_4(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	main.b(%rip), %xmm1
	xorps	%xmm0, %xmm1
	movaps	main.b+16(%rip), %xmm2
	xorps	%xmm0, %xmm2
	movaps	%xmm1, main.b(%rip)
	movaps	%xmm2, main.b+16(%rip)
	movaps	main.b+32(%rip), %xmm1
	xorps	%xmm0, %xmm1
	movaps	main.b+48(%rip), %xmm2
	xorps	%xmm0, %xmm2
	movaps	%xmm1, main.b+32(%rip)
	movaps	%xmm2, main.b+48(%rip)
	movaps	main.b+64(%rip), %xmm1
	xorps	%xmm0, %xmm1
	movaps	main.b+80(%rip), %xmm2
	xorps	%xmm0, %xmm2
	movaps	%xmm1, main.b+64(%rip)
	movaps	%xmm2, main.b+80(%rip)
	movaps	main.b+96(%rip), %xmm1
	xorps	%xmm0, %xmm1
	movaps	main.b+112(%rip), %xmm2
	xorps	%xmm0, %xmm2
	movaps	%xmm1, main.b+96(%rip)
	movaps	%xmm2, main.b+112(%rip)
	movaps	main.b+128(%rip), %xmm1
	xorps	%xmm0, %xmm1
	movaps	main.b+144(%rip), %xmm2
	xorps	%xmm0, %xmm2
	movaps	%xmm1, main.b+128(%rip)
	movaps	%xmm2, main.b+144(%rip)
	movaps	main.b+160(%rip), %xmm1
	xorps	%xmm0, %xmm1
	movaps	main.b+176(%rip), %xmm2
	xorps	%xmm0, %xmm2
	movaps	%xmm1, main.b+160(%rip)
	movaps	%xmm2, main.b+176(%rip)
	movaps	main.b+192(%rip), %xmm1
	xorps	%xmm0, %xmm1
	movaps	main.b+208(%rip), %xmm2
	xorps	%xmm0, %xmm2
	movaps	%xmm1, main.b+192(%rip)
	movaps	%xmm2, main.b+208(%rip)
	movaps	main.b+224(%rip), %xmm1
	xorps	%xmm0, %xmm1
	movaps	main.b+240(%rip), %xmm2
	xorps	%xmm0, %xmm2
	movaps	%xmm1, main.b+224(%rip)
	movaps	%xmm2, main.b+240(%rip)
	movaps	main.b+256(%rip), %xmm1
	xorps	%xmm0, %xmm1
	movaps	main.b+272(%rip), %xmm2
	xorps	%xmm0, %xmm2
	movaps	%xmm1, main.b+256(%rip)
	movaps	%xmm2, main.b+272(%rip)
	movaps	main.b+288(%rip), %xmm1
	xorps	%xmm0, %xmm1
	movaps	main.b+304(%rip), %xmm2
	xorps	%xmm0, %xmm2
	movaps	%xmm1, main.b+288(%rip)
	movaps	%xmm2, main.b+304(%rip)
	movaps	main.b+320(%rip), %xmm1
	xorps	%xmm0, %xmm1
	movaps	main.b+336(%rip), %xmm2
	xorps	%xmm0, %xmm2
	movaps	%xmm1, main.b+320(%rip)
	movaps	%xmm2, main.b+336(%rip)
	movaps	main.b+352(%rip), %xmm1
	xorps	%xmm0, %xmm1
	movaps	main.b+368(%rip), %xmm2
	xorps	%xmm0, %xmm2
	movaps	%xmm1, main.b+352(%rip)
	movaps	%xmm2, main.b+368(%rip)
	movl	$96, %ebp
	.p2align	4, 0x90
.LBB2_40:                               # %matgen.exit184
                                        # =>This Inner Loop Header: Depth=1
	movss	main.b(,%rbp,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm1
	movss	%xmm1, main.b(,%rbp,4)
	incq	%rbp
	cmpl	%ebx, %ebp
	jl	.LBB2_40
	jmp	.LBB2_42
.LBB2_41:                               # %matgen.exit184.preheader.thread
	xorps	%xmm0, %xmm0
	movss	%xmm0, 40(%rsp)         # 4-byte Spill
	movb	main.lda(%rip), %al
.LBB2_42:                               # %.sink.split
	movl	%ebp, main.i(%rip)
.LBB2_43:
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	addss	%xmm4, %xmm3
	movss	%xmm3, 8(%rsp)          # 4-byte Spill
	xorl	%ebp, %ebp
	testb	$1, %al
	movl	$201, %r14d
	movl	$0, %ecx
	cmovnel	%r14d, %ecx
	movl	$main.b, %esi
	movl	$main.x, %r8d
	movl	$main.a, %r9d
	movl	%ebx, %edi
	movl	%ebx, %edx
	callq	dmxpy
	movl	$0, main.i(%rip)
	movb	main.n(%rip), %al
	testb	%al, %al
	movl	$100, %ebx
	movl	$0, %eax
	cmovnel	%ebx, %eax
	xorps	%xmm3, %xmm3
	xorpd	%xmm0, %xmm0
	je	.LBB2_47
# BB#44:                                # %.lr.ph409
	movl	%eax, %ecx
	xorpd	%xmm0, %xmm0
	xorl	%edx, %edx
	movaps	.LCPI2_5(%rip), %xmm1   # xmm1 = [nan,nan,nan,nan]
	xorps	%xmm3, %xmm3
	.p2align	4, 0x90
.LBB2_45:                               # =>This Inner Loop Header: Depth=1
	movss	main.b(,%rdx,4), %xmm2  # xmm2 = mem[0],zero,zero,zero
	andps	%xmm1, %xmm2
	maxss	%xmm2, %xmm3
	movss	main.x(,%rdx,4), %xmm2  # xmm2 = mem[0],zero,zero,zero
	andps	%xmm1, %xmm2
	maxss	%xmm2, %xmm0
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB2_45
# BB#46:                                # %._crit_edge410
	movl	%edx, main.i(%rip)
.LBB2_47:
	movss	%xmm3, 32(%rsp)         # 4-byte Spill
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, 28(%rsp)         # 4-byte Spill
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%eax, %xmm1
	mulss	40(%rsp), %xmm1         # 4-byte Folded Reload
	mulss	%xmm0, %xmm1
	mulss	.LCPI2_6(%rip), %xmm1
	movaps	%xmm3, %xmm0
	divss	%xmm1, %xmm0
	movss	%xmm0, (%rsp)           # 4-byte Spill
	movss	.LCPI2_7(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	main.x(%rip), %xmm1     # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 16(%rsp)         # 4-byte Spill
	cltq
	addss	main.x-4(,%rax,4), %xmm0
	movss	%xmm0, 40(%rsp)         # 4-byte Spill
	movq	stderr(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$38, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$34, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	movss	(%rsp), %xmm0           # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movss	32(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	movss	16(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm3
	cvtss2sd	%xmm2, %xmm3
	movss	40(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm4
	movsd	.LCPI2_8(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.9, %esi
	movb	$5, %al
	callq	fprintf
	movq	stderr(%rip), %rdi
	movb	main.n(%rip), %al
	testb	%al, %al
	cmovel	%ebp, %ebx
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	fprintf
	movq	stderr(%rip), %rdi
	movb	main.lda(%rip), %al
	testb	%al, %al
	cmovel	%ebp, %r14d
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movl	%r14d, %edx
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.12, %edi
	movl	$55, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.13, %edi
	movl	$12, %esi
	movl	$1, %edx
	callq	fwrite
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	xorpd	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jbe	.LBB2_49
# BB#48:
	cvtss2sd	%xmm0, %xmm0
	movss	28(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	mulsd	.LCPI2_9(%rip), %xmm0
	divsd	%xmm0, %xmm1
	cvtsd2ss	%xmm1, %xmm1
.LBB2_49:
	movss	%xmm1, atime.3(%rip)
	movq	stderr(%rip), %rcx
	movl	$.L.str.14, %edi
	movl	$29, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$-3, %r15d
	movl	$100, %ebx
	.p2align	4, 0x90
.LBB2_50:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_70 Depth 2
                                        #     Child Loop BB2_72 Depth 2
                                        #     Child Loop BB2_53 Depth 2
                                        #       Child Loop BB2_54 Depth 3
                                        #         Child Loop BB2_55 Depth 4
                                        #       Child Loop BB2_58 Depth 3
                                        #         Child Loop BB2_61 Depth 4
                                        #         Child Loop BB2_66 Depth 4
	callq	clock
	movq	%rax, 48(%rsp)          # 8-byte Spill
	incl	%r15d
	movl	$0, main.i(%rip)
	testl	%ebx, %ebx
	movq	%rbx, %rbp
	jle	.LBB2_74
# BB#51:                                # %.lr.ph396
                                        #   in Loop: Header=BB2_50 Depth=1
	movb	main.lda(%rip), %al
	movb	main.n(%rip), %bl
	testb	%bl, %bl
	movl	$0, %r14d
	movl	$100, %ecx
	cmovnel	%ecx, %r14d
	testb	%al, %al
	movl	$0, %r13d
	movl	$201, %eax
	cmovneq	%rax, %r13
	testb	%bl, %bl
	movl	%r15d, 32(%rsp)         # 4-byte Spill
	je	.LBB2_69
# BB#52:                                # %.lr.ph396.split.us.preheader
                                        #   in Loop: Header=BB2_50 Depth=1
	leal	-1(%r14), %eax
	leaq	4(,%rax,4), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%r14d, %r8d
	andl	$4, %r8d
	movq	%r14, %r12
	subq	%r8, %r12
	leaq	(,%r13,4), %r15
	xorl	%edx, %edx
	movsd	.LCPI2_2(%rip), %xmm1   # xmm1 = mem[0],zero
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movb	%bl, 16(%rsp)           # 1-byte Spill
	.p2align	4, 0x90
.LBB2_53:                               # %.lr.ph396.split.us
                                        #   Parent Loop BB2_50 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_54 Depth 3
                                        #         Child Loop BB2_55 Depth 4
                                        #       Child Loop BB2_58 Depth 3
                                        #         Child Loop BB2_61 Depth 4
                                        #         Child Loop BB2_66 Depth 4
	movl	%edx, (%rsp)            # 4-byte Spill
	movl	$1325, %edi             # imm = 0x52D
	movl	$main.a+4, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_54:                               # %.preheader53.us.i189.us.new
                                        #   Parent Loop BB2_50 Depth=1
                                        #     Parent Loop BB2_53 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_55 Depth 4
	movq	%rax, %rdx
	movq	%r14, %rsi
	.p2align	4, 0x90
.LBB2_55:                               #   Parent Loop BB2_50 Depth=1
                                        #     Parent Loop BB2_53 Depth=2
                                        #       Parent Loop BB2_54 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	imull	$3125, %edi, %ebp       # imm = 0xC35
	movl	%ebp, %edi
	sarl	$31, %edi
	shrl	$16, %edi
	addl	%ebp, %edi
	andl	$-65536, %edi           # imm = 0xFFFF0000
	movl	%ebp, %ebx
	subl	%edi, %ebx
	negl	%edi
	leal	-32768(%rbp,%rdi), %edi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, -4(%rdx)
	imull	$3125, %ebx, %ebx       # imm = 0xC35
	movl	%ebx, %ebp
	sarl	$31, %ebp
	shrl	$16, %ebp
	addl	%ebx, %ebp
	andl	$-65536, %ebp           # imm = 0xFFFF0000
	movl	%ebx, %edi
	subl	%ebp, %edi
	negl	%ebp
	leal	-32768(%rbx,%rbp), %ebp
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rdx)
	addq	$8, %rdx
	addq	$-2, %rsi
	jne	.LBB2_55
# BB#56:                                # %._crit_edge62.us.i198.us
                                        #   in Loop: Header=BB2_54 Depth=3
	incq	%rcx
	addq	%r15, %rax
	cmpq	%r14, %rcx
	jne	.LBB2_54
# BB#57:                                # %.preheader52.i199.us
                                        #   in Loop: Header=BB2_53 Depth=2
	movl	$main.b, %edi
	xorl	%esi, %esi
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	%r8, %rbx
	callq	memset
	movq	%rbx, %r8
	xorl	%eax, %eax
	movl	$main.a+16, %ecx
	xorl	%edx, %edx
	movq	8(%rsp), %rbp           # 8-byte Reload
	movb	16(%rsp), %bl           # 1-byte Reload
	.p2align	4, 0x90
.LBB2_58:                               # %.preheader.us.i203.us
                                        #   Parent Loop BB2_50 Depth=1
                                        #     Parent Loop BB2_53 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_61 Depth 4
                                        #         Child Loop BB2_66 Depth 4
	testb	%bl, %bl
	je	.LBB2_64
# BB#59:                                # %min.iters.checked563
                                        #   in Loop: Header=BB2_58 Depth=3
	testq	%r12, %r12
	je	.LBB2_64
# BB#60:                                # %vector.body559.preheader
                                        #   in Loop: Header=BB2_58 Depth=3
	movq	%rcx, %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_61:                               # %vector.body559
                                        #   Parent Loop BB2_50 Depth=1
                                        #     Parent Loop BB2_53 Depth=2
                                        #       Parent Loop BB2_58 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	addps	main.b(,%rdi,4), %xmm0
	addps	main.b+16(,%rdi,4), %xmm1
	movaps	%xmm0, main.b(,%rdi,4)
	movaps	%xmm1, main.b+16(,%rdi,4)
	addq	$8, %rdi
	addq	$32, %rsi
	cmpq	%rdi, %r12
	jne	.LBB2_61
# BB#62:                                # %middle.block560
                                        #   in Loop: Header=BB2_58 Depth=3
	testl	%r8d, %r8d
	movq	%r12, %rsi
	jne	.LBB2_65
	jmp	.LBB2_67
	.p2align	4, 0x90
.LBB2_64:                               #   in Loop: Header=BB2_58 Depth=3
	xorl	%esi, %esi
.LBB2_65:                               # %scalar.ph561.preheader
                                        #   in Loop: Header=BB2_58 Depth=3
	leaq	(%rsi,%rax), %rdi
	leaq	main.a(,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB2_66:                               # %scalar.ph561
                                        #   Parent Loop BB2_50 Depth=1
                                        #     Parent Loop BB2_53 Depth=2
                                        #       Parent Loop BB2_58 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movss	main.b(,%rsi,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	(%rdi), %xmm0
	movss	%xmm0, main.b(,%rsi,4)
	incq	%rsi
	addq	$4, %rdi
	cmpq	%rsi, %r14
	jne	.LBB2_66
.LBB2_67:                               # %._crit_edge.us.i209.us
                                        #   in Loop: Header=BB2_58 Depth=3
	incq	%rdx
	addq	%r15, %rcx
	addq	%r13, %rax
	cmpq	%r14, %rdx
	jne	.LBB2_58
# BB#68:                                # %matgen.exit210.loopexit.us
                                        #   in Loop: Header=BB2_53 Depth=2
	movl	(%rsp), %edx            # 4-byte Reload
	incl	%edx
	cmpl	%ebp, %edx
	movsd	.LCPI2_2(%rip), %xmm1   # xmm1 = mem[0],zero
	jl	.LBB2_53
	jmp	.LBB2_73
	.p2align	4, 0x90
.LBB2_69:                               # %.lr.ph396.split.preheader
                                        #   in Loop: Header=BB2_50 Depth=1
	leal	-1(%rbp), %eax
	movl	%ebp, %ecx
	xorl	%edx, %edx
	andl	$6, %ecx
	je	.LBB2_71
	.p2align	4, 0x90
.LBB2_70:                               # %.lr.ph396.split.prol
                                        #   Parent Loop BB2_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%edx
	cmpl	%edx, %ecx
	jne	.LBB2_70
.LBB2_71:                               # %.lr.ph396.split.prol.loopexit
                                        #   in Loop: Header=BB2_50 Depth=1
	cmpl	$7, %eax
	jb	.LBB2_73
	.p2align	4, 0x90
.LBB2_72:                               # %.lr.ph396.split
                                        #   Parent Loop BB2_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	$8, %edx
	cmpl	%ebp, %edx
	jl	.LBB2_72
.LBB2_73:                               # %._crit_edge397
                                        #   in Loop: Header=BB2_50 Depth=1
	movl	%edx, main.i(%rip)
	movl	32(%rsp), %r15d         # 4-byte Reload
.LBB2_74:                               #   in Loop: Header=BB2_50 Depth=1
	callq	clock
	movq	%rbp, %rbx
	movq	%rax, %rbp
	movq	stderr(%rip), %rdi
	movl	$.L.str.15, %esi
	xorpd	%xmm0, %xmm0
	movb	$1, %al
	movl	%ebx, %edx
	callq	fprintf
	addl	%ebx, %ebx
	testl	%r15d, %r15d
	jne	.LBB2_50
# BB#75:
	movq	48(%rsp), %rax          # 8-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	movss	.LCPI2_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	cvtsi2ssq	%rbp, %xmm2
	divss	%xmm1, %xmm2
	subss	%xmm0, %xmm2
	movss	%xmm2, 8(%rsp)          # 4-byte Spill
	movq	stderr(%rip), %rdi
	movl	$.L.str.16, %esi
	xorps	%xmm0, %xmm0
	movb	$1, %al
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.17, %edi
	movl	$46, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$-3, (%rsp)             # 4-byte Folded Spill
	movl	$100, %ebp
	movl	$100, %eax
	jmp	.LBB2_78
	.p2align	4, 0x90
.LBB2_76:                               # %._crit_edge391
                                        #   in Loop: Header=BB2_78 Depth=1
	callq	clock
	movq	stderr(%rip), %rdi
	movl	main.ntimes(%rip), %edx
	movl	$.L.str.15, %esi
	xorps	%xmm0, %xmm0
	movb	$1, %al
	callq	fprintf
	movl	(%rsp), %eax            # 4-byte Reload
	cmpl	$-2, %eax
	jg	.LBB2_96
# BB#77:                                #   in Loop: Header=BB2_78 Depth=1
	incl	%eax
	movl	%eax, (%rsp)            # 4-byte Spill
	movl	main.ntimes(%rip), %eax
	addl	%eax, %eax
.LBB2_78:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_79 Depth 2
                                        #       Child Loop BB2_81 Depth 3
                                        #         Child Loop BB2_82 Depth 4
                                        #       Child Loop BB2_85 Depth 3
                                        #         Child Loop BB2_88 Depth 4
                                        #         Child Loop BB2_93 Depth 4
	movl	%eax, main.ntimes(%rip)
	callq	clock
	movl	$0, main.i(%rip)
	cmpl	$0, main.ntimes(%rip)
	jle	.LBB2_76
	.p2align	4, 0x90
.LBB2_79:                               # %.lr.ph390
                                        #   Parent Loop BB2_78 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_81 Depth 3
                                        #         Child Loop BB2_82 Depth 4
                                        #       Child Loop BB2_85 Depth 3
                                        #         Child Loop BB2_88 Depth 4
                                        #         Child Loop BB2_93 Depth 4
	movb	main.lda(%rip), %r10b
	movb	main.n(%rip), %r14b
	testb	%r14b, %r14b
	movl	$0, %r9d
	cmovnel	%ebp, %r9d
	je	.LBB2_95
# BB#80:                                # %.preheader53.lr.ph.i212
                                        #   in Loop: Header=BB2_79 Depth=2
	testb	$1, %r10b
	movl	$0, %r15d
	movl	$201, %eax
	cmovneq	%rax, %r15
	movl	%r9d, %r12d
	leaq	(,%r15,4), %r13
	movl	$1325, %edi             # imm = 0x52D
	movl	$main.a+4, %eax
	xorl	%ecx, %ecx
	movsd	.LCPI2_2(%rip), %xmm1   # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB2_81:                               # %.preheader53.us.i215.new
                                        #   Parent Loop BB2_78 Depth=1
                                        #     Parent Loop BB2_79 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_82 Depth 4
	movq	%rax, %rdx
	movq	%r12, %rsi
	.p2align	4, 0x90
.LBB2_82:                               #   Parent Loop BB2_78 Depth=1
                                        #     Parent Loop BB2_79 Depth=2
                                        #       Parent Loop BB2_81 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	imull	$3125, %edi, %ebx       # imm = 0xC35
	movl	%ebx, %edi
	sarl	$31, %edi
	shrl	$16, %edi
	addl	%ebx, %edi
	andl	$-65536, %edi           # imm = 0xFFFF0000
	movl	%ebx, %ebp
	subl	%edi, %ebp
	negl	%edi
	leal	-32768(%rbx,%rdi), %edi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, -4(%rdx)
	imull	$3125, %ebp, %ebx       # imm = 0xC35
	movl	%ebx, %ebp
	sarl	$31, %ebp
	shrl	$16, %ebp
	addl	%ebx, %ebp
	andl	$-65536, %ebp           # imm = 0xFFFF0000
	movl	%ebx, %edi
	subl	%ebp, %edi
	negl	%ebp
	leal	-32768(%rbx,%rbp), %ebp
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rdx)
	addq	$8, %rdx
	addq	$-2, %rsi
	jne	.LBB2_82
# BB#83:                                # %._crit_edge62.us.i224
                                        #   in Loop: Header=BB2_81 Depth=3
	incq	%rcx
	addq	%r13, %rax
	cmpq	%r12, %rcx
	jne	.LBB2_81
# BB#84:                                # %.preheader.us.preheader.i227
                                        #   in Loop: Header=BB2_79 Depth=2
	leal	-1(%r9), %eax
	leaq	4(,%rax,4), %rdx
	movl	$main.b, %edi
	xorl	%esi, %esi
	movq	%r9, %rbp
	movl	%r10d, %ebx
	callq	memset
	movl	%ebx, %r10d
	movq	%rbp, %r9
	movl	%r9d, %r8d
	andl	$4, %r8d
	movq	%r12, %rbx
	subq	%r8, %rbx
	xorl	%edx, %edx
	movl	$main.a+16, %esi
	xorl	%edi, %edi
	movl	$100, %ebp
	.p2align	4, 0x90
.LBB2_85:                               # %.preheader.us.i229
                                        #   Parent Loop BB2_78 Depth=1
                                        #     Parent Loop BB2_79 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_88 Depth 4
                                        #         Child Loop BB2_93 Depth 4
	testb	%r14b, %r14b
	je	.LBB2_91
# BB#86:                                # %min.iters.checked584
                                        #   in Loop: Header=BB2_85 Depth=3
	testq	%rbx, %rbx
	je	.LBB2_91
# BB#87:                                # %vector.body580.preheader
                                        #   in Loop: Header=BB2_85 Depth=3
	movq	%rsi, %rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_88:                               # %vector.body580
                                        #   Parent Loop BB2_78 Depth=1
                                        #     Parent Loop BB2_79 Depth=2
                                        #       Parent Loop BB2_85 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	addps	main.b(,%rax,4), %xmm0
	addps	main.b+16(,%rax,4), %xmm1
	movaps	%xmm0, main.b(,%rax,4)
	movaps	%xmm1, main.b+16(,%rax,4)
	addq	$8, %rax
	addq	$32, %rcx
	cmpq	%rax, %rbx
	jne	.LBB2_88
# BB#89:                                # %middle.block581
                                        #   in Loop: Header=BB2_85 Depth=3
	testl	%r8d, %r8d
	movq	%rbx, %rcx
	jne	.LBB2_92
	jmp	.LBB2_94
	.p2align	4, 0x90
.LBB2_91:                               #   in Loop: Header=BB2_85 Depth=3
	xorl	%ecx, %ecx
.LBB2_92:                               # %scalar.ph582.preheader
                                        #   in Loop: Header=BB2_85 Depth=3
	leaq	(%rcx,%rdx), %rax
	leaq	main.a(,%rax,4), %rax
	.p2align	4, 0x90
.LBB2_93:                               # %scalar.ph582
                                        #   Parent Loop BB2_78 Depth=1
                                        #     Parent Loop BB2_79 Depth=2
                                        #       Parent Loop BB2_85 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movss	main.b(,%rcx,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, main.b(,%rcx,4)
	incq	%rcx
	addq	$4, %rax
	cmpq	%rcx, %r12
	jne	.LBB2_93
.LBB2_94:                               # %._crit_edge.us.i235
                                        #   in Loop: Header=BB2_85 Depth=3
	incq	%rdi
	addq	%r13, %rsi
	addq	%r15, %rdx
	cmpq	%r12, %rdi
	jne	.LBB2_85
.LBB2_95:                               # %matgen.exit236
                                        #   in Loop: Header=BB2_79 Depth=2
	testb	$1, %r10b
	movl	$0, %esi
	movl	$201, %eax
	cmovnel	%eax, %esi
	movl	$main.a, %edi
	movl	$main.ipvt, %ecx
	movl	$main.info, %r8d
	movl	%r9d, %edx
	callq	dgefa
	movl	main.i(%rip), %eax
	incl	%eax
	movl	%eax, main.i(%rip)
	cmpl	main.ntimes(%rip), %eax
	jl	.LBB2_79
	jmp	.LBB2_76
.LBB2_96:                               # %.critedge
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	divss	.LCPI2_10(%rip), %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movl	$1000, main.ntimes(%rip) # imm = 0x3E8
	movq	stderr(%rip), %rdi
	movl	$.L.str.18, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movb	main.lda(%rip), %al
	testb	%al, %al
	movl	$201, %ebx
	movl	$0, %edx
	cmovnel	%ebx, %edx
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.12, %edi
	movl	$55, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.13, %edi
	movl	$12, %esi
	movl	$1, %edx
	callq	fwrite
	movl	main.ntimes(%rip), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	mulss	8(%rsp), %xmm0          # 4-byte Folded Reload
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movl	$0, atime.3+24(%rip)
	movl	$1, main.j(%rip)
	movss	28(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movl	$100, %ebp
	.p2align	4, 0x90
.LBB2_97:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_98 Depth 2
                                        #       Child Loop BB2_100 Depth 3
                                        #         Child Loop BB2_101 Depth 4
                                        #       Child Loop BB2_104 Depth 3
                                        #         Child Loop BB2_107 Depth 4
                                        #         Child Loop BB2_112 Depth 4
                                        #     Child Loop BB2_116 Depth 2
	callq	clock
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	divss	.LCPI2_3(%rip), %xmm0
	movss	%xmm0, (%rsp)           # 4-byte Spill
	movl	$0, main.i(%rip)
	cmpl	$0, main.ntimes(%rip)
	jle	.LBB2_115
	.p2align	4, 0x90
.LBB2_98:                               # %.lr.ph381
                                        #   Parent Loop BB2_97 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_100 Depth 3
                                        #         Child Loop BB2_101 Depth 4
                                        #       Child Loop BB2_104 Depth 3
                                        #         Child Loop BB2_107 Depth 4
                                        #         Child Loop BB2_112 Depth 4
	movb	main.lda(%rip), %r10b
	movb	main.n(%rip), %r12b
	testb	%r12b, %r12b
	movl	$0, %r9d
	cmovnel	%ebp, %r9d
	je	.LBB2_114
# BB#99:                                # %.preheader53.lr.ph.i238
                                        #   in Loop: Header=BB2_98 Depth=2
	testb	$1, %r10b
	movl	$0, %r15d
	movl	$201, %eax
	cmovneq	%rax, %r15
	movl	%r9d, %r14d
	leaq	(,%r15,4), %r13
	movl	$1325, %edi             # imm = 0x52D
	movl	$main.a+4, %eax
	xorl	%ecx, %ecx
	movsd	.LCPI2_2(%rip), %xmm1   # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB2_100:                              # %.preheader53.us.i241.new
                                        #   Parent Loop BB2_97 Depth=1
                                        #     Parent Loop BB2_98 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_101 Depth 4
	movq	%rax, %rdx
	movq	%r14, %rsi
	.p2align	4, 0x90
.LBB2_101:                              #   Parent Loop BB2_97 Depth=1
                                        #     Parent Loop BB2_98 Depth=2
                                        #       Parent Loop BB2_100 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	imull	$3125, %edi, %ebp       # imm = 0xC35
	movl	%ebp, %edi
	sarl	$31, %edi
	shrl	$16, %edi
	addl	%ebp, %edi
	andl	$-65536, %edi           # imm = 0xFFFF0000
	movl	%ebp, %ebx
	subl	%edi, %ebx
	negl	%edi
	leal	-32768(%rbp,%rdi), %edi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, -4(%rdx)
	imull	$3125, %ebx, %ebx       # imm = 0xC35
	movl	%ebx, %ebp
	sarl	$31, %ebp
	shrl	$16, %ebp
	addl	%ebx, %ebp
	andl	$-65536, %ebp           # imm = 0xFFFF0000
	movl	%ebx, %edi
	subl	%ebp, %edi
	negl	%ebp
	leal	-32768(%rbx,%rbp), %ebp
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rdx)
	addq	$8, %rdx
	addq	$-2, %rsi
	jne	.LBB2_101
# BB#102:                               # %._crit_edge62.us.i250
                                        #   in Loop: Header=BB2_100 Depth=3
	incq	%rcx
	addq	%r13, %rax
	cmpq	%r14, %rcx
	jne	.LBB2_100
# BB#103:                               # %.preheader.us.preheader.i253
                                        #   in Loop: Header=BB2_98 Depth=2
	leal	-1(%r9), %eax
	leaq	4(,%rax,4), %rdx
	movl	$main.b, %edi
	xorl	%esi, %esi
	movq	%r9, %rbp
	movl	%r10d, %ebx
	callq	memset
	movl	%ebx, %r10d
	movq	%rbp, %r9
	movl	%r9d, %r8d
	andl	$4, %r8d
	movq	%r14, %rbp
	subq	%r8, %rbp
	xorl	%edx, %edx
	movl	$main.a+16, %esi
	xorl	%edi, %edi
	movl	$201, %ebx
	.p2align	4, 0x90
.LBB2_104:                              # %.preheader.us.i255
                                        #   Parent Loop BB2_97 Depth=1
                                        #     Parent Loop BB2_98 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_107 Depth 4
                                        #         Child Loop BB2_112 Depth 4
	testb	%r12b, %r12b
	je	.LBB2_110
# BB#105:                               # %min.iters.checked605
                                        #   in Loop: Header=BB2_104 Depth=3
	testq	%rbp, %rbp
	je	.LBB2_110
# BB#106:                               # %vector.body601.preheader
                                        #   in Loop: Header=BB2_104 Depth=3
	movq	%rsi, %rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_107:                              # %vector.body601
                                        #   Parent Loop BB2_97 Depth=1
                                        #     Parent Loop BB2_98 Depth=2
                                        #       Parent Loop BB2_104 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	addps	main.b(,%rax,4), %xmm0
	addps	main.b+16(,%rax,4), %xmm1
	movaps	%xmm0, main.b(,%rax,4)
	movaps	%xmm1, main.b+16(,%rax,4)
	addq	$8, %rax
	addq	$32, %rcx
	cmpq	%rax, %rbp
	jne	.LBB2_107
# BB#108:                               # %middle.block602
                                        #   in Loop: Header=BB2_104 Depth=3
	testl	%r8d, %r8d
	movq	%rbp, %rcx
	jne	.LBB2_111
	jmp	.LBB2_113
	.p2align	4, 0x90
.LBB2_110:                              #   in Loop: Header=BB2_104 Depth=3
	xorl	%ecx, %ecx
.LBB2_111:                              # %scalar.ph603.preheader
                                        #   in Loop: Header=BB2_104 Depth=3
	leaq	(%rcx,%rdx), %rax
	leaq	main.a(,%rax,4), %rax
	.p2align	4, 0x90
.LBB2_112:                              # %scalar.ph603
                                        #   Parent Loop BB2_97 Depth=1
                                        #     Parent Loop BB2_98 Depth=2
                                        #       Parent Loop BB2_104 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movss	main.b(,%rcx,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, main.b(,%rcx,4)
	incq	%rcx
	addq	$4, %rax
	cmpq	%rcx, %r14
	jne	.LBB2_112
.LBB2_113:                              # %._crit_edge.us.i261
                                        #   in Loop: Header=BB2_104 Depth=3
	incq	%rdi
	addq	%r13, %rsi
	addq	%r15, %rdx
	cmpq	%r14, %rdi
	jne	.LBB2_104
.LBB2_114:                              # %matgen.exit262
                                        #   in Loop: Header=BB2_98 Depth=2
	testb	$1, %r10b
	movl	$0, %esi
	cmovnel	%ebx, %esi
	movl	$main.a, %edi
	movl	$main.ipvt, %ecx
	movl	$main.info, %r8d
	movl	%r9d, %edx
	callq	dgefa
	movl	main.i(%rip), %eax
	incl	%eax
	movl	%eax, main.i(%rip)
	cmpl	main.ntimes(%rip), %eax
	movl	$100, %ebp
	jl	.LBB2_98
.LBB2_115:                              # %._crit_edge382
                                        #   in Loop: Header=BB2_97 Depth=1
	callq	clock
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	movss	.LCPI2_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	subss	(%rsp), %xmm0           # 4-byte Folded Reload
	subss	16(%rsp), %xmm0         # 4-byte Folded Reload
	xorps	%xmm1, %xmm1
	cvtsi2ssl	main.ntimes(%rip), %xmm1
	divss	%xmm1, %xmm0
	movslq	main.j(%rip), %rax
	movss	%xmm0, atime.0(,%rax,4)
	callq	clock
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	divss	.LCPI2_3(%rip), %xmm0
	movss	%xmm0, (%rsp)           # 4-byte Spill
	movl	$0, main.i(%rip)
	cmpl	$0, main.ntimes(%rip)
	jle	.LBB2_117
	.p2align	4, 0x90
.LBB2_116:                              # %.lr.ph384
                                        #   Parent Loop BB2_97 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	main.lda(%rip), %eax
	testb	%al, %al
	movl	$0, %esi
	cmovnel	%ebx, %esi
	movzbl	main.n(%rip), %eax
	testb	%al, %al
	movl	$0, %edx
	cmovnel	%ebp, %edx
	movl	$main.a, %edi
	movl	$main.ipvt, %ecx
	movl	$main.b, %r8d
	xorl	%r9d, %r9d
	callq	dgesl
	movl	main.i(%rip), %eax
	incl	%eax
	movl	%eax, main.i(%rip)
	cmpl	main.ntimes(%rip), %eax
	jl	.LBB2_116
.LBB2_117:                              # %._crit_edge385
                                        #   in Loop: Header=BB2_97 Depth=1
	callq	clock
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	movss	.LCPI2_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	subss	(%rsp), %xmm0           # 4-byte Folded Reload
	xorps	%xmm1, %xmm1
	cvtsi2ssl	main.ntimes(%rip), %xmm1
	divss	%xmm1, %xmm0
	movslq	main.j(%rip), %rax
	movss	%xmm0, atime.1(,%rax,4)
	addss	atime.0(,%rax,4), %xmm0
	cvtss2sd	%xmm0, %xmm0
	mulsd	.LCPI2_9(%rip), %xmm0
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, atime.3(,%rax,4)
	addss	atime.3+24(%rip), %xmm0
	movss	%xmm0, atime.3+24(%rip)
	leal	1(%rax), %eax
	movl	%eax, main.j(%rip)
	cmpl	$6, %eax
	jl	.LBB2_97
# BB#118:                               # %.lr.ph374
	divss	.LCPI2_11(%rip), %xmm0
	movss	%xmm0, atime.3+24(%rip)
	movq	stderr(%rip), %rdi
	movl	$.L.str.20, %esi
	xorps	%xmm0, %xmm0
	movb	$1, %al
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.21, %edi
	movl	$30, %esi
	movl	$1, %edx
	callq	fwrite
	callq	clock
	movl	$0, main.i(%rip)
	movb	main.ldaa(%rip), %dl
	movb	main.n(%rip), %r15b
	xorl	%ecx, %ecx
	testb	%r15b, %r15b
	movl	$100, %ebp
	cmovel	%ecx, %ebp
	testb	%dl, %dl
	movl	$200, %r12d
	cmoveq	%rcx, %r12
	testb	%r15b, %r15b
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	divss	.LCPI2_3(%rip), %xmm0
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	je	.LBB2_136
# BB#119:                               # %.lr.ph374.split.us.preheader
	leal	-1(%rbp), %eax
	leaq	4(,%rax,4), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%ebp, %ebx
	andl	$4, %ebx
	movq	%rbp, %r14
	subq	%rbx, %r14
	leaq	(,%r12,4), %r13
	xorl	%eax, %eax
	movsd	.LCPI2_2(%rip), %xmm1   # xmm1 = mem[0],zero
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_120:                              # %.lr.ph374.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_121 Depth 2
                                        #       Child Loop BB2_122 Depth 3
                                        #     Child Loop BB2_125 Depth 2
                                        #       Child Loop BB2_128 Depth 3
                                        #       Child Loop BB2_133 Depth 3
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	$1325, %edi             # imm = 0x52D
	movl	$main.aa+4, %r8d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_121:                              # %.preheader53.us.i293.us.new
                                        #   Parent Loop BB2_120 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_122 Depth 3
	movq	%r8, %rdx
	movq	%rbp, %rsi
	.p2align	4, 0x90
.LBB2_122:                              #   Parent Loop BB2_120 Depth=1
                                        #     Parent Loop BB2_121 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	imull	$3125, %edi, %ebx       # imm = 0xC35
	movl	%ebx, %edi
	sarl	$31, %edi
	shrl	$16, %edi
	addl	%ebx, %edi
	andl	$-65536, %edi           # imm = 0xFFFF0000
	movl	%ebx, %eax
	subl	%edi, %eax
	negl	%edi
	leal	-32768(%rbx,%rdi), %edi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, -4(%rdx)
	imull	$3125, %eax, %ebx       # imm = 0xC35
	movl	%ebx, %eax
	sarl	$31, %eax
	shrl	$16, %eax
	addl	%ebx, %eax
	andl	$-65536, %eax           # imm = 0xFFFF0000
	movl	%ebx, %edi
	subl	%eax, %edi
	negl	%eax
	leal	-32768(%rbx,%rax), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rdx)
	addq	$8, %rdx
	addq	$-2, %rsi
	jne	.LBB2_122
# BB#123:                               # %._crit_edge62.us.i302.us
                                        #   in Loop: Header=BB2_121 Depth=2
	incq	%rcx
	addq	%r13, %r8
	cmpq	%rbp, %rcx
	jne	.LBB2_121
# BB#124:                               # %.preheader52.i303.us
                                        #   in Loop: Header=BB2_120 Depth=1
	movl	$main.b, %edi
	xorl	%esi, %esi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	memset
	xorl	%eax, %eax
	movl	$main.aa+16, %ecx
	xorl	%edx, %edx
	movq	40(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_125:                              # %.preheader.us.i307.us
                                        #   Parent Loop BB2_120 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_128 Depth 3
                                        #       Child Loop BB2_133 Depth 3
	testb	%r15b, %r15b
	je	.LBB2_131
# BB#126:                               # %min.iters.checked626
                                        #   in Loop: Header=BB2_125 Depth=2
	testq	%r14, %r14
	je	.LBB2_131
# BB#127:                               # %vector.body622.preheader
                                        #   in Loop: Header=BB2_125 Depth=2
	movq	%rcx, %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_128:                              # %vector.body622
                                        #   Parent Loop BB2_120 Depth=1
                                        #     Parent Loop BB2_125 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movaps	main.b(,%rdi,4), %xmm0
	movaps	main.b+16(,%rdi,4), %xmm1
	addps	-16(%rsi), %xmm0
	addps	(%rsi), %xmm1
	movaps	%xmm0, main.b(,%rdi,4)
	movaps	%xmm1, main.b+16(,%rdi,4)
	addq	$8, %rdi
	addq	$32, %rsi
	cmpq	%rdi, %r14
	jne	.LBB2_128
# BB#129:                               # %middle.block623
                                        #   in Loop: Header=BB2_125 Depth=2
	testl	%ebx, %ebx
	movq	%r14, %rsi
	jne	.LBB2_132
	jmp	.LBB2_134
	.p2align	4, 0x90
.LBB2_131:                              #   in Loop: Header=BB2_125 Depth=2
	xorl	%esi, %esi
.LBB2_132:                              # %scalar.ph624.preheader
                                        #   in Loop: Header=BB2_125 Depth=2
	leaq	(%rsi,%rax), %rdi
	leaq	main.aa(,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB2_133:                              # %scalar.ph624
                                        #   Parent Loop BB2_120 Depth=1
                                        #     Parent Loop BB2_125 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	main.b(,%rsi,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	(%rdi), %xmm0
	movss	%xmm0, main.b(,%rsi,4)
	incq	%rsi
	addq	$4, %rdi
	cmpq	%rsi, %rbp
	jne	.LBB2_133
.LBB2_134:                              # %._crit_edge.us.i313.us
                                        #   in Loop: Header=BB2_125 Depth=2
	incq	%rdx
	addq	%r13, %rcx
	addq	%r12, %rax
	cmpq	%rbp, %rdx
	jne	.LBB2_125
# BB#135:                               # %matgen.exit314.loopexit.us
                                        #   in Loop: Header=BB2_120 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	incl	%eax
	cmpl	$400, %eax              # imm = 0x190
	movsd	.LCPI2_2(%rip), %xmm1   # xmm1 = mem[0],zero
	jl	.LBB2_120
	jmp	.LBB2_138
	.p2align	4, 0x90
.LBB2_136:                              # %.lr.ph374.split
                                        # =>This Inner Loop Header: Depth=1
	addl	$25, %ecx
	cmpl	$400, %ecx              # imm = 0x190
	jl	.LBB2_136
# BB#137:
	movl	$400, %eax              # imm = 0x190
.LBB2_138:                              # %.loopexit
	movl	%eax, main.i(%rip)
	callq	clock
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	divss	.LCPI2_3(%rip), %xmm0
	subss	32(%rsp), %xmm0         # 4-byte Folded Reload
	divss	.LCPI2_10(%rip), %xmm0
	movss	%xmm0, (%rsp)           # 4-byte Spill
	movq	stderr(%rip), %rdi
	movl	$.L.str.16, %esi
	xorps	%xmm0, %xmm0
	movb	$1, %al
	callq	fprintf
	movq	stderr(%rip), %rdi
	movb	main.ldaa(%rip), %al
	testb	%al, %al
	movl	$200, %ebx
	movl	$0, %edx
	cmovnel	%ebx, %edx
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.12, %edi
	movl	$55, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.13, %edi
	movl	$12, %esi
	movl	$1, %edx
	callq	fwrite
	movl	main.ntimes(%rip), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	mulss	(%rsp), %xmm0           # 4-byte Folded Reload
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movl	$0, atime.3+48(%rip)
	movl	$7, main.j(%rip)
	movl	$100, %ebp
	.p2align	4, 0x90
.LBB2_139:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_140 Depth 2
                                        #       Child Loop BB2_142 Depth 3
                                        #         Child Loop BB2_143 Depth 4
                                        #       Child Loop BB2_146 Depth 3
                                        #         Child Loop BB2_149 Depth 4
                                        #         Child Loop BB2_154 Depth 4
                                        #     Child Loop BB2_158 Depth 2
	callq	clock
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	divss	.LCPI2_3(%rip), %xmm0
	movss	%xmm0, (%rsp)           # 4-byte Spill
	movl	$0, main.i(%rip)
	cmpl	$0, main.ntimes(%rip)
	jle	.LBB2_157
	.p2align	4, 0x90
.LBB2_140:                              # %.lr.ph
                                        #   Parent Loop BB2_139 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_142 Depth 3
                                        #         Child Loop BB2_143 Depth 4
                                        #       Child Loop BB2_146 Depth 3
                                        #         Child Loop BB2_149 Depth 4
                                        #         Child Loop BB2_154 Depth 4
	movb	main.ldaa(%rip), %r10b
	movb	main.n(%rip), %r12b
	testb	%r12b, %r12b
	movl	$0, %r9d
	cmovnel	%ebp, %r9d
	je	.LBB2_156
# BB#141:                               # %.preheader53.lr.ph.i264
                                        #   in Loop: Header=BB2_140 Depth=2
	testb	$1, %r10b
	movl	$0, %r15d
	movl	$200, %eax
	cmovneq	%rax, %r15
	movl	%r9d, %r14d
	leaq	(,%r15,4), %r13
	movl	$1325, %edi             # imm = 0x52D
	movl	$main.aa+4, %eax
	xorl	%ecx, %ecx
	movsd	.LCPI2_2(%rip), %xmm1   # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB2_142:                              # %.preheader53.us.i267.new
                                        #   Parent Loop BB2_139 Depth=1
                                        #     Parent Loop BB2_140 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_143 Depth 4
	movq	%rax, %rdx
	movq	%r14, %rsi
	.p2align	4, 0x90
.LBB2_143:                              #   Parent Loop BB2_139 Depth=1
                                        #     Parent Loop BB2_140 Depth=2
                                        #       Parent Loop BB2_142 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	imull	$3125, %edi, %ebp       # imm = 0xC35
	movl	%ebp, %edi
	sarl	$31, %edi
	shrl	$16, %edi
	addl	%ebp, %edi
	andl	$-65536, %edi           # imm = 0xFFFF0000
	movl	%ebp, %ebx
	subl	%edi, %ebx
	negl	%edi
	leal	-32768(%rbp,%rdi), %edi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, -4(%rdx)
	imull	$3125, %ebx, %ebx       # imm = 0xC35
	movl	%ebx, %ebp
	sarl	$31, %ebp
	shrl	$16, %ebp
	addl	%ebx, %ebp
	andl	$-65536, %ebp           # imm = 0xFFFF0000
	movl	%ebx, %edi
	subl	%ebp, %edi
	negl	%ebp
	leal	-32768(%rbx,%rbp), %ebp
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rdx)
	addq	$8, %rdx
	addq	$-2, %rsi
	jne	.LBB2_143
# BB#144:                               # %._crit_edge62.us.i276
                                        #   in Loop: Header=BB2_142 Depth=3
	incq	%rcx
	addq	%r13, %rax
	cmpq	%r14, %rcx
	jne	.LBB2_142
# BB#145:                               # %.preheader.us.preheader.i279
                                        #   in Loop: Header=BB2_140 Depth=2
	leal	-1(%r9), %eax
	leaq	4(,%rax,4), %rdx
	movl	$main.b, %edi
	xorl	%esi, %esi
	movq	%r9, %rbp
	movl	%r10d, %ebx
	callq	memset
	movl	%ebx, %r10d
	movq	%rbp, %r9
	movl	%r9d, %r8d
	andl	$4, %r8d
	movq	%r14, %rbp
	subq	%r8, %rbp
	xorl	%edx, %edx
	movl	$main.aa+16, %esi
	xorl	%edi, %edi
	movl	$200, %ebx
	.p2align	4, 0x90
.LBB2_146:                              # %.preheader.us.i281
                                        #   Parent Loop BB2_139 Depth=1
                                        #     Parent Loop BB2_140 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_149 Depth 4
                                        #         Child Loop BB2_154 Depth 4
	testb	%r12b, %r12b
	je	.LBB2_152
# BB#147:                               # %min.iters.checked647
                                        #   in Loop: Header=BB2_146 Depth=3
	testq	%rbp, %rbp
	je	.LBB2_152
# BB#148:                               # %vector.body643.preheader
                                        #   in Loop: Header=BB2_146 Depth=3
	movq	%rsi, %rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_149:                              # %vector.body643
                                        #   Parent Loop BB2_139 Depth=1
                                        #     Parent Loop BB2_140 Depth=2
                                        #       Parent Loop BB2_146 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movaps	main.b(,%rax,4), %xmm0
	movaps	main.b+16(,%rax,4), %xmm1
	addps	-16(%rcx), %xmm0
	addps	(%rcx), %xmm1
	movaps	%xmm0, main.b(,%rax,4)
	movaps	%xmm1, main.b+16(,%rax,4)
	addq	$8, %rax
	addq	$32, %rcx
	cmpq	%rax, %rbp
	jne	.LBB2_149
# BB#150:                               # %middle.block644
                                        #   in Loop: Header=BB2_146 Depth=3
	testl	%r8d, %r8d
	movq	%rbp, %rcx
	jne	.LBB2_153
	jmp	.LBB2_155
	.p2align	4, 0x90
.LBB2_152:                              #   in Loop: Header=BB2_146 Depth=3
	xorl	%ecx, %ecx
.LBB2_153:                              # %scalar.ph645.preheader
                                        #   in Loop: Header=BB2_146 Depth=3
	leaq	(%rcx,%rdx), %rax
	leaq	main.aa(,%rax,4), %rax
	.p2align	4, 0x90
.LBB2_154:                              # %scalar.ph645
                                        #   Parent Loop BB2_139 Depth=1
                                        #     Parent Loop BB2_140 Depth=2
                                        #       Parent Loop BB2_146 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movss	main.b(,%rcx,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, main.b(,%rcx,4)
	incq	%rcx
	addq	$4, %rax
	cmpq	%rcx, %r14
	jne	.LBB2_154
.LBB2_155:                              # %._crit_edge.us.i287
                                        #   in Loop: Header=BB2_146 Depth=3
	incq	%rdi
	addq	%r13, %rsi
	addq	%r15, %rdx
	cmpq	%r14, %rdi
	jne	.LBB2_146
.LBB2_156:                              # %matgen.exit288
                                        #   in Loop: Header=BB2_140 Depth=2
	testb	$1, %r10b
	movl	$0, %esi
	cmovnel	%ebx, %esi
	movl	$main.aa, %edi
	movl	$main.ipvt, %ecx
	movl	$main.info, %r8d
	movl	%r9d, %edx
	callq	dgefa
	movl	main.i(%rip), %eax
	incl	%eax
	movl	%eax, main.i(%rip)
	cmpl	main.ntimes(%rip), %eax
	movl	$100, %ebp
	jl	.LBB2_140
.LBB2_157:                              # %._crit_edge
                                        #   in Loop: Header=BB2_139 Depth=1
	callq	clock
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	movss	.LCPI2_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	subss	(%rsp), %xmm0           # 4-byte Folded Reload
	subss	16(%rsp), %xmm0         # 4-byte Folded Reload
	xorps	%xmm1, %xmm1
	cvtsi2ssl	main.ntimes(%rip), %xmm1
	divss	%xmm1, %xmm0
	movslq	main.j(%rip), %rax
	movss	%xmm0, atime.0(,%rax,4)
	callq	clock
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	divss	.LCPI2_3(%rip), %xmm0
	movss	%xmm0, (%rsp)           # 4-byte Spill
	movl	$0, main.i(%rip)
	cmpl	$0, main.ntimes(%rip)
	jle	.LBB2_159
	.p2align	4, 0x90
.LBB2_158:                              # %.lr.ph368
                                        #   Parent Loop BB2_139 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	main.ldaa(%rip), %eax
	testb	%al, %al
	movl	$0, %esi
	cmovnel	%ebx, %esi
	movzbl	main.n(%rip), %eax
	testb	%al, %al
	movl	$0, %edx
	cmovnel	%ebp, %edx
	movl	$main.aa, %edi
	movl	$main.ipvt, %ecx
	movl	$main.b, %r8d
	xorl	%r9d, %r9d
	callq	dgesl
	movl	main.i(%rip), %eax
	incl	%eax
	movl	%eax, main.i(%rip)
	cmpl	main.ntimes(%rip), %eax
	jl	.LBB2_158
.LBB2_159:                              # %._crit_edge369
                                        #   in Loop: Header=BB2_139 Depth=1
	callq	clock
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	movss	.LCPI2_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	subss	(%rsp), %xmm0           # 4-byte Folded Reload
	xorps	%xmm1, %xmm1
	cvtsi2ssl	main.ntimes(%rip), %xmm1
	divss	%xmm1, %xmm0
	movslq	main.j(%rip), %rax
	movss	%xmm0, atime.1(,%rax,4)
	addss	atime.0(,%rax,4), %xmm0
	cvtss2sd	%xmm0, %xmm0
	mulsd	.LCPI2_9(%rip), %xmm0
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, atime.3(,%rax,4)
	addss	atime.3+48(%rip), %xmm0
	movss	%xmm0, atime.3+48(%rip)
	leal	1(%rax), %eax
	movl	%eax, main.j(%rip)
	cmpl	$12, %eax
	jl	.LBB2_139
# BB#160:
	divss	.LCPI2_11(%rip), %xmm0
	movss	%xmm0, atime.3+48(%rip)
	movq	stderr(%rip), %rdi
	movl	$.L.str.20, %esi
	xorps	%xmm0, %xmm0
	movb	$1, %al
	callq	fprintf
	xorl	%eax, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4544132024016830464     # double 6.103515625E-5
	.text
	.globl	matgen
	.p2align	4, 0x90
	.type	matgen,@function
matgen:                                 # @matgen
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 112
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, %r15
	movl	$0, (%r8)
	testl	%edx, %edx
	jle	.LBB3_24
# BB#1:                                 # %.preheader53.lr.ph
	movslq	%esi, %rax
	movq	%rdx, %r9
	movl	%edx, %r12d
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	(,%rax,4), %r10
	movl	$1325, %edx             # imm = 0x52D
	xorl	%eax, %eax
	movsd	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB3_2:                                # %.preheader53.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_3 Depth 2
	movq	%rcx, %rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_3:                                #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imull	$3125, %edx, %ebp       # imm = 0xC35
	movl	%ebp, %ebx
	sarl	$31, %ebx
	shrl	$16, %ebx
	addl	%ebp, %ebx
	andl	$-65536, %ebx           # imm = 0xFFFF0000
	movl	%ebp, %edx
	subl	%ebx, %edx
	negl	%ebx
	leal	-32768(%rbp,%rbx), %ebp
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebp, %xmm1
	mulsd	%xmm0, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, (%rdi)
	ucomiss	(%r8), %xmm1
	movq	%r8, %rbp
	cmovaq	%rdi, %rbp
	movl	(%rbp), %ebp
	movl	%ebp, (%r8)
	incq	%rsi
	addq	$4, %rdi
	cmpq	%rsi, %r12
	jne	.LBB3_3
# BB#4:                                 # %._crit_edge62.us
                                        #   in Loop: Header=BB3_2 Depth=1
	incq	%rax
	addq	%r10, %rcx
	cmpq	%rsi, %rax
	jne	.LBB3_2
# BB#5:                                 # %.preheader52
	movq	%r9, 16(%rsp)           # 8-byte Spill
	testl	%r9d, %r9d
	jle	.LBB3_24
# BB#6:                                 # %.preheader.us.preheader
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rbx
	leal	-1(%rbx), %eax
	leaq	4(,%rax,4), %rdx
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%r10, 8(%rsp)           # 8-byte Spill
	callq	memset
	movq	8(%rsp), %rbp           # 8-byte Reload
	leaq	(%r13,%r12,4), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	-1(%r12), %r9
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	andl	$7, %ebx
	movq	%r12, %r8
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	subq	%rbx, %r8
	leaq	16(%r13), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	16(%r15), %rdi
	leaq	12(%r13), %r11
	movl	$2, %edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_7:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_15 Depth 2
                                        #     Child Loop BB3_19 Depth 2
                                        #     Child Loop BB3_22 Depth 2
	cmpl	$8, 16(%rsp)            # 4-byte Folded Reload
	jae	.LBB3_9
# BB#8:                                 #   in Loop: Header=BB3_7 Depth=1
	xorl	%esi, %esi
	jmp	.LBB3_17
	.p2align	4, 0x90
.LBB3_9:                                # %min.iters.checked
                                        #   in Loop: Header=BB3_7 Depth=1
	testq	%r8, %r8
	je	.LBB3_13
# BB#10:                                # %vector.memcheck
                                        #   in Loop: Header=BB3_7 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	imulq	%rax, %rcx
	leaq	(%r12,%rcx), %rsi
	leaq	(%r15,%rsi,4), %rsi
	cmpq	%r13, %rsi
	jbe	.LBB3_14
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB3_7 Depth=1
	leaq	(%r15,%rcx,4), %rcx
	cmpq	48(%rsp), %rcx          # 8-byte Folded Reload
	jae	.LBB3_14
.LBB3_13:                               #   in Loop: Header=BB3_7 Depth=1
	xorl	%esi, %esi
	jmp	.LBB3_17
.LBB3_14:                               # %vector.body.preheader
                                        #   in Loop: Header=BB3_7 Depth=1
	movq	%r8, %rcx
	movq	%rdi, %rsi
	movq	32(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_15:                               # %vector.body
                                        #   Parent Loop BB3_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	-16(%rsi), %xmm2
	movups	(%rsi), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm1, %xmm3
	movups	%xmm2, -16(%rbx)
	movups	%xmm3, (%rbx)
	addq	$32, %rbx
	addq	$32, %rsi
	addq	$-8, %rcx
	jne	.LBB3_15
# BB#16:                                # %middle.block
                                        #   in Loop: Header=BB3_7 Depth=1
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	movq	%r8, %rsi
	je	.LBB3_23
	.p2align	4, 0x90
.LBB3_17:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB3_7 Depth=1
	movq	%r15, %r10
	movl	%r12d, %r15d
	subl	%esi, %r15d
	movq	%r9, %rbx
	subq	%rsi, %rbx
	andq	$3, %r15
	je	.LBB3_20
# BB#18:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB3_7 Depth=1
	leaq	(%rsi,%r14), %rcx
	leaq	(%r10,%rcx,4), %rcx
	negq	%r15
	.p2align	4, 0x90
.LBB3_19:                               # %scalar.ph.prol
                                        #   Parent Loop BB3_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%r13,%rsi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%rcx), %xmm0
	movss	%xmm0, (%r13,%rsi,4)
	incq	%rsi
	addq	$4, %rcx
	incq	%r15
	jne	.LBB3_19
.LBB3_20:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB3_7 Depth=1
	cmpq	$3, %rbx
	movq	%r10, %r15
	movq	8(%rsp), %rbp           # 8-byte Reload
	jb	.LBB3_23
# BB#21:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB3_7 Depth=1
	movq	%r12, %rbx
	subq	%rsi, %rbx
	leaq	(%rsi,%rdx), %rcx
	leaq	(%r15,%rcx,4), %rcx
	leaq	(%r11,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB3_22:                               # %scalar.ph
                                        #   Parent Loop BB3_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	-12(%rsi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	addss	-8(%rcx), %xmm0
	movss	%xmm0, -12(%rsi)
	movss	-8(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rcx), %xmm0
	movss	%xmm0, -8(%rsi)
	movss	-4(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	(%rcx), %xmm0
	movss	%xmm0, -4(%rsi)
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	4(%rcx), %xmm0
	movss	%xmm0, (%rsi)
	addq	$16, %rcx
	addq	$16, %rsi
	addq	$-4, %rbx
	jne	.LBB3_22
.LBB3_23:                               # %._crit_edge.us
                                        #   in Loop: Header=BB3_7 Depth=1
	incq	%rax
	addq	%rbp, %rdi
	movq	24(%rsp), %rcx          # 8-byte Reload
	addq	%rcx, %r14
	addq	%rcx, %rdx
	cmpq	%r12, %rax
	jne	.LBB3_7
.LBB3_24:                               # %._crit_edge56
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	matgen, .Lfunc_end3-matgen
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_1:
	.long	3212836864              # float -1
	.text
	.globl	dgefa
	.p2align	4, 0x90
	.type	dgefa,@function
dgefa:                                  # @dgefa
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$144, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 200
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rcx, 40(%rsp)          # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%esi, -124(%rsp)        # 4-byte Spill
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movl	$0, (%r8)
	leal	-1(%rdx), %eax
	movl	%eax, -68(%rsp)         # 4-byte Spill
	cmpl	$2, %edx
	jl	.LBB4_71
# BB#1:                                 # %.lr.ph127.preheader
	movslq	-124(%rsp), %rax        # 4-byte Folded Reload
	movslq	%edx, %r15
	movl	%edx, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	-68(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, %esi
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leal	1(%rax), %ebp
	movq	%rbp, -24(%rsp)         # 8-byte Spill
	addb	$3, %dl
	leaq	12(%rdi), %rax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	leaq	16(%rdi), %rax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	xorl	%r12d, %r12d
	movl	$1, %eax
	movl	$1, %ebx
	movq	%rbx, -40(%rsp)         # 8-byte Spill
	xorps	%xmm0, %xmm0
	movss	.LCPI4_1(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
	movaps	.LCPI4_0(%rip), %xmm2   # xmm2 = [nan,nan,nan,nan]
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movq	%rsi, -8(%rsp)          # 8-byte Spill
	movl	%ebp, %esi
	movl	%esi, -116(%rsp)        # 4-byte Spill
	movl	%ecx, %r13d
	xorl	%esi, %esi
	xorl	%ebp, %ebp
	movq	%r15, 96(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph127
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_6 Depth 2
                                        #     Child Loop BB4_11 Depth 2
                                        #     Child Loop BB4_22 Depth 2
                                        #     Child Loop BB4_26 Depth 2
                                        #     Child Loop BB4_51 Depth 2
                                        #       Child Loop BB4_61 Depth 3
                                        #       Child Loop BB4_65 Depth 3
                                        #       Child Loop BB4_68 Depth 3
                                        #     Child Loop BB4_30 Depth 2
                                        #       Child Loop BB4_40 Depth 3
                                        #       Child Loop BB4_44 Depth 3
                                        #       Child Loop BB4_47 Depth 3
	movq	%rsi, -112(%rsp)        # 8-byte Spill
	movl	%ecx, %ecx
	movq	%rcx, -88(%rsp)         # 8-byte Spill
	movslq	%eax, %r10
	movq	%rsi, %r11
	imulq	16(%rsp), %r11          # 8-byte Folded Reload
	movl	%ebp, %ecx
	imull	-124(%rsp), %ecx        # 4-byte Folded Reload
	addl	%esi, %ecx
	movq	%r15, %rax
	subq	%rsi, %rax
	movslq	%ecx, %r9
	movq	%r10, -56(%rsp)         # 8-byte Spill
	movl	%ebp, 28(%rsp)          # 4-byte Spill
	jle	.LBB4_7
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	xorl	%r14d, %r14d
	cmpl	$1, %eax
	je	.LBB4_12
# BB#4:                                 #   in Loop: Header=BB4_2 Depth=1
	movslq	%r12d, %r8
	movq	8(%rsp), %rcx           # 8-byte Reload
	subq	-112(%rsp), %rcx        # 8-byte Folded Reload
	movl	%ecx, %r10d
	addq	$3, %rcx
	addq	$-2, %r10
	movss	(%rdi,%r9,4), %xmm3     # xmm3 = mem[0],zero,zero,zero
	andps	%xmm2, %xmm3
	testb	$3, %cl
	je	.LBB4_8
# BB#5:                                 # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB4_2 Depth=1
	leaq	(%rdi,%r8,4), %rcx
	movl	%edx, %ebx
	andb	$3, %bl
	movzbl	%bl, %ebp
	negq	%rbp
	xorl	%r14d, %r14d
	movl	$1, %ebx
	movaps	%xmm3, %xmm4
	.p2align	4, 0x90
.LBB4_6:                                # %.lr.ph.i.prol
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rcx,%rbx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	andps	%xmm2, %xmm3
	ucomiss	%xmm4, %xmm3
	maxss	%xmm4, %xmm3
	cmoval	%ebx, %r14d
	leaq	1(%rbp,%rbx), %rsi
	incq	%rbx
	cmpq	$1, %rsi
	movaps	%xmm3, %xmm4
	jne	.LBB4_6
	jmp	.LBB4_9
	.p2align	4, 0x90
.LBB4_7:                                #   in Loop: Header=BB4_2 Depth=1
	movl	$-1, %r14d
	jmp	.LBB4_12
.LBB4_8:                                #   in Loop: Header=BB4_2 Depth=1
	xorl	%r14d, %r14d
	movl	$1, %ebx
.LBB4_9:                                # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB4_2 Depth=1
	cmpq	$3, %r10
	movq	-56(%rsp), %r10         # 8-byte Reload
	jb	.LBB4_12
# BB#10:                                # %.new
                                        #   in Loop: Header=BB4_2 Depth=1
	movl	%eax, %eax
	movq	-96(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r8,4), %rcx
	.p2align	4, 0x90
.LBB4_11:                               # %.lr.ph.i
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	-12(%rcx,%rbx,4), %xmm4 # xmm4 = mem[0],zero,zero,zero
	andps	%xmm2, %xmm4
	ucomiss	%xmm3, %xmm4
	maxss	%xmm3, %xmm4
	cmoval	%ebx, %r14d
	movss	-8(%rcx,%rbx,4), %xmm3  # xmm3 = mem[0],zero,zero,zero
	andps	%xmm2, %xmm3
	leal	1(%rbx), %esi
	ucomiss	%xmm4, %xmm3
	maxss	%xmm4, %xmm3
	cmovbel	%r14d, %esi
	movss	-4(%rcx,%rbx,4), %xmm4  # xmm4 = mem[0],zero,zero,zero
	andps	%xmm2, %xmm4
	leal	2(%rbx), %ebp
	ucomiss	%xmm3, %xmm4
	maxss	%xmm3, %xmm4
	cmovbel	%esi, %ebp
	movss	(%rcx,%rbx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	andps	%xmm2, %xmm3
	leal	3(%rbx), %r14d
	ucomiss	%xmm4, %xmm3
	maxss	%xmm4, %xmm3
	cmovbel	%ebp, %r14d
	addq	$4, %rbx
	cmpq	%rax, %rbx
	jne	.LBB4_11
	.p2align	4, 0x90
.LBB4_12:                               # %idamax.exit
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	-112(%rsp), %rcx        # 8-byte Reload
	leaq	1(%rcx), %rbp
	leal	(%r14,%rcx), %ebx
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%ebx, (%rax,%rcx,4)
	addl	%ebx, %r11d
	movslq	%r11d, %rax
	movss	(%rdi,%rax,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm4
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	movq	%r12, 128(%rsp)         # 8-byte Spill
	movb	%r13b, -117(%rsp)       # 1-byte Spill
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	jne	.LBB4_13
	jnp	.LBB4_15
.LBB4_13:                               #   in Loop: Header=BB4_2 Depth=1
	movq	64(%rsp), %rsi          # 8-byte Reload
	subq	%rcx, %rsi
	movl	%r13d, %ecx
	andb	$7, %cl
	movzbl	%cl, %ecx
	movq	%rsi, -80(%rsp)         # 8-byte Spill
	movl	%esi, %r8d
	testl	%r14d, %r14d
	je	.LBB4_16
# BB#14:                                #   in Loop: Header=BB4_2 Depth=1
	movl	(%rdi,%r9,4), %esi
	movl	%esi, (%rdi,%rax,4)
	movss	%xmm4, (%rdi,%r9,4)
	jmp	.LBB4_17
	.p2align	4, 0x90
.LBB4_15:                               #   in Loop: Header=BB4_2 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%ecx, (%rax)
	jmp	.LBB4_70
	.p2align	4, 0x90
.LBB4_16:                               # %._crit_edge
                                        #   in Loop: Header=BB4_2 Depth=1
	movss	(%rdi,%r9,4), %xmm4     # xmm4 = mem[0],zero,zero,zero
.LBB4_17:                               #   in Loop: Header=BB4_2 Depth=1
	movq	-88(%rsp), %rax         # 8-byte Reload
	subq	%rcx, %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	movq	%r15, %r12
	subq	%rbp, %r12
	jle	.LBB4_27
# BB#18:                                #   in Loop: Header=BB4_2 Depth=1
	movaps	%xmm8, %xmm3
	divss	%xmm4, %xmm3
	cmpq	$8, %r8
	jb	.LBB4_24
# BB#20:                                # %min.iters.checked203
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	-80(%rsp), %rdx         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	andl	$7, %edx
	movq	%r8, %rax
	subq	%rdx, %rax
	je	.LBB4_24
# BB#21:                                # %vector.ph207
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	-104(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%r10,4), %rcx
	movaps	%xmm3, %xmm4
	shufps	$0, %xmm4, %xmm4        # xmm4 = xmm4[0,0,0,0]
	movq	-32(%rsp), %rsi         # 8-byte Reload
	.p2align	4, 0x90
.LBB4_22:                               # %vector.body199
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rcx), %xmm5
	movups	(%rcx), %xmm6
	mulps	%xmm4, %xmm5
	mulps	%xmm4, %xmm6
	movups	%xmm5, -16(%rcx)
	movups	%xmm6, (%rcx)
	addq	$32, %rcx
	addq	$-8, %rsi
	jne	.LBB4_22
# BB#23:                                # %middle.block200
                                        #   in Loop: Header=BB4_2 Depth=1
	testq	%rdx, %rdx
	jne	.LBB4_25
	jmp	.LBB4_27
	.p2align	4, 0x90
.LBB4_24:                               #   in Loop: Header=BB4_2 Depth=1
	xorl	%eax, %eax
.LBB4_25:                               # %.lr.ph.i111.preheader
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	-88(%rsp), %rcx         # 8-byte Reload
	subq	%rax, %rcx
	addq	%r10, %rax
	leaq	(%rdi,%rax,4), %rax
	.p2align	4, 0x90
.LBB4_26:                               # %.lr.ph.i111
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	movss	%xmm4, (%rax)
	addq	$4, %rax
	decq	%rcx
	jne	.LBB4_26
.LBB4_27:                               # %dscal.exit.preheader
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	%r8, -48(%rsp)          # 8-byte Spill
	cmpq	%rbp, %r15
	jle	.LBB4_70
# BB#28:                                # %.lr.ph
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	-24(%rsp), %r8          # 8-byte Reload
	movq	%r8, %rbp
	imulq	-112(%rsp), %rbp        # 8-byte Folded Reload
	movq	%rbp, %rcx
	shlq	$32, %rcx
	movabsq	$4294967296, %rax       # imm = 0x100000000
	addq	%rax, %rcx
	movq	%rcx, %rax
	sarq	$32, %rax
	movq	-56(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdi,%rdx,4), %r15
	sarq	$30, %rcx
	movq	-48(%rsp), %rsi         # 8-byte Reload
	addq	%rsi, %rax
	addq	%rdi, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leaq	(%rdi,%rax,4), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	testl	%r14d, %r14d
	je	.LBB4_49
# BB#29:                                # %.lr.ph.split.us.preheader
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	-104(%rsp), %rax        # 8-byte Reload
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	addl	%r8d, %ebp
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	movslq	%ebx, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	(%rdi,%rsi,4), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	-1(%rsi), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movq	-80(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$7, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	subq	%rax, %rsi
	movq	%rsi, -64(%rsp)         # 8-byte Spill
	movq	-96(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	xorl	%r10d, %r10d
	movl	-116(%rsp), %r14d       # 4-byte Reload
	movq	-40(%rsp), %r9          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_30:                               # %.lr.ph.split.us
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_40 Depth 3
                                        #       Child Loop BB4_44 Depth 3
                                        #       Child Loop BB4_47 Depth 3
	testq	%r12, %r12
	setg	%al
	movq	%r9, %rbx
	imulq	16(%rsp), %rbx          # 8-byte Folded Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	(%rbx,%rcx), %rsi
	movss	(%rdi,%rsi,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	addq	-112(%rsp), %rbx        # 8-byte Folded Reload
	ucomiss	%xmm3, %xmm0
	setp	%dl
	setne	%cl
	testq	%r12, %r12
	movl	(%rdi,%rbx,4), %ebp
	movl	%ebp, (%rdi,%rsi,4)
	movss	%xmm3, (%rdi,%rbx,4)
	jle	.LBB4_48
# BB#31:                                # %.lr.ph.split.us
                                        #   in Loop: Header=BB4_30 Depth=2
	orb	%dl, %cl
	andb	%cl, %al
	je	.LBB4_48
# BB#32:                                # %.lr.ph53.i.us.preheader
                                        #   in Loop: Header=BB4_30 Depth=2
	movslq	%r14d, %r11
	cmpq	$8, -48(%rsp)           # 8-byte Folded Reload
	jae	.LBB4_34
# BB#33:                                #   in Loop: Header=BB4_30 Depth=2
	xorl	%eax, %eax
	jmp	.LBB4_42
	.p2align	4, 0x90
.LBB4_34:                               # %min.iters.checked
                                        #   in Loop: Header=BB4_30 Depth=2
	cmpq	$0, -64(%rsp)           # 8-byte Folded Reload
	je	.LBB4_38
# BB#35:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_30 Depth=2
	movl	%r10d, %eax
	imull	-124(%rsp), %eax        # 4-byte Folded Reload
	addl	104(%rsp), %eax         # 4-byte Folded Reload
	cltq
	leaq	(%rdi,%rax,4), %rcx
	cmpq	56(%rsp), %rcx          # 8-byte Folded Reload
	jae	.LBB4_39
# BB#36:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_30 Depth=2
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rax
	cmpq	%rax, 48(%rsp)          # 8-byte Folded Reload
	jae	.LBB4_39
.LBB4_38:                               #   in Loop: Header=BB4_30 Depth=2
	xorl	%eax, %eax
	jmp	.LBB4_42
.LBB4_39:                               # %vector.ph
                                        #   in Loop: Header=BB4_30 Depth=2
	movq	-104(%rsp), %rax        # 8-byte Reload
	leaq	(%rax,%r11,4), %rax
	movaps	%xmm3, %xmm4
	shufps	$0, %xmm4, %xmm4        # xmm4 = xmm4[0,0,0,0]
	movq	-32(%rsp), %rcx         # 8-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_40:                               # %vector.body
                                        #   Parent Loop BB4_2 Depth=1
                                        #     Parent Loop BB4_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-16(%rax), %xmm5
	movups	(%rax), %xmm6
	movups	-16(%rdx), %xmm7
	movups	(%rdx), %xmm1
	mulps	%xmm4, %xmm7
	mulps	%xmm4, %xmm1
	addps	%xmm5, %xmm7
	addps	%xmm6, %xmm1
	movups	%xmm7, -16(%rax)
	movups	%xmm1, (%rax)
	addq	$32, %rax
	addq	$32, %rdx
	addq	$-8, %rcx
	jne	.LBB4_40
# BB#41:                                # %middle.block
                                        #   in Loop: Header=BB4_30 Depth=2
	cmpq	$0, 72(%rsp)            # 8-byte Folded Reload
	movq	-64(%rsp), %rax         # 8-byte Reload
	je	.LBB4_48
	.p2align	4, 0x90
.LBB4_42:                               # %.lr.ph53.i.us.preheader222
                                        #   in Loop: Header=BB4_30 Depth=2
	movq	-80(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	subl	%eax, %ecx
	movq	-16(%rsp), %rdx         # 8-byte Reload
	subq	%rax, %rdx
	testb	$3, %cl
	je	.LBB4_45
# BB#43:                                # %.lr.ph53.i.us.prol.preheader
                                        #   in Loop: Header=BB4_30 Depth=2
	leaq	(%rdi,%r11,4), %rcx
	movq	-8(%rsp), %rbx          # 8-byte Reload
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	subl	%eax, %ebx
	andl	$3, %ebx
	negq	%rbx
	.p2align	4, 0x90
.LBB4_44:                               # %.lr.ph53.i.us.prol
                                        #   Parent Loop BB4_2 Depth=1
                                        #     Parent Loop BB4_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	(%r15,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	(%rcx,%rax,4), %xmm1
	movss	%xmm1, (%rcx,%rax,4)
	incq	%rax
	incq	%rbx
	jne	.LBB4_44
.LBB4_45:                               # %.lr.ph53.i.us.prol.loopexit
                                        #   in Loop: Header=BB4_30 Depth=2
	cmpq	$3, %rdx
	jb	.LBB4_48
# BB#46:                                # %.lr.ph53.i.us.preheader222.new
                                        #   in Loop: Header=BB4_30 Depth=2
	movq	-88(%rsp), %r8          # 8-byte Reload
	subq	%rax, %r8
	movq	112(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,4), %r13
	addq	%rax, %r11
	movq	-96(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11,4), %rax
	.p2align	4, 0x90
.LBB4_47:                               # %.lr.ph53.i.us
                                        #   Parent Loop BB4_2 Depth=1
                                        #     Parent Loop BB4_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	-12(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	-12(%rax), %xmm1
	movss	%xmm1, -12(%rax)
	movss	-8(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	-8(%rax), %xmm1
	movss	%xmm1, -8(%rax)
	movss	-4(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	-4(%rax), %xmm1
	movss	%xmm1, -4(%rax)
	movss	(%r13), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	(%rax), %xmm1
	movss	%xmm1, (%rax)
	addq	$16, %r13
	addq	$16, %rax
	addq	$-4, %r8
	jne	.LBB4_47
.LBB4_48:                               # %daxpy.exit.us
                                        #   in Loop: Header=BB4_30 Depth=2
	incq	%r9
	incl	%r10d
	addl	-124(%rsp), %r14d       # 4-byte Folded Reload
	cmpq	8(%rsp), %r9            # 8-byte Folded Reload
	jne	.LBB4_30
	jmp	.LBB4_70
.LBB4_49:                               # %.lr.ph.split
                                        #   in Loop: Header=BB4_2 Depth=1
	testq	%r12, %r12
	jle	.LBB4_70
# BB#50:                                # %.lr.ph.split.split.us.preheader
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	-104(%rsp), %rax        # 8-byte Reload
	movq	-56(%rsp), %rcx         # 8-byte Reload
	leaq	(%rax,%rcx,4), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	addl	-24(%rsp), %ebp         # 4-byte Folded Reload
	movslq	%ebx, %r13
	movq	-48(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdi,%rdx,4), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	-1(%rdx), %r11
	movq	-80(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$7, %eax
	movq	%rdx, %rsi
	movq	%rax, -64(%rsp)         # 8-byte Spill
	subq	%rax, %rsi
	movq	-96(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,4), %rax
	movq	%rax, -112(%rsp)        # 8-byte Spill
	xorl	%r8d, %r8d
	movl	-116(%rsp), %r14d       # 4-byte Reload
	movq	-40(%rsp), %r9          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_51:                               # %.lr.ph.split.split.us
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_61 Depth 3
                                        #       Child Loop BB4_65 Depth 3
                                        #       Child Loop BB4_68 Depth 3
	testq	%r12, %r12
	jle	.LBB4_69
# BB#52:                                # %.lr.ph.split.split.us
                                        #   in Loop: Header=BB4_51 Depth=2
	movq	%r9, %rax
	imulq	16(%rsp), %rax          # 8-byte Folded Reload
	addq	%r13, %rax
	movss	(%rdi,%rax,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm3
	jne	.LBB4_53
	jnp	.LBB4_69
.LBB4_53:                               # %.lr.ph53.i.us120.preheader
                                        #   in Loop: Header=BB4_51 Depth=2
	movslq	%r14d, %r10
	cmpq	$8, -48(%rsp)           # 8-byte Folded Reload
	jae	.LBB4_55
# BB#54:                                #   in Loop: Header=BB4_51 Depth=2
	xorl	%eax, %eax
	jmp	.LBB4_63
	.p2align	4, 0x90
.LBB4_55:                               # %min.iters.checked163
                                        #   in Loop: Header=BB4_51 Depth=2
	testq	%rsi, %rsi
	je	.LBB4_59
# BB#56:                                # %vector.memcheck183
                                        #   in Loop: Header=BB4_51 Depth=2
	movl	%r8d, %eax
	imull	-124(%rsp), %eax        # 4-byte Folded Reload
	addl	%ebp, %eax
	cltq
	leaq	(%rdi,%rax,4), %rcx
	cmpq	56(%rsp), %rcx          # 8-byte Folded Reload
	jae	.LBB4_60
# BB#57:                                # %vector.memcheck183
                                        #   in Loop: Header=BB4_51 Depth=2
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	(%rcx,%rax,4), %rax
	cmpq	%rax, 48(%rsp)          # 8-byte Folded Reload
	jae	.LBB4_60
.LBB4_59:                               #   in Loop: Header=BB4_51 Depth=2
	xorl	%eax, %eax
	jmp	.LBB4_63
.LBB4_60:                               # %vector.ph184
                                        #   in Loop: Header=BB4_51 Depth=2
	movq	-104(%rsp), %rax        # 8-byte Reload
	leaq	(%rax,%r10,4), %rax
	movaps	%xmm3, %xmm4
	shufps	$0, %xmm4, %xmm4        # xmm4 = xmm4[0,0,0,0]
	movq	-32(%rsp), %rdx         # 8-byte Reload
	movq	-16(%rsp), %rbx         # 8-byte Reload
	.p2align	4, 0x90
.LBB4_61:                               # %vector.body159
                                        #   Parent Loop BB4_2 Depth=1
                                        #     Parent Loop BB4_51 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-16(%rax), %xmm1
	movups	(%rax), %xmm5
	movups	-16(%rbx), %xmm6
	movups	(%rbx), %xmm7
	mulps	%xmm4, %xmm6
	mulps	%xmm4, %xmm7
	addps	%xmm1, %xmm6
	addps	%xmm5, %xmm7
	movups	%xmm6, -16(%rax)
	movups	%xmm7, (%rax)
	addq	$32, %rax
	addq	$32, %rbx
	addq	$-8, %rdx
	jne	.LBB4_61
# BB#62:                                # %middle.block160
                                        #   in Loop: Header=BB4_51 Depth=2
	cmpq	$0, -64(%rsp)           # 8-byte Folded Reload
	movq	%rsi, %rax
	je	.LBB4_69
	.p2align	4, 0x90
.LBB4_63:                               # %.lr.ph53.i.us120.preheader223
                                        #   in Loop: Header=BB4_51 Depth=2
	movq	-80(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	subl	%eax, %ecx
	movq	%r11, %rdx
	subq	%rax, %rdx
	testb	$3, %cl
	je	.LBB4_66
# BB#64:                                # %.lr.ph53.i.us120.prol.preheader
                                        #   in Loop: Header=BB4_51 Depth=2
	leaq	(%rdi,%r10,4), %rbx
	movq	-8(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	subl	%eax, %ecx
	andl	$3, %ecx
	negq	%rcx
	.p2align	4, 0x90
.LBB4_65:                               # %.lr.ph53.i.us120.prol
                                        #   Parent Loop BB4_2 Depth=1
                                        #     Parent Loop BB4_51 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	(%r15,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	(%rbx,%rax,4), %xmm1
	movss	%xmm1, (%rbx,%rax,4)
	incq	%rax
	incq	%rcx
	jne	.LBB4_65
.LBB4_66:                               # %.lr.ph53.i.us120.prol.loopexit
                                        #   in Loop: Header=BB4_51 Depth=2
	cmpq	$3, %rdx
	jb	.LBB4_69
# BB#67:                                # %.lr.ph53.i.us120.preheader223.new
                                        #   in Loop: Header=BB4_51 Depth=2
	movq	-88(%rsp), %rdx         # 8-byte Reload
	subq	%rax, %rdx
	movq	-112(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rax,4), %rbx
	addq	%rax, %r10
	movq	-96(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r10,4), %rax
	.p2align	4, 0x90
.LBB4_68:                               # %.lr.ph53.i.us120
                                        #   Parent Loop BB4_2 Depth=1
                                        #     Parent Loop BB4_51 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	-12(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	-12(%rax), %xmm1
	movss	%xmm1, -12(%rax)
	movss	-8(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	-8(%rax), %xmm1
	movss	%xmm1, -8(%rax)
	movss	-4(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	-4(%rax), %xmm1
	movss	%xmm1, -4(%rax)
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	(%rax), %xmm1
	movss	%xmm1, (%rax)
	addq	$16, %rbx
	addq	$16, %rax
	addq	$-4, %rdx
	jne	.LBB4_68
.LBB4_69:                               # %daxpy.exit.us124
                                        #   in Loop: Header=BB4_51 Depth=2
	incq	%r9
	incl	%r8d
	addl	-124(%rsp), %r14d       # 4-byte Folded Reload
	cmpq	8(%rsp), %r9            # 8-byte Folded Reload
	jne	.LBB4_51
	.p2align	4, 0x90
.LBB4_70:                               # %.backedge
                                        #   in Loop: Header=BB4_2 Depth=1
	movl	28(%rsp), %ebp          # 4-byte Reload
	incl	%ebp
	incq	-40(%rsp)               # 8-byte Folded Spill
	movq	-24(%rsp), %rsi         # 8-byte Reload
	movq	128(%rsp), %r12         # 8-byte Reload
	addl	%esi, %r12d
	movq	136(%rsp), %rdx         # 8-byte Reload
	addb	$3, %dl
	movq	-56(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rsi), %eax
	movq	-88(%rsp), %rcx         # 8-byte Reload
	leal	-1(%rcx), %ecx
	movb	-117(%rsp), %r13b       # 1-byte Reload
	addb	$7, %r13b
	addl	%esi, -116(%rsp)        # 4-byte Folded Spill
	decq	-8(%rsp)                # 8-byte Folded Spill
	movq	120(%rsp), %rsi         # 8-byte Reload
	cmpq	64(%rsp), %rsi          # 8-byte Folded Reload
	movq	96(%rsp), %r15          # 8-byte Reload
	jne	.LBB4_2
.LBB4_71:                               # %.loopexit113
	movl	-68(%rsp), %ecx         # 4-byte Reload
	movslq	%ecx, %rax
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	%ecx, (%rdx,%rax,4)
	movl	-124(%rsp), %eax        # 4-byte Reload
	imull	%ecx, %eax
	addl	%ecx, %eax
	cltq
	movss	(%rdi,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB4_73
	jp	.LBB4_73
# BB#72:
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%ecx, (%rax)
.LBB4_73:
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	dgefa, .Lfunc_end4-dgefa
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	2147483648              # float -0
	.text
	.globl	dgesl
	.p2align	4, 0x90
	.type	dgesl,@function
dgesl:                                  # @dgesl
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rcx, -56(%rsp)         # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rsi, -104(%rsp)        # 8-byte Spill
	movq	%rdi, -112(%rsp)        # 8-byte Spill
	leal	-1(%rdx), %r10d
	testl	%r9d, %r9d
	movq	%rdx, -96(%rsp)         # 8-byte Spill
	je	.LBB5_25
# BB#1:                                 # %.preheader128
	testl	%edx, %edx
	jle	.LBB5_68
# BB#2:                                 # %.lr.ph136.preheader
	movslq	-104(%rsp), %r11        # 4-byte Folded Reload
	movq	-96(%rsp), %r15         # 8-byte Reload
	movl	%r15d, %r9d
	shlq	$2, %r11
	xorl	%edx, %edx
	movq	-112(%rsp), %r12        # 8-byte Reload
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph136
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_6 Depth 2
                                        #     Child Loop BB5_10 Depth 2
	movl	%ebx, %ecx
	imull	-104(%rsp), %ecx        # 4-byte Folded Reload
	testq	%rdx, %rdx
	jle	.LBB5_7
# BB#4:                                 # %.lr.ph55.i113.preheader
                                        #   in Loop: Header=BB5_3 Depth=1
	leaq	-1(%rdx), %r14
	testb	$3, %dl
	je	.LBB5_8
# BB#5:                                 # %.lr.ph55.i113.prol.preheader
                                        #   in Loop: Header=BB5_3 Depth=1
	movl	%ebx, %eax
	andl	$3, %eax
	xorps	%xmm0, %xmm0
	movq	%rdi, %rbp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_6:                                # %.lr.ph55.i113.prol
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rbp), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%r8,%rsi,4), %xmm1
	addss	%xmm1, %xmm0
	incq	%rsi
	addq	$4, %rbp
	cmpq	%rsi, %rax
	jne	.LBB5_6
	jmp	.LBB5_9
	.p2align	4, 0x90
.LBB5_7:                                #   in Loop: Header=BB5_3 Depth=1
	xorps	%xmm0, %xmm0
	jmp	.LBB5_11
	.p2align	4, 0x90
.LBB5_8:                                #   in Loop: Header=BB5_3 Depth=1
	xorps	%xmm0, %xmm0
	xorl	%esi, %esi
.LBB5_9:                                # %.lr.ph55.i113.prol.loopexit
                                        #   in Loop: Header=BB5_3 Depth=1
	cmpq	$3, %r14
	jb	.LBB5_11
	.p2align	4, 0x90
.LBB5_10:                               # %.lr.ph55.i113
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rdi,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	(%r8,%rsi,4), %xmm1
	addss	%xmm0, %xmm1
	movss	4(%rdi,%rsi,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	mulss	4(%r8,%rsi,4), %xmm0
	addss	%xmm1, %xmm0
	movss	8(%rdi,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	mulss	8(%r8,%rsi,4), %xmm1
	addss	%xmm0, %xmm1
	movss	12(%rdi,%rsi,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	mulss	12(%r8,%rsi,4), %xmm0
	addss	%xmm1, %xmm0
	addq	$4, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB5_10
.LBB5_11:                               # %ddot.exit115
                                        #   in Loop: Header=BB5_3 Depth=1
	movss	(%r8,%rdx,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	addl	%edx, %ecx
	movslq	%ecx, %rax
	divss	(%r12,%rax,4), %xmm1
	movss	%xmm1, (%r8,%rdx,4)
	incq	%rdx
	incl	%ebx
	addq	%r11, %rdi
	cmpq	%r9, %rdx
	jne	.LBB5_3
# BB#12:                                # %._crit_edge
	cmpl	$2, %r15d
	jl	.LBB5_68
# BB#13:                                # %._crit_edge
	cmpl	$2, %r10d
	jl	.LBB5_68
# BB#14:                                # %.preheader
	movl	%r10d, %r14d
	movq	-96(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rcx
	leal	-2(%rcx), %eax
	movq	-104(%rsp), %r9         # 8-byte Reload
	imull	%r9d, %eax
	leal	-1(%rcx,%rax), %ecx
	notl	%r9d
	leaq	12(%r8), %rax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	movq	-112(%rsp), %rax        # 8-byte Reload
	leaq	12(%rax), %r11
	movl	$1, %edx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_15:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_17 Depth 2
                                        #     Child Loop BB5_21 Depth 2
	movslq	%r10d, %r13
	movslq	%ecx, %r15
	leal	1(%rbx), %eax
	leaq	1(%rdx), %r12
	movq	-96(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	subl	%r12d, %esi
	movslq	%esi, %rbp
	movss	(%r8,%rbp,4), %xmm0     # xmm0 = mem[0],zero,zero,zero
	testb	$3, %al
	je	.LBB5_18
# BB#16:                                # %.lr.ph55.i.prol.preheader
                                        #   in Loop: Header=BB5_15 Depth=1
	leaq	(%r8,%r13,4), %rax
	movq	-112(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%r15,4), %rdi
	andl	$3, %edx
	xorps	%xmm1, %xmm1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_17:                               # %.lr.ph55.i.prol
                                        #   Parent Loop BB5_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rdi,%rsi,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	mulss	(%rax,%rsi,4), %xmm2
	addss	%xmm2, %xmm1
	incq	%rsi
	cmpq	%rsi, %rdx
	jne	.LBB5_17
	jmp	.LBB5_19
	.p2align	4, 0x90
.LBB5_18:                               #   in Loop: Header=BB5_15 Depth=1
	xorps	%xmm1, %xmm1
	xorl	%esi, %esi
.LBB5_19:                               # %.lr.ph55.i.prol.loopexit
                                        #   in Loop: Header=BB5_15 Depth=1
	cmpq	$3, %rbx
	jb	.LBB5_22
# BB#20:                                # %.new
                                        #   in Loop: Header=BB5_15 Depth=1
	movq	-104(%rsp), %rax        # 8-byte Reload
	leaq	(%rax,%r13,4), %rdx
	leaq	(%r11,%r15,4), %rdi
	leaq	1(%rbx), %rax
	.p2align	4, 0x90
.LBB5_21:                               # %.lr.ph55.i
                                        #   Parent Loop BB5_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	-12(%rdi,%rsi,4), %xmm2 # xmm2 = mem[0],zero,zero,zero
	mulss	-12(%rdx,%rsi,4), %xmm2
	addss	%xmm1, %xmm2
	movss	-8(%rdi,%rsi,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	mulss	-8(%rdx,%rsi,4), %xmm1
	addss	%xmm2, %xmm1
	movss	-4(%rdi,%rsi,4), %xmm2  # xmm2 = mem[0],zero,zero,zero
	mulss	-4(%rdx,%rsi,4), %xmm2
	addss	%xmm1, %xmm2
	movss	(%rdi,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdx,%rsi,4), %xmm1
	addss	%xmm2, %xmm1
	addq	$4, %rsi
	cmpq	%rsi, %rax
	jne	.LBB5_21
.LBB5_22:                               # %ddot.exit
                                        #   in Loop: Header=BB5_15 Depth=1
	addss	%xmm1, %xmm0
	movss	%xmm0, (%r8,%rbp,4)
	movq	-56(%rsp), %rax         # 8-byte Reload
	movslq	(%rax,%rbp,4), %rax
	cmpl	%ebp, %eax
	je	.LBB5_24
# BB#23:                                #   in Loop: Header=BB5_15 Depth=1
	movl	(%r8,%rax,4), %edx
	movss	%xmm0, (%r8,%rax,4)
	movl	%edx, (%r8,%rbp,4)
.LBB5_24:                               # %.backedge
                                        #   in Loop: Header=BB5_15 Depth=1
	incq	%rbx
	decl	%r10d
	addl	%r9d, %ecx
	cmpq	%r14, %r12
	movq	%r12, %rdx
	jne	.LBB5_15
	jmp	.LBB5_68
.LBB5_25:
	cmpl	$2, %edx
	jl	.LBB5_48
# BB#26:                                # %.lr.ph132.preheader
	movslq	-96(%rsp), %rax         # 4-byte Folded Reload
	movq	%rax, -40(%rsp)         # 8-byte Spill
	movl	%r10d, %edi
	movq	-104(%rsp), %rax        # 8-byte Reload
	leal	1(%rax), %eax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	leaq	20(%r8), %r13
	movq	-112(%rsp), %rcx        # 8-byte Reload
	leaq	16(%rcx), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	leaq	4(%r8), %rax
	leaq	12(%rcx), %rcx
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	leaq	8(%r8), %r11
	movl	$1, %ebp
	xorl	%ebx, %ebx
	xorps	%xmm0, %xmm0
	movq	%rdi, %r14
	movl	%r10d, %r9d
	movq	%rdi, -80(%rsp)         # 8-byte Spill
	jmp	.LBB5_30
.LBB5_27:                               # %vector.ph
                                        #   in Loop: Header=BB5_30 Depth=1
	andl	$7, %r9d
	movq	-24(%rsp), %rsi         # 8-byte Reload
	movq	-88(%rsp), %r12         # 8-byte Reload
	leaq	(%rsi,%r12,4), %rsi
	movq	-64(%rsp), %rdi         # 8-byte Reload
	subq	%r9, %rdi
	movaps	%xmm1, %xmm2
	shufps	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB5_28:                               # %vector.body
                                        #   Parent Loop BB5_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rbx), %xmm3
	movups	(%rbx), %xmm4
	movups	-16(%rsi), %xmm5
	movups	(%rsi), %xmm6
	mulps	%xmm2, %xmm5
	mulps	%xmm2, %xmm6
	addps	%xmm3, %xmm5
	addps	%xmm4, %xmm6
	movups	%xmm5, -16(%rbx)
	movups	%xmm6, (%rbx)
	addq	$32, %rbx
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB5_28
# BB#29:                                # %middle.block
                                        #   in Loop: Header=BB5_30 Depth=1
	cmpq	$0, -16(%rsp)           # 8-byte Folded Reload
	movq	-80(%rsp), %rdi         # 8-byte Reload
	movq	-32(%rsp), %rbx         # 8-byte Reload
	jne	.LBB5_41
	jmp	.LBB5_47
	.p2align	4, 0x90
.LBB5_30:                               # %.lr.ph132
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_28 Depth 2
                                        #     Child Loop BB5_43 Depth 2
                                        #     Child Loop BB5_46 Depth 2
	movq	%rbx, %r12
	movl	%r9d, %ecx
	movq	%rcx, -64(%rsp)         # 8-byte Spill
	movq	-56(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%r12,4), %edx
	movslq	%edx, %rcx
	movss	(%r8,%rcx,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	cmpq	%r12, %rdx
	je	.LBB5_32
# BB#31:                                #   in Loop: Header=BB5_30 Depth=1
	movl	(%r8,%r12,4), %edx
	movl	%edx, (%r8,%rcx,4)
	movss	%xmm1, (%r8,%r12,4)
.LBB5_32:                               #   in Loop: Header=BB5_30 Depth=1
	leaq	1(%r12), %rbx
	cmpq	%rbx, -40(%rsp)         # 8-byte Folded Reload
	jle	.LBB5_47
# BB#33:                                #   in Loop: Header=BB5_30 Depth=1
	ucomiss	%xmm0, %xmm1
	jne	.LBB5_34
	jnp	.LBB5_47
.LBB5_34:                               # %.lr.ph53.preheader.i
                                        #   in Loop: Header=BB5_30 Depth=1
	movq	%rdi, %r15
	subq	%r12, %r15
	movl	%r15d, %ecx
	movslq	%ebp, %rdx
	movq	%rdx, -88(%rsp)         # 8-byte Spill
	cmpq	$8, %rcx
	jb	.LBB5_39
# BB#35:                                # %min.iters.checked
                                        #   in Loop: Header=BB5_30 Depth=1
	movl	%r15d, %esi
	andl	$7, %esi
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	je	.LBB5_39
# BB#36:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_30 Depth=1
	movq	%rsi, -16(%rsp)         # 8-byte Spill
	movq	%rbx, -32(%rsp)         # 8-byte Spill
	movq	-72(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	imull	%r12d, %esi
	shlq	$32, %rsi
	movabsq	$4294967296, %rdi       # imm = 0x100000000
	addq	%rdi, %rsi
	movq	%rsi, %rdi
	sarq	$32, %rdi
	addq	%rcx, %rdi
	leaq	4(%r8,%r12,4), %rbx
	movq	%rbx, -48(%rsp)         # 8-byte Spill
	movq	-112(%rsp), %rbx        # 8-byte Reload
	leaq	(%rbx,%rdi,4), %rdi
	cmpq	%rdi, -48(%rsp)         # 8-byte Folded Reload
	jae	.LBB5_27
# BB#37:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_30 Depth=1
	leaq	(%r8,%r12,4), %rdi
	sarq	$30, %rsi
	leaq	4(%rdi,%rcx,4), %rdi
	addq	%rbx, %rsi
	cmpq	%rdi, %rsi
	jae	.LBB5_27
# BB#38:                                #   in Loop: Header=BB5_30 Depth=1
	xorl	%edx, %edx
	movq	-32(%rsp), %rbx         # 8-byte Reload
	jmp	.LBB5_40
	.p2align	4, 0x90
.LBB5_39:                               #   in Loop: Header=BB5_30 Depth=1
	xorl	%edx, %edx
.LBB5_40:                               # %.lr.ph53.i.preheader
                                        #   in Loop: Header=BB5_30 Depth=1
	movq	-88(%rsp), %r12         # 8-byte Reload
.LBB5_41:                               # %.lr.ph53.i.preheader
                                        #   in Loop: Header=BB5_30 Depth=1
	movq	%rbx, %r9
	subl	%edx, %r15d
	decq	%rcx
	subq	%rdx, %rcx
	testb	$3, %r15b
	je	.LBB5_44
# BB#42:                                # %.lr.ph53.i.prol.preheader
                                        #   in Loop: Header=BB5_30 Depth=1
	movq	-112(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%r12,4), %rsi
	leaq	(%rax,%rdx,4), %rdi
	movl	%r14d, %ebx
	subl	%edx, %ebx
	andl	$3, %ebx
	negq	%rbx
	.p2align	4, 0x90
.LBB5_43:                               # %.lr.ph53.i.prol
                                        #   Parent Loop BB5_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rsi,%rdx,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	addss	(%rdi), %xmm2
	movss	%xmm2, (%rdi)
	incq	%rdx
	addq	$4, %rdi
	incq	%rbx
	jne	.LBB5_43
.LBB5_44:                               # %.lr.ph53.i.prol.loopexit
                                        #   in Loop: Header=BB5_30 Depth=1
	cmpq	$3, %rcx
	movq	-80(%rsp), %rdi         # 8-byte Reload
	movq	%r9, %rbx
	jb	.LBB5_47
# BB#45:                                # %.lr.ph53.i.preheader.new
                                        #   in Loop: Header=BB5_30 Depth=1
	movq	-64(%rsp), %rcx         # 8-byte Reload
	subq	%rdx, %rcx
	addq	%rdx, %r12
	movq	-8(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%r12,4), %rsi
	leaq	(%r11,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB5_46:                               # %.lr.ph53.i
                                        #   Parent Loop BB5_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	-12(%rsi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	addss	-4(%rdx), %xmm2
	movss	%xmm2, -4(%rdx)
	movss	-8(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	addss	(%rdx), %xmm2
	movss	%xmm2, (%rdx)
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	addss	4(%rdx), %xmm2
	movss	%xmm2, 4(%rdx)
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	addss	8(%rdx), %xmm2
	movss	%xmm2, 8(%rdx)
	addq	$16, %rsi
	addq	$16, %rdx
	addq	$-4, %rcx
	jne	.LBB5_46
.LBB5_47:                               # %daxpy.exit.backedge
                                        #   in Loop: Header=BB5_30 Depth=1
	addq	$4, %r13
	addl	-72(%rsp), %ebp         # 4-byte Folded Reload
	movq	-64(%rsp), %rcx         # 8-byte Reload
	decl	%ecx
	addq	$4, %rax
	decq	%r14
	addq	$4, %r11
	cmpq	%rdi, %rbx
	movl	%ecx, %r9d
	jne	.LBB5_30
.LBB5_48:                               # %daxpy.exit123.preheader
	cmpl	$0, -96(%rsp)           # 4-byte Folded Reload
	jle	.LBB5_68
# BB#49:                                # %.lr.ph.preheader
	movq	-96(%rsp), %rax         # 8-byte Reload
	movslq	%eax, %rbx
	movslq	-104(%rsp), %rcx        # 4-byte Folded Reload
	movl	%eax, %eax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	movl	%r10d, %edx
	leaq	-1(%rbx), %rsi
	imulq	%rcx, %rsi
	movq	%rcx, %rax
	negq	%rax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	leaq	16(%r8), %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	movq	-112(%rsp), %rax        # 8-byte Reload
	leaq	16(%rax,%rsi,4), %r13
	shlq	$2, %rcx
	negq	%rcx
	movq	%rcx, -64(%rsp)         # 8-byte Spill
	movq	%rsi, -40(%rsp)         # 8-byte Spill
	leaq	(%rax,%rsi,4), %rbp
	xorl	%r11d, %r11d
	movss	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movq	%rdx, -72(%rsp)         # 8-byte Spill
	movq	%rdx, %rax
	xorl	%r9d, %r9d
	movq	%rbx, -80(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB5_50:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_60 Depth 2
                                        #     Child Loop BB5_64 Depth 2
                                        #     Child Loop BB5_66 Depth 2
	movq	%r11, %rdi
	movl	%r10d, %r15d
	leaq	1(%rdi), %r11
	incl	%r9d
	movq	-96(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%r9d, %ecx
	imull	-104(%rsp), %ecx        # 4-byte Folded Reload
	movq	%rbx, %rdx
	subq	%r11, %rdx
	addl	%ecx, %edx
	movq	%rbx, %rcx
	subq	%r11, %rcx
	movss	(%r8,%rcx,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	movslq	%edx, %rdx
	movq	-112(%rsp), %rsi        # 8-byte Reload
	divss	(%rsi,%rdx,4), %xmm1
	movss	%xmm1, (%r8,%rcx,4)
	jle	.LBB5_67
# BB#51:                                # %.lr.ph
                                        #   in Loop: Header=BB5_50 Depth=1
	ucomiss	%xmm0, %xmm1
	jne	.LBB5_52
	jnp	.LBB5_67
.LBB5_52:                               # %.lr.ph53.preheader.i118
                                        #   in Loop: Header=BB5_50 Depth=1
	movq	-72(%rsp), %r12         # 8-byte Reload
	subq	%rdi, %r12
	movl	%r12d, %edx
	cmpq	$8, %rdx
	jae	.LBB5_54
# BB#53:                                #   in Loop: Header=BB5_50 Depth=1
	xorl	%r14d, %r14d
	jmp	.LBB5_62
	.p2align	4, 0x90
.LBB5_54:                               # %min.iters.checked175
                                        #   in Loop: Header=BB5_50 Depth=1
	movl	%r12d, %esi
	andl	$7, %esi
	movq	%rdx, %r14
	subq	%rsi, %r14
	je	.LBB5_58
# BB#55:                                # %vector.memcheck190
                                        #   in Loop: Header=BB5_50 Depth=1
	movq	-72(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	subl	%edi, %ecx
	imulq	-88(%rsp), %rdi         # 8-byte Folded Reload
	addq	-40(%rsp), %rdi         # 8-byte Folded Reload
	movq	-112(%rsp), %rbx        # 8-byte Reload
	leaq	(%rbx,%rdi,4), %rdi
	leaq	(%rdi,%rcx,4), %rbx
	cmpq	%r8, %rbx
	jbe	.LBB5_59
# BB#56:                                # %vector.memcheck190
                                        #   in Loop: Header=BB5_50 Depth=1
	leaq	(%r8,%rcx,4), %rcx
	cmpq	%rcx, %rdi
	jae	.LBB5_59
# BB#57:                                #   in Loop: Header=BB5_50 Depth=1
	xorl	%r14d, %r14d
	movq	-80(%rsp), %rbx         # 8-byte Reload
	jmp	.LBB5_62
.LBB5_58:                               #   in Loop: Header=BB5_50 Depth=1
	xorl	%r14d, %r14d
	jmp	.LBB5_62
.LBB5_59:                               # %vector.ph191
                                        #   in Loop: Header=BB5_50 Depth=1
	andl	$7, %r10d
	movq	%r15, %rdi
	subq	%r10, %rdi
	movaps	%xmm1, %xmm2
	shufps	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	movq	%r13, %rbx
	movq	-48(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB5_60:                               # %vector.body171
                                        #   Parent Loop BB5_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rcx), %xmm3
	movups	(%rcx), %xmm4
	movups	-16(%rbx), %xmm5
	movups	(%rbx), %xmm6
	mulps	%xmm2, %xmm5
	mulps	%xmm2, %xmm6
	subps	%xmm5, %xmm3
	subps	%xmm6, %xmm4
	movups	%xmm3, -16(%rcx)
	movups	%xmm4, (%rcx)
	addq	$32, %rcx
	addq	$32, %rbx
	addq	$-8, %rdi
	jne	.LBB5_60
# BB#61:                                # %middle.block172
                                        #   in Loop: Header=BB5_50 Depth=1
	testq	%rsi, %rsi
	movq	-80(%rsp), %rbx         # 8-byte Reload
	je	.LBB5_67
	.p2align	4, 0x90
.LBB5_62:                               # %.lr.ph53.i122.preheader
                                        #   in Loop: Header=BB5_50 Depth=1
	subl	%r14d, %r12d
	decq	%rdx
	subq	%r14, %rdx
	testb	$3, %r12b
	je	.LBB5_65
# BB#63:                                # %.lr.ph53.i122.prol.preheader
                                        #   in Loop: Header=BB5_50 Depth=1
	leaq	(%rbp,%r14,4), %rsi
	movl	%eax, %edi
	subl	%r14d, %edi
	andl	$3, %edi
	negq	%rdi
	.p2align	4, 0x90
.LBB5_64:                               # %.lr.ph53.i122.prol
                                        #   Parent Loop BB5_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%r8,%r14,4), %xmm2     # xmm2 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	subss	%xmm3, %xmm2
	movss	%xmm2, (%r8,%r14,4)
	incq	%r14
	addq	$4, %rsi
	incq	%rdi
	jne	.LBB5_64
.LBB5_65:                               # %.lr.ph53.i122.prol.loopexit
                                        #   in Loop: Header=BB5_50 Depth=1
	cmpq	$3, %rdx
	jb	.LBB5_67
	.p2align	4, 0x90
.LBB5_66:                               # %.lr.ph53.i122
                                        #   Parent Loop BB5_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%r8,%r14,4), %xmm2     # xmm2 = mem[0],zero,zero,zero
	movss	(%rbp,%r14,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	subss	%xmm3, %xmm2
	movss	%xmm2, (%r8,%r14,4)
	movss	4(%r8,%r14,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movss	4(%rbp,%r14,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	subss	%xmm3, %xmm2
	movss	%xmm2, 4(%r8,%r14,4)
	movss	8(%r8,%r14,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movss	8(%rbp,%r14,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	subss	%xmm3, %xmm2
	movss	%xmm2, 8(%r8,%r14,4)
	movss	12(%r8,%r14,4), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movss	12(%rbp,%r14,4), %xmm3  # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	subss	%xmm3, %xmm2
	movss	%xmm2, 12(%r8,%r14,4)
	addq	$4, %r14
	cmpq	%r14, %r15
	jne	.LBB5_66
.LBB5_67:                               # %daxpy.exit123.backedge
                                        #   in Loop: Header=BB5_50 Depth=1
	movq	-64(%rsp), %rcx         # 8-byte Reload
	addq	%rcx, %r13
	decl	%r15d
	addq	%rcx, %rbp
	decq	%rax
	cmpq	-56(%rsp), %r11         # 8-byte Folded Reload
	movl	%r15d, %r10d
	jne	.LBB5_50
.LBB5_68:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	dgesl, .Lfunc_end5-dgesl
	.cfi_endproc

	.globl	dmxpy
	.p2align	4, 0x90
	.type	dmxpy,@function
dmxpy:                                  # @dmxpy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi58:
	.cfi_def_cfa_offset 64
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movq	%r9, -120(%rsp)         # 8-byte Spill
	movl	%ecx, -112(%rsp)        # 4-byte Spill
	movl	%edx, %r13d
	movl	%r13d, %ecx
	shrl	$31, %ecx
	addl	%r13d, %ecx
	andl	$-2, %ecx
	movl	%r13d, %eax
	subl	%ecx, %eax
	testl	%eax, %eax
	jle	.LBB6_16
# BB#1:
	testl	%edi, %edi
	jle	.LBB6_16
# BB#2:                                 # %.lr.ph260
	decl	%eax
	movslq	%eax, %rcx
	leaq	(%r8,%rcx,4), %rax
	imull	-112(%rsp), %ecx        # 4-byte Folded Reload
	movslq	%ecx, %r15
	movl	%edi, %ebp
	cmpl	$8, %edi
	jb	.LBB6_10
# BB#4:                                 # %min.iters.checked
	movl	%edi, %r11d
	andl	$7, %r11d
	movq	%rbp, %r9
	subq	%r11, %r9
	je	.LBB6_10
# BB#5:                                 # %vector.memcheck
	leaq	(%rsi,%rbp,4), %rbx
	movq	-120(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%r15,4), %r10
	leaq	(%r15,%rbp), %rdx
	leaq	(%rcx,%rdx,4), %r14
	cmpq	%rax, %rsi
	sbbb	%dl, %dl
	cmpq	%rbx, %rax
	sbbb	%cl, %cl
	andb	%dl, %cl
	cmpq	%r14, %rsi
	sbbb	%dl, %dl
	cmpq	%rbx, %r10
	sbbb	%r10b, %r10b
	xorl	%ebx, %ebx
	testb	$1, %cl
	jne	.LBB6_11
# BB#6:                                 # %vector.memcheck
	andb	%r10b, %dl
	andb	$1, %dl
	jne	.LBB6_11
# BB#7:                                 # %vector.body.preheader
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%rsi), %rcx
	movq	-120(%rsp), %rdx        # 8-byte Reload
	leaq	16(%rdx,%r15,4), %rbx
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB6_8:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rcx), %xmm1
	movups	(%rcx), %xmm2
	movups	-16(%rbx), %xmm3
	movups	(%rbx), %xmm4
	mulps	%xmm0, %xmm3
	mulps	%xmm0, %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rcx)
	movups	%xmm4, (%rcx)
	addq	$32, %rcx
	addq	$32, %rbx
	addq	$-8, %rdx
	jne	.LBB6_8
# BB#9:                                 # %middle.block
	testl	%r11d, %r11d
	movq	%r9, %rbx
	jne	.LBB6_11
	jmp	.LBB6_16
.LBB6_10:
	xorl	%ebx, %ebx
.LBB6_11:                               # %scalar.ph.preheader
	movl	%ebp, %edx
	subl	%ebx, %edx
	leaq	-1(%rbp), %r9
	testb	$1, %dl
	movq	%rbx, %rdx
	je	.LBB6_13
# BB#12:                                # %scalar.ph.prol
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	leaq	(%rbx,%r15), %rdx
	movq	-120(%rsp), %rcx        # 8-byte Reload
	mulss	(%rcx,%rdx,4), %xmm0
	addss	(%rsi,%rbx,4), %xmm0
	movss	%xmm0, (%rsi,%rbx,4)
	leaq	1(%rbx), %rdx
.LBB6_13:                               # %scalar.ph.prol.loopexit
	cmpq	%rbx, %r9
	je	.LBB6_16
# BB#14:                                # %scalar.ph.preheader.new
	subq	%rdx, %rbp
	leaq	4(%rsi,%rdx,4), %rcx
	addq	%rdx, %r15
	movq	-120(%rsp), %rdx        # 8-byte Reload
	leaq	4(%rdx,%r15,4), %rdx
	.p2align	4, 0x90
.LBB6_15:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	-4(%rdx), %xmm0
	addss	-4(%rcx), %xmm0
	movss	%xmm0, -4(%rcx)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%rdx), %xmm0
	addss	(%rcx), %xmm0
	movss	%xmm0, (%rcx)
	addq	$8, %rcx
	addq	$8, %rdx
	addq	$-2, %rbp
	jne	.LBB6_15
.LBB6_16:                               # %.loopexit245
	movl	%r13d, %r9d
	sarl	$31, %r9d
	movl	%r9d, %ecx
	shrl	$30, %ecx
	addl	%r13d, %ecx
	andl	$-4, %ecx
	movl	%r13d, %eax
	subl	%ecx, %eax
	cmpl	$2, %eax
	movq	%rsi, -96(%rsp)         # 8-byte Spill
	movl	%edi, -124(%rsp)        # 4-byte Spill
	movl	%r13d, -108(%rsp)       # 4-byte Spill
	movl	%r9d, -72(%rsp)         # 4-byte Spill
	jl	.LBB6_34
# BB#17:
	testl	%edi, %edi
	jle	.LBB6_34
# BB#18:                                # %.lr.ph257
	leal	-1(%rax), %ecx
	addl	$-2, %eax
	cltq
	movl	%eax, %edx
	movl	-112(%rsp), %ebp        # 4-byte Reload
	imull	%ebp, %edx
	movslq	%ecx, %rcx
	leaq	(%r8,%rcx,4), %rbx
	imull	%ebp, %ecx
	movslq	%ecx, %r11
	movslq	%edx, %rbp
	movl	%edi, %r14d
	cmpl	$4, %edi
	jae	.LBB6_20
# BB#19:
	xorl	%r12d, %r12d
	jmp	.LBB6_29
.LBB6_20:                               # %min.iters.checked296
	movl	%edi, %r10d
	andl	$3, %r10d
	movq	%r14, %r15
	subq	%r10, %r15
	je	.LBB6_27
# BB#21:                                # %vector.memcheck325
	leaq	(%rsi,%r14,4), %r9
	movq	%rbp, %rdi
	movq	-120(%rsp), %rbp        # 8-byte Reload
	leaq	(%rbp,%r11,4), %rdx
	movq	%r11, -88(%rsp)         # 8-byte Spill
	leaq	(%r11,%r14), %rcx
	leaq	(%rbp,%rcx,4), %r13
	leaq	(%rbp,%rdi,4), %rcx
	movq	%rdi, -104(%rsp)        # 8-byte Spill
	leaq	(%rdi,%r14), %rdi
	leaq	(%rbp,%rdi,4), %r12
	cmpq	%rbx, %rsi
	sbbb	%dil, %dil
	cmpq	%r9, %rbx
	sbbb	%r11b, %r11b
	andb	%dil, %r11b
	cmpq	%r13, %rsi
	sbbb	%dil, %dil
	cmpq	%r9, %rdx
	sbbb	%r13b, %r13b
	cmpq	%r12, %rsi
	sbbb	%sil, %sil
	cmpq	%r9, %rcx
	sbbb	%r9b, %r9b
	xorl	%r12d, %r12d
	testb	$1, %r11b
	jne	.LBB6_28
# BB#22:                                # %vector.memcheck325
	andb	%r13b, %dil
	andb	$1, %dil
	jne	.LBB6_28
# BB#23:                                # %vector.memcheck325
	andb	%r9b, %sil
	andb	$1, %sil
	movq	-96(%rsp), %rsi         # 8-byte Reload
	movl	-108(%rsp), %r13d       # 4-byte Reload
	movl	-72(%rsp), %r9d         # 4-byte Reload
	movq	-88(%rsp), %r11         # 8-byte Reload
	movq	-104(%rsp), %rbp        # 8-byte Reload
	jne	.LBB6_29
# BB#24:                                # %vector.body292.preheader
	movss	(%r8,%rax,4), %xmm0     # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movq	%r15, %rbp
	movq	%rsi, %rdi
	.p2align	4, 0x90
.LBB6_25:                               # %vector.body292
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi), %xmm2
	movups	(%rcx), %xmm3
	mulps	%xmm0, %xmm3
	addps	%xmm2, %xmm3
	movups	(%rdx), %xmm2
	mulps	%xmm1, %xmm2
	addps	%xmm3, %xmm2
	movups	%xmm2, (%rdi)
	addq	$16, %rdi
	addq	$16, %rcx
	addq	$16, %rdx
	addq	$-4, %rbp
	jne	.LBB6_25
# BB#26:                                # %middle.block293
	testl	%r10d, %r10d
	movq	%r15, %r12
	movq	-104(%rsp), %rbp        # 8-byte Reload
	jne	.LBB6_29
	jmp	.LBB6_34
.LBB6_27:
	xorl	%r12d, %r12d
	jmp	.LBB6_29
.LBB6_28:
	movq	-96(%rsp), %rsi         # 8-byte Reload
	movl	-108(%rsp), %r13d       # 4-byte Reload
	movl	-72(%rsp), %r9d         # 4-byte Reload
	movq	-88(%rsp), %r11         # 8-byte Reload
	movq	-104(%rsp), %rbp        # 8-byte Reload
.LBB6_29:                               # %scalar.ph294.preheader
	movl	%r14d, %ecx
	subl	%r12d, %ecx
	leaq	-1(%r14), %rdx
	testb	$1, %cl
	movq	%r12, %rcx
	je	.LBB6_31
# BB#30:                                # %scalar.ph294.prol
	movss	(%r8,%rax,4), %xmm0     # xmm0 = mem[0],zero,zero,zero
	leaq	(%r12,%rbp), %rcx
	movq	-120(%rsp), %rdi        # 8-byte Reload
	mulss	(%rdi,%rcx,4), %xmm0
	addss	(%rsi,%r12,4), %xmm0
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	leaq	(%r12,%r11), %rcx
	mulss	(%rdi,%rcx,4), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rsi,%r12,4)
	leaq	1(%r12), %rcx
.LBB6_31:                               # %scalar.ph294.prol.loopexit
	cmpq	%r12, %rdx
	je	.LBB6_34
# BB#32:                                # %scalar.ph294.preheader.new
	movq	-120(%rsp), %rdi        # 8-byte Reload
	leaq	4(%rdi,%rbp,4), %rdx
	leaq	4(%rdi,%r11,4), %rdi
	.p2align	4, 0x90
.LBB6_33:                               # %scalar.ph294
                                        # =>This Inner Loop Header: Depth=1
	movss	(%r8,%rax,4), %xmm0     # xmm0 = mem[0],zero,zero,zero
	mulss	-4(%rdx,%rcx,4), %xmm0
	addss	(%rsi,%rcx,4), %xmm0
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	-4(%rdi,%rcx,4), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rsi,%rcx,4)
	movss	(%r8,%rax,4), %xmm0     # xmm0 = mem[0],zero,zero,zero
	mulss	(%rdx,%rcx,4), %xmm0
	addss	4(%rsi,%rcx,4), %xmm0
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdi,%rcx,4), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, 4(%rsi,%rcx,4)
	addq	$2, %rcx
	cmpq	%rcx, %r14
	jne	.LBB6_33
.LBB6_34:                               # %.loopexit244
	movl	%r9d, %ecx
	shrl	$29, %ecx
	addl	%r13d, %ecx
	andl	$-8, %ecx
	movl	%r13d, %eax
	subl	%ecx, %eax
	cmpl	$4, %eax
	jl	.LBB6_50
# BB#35:
	cmpl	$0, -124(%rsp)          # 4-byte Folded Reload
	jle	.LBB6_50
# BB#36:                                # %.lr.ph254
	leal	-1(%rax), %ecx
	leal	-4(%rax), %edx
	movslq	%edx, %rbx
	movl	%ebx, %edx
	movl	-112(%rsp), %edi        # 4-byte Reload
	imull	%edi, %edx
	leal	-3(%rax), %esi
	movslq	%esi, %r14
	movl	%r14d, %esi
	imull	%edi, %esi
	addl	$-2, %eax
	movslq	%eax, %r15
	movl	%r15d, %eax
	imull	%edi, %eax
	movslq	%ecx, %rcx
	leaq	(%r8,%rcx,4), %r9
	imull	%edi, %ecx
	movslq	%ecx, %rcx
	movslq	%eax, %rdi
	movslq	%esi, %r10
	movslq	%edx, %rsi
	movl	-124(%rsp), %eax        # 4-byte Reload
	movl	%eax, %r11d
	cmpl	$4, %eax
	movq	%rdi, -104(%rsp)        # 8-byte Spill
	movq	%r10, -88(%rsp)         # 8-byte Spill
	jb	.LBB6_47
# BB#38:                                # %min.iters.checked341
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	andl	$3, %eax
	movq	%r11, %rdx
	subq	%rax, %rdx
	movq	%rdx, -16(%rsp)         # 8-byte Spill
	je	.LBB6_47
# BB#39:                                # %vector.memcheck386
	movq	%rbx, -48(%rsp)         # 8-byte Spill
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movq	-96(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%r11,4), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	movq	%rsi, -56(%rsp)         # 8-byte Spill
	movq	-120(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,4), %r13
	movq	%rcx, -64(%rsp)         # 8-byte Spill
	leaq	(%rcx,%r11), %rax
	leaq	(%rsi,%rax,4), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	leaq	(%rsi,%rdi,4), %rbp
	leaq	(%rdi,%r11), %rcx
	leaq	(%rsi,%rcx,4), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	leaq	(%rsi,%r10,4), %r12
	leaq	(%r10,%r11), %rdx
	leaq	(%rsi,%rdx,4), %rax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	movq	-56(%rsp), %rax         # 8-byte Reload
	leaq	(%rsi,%rax,4), %r10
	cmpq	%r9, %rbx
	sbbb	%dl, %dl
	movq	-40(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, %r9
	sbbb	%dil, %dil
	andb	%dl, %dil
	leaq	(%rax,%r11), %rdx
	leaq	(%rsi,%rdx,4), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rbx, %rsi
	cmpq	-32(%rsp), %rsi         # 8-byte Folded Reload
	sbbb	%al, %al
	movq	%rcx, %rbx
	cmpq	%rbx, %r13
	sbbb	%cl, %cl
	movb	%cl, -32(%rsp)          # 1-byte Spill
	cmpq	-24(%rsp), %rsi         # 8-byte Folded Reload
	sbbb	%dl, %dl
	cmpq	%rbx, %rbp
	sbbb	%cl, %cl
	movb	%cl, -24(%rsp)          # 1-byte Spill
	cmpq	-80(%rsp), %rsi         # 8-byte Folded Reload
	sbbb	%cl, %cl
	movb	%cl, -80(%rsp)          # 1-byte Spill
	cmpq	%rbx, %r12
	sbbb	%cl, %cl
	movb	%cl, -125(%rsp)         # 1-byte Spill
	cmpq	(%rsp), %rsi            # 8-byte Folded Reload
	sbbb	%cl, %cl
	cmpq	%rbx, %r10
	sbbb	%bl, %bl
	testb	$1, %dil
	jne	.LBB6_62
# BB#40:                                # %vector.memcheck386
	movb	%bl, -40(%rsp)          # 1-byte Spill
	movl	%ecx, %edi
	andb	-32(%rsp), %al          # 1-byte Folded Reload
	andb	$1, %al
	movq	-64(%rsp), %rcx         # 8-byte Reload
	movq	-56(%rsp), %rsi         # 8-byte Reload
	movq	-48(%rsp), %rbx         # 8-byte Reload
	jne	.LBB6_47
# BB#41:                                # %vector.memcheck386
	andb	-24(%rsp), %dl          # 1-byte Folded Reload
	andb	$1, %dl
	jne	.LBB6_47
# BB#42:                                # %vector.memcheck386
	movb	-80(%rsp), %al          # 1-byte Reload
	andb	-125(%rsp), %al         # 1-byte Folded Reload
	andb	$1, %al
	movl	$0, %edx
	jne	.LBB6_48
# BB#43:                                # %vector.memcheck386
	andb	-40(%rsp), %dil         # 1-byte Folded Reload
	andb	$1, %dil
	jne	.LBB6_48
# BB#44:                                # %vector.body337.preheader
	movss	(%r8,%rbx,4), %xmm0     # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movss	(%r8,%r14,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movss	(%r8,%r15,4), %xmm2     # xmm2 = mem[0],zero,zero,zero
	shufps	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	movss	(%r9), %xmm3            # xmm3 = mem[0],zero,zero,zero
	shufps	$0, %xmm3, %xmm3        # xmm3 = xmm3[0,0,0,0]
	movq	-16(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rcx
	movq	-96(%rsp), %rdx         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_45:                               # %vector.body337
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdx), %xmm4
	movups	(%r10), %xmm5
	mulps	%xmm0, %xmm5
	addps	%xmm4, %xmm5
	movups	(%r12), %xmm4
	mulps	%xmm1, %xmm4
	addps	%xmm5, %xmm4
	movups	(%rbp), %xmm5
	mulps	%xmm2, %xmm5
	addps	%xmm4, %xmm5
	movups	(%r13), %xmm4
	mulps	%xmm3, %xmm4
	addps	%xmm5, %xmm4
	movups	%xmm4, (%rdx)
	addq	$16, %rdx
	addq	$16, %r10
	addq	$16, %r12
	addq	$16, %rbp
	addq	$16, %r13
	addq	$-4, %rcx
	jne	.LBB6_45
# BB#46:                                # %middle.block338
	cmpl	$0, -8(%rsp)            # 4-byte Folded Reload
	movq	%rax, %rdx
	movq	-64(%rsp), %rcx         # 8-byte Reload
	jne	.LBB6_48
	jmp	.LBB6_50
.LBB6_47:
	xorl	%edx, %edx
.LBB6_48:                               # %scalar.ph339.preheader
	movq	-96(%rsp), %rax         # 8-byte Reload
	movq	%rbx, %r10
	movq	%rdx, %rbx
	leaq	(%rax,%rbx,4), %rdx
	addq	%rbx, %rsi
	movq	-120(%rsp), %rax        # 8-byte Reload
	leaq	(%rax,%rsi,4), %rsi
	movq	-88(%rsp), %rdi         # 8-byte Reload
	addq	%rbx, %rdi
	leaq	(%rax,%rdi,4), %rdi
	movq	-104(%rsp), %rbp        # 8-byte Reload
	addq	%rbx, %rbp
	leaq	(%rax,%rbp,4), %rbp
	addq	%rbx, %rcx
	leaq	(%rax,%rcx,4), %rcx
	subq	%rbx, %r11
	movq	%r10, %rbx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_49:                               # %scalar.ph339
                                        # =>This Inner Loop Header: Depth=1
	movss	(%r8,%rbx,4), %xmm0     # xmm0 = mem[0],zero,zero,zero
	mulss	(%rsi,%rax,4), %xmm0
	addss	(%rdx,%rax,4), %xmm0
	movss	(%r8,%r14,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdi,%rax,4), %xmm1
	addss	%xmm0, %xmm1
	movss	(%r8,%r15,4), %xmm0     # xmm0 = mem[0],zero,zero,zero
	mulss	(%rbp,%rax,4), %xmm0
	addss	%xmm1, %xmm0
	movss	(%r9), %xmm1            # xmm1 = mem[0],zero,zero,zero
	mulss	(%rcx,%rax,4), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rdx,%rax,4)
	incq	%rax
	cmpq	%rax, %r11
	jne	.LBB6_49
.LBB6_50:                               # %.loopexit243
	movl	-72(%rsp), %ecx         # 4-byte Reload
	shrl	$28, %ecx
	movl	-108(%rsp), %eax        # 4-byte Reload
	addl	%eax, %ecx
	andl	$-16, %ecx
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	subl	%ecx, %eax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	cmpl	$8, %eax
	movq	-96(%rsp), %r12         # 8-byte Reload
	jl	.LBB6_54
# BB#51:
	cmpl	$0, -124(%rsp)          # 4-byte Folded Reload
	jle	.LBB6_54
# BB#52:                                # %.lr.ph251
	movq	-80(%rsp), %rsi         # 8-byte Reload
	leal	-1(%rsi), %ebx
	leal	-8(%rsi), %eax
	movslq	%eax, %rcx
	movq	%rcx, %rax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	movl	%ecx, %r9d
	movl	-112(%rsp), %eax        # 4-byte Reload
	imull	%eax, %r9d
	leal	-7(%rsi), %ecx
	movslq	%ecx, %rdx
	movq	%rdx, %rcx
	movq	%rcx, -104(%rsp)        # 8-byte Spill
	movl	%edx, %r10d
	imull	%eax, %r10d
	leal	-6(%rsi), %ecx
	movslq	%ecx, %rdx
	movq	%rdx, %rcx
	movq	%rcx, -88(%rsp)         # 8-byte Spill
	movl	%edx, %r11d
	imull	%eax, %r11d
	leal	-5(%rsi), %ecx
	movslq	%ecx, %rdx
	movq	%rdx, %rcx
	movq	%rcx, -56(%rsp)         # 8-byte Spill
	movl	%edx, %ebp
	imull	%eax, %ebp
	leal	-4(%rsi), %ecx
	movslq	%ecx, %rdx
	movq	%rdx, %rcx
	movq	%rcx, -64(%rsp)         # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	imull	%eax, %edx
	leal	-3(%rsi), %ecx
	movslq	%ecx, %rdi
	movq	%rdi, %rcx
	movq	%rcx, -16(%rsp)         # 8-byte Spill
	movl	%edi, %r14d
	imull	%eax, %r14d
	leal	-2(%rsi), %ecx
	movslq	%ecx, %rsi
	movq	%rsi, %rcx
	movq	%rcx, -48(%rsp)         # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	imull	%eax, %esi
	movslq	%ebx, %rbx
	movl	%ebx, %edi
	imull	%eax, %edi
	movslq	%edi, %rcx
	movslq	%esi, %r15
	movslq	%r14d, %r14
	movslq	%edx, %rdi
	movslq	%ebp, %rsi
	movslq	%r11d, %rbp
	movslq	%r10d, %rdx
	movslq	%r9d, %rax
	movq	-120(%rsp), %r9         # 8-byte Reload
	leaq	(%r9,%rax,4), %rax
	leaq	(%r9,%rdx,4), %rdx
	leaq	(%r9,%rbp,4), %rbp
	leaq	(%r9,%rsi,4), %r10
	leaq	(%r9,%rdi,4), %rdi
	leaq	(%r9,%r14,4), %r11
	leaq	(%r9,%r15,4), %rsi
	leaq	(%r9,%rcx,4), %r14
	movl	-124(%rsp), %r9d        # 4-byte Reload
	xorl	%r15d, %r15d
	movq	-72(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_53:                               # =>This Inner Loop Header: Depth=1
	movss	(%r8,%r13,4), %xmm0     # xmm0 = mem[0],zero,zero,zero
	mulss	(%rax,%r15,4), %xmm0
	addss	(%r12,%r15,4), %xmm0
	movq	-104(%rsp), %rcx        # 8-byte Reload
	movss	(%r8,%rcx,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdx,%r15,4), %xmm1
	addss	%xmm0, %xmm1
	movq	-88(%rsp), %rcx         # 8-byte Reload
	movss	(%r8,%rcx,4), %xmm0     # xmm0 = mem[0],zero,zero,zero
	mulss	(%rbp,%r15,4), %xmm0
	addss	%xmm1, %xmm0
	movq	-56(%rsp), %rcx         # 8-byte Reload
	movss	(%r8,%rcx,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	(%r10,%r15,4), %xmm1
	addss	%xmm0, %xmm1
	movq	-64(%rsp), %rcx         # 8-byte Reload
	movss	(%r8,%rcx,4), %xmm0     # xmm0 = mem[0],zero,zero,zero
	mulss	(%rdi,%r15,4), %xmm0
	addss	%xmm1, %xmm0
	movq	-16(%rsp), %rcx         # 8-byte Reload
	movss	(%r8,%rcx,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	(%r11,%r15,4), %xmm1
	addss	%xmm0, %xmm1
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movss	(%r8,%rcx,4), %xmm0     # xmm0 = mem[0],zero,zero,zero
	mulss	(%rsi,%r15,4), %xmm0
	addss	%xmm1, %xmm0
	movss	(%r8,%rbx,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	(%r14,%r15,4), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r12,%r15,4)
	incq	%r15
	cmpq	%r15, %r9
	jne	.LBB6_53
.LBB6_54:                               # %.loopexit
	movq	-80(%rsp), %rcx         # 8-byte Reload
	leal	15(%rcx), %eax
	movl	-108(%rsp), %edx        # 4-byte Reload
	cmpl	%edx, %eax
	movq	%r12, %r13
	jge	.LBB6_60
# BB#55:                                # %.preheader.lr.ph
	movslq	%ecx, %rcx
	movslq	-112(%rsp), %rax        # 4-byte Folded Reload
	movslq	%edx, %r11
	movl	-124(%rsp), %r10d       # 4-byte Reload
	leaq	15(%rcx), %rbp
	imulq	%rax, %rcx
	movq	-120(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,4), %r14
	movq	%rax, %r9
	shlq	$6, %r9
	leaq	(,%rax,4), %rcx
	.p2align	4, 0x90
.LBB6_56:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_58 Depth 2
	cmpl	$0, -124(%rsp)          # 4-byte Folded Reload
	jle	.LBB6_59
# BB#57:                                # %.lr.ph
                                        #   in Loop: Header=BB6_56 Depth=1
	movq	%r10, %r15
	movq	%r14, %rbx
	movq	%r13, %r12
	.p2align	4, 0x90
.LBB6_58:                               #   Parent Loop BB6_56 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	-60(%r8,%rbp,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	mulss	(%rbx), %xmm0
	addss	(%r12), %xmm0
	movss	-56(%r8,%rbp,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	leaq	(%rbx,%rcx), %rsi
	mulss	(%rbx,%rax,4), %xmm1
	addss	%xmm0, %xmm1
	movss	-52(%r8,%rbp,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	leaq	(%rsi,%rcx), %rdi
	mulss	(%rbx,%rax,8), %xmm0
	addss	%xmm1, %xmm0
	movss	-48(%r8,%rbp,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	leaq	(%rdi,%rcx), %rdx
	mulss	(%rsi,%rax,8), %xmm1
	addss	%xmm0, %xmm1
	movss	-44(%r8,%rbp,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	leaq	(%rdx,%rcx), %rsi
	mulss	(%rdi,%rax,8), %xmm0
	addss	%xmm1, %xmm0
	movss	-40(%r8,%rbp,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	leaq	(%rsi,%rcx), %rdi
	mulss	(%rdx,%rax,8), %xmm1
	addss	%xmm0, %xmm1
	movss	-36(%r8,%rbp,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	leaq	(%rdi,%rcx), %rdx
	mulss	(%rsi,%rax,8), %xmm0
	addss	%xmm1, %xmm0
	movss	-32(%r8,%rbp,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	leaq	(%rdx,%rcx), %rsi
	mulss	(%rdi,%rax,8), %xmm1
	addss	%xmm0, %xmm1
	movss	-28(%r8,%rbp,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	leaq	(%rsi,%rcx), %rdi
	mulss	(%rdx,%rax,8), %xmm0
	addss	%xmm1, %xmm0
	movss	-24(%r8,%rbp,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	leaq	(%rdi,%rcx), %rdx
	mulss	(%rsi,%rax,8), %xmm1
	addss	%xmm0, %xmm1
	movss	-20(%r8,%rbp,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	leaq	(%rdx,%rcx), %rsi
	mulss	(%rdi,%rax,8), %xmm0
	addss	%xmm1, %xmm0
	movss	-16(%r8,%rbp,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	leaq	(%rsi,%rcx), %rdi
	mulss	(%rdx,%rax,8), %xmm1
	addss	%xmm0, %xmm1
	movss	-12(%r8,%rbp,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	leaq	(%rdi,%rcx), %rdx
	mulss	(%rsi,%rax,8), %xmm0
	addss	%xmm1, %xmm0
	movss	-8(%r8,%rbp,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdi,%rax,8), %xmm1
	addss	%xmm0, %xmm1
	movss	-4(%r8,%rbp,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	mulss	(%rdx,%rax,8), %xmm0
	addq	%rcx, %rdx
	addss	%xmm1, %xmm0
	movss	(%r8,%rbp,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdx,%rax,8), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r12)
	addq	$4, %r12
	addq	$4, %rbx
	decq	%r15
	jne	.LBB6_58
.LBB6_59:                               # %._crit_edge
                                        #   in Loop: Header=BB6_56 Depth=1
	addq	$16, %rbp
	addq	%r9, %r14
	cmpq	%r11, %rbp
	jl	.LBB6_56
.LBB6_60:                               # %._crit_edge248
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_62:
	movq	-64(%rsp), %rcx         # 8-byte Reload
	movq	-56(%rsp), %rsi         # 8-byte Reload
	xorl	%edx, %edx
	movq	-48(%rsp), %rbx         # 8-byte Reload
	jmp	.LBB6_48
.Lfunc_end6:
	.size	dmxpy, .Lfunc_end6-dmxpy
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI7_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI7_1:
	.long	872415232               # float 1.1920929E-7
	.text
	.globl	epslon
	.p2align	4, 0x90
	.type	epslon,@function
epslon:                                 # @epslon
	.cfi_startproc
# BB#0:
	andps	.LCPI7_0(%rip), %xmm0
	mulss	.LCPI7_1(%rip), %xmm0
	retq
.Lfunc_end7:
	.size	epslon, .Lfunc_end7-epslon
	.cfi_endproc

	.globl	print_time
	.p2align	4, 0x90
	.type	print_time,@function
print_time:                             # @print_time
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end8:
	.size	print_time, .Lfunc_end8-print_time
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.globl	idamax
	.p2align	4, 0x90
	.type	idamax,@function
idamax:                                 # @idamax
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testl	%edi, %edi
	jle	.LBB9_1
# BB#2:
	xorl	%eax, %eax
	cmpl	$1, %edi
	jne	.LBB9_3
.LBB9_17:                               # %.loopexit
	retq
.LBB9_1:
	movl	$-1, %eax
	retq
.LBB9_3:
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	andps	.LCPI9_0(%rip), %xmm0
	cmpl	$1, %edx
	jne	.LBB9_11
# BB#4:                                 # %.lr.ph.preheader
	movl	%edi, %r9d
	addl	$3, %edi
	leaq	-2(%r9), %r8
	andq	$3, %rdi
	je	.LBB9_5
# BB#6:                                 # %.lr.ph.prol.preheader
	negq	%rdi
	xorl	%eax, %eax
	movl	$1, %edx
	movaps	.LCPI9_0(%rip), %xmm1   # xmm1 = [nan,nan,nan,nan]
	movaps	%xmm0, %xmm2
	.p2align	4, 0x90
.LBB9_7:                                # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rsi,%rdx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	andps	%xmm1, %xmm0
	ucomiss	%xmm2, %xmm0
	maxss	%xmm2, %xmm0
	cmoval	%edx, %eax
	leaq	1(%rdi,%rdx), %rcx
	incq	%rdx
	cmpq	$1, %rcx
	movaps	%xmm0, %xmm2
	jne	.LBB9_7
	jmp	.LBB9_8
.LBB9_11:                               # %.lr.ph54.preheader
	leal	1(%rdx), %eax
	movslq	%eax, %r8
	movslq	%edx, %rcx
	movl	$1, %eax
	testb	$1, %dil
	jne	.LBB9_12
# BB#13:                                # %.lr.ph54.prol
	movss	(%rsi,%r8,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	andps	.LCPI9_0(%rip), %xmm1
	maxss	%xmm0, %xmm1
	addq	%rcx, %r8
	movl	$2, %edx
	movaps	%xmm1, %xmm0
	cmpl	$2, %edi
	je	.LBB9_17
	jmp	.LBB9_15
.LBB9_12:
	movl	$1, %edx
	cmpl	$2, %edi
	je	.LBB9_17
.LBB9_15:                               # %.lr.ph54.preheader.new
	leaq	(,%rcx,8), %r9
	shlq	$2, %rcx
	leaq	(%rcx,%r8,4), %r10
	shlq	$2, %r8
	movl	$1, %eax
	movaps	.LCPI9_0(%rip), %xmm1   # xmm1 = [nan,nan,nan,nan]
	.p2align	4, 0x90
.LBB9_16:                               # %.lr.ph54
                                        # =>This Inner Loop Header: Depth=1
	movss	(%r8,%rsi), %xmm2       # xmm2 = mem[0],zero,zero,zero
	andps	%xmm1, %xmm2
	ucomiss	%xmm0, %xmm2
	maxss	%xmm0, %xmm2
	cmoval	%edx, %eax
	movss	(%r10,%rsi), %xmm0      # xmm0 = mem[0],zero,zero,zero
	andps	%xmm1, %xmm0
	leal	1(%rdx), %ecx
	ucomiss	%xmm2, %xmm0
	maxss	%xmm2, %xmm0
	cmoval	%ecx, %eax
	addq	%r9, %rsi
	addl	$2, %edx
	cmpl	%edi, %edx
	jne	.LBB9_16
	jmp	.LBB9_17
.LBB9_5:
	xorl	%eax, %eax
	movl	$1, %edx
.LBB9_8:                                # %.lr.ph.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB9_17
# BB#9:
	movaps	.LCPI9_0(%rip), %xmm1   # xmm1 = [nan,nan,nan,nan]
	.p2align	4, 0x90
.LBB9_10:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rsi,%rdx,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	andps	%xmm1, %xmm2
	ucomiss	%xmm0, %xmm2
	maxss	%xmm0, %xmm2
	cmoval	%edx, %eax
	movss	4(%rsi,%rdx,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	andps	%xmm1, %xmm0
	leal	1(%rdx), %ecx
	ucomiss	%xmm2, %xmm0
	maxss	%xmm2, %xmm0
	cmovbel	%eax, %ecx
	movss	8(%rsi,%rdx,4), %xmm2   # xmm2 = mem[0],zero,zero,zero
	andps	%xmm1, %xmm2
	leal	2(%rdx), %edi
	ucomiss	%xmm0, %xmm2
	maxss	%xmm0, %xmm2
	cmovbel	%ecx, %edi
	movss	12(%rsi,%rdx,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	andps	%xmm1, %xmm0
	leal	3(%rdx), %eax
	ucomiss	%xmm2, %xmm0
	maxss	%xmm2, %xmm0
	cmovbel	%edi, %eax
	addq	$4, %rdx
	cmpq	%r9, %rdx
	jne	.LBB9_10
	jmp	.LBB9_17
.Lfunc_end9:
	.size	idamax, .Lfunc_end9-idamax
	.cfi_endproc

	.globl	dscal
	.p2align	4, 0x90
	.type	dscal,@function
dscal:                                  # @dscal
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testl	%edi, %edi
	jle	.LBB10_13
# BB#1:
	cmpl	$1, %edx
	jne	.LBB10_10
# BB#2:                                 # %.lr.ph.preheader
	movl	%edi, %eax
	cmpl	$7, %edi
	jbe	.LBB10_3
# BB#6:                                 # %min.iters.checked
	andl	$7, %edi
	movq	%rax, %r8
	subq	%rdi, %r8
	je	.LBB10_3
# BB#7:                                 # %vector.ph
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	leaq	16(%rsi), %rdx
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB10_8:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdx), %xmm2
	movups	(%rdx), %xmm3
	mulps	%xmm1, %xmm2
	mulps	%xmm1, %xmm3
	movups	%xmm2, -16(%rdx)
	movups	%xmm3, (%rdx)
	addq	$32, %rdx
	addq	$-8, %rcx
	jne	.LBB10_8
# BB#9:                                 # %middle.block
	testl	%edi, %edi
	jne	.LBB10_4
	jmp	.LBB10_13
.LBB10_10:
	imull	%edx, %edi
	testl	%edi, %edi
	jle	.LBB10_13
# BB#11:                                # %.lr.ph30.preheader
	movslq	%edx, %rax
	movslq	%edi, %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB10_12:                              # %.lr.ph30
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rsi,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsi,%rdx,4)
	addq	%rax, %rdx
	cmpq	%rcx, %rdx
	jl	.LBB10_12
	jmp	.LBB10_13
.LBB10_3:
	xorl	%r8d, %r8d
.LBB10_4:                               # %.lr.ph.preheader40
	leaq	(%rsi,%r8,4), %rcx
	subq	%r8, %rax
	.p2align	4, 0x90
.LBB10_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rcx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rcx)
	addq	$4, %rcx
	decq	%rax
	jne	.LBB10_5
.LBB10_13:                              # %.loopexit
	retq
.Lfunc_end10:
	.size	dscal, .Lfunc_end10-dscal
	.cfi_endproc

	.globl	daxpy
	.p2align	4, 0x90
	.type	daxpy,@function
daxpy:                                  # @daxpy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi69:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 56
.Lcfi71:
	.cfi_offset %rbx, -56
.Lcfi72:
	.cfi_offset %r12, -48
.Lcfi73:
	.cfi_offset %r13, -40
.Lcfi74:
	.cfi_offset %r14, -32
.Lcfi75:
	.cfi_offset %r15, -24
.Lcfi76:
	.cfi_offset %rbp, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testl	%edi, %edi
	jle	.LBB11_36
# BB#1:
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB11_2
	jnp	.LBB11_36
.LBB11_2:
	cmpl	$1, %edx
	jne	.LBB11_19
# BB#3:
	cmpl	$1, %r8d
	jne	.LBB11_19
# BB#4:                                 # %.preheader
	testl	%edi, %edi
	jle	.LBB11_36
# BB#5:                                 # %.lr.ph53.preheader
	movl	%edi, %r8d
	cmpl	$7, %edi
	jbe	.LBB11_6
# BB#13:                                # %min.iters.checked
	andl	$7, %edi
	movq	%r8, %rdx
	subq	%rdi, %rdx
	je	.LBB11_6
# BB#14:                                # %vector.memcheck
	leaq	(%rsi,%r8,4), %rax
	cmpq	%rcx, %rax
	jbe	.LBB11_16
# BB#15:                                # %vector.memcheck
	leaq	(%rcx,%r8,4), %rax
	cmpq	%rsi, %rax
	jbe	.LBB11_16
.LBB11_6:
	xorl	%edx, %edx
.LBB11_7:                               # %.lr.ph53.preheader131
	movl	%r8d, %eax
	subl	%edx, %eax
	leaq	-1(%r8), %rdi
	subq	%rdx, %rdi
	andq	$3, %rax
	je	.LBB11_10
# BB#8:                                 # %.lr.ph53.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB11_9:                               # %.lr.ph53.prol
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rsi,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rcx,%rdx,4), %xmm1
	movss	%xmm1, (%rcx,%rdx,4)
	incq	%rdx
	incq	%rax
	jne	.LBB11_9
.LBB11_10:                              # %.lr.ph53.prol.loopexit
	cmpq	$3, %rdi
	jb	.LBB11_36
# BB#11:                                # %.lr.ph53.preheader131.new
	subq	%rdx, %r8
	leaq	12(%rsi,%rdx,4), %rsi
	leaq	12(%rcx,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB11_12:                              # %.lr.ph53
                                        # =>This Inner Loop Header: Depth=1
	movss	-12(%rsi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	-12(%rcx), %xmm1
	movss	%xmm1, -12(%rcx)
	movss	-8(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	-8(%rcx), %xmm1
	movss	%xmm1, -8(%rcx)
	movss	-4(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	-4(%rcx), %xmm1
	movss	%xmm1, -4(%rcx)
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rcx), %xmm1
	movss	%xmm1, (%rcx)
	addq	$16, %rsi
	addq	$16, %rcx
	addq	$-4, %r8
	jne	.LBB11_12
	jmp	.LBB11_36
.LBB11_19:
	testl	%edi, %edi
	jle	.LBB11_36
# BB#20:                                # %.lr.ph.preheader
	movl	$1, %eax
	subl	%edi, %eax
	movl	%eax, %ebp
	imull	%r8d, %ebp
	imull	%edx, %eax
	xorl	%r10d, %r10d
	testl	%edx, %edx
	movslq	%eax, %r12
	cmovnsq	%r10, %r12
	testl	%r8d, %r8d
	movslq	%edx, %r9
	movslq	%ebp, %rbx
	cmovnsq	%r10, %rbx
	movslq	%r8d, %r10
	leal	-1(%rdi), %r11d
	leaq	1(%r11), %r14
	cmpq	$8, %r14
	jb	.LBB11_21
# BB#22:                                # %min.iters.checked75
	movabsq	$8589934584, %r15       # imm = 0x1FFFFFFF8
	andq	%r14, %r15
	je	.LBB11_21
# BB#23:                                # %vector.scevcheck
	cmpl	$1, %r8d
	jne	.LBB11_21
# BB#24:                                # %vector.scevcheck
	cmpl	$1, %edx
	jne	.LBB11_21
# BB#25:                                # %vector.memcheck94
	leaq	(%rcx,%rbx,4), %rax
	leaq	(%r12,%r11), %rdx
	leaq	4(%rsi,%rdx,4), %rdx
	cmpq	%rdx, %rax
	jae	.LBB11_27
# BB#26:                                # %vector.memcheck94
	leaq	(%rbx,%r11), %rax
	leaq	4(%rcx,%rax,4), %rax
	leaq	(%rsi,%r12,4), %rdx
	cmpq	%rax, %rdx
	jae	.LBB11_27
.LBB11_21:
	movq	%rbx, %r13
	movq	%r12, %r8
	xorl	%r15d, %r15d
.LBB11_30:                              # %.lr.ph.preheader130
	movl	%edi, %eax
	subl	%r15d, %eax
	testb	$1, %al
	jne	.LBB11_32
# BB#31:
	movl	%r15d, %ebp
	cmpl	%r15d, %r11d
	jne	.LBB11_34
	jmp	.LBB11_36
.LBB11_32:                              # %.lr.ph.prol
	movss	(%rsi,%r8,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rcx,%r13,4), %xmm1
	movss	%xmm1, (%rcx,%r13,4)
	addq	%r9, %r8
	addq	%r10, %r13
	leal	1(%r15), %ebp
	cmpl	%r15d, %r11d
	je	.LBB11_36
.LBB11_34:                              # %.lr.ph.preheader130.new
	leaq	(,%r10,8), %r11
	shlq	$2, %r10
	leaq	(%r10,%r13,4), %rax
	shlq	$2, %r13
	leaq	(,%r9,8), %rdx
	shlq	$2, %r9
	leaq	(%r9,%r8,4), %rbx
	shlq	$2, %r8
	subl	%ebp, %edi
	.p2align	4, 0x90
.LBB11_35:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%r8,%rsi), %xmm1       # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%r13,%rcx), %xmm1
	movss	%xmm1, (%r13,%rcx)
	movss	(%rbx,%rsi), %xmm1      # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rax,%rcx), %xmm1
	movss	%xmm1, (%rax,%rcx)
	addq	%r11, %rcx
	addq	%rdx, %rsi
	addl	$-2, %edi
	jne	.LBB11_35
.LBB11_36:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_16:                              # %vector.ph
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	leaq	16(%rcx), %rbx
	leaq	16(%rsi), %rax
	movq	%rdx, %r9
	.p2align	4, 0x90
.LBB11_17:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx), %xmm2
	movups	(%rbx), %xmm3
	movups	-16(%rax), %xmm4
	movups	(%rax), %xmm5
	mulps	%xmm1, %xmm4
	mulps	%xmm1, %xmm5
	addps	%xmm2, %xmm4
	addps	%xmm3, %xmm5
	movups	%xmm4, -16(%rbx)
	movups	%xmm5, (%rbx)
	addq	$32, %rbx
	addq	$32, %rax
	addq	$-8, %r9
	jne	.LBB11_17
# BB#18:                                # %middle.block
	testl	%edi, %edi
	jne	.LBB11_7
	jmp	.LBB11_36
.LBB11_27:                              # %vector.ph95
	movq	%r15, %r13
	imulq	%r10, %r13
	addq	%rbx, %r13
	movq	%r15, %r8
	imulq	%r9, %r8
	addq	%r12, %r8
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	leaq	16(%rsi,%r12,4), %rax
	movq	%r9, %r12
	shlq	$5, %r12
	leaq	16(%rcx,%rbx,4), %rbx
	movq	%r10, %rdx
	shlq	$5, %rdx
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB11_28:                              # %vector.body71
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx), %xmm2
	movups	(%rbx), %xmm3
	movups	-16(%rax), %xmm4
	movups	(%rax), %xmm5
	mulps	%xmm1, %xmm4
	mulps	%xmm1, %xmm5
	addps	%xmm2, %xmm4
	addps	%xmm3, %xmm5
	movups	%xmm4, -16(%rbx)
	movups	%xmm5, (%rbx)
	addq	%r12, %rax
	addq	%rdx, %rbx
	addq	$-8, %rbp
	jne	.LBB11_28
# BB#29:                                # %middle.block72
	cmpq	%r15, %r14
	jne	.LBB11_30
	jmp	.LBB11_36
.Lfunc_end11:
	.size	daxpy, .Lfunc_end11-daxpy
	.cfi_endproc

	.globl	ddot
	.p2align	4, 0x90
	.type	ddot,@function
ddot:                                   # @ddot
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 24
.Lcfi79:
	.cfi_offset %rbx, -24
.Lcfi80:
	.cfi_offset %r14, -16
	testl	%edi, %edi
	jle	.LBB12_1
# BB#2:
	cmpl	$1, %edx
	jne	.LBB12_11
# BB#3:
	cmpl	$1, %r8d
	jne	.LBB12_11
# BB#4:                                 # %.lr.ph55.preheader
	movl	%edi, %eax
	leaq	-1(%rax), %rdi
	movq	%rax, %rbx
	andq	$3, %rbx
	je	.LBB12_5
# BB#6:                                 # %.lr.ph55.prol.preheader
	xorps	%xmm0, %xmm0
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB12_7:                               # %.lr.ph55.prol
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rsi,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	(%rcx,%rdx,4), %xmm1
	addss	%xmm1, %xmm0
	incq	%rdx
	cmpq	%rdx, %rbx
	jne	.LBB12_7
	jmp	.LBB12_8
.LBB12_1:
	xorps	%xmm0, %xmm0
	jmp	.LBB12_17
.LBB12_11:                              # %.lr.ph.preheader
	movl	$1, %eax
	subl	%edi, %eax
	movl	%eax, %ebx
	imull	%r8d, %ebx
	imull	%edx, %eax
	xorl	%r10d, %r10d
	testl	%r8d, %r8d
	movslq	%ebx, %r9
	cmovnsq	%r10, %r9
	testl	%edx, %edx
	movslq	%r8d, %r8
	movslq	%eax, %r11
	cmovnsq	%r10, %r11
	movslq	%edx, %rax
	testb	$1, %dil
	jne	.LBB12_13
# BB#12:
	xorps	%xmm0, %xmm0
	cmpl	$1, %edi
	jne	.LBB12_15
	jmp	.LBB12_17
.LBB12_13:                              # %.lr.ph.prol
	movss	(%rsi,%r11,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	(%rcx,%r9,4), %xmm1
	xorps	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	addq	%rax, %r11
	addq	%r8, %r9
	movl	$1, %r10d
	cmpl	$1, %edi
	je	.LBB12_17
.LBB12_15:                              # %.lr.ph.preheader.new
	leaq	(,%rax,8), %r14
	shlq	$2, %rax
	leaq	(%rax,%r11,4), %rax
	shlq	$2, %r11
	leaq	(,%r8,8), %rbx
	shlq	$2, %r8
	leaq	(%r8,%r9,4), %rdx
	shlq	$2, %r9
	subl	%r10d, %edi
	.p2align	4, 0x90
.LBB12_16:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%r11,%rsi), %xmm1      # xmm1 = mem[0],zero,zero,zero
	mulss	(%r9,%rcx), %xmm1
	addss	%xmm0, %xmm1
	movss	(%rax,%rsi), %xmm0      # xmm0 = mem[0],zero,zero,zero
	mulss	(%rdx,%rcx), %xmm0
	addss	%xmm1, %xmm0
	addq	%r14, %rsi
	addq	%rbx, %rcx
	addl	$-2, %edi
	jne	.LBB12_16
	jmp	.LBB12_17
.LBB12_5:
	xorl	%edx, %edx
	xorps	%xmm0, %xmm0
.LBB12_8:                               # %.lr.ph55.prol.loopexit
	cmpq	$3, %rdi
	jb	.LBB12_17
# BB#9:                                 # %.lr.ph55.preheader.new
	subq	%rdx, %rax
	leaq	12(%rcx,%rdx,4), %rcx
	leaq	12(%rsi,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB12_10:                              # %.lr.ph55
                                        # =>This Inner Loop Header: Depth=1
	movss	-12(%rdx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	-8(%rdx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	-12(%rcx), %xmm1
	addss	%xmm0, %xmm1
	mulss	-8(%rcx), %xmm2
	addss	%xmm1, %xmm2
	movss	-4(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	-4(%rcx), %xmm1
	addss	%xmm2, %xmm1
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm0
	addss	%xmm1, %xmm0
	addq	$16, %rcx
	addq	$16, %rdx
	addq	$-4, %rax
	jne	.LBB12_10
.LBB12_17:                              # %.loopexit
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end12:
	.size	ddot, .Lfunc_end12-ddot
	.cfi_endproc

	.type	main.aa,@object         # @main.aa
	.local	main.aa
	.comm	main.aa,160000,16
	.type	main.a,@object          # @main.a
	.local	main.a
	.comm	main.a,160800,16
	.type	main.b,@object          # @main.b
	.local	main.b
	.comm	main.b,800,16
	.type	main.x,@object          # @main.x
	.local	main.x
	.comm	main.x,800,16
	.type	main.ipvt,@object       # @main.ipvt
	.local	main.ipvt
	.comm	main.ipvt,800,16
	.type	main.n,@object          # @main.n
	.local	main.n
	.comm	main.n,1,4
	.type	main.i,@object          # @main.i
	.local	main.i
	.comm	main.i,4,4
	.type	main.j,@object          # @main.j
	.local	main.j
	.comm	main.j,4,4
	.type	main.ntimes,@object     # @main.ntimes
	.local	main.ntimes
	.comm	main.ntimes,4,4
	.type	main.info,@object       # @main.info
	.local	main.info
	.comm	main.info,4,4
	.type	main.lda,@object        # @main.lda
	.local	main.lda
	.comm	main.lda,1,4
	.type	main.ldaa,@object       # @main.ldaa
	.local	main.ldaa
	.comm	main.ldaa,1,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"INSERT COMPILER NAME HERE"
	.size	.L.str, 26

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"INSERT OPTIMISATION OPTIONS HERE"
	.size	.L.str.1, 33

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Rolled "
	.size	.L.str.2, 8

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Single "
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Precision Linpack Benchmark - PC Version in 'C/C++'\n\n"
	.size	.L.str.4, 54

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Compiler     %s\n"
	.size	.L.str.5, 17

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Optimisation %s\n\n"
	.size	.L.str.6, 18

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"norm resid      resid           machep"
	.size	.L.str.7, 39

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"         x[0]-1          x[n-1]-1\n"
	.size	.L.str.8, 35

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%6.1f %17.8e%17.8e%17.8e%17.8e\n\n"
	.size	.L.str.9, 33

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Times are reported for matrices of order        %5d\n"
	.size	.L.str.10, 53

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"1 pass times for array with leading dimension of%5d\n\n"
	.size	.L.str.11, 54

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"      dgefa      dgesl      total     Mflops       unit"
	.size	.L.str.12, 56

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"      ratio\n"
	.size	.L.str.13, 13

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"\nCalculating matgen overhead\n"
	.size	.L.str.14, 30

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"%10d times %6.2f seconds\n"
	.size	.L.str.15, 26

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Overhead for 1 matgen %12.5f seconds\n\n"
	.size	.L.str.16, 39

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Calculating matgen/dgefa passes for 5 seconds\n"
	.size	.L.str.17, 47

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Passes used %10d \n\n"
	.size	.L.str.18, 20

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Times for array with leading dimension of%4d\n\n"
	.size	.L.str.19, 47

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Average                          %11.2f\n"
	.size	.L.str.20, 41

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"\nCalculating matgen2 overhead\n"
	.size	.L.str.21, 31

	.type	atime.0,@object         # @atime.0
	.local	atime.0
	.comm	atime.0,60,16
	.type	atime.1,@object         # @atime.1
	.local	atime.1
	.comm	atime.1,60,16
	.type	atime.3,@object         # @atime.3
	.local	atime.3
	.comm	atime.3,60,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
