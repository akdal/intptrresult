	.text
	.file	"llubenchmark.bc"
	.globl	usage
	.p2align	4, 0x90
	.type	usage,@function
usage:                                  # @usage
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rcx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	movl	$.Lstr, %edi
	callq	puts
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$.Lstr.5, %edi
	callq	puts
	movl	$.Lstr.6, %edi
	popq	%rax
	jmp	puts                    # TAILCALL
.Lfunc_end0:
	.size	usage, .Lfunc_end0-usage
	.cfi_endproc

	.globl	allocate
	.p2align	4, 0x90
	.type	allocate,@function
allocate:                               # @allocate
	.cfi_startproc
# BB#0:
	incl	num_allocated(%rip)
	movl	$16, %edi
	jmp	malloc                  # TAILCALL
.Lfunc_end1:
	.size	allocate, .Lfunc_end1-allocate
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	1051361018              # float 0.333000004
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 112
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %r12d
	movl	$.Lstr.7, %edi
	callq	puts
	cmpl	$2, %r12d
	jl	.LBB2_1
# BB#17:                                # %.lr.ph235.lr.ph.lr.ph.lr.ph.preheader
	movl	$196, %r13d
	movss	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movl	$1, %ebx
	movl	$1000, %eax             # imm = 0x3E8
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB2_18:                               # =>This Inner Loop Header: Depth=1
	movslq	%ebx, %rax
	movq	(%r14,%rax,8), %rsi
	cmpb	$45, (%rsi)
	jne	.LBB2_20
# BB#19:                                #   in Loop: Header=BB2_18 Depth=1
	cmpb	$0, 2(%rsi)
	jne	.LBB2_20
# BB#22:                                #   in Loop: Header=BB2_18 Depth=1
	movsbl	1(%rsi), %esi
	leal	-100(%rsi), %ecx
	cmpl	$16, %ecx
	ja	.LBB2_30
# BB#23:                                #   in Loop: Header=BB2_18 Depth=1
	leal	1(%rbx), %eax
	jmpq	*.LJTI2_0(,%rcx,8)
.LBB2_28:                               #   in Loop: Header=BB2_18 Depth=1
	addl	$2, %ebx
	cltq
	movq	(%r14,%rax,8), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, element_size(%rip)
	movl	%ebx, %eax
.LBB2_29:                               # %.backedge
                                        #   in Loop: Header=BB2_18 Depth=1
	cmpl	%r12d, %eax
	movl	%eax, %ebx
	jl	.LBB2_18
	jmp	.LBB2_2
.LBB2_24:                               # %.outer155
                                        #   in Loop: Header=BB2_18 Depth=1
	addl	$2, %ebx
	cltq
	movq	(%r14,%rax,8), %rdi
	xorl	%esi, %esi
	callq	strtod
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	jmp	.LBB2_27
.LBB2_25:                               # %.outer159
                                        #   in Loop: Header=BB2_18 Depth=1
	addl	$2, %ebx
	cltq
	movq	(%r14,%rax,8), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB2_27
.LBB2_26:                               # %.outer150
                                        #   in Loop: Header=BB2_18 Depth=1
	addl	$2, %ebx
	cltq
	movq	(%r14,%rax,8), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r15
	jmp	.LBB2_27
.LBB2_31:                               # %.outer
                                        #   in Loop: Header=BB2_18 Depth=1
	addl	$2, %ebx
	cltq
	movq	(%r14,%rax,8), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r13
.LBB2_27:                               # %.outer150
                                        #   in Loop: Header=BB2_18 Depth=1
	cmpl	%r12d, %ebx
	jl	.LBB2_18
	jmp	.LBB2_2
.LBB2_1:
	movl	$1000, %eax             # imm = 0x3E8
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movss	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movl	$1, %r15d
	movl	$196, %r13d
.LBB2_2:                                # %.outer159._crit_edge
	movslq	%r13d, %rbx
	leaq	(,%rbx,8), %rdi
	callq	malloc
	movq	%rax, %rbp
	testl	%ebx, %ebx
	jle	.LBB2_4
# BB#3:                                 # %.lr.ph233.preheader
	leal	-1(%r13), %eax
	leaq	8(,%rax,8), %rdx
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	memset
.LBB2_4:                                # %.preheader149
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%r13, 24(%rsp)          # 8-byte Spill
	testl	%r13d, %r13d
	setle	%al
	testl	%r15d, %r15d
	jle	.LBB2_10
# BB#5:                                 # %.preheader149
	testb	%al, %al
	jne	.LBB2_10
# BB#6:                                 # %.preheader148.us.preheader
	movl	num_allocated(%rip), %r13d
	movl	24(%rsp), %r14d         # 4-byte Reload
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_7:                                # %.preheader148.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_8 Depth 2
	movq	%r14, %rbx
	movq	32(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_8:                                #   Parent Loop BB2_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%r13d
	movl	%r13d, num_allocated(%rip)
	movl	$16, %edi
	callq	malloc
	movq	(%rbp), %rcx
	movq	%rcx, (%rax)
	movl	$0, 8(%rax)
	movq	%rax, (%rbp)
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB2_8
# BB#9:                                 # %._crit_edge229.us
                                        #   in Loop: Header=BB2_7 Depth=1
	incl	%r12d
	cmpl	%r15d, %r12d
	jne	.LBB2_7
.LBB2_10:                               # %.preheader147
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	movl	$0, %ebp
	movq	24(%rsp), %r14          # 8-byte Reload
	jle	.LBB2_57
# BB#11:                                # %.lr.ph214
	testl	%r14d, %r14d
	jle	.LBB2_12
# BB#32:                                # %.lr.ph214.split.us.preheader
	movl	%r14d, %r12d
	movl	%r12d, %eax
	andl	$1, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorps	%xmm0, %xmm0
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_33:                               # %.lr.ph214.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_39 Depth 2
                                        #     Child Loop BB2_42 Depth 2
                                        #       Child Loop BB2_43 Depth 3
                                        #       Child Loop BB2_46 Depth 3
                                        #     Child Loop BB2_51 Depth 2
                                        #       Child Loop BB2_52 Depth 3
                                        #         Child Loop BB2_53 Depth 4
	movslq	%ebx, %rax
	imulq	$274877907, %rax, %rcx  # imm = 0x10624DD3
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$38, %rcx
	addl	%edx, %ecx
	imull	$1000, %ecx, %ecx       # imm = 0x3E8
	cmpl	%ecx, %eax
	jne	.LBB2_35
# BB#34:                                #   in Loop: Header=BB2_33 Depth=1
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	callq	printf
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
.LBB2_35:                               # %.lr.ph202.us.preheader
                                        #   in Loop: Header=BB2_33 Depth=1
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	movl	%ebx, 44(%rsp)          # 4-byte Spill
	jne	.LBB2_37
# BB#36:                                #   in Loop: Header=BB2_33 Depth=1
	xorl	%eax, %eax
	jmp	.LBB2_41
	.p2align	4, 0x90
.LBB2_37:                               # %.lr.ph202.us.prol
                                        #   in Loop: Header=BB2_33 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	jmp	.LBB2_39
	.p2align	4, 0x90
.LBB2_38:                               # %.lr.ph.us.prol
                                        #   in Loop: Header=BB2_39 Depth=2
	movl	8(%rax), %ecx
	addl	%ecx, %ebp
	incl	%ecx
	movl	%ecx, 8(%rax)
.LBB2_39:                               # %.lr.ph.us.prol
                                        #   Parent Loop BB2_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB2_38
# BB#40:                                #   in Loop: Header=BB2_33 Depth=1
	movl	$1, %eax
.LBB2_41:                               # %.lr.ph202.us.prol.loopexit
                                        #   in Loop: Header=BB2_33 Depth=1
	cmpl	$1, %r14d
	movq	32(%rsp), %rbx          # 8-byte Reload
	je	.LBB2_49
	.p2align	4, 0x90
.LBB2_42:                               # %.lr.ph202.us
                                        #   Parent Loop BB2_33 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_43 Depth 3
                                        #       Child Loop BB2_46 Depth 3
	movq	(%rbx,%rax,8), %rcx
	testq	%rcx, %rcx
	je	.LBB2_45
	.p2align	4, 0x90
.LBB2_43:                               # %.lr.ph.us
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_42 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	8(%rcx), %edx
	addl	%edx, %ebp
	incl	%edx
	movl	%edx, 8(%rcx)
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB2_43
.LBB2_45:                               # %._crit_edge.us
                                        #   in Loop: Header=BB2_42 Depth=2
	movq	8(%rbx,%rax,8), %rcx
	testq	%rcx, %rcx
	je	.LBB2_48
	.p2align	4, 0x90
.LBB2_46:                               # %.lr.ph.us.1
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_42 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	8(%rcx), %edx
	addl	%edx, %ebp
	incl	%edx
	movl	%edx, 8(%rcx)
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB2_46
.LBB2_48:                               # %._crit_edge.us.1
                                        #   in Loop: Header=BB2_42 Depth=2
	addq	$2, %rax
	cmpq	%r12, %rax
	jne	.LBB2_42
.LBB2_49:                               # %._crit_edge203.us
                                        #   in Loop: Header=BB2_33 Depth=1
	addss	8(%rsp), %xmm0          # 4-byte Folded Reload
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	cvttss2si	%xmm0, %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	movss	%xmm0, 40(%rsp)         # 4-byte Spill
	testl	%eax, %eax
	jle	.LBB2_56
# BB#50:                                # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB2_33 Depth=1
	movslq	%eax, %r15
	movl	num_allocated(%rip), %r14d
	.p2align	4, 0x90
.LBB2_51:                               # %.preheader.us.us
                                        #   Parent Loop BB2_33 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_52 Depth 3
                                        #         Child Loop BB2_53 Depth 4
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_52:                               #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_51 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_53 Depth 4
	incl	%r14d
	movl	%r14d, num_allocated(%rip)
	movl	$16, %edi
	callq	malloc
	leal	(%r13,%r15), %ecx
	movl	%ecx, 8(%rax)
	movq	(%rbx,%r13,8), %rcx
	.p2align	4, 0x90
.LBB2_53:                               #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_51 Depth=2
                                        #       Parent Loop BB2_52 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rcx, %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.LBB2_53
# BB#54:                                #   in Loop: Header=BB2_52 Depth=3
	movq	%rax, (%rdx)
	movq	$0, (%rax)
	incq	%r13
	cmpq	%r12, %r13
	jne	.LBB2_52
# BB#55:                                # %._crit_edge207.us.us
                                        #   in Loop: Header=BB2_51 Depth=2
	cmpq	$1, %r15
	leaq	-1(%r15), %r15
	jg	.LBB2_51
.LBB2_56:                               # %._crit_edge210.us
                                        #   in Loop: Header=BB2_33 Depth=1
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	subss	40(%rsp), %xmm0         # 4-byte Folded Reload
	movl	44(%rsp), %ebx          # 4-byte Reload
	incl	%ebx
	cmpl	16(%rsp), %ebx          # 4-byte Folded Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	jne	.LBB2_33
	jmp	.LBB2_57
.LBB2_20:
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	printf
.LBB2_21:
	movq	(%r14), %rsi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr, %edi
	callq	puts
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$.Lstr.5, %edi
	callq	puts
	movl	$.Lstr.6, %edi
	callq	puts
	movl	$-1, %eax
	jmp	.LBB2_58
.LBB2_12:                               # %.lr.ph214.split.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_13:                               # %.lr.ph214.split
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ebx, %rax
	imulq	$274877907, %rax, %rcx  # imm = 0x10624DD3
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$38, %rcx
	addl	%edx, %ecx
	imull	$1000, %ecx, %ecx       # imm = 0x3E8
	cmpl	%ecx, %eax
	jne	.LBB2_15
# BB#14:                                #   in Loop: Header=BB2_13 Depth=1
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
.LBB2_15:                               # %._crit_edge210
                                        #   in Loop: Header=BB2_13 Depth=1
	incl	%ebx
	cmpl	%ebx, 16(%rsp)          # 4-byte Folded Reload
	jne	.LBB2_13
# BB#16:
	xorl	%ebp, %ebp
.LBB2_57:                               # %._crit_edge215
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
	movl	num_allocated(%rip), %esi
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	printf
	xorl	%eax, %eax
.LBB2_58:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_30:
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	jmp	.LBB2_21
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_29
	.quad	.LBB2_30
	.quad	.LBB2_30
	.quad	.LBB2_24
	.quad	.LBB2_30
	.quad	.LBB2_25
	.quad	.LBB2_30
	.quad	.LBB2_30
	.quad	.LBB2_26
	.quad	.LBB2_30
	.quad	.LBB2_31
	.quad	.LBB2_30
	.quad	.LBB2_30
	.quad	.LBB2_30
	.quad	.LBB2_30
	.quad	.LBB2_28
	.quad	.LBB2_29

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s:\n"
	.size	.L.str, 5

	.type	free_list,@object       # @free_list
	.bss
	.globl	free_list
	.p2align	3
free_list:
	.quad	0
	.size	free_list, 8

	.type	next_free,@object       # @next_free
	.data
	.globl	next_free
	.p2align	2
next_free:
	.long	127                     # 0x7f
	.size	next_free, 4

	.type	element_size,@object    # @element_size
	.globl	element_size
	.p2align	2
element_size:
	.long	32                      # 0x20
	.size	element_size, 4

	.type	num_allocated,@object   # @num_allocated
	.bss
	.globl	num_allocated
	.p2align	2
num_allocated:
	.long	0                       # 0x0
	.size	num_allocated, 4

	.type	.L.str.9,@object        # @.str.9
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.9:
	.asciz	"parse error in %s\n"
	.size	.L.str.9, 19

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"unrecognized option: %c\n"
	.size	.L.str.10, 25

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%d\n"
	.size	.L.str.11, 4

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"output = %d\n"
	.size	.L.str.12, 13

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"num allocated %d\n"
	.size	.L.str.13, 18

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"-i <number of (I)terations>"
	.size	.Lstr, 28

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"[-l <initial (L)ength of list, in elements>] (default 1)"
	.size	.Lstr.1, 57

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"[-n <(N)umber of lists>] (default 1 list)"
	.size	.Lstr.2, 42

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"[-s <(S)ize of element>] (default 32 bytes)"
	.size	.Lstr.3, 44

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"[-g <(G)rowth rate per list, in elements per iteration>] (default 0)"
	.size	.Lstr.4, 69

	.type	.Lstr.5,@object         # @str.5
	.p2align	4
.Lstr.5:
	.asciz	"[-d] ((D)irty each element during traversal, default off)"
	.size	.Lstr.5, 58

	.type	.Lstr.6,@object         # @str.6
	.p2align	4
.Lstr.6:
	.asciz	"[-t] (insert at (T)ail of list, default off)"
	.size	.Lstr.6, 45

	.type	.Lstr.7,@object         # @str.7
	.p2align	4
.Lstr.7:
	.asciz	"This benchmark modified to not use hard coded pool allocation!"
	.size	.Lstr.7, 63


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
