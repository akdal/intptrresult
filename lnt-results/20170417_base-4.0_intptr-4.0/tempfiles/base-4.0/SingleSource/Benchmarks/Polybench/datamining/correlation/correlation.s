	.text
	.file	"correlation.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4652007308841189376     # double 1000
.LCPI6_1:
	.quad	4608083138725491507     # double 1.2
.LCPI6_3:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_2:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 96
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8000000, %edx          # imm = 0x7A1200
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_32
# BB#1:
	movq	8(%rsp), %r14
	testq	%r14, %r14
	je	.LBB6_32
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8000000, %edx          # imm = 0x7A1200
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_32
# BB#3:                                 # %polybench_alloc_data.exit
	movq	8(%rsp), %r15
	testq	%r15, %r15
	je	.LBB6_32
# BB#4:                                 # %polybench_alloc_data.exit42
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8000000, %edx          # imm = 0x7A1200
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_32
# BB#5:                                 # %polybench_alloc_data.exit42
	movq	8(%rsp), %r12
	testq	%r12, %r12
	je	.LBB6_32
# BB#6:                                 # %polybench_alloc_data.exit44
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8000, %edx             # imm = 0x1F40
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_32
# BB#7:                                 # %polybench_alloc_data.exit44
	movq	8(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB6_32
# BB#8:                                 # %polybench_alloc_data.exit46
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8000, %edx             # imm = 0x1F40
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_32
# BB#9:                                 # %polybench_alloc_data.exit46
	movq	8(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB6_32
# BB#10:                                # %polybench_alloc_data.exit48
	leaq	8(%r14), %rax
	xorl	%ecx, %ecx
	movsd	.LCPI6_0(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB6_11:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_12 Depth 2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movq	%rax, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_12:                               #   Parent Loop BB6_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	mulsd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movsd	%xmm1, -8(%rdx)
	movl	%esi, %edi
	orl	$1, %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movsd	%xmm1, (%rdx)
	addq	$2, %rsi
	addq	$16, %rdx
	cmpq	$1000, %rsi             # imm = 0x3E8
	jne	.LBB6_12
# BB#13:                                #   in Loop: Header=BB6_11 Depth=1
	incq	%rcx
	addq	$8000, %rax             # imm = 0x1F40
	cmpq	$1000, %rcx             # imm = 0x3E8
	jne	.LBB6_11
# BB#14:                                # %init_array.exit
	movsd	.LCPI6_1(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	movq	%rbp, %rcx
	callq	kernel_correlation
	movsd	.LCPI6_0(%rip), %xmm2   # xmm2 = mem[0],zero
	leaq	8(%r14), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_15:                               # %.preheader.i50
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_16 Depth 2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movq	%rax, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_16:                               #   Parent Loop BB6_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	mulsd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movsd	%xmm1, -8(%rdx)
	movl	%esi, %edi
	orl	$1, %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movsd	%xmm1, (%rdx)
	addq	$2, %rsi
	addq	$16, %rdx
	cmpq	$1000, %rsi             # imm = 0x3E8
	jne	.LBB6_16
# BB#17:                                #   in Loop: Header=BB6_15 Depth=1
	incq	%rcx
	addq	$8000, %rax             # imm = 0x1F40
	cmpq	$1000, %rcx             # imm = 0x3E8
	jne	.LBB6_15
# BB#18:                                # %init_array.exit56
	movsd	.LCPI6_1(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	%rbp, %rcx
	callq	kernel_correlation
	xorl	%edx, %edx
	movl	$1, %eax
	movapd	.LCPI6_2(%rip), %xmm2   # xmm2 = [nan,nan]
	movsd	.LCPI6_3(%rip), %xmm3   # xmm3 = mem[0],zero
.LBB6_19:                               # %.preheader.i57
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_20 Depth 2
	movq	%rax, %rsi
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB6_20:                               #   Parent Loop BB6_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%r15,%rsi,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%r12,%rsi,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_21
# BB#23:                                #   in Loop: Header=BB6_20 Depth=2
	movsd	(%r15,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r12,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_22
# BB#24:                                #   in Loop: Header=BB6_20 Depth=2
	addq	$2, %rsi
	leaq	2(%rcx), %rdi
	incq	%rcx
	cmpq	$1000, %rcx             # imm = 0x3E8
	movq	%rdi, %rcx
	jl	.LBB6_20
# BB#25:                                #   in Loop: Header=BB6_19 Depth=1
	incq	%rdx
	addq	$1000, %rax             # imm = 0x3E8
	cmpq	$1000, %rdx             # imm = 0x3E8
	jl	.LBB6_19
# BB#26:                                # %check_FP.exit
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movl	$16001, %edi            # imm = 0x3E81
	callq	malloc
	movq	%rax, %rbp
	movb	$0, 16000(%rbp)
	xorl	%eax, %eax
	movq	%r12, %r13
	.p2align	4, 0x90
.LBB6_27:                               # %.preheader.i61
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_28 Depth 2
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r13, %rax
	movl	$15, %ecx
	.p2align	4, 0x90
.LBB6_28:                               #   Parent Loop BB6_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdx
	movl	%edx, %ebx
	andb	$15, %bl
	orb	$48, %bl
	movb	%bl, -15(%rbp,%rcx)
	movb	%bl, -14(%rbp,%rcx)
	movq	%rdx, %rsi
	shrq	$8, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -13(%rbp,%rcx)
	movb	%sil, -12(%rbp,%rcx)
	movq	%rdx, %rsi
	shrq	$16, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -11(%rbp,%rcx)
	movb	%sil, -10(%rbp,%rcx)
	movl	%edx, %esi
	shrl	$24, %esi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -9(%rbp,%rcx)
	movb	%sil, -8(%rbp,%rcx)
	movq	%rdx, %rsi
	shrq	$32, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -7(%rbp,%rcx)
	movb	%sil, -6(%rbp,%rcx)
	movq	%rdx, %rsi
	shrq	$40, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -5(%rbp,%rcx)
	movb	%sil, -4(%rbp,%rcx)
	movq	%rdx, %rsi
	shrq	$48, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -3(%rbp,%rcx)
	movb	%sil, -2(%rbp,%rcx)
	shrq	$56, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, -1(%rbp,%rcx)
	movb	%dl, (%rbp,%rcx)
	addq	$16, %rcx
	addq	$8, %rax
	cmpq	$16015, %rcx            # imm = 0x3E8F
	jne	.LBB6_28
# BB#29:                                #   in Loop: Header=BB6_27 Depth=1
	movq	stderr(%rip), %rsi
	movq	%rbp, %rdi
	callq	fputs
	movq	32(%rsp), %rax          # 8-byte Reload
	incq	%rax
	addq	$8000, %r13             # imm = 0x1F40
	cmpq	$1000, %rax             # imm = 0x3E8
	jne	.LBB6_27
# BB#30:                                # %print_array.exit
	movq	%rbp, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	xorl	%eax, %eax
	jmp	.LBB6_31
.LBB6_21:                               # %check_FP.exit.threadsplit
	decq	%rcx
.LBB6_22:                               # %check_FP.exit.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_3(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %r8d
	movl	%ecx, %r9d
	callq	fprintf
	movl	$1, %eax
.LBB6_31:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_32:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI7_0:
	.quad	4591870180174331904     # double 0.10000000149011612
.LCPI7_1:
	.quad	4607182418800017408     # double 1
	.text
	.p2align	4, 0x90
	.type	kernel_correlation,@function
kernel_correlation:                     # @kernel_correlation
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 96
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%rsi, %r14
	movq	%rdi, %r15
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB7_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_2 Depth 2
	movq	$0, (%r13,%rax,8)
	xorpd	%xmm0, %xmm0
	movq	%rcx, %rdx
	movl	$1000, %esi             # imm = 0x3E8
	.p2align	4, 0x90
.LBB7_2:                                #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rdx), %xmm0
	movsd	%xmm0, (%r13,%rax,8)
	addsd	8000(%rdx), %xmm0
	movsd	%xmm0, (%r13,%rax,8)
	addsd	16000(%rdx), %xmm0
	movsd	%xmm0, (%r13,%rax,8)
	addsd	24000(%rdx), %xmm0
	movsd	%xmm0, (%r13,%rax,8)
	addsd	32000(%rdx), %xmm0
	movsd	%xmm0, (%r13,%rax,8)
	addq	$40000, %rdx            # imm = 0x9C40
	addq	$-5, %rsi
	jne	.LBB7_2
# BB#3:                                 #   in Loop: Header=BB7_1 Depth=1
	divsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, (%r13,%rax,8)
	incq	%rax
	addq	$8, %rcx
	cmpq	$1000, %rax             # imm = 0x3E8
	jne	.LBB7_1
# BB#4:                                 # %.preheader3.preheader
	xorl	%ebx, %ebx
	movsd	.LCPI7_0(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI7_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB7_5:                                # %.preheader3
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_6 Depth 2
	movq	$0, (%r12,%rbx,8)
	xorpd	%xmm0, %xmm0
	movl	$1000, %eax             # imm = 0x3E8
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB7_6:                                #   Parent Loop BB7_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	subsd	(%r13,%rbx,8), %xmm1
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%r12,%rbx,8)
	movsd	8000(%rcx), %xmm0       # xmm0 = mem[0],zero
	subsd	(%r13,%rbx,8), %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, (%r12,%rbx,8)
	addq	$16000, %rcx            # imm = 0x3E80
	addq	$-2, %rax
	jne	.LBB7_6
# BB#7:                                 #   in Loop: Header=BB7_5 Depth=1
	divsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, (%r12,%rbx,8)
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB7_9
# BB#8:                                 # %call.sqrt
                                        #   in Loop: Header=BB7_5 Depth=1
	callq	sqrt
	movsd	.LCPI7_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI7_0(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB7_9:                                # %.split
                                        #   in Loop: Header=BB7_5 Depth=1
	movapd	%xmm1, %xmm0
	cmpnlesd	%xmm2, %xmm0
	andpd	%xmm0, %xmm1
	andnpd	%xmm3, %xmm0
	orpd	%xmm1, %xmm0
	movsd	%xmm0, (%r12,%rbx,8)
	incq	%rbx
	addq	$8, %rbp
	cmpq	$1000, %rbx             # imm = 0x3E8
	jne	.LBB7_5
# BB#10:                                # %.preheader1.preheader
	xorl	%eax, %eax
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	xorps	%xmm2, %xmm2
	sqrtsd	%xmm0, %xmm2
	movq	%r15, %rbp
	movsd	%xmm2, 32(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB7_11:                               # %.preheader1
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_12 Depth 2
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_12:                               #   Parent Loop BB7_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	subsd	(%r13,%rbx,8), %xmm0
	movsd	%xmm0, (%rbp)
	ucomisd	%xmm2, %xmm2
	movapd	%xmm2, %xmm0
	jnp	.LBB7_14
# BB#13:                                # %call.sqrt90
                                        #   in Loop: Header=BB7_12 Depth=2
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sqrt
	movsd	32(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB7_14:                               # %.split89
                                        #   in Loop: Header=BB7_12 Depth=2
	mulsd	(%r12,%rbx,8), %xmm0
	movsd	(%rbp), %xmm1           # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, (%rbp)
	incq	%rbx
	addq	$8, %rbp
	cmpq	$1000, %rbx             # imm = 0x3E8
	jne	.LBB7_12
# BB#15:                                #   in Loop: Header=BB7_11 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	incq	%rax
	movq	16(%rsp), %rbp          # 8-byte Reload
	addq	$8000, %rbp             # imm = 0x1F40
	cmpq	$1000, %rax             # imm = 0x3E8
	jne	.LBB7_11
# BB#16:                                # %.lr.ph.preheader.preheader
	leaq	8008(%r15), %r10
	movl	$1, %r11d
	xorl	%r9d, %r9d
	movabsq	$4607182418800017408, %r8 # imm = 0x3FF0000000000000
	.p2align	4, 0x90
.LBB7_18:                               # %.lr.ph.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_19 Depth 2
                                        #       Child Loop BB7_20 Depth 3
	movq	%r9, %r12
	imulq	$8000, %r12, %rbp       # imm = 0x1F40
	addq	%r14, %rbp
	movq	%r8, (%rbp,%r12,8)
	leaq	1(%r12), %r9
	movq	%r10, %rdx
	movq	%r11, %rcx
	.p2align	4, 0x90
.LBB7_19:                               # %.lr.ph
                                        #   Parent Loop BB7_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_20 Depth 3
	leaq	(%rbp,%rcx,8), %rsi
	movq	$0, (%rbp,%rcx,8)
	xorpd	%xmm0, %xmm0
	movl	$1000, %eax             # imm = 0x3E8
	movq	%rdx, %rbx
	movq	%r15, %rdi
	.p2align	4, 0x90
.LBB7_20:                               #   Parent Loop BB7_18 Depth=1
                                        #     Parent Loop BB7_19 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rdi), %xmm1           # xmm1 = mem[0],zero
	mulsd	-8000(%rbx), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rsi)
	movsd	8000(%rdi), %xmm0       # xmm0 = mem[0],zero
	mulsd	(%rbx), %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, (%rsi)
	addq	$16000, %rdi            # imm = 0x3E80
	addq	$16000, %rbx            # imm = 0x3E80
	addq	$-2, %rax
	jne	.LBB7_20
# BB#21:                                #   in Loop: Header=BB7_19 Depth=2
	imulq	$8000, %rcx, %rax       # imm = 0x1F40
	addq	%r14, %rax
	movsd	%xmm0, (%rax,%r12,8)
	incq	%rcx
	addq	$8, %rdx
	cmpq	$1000, %rcx             # imm = 0x3E8
	jne	.LBB7_19
# BB#17:                                # %.loopexit
                                        #   in Loop: Header=BB7_18 Depth=1
	incq	%r11
	addq	$8, %r15
	addq	$8, %r10
	cmpq	$999, %r9               # imm = 0x3E7
	jne	.LBB7_18
# BB#22:
	movq	%r8, 7999992(%r14)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	kernel_correlation, .Lfunc_end7-kernel_correlation
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A[%d][%d] = %lf and B[%d][%d] = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 76


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
