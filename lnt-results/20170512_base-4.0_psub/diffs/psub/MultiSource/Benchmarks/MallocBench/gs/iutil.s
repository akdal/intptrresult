	.text
	.file	"iutil.bc"
	.globl	refcpy
	.p2align	4, 0x90
	.type	refcpy,@function
refcpy:                                 # @refcpy
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	testl	%edx, %edx
	je	.LBB0_6
# BB#1:                                 # %.lr.ph.preheader
	leal	-1(%rdx), %eax
	movl	%edx, %ecx
	andl	$7, %ecx
	je	.LBB0_4
# BB#2:                                 # %.lr.ph.prol.preheader
	negl	%ecx
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	decl	%edx
	movups	(%rsi), %xmm0
	addq	$16, %rsi
	movups	%xmm0, (%rdi)
	addq	$16, %rdi
	incl	%ecx
	jne	.LBB0_3
.LBB0_4:                                # %.lr.ph.prol.loopexit
	cmpl	$7, %eax
	jb	.LBB0_6
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rsi), %xmm0
	movups	%xmm0, (%rdi)
	movups	16(%rsi), %xmm0
	movups	%xmm0, 16(%rdi)
	movups	32(%rsi), %xmm0
	movups	%xmm0, 32(%rdi)
	movups	48(%rsi), %xmm0
	movups	%xmm0, 48(%rdi)
	movups	64(%rsi), %xmm0
	movups	%xmm0, 64(%rdi)
	movups	80(%rsi), %xmm0
	movups	%xmm0, 80(%rdi)
	movups	96(%rsi), %xmm0
	movups	%xmm0, 96(%rdi)
	movups	112(%rsi), %xmm0
	movups	%xmm0, 112(%rdi)
	subq	$-128, %rdi
	subq	$-128, %rsi
	addl	$-8, %edx
	jne	.LBB0_5
.LBB0_6:                                # %._crit_edge
	retq
.Lfunc_end0:
	.size	refcpy, .Lfunc_end0-refcpy
	.cfi_endproc

	.globl	obj_eq
	.p2align	4, 0x90
	.type	obj_eq,@function
obj_eq:                                 # @obj_eq
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 48
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movzwl	8(%rbx), %ecx
	movl	%ecx, %eax
	andl	$252, %eax
	movl	%eax, %esi
	shrl	$2, %esi
	cmpl	$60, %eax
	movl	$9, %eax
	cmoval	%eax, %esi
	movzwl	8(%r14), %edx
	andl	$252, %edx
	movl	%edx, %edi
	shrl	$2, %edi
	cmpl	$60, %edx
	cmoval	%eax, %edi
	cmpl	%edi, %esi
	jne	.LBB1_1
.LBB1_13:
	movzwl	8(%rbx), %eax
	andl	$252, %eax
	cmpl	$60, %eax
	movb	$9, %cl
	ja	.LBB1_15
# BB#14:
	shrl	$2, %eax
	movl	%eax, %ecx
.LBB1_15:
	cmpb	$15, %cl
	ja	.LBB1_29
# BB#16:
	movb	$1, %al
	movzbl	%cl, %ecx
	jmpq	*.LJTI1_1(,%rcx,8)
.LBB1_28:
	movq	(%rbx), %rax
	cmpq	(%r14), %rax
	sete	%al
	jmp	.LBB1_30
.LBB1_1:
	shrb	$2, %cl
	xorl	%eax, %eax
	addb	$-5, %cl
	cmpb	$8, %cl
	ja	.LBB1_30
# BB#2:
	movzbl	%cl, %ecx
	jmpq	*.LJTI1_0(,%rcx,8)
.LBB1_3:
	movzwl	%dx, %eax
	cmpl	$44, %eax
	jne	.LBB1_29
# BB#4:
	cvtsi2ssq	(%rbx), %xmm0
	jmp	.LBB1_5
.LBB1_9:
	movzwl	%dx, %eax
	cmpl	$52, %eax
	jne	.LBB1_29
# BB#10:
	movq	%rsp, %r15
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	name_string_ref
	movq	%r15, %rbx
	jmp	.LBB1_13
.LBB1_7:
	movzwl	%dx, %eax
	cmpl	$20, %eax
	jne	.LBB1_29
# BB#8:
	cvtsi2ssq	(%r14), %xmm0
	cmpeqss	(%rbx), %xmm0
	jmp	.LBB1_6
.LBB1_11:
	movzwl	%dx, %eax
	cmpl	$28, %eax
	jne	.LBB1_29
# BB#12:
	movq	%rsp, %r15
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	name_string_ref
	movq	%r15, %r14
	jmp	.LBB1_13
.LBB1_17:
	movq	(%rbx), %rax
	cmpq	(%r14), %rax
	jne	.LBB1_29
# BB#18:
	movzwl	10(%rbx), %eax
	cmpw	10(%r14), %ax
	sete	%al
	jmp	.LBB1_30
.LBB1_19:
	movzwl	(%rbx), %eax
	cmpw	(%r14), %ax
	sete	%al
	jmp	.LBB1_30
.LBB1_21:
	movq	(%rbx), %rdx
	movzwl	10(%rbx), %r8d
	movq	(%r14), %rdi
	movzwl	10(%r14), %esi
	movl	%esi, %eax
	notl	%eax
	movl	%r8d, %ecx
	notl	%ecx
	cmpl	%ecx, %eax
	cmoval	%eax, %ecx
	incl	%ecx
	.p2align	4, 0x90
.LBB1_22:                               # =>This Inner Loop Header: Depth=1
	testl	%ecx, %ecx
	je	.LBB1_24
# BB#23:                                #   in Loop: Header=BB1_22 Depth=1
	movzbl	(%rdx), %ebx
	incq	%rdx
	incl	%ecx
	xorl	%eax, %eax
	cmpb	(%rdi), %bl
	leaq	1(%rdi), %rdi
	je	.LBB1_22
	jmp	.LBB1_30
.LBB1_20:
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
.LBB1_5:                                # %bytes_compare.exit
	cmpeqss	(%r14), %xmm0
.LBB1_6:                                # %bytes_compare.exit
	movd	%xmm0, %eax
	andl	$1, %eax
	jmp	.LBB1_30
.LBB1_25:
	movq	(%rbx), %rax
	movq	(%r14), %rcx
	movzwl	(%rax), %edx
	cmpw	(%rcx), %dx
	jne	.LBB1_29
# BB#26:
	movzwl	2(%rax), %edx
	cmpw	2(%rcx), %dx
	jne	.LBB1_29
# BB#27:
	movzwl	4(%rax), %eax
	cmpw	4(%rcx), %ax
	sete	%al
	jmp	.LBB1_30
.LBB1_29:
	xorl	%eax, %eax
.LBB1_30:                               # %bytes_compare.exit
	movzbl	%al, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB1_24:
	cmpw	%si, %r8w
	sete	%al
	jmp	.LBB1_30
.Lfunc_end1:
	.size	obj_eq, .Lfunc_end1-obj_eq
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_3
	.quad	.LBB1_30
	.quad	.LBB1_9
	.quad	.LBB1_30
	.quad	.LBB1_30
	.quad	.LBB1_30
	.quad	.LBB1_7
	.quad	.LBB1_30
	.quad	.LBB1_11
.LJTI1_1:
	.quad	.LBB1_17
	.quad	.LBB1_19
	.quad	.LBB1_28
	.quad	.LBB1_28
	.quad	.LBB1_28
	.quad	.LBB1_28
	.quad	.LBB1_30
	.quad	.LBB1_28
	.quad	.LBB1_30
	.quad	.LBB1_28
	.quad	.LBB1_17
	.quad	.LBB1_20
	.quad	.LBB1_28
	.quad	.LBB1_21
	.quad	.LBB1_25
	.quad	.LBB1_28

	.text
	.globl	bytes_compare
	.p2align	4, 0x90
	.type	bytes_compare,@function
bytes_compare:                          # @bytes_compare
	.cfi_startproc
# BB#0:
	movl	%ecx, %r8d
	notl	%r8d
	movl	%esi, %eax
	notl	%eax
	cmpl	%eax, %r8d
	cmoval	%r8d, %eax
	incl	%eax
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	testl	%eax, %eax
	je	.LBB2_4
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
	movzbl	(%rdx), %r8d
	incq	%rdx
	incl	%eax
	cmpb	%r8b, (%rdi)
	leaq	1(%rdi), %rdi
	je	.LBB2_1
# BB#3:
	movl	$-1, %ecx
	movl	$1, %eax
	cmovbl	%ecx, %eax
	retq
.LBB2_4:
	xorl	%edx, %edx
	cmpl	%ecx, %esi
	movl	$-1, %ecx
	movl	$1, %eax
	cmovbl	%ecx, %eax
	cmovel	%edx, %eax
	retq
.Lfunc_end2:
	.size	bytes_compare, .Lfunc_end2-bytes_compare
	.cfi_endproc

	.globl	string_hash
	.p2align	4, 0x90
	.type	string_hash,@function
string_hash:                            # @string_hash
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	testl	%esi, %esi
	je	.LBB3_1
# BB#2:                                 # %.lr.ph.preheader
	leal	-1(%rsi), %r8d
	movl	%esi, %edx
	andl	$3, %edx
	je	.LBB3_3
# BB#4:                                 # %.lr.ph.prol.preheader
	negl	%edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	decl	%esi
	imull	$19, %eax, %ecx
	movzbl	(%rdi), %eax
	incq	%rdi
	addl	%ecx, %eax
	incl	%edx
	jne	.LBB3_5
	jmp	.LBB3_6
.LBB3_1:
	xorl	%eax, %eax
	retq
.LBB3_3:
	xorl	%eax, %eax
.LBB3_6:                                # %.lr.ph.prol.loopexit
	cmpl	$3, %r8d
	jb	.LBB3_8
	.p2align	4, 0x90
.LBB3_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	imull	$19, %eax, %eax
	movzbl	(%rdi), %ecx
	addl	%eax, %ecx
	imull	$19, %ecx, %eax
	movzbl	1(%rdi), %ecx
	addl	%eax, %ecx
	imull	$19, %ecx, %eax
	movzbl	2(%rdi), %ecx
	addl	%eax, %ecx
	imull	$19, %ecx, %ecx
	movzbl	3(%rdi), %eax
	addl	%ecx, %eax
	addq	$4, %rdi
	addl	$-4, %esi
	jne	.LBB3_7
.LBB3_8:                                # %._crit_edge
	retq
.Lfunc_end3:
	.size	string_hash, .Lfunc_end3-string_hash
	.cfi_endproc

	.globl	string_to_ref
	.p2align	4, 0x90
	.type	string_to_ref,@function
string_to_ref:                          # @string_to_ref
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 48
.Lcfi12:
	.cfi_offset %rbx, -40
.Lcfi13:
	.cfi_offset %r12, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	callq	strlen
	movq	%rax, %r12
	movl	$1, %esi
	movl	%r12d, %edi
	movq	%rbx, %rdx
	callq	alloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB4_1
# BB#2:
	movl	%r12d, %edx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	memcpy
	movq	%rbx, (%r14)
	movw	$822, 8(%r14)           # imm = 0x336
	movw	%r12w, 10(%r14)
	xorl	%eax, %eax
	jmp	.LBB4_3
.LBB4_1:
	movl	$-25, %eax
.LBB4_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	string_to_ref, .Lfunc_end4-string_to_ref
	.cfi_endproc

	.globl	ref_to_string
	.p2align	4, 0x90
	.type	ref_to_string,@function
ref_to_string:                          # @ref_to_string
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %r15, -16
	movq	%rsi, %rax
	movq	%rdi, %r15
	movzwl	10(%r15), %r14d
	leal	1(%r14), %edi
	movl	$1, %esi
	movq	%rax, %rdx
	callq	alloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB5_1
# BB#2:
	movq	(%r15), %rsi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movb	$0, (%rbx,%r14)
	jmp	.LBB5_3
.LBB5_1:
	xorl	%ebx, %ebx
.LBB5_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	ref_to_string, .Lfunc_end5-ref_to_string
	.cfi_endproc

	.globl	num_params
	.p2align	4, 0x90
	.type	num_params,@function
num_params:                             # @num_params
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB6_1
# BB#2:                                 # %.lr.ph
	testq	%rdx, %rdx
	je	.LBB6_3
# BB#9:                                 # %.lr.ph.split.split.preheader
	movslq	%esi, %rsi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_10:                               # %.lr.ph.split.split
                                        # =>This Inner Loop Header: Depth=1
	addl	%eax, %eax
	movzbl	8(%rdi), %ecx
	shrb	$2, %cl
	cmpb	$5, %cl
	je	.LBB6_14
# BB#11:                                # %.lr.ph.split.split
                                        #   in Loop: Header=BB6_10 Depth=1
	cmpb	$11, %cl
	jne	.LBB6_12
# BB#13:                                #   in Loop: Header=BB6_10 Depth=1
	movl	(%rdi), %ecx
	movl	%ecx, -4(%rdx,%rsi,4)
	jmp	.LBB6_15
	.p2align	4, 0x90
.LBB6_14:                               #   in Loop: Header=BB6_10 Depth=1
	cvtsi2ssq	(%rdi), %xmm0
	movss	%xmm0, -4(%rdx,%rsi,4)
	orl	$1, %eax
.LBB6_15:                               #   in Loop: Header=BB6_10 Depth=1
	decq	%rsi
	addq	$-16, %rdi
	testq	%rsi, %rsi
	jg	.LBB6_10
	jmp	.LBB6_16
.LBB6_1:
	xorl	%eax, %eax
	retq
.LBB6_3:                                # %.lr.ph.split.us.split.us.preheader
	incl	%esi
	addq	$8, %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_4:                                # %.lr.ph.split.us.split.us
                                        # =>This Inner Loop Header: Depth=1
	addl	%eax, %eax
	movzbl	(%rdi), %ecx
	shrb	$2, %cl
	cmpb	$11, %cl
	je	.LBB6_8
# BB#5:                                 # %.lr.ph.split.us.split.us
                                        #   in Loop: Header=BB6_4 Depth=1
	cmpb	$5, %cl
	jne	.LBB6_6
# BB#7:                                 #   in Loop: Header=BB6_4 Depth=1
	orl	$1, %eax
.LBB6_8:                                #   in Loop: Header=BB6_4 Depth=1
	decl	%esi
	addq	$-16, %rdi
	cmpl	$1, %esi
	jg	.LBB6_4
.LBB6_16:                               # %._crit_edge
	retq
.LBB6_12:
	movl	$-20, %eax
	retq
.LBB6_6:
	movl	$-20, %eax
	retq
.Lfunc_end6:
	.size	num_params, .Lfunc_end6-num_params
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI7_0:
	.long	1065353216              # float 1
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI7_1:
	.quad	4607182463836013682     # double 1.0000100000000001
.LCPI7_2:
	.quad	-4691351453243840271    # double -1.0000000000000001E-5
	.text
	.globl	real_param
	.p2align	4, 0x90
	.type	real_param,@function
real_param:                             # @real_param
	.cfi_startproc
# BB#0:
	movb	8(%rdi), %cl
	shrb	$2, %cl
	cmpb	$11, %cl
	je	.LBB7_3
# BB#1:
	movl	$-20, %eax
	cmpb	$5, %cl
	jne	.LBB7_12
# BB#2:
	cvtsi2ssq	(%rdi), %xmm1
	testl	%edx, %edx
	jne	.LBB7_6
	jmp	.LBB7_5
.LBB7_3:
	movss	(%rdi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	testl	%edx, %edx
	je	.LBB7_5
.LBB7_6:
	cvtss2sd	%xmm1, %xmm2
	xorps	%xmm0, %xmm0
	ucomiss	%xmm1, %xmm0
	jbe	.LBB7_8
# BB#7:
	movl	$-15, %eax
	movsd	.LCPI7_2(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm2, %xmm1
	jbe	.LBB7_11
	jmp	.LBB7_12
.LBB7_5:
	movaps	%xmm1, %xmm0
	jmp	.LBB7_11
.LBB7_8:
	ucomiss	.LCPI7_0(%rip), %xmm1
	movaps	%xmm1, %xmm0
	jbe	.LBB7_11
# BB#9:
	movl	$-15, %eax
	ucomisd	.LCPI7_1(%rip), %xmm2
	ja	.LBB7_12
# BB#10:
	movss	.LCPI7_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB7_11:
	movss	%xmm0, (%rsi)
	xorl	%eax, %eax
.LBB7_12:
	retq
.Lfunc_end7:
	.size	real_param, .Lfunc_end7-real_param
	.cfi_endproc

	.globl	read_matrix
	.p2align	4, 0x90
	.type	read_matrix,@function
read_matrix:                            # @read_matrix
	.cfi_startproc
# BB#0:
	movw	8(%rdi), %cx
	movl	%ecx, %eax
	shrb	$2, %al
	cmpb	$10, %al
	je	.LBB8_2
# BB#1:
	testb	%al, %al
	jne	.LBB8_9
.LBB8_2:
	movl	$-15, %eax
	movzwl	10(%rdi), %edx
	cmpl	$6, %edx
	jne	.LBB8_10
# BB#3:
	movl	$-7, %eax
	testb	$2, %ch
	je	.LBB8_10
# BB#4:
	movq	(%rdi), %rax
	movups	80(%rax), %xmm0
	movups	%xmm0, 80(%rsi)
	movups	64(%rax), %xmm0
	movups	%xmm0, 64(%rsi)
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movups	32(%rax), %xmm2
	movups	48(%rax), %xmm3
	movups	%xmm3, 48(%rsi)
	movups	%xmm2, 32(%rsi)
	movups	%xmm1, 16(%rsi)
	movups	%xmm0, (%rsi)
	movb	8(%rsi), %al
	shrb	$2, %al
	cmpb	$11, %al
	je	.LBB8_7
# BB#5:
	cmpb	$5, %al
	jne	.LBB8_9
# BB#6:
	xorps	%xmm0, %xmm0
	cvtsi2ssq	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	movw	$44, 8(%rsi)
.LBB8_7:
	movb	24(%rsi), %al
	shrb	$2, %al
	cmpb	$5, %al
	je	.LBB8_11
# BB#8:
	cmpb	$11, %al
	je	.LBB8_12
	jmp	.LBB8_9
.LBB8_11:
	xorps	%xmm0, %xmm0
	cvtsi2ssq	16(%rsi), %xmm0
	movss	%xmm0, 16(%rsi)
	movw	$44, 24(%rsi)
.LBB8_12:
	movb	40(%rsi), %al
	shrb	$2, %al
	cmpb	$11, %al
	je	.LBB8_15
# BB#13:
	cmpb	$5, %al
	jne	.LBB8_9
# BB#14:
	xorps	%xmm0, %xmm0
	cvtsi2ssq	32(%rsi), %xmm0
	movss	%xmm0, 32(%rsi)
	movw	$44, 40(%rsi)
.LBB8_15:
	movb	56(%rsi), %al
	shrb	$2, %al
	cmpb	$11, %al
	je	.LBB8_18
# BB#16:
	cmpb	$5, %al
	jne	.LBB8_9
# BB#17:
	xorps	%xmm0, %xmm0
	cvtsi2ssq	48(%rsi), %xmm0
	movss	%xmm0, 48(%rsi)
	movw	$44, 56(%rsi)
.LBB8_18:
	movb	72(%rsi), %al
	shrb	$2, %al
	cmpb	$11, %al
	je	.LBB8_21
# BB#19:
	cmpb	$5, %al
	jne	.LBB8_9
# BB#20:
	xorps	%xmm0, %xmm0
	cvtsi2ssq	64(%rsi), %xmm0
	movss	%xmm0, 64(%rsi)
	movw	$44, 72(%rsi)
.LBB8_21:
	movb	88(%rsi), %al
	shrb	$2, %al
	cmpb	$11, %al
	je	.LBB8_24
# BB#22:
	cmpb	$5, %al
	movl	$-20, %eax
	jne	.LBB8_10
# BB#23:
	xorps	%xmm0, %xmm0
	cvtsi2ssq	80(%rsi), %xmm0
	movss	%xmm0, 80(%rsi)
	movw	$44, 88(%rsi)
.LBB8_24:
	xorl	%eax, %eax
	retq
.LBB8_9:
	movl	$-20, %eax
.LBB8_10:                               # %.loopexit
	retq
.Lfunc_end8:
	.size	read_matrix, .Lfunc_end8-read_matrix
	.cfi_endproc

	.globl	write_matrix
	.p2align	4, 0x90
	.type	write_matrix,@function
write_matrix:                           # @write_matrix
	.cfi_startproc
# BB#0:
	movzwl	8(%rdi), %ecx
	movl	$-20, %eax
	testb	$-4, %cl
	jne	.LBB9_21
# BB#1:
	movl	$-15, %eax
	movzwl	10(%rdi), %edx
	cmpl	$6, %edx
	jne	.LBB9_21
# BB#2:
	movl	$-7, %eax
	testb	$1, %ch
	je	.LBB9_21
# BB#3:
	movq	(%rdi), %rcx
	movb	8(%rcx), %al
	shrb	$2, %al
	cmpb	$5, %al
	je	.LBB9_6
# BB#4:
	cmpb	$11, %al
	je	.LBB9_6
# BB#5:
	movq	$0, (%rcx)
	movw	$20, 8(%rcx)
.LBB9_6:
	movb	24(%rcx), %al
	shrb	$2, %al
	cmpb	$5, %al
	je	.LBB9_9
# BB#7:
	cmpb	$11, %al
	je	.LBB9_9
# BB#8:
	movq	$0, 16(%rcx)
	movw	$20, 24(%rcx)
.LBB9_9:
	movb	40(%rcx), %al
	shrb	$2, %al
	cmpb	$5, %al
	je	.LBB9_12
# BB#10:
	cmpb	$11, %al
	je	.LBB9_12
# BB#11:
	movq	$0, 32(%rcx)
	movw	$20, 40(%rcx)
.LBB9_12:
	movb	56(%rcx), %al
	shrb	$2, %al
	cmpb	$5, %al
	je	.LBB9_15
# BB#13:
	cmpb	$11, %al
	je	.LBB9_15
# BB#14:
	movq	$0, 48(%rcx)
	movw	$20, 56(%rcx)
.LBB9_15:
	movb	72(%rcx), %al
	shrb	$2, %al
	cmpb	$5, %al
	je	.LBB9_18
# BB#16:
	cmpb	$11, %al
	je	.LBB9_18
# BB#17:
	movq	$0, 64(%rcx)
	movw	$20, 72(%rcx)
.LBB9_18:
	movb	88(%rcx), %dl
	shrb	$2, %dl
	xorl	%eax, %eax
	cmpb	$5, %dl
	je	.LBB9_21
# BB#19:
	cmpb	$11, %dl
	je	.LBB9_21
# BB#20:
	movq	$0, 80(%rcx)
	movw	$20, 88(%rcx)
.LBB9_21:                               # %.loopexit
	retq
.Lfunc_end9:
	.size	write_matrix, .Lfunc_end9-write_matrix
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
