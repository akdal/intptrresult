	.text
	.file	"pat.bc"
	.globl	_ZN2kc17syn_patternchainsEPNS_18impl_patternchainsE
	.p2align	4, 0x90
	.type	_ZN2kc17syn_patternchainsEPNS_18impl_patternchainsE,@function
_ZN2kc17syn_patternchainsEPNS_18impl_patternchainsE: # @_ZN2kc17syn_patternchainsEPNS_18impl_patternchainsE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB0_7
# BB#1:
	movq	24(%rbx), %r12
	movq	32(%rbx), %r14
	callq	_ZN2kc7NilpathEv
	movq	%rax, %r15
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$74, %eax
	jne	.LBB0_3
# BB#2:
	movq	%r12, %rdi
	callq	_ZNK2kc18impl_abstract_list6lengthEv
	movq	%r12, %rdi
	movq	%r15, %rsi
	movl	%eax, %edx
	callq	_ZN2kcL18t_syn_patternchainEPNS_17impl_patternchainEPNS_9impl_pathEi
	movq	%rax, %rbx
	jmp	.LBB0_6
.LBB0_7:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$71, %eax
	jne	.LBB0_8
# BB#9:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN2kc25NilpatternrepresentationsEv # TAILCALL
.LBB0_3:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$73, %eax
	jne	.LBB0_5
# BB#4:
	callq	_ZN2kc24NilpatternrepresentationEv
	movq	%rax, %rbx
	jmp	.LBB0_6
.LBB0_8:
	movl	$.L.str, %edi
	movl	$90, %esi
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB0_5:
	movl	$.L.str.2, %edi
	movl	$109, %esi
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%ebx, %ebx
.LBB0_6:                                # %_ZN2kc16syn_patternchainEPNS_17impl_patternchainEPNS_9impl_pathE.exit
	movq	%r14, %rdi
	callq	_ZN2kc17syn_patternchainsEPNS_18impl_patternchainsE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN2kc26ConspatternrepresentationsEPNS_26impl_patternrepresentationEPNS_27impl_patternrepresentationsE # TAILCALL
.Lfunc_end0:
	.size	_ZN2kc17syn_patternchainsEPNS_18impl_patternchainsE, .Lfunc_end0-_ZN2kc17syn_patternchainsEPNS_18impl_patternchainsE
	.cfi_endproc

	.globl	_ZN2kc16syn_patternchainEPNS_17impl_patternchainEPNS_9impl_pathE
	.p2align	4, 0x90
	.type	_ZN2kc16syn_patternchainEPNS_17impl_patternchainEPNS_9impl_pathE,@function
_ZN2kc16syn_patternchainEPNS_17impl_patternchainEPNS_9impl_pathE: # @_ZN2kc16syn_patternchainEPNS_17impl_patternchainEPNS_9impl_pathE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -24
.Lcfi13:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$74, %eax
	jne	.LBB1_1
# BB#3:
	movq	%rbx, %rdi
	callq	_ZNK2kc18impl_abstract_list6lengthEv
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	%eax, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kcL18t_syn_patternchainEPNS_17impl_patternchainEPNS_9impl_pathEi # TAILCALL
.LBB1_1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$73, %eax
	jne	.LBB1_2
# BB#4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc24NilpatternrepresentationEv # TAILCALL
.LBB1_2:
	movl	$.L.str.2, %edi
	movl	$109, %esi
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	_ZN2kc16syn_patternchainEPNS_17impl_patternchainEPNS_9impl_pathE, .Lfunc_end1-_ZN2kc16syn_patternchainEPNS_17impl_patternchainEPNS_9impl_pathE
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL18t_syn_patternchainEPNS_17impl_patternchainEPNS_9impl_pathEi,@function
_ZN2kcL18t_syn_patternchainEPNS_17impl_patternchainEPNS_9impl_pathEi: # @_ZN2kcL18t_syn_patternchainEPNS_17impl_patternchainEPNS_9impl_pathEi
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 48
.Lcfi19:
	.cfi_offset %rbx, -40
.Lcfi20:
	.cfi_offset %r12, -32
.Lcfi21:
	.cfi_offset %r14, -24
.Lcfi22:
	.cfi_offset %r15, -16
	movl	%edx, %r15d
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$74, %eax
	jne	.LBB2_1
# BB#3:
	movq	24(%rbx), %r12
	movq	32(%rbx), %rdi
	leal	-1(%r15), %edx
	movq	%r14, %rsi
	callq	_ZN2kcL18t_syn_patternchainEPNS_17impl_patternchainEPNS_9impl_pathEi
	movq	%rax, %rbx
	movl	%r15d, %edi
	callq	_ZN2kc9mkintegerEi
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	_ZN2kc8ConspathEPNS_17impl_integer__IntEPNS_9impl_pathE
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc20syn_patternchainitemEPNS_21impl_patternchainitemEPNS_9impl_pathE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN2kc6concatEPKNS_26impl_patternrepresentationES2_ # TAILCALL
.LBB2_1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$73, %eax
	jne	.LBB2_2
# BB#4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN2kc24NilpatternrepresentationEv # TAILCALL
.LBB2_2:
	movl	$.L.str.24, %edi
	movl	$130, %esi
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	_ZN2kcL18t_syn_patternchainEPNS_17impl_patternchainEPNS_9impl_pathEi, .Lfunc_end2-_ZN2kcL18t_syn_patternchainEPNS_17impl_patternchainEPNS_9impl_pathEi
	.cfi_endproc

	.globl	_ZN2kc20syn_patternchainitemEPNS_21impl_patternchainitemEPNS_9impl_pathE
	.p2align	4, 0x90
	.type	_ZN2kc20syn_patternchainitemEPNS_21impl_patternchainitemEPNS_9impl_pathE,@function
_ZN2kc20syn_patternchainitemEPNS_21impl_patternchainitemEPNS_9impl_pathE: # @_ZN2kc20syn_patternchainitemEPNS_21impl_patternchainitemEPNS_9impl_pathE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -24
.Lcfi27:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$79, %eax
	jne	.LBB3_1
# BB#4:
	movq	32(%rbx), %rsi
	movq	%r14, %rdi
	callq	_ZN2kc9PRBindingEPNS_9impl_pathEPNS_7impl_IDE
	movq	%rax, %r14
	movq	24(%rbx), %rax
	movq	%rax, 24(%r14)
	callq	_ZN2kc24NilpatternrepresentationEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc25ConspatternrepresentationEPNS_31impl_elem_patternrepresentationEPNS_26impl_patternrepresentationE # TAILCALL
.LBB3_1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$78, %eax
	jne	.LBB3_2
# BB#5:
	movq	16(%rbx), %rdi
	movl	8(%rbx), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.3, %edi
	callq	_ZN2kc9Problem1SEPKc
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc24NilpatternrepresentationEv # TAILCALL
.LBB3_2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$77, %eax
	jne	.LBB3_3
# BB#6:
	movq	32(%rbx), %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kcL18syn_outmostpatternEPNS_19impl_outmostpatternEPNS_9impl_pathE # TAILCALL
.LBB3_3:
	movl	$.L.str.4, %edi
	movl	$163, %esi
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	_ZN2kc20syn_patternchainitemEPNS_21impl_patternchainitemEPNS_9impl_pathE, .Lfunc_end3-_ZN2kc20syn_patternchainitemEPNS_21impl_patternchainitemEPNS_9impl_pathE
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL18syn_outmostpatternEPNS_19impl_outmostpatternEPNS_9impl_pathE,@function
_ZN2kcL18syn_outmostpatternEPNS_19impl_outmostpatternEPNS_9impl_pathE: # @_ZN2kcL18syn_outmostpatternEPNS_19impl_outmostpatternEPNS_9impl_pathE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 64
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movq	(%rbp), %rax
	callq	*(%rax)
	cmpl	$83, %eax
	jne	.LBB4_2
# BB#1:
	movq	32(%rbp), %r14
	movq	%r15, %rdi
	callq	_ZN2kc10PRWildcardEPNS_9impl_pathE
	jmp	.LBB4_4
.LBB4_2:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$84, %eax
	jne	.LBB4_10
# BB#3:
	movq	32(%rbp), %r14
	callq	_ZN2kc9PRDefaultEv
.LBB4_4:
	movq	%rax, %rbx
	movups	16(%rbp), %xmm0
	movups	%xmm0, 16(%rbx)
	movl	8(%rbp), %eax
	movl	%eax, 8(%rbx)
	callq	_ZN2kc24NilpatternrepresentationEv
.LBB4_5:
	movq	%rbx, %rdi
.LBB4_6:
	movq	%rax, %rsi
.LBB4_7:
	callq	_ZN2kc25ConspatternrepresentationEPNS_31impl_elem_patternrepresentationEPNS_26impl_patternrepresentationE
	movq	%rax, %rbx
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB4_9
# BB#8:
	movq	%r14, %rdi
	callq	_ZN2kc15PRUserPredicateEPNS_16impl_CexpressionE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc26impl_patternrepresentation6appendEPNS_31impl_elem_patternrepresentationE
.LBB4_9:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_10:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$82, %eax
	jne	.LBB4_12
# BB#11:
	movq	32(%rbp), %r13
	movq	40(%rbp), %r12
	callq	_ZN2kc14NilCexpressionEv
	movq	%rax, %r14
	callq	_ZN2kc7NilpathEv
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	_ZN2kcL18syn_outmostpatternEPNS_19impl_outmostpatternEPNS_9impl_pathE
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc16PRNonLeafBindingEPNS_9impl_pathEPNS_7impl_IDEPNS_26impl_patternrepresentationE
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZN2kc19f_phylumofpatternIDEPNS_7impl_IDE
	movq	%rax, 16(%r15)
	movups	16(%rbp), %xmm0
	movups	%xmm0, 16(%rbx)
	movl	8(%rbp), %eax
	movl	%eax, 8(%rbx)
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_ZN2kcL18syn_outmostpatternEPNS_19impl_outmostpatternEPNS_9impl_pathE
	jmp	.LBB4_5
.LBB4_12:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$81, %eax
	jne	.LBB4_15
# BB#13:
	movq	32(%rbp), %rbx
	movq	40(%rbp), %r13
	movq	48(%rbp), %r14
	xorl	%edi, %edi
	callq	_ZN2kc9mkintegerEi
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	_ZN2kc8ConspathEPNS_17impl_integer__IntEPNS_9impl_pathE
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	_ZN2kc15PROperPredicateEPNS_9impl_pathEPNS_7impl_IDE
	movq	%rax, %r12
	movups	16(%rbp), %xmm0
	movups	%xmm0, 16(%r12)
	movl	8(%rbp), %eax
	movl	%eax, 8(%r12)
	movq	%rbx, 8(%r15)
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$92, %eax
	jne	.LBB4_20
# BB#14:
	movq	%r13, %rdi
	callq	_ZNK2kc18impl_abstract_list6lengthEv
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	%eax, %edx
	callq	_ZN2kcL14t_syn_patternsEPNS_13impl_patternsEPNS_9impl_pathEi
	jmp	.LBB4_22
.LBB4_15:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$80, %eax
	jne	.LBB4_23
# BB#16:
	movq	32(%rbp), %r12
	movq	40(%rbp), %r14
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB4_24
# BB#17:
	movq	40(%r12), %rax
	movq	8(%rax), %r13
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$172, %eax
	je	.LBB4_19
# BB#18:
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$185, %eax
	jne	.LBB4_27
.LBB4_19:
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	_ZN2kc9PRBindingEPNS_9impl_pathEPNS_7impl_IDE
	movq	%rax, %r13
	movups	16(%rbp), %xmm0
	movups	%xmm0, 16(%r13)
	movl	8(%rbp), %eax
	movl	%eax, 8(%r13)
	jmp	.LBB4_28
.LBB4_20:
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$91, %eax
	jne	.LBB4_26
# BB#21:
	callq	_ZN2kc24NilpatternrepresentationEv
.LBB4_22:
	movq	%rax, %rsi
	movq	%r12, %rdi
	jmp	.LBB4_7
.LBB4_23:
	movl	$.L.str.25, %edi
	movl	$314, %esi              # imm = 0x13A
	jmp	.LBB4_25
.LBB4_24:
	movl	$.L.str.25, %edi
	movl	$309, %esi              # imm = 0x135
.LBB4_25:
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%ebx, %ebx
	jmp	.LBB4_9
.LBB4_26:
	movl	$.L.str.26, %edi
	movl	$400, %esi              # imm = 0x190
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%esi, %esi
	movq	%r12, %rdi
	jmp	.LBB4_7
.LBB4_27:
	xorl	%edi, %edi
	callq	_ZN2kc9mkintegerEi
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	_ZN2kc8ConspathEPNS_17impl_integer__IntEPNS_9impl_pathE
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	_ZN2kc15PROperPredicateEPNS_9impl_pathEPNS_7impl_IDE
	movq	%rax, %r13
	movups	16(%rbp), %xmm0
	movups	%xmm0, 16(%r13)
	movl	8(%rbp), %eax
	movl	%eax, 8(%r13)
	movq	%r12, 8(%r15)
.LBB4_28:                               # %.thread
	callq	_ZN2kc24NilpatternrepresentationEv
	movq	%r13, %rdi
	jmp	.LBB4_6
.Lfunc_end4:
	.size	_ZN2kcL18syn_outmostpatternEPNS_19impl_outmostpatternEPNS_9impl_pathE, .Lfunc_end4-_ZN2kcL18syn_outmostpatternEPNS_19impl_outmostpatternEPNS_9impl_pathE
	.cfi_endproc

	.globl	_ZN2kc19syn_outmostpatternsEPNS_20impl_outmostpatternsE
	.p2align	4, 0x90
	.type	_ZN2kc19syn_outmostpatternsEPNS_20impl_outmostpatternsE,@function
_ZN2kc19syn_outmostpatternsEPNS_20impl_outmostpatternsE: # @_ZN2kc19syn_outmostpatternsEPNS_20impl_outmostpatternsE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 32
.Lcfi44:
	.cfi_offset %rbx, -32
.Lcfi45:
	.cfi_offset %r14, -24
.Lcfi46:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$76, %eax
	jne	.LBB5_1
# BB#3:
	movq	8(%rbx), %r14
	movq	16(%rbx), %r15
	callq	_ZN2kc7NilpathEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kcL18syn_outmostpatternEPNS_19impl_outmostpatternEPNS_9impl_pathE
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	_ZN2kc19syn_outmostpatternsEPNS_20impl_outmostpatternsE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN2kc26ConspatternrepresentationsEPNS_26impl_patternrepresentationEPNS_27impl_patternrepresentationsE # TAILCALL
.LBB5_1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$75, %eax
	jne	.LBB5_2
# BB#4:
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN2kc25NilpatternrepresentationsEv # TAILCALL
.LBB5_2:
	movl	$.L.str.5, %edi
	movl	$187, %esi
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	_ZN2kc19syn_outmostpatternsEPNS_20impl_outmostpatternsE, .Lfunc_end5-_ZN2kc19syn_outmostpatternsEPNS_20impl_outmostpatternsE
	.cfi_endproc

	.globl	_ZN2kc18clone_TypeFileLineEPNS_31impl_elem_patternrepresentationEPNS_19impl_outmostpatternE
	.p2align	4, 0x90
	.type	_ZN2kc18clone_TypeFileLineEPNS_31impl_elem_patternrepresentationEPNS_19impl_outmostpatternE,@function
_ZN2kc18clone_TypeFileLineEPNS_31impl_elem_patternrepresentationEPNS_19impl_outmostpatternE: # @_ZN2kc18clone_TypeFileLineEPNS_31impl_elem_patternrepresentationEPNS_19impl_outmostpatternE
	.cfi_startproc
# BB#0:
	movups	16(%rsi), %xmm0
	movups	%xmm0, 16(%rdi)
	movl	8(%rsi), %eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end6:
	.size	_ZN2kc18clone_TypeFileLineEPNS_31impl_elem_patternrepresentationEPNS_19impl_outmostpatternE, .Lfunc_end6-_ZN2kc18clone_TypeFileLineEPNS_31impl_elem_patternrepresentationEPNS_19impl_outmostpatternE
	.cfi_endproc

	.globl	_ZN2kc17f_bindingidmarkedEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc17f_bindingidmarkedEPNS_7impl_IDE,@function
_ZN2kc17f_bindingidmarkedEPNS_7impl_IDE: # @_ZN2kc17f_bindingidmarkedEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 16
.Lcfi48:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB7_2
# BB#1:
	movq	40(%rbx), %rdi
	callq	_ZN2kc13BindingIdMarkEPNS_11impl_uniqIDE
	cmpb	$0, 8(%rax)
	setne	%al
	jmp	.LBB7_3
.LBB7_2:
	movl	$.L.str.6, %edi
	movl	$436, %esi              # imm = 0x1B4
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
.LBB7_3:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	retq
.Lfunc_end7:
	.size	_ZN2kc17f_bindingidmarkedEPNS_7impl_IDE, .Lfunc_end7-_ZN2kc17f_bindingidmarkedEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc15v_markbindingidEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc15v_markbindingidEPNS_7impl_IDE,@function
_ZN2kc15v_markbindingidEPNS_7impl_IDE:  # @_ZN2kc15v_markbindingidEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 16
.Lcfi50:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB8_2
# BB#1:
	movq	40(%rbx), %rdi
	callq	_ZN2kc13BindingIdMarkEPNS_11impl_uniqIDE
	movb	$1, 8(%rax)
	popq	%rbx
	retq
.LBB8_2:
	movl	$.L.str.7, %edi
	movl	$451, %esi              # imm = 0x1C3
	movl	$.L.str.1, %edx
	popq	%rbx
	jmp	_ZN2kc21kc_no_default_in_withEPKciS1_ # TAILCALL
.Lfunc_end8:
	.size	_ZN2kc15v_markbindingidEPNS_7impl_IDE, .Lfunc_end8-_ZN2kc15v_markbindingidEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc21v_resetbindingidmarksEv
	.p2align	4, 0x90
	.type	_ZN2kc21v_resetbindingidmarksEv,@function
_ZN2kc21v_resetbindingidmarksEv:        # @_ZN2kc21v_resetbindingidmarksEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 16
.Lcfi52:
	.cfi_offset %rbx, -16
	movq	Thebindingidmarks(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB9_2
	jmp	.LBB9_3
	.p2align	4, 0x90
.LBB9_1:                                # %.lr.ph
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	8(%rbx), %rax
	movb	$0, 8(%rax)
	movq	16(%rbx), %rbx
.LBB9_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$337, %eax              # imm = 0x151
	je	.LBB9_1
.LBB9_3:                                # %.loopexit
	popq	%rbx
	retq
.Lfunc_end9:
	.size	_ZN2kc21v_resetbindingidmarksEv, .Lfunc_end9-_ZN2kc21v_resetbindingidmarksEv
	.cfi_endproc

	.globl	_ZN2kc40add_predicates_to_patternrepresentationsEPNS_27impl_patternrepresentationsE
	.p2align	4, 0x90
	.type	_ZN2kc40add_predicates_to_patternrepresentationsEPNS_27impl_patternrepresentationsE,@function
_ZN2kc40add_predicates_to_patternrepresentationsEPNS_27impl_patternrepresentationsE: # @_ZN2kc40add_predicates_to_patternrepresentationsEPNS_27impl_patternrepresentationsE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 32
.Lcfi56:
	.cfi_offset %rbx, -32
.Lcfi57:
	.cfi_offset %r14, -24
.Lcfi58:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$199, %eax
	jne	.LBB10_5
# BB#1:
	movq	8(%rbx), %r15
	movq	16(%rbx), %r14
	movq	Thebindingidmarks(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB10_3
	jmp	.LBB10_4
	.p2align	4, 0x90
.LBB10_2:                               # %.lr.ph.i
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	8(%rbx), %rax
	movb	$0, 8(%rax)
	movq	16(%rbx), %rbx
.LBB10_3:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$337, %eax              # imm = 0x151
	je	.LBB10_2
.LBB10_4:                               # %_ZN2kc21v_resetbindingidmarksEv.exit
	movq	%r15, %rdi
	callq	_ZN2kcL14add_predicatesEPNS_26impl_patternrepresentationE
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc6concatEPKNS_26impl_patternrepresentationES2_
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	_ZN2kc40add_predicates_to_patternrepresentationsEPNS_27impl_patternrepresentationsE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN2kc26ConspatternrepresentationsEPNS_26impl_patternrepresentationEPNS_27impl_patternrepresentationsE # TAILCALL
.LBB10_5:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$198, %eax
	jne	.LBB10_6
# BB#7:
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN2kc25NilpatternrepresentationsEv # TAILCALL
.LBB10_6:
	movl	$.L.str.8, %edi
	movl	$506, %esi              # imm = 0x1FA
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end10:
	.size	_ZN2kc40add_predicates_to_patternrepresentationsEPNS_27impl_patternrepresentationsE, .Lfunc_end10-_ZN2kc40add_predicates_to_patternrepresentationsEPNS_27impl_patternrepresentationsE
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL14add_predicatesEPNS_26impl_patternrepresentationE,@function
_ZN2kcL14add_predicatesEPNS_26impl_patternrepresentationE: # @_ZN2kcL14add_predicatesEPNS_26impl_patternrepresentationE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 32
.Lcfi62:
	.cfi_offset %rbx, -32
.Lcfi63:
	.cfi_offset %r14, -24
.Lcfi64:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$201, %eax
	jne	.LBB11_12
# BB#1:
	movq	8(%rbx), %r15
	movq	16(%rbx), %r14
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$206, %eax
	je	.LBB11_2
# BB#9:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$202, %eax
	jne	.LBB11_10
.LBB11_2:
	movq	40(%r15), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB11_3
# BB#4:                                 # %_ZN2kc17f_bindingidmarkedEPNS_7impl_IDE.exit.i
	movq	40(%rbx), %rdi
	callq	_ZN2kc13BindingIdMarkEPNS_11impl_uniqIDE
	cmpb	$0, 8(%rax)
	je	.LBB11_5
.LBB11_10:
	callq	_ZN2kc24NilpatternrepresentationEv
	jmp	.LBB11_11
.LBB11_12:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$200, %eax
	jne	.LBB11_13
# BB#14:
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN2kc24NilpatternrepresentationEv # TAILCALL
.LBB11_13:
	movl	$.L.str.29, %edi
	movl	$539, %esi              # imm = 0x21B
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB11_3:                               # %_ZN2kc17f_bindingidmarkedEPNS_7impl_IDE.exit.thread.i
	movl	$.L.str.6, %edi
	movl	$436, %esi              # imm = 0x1B4
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB11_5:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB11_7
# BB#6:
	movq	40(%rbx), %rdi
	callq	_ZN2kc13BindingIdMarkEPNS_11impl_uniqIDE
	movb	$1, 8(%rax)
	jmp	.LBB11_8
.LBB11_7:
	movl	$.L.str.7, %edi
	movl	$451, %esi              # imm = 0x1C3
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB11_8:                               # %_ZN2kc15v_markbindingidEPNS_7impl_IDE.exit19.i
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	_ZN2kcL15make_predicatesEPNS_31impl_elem_patternrepresentationEPNS_26impl_patternrepresentationE
.LBB11_11:                              # %_ZN2kcL13add_predicateEPNS_31impl_elem_patternrepresentationEPNS_26impl_patternrepresentationE.exit
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	_ZN2kcL14add_predicatesEPNS_26impl_patternrepresentationE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN2kc6concatEPKNS_26impl_patternrepresentationES2_ # TAILCALL
.Lfunc_end11:
	.size	_ZN2kcL14add_predicatesEPNS_26impl_patternrepresentationE, .Lfunc_end11-_ZN2kcL14add_predicatesEPNS_26impl_patternrepresentationE
	.cfi_endproc

	.globl	_ZN2kc34v_add_rewriterulesinfo_to_operatorEPNS_27impl_patternrepresentationsEPNS_19impl_rewriteclausesE
	.p2align	4, 0x90
	.type	_ZN2kc34v_add_rewriterulesinfo_to_operatorEPNS_27impl_patternrepresentationsEPNS_19impl_rewriteclausesE,@function
_ZN2kc34v_add_rewriterulesinfo_to_operatorEPNS_27impl_patternrepresentationsEPNS_19impl_rewriteclausesE: # @_ZN2kc34v_add_rewriterulesinfo_to_operatorEPNS_27impl_patternrepresentationsEPNS_19impl_rewriteclausesE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi69:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi71:
	.cfi_def_cfa_offset 80
.Lcfi72:
	.cfi_offset %rbx, -56
.Lcfi73:
	.cfi_offset %r12, -48
.Lcfi74:
	.cfi_offset %r13, -40
.Lcfi75:
	.cfi_offset %r14, -32
.Lcfi76:
	.cfi_offset %r15, -24
.Lcfi77:
	.cfi_offset %rbp, -16
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdi, %rbx
	jmp	.LBB12_1
	.p2align	4, 0x90
.LBB12_13:                              # %.loopexit
                                        #   in Loop: Header=BB12_1 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	16(%rbx), %rbx
.LBB12_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_6 Depth 2
                                        #       Child Loop BB12_8 Depth 3
                                        #       Child Loop BB12_11 Depth 3
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$199, %eax
	jne	.LBB12_14
# BB#2:                                 #   in Loop: Header=BB12_1 Depth=1
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	8(%rbx), %r15
	movq	%r15, %rdi
	callq	_ZN2kc33f_operatorofpatternrepresentationEPNS_26impl_patternrepresentationE
	movq	%rax, %rbx
	callq	_ZN2kc9f_emptyIdEv
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	jne	.LBB12_13
# BB#3:                                 #   in Loop: Header=BB12_1 Depth=1
	movq	%rbx, %rdi
	callq	_ZN2kc23f_alternativeofoperatorEPNS_7impl_IDE
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB12_13
# BB#4:                                 # %.preheader
                                        #   in Loop: Header=BB12_1 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$69, %eax
	jne	.LBB12_13
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB12_1 Depth=1
	movq	(%rsp), %rbx            # 8-byte Reload
	.p2align	4, 0x90
.LBB12_6:                               #   Parent Loop BB12_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_8 Depth 3
                                        #       Child Loop BB12_11 Depth 3
	movq	8(%rbx), %r12
	movq	%r15, %rdi
	callq	_ZN2kcL16f_get_predicatesEPNS_26impl_patternrepresentationE
	movq	%rax, %r14
	movq	Thebindingidmarks(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB12_8
	jmp	.LBB12_9
	.p2align	4, 0x90
.LBB12_7:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB12_8 Depth=3
	movq	8(%rbp), %rax
	movb	$0, 8(%rax)
	movq	16(%rbp), %rbp
.LBB12_8:                               # %.lr.ph.i.i
                                        #   Parent Loop BB12_1 Depth=1
                                        #     Parent Loop BB12_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$337, %eax              # imm = 0x151
	je	.LBB12_7
.LBB12_9:                               # %_ZN2kc21v_resetbindingidmarksEv.exit.i
                                        #   in Loop: Header=BB12_6 Depth=2
	movq	%r15, %r13
	movq	%r15, %rdi
	callq	_ZN2kcL17f_do_get_bindingsEPNS_26impl_patternrepresentationE
	movq	%rax, %r15
	movq	Thebindingidmarks(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB12_11
	jmp	.LBB12_12
	.p2align	4, 0x90
.LBB12_10:                              # %.lr.ph.i5.i
                                        #   in Loop: Header=BB12_11 Depth=3
	movq	8(%rbp), %rax
	movb	$0, 8(%rax)
	movq	16(%rbp), %rbp
.LBB12_11:                              # %.lr.ph.i5.i
                                        #   Parent Loop BB12_1 Depth=1
                                        #     Parent Loop BB12_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$337, %eax              # imm = 0x151
	je	.LBB12_10
.LBB12_12:                              # %_ZN2kcL14f_get_bindingsEPNS_26impl_patternrepresentationE.exit
                                        #   in Loop: Header=BB12_6 Depth=2
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	_ZN2kc15RewriteruleinfoEPNS_26impl_patternrepresentationES1_PNS_18impl_rewriteclauseE
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	8(%rbp), %rsi
	movq	%rax, %rdi
	callq	_ZN2kcL25insertin_rewriterulesinfoEPNS_20impl_rewriteruleinfoEPNS_21impl_rewriterulesinfoE
	movq	%rax, 8(%rbp)
	movq	16(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$69, %eax
	movq	%r13, %r15
	je	.LBB12_6
	jmp	.LBB12_13
.LBB12_14:                              # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN2kc34v_add_rewriterulesinfo_to_operatorEPNS_27impl_patternrepresentationsEPNS_19impl_rewriteclausesE, .Lfunc_end12-_ZN2kc34v_add_rewriterulesinfo_to_operatorEPNS_27impl_patternrepresentationsEPNS_19impl_rewriteclausesE
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL25insertin_rewriterulesinfoEPNS_20impl_rewriteruleinfoEPNS_21impl_rewriterulesinfoE,@function
_ZN2kcL25insertin_rewriterulesinfoEPNS_20impl_rewriteruleinfoEPNS_21impl_rewriterulesinfoE: # @_ZN2kcL25insertin_rewriterulesinfoEPNS_20impl_rewriteruleinfoEPNS_21impl_rewriterulesinfoE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi80:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi82:
	.cfi_def_cfa_offset 48
.Lcfi83:
	.cfi_offset %rbx, -40
.Lcfi84:
	.cfi_offset %r12, -32
.Lcfi85:
	.cfi_offset %r14, -24
.Lcfi86:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$218, %eax
	jne	.LBB13_8
# BB#1:
	movq	8(%rbx), %r15
	movq	16(%rbx), %r12
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$219, %eax
	jne	.LBB13_3
# BB#2:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$219, %eax
	jne	.LBB13_3
# BB#5:                                 # %_ZN2kcL18lt_rewriteruleinfoEPNS_20impl_rewriteruleinfoES1_.exit
	movq	8(%r15), %rdi
	movq	8(%r14), %rsi
	callq	_ZN2kcL24lt_patternrepresentationEPNS_26impl_patternrepresentationES1_
	testb	%al, %al
	je	.LBB13_4
# BB#6:
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	_ZN2kcL25insertin_rewriterulesinfoEPNS_20impl_rewriteruleinfoEPNS_21impl_rewriterulesinfoE
	movq	%r15, %rdi
	movq	%rax, %rsi
	jmp	.LBB13_7
.LBB13_8:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$217, %eax
	je	.LBB13_4
# BB#9:
	movl	$.L.str.34, %edi
	movl	$931, %esi              # imm = 0x3A3
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB13_3:                               # %_ZN2kcL18lt_rewriteruleinfoEPNS_20impl_rewriteruleinfoES1_.exit.thread
	movl	$.L.str.35, %edi
	movl	$948, %esi              # imm = 0x3B4
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB13_4:
	movq	%r14, %rdi
	movq	%rbx, %rsi
.LBB13_7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN2kc20ConsrewriterulesinfoEPNS_20impl_rewriteruleinfoEPNS_21impl_rewriterulesinfoE # TAILCALL
.Lfunc_end13:
	.size	_ZN2kcL25insertin_rewriterulesinfoEPNS_20impl_rewriteruleinfoEPNS_21impl_rewriterulesinfoE, .Lfunc_end13-_ZN2kcL25insertin_rewriterulesinfoEPNS_20impl_rewriteruleinfoEPNS_21impl_rewriterulesinfoE
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL16f_get_predicatesEPNS_26impl_patternrepresentationE,@function
_ZN2kcL16f_get_predicatesEPNS_26impl_patternrepresentationE: # @_ZN2kcL16f_get_predicatesEPNS_26impl_patternrepresentationE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 32
.Lcfi90:
	.cfi_offset %rbx, -32
.Lcfi91:
	.cfi_offset %r14, -24
.Lcfi92:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB14_1:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$201, %eax
	jne	.LBB14_5
# BB#2:                                 #   in Loop: Header=BB14_1 Depth=1
	movq	8(%rbx), %r14
	movq	16(%rbx), %rbx
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$206, %eax
	je	.LBB14_1
# BB#3:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_ZN2kcL16f_get_predicatesEPNS_26impl_patternrepresentationE
	cmpl	$202, %ebp
	je	.LBB14_7
# BB#4:
	movq	%r14, %rdi
	movq	%rax, %rsi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	_ZN2kc25ConspatternrepresentationEPNS_31impl_elem_patternrepresentationEPNS_26impl_patternrepresentationE # TAILCALL
.LBB14_5:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$200, %eax
	jne	.LBB14_6
# BB#8:
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	_ZN2kc24NilpatternrepresentationEv # TAILCALL
.LBB14_6:
	movl	$.L.str.32, %edi
	movl	$849, %esi              # imm = 0x351
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
.LBB14_7:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end14:
	.size	_ZN2kcL16f_get_predicatesEPNS_26impl_patternrepresentationE, .Lfunc_end14-_ZN2kcL16f_get_predicatesEPNS_26impl_patternrepresentationE
	.cfi_endproc

	.globl	_ZN2kc15f_withcasesinfoEPNS_27impl_patternrepresentationsEPNS_10impl_CtextE
	.p2align	4, 0x90
	.type	_ZN2kc15f_withcasesinfoEPNS_27impl_patternrepresentationsEPNS_10impl_CtextE,@function
_ZN2kc15f_withcasesinfoEPNS_27impl_patternrepresentationsEPNS_10impl_CtextE: # @_ZN2kc15f_withcasesinfoEPNS_27impl_patternrepresentationsEPNS_10impl_CtextE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi95:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi96:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi97:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi99:
	.cfi_def_cfa_offset 64
.Lcfi100:
	.cfi_offset %rbx, -56
.Lcfi101:
	.cfi_offset %r12, -48
.Lcfi102:
	.cfi_offset %r13, -40
.Lcfi103:
	.cfi_offset %r14, -32
.Lcfi104:
	.cfi_offset %r15, -24
.Lcfi105:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	callq	_ZN2kc16NilwithcasesinfoEv
	movq	%rax, %r15
	jmp	.LBB15_1
	.p2align	4, 0x90
.LBB15_9:                               # %_ZN2kcL14f_get_bindingsEPNS_26impl_patternrepresentationE.exit
                                        #   in Loop: Header=BB15_1 Depth=1
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	_ZN2kc12WithcaseinfoEPNS_26impl_patternrepresentationES1_PNS_10impl_CtextE
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	_ZN2kc22insertin_withcasesinfoEPNS_17impl_withcaseinfoEPNS_18impl_withcasesinfoE
	movq	%rax, %r15
	movq	16(%r13), %r13
.LBB15_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_5 Depth 2
                                        #     Child Loop BB15_8 Depth 2
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$199, %eax
	jne	.LBB15_13
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB15_1 Depth=1
	movq	8(%r13), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$201, %eax
	jne	.LBB15_10
# BB#3:                                 #   in Loop: Header=BB15_1 Depth=1
	movq	%rbx, %rdi
	callq	_ZN2kcL16f_get_predicatesEPNS_26impl_patternrepresentationE
	movq	%rax, %r12
	movq	Thebindingidmarks(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB15_5
	jmp	.LBB15_6
	.p2align	4, 0x90
.LBB15_4:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB15_5 Depth=2
	movq	8(%rbp), %rax
	movb	$0, 8(%rax)
	movq	16(%rbp), %rbp
.LBB15_5:                               # %.lr.ph.i.i
                                        #   Parent Loop BB15_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$337, %eax              # imm = 0x151
	je	.LBB15_4
.LBB15_6:                               # %_ZN2kc21v_resetbindingidmarksEv.exit.i
                                        #   in Loop: Header=BB15_1 Depth=1
	movq	%rbx, %rdi
	callq	_ZN2kcL17f_do_get_bindingsEPNS_26impl_patternrepresentationE
	movq	%rax, %rbx
	movq	Thebindingidmarks(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB15_8
	jmp	.LBB15_9
	.p2align	4, 0x90
.LBB15_7:                               # %.lr.ph.i5.i
                                        #   in Loop: Header=BB15_8 Depth=2
	movq	8(%rbp), %rax
	movb	$0, 8(%rax)
	movq	16(%rbp), %rbp
.LBB15_8:                               # %.lr.ph.i5.i
                                        #   Parent Loop BB15_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$337, %eax              # imm = 0x151
	je	.LBB15_7
	jmp	.LBB15_9
	.p2align	4, 0x90
.LBB15_10:                              #   in Loop: Header=BB15_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$200, %eax
	jne	.LBB15_12
# BB#11:                                # %.thread
                                        #   in Loop: Header=BB15_1 Depth=1
	movq	16(%r13), %r13
	jmp	.LBB15_1
.LBB15_12:
	movl	$.L.str.9, %edi
	movl	$748, %esi              # imm = 0x2EC
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%r15d, %r15d
.LBB15_13:                              # %.loopexit
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	_ZN2kc15f_withcasesinfoEPNS_27impl_patternrepresentationsEPNS_10impl_CtextE, .Lfunc_end15-_ZN2kc15f_withcasesinfoEPNS_27impl_patternrepresentationsEPNS_10impl_CtextE
	.cfi_endproc

	.globl	_ZN2kc22insertin_withcasesinfoEPNS_17impl_withcaseinfoEPNS_18impl_withcasesinfoE
	.p2align	4, 0x90
	.type	_ZN2kc22insertin_withcasesinfoEPNS_17impl_withcaseinfoEPNS_18impl_withcasesinfoE,@function
_ZN2kc22insertin_withcasesinfoEPNS_17impl_withcaseinfoEPNS_18impl_withcasesinfoE: # @_ZN2kc22insertin_withcasesinfoEPNS_17impl_withcaseinfoEPNS_18impl_withcasesinfoE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi106:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi107:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi108:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi109:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi110:
	.cfi_def_cfa_offset 48
.Lcfi111:
	.cfi_offset %rbx, -40
.Lcfi112:
	.cfi_offset %r12, -32
.Lcfi113:
	.cfi_offset %r14, -24
.Lcfi114:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$221, %eax
	jne	.LBB16_8
# BB#1:
	movq	8(%rbx), %r15
	movq	16(%rbx), %r12
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$222, %eax
	jne	.LBB16_3
# BB#2:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$222, %eax
	jne	.LBB16_3
# BB#5:                                 # %_ZN2kc15lt_withcaseinfoEPNS_17impl_withcaseinfoES1_.exit
	movq	8(%r15), %rdi
	movq	8(%r14), %rsi
	callq	_ZN2kcL24lt_patternrepresentationEPNS_26impl_patternrepresentationES1_
	testb	%al, %al
	je	.LBB16_4
# BB#6:
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	_ZN2kc22insertin_withcasesinfoEPNS_17impl_withcaseinfoEPNS_18impl_withcasesinfoE
	movq	%r15, %rdi
	movq	%rax, %rsi
	jmp	.LBB16_7
.LBB16_8:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$220, %eax
	je	.LBB16_4
# BB#9:
	movl	$.L.str.10, %edi
	movl	$972, %esi              # imm = 0x3CC
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB16_3:                               # %_ZN2kc15lt_withcaseinfoEPNS_17impl_withcaseinfoES1_.exit.thread
	movl	$.L.str.11, %edi
	movl	$989, %esi              # imm = 0x3DD
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB16_4:
	movq	%r14, %rdi
	movq	%rbx, %rsi
.LBB16_7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN2kc17ConswithcasesinfoEPNS_17impl_withcaseinfoEPNS_18impl_withcasesinfoE # TAILCALL
.Lfunc_end16:
	.size	_ZN2kc22insertin_withcasesinfoEPNS_17impl_withcaseinfoEPNS_18impl_withcasesinfoE, .Lfunc_end16-_ZN2kc22insertin_withcasesinfoEPNS_17impl_withcaseinfoEPNS_18impl_withcasesinfoE
	.cfi_endproc

	.globl	_ZN2kc34v_add_unparsedeclsinfo_to_operatorEPNS_27impl_patternrepresentationsEPNS_19impl_unparseclausesE
	.p2align	4, 0x90
	.type	_ZN2kc34v_add_unparsedeclsinfo_to_operatorEPNS_27impl_patternrepresentationsEPNS_19impl_unparseclausesE,@function
_ZN2kc34v_add_unparsedeclsinfo_to_operatorEPNS_27impl_patternrepresentationsEPNS_19impl_unparseclausesE: # @_ZN2kc34v_add_unparsedeclsinfo_to_operatorEPNS_27impl_patternrepresentationsEPNS_19impl_unparseclausesE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi117:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi118:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi119:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi120:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi121:
	.cfi_def_cfa_offset 80
.Lcfi122:
	.cfi_offset %rbx, -56
.Lcfi123:
	.cfi_offset %r12, -48
.Lcfi124:
	.cfi_offset %r13, -40
.Lcfi125:
	.cfi_offset %r14, -32
.Lcfi126:
	.cfi_offset %r15, -24
.Lcfi127:
	.cfi_offset %rbp, -16
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdi, %rbx
	jmp	.LBB17_1
	.p2align	4, 0x90
.LBB17_13:                              # %.loopexit
                                        #   in Loop: Header=BB17_1 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	16(%rbx), %rbx
.LBB17_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_6 Depth 2
                                        #       Child Loop BB17_8 Depth 3
                                        #       Child Loop BB17_11 Depth 3
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$199, %eax
	jne	.LBB17_14
# BB#2:                                 #   in Loop: Header=BB17_1 Depth=1
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	8(%rbx), %r15
	movq	%r15, %rdi
	callq	_ZN2kc33f_operatorofpatternrepresentationEPNS_26impl_patternrepresentationE
	movq	%rax, %rbx
	callq	_ZN2kc9f_emptyIdEv
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	jne	.LBB17_13
# BB#3:                                 #   in Loop: Header=BB17_1 Depth=1
	movq	%rbx, %rdi
	callq	_ZN2kc23f_alternativeofoperatorEPNS_7impl_IDE
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB17_13
# BB#4:                                 # %.preheader
                                        #   in Loop: Header=BB17_1 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$142, %eax
	jne	.LBB17_13
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB17_1 Depth=1
	movq	(%rsp), %rbx            # 8-byte Reload
	.p2align	4, 0x90
.LBB17_6:                               #   Parent Loop BB17_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_8 Depth 3
                                        #       Child Loop BB17_11 Depth 3
	movq	8(%rbx), %r12
	movq	%r15, %rdi
	callq	_ZN2kcL16f_get_predicatesEPNS_26impl_patternrepresentationE
	movq	%rax, %r14
	movq	Thebindingidmarks(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB17_8
	jmp	.LBB17_9
	.p2align	4, 0x90
.LBB17_7:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB17_8 Depth=3
	movq	8(%rbp), %rax
	movb	$0, 8(%rax)
	movq	16(%rbp), %rbp
.LBB17_8:                               # %.lr.ph.i.i
                                        #   Parent Loop BB17_1 Depth=1
                                        #     Parent Loop BB17_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$337, %eax              # imm = 0x151
	je	.LBB17_7
.LBB17_9:                               # %_ZN2kc21v_resetbindingidmarksEv.exit.i
                                        #   in Loop: Header=BB17_6 Depth=2
	movq	%r15, %r13
	movq	%r15, %rdi
	callq	_ZN2kcL17f_do_get_bindingsEPNS_26impl_patternrepresentationE
	movq	%rax, %r15
	movq	Thebindingidmarks(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB17_11
	jmp	.LBB17_12
	.p2align	4, 0x90
.LBB17_10:                              # %.lr.ph.i5.i
                                        #   in Loop: Header=BB17_11 Depth=3
	movq	8(%rbp), %rax
	movb	$0, 8(%rax)
	movq	16(%rbp), %rbp
.LBB17_11:                              # %.lr.ph.i5.i
                                        #   Parent Loop BB17_1 Depth=1
                                        #     Parent Loop BB17_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$337, %eax              # imm = 0x151
	je	.LBB17_10
.LBB17_12:                              # %_ZN2kcL14f_get_bindingsEPNS_26impl_patternrepresentationE.exit
                                        #   in Loop: Header=BB17_6 Depth=2
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	_ZN2kc15UnparsedeclinfoEPNS_26impl_patternrepresentationES1_PNS_18impl_unparseclauseE
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	16(%rbp), %rsi
	movq	%rax, %rdi
	callq	_ZN2kcL25insertin_unparsedeclsinfoEPNS_20impl_unparsedeclinfoEPNS_21impl_unparsedeclsinfoE
	movq	%rax, 16(%rbp)
	movq	16(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$142, %eax
	movq	%r13, %r15
	je	.LBB17_6
	jmp	.LBB17_13
.LBB17_14:                              # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	_ZN2kc34v_add_unparsedeclsinfo_to_operatorEPNS_27impl_patternrepresentationsEPNS_19impl_unparseclausesE, .Lfunc_end17-_ZN2kc34v_add_unparsedeclsinfo_to_operatorEPNS_27impl_patternrepresentationsEPNS_19impl_unparseclausesE
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL25insertin_unparsedeclsinfoEPNS_20impl_unparsedeclinfoEPNS_21impl_unparsedeclsinfoE,@function
_ZN2kcL25insertin_unparsedeclsinfoEPNS_20impl_unparsedeclinfoEPNS_21impl_unparsedeclsinfoE: # @_ZN2kcL25insertin_unparsedeclsinfoEPNS_20impl_unparsedeclinfoEPNS_21impl_unparsedeclsinfoE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi128:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi129:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi130:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi131:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi132:
	.cfi_def_cfa_offset 48
.Lcfi133:
	.cfi_offset %rbx, -40
.Lcfi134:
	.cfi_offset %r12, -32
.Lcfi135:
	.cfi_offset %r14, -24
.Lcfi136:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$230, %eax
	jne	.LBB18_8
# BB#1:
	movq	8(%rbx), %r15
	movq	16(%rbx), %r12
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$231, %eax
	jne	.LBB18_3
# BB#2:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$231, %eax
	jne	.LBB18_3
# BB#5:                                 # %_ZN2kcL18lt_unparsedeclinfoEPNS_20impl_unparsedeclinfoES1_.exit
	movq	8(%r15), %rdi
	movq	8(%r14), %rsi
	callq	_ZN2kcL24lt_patternrepresentationEPNS_26impl_patternrepresentationES1_
	testb	%al, %al
	je	.LBB18_4
# BB#6:
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	_ZN2kcL25insertin_unparsedeclsinfoEPNS_20impl_unparsedeclinfoEPNS_21impl_unparsedeclsinfoE
	movq	%r15, %rdi
	movq	%rax, %rsi
	jmp	.LBB18_7
.LBB18_8:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$229, %eax
	je	.LBB18_4
# BB#9:
	movl	$.L.str.36, %edi
	movl	$1013, %esi             # imm = 0x3F5
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB18_3:                               # %_ZN2kcL18lt_unparsedeclinfoEPNS_20impl_unparsedeclinfoES1_.exit.thread
	movl	$.L.str.37, %edi
	movl	$1030, %esi             # imm = 0x406
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB18_4:
	movq	%r14, %rdi
	movq	%rbx, %rsi
.LBB18_7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN2kc20ConsunparsedeclsinfoEPNS_20impl_unparsedeclinfoEPNS_21impl_unparsedeclsinfoE # TAILCALL
.Lfunc_end18:
	.size	_ZN2kcL25insertin_unparsedeclsinfoEPNS_20impl_unparsedeclinfoEPNS_21impl_unparsedeclsinfoE, .Lfunc_end18-_ZN2kcL25insertin_unparsedeclsinfoEPNS_20impl_unparsedeclinfoEPNS_21impl_unparsedeclsinfoE
	.cfi_endproc

	.globl	_ZN2kc15lt_withcaseinfoEPNS_17impl_withcaseinfoES1_
	.p2align	4, 0x90
	.type	_ZN2kc15lt_withcaseinfoEPNS_17impl_withcaseinfoES1_,@function
_ZN2kc15lt_withcaseinfoEPNS_17impl_withcaseinfoES1_: # @_ZN2kc15lt_withcaseinfoEPNS_17impl_withcaseinfoES1_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi137:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi138:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi139:
	.cfi_def_cfa_offset 32
.Lcfi140:
	.cfi_offset %rbx, -24
.Lcfi141:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$222, %eax
	jne	.LBB19_2
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$222, %eax
	jne	.LBB19_2
# BB#3:
	movq	8(%rbx), %rdi
	movq	8(%r14), %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kcL24lt_patternrepresentationEPNS_26impl_patternrepresentationES1_ # TAILCALL
.LBB19_2:
	movl	$.L.str.11, %edi
	movl	$989, %esi              # imm = 0x3DD
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end19:
	.size	_ZN2kc15lt_withcaseinfoEPNS_17impl_withcaseinfoES1_, .Lfunc_end19-_ZN2kc15lt_withcaseinfoEPNS_17impl_withcaseinfoES1_
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL24lt_patternrepresentationEPNS_26impl_patternrepresentationES1_,@function
_ZN2kcL24lt_patternrepresentationEPNS_26impl_patternrepresentationES1_: # @_ZN2kcL24lt_patternrepresentationEPNS_26impl_patternrepresentationES1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi142:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi143:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi144:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi145:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi146:
	.cfi_def_cfa_offset 48
.Lcfi147:
	.cfi_offset %rbx, -48
.Lcfi148:
	.cfi_offset %r12, -40
.Lcfi149:
	.cfi_offset %r13, -32
.Lcfi150:
	.cfi_offset %r14, -24
.Lcfi151:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	jmp	.LBB20_2
	.p2align	4, 0x90
.LBB20_1:                               # %.thread57
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	16(%r15), %r15
	movq	16(%r14), %r14
.LBB20_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_54 Depth 2
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$201, %eax
	jne	.LBB20_82
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$201, %eax
	jne	.LBB20_82
# BB#4:                                 #   in Loop: Header=BB20_2 Depth=1
	movq	8(%r15), %rbx
	movq	8(%r14), %r12
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$208, %eax
	jne	.LBB20_6
# BB#5:                                 #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$207, %eax
	je	.LBB20_18
.LBB20_6:                               #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$207, %eax
	jne	.LBB20_8
# BB#7:                                 #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$208, %eax
	je	.LBB20_21
.LBB20_8:                               #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$203, %eax
	jne	.LBB20_10
# BB#9:                                 #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$204, %eax
	je	.LBB20_18
.LBB20_10:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$204, %eax
	jne	.LBB20_12
# BB#11:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$203, %eax
	je	.LBB20_21
.LBB20_12:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$203, %eax
	jne	.LBB20_14
# BB#13:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$210, %eax
	je	.LBB20_18
.LBB20_14:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$210, %eax
	jne	.LBB20_16
# BB#15:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$203, %eax
	je	.LBB20_21
.LBB20_16:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$203, %eax
	jne	.LBB20_19
# BB#17:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$209, %eax
	jne	.LBB20_19
	.p2align	4, 0x90
.LBB20_18:                              #   in Loop: Header=BB20_2 Depth=1
	callq	_ZN2kc6BiggerEv
	jmp	.LBB20_47
.LBB20_19:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$209, %eax
	jne	.LBB20_22
# BB#20:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$203, %eax
	jne	.LBB20_22
	.p2align	4, 0x90
.LBB20_21:                              #   in Loop: Header=BB20_2 Depth=1
	callq	_ZN2kc7SmallerEv
	jmp	.LBB20_47
.LBB20_22:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$210, %eax
	jne	.LBB20_24
# BB#23:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$209, %eax
	je	.LBB20_46
.LBB20_24:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$209, %eax
	jne	.LBB20_26
# BB#25:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$210, %eax
	je	.LBB20_46
.LBB20_26:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$204, %eax
	jne	.LBB20_28
# BB#27:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$210, %eax
	je	.LBB20_46
.LBB20_28:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$210, %eax
	jne	.LBB20_30
# BB#29:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$204, %eax
	je	.LBB20_46
.LBB20_30:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$204, %eax
	jne	.LBB20_32
# BB#31:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$209, %eax
	je	.LBB20_46
.LBB20_32:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$209, %eax
	jne	.LBB20_34
# BB#33:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$204, %eax
	je	.LBB20_46
.LBB20_34:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$210, %eax
	jne	.LBB20_36
# BB#35:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$210, %eax
	je	.LBB20_46
.LBB20_36:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$209, %eax
	jne	.LBB20_38
# BB#37:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$209, %eax
	je	.LBB20_46
.LBB20_38:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$206, %eax
	jne	.LBB20_40
# BB#39:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$206, %eax
	je	.LBB20_46
.LBB20_40:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$202, %eax
	jne	.LBB20_42
# BB#41:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$202, %eax
	je	.LBB20_46
.LBB20_42:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$207, %eax
	jne	.LBB20_44
# BB#43:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$207, %eax
	je	.LBB20_46
.LBB20_44:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$204, %eax
	jne	.LBB20_50
# BB#45:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$204, %eax
	jne	.LBB20_50
.LBB20_46:                              #   in Loop: Header=BB20_2 Depth=1
	movq	32(%rbx), %rdi
	movq	32(%r12), %rsi
	callq	_ZN2kcL10equal_pathEPNS_9impl_pathES1_
	.p2align	4, 0x90
.LBB20_47:                              # %_ZN2kcL32equal_elem_patternrepresentationEPNS_31impl_elem_patternrepresentationES1_.exit
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	%rax, %rbx
.LBB20_48:                              # %_ZN2kcL32equal_elem_patternrepresentationEPNS_31impl_elem_patternrepresentationES1_.exit
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$197, %eax
	je	.LBB20_87
# BB#49:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$196, %eax
	jne	.LBB20_1
	jmp	.LBB20_89
.LBB20_50:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$203, %eax
	jne	.LBB20_58
# BB#51:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$203, %eax
	jne	.LBB20_58
# BB#52:                                #   in Loop: Header=BB20_2 Depth=1
	movq	32(%rbx), %r13
	movq	32(%r12), %r12
	jmp	.LBB20_54
.LBB20_53:                              # %.thread59.i.i
                                        #   in Loop: Header=BB20_54 Depth=2
	movq	16(%r13), %r13
	movq	16(%r12), %r12
.LBB20_54:                              #   Parent Loop BB20_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$214, %eax
	jne	.LBB20_74
# BB#55:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB20_54 Depth=2
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$214, %eax
	jne	.LBB20_74
# BB#56:                                #   in Loop: Header=BB20_54 Depth=2
	movq	8(%r13), %rdi
	movq	8(%r12), %rsi
	callq	_ZN2kcL10equal_pathEPNS_9impl_pathES1_
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$197, %eax
	je	.LBB20_18
# BB#57:                                #   in Loop: Header=BB20_54 Depth=2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$196, %eax
	jne	.LBB20_53
	jmp	.LBB20_21
.LBB20_58:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$205, %eax
	jne	.LBB20_60
# BB#59:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$205, %eax
	je	.LBB20_62
.LBB20_60:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$208, %eax
	jne	.LBB20_63
# BB#61:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$208, %eax
	jne	.LBB20_63
.LBB20_62:                              #   in Loop: Header=BB20_2 Depth=1
	callq	_ZN2kc5EqualEv
	jmp	.LBB20_47
.LBB20_63:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$205, %eax
	je	.LBB20_18
# BB#64:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$206, %eax
	je	.LBB20_18
# BB#65:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$202, %eax
	je	.LBB20_18
# BB#66:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$207, %eax
	je	.LBB20_18
# BB#67:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$208, %eax
	je	.LBB20_18
# BB#68:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$205, %eax
	je	.LBB20_21
# BB#69:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$206, %eax
	je	.LBB20_21
# BB#70:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$202, %eax
	je	.LBB20_21
# BB#71:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$207, %eax
	je	.LBB20_21
# BB#72:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$208, %eax
	je	.LBB20_21
# BB#73:                                #   in Loop: Header=BB20_2 Depth=1
	movl	$.L.str.39, %edi
	movl	$1318, %esi             # imm = 0x526
	jmp	.LBB20_81
.LBB20_74:                              # %.critedge.i.i
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$213, %eax
	jne	.LBB20_76
# BB#75:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$214, %eax
	je	.LBB20_18
.LBB20_76:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$214, %eax
	jne	.LBB20_78
# BB#77:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$213, %eax
	je	.LBB20_21
.LBB20_78:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$213, %eax
	jne	.LBB20_80
# BB#79:                                #   in Loop: Header=BB20_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$213, %eax
	je	.LBB20_62
.LBB20_80:                              #   in Loop: Header=BB20_2 Depth=1
	movl	$.L.str.40, %edi
	movl	$1446, %esi             # imm = 0x5A6
.LBB20_81:                              # %_ZN2kcL32equal_elem_patternrepresentationEPNS_31impl_elem_patternrepresentationES1_.exit
                                        #   in Loop: Header=BB20_2 Depth=1
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%ebx, %ebx
	jmp	.LBB20_48
.LBB20_82:                              # %.critedge
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$200, %eax
	jne	.LBB20_84
# BB#83:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$200, %eax
	je	.LBB20_87
.LBB20_84:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$200, %eax
	je	.LBB20_87
# BB#85:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	movl	%eax, %ecx
	movb	$1, %al
	cmpl	$200, %ecx
	je	.LBB20_88
# BB#86:
	movl	$.L.str.38, %edi
	movl	$1147, %esi             # imm = 0x47B
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB20_87:                              # %select.unfold
	xorl	%eax, %eax
.LBB20_88:                              # %select.unfold
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB20_89:
	movb	$1, %al
	jmp	.LBB20_88
.Lfunc_end20:
	.size	_ZN2kcL24lt_patternrepresentationEPNS_26impl_patternrepresentationES1_, .Lfunc_end20-_ZN2kcL24lt_patternrepresentationEPNS_26impl_patternrepresentationES1_
	.cfi_endproc

	.globl	_ZN2kc27warn_drop_identical_patternEPNS_20impl_rewriteruleinfoE
	.p2align	4, 0x90
	.type	_ZN2kc27warn_drop_identical_patternEPNS_20impl_rewriteruleinfoE,@function
_ZN2kc27warn_drop_identical_patternEPNS_20impl_rewriteruleinfoE: # @_ZN2kc27warn_drop_identical_patternEPNS_20impl_rewriteruleinfoE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi152:
	.cfi_def_cfa_offset 16
.Lcfi153:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$219, %eax
	jne	.LBB21_3
# BB#1:
	movq	8(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$201, %eax
	jne	.LBB21_4
# BB#2:                                 # %_ZN2kc27warn_drop_identical_patternEPNS_26impl_patternrepresentationE.exit
	movq	8(%rbx), %rax
	movq	16(%rax), %rdi
	movl	8(%rax), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.13, %edi
	callq	_ZN2kc9Problem1SEPKc
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc7WarningEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	popq	%rbx
	jmp	_ZN2kc8v_reportEPNS_10impl_errorE # TAILCALL
.LBB21_3:
	movl	$.L.str.12, %edi
	movl	$1043, %esi             # imm = 0x413
	movl	$.L.str.1, %edx
	popq	%rbx
	jmp	_ZN2kc21kc_no_default_in_withEPKciS1_ # TAILCALL
.LBB21_4:
	movl	$.L.str.1, %edi
	movl	$1084, %esi             # imm = 0x43C
	movl	$.L.str.14, %edx
	callq	_ZN2kc24kc_assertionReasonFailedEPKciS1_
.Lfunc_end21:
	.size	_ZN2kc27warn_drop_identical_patternEPNS_20impl_rewriteruleinfoE, .Lfunc_end21-_ZN2kc27warn_drop_identical_patternEPNS_20impl_rewriteruleinfoE
	.cfi_endproc

	.globl	_ZN2kc27warn_drop_identical_patternEPNS_26impl_patternrepresentationE
	.p2align	4, 0x90
	.type	_ZN2kc27warn_drop_identical_patternEPNS_26impl_patternrepresentationE,@function
_ZN2kc27warn_drop_identical_patternEPNS_26impl_patternrepresentationE: # @_ZN2kc27warn_drop_identical_patternEPNS_26impl_patternrepresentationE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi154:
	.cfi_def_cfa_offset 16
.Lcfi155:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$201, %eax
	jne	.LBB22_2
# BB#1:
	movq	8(%rbx), %rax
	movq	16(%rax), %rdi
	movl	8(%rax), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.13, %edi
	callq	_ZN2kc9Problem1SEPKc
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc7WarningEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	popq	%rbx
	jmp	_ZN2kc8v_reportEPNS_10impl_errorE # TAILCALL
.LBB22_2:
	movl	$.L.str.1, %edi
	movl	$1084, %esi             # imm = 0x43C
	movl	$.L.str.14, %edx
	callq	_ZN2kc24kc_assertionReasonFailedEPKciS1_
.Lfunc_end22:
	.size	_ZN2kc27warn_drop_identical_patternEPNS_26impl_patternrepresentationE, .Lfunc_end22-_ZN2kc27warn_drop_identical_patternEPNS_26impl_patternrepresentationE
	.cfi_endproc

	.globl	_ZN2kc27warn_drop_identical_patternEPNS_17impl_withcaseinfoE
	.p2align	4, 0x90
	.type	_ZN2kc27warn_drop_identical_patternEPNS_17impl_withcaseinfoE,@function
_ZN2kc27warn_drop_identical_patternEPNS_17impl_withcaseinfoE: # @_ZN2kc27warn_drop_identical_patternEPNS_17impl_withcaseinfoE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi156:
	.cfi_def_cfa_offset 16
.Lcfi157:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$222, %eax
	jne	.LBB23_3
# BB#1:
	movq	8(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$201, %eax
	jne	.LBB23_4
# BB#2:                                 # %_ZN2kc27warn_drop_identical_patternEPNS_26impl_patternrepresentationE.exit
	movq	8(%rbx), %rax
	movq	16(%rax), %rdi
	movl	8(%rax), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.13, %edi
	callq	_ZN2kc9Problem1SEPKc
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc7WarningEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	popq	%rbx
	jmp	_ZN2kc8v_reportEPNS_10impl_errorE # TAILCALL
.LBB23_3:
	movl	$.L.str.12, %edi
	movl	$1055, %esi             # imm = 0x41F
	movl	$.L.str.1, %edx
	popq	%rbx
	jmp	_ZN2kc21kc_no_default_in_withEPKciS1_ # TAILCALL
.LBB23_4:
	movl	$.L.str.1, %edi
	movl	$1084, %esi             # imm = 0x43C
	movl	$.L.str.14, %edx
	callq	_ZN2kc24kc_assertionReasonFailedEPKciS1_
.Lfunc_end23:
	.size	_ZN2kc27warn_drop_identical_patternEPNS_17impl_withcaseinfoE, .Lfunc_end23-_ZN2kc27warn_drop_identical_patternEPNS_17impl_withcaseinfoE
	.cfi_endproc

	.globl	_ZN2kc27warn_drop_identical_patternEPNS_20impl_unparsedeclinfoE
	.p2align	4, 0x90
	.type	_ZN2kc27warn_drop_identical_patternEPNS_20impl_unparsedeclinfoE,@function
_ZN2kc27warn_drop_identical_patternEPNS_20impl_unparsedeclinfoE: # @_ZN2kc27warn_drop_identical_patternEPNS_20impl_unparsedeclinfoE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi158:
	.cfi_def_cfa_offset 16
.Lcfi159:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$231, %eax
	jne	.LBB24_3
# BB#1:
	movq	8(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$201, %eax
	jne	.LBB24_4
# BB#2:                                 # %_ZN2kc27warn_drop_identical_patternEPNS_26impl_patternrepresentationE.exit
	movq	8(%rbx), %rax
	movq	16(%rax), %rdi
	movl	8(%rax), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.13, %edi
	callq	_ZN2kc9Problem1SEPKc
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc7WarningEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	popq	%rbx
	jmp	_ZN2kc8v_reportEPNS_10impl_errorE # TAILCALL
.LBB24_3:
	movl	$.L.str.12, %edi
	movl	$1067, %esi             # imm = 0x42B
	movl	$.L.str.1, %edx
	popq	%rbx
	jmp	_ZN2kc21kc_no_default_in_withEPKciS1_ # TAILCALL
.LBB24_4:
	movl	$.L.str.1, %edi
	movl	$1084, %esi             # imm = 0x43C
	movl	$.L.str.14, %edx
	callq	_ZN2kc24kc_assertionReasonFailedEPKciS1_
.Lfunc_end24:
	.size	_ZN2kc27warn_drop_identical_patternEPNS_20impl_unparsedeclinfoE, .Lfunc_end24-_ZN2kc27warn_drop_identical_patternEPNS_20impl_unparsedeclinfoE
	.cfi_endproc

	.globl	_ZN2kc22check_rewrite_patternsEPNS_21impl_rewriterulesinfoE
	.p2align	4, 0x90
	.type	_ZN2kc22check_rewrite_patternsEPNS_21impl_rewriterulesinfoE,@function
_ZN2kc22check_rewrite_patternsEPNS_21impl_rewriterulesinfoE: # @_ZN2kc22check_rewrite_patternsEPNS_21impl_rewriterulesinfoE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi160:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi161:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi162:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi163:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi164:
	.cfi_def_cfa_offset 48
.Lcfi165:
	.cfi_offset %rbx, -40
.Lcfi166:
	.cfi_offset %r12, -32
.Lcfi167:
	.cfi_offset %r14, -24
.Lcfi168:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	callq	_ZN2kc38f_outmost_nl_preds_in_rewriterulesinfoEPNS_21impl_rewriterulesinfoE
	testq	%rax, %rax
	je	.LBB25_2
# BB#1:
	movq	16(%rax), %rdi
	movl	8(%rax), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.15, %edi
	callq	_ZN2kc9Problem1SEPKc
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc7WarningEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB25_2:
	callq	_ZN2kc25NilpatternrepresentationsEv
	movq	%rax, %r14
	jmp	.LBB25_3
	.p2align	4, 0x90
.LBB25_6:                               #   in Loop: Header=BB25_3 Depth=1
	movq	16(%r15), %r15
.LBB25_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$218, %eax
	jne	.LBB25_7
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB25_3 Depth=1
	movq	8(%r15), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$219, %eax
	jne	.LBB25_6
# BB#5:                                 #   in Loop: Header=BB25_3 Depth=1
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	callq	_ZN2kc26ConspatternrepresentationsEPNS_26impl_patternrepresentationEPNS_27impl_patternrepresentationsE
	movq	%rax, %r14
	jmp	.LBB25_6
.LBB25_7:                               # %._crit_edge
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB25_15
# BB#8:
	movq	16(%r14), %rdi
	movq	(%rdi), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB25_15
# BB#9:                                 # %.preheader21.i
	leaq	16(%r14), %r12
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB25_15
# BB#10:                                # %.preheader.i.preheader
	movq	%r14, %r15
	.p2align	4, 0x90
.LBB25_11:                              # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_13 Depth 2
	movq	(%r12), %rbx
	jmp	.LBB25_13
	.p2align	4, 0x90
.LBB25_12:                              #   in Loop: Header=BB25_13 Depth=2
	movq	8(%rbx), %rsi
	movq	8(%r15), %rdi
	movq	%r15, %rdx
	callq	_ZN2kc16compare_patternsEPNS_26impl_patternrepresentationES1_PNS_27impl_patternrepresentationsE
	movq	16(%rbx), %rbx
.LBB25_13:                              #   Parent Loop BB25_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$199, %eax
	je	.LBB25_12
# BB#14:                                # %._crit_edge.i
                                        #   in Loop: Header=BB25_11 Depth=1
	movq	(%r12), %r15
	leaq	16(%r15), %r12
	movq	16(%r15), %rdi
	movq	(%rdi), %rax
	callq	*88(%rax)
	testb	%al, %al
	je	.LBB25_11
.LBB25_15:                              # %_ZN2kc14check_patternsEPNS_27impl_patternrepresentationsE.exit
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN2kc18impl_abstract_list8freelistEv # TAILCALL
.Lfunc_end25:
	.size	_ZN2kc22check_rewrite_patternsEPNS_21impl_rewriterulesinfoE, .Lfunc_end25-_ZN2kc22check_rewrite_patternsEPNS_21impl_rewriterulesinfoE
	.cfi_endproc

	.globl	_ZN2kc14check_patternsEPNS_27impl_patternrepresentationsE
	.p2align	4, 0x90
	.type	_ZN2kc14check_patternsEPNS_27impl_patternrepresentationsE,@function
_ZN2kc14check_patternsEPNS_27impl_patternrepresentationsE: # @_ZN2kc14check_patternsEPNS_27impl_patternrepresentationsE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi169:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi170:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi171:
	.cfi_def_cfa_offset 32
.Lcfi172:
	.cfi_offset %rbx, -32
.Lcfi173:
	.cfi_offset %r14, -24
.Lcfi174:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	(%r14), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB26_8
# BB#1:
	movq	16(%r14), %rdi
	movq	(%rdi), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB26_8
# BB#2:                                 # %.preheader21
	leaq	16(%r14), %r15
	movq	(%r15), %rdi
	jmp	.LBB26_3
	.p2align	4, 0x90
.LBB26_7:                               # %._crit_edge
                                        #   in Loop: Header=BB26_3 Depth=1
	movq	(%r15), %r14
	leaq	16(%r14), %r15
	movq	16(%r14), %rdi
.LBB26_3:                               # %.preheader21
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_6 Depth 2
	movq	(%rdi), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB26_8
# BB#4:                                 # %.preheader
                                        #   in Loop: Header=BB26_3 Depth=1
	movq	(%r15), %rbx
	jmp	.LBB26_6
	.p2align	4, 0x90
.LBB26_5:                               #   in Loop: Header=BB26_6 Depth=2
	movq	8(%rbx), %rsi
	movq	8(%r14), %rdi
	movq	%r14, %rdx
	callq	_ZN2kc16compare_patternsEPNS_26impl_patternrepresentationES1_PNS_27impl_patternrepresentationsE
	movq	16(%rbx), %rbx
.LBB26_6:                               #   Parent Loop BB26_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$199, %eax
	je	.LBB26_5
	jmp	.LBB26_7
.LBB26_8:                               # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end26:
	.size	_ZN2kc14check_patternsEPNS_27impl_patternrepresentationsE, .Lfunc_end26-_ZN2kc14check_patternsEPNS_27impl_patternrepresentationsE
	.cfi_endproc

	.globl	_ZN2kc19check_with_patternsEPNS_18impl_withcasesinfoE
	.p2align	4, 0x90
	.type	_ZN2kc19check_with_patternsEPNS_18impl_withcasesinfoE,@function
_ZN2kc19check_with_patternsEPNS_18impl_withcasesinfoE: # @_ZN2kc19check_with_patternsEPNS_18impl_withcasesinfoE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi175:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi176:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi177:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi178:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi179:
	.cfi_def_cfa_offset 48
.Lcfi180:
	.cfi_offset %rbx, -48
.Lcfi181:
	.cfi_offset %r12, -40
.Lcfi182:
	.cfi_offset %r13, -32
.Lcfi183:
	.cfi_offset %r14, -24
.Lcfi184:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	callq	_ZN2kc25NilpatternrepresentationsEv
	movq	%rax, %r15
	jmp	.LBB27_1
	.p2align	4, 0x90
.LBB27_4:                               #   in Loop: Header=BB27_1 Depth=1
	movq	16(%rbx), %rbx
.LBB27_1:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$221, %eax
	jne	.LBB27_5
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB27_1 Depth=1
	movq	8(%rbx), %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$222, %eax
	jne	.LBB27_4
# BB#3:                                 #   in Loop: Header=BB27_1 Depth=1
	movq	8(%r14), %rdi
	movq	%r15, %rsi
	callq	_ZN2kc26ConspatternrepresentationsEPNS_26impl_patternrepresentationEPNS_27impl_patternrepresentationsE
	movq	%rax, %r15
	jmp	.LBB27_4
.LBB27_5:                               # %._crit_edge
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*96(%rax)
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB27_13
# BB#6:
	movq	16(%r14), %rdi
	movq	(%rdi), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB27_13
# BB#7:                                 # %.preheader21.i
	movq	%r14, %r13
	addq	$16, %r13
	movq	(%r13), %rdi
	movq	(%rdi), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB27_13
# BB#8:                                 # %.preheader.i.preheader
	movq	%r14, %r12
	.p2align	4, 0x90
.LBB27_9:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_11 Depth 2
	movq	(%r13), %rbx
	jmp	.LBB27_11
	.p2align	4, 0x90
.LBB27_10:                              #   in Loop: Header=BB27_11 Depth=2
	movq	8(%rbx), %rsi
	movq	8(%r12), %rdi
	movq	%r12, %rdx
	callq	_ZN2kc16compare_patternsEPNS_26impl_patternrepresentationES1_PNS_27impl_patternrepresentationsE
	movq	16(%rbx), %rbx
.LBB27_11:                              #   Parent Loop BB27_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$199, %eax
	je	.LBB27_10
# BB#12:                                # %._crit_edge.i
                                        #   in Loop: Header=BB27_9 Depth=1
	movq	(%r13), %r12
	leaq	16(%r12), %r13
	movq	16(%r12), %rdi
	movq	(%rdi), %rax
	callq	*88(%rax)
	testb	%al, %al
	je	.LBB27_9
.LBB27_13:                              # %_ZN2kc14check_patternsEPNS_27impl_patternrepresentationsE.exit
	movq	%r15, %rdi
	callq	_ZN2kc18impl_abstract_list8freelistEv
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	_ZN2kc18impl_abstract_list8freelistEv # TAILCALL
.Lfunc_end27:
	.size	_ZN2kc19check_with_patternsEPNS_18impl_withcasesinfoE, .Lfunc_end27-_ZN2kc19check_with_patternsEPNS_18impl_withcasesinfoE
	.cfi_endproc

	.globl	_ZN2kc22check_unparse_patternsEPNS_21impl_unparsedeclsinfoE
	.p2align	4, 0x90
	.type	_ZN2kc22check_unparse_patternsEPNS_21impl_unparsedeclsinfoE,@function
_ZN2kc22check_unparse_patternsEPNS_21impl_unparsedeclsinfoE: # @_ZN2kc22check_unparse_patternsEPNS_21impl_unparsedeclsinfoE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi185:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi186:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi187:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi188:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi189:
	.cfi_def_cfa_offset 48
.Lcfi190:
	.cfi_offset %rbx, -40
.Lcfi191:
	.cfi_offset %r12, -32
.Lcfi192:
	.cfi_offset %r14, -24
.Lcfi193:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	callq	_ZN2kc38f_outmost_nl_preds_in_unparsedeclsinfoEPNS_21impl_unparsedeclsinfoE
	testq	%rax, %rax
	je	.LBB28_2
# BB#1:
	movq	16(%rax), %rdi
	movl	8(%rax), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.15, %edi
	callq	_ZN2kc9Problem1SEPKc
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc7WarningEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB28_2:
	callq	_ZN2kc25NilpatternrepresentationsEv
	movq	%rax, %r14
	jmp	.LBB28_3
	.p2align	4, 0x90
.LBB28_6:                               #   in Loop: Header=BB28_3 Depth=1
	movq	16(%r15), %r15
.LBB28_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$230, %eax
	jne	.LBB28_7
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB28_3 Depth=1
	movq	8(%r15), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$231, %eax
	jne	.LBB28_6
# BB#5:                                 #   in Loop: Header=BB28_3 Depth=1
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	callq	_ZN2kc26ConspatternrepresentationsEPNS_26impl_patternrepresentationEPNS_27impl_patternrepresentationsE
	movq	%rax, %r14
	jmp	.LBB28_6
.LBB28_7:                               # %._crit_edge
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB28_15
# BB#8:
	movq	16(%r14), %rdi
	movq	(%rdi), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB28_15
# BB#9:                                 # %.preheader21.i
	leaq	16(%r14), %r12
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB28_15
# BB#10:                                # %.preheader.i.preheader
	movq	%r14, %r15
	.p2align	4, 0x90
.LBB28_11:                              # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB28_13 Depth 2
	movq	(%r12), %rbx
	jmp	.LBB28_13
	.p2align	4, 0x90
.LBB28_12:                              #   in Loop: Header=BB28_13 Depth=2
	movq	8(%rbx), %rsi
	movq	8(%r15), %rdi
	movq	%r15, %rdx
	callq	_ZN2kc16compare_patternsEPNS_26impl_patternrepresentationES1_PNS_27impl_patternrepresentationsE
	movq	16(%rbx), %rbx
.LBB28_13:                              #   Parent Loop BB28_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$199, %eax
	je	.LBB28_12
# BB#14:                                # %._crit_edge.i
                                        #   in Loop: Header=BB28_11 Depth=1
	movq	(%r12), %r15
	leaq	16(%r15), %r12
	movq	16(%r15), %rdi
	movq	(%rdi), %rax
	callq	*88(%rax)
	testb	%al, %al
	je	.LBB28_11
.LBB28_15:                              # %_ZN2kc14check_patternsEPNS_27impl_patternrepresentationsE.exit
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN2kc18impl_abstract_list8freelistEv # TAILCALL
.Lfunc_end28:
	.size	_ZN2kc22check_unparse_patternsEPNS_21impl_unparsedeclsinfoE, .Lfunc_end28-_ZN2kc22check_unparse_patternsEPNS_21impl_unparsedeclsinfoE
	.cfi_endproc

	.globl	_ZN2kc16compare_patternsEPNS_26impl_patternrepresentationES1_PNS_27impl_patternrepresentationsE
	.p2align	4, 0x90
	.type	_ZN2kc16compare_patternsEPNS_26impl_patternrepresentationES1_PNS_27impl_patternrepresentationsE,@function
_ZN2kc16compare_patternsEPNS_26impl_patternrepresentationES1_PNS_27impl_patternrepresentationsE: # @_ZN2kc16compare_patternsEPNS_26impl_patternrepresentationES1_PNS_27impl_patternrepresentationsE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi194:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi195:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi196:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi197:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi198:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi199:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi200:
	.cfi_def_cfa_offset 80
.Lcfi201:
	.cfi_offset %rbx, -56
.Lcfi202:
	.cfi_offset %r12, -48
.Lcfi203:
	.cfi_offset %r13, -40
.Lcfi204:
	.cfi_offset %r14, -32
.Lcfi205:
	.cfi_offset %r15, -24
.Lcfi206:
	.cfi_offset %rbp, -16
	movq	%rdx, %r13
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	8(%r15), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	8(%r14), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	callq	_ZN2kc24NilpatternrepresentationEv
	movq	%rax, %r12
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*88(%rax)
	testb	%al, %al
	je	.LBB29_2
# BB#1:
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movl	$0, (%rsp)              # 4-byte Folded Spill
	jmp	.LBB29_7
.LBB29_2:                               # %.lr.ph487.preheader
	movl	$0, (%rsp)              # 4-byte Folded Spill
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB29_3:                               # %.lr.ph487
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB29_7
# BB#4:                                 #   in Loop: Header=BB29_3 Depth=1
	movq	8(%r15), %rdi
	movq	8(%r14), %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	movq	8(%r15), %rbp
	testb	%al, %al
	je	.LBB29_10
# BB#5:                                 #   in Loop: Header=BB29_3 Depth=1
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	_ZN2kc26impl_patternrepresentation6appendEPNS_31impl_elem_patternrepresentationE
	movq	16(%r15), %r15
	movq	16(%r14), %r14
	jmp	.LBB29_6
	.p2align	4, 0x90
.LBB29_10:                              #   in Loop: Header=BB29_3 Depth=1
	movq	8(%r14), %rbx
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$207, %eax
	jne	.LBB29_14
# BB#11:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$207, %eax
	je	.LBB29_12
.LBB29_14:                              #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$208, %eax
	jne	.LBB29_16
# BB#15:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$208, %eax
	je	.LBB29_12
.LBB29_16:                              #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$210, %eax
	jne	.LBB29_25
# BB#17:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$209, %eax
	jne	.LBB29_25
# BB#18:                                #   in Loop: Header=BB29_3 Depth=1
	movq	32(%rbp), %rdi
	movq	32(%rbx), %rsi
	callq	_ZN2kcL10equal_pathEPNS_9impl_pathES1_
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$197, %eax
	je	.LBB29_19
# BB#20:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$196, %eax
	je	.LBB29_21
# BB#22:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$195, %eax
	je	.LBB29_78
# BB#23:                                #   in Loop: Header=BB29_3 Depth=1
	movl	$.L.str.18, %edi
	movl	$1671, %esi             # imm = 0x687
	jmp	.LBB29_24
	.p2align	4, 0x90
.LBB29_25:                              #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$209, %eax
	jne	.LBB29_31
# BB#26:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$210, %eax
	jne	.LBB29_31
# BB#27:                                #   in Loop: Header=BB29_3 Depth=1
	movq	32(%rbp), %rdi
	movq	32(%rbx), %rsi
	callq	_ZN2kcL10equal_pathEPNS_9impl_pathES1_
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$197, %eax
	je	.LBB29_19
# BB#28:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$196, %eax
	je	.LBB29_21
# BB#29:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$195, %eax
	je	.LBB29_78
# BB#30:                                #   in Loop: Header=BB29_3 Depth=1
	movl	$.L.str.18, %edi
	movl	$1699, %esi             # imm = 0x6A3
	jmp	.LBB29_24
	.p2align	4, 0x90
.LBB29_31:                              #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$204, %eax
	jne	.LBB29_37
# BB#32:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$210, %eax
	jne	.LBB29_37
# BB#33:                                #   in Loop: Header=BB29_3 Depth=1
	movq	32(%rbp), %rdi
	movq	32(%rbx), %rsi
	callq	_ZN2kcL10equal_pathEPNS_9impl_pathES1_
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$197, %eax
	je	.LBB29_19
# BB#34:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$196, %eax
	je	.LBB29_21
# BB#35:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$195, %eax
	je	.LBB29_78
# BB#36:                                #   in Loop: Header=BB29_3 Depth=1
	movl	$.L.str.18, %edi
	movl	$1727, %esi             # imm = 0x6BF
	jmp	.LBB29_24
.LBB29_37:                              #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$210, %eax
	jne	.LBB29_43
# BB#38:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$204, %eax
	jne	.LBB29_43
# BB#39:                                #   in Loop: Header=BB29_3 Depth=1
	movq	32(%rbp), %rdi
	movq	32(%rbx), %rsi
	callq	_ZN2kcL10equal_pathEPNS_9impl_pathES1_
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$197, %eax
	je	.LBB29_19
# BB#40:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$196, %eax
	je	.LBB29_21
# BB#41:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$195, %eax
	je	.LBB29_78
# BB#42:                                #   in Loop: Header=BB29_3 Depth=1
	movl	$.L.str.18, %edi
	movl	$1755, %esi             # imm = 0x6DB
	jmp	.LBB29_24
.LBB29_43:                              #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$204, %eax
	jne	.LBB29_49
# BB#44:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$209, %eax
	jne	.LBB29_49
# BB#45:                                #   in Loop: Header=BB29_3 Depth=1
	movq	32(%rbp), %rdi
	movq	32(%rbx), %rsi
	callq	_ZN2kcL10equal_pathEPNS_9impl_pathES1_
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$197, %eax
	je	.LBB29_19
# BB#46:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$196, %eax
	je	.LBB29_21
# BB#47:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$195, %eax
	je	.LBB29_78
# BB#48:                                #   in Loop: Header=BB29_3 Depth=1
	movl	$.L.str.18, %edi
	movl	$1783, %esi             # imm = 0x6F7
	jmp	.LBB29_24
.LBB29_49:                              #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$209, %eax
	jne	.LBB29_55
# BB#50:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$204, %eax
	jne	.LBB29_55
# BB#51:                                #   in Loop: Header=BB29_3 Depth=1
	movq	32(%rbp), %rdi
	movq	32(%rbx), %rsi
	callq	_ZN2kcL10equal_pathEPNS_9impl_pathES1_
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$197, %eax
	je	.LBB29_19
# BB#52:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$196, %eax
	je	.LBB29_21
# BB#53:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$195, %eax
	je	.LBB29_78
# BB#54:                                #   in Loop: Header=BB29_3 Depth=1
	movl	$.L.str.18, %edi
	movl	$1811, %esi             # imm = 0x713
	jmp	.LBB29_24
.LBB29_55:                              #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$210, %eax
	jne	.LBB29_61
# BB#56:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$210, %eax
	jne	.LBB29_61
# BB#57:                                #   in Loop: Header=BB29_3 Depth=1
	movq	32(%rbp), %rdi
	movq	32(%rbx), %rsi
	callq	_ZN2kcL10equal_pathEPNS_9impl_pathES1_
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$197, %eax
	je	.LBB29_19
# BB#58:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$196, %eax
	je	.LBB29_21
# BB#59:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$195, %eax
	je	.LBB29_78
# BB#60:                                #   in Loop: Header=BB29_3 Depth=1
	movl	$.L.str.18, %edi
	movl	$1839, %esi             # imm = 0x72F
	jmp	.LBB29_24
.LBB29_61:                              #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$209, %eax
	jne	.LBB29_67
# BB#62:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$209, %eax
	jne	.LBB29_67
# BB#63:                                #   in Loop: Header=BB29_3 Depth=1
	movq	32(%rbp), %rdi
	movq	32(%rbx), %rsi
	callq	_ZN2kcL10equal_pathEPNS_9impl_pathES1_
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$197, %eax
	je	.LBB29_19
# BB#64:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$196, %eax
	je	.LBB29_21
# BB#65:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$195, %eax
	je	.LBB29_78
# BB#66:                                #   in Loop: Header=BB29_3 Depth=1
	movl	$.L.str.18, %edi
	movl	$1867, %esi             # imm = 0x74B
	jmp	.LBB29_24
.LBB29_67:                              #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$204, %eax
	jne	.LBB29_73
# BB#68:                                #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$204, %eax
	jne	.LBB29_73
# BB#69:                                #   in Loop: Header=BB29_3 Depth=1
	movq	32(%rbp), %rdi
	movq	32(%rbx), %rsi
	callq	_ZN2kcL10equal_pathEPNS_9impl_pathES1_
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$197, %eax
	jne	.LBB29_70
	.p2align	4, 0x90
.LBB29_19:                              #   in Loop: Header=BB29_3 Depth=1
	movq	8(%r14), %rsi
	movq	%r12, %rdi
	callq	_ZN2kc26impl_patternrepresentation6appendEPNS_31impl_elem_patternrepresentationE
	movq	16(%r14), %r14
	movb	$1, %al
	movl	%eax, 4(%rsp)           # 4-byte Spill
	jmp	.LBB29_6
.LBB29_70:                              #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$196, %eax
	jne	.LBB29_71
.LBB29_21:                              #   in Loop: Header=BB29_3 Depth=1
	movq	8(%r15), %rsi
	movq	%r12, %rdi
	callq	_ZN2kc26impl_patternrepresentation6appendEPNS_31impl_elem_patternrepresentationE
	movq	16(%r15), %r15
	movb	$1, %al
	movl	%eax, (%rsp)            # 4-byte Spill
	jmp	.LBB29_6
.LBB29_71:                              #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$195, %eax
	je	.LBB29_78
# BB#72:                                #   in Loop: Header=BB29_3 Depth=1
	movl	$.L.str.18, %edi
	movl	$1895, %esi             # imm = 0x767
.LBB29_24:                              # %.thread.backedge
                                        #   in Loop: Header=BB29_3 Depth=1
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	.p2align	4, 0x90
.LBB29_6:                               # %.thread.backedge
                                        #   in Loop: Header=BB29_3 Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*88(%rax)
	testb	%al, %al
	je	.LBB29_3
.LBB29_7:                               # %.critedge
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*88(%rax)
	testb	%al, %al
	je	.LBB29_79
# BB#8:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*88(%rax)
	testb	%al, %al
	je	.LBB29_79
# BB#9:
	movl	4(%rsp), %ebp           # 4-byte Reload
	movl	(%rsp), %ecx            # 4-byte Reload
	jmp	.LBB29_83
.LBB29_79:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*88(%rax)
	testb	%al, %al
	cmoveq	%r15, %r14
	movb	$1, %r15b
	jne	.LBB29_80
# BB#81:
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB29_82
.LBB29_80:
	movb	$1, %bpl
	movl	(%rsp), %eax            # 4-byte Reload
	movl	%eax, %r15d
.LBB29_82:
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZN2kc6concatEPKNS_26impl_patternrepresentationES2_
	movq	%rax, %rbx
	movq	%r12, %rdi
	callq	_ZN2kc18impl_abstract_list8freelistEv
	movq	%rbx, %r12
	movb	%r15b, %cl
.LBB29_83:
	movl	%ecx, %eax
	orb	%bpl, %al
	testb	$1, %al
	jne	.LBB29_87
# BB#84:
	cmpb	$0, g_options+345(%rip)
	je	.LBB29_78
# BB#85:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	16(%rax), %rdi
	movl	8(%rax), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rax
	movq	8(%rax), %rsi
	movl	8(%rcx), %ecx
	movl	$.L.str.19, %edi
	movl	$.L.str.20, %edx
	movl	$.L.str.21, %r8d
	jmp	.LBB29_86
.LBB29_87:
	andb	%cl, %bpl
	testb	$1, %bpl
	je	.LBB29_78
# BB#88:                                # %.preheader
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$199, %eax
	jne	.LBB29_89
# BB#90:                                # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB29_91:                              # =>This Inner Loop Header: Depth=1
	movq	8(%r13), %rdi
	movq	%r12, %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	movb	$1, %bl
	jne	.LBB29_93
# BB#92:                                #   in Loop: Header=BB29_91 Depth=1
	movb	%bpl, %bl
.LBB29_93:                              #   in Loop: Header=BB29_91 Depth=1
	movq	16(%r13), %r13
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$199, %eax
	movb	%bl, %bpl
	je	.LBB29_91
# BB#94:                                # %._crit_edge.loopexit
	andb	$1, %bl
	testb	%bl, %bl
	je	.LBB29_96
	jmp	.LBB29_78
.LBB29_89:
	xorl	%ebx, %ebx
	testb	%bl, %bl
	jne	.LBB29_78
.LBB29_96:                              # %._crit_edge
	movb	g_options+346(%rip), %al
	testb	%al, %al
	je	.LBB29_78
# BB#97:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	16(%rax), %rdi
	movl	8(%rax), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rax
	movq	8(%rax), %rsi
	movl	8(%rcx), %ecx
	movl	$.L.str.22, %edi
	movl	$.L.str.20, %edx
	movl	$.L.str.23, %r8d
.LBB29_86:
	callq	_ZN2kc15Problem3S1int1SEPKcS1_S1_iS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc7WarningEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc8v_reportEPNS_10impl_errorE # TAILCALL
.LBB29_12:
	cmpb	$0, g_options+65(%rip)
	je	.LBB29_78
.LBB29_13:
	movl	$.Lstr.2, %edi
	callq	puts
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	16(%rcx), %rax
	movq	8(%rax), %rsi
	movl	8(%rcx), %edx
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	printf
	movq	8(%r15), %rdi
	callq	_ZN2kc20impl_abstract_phylum5printEv
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rax
	movq	8(%rax), %rsi
	movl	8(%rcx), %edx
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	printf
	movq	8(%r14), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc20impl_abstract_phylum5printEv # TAILCALL
.LBB29_73:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$207, %eax
	je	.LBB29_78
# BB#74:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$208, %eax
	je	.LBB29_78
# BB#75:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$207, %eax
	jne	.LBB29_76
.LBB29_78:                              # %.thread438
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB29_76:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$208, %eax
	je	.LBB29_78
# BB#77:
	movb	g_options+65(%rip), %al
	testb	%al, %al
	jne	.LBB29_13
	jmp	.LBB29_78
.Lfunc_end29:
	.size	_ZN2kc16compare_patternsEPNS_26impl_patternrepresentationES1_PNS_27impl_patternrepresentationsE, .Lfunc_end29-_ZN2kc16compare_patternsEPNS_26impl_patternrepresentationES1_PNS_27impl_patternrepresentationsE
	.cfi_endproc

	.globl	_ZN2kc4nextEPNS_26impl_patternrepresentationE
	.p2align	4, 0x90
	.type	_ZN2kc4nextEPNS_26impl_patternrepresentationE,@function
_ZN2kc4nextEPNS_26impl_patternrepresentationE: # @_ZN2kc4nextEPNS_26impl_patternrepresentationE
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	retq
.Lfunc_end30:
	.size	_ZN2kc4nextEPNS_26impl_patternrepresentationE, .Lfunc_end30-_ZN2kc4nextEPNS_26impl_patternrepresentationE
	.cfi_endproc

	.globl	_ZN2kc4elemEPNS_26impl_patternrepresentationE
	.p2align	4, 0x90
	.type	_ZN2kc4elemEPNS_26impl_patternrepresentationE,@function
_ZN2kc4elemEPNS_26impl_patternrepresentationE: # @_ZN2kc4elemEPNS_26impl_patternrepresentationE
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	retq
.Lfunc_end31:
	.size	_ZN2kc4elemEPNS_26impl_patternrepresentationE, .Lfunc_end31-_ZN2kc4elemEPNS_26impl_patternrepresentationE
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL10equal_pathEPNS_9impl_pathES1_,@function
_ZN2kcL10equal_pathEPNS_9impl_pathES1_: # @_ZN2kcL10equal_pathEPNS_9impl_pathES1_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi207:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi208:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi209:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi210:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi211:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi212:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi213:
	.cfi_def_cfa_offset 64
.Lcfi214:
	.cfi_offset %rbx, -56
.Lcfi215:
	.cfi_offset %r12, -48
.Lcfi216:
	.cfi_offset %r13, -40
.Lcfi217:
	.cfi_offset %r14, -32
.Lcfi218:
	.cfi_offset %r15, -24
.Lcfi219:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	(%rdi), %rax
	callq	*96(%rax)
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*96(%rax)
	movq	%rax, %r14
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	xorl	%ebp, %ebp
	cmpl	$212, %eax
	jne	.LBB32_1
# BB#2:                                 # %.lr.ph.preheader
	movq	%r15, %rbx
                                        # implicit-def: %R12
	movq	%r14, %r13
	.p2align	4, 0x90
.LBB32_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$212, %eax
	jne	.LBB32_11
# BB#4:                                 #   in Loop: Header=BB32_3 Depth=1
	testb	$1, %bpl
	jne	.LBB32_10
# BB#5:                                 #   in Loop: Header=BB32_3 Depth=1
	movq	24(%r13), %rax
	movq	24(%rbx), %rcx
	movl	8(%rax), %eax
	cmpl	%eax, 8(%rcx)
	jge	.LBB32_7
# BB#6:                                 #   in Loop: Header=BB32_3 Depth=1
	callq	_ZN2kc7SmallerEv
	jmp	.LBB32_9
	.p2align	4, 0x90
.LBB32_7:                               #   in Loop: Header=BB32_3 Depth=1
	jle	.LBB32_10
# BB#8:                                 #   in Loop: Header=BB32_3 Depth=1
	callq	_ZN2kc6BiggerEv
.LBB32_9:                               #   in Loop: Header=BB32_3 Depth=1
	movq	%rax, %r12
	movb	$1, %bpl
.LBB32_10:                              #   in Loop: Header=BB32_3 Depth=1
	movq	32(%rbx), %rbx
	movq	32(%r13), %r13
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$212, %eax
	je	.LBB32_3
	jmp	.LBB32_11
.LBB32_1:
	movq	%r14, %r13
	movq	%r15, %rbx
                                        # implicit-def: %R12
.LBB32_11:                              # %.critedge
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$211, %eax
	jne	.LBB32_15
# BB#12:
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$212, %eax
	jne	.LBB32_15
# BB#13:
	testb	$1, %bpl
	jne	.LBB32_24
# BB#14:
	callq	_ZN2kc6BiggerEv
	jmp	.LBB32_23
.LBB32_15:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$212, %eax
	jne	.LBB32_19
# BB#16:
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$211, %eax
	jne	.LBB32_19
# BB#17:
	testb	$1, %bpl
	jne	.LBB32_24
# BB#18:
	callq	_ZN2kc7SmallerEv
	jmp	.LBB32_23
.LBB32_19:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$211, %eax
	jne	.LBB32_26
# BB#20:
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$211, %eax
	jne	.LBB32_26
# BB#21:
	testb	$1, %bpl
	jne	.LBB32_24
# BB#22:
	callq	_ZN2kc5EqualEv
.LBB32_23:
	movq	%rax, %r12
.LBB32_24:
	movq	%r15, %rdi
	callq	_ZN2kc18impl_abstract_list8freelistEv
	movq	%r14, %rdi
	callq	_ZN2kc18impl_abstract_list8freelistEv
	jmp	.LBB32_25
.LBB32_26:
	movl	$.L.str.41, %edi
	movl	$1376, %esi             # imm = 0x560
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%r12d, %r12d
.LBB32_25:
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end32:
	.size	_ZN2kcL10equal_pathEPNS_9impl_pathES1_, .Lfunc_end32-_ZN2kcL10equal_pathEPNS_9impl_pathES1_
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL14t_syn_patternsEPNS_13impl_patternsEPNS_9impl_pathEi,@function
_ZN2kcL14t_syn_patternsEPNS_13impl_patternsEPNS_9impl_pathEi: # @_ZN2kcL14t_syn_patternsEPNS_13impl_patternsEPNS_9impl_pathEi
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi220:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi221:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi222:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi223:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi224:
	.cfi_def_cfa_offset 48
.Lcfi225:
	.cfi_offset %rbx, -40
.Lcfi226:
	.cfi_offset %r12, -32
.Lcfi227:
	.cfi_offset %r14, -24
.Lcfi228:
	.cfi_offset %r15, -16
	movl	%edx, %r15d
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$92, %eax
	jne	.LBB33_1
# BB#3:
	movq	8(%rbx), %r12
	movq	16(%rbx), %rdi
	leal	-1(%r15), %edx
	movq	%r14, %rsi
	callq	_ZN2kcL14t_syn_patternsEPNS_13impl_patternsEPNS_9impl_pathEi
	movq	%rax, %rbx
	movl	%r15d, %edi
	callq	_ZN2kc9mkintegerEi
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	_ZN2kc8ConspathEPNS_17impl_integer__IntEPNS_9impl_pathE
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	_ZN2kcL11syn_patternEPNS_12impl_patternEPNS_9impl_pathE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN2kc6concatEPKNS_26impl_patternrepresentationES2_ # TAILCALL
.LBB33_1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$91, %eax
	jne	.LBB33_2
# BB#4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN2kc24NilpatternrepresentationEv # TAILCALL
.LBB33_2:
	movl	$.L.str.27, %edi
	movl	$421, %esi              # imm = 0x1A5
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end33:
	.size	_ZN2kcL14t_syn_patternsEPNS_13impl_patternsEPNS_9impl_pathEi, .Lfunc_end33-_ZN2kcL14t_syn_patternsEPNS_13impl_patternsEPNS_9impl_pathEi
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL11syn_patternEPNS_12impl_patternEPNS_9impl_pathE,@function
_ZN2kcL11syn_patternEPNS_12impl_patternEPNS_9impl_pathE: # @_ZN2kcL11syn_patternEPNS_12impl_patternEPNS_9impl_pathE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi229:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi230:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi231:
	.cfi_def_cfa_offset 32
.Lcfi232:
	.cfi_offset %rbx, -32
.Lcfi233:
	.cfi_offset %r14, -24
.Lcfi234:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$90, %eax
	jne	.LBB34_2
# BB#1:
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	_ZN2kc12PRIntLiteralEPNS_9impl_pathEPNS_8impl_INTE
	jmp	.LBB34_4
.LBB34_2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$89, %eax
	jne	.LBB34_7
# BB#3:
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	_ZN2kc15PRStringLiteralEPNS_9impl_pathEPNS_18impl_CexpressionDQE
.LBB34_4:
	movq	%rax, %rbx
	callq	_ZN2kc24NilpatternrepresentationEv
	movq	%rbx, %rdi
.LBB34_5:
	movq	%rax, %rsi
.LBB34_6:
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN2kc25ConspatternrepresentationEPNS_31impl_elem_patternrepresentationEPNS_26impl_patternrepresentationE # TAILCALL
.LBB34_7:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$88, %eax
	jne	.LBB34_9
# BB#8:
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN2kc24NilpatternrepresentationEv # TAILCALL
.LBB34_9:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$87, %eax
	jne	.LBB34_11
# BB#10:
	movq	8(%rbx), %r15
	movq	16(%rbx), %rbx
	callq	_ZN2kc7NilpathEv
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kcL11syn_patternEPNS_12impl_patternEPNS_9impl_pathE
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc16PRNonLeafBindingEPNS_9impl_pathEPNS_7impl_IDEPNS_26impl_patternrepresentationE
	movq	%rax, %r15
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN2kcL11syn_patternEPNS_12impl_patternEPNS_9impl_pathE
	movq	%r15, %rdi
	jmp	.LBB34_5
.LBB34_11:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$86, %eax
	jne	.LBB34_14
# BB#12:
	movq	8(%rbx), %r15
	movq	16(%rbx), %rbx
	movq	%r15, 8(%r14)
	xorl	%edi, %edi
	callq	_ZN2kc9mkintegerEi
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	_ZN2kc8ConspathEPNS_17impl_integer__IntEPNS_9impl_pathE
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	_ZN2kc15PROperPredicateEPNS_9impl_pathEPNS_7impl_IDE
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$92, %eax
	jne	.LBB34_16
# BB#13:
	movq	%rbx, %rdi
	callq	_ZNK2kc18impl_abstract_list6lengthEv
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	%eax, %edx
	callq	_ZN2kcL14t_syn_patternsEPNS_13impl_patternsEPNS_9impl_pathEi
	jmp	.LBB34_18
.LBB34_14:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$85, %eax
	jne	.LBB34_19
# BB#15:
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	_ZN2kc9PRBindingEPNS_9impl_pathEPNS_7impl_IDE
	jmp	.LBB34_4
.LBB34_16:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$91, %eax
	jne	.LBB34_20
# BB#17:
	callq	_ZN2kc24NilpatternrepresentationEv
.LBB34_18:
	movq	%rax, %rsi
	movq	%r15, %rdi
	jmp	.LBB34_6
.LBB34_19:
	movl	$.L.str.28, %edi
	movl	$381, %esi              # imm = 0x17D
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB34_20:
	movl	$.L.str.26, %edi
	movl	$400, %esi              # imm = 0x190
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%esi, %esi
	movq	%r15, %rdi
	jmp	.LBB34_6
.Lfunc_end34:
	.size	_ZN2kcL11syn_patternEPNS_12impl_patternEPNS_9impl_pathE, .Lfunc_end34-_ZN2kcL11syn_patternEPNS_12impl_patternEPNS_9impl_pathE
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL15make_predicatesEPNS_31impl_elem_patternrepresentationEPNS_26impl_patternrepresentationE,@function
_ZN2kcL15make_predicatesEPNS_31impl_elem_patternrepresentationEPNS_26impl_patternrepresentationE: # @_ZN2kcL15make_predicatesEPNS_31impl_elem_patternrepresentationEPNS_26impl_patternrepresentationE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi235:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi236:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi237:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi238:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi239:
	.cfi_def_cfa_offset 48
.Lcfi240:
	.cfi_offset %rbx, -40
.Lcfi241:
	.cfi_offset %r12, -32
.Lcfi242:
	.cfi_offset %r14, -24
.Lcfi243:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$206, %eax
	jne	.LBB35_3
# BB#1:
	movq	32(%rbx), %r15
	movq	40(%rbx), %r12
	movq	48(%rbx), %rbx
	callq	_ZN2kc8NilpathsEv
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc9ConspathsEPNS_9impl_pathEPNS_10impl_pathsE
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rbx, %rdx
	jmp	.LBB35_2
.LBB35_3:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$202, %eax
	jne	.LBB35_5
# BB#4:
	movq	32(%rbx), %r15
	movq	40(%rbx), %r12
	callq	_ZN2kc8NilpathsEv
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc9ConspathsEPNS_9impl_pathEPNS_10impl_pathsE
	movq	%rax, %rbx
	callq	_ZN2kc24NilpatternrepresentationEv
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
.LBB35_2:
	movq	%r14, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN2kcL17t_make_predicatesEPNS_7impl_IDEPNS_10impl_pathsEPNS_26impl_patternrepresentationES5_b # TAILCALL
.LBB35_5:
	movl	$.L.str.30, %edi
	movl	$658, %esi              # imm = 0x292
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end35:
	.size	_ZN2kcL15make_predicatesEPNS_31impl_elem_patternrepresentationEPNS_26impl_patternrepresentationE, .Lfunc_end35-_ZN2kcL15make_predicatesEPNS_31impl_elem_patternrepresentationEPNS_26impl_patternrepresentationE
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL17t_make_predicatesEPNS_7impl_IDEPNS_10impl_pathsEPNS_26impl_patternrepresentationES5_b,@function
_ZN2kcL17t_make_predicatesEPNS_7impl_IDEPNS_10impl_pathsEPNS_26impl_patternrepresentationES5_b: # @_ZN2kcL17t_make_predicatesEPNS_7impl_IDEPNS_10impl_pathsEPNS_26impl_patternrepresentationES5_b
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi244:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi245:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi246:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi247:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi248:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi249:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi250:
	.cfi_def_cfa_offset 64
.Lcfi251:
	.cfi_offset %rbx, -56
.Lcfi252:
	.cfi_offset %r12, -48
.Lcfi253:
	.cfi_offset %r13, -40
.Lcfi254:
	.cfi_offset %r14, -32
.Lcfi255:
	.cfi_offset %r15, -24
.Lcfi256:
	.cfi_offset %rbp, -16
	movl	%r8d, %r13d
	movq	%rcx, %rbx
	movq	%rdx, %rbp
	movq	%rsi, %r15
	movq	%rdi, %r12
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$201, %eax
	je	.LBB36_2
	jmp	.LBB36_8
	.p2align	4, 0x90
.LBB36_11:                              # %tailrecurse.outer.outer
                                        #   in Loop: Header=BB36_2 Depth=1
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_ZN2kc9ConspathsEPNS_9impl_pathEPNS_10impl_pathsE
	movq	%rax, %r15
	movq	%rbp, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	_ZN2kc6concatEPKNS_26impl_patternrepresentationES2_
	movq	%rax, %rbp
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	xorl	%r13d, %r13d
	cmpl	$201, %eax
	jne	.LBB36_8
	.p2align	4, 0x90
.LBB36_2:                               # %.lr.ph.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB36_3 Depth 2
	movq	%rbp, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB36_3:                               #   Parent Loop BB36_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rbp
	movq	16(%rbx), %rbx
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$206, %eax
	jne	.LBB36_12
# BB#4:                                 #   in Loop: Header=BB36_3 Depth=2
	movq	32(%rbp), %r14
	movq	40(%rbp), %rsi
	movq	48(%rbp), %rbp
	movq	%r12, %rdi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	je	.LBB36_5
	jmp	.LBB36_11
	.p2align	4, 0x90
.LBB36_12:                              #   in Loop: Header=BB36_3 Depth=2
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$202, %eax
	jne	.LBB36_5
# BB#13:                                #   in Loop: Header=BB36_3 Depth=2
	movq	32(%rbp), %r14
	movq	40(%rbp), %rsi
	movq	%r12, %rdi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	jne	.LBB36_14
.LBB36_5:                               # %tailrecurse.backedge
                                        #   in Loop: Header=BB36_3 Depth=2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$201, %eax
	je	.LBB36_3
	jmp	.LBB36_7
.LBB36_14:                              # %tailrecurse.outer
                                        #   in Loop: Header=BB36_3 Depth=2
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_ZN2kc9ConspathsEPNS_9impl_pathEPNS_10impl_pathsE
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	xorl	%r13d, %r13d
	cmpl	$201, %eax
	je	.LBB36_3
.LBB36_7:
	movq	(%rsp), %rbp            # 8-byte Reload
.LBB36_8:                               # %tailrecurse.outer._crit_edge
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$200, %eax
	jne	.LBB36_15
# BB#9:
	testb	$1, %r13b
	je	.LBB36_10
# BB#16:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc24NilpatternrepresentationEv # TAILCALL
.LBB36_15:
	movl	$.L.str.31, %edi
	movl	$634, %esi              # imm = 0x27A
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB36_10:
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	_ZN2kc14PRVarPredicateEPNS_10impl_pathsEPNS_7impl_IDEPNS_26impl_patternrepresentationE
	movq	%rax, %rbx
	movq	32(%r12), %rax
	movq	%rax, 16(%rbx)
	movl	24(%r12), %eax
	movl	%eax, 8(%rbx)
	callq	_ZN2kc24NilpatternrepresentationEv
	movq	%rbx, %rdi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc25ConspatternrepresentationEPNS_31impl_elem_patternrepresentationEPNS_26impl_patternrepresentationE # TAILCALL
.Lfunc_end36:
	.size	_ZN2kcL17t_make_predicatesEPNS_7impl_IDEPNS_10impl_pathsEPNS_26impl_patternrepresentationES5_b, .Lfunc_end36-_ZN2kcL17t_make_predicatesEPNS_7impl_IDEPNS_10impl_pathsEPNS_26impl_patternrepresentationES5_b
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL17f_do_get_bindingsEPNS_26impl_patternrepresentationE,@function
_ZN2kcL17f_do_get_bindingsEPNS_26impl_patternrepresentationE: # @_ZN2kcL17f_do_get_bindingsEPNS_26impl_patternrepresentationE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi257:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi258:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi259:
	.cfi_def_cfa_offset 32
.Lcfi260:
	.cfi_offset %rbx, -32
.Lcfi261:
	.cfi_offset %r14, -24
.Lcfi262:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	.p2align	4, 0x90
.LBB37_1:                               # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$201, %eax
	jne	.LBB37_7
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB37_1 Depth=1
	movq	8(%r15), %r14
	movq	16(%r15), %r15
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$206, %eax
	je	.LBB37_3
# BB#11:                                #   in Loop: Header=BB37_1 Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$202, %eax
	jne	.LBB37_1
.LBB37_3:                               #   in Loop: Header=BB37_1 Depth=1
	movq	40(%r14), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB37_4
# BB#12:                                # %_ZN2kc17f_bindingidmarkedEPNS_7impl_IDE.exit
                                        #   in Loop: Header=BB37_1 Depth=1
	movq	40(%rbx), %rdi
	callq	_ZN2kc13BindingIdMarkEPNS_11impl_uniqIDE
	cmpb	$0, 8(%rax)
	jne	.LBB37_1
	jmp	.LBB37_5
.LBB37_7:                               # %tailrecurse._crit_edge
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$200, %eax
	jne	.LBB37_13
# BB#8:
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN2kc24NilpatternrepresentationEv # TAILCALL
.LBB37_13:
	movl	$.L.str.33, %edi
	movl	$907, %esi              # imm = 0x38B
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB37_4:                               # %_ZN2kc17f_bindingidmarkedEPNS_7impl_IDE.exit.thread
	movl	$.L.str.6, %edi
	movl	$436, %esi              # imm = 0x1B4
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB37_5:                               # %.loopexit36
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB37_9
# BB#6:
	movq	40(%rbx), %rdi
	callq	_ZN2kc13BindingIdMarkEPNS_11impl_uniqIDE
	movb	$1, 8(%rax)
	jmp	.LBB37_10
.LBB37_9:
	movl	$.L.str.7, %edi
	movl	$451, %esi              # imm = 0x1C3
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB37_10:                              # %_ZN2kc15v_markbindingidEPNS_7impl_IDE.exit
	movq	%r15, %rdi
	callq	_ZN2kcL17f_do_get_bindingsEPNS_26impl_patternrepresentationE
	movq	%r14, %rdi
	movq	%rax, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN2kc25ConspatternrepresentationEPNS_31impl_elem_patternrepresentationEPNS_26impl_patternrepresentationE # TAILCALL
.Lfunc_end37:
	.size	_ZN2kcL17f_do_get_bindingsEPNS_26impl_patternrepresentationE, .Lfunc_end37-_ZN2kcL17f_do_get_bindingsEPNS_26impl_patternrepresentationE
	.cfi_endproc

	.type	Thebindingidmarks,@object # @Thebindingidmarks
	.bss
	.globl	Thebindingidmarks
	.p2align	3
Thebindingidmarks:
	.quad	0
	.size	Thebindingidmarks, 8

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"syn_patternchains"
	.size	.L.str, 18

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/kimwitu++/pat.cc"
	.size	.L.str.1, 80

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"syn_patternchain"
	.size	.L.str.2, 17

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Internal Error: PatternchainitemGroup was not handled correctly"
	.size	.L.str.3, 64

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"syn_patternchainitem"
	.size	.L.str.4, 21

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"syn_outmostpatterns"
	.size	.L.str.5, 20

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"f_bindingidmarked"
	.size	.L.str.6, 18

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"v_markbindingid"
	.size	.L.str.7, 16

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"add_predicates_to_patternrepresentations"
	.size	.L.str.8, 41

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"f_withcasesinfo"
	.size	.L.str.9, 16

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"insertin_withcasesinfo"
	.size	.L.str.10, 23

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"lt_withcaseinfo"
	.size	.L.str.11, 16

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"warn_drop_identical_pattern"
	.size	.L.str.12, 28

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Warning: dropped pattern"
	.size	.L.str.13, 25

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Dropping empty pattern"
	.size	.L.str.14, 23

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Cannot handle outmost non-leaf predicates"
	.size	.L.str.15, 42

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"%s:%d "
	.size	.L.str.17, 7

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"compare_patterns"
	.size	.L.str.18, 17

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"pattern equivalent to"
	.size	.L.str.19, 22

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"line"
	.size	.L.str.20, 5

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"(will never match)"
	.size	.L.str.21, 19

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"pattern overlaps"
	.size	.L.str.22, 17

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"(which will match?)"
	.size	.L.str.23, 20

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"t_syn_patternchain"
	.size	.L.str.24, 19

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"syn_outmostpattern"
	.size	.L.str.25, 19

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"syn_patterns"
	.size	.L.str.26, 13

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"t_syn_patterns"
	.size	.L.str.27, 15

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"syn_pattern"
	.size	.L.str.28, 12

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"add_predicates"
	.size	.L.str.29, 15

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"make_predicates"
	.size	.L.str.30, 16

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"t_make_predicates"
	.size	.L.str.31, 18

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"f_get_predicates"
	.size	.L.str.32, 17

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"f_do_get_bindings"
	.size	.L.str.33, 18

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"insertin_rewriterulesinfo"
	.size	.L.str.34, 26

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"lt_rewriteruleinfo"
	.size	.L.str.35, 19

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"insertin_unparsedeclsinfo"
	.size	.L.str.36, 26

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"lt_unparsedeclinfo"
	.size	.L.str.37, 19

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"lt_patternrepresentation"
	.size	.L.str.38, 25

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"equal_elem_patternrepresentation"
	.size	.L.str.39, 33

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"equal_paths"
	.size	.L.str.40, 12

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"equal_path"
	.size	.L.str.41, 11

	.type	.Lstr.2,@object         # @str.2
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.2:
	.asciz	"Don't know how to compare these yet:"
	.size	.Lstr.2, 37


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
