	.text
	.file	"parset.bc"
	.globl	Scaling_List
	.p2align	4, 0x90
	.type	Scaling_List,@function
Scaling_List:                           # @Scaling_List
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%rdx, %r15
	movq	%rdi, %r13
	testl	%esi, %esi
	jle	.LBB0_10
# BB#1:                                 # %.lr.ph
	cmpl	$16, %esi
	jne	.LBB0_6
# BB#2:                                 # %.lr.ph.split.us.preheader
	movl	$8, %eax
	xorl	%r12d, %r12d
	movl	$8, %ebx
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movzbl	ZZ_SCAN(%r12), %ebp
	testl	%eax, %eax
	movl	$0, %eax
	je	.LBB0_5
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	movl	$.L.str, %edi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	se_v
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rbx,%rax), %ecx
	leal	256(%rbx,%rax), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$24, %edx
	leal	256(%rdx,%rcx), %ecx
	andl	$-256, %ecx
	subl	%ecx, %eax
	xorl	%ecx, %ecx
	movl	%r12d, %edx
	orl	%eax, %edx
	sete	%cl
	movl	%ecx, (%r15)
.LBB0_5:                                #   in Loop: Header=BB0_3 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %ebx
	movl	%ebx, (%r13,%rbp,4)
	incq	%r12
	cmpq	$16, %r12
	jne	.LBB0_3
	jmp	.LBB0_10
.LBB0_6:                                # %.lr.ph.split.preheader
	movl	%esi, %r12d
	movl	$8, %eax
	xorl	%ebx, %ebx
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movzbl	ZZ_SCAN8(%rbx), %r14d
	testl	%eax, %eax
	movl	$0, %eax
	je	.LBB0_9
# BB#8:                                 #   in Loop: Header=BB0_7 Depth=1
	movl	$.L.str, %edi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	se_v
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rbp,%rax), %ecx
	leal	256(%rbp,%rax), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$24, %edx
	leal	256(%rdx,%rcx), %ecx
	andl	$-256, %ecx
	subl	%ecx, %eax
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	orl	%eax, %edx
	sete	%cl
	movl	%ecx, (%r15)
.LBB0_9:                                #   in Loop: Header=BB0_7 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %ebp
	movl	%ebp, (%r13,%r14,4)
	incq	%rbx
	cmpq	%rbx, %r12
	jne	.LBB0_7
.LBB0_10:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	Scaling_List, .Lfunc_end0-Scaling_List
	.cfi_endproc

	.globl	InterpretSPS
	.p2align	4, 0x90
	.type	InterpretSPS,@function
InterpretSPS:                           # @InterpretSPS
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%r14), %rbp
	movl	$0, UsedBits(%rip)
	movl	$8, %edi
	movl	$.L.str.1, %esi
	movq	%rbp, %rdx
	callq	u_v
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movl	%eax, 4(%rbx)
	leal	-66(%rax), %ecx
	cmpl	$56, %ecx
	ja	.LBB1_1
# BB#2:
	movabsq	$72075203408037889, %rdx # imm = 0x100100400400801
	btq	%rcx, %rdx
	jae	.LBB1_1
.LBB1_3:
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movl	$.L.str.2, %edi
	movq	%rbp, %rsi
	callq	u_1
	movl	%eax, 8(%rbx)
	movl	$.L.str.3, %edi
	movq	%rbp, %rsi
	callq	u_1
	movl	%eax, 12(%rbx)
	movl	$.L.str.4, %edi
	movq	%rbp, %rsi
	callq	u_1
	movl	%eax, 16(%rbx)
	movl	$.L.str.5, %edi
	movq	%rbp, %rsi
	callq	u_1
	movl	%eax, 20(%rbx)
	movl	$4, %edi
	movl	$.L.str.6, %esi
	movq	%rbp, %rdx
	callq	u_v
	movl	$8, %edi
	movl	$.L.str.7, %esi
	movq	%rbp, %rdx
	callq	u_v
	movl	%eax, 24(%rbx)
	movl	$.L.str.8, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movl	%eax, 28(%rbx)
	movl	$1, 32(%rbx)
	movl	$0, 1000(%rbx)
	movl	$0, 1004(%rbx)
	movq	img(%rip), %rax
	movl	$0, 5920(%rax)
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movl	4(%rbx), %eax
	addl	$-100, %eax
	cmpl	$44, %eax
	ja	.LBB1_23
# BB#4:
	movabsq	$17592190239745, %rcx   # imm = 0x100000400401
	btq	%rax, %rcx
	jae	.LBB1_23
# BB#5:
	movl	$.L.str.9, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%eax, 32(%rcx)
	cmpl	$3, %eax
	jne	.LBB1_8
# BB#6:
	movl	$.L.str.10, %edi
	movq	%rbp, %rsi
	callq	u_1
	cmpl	$1, %eax
	jne	.LBB1_8
# BB#7:
	movl	$.L.str.11, %edi
	movl	$1000, %esi             # imm = 0x3E8
	callq	error
.LBB1_8:
	movl	$.L.str.12, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	%eax, 1000(%rbx)
	movl	$.L.str.13, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movl	%eax, 1004(%rbx)
	movl	$.L.str.14, %edi
	movq	%rbp, %rsi
	callq	u_1
	movq	img(%rip), %rcx
	movl	%eax, 5920(%rcx)
	movl	$.L.str.15, %edi
	movq	%rbp, %rsi
	callq	u_1
	movl	%eax, 36(%rbx)
	testl	%eax, %eax
	je	.LBB1_23
# BB#9:                                 # %.preheader.preheader
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB1_10:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_18 Depth 2
                                        #     Child Loop BB1_13 Depth 2
	movl	$.L.str.16, %edi
	movq	%rbp, %r12
	movq	%rbp, %rsi
	callq	u_1
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%eax, 40(%rsi,%r13,4)
	testl	%eax, %eax
	je	.LBB1_22
# BB#11:                                #   in Loop: Header=BB1_10 Depth=1
	cmpq	$5, %r13
	ja	.LBB1_17
# BB#12:                                #   in Loop: Header=BB1_10 Depth=1
	movl	$8, %eax
	xorl	%ebx, %ebx
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB1_13:                               # %.lr.ph.split.us.i
                                        #   Parent Loop BB1_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	ZZ_SCAN(%rbx), %r14d
	testl	%eax, %eax
	je	.LBB1_14
# BB#15:                                #   in Loop: Header=BB1_13 Depth=2
	movl	$.L.str, %edi
	movq	%r12, %rsi
	callq	se_v
	movq	8(%rsp), %rsi           # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rbp,%rax), %ecx
	leal	256(%rbp,%rax), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$24, %edx
	leal	256(%rdx,%rcx), %ecx
	andl	$-256, %ecx
	subl	%ecx, %eax
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	orl	%eax, %edx
	sete	%cl
	movl	%ecx, 968(%rsi,%r13,4)
	jmp	.LBB1_16
	.p2align	4, 0x90
.LBB1_14:                               #   in Loop: Header=BB1_13 Depth=2
	xorl	%eax, %eax
.LBB1_16:                               #   in Loop: Header=BB1_13 Depth=2
	testl	%eax, %eax
	cmovnel	%eax, %ebp
	movq	%r13, %rcx
	shlq	$6, %rcx
	addq	%rsi, %rcx
	movl	%ebp, 72(%rcx,%r14,4)
	incq	%rbx
	cmpq	$16, %rbx
	jne	.LBB1_13
	jmp	.LBB1_22
	.p2align	4, 0x90
.LBB1_17:                               #   in Loop: Header=BB1_10 Depth=1
	leal	-6(%r13), %ebx
	movl	$8, %eax
	xorl	%ebp, %ebp
	movl	$8, %r14d
	.p2align	4, 0x90
.LBB1_18:                               # %.lr.ph.split.i
                                        #   Parent Loop BB1_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	ZZ_SCAN8(%rbp), %r15d
	testl	%eax, %eax
	je	.LBB1_19
# BB#20:                                #   in Loop: Header=BB1_18 Depth=2
	movl	$.L.str, %edi
	movq	%r12, %rsi
	callq	se_v
	movq	8(%rsp), %rsi           # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%r14,%rax), %ecx
	leal	256(%r14,%rax), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$24, %edx
	leal	256(%rdx,%rcx), %ecx
	andl	$-256, %ecx
	subl	%ecx, %eax
	xorl	%ecx, %ecx
	movl	%ebp, %edx
	orl	%eax, %edx
	sete	%cl
	movl	%ecx, 992(%rsi,%rbx,4)
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_19:                               #   in Loop: Header=BB1_18 Depth=2
	xorl	%eax, %eax
.LBB1_21:                               #   in Loop: Header=BB1_18 Depth=2
	testl	%eax, %eax
	cmovnel	%eax, %r14d
	movq	%rbx, %rcx
	shlq	$8, %rcx
	addq	%rsi, %rcx
	movl	%r14d, 456(%rcx,%r15,4)
	incq	%rbp
	cmpq	$64, %rbp
	jne	.LBB1_18
.LBB1_22:                               # %Scaling_List.exit
                                        #   in Loop: Header=BB1_10 Depth=1
	incq	%r13
	cmpq	$8, %r13
	movq	%r12, %rbp
	jne	.LBB1_10
.LBB1_23:                               # %.loopexit122
	movl	$.L.str.17, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movq	8(%rsp), %r14           # 8-byte Reload
	movl	%eax, 1008(%r14)
	movl	$.L.str.18, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movl	%eax, 1012(%r14)
	cmpl	$1, %eax
	je	.LBB1_26
# BB#24:                                # %.loopexit122
	testl	%eax, %eax
	jne	.LBB1_29
# BB#25:
	movl	$.L.str.19, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movl	%eax, 1016(%r14)
	jmp	.LBB1_29
.LBB1_26:
	movl	$.L.str.20, %edi
	movq	%rbp, %rsi
	callq	u_1
	movl	%eax, 1020(%r14)
	movl	$.L.str.21, %edi
	movq	%rbp, %rsi
	callq	se_v
	movl	%eax, 1024(%r14)
	movl	$.L.str.22, %edi
	movq	%rbp, %rsi
	callq	se_v
	movl	%eax, 1028(%r14)
	movl	$.L.str.23, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movl	%eax, 1032(%r14)
	testl	%eax, %eax
	je	.LBB1_29
# BB#27:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_28:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.24, %edi
	movq	%rbp, %rsi
	callq	se_v
	movl	%ebx, %ecx
	movl	%eax, 1036(%r14,%rcx,4)
	incl	%ebx
	cmpl	1032(%r14), %ebx
	jb	.LBB1_28
.LBB1_29:                               # %.loopexit
	movl	$.L.str.25, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movl	%eax, 2060(%r14)
	movl	$.L.str.26, %edi
	movq	%rbp, %rsi
	callq	u_1
	movl	%eax, 2064(%r14)
	movl	$.L.str.27, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movl	%eax, 2068(%r14)
	movl	$.L.str.28, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movl	%eax, 2072(%r14)
	movl	$.L.str.29, %edi
	movq	%rbp, %rsi
	callq	u_1
	movl	%eax, 2076(%r14)
	testl	%eax, %eax
	jne	.LBB1_31
# BB#30:
	movl	$.L.str.30, %edi
	movq	%rbp, %rsi
	callq	u_1
	movl	%eax, 2080(%r14)
.LBB1_31:
	movl	$.L.str.31, %edi
	movq	%rbp, %rsi
	callq	u_1
	movl	%eax, 2084(%r14)
	movl	$.L.str.32, %edi
	movq	%rbp, %rsi
	callq	u_1
	movl	%eax, 2088(%r14)
	testl	%eax, %eax
	movq	16(%rsp), %rbx          # 8-byte Reload
	je	.LBB1_33
# BB#32:
	movl	$.L.str.33, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movl	%eax, 2092(%r14)
	movl	$.L.str.34, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movl	%eax, 2096(%r14)
	movl	$.L.str.35, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movl	%eax, 2100(%r14)
	movl	$.L.str.36, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movl	%eax, 2104(%r14)
.LBB1_33:
	movl	$.L.str.37, %edi
	movq	%rbp, %rsi
	callq	u_1
	movl	%eax, 2108(%r14)
	movl	$2, 2160(%r14)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	ReadVUI
	movl	$1, (%r14)
.LBB1_34:
	movl	UsedBits(%rip), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_1:
	cmpl	$144, %eax
	je	.LBB1_3
	jmp	.LBB1_34
.Lfunc_end1:
	.size	InterpretSPS, .Lfunc_end1-InterpretSPS
	.cfi_endproc

	.globl	InitVUI
	.p2align	4, 0x90
	.type	InitVUI,@function
InitVUI:                                # @InitVUI
	.cfi_startproc
# BB#0:
	movl	$2, 2160(%rdi)
	retq
.Lfunc_end2:
	.size	InitVUI, .Lfunc_end2-InitVUI
	.cfi_endproc

	.globl	ReadVUI
	.p2align	4, 0x90
	.type	ReadVUI,@function
ReadVUI:                                # @ReadVUI
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	cmpl	$0, 2108(%rbx)
	je	.LBB3_22
# BB#1:
	movq	(%r15), %r14
	movl	$.L.str.38, %edi
	movq	%r14, %rsi
	callq	u_1
	movl	%eax, 2112(%rbx)
	testl	%eax, %eax
	je	.LBB3_4
# BB#2:
	movl	$8, %edi
	movl	$.L.str.39, %esi
	movq	%r14, %rdx
	callq	u_v
	movl	%eax, 2116(%rbx)
	cmpl	$255, %eax
	jne	.LBB3_4
# BB#3:
	movl	$16, %edi
	movl	$.L.str.40, %esi
	movq	%r14, %rdx
	callq	u_v
	movl	%eax, 2120(%rbx)
	movl	$16, %edi
	movl	$.L.str.41, %esi
	movq	%r14, %rdx
	callq	u_v
	movl	%eax, 2124(%rbx)
.LBB3_4:
	movl	$.L.str.42, %edi
	movq	%r14, %rsi
	callq	u_1
	movl	%eax, 2128(%rbx)
	testl	%eax, %eax
	je	.LBB3_6
# BB#5:
	movl	$.L.str.43, %edi
	movq	%r14, %rsi
	callq	u_1
	movl	%eax, 2132(%rbx)
.LBB3_6:
	movl	$.L.str.44, %edi
	movq	%r14, %rsi
	callq	u_1
	movl	%eax, 2136(%rbx)
	testl	%eax, %eax
	je	.LBB3_9
# BB#7:
	movl	$3, %edi
	movl	$.L.str.45, %esi
	movq	%r14, %rdx
	callq	u_v
	movl	%eax, 2140(%rbx)
	movl	$.L.str.46, %edi
	movq	%r14, %rsi
	callq	u_1
	movl	%eax, 2144(%rbx)
	movl	$.L.str.47, %edi
	movq	%r14, %rsi
	callq	u_1
	movl	%eax, 2148(%rbx)
	testl	%eax, %eax
	je	.LBB3_9
# BB#8:
	movl	$8, %edi
	movl	$.L.str.48, %esi
	movq	%r14, %rdx
	callq	u_v
	movl	%eax, 2152(%rbx)
	movl	$8, %edi
	movl	$.L.str.49, %esi
	movq	%r14, %rdx
	callq	u_v
	movl	%eax, 2156(%rbx)
	movl	$8, %edi
	movl	$.L.str.50, %esi
	movq	%r14, %rdx
	callq	u_v
	movl	%eax, 2160(%rbx)
.LBB3_9:
	movl	$.L.str.51, %edi
	movq	%r14, %rsi
	callq	u_1
	movl	%eax, 2164(%rbx)
	testl	%eax, %eax
	je	.LBB3_11
# BB#10:
	movl	$.L.str.52, %edi
	movq	%r14, %rsi
	callq	ue_v
	movl	%eax, 2168(%rbx)
	movl	$.L.str.53, %edi
	movq	%r14, %rsi
	callq	ue_v
	movl	%eax, 2172(%rbx)
.LBB3_11:
	movl	$.L.str.54, %edi
	movq	%r14, %rsi
	callq	u_1
	movl	%eax, 2176(%rbx)
	testl	%eax, %eax
	je	.LBB3_13
# BB#12:
	movl	$32, %edi
	movl	$.L.str.55, %esi
	movq	%r14, %rdx
	callq	u_v
	movl	%eax, 2180(%rbx)
	movl	$32, %edi
	movl	$.L.str.56, %esi
	movq	%r14, %rdx
	callq	u_v
	movl	%eax, 2184(%rbx)
	movl	$.L.str.57, %edi
	movq	%r14, %rsi
	callq	u_1
	movl	%eax, 2188(%rbx)
.LBB3_13:
	movl	$.L.str.58, %edi
	movq	%r14, %rsi
	callq	u_1
	movl	%eax, 2192(%rbx)
	testl	%eax, %eax
	je	.LBB3_15
# BB#14:
	leaq	2196(%rbx), %rsi
	movq	%r15, %rdi
	callq	ReadHRDParameters
.LBB3_15:
	movl	$.L.str.59, %edi
	movq	%r14, %rsi
	callq	u_1
	movl	%eax, 2608(%rbx)
	testl	%eax, %eax
	je	.LBB3_17
# BB#16:
	leaq	2612(%rbx), %rsi
	movq	%r15, %rdi
	callq	ReadHRDParameters
.LBB3_17:
	cmpl	$0, 2192(%rbx)
	jne	.LBB3_19
# BB#18:
	cmpl	$0, 2608(%rbx)
	je	.LBB3_20
.LBB3_19:
	movl	$.L.str.60, %edi
	movq	%r14, %rsi
	callq	u_1
	movl	%eax, 3024(%rbx)
.LBB3_20:
	movl	$.L.str.61, %edi
	movq	%r14, %rsi
	callq	u_1
	movl	%eax, 3028(%rbx)
	movl	$.L.str.62, %edi
	movq	%r14, %rsi
	callq	u_1
	movl	%eax, 3032(%rbx)
	testl	%eax, %eax
	je	.LBB3_22
# BB#21:
	movl	$.L.str.63, %edi
	movq	%r14, %rsi
	callq	u_1
	movl	%eax, 3036(%rbx)
	movl	$.L.str.64, %edi
	movq	%r14, %rsi
	callq	ue_v
	movl	%eax, 3040(%rbx)
	movl	$.L.str.65, %edi
	movq	%r14, %rsi
	callq	ue_v
	movl	%eax, 3044(%rbx)
	movl	$.L.str.66, %edi
	movq	%r14, %rsi
	callq	ue_v
	movl	%eax, 3052(%rbx)
	movl	$.L.str.67, %edi
	movq	%r14, %rsi
	callq	ue_v
	movl	%eax, 3048(%rbx)
	movl	$.L.str.68, %edi
	movq	%r14, %rsi
	callq	ue_v
	movl	%eax, 3056(%rbx)
	movl	$.L.str.69, %edi
	movq	%r14, %rsi
	callq	ue_v
	movl	%eax, 3060(%rbx)
.LBB3_22:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	ReadVUI, .Lfunc_end3-ReadVUI
	.cfi_endproc

	.globl	ReadHRDParameters
	.p2align	4, 0x90
	.type	ReadHRDParameters,@function
ReadHRDParameters:                      # @ReadHRDParameters
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi36:
	.cfi_def_cfa_offset 48
.Lcfi37:
	.cfi_offset %rbx, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	(%rdi), %r14
	movl	$.L.str.70, %edi
	movq	%r14, %rsi
	callq	ue_v
	movl	%eax, (%r15)
	movl	$4, %edi
	movl	$.L.str.71, %esi
	movq	%r14, %rdx
	callq	u_v
	movl	%eax, 4(%r15)
	movl	$4, %edi
	movl	$.L.str.72, %esi
	movq	%r14, %rdx
	callq	u_v
	movl	%eax, 8(%r15)
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	movl	$.L.str.73, %edi
	movq	%r14, %rsi
	callq	ue_v
	movl	%ebp, %ebx
	movl	%eax, 12(%r15,%rbx,4)
	movl	$.L.str.74, %edi
	movq	%r14, %rsi
	callq	ue_v
	movl	%eax, 140(%r15,%rbx,4)
	movl	$.L.str.75, %edi
	movq	%r14, %rsi
	callq	u_1
	movl	%eax, 268(%r15,%rbx,4)
	incl	%ebp
	cmpl	(%r15), %ebp
	jbe	.LBB4_1
# BB#2:
	movl	$5, %edi
	movl	$.L.str.76, %esi
	movq	%r14, %rdx
	callq	u_v
	movl	%eax, 396(%r15)
	movl	$5, %edi
	movl	$.L.str.77, %esi
	movq	%r14, %rdx
	callq	u_v
	movl	%eax, 400(%r15)
	movl	$5, %edi
	movl	$.L.str.78, %esi
	movq	%r14, %rdx
	callq	u_v
	movl	%eax, 404(%r15)
	movl	$5, %edi
	movl	$.L.str.79, %esi
	movq	%r14, %rdx
	callq	u_v
	movl	%eax, 408(%r15)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	ReadHRDParameters, .Lfunc_end4-ReadHRDParameters
	.cfi_endproc

	.globl	InterpretPPS
	.p2align	4, 0x90
	.type	InterpretPPS,@function
InterpretPPS:                           # @InterpretPPS
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi45:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi47:
	.cfi_def_cfa_offset 64
.Lcfi48:
	.cfi_offset %rbx, -56
.Lcfi49:
	.cfi_offset %r12, -48
.Lcfi50:
	.cfi_offset %r13, -40
.Lcfi51:
	.cfi_offset %r14, -32
.Lcfi52:
	.cfi_offset %r15, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	(%rdi), %rbp
	movl	$0, UsedBits(%rip)
	movl	$.L.str.80, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movl	%eax, 4(%rbx)
	movl	$.L.str.81, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movl	%eax, 8(%rbx)
	movl	$.L.str.82, %edi
	movq	%rbp, %rsi
	callq	u_1
	movl	%eax, 12(%rbx)
	movl	$.L.str.83, %edi
	movq	%rbp, %rsi
	callq	u_1
	movl	%eax, 984(%rbx)
	movl	$.L.str.84, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movq	%rbx, %rsi
	movl	%eax, 988(%rsi)
	testl	%eax, %eax
	movq	%rsi, (%rsp)            # 8-byte Spill
	je	.LBB5_11
# BB#1:
	movl	$.L.str.85, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%eax, 992(%rsi)
	cmpl	$6, %eax
	ja	.LBB5_11
# BB#2:
	movl	%eax, %eax
	jmpq	*.LJTI5_0(,%rax,8)
.LBB5_8:
	movl	$.L.str.89, %edi
	movq	%rbp, %rsi
	callq	u_1
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%eax, 1092(%rcx)
	movl	$.L.str.90, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%eax, 1096(%rsi)
	jmp	.LBB5_11
.LBB5_6:                                # %.preheader110.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_7:                                # %.preheader110
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.86, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%ebx, %ecx
	movl	%eax, 996(%rsi,%rcx,4)
	incl	%ebx
	cmpl	988(%rsi), %ebx
	jbe	.LBB5_7
	jmp	.LBB5_11
.LBB5_3:                                # %.preheader112
	cmpl	$0, 988(%rsi)
	je	.LBB5_11
# BB#4:                                 # %.lr.ph118.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_5:                                # %.lr.ph118
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.87, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movl	%ebx, %r14d
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%eax, 1028(%rcx,%r14,4)
	movl	$.L.str.88, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%eax, 1060(%rsi,%r14,4)
	incl	%ebx
	cmpl	988(%rsi), %ebx
	jb	.LBB5_5
	jmp	.LBB5_11
.LBB5_9:
	movl	988(%rsi), %eax
	incl	%eax
	xorl	%ecx, %ecx
	cmpl	$2, %eax
	seta	%cl
	incl	%ecx
	cmpl	$4, %eax
	movl	$3, %r14d
	cmovbel	%ecx, %r14d
	movl	$.L.str.91, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%eax, 1100(%rcx)
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_10:                               # =>This Inner Loop Header: Depth=1
	movl	$.L.str.92, %esi
	movl	%r14d, %edi
	movq	%rbp, %rdx
	callq	u_v
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	1104(%rsi), %rcx
	movl	%ebx, %edx
	movl	%eax, (%rcx,%rdx,4)
	incl	%ebx
	cmpl	1100(%rsi), %ebx
	jbe	.LBB5_10
.LBB5_11:                               # %.loopexit111
	movl	$.L.str.93, %edi
	movq	%rsi, %rbx
	movq	%rbp, %rsi
	callq	ue_v
	movl	%eax, 1112(%rbx)
	movl	$.L.str.94, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movl	%eax, 1116(%rbx)
	movl	$.L.str.95, %edi
	movq	%rbp, %rsi
	callq	u_1
	movl	%eax, 1120(%rbx)
	movl	$2, %edi
	movl	$.L.str.96, %esi
	movq	%rbp, %rdx
	callq	u_v
	movl	%eax, 1124(%rbx)
	movl	$.L.str.97, %edi
	movq	%rbp, %rsi
	callq	se_v
	movl	%eax, 1128(%rbx)
	movl	$.L.str.98, %edi
	movq	%rbp, %rsi
	callq	se_v
	movl	%eax, 1132(%rbx)
	movl	$.L.str.99, %edi
	movq	%rbp, %rsi
	callq	se_v
	movl	%eax, 1136(%rbx)
	movl	$.L.str.100, %edi
	movq	%rbp, %rsi
	callq	u_1
	movl	%eax, 1144(%rbx)
	movl	$.L.str.101, %edi
	movq	%rbp, %rsi
	callq	u_1
	movl	%eax, 1148(%rbx)
	movl	$.L.str.102, %edi
	movq	%rbp, %rsi
	callq	u_1
	movl	%eax, 1152(%rbx)
	movq	16(%rbp), %rdi
	movl	8(%rbp), %esi
	movl	12(%rbp), %edx
	callq	more_rbsp_data
	testl	%eax, %eax
	je	.LBB5_29
# BB#12:
	movl	$.L.str.103, %edi
	movq	%rbp, %rsi
	callq	u_1
	movl	%eax, 16(%rbx)
	movl	$.L.str.104, %edi
	movq	%rbp, %rsi
	callq	u_1
	movl	%eax, 20(%rbx)
	testl	%eax, %eax
	je	.LBB5_28
# BB#13:                                # %.preheader
	movl	$2147483647, %eax       # imm = 0x7FFFFFFF
	andl	16(%rbx), %eax
	cmpl	$2147483645, %eax       # imm = 0x7FFFFFFD
	je	.LBB5_28
# BB#14:                                # %.lr.ph.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB5_15:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_23 Depth 2
                                        #     Child Loop BB5_18 Depth 2
	movl	$.L.str.105, %edi
	movq	%rbp, %r15
	movq	%rbp, %rsi
	callq	u_1
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%r12d, %r12d
	movl	%eax, 24(%rsi,%r12,4)
	testl	%eax, %eax
	je	.LBB5_27
# BB#16:                                #   in Loop: Header=BB5_15 Depth=1
	cmpl	$5, %r12d
	ja	.LBB5_22
# BB#17:                                #   in Loop: Header=BB5_15 Depth=1
	movl	$8, %eax
	xorl	%ebx, %ebx
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB5_18:                               # %.lr.ph.split.us.i
                                        #   Parent Loop BB5_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	ZZ_SCAN(%rbx), %r14d
	testl	%eax, %eax
	je	.LBB5_19
# BB#20:                                #   in Loop: Header=BB5_18 Depth=2
	movl	$.L.str, %edi
	movq	%r15, %rsi
	callq	se_v
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rbp,%rax), %ecx
	leal	256(%rbp,%rax), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$24, %edx
	leal	256(%rdx,%rcx), %ecx
	andl	$-256, %ecx
	subl	%ecx, %eax
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	orl	%eax, %edx
	sete	%cl
	movl	%ecx, 952(%rsi,%r12,4)
	jmp	.LBB5_21
	.p2align	4, 0x90
.LBB5_19:                               #   in Loop: Header=BB5_18 Depth=2
	xorl	%eax, %eax
.LBB5_21:                               #   in Loop: Header=BB5_18 Depth=2
	testl	%eax, %eax
	cmovnel	%eax, %ebp
	movq	%r12, %rcx
	shlq	$6, %rcx
	addq	%rsi, %rcx
	movl	%ebp, 56(%rcx,%r14,4)
	incq	%rbx
	cmpq	$16, %rbx
	jne	.LBB5_18
	jmp	.LBB5_27
	.p2align	4, 0x90
.LBB5_22:                               #   in Loop: Header=BB5_15 Depth=1
	leal	-6(%r12), %r13d
	movl	$8, %eax
	xorl	%ebp, %ebp
	movl	$8, %ebx
	.p2align	4, 0x90
.LBB5_23:                               # %.lr.ph.split.i
                                        #   Parent Loop BB5_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	ZZ_SCAN8(%rbp), %r14d
	testl	%eax, %eax
	je	.LBB5_24
# BB#25:                                #   in Loop: Header=BB5_23 Depth=2
	movl	$.L.str, %edi
	movq	%r15, %rsi
	callq	se_v
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rbx,%rax), %ecx
	leal	256(%rbx,%rax), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$24, %edx
	leal	256(%rdx,%rcx), %ecx
	andl	$-256, %ecx
	subl	%ecx, %eax
	xorl	%ecx, %ecx
	movl	%ebp, %edx
	orl	%eax, %edx
	sete	%cl
	movl	%ecx, 976(%rsi,%r13,4)
	jmp	.LBB5_26
	.p2align	4, 0x90
.LBB5_24:                               #   in Loop: Header=BB5_23 Depth=2
	xorl	%eax, %eax
.LBB5_26:                               #   in Loop: Header=BB5_23 Depth=2
	testl	%eax, %eax
	cmovnel	%eax, %ebx
	movq	%r13, %rcx
	shlq	$8, %rcx
	addq	%rsi, %rcx
	movl	%ebx, 440(%rcx,%r14,4)
	incq	%rbp
	cmpq	$64, %rbp
	jne	.LBB5_23
.LBB5_27:                               # %Scaling_List.exit
                                        #   in Loop: Header=BB5_15 Depth=1
	incl	%r12d
	movl	16(%rsi), %eax
	leal	6(%rax,%rax), %eax
	cmpl	%eax, %r12d
	movq	%r15, %rbp
	jb	.LBB5_15
.LBB5_28:                               # %.loopexit
	movl	$.L.str.106, %edi
	movq	%rbp, %rsi
	callq	se_v
	movq	(%rsp), %rcx            # 8-byte Reload
	jmp	.LBB5_30
.LBB5_29:
	movl	1136(%rbx), %eax
	movq	%rbx, %rcx
.LBB5_30:
	movl	%eax, 1140(%rcx)
	movl	$1, (%rcx)
	movl	UsedBits(%rip), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	InterpretPPS, .Lfunc_end5-InterpretPPS
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI5_0:
	.quad	.LBB5_6
	.quad	.LBB5_11
	.quad	.LBB5_3
	.quad	.LBB5_8
	.quad	.LBB5_8
	.quad	.LBB5_8
	.quad	.LBB5_9

	.text
	.globl	PPSConsistencyCheck
	.p2align	4, 0x90
	.type	PPSConsistencyCheck,@function
PPSConsistencyCheck:                    # @PPSConsistencyCheck
	.cfi_startproc
# BB#0:
	movl	$.Lstr, %edi
	jmp	puts                    # TAILCALL
.Lfunc_end6:
	.size	PPSConsistencyCheck, .Lfunc_end6-PPSConsistencyCheck
	.cfi_endproc

	.globl	SPSConsistencyCheck
	.p2align	4, 0x90
	.type	SPSConsistencyCheck,@function
SPSConsistencyCheck:                    # @SPSConsistencyCheck
	.cfi_startproc
# BB#0:
	movl	$.Lstr.1, %edi
	jmp	puts                    # TAILCALL
.Lfunc_end7:
	.size	SPSConsistencyCheck, .Lfunc_end7-SPSConsistencyCheck
	.cfi_endproc

	.globl	MakePPSavailable
	.p2align	4, 0x90
	.type	MakePPSavailable,@function
MakePPSavailable:                       # @MakePPSavailable
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi56:
	.cfi_def_cfa_offset 32
.Lcfi57:
	.cfi_offset %rbx, -24
.Lcfi58:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movslq	%edi, %rax
	imulq	$1160, %rax, %rax       # imm = 0x488
	leaq	PicParSet(%rax), %r14
	cmpl	$1, PicParSet(%rax)
	jne	.LBB8_3
# BB#1:
	movq	1104(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB8_3
# BB#2:
	callq	free
.LBB8_3:                                # %._crit_edge
	movl	$1160, %edx             # imm = 0x488
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	memcpy
	movq	1104(%rbx), %rax
	movq	%rax, 1104(%r14)
	movq	$0, 1104(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	MakePPSavailable, .Lfunc_end8-MakePPSavailable
	.cfi_endproc

	.globl	CleanUpPPS
	.p2align	4, 0x90
	.type	CleanUpPPS,@function
CleanUpPPS:                             # @CleanUpPPS
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 16
.Lcfi60:
	.cfi_offset %rbx, -16
	movq	$-296960, %rbx          # imm = 0xFFFB7800
	.p2align	4, 0x90
.LBB9_1:                                # =>This Inner Loop Header: Depth=1
	cmpl	$1, PicParSet+296960(%rbx)
	jne	.LBB9_4
# BB#2:                                 #   in Loop: Header=BB9_1 Depth=1
	movq	PicParSet+298064(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_4
# BB#3:                                 #   in Loop: Header=BB9_1 Depth=1
	callq	free
.LBB9_4:                                #   in Loop: Header=BB9_1 Depth=1
	movl	$0, PicParSet+296960(%rbx)
	addq	$1160, %rbx             # imm = 0x488
	jne	.LBB9_1
# BB#5:
	popq	%rbx
	retq
.Lfunc_end9:
	.size	CleanUpPPS, .Lfunc_end9-CleanUpPPS
	.cfi_endproc

	.globl	MakeSPSavailable
	.p2align	4, 0x90
	.type	MakeSPSavailable,@function
MakeSPSavailable:                       # @MakeSPSavailable
	.cfi_startproc
# BB#0:
	movslq	%edi, %rax
	imulq	$3064, %rax, %rax       # imm = 0xBF8
	leaq	SeqParSet(%rax), %rdi
	movl	$3064, %edx             # imm = 0xBF8
	jmp	memcpy                  # TAILCALL
.Lfunc_end10:
	.size	MakeSPSavailable, .Lfunc_end10-MakeSPSavailable
	.cfi_endproc

	.globl	ProcessSPS
	.p2align	4, 0x90
	.type	ProcessSPS,@function
ProcessSPS:                             # @ProcessSPS
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 32
.Lcfi64:
	.cfi_offset %rbx, -32
.Lcfi65:
	.cfi_offset %r14, -24
.Lcfi66:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$1, %edi
	xorl	%eax, %eax
	callq	AllocPartition
	movq	%rax, %r14
	xorl	%eax, %eax
	callq	AllocSPS
	movq	%rax, %r15
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	movq	24(%rbx), %rsi
	incq	%rsi
	movl	4(%rbx), %edx
	decl	%edx
	callq	memcpy
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	movl	4(%rbx), %esi
	decl	%esi
	callq	RBSPtoSODB
	movq	(%r14), %rcx
	movl	%eax, 12(%rcx)
	movl	%eax, 4(%rcx)
	movl	$0, 24(%rcx)
	movl	$0, 8(%rcx)
	movl	$0, (%rcx)
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	InterpretSPS
	cmpl	$0, (%r15)
	je	.LBB11_8
# BB#1:
	movq	active_sps(%rip), %rsi
	testq	%rsi, %rsi
	je	.LBB11_7
# BB#2:
	movl	28(%r15), %eax
	cmpl	28(%rsi), %eax
	jne	.LBB11_7
# BB#3:
	movq	%r15, %rdi
	callq	sps_is_equal
	testl	%eax, %eax
	jne	.LBB11_7
# BB#4:
	cmpq	$0, dec_picture(%rip)
	je	.LBB11_6
# BB#5:
	xorl	%eax, %eax
	callq	exit_picture
.LBB11_6:
	movq	$0, active_sps(%rip)
.LBB11_7:                               # %._crit_edge
	movslq	28(%r15), %rax
	imulq	$3064, %rax, %rax       # imm = 0xBF8
	leaq	SeqParSet(%rax), %rdi
	movl	$3064, %edx             # imm = 0xBF8
	movq	%r15, %rsi
	callq	memcpy
	movl	4(%r15), %eax
	movq	img(%rip), %rcx
	movl	%eax, 5912(%rcx)
.LBB11_8:
	movl	$1, %esi
	movq	%r14, %rdi
	callq	FreePartition
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	FreeSPS                 # TAILCALL
.Lfunc_end11:
	.size	ProcessSPS, .Lfunc_end11-ProcessSPS
	.cfi_endproc

	.globl	ProcessPPS
	.p2align	4, 0x90
	.type	ProcessPPS,@function
ProcessPPS:                             # @ProcessPPS
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 32
.Lcfi70:
	.cfi_offset %rbx, -32
.Lcfi71:
	.cfi_offset %r14, -24
.Lcfi72:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$1, %edi
	xorl	%eax, %eax
	callq	AllocPartition
	movq	%rax, %r14
	xorl	%eax, %eax
	callq	AllocPPS
	movq	%rax, %r15
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	movq	24(%rbx), %rsi
	incq	%rsi
	movl	4(%rbx), %edx
	decl	%edx
	callq	memcpy
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	movl	4(%rbx), %esi
	decl	%esi
	callq	RBSPtoSODB
	movq	(%r14), %rcx
	movl	%eax, 12(%rcx)
	movl	%eax, 4(%rcx)
	movl	$0, 24(%rcx)
	movl	$0, 8(%rcx)
	movl	$0, (%rcx)
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	InterpretPPS
	movq	active_pps(%rip), %rsi
	testq	%rsi, %rsi
	je	.LBB12_6
# BB#1:
	movl	4(%r15), %eax
	cmpl	4(%rsi), %eax
	jne	.LBB12_6
# BB#2:
	movq	%r15, %rdi
	callq	pps_is_equal
	testl	%eax, %eax
	jne	.LBB12_6
# BB#3:
	cmpq	$0, dec_picture(%rip)
	je	.LBB12_5
# BB#4:
	xorl	%eax, %eax
	callq	exit_picture
.LBB12_5:
	movq	$0, active_pps(%rip)
.LBB12_6:                               # %._crit_edge
	movslq	4(%r15), %rax
	imulq	$1160, %rax, %rax       # imm = 0x488
	leaq	PicParSet(%rax), %rbx
	cmpl	$1, PicParSet(%rax)
	jne	.LBB12_9
# BB#7:
	movq	1104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_9
# BB#8:
	callq	free
.LBB12_9:                               # %MakePPSavailable.exit
	movl	$1160, %edx             # imm = 0x488
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	memcpy
	movq	1104(%r15), %rax
	movq	%rax, 1104(%rbx)
	movq	$0, 1104(%r15)
	movl	$1, %esi
	movq	%r14, %rdi
	callq	FreePartition
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	FreePPS                 # TAILCALL
.Lfunc_end12:
	.size	ProcessPPS, .Lfunc_end12-ProcessPPS
	.cfi_endproc

	.globl	activate_sps
	.p2align	4, 0x90
	.type	activate_sps,@function
activate_sps:                           # @activate_sps
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 16
.Lcfi74:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpq	%rbx, active_sps(%rip)
	je	.LBB13_17
# BB#1:
	cmpq	$0, dec_picture(%rip)
	je	.LBB13_3
# BB#2:
	xorl	%eax, %eax
	callq	exit_picture
.LBB13_3:
	movq	%rbx, active_sps(%rip)
	movq	img(%rip), %rdi
	movl	$0, 5880(%rdi)
	movl	$0, 56(%rdi)
	movl	$0, 64(%rdi)
	movl	1000(%rbx), %eax
	addl	$8, %eax
	movl	%eax, 5876(%rdi)
	movl	32(%rbx), %edx
	testl	%edx, %edx
	je	.LBB13_5
# BB#4:
	movl	1004(%rbx), %eax
	addl	$8, %eax
	movl	%eax, 5880(%rdi)
.LBB13_5:
	movl	1008(%rbx), %ecx
	addl	$4, %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movl	%eax, 5816(%rdi)
	movl	2068(%rbx), %eax
	incl	%eax
	movl	%eax, 5820(%rdi)
	movl	2072(%rbx), %esi
	incl	%esi
	movl	%esi, 5824(%rdi)
	movl	$2, %ecx
	subl	2076(%rbx), %ecx
	imull	%esi, %ecx
	movl	%ecx, 5828(%rdi)
	movl	%ecx, %esi
	imull	%eax, %esi
	movl	%esi, 5840(%rdi)
	movl	%edx, 5916(%rdi)
	shll	$4, %eax
	movl	%eax, 48(%rdi)
	shll	$4, %ecx
	movl	%ecx, 52(%rdi)
	cmpl	$3, %edx
	je	.LBB13_10
# BB#6:
	cmpl	$2, %edx
	je	.LBB13_9
# BB#7:
	xorl	%esi, %esi
	cmpl	$1, %edx
	jne	.LBB13_12
# BB#8:
	sarl	%eax
	movl	%eax, 56(%rdi)
	sarl	%ecx
	jmp	.LBB13_11
.LBB13_17:
	popq	%rbx
	retq
.LBB13_9:
	sarl	%eax
.LBB13_10:
	movl	%eax, 56(%rdi)
.LBB13_11:                              # %.sink.split
	movl	%ecx, 64(%rdi)
	movl	%eax, %esi
.LBB13_12:
	decl	%esi
	movl	%esi, 60(%rdi)
	callq	init_frext
	xorl	%eax, %eax
	callq	init_global_buffers
	movq	img(%rip), %rax
	cmpl	$0, 5848(%rax)
	jne	.LBB13_14
# BB#13:
	callq	flush_dpb
.LBB13_14:
	callq	init_dpb
	movq	Co_located(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB13_16
# BB#15:
	callq	free_colocated
.LBB13_16:
	movq	img(%rip), %rax
	movl	48(%rax), %edi
	movl	52(%rax), %esi
	movl	2080(%rbx), %edx
	callq	alloc_colocated
	movq	%rax, Co_located(%rip)
	movq	img(%rip), %rax
	movl	48(%rax), %edi
	movl	52(%rax), %esi
	movl	$1, %edx
	popq	%rbx
	jmp	ercInit                 # TAILCALL
.Lfunc_end13:
	.size	activate_sps, .Lfunc_end13-activate_sps
	.cfi_endproc

	.globl	activate_pps
	.p2align	4, 0x90
	.type	activate_pps,@function
activate_pps:                           # @activate_pps
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 16
.Lcfi76:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpq	%rbx, active_pps(%rip)
	je	.LBB14_4
# BB#1:
	cmpq	$0, dec_picture(%rip)
	je	.LBB14_3
# BB#2:
	xorl	%eax, %eax
	callq	exit_picture
.LBB14_3:
	movq	%rbx, active_pps(%rip)
	movl	16(%rbx), %eax
	movq	img(%rip), %rcx
	movl	%eax, 5908(%rcx)
.LBB14_4:
	popq	%rbx
	retq
.Lfunc_end14:
	.size	activate_pps, .Lfunc_end14-activate_pps
	.cfi_endproc

	.globl	UseParameterSet
	.p2align	4, 0x90
	.type	UseParameterSet,@function
UseParameterSet:                        # @UseParameterSet
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi79:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi81:
	.cfi_def_cfa_offset 48
.Lcfi82:
	.cfi_offset %rbx, -40
.Lcfi83:
	.cfi_offset %r12, -32
.Lcfi84:
	.cfi_offset %r14, -24
.Lcfi85:
	.cfi_offset %r15, -16
	movl	%edi, %ebx
	movslq	%ebx, %rax
	imulq	$1160, %rax, %r14       # imm = 0x488
	leaq	PicParSet+8(%r14), %r15
	cmpl	$1, PicParSet(%r14)
	je	.LBB15_2
# BB#1:
	movl	$.L.str.109, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
.LBB15_2:
	movl	(%r15), %edx
	imulq	$3064, %rdx, %rax       # imm = 0xBF8
	cmpl	$1, SeqParSet(%rax)
	je	.LBB15_4
# BB#3:
	movl	$.L.str.110, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	printf
	movl	(%r15), %edx
.LBB15_4:
	movl	%edx, %eax
	imulq	$3064, %rax, %rbx       # imm = 0xBF8
	movl	SeqParSet+1012(%rbx), %esi
	testl	%esi, %esi
	js	.LBB15_6
# BB#5:
	cmpl	$3, %esi
	jb	.LBB15_7
.LBB15_6:
	leaq	SeqParSet+1012(%rbx), %r15
	movl	$.L.str.111, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.112, %edi
	movl	$-1000, %esi            # imm = 0xFC18
	callq	error
	movl	(%r15), %esi
.LBB15_7:
	leaq	PicParSet(%r14), %r12
	leaq	SeqParSet(%rbx), %r15
	cmpl	$1, %esi
	jne	.LBB15_10
# BB#8:
	cmpl	$256, SeqParSet+1032(%rbx) # imm = 0x100
	jb	.LBB15_10
# BB#9:
	movl	$.L.str.113, %edi
	movl	$-1011, %esi            # imm = 0xFC0D
	callq	error
.LBB15_10:
	movq	%r15, %rdi
	callq	activate_sps
	cmpq	%r12, active_pps(%rip)
	je	.LBB15_14
# BB#11:
	cmpq	$0, dec_picture(%rip)
	je	.LBB15_13
# BB#12:
	xorl	%eax, %eax
	callq	exit_picture
.LBB15_13:
	movq	%r12, active_pps(%rip)
	movl	PicParSet+16(%r14), %eax
	movq	img(%rip), %rcx
	movl	%eax, 5908(%rcx)
.LBB15_14:                              # %activate_pps.exit
	cmpl	$0, PicParSet+12(%r14)
	movl	$uvlc_startcode_follows, %eax
	movl	$cabac_startcode_follows, %ecx
	cmoveq	%rax, %rcx
	movl	$readSyntaxElement_UVLC, %eax
	movl	$readSyntaxElement_CABAC, %edx
	cmoveq	%rax, %rdx
	movq	%rcx, nal_startcode_follows(%rip)
	movq	img(%rip), %rax
	movq	5592(%rax), %rax
	movq	40(%rax), %rax
	movq	%rdx, 48(%rax)
	movq	%rdx, 104(%rax)
	movq	%rdx, 160(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end15:
	.size	UseParameterSet, .Lfunc_end15-UseParameterSet
	.cfi_endproc

	.type	ZZ_SCAN,@object         # @ZZ_SCAN
	.section	.rodata,"a",@progbits
	.globl	ZZ_SCAN
	.p2align	4
ZZ_SCAN:
	.ascii	"\000\001\004\b\005\002\003\006\t\f\r\n\007\013\016\017"
	.size	ZZ_SCAN, 16

	.type	ZZ_SCAN8,@object        # @ZZ_SCAN8
	.globl	ZZ_SCAN8
	.p2align	4
ZZ_SCAN8:
	.ascii	"\000\001\b\020\t\002\003\n\021\030 \031\022\013\004\005\f\023\032!(0)\"\033\024\r\006\007\016\025\034#*1892+$\035\026\017\027\036%,3:;4-&\037'.5<=6/7>?"
	.size	ZZ_SCAN8, 64

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"   : delta_sl   "
	.size	.L.str, 17

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"SPS: profile_idc"
	.size	.L.str.1, 17

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"SPS: constrained_set0_flag"
	.size	.L.str.2, 27

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"SPS: constrained_set1_flag"
	.size	.L.str.3, 27

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"SPS: constrained_set2_flag"
	.size	.L.str.4, 27

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"SPS: constrained_set3_flag"
	.size	.L.str.5, 27

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"SPS: reserved_zero_4bits"
	.size	.L.str.6, 25

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"SPS: level_idc"
	.size	.L.str.7, 15

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"SPS: seq_parameter_set_id"
	.size	.L.str.8, 26

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"SPS: chroma_format_idc"
	.size	.L.str.9, 23

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"SPS: residue_transform_flag"
	.size	.L.str.10, 28

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"[Deprecated High444 Profile] residue_transform_flag = 1 is no longer supported"
	.size	.L.str.11, 79

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"SPS: bit_depth_luma_minus8"
	.size	.L.str.12, 27

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"SPS: bit_depth_chroma_minus8"
	.size	.L.str.13, 29

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"SPS: lossless_qpprime_y_zero_flag"
	.size	.L.str.14, 34

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"SPS: seq_scaling_matrix_present_flag"
	.size	.L.str.15, 37

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"SPS: seq_scaling_list_present_flag"
	.size	.L.str.16, 35

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"SPS: log2_max_frame_num_minus4"
	.size	.L.str.17, 31

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"SPS: pic_order_cnt_type"
	.size	.L.str.18, 24

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"SPS: log2_max_pic_order_cnt_lsb_minus4"
	.size	.L.str.19, 39

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"SPS: delta_pic_order_always_zero_flag"
	.size	.L.str.20, 38

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"SPS: offset_for_non_ref_pic"
	.size	.L.str.21, 28

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"SPS: offset_for_top_to_bottom_field"
	.size	.L.str.22, 36

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"SPS: num_ref_frames_in_pic_order_cnt_cycle"
	.size	.L.str.23, 43

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"SPS: offset_for_ref_frame[i]"
	.size	.L.str.24, 29

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"SPS: num_ref_frames"
	.size	.L.str.25, 20

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"SPS: gaps_in_frame_num_value_allowed_flag"
	.size	.L.str.26, 42

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"SPS: pic_width_in_mbs_minus1"
	.size	.L.str.27, 29

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"SPS: pic_height_in_map_units_minus1"
	.size	.L.str.28, 36

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"SPS: frame_mbs_only_flag"
	.size	.L.str.29, 25

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"SPS: mb_adaptive_frame_field_flag"
	.size	.L.str.30, 34

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"SPS: direct_8x8_inference_flag"
	.size	.L.str.31, 31

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"SPS: frame_cropping_flag"
	.size	.L.str.32, 25

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"SPS: frame_cropping_rect_left_offset"
	.size	.L.str.33, 37

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"SPS: frame_cropping_rect_right_offset"
	.size	.L.str.34, 38

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"SPS: frame_cropping_rect_top_offset"
	.size	.L.str.35, 36

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"SPS: frame_cropping_rect_bottom_offset"
	.size	.L.str.36, 39

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"SPS: vui_parameters_present_flag"
	.size	.L.str.37, 33

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"VUI: aspect_ratio_info_present_flag"
	.size	.L.str.38, 36

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"VUI: aspect_ratio_idc"
	.size	.L.str.39, 22

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"VUI: sar_width"
	.size	.L.str.40, 15

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"VUI: sar_height"
	.size	.L.str.41, 16

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"VUI: overscan_info_present_flag"
	.size	.L.str.42, 32

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"VUI: overscan_appropriate_flag"
	.size	.L.str.43, 31

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"VUI: video_signal_type_present_flag"
	.size	.L.str.44, 36

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"VUI: video_format"
	.size	.L.str.45, 18

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"VUI: video_full_range_flag"
	.size	.L.str.46, 27

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"VUI: color_description_present_flag"
	.size	.L.str.47, 36

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"VUI: colour_primaries"
	.size	.L.str.48, 22

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"VUI: transfer_characteristics"
	.size	.L.str.49, 30

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"VUI: matrix_coefficients"
	.size	.L.str.50, 25

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"VUI: chroma_loc_info_present_flag"
	.size	.L.str.51, 34

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"VUI: chroma_sample_loc_type_top_field"
	.size	.L.str.52, 38

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"VUI: chroma_sample_loc_type_bottom_field"
	.size	.L.str.53, 41

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"VUI: timing_info_present_flag"
	.size	.L.str.54, 30

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"VUI: num_units_in_tick"
	.size	.L.str.55, 23

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"VUI: time_scale"
	.size	.L.str.56, 16

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"VUI: fixed_frame_rate_flag"
	.size	.L.str.57, 27

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"VUI: nal_hrd_parameters_present_flag"
	.size	.L.str.58, 37

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"VUI: vcl_hrd_parameters_present_flag"
	.size	.L.str.59, 37

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"VUI: low_delay_hrd_flag"
	.size	.L.str.60, 24

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"VUI: pic_struct_present_flag   "
	.size	.L.str.61, 32

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"VUI: bitstream_restriction_flag"
	.size	.L.str.62, 32

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"VUI: motion_vectors_over_pic_boundaries_flag"
	.size	.L.str.63, 45

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"VUI: max_bytes_per_pic_denom"
	.size	.L.str.64, 29

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"VUI: max_bits_per_mb_denom"
	.size	.L.str.65, 27

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"VUI: log2_max_mv_length_horizontal"
	.size	.L.str.66, 35

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"VUI: log2_max_mv_length_vertical"
	.size	.L.str.67, 33

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"VUI: num_reorder_frames"
	.size	.L.str.68, 24

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"VUI: max_dec_frame_buffering"
	.size	.L.str.69, 29

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"VUI: cpb_cnt_minus1"
	.size	.L.str.70, 20

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"VUI: bit_rate_scale"
	.size	.L.str.71, 20

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"VUI: cpb_size_scale"
	.size	.L.str.72, 20

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"VUI: bit_rate_value_minus1"
	.size	.L.str.73, 27

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"VUI: cpb_size_value_minus1"
	.size	.L.str.74, 27

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"VUI: cbr_flag"
	.size	.L.str.75, 14

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"VUI: initial_cpb_removal_delay_length_minus1"
	.size	.L.str.76, 45

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"VUI: cpb_removal_delay_length_minus1"
	.size	.L.str.77, 37

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"VUI: dpb_output_delay_length_minus1"
	.size	.L.str.78, 36

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"VUI: time_offset_length"
	.size	.L.str.79, 24

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"PPS: pic_parameter_set_id"
	.size	.L.str.80, 26

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"PPS: seq_parameter_set_id"
	.size	.L.str.81, 26

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"PPS: entropy_coding_mode_flag"
	.size	.L.str.82, 30

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"PPS: pic_order_present_flag"
	.size	.L.str.83, 28

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"PPS: num_slice_groups_minus1"
	.size	.L.str.84, 29

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"PPS: slice_group_map_type"
	.size	.L.str.85, 26

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"PPS: run_length_minus1 [i]"
	.size	.L.str.86, 27

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"PPS: top_left [i]"
	.size	.L.str.87, 18

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"PPS: bottom_right [i]"
	.size	.L.str.88, 22

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"PPS: slice_group_change_direction_flag"
	.size	.L.str.89, 39

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"PPS: slice_group_change_rate_minus1"
	.size	.L.str.90, 36

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"PPS: num_slice_group_map_units_minus1"
	.size	.L.str.91, 38

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"slice_group_id[i]"
	.size	.L.str.92, 18

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"PPS: num_ref_idx_l0_active_minus1"
	.size	.L.str.93, 34

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"PPS: num_ref_idx_l1_active_minus1"
	.size	.L.str.94, 34

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"PPS: weighted_pred_flag"
	.size	.L.str.95, 24

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"PPS: weighted_bipred_idc"
	.size	.L.str.96, 25

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"PPS: pic_init_qp_minus26"
	.size	.L.str.97, 25

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"PPS: pic_init_qs_minus26"
	.size	.L.str.98, 25

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"PPS: chroma_qp_index_offset"
	.size	.L.str.99, 28

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"PPS: deblocking_filter_control_present_flag"
	.size	.L.str.100, 44

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"PPS: constrained_intra_pred_flag"
	.size	.L.str.101, 33

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"PPS: redundant_pic_cnt_present_flag"
	.size	.L.str.102, 36

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"PPS: transform_8x8_mode_flag"
	.size	.L.str.103, 29

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"PPS: pic_scaling_matrix_present_flag"
	.size	.L.str.104, 37

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"PPS: pic_scaling_list_present_flag"
	.size	.L.str.105, 35

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"PPS: second_chroma_qp_index_offset"
	.size	.L.str.106, 35

	.type	PicParSet,@object       # @PicParSet
	.comm	PicParSet,296960,16
	.type	SeqParSet,@object       # @SeqParSet
	.comm	SeqParSet,98048,16
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"Trying to use an invalid (uninitialized) Picture Parameter Set with ID %d, expect the unexpected...\n"
	.size	.L.str.109, 101

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"PicParset %d references an invalid (uninitialized) Sequence Parameter Set with ID %d, expect the unexpected...\n"
	.size	.L.str.110, 112

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"invalid sps->pic_order_cnt_type = %d\n"
	.size	.L.str.111, 38

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"pic_order_cnt_type != 1"
	.size	.L.str.112, 24

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"num_ref_frames_in_pic_order_cnt_cycle too large"
	.size	.L.str.113, 48

	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	last_out_fs,@object     # @last_out_fs
	.comm	last_out_fs,8,8
	.type	pocs_in_dpb,@object     # @pocs_in_dpb
	.comm	pocs_in_dpb,400,16
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Consistency checking a picture parset, to be implemented"
	.size	.Lstr, 57

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"Consistency checking a sequence parset, to be implemented"
	.size	.Lstr.1, 58


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
