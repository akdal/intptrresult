	.text
	.file	"main.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	subq	$40, %rsp
.Lcfi0:
	.cfi_def_cfa_offset 48
	movl	$10, %edi
	callq	putchar
	movl	$.Lstr.19, %edi
	callq	puts
	movl	$.Lstr.18, %edi
	callq	puts
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$.Lstr.18, %edi
	callq	puts
	movl	$.Lstr.19, %edi
	callq	puts
	leaq	24(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	callq	clock
	movq	$0, totalWallTime(%rip)
	movq	$0, totalCPUTime(%rip)
	callq	test_Matvec
	movl	$10, %edi
	callq	putchar
	movl	$.Lstr.19, %edi
	callq	puts
	movl	$.Lstr.18, %edi
	callq	puts
	movl	$.Lstr.7, %edi
	callq	puts
	movl	$.Lstr.18, %edi
	callq	puts
	movl	$.Lstr.19, %edi
	callq	puts
	movq	$0, totalWallTime(%rip)
	movq	$0, totalCPUTime(%rip)
	callq	test_Relax
	movl	$10, %edi
	callq	putchar
	movl	$.Lstr.19, %edi
	callq	puts
	movl	$.Lstr.18, %edi
	callq	puts
	movl	$.Lstr.12, %edi
	callq	puts
	movl	$.Lstr.18, %edi
	callq	puts
	movl	$.Lstr.19, %edi
	callq	puts
	movq	$0, totalWallTime(%rip)
	movq	$0, totalCPUTime(%rip)
	callq	test_Axpy
	movl	$10, %edi
	callq	putchar
	movl	$.Lstr.19, %edi
	callq	puts
	movl	$.Lstr.18, %edi
	callq	puts
	movl	$.Lstr.17, %edi
	callq	puts
	movl	$.Lstr.18, %edi
	callq	puts
	movl	$.Lstr.19, %edi
	callq	puts
	leaq	8(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	callq	clock
	xorl	%eax, %eax
	addq	$40, %rsp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.quad	4618441417868443648     # double 6
	.quad	-4616189618054758400    # double -1
.LCPI1_1:
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
.LCPI1_4:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_2:
	.quad	4607182418800017408     # double 1
.LCPI1_3:
	.quad	4696837146684686336     # double 1.0E+6
	.text
	.globl	test_Matvec
	.p2align	4, 0x90
	.type	test_Matvec,@function
test_Matvec:                            # @test_Matvec
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 40
	subq	$72, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 112
.Lcfi6:
	.cfi_offset %rbx, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	$4, %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rax, %r14
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [6.000000e+00,-1.000000e+00]
	movups	%xmm0, (%r14)
	movaps	.LCPI1_1(%rip), %xmm0   # xmm0 = [-1.000000e+00,-1.000000e+00]
	movups	%xmm0, 16(%r14)
	leaq	32(%rsp), %rax
	movq	%rax, (%rsp)
	leaq	16(%rsp), %r8
	leaq	24(%rsp), %r9
	movl	$50, %edi
	movl	$50, %esi
	movl	$50, %edx
	movq	%r14, %rcx
	callq	GenerateSeqLaplacian
	movq	%rax, %rbx
	movq	24(%rsp), %rdi
	movsd	.LCPI1_2(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	hypre_SeqVectorSetConstantValues
	movq	16(%rsp), %rdi
	xorps	%xmm0, %xmm0
	callq	hypre_SeqVectorSetConstantValues
	leaq	56(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	movl	$2000, %ebp             # imm = 0x7D0
	callq	clock
	movq	%rax, %r15
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rsi
	movq	16(%rsp), %rdx
	xorps	%xmm1, %xmm1
	movsd	.LCPI1_2(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	%rbx, %rdi
	callq	hypre_CSRMatrixMatvec
	decl	%ebp
	jne	.LBB1_1
# BB#2:
	leaq	40(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	callq	clock
	movq	40(%rsp), %rcx
	movq	48(%rsp), %rdx
	subq	56(%rsp), %rcx
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rcx, %xmm0
	subq	64(%rsp), %rdx
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rdx, %xmm1
	movsd	.LCPI1_3(%rip), %xmm2   # xmm2 = mem[0],zero
	divsd	%xmm2, %xmm1
	addsd	%xmm0, %xmm1
	addsd	totalWallTime(%rip), %xmm1
	movsd	%xmm1, totalWallTime(%rip)
	subq	%r15, %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	%xmm2, %xmm0
	addsd	totalCPUTime(%rip), %xmm0
	movsd	%xmm0, totalCPUTime(%rip)
	movq	16(%rsp), %rax
	movq	(%rax), %rax
	movq	32(%rsp), %rcx
	movq	(%rcx), %rcx
	xorpd	%xmm0, %xmm0
	movl	$1, %edx
	movapd	.LCPI1_4(%rip), %xmm1   # xmm1 = [nan,nan]
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movsd	-8(%rax,%rdx,8), %xmm2  # xmm2 = mem[0],zero
	subsd	-8(%rcx,%rdx,8), %xmm2
	andpd	%xmm1, %xmm2
	maxsd	%xmm0, %xmm2
	movsd	(%rax,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	subsd	(%rcx,%rdx,8), %xmm0
	andpd	%xmm1, %xmm0
	maxsd	%xmm2, %xmm0
	addq	$2, %rdx
	cmpq	$125001, %rdx           # imm = 0x1E849
	jne	.LBB1_3
# BB#4:
	xorpd	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jbe	.LBB1_6
# BB#5:
	movl	$.L.str.7, %edi
	movb	$1, %al
	callq	printf
.LBB1_6:
	movq	%r14, %rdi
	callq	hypre_Free
	movq	%rbx, %rdi
	callq	hypre_CSRMatrixDestroy
	movq	24(%rsp), %rdi
	callq	hypre_SeqVectorDestroy
	movq	16(%rsp), %rdi
	callq	hypre_SeqVectorDestroy
	movq	32(%rsp), %rdi
	callq	hypre_SeqVectorDestroy
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	test_Matvec, .Lfunc_end1-test_Matvec
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.quad	4618441417868443648     # double 6
	.quad	-4616189618054758400    # double -1
.LCPI2_1:
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
.LCPI2_5:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_2:
	.quad	4607182418800017408     # double 1
.LCPI2_3:
	.quad	4696837146684686336     # double 1.0E+6
.LCPI2_4:
	.quad	-4616189618054758400    # double -1
	.text
	.globl	test_Relax
	.p2align	4, 0x90
	.type	test_Relax,@function
test_Relax:                             # @test_Relax
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 40
	subq	$72, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 112
.Lcfi15:
	.cfi_offset %rbx, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movl	$4, %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rax, %r14
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [6.000000e+00,-1.000000e+00]
	movups	%xmm0, (%r14)
	movaps	.LCPI2_1(%rip), %xmm0   # xmm0 = [-1.000000e+00,-1.000000e+00]
	movups	%xmm0, 16(%r14)
	leaq	24(%rsp), %rax
	movq	%rax, (%rsp)
	leaq	32(%rsp), %r8
	leaq	16(%rsp), %r9
	movl	$50, %edi
	movl	$50, %esi
	movl	$50, %edx
	movq	%r14, %rcx
	callq	GenerateSeqLaplacian
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	movsd	.LCPI2_2(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	hypre_SeqVectorSetConstantValues
	leaq	56(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	movl	$2000, %ebp             # imm = 0x7D0
	callq	clock
	movq	%rax, %r15
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rsi
	movq	16(%rsp), %rdx
	movq	%rbx, %rdi
	callq	hypre_BoomerAMGSeqRelax
	decl	%ebp
	jne	.LBB2_1
# BB#2:
	leaq	40(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	callq	clock
	movq	40(%rsp), %rcx
	movq	48(%rsp), %rdx
	subq	56(%rsp), %rcx
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rcx, %xmm0
	subq	64(%rsp), %rdx
	cvtsi2sdq	%rdx, %xmm1
	movsd	.LCPI2_3(%rip), %xmm2   # xmm2 = mem[0],zero
	divsd	%xmm2, %xmm1
	addsd	%xmm0, %xmm1
	addsd	totalWallTime(%rip), %xmm1
	movsd	%xmm1, totalWallTime(%rip)
	subq	%r15, %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	%xmm2, %xmm0
	addsd	totalCPUTime(%rip), %xmm0
	movsd	%xmm0, totalCPUTime(%rip)
	movq	16(%rsp), %rax
	movq	(%rax), %rax
	xorpd	%xmm0, %xmm0
	movl	$3, %ecx
	movsd	.LCPI2_4(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	.LCPI2_5(%rip), %xmm2   # xmm2 = [nan,nan]
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	movsd	-24(%rax,%rcx,8), %xmm3 # xmm3 = mem[0],zero
	addsd	%xmm1, %xmm3
	andpd	%xmm2, %xmm3
	maxsd	%xmm0, %xmm3
	movsd	-16(%rax,%rcx,8), %xmm0 # xmm0 = mem[0],zero
	addsd	%xmm1, %xmm0
	andpd	%xmm2, %xmm0
	maxsd	%xmm3, %xmm0
	movsd	-8(%rax,%rcx,8), %xmm3  # xmm3 = mem[0],zero
	addsd	%xmm1, %xmm3
	andpd	%xmm2, %xmm3
	maxsd	%xmm0, %xmm3
	movsd	(%rax,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	addsd	%xmm1, %xmm0
	andpd	%xmm2, %xmm0
	maxsd	%xmm3, %xmm0
	addq	$4, %rcx
	cmpq	$125003, %rcx           # imm = 0x1E84B
	jne	.LBB2_3
# BB#4:
	xorpd	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jbe	.LBB2_6
# BB#5:
	movl	$.L.str.8, %edi
	movb	$1, %al
	callq	printf
.LBB2_6:
	movq	%r14, %rdi
	callq	hypre_Free
	movq	%rbx, %rdi
	callq	hypre_CSRMatrixDestroy
	movq	16(%rsp), %rdi
	callq	hypre_SeqVectorDestroy
	movq	32(%rsp), %rdi
	callq	hypre_SeqVectorDestroy
	movq	24(%rsp), %rdi
	callq	hypre_SeqVectorDestroy
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	test_Relax, .Lfunc_end2-test_Relax
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4607182418800017408     # double 1
.LCPI3_1:
	.quad	4602678819172646912     # double 0.5
.LCPI3_2:
	.quad	-4616189618054758400    # double -1
.LCPI3_3:
	.quad	-4571364728013586432    # double -1000
.LCPI3_5:
	.quad	4696837146684686336     # double 1.0E+6
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_4:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	test_Axpy
	.p2align	4, 0x90
	.type	test_Axpy,@function
test_Axpy:                              # @test_Axpy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 80
.Lcfi25:
	.cfi_offset %rbx, -48
.Lcfi26:
	.cfi_offset %r12, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movl	$125000, %edi           # imm = 0x1E848
	callq	hypre_SeqVectorCreate
	movq	%rax, %r14
	movl	$125000, %edi           # imm = 0x1E848
	callq	hypre_SeqVectorCreate
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	hypre_SeqVectorInitialize
	movq	%rbx, %rdi
	callq	hypre_SeqVectorInitialize
	movsd	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	%r14, %rdi
	callq	hypre_SeqVectorSetConstantValues
	movq	%rbx, %rdi
	movsd	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	hypre_SeqVectorSetConstantValues
	leaq	16(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	movl	$2000, %ebp             # imm = 0x7D0
	callq	clock
	movq	%rax, %r15
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	movsd	.LCPI3_1(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	hypre_SeqVectorAxpy
	decl	%ebp
	jne	.LBB3_1
# BB#2:
	movq	%rsp, %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	callq	clock
	movq	%rax, %r12
	movq	(%rbx), %rax
	xorpd	%xmm0, %xmm0
	movl	$1, %ecx
	movsd	.LCPI3_2(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI3_3(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	.LCPI3_4(%rip), %xmm3   # xmm3 = [nan,nan]
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movsd	-8(%rax,%rcx,8), %xmm4  # xmm4 = mem[0],zero
	addsd	%xmm1, %xmm4
	addsd	%xmm2, %xmm4
	andpd	%xmm3, %xmm4
	maxsd	%xmm0, %xmm4
	movsd	(%rax,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	addsd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	andpd	%xmm3, %xmm0
	maxsd	%xmm4, %xmm0
	addq	$2, %rcx
	cmpq	$125001, %rcx           # imm = 0x1E849
	jne	.LBB3_3
# BB#4:
	xorpd	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jbe	.LBB3_6
# BB#5:
	movl	$.L.str.9, %edi
	movb	$1, %al
	callq	printf
.LBB3_6:
	movq	(%rsp), %rax
	movq	8(%rsp), %rcx
	subq	16(%rsp), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	subq	24(%rsp), %rcx
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rcx, %xmm1
	movsd	.LCPI3_5(%rip), %xmm2   # xmm2 = mem[0],zero
	divsd	%xmm2, %xmm1
	addsd	%xmm0, %xmm1
	addsd	totalWallTime(%rip), %xmm1
	movsd	%xmm1, totalWallTime(%rip)
	subq	%r15, %r12
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
	divsd	%xmm2, %xmm0
	addsd	totalCPUTime(%rip), %xmm0
	movsd	%xmm0, totalCPUTime(%rip)
	movq	%r14, %rdi
	callq	hypre_SeqVectorDestroy
	movq	%rbx, %rdi
	callq	hypre_SeqVectorDestroy
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	test_Axpy, .Lfunc_end3-test_Axpy
	.cfi_endproc

	.type	testIter,@object        # @testIter
	.section	.rodata,"a",@progbits
	.globl	testIter
	.p2align	2
testIter:
	.long	2000                    # 0x7d0
	.size	testIter, 4

	.type	totalWallTime,@object   # @totalWallTime
	.bss
	.globl	totalWallTime
	.p2align	3
totalWallTime:
	.quad	0                       # double 0
	.size	totalWallTime, 8

	.type	totalCPUTime,@object    # @totalCPUTime
	.globl	totalCPUTime
	.p2align	3
totalCPUTime:
	.quad	0                       # double 0
	.size	totalCPUTime, 8

	.type	.L.str.7,@object        # @.str.7
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.7:
	.asciz	" \n Matvec: error: %e\n"
	.size	.L.str.7, 22

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	" \n Relax: error: %e\n"
	.size	.L.str.8, 21

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	" \n Axpy: error: %e\n"
	.size	.L.str.9, 20

	.type	.Lstr.2,@object         # @str.2
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.2:
	.asciz	"//  Sequoia Benchmark Version 1.0 "
	.size	.Lstr.2, 35

	.type	.Lstr.7,@object         # @str.7
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.7:
	.asciz	"//   MATVEC"
	.size	.Lstr.7, 12

	.type	.Lstr.12,@object        # @str.12
.Lstr.12:
	.asciz	"//   Relax"
	.size	.Lstr.12, 11

	.type	.Lstr.17,@object        # @str.17
.Lstr.17:
	.asciz	"//   Axpy"
	.size	.Lstr.17, 10

	.type	.Lstr.18,@object        # @str.18
.Lstr.18:
	.asciz	"// "
	.size	.Lstr.18, 4

	.type	.Lstr.19,@object        # @str.19
.Lstr.19:
	.asciz	"//------------ "
	.size	.Lstr.19, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
