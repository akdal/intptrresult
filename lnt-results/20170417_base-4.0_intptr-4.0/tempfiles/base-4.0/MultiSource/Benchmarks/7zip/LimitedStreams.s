	.text
	.file	"LimitedStreams.bc"
	.globl	_ZN26CLimitedSequentialInStream4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN26CLimitedSequentialInStream4ReadEPvjPj,@function
_ZN26CLimitedSequentialInStream4ReadEPvjPj: # @_ZN26CLimitedSequentialInStream4ReadEPvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 48
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %eax
	movq	%rdi, %rbx
	movl	$0, 12(%rsp)
	movq	24(%rbx), %rdx
	subq	32(%rbx), %rdx
	movl	%eax, %ecx
	cmpq	%rcx, %rdx
	cmovael	%eax, %edx
	xorl	%ebp, %ebp
	testl	%edx, %edx
	je	.LBB0_1
# BB#2:
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	leaq	12(%rsp), %rcx
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	*40(%rax)
	movl	12(%rsp), %ecx
	addq	%rcx, 32(%rbx)
	testq	%rcx, %rcx
	je	.LBB0_4
# BB#3:
	movl	%ecx, %ebp
	testq	%r14, %r14
	jne	.LBB0_6
	jmp	.LBB0_7
.LBB0_1:
	xorl	%eax, %eax
	testq	%r14, %r14
	jne	.LBB0_6
	jmp	.LBB0_7
.LBB0_4:
	movb	$1, 40(%rbx)
	testq	%r14, %r14
	je	.LBB0_7
.LBB0_6:
	movl	%ebp, (%r14)
.LBB0_7:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN26CLimitedSequentialInStream4ReadEPvjPj, .Lfunc_end0-_ZN26CLimitedSequentialInStream4ReadEPvjPj
	.cfi_endproc

	.globl	_ZN16CLimitedInStream4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN16CLimitedInStream4ReadEPvjPj,@function
_ZN16CLimitedInStream4ReadEPvjPj:       # @_ZN16CLimitedInStream4ReadEPvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 48
.Lcfi12:
	.cfi_offset %rbx, -40
.Lcfi13:
	.cfi_offset %r14, -32
.Lcfi14:
	.cfi_offset %r15, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %ebp
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	%ebp, 4(%rsp)
	testq	%r14, %r14
	je	.LBB1_2
# BB#1:
	movl	$0, (%r14)
.LBB1_2:
	movq	24(%rbx), %rsi
	movq	40(%rbx), %rcx
	movq	%rcx, %rax
	subq	%rsi, %rax
	jbe	.LBB1_3
# BB#4:
	movl	%ebp, %ecx
	cmpq	%rcx, %rax
	jae	.LBB1_6
# BB#5:
	movl	%eax, 4(%rsp)
	movl	%eax, %ebp
.LBB1_6:
	addq	48(%rbx), %rsi
	cmpq	32(%rbx), %rsi
	je	.LBB1_8
# BB#7:
	movq	%rsi, 32(%rbx)
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	testl	%eax, %eax
	jne	.LBB1_11
.LBB1_8:
	leaq	16(%rbx), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	leaq	4(%rsp), %rcx
	movq	%r15, %rsi
	movl	%ebp, %edx
	callq	*40(%rax)
	movl	4(%rsp), %ecx
	testq	%r14, %r14
	je	.LBB1_10
# BB#9:
	movl	%ecx, (%r14)
.LBB1_10:                               # %._crit_edge
	movdqu	24(%rbx), %xmm0
	movd	%rcx, %xmm1
	pshufd	$68, %xmm1, %xmm1       # xmm1 = xmm1[0,1,0,1]
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 24(%rbx)
	jmp	.LBB1_11
.LBB1_3:
	xorl	%edx, %edx
	cmpq	%rsi, %rcx
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovel	%edx, %eax
.LBB1_11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN16CLimitedInStream4ReadEPvjPj, .Lfunc_end1-_ZN16CLimitedInStream4ReadEPvjPj
	.cfi_endproc

	.globl	_ZN16CLimitedInStream4SeekExjPy
	.p2align	4, 0x90
	.type	_ZN16CLimitedInStream4SeekExjPy,@function
_ZN16CLimitedInStream4SeekExjPy:        # @_ZN16CLimitedInStream4SeekExjPy
	.cfi_startproc
# BB#0:
	cmpl	$2, %edx
	je	.LBB2_4
# BB#1:
	cmpl	$1, %edx
	je	.LBB2_3
# BB#2:
	movl	$-2147287039, %eax      # imm = 0x80030001
	testl	%edx, %edx
	je	.LBB2_5
	jmp	.LBB2_7
.LBB2_4:
	addq	40(%rdi), %rsi
	jmp	.LBB2_5
.LBB2_3:
	addq	24(%rdi), %rsi
.LBB2_5:
	movq	%rsi, 24(%rdi)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.LBB2_7
# BB#6:
	movq	%rsi, (%rcx)
.LBB2_7:
	retq
.Lfunc_end2:
	.size	_ZN16CLimitedInStream4SeekExjPy, .Lfunc_end2-_ZN16CLimitedInStream4SeekExjPy
	.cfi_endproc

	.globl	_ZN16CClusterInStream4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN16CClusterInStream4ReadEPvjPj,@function
_ZN16CClusterInStream4ReadEPvjPj:       # @_ZN16CClusterInStream4ReadEPvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 80
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %r12d
	movq	%rsi, %r8
	movq	%rdi, %rbx
	movl	%r12d, 4(%rsp)
	testq	%r14, %r14
	je	.LBB3_2
# BB#1:
	movl	$0, (%r14)
.LBB3_2:
	movq	16(%rbx), %rax
	movq	56(%rbx), %rcx
	cmpq	%rcx, %rax
	jae	.LBB3_3
# BB#4:
	movl	32(%rbx), %ebp
	testl	%ebp, %ebp
	je	.LBB3_5
.LBB3_12:                               # %.thread
	cmpl	%r12d, %ebp
	jae	.LBB3_14
.LBB3_13:
	movl	%ebp, 4(%rsp)
	movl	%ebp, %r12d
.LBB3_14:
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	leaq	4(%rsp), %rcx
	movq	%r8, %rsi
	movl	%r12d, %edx
	callq	*40(%rax)
	movl	4(%rsp), %ecx
	testq	%r14, %r14
	je	.LBB3_16
# BB#15:
	movl	%ecx, (%r14)
.LBB3_16:                               # %._crit_edge
	movdqu	16(%rbx), %xmm0
	movd	%rcx, %xmm1
	pshufd	$68, %xmm1, %xmm1       # xmm1 = xmm1[0,1,0,1]
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 16(%rbx)
	subl	%ecx, 32(%rbx)
	jmp	.LBB3_17
.LBB3_3:
	xorl	%edx, %edx
	cmpq	%rcx, %rax
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovel	%edx, %eax
.LBB3_17:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_5:
	movb	64(%rbx), %cl
	movl	$1, %ebp
	shll	%cl, %ebp
	movq	%rax, %r13
	shrq	%cl, %r13
	leal	-1(%rbp), %edi
	andl	%eax, %edi
	movq	88(%rbx), %rax
	movslq	%r13d, %rdx
	movl	(%rax,%rdx,4), %r15d
	movq	%r15, %rax
	shlq	%cl, %rax
	addq	48(%rbx), %rax
	leaq	(%rdi,%rax), %rsi
	cmpq	24(%rbx), %rsi
	je	.LBB3_7
# BB#6:
	movq	%rsi, 24(%rbx)
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r8, 16(%rsp)           # 8-byte Spill
	callq	*48(%rax)
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	testl	%eax, %eax
	jne	.LBB3_17
.LBB3_7:
	subl	%edi, %ebp
	movl	%ebp, 32(%rbx)
	movl	84(%rbx), %eax
	movl	$1, %edx
	.p2align	4, 0x90
.LBB3_8:                                # =>This Inner Loop Header: Depth=1
	leaq	(%r13,%rdx), %rcx
	cmpl	%eax, %ecx
	jae	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_8 Depth=1
	leal	(%r15,%rdx), %esi
	movq	88(%rbx), %rdi
	movslq	%ecx, %rcx
	cmpl	(%rdi,%rcx,4), %esi
	jne	.LBB3_10
# BB#18:                                #   in Loop: Header=BB3_8 Depth=1
	movzbl	64(%rbx), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	addl	%esi, %ebp
	movl	%ebp, 32(%rbx)
	incq	%rdx
	cmpq	$64, %rdx
	jl	.LBB3_8
.LBB3_10:                               # %.critedge
	movq	56(%rbx), %rax
	subq	16(%rbx), %rax
	movl	%ebp, %ecx
	cmpq	%rax, %rcx
	jbe	.LBB3_12
# BB#11:
	movl	%eax, 32(%rbx)
	movl	%eax, %ebp
	cmpl	%r12d, %ebp
	jb	.LBB3_13
	jmp	.LBB3_14
.Lfunc_end3:
	.size	_ZN16CClusterInStream4ReadEPvjPj, .Lfunc_end3-_ZN16CClusterInStream4ReadEPvjPj
	.cfi_endproc

	.globl	_ZN16CClusterInStream4SeekExjPy
	.p2align	4, 0x90
	.type	_ZN16CClusterInStream4SeekExjPy,@function
_ZN16CClusterInStream4SeekExjPy:        # @_ZN16CClusterInStream4SeekExjPy
	.cfi_startproc
# BB#0:
	testl	%edx, %edx
	je	.LBB4_6
# BB#1:
	cmpl	$2, %edx
	je	.LBB4_4
# BB#2:
	movl	$-2147287039, %eax      # imm = 0x80030001
	cmpl	$1, %edx
	jne	.LBB4_10
# BB#3:
	leaq	16(%rdi), %rax
	jmp	.LBB4_5
.LBB4_4:
	leaq	56(%rdi), %rax
.LBB4_5:                                # %.sink.split
	addq	(%rax), %rsi
.LBB4_6:
	cmpq	%rsi, 16(%rdi)
	je	.LBB4_8
# BB#7:
	movl	$0, 32(%rdi)
.LBB4_8:
	movq	%rsi, 16(%rdi)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.LBB4_10
# BB#9:
	movq	%rsi, (%rcx)
.LBB4_10:
	retq
.Lfunc_end4:
	.size	_ZN16CClusterInStream4SeekExjPy, .Lfunc_end4-_ZN16CClusterInStream4SeekExjPy
	.cfi_endproc

	.globl	_Z21CreateLimitedInStreamP9IInStreamyyPP19ISequentialInStream
	.p2align	4, 0x90
	.type	_Z21CreateLimitedInStreamP9IInStreamyyPP19ISequentialInStream,@function
_Z21CreateLimitedInStreamP9IInStreamyyPP19ISequentialInStream: # @_Z21CreateLimitedInStreamP9IInStreamyyPP19ISequentialInStream
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 48
.Lcfi34:
	.cfi_offset %rbx, -48
.Lcfi35:
	.cfi_offset %r12, -40
.Lcfi36:
	.cfi_offset %r13, -32
.Lcfi37:
	.cfi_offset %r14, -24
.Lcfi38:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r13
	movq	%rsi, %r15
	movq	%rdi, %r12
	movq	$0, (%r14)
	movl	$56, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	$_ZTV16CLimitedInStream+16, (%rbx)
	movq	$0, 16(%rbx)
	movl	$1, 8(%rbx)
	testq	%r12, %r12
	je	.LBB5_4
# BB#1:
	movq	(%r12), %rax
.Ltmp0:
	movq	%r12, %rdi
	callq	*8(%rax)
.Ltmp1:
# BB#2:                                 # %.noexc
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp2:
	callq	*16(%rax)
.Ltmp3:
.LBB5_4:                                # %.noexc.thread
	movq	%r12, 16(%rbx)
	movq	%r15, 48(%rbx)
	movq	%r15, 32(%rbx)
	movq	$0, 24(%rbx)
	movq	%r13, 40(%rbx)
	movq	(%r12), %rax
.Ltmp4:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	*48(%rax)
	movl	%eax, %r15d
.Ltmp5:
# BB#5:                                 # %_ZN16CLimitedInStream11InitAndSeekEyy.exit
	testl	%r15d, %r15d
	je	.LBB5_6
# BB#8:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*16(%rax)
	jmp	.LBB5_9
.LBB5_6:
	movq	(%rbx), %rax
.Ltmp6:
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	*48(%rax)
.Ltmp7:
# BB#7:                                 # %.thread
	movq	%rbx, (%r14)
.LBB5_9:                                # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit25
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB5_10:
.Ltmp8:
	movq	%rax, %r14
	movq	(%rbx), %rax
.Ltmp9:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp10:
# BB#11:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_12:
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_Z21CreateLimitedInStreamP9IInStreamyyPP19ISequentialInStream, .Lfunc_end5-_Z21CreateLimitedInStreamP9IInStreamyyPP19ISequentialInStream
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp5-.Ltmp0           #   Call between .Ltmp0 and .Ltmp5
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Lfunc_end5-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end6:
	.size	__clang_call_terminate, .Lfunc_end6-__clang_call_terminate

	.text
	.globl	_ZN27CLimitedSequentialOutStream5WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZN27CLimitedSequentialOutStream5WriteEPKvjPj,@function
_ZN27CLimitedSequentialOutStream5WriteEPKvjPj: # @_ZN27CLimitedSequentialOutStream5WriteEPKvjPj
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 32
.Lcfi42:
	.cfi_offset %rbx, -24
.Lcfi43:
	.cfi_offset %r14, -16
	movq	%rcx, %r14
	movq	%rdi, %rbx
	movl	%edx, 4(%rsp)
	testq	%r14, %r14
	je	.LBB7_2
# BB#1:
	movl	$0, (%r14)
.LBB7_2:
	movl	%edx, %eax
	movq	24(%rbx), %rcx
	cmpq	%rcx, %rax
	jbe	.LBB7_8
# BB#3:
	testq	%rcx, %rcx
	je	.LBB7_4
# BB#7:
	movl	%ecx, 4(%rsp)
	movl	%ecx, %edx
.LBB7_8:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_9
# BB#10:
	movq	(%rdi), %rax
	leaq	4(%rsp), %rcx
	callq	*40(%rax)
	movl	4(%rsp), %edx
	movq	24(%rbx), %rcx
	jmp	.LBB7_11
.LBB7_9:
	xorl	%eax, %eax
.LBB7_11:
	movl	%edx, %esi
	subq	%rsi, %rcx
	movq	%rcx, 24(%rbx)
	testq	%r14, %r14
	je	.LBB7_13
# BB#12:
	movl	%edx, (%r14)
.LBB7_13:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB7_4:
	movb	$1, 32(%rbx)
	testq	%r14, %r14
	sete	%cl
	xorl	%esi, %esi
	cmpb	$0, 33(%rbx)
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovnel	%esi, %eax
	je	.LBB7_13
# BB#5:
	testb	%cl, %cl
	jne	.LBB7_13
# BB#6:
	movl	%edx, (%r14)
	xorl	%eax, %eax
	jmp	.LBB7_13
.Lfunc_end7:
	.size	_ZN27CLimitedSequentialOutStream5WriteEPKvjPj, .Lfunc_end7-_ZN27CLimitedSequentialOutStream5WriteEPKvjPj
	.cfi_endproc

	.section	.text._ZN26CLimitedSequentialInStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN26CLimitedSequentialInStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN26CLimitedSequentialInStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN26CLimitedSequentialInStream14QueryInterfaceERK4GUIDPPv,@function
_ZN26CLimitedSequentialInStream14QueryInterfaceERK4GUIDPPv: # @_ZN26CLimitedSequentialInStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB8_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB8_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB8_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB8_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB8_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB8_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB8_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB8_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB8_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB8_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB8_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB8_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB8_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB8_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB8_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB8_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB8_17:                               # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end8:
	.size	_ZN26CLimitedSequentialInStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end8-_ZN26CLimitedSequentialInStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN26CLimitedSequentialInStream6AddRefEv,"axG",@progbits,_ZN26CLimitedSequentialInStream6AddRefEv,comdat
	.weak	_ZN26CLimitedSequentialInStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN26CLimitedSequentialInStream6AddRefEv,@function
_ZN26CLimitedSequentialInStream6AddRefEv: # @_ZN26CLimitedSequentialInStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end9:
	.size	_ZN26CLimitedSequentialInStream6AddRefEv, .Lfunc_end9-_ZN26CLimitedSequentialInStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN26CLimitedSequentialInStream7ReleaseEv,"axG",@progbits,_ZN26CLimitedSequentialInStream7ReleaseEv,comdat
	.weak	_ZN26CLimitedSequentialInStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN26CLimitedSequentialInStream7ReleaseEv,@function
_ZN26CLimitedSequentialInStream7ReleaseEv: # @_ZN26CLimitedSequentialInStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi45:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB10_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB10_2:
	popq	%rcx
	retq
.Lfunc_end10:
	.size	_ZN26CLimitedSequentialInStream7ReleaseEv, .Lfunc_end10-_ZN26CLimitedSequentialInStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN26CLimitedSequentialInStreamD2Ev,"axG",@progbits,_ZN26CLimitedSequentialInStreamD2Ev,comdat
	.weak	_ZN26CLimitedSequentialInStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN26CLimitedSequentialInStreamD2Ev,@function
_ZN26CLimitedSequentialInStreamD2Ev:    # @_ZN26CLimitedSequentialInStreamD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV26CLimitedSequentialInStream+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB11_1
# BB#2:
	movq	(%rdi), %rax
	jmpq	*16(%rax)               # TAILCALL
.LBB11_1:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	retq
.Lfunc_end11:
	.size	_ZN26CLimitedSequentialInStreamD2Ev, .Lfunc_end11-_ZN26CLimitedSequentialInStreamD2Ev
	.cfi_endproc

	.section	.text._ZN26CLimitedSequentialInStreamD0Ev,"axG",@progbits,_ZN26CLimitedSequentialInStreamD0Ev,comdat
	.weak	_ZN26CLimitedSequentialInStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN26CLimitedSequentialInStreamD0Ev,@function
_ZN26CLimitedSequentialInStreamD0Ev:    # @_ZN26CLimitedSequentialInStreamD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi48:
	.cfi_def_cfa_offset 32
.Lcfi49:
	.cfi_offset %rbx, -24
.Lcfi50:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV26CLimitedSequentialInStream+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp12:
	callq	*16(%rax)
.Ltmp13:
.LBB12_2:                               # %_ZN26CLimitedSequentialInStreamD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB12_3:
.Ltmp14:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZN26CLimitedSequentialInStreamD0Ev, .Lfunc_end12-_ZN26CLimitedSequentialInStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end12-.Ltmp13    #   Call between .Ltmp13 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN16CLimitedInStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN16CLimitedInStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN16CLimitedInStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN16CLimitedInStream14QueryInterfaceERK4GUIDPPv,@function
_ZN16CLimitedInStream14QueryInterfaceERK4GUIDPPv: # @_ZN16CLimitedInStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB13_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB13_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB13_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB13_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB13_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB13_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB13_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB13_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB13_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB13_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB13_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB13_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB13_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB13_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB13_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB13_32
.LBB13_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IInStream(%rip), %cl
	jne	.LBB13_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_IInStream+1(%rip), %cl
	jne	.LBB13_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_IInStream+2(%rip), %cl
	jne	.LBB13_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_IInStream+3(%rip), %cl
	jne	.LBB13_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_IInStream+4(%rip), %cl
	jne	.LBB13_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_IInStream+5(%rip), %cl
	jne	.LBB13_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_IInStream+6(%rip), %cl
	jne	.LBB13_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_IInStream+7(%rip), %cl
	jne	.LBB13_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_IInStream+8(%rip), %cl
	jne	.LBB13_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_IInStream+9(%rip), %cl
	jne	.LBB13_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_IInStream+10(%rip), %cl
	jne	.LBB13_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_IInStream+11(%rip), %cl
	jne	.LBB13_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_IInStream+12(%rip), %cl
	jne	.LBB13_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_IInStream+13(%rip), %cl
	jne	.LBB13_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_IInStream+14(%rip), %cl
	jne	.LBB13_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit6
	movb	15(%rsi), %cl
	cmpb	IID_IInStream+15(%rip), %cl
	jne	.LBB13_33
.LBB13_32:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB13_33:                              # %_ZeqRK4GUIDS1_.exit6.thread
	popq	%rcx
	retq
.Lfunc_end13:
	.size	_ZN16CLimitedInStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end13-_ZN16CLimitedInStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN16CLimitedInStream6AddRefEv,"axG",@progbits,_ZN16CLimitedInStream6AddRefEv,comdat
	.weak	_ZN16CLimitedInStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN16CLimitedInStream6AddRefEv,@function
_ZN16CLimitedInStream6AddRefEv:         # @_ZN16CLimitedInStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end14:
	.size	_ZN16CLimitedInStream6AddRefEv, .Lfunc_end14-_ZN16CLimitedInStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN16CLimitedInStream7ReleaseEv,"axG",@progbits,_ZN16CLimitedInStream7ReleaseEv,comdat
	.weak	_ZN16CLimitedInStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN16CLimitedInStream7ReleaseEv,@function
_ZN16CLimitedInStream7ReleaseEv:        # @_ZN16CLimitedInStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB15_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB15_2:
	popq	%rcx
	retq
.Lfunc_end15:
	.size	_ZN16CLimitedInStream7ReleaseEv, .Lfunc_end15-_ZN16CLimitedInStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN16CLimitedInStreamD2Ev,"axG",@progbits,_ZN16CLimitedInStreamD2Ev,comdat
	.weak	_ZN16CLimitedInStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN16CLimitedInStreamD2Ev,@function
_ZN16CLimitedInStreamD2Ev:              # @_ZN16CLimitedInStreamD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV16CLimitedInStream+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB16_1
# BB#2:
	movq	(%rdi), %rax
	jmpq	*16(%rax)               # TAILCALL
.LBB16_1:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	retq
.Lfunc_end16:
	.size	_ZN16CLimitedInStreamD2Ev, .Lfunc_end16-_ZN16CLimitedInStreamD2Ev
	.cfi_endproc

	.section	.text._ZN16CLimitedInStreamD0Ev,"axG",@progbits,_ZN16CLimitedInStreamD0Ev,comdat
	.weak	_ZN16CLimitedInStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN16CLimitedInStreamD0Ev,@function
_ZN16CLimitedInStreamD0Ev:              # @_ZN16CLimitedInStreamD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi55:
	.cfi_def_cfa_offset 32
.Lcfi56:
	.cfi_offset %rbx, -24
.Lcfi57:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV16CLimitedInStream+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB17_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp15:
	callq	*16(%rax)
.Ltmp16:
.LBB17_2:                               # %_ZN16CLimitedInStreamD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB17_3:
.Ltmp17:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end17:
	.size	_ZN16CLimitedInStreamD0Ev, .Lfunc_end17-_ZN16CLimitedInStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp15-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin2   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end17-.Ltmp16    #   Call between .Ltmp16 and .Lfunc_end17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN16CClusterInStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN16CClusterInStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN16CClusterInStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN16CClusterInStream14QueryInterfaceERK4GUIDPPv,@function
_ZN16CClusterInStream14QueryInterfaceERK4GUIDPPv: # @_ZN16CClusterInStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi58:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB18_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB18_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB18_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB18_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB18_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB18_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB18_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB18_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB18_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB18_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB18_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB18_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB18_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB18_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB18_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB18_32
.LBB18_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IInStream(%rip), %cl
	jne	.LBB18_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_IInStream+1(%rip), %cl
	jne	.LBB18_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_IInStream+2(%rip), %cl
	jne	.LBB18_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_IInStream+3(%rip), %cl
	jne	.LBB18_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_IInStream+4(%rip), %cl
	jne	.LBB18_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_IInStream+5(%rip), %cl
	jne	.LBB18_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_IInStream+6(%rip), %cl
	jne	.LBB18_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_IInStream+7(%rip), %cl
	jne	.LBB18_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_IInStream+8(%rip), %cl
	jne	.LBB18_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_IInStream+9(%rip), %cl
	jne	.LBB18_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_IInStream+10(%rip), %cl
	jne	.LBB18_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_IInStream+11(%rip), %cl
	jne	.LBB18_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_IInStream+12(%rip), %cl
	jne	.LBB18_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_IInStream+13(%rip), %cl
	jne	.LBB18_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_IInStream+14(%rip), %cl
	jne	.LBB18_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit6
	movb	15(%rsi), %cl
	cmpb	IID_IInStream+15(%rip), %cl
	jne	.LBB18_33
.LBB18_32:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB18_33:                              # %_ZeqRK4GUIDS1_.exit6.thread
	popq	%rcx
	retq
.Lfunc_end18:
	.size	_ZN16CClusterInStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end18-_ZN16CClusterInStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN16CClusterInStream6AddRefEv,"axG",@progbits,_ZN16CClusterInStream6AddRefEv,comdat
	.weak	_ZN16CClusterInStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN16CClusterInStream6AddRefEv,@function
_ZN16CClusterInStream6AddRefEv:         # @_ZN16CClusterInStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end19:
	.size	_ZN16CClusterInStream6AddRefEv, .Lfunc_end19-_ZN16CClusterInStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN16CClusterInStream7ReleaseEv,"axG",@progbits,_ZN16CClusterInStream7ReleaseEv,comdat
	.weak	_ZN16CClusterInStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN16CClusterInStream7ReleaseEv,@function
_ZN16CClusterInStream7ReleaseEv:        # @_ZN16CClusterInStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi59:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB20_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB20_2:
	popq	%rcx
	retq
.Lfunc_end20:
	.size	_ZN16CClusterInStream7ReleaseEv, .Lfunc_end20-_ZN16CClusterInStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN16CClusterInStreamD2Ev,"axG",@progbits,_ZN16CClusterInStreamD2Ev,comdat
	.weak	_ZN16CClusterInStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN16CClusterInStreamD2Ev,@function
_ZN16CClusterInStreamD2Ev:              # @_ZN16CClusterInStreamD2Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi62:
	.cfi_def_cfa_offset 32
.Lcfi63:
	.cfi_offset %rbx, -24
.Lcfi64:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV16CClusterInStream+16, (%rbx)
	leaq	72(%rbx), %rdi
.Ltmp18:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp19:
# BB#1:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB21_3
# BB#2:
	movq	(%rdi), %rax
.Ltmp24:
	callq	*16(%rax)
.Ltmp25:
.LBB21_3:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB21_6:
.Ltmp26:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB21_4:
.Ltmp20:
	movq	%rax, %r14
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB21_7
# BB#5:
	movq	(%rdi), %rax
.Ltmp21:
	callq	*16(%rax)
.Ltmp22:
.LBB21_7:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit4
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB21_8:
.Ltmp23:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end21:
	.size	_ZN16CClusterInStreamD2Ev, .Lfunc_end21-_ZN16CClusterInStreamD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp18-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin3   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin3   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp21-.Ltmp25         #   Call between .Ltmp25 and .Ltmp21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin3   #     jumps to .Ltmp23
	.byte	1                       #   On action: 1
	.long	.Ltmp22-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Lfunc_end21-.Ltmp22    #   Call between .Ltmp22 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN16CClusterInStreamD0Ev,"axG",@progbits,_ZN16CClusterInStreamD0Ev,comdat
	.weak	_ZN16CClusterInStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN16CClusterInStreamD0Ev,@function
_ZN16CClusterInStreamD0Ev:              # @_ZN16CClusterInStreamD0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi67:
	.cfi_def_cfa_offset 32
.Lcfi68:
	.cfi_offset %rbx, -24
.Lcfi69:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV16CClusterInStream+16, (%rbx)
	leaq	72(%rbx), %rdi
.Ltmp27:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp28:
# BB#1:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_3
# BB#2:
	movq	(%rdi), %rax
.Ltmp33:
	callq	*16(%rax)
.Ltmp34:
.LBB22_3:                               # %_ZN16CClusterInStreamD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB22_7:
.Ltmp35:
	movq	%rax, %r14
	jmp	.LBB22_8
.LBB22_4:
.Ltmp29:
	movq	%rax, %r14
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_8
# BB#5:
	movq	(%rdi), %rax
.Ltmp30:
	callq	*16(%rax)
.Ltmp31:
.LBB22_8:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB22_6:
.Ltmp32:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end22:
	.size	_ZN16CClusterInStreamD0Ev, .Lfunc_end22-_ZN16CClusterInStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp27-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin4   #     jumps to .Ltmp29
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin4   #     jumps to .Ltmp35
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin4   #     jumps to .Ltmp32
	.byte	1                       #   On action: 1
	.long	.Ltmp31-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Lfunc_end22-.Ltmp31    #   Call between .Ltmp31 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN27CLimitedSequentialOutStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN27CLimitedSequentialOutStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN27CLimitedSequentialOutStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN27CLimitedSequentialOutStream14QueryInterfaceERK4GUIDPPv,@function
_ZN27CLimitedSequentialOutStream14QueryInterfaceERK4GUIDPPv: # @_ZN27CLimitedSequentialOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi70:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB23_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB23_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB23_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB23_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB23_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB23_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB23_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB23_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB23_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB23_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB23_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB23_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB23_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB23_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB23_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB23_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB23_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end23:
	.size	_ZN27CLimitedSequentialOutStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end23-_ZN27CLimitedSequentialOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN27CLimitedSequentialOutStream6AddRefEv,"axG",@progbits,_ZN27CLimitedSequentialOutStream6AddRefEv,comdat
	.weak	_ZN27CLimitedSequentialOutStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN27CLimitedSequentialOutStream6AddRefEv,@function
_ZN27CLimitedSequentialOutStream6AddRefEv: # @_ZN27CLimitedSequentialOutStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end24:
	.size	_ZN27CLimitedSequentialOutStream6AddRefEv, .Lfunc_end24-_ZN27CLimitedSequentialOutStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN27CLimitedSequentialOutStream7ReleaseEv,"axG",@progbits,_ZN27CLimitedSequentialOutStream7ReleaseEv,comdat
	.weak	_ZN27CLimitedSequentialOutStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN27CLimitedSequentialOutStream7ReleaseEv,@function
_ZN27CLimitedSequentialOutStream7ReleaseEv: # @_ZN27CLimitedSequentialOutStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi71:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB25_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB25_2:
	popq	%rcx
	retq
.Lfunc_end25:
	.size	_ZN27CLimitedSequentialOutStream7ReleaseEv, .Lfunc_end25-_ZN27CLimitedSequentialOutStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN27CLimitedSequentialOutStreamD2Ev,"axG",@progbits,_ZN27CLimitedSequentialOutStreamD2Ev,comdat
	.weak	_ZN27CLimitedSequentialOutStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN27CLimitedSequentialOutStreamD2Ev,@function
_ZN27CLimitedSequentialOutStreamD2Ev:   # @_ZN27CLimitedSequentialOutStreamD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV27CLimitedSequentialOutStream+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB26_1
# BB#2:
	movq	(%rdi), %rax
	jmpq	*16(%rax)               # TAILCALL
.LBB26_1:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	retq
.Lfunc_end26:
	.size	_ZN27CLimitedSequentialOutStreamD2Ev, .Lfunc_end26-_ZN27CLimitedSequentialOutStreamD2Ev
	.cfi_endproc

	.section	.text._ZN27CLimitedSequentialOutStreamD0Ev,"axG",@progbits,_ZN27CLimitedSequentialOutStreamD0Ev,comdat
	.weak	_ZN27CLimitedSequentialOutStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN27CLimitedSequentialOutStreamD0Ev,@function
_ZN27CLimitedSequentialOutStreamD0Ev:   # @_ZN27CLimitedSequentialOutStreamD0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi74:
	.cfi_def_cfa_offset 32
.Lcfi75:
	.cfi_offset %rbx, -24
.Lcfi76:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV27CLimitedSequentialOutStream+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB27_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp36:
	callq	*16(%rax)
.Ltmp37:
.LBB27_2:                               # %_ZN27CLimitedSequentialOutStreamD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB27_3:
.Ltmp38:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end27:
	.size	_ZN27CLimitedSequentialOutStreamD0Ev, .Lfunc_end27-_ZN27CLimitedSequentialOutStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table27:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp36-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp37-.Ltmp36         #   Call between .Ltmp36 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin5   #     jumps to .Ltmp38
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Lfunc_end27-.Ltmp37    #   Call between .Ltmp37 and .Lfunc_end27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTV26CLimitedSequentialInStream,@object # @_ZTV26CLimitedSequentialInStream
	.section	.rodata,"a",@progbits
	.globl	_ZTV26CLimitedSequentialInStream
	.p2align	3
_ZTV26CLimitedSequentialInStream:
	.quad	0
	.quad	_ZTI26CLimitedSequentialInStream
	.quad	_ZN26CLimitedSequentialInStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN26CLimitedSequentialInStream6AddRefEv
	.quad	_ZN26CLimitedSequentialInStream7ReleaseEv
	.quad	_ZN26CLimitedSequentialInStreamD2Ev
	.quad	_ZN26CLimitedSequentialInStreamD0Ev
	.quad	_ZN26CLimitedSequentialInStream4ReadEPvjPj
	.size	_ZTV26CLimitedSequentialInStream, 64

	.type	_ZTS26CLimitedSequentialInStream,@object # @_ZTS26CLimitedSequentialInStream
	.globl	_ZTS26CLimitedSequentialInStream
	.p2align	4
_ZTS26CLimitedSequentialInStream:
	.asciz	"26CLimitedSequentialInStream"
	.size	_ZTS26CLimitedSequentialInStream, 29

	.type	_ZTS19ISequentialInStream,@object # @_ZTS19ISequentialInStream
	.section	.rodata._ZTS19ISequentialInStream,"aG",@progbits,_ZTS19ISequentialInStream,comdat
	.weak	_ZTS19ISequentialInStream
	.p2align	4
_ZTS19ISequentialInStream:
	.asciz	"19ISequentialInStream"
	.size	_ZTS19ISequentialInStream, 22

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI19ISequentialInStream,@object # @_ZTI19ISequentialInStream
	.section	.rodata._ZTI19ISequentialInStream,"aG",@progbits,_ZTI19ISequentialInStream,comdat
	.weak	_ZTI19ISequentialInStream
	.p2align	4
_ZTI19ISequentialInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19ISequentialInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI19ISequentialInStream, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI26CLimitedSequentialInStream,@object # @_ZTI26CLimitedSequentialInStream
	.section	.rodata,"a",@progbits
	.globl	_ZTI26CLimitedSequentialInStream
	.p2align	4
_ZTI26CLimitedSequentialInStream:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS26CLimitedSequentialInStream
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI19ISequentialInStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI26CLimitedSequentialInStream, 56

	.type	_ZTV16CLimitedInStream,@object # @_ZTV16CLimitedInStream
	.globl	_ZTV16CLimitedInStream
	.p2align	3
_ZTV16CLimitedInStream:
	.quad	0
	.quad	_ZTI16CLimitedInStream
	.quad	_ZN16CLimitedInStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN16CLimitedInStream6AddRefEv
	.quad	_ZN16CLimitedInStream7ReleaseEv
	.quad	_ZN16CLimitedInStreamD2Ev
	.quad	_ZN16CLimitedInStreamD0Ev
	.quad	_ZN16CLimitedInStream4ReadEPvjPj
	.quad	_ZN16CLimitedInStream4SeekExjPy
	.size	_ZTV16CLimitedInStream, 72

	.type	_ZTS16CLimitedInStream,@object # @_ZTS16CLimitedInStream
	.globl	_ZTS16CLimitedInStream
	.p2align	4
_ZTS16CLimitedInStream:
	.asciz	"16CLimitedInStream"
	.size	_ZTS16CLimitedInStream, 19

	.type	_ZTS9IInStream,@object  # @_ZTS9IInStream
	.section	.rodata._ZTS9IInStream,"aG",@progbits,_ZTS9IInStream,comdat
	.weak	_ZTS9IInStream
_ZTS9IInStream:
	.asciz	"9IInStream"
	.size	_ZTS9IInStream, 11

	.type	_ZTI9IInStream,@object  # @_ZTI9IInStream
	.section	.rodata._ZTI9IInStream,"aG",@progbits,_ZTI9IInStream,comdat
	.weak	_ZTI9IInStream
	.p2align	4
_ZTI9IInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS9IInStream
	.quad	_ZTI19ISequentialInStream
	.size	_ZTI9IInStream, 24

	.type	_ZTI16CLimitedInStream,@object # @_ZTI16CLimitedInStream
	.section	.rodata,"a",@progbits
	.globl	_ZTI16CLimitedInStream
	.p2align	4
_ZTI16CLimitedInStream:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS16CLimitedInStream
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI9IInStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI16CLimitedInStream, 56

	.type	_ZTV16CClusterInStream,@object # @_ZTV16CClusterInStream
	.globl	_ZTV16CClusterInStream
	.p2align	3
_ZTV16CClusterInStream:
	.quad	0
	.quad	_ZTI16CClusterInStream
	.quad	_ZN16CClusterInStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN16CClusterInStream6AddRefEv
	.quad	_ZN16CClusterInStream7ReleaseEv
	.quad	_ZN16CClusterInStreamD2Ev
	.quad	_ZN16CClusterInStreamD0Ev
	.quad	_ZN16CClusterInStream4ReadEPvjPj
	.quad	_ZN16CClusterInStream4SeekExjPy
	.size	_ZTV16CClusterInStream, 72

	.type	_ZTS16CClusterInStream,@object # @_ZTS16CClusterInStream
	.globl	_ZTS16CClusterInStream
	.p2align	4
_ZTS16CClusterInStream:
	.asciz	"16CClusterInStream"
	.size	_ZTS16CClusterInStream, 19

	.type	_ZTI16CClusterInStream,@object # @_ZTI16CClusterInStream
	.globl	_ZTI16CClusterInStream
	.p2align	4
_ZTI16CClusterInStream:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS16CClusterInStream
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI9IInStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI16CClusterInStream, 56

	.type	_ZTV27CLimitedSequentialOutStream,@object # @_ZTV27CLimitedSequentialOutStream
	.globl	_ZTV27CLimitedSequentialOutStream
	.p2align	3
_ZTV27CLimitedSequentialOutStream:
	.quad	0
	.quad	_ZTI27CLimitedSequentialOutStream
	.quad	_ZN27CLimitedSequentialOutStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN27CLimitedSequentialOutStream6AddRefEv
	.quad	_ZN27CLimitedSequentialOutStream7ReleaseEv
	.quad	_ZN27CLimitedSequentialOutStreamD2Ev
	.quad	_ZN27CLimitedSequentialOutStreamD0Ev
	.quad	_ZN27CLimitedSequentialOutStream5WriteEPKvjPj
	.size	_ZTV27CLimitedSequentialOutStream, 64

	.type	_ZTS27CLimitedSequentialOutStream,@object # @_ZTS27CLimitedSequentialOutStream
	.globl	_ZTS27CLimitedSequentialOutStream
	.p2align	4
_ZTS27CLimitedSequentialOutStream:
	.asciz	"27CLimitedSequentialOutStream"
	.size	_ZTS27CLimitedSequentialOutStream, 30

	.type	_ZTS20ISequentialOutStream,@object # @_ZTS20ISequentialOutStream
	.section	.rodata._ZTS20ISequentialOutStream,"aG",@progbits,_ZTS20ISequentialOutStream,comdat
	.weak	_ZTS20ISequentialOutStream
	.p2align	4
_ZTS20ISequentialOutStream:
	.asciz	"20ISequentialOutStream"
	.size	_ZTS20ISequentialOutStream, 23

	.type	_ZTI20ISequentialOutStream,@object # @_ZTI20ISequentialOutStream
	.section	.rodata._ZTI20ISequentialOutStream,"aG",@progbits,_ZTI20ISequentialOutStream,comdat
	.weak	_ZTI20ISequentialOutStream
	.p2align	4
_ZTI20ISequentialOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ISequentialOutStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ISequentialOutStream, 24

	.type	_ZTI27CLimitedSequentialOutStream,@object # @_ZTI27CLimitedSequentialOutStream
	.section	.rodata,"a",@progbits
	.globl	_ZTI27CLimitedSequentialOutStream
	.p2align	4
_ZTI27CLimitedSequentialOutStream:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS27CLimitedSequentialOutStream
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI20ISequentialOutStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI27CLimitedSequentialOutStream, 56


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
