	.text
	.file	"main.bc"
	.section	.text._ZN12broken_light10next_stateEv,"axG",@progbits,_ZN12broken_light10next_stateEv,comdat
	.weak	_ZN12broken_light10next_stateEv
	.p2align	4, 0x90
	.type	_ZN12broken_light10next_stateEv,@function
_ZN12broken_light10next_stateEv:        # @_ZN12broken_light10next_stateEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	random
	movq	%rax, %rcx
	sarq	$63, %rcx
	shrq	$62, %rcx
	addq	%rax, %rcx
	andq	$-4, %rcx
	subq	%rcx, %rax
	cmpq	$1, %rax
	movl	8(%rbx), %eax
	je	.LBB0_2
# BB#1:
	leal	1(%rax), %ecx
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$30, %edx
	leal	1(%rax,%rdx), %eax
	andl	$-4, %eax
	subl	%eax, %ecx
	movl	%ecx, %eax
.LBB0_2:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN12broken_light10next_stateEv, .Lfunc_end0-_ZN12broken_light10next_stateEv
	.cfi_endproc

	.text
	.globl	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	.p2align	4, 0x90
	.type	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E,@function
_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E: # @_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	.cfi_startproc
# BB#0:
	movslq	%esi, %rax
	movq	%rdx, 16(%rdi,%rax,8)
	movslq	%ecx, %rcx
	movq	%rdi, 16(%rdx,%rcx,8)
	movq	%r8, 80(%rdi,%rax,8)
	retq
.Lfunc_end1:
	.size	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E, .Lfunc_end1-_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi8:
	.cfi_def_cfa_offset 368
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movl	$-1, 132(%rsp)
	movq	$0, 104(%rsp)
	movq	$.L.str, 112(%rsp)
	movq	$100, 120(%rsp)
	movl	N(%rip), %eax
	movl	%eax, 128(%rsp)
	movl	$5, %edi
	callq	srandom
	movl	$144, %edi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp0:
	movl	$.L.str.1, %esi
	movq	%r14, %rdi
	callq	_ZN7roadlet4initEPKc
.Ltmp1:
# BB#1:                                 # %_ZN7roadletC2EPKc.exit
	movl	$144, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp3:
	movl	$.L.str.2, %esi
	movq	%r15, %rdi
	callq	_ZN7roadlet4initEPKc
.Ltmp4:
# BB#2:                                 # %_ZN7roadletC2EPKc.exit78
	movslq	W(%rip), %rax
	movslq	E(%rip), %rcx
	movq	%r15, 16(%r14,%rax,8)
	movq	%r14, 16(%r15,%rcx,8)
	movq	$_Z11return_nullP7roadletP7vehicle9direction, 80(%r14,%rax,8)
	movq	%r14, 104(%rsp)
	leaq	104(%rsp), %rax
	movq	%rax, 8(%r14)
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	movl	$4, %edi
	callq	malloc
	movq	%rax, %rbp
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movl	%r12d, %edx
	callq	sprintf
	movl	$144, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp6:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	_ZN7roadlet4initEPKc
.Ltmp7:
# BB#4:                                 # %_ZN7roadletC2EPKc.exit81
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$4, %edi
	callq	malloc
	movq	%rax, %r13
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%r12d, %edx
	callq	sprintf
	movl	$144, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp9:
	movq	%rbp, %rdi
	movq	%r13, %rsi
	callq	_ZN7roadlet4initEPKc
.Ltmp10:
# BB#5:                                 # %_ZN7roadletC2EPKc.exit82
                                        #   in Loop: Header=BB2_3 Depth=1
	movslq	N(%rip), %rax
	movslq	S(%rip), %rcx
	movq	%rbx, 16(%r14,%rax,8)
	movq	%r14, 16(%rbx,%rcx,8)
	movq	$_Z8is_emptyP7roadletP7vehicle9direction, 80(%r14,%rax,8)
	movq	%rbp, 16(%r15,%rax,8)
	movq	%r15, 16(%rbp,%rcx,8)
	movq	$_Z8is_emptyP7roadletP7vehicle9direction, 80(%r15,%rax,8)
	movslq	NW(%rip), %rcx
	movslq	SE(%rip), %rdx
	movq	%rbp, 16(%r14,%rcx,8)
	movq	%r14, 16(%rbp,%rdx,8)
	movq	$_Z14lane_switch_okP7roadletP7vehicle9direction, 80(%r14,%rcx,8)
	movslq	NE(%rip), %rcx
	movslq	SW(%rip), %rdx
	movq	%rbx, 16(%r15,%rcx,8)
	movq	%r15, 16(%rbx,%rdx,8)
	movq	$_Z14lane_switch_okP7roadletP7vehicle9direction, 80(%r15,%rcx,8)
	movslq	W(%rip), %rcx
	movslq	E(%rip), %rdx
	movq	%rbp, 16(%rbx,%rcx,8)
	movq	%rbx, 16(%rbp,%rdx,8)
	movq	$_Z11return_nullP7roadletP7vehicle9direction, 80(%rbx,%rcx,8)
	incl	%r12d
	cmpl	$10, %r12d
	movq	%rbx, %r14
	movq	%rbp, %r15
	jl	.LBB2_3
# BB#6:
	movl	$-1, 100(%rsp)
	movq	$.L.str.5, 80(%rsp)
	movl	$100, 88(%rsp)
	movl	$0, 92(%rsp)
	movl	%eax, 96(%rsp)
	movq	%rbx, 72(%rsp)
	leaq	72(%rsp), %rax
	movq	%rax, 8(%rbx)
	movups	72(%rsp), %xmm0
	movups	88(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movl	$_ZSt4cout, %edi
	callq	_ZlsRSo3car
	movb	$10, 176(%rsp)
	leaq	176(%rsp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	leaq	176(%rsp), %r14
	movl	$.L.str.6, %esi
	movq	%r14, %rdi
	callq	_ZN16intersection_4x4C1EPKc
	movq	%r14, %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	_ZN16intersection_4x410connectSinEP7roadletS1_
	movq	$_ZTV5light+16, 136(%rsp)
	leaq	136(%rsp), %rbx
	movl	$3, %esi
	movl	$1, %edx
	movl	$4, %ecx
	movl	$1, %r8d
	movq	%rbx, %rdi
	callq	_ZN5light4initEiiii
	movq	$_ZTV12broken_light+16, 136(%rsp)
	movl	$100000, %ebp           # imm = 0x186A0
	.p2align	4, 0x90
.LBB2_7:                                # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_ZN5light4tickEv
	decl	%ebp
	jne	.LBB2_7
# BB#8:                                 # %.preheader
	movl	$100000, %ebx           # imm = 0x186A0
	.p2align	4, 0x90
.LBB2_9:                                # =>This Inner Loop Header: Depth=1
	movq	176(%rsp), %rdi
	callq	_ZN5light4tickEv
	decl	%ebx
	jne	.LBB2_9
# BB#10:
	movl	$144, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp12:
	movl	$.L.str.7, %esi
	movq	%rbx, %rdi
	callq	_ZN7roadlet4initEPKc
.Ltmp13:
# BB#11:                                # %_ZN7roadletC2EPKc.exit80
	movl	$144, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp15:
	movl	$.L.str.8, %esi
	movq	%rbp, %rdi
	callq	_ZN7roadlet4initEPKc
.Ltmp16:
# BB#12:                                # %_ZN7roadletC2EPKc.exit79
	movslq	N(%rip), %rax
	movslq	S(%rip), %rcx
	movq	%rbp, 16(%rbx,%rax,8)
	movq	%rbx, 16(%rbp,%rcx,8)
	movq	$_Z11return_nullP7roadletP7vehicle9direction, 80(%rbx,%rax,8)
	movl	$-1, 68(%rsp)
	movq	$.L.str.9, 48(%rsp)
	movl	$100, 56(%rsp)
	movl	$0, 60(%rsp)
	movl	%eax, 64(%rsp)
	movq	%rbx, 40(%rsp)
	leaq	40(%rsp), %rax
	movq	%rax, 8(%rbx)
	movups	40(%rsp), %xmm0
	movups	56(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movl	$_ZSt4cout, %edi
	callq	_ZlsRSo3car
	movb	$10, 39(%rsp)
	leaq	39(%rsp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	leaq	176(%rsp), %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	_ZN16intersection_4x411connectEoutEP7roadletS1_
	movl	$100000, %ebp           # imm = 0x186A0
	leaq	104(%rsp), %rbx
	.p2align	4, 0x90
.LBB2_13:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_ZN7vehicle4tickEv
	decl	%ebp
	jne	.LBB2_13
# BB#14:
	xorl	%eax, %eax
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_23:
.Ltmp17:
	jmp	.LBB2_19
.LBB2_20:
.Ltmp14:
	jmp	.LBB2_21
.LBB2_16:
.Ltmp5:
	movq	%rax, %r12
	movq	%r15, %rdi
	jmp	.LBB2_22
.LBB2_15:
.Ltmp2:
	movq	%rax, %r12
	movq	%r14, %rdi
	jmp	.LBB2_22
.LBB2_18:
.Ltmp11:
.LBB2_19:
	movq	%rax, %r12
	movq	%rbp, %rdi
	jmp	.LBB2_22
.LBB2_17:
.Ltmp8:
.LBB2_21:
	movq	%rax, %r12
	movq	%rbx, %rdi
.LBB2_22:
	callq	_ZdlPv
	movq	%r12, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\254\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\251\001"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp6-.Ltmp4           #   Call between .Ltmp4 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 7 <<
	.long	.Ltmp9-.Ltmp7           #   Call between .Ltmp7 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 8 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp12-.Ltmp10         #   Call between .Ltmp10 and .Ltmp12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp15-.Ltmp13         #   Call between .Ltmp13 and .Ltmp15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Lfunc_end2-.Ltmp16     #   Call between .Ltmp16 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN5light10next_stateEv,"axG",@progbits,_ZN5light10next_stateEv,comdat
	.weak	_ZN5light10next_stateEv
	.p2align	4, 0x90
	.type	_ZN5light10next_stateEv,@function
_ZN5light10next_stateEv:                # @_ZN5light10next_stateEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %ecx
	leal	1(%rcx), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$30, %edx
	leal	1(%rcx,%rdx), %ecx
	andl	$-4, %ecx
	subl	%ecx, %eax
	retq
.Lfunc_end3:
	.size	_ZN5light10next_stateEv, .Lfunc_end3-_ZN5light10next_stateEv
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_main.ii,@function
_GLOBAL__sub_I_main.ii:                 # @_GLOBAL__sub_I_main.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end4:
	.size	_GLOBAL__sub_I_main.ii, .Lfunc_end4-_GLOBAL__sub_I_main.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"fred"
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"R start"
	.size	.L.str.1, 8

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"L start"
	.size	.L.str.2, 8

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"R%d"
	.size	.L.str.3, 4

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"L%d"
	.size	.L.str.4, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"blocker 2"
	.size	.L.str.5, 10

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"intersection "
	.size	.L.str.6, 14

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"East Road R "
	.size	.L.str.7, 13

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"East Road L "
	.size	.L.str.8, 13

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"blocker"
	.size	.L.str.9, 8

	.type	_ZTV12broken_light,@object # @_ZTV12broken_light
	.section	.rodata._ZTV12broken_light,"aG",@progbits,_ZTV12broken_light,comdat
	.weak	_ZTV12broken_light
	.p2align	3
_ZTV12broken_light:
	.quad	0
	.quad	_ZTI12broken_light
	.quad	_ZN12broken_light10next_stateEv
	.size	_ZTV12broken_light, 24

	.type	_ZTS12broken_light,@object # @_ZTS12broken_light
	.section	.rodata._ZTS12broken_light,"aG",@progbits,_ZTS12broken_light,comdat
	.weak	_ZTS12broken_light
_ZTS12broken_light:
	.asciz	"12broken_light"
	.size	_ZTS12broken_light, 15

	.type	_ZTS5light,@object      # @_ZTS5light
	.section	.rodata._ZTS5light,"aG",@progbits,_ZTS5light,comdat
	.weak	_ZTS5light
_ZTS5light:
	.asciz	"5light"
	.size	_ZTS5light, 7

	.type	_ZTI5light,@object      # @_ZTI5light
	.section	.rodata._ZTI5light,"aG",@progbits,_ZTI5light,comdat
	.weak	_ZTI5light
	.p2align	3
_ZTI5light:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS5light
	.size	_ZTI5light, 16

	.type	_ZTI12broken_light,@object # @_ZTI12broken_light
	.section	.rodata._ZTI12broken_light,"aG",@progbits,_ZTI12broken_light,comdat
	.weak	_ZTI12broken_light
	.p2align	4
_ZTI12broken_light:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS12broken_light
	.quad	_ZTI5light
	.size	_ZTI12broken_light, 24

	.type	_ZTV5light,@object      # @_ZTV5light
	.section	.rodata._ZTV5light,"aG",@progbits,_ZTV5light,comdat
	.weak	_ZTV5light
	.p2align	3
_ZTV5light:
	.quad	0
	.quad	_ZTI5light
	.quad	_ZN5light10next_stateEv
	.size	_ZTV5light, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_main.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
