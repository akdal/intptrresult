	.text
	.file	"libclamav_htmlnorm.bc"
	.globl	cli_readline
	.p2align	4, 0x90
	.type	cli_readline,@function
cli_readline:                           # @cli_readline
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	%r12d, %edi
	callq	cli_malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_1
# BB#2:
	testq	%r14, %r14
	je	.LBB0_19
# BB#3:
	movq	16(%r14), %r13
	movq	8(%r14), %rax
	cmpq	%rax, %r13
	jge	.LBB0_23
# BB#4:                                 # %.lr.ph94
	movq	(%r14), %rcx
	addq	%rcx, %r13
	addq	%rcx, %rax
	decl	%r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	leal	1(%rbx), %ecx
	cmpl	%r12d, %ecx
	jae	.LBB0_8
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=1
	cmpb	$10, (%r13,%rbx)
	je	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_5 Depth=1
	leaq	1(%r13,%rbx), %rcx
	incq	%rbx
	cmpq	%rax, %rcx
	jb	.LBB0_5
.LBB0_8:                                # %..critedge_crit_edge
	leal	1(%rbx), %r12d
	addq	%r13, %rbx
	cmpq	%rax, %rbx
	je	.LBB0_9
# BB#10:
	movzbl	(%rbx), %ebp
	cmpq	$10, %rbp
	jne	.LBB0_11
# BB#30:
	movl	%r12d, %edx
	movq	%r15, %rdi
	movq	%r13, %rsi
	jmp	.LBB0_18
.LBB0_1:
	xorl	%r15d, %r15d
	jmp	.LBB0_29
.LBB0_19:
	testq	%r13, %r13
	je	.LBB0_20
# BB#21:
	movq	%r15, %rdi
	movl	%r12d, %esi
	movq	%r13, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB0_23
# BB#22:
	movq	%r15, %rdi
	callq	strlen
	testl	%eax, %eax
	je	.LBB0_23
# BB#24:
	leal	-1(%r12), %ebx
	cmpl	%ebx, %eax
	jne	.LBB0_29
# BB#25:                                # %.preheader
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movl	%ebx, %r14d
	addl	$-2, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_26:                               # =>This Inner Loop Header: Depth=1
	leal	(%r12,%rbx), %ecx
	movzbl	(%r15,%rcx), %ecx
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB0_28
# BB#27:                                #   in Loop: Header=BB0_26 Depth=1
	decq	%rbx
	movl	%ebx, %ecx
	addl	%r14d, %ecx
	jne	.LBB0_26
	jmp	.LBB0_29
.LBB0_20:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_23:
	movq	%r15, %rdi
	callq	free
	xorl	%r15d, %r15d
	jmp	.LBB0_29
.LBB0_9:
	decl	%r12d
	jmp	.LBB0_17
.LBB0_11:                               # %.preheader85
	callq	__ctype_b_loc
	cmpl	$2, %r12d
	movl	%r12d, %ecx
	jb	.LBB0_16
# BB#12:                                # %.preheader85
	movq	(%rax), %rax
	movzwl	(%rax,%rbp,2), %ecx
	andl	$8192, %ecx             # imm = 0x2000
	testw	%cx, %cx
	movl	%r12d, %ecx
	jne	.LBB0_16
# BB#13:                                # %.lr.ph.preheader
	decq	%rbx
	movl	%r12d, %ecx
	.p2align	4, 0x90
.LBB0_14:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	decl	%ecx
	cmpl	$2, %ecx
	jb	.LBB0_16
# BB#15:                                # %.lr.ph
                                        #   in Loop: Header=BB0_14 Depth=1
	movzbl	(%rbx), %edx
	movzwl	(%rax,%rdx,2), %edx
	andl	$8192, %edx             # imm = 0x2000
	decq	%rbx
	testw	%dx, %dx
	je	.LBB0_14
.LBB0_16:                               # %._crit_edge
	cmpl	$1, %ecx
	cmovnel	%ecx, %r12d
.LBB0_17:
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
.LBB0_18:
	callq	memcpy
	movl	%r12d, %eax
	movb	$0, (%r15,%rax)
	addq	%rax, 16(%r14)
.LBB0_29:                               # %.loopexit
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_28:
	movl	%ebx, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	callq	fseek
	addl	%ebx, %r14d
	movb	$0, (%r15,%r14)
	jmp	.LBB0_29
.Lfunc_end0:
	.size	cli_readline, .Lfunc_end0-cli_readline
	.cfi_endproc

	.globl	html_tag_arg_free
	.p2align	4, 0x90
	.type	html_tag_arg_free,@function
html_tag_arg_free:                      # @html_tag_arg_free
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	8(%r14), %rdi
	cmpl	$0, (%r14)
	jle	.LBB1_8
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %rdi
	callq	free
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	callq	free
.LBB1_4:                                #   in Loop: Header=BB1_2 Depth=1
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.LBB1_7
# BB#5:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	(%rax,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB1_7
# BB#6:                                 #   in Loop: Header=BB1_2 Depth=1
	callq	blobDestroy
.LBB1_7:                                #   in Loop: Header=BB1_2 Depth=1
	incq	%rbx
	movslq	(%r14), %rax
	movq	8(%r14), %rdi
	cmpq	%rax, %rbx
	jl	.LBB1_2
.LBB1_8:                                # %._crit_edge
	testq	%rdi, %rdi
	je	.LBB1_10
# BB#9:
	callq	free
.LBB1_10:
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_12
# BB#11:
	callq	free
.LBB1_12:
	leaq	8(%r14), %rbx
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_14
# BB#13:
	callq	free
.LBB1_14:
	movl	$0, (%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movq	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	html_tag_arg_free, .Lfunc_end1-html_tag_arg_free
	.cfi_endproc

	.globl	html_normalise_mem
	.p2align	4, 0x90
	.type	html_normalise_mem,@function
html_normalise_mem:                     # @html_normalise_mem
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi18:
	.cfi_def_cfa_offset 32
	movq	%rdi, (%rsp)
	movq	%rsi, 8(%rsp)
	movq	$0, 16(%rsp)
	movq	%rsp, %rsi
	movl	$-1, %edi
	callq	cli_html_normalise
	addq	$24, %rsp
	retq
.Lfunc_end2:
	.size	html_normalise_mem, .Lfunc_end2-html_normalise_mem
	.cfi_endproc

	.p2align	4, 0x90
	.type	cli_html_normalise,@function
cli_html_normalise:                     # @cli_html_normalise
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$5704, %rsp             # imm = 0x1648
.Lcfi25:
	.cfi_def_cfa_offset 5760
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %rbp
	movl	%edi, %ebx
	testq	%r8, %r8
	je	.LBB3_2
# BB#1:
	movb	24(%r8), %r12b
	andb	$2, %r12b
	shrb	%r12b
	jmp	.LBB3_3
.LBB3_2:
	xorl	%r12d, %r12d
.LBB3_3:
	movl	$0, 228(%rsp)
	testq	%rsi, %rsi
	movq	%rsi, 264(%rsp)         # 8-byte Spill
	je	.LBB3_19
# BB#4:
	xorl	%eax, %eax
	movq	%rax, 216(%rsp)         # 8-byte Spill
.LBB3_5:
	testb	%r12b, %r12b
	je	.LBB3_9
# BB#6:
	leaq	336(%rsp), %rdi
	movl	$.L.str.9, %esi
	movl	$16384, %edx            # imm = 0x4000
	callq	init_entity_converter
	movl	%eax, %r14d
	testl	%r14d, %r14d
	je	.LBB3_9
# BB#7:
	cmpq	$0, 264(%rsp)           # 8-byte Folded Reload
	jne	.LBB3_1092
# BB#8:
	movq	216(%rsp), %rdi         # 8-byte Reload
	callq	fclose
	jmp	.LBB3_1092
.LBB3_9:
	movl	$0, 224(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 232(%rsp)
	movq	$0, 248(%rsp)
	xorl	%r14d, %r14d
	testq	%rbp, %rbp
	je	.LBB3_23
# BB#10:
	leaq	4672(%rsp), %rbx
	movl	$1024, %esi             # imm = 0x400
	movl	$.L.str.10, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rcx
	callq	snprintf
	movl	$448, %esi              # imm = 0x1C0
	movq	%rbx, %rdi
	callq	mkdir
	testl	%eax, %eax
	je	.LBB3_12
# BB#11:
	callq	__errno_location
	cmpl	$17, (%rax)
	movl	$0, %r13d
	movl	$0, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$0, %eax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movl	$0, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	$0, %eax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movl	$0, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jne	.LBB3_1076
.LBB3_12:
	movl	$8200, %edi             # imm = 0x2008
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_1029
# BB#13:
	movl	$8200, %edi             # imm = 0x2008
	callq	cli_malloc
	testq	%rax, %rax
	je	.LBB3_1026
# BB#14:
	movq	%rbp, %r13
	movq	%r15, 128(%rsp)         # 8-byte Spill
	movq	%rbx, 160(%rsp)         # 8-byte Spill
	movq	%rax, %r15
	movl	$8200, %edi             # imm = 0x2008
	callq	cli_malloc
	testq	%rax, %rax
	je	.LBB3_1027
# BB#15:
	movq	%rax, 176(%rsp)         # 8-byte Spill
	xorl	%r14d, %r14d
	leaq	4672(%rsp), %rbx
	movl	$1024, %esi             # imm = 0x400
	movl	$.L.str.11, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r13, %rbp
	movq	%rbp, %rcx
	callq	snprintf
	movl	$577, %esi              # imm = 0x241
	movl	$384, %edx              # imm = 0x180
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	open
	movq	160(%rsp), %rbx         # 8-byte Reload
	movl	%eax, (%rbx)
	testl	%eax, %eax
	je	.LBB3_1048
# BB#16:
	xorl	%r14d, %r14d
	leaq	4672(%rsp), %rbx
	movl	$1024, %esi             # imm = 0x400
	movl	$.L.str.12, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rcx
	callq	snprintf
	movl	$577, %esi              # imm = 0x241
	movl	$384, %edx              # imm = 0x180
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	open
	movq	%r15, %rbx
	movl	%eax, (%rbx)
	testl	%eax, %eax
	je	.LBB3_1049
# BB#17:
	xorl	%r14d, %r14d
	leaq	4672(%rsp), %rbx
	movl	$1024, %esi             # imm = 0x400
	movl	$.L.str.13, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rcx
	callq	snprintf
	movl	$577, %esi              # imm = 0x241
	movl	$384, %edx              # imm = 0x180
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	open
	movq	176(%rsp), %rbx         # 8-byte Reload
	movl	%eax, (%rbx)
	testl	%eax, %eax
	je	.LBB3_1062
# BB#18:
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rcx
	movl	$0, 8196(%rcx)
	movq	%r15, %rax
	movl	$0, 8196(%rax)
	movl	$0, 8196(%rbx)
	movq	%rbx, %rdx
	movq	%rax, %rsi
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	128(%rsp), %r15         # 8-byte Reload
	movq	%r13, %rbp
	movq	%rbx, %r14
	jmp	.LBB3_24
.LBB3_19:
	xorl	%r14d, %r14d
	testl	%ebx, %ebx
	js	.LBB3_26
# BB#20:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%ebx, %edi
	callq	lseek
	movl	%ebx, %edi
	callq	dup
	movl	%eax, %ebx
	testl	%ebx, %ebx
	js	.LBB3_1092
# BB#21:
	movl	$.L.str.4, %esi
	movl	%ebx, %edi
	callq	fdopen
	movq	%rax, %rcx
	movq	%rcx, 216(%rsp)         # 8-byte Spill
	testq	%rax, %rax
	jne	.LBB3_5
# BB#22:
	movl	%ebx, %edi
	callq	close
	jmp	.LBB3_1092
.LBB3_23:
	xorl	%edx, %edx
	xorl	%eax, %eax
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
.LBB3_24:
	movq	%rbp, 304(%rsp)         # 8-byte Spill
	movq	%r15, 128(%rsp)         # 8-byte Spill
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rax, 208(%rsp)         # 8-byte Spill
	testb	%r12b, %r12b
	movq	264(%rsp), %rbx         # 8-byte Reload
	je	.LBB3_27
# BB#25:
	leaq	336(%rsp), %rdi
	movl	$8192, %ecx             # imm = 0x2000
	movq	216(%rsp), %rsi         # 8-byte Reload
	movq	%rbx, %rdx
	callq	encoding_norm_readline
	jmp	.LBB3_28
.LBB3_26:
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB3_1092
.LBB3_27:
	movl	$8192, %edx             # imm = 0x2000
	movq	216(%rsp), %rdi         # 8-byte Reload
	movq	%rbx, %rsi
	callq	cli_readline
.LBB3_28:                               # %.preheader1772
	movq	%rax, %r15
	movq	%r14, 176(%rsp)         # 8-byte Spill
	testq	%r15, %r15
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	%r12d, 116(%rsp)        # 4-byte Spill
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	je	.LBB3_1012
# BB#29:                                # %.lr.ph1987
	leaq	4(%rcx), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	leaq	4(%rdx), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	leaq	4(%rax), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
                                        # implicit-def: %EAX
	movl	%eax, 188(%rsp)         # 4-byte Spill
                                        # implicit-def: %EAX
	movl	%eax, 88(%rsp)          # 4-byte Spill
	movl	$0, %r8d
                                        # implicit-def: %EAX
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movl	$0, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
                                        # implicit-def: %EAX
	movl	%eax, 204(%rsp)         # 4-byte Spill
	movl	$0, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
                                        # implicit-def: %EAX
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movl	$0, 92(%rsp)            # 4-byte Folded Spill
	movl	$1, %ebp
	movl	$0, %r13d
                                        # implicit-def: %EAX
	movl	%eax, 60(%rsp)          # 4-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 144(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 168(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movl	$0, 28(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB3_30:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_32 Depth 2
                                        #     Child Loop BB3_989 Depth 2
                                        #       Child Loop BB3_759 Depth 3
                                        #       Child Loop BB3_754 Depth 3
                                        #       Child Loop BB3_849 Depth 3
                                        #       Child Loop BB3_624 Depth 3
                                        #       Child Loop BB3_863 Depth 3
                                        #       Child Loop BB3_289 Depth 3
                                        #       Child Loop BB3_813 Depth 3
                                        #       Child Loop BB3_673 Depth 3
                                        #       Child Loop BB3_822 Depth 3
                                        #       Child Loop BB3_831 Depth 3
                                        #       Child Loop BB3_834 Depth 3
                                        #       Child Loop BB3_836 Depth 3
                                        #       Child Loop BB3_839 Depth 3
                                        #       Child Loop BB3_435 Depth 3
                                        #       Child Loop BB3_669 Depth 3
                                        #       Child Loop BB3_845 Depth 3
                                        #       Child Loop BB3_898 Depth 3
                                        #         Child Loop BB3_921 Depth 4
                                        #       Child Loop BB3_599 Depth 3
                                        #       Child Loop BB3_681 Depth 3
                                        #       Child Loop BB3_80 Depth 3
                                        #       Child Loop BB3_636 Depth 3
                                        #       Child Loop BB3_419 Depth 3
                                        #       Child Loop BB3_709 Depth 3
	movq	32(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	cmovneq	%r15, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movb	(%r15), %bl
	testb	%bl, %bl
	movq	%r15, 328(%rsp)         # 8-byte Spill
	je	.LBB3_35
# BB#31:                                # %.lr.ph1888
                                        #   in Loop: Header=BB3_30 Depth=1
	movq	%r13, %r14
	movl	%r8d, %r13d
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movq	%r15, %r12
	.p2align	4, 0x90
.LBB3_32:                               #   Parent Loop BB3_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%bl, %ecx
	testb	$32, 1(%rax,%rcx,2)
	je	.LBB3_36
# BB#33:                                #   in Loop: Header=BB3_32 Depth=2
	movzbl	1(%r12), %ebx
	incq	%r12
	testb	%bl, %bl
	jne	.LBB3_32
# BB#34:                                #   in Loop: Header=BB3_30 Depth=1
	movl	%r13d, %r8d
	movq	%r14, %r13
	jmp	.LBB3_999
	.p2align	4, 0x90
.LBB3_35:                               #   in Loop: Header=BB3_30 Depth=1
	movq	%r15, %r12
	jmp	.LBB3_999
	.p2align	4, 0x90
.LBB3_36:                               # %.lr.ph1947.preheader
                                        #   in Loop: Header=BB3_30 Depth=1
	movl	%r14d, %r15d
	xorl	%eax, %eax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%r13d, %r8d
	jmp	.LBB3_989
	.p2align	4, 0x90
.LBB3_37:                               #   in Loop: Header=BB3_989 Depth=2
	cmpl	$23, %ebp
	ja	.LBB3_996
# BB#38:                                #   in Loop: Header=BB3_989 Depth=2
	movl	%ebp, %eax
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_39:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$60, %bl
	jne	.LBB3_229
# BB#40:                                #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	movq	128(%rsp), %r14         # 8-byte Reload
	je	.LBB3_44
# BB#41:                                #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_43
# BB#42:                                # %html_output_flush.exit.i1451
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebx
	callq	cli_writen
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_43:                               #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	$60, 4(%rbp,%rax)
.LBB3_44:                               #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_48
# BB#45:                                #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_47
# BB#46:                                # %html_output_flush.exit11.i1452
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebx
	callq	cli_writen
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_47:                               #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	$60, 4(%rdi,%rax)
.LBB3_48:                               # %html_output_c.exit1453
                                        #   in Loop: Header=BB3_989 Depth=2
	testq	%rsi, %rsi
	je	.LBB3_53
# BB#49:                                # %html_output_c.exit1453
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpl	$0, 92(%rsp)            # 4-byte Folded Reload
	je	.LBB3_53
# BB#50:                                #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rsi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_52
# BB#51:                                # %html_output_flush.exit.i1454
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rsi), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	104(%rsp), %rsi         # 8-byte Reload
	movl	%r8d, %ebx
	callq	cli_writen
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
	xorl	%eax, %eax
.LBB3_52:                               #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rsi)
	cltq
	movb	$60, 4(%rsi,%rax)
.LBB3_53:                               # %html_output_c.exit1455
                                        #   in Loop: Header=BB3_989 Depth=2
	testq	%r14, %r14
	je	.LBB3_404
# BB#54:                                #   in Loop: Header=BB3_989 Depth=2
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB3_626
# BB#55:                                #   in Loop: Header=BB3_989 Depth=2
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	je	.LBB3_735
# BB#56:                                #   in Loop: Header=BB3_989 Depth=2
	movl	4(%r14), %eax
	testl	%eax, %eax
	je	.LBB3_735
# BB#57:                                #   in Loop: Header=BB3_989 Depth=2
	cmpq	%r12, 32(%rsp)          # 8-byte Folded Reload
	jae	.LBB3_61
# BB#58:                                #   in Loop: Header=BB3_989 Depth=2
	movl	%r8d, %r15d
	movq	24(%r14), %rax
	movslq	28(%rsp), %rbx          # 4-byte Folded Reload
	movq	-8(%rax,%rbx,8), %rdi
	callq	blobGetDataSize
	cmpq	$1024, %rax             # imm = 0x400
	ja	.LBB3_817
# BB#59:                                #   in Loop: Header=BB3_989 Depth=2
	movl	$1024, %ecx             # imm = 0x400
	subq	%rax, %rcx
	movl	$0, %eax
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%r15d, %r8d
	je	.LBB3_818
# BB#60:                                #   in Loop: Header=BB3_989 Depth=2
	decq	%rbx
	movq	%r12, %rdx
	movq	32(%rsp), %rsi          # 8-byte Reload
	subq	%rsi, %rdx
	movq	24(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	cmpq	%rdx, %rcx
	cmovbq	%rcx, %rdx
	callq	blobAddData
	movl	%r15d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB3_61:                               # %html_tag_contents_append.exit
                                        #   in Loop: Header=BB3_989 Depth=2
	xorl	%eax, %eax
	jmp	.LBB3_818
.LBB3_62:                               #   in Loop: Header=BB3_989 Depth=2
	movl	%r8d, %r13d
	callq	__ctype_tolower_loc
	movl	%r13d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rax, %r14
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB3_66
# BB#63:                                #   in Loop: Header=BB3_989 Depth=2
	movq	(%r14), %rax
	movzbl	%bl, %ecx
	movb	(%rax,%rcx,4), %bl
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_65
# BB#64:                                # %html_output_flush.exit.i1514
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	%r13d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_65:                               #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	%bl, 4(%rbp,%rax)
.LBB3_66:                               # %html_output_c.exit1515
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpl	$0, 92(%rsp)            # 4-byte Folded Reload
	je	.LBB3_71
# BB#67:                                #   in Loop: Header=BB3_989 Depth=2
	testq	%rsi, %rsi
	je	.LBB3_71
# BB#68:                                #   in Loop: Header=BB3_989 Depth=2
	movq	(%r14), %rax
	movzbl	(%r12), %ecx
	movb	(%rax,%rcx,4), %bl
	movl	8196(%rsi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_70
# BB#69:                                # %html_output_flush.exit.i1516
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rsi), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	104(%rsp), %rsi         # 8-byte Reload
	callq	cli_writen
	movl	%r13d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
	xorl	%eax, %eax
.LBB3_70:                               #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rsi)
	cltq
	movb	%bl, 4(%rsi,%rax)
.LBB3_71:                               # %html_output_c.exit1517
                                        #   in Loop: Header=BB3_989 Depth=2
	xorl	%eax, %eax
	cmpb	$62, (%r12)
	sete	%al
	movl	$1, %ecx
	cmovel	%ecx, %r15d
	movl	%r15d, %r13d
	leal	2(%rax,%rax,2), %r15d
	incq	%r12
	jmp	.LBB3_998
.LBB3_72:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$35, %bl
	jne	.LBB3_232
# BB#73:                                #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	movl	$0, 204(%rsp)           # 4-byte Folded Spill
	movl	%r15d, %r13d
	movl	$12, %r15d
	xorl	%eax, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	jmp	.LBB3_998
.LBB3_74:                               #   in Loop: Header=BB3_989 Depth=2
	movl	%r8d, 44(%rsp)          # 4-byte Spill
	cmpb	$59, %bl
	jne	.LBB3_244
# BB#75:                                #   in Loop: Header=BB3_989 Depth=2
	movq	120(%rsp), %rax         # 8-byte Reload
	movb	$0, 2592(%rsp,%rax)
	leaq	336(%rsp), %rdi
	leaq	2592(%rsp), %rsi
	callq	entity_norm
	testq	%rax, %rax
	je	.LBB3_405
# BB#76:                                # %.preheader1769
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movb	(%rax), %r14b
	testb	%r14b, %r14b
	je	.LBB3_531
# BB#77:                                # %.lr.ph1897
                                        #   in Loop: Header=BB3_989 Depth=2
	callq	__ctype_tolower_loc
	movq	%rax, 120(%rsp)         # 8-byte Spill
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB3_634
# BB#78:                                # %.lr.ph1897.split.preheader
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	$1, %r13d
	jmp	.LBB3_80
	.p2align	4, 0x90
.LBB3_79:                               # %..lr.ph1897.split_crit_edge
                                        #   in Loop: Header=BB3_80 Depth=3
	movzbl	(%rbx,%r13), %r14d
	incq	%r13
.LBB3_80:                               # %.lr.ph1897.split
                                        #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	movzbl	%r14b, %ecx
	movzbl	(%rax,%rcx,4), %r14d
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_82
# BB#81:                                # %html_output_flush.exit.i1602
                                        #   in Loop: Header=BB3_80 Depth=3
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_82:                               #   in Loop: Header=BB3_80 Depth=3
	leal	1(%rax), %ecx
	testq	%rdx, %rdx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	%r14b, 4(%rbp,%rax)
	movq	80(%rsp), %rbx          # 8-byte Reload
	je	.LBB3_86
# BB#83:                                #   in Loop: Header=BB3_80 Depth=3
	movl	8196(%rdx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_85
# BB#84:                                # %html_output_flush.exit11.i1603
                                        #   in Loop: Header=BB3_80 Depth=3
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	$0, 8196(%rdx)
	xorl	%eax, %eax
.LBB3_85:                               #   in Loop: Header=BB3_80 Depth=3
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdx)
	cltq
	movb	%r14b, 4(%rdx,%rax)
.LBB3_86:                               # %html_output_c.exit1604
                                        #   in Loop: Header=BB3_80 Depth=3
	cmpl	$9, %r15d
	jne	.LBB3_89
# BB#87:                                # %html_output_c.exit1604
                                        #   in Loop: Header=BB3_80 Depth=3
	movq	48(%rsp), %rcx          # 8-byte Reload
	cmpl	$1023, %ecx             # imm = 0x3FF
	jg	.LBB3_89
# BB#88:                                #   in Loop: Header=BB3_80 Depth=3
	movslq	%ecx, %rax
	incl	%ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movb	%r14b, 512(%rsp,%rax)
.LBB3_89:                               #   in Loop: Header=BB3_80 Depth=3
	movq	%rbx, %rdi
	callq	strlen
	cmpq	%rax, %r13
	jb	.LBB3_79
	jmp	.LBB3_644
.LBB3_90:                               #   in Loop: Header=BB3_989 Depth=2
	movl	%r8d, %ebp
	callq	__ctype_b_loc
	movl	%ebp, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rax), %rax
	movzbl	%bl, %ecx
	movb	1(%rax,%rcx,2), %al
	leaq	1(%r12), %rcx
	andb	$32, %al
	cmovneq	%rcx, %r12
	movl	%r15d, %r13d
	movl	$0, %ecx
	cmovel	%ecx, %r13d
	testb	%al, %al
	movl	$5, %eax
	cmovnel	%eax, %r15d
	jmp	.LBB3_998
.LBB3_91:                               #   in Loop: Header=BB3_989 Depth=2
	movl	%r8d, %ebp
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movzbl	%bl, %ecx
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB3_250
# BB#92:                                #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	movq	(%rsp), %rdi            # 8-byte Reload
	je	.LBB3_96
# BB#93:                                #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_95
# BB#94:                                # %html_output_flush.exit.i
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbx), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	$0, 8196(%rbx)
	xorl	%eax, %eax
.LBB3_95:                               #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbx)
	cltq
	movb	$32, 4(%rbx,%rax)
.LBB3_96:                               #   in Loop: Header=BB3_989 Depth=2
	xorl	%r13d, %r13d
	testq	%rdi, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%ebp, %r8d
	je	.LBB3_998
# BB#97:                                #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_99
# BB#98:                                # %html_output_flush.exit11.i
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	%ebp, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_99:                               #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	$32, 4(%rdi,%rax)
	jmp	.LBB3_998
.LBB3_100:                              #   in Loop: Header=BB3_989 Depth=2
	movl	188(%rsp), %ebp         # 4-byte Reload
	testl	%ebp, %ebp
	jne	.LBB3_251
# BB#101:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$33, %bl
	jne	.LBB3_251
# BB#102:                               #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB3_106
# BB#103:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_105
# BB#104:                               # %html_output_flush.exit.i1461
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebx
	callq	cli_writen
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_105:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	$33, 4(%rbp,%rax)
.LBB3_106:                              # %html_output_c.exit1462
                                        #   in Loop: Header=BB3_989 Depth=2
	testq	%rsi, %rsi
	je	.LBB3_111
# BB#107:                               # %html_output_c.exit1462
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpl	$0, 92(%rsp)            # 4-byte Folded Reload
	je	.LBB3_111
# BB#108:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rsi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_110
# BB#109:                               # %html_output_flush.exit.i1463
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rsi), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	104(%rsp), %rsi         # 8-byte Reload
	movl	%r8d, %ebx
	callq	cli_writen
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
	xorl	%eax, %eax
.LBB3_110:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rsi)
	cltq
	movb	$33, 4(%rsi,%rax)
.LBB3_111:                              # %html_output_c.exit1464
                                        #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_114
# BB#112:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB3_114
# BB#113:                               #   in Loop: Header=BB3_989 Depth=2
	decl	%eax
	movl	%eax, 8196(%rdi)
.LBB3_114:                              #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	movl	$2, %r15d
	xorl	%r13d, %r13d
	movl	$0, 188(%rsp)           # 4-byte Folded Spill
	jmp	.LBB3_998
.LBB3_115:                              #   in Loop: Header=BB3_989 Depth=2
	cmpb	$61, %bl
	jne	.LBB3_266
# BB#116:                               #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	movl	88(%rsp), %ebp          # 4-byte Reload
	je	.LBB3_120
# BB#117:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_119
# BB#118:                               # %html_output_flush.exit.i1475
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbx), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %r14d
	callq	cli_writen
	movl	%r14d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbx)
	xorl	%eax, %eax
.LBB3_119:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbx)
	cltq
	movb	$61, 4(%rbx,%rax)
.LBB3_120:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_124
# BB#121:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_123
# BB#122:                               # %html_output_flush.exit11.i1476
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebx
	callq	cli_writen
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_123:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	$61, 4(%rdi,%rax)
.LBB3_124:                              # %html_output_c.exit1477
                                        #   in Loop: Header=BB3_989 Depth=2
	movslq	%ebp, %rax
	movb	$0, 3632(%rsp,%rax)
	jmp	.LBB3_145
.LBB3_125:                              #   in Loop: Header=BB3_989 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	cmpl	$6, %eax
	je	.LBB3_326
# BB#126:                               #   in Loop: Header=BB3_989 Depth=2
	cmpl	$5, %eax
	jne	.LBB3_327
# BB#127:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.16, %esi
	movl	$5, %edx
	leaq	512(%rsp), %rdi
	movl	%r8d, %ebp
	callq	strncmp
	movl	%ebp, %r8d
	movq	8(%rsp), %rsi           # 8-byte Reload
	testl	%eax, %eax
	jne	.LBB3_327
# BB#128:                               #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	je	.LBB3_131
# BB#129:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rcx), %eax
	testl	%eax, %eax
	jle	.LBB3_131
# BB#130:                               #   in Loop: Header=BB3_989 Depth=2
	decl	%eax
	movl	%eax, 8196(%rcx)
.LBB3_131:                              #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB3_134
# BB#132:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB3_134
# BB#133:                               #   in Loop: Header=BB3_989 Depth=2
	decl	%eax
	movl	%eax, 8196(%rdi)
.LBB3_134:                              #   in Loop: Header=BB3_989 Depth=2
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	$18, %ebx
	movl	$8, %r15d
	cmpl	$2, 60(%rsp)            # 4-byte Folded Reload
	jne	.LBB3_523
# BB#135:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$2, 60(%rsp)            # 4-byte Folded Spill
	jmp	.LBB3_733
.LBB3_136:                              #   in Loop: Header=BB3_989 Depth=2
	cmpb	$61, %bl
	jne	.LBB3_279
# BB#137:                               #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB3_141
# BB#138:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_140
# BB#139:                               # %html_output_flush.exit.i1487
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebx
	callq	cli_writen
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_140:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	$61, 4(%rbp,%rax)
.LBB3_141:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_145
# BB#142:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_144
# BB#143:                               # %html_output_flush.exit11.i1488
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebx
	callq	cli_writen
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_144:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	$61, 4(%rdi,%rax)
.LBB3_145:                              # %html_output_c.exit1489
                                        #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	$5, %r15d
	movl	$9, %r13d
	movl	$2, 60(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	jmp	.LBB3_998
.LBB3_146:                              #   in Loop: Header=BB3_989 Depth=2
	movl	%r8d, 44(%rsp)          # 4-byte Spill
	movb	1552(%rsp), %bl
	cmpb	$47, %bl
	jne	.LBB3_281
# BB#147:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.18, %esi
	leaq	1552(%rsp), %rdi
	callq	strcmp
	testl	%eax, %eax
	movl	92(%rsp), %ecx          # 4-byte Reload
	cmovel	%eax, %ecx
	movl	%ecx, 92(%rsp)          # 4-byte Spill
	movq	128(%rsp), %rbp         # 8-byte Reload
	jne	.LBB3_152
# BB#148:                               #   in Loop: Header=BB3_989 Depth=2
	movq	8(%rsp), %rdx           # 8-byte Reload
	testq	%rdx, %rdx
	je	.LBB3_152
# BB#149:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_151
# BB#150:                               # %html_output_flush.exit.i1518
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rdx), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	104(%rsp), %rsi         # 8-byte Reload
	callq	cli_writen
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	$0, 8196(%rdx)
	xorl	%eax, %eax
.LBB3_151:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdx)
	cltq
	movb	$10, 4(%rdx,%rax)
	movl	$0, 92(%rsp)            # 4-byte Folded Spill
.LBB3_152:                              # %html_output_c.exit1519
                                        #   in Loop: Header=BB3_989 Depth=2
	testq	%rbp, %rbp
	je	.LBB3_628
# BB#153:                               #   in Loop: Header=BB3_989 Depth=2
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	je	.LBB3_628
# BB#154:                               #   in Loop: Header=BB3_989 Depth=2
	movl	4(%rbp), %eax
	testl	%eax, %eax
	je	.LBB3_628
# BB#155:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$47, 1552(%rsp)
	jne	.LBB3_159
# BB#156:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$97, 1553(%rsp)
	jne	.LBB3_159
# BB#157:                               #   in Loop: Header=BB3_989 Depth=2
	movb	1554(%rsp), %al
	testb	%al, %al
	jne	.LBB3_159
# BB#158:                               #   in Loop: Header=BB3_989 Depth=2
	movq	24(%rbp), %rax
	movslq	28(%rsp), %rbx          # 4-byte Folded Reload
	movq	-8(%rax,%rbx,8), %rdi
	movl	$.L.str.52, %esi
	movl	$1, %edx
	callq	blobAddData
	movq	24(%rbp), %rax
	movq	-8(%rax,%rbx,8), %rdi
	callq	blobClose
	movl	$0, 28(%rsp)            # 4-byte Folded Spill
.LBB3_159:                              # %.thread2060
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	%r12, %r14
	jmp	.LBB3_629
.LBB3_160:                              #   in Loop: Header=BB3_989 Depth=2
	movq	136(%rsp), %rbp         # 8-byte Reload
	testl	%ebp, %ebp
	je	.LBB3_304
# BB#161:                               # %thread-pre-split1733
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpb	$59, %bl
	jne	.LBB3_311
	jmp	.LBB3_308
.LBB3_162:                              #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	decq	144(%rsp)               # 8-byte Folded Spill
	movl	$13, %eax
	cmovel	%r15d, %eax
	jmp	.LBB3_375
.LBB3_163:                              #   in Loop: Header=BB3_989 Depth=2
	movl	%r8d, %r13d
	movl	$.L.str.7, %esi
	movl	$4, %edx
	movq	%r12, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_316
# BB#164:                               #   in Loop: Header=BB3_989 Depth=2
	callq	__ctype_tolower_loc
	movq	%rax, %r14
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	movq	(%r14), %rax
	movzbl	%bl, %ecx
	movb	(%rax,%rcx,4), %bl
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	je	.LBB3_168
# BB#165:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_167
# BB#166:                               # %html_output_flush.exit.i1637
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_167:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	%bl, 4(%rbp,%rax)
.LBB3_168:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	movl	%r13d, %r8d
	je	.LBB3_172
# BB#169:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_171
# BB#170:                               # %html_output_flush.exit11.i1638
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	%r13d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_171:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	%bl, 4(%rdi,%rax)
.LBB3_172:                              # %html_output_c.exit1639
                                        #   in Loop: Header=BB3_989 Depth=2
	testq	%rsi, %rsi
	je	.LBB3_176
# BB#173:                               #   in Loop: Header=BB3_989 Depth=2
	movq	(%r14), %rax
	movzbl	(%r12), %ecx
	movb	(%rax,%rcx,4), %bl
	movl	8196(%rsi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_175
# BB#174:                               # %html_output_flush.exit.i1640
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rsi), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	104(%rsp), %rsi         # 8-byte Reload
	callq	cli_writen
	movl	%r13d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
	xorl	%eax, %eax
.LBB3_175:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rsi)
	cltq
	movb	%bl, 4(%rsi,%rax)
.LBB3_176:                              # %html_output_c.exit1641
                                        #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	movl	%r15d, %r13d
	movl	$14, %r15d
	jmp	.LBB3_998
.LBB3_177:                              #   in Loop: Header=BB3_989 Depth=2
	movl	%r8d, %ebp
	movq	%r12, %rdi
	callq	strlen
	cmpq	$8, %rax
	jae	.LBB3_291
# BB#178:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$1, %r15d
	xorl	%r13d, %r13d
	jmp	.LBB3_292
.LBB3_179:                              #   in Loop: Header=BB3_989 Depth=2
	cmpq	$0, 144(%rsp)           # 8-byte Folded Reload
	je	.LBB3_318
# BB#180:                               #   in Loop: Header=BB3_989 Depth=2
	testb	%bl, %bl
	js	.LBB3_402
# BB#181:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	280(%rsp), %rax         # 4-byte Folded Reload
	movslq	table_order(,%rax,4), %rax
	movzbl	%bl, %ecx
	shlq	$9, %rax
	movslq	decrypt_tables(%rax,%rcx,4), %rbp
	cmpq	$255, %rbp
	jne	.LBB3_388
# BB#182:                               #   in Loop: Header=BB3_989 Depth=2
	leaq	1(%r12), %rbx
	decq	144(%rsp)               # 8-byte Folded Spill
	movl	$255, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movzbl	1(%r12), %eax
	cmpq	$42, %rax
	ja	.LBB3_403
# BB#183:                               #   in Loop: Header=BB3_989 Depth=2
	jmpq	*.LJTI3_2(,%rax,8)
.LBB3_184:                              #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB3_188
# BB#185:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_187
# BB#186:                               # %html_output_flush.exit.i1645
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %r14d
	callq	cli_writen
	movl	%r14d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_187:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	$60, 4(%rbp,%rax)
.LBB3_188:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_192
# BB#189:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_191
# BB#190:                               # %html_output_flush.exit11.i1646
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebp
	callq	cli_writen
	movl	%ebp, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_191:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	$60, 4(%rdi,%rax)
.LBB3_192:                              # %html_output_c.exit1647
                                        #   in Loop: Header=BB3_989 Depth=2
	testq	%rsi, %rsi
	je	.LBB3_403
# BB#193:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rsi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_195
# BB#194:                               # %html_output_flush.exit.i1648
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rsi), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	104(%rsp), %rsi         # 8-byte Reload
	movl	%r8d, %ebp
	callq	cli_writen
	movl	%ebp, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
	xorl	%eax, %eax
.LBB3_195:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rsi)
	cltq
	movb	$60, 4(%rsi,%rax)
	jmp	.LBB3_403
.LBB3_196:                              #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	movl	%r8d, %ebx
	callq	cli_dbgmsg
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%r15d, %r13d
	movl	$17, %r15d
	jmp	.LBB3_998
.LBB3_197:                              #   in Loop: Header=BB3_989 Depth=2
	cmpb	$34, %bl
	je	.LBB3_331
# BB#198:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$39, %bl
	jne	.LBB3_338
# BB#199:                               #   in Loop: Header=BB3_989 Depth=2
	movq	152(%rsp), %rax         # 8-byte Reload
	orl	60(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB3_334
# BB#200:                               #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	movl	$0, 88(%rsp)            # 4-byte Folded Spill
	movl	$5, %eax
	movl	$8, %r15d
	jmp	.LBB3_337
.LBB3_201:                              #   in Loop: Header=BB3_989 Depth=2
	movl	$1, %r8d
	cmpq	$0, 304(%rsp)           # 8-byte Folded Reload
	je	.LBB3_324
# BB#202:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$8200, %edi             # imm = 0x2008
	callq	cli_malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB3_1069
# BB#203:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$1024, %esi             # imm = 0x400
	movl	$.L.str.10, %edx
	xorl	%eax, %eax
	leaq	4672(%rsp), %rbx
	movq	%rbx, %rdi
	movq	304(%rsp), %rcx         # 8-byte Reload
	callq	snprintf
	movq	%rbx, %rdi
	callq	cli_gentemp
	movq	%rax, %rbx
	movl	$.L.str.46, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	movl	$577, %esi              # imm = 0x241
	movl	$384, %edx              # imm = 0x180
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	open
	movl	%eax, (%rbp)
	movq	%rbx, %rdi
	callq	free
	movl	(%rbp), %edi
	testl	%edi, %edi
	movq	176(%rsp), %r13         # 8-byte Reload
	je	.LBB3_1093
# BB#204:                               # %html_output_str.exit1682
                                        #   in Loop: Header=BB3_989 Depth=2
	movups	.L.str.47(%rip), %xmm0
	movups	%xmm0, 4(%rbp)
	movl	$174420841, 20(%rbp)    # imm = 0xA657369
	movabsq	$2322280091611311476, %rax # imm = 0x203A657079742D74
	movq	%rax, 30(%rbp)
	movabsq	$3275364211029339971, %rax # imm = 0x2D746E65746E6F43
	movq	%rax, 24(%rbp)
	movl	$34, 8196(%rbp)
	movl	$34, %edx
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	jne	.LBB3_207
# BB#205:                               # %html_output_str.exit1682
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpb	$59, 512(%rsp)
	jne	.LBB3_207
# BB#206:                               # %html_output_str.exit1685
                                        #   in Loop: Header=BB3_989 Depth=2
	movabsq	$7020109268283581812, %rax # imm = 0x616C702F74786574
	movq	%rax, 38(%rbp)
	movb	$10, 48(%rbp)
	movw	$28265, 46(%rbp)        # imm = 0x6E69
	movl	$45, 8196(%rbp)
	movl	$45, %edx
.LBB3_207:                              #   in Loop: Header=BB3_989 Depth=2
	leaq	4(%rbp), %r14
	movq	48(%rsp), %rax          # 8-byte Reload
	leal	(%rdx,%rax), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jl	.LBB3_209
# BB#208:                               #   in Loop: Header=BB3_989 Depth=2
	movq	%r14, %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_writen
	movl	$0, 8196(%rbp)
	xorl	%edx, %edx
.LBB3_209:                              # %html_output_flush.exit.i1687
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	48(%rsp), %rbx          # 8-byte Reload
	cmpl	$8192, %ebx             # imm = 0x2000
	jl	.LBB3_342
# BB#210:                               #   in Loop: Header=BB3_989 Depth=2
	testl	%edx, %edx
	jle	.LBB3_212
# BB#211:                               #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movq	%r14, %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_writen
	movl	$0, 8196(%rbp)
.LBB3_212:                              # %html_output_flush.exit13.i1688
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	leaq	512(%rsp), %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_writen
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	je	.LBB3_343
	jmp	.LBB3_344
.LBB3_213:                              #   in Loop: Header=BB3_989 Depth=2
	movl	%ebx, %eax
	addb	$-34, %al
	cmpb	$5, %al
	ja	.LBB3_376
# BB#214:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$3, %ebp
	movl	$20, %r13d
	movzbl	%al, %eax
	jmpq	*.LJTI3_1(,%rax,8)
.LBB3_215:                              #   in Loop: Header=BB3_989 Depth=2
	cmpl	$1, 60(%rsp)            # 4-byte Folded Reload
	jne	.LBB3_571
# BB#216:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$21, %ebp
	cmpl	$0, 152(%rsp)           # 4-byte Folded Reload
	je	.LBB3_819
.LBB3_571:                              #   in Loop: Header=BB3_989 Depth=2
	movl	$20, %ebp
	movq	168(%rsp), %rbx         # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB3_819
# BB#572:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_574
# BB#573:                               # %html_output_flush.exit.i1699
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbx), %edi
	leaq	4(%rbx), %rsi
	movl	$8192, %edx             # imm = 0x2000
	movl	%r8d, %r13d
	callq	cli_writen
	movl	%r13d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbx)
	xorl	%eax, %eax
.LBB3_574:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbx)
	cltq
	movb	$34, 4(%rbx,%rax)
	jmp	.LBB3_819
.LBB3_218:                              #   in Loop: Header=BB3_989 Depth=2
	xorl	%eax, %eax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movl	$5, %r15d
	movl	$8, %r13d
	movl	$2, 60(%rsp)            # 4-byte Folded Spill
	movq	168(%rsp), %rbx         # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB3_325
# BB#219:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbx), %edx
	testl	%edx, %edx
	jle	.LBB3_221
# BB#220:                               #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbx), %edi
	leaq	4(%rbx), %rsi
	callq	cli_writen
	movl	$0, 8196(%rbx)
.LBB3_221:                              # %html_output_flush.exit
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbx), %edi
	callq	close
	movq	288(%rsp), %rdi         # 8-byte Reload
	callq	free
	xorl	%r8d, %r8d
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	jmp	.LBB3_998
.LBB3_222:                              #   in Loop: Header=BB3_989 Depth=2
	cmpq	$2, 144(%rsp)           # 8-byte Folded Reload
	jne	.LBB3_293
# BB#223:                               #   in Loop: Header=BB3_989 Depth=2
	movq	168(%rsp), %rbp         # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB3_430
# BB#224:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_226
# BB#225:                               # %html_output_flush.exit.i1705
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	leaq	4(%rbp), %rsi
	movl	$8192, %edx             # imm = 0x2000
	movl	%r8d, %ebx
	callq	cli_writen
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_226:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movq	136(%rsp), %rcx         # 8-byte Reload
	movb	%cl, 4(%rbp,%rax)
	jmp	.LBB3_431
.LBB3_227:                              #   in Loop: Header=BB3_989 Depth=2
	movl	%r8d, %r13d
	movq	136(%rsp), %rbp         # 8-byte Reload
	shll	$4, %ebp
	incq	144(%rsp)               # 8-byte Folded Spill
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movzbl	%bl, %ebx
	movzwl	(%rax,%rbx,2), %eax
	testb	$16, %ah
	jne	.LBB3_302
# BB#228:                               #   in Loop: Header=BB3_989 Depth=2
	movq	%rbp, 136(%rsp)         # 8-byte Spill
	movl	%r15d, %eax
	jmp	.LBB3_374
.LBB3_229:                              #   in Loop: Header=BB3_989 Depth=2
	movl	%r8d, %r13d
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movzbl	%bl, %ebp
	testb	$32, 1(%rax,%rbp,2)
	jne	.LBB3_352
# BB#230:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$38, %bl
	jne	.LBB3_480
# BB#231:                               #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	movl	$3, %r15d
	jmp	.LBB3_353
.LBB3_232:                              #   in Loop: Header=BB3_989 Depth=2
	cmpb	$0, 116(%rsp)           # 1-byte Folded Reload
	jne	.LBB3_249
# BB#233:                               #   in Loop: Header=BB3_989 Depth=2
	cmpl	$9, %r15d
	movq	48(%rsp), %rbp          # 8-byte Reload
	jne	.LBB3_236
# BB#234:                               #   in Loop: Header=BB3_989 Depth=2
	cmpl	$1023, %ebp             # imm = 0x3FF
	jg	.LBB3_236
# BB#235:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	%ebp, %rax
	incl	%ebp
	movb	$38, 512(%rsp,%rax)
.LBB3_236:                              #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB3_240
# BB#237:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_239
# BB#238:                               # %html_output_flush.exit.i1599
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbx), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %r14d
	callq	cli_writen
	movl	%r14d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbx)
	xorl	%eax, %eax
.LBB3_239:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbx)
	cltq
	movb	$38, 4(%rbx,%rax)
.LBB3_240:                              #   in Loop: Header=BB3_989 Depth=2
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	testq	%rdi, %rdi
	je	.LBB3_998
# BB#241:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_243
# BB#242:                               # %html_output_flush.exit11.i1600
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebx
	callq	cli_writen
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_243:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	$38, 4(%rdi,%rax)
	jmp	.LBB3_998
.LBB3_244:                              #   in Loop: Header=BB3_989 Depth=2
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movzbl	%bl, %ecx
	testb	$8, (%rax,%rcx,2)
	jne	.LBB3_247
# BB#245:                               # %switch.early.test
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpb	$58, %bl
	je	.LBB3_247
# BB#246:                               # %switch.early.test
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpb	$95, %bl
	jne	.LBB3_583
.LBB3_247:                              #   in Loop: Header=BB3_989 Depth=2
	cmpq	$1023, 120(%rsp)        # 8-byte Folded Reload
                                        # imm = 0x3FF
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	44(%rsp), %r8d          # 4-byte Reload
	ja	.LBB3_585
.LBB3_248:                              #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	movq	120(%rsp), %rax         # 8-byte Reload
	movb	%bl, 2592(%rsp,%rax)
	incq	%rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
.LBB3_249:                              #   in Loop: Header=BB3_989 Depth=2
	movl	%r15d, %r13d
	movl	$4, %r15d
	jmp	.LBB3_998
.LBB3_250:                              #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	movl	%r15d, %r13d
	movl	$6, %r15d
	jmp	.LBB3_292
.LBB3_251:                              # %thread-pre-split
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpb	$62, %bl
	jne	.LBB3_354
# BB#252:                               #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB3_256
# BB#253:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_255
# BB#254:                               # %html_output_flush.exit.i1465
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbx), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %r14d
	callq	cli_writen
	movl	%r14d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbx)
	xorl	%eax, %eax
.LBB3_255:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbx)
	cltq
	movb	$62, 4(%rbx,%rax)
.LBB3_256:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_260
# BB#257:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_259
# BB#258:                               # %html_output_flush.exit11.i1466
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebx
	callq	cli_writen
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_259:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	$62, 4(%rdi,%rax)
.LBB3_260:                              # %html_output_c.exit1467
                                        #   in Loop: Header=BB3_989 Depth=2
	testq	%rsi, %rsi
	je	.LBB3_265
# BB#261:                               # %html_output_c.exit1467
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpl	$0, 92(%rsp)            # 4-byte Folded Reload
	je	.LBB3_265
# BB#262:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rsi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_264
# BB#263:                               # %html_output_flush.exit.i1468
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rsi), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	104(%rsp), %rsi         # 8-byte Reload
	movl	%r8d, %ebx
	callq	cli_writen
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
	xorl	%eax, %eax
.LBB3_264:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rsi)
	cltq
	movb	$62, 4(%rsi,%rax)
.LBB3_265:                              # %html_output_c.exit1469
                                        #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	movslq	%ebp, %rax
	movb	$0, 1552(%rsp,%rax)
	movl	$5, %r15d
	movl	$11, %r13d
	jmp	.LBB3_998
.LBB3_266:                              #   in Loop: Header=BB3_989 Depth=2
	movl	%r8d, %r13d
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movzbl	%bl, %ecx
	testb	$32, 1(%rax,%rcx,2)
	movl	88(%rsp), %ebp          # 4-byte Reload
	jne	.LBB3_371
# BB#267:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$62, %bl
	movq	(%rsp), %rdi            # 8-byte Reload
	jne	.LBB3_494
# BB#268:                               #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB3_272
# BB#269:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_271
# BB#270:                               # %html_output_flush.exit.i1478
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbx), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	$0, 8196(%rbx)
	xorl	%eax, %eax
.LBB3_271:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbx)
	cltq
	movb	$62, 4(%rbx,%rax)
.LBB3_272:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_276
# BB#273:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_275
# BB#274:                               # %html_output_flush.exit11.i1479
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_275:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	$62, 4(%rdi,%rax)
.LBB3_276:                              # %html_output_c.exit1480
                                        #   in Loop: Header=BB3_989 Depth=2
	testl	%ebp, %ebp
	jle	.LBB3_278
# BB#277:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	%ebp, %rax
	movb	$0, 3632(%rsp,%rax)
	xorl	%edx, %edx
	leaq	224(%rsp), %rdi
	leaq	3632(%rsp), %rsi
	callq	html_tag_arg_add
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB3_278:                              #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	movl	$11, %r15d
	movq	8(%rsp), %rsi           # 8-byte Reload
	jmp	.LBB3_317
.LBB3_279:                              #   in Loop: Header=BB3_989 Depth=2
	movl	$8, %r15d
	movl	88(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	jle	.LBB3_432
# BB#280:                               #   in Loop: Header=BB3_989 Depth=2
	cltq
	movb	$0, 3632(%rsp,%rax)
	xorl	%r13d, %r13d
	xorl	%edx, %edx
	leaq	224(%rsp), %rdi
	leaq	3632(%rsp), %rsi
	movl	%r8d, %ebx
	callq	html_tag_arg_add
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 88(%rsp)            # 4-byte Folded Spill
	jmp	.LBB3_998
.LBB3_281:                              #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.21, %esi
	leaq	1552(%rsp), %rdi
	callq	strcmp
	testl	%eax, %eax
	movq	128(%rsp), %rbp         # 8-byte Reload
	je	.LBB3_433
# BB#282:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$0, 116(%rsp)           # 1-byte Folded Reload
	je	.LBB3_284
# BB#283:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.27, %esi
	leaq	1552(%rsp), %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_671
.LBB3_284:                              #   in Loop: Header=BB3_989 Depth=2
	movl	$1, %r13d
	testq	%rbp, %rbp
	je	.LBB3_933
# BB#285:                               #   in Loop: Header=BB3_989 Depth=2
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	movq	%r12, %r14
	movq	32(%rsp), %rax          # 8-byte Reload
	cmoveq	%rax, %r14
	testq	%rax, %rax
	cmovneq	%rax, %r14
	cmpb	$97, %bl
	jne	.LBB3_617
# BB#286:                               #   in Loop: Header=BB3_989 Depth=2
	movb	1553(%rsp), %al
	testb	%al, %al
	jne	.LBB3_617
# BB#287:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	224(%rsp), %rbx
	testq	%rbx, %rbx
	jle	.LBB3_934
# BB#288:                               # %.lr.ph.preheader.i1543
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	%rbp, %r14
	movq	232(%rsp), %r13
	movq	240(%rsp), %r15
	xorl	%ebp, %ebp
.LBB3_289:                              # %.lr.ph.i1546
                                        #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r13,%rbp,8), %rdi
	movl	$.L.str.34, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_809
# BB#290:                               #   in Loop: Header=BB3_289 Depth=3
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB3_289
	jmp	.LBB3_865
.LBB3_291:                              #   in Loop: Header=BB3_989 Depth=2
	movzbl	%bl, %eax
	movl	base64_chars(,%rax,4), %eax
	shll	$2, %eax
	cltq
	movzbl	1(%r12), %ecx
	movl	base64_chars(,%rcx,4), %ecx
	movl	%ecx, %edx
	sarl	$4, %edx
	movslq	%edx, %rdx
	addq	%rax, %rdx
	andl	$15, %ecx
	shll	$12, %ecx
	addq	%rdx, %rcx
	movzbl	2(%r12), %eax
	movl	base64_chars(,%rax,4), %eax
	movl	%eax, %edx
	andl	$-4, %edx
	shll	$6, %edx
	movslq	%edx, %rdx
	addq	%rcx, %rdx
	andl	$3, %eax
	shll	$22, %eax
	addq	%rdx, %rax
	movzbl	3(%r12), %ecx
	movl	base64_chars(,%rcx,4), %ecx
	shll	$16, %ecx
	movslq	%ecx, %rcx
	addq	%rax, %rcx
	movzbl	4(%r12), %eax
	movl	base64_chars(,%rax,4), %eax
	shll	$26, %eax
	cltq
	addq	%rcx, %rax
	movzbl	5(%r12), %ecx
	movl	base64_chars(,%rcx,4), %ecx
	andl	$-16, %ecx
	shll	$20, %ecx
	movslq	%ecx, %rcx
	addq	%rax, %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	addq	$8, %r12
	movl	$16, %r15d
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	movq	%rax, 280(%rsp)         # 8-byte Spill
.LBB3_292:                              # %.critedge.backedge
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%ebp, %r8d
	jmp	.LBB3_998
.LBB3_293:                              #   in Loop: Header=BB3_989 Depth=2
	movq	168(%rsp), %rbp         # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB3_297
# BB#294:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_296
# BB#295:                               # %html_output_flush.exit.i1707
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	leaq	4(%rbp), %rsi
	movl	$8192, %edx             # imm = 0x2000
	movl	%r8d, %ebx
	callq	cli_writen
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_296:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	$37, 4(%rbp,%rax)
.LBB3_297:                              # %html_output_c.exit1708
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpq	$1, 144(%rsp)           # 8-byte Folded Reload
	jne	.LBB3_580
# BB#298:                               #   in Loop: Header=BB3_989 Depth=2
	testq	%rbp, %rbp
	movq	136(%rsp), %rax         # 8-byte Reload
	je	.LBB3_577
# BB#299:                               #   in Loop: Header=BB3_989 Depth=2
	leal	48(%rax), %ebx
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_301
# BB#300:                               # %html_output_flush.exit.i1709
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	leaq	4(%rbp), %rsi
	movl	$8192, %edx             # imm = 0x2000
	movl	%r8d, %ebp
	callq	cli_writen
	movl	%ebp, %r8d
	movq	168(%rsp), %rbp         # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_301:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	%bl, 4(%rbp,%rax)
	jmp	.LBB3_578
.LBB3_302:                              #   in Loop: Header=BB3_989 Depth=2
	testb	$8, %ah
	jne	.LBB3_372
# BB#303:                               #   in Loop: Header=BB3_989 Depth=2
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movl	(%rax,%rbx,4), %eax
	leal	-87(%rbp,%rax), %ebp
	jmp	.LBB3_373
.LBB3_304:                              #   in Loop: Header=BB3_989 Depth=2
	cmpb	$59, %bl
	je	.LBB3_308
# BB#305:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$120, %bl
	je	.LBB3_307
# BB#306:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$88, %bl
	jne	.LBB3_311
.LBB3_307:                              #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	xorl	%eax, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	$1, 204(%rsp)           # 4-byte Folded Spill
	jmp	.LBB3_479
.LBB3_308:                              #   in Loop: Header=BB3_989 Depth=2
	cmpl	$9, %r15d
	jne	.LBB3_532
# BB#309:                               #   in Loop: Header=BB3_989 Depth=2
	movq	48(%rsp), %rcx          # 8-byte Reload
	cmpl	$1023, %ecx             # imm = 0x3FF
	leaq	318(%rsp), %rbp
	jg	.LBB3_533
# BB#310:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	%ecx, %rax
	incl	%ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	136(%rsp), %rcx         # 8-byte Reload
	movb	%cl, 512(%rsp,%rax)
	cmpb	$0, 116(%rsp)           # 1-byte Folded Reload
	jne	.LBB3_534
	jmp	.LBB3_545
.LBB3_311:                              #   in Loop: Header=BB3_989 Depth=2
	movl	%r8d, %r13d
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movzbl	%bl, %ebx
	movzwl	(%rax,%rbx,2), %eax
	testb	$8, %ah
	jne	.LBB3_314
# BB#312:                               #   in Loop: Header=BB3_989 Depth=2
	cmpl	$0, 204(%rsp)           # 4-byte Folded Reload
	je	.LBB3_555
# BB#313:                               #   in Loop: Header=BB3_989 Depth=2
	movl	%eax, %ecx
	andl	$4096, %ecx             # imm = 0x1000
	je	.LBB3_555
.LBB3_314:                              #   in Loop: Header=BB3_989 Depth=2
	movl	%ebp, %ecx
	shll	$4, %ecx
	addl	%ebp, %ebp
	cmpl	$0, 204(%rsp)           # 4-byte Folded Reload
	leal	(%rbp,%rbp,4), %ebp
	cmovnel	%ecx, %ebp
	testb	$8, %ah
	jne	.LBB3_477
# BB#315:                               #   in Loop: Header=BB3_989 Depth=2
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movl	(%rax,%rbx,4), %eax
	addl	$-87, %eax
	jmp	.LBB3_478
.LBB3_316:                              #   in Loop: Header=BB3_989 Depth=2
	addq	$4, %r12
	movl	$15, %r15d
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB3_317:                              # %.critedge.backedge
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	%r13d, %r8d
	xorl	%r13d, %r13d
	jmp	.LBB3_998
.LBB3_318:                              #   in Loop: Header=BB3_989 Depth=2
	movl	$13, %r15d
	movl	$1, %r13d
	testq	%rsi, %rsi
	je	.LBB3_323
# BB#319:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rsi), %edx
	testl	%edx, %edx
	jle	.LBB3_322
# BB#320:                               #   in Loop: Header=BB3_989 Depth=2
	leal	10(%rdx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jl	.LBB3_322
# BB#321:                               #   in Loop: Header=BB3_989 Depth=2
	movl	(%rsi), %edi
	movq	104(%rsp), %rsi         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r8d, %ebx
	callq	cli_writen
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
	xorl	%edx, %edx
.LBB3_322:                              # %html_output_flush.exit.i1643
                                        #   in Loop: Header=BB3_989 Depth=2
	movslq	%edx, %rax
	movabsq	$8390322045806915388, %rcx # imm = 0x7470697263732F3C
	movq	%rcx, 4(%rsi,%rax)
	movw	$2622, 12(%rsi,%rax)    # imm = 0xA3E
	addl	$10, 8196(%rsi)
.LBB3_323:                              #   in Loop: Header=BB3_989 Depth=2
	movl	$12, %eax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	jmp	.LBB3_998
.LBB3_324:                              #   in Loop: Header=BB3_989 Depth=2
	xorl	%eax, %eax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	jmp	.LBB3_580
.LBB3_325:                              #   in Loop: Header=BB3_989 Depth=2
	xorl	%eax, %eax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	xorl	%r8d, %r8d
	jmp	.LBB3_998
.LBB3_628:                              #   in Loop: Header=BB3_989 Depth=2
	movq	32(%rsp), %r14          # 8-byte Reload
.LBB3_629:                              # %.thread2060
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.20, %esi
	leaq	1552(%rsp), %rdi
	callq	strcmp
	movl	$1, %r13d
	testl	%eax, %eax
	jne	.LBB3_934
# BB#630:                               #   in Loop: Header=BB3_989 Depth=2
	movq	96(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB3_632
# BB#631:                               #   in Loop: Header=BB3_989 Depth=2
	callq	free
.LBB3_632:                              #   in Loop: Header=BB3_989 Depth=2
	xorl	%eax, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	jmp	.LBB3_934
.LBB3_326:                              #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.17, %esi
	movl	$6, %edx
	leaq	512(%rsp), %rdi
	movl	%r8d, %ebp
	callq	strncmp
	movl	%ebp, %r8d
	movq	8(%rsp), %rsi           # 8-byte Reload
	testl	%eax, %eax
	je	.LBB3_514
.LBB3_327:                              #   in Loop: Header=BB3_989 Depth=2
	cmpb	$34, %bl
	je	.LBB3_438
# BB#328:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$39, %bl
	movq	48(%rsp), %rbp          # 8-byte Reload
	je	.LBB3_451
# BB#329:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$38, %bl
	jne	.LBB3_463
# BB#330:                               #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	movl	$3, %ebx
	movl	$9, %r15d
	movq	(%rsp), %rdi            # 8-byte Reload
	jmp	.LBB3_733
.LBB3_331:                              #   in Loop: Header=BB3_989 Depth=2
	cmpl	$1, 60(%rsp)            # 4-byte Folded Reload
	jne	.LBB3_334
# BB#332:                               #   in Loop: Header=BB3_989 Depth=2
	cmpl	$0, 152(%rsp)           # 4-byte Folded Reload
	jne	.LBB3_334
# BB#333:                               #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	movl	$0, 88(%rsp)            # 4-byte Folded Spill
	movl	$5, %eax
	movl	$8, %r15d
	movl	$1, 60(%rsp)            # 4-byte Folded Spill
	jmp	.LBB3_337
.LBB3_334:                              #   in Loop: Header=BB3_989 Depth=2
	movq	48(%rsp), %rcx          # 8-byte Reload
	cmpl	$1023, %ecx             # imm = 0x3FF
	jg	.LBB3_336
# BB#335:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	%ecx, %rax
	incl	%ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movb	$34, 512(%rsp,%rax)
.LBB3_336:                              #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	movl	$18, %eax
.LBB3_337:                              #   in Loop: Header=BB3_989 Depth=2
	xorl	%ecx, %ecx
	cmpb	$92, (%r12)
	sete	%cl
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	jmp	.LBB3_375
.LBB3_338:                              #   in Loop: Header=BB3_989 Depth=2
	movl	%r8d, %r13d
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movzbl	%bl, %ebp
	movzwl	(%rax,%rbp,2), %ecx
	andl	$8192, %ecx             # imm = 0x2000
	cmpb	$62, %bl
	je	.LBB3_475
# BB#339:                               #   in Loop: Header=BB3_989 Depth=2
	testw	%cx, %cx
	jne	.LBB3_475
# BB#340:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$44, %bl
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%r13d, %r8d
	jne	.LBB3_610
# BB#341:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	48(%rsp), %rax          # 4-byte Folded Reload
	movb	$0, 512(%rsp,%rax)
	incq	%r12
	movl	$19, %eax
	xorl	%r15d, %r15d
	jmp	.LBB3_337
.LBB3_342:                              #   in Loop: Header=BB3_989 Depth=2
	movslq	%edx, %rax
	leaq	4(%rbp,%rax), %rdi
	movslq	%ebx, %rdx
	leaq	512(%rsp), %rsi
	callq	memcpy
	movl	8196(%rbp), %eax
	addl	%ebx, %eax
	movl	%eax, 8196(%rbp)
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_344
.LBB3_343:                              # %html_output_flush.exit.i1690
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	%r14, %rsi
	callq	cli_writen
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_344:                              # %html_output_c.exit1691
                                        #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	$10, 4(%rbp,%rax)
	movl	$.L.str.50, %esi
	leaq	512(%rsp), %rdi
	callq	strstr
	testq	%rax, %rax
	movl	8196(%rbp), %edx
	je	.LBB3_349
# BB#345:                               #   in Loop: Header=BB3_989 Depth=2
	testl	%edx, %edx
	jle	.LBB3_348
# BB#346:                               #   in Loop: Header=BB3_989 Depth=2
	leal	34(%rdx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jl	.LBB3_348
# BB#347:                               #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movq	%r14, %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_writen
	movl	$0, 8196(%rbp)
	xorl	%edx, %edx
.LBB3_348:                              # %html_output_str.exit1694
                                        #   in Loop: Header=BB3_989 Depth=2
	movslq	%edx, %rax
	movups	.L.str.51+16(%rip), %xmm0
	movups	%xmm0, 20(%rbp,%rax)
	movups	.L.str.51(%rip), %xmm0
	movups	%xmm0, 4(%rbp,%rax)
	movw	$2612, 36(%rbp,%rax)    # imm = 0xA34
	movl	8196(%rbp), %edx
	addl	$34, %edx
	movl	%edx, 8196(%rbp)
.LBB3_349:                              # %html_output_c.exit1691._crit_edge
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpl	$8192, %edx             # imm = 0x2000
	jne	.LBB3_351
# BB#350:                               # %html_output_flush.exit.i1695
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	%r14, %rsi
	callq	cli_writen
	movl	$0, 8196(%rbp)
	xorl	%edx, %edx
.LBB3_351:                              # %html_output_c.exit1696
                                        #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rdx), %eax
	movl	%eax, 8196(%rbp)
	movslq	%edx, %rax
	movb	$10, 4(%rbp,%rax)
	movq	%rbp, %rax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movq	%rbp, 168(%rsp)         # 8-byte Spill
	movl	%r15d, %eax
	movl	$20, %r15d
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	$1, %r8d
	movq	%rax, %r13
	jmp	.LBB3_998
.LBB3_352:                              #   in Loop: Header=BB3_989 Depth=2
	movl	$6, %r15d
.LBB3_353:                              # %.critedge.backedge
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%r13d, %r8d
	movl	$1, %r13d
	jmp	.LBB3_998
.LBB3_354:                              #   in Loop: Header=BB3_989 Depth=2
	movl	%r8d, %r14d
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movzbl	%bl, %ebx
	testb	$32, 1(%rax,%rbx,2)
	jne	.LBB3_522
# BB#355:                               #   in Loop: Header=BB3_989 Depth=2
	callq	__ctype_tolower_loc
	movq	%rax, %r13
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	movq	(%r13), %rax
	movb	(%rax,%rbx,4), %bl
	movq	(%rsp), %rdi            # 8-byte Reload
	je	.LBB3_359
# BB#356:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_358
# BB#357:                               # %html_output_flush.exit.i1470
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_358:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	%bl, 4(%rbp,%rax)
.LBB3_359:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%r14d, %r8d
	je	.LBB3_363
# BB#360:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_362
# BB#361:                               # %html_output_flush.exit11.i1471
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	%r14d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_362:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	%bl, 4(%rdi,%rax)
.LBB3_363:                              # %html_output_c.exit1472
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpl	$0, 92(%rsp)            # 4-byte Folded Reload
	je	.LBB3_368
# BB#364:                               #   in Loop: Header=BB3_989 Depth=2
	testq	%rsi, %rsi
	je	.LBB3_368
# BB#365:                               #   in Loop: Header=BB3_989 Depth=2
	movq	(%r13), %rax
	movzbl	(%r12), %ecx
	movb	(%rax,%rcx,4), %bl
	movl	8196(%rsi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_367
# BB#366:                               # %html_output_flush.exit.i1473
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rsi), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	104(%rsp), %rsi         # 8-byte Reload
	callq	cli_writen
	movl	%r14d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
	xorl	%eax, %eax
.LBB3_367:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rsi)
	cltq
	movb	%bl, 4(%rsi,%rax)
.LBB3_368:                              # %html_output_c.exit1474
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	188(%rsp), %edx         # 4-byte Reload
	cmpl	$1023, %edx             # imm = 0x3FF
	jg	.LBB3_370
# BB#369:                               #   in Loop: Header=BB3_989 Depth=2
	movq	(%r13), %rax
	movzbl	(%r12), %ecx
	movb	(%rax,%rcx,4), %al
	movslq	%edx, %rcx
	incl	%edx
	movl	%edx, 188(%rsp)         # 4-byte Spill
	movb	%al, 1552(%rsp,%rcx)
.LBB3_370:                              #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	movl	%r15d, %r13d
	movl	$7, %r15d
	jmp	.LBB3_998
.LBB3_371:                              #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	movslq	%ebp, %rax
	movb	$0, 3632(%rsp,%rax)
	movl	$5, %r15d
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%r13d, %r8d
	movl	$10, %r13d
	jmp	.LBB3_998
.LBB3_372:                              #   in Loop: Header=BB3_989 Depth=2
	leal	-48(%rbp,%rbx), %ebp
.LBB3_373:                              #   in Loop: Header=BB3_989 Depth=2
	movq	%rbp, 136(%rsp)         # 8-byte Spill
	movl	$23, %eax
.LBB3_374:                              #   in Loop: Header=BB3_989 Depth=2
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%r13d, %r8d
	cmpq	$2, 144(%rsp)           # 8-byte Folded Reload
	cmovel	%r15d, %eax
	incq	%r12
.LBB3_375:                              # %.critedge.backedge
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	%r15d, %r13d
	movl	%eax, %r15d
	jmp	.LBB3_998
.LBB3_376:                              #   in Loop: Header=BB3_989 Depth=2
	movl	%r8d, 44(%rsp)          # 4-byte Spill
	callq	__ctype_b_loc
	cmpb	$62, %bl
	je	.LBB3_575
# BB#377:                               #   in Loop: Header=BB3_989 Depth=2
	movq	(%rax), %rax
	movzbl	%bl, %ecx
	movzwl	(%rax,%rcx,2), %eax
	andl	$8192, %eax             # imm = 0x2000
	testw	%ax, %ax
	jne	.LBB3_575
# BB#378:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$20, %ebp
	movq	168(%rsp), %r13         # 8-byte Reload
	testq	%r13, %r13
	je	.LBB3_576
# BB#379:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%r13), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_381
# BB#380:                               # %html_output_flush.exit.i1703
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%r13), %edi
	leaq	4(%r13), %rsi
	movl	$8192, %edx             # imm = 0x2000
	callq	cli_writen
	movl	$0, 8196(%r13)
	xorl	%eax, %eax
.LBB3_381:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%r13)
	cltq
	movb	%bl, 4(%r13,%rax)
	jmp	.LBB3_576
.LBB3_382:                              #   in Loop: Header=BB3_989 Depth=2
	xorl	%eax, %eax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	$23, %ebp
	movl	$22, %r13d
	xorl	%eax, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	jmp	.LBB3_820
.LBB3_383:                              #   in Loop: Header=BB3_989 Depth=2
	movq	152(%rsp), %rax         # 8-byte Reload
	orl	60(%rsp), %eax          # 4-byte Folded Reload
	je	.LBB3_633
# BB#384:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$20, %ebp
	movq	168(%rsp), %rbx         # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB3_819
# BB#385:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_387
# BB#386:                               # %html_output_flush.exit.i1697
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbx), %edi
	leaq	4(%rbx), %rsi
	movl	$8192, %edx             # imm = 0x2000
	movl	%r8d, %r13d
	callq	cli_writen
	movl	%r13d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbx)
	xorl	%eax, %eax
.LBB3_387:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbx)
	cltq
	movb	$39, 4(%rbx,%rax)
	jmp	.LBB3_819
.LBB3_388:                              #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB3_392
# BB#389:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_391
# BB#390:                               # %html_output_flush.exit.i1670
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbx), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %r14d
	callq	cli_writen
	movl	%r14d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbx)
	xorl	%eax, %eax
.LBB3_391:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbx)
	cltq
	movb	%bpl, 4(%rbx,%rax)
.LBB3_392:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_396
# BB#393:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_395
# BB#394:                               # %html_output_flush.exit11.i1671
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebx
	callq	cli_writen
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_395:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	%bpl, 4(%rdi,%rax)
.LBB3_396:                              # %html_output_c.exit1672
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	%ebp, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	movl	%ebp, %ebx
	ja	.LBB3_398
# BB#397:                               #   in Loop: Header=BB3_989 Depth=2
	movl	%r8d, %ebx
	callq	__ctype_tolower_loc
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rax), %rax
	movl	(%rax,%rbp,4), %ebx
.LBB3_398:                              # %tolower.exit1674
                                        #   in Loop: Header=BB3_989 Depth=2
	testq	%rsi, %rsi
	movq	%rbp, 136(%rsp)         # 8-byte Spill
	je	.LBB3_402
# BB#399:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rsi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_401
# BB#400:                               # %html_output_flush.exit.i1675
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rsi), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	104(%rsp), %rsi         # 8-byte Reload
	movl	%r8d, %ebp
	callq	cli_writen
	movl	%ebp, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
	xorl	%eax, %eax
.LBB3_401:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rsi)
	cltq
	movb	%bl, 4(%rsi,%rax)
.LBB3_402:                              # %html_output_c.exit1649
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	%r12, %rbx
.LBB3_403:                              # %html_output_c.exit1649
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	280(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rdx
	leal	1(%rdx), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$26, %ecx
	leal	1(%rdx,%rcx), %ecx
	andl	$-64, %ecx
	subl	%ecx, %eax
	incq	%rbx
	decq	144(%rsp)               # 8-byte Folded Spill
	movq	%rbx, %r12
	movl	%r15d, %r13d
	movl	$16, %r15d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 280(%rsp)         # 8-byte Spill
	jmp	.LBB3_998
.LBB3_404:                              #   in Loop: Header=BB3_989 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	jmp	.LBB3_818
.LBB3_405:                              #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	48(%rsp), %rbx          # 8-byte Reload
	je	.LBB3_409
# BB#406:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_408
# BB#407:                               # %html_output_flush.exit.i1605
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_408:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	$38, 4(%rbp,%rax)
.LBB3_409:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_413
# BB#410:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_412
# BB#411:                               # %html_output_flush.exit11.i1606
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_412:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	$38, 4(%rdi,%rax)
.LBB3_413:                              # %html_output_c.exit1607
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpl	$9, %r15d
	jne	.LBB3_416
# BB#414:                               # %html_output_c.exit1607
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpl	$1023, %ebx             # imm = 0x3FF
	jg	.LBB3_416
# BB#415:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	%ebx, %rax
	incl	%ebx
	movb	$38, 512(%rsp,%rax)
.LBB3_416:                              # %.preheader1768
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpq	$0, 120(%rsp)           # 8-byte Folded Reload
	je	.LBB3_645
# BB#417:                               # %.lr.ph1902
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	%rbx, %r14
	callq	__ctype_tolower_loc
	movq	%rax, 80(%rsp)          # 8-byte Spill
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB3_708
# BB#418:                               # %.lr.ph1902.split.preheader
                                        #   in Loop: Header=BB3_989 Depth=2
	leaq	2592(%rsp), %rbp
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	44(%rsp), %r8d          # 4-byte Reload
	.p2align	4, 0x90
.LBB3_419:                              # %.lr.ph1902.split
                                        #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movzbl	(%rbp), %ecx
	movzbl	(%rax,%rcx,4), %ebx
	movq	16(%rsp), %r13          # 8-byte Reload
	movl	8196(%r13), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_421
# BB#420:                               # %html_output_flush.exit.i1608
                                        #   in Loop: Header=BB3_419 Depth=3
	movl	(%r13), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	44(%rsp), %r8d          # 4-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%r13)
	xorl	%eax, %eax
.LBB3_421:                              #   in Loop: Header=BB3_419 Depth=3
	leal	1(%rax), %ecx
	testq	%rdi, %rdi
	movl	%ecx, 8196(%r13)
	cltq
	movb	%bl, 4(%r13,%rax)
	je	.LBB3_425
# BB#422:                               #   in Loop: Header=BB3_419 Depth=3
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_424
# BB#423:                               # %html_output_flush.exit11.i1609
                                        #   in Loop: Header=BB3_419 Depth=3
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	44(%rsp), %r8d          # 4-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_424:                              #   in Loop: Header=BB3_419 Depth=3
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	%bl, 4(%rdi,%rax)
.LBB3_425:                              # %html_output_c.exit1610
                                        #   in Loop: Header=BB3_419 Depth=3
	cmpl	$9, %r15d
	jne	.LBB3_428
# BB#426:                               # %html_output_c.exit1610
                                        #   in Loop: Header=BB3_419 Depth=3
	movq	%r14, %rcx
	cmpl	$1023, %ecx             # imm = 0x3FF
	jg	.LBB3_428
# BB#427:                               #   in Loop: Header=BB3_419 Depth=3
	movslq	%ecx, %rax
	incl	%ecx
	movq	%rcx, %r14
	movb	%bl, 512(%rsp,%rax)
.LBB3_428:                              #   in Loop: Header=BB3_419 Depth=3
	incq	%rbp
	decq	120(%rsp)               # 8-byte Folded Spill
	jne	.LBB3_419
	jmp	.LBB3_717
.LBB3_430:                              #   in Loop: Header=BB3_989 Depth=2
	xorl	%eax, %eax
	movq	%rax, 168(%rsp)         # 8-byte Spill
.LBB3_431:                              # %.critedge.backedge
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	$2, %eax
	jmp	.LBB3_579
.LBB3_432:                              #   in Loop: Header=BB3_989 Depth=2
	xorl	%r13d, %r13d
	movl	$0, 88(%rsp)            # 4-byte Folded Spill
	jmp	.LBB3_998
.LBB3_433:                              #   in Loop: Header=BB3_989 Depth=2
	movslq	224(%rsp), %rcx
	movq	%rcx, %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	testq	%rcx, %rcx
	movq	232(%rsp), %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	movq	240(%rsp), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	$1, %r13d
	jle	.LBB3_646
# BB#434:                               # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB3_989 Depth=2
	xorl	%ebx, %ebx
	leaq	1552(%rsp), %r15
	.p2align	4, 0x90
.LBB3_435:                              # %.lr.ph.i
                                        #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	256(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbx,8), %rdi
	movl	$.L.str.22, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_665
# BB#436:                               #   in Loop: Header=BB3_435 Depth=3
	incq	%rbx
	cmpq	192(%rsp), %rbx         # 8-byte Folded Reload
	jl	.LBB3_435
# BB#437:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$1, %eax
	jmp	.LBB3_885
.LBB3_438:                              #   in Loop: Header=BB3_989 Depth=2
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	je	.LBB3_647
# BB#439:                               #   in Loop: Header=BB3_989 Depth=2
	cmpl	$0, 152(%rsp)           # 4-byte Folded Reload
	sete	%r14b
	cmpl	$1, 60(%rsp)            # 4-byte Folded Reload
	sete	%bl
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB3_443
# BB#440:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_442
# BB#441:                               # %html_output_flush.exit.i1505
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, 44(%rsp)          # 4-byte Spill
	callq	cli_writen
	movl	44(%rsp), %r8d          # 4-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_442:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	$34, 4(%rbp,%rax)
.LBB3_443:                              #   in Loop: Header=BB3_989 Depth=2
	andb	%r14b, %bl
	testq	%rdi, %rdi
	movq	48(%rsp), %rdx          # 8-byte Reload
	je	.LBB3_447
# BB#444:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_446
# BB#445:                               # %html_output_flush.exit11.i1506
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebp
	callq	cli_writen
	movl	%ebp, %r8d
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_446:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	$34, 4(%rdi,%rax)
.LBB3_447:                              # %html_output_c.exit1507
                                        #   in Loop: Header=BB3_989 Depth=2
	testb	%bl, %bl
	je	.LBB3_676
# BB#448:                               #   in Loop: Header=BB3_989 Depth=2
	movl	%r8d, %ebp
	cmpl	$1023, %edx             # imm = 0x3FF
	jg	.LBB3_450
# BB#449:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	%edx, %rax
	incl	%edx
	movb	$34, 512(%rsp,%rax)
.LBB3_450:                              #   in Loop: Header=BB3_989 Depth=2
	movq	%rdx, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movslq	%edx, %rax
	movb	$0, 512(%rsp,%rax)
	leaq	224(%rsp), %rdi
	leaq	3632(%rsp), %rsi
	leaq	512(%rsp), %rdx
	callq	html_tag_arg_add
	incq	%r12
	movl	$0, 88(%rsp)            # 4-byte Folded Spill
	movl	$5, %ebx
	movl	$8, %r15d
	movl	$1, 60(%rsp)            # 4-byte Folded Spill
	jmp	.LBB3_732
.LBB3_451:                              #   in Loop: Header=BB3_989 Depth=2
	testl	%ebp, %ebp
	movq	(%rsp), %rdi            # 8-byte Reload
	je	.LBB3_656
# BB#452:                               #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB3_456
# BB#453:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_455
# BB#454:                               # %html_output_flush.exit.i1499
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbx), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %r14d
	callq	cli_writen
	movl	%r14d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbx)
	xorl	%eax, %eax
.LBB3_455:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbx)
	cltq
	movb	$34, 4(%rbx,%rax)
.LBB3_456:                              #   in Loop: Header=BB3_989 Depth=2
	movq	152(%rsp), %rax         # 8-byte Reload
	orl	60(%rsp), %eax          # 4-byte Folded Reload
	movq	%rax, 152(%rsp)         # 8-byte Spill
	testq	%rdi, %rdi
	je	.LBB3_460
# BB#457:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_459
# BB#458:                               # %html_output_flush.exit11.i1500
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebx
	callq	cli_writen
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_459:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	$34, 4(%rdi,%rax)
.LBB3_460:                              # %html_output_c.exit1501
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpl	$0, 152(%rsp)           # 4-byte Folded Reload
	je	.LBB3_729
# BB#461:                               #   in Loop: Header=BB3_989 Depth=2
	cmpl	$1023, %ebp             # imm = 0x3FF
	jg	.LBB3_679
# BB#462:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	%ebp, %rax
	incl	%ebp
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	jmp	.LBB3_678
.LBB3_463:                              #   in Loop: Header=BB3_989 Depth=2
	movl	%r8d, %r13d
	callq	__ctype_b_loc
	movq	%rax, %r14
	cmpb	$62, %bl
	je	.LBB3_581
# BB#464:                               #   in Loop: Header=BB3_989 Depth=2
	movq	(%r14), %rax
	movzbl	%bl, %ebp
	movzwl	(%rax,%rbp,2), %eax
	andl	$8192, %eax             # imm = 0x2000
	testw	%ax, %ax
	jne	.LBB3_581
# BB#465:                               #   in Loop: Header=BB3_989 Depth=2
	callq	__ctype_tolower_loc
	movq	16(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	movq	(%rax), %rax
	movb	(%rax,%rbp,4), %r14b
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%r13d, %r8d
	movq	48(%rsp), %rbp          # 8-byte Reload
	je	.LBB3_469
# BB#466:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_468
# BB#467:                               # %html_output_flush.exit.i1511
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbx), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	%r13d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbx)
	xorl	%eax, %eax
.LBB3_468:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbx)
	cltq
	movb	%r14b, 4(%rbx,%rax)
.LBB3_469:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_473
# BB#470:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_472
# BB#471:                               # %html_output_flush.exit11.i1512
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	%r13d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_472:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	%r14b, 4(%rdi,%rax)
.LBB3_473:                              # %html_output_c.exit1513
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpl	$1023, %ebp             # imm = 0x3FF
	jg	.LBB3_679
# BB#474:                               #   in Loop: Header=BB3_989 Depth=2
	movb	(%r12), %al
	movslq	%ebp, %rcx
	incl	%ebp
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movb	%al, 512(%rsp,%rcx)
	jmp	.LBB3_679
.LBB3_475:                              #   in Loop: Header=BB3_989 Depth=2
	movl	$5, %eax
	cmpl	$2, 60(%rsp)            # 4-byte Folded Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%r13d, %r8d
	jne	.LBB3_612
# BB#476:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$0, 88(%rsp)            # 4-byte Folded Spill
	movl	$8, %r15d
	movl	$2, 60(%rsp)            # 4-byte Folded Spill
	jmp	.LBB3_337
.LBB3_477:                              #   in Loop: Header=BB3_989 Depth=2
	addl	$-48, %ebx
	movl	%ebx, %eax
.LBB3_478:                              #   in Loop: Header=BB3_989 Depth=2
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%r13d, %r8d
	addl	%ebp, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	incq	%r12
.LBB3_479:                              # %.critedge.backedge
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	%r15d, %r13d
	movl	$12, %r15d
	jmp	.LBB3_998
.LBB3_480:                              #   in Loop: Header=BB3_989 Depth=2
	callq	__ctype_tolower_loc
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%rax, %rbx
	testq	%r14, %r14
	movq	(%rbx), %rax
	movb	(%rax,%rbp,4), %bpl
	movq	(%rsp), %rdi            # 8-byte Reload
	je	.LBB3_484
# BB#481:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%r14), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_483
# BB#482:                               # %html_output_flush.exit.i1456
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%r14), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	$0, 8196(%r14)
	xorl	%eax, %eax
.LBB3_483:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%r14)
	cltq
	movb	%bpl, 4(%r14,%rax)
.LBB3_484:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%r13d, %r8d
	je	.LBB3_488
# BB#485:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_487
# BB#486:                               # %html_output_flush.exit11.i1457
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	%r13d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_487:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	%bpl, 4(%rdi,%rax)
.LBB3_488:                              # %html_output_c.exit1458
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpl	$0, 92(%rsp)            # 4-byte Folded Reload
	je	.LBB3_493
# BB#489:                               #   in Loop: Header=BB3_989 Depth=2
	testq	%rsi, %rsi
	je	.LBB3_493
# BB#490:                               #   in Loop: Header=BB3_989 Depth=2
	movq	(%rbx), %rax
	movzbl	(%r12), %ecx
	movb	(%rax,%rcx,4), %bl
	movl	8196(%rsi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_492
# BB#491:                               # %html_output_flush.exit.i1459
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rsi), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	104(%rsp), %rsi         # 8-byte Reload
	callq	cli_writen
	movl	%r13d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
	xorl	%eax, %eax
.LBB3_492:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rsi)
	cltq
	movb	%bl, 4(%rsi,%rax)
.LBB3_493:                              # %html_output_c.exit1460
                                        #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	movl	%r15d, %r13d
	movl	$1, %r15d
	jmp	.LBB3_998
.LBB3_494:                              #   in Loop: Header=BB3_989 Depth=2
	testl	%ebp, %ebp
	jne	.LBB3_503
# BB#495:                               #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB3_499
# BB#496:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_498
# BB#497:                               # %html_output_flush.exit.i1481
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbx), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	$0, 8196(%rbx)
	xorl	%eax, %eax
.LBB3_498:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbx)
	cltq
	movb	$32, 4(%rbx,%rax)
.LBB3_499:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_503
# BB#500:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_502
# BB#501:                               # %html_output_flush.exit11.i1482
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_502:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	$32, 4(%rdi,%rax)
.LBB3_503:                              # %html_output_c.exit1483
                                        #   in Loop: Header=BB3_989 Depth=2
	callq	__ctype_tolower_loc
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rax, %rbp
	movq	16(%rsp), %r14          # 8-byte Reload
	testq	%r14, %r14
	movq	(%rbp), %rax
	movzbl	(%r12), %ecx
	movb	(%rax,%rcx,4), %bl
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%r13d, %r8d
	je	.LBB3_507
# BB#504:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%r14), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_506
# BB#505:                               # %html_output_flush.exit.i1484
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%r14), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	%r13d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%r14)
	xorl	%eax, %eax
.LBB3_506:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%r14)
	cltq
	movb	%bl, 4(%r14,%rax)
.LBB3_507:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_511
# BB#508:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_510
# BB#509:                               # %html_output_flush.exit11.i1485
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	%r13d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_510:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	%bl, 4(%rdi,%rax)
.LBB3_511:                              # %html_output_c.exit1486
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	88(%rsp), %edx          # 4-byte Reload
	cmpl	$1023, %edx             # imm = 0x3FF
	jg	.LBB3_513
# BB#512:                               #   in Loop: Header=BB3_989 Depth=2
	movq	(%rbp), %rax
	movzbl	(%r12), %ecx
	movb	(%rax,%rcx,4), %al
	movslq	%edx, %rcx
	incl	%edx
	movl	%edx, 88(%rsp)          # 4-byte Spill
	movb	%al, 3632(%rsp,%rcx)
.LBB3_513:                              #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	movl	%r15d, %r13d
	movl	$8, %r15d
	jmp	.LBB3_998
.LBB3_514:                              #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	je	.LBB3_517
# BB#515:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rcx), %eax
	testl	%eax, %eax
	jle	.LBB3_517
# BB#516:                               #   in Loop: Header=BB3_989 Depth=2
	decl	%eax
	movl	%eax, 8196(%rcx)
.LBB3_517:                              #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB3_520
# BB#518:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB3_520
# BB#519:                               #   in Loop: Header=BB3_989 Depth=2
	decl	%eax
	movl	%eax, 8196(%rdi)
.LBB3_520:                              #   in Loop: Header=BB3_989 Depth=2
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	$18, %ebx
	movl	$8, %r15d
	cmpl	$2, 60(%rsp)            # 4-byte Folded Reload
	jne	.LBB3_523
# BB#521:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$2, 60(%rsp)            # 4-byte Folded Spill
	jmp	.LBB3_733
.LBB3_522:                              #   in Loop: Header=BB3_989 Depth=2
	movslq	188(%rsp), %rax         # 4-byte Folded Reload
	movb	$0, 1552(%rsp,%rax)
	movl	$0, 88(%rsp)            # 4-byte Folded Spill
	movl	$5, %r15d
	movl	$8, %r13d
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%r14d, %r8d
	jmp	.LBB3_998
.LBB3_523:                              #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %r14          # 8-byte Reload
	testq	%r14, %r14
	je	.LBB3_527
# BB#524:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%r14), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_526
# BB#525:                               # %html_output_flush.exit.i1490
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%r14), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	%ebp, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%r14)
	xorl	%eax, %eax
.LBB3_526:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%r14)
	cltq
	movb	$34, 4(%r14,%rax)
.LBB3_527:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_733
# BB#528:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_530
# BB#529:                               # %html_output_flush.exit11.i1491
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	%ebp, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_530:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	$34, 4(%rdi,%rax)
	jmp	.LBB3_733
.LBB3_531:                              #   in Loop: Header=BB3_989 Depth=2
	movq	80(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB3_644
.LBB3_532:                              #   in Loop: Header=BB3_989 Depth=2
	leaq	318(%rsp), %rbp
.LBB3_533:                              #   in Loop: Header=BB3_989 Depth=2
	cmpb	$0, 116(%rsp)           # 1-byte Folded Reload
	je	.LBB3_545
.LBB3_534:                              #   in Loop: Header=BB3_989 Depth=2
	movq	136(%rsp), %rcx         # 8-byte Reload
	cmpl	$127, %ecx
	jg	.LBB3_563
# BB#535:                               #   in Loop: Header=BB3_989 Depth=2
	movl	%ecx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	movl	%ecx, %ebx
	movq	%rcx, %rbp
	ja	.LBB3_537
# BB#536:                               #   in Loop: Header=BB3_989 Depth=2
	movl	%r8d, %ebx
	callq	__ctype_tolower_loc
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rax), %rax
	movslq	%ebp, %rcx
	movl	(%rax,%rcx,4), %ebx
.LBB3_537:                              # %tolower.exit
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB3_541
# BB#538:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_540
# BB#539:                               # %html_output_flush.exit.i1620
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %r14d
	callq	cli_writen
	movl	%r14d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_540:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	%bl, 4(%rbp,%rax)
.LBB3_541:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_749
# BB#542:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_544
# BB#543:                               # %html_output_flush.exit11.i1621
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebp
	callq	cli_writen
	movl	%ebp, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_544:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	%bl, 4(%rdi,%rax)
	jmp	.LBB3_749
.LBB3_545:                              #   in Loop: Header=BB3_989 Depth=2
	movzbl	136(%rsp), %ebx         # 1-byte Folded Reload
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB3_547
# BB#546:                               #   in Loop: Header=BB3_989 Depth=2
	movl	%r8d, %ebp
	callq	__ctype_tolower_loc
	movl	%ebp, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rax), %rax
	movl	(%rax,%rbx,4), %ebx
.LBB3_547:                              # %tolower.exit1630
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB3_551
# BB#548:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_550
# BB#549:                               # %html_output_flush.exit.i1631
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %r14d
	callq	cli_writen
	movl	%r14d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_550:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	%bl, 4(%rbp,%rax)
.LBB3_551:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_749
# BB#552:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_554
# BB#553:                               # %html_output_flush.exit11.i1632
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebp
	callq	cli_writen
	movl	%ebp, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_554:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	%bl, 4(%rdi,%rax)
	jmp	.LBB3_749
.LBB3_555:                              #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%r13d, %r8d
	je	.LBB3_559
# BB#556:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_558
# BB#557:                               # %html_output_flush.exit.i1634
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbx), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	%r13d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbx)
	xorl	%eax, %eax
.LBB3_558:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbx)
	cltq
	movb	%bpl, 4(%rbx,%rax)
.LBB3_559:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_736
# BB#560:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_562
# BB#561:                               # %html_output_flush.exit11.i1635
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	%r13d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_562:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	%bpl, 4(%rdi,%rax)
	xorl	%r13d, %r13d
	jmp	.LBB3_998
.LBB3_563:                              #   in Loop: Header=BB3_989 Depth=2
	movl	%r8d, 44(%rsp)          # 4-byte Spill
	movl	$9, %esi
	movl	$.L.str.44, %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	snprintf
	movb	$0, 327(%rsp)
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	16(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB3_739
# BB#564:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rax), %edx
	testl	%edx, %edx
	jle	.LBB3_567
# BB#565:                               #   in Loop: Header=BB3_989 Depth=2
	leal	(%rdx,%rbx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jl	.LBB3_567
# BB#566:                               #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, %r14
	movl	(%r14), %edi
	movq	72(%rsp), %rsi          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_writen
	movl	$0, 8196(%r14)
	xorl	%edx, %edx
.LBB3_567:                              # %html_output_flush.exit.i1624
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpl	$8192, %ebx             # imm = 0x2000
	jl	.LBB3_738
# BB#568:                               #   in Loop: Header=BB3_989 Depth=2
	testl	%edx, %edx
	movq	16(%rsp), %r14          # 8-byte Reload
	jle	.LBB3_570
# BB#569:                               #   in Loop: Header=BB3_989 Depth=2
	movl	(%r14), %edi
	movq	72(%rsp), %rsi          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_writen
	movl	$0, 8196(%r14)
.LBB3_570:                              # %html_output_flush.exit13.i
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%r14), %edi
	movq	%rbp, %rsi
	movl	%ebx, %edx
	callq	cli_writen
	jmp	.LBB3_739
.LBB3_575:                              #   in Loop: Header=BB3_989 Depth=2
	movl	$21, %ebp
	cmpl	$2, 60(%rsp)            # 4-byte Folded Reload
	jne	.LBB3_690
.LBB3_576:                              # %html_output_c.exit1698
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	%r15d, %r13d
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	44(%rsp), %r8d          # 4-byte Reload
	jmp	.LBB3_820
.LBB3_577:                              #   in Loop: Header=BB3_989 Depth=2
	xorl	%eax, %eax
	movq	%rax, 168(%rsp)         # 8-byte Spill
.LBB3_578:                              # %.critedge.backedge
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	$1, %eax
.LBB3_579:                              #   in Loop: Header=BB3_989 Depth=2
	movq	%rax, 144(%rsp)         # 8-byte Spill
.LBB3_580:                              #   in Loop: Header=BB3_989 Depth=2
	movl	%r15d, %r13d
	movl	$20, %r15d
	jmp	.LBB3_998
.LBB3_581:                              #   in Loop: Header=BB3_989 Depth=2
	cmpl	$2, 60(%rsp)            # 4-byte Folded Reload
	jne	.LBB3_695
# BB#582:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	48(%rsp), %rax          # 4-byte Folded Reload
	movb	$0, 512(%rsp,%rax)
	leaq	224(%rsp), %rdi
	leaq	3632(%rsp), %rsi
	leaq	512(%rsp), %rdx
	callq	html_tag_arg_add
	movl	$0, 88(%rsp)            # 4-byte Folded Spill
	movl	$5, %ebx
	movl	$8, %r15d
	movl	$2, 60(%rsp)            # 4-byte Folded Spill
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%r13d, %r8d
	jmp	.LBB3_733
.LBB3_583:                              #   in Loop: Header=BB3_989 Depth=2
	cmpq	$1023, 120(%rsp)        # 8-byte Folded Reload
                                        # imm = 0x3FF
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	44(%rsp), %r8d          # 4-byte Reload
	ja	.LBB3_585
# BB#584:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$45, %bl
	je	.LBB3_248
.LBB3_585:                              #   in Loop: Header=BB3_989 Depth=2
	cmpl	$9, %r15d
	movq	48(%rsp), %rbx          # 8-byte Reload
	jne	.LBB3_588
# BB#586:                               #   in Loop: Header=BB3_989 Depth=2
	cmpl	$1023, %ebx             # imm = 0x3FF
	jg	.LBB3_588
# BB#587:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	%ebx, %rax
	incl	%ebx
	movb	$38, 512(%rsp,%rax)
.LBB3_588:                              #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB3_592
# BB#589:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_591
# BB#590:                               # %html_output_flush.exit.i1614
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_591:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	$38, 4(%rbp,%rax)
.LBB3_592:                              #   in Loop: Header=BB3_989 Depth=2
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	testq	%rdi, %rdi
	movq	%rbp, %rbx
	je	.LBB3_596
# BB#593:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_595
# BB#594:                               # %html_output_flush.exit11.i1615
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_595:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	$38, 4(%rdi,%rax)
.LBB3_596:                              # %html_output_c.exit1616.preheader
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpq	$0, 120(%rsp)           # 8-byte Folded Reload
	je	.LBB3_609
# BB#597:                               # %.lr.ph1893
                                        #   in Loop: Header=BB3_989 Depth=2
	callq	__ctype_tolower_loc
	movq	%rbx, %rcx
	movq	%rax, %r14
	leaq	2592(%rsp), %rbp
	testq	%rcx, %rcx
	je	.LBB3_680
# BB#598:                               # %.lr.ph1893.split.preheader
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	44(%rsp), %r8d          # 4-byte Reload
	.p2align	4, 0x90
.LBB3_599:                              # %.lr.ph1893.split
                                        #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r14), %rax
	movzbl	(%rbp), %ecx
	movzbl	(%rax,%rcx,4), %r13d
	movq	16(%rsp), %rbx          # 8-byte Reload
	movl	8196(%rbx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_601
# BB#600:                               # %html_output_flush.exit.i1617
                                        #   in Loop: Header=BB3_599 Depth=3
	movl	(%rbx), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	44(%rsp), %r8d          # 4-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbx)
	xorl	%eax, %eax
.LBB3_601:                              #   in Loop: Header=BB3_599 Depth=3
	leal	1(%rax), %ecx
	testq	%rdi, %rdi
	movl	%ecx, 8196(%rbx)
	cltq
	movb	%r13b, 4(%rbx,%rax)
	je	.LBB3_605
# BB#602:                               #   in Loop: Header=BB3_599 Depth=3
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_604
# BB#603:                               # %html_output_flush.exit11.i1618
                                        #   in Loop: Header=BB3_599 Depth=3
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	44(%rsp), %r8d          # 4-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_604:                              #   in Loop: Header=BB3_599 Depth=3
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	%r13b, 4(%rdi,%rax)
.LBB3_605:                              # %html_output_c.exit1619
                                        #   in Loop: Header=BB3_599 Depth=3
	cmpl	$9, %r15d
	jne	.LBB3_608
# BB#606:                               # %html_output_c.exit1619
                                        #   in Loop: Header=BB3_599 Depth=3
	movq	48(%rsp), %rcx          # 8-byte Reload
	cmpl	$1023, %ecx             # imm = 0x3FF
	jg	.LBB3_608
# BB#607:                               #   in Loop: Header=BB3_599 Depth=3
	movslq	%ecx, %rax
	incl	%ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movb	%r13b, 512(%rsp,%rax)
.LBB3_608:                              # %html_output_c.exit1616
                                        #   in Loop: Header=BB3_599 Depth=3
	incq	%rbp
	xorl	%r13d, %r13d
	decq	120(%rsp)               # 8-byte Folded Spill
	jne	.LBB3_599
	jmp	.LBB3_689
.LBB3_609:                              #   in Loop: Header=BB3_989 Depth=2
	xorl	%eax, %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	xorl	%r13d, %r13d
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	44(%rsp), %r8d          # 4-byte Reload
	jmp	.LBB3_998
.LBB3_610:                              #   in Loop: Header=BB3_989 Depth=2
	movq	48(%rsp), %rbx          # 8-byte Reload
	cmpl	$1023, %ebx             # imm = 0x3FF
	jg	.LBB3_336
# BB#611:                               #   in Loop: Header=BB3_989 Depth=2
	callq	__ctype_tolower_loc
	movl	%r13d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rax), %rax
	movb	(%rax,%rbp,4), %al
	movslq	%ebx, %rcx
	incl	%ebx
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movb	%al, 512(%rsp,%rcx)
	jmp	.LBB3_336
.LBB3_612:                              #   in Loop: Header=BB3_989 Depth=2
	movq	48(%rsp), %rbp          # 8-byte Reload
	cmpl	$1023, %ebp             # imm = 0x3FF
	jg	.LBB3_616
# BB#613:                               #   in Loop: Header=BB3_989 Depth=2
	testw	%cx, %cx
	movslq	%ebp, %rcx
	movb	$32, %dl
	jne	.LBB3_615
# BB#614:                               #   in Loop: Header=BB3_989 Depth=2
	movb	$62, %dl
.LBB3_615:                              #   in Loop: Header=BB3_989 Depth=2
	incl	%ebp
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movb	%dl, 512(%rsp,%rcx)
.LBB3_616:                              #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	movl	$18, %r15d
	movl	$2, 60(%rsp)            # 4-byte Folded Spill
	jmp	.LBB3_337
.LBB3_617:                              # %.thread2061
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.37, %esi
	leaq	1552(%rsp), %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_756
.LBB3_618:                              #   in Loop: Header=BB3_989 Depth=2
	cmpb	$105, %bl
	jne	.LBB3_750
# BB#619:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$109, 1553(%rsp)
	jne	.LBB3_750
# BB#620:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$103, 1554(%rsp)
	jne	.LBB3_750
# BB#621:                               #   in Loop: Header=BB3_989 Depth=2
	movb	1555(%rsp), %al
	testb	%al, %al
	jne	.LBB3_750
# BB#622:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	224(%rsp), %r15
	testq	%r15, %r15
	jle	.LBB3_934
# BB#623:                               # %.lr.ph.preheader.i1567
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	232(%rsp), %r14
	movq	240(%rsp), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
.LBB3_624:                              # %.lr.ph.i1570
                                        #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r14,%rbx,8), %rdi
	movl	$.L.str.40, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_851
# BB#625:                               #   in Loop: Header=BB3_624 Depth=3
	incq	%rbx
	cmpq	%r15, %rbx
	jl	.LBB3_624
	jmp	.LBB3_862
.LBB3_626:                              #   in Loop: Header=BB3_989 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	jmp	.LBB3_818
.LBB3_735:                              #   in Loop: Header=BB3_989 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	jmp	.LBB3_818
.LBB3_633:                              #   in Loop: Header=BB3_989 Depth=2
	movl	$21, %ebp
	jmp	.LBB3_819
.LBB3_634:                              # %.lr.ph1897.split.us.preheader
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	$1, %r13d
	jmp	.LBB3_636
	.p2align	4, 0x90
.LBB3_635:                              # %..lr.ph1897.split.us_crit_edge
                                        #   in Loop: Header=BB3_636 Depth=3
	movzbl	(%rbx,%r13), %r14d
	incq	%r13
.LBB3_636:                              # %.lr.ph1897.split.us
                                        #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rsp), %rdx            # 8-byte Reload
	testq	%rdx, %rdx
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	movzbl	%r14b, %ecx
	movzbl	(%rax,%rcx,4), %r14d
	je	.LBB3_640
# BB#637:                               #   in Loop: Header=BB3_636 Depth=3
	movl	8196(%rdx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_639
# BB#638:                               # %html_output_flush.exit11.i1603.us
                                        #   in Loop: Header=BB3_636 Depth=3
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	$0, 8196(%rdx)
	xorl	%eax, %eax
.LBB3_639:                              #   in Loop: Header=BB3_636 Depth=3
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdx)
	cltq
	movb	%r14b, 4(%rdx,%rax)
.LBB3_640:                              # %html_output_c.exit1604.us
                                        #   in Loop: Header=BB3_636 Depth=3
	cmpl	$9, %r15d
	jne	.LBB3_643
# BB#641:                               # %html_output_c.exit1604.us
                                        #   in Loop: Header=BB3_636 Depth=3
	movq	48(%rsp), %rcx          # 8-byte Reload
	cmpl	$1023, %ecx             # imm = 0x3FF
	jg	.LBB3_643
# BB#642:                               #   in Loop: Header=BB3_636 Depth=3
	movslq	%ecx, %rax
	incl	%ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movb	%r14b, 512(%rsp,%rax)
.LBB3_643:                              #   in Loop: Header=BB3_636 Depth=3
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	strlen
	cmpq	%rax, %r13
	jb	.LBB3_635
.LBB3_644:                              # %._crit_edge1898
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	%rbx, %rdi
	callq	free
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	44(%rsp), %r8d          # 4-byte Reload
	jmp	.LBB3_728
.LBB3_645:                              #   in Loop: Header=BB3_989 Depth=2
	movq	%rbx, %r14
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	44(%rsp), %r8d          # 4-byte Reload
	cmpl	$9, %r15d
	je	.LBB3_718
	jmp	.LBB3_720
.LBB3_646:                              #   in Loop: Header=BB3_989 Depth=2
	movl	$1, %eax
	leaq	1552(%rsp), %r15
	jmp	.LBB3_885
.LBB3_647:                              #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB3_651
# BB#648:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_650
# BB#649:                               # %html_output_flush.exit.i1502
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebx
	callq	cli_writen
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_650:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	$34, 4(%rbp,%rax)
.LBB3_651:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_655
# BB#652:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_654
# BB#653:                               # %html_output_flush.exit11.i1503
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebx
	callq	cli_writen
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_654:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	$34, 4(%rdi,%rax)
.LBB3_655:                              # %html_output_c.exit1504
                                        #   in Loop: Header=BB3_989 Depth=2
	movb	$34, 512(%rsp)
	incq	%r12
	movl	$9, %ebx
	movl	$1, 60(%rsp)            # 4-byte Folded Spill
	movl	$1, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	jmp	.LBB3_733
.LBB3_656:                              #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB3_660
# BB#657:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_659
# BB#658:                               # %html_output_flush.exit.i1496
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebx
	callq	cli_writen
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_659:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	$34, 4(%rbp,%rax)
.LBB3_660:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_664
# BB#661:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_663
# BB#662:                               # %html_output_flush.exit11.i1497
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebx
	callq	cli_writen
	movl	%ebx, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_663:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	$34, 4(%rdi,%rax)
.LBB3_664:                              # %html_output_c.exit1498
                                        #   in Loop: Header=BB3_989 Depth=2
	movb	$34, 512(%rsp)
	incq	%r12
	movl	$1, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	$9, %ebx
	movl	$0, 60(%rsp)            # 4-byte Folded Spill
	jmp	.LBB3_733
.LBB3_665:                              # %html_tag_arg_value.exit
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rbx
	testq	%rbx, %rbx
	je	.LBB3_816
# BB#666:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.23, %esi
	movq	%rbx, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB3_844
# BB#667:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.25, %esi
	movq	%rbx, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	movl	$1, %eax
	jne	.LBB3_885
# BB#668:                               # %.lr.ph.i1523.preheader
                                        #   in Loop: Header=BB3_989 Depth=2
	xorl	%ebx, %ebx
.LBB3_669:                              # %.lr.ph.i1523
                                        #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	256(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbx,8), %rdi
	movl	$.L.str.22, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_882
# BB#670:                               #   in Loop: Header=BB3_669 Depth=3
	incq	%rbx
	movl	$14, %r13d
	cmpq	192(%rsp), %rbx         # 8-byte Folded Reload
	jl	.LBB3_669
	jmp	.LBB3_884
.LBB3_671:                              #   in Loop: Header=BB3_989 Depth=2
	movslq	224(%rsp), %r13
	testq	%r13, %r13
	jle	.LBB3_824
# BB#672:                               # %.lr.ph.preheader.i1530
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	232(%rsp), %rbx
	movq	240(%rsp), %r14
	xorl	%ebp, %ebp
.LBB3_673:                              # %.lr.ph.i1533
                                        #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbx,%rbp,8), %rdi
	movl	$.L.str.28, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_821
# BB#674:                               #   in Loop: Header=BB3_673 Depth=3
	incq	%rbp
	cmpq	%r13, %rbp
	jl	.LBB3_673
# BB#675:                               #   in Loop: Header=BB3_989 Depth=2
	xorl	%r15d, %r15d
	xorl	%ebp, %ebp
	jmp	.LBB3_822
.LBB3_676:                              #   in Loop: Header=BB3_989 Depth=2
	cmpl	$1023, %edx             # imm = 0x3FF
	jg	.LBB3_679
# BB#677:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	%edx, %rax
	incl	%edx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
.LBB3_678:                              #   in Loop: Header=BB3_989 Depth=2
	movb	$34, 512(%rsp,%rax)
.LBB3_679:                              #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	movl	$9, %ebx
	jmp	.LBB3_733
.LBB3_680:                              # %.lr.ph1893.split.us.preheader
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	44(%rsp), %r8d          # 4-byte Reload
.LBB3_681:                              # %.lr.ph1893.split.us
                                        #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rdi, %rdi
	movq	(%r14), %rax
	movzbl	(%rbp), %ecx
	movzbl	(%rax,%rcx,4), %ebx
	je	.LBB3_685
# BB#682:                               #   in Loop: Header=BB3_681 Depth=3
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_684
# BB#683:                               # %html_output_flush.exit11.i1618.us
                                        #   in Loop: Header=BB3_681 Depth=3
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	44(%rsp), %r8d          # 4-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_684:                              #   in Loop: Header=BB3_681 Depth=3
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	%bl, 4(%rdi,%rax)
.LBB3_685:                              # %html_output_c.exit1619.us
                                        #   in Loop: Header=BB3_681 Depth=3
	cmpl	$9, %r15d
	jne	.LBB3_688
# BB#686:                               # %html_output_c.exit1619.us
                                        #   in Loop: Header=BB3_681 Depth=3
	movq	48(%rsp), %rcx          # 8-byte Reload
	cmpl	$1023, %ecx             # imm = 0x3FF
	jg	.LBB3_688
# BB#687:                               #   in Loop: Header=BB3_681 Depth=3
	movslq	%ecx, %rax
	incl	%ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movb	%bl, 512(%rsp,%rax)
.LBB3_688:                              # %html_output_c.exit1616.us
                                        #   in Loop: Header=BB3_681 Depth=3
	incq	%rbp
	xorl	%r13d, %r13d
	decq	120(%rsp)               # 8-byte Folded Spill
	jne	.LBB3_681
.LBB3_689:                              #   in Loop: Header=BB3_989 Depth=2
	xorl	%eax, %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	jmp	.LBB3_998
.LBB3_690:                              #   in Loop: Header=BB3_989 Depth=2
	movl	$20, %ebp
	movq	168(%rsp), %r13         # 8-byte Reload
	testq	%r13, %r13
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	44(%rsp), %r8d          # 4-byte Reload
	je	.LBB3_819
# BB#691:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%r13), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_693
# BB#692:                               # %html_output_flush.exit.i1701
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%r13), %edi
	leaq	4(%r13), %rsi
	movl	$8192, %edx             # imm = 0x2000
	callq	cli_writen
	movl	44(%rsp), %r8d          # 4-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%r13)
	xorl	%eax, %eax
.LBB3_693:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%r13)
	cltq
	movb	%bl, 4(%r13,%rax)
.LBB3_819:                              #   in Loop: Header=BB3_989 Depth=2
	movl	%r15d, %r13d
.LBB3_820:                              # %html_output_c.exit1698
                                        #   in Loop: Header=BB3_989 Depth=2
	xorl	%eax, %eax
	cmpb	$92, 1(%r12)
	leaq	1(%r12), %r12
	sete	%al
	movq	%rax, 152(%rsp)         # 8-byte Spill
	jmp	.LBB3_997
.LBB3_695:                              #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%r13d, %r8d
	je	.LBB3_699
# BB#696:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_698
# BB#697:                               # %html_output_flush.exit.i1508
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	%r13d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_698:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	%bl, 4(%rbp,%rax)
.LBB3_699:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	movq	48(%rsp), %rbp          # 8-byte Reload
	je	.LBB3_703
# BB#700:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_702
# BB#701:                               # %html_output_flush.exit11.i1509
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	%r13d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_702:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	%bl, 4(%rdi,%rax)
.LBB3_703:                              # %html_output_c.exit1510
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpl	$1023, %ebp             # imm = 0x3FF
	jg	.LBB3_707
# BB#704:                               #   in Loop: Header=BB3_989 Depth=2
	movq	(%r14), %rax
	movzbl	(%r12), %ecx
	movzwl	(%rax,%rcx,2), %ecx
	andl	$8192, %ecx             # imm = 0x2000
	movslq	%ebp, %rax
	testw	%cx, %cx
	movb	$32, %cl
	jne	.LBB3_706
# BB#705:                               #   in Loop: Header=BB3_989 Depth=2
	movb	$62, %cl
.LBB3_706:                              #   in Loop: Header=BB3_989 Depth=2
	incl	%ebp
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movb	%cl, 512(%rsp,%rax)
.LBB3_707:                              #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	movl	$5, %ebx
	movl	$9, %r15d
	movl	$2, 60(%rsp)            # 4-byte Folded Spill
	jmp	.LBB3_733
.LBB3_708:                              # %.lr.ph1902.split.us.preheader
                                        #   in Loop: Header=BB3_989 Depth=2
	leaq	2592(%rsp), %rbp
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	44(%rsp), %r8d          # 4-byte Reload
.LBB3_709:                              # %.lr.ph1902.split.us
                                        #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rdi, %rdi
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movzbl	(%rbp), %ecx
	movzbl	(%rax,%rcx,4), %ebx
	je	.LBB3_713
# BB#710:                               #   in Loop: Header=BB3_709 Depth=3
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_712
# BB#711:                               # %html_output_flush.exit11.i1609.us
                                        #   in Loop: Header=BB3_709 Depth=3
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	44(%rsp), %r8d          # 4-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_712:                              #   in Loop: Header=BB3_709 Depth=3
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	%bl, 4(%rdi,%rax)
.LBB3_713:                              # %html_output_c.exit1610.us
                                        #   in Loop: Header=BB3_709 Depth=3
	cmpl	$9, %r15d
	jne	.LBB3_716
# BB#714:                               # %html_output_c.exit1610.us
                                        #   in Loop: Header=BB3_709 Depth=3
	movq	%r14, %rcx
	cmpl	$1023, %ecx             # imm = 0x3FF
	jg	.LBB3_716
# BB#715:                               #   in Loop: Header=BB3_709 Depth=3
	movslq	%ecx, %rax
	incl	%ecx
	movq	%rcx, %r14
	movb	%bl, 512(%rsp,%rax)
.LBB3_716:                              #   in Loop: Header=BB3_709 Depth=3
	incq	%rbp
	decq	120(%rsp)               # 8-byte Folded Spill
	jne	.LBB3_709
.LBB3_717:                              # %._crit_edge1903
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpl	$9, %r15d
	jne	.LBB3_720
.LBB3_718:                              # %._crit_edge1903
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpl	$1023, %r14d            # imm = 0x3FF
	jg	.LBB3_720
# BB#719:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	%r14d, %rax
	incl	%r14d
	movb	$59, 512(%rsp,%rax)
.LBB3_720:                              #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB3_724
# BB#721:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_723
# BB#722:                               # %html_output_flush.exit.i1611
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbx), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	44(%rsp), %r8d          # 4-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbx)
	xorl	%eax, %eax
.LBB3_723:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbx)
	cltq
	movb	$59, 4(%rbx,%rax)
.LBB3_724:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	movq	%r14, 48(%rsp)          # 8-byte Spill
	je	.LBB3_728
# BB#725:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_727
# BB#726:                               # %html_output_flush.exit11.i1612
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movl	44(%rsp), %r8d          # 4-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_727:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	$59, 4(%rdi,%rax)
.LBB3_728:                              # %html_output_c.exit1613
                                        #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	xorl	%eax, %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	xorl	%r13d, %r13d
	jmp	.LBB3_998
.LBB3_729:                              #   in Loop: Header=BB3_989 Depth=2
	cmpl	$1023, %ebp             # imm = 0x3FF
	movq	%rbp, %rcx
	movl	%r8d, %ebp
	jg	.LBB3_731
# BB#730:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	%ecx, %rax
	incl	%ecx
	movb	$34, 512(%rsp,%rax)
.LBB3_731:                              #   in Loop: Header=BB3_989 Depth=2
	movq	%rcx, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movslq	%ecx, %rax
	movb	$0, 512(%rsp,%rax)
	leaq	224(%rsp), %rdi
	leaq	3632(%rsp), %rsi
	leaq	512(%rsp), %rdx
	callq	html_tag_arg_add
	incq	%r12
	movl	$0, 88(%rsp)            # 4-byte Folded Spill
	movl	$5, %ebx
	movl	$8, %r15d
.LBB3_732:                              # %html_output_c.exit1492
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%ebp, %r8d
.LBB3_733:                              # %html_output_c.exit1492
                                        #   in Loop: Header=BB3_989 Depth=2
	xorl	%eax, %eax
	cmpb	$92, (%r12)
	sete	%al
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movl	%r15d, %r13d
	movl	%ebx, %r15d
	jmp	.LBB3_998
.LBB3_736:                              #   in Loop: Header=BB3_989 Depth=2
	xorl	%r13d, %r13d
	jmp	.LBB3_998
.LBB3_738:                              #   in Loop: Header=BB3_989 Depth=2
	movslq	%edx, %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %r14
	leaq	4(%r14,%rax), %rdi
	movslq	%ebx, %rdx
	movq	%rbp, %rsi
	callq	memcpy
	addl	%ebx, 8196(%r14)
.LBB3_739:                              # %html_output_str.exit
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	(%rsp), %rdi            # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB3_748
# BB#740:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %edx
	testl	%edx, %edx
	jle	.LBB3_743
# BB#741:                               #   in Loop: Header=BB3_989 Depth=2
	leal	(%rdx,%rbx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jl	.LBB3_743
# BB#742:                               #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movq	64(%rsp), %rsi          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_writen
	movq	(%rsp), %rax            # 8-byte Reload
	movl	$0, 8196(%rax)
	xorl	%edx, %edx
.LBB3_743:                              # %html_output_flush.exit.i1626
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpl	$8192, %ebx             # imm = 0x2000
	jl	.LBB3_747
# BB#744:                               #   in Loop: Header=BB3_989 Depth=2
	testl	%edx, %edx
	jle	.LBB3_746
# BB#745:                               #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movq	64(%rsp), %rsi          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_writen
	movq	(%rsp), %rax            # 8-byte Reload
	movl	$0, 8196(%rax)
.LBB3_746:                              # %html_output_flush.exit13.i1627
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movq	%rbp, %rsi
	movl	%ebx, %edx
	callq	cli_writen
	movq	(%rsp), %rdi            # 8-byte Reload
	jmp	.LBB3_748
.LBB3_747:                              #   in Loop: Header=BB3_989 Depth=2
	movslq	%edx, %rax
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	4(%rcx,%rax), %rdi
	movslq	%ebx, %rdx
	movq	%rbp, %rsi
	callq	memcpy
	movq	(%rsp), %rdi            # 8-byte Reload
	addl	%ebx, 8196(%rdi)
.LBB3_748:                              # %html_output_str.exit1628
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	44(%rsp), %r8d          # 4-byte Reload
.LBB3_749:                              # %html_output_c.exit1622
                                        #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
	xorl	%r13d, %r13d
	jmp	.LBB3_998
.LBB3_750:                              # %.thread2063
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.42, %esi
	leaq	1552(%rsp), %rbx
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_847
# BB#751:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.43, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB3_934
# BB#752:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	224(%rsp), %rbx
	testq	%rbx, %rbx
	jle	.LBB3_934
# BB#753:                               # %.lr.ph.preheader.i1591
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	232(%rsp), %r15
	movq	240(%rsp), %r14
	xorl	%ebp, %ebp
.LBB3_754:                              # %.lr.ph.i1594
                                        #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r15,%rbp,8), %rdi
	movl	$.L.str.34, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_955
# BB#755:                               #   in Loop: Header=BB3_754 Depth=3
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB3_754
	jmp	.LBB3_865
.LBB3_756:                              #   in Loop: Header=BB3_989 Depth=2
	cmpl	$0, 4(%rbp)
	je	.LBB3_618
# BB#757:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	224(%rsp), %rbx
	testq	%rbx, %rbx
	jle	.LBB3_934
# BB#758:                               # %.lr.ph.preheader.i1561
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	232(%rsp), %r15
	movq	240(%rsp), %r14
	xorl	%ebp, %ebp
.LBB3_759:                              # %.lr.ph.i1564
                                        #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r15,%rbp,8), %rdi
	movl	$.L.str.38, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_878
# BB#760:                               #   in Loop: Header=BB3_759 Depth=3
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB3_759
	jmp	.LBB3_865
.LBB3_761:                              #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB3_765
# BB#762:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_764
# BB#763:                               # %html_output_flush.exit.i1650
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %r14d
	callq	cli_writen
	movl	%r14d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_764:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	$13, 4(%rbp,%rax)
.LBB3_765:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_769
# BB#766:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_768
# BB#767:                               # %html_output_flush.exit11.i1651
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebp
	callq	cli_writen
	movl	%ebp, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_768:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	$13, 4(%rdi,%rax)
.LBB3_769:                              # %html_output_c.exit1652
                                        #   in Loop: Header=BB3_989 Depth=2
	testq	%rsi, %rsi
	je	.LBB3_403
# BB#770:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rsi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_772
# BB#771:                               # %html_output_flush.exit.i1653
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rsi), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	104(%rsp), %rsi         # 8-byte Reload
	movl	%r8d, %ebp
	callq	cli_writen
	movl	%ebp, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
	xorl	%eax, %eax
.LBB3_772:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rsi)
	cltq
	movb	$13, 4(%rsi,%rax)
	jmp	.LBB3_403
.LBB3_773:                              #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB3_777
# BB#774:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_776
# BB#775:                               # %html_output_flush.exit.i1655
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %r14d
	callq	cli_writen
	movl	%r14d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_776:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	$64, 4(%rbp,%rax)
.LBB3_777:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_781
# BB#778:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_780
# BB#779:                               # %html_output_flush.exit11.i1656
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebp
	callq	cli_writen
	movl	%ebp, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_780:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	$64, 4(%rdi,%rax)
.LBB3_781:                              # %html_output_c.exit1657
                                        #   in Loop: Header=BB3_989 Depth=2
	testq	%rsi, %rsi
	je	.LBB3_403
# BB#782:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rsi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_784
# BB#783:                               # %html_output_flush.exit.i1658
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rsi), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	104(%rsp), %rsi         # 8-byte Reload
	movl	%r8d, %ebp
	callq	cli_writen
	movl	%ebp, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
	xorl	%eax, %eax
.LBB3_784:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rsi)
	cltq
	movb	$64, 4(%rsi,%rax)
	jmp	.LBB3_403
.LBB3_785:                              #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB3_789
# BB#786:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_788
# BB#787:                               # %html_output_flush.exit.i1660
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %r14d
	callq	cli_writen
	movl	%r14d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_788:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	$10, 4(%rbp,%rax)
.LBB3_789:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_793
# BB#790:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_792
# BB#791:                               # %html_output_flush.exit11.i1661
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebp
	callq	cli_writen
	movl	%ebp, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_792:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	$10, 4(%rdi,%rax)
.LBB3_793:                              # %html_output_c.exit1662
                                        #   in Loop: Header=BB3_989 Depth=2
	testq	%rsi, %rsi
	je	.LBB3_403
# BB#794:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rsi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_796
# BB#795:                               # %html_output_flush.exit.i1663
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rsi), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	104(%rsp), %rsi         # 8-byte Reload
	movl	%r8d, %ebp
	callq	cli_writen
	movl	%ebp, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
	xorl	%eax, %eax
.LBB3_796:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rsi)
	cltq
	movb	$10, 4(%rsi,%rax)
	jmp	.LBB3_403
.LBB3_797:                              #   in Loop: Header=BB3_989 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB3_801
# BB#798:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_800
# BB#799:                               # %html_output_flush.exit.i1665
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %r14d
	callq	cli_writen
	movl	%r14d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rbp)
	xorl	%eax, %eax
.LBB3_800:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbp)
	cltq
	movb	$62, 4(%rbp,%rax)
.LBB3_801:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%rdi, %rdi
	je	.LBB3_805
# BB#802:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rdi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_804
# BB#803:                               # %html_output_flush.exit11.i1666
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %ebp
	callq	cli_writen
	movl	%ebp, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rdi)
	xorl	%eax, %eax
.LBB3_804:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdi)
	cltq
	movb	$62, 4(%rdi,%rax)
.LBB3_805:                              # %html_output_c.exit1667
                                        #   in Loop: Header=BB3_989 Depth=2
	testq	%rsi, %rsi
	je	.LBB3_403
# BB#806:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rsi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_808
# BB#807:                               # %html_output_flush.exit.i1668
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rsi), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	104(%rsp), %rsi         # 8-byte Reload
	movl	%r8d, %ebp
	callq	cli_writen
	movl	%ebp, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
	xorl	%eax, %eax
.LBB3_808:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rsi)
	cltq
	movb	$62, 4(%rsi,%rax)
	jmp	.LBB3_403
.LBB3_809:                              # %html_tag_arg_value.exit1548
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%r15,%rbp,8), %rdx
	testq	%rdx, %rdx
	je	.LBB3_865
# BB#810:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$0, (%rdx)
	je	.LBB3_865
# BB#811:                               #   in Loop: Header=BB3_989 Depth=2
	cmpl	$0, 4(%r14)
	je	.LBB3_936
# BB#812:                               # %.lr.ph.i1552.preheader
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
.LBB3_813:                              # %.lr.ph.i1552
                                        #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r13,%rbp,8), %rdi
	movl	$.L.str.35, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_949
# BB#814:                               #   in Loop: Header=BB3_813 Depth=3
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB3_813
# BB#815:                               #   in Loop: Header=BB3_989 Depth=2
	xorl	%r13d, %r13d
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	jne	.LBB3_950
	jmp	.LBB3_970
.LBB3_816:                              #   in Loop: Header=BB3_989 Depth=2
	movl	$1, %eax
	jmp	.LBB3_885
.LBB3_817:                              #   in Loop: Header=BB3_989 Depth=2
	xorl	%eax, %eax
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%r15d, %r8d
.LBB3_818:                              # %html_tag_contents_append.exit
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	%r12, 296(%rsp)         # 8-byte Spill
	incq	%r12
	movl	$0, 188(%rsp)           # 4-byte Folded Spill
	movl	$5, %r15d
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$7, %r13d
	jmp	.LBB3_998
.LBB3_821:                              #   in Loop: Header=BB3_989 Depth=2
	movq	(%r14,%rbp,8), %r15
	xorl	%ebp, %ebp
.LBB3_822:                              # %.lr.ph.i1539
                                        #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbx,%rbp,8), %rdi
	movl	$.L.str.29, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_825
# BB#823:                               #   in Loop: Header=BB3_822 Depth=3
	incq	%rbp
	cmpq	%r13, %rbp
	jl	.LBB3_822
.LBB3_824:                              #   in Loop: Header=BB3_989 Depth=2
	movq	32(%rsp), %r14          # 8-byte Reload
	movl	$1, %r13d
	jmp	.LBB3_934
.LBB3_825:                              # %html_tag_arg_value.exit1541
                                        #   in Loop: Header=BB3_989 Depth=2
	testq	%r15, %r15
	movl	$1, %r13d
	je	.LBB3_933
# BB#826:                               # %html_tag_arg_value.exit1541
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%r14,%rbp,8), %rbp
	testq	%rbp, %rbp
	je	.LBB3_933
# BB#827:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.30, %esi
	movq	%r15, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	jne	.LBB3_933
# BB#828:                               #   in Loop: Header=BB3_989 Depth=2
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %r13
	leaq	1(%r13), %rdi
	callq	cli_malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB3_1094
# BB#829:                               # %.preheader1762
                                        #   in Loop: Header=BB3_989 Depth=2
	testq	%r13, %r13
	movabsq	$571230650369, %r15     # imm = 0x8500000001
	je	.LBB3_835
# BB#830:                               # %.lr.ph1906
                                        #   in Loop: Header=BB3_989 Depth=2
	callq	__ctype_tolower_loc
	leaq	-1(%r13), %r8
	movq	%r13, %rdx
	xorl	%esi, %esi
	andq	$3, %rdx
	je	.LBB3_832
.LBB3_831:                              #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rdi
	movzbl	(%rbp,%rsi), %ecx
	movzbl	(%rdi,%rcx,4), %ecx
	movb	%cl, (%r14,%rsi)
	incq	%rsi
	cmpq	%rsi, %rdx
	jne	.LBB3_831
.LBB3_832:                              # %.prol.loopexit
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpq	$3, %r8
	jb	.LBB3_835
# BB#833:                               # %.lr.ph1906.new
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	%r13, %rcx
	subq	%rsi, %rcx
	leaq	3(%r14,%rsi), %rdx
	leaq	3(%rbp,%rsi), %rsi
.LBB3_834:                              #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rdi
	movzbl	-3(%rsi), %ebp
	movzbl	(%rdi,%rbp,4), %ebx
	movb	%bl, -3(%rdx)
	movq	(%rax), %rdi
	movzbl	-2(%rsi), %ebp
	movzbl	(%rdi,%rbp,4), %ebx
	movb	%bl, -2(%rdx)
	movq	(%rax), %rdi
	movzbl	-1(%rsi), %ebp
	movzbl	(%rdi,%rbp,4), %ebx
	movb	%bl, -1(%rdx)
	movq	(%rax), %rdi
	movzbl	(%rsi), %ebp
	movzbl	(%rdi,%rbp,4), %ebx
	movb	%bl, (%rdx)
	addq	$4, %rdx
	addq	$4, %rsi
	addq	$-4, %rcx
	jne	.LBB3_834
.LBB3_835:                              # %._crit_edge1907
                                        #   in Loop: Header=BB3_989 Depth=2
	movb	$0, (%r14,%r13)
	movl	$.L.str.31, %esi
	movq	%r14, %rdi
	callq	strstr
	testq	%rax, %rax
	je	.LBB3_843
.LBB3_836:                              # %.preheader1761
                                        #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	setne	%dl
	incq	%rax
	cmpb	$61, %cl
	je	.LBB3_838
# BB#837:                               # %.preheader1761
                                        #   in Loop: Header=BB3_836 Depth=3
	testb	%dl, %dl
	jne	.LBB3_836
.LBB3_838:                              #   in Loop: Header=BB3_989 Depth=2
	leaq	-1(%rax), %rsi
	testb	%cl, %cl
	cmovneq	%rax, %rsi
	xorl	%eax, %eax
.LBB3_839:                              #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rsi,%rax), %ecx
	incq	%rax
	cmpq	$39, %rcx
	ja	.LBB3_839
# BB#840:                               #   in Loop: Header=BB3_839 Depth=3
	btq	%rcx, %r15
	jae	.LBB3_839
# BB#841:                               # %__strcspn_c3.exit
                                        #   in Loop: Header=BB3_989 Depth=2
	movb	$0, -1(%rsi,%rax)
	cmpq	$1, %rax
	je	.LBB3_843
# BB#842:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$5, %edx
	leaq	336(%rsp), %rdi
	callq	process_encoding_set
.LBB3_843:                              # %.thread
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	%r14, %rdi
	callq	free
	jmp	.LBB3_824
.LBB3_844:                              # %.lr.ph.i1520.preheader
                                        #   in Loop: Header=BB3_989 Depth=2
	xorl	%ebx, %ebx
.LBB3_845:                              # %.lr.ph.i1520
                                        #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	256(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbx,8), %rdi
	movl	$.L.str.22, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_877
# BB#846:                               #   in Loop: Header=BB3_845 Depth=3
	incq	%rbx
	cmpq	192(%rsp), %rbx         # 8-byte Folded Reload
	jl	.LBB3_845
	jmp	.LBB3_883
.LBB3_847:                              #   in Loop: Header=BB3_989 Depth=2
	movslq	224(%rsp), %rbx
	testq	%rbx, %rbx
	jle	.LBB3_934
# BB#848:                               # %.lr.ph.preheader.i1583
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	232(%rsp), %r15
	movq	240(%rsp), %r14
	xorl	%ebp, %ebp
.LBB3_849:                              # %.lr.ph.i1586
                                        #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r15,%rbp,8), %rdi
	movl	$.L.str.40, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_937
# BB#850:                               #   in Loop: Header=BB3_849 Depth=3
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB3_849
	jmp	.LBB3_865
.LBB3_851:                              # %html_tag_arg_value.exit1572
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %r13
	testq	%r13, %r13
	je	.LBB3_862
# BB#852:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$0, (%r13)
	je	.LBB3_862
# BB#853:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.40, %esi
	movq	%rbp, %rbx
	movq	%rbx, %rdi
	movq	%r13, %rdx
	callq	html_tag_arg_add
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	je	.LBB3_856
# BB#854:                               #   in Loop: Header=BB3_989 Depth=2
	movl	4(%rbx), %eax
	testl	%eax, %eax
	je	.LBB3_856
# BB#855:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	(%rbx), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	callq	blobCreate
	movq	24(%rbx), %rcx
	movq	192(%rsp), %rdx         # 8-byte Reload
	movq	%rax, -8(%rcx,%rdx,8)
	movq	16(%rbx), %rax
	movq	24(%rbx), %rcx
	movq	-8(%rcx,%rdx,8), %rcx
	movq	%rcx, 256(%rsp)         # 8-byte Spill
	movslq	28(%rsp), %rcx          # 4-byte Folded Reload
	movq	-8(%rax,%rcx,8), %rdi
	movq	%rdi, 272(%rsp)         # 8-byte Spill
	callq	strlen
	movq	256(%rsp), %rdi         # 8-byte Reload
	movq	272(%rsp), %rsi         # 8-byte Reload
	movq	%rax, %rdx
	callq	blobAddData
	movq	24(%rbx), %rax
	movq	192(%rsp), %rcx         # 8-byte Reload
	movq	-8(%rax,%rcx,8), %rdi
	movl	$.L.str.52, %esi
	movl	$1, %edx
	callq	blobAddData
	movq	24(%rbx), %rax
	movq	192(%rsp), %rcx         # 8-byte Reload
	movq	-8(%rax,%rcx,8), %rdi
	callq	blobClose
.LBB3_856:                              #   in Loop: Header=BB3_989 Depth=2
	cmpq	$0, 96(%rsp)            # 8-byte Folded Reload
	je	.LBB3_862
# BB#857:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.37, %esi
	movq	%rbp, %rbx
	movq	%rbx, %rdi
	movq	%r13, %rdx
	callq	html_tag_arg_add
	callq	blobCreate
	movq	24(%rbx), %rcx
	movslq	(%rbx), %rdx
	movq	%rdx, 192(%rsp)         # 8-byte Spill
	movq	%rax, -8(%rcx,%rdx,8)
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	strlen
	movq	%rax, %r13
	testq	%r13, %r13
	jle	.LBB3_861
# BB#858:                               #   in Loop: Header=BB3_989 Depth=2
	movq	24(%rbx), %rax
	movq	192(%rsp), %rcx         # 8-byte Reload
	movq	-8(%rax,%rcx,8), %rdi
	callq	blobGetDataSize
	cmpq	$1024, %rax             # imm = 0x400
	ja	.LBB3_861
# BB#859:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$1024, %ecx             # imm = 0x400
	subq	%rax, %rcx
	je	.LBB3_861
# BB#860:                               #   in Loop: Header=BB3_989 Depth=2
	movq	24(%rbx), %rax
	movq	192(%rsp), %rdx         # 8-byte Reload
	movq	-8(%rax,%rdx,8), %rdi
	cmpq	%r13, %rcx
	cmovbq	%rcx, %r13
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	%r13, %rdx
	callq	blobAddData
.LBB3_861:                              # %html_tag_contents_append.exit1574
                                        #   in Loop: Header=BB3_989 Depth=2
	movslq	(%rbx), %r13
	movq	24(%rbx), %rax
	movq	-8(%rax,%r13,8), %rdi
	movl	$.L.str.52, %esi
	movl	$1, %edx
	callq	blobAddData
	movq	24(%rbx), %rax
	movq	-8(%rax,%r13,8), %rdi
	callq	blobClose
.LBB3_862:                              # %.lr.ph.i1578.preheader
                                        #   in Loop: Header=BB3_989 Depth=2
	xorl	%ebx, %ebx
.LBB3_863:                              # %.lr.ph.i1578
                                        #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r14,%rbx,8), %rdi
	movl	$.L.str.41, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_866
# BB#864:                               #   in Loop: Header=BB3_863 Depth=3
	incq	%rbx
	cmpq	%r15, %rbx
	jl	.LBB3_863
.LBB3_865:                              # %html_output_tag.exit
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	$1, %r13d
	jmp	.LBB3_933
.LBB3_866:                              # %html_tag_arg_value.exit1580
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %r15
	testq	%r15, %r15
	movq	%rbp, %rbx
	movq	32(%rsp), %r14          # 8-byte Reload
	je	.LBB3_948
# BB#867:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$0, (%r15)
	je	.LBB3_967
# BB#868:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.41, %esi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	html_tag_arg_add
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	je	.LBB3_871
# BB#869:                               #   in Loop: Header=BB3_989 Depth=2
	movl	4(%rbx), %eax
	testl	%eax, %eax
	je	.LBB3_871
# BB#870:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	(%rbx), %r14
	callq	blobCreate
	movq	24(%rbx), %rcx
	movq	%rax, -8(%rcx,%r14,8)
	movq	16(%rbx), %rax
	movq	24(%rbx), %rcx
	movq	-8(%rcx,%r14,8), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movslq	28(%rsp), %rcx          # 4-byte Folded Reload
	movq	-8(%rax,%rcx,8), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	blobAddData
	movq	24(%rbx), %rax
	movq	-8(%rax,%r14,8), %rdi
	movl	$.L.str.52, %esi
	movl	$1, %edx
	callq	blobAddData
	movq	24(%rbx), %rax
	movq	-8(%rax,%r14,8), %rdi
	movq	32(%rsp), %r14          # 8-byte Reload
	callq	blobClose
.LBB3_871:                              #   in Loop: Header=BB3_989 Depth=2
	movq	96(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB3_986
# BB#872:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.37, %esi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	html_tag_arg_add
	callq	blobCreate
	movq	24(%rbx), %rcx
	movslq	(%rbx), %r15
	movq	%rax, -8(%rcx,%r15,8)
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rbp
	testq	%rbp, %rbp
	movl	$1, %r13d
	jle	.LBB3_876
# BB#873:                               #   in Loop: Header=BB3_989 Depth=2
	movq	24(%rbx), %rax
	movq	-8(%rax,%r15,8), %rdi
	callq	blobGetDataSize
	cmpq	$1024, %rax             # imm = 0x400
	ja	.LBB3_876
# BB#874:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$1024, %ecx             # imm = 0x400
	subq	%rax, %rcx
	je	.LBB3_876
# BB#875:                               #   in Loop: Header=BB3_989 Depth=2
	movq	24(%rbx), %rax
	movq	-8(%rax,%r15,8), %rdi
	cmpq	%rbp, %rcx
	cmovbq	%rcx, %rbp
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rdx
	callq	blobAddData
.LBB3_876:                              # %html_tag_contents_append.exit1582
                                        #   in Loop: Header=BB3_989 Depth=2
	movslq	(%rbx), %rbp
	movq	24(%rbx), %rax
	movq	-8(%rax,%rbp,8), %rdi
	movl	$.L.str.52, %esi
	movl	$1, %edx
	callq	blobAddData
	movq	24(%rbx), %rax
	movq	-8(%rax,%rbp,8), %rdi
	callq	blobClose
	jmp	.LBB3_934
.LBB3_877:                              #   in Loop: Header=BB3_989 Depth=2
	movq	80(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp,%rbx,8), %rdi
	callq	free
	movl	$.L.str.24, %edi
	callq	cli_strdup
	movq	%rax, (%rbp,%rbx,8)
	jmp	.LBB3_883
.LBB3_878:                              # %html_tag_arg_value.exit1566
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%r14,%rbp,8), %rbx
	testq	%rbx, %rbx
	je	.LBB3_865
# BB#879:                               #   in Loop: Header=BB3_989 Depth=2
	movq	96(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	movq	32(%rsp), %r14          # 8-byte Reload
	je	.LBB3_881
# BB#880:                               #   in Loop: Header=BB3_989 Depth=2
	callq	free
.LBB3_881:                              #   in Loop: Header=BB3_989 Depth=2
	movq	%rbx, %rdi
	callq	cli_strdup
	jmp	.LBB3_987
.LBB3_882:                              #   in Loop: Header=BB3_989 Depth=2
	movq	80(%rsp), %r14          # 8-byte Reload
	movq	(%r14,%rbx,8), %rdi
	callq	free
	movl	$.L.str.26, %edi
	callq	cli_strdup
	movq	%rax, (%r14,%rbx,8)
.LBB3_883:                              # %.critedge1410
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	$14, %r13d
.LBB3_884:                              # %.critedge1410
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	92(%rsp), %eax          # 4-byte Reload
.LBB3_885:                              # %.critedge1410
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	%eax, 92(%rsp)          # 4-byte Spill
	movq	8(%rsp), %rsi           # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB3_896
# BB#886:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rsi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_888
# BB#887:                               # %html_output_flush.exit.i.i
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rsi), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	104(%rsp), %rsi         # 8-byte Reload
	callq	cli_writen
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
	xorl	%eax, %eax
.LBB3_888:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rsi)
	cltq
	movb	$60, 4(%rsi,%rax)
	movq	%r15, %rdi
	callq	strlen
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, %rbx
	movl	8196(%rcx), %edx
	testl	%edx, %edx
	jle	.LBB3_891
# BB#889:                               #   in Loop: Header=BB3_989 Depth=2
	leal	(%rdx,%rbx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jl	.LBB3_891
# BB#890:                               #   in Loop: Header=BB3_989 Depth=2
	movl	(%rcx), %edi
	movq	104(%rsp), %rsi         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_writen
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	$0, 8196(%rcx)
	xorl	%edx, %edx
.LBB3_891:                              # %html_output_flush.exit.i34.i
                                        #   in Loop: Header=BB3_989 Depth=2
	cmpl	$8192, %ebx             # imm = 0x2000
	jl	.LBB3_895
# BB#892:                               #   in Loop: Header=BB3_989 Depth=2
	testl	%edx, %edx
	jle	.LBB3_894
# BB#893:                               #   in Loop: Header=BB3_989 Depth=2
	movl	(%rcx), %edi
	movq	104(%rsp), %rsi         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_writen
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	$0, 8196(%rcx)
.LBB3_894:                              # %html_output_flush.exit13.i.i
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rcx), %edi
	movq	%r15, %rsi
	movl	%ebx, %edx
	callq	cli_writen
	movq	8(%rsp), %rsi           # 8-byte Reload
	jmp	.LBB3_896
.LBB3_895:                              #   in Loop: Header=BB3_989 Depth=2
	movslq	%edx, %rax
	leaq	4(%rcx,%rax), %rdi
	movslq	%ebx, %rdx
	movq	%r15, %rsi
	callq	memcpy
	movq	8(%rsp), %rsi           # 8-byte Reload
	addl	%ebx, 8196(%rsi)
.LBB3_896:                              # %html_output_str.exit.preheader.i
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	%r13, 272(%rsp)         # 8-byte Spill
	cmpl	$0, 192(%rsp)           # 4-byte Folded Reload
	jle	.LBB3_929
# BB#897:                               # %.lr.ph53.i.preheader
                                        #   in Loop: Header=BB3_989 Depth=2
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB3_898:                              # %.lr.ph53.i
                                        #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_921 Depth 4
	testq	%rsi, %rsi
	je	.LBB3_902
# BB#899:                               #   in Loop: Header=BB3_898 Depth=3
	movl	8196(%rsi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_901
# BB#900:                               # %html_output_flush.exit.i35.i
                                        #   in Loop: Header=BB3_898 Depth=3
	movl	(%rsi), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	104(%rsp), %rsi         # 8-byte Reload
	callq	cli_writen
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
	xorl	%eax, %eax
.LBB3_901:                              #   in Loop: Header=BB3_898 Depth=3
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rsi)
	cltq
	movb	$32, 4(%rsi,%rax)
.LBB3_902:                              # %html_output_c.exit36.i
                                        #   in Loop: Header=BB3_898 Depth=3
	movq	256(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r13,8), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rax, %rbp
	testq	%rsi, %rsi
	je	.LBB3_911
# BB#903:                               #   in Loop: Header=BB3_898 Depth=3
	movl	8196(%rsi), %edx
	testl	%edx, %edx
	jle	.LBB3_906
# BB#904:                               #   in Loop: Header=BB3_898 Depth=3
	leal	(%rdx,%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jl	.LBB3_906
# BB#905:                               #   in Loop: Header=BB3_898 Depth=3
	movl	(%rsi), %edi
	movq	104(%rsp), %rsi         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_writen
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
	xorl	%edx, %edx
.LBB3_906:                              # %html_output_flush.exit.i38.i
                                        #   in Loop: Header=BB3_898 Depth=3
	cmpl	$8192, %ebp             # imm = 0x2000
	jl	.LBB3_910
# BB#907:                               #   in Loop: Header=BB3_898 Depth=3
	testl	%edx, %edx
	jle	.LBB3_909
# BB#908:                               #   in Loop: Header=BB3_898 Depth=3
	movl	(%rsi), %edi
	movq	104(%rsp), %rsi         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_writen
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
.LBB3_909:                              # %html_output_flush.exit13.i39.i
                                        #   in Loop: Header=BB3_898 Depth=3
	movl	(%rsi), %edi
	movq	%rbx, %rsi
	movl	%ebp, %edx
	callq	cli_writen
	movq	8(%rsp), %rsi           # 8-byte Reload
	jmp	.LBB3_911
.LBB3_910:                              #   in Loop: Header=BB3_898 Depth=3
	movslq	%edx, %rax
	leaq	4(%rsi,%rax), %rdi
	movslq	%ebp, %rdx
	movq	%rbx, %rsi
	callq	memcpy
	movq	8(%rsp), %rsi           # 8-byte Reload
	addl	%ebp, 8196(%rsi)
.LBB3_911:                              # %html_output_str.exit40.i
                                        #   in Loop: Header=BB3_898 Depth=3
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r13,8), %rbp
	testq	%rbp, %rbp
	je	.LBB3_928
# BB#912:                               #   in Loop: Header=BB3_898 Depth=3
	testq	%rsi, %rsi
	je	.LBB3_917
# BB#913:                               #   in Loop: Header=BB3_898 Depth=3
	movl	8196(%rsi), %edx
	testl	%edx, %edx
	jle	.LBB3_916
# BB#914:                               #   in Loop: Header=BB3_898 Depth=3
	leal	2(%rdx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jl	.LBB3_916
# BB#915:                               #   in Loop: Header=BB3_898 Depth=3
	movl	(%rsi), %edi
	movq	104(%rsp), %rsi         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_writen
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
	xorl	%edx, %edx
.LBB3_916:                              # %html_output_flush.exit.i42.i
                                        #   in Loop: Header=BB3_898 Depth=3
	movslq	%edx, %rax
	movw	$8765, 4(%rsi,%rax)     # imm = 0x223D
	addl	$2, 8196(%rsi)
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r13,8), %rbp
.LBB3_917:                              # %html_output_str.exit43.i
                                        #   in Loop: Header=BB3_898 Depth=3
	movq	%rbp, %rdi
	callq	strlen
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rax, %r15
	testl	%r15d, %r15d
	jle	.LBB3_924
# BB#918:                               # %.lr.ph.i1527
                                        #   in Loop: Header=BB3_898 Depth=3
	callq	__ctype_tolower_loc
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rax, %r14
	testq	%rsi, %rsi
	je	.LBB3_928
# BB#919:                               # %.lr.ph.split.preheader.i
                                        #   in Loop: Header=BB3_898 Depth=3
	movl	%r15d, %r15d
	decq	%r15
	xorl	%ebx, %ebx
	jmp	.LBB3_921
	.p2align	4, 0x90
.LBB3_920:                              # %html_output_c.exit45..lr.ph.split_crit_edge.i
                                        #   in Loop: Header=BB3_921 Depth=4
	incq	%rbx
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r13,8), %rbp
.LBB3_921:                              # %.lr.ph.split.i
                                        #   Parent Loop BB3_30 Depth=1
                                        #     Parent Loop BB3_989 Depth=2
                                        #       Parent Loop BB3_898 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%r14), %rax
	movzbl	(%rbp,%rbx), %ecx
	movzbl	(%rax,%rcx,4), %ebp
	movl	8196(%rsi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_923
# BB#922:                               # %html_output_flush.exit.i44.i
                                        #   in Loop: Header=BB3_921 Depth=4
	movl	(%rsi), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	104(%rsp), %rsi         # 8-byte Reload
	callq	cli_writen
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
	xorl	%eax, %eax
.LBB3_923:                              # %html_output_c.exit45.i
                                        #   in Loop: Header=BB3_921 Depth=4
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rsi)
	cltq
	movb	%bpl, 4(%rsi,%rax)
	cmpq	%rbx, %r15
	jne	.LBB3_920
.LBB3_924:                              # %._crit_edge.i
                                        #   in Loop: Header=BB3_898 Depth=3
	testq	%rsi, %rsi
	je	.LBB3_928
# BB#925:                               #   in Loop: Header=BB3_898 Depth=3
	movl	8196(%rsi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_927
# BB#926:                               # %html_output_flush.exit.i46.i
                                        #   in Loop: Header=BB3_898 Depth=3
	movl	(%rsi), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	104(%rsp), %rsi         # 8-byte Reload
	callq	cli_writen
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
	xorl	%eax, %eax
.LBB3_927:                              #   in Loop: Header=BB3_898 Depth=3
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rsi)
	cltq
	movb	$34, 4(%rsi,%rax)
.LBB3_928:                              # %html_output_c.exit47.i
                                        #   in Loop: Header=BB3_898 Depth=3
	incq	%r13
	cmpq	192(%rsp), %r13         # 8-byte Folded Reload
	jl	.LBB3_898
.LBB3_929:                              # %html_output_str.exit._crit_edge.i
                                        #   in Loop: Header=BB3_989 Depth=2
	testq	%rsi, %rsi
	je	.LBB3_935
# BB#930:                               #   in Loop: Header=BB3_989 Depth=2
	movl	8196(%rsi), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	movq	272(%rsp), %r13         # 8-byte Reload
	jne	.LBB3_932
# BB#931:                               # %html_output_flush.exit.i48.i
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	(%rsi), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	104(%rsp), %rsi         # 8-byte Reload
	callq	cli_writen
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	$0, 8196(%rsi)
	xorl	%eax, %eax
.LBB3_932:                              #   in Loop: Header=BB3_989 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rsi)
	cltq
	movb	$62, 4(%rsi,%rax)
.LBB3_933:                              # %html_output_tag.exit
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	32(%rsp), %r14          # 8-byte Reload
.LBB3_934:                              # %html_output_tag.exit
                                        #   in Loop: Header=BB3_989 Depth=2
	leaq	224(%rsp), %rdi
	callq	html_tag_arg_free
	movl	$5, %r15d
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	44(%rsp), %r8d          # 4-byte Reload
	jmp	.LBB3_998
.LBB3_935:                              #   in Loop: Header=BB3_989 Depth=2
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	272(%rsp), %r13         # 8-byte Reload
	jmp	.LBB3_934
.LBB3_936:                              #   in Loop: Header=BB3_989 Depth=2
	movq	%r14, %rbp
	movl	$1, %r13d
	jmp	.LBB3_984
.LBB3_937:                              # %html_tag_arg_value.exit1588
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%r14,%rbp,8), %rbx
	testq	%rbx, %rbx
	je	.LBB3_865
# BB#938:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$0, (%rbx)
	je	.LBB3_865
# BB#939:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.42, %esi
	movq	128(%rsp), %r15         # 8-byte Reload
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	html_tag_arg_add
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	je	.LBB3_942
# BB#940:                               #   in Loop: Header=BB3_989 Depth=2
	movl	4(%r15), %eax
	testl	%eax, %eax
	je	.LBB3_942
# BB#941:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	(%r15), %r14
	callq	blobCreate
	movq	24(%r15), %rcx
	movq	%rax, -8(%rcx,%r14,8)
	movq	16(%r15), %rax
	movq	24(%r15), %rcx
	movq	-8(%rcx,%r14,8), %r13
	movslq	28(%rsp), %rcx          # 4-byte Folded Reload
	movq	-8(%rax,%rcx,8), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%r13, %rdi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	blobAddData
	movq	24(%r15), %rax
	movq	-8(%rax,%r14,8), %rdi
	movl	$.L.str.52, %esi
	movl	$1, %edx
	callq	blobAddData
	movq	24(%r15), %rax
	movq	-8(%rax,%r14,8), %rdi
	callq	blobClose
.LBB3_942:                              #   in Loop: Header=BB3_989 Depth=2
	movq	96(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB3_988
# BB#943:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.37, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	html_tag_arg_add
	callq	blobCreate
	movq	24(%r15), %rcx
	movslq	(%r15), %r14
	movq	%rax, -8(%rcx,%r14,8)
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jle	.LBB3_947
# BB#944:                               #   in Loop: Header=BB3_989 Depth=2
	movq	24(%r15), %rax
	movq	-8(%rax,%r14,8), %rdi
	callq	blobGetDataSize
	cmpq	$1024, %rax             # imm = 0x400
	ja	.LBB3_947
# BB#945:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$1024, %ecx             # imm = 0x400
	subq	%rax, %rcx
	je	.LBB3_947
# BB#946:                               #   in Loop: Header=BB3_989 Depth=2
	movq	24(%r15), %rax
	movq	-8(%rax,%r14,8), %rdi
	cmpq	%rbx, %rcx
	cmovbq	%rcx, %rbx
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	callq	blobAddData
.LBB3_947:                              # %html_tag_contents_append.exit1590
                                        #   in Loop: Header=BB3_989 Depth=2
	movslq	(%r15), %rbx
	movq	24(%r15), %rax
	movq	-8(%rax,%rbx,8), %rdi
	movl	$.L.str.52, %esi
	movl	$1, %edx
	callq	blobAddData
	movq	24(%r15), %rax
	jmp	.LBB3_966
.LBB3_948:                              #   in Loop: Header=BB3_989 Depth=2
	movl	$1, %r13d
	jmp	.LBB3_934
.LBB3_949:                              #   in Loop: Header=BB3_989 Depth=2
	movq	(%r15,%rbp,8), %r13
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	je	.LBB3_970
.LBB3_950:                              # %html_tag_arg_value.exit1554
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB3_970
# BB#951:                               #   in Loop: Header=BB3_989 Depth=2
	cmpq	296(%rsp), %rax         # 8-byte Folded Reload
	jae	.LBB3_968
# BB#952:                               #   in Loop: Header=BB3_989 Depth=2
	movq	24(%r14), %rax
	movslq	28(%rsp), %rbp          # 4-byte Folded Reload
	movq	-8(%rax,%rbp,8), %rdi
	decq	%rbp
	callq	blobGetDataSize
	cmpq	$1024, %rax             # imm = 0x400
	ja	.LBB3_969
# BB#953:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$1024, %ecx             # imm = 0x400
	subq	%rax, %rcx
	je	.LBB3_969
# BB#954:                               #   in Loop: Header=BB3_989 Depth=2
	movq	296(%rsp), %rdx         # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	subq	%rsi, %rdx
	movq	24(%r14), %rax
	movq	(%rax,%rbp,8), %rdi
	cmpq	%rdx, %rcx
	cmovbq	%rcx, %rdx
	callq	blobAddData
	jmp	.LBB3_969
.LBB3_955:                              # %html_tag_arg_value.exit1596
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	(%r14,%rbp,8), %rbx
	testq	%rbx, %rbx
	je	.LBB3_865
# BB#956:                               #   in Loop: Header=BB3_989 Depth=2
	cmpb	$0, (%rbx)
	je	.LBB3_865
# BB#957:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.43, %esi
	movq	128(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	html_tag_arg_add
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	je	.LBB3_960
# BB#958:                               #   in Loop: Header=BB3_989 Depth=2
	movl	4(%rbp), %eax
	testl	%eax, %eax
	je	.LBB3_960
# BB#959:                               #   in Loop: Header=BB3_989 Depth=2
	movslq	(%rbp), %r14
	callq	blobCreate
	movq	24(%rbp), %rcx
	movq	%rax, -8(%rcx,%r14,8)
	movq	16(%rbp), %rax
	movq	24(%rbp), %rcx
	movq	-8(%rcx,%r14,8), %r13
	movslq	28(%rsp), %rcx          # 4-byte Folded Reload
	movq	-8(%rax,%rcx,8), %r15
	movq	%r15, %rdi
	callq	strlen
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	blobAddData
	movq	24(%rbp), %rax
	movq	-8(%rax,%r14,8), %rdi
	movl	$.L.str.52, %esi
	movl	$1, %edx
	callq	blobAddData
	movq	24(%rbp), %rax
	movq	-8(%rax,%r14,8), %rdi
	callq	blobClose
.LBB3_960:                              #   in Loop: Header=BB3_989 Depth=2
	movq	96(%rsp), %r14          # 8-byte Reload
	testq	%r14, %r14
	je	.LBB3_988
# BB#961:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.37, %esi
	movq	128(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	html_tag_arg_add
	callq	blobCreate
	movq	24(%rbp), %rcx
	movslq	(%rbp), %rbp
	movq	%rax, -8(%rcx,%rbp,8)
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jle	.LBB3_965
# BB#962:                               #   in Loop: Header=BB3_989 Depth=2
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	24(%rax), %rax
	movq	-8(%rax,%rbp,8), %rdi
	callq	blobGetDataSize
	cmpq	$1024, %rax             # imm = 0x400
	ja	.LBB3_965
# BB#963:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$1024, %ecx             # imm = 0x400
	subq	%rax, %rcx
	je	.LBB3_965
# BB#964:                               #   in Loop: Header=BB3_989 Depth=2
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	24(%rax), %rax
	movq	-8(%rax,%rbp,8), %rdi
	cmpq	%rbx, %rcx
	cmovbq	%rcx, %rbx
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	callq	blobAddData
.LBB3_965:                              # %html_tag_contents_append.exit1598
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	128(%rsp), %rbp         # 8-byte Reload
	movslq	(%rbp), %rbx
	movq	24(%rbp), %rax
	movq	-8(%rax,%rbx,8), %rdi
	movl	$.L.str.52, %esi
	movl	$1, %edx
	callq	blobAddData
	movq	24(%rbp), %rax
.LBB3_966:                              # %html_output_tag.exit
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	-8(%rax,%rbx,8), %rdi
	callq	blobClose
	jmp	.LBB3_865
.LBB3_967:                              #   in Loop: Header=BB3_989 Depth=2
	movl	$1, %r13d
	jmp	.LBB3_934
.LBB3_968:                              # %.html_tag_contents_append.exit1556_crit_edge
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	28(%rsp), %eax          # 4-byte Reload
	decl	%eax
	movslq	%eax, %rbp
.LBB3_969:                              # %html_tag_contents_append.exit1556
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	%r14, %rbx
	movq	24(%rbx), %rax
	movq	(%rax,%rbp,8), %rdi
	movl	$.L.str.52, %esi
	movl	$1, %edx
	callq	blobAddData
	movq	24(%rbx), %rax
	movq	(%rax,%rbp,8), %rdi
	callq	blobClose
	movl	$0, 28(%rsp)            # 4-byte Folded Spill
.LBB3_970:                              #   in Loop: Header=BB3_989 Depth=2
	testq	%r13, %r13
	je	.LBB3_976
# BB#971:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.36, %esi
	movq	%r14, %rbp
	movq	%rbp, %rdi
	movq	%r13, %rdx
	callq	html_tag_arg_add
	callq	blobCreate
	movq	24(%rbp), %rcx
	movslq	(%rbp), %r13
	movq	%rax, -8(%rcx,%r13,8)
	movq	80(%rsp), %rdi          # 8-byte Reload
	callq	strlen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jle	.LBB3_975
# BB#972:                               #   in Loop: Header=BB3_989 Depth=2
	movq	24(%rbp), %rax
	movq	-8(%rax,%r13,8), %rdi
	callq	blobGetDataSize
	cmpq	$1024, %rax             # imm = 0x400
	ja	.LBB3_975
# BB#973:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$1024, %ecx             # imm = 0x400
	subq	%rax, %rcx
	je	.LBB3_975
# BB#974:                               #   in Loop: Header=BB3_989 Depth=2
	movq	24(%rbp), %rax
	movq	-8(%rax,%r13,8), %rdi
	cmpq	%rbx, %rcx
	cmovbq	%rcx, %rbx
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	callq	blobAddData
.LBB3_975:                              # %html_tag_contents_append.exit1558
                                        #   in Loop: Header=BB3_989 Depth=2
	movslq	(%rbp), %rbx
	movq	24(%rbp), %rax
	movq	-8(%rax,%rbx,8), %rdi
	movl	$.L.str.52, %esi
	movl	$1, %edx
	callq	blobAddData
	movq	24(%rbp), %rax
	movq	-8(%rax,%rbx,8), %rdi
	callq	blobClose
.LBB3_976:                              #   in Loop: Header=BB3_989 Depth=2
	movq	96(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB3_982
# BB#977:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.37, %esi
	movq	%r14, %rbp
	movq	%rbp, %rdi
	movq	80(%rsp), %rdx          # 8-byte Reload
	callq	html_tag_arg_add
	callq	blobCreate
	movq	24(%rbp), %rcx
	movslq	(%rbp), %r14
	movq	%rax, -8(%rcx,%r14,8)
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	movl	$1, %r13d
	jle	.LBB3_981
# BB#978:                               #   in Loop: Header=BB3_989 Depth=2
	movq	24(%rbp), %rax
	movq	-8(%rax,%r14,8), %rdi
	callq	blobGetDataSize
	cmpq	$1024, %rax             # imm = 0x400
	ja	.LBB3_981
# BB#979:                               #   in Loop: Header=BB3_989 Depth=2
	movl	$1024, %ecx             # imm = 0x400
	subq	%rax, %rcx
	je	.LBB3_981
# BB#980:                               #   in Loop: Header=BB3_989 Depth=2
	movq	24(%rbp), %rax
	movq	-8(%rax,%r14,8), %rdi
	cmpq	%rbx, %rcx
	cmovbq	%rcx, %rbx
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	callq	blobAddData
.LBB3_981:                              # %html_tag_contents_append.exit1560
                                        #   in Loop: Header=BB3_989 Depth=2
	movslq	(%rbp), %rbx
	movq	24(%rbp), %rax
	movq	-8(%rax,%rbx,8), %rdi
	movl	$.L.str.52, %esi
	movl	$1, %edx
	callq	blobAddData
	movq	24(%rbp), %rax
	movq	-8(%rax,%rbx,8), %rdi
	callq	blobClose
	jmp	.LBB3_983
.LBB3_982:                              #   in Loop: Header=BB3_989 Depth=2
	movq	%r14, %rbp
	movl	$1, %r13d
.LBB3_983:                              #   in Loop: Header=BB3_989 Depth=2
	movq	80(%rsp), %rdx          # 8-byte Reload
.LBB3_984:                              #   in Loop: Header=BB3_989 Depth=2
	movl	$.L.str.34, %esi
	movq	%rbp, %rdi
	callq	html_tag_arg_add
	cmpl	$0, 4(%rbp)
	movq	32(%rsp), %r14          # 8-byte Reload
	je	.LBB3_934
# BB#985:                               #   in Loop: Header=BB3_989 Depth=2
	movl	(%rbp), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	callq	blobCreate
	movq	24(%rbp), %rcx
	movslq	(%rbp), %rdx
	movq	%rax, -8(%rcx,%rdx,8)
	movq	%r12, %r14
	jmp	.LBB3_934
.LBB3_986:                              #   in Loop: Header=BB3_989 Depth=2
	xorl	%eax, %eax
.LBB3_987:                              # %html_output_tag.exit
                                        #   in Loop: Header=BB3_989 Depth=2
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	$1, %r13d
	jmp	.LBB3_934
.LBB3_988:                              #   in Loop: Header=BB3_989 Depth=2
	xorl	%eax, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	jmp	.LBB3_865
	.p2align	4, 0x90
.LBB3_989:                              # %.lr.ph1947
                                        #   Parent Loop BB3_30 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_759 Depth 3
                                        #       Child Loop BB3_754 Depth 3
                                        #       Child Loop BB3_849 Depth 3
                                        #       Child Loop BB3_624 Depth 3
                                        #       Child Loop BB3_863 Depth 3
                                        #       Child Loop BB3_289 Depth 3
                                        #       Child Loop BB3_813 Depth 3
                                        #       Child Loop BB3_673 Depth 3
                                        #       Child Loop BB3_822 Depth 3
                                        #       Child Loop BB3_831 Depth 3
                                        #       Child Loop BB3_834 Depth 3
                                        #       Child Loop BB3_836 Depth 3
                                        #       Child Loop BB3_839 Depth 3
                                        #       Child Loop BB3_435 Depth 3
                                        #       Child Loop BB3_669 Depth 3
                                        #       Child Loop BB3_845 Depth 3
                                        #       Child Loop BB3_898 Depth 3
                                        #         Child Loop BB3_921 Depth 4
                                        #       Child Loop BB3_599 Depth 3
                                        #       Child Loop BB3_681 Depth 3
                                        #       Child Loop BB3_80 Depth 3
                                        #       Child Loop BB3_636 Depth 3
                                        #       Child Loop BB3_419 Depth 3
                                        #       Child Loop BB3_709 Depth 3
	cmpb	$10, %bl
	jne	.LBB3_992
# BB#990:                               # %.lr.ph1947
                                        #   in Loop: Header=BB3_989 Depth=2
	testl	%r8d, %r8d
	jne	.LBB3_992
# BB#991:                               #   in Loop: Header=BB3_989 Depth=2
	movb	$32, (%r12)
	jmp	.LBB3_995
	.p2align	4, 0x90
.LBB3_992:                              #   in Loop: Header=BB3_989 Depth=2
	cmpb	$13, %bl
	jne	.LBB3_37
# BB#993:                               #   in Loop: Header=BB3_989 Depth=2
	testl	%r8d, %r8d
	jne	.LBB3_37
# BB#994:                               #   in Loop: Header=BB3_989 Depth=2
	incq	%r12
.LBB3_995:                              # %.critedge.backedge
                                        #   in Loop: Header=BB3_989 Depth=2
	xorl	%r8d, %r8d
.LBB3_996:                              # %.critedge.backedge
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	%r15d, %r13d
.LBB3_997:                              # %.critedge.backedge
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	%ebp, %r15d
.LBB3_998:                              # %.critedge.backedge
                                        #   in Loop: Header=BB3_989 Depth=2
	movl	%r15d, %ebp
	movb	(%r12), %bl
	testb	%bl, %bl
	movl	%r13d, %r15d
	jne	.LBB3_989
.LBB3_999:                              # %.critedge._crit_edge
                                        #   in Loop: Header=BB3_30 Depth=1
	movq	%r13, %r14
	movl	%r8d, %r13d
	movq	128(%rsp), %rbx         # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB3_1007
# BB#1000:                              #   in Loop: Header=BB3_30 Depth=1
	cmpq	%r12, 32(%rsp)          # 8-byte Folded Reload
	jae	.LBB3_1007
# BB#1001:                              #   in Loop: Header=BB3_30 Depth=1
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB3_1007
# BB#1002:                              #   in Loop: Header=BB3_30 Depth=1
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	je	.LBB3_1007
# BB#1003:                              #   in Loop: Header=BB3_30 Depth=1
	movl	4(%rbx), %eax
	testl	%eax, %eax
	je	.LBB3_1007
# BB#1004:                              #   in Loop: Header=BB3_30 Depth=1
	movq	24(%rbx), %rax
	movslq	28(%rsp), %r15          # 4-byte Folded Reload
	movq	-8(%rax,%r15,8), %rdi
	callq	blobGetDataSize
	cmpq	$1024, %rax             # imm = 0x400
	ja	.LBB3_1007
# BB#1005:                              #   in Loop: Header=BB3_30 Depth=1
	movl	$1024, %ecx             # imm = 0x400
	subq	%rax, %rcx
	je	.LBB3_1007
# BB#1006:                              #   in Loop: Header=BB3_30 Depth=1
	decq	%r15
	movq	32(%rsp), %rsi          # 8-byte Reload
	subq	%rsi, %r12
	movq	24(%rbx), %rax
	movq	(%rax,%r15,8), %rdi
	cmpq	%r12, %rcx
	cmovbq	%rcx, %r12
	movq	%r12, %rdx
	callq	blobAddData
	.p2align	4, 0x90
.LBB3_1007:                             # %html_tag_contents_append.exit1712
                                        #   in Loop: Header=BB3_30 Depth=1
	movq	328(%rsp), %rdi         # 8-byte Reload
	callq	free
	movl	116(%rsp), %r12d        # 4-byte Reload
	testb	%r12b, %r12b
	je	.LBB3_1009
# BB#1008:                              #   in Loop: Header=BB3_30 Depth=1
	movl	$8192, %ecx             # imm = 0x2000
	leaq	336(%rsp), %rdi
	movq	216(%rsp), %rsi         # 8-byte Reload
	movq	264(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdx
	callq	encoding_norm_readline
	jmp	.LBB3_1010
	.p2align	4, 0x90
.LBB3_1009:                             #   in Loop: Header=BB3_30 Depth=1
	movl	$8192, %edx             # imm = 0x2000
	movq	216(%rsp), %rdi         # 8-byte Reload
	movq	264(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rsi
	callq	cli_readline
.LBB3_1010:                             # %.backedge
                                        #   in Loop: Header=BB3_30 Depth=1
	movq	%rax, %r15
	testq	%r15, %r15
	movl	%r13d, %r8d
	movq	%r14, %r13
	jne	.LBB3_30
	jmp	.LBB3_1013
.LBB3_1011:
	xorl	%r14d, %r14d
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB3_1070:
	movl	116(%rsp), %r12d        # 4-byte Reload
	jmp	.LBB3_1071
.LBB3_1012:
	xorl	%eax, %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	$0, 28(%rsp)            # 4-byte Folded Spill
.LBB3_1013:                             # %._crit_edge1988
	movl	$1, %r14d
	testb	%r12b, %r12b
	je	.LBB3_1071
# BB#1014:
	movq	120(%rsp), %rbp         # 8-byte Reload
	movb	$0, 2592(%rsp,%rbp)
	leaq	336(%rsp), %rdi
	leaq	2592(%rsp), %r13
	movq	%r13, %rsi
	callq	entity_norm
	movq	%rax, %r12
	testq	%r12, %r12
	movq	128(%rsp), %r15         # 8-byte Reload
	je	.LBB3_1030
# BB#1015:                              # %.preheader
	movb	(%r12), %r15b
	testb	%r15b, %r15b
	je	.LBB3_1058
# BB#1016:                              # %.lr.ph1885
	callq	__ctype_tolower_loc
	movq	%rax, %r13
	movq	(%rsp), %rdx            # 8-byte Reload
	leaq	4(%rdx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB3_1052
# BB#1017:                              # %.lr.ph1885.split.preheader
	leaq	4(%rax), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	$1, %ebp
	jmp	.LBB3_1019
	.p2align	4, 0x90
.LBB3_1018:                             # %html_output_c.exit1715..lr.ph1885.split_crit_edge
                                        #   in Loop: Header=BB3_1019 Depth=1
	movzbl	(%r12,%rbp), %r15d
	incq	%rbp
	movq	(%rsp), %rdx            # 8-byte Reload
.LBB3_1019:                             # %.lr.ph1885.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movzbl	%r15b, %ecx
	movzbl	(%rax,%rcx,4), %r15d
	movq	16(%rsp), %rbx          # 8-byte Reload
	movl	8196(%rbx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_1021
# BB#1020:                              # %html_output_flush.exit.i1713
                                        #   in Loop: Header=BB3_1019 Depth=1
	movl	(%rbx), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	80(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	$0, 8196(%rbx)
	xorl	%eax, %eax
.LBB3_1021:                             #   in Loop: Header=BB3_1019 Depth=1
	leal	1(%rax), %ecx
	testq	%rdx, %rdx
	movl	%ecx, 8196(%rbx)
	cltq
	movb	%r15b, 4(%rbx,%rax)
	je	.LBB3_1025
# BB#1022:                              #   in Loop: Header=BB3_1019 Depth=1
	movl	8196(%rdx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_1024
# BB#1023:                              # %html_output_flush.exit11.i1714
                                        #   in Loop: Header=BB3_1019 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	$0, 8196(%rdx)
	xorl	%eax, %eax
.LBB3_1024:                             #   in Loop: Header=BB3_1019 Depth=1
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdx)
	cltq
	movb	%r15b, 4(%rdx,%rax)
.LBB3_1025:                             # %html_output_c.exit1715
                                        #   in Loop: Header=BB3_1019 Depth=1
	movq	%r12, %rdi
	callq	strlen
	cmpq	%rax, %rbp
	jb	.LBB3_1018
	jmp	.LBB3_1058
.LBB3_1071:
	movq	176(%rsp), %r13         # 8-byte Reload
.LBB3_1072:                             # %.loopexit
	movq	96(%rsp), %rdi          # 8-byte Reload
	movl	28(%rsp), %ebx          # 4-byte Reload
	movq	128(%rsp), %r15         # 8-byte Reload
	testq	%rdi, %rdi
	jne	.LBB3_1073
	jmp	.LBB3_1074
.LBB3_1026:
	movq	%rbx, %rdi
	jmp	.LBB3_1028
.LBB3_1027:
	movq	160(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	%r15, %rdi
.LBB3_1028:
	callq	free
.LBB3_1029:
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	testb	%r12b, %r12b
	jne	.LBB3_1077
	jmp	.LBB3_1078
.LBB3_1030:
	testq	%rbp, %rbp
	movq	96(%rsp), %rdi          # 8-byte Reload
	movl	28(%rsp), %ebx          # 4-byte Reload
	je	.LBB3_1061
# BB#1031:
	movq	16(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	movq	(%rsp), %rdx            # 8-byte Reload
	je	.LBB3_1035
# BB#1032:
	movl	8196(%rbx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_1034
# BB#1033:                              # %html_output_flush.exit.i1716
	movl	(%rbx), %edi
	leaq	4(%rbx), %rsi
	movl	$8192, %edx             # imm = 0x2000
	callq	cli_writen
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	$0, 8196(%rbx)
	xorl	%eax, %eax
.LBB3_1034:
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rbx)
	cltq
	movb	$38, 4(%rbx,%rax)
.LBB3_1035:
	testq	%rdx, %rdx
	je	.LBB3_1039
# BB#1036:
	movl	8196(%rdx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_1038
# BB#1037:                              # %html_output_flush.exit11.i1717
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	4(%rax), %rsi
	movl	$8192, %edx             # imm = 0x2000
	callq	cli_writen
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	$0, 8196(%rdx)
	xorl	%eax, %eax
.LBB3_1038:
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdx)
	cltq
	movb	$38, 4(%rdx,%rax)
.LBB3_1039:                             # %.lr.ph
	callq	__ctype_tolower_loc
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	4(%rdx), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB3_1063
# BB#1040:                              # %.lr.ph.split.preheader
	leaq	4(%rax), %r12
	movq	16(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_1041:                             # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movzbl	(%r13), %ecx
	movzbl	(%rax,%rcx,4), %ebx
	movl	8196(%r15), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_1043
# BB#1042:                              # %html_output_flush.exit.i1719
                                        #   in Loop: Header=BB3_1041 Depth=1
	movl	(%r15), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	%r12, %rsi
	callq	cli_writen
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	$0, 8196(%r15)
	xorl	%eax, %eax
.LBB3_1043:                             #   in Loop: Header=BB3_1041 Depth=1
	leal	1(%rax), %ecx
	testq	%rdx, %rdx
	movl	%ecx, 8196(%r15)
	cltq
	movb	%bl, 4(%r15,%rax)
	je	.LBB3_1047
# BB#1044:                              #   in Loop: Header=BB3_1041 Depth=1
	movl	8196(%rdx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_1046
# BB#1045:                              # %html_output_flush.exit11.i1720
                                        #   in Loop: Header=BB3_1041 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	80(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	$0, 8196(%rdx)
	xorl	%eax, %eax
.LBB3_1046:                             #   in Loop: Header=BB3_1041 Depth=1
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdx)
	cltq
	movb	%bl, 4(%rdx,%rax)
.LBB3_1047:                             # %html_output_c.exit1721
                                        #   in Loop: Header=BB3_1041 Depth=1
	incq	%r13
	decq	%rbp
	jne	.LBB3_1041
	jmp	.LBB3_1059
.LBB3_1048:
	leaq	4672(%rsp), %rsi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	callq	free
	movq	%r15, %rdi
	jmp	.LBB3_1050
.LBB3_1049:
	leaq	4672(%rsp), %rsi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	160(%rsp), %r15         # 8-byte Reload
	movl	(%r15), %edi
	callq	close
	movq	%r15, %rdi
	callq	free
	movq	%rbx, %rdi
.LBB3_1050:                             # %.thread1742
	callq	free
	movq	176(%rsp), %rdi         # 8-byte Reload
.LBB3_1051:                             # %.thread1742
	callq	free
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testb	%r12b, %r12b
	jne	.LBB3_1077
	jmp	.LBB3_1078
.LBB3_1052:                             # %.lr.ph1885.split.us.preheader
	movl	$1, %ebp
	testq	%rdx, %rdx
	je	.LBB3_1057
.LBB3_1054:
	movq	(%r13), %rax
	movzbl	%r15b, %ecx
	movb	(%rax,%rcx,4), %bl
	movl	8196(%rdx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_1056
# BB#1055:                              # %html_output_flush.exit11.i1714.us
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	$0, 8196(%rdx)
	xorl	%eax, %eax
.LBB3_1056:
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdx)
	cltq
	movb	%bl, 4(%rdx,%rax)
	jmp	.LBB3_1057
	.p2align	4, 0x90
.LBB3_1053:                             # %html_output_c.exit1715.us..lr.ph1885.split.us_crit_edge
                                        #   in Loop: Header=BB3_1057 Depth=1
	movzbl	(%r12,%rbp), %r15d
	incq	%rbp
	movq	(%rsp), %rdx            # 8-byte Reload
	testq	%rdx, %rdx
	jne	.LBB3_1054
.LBB3_1057:                             # %html_output_c.exit1715.us
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	strlen
	cmpq	%rax, %rbp
	jb	.LBB3_1053
.LBB3_1058:                             # %._crit_edge
	movq	%r12, %rdi
	callq	free
.LBB3_1059:
	movl	116(%rsp), %r12d        # 4-byte Reload
	movq	176(%rsp), %r13         # 8-byte Reload
.LBB3_1060:                             # %.loopexit
	movq	128(%rsp), %r15         # 8-byte Reload
	movq	96(%rsp), %rdi          # 8-byte Reload
	movl	28(%rsp), %ebx          # 4-byte Reload
	testq	%rdi, %rdi
	je	.LBB3_1074
.LBB3_1073:
	callq	free
.LBB3_1074:
	testl	%ebx, %ebx
	je	.LBB3_1076
# BB#1075:
	movq	24(%r15), %rax
	movslq	%ebx, %rbx
	movq	-8(%rax,%rbx,8), %rdi
	movl	$.L.str.52, %esi
	movl	$1, %edx
	callq	blobAddData
	movq	24(%r15), %rax
	movq	-8(%rax,%rbx,8), %rdi
	callq	blobClose
.LBB3_1076:                             # %.thread1742
	testb	%r12b, %r12b
	je	.LBB3_1078
.LBB3_1077:
	leaq	336(%rsp), %rdi
	callq	entity_norm_done
.LBB3_1078:
	leaq	224(%rsp), %rdi
	callq	html_tag_arg_free
	cmpq	$0, 264(%rsp)           # 8-byte Folded Reload
	jne	.LBB3_1080
# BB#1079:
	movq	216(%rsp), %rdi         # 8-byte Reload
	callq	fclose
.LBB3_1080:
	movq	16(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	movq	8(%rsp), %rax           # 8-byte Reload
	je	.LBB3_1084
# BB#1081:
	movl	8196(%rbx), %edx
	testl	%edx, %edx
	jle	.LBB3_1083
# BB#1082:
	movl	(%rbx), %edi
	leaq	4(%rbx), %rsi
	callq	cli_writen
	movl	$0, 8196(%rbx)
.LBB3_1083:                             # %html_output_flush.exit1722
	movl	(%rbx), %edi
	callq	close
	movq	160(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB3_1084:
	movq	(%rsp), %rcx            # 8-byte Reload
	testq	%rcx, %rcx
	je	.LBB3_1088
# BB#1085:
	movl	8196(%rcx), %edx
	testl	%edx, %edx
	jle	.LBB3_1087
# BB#1086:
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	4(%rax), %rsi
	callq	cli_writen
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	$0, 8196(%rcx)
.LBB3_1087:                             # %html_output_flush.exit1723
	movl	(%rcx), %edi
	callq	close
	movq	208(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB3_1088:
	testq	%rax, %rax
	je	.LBB3_1092
# BB#1089:
	movl	8196(%rax), %edx
	testl	%edx, %edx
	jle	.LBB3_1091
# BB#1090:
	movl	(%rax), %edi
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	4(%rax), %rsi
	callq	cli_writen
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$0, 8196(%rax)
.LBB3_1091:                             # %html_output_flush.exit1724
	movl	(%rax), %edi
	callq	close
	movq	%r13, %rdi
	callq	free
.LBB3_1092:                             # %.loopexit1771
	movl	%r14d, %eax
	addq	$5704, %rsp             # imm = 0x1648
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_1061:
	movl	116(%rsp), %r12d        # 4-byte Reload
	movq	176(%rsp), %r13         # 8-byte Reload
	testq	%rdi, %rdi
	jne	.LBB3_1073
	jmp	.LBB3_1074
.LBB3_1062:
	leaq	4672(%rsp), %rsi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	160(%rsp), %r13         # 8-byte Reload
	movl	(%r13), %edi
	callq	close
	movl	(%r15), %edi
	callq	close
	movq	%r13, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	%rbx, %rdi
	jmp	.LBB3_1051
.LBB3_1063:                             # %.lr.ph.split.us.preheader
	leaq	2592(%rsp), %rbx
	movl	116(%rsp), %r12d        # 4-byte Reload
	movq	176(%rsp), %r13         # 8-byte Reload
.LBB3_1064:                             # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	testq	%rdx, %rdx
	je	.LBB3_1068
# BB#1065:                              #   in Loop: Header=BB3_1064 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movzbl	(%rbx), %ecx
	movzbl	(%rax,%rcx,4), %r15d
	movl	8196(%rdx), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB3_1067
# BB#1066:                              # %html_output_flush.exit11.i1720.us
                                        #   in Loop: Header=BB3_1064 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	movl	$8192, %edx             # imm = 0x2000
	movq	80(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	$0, 8196(%rdx)
	xorl	%eax, %eax
.LBB3_1067:                             #   in Loop: Header=BB3_1064 Depth=1
	leal	1(%rax), %ecx
	movl	%ecx, 8196(%rdx)
	cltq
	movb	%r15b, 4(%rdx,%rax)
	movq	176(%rsp), %r13         # 8-byte Reload
.LBB3_1068:                             # %html_output_c.exit1721.us
                                        #   in Loop: Header=BB3_1064 Depth=1
	incq	%rbx
	decq	%rbp
	jne	.LBB3_1064
	jmp	.LBB3_1060
.LBB3_1069:
	xorl	%r14d, %r14d
	jmp	.LBB3_1070
.LBB3_1093:
	xorl	%r14d, %r14d
	leaq	4672(%rsp), %rsi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rbp, %rdi
	callq	free
	movl	116(%rsp), %r12d        # 4-byte Reload
	jmp	.LBB3_1072
.LBB3_1094:
	movl	$-114, %r14d
	jmp	.LBB3_1092
.Lfunc_end3:
	.size	cli_html_normalise, .Lfunc_end3-cli_html_normalise
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_1011
	.quad	.LBB3_39
	.quad	.LBB3_62
	.quad	.LBB3_72
	.quad	.LBB3_74
	.quad	.LBB3_90
	.quad	.LBB3_91
	.quad	.LBB3_100
	.quad	.LBB3_115
	.quad	.LBB3_125
	.quad	.LBB3_136
	.quad	.LBB3_146
	.quad	.LBB3_160
	.quad	.LBB3_162
	.quad	.LBB3_163
	.quad	.LBB3_177
	.quad	.LBB3_179
	.quad	.LBB3_196
	.quad	.LBB3_197
	.quad	.LBB3_201
	.quad	.LBB3_213
	.quad	.LBB3_218
	.quad	.LBB3_222
	.quad	.LBB3_227
.LJTI3_1:
	.quad	.LBB3_215
	.quad	.LBB3_376
	.quad	.LBB3_376
	.quad	.LBB3_382
	.quad	.LBB3_820
	.quad	.LBB3_383
.LJTI3_2:
	.quad	.LBB3_402
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_184
	.quad	.LBB3_403
	.quad	.LBB3_761
	.quad	.LBB3_773
	.quad	.LBB3_403
	.quad	.LBB3_785
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_403
	.quad	.LBB3_797

	.text
	.globl	html_normalise_fd
	.p2align	4, 0x90
	.type	html_normalise_fd,@function
html_normalise_fd:                      # @html_normalise_fd
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 40
	subq	$168, %rsp
.Lcfi36:
	.cfi_def_cfa_offset 208
.Lcfi37:
	.cfi_offset %rbx, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movl	%edi, %ebp
	leaq	24(%rsp), %rdx
	movl	$1, %edi
	movl	%ebp, %esi
	callq	__fxstat
	testl	%eax, %eax
	je	.LBB4_1
# BB#4:
	movl	$.L.str.3, %edi
.LBB4_5:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	xorl	%esi, %esi
	movl	%ebp, %edi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	callq	cli_html_normalise
	movl	%eax, %ebx
	jmp	.LBB4_6
.LBB4_1:
	movq	72(%rsp), %rsi
	movq	%rsi, 8(%rsp)
	xorl	%edi, %edi
	movl	$1, %edx
	movl	$2, %ecx
	xorl	%r9d, %r9d
	movl	%ebp, %r8d
	callq	mmap
	movq	%rax, (%rsp)
	movq	$0, 16(%rsp)
	cmpq	$-1, %rax
	je	.LBB4_2
# BB#3:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rsp, %rsi
	movl	$-1, %edi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	callq	cli_html_normalise
	movl	%eax, %ebx
	movq	(%rsp), %rdi
	movq	8(%rsp), %rsi
	callq	munmap
.LBB4_6:
	movl	%ebx, %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_2:
	movl	$.L.str.1, %edi
	jmp	.LBB4_5
.Lfunc_end4:
	.size	html_normalise_fd, .Lfunc_end4-html_normalise_fd
	.cfi_endproc

	.globl	html_screnc_decode
	.p2align	4, 0x90
	.type	html_screnc_decode,@function
html_screnc_decode:                     # @html_screnc_decode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi45:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 56
	subq	$9256, %rsp             # imm = 0x2428
.Lcfi47:
	.cfi_def_cfa_offset 9312
.Lcfi48:
	.cfi_offset %rbx, -56
.Lcfi49:
	.cfi_offset %r12, -48
.Lcfi50:
	.cfi_offset %r13, -40
.Lcfi51:
	.cfi_offset %r14, -32
.Lcfi52:
	.cfi_offset %r15, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	lseek
	movl	%ebp, %edi
	callq	dup
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB5_54
# BB#1:
	movl	$.L.str.4, %esi
	movl	%ebp, %edi
	callq	fdopen
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB5_2
# BB#3:
	xorl	%r13d, %r13d
	leaq	8224(%rsp), %rbp
	movl	$1024, %esi             # imm = 0x400
	movl	$.L.str.5, %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rcx
	callq	snprintf
	movl	$577, %esi              # imm = 0x241
	movl	$384, %edx              # imm = 0x180
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	open
	movl	%eax, 24(%rsp)
	movl	$0, 8220(%rsp)
	testl	%eax, %eax
	jne	.LBB5_4
# BB#75:
	xorl	%r13d, %r13d
	leaq	8224(%rsp), %rsi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r14, %rdi
	callq	fclose
	jmp	.LBB5_54
	.p2align	4, 0x90
.LBB5_6:                                #   in Loop: Header=BB5_4 Depth=1
	movq	%r15, %rdi
	callq	free
	xorl	%r13d, %r13d
.LBB5_4:                                # %.preheader120
                                        # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	movl	$8192, %edx             # imm = 0x2000
	movq	%r14, %rdi
	callq	cli_readline
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB5_50
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB5_4 Depth=1
	movl	$.L.str.7, %esi
	movq	%r15, %rdi
	callq	strstr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB5_6
# BB#7:
	movb	4(%rbx), %al
	testb	%al, %al
	je	.LBB5_9
# BB#8:
	movb	%al, 16(%rsp)           # 1-byte Spill
	addq	$4, %rbx
	jmp	.LBB5_11
.LBB5_2:
	movl	%ebp, %edi
	jmp	.LBB5_53
.LBB5_9:
	movq	%r15, %rdi
	callq	free
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	movl	$8192, %edx             # imm = 0x2000
	movq	%r14, %rdi
	callq	cli_readline
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB5_50
# BB#10:                                # %._crit_edge
	movb	(%r15), %al
	movb	%al, 16(%rsp)           # 1-byte Spill
	movq	%r15, %rbx
.LBB5_11:
	movb	1(%rbx), %r12b
	testb	%r12b, %r12b
	je	.LBB5_55
# BB#12:
	incq	%rbx
	jmp	.LBB5_57
.LBB5_55:
	movq	%r15, %rdi
	callq	free
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	movl	$8192, %edx             # imm = 0x2000
	movq	%r14, %rdi
	callq	cli_readline
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB5_50
# BB#56:                                # %._crit_edge218
	movb	(%r15), %r12b
	movq	%r15, %rbx
.LBB5_57:
	movb	1(%rbx), %al
	testb	%al, %al
	je	.LBB5_59
# BB#58:
	incq	%rbx
	jmp	.LBB5_61
.LBB5_59:
	movq	%r15, %rdi
	callq	free
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	movl	$8192, %edx             # imm = 0x2000
	movq	%r14, %rdi
	callq	cli_readline
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB5_50
# BB#60:                                # %._crit_edge220
	movb	(%r15), %al
	movq	%r15, %rbx
.LBB5_61:
	movb	%al, 12(%rsp)           # 1-byte Spill
	movb	1(%rbx), %al
	testb	%al, %al
	je	.LBB5_63
# BB#62:
	incq	%rbx
	jmp	.LBB5_65
.LBB5_63:
	movq	%r15, %rdi
	callq	free
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	movl	$8192, %edx             # imm = 0x2000
	movq	%r14, %rdi
	callq	cli_readline
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB5_50
# BB#64:                                # %._crit_edge222
	movb	(%r15), %al
	movq	%r15, %rbx
.LBB5_65:
	movb	1(%rbx), %bpl
	testb	%bpl, %bpl
	je	.LBB5_67
# BB#66:
	incq	%rbx
	jmp	.LBB5_69
.LBB5_67:
	movb	%al, 11(%rsp)           # 1-byte Spill
	movq	%r15, %rdi
	callq	free
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	movl	$8192, %edx             # imm = 0x2000
	movq	%r14, %rdi
	callq	cli_readline
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB5_50
# BB#68:                                # %._crit_edge224
	movb	(%r15), %bpl
	movq	%r15, %rbx
	movb	11(%rsp), %al           # 1-byte Reload
.LBB5_69:
	movb	1(%rbx), %dil
	testb	%dil, %dil
	je	.LBB5_71
# BB#70:
	incq	%rbx
	jmp	.LBB5_73
.LBB5_71:
	movb	%al, 11(%rsp)           # 1-byte Spill
	movq	%r15, %rdi
	callq	free
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	movl	$8192, %edx             # imm = 0x2000
	movq	%r14, %rdi
	callq	cli_readline
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB5_50
# BB#72:                                # %._crit_edge226
	movb	(%r15), %dil
	movq	%r15, %rbx
	movb	11(%rsp), %al           # 1-byte Reload
.LBB5_73:
	movl	$1, %r13d
	testq	%r15, %r15
	je	.LBB5_50
# BB#74:
	movzbl	16(%rsp), %ecx          # 1-byte Folded Reload
	movl	base64_chars(,%rcx,4), %ecx
	shll	$2, %ecx
	movslq	%ecx, %rcx
	movzbl	%r12b, %edx
	movl	base64_chars(,%rdx,4), %edx
	movl	%edx, %esi
	sarl	$4, %esi
	movslq	%esi, %rsi
	addq	%rcx, %rsi
	andl	$15, %edx
	shll	$12, %edx
	addq	%rsi, %rdx
	movzbl	12(%rsp), %ecx          # 1-byte Folded Reload
	movl	base64_chars(,%rcx,4), %ecx
	movl	%ecx, %esi
	andl	$-4, %esi
	shll	$6, %esi
	movslq	%esi, %rsi
	addq	%rdx, %rsi
	andl	$3, %ecx
	shll	$22, %ecx
	addq	%rsi, %rcx
	movzbl	%al, %edx
	movl	base64_chars(,%rdx,4), %edx
	shll	$16, %edx
	movslq	%edx, %rdx
	addq	%rcx, %rdx
	movzbl	%bpl, %ecx
	movl	base64_chars(,%rcx,4), %ecx
	shll	$26, %ecx
	movslq	%ecx, %rcx
	addq	%rdx, %rcx
	movzbl	%dil, %eax
	movl	base64_chars(,%rax,4), %eax
	andl	$-16, %eax
	shll	$20, %eax
	movslq	%eax, %rbp
	addq	%rcx, %rbp
	je	.LBB5_50
# BB#13:                                # %.preheader.lr.ph.lr.ph
	incq	%rbx
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$2, 12(%rsp)            # 4-byte Folded Spill
	movl	$13, %eax
	testq	%rbp, %rbp
	jne	.LBB5_16
	jmp	.LBB5_15
	.p2align	4, 0x90
.LBB5_48:                               # %.outer118.loopexit
                                        #   in Loop: Header=BB5_16 Depth=1
	movl	%eax, %r12d
.LBB5_49:                               # %.outer118
                                        #   in Loop: Header=BB5_16 Depth=1
	movq	%r15, %rdi
	callq	free
	xorl	%esi, %esi
	movl	$8192, %edx             # imm = 0x2000
	movq	%r14, %rdi
	callq	cli_readline
	movq	%rax, %rbx
	movl	$1, %r13d
	testq	%rbx, %rbx
	movq	%rbx, %r15
	movl	%r12d, %eax
	jne	.LBB5_14
	jmp	.LBB5_50
.LBB5_26:                               # %.outer116.split.split.us
                                        #   in Loop: Header=BB5_16 Depth=1
	movl	$13, %r12d
	testb	%cl, %cl
	je	.LBB5_49
# BB#27:                                # %.us-lcssa143
                                        #   in Loop: Header=BB5_16 Depth=1
	incq	%rbx
	decl	12(%rsp)                # 4-byte Folded Spill
	movl	$13, %eax
	movl	$1, %ecx
	cmovel	%ecx, %eax
	.p2align	4, 0x90
.LBB5_14:                               # %.preheader.lr.ph
                                        #   in Loop: Header=BB5_16 Depth=1
	testq	%rbp, %rbp
	jne	.LBB5_16
	jmp	.LBB5_15
.LBB5_40:                               #   in Loop: Header=BB5_16 Depth=1
	movl	8220(%rsp), %eax
	movb	$13, %r13b
	cmpl	$8192, %eax             # imm = 0x2000
	je	.LBB5_45
	jmp	.LBB5_46
.LBB5_41:                               #   in Loop: Header=BB5_16 Depth=1
	movl	8220(%rsp), %eax
	movb	$64, %r13b
	cmpl	$8192, %eax             # imm = 0x2000
	je	.LBB5_45
	jmp	.LBB5_46
.LBB5_42:                               #   in Loop: Header=BB5_16 Depth=1
	movl	8220(%rsp), %eax
	movb	$10, %r13b
	cmpl	$8192, %eax             # imm = 0x2000
	je	.LBB5_45
	jmp	.LBB5_46
.LBB5_43:                               #   in Loop: Header=BB5_16 Depth=1
	movl	8220(%rsp), %eax
	movb	$62, %r13b
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB5_46
.LBB5_45:                               # %html_output_flush.exit.i97
                                        #   in Loop: Header=BB5_16 Depth=1
	movl	24(%rsp), %edi
	movl	$8192, %edx             # imm = 0x2000
	leaq	28(%rsp), %rsi
	callq	cli_writen
	movl	$0, 8220(%rsp)
	xorl	%eax, %eax
.LBB5_46:                               # %.sink.split
                                        #   in Loop: Header=BB5_16 Depth=1
	leal	1(%rax), %ecx
	movl	%ecx, 8220(%rsp)
	cltq
	movb	%r13b, 28(%rsp,%rax)
.LBB5_47:                               # %.outer112
                                        #   in Loop: Header=BB5_16 Depth=1
	incq	%rbx
	decq	%rbp
	movl	$1, %eax
	jne	.LBB5_16
	jmp	.LBB5_15
	.p2align	4, 0x90
.LBB5_23:                               # %.us-lcssa142
                                        #   in Loop: Header=BB5_16 Depth=1
	incq	%rbx
.LBB5_16:                               # %.outer116
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_20 Depth 2
	cmpl	$1, %eax
	je	.LBB5_30
# BB#17:                                # %.outer116
                                        #   in Loop: Header=BB5_16 Depth=1
	cmpl	$13, %eax
	je	.LBB5_24
# BB#18:                                # %.outer116
                                        #   in Loop: Header=BB5_16 Depth=1
	cmpl	$17, %eax
	jne	.LBB5_19
# BB#28:                                # %.outer116.split.split.split.us
                                        #   in Loop: Header=BB5_16 Depth=1
	movzbl	(%rbx), %ecx
	cmpq	$42, %rcx
	ja	.LBB5_47
# BB#29:                                # %.outer116.split.split.split.us
                                        #   in Loop: Header=BB5_16 Depth=1
	movl	$17, %r12d
	jmpq	*.LJTI5_0(,%rcx,8)
.LBB5_39:                               #   in Loop: Header=BB5_16 Depth=1
	movl	8220(%rsp), %eax
	movb	$60, %r13b
	cmpl	$8192, %eax             # imm = 0x2000
	je	.LBB5_45
	jmp	.LBB5_46
	.p2align	4, 0x90
.LBB5_30:                               # %.outer116.split.split.split.split.us
                                        #   in Loop: Header=BB5_16 Depth=1
	movzbl	(%rbx), %ecx
	cmpq	$10, %rcx
	je	.LBB5_23
# BB#31:                                # %.outer116.split.split.split.split.us
                                        #   in Loop: Header=BB5_16 Depth=1
	cmpb	$13, %cl
	je	.LBB5_23
# BB#32:                                # %.outer116.split.split.split.split.us
                                        #   in Loop: Header=BB5_16 Depth=1
	movl	$1, %r12d
	testb	%cl, %cl
	je	.LBB5_49
# BB#33:                                # %.us-lcssa145
                                        #   in Loop: Header=BB5_16 Depth=1
	movl	$1, %eax
	testb	%cl, %cl
	js	.LBB5_38
# BB#34:                                #   in Loop: Header=BB5_16 Depth=1
	movslq	16(%rsp), %rax          # 4-byte Folded Reload
	movslq	table_order(,%rax,4), %rax
	shlq	$9, %rax
	movl	decrypt_tables(%rax,%rcx,4), %r12d
	movl	$17, %eax
	cmpl	$255, %r12d
	je	.LBB5_38
# BB#35:                                #   in Loop: Header=BB5_16 Depth=1
	movl	8220(%rsp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB5_37
# BB#36:                                # %html_output_flush.exit.i99
                                        #   in Loop: Header=BB5_16 Depth=1
	movl	24(%rsp), %edi
	movl	$8192, %edx             # imm = 0x2000
	leaq	28(%rsp), %rsi
	callq	cli_writen
	movl	$0, 8220(%rsp)
	xorl	%eax, %eax
.LBB5_37:                               # %html_output_c.exit100
                                        #   in Loop: Header=BB5_16 Depth=1
	leal	1(%rax), %ecx
	movl	%ecx, 8220(%rsp)
	cltq
	movb	%r12b, 28(%rsp,%rax)
	movl	$1, %eax
.LBB5_38:                               # %.outer
                                        #   in Loop: Header=BB5_16 Depth=1
	incq	%rbx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %rsi
	leal	1(%rsi), %ecx
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$26, %edx
	leal	1(%rsi,%rdx), %edx
	andl	$-64, %edx
	subl	%edx, %ecx
	decq	%rbp
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	jne	.LBB5_16
	jmp	.LBB5_15
	.p2align	4, 0x90
.LBB5_24:                               # %.outer116.split.split.us
                                        #   in Loop: Header=BB5_16 Depth=1
	movb	(%rbx), %cl
	cmpb	$10, %cl
	je	.LBB5_23
# BB#25:                                # %.outer116.split.split.us
                                        #   in Loop: Header=BB5_16 Depth=1
	cmpb	$13, %cl
	je	.LBB5_23
	jmp	.LBB5_26
	.p2align	4, 0x90
.LBB5_19:                               # %.outer116.split.split.split.split.preheader
                                        #   in Loop: Header=BB5_16 Depth=1
	movb	(%rbx), %cl
	.p2align	4, 0x90
.LBB5_20:                               # %.outer116.split.split.split.split
                                        #   Parent Loop BB5_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	%cl, %cl
	je	.LBB5_48
# BB#21:                                # %.outer116.split.split.split.split
                                        #   in Loop: Header=BB5_20 Depth=2
	cmpb	$13, %cl
	je	.LBB5_23
# BB#22:                                # %.outer116.split.split.split.split
                                        #   in Loop: Header=BB5_20 Depth=2
	cmpb	$10, %cl
	jne	.LBB5_20
	jmp	.LBB5_23
.LBB5_15:                               # %.outer112.split.us
	movq	%r15, %rdi
	callq	free
	movl	$1, %r13d
.LBB5_50:                               # %.loopexit
	movq	%r14, %rdi
	callq	fclose
	movl	8220(%rsp), %edx
	testl	%edx, %edx
	jle	.LBB5_52
# BB#51:
	movl	24(%rsp), %edi
	leaq	28(%rsp), %rsi
	callq	cli_writen
	movl	$0, 8220(%rsp)
.LBB5_52:                               # %html_output_flush.exit
	movl	24(%rsp), %edi
.LBB5_53:
	callq	close
.LBB5_54:
	movl	%r13d, %eax
	addq	$9256, %rsp             # imm = 0x2428
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	html_screnc_decode, .Lfunc_end5-html_screnc_decode
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI5_0:
	.quad	.LBB5_49
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_23
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_23
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_39
	.quad	.LBB5_47
	.quad	.LBB5_40
	.quad	.LBB5_41
	.quad	.LBB5_47
	.quad	.LBB5_42
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_47
	.quad	.LBB5_43

	.text
	.p2align	4, 0x90
	.type	html_tag_arg_add,@function
html_tag_arg_add:                       # @html_tag_arg_add
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi58:
	.cfi_def_cfa_offset 48
.Lcfi59:
	.cfi_offset %rbx, -40
.Lcfi60:
	.cfi_offset %r12, -32
.Lcfi61:
	.cfi_offset %r14, -24
.Lcfi62:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	movslq	(%r12), %rax
	leaq	1(%rax), %rcx
	movl	%ecx, (%r12)
	movq	8(%r12), %rdi
	leaq	8(,%rax,8), %rsi
	callq	cli_realloc2
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.LBB6_11
# BB#1:
	movq	16(%r12), %rdi
	movslq	(%r12), %rsi
	shlq	$3, %rsi
	callq	cli_realloc2
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.LBB6_11
# BB#2:
	cmpl	$0, 4(%r12)
	je	.LBB6_5
# BB#3:
	movq	24(%r12), %rdi
	movslq	(%r12), %rsi
	shlq	$3, %rsi
	callq	cli_realloc2
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.LBB6_11
# BB#4:
	movslq	(%r12), %rcx
	movq	$0, -8(%rax,%rcx,8)
.LBB6_5:
	movq	%r15, %rdi
	callq	cli_strdup
	movq	8(%r12), %rdx
	movslq	(%r12), %rcx
	movq	%rax, -8(%rdx,%rcx,8)
	testq	%r14, %r14
	je	.LBB6_10
# BB#6:
	cmpb	$34, (%r14)
	jne	.LBB6_9
# BB#7:
	incq	%r14
	movq	%r14, %rdi
	callq	cli_strdup
	movq	16(%r12), %rcx
	movslq	(%r12), %rbx
	movq	%rax, -8(%rcx,%rbx,8)
	movq	%r14, %rdi
	callq	strlen
	testl	%eax, %eax
	jle	.LBB6_28
# BB#8:
	movq	16(%r12), %rcx
	movq	-8(%rcx,%rbx,8), %rcx
	shlq	$32, %rax
	movabsq	$-4294967296, %rdx      # imm = 0xFFFFFFFF00000000
	addq	%rax, %rdx
	sarq	$32, %rdx
	movb	$0, (%rcx,%rdx)
	jmp	.LBB6_28
.LBB6_11:
	movl	(%r12), %ecx
	leal	-1(%rcx), %eax
	movl	%eax, (%r12)
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	setne	%al
	cmpl	$2, %ecx
	jl	.LBB6_21
# BB#12:                                # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_13:                               # =>This Inner Loop Header: Depth=1
	testb	$1, %al
	je	.LBB6_15
# BB#14:                                #   in Loop: Header=BB6_13 Depth=1
	movq	(%rdi,%rbx,8), %rdi
	callq	free
.LBB6_15:                               #   in Loop: Header=BB6_13 Depth=1
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.LBB6_17
# BB#16:                                #   in Loop: Header=BB6_13 Depth=1
	movq	(%rax,%rbx,8), %rdi
	callq	free
.LBB6_17:                               #   in Loop: Header=BB6_13 Depth=1
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.LBB6_20
# BB#18:                                #   in Loop: Header=BB6_13 Depth=1
	movq	(%rax,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB6_20
# BB#19:                                #   in Loop: Header=BB6_13 Depth=1
	callq	blobDestroy
.LBB6_20:                               #   in Loop: Header=BB6_13 Depth=1
	incq	%rbx
	movslq	(%r12), %rcx
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	setne	%al
	cmpq	%rcx, %rbx
	jl	.LBB6_13
.LBB6_21:                               # %._crit_edge
	testb	%al, %al
	je	.LBB6_23
# BB#22:
	callq	free
.LBB6_23:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB6_25
# BB#24:
	callq	free
.LBB6_25:
	leaq	8(%r12), %rbx
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB6_27
# BB#26:
	callq	free
.LBB6_27:
	movl	$0, (%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movq	$0, 16(%rbx)
.LBB6_28:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB6_10:
	movq	16(%r12), %rax
	movq	$0, -8(%rax,%rcx,8)
	jmp	.LBB6_28
.LBB6_9:
	movq	%r14, %rdi
	callq	cli_strdup
	movq	16(%r12), %rcx
	movslq	(%r12), %rdx
	movq	%rax, -8(%rcx,%rdx,8)
	jmp	.LBB6_28
.Lfunc_end6:
	.size	html_tag_arg_add, .Lfunc_end6-html_tag_arg_add
	.cfi_endproc

	.type	table_order,@object     # @table_order
	.data
	.globl	table_order
	.p2align	4
table_order:
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.size	table_order, 256

	.type	decrypt_tables,@object  # @decrypt_tables
	.globl	decrypt_tables
	.p2align	4
decrypt_tables:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	87                      # 0x57
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	17                      # 0x11
	.long	18                      # 0x12
	.long	19                      # 0x13
	.long	20                      # 0x14
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	23                      # 0x17
	.long	24                      # 0x18
	.long	25                      # 0x19
	.long	26                      # 0x1a
	.long	27                      # 0x1b
	.long	28                      # 0x1c
	.long	29                      # 0x1d
	.long	30                      # 0x1e
	.long	31                      # 0x1f
	.long	46                      # 0x2e
	.long	71                      # 0x47
	.long	122                     # 0x7a
	.long	86                      # 0x56
	.long	66                      # 0x42
	.long	106                     # 0x6a
	.long	47                      # 0x2f
	.long	38                      # 0x26
	.long	73                      # 0x49
	.long	65                      # 0x41
	.long	52                      # 0x34
	.long	50                      # 0x32
	.long	91                      # 0x5b
	.long	118                     # 0x76
	.long	114                     # 0x72
	.long	67                      # 0x43
	.long	56                      # 0x38
	.long	57                      # 0x39
	.long	112                     # 0x70
	.long	69                      # 0x45
	.long	104                     # 0x68
	.long	113                     # 0x71
	.long	79                      # 0x4f
	.long	9                       # 0x9
	.long	98                      # 0x62
	.long	68                      # 0x44
	.long	35                      # 0x23
	.long	117                     # 0x75
	.long	60                      # 0x3c
	.long	126                     # 0x7e
	.long	62                      # 0x3e
	.long	94                      # 0x5e
	.long	255                     # 0xff
	.long	119                     # 0x77
	.long	74                      # 0x4a
	.long	97                      # 0x61
	.long	93                      # 0x5d
	.long	34                      # 0x22
	.long	75                      # 0x4b
	.long	111                     # 0x6f
	.long	78                      # 0x4e
	.long	59                      # 0x3b
	.long	76                      # 0x4c
	.long	80                      # 0x50
	.long	103                     # 0x67
	.long	42                      # 0x2a
	.long	125                     # 0x7d
	.long	116                     # 0x74
	.long	84                      # 0x54
	.long	43                      # 0x2b
	.long	45                      # 0x2d
	.long	44                      # 0x2c
	.long	48                      # 0x30
	.long	110                     # 0x6e
	.long	107                     # 0x6b
	.long	102                     # 0x66
	.long	53                      # 0x35
	.long	37                      # 0x25
	.long	33                      # 0x21
	.long	100                     # 0x64
	.long	77                      # 0x4d
	.long	82                      # 0x52
	.long	99                      # 0x63
	.long	63                      # 0x3f
	.long	123                     # 0x7b
	.long	120                     # 0x78
	.long	41                      # 0x29
	.long	40                      # 0x28
	.long	115                     # 0x73
	.long	89                      # 0x59
	.long	51                      # 0x33
	.long	127                     # 0x7f
	.long	109                     # 0x6d
	.long	85                      # 0x55
	.long	83                      # 0x53
	.long	124                     # 0x7c
	.long	58                      # 0x3a
	.long	95                      # 0x5f
	.long	101                     # 0x65
	.long	70                      # 0x46
	.long	88                      # 0x58
	.long	49                      # 0x31
	.long	105                     # 0x69
	.long	108                     # 0x6c
	.long	90                      # 0x5a
	.long	72                      # 0x48
	.long	39                      # 0x27
	.long	92                      # 0x5c
	.long	61                      # 0x3d
	.long	36                      # 0x24
	.long	121                     # 0x79
	.long	55                      # 0x37
	.long	96                      # 0x60
	.long	81                      # 0x51
	.long	32                      # 0x20
	.long	54                      # 0x36
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	123                     # 0x7b
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	17                      # 0x11
	.long	18                      # 0x12
	.long	19                      # 0x13
	.long	20                      # 0x14
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	23                      # 0x17
	.long	24                      # 0x18
	.long	25                      # 0x19
	.long	26                      # 0x1a
	.long	27                      # 0x1b
	.long	28                      # 0x1c
	.long	29                      # 0x1d
	.long	30                      # 0x1e
	.long	31                      # 0x1f
	.long	50                      # 0x32
	.long	48                      # 0x30
	.long	33                      # 0x21
	.long	41                      # 0x29
	.long	91                      # 0x5b
	.long	56                      # 0x38
	.long	51                      # 0x33
	.long	61                      # 0x3d
	.long	88                      # 0x58
	.long	58                      # 0x3a
	.long	53                      # 0x35
	.long	101                     # 0x65
	.long	57                      # 0x39
	.long	92                      # 0x5c
	.long	86                      # 0x56
	.long	115                     # 0x73
	.long	102                     # 0x66
	.long	78                      # 0x4e
	.long	69                      # 0x45
	.long	107                     # 0x6b
	.long	98                      # 0x62
	.long	89                      # 0x59
	.long	120                     # 0x78
	.long	94                      # 0x5e
	.long	125                     # 0x7d
	.long	74                      # 0x4a
	.long	109                     # 0x6d
	.long	113                     # 0x71
	.long	60                      # 0x3c
	.long	96                      # 0x60
	.long	62                      # 0x3e
	.long	83                      # 0x53
	.long	255                     # 0xff
	.long	66                      # 0x42
	.long	39                      # 0x27
	.long	72                      # 0x48
	.long	114                     # 0x72
	.long	117                     # 0x75
	.long	49                      # 0x31
	.long	55                      # 0x37
	.long	77                      # 0x4d
	.long	82                      # 0x52
	.long	34                      # 0x22
	.long	84                      # 0x54
	.long	106                     # 0x6a
	.long	71                      # 0x47
	.long	100                     # 0x64
	.long	45                      # 0x2d
	.long	32                      # 0x20
	.long	127                     # 0x7f
	.long	46                      # 0x2e
	.long	76                      # 0x4c
	.long	93                      # 0x5d
	.long	126                     # 0x7e
	.long	108                     # 0x6c
	.long	111                     # 0x6f
	.long	121                     # 0x79
	.long	116                     # 0x74
	.long	67                      # 0x43
	.long	38                      # 0x26
	.long	118                     # 0x76
	.long	37                      # 0x25
	.long	36                      # 0x24
	.long	43                      # 0x2b
	.long	40                      # 0x28
	.long	35                      # 0x23
	.long	65                      # 0x41
	.long	52                      # 0x34
	.long	9                       # 0x9
	.long	42                      # 0x2a
	.long	68                      # 0x44
	.long	63                      # 0x3f
	.long	119                     # 0x77
	.long	59                      # 0x3b
	.long	85                      # 0x55
	.long	105                     # 0x69
	.long	97                      # 0x61
	.long	99                      # 0x63
	.long	80                      # 0x50
	.long	103                     # 0x67
	.long	81                      # 0x51
	.long	73                      # 0x49
	.long	79                      # 0x4f
	.long	70                      # 0x46
	.long	104                     # 0x68
	.long	124                     # 0x7c
	.long	54                      # 0x36
	.long	112                     # 0x70
	.long	110                     # 0x6e
	.long	122                     # 0x7a
	.long	47                      # 0x2f
	.long	95                      # 0x5f
	.long	75                      # 0x4b
	.long	90                      # 0x5a
	.long	44                      # 0x2c
	.long	87                      # 0x57
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	110                     # 0x6e
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	6                       # 0x6
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	17                      # 0x11
	.long	18                      # 0x12
	.long	19                      # 0x13
	.long	20                      # 0x14
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	23                      # 0x17
	.long	24                      # 0x18
	.long	25                      # 0x19
	.long	26                      # 0x1a
	.long	27                      # 0x1b
	.long	28                      # 0x1c
	.long	29                      # 0x1d
	.long	30                      # 0x1e
	.long	31                      # 0x1f
	.long	45                      # 0x2d
	.long	117                     # 0x75
	.long	82                      # 0x52
	.long	96                      # 0x60
	.long	113                     # 0x71
	.long	94                      # 0x5e
	.long	73                      # 0x49
	.long	92                      # 0x5c
	.long	98                      # 0x62
	.long	125                     # 0x7d
	.long	41                      # 0x29
	.long	54                      # 0x36
	.long	32                      # 0x20
	.long	124                     # 0x7c
	.long	122                     # 0x7a
	.long	127                     # 0x7f
	.long	107                     # 0x6b
	.long	99                      # 0x63
	.long	51                      # 0x33
	.long	43                      # 0x2b
	.long	104                     # 0x68
	.long	81                      # 0x51
	.long	102                     # 0x66
	.long	118                     # 0x76
	.long	49                      # 0x31
	.long	100                     # 0x64
	.long	84                      # 0x54
	.long	67                      # 0x43
	.long	60                      # 0x3c
	.long	58                      # 0x3a
	.long	62                      # 0x3e
	.long	126                     # 0x7e
	.long	255                     # 0xff
	.long	69                      # 0x45
	.long	44                      # 0x2c
	.long	42                      # 0x2a
	.long	116                     # 0x74
	.long	39                      # 0x27
	.long	55                      # 0x37
	.long	68                      # 0x44
	.long	121                     # 0x79
	.long	89                      # 0x59
	.long	47                      # 0x2f
	.long	111                     # 0x6f
	.long	38                      # 0x26
	.long	114                     # 0x72
	.long	106                     # 0x6a
	.long	57                      # 0x39
	.long	123                     # 0x7b
	.long	63                      # 0x3f
	.long	56                      # 0x38
	.long	119                     # 0x77
	.long	103                     # 0x67
	.long	83                      # 0x53
	.long	71                      # 0x47
	.long	52                      # 0x34
	.long	120                     # 0x78
	.long	93                      # 0x5d
	.long	48                      # 0x30
	.long	35                      # 0x23
	.long	90                      # 0x5a
	.long	91                      # 0x5b
	.long	108                     # 0x6c
	.long	72                      # 0x48
	.long	85                      # 0x55
	.long	112                     # 0x70
	.long	105                     # 0x69
	.long	46                      # 0x2e
	.long	76                      # 0x4c
	.long	33                      # 0x21
	.long	36                      # 0x24
	.long	78                      # 0x4e
	.long	80                      # 0x50
	.long	9                       # 0x9
	.long	86                      # 0x56
	.long	115                     # 0x73
	.long	53                      # 0x35
	.long	97                      # 0x61
	.long	75                      # 0x4b
	.long	88                      # 0x58
	.long	59                      # 0x3b
	.long	87                      # 0x57
	.long	34                      # 0x22
	.long	109                     # 0x6d
	.long	77                      # 0x4d
	.long	37                      # 0x25
	.long	40                      # 0x28
	.long	70                      # 0x46
	.long	74                      # 0x4a
	.long	50                      # 0x32
	.long	65                      # 0x41
	.long	61                      # 0x3d
	.long	95                      # 0x5f
	.long	79                      # 0x4f
	.long	66                      # 0x42
	.long	101                     # 0x65
	.size	decrypt_tables, 1536

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"No HTML stream\n"
	.size	.L.str, 16

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"mmap HTML failed\n"
	.size	.L.str.1, 18

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"mmap'ed file\n"
	.size	.L.str.2, 14

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"fstat HTML failed\n"
	.size	.L.str.3, 19

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"r"
	.size	.L.str.4, 2

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%s/screnc.html"
	.size	.L.str.5, 15

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"open failed: %s\n"
	.size	.L.str.6, 17

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"#@~^"
	.size	.L.str.7, 5

	.type	base64_chars,@object    # @base64_chars
	.section	.rodata,"a",@progbits
	.p2align	4
base64_chars:
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	62                      # 0x3e
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	63                      # 0x3f
	.long	52                      # 0x34
	.long	53                      # 0x35
	.long	54                      # 0x36
	.long	55                      # 0x37
	.long	56                      # 0x38
	.long	57                      # 0x39
	.long	58                      # 0x3a
	.long	59                      # 0x3b
	.long	60                      # 0x3c
	.long	61                      # 0x3d
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	17                      # 0x11
	.long	18                      # 0x12
	.long	19                      # 0x13
	.long	20                      # 0x14
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	23                      # 0x17
	.long	24                      # 0x18
	.long	25                      # 0x19
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	26                      # 0x1a
	.long	27                      # 0x1b
	.long	28                      # 0x1c
	.long	29                      # 0x1d
	.long	30                      # 0x1e
	.long	31                      # 0x1f
	.long	32                      # 0x20
	.long	33                      # 0x21
	.long	34                      # 0x22
	.long	35                      # 0x23
	.long	36                      # 0x24
	.long	37                      # 0x25
	.long	38                      # 0x26
	.long	39                      # 0x27
	.long	40                      # 0x28
	.long	41                      # 0x29
	.long	42                      # 0x2a
	.long	43                      # 0x2b
	.long	44                      # 0x2c
	.long	45                      # 0x2d
	.long	46                      # 0x2e
	.long	47                      # 0x2f
	.long	48                      # 0x30
	.long	49                      # 0x31
	.long	50                      # 0x32
	.long	51                      # 0x33
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.size	base64_chars, 1024

	.type	.L.str.8,@object        # @.str.8
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.8:
	.asciz	"Invalid HTML fd\n"
	.size	.L.str.8, 17

	.type	.L.str.9,@object        # @.str.9
	.section	.rodata,"a",@progbits
.L.str.9:
	.zero	2
	.size	.L.str.9, 2

	.type	.L.str.10,@object       # @.str.10
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.10:
	.asciz	"%s/rfc2397"
	.size	.L.str.10, 11

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%s/comment.html"
	.size	.L.str.11, 16

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"%s/nocomment.html"
	.size	.L.str.12, 18

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"%s/script.html"
	.size	.L.str.13, 15

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Impossible, special_char can't occur here\n"
	.size	.L.str.14, 43

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"HTML Engine Error\n"
	.size	.L.str.15, 19

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"data:"
	.size	.L.str.16, 6

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"\"data:"
	.size	.L.str.17, 7

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"/script"
	.size	.L.str.18, 8

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"/form"
	.size	.L.str.20, 6

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"script"
	.size	.L.str.21, 7

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"language"
	.size	.L.str.22, 9

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"jscript.encode"
	.size	.L.str.23, 15

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"javascript"
	.size	.L.str.24, 11

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"vbscript.encode"
	.size	.L.str.25, 16

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"vbscript"
	.size	.L.str.26, 9

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"meta"
	.size	.L.str.27, 5

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"http-equiv"
	.size	.L.str.28, 11

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"content"
	.size	.L.str.29, 8

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"content-type"
	.size	.L.str.30, 13

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"charset"
	.size	.L.str.31, 8

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"href"
	.size	.L.str.34, 5

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"title"
	.size	.L.str.35, 6

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"href_title"
	.size	.L.str.36, 11

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"form"
	.size	.L.str.37, 5

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"action"
	.size	.L.str.38, 7

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"src"
	.size	.L.str.40, 4

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"dynsrc"
	.size	.L.str.41, 7

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"iframe"
	.size	.L.str.42, 7

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"area"
	.size	.L.str.43, 5

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"&#%d;"
	.size	.L.str.44, 6

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"</script>\n"
	.size	.L.str.45, 11

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"RFC2397 data file: %s\n"
	.size	.L.str.46, 23

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"From html-normalise\n"
	.size	.L.str.47, 21

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"Content-type: "
	.size	.L.str.48, 15

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"text/plain\n"
	.size	.L.str.49, 12

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	";base64"
	.size	.L.str.50, 8

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"Content-transfer-encoding: base64\n"
	.size	.L.str.51, 35

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.zero	1
	.size	.L.str.52, 1


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
