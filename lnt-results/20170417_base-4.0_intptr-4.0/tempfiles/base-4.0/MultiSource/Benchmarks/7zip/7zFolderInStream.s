	.text
	.file	"7zFolderInStream.bc"
	.globl	_ZN8NArchive3N7z15CFolderInStreamC2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z15CFolderInStreamC2Ev,@function
_ZN8NArchive3N7z15CFolderInStreamC2Ev:  # @_ZN8NArchive3N7z15CFolderInStreamC2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$0, 16(%rbx)
	movl	$_ZTVN8NArchive3N7z15CFolderInStreamE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive3N7z15CFolderInStreamE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	leaq	32(%rbx), %rbp
	leaq	88(%rbx), %r14
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 96(%rbx)
	movdqu	%xmm0, 32(%rbx)
	movq	$1, 112(%rbx)
	movq	$_ZTV13CRecordVectorIbE+16, 88(%rbx)
	leaq	120(%rbx), %r15
	movdqu	%xmm0, 128(%rbx)
	movq	$4, 144(%rbx)
	movq	$_ZTV13CRecordVectorIjE+16, 120(%rbx)
	leaq	152(%rbx), %r13
	movdqu	%xmm0, 160(%rbx)
	movq	$8, 176(%rbx)
	movq	$_ZTV13CRecordVectorIyE+16, 152(%rbx)
.Ltmp0:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %r12
.Ltmp1:
# BB#1:
	movl	$0, 8(%r12)
	movq	$_ZTV26CSequentialInStreamWithCRC+16, (%r12)
	movq	$0, 16(%r12)
	movq	%r12, 24(%rbx)
.Ltmp2:
	movq	%r12, %rdi
	callq	*_ZTV26CSequentialInStreamWithCRC+24(%rip)
.Ltmp3:
# BB#2:                                 # %.noexc5
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp4:
	callq	*16(%rax)
.Ltmp5:
.LBB0_4:
	movq	%r12, (%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_5:
.Ltmp6:
	movq	%rax, %r12
.Ltmp7:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp8:
# BB#6:
.Ltmp9:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp10:
# BB#7:
.Ltmp11:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp12:
# BB#8:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_10
# BB#9:
	movq	(%rdi), %rax
.Ltmp13:
	callq	*16(%rax)
.Ltmp14:
.LBB0_10:                               # %_ZN9CMyComPtrI22IArchiveUpdateCallbackED2Ev.exit
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_12
# BB#11:
	movq	(%rdi), %rax
.Ltmp15:
	callq	*16(%rax)
.Ltmp16:
.LBB0_12:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	%r12, %rdi
	callq	_Unwind_Resume
.LBB0_13:
.Ltmp17:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN8NArchive3N7z15CFolderInStreamC2Ev, .Lfunc_end0-_ZN8NArchive3N7z15CFolderInStreamC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\257\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp5-.Ltmp0           #   Call between .Ltmp0 and .Ltmp5
	.long	.Ltmp6-.Lfunc_begin0    #     jumps to .Ltmp6
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp16-.Ltmp7          #   Call between .Ltmp7 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp16     #   Call between .Ltmp16 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN8NArchive3N7z15CFolderInStream4InitEP22IArchiveUpdateCallbackPKjj
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z15CFolderInStream4InitEP22IArchiveUpdateCallbackPKjj,@function
_ZN8NArchive3N7z15CFolderInStream4InitEP22IArchiveUpdateCallbackPKjj: # @_ZN8NArchive3N7z15CFolderInStream4InitEP22IArchiveUpdateCallbackPKjj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movq	%rdx, %r14
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	testq	%rbp, %rbp
	je	.LBB2_2
# BB#1:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*8(%rax)
.LBB2_2:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB2_4:                                # %_ZN9CMyComPtrI22IArchiveUpdateCallbackEaSEPS0_.exit
	movq	%rbp, 40(%rbx)
	movl	%r15d, 80(%rbx)
	movl	$0, 84(%rbx)
	movq	%r14, 72(%rbx)
	leaq	88(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	120(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	152(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movw	$0, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN8NArchive3N7z15CFolderInStream4InitEP22IArchiveUpdateCallbackPKjj, .Lfunc_end2-_ZN8NArchive3N7z15CFolderInStream4InitEP22IArchiveUpdateCallbackPKjj
	.cfi_endproc

	.globl	_ZN8NArchive3N7z15CFolderInStream10OpenStreamEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z15CFolderInStream10OpenStreamEv,@function
_ZN8NArchive3N7z15CFolderInStream10OpenStreamEv: # @_ZN8NArchive3N7z15CFolderInStream10OpenStreamEv
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi28:
	.cfi_def_cfa_offset 112
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movq	$0, 64(%r13)
	leaq	152(%r13), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	88(%r13), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	120(%r13), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	56(%r13), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
                                        # implicit-def: %R14D
	jmp	.LBB3_1
	.p2align	4, 0x90
.LBB3_3:                                #   in Loop: Header=BB3_1 Depth=1
	movq	$0, 8(%rsp)
	movq	40(%r13), %rdi
	movq	72(%r13), %rcx
	movq	(%rdi), %rbp
	movl	(%rcx,%rax,4), %esi
.Ltmp18:
	leaq	8(%rsp), %rdx
	callq	*72(%rbp)
	movl	%eax, %r15d
.Ltmp19:
# BB#4:                                 #   in Loop: Header=BB3_1 Depth=1
	movl	$1, %ebx
	cmpl	$1, %r15d
	jbe	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_1 Depth=1
	movl	%r15d, %r12d
.LBB3_29:                               #   in Loop: Header=BB3_1 Depth=1
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_31
# BB#30:                                #   in Loop: Header=BB3_1 Depth=1
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB3_31:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit34
                                        #   in Loop: Header=BB3_1 Depth=1
	testl	%ebx, %ebx
	movl	%r12d, %r14d
	je	.LBB3_1
	jmp	.LBB3_32
	.p2align	4, 0x90
.LBB3_6:                                #   in Loop: Header=BB3_1 Depth=1
	incl	84(%r13)
	movq	24(%r13), %rbx
	movq	8(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB3_8
# BB#7:                                 #   in Loop: Header=BB3_1 Depth=1
	movq	(%rbp), %rax
.Ltmp21:
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp22:
.LBB3_8:                                # %.noexc36
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_1 Depth=1
	movq	(%rdi), %rax
.Ltmp23:
	callq	*16(%rax)
.Ltmp24:
.LBB3_10:                               #   in Loop: Header=BB3_1 Depth=1
	movq	%rbp, 16(%rbx)
	movq	24(%r13), %rax
	movq	$0, 24(%rax)
	movb	$0, 36(%rax)
	movl	$-1, 32(%rax)
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_23
# BB#11:                                #   in Loop: Header=BB3_1 Depth=1
	movb	$1, 49(%r13)
	movq	$0, 16(%rsp)
	movq	(%rdi), %rax
.Ltmp25:
	movl	$IID_IStreamGetSize, %esi
	leaq	16(%rsp), %rdx
	callq	*(%rax)
.Ltmp26:
	movl	$1, %ebx
# BB#12:                                # %_ZNK9CMyComPtrI19ISequentialInStreamE14QueryInterfaceI14IStreamGetSizeEEiRK4GUIDPPT_.exit
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_13
# BB#14:                                #   in Loop: Header=BB3_1 Depth=1
	movq	(%rdi), %rax
.Ltmp27:
	movq	48(%rsp), %rsi          # 8-byte Reload
	callq	*40(%rax)
	movl	%eax, %r12d
.Ltmp28:
# BB#15:                                #   in Loop: Header=BB3_1 Depth=1
	testl	%r12d, %r12d
	jne	.LBB3_17
# BB#16:                                #   in Loop: Header=BB3_1 Depth=1
	movb	$1, 48(%r13)
	xorl	%r12d, %r12d
.LBB3_17:                               #   in Loop: Header=BB3_1 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_29
# BB#18:                                #   in Loop: Header=BB3_1 Depth=1
	movq	(%rdi), %rax
.Ltmp32:
	callq	*16(%rax)
.Ltmp33:
	jmp	.LBB3_29
.LBB3_23:                               #   in Loop: Header=BB3_1 Depth=1
	movq	40(%r13), %rdi
	movq	(%rdi), %rax
.Ltmp35:
	xorl	%esi, %esi
	callq	*80(%rax)
	movl	%eax, %r12d
.Ltmp36:
# BB#24:                                #   in Loop: Header=BB3_1 Depth=1
	testl	%r12d, %r12d
	movl	$1, %ebx
	jne	.LBB3_29
# BB#25:                                #   in Loop: Header=BB3_1 Depth=1
.Ltmp37:
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp38:
# BB#26:                                #   in Loop: Header=BB3_1 Depth=1
	movq	168(%r13), %rax
	movslq	164(%r13), %rcx
	movq	$0, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 164(%r13)
.Ltmp39:
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp40:
# BB#27:                                #   in Loop: Header=BB3_1 Depth=1
	testl	%r15d, %r15d
	movq	104(%r13), %rax
	movslq	100(%r13), %rcx
	sete	(%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 100(%r13)
	movq	24(%r13), %rax
	movl	32(%rax), %ebx
.Ltmp41:
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp42:
# BB#28:                                # %_ZN8NArchive3N7z15CFolderInStream9AddDigestEv.exit
                                        #   in Loop: Header=BB3_1 Depth=1
	notl	%ebx
	movq	136(%r13), %rax
	movslq	132(%r13), %rcx
	movl	%ebx, (%rax,%rcx,4)
	incl	132(%r13)
	xorl	%ebx, %ebx
	movl	%r14d, %r12d
	jmp	.LBB3_29
.LBB3_13:                               #   in Loop: Header=BB3_1 Depth=1
	xorl	%r12d, %r12d
	jmp	.LBB3_29
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	movl	84(%r13), %eax
	cmpl	80(%r13), %eax
	jb	.LBB3_3
# BB#2:
	xorl	%r12d, %r12d
.LBB3_32:
	movl	%r12d, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_22:
.Ltmp34:
	jmp	.LBB3_34
.LBB3_20:
.Ltmp29:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_35
# BB#21:
	movq	(%rdi), %rax
.Ltmp30:
	callq	*16(%rax)
.Ltmp31:
	jmp	.LBB3_35
.LBB3_33:
.Ltmp20:
	jmp	.LBB3_34
.LBB3_19:
.Ltmp43:
.LBB3_34:
	movq	%rax, %rbx
.LBB3_35:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_37
# BB#36:
	movq	(%rdi), %rax
.Ltmp44:
	callq	*16(%rax)
.Ltmp45:
.LBB3_37:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB3_38:
.Ltmp46:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN8NArchive3N7z15CFolderInStream10OpenStreamEv, .Lfunc_end3-_ZN8NArchive3N7z15CFolderInStream10OpenStreamEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp18-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin1   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp21-.Ltmp19         #   Call between .Ltmp19 and .Ltmp21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp24-.Ltmp21         #   Call between .Ltmp21 and .Ltmp24
	.long	.Ltmp43-.Lfunc_begin1   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp28-.Ltmp25         #   Call between .Ltmp25 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin1   #     jumps to .Ltmp29
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin1   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp42-.Ltmp35         #   Call between .Ltmp35 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin1   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp45-.Ltmp30         #   Call between .Ltmp30 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin1   #     jumps to .Ltmp46
	.byte	1                       #   On action: 1
	.long	.Ltmp45-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Lfunc_end3-.Ltmp45     #   Call between .Ltmp45 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3N7z15CFolderInStream9AddDigestEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z15CFolderInStream9AddDigestEv,@function
_ZN8NArchive3N7z15CFolderInStream9AddDigestEv: # @_ZN8NArchive3N7z15CFolderInStream9AddDigestEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rax
	movl	32(%rax), %ebp
	notl	%ebp
	leaq	120(%rbx), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	136(%rbx), %rax
	movslq	132(%rbx), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	132(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN8NArchive3N7z15CFolderInStream9AddDigestEv, .Lfunc_end4-_ZN8NArchive3N7z15CFolderInStream9AddDigestEv
	.cfi_endproc

	.globl	_ZN8NArchive3N7z15CFolderInStream11CloseStreamEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z15CFolderInStream11CloseStreamEv,@function
_ZN8NArchive3N7z15CFolderInStream11CloseStreamEv: # @_ZN8NArchive3N7z15CFolderInStream11CloseStreamEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 32
.Lcfi43:
	.cfi_offset %rbx, -32
.Lcfi44:
	.cfi_offset %r14, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	xorl	%esi, %esi
	callq	*80(%rax)
	testl	%eax, %eax
	jne	.LBB5_4
# BB#1:
	movq	24(%rbx), %r14
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB5_3
# BB#2:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 16(%r14)
.LBB5_3:                                # %_ZN26CSequentialInStreamWithCRC13ReleaseStreamEv.exit
	movw	$0, 48(%rbx)
	leaq	88(%rbx), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	104(%rbx), %rax
	movslq	100(%rbx), %rcx
	movb	$1, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 100(%rbx)
	movq	64(%rbx), %r14
	leaq	152(%rbx), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	168(%rbx), %rax
	movslq	164(%rbx), %rcx
	movq	%r14, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 164(%rbx)
	movq	24(%rbx), %rax
	movl	32(%rax), %ebp
	notl	%ebp
	leaq	120(%rbx), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	136(%rbx), %rax
	movslq	132(%rbx), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	132(%rbx)
	xorl	%eax, %eax
.LBB5_4:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN8NArchive3N7z15CFolderInStream11CloseStreamEv, .Lfunc_end5-_ZN8NArchive3N7z15CFolderInStream11CloseStreamEv
	.cfi_endproc

	.globl	_ZN8NArchive3N7z15CFolderInStream4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z15CFolderInStream4ReadEPvjPj,@function
_ZN8NArchive3N7z15CFolderInStream4ReadEPvjPj: # @_ZN8NArchive3N7z15CFolderInStream4ReadEPvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 64
.Lcfi53:
	.cfi_offset %rbx, -56
.Lcfi54:
	.cfi_offset %r12, -48
.Lcfi55:
	.cfi_offset %r13, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movl	%edx, %r15d
	movq	%rsi, %r12
	movq	%rdi, %rbp
	testq	%rbx, %rbx
	je	.LBB6_2
# BB#1:
	movl	$0, (%rbx)
.LBB6_2:                                # %.preheader
	xorl	%r13d, %r13d
	testl	%r15d, %r15d
	je	.LBB6_18
# BB#3:                                 # %.lr.ph
                                        # implicit-def: %R14D
	.p2align	4, 0x90
.LBB6_4:                                # %.backedge
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, 49(%rbp)
	jne	.LBB6_5
# BB#15:                                #   in Loop: Header=BB6_4 Depth=1
	movl	84(%rbp), %eax
	cmpl	80(%rbp), %eax
	jae	.LBB6_18
# BB#16:                                #   in Loop: Header=BB6_4 Depth=1
	movq	%rbp, %rdi
	callq	_ZN8NArchive3N7z15CFolderInStream10OpenStreamEv
	testl	%eax, %eax
	je	.LBB6_4
	jmp	.LBB6_17
.LBB6_5:                                #   in Loop: Header=BB6_4 Depth=1
	movq	32(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	movl	%r15d, %edx
	leaq	4(%rsp), %rcx
	callq	*40(%rax)
	testl	%eax, %eax
	cmovnel	%eax, %r14d
	jne	.LBB6_17
# BB#6:                                 #   in Loop: Header=BB6_4 Depth=1
	movl	4(%rsp), %eax
	testq	%rax, %rax
	jne	.LBB6_7
# BB#10:                                #   in Loop: Header=BB6_4 Depth=1
	movq	%rbp, %rdi
	callq	_ZN8NArchive3N7z15CFolderInStream11CloseStreamEv
	testl	%eax, %eax
	setne	%cl
	cmovnel	%eax, %r14d
	movb	$2, %al
	je	.LBB6_12
# BB#11:                                #   in Loop: Header=BB6_4 Depth=1
	movl	%ecx, %eax
.LBB6_12:                               #   in Loop: Header=BB6_4 Depth=1
	cmpb	$2, %al
	je	.LBB6_4
# BB#13:
	cmpb	$3, %al
	je	.LBB6_18
# BB#14:                                # %.loopexit.loopexit66
	movl	%r14d, %r13d
	jmp	.LBB6_18
.LBB6_17:
	movl	%eax, %r13d
.LBB6_18:                               # %.loopexit
	movl	%r13d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_7:                                # %.us-lcssa
	testq	%rbx, %rbx
	je	.LBB6_9
# BB#8:
	movl	%eax, (%rbx)
.LBB6_9:                                # %.thread
	addq	%rax, 64(%rbp)
	jmp	.LBB6_18
.Lfunc_end6:
	.size	_ZN8NArchive3N7z15CFolderInStream4ReadEPvjPj, .Lfunc_end6-_ZN8NArchive3N7z15CFolderInStream4ReadEPvjPj
	.cfi_endproc

	.globl	_ZN8NArchive3N7z15CFolderInStream16GetSubStreamSizeEyPy
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z15CFolderInStream16GetSubStreamSizeEyPy,@function
_ZN8NArchive3N7z15CFolderInStream16GetSubStreamSizeEyPy: # @_ZN8NArchive3N7z15CFolderInStream16GetSubStreamSizeEyPy
	.cfi_startproc
# BB#0:
	movq	$0, (%rdx)
	movl	$-2147467259, %eax      # imm = 0x80004005
	testl	%esi, %esi
	js	.LBB7_8
# BB#1:
	movslq	164(%rdi), %rcx
	cmpq	%rsi, %rcx
	jb	.LBB7_8
# BB#2:
	cmpl	%ecx, %esi
	jge	.LBB7_4
# BB#3:
	movq	168(%rdi), %rax
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rax
	jmp	.LBB7_7
.LBB7_4:
	cmpb	$0, 48(%rdi)
	je	.LBB7_5
# BB#6:
	movq	56(%rdi), %rax
.LBB7_7:
	movq	%rax, (%rdx)
	xorl	%eax, %eax
.LBB7_8:
	retq
.LBB7_5:
	movl	$1, %eax
	retq
.Lfunc_end7:
	.size	_ZN8NArchive3N7z15CFolderInStream16GetSubStreamSizeEyPy, .Lfunc_end7-_ZN8NArchive3N7z15CFolderInStream16GetSubStreamSizeEyPy
	.cfi_endproc

	.globl	_ZThn8_N8NArchive3N7z15CFolderInStream16GetSubStreamSizeEyPy
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3N7z15CFolderInStream16GetSubStreamSizeEyPy,@function
_ZThn8_N8NArchive3N7z15CFolderInStream16GetSubStreamSizeEyPy: # @_ZThn8_N8NArchive3N7z15CFolderInStream16GetSubStreamSizeEyPy
	.cfi_startproc
# BB#0:
	movq	$0, (%rdx)
	movl	$-2147467259, %eax      # imm = 0x80004005
	testl	%esi, %esi
	js	.LBB8_8
# BB#1:
	movslq	156(%rdi), %rcx
	cmpq	%rsi, %rcx
	jb	.LBB8_8
# BB#2:
	cmpl	%ecx, %esi
	jge	.LBB8_4
# BB#3:
	movq	160(%rdi), %rax
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rax
	jmp	.LBB8_7
.LBB8_4:
	cmpb	$0, 40(%rdi)
	je	.LBB8_5
# BB#6:
	movq	48(%rdi), %rax
.LBB8_7:                                # %_ZN8NArchive3N7z15CFolderInStream16GetSubStreamSizeEyPy.exit
	movq	%rax, (%rdx)
	xorl	%eax, %eax
.LBB8_8:                                # %_ZN8NArchive3N7z15CFolderInStream16GetSubStreamSizeEyPy.exit
	retq
.LBB8_5:
	movl	$1, %eax
	retq
.Lfunc_end8:
	.size	_ZThn8_N8NArchive3N7z15CFolderInStream16GetSubStreamSizeEyPy, .Lfunc_end8-_ZThn8_N8NArchive3N7z15CFolderInStream16GetSubStreamSizeEyPy
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z15CFolderInStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive3N7z15CFolderInStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive3N7z15CFolderInStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z15CFolderInStream14QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive3N7z15CFolderInStream14QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive3N7z15CFolderInStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 32
.Lcfi62:
	.cfi_offset %rbx, -32
.Lcfi63:
	.cfi_offset %r14, -24
.Lcfi64:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB9_3
# BB#1:
	movl	$IID_ICompressGetSubStreamSize, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB9_2
.LBB9_3:
	leaq	8(%rbx), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB9_4:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB9_2:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB9_4
.Lfunc_end9:
	.size	_ZN8NArchive3N7z15CFolderInStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end9-_ZN8NArchive3N7z15CFolderInStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z15CFolderInStream6AddRefEv,"axG",@progbits,_ZN8NArchive3N7z15CFolderInStream6AddRefEv,comdat
	.weak	_ZN8NArchive3N7z15CFolderInStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z15CFolderInStream6AddRefEv,@function
_ZN8NArchive3N7z15CFolderInStream6AddRefEv: # @_ZN8NArchive3N7z15CFolderInStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end10:
	.size	_ZN8NArchive3N7z15CFolderInStream6AddRefEv, .Lfunc_end10-_ZN8NArchive3N7z15CFolderInStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z15CFolderInStream7ReleaseEv,"axG",@progbits,_ZN8NArchive3N7z15CFolderInStream7ReleaseEv,comdat
	.weak	_ZN8NArchive3N7z15CFolderInStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z15CFolderInStream7ReleaseEv,@function
_ZN8NArchive3N7z15CFolderInStream7ReleaseEv: # @_ZN8NArchive3N7z15CFolderInStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi65:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB11_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB11_2:
	popq	%rcx
	retq
.Lfunc_end11:
	.size	_ZN8NArchive3N7z15CFolderInStream7ReleaseEv, .Lfunc_end11-_ZN8NArchive3N7z15CFolderInStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z15CFolderInStreamD2Ev,"axG",@progbits,_ZN8NArchive3N7z15CFolderInStreamD2Ev,comdat
	.weak	_ZN8NArchive3N7z15CFolderInStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z15CFolderInStreamD2Ev,@function
_ZN8NArchive3N7z15CFolderInStreamD2Ev:  # @_ZN8NArchive3N7z15CFolderInStreamD2Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi68:
	.cfi_def_cfa_offset 32
.Lcfi69:
	.cfi_offset %rbx, -24
.Lcfi70:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN8NArchive3N7z15CFolderInStreamE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive3N7z15CFolderInStreamE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	leaq	152(%rbx), %rdi
.Ltmp47:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp48:
# BB#1:
	leaq	120(%rbx), %rdi
.Ltmp52:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp53:
# BB#2:
	leaq	88(%rbx), %rdi
.Ltmp57:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp58:
# BB#3:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_5
# BB#4:
	movq	(%rdi), %rax
.Ltmp62:
	callq	*16(%rax)
.Ltmp63:
.LBB12_5:                               # %_ZN9CMyComPtrI22IArchiveUpdateCallbackED2Ev.exit
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_7
# BB#6:
	movq	(%rdi), %rax
.Ltmp68:
	callq	*16(%rax)
.Ltmp69:
.LBB12_7:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB12_14:
.Ltmp70:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB12_15:
.Ltmp64:
	movq	%rax, %r14
	jmp	.LBB12_16
.LBB12_11:
.Ltmp59:
	movq	%rax, %r14
	jmp	.LBB12_12
.LBB12_9:
.Ltmp54:
	movq	%rax, %r14
	jmp	.LBB12_10
.LBB12_8:
.Ltmp49:
	movq	%rax, %r14
	leaq	120(%rbx), %rdi
.Ltmp50:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp51:
.LBB12_10:
	leaq	88(%rbx), %rdi
.Ltmp55:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp56:
.LBB12_12:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_16
# BB#13:
	movq	(%rdi), %rax
.Ltmp60:
	callq	*16(%rax)
.Ltmp61:
.LBB12_16:                              # %_ZN9CMyComPtrI22IArchiveUpdateCallbackED2Ev.exit11
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_18
# BB#17:
	movq	(%rdi), %rax
.Ltmp65:
	callq	*16(%rax)
.Ltmp66:
.LBB12_18:                              # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit9
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB12_19:
.Ltmp67:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZN8NArchive3N7z15CFolderInStreamD2Ev, .Lfunc_end12-_ZN8NArchive3N7z15CFolderInStreamD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp47-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin2   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin2   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin2   #     jumps to .Ltmp59
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp63-.Ltmp62         #   Call between .Ltmp62 and .Ltmp63
	.long	.Ltmp64-.Lfunc_begin2   #     jumps to .Ltmp64
	.byte	0                       #   On action: cleanup
	.long	.Ltmp68-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp69-.Ltmp68         #   Call between .Ltmp68 and .Ltmp69
	.long	.Ltmp70-.Lfunc_begin2   #     jumps to .Ltmp70
	.byte	0                       #   On action: cleanup
	.long	.Ltmp69-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp50-.Ltmp69         #   Call between .Ltmp69 and .Ltmp50
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp66-.Ltmp50         #   Call between .Ltmp50 and .Ltmp66
	.long	.Ltmp67-.Lfunc_begin2   #     jumps to .Ltmp67
	.byte	1                       #   On action: 1
	.long	.Ltmp66-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Lfunc_end12-.Ltmp66    #   Call between .Ltmp66 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z15CFolderInStreamD0Ev,"axG",@progbits,_ZN8NArchive3N7z15CFolderInStreamD0Ev,comdat
	.weak	_ZN8NArchive3N7z15CFolderInStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z15CFolderInStreamD0Ev,@function
_ZN8NArchive3N7z15CFolderInStreamD0Ev:  # @_ZN8NArchive3N7z15CFolderInStreamD0Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi73:
	.cfi_def_cfa_offset 32
.Lcfi74:
	.cfi_offset %rbx, -24
.Lcfi75:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp71:
	callq	_ZN8NArchive3N7z15CFolderInStreamD2Ev
.Ltmp72:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB13_2:
.Ltmp73:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end13:
	.size	_ZN8NArchive3N7z15CFolderInStreamD0Ev, .Lfunc_end13-_ZN8NArchive3N7z15CFolderInStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp71-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp72-.Ltmp71         #   Call between .Ltmp71 and .Ltmp72
	.long	.Ltmp73-.Lfunc_begin3   #     jumps to .Ltmp73
	.byte	0                       #   On action: cleanup
	.long	.Ltmp72-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Lfunc_end13-.Ltmp72    #   Call between .Ltmp72 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N8NArchive3N7z15CFolderInStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N8NArchive3N7z15CFolderInStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N8NArchive3N7z15CFolderInStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3N7z15CFolderInStream14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N8NArchive3N7z15CFolderInStream14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N8NArchive3N7z15CFolderInStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi76:
	.cfi_def_cfa_offset 16
	addq	$-8, %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB14_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB14_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB14_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB14_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB14_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB14_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB14_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB14_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB14_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB14_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB14_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB14_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB14_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB14_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB14_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB14_32
.LBB14_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressGetSubStreamSize(%rip), %cl
	jne	.LBB14_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+1(%rip), %cl
	jne	.LBB14_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+2(%rip), %cl
	jne	.LBB14_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+3(%rip), %cl
	jne	.LBB14_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+4(%rip), %cl
	jne	.LBB14_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+5(%rip), %cl
	jne	.LBB14_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+6(%rip), %cl
	jne	.LBB14_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+7(%rip), %cl
	jne	.LBB14_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+8(%rip), %cl
	jne	.LBB14_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+9(%rip), %cl
	jne	.LBB14_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+10(%rip), %cl
	jne	.LBB14_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+11(%rip), %cl
	jne	.LBB14_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+12(%rip), %cl
	jne	.LBB14_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+13(%rip), %cl
	jne	.LBB14_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+14(%rip), %cl
	jne	.LBB14_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+15(%rip), %cl
	jne	.LBB14_33
.LBB14_32:
	leaq	8(%rdi), %rax
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB14_33:                              # %_ZN8NArchive3N7z15CFolderInStream14QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end14:
	.size	_ZThn8_N8NArchive3N7z15CFolderInStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end14-_ZThn8_N8NArchive3N7z15CFolderInStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive3N7z15CFolderInStream6AddRefEv,"axG",@progbits,_ZThn8_N8NArchive3N7z15CFolderInStream6AddRefEv,comdat
	.weak	_ZThn8_N8NArchive3N7z15CFolderInStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3N7z15CFolderInStream6AddRefEv,@function
_ZThn8_N8NArchive3N7z15CFolderInStream6AddRefEv: # @_ZThn8_N8NArchive3N7z15CFolderInStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end15:
	.size	_ZThn8_N8NArchive3N7z15CFolderInStream6AddRefEv, .Lfunc_end15-_ZThn8_N8NArchive3N7z15CFolderInStream6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive3N7z15CFolderInStream7ReleaseEv,"axG",@progbits,_ZThn8_N8NArchive3N7z15CFolderInStream7ReleaseEv,comdat
	.weak	_ZThn8_N8NArchive3N7z15CFolderInStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3N7z15CFolderInStream7ReleaseEv,@function
_ZThn8_N8NArchive3N7z15CFolderInStream7ReleaseEv: # @_ZThn8_N8NArchive3N7z15CFolderInStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi77:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB16_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB16_2:                               # %_ZN8NArchive3N7z15CFolderInStream7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end16:
	.size	_ZThn8_N8NArchive3N7z15CFolderInStream7ReleaseEv, .Lfunc_end16-_ZThn8_N8NArchive3N7z15CFolderInStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive3N7z15CFolderInStreamD1Ev,"axG",@progbits,_ZThn8_N8NArchive3N7z15CFolderInStreamD1Ev,comdat
	.weak	_ZThn8_N8NArchive3N7z15CFolderInStreamD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3N7z15CFolderInStreamD1Ev,@function
_ZThn8_N8NArchive3N7z15CFolderInStreamD1Ev: # @_ZThn8_N8NArchive3N7z15CFolderInStreamD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN8NArchive3N7z15CFolderInStreamD2Ev # TAILCALL
.Lfunc_end17:
	.size	_ZThn8_N8NArchive3N7z15CFolderInStreamD1Ev, .Lfunc_end17-_ZThn8_N8NArchive3N7z15CFolderInStreamD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive3N7z15CFolderInStreamD0Ev,"axG",@progbits,_ZThn8_N8NArchive3N7z15CFolderInStreamD0Ev,comdat
	.weak	_ZThn8_N8NArchive3N7z15CFolderInStreamD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3N7z15CFolderInStreamD0Ev,@function
_ZThn8_N8NArchive3N7z15CFolderInStreamD0Ev: # @_ZThn8_N8NArchive3N7z15CFolderInStreamD0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi80:
	.cfi_def_cfa_offset 32
.Lcfi81:
	.cfi_offset %rbx, -24
.Lcfi82:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp74:
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z15CFolderInStreamD2Ev
.Ltmp75:
# BB#1:                                 # %_ZN8NArchive3N7z15CFolderInStreamD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB18_2:
.Ltmp76:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end18:
	.size	_ZThn8_N8NArchive3N7z15CFolderInStreamD0Ev, .Lfunc_end18-_ZThn8_N8NArchive3N7z15CFolderInStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp74-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp75-.Ltmp74         #   Call between .Ltmp74 and .Ltmp75
	.long	.Ltmp76-.Lfunc_begin4   #     jumps to .Ltmp76
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Lfunc_end18-.Ltmp75    #   Call between .Ltmp75 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZeqRK4GUIDS1_,"axG",@progbits,_ZeqRK4GUIDS1_,comdat
	.weak	_ZeqRK4GUIDS1_
	.p2align	4, 0x90
	.type	_ZeqRK4GUIDS1_,@function
_ZeqRK4GUIDS1_:                         # @_ZeqRK4GUIDS1_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	cmpb	(%rsi), %al
	jne	.LBB19_16
# BB#1:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	jne	.LBB19_16
# BB#2:
	movb	2(%rdi), %al
	cmpb	2(%rsi), %al
	jne	.LBB19_16
# BB#3:
	movb	3(%rdi), %al
	cmpb	3(%rsi), %al
	jne	.LBB19_16
# BB#4:
	movb	4(%rdi), %al
	cmpb	4(%rsi), %al
	jne	.LBB19_16
# BB#5:
	movb	5(%rdi), %al
	cmpb	5(%rsi), %al
	jne	.LBB19_16
# BB#6:
	movb	6(%rdi), %al
	cmpb	6(%rsi), %al
	jne	.LBB19_16
# BB#7:
	movb	7(%rdi), %al
	cmpb	7(%rsi), %al
	jne	.LBB19_16
# BB#8:
	movb	8(%rdi), %al
	cmpb	8(%rsi), %al
	jne	.LBB19_16
# BB#9:
	movb	9(%rdi), %al
	cmpb	9(%rsi), %al
	jne	.LBB19_16
# BB#10:
	movb	10(%rdi), %al
	cmpb	10(%rsi), %al
	jne	.LBB19_16
# BB#11:
	movb	11(%rdi), %al
	cmpb	11(%rsi), %al
	jne	.LBB19_16
# BB#12:
	movb	12(%rdi), %al
	cmpb	12(%rsi), %al
	jne	.LBB19_16
# BB#13:
	movb	13(%rdi), %al
	cmpb	13(%rsi), %al
	jne	.LBB19_16
# BB#14:
	movb	14(%rdi), %al
	cmpb	14(%rsi), %al
	jne	.LBB19_16
# BB#15:
	movb	15(%rdi), %cl
	xorl	%eax, %eax
	cmpb	15(%rsi), %cl
	sete	%al
	retq
.LBB19_16:
	xorl	%eax, %eax
	retq
.Lfunc_end19:
	.size	_ZeqRK4GUIDS1_, .Lfunc_end19-_ZeqRK4GUIDS1_
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIjED0Ev,"axG",@progbits,_ZN13CRecordVectorIjED0Ev,comdat
	.weak	_ZN13CRecordVectorIjED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIjED0Ev,@function
_ZN13CRecordVectorIjED0Ev:              # @_ZN13CRecordVectorIjED0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi85:
	.cfi_def_cfa_offset 32
.Lcfi86:
	.cfi_offset %rbx, -24
.Lcfi87:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp77:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp78:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB20_2:
.Ltmp79:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end20:
	.size	_ZN13CRecordVectorIjED0Ev, .Lfunc_end20-_ZN13CRecordVectorIjED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table20:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp77-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp78-.Ltmp77         #   Call between .Ltmp77 and .Ltmp78
	.long	.Ltmp79-.Lfunc_begin5   #     jumps to .Ltmp79
	.byte	0                       #   On action: cleanup
	.long	.Ltmp78-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Lfunc_end20-.Ltmp78    #   Call between .Ltmp78 and .Lfunc_end20
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIyED0Ev,"axG",@progbits,_ZN13CRecordVectorIyED0Ev,comdat
	.weak	_ZN13CRecordVectorIyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyED0Ev,@function
_ZN13CRecordVectorIyED0Ev:              # @_ZN13CRecordVectorIyED0Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi90:
	.cfi_def_cfa_offset 32
.Lcfi91:
	.cfi_offset %rbx, -24
.Lcfi92:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp80:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp81:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB21_2:
.Ltmp82:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end21:
	.size	_ZN13CRecordVectorIyED0Ev, .Lfunc_end21-_ZN13CRecordVectorIyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp80-.Lfunc_begin6   # >> Call Site 1 <<
	.long	.Ltmp81-.Ltmp80         #   Call between .Ltmp80 and .Ltmp81
	.long	.Ltmp82-.Lfunc_begin6   #     jumps to .Ltmp82
	.byte	0                       #   On action: cleanup
	.long	.Ltmp81-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Lfunc_end21-.Ltmp81    #   Call between .Ltmp81 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIbED0Ev,"axG",@progbits,_ZN13CRecordVectorIbED0Ev,comdat
	.weak	_ZN13CRecordVectorIbED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIbED0Ev,@function
_ZN13CRecordVectorIbED0Ev:              # @_ZN13CRecordVectorIbED0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi95:
	.cfi_def_cfa_offset 32
.Lcfi96:
	.cfi_offset %rbx, -24
.Lcfi97:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp83:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp84:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB22_2:
.Ltmp85:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end22:
	.size	_ZN13CRecordVectorIbED0Ev, .Lfunc_end22-_ZN13CRecordVectorIbED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp83-.Lfunc_begin7   # >> Call Site 1 <<
	.long	.Ltmp84-.Ltmp83         #   Call between .Ltmp83 and .Ltmp84
	.long	.Ltmp85-.Lfunc_begin7   #     jumps to .Ltmp85
	.byte	0                       #   On action: cleanup
	.long	.Ltmp84-.Lfunc_begin7   # >> Call Site 2 <<
	.long	.Lfunc_end22-.Ltmp84    #   Call between .Ltmp84 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTVN8NArchive3N7z15CFolderInStreamE,@object # @_ZTVN8NArchive3N7z15CFolderInStreamE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN8NArchive3N7z15CFolderInStreamE
	.p2align	3
_ZTVN8NArchive3N7z15CFolderInStreamE:
	.quad	0
	.quad	_ZTIN8NArchive3N7z15CFolderInStreamE
	.quad	_ZN8NArchive3N7z15CFolderInStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive3N7z15CFolderInStream6AddRefEv
	.quad	_ZN8NArchive3N7z15CFolderInStream7ReleaseEv
	.quad	_ZN8NArchive3N7z15CFolderInStreamD2Ev
	.quad	_ZN8NArchive3N7z15CFolderInStreamD0Ev
	.quad	_ZN8NArchive3N7z15CFolderInStream4ReadEPvjPj
	.quad	_ZN8NArchive3N7z15CFolderInStream16GetSubStreamSizeEyPy
	.quad	-8
	.quad	_ZTIN8NArchive3N7z15CFolderInStreamE
	.quad	_ZThn8_N8NArchive3N7z15CFolderInStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N8NArchive3N7z15CFolderInStream6AddRefEv
	.quad	_ZThn8_N8NArchive3N7z15CFolderInStream7ReleaseEv
	.quad	_ZThn8_N8NArchive3N7z15CFolderInStreamD1Ev
	.quad	_ZThn8_N8NArchive3N7z15CFolderInStreamD0Ev
	.quad	_ZThn8_N8NArchive3N7z15CFolderInStream16GetSubStreamSizeEyPy
	.size	_ZTVN8NArchive3N7z15CFolderInStreamE, 136

	.type	_ZTSN8NArchive3N7z15CFolderInStreamE,@object # @_ZTSN8NArchive3N7z15CFolderInStreamE
	.globl	_ZTSN8NArchive3N7z15CFolderInStreamE
	.p2align	4
_ZTSN8NArchive3N7z15CFolderInStreamE:
	.asciz	"N8NArchive3N7z15CFolderInStreamE"
	.size	_ZTSN8NArchive3N7z15CFolderInStreamE, 33

	.type	_ZTS19ISequentialInStream,@object # @_ZTS19ISequentialInStream
	.section	.rodata._ZTS19ISequentialInStream,"aG",@progbits,_ZTS19ISequentialInStream,comdat
	.weak	_ZTS19ISequentialInStream
	.p2align	4
_ZTS19ISequentialInStream:
	.asciz	"19ISequentialInStream"
	.size	_ZTS19ISequentialInStream, 22

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI19ISequentialInStream,@object # @_ZTI19ISequentialInStream
	.section	.rodata._ZTI19ISequentialInStream,"aG",@progbits,_ZTI19ISequentialInStream,comdat
	.weak	_ZTI19ISequentialInStream
	.p2align	4
_ZTI19ISequentialInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19ISequentialInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI19ISequentialInStream, 24

	.type	_ZTS25ICompressGetSubStreamSize,@object # @_ZTS25ICompressGetSubStreamSize
	.section	.rodata._ZTS25ICompressGetSubStreamSize,"aG",@progbits,_ZTS25ICompressGetSubStreamSize,comdat
	.weak	_ZTS25ICompressGetSubStreamSize
	.p2align	4
_ZTS25ICompressGetSubStreamSize:
	.asciz	"25ICompressGetSubStreamSize"
	.size	_ZTS25ICompressGetSubStreamSize, 28

	.type	_ZTI25ICompressGetSubStreamSize,@object # @_ZTI25ICompressGetSubStreamSize
	.section	.rodata._ZTI25ICompressGetSubStreamSize,"aG",@progbits,_ZTI25ICompressGetSubStreamSize,comdat
	.weak	_ZTI25ICompressGetSubStreamSize
	.p2align	4
_ZTI25ICompressGetSubStreamSize:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS25ICompressGetSubStreamSize
	.quad	_ZTI8IUnknown
	.size	_ZTI25ICompressGetSubStreamSize, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN8NArchive3N7z15CFolderInStreamE,@object # @_ZTIN8NArchive3N7z15CFolderInStreamE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive3N7z15CFolderInStreamE
	.p2align	4
_ZTIN8NArchive3N7z15CFolderInStreamE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive3N7z15CFolderInStreamE
	.long	1                       # 0x1
	.long	3                       # 0x3
	.quad	_ZTI19ISequentialInStream
	.quad	2                       # 0x2
	.quad	_ZTI25ICompressGetSubStreamSize
	.quad	2050                    # 0x802
	.quad	_ZTI13CMyUnknownImp
	.quad	4098                    # 0x1002
	.size	_ZTIN8NArchive3N7z15CFolderInStreamE, 72

	.type	_ZTV13CRecordVectorIjE,@object # @_ZTV13CRecordVectorIjE
	.section	.rodata._ZTV13CRecordVectorIjE,"aG",@progbits,_ZTV13CRecordVectorIjE,comdat
	.weak	_ZTV13CRecordVectorIjE
	.p2align	3
_ZTV13CRecordVectorIjE:
	.quad	0
	.quad	_ZTI13CRecordVectorIjE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIjED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIjE, 40

	.type	_ZTS13CRecordVectorIjE,@object # @_ZTS13CRecordVectorIjE
	.section	.rodata._ZTS13CRecordVectorIjE,"aG",@progbits,_ZTS13CRecordVectorIjE,comdat
	.weak	_ZTS13CRecordVectorIjE
	.p2align	4
_ZTS13CRecordVectorIjE:
	.asciz	"13CRecordVectorIjE"
	.size	_ZTS13CRecordVectorIjE, 19

	.type	_ZTI13CRecordVectorIjE,@object # @_ZTI13CRecordVectorIjE
	.section	.rodata._ZTI13CRecordVectorIjE,"aG",@progbits,_ZTI13CRecordVectorIjE,comdat
	.weak	_ZTI13CRecordVectorIjE
	.p2align	4
_ZTI13CRecordVectorIjE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIjE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIjE, 24

	.type	_ZTV13CRecordVectorIyE,@object # @_ZTV13CRecordVectorIyE
	.section	.rodata._ZTV13CRecordVectorIyE,"aG",@progbits,_ZTV13CRecordVectorIyE,comdat
	.weak	_ZTV13CRecordVectorIyE
	.p2align	3
_ZTV13CRecordVectorIyE:
	.quad	0
	.quad	_ZTI13CRecordVectorIyE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIyED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIyE, 40

	.type	_ZTS13CRecordVectorIyE,@object # @_ZTS13CRecordVectorIyE
	.section	.rodata._ZTS13CRecordVectorIyE,"aG",@progbits,_ZTS13CRecordVectorIyE,comdat
	.weak	_ZTS13CRecordVectorIyE
	.p2align	4
_ZTS13CRecordVectorIyE:
	.asciz	"13CRecordVectorIyE"
	.size	_ZTS13CRecordVectorIyE, 19

	.type	_ZTI13CRecordVectorIyE,@object # @_ZTI13CRecordVectorIyE
	.section	.rodata._ZTI13CRecordVectorIyE,"aG",@progbits,_ZTI13CRecordVectorIyE,comdat
	.weak	_ZTI13CRecordVectorIyE
	.p2align	4
_ZTI13CRecordVectorIyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIyE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIyE, 24

	.type	_ZTV13CRecordVectorIbE,@object # @_ZTV13CRecordVectorIbE
	.section	.rodata._ZTV13CRecordVectorIbE,"aG",@progbits,_ZTV13CRecordVectorIbE,comdat
	.weak	_ZTV13CRecordVectorIbE
	.p2align	3
_ZTV13CRecordVectorIbE:
	.quad	0
	.quad	_ZTI13CRecordVectorIbE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIbED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIbE, 40

	.type	_ZTS13CRecordVectorIbE,@object # @_ZTS13CRecordVectorIbE
	.section	.rodata._ZTS13CRecordVectorIbE,"aG",@progbits,_ZTS13CRecordVectorIbE,comdat
	.weak	_ZTS13CRecordVectorIbE
	.p2align	4
_ZTS13CRecordVectorIbE:
	.asciz	"13CRecordVectorIbE"
	.size	_ZTS13CRecordVectorIbE, 19

	.type	_ZTI13CRecordVectorIbE,@object # @_ZTI13CRecordVectorIbE
	.section	.rodata._ZTI13CRecordVectorIbE,"aG",@progbits,_ZTI13CRecordVectorIbE,comdat
	.weak	_ZTI13CRecordVectorIbE
	.p2align	4
_ZTI13CRecordVectorIbE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIbE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIbE, 24


	.globl	_ZN8NArchive3N7z15CFolderInStreamC1Ev
	.type	_ZN8NArchive3N7z15CFolderInStreamC1Ev,@function
_ZN8NArchive3N7z15CFolderInStreamC1Ev = _ZN8NArchive3N7z15CFolderInStreamC2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
