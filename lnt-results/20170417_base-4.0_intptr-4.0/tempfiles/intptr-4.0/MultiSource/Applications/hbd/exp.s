	.text
	.file	"exp.bc"
	.globl	_Z7killexpP3Exp
	.p2align	4, 0x90
	.type	_Z7killexpP3Exp,@function
_Z7killexpP3Exp:                        # @_Z7killexpP3Exp
	.cfi_startproc
# BB#0:
	decl	8(%rdi)
	retq
.Lfunc_end0:
	.size	_Z7killexpP3Exp, .Lfunc_end0-_Z7killexpP3Exp
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	10                      # 0xa
	.long	34                      # 0x22
	.text
	.globl	_Z6notexpPP3Exp
	.p2align	4, 0x90
	.type	_Z6notexpPP3Exp,@function
_Z6notexpPP3Exp:                        # @_Z6notexpPP3Exp
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	(%r14), %rbx
	movq	(%rbx), %rax
	movl	12(%rax), %ecx
	leal	-10(%rcx), %edx
	cmpl	$26, %edx
	ja	.LBB1_7
# BB#1:
	jmpq	*.LJTI1_0(,%rdx,8)
.LBB1_5:
	leaq	24(%rbx), %rdi
	callq	_Z6notexpPP3Exp
	leaq	32(%rbx), %rdi
	callq	_Z6notexpPP3Exp
	movq	(%rbx), %rax
	movl	12(%rax), %ecx
.LBB1_6:
	xorl	$1, %ecx
	movl	%ecx, 12(%rax)
	jmp	.LBB1_13
.LBB1_7:
	cmpl	$10, 8(%rax)
	jne	.LBB1_8
# BB#9:
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r15
	movl	12(%rbx), %eax
	movl	16(%rbx), %ecx
	movl	$1, 8(%r15)
	movl	%eax, 12(%r15)
	movl	%ecx, 16(%r15)
.Ltmp0:
	movl	$24, %edi
	callq	_Znwm
.Ltmp1:
# BB#10:
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [0,2,10,34]
	movups	%xmm0, (%rax)
	movq	%rax, (%r15)
	movq	%rbx, 24(%r15)
	movq	$0, 32(%r15)
	movq	%r15, (%r14)
	jmp	.LBB1_13
.LBB1_12:
	movq	24(%rbx), %rax
	movq	%rax, (%r14)
	decl	8(%rbx)
	jmp	.LBB1_13
.LBB1_4:
	movl	$36, 12(%rax)
	jmp	.LBB1_3
.LBB1_2:
	movl	$35, 12(%rax)
.LBB1_3:
	leaq	24(%rbx), %rdi
	callq	_Z6notexpPP3Exp
	addq	$32, %rbx
	movq	%rbx, %rdi
	callq	_Z6notexpPP3Exp
.LBB1_13:
	xorl	%eax, %eax
.LBB1_14:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB1_8:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$24, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %eax
	jmp	.LBB1_14
.LBB1_11:
.Ltmp2:
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_Z6notexpPP3Exp, .Lfunc_end1-_Z6notexpPP3Exp
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_5
	.quad	.LBB1_5
	.quad	.LBB1_7
	.quad	.LBB1_7
	.quad	.LBB1_7
	.quad	.LBB1_7
	.quad	.LBB1_7
	.quad	.LBB1_7
	.quad	.LBB1_7
	.quad	.LBB1_7
	.quad	.LBB1_7
	.quad	.LBB1_7
	.quad	.LBB1_7
	.quad	.LBB1_7
	.quad	.LBB1_7
	.quad	.LBB1_7
	.quad	.LBB1_7
	.quad	.LBB1_7
	.quad	.LBB1_6
	.quad	.LBB1_6
	.quad	.LBB1_6
	.quad	.LBB1_6
	.quad	.LBB1_6
	.quad	.LBB1_6
	.quad	.LBB1_12
	.quad	.LBB1_4
	.quad	.LBB1_2
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end1-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN3Exp8toStringEj
	.p2align	4, 0x90
	.type	_ZN3Exp8toStringEj,@function
_ZN3Exp8toStringEj:                     # @_ZN3Exp8toStringEj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 96
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	(%r14), %rbp
	movl	4(%rbp), %edx
	leal	-1(%rdx), %eax
	cmpl	$8, %eax
	ja	.LBB2_26
# BB#1:
	jmpq	*.LJTI2_0(,%rax,8)
.LBB2_27:
	movq	16(%rbp), %rax
	movq	(%rax), %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, %rbx
	movq	16(%rbp), %rax
	movq	(%rax), %rsi
	movq	%rbx, %rdi
	callq	strcpy
	jmp	.LBB2_28
.LBB2_2:
	movl	8(%r14), %eax
	movq	24(%r14), %rdi
	movl	8(%rdi), %ecx
	leal	-1(%rax,%rcx), %eax
	movl	%eax, 8(%rdi)
	callq	_ZN3Exp8toStringEj
	movq	%rax, %r15
	movq	(%r14), %rbx
	movl	12(%rbx), %eax
	cmpq	$15, %rax
	jne	.LBB2_6
# BB#3:
	movl	8(%rbx), %eax
	cmpq	$8, %rax
	jne	.LBB2_5
# BB#4:
	movl	8(%r14), %eax
	movq	32(%r14), %rdi
	movl	8(%rdi), %ecx
	leal	-1(%rax,%rcx), %eax
	movl	%eax, 8(%rdi)
	callq	_ZN3Exp8toStringEj
	movq	%rax, %rbx
	movq	32(%r14), %rax
	decl	8(%rax)
	movq	%rbx, %rdi
	callq	strlen
	leaq	3(%rax), %rdi
	callq	_Znam
	movq	%rax, %r12
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	sprintf
	movq	%rbx, %rdi
	callq	_ZdaPv
	jmp	.LBB2_7
.LBB2_11:
	movl	8(%r14), %eax
	movq	24(%r14), %rdi
	movl	8(%rdi), %ecx
	leal	-1(%rax,%rcx), %eax
	movl	%eax, 8(%rdi)
	callq	_ZN3Exp8toStringEj
	movq	%rax, %r12
	movq	(%r14), %rax
	movl	12(%rax), %ebx
	movq	op2str(,%rbx,8), %r15
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%r12, %rdi
	callq	strlen
	leaq	5(%rbp,%rax), %rdi
	callq	_Znam
	movq	%rax, %rbp
	movq	24(%r14), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	movl	op_prec(,%rax,4), %eax
	cmpl	op_prec(,%rbx,4), %eax
	movl	$.L.str.4, %eax
	movl	$.L.str.3, %esi
	cmovlq	%rax, %rsi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r12, %rdx
	movq	%r15, %rcx
	callq	sprintf
	movq	24(%r14), %rax
	decl	8(%rax)
	jmp	.LBB2_12
.LBB2_14:
	movl	8(%r14), %eax
	movq	24(%r14), %rdi
	movl	8(%rdi), %ecx
	leal	-1(%rax,%rcx), %eax
	movl	%eax, 8(%rdi)
	movl	8(%r14), %eax
	movq	32(%r14), %rcx
	movl	8(%rcx), %edx
	leal	-1(%rax,%rdx), %eax
	movl	%eax, 8(%rcx)
	callq	_ZN3Exp8toStringEj
	movq	%rax, %r15
	movq	32(%r14), %rdi
	callq	_ZN3Exp8toStringEj
	movq	%rax, %r12
	movq	(%r14), %rbp
	movl	12(%rbp), %eax
	movq	op2str(,%rax,8), %r13
	movq	%r13, %rdi
	callq	strlen
	leaq	9(%rax), %rdi
	callq	_Znam
	movq	%rax, %rbx
	movq	24(%r14), %rax
	movq	32(%r14), %rcx
	movq	(%rax), %rax
	movl	12(%rax), %eax
	movl	12(%rbp), %edx
	movl	op_prec(,%rdx,4), %esi
	cmpl	%esi, op_prec(,%rax,4)
	movl	$.L.str.1, %eax
	movl	$.L.str.6, %r8d
	movl	$.L.str.6, %edx
	cmovlq	%rax, %rdx
	movq	(%rcx), %rcx
	movl	12(%rcx), %ecx
	cmpl	%esi, op_prec(,%rcx,4)
	cmovlq	%rax, %r8
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r13, %rcx
	callq	sprintf
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r13
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%r12, %rdi
	callq	strlen
	addq	%r13, %rbp
	leaq	-3(%rax,%rbp), %rdi
	callq	_Znam
	movq	%rax, %rbp
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	sprintf
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	24(%r14), %rax
	decl	8(%rax)
	movq	32(%r14), %rax
	decl	8(%rax)
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB2_12:
	movq	%r12, %rdi
	jmp	.LBB2_13
.LBB2_15:
	movl	8(%r14), %eax
	movq	24(%r14), %rdi
	movl	8(%rdi), %ecx
	leal	-1(%rax,%rcx), %eax
	movl	%eax, 8(%rdi)
	movl	8(%r14), %eax
	movq	32(%r14), %rcx
	movl	8(%rcx), %edx
	leal	-1(%rax,%rdx), %eax
	movl	%eax, 8(%rcx)
	movl	8(%r14), %eax
	movq	40(%r14), %rcx
	movl	8(%rcx), %edx
	leal	-1(%rax,%rdx), %eax
	movl	%eax, 8(%rcx)
	callq	_ZN3Exp8toStringEj
	movq	%rax, %r13
	movq	32(%r14), %rdi
	callq	_ZN3Exp8toStringEj
	movq	%rax, %r12
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	40(%r14), %rdi
	callq	_ZN3Exp8toStringEj
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	(%r14), %rbp
	movl	12(%rbp), %eax
	movslq	%eax, %rcx
	movq	op2str(,%rax,8), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	op2str+8(,%rcx,8), %r15
	movl	$19, %edi
	callq	_Znam
	movq	%rax, %rbx
	movq	24(%r14), %rax
	movq	32(%r14), %rcx
	movq	(%rax), %rax
	movl	12(%rax), %eax
	movl	12(%rbp), %edx
	movl	op_prec(,%rdx,4), %esi
	cmpl	%esi, op_prec(,%rax,4)
	movl	$.L.str.1, %eax
	movl	$.L.str.6, %edi
	movl	$.L.str.6, %edx
	cmovlq	%rax, %rdx
	movq	(%rcx), %rcx
	movl	12(%rcx), %ecx
	cmpl	%esi, op_prec(,%rcx,4)
	movl	$.L.str.6, %r8d
	cmovlq	%rax, %r8
	movq	40(%r14), %rcx
	movq	(%rcx), %rcx
	movl	12(%rcx), %ecx
	cmpl	%esi, op_prec(,%rcx,4)
	cmovlq	%rax, %rdi
	movq	%rdi, (%rsp)
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%r15, %r9
	callq	sprintf
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%r13, %rdi
	movq	%r13, %rbx
	callq	strlen
	movq	%rax, %r15
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %r12
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rdi
	callq	strlen
	addq	%rbp, %r15
	addq	%r12, %r15
	leaq	-5(%rax,%r15), %rdi
	callq	_Znam
	movq	%rax, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rsi
	movq	%rbx, %rdx
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rcx
	movq	%r13, %r8
	callq	sprintf
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	24(%r14), %rax
	decl	8(%rax)
	movq	32(%r14), %rax
	decl	8(%rax)
	movq	40(%r14), %rax
	decl	8(%rax)
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	%rbp, %rdi
	callq	_ZdaPv
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	8(%rsp), %rax           # 8-byte Reload
	jmp	.LBB2_29
.LBB2_16:
	movl	$256, %edi              # imm = 0x100
	callq	_Znam
	movq	%rax, %r15
	movl	8(%r14), %eax
	movq	24(%r14), %rdi
	movl	8(%rdi), %ecx
	leal	-1(%rax,%rcx), %eax
	movl	%eax, 8(%rdi)
	callq	_ZN3Exp8toStringEj
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %r12
	addq	$3, %r12
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbp, %rdx
	callq	sprintf
	movq	24(%r14), %rax
	decl	8(%rax)
	movq	%rbp, %rdi
	callq	_ZdaPv
	movslq	48(%r14), %rdx
	testq	%rdx, %rdx
	je	.LBB2_21
# BB#17:                                # %.preheader
	leaq	-1(%rdx), %rbx
	movl	8(%r14), %eax
	decl	%eax
	testl	%ebx, %ebx
	movq	56(%r14), %rcx
	je	.LBB2_20
# BB#18:                                # %.lr.ph.preheader
	shlq	$3, %rbx
	movl	$1, %r13d
	subl	%edx, %r13d
	.p2align	4, 0x90
.LBB2_19:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rbx), %rdi
	addl	%eax, 8(%rdi)
	callq	_ZN3Exp8toStringEj
	movq	%rax, %rbp
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	strcat
	movq	%rbp, %rdi
	callq	strlen
	movslq	%r12d, %rcx
	leaq	2(%rcx,%rax), %r12
	movq	56(%r14), %rax
	movq	(%rax,%rbx), %rax
	decl	8(%rax)
	movq	%rbp, %rdi
	callq	_ZdaPv
	movq	%r15, %rdi
	callq	strlen
	movb	$0, 2(%r15,%rax)
	movw	$8236, (%r15,%rax)      # imm = 0x202C
	movl	8(%r14), %eax
	decl	%eax
	movq	56(%r14), %rcx
	addq	$-8, %rbx
	incl	%r13d
	jne	.LBB2_19
.LBB2_20:                               # %._crit_edge
	movq	(%rcx), %rdi
	addl	%eax, 8(%rdi)
	callq	_ZN3Exp8toStringEj
	movq	%rax, %rbp
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	strcat
	movq	%rbp, %rdi
	callq	strlen
	movslq	%r12d, %r12
	addq	%rax, %r12
	movq	56(%r14), %rax
	movq	(%rax), %rax
	decl	8(%rax)
	movq	%rbp, %rdi
	callq	_ZdaPv
.LBB2_21:
	movq	%r15, %rdi
	callq	strlen
	movw	$41, (%r15,%rax)
	movq	%r12, %rax
	shlq	$32, %rax
	movslq	%r12d, %rax
	movq	$-1, %rdi
	cmovnsq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	strcpy
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB2_28
.LBB2_22:
	movl	8(%r14), %eax
	movq	24(%r14), %rdi
	movl	8(%rdi), %ecx
	leal	-1(%rax,%rcx), %eax
	movl	%eax, 8(%rdi)
	movl	8(%r14), %eax
	movq	32(%r14), %rcx
	movl	8(%rcx), %edx
	leal	-1(%rax,%rdx), %eax
	movl	%eax, 8(%rcx)
	callq	_ZN3Exp8toStringEj
	movq	%rax, %r15
	movq	32(%r14), %rdi
	callq	_ZN3Exp8toStringEj
	movq	%rax, %rbp
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	strlen
	leaq	3(%rbx,%rax), %rdi
	callq	_Znam
	movq	%rax, %rbx
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%rbp, %rcx
	callq	sprintf
	movq	24(%r14), %rax
	decl	8(%rax)
	movq	32(%r14), %rax
	decl	8(%rax)
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	%rbp, %rdi
	callq	_ZdaPv
	jmp	.LBB2_28
.LBB2_23:
	leaq	24(%r14), %rdi
	callq	_Z6notexpPP3Exp
	movl	8(%r14), %eax
	movq	24(%r14), %rdi
	movl	8(%rdi), %ecx
	leal	-1(%rax,%rcx), %eax
	movl	%eax, 8(%rdi)
	callq	_ZN3Exp8toStringEj
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	strlen
	leaq	8(%rax), %rdi
	callq	_Znam
	movq	%rax, %rbp
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	sprintf
	jmp	.LBB2_25
.LBB2_24:
	movl	8(%r14), %eax
	movq	24(%r14), %rdi
	movl	8(%rdi), %ecx
	leal	-1(%rax,%rcx), %eax
	movl	%eax, 8(%rdi)
	callq	_ZN3Exp8toStringEj
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	strlen
	leaq	29(%rax), %rdi
	callq	_Znam
	movq	%rax, %rbp
	movl	40(%r14), %ecx
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	sprintf
.LBB2_25:
	movq	24(%r14), %rax
	decl	8(%rax)
	movq	%rbx, %rdi
.LBB2_13:
	callq	_ZdaPv
	movq	%rbp, %rax
	jmp	.LBB2_29
.LBB2_6:
	movq	op2str(,%rax,8), %rdi
	callq	strdup
	movq	%rax, %r12
	jmp	.LBB2_7
.LBB2_5:
	movq	type2str(,%rax,8), %rdi
	callq	strlen
	leaq	3(%rax), %rdi
	callq	_Znam
	movq	%rax, %r12
	movl	8(%rbx), %eax
	movq	type2str(,%rax,8), %rdx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sprintf
.LBB2_7:
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	strlen
	leaq	5(%rbx,%rax), %rdi
	callq	_Znam
	movq	%rax, %rbx
	movq	(%r14), %rax
	movq	24(%r14), %rcx
	movq	(%rcx), %rcx
	movl	12(%rcx), %ecx
	movl	op_prec(,%rcx,4), %ecx
	movl	12(%rax), %eax
	cmpl	op_prec(,%rax,4), %ecx
	jge	.LBB2_9
# BB#8:
	movl	$.L.str.2, %esi
	jmp	.LBB2_10
.LBB2_9:
	movl	$.L.str.3, %esi
.LBB2_10:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%r15, %rcx
	callq	sprintf
	movq	24(%r14), %rax
	decl	8(%rax)
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	%r12, %rdi
	callq	free
.LBB2_28:
	movq	%rbx, %rax
.LBB2_29:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_26:
	movq	stderr(%rip), %rdi
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	movl	$-1, %edi
	callq	exit
.Lfunc_end2:
	.size	_ZN3Exp8toStringEj, .Lfunc_end2-_ZN3Exp8toStringEj
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_27
	.quad	.LBB2_2
	.quad	.LBB2_11
	.quad	.LBB2_14
	.quad	.LBB2_15
	.quad	.LBB2_16
	.quad	.LBB2_22
	.quad	.LBB2_23
	.quad	.LBB2_24

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	1                       # 0x1
	.long	8                       # 0x8
	.long	0                       # 0x0
	.long	39                      # 0x27
.LCPI3_1:
	.long	1                       # 0x1
	.long	5                       # 0x5
	.long	10                      # 0xa
	.long	24                      # 0x18
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_exp.ii,@function
_GLOBAL__sub_I_exp.ii:                  # @_GLOBAL__sub_I_exp.ii
	.cfi_startproc
# BB#0:
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, std_exps(%rip)
	movl	idnull+8(%rip), %ecx
	movl	%ecx, std_exps+8(%rip)
	movl	$39, std_exps+12(%rip)
	movq	$idnull, std_exps+16(%rip)
	movq	%rax, std_exps+24(%rip)
	movl	idneg1+8(%rip), %ecx
	movl	%ecx, std_exps+32(%rip)
	movl	$39, std_exps+36(%rip)
	movq	$idneg1, std_exps+40(%rip)
	movq	%rax, std_exps+48(%rip)
	movl	id0i+8(%rip), %ecx
	movl	%ecx, std_exps+56(%rip)
	movl	$39, std_exps+60(%rip)
	movq	$id0i, std_exps+64(%rip)
	movq	%rax, std_exps+72(%rip)
	movl	id1i+8(%rip), %ecx
	movl	%ecx, std_exps+80(%rip)
	movl	$39, std_exps+84(%rip)
	movq	$id1i, std_exps+88(%rip)
	movq	%rax, std_exps+96(%rip)
	movl	id2i+8(%rip), %ecx
	movl	%ecx, std_exps+104(%rip)
	movl	$39, std_exps+108(%rip)
	movq	$id2i, std_exps+112(%rip)
	movq	%rax, std_exps+120(%rip)
	movl	id3i+8(%rip), %ecx
	movl	%ecx, std_exps+128(%rip)
	movl	$39, std_exps+132(%rip)
	movq	$id3i, std_exps+136(%rip)
	movq	%rax, std_exps+144(%rip)
	movl	id4i+8(%rip), %ecx
	movl	%ecx, std_exps+152(%rip)
	movl	$39, std_exps+156(%rip)
	movq	$id4i, std_exps+160(%rip)
	movq	%rax, std_exps+168(%rip)
	movl	id5i+8(%rip), %ecx
	movl	%ecx, std_exps+176(%rip)
	movl	$39, std_exps+180(%rip)
	movq	$id5i, std_exps+184(%rip)
	movq	%rax, std_exps+192(%rip)
	movl	id0L+8(%rip), %ecx
	movl	%ecx, std_exps+200(%rip)
	movl	$39, std_exps+204(%rip)
	movq	$id0L, std_exps+208(%rip)
	movq	%rax, std_exps+216(%rip)
	movl	id1L+8(%rip), %ecx
	movl	%ecx, std_exps+224(%rip)
	movl	$39, std_exps+228(%rip)
	movq	$id1L, std_exps+232(%rip)
	movq	%rax, std_exps+240(%rip)
	movl	id0f+8(%rip), %ecx
	movl	%ecx, std_exps+248(%rip)
	movl	$39, std_exps+252(%rip)
	movq	$id0f, std_exps+256(%rip)
	movq	%rax, std_exps+264(%rip)
	movl	id1f+8(%rip), %ecx
	movl	%ecx, std_exps+272(%rip)
	movl	$39, std_exps+276(%rip)
	movq	$id1f, std_exps+280(%rip)
	movq	%rax, std_exps+288(%rip)
	movl	id2f+8(%rip), %ecx
	movl	%ecx, std_exps+296(%rip)
	movl	$39, std_exps+300(%rip)
	movq	$id2f, std_exps+304(%rip)
	movq	%rax, std_exps+312(%rip)
	movl	id0d+8(%rip), %ecx
	movl	%ecx, std_exps+320(%rip)
	movl	$39, std_exps+324(%rip)
	movq	$id0d, std_exps+328(%rip)
	movq	%rax, std_exps+336(%rip)
	movl	id1d+8(%rip), %ecx
	movl	%ecx, std_exps+344(%rip)
	movl	$39, std_exps+348(%rip)
	movq	$id1d, std_exps+352(%rip)
	movq	%rax, std_exps+360(%rip)
	movl	idfalse+8(%rip), %ecx
	movl	%ecx, std_exps+368(%rip)
	movl	$39, std_exps+372(%rip)
	movq	$idfalse, std_exps+376(%rip)
	movq	%rax, std_exps+384(%rip)
	movl	idtrue+8(%rip), %eax
	movl	%eax, std_exps+392(%rip)
	movl	$39, std_exps+396(%rip)
	movq	$idtrue, std_exps+400(%rip)
	movaps	.LCPI3_0(%rip), %xmm0   # xmm0 = [1,8,0,39]
	movups	%xmm0, std_exps+408(%rip)
	movq	$0, std_exps+424(%rip)
	movaps	.LCPI3_1(%rip), %xmm0   # xmm0 = [1,5,10,24]
	movaps	%xmm0, std_exps+432(%rip)
	movq	$0, std_exps+448(%rip)
	retq
.Lfunc_end3:
	.size	_GLOBAL__sub_I_exp.ii, .Lfunc_end3-_GLOBAL__sub_I_exp.ii
	.cfi_endproc

	.type	std_exps,@object        # @std_exps
	.bss
	.globl	std_exps
	.p2align	4
std_exps:
	.zero	456
	.size	std_exps, 456

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Can't not a non-boolean\n"
	.size	.L.str, 25

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"(%s)"
	.size	.L.str.1, 5

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%s(%s)"
	.size	.L.str.2, 7

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%s%s"
	.size	.L.str.3, 5

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"(%s)%s"
	.size	.L.str.4, 7

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%s%s%s"
	.size	.L.str.5, 7

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%s"
	.size	.L.str.6, 3

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%s%s%s%s%s"
	.size	.L.str.7, 11

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"%s("
	.size	.L.str.8, 4

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	", "
	.size	.L.str.9, 3

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%s[%s]"
	.size	.L.str.11, 7

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"if (%s) {"
	.size	.L.str.12, 10

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"switch (%s) default: label%d"
	.size	.L.str.13, 29

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Error converting expressions to strings. %d\n"
	.size	.L.str.14, 45

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_exp.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
