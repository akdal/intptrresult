	.text
	.file	"fasta.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	1208528896              # float 139968
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:                                 # %.lr.ph.i
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$72, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 112
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	xorps	%xmm0, %xmm0
	movss	main.iub(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, main.iub(%rip)
	addss	main.iub+8(%rip), %xmm1
	movss	%xmm1, main.iub+8(%rip)
	addss	main.iub+16(%rip), %xmm1
	movss	%xmm1, main.iub+16(%rip)
	addss	main.iub+24(%rip), %xmm1
	movss	%xmm1, main.iub+24(%rip)
	addss	main.iub+32(%rip), %xmm1
	movss	%xmm1, main.iub+32(%rip)
	addss	main.iub+40(%rip), %xmm1
	movss	%xmm1, main.iub+40(%rip)
	addss	main.iub+48(%rip), %xmm1
	movss	%xmm1, main.iub+48(%rip)
	addss	main.iub+56(%rip), %xmm1
	movss	%xmm1, main.iub+56(%rip)
	addss	main.iub+64(%rip), %xmm1
	movss	%xmm1, main.iub+64(%rip)
	addss	main.iub+72(%rip), %xmm1
	movss	%xmm1, main.iub+72(%rip)
	addss	main.iub+80(%rip), %xmm1
	movss	%xmm1, main.iub+80(%rip)
	addss	main.iub+88(%rip), %xmm1
	movss	%xmm1, main.iub+88(%rip)
	addss	main.iub+96(%rip), %xmm1
	movss	%xmm1, main.iub+96(%rip)
	addss	main.iub+104(%rip), %xmm1
	movss	%xmm1, main.iub+104(%rip)
	addss	main.iub+112(%rip), %xmm1
	movss	%xmm1, main.iub+112(%rip)
	addss	main.homosapiens(%rip), %xmm0
	movss	%xmm0, main.homosapiens(%rip)
	addss	main.homosapiens+8(%rip), %xmm0
	movss	%xmm0, main.homosapiens+8(%rip)
	addss	main.homosapiens+16(%rip), %xmm0
	movss	%xmm0, main.homosapiens+16(%rip)
	addss	main.homosapiens+24(%rip), %xmm0
	movss	%xmm0, main.homosapiens+24(%rip)
	movq	stdout(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$22, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$347, %edi              # imm = 0x15B
	callq	malloc
	movq	%rax, %r14
	movl	$.L.str, %esi
	movl	$287, %edx              # imm = 0x11F
	movq	%r14, %rdi
	callq	memcpy
	movups	.L.str+44(%rip), %xmm0
	movups	%xmm0, 331(%r14)
	movups	.L.str+32(%rip), %xmm0
	movups	%xmm0, 319(%r14)
	movups	.L.str+16(%rip), %xmm0
	movups	%xmm0, 303(%r14)
	movups	.L.str(%rip), %xmm0
	movups	%xmm0, 287(%r14)
	movl	$10000000, %r15d        # imm = 0x989680
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	cmpq	$60, %r15
	movl	$60, %r12d
	cmovbq	%r15, %r12
	leaq	(%r14,%rbx), %rdi
	movq	stdout(%rip), %rcx
	movl	$1, %esi
	movq	%r12, %rdx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	leaq	(%r12,%rbx), %rax
	cmpq	$286, %rax              # imm = 0x11E
	leaq	-287(%r12,%rbx), %rbx
	cmovbeq	%rax, %rbx
	subq	%r12, %r15
	jne	.LBB0_1
# BB#2:                                 # %repeat_fasta.exit
	movq	%r14, %rdi
	callq	free
	movq	stdout(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$25, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$15000000, %r15d        # imm = 0xE4E1C0
	movabsq	$4318579316753219217, %r12 # imm = 0x3BEEAD01FD6CBE91
	movq	%rsp, %r14
	.p2align	4, 0x90
.LBB0_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
                                        #       Child Loop BB0_5 Depth 3
	cmpq	$60, %r15
	movl	$60, %ebx
	cmovbq	%r15, %rbx
	movq	myrandom.last(%rip), %rcx
	xorl	%esi, %esi
	movss	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB0_4:                                #   Parent Loop BB0_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_5 Depth 3
	imulq	$3877, %rcx, %rcx       # imm = 0xF25
	addq	$29573, %rcx            # imm = 0x7385
	movq	%rcx, %rax
	mulq	%r12
	shrq	$15, %rdx
	imulq	$139968, %rdx, %rax     # imm = 0x222C0
	subq	%rax, %rcx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ecx, %xmm0
	divss	%xmm1, %xmm0
	movl	$main.iub, %eax
	.p2align	4, 0x90
.LBB0_5:                                #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	ucomiss	(%rax), %xmm0
	leaq	8(%rax), %rax
	ja	.LBB0_5
# BB#6:                                 #   in Loop: Header=BB0_4 Depth=2
	movb	-4(%rax), %al
	movb	%al, (%rsp,%rsi)
	incq	%rsi
	cmpq	%rbx, %rsi
	jb	.LBB0_4
# BB#7:                                 #   in Loop: Header=BB0_3 Depth=1
	movq	%rcx, myrandom.last(%rip)
	movb	$10, (%rsp,%rbx)
	leaq	1(%rbx), %rdx
	movq	stdout(%rip), %rcx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	fwrite
	subq	%rbx, %r15
	jne	.LBB0_3
# BB#8:                                 # %random_fasta.exit13
	movq	stdout(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$30, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$25000000, %r15d        # imm = 0x17D7840
	movq	%rsp, %r14
	.p2align	4, 0x90
.LBB0_9:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_10 Depth 2
                                        #       Child Loop BB0_11 Depth 3
	cmpq	$60, %r15
	movl	$60, %ebx
	cmovbq	%r15, %rbx
	movq	myrandom.last(%rip), %rcx
	xorl	%esi, %esi
	movss	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB0_10:                               #   Parent Loop BB0_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_11 Depth 3
	imulq	$3877, %rcx, %rcx       # imm = 0xF25
	addq	$29573, %rcx            # imm = 0x7385
	movq	%rcx, %rax
	mulq	%r12
	shrq	$15, %rdx
	imulq	$139968, %rdx, %rax     # imm = 0x222C0
	subq	%rax, %rcx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ecx, %xmm0
	divss	%xmm1, %xmm0
	movl	$main.homosapiens, %eax
	.p2align	4, 0x90
.LBB0_11:                               #   Parent Loop BB0_9 Depth=1
                                        #     Parent Loop BB0_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	ucomiss	(%rax), %xmm0
	leaq	8(%rax), %rax
	ja	.LBB0_11
# BB#12:                                #   in Loop: Header=BB0_10 Depth=2
	movb	-4(%rax), %al
	movb	%al, (%rsp,%rsi)
	incq	%rsi
	cmpq	%rbx, %rsi
	jb	.LBB0_10
# BB#13:                                #   in Loop: Header=BB0_9 Depth=1
	movq	%rcx, myrandom.last(%rip)
	movb	$10, (%rsp,%rbx)
	leaq	1(%rbx), %rdx
	movq	stdout(%rip), %rcx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	fwrite
	subq	%rbx, %r15
	jne	.LBB0_9
# BB#14:                                # %random_fasta.exit
	xorl	%eax, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	main.iub,@object        # @main.iub
	.data
	.p2align	4
main.iub:
	.long	1049247089              # float 0.270000011
	.byte	97                      # 0x61
	.zero	3
	.long	1039516303              # float 0.119999997
	.byte	99                      # 0x63
	.zero	3
	.long	1039516303              # float 0.119999997
	.byte	103                     # 0x67
	.zero	3
	.long	1049247089              # float 0.270000011
	.byte	116                     # 0x74
	.zero	3
	.long	1017370378              # float 0.0199999996
	.byte	66                      # 0x42
	.zero	3
	.long	1017370378              # float 0.0199999996
	.byte	68                      # 0x44
	.zero	3
	.long	1017370378              # float 0.0199999996
	.byte	72                      # 0x48
	.zero	3
	.long	1017370378              # float 0.0199999996
	.byte	75                      # 0x4b
	.zero	3
	.long	1017370378              # float 0.0199999996
	.byte	77                      # 0x4d
	.zero	3
	.long	1017370378              # float 0.0199999996
	.byte	78                      # 0x4e
	.zero	3
	.long	1017370378              # float 0.0199999996
	.byte	82                      # 0x52
	.zero	3
	.long	1017370378              # float 0.0199999996
	.byte	83                      # 0x53
	.zero	3
	.long	1017370378              # float 0.0199999996
	.byte	86                      # 0x56
	.zero	3
	.long	1017370378              # float 0.0199999996
	.byte	87                      # 0x57
	.zero	3
	.long	1017370378              # float 0.0199999996
	.byte	89                      # 0x59
	.zero	3
	.size	main.iub, 120

	.type	main.homosapiens,@object # @main.homosapiens
	.p2align	4
main.homosapiens:
	.long	1050352873              # float 0.302954942
	.byte	97                      # 0x61
	.zero	3
	.long	1045085554              # float 0.197988302
	.byte	99                      # 0x63
	.zero	3
	.long	1045055959              # float 0.197547302
	.byte	103                     # 0x67
	.zero	3
	.long	1050304370              # float 0.30150944
	.byte	116                     # 0x74
	.zero	3
	.size	main.homosapiens, 32

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGGGAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGACCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAATACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCAGCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGGAGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCCAGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA"
	.size	.L.str, 288

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	">ONE Homo sapiens alu\n"
	.size	.L.str.1, 23

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	">TWO IUB ambiguity codes\n"
	.size	.L.str.2, 26

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	">THREE Homo sapiens frequency\n"
	.size	.L.str.3, 31

	.type	myrandom.last,@object   # @myrandom.last
	.data
	.p2align	3
myrandom.last:
	.quad	42                      # 0x2a
	.size	myrandom.last, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
