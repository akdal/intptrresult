	.text
	.file	"endgame.bc"
	.globl	endgame
	.p2align	4, 0x90
	.type	endgame,@function
endgame:                                # @endgame
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 80
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movl	$.Lstr, %edi
	callq	puts
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$.Lstr.5, %edi
	callq	puts
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	leaq	22(%rsp), %rbx
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	scanf
	movl	$.L.str.10, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_8
# BB#1:                                 # %.lr.ph97.preheader
	leaq	22(%rsp), %rbx
	leaq	12(%rsp), %r14
	leaq	16(%rsp), %r15
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph97
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	getij
	movslq	12(%rsp), %rcx
	movslq	16(%rsp), %rax
	imulq	$19, %rcx, %rcx
	movzbl	p(%rcx,%rax), %edx
	cmpl	mymove(%rip), %edx
	jne	.LBB0_4
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	$mk, %edx
	jmp	.LBB0_6
	.p2align	4, 0x90
.LBB0_4:                                #   in Loop: Header=BB0_2 Depth=1
	cmpl	umove(%rip), %edx
	jne	.LBB0_7
# BB#5:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	$uk, %edx
.LBB0_6:                                # %.sink.split
                                        #   in Loop: Header=BB0_2 Depth=1
	leaq	p(%rcx,%rax), %rax
	movb	$0, (%rax)
	incl	(%rdx)
.LBB0_7:                                #   in Loop: Header=BB0_2 Depth=1
	callq	showboard
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	scanf
	movl	$.L.str.10, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB0_2
.LBB0_8:                                # %._crit_edge
	xorl	%ebx, %ebx
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.6, %edi
	callq	puts
	movl	$.Lstr.7, %edi
	callq	puts
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	printf
	leaq	22(%rsp), %rbp
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	scanf
	movl	$.L.str.10, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_11
# BB#9:
	leaq	22(%rsp), %r14
	leaq	12(%rsp), %r15
	leaq	16(%rsp), %r12
	.p2align	4, 0x90
.LBB0_10:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	getij
	movzbl	umove(%rip), %eax
	movslq	12(%rsp), %rcx
	movslq	16(%rsp), %rdx
	imulq	$19, %rcx, %rcx
	movb	%al, p(%rcx,%rdx)
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	scanf
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	getij
	movzbl	mymove(%rip), %eax
	movslq	12(%rsp), %rcx
	movslq	16(%rsp), %rdx
	imulq	$19, %rcx, %rcx
	movb	%al, p(%rcx,%rdx)
	callq	showboard
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	scanf
	movl	$.L.str.10, %esi
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB0_10
.LBB0_11:                               # %.thread.preheader
	movl	$0, 12(%rsp)
	.p2align	4, 0x90
.LBB0_12:                               # %.preheader86
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_13 Depth 2
	movl	$0, 16(%rsp)
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_13:                               #   Parent Loop BB0_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebx, %rax
	movslq	%esi, %rcx
	imulq	$19, %rax, %rax
	cmpb	$0, p(%rax,%rcx)
	jne	.LBB0_15
# BB#14:                                #   in Loop: Header=BB0_13 Depth=2
	movl	%ebx, %edi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	findcolor
	movslq	12(%rsp), %rbx
	movslq	16(%rsp), %rsi
	imulq	$19, %rbx, %rcx
	movb	%al, p(%rcx,%rsi)
.LBB0_15:                               #   in Loop: Header=BB0_13 Depth=2
	incl	%esi
	movl	%esi, 16(%rsp)
	cmpl	$19, %esi
	jl	.LBB0_13
# BB#16:                                # %.thread
                                        #   in Loop: Header=BB0_12 Depth=1
	incl	%ebx
	movl	%ebx, 12(%rsp)
	cmpl	$19, %ebx
	jl	.LBB0_12
# BB#17:                                # %.preheader85
	movl	$0, 12(%rsp)
	movl	mymove(%rip), %r8d
	movl	umove(%rip), %r9d
	movd	%r8d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movd	%r9d, %xmm1
	pshufd	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	xorl	%ebx, %ebx
	movq	$-361, %rdx             # imm = 0xFE97
	pxor	%xmm2, %xmm2
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_18:                               # %.loopexit117
                                        # =>This Inner Loop Header: Depth=1
	movd	%ebx, %xmm3
	movd	%ebp, %xmm4
	movd	p+361(%rdx), %xmm5      # xmm5 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3],xmm5[4],xmm2[4],xmm5[5],xmm2[5],xmm5[6],xmm2[6],xmm5[7],xmm2[7]
	punpcklwd	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3]
	movdqa	%xmm5, %xmm6
	pcmpeqd	%xmm1, %xmm6
	pcmpeqd	%xmm0, %xmm5
	psubd	%xmm5, %xmm3
	pandn	%xmm6, %xmm5
	psubd	%xmm5, %xmm4
	movd	p+365(%rdx), %xmm5      # xmm5 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3],xmm5[4],xmm2[4],xmm5[5],xmm2[5],xmm5[6],xmm2[6],xmm5[7],xmm2[7]
	punpcklwd	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3]
	movdqa	%xmm5, %xmm6
	pcmpeqd	%xmm1, %xmm6
	pcmpeqd	%xmm0, %xmm5
	psubd	%xmm5, %xmm3
	pandn	%xmm6, %xmm5
	psubd	%xmm5, %xmm4
	movd	p+369(%rdx), %xmm5      # xmm5 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3],xmm5[4],xmm2[4],xmm5[5],xmm2[5],xmm5[6],xmm2[6],xmm5[7],xmm2[7]
	punpcklwd	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3]
	movdqa	%xmm5, %xmm6
	pcmpeqd	%xmm1, %xmm6
	pcmpeqd	%xmm0, %xmm5
	psubd	%xmm5, %xmm3
	pandn	%xmm6, %xmm5
	psubd	%xmm5, %xmm4
	movd	p+373(%rdx), %xmm5      # xmm5 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3],xmm5[4],xmm2[4],xmm5[5],xmm2[5],xmm5[6],xmm2[6],xmm5[7],xmm2[7]
	punpcklwd	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3]
	movdqa	%xmm5, %xmm6
	pcmpeqd	%xmm1, %xmm6
	pcmpeqd	%xmm0, %xmm5
	psubd	%xmm5, %xmm3
	pandn	%xmm6, %xmm5
	psubd	%xmm5, %xmm4
	pshufd	$78, %xmm4, %xmm5       # xmm5 = xmm4[2,3,0,1]
	paddd	%xmm4, %xmm5
	pshufd	$78, %xmm3, %xmm4       # xmm4 = xmm3[2,3,0,1]
	paddd	%xmm3, %xmm4
	pshufd	$229, %xmm4, %xmm3      # xmm3 = xmm4[1,1,2,3]
	paddd	%xmm4, %xmm3
	movd	%xmm3, %esi
	pshufd	$229, %xmm5, %xmm3      # xmm3 = xmm5[1,1,2,3]
	paddd	%xmm5, %xmm3
	movd	%xmm3, %edi
	movzbl	p+377(%rdx), %ebp
	cmpl	%r9d, %ebp
	sete	%bl
	xorl	%eax, %eax
	cmpl	%r8d, %ebp
	sete	%al
	setne	%cl
	andb	%bl, %cl
	movzbl	%cl, %ecx
	addl	%edi, %ecx
	addl	%esi, %eax
	movzbl	p+378(%rdx), %esi
	cmpl	%r9d, %esi
	sete	%r10b
	xorl	%edi, %edi
	cmpl	%r8d, %esi
	sete	%dil
	setne	%bl
	andb	%r10b, %bl
	movzbl	%bl, %esi
	addl	%ecx, %esi
	addl	%eax, %edi
	movzbl	p+379(%rdx), %eax
	cmpl	%r9d, %eax
	sete	%cl
	xorl	%ebx, %ebx
	cmpl	%r8d, %eax
	sete	%bl
	setne	%al
	andb	%cl, %al
	movzbl	%al, %ebp
	addl	%esi, %ebp
	addl	%edi, %ebx
	addq	$19, %rdx
	jne	.LBB0_18
# BB#19:
	movl	$19, 16(%rsp)
	movl	$19, 12(%rsp)
	callq	showboard
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	endgame, .Lfunc_end0-endgame
	.cfi_endproc

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"Second, I need you to fill in neutral territories with "
	.size	.L.str.2, 56

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\nFirst, you should enter the dead pieces (blank and white) to"
	.size	.L.str.5, 62

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Dead piece? "
	.size	.L.str.8, 13

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%s"
	.size	.L.str.9, 3

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"stop"
	.size	.L.str.10, 5

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Next, you need to fill in pieces (black and white) in all neutral"
	.size	.L.str.11, 66

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Your piece? "
	.size	.L.str.14, 13

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"My piece? "
	.size	.L.str.15, 11

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Your total number of pieces %d\n"
	.size	.L.str.16, 32

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"My total number of pieces %d\n"
	.size	.L.str.17, 30

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"\nTo count score, we need the following steps:"
	.size	.Lstr, 46

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"First, I need you to remove all dead pieces on the board."
	.size	.Lstr.1, 58

	.type	.Lstr.2,@object         # @str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.2:
	.asciz	"pieces."
	.size	.Lstr.2, 8

	.type	.Lstr.3,@object         # @str.3
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.3:
	.asciz	"Last, I will fill in all pieces and anounce the winner."
	.size	.Lstr.3, 56

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	" be removed.  Enter"
	.size	.Lstr.4, 20

	.type	.Lstr.5,@object         # @str.5
	.p2align	4
.Lstr.5:
	.asciz	" 'stop' when you have finished."
	.size	.Lstr.5, 32

	.type	.Lstr.6,@object         # @str.6
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.6:
	.asciz	" territories."
	.size	.Lstr.6, 14

	.type	.Lstr.7,@object         # @str.7
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.7:
	.asciz	"Enter your and my pieces alternately and enter 'stop' when finish"
	.size	.Lstr.7, 66


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
