	.text
	.file	"btTriangleIndexVertexArray.bc"
	.globl	_ZN26btTriangleIndexVertexArrayC2EiPiiiPfi
	.p2align	4, 0x90
	.type	_ZN26btTriangleIndexVertexArrayC2EiPiiiPfi,@function
_ZN26btTriangleIndexVertexArrayC2EiPiiiPfi: # @_ZN26btTriangleIndexVertexArrayC2EiPiiiPfi
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$56, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 80
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	80(%rsp), %eax
	movl	$1065353216, 8(%rbx)    # imm = 0x3F800000
	movl	$1065353216, 12(%rbx)   # imm = 0x3F800000
	movl	$1065353216, 16(%rbx)   # imm = 0x3F800000
	movl	$0, 20(%rbx)
	movq	$_ZTV26btTriangleIndexVertexArray+16, (%rbx)
	movb	$1, 48(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 28(%rbx)
	movl	$0, 64(%rbx)
	movl	$2, 44(%rsp)
	movl	$0, 48(%rsp)
	movl	%esi, 8(%rsp)
	movq	%rdx, 16(%rsp)
	movl	%ecx, 24(%rsp)
	movl	%r8d, 28(%rsp)
	movq	%r9, 32(%rsp)
	movl	%eax, 40(%rsp)
.Ltmp0:
	leaq	8(%rsp), %rsi
	movl	$2, %edx
	callq	_ZN26btTriangleIndexVertexArray14addIndexedMeshERK13btIndexedMesh14PHY_ScalarType
.Ltmp1:
# BB#1:
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_2:
.Ltmp2:
	movq	%rax, %r14
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_6
# BB#3:
	cmpb	$0, 48(%rbx)
	je	.LBB0_5
# BB#4:
.Ltmp3:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
.LBB0_5:                                # %.noexc
	movq	$0, 40(%rbx)
.LBB0_6:
	movb	$1, 48(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 28(%rbx)
.Ltmp5:
	movq	%rbx, %rdi
	callq	_ZN23btStridingMeshInterfaceD2Ev
.Ltmp6:
# BB#7:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_8:
.Ltmp7:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN26btTriangleIndexVertexArrayC2EiPiiiPfi, .Lfunc_end0-_ZN26btTriangleIndexVertexArrayC2EiPiiiPfi
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\257\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp6-.Ltmp3           #   Call between .Ltmp3 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	1                       #   On action: 1
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp6      #   Call between .Ltmp6 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN26btTriangleIndexVertexArray14addIndexedMeshERK13btIndexedMesh14PHY_ScalarType,"axG",@progbits,_ZN26btTriangleIndexVertexArray14addIndexedMeshERK13btIndexedMesh14PHY_ScalarType,comdat
	.weak	_ZN26btTriangleIndexVertexArray14addIndexedMeshERK13btIndexedMesh14PHY_ScalarType
	.p2align	4, 0x90
	.type	_ZN26btTriangleIndexVertexArray14addIndexedMeshERK13btIndexedMesh14PHY_ScalarType,@function
_ZN26btTriangleIndexVertexArray14addIndexedMeshERK13btIndexedMesh14PHY_ScalarType: # @_ZN26btTriangleIndexVertexArray14addIndexedMeshERK13btIndexedMesh14PHY_ScalarType
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 48
.Lcfi10:
	.cfi_offset %rbx, -48
.Lcfi11:
	.cfi_offset %r12, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	28(%rbx), %eax
	cmpl	32(%rbx), %eax
	jne	.LBB1_18
# BB#1:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %r12d
	cmovnel	%ecx, %r12d
	cmpl	%r12d, %eax
	jge	.LBB1_18
# BB#2:
	testl	%r12d, %r12d
	je	.LBB1_3
# BB#4:
	movslq	%r12d, %rax
	shlq	$4, %rax
	leaq	(%rax,%rax,2), %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
	movl	28(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB1_6
	jmp	.LBB1_13
.LBB1_3:
	xorl	%ebp, %ebp
	testl	%eax, %eax
	jle	.LBB1_13
.LBB1_6:                                # %.lr.ph.i.i.i
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB1_7
# BB#8:                                 # %.prol.preheader
	xorl	%edi, %edi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_9:                                # =>This Inner Loop Header: Depth=1
	movq	40(%rbx), %rcx
	movups	(%rcx,%rdi), %xmm0
	movups	16(%rcx,%rdi), %xmm1
	movups	32(%rcx,%rdi), %xmm2
	movups	%xmm2, 32(%rbp,%rdi)
	movups	%xmm1, 16(%rbp,%rdi)
	movups	%xmm0, (%rbp,%rdi)
	incq	%rdx
	addq	$48, %rdi
	cmpq	%rdx, %rsi
	jne	.LBB1_9
	jmp	.LBB1_10
.LBB1_7:
	xorl	%edx, %edx
.LBB1_10:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB1_13
# BB#11:                                # %.lr.ph.i.i.i.new
	subq	%rdx, %rax
	leaq	(%rdx,%rdx,2), %rcx
	shlq	$4, %rcx
	addq	$144, %rcx
	.p2align	4, 0x90
.LBB1_12:                               # =>This Inner Loop Header: Depth=1
	movq	40(%rbx), %rdx
	movups	-144(%rdx,%rcx), %xmm0
	movups	-128(%rdx,%rcx), %xmm1
	movups	-112(%rdx,%rcx), %xmm2
	movups	%xmm2, -112(%rbp,%rcx)
	movups	%xmm1, -128(%rbp,%rcx)
	movups	%xmm0, -144(%rbp,%rcx)
	movq	40(%rbx), %rdx
	movups	-96(%rdx,%rcx), %xmm0
	movups	-80(%rdx,%rcx), %xmm1
	movups	-64(%rdx,%rcx), %xmm2
	movups	%xmm2, -64(%rbp,%rcx)
	movups	%xmm1, -80(%rbp,%rcx)
	movups	%xmm0, -96(%rbp,%rcx)
	movq	40(%rbx), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	-32(%rdx,%rcx), %xmm1
	movups	-16(%rdx,%rcx), %xmm2
	movups	%xmm2, -16(%rbp,%rcx)
	movups	%xmm1, -32(%rbp,%rcx)
	movups	%xmm0, -48(%rbp,%rcx)
	movq	40(%rbx), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	16(%rdx,%rcx), %xmm1
	movups	32(%rdx,%rcx), %xmm2
	movups	%xmm2, 32(%rbp,%rcx)
	movups	%xmm1, 16(%rbp,%rcx)
	movups	%xmm0, (%rbp,%rcx)
	addq	$192, %rcx
	addq	$-4, %rax
	jne	.LBB1_12
.LBB1_13:                               # %_ZNK20btAlignedObjectArrayI13btIndexedMeshE4copyEiiPS0_.exit.i.i
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_17
# BB#14:
	cmpb	$0, 48(%rbx)
	je	.LBB1_16
# BB#15:
	callq	_Z21btAlignedFreeInternalPv
.LBB1_16:
	movq	$0, 40(%rbx)
.LBB1_17:                               # %_ZN20btAlignedObjectArrayI13btIndexedMeshE10deallocateEv.exit.i.i
	movb	$1, 48(%rbx)
	movq	%rbp, 40(%rbx)
	movl	%r12d, 32(%rbx)
	movl	28(%rbx), %eax
.LBB1_18:                               # %_ZN20btAlignedObjectArrayI13btIndexedMeshE9push_backERKS0_.exit
	movq	40(%rbx), %rcx
	cltq
	leaq	(%rax,%rax,2), %rax
	shlq	$4, %rax
	movups	(%r15), %xmm0
	movups	16(%r15), %xmm1
	movups	32(%r15), %xmm2
	movups	%xmm2, 32(%rcx,%rax)
	movups	%xmm1, 16(%rcx,%rax)
	movups	%xmm0, (%rcx,%rax)
	movslq	28(%rbx), %rax
	leal	1(%rax), %ecx
	movl	%ecx, 28(%rbx)
	movq	40(%rbx), %rcx
	leaq	(%rax,%rax,2), %rax
	shlq	$4, %rax
	movl	%r14d, 36(%rcx,%rax)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN26btTriangleIndexVertexArray14addIndexedMeshERK13btIndexedMesh14PHY_ScalarType, .Lfunc_end1-_ZN26btTriangleIndexVertexArray14addIndexedMeshERK13btIndexedMesh14PHY_ScalarType
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.text
	.globl	_ZN26btTriangleIndexVertexArrayD2Ev
	.p2align	4, 0x90
	.type	_ZN26btTriangleIndexVertexArrayD2Ev,@function
_ZN26btTriangleIndexVertexArrayD2Ev:    # @_ZN26btTriangleIndexVertexArrayD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV26btTriangleIndexVertexArray+16, (%rbx)
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_4
# BB#1:
	cmpb	$0, 48(%rbx)
	je	.LBB3_3
# BB#2:
.Ltmp8:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp9:
.LBB3_3:                                # %.noexc
	movq	$0, 40(%rbx)
.LBB3_4:
	movb	$1, 48(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 28(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN23btStridingMeshInterfaceD2Ev # TAILCALL
.LBB3_5:
.Ltmp10:
	movq	%rax, %r14
.Ltmp11:
	movq	%rbx, %rdi
	callq	_ZN23btStridingMeshInterfaceD2Ev
.Ltmp12:
# BB#6:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_7:
.Ltmp13:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN26btTriangleIndexVertexArrayD2Ev, .Lfunc_end3-_ZN26btTriangleIndexVertexArrayD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp8-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin1   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp11-.Ltmp9          #   Call between .Ltmp9 and .Ltmp11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin1   #     jumps to .Ltmp13
	.byte	1                       #   On action: 1
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp12     #   Call between .Ltmp12 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN26btTriangleIndexVertexArrayD0Ev
	.p2align	4, 0x90
	.type	_ZN26btTriangleIndexVertexArrayD0Ev,@function
_ZN26btTriangleIndexVertexArrayD0Ev:    # @_ZN26btTriangleIndexVertexArrayD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 32
.Lcfi23:
	.cfi_offset %rbx, -24
.Lcfi24:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV26btTriangleIndexVertexArray+16, (%rbx)
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_4
# BB#1:
	cmpb	$0, 48(%rbx)
	je	.LBB4_3
# BB#2:
.Ltmp14:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp15:
.LBB4_3:                                # %.noexc.i
	movq	$0, 40(%rbx)
.LBB4_4:
	movb	$1, 48(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 28(%rbx)
.Ltmp20:
	movq	%rbx, %rdi
	callq	_ZN23btStridingMeshInterfaceD2Ev
.Ltmp21:
# BB#5:                                 # %_ZN26btTriangleIndexVertexArrayD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB4_6:
.Ltmp16:
	movq	%rax, %r14
.Ltmp17:
	movq	%rbx, %rdi
	callq	_ZN23btStridingMeshInterfaceD2Ev
.Ltmp18:
	jmp	.LBB4_9
.LBB4_7:
.Ltmp19:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB4_8:
.Ltmp22:
	movq	%rax, %r14
.LBB4_9:                                # %.body
.Ltmp23:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp24:
# BB#10:                                # %_ZN26btTriangleIndexVertexArraydlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB4_11:
.Ltmp25:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN26btTriangleIndexVertexArrayD0Ev, .Lfunc_end4-_ZN26btTriangleIndexVertexArrayD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp14-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin2   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin2   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp17-.Ltmp21         #   Call between .Ltmp21 and .Ltmp17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin2   #     jumps to .Ltmp19
	.byte	1                       #   On action: 1
	.long	.Ltmp23-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin2   #     jumps to .Ltmp25
	.byte	1                       #   On action: 1
	.long	.Ltmp24-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Lfunc_end4-.Ltmp24     #   Call between .Ltmp24 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN26btTriangleIndexVertexArray24getLockedVertexIndexBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i
	.p2align	4, 0x90
	.type	_ZN26btTriangleIndexVertexArray24getLockedVertexIndexBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i,@function
_ZN26btTriangleIndexVertexArray24getLockedVertexIndexBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i: # @_ZN26btTriangleIndexVertexArray24getLockedVertexIndexBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 24
.Lcfi27:
	.cfi_offset %rbx, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	40(%rsp), %r10
	movq	24(%rsp), %r11
	movq	32(%rsp), %rax
	movq	40(%rdi), %rdi
	movslq	48(%rsp), %rbx
	leaq	(%rbx,%rbx,2), %rbx
	shlq	$4, %rbx
	movl	20(%rdi,%rbx), %ebp
	movl	%ebp, (%rdx)
	movq	24(%rdi,%rbx), %rdx
	movq	%rdx, (%rsi)
	movl	40(%rdi,%rbx), %edx
	movl	%edx, (%rcx)
	movl	32(%rdi,%rbx), %ecx
	movl	%ecx, (%r8)
	movl	(%rdi,%rbx), %ecx
	movl	%ecx, (%rax)
	movq	8(%rdi,%rbx), %rax
	movq	%rax, (%r9)
	movl	16(%rdi,%rbx), %eax
	movl	%eax, (%r11)
	movl	36(%rdi,%rbx), %eax
	movl	%eax, (%r10)
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN26btTriangleIndexVertexArray24getLockedVertexIndexBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i, .Lfunc_end5-_ZN26btTriangleIndexVertexArray24getLockedVertexIndexBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i
	.cfi_endproc

	.globl	_ZNK26btTriangleIndexVertexArray32getLockedReadOnlyVertexIndexBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i
	.p2align	4, 0x90
	.type	_ZNK26btTriangleIndexVertexArray32getLockedReadOnlyVertexIndexBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i,@function
_ZNK26btTriangleIndexVertexArray32getLockedReadOnlyVertexIndexBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i: # @_ZNK26btTriangleIndexVertexArray32getLockedReadOnlyVertexIndexBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 24
.Lcfi31:
	.cfi_offset %rbx, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	40(%rsp), %r10
	movq	24(%rsp), %r11
	movq	32(%rsp), %rax
	movq	40(%rdi), %rdi
	movslq	48(%rsp), %rbx
	leaq	(%rbx,%rbx,2), %rbx
	shlq	$4, %rbx
	movl	20(%rdi,%rbx), %ebp
	movl	%ebp, (%rdx)
	movq	24(%rdi,%rbx), %rdx
	movq	%rdx, (%rsi)
	movl	40(%rdi,%rbx), %edx
	movl	%edx, (%rcx)
	movl	32(%rdi,%rbx), %ecx
	movl	%ecx, (%r8)
	movl	(%rdi,%rbx), %ecx
	movl	%ecx, (%rax)
	movq	8(%rdi,%rbx), %rax
	movq	%rax, (%r9)
	movl	16(%rdi,%rbx), %eax
	movl	%eax, (%r11)
	movl	36(%rdi,%rbx), %eax
	movl	%eax, (%r10)
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZNK26btTriangleIndexVertexArray32getLockedReadOnlyVertexIndexBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i, .Lfunc_end6-_ZNK26btTriangleIndexVertexArray32getLockedReadOnlyVertexIndexBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i
	.cfi_endproc

	.globl	_ZNK26btTriangleIndexVertexArray14hasPremadeAabbEv
	.p2align	4, 0x90
	.type	_ZNK26btTriangleIndexVertexArray14hasPremadeAabbEv,@function
_ZNK26btTriangleIndexVertexArray14hasPremadeAabbEv: # @_ZNK26btTriangleIndexVertexArray14hasPremadeAabbEv
	.cfi_startproc
# BB#0:
	cmpl	$1, 64(%rdi)
	sete	%al
	retq
.Lfunc_end7:
	.size	_ZNK26btTriangleIndexVertexArray14hasPremadeAabbEv, .Lfunc_end7-_ZNK26btTriangleIndexVertexArray14hasPremadeAabbEv
	.cfi_endproc

	.globl	_ZNK26btTriangleIndexVertexArray14setPremadeAabbERK9btVector3S2_
	.p2align	4, 0x90
	.type	_ZNK26btTriangleIndexVertexArray14setPremadeAabbERK9btVector3S2_,@function
_ZNK26btTriangleIndexVertexArray14setPremadeAabbERK9btVector3S2_: # @_ZNK26btTriangleIndexVertexArray14setPremadeAabbERK9btVector3S2_
	.cfi_startproc
# BB#0:
	movups	(%rsi), %xmm0
	movups	%xmm0, 68(%rdi)
	movups	(%rdx), %xmm0
	movups	%xmm0, 84(%rdi)
	movl	$1, 64(%rdi)
	retq
.Lfunc_end8:
	.size	_ZNK26btTriangleIndexVertexArray14setPremadeAabbERK9btVector3S2_, .Lfunc_end8-_ZNK26btTriangleIndexVertexArray14setPremadeAabbERK9btVector3S2_
	.cfi_endproc

	.globl	_ZNK26btTriangleIndexVertexArray14getPremadeAabbEP9btVector3S1_
	.p2align	4, 0x90
	.type	_ZNK26btTriangleIndexVertexArray14getPremadeAabbEP9btVector3S1_,@function
_ZNK26btTriangleIndexVertexArray14getPremadeAabbEP9btVector3S1_: # @_ZNK26btTriangleIndexVertexArray14getPremadeAabbEP9btVector3S1_
	.cfi_startproc
# BB#0:
	movups	68(%rdi), %xmm0
	movups	%xmm0, (%rsi)
	movups	84(%rdi), %xmm0
	movups	%xmm0, (%rdx)
	retq
.Lfunc_end9:
	.size	_ZNK26btTriangleIndexVertexArray14getPremadeAabbEP9btVector3S1_, .Lfunc_end9-_ZNK26btTriangleIndexVertexArray14getPremadeAabbEP9btVector3S1_
	.cfi_endproc

	.section	.text._ZN26btTriangleIndexVertexArray16unLockVertexBaseEi,"axG",@progbits,_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi,comdat
	.weak	_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi
	.p2align	4, 0x90
	.type	_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi,@function
_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi: # @_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end10:
	.size	_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi, .Lfunc_end10-_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi
	.cfi_endproc

	.section	.text._ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi,"axG",@progbits,_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi,comdat
	.weak	_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi
	.p2align	4, 0x90
	.type	_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi,@function
_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi: # @_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end11:
	.size	_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi, .Lfunc_end11-_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi
	.cfi_endproc

	.section	.text._ZNK26btTriangleIndexVertexArray14getNumSubPartsEv,"axG",@progbits,_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv,comdat
	.weak	_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv
	.p2align	4, 0x90
	.type	_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv,@function
_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv: # @_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv
	.cfi_startproc
# BB#0:
	movl	28(%rdi), %eax
	retq
.Lfunc_end12:
	.size	_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv, .Lfunc_end12-_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv
	.cfi_endproc

	.section	.text._ZN26btTriangleIndexVertexArray19preallocateVerticesEi,"axG",@progbits,_ZN26btTriangleIndexVertexArray19preallocateVerticesEi,comdat
	.weak	_ZN26btTriangleIndexVertexArray19preallocateVerticesEi
	.p2align	4, 0x90
	.type	_ZN26btTriangleIndexVertexArray19preallocateVerticesEi,@function
_ZN26btTriangleIndexVertexArray19preallocateVerticesEi: # @_ZN26btTriangleIndexVertexArray19preallocateVerticesEi
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end13:
	.size	_ZN26btTriangleIndexVertexArray19preallocateVerticesEi, .Lfunc_end13-_ZN26btTriangleIndexVertexArray19preallocateVerticesEi
	.cfi_endproc

	.section	.text._ZN26btTriangleIndexVertexArray18preallocateIndicesEi,"axG",@progbits,_ZN26btTriangleIndexVertexArray18preallocateIndicesEi,comdat
	.weak	_ZN26btTriangleIndexVertexArray18preallocateIndicesEi
	.p2align	4, 0x90
	.type	_ZN26btTriangleIndexVertexArray18preallocateIndicesEi,@function
_ZN26btTriangleIndexVertexArray18preallocateIndicesEi: # @_ZN26btTriangleIndexVertexArray18preallocateIndicesEi
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end14:
	.size	_ZN26btTriangleIndexVertexArray18preallocateIndicesEi, .Lfunc_end14-_ZN26btTriangleIndexVertexArray18preallocateIndicesEi
	.cfi_endproc

	.type	_ZTV26btTriangleIndexVertexArray,@object # @_ZTV26btTriangleIndexVertexArray
	.section	.rodata,"a",@progbits
	.globl	_ZTV26btTriangleIndexVertexArray
	.p2align	3
_ZTV26btTriangleIndexVertexArray:
	.quad	0
	.quad	_ZTI26btTriangleIndexVertexArray
	.quad	_ZN26btTriangleIndexVertexArrayD2Ev
	.quad	_ZN26btTriangleIndexVertexArrayD0Ev
	.quad	_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_
	.quad	_ZN26btTriangleIndexVertexArray24getLockedVertexIndexBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i
	.quad	_ZNK26btTriangleIndexVertexArray32getLockedReadOnlyVertexIndexBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i
	.quad	_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi
	.quad	_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi
	.quad	_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv
	.quad	_ZN26btTriangleIndexVertexArray19preallocateVerticesEi
	.quad	_ZN26btTriangleIndexVertexArray18preallocateIndicesEi
	.quad	_ZNK26btTriangleIndexVertexArray14hasPremadeAabbEv
	.quad	_ZNK26btTriangleIndexVertexArray14setPremadeAabbERK9btVector3S2_
	.quad	_ZNK26btTriangleIndexVertexArray14getPremadeAabbEP9btVector3S1_
	.size	_ZTV26btTriangleIndexVertexArray, 120

	.type	_ZTS26btTriangleIndexVertexArray,@object # @_ZTS26btTriangleIndexVertexArray
	.globl	_ZTS26btTriangleIndexVertexArray
	.p2align	4
_ZTS26btTriangleIndexVertexArray:
	.asciz	"26btTriangleIndexVertexArray"
	.size	_ZTS26btTriangleIndexVertexArray, 29

	.type	_ZTI26btTriangleIndexVertexArray,@object # @_ZTI26btTriangleIndexVertexArray
	.globl	_ZTI26btTriangleIndexVertexArray
	.p2align	4
_ZTI26btTriangleIndexVertexArray:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS26btTriangleIndexVertexArray
	.quad	_ZTI23btStridingMeshInterface
	.size	_ZTI26btTriangleIndexVertexArray, 24


	.globl	_ZN26btTriangleIndexVertexArrayC1EiPiiiPfi
	.type	_ZN26btTriangleIndexVertexArrayC1EiPiiiPfi,@function
_ZN26btTriangleIndexVertexArrayC1EiPiiiPfi = _ZN26btTriangleIndexVertexArrayC2EiPiiiPfi
	.globl	_ZN26btTriangleIndexVertexArrayD1Ev
	.type	_ZN26btTriangleIndexVertexArrayD1Ev,@function
_ZN26btTriangleIndexVertexArrayD1Ev = _ZN26btTriangleIndexVertexArrayD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
