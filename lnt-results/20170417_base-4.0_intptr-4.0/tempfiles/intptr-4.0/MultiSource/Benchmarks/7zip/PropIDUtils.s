	.text
	.file	"PropIDUtils.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	15                      # 0xf
.LCPI0_1:
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
.LCPI0_2:
	.long	2147483658              # 0x8000000a
	.long	2147483658              # 0x8000000a
	.long	2147483658              # 0x8000000a
	.long	2147483658              # 0x8000000a
.LCPI0_3:
	.long	48                      # 0x30
	.long	48                      # 0x30
	.long	48                      # 0x30
	.long	48                      # 0x30
.LCPI0_4:
	.long	55                      # 0x37
	.long	55                      # 0x37
	.long	55                      # 0x37
	.long	55                      # 0x37
.LCPI0_5:
	.long	536870912               # 0x20000000
	.long	2147483658              # 0x8000000a
	.long	2147483658              # 0x8000000a
	.long	2147483658              # 0x8000000a
	.text
	.globl	_Z18ConvertUInt32ToHexjPw
	.p2align	4, 0x90
	.type	_Z18ConvertUInt32ToHexjPw,@function
_Z18ConvertUInt32ToHexjPw:              # @_Z18ConvertUInt32ToHexjPw
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	shrl	$4, %eax
	movl	%edi, %ecx
	shrl	$8, %ecx
	movl	%edi, %edx
	shrl	$12, %edx
	movd	%ecx, %xmm1
	movd	%edi, %xmm0
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movd	%eax, %xmm2
	movd	%edx, %xmm4
	punpckldq	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	punpckldq	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	pand	.LCPI0_0(%rip), %xmm4
	movl	%edi, %eax
	shrl	$16, %eax
	movdqa	.LCPI0_1(%rip), %xmm3   # xmm3 = [2147483648,2147483648,2147483648,2147483648]
	movdqa	%xmm4, %xmm1
	pxor	%xmm3, %xmm1
	movdqa	.LCPI0_2(%rip), %xmm5   # xmm5 = [2147483658,2147483658,2147483658,2147483658]
	pcmpgtd	%xmm1, %xmm5
	movdqa	.LCPI0_3(%rip), %xmm1   # xmm1 = [48,48,48,48]
	movdqa	%xmm4, %xmm6
	por	%xmm1, %xmm6
	movdqa	.LCPI0_4(%rip), %xmm2   # xmm2 = [55,55,55,55]
	paddd	%xmm2, %xmm4
	pand	%xmm5, %xmm6
	pandn	%xmm4, %xmm5
	por	%xmm6, %xmm5
	movdqu	%xmm5, 16(%rsi)
	andl	$15, %eax
	movl	%edi, %ecx
	shrl	$20, %ecx
	andl	$15, %ecx
	movl	%edi, %edx
	shrl	$24, %edx
	andl	$15, %edx
	shrl	$28, %edi
	movd	%ecx, %xmm4
	punpckldq	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1]
	movd	%eax, %xmm5
	movd	%edx, %xmm6
	punpckldq	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	punpckldq	%xmm6, %xmm0    # xmm0 = xmm0[0],xmm6[0],xmm0[1],xmm6[1]
	pxor	%xmm3, %xmm0
	movdqa	.LCPI0_5(%rip), %xmm3   # xmm3 = [536870912,2147483658,2147483658,2147483658]
	pcmpgtd	%xmm0, %xmm3
	movd	%edi, %xmm0
	punpckldq	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1]
	punpckldq	%xmm6, %xmm0    # xmm0 = xmm0[0],xmm6[0],xmm0[1],xmm6[1]
	por	%xmm0, %xmm1
	paddd	%xmm2, %xmm0
	pand	%xmm3, %xmm1
	pandn	%xmm0, %xmm3
	por	%xmm1, %xmm3
	movdqu	%xmm3, (%rsi)
	movl	$0, 32(%rsi)
	retq
.Lfunc_end0:
	.size	_Z18ConvertUInt32ToHexjPw, .Lfunc_end0-_Z18ConvertUInt32ToHexjPw
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	45                      # 0x2d
	.long	45                      # 0x2d
	.long	45                      # 0x2d
	.long	45                      # 0x2d
.LCPI1_1:
	.long	114                     # 0x72
	.long	119                     # 0x77
	.long	120                     # 0x78
	.long	114                     # 0x72
.LCPI1_2:
	.long	16                      # 0x10
	.long	8                       # 0x8
	.long	4                       # 0x4
	.long	2                       # 0x2
.LCPI1_3:
	.long	119                     # 0x77
	.long	120                     # 0x78
	.long	114                     # 0x72
	.long	119                     # 0x77
.LCPI1_4:
	.long	48                      # 0x30
	.long	48                      # 0x30
	.long	48                      # 0x30
	.long	48                      # 0x30
.LCPI1_5:
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
.LCPI1_6:
	.long	536870912               # 0x20000000
	.long	2147483658              # 0x8000000a
	.long	2147483658              # 0x8000000a
	.long	2147483658              # 0x8000000a
.LCPI1_7:
	.long	55                      # 0x37
	.long	55                      # 0x37
	.long	55                      # 0x37
	.long	55                      # 0x37
.LCPI1_8:
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	15                      # 0xf
.LCPI1_9:
	.long	2147483658              # 0x8000000a
	.long	2147483658              # 0x8000000a
	.long	2147483658              # 0x8000000a
	.long	2147483658              # 0x8000000a
.LCPI1_10:
	.zero	16
	.text
	.globl	_Z23ConvertPropertyToStringRK14tagPROPVARIANTjb
	.p2align	4, 0x90
	.type	_Z23ConvertPropertyToStringRK14tagPROPVARIANTjb,@function
_Z23ConvertPropertyToStringRK14tagPROPVARIANTjb: # @_Z23ConvertPropertyToStringRK14tagPROPVARIANTjb
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 272
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebx
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rsi, %rbp
	movq	%rdi, %r12
	addl	$-9, %edx
	cmpl	$44, %edx
	ja	.LBB1_123
# BB#1:
	jmpq	*.LJTI1_0(,%rdx,8)
.LBB1_2:
	movzwl	(%rbp), %eax
	cmpl	$64, %eax
	jne	.LBB1_123
# BB#3:
	leaq	8(%rbp), %rdi
	cmpl	$0, 12(%rbp)
	jne	.LBB1_5
# BB#4:
	cmpl	$0, (%rdi)
	je	.LBB1_6
.LBB1_5:
	leaq	80(%rsp), %rsi
	callq	FileTimeToLocalFileTime
	testl	%eax, %eax
	je	.LBB1_6
# BB#7:
	movzbl	%bl, %ecx
	leaq	80(%rsp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	callq	_Z23ConvertFileTimeToStringRK9_FILETIMEbb
	jmp	.LBB1_124
.LBB1_14:
	movzwl	(%rbp), %eax
	cmpl	$19, %eax
	jne	.LBB1_123
# BB#15:
	movl	8(%rbp), %eax
	xorl	%esi, %esi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_16:                               # =>This Inner Loop Header: Depth=1
	btl	%esi, %eax
	jae	.LBB1_18
# BB#17:                                #   in Loop: Header=BB1_16 Depth=1
	movsbl	_ZL11g_WinAttrib(%rsi), %ecx
	movslq	%edx, %rdi
	incl	%edx
	movl	%ecx, 80(%rsp,%rdi,4)
.LBB1_18:                               #   in Loop: Header=BB1_16 Depth=1
	leaq	1(%rsi), %rcx
	movl	$1, %edi
	shll	%cl, %edi
	cmpq	$7, %rcx
	je	.LBB1_21
# BB#19:                                #   in Loop: Header=BB1_16 Depth=1
	andl	%eax, %edi
	je	.LBB1_21
# BB#20:                                #   in Loop: Header=BB1_16 Depth=1
	movsbl	_ZL11g_WinAttrib+1(%rsi), %esi
	movslq	%edx, %rdi
	incl	%edx
	movl	%esi, 80(%rsp,%rdi,4)
.LBB1_21:                               #   in Loop: Header=BB1_16 Depth=1
	incq	%rcx
	cmpq	$16, %rcx
	movq	%rcx, %rsi
	jne	.LBB1_16
# BB#22:
	movslq	%edx, %rax
	movl	$0, 80(%rsp,%rax,4)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%r12)
	movl	$-1, %ebp
	leaq	80(%rsp), %rax
	.p2align	4, 0x90
.LBB1_23:                               # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB1_23
# BB#24:                                # %_Z11MyStringLenIwEiPKT_.exit.i65
	leaq	80(%rsp), %rbx
	leal	1(%rbp), %eax
	movslq	%eax, %r14
	movl	$4, %ecx
	movq	%r14, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r12)
	movl	$0, (%rax)
	movl	%r14d, 12(%r12)
	.p2align	4, 0x90
.LBB1_25:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i68
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB1_25
# BB#26:                                # %_ZN11CStringBaseIwEC2EPKw.exit69
	movl	%ebp, 8(%r12)
	jmp	.LBB1_124
.LBB1_8:
	movzwl	(%rbp), %eax
	cmpl	$19, %eax
	jne	.LBB1_123
# BB#9:
	movl	8(%rbp), %eax
	movl	%eax, %ecx
	shrl	$4, %ecx
	movl	%eax, %edx
	shrl	$8, %edx
	movl	%eax, %esi
	shrl	$12, %esi
	movd	%edx, %xmm1
	movd	%eax, %xmm0
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movd	%ecx, %xmm2
	movd	%esi, %xmm4
	punpckldq	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	punpckldq	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	pand	.LCPI1_8(%rip), %xmm4
	movl	%eax, %ecx
	shrl	$16, %ecx
	movdqa	.LCPI1_5(%rip), %xmm3   # xmm3 = [2147483648,2147483648,2147483648,2147483648]
	movdqa	%xmm4, %xmm1
	pxor	%xmm3, %xmm1
	movdqa	.LCPI1_9(%rip), %xmm5   # xmm5 = [2147483658,2147483658,2147483658,2147483658]
	pcmpgtd	%xmm1, %xmm5
	movdqa	.LCPI1_4(%rip), %xmm1   # xmm1 = [48,48,48,48]
	movdqa	%xmm4, %xmm6
	por	%xmm1, %xmm6
	movdqa	.LCPI1_7(%rip), %xmm2   # xmm2 = [55,55,55,55]
	paddd	%xmm2, %xmm4
	pand	%xmm5, %xmm6
	pandn	%xmm4, %xmm5
	por	%xmm6, %xmm5
	movdqa	%xmm5, 96(%rsp)
	andl	$15, %ecx
	movl	%eax, %edx
	shrl	$20, %edx
	andl	$15, %edx
	movl	%eax, %esi
	shrl	$24, %esi
	andl	$15, %esi
	shrl	$28, %eax
	movd	%edx, %xmm4
	punpckldq	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1]
	movd	%ecx, %xmm5
	movd	%esi, %xmm6
	punpckldq	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	punpckldq	%xmm6, %xmm0    # xmm0 = xmm0[0],xmm6[0],xmm0[1],xmm6[1]
	pxor	%xmm3, %xmm0
	movdqa	.LCPI1_6(%rip), %xmm3   # xmm3 = [536870912,2147483658,2147483658,2147483658]
	pcmpgtd	%xmm0, %xmm3
	movd	%eax, %xmm0
	punpckldq	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1]
	punpckldq	%xmm6, %xmm0    # xmm0 = xmm0[0],xmm6[0],xmm0[1],xmm6[1]
	por	%xmm0, %xmm1
	paddd	%xmm2, %xmm0
	pand	%xmm3, %xmm1
	pandn	%xmm0, %xmm3
	por	%xmm1, %xmm3
	movdqa	%xmm3, 16(%rsp)         # 16-byte Spill
	movdqa	%xmm3, 80(%rsp)
	movl	$0, 112(%rsp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%r12)
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_10:                               # %._crit_edge161
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, 84(%rsp,%rbp,4)
	leaq	1(%rbp), %rbp
	jne	.LBB1_10
# BB#11:                                # %_Z11MyStringLenIwEiPKT_.exit.i
	leaq	1(%rbp), %rax
	movslq	%eax, %rbx
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r12)
	movl	$0, (%rax)
	movl	%ebx, 12(%r12)
	leaq	84(%rsp), %rcx
	movdqa	16(%rsp), %xmm0         # 16-byte Reload
	movd	%xmm0, (%rax)
	addq	$4, %rax
	.p2align	4, 0x90
.LBB1_12:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i._ZN11CStringBaseIwE11SetCapacityEi.exit.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB1_12
# BB#13:                                # %_ZN11CStringBaseIwEC2EPKw.exit
	movl	%ebp, 8(%r12)
	jmp	.LBB1_124
.LBB1_27:
	movzwl	(%rbp), %eax
	cmpl	$19, %eax
	jne	.LBB1_123
# BB#28:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%r12)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbx
	movq	%rbx, (%r12)
	movl	$0, (%rbx)
	movl	$4, 12(%r12)
	movl	8(%rbp), %edi
	movl	%edi, %eax
	shrl	$12, %eax
	andl	$15, %eax
	movsbl	_ZL11kPosixTypes(%rax), %eax
	movl	%eax, 80(%rsp)
	movl	%edi, %eax
	andl	$256, %eax              # imm = 0x100
	shrl	$8, %eax
	xorl	%ecx, %ecx
	testb	%dil, %dil
	sets	%cl
	movl	%edi, %edx
	andl	$64, %edx
	shrl	$6, %edx
	movl	%edi, %esi
	andl	$32, %esi
	shrl	$5, %esi
	pinsrw	$0, %eax, %xmm0
	pinsrw	$2, %ecx, %xmm0
	pinsrw	$4, %edx, %xmm0
	movl	%edi, %edx
	pinsrw	$6, %esi, %xmm0
	pslld	$31, %xmm0
	psrad	$31, %xmm0
	movdqa	.LCPI1_0(%rip), %xmm1   # xmm1 = [45,45,45,45]
	movdqa	%xmm0, %xmm2
	pandn	%xmm1, %xmm2
	pand	.LCPI1_1(%rip), %xmm0
	por	%xmm2, %xmm0
	movdqu	%xmm0, 84(%rsp)
	movd	%edx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	pand	.LCPI1_2(%rip), %xmm0
	pcmpeqd	.LCPI1_10, %xmm0
	pcmpeqd	%xmm2, %xmm2
	pxor	%xmm0, %xmm2
	pandn	%xmm1, %xmm2
	pandn	.LCPI1_3(%rip), %xmm0
	por	%xmm2, %xmm0
	movdqu	%xmm0, 100(%rsp)
	testb	$1, %dl
	movl	$120, %eax
	movl	$45, %ecx
	cmovnel	%eax, %ecx
	movl	%ecx, 116(%rsp)
	testb	$8, %dh
	je	.LBB1_30
# BB#29:
	movl	%edx, %eax
	shrl	%eax
	andl	$32, %eax
	orl	$83, %eax
	movl	%eax, 92(%rsp)
.LBB1_30:
	testb	$4, %dh
	je	.LBB1_32
# BB#31:
	movl	%edx, %eax
	andl	$8, %eax
	leal	83(,%rax,4), %eax
	movl	%eax, 104(%rsp)
.LBB1_32:
	testb	$2, %dh
	je	.LBB1_34
# BB#33:
	movl	%edx, %eax
	shll	$5, %eax
	andl	$32, %eax
	orl	$84, %eax
	movl	%eax, 116(%rsp)
.LBB1_34:
	movl	$0, 120(%rsp)
	movl	$0, 8(%r12)
	movl	$0, (%rbx)
	movl	$-1, %ebp
	leaq	80(%rsp), %rax
	.p2align	4, 0x90
.LBB1_35:                               # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB1_35
# BB#36:                                # %_Z11MyStringLenIwEiPKT_.exit.i71
	cmpl	$3, %ebp
	je	.LBB1_39
# BB#37:
	movl	%edx, %r13d
	leal	1(%rbp), %r15d
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp0:
	callq	_Znam
	movq	%rax, %r14
.Ltmp1:
# BB#38:                                # %._crit_edge16.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%r12), %rax
	movq	%r14, (%r12)
	movl	$0, (%r14,%rax,4)
	movl	%r15d, 12(%r12)
	movq	%r14, %rbx
	movl	%r13d, %edx
.LBB1_39:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i74.preheader
	leaq	80(%rsp), %rax
	.p2align	4, 0x90
.LBB1_40:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i74
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB1_40
# BB#41:
	movl	%ebp, 8(%r12)
	movl	%edx, %eax
	andl	$-65536, %eax           # imm = 0xFFFF0000
	je	.LBB1_124
# BB#42:
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movl	%edx, %ecx
	shrl	$16, %ecx
	movdqa	.LCPI1_4(%rip), %xmm0   # xmm0 = [48,48,48,48]
	movdqa	%xmm0, 96(%rsp)
	andl	$15, %ecx
	movl	%edx, %edi
	shrl	$20, %edx
	andl	$15, %edx
	movl	%edi, %esi
	shrl	$24, %esi
	andl	$15, %esi
	shrl	$28, %edi
	movd	%eax, %xmm1
	movd	%edx, %xmm2
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movd	%ecx, %xmm3
	movd	%esi, %xmm4
	punpckldq	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	punpckldq	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	pxor	.LCPI1_5(%rip), %xmm1
	movdqa	.LCPI1_6(%rip), %xmm3   # xmm3 = [536870912,2147483658,2147483658,2147483658]
	pcmpgtd	%xmm1, %xmm3
	movd	%edi, %xmm1
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	punpckldq	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	por	%xmm1, %xmm0
	paddd	.LCPI1_7(%rip), %xmm1
	pand	%xmm3, %xmm0
	pandn	%xmm1, %xmm3
	por	%xmm0, %xmm3
	movdqa	%xmm3, 48(%rsp)         # 16-byte Spill
	movdqa	%xmm3, 80(%rsp)
	movl	$0, 112(%rsp)
	xorl	%r14d, %r14d
	movabsq	$4294967296, %rax       # imm = 0x100000000
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB1_43:                               # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	addq	%rax, %rbp
	decl	%r14d
	cmpl	$0, 84(%rsp,%r12,4)
	leaq	1(%r12), %r12
	jne	.LBB1_43
# BB#44:                                # %_Z11MyStringLenIwEiPKT_.exit.i77
	leaq	1(%r12), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	cltq
	movl	$4, %ecx
	movq	%rax, 8(%rsp)           # 8-byte Spill
	mulq	%rcx
	movq	$-1, %rbx
	cmovnoq	%rax, %rbx
.Ltmp3:
	movq	%rbx, %rdi
	callq	_Znam
	movq	%rax, %r13
.Ltmp4:
# BB#45:                                # %.noexc81
	movdqa	48(%rsp), %xmm0         # 16-byte Reload
	movd	%xmm0, (%r13)
	movl	$4, %eax
	.p2align	4, 0x90
.LBB1_46:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i80._ZN11CStringBaseIwE11SetCapacityEi.exit.i80_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movl	80(%rsp,%rax), %ecx
	movl	%ecx, (%r13,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB1_46
# BB#47:                                # %_ZN11CStringBaseIwEC2EPKw.exit82
	cmpl	$-1, %r12d
	je	.LBB1_48
# BB#49:                                # %._crit_edge16.i.i.i
.Ltmp6:
	movq	%rbx, %rdi
	callq	_Znam
	movq	%rax, %rdx
.Ltmp7:
# BB#50:                                # %.noexc83
	movl	$0, (%rdx)
	movq	(%rsp), %rax            # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	movq	%rdx, %r15
	jmp	.LBB1_51
.LBB1_123:
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	_Z26ConvertPropVariantToStringRK14tagPROPVARIANT
.LBB1_124:
	movq	%r12, %rax
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_6:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%r12)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, (%r12)
	movl	$0, (%rax)
	movl	$4, 12(%r12)
	jmp	.LBB1_124
.LBB1_48:
	xorl	%eax, %eax
	xorl	%edx, %edx
	xorl	%r15d, %r15d
.LBB1_51:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
	movl	%eax, %ebx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_52:                               # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rax), %ecx
	movl	%ecx, (%r15,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB1_52
# BB#53:                                # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
	movl	%ebx, %eax
	subl	%r12d, %eax
	cmpl	$1, %eax
	jg	.LBB1_54
# BB#55:
	cmpl	$8, %ebx
	movl	$16, %ecx
	movl	$4, %eax
	cmovgl	%ecx, %eax
	cmpl	$65, %ebx
	jl	.LBB1_57
# BB#56:                                # %select.true.sink
	movl	%ebx, %eax
	shrl	$31, %eax
	addl	%ebx, %eax
	sarl	%eax
.LBB1_57:                               # %select.end
	addl	%ebx, %eax
	leal	-1(%r14,%rax), %ecx
	testl	%ecx, %ecx
	movl	$1, %eax
	cmovgl	%ecx, %eax
	movl	%eax, %ecx
	subl	%ebx, %ecx
	leal	(%r12,%rcx), %ecx
	cmpl	$-2, %ecx
	jne	.LBB1_58
.LBB1_54:
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r14           # 8-byte Reload
	jmp	.LBB1_65
.LBB1_58:
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	leaq	2(%rax,%r12), %rax
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp9:
	callq	_Znam
	movq	%rax, 48(%rsp)          # 8-byte Spill
.Ltmp10:
	movq	8(%rsp), %r14           # 8-byte Reload
# BB#59:                                # %.noexc114
	testl	%ebx, %ebx
	jle	.LBB1_64
# BB#60:                                # %.preheader.i.i
	testl	%r12d, %r12d
	jle	.LBB1_62
# BB#61:                                # %.lr.ph.i.i
	movq	%rbp, %rdx
	sarq	$30, %rdx
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	memcpy
	jmp	.LBB1_63
.LBB1_62:                               # %._crit_edge.i.i
	testq	%r15, %r15
	je	.LBB1_64
.LBB1_63:                               # %._crit_edge.thread.i.i111
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
.LBB1_64:                               # %._crit_edge16.i.i112
	movq	%rbp, %rax
	sarq	$30, %rax
	movq	48(%rsp), %r15          # 8-byte Reload
	movl	$0, (%r15,%rax)
.LBB1_65:                               # %._crit_edge16.i.i.i84
	sarq	$30, %rbp
	movl	$32, (%r15,%rbp)
	movl	$0, (%r15,%r14,4)
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 32(%rsp)
	leaq	2(%r12), %rbp
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp12:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp13:
# BB#66:                                # %.noexc90
	movq	%rbx, 32(%rsp)
	movl	$0, (%rbx)
	movl	%ebp, 44(%rsp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_67:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i85
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB1_67
# BB#68:                                # %_ZN11CStringBaseIwEC2ERKS0_.exit.i88
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, 40(%rsp)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	8(%rax), %eax
	testl	%eax, %eax
	jle	.LBB1_93
# BB#69:
	cmpl	$8, %ebp
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebp
	jl	.LBB1_71
# BB#70:                                # %select.true.sink349
	movl	%ebp, %ecx
	shrl	$31, %ecx
	addl	%ebp, %ecx
	sarl	%ecx
.LBB1_71:                               # %select.end348
	cmpl	%eax, %ecx
	cmovll	%eax, %ecx
	movslq	%ecx, %rax
	leaq	3(%rax,%r12), %rax
	cmpl	%eax, %ebp
	je	.LBB1_93
# BB#72:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp15:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp16:
# BB#73:                                # %.noexc128
	cmpl	$-1, %r12d
	jl	.LBB1_92
# BB#74:                                # %.preheader.i.i118
	testl	%r12d, %r12d
	js	.LBB1_91
# BB#75:                                # %.lr.ph.i.i119.preheader
	cmpl	$8, (%rsp)              # 4-byte Folded Reload
	jae	.LBB1_79
# BB#76:
	xorl	%eax, %eax
	jmp	.LBB1_89
.LBB1_79:                               # %min.iters.checked
	movq	%r14, %rax
	andq	$-8, %rax
	je	.LBB1_80
# BB#81:                                # %vector.body.preheader
	leaq	-8(%rax), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	testb	$3, %dl
	je	.LBB1_82
# BB#83:                                # %vector.body.prol.preheader
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	andl	$24, %esi
	addl	$-8, %esi
	shrl	$3, %esi
	incl	%esi
	andl	$3, %esi
	negq	%rsi
	xorl	%edx, %edx
.LBB1_84:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%rbx,%rdx,4), %xmm0
	movdqu	16(%rbx,%rdx,4), %xmm1
	movdqu	%xmm0, (%rbp,%rdx,4)
	movdqu	%xmm1, 16(%rbp,%rdx,4)
	addq	$8, %rdx
	incq	%rsi
	jne	.LBB1_84
	jmp	.LBB1_85
.LBB1_80:
	xorl	%eax, %eax
	jmp	.LBB1_89
.LBB1_82:
	xorl	%edx, %edx
.LBB1_85:                               # %vector.body.prol.loopexit
	cmpq	$24, %rcx
	jb	.LBB1_88
# BB#86:                                # %vector.body.preheader.new
	movslq	(%rsp), %rcx            # 4-byte Folded Reload
	andq	$-8, %rcx
	subq	%rdx, %rcx
	leaq	112(%rbp,%rdx,4), %rsi
	leaq	112(%rbx,%rdx,4), %rdx
.LBB1_87:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rdx), %xmm0
	movdqu	(%rdx), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-32, %rcx
	jne	.LBB1_87
.LBB1_88:                               # %middle.block
	cmpq	%rax, %r14
	je	.LBB1_91
.LBB1_89:                               # %.lr.ph.i.i119.preheader171
	movslq	(%rsp), %rcx            # 4-byte Folded Reload
	subq	%rax, %rcx
	leaq	(%rbp,%rax,4), %rdx
	leaq	(%rbx,%rax,4), %rax
.LBB1_90:                               # %.lr.ph.i.i119
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %esi
	movl	%esi, (%rdx)
	addq	$4, %rdx
	addq	$4, %rax
	decq	%rcx
	jne	.LBB1_90
.LBB1_91:                               # %._crit_edge.thread.i.i125
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB1_92:                               # %._crit_edge16.i.i126
	movq	%rbp, 32(%rsp)
	movl	$0, (%rbp,%r14,4)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 44(%rsp)
	movq	%rbp, %rbx
.LBB1_93:                               # %.noexc.i
	leaq	(%rbx,%r14,4), %rcx
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	.p2align	4, 0x90
.LBB1_94:                               # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %eax
	addq	$4, %rsi
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB1_94
# BB#95:
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	8(%rdx), %eax
	leaq	(%rax,%r12), %rcx
	incl	%ecx
	movl	%ecx, 40(%rsp)
	movl	$0, 8(%rdx)
	movq	(%rdx), %rbx
	movl	$0, (%rbx)
	movl	12(%rdx), %r14d
	movl	%eax, %ecx
	subl	%r14d, %ecx
	leal	(%r12,%rcx), %ecx
	cmpl	$-2, %ecx
	jne	.LBB1_97
# BB#96:
	movq	16(%rsp), %r12          # 8-byte Reload
	jmp	.LBB1_102
.LBB1_97:
	leaq	2(%rax,%r12), %r12
	movslq	%r12d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp18:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp19:
# BB#98:                                # %.noexc101
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB1_101
# BB#99:                                # %.noexc101
	testl	%r14d, %r14d
	jle	.LBB1_101
# BB#100:                               # %._crit_edge.thread.i.i96
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	16(%rsp), %rax          # 8-byte Reload
	movslq	8(%rax), %rax
.LBB1_101:                              # %._crit_edge16.i.i97
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rbp, (%rcx)
	movl	$0, (%rbp,%rax,4)
	movl	%r12d, 12(%rcx)
	movq	%rcx, %r12
	movq	%rbp, %rbx
.LBB1_102:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i98
	movq	32(%rsp), %rax
	.p2align	4, 0x90
.LBB1_103:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB1_103
# BB#104:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	40(%rsp), %eax
	movl	%eax, 8(%r12)
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_106
# BB#105:
	callq	_ZdaPv
.LBB1_106:                              # %_ZN11CStringBaseIwED2Ev.exit102
	testq	%r15, %r15
	je	.LBB1_108
# BB#107:
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
.LBB1_108:                              # %_ZN11CStringBaseIwED2Ev.exit104
	movq	%r13, %rdi
	callq	_ZdaPv
	jmp	.LBB1_124
.LBB1_77:
.Ltmp11:
	movq	%rax, %rbp
	testq	%r15, %r15
	je	.LBB1_117
# BB#78:
	movq	72(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB1_116
.LBB1_125:
.Ltmp17:
	movq	%rax, %rbp
	movq	%rbx, %rdi
	jmp	.LBB1_113
.LBB1_112:
.Ltmp20:
	movq	%rax, %rbp
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_114
.LBB1_113:
	callq	_ZdaPv
	testq	%r15, %r15
	jne	.LBB1_115
	jmp	.LBB1_117
.LBB1_110:
.Ltmp8:
	movq	%rax, %rbp
	jmp	.LBB1_117
.LBB1_118:
.Ltmp2:
	movq	%r12, 16(%rsp)          # 8-byte Spill
	jmp	.LBB1_119
.LBB1_111:
.Ltmp14:
	movq	%rax, %rbp
.LBB1_114:                              # %_ZN11CStringBaseIwED2Ev.exit105
	testq	%r15, %r15
	je	.LBB1_117
.LBB1_115:
	movq	48(%rsp), %rdi          # 8-byte Reload
.LBB1_116:                              # %_ZN11CStringBaseIwED2Ev.exit107
	callq	_ZdaPv
.LBB1_117:                              # %_ZN11CStringBaseIwED2Ev.exit107
	movq	%r13, %rdi
	callq	_ZdaPv
	jmp	.LBB1_120
.LBB1_109:
.Ltmp5:
.LBB1_119:
	movq	%rax, %rbp
.LBB1_120:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB1_122
# BB#121:
	callq	_ZdaPv
.LBB1_122:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_Z23ConvertPropertyToStringRK14tagPROPVARIANTjb, .Lfunc_end1-_Z23ConvertPropertyToStringRK14tagPROPVARIANTjb
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_14
	.quad	.LBB1_2
	.quad	.LBB1_2
	.quad	.LBB1_2
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_8
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_123
	.quad	.LBB1_27
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\222\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp9-.Ltmp7           #   Call between .Ltmp7 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp12-.Ltmp10         #   Call between .Ltmp10 and .Ltmp12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Lfunc_end1-.Ltmp19     #   Call between .Ltmp19 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZL11g_WinAttrib,@object # @_ZL11g_WinAttrib
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
_ZL11g_WinAttrib:
	.asciz	"RHS8DAdNTsrCOnE_"
	.size	_ZL11g_WinAttrib, 17

	.type	_ZL11kPosixTypes,@object # @_ZL11kPosixTypes
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
_ZL11kPosixTypes:
	.ascii	"0pc3d5b7-9lBsDEF"
	.size	_ZL11kPosixTypes, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
