	.text
	.file	"z23.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	1006632960              # float 0.0078125
.LCPI0_1:
	.long	1065353216              # float 1
	.text
	.globl	FixAndPrintObject
	.p2align	4, 0x90
	.type	FixAndPrintObject,@function
FixAndPrintObject:                      # @FixAndPrintObject
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 272
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, %r10d
	movl	%r8d, %r11d
	movl	%ecx, %ebp
	movl	%edx, %r9d
	movl	%esi, %r8d
	movq	%rdi, %r12
	movzbl	32(%r12), %edi
	movl	%edi, %eax
	addb	$-2, %al
	cmpb	$97, %al
	ja	.LBB0_101
# BB#1:
	movq	296(%rsp), %r14
	movq	288(%rsp), %rsi
	movl	280(%rsp), %r15d
	movl	272(%rsp), %ebx
	leaq	32(%r12), %r13
	movzbl	%al, %eax
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_2:
	movq	%rbp, %rcx
	movq	8(%r12), %rax
	movq	16(%rax), %rax
	movl	$1, %ebp
	cmpb	$0, 32(%rax)
	je	.LBB0_17
# BB#3:
	movq	%rbx, %r13
	movq	%rsi, %r15
	jmp	.LBB0_19
.LBB0_4:
	testl	%r11d, %r11d
	jne	.LBB0_282
# BB#5:
	callq	Image
	movq	%rax, %rbp
	movl	$23, %edi
	movl	$8, %esi
	movl	$.L.str.20, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r13, %r8
	movq	%rbp, %r9
	callq	Error
	jmp	.LBB0_282
.LBB0_6:
	movq	%r9, %rdx
	movl	%r10d, %r9d
	movq	%rbx, %r10
	movq	%rsi, %r13
	leaq	8(%r12), %rax
	testl	%r11d, %r11d
	cmovneq	%r12, %rax
	movq	(%rax), %rbx
	addq	$16, %rbx
	movl	$1, %eax
	jmp	.LBB0_8
	.p2align	4, 0x90
.LBB0_7:                                #   in Loop: Header=BB0_8 Depth=1
	addq	$16, %rbx
	incl	%eax
.LBB0_8:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %ecx
	testb	%cl, %cl
	je	.LBB0_7
# BB#9:
	addb	$-13, %cl
	cmpb	$2, %cl
	jae	.LBB0_192
# BB#10:
	movq	%r8, %rdx
	movq	8(%rbx), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB0_11:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB0_11
# BB#12:
	movq	%rbp, %r8
	leaq	16(%r12), %rax
	leaq	24(%r12), %rcx
	testl	%r11d, %r11d
	cmoveq	%rcx, %rax
	movq	(%rax), %rax
	.p2align	4, 0x90
.LBB0_13:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB0_13
# BB#14:
	movslq	%r11d, %r15
	movq	%rdx, %r9
	movl	%r9d, %esi
	subl	48(%rax,%r15,4), %esi
	addl	48(%rdi,%r15,4), %esi
	movl	%esi, 88(%rbx)
	movl	40(%rbx), %ebp
	movl	%ebp, %ecx
	shrl	$23, %ecx
	incl	%ecx
	andl	$63, %ecx
	movl	%ecx, %edx
	shll	$23, %edx
	movl	%ebp, %eax
	andl	$-528482305, %eax       # imm = 0xE07FFFFF
	orl	%edx, %eax
	movl	%eax, 40(%rbx)
	andl	$4095, %ebp             # imm = 0xFFF
	cmpl	%ebp, %ecx
	jne	.LBB0_16
# BB#15:
	addl	%r9d, %r8d
	subl	%esi, %r8d
	movl	56(%rdi,%r15,4), %eax
	cmpl	%eax, %r8d
	cmovll	%eax, %r8d
	movl	68(%rbx), %ecx
	movl	48(%rdi,%r15,4), %edx
	subl	%edx, %ecx
	cmpl	%ecx, %r8d
	cmovgel	%r8d, %ecx
	leaq	16(%rsp), %rax
	leaq	20(%rsp), %rbp
	movl	$0, %r9d
	movl	%r11d, %r8d
	pushq	%rax
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -32
	andl	$-528482305, 40(%rbx)   # imm = 0xE07FFFFF
.LBB0_16:
	movl	48(%r12,%r15,4), %eax
	movl	%eax, (%r13)
	movl	56(%r12,%r15,4), %eax
	movl	%eax, (%r14)
	jmp	.LBB0_282
.LBB0_17:                               # %.lr.ph2339.preheader
	movq	%rbx, %r13
	movq	%rsi, %r15
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB0_18:                               # %.lr.ph2339
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	movq	16(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB0_18
.LBB0_19:                               # %._crit_edge2340
	testl	%r11d, %r11d
	sete	%sil
	cmpb	$24, %dil
	sete	%dl
	cmpb	$40, %dil
	sete	%bl
	orb	%dl, %bl
	xorb	%sil, %bl
	je	.LBB0_21
# BB#20:
	movq	%rax, %rdi
	movl	%r8d, %esi
	movl	%r9d, %edx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r11d, %r8d
	movl	%r10d, %r9d
	pushq	%r14
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi22:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB0_282
.LBB0_21:
	movq	%r12, %r14
	movq	%r9, %r12
	leaq	20(%rsp), %r10
	movl	$0, %r9d
	movq	%rax, %rdi
	movl	%r8d, %esi
	movl	%r12d, %edx
	movl	%r11d, %r8d
	movq	%rcx, %rbx
	leaq	16(%rsp), %rax
	pushq	%rax
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi27:
	.cfi_adjust_cfa_offset -32
	movl	%r12d, (%r15)
	movq	%r14, %r12
	movq	296(%rsp), %rax
	movl	%ebx, (%rax)
	jmp	.LBB0_282
.LBB0_22:
	testl	%r11d, %r11d
	je	.LBB0_206
# BB#23:
	cmpb	$0, 64(%r12)
	je	.LBB0_218
# BB#24:
	movq	BackEnd(%rip), %rax
	movq	%rsi, %r15
	movl	36(%r12), %esi
	subl	%r8d, %ebx
	movq	%r12, %rdi
	movl	%ebx, %edx
	movq	%r9, %rbx
	callq	*112(%rax)
	jmp	.LBB0_217
.LBB0_25:
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	cmpb	$16, %dil
	sete	%al
	testl	%r11d, %r11d
	sete	%cl
	cmpb	%al, %cl
	je	.LBB0_27
# BB#26:
	movq	%r8, %r14
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	%r11d, %ebp
	movq	%r9, %rbx
	movl	$.L.str.17, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	%ebp, %r11d
	movq	%r14, %r8
	movq	%rbx, %r9
.LBB0_27:                               # %.preheader2053.preheader
	movl	$1, %eax
	movq	%r12, %rbx
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB0_28:                               # %.preheader2053
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rbp
	movq	24(%rbx), %rbx
	cmpq	%r12, %rbp
	setne	%cl
	cmpq	%r12, %rbx
	setne	%dl
	cmpl	%r15d, %eax
	jge	.LBB0_30
# BB#29:                                # %.preheader2053
                                        #   in Loop: Header=BB0_28 Depth=1
	andb	%dl, %cl
	incl	%eax
	testb	%cl, %cl
	jne	.LBB0_28
.LBB0_30:                               # %.critedge36
	cmpq	%r12, %rbp
	je	.LBB0_32
# BB#31:                                # %.critedge36
	cmpq	%r12, %rbx
	jne	.LBB0_33
.LBB0_32:
	movq	%r8, %r15
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	%r11d, %r13d
	movq	%r9, %r10
	movl	$.L.str.18, %r9d
	xorl	%eax, %eax
	movq	%r10, %r14
	callq	Error
	movl	%r13d, %r11d
	movq	%r15, %r8
	movq	%r14, %r9
.LBB0_33:
	movq	%r12, %r14
	movq	16(%rbp), %r12
	movb	32(%r12), %al
	movl	$1, %r15d
	testb	%al, %al
	jne	.LBB0_36
# BB#34:                                # %.lr.ph2730.preheader
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB0_35:                               # %.lr.ph2730
                                        # =>This Inner Loop Header: Depth=1
	incl	%r15d
	movq	16(%r12), %r12
	movzbl	32(%r12), %eax
	testb	%al, %al
	je	.LBB0_35
.LBB0_36:                               # %._crit_edge2731
	movq	%rbx, xx_link(%rip)
	movq	%rbx, zz_hold(%rip)
	movq	24(%rbx), %rcx
	cmpq	%rbx, %rcx
	je	.LBB0_38
# BB#37:
	movq	%rcx, zz_res(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rbx), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
.LBB0_38:
	movq	%rbx, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB0_40
# BB#39:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rbx), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
.LBB0_40:
	movq	%rbp, xx_link(%rip)
	movq	%rbp, zz_hold(%rip)
	movq	24(%rbp), %rcx
	cmpq	%rbp, %rcx
	je	.LBB0_42
# BB#41:
	movq	%rcx, zz_res(%rip)
	movq	16(%rbp), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rbp), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
.LBB0_42:
	movq	%rbp, zz_hold(%rip)
	movq	8(%rbp), %rcx
	cmpq	%rbp, %rcx
	movq	288(%rsp), %rbx
	movq	%rbx, %r13
	je	.LBB0_44
# BB#43:
	movq	%rcx, zz_res(%rip)
	movq	(%rbp), %rdx
	movq	%rdx, (%rcx)
	movq	zz_res(%rip), %rcx
	movq	zz_hold(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rsi)
	movq	%rdx, 8(%rdx)
	movq	%rdx, (%rdx)
	movq	xx_link(%rip), %rbp
.LBB0_44:
	movq	%rbp, zz_hold(%rip)
	movzbl	32(%rbp), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rbp), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rbp)
	movq	zz_hold(%rip), %rdx
	movq	%rdx, zz_free(,%rcx,8)
	cmpb	$1, %al
	jne	.LBB0_46
# BB#45:
	movq	%r8, %r13
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	%r11d, %ebx
	movq	%r9, %rbp
	movl	$.L.str.19, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	%ebx, %r11d
	movq	%r13, %r8
	movq	288(%rsp), %r13
	movq	%rbp, %r9
.LBB0_46:
	movq	%r8, %rsi
	movl	%r11d, %r8d
	movslq	%r11d, %rax
	cmpb	$2, 41(%r14)
	je	.LBB0_48
# BB#47:
	movl	%r9d, 48(%r14,%rax,4)
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 56(%r14,%rax,4)
	movb	$2, 41(%r14)
.LBB0_48:
	movl	48(%r14,%rax,4), %edx
	movl	56(%r14,%rax,4), %ecx
	leaq	16(%rsp), %rax
	leaq	20(%rsp), %rbp
	movq	%r9, %rbx
	movl	$0, %r9d
	movq	%r12, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	pushq	%rax
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rbp
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi32:
	.cfi_adjust_cfa_offset -32
	movl	%ebp, (%r13)
	movq	296(%rsp), %rax
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, (%rax)
	movq	%r14, %r12
	jmp	.LBB0_282
.LBB0_49:
	cmpb	$19, %dil
	sete	%al
	cmpl	$1, %r11d
	sete	%cl
	xorb	%al, %cl
	movq	%r12, 8(%rsp)           # 8-byte Spill
	je	.LBB0_193
# BB#50:
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	8(%r12), %r10
	cmpq	%r12, %r10
	je	.LBB0_282
# BB#51:                                # %.preheader2014.lr.ph.preheader
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB0_52:                               # %.preheader2014
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_53 Depth 2
	movq	%r10, %rbp
	.p2align	4, 0x90
.LBB0_53:                               #   Parent Loop BB0_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB0_53
# BB#54:                                #   in Loop: Header=BB0_52 Depth=1
	cmpb	$9, %al
	je	.LBB0_58
# BB#55:                                #   in Loop: Header=BB0_52 Depth=1
	cmpb	$1, %al
	jne	.LBB0_59
# BB#56:                                #   in Loop: Header=BB0_52 Depth=1
	testl	%ecx, %ecx
	movq	8(%rsp), %r12           # 8-byte Reload
	je	.LBB0_60
# BB#57:                                #   in Loop: Header=BB0_52 Depth=1
	movb	45(%rbp), %al
	andb	$2, %al
	shrb	%al
	jmp	.LBB0_61
.LBB0_58:                               #   in Loop: Header=BB0_52 Depth=1
	movq	%rbp, %rdi
	movq	%r9, %rbx
	movq	%r8, %r13
	movl	%r11d, %r15d
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	movq	%r10, %r14
	movq	%rsi, %r12
	callq	SplitIsDefinite
	movq	%r12, %rsi
	movq	%r14, %r10
	movl	40(%rsp), %ecx          # 4-byte Reload
	movl	%r15d, %r11d
	movl	280(%rsp), %r15d
	movq	%r13, %r8
	movq	%rbx, %r9
	movl	272(%rsp), %ebx
	testl	%eax, %eax
	movq	8(%rsp), %r12           # 8-byte Reload
	je	.LBB0_62
	jmp	.LBB0_244
.LBB0_59:                               #   in Loop: Header=BB0_52 Depth=1
	addb	$-9, %al
	cmpb	$90, %al
	movq	8(%rsp), %r12           # 8-byte Reload
	ja	.LBB0_62
	jmp	.LBB0_244
.LBB0_60:                               #   in Loop: Header=BB0_52 Depth=1
	xorl	%eax, %eax
.LBB0_61:                               # %.critedge9.backedge
                                        #   in Loop: Header=BB0_52 Depth=1
	movzbl	%al, %ecx
.LBB0_62:                               # %.critedge9.backedge
                                        #   in Loop: Header=BB0_52 Depth=1
	movq	8(%r10), %r10
	cmpq	%r12, %r10
	jne	.LBB0_52
	jmp	.LBB0_282
.LBB0_63:
	movq	%rbp, %r13
	movq	8(%r12), %rax
	movq	16(%rax), %rax
	movl	$1, %ebp
	cmpb	$0, 32(%rax)
	jne	.LBB0_66
# BB#64:                                # %.lr.ph.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB0_65:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	movq	16(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB0_65
.LBB0_66:                               # %._crit_edge
	testl	%r11d, %r11d
	sete	%cl
	cmpb	$26, %dil
	sete	%dl
	xorb	%cl, %dl
	je	.LBB0_203
# BB#67:
	movq	%rax, %rdi
	movq	%rsi, %rax
	movl	%r8d, %esi
	movl	%r9d, %edx
	movl	%r13d, %ecx
	movl	%r11d, %r8d
	movl	%r10d, %r9d
	pushq	%r14
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi37:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB0_282
.LBB0_68:
	movq	%rbp, %r13
	movq	8(%r12), %rax
	movq	16(%rax), %rbx
	movl	$1, %ebp
	cmpb	$0, 32(%rbx)
	jne	.LBB0_71
# BB#69:                                # %.lr.ph2323.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB0_70:                               # %.lr.ph2323
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB0_70
.LBB0_71:                               # %._crit_edge2324
	testl	%r11d, %r11d
	sete	%al
	cmpb	$28, %dil
	sete	%cl
	xorb	%al, %cl
	je	.LBB0_204
# BB#72:
	movq	%rbx, %rdi
	movl	%r8d, %esi
	movl	%r9d, %edx
	movl	%r13d, %ecx
	movl	%r11d, %r8d
	movl	%r10d, %r9d
	pushq	%r14
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	pushq	296(%rsp)
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi42:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB0_282
.LBB0_73:
	movq	%rbp, %r13
	movq	8(%r12), %rax
	movq	16(%rax), %rax
	movl	$1, %ebp
	cmpb	$0, 32(%rax)
	jne	.LBB0_76
# BB#74:                                # %.lr.ph2331.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB0_75:                               # %.lr.ph2331
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	movq	16(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB0_75
.LBB0_76:                               # %._crit_edge2332
	testl	%r11d, %r11d
	sete	%cl
	cmpb	$36, %dil
	sete	%dl
	xorb	%cl, %dl
	je	.LBB0_205
# BB#77:
	movq	%rax, %rdi
	movq	%rsi, %rax
	movl	%r8d, %esi
	movl	%r9d, %edx
	movl	%r13d, %ecx
	movl	%r11d, %r8d
	movl	%r10d, %r9d
	pushq	%r14
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi47:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB0_282
.LBB0_78:
	movq	%rsi, %r15
	testl	%r11d, %r11d
	sete	%al
	cmpb	$45, %dil
	sete	%cl
	xorb	%al, %cl
	jne	.LBB0_282
# BB#79:
	leaq	8(%r12), %rax
	testl	%r11d, %r11d
	cmovneq	%r12, %rax
	movq	(%rax), %rbx
	.p2align	4, 0x90
.LBB0_80:                               # =>This Inner Loop Header: Depth=1
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_80
# BB#81:
	addb	$-13, %al
	cmpb	$2, %al
	jb	.LBB0_83
# BB#82:
	movq	%r8, %r14
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movq	%rbp, %r13
	movl	%r11d, %ebp
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	%ebp, %r11d
	movq	%r13, %rbp
	movq	%r14, %r8
	movq	296(%rsp), %r14
.LBB0_83:                               # %.loopexit
	movl	40(%rbx), %eax
	movl	%eax, %ecx
	shrl	$23, %ecx
	incl	%ecx
	andl	$63, %ecx
	movl	%ecx, %edx
	shll	$23, %edx
	movl	%eax, %esi
	andl	$-528482305, %esi       # imm = 0xE07FFFFF
	orl	%edx, %esi
	movl	%esi, 40(%rbx)
	andl	$4095, %eax             # imm = 0xFFF
	cmpl	%eax, %ecx
	jne	.LBB0_282
# BB#84:
	movq	8(%rbx), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB0_85:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB0_85
# BB#86:
	addl	%r8d, %ebp
	movl	68(%rbx), %ecx
	movl	88(%rbx), %esi
	subl	%esi, %ebp
	movslq	%r11d, %r13
	movl	56(%rdi,%r13,4), %eax
	cmpl	%eax, %ebp
	cmovll	%eax, %ebp
	movl	48(%rdi,%r13,4), %edx
	subl	%edx, %ecx
	cmpl	%ecx, %ebp
	cmovgel	%ebp, %ecx
	leaq	16(%rsp), %rax
	leaq	20(%rsp), %r10
	movl	$0, %r9d
	movl	%r11d, %r8d
	pushq	%rax
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi52:
	.cfi_adjust_cfa_offset -32
	movl	48(%r12,%r13,4), %eax
	movl	%eax, (%r15)
	movl	56(%r12,%r13,4), %eax
	movl	%eax, (%r14)
	andl	$-528482305, 40(%rbx)   # imm = 0xE07FFFFF
	jmp	.LBB0_282
.LBB0_87:
	movq	8(%r12), %rax
	.p2align	4, 0x90
.LBB0_88:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB0_88
# BB#89:
	movq	BackEnd(%rip), %rax
	cmpl	$0, 28(%rax)
	je	.LBB0_218
# BB#90:
	testl	%r11d, %r11d
	je	.LBB0_213
# BB#91:
	cmpb	$0, 41(%r12)
	je	.LBB0_218
# BB#92:
	movq	%rsi, %r15
	movl	88(%r12), %esi
	subl	%r8d, %ebx
	movq	%r12, %rdi
	movl	%ebx, %edx
	movq	%r9, %rbx
	callq	*200(%rax)
	jmp	.LBB0_217
.LBB0_93:
	movq	%rbx, %r14
	movq	%rbp, %r13
	movq	(%r12), %rax
	movq	16(%rax), %rbx
	movl	$1, %ebp
	cmpb	$0, 32(%rbx)
	jne	.LBB0_96
# BB#94:                                # %.lr.ph2422.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB0_95:                               # %.lr.ph2422
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB0_95
.LBB0_96:                               # %._crit_edge2423
	testl	%r11d, %r11d
	movl	%r11d, 24(%rsp)         # 4-byte Spill
	je	.LBB0_208
# BB#97:
	movq	8(%r12), %rcx
	addq	$16, %rcx
	.p2align	4, 0x90
.LBB0_98:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rax
	leaq	16(%rax), %rcx
	cmpb	$0, 32(%rax)
	je	.LBB0_98
# BB#99:
	movq	%r8, %r15
	movq	BackEnd(%rip), %r11
	cmpb	$98, %dil
	movq	%r12, 8(%rsp)           # 8-byte Spill
	jne	.LBB0_209
# BB#100:
	movl	88(%r12), %ecx
	movl	%ecx, %esi
	subl	48(%r12), %esi
	movl	%r14d, %r8d
	subl	%r15d, %r8d
	movl	%r8d, %edx
	subl	%r13d, %edx
	addl	56(%r12), %ecx
	addl	%r9d, %r8d
	movq	%rax, %rdi
	movq	%r9, %r12
	callq	*208(%r11)
	jmp	.LBB0_210
.LBB0_101:
	movq	no_fpos(%rip), %rbx
	callq	Image
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.21, %edx
	movl	$0, %ecx
	movl	$.L.str.22, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	pushq	%rbp
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi55:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB0_282
.LBB0_102:
	leaq	8(%r12), %rax
	testl	%r11d, %r11d
	cmovneq	%r12, %rax
	movq	(%rax), %rax
	movq	16(%rax), %rdi
	movl	$1, %eax
	cmpb	$0, 32(%rdi)
	je	.LBB0_219
# BB#103:
	movq	%rsi, %r15
	jmp	.LBB0_221
.LBB0_104:
	testl	%r11d, %r11d
	movq	%r12, %rax
	movq	8(%rax), %r12
	movq	%rax, 8(%rsp)           # 8-byte Spill
	je	.LBB0_222
# BB#105:                               # %.preheader2050
	cmpq	%rax, %r12
	je	.LBB0_122
# BB#106:                               # %.preheader2049.lr.ph
	movl	%ebx, %eax
	subl	%r8d, %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movq	%r8, 88(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB0_107:                              # %.preheader2049
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_108 Depth 2
	movq	%rbp, %r13
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB0_108:                              #   Parent Loop BB0_107 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB0_108
# BB#109:                               #   in Loop: Header=BB0_107 Depth=1
	movl	%eax, %ecx
	addb	$-9, %cl
	cmpb	$91, %cl
	jae	.LBB0_111
# BB#110:                               #   in Loop: Header=BB0_107 Depth=1
	movl	%r11d, %r14d
	movq	%r9, %rdx
	movl	$0, %r9d
	movq	%rbp, %rdi
	movl	%r8d, %esi
	movq	%r15, %rax
	movq	%r13, %r15
	movl	%r15d, %ecx
	movq	%rax, %rbp
	movl	%r14d, %r8d
	leaq	16(%rsp), %rax
	pushq	%rax
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	leaq	28(%rsp), %rax
	pushq	%rax
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %r13
	movq	%rdx, %rbx
	callq	FixAndPrintObject
	movl	%r14d, %r11d
	movq	120(%rsp), %r8          # 8-byte Reload
	movq	%rbp, %rax
	movq	%r15, %rbp
	movq	%rax, %r15
	movq	%rbx, %r9
	movq	%r13, %rbx
	addq	$32, %rsp
.Lcfi60:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB0_121
.LBB0_111:                              #   in Loop: Header=BB0_107 Depth=1
	cmpb	$3, %al
	jne	.LBB0_114
# BB#112:                               #   in Loop: Header=BB0_107 Depth=1
	movl	%r11d, %r14d
	movq	%r8, %r15
	movq	%r9, %rbx
	movq	BackEnd(%rip), %rax
	movl	48(%rbp), %edx
	movl	52(%rbp), %edi
	movl	60(%rbp), %esi
	movl	56(%rbp), %ecx
	movl	40(%rsp), %r8d          # 4-byte Reload
	callq	*128(%rax)
	movq	(%r12), %r12
	movq	24(%rbp), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_115
# BB#113:                               #   in Loop: Header=BB0_107 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB0_116
.LBB0_114:                              #   in Loop: Header=BB0_107 Depth=1
	movq	%r13, %rbp
	jmp	.LBB0_121
.LBB0_115:                              #   in Loop: Header=BB0_107 Depth=1
	xorl	%ecx, %ecx
.LBB0_116:                              #   in Loop: Header=BB0_107 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	movq	%r13, %rbp
	movq	%rbx, %r9
	movq	%r15, %r8
	movl	%r14d, %r11d
	je	.LBB0_118
# BB#117:                               #   in Loop: Header=BB0_107 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_118:                              #   in Loop: Header=BB0_107 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB0_120
# BB#119:                               #   in Loop: Header=BB0_107 Depth=1
	callq	DisposeObject
	movl	%r14d, %r11d
	movq	%r15, %r8
	movq	%rbx, %r9
.LBB0_120:                              #   in Loop: Header=BB0_107 Depth=1
	movl	272(%rsp), %ebx
	movl	280(%rsp), %r15d
.LBB0_121:                              # %.backedge2052
                                        #   in Loop: Header=BB0_107 Depth=1
	movq	8(%r12), %r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	jne	.LBB0_107
.LBB0_122:                              # %.loopexit2051
	movq	288(%rsp), %rax
	movl	%r9d, (%rax)
	movq	296(%rsp), %rax
	movl	%ebp, (%rax)
	movq	8(%rsp), %r12           # 8-byte Reload
	jmp	.LBB0_282
.LBB0_123:
	movq	%rbp, %r13
	movq	%rsi, %r15
	movq	8(%r12), %rax
	movq	16(%rax), %rbx
	movb	32(%rbx), %al
	movl	$1, %ebp
	testb	%al, %al
	jne	.LBB0_126
# BB#124:                               # %.lr.ph2356.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB0_125:                              # %.lr.ph2356
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_125
.LBB0_126:                              # %._crit_edge2357
	movq	BackEnd(%rip), %rcx
	cmpl	$0, 16(%rcx)
	je	.LBB0_301
# BB#127:
	testl	%r11d, %r11d
	je	.LBB0_268
# BB#128:
	movl	72(%r12), %eax
	addl	64(%r12), %eax
	jle	.LBB0_301
# BB#129:
	movl	56(%rbx), %edx
	addl	48(%rbx), %edx
	testl	%edx, %edx
	jle	.LBB0_301
# BB#130:                               # %ScaleFactor.exit1953
	cvtsi2ssl	%eax, %xmm1
	cvtsi2ssl	%edx, %xmm0
	divss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm1
	jbe	.LBB0_301
# BB#131:
	movq	%rbx, %rdi
	movq	%r9, 56(%rsp)           # 8-byte Spill
	movq	%r8, %r14
	movl	%r11d, 24(%rsp)         # 4-byte Spill
	movss	%xmm1, 40(%rsp)         # 4-byte Spill
	callq	*160(%rcx)
	movq	BackEnd(%rip), %rax
	movl	88(%r12), %ecx
	subl	64(%r12), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	48(%rbx), %xmm0
	mulss	40(%rsp), %xmm0         # 4-byte Folded Reload
	cvttss2si	%xmm0, %edi
	addl	%ecx, %edi
	xorl	%esi, %esi
	callq	*136(%rax)
	movq	BackEnd(%rip), %rax
	movss	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	40(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	*152(%rax)
	leaq	16(%rsp), %rax
	leaq	20(%rsp), %r10
	movl	$0, %r9d
	movq	%rbx, %rdi
	movl	%r14d, %esi
	movq	296(%rsp), %r14
	movq	56(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	24(%rsp), %r8d          # 4-byte Reload
	movl	%r13d, %ecx
	pushq	%rax
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi65:
	.cfi_adjust_cfa_offset -32
	movq	BackEnd(%rip), %rax
	callq	*168(%rax)
	movq	56(%rsp), %r9           # 8-byte Reload
	jmp	.LBB0_301
.LBB0_132:
	movq	%rbp, %r15
	movq	8(%r12), %rax
	movq	16(%rax), %rbx
	movb	32(%rbx), %al
	movl	$1, %ebp
	testb	%al, %al
	jne	.LBB0_135
# BB#133:                               # %.lr.ph2347.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB0_134:                              # %.lr.ph2347
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_134
.LBB0_135:                              # %._crit_edge2348
	movq	BackEnd(%rip), %rcx
	cmpl	$0, 16(%rcx)
	je	.LBB0_273
# BB#136:
	testl	%r11d, %r11d
	je	.LBB0_272
# BB#137:
	movl	%r15d, %edx
	addl	%r9d, %edx
	jle	.LBB0_241
# BB#138:
	movl	60(%rbx), %esi
	addl	52(%rbx), %esi
	testl	%esi, %esi
	jle	.LBB0_241
# BB#139:                               # %ScaleFactor.exit
	cvtsi2ssl	%edx, %xmm1
	cvtsi2ssl	%esi, %xmm0
	divss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm1
	jbe	.LBB0_241
# BB#140:
	movq	%rbx, %rdi
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movq	%r9, %r12
	movq	%r8, %r13
	movl	%r11d, %r14d
	movss	%xmm1, 40(%rsp)         # 4-byte Spill
	callq	*160(%rcx)
	movq	BackEnd(%rip), %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	52(%rbx), %xmm0
	mulss	40(%rsp), %xmm0         # 4-byte Folded Reload
	cvttss2si	%xmm0, %ecx
	movl	%r12d, %esi
	subl	%r13d, %esi
	movl	272(%rsp), %edx
	addl	%edx, %esi
	subl	%ecx, %esi
	xorl	%edi, %edi
	callq	*136(%rax)
	movq	BackEnd(%rip), %rax
	movss	.LCPI0_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	40(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	callq	*152(%rax)
	movl	52(%rbx), %edx
	movl	60(%rbx), %ecx
	leaq	16(%rsp), %rax
	leaq	20(%rsp), %r10
	movl	$0, %esi
	movl	$0, %r9d
	movq	%rbx, %rdi
	movl	%r14d, %r8d
	movq	296(%rsp), %r14
	pushq	%rax
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi67:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi68:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi69:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi70:
	.cfi_adjust_cfa_offset -32
	movq	BackEnd(%rip), %rax
	callq	*168(%rax)
	movq	%r12, %r9
	movq	8(%rsp), %r12           # 8-byte Reload
	jmp	.LBB0_273
.LBB0_141:
	movq	%rbp, %r15
	movq	8(%r12), %rax
	movq	16(%rax), %rbx
	movl	$1, %ebp
	cmpb	$0, 32(%rbx)
	jne	.LBB0_144
# BB#142:                               # %.lr.ph2366.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB0_143:                              # %.lr.ph2366
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB0_143
.LBB0_144:                              # %._crit_edge2367
	movq	BackEnd(%rip), %rcx
	cmpl	$0, 16(%rcx)
	je	.LBB0_233
# BB#145:
	testl	%r11d, %r11d
	je	.LBB0_274
# BB#146:
	movl	%r11d, 24(%rsp)         # 4-byte Spill
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movl	72(%r12), %esi
	testl	%esi, %esi
	jg	.LBB0_148
# BB#147:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movq	%r9, %r10
	movl	$.L.str.5, %r9d
	xorl	%eax, %eax
	movq	%r10, %r14
	callq	Error
	movq	%r14, %r9
	movl	72(%r12), %esi
	movq	BackEnd(%rip), %rcx
.LBB0_148:
	movq	%r9, 56(%rsp)           # 8-byte Spill
	movl	%r9d, %eax
	shll	$7, %eax
	cltd
	idivl	%esi
	movq	%r12, %r14
	movl	%eax, %r12d
	movq	%r15, %r13
	movl	%r13d, %eax
	shll	$7, %eax
	cltd
	idivl	%esi
	movl	%eax, %r15d
	movq	%rbx, %rdi
	callq	*160(%rcx)
	movq	BackEnd(%rip), %rax
	movl	88(%r14), %edi
	movl	272(%rsp), %esi
	subl	88(%rsp), %esi          # 4-byte Folded Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	*136(%rax)
	movq	BackEnd(%rip), %rax
	cvtsi2ssl	64(%r14), %xmm0
	movss	.LCPI0_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	cvtsi2ssl	72(%r14), %xmm1
	mulss	%xmm2, %xmm1
	callq	*152(%rax)
	leaq	16(%rsp), %rax
	leaq	20(%rsp), %r10
	xorl	%esi, %esi
	movl	$0, %r9d
	movq	%rbx, %rdi
	movl	24(%rsp), %r8d          # 4-byte Reload
	movl	%r12d, %edx
	movl	%r15d, %ecx
	pushq	%rax
.Lcfi71:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi72:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi73:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi74:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi75:
	.cfi_adjust_cfa_offset -32
	movq	BackEnd(%rip), %rax
	callq	*168(%rax)
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	296(%rsp), %r12
	movq	%r12, %rax
	movq	%r14, %r12
	movq	%rax, %r14
	jmp	.LBB0_277
.LBB0_149:
	movq	(%r12), %rax
	movq	16(%rax), %rdi
	movl	$1, %eax
	cmpb	$0, 32(%rdi)
	jne	.LBB0_152
# BB#150:                               # %.lr.ph2374.preheader
	movl	$1, %eax
	.p2align	4, 0x90
.LBB0_151:                              # %.lr.ph2374
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	movq	16(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB0_151
.LBB0_152:                              # %._crit_edge2375
	testl	%r11d, %r11d
	je	.LBB0_236
# BB#153:
	movq	%rsi, %r15
	movl	%r8d, %esi
	movl	%r9d, %edx
	movl	%ebp, %ecx
	movl	%r11d, %r8d
	movl	%r10d, %r9d
	pushq	%r14
.Lcfi76:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi77:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi78:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi79:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi80:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB0_282
.LBB0_154:
	movq	%rbp, %r13
	movq	8(%r12), %rax
	movq	16(%rax), %rbx
	movl	$1, %ebp
	cmpb	$0, 32(%rbx)
	jne	.LBB0_157
# BB#155:                               # %.lr.ph2398.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB0_156:                              # %.lr.ph2398
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB0_156
.LBB0_157:                              # %._crit_edge2399
	movq	BackEnd(%rip), %rax
	cmpl	$0, 20(%rax)
	je	.LBB0_237
# BB#158:
	movq	%r12, %r14
	movq	%r9, %r12
	testl	%r11d, %r11d
	je	.LBB0_278
# BB#159:
	movq	%rbx, %rdi
	movq	%r8, %r15
	callq	*160(%rax)
	movq	BackEnd(%rip), %rax
	movl	88(%r14), %edi
	movl	272(%rsp), %esi
	subl	%r15d, %esi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	*136(%rax)
	movq	BackEnd(%rip), %rax
	movl	76(%r14), %edi
	callq	*144(%rax)
	movl	48(%r14), %eax
	movl	%eax, 112(%rsp)
	movl	$8388607, 116(%rsp)     # imm = 0x7FFFFF
	movl	56(%r14), %eax
	movl	%eax, 120(%rsp)
	movl	52(%r14), %eax
	movl	%eax, 144(%rsp)
	movl	$8388607, 148(%rsp)     # imm = 0x7FFFFF
	movl	60(%r14), %eax
	movl	%eax, 152(%rsp)
	movl	76(%r14), %edx
	leaq	192(%rsp), %rdi
	leaq	112(%rsp), %rcx
	leaq	144(%rsp), %r8
	movl	$1, %r9d
	movq	%rbx, %rsi
	callq	RotateConstraint
	movl	192(%rsp), %edx
	movl	200(%rsp), %ecx
	leaq	16(%rsp), %rax
	leaq	20(%rsp), %r10
	xorl	%esi, %esi
	movl	$1, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdi
	pushq	%rax
.Lcfi81:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi82:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi83:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi84:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi85:
	.cfi_adjust_cfa_offset -32
	movq	BackEnd(%rip), %rax
	callq	*168(%rax)
	jmp	.LBB0_279
.LBB0_160:
	movq	8(%r12), %rax
	movq	16(%rax), %rdi
	movl	$1, %ebx
	cmpb	$0, 32(%rdi)
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %r13
	movl	$1, %eax
	jne	.LBB0_162
	.p2align	4, 0x90
.LBB0_161:                              # %.lr.ph2390
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	movq	16(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB0_161
.LBB0_162:                              # %._crit_edge2391
	leaq	16(%rsp), %r15
	leaq	20(%rsp), %r14
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movl	%r8d, %esi
	movq	%r9, 56(%rsp)           # 8-byte Spill
	movl	%r9d, %edx
	movl	%r13d, %ecx
	movl	%r11d, %ebp
	movl	%r11d, %r8d
	movl	%r10d, %r12d
	movl	%r10d, %r9d
	pushq	%r15
.Lcfi86:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi87:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi88:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi89:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi90:
	.cfi_adjust_cfa_offset -32
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rax
	movq	16(%rax), %rdi
	cmpb	$0, 32(%rdi)
	jne	.LBB0_165
# BB#163:                               # %.lr.ph2382.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB0_164:                              # %.lr.ph2382
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebx
	movq	16(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB0_164
.LBB0_165:                              # %._crit_edge2383
	movq	88(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	56(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r13d, %ecx
	movl	%ebp, %r8d
	movl	%r12d, %r9d
	leaq	16(%rsp), %rax
	pushq	%rax
.Lcfi91:
	.cfi_adjust_cfa_offset 8
	leaq	28(%rsp), %rax
	pushq	%rax
.Lcfi92:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi93:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi94:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi95:
	.cfi_adjust_cfa_offset -32
	movslq	%ebp, %rax
	movq	8(%rsp), %r12           # 8-byte Reload
	movl	48(%r12,%rax,4), %ecx
	movq	288(%rsp), %rdx
	movl	%ecx, (%rdx)
	movl	56(%r12,%rax,4), %eax
	jmp	.LBB0_232
.LBB0_166:
	movl	%r11d, %r13d
	movq	%r8, %r11
	movq	%rbp, %r14
	movq	(%r12), %rax
	movq	16(%rax), %rbx
	movl	$1, %ebp
	cmpb	$0, 32(%rbx)
	jne	.LBB0_169
# BB#167:                               # %.lr.ph2406.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB0_168:                              # %.lr.ph2406
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB0_168
.LBB0_169:                              # %._crit_edge2407
	movq	BackEnd(%rip), %rax
	cmpl	$0, 32(%rax)
	movq	%r12, 8(%rsp)           # 8-byte Spill
	je	.LBB0_239
# BB#170:
	testl	%r13d, %r13d
	je	.LBB0_283
# BB#171:
	movq	8(%r12), %rdi
	.p2align	4, 0x90
.LBB0_172:                              # =>This Inner Loop Header: Depth=1
	addq	$16, %rdi
	movq	(%rdi), %rdi
	movzbl	32(%rdi), %ecx
	testb	%cl, %cl
	je	.LBB0_172
# BB#173:
	cmpb	$19, %cl
	movl	%r10d, 40(%rsp)         # 4-byte Spill
	jne	.LBB0_289
# BB#174:
	movq	8(%rdi), %rdx
	addq	$16, %rdx
.LBB0_175:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rcx
	leaq	16(%rcx), %rdx
	cmpb	$0, 32(%rcx)
	je	.LBB0_175
# BB#176:
	movq	(%rdi), %rdx
	addq	$16, %rdx
.LBB0_177:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %r15
	leaq	16(%r15), %rdx
	cmpb	$0, 32(%r15)
	je	.LBB0_177
# BB#178:
	movq	%rcx, %rdi
	jmp	.LBB0_290
.LBB0_179:
	movq	(%r12), %rax
	movq	16(%rax), %rbx
	movl	$1, %r15d
	cmpb	$0, 32(%rbx)
	jne	.LBB0_182
# BB#180:                               # %.lr.ph2414.preheader
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB0_181:                              # %.lr.ph2414
                                        # =>This Inner Loop Header: Depth=1
	incl	%r15d
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB0_181
.LBB0_182:                              # %._crit_edge2415
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	BackEnd(%rip), %rax
	cmpl	$0, 24(%rax)
	je	.LBB0_240
# BB#183:
	testl	%r11d, %r11d
	je	.LBB0_284
# BB#184:
	movq	8(%r12), %rbp
	.p2align	4, 0x90
.LBB0_185:                              # =>This Inner Loop Header: Depth=1
	addq	$16, %rbp
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %ecx
	testb	%cl, %cl
	je	.LBB0_185
# BB#186:
	movq	%r12, %rdi
	cmpb	$19, %cl
	jne	.LBB0_293
# BB#187:
	movq	8(%rbp), %rdx
	addq	$16, %rdx
.LBB0_188:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rcx
	leaq	16(%rcx), %rdx
	cmpb	$0, 32(%rcx)
	je	.LBB0_188
# BB#189:
	movq	(%rbp), %rdx
	addq	$16, %rdx
.LBB0_190:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %r12
	leaq	16(%r12), %rdx
	cmpb	$0, 32(%r12)
	je	.LBB0_190
# BB#191:
	movq	%rcx, %rbp
	jmp	.LBB0_294
.LBB0_192:
	movq	%rbx, %rdi
	movl	%r8d, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%ebp, %ecx
	movl	%r11d, %r8d
	pushq	%r14
.Lcfi96:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi97:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi98:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi99:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi100:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB0_282
.LBB0_193:
	movq	8(%r12), %rbx
	cmpq	%r12, %rbx
	je	.LBB0_202
# BB#194:                               # %.preheader2030.preheader
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_195:                              # %.preheader2030
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_196 Depth 2
	movq	%rbx, %r13
	.p2align	4, 0x90
.LBB0_196:                              #   Parent Loop BB0_195 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r13), %r13
	movzbl	32(%r13), %eax
	testb	%al, %al
	je	.LBB0_196
# BB#197:                               #   in Loop: Header=BB0_195 Depth=1
	cmpb	$1, %al
	je	.LBB0_201
# BB#198:                               #   in Loop: Header=BB0_195 Depth=1
	cmpb	$9, %al
	jne	.LBB0_200
# BB#199:                               #   in Loop: Header=BB0_195 Depth=1
	movq	%r13, %rdi
	movq	%r9, %r15
	movq	%r8, %rbp
	movl	%r11d, %r14d
	movl	%r10d, %r12d
	callq	SplitIsDefinite
	movl	%r12d, %r10d
	movl	%r14d, %r11d
	movq	296(%rsp), %r14
	movq	%rbp, %r8
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	%r15, %r9
	movq	8(%rsp), %r12           # 8-byte Reload
	testl	%eax, %eax
	je	.LBB0_201
	jmp	.LBB0_258
.LBB0_200:                              #   in Loop: Header=BB0_195 Depth=1
	addb	$-9, %al
	cmpb	$91, %al
	jb	.LBB0_258
.LBB0_201:                              # %.critedge
                                        #   in Loop: Header=BB0_195 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r12, %rbx
	jne	.LBB0_195
.LBB0_202:                              # %.thread
	movq	288(%rsp), %rax
	movl	%r9d, (%rax)
	movl	%ebp, (%r14)
	jmp	.LBB0_282
.LBB0_203:
	movl	68(%r12), %ecx
	movslq	%r11d, %rdx
	movl	48(%rax,%rdx,4), %edx
	subl	%edx, %ecx
	movq	%rbx, %r14
	leaq	16(%rsp), %r15
	leaq	20(%rsp), %r10
	movq	%r9, %rbx
	movl	$0, %r9d
	movq	%rax, %rdi
	movl	%r8d, %esi
	movl	%r11d, %r8d
	pushq	%r15
.Lcfi101:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi102:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi103:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi104:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi105:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB0_212
.LBB0_204:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movl	%r11d, %edx
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movq	%r9, %r12
	movq	%r8, %r14
	movl	%r11d, 24(%rsp)         # 4-byte Spill
	movl	%r10d, 40(%rsp)         # 4-byte Spill
	callq	FindShift
	subl	%eax, %r14d
	xorl	%r15d, %r15d
	subl	%eax, %r12d
	cmovsl	%r15d, %r12d
	addl	%eax, %r13d
	cmovsl	%r15d, %r13d
	leaq	16(%rsp), %rax
	leaq	20(%rsp), %r10
	movq	%rbx, %rdi
	movl	%r14d, %esi
	movl	%r12d, %edx
	movq	8(%rsp), %r12           # 8-byte Reload
	movl	%r13d, %ecx
	movl	24(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %r8d
	movl	40(%rsp), %r9d          # 4-byte Reload
	pushq	%rax
.Lcfi106:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi107:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi108:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi109:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi110:
	.cfi_adjust_cfa_offset -32
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	%ebx, %edx
	callq	FindShift
	movl	20(%rsp), %ecx
	addl	%eax, %ecx
	movl	$0, %edx
	cmovnsl	%ecx, %edx
	cmpl	$8388607, %ecx          # imm = 0x7FFFFF
	movl	$8388607, %ecx          # imm = 0x7FFFFF
	cmovgl	%ecx, %edx
	movq	288(%rsp), %rsi
	movl	%edx, (%rsi)
	movl	16(%rsp), %edx
	subl	%eax, %edx
	cmovnsl	%edx, %r15d
	cmpl	$8388607, %edx          # imm = 0x7FFFFF
	cmovgl	%ecx, %r15d
	movq	296(%rsp), %rax
	movl	%r15d, (%rax)
	jmp	.LBB0_282
.LBB0_205:
	movslq	%r11d, %rcx
	movl	48(%rax,%rcx,4), %edx
	movl	56(%rax,%rcx,4), %ecx
	movq	%rbx, %r14
	leaq	16(%rsp), %r15
	leaq	20(%rsp), %r10
	movq	%r9, %rbx
	movl	$0, %r9d
	movq	%rax, %rdi
	movl	%r8d, %esi
	movl	%r11d, %r8d
	pushq	%r15
.Lcfi111:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi112:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi113:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi114:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi115:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB0_212
.LBB0_206:
	movl	%r8d, 36(%r12)
	cmpb	$0, 64(%r12)
	je	.LBB0_218
# BB#207:
	movq	finfo(%rip), %rax
	movl	40(%r12), %ecx
	jmp	.LBB0_215
.LBB0_208:
	movl	%r8d, 88(%r12)
	movq	%r9, %rdx
	jmp	.LBB0_211
.LBB0_209:
	movl	88(%r12), %ecx
	movl	%ecx, %esi
	subl	48(%r12), %esi
	movl	%r14d, %r8d
	subl	%r15d, %r8d
	movl	%r8d, %edx
	subl	%r13d, %edx
	addl	56(%r12), %ecx
	addl	%r9d, %r8d
	movq	%rax, %rdi
	movq	%r9, %r12
	callq	*216(%r11)
.LBB0_210:
	movq	%r15, %r8
	movq	%r12, %rdx
	movq	8(%rsp), %r12           # 8-byte Reload
.LBB0_211:
	leaq	16(%rsp), %rax
	leaq	20(%rsp), %r10
	movl	$0, %r9d
	movq	%rbx, %rdi
	movl	%r8d, %esi
	movl	24(%rsp), %r8d          # 4-byte Reload
	movl	%r13d, %ecx
	pushq	%rax
.Lcfi116:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi117:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi118:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi119:
	.cfi_adjust_cfa_offset 8
	movq	%rdx, %rbx
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi120:
	.cfi_adjust_cfa_offset -32
.LBB0_212:
	movq	288(%rsp), %rax
	movl	%ebx, (%rax)
	jmp	.LBB0_281
.LBB0_213:
	movl	%r8d, 88(%r12)
	cmpb	$0, 41(%r12)
	je	.LBB0_218
# BB#214:
	movq	finfo(%rip), %rax
	movl	76(%r12), %ecx
.LBB0_215:
	andl	$4095, %ecx             # imm = 0xFFF
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	movq	48(%rax,%rcx), %rdi
	movzwl	42(%rdi), %eax
	cmpl	font_curr_page(%rip), %eax
	jge	.LBB0_218
# BB#216:
	movq	%r9, %rbx
	movq	%rsi, %r15
	callq	FontPageUsed
.LBB0_217:
	movq	%r15, %rsi
	movq	%rbx, %r9
.LBB0_218:
	movl	%r9d, (%rsi)
	movl	%ebp, (%r14)
	jmp	.LBB0_282
.LBB0_219:                              # %.lr.ph2430.preheader
	movq	%rsi, %r15
	movl	$1, %eax
	.p2align	4, 0x90
.LBB0_220:                              # %.lr.ph2430
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	movq	16(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB0_220
.LBB0_221:                              # %._crit_edge2431
	movslq	%r11d, %rcx
	movl	48(%rdi,%rcx,4), %edx
	cmpl	%r9d, %edx
	cmovll	%r9d, %edx
	movl	56(%rdi,%rcx,4), %ecx
	cmpl	%ebp, %ecx
	cmovll	%ebp, %ecx
	movl	%r8d, %esi
	movl	%r11d, %r8d
	movl	%r10d, %r9d
	pushq	%r14
.Lcfi121:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi122:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi123:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi124:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi125:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB0_282
.LBB0_222:
	cmpq	%rax, %r12
	je	.LBB0_231
# BB#223:                               # %.preheader2048.preheader
	movq	%r13, 32(%rsp)          # 8-byte Spill
.LBB0_224:                              # %.preheader2048
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_225 Depth 2
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB0_225:                              #   Parent Loop BB0_224 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_225
# BB#226:                               #   in Loop: Header=BB0_224 Depth=1
	cmpb	$1, %al
	je	.LBB0_230
# BB#227:                               #   in Loop: Header=BB0_224 Depth=1
	cmpb	$9, %al
	jne	.LBB0_229
# BB#228:                               #   in Loop: Header=BB0_224 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %r14
	movq	%r9, %r15
	movq	%rbp, %r13
	movq	%r8, %rbp
	callq	SplitIsDefinite
	movq	%rbp, %r8
	movq	%r13, %rbp
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	%r15, %r9
	movq	%r14, %r15
	testl	%eax, %eax
	je	.LBB0_230
	jmp	.LBB0_303
.LBB0_229:                              #   in Loop: Header=BB0_224 Depth=1
	addb	$-9, %al
	cmpb	$91, %al
	jb	.LBB0_303
.LBB0_230:                              # %.critedge14
                                        #   in Loop: Header=BB0_224 Depth=1
	movq	8(%r12), %r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	jne	.LBB0_224
.LBB0_231:                              # %.critedge1911
	movq	8(%rsp), %r12           # 8-byte Reload
	movl	48(%r12), %eax
	movq	288(%rsp), %rcx
	movl	%eax, (%rcx)
	movl	56(%r12), %eax
.LBB0_232:
	movq	296(%rsp), %rcx
	movl	%eax, (%rcx)
	jmp	.LBB0_282
.LBB0_233:
	cmpl	$128, 64(%r12)
	movq	%r15, %r13
	jne	.LBB0_277
# BB#234:
	cmpl	$128, 72(%r12)
	jne	.LBB0_277
# BB#235:
	leaq	16(%rsp), %rax
	movq	%r9, %rdx
	movl	%r10d, %r9d
	leaq	20(%rsp), %r10
	movq	%rbx, %rdi
	movl	%r8d, %esi
	movl	%r13d, %ecx
	movl	%r11d, %r8d
	pushq	%rax
.Lcfi126:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi127:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi128:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi129:
	.cfi_adjust_cfa_offset 8
	movq	%rdx, %rbx
	callq	FixAndPrintObject
	movq	%rbx, %r9
	addq	$32, %rsp
.Lcfi130:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB0_277
.LBB0_236:
	movl	48(%rdi), %edx
	movl	56(%rdi), %ecx
	leaq	16(%rsp), %rbp
	movq	%rsi, %r15
	movq	%rbx, %r10
	leaq	20(%rsp), %rbx
	movq	%r8, %rsi
	movl	$0, %r8d
	movl	$0, %r9d
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	pushq	%rbp
.Lcfi131:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi132:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi133:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi134:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi135:
	.cfi_adjust_cfa_offset -32
	movl	48(%r12), %eax
	movl	%eax, (%r15)
	movl	56(%r12), %eax
	movl	%eax, (%r14)
	jmp	.LBB0_282
.LBB0_237:
	cmpl	$0, 76(%r12)
	jne	.LBB0_280
# BB#238:
	leaq	16(%rsp), %rax
	movq	%r9, %rdx
	movl	%r10d, %r9d
	leaq	20(%rsp), %r10
	movq	%rbx, %rdi
	movl	%r8d, %esi
	movl	%r13d, %ecx
	movl	%r11d, %r8d
	pushq	%rax
.Lcfi136:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi137:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi138:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi139:
	.cfi_adjust_cfa_offset 8
	movq	%rdx, %rbx
	callq	FixAndPrintObject
	movq	%rbx, %r9
	addq	$32, %rsp
.Lcfi140:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB0_280
.LBB0_239:
	leaq	16(%rsp), %rax
	movq	%r9, %r12
	movl	%r10d, %r9d
	leaq	20(%rsp), %r10
	movq	%rbx, %rdi
	movl	%r11d, %esi
	movl	%r12d, %edx
	movl	%r14d, %ecx
	movl	%r13d, %r8d
	pushq	%rax
.Lcfi141:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi142:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi143:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi144:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi145:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB0_292
.LBB0_240:
	leaq	16(%rsp), %rax
	leaq	20(%rsp), %r13
	movq	%rbx, %rdi
	movl	%r8d, %esi
	movl	%r9d, %edx
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	%r11d, %r8d
	movq	%r9, %rbp
	movl	%r10d, %r9d
	pushq	%rax
.Lcfi146:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	movq	%rcx, %rbx
.Lcfi147:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi148:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi149:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi150:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB0_288
.LBB0_241:                              # %ScaleFactor.exit.thread
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB0_243
# BB#242:
	cmpb	$0, 64(%rbx)
	je	.LBB0_273
.LBB0_243:
	movl	$23, %edi
	movl	$1, %esi
	movl	$.L.str.2, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r13, %r8
	movq	%r9, %rbx
	callq	Error
	movq	%rbx, %r9
	jmp	.LBB0_273
.LBB0_244:
	movslq	%r11d, %rax
	movl	48(%rbp,%rax,4), %edx
	movl	%edx, 112(%rsp)
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	56(%rbp,%rax,4), %eax
	movl	%eax, 144(%rsp)
	xorl	$1, %ecx
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	movq	%r12, %rax
	movq	8(%r10), %r12
	cmpq	%rax, %r12
	movl	%r11d, 24(%rsp)         # 4-byte Spill
	je	.LBB0_302
# BB#245:                               # %.preheader2013.lr.ph.preheader
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movl	$1, %ecx
	xorl	%ebx, %ebx
	movq	%r9, 56(%rsp)           # 8-byte Spill
.LBB0_246:                              # %.preheader2013.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_247 Depth 2
                                        #       Child Loop BB0_248 Depth 3
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movl	%ecx, 32(%rsp)          # 4-byte Spill
.LBB0_247:                              # %.preheader2013
                                        #   Parent Loop BB0_246 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_248 Depth 3
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB0_248:                              #   Parent Loop BB0_246 Depth=1
                                        #     Parent Loop BB0_247 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_248
# BB#249:                               #   in Loop: Header=BB0_247 Depth=2
	cmpb	$9, %al
	je	.LBB0_252
# BB#250:                               #   in Loop: Header=BB0_247 Depth=2
	cmpb	$1, %al
	je	.LBB0_254
# BB#251:                               #   in Loop: Header=BB0_247 Depth=2
	addb	$-9, %al
	cmpb	$90, %al
	ja	.LBB0_253
	jmp	.LBB0_327
.LBB0_252:                              #   in Loop: Header=BB0_247 Depth=2
	movq	%rbx, %rdi
	movq	%r8, %r13
	movl	%r11d, %r15d
	movq	%r10, %r14
	callq	SplitIsDefinite
	movl	32(%rsp), %ecx          # 4-byte Reload
	movq	%r14, %r10
	movl	%r15d, %r11d
	movl	280(%rsp), %r15d
	movq	%r13, %r8
	movq	56(%rsp), %r9           # 8-byte Reload
	testl	%eax, %eax
	jne	.LBB0_327
.LBB0_253:                              # %.critedge10.backedge
                                        #   in Loop: Header=BB0_247 Depth=2
	movq	8(%r12), %r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	jne	.LBB0_247
	jmp	.LBB0_331
.LBB0_254:                              #   in Loop: Header=BB0_246 Depth=1
	xorl	%r14d, %r14d
	testl	%ecx, %ecx
	je	.LBB0_256
# BB#255:                               #   in Loop: Header=BB0_246 Depth=1
	movb	45(%rbx), %al
	andb	$2, %al
	shrb	%al
	jmp	.LBB0_257
.LBB0_256:                              #   in Loop: Header=BB0_246 Depth=1
	xorl	%eax, %eax
.LBB0_257:                              # %.critedge10.outer
                                        #   in Loop: Header=BB0_246 Depth=1
	movzbl	%al, %ecx
	movq	8(%r12), %r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	jne	.LBB0_246
	jmp	.LBB0_403
.LBB0_258:                              # %.critedge4.preheader
	movq	8(%rbx), %rbp
	cmpq	%r12, %rbp
	je	.LBB0_337
# BB#259:                               # %.preheader2028.lr.ph.preheader
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
.LBB0_260:                              # %.preheader2028
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_261 Depth 2
	movq	%rbp, %rdi
	.p2align	4, 0x90
.LBB0_261:                              #   Parent Loop BB0_260 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdi), %rdi
	movzbl	32(%rdi), %eax
	testb	%al, %al
	je	.LBB0_261
# BB#262:                               #   in Loop: Header=BB0_260 Depth=1
	cmpb	$9, %al
	je	.LBB0_265
# BB#263:                               #   in Loop: Header=BB0_260 Depth=1
	cmpb	$1, %al
	jne	.LBB0_266
# BB#264:                               # %.critedge4.outer
                                        #   in Loop: Header=BB0_260 Depth=1
	movq	8(%rbp), %rbp
	cmpq	%r12, %rbp
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	jne	.LBB0_260
	jmp	.LBB0_337
.LBB0_265:                              #   in Loop: Header=BB0_260 Depth=1
	movq	%r9, %r15
	movq	%r8, %rbx
	movl	%r11d, %r14d
	movl	%r10d, %r12d
	callq	SplitIsDefinite
	movl	%r12d, %r10d
	movl	%r14d, %r11d
	movq	%rbx, %r8
	movq	%r15, %r9
	movq	8(%rsp), %r12           # 8-byte Reload
	testl	%eax, %eax
	je	.LBB0_267
	jmp	.LBB0_332
.LBB0_266:                              #   in Loop: Header=BB0_260 Depth=1
	addb	$-9, %al
	cmpb	$90, %al
	jbe	.LBB0_332
.LBB0_267:                              # %.critedge4.backedge
                                        #   in Loop: Header=BB0_260 Depth=1
	movq	8(%rbp), %rbp
	cmpq	%r12, %rbp
	jne	.LBB0_260
	jmp	.LBB0_337
.LBB0_268:
	movl	%r8d, 88(%r12)
	movl	%r9d, 64(%r12)
	movq	%r13, %rcx
	movl	%ecx, 72(%r12)
	movl	%ecx, %esi
	addl	%r9d, %esi
	jle	.LBB0_298
# BB#269:
	movl	48(%rbx), %edx
	movl	56(%rbx), %ecx
	leal	(%rcx,%rdx), %edi
	testl	%edi, %edi
	jle	.LBB0_298
# BB#270:                               # %ScaleFactor.exit1951
	cvtsi2ssl	%esi, %xmm0
	cvtsi2ssl	%edi, %xmm1
	divss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jbe	.LBB0_298
# BB#271:
	leaq	16(%rsp), %rax
	leaq	20(%rsp), %r10
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	movq	%r9, %r11
	xorl	%r9d, %r9d
	movq	%rbx, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	pushq	%rax
.Lcfi151:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi152:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi153:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi154:
	.cfi_adjust_cfa_offset 8
	movq	%r11, %rbx
	callq	FixAndPrintObject
	movq	%rbx, %r9
	addq	$32, %rsp
.Lcfi155:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB0_301
.LBB0_272:
	leaq	16(%rsp), %rax
	leaq	20(%rsp), %r10
	movq	%r8, %rsi
	movl	$0, %r8d
	movq	%r9, %rdx
	movl	$0, %r9d
	movq	%rbx, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %ecx
	pushq	%rax
.Lcfi156:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi157:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi158:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi159:
	.cfi_adjust_cfa_offset 8
	movq	%rdx, %rbx
	callq	FixAndPrintObject
	movq	%rbx, %r9
	addq	$32, %rsp
.Lcfi160:
	.cfi_adjust_cfa_offset -32
.LBB0_273:
	movq	288(%rsp), %rax
	movl	%r9d, (%rax)
	movl	%r15d, (%r14)
	jmp	.LBB0_282
.LBB0_274:
	movl	64(%r12), %esi
	testl	%esi, %esi
	jg	.LBB0_276
# BB#275:
	movq	%r8, %r13
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movq	%r9, %r10
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	movq	%r10, %r14
	callq	Error
	movq	%r13, %r8
	movq	%r14, %r9
	movq	296(%rsp), %r14
	movl	64(%r12), %esi
.LBB0_276:
	movl	%r8d, 88(%r12)
	movl	%r9d, %eax
	shll	$7, %eax
	cltd
	idivl	%esi
	movl	%eax, %ecx
	movq	%r15, %r13
	movl	%r13d, %eax
	shll	$7, %eax
	cltd
	idivl	%esi
	leaq	16(%rsp), %r10
	movq	%r9, %r15
	leaq	20(%rsp), %r11
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rbx, %rdi
	movl	%ecx, %edx
	movl	%eax, %ecx
	pushq	%r10
.Lcfi161:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi162:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi163:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi164:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	movq	%r15, %r9
	addq	$32, %rsp
.Lcfi165:
	.cfi_adjust_cfa_offset -32
.LBB0_277:
	movq	288(%rsp), %rax
	movl	%r9d, (%rax)
	movl	%r13d, (%r14)
	jmp	.LBB0_282
.LBB0_278:
	movl	%r8d, 88(%r14)
	movl	48(%r14), %eax
	movl	%eax, 112(%rsp)
	movl	$8388607, 116(%rsp)     # imm = 0x7FFFFF
	movl	56(%r14), %eax
	movl	%eax, 120(%rsp)
	movl	52(%r14), %eax
	movl	%eax, 144(%rsp)
	movl	$8388607, 148(%rsp)     # imm = 0x7FFFFF
	movl	60(%r14), %eax
	movl	%eax, 152(%rsp)
	movl	76(%r14), %edx
	leaq	192(%rsp), %rdi
	leaq	112(%rsp), %rcx
	leaq	144(%rsp), %r8
	xorl	%r9d, %r9d
	movq	%rbx, %rsi
	callq	RotateConstraint
	movl	192(%rsp), %edx
	movl	200(%rsp), %ecx
	leaq	16(%rsp), %rax
	leaq	20(%rsp), %r10
	movl	$0, %esi
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdi
	pushq	%rax
.Lcfi166:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi167:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi168:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi169:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi170:
	.cfi_adjust_cfa_offset -32
.LBB0_279:
	movq	%r12, %r9
	movq	%r14, %r12
.LBB0_280:
	movq	288(%rsp), %rax
	movl	%r9d, (%rax)
.LBB0_281:
	movq	296(%rsp), %rax
	movl	%r13d, (%rax)
.LBB0_282:
	movq	%r12, %rax
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_283:
	movl	%r9d, 48(%r12)
	movl	%r14d, 56(%r12)
	movl	%r11d, %eax
	subl	%r9d, %eax
	movl	%eax, 88(%r12)
	leaq	16(%rsp), %rax
	movq	%r9, %r12
	movl	%r10d, %r9d
	leaq	20(%rsp), %r10
	movl	$0, %r8d
	movq	%rbx, %rdi
	movl	%r11d, %esi
	movl	%r12d, %edx
	movl	%r14d, %ecx
	pushq	%rax
.Lcfi171:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi172:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi173:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi174:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi175:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB0_292
.LBB0_284:
	movl	$4095, %eax             # imm = 0xFFF
	andl	76(%r12), %eax
	je	.LBB0_287
# BB#285:
	movq	finfo(%rip), %rcx
	movl	%eax, %eax
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	movq	48(%rcx,%rax), %rdi
	movzwl	42(%rdi), %eax
	cmpl	font_curr_page(%rip), %eax
	jge	.LBB0_287
# BB#286:
	movq	%r9, %r14
	movq	%r8, %rbp
	callq	FontPageUsed
	movq	%rbp, %r8
	movq	%r14, %r9
	movq	296(%rsp), %r14
.LBB0_287:
	movl	%r9d, 48(%r12)
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 56(%r12)
	subl	%r9d, %r8d
	movl	%r8d, 88(%r12)
	leaq	16(%rsp), %rax
	leaq	20(%rsp), %rbp
	movl	$0, %r8d
	movq	%r9, %rsi
	movl	$0, %r9d
	movq	%rbx, %rdi
	movl	%esi, %edx
	pushq	%rax
.Lcfi176:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	movq	%rcx, %rbx
.Lcfi177:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi178:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi179:
	.cfi_adjust_cfa_offset 8
	movq	%rsi, %rbp
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi180:
	.cfi_adjust_cfa_offset -32
.LBB0_288:
	movq	288(%rsp), %rax
	jmp	.LBB0_297
.LBB0_289:                              # %.loopexit2004.loopexit3770
	xorl	%r15d, %r15d
.LBB0_290:                              # %.loopexit2004
	movl	%r13d, 24(%rsp)         # 4-byte Spill
	movslq	%r13d, %rcx
	movl	%r9d, 48(%r12,%rcx,4)
	movq	%r14, 64(%rsp)          # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	%r14d, 56(%r12,%rcx,4)
	movl	88(%r12), %esi
	movl	272(%rsp), %ecx
	movl	%ecx, %r13d
	movq	%r11, %r14
	subl	%r14d, %r13d
	leal	(%r13,%r9), %edx
	movq	%r12, %rcx
	movq	%r9, %r12
	callq	*120(%rax)
	leaq	16(%rsp), %rax
	leaq	20(%rsp), %r10
	movq	%rbx, %rdi
	movl	%r14d, %esi
	movq	64(%rsp), %r14          # 8-byte Reload
	movl	%r12d, %edx
	movl	%r14d, %ecx
	movl	24(%rsp), %r8d          # 4-byte Reload
	movl	40(%rsp), %r9d          # 4-byte Reload
	pushq	%rax
.Lcfi181:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi182:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi183:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi184:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi185:
	.cfi_adjust_cfa_offset -32
	testq	%r15, %r15
	je	.LBB0_292
# BB#291:
	movq	BackEnd(%rip), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	88(%rcx), %esi
	movq	32(%rsp), %rdx          # 8-byte Reload
	addl	48(%rcx,%rdx,4), %r13d
	movq	%r15, %rdi
	movl	%r13d, %edx
	callq	*120(%rax)
.LBB0_292:
	movq	288(%rsp), %rax
	movl	%r12d, (%rax)
	movq	296(%rsp), %rax
	movl	%r14d, (%rax)
	movq	8(%rsp), %r12           # 8-byte Reload
	jmp	.LBB0_282
.LBB0_293:                              # %.loopexit2006.loopexit3771
	xorl	%r12d, %r12d
.LBB0_294:                              # %.loopexit2006
	movslq	%r11d, %rcx
	movl	%r9d, 48(%rdi,%rcx,4)
	movq	64(%rsp), %r14          # 8-byte Reload
	movl	%r14d, 56(%rdi,%rcx,4)
	movl	88(%rdi), %esi
	movl	272(%rsp), %edx
	subl	%r8d, %edx
	subl	60(%rdi), %edx
	movq	%rdi, 8(%rsp)           # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	%r9, %r13
	movl	%r11d, 24(%rsp)         # 4-byte Spill
	callq	*192(%rax)
	movq	BackEnd(%rip), %rax
	movq	%rbp, %rdi
	callq	*176(%rax)
	movq	BackEnd(%rip), %rax
	callq	*168(%rax)
	leal	(%r14,%r13), %eax
	leaq	16(%rsp), %r11
	leaq	20(%rsp), %r10
	movl	$0, %r9d
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%r13, %rbp
	movl	%r13d, %edx
	movl	24(%rsp), %r8d          # 4-byte Reload
	movl	%r14d, %ecx
	pushq	%r11
.Lcfi186:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi187:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi188:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi189:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi190:
	.cfi_adjust_cfa_offset -32
	testq	%r12, %r12
	je	.LBB0_296
# BB#295:
	movq	BackEnd(%rip), %rax
	movq	%r12, %rdi
	callq	*176(%rax)
.LBB0_296:
	movq	BackEnd(%rip), %rax
	callq	*168(%rax)
	movq	296(%rsp), %r12
	movq	%r12, %r14
	movq	288(%rsp), %rax
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	8(%rsp), %r12           # 8-byte Reload
.LBB0_297:
	movl	%ebp, (%rax)
	movl	%ebx, (%r14)
	jmp	.LBB0_282
.LBB0_298:                              # %ScaleFactor.exit1951.thread
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB0_300
# BB#299:
	cmpb	$0, 64(%rbx)
	je	.LBB0_301
.LBB0_300:
	addq	$32, %rbx
	movl	$23, %edi
	movl	$2, %esi
	movl	$.L.str.3, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%r9, %rbx
	callq	Error
	movq	%rbx, %r9
.LBB0_301:                              # %ScaleFactor.exit1953.thread
	movl	%r9d, (%r15)
	movl	%r13d, (%r14)
	jmp	.LBB0_282
.LBB0_302:
	movl	$1, %ecx
	xorl	%r14d, %r14d
	jmp	.LBB0_406
.LBB0_303:
	movl	%r8d, %ecx
	subl	%r9d, %ecx
	movl	48(%rbx), %eax
	movl	%ecx, 136(%rsp)         # 4-byte Spill
	addl	%ecx, %eax
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	leal	(%rbp,%r9), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movq	8(%r12), %r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	movq	%rbx, %rbp
	je	.LBB0_313
# BB#304:                               # %.preheader2046.lr.ph.preheader
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
.LBB0_305:                              # %.preheader2046
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_306 Depth 2
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB0_306:                              #   Parent Loop BB0_305 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB0_306
# BB#307:                               #   in Loop: Header=BB0_305 Depth=1
	cmpb	$9, %al
	je	.LBB0_310
# BB#308:                               #   in Loop: Header=BB0_305 Depth=1
	cmpb	$1, %al
	jne	.LBB0_311
# BB#309:                               # %.critedge15.outer
                                        #   in Loop: Header=BB0_305 Depth=1
	movq	8(%r12), %r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	movq	%rbp, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rax, 40(%rsp)          # 8-byte Spill
	jne	.LBB0_305
	jmp	.LBB0_313
.LBB0_310:                              #   in Loop: Header=BB0_305 Depth=1
	movq	%rbp, %rdi
	movq	%r9, %r14
	movq	%r8, %r15
	callq	SplitIsDefinite
	movq	%r15, %r8
	movl	280(%rsp), %r15d
	movq	%r14, %r9
	testl	%eax, %eax
	je	.LBB0_312
	jmp	.LBB0_428
.LBB0_311:                              #   in Loop: Header=BB0_305 Depth=1
	addb	$-9, %al
	cmpb	$90, %al
	jbe	.LBB0_428
.LBB0_312:                              # %.critedge15.backedge
                                        #   in Loop: Header=BB0_305 Depth=1
	movq	8(%r12), %r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	jne	.LBB0_305
.LBB0_313:                              # %.preheader2044.thread
	movl	56(%rbx), %edi
.LBB0_314:
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
.LBB0_315:                              # %._crit_edge2692
	movl	72(%rsp), %ecx          # 4-byte Reload
	subl	136(%rsp), %ecx         # 4-byte Folded Reload
	addl	%edi, %ecx
	testl	%eax, %eax
	movl	48(%rsp), %edx          # 4-byte Reload
	jne	.LBB0_361
# BB#316:                               # %._crit_edge2692
	cmpl	%ecx, %edx
	jge	.LBB0_361
# BB#317:
	movl	$0, 112(%rsp)
	movl	%edx, 116(%rsp)
	movl	%edx, 120(%rsp)
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	%ecx, 56(%rbp)
	movq	BackEnd(%rip), %rax
	cmpl	$0, 16(%rax)
	je	.LBB0_529
# BB#318:
	movl	%ecx, %r14d
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movq	%rbp, %rbx
	movq	%r9, %r12
	leaq	112(%rsp), %rsi
	movq	%rbx, %rdi
	callq	InsertScale
	testl	%eax, %eax
	je	.LBB0_528
# BB#319:
	movq	24(%rbx), %rbx
.LBB0_320:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB0_320
# BB#321:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	8(%rax), %rax
	movq	%rax, %rbp
.LBB0_322:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %ecx
	testb	%cl, %cl
	je	.LBB0_322
# BB#323:
	subl	48(%rsp), %r14d         # 4-byte Folded Reload
	cmpl	$20, %r14d
	jl	.LBB0_630
# BB#324:
	addb	$-11, %cl
	cmpb	$1, %cl
	ja	.LBB0_629
# BB#325:
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpq	(%rcx), %rax
	jne	.LBB0_629
# BB#326:
	leaq	32(%rbp), %r13
	addq	$64, %rbp
	movl	64(%rbx), %eax
	cvtsi2ssl	%eax, %xmm0
	mulss	.LCPI0_0(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movl	48(%rsp), %edi          # 4-byte Reload
	callq	EchoLength
	movq	%rax, %r10
	subq	$8, %rsp
.Lcfi191:
	.cfi_adjust_cfa_offset 8
	movl	$23, %edi
	movl	$3, %esi
	movl	$.L.str.8, %edx
	movl	$2, %ecx
	movb	$1, %al
	movq	%r13, %r8
	movq	%rbp, %r9
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	pushq	%r10
.Lcfi192:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi193:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB0_630
.LBB0_327:
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	movq	%r8, 88(%rsp)           # 8-byte Spill
	jne	.LBB0_329
# BB#328:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movq	%r10, %r13
	movq	%r9, %r10
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	movq	%r10, %r14
	callq	Error
	movl	32(%rsp), %ecx          # 4-byte Reload
	movq	%r13, %r10
	movl	24(%rsp), %r11d         # 4-byte Reload
	movq	88(%rsp), %r8           # 8-byte Reload
	movq	%r14, %r9
.LBB0_329:                              # %.preheader2012
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	je	.LBB0_331
# BB#330:                               # %.lr.ph2466.preheader
	xorl	%r14d, %r14d
	testl	%ecx, %ecx
	jne	.LBB0_378
	jmp	.LBB0_379
.LBB0_331:
	xorl	%r14d, %r14d
	jmp	.LBB0_403
.LBB0_332:
	movq	32(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	jne	.LBB0_334
# BB#333:
	movq	%r8, %r15
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	%r11d, %r14d
	movq	%r9, %rbp
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	movq	%rbp, %rbx
	movl	%r10d, %ebp
	callq	Error
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%ebp, %r10d
	movl	%r14d, %r11d
	movq	%r15, %r8
	movq	%rbx, %r9
.LBB0_334:
	movzwl	44(%rax), %eax
	andl	$64512, %eax            # imm = 0xFC00
	cmpl	$52224, %eax            # imm = 0xCC00
	jne	.LBB0_337
# BB#335:
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpw	$0, 46(%rax)
	jne	.LBB0_337
# BB#336:
	movslq	%r11d, %rax
	movl	%r9d, %ecx
	subl	48(%r12,%rax,4), %ecx
	addl	%ecx, 56(%r13,%rax,4)
	movl	%r9d, 48(%r12,%rax,4)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	movzwl	44(%rcx), %eax
	andl	$1023, %eax             # imm = 0x3FF
	orl	$9216, %eax             # imm = 0x2400
	movw	%ax, 44(%rcx)
.LBB0_337:                              # %.thread1954.preheader
	movq	8(%r12), %rbx
	cmpq	%r12, %rbx
	je	.LBB0_347
.LBB0_338:                              # %.preheader2027
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_339 Depth 2
	movl	%r10d, %r12d
	movl	%r11d, %r14d
	movq	%r8, %rbp
	movq	%rbx, %r13
	.p2align	4, 0x90
.LBB0_339:                              #   Parent Loop BB0_338 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r13), %r13
	movzbl	32(%r13), %eax
	testb	%al, %al
	je	.LBB0_339
# BB#340:                               #   in Loop: Header=BB0_338 Depth=1
	cmpb	$9, %al
	je	.LBB0_343
# BB#341:                               #   in Loop: Header=BB0_338 Depth=1
	cmpb	$1, %al
	jne	.LBB0_344
# BB#342:                               #   in Loop: Header=BB0_338 Depth=1
	movq	%rbp, %r8
	movl	%r14d, %r11d
	movl	%r12d, %r10d
	jmp	.LBB0_345
.LBB0_343:                              #   in Loop: Header=BB0_338 Depth=1
	movq	%r13, %rdi
	movq	%r9, %r15
	callq	SplitIsDefinite
	movq	%r15, %r9
	testl	%eax, %eax
	movq	%rbp, %r8
	movl	%r14d, %r11d
	movl	%r12d, %r10d
	je	.LBB0_345
	jmp	.LBB0_348
.LBB0_344:                              #   in Loop: Header=BB0_338 Depth=1
	addb	$-9, %al
	cmpb	$91, %al
	movq	%rbp, %r8
	movl	%r14d, %r11d
	movl	%r12d, %r10d
	jb	.LBB0_348
.LBB0_345:                              # %.critedge5
                                        #   in Loop: Header=BB0_338 Depth=1
	movq	8(%rbx), %rbx
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	jne	.LBB0_338
# BB#346:
	movq	8(%rsp), %r12           # 8-byte Reload
.LBB0_347:                              # %._crit_edge2553
	movq	%r12, %rbx
	jmp	.LBB0_349
.LBB0_348:
	movq	8(%rsp), %r12           # 8-byte Reload
.LBB0_349:                              # %._crit_edge2553
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movslq	%r11d, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	48(%r12,%rax,4), %ecx
	movl	%r8d, %eax
	subl	%ecx, %eax
	movl	%eax, 136(%rsp)         # 4-byte Spill
	addl	%edx, %ecx
	movl	%ecx, 128(%rsp)         # 4-byte Spill
	xorl	%eax, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	testl	%r10d, %r10d
	movq	%r9, 56(%rsp)           # 8-byte Spill
	jne	.LBB0_588
# BB#350:                               # %._crit_edge2553
	movzwl	42(%r12), %eax
	andl	$2048, %eax             # imm = 0x800
	testw	%ax, %ax
	je	.LBB0_588
# BB#351:
	movq	8(%r12), %rbp
	cmpq	%r12, %rbp
	je	.LBB0_588
# BB#352:                               # %.preheader121.lr.ph.i.preheader
	movl	%r10d, 40(%rsp)         # 4-byte Spill
.LBB0_353:                              # %.preheader121.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_354 Depth 2
	movl	%r11d, %r15d
	movq	%r8, %r14
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB0_354:                              #   Parent Loop BB0_353 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_354
# BB#355:                               #   in Loop: Header=BB0_353 Depth=1
	cmpb	$9, %al
	je	.LBB0_358
# BB#356:                               #   in Loop: Header=BB0_353 Depth=1
	cmpb	$1, %al
	jne	.LBB0_359
# BB#357:                               # %.critedge.outer.i
                                        #   in Loop: Header=BB0_353 Depth=1
	movq	8(%rbp), %rbp
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	%r14, %r8
	movl	%r15d, %r11d
	movl	40(%rsp), %r10d         # 4-byte Reload
	jne	.LBB0_353
	jmp	.LBB0_588
.LBB0_358:                              #   in Loop: Header=BB0_353 Depth=1
	movq	%rbx, %rdi
	callq	SplitIsDefinite
	testl	%eax, %eax
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	%r14, %r8
	movl	%r15d, %r11d
	movl	40(%rsp), %r10d         # 4-byte Reload
	je	.LBB0_360
	jmp	.LBB0_364
.LBB0_359:                              #   in Loop: Header=BB0_353 Depth=1
	addb	$-9, %al
	cmpb	$90, %al
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	%r14, %r8
	movl	%r15d, %r11d
	movl	40(%rsp), %r10d         # 4-byte Reload
	jbe	.LBB0_364
.LBB0_360:                              # %.critedge.backedge.i
                                        #   in Loop: Header=BB0_353 Depth=1
	movq	8(%rbp), %rbp
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	jne	.LBB0_353
	jmp	.LBB0_588
.LBB0_390:                              # %.preheader2009.lr.ph.preheader
	movl	$1, %ecx
	xorl	%ebx, %ebx
	movq	%r15, 136(%rsp)         # 8-byte Spill
	movl	%r14d, 48(%rsp)         # 4-byte Spill
.LBB0_391:                              # %.preheader2009.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_392 Depth 2
                                        #       Child Loop BB0_393 Depth 3
	movq	%rbx, 32(%rsp)          # 8-byte Spill
.LBB0_392:                              # %.preheader2009
                                        #   Parent Loop BB0_391 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_393 Depth 3
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB0_393:                              #   Parent Loop BB0_391 Depth=1
                                        #     Parent Loop BB0_392 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_393
# BB#394:                               #   in Loop: Header=BB0_392 Depth=2
	cmpb	$9, %al
	je	.LBB0_397
# BB#395:                               #   in Loop: Header=BB0_392 Depth=2
	cmpb	$1, %al
	je	.LBB0_399
# BB#396:                               #   in Loop: Header=BB0_392 Depth=2
	addb	$-9, %al
	cmpb	$90, %al
	ja	.LBB0_398
	jmp	.LBB0_374
.LBB0_397:                              #   in Loop: Header=BB0_392 Depth=2
	movq	%rbx, %rdi
	movq	%r8, %r13
	movq	%r10, %r14
	movl	%ecx, %r15d
	callq	SplitIsDefinite
	movl	%r15d, %ecx
	movq	%r14, %r10
	movl	24(%rsp), %r11d         # 4-byte Reload
	movq	136(%rsp), %r15         # 8-byte Reload
	movq	%r13, %r8
	movl	48(%rsp), %r14d         # 4-byte Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	testl	%eax, %eax
	jne	.LBB0_374
.LBB0_398:                              # %.critedge11.backedge
                                        #   in Loop: Header=BB0_392 Depth=2
	movq	8(%r12), %r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	jne	.LBB0_392
	jmp	.LBB0_403
.LBB0_399:                              #   in Loop: Header=BB0_391 Depth=1
	testl	%ecx, %ecx
	je	.LBB0_401
# BB#400:                               #   in Loop: Header=BB0_391 Depth=1
	movb	45(%rbx), %al
	andb	$2, %al
	shrb	%al
	jmp	.LBB0_402
.LBB0_401:                              #   in Loop: Header=BB0_391 Depth=1
	xorl	%eax, %eax
.LBB0_402:                              # %.critedge11.outer
                                        #   in Loop: Header=BB0_391 Depth=1
	movzbl	%al, %ecx
	movq	8(%r12), %r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	jne	.LBB0_391
	jmp	.LBB0_403
.LBB0_374:
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_376
# BB#375:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	xorl	%ecx, %ecx
	movq	%r10, %r14
	movq	%r9, %r13
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	32(%rsp), %ecx          # 4-byte Reload
	movq	%r14, %r10
	movl	48(%rsp), %r14d         # 4-byte Reload
	movl	24(%rsp), %r11d         # 4-byte Reload
	movq	88(%rsp), %r8           # 8-byte Reload
	movq	%r13, %r9
.LBB0_376:                              # %.backedge
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	je	.LBB0_403
# BB#377:                               # %.backedge..lr.ph2466_crit_edge
	movl	112(%rsp), %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	je	.LBB0_379
.LBB0_378:
	movq	72(%rsp), %rdx          # 8-byte Reload
	movl	48(%rbx,%rdx,4), %eax
	movq	96(%rsp), %rcx          # 8-byte Reload
	cmpl	%eax, %ecx
	cmovll	%eax, %ecx
	movl	%ecx, 112(%rsp)
	movl	144(%rsp), %eax
	movl	56(%rbx,%rdx,4), %ecx
	cmpl	%ecx, %eax
	cmovll	%ecx, %eax
	movl	%eax, 144(%rsp)
	cmpl	56(%rbp,%rdx,4), %ecx
	cmovgq	%rbx, %rbp
	jmp	.LBB0_389
.LBB0_379:
	movl	%r14d, 48(%rsp)         # 4-byte Spill
	movq	96(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%r8), %esi
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	subl	%edx, %ecx
	movl	$0, %r9d
	movq	%rbp, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r11d, %r13d
	movl	%r11d, %r8d
	leaq	16(%rsp), %rax
	pushq	%rax
.Lcfi194:
	.cfi_adjust_cfa_offset 8
	leaq	28(%rsp), %rax
	pushq	%rax
.Lcfi195:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi196:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi197:
	.cfi_adjust_cfa_offset 8
	movq	%r10, 64(%rsp)          # 8-byte Spill
	callq	FixAndPrintObject
	movq	64(%rsp), %r14          # 8-byte Reload
	addq	$32, %rsp
.Lcfi198:
	.cfi_adjust_cfa_offset -32
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	48(%rbp,%rax,4), %edx
	movl	%edx, 112(%rsp)
	movl	56(%rbp,%rax,4), %ecx
	movl	%ecx, 144(%rsp)
	cmpq	%r12, %r14
	je	.LBB0_387
# BB#380:                               # %.preheader2010.preheader
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	88(%rsp), %r8           # 8-byte Reload
	movl	%r13d, %r11d
.LBB0_381:                              # %.preheader2010
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_382 Depth 2
	xorl	%r15d, %r15d
	movq	%r14, %rdi
	.p2align	4, 0x90
.LBB0_382:                              #   Parent Loop BB0_381 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdi), %rdi
	movzbl	32(%rdi), %eax
	incl	%r15d
	testb	%al, %al
	je	.LBB0_382
# BB#383:                               #   in Loop: Header=BB0_381 Depth=1
	cmpq	%rbp, %rdi
	je	.LBB0_386
# BB#384:                               #   in Loop: Header=BB0_381 Depth=1
	addb	$-9, %al
	cmpb	$90, %al
	ja	.LBB0_386
# BB#385:                               #   in Loop: Header=BB0_381 Depth=1
	leal	(%rdx,%r8), %esi
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	subl	%edx, %ecx
	movl	$1, %r9d
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r13d, %r8d
	leaq	16(%rsp), %rax
	pushq	%rax
.Lcfi199:
	.cfi_adjust_cfa_offset 8
	leaq	28(%rsp), %rax
	pushq	%rax
.Lcfi200:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi201:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi202:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	movl	%r13d, %r11d
	movq	120(%rsp), %r8          # 8-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	addq	$32, %rsp
.Lcfi203:
	.cfi_adjust_cfa_offset -32
	movl	112(%rsp), %edx
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	48(%rax,%rsi,4), %ecx
	cmpl	%ecx, %edx
	cmovll	%ecx, %edx
	movl	%edx, 112(%rsp)
	movl	144(%rsp), %ecx
	movl	56(%rax,%rsi,4), %eax
	cmpl	%eax, %ecx
	cmovll	%eax, %ecx
	movl	%ecx, 144(%rsp)
.LBB0_386:                              #   in Loop: Header=BB0_381 Depth=1
	movq	8(%r14), %r14
	cmpq	%r12, %r14
	jne	.LBB0_381
	jmp	.LBB0_388
.LBB0_387:
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	88(%rsp), %r8           # 8-byte Reload
	movl	%r13d, %r11d
.LBB0_388:                              # %._crit_edge2439
	addl	%edx, %ecx
	movl	48(%rsp), %r14d         # 4-byte Reload
	cmpl	%ecx, %r14d
	cmovll	%ecx, %r14d
	movq	72(%rsp), %rcx          # 8-byte Reload
	movl	48(%rbx,%rcx,4), %eax
	movl	%eax, 112(%rsp)
	movl	56(%rbx,%rcx,4), %eax
	movl	%eax, 144(%rsp)
	movl	$1, 40(%rsp)            # 4-byte Folded Spill
	movq	%r12, %r10
	movq	%rbx, %rbp
.LBB0_389:
	movq	8(%r12), %r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	jne	.LBB0_390
# BB#632:
	movl	$1, %ecx
.LBB0_403:                              # %._crit_edge2467
	testq	%r10, %r10
	movq	288(%rsp), %rbx
	movq	%rbx, %rsi
	jne	.LBB0_405
# BB#404:
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	movl	%ecx, %r13d
	xorl	%ecx, %ecx
	movq	%r9, %rbx
	movl	$.L.str.7, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	%r13d, %ecx
	movq	288(%rsp), %rsi
	xorl	%r10d, %r10d
	movq	88(%rsp), %r8           # 8-byte Reload
	movq	%rbx, %r9
.LBB0_405:                              # %._crit_edge2467.thread
	movl	272(%rsp), %ebx
.LBB0_406:                              # %._crit_edge2467.thread
	movq	%r10, 32(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	je	.LBB0_415
# BB#407:                               # %._crit_edge2467.thread
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jne	.LBB0_415
# BB#408:
	leaq	144(%rsp), %rax
	leaq	112(%rsp), %r10
	movq	%r9, %rdx
	movl	$0, %r9d
	movq	%rbp, %rdi
	movq	%r8, %r13
	movl	%r8d, %esi
	movq	%rdx, %r12
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	64(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	24(%rsp), %r8d          # 4-byte Reload
	pushq	%rax
.Lcfi204:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi205:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi206:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %r15
	pushq	%rbx
.Lcfi207:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi208:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r14
	movq	32(%rsp), %rbp          # 8-byte Reload
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	je	.LBB0_426
.LBB0_410:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_411 Depth 2
	xorl	%eax, %eax
	movq	%rbp, %rdi
	.p2align	4, 0x90
.LBB0_411:                              #   Parent Loop BB0_410 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdi), %rdi
	movzbl	32(%rdi), %ecx
	incl	%eax
	testb	%cl, %cl
	je	.LBB0_411
# BB#412:                               #   in Loop: Header=BB0_410 Depth=1
	cmpq	%r14, %rdi
	je	.LBB0_409
# BB#413:                               #   in Loop: Header=BB0_410 Depth=1
	addb	$-9, %cl
	cmpb	$90, %cl
	ja	.LBB0_409
# BB#414:                               #   in Loop: Header=BB0_410 Depth=1
	movl	$1, %r9d
	movl	%r13d, %esi
	movl	%r12d, %edx
	movq	64(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	24(%rsp), %r8d          # 4-byte Reload
	leaq	16(%rsp), %rbx
	pushq	%rbx
.Lcfi209:
	.cfi_adjust_cfa_offset 8
	leaq	28(%rsp), %rbx
	pushq	%rbx
.Lcfi210:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi211:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi212:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi213:
	.cfi_adjust_cfa_offset -32
	movl	112(%rsp), %eax
	movl	20(%rsp), %ecx
	cmpl	%ecx, %eax
	cmovll	%ecx, %eax
	movl	%eax, 112(%rsp)
	movl	144(%rsp), %eax
	movl	16(%rsp), %ecx
	cmpl	%ecx, %eax
	cmovll	%ecx, %eax
	movl	%eax, 144(%rsp)
.LBB0_409:                              #   in Loop: Header=BB0_410 Depth=1
	movq	8(%rbp), %rbp
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	jne	.LBB0_410
.LBB0_426:                              # %._crit_edge2435
	movl	112(%rsp), %eax
	movq	288(%rsp), %rcx
	movl	%eax, (%rcx)
	movl	144(%rsp), %ecx
	movq	296(%rsp), %rax
	jmp	.LBB0_427
.LBB0_415:
	movq	%rsi, %r13
	movl	%r14d, 48(%rsp)         # 4-byte Spill
	movl	112(%rsp), %edx
	movq	%r8, %r12
	leal	(%rdx,%r8), %esi
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	subl	%edx, %ecx
	leaq	16(%rsp), %rax
	leaq	20(%rsp), %r10
	movl	$0, %r9d
	movq	%rbp, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	24(%rsp), %r8d          # 4-byte Reload
	pushq	%rax
.Lcfi214:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi215:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi216:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi217:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi218:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r14
	movq	72(%rsp), %r15          # 8-byte Reload
	movl	48(%r14,%r15,4), %edx
	movl	%edx, 112(%rsp)
	movl	56(%r14,%r15,4), %ecx
	movl	%ecx, 144(%rsp)
	movq	32(%rsp), %rbp          # 8-byte Reload
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	je	.LBB0_425
# BB#416:                               # %.preheader2008.preheader
	movq	%r12, %rsi
.LBB0_417:                              # %.preheader2008
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_418 Depth 2
	movq	%rbx, %r10
	xorl	%eax, %eax
	movq	%rbp, %rdi
	.p2align	4, 0x90
.LBB0_418:                              #   Parent Loop BB0_417 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdi), %rdi
	movzbl	32(%rdi), %ebx
	incl	%eax
	testb	%bl, %bl
	je	.LBB0_418
# BB#419:                               #   in Loop: Header=BB0_417 Depth=1
	cmpq	%r14, %rdi
	je	.LBB0_423
# BB#420:                               #   in Loop: Header=BB0_417 Depth=1
	addb	$-9, %bl
	cmpb	$90, %bl
	ja	.LBB0_423
# BB#421:                               #   in Loop: Header=BB0_417 Depth=1
	leal	(%rdx,%rsi), %esi
	movq	64(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%edx, %ecx
	movl	$1, %r9d
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	24(%rsp), %r8d          # 4-byte Reload
	leaq	16(%rsp), %rbx
	pushq	%rbx
.Lcfi219:
	.cfi_adjust_cfa_offset 8
	leaq	28(%rsp), %rbx
	pushq	%rbx
.Lcfi220:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi221:
	.cfi_adjust_cfa_offset 8
	movq	%r10, %rbx
	pushq	%rbx
.Lcfi222:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	movq	%r12, %rsi
	addq	$32, %rsp
.Lcfi223:
	.cfi_adjust_cfa_offset -32
	movl	112(%rsp), %edx
	movl	48(%rax,%r15,4), %ecx
	cmpl	%ecx, %edx
	cmovll	%ecx, %edx
	movl	%edx, 112(%rsp)
	movl	144(%rsp), %ecx
	movl	56(%rax,%r15,4), %eax
	cmpl	%eax, %ecx
	cmovll	%eax, %ecx
	movl	%ecx, 144(%rsp)
	jmp	.LBB0_424
.LBB0_423:                              #   in Loop: Header=BB0_417 Depth=1
	movq	%r10, %rbx
.LBB0_424:                              #   in Loop: Header=BB0_417 Depth=1
	movq	8(%rbp), %rbp
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	jne	.LBB0_417
.LBB0_425:                              # %._crit_edge2437
	addl	%edx, %ecx
	movl	48(%rsp), %eax          # 4-byte Reload
	cmpl	%ecx, %eax
	cmovgel	%eax, %ecx
	movl	$0, (%r13)
	movq	296(%rsp), %r12
	movq	%r12, %rax
.LBB0_427:                              # %.sink.split
	movl	%ecx, (%rax)
	movq	8(%rsp), %r12           # 8-byte Reload
	jmp	.LBB0_282
.LBB0_361:
	movl	%edx, %esi
	subl	%ecx, %esi
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rax, 128(%rsp)         # 8-byte Spill
	jge	.LBB0_450
# BB#362:
	testl	%eax, %eax
	jg	.LBB0_453
# BB#363:
	movl	%ecx, %r13d
	movq	%r15, %r12
	movq	%r8, %r15
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movq	%r9, %r10
	movl	$.L.str.15, %r9d
	xorl	%eax, %eax
	movq	%r10, %r14
	callq	Error
	movq	%r15, %r8
	movq	%r12, %r15
	movl	%r13d, %ecx
	movl	48(%rsp), %edx          # 4-byte Reload
	movq	%r14, %r9
	jmp	.LBB0_453
.LBB0_364:
	movq	8(%rbp), %r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	je	.LBB0_588
# BB#365:                               # %.preheader119.lr.ph.i.preheader
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	48(%rbx,%rax,4), %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
.LBB0_366:                              # %.preheader119.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_367 Depth 2
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB0_367:                              #   Parent Loop BB0_366 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB0_367
# BB#368:                               #   in Loop: Header=BB0_366 Depth=1
	cmpb	$9, %al
	je	.LBB0_371
# BB#369:                               #   in Loop: Header=BB0_366 Depth=1
	cmpb	$1, %al
	jne	.LBB0_372
# BB#370:                               # %.critedge1.outer.i
                                        #   in Loop: Header=BB0_366 Depth=1
	movq	8(%r12), %r12
	xorl	%eax, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	jne	.LBB0_366
	jmp	.LBB0_588
.LBB0_371:                              #   in Loop: Header=BB0_366 Depth=1
	movq	%rbp, %rdi
	callq	SplitIsDefinite
	movl	40(%rsp), %r10d         # 4-byte Reload
	movl	%r15d, %r11d
	movq	%r14, %r8
	movq	56(%rsp), %r9           # 8-byte Reload
	testl	%eax, %eax
	je	.LBB0_373
	jmp	.LBB0_566
.LBB0_372:                              #   in Loop: Header=BB0_366 Depth=1
	addb	$-9, %al
	cmpb	$90, %al
	jbe	.LBB0_566
.LBB0_373:                              # %.critedge1.backedge.i
                                        #   in Loop: Header=BB0_366 Depth=1
	movq	8(%r12), %r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	jne	.LBB0_366
	jmp	.LBB0_586
.LBB0_428:
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_430
# BB#429:
	movq	%r8, %r15
	movq	no_fpos(%rip), %r8
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movq	%r9, %r10
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	movq	%r10, %r14
	callq	Error
	movq	%r15, %r8
	movl	280(%rsp), %r15d
	movq	%r14, %r9
.LBB0_430:                              # %.preheader2044
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	movl	56(%rbx), %edi
	je	.LBB0_314
# BB#431:                               # %.lr.ph2691.preheader
	xorl	%eax, %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	$0, 84(%rsp)            # 4-byte Folded Spill
	movq	%rbp, %rcx
	movq	%r9, 56(%rsp)           # 8-byte Spill
.LBB0_432:                              # %.lr.ph2691
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_439 Depth 2
                                        #       Child Loop BB0_440 Depth 3
	movq	%r8, %r14
	movl	48(%rcx), %esi
	movq	%rcx, %rbx
	movl	56(%rcx), %edx
	movq	40(%rsp), %rbp          # 8-byte Reload
	leaq	44(%rbp), %rcx
	movl	72(%rsp), %r9d          # 4-byte Reload
	subl	136(%rsp), %r9d         # 4-byte Folded Reload
	movl	48(%rsp), %r8d          # 4-byte Reload
	callq	ActualGap
	movw	%ax, 54(%rbp)
	cwtl
	movzwl	44(%rbp), %ecx
	movl	%ecx, %edx
	andl	$57344, %edx            # imm = 0xE000
	cmpl	$49152, %edx            # imm = 0xC000
	je	.LBB0_435
# BB#433:                               # %.lr.ph2691
                                        #   in Loop: Header=BB0_432 Depth=1
	andl	$6144, %ecx             # imm = 0x1800
	movzwl	%cx, %ecx
	cmpl	$2048, %ecx             # imm = 0x800
	movl	$0, %ecx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	je	.LBB0_437
# BB#434:                               #   in Loop: Header=BB0_432 Depth=1
	xorl	%ecx, %ecx
	cmpw	$0, 46(%rbp)
	setg	%cl
	addl	84(%rsp), %ecx          # 4-byte Folded Reload
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movq	104(%rsp), %rcx         # 8-byte Reload
	jmp	.LBB0_436
.LBB0_435:                              #   in Loop: Header=BB0_432 Depth=1
	xorl	%ecx, %ecx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movq	24(%rsp), %rcx          # 8-byte Reload
.LBB0_436:                              #   in Loop: Header=BB0_432 Depth=1
	movq	%rcx, 96(%rsp)          # 8-byte Spill
.LBB0_437:                              #   in Loop: Header=BB0_432 Depth=1
	addl	%eax, 72(%rsp)          # 4-byte Folded Spill
	movq	8(%r12), %r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	%r14, %r8
	je	.LBB0_633
# BB#438:                               # %.preheader2041.lr.ph.preheader
                                        #   in Loop: Header=BB0_432 Depth=1
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rbx, %rcx
.LBB0_439:                              # %.preheader2041
                                        #   Parent Loop BB0_432 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_440 Depth 3
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB0_440:                              #   Parent Loop BB0_432 Depth=1
                                        #     Parent Loop BB0_439 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB0_440
# BB#441:                               #   in Loop: Header=BB0_439 Depth=2
	cmpb	$9, %al
	je	.LBB0_444
# BB#442:                               #   in Loop: Header=BB0_439 Depth=2
	cmpb	$1, %al
	jne	.LBB0_445
# BB#443:                               # %.critedge16.outer
                                        #   in Loop: Header=BB0_439 Depth=2
	movq	8(%r12), %r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	movq	%rbp, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rax, 40(%rsp)          # 8-byte Spill
	jne	.LBB0_439
	jmp	.LBB0_634
.LBB0_444:                              #   in Loop: Header=BB0_439 Depth=2
	movq	%rbp, %rdi
	callq	SplitIsDefinite
	movq	%rbx, %rcx
	movq	%r14, %r8
	movq	56(%rsp), %r9           # 8-byte Reload
	testl	%eax, %eax
	je	.LBB0_446
	jmp	.LBB0_447
.LBB0_445:                              #   in Loop: Header=BB0_439 Depth=2
	addb	$-9, %al
	cmpb	$90, %al
	jbe	.LBB0_447
.LBB0_446:                              # %.critedge16.backedge
                                        #   in Loop: Header=BB0_439 Depth=2
	movq	8(%r12), %r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	jne	.LBB0_439
	jmp	.LBB0_634
.LBB0_447:                              #   in Loop: Header=BB0_432 Depth=1
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_449
# BB#448:                               #   in Loop: Header=BB0_432 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	%rbx, %rcx
	movq	%r14, %r8
	movq	56(%rsp), %r9           # 8-byte Reload
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
.LBB0_449:                              # %.backedge2045
                                        #   in Loop: Header=BB0_432 Depth=1
	movl	56(%rcx), %edi
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	128(%rsp), %rax         # 8-byte Reload
	movl	%eax, %ecx
	movl	%ecx, 84(%rsp)          # 4-byte Spill
	movq	%rbp, %rcx
	jne	.LBB0_432
	jmp	.LBB0_315
.LBB0_450:
	movb	68(%rbx), %al
	shrb	$4, %al
	andb	$7, %al
	cmpb	$5, %al
	je	.LBB0_626
# BB#451:
	cmpb	$6, %al
	je	.LBB0_627
# BB#452:
	cmpb	$7, %al
	jne	.LBB0_628
.LBB0_453:
	movzwl	42(%rbx), %eax
	orl	$2048, %eax             # imm = 0x800
	movw	%ax, 42(%rbx)
.LBB0_454:
	xorl	%esi, %esi
.LBB0_455:
	movl	$0, 136(%rsp)           # 4-byte Folded Spill
	cmpl	$0, 128(%rsp)           # 4-byte Folded Reload
	movl	%esi, 32(%rsp)          # 4-byte Spill
	jle	.LBB0_458
# BB#456:
	andl	$2048, %eax             # imm = 0x800
	testw	%ax, %ax
                                        # implicit-def: %ESI
	je	.LBB0_459
# BB#457:
	movl	%edx, %eax
	subl	%ecx, %eax
	cltd
	idivl	128(%rsp)               # 4-byte Folded Reload
	xorl	%esi, %esi
	testl	%eax, %eax
	cmovnsl	%eax, %esi
	movb	$1, %al
	movl	%eax, 136(%rsp)         # 4-byte Spill
	jmp	.LBB0_459
.LBB0_458:
                                        # implicit-def: %ESI
.LBB0_459:
	movl	%esi, 168(%rsp)         # 4-byte Spill
	movl	%ecx, 72(%rsp)          # 4-byte Spill
	cmpq	$0, 96(%rsp)            # 8-byte Folded Reload
	sete	%r12b
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	8(%r13), %rbx
	cmpq	%r13, %rbx
	je	.LBB0_469
.LBB0_460:                              # %.preheader2040
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_461 Depth 2
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB0_461:                              #   Parent Loop BB0_460 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB0_461
# BB#462:                               #   in Loop: Header=BB0_460 Depth=1
	cmpb	$1, %al
	je	.LBB0_466
# BB#463:                               #   in Loop: Header=BB0_460 Depth=1
	cmpb	$9, %al
	jne	.LBB0_465
# BB#464:                               #   in Loop: Header=BB0_460 Depth=1
	movq	%rbp, %rdi
	movq	%r9, %r14
	movq	%r15, %r13
	movq	%r8, %r15
	callq	SplitIsDefinite
	movq	%r15, %r8
	movq	%r13, %r15
	movq	%r14, %r9
	testl	%eax, %eax
	je	.LBB0_466
	jmp	.LBB0_468
.LBB0_465:                              #   in Loop: Header=BB0_460 Depth=1
	addb	$-9, %al
	cmpb	$91, %al
	jb	.LBB0_468
.LBB0_466:                              # %.critedge23
                                        #   in Loop: Header=BB0_460 Depth=1
	movq	8(%rbx), %rbx
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	jne	.LBB0_460
# BB#467:
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB0_468:                              # %._crit_edge2654.loopexit
	movq	8(%rbx), %r13
.LBB0_469:                              # %._crit_edge2654
	movl	32(%rsp), %ecx          # 4-byte Reload
	addl	%r8d, %ecx
	movq	8(%rsp), %rax           # 8-byte Reload
	subl	48(%rax), %ecx
	addl	48(%rbp), %ecx
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	cmpq	%rax, %r13
	je	.LBB0_479
# BB#470:                               # %.preheader2038.lr.ph.preheader
	andb	136(%rsp), %r12b        # 1-byte Folded Reload
	movzbl	%r12b, %eax
	movl	%eax, 160(%rsp)         # 4-byte Spill
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
.LBB0_471:                              # %.preheader2038
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_472 Depth 2
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB0_472:                              #   Parent Loop BB0_471 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_472
# BB#473:                               #   in Loop: Header=BB0_471 Depth=1
	cmpb	$9, %al
	je	.LBB0_476
# BB#474:                               #   in Loop: Header=BB0_471 Depth=1
	cmpb	$1, %al
	jne	.LBB0_477
# BB#475:                               # %.critedge24.outer
                                        #   in Loop: Header=BB0_471 Depth=1
	movq	8(%r13), %r13
	cmpq	8(%rsp), %r13           # 8-byte Folded Reload
	movq	%rbx, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	jne	.LBB0_471
	jmp	.LBB0_479
.LBB0_476:                              #   in Loop: Header=BB0_471 Depth=1
	movq	%rbx, %rdi
	movq	%r9, %r14
	movq	%r15, %r12
	movq	%r8, %r15
	callq	SplitIsDefinite
	movq	%r15, %r8
	movq	%r12, %r15
	movq	%r14, %r9
	testl	%eax, %eax
	je	.LBB0_478
	jmp	.LBB0_488
.LBB0_477:                              #   in Loop: Header=BB0_471 Depth=1
	addb	$-9, %al
	cmpb	$90, %al
	jbe	.LBB0_488
.LBB0_478:                              # %.critedge24.backedge
                                        #   in Loop: Header=BB0_471 Depth=1
	movq	8(%r13), %r13
	cmpq	8(%rsp), %r13           # 8-byte Folded Reload
	jne	.LBB0_471
.LBB0_479:                              # %.preheader2036.thread
	leaq	40(%rbp), %rsi
	movl	40(%rbp), %ecx
	movl	%ecx, %eax
	shrl	$29, %eax
	andl	$3, %eax
.LBB0_480:
	movl	$0, 84(%rsp)            # 4-byte Folded Spill
                                        # implicit-def: %EDX
	movl	%edx, 104(%rsp)         # 4-byte Spill
                                        # implicit-def: %EDX
	movl	%edx, 72(%rsp)          # 4-byte Spill
                                        # implicit-def: %R14D
.LBB0_481:                              # %._crit_edge2620
	movq	%r9, %r12
	decl	%eax
	cmpl	$2, %eax
	jb	.LBB0_483
# BB#482:
	movq	%r8, %r13
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movq	%rsi, %rbx
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.16, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	%rbx, %rsi
	movq	%r13, %r8
	movl	(%rsi), %ecx
.LBB0_483:
	andl	$1610612736, %ecx       # imm = 0x60000000
	cmpl	$1073741824, %ecx       # imm = 0x40000000
	movl	32(%rsp), %r13d         # 4-byte Reload
	jne	.LBB0_565
# BB#484:
	cmpl	$0, 84(%rsp)            # 4-byte Folded Reload
	jne	.LBB0_486
# BB#485:
	movb	32(%rbp), %al
	addb	$-11, %al
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	76(%rcx), %rcx
	cmpb	$2, %al
	cmovbq	%rsi, %rcx
	movl	(%rcx), %r14d
	movl	%r14d, %eax
	andl	$4095, %eax             # imm = 0xFFF
	movl	%eax, 72(%rsp)          # 4-byte Spill
	shrl	$12, %r14d
	andl	$1023, %r14d            # imm = 0x3FF
	movl	%r13d, %eax
	subl	48(%rbp), %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
.LBB0_486:
	movzbl	zz_lengths+3(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB0_555
# BB#487:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_556
.LBB0_488:
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	movl	48(%rsp), %r12d         # 4-byte Reload
	jne	.LBB0_490
# BB#489:
	movq	%r8, %r15
	movq	no_fpos(%rip), %r8
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movq	%r9, %r10
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	movq	%r10, %r14
	callq	Error
	movq	%r15, %r8
	movl	280(%rsp), %r15d
	movq	%r14, %r9
.LBB0_490:                              # %.preheader2036
	leaq	40(%rbp), %rsi
	movl	40(%rbp), %ecx
	movl	%ecx, %eax
	shrl	$29, %eax
	andl	$3, %eax
	movq	8(%rsp), %rdx           # 8-byte Reload
	cmpq	%rdx, %r13
	je	.LBB0_480
# BB#491:                               # %.lr.ph2619
	leaq	76(%rdx), %rcx
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	subl	72(%rsp), %r12d         # 4-byte Folded Reload
	movl	%r12d, 188(%rsp)        # 4-byte Spill
                                        # implicit-def: %R14D
                                        # implicit-def: %ECX
	movl	%ecx, 72(%rsp)          # 4-byte Spill
                                        # implicit-def: %ECX
	movl	%ecx, 104(%rsp)         # 4-byte Spill
	movl	$0, 84(%rsp)            # 4-byte Folded Spill
	movl	$0, 184(%rsp)           # 4-byte Folded Spill
	movl	$0, 180(%rsp)           # 4-byte Folded Spill
	movq	%rbp, %rdi
	movq	%r9, 56(%rsp)           # 8-byte Spill
.LBB0_492:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_517 Depth 2
                                        #       Child Loop BB0_518 Depth 3
	movq	%rbx, %rbp
	cmpl	$2, %eax
	jne	.LBB0_498
# BB#493:                               #   in Loop: Header=BB0_492 Depth=1
	cmpl	$0, 84(%rsp)            # 4-byte Folded Reload
	jne	.LBB0_495
# BB#494:                               #   in Loop: Header=BB0_492 Depth=1
	movb	32(%rdi), %al
	addb	$-11, %al
	cmpb	$2, %al
	cmovaeq	208(%rsp), %rsi         # 8-byte Folded Reload
	movl	(%rsi), %r14d
	movl	%r14d, %eax
	andl	$4095, %eax             # imm = 0xFFF
	movl	%eax, 72(%rsp)          # 4-byte Spill
	shrl	$12, %r14d
	andl	$1023, %r14d            # imm = 0x3FF
	movl	32(%rsp), %eax          # 4-byte Reload
	subl	48(%rdi), %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movl	$1, 84(%rsp)            # 4-byte Folded Spill
.LBB0_495:                              #   in Loop: Header=BB0_492 Depth=1
	movl	%r14d, %r12d
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	40(%rax), %eax
	movl	$1610612736, %ecx       # imm = 0x60000000
	andl	%ecx, %eax
	cmpl	$536870912, %eax        # imm = 0x20000000
	jne	.LBB0_499
# BB#496:                               #   in Loop: Header=BB0_492 Depth=1
	movq	%rdi, %r14
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movzbl	zz_lengths+3(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB0_500
# BB#497:                               #   in Loop: Header=BB0_492 Depth=1
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_501
.LBB0_498:                              #   in Loop: Header=BB0_492 Depth=1
	movl	%r14d, %r12d
.LBB0_499:                              #   in Loop: Header=BB0_492 Depth=1
	movl	272(%rsp), %eax
	movq	%rax, %r10
	jmp	.LBB0_511
.LBB0_500:                              #   in Loop: Header=BB0_492 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB0_501:                              #   in Loop: Header=BB0_492 Depth=1
	movb	$3, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movl	104(%rsp), %eax         # 4-byte Reload
	movl	%eax, 48(%rbx)
	movl	56(%r14), %eax
	addl	32(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 56(%rbx)
	movl	72(%rsp), %eax          # 4-byte Reload
	movl	%eax, 52(%rbx)
	movl	%r12d, 60(%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_503
# BB#502:                               #   in Loop: Header=BB0_492 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_504
.LBB0_503:                              #   in Loop: Header=BB0_492 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_504:                              #   in Loop: Header=BB0_492 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	24(%r14), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_507
# BB#505:                               #   in Loop: Header=BB0_492 Depth=1
	testq	%rcx, %rcx
	je	.LBB0_507
# BB#506:                               #   in Loop: Header=BB0_492 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_507:                              #   in Loop: Header=BB0_492 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	movl	$0, 84(%rsp)            # 4-byte Folded Spill
	testq	%rbx, %rbx
	je	.LBB0_510
# BB#508:                               #   in Loop: Header=BB0_492 Depth=1
	testq	%rax, %rax
	je	.LBB0_510
# BB#509:                               #   in Loop: Header=BB0_492 Depth=1
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_510:                              #   in Loop: Header=BB0_492 Depth=1
	movl	272(%rsp), %eax
	movq	%rax, %r10
	movq	88(%rsp), %r8           # 8-byte Reload
	movq	%r14, %rdi
.LBB0_511:                              #   in Loop: Header=BB0_492 Depth=1
	movq	%r8, %r14
	cmpl	$0, 160(%rsp)           # 4-byte Folded Reload
	je	.LBB0_514
# BB#512:                               #   in Loop: Header=BB0_492 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	cmpw	$0, 46(%rcx)
	jle	.LBB0_514
# BB#513:                               #   in Loop: Header=BB0_492 Depth=1
	movl	48(%rdi), %edx
	movl	56(%rdi), %ecx
	addl	168(%rsp), %ecx         # 4-byte Folded Reload
	movl	$0, %r8d
	movl	$0, %r9d
	movl	32(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %esi
	leaq	16(%rsp), %rax
	pushq	%rax
.Lcfi224:
	.cfi_adjust_cfa_offset 8
	leaq	28(%rsp), %rax
	pushq	%rax
.Lcfi225:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi226:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi227:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi228:
	.cfi_adjust_cfa_offset -32
	movl	184(%rsp), %eax         # 4-byte Reload
	incl	%eax
	movl	%eax, 184(%rsp)         # 4-byte Spill
	imull	188(%rsp), %eax         # 4-byte Folded Reload
	cltd
	idivl	128(%rsp)               # 4-byte Folded Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movswl	54(%rcx), %edx
	movl	%eax, %ecx
	subl	180(%rsp), %ecx         # 4-byte Folded Reload
	addl	%edx, %ecx
	movl	%eax, 180(%rsp)         # 4-byte Spill
	jmp	.LBB0_515
.LBB0_514:                              #   in Loop: Header=BB0_492 Depth=1
	movl	48(%rdi), %edx
	movl	56(%rdi), %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movl	32(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %esi
	leaq	16(%rsp), %rax
	pushq	%rax
.Lcfi229:
	.cfi_adjust_cfa_offset 8
	leaq	28(%rsp), %rax
	pushq	%rax
.Lcfi230:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi231:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi232:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi233:
	.cfi_adjust_cfa_offset -32
	movq	40(%rsp), %rax          # 8-byte Reload
	movswl	54(%rax), %ecx
.LBB0_515:                              #   in Loop: Header=BB0_492 Depth=1
	addl	%ecx, %ebx
	movl	%ebx, 32(%rsp)          # 4-byte Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpq	96(%rsp), %rax          # 8-byte Folded Reload
	movl	160(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, %eax
	movl	$1, %edx
	cmovel	%edx, %eax
	testl	%ecx, %ecx
	cmovnel	%ecx, %eax
	cmpb	$0, 136(%rsp)           # 1-byte Folded Reload
	cmovnel	%eax, %ecx
	movl	%ecx, 160(%rsp)         # 4-byte Spill
	movq	8(%r13), %r13
	cmpq	8(%rsp), %r13           # 8-byte Folded Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	%r14, %r8
	je	.LBB0_625
# BB#516:                               # %.preheader2033.lr.ph.preheader
                                        #   in Loop: Header=BB0_492 Depth=1
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
.LBB0_517:                              # %.preheader2033
                                        #   Parent Loop BB0_492 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_518 Depth 3
	movq	%r13, %rbx
.LBB0_518:                              #   Parent Loop BB0_492 Depth=1
                                        #     Parent Loop BB0_517 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_518
# BB#519:                               #   in Loop: Header=BB0_517 Depth=2
	cmpb	$9, %al
	je	.LBB0_522
# BB#520:                               #   in Loop: Header=BB0_517 Depth=2
	cmpb	$1, %al
	jne	.LBB0_523
# BB#521:                               # %.critedge30.outer
                                        #   in Loop: Header=BB0_517 Depth=2
	movq	8(%r13), %r13
	cmpq	8(%rsp), %r13           # 8-byte Folded Reload
	movq	%rbx, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	jne	.LBB0_517
	jmp	.LBB0_625
.LBB0_522:                              #   in Loop: Header=BB0_517 Depth=2
	movq	%rbx, %rdi
	callq	SplitIsDefinite
	movq	%r14, %r8
	movq	56(%rsp), %r9           # 8-byte Reload
	testl	%eax, %eax
	je	.LBB0_524
	jmp	.LBB0_525
.LBB0_523:                              #   in Loop: Header=BB0_517 Depth=2
	addb	$-9, %al
	cmpb	$90, %al
	jbe	.LBB0_525
.LBB0_524:                              # %.critedge30.backedge
                                        #   in Loop: Header=BB0_517 Depth=2
	movq	8(%r13), %r13
	cmpq	8(%rsp), %r13           # 8-byte Folded Reload
	jne	.LBB0_517
	jmp	.LBB0_625
.LBB0_525:                              #   in Loop: Header=BB0_492 Depth=1
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_527
# BB#526:                               #   in Loop: Header=BB0_492 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	%r14, %r8
	movq	56(%rsp), %r9           # 8-byte Reload
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
.LBB0_527:                              # %.backedge2037
                                        #   in Loop: Header=BB0_492 Depth=1
	leaq	40(%rbp), %rsi
	movl	40(%rbp), %ecx
	movl	%ecx, %eax
	shrl	$29, %eax
	andl	$3, %eax
	cmpq	8(%rsp), %r13           # 8-byte Folded Reload
	movq	%rbp, %rdi
	movl	%r12d, %r14d
	jne	.LBB0_492
	jmp	.LBB0_481
.LBB0_528:                              # %._crit_edge3031
	movl	56(%rbx), %ecx
	movq	%r12, %r9
	movq	%rbx, %rbp
.LBB0_529:
	addl	48(%rbp), %ecx
	jg	.LBB0_531
# BB#530:
	movl	$23, %edi
	movl	$5, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r13, %r8
	movq	%r9, %rbx
	callq	Error
	movq	%rbx, %r9
.LBB0_531:
	movq	8(%rbp), %rax
	movq	%rax, %rbx
.LBB0_532:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %ecx
	testb	%cl, %cl
	je	.LBB0_532
# BB#533:
	movq	%r9, %r12
	addb	$-11, %cl
	cmpb	$1, %cl
	ja	.LBB0_536
# BB#534:
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpq	(%rcx), %rax
	jne	.LBB0_536
# BB#535:
	leaq	32(%rbx), %r14
	addq	$64, %rbx
	movl	48(%rsp), %edi          # 4-byte Reload
	callq	EchoLength
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi234:
	.cfi_adjust_cfa_offset 8
	movl	$23, %edi
	movl	$6, %esi
	movl	$.L.str.11, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbx, %r9
	pushq	%rbp
.Lcfi235:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi236:
	.cfi_adjust_cfa_offset -16
	movq	8(%rsp), %rbx           # 8-byte Reload
	jmp	.LBB0_537
.LBB0_536:
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	56(%rbx), %edi
	addl	48(%rbx), %edi
	callq	EchoLength
	movq	%rax, %r14
	movl	48(%rsp), %edi          # 4-byte Reload
	callq	EchoLength
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi237:
	.cfi_adjust_cfa_offset 8
	movl	$23, %edi
	movl	$7, %esi
	movl	$.L.str.12, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r13, %r8
	movq	%r14, %r9
	pushq	%rbp
.Lcfi238:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi239:
	.cfi_adjust_cfa_offset -16
.LBB0_537:                              # %.preheader2031
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	movq	64(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_546
.LBB0_538:                              # %.lr.ph2582
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_540
# BB#539:                               #   in Loop: Header=BB0_538 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB0_541
.LBB0_540:                              #   in Loop: Header=BB0_538 Depth=1
	xorl	%ecx, %ecx
.LBB0_541:                              #   in Loop: Header=BB0_538 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_543
# BB#542:                               #   in Loop: Header=BB0_538 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_543:                              #   in Loop: Header=BB0_538 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB0_545
# BB#544:                               #   in Loop: Header=BB0_538 Depth=1
	callq	DisposeObject
.LBB0_545:                              # %.backedge2032
                                        #   in Loop: Header=BB0_538 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	8(%rcx), %rax
	cmpq	%rcx, %rax
	jne	.LBB0_538
.LBB0_546:                              # %._crit_edge2583
	movl	$11, %edi
	movl	$.L.str.13, %esi
	movq	%r13, %rdx
	callq	MakeWord
	movq	%rax, %rbx
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_548
# BB#547:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_549
.LBB0_548:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_549:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rdx, zz_hold(%rip)
	testq	%rax, %rax
	movq	%r12, %r9
	je	.LBB0_551
# BB#550:
	movq	(%rdx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_551:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB0_554
# BB#552:
	testq	%rax, %rax
	je	.LBB0_554
# BB#553:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_554:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
	jmp	.LBB0_122
.LBB0_555:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r8, %rbx
	callq	GetMemory
	movq	%rbx, %r8
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB0_556:
	movb	$3, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movl	104(%rsp), %eax         # 4-byte Reload
	movl	%eax, 48(%rbx)
	movl	56(%rbp), %eax
	addl	%r13d, %eax
	movl	%eax, 56(%rbx)
	movl	72(%rsp), %eax          # 4-byte Reload
	movl	%eax, 52(%rbx)
	movl	%r14d, 60(%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_558
# BB#557:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_559
.LBB0_558:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r8, %r14
	callq	GetMemory
	movq	%r14, %r8
	movq	%rax, zz_hold(%rip)
.LBB0_559:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	24(%rbp), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_562
# BB#560:
	testq	%rcx, %rcx
	je	.LBB0_562
# BB#561:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_562:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB0_565
# BB#563:
	testq	%rax, %rax
	je	.LBB0_565
# BB#564:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_565:
	movl	48(%rbp), %edx
	movl	48(%rsp), %ecx          # 4-byte Reload
	addl	%r8d, %ecx
	subl	%r13d, %ecx
	movq	8(%rsp), %rax           # 8-byte Reload
	subl	48(%rax), %ecx
	leaq	16(%rsp), %rax
	leaq	20(%rsp), %rbx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbp, %rdi
	movl	%r13d, %esi
	pushq	%rax
.Lcfi240:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi241:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi242:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi243:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi244:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB0_631
.LBB0_566:
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_568
# BB#567:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movq	%r12, 168(%rsp)         # 8-byte Spill
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	168(%rsp), %r12         # 8-byte Reload
	movl	40(%rsp), %r10d         # 4-byte Reload
	movl	%r15d, %r11d
	movq	%r14, %r8
	movq	56(%rsp), %r9           # 8-byte Reload
.LBB0_568:                              # %.preheader118.i
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	je	.LBB0_586
# BB#569:                               # %.lr.ph.i.preheader
	xorl	%eax, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	$0, 84(%rsp)            # 4-byte Folded Spill
.LBB0_570:                              # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_574 Depth 2
                                        #       Child Loop BB0_575 Depth 3
	movq	48(%rsp), %rax          # 8-byte Reload
	movzwl	44(%rax), %eax
	movl	%eax, %ecx
	andl	$57344, %ecx            # imm = 0xE000
	cmpl	$49152, %ecx            # imm = 0xC000
	je	.LBB0_588
# BB#571:                               # %.lr.ph.i
                                        #   in Loop: Header=BB0_570 Depth=1
	andl	$6144, %eax             # imm = 0x1800
	movzwl	%ax, %eax
	cmpl	$2048, %eax             # imm = 0x800
	je	.LBB0_635
# BB#572:                               #   in Loop: Header=BB0_570 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	addq	$44, %rcx
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	56(%rbx,%rax,4), %edi
	movl	48(%rbp,%rax,4), %esi
	movq	%rbp, 160(%rsp)         # 8-byte Spill
	movl	56(%rbp,%rax,4), %edx
	movl	128(%rsp), %r8d         # 4-byte Reload
	movl	104(%rsp), %ebx         # 4-byte Reload
	movl	%ebx, %r9d
	callq	ActualGap
	addl	%eax, %ebx
	movl	%ebx, 104(%rsp)         # 4-byte Spill
	incl	84(%rsp)                # 4-byte Folded Spill
	movq	%r12, %rcx
	movq	8(%rcx), %rcx
	cmpq	8(%rsp), %rcx           # 8-byte Folded Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	%r14, %r8
	movl	%r15d, %r11d
	movl	40(%rsp), %r10d         # 4-byte Reload
	je	.LBB0_585
# BB#573:                               # %.preheader.lr.ph.i.preheader
                                        #   in Loop: Header=BB0_570 Depth=1
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
.LBB0_574:                              # %.preheader.i
                                        #   Parent Loop BB0_570 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_575 Depth 3
	movq	%rcx, %rbp
.LBB0_575:                              #   Parent Loop BB0_570 Depth=1
                                        #     Parent Loop BB0_574 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB0_575
# BB#576:                               #   in Loop: Header=BB0_574 Depth=2
	cmpb	$9, %al
	je	.LBB0_579
# BB#577:                               #   in Loop: Header=BB0_574 Depth=2
	cmpb	$1, %al
	jne	.LBB0_580
# BB#578:                               # %.critedge2.outer.i
                                        #   in Loop: Header=BB0_574 Depth=2
	movq	8(%rcx), %rcx
	cmpq	8(%rsp), %rcx           # 8-byte Folded Reload
	movq	%rbp, %r12
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	jne	.LBB0_574
	jmp	.LBB0_585
.LBB0_579:                              #   in Loop: Header=BB0_574 Depth=2
	movq	%rbp, %rdi
	movq	%rcx, %rbx
	callq	SplitIsDefinite
	movq	%rbx, %rcx
	movl	40(%rsp), %r10d         # 4-byte Reload
	movl	%r15d, %r11d
	movq	%r14, %r8
	movq	56(%rsp), %r9           # 8-byte Reload
	testl	%eax, %eax
	je	.LBB0_581
	jmp	.LBB0_582
.LBB0_580:                              #   in Loop: Header=BB0_574 Depth=2
	addb	$-9, %al
	cmpb	$90, %al
	jbe	.LBB0_582
.LBB0_581:                              # %.critedge2.backedge.i
                                        #   in Loop: Header=BB0_574 Depth=2
	movq	8(%rcx), %rcx
	cmpq	8(%rsp), %rcx           # 8-byte Folded Reload
	jne	.LBB0_574
	jmp	.LBB0_585
.LBB0_582:                              #   in Loop: Header=BB0_570 Depth=1
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	testq	%r12, %r12
	jne	.LBB0_584
# BB#583:                               #   in Loop: Header=BB0_570 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_584:                              # %.backedge.i
                                        #   in Loop: Header=BB0_570 Depth=1
	movq	168(%rsp), %r12         # 8-byte Reload
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	movq	160(%rsp), %rbx         # 8-byte Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	%r14, %r8
	movl	%r15d, %r11d
	movl	40(%rsp), %r10d         # 4-byte Reload
	jne	.LBB0_570
.LBB0_585:                              # %.backedge.thread.i
	movl	128(%rsp), %eax         # 4-byte Reload
	subl	104(%rsp), %eax         # 4-byte Folded Reload
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	160(%rsp), %rdx         # 8-byte Reload
	subl	56(%rdx,%rcx,4), %eax
	cltd
	idivl	84(%rsp)                # 4-byte Folded Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	jmp	.LBB0_587
.LBB0_586:
	xorl	%eax, %eax
.LBB0_587:                              # %FindAdjustIncrement.exit
	movq	%rax, 96(%rsp)          # 8-byte Spill
.LBB0_588:                              # %FindAdjustIncrement.exit
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	48(%r13,%rax,4), %r14d
	addl	136(%rsp), %r14d        # 4-byte Folded Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rbp
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movl	280(%rsp), %r15d
	je	.LBB0_598
# BB#589:                               # %.preheader2019.lr.ph.preheader
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%r14d, 48(%rsp)         # 4-byte Spill
.LBB0_590:                              # %.preheader2019
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_591 Depth 2
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB0_591:                              #   Parent Loop BB0_590 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_591
# BB#592:                               #   in Loop: Header=BB0_590 Depth=1
	cmpb	$9, %al
	je	.LBB0_595
# BB#593:                               #   in Loop: Header=BB0_590 Depth=1
	cmpb	$1, %al
	jne	.LBB0_596
# BB#594:                               # %.critedge7.outer
                                        #   in Loop: Header=BB0_590 Depth=1
	movq	8(%rbp), %rbp
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	jne	.LBB0_590
	jmp	.LBB0_598
.LBB0_595:                              #   in Loop: Header=BB0_590 Depth=1
	movq	%rbx, %rdi
	movq	%r8, %r15
	movl	%r11d, %r12d
	movl	%r10d, %r14d
	callq	SplitIsDefinite
	movl	%r14d, %r10d
	movl	48(%rsp), %r14d         # 4-byte Reload
	movl	%r12d, %r11d
	movq	%r15, %r8
	movl	280(%rsp), %r15d
	movq	56(%rsp), %r9           # 8-byte Reload
	testl	%eax, %eax
	je	.LBB0_597
	jmp	.LBB0_603
.LBB0_596:                              #   in Loop: Header=BB0_590 Depth=1
	addb	$-9, %al
	cmpb	$90, %al
	jbe	.LBB0_603
.LBB0_597:                              # %.critedge7.backedge
                                        #   in Loop: Header=BB0_590 Depth=1
	movq	8(%rbp), %rbp
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	jne	.LBB0_590
.LBB0_598:                              # %.preheader2017.thread
	movl	%r14d, %ecx
	negl	%ecx
.LBB0_599:                              # %._crit_edge2524
	testl	%r10d, %r10d
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	48(%r13,%rax,4), %edx
	movl	56(%r13,%rax,4), %eax
	je	.LBB0_601
# BB#600:
	movl	%r11d, %r8d
	movq	%r9, %rbp
	jmp	.LBB0_602
.LBB0_601:
	movq	%r8, %rdi
	movl	%r11d, %r8d
	movq	%r9, %rbp
	movq	64(%rsp), %rsi          # 8-byte Reload
	addl	%edi, %esi
	addl	%ecx, %esi
	cmpl	%esi, %eax
	cmovgel	%eax, %esi
	movl	%esi, %eax
.LBB0_602:
	movq	296(%rsp), %rbx
	movq	8(%rsp), %r12           # 8-byte Reload
	leaq	16(%rsp), %r11
	leaq	20(%rsp), %r10
	movl	$0, %r9d
	movq	%r13, %rdi
	movl	%r14d, %esi
	movl	%eax, %ecx
	pushq	%r11
.Lcfi245:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi246:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi247:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi248:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi249:
	.cfi_adjust_cfa_offset -32
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	48(%r12,%rsi,4), %ecx
	cmpl	%ebp, %ecx
	cmovll	%ebp, %ecx
	movq	288(%rsp), %rdx
	movl	%ecx, (%rdx)
	subl	136(%rsp), %r14d        # 4-byte Folded Reload
	addl	56(%rax,%rsi,4), %r14d
	subl	%ecx, %r14d
	movl	%r14d, (%rbx)
	jmp	.LBB0_282
.LBB0_603:
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_605
# BB#604:
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	%r11d, %r15d
	movq	%r9, %r11
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	movq	%r11, %r14
	movl	%r10d, %r12d
	callq	Error
	movl	%r12d, %r10d
	movl	%r15d, %r11d
	movl	280(%rsp), %r15d
	movq	88(%rsp), %r8           # 8-byte Reload
	movq	%r14, %r9
	movl	48(%rsp), %r14d         # 4-byte Reload
.LBB0_605:                              # %.preheader2017
	movl	%r14d, %ecx
	negl	%ecx
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	je	.LBB0_599
# BB#606:                               # %.lr.ph2523
	movq	64(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r8), %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movq	%r13, %rdi
	movl	%r10d, 40(%rsp)         # 4-byte Spill
.LBB0_607:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_613 Depth 2
                                        #       Child Loop BB0_614 Depth 3
	movl	%r14d, %r12d
	movl	%r11d, %r15d
	movq	%r8, %r14
	movq	%rbx, %r13
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	44(%rdx), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movzwl	44(%rdx), %eax
	andl	$64512, %eax            # imm = 0xFC00
	cmpl	$52224, %eax            # imm = 0xCC00
	jne	.LBB0_610
# BB#608:                               #   in Loop: Header=BB0_607 Depth=1
	movzwl	46(%rdx), %eax
	cmpl	$4096, %eax             # imm = 0x1000
	jne	.LBB0_610
# BB#609:                               #   in Loop: Header=BB0_607 Depth=1
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	48(%rdi,%rsi,4), %edx
	movl	56(%rdi,%rsi,4), %eax
	leaq	48(%r13,%rsi,4), %rbx
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	leaq	56(%r13,%rsi,4), %rbx
	addl	104(%rsp), %ecx         # 4-byte Folded Reload
	subl	48(%r13,%rsi,4), %ecx
	subl	56(%r13,%rsi,4), %ecx
	cmpl	%ecx, %eax
	cmovgel	%eax, %ecx
	movl	$0, %r9d
	movl	%r12d, %esi
	movl	%r15d, %r8d
	leaq	16(%rsp), %rax
	pushq	%rax
.Lcfi250:
	.cfi_adjust_cfa_offset 8
	leaq	28(%rsp), %rax
	pushq	%rax
.Lcfi251:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi252:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi253:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	movq	64(%rsp), %rax          # 8-byte Reload
	addq	$32, %rsp
.Lcfi254:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB0_611
.LBB0_610:                              #   in Loop: Header=BB0_607 Depth=1
	movq	72(%rsp), %rbx          # 8-byte Reload
	movl	48(%rdi,%rbx,4), %edx
	movl	56(%rdi,%rbx,4), %ecx
	addl	96(%rsp), %ecx          # 4-byte Folded Reload
	movl	$0, %r9d
	movl	%r12d, %esi
	movl	%r15d, %r8d
	leaq	16(%rsp), %rax
	pushq	%rax
.Lcfi255:
	.cfi_adjust_cfa_offset 8
	leaq	28(%rsp), %rax
	pushq	%rax
.Lcfi256:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi257:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi258:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi259:
	.cfi_adjust_cfa_offset -32
	leaq	48(%r13,%rbx,4), %rax
	leaq	56(%r13,%rbx,4), %rbx
.LBB0_611:                              #   in Loop: Header=BB0_607 Depth=1
	movl	16(%rsp), %edi
	movl	(%rax), %esi
	movl	(%rbx), %edx
	movl	%r12d, %r9d
	subl	136(%rsp), %r9d         # 4-byte Folded Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	128(%rsp), %r8d         # 4-byte Reload
	callq	ActualGap
	addl	%eax, %r12d
	movq	8(%rbp), %rbp
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	%r14, %r8
	movl	%r15d, %r11d
	movl	40(%rsp), %r10d         # 4-byte Reload
	je	.LBB0_624
# BB#612:                               # %.preheader2015.lr.ph.preheader
                                        #   in Loop: Header=BB0_607 Depth=1
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
.LBB0_613:                              # %.preheader2015
                                        #   Parent Loop BB0_607 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_614 Depth 3
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB0_614:                              #   Parent Loop BB0_607 Depth=1
                                        #     Parent Loop BB0_613 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_614
# BB#615:                               #   in Loop: Header=BB0_613 Depth=2
	cmpb	$9, %al
	je	.LBB0_618
# BB#616:                               #   in Loop: Header=BB0_613 Depth=2
	cmpb	$1, %al
	jne	.LBB0_619
# BB#617:                               # %.critedge8.outer
                                        #   in Loop: Header=BB0_613 Depth=2
	movq	8(%rbp), %rbp
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	%rbx, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	jne	.LBB0_613
	jmp	.LBB0_624
.LBB0_618:                              #   in Loop: Header=BB0_613 Depth=2
	movq	%rbx, %rdi
	callq	SplitIsDefinite
	movl	40(%rsp), %r10d         # 4-byte Reload
	movl	%r15d, %r11d
	movq	%r14, %r8
	movq	56(%rsp), %r9           # 8-byte Reload
	testl	%eax, %eax
	je	.LBB0_620
	jmp	.LBB0_621
.LBB0_619:                              #   in Loop: Header=BB0_613 Depth=2
	addb	$-9, %al
	cmpb	$90, %al
	jbe	.LBB0_621
.LBB0_620:                              # %.critedge8.backedge
                                        #   in Loop: Header=BB0_613 Depth=2
	movq	8(%rbp), %rbp
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	jne	.LBB0_613
	jmp	.LBB0_624
.LBB0_621:                              #   in Loop: Header=BB0_607 Depth=1
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_623
# BB#622:                               #   in Loop: Header=BB0_607 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	40(%rsp), %r10d         # 4-byte Reload
	movl	%r15d, %r11d
	movq	%r14, %r8
	movq	56(%rsp), %r9           # 8-byte Reload
.LBB0_623:                              # %.backedge2018
                                        #   in Loop: Header=BB0_607 Depth=1
	movl	%r12d, %r14d
	movl	%r14d, %ecx
	negl	%ecx
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	%r13, %rdi
	movl	280(%rsp), %r15d
	jne	.LBB0_607
	jmp	.LBB0_599
.LBB0_624:                              # %.backedge2018.thread
	movl	%r12d, %r14d
	movl	%r14d, %ecx
	negl	%ecx
	movl	280(%rsp), %r15d
	jmp	.LBB0_599
.LBB0_625:                              # %.backedge2037.thread
	movl	40(%rbp), %ecx
	movl	%ecx, %eax
	shrl	$29, %eax
	andl	$3, %eax
	leaq	40(%rbp), %rsi
	movl	%r12d, %r14d
	jmp	.LBB0_481
.LBB0_626:
	movzwl	42(%rbx), %eax
	andl	$63487, %eax            # imm = 0xF7FF
	movw	%ax, 42(%rbx)
	movl	%edx, %edi
	subl	%ecx, %edi
	movl	%edi, %esi
	shrl	$31, %esi
	addl	%edi, %esi
	sarl	%esi
	jmp	.LBB0_455
.LBB0_627:
	movzwl	42(%rbx), %eax
	andl	$63487, %eax            # imm = 0xF7FF
	movw	%ax, 42(%rbx)
	jmp	.LBB0_455
.LBB0_628:                              # %._crit_edge3023
	movw	42(%rbx), %ax
	jmp	.LBB0_454
.LBB0_629:
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	56(%rax), %edi
	addl	48(%rax), %edi
	callq	EchoLength
	movq	%rax, %r14
	movl	64(%rbx), %eax
	cvtsi2ssl	%eax, %xmm0
	mulss	.LCPI0_0(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movl	48(%rsp), %edi          # 4-byte Reload
	callq	EchoLength
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi260:
	.cfi_adjust_cfa_offset 8
	movl	$23, %edi
	movl	$4, %esi
	movl	$.L.str.9, %edx
	movl	$2, %ecx
	movb	$1, %al
	movq	%r13, %r8
	movq	%r14, %r9
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	pushq	%rbp
.Lcfi261:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi262:
	.cfi_adjust_cfa_offset -16
.LBB0_630:
	movl	48(%rbx), %edx
	movl	56(%rbx), %ecx
	leaq	16(%rsp), %rax
	leaq	20(%rsp), %rbp
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdi
	movq	88(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	pushq	%rax
.Lcfi263:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi264:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi265:
	.cfi_adjust_cfa_offset 8
	movl	296(%rsp), %eax
	pushq	%rax
.Lcfi266:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi267:
	.cfi_adjust_cfa_offset -32
.LBB0_631:                              # %.loopexit2051
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	%r12, %r9
	jmp	.LBB0_122
.LBB0_633:
	movq	%rbx, %rcx
	movq	%rcx, %rbp
.LBB0_634:                              # %.backedge2045.thread
	movl	56(%rcx), %edi
	movq	128(%rsp), %rax         # 8-byte Reload
	jmp	.LBB0_315
.LBB0_635:
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	%r14, %r8
	movl	%r15d, %r11d
	movl	40(%rsp), %r10d         # 4-byte Reload
	jmp	.LBB0_588
.Lfunc_end0:
	.size	FixAndPrintObject, .Lfunc_end0-FixAndPrintObject
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_218
	.quad	.LBB0_101
	.quad	.LBB0_218
	.quad	.LBB0_218
	.quad	.LBB0_218
	.quad	.LBB0_218
	.quad	.LBB0_101
	.quad	.LBB0_102
	.quad	.LBB0_101
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_25
	.quad	.LBB0_25
	.quad	.LBB0_104
	.quad	.LBB0_49
	.quad	.LBB0_49
	.quad	.LBB0_4
	.quad	.LBB0_4
	.quad	.LBB0_4
	.quad	.LBB0_4
	.quad	.LBB0_2
	.quad	.LBB0_2
	.quad	.LBB0_63
	.quad	.LBB0_63
	.quad	.LBB0_68
	.quad	.LBB0_68
	.quad	.LBB0_123
	.quad	.LBB0_132
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_141
	.quad	.LBB0_149
	.quad	.LBB0_73
	.quad	.LBB0_73
	.quad	.LBB0_2
	.quad	.LBB0_2
	.quad	.LBB0_2
	.quad	.LBB0_2
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_78
	.quad	.LBB0_78
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_154
	.quad	.LBB0_160
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_101
	.quad	.LBB0_87
	.quad	.LBB0_87
	.quad	.LBB0_166
	.quad	.LBB0_179
	.quad	.LBB0_93
	.quad	.LBB0_93

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"assert failed in %s"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"FAPO HSPAN/VSPAN!"
	.size	.L.str.1, 18

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"object deleted (it cannot be scaled vertically)"
	.size	.L.str.2, 48

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"object deleted (it cannot be scaled horizontally)"
	.size	.L.str.3, 50

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"FAPO: horizontal scale factor!"
	.size	.L.str.4, 31

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"FAPO: vertical scale factor!"
	.size	.L.str.5, 29

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"NextDefiniteWithGap: g == nilobj!"
	.size	.L.str.6, 34

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"FAPO: final start_group!"
	.size	.L.str.7, 25

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"word %s horizontally scaled by factor %.2f (too wide for %s paragraph)"
	.size	.L.str.8, 71

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%s object horizontally scaled by factor %.2f (too wide for %s paragraph)"
	.size	.L.str.9, 73

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"oversize object has size 0 or less"
	.size	.L.str.10, 35

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"word %s deleted (too wide for %s paragraph)"
	.size	.L.str.11, 44

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"%s object deleted (too wide for %s paragraph)"
	.size	.L.str.12, 46

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.zero	1
	.size	.L.str.13, 1

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"FAPO: adjustable_gaps!"
	.size	.L.str.15, 23

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"FixAndPrint: underline(prev)!"
	.size	.L.str.16, 30

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"FixAndPrintObject: thr!"
	.size	.L.str.17, 24

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"FixAndPrintObject: link or uplink!"
	.size	.L.str.18, 35

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"FAPO: THR!"
	.size	.L.str.19, 11

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"%s symbol ignored (out of place)"
	.size	.L.str.20, 33

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"assert failed in %s %s"
	.size	.L.str.21, 23

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"FixAndPrintObject:"
	.size	.L.str.22, 19


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
