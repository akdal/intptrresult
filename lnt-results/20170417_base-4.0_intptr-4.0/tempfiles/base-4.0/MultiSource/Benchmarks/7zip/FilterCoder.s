	.text
	.file	"FilterCoder.bc"
	.globl	_ZN12CFilterCoderC2Ev
	.p2align	4, 0x90
	.type	_ZN12CFilterCoderC2Ev,@function
_ZN12CFilterCoderC2Ev:                  # @_ZN12CFilterCoderC2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$0, 88(%rbx)
	movl	$_ZTV12CFilterCoder+176, %eax
	movd	%rax, %xmm0
	movl	$_ZTV12CFilterCoder+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTV12CFilterCoder+312, %eax
	movd	%rax, %xmm0
	movl	$_ZTV12CFilterCoder+248, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	movl	$_ZTV12CFilterCoder+448, %eax
	movd	%rax, %xmm0
	movl	$_ZTV12CFilterCoder+384, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 32(%rbx)
	movl	$_ZTV12CFilterCoder+576, %eax
	movd	%rax, %xmm0
	movl	$_ZTV12CFilterCoder+512, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 48(%rbx)
	movl	$_ZTV12CFilterCoder+704, %eax
	movd	%rax, %xmm0
	movl	$_ZTV12CFilterCoder+640, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 64(%rbx)
	movq	$_ZTV12CFilterCoder+768, 80(%rbx)
	leaq	104(%rbx), %r15
	leaq	152(%rbx), %r12
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 104(%rbx)
	movdqu	%xmm0, 184(%rbx)
	movdqu	%xmm0, 168(%rbx)
	movdqu	%xmm0, 152(%rbx)
.Ltmp0:
	movl	$131072, %edi           # imm = 0x20000
	callq	MidAlloc
.Ltmp1:
# BB#1:
	movq	%rax, 96(%rbx)
	testq	%rax, %rax
	je	.LBB0_2
# BB#22:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB0_2:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$1, (%rax)
.Ltmp2:
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp3:
# BB#21:
.LBB0_3:
.Ltmp4:
	movq	%rax, %r14
	movq	192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_5
# BB#4:
	movq	(%rdi), %rax
.Ltmp5:
	callq	*16(%rax)
.Ltmp6:
.LBB0_5:                                # %_ZN9CMyComPtrI15ICompressFilterED2Ev.exit
	movq	184(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_7
# BB#6:
	movq	(%rdi), %rax
.Ltmp7:
	callq	*16(%rax)
.Ltmp8:
.LBB0_7:                                # %_ZN9CMyComPtrI30ICompressSetDecoderProperties2ED2Ev.exit
	movq	176(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_9
# BB#8:
	movq	(%rdi), %rax
.Ltmp9:
	callq	*16(%rax)
.Ltmp10:
.LBB0_9:                                # %_ZN9CMyComPtrI22ICryptoResetInitVectorED2Ev.exit
	movq	168(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_11
# BB#10:
	movq	(%rdi), %rax
.Ltmp11:
	callq	*16(%rax)
.Ltmp12:
.LBB0_11:                               # %_ZN9CMyComPtrI29ICompressWriteCoderPropertiesED2Ev.exit
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_13
# BB#12:
	movq	(%rdi), %rax
.Ltmp13:
	callq	*16(%rax)
.Ltmp14:
.LBB0_13:                               # %_ZN9CMyComPtrI27ICompressSetCoderPropertiesED2Ev.exit
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB0_15
# BB#14:
	movq	(%rdi), %rax
.Ltmp15:
	callq	*16(%rax)
.Ltmp16:
.LBB0_15:                               # %_ZN9CMyComPtrI18ICryptoSetPasswordED2Ev.exit
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_17
# BB#16:
	movq	(%rdi), %rax
.Ltmp17:
	callq	*16(%rax)
.Ltmp18:
.LBB0_17:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB0_19
# BB#18:
	movq	(%rdi), %rax
.Ltmp19:
	callq	*16(%rax)
.Ltmp20:
.LBB0_19:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_20:
.Ltmp21:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN12CFilterCoderC2Ev, .Lfunc_end0-_ZN12CFilterCoderC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp2-.Ltmp1           #   Call between .Ltmp1 and .Ltmp2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp2-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp2           #   Call between .Ltmp2 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp20-.Ltmp5          #   Call between .Ltmp5 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin0   #     jumps to .Ltmp21
	.byte	1                       #   On action: 1
	.long	.Ltmp20-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Lfunc_end0-.Ltmp20     #   Call between .Ltmp20 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN12CFilterCoderD2Ev
	.p2align	4, 0x90
	.type	_ZN12CFilterCoderD2Ev,@function
_ZN12CFilterCoderD2Ev:                  # @_ZN12CFilterCoderD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -24
.Lcfi13:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTV12CFilterCoder+176, %eax
	movd	%rax, %xmm0
	movl	$_ZTV12CFilterCoder+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTV12CFilterCoder+312, %eax
	movd	%rax, %xmm0
	movl	$_ZTV12CFilterCoder+248, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	movl	$_ZTV12CFilterCoder+448, %eax
	movd	%rax, %xmm0
	movl	$_ZTV12CFilterCoder+384, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 32(%rbx)
	movl	$_ZTV12CFilterCoder+576, %eax
	movd	%rax, %xmm0
	movl	$_ZTV12CFilterCoder+512, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 48(%rbx)
	movl	$_ZTV12CFilterCoder+704, %eax
	movd	%rax, %xmm0
	movl	$_ZTV12CFilterCoder+640, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 64(%rbx)
	movq	$_ZTV12CFilterCoder+768, 80(%rbx)
	movq	96(%rbx), %rdi
.Ltmp22:
	callq	MidFree
.Ltmp23:
# BB#1:
	movq	192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_3
# BB#2:
	movq	(%rdi), %rax
.Ltmp27:
	callq	*16(%rax)
.Ltmp28:
.LBB2_3:                                # %_ZN9CMyComPtrI15ICompressFilterED2Ev.exit
	movq	184(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_5
# BB#4:
	movq	(%rdi), %rax
.Ltmp32:
	callq	*16(%rax)
.Ltmp33:
.LBB2_5:                                # %_ZN9CMyComPtrI30ICompressSetDecoderProperties2ED2Ev.exit
	movq	176(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_7
# BB#6:
	movq	(%rdi), %rax
.Ltmp37:
	callq	*16(%rax)
.Ltmp38:
.LBB2_7:                                # %_ZN9CMyComPtrI22ICryptoResetInitVectorED2Ev.exit
	movq	168(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_9
# BB#8:
	movq	(%rdi), %rax
.Ltmp42:
	callq	*16(%rax)
.Ltmp43:
.LBB2_9:                                # %_ZN9CMyComPtrI29ICompressWriteCoderPropertiesED2Ev.exit
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_11
# BB#10:
	movq	(%rdi), %rax
.Ltmp47:
	callq	*16(%rax)
.Ltmp48:
.LBB2_11:                               # %_ZN9CMyComPtrI27ICompressSetCoderPropertiesED2Ev.exit
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_13
# BB#12:
	movq	(%rdi), %rax
.Ltmp52:
	callq	*16(%rax)
.Ltmp53:
.LBB2_13:                               # %_ZN9CMyComPtrI18ICryptoSetPasswordED2Ev.exit
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_15
# BB#14:
	movq	(%rdi), %rax
.Ltmp57:
	callq	*16(%rax)
.Ltmp58:
.LBB2_15:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_17
# BB#16:
	movq	(%rdi), %rax
.Ltmp63:
	callq	*16(%rax)
.Ltmp64:
.LBB2_17:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB2_38:
.Ltmp65:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_39:
.Ltmp59:
	movq	%rax, %r14
	jmp	.LBB2_40
.LBB2_35:
.Ltmp54:
	movq	%rax, %r14
	jmp	.LBB2_36
.LBB2_32:
.Ltmp49:
	movq	%rax, %r14
	jmp	.LBB2_33
.LBB2_29:
.Ltmp44:
	movq	%rax, %r14
	jmp	.LBB2_30
.LBB2_26:
.Ltmp39:
	movq	%rax, %r14
	jmp	.LBB2_27
.LBB2_23:
.Ltmp34:
	movq	%rax, %r14
	jmp	.LBB2_24
.LBB2_20:
.Ltmp29:
	movq	%rax, %r14
	jmp	.LBB2_21
.LBB2_18:
.Ltmp24:
	movq	%rax, %r14
	movq	192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_21
# BB#19:
	movq	(%rdi), %rax
.Ltmp25:
	callq	*16(%rax)
.Ltmp26:
.LBB2_21:                               # %_ZN9CMyComPtrI15ICompressFilterED2Ev.exit47
	movq	184(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_24
# BB#22:
	movq	(%rdi), %rax
.Ltmp30:
	callq	*16(%rax)
.Ltmp31:
.LBB2_24:                               # %_ZN9CMyComPtrI30ICompressSetDecoderProperties2ED2Ev.exit45
	movq	176(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_27
# BB#25:
	movq	(%rdi), %rax
.Ltmp35:
	callq	*16(%rax)
.Ltmp36:
.LBB2_27:                               # %_ZN9CMyComPtrI22ICryptoResetInitVectorED2Ev.exit43
	movq	168(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_30
# BB#28:
	movq	(%rdi), %rax
.Ltmp40:
	callq	*16(%rax)
.Ltmp41:
.LBB2_30:                               # %_ZN9CMyComPtrI29ICompressWriteCoderPropertiesED2Ev.exit41
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_33
# BB#31:
	movq	(%rdi), %rax
.Ltmp45:
	callq	*16(%rax)
.Ltmp46:
.LBB2_33:                               # %_ZN9CMyComPtrI27ICompressSetCoderPropertiesED2Ev.exit39
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_36
# BB#34:
	movq	(%rdi), %rax
.Ltmp50:
	callq	*16(%rax)
.Ltmp51:
.LBB2_36:                               # %_ZN9CMyComPtrI18ICryptoSetPasswordED2Ev.exit37
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_40
# BB#37:
	movq	(%rdi), %rax
.Ltmp55:
	callq	*16(%rax)
.Ltmp56:
.LBB2_40:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit35
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_42
# BB#41:
	movq	(%rdi), %rax
.Ltmp60:
	callq	*16(%rax)
.Ltmp61:
.LBB2_42:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit33
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_43:
.Ltmp62:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN12CFilterCoderD2Ev, .Lfunc_end2-_ZN12CFilterCoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\245\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\234\001"              # Call site table length
	.long	.Ltmp22-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin1   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin1   #     jumps to .Ltmp29
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin1   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin1   #     jumps to .Ltmp39
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin1   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin1   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin1   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin1   #     jumps to .Ltmp59
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp64-.Ltmp63         #   Call between .Ltmp63 and .Ltmp64
	.long	.Ltmp65-.Lfunc_begin1   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp64-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Ltmp25-.Ltmp64         #   Call between .Ltmp64 and .Ltmp25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin1   # >> Call Site 11 <<
	.long	.Ltmp61-.Ltmp25         #   Call between .Ltmp25 and .Ltmp61
	.long	.Ltmp62-.Lfunc_begin1   #     jumps to .Ltmp62
	.byte	1                       #   On action: 1
	.long	.Ltmp61-.Lfunc_begin1   # >> Call Site 12 <<
	.long	.Lfunc_end2-.Ltmp61     #   Call between .Ltmp61 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn8_N12CFilterCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N12CFilterCoderD1Ev,@function
_ZThn8_N12CFilterCoderD1Ev:             # @_ZThn8_N12CFilterCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN12CFilterCoderD2Ev   # TAILCALL
.Lfunc_end3:
	.size	_ZThn8_N12CFilterCoderD1Ev, .Lfunc_end3-_ZThn8_N12CFilterCoderD1Ev
	.cfi_endproc

	.globl	_ZThn16_N12CFilterCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N12CFilterCoderD1Ev,@function
_ZThn16_N12CFilterCoderD1Ev:            # @_ZThn16_N12CFilterCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN12CFilterCoderD2Ev   # TAILCALL
.Lfunc_end4:
	.size	_ZThn16_N12CFilterCoderD1Ev, .Lfunc_end4-_ZThn16_N12CFilterCoderD1Ev
	.cfi_endproc

	.globl	_ZThn24_N12CFilterCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn24_N12CFilterCoderD1Ev,@function
_ZThn24_N12CFilterCoderD1Ev:            # @_ZThn24_N12CFilterCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN12CFilterCoderD2Ev   # TAILCALL
.Lfunc_end5:
	.size	_ZThn24_N12CFilterCoderD1Ev, .Lfunc_end5-_ZThn24_N12CFilterCoderD1Ev
	.cfi_endproc

	.globl	_ZThn32_N12CFilterCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn32_N12CFilterCoderD1Ev,@function
_ZThn32_N12CFilterCoderD1Ev:            # @_ZThn32_N12CFilterCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-32, %rdi
	jmp	_ZN12CFilterCoderD2Ev   # TAILCALL
.Lfunc_end6:
	.size	_ZThn32_N12CFilterCoderD1Ev, .Lfunc_end6-_ZThn32_N12CFilterCoderD1Ev
	.cfi_endproc

	.globl	_ZThn40_N12CFilterCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn40_N12CFilterCoderD1Ev,@function
_ZThn40_N12CFilterCoderD1Ev:            # @_ZThn40_N12CFilterCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-40, %rdi
	jmp	_ZN12CFilterCoderD2Ev   # TAILCALL
.Lfunc_end7:
	.size	_ZThn40_N12CFilterCoderD1Ev, .Lfunc_end7-_ZThn40_N12CFilterCoderD1Ev
	.cfi_endproc

	.globl	_ZThn48_N12CFilterCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn48_N12CFilterCoderD1Ev,@function
_ZThn48_N12CFilterCoderD1Ev:            # @_ZThn48_N12CFilterCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-48, %rdi
	jmp	_ZN12CFilterCoderD2Ev   # TAILCALL
.Lfunc_end8:
	.size	_ZThn48_N12CFilterCoderD1Ev, .Lfunc_end8-_ZThn48_N12CFilterCoderD1Ev
	.cfi_endproc

	.globl	_ZThn56_N12CFilterCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn56_N12CFilterCoderD1Ev,@function
_ZThn56_N12CFilterCoderD1Ev:            # @_ZThn56_N12CFilterCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-56, %rdi
	jmp	_ZN12CFilterCoderD2Ev   # TAILCALL
.Lfunc_end9:
	.size	_ZThn56_N12CFilterCoderD1Ev, .Lfunc_end9-_ZThn56_N12CFilterCoderD1Ev
	.cfi_endproc

	.globl	_ZThn64_N12CFilterCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn64_N12CFilterCoderD1Ev,@function
_ZThn64_N12CFilterCoderD1Ev:            # @_ZThn64_N12CFilterCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-64, %rdi
	jmp	_ZN12CFilterCoderD2Ev   # TAILCALL
.Lfunc_end10:
	.size	_ZThn64_N12CFilterCoderD1Ev, .Lfunc_end10-_ZThn64_N12CFilterCoderD1Ev
	.cfi_endproc

	.globl	_ZThn72_N12CFilterCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn72_N12CFilterCoderD1Ev,@function
_ZThn72_N12CFilterCoderD1Ev:            # @_ZThn72_N12CFilterCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-72, %rdi
	jmp	_ZN12CFilterCoderD2Ev   # TAILCALL
.Lfunc_end11:
	.size	_ZThn72_N12CFilterCoderD1Ev, .Lfunc_end11-_ZThn72_N12CFilterCoderD1Ev
	.cfi_endproc

	.globl	_ZThn80_N12CFilterCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn80_N12CFilterCoderD1Ev,@function
_ZThn80_N12CFilterCoderD1Ev:            # @_ZThn80_N12CFilterCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-80, %rdi
	jmp	_ZN12CFilterCoderD2Ev   # TAILCALL
.Lfunc_end12:
	.size	_ZThn80_N12CFilterCoderD1Ev, .Lfunc_end12-_ZThn80_N12CFilterCoderD1Ev
	.cfi_endproc

	.globl	_ZN12CFilterCoderD0Ev
	.p2align	4, 0x90
	.type	_ZN12CFilterCoderD0Ev,@function
_ZN12CFilterCoderD0Ev:                  # @_ZN12CFilterCoderD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -24
.Lcfi18:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp66:
	callq	_ZN12CFilterCoderD2Ev
.Ltmp67:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB13_2:
.Ltmp68:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end13:
	.size	_ZN12CFilterCoderD0Ev, .Lfunc_end13-_ZN12CFilterCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp66-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp67-.Ltmp66         #   Call between .Ltmp66 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin2   #     jumps to .Ltmp68
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end13-.Ltmp67    #   Call between .Ltmp67 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn8_N12CFilterCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N12CFilterCoderD0Ev,@function
_ZThn8_N12CFilterCoderD0Ev:             # @_ZThn8_N12CFilterCoderD0Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -24
.Lcfi23:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp69:
	movq	%rbx, %rdi
	callq	_ZN12CFilterCoderD2Ev
.Ltmp70:
# BB#1:                                 # %_ZN12CFilterCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB14_2:
.Ltmp71:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end14:
	.size	_ZThn8_N12CFilterCoderD0Ev, .Lfunc_end14-_ZThn8_N12CFilterCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp69-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp70-.Ltmp69         #   Call between .Ltmp69 and .Ltmp70
	.long	.Ltmp71-.Lfunc_begin3   #     jumps to .Ltmp71
	.byte	0                       #   On action: cleanup
	.long	.Ltmp70-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Lfunc_end14-.Ltmp70    #   Call between .Ltmp70 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn16_N12CFilterCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N12CFilterCoderD0Ev,@function
_ZThn16_N12CFilterCoderD0Ev:            # @_ZThn16_N12CFilterCoderD0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -24
.Lcfi28:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-16, %rbx
.Ltmp72:
	movq	%rbx, %rdi
	callq	_ZN12CFilterCoderD2Ev
.Ltmp73:
# BB#1:                                 # %_ZN12CFilterCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB15_2:
.Ltmp74:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end15:
	.size	_ZThn16_N12CFilterCoderD0Ev, .Lfunc_end15-_ZThn16_N12CFilterCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp72-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp73-.Ltmp72         #   Call between .Ltmp72 and .Ltmp73
	.long	.Ltmp74-.Lfunc_begin4   #     jumps to .Ltmp74
	.byte	0                       #   On action: cleanup
	.long	.Ltmp73-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Lfunc_end15-.Ltmp73    #   Call between .Ltmp73 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn24_N12CFilterCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn24_N12CFilterCoderD0Ev,@function
_ZThn24_N12CFilterCoderD0Ev:            # @_ZThn24_N12CFilterCoderD0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 32
.Lcfi32:
	.cfi_offset %rbx, -24
.Lcfi33:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-24, %rbx
.Ltmp75:
	movq	%rbx, %rdi
	callq	_ZN12CFilterCoderD2Ev
.Ltmp76:
# BB#1:                                 # %_ZN12CFilterCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB16_2:
.Ltmp77:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end16:
	.size	_ZThn24_N12CFilterCoderD0Ev, .Lfunc_end16-_ZThn24_N12CFilterCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp75-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp76-.Ltmp75         #   Call between .Ltmp75 and .Ltmp76
	.long	.Ltmp77-.Lfunc_begin5   #     jumps to .Ltmp77
	.byte	0                       #   On action: cleanup
	.long	.Ltmp76-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Lfunc_end16-.Ltmp76    #   Call between .Ltmp76 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn32_N12CFilterCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn32_N12CFilterCoderD0Ev,@function
_ZThn32_N12CFilterCoderD0Ev:            # @_ZThn32_N12CFilterCoderD0Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi36:
	.cfi_def_cfa_offset 32
.Lcfi37:
	.cfi_offset %rbx, -24
.Lcfi38:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-32, %rbx
.Ltmp78:
	movq	%rbx, %rdi
	callq	_ZN12CFilterCoderD2Ev
.Ltmp79:
# BB#1:                                 # %_ZN12CFilterCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB17_2:
.Ltmp80:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end17:
	.size	_ZThn32_N12CFilterCoderD0Ev, .Lfunc_end17-_ZThn32_N12CFilterCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp78-.Lfunc_begin6   # >> Call Site 1 <<
	.long	.Ltmp79-.Ltmp78         #   Call between .Ltmp78 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin6   #     jumps to .Ltmp80
	.byte	0                       #   On action: cleanup
	.long	.Ltmp79-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Lfunc_end17-.Ltmp79    #   Call between .Ltmp79 and .Lfunc_end17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn40_N12CFilterCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn40_N12CFilterCoderD0Ev,@function
_ZThn40_N12CFilterCoderD0Ev:            # @_ZThn40_N12CFilterCoderD0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 32
.Lcfi42:
	.cfi_offset %rbx, -24
.Lcfi43:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-40, %rbx
.Ltmp81:
	movq	%rbx, %rdi
	callq	_ZN12CFilterCoderD2Ev
.Ltmp82:
# BB#1:                                 # %_ZN12CFilterCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB18_2:
.Ltmp83:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end18:
	.size	_ZThn40_N12CFilterCoderD0Ev, .Lfunc_end18-_ZThn40_N12CFilterCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp81-.Lfunc_begin7   # >> Call Site 1 <<
	.long	.Ltmp82-.Ltmp81         #   Call between .Ltmp81 and .Ltmp82
	.long	.Ltmp83-.Lfunc_begin7   #     jumps to .Ltmp83
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin7   # >> Call Site 2 <<
	.long	.Lfunc_end18-.Ltmp82    #   Call between .Ltmp82 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn48_N12CFilterCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn48_N12CFilterCoderD0Ev,@function
_ZThn48_N12CFilterCoderD0Ev:            # @_ZThn48_N12CFilterCoderD0Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -24
.Lcfi48:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-48, %rbx
.Ltmp84:
	movq	%rbx, %rdi
	callq	_ZN12CFilterCoderD2Ev
.Ltmp85:
# BB#1:                                 # %_ZN12CFilterCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB19_2:
.Ltmp86:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end19:
	.size	_ZThn48_N12CFilterCoderD0Ev, .Lfunc_end19-_ZThn48_N12CFilterCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table19:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp84-.Lfunc_begin8   # >> Call Site 1 <<
	.long	.Ltmp85-.Ltmp84         #   Call between .Ltmp84 and .Ltmp85
	.long	.Ltmp86-.Lfunc_begin8   #     jumps to .Ltmp86
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin8   # >> Call Site 2 <<
	.long	.Lfunc_end19-.Ltmp85    #   Call between .Ltmp85 and .Lfunc_end19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn56_N12CFilterCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn56_N12CFilterCoderD0Ev,@function
_ZThn56_N12CFilterCoderD0Ev:            # @_ZThn56_N12CFilterCoderD0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 32
.Lcfi52:
	.cfi_offset %rbx, -24
.Lcfi53:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-56, %rbx
.Ltmp87:
	movq	%rbx, %rdi
	callq	_ZN12CFilterCoderD2Ev
.Ltmp88:
# BB#1:                                 # %_ZN12CFilterCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB20_2:
.Ltmp89:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end20:
	.size	_ZThn56_N12CFilterCoderD0Ev, .Lfunc_end20-_ZThn56_N12CFilterCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table20:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp87-.Lfunc_begin9   # >> Call Site 1 <<
	.long	.Ltmp88-.Ltmp87         #   Call between .Ltmp87 and .Ltmp88
	.long	.Ltmp89-.Lfunc_begin9   #     jumps to .Ltmp89
	.byte	0                       #   On action: cleanup
	.long	.Ltmp88-.Lfunc_begin9   # >> Call Site 2 <<
	.long	.Lfunc_end20-.Ltmp88    #   Call between .Ltmp88 and .Lfunc_end20
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn64_N12CFilterCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn64_N12CFilterCoderD0Ev,@function
_ZThn64_N12CFilterCoderD0Ev:            # @_ZThn64_N12CFilterCoderD0Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi56:
	.cfi_def_cfa_offset 32
.Lcfi57:
	.cfi_offset %rbx, -24
.Lcfi58:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-64, %rbx
.Ltmp90:
	movq	%rbx, %rdi
	callq	_ZN12CFilterCoderD2Ev
.Ltmp91:
# BB#1:                                 # %_ZN12CFilterCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB21_2:
.Ltmp92:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end21:
	.size	_ZThn64_N12CFilterCoderD0Ev, .Lfunc_end21-_ZThn64_N12CFilterCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp90-.Lfunc_begin10  # >> Call Site 1 <<
	.long	.Ltmp91-.Ltmp90         #   Call between .Ltmp90 and .Ltmp91
	.long	.Ltmp92-.Lfunc_begin10  #     jumps to .Ltmp92
	.byte	0                       #   On action: cleanup
	.long	.Ltmp91-.Lfunc_begin10  # >> Call Site 2 <<
	.long	.Lfunc_end21-.Ltmp91    #   Call between .Ltmp91 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn72_N12CFilterCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn72_N12CFilterCoderD0Ev,@function
_ZThn72_N12CFilterCoderD0Ev:            # @_ZThn72_N12CFilterCoderD0Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi61:
	.cfi_def_cfa_offset 32
.Lcfi62:
	.cfi_offset %rbx, -24
.Lcfi63:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-72, %rbx
.Ltmp93:
	movq	%rbx, %rdi
	callq	_ZN12CFilterCoderD2Ev
.Ltmp94:
# BB#1:                                 # %_ZN12CFilterCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB22_2:
.Ltmp95:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end22:
	.size	_ZThn72_N12CFilterCoderD0Ev, .Lfunc_end22-_ZThn72_N12CFilterCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp93-.Lfunc_begin11  # >> Call Site 1 <<
	.long	.Ltmp94-.Ltmp93         #   Call between .Ltmp93 and .Ltmp94
	.long	.Ltmp95-.Lfunc_begin11  #     jumps to .Ltmp95
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin11  # >> Call Site 2 <<
	.long	.Lfunc_end22-.Ltmp94    #   Call between .Ltmp94 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn80_N12CFilterCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn80_N12CFilterCoderD0Ev,@function
_ZThn80_N12CFilterCoderD0Ev:            # @_ZThn80_N12CFilterCoderD0Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi66:
	.cfi_def_cfa_offset 32
.Lcfi67:
	.cfi_offset %rbx, -24
.Lcfi68:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-80, %rbx
.Ltmp96:
	movq	%rbx, %rdi
	callq	_ZN12CFilterCoderD2Ev
.Ltmp97:
# BB#1:                                 # %_ZN12CFilterCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB23_2:
.Ltmp98:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end23:
	.size	_ZThn80_N12CFilterCoderD0Ev, .Lfunc_end23-_ZThn80_N12CFilterCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp96-.Lfunc_begin12  # >> Call Site 1 <<
	.long	.Ltmp97-.Ltmp96         #   Call between .Ltmp96 and .Ltmp97
	.long	.Ltmp98-.Lfunc_begin12  #     jumps to .Ltmp98
	.byte	0                       #   On action: cleanup
	.long	.Ltmp97-.Lfunc_begin12  # >> Call Site 2 <<
	.long	.Lfunc_end23-.Ltmp97    #   Call between .Ltmp97 and .Lfunc_end23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN12CFilterCoder14WriteWithLimitEP20ISequentialOutStreamj
	.p2align	4, 0x90
	.type	_ZN12CFilterCoder14WriteWithLimitEP20ISequentialOutStreamj,@function
_ZN12CFilterCoder14WriteWithLimitEP20ISequentialOutStreamj: # @_ZN12CFilterCoder14WriteWithLimitEP20ISequentialOutStreamj
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi71:
	.cfi_def_cfa_offset 32
.Lcfi72:
	.cfi_offset %rbx, -24
.Lcfi73:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	cmpb	$0, 132(%rbx)
	je	.LBB24_2
# BB#1:
	movq	136(%rbx), %rax
	subq	144(%rbx), %rax
	movl	%edx, %ecx
	cmpq	%rax, %rcx
	cmovbel	%edx, %eax
	movl	%eax, %edx
.LBB24_2:
	movq	96(%rbx), %rax
	movl	%edx, %r14d
	movq	%rsi, %rdi
	movq	%rax, %rsi
	movq	%r14, %rdx
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	testl	%eax, %eax
	jne	.LBB24_4
# BB#3:
	addq	%r14, 144(%rbx)
	xorl	%eax, %eax
.LBB24_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end24:
	.size	_ZN12CFilterCoder14WriteWithLimitEP20ISequentialOutStreamj, .Lfunc_end24-_ZN12CFilterCoder14WriteWithLimitEP20ISequentialOutStreamj
	.cfi_endproc

	.globl	_ZN12CFilterCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS5_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN12CFilterCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS5_P21ICompressProgressInfo,@function
_ZN12CFilterCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS5_P21ICompressProgressInfo: # @_ZN12CFilterCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS5_P21ICompressProgressInfo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi74:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi75:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi76:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi77:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi78:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi80:
	.cfi_def_cfa_offset 96
.Lcfi81:
	.cfi_offset %rbx, -56
.Lcfi82:
	.cfi_offset %r12, -48
.Lcfi83:
	.cfi_offset %r13, -40
.Lcfi84:
	.cfi_offset %r14, -32
.Lcfi85:
	.cfi_offset %r15, -24
.Lcfi86:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movq	%rdx, %rbp
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	$0, 144(%r12)
	movb	$0, 132(%r12)
	movq	192(%r12), %rdi
	movq	(%rdi), %rax
	callq	*40(%rax)
	testl	%eax, %eax
	je	.LBB25_1
.LBB25_6:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB25_1:
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%r14, 8(%rsp)           # 8-byte Spill
	leaq	144(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	testq	%r15, %r15
	setne	%al
	setne	132(%r12)
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	je	.LBB25_3
# BB#2:
	movq	(%r15), %rcx
	movq	%rcx, 136(%r12)
	testb	%al, %al
	jne	.LBB25_4
	jmp	.LBB25_7
	.p2align	4, 0x90
.LBB25_39:                              # %_ZN12CFilterCoder14WriteWithLimitEP20ISequentialOutStreamj.exit77.thread84
	movb	132(%r12), %al
	movq	32(%rsp), %rbx          # 8-byte Reload
.LBB25_3:
	testb	%al, %al
	je	.LBB25_7
.LBB25_4:
	movq	144(%r12), %rax
	cmpq	136(%r12), %rax
	jae	.LBB25_5
.LBB25_7:                               # %.critedge
	movl	$131072, %eax           # imm = 0x20000
	subl	%r13d, %eax
	movq	%rax, 24(%rsp)
	movl	%r13d, %esi
	addq	96(%r12), %rsi
	leaq	24(%rsp), %rdx
	movq	%rbx, %rdi
	callq	_Z10ReadStreamP19ISequentialInStreamPvPm
	testl	%eax, %eax
	jne	.LBB25_6
# BB#8:
	addl	24(%rsp), %r13d
	movq	96(%r12), %rsi
	movq	192(%r12), %rdi
	movq	(%rdi), %rax
	movl	%r13d, %edx
	callq	*48(%rax)
	movl	%eax, %r15d
	movl	%r15d, %edx
	subl	%r13d, %edx
	jbe	.LBB25_9
# BB#10:                                # %.lr.ph90.preheader
	movl	%r13d, %eax
	movl	%r15d, %ecx
	leaq	-1(%rcx), %rsi
	subq	%rax, %rsi
	andq	$7, %rdx
	je	.LBB25_11
# BB#12:                                # %.lr.ph90.prol.preheader
	negq	%rdx
	movq	16(%rsp), %r13          # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB25_13:                              # %.lr.ph90.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	96(%r12), %rdi
	movb	$0, (%rdi,%rax)
	incq	%rax
	incq	%rdx
	jne	.LBB25_13
	jmp	.LBB25_14
	.p2align	4, 0x90
.LBB25_9:
	movl	%r15d, %r14d
	movl	%r13d, %r15d
	movq	16(%rsp), %r13          # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	testl	%r14d, %r14d
	jne	.LBB25_24
	jmp	.LBB25_18
.LBB25_11:
	movq	16(%rsp), %r13          # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB25_14:                              # %.lr.ph90.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB25_16
	.p2align	4, 0x90
.LBB25_15:                              # %.lr.ph90
                                        # =>This Inner Loop Header: Depth=1
	movq	96(%r12), %rdx
	movb	$0, (%rdx,%rax)
	movq	96(%r12), %rdx
	movb	$0, 1(%rdx,%rax)
	movq	96(%r12), %rdx
	movb	$0, 2(%rdx,%rax)
	movq	96(%r12), %rdx
	movb	$0, 3(%rdx,%rax)
	movq	96(%r12), %rdx
	movb	$0, 4(%rdx,%rax)
	movq	96(%r12), %rdx
	movb	$0, 5(%rdx,%rax)
	movq	96(%r12), %rdx
	movb	$0, 6(%rdx,%rax)
	movq	96(%r12), %rdx
	movb	$0, 7(%rdx,%rax)
	addq	$8, %rax
	cmpq	%rax, %rcx
	jne	.LBB25_15
.LBB25_16:                              # %._crit_edge
	movq	96(%r12), %rsi
	movq	192(%r12), %rdi
	movq	(%rdi), %rax
	movl	%r15d, %edx
	callq	*48(%rax)
	movl	%eax, %r14d
	testl	%r14d, %r14d
	je	.LBB25_18
.LBB25_24:
	cmpb	$0, 132(%r12)
	movl	%r14d, %eax
	je	.LBB25_26
# BB#25:
	movq	136(%r12), %rax
	subq	144(%r12), %rax
	movl	%r14d, %ecx
	cmpq	%rax, %rcx
	cmovbel	%r14d, %eax
.LBB25_26:
	movq	96(%r12), %rsi
	movl	%eax, %ebp
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbp, %rdx
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	testl	%eax, %eax
	jne	.LBB25_6
# BB#27:
	addq	%rbp, (%r13)
	testq	%rbx, %rbx
	je	.LBB25_29
# BB#28:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%r13, %rdx
	callq	*40(%rax)
	testl	%eax, %eax
	jne	.LBB25_6
.LBB25_29:                              # %.preheader
	xorl	%r13d, %r13d
	movl	%r15d, %r9d
	subl	%r14d, %r9d
	jbe	.LBB25_39
# BB#30:                                # %.lr.ph.preheader
	movl	%r14d, %ecx
	leal	-1(%r15), %r8d
	subl	%r14d, %r8d
	testb	$3, %r9b
	je	.LBB25_31
# BB#32:                                # %.lr.ph.prol.preheader
	movl	%r9d, %edi
	andl	$3, %edi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB25_33:                              # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	96(%r12), %rbp
	leaq	(%rbp,%rcx), %rsi
	movzbl	(%rdx,%rsi), %eax
	movb	%al, (%rbp,%rdx)
	incq	%rdx
	cmpl	%edx, %edi
	jne	.LBB25_33
# BB#34:                                # %.lr.ph.prol.loopexit.unr-lcssa
	addq	%rdx, %rcx
	cmpl	$3, %r8d
	jae	.LBB25_36
	jmp	.LBB25_38
.LBB25_31:
	xorl	%edx, %edx
	cmpl	$3, %r8d
	jb	.LBB25_38
.LBB25_36:                              # %.lr.ph.preheader.new
	subl	%r14d, %r15d
	subl	%edx, %r15d
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB25_37:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	96(%r12), %rax
	leaq	(%rax,%rcx), %rdi
	movzbl	(%rsi,%rdi), %ebx
	addq	%rdx, %rax
	movb	%bl, (%rsi,%rax)
	movq	96(%r12), %rax
	leaq	1(%rax,%rcx), %rdi
	movzbl	(%rsi,%rdi), %ebx
	leaq	1(%rax,%rdx), %rax
	movb	%bl, (%rsi,%rax)
	movq	96(%r12), %rax
	leaq	2(%rax,%rcx), %rdi
	movzbl	(%rsi,%rdi), %ebx
	leaq	2(%rax,%rdx), %rax
	movb	%bl, (%rsi,%rax)
	movq	96(%r12), %rax
	leaq	3(%rax,%rcx), %rdi
	movzbl	(%rsi,%rdi), %ebx
	leaq	3(%rax,%rdx), %rax
	movb	%bl, (%rsi,%rax)
	addq	$4, %rsi
	cmpl	%esi, %r15d
	jne	.LBB25_37
.LBB25_38:
	movl	%r9d, %r13d
	jmp	.LBB25_39
.LBB25_18:
	xorl	%eax, %eax
	testl	%r15d, %r15d
	je	.LBB25_6
# BB#19:
	cmpb	$0, 132(%r12)
	je	.LBB25_20
# BB#21:
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	136(%r12), %rax
	subq	144(%r12), %rax
	movl	%r15d, %ecx
	cmpq	%rax, %rcx
	cmovbel	%r15d, %eax
	movl	%eax, %r15d
	jmp	.LBB25_22
.LBB25_20:
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB25_22:
	movq	96(%r12), %rsi
	movl	%r15d, %ebp
	movq	%rbp, %rdx
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	testl	%eax, %eax
	jne	.LBB25_6
# BB#23:
	addq	%rbp, (%r13)
.LBB25_5:
	xorl	%eax, %eax
	jmp	.LBB25_6
.Lfunc_end25:
	.size	_ZN12CFilterCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS5_P21ICompressProgressInfo, .Lfunc_end25-_ZN12CFilterCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS5_P21ICompressProgressInfo
	.cfi_endproc

	.globl	_ZN12CFilterCoder12SetOutStreamEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZN12CFilterCoder12SetOutStreamEP20ISequentialOutStream,@function
_ZN12CFilterCoder12SetOutStreamEP20ISequentialOutStream: # @_ZN12CFilterCoder12SetOutStreamEP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi89:
	.cfi_def_cfa_offset 32
.Lcfi90:
	.cfi_offset %rbx, -24
.Lcfi91:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$0, 120(%rbx)
	testq	%r14, %r14
	je	.LBB26_2
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
.LBB26_2:
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB26_4:                               # %_ZN9CMyComPtrI20ISequentialOutStreamEaSEPS0_.exit
	movq	%r14, 112(%rbx)
	movq	$0, 144(%rbx)
	movb	$0, 132(%rbx)
	movq	192(%rbx), %rdi
	movq	(%rdi), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*40(%rax)               # TAILCALL
.Lfunc_end26:
	.size	_ZN12CFilterCoder12SetOutStreamEP20ISequentialOutStream, .Lfunc_end26-_ZN12CFilterCoder12SetOutStreamEP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZThn24_N12CFilterCoder12SetOutStreamEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZThn24_N12CFilterCoder12SetOutStreamEP20ISequentialOutStream,@function
_ZThn24_N12CFilterCoder12SetOutStreamEP20ISequentialOutStream: # @_ZThn24_N12CFilterCoder12SetOutStreamEP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi94:
	.cfi_def_cfa_offset 32
.Lcfi95:
	.cfi_offset %rbx, -24
.Lcfi96:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$0, 96(%rbx)
	testq	%r14, %r14
	je	.LBB27_2
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
.LBB27_2:
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB27_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB27_4:                               # %_ZN12CFilterCoder12SetOutStreamEP20ISequentialOutStream.exit
	movq	%r14, 88(%rbx)
	movq	$0, 120(%rbx)
	movb	$0, 108(%rbx)
	movq	168(%rbx), %rdi
	movq	(%rdi), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*40(%rax)               # TAILCALL
.Lfunc_end27:
	.size	_ZThn24_N12CFilterCoder12SetOutStreamEP20ISequentialOutStream, .Lfunc_end27-_ZThn24_N12CFilterCoder12SetOutStreamEP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZN12CFilterCoder16ReleaseOutStreamEv
	.p2align	4, 0x90
	.type	_ZN12CFilterCoder16ReleaseOutStreamEv,@function
_ZN12CFilterCoder16ReleaseOutStreamEv:  # @_ZN12CFilterCoder16ReleaseOutStreamEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 16
.Lcfi98:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB28_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 112(%rbx)
.LBB28_2:                               # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end28:
	.size	_ZN12CFilterCoder16ReleaseOutStreamEv, .Lfunc_end28-_ZN12CFilterCoder16ReleaseOutStreamEv
	.cfi_endproc

	.globl	_ZThn24_N12CFilterCoder16ReleaseOutStreamEv
	.p2align	4, 0x90
	.type	_ZThn24_N12CFilterCoder16ReleaseOutStreamEv,@function
_ZThn24_N12CFilterCoder16ReleaseOutStreamEv: # @_ZThn24_N12CFilterCoder16ReleaseOutStreamEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 16
.Lcfi100:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB29_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 88(%rbx)
.LBB29_2:                               # %_ZN12CFilterCoder16ReleaseOutStreamEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end29:
	.size	_ZThn24_N12CFilterCoder16ReleaseOutStreamEv, .Lfunc_end29-_ZThn24_N12CFilterCoder16ReleaseOutStreamEv
	.cfi_endproc

	.globl	_ZN12CFilterCoder5WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZN12CFilterCoder5WriteEPKvjPj,@function
_ZN12CFilterCoder5WriteEPKvjPj:         # @_ZN12CFilterCoder5WriteEPKvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi104:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi105:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi107:
	.cfi_def_cfa_offset 64
.Lcfi108:
	.cfi_offset %rbx, -56
.Lcfi109:
	.cfi_offset %r12, -48
.Lcfi110:
	.cfi_offset %r13, -40
.Lcfi111:
	.cfi_offset %r14, -32
.Lcfi112:
	.cfi_offset %r15, -24
.Lcfi113:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbp
	movl	%edx, %r15d
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbp, %rbp
	je	.LBB30_2
# BB#1:
	movl	$0, (%rbp)
.LBB30_2:                               # %.preheader
	testl	%r15d, %r15d
	je	.LBB30_25
# BB#3:                                 # %.lr.ph57
	testq	%rbp, %rbp
	movl	120(%rbx), %eax
	je	.LBB30_14
# BB#4:                                 # %.lr.ph57.split.us.preheader
	movq	%rbp, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB30_5:                               # %.lr.ph57.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_12 Depth 2
	movl	$131072, %r13d          # imm = 0x20000
	subl	%eax, %r13d
	cmpl	%r13d, %r15d
	cmovbl	%r15d, %r13d
	movl	%eax, %edi
	addq	96(%rbx), %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	callq	memcpy
	addl	%r13d, (%rbp)
	movl	120(%rbx), %ebp
	addl	%r13d, %ebp
	movq	96(%rbx), %rsi
	movq	192(%rbx), %rdi
	movq	(%rdi), %rax
	movl	%ebp, %edx
	callq	*48(%rax)
	movl	%eax, 120(%rbx)
	testl	%eax, %eax
	je	.LBB30_15
# BB#6:                                 #   in Loop: Header=BB30_5 Depth=1
	subl	%r13d, %r15d
	cmpl	%ebp, %eax
	ja	.LBB30_24
# BB#7:                                 #   in Loop: Header=BB30_5 Depth=1
	movq	112(%rbx), %rdi
	cmpb	$0, 132(%rbx)
	je	.LBB30_9
# BB#8:                                 #   in Loop: Header=BB30_5 Depth=1
	movq	136(%rbx), %rcx
	subq	144(%rbx), %rcx
	movl	%eax, %edx
	cmpq	%rcx, %rdx
	cmovbel	%eax, %ecx
	movl	%ecx, %eax
.LBB30_9:                               #   in Loop: Header=BB30_5 Depth=1
	movq	96(%rbx), %rsi
	movl	%eax, %r12d
	movq	%r12, %rdx
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	testl	%eax, %eax
	jne	.LBB30_26
# BB#10:                                # %_ZN12CFilterCoder14WriteWithLimitEP20ISequentialOutStreamj.exit.thread.us
                                        #   in Loop: Header=BB30_5 Depth=1
	addq	%r12, 144(%rbx)
	movl	120(%rbx), %ecx
	xorl	%eax, %eax
	cmpl	%ebp, %ecx
	jae	.LBB30_13
# BB#11:                                # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB30_5 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB30_12:                              # %.lr.ph.us
                                        #   Parent Loop BB30_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	96(%rbx), %rdx
	leal	1(%rcx), %esi
	movl	%esi, 120(%rbx)
	movl	%ecx, %ecx
	movzbl	(%rdx,%rcx), %ecx
	movl	%eax, %esi
	incl	%eax
	movb	%cl, (%rdx,%rsi)
	movl	120(%rbx), %ecx
	cmpl	%ebp, %ecx
	jb	.LBB30_12
.LBB30_13:                              # %.thread.us
                                        #   in Loop: Header=BB30_5 Depth=1
	addq	%r13, %r14
	movl	%eax, 120(%rbx)
	testl	%r15d, %r15d
	movq	(%rsp), %rbp            # 8-byte Reload
	jne	.LBB30_5
	jmp	.LBB30_25
	.p2align	4, 0x90
.LBB30_14:                              # %.lr.ph57.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_22 Depth 2
	movl	$131072, %r12d          # imm = 0x20000
	subl	%eax, %r12d
	cmpl	%r12d, %r15d
	cmovbl	%r15d, %r12d
	movl	%eax, %edi
	addq	96(%rbx), %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	memcpy
	movl	120(%rbx), %ebp
	addl	%r12d, %ebp
	movq	96(%rbx), %rsi
	movq	192(%rbx), %rdi
	movq	(%rdi), %rax
	movl	%ebp, %edx
	callq	*48(%rax)
	movl	%eax, 120(%rbx)
	testl	%eax, %eax
	je	.LBB30_15
# BB#16:                                #   in Loop: Header=BB30_14 Depth=1
	subl	%r12d, %r15d
	cmpl	%ebp, %eax
	ja	.LBB30_24
# BB#17:                                #   in Loop: Header=BB30_14 Depth=1
	movq	112(%rbx), %rdi
	cmpb	$0, 132(%rbx)
	je	.LBB30_19
# BB#18:                                #   in Loop: Header=BB30_14 Depth=1
	movq	136(%rbx), %rcx
	subq	144(%rbx), %rcx
	movl	%eax, %edx
	cmpq	%rcx, %rdx
	cmovbel	%eax, %ecx
	movl	%ecx, %eax
.LBB30_19:                              #   in Loop: Header=BB30_14 Depth=1
	movq	96(%rbx), %rsi
	movl	%eax, %r13d
	movq	%r13, %rdx
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	testl	%eax, %eax
	jne	.LBB30_26
# BB#20:                                # %_ZN12CFilterCoder14WriteWithLimitEP20ISequentialOutStreamj.exit.thread
                                        #   in Loop: Header=BB30_14 Depth=1
	addq	%r13, 144(%rbx)
	movl	120(%rbx), %ecx
	xorl	%eax, %eax
	cmpl	%ebp, %ecx
	jae	.LBB30_23
# BB#21:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB30_14 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB30_22:                              # %.lr.ph
                                        #   Parent Loop BB30_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	96(%rbx), %rdx
	leal	1(%rcx), %esi
	movl	%esi, 120(%rbx)
	movl	%ecx, %ecx
	movzbl	(%rdx,%rcx), %ecx
	movl	%eax, %esi
	incl	%eax
	movb	%cl, (%rdx,%rsi)
	movl	120(%rbx), %ecx
	cmpl	%ebp, %ecx
	jb	.LBB30_22
.LBB30_23:                              # %.thread
                                        #   in Loop: Header=BB30_14 Depth=1
	addq	%r12, %r14
	movl	%eax, 120(%rbx)
	testl	%r15d, %r15d
	jne	.LBB30_14
	jmp	.LBB30_25
.LBB30_15:                              # %.thread43
	movl	%ebp, 120(%rbx)
	jmp	.LBB30_25
.LBB30_24:                              # %_ZN12CFilterCoder14WriteWithLimitEP20ISequentialOutStreamj.exit
	movl	$-2147467259, %eax      # imm = 0x80004005
	testl	%r15d, %r15d
	jne	.LBB30_26
.LBB30_25:                              # %.loopexit
	xorl	%eax, %eax
.LBB30_26:                              # %_ZN12CFilterCoder14WriteWithLimitEP20ISequentialOutStreamj.exit.thread84
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end30:
	.size	_ZN12CFilterCoder5WriteEPKvjPj, .Lfunc_end30-_ZN12CFilterCoder5WriteEPKvjPj
	.cfi_endproc

	.globl	_ZThn32_N12CFilterCoder5WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZThn32_N12CFilterCoder5WriteEPKvjPj,@function
_ZThn32_N12CFilterCoder5WriteEPKvjPj:   # @_ZThn32_N12CFilterCoder5WriteEPKvjPj
	.cfi_startproc
# BB#0:
	addq	$-32, %rdi
	jmp	_ZN12CFilterCoder5WriteEPKvjPj # TAILCALL
.Lfunc_end31:
	.size	_ZThn32_N12CFilterCoder5WriteEPKvjPj, .Lfunc_end31-_ZThn32_N12CFilterCoder5WriteEPKvjPj
	.cfi_endproc

	.globl	_ZN12CFilterCoder5FlushEv
	.p2align	4, 0x90
	.type	_ZN12CFilterCoder5FlushEv,@function
_ZN12CFilterCoder5FlushEv:              # @_ZN12CFilterCoder5FlushEv
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%rbp
.Lcfi114:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi117:
	.cfi_def_cfa_offset 48
.Lcfi118:
	.cfi_offset %rbx, -32
.Lcfi119:
	.cfi_offset %r14, -24
.Lcfi120:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	120(%rbx), %edx
	testl	%edx, %edx
	je	.LBB32_9
# BB#1:
	movq	96(%rbx), %rsi
	movq	192(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*48(%rax)
	movl	%eax, %ebp
	movl	120(%rbx), %eax
	cmpl	%eax, %ebp
	jbe	.LBB32_5
	.p2align	4, 0x90
.LBB32_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	96(%rbx), %rcx
	movl	%eax, %eax
	movb	$0, (%rcx,%rax)
	movl	120(%rbx), %eax
	incl	%eax
	movl	%eax, 120(%rbx)
	cmpl	%ebp, %eax
	jb	.LBB32_2
# BB#3:                                 # %._crit_edge
	movq	96(%rbx), %rsi
	movq	192(%rbx), %rdi
	movq	(%rdi), %rax
	movl	%ebp, %edx
	callq	*48(%rax)
	movl	$-2147467259, %r14d     # imm = 0x80004005
	cmpl	%ebp, %eax
	jne	.LBB32_18
# BB#4:                                 # %._crit_edge._crit_edge
	movl	120(%rbx), %eax
.LBB32_5:
	movq	112(%rbx), %rdi
	cmpb	$0, 132(%rbx)
	je	.LBB32_7
# BB#6:
	movq	136(%rbx), %rcx
	subq	144(%rbx), %rcx
	movl	%eax, %edx
	cmpq	%rcx, %rdx
	cmovbel	%eax, %ecx
	movl	%ecx, %eax
.LBB32_7:
	movq	96(%rbx), %rsi
	movl	%eax, %ebp
	movq	%rbp, %rdx
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	movl	%eax, %r14d
	testl	%r14d, %r14d
	jne	.LBB32_18
# BB#8:                                 # %.thread
	addq	%rbp, 144(%rbx)
	movl	$0, 120(%rbx)
.LBB32_9:
	movq	$0, 8(%rsp)
	movq	112(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp99:
	leaq	8(%rsp), %rdx
	movl	$IID_IOutStreamFlush, %esi
	callq	*(%rax)
.Ltmp100:
# BB#10:                                # %_ZNK9CMyComPtrI20ISequentialOutStreamE14QueryInterfaceI15IOutStreamFlushEEiRK4GUIDPPT_.exit
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB32_11
# BB#15:
	movq	(%rdi), %rax
.Ltmp101:
	callq	*40(%rax)
	movl	%eax, %r14d
.Ltmp102:
# BB#16:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB32_18
# BB#17:
	movq	(%rdi), %rax
	callq	*16(%rax)
	jmp	.LBB32_18
.LBB32_11:
	xorl	%r14d, %r14d
.LBB32_18:                              # %_ZN12CFilterCoder14WriteWithLimitEP20ISequentialOutStreamj.exit
	movl	%r14d, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB32_12:
.Ltmp103:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB32_14
# BB#13:
	movq	(%rdi), %rax
.Ltmp104:
	callq	*16(%rax)
.Ltmp105:
.LBB32_14:                              # %_ZN9CMyComPtrI15IOutStreamFlushED2Ev.exit16
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB32_19:
.Ltmp106:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end32:
	.size	_ZN12CFilterCoder5FlushEv, .Lfunc_end32-_ZN12CFilterCoder5FlushEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin13-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp99-.Lfunc_begin13  #   Call between .Lfunc_begin13 and .Ltmp99
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp99-.Lfunc_begin13  # >> Call Site 2 <<
	.long	.Ltmp102-.Ltmp99        #   Call between .Ltmp99 and .Ltmp102
	.long	.Ltmp103-.Lfunc_begin13 #     jumps to .Ltmp103
	.byte	0                       #   On action: cleanup
	.long	.Ltmp102-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Ltmp104-.Ltmp102       #   Call between .Ltmp102 and .Ltmp104
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp104-.Lfunc_begin13 # >> Call Site 4 <<
	.long	.Ltmp105-.Ltmp104       #   Call between .Ltmp104 and .Ltmp105
	.long	.Ltmp106-.Lfunc_begin13 #     jumps to .Ltmp106
	.byte	1                       #   On action: 1
	.long	.Ltmp105-.Lfunc_begin13 # >> Call Site 5 <<
	.long	.Lfunc_end32-.Ltmp105   #   Call between .Ltmp105 and .Lfunc_end32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn40_N12CFilterCoder5FlushEv
	.p2align	4, 0x90
	.type	_ZThn40_N12CFilterCoder5FlushEv,@function
_ZThn40_N12CFilterCoder5FlushEv:        # @_ZThn40_N12CFilterCoder5FlushEv
	.cfi_startproc
# BB#0:
	addq	$-40, %rdi
	jmp	_ZN12CFilterCoder5FlushEv # TAILCALL
.Lfunc_end33:
	.size	_ZThn40_N12CFilterCoder5FlushEv, .Lfunc_end33-_ZThn40_N12CFilterCoder5FlushEv
	.cfi_endproc

	.globl	_ZN12CFilterCoder11SetInStreamEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN12CFilterCoder11SetInStreamEP19ISequentialInStream,@function
_ZN12CFilterCoder11SetInStreamEP19ISequentialInStream: # @_ZN12CFilterCoder11SetInStreamEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi121:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi122:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi123:
	.cfi_def_cfa_offset 32
.Lcfi124:
	.cfi_offset %rbx, -24
.Lcfi125:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$0, 128(%rbx)
	movq	$0, 120(%rbx)
	testq	%r14, %r14
	je	.LBB34_2
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
.LBB34_2:
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB34_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB34_4:                               # %_ZN9CMyComPtrI19ISequentialInStreamEaSEPS0_.exit
	movq	%r14, 104(%rbx)
	movq	$0, 144(%rbx)
	movb	$0, 132(%rbx)
	movq	192(%rbx), %rdi
	movq	(%rdi), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*40(%rax)               # TAILCALL
.Lfunc_end34:
	.size	_ZN12CFilterCoder11SetInStreamEP19ISequentialInStream, .Lfunc_end34-_ZN12CFilterCoder11SetInStreamEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZThn8_N12CFilterCoder11SetInStreamEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZThn8_N12CFilterCoder11SetInStreamEP19ISequentialInStream,@function
_ZThn8_N12CFilterCoder11SetInStreamEP19ISequentialInStream: # @_ZThn8_N12CFilterCoder11SetInStreamEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi126:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi127:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi128:
	.cfi_def_cfa_offset 32
.Lcfi129:
	.cfi_offset %rbx, -24
.Lcfi130:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$0, 120(%rbx)
	movq	$0, 112(%rbx)
	testq	%r14, %r14
	je	.LBB35_2
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
.LBB35_2:
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB35_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB35_4:                               # %_ZN12CFilterCoder11SetInStreamEP19ISequentialInStream.exit
	movq	%r14, 96(%rbx)
	movq	$0, 136(%rbx)
	movb	$0, 124(%rbx)
	movq	184(%rbx), %rdi
	movq	(%rdi), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*40(%rax)               # TAILCALL
.Lfunc_end35:
	.size	_ZThn8_N12CFilterCoder11SetInStreamEP19ISequentialInStream, .Lfunc_end35-_ZThn8_N12CFilterCoder11SetInStreamEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZN12CFilterCoder15ReleaseInStreamEv
	.p2align	4, 0x90
	.type	_ZN12CFilterCoder15ReleaseInStreamEv,@function
_ZN12CFilterCoder15ReleaseInStreamEv:   # @_ZN12CFilterCoder15ReleaseInStreamEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi131:
	.cfi_def_cfa_offset 16
.Lcfi132:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB36_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 104(%rbx)
.LBB36_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamE7ReleaseEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end36:
	.size	_ZN12CFilterCoder15ReleaseInStreamEv, .Lfunc_end36-_ZN12CFilterCoder15ReleaseInStreamEv
	.cfi_endproc

	.globl	_ZThn8_N12CFilterCoder15ReleaseInStreamEv
	.p2align	4, 0x90
	.type	_ZThn8_N12CFilterCoder15ReleaseInStreamEv,@function
_ZThn8_N12CFilterCoder15ReleaseInStreamEv: # @_ZThn8_N12CFilterCoder15ReleaseInStreamEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 16
.Lcfi134:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB37_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 96(%rbx)
.LBB37_2:                               # %_ZN12CFilterCoder15ReleaseInStreamEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end37:
	.size	_ZThn8_N12CFilterCoder15ReleaseInStreamEv, .Lfunc_end37-_ZThn8_N12CFilterCoder15ReleaseInStreamEv
	.cfi_endproc

	.globl	_ZN12CFilterCoder4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN12CFilterCoder4ReadEPvjPj,@function
_ZN12CFilterCoder4ReadEPvjPj:           # @_ZN12CFilterCoder4ReadEPvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi135:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi136:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi137:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi138:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi139:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi140:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi141:
	.cfi_def_cfa_offset 80
.Lcfi142:
	.cfi_offset %rbx, -56
.Lcfi143:
	.cfi_offset %r12, -48
.Lcfi144:
	.cfi_offset %r13, -40
.Lcfi145:
	.cfi_offset %r14, -32
.Lcfi146:
	.cfi_offset %r15, -24
.Lcfi147:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movl	%edx, %r12d
	movq	%rdi, %rbp
	testq	%rbx, %rbx
	je	.LBB38_2
# BB#1:
	movl	$0, (%rbx)
.LBB38_2:                               # %.preheader34
	xorl	%r14d, %r14d
	testl	%r12d, %r12d
	je	.LBB38_20
# BB#3:                                 # %.lr.ph55
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	124(%rbp), %eax
	movl	128(%rbp), %r15d
	cmpl	%eax, %r15d
	jne	.LBB38_12
# BB#4:                                 # %.preheader33.preheader
	leaq	8(%rsp), %r13
	.p2align	4, 0x90
.LBB38_5:                               # %.preheader33
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB38_7 Depth 2
                                        #     Child Loop BB38_15 Depth 2
	xorl	%eax, %eax
	cmpl	120(%rbp), %r15d
	jae	.LBB38_8
# BB#6:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB38_5 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB38_7:                               # %.lr.ph
                                        #   Parent Loop BB38_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	96(%rbp), %rcx
	movl	%r15d, %edx
	movzbl	(%rcx,%rdx), %edx
	movl	%eax, %esi
	movb	%dl, (%rcx,%rsi)
	incl	%eax
	movl	128(%rbp), %r15d
	addl	%eax, %r15d
	cmpl	120(%rbp), %r15d
	jb	.LBB38_7
.LBB38_8:                               # %._crit_edge
                                        #   in Loop: Header=BB38_5 Depth=1
	movl	%eax, 120(%rbp)
	movl	$0, 128(%rbp)
	movl	$0, 124(%rbp)
	movl	$131072, %ecx           # imm = 0x20000
	subl	%eax, %ecx
	movq	%rcx, 8(%rsp)
	movq	104(%rbp), %rdi
	movl	%eax, %esi
	addq	96(%rbp), %rsi
	movq	%r13, %rdx
	callq	_Z10ReadStreamP19ISequentialInStreamPvPm
	testl	%eax, %eax
	jne	.LBB38_19
# BB#9:                                 #   in Loop: Header=BB38_5 Depth=1
	movl	120(%rbp), %edx
	addl	8(%rsp), %edx
	movl	%edx, 120(%rbp)
	movq	96(%rbp), %rsi
	movq	192(%rbp), %rdi
	movq	(%rdi), %rax
	callq	*48(%rax)
	movl	%eax, %r15d
	movl	%r15d, 128(%rbp)
	movl	120(%rbp), %eax
	testl	%r15d, %r15d
	je	.LBB38_10
# BB#14:                                #   in Loop: Header=BB38_5 Depth=1
	cmpl	%eax, %r15d
	jbe	.LBB38_18
	.p2align	4, 0x90
.LBB38_15:                              # %.lr.ph51
                                        #   Parent Loop BB38_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	96(%rbp), %rcx
	movl	%eax, %eax
	movb	$0, (%rcx,%rax)
	movl	120(%rbp), %eax
	incl	%eax
	movl	%eax, 120(%rbp)
	cmpl	128(%rbp), %eax
	jb	.LBB38_15
# BB#16:                                # %._crit_edge52
                                        #   in Loop: Header=BB38_5 Depth=1
	movq	96(%rbp), %rsi
	movq	192(%rbp), %rdi
	movq	(%rdi), %rcx
	movl	%eax, %edx
	callq	*48(%rcx)
	jmp	.LBB38_17
	.p2align	4, 0x90
.LBB38_10:                              #   in Loop: Header=BB38_5 Depth=1
	testl	%eax, %eax
	je	.LBB38_11
.LBB38_17:                              # %.thread.sink.split
                                        #   in Loop: Header=BB38_5 Depth=1
	movl	%eax, 128(%rbp)
	movl	%eax, %r15d
.LBB38_18:                              # %.thread
                                        #   in Loop: Header=BB38_5 Depth=1
	movl	124(%rbp), %eax
	cmpl	%eax, %r15d
	je	.LBB38_5
.LBB38_12:                              # %.us-lcssa
	subl	%eax, %r15d
	cmpl	%r12d, %r15d
	cmoval	%r12d, %r15d
	movl	%eax, %esi
	addq	96(%rbp), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r15, %rdx
	callq	memcpy
	addl	%r15d, 124(%rbp)
	testq	%rbx, %rbx
	je	.LBB38_20
# BB#13:
	addl	%r15d, (%rbx)
	jmp	.LBB38_20
.LBB38_19:                              # %.us-lcssa56.us
	movl	%eax, %r14d
	jmp	.LBB38_20
.LBB38_11:                              # %.thread32
	xorl	%r14d, %r14d
.LBB38_20:                              # %.loopexit
	movl	%r14d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end38:
	.size	_ZN12CFilterCoder4ReadEPvjPj, .Lfunc_end38-_ZN12CFilterCoder4ReadEPvjPj
	.cfi_endproc

	.globl	_ZThn16_N12CFilterCoder4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZThn16_N12CFilterCoder4ReadEPvjPj,@function
_ZThn16_N12CFilterCoder4ReadEPvjPj:     # @_ZThn16_N12CFilterCoder4ReadEPvjPj
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN12CFilterCoder4ReadEPvjPj # TAILCALL
.Lfunc_end39:
	.size	_ZThn16_N12CFilterCoder4ReadEPvjPj, .Lfunc_end39-_ZThn16_N12CFilterCoder4ReadEPvjPj
	.cfi_endproc

	.globl	_ZN12CFilterCoder17CryptoSetPasswordEPKhj
	.p2align	4, 0x90
	.type	_ZN12CFilterCoder17CryptoSetPasswordEPKhj,@function
_ZN12CFilterCoder17CryptoSetPasswordEPKhj: # @_ZN12CFilterCoder17CryptoSetPasswordEPKhj
	.cfi_startproc
# BB#0:
	movq	152(%rdi), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end40:
	.size	_ZN12CFilterCoder17CryptoSetPasswordEPKhj, .Lfunc_end40-_ZN12CFilterCoder17CryptoSetPasswordEPKhj
	.cfi_endproc

	.globl	_ZThn48_N12CFilterCoder17CryptoSetPasswordEPKhj
	.p2align	4, 0x90
	.type	_ZThn48_N12CFilterCoder17CryptoSetPasswordEPKhj,@function
_ZThn48_N12CFilterCoder17CryptoSetPasswordEPKhj: # @_ZThn48_N12CFilterCoder17CryptoSetPasswordEPKhj
	.cfi_startproc
# BB#0:
	movq	104(%rdi), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end41:
	.size	_ZThn48_N12CFilterCoder17CryptoSetPasswordEPKhj, .Lfunc_end41-_ZThn48_N12CFilterCoder17CryptoSetPasswordEPKhj
	.cfi_endproc

	.globl	_ZN12CFilterCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.p2align	4, 0x90
	.type	_ZN12CFilterCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj,@function
_ZN12CFilterCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj: # @_ZN12CFilterCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_startproc
# BB#0:
	movq	160(%rdi), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end42:
	.size	_ZN12CFilterCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj, .Lfunc_end42-_ZN12CFilterCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_endproc

	.globl	_ZThn56_N12CFilterCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.p2align	4, 0x90
	.type	_ZThn56_N12CFilterCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj,@function
_ZThn56_N12CFilterCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj: # @_ZThn56_N12CFilterCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_startproc
# BB#0:
	movq	104(%rdi), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end43:
	.size	_ZThn56_N12CFilterCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj, .Lfunc_end43-_ZThn56_N12CFilterCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_endproc

	.globl	_ZN12CFilterCoder20WriteCoderPropertiesEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZN12CFilterCoder20WriteCoderPropertiesEP20ISequentialOutStream,@function
_ZN12CFilterCoder20WriteCoderPropertiesEP20ISequentialOutStream: # @_ZN12CFilterCoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	movq	168(%rdi), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end44:
	.size	_ZN12CFilterCoder20WriteCoderPropertiesEP20ISequentialOutStream, .Lfunc_end44-_ZN12CFilterCoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZThn64_N12CFilterCoder20WriteCoderPropertiesEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZThn64_N12CFilterCoder20WriteCoderPropertiesEP20ISequentialOutStream,@function
_ZThn64_N12CFilterCoder20WriteCoderPropertiesEP20ISequentialOutStream: # @_ZThn64_N12CFilterCoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	movq	104(%rdi), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end45:
	.size	_ZThn64_N12CFilterCoder20WriteCoderPropertiesEP20ISequentialOutStream, .Lfunc_end45-_ZThn64_N12CFilterCoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZN12CFilterCoder15ResetInitVectorEv
	.p2align	4, 0x90
	.type	_ZN12CFilterCoder15ResetInitVectorEv,@function
_ZN12CFilterCoder15ResetInitVectorEv:   # @_ZN12CFilterCoder15ResetInitVectorEv
	.cfi_startproc
# BB#0:
	movq	176(%rdi), %rdi
	movq	(%rdi), %rax
	jmpq	*40(%rax)               # TAILCALL
.Lfunc_end46:
	.size	_ZN12CFilterCoder15ResetInitVectorEv, .Lfunc_end46-_ZN12CFilterCoder15ResetInitVectorEv
	.cfi_endproc

	.globl	_ZThn72_N12CFilterCoder15ResetInitVectorEv
	.p2align	4, 0x90
	.type	_ZThn72_N12CFilterCoder15ResetInitVectorEv,@function
_ZThn72_N12CFilterCoder15ResetInitVectorEv: # @_ZThn72_N12CFilterCoder15ResetInitVectorEv
	.cfi_startproc
# BB#0:
	movq	104(%rdi), %rdi
	movq	(%rdi), %rax
	jmpq	*40(%rax)               # TAILCALL
.Lfunc_end47:
	.size	_ZThn72_N12CFilterCoder15ResetInitVectorEv, .Lfunc_end47-_ZThn72_N12CFilterCoder15ResetInitVectorEv
	.cfi_endproc

	.globl	_ZN12CFilterCoder21SetDecoderProperties2EPKhj
	.p2align	4, 0x90
	.type	_ZN12CFilterCoder21SetDecoderProperties2EPKhj,@function
_ZN12CFilterCoder21SetDecoderProperties2EPKhj: # @_ZN12CFilterCoder21SetDecoderProperties2EPKhj
	.cfi_startproc
# BB#0:
	movq	184(%rdi), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end48:
	.size	_ZN12CFilterCoder21SetDecoderProperties2EPKhj, .Lfunc_end48-_ZN12CFilterCoder21SetDecoderProperties2EPKhj
	.cfi_endproc

	.globl	_ZThn80_N12CFilterCoder21SetDecoderProperties2EPKhj
	.p2align	4, 0x90
	.type	_ZThn80_N12CFilterCoder21SetDecoderProperties2EPKhj,@function
_ZThn80_N12CFilterCoder21SetDecoderProperties2EPKhj: # @_ZThn80_N12CFilterCoder21SetDecoderProperties2EPKhj
	.cfi_startproc
# BB#0:
	movq	104(%rdi), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end49:
	.size	_ZThn80_N12CFilterCoder21SetDecoderProperties2EPKhj, .Lfunc_end49-_ZThn80_N12CFilterCoder21SetDecoderProperties2EPKhj
	.cfi_endproc

	.section	.text._ZN12CFilterCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN12CFilterCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN12CFilterCoder14QueryInterfaceERK4GUIDPPv,@function
_ZN12CFilterCoder14QueryInterfaceERK4GUIDPPv: # @_ZN12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi148:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi149:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi150:
	.cfi_def_cfa_offset 32
.Lcfi151:
	.cfi_offset %rbx, -24
.Lcfi152:
	.cfi_offset %r14, -16
	movq	%rdx, %rbx
	movq	%rdi, %r14
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB50_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB50_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB50_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB50_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB50_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB50_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB50_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB50_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB50_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB50_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB50_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB50_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB50_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB50_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB50_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB50_16
.LBB50_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_ICompressCoder(%rip), %cl
	jne	.LBB50_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_ICompressCoder+1(%rip), %al
	jne	.LBB50_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_ICompressCoder+2(%rip), %al
	jne	.LBB50_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_ICompressCoder+3(%rip), %al
	jne	.LBB50_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_ICompressCoder+4(%rip), %al
	jne	.LBB50_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_ICompressCoder+5(%rip), %al
	jne	.LBB50_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_ICompressCoder+6(%rip), %al
	jne	.LBB50_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_ICompressCoder+7(%rip), %al
	jne	.LBB50_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_ICompressCoder+8(%rip), %al
	jne	.LBB50_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_ICompressCoder+9(%rip), %al
	jne	.LBB50_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_ICompressCoder+10(%rip), %al
	jne	.LBB50_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_ICompressCoder+11(%rip), %al
	jne	.LBB50_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_ICompressCoder+12(%rip), %al
	jne	.LBB50_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_ICompressCoder+13(%rip), %al
	jne	.LBB50_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_ICompressCoder+14(%rip), %al
	jne	.LBB50_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit63
	movb	15(%rsi), %al
	cmpb	IID_ICompressCoder+15(%rip), %al
	jne	.LBB50_33
.LBB50_16:
	movq	%r14, (%rbx)
	jmp	.LBB50_214
.LBB50_33:                              # %_ZeqRK4GUIDS1_.exit63.thread
	cmpb	IID_ICompressSetInStream(%rip), %cl
	jne	.LBB50_50
# BB#34:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetInStream+1(%rip), %al
	jne	.LBB50_50
# BB#35:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetInStream+2(%rip), %al
	jne	.LBB50_50
# BB#36:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetInStream+3(%rip), %al
	jne	.LBB50_50
# BB#37:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetInStream+4(%rip), %al
	jne	.LBB50_50
# BB#38:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetInStream+5(%rip), %al
	jne	.LBB50_50
# BB#39:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetInStream+6(%rip), %al
	jne	.LBB50_50
# BB#40:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetInStream+7(%rip), %al
	jne	.LBB50_50
# BB#41:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetInStream+8(%rip), %al
	jne	.LBB50_50
# BB#42:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetInStream+9(%rip), %al
	jne	.LBB50_50
# BB#43:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetInStream+10(%rip), %al
	jne	.LBB50_50
# BB#44:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetInStream+11(%rip), %al
	jne	.LBB50_50
# BB#45:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetInStream+12(%rip), %al
	jne	.LBB50_50
# BB#46:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetInStream+13(%rip), %al
	jne	.LBB50_50
# BB#47:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetInStream+14(%rip), %al
	jne	.LBB50_50
# BB#48:                                # %_ZeqRK4GUIDS1_.exit65
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetInStream+15(%rip), %al
	jne	.LBB50_50
# BB#49:
	leaq	8(%r14), %rax
	jmp	.LBB50_213
.LBB50_50:                              # %_ZeqRK4GUIDS1_.exit65.thread
	cmpb	IID_ISequentialInStream(%rip), %cl
	jne	.LBB50_67
# BB#51:
	movb	1(%rsi), %al
	cmpb	IID_ISequentialInStream+1(%rip), %al
	jne	.LBB50_67
# BB#52:
	movb	2(%rsi), %al
	cmpb	IID_ISequentialInStream+2(%rip), %al
	jne	.LBB50_67
# BB#53:
	movb	3(%rsi), %al
	cmpb	IID_ISequentialInStream+3(%rip), %al
	jne	.LBB50_67
# BB#54:
	movb	4(%rsi), %al
	cmpb	IID_ISequentialInStream+4(%rip), %al
	jne	.LBB50_67
# BB#55:
	movb	5(%rsi), %al
	cmpb	IID_ISequentialInStream+5(%rip), %al
	jne	.LBB50_67
# BB#56:
	movb	6(%rsi), %al
	cmpb	IID_ISequentialInStream+6(%rip), %al
	jne	.LBB50_67
# BB#57:
	movb	7(%rsi), %al
	cmpb	IID_ISequentialInStream+7(%rip), %al
	jne	.LBB50_67
# BB#58:
	movb	8(%rsi), %al
	cmpb	IID_ISequentialInStream+8(%rip), %al
	jne	.LBB50_67
# BB#59:
	movb	9(%rsi), %al
	cmpb	IID_ISequentialInStream+9(%rip), %al
	jne	.LBB50_67
# BB#60:
	movb	10(%rsi), %al
	cmpb	IID_ISequentialInStream+10(%rip), %al
	jne	.LBB50_67
# BB#61:
	movb	11(%rsi), %al
	cmpb	IID_ISequentialInStream+11(%rip), %al
	jne	.LBB50_67
# BB#62:
	movb	12(%rsi), %al
	cmpb	IID_ISequentialInStream+12(%rip), %al
	jne	.LBB50_67
# BB#63:
	movb	13(%rsi), %al
	cmpb	IID_ISequentialInStream+13(%rip), %al
	jne	.LBB50_67
# BB#64:
	movb	14(%rsi), %al
	cmpb	IID_ISequentialInStream+14(%rip), %al
	jne	.LBB50_67
# BB#65:                                # %_ZeqRK4GUIDS1_.exit69
	movb	15(%rsi), %al
	cmpb	IID_ISequentialInStream+15(%rip), %al
	jne	.LBB50_67
# BB#66:
	leaq	16(%r14), %rax
	jmp	.LBB50_213
.LBB50_67:                              # %_ZeqRK4GUIDS1_.exit69.thread
	cmpb	IID_ICompressSetOutStream(%rip), %cl
	jne	.LBB50_84
# BB#68:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetOutStream+1(%rip), %al
	jne	.LBB50_84
# BB#69:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetOutStream+2(%rip), %al
	jne	.LBB50_84
# BB#70:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetOutStream+3(%rip), %al
	jne	.LBB50_84
# BB#71:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetOutStream+4(%rip), %al
	jne	.LBB50_84
# BB#72:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetOutStream+5(%rip), %al
	jne	.LBB50_84
# BB#73:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetOutStream+6(%rip), %al
	jne	.LBB50_84
# BB#74:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetOutStream+7(%rip), %al
	jne	.LBB50_84
# BB#75:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetOutStream+8(%rip), %al
	jne	.LBB50_84
# BB#76:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetOutStream+9(%rip), %al
	jne	.LBB50_84
# BB#77:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetOutStream+10(%rip), %al
	jne	.LBB50_84
# BB#78:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetOutStream+11(%rip), %al
	jne	.LBB50_84
# BB#79:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetOutStream+12(%rip), %al
	jne	.LBB50_84
# BB#80:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetOutStream+13(%rip), %al
	jne	.LBB50_84
# BB#81:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetOutStream+14(%rip), %al
	jne	.LBB50_84
# BB#82:                                # %_ZeqRK4GUIDS1_.exit71
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetOutStream+15(%rip), %al
	jne	.LBB50_84
# BB#83:
	leaq	24(%r14), %rax
	jmp	.LBB50_213
.LBB50_84:                              # %_ZeqRK4GUIDS1_.exit71.thread
	cmpb	IID_ISequentialOutStream(%rip), %cl
	jne	.LBB50_101
# BB#85:
	movb	1(%rsi), %al
	cmpb	IID_ISequentialOutStream+1(%rip), %al
	jne	.LBB50_101
# BB#86:
	movb	2(%rsi), %al
	cmpb	IID_ISequentialOutStream+2(%rip), %al
	jne	.LBB50_101
# BB#87:
	movb	3(%rsi), %al
	cmpb	IID_ISequentialOutStream+3(%rip), %al
	jne	.LBB50_101
# BB#88:
	movb	4(%rsi), %al
	cmpb	IID_ISequentialOutStream+4(%rip), %al
	jne	.LBB50_101
# BB#89:
	movb	5(%rsi), %al
	cmpb	IID_ISequentialOutStream+5(%rip), %al
	jne	.LBB50_101
# BB#90:
	movb	6(%rsi), %al
	cmpb	IID_ISequentialOutStream+6(%rip), %al
	jne	.LBB50_101
# BB#91:
	movb	7(%rsi), %al
	cmpb	IID_ISequentialOutStream+7(%rip), %al
	jne	.LBB50_101
# BB#92:
	movb	8(%rsi), %al
	cmpb	IID_ISequentialOutStream+8(%rip), %al
	jne	.LBB50_101
# BB#93:
	movb	9(%rsi), %al
	cmpb	IID_ISequentialOutStream+9(%rip), %al
	jne	.LBB50_101
# BB#94:
	movb	10(%rsi), %al
	cmpb	IID_ISequentialOutStream+10(%rip), %al
	jne	.LBB50_101
# BB#95:
	movb	11(%rsi), %al
	cmpb	IID_ISequentialOutStream+11(%rip), %al
	jne	.LBB50_101
# BB#96:
	movb	12(%rsi), %al
	cmpb	IID_ISequentialOutStream+12(%rip), %al
	jne	.LBB50_101
# BB#97:
	movb	13(%rsi), %al
	cmpb	IID_ISequentialOutStream+13(%rip), %al
	jne	.LBB50_101
# BB#98:
	movb	14(%rsi), %al
	cmpb	IID_ISequentialOutStream+14(%rip), %al
	jne	.LBB50_101
# BB#99:                                # %_ZeqRK4GUIDS1_.exit75
	movb	15(%rsi), %al
	cmpb	IID_ISequentialOutStream+15(%rip), %al
	jne	.LBB50_101
# BB#100:
	leaq	32(%r14), %rax
	jmp	.LBB50_213
.LBB50_101:                             # %_ZeqRK4GUIDS1_.exit75.thread
	cmpb	IID_IOutStreamFlush(%rip), %cl
	jne	.LBB50_118
# BB#102:
	movb	1(%rsi), %al
	cmpb	IID_IOutStreamFlush+1(%rip), %al
	jne	.LBB50_118
# BB#103:
	movb	2(%rsi), %al
	cmpb	IID_IOutStreamFlush+2(%rip), %al
	jne	.LBB50_118
# BB#104:
	movb	3(%rsi), %al
	cmpb	IID_IOutStreamFlush+3(%rip), %al
	jne	.LBB50_118
# BB#105:
	movb	4(%rsi), %al
	cmpb	IID_IOutStreamFlush+4(%rip), %al
	jne	.LBB50_118
# BB#106:
	movb	5(%rsi), %al
	cmpb	IID_IOutStreamFlush+5(%rip), %al
	jne	.LBB50_118
# BB#107:
	movb	6(%rsi), %al
	cmpb	IID_IOutStreamFlush+6(%rip), %al
	jne	.LBB50_118
# BB#108:
	movb	7(%rsi), %al
	cmpb	IID_IOutStreamFlush+7(%rip), %al
	jne	.LBB50_118
# BB#109:
	movb	8(%rsi), %al
	cmpb	IID_IOutStreamFlush+8(%rip), %al
	jne	.LBB50_118
# BB#110:
	movb	9(%rsi), %al
	cmpb	IID_IOutStreamFlush+9(%rip), %al
	jne	.LBB50_118
# BB#111:
	movb	10(%rsi), %al
	cmpb	IID_IOutStreamFlush+10(%rip), %al
	jne	.LBB50_118
# BB#112:
	movb	11(%rsi), %al
	cmpb	IID_IOutStreamFlush+11(%rip), %al
	jne	.LBB50_118
# BB#113:
	movb	12(%rsi), %al
	cmpb	IID_IOutStreamFlush+12(%rip), %al
	jne	.LBB50_118
# BB#114:
	movb	13(%rsi), %al
	cmpb	IID_IOutStreamFlush+13(%rip), %al
	jne	.LBB50_118
# BB#115:
	movb	14(%rsi), %al
	cmpb	IID_IOutStreamFlush+14(%rip), %al
	jne	.LBB50_118
# BB#116:                               # %_ZeqRK4GUIDS1_.exit77
	movb	15(%rsi), %al
	cmpb	IID_IOutStreamFlush+15(%rip), %al
	jne	.LBB50_118
# BB#117:
	leaq	40(%r14), %rax
	jmp	.LBB50_213
.LBB50_118:                             # %_ZeqRK4GUIDS1_.exit77.thread
	cmpb	IID_ICryptoSetPassword(%rip), %cl
	jne	.LBB50_137
# BB#119:
	movb	1(%rsi), %al
	cmpb	IID_ICryptoSetPassword+1(%rip), %al
	jne	.LBB50_137
# BB#120:
	movb	2(%rsi), %al
	cmpb	IID_ICryptoSetPassword+2(%rip), %al
	jne	.LBB50_137
# BB#121:
	movb	3(%rsi), %al
	cmpb	IID_ICryptoSetPassword+3(%rip), %al
	jne	.LBB50_137
# BB#122:
	movb	4(%rsi), %al
	cmpb	IID_ICryptoSetPassword+4(%rip), %al
	jne	.LBB50_137
# BB#123:
	movb	5(%rsi), %al
	cmpb	IID_ICryptoSetPassword+5(%rip), %al
	jne	.LBB50_137
# BB#124:
	movb	6(%rsi), %al
	cmpb	IID_ICryptoSetPassword+6(%rip), %al
	jne	.LBB50_137
# BB#125:
	movb	7(%rsi), %al
	cmpb	IID_ICryptoSetPassword+7(%rip), %al
	jne	.LBB50_137
# BB#126:
	movb	8(%rsi), %al
	cmpb	IID_ICryptoSetPassword+8(%rip), %al
	jne	.LBB50_137
# BB#127:
	movb	9(%rsi), %al
	cmpb	IID_ICryptoSetPassword+9(%rip), %al
	jne	.LBB50_137
# BB#128:
	movb	10(%rsi), %al
	cmpb	IID_ICryptoSetPassword+10(%rip), %al
	jne	.LBB50_137
# BB#129:
	movb	11(%rsi), %al
	cmpb	IID_ICryptoSetPassword+11(%rip), %al
	jne	.LBB50_137
# BB#130:
	movb	12(%rsi), %al
	cmpb	IID_ICryptoSetPassword+12(%rip), %al
	jne	.LBB50_137
# BB#131:
	movb	13(%rsi), %al
	cmpb	IID_ICryptoSetPassword+13(%rip), %al
	jne	.LBB50_137
# BB#132:
	movb	14(%rsi), %al
	cmpb	IID_ICryptoSetPassword+14(%rip), %al
	jne	.LBB50_137
# BB#133:                               # %_ZeqRK4GUIDS1_.exit81
	movb	15(%rsi), %al
	cmpb	IID_ICryptoSetPassword+15(%rip), %al
	jne	.LBB50_137
# BB#134:
	cmpq	$0, 152(%r14)
	jne	.LBB50_136
# BB#135:
	leaq	152(%r14), %rdx
	movq	192(%r14), %rdi
	movq	(%rdi), %rax
	movl	$IID_ICryptoSetPassword, %esi
	callq	*(%rax)
	testl	%eax, %eax
	jne	.LBB50_215
.LBB50_136:
	leaq	48(%r14), %rax
	jmp	.LBB50_213
.LBB50_137:                             # %_ZeqRK4GUIDS1_.exit81.thread
	cmpb	IID_ICompressSetCoderProperties(%rip), %cl
	jne	.LBB50_156
# BB#138:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+1(%rip), %al
	jne	.LBB50_156
# BB#139:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+2(%rip), %al
	jne	.LBB50_156
# BB#140:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+3(%rip), %al
	jne	.LBB50_156
# BB#141:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+4(%rip), %al
	jne	.LBB50_156
# BB#142:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+5(%rip), %al
	jne	.LBB50_156
# BB#143:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+6(%rip), %al
	jne	.LBB50_156
# BB#144:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+7(%rip), %al
	jne	.LBB50_156
# BB#145:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+8(%rip), %al
	jne	.LBB50_156
# BB#146:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+9(%rip), %al
	jne	.LBB50_156
# BB#147:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+10(%rip), %al
	jne	.LBB50_156
# BB#148:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+11(%rip), %al
	jne	.LBB50_156
# BB#149:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+12(%rip), %al
	jne	.LBB50_156
# BB#150:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+13(%rip), %al
	jne	.LBB50_156
# BB#151:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+14(%rip), %al
	jne	.LBB50_156
# BB#152:                               # %_ZeqRK4GUIDS1_.exit83
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+15(%rip), %al
	jne	.LBB50_156
# BB#153:
	cmpq	$0, 160(%r14)
	jne	.LBB50_155
# BB#154:
	leaq	160(%r14), %rdx
	movq	192(%r14), %rdi
	movq	(%rdi), %rax
	movl	$IID_ICompressSetCoderProperties, %esi
	callq	*(%rax)
	testl	%eax, %eax
	jne	.LBB50_215
.LBB50_155:
	leaq	56(%r14), %rax
	jmp	.LBB50_213
.LBB50_156:                             # %_ZeqRK4GUIDS1_.exit83.thread
	cmpb	IID_ICompressWriteCoderProperties(%rip), %cl
	jne	.LBB50_175
# BB#157:
	movb	1(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+1(%rip), %al
	jne	.LBB50_175
# BB#158:
	movb	2(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+2(%rip), %al
	jne	.LBB50_175
# BB#159:
	movb	3(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+3(%rip), %al
	jne	.LBB50_175
# BB#160:
	movb	4(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+4(%rip), %al
	jne	.LBB50_175
# BB#161:
	movb	5(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+5(%rip), %al
	jne	.LBB50_175
# BB#162:
	movb	6(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+6(%rip), %al
	jne	.LBB50_175
# BB#163:
	movb	7(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+7(%rip), %al
	jne	.LBB50_175
# BB#164:
	movb	8(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+8(%rip), %al
	jne	.LBB50_175
# BB#165:
	movb	9(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+9(%rip), %al
	jne	.LBB50_175
# BB#166:
	movb	10(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+10(%rip), %al
	jne	.LBB50_175
# BB#167:
	movb	11(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+11(%rip), %al
	jne	.LBB50_175
# BB#168:
	movb	12(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+12(%rip), %al
	jne	.LBB50_175
# BB#169:
	movb	13(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+13(%rip), %al
	jne	.LBB50_175
# BB#170:
	movb	14(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+14(%rip), %al
	jne	.LBB50_175
# BB#171:                               # %_ZeqRK4GUIDS1_.exit79
	movb	15(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+15(%rip), %al
	jne	.LBB50_175
# BB#172:
	cmpq	$0, 168(%r14)
	jne	.LBB50_174
# BB#173:
	leaq	168(%r14), %rdx
	movq	192(%r14), %rdi
	movq	(%rdi), %rax
	movl	$IID_ICompressWriteCoderProperties, %esi
	callq	*(%rax)
	testl	%eax, %eax
	jne	.LBB50_215
.LBB50_174:
	leaq	64(%r14), %rax
	jmp	.LBB50_213
.LBB50_175:                             # %_ZeqRK4GUIDS1_.exit79.thread
	cmpb	IID_ICryptoResetInitVector(%rip), %cl
	jne	.LBB50_194
# BB#176:
	movb	1(%rsi), %al
	cmpb	IID_ICryptoResetInitVector+1(%rip), %al
	jne	.LBB50_194
# BB#177:
	movb	2(%rsi), %al
	cmpb	IID_ICryptoResetInitVector+2(%rip), %al
	jne	.LBB50_194
# BB#178:
	movb	3(%rsi), %al
	cmpb	IID_ICryptoResetInitVector+3(%rip), %al
	jne	.LBB50_194
# BB#179:
	movb	4(%rsi), %al
	cmpb	IID_ICryptoResetInitVector+4(%rip), %al
	jne	.LBB50_194
# BB#180:
	movb	5(%rsi), %al
	cmpb	IID_ICryptoResetInitVector+5(%rip), %al
	jne	.LBB50_194
# BB#181:
	movb	6(%rsi), %al
	cmpb	IID_ICryptoResetInitVector+6(%rip), %al
	jne	.LBB50_194
# BB#182:
	movb	7(%rsi), %al
	cmpb	IID_ICryptoResetInitVector+7(%rip), %al
	jne	.LBB50_194
# BB#183:
	movb	8(%rsi), %al
	cmpb	IID_ICryptoResetInitVector+8(%rip), %al
	jne	.LBB50_194
# BB#184:
	movb	9(%rsi), %al
	cmpb	IID_ICryptoResetInitVector+9(%rip), %al
	jne	.LBB50_194
# BB#185:
	movb	10(%rsi), %al
	cmpb	IID_ICryptoResetInitVector+10(%rip), %al
	jne	.LBB50_194
# BB#186:
	movb	11(%rsi), %al
	cmpb	IID_ICryptoResetInitVector+11(%rip), %al
	jne	.LBB50_194
# BB#187:
	movb	12(%rsi), %al
	cmpb	IID_ICryptoResetInitVector+12(%rip), %al
	jne	.LBB50_194
# BB#188:
	movb	13(%rsi), %al
	cmpb	IID_ICryptoResetInitVector+13(%rip), %al
	jne	.LBB50_194
# BB#189:
	movb	14(%rsi), %al
	cmpb	IID_ICryptoResetInitVector+14(%rip), %al
	jne	.LBB50_194
# BB#190:                               # %_ZeqRK4GUIDS1_.exit73
	movb	15(%rsi), %al
	cmpb	IID_ICryptoResetInitVector+15(%rip), %al
	jne	.LBB50_194
# BB#191:
	cmpq	$0, 176(%r14)
	jne	.LBB50_193
# BB#192:
	leaq	176(%r14), %rdx
	movq	192(%r14), %rdi
	movq	(%rdi), %rax
	movl	$IID_ICryptoResetInitVector, %esi
	callq	*(%rax)
	testl	%eax, %eax
	jne	.LBB50_215
.LBB50_193:
	leaq	72(%r14), %rax
	jmp	.LBB50_213
.LBB50_194:                             # %_ZeqRK4GUIDS1_.exit73.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressSetDecoderProperties2(%rip), %cl
	jne	.LBB50_215
# BB#195:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+1(%rip), %cl
	jne	.LBB50_215
# BB#196:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+2(%rip), %cl
	jne	.LBB50_215
# BB#197:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+3(%rip), %cl
	jne	.LBB50_215
# BB#198:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+4(%rip), %cl
	jne	.LBB50_215
# BB#199:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+5(%rip), %cl
	jne	.LBB50_215
# BB#200:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+6(%rip), %cl
	jne	.LBB50_215
# BB#201:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+7(%rip), %cl
	jne	.LBB50_215
# BB#202:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+8(%rip), %cl
	jne	.LBB50_215
# BB#203:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+9(%rip), %cl
	jne	.LBB50_215
# BB#204:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+10(%rip), %cl
	jne	.LBB50_215
# BB#205:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+11(%rip), %cl
	jne	.LBB50_215
# BB#206:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+12(%rip), %cl
	jne	.LBB50_215
# BB#207:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+13(%rip), %cl
	jne	.LBB50_215
# BB#208:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+14(%rip), %cl
	jne	.LBB50_215
# BB#209:                               # %_ZeqRK4GUIDS1_.exit67
	movb	15(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+15(%rip), %cl
	jne	.LBB50_215
# BB#210:
	cmpq	$0, 184(%r14)
	jne	.LBB50_212
# BB#211:
	leaq	184(%r14), %rdx
	movq	192(%r14), %rdi
	movq	(%rdi), %rax
	movl	$IID_ICompressSetDecoderProperties2, %esi
	callq	*(%rax)
	testl	%eax, %eax
	jne	.LBB50_215
.LBB50_212:
	leaq	80(%r14), %rax
.LBB50_213:                             # %_ZeqRK4GUIDS1_.exit67.thread
	movq	%rax, (%rbx)
.LBB50_214:                             # %_ZeqRK4GUIDS1_.exit67.thread
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB50_215:                             # %_ZeqRK4GUIDS1_.exit67.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end50:
	.size	_ZN12CFilterCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end50-_ZN12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN12CFilterCoder6AddRefEv,"axG",@progbits,_ZN12CFilterCoder6AddRefEv,comdat
	.weak	_ZN12CFilterCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN12CFilterCoder6AddRefEv,@function
_ZN12CFilterCoder6AddRefEv:             # @_ZN12CFilterCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	88(%rdi), %eax
	incl	%eax
	movl	%eax, 88(%rdi)
	retq
.Lfunc_end51:
	.size	_ZN12CFilterCoder6AddRefEv, .Lfunc_end51-_ZN12CFilterCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN12CFilterCoder7ReleaseEv,"axG",@progbits,_ZN12CFilterCoder7ReleaseEv,comdat
	.weak	_ZN12CFilterCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN12CFilterCoder7ReleaseEv,@function
_ZN12CFilterCoder7ReleaseEv:            # @_ZN12CFilterCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi153:
	.cfi_def_cfa_offset 16
	movl	88(%rdi), %eax
	decl	%eax
	movl	%eax, 88(%rdi)
	jne	.LBB52_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB52_2:
	popq	%rcx
	retq
.Lfunc_end52:
	.size	_ZN12CFilterCoder7ReleaseEv, .Lfunc_end52-_ZN12CFilterCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N12CFilterCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN12CFilterCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end53:
	.size	_ZThn8_N12CFilterCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end53-_ZThn8_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N12CFilterCoder6AddRefEv,"axG",@progbits,_ZThn8_N12CFilterCoder6AddRefEv,comdat
	.weak	_ZThn8_N12CFilterCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N12CFilterCoder6AddRefEv,@function
_ZThn8_N12CFilterCoder6AddRefEv:        # @_ZThn8_N12CFilterCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	80(%rdi), %eax
	incl	%eax
	movl	%eax, 80(%rdi)
	retq
.Lfunc_end54:
	.size	_ZThn8_N12CFilterCoder6AddRefEv, .Lfunc_end54-_ZThn8_N12CFilterCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N12CFilterCoder7ReleaseEv,"axG",@progbits,_ZThn8_N12CFilterCoder7ReleaseEv,comdat
	.weak	_ZThn8_N12CFilterCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N12CFilterCoder7ReleaseEv,@function
_ZThn8_N12CFilterCoder7ReleaseEv:       # @_ZThn8_N12CFilterCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi154:
	.cfi_def_cfa_offset 16
	movl	80(%rdi), %eax
	decl	%eax
	movl	%eax, 80(%rdi)
	jne	.LBB55_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB55_2:                               # %_ZN12CFilterCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end55:
	.size	_ZThn8_N12CFilterCoder7ReleaseEv, .Lfunc_end55-_ZThn8_N12CFilterCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N12CFilterCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN12CFilterCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end56:
	.size	_ZThn16_N12CFilterCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end56-_ZThn16_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N12CFilterCoder6AddRefEv,"axG",@progbits,_ZThn16_N12CFilterCoder6AddRefEv,comdat
	.weak	_ZThn16_N12CFilterCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N12CFilterCoder6AddRefEv,@function
_ZThn16_N12CFilterCoder6AddRefEv:       # @_ZThn16_N12CFilterCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	72(%rdi), %eax
	incl	%eax
	movl	%eax, 72(%rdi)
	retq
.Lfunc_end57:
	.size	_ZThn16_N12CFilterCoder6AddRefEv, .Lfunc_end57-_ZThn16_N12CFilterCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N12CFilterCoder7ReleaseEv,"axG",@progbits,_ZThn16_N12CFilterCoder7ReleaseEv,comdat
	.weak	_ZThn16_N12CFilterCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N12CFilterCoder7ReleaseEv,@function
_ZThn16_N12CFilterCoder7ReleaseEv:      # @_ZThn16_N12CFilterCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi155:
	.cfi_def_cfa_offset 16
	movl	72(%rdi), %eax
	decl	%eax
	movl	%eax, 72(%rdi)
	jne	.LBB58_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB58_2:                               # %_ZN12CFilterCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end58:
	.size	_ZThn16_N12CFilterCoder7ReleaseEv, .Lfunc_end58-_ZThn16_N12CFilterCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn24_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn24_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn24_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn24_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn24_N12CFilterCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn24_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN12CFilterCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end59:
	.size	_ZThn24_N12CFilterCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end59-_ZThn24_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn24_N12CFilterCoder6AddRefEv,"axG",@progbits,_ZThn24_N12CFilterCoder6AddRefEv,comdat
	.weak	_ZThn24_N12CFilterCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn24_N12CFilterCoder6AddRefEv,@function
_ZThn24_N12CFilterCoder6AddRefEv:       # @_ZThn24_N12CFilterCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	64(%rdi), %eax
	incl	%eax
	movl	%eax, 64(%rdi)
	retq
.Lfunc_end60:
	.size	_ZThn24_N12CFilterCoder6AddRefEv, .Lfunc_end60-_ZThn24_N12CFilterCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn24_N12CFilterCoder7ReleaseEv,"axG",@progbits,_ZThn24_N12CFilterCoder7ReleaseEv,comdat
	.weak	_ZThn24_N12CFilterCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn24_N12CFilterCoder7ReleaseEv,@function
_ZThn24_N12CFilterCoder7ReleaseEv:      # @_ZThn24_N12CFilterCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi156:
	.cfi_def_cfa_offset 16
	movl	64(%rdi), %eax
	decl	%eax
	movl	%eax, 64(%rdi)
	jne	.LBB61_2
# BB#1:
	addq	$-24, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB61_2:                               # %_ZN12CFilterCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end61:
	.size	_ZThn24_N12CFilterCoder7ReleaseEv, .Lfunc_end61-_ZThn24_N12CFilterCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn32_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn32_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn32_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn32_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn32_N12CFilterCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn32_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-32, %rdi
	jmp	_ZN12CFilterCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end62:
	.size	_ZThn32_N12CFilterCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end62-_ZThn32_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn32_N12CFilterCoder6AddRefEv,"axG",@progbits,_ZThn32_N12CFilterCoder6AddRefEv,comdat
	.weak	_ZThn32_N12CFilterCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn32_N12CFilterCoder6AddRefEv,@function
_ZThn32_N12CFilterCoder6AddRefEv:       # @_ZThn32_N12CFilterCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	56(%rdi), %eax
	incl	%eax
	movl	%eax, 56(%rdi)
	retq
.Lfunc_end63:
	.size	_ZThn32_N12CFilterCoder6AddRefEv, .Lfunc_end63-_ZThn32_N12CFilterCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn32_N12CFilterCoder7ReleaseEv,"axG",@progbits,_ZThn32_N12CFilterCoder7ReleaseEv,comdat
	.weak	_ZThn32_N12CFilterCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn32_N12CFilterCoder7ReleaseEv,@function
_ZThn32_N12CFilterCoder7ReleaseEv:      # @_ZThn32_N12CFilterCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi157:
	.cfi_def_cfa_offset 16
	movl	56(%rdi), %eax
	decl	%eax
	movl	%eax, 56(%rdi)
	jne	.LBB64_2
# BB#1:
	addq	$-32, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB64_2:                               # %_ZN12CFilterCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end64:
	.size	_ZThn32_N12CFilterCoder7ReleaseEv, .Lfunc_end64-_ZThn32_N12CFilterCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn40_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn40_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn40_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn40_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn40_N12CFilterCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn40_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-40, %rdi
	jmp	_ZN12CFilterCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end65:
	.size	_ZThn40_N12CFilterCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end65-_ZThn40_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn40_N12CFilterCoder6AddRefEv,"axG",@progbits,_ZThn40_N12CFilterCoder6AddRefEv,comdat
	.weak	_ZThn40_N12CFilterCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn40_N12CFilterCoder6AddRefEv,@function
_ZThn40_N12CFilterCoder6AddRefEv:       # @_ZThn40_N12CFilterCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	48(%rdi), %eax
	incl	%eax
	movl	%eax, 48(%rdi)
	retq
.Lfunc_end66:
	.size	_ZThn40_N12CFilterCoder6AddRefEv, .Lfunc_end66-_ZThn40_N12CFilterCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn40_N12CFilterCoder7ReleaseEv,"axG",@progbits,_ZThn40_N12CFilterCoder7ReleaseEv,comdat
	.weak	_ZThn40_N12CFilterCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn40_N12CFilterCoder7ReleaseEv,@function
_ZThn40_N12CFilterCoder7ReleaseEv:      # @_ZThn40_N12CFilterCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi158:
	.cfi_def_cfa_offset 16
	movl	48(%rdi), %eax
	decl	%eax
	movl	%eax, 48(%rdi)
	jne	.LBB67_2
# BB#1:
	addq	$-40, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB67_2:                               # %_ZN12CFilterCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end67:
	.size	_ZThn40_N12CFilterCoder7ReleaseEv, .Lfunc_end67-_ZThn40_N12CFilterCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn48_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn48_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn48_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn48_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn48_N12CFilterCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn48_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-48, %rdi
	jmp	_ZN12CFilterCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end68:
	.size	_ZThn48_N12CFilterCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end68-_ZThn48_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn48_N12CFilterCoder6AddRefEv,"axG",@progbits,_ZThn48_N12CFilterCoder6AddRefEv,comdat
	.weak	_ZThn48_N12CFilterCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn48_N12CFilterCoder6AddRefEv,@function
_ZThn48_N12CFilterCoder6AddRefEv:       # @_ZThn48_N12CFilterCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	40(%rdi), %eax
	incl	%eax
	movl	%eax, 40(%rdi)
	retq
.Lfunc_end69:
	.size	_ZThn48_N12CFilterCoder6AddRefEv, .Lfunc_end69-_ZThn48_N12CFilterCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn48_N12CFilterCoder7ReleaseEv,"axG",@progbits,_ZThn48_N12CFilterCoder7ReleaseEv,comdat
	.weak	_ZThn48_N12CFilterCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn48_N12CFilterCoder7ReleaseEv,@function
_ZThn48_N12CFilterCoder7ReleaseEv:      # @_ZThn48_N12CFilterCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi159:
	.cfi_def_cfa_offset 16
	movl	40(%rdi), %eax
	decl	%eax
	movl	%eax, 40(%rdi)
	jne	.LBB70_2
# BB#1:
	addq	$-48, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB70_2:                               # %_ZN12CFilterCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end70:
	.size	_ZThn48_N12CFilterCoder7ReleaseEv, .Lfunc_end70-_ZThn48_N12CFilterCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn56_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn56_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn56_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn56_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn56_N12CFilterCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn56_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-56, %rdi
	jmp	_ZN12CFilterCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end71:
	.size	_ZThn56_N12CFilterCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end71-_ZThn56_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn56_N12CFilterCoder6AddRefEv,"axG",@progbits,_ZThn56_N12CFilterCoder6AddRefEv,comdat
	.weak	_ZThn56_N12CFilterCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn56_N12CFilterCoder6AddRefEv,@function
_ZThn56_N12CFilterCoder6AddRefEv:       # @_ZThn56_N12CFilterCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	32(%rdi), %eax
	incl	%eax
	movl	%eax, 32(%rdi)
	retq
.Lfunc_end72:
	.size	_ZThn56_N12CFilterCoder6AddRefEv, .Lfunc_end72-_ZThn56_N12CFilterCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn56_N12CFilterCoder7ReleaseEv,"axG",@progbits,_ZThn56_N12CFilterCoder7ReleaseEv,comdat
	.weak	_ZThn56_N12CFilterCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn56_N12CFilterCoder7ReleaseEv,@function
_ZThn56_N12CFilterCoder7ReleaseEv:      # @_ZThn56_N12CFilterCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi160:
	.cfi_def_cfa_offset 16
	movl	32(%rdi), %eax
	decl	%eax
	movl	%eax, 32(%rdi)
	jne	.LBB73_2
# BB#1:
	addq	$-56, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB73_2:                               # %_ZN12CFilterCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end73:
	.size	_ZThn56_N12CFilterCoder7ReleaseEv, .Lfunc_end73-_ZThn56_N12CFilterCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn64_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn64_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn64_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn64_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn64_N12CFilterCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn64_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-64, %rdi
	jmp	_ZN12CFilterCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end74:
	.size	_ZThn64_N12CFilterCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end74-_ZThn64_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn64_N12CFilterCoder6AddRefEv,"axG",@progbits,_ZThn64_N12CFilterCoder6AddRefEv,comdat
	.weak	_ZThn64_N12CFilterCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn64_N12CFilterCoder6AddRefEv,@function
_ZThn64_N12CFilterCoder6AddRefEv:       # @_ZThn64_N12CFilterCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	incl	%eax
	movl	%eax, 24(%rdi)
	retq
.Lfunc_end75:
	.size	_ZThn64_N12CFilterCoder6AddRefEv, .Lfunc_end75-_ZThn64_N12CFilterCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn64_N12CFilterCoder7ReleaseEv,"axG",@progbits,_ZThn64_N12CFilterCoder7ReleaseEv,comdat
	.weak	_ZThn64_N12CFilterCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn64_N12CFilterCoder7ReleaseEv,@function
_ZThn64_N12CFilterCoder7ReleaseEv:      # @_ZThn64_N12CFilterCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi161:
	.cfi_def_cfa_offset 16
	movl	24(%rdi), %eax
	decl	%eax
	movl	%eax, 24(%rdi)
	jne	.LBB76_2
# BB#1:
	addq	$-64, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB76_2:                               # %_ZN12CFilterCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end76:
	.size	_ZThn64_N12CFilterCoder7ReleaseEv, .Lfunc_end76-_ZThn64_N12CFilterCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn72_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn72_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn72_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn72_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn72_N12CFilterCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn72_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-72, %rdi
	jmp	_ZN12CFilterCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end77:
	.size	_ZThn72_N12CFilterCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end77-_ZThn72_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn72_N12CFilterCoder6AddRefEv,"axG",@progbits,_ZThn72_N12CFilterCoder6AddRefEv,comdat
	.weak	_ZThn72_N12CFilterCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn72_N12CFilterCoder6AddRefEv,@function
_ZThn72_N12CFilterCoder6AddRefEv:       # @_ZThn72_N12CFilterCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end78:
	.size	_ZThn72_N12CFilterCoder6AddRefEv, .Lfunc_end78-_ZThn72_N12CFilterCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn72_N12CFilterCoder7ReleaseEv,"axG",@progbits,_ZThn72_N12CFilterCoder7ReleaseEv,comdat
	.weak	_ZThn72_N12CFilterCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn72_N12CFilterCoder7ReleaseEv,@function
_ZThn72_N12CFilterCoder7ReleaseEv:      # @_ZThn72_N12CFilterCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi162:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB79_2
# BB#1:
	addq	$-72, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB79_2:                               # %_ZN12CFilterCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end79:
	.size	_ZThn72_N12CFilterCoder7ReleaseEv, .Lfunc_end79-_ZThn72_N12CFilterCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn80_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn80_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn80_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn80_N12CFilterCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn80_N12CFilterCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn80_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-80, %rdi
	jmp	_ZN12CFilterCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end80:
	.size	_ZThn80_N12CFilterCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end80-_ZThn80_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn80_N12CFilterCoder6AddRefEv,"axG",@progbits,_ZThn80_N12CFilterCoder6AddRefEv,comdat
	.weak	_ZThn80_N12CFilterCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn80_N12CFilterCoder6AddRefEv,@function
_ZThn80_N12CFilterCoder6AddRefEv:       # @_ZThn80_N12CFilterCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end81:
	.size	_ZThn80_N12CFilterCoder6AddRefEv, .Lfunc_end81-_ZThn80_N12CFilterCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn80_N12CFilterCoder7ReleaseEv,"axG",@progbits,_ZThn80_N12CFilterCoder7ReleaseEv,comdat
	.weak	_ZThn80_N12CFilterCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn80_N12CFilterCoder7ReleaseEv,@function
_ZThn80_N12CFilterCoder7ReleaseEv:      # @_ZThn80_N12CFilterCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi163:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB82_2
# BB#1:
	addq	$-80, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB82_2:                               # %_ZN12CFilterCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end82:
	.size	_ZThn80_N12CFilterCoder7ReleaseEv, .Lfunc_end82-_ZThn80_N12CFilterCoder7ReleaseEv
	.cfi_endproc

	.type	_ZTV12CFilterCoder,@object # @_ZTV12CFilterCoder
	.section	.rodata,"a",@progbits
	.globl	_ZTV12CFilterCoder
	.p2align	3
_ZTV12CFilterCoder:
	.quad	0
	.quad	_ZTI12CFilterCoder
	.quad	_ZN12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN12CFilterCoder6AddRefEv
	.quad	_ZN12CFilterCoder7ReleaseEv
	.quad	_ZN12CFilterCoderD2Ev
	.quad	_ZN12CFilterCoderD0Ev
	.quad	_ZN12CFilterCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS5_P21ICompressProgressInfo
	.quad	_ZN12CFilterCoder15ReleaseInStreamEv
	.quad	_ZN12CFilterCoder11SetInStreamEP19ISequentialInStream
	.quad	_ZN12CFilterCoder4ReadEPvjPj
	.quad	_ZN12CFilterCoder12SetOutStreamEP20ISequentialOutStream
	.quad	_ZN12CFilterCoder16ReleaseOutStreamEv
	.quad	_ZN12CFilterCoder5WriteEPKvjPj
	.quad	_ZN12CFilterCoder5FlushEv
	.quad	_ZN12CFilterCoder17CryptoSetPasswordEPKhj
	.quad	_ZN12CFilterCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.quad	_ZN12CFilterCoder20WriteCoderPropertiesEP20ISequentialOutStream
	.quad	_ZN12CFilterCoder15ResetInitVectorEv
	.quad	_ZN12CFilterCoder21SetDecoderProperties2EPKhj
	.quad	-8
	.quad	_ZTI12CFilterCoder
	.quad	_ZThn8_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N12CFilterCoder6AddRefEv
	.quad	_ZThn8_N12CFilterCoder7ReleaseEv
	.quad	_ZThn8_N12CFilterCoderD1Ev
	.quad	_ZThn8_N12CFilterCoderD0Ev
	.quad	_ZThn8_N12CFilterCoder11SetInStreamEP19ISequentialInStream
	.quad	_ZThn8_N12CFilterCoder15ReleaseInStreamEv
	.quad	-16
	.quad	_ZTI12CFilterCoder
	.quad	_ZThn16_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N12CFilterCoder6AddRefEv
	.quad	_ZThn16_N12CFilterCoder7ReleaseEv
	.quad	_ZThn16_N12CFilterCoderD1Ev
	.quad	_ZThn16_N12CFilterCoderD0Ev
	.quad	_ZThn16_N12CFilterCoder4ReadEPvjPj
	.quad	-24
	.quad	_ZTI12CFilterCoder
	.quad	_ZThn24_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn24_N12CFilterCoder6AddRefEv
	.quad	_ZThn24_N12CFilterCoder7ReleaseEv
	.quad	_ZThn24_N12CFilterCoderD1Ev
	.quad	_ZThn24_N12CFilterCoderD0Ev
	.quad	_ZThn24_N12CFilterCoder12SetOutStreamEP20ISequentialOutStream
	.quad	_ZThn24_N12CFilterCoder16ReleaseOutStreamEv
	.quad	-32
	.quad	_ZTI12CFilterCoder
	.quad	_ZThn32_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn32_N12CFilterCoder6AddRefEv
	.quad	_ZThn32_N12CFilterCoder7ReleaseEv
	.quad	_ZThn32_N12CFilterCoderD1Ev
	.quad	_ZThn32_N12CFilterCoderD0Ev
	.quad	_ZThn32_N12CFilterCoder5WriteEPKvjPj
	.quad	-40
	.quad	_ZTI12CFilterCoder
	.quad	_ZThn40_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn40_N12CFilterCoder6AddRefEv
	.quad	_ZThn40_N12CFilterCoder7ReleaseEv
	.quad	_ZThn40_N12CFilterCoderD1Ev
	.quad	_ZThn40_N12CFilterCoderD0Ev
	.quad	_ZThn40_N12CFilterCoder5FlushEv
	.quad	-48
	.quad	_ZTI12CFilterCoder
	.quad	_ZThn48_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn48_N12CFilterCoder6AddRefEv
	.quad	_ZThn48_N12CFilterCoder7ReleaseEv
	.quad	_ZThn48_N12CFilterCoderD1Ev
	.quad	_ZThn48_N12CFilterCoderD0Ev
	.quad	_ZThn48_N12CFilterCoder17CryptoSetPasswordEPKhj
	.quad	-56
	.quad	_ZTI12CFilterCoder
	.quad	_ZThn56_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn56_N12CFilterCoder6AddRefEv
	.quad	_ZThn56_N12CFilterCoder7ReleaseEv
	.quad	_ZThn56_N12CFilterCoderD1Ev
	.quad	_ZThn56_N12CFilterCoderD0Ev
	.quad	_ZThn56_N12CFilterCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.quad	-64
	.quad	_ZTI12CFilterCoder
	.quad	_ZThn64_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn64_N12CFilterCoder6AddRefEv
	.quad	_ZThn64_N12CFilterCoder7ReleaseEv
	.quad	_ZThn64_N12CFilterCoderD1Ev
	.quad	_ZThn64_N12CFilterCoderD0Ev
	.quad	_ZThn64_N12CFilterCoder20WriteCoderPropertiesEP20ISequentialOutStream
	.quad	-72
	.quad	_ZTI12CFilterCoder
	.quad	_ZThn72_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn72_N12CFilterCoder6AddRefEv
	.quad	_ZThn72_N12CFilterCoder7ReleaseEv
	.quad	_ZThn72_N12CFilterCoderD1Ev
	.quad	_ZThn72_N12CFilterCoderD0Ev
	.quad	_ZThn72_N12CFilterCoder15ResetInitVectorEv
	.quad	-80
	.quad	_ZTI12CFilterCoder
	.quad	_ZThn80_N12CFilterCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn80_N12CFilterCoder6AddRefEv
	.quad	_ZThn80_N12CFilterCoder7ReleaseEv
	.quad	_ZThn80_N12CFilterCoderD1Ev
	.quad	_ZThn80_N12CFilterCoderD0Ev
	.quad	_ZThn80_N12CFilterCoder21SetDecoderProperties2EPKhj
	.size	_ZTV12CFilterCoder, 816

	.type	_ZTS12CFilterCoder,@object # @_ZTS12CFilterCoder
	.globl	_ZTS12CFilterCoder
_ZTS12CFilterCoder:
	.asciz	"12CFilterCoder"
	.size	_ZTS12CFilterCoder, 15

	.type	_ZTS14ICompressCoder,@object # @_ZTS14ICompressCoder
	.section	.rodata._ZTS14ICompressCoder,"aG",@progbits,_ZTS14ICompressCoder,comdat
	.weak	_ZTS14ICompressCoder
	.p2align	4
_ZTS14ICompressCoder:
	.asciz	"14ICompressCoder"
	.size	_ZTS14ICompressCoder, 17

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI14ICompressCoder,@object # @_ZTI14ICompressCoder
	.section	.rodata._ZTI14ICompressCoder,"aG",@progbits,_ZTI14ICompressCoder,comdat
	.weak	_ZTI14ICompressCoder
	.p2align	4
_ZTI14ICompressCoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ICompressCoder
	.quad	_ZTI8IUnknown
	.size	_ZTI14ICompressCoder, 24

	.type	_ZTS20ICompressSetInStream,@object # @_ZTS20ICompressSetInStream
	.section	.rodata._ZTS20ICompressSetInStream,"aG",@progbits,_ZTS20ICompressSetInStream,comdat
	.weak	_ZTS20ICompressSetInStream
	.p2align	4
_ZTS20ICompressSetInStream:
	.asciz	"20ICompressSetInStream"
	.size	_ZTS20ICompressSetInStream, 23

	.type	_ZTI20ICompressSetInStream,@object # @_ZTI20ICompressSetInStream
	.section	.rodata._ZTI20ICompressSetInStream,"aG",@progbits,_ZTI20ICompressSetInStream,comdat
	.weak	_ZTI20ICompressSetInStream
	.p2align	4
_ZTI20ICompressSetInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ICompressSetInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ICompressSetInStream, 24

	.type	_ZTS19ISequentialInStream,@object # @_ZTS19ISequentialInStream
	.section	.rodata._ZTS19ISequentialInStream,"aG",@progbits,_ZTS19ISequentialInStream,comdat
	.weak	_ZTS19ISequentialInStream
	.p2align	4
_ZTS19ISequentialInStream:
	.asciz	"19ISequentialInStream"
	.size	_ZTS19ISequentialInStream, 22

	.type	_ZTI19ISequentialInStream,@object # @_ZTI19ISequentialInStream
	.section	.rodata._ZTI19ISequentialInStream,"aG",@progbits,_ZTI19ISequentialInStream,comdat
	.weak	_ZTI19ISequentialInStream
	.p2align	4
_ZTI19ISequentialInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19ISequentialInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI19ISequentialInStream, 24

	.type	_ZTS21ICompressSetOutStream,@object # @_ZTS21ICompressSetOutStream
	.section	.rodata._ZTS21ICompressSetOutStream,"aG",@progbits,_ZTS21ICompressSetOutStream,comdat
	.weak	_ZTS21ICompressSetOutStream
	.p2align	4
_ZTS21ICompressSetOutStream:
	.asciz	"21ICompressSetOutStream"
	.size	_ZTS21ICompressSetOutStream, 24

	.type	_ZTI21ICompressSetOutStream,@object # @_ZTI21ICompressSetOutStream
	.section	.rodata._ZTI21ICompressSetOutStream,"aG",@progbits,_ZTI21ICompressSetOutStream,comdat
	.weak	_ZTI21ICompressSetOutStream
	.p2align	4
_ZTI21ICompressSetOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS21ICompressSetOutStream
	.quad	_ZTI8IUnknown
	.size	_ZTI21ICompressSetOutStream, 24

	.type	_ZTS20ISequentialOutStream,@object # @_ZTS20ISequentialOutStream
	.section	.rodata._ZTS20ISequentialOutStream,"aG",@progbits,_ZTS20ISequentialOutStream,comdat
	.weak	_ZTS20ISequentialOutStream
	.p2align	4
_ZTS20ISequentialOutStream:
	.asciz	"20ISequentialOutStream"
	.size	_ZTS20ISequentialOutStream, 23

	.type	_ZTI20ISequentialOutStream,@object # @_ZTI20ISequentialOutStream
	.section	.rodata._ZTI20ISequentialOutStream,"aG",@progbits,_ZTI20ISequentialOutStream,comdat
	.weak	_ZTI20ISequentialOutStream
	.p2align	4
_ZTI20ISequentialOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ISequentialOutStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ISequentialOutStream, 24

	.type	_ZTS15IOutStreamFlush,@object # @_ZTS15IOutStreamFlush
	.section	.rodata._ZTS15IOutStreamFlush,"aG",@progbits,_ZTS15IOutStreamFlush,comdat
	.weak	_ZTS15IOutStreamFlush
	.p2align	4
_ZTS15IOutStreamFlush:
	.asciz	"15IOutStreamFlush"
	.size	_ZTS15IOutStreamFlush, 18

	.type	_ZTI15IOutStreamFlush,@object # @_ZTI15IOutStreamFlush
	.section	.rodata._ZTI15IOutStreamFlush,"aG",@progbits,_ZTI15IOutStreamFlush,comdat
	.weak	_ZTI15IOutStreamFlush
	.p2align	4
_ZTI15IOutStreamFlush:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15IOutStreamFlush
	.quad	_ZTI8IUnknown
	.size	_ZTI15IOutStreamFlush, 24

	.type	_ZTS18ICryptoSetPassword,@object # @_ZTS18ICryptoSetPassword
	.section	.rodata._ZTS18ICryptoSetPassword,"aG",@progbits,_ZTS18ICryptoSetPassword,comdat
	.weak	_ZTS18ICryptoSetPassword
	.p2align	4
_ZTS18ICryptoSetPassword:
	.asciz	"18ICryptoSetPassword"
	.size	_ZTS18ICryptoSetPassword, 21

	.type	_ZTI18ICryptoSetPassword,@object # @_ZTI18ICryptoSetPassword
	.section	.rodata._ZTI18ICryptoSetPassword,"aG",@progbits,_ZTI18ICryptoSetPassword,comdat
	.weak	_ZTI18ICryptoSetPassword
	.p2align	4
_ZTI18ICryptoSetPassword:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18ICryptoSetPassword
	.quad	_ZTI8IUnknown
	.size	_ZTI18ICryptoSetPassword, 24

	.type	_ZTS27ICompressSetCoderProperties,@object # @_ZTS27ICompressSetCoderProperties
	.section	.rodata._ZTS27ICompressSetCoderProperties,"aG",@progbits,_ZTS27ICompressSetCoderProperties,comdat
	.weak	_ZTS27ICompressSetCoderProperties
	.p2align	4
_ZTS27ICompressSetCoderProperties:
	.asciz	"27ICompressSetCoderProperties"
	.size	_ZTS27ICompressSetCoderProperties, 30

	.type	_ZTI27ICompressSetCoderProperties,@object # @_ZTI27ICompressSetCoderProperties
	.section	.rodata._ZTI27ICompressSetCoderProperties,"aG",@progbits,_ZTI27ICompressSetCoderProperties,comdat
	.weak	_ZTI27ICompressSetCoderProperties
	.p2align	4
_ZTI27ICompressSetCoderProperties:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS27ICompressSetCoderProperties
	.quad	_ZTI8IUnknown
	.size	_ZTI27ICompressSetCoderProperties, 24

	.type	_ZTS29ICompressWriteCoderProperties,@object # @_ZTS29ICompressWriteCoderProperties
	.section	.rodata._ZTS29ICompressWriteCoderProperties,"aG",@progbits,_ZTS29ICompressWriteCoderProperties,comdat
	.weak	_ZTS29ICompressWriteCoderProperties
	.p2align	4
_ZTS29ICompressWriteCoderProperties:
	.asciz	"29ICompressWriteCoderProperties"
	.size	_ZTS29ICompressWriteCoderProperties, 32

	.type	_ZTI29ICompressWriteCoderProperties,@object # @_ZTI29ICompressWriteCoderProperties
	.section	.rodata._ZTI29ICompressWriteCoderProperties,"aG",@progbits,_ZTI29ICompressWriteCoderProperties,comdat
	.weak	_ZTI29ICompressWriteCoderProperties
	.p2align	4
_ZTI29ICompressWriteCoderProperties:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS29ICompressWriteCoderProperties
	.quad	_ZTI8IUnknown
	.size	_ZTI29ICompressWriteCoderProperties, 24

	.type	_ZTS22ICryptoResetInitVector,@object # @_ZTS22ICryptoResetInitVector
	.section	.rodata._ZTS22ICryptoResetInitVector,"aG",@progbits,_ZTS22ICryptoResetInitVector,comdat
	.weak	_ZTS22ICryptoResetInitVector
	.p2align	4
_ZTS22ICryptoResetInitVector:
	.asciz	"22ICryptoResetInitVector"
	.size	_ZTS22ICryptoResetInitVector, 25

	.type	_ZTI22ICryptoResetInitVector,@object # @_ZTI22ICryptoResetInitVector
	.section	.rodata._ZTI22ICryptoResetInitVector,"aG",@progbits,_ZTI22ICryptoResetInitVector,comdat
	.weak	_ZTI22ICryptoResetInitVector
	.p2align	4
_ZTI22ICryptoResetInitVector:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS22ICryptoResetInitVector
	.quad	_ZTI8IUnknown
	.size	_ZTI22ICryptoResetInitVector, 24

	.type	_ZTS30ICompressSetDecoderProperties2,@object # @_ZTS30ICompressSetDecoderProperties2
	.section	.rodata._ZTS30ICompressSetDecoderProperties2,"aG",@progbits,_ZTS30ICompressSetDecoderProperties2,comdat
	.weak	_ZTS30ICompressSetDecoderProperties2
	.p2align	4
_ZTS30ICompressSetDecoderProperties2:
	.asciz	"30ICompressSetDecoderProperties2"
	.size	_ZTS30ICompressSetDecoderProperties2, 33

	.type	_ZTI30ICompressSetDecoderProperties2,@object # @_ZTI30ICompressSetDecoderProperties2
	.section	.rodata._ZTI30ICompressSetDecoderProperties2,"aG",@progbits,_ZTI30ICompressSetDecoderProperties2,comdat
	.weak	_ZTI30ICompressSetDecoderProperties2
	.p2align	4
_ZTI30ICompressSetDecoderProperties2:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS30ICompressSetDecoderProperties2
	.quad	_ZTI8IUnknown
	.size	_ZTI30ICompressSetDecoderProperties2, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI12CFilterCoder,@object # @_ZTI12CFilterCoder
	.section	.rodata,"a",@progbits
	.globl	_ZTI12CFilterCoder
	.p2align	4
_ZTI12CFilterCoder:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS12CFilterCoder
	.long	1                       # 0x1
	.long	12                      # 0xc
	.quad	_ZTI14ICompressCoder
	.quad	2                       # 0x2
	.quad	_ZTI20ICompressSetInStream
	.quad	2050                    # 0x802
	.quad	_ZTI19ISequentialInStream
	.quad	4098                    # 0x1002
	.quad	_ZTI21ICompressSetOutStream
	.quad	6146                    # 0x1802
	.quad	_ZTI20ISequentialOutStream
	.quad	8194                    # 0x2002
	.quad	_ZTI15IOutStreamFlush
	.quad	10242                   # 0x2802
	.quad	_ZTI18ICryptoSetPassword
	.quad	12290                   # 0x3002
	.quad	_ZTI27ICompressSetCoderProperties
	.quad	14338                   # 0x3802
	.quad	_ZTI29ICompressWriteCoderProperties
	.quad	16386                   # 0x4002
	.quad	_ZTI22ICryptoResetInitVector
	.quad	18434                   # 0x4802
	.quad	_ZTI30ICompressSetDecoderProperties2
	.quad	20482                   # 0x5002
	.quad	_ZTI13CMyUnknownImp
	.quad	22530                   # 0x5802
	.size	_ZTI12CFilterCoder, 216


	.globl	_ZN12CFilterCoderC1Ev
	.type	_ZN12CFilterCoderC1Ev,@function
_ZN12CFilterCoderC1Ev = _ZN12CFilterCoderC2Ev
	.globl	_ZN12CFilterCoderD1Ev
	.type	_ZN12CFilterCoderD1Ev,@function
_ZN12CFilterCoderD1Ev = _ZN12CFilterCoderD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
