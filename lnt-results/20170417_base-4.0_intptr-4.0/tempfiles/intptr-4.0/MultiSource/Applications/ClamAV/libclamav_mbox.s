	.text
	.file	"libclamav_mbox.bc"
	.globl	cli_mbox
	.p2align	4, 0x90
	.type	cli_mbox,@function
cli_mbox:                               # @cli_mbox
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$4184, %rsp             # imm = 0x1058
.Lcfi6:
	.cfi_def_cfa_offset 4240
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r13
	movl	%esi, %ebx
	movq	%rdi, %r12
	testq	%r12, %r12
	je	.LBB0_16
# BB#1:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%ebx, %edi
	callq	dup
	movl	%eax, %ebp
	movl	$.L.str.2, %esi
	movl	%ebp, %edi
	callq	fdopen
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_17
# BB#2:
	movq	%r15, %rdi
	callq	rewind
	leaq	144(%rsp), %rdi
	movl	$1000, %esi             # imm = 0x3E8
	movq	%r15, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB0_18
# BB#3:
	movq	cli_parse_mbox.rfc821(%rip), %rax
	testq	%rax, %rax
	je	.LBB0_19
.LBB0_4:                                # %initialiseTables.exit.i
	movq	%r12, 104(%rsp)
	movq	%rax, 120(%rsp)
	movq	cli_parse_mbox.subtype(%rip), %rax
	movq	%rax, 128(%rsp)
	movq	%r13, 136(%rsp)
	movl	$0, 112(%rsp)
	leaq	144(%rsp), %rdi
	movl	$.L.str.4, %esi
	movl	$5, %edx
	callq	strncmp
	testl	%eax, %eax
	movq	%r15, 8(%rsp)           # 8-byte Spill
	je	.LBB0_29
# BB#5:
	leaq	144(%rsp), %rdi
	movl	$.L.str.9, %esi
	movl	$4, %edx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB0_10
# BB#6:                                 # %.preheader.i.preheader
	leaq	144(%rsp), %rbx
	.p2align	4, 0x90
.LBB0_7:                                # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$1000, %esi             # imm = 0x3E8
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB0_10
# BB#8:                                 #   in Loop: Header=BB0_7 Depth=1
	movzbl	144(%rsp), %ecx
	movl	$1, %eax
	shll	%cl, %eax
	cmpb	$15, %cl
	ja	.LBB0_7
# BB#9:                                 #   in Loop: Header=BB0_7 Depth=1
	andl	$9217, %eax             # imm = 0x2401
	testw	%ax, %ax
	je	.LBB0_7
.LBB0_10:                               # %.critedge.i.preheader
	leaq	144(%rsp), %rbx
	.p2align	4, 0x90
.LBB0_11:                               # %.critedge.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	144(%rsp), %ecx
	movl	$1, %eax
	shll	%cl, %eax
	cmpb	$15, %cl
	ja	.LBB0_14
# BB#12:                                # %.critedge.i
                                        #   in Loop: Header=BB0_11 Depth=1
	andl	$9217, %eax             # imm = 0x2401
	testw	%ax, %ax
	je	.LBB0_14
# BB#13:                                #   in Loop: Header=BB0_11 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	getline_from_mbox
	testq	%rax, %rax
	jne	.LBB0_11
.LBB0_14:                               # %.critedge1.i
	movb	$0, 1144(%rsp)
	movq	cli_parse_mbox.rfc821(%rip), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	$.L.str.244, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	callq	messageCreate
	testq	%rax, %rax
	je	.LBB0_49
# BB#15:
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r13, 40(%rsp)          # 8-byte Spill
	leaq	1152(%rsp), %r13
	leaq	144(%rsp), %rsi
	movq	%r13, %rdi
	callq	strcpy
	movl	$-1, 68(%rsp)           # 4-byte Folded Spill
	movb	$1, %al
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	movl	$0, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movb	$1, %al
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r12, 56(%rsp)          # 8-byte Spill
	jmp	.LBB0_65
.LBB0_16:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movl	$-111, %ebx
	jmp	.LBB0_160
.LBB0_17:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_errmsg
	movl	%ebp, %edi
	callq	close
	movl	$-115, %ebx
	jmp	.LBB0_160
.LBB0_18:
	movq	%r15, %rdi
	callq	fclose
	xorl	%ebx, %ebx
	jmp	.LBB0_160
.LBB0_19:
	callq	tableCreate
	movq	%rax, cli_parse_mbox.rfc821(%rip)
	movl	$.L.str.13, %esi
	movl	$1, %edx
	movq	%rax, %rdi
	callq	tableInsert
	testl	%eax, %eax
	js	.LBB0_27
# BB#20:
	movq	cli_parse_mbox.rfc821(%rip), %rdi
	movl	$.L.str.14, %esi
	movl	$2, %edx
	callq	tableInsert
	testl	%eax, %eax
	js	.LBB0_27
# BB#21:
	movq	cli_parse_mbox.rfc821(%rip), %rdi
	movl	$.L.str.15, %esi
	movl	$3, %edx
	callq	tableInsert
	testl	%eax, %eax
	js	.LBB0_27
# BB#22:
	callq	tableCreate
	movq	%rax, cli_parse_mbox.subtype(%rip)
	movl	$.L.str.16, %esi
	movl	$1, %edx
	movq	%rax, %rdi
	callq	tableInsert
	testl	%eax, %eax
	js	.LBB0_26
# BB#23:                                # %.lr.ph.i.preheader
	movl	$mimeSubtypes+16, %ebx
	.p2align	4, 0x90
.LBB0_24:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB0_161
# BB#25:                                # %._crit_edge.i.i
                                        #   in Loop: Header=BB0_24 Depth=1
	movq	cli_parse_mbox.subtype(%rip), %rdi
	movl	8(%rbx), %edx
	callq	tableInsert
	addq	$16, %rbx
	testl	%eax, %eax
	jns	.LBB0_24
.LBB0_26:                               # %._crit_edge.i
	movq	cli_parse_mbox.rfc821(%rip), %rdi
	callq	tableDestroy
	movl	$cli_parse_mbox.subtype, %ebx
	movq	cli_parse_mbox.subtype(%rip), %rdi
	callq	tableDestroy
	movq	$0, cli_parse_mbox.rfc821(%rip)
	jmp	.LBB0_28
.LBB0_27:
	movl	$cli_parse_mbox.rfc821, %ebx
	movq	cli_parse_mbox.rfc821(%rip), %rdi
	callq	tableDestroy
.LBB0_28:
	movq	$0, (%rbx)
	movq	$0, cli_parse_mbox.rfc821(%rip)
	movq	$0, cli_parse_mbox.subtype(%rip)
	jmp	.LBB0_48
.LBB0_29:
	callq	messageCreate
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_48
# BB#30:
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	%r14, %rdi
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	%r13, %rsi
	callq	messageSetCTX
	movl	$1, %r15d
	leaq	144(%rsp), %r12
	xorl	%r13d, %r13d
	jmp	.LBB0_38
.LBB0_31:                               #   in Loop: Header=BB0_38 Depth=1
	leal	1(%r15), %ebp
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	cli_dbgmsg
	movq	cli_parse_mbox.rfc821(%rip), %rsi
	movq	%r14, %rdi
	callq	parseEmailHeaders
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_36
# BB#32:                                #   in Loop: Header=BB0_38 Depth=1
	movl	%ebp, %r15d
	movq	%rbx, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	messageSetCTX
	movq	%r14, %rdi
	callq	messageDestroy
	movq	%rbx, %rdi
	callq	messageGetBody
	testq	%rax, %rax
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB0_35
# BB#33:                                #   in Loop: Header=BB0_38 Depth=1
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	leaq	104(%rsp), %rdx
	callq	parseEmailBody
	testl	%eax, %eax
	je	.LBB0_37
# BB#34:                                #   in Loop: Header=BB0_38 Depth=1
	cmpl	$3, %eax
	je	.LBB0_162
.LBB0_35:                               # %.thread.i
                                        #   in Loop: Header=BB0_38 Depth=1
	movq	%rbx, %rdi
	callq	messageReset
	movq	%rbx, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	messageSetCTX
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB0_41
.LBB0_36:                               #   in Loop: Header=BB0_38 Depth=1
	movq	%r14, %rdi
	callq	messageReset
	movq	%r14, %rbx
	jmp	.LBB0_45
.LBB0_37:                               # %.thread107.i
                                        #   in Loop: Header=BB0_38 Depth=1
	movq	%rbx, %rdi
	callq	messageReset
	movl	%r15d, %ebp
	jmp	.LBB0_45
	.p2align	4, 0x90
.LBB0_38:                               # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	cli_chomp
	testb	$1, %r13b
	je	.LBB0_40
# BB#39:                                #   in Loop: Header=BB0_38 Depth=1
	movl	$.L.str.4, %esi
	movl	$5, %edx
	movq	%r12, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB0_31
.LBB0_40:                               #   in Loop: Header=BB0_38 Depth=1
	cmpb	$0, 144(%rsp)
	sete	%r13b
                                        # kill: %R15D<def> %R15D<kill> %R15<kill>
	movq	%r14, %rbx
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB0_41:                               #   in Loop: Header=BB0_38 Depth=1
	movq	%r12, %rdi
	callq	isuuencodebegin
	testl	%eax, %eax
	je	.LBB0_44
# BB#42:                                #   in Loop: Header=BB0_38 Depth=1
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	%rbp, %rcx
	callq	uudecodeFile
	testl	%eax, %eax
	js	.LBB0_44
# BB#43:                                #   in Loop: Header=BB0_38 Depth=1
	movl	%r15d, %ebp
	jmp	.LBB0_45
	.p2align	4, 0x90
.LBB0_44:                               #   in Loop: Header=BB0_38 Depth=1
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	messageAddStr
	testl	%eax, %eax
	movl	%r15d, %ebp
	js	.LBB0_46
.LBB0_45:                               #   in Loop: Header=BB0_38 Depth=1
	movl	$1000, %esi             # imm = 0x3E8
	movq	%r12, %rdi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	fgets
	testq	%rax, %rax
	movl	%ebp, %r15d
	movq	%rbx, %r14
	jne	.LBB0_38
.LBB0_46:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	fclose
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	movq	cli_parse_mbox.rfc821(%rip), %rsi
	movq	%rbx, %rdi
	callq	parseEmailHeaders
	movq	%rax, %rbp
	testq	%rbx, %rbx
	je	.LBB0_157
# BB#47:
	movq	%rbx, %rdi
	callq	messageDestroy
.LBB0_157:
	movq	40(%rsp), %r13          # 8-byte Reload
	testq	%rbp, %rbp
	jne	.LBB0_149
	jmp	.LBB0_153
.LBB0_48:
	movq	%r15, %rdi
	callq	fclose
	movl	$-114, %ebx
	jmp	.LBB0_160
.LBB0_49:
	xorl	%ebp, %ebp
	jmp	.LBB0_148
.LBB0_50:                               #   in Loop: Header=BB0_65 Depth=1
	movl	$.L.str.85, %esi
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	messageFindArgument
	movq	%rax, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	movl	$0, %esi
	movl	$0, %eax
	jne	.LBB0_52
	jmp	.LBB0_98
.LBB0_51:                               #   in Loop: Header=BB0_65 Depth=1
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r15d
	movq	%r14, %rsi
	movq	88(%rsp), %r14          # 8-byte Reload
.LBB0_52:                               # %.thread167.i.i
                                        #   in Loop: Header=BB0_65 Depth=1
	movq	%rsi, (%rsp)            # 8-byte Spill
	movb	%r15b, %al
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	8(%rsp), %r15           # 8-byte Reload
	jmp	.LBB0_136
.LBB0_53:                               #   in Loop: Header=BB0_65 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rbx
.LBB0_54:                               #   in Loop: Header=BB0_65 Depth=1
	movq	%rbx, %rsi
	movq	72(%rsp), %rdx          # 8-byte Reload
	callq	parseEmailHeader
	testl	%eax, %eax
	js	.LBB0_56
# BB#55:                                #   in Loop: Header=BB0_65 Depth=1
	movq	%rbx, %rdi
	callq	free
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	jmp	.LBB0_57
.LBB0_56:                               #   in Loop: Header=BB0_65 Depth=1
	movq	%rbx, (%rsp)            # 8-byte Spill
.LBB0_57:                               #   in Loop: Header=BB0_65 Depth=1
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r15           # 8-byte Reload
	jmp	.LBB0_136
.LBB0_58:                               #   in Loop: Header=BB0_65 Depth=1
	testb	$1, %r13b
	jne	.LBB0_63
# BB#59:                                #   in Loop: Header=BB0_65 Depth=1
	movl	$.L.str.47, %esi
	movq	%r14, %rdi
	callq	strcasecmp
	movb	$1, %bpl
	testl	%eax, %eax
	je	.LBB0_64
# BB#60:                                #   in Loop: Header=BB0_65 Depth=1
	movl	$.L.str.48, %esi
	movq	%r14, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	leaq	1152(%rsp), %r13
	je	.LBB0_62
# BB#61:                                #   in Loop: Header=BB0_65 Depth=1
	movl	$.L.str.49, %esi
	movq	%r14, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	sete	%bpl
.LBB0_62:                               # %usefulHeader.exit.i.i
                                        #   in Loop: Header=BB0_65 Depth=1
	movq	%rbp, %r14
	jmp	.LBB0_135
.LBB0_63:                               #   in Loop: Header=BB0_65 Depth=1
	movq	%r13, %r14
	jmp	.LBB0_134
.LBB0_64:                               #   in Loop: Header=BB0_65 Depth=1
	leaq	1152(%rsp), %r13
	movq	%rbp, %r14
	jmp	.LBB0_135
	.p2align	4, 0x90
.LBB0_65:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_90 Depth 2
                                        #     Child Loop BB0_126 Depth 2
	movq	%r13, %rdi
	callq	cli_chomp
	movb	1152(%rsp), %bl
	testb	%bl, %bl
	movl	$0, %ebp
	cmovneq	%r13, %rbp
	testb	$1, 20(%rsp)            # 1-byte Folded Reload
	je	.LBB0_69
# BB#66:                                #   in Loop: Header=BB0_65 Depth=1
	movq	%r13, %rdi
	movq	48(%rsp), %rsi          # 8-byte Reload
	callq	boundaryStart
	testl	%eax, %eax
	je	.LBB0_68
# BB#67:                                # %.thread.i.i
                                        #   in Loop: Header=BB0_65 Depth=1
	xorl	%eax, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	$.L.str.245, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	testb	%bl, %bl
	jne	.LBB0_71
	jmp	.LBB0_77
.LBB0_68:                               #   in Loop: Header=BB0_65 Depth=1
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
.LBB0_69:                               #   in Loop: Header=BB0_65 Depth=1
	testb	$1, 80(%rsp)            # 1-byte Folded Reload
	jne	.LBB0_74
# BB#70:                                #   in Loop: Header=BB0_65 Depth=1
	testb	%bl, %bl
	je	.LBB0_77
.LBB0_71:                               #   in Loop: Header=BB0_65 Depth=1
	movq	%rbp, %rdi
	callq	isuuencodebegin
	testl	%eax, %eax
	je	.LBB0_80
# BB#72:                                #   in Loop: Header=BB0_65 Depth=1
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r12, %rdx
	movq	%r15, %rcx
	callq	uudecodeFile
	testl	%eax, %eax
	js	.LBB0_106
# BB#73:                                #   in Loop: Header=BB0_65 Depth=1
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	jmp	.LBB0_136
	.p2align	4, 0x90
.LBB0_74:                               #   in Loop: Header=BB0_65 Depth=1
	movl	$.L.str.246, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	movq	(%rsp), %rdx            # 8-byte Reload
	callq	cli_dbgmsg
	testq	%rbp, %rbp
	je	.LBB0_86
# BB#75:                                #   in Loop: Header=BB0_65 Depth=1
	callq	__ctype_b_loc
	movq	%rax, %r15
	movq	(%r15), %rax
	movzbl	(%rbp), %ecx
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB0_87
# BB#76:                                #   in Loop: Header=BB0_65 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
.LBB0_86:                               #   in Loop: Header=BB0_65 Depth=1
	movq	(%rsp), %rsi            # 8-byte Reload
	testb	%bl, %bl
	je	.LBB0_99
	jmp	.LBB0_102
	.p2align	4, 0x90
.LBB0_77:                               #   in Loop: Header=BB0_65 Depth=1
	movb	$1, %bl
	testb	$1, 96(%rsp)            # 1-byte Folded Reload
	je	.LBB0_85
# BB#78:                                #   in Loop: Header=BB0_65 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	messageGetMimeType
	cmpl	$6, %eax
	je	.LBB0_85
# BB#79:                                #   in Loop: Header=BB0_65 Depth=1
	movl	$.L.str.247, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB0_136
.LBB0_80:                               #   in Loop: Header=BB0_65 Depth=1
	testb	$1, 16(%rsp)            # 1-byte Folded Reload
	je	.LBB0_84
# BB#81:                                #   in Loop: Header=BB0_65 Depth=1
	movl	$.L.str.71, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	movl	$.L.str.72, %esi
	movl	$12, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB0_136
# BB#82:                                # %newline_in_header.exit.i.i
                                        #   in Loop: Header=BB0_65 Depth=1
	movl	$.L.str.73, %esi
	movl	$6, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB0_136
# BB#83:                                #   in Loop: Header=BB0_65 Depth=1
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
.LBB0_84:                               #   in Loop: Header=BB0_65 Depth=1
	xorl	%ebx, %ebx
.LBB0_85:                               #   in Loop: Header=BB0_65 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rsi
	callq	messageAddStr
	testl	%eax, %eax
	movb	%bl, %al
	movq	%rax, 96(%rsp)          # 8-byte Spill
	jns	.LBB0_136
	jmp	.LBB0_137
.LBB0_87:                               #   in Loop: Header=BB0_65 Depth=1
	movq	%r13, %rsi
	movq	%r14, %r13
	movq	%r12, %r14
	leaq	2160(%rsp), %r12
	movq	%r12, %rdi
	callq	strcpy
	movq	%r12, %rdi
	movq	%r14, %r12
	callq	strlen
	testl	%eax, %eax
	movq	(%rsp), %rsi            # 8-byte Reload
	js	.LBB0_96
# BB#88:                                #   in Loop: Header=BB0_65 Depth=1
	leal	1(%rax), %ecx
	cmpl	%eax, %ecx
	jle	.LBB0_96
# BB#89:                                #   in Loop: Header=BB0_65 Depth=1
	cltq
	movb	2160(%rsp,%rax), %cl
	.p2align	4, 0x90
.LBB0_90:                               #   Parent Loop BB0_65 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	%cl, %cl
	je	.LBB0_92
# BB#91:                                #   in Loop: Header=BB0_90 Depth=2
	movb	$0, 2160(%rsp,%rax)
.LBB0_92:                               #   in Loop: Header=BB0_90 Depth=2
	testl	%eax, %eax
	jle	.LBB0_96
# BB#93:                                #   in Loop: Header=BB0_90 Depth=2
	movq	(%r15), %rdx
	movsbq	2159(%rsp,%rax), %rcx
	cmpw	$0, (%rdx,%rcx,2)
	js	.LBB0_96
# BB#94:                                # %switch.early.test.i.i.i.i
                                        #   in Loop: Header=BB0_90 Depth=2
	cmpb	$13, %cl
	je	.LBB0_96
# BB#95:                                # %switch.early.test.i.i.i.i
                                        #   in Loop: Header=BB0_90 Depth=2
	decq	%rax
	cmpb	$10, %cl
	jne	.LBB0_90
.LBB0_96:                               # %strstrip.exit.i.i
                                        #   in Loop: Header=BB0_65 Depth=1
	cmpb	$0, 2160(%rsp)
	movq	%r13, %r14
	je	.LBB0_113
# BB#97:                                #   in Loop: Header=BB0_65 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	1152(%rsp), %r13
.LBB0_98:                               #   in Loop: Header=BB0_65 Depth=1
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r15           # 8-byte Reload
	testb	%bl, %bl
	jne	.LBB0_102
.LBB0_99:                               #   in Loop: Header=BB0_65 Depth=1
	testq	%rsi, %rsi
	jne	.LBB0_102
# BB#100:                               #   in Loop: Header=BB0_65 Depth=1
	testb	$1, %r14b
	je	.LBB0_135
# BB#101:                               #   in Loop: Header=BB0_65 Depth=1
	xorl	%eax, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movb	$1, %al
	movl	%eax, 16(%rsp)          # 4-byte Spill
	jmp	.LBB0_135
.LBB0_102:                              #   in Loop: Header=BB0_65 Depth=1
	testq	%rsi, %rsi
	je	.LBB0_108
# BB#103:                               #   in Loop: Header=BB0_65 Depth=1
	movq	%rsi, (%rsp)            # 8-byte Spill
	testq	%rbp, %rbp
	je	.LBB0_118
# BB#104:                               #   in Loop: Header=BB0_65 Depth=1
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %r15
	addq	24(%rsp), %r15          # 8-byte Folded Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r15, %rsi
	callq	cli_realloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_57
# BB#105:                               #   in Loop: Header=BB0_65 Depth=1
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	strcat
	movq	%rbx, (%rsp)            # 8-byte Spill
	jmp	.LBB0_119
.LBB0_106:                              #   in Loop: Header=BB0_65 Depth=1
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	messageAddStr
	testl	%eax, %eax
	js	.LBB0_137
# BB#107:                               #   in Loop: Header=BB0_65 Depth=1
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	jmp	.LBB0_136
.LBB0_108:                              #   in Loop: Header=BB0_65 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movsbq	(%rbp), %rcx
	testb	$1, (%rax,%rcx,2)
	jne	.LBB0_135
# BB#109:                               #   in Loop: Header=BB0_65 Depth=1
	movq	%r14, %r13
	movl	$58, %esi
	movq	%rbp, %rdi
	callq	strchr
	testq	%rax, %rax
	leaq	2160(%rsp), %r14
	je	.LBB0_131
# BB#110:                               #   in Loop: Header=BB0_65 Depth=1
	xorl	%esi, %esi
	movl	$.L.str.40, %edx
	movq	%rbp, %rdi
	movq	%r14, %rcx
	callq	cli_strtokbuf
	testq	%rax, %rax
	je	.LBB0_131
# BB#111:                               #   in Loop: Header=BB0_65 Depth=1
	movq	%r14, %rdi
	leaq	3168(%rsp), %rsi
	callq	rfc822comments
	testq	%rax, %rax
	cmoveq	%r14, %rax
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	tableFind
	movl	%eax, %ebx
	decl	%eax
	cmpl	$3, %eax
	movl	%ebx, 68(%rsp)          # 4-byte Spill
	jae	.LBB0_58
# BB#112:                               #   in Loop: Header=BB0_65 Depth=1
	movq	%rbp, %rdi
	callq	cli_strdup
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %r15
	incq	%r15
	movb	$1, %r14b
	leaq	1152(%rsp), %r13
	jmp	.LBB0_119
.LBB0_113:                              #   in Loop: Header=BB0_65 Depth=1
	testq	%rsi, %rsi
	leaq	1152(%rsp), %r13
	je	.LBB0_116
# BB#114:                               #   in Loop: Header=BB0_65 Depth=1
	movq	%r14, 88(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	%rsi, %r14
	callq	parseEmailHeader
	testl	%eax, %eax
	js	.LBB0_51
# BB#115:                               #   in Loop: Header=BB0_65 Depth=1
	movq	%r14, %rdi
	callq	free
	movq	88(%rsp), %r14          # 8-byte Reload
.LBB0_116:                              #   in Loop: Header=BB0_65 Depth=1
	movb	$1, %r15b
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	je	.LBB0_50
# BB#117:                               #   in Loop: Header=BB0_65 Depth=1
	xorl	%esi, %esi
	jmp	.LBB0_52
.LBB0_118:                              #   in Loop: Header=BB0_65 Depth=1
	movq	24(%rsp), %r15          # 8-byte Reload
.LBB0_119:                              #   in Loop: Header=BB0_65 Depth=1
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	%r12, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	je	.LBB0_121
# BB#120:                               #   in Loop: Header=BB0_65 Depth=1
	movl	%ebx, %edi
	movq	%r12, %rsi
	callq	ungetc
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	testb	$1, (%rax,%rcx,2)
	jne	.LBB0_130
.LBB0_121:                              #   in Loop: Header=BB0_65 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	cmpb	$59, -2(%rdi,%r15)
	jne	.LBB0_123
# BB#122:                               #   in Loop: Header=BB0_65 Depth=1
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	56(%rsp), %r12          # 8-byte Reload
	jmp	.LBB0_136
.LBB0_123:                              #   in Loop: Header=BB0_65 Depth=1
	testq	%rbp, %rbp
	movq	56(%rsp), %r12          # 8-byte Reload
	je	.LBB0_128
# BB#124:                               #   in Loop: Header=BB0_65 Depth=1
	movb	(%rdi), %dl
	testb	%dl, %dl
	je	.LBB0_128
# BB#125:                               # %.lr.ph.i.i.i.preheader
                                        #   in Loop: Header=BB0_65 Depth=1
	leaq	1(%rdi), %rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_126:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB0_65 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%esi, %esi
	cmpb	$34, %dl
	sete	%sil
	addl	%esi, %eax
	movzbl	(%rcx), %edx
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB0_126
# BB#127:                               # %count_quotes.exit.i.i
                                        #   in Loop: Header=BB0_65 Depth=1
	testb	$1, %al
	jne	.LBB0_57
.LBB0_128:                              # %count_quotes.exit.thread.i.i
                                        #   in Loop: Header=BB0_65 Depth=1
	xorl	%esi, %esi
	movq	%rdi, %rbp
	callq	rfc822comments
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_53
# BB#129:                               #   in Loop: Header=BB0_65 Depth=1
	movq	%rbp, %rdi
	callq	free
	movq	32(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_54
.LBB0_130:                              #   in Loop: Header=BB0_65 Depth=1
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	%r12, %r15
	movq	56(%rsp), %r12          # 8-byte Reload
	jmp	.LBB0_136
.LBB0_131:                              #   in Loop: Header=BB0_65 Depth=1
	movl	$.L.str.4, %esi
	movl	$5, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	movb	$1, %al
	je	.LBB0_133
# BB#132:                               #   in Loop: Header=BB0_65 Depth=1
	movl	%r13d, %eax
.LBB0_133:                              #   in Loop: Header=BB0_65 Depth=1
	movb	%al, %r14b
.LBB0_134:                              #   in Loop: Header=BB0_65 Depth=1
	leaq	1152(%rsp), %r13
	.p2align	4, 0x90
.LBB0_135:                              #   in Loop: Header=BB0_65 Depth=1
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB0_136:                              # %.thread173.i.i
                                        #   in Loop: Header=BB0_65 Depth=1
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	getline_from_mbox
	testq	%rax, %rax
	jne	.LBB0_65
.LBB0_137:
	movq	48(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB0_139
# BB#138:
	callq	free
.LBB0_139:
	movq	(%rsp), %rcx            # 8-byte Reload
	testq	%rcx, %rcx
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_144
# BB#140:
	movl	68(%rsp), %eax          # 4-byte Reload
	decl	%eax
	cmpl	$2, %eax
	ja	.LBB0_143
# BB#141:
	movb	(%rcx), %al
	testb	%al, %al
	je	.LBB0_143
# BB#142:
	movl	$.L.str.248, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	cli_dbgmsg
	movq	(%rsp), %rcx            # 8-byte Reload
.LBB0_143:
	movq	%rcx, %rdi
	callq	free
.LBB0_144:
	testb	$1, %r14b
	jne	.LBB0_146
# BB#145:
	movq	%rbp, %rdi
	callq	messageDestroy
	xorl	%ebp, %ebp
	movl	$.L.str.249, %edi
	jmp	.LBB0_147
.LBB0_146:
	movl	$.L.str.250, %edi
.LBB0_147:                              # %parseEmailFile.exit.i
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_148:                              # %parseEmailFile.exit.i
	movq	%r15, %rdi
	callq	fclose
	testq	%rbp, %rbp
	je	.LBB0_153
.LBB0_149:
	movq	%rbp, %rdi
	callq	messageGetBody
	testq	%rax, %rax
	je	.LBB0_152
# BB#150:
	movq	%rbp, %rdi
	movq	%r13, %rsi
	callq	messageSetCTX
	leaq	104(%rsp), %rdx
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	movq	%rbp, %rdi
	callq	parseEmailBody
	cmpl	$5, %eax
	ja	.LBB0_152
# BB#151:                               # %switch.hole_check.i
	movl	$57, %ecx
	btl	%eax, %ecx
	jb	.LBB0_158
.LBB0_152:                              # %.thread120.i
	movq	%rbp, %rdi
	callq	messageDestroy
.LBB0_153:                              # %.thread119.i
	xorl	%ebx, %ebx
	cmpl	$0, 52(%r13)
	je	.LBB0_159
# BB#154:
	movq	(%r13), %rax
	cmpq	$0, (%rax)
	jne	.LBB0_159
# BB#155:
	movq	$.L.str.11, (%rax)
	movl	$0, 52(%r13)
.LBB0_156:
	movl	$1, %ebx
	jmp	.LBB0_159
.LBB0_158:
	cltq
	movl	.Lswitch.table(,%rax,4), %ebx
	movq	%rbp, %rdi
	callq	messageDestroy
.LBB0_159:
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_dbgmsg
.LBB0_160:
	movl	%ebx, %eax
	addq	$4184, %rsp             # imm = 0x1058
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_161:                              # %initialiseTables.exit.loopexit.i
	movq	cli_parse_mbox.rfc821(%rip), %rax
	jmp	.LBB0_4
.LBB0_162:                              # %.thread121.i
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	cli_dbgmsg
	movq	%rbp, %rdi
	callq	fclose
	movq	%rbx, %rdi
	callq	messageDestroy
	jmp	.LBB0_156
.Lfunc_end0:
	.size	cli_mbox, .Lfunc_end0-cli_mbox
	.cfi_endproc

	.globl	strstrip
	.p2align	4, 0x90
	.type	strstrip,@function
strstrip:                               # @strstrip
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB1_11
# BB#1:
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbx
	testl	%ebx, %ebx
	js	.LBB1_11
# BB#2:
	leal	1(%rbx), %eax
	cmpl	%ebx, %eax
	jle	.LBB1_12
# BB#3:
	movslq	%ebx, %rcx
	movb	(%r14,%rcx), %al
	.p2align	4, 0x90
.LBB1_4:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rbx
	testb	%al, %al
	je	.LBB1_6
# BB#5:                                 #   in Loop: Header=BB1_4 Depth=1
	movb	$0, (%r14,%rbx)
.LBB1_6:                                #   in Loop: Header=BB1_4 Depth=1
	testl	%ebx, %ebx
	jle	.LBB1_12
# BB#7:                                 #   in Loop: Header=BB1_4 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movsbq	-1(%r14,%rbx), %rax
	cmpw	$0, (%rcx,%rax,2)
	js	.LBB1_12
# BB#8:                                 # %switch.early.test.i
                                        #   in Loop: Header=BB1_4 Depth=1
	cmpb	$13, %al
	je	.LBB1_12
# BB#9:                                 # %switch.early.test.i
                                        #   in Loop: Header=BB1_4 Depth=1
	leaq	-1(%rbx), %rcx
	cmpb	$10, %al
	jne	.LBB1_4
	jmp	.LBB1_12
.LBB1_11:
	xorl	%ebx, %ebx
.LBB1_12:                               # %strip.exit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	strstrip, .Lfunc_end1-strstrip
	.cfi_endproc

	.p2align	4, 0x90
	.type	parseEmailHeaders,@function
parseEmailHeaders:                      # @parseEmailHeaders
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	subq	$1064, %rsp             # imm = 0x428
.Lcfi24:
	.cfi_def_cfa_offset 1120
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	xorl	%r14d, %r14d
	movl	$.L.str.35, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testq	%rbx, %rbx
	je	.LBB2_54
# BB#1:
	callq	messageCreate
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	messageGetBody
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB2_51
# BB#2:                                 # %.lr.ph
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movb	$1, %r14b
	movl	$-1, 12(%rsp)           # 4-byte Folded Spill
	movl	$0, %ebx
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	jmp	.LBB2_3
.LBB2_20:                               #   in Loop: Header=BB2_3 Depth=1
	testb	$1, (%rsp)              # 1-byte Folded Reload
	jne	.LBB2_21
# BB#22:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$.L.str.47, %esi
	movq	%r13, %rdi
	callq	strcasecmp
	movb	$1, %cl
	movq	%rcx, (%rsp)            # 8-byte Spill
	testl	%eax, %eax
	movq	24(%rsp), %rbx          # 8-byte Reload
	je	.LBB2_26
# BB#23:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$.L.str.48, %esi
	movq	%r13, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB2_26
# BB#24:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$.L.str.49, %esi
	movq	%r13, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	sete	%al
	jmp	.LBB2_25
.LBB2_21:                               #   in Loop: Header=BB2_3 Depth=1
	movq	24(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB2_26
	.p2align	4, 0x90
.LBB2_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_34 Depth 2
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_4
# BB#5:                                 #   in Loop: Header=BB2_3 Depth=1
	callq	lineGetData
	movq	%rax, %r12
	testb	$1, %r14b
	je	.LBB2_41
	jmp	.LBB2_7
	.p2align	4, 0x90
.LBB2_4:                                #   in Loop: Header=BB2_3 Depth=1
	xorl	%r12d, %r12d
	testb	$1, %r14b
	jne	.LBB2_7
.LBB2_41:                               #   in Loop: Header=BB2_3 Depth=1
	testq	%r12, %r12
	je	.LBB2_27
# BB#42:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$.L.str.71, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_dbgmsg
	movl	$.L.str.72, %esi
	movl	$12, %edx
	movq	%r12, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB2_27
# BB#43:                                # %newline_in_header.exit
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$.L.str.73, %esi
	movl	$6, %edx
	movq	%r12, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB2_27
	jmp	.LBB2_44
	.p2align	4, 0x90
.LBB2_7:                                #   in Loop: Header=BB2_3 Depth=1
	testq	%r12, %r12
	movl	$.L.str.37, %esi
	cmovneq	%r12, %rsi
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testq	%r12, %r12
	je	.LBB2_8
# BB#10:                                #   in Loop: Header=BB2_3 Depth=1
	testq	%r13, %r13
	je	.LBB2_11
# BB#29:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %r15
	addq	%rbx, %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	cli_realloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB2_40
# BB#30:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	strcat
	movq	%rbx, %r13
.LBB2_31:                               #   in Loop: Header=BB2_3 Depth=1
	movq	%rbp, %rdi
	callq	next_is_folded_header
	testb	%al, %al
	jne	.LBB2_40
# BB#32:                                #   in Loop: Header=BB2_3 Depth=1
	movq	(%rbp), %rdi
	callq	lineUnlink
	movq	$0, (%rbp)
	movb	(%r13), %dl
	testb	%dl, %dl
	je	.LBB2_36
# BB#33:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%r13), %rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_34:                               # %.lr.ph.i
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%esi, %esi
	cmpb	$34, %dl
	sete	%sil
	addl	%esi, %eax
	movzbl	(%rcx), %edx
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB2_34
# BB#35:                                # %count_quotes.exit
                                        #   in Loop: Header=BB2_3 Depth=1
	testb	$1, %al
	jne	.LBB2_40
.LBB2_36:                               # %count_quotes.exit.thread
                                        #   in Loop: Header=BB2_3 Depth=1
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	rfc822comments
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB2_38
# BB#37:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%r13, %rdi
	callq	free
	movq	%rbx, %r13
.LBB2_38:                               #   in Loop: Header=BB2_3 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	parseEmailHeader
	testl	%eax, %eax
	js	.LBB2_40
# BB#39:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%r13, %rdi
	callq	free
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_40:                               # %.thread119
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%r15, %rbx
	jmp	.LBB2_27
.LBB2_8:                                #   in Loop: Header=BB2_3 Depth=1
	xorl	%r14d, %r14d
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testb	$1, (%rsp)              # 1-byte Folded Reload
	jne	.LBB2_27
	jmp	.LBB2_9
.LBB2_11:                               #   in Loop: Header=BB2_3 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movsbq	(%r12), %rcx
	testb	$1, (%rax,%rcx,2)
	jne	.LBB2_26
# BB#12:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$58, %esi
	movq	%r12, %rdi
	callq	strchr
	testq	%rax, %rax
	leaq	48(%rsp), %r13
	je	.LBB2_14
# BB#13:                                #   in Loop: Header=BB2_3 Depth=1
	xorl	%esi, %esi
	movl	$.L.str.40, %edx
	movq	%r12, %rdi
	movq	%r13, %rcx
	callq	cli_strtokbuf
	testq	%rax, %rax
	je	.LBB2_14
# BB#17:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	rfc822comments
	movq	%rax, %rbx
	testq	%rbx, %rbx
	movq	%r13, %rsi
	cmovneq	%rbx, %rsi
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	tableFind
	movl	%eax, %r15d
	testq	%rbx, %rbx
	je	.LBB2_19
# BB#18:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%rbx, %rdi
	callq	free
.LBB2_19:                               #   in Loop: Header=BB2_3 Depth=1
	movl	%r15d, %eax
	decl	%eax
	cmpl	$3, %eax
	movl	%r15d, 12(%rsp)         # 4-byte Spill
	jae	.LBB2_20
# BB#28:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%r12, %rdi
	callq	cli_strdup
	movq	%rax, %r13
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %r15
	incq	%r15
	movb	$1, %al
	movq	%rax, (%rsp)            # 8-byte Spill
	jmp	.LBB2_31
.LBB2_14:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$.L.str.4, %esi
	movl	$5, %edx
	movq	%r12, %rdi
	callq	strncmp
	testl	%eax, %eax
	movb	$1, %al
	je	.LBB2_16
# BB#15:                                #   in Loop: Header=BB2_3 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
.LBB2_16:                               #   in Loop: Header=BB2_3 Depth=1
                                        # kill: %AL<def> %AL<kill> %RAX<def>
.LBB2_25:                               # %.thread
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB2_26:                               # %.thread
                                        #   in Loop: Header=BB2_3 Depth=1
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_27:                               # %.thread119
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	8(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB2_3
# BB#45:                                # %.loopexit
	testq	%r13, %r13
	je	.LBB2_50
.LBB2_46:
	movl	12(%rsp), %eax          # 4-byte Reload
	decl	%eax
	cmpl	$2, %eax
	ja	.LBB2_49
# BB#47:
	movb	(%r13), %al
	testb	%al, %al
	je	.LBB2_49
# BB#48:
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	cli_dbgmsg
.LBB2_49:
	movq	%r13, %rdi
	callq	free
.LBB2_50:
	testb	$1, (%rsp)              # 1-byte Folded Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	jne	.LBB2_52
.LBB2_51:                               # %.thread161
	movq	%r14, %rdi
	callq	messageDestroy
	xorl	%r14d, %r14d
	movl	$.L.str.43, %edi
	jmp	.LBB2_53
.LBB2_52:
	movl	$.L.str.44, %edi
.LBB2_53:
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB2_54:
	movq	%r14, %rax
	addq	$1064, %rsp             # imm = 0x428
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_44:
	movl	$.L.str.41, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rsi
	movq	40(%rsp), %rdx          # 8-byte Reload
	callq	messageMoveText
	testq	%r13, %r13
	jne	.LBB2_46
	jmp	.LBB2_50
.LBB2_9:
	movl	$.L.str.39, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testq	%r13, %r13
	jne	.LBB2_46
	jmp	.LBB2_50
.Lfunc_end2:
	.size	parseEmailHeaders, .Lfunc_end2-parseEmailHeaders
	.cfi_endproc

	.p2align	4, 0x90
	.type	parseEmailBody,@function
parseEmailBody:                         # @parseEmailBody
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$1160, %rsp             # imm = 0x488
.Lcfi37:
	.cfi_def_cfa_offset 1216
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movq	%rdx, %rbp
	movq	%rsi, %r13
	movq	%rdi, %r15
	movq	%r13, 88(%rsp)
	movq	32(%rbp), %rax
	movq	24(%rax), %rcx
	testb	$8, 8(%rcx)
	jne	.LBB3_2
# BB#1:
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	jmp	.LBB3_3
.LBB3_2:
	movq	56(%rax), %rcx
	movb	24(%rcx), %cl
	andb	$1, %cl
	movl	%ecx, 16(%rsp)          # 4-byte Spill
.LBB3_3:
	movq	32(%rax), %rbx
	movl	8(%rbp), %esi
	movl	$.L.str.74, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testq	%rbx, %rbx
	je	.LBB3_11
# BB#4:
	movl	8(%rbx), %eax
	testl	%eax, %eax
	je	.LBB3_9
# BB#5:
	cmpl	%r12d, %eax
	jae	.LBB3_9
# BB#6:
	movq	32(%rbp), %rbx
	movl	$.L.str.75, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	cli_warnmsg
	movl	$4, %ebp
	testb	$1, 41(%rbx)
	je	.LBB3_149
# BB#7:
	movq	(%rbx), %rax
	movl	$3, %ebp
	testq	%rax, %rax
	je	.LBB3_149
# BB#8:
	movq	$.L.str.76, (%rax)
	jmp	.LBB3_149
.LBB3_9:
	movl	4(%rbx), %esi
	testl	%esi, %esi
	je	.LBB3_11
# BB#10:
	cmpl	%esi, 8(%rbp)
	jae	.LBB3_20
.LBB3_11:
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movl	$1, 12(%rsp)
	movl	$1, %r14d
	testq	%r15, %r15
	je	.LBB3_61
# BB#12:
	movq	%r15, %rdi
	callq	messageGetBody
	testq	%rax, %rax
	je	.LBB3_61
# BB#13:
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movl	$.L.str.78, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r15, %rdi
	callq	messageGetMimeType
	movl	%eax, %ebx
	movq	%r15, %rdi
	callq	messageGetMimeSubtype
	movq	%rax, %rbp
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rdi
	movq	%rbp, %rsi
	callq	tableFind
	movl	%eax, %r12d
	cmpl	$6, %ebx
	jne	.LBB3_21
# BB#14:
	cmpl	$1, %r12d
	jne	.LBB3_21
# BB#15:
	movl	$.L.str.79, %edi
.LBB3_16:                               # %.thread608
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.37, %esi
	movq	%r15, %rdi
	callq	messageSetMimeSubtype
.LBB3_17:                               # %.thread608
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movl	$.L.str.83, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	textAddMessage
	movq	%rax, %rbx
	movq	%rbx, 88(%rsp)
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	je	.LBB3_63
.LBB3_18:
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsi), %rax
	cmpb	$0, 40(%rax)
	sets	%al
	cmpl	$3, %r12d
	sete	%cl
	andb	%al, %cl
	orb	16(%rsp), %cl           # 1-byte Folded Reload
	cmpb	$1, %cl
	jne	.LBB3_48
# BB#19:
	xorl	%ecx, %ecx
	cmpl	$3, %r12d
	sete	%cl
	leaq	12(%rsp), %rdx
	movq	%r15, %rdi
	callq	checkURLs
	movl	12(%rsp), %r14d
	cmpl	$3, %r14d
	sete	%al
	movl	%eax, 8(%rsp)           # 4-byte Spill
	testq	%r13, %r13
	je	.LBB3_64
	jmp	.LBB3_116
.LBB3_20:
	movl	$.L.str.77, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$5, %ebp
	jmp	.LBB3_149
.LBB3_21:
	cmpl	$4, %ebx
	jne	.LBB3_32
# BB#22:
	movl	$.L.str.80, %esi
	movq	%rbp, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB3_53
# BB#23:                                # %.thread609
	movl	$.L.str.82, %edi
	movl	$4, %esi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r15, %rdi
	callq	messageGetEncoding
	cmpl	$4, %eax
	ja	.LBB3_25
# BB#24:                                # %.thread609
	movl	$25, %ecx
	btl	%eax, %ecx
	jb	.LBB3_26
.LBB3_25:
	movl	$.L.str.117, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB3_26:
	movl	$0, 12(%rsp)
	movl	$.L.str.118, %esi
	movq	%rbp, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	movq	40(%rsp), %r12          # 8-byte Reload
	je	.LBB3_49
# BB#27:
	movl	$.L.str.119, %esi
	movq	%rbp, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB3_49
# BB#28:
	movl	$.L.str.121, %esi
	movq	%rbp, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB3_60
# BB#29:
	movl	$.L.str.122, %esi
	movq	%rbp, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB3_156
# BB#30:
	movl	$.L.str.123, %esi
	movq	%rbp, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB3_159
# BB#31:
	movq	%rbp, %rsi
	xorl	%ebp, %ebp
	movl	$.L.str.125, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB3_149
.LBB3_32:
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movl	$.L.str.82, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_dbgmsg
	cmpl	$7, %ebx
	ja	.LBB3_55
# BB#33:
	movl	%ebx, %eax
	movq	%r13, %rbx
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_34:
	movl	$.L.str.84, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.85, %esi
	movq	%r15, %rdi
	callq	messageFindArgument
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB3_158
# BB#35:
	cmpb	$0, (%rbp)
	jne	.LBB3_37
# BB#36:
	movl	$.L.str.87, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movl	$.L.str.21, %ebp
	movl	$.L.str.21, %esi
	movq	%r15, %rdi
	callq	messageSetMimeSubtype
.LBB3_37:
	movq	%r15, %rdi
	callq	messageGetBody
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_160
# BB#38:
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
.LBB3_39:                               # %.preheader657
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_46
# BB#40:                                #   in Loop: Header=BB3_39 Depth=1
	callq	lineGetData
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	boundaryStart
	movl	$1, 8(%rsp)             # 4-byte Folded Spill
	testl	%eax, %eax
	jne	.LBB3_163
# BB#41:                                #   in Loop: Header=BB3_39 Depth=1
	movq	%r15, %rdi
	callq	binhexBegin
	cmpq	%rbx, %rax
	je	.LBB3_45
# BB#42:                                #   in Loop: Header=BB3_39 Depth=1
	cmpq	$0, 8(%rbx)
	je	.LBB3_46
# BB#43:                                #   in Loop: Header=BB3_39 Depth=1
	movq	%r15, %rdi
	callq	encodingLine
	cmpq	8(%rbx), %rax
	jne	.LBB3_46
# BB#44:                                #   in Loop: Header=BB3_39 Depth=1
	movq	(%rax), %rdi
	callq	lineGetData
	movq	%rax, %rcx
	movl	$.L.str.89, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	cli_dbgmsg
	movq	%r15, %rdi
	callq	messageGetEncoding
	testl	%eax, %eax
	jne	.LBB3_46
	jmp	.LBB3_165
.LBB3_45:                               #   in Loop: Header=BB3_39 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%r15, %rsi
	callq	exportBinhexMessage
	testb	%al, %al
	jne	.LBB3_164
.LBB3_46:                               #   in Loop: Header=BB3_39 Depth=1
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_39
# BB#47:
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movl	$.L.str.90, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_dbgmsg
	jmp	.LBB3_161
.LBB3_49:
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	16(%rbp), %rsi
	movq	%r15, %rdi
	callq	parseEmailHeaders
	movq	%rax, %rbx
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	je	.LBB3_54
# BB#50:
	movl	$.L.str.120, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	32(%rbp), %rsi
	movq	%rbx, %rdi
	callq	messageSetCTX
	movq	%r15, %rdi
	callq	messageReset
	movq	%rbx, %rdi
	callq	messageGetBody
	testq	%rax, %rax
	je	.LBB3_52
# BB#51:
	incl	%r12d
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	movl	%r12d, %ecx
	callq	parseEmailBody
	movl	%eax, %r14d
	movl	%r14d, 12(%rsp)
.LBB3_52:
	movq	%rbx, %rdi
	callq	messageDestroy
	jmp	.LBB3_61
.LBB3_53:
	movl	$.L.str.81, %edi
	jmp	.LBB3_16
.LBB3_54:
	movq	%r13, %rbx
.LBB3_48:
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	testq	%r13, %r13
	je	.LBB3_64
	jmp	.LBB3_116
.LBB3_55:
	movl	$.L.str.126, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB3_56:
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	movl	$1, %r14d
	movl	$1, %edx
	movq	%r15, %rdi
	callq	messageToFileblob
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_61
# BB#57:
	movl	$.L.str.127, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	callq	fileblobScanAndDestroy
	movl	$1, %r14d
	cmpl	$1, %eax
	jne	.LBB3_59
# BB#58:
	movl	$3, 12(%rsp)
	movl	$3, %r14d
.LBB3_59:
	movq	32(%rsp), %rax          # 8-byte Reload
	incl	8(%rax)
	movq	%r15, %rdi
	callq	messageReset
	jmp	.LBB3_61
.LBB3_60:
	movl	$1, 12(%rsp)
.LBB3_61:                               # %.thread642
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
.LBB3_62:                               # %.thread642
	movq	%r13, %rbx
.LBB3_63:                               # %.thread642
	testq	%r13, %r13
	jne	.LBB3_116
.LBB3_64:                               # %.thread642
	testq	%rbx, %rbx
	je	.LBB3_116
# BB#65:                                # %.preheader649
	movq	%r15, 72(%rsp)          # 8-byte Spill
	movl	$3, %r15d
	cmpl	$3, %r14d
	je	.LBB3_115
# BB#66:                                # %.lr.ph696
	movl	%r14d, %r15d
	xorl	%r12d, %r12d
	movq	%rbx, %r13
	jmp	.LBB3_107
	.p2align	4, 0x90
.LBB3_67:                               #   in Loop: Header=BB3_107 Depth=1
	movq	(%rbx), %rdi
	callq	lineGetData
	movq	%rax, %rdi
	callq	isBounceStart
	testb	%al, %al
	je	.LBB3_69
# BB#68:                                #   in Loop: Header=BB3_107 Depth=1
	movb	$1, %r12b
	jmp	.LBB3_87
	.p2align	4, 0x90
.LBB3_69:                               # %.lr.ph
                                        #   in Loop: Header=BB3_107 Depth=1
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	je	.LBB3_74
	.p2align	4, 0x90
.LBB3_70:                               # %.lr.ph.split.us
                                        #   Parent Loop BB3_107 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_86
# BB#71:                                #   in Loop: Header=BB3_70 Depth=2
	callq	lineGetData
	movq	%rax, %rbp
	movl	$.L.str.129, %esi
	movl	$13, %edx
	movq	%rbp, %rdi
	callq	strncasecmp
	testl	%eax, %eax
	jne	.LBB3_73
# BB#72:                                #   in Loop: Header=BB3_70 Depth=2
	movl	$.L.str.130, %esi
	movq	%rbp, %rdi
	callq	strcasestr
	testq	%rax, %rax
	je	.LBB3_79
.LBB3_73:                               #   in Loop: Header=BB3_70 Depth=2
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_70
	jmp	.LBB3_113
	.p2align	4, 0x90
.LBB3_74:                               # %.lr.ph.split
                                        #   Parent Loop BB3_107 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_86
# BB#75:                                #   in Loop: Header=BB3_74 Depth=2
	callq	lineGetData
	movq	%rax, %rbp
	movl	$.L.str.129, %esi
	movl	$13, %edx
	movq	%rbp, %rdi
	callq	strncasecmp
	testl	%eax, %eax
	jne	.LBB3_78
# BB#76:                                #   in Loop: Header=BB3_74 Depth=2
	movl	$.L.str.130, %esi
	movq	%rbp, %rdi
	callq	strcasestr
	testq	%rax, %rax
	jne	.LBB3_78
# BB#77:                                #   in Loop: Header=BB3_74 Depth=2
	movl	$.L.str.131, %esi
	movq	%rbp, %rdi
	callq	strcasestr
	testq	%rax, %rax
	je	.LBB3_79
	.p2align	4, 0x90
.LBB3_78:                               #   in Loop: Header=BB3_74 Depth=2
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_74
	jmp	.LBB3_113
.LBB3_79:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB3_107 Depth=1
	cmpq	$0, (%rbx)
	je	.LBB3_86
	.p2align	4, 0x90
.LBB3_80:                               # %.lr.ph693
                                        #   Parent Loop BB3_107 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_85
# BB#81:                                #   in Loop: Header=BB3_80 Depth=2
	callq	lineGetData
	movq	%rax, %rbp
	movl	$.L.str.129, %esi
	movl	$13, %edx
	movq	%rbp, %rdi
	callq	strncasecmp
	testl	%eax, %eax
	jne	.LBB3_85
# BB#82:                                #   in Loop: Header=BB3_80 Depth=2
	movl	$.L.str.133, %esi
	movq	%rbp, %rdi
	callq	strstr
	testq	%rax, %rax
	jne	.LBB3_85
# BB#83:                                #   in Loop: Header=BB3_80 Depth=2
	movl	$.L.str.134, %esi
	movq	%rbp, %rdi
	callq	strstr
	testq	%rax, %rax
	jne	.LBB3_85
# BB#84:                                #   in Loop: Header=BB3_80 Depth=2
	movl	$.L.str.130, %esi
	movq	%rbp, %rdi
	callq	strstr
	testq	%rax, %rax
	je	.LBB3_90
	.p2align	4, 0x90
.LBB3_85:                               # %.critedge589
                                        #   in Loop: Header=BB3_80 Depth=2
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_80
	jmp	.LBB3_113
.LBB3_86:                               # %.thread643
                                        #   in Loop: Header=BB3_107 Depth=1
	movl	$.L.str.132, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rbx, %r13
.LBB3_87:                               #   in Loop: Header=BB3_107 Depth=1
	movq	%r14, %rbx
.LBB3_88:                               #   in Loop: Header=BB3_107 Depth=1
	movq	8(%r13), %r13
	testq	%r13, %r13
	je	.LBB3_115
# BB#89:                                #   in Loop: Header=BB3_107 Depth=1
	cmpl	$3, %r15d
	jne	.LBB3_107
	jmp	.LBB3_115
.LBB3_90:                               #   in Loop: Header=BB3_107 Depth=1
	callq	fileblobCreate
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB3_114
# BB#91:                                #   in Loop: Header=BB3_107 Depth=1
	movl	$.L.str.136, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %rsi
	movl	$.L.str.137, %edx
	movq	%rbp, %rdi
	callq	fileblobSetFilename
	movl	$.L.str.138, %esi
	movl	$28, %edx
	movq	%rbp, %rdi
	callq	fileblobAddData
	movq	32(%rbx), %rsi
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	callq	fileblobSetCTX
	movb	$1, %al
	movl	%eax, 24(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
.LBB3_92:                               #   Parent Loop BB3_107 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB3_94
# BB#93:                                #   in Loop: Header=BB3_92 Depth=2
	callq	lineGetData
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	fileblobAddData
	jmp	.LBB3_97
.LBB3_94:                               #   in Loop: Header=BB3_92 Depth=2
	movl	24(%rsp), %eax          # 4-byte Reload
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	andb	$1, %al
	movq	40(%rsp), %rbx          # 8-byte Reload
	je	.LBB3_96
# BB#95:                                #   in Loop: Header=BB3_92 Depth=2
	movl	$0, 24(%rsp)            # 4-byte Folded Spill
.LBB3_96:                               #   in Loop: Header=BB3_92 Depth=2
	testb	%al, %al
	movq	80(%rsp), %rax          # 8-byte Reload
	cmovneq	%r13, %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
.LBB3_97:                               #   in Loop: Header=BB3_92 Depth=2
	movl	$.L.str.139, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	fileblobAddData
	movq	%r13, %rax
	movq	8(%rax), %r13
	testq	%r13, %r13
	je	.LBB3_102
# BB#98:                                #   in Loop: Header=BB3_92 Depth=2
	testb	$1, 24(%rsp)            # 1-byte Folded Reload
	jne	.LBB3_101
# BB#99:                                #   in Loop: Header=BB3_92 Depth=2
	movq	(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB3_101
# BB#100:                               #   in Loop: Header=BB3_92 Depth=2
	callq	lineGetData
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	isBounceStart
	testb	%al, %al
	jne	.LBB3_103
.LBB3_101:                              #   in Loop: Header=BB3_92 Depth=2
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	fileblobInfected
	testl	%eax, %eax
	je	.LBB3_92
	jmp	.LBB3_104
.LBB3_102:                              #   in Loop: Header=BB3_107 Depth=1
	movq	%rax, %r13
	jmp	.LBB3_104
.LBB3_103:                              #   in Loop: Header=BB3_107 Depth=1
	movl	$.L.str.140, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	movb	$1, %r12b
.LBB3_104:                              # %.loopexit
                                        #   in Loop: Header=BB3_107 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	fileblobScanAndDestroy
	cmpl	$1, %eax
	movq	%r14, %rbx
	jne	.LBB3_106
# BB#105:                               #   in Loop: Header=BB3_107 Depth=1
	movl	$3, 12(%rsp)
	movl	$3, %r15d
.LBB3_106:                              #   in Loop: Header=BB3_107 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	incl	8(%rax)
	movq	80(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	cmovneq	%rax, %r13
	jmp	.LBB3_88
	.p2align	4, 0x90
.LBB3_107:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_74 Depth 2
                                        #     Child Loop BB3_70 Depth 2
                                        #     Child Loop BB3_80 Depth 2
                                        #     Child Loop BB3_92 Depth 2
	movq	(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB3_88
# BB#108:                               #   in Loop: Header=BB3_107 Depth=1
	testb	$1, %r12b
	jne	.LBB3_110
# BB#109:                               #   in Loop: Header=BB3_107 Depth=1
	callq	lineGetData
	movq	%rax, %rdi
	callq	isBounceStart
	testb	%al, %al
	je	.LBB3_88
	jmp	.LBB3_111
	.p2align	4, 0x90
.LBB3_110:                              #   in Loop: Header=BB3_107 Depth=1
	xorl	%r12d, %r12d
.LBB3_111:                              #   in Loop: Header=BB3_107 Depth=1
	movq	%rbx, %r14
	movq	8(%r13), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_67
	jmp	.LBB3_114
.LBB3_113:                              # %.critedge589.preheader._crit_edge
	movl	$.L.str.135, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB3_114:                              # %.loopexit650
	movq	%r14, %rbx
.LBB3_115:                              # %.loopexit650
	movq	%rbx, %rdi
	callq	textDestroy
	movq	$0, 88(%rsp)
	movl	%r15d, %r14d
	movq	72(%rsp), %r15          # 8-byte Reload
.LBB3_116:
	testq	%r15, %r15
	je	.LBB3_145
# BB#117:
	cmpl	$3, %r14d
	je	.LBB3_145
# BB#118:
	movq	%r15, %rdi
	callq	encodingLine
	testq	%rax, %rax
	je	.LBB3_134
# BB#119:
	movq	%r15, %rdi
	callq	bounceBegin
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB3_134
# BB#120:                               # %.lr.ph.i
	leaq	144(%rsp), %r15
	movq	%r12, %rbx
	movq	32(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_121:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	callq	lineGetData
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB3_130
# BB#122:                               #   in Loop: Header=BB3_121 Depth=1
	xorl	%esi, %esi
	movl	$.L.str.40, %edx
	movq	%rbp, %rdi
	movq	%r15, %rcx
	callq	cli_strtokbuf
	testq	%rax, %rax
	je	.LBB3_130
# BB#123:                               #   in Loop: Header=BB3_121 Depth=1
	movq	16(%r14), %rdi
	movq	%r15, %rsi
	callq	tableFind
	cmpl	$2, %eax
	je	.LBB3_128
# BB#124:                               #   in Loop: Header=BB3_121 Depth=1
	cmpl	$1, %eax
	je	.LBB3_150
# BB#125:                               #   in Loop: Header=BB3_121 Depth=1
	cmpl	$3, %eax
	je	.LBB3_151
# BB#126:                               #   in Loop: Header=BB3_121 Depth=1
	movl	$.L.str.47, %esi
	movq	%r15, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	movq	%rbx, %rax
	je	.LBB3_131
# BB#127:                               #   in Loop: Header=BB3_121 Depth=1
	movl	$.L.str.48, %esi
	movq	%r15, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	cmoveq	%rbx, %r12
	jmp	.LBB3_130
.LBB3_128:                              #   in Loop: Header=BB3_121 Depth=1
	movl	$.L.str.234, %esi
	movq	%rbp, %rdi
	callq	strstr
	testq	%rax, %rax
	jne	.LBB3_130
# BB#129:                               #   in Loop: Header=BB3_121 Depth=1
	movl	$.L.str.235, %esi
	movq	%rbp, %rdi
	callq	strstr
	testq	%rax, %rax
	je	.LBB3_151
	.p2align	4, 0x90
.LBB3_130:                              #   in Loop: Header=BB3_121 Depth=1
	movq	%r12, %rax
.LBB3_131:                              #   in Loop: Header=BB3_121 Depth=1
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %r12
	jne	.LBB3_121
.LBB3_132:                              # %.thread.i
	xorl	%ebx, %ebx
	movl	$.L.str.238, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB3_133:                              # %exportBounceMessage.exit
	movl	8(%rsp), %ecx           # 4-byte Reload
	xorl	%eax, %eax
	cmpl	$1, %ebx
	sete	%al
	leal	1(%rax,%rax), %r14d
	movl	%r14d, 12(%rsp)
	testb	%cl, %cl
	jne	.LBB3_146
	jmp	.LBB3_148
.LBB3_134:
	movq	%r15, %rdi
	callq	messageGetMimeType
	movl	%eax, %ebp
	movq	%r15, %rdi
	callq	encodingLine
	movq	%rax, %rbx
	cmpl	$4, %ebp
	jne	.LBB3_136
# BB#135:
	testq	%rbx, %rbx
	jne	.LBB3_141
	jmp	.LBB3_145
.LBB3_136:
	testq	%rbx, %rbx
	je	.LBB3_141
# BB#137:
	callq	fileblobCreate
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB3_145
# BB#138:
	movq	(%rbx), %rdi
	callq	lineGetData
	movq	%rax, %rcx
	movl	$.L.str.141, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	cli_dbgmsg
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rsi
	movl	$.L.str.137, %edx
	movq	%r15, %rdi
	callq	fileblobSetFilename
	movl	$.L.str.138, %esi
	movl	$28, %edx
	movq	%r15, %rdi
	callq	fileblobAddData
	movq	32(%rbp), %rsi
	movq	%r15, %rdi
	callq	fileblobSetCTX
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	textToFileblob
	movq	%rax, %rdi
	callq	fileblobScanAndDestroy
	cmpl	$1, %eax
	jne	.LBB3_140
# BB#139:
	movl	$3, 12(%rsp)
	movl	$3, %r14d
.LBB3_140:                              # %.critedge592
	movq	32(%rsp), %rax          # 8-byte Reload
	incl	8(%rax)
	jmp	.LBB3_145
.LBB3_141:                              # %.critedge593
	movl	$.L.str.142, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	cli_dbgmsg
	movl	$.L.str.239, %esi
	movq	%r15, %rdi
	callq	messageAddArgument
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rsi
	movl	$1, %edx
	movq	%r15, %rdi
	callq	messageToFileblob
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_144
# BB#142:                               # %saveTextPart.exit
	movl	$.L.str.240, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	incl	8(%rbp)
	movq	%rbx, %rdi
	callq	fileblobScanAndDestroy
	cmpl	$1, %eax
	jne	.LBB3_144
# BB#143:
	movl	$3, 12(%rsp)
	movl	$3, %r14d
.LBB3_144:                              # %saveTextPart.exit.thread
	movq	%r15, %rdi
	callq	messageReset
.LBB3_145:
	movl	8(%rsp), %ecx           # 4-byte Reload
	testb	%cl, %cl
	je	.LBB3_148
.LBB3_146:
	testl	%r14d, %r14d
	je	.LBB3_148
# BB#147:
	movl	$3, 12(%rsp)
	movl	$3, %r14d
.LBB3_148:
	movl	$.L.str.143, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	cli_dbgmsg
	movl	12(%rsp), %ebp
.LBB3_149:                              # %.thread
	movl	%ebp, %eax
	addq	$1160, %rsp             # imm = 0x488
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_150:
	movl	$.L.str.130, %esi
	movq	%rbp, %rdi
	callq	strstr
	testq	%rax, %rax
	jne	.LBB3_132
.LBB3_151:
	callq	fileblobCreate
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB3_132
# BB#152:
	movl	$.L.str.236, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	(%r14), %rsi
	movl	$.L.str.137, %edx
	movq	%rbp, %rdi
	callq	fileblobSetFilename
	movq	32(%r14), %rsi
	movq	%rbp, %rdi
	callq	fileblobSetCTX
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	textToFileblob
	testq	%rax, %rax
	je	.LBB3_154
# BB#153:
	movq	%rbp, %rdi
	callq	fileblobScanAndDestroy
	movl	%eax, %ebx
	jmp	.LBB3_155
.LBB3_154:
	xorl	%ebx, %ebx
	movl	$.L.str.237, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rbp, %rdi
	callq	fileblobDestroy
.LBB3_155:                              # %exportBounceMessage.exit
	incl	8(%r14)
	jmp	.LBB3_133
.LBB3_156:
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	movq	%r15, %rdi
	callq	rfc1341
	testl	%eax, %eax
	js	.LBB3_162
# BB#157:
	movl	$1, 12(%rsp)
	movl	$1, %ebp
	jmp	.LBB3_149
.LBB3_158:
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movl	$.L.str.86, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_warnmsg
	jmp	.LBB3_62
.LBB3_159:
	xorl	%ebp, %ebp
	movl	$.L.str.124, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB3_149
.LBB3_160:
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movl	$.L.str.88, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB3_161:                              # %.thread642
	movq	%r12, %rdi
	callq	free
	jmp	.LBB3_62
.LBB3_162:
	xorl	%ebp, %ebp
	jmp	.LBB3_149
.LBB3_163:
	xorl	%ebp, %ebp
	jmp	.LBB3_165
.LBB3_164:
	movl	$3, 12(%rsp)
	movb	$1, %bpl
	movl	$3, 8(%rsp)             # 4-byte Folded Spill
.LBB3_165:                              # %.loopexit658
	movq	%r12, 104(%rsp)         # 8-byte Spill
	movq	%r13, 128(%rsp)         # 8-byte Spill
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	24(%r14), %rdi
	movq	80(%rsp), %rsi          # 8-byte Reload
	callq	tableFind
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	testb	%bpl, %bpl
	movq	%r15, 72(%rsp)          # 8-byte Spill
	movq	%rax, 136(%rsp)         # 8-byte Spill
	je	.LBB3_167
# BB#166:
	movl	%ebp, 52(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	8(%rsp), %eax           # 4-byte Reload
	movl	%eax, %ebp
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r15, 56(%rsp)          # 8-byte Spill
	jmp	.LBB3_239
.LBB3_167:                              # %.lr.ph730
	movq	%r15, %rax
	movl	$1, %r15d
	movl	8(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, 100(%rsp)         # 4-byte Spill
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	$0, 52(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB3_231
.LBB3_168:                              #   in Loop: Header=BB3_231 Depth=1
	callq	messageCreate
	movq	%rax, %rcx
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	%rcx, (%r13,%rax,8)
	testq	%rcx, %rcx
	je	.LBB3_173
# BB#169:                               #   in Loop: Header=BB3_231 Depth=1
	movq	%r13, 64(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	32(%rax), %rsi
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rcx, %rdi
	callq	messageSetCTX
	movl	$.L.str.91, %edi
	xorl	%eax, %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	movq	40(%rsp), %r12          # 8-byte Reload
	je	.LBB3_233
.LBB3_170:                              # %.lr.ph716
                                        #   Parent Loop BB3_231 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_172
# BB#171:                               #   in Loop: Header=BB3_170 Depth=2
	callq	lineGetData
	cmpb	$0, (%rax)
	jne	.LBB3_174
.LBB3_172:                              # %.backedge
                                        #   in Loop: Header=BB3_170 Depth=2
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_170
	jmp	.LBB3_233
.LBB3_173:                              #   in Loop: Header=BB3_231 Depth=1
	movq	16(%rsp), %r12          # 8-byte Reload
	decl	%r12d
	movl	8(%rsp), %eax           # 4-byte Reload
	movl	%eax, %ebp
	jmp	.LBB3_226
.LBB3_174:                              # %.preheader656.preheader
                                        #   in Loop: Header=BB3_231 Depth=1
	xorl	%r14d, %r14d
.LBB3_175:                              # %.preheader656
                                        #   Parent Loop BB3_231 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_181 Depth 3
	movq	(%rbx), %rdi
	callq	lineGetData
	movq	%rax, %rbp
	testl	%r15d, %r15d
	je	.LBB3_187
# BB#176:                               #   in Loop: Header=BB3_175 Depth=2
	testq	%rbp, %rbp
	je	.LBB3_197
# BB#177:                               #   in Loop: Header=BB3_175 Depth=2
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movsbq	(%rbp), %rcx
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB3_203
# BB#178:                               #   in Loop: Header=BB3_175 Depth=2
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	rfc822comments
	movq	%rax, %rcx
	movq	%rcx, %r13
	testq	%rcx, %rcx
	jne	.LBB3_180
# BB#179:                               #   in Loop: Header=BB3_175 Depth=2
	movq	%rbp, %rdi
	callq	cli_strdup
	movq	%rax, %r13
.LBB3_180:                              # %.preheader655.preheader
                                        #   in Loop: Header=BB3_175 Depth=2
	movl	%r14d, 124(%rsp)        # 4-byte Spill
.LBB3_181:                              # %.preheader655
                                        #   Parent Loop BB3_231 Depth=1
                                        #     Parent Loop BB3_175 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rbx, %rdi
	callq	next_is_folded_header
	testb	%al, %al
	je	.LBB3_186
# BB#182:                               #   in Loop: Header=BB3_181 Depth=3
	movq	8(%rbx), %rbx
	movq	(%rbx), %rdi
	callq	lineGetData
	movq	%rax, %rbp
	cmpb	$0, 1(%rbp)
	je	.LBB3_210
# BB#183:                               #   in Loop: Header=BB3_181 Depth=3
	movq	%r13, %r14
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %r12
	movq	%rbp, %rdi
	callq	strlen
	leaq	1(%r12,%rax), %rsi
	movq	%r14, %rdi
	callq	cli_realloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB3_186
# BB#184:                               #   in Loop: Header=BB3_181 Depth=3
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	strcat
	testq	%rbx, %rbx
	movq	%r12, %r13
	jne	.LBB3_181
# BB#185:                               #   in Loop: Header=BB3_175 Depth=2
	xorl	%ebx, %ebx
	movq	%r12, %r13
.LBB3_186:                              # %.thread618
                                        #   in Loop: Header=BB3_175 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB3_211
.LBB3_187:                              #   in Loop: Header=BB3_175 Depth=2
	testq	%rbp, %rbp
	je	.LBB3_194
# BB#188:                               #   in Loop: Header=BB3_175 Depth=2
	cmpb	$45, (%rbp)
	jne	.LBB3_194
# BB#189:                               #   in Loop: Header=BB3_175 Depth=2
	cmpb	$45, 1(%rbp)
	jne	.LBB3_194
# BB#190:                               #   in Loop: Header=BB3_175 Depth=2
	movq	%rbp, %r12
	addq	$2, %r12
	movq	104(%rsp), %r15         # 8-byte Reload
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %r13
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	strncasecmp
	testl	%eax, %eax
	jne	.LBB3_194
# BB#191:                               #   in Loop: Header=BB3_175 Depth=2
	movq	%r12, %rdi
	callq	strlen
	leaq	2(%r13), %rcx
	cmpq	%rcx, %rax
	jb	.LBB3_194
# BB#192:                               #   in Loop: Header=BB3_175 Depth=2
	cmpb	$45, (%r12,%r13)
	jne	.LBB3_194
# BB#193:                               #   in Loop: Header=BB3_175 Depth=2
	leaq	1(%r12,%r13), %rdx
	cmpb	$45, (%rdx)
	je	.LBB3_230
.LBB3_194:                              #   in Loop: Header=BB3_175 Depth=2
	movq	%rbp, %rdi
	movq	104(%rsp), %rsi         # 8-byte Reload
	callq	boundaryStart
	testl	%eax, %eax
	jne	.LBB3_214
# BB#195:                               #   in Loop: Header=BB3_175 Depth=2
	movq	(%rbx), %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	messageAddLine
	testl	%eax, %eax
	movq	40(%rsp), %r12          # 8-byte Reload
	js	.LBB3_215
# BB#196:                               #   in Loop: Header=BB3_175 Depth=2
	incl	%r14d
	xorl	%r15d, %r15d
	jmp	.LBB3_212
.LBB3_197:                              #   in Loop: Header=BB3_175 Depth=2
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.LBB3_208
# BB#198:                               #   in Loop: Header=BB3_175 Depth=2
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB3_208
# BB#199:                               #   in Loop: Header=BB3_175 Depth=2
	callq	lineGetData
	movq	%rax, %rbp
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	messageGetEncoding
	testl	%eax, %eax
	jne	.LBB3_206
# BB#200:                               #   in Loop: Header=BB3_175 Depth=2
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	messageGetMimeType
	cmpl	$1, %eax
	jne	.LBB3_206
# BB#201:                               #   in Loop: Header=BB3_175 Depth=2
	movl	$.L.str.57, %esi
	movq	%rbp, %rdi
	callq	strstr
	testq	%rax, %rax
	je	.LBB3_206
# BB#202:                               #   in Loop: Header=BB3_175 Depth=2
	movl	$.L.str.57, %esi
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	messageSetEncoding
	jmp	.LBB3_209
.LBB3_203:                              #   in Loop: Header=BB3_175 Depth=2
	movl	%r14d, %r12d
	movl	%r15d, %r13d
	movl	$.L.str.98, %edi
	xorl	%eax, %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	messageAddArgument
	movq	%r14, %rdi
	callq	messageGetMimeType
	testl	%eax, %eax
	jne	.LBB3_205
# BB#204:                               #   in Loop: Header=BB3_175 Depth=2
	movl	$.L.str.64, %esi
	movq	%r14, %rdi
	callq	messageSetMimeType
.LBB3_205:                              #   in Loop: Header=BB3_175 Depth=2
	movl	%r13d, %r15d
	movl	%r12d, %r14d
	movq	40(%rsp), %r12          # 8-byte Reload
	jmp	.LBB3_212
.LBB3_206:                              #   in Loop: Header=BB3_175 Depth=2
	movl	$.L.str.95, %esi
	movl	$7, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_209
# BB#207:                               #   in Loop: Header=BB3_175 Depth=2
	movl	$.L.str.96, %esi
	movl	$9, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_209
.LBB3_208:                              #   in Loop: Header=BB3_175 Depth=2
	xorl	%r15d, %r15d
	movl	$.L.str.97, %edi
	xorl	%eax, %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	jmp	.LBB3_212
.LBB3_209:                              #   in Loop: Header=BB3_175 Depth=2
	movl	$.L.str.94, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB3_212
.LBB3_210:                              #   in Loop: Header=BB3_175 Depth=2
	xorl	%r15d, %r15d
	movl	$.L.str.99, %edi
	xorl	%eax, %eax
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %esi
	callq	cli_dbgmsg
.LBB3_211:                              # %.thread618
                                        #   in Loop: Header=BB3_175 Depth=2
	movl	$.L.str.100, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	movq	%r13, %rdx
	callq	cli_dbgmsg
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rdx
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	callq	parseEmailHeader
	movq	%r13, %rdi
	callq	free
	movq	40(%rsp), %r12          # 8-byte Reload
	movl	124(%rsp), %r14d        # 4-byte Reload
.LBB3_212:                              # %.thread621
                                        #   in Loop: Header=BB3_175 Depth=2
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_175
# BB#213:                               #   in Loop: Header=BB3_231 Depth=1
	movl	%r14d, %ebp
	xorl	%ebx, %ebx
	jmp	.LBB3_216
.LBB3_214:                              #   in Loop: Header=BB3_231 Depth=1
	movl	%r14d, %ebp
	movl	$1, %r15d
	movq	40(%rsp), %r12          # 8-byte Reload
	jmp	.LBB3_216
.LBB3_215:                              #   in Loop: Header=BB3_231 Depth=1
	movl	%r14d, %ebp
	xorl	%r15d, %r15d
.LBB3_216:                              # %.thread624
                                        #   in Loop: Header=BB3_231 Depth=1
	movq	64(%rsp), %r13          # 8-byte Reload
	movl	$.L.str.101, %edi
	xorl	%eax, %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%ebp, %edx
	movl	100(%rsp), %ecx         # 4-byte Reload
	callq	cli_dbgmsg
	movq	136(%rsp), %rax         # 8-byte Reload
	leal	1(%rax), %eax
	cmpl	$15, %eax
	ja	.LBB3_229
# BB#217:                               # %.thread624
                                        #   in Loop: Header=BB3_231 Depth=1
	movl	$45505, %ecx            # imm = 0xB1C1
	btl	%eax, %ecx
	jae	.LBB3_229
# BB#218:                               #   in Loop: Header=BB3_231 Depth=1
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	leaq	12(%rsp), %rcx
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	pushq	%r12
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	leaq	96(%rsp), %rax
	pushq	%rax
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	callq	do_multipart
	addq	$16, %rsp
.Lcfi46:
	.cfi_adjust_cfa_offset -16
	movl	12(%rsp), %ebp
	cmpl	$1, 8(%rsp)             # 4-byte Folded Reload
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jne	.LBB3_221
# BB#219:                               #   in Loop: Header=BB3_231 Depth=1
	cmpl	$2, %ebp
	jne	.LBB3_221
# BB#220:                               #   in Loop: Header=BB3_231 Depth=1
	movl	$1, 12(%rsp)
	movl	$1, %ebp
.LBB3_221:                              #   in Loop: Header=BB3_231 Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	(%r13,%rax,8), %rdi
	testq	%rdi, %rdi
	movq	16(%rsp), %r12          # 8-byte Reload
	je	.LBB3_223
# BB#222:                               #   in Loop: Header=BB3_231 Depth=1
	callq	messageDestroy
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	$0, (%r13,%rax,8)
.LBB3_223:                              #   in Loop: Header=BB3_231 Depth=1
	cmpl	$3, %ebp
	movb	$1, %al
	je	.LBB3_225
# BB#224:                               #   in Loop: Header=BB3_231 Depth=1
	movl	52(%rsp), %eax          # 4-byte Reload
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
.LBB3_225:                              #   in Loop: Header=BB3_231 Depth=1
	decl	%r12d
	movl	%ebp, %ecx
	movl	%ecx, 100(%rsp)         # 4-byte Spill
                                        # kill: %AL<def> %AL<kill> %EAX<def>
	movl	%eax, 52(%rsp)          # 4-byte Spill
.LBB3_226:                              #   in Loop: Header=BB3_231 Depth=1
	movq	32(%rsp), %r14          # 8-byte Reload
.LBB3_227:                              #   in Loop: Header=BB3_231 Depth=1
	incl	%r12d
	movq	%r12, 16(%rsp)          # 8-byte Spill
	testq	%rbx, %rbx
	je	.LBB3_238
# BB#228:                               #   in Loop: Header=BB3_231 Depth=1
	movl	52(%rsp), %eax          # 4-byte Reload
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	andb	$1, %al
	movl	%ebp, %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	%r13, %r12
	je	.LBB3_231
	jmp	.LBB3_239
.LBB3_229:                              #   in Loop: Header=BB3_231 Depth=1
	movl	8(%rsp), %eax           # 4-byte Reload
	movl	%eax, %ebp
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	jmp	.LBB3_227
.LBB3_230:                              # %boundaryEnd.exit
                                        #   in Loop: Header=BB3_231 Depth=1
	movl	%r14d, %ebp
	xorl	%r15d, %r15d
	movl	$.L.str.192, %edi
	xorl	%eax, %eax
	movq	104(%rsp), %rsi         # 8-byte Reload
	callq	cli_dbgmsg
	movq	40(%rsp), %r12          # 8-byte Reload
	jmp	.LBB3_216
.LBB3_231:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_170 Depth 2
                                        #     Child Loop BB3_175 Depth 2
                                        #       Child Loop BB3_181 Depth 3
	movslq	16(%rsp), %rax          # 4-byte Folded Reload
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leaq	8(,%rax,8), %rsi
	movq	%r12, %rdi
	callq	cli_realloc
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.LBB3_168
# BB#232:
	movl	8(%rsp), %eax           # 4-byte Reload
	movl	%eax, %ebp
	movq	%r12, %r13
	movq	32(%rsp), %r14          # 8-byte Reload
	jmp	.LBB3_239
.LBB3_233:                              # %._crit_edge717
	movl	$.L.str.92, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	56(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB3_236
# BB#234:
	callq	binhexBegin
	testq	%rax, %rax
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	jne	.LBB3_237
# BB#235:
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	messageDestroy
	decl	%ebx
	jmp	.LBB3_237
.LBB3_236:
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB3_237:                              # %.thread789
	incl	%ebx
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movl	8(%rsp), %eax           # 4-byte Reload
	movl	%eax, %ebp
	movq	64(%rsp), %r13          # 8-byte Reload
.LBB3_238:                              # %.critedge
	movq	%r13, 24(%rsp)          # 8-byte Spill
.LBB3_239:                              # %.critedge
	movq	104(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	136(%rsp), %rax         # 8-byte Reload
	cmpl	$-1, %eax
	je	.LBB3_242
# BB#240:                               # %.critedge
	cmpl	$14, %eax
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	jne	.LBB3_243
# BB#241:
	movl	$.L.str.102, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.20, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	jmp	.LBB3_243
.LBB3_242:
	movl	$.L.str.103, %edi
	xorl	%eax, %eax
	movq	80(%rsp), %rsi          # 8-byte Reload
	callq	cli_dbgmsg
	movl	$.L.str.20, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
.LBB3_243:
	movq	56(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB3_246
# BB#244:
	cmpq	%rbx, %rdi
	je	.LBB3_246
# BB#245:
	callq	messageDestroy
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
.LBB3_246:
	movl	$.L.str.104, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	cli_dbgmsg
	testb	$1, 52(%rsp)            # 1-byte Folded Reload
	jne	.LBB3_249
# BB#247:
	movq	88(%rsp), %r15
	testl	%r12d, %r12d
	jne	.LBB3_260
# BB#248:
	testq	%r15, %r15
	jne	.LBB3_260
.LBB3_249:
	movq	24(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB3_256
# BB#250:                               # %.preheader651
	testl	%r12d, %r12d
	jle	.LBB3_255
# BB#251:                               # %.lr.ph700.preheader
	movl	%r12d, %r14d
.LBB3_252:                              # %.lr.ph700
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_254
# BB#253:                               #   in Loop: Header=BB3_252 Depth=1
	callq	messageDestroy
.LBB3_254:                              #   in Loop: Header=BB3_252 Depth=1
	addq	$8, %rbx
	decq	%r14
	jne	.LBB3_252
.LBB3_255:                              # %._crit_edge
	movq	%r13, %rdi
	callq	free
.LBB3_256:
	cmpq	$0, 128(%rsp)           # 8-byte Folded Reload
	jne	.LBB3_259
# BB#257:
	movq	88(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_259
# BB#258:
	callq	textDestroy
.LBB3_259:
	leal	-3(%rbp), %eax
	cmpl	$2, %eax
	movl	$2, %eax
	cmovbl	%ebp, %eax
	movl	%eax, %ebp
	jmp	.LBB3_149
.LBB3_260:
	movl	$.L.str.105, %edi
	xorl	%eax, %eax
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	movq	24(%r14), %rdi
	movq	%rbx, %rsi
	callq	tableFind
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	addl	$-5, %eax
	cmpl	$8, %eax
	ja	.LBB3_339
# BB#261:
	movq	%r13, 64(%rsp)          # 8-byte Spill
	jmpq	*.LJTI3_1(,%rax,8)
.LBB3_262:
	movq	%r14, %r13
	testl	%r12d, %r12d
	je	.LBB3_279
# BB#263:                               # %.lr.ph.i603.preheader
	movslq	%r12d, %rbp
	movl	$-1, %r14d
	xorl	%ebx, %ebx
.LBB3_264:                              # %.lr.ph.i603
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB3_268
# BB#265:                               #   in Loop: Header=BB3_264 Depth=1
	callq	messageGetMimeType
	cmpl	$6, %eax
	jne	.LBB3_268
# BB#266:                               #   in Loop: Header=BB3_264 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rdi
	callq	messageGetMimeSubtype
	movl	$.L.str.18, %esi
	movq	%rax, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB3_283
# BB#267:                               # %._crit_edge1062
                                        #   in Loop: Header=BB3_264 Depth=1
	movl	%ebx, %r14d
.LBB3_268:                              #   in Loop: Header=BB3_264 Depth=1
	incq	%rbx
	cmpq	%rbp, %rbx
	jb	.LBB3_264
	jmp	.LBB3_284
.LBB3_269:
	movl	$.L.str.106, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testl	%r12d, %r12d
	je	.LBB3_304
# BB#270:                               # %.lr.ph.i600.preheader
	movslq	16(%rsp), %r13          # 4-byte Folded Reload
	movl	$-1, %r14d
	xorl	%ebx, %ebx
.LBB3_271:                              # %.lr.ph.i600
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB3_275
# BB#272:                               #   in Loop: Header=BB3_271 Depth=1
	callq	messageGetMimeType
	cmpl	$6, %eax
	jne	.LBB3_275
# BB#273:                               #   in Loop: Header=BB3_271 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rdi
	callq	messageGetMimeSubtype
	movl	$.L.str.18, %esi
	movq	%rax, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB3_289
# BB#274:                               # %._crit_edge1058
                                        #   in Loop: Header=BB3_271 Depth=1
	movl	%ebx, %r14d
.LBB3_275:                              #   in Loop: Header=BB3_271 Depth=1
	incq	%rbx
	cmpq	%r13, %rbx
	jb	.LBB3_271
	jmp	.LBB3_290
.LBB3_276:
	movl	$0, 12(%rsp)
	movl	$.L.str.110, %esi
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	messageFindArgument
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_281
# BB#277:
	movl	$.L.str.111, %esi
	movq	%rbx, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB3_285
# BB#278:
	xorl	%ebp, %ebp
	movl	$.L.str.113, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_warnmsg
	jmp	.LBB3_286
.LBB3_279:
	movl	$-1, %r14d
	jmp	.LBB3_284
.LBB3_281:
	xorl	%ebp, %ebp
	movl	$.L.str.114, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB3_287
.LBB3_283:                              # %.getTextPart.exit606.loopexit_crit_edge
	movl	%ebx, %r14d
.LBB3_284:                              # %getTextPart.exit606
	xorl	%eax, %eax
	cmpl	$-1, %r14d
	movslq	%r14d, %rcx
	cmovneq	%rcx, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %rdi
	movq	40(%rsp), %rcx          # 8-byte Reload
	incl	%ecx
	movq	%r15, %rsi
	movq	%r13, %r14
	movq	%r14, %rdx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	parseEmailBody
	movl	%eax, %ebp
	movl	%ebp, 12(%rsp)
	movq	72(%rsp), %r15          # 8-byte Reload
	jmp	.LBB3_288
.LBB3_285:
	movl	$.L.str.112, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movl	$2, 12(%rsp)
	movl	$2, %ebp
.LBB3_286:
	movq	%rbx, %rdi
	callq	free
.LBB3_287:                              # %.fold.split
	movq	72(%rsp), %r15          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
.LBB3_288:                              # %.fold.split
	movq	64(%rsp), %r13          # 8-byte Reload
	jmp	.LBB3_340
.LBB3_289:                              # %.getTextPart.exit_crit_edge
	movl	%ebx, %r14d
.LBB3_290:                              # %getTextPart.exit
	testl	%r14d, %r14d
	js	.LBB3_294
# BB#291:
	movslq	%r14d, %rbx
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rdi
	callq	messageGetBody
	testq	%rax, %rax
	je	.LBB3_293
# BB#292:
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rsi
	movq	%r15, %rdi
	callq	textAddMessage
	movq	%rax, %r15
	movq	%r15, 88(%rsp)
.LBB3_293:                              # %.thread638
	xorl	%r12d, %r12d
	jmp	.LBB3_301
.LBB3_294:                              # %getTextPart.exit.thread.preheader
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_298
# BB#295:                               # %.lr.ph713.preheader
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
.LBB3_296:                              # %.lr.ph713
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rdi
	callq	messageGetMimeType
	cmpl	$5, %eax
	je	.LBB3_299
# BB#297:                               # %getTextPart.exit.thread
                                        #   in Loop: Header=BB3_296 Depth=1
	incq	%rbx
	cmpq	%r13, %rbx
	jl	.LBB3_296
	jmp	.LBB3_300
.LBB3_298:
	xorl	%r12d, %r12d
	cmpl	$-1, %r14d
	jne	.LBB3_301
	jmp	.LBB3_304
.LBB3_299:
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %r12
	movl	%ebx, %r14d
.LBB3_300:                              # %.loopexit654
	cmpl	$-1, %r14d
	je	.LBB3_304
.LBB3_301:                              # %.thread638
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	1(%rax), %ecx
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	parseEmailBody
	movl	%eax, %ebp
	movl	%ebp, 12(%rsp)
	testq	%r12, %r12
	je	.LBB3_337
# BB#302:                               # %.thread638
	cmpl	$1, %ebp
	jne	.LBB3_337
# BB#303:
	movq	%r12, %rdi
	callq	messageDestroy
	movslq	%r14d, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	$0, (%rcx,%rax,8)
	movl	$1, %ebp
	movq	88(%rsp), %r15
	jmp	.LBB3_305
.LBB3_304:                              # %.loopexit654.thread
	movl	$.L.str.107, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB3_305:
	movq	32(%rsp), %r14          # 8-byte Reload
.LBB3_306:
	movl	$.L.str.108, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB3_307:
	testq	%r15, %r15
	je	.LBB3_311
# BB#308:
	xorl	%r12d, %r12d
	movq	56(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	movq	72(%rsp), %r15          # 8-byte Reload
	je	.LBB3_312
# BB#309:
	cmpq	%r15, %rdi
	je	.LBB3_312
# BB#310:
	callq	messageDestroy
	xorl	%r12d, %r12d
	jmp	.LBB3_312
.LBB3_311:
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	72(%rsp), %r15          # 8-byte Reload
.LBB3_312:
	xorl	%ebx, %ebx
	movl	$.L.str.109, %edi
	xorl	%eax, %eax
	movq	16(%rsp), %r13          # 8-byte Reload
	movl	%r13d, %esi
	callq	cli_dbgmsg
	testl	%r13d, %r13d
	jle	.LBB3_317
# BB#313:                               # %.lr.ph707
	movq	%r14, %r13
	movq	40(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	88(%rsp), %r14
.LBB3_314:                              # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	%ebx, %edx
	leaq	12(%rsp), %rcx
	movq	%r13, %r8
	movq	%r15, %r9
	pushq	40(%rsp)                # 8-byte Folded Reload
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	callq	do_multipart
	addq	$16, %rsp
.Lcfi49:
	.cfi_adjust_cfa_offset -16
	movq	%rax, %r12
	movl	12(%rsp), %ebp
	cmpl	$3, %ebp
	je	.LBB3_318
# BB#315:                               #   in Loop: Header=BB3_314 Depth=1
	cmpl	$4, %ebp
	je	.LBB3_319
# BB#316:                               #   in Loop: Header=BB3_314 Depth=1
	incl	%ebx
	cmpl	16(%rsp), %ebx          # 4-byte Folded Reload
	jl	.LBB3_314
	jmp	.LBB3_320
.LBB3_317:
	movq	%r12, %rdi
	jmp	.LBB3_321
.LBB3_318:
	movb	$1, %al
	movl	%eax, 52(%rsp)          # 4-byte Spill
	movl	$3, %ebp
	jmp	.LBB3_320
.LBB3_319:                              # %.fold.split.loopexit
	movl	$4, %ebp
.LBB3_320:                              # %.fold.split
	movq	%r12, %rdi
	movq	%r13, %r14
.LBB3_321:                              # %.fold.split
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	64(%rsp), %r13          # 8-byte Reload
	testq	%rdi, %rdi
	jne	.LBB3_322
	jmp	.LBB3_324
.LBB3_337:
	cmpl	$3, %ebp
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	64(%rsp), %r13          # 8-byte Reload
	jne	.LBB3_306
# BB#338:
	movb	$1, %al
	movl	%eax, 52(%rsp)          # 4-byte Spill
	movl	$3, %ebp
.LBB3_339:
	movq	72(%rsp), %r15          # 8-byte Reload
.LBB3_340:
	movq	56(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB3_324
.LBB3_322:                              # %.fold.split
	cmpq	%r15, %rdi
	je	.LBB3_324
# BB#323:
	callq	messageDestroy
.LBB3_324:
	cmpq	$0, 128(%rsp)           # 8-byte Folded Reload
	jne	.LBB3_330
# BB#325:
	movq	88(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB3_330
# BB#326:
	testb	$1, 52(%rsp)            # 1-byte Folded Reload
	jne	.LBB3_329
# BB#327:
	callq	fileblobCreate
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB3_329
# BB#328:
	movl	$.L.str.115, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	(%r14), %rsi
	movl	$.L.str.116, %edx
	movq	%r15, %rdi
	callq	fileblobSetFilename
	movq	32(%r14), %rsi
	movq	%r15, %rdi
	callq	fileblobSetCTX
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	textToFileblob
	movq	%r15, %rdi
	callq	fileblobDestroy
	incl	8(%r14)
.LBB3_329:
	movq	%rbx, %rdi
	callq	textDestroy
.LBB3_330:                              # %.preheader652
	testl	%r12d, %r12d
	movq	24(%rsp), %r15          # 8-byte Reload
	jle	.LBB3_335
# BB#331:                               # %.lr.ph702.preheader
	movl	%r12d, %r14d
	movq	%r15, %rbx
.LBB3_332:                              # %.lr.ph702
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_334
# BB#333:                               #   in Loop: Header=BB3_332 Depth=1
	callq	messageDestroy
.LBB3_334:                              #   in Loop: Header=BB3_332 Depth=1
	addq	$8, %rbx
	decq	%r14
	jne	.LBB3_332
.LBB3_335:                              # %._crit_edge703
	testq	%r15, %r15
	je	.LBB3_149
# BB#336:
	movq	%r13, %rdi
	callq	free
	jmp	.LBB3_149
.Lfunc_end3:
	.size	parseEmailBody, .Lfunc_end3-parseEmailBody
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_17
	.quad	.LBB3_56
	.quad	.LBB3_63
	.quad	.LBB3_63
	.quad	.LBB3_55
	.quad	.LBB3_34
	.quad	.LBB3_18
	.quad	.LBB3_63
.LJTI3_1:
	.quad	.LBB3_307
	.quad	.LBB3_306
	.quad	.LBB3_306
	.quad	.LBB3_262
	.quad	.LBB3_262
	.quad	.LBB3_269
	.quad	.LBB3_307
	.quad	.LBB3_307
	.quad	.LBB3_276

	.text
	.p2align	4, 0x90
	.type	getline_from_mbox,@function
getline_from_mbox:                      # @getline_from_mbox
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 48
.Lcfi55:
	.cfi_offset %rbx, -48
.Lcfi56:
	.cfi_offset %r12, -40
.Lcfi57:
	.cfi_offset %r14, -32
.Lcfi58:
	.cfi_offset %r15, -24
.Lcfi59:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	%r15, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB4_3
.LBB4_1:
	xorl	%r14d, %r14d
.LBB4_2:                                # %.thread2
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_3:
	testq	%r14, %r14
	je	.LBB4_12
# BB#4:                                 # %.preheader.preheader
	movl	$1000, %ebx             # imm = 0x3E8
	movq	%r14, %r12
	.p2align	4, 0x90
.LBB4_5:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%r15, %rdi
	callq	ferror
	testl	%eax, %eax
	jne	.LBB4_1
# BB#6:                                 #   in Loop: Header=BB4_5 Depth=1
	cmpl	$-1, %ebp
	je	.LBB4_17
# BB#7:                                 #   in Loop: Header=BB4_5 Depth=1
	cmpl	$13, %ebp
	je	.LBB4_15
# BB#8:                                 #   in Loop: Header=BB4_5 Depth=1
	cmpl	$10, %ebp
	je	.LBB4_16
# BB#9:                                 #   in Loop: Header=BB4_5 Depth=1
	movb	%bpl, (%r12)
	incq	%r12
	decq	%rbx
	cmpq	$1, %rbx
	ja	.LBB4_5
# BB#10:                                # %.thread
	testq	%rbx, %rbx
	je	.LBB4_14
# BB#11:
	movb	$0, (%r12)
	movl	$.L.str.243, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB4_2
.LBB4_12:
	xorl	%r14d, %r14d
	movl	$.L.str.241, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB4_2
.LBB4_14:
	xorl	%r14d, %r14d
	movl	$.L.str.242, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB4_2
.LBB4_15:
	movb	$10, (%r12)
	incq	%r12
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$10, %ebp
	je	.LBB4_17
	jmp	.LBB4_18
.LBB4_16:
	movb	$10, (%r12)
	incq	%r12
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$13, %ebp
	jne	.LBB4_18
.LBB4_17:                               # %.thread5
	movb	$0, (%r12)
	jmp	.LBB4_2
.LBB4_18:
	movq	%r15, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB4_20
# BB#19:
	movl	%ebp, %edi
	movq	%r15, %rsi
	callq	ungetc
.LBB4_20:
	movb	$0, (%r12)
	jmp	.LBB4_2
.Lfunc_end4:
	.size	getline_from_mbox, .Lfunc_end4-getline_from_mbox
	.cfi_endproc

	.p2align	4, 0x90
	.type	rfc822comments,@function
rfc822comments:                         # @rfc822comments
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 48
.Lcfi65:
	.cfi_offset %rbx, -40
.Lcfi66:
	.cfi_offset %r14, -32
.Lcfi67:
	.cfi_offset %r15, -24
.Lcfi68:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB5_31
# BB#1:
	movl	$40, %esi
	movq	%r14, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB5_31
# BB#2:
	testq	%r15, %r15
	jne	.LBB5_4
# BB#3:
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	cli_malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB5_31
.LBB5_4:
	xorl	%ebp, %ebp
	movl	$.L.str.45, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movb	(%r14), %bl
	testb	%bl, %bl
	movq	%r15, %rax
	je	.LBB5_29
# BB#5:                                 # %.lr.ph.preheader
	leaq	1(%r14), %rcx
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r15, %rax
	jmp	.LBB5_10
.LBB5_31:
	xorl	%r15d, %r15d
	jmp	.LBB5_32
.LBB5_7:                                #   in Loop: Header=BB5_10 Depth=1
	incl	%ebp
	jmp	.LBB5_9
.LBB5_8:                                #   in Loop: Header=BB5_10 Depth=1
	xorl	%edx, %edx
	testl	%ebp, %ebp
	setg	%dl
	subl	%edx, %ebp
.LBB5_9:                                #   in Loop: Header=BB5_10 Depth=1
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.LBB5_25
	.p2align	4, 0x90
.LBB5_10:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testb	$1, %sil
	je	.LBB5_13
.LBB5_11:                               #   in Loop: Header=BB5_10 Depth=1
	xorl	%edi, %edi
	testl	%ebp, %ebp
	jne	.LBB5_24
# BB#12:                                #   in Loop: Header=BB5_10 Depth=1
	movb	%bl, (%rax)
	incq	%rax
	xorl	%esi, %esi
	jmp	.LBB5_26
	.p2align	4, 0x90
.LBB5_13:                               #   in Loop: Header=BB5_10 Depth=1
	movsbl	%bl, %edi
	cmpl	$40, %edi
	jg	.LBB5_18
# BB#14:                                #   in Loop: Header=BB5_10 Depth=1
	cmpl	$34, %edi
	je	.LBB5_20
# BB#15:                                #   in Loop: Header=BB5_10 Depth=1
	cmpl	$40, %edi
	jne	.LBB5_11
# BB#16:                                #   in Loop: Header=BB5_10 Depth=1
	testl	%edx, %edx
	je	.LBB5_7
# BB#17:                                #   in Loop: Header=BB5_10 Depth=1
	movb	$40, (%rax)
	jmp	.LBB5_23
	.p2align	4, 0x90
.LBB5_18:                               #   in Loop: Header=BB5_10 Depth=1
	cmpl	$41, %edi
	je	.LBB5_21
# BB#19:                                #   in Loop: Header=BB5_10 Depth=1
	movb	$1, %sil
	cmpl	$92, %edi
	je	.LBB5_25
	jmp	.LBB5_11
.LBB5_20:                               #   in Loop: Header=BB5_10 Depth=1
	movb	$34, (%rax)
	incq	%rax
	xorl	%edi, %edi
	testl	%edx, %edx
	sete	%dil
	xorl	%esi, %esi
	movl	%edi, %edx
	jmp	.LBB5_25
.LBB5_21:                               #   in Loop: Header=BB5_10 Depth=1
	testl	%edx, %edx
	je	.LBB5_8
# BB#22:                                #   in Loop: Header=BB5_10 Depth=1
	movb	$41, (%rax)
.LBB5_23:                               #   in Loop: Header=BB5_10 Depth=1
	incq	%rax
.LBB5_24:                               #   in Loop: Header=BB5_10 Depth=1
	xorl	%esi, %esi
.LBB5_25:                               #   in Loop: Header=BB5_10 Depth=1
	movl	%ebp, %edi
.LBB5_26:                               #   in Loop: Header=BB5_10 Depth=1
	movzbl	(%rcx), %ebx
	incq	%rcx
	testb	%bl, %bl
	movl	%edi, %ebp
	jne	.LBB5_10
# BB#27:                                # %._crit_edge
	testb	%sil, %sil
	je	.LBB5_29
# BB#28:
	movb	$92, (%rax)
	incq	%rax
.LBB5_29:                               # %._crit_edge.thread
	movb	$0, (%rax)
	movl	$.L.str.46, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	cli_dbgmsg
.LBB5_32:
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	rfc822comments, .Lfunc_end5-rfc822comments
	.cfi_endproc

	.p2align	4, 0x90
	.type	next_is_folded_header,@function
next_is_folded_header:                  # @next_is_folded_header
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 32
.Lcfi72:
	.cfi_offset %rbx, -32
.Lcfi73:
	.cfi_offset %r14, -24
.Lcfi74:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	8(%r14), %rax
	testq	%rax, %rax
	je	.LBB6_9
# BB#1:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB6_9
# BB#2:
	callq	lineGetData
	movq	%rax, %r15
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movsbq	(%r15), %rcx
	movb	$1, %bl
	testb	$1, (%rax,%rcx,2)
	jne	.LBB6_10
# BB#3:
	movl	$61, %esi
	movq	%r15, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB6_9
# BB#4:
	movq	(%r14), %rdi
	callq	lineGetData
	movq	%rax, %r14
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	__rawmemchr
	decq	%rax
	cmpq	%r14, %rax
	jbe	.LBB6_10
# BB#5:                                 # %.lr.ph.preheader
	movabsq	$4294977024, %rcx       # imm = 0x100002600
	.p2align	4, 0x90
.LBB6_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%rax), %edx
	cmpl	$59, %edx
	ja	.LBB6_9
# BB#7:                                 # %.lr.ph
                                        #   in Loop: Header=BB6_6 Depth=1
	btq	%rdx, %rcx
	jae	.LBB6_8
# BB#11:                                # %.backedge
                                        #   in Loop: Header=BB6_6 Depth=1
	decq	%rax
	xorl	%ebx, %ebx
	cmpq	%r14, %rax
	ja	.LBB6_6
	jmp	.LBB6_10
.LBB6_8:                                # %.lr.ph
	cmpq	$59, %rdx
	movb	$1, %bl
	je	.LBB6_10
.LBB6_9:                                # %.loopexit.loopexit21
	xorl	%ebx, %ebx
.LBB6_10:                               # %.loopexit
	movl	%ebx, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	next_is_folded_header, .Lfunc_end6-next_is_folded_header
	.cfi_endproc

	.p2align	4, 0x90
	.type	parseEmailHeader,@function
parseEmailHeader:                       # @parseEmailHeader
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi75:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi78:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi79:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi81:
	.cfi_def_cfa_offset 96
.Lcfi82:
	.cfi_offset %rbx, -56
.Lcfi83:
	.cfi_offset %r12, -48
.Lcfi84:
	.cfi_offset %r13, -40
.Lcfi85:
	.cfi_offset %r14, -32
.Lcfi86:
	.cfi_offset %r15, -24
.Lcfi87:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r12
	movq	%rdi, %r15
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$58, %esi
	movq	%r12, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB7_2
# BB#1:
	movl	$.L.str.51, %r14d
	cmpb	$0, (%r14)
	jne	.LBB7_6
	jmp	.LBB7_35
.LBB7_2:
	movl	$61, %esi
	movq	%r12, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB7_4
# BB#3:
	movl	$.L.str.51+1, %r14d
	cmpb	$0, (%r14)
	jne	.LBB7_6
	jmp	.LBB7_35
.LBB7_4:
	movl	$32, %esi
	movq	%r12, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB7_35
# BB#5:
	movl	$.L.str.51+2, %r14d
	cmpb	$0, (%r14)
	je	.LBB7_35
.LBB7_6:
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movl	$.L.str.52, %esi
	movq	%r12, %rdi
	callq	strstr
	testq	%rax, %rax
	je	.LBB7_33
# BB#7:
	movl	$.L.str.53, %esi
	movq	%r12, %rdi
	callq	strstr
	testq	%rax, %rax
	je	.LBB7_33
# BB#8:
	movl	$.L.str.54, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_dbgmsg
	movq	%r12, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	cli_malloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB7_44
# BB#9:                                 # %.preheader88.i
	movb	(%r12), %al
	testb	%al, %al
	movq	%r13, %rbx
	je	.LBB7_43
# BB#10:                                # %.preheader87.i.preheader
	movq	%r12, %rbp
	movq	%r13, %rbx
	cmpb	$61, %al
	jne	.LBB7_13
	jmp	.LBB7_14
	.p2align	4, 0x90
.LBB7_11:                               # %._crit_edge.i
	incq	%rbp
	movb	%al, (%rbx)
	incq	%rbx
	movb	(%rbp), %al
.LBB7_12:
	cmpb	$61, %al
	je	.LBB7_14
.LBB7_13:
	testb	%al, %al
	jne	.LBB7_11
	jmp	.LBB7_16
	.p2align	4, 0x90
.LBB7_14:
	cmpb	$63, 1(%rbp)
	jne	.LBB7_11
# BB#15:
	addq	$2, %rbp
.LBB7_16:                               # %.preheader.i.preheader.loopexit
	addq	$3, %rbp
	jmp	.LBB7_18
	.p2align	4, 0x90
.LBB7_17:                               #   in Loop: Header=BB7_18 Depth=1
	incq	%rbp
.LBB7_18:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rbp), %eax
	testb	%al, %al
	je	.LBB7_43
# BB#19:                                # %.preheader.i
                                        #   in Loop: Header=BB7_18 Depth=1
	cmpb	$63, %al
	jne	.LBB7_17
# BB#20:
	movq	%r15, (%rsp)            # 8-byte Spill
	movsbq	-2(%rbp), %r15
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movl	(%rax,%r15,4), %eax
	movsbl	%al, %r15d
	cmpb	$98, %r15b
	je	.LBB7_22
# BB#21:
	cmpl	$113, %r15d
	jne	.LBB7_36
.LBB7_22:
	cmpb	$63, -1(%rbp)
	jne	.LBB7_38
# BB#23:
	cmpb	$0, (%rbp)
	je	.LBB7_38
# BB#24:
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %rdi
	callq	cli_strdup
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB7_39
# BB#25:
	movl	$.L.str.53, %esi
	movq	%rbp, %rdi
	callq	strstr
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB7_41
# BB#26:
	movl	$.L.str.53, %esi
	movq	%r13, %rdi
	callq	strstr
	movb	$0, (%rax)
	callq	messageCreate
	testq	%rax, %rax
	je	.LBB7_42
# BB#27:
	addq	$2, %rbp
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	messageAddStr
	movq	%r13, %rdi
	callq	free
	cmpl	$98, %r15d
	je	.LBB7_30
# BB#28:
	cmpl	$113, %r15d
	movq	32(%rsp), %r15          # 8-byte Reload
	jne	.LBB7_32
# BB#29:
	movl	$.L.str.56, %esi
	jmp	.LBB7_31
.LBB7_30:
	movl	$.L.str.57, %esi
	movq	32(%rsp), %r15          # 8-byte Reload
.LBB7_31:
	movq	%r15, %rdi
	callq	messageSetEncoding
.LBB7_32:
	movl	$1, %esi
	movq	%r15, %rdi
	callq	messageToBlob
	movq	%rax, %r13
	movq	%r13, %rdi
	callq	blobGetDataSize
	movq	%rax, %r15
	movq	%r13, %rdi
	callq	blobGetData
	movq	%rax, %rcx
	movl	$.L.str.58, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	movl	%r15d, %edx
	callq	cli_dbgmsg
	movq	%r13, %rdi
	callq	blobGetData
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%r15, %rdx
	callq	memcpy
	movq	%r13, %rdi
	callq	blobDestroy
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	messageDestroy
	leaq	(%rbx,%r15), %rax
	cmpb	$10, -1(%rbx,%r15)
	leaq	-1(%rbx,%r15), %rbx
	cmovneq	%rax, %rbx
	movb	(%rbp), %al
	testb	%al, %al
	movq	(%rsp), %r15            # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
	jne	.LBB7_12
	jmp	.LBB7_43
.LBB7_35:
	movl	$-1, %r14d
	jmp	.LBB7_131
.LBB7_33:
	movq	%r12, %rdi
	callq	cli_strdup
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.LBB7_45
	jmp	.LBB7_44
.LBB7_38:
	movq	(%rsp), %r15            # 8-byte Reload
	jmp	.LBB7_43
.LBB7_36:
	movl	$.L.str.55, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	cli_warnmsg
	movq	%r13, %rdi
	jmp	.LBB7_40
.LBB7_39:
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB7_40:                               # %rfc2047.exit.thread
	callq	free
	movq	(%rsp), %r15            # 8-byte Reload
	jmp	.LBB7_44
.LBB7_41:
	movq	%r13, %rdi
	callq	free
.LBB7_42:
	movq	(%rsp), %r15            # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB7_43:                               # %.thread.i
	movb	$0, (%rbx)
	movl	$.L.str.59, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	cli_dbgmsg
	testq	%r13, %r13
	jne	.LBB7_45
.LBB7_44:                               # %rfc2047.exit.thread
	movq	%r12, %rdi
	callq	cli_strdup
	movq	%rax, %r13
.LBB7_45:
	movb	(%r14), %al
	movb	%al, 22(%rsp)
	movb	$0, 23(%rsp)
	leaq	22(%rsp), %rsi
	movq	%r13, %rdi
	callq	strtok
	movq	%rax, %rbx
	movl	$-1, %r14d
	testq	%rbx, %rbx
	je	.LBB7_130
# BB#46:
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rbp
	testl	%ebp, %ebp
	js	.LBB7_130
# BB#47:
	leal	1(%rbp), %eax
	cmpl	%ebp, %eax
	jle	.LBB7_55
# BB#48:
	movslq	%ebp, %rcx
	movb	(%rbx,%rcx), %al
	.p2align	4, 0x90
.LBB7_49:                               # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rbp
	testb	%al, %al
	je	.LBB7_51
# BB#50:                                #   in Loop: Header=BB7_49 Depth=1
	movb	$0, (%rbx,%rbp)
.LBB7_51:                               #   in Loop: Header=BB7_49 Depth=1
	testl	%ebp, %ebp
	jle	.LBB7_55
# BB#52:                                #   in Loop: Header=BB7_49 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movsbq	-1(%rbx,%rbp), %rax
	cmpw	$0, (%rcx,%rax,2)
	js	.LBB7_55
# BB#53:                                # %switch.early.test.i.i
                                        #   in Loop: Header=BB7_49 Depth=1
	cmpb	$13, %al
	je	.LBB7_55
# BB#54:                                # %switch.early.test.i.i
                                        #   in Loop: Header=BB7_49 Depth=1
	leaq	-1(%rbp), %rcx
	cmpb	$10, %al
	jne	.LBB7_49
.LBB7_55:                               # %strstrip.exit
	testq	%rbp, %rbp
	je	.LBB7_130
# BB#56:
	movq	%r15, (%rsp)            # 8-byte Spill
	xorl	%edi, %edi
	movl	$.L.str.37, %esi
	callq	strtok
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB7_130
# BB#57:
	movq	%r15, %r12
	movl	$.L.str.60, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%r12, %rdx
	callq	cli_dbgmsg
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	rfc822comments
	movq	%rax, %rbp
	testq	%rbp, %rbp
	movq	%r13, 8(%rsp)           # 8-byte Spill
	je	.LBB7_59
# BB#58:
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rsi
	callq	tableFind
	movl	%eax, %ebx
	movq	%rbp, %rdi
	callq	free
	jmp	.LBB7_60
.LBB7_59:
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	tableFind
	movl	%eax, %ebx
.LBB7_60:
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	rfc822comments
	movq	%rax, %rbp
	testq	%rbp, %rbp
	cmovneq	%rbp, %r15
	cmpl	$3, %ebx
	je	.LBB7_67
# BB#61:
	cmpl	$2, %ebx
	je	.LBB7_73
# BB#62:
	cmpl	$1, %ebx
	jne	.LBB7_124
# BB#63:
	movl	$47, %esi
	movq	%r15, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB7_76
# BB#64:
	movq	%r15, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	cli_malloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB7_74
# BB#65:
	cmpb	$47, (%r12)
	jne	.LBB7_77
# BB#66:
	movq	%rbp, %r14
	movl	$.L.str.63, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movl	$.L.str.64, %esi
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rbx, %rdi
	callq	messageSetMimeType
	movl	$.L.str.65, %esi
	movq	%rbx, %rdi
	callq	messageSetMimeSubtype
	movq	%r15, %r12
	jmp	.LBB7_119
.LBB7_67:
	movq	%r15, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	cli_malloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB7_74
# BB#68:
	xorl	%esi, %esi
	movl	$.L.str.66, %edx
	movq	%r15, %rdi
	movq	%r13, %rcx
	callq	cli_strtokbuf
	testq	%rax, %rax
	je	.LBB7_71
# BB#69:
	cmpb	$0, (%rax)
	je	.LBB7_71
# BB#70:
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	messageSetDispositionType
	movl	$1, %esi
	movl	$.L.str.66, %edx
	movq	%r15, %rdi
	movq	%r13, %rcx
	callq	cli_strtokbuf
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	messageAddArgument
.LBB7_71:
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rbx, %rdi
	callq	messageHasFilename
	testl	%eax, %eax
	jne	.LBB7_124
# BB#72:
	movl	$.L.str.70, %esi
	movq	%rbx, %rdi
	callq	messageAddArgument
	testq	%rbp, %rbp
	jne	.LBB7_125
	jmp	.LBB7_126
.LBB7_73:
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r15, %rsi
	callq	messageSetEncoding
	xorl	%r13d, %r13d
	testq	%rbp, %rbp
	jne	.LBB7_125
	jmp	.LBB7_126
.LBB7_74:
	testq	%rbp, %rbp
	je	.LBB7_129
# BB#75:
	movq	%rbp, %rdi
	jmp	.LBB7_128
.LBB7_76:
	xorl	%r13d, %r13d
	movl	$.L.str.62, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	testq	%rbp, %rbp
	jne	.LBB7_125
	jmp	.LBB7_126
.LBB7_77:                               # %.preheader142.i
	callq	__ctype_b_loc
	movq	%rax, %rbx
	movq	(%rax), %rax
.LBB7_78:                               # =>This Inner Loop Header: Depth=1
	movsbq	(%r15), %rcx
	incq	%r15
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB7_78
# BB#79:
	leaq	-1(%r15), %r12
	cmpb	$34, %cl
	cmoveq	%r15, %r12
	cmpb	$47, (%r12)
	je	.LBB7_118
# BB#81:
	xorl	%esi, %esi
	movl	$.L.str.66, %edx
	movq	%r12, %rdi
	movq	%r13, %rcx
	callq	cli_strtokbuf
	testq	%rax, %rax
	je	.LBB7_118
# BB#82:
	cmpb	$0, (%rax)
	je	.LBB7_118
# BB#83:
	movq	%rax, %r15
	movq	%r13, %rdi
	callq	cli_strdup
	testq	%rax, %rax
	je	.LBB7_132
# BB#84:
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rbp, %r14
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rbx, %rbp
	movq	%r15, %rdi
	movq	%rax, %r15
.LBB7_85:                               # %.preheader.i34
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_90 Depth 2
                                        #     Child Loop BB7_102 Depth 2
                                        #     Child Loop BB7_113 Depth 2
	movl	$.L.str.67, %esi
	callq	strtok
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	messageSetMimeType
	movl	%eax, %ebx
	xorl	%edi, %edi
	movl	$.L.str.66, %esi
	callq	strtok
	movq	%rax, %rdi
	testq	%rdi, %rdi
	je	.LBB7_116
# BB#86:                                #   in Loop: Header=BB7_85 Depth=1
	testl	%ebx, %ebx
	je	.LBB7_113
# BB#87:                                #   in Loop: Header=BB7_85 Depth=1
	movq	%rdi, %rbx
	callq	strlen
	testl	%eax, %eax
	js	.LBB7_96
# BB#88:                                #   in Loop: Header=BB7_85 Depth=1
	leal	1(%rax), %ecx
	cmpl	%eax, %ecx
	jle	.LBB7_97
# BB#89:                                #   in Loop: Header=BB7_85 Depth=1
	movslq	%eax, %rdx
	movq	%rbx, %rdi
	movb	(%rdi,%rdx), %cl
.LBB7_90:                               #   Parent Loop BB7_85 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rax
	testb	%cl, %cl
	je	.LBB7_92
# BB#91:                                #   in Loop: Header=BB7_90 Depth=2
	movb	$0, (%rdi,%rax)
.LBB7_92:                               #   in Loop: Header=BB7_90 Depth=2
	testl	%eax, %eax
	jle	.LBB7_98
# BB#93:                                #   in Loop: Header=BB7_90 Depth=2
	movq	(%rbp), %rdx
	movsbq	-1(%rdi,%rax), %rcx
	cmpw	$0, (%rdx,%rcx,2)
	js	.LBB7_98
# BB#94:                                # %switch.early.test.i.i.i
                                        #   in Loop: Header=BB7_90 Depth=2
	cmpb	$13, %cl
	je	.LBB7_98
# BB#95:                                # %switch.early.test.i.i.i
                                        #   in Loop: Header=BB7_90 Depth=2
	leaq	-1(%rax), %rdx
	cmpb	$10, %cl
	jne	.LBB7_90
	jmp	.LBB7_98
.LBB7_96:                               #   in Loop: Header=BB7_85 Depth=1
	xorl	%eax, %eax
.LBB7_97:                               #   in Loop: Header=BB7_85 Depth=1
	movq	%rbx, %rdi
.LBB7_98:                               # %strstrip.exit.i
                                        #   in Loop: Header=BB7_85 Depth=1
	cmpb	$34, -1(%rdi,%rax)
	leaq	-1(%rax), %rax
	jne	.LBB7_108
# BB#99:                                #   in Loop: Header=BB7_85 Depth=1
	movb	$0, (%rdi,%rax)
	movq	%rbx, %rdi
	callq	strlen
	movq	%rbx, %rdi
	testl	%eax, %eax
	js	.LBB7_113
# BB#100:                               #   in Loop: Header=BB7_85 Depth=1
	leal	1(%rax), %ecx
	cmpl	%eax, %ecx
	jle	.LBB7_108
# BB#101:                               #   in Loop: Header=BB7_85 Depth=1
	movslq	%eax, %rdx
	movb	(%rdi,%rdx), %cl
.LBB7_102:                              #   Parent Loop BB7_85 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rax
	testb	%cl, %cl
	je	.LBB7_104
# BB#103:                               #   in Loop: Header=BB7_102 Depth=2
	movb	$0, (%rdi,%rax)
.LBB7_104:                              #   in Loop: Header=BB7_102 Depth=2
	testl	%eax, %eax
	jle	.LBB7_108
# BB#105:                               #   in Loop: Header=BB7_102 Depth=2
	movq	(%rbp), %rdx
	movsbq	-1(%rdi,%rax), %rcx
	cmpw	$0, (%rdx,%rcx,2)
	js	.LBB7_108
# BB#106:                               # %switch.early.test.i.i128.i
                                        #   in Loop: Header=BB7_102 Depth=2
	cmpb	$13, %cl
	je	.LBB7_108
# BB#107:                               # %switch.early.test.i.i128.i
                                        #   in Loop: Header=BB7_102 Depth=2
	leaq	-1(%rax), %rdx
	cmpb	$10, %cl
	jne	.LBB7_102
.LBB7_108:                              # %strstrip.exit131.i
                                        #   in Loop: Header=BB7_85 Depth=1
	testq	%rax, %rax
	je	.LBB7_113
# BB#109:                               #   in Loop: Header=BB7_85 Depth=1
	movl	$32, %esi
	movq	%rdi, %rbx
	callq	strchr
	testq	%rax, %rax
	je	.LBB7_111
# BB#110:                               #   in Loop: Header=BB7_85 Depth=1
	xorl	%esi, %esi
	movl	$.L.str.68, %edx
	movq	%rbx, %rdi
	movq	24(%rsp), %rcx          # 8-byte Reload
	callq	cli_strtokbuf
	movq	%r15, %rdi
	movq	%rax, %rsi
	jmp	.LBB7_112
.LBB7_111:                              #   in Loop: Header=BB7_85 Depth=1
	movq	%r15, %rdi
	movq	%rbx, %rsi
.LBB7_112:                              # %strstrip.exit131.thread.i.preheader
                                        #   in Loop: Header=BB7_85 Depth=1
	callq	messageSetMimeSubtype
	movq	%rbx, %rdi
.LBB7_113:                              # %strstrip.exit131.thread.i
                                        #   Parent Loop BB7_85 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	(%rdi), %rax
	testq	%rax, %rax
	je	.LBB7_116
# BB#114:                               #   in Loop: Header=BB7_113 Depth=2
	movq	(%rbp), %rcx
	incq	%rdi
	testb	$32, 1(%rcx,%rax,2)
	je	.LBB7_113
# BB#115:                               # %.loopexit141.i
                                        #   in Loop: Header=BB7_85 Depth=1
	cmpb	$0, (%rdi)
	jne	.LBB7_85
.LBB7_116:                              # %.thread133.i
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	jmp	.LBB7_119
.LBB7_118:
	movq	%rbp, %r14
.LBB7_119:                              # %.thread136.i
	movl	$1, %esi
	movl	$.L.str.66, %edx
	movq	%r12, %rdi
	movq	%r13, %rcx
	callq	cli_strtokbuf
	testq	%rax, %rax
	je	.LBB7_123
# BB#120:                               # %.lr.ph.i.preheader
	movl	$2, %ebp
	movq	(%rsp), %rbx            # 8-byte Reload
.LBB7_121:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.69, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	messageAddArguments
	movl	$.L.str.66, %edx
	movq	%r12, %rdi
	movl	%ebp, %esi
	movq	%r13, %rcx
	callq	cli_strtokbuf
	incl	%ebp
	testq	%rax, %rax
	jne	.LBB7_121
# BB#122:
	movq	%r14, %rbp
	testq	%rbp, %rbp
	jne	.LBB7_125
	jmp	.LBB7_126
.LBB7_123:
	movq	%r14, %rbp
.LBB7_124:                              # %.loopexit.i
	testq	%rbp, %rbp
	je	.LBB7_126
.LBB7_125:
	movq	%rbp, %rdi
	callq	free
.LBB7_126:
	xorl	%r14d, %r14d
	testq	%r13, %r13
	je	.LBB7_129
.LBB7_127:
	movq	%r13, %rdi
.LBB7_128:                              # %parseMimeHeader.exit
	callq	free
.LBB7_129:                              # %parseMimeHeader.exit
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB7_130:                              # %parseMimeHeader.exit
	movq	%r13, %rdi
	callq	free
.LBB7_131:                              # %.thread
	movl	%r14d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_132:
	testq	%rbp, %rbp
	je	.LBB7_127
# BB#133:
	movq	%rbp, %rdi
	callq	free
	jmp	.LBB7_127
.Lfunc_end7:
	.size	parseEmailHeader, .Lfunc_end7-parseEmailHeader
	.cfi_endproc

	.p2align	4, 0x90
	.type	checkURLs,@function
checkURLs:                              # @checkURLs
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi91:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi92:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 56
	subq	$344, %rsp              # imm = 0x158
.Lcfi94:
	.cfi_def_cfa_offset 400
.Lcfi95:
	.cfi_offset %rbx, -56
.Lcfi96:
	.cfi_offset %r12, -48
.Lcfi97:
	.cfi_offset %r13, -40
.Lcfi98:
	.cfi_offset %r14, -32
.Lcfi99:
	.cfi_offset %r15, -24
.Lcfi100:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r13
	cmpl	$3, (%rbx)
	je	.LBB8_38
# BB#1:
	movq	32(%r14), %rax
	movq	24(%rax), %rcx
	testb	$8, 8(%rcx)
	jne	.LBB8_3
# BB#2:
	xorl	%eax, %eax
	jmp	.LBB8_4
.LBB8_3:
	movq	56(%rax), %rax
	movb	24(%rax), %al
	andb	$1, %al
.LBB8_4:
	movzbl	%al, %eax
	movl	%eax, 4(%rsp)
	movl	$0, (%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rsp)
	movq	$0, 24(%rsp)
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	messageToBlob
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB8_37
# BB#5:
	movq	%r15, %rdi
	callq	blobGetDataSize
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB8_36
# BB#6:
	cmpq	$102401, %rbp           # imm = 0x19001
	jb	.LBB8_8
# BB#7:
	movl	$.L.str.145, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB8_36
.LBB8_8:
	leaq	8(%rsp), %rax
	movl	$0, (%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	$0, 16(%rax)
	movl	$.L.str.146, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r15, %rdi
	callq	blobGetData
	movq	72(%r13), %rcx
	movq	56(%rcx), %r8
	movq	%rsp, %rcx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	html_normalise_mem
	testl	%eax, %eax
	je	.LBB8_36
# BB#9:
	movl	$.L.str.147, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpl	$0, 4(%rsp)
	je	.LBB8_12
# BB#10:
	movq	(%r14), %rsi
	movq	32(%r14), %rdx
	movq	%rsp, %rcx
	movq	%r13, %rdi
	callq	phishingScan
	cmpl	$1, %eax
	jne	.LBB8_12
# BB#11:
	orb	$1, 88(%r13)
	movl	$3, (%rbx)
	movl	$.L.str.144, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB8_12:
	testl	%r12d, %r12d
	je	.LBB8_36
# BB#13:
	movq	32(%r14), %rax
	cmpb	$0, 40(%rax)
	jns	.LBB8_36
# BB#14:
	cmpl	$3, (%rbx)
	je	.LBB8_36
# BB#15:
	movq	(%r14), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	callq	tableCreate
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB8_36
# BB#16:                                # %.preheader69.i
	movl	(%rsp), %r12d
	cmpl	$6, %r12d
	jl	.LBB8_23
# BB#17:                                # %.lr.ph77.i.preheader
	movl	$6, %ebp
	xorl	%r13d, %r13d
.LBB8_18:                               # %.lr.ph77.i
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %r14
	movq	-8(%r14,%rbp,8), %rbx
	movl	$.L.str.148, %edi
	movl	$7, %edx
	movq	%rbx, %rsi
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB8_19
.LBB8_22:                               #   in Loop: Header=BB8_18 Depth=1
	cmpl	$4, %r13d
	jg	.LBB8_23
.LBB8_39:                               #   in Loop: Header=BB8_18 Depth=1
	movslq	%r12d, %rax
	cmpq	%rax, %rbp
	leaq	1(%rbp), %rbp
	jl	.LBB8_18
	jmp	.LBB8_23
.LBB8_19:                               #   in Loop: Header=BB8_18 Depth=1
	movl	$46, %esi
	movq	%rbx, %rdi
	callq	strrchr
	testq	%rax, %rax
	je	.LBB8_22
# BB#20:                                #   in Loop: Header=BB8_18 Depth=1
	movl	$.L.str.149, %esi
	movq	%rax, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	jne	.LBB8_22
# BB#21:                                #   in Loop: Header=BB8_18 Depth=1
	movslq	%r13d, %r12
	movq	(%r14,%r12,8), %rsi
	movl	$.L.str.150, %edi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	cli_dbgmsg
	movq	16(%rsp), %rax
	movq	(%rax,%r12,8), %rcx
	leal	1(%r12), %r13d
	movq	%rbx, (%rax,%r12,8)
	movq	16(%rsp), %rax
	movq	%rcx, -8(%rax,%rbp,8)
	movl	(%rsp), %r12d
	cmpl	$4, %r13d
	jle	.LBB8_39
.LBB8_23:                               # %.preheader.i
	testl	%r12d, %r12d
	jle	.LBB8_35
# BB#24:                                # %.lr.ph.i
	leaq	80(%rsp), %rbx
	leaq	48(%rsp), %r14
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
.LBB8_25:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rax
	movq	(%rax,%r12,8), %rbp
	movl	$.L.str.148, %edi
	movl	$7, %edx
	movq	%rbp, %rsi
	callq	strncasecmp
	testl	%eax, %eax
	jne	.LBB8_34
# BB#26:                                #   in Loop: Header=BB8_25 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rsi
	callq	tableFind
	cmpl	$1, %eax
	jne	.LBB8_27
# BB#33:                                #   in Loop: Header=BB8_25 Depth=1
	movl	$.L.str.151, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	jmp	.LBB8_34
.LBB8_27:                               #   in Loop: Header=BB8_25 Depth=1
	movl	$37, %esi
	movq	%rbp, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB8_30
# BB#28:                                #   in Loop: Header=BB8_25 Depth=1
	movl	$64, %esi
	movq	%rbp, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB8_30
# BB#29:                                #   in Loop: Header=BB8_25 Depth=1
	movl	$.L.str.152, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_warnmsg
.LBB8_30:                               #   in Loop: Header=BB8_25 Depth=1
	cmpl	$5, %r13d
	je	.LBB8_31
# BB#32:                                #   in Loop: Header=BB8_25 Depth=1
	movl	$1, %edx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rsi
	callq	tableInsert
	movl	$.L.str.154, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	movl	$256, %edx              # imm = 0x100
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	strncpy
	movb	$0, 336(%rsp)
	movq	%rbx, %rdi
	callq	sanitiseName
	movq	%rbp, %rdi
	callq	cli_strdup
	movq	%rax, 48(%rsp)
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, 56(%rsp)
	movq	%rbx, 64(%rsp)
	movl	$0, 72(%rsp)
	movq	%r14, %rdi
	callq	getURL
	movq	48(%rsp), %rdi
	callq	free
	incl	%r13d
.LBB8_34:                               # %.thread65.i
                                        #   in Loop: Header=BB8_25 Depth=1
	incq	%r12
	movslq	(%rsp), %rax
	cmpq	%rax, %r12
	jl	.LBB8_25
.LBB8_35:                               # %.loopexit.i
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	tableDestroy
.LBB8_36:
	movq	%r15, %rdi
	callq	blobDestroy
.LBB8_37:                               # %hrefs_done.exit
	movq	%rsp, %rdi
	callq	html_tag_arg_free
.LBB8_38:
	addq	$344, %rsp              # imm = 0x158
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_31:                               # %.thread.i
	movl	$.L.str.153, %edi
	movl	$5, %edx
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_warnmsg
	jmp	.LBB8_35
.Lfunc_end8:
	.size	checkURLs, .Lfunc_end8-checkURLs
	.cfi_endproc

	.p2align	4, 0x90
	.type	boundaryStart,@function
boundaryStart:                          # @boundaryStart
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi104:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi105:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 56
	subq	$1016, %rsp             # imm = 0x3F8
.Lcfi107:
	.cfi_def_cfa_offset 1072
.Lcfi108:
	.cfi_offset %rbx, -56
.Lcfi109:
	.cfi_offset %r12, -48
.Lcfi110:
	.cfi_offset %r13, -40
.Lcfi111:
	.cfi_offset %r14, -32
.Lcfi112:
	.cfi_offset %r15, -24
.Lcfi113:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	xorl	%r14d, %r14d
	testq	%r13, %r13
	je	.LBB9_27
# BB#1:
	testq	%r12, %r12
	je	.LBB9_27
# BB#2:
	movb	(%r13), %al
	cmpb	$45, %al
	je	.LBB9_4
# BB#3:
	cmpb	$40, %al
	jne	.LBB9_27
.LBB9_4:
	movl	$45, %esi
	movq	%r13, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB9_27
# BB#5:
	movq	%r13, %rdi
	callq	strlen
	cmpq	$1001, %rax             # imm = 0x3E9
	ja	.LBB9_7
# BB#6:
	movq	%rsp, %rsi
	movq	%r13, %rdi
	callq	rfc822comments
	movq	%rax, %rbx
	xorl	%r15d, %r15d
	jmp	.LBB9_8
.LBB9_7:
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	rfc822comments
	movq	%rax, %r15
	movq	%r15, %rbx
.LBB9_8:
	testq	%rbx, %rbx
	cmoveq	%r13, %rbx
	cmpb	$45, (%rbx)
	jne	.LBB9_10
# BB#9:
	movb	1(%rbx), %bpl
	testb	%bpl, %bpl
	je	.LBB9_10
# BB#12:
	addq	$2, %rbx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	strstr
	testq	%rax, %rax
	jne	.LBB9_14
# BB#13:
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	strstr
	testq	%rax, %rax
	je	.LBB9_21
.LBB9_14:                               # %.preheader.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB9_15:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB9_23
# BB#16:                                #   in Loop: Header=BB9_15 Depth=1
	cmpb	$45, (%rbx)
	leaq	1(%rbx), %rbx
	je	.LBB9_15
# BB#17:
	leaq	2(%r13), %rbx
	.p2align	4, 0x90
.LBB9_18:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB9_23
# BB#19:                                #   in Loop: Header=BB9_18 Depth=1
	cmpb	$45, (%rbx)
	leaq	1(%rbx), %rbx
	je	.LBB9_18
# BB#20:
	xorl	%ebp, %ebp
	testq	%r15, %r15
	jne	.LBB9_24
	jmp	.LBB9_25
.LBB9_10:
	testq	%r15, %r15
	je	.LBB9_27
# BB#11:
	movq	%r15, %rdi
	callq	free
	jmp	.LBB9_27
.LBB9_21:
	movl	%ebp, %eax
	xorl	%ebp, %ebp
	cmpb	$45, %al
	jne	.LBB9_23
# BB#22:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	strcasecmp
	xorl	%ebp, %ebp
	testl	%eax, %eax
	sete	%bpl
.LBB9_23:                               # %.loopexit
	testq	%r15, %r15
	je	.LBB9_25
.LBB9_24:
	movq	%r15, %rdi
	callq	free
.LBB9_25:
	testl	%ebp, %ebp
	je	.LBB9_27
# BB#26:
	movl	$.L.str.188, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	movq	%r13, %rdx
	callq	cli_dbgmsg
	movl	$1, %r14d
.LBB9_27:
	movl	%r14d, %eax
	addq	$1016, %rsp             # imm = 0x3F8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	boundaryStart, .Lfunc_end9-boundaryStart
	.cfi_endproc

	.p2align	4, 0x90
	.type	exportBinhexMessage,@function
exportBinhexMessage:                    # @exportBinhexMessage
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi114:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi116:
	.cfi_def_cfa_offset 32
.Lcfi117:
	.cfi_offset %rbx, -24
.Lcfi118:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	%rbx, %rdi
	callq	messageGetEncoding
	testl	%eax, %eax
	jne	.LBB10_2
# BB#1:
	movl	$.L.str.189, %esi
	movq	%rbx, %rdi
	callq	messageSetEncoding
.LBB10_2:
	movq	(%r14), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	messageToFileblob
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB10_4
# BB#3:
	movq	%rbx, %rdi
	callq	fileblobGetFilename
	movq	%rax, %rcx
	movl	$.L.str.190, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	callq	fileblobScanAndDestroy
	cmpl	$1, %eax
	sete	%bl
	incl	8(%r14)
	jmp	.LBB10_5
.LBB10_4:
	movq	(%r14), %rsi
	xorl	%ebx, %ebx
	movl	$.L.str.191, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB10_5:
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	exportBinhexMessage, .Lfunc_end10-exportBinhexMessage
	.cfi_endproc

	.p2align	4, 0x90
	.type	do_multipart,@function
do_multipart:                           # @do_multipart
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi119:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi120:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi121:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi122:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi123:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi125:
	.cfi_def_cfa_offset 96
.Lcfi126:
	.cfi_offset %rbx, -56
.Lcfi127:
	.cfi_offset %r12, -48
.Lcfi128:
	.cfi_offset %r13, -40
.Lcfi129:
	.cfi_offset %r14, -32
.Lcfi130:
	.cfi_offset %r15, -24
.Lcfi131:
	.cfi_offset %rbp, -16
	movq	%r9, %rbp
	movl	%edx, %r14d
	movq	%rdi, %r13
	movslq	%r14d, %rdx
	movq	(%rsi,%rdx,8), %rbx
	movq	32(%r8), %rax
	movq	24(%rax), %rdi
	testb	$8, 8(%rdi)
	jne	.LBB11_2
# BB#1:
	xorl	%r15d, %r15d
	testq	%rbx, %rbx
	jne	.LBB11_3
	jmp	.LBB11_57
.LBB11_2:
	movq	56(%rax), %rax
	movb	24(%rax), %r15b
	andb	$1, %r15b
	testq	%rbx, %rbx
	je	.LBB11_57
.LBB11_3:
	cmpl	$1, (%rcx)
	jne	.LBB11_57
# BB#4:
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	messageGetMimeType
	movl	%eax, %ecx
	xorl	%r12d, %r12d
	movl	$.L.str.193, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	movl	%ecx, %edx
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	callq	messageGetMimeType
	cmpl	$7, %eax
	ja	.LBB11_12
# BB#5:
	movl	%eax, %eax
	jmpq	*.LJTI11_0(,%rax,8)
.LBB11_6:
	movl	$.L.str.194, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	callq	binhexBegin
	testq	%r13, %r13
	je	.LBB11_33
# BB#7:
	testq	%rax, %rax
	movq	16(%rsp), %r14          # 8-byte Reload
	je	.LBB11_10
# BB#8:
	movl	$.L.str.195, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	exportBinhexMessage
	testb	%al, %al
	je	.LBB11_10
# BB#9:
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$3, (%rax)
.LBB11_10:
	cmpq	%rbp, %r13
	je	.LBB11_37
# BB#11:
	movq	%r13, %rdi
	callq	messageDestroy
	jmp	.LBB11_37
.LBB11_12:
	movq	%rbx, %rdi
	callq	messageGetMimeType
	movl	%eax, %ecx
	xorl	%r12d, %r12d
	movl	$.L.str.211, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	cli_warnmsg
	jmp	.LBB11_46
.LBB11_13:
	movq	%rbx, %rdi
	callq	messageGetEncoding
	movl	%eax, %ecx
	movl	$.L.str.206, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	callq	messageGetEncoding
	cmpl	$4, %eax
	ja	.LBB11_16
# BB#14:
	movl	$25, %ecx
	btl	%eax, %ecx
	jae	.LBB11_16
# BB#15:
	movq	%rbx, %rdi
	callq	encodingLine
	testq	%rax, %rax
	je	.LBB11_39
.LBB11_16:
	movl	$.L.str.208, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.239, %esi
	movq	%rbx, %rdi
	callq	messageAddArgument
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	(%r15), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	messageToFileblob
	movq	%rax, %rbx
	testq	%rbx, %rbx
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB11_19
# BB#17:                                # %saveTextPart.exit
	movl	$.L.str.240, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	incl	8(%r15)
	movq	%rbx, %rdi
	callq	fileblobScanAndDestroy
	cmpl	$1, %eax
	jne	.LBB11_19
# BB#18:
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$3, (%rax)
.LBB11_19:                              # %saveTextPart.exit.thread
	movq	(%r14,%rbp,8), %rdi
	callq	messageDestroy
	movq	$0, (%r14,%rbp,8)
	jmp	.LBB11_57
.LBB11_20:
	movl	104(%rsp), %ebp
	movl	$.L.str.209, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	96(%rsp), %rax
	movq	(%rax), %rsi
	incl	%ebp
	movq	%rbx, %rdi
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	%ebp, %ecx
	callq	parseEmailBody
	movl	%eax, %ecx
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%ecx, (%rax)
	movl	$.L.str.210, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	cli_dbgmsg
	jmp	.LBB11_40
.LBB11_21:
	movq	%rbx, %rdi
	callq	messageGetDispositionType
	movq	%rax, %r14
	xorl	%r12d, %r12d
	movl	$.L.str.198, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	movl	$.L.str.199, %esi
	movq	%r14, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB11_46
# BB#22:
	cmpb	$0, (%r14)
	je	.LBB11_25
# BB#23:
	movl	$.L.str.200, %esi
	movq	%r14, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB11_25
# BB#24:
	movl	$.L.str.205, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	jmp	.LBB11_57
.LBB11_25:
	testq	%r13, %r13
	je	.LBB11_28
# BB#26:
	cmpq	%rbp, %r13
	je	.LBB11_28
# BB#27:
	movq	%r13, %rdi
	callq	messageDestroy
.LBB11_28:
	movq	%rbx, %rdi
	callq	messageGetMimeSubtype
	movq	%rax, %rbp
	movl	$.L.str.201, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	24(%r14), %rdi
	movq	%rbp, %rsi
	callq	tableFind
	cmpl	$1, %eax
	jne	.LBB11_30
# BB#29:
	movq	%rbx, %rdi
	callq	messageGetEncoding
	testl	%eax, %eax
	je	.LBB11_58
.LBB11_30:
	movq	24(%r14), %rdi
	movq	%rbp, %rsi
	callq	tableFind
	xorl	%ecx, %ecx
	cmpl	$3, %eax
	sete	%al
	jne	.LBB11_41
# BB#31:
	movq	32(%r14), %rdx
	movb	40(%rdx), %dl
	testb	%dl, %dl
	jns	.LBB11_41
# BB#32:
	movl	$1, %ecx
	jmp	.LBB11_43
.LBB11_33:
	testq	%rax, %rax
	movq	16(%rsp), %rbp          # 8-byte Reload
	je	.LBB11_37
# BB#34:
	movl	$.L.str.196, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	exportBinhexMessage
	testb	%al, %al
	je	.LBB11_36
# BB#35:
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$3, (%rax)
.LBB11_36:
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rdi
	callq	messageReset
.LBB11_37:
	movq	%rbx, %rdi
	callq	messageGetBody
	movb	$1, %r12b
	xorl	%r13d, %r13d
	testq	%rax, %rax
	jne	.LBB11_46
# BB#38:
	movl	$.L.str.197, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB11_46
.LBB11_39:
	movl	$.L.str.207, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB11_40:
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	(%rbx,%rbp,8), %rdi
	callq	messageDestroy
	movq	$0, (%rbx,%rbp,8)
	jmp	.LBB11_57
.LBB11_41:
	testb	%r15b, %r15b
	je	.LBB11_44
# BB#42:
	movb	%al, %cl
.LBB11_43:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	checkURLs
.LBB11_44:
	movl	$.L.str.204, %esi
	movq	%rbx, %rdi
	callq	messageAddArgument
	xorl	%r12d, %r12d
.LBB11_45:
	xorl	%r13d, %r13d
.LBB11_46:
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$3, (%rax)
	je	.LBB11_56
# BB#47:
	testb	%r12b, %r12b
	je	.LBB11_50
# BB#48:
	movl	$.L.str.212, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	callq	messageGetBody
	testq	%rax, %rax
	je	.LBB11_54
# BB#49:
	movq	96(%rsp), %rax
	movq	%rax, %r14
	movq	(%r14), %rbp
	movq	%rbx, %rdi
	callq	messageGetBody
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	textMove
	movq	%rax, (%r14)
	jmp	.LBB11_54
.LBB11_50:
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	messageToFileblob
	testq	%rax, %rax
	je	.LBB11_54
# BB#51:
	movq	%rax, %rdi
	callq	fileblobScanAndDestroy
	cmpl	$1, %eax
	jne	.LBB11_53
# BB#52:
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$3, (%rax)
.LBB11_53:
	incl	8(%rbp)
.LBB11_54:
	movq	%rbx, %rdi
	callq	messageContainsVirus
	testl	%eax, %eax
	movq	8(%rsp), %rax           # 8-byte Reload
	je	.LBB11_56
# BB#55:
	movl	$3, (%rax)
.LBB11_56:
	movq	%rbx, %rdi
	callq	messageDestroy
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	$0, (%rax,%rcx,8)
.LBB11_57:
	movq	%r13, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_58:
	movq	%rbx, %rdi
	callq	messageHasFilename
	testl	%eax, %eax
	je	.LBB11_60
# BB#59:
	xorl	%r12d, %r12d
	movl	$.L.str.203, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB11_45
.LBB11_60:
	xorl	%r13d, %r13d
	movl	$.L.str.202, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movb	$1, %r12b
	jmp	.LBB11_46
.Lfunc_end11:
	.size	do_multipart, .Lfunc_end11-do_multipart
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI11_0:
	.quad	.LBB11_6
	.quad	.LBB11_46
	.quad	.LBB11_46
	.quad	.LBB11_46
	.quad	.LBB11_13
	.quad	.LBB11_20
	.quad	.LBB11_21
	.quad	.LBB11_46

	.text
	.p2align	4, 0x90
	.type	rfc1341,@function
rfc1341:                                # @rfc1341
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi132:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi133:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi134:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi135:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi136:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi137:
	.cfi_def_cfa_offset 56
	subq	$9496, %rsp             # imm = 0x2518
.Lcfi138:
	.cfi_def_cfa_offset 9552
.Lcfi139:
	.cfi_offset %rbx, -56
.Lcfi140:
	.cfi_offset %r12, -48
.Lcfi141:
	.cfi_offset %r13, -40
.Lcfi142:
	.cfi_offset %r14, -32
.Lcfi143:
	.cfi_offset %r15, -24
.Lcfi144:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movl	$.L.str.213, %esi
	callq	messageFindArgument
	movq	%rax, %r13
	movl	$-1, %eax
	testq	%r13, %r13
	je	.LBB12_53
# BB#1:
	movl	$.L.str.214, %edi
	callq	getenv
	movq	%rax, %rcx
	testq	%rcx, %rcx
	jne	.LBB12_4
# BB#2:
	movl	$.L.str.215, %edi
	callq	getenv
	movq	%rax, %rcx
	testq	%rcx, %rcx
	jne	.LBB12_4
# BB#3:
	movl	$.L.str.216, %edi
	callq	getenv
	testq	%rax, %rax
	movl	$.L.str.217, %ecx
	cmovneq	%rax, %rcx
.LBB12_4:
	leaq	64(%rsp), %rbp
	movl	$256, %esi              # imm = 0x100
	movl	$.L.str.218, %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	snprintf
	movl	$448, %esi              # imm = 0x1C0
	movq	%rbp, %rdi
	callq	mkdir
	movl	%eax, %ebx
	callq	__errno_location
	movq	%rax, %rbp
	testl	%ebx, %ebx
	jns	.LBB12_6
# BB#5:
	cmpl	$17, (%rbp)
	jne	.LBB12_47
.LBB12_6:                               # %._crit_edge125
	cmpl	$17, (%rbp)
	jne	.LBB12_10
# BB#7:
	leaq	64(%rsp), %rsi
	leaq	1296(%rsp), %rdx
	movl	$1, %edi
	callq	__xstat
	testl	%eax, %eax
	js	.LBB12_49
# BB#8:
	movl	1320(%rsp), %edx
	testb	$63, %dl
	je	.LBB12_10
# BB#9:
	andl	$511, %edx              # imm = 0x1FF
	leaq	64(%rsp), %rsi
	movl	$.L.str.221, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB12_10:
	movl	$.L.str.222, %esi
	movq	%r12, %rdi
	callq	messageFindArgument
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB12_50
# BB#11:
	movq	%r12, %rdi
	callq	messageGetFilename
	movq	%rax, %r15
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	strlen
	leaq	10(%rbx,%rax), %rdi
	callq	cli_malloc
	movq	%rbp, %rbx
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB12_13
# BB#12:
	movl	$.L.str.223, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r13, %rdx
	movq	%rbx, %rcx
	callq	sprintf
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	messageAddArgument
	movq	%rbp, %rdi
	callq	free
.LBB12_13:
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	testq	%r15, %r15
	je	.LBB12_15
# BB#14:
	movl	$.L.str.224, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_warnmsg
	movq	%r15, %rdi
	callq	free
.LBB12_15:
	leaq	64(%rsp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	callq	messageToFileblob
	testq	%rax, %rax
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB12_48
# BB#16:
	movq	%rax, %rdi
	callq	fileblobDestroy
	movl	$.L.str.225, %esi
	movq	%r12, %rdi
	callq	messageFindArgument
	movq	%rax, %rbx
	movl	$.L.str.227, %ecx
	testq	%rbx, %rbx
	cmovneq	%rbx, %rcx
	movl	$.L.str.226, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	cli_dbgmsg
	testq	%rbx, %rbx
	je	.LBB12_46
# BB#17:
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbp, %rdi
	callq	strtol
	movq	%rax, %rbp
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, %r15
	movq	%rbx, %rdi
	callq	free
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	cmpl	%r15d, %ebp
	movq	8(%rsp), %rbp           # 8-byte Reload
	jne	.LBB12_46
# BB#18:
	leaq	64(%rsp), %rdi
	callq	opendir
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB12_46
# BB#19:
	movq	%r13, %rdi
	callq	sanitiseName
	leaq	480(%rsp), %rbx
	movl	$256, %esi              # imm = 0x100
	movl	$.L.str.156, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rcx
	movq	%r13, %r8
	callq	snprintf
	movl	$.L.str.228, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	movl	$.L.str.157, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB12_54
# BB#20:
	leaq	56(%rsp), %rdi
	callq	time
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB12_45
# BB#21:                                # %.lr.ph123
	movl	$1, %ebp
	leaq	1024(%rsp), %r14
	leaq	752(%rsp), %r15
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	%r12, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB12_22:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_24 Depth 2
                                        #     Child Loop BB12_34 Depth 2
                                        #       Child Loop BB12_37 Depth 3
	movl	$257, %esi              # imm = 0x101
	movl	$.L.str.230, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r13, %rcx
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	%rbp, %r8
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	snprintf
	jmp	.LBB12_24
.LBB12_23:                              #   in Loop: Header=BB12_24 Depth=2
	movl	$.L.str.231, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_warnmsg
	.p2align	4, 0x90
.LBB12_24:                              #   Parent Loop BB12_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r12, %rdi
	callq	readdir
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB12_44
# BB#25:                                # %.lr.ph130
                                        #   in Loop: Header=BB12_24 Depth=2
	cmpq	$0, (%rbp)
	je	.LBB12_24
# BB#26:                                #   in Loop: Header=BB12_24 Depth=2
	addq	$19, %rbp
	movl	$256, %esi              # imm = 0x100
	movl	$.L.str.156, %edx
	xorl	%eax, %eax
	movq	%r15, %rdi
	leaq	64(%rsp), %rcx
	movq	%rbp, %r8
	callq	snprintf
	movq	%r14, %rdi
	callq	strlen
	movq	%r14, %rdi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB12_31
# BB#27:                                #   in Loop: Header=BB12_24 Depth=2
	cmpb	$0, cli_leavetemps_flag(%rip)
	je	.LBB12_24
# BB#28:                                #   in Loop: Header=BB12_24 Depth=2
	movl	$1, %edi
	movq	%r15, %rsi
	leaq	336(%rsp), %rdx
	callq	__xstat
	testl	%eax, %eax
	js	.LBB12_24
# BB#29:                                #   in Loop: Header=BB12_24 Depth=2
	movq	56(%rsp), %rax
	subq	424(%rsp), %rax
	cmpq	$604801, %rax           # imm = 0x93A81
	jl	.LBB12_24
# BB#30:                                #   in Loop: Header=BB12_24 Depth=2
	movq	%r15, %rdi
	callq	unlink
	testl	%eax, %eax
	jns	.LBB12_23
	jmp	.LBB12_24
.LBB12_31:                              #   in Loop: Header=BB12_22 Depth=1
	movl	$.L.str.2, %esi
	movq	%r15, %rdi
	callq	fopen
	testq	%rax, %rax
	je	.LBB12_56
# BB#32:                                # %.preheader118
                                        #   in Loop: Header=BB12_22 Depth=1
	movl	$8191, %esi             # imm = 0x1FFF
	leaq	1296(%rsp), %r13
	movq	%r13, %rdi
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB12_41
# BB#33:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB12_22 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB12_34:                              # %.lr.ph
                                        #   Parent Loop BB12_22 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_37 Depth 3
	cmpb	$10, 1296(%rsp)
	jne	.LBB12_36
# BB#35:                                #   in Loop: Header=BB12_34 Depth=2
	incl	%ebp
	jmp	.LBB12_40
	.p2align	4, 0x90
.LBB12_36:                              #   in Loop: Header=BB12_34 Depth=2
	movq	%r14, %r12
	movq	%r15, %r14
	testl	%ebp, %ebp
	je	.LBB12_38
	.p2align	4, 0x90
.LBB12_37:                              # %.preheader
                                        #   Parent Loop BB12_22 Depth=1
                                        #     Parent Loop BB12_34 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$10, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	leal	-1(%rbp), %r15d
	cmpl	$1, %ebp
	movl	%r15d, %ebp
	jg	.LBB12_37
	jmp	.LBB12_39
.LBB12_38:                              #   in Loop: Header=BB12_34 Depth=2
	xorl	%r15d, %r15d
.LBB12_39:                              # %.loopexit
                                        #   in Loop: Header=BB12_34 Depth=2
	leaq	1296(%rsp), %r13
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	fputs
	movl	%r15d, %ebp
	movq	%r14, %r15
	movq	%r12, %r14
	movq	48(%rsp), %r12          # 8-byte Reload
.LBB12_40:                              # %.backedge
                                        #   in Loop: Header=BB12_34 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	$8191, %esi             # imm = 0x1FFF
	movq	%r13, %rdi
	movq	%rax, %rdx
	callq	fgets
	testq	%rax, %rax
	jne	.LBB12_34
.LBB12_41:                              # %._crit_edge
                                        #   in Loop: Header=BB12_22 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB12_43
# BB#42:                                #   in Loop: Header=BB12_22 Depth=1
	movq	%r15, %rdi
	callq	unlink
.LBB12_43:                              # %.thread127
                                        #   in Loop: Header=BB12_22 Depth=1
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB12_44:                              # %.loopexit128
                                        #   in Loop: Header=BB12_22 Depth=1
	movq	%r12, %rdi
	callq	rewinddir
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpl	32(%rsp), %eax          # 4-byte Folded Reload
	leal	1(%rax), %eax
	movl	%eax, %ebp
	jl	.LBB12_22
.LBB12_45:                              # %._crit_edge124
	movq	%r12, %rdi
	callq	closedir
	movq	%rbx, %rdi
	callq	fclose
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB12_46:                              # %.thread116
	movq	%rbp, %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
	xorl	%eax, %eax
	jmp	.LBB12_53
.LBB12_47:
	leaq	64(%rsp), %rsi
	movl	$.L.str.219, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB12_50
.LBB12_48:
	movq	%r13, %rdi
	callq	free
	movq	%rbp, %rdi
	jmp	.LBB12_51
.LBB12_49:                              # %.critedge
	movl	(%rbp), %edi
	callq	strerror
	movq	%rax, %rcx
	leaq	64(%rsp), %rsi
	movl	$.L.str.220, %edi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	cli_errmsg
.LBB12_50:
	movq	%r13, %rdi
.LBB12_51:
	callq	free
.LBB12_52:
	movl	$-1, %eax
.LBB12_53:
	addq	$9496, %rsp             # imm = 0x2518
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_54:
	leaq	480(%rsp), %rsi
	movl	$.L.str.229, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	%r13, %rdi
	callq	free
	movq	%rbp, %rdi
.LBB12_55:
	callq	free
	movq	%r12, %rdi
	callq	closedir
	jmp	.LBB12_52
.LBB12_56:
	leaq	752(%rsp), %rsi
	movl	$.L.str.232, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	%rbx, %rdi
	callq	fclose
	leaq	480(%rsp), %rdi
	callq	unlink
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB12_55
.Lfunc_end12:
	.size	rfc1341, .Lfunc_end12-rfc1341
	.cfi_endproc

	.p2align	4, 0x90
	.type	isBounceStart,@function
isBounceStart:                          # @isBounceStart
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi145:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi146:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi147:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi148:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi149:
	.cfi_def_cfa_offset 48
.Lcfi150:
	.cfi_offset %rbx, -40
.Lcfi151:
	.cfi_offset %r14, -32
.Lcfi152:
	.cfi_offset %r15, -24
.Lcfi153:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	testq	%rbp, %rbp
	je	.LBB13_5
# BB#1:
	cmpb	$0, (%rbp)
	je	.LBB13_5
# BB#2:
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %r14
	leaq	-6(%r14), %rax
	cmpq	$65, %rax
	jbe	.LBB13_6
.LBB13_5:
	xorl	%eax, %eax
.LBB13_15:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_6:
	movl	$.L.str.4, %esi
	movl	$5, %edx
	movq	%rbp, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB13_9
# BB#7:
	movl	$.L.str.233, %esi
	movl	$6, %edx
	movq	%rbp, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB13_9
# BB#8:
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	cli_filetype
	cmpl	$529, %eax              # imm = 0x211
	sete	%al
	jmp	.LBB13_15
.LBB13_9:
	movb	4(%rbp), %bl
	addq	$5, %rbp
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB13_10:                              # =>This Inner Loop Header: Depth=1
	cmpb	$32, %bl
	jne	.LBB13_12
# BB#11:                                #   in Loop: Header=BB13_10 Depth=1
	incl	%r14d
	jmp	.LBB13_13
	.p2align	4, 0x90
.LBB13_12:                              #   in Loop: Header=BB13_10 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movzbl	%bl, %ecx
	movzwl	(%rax,%rcx,2), %eax
	shrl	$11, %eax
	andl	$1, %eax
	addl	%eax, %r15d
.LBB13_13:                              #   in Loop: Header=BB13_10 Depth=1
	movzbl	(%rbp), %ebx
	incq	%rbp
	testb	%bl, %bl
	jne	.LBB13_10
# BB#14:
	cmpl	$10, %r15d
	setg	%cl
	cmpl	$5, %r14d
	setg	%al
	andb	%cl, %al
	jmp	.LBB13_15
.Lfunc_end13:
	.size	isBounceStart, .Lfunc_end13-isBounceStart
	.cfi_endproc

	.p2align	4, 0x90
	.type	getURL,@function
getURL:                                 # @getURL
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi154:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi155:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi156:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi157:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi158:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi159:
	.cfi_def_cfa_offset 56
	subq	$16952, %rsp            # imm = 0x4238
.Lcfi160:
	.cfi_def_cfa_offset 17008
.Lcfi161:
	.cfi_offset %rbx, -56
.Lcfi162:
	.cfi_offset %r12, -48
.Lcfi163:
	.cfi_offset %r13, -40
.Lcfi164:
	.cfi_offset %r14, -32
.Lcfi165:
	.cfi_offset %r15, -24
.Lcfi166:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	(%rbx), %r15
	movq	8(%rbx), %r14
	movq	16(%rbx), %rbp
	movq	%r15, %rdi
	callq	strlen
	cmpq	$8192, %rax             # imm = 0x2000
	jb	.LBB14_2
# BB#1:
	movl	$.L.str.155, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	jmp	.LBB14_22
.LBB14_2:
	leaq	272(%rsp), %r12
	movl	$256, %esi              # imm = 0x100
	movl	$.L.str.156, %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%rbp, %r8
	callq	snprintf
	movl	$.L.str.157, %esi
	movq	%r12, %rdi
	callq	fopen
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB14_9
# BB#3:
	leaq	272(%rsp), %rdx
	movl	$.L.str.159, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	cmpl	$0, getURL.tcp(%rip)
	jne	.LBB14_6
# BB#4:
	movl	$.L.str.160, %edi
	callq	getprotobyname
	testq	%rax, %rax
	je	.LBB14_10
# BB#5:
	movl	16(%rax), %eax
	movl	%eax, getURL.tcp(%rip)
	callq	endprotoent
.LBB14_6:
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	movw	getURL.default_port(%rip), %bx
	testw	%bx, %bx
	jne	.LBB14_13
# BB#7:
	movl	$.L.str.162, %edi
	movl	$.L.str.160, %esi
	callq	getservbyname
	testq	%rax, %rax
	je	.LBB14_11
# BB#8:
	movzwl	16(%rax), %eax
	rolw	$8, %ax
	jmp	.LBB14_12
.LBB14_9:
	leaq	272(%rsp), %rsi
	movl	$.L.str.158, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB14_22
.LBB14_10:                              # %.critedge
	movl	$.L.str.161, %edi
	jmp	.LBB14_19
.LBB14_11:
	movw	$80, %ax
.LBB14_12:
	movw	%ax, getURL.default_port(%rip)
	callq	endservent
	movw	getURL.default_port(%rip), %bx
.LBB14_13:
	movl	$.L.str.163, %edi
	callq	getenv
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB14_17
# BB#14:
	cmpb	$0, (%rbp)
	je	.LBB14_17
# BB#15:
	movl	$.L.str.148, %esi
	movl	$7, %edx
	movq	%rbp, %rdi
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB14_33
# BB#16:
	movl	$.L.str.164, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_warnmsg
	jmp	.LBB14_20
.LBB14_17:                              # %.thread
	movl	$.L.str.166, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	movl	$.L.str.148, %esi
	movl	$7, %edx
	movq	%r15, %rdi
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB14_23
# BB#18:
	movl	$.L.str.167, %edi
.LBB14_19:
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB14_20:
	movq	%r12, %rdi
.LBB14_21:
	callq	fclose
.LBB14_22:
	addq	$16952, %rsp            # imm = 0x4238
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_23:
	leaq	8752(%rsp), %r14
	addq	$7, %r15
	jmp	.LBB14_27
.LBB14_24:                              # %.loopexit27
                                        #   in Loop: Header=BB14_27 Depth=1
	testb	%al, %al
	je	.LBB14_32
# BB#25:                                #   in Loop: Header=BB14_27 Depth=1
	incq	%r15
	cmpb	$47, %al
	je	.LBB14_101
# BB#26:                                #   in Loop: Header=BB14_27 Depth=1
	movb	%al, (%r14)
	incq	%r14
.LBB14_27:                              # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_31 Depth 2
                                        #       Child Loop BB14_30 Depth 3
	movb	(%r15), %al
	jmp	.LBB14_31
	.p2align	4, 0x90
.LBB14_28:                              # %.preheader
                                        #   in Loop: Header=BB14_31 Depth=2
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movsbq	1(%r15), %rax
	incq	%r15
	xorl	%ebx, %ebx
	testb	$8, 1(%rcx,%rax,2)
	je	.LBB14_31
# BB#29:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB14_31 Depth=2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB14_30:                              # %.lr.ph
                                        #   Parent Loop BB14_27 Depth=1
                                        #     Parent Loop BB14_31 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsbl	%al, %eax
	leal	(%rbx,%rbx,4), %edx
	leal	-48(%rax,%rdx,2), %ebx
	movsbq	1(%r15), %rax
	incq	%r15
	testb	$8, 1(%rcx,%rax,2)
	jne	.LBB14_30
.LBB14_31:                              # %.loopexit27
                                        #   Parent Loop BB14_27 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB14_30 Depth 3
	cmpb	$58, %al
	je	.LBB14_28
	jmp	.LBB14_24
.LBB14_32:                              # %.loopexit28.loopexit
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	jmp	.LBB14_43
.LBB14_33:
	leaq	8752(%rsp), %r14
	movl	$.L.str.165, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	cli_dbgmsg
	addq	$7, %rbp
	jmp	.LBB14_37
.LBB14_34:                              # %.loopexit31
                                        #   in Loop: Header=BB14_37 Depth=1
	testb	%al, %al
	je	.LBB14_42
# BB#35:                                # %.loopexit31
                                        #   in Loop: Header=BB14_37 Depth=1
	cmpb	$47, %al
	je	.LBB14_42
# BB#36:                                #   in Loop: Header=BB14_37 Depth=1
	incq	%rbp
	movb	%al, (%r14)
	incq	%r14
.LBB14_37:                              # %.outer33
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_41 Depth 2
                                        #       Child Loop BB14_40 Depth 3
	movb	(%rbp), %al
	jmp	.LBB14_41
	.p2align	4, 0x90
.LBB14_38:                              # %.preheader30
                                        #   in Loop: Header=BB14_41 Depth=2
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movsbq	1(%rbp), %rax
	incq	%rbp
	xorl	%ebx, %ebx
	testb	$8, 1(%rcx,%rax,2)
	je	.LBB14_41
# BB#39:                                # %.lr.ph67.preheader
                                        #   in Loop: Header=BB14_41 Depth=2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB14_40:                              # %.lr.ph67
                                        #   Parent Loop BB14_37 Depth=1
                                        #     Parent Loop BB14_41 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsbl	%al, %eax
	leal	(%rbx,%rbx,4), %edx
	leal	-48(%rax,%rdx,2), %ebx
	movsbq	1(%rbp), %rax
	incq	%rbp
	testb	$8, 1(%rcx,%rax,2)
	jne	.LBB14_40
.LBB14_41:                              # %.loopexit31
                                        #   Parent Loop BB14_37 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB14_40 Depth 3
	cmpb	$58, %al
	je	.LBB14_38
	jmp	.LBB14_34
.LBB14_42:                              # %.loopexit28.loopexit139
	movb	$1, %al
	movl	%eax, 16(%rsp)          # 4-byte Spill
.LBB14_43:                              # %.loopexit28
	movb	$0, (%r14)
	movl	$0, 92(%rsp)
	movq	$0, 84(%rsp)
	movw	$2, 80(%rsp)
	rolw	$8, %bx
	movw	%bx, 82(%rsp)
	leaq	8752(%rsp), %rdi
	callq	inet_addr
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	je	.LBB14_48
.LBB14_44:
	movl	getURL.tcp(%rip), %edx
	movl	$2, %edi
	movl	$1, %esi
	callq	socket
	movl	%eax, %r14d
	testl	%r14d, %r14d
	js	.LBB14_20
# BB#45:
	movl	$3, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	movl	%r14d, %edi
	callq	fcntl
	movl	%eax, 36(%rsp)          # 4-byte Spill
	cltq
	cmpl	$-1, %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	je	.LBB14_53
# BB#46:
	movq	%rax, %rdx
	orq	$2048, %rdx             # imm = 0x800
	movl	$4, %esi
	xorl	%eax, %eax
	movl	%r14d, %edi
	callq	fcntl
	testl	%eax, %eax
	jns	.LBB14_55
# BB#47:
	callq	__errno_location
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.170, %edi
	jmp	.LBB14_54
.LBB14_48:
	leaq	8752(%rsp), %rdi
	callq	gethostbyname
	testq	%rax, %rax
	je	.LBB14_52
# BB#49:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.LBB14_52
# BB#50:                                # %.thread9
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB14_52
# BB#51:                                # %.thread11
	movl	(%rax), %ebp
	jmp	.LBB14_44
.LBB14_52:                              # %my_r_gethostbyname.exit
	leaq	8752(%rsp), %rsi
	movl	$.L.str.168, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB14_20
.LBB14_53:
	callq	__errno_location
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.169, %edi
.LBB14_54:
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	cli_warnmsg
.LBB14_55:
	movl	%ebp, 84(%rsp)
	xorl	%ebx, %ebx
	leaq	144(%rsp), %rdi
	xorl	%esi, %esi
	movq	%rdi, %r13
	callq	gettimeofday
	leaq	80(%rsp), %rsi
	movl	$16, %edx
	movl	%r14d, %edi
	callq	connect
	testl	%eax, %eax
	je	.LBB14_58
# BB#56:
	callq	__errno_location
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	(%rax), %edi
	leal	-114(%rdi), %eax
	cmpl	$2, %eax
	jae	.LBB14_61
# BB#57:
	movq	%r12, (%rsp)            # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.182, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	movq	%rcx, %rdx
	callq	cli_dbgmsg
	leal	1(%r14), %eax
	movl	%eax, 112(%rsp)         # 4-byte Spill
	movq	144(%rsp), %rbx
	addq	$5, %rbx
	movq	%rbx, 144(%rsp)
	movq	152(%rsp), %rbp
	movl	%r14d, %eax
	sarl	$31, %eax
	shrl	$26, %eax
	addl	%r14d, %eax
	movl	%eax, %edx
	andl	$192, %edx
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movl	%r14d, %ecx
	subl	%edx, %ecx
	movl	$1, %r14d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %r14
	sarl	$6, %eax
	cltq
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	$3, %r12d
	movl	$1, 20(%rsp)            # 4-byte Folded Spill
	jmp	.LBB14_90
.LBB14_58:
	movl	$4, 48(%rsp)
	leaq	544(%rsp), %rcx
	leaq	48(%rsp), %r8
	movl	$1, %esi
	movl	$4, %edx
	movl	%r14d, %edi
	callq	getsockopt
	movl	544(%rsp), %edi
	testl	%edi, %edi
	je	.LBB14_60
# BB#59:
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.187, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	movq	%rcx, %rdx
	callq	cli_warnmsg
	movl	$-1, %ebx
.LBB14_60:                              # %nonblock_connect.exit
	testl	%ebx, %ebx
	jns	.LBB14_62
	jmp	.LBB14_100
.LBB14_61:
	cmpl	$106, %edi
	jne	.LBB14_88
.LBB14_62:
	cmpl	$-1, 36(%rsp)           # 4-byte Folded Reload
	je	.LBB14_65
# BB#63:
	movl	$4, %esi
	xorl	%eax, %eax
	movl	%r14d, %edi
	movq	128(%rsp), %rdx         # 8-byte Reload
	callq	fcntl
	testl	%eax, %eax
	je	.LBB14_65
# BB#64:
	callq	__errno_location
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.171, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	cli_warnmsg
.LBB14_65:
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	je	.LBB14_67
# BB#66:
	leaq	544(%rsp), %rdi
	movl	$8192, %esi             # imm = 0x2000
	movl	$.L.str.172, %edx
	jmp	.LBB14_68
.LBB14_67:
	leaq	544(%rsp), %rdi
	movl	$8192, %esi             # imm = 0x2000
	movl	$.L.str.174, %edx
.LBB14_68:
	movl	$.L.str.173, %r8d
	xorl	%eax, %eax
	movq	%r15, %rcx
	callq	snprintf
	leaq	544(%rsp), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movslq	%eax, %rdx
	xorl	%ecx, %ecx
	movl	%r14d, %edi
	movq	%rbx, %rsi
	callq	send
	testq	%rax, %rax
	js	.LBB14_100
# BB#69:
	movq	%r12, (%rsp)            # 8-byte Spill
	movl	$1, %ebx
	movl	$1, %esi
	movl	%r14d, %edi
	callq	shutdown
	movl	%r14d, %eax
	sarl	$31, %eax
	shrl	$26, %eax
	addl	%r14d, %eax
	movl	%eax, %edx
	andl	$192, %edx
	movl	%r14d, %ecx
	subl	%edx, %ecx
	movl	$1, %r15d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %r15
	sarl	$6, %eax
	movq	%r14, %rcx
	movslq	%eax, %r14
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	%ecx, %eax
	incl	%eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	$30, %eax
	movd	%rax, %xmm0
	movdqa	%xmm0, 112(%rsp)        # 16-byte Spill
	leaq	544(%rsp), %rbp
	movq	%r13, %r12
	jmp	.LBB14_71
.LBB14_70:                              #   in Loop: Header=BB14_71 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB14_71
.LBB14_87:                              #   in Loop: Header=BB14_71 Depth=1
	movq	%rbp, %rax
	testl	%r13d, %r13d
	jne	.LBB14_86
	jmp	.LBB14_70
.LBB14_71:                              # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movl	$16, %ecx
	movq	%r12, %rdi
	#APP
	cld
	rep
	stosq	%rax, %es:(%rdi)
	#NO_APP
	orq	%r15, 144(%rsp,%r14,8)
	movdqa	112(%rsp), %xmm0        # 16-byte Reload
	movdqa	%xmm0, 48(%rsp)
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	24(%rsp), %edi          # 4-byte Reload
	movq	%r12, %rsi
	leaq	48(%rsp), %r8
	callq	select
	testl	%eax, %eax
	js	.LBB14_81
# BB#72:                                #   in Loop: Header=BB14_71 Depth=1
	testq	144(%rsp,%r14,8), %r15
	je	.LBB14_103
# BB#73:                                #   in Loop: Header=BB14_71 Depth=1
	movl	$8192, %edx             # imm = 0x2000
	xorl	%ecx, %ecx
	movq	8(%rsp), %rdi           # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%rbp, %rsi
	callq	recv
	movq	%rax, %r13
	testl	%r13d, %r13d
	js	.LBB14_103
# BB#74:                                #   in Loop: Header=BB14_71 Depth=1
	je	.LBB14_103
# BB#75:                                #   in Loop: Header=BB14_71 Depth=1
	testl	%ebx, %ebx
	je	.LBB14_82
# BB#76:                                #   in Loop: Header=BB14_71 Depth=1
	movslq	%r13d, %rax
	movb	$0, 544(%rsp,%rax)
	movl	$1, %esi
	movl	$.L.str.68, %edx
	movq	%rbp, %rdi
	callq	cli_strtok
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB14_79
# BB#77:                                #   in Loop: Header=BB14_71 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, %rbp
	movl	$.L.str.175, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	callq	free
	addl	$-301, %ebp             # imm = 0xFED3
	cmpl	$1, %ebp
	leaq	544(%rsp), %rbp
	ja	.LBB14_79
# BB#78:                                #   in Loop: Header=BB14_71 Depth=1
	movl	$.L.str.176, %esi
	movq	%rbp, %rdi
	callq	strstr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB14_105
.LBB14_79:                              # %.thread16
                                        #   in Loop: Header=BB14_71 Depth=1
	movl	$.L.str.179, %esi
	movq	%rbp, %rdi
	callq	strstr
	testq	%rax, %rax
	je	.LBB14_83
# BB#80:                                #   in Loop: Header=BB14_71 Depth=1
	addq	$4, %rax
	jmp	.LBB14_85
.LBB14_81:                              #   in Loop: Header=BB14_71 Depth=1
	callq	__errno_location
	cmpl	$4, (%rax)
	je	.LBB14_71
	jmp	.LBB14_104
.LBB14_82:                              #   in Loop: Header=BB14_71 Depth=1
	movq	%rbp, %rax
	jmp	.LBB14_86
.LBB14_83:                              #   in Loop: Header=BB14_71 Depth=1
	movl	$.L.str.180, %esi
	movq	%rbp, %rdi
	callq	strstr
	testq	%rax, %rax
	je	.LBB14_87
# BB#84:                                #   in Loop: Header=BB14_71 Depth=1
	addq	$2, %rax
.LBB14_85:                              #   in Loop: Header=BB14_71 Depth=1
	movl	%eax, %ecx
	subl	%ebp, %ecx
	subl	%ecx, %r13d
	testl	%r13d, %r13d
	je	.LBB14_70
.LBB14_86:                              # %.thread20
                                        #   in Loop: Header=BB14_71 Depth=1
	movslq	%r13d, %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movq	(%rsp), %rcx            # 8-byte Reload
	callq	fwrite
	xorl	%ebx, %ebx
	cmpq	$1, %rax
	je	.LBB14_71
# BB#102:
	leaq	272(%rsp), %rdx
	movl	$.L.str.181, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	cli_warnmsg
.LBB14_103:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	fclose
	movq	8(%rsp), %rdi           # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	close
	jmp	.LBB14_22
.LBB14_88:
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movq	%r12, (%rsp)            # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.182, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	movq	%rcx, %rdx
	callq	cli_warnmsg
	jmp	.LBB14_99
.LBB14_89:                              # %.thread47.i
                                        #   in Loop: Header=BB14_90 Depth=1
	incl	%eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB14_90:                              # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	leaq	48(%rsp), %rdi
	callq	gettimeofday
	movq	48(%rsp), %rdx
	movq	56(%rsp), %rcx
	movq	%rbx, %rax
	subq	%rdx, %rax
	cmoveq	%rcx, %rdx
	movq	%rbx, %rsi
	cmoveq	%rbp, %rsi
	cmpq	%rsi, %rdx
	jg	.LBB14_98
# BB#91:                                #   in Loop: Header=BB14_90 Depth=1
	movq	%rax, 96(%rsp)
	movq	%rbp, %rdx
	subq	%rcx, %rdx
	movq	%rdx, 104(%rsp)
	jns	.LBB14_93
# BB#92:                                #   in Loop: Header=BB14_90 Depth=1
	decq	%rax
	movq	%rax, 96(%rsp)
	addq	$1000000, %rdx          # imm = 0xF4240
	movq	%rdx, 104(%rsp)
.LBB14_93:                              #   in Loop: Header=BB14_90 Depth=1
	xorl	%eax, %eax
	movl	$16, %ecx
	leaq	544(%rsp), %rdx
	movq	%rdx, %rdi
	#APP
	cld
	rep
	stosq	%rax, %es:(%rdi)
	#NO_APP
	movq	136(%rsp), %rax         # 8-byte Reload
	orq	%r14, 544(%rsp,%rax,8)
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	movl	112(%rsp), %edi         # 4-byte Reload
	leaq	96(%rsp), %r8
	callq	select
	movl	%eax, %r13d
	testl	%r13d, %r13d
	jns	.LBB14_95
# BB#94:                                #   in Loop: Header=BB14_90 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.184, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	movl	%r12d, %edx
	callq	cli_warnmsg
	testl	%r12d, %r12d
	leal	-1(%r12), %eax
	movl	%eax, %r12d
	jg	.LBB14_90
	jmp	.LBB14_99
.LBB14_95:                              #   in Loop: Header=BB14_90 Depth=1
	movl	$.L.str.185, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	movl	%r13d, %edx
	callq	cli_dbgmsg
	testl	%r13d, %r13d
	jne	.LBB14_112
# BB#96:                                #   in Loop: Header=BB14_90 Depth=1
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	$10, %eax
	jne	.LBB14_89
# BB#97:
	movl	$.L.str.186, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_warnmsg
	jmp	.LBB14_99
.LBB14_98:
	movl	$.L.str.183, %edi
	movl	$5, %edx
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_warnmsg
.LBB14_99:                              # %nonblock_connect.exit.thread
	movq	(%rsp), %r12            # 8-byte Reload
	movq	8(%rsp), %r14           # 8-byte Reload
.LBB14_100:
	movl	%r14d, %edi
	callq	close
	jmp	.LBB14_20
.LBB14_101:
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	jmp	.LBB14_43
.LBB14_104:
	movq	8(%rsp), %rdi           # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	close
	movq	(%rsp), %rdi            # 8-byte Reload
	jmp	.LBB14_21
.LBB14_105:
	leaq	272(%rsp), %rdi
	callq	unlink
	movq	72(%rsp), %rbp          # 8-byte Reload
	cmpl	$5, 24(%rbp)
	jl	.LBB14_107
# BB#106:
	movq	(%rbp), %rsi
	movl	$.L.str.177, %edi
	movl	$5, %ecx
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	cli_warnmsg
	jmp	.LBB14_103
.LBB14_107:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	fclose
	movq	8(%rsp), %rdi           # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	close
	addq	$11, %rbx
	movq	(%rbp), %rdi
	callq	free
	movq	%rbx, %rax
	jmp	.LBB14_109
.LBB14_108:                             #   in Loop: Header=BB14_109 Depth=1
	incq	%rax
.LBB14_109:                             # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	je	.LBB14_111
# BB#110:                               #   in Loop: Header=BB14_109 Depth=1
	cmpb	$10, %cl
	jne	.LBB14_108
.LBB14_111:
	movb	$0, (%rax)
	movq	%rbx, %rdi
	callq	cli_strdup
	movq	%rax, %rcx
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	%rcx, (%rbx)
	incl	24(%rbx)
	movl	$.L.str.178, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	callq	getURL
	jmp	.LBB14_22
.LBB14_112:
	movl	$4, 40(%rsp)
	leaq	44(%rsp), %rcx
	leaq	40(%rsp), %r8
	movl	$1, %esi
	movl	$4, %edx
	movq	8(%rsp), %r14           # 8-byte Reload
	movl	%r14d, %edi
	callq	getsockopt
	movl	44(%rsp), %edi
	testl	%edi, %edi
	je	.LBB14_114
# BB#113:
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.187, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	movq	%rcx, %rdx
	callq	cli_warnmsg
	movl	$-1, %ebx
	jmp	.LBB14_115
.LBB14_114:
	xorl	%ebx, %ebx
.LBB14_115:                             # %.thread46.i
	movq	(%rsp), %r12            # 8-byte Reload
	leaq	144(%rsp), %rbp
	movq	%rbp, %r13
	testl	%ebx, %ebx
	jns	.LBB14_62
	jmp	.LBB14_100
.Lfunc_end14:
	.size	getURL, .Lfunc_end14-getURL
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"cli_mbox called with NULL dir\n"
	.size	.L.str, 31

	.type	cli_parse_mbox.rfc821,@object # @cli_parse_mbox.rfc821
	.local	cli_parse_mbox.rfc821
	.comm	cli_parse_mbox.rfc821,8,8
	.type	cli_parse_mbox.subtype,@object # @cli_parse_mbox.subtype
	.local	cli_parse_mbox.subtype
	.comm	cli_parse_mbox.subtype,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"in mbox()\n"
	.size	.L.str.1, 11

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"rb"
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Can't open descriptor %d\n"
	.size	.L.str.3, 26

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"From "
	.size	.L.str.4, 6

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Deal with message number %d\n"
	.size	.L.str.5, 29

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Message number %d is infected\n"
	.size	.L.str.6, 31

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Finished processing message\n"
	.size	.L.str.7, 29

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Extract attachments from email %d\n"
	.size	.L.str.8, 35

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"P I "
	.size	.L.str.9, 5

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Phishing.Heuristics.Email"
	.size	.L.str.11, 26

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"cli_mbox returning %d\n"
	.size	.L.str.12, 23

	.type	mimeSubtypes,@object    # @mimeSubtypes
	.section	.rodata,"a",@progbits
	.p2align	4
mimeSubtypes:
	.quad	.L.str.16
	.long	1                       # 0x1
	.zero	4
	.quad	.L.str.17
	.long	2                       # 0x2
	.zero	4
	.quad	.L.str.18
	.long	3                       # 0x3
	.zero	4
	.quad	.L.str.19
	.long	4                       # 0x4
	.zero	4
	.quad	.L.str.20
	.long	5                       # 0x5
	.zero	4
	.quad	.L.str.21
	.long	6                       # 0x6
	.zero	4
	.quad	.L.str.22
	.long	7                       # 0x7
	.zero	4
	.quad	.L.str.23
	.long	8                       # 0x8
	.zero	4
	.quad	.L.str.24
	.long	9                       # 0x9
	.zero	4
	.quad	.L.str.25
	.long	10                      # 0xa
	.zero	4
	.quad	.L.str.26
	.long	11                      # 0xb
	.zero	4
	.quad	.L.str.27
	.long	12                      # 0xc
	.zero	4
	.quad	.L.str.28
	.long	5                       # 0x5
	.zero	4
	.quad	.L.str.29
	.long	13                      # 0xd
	.zero	4
	.quad	.L.str.30
	.long	10                      # 0xa
	.zero	4
	.quad	.L.str.31
	.long	14                      # 0xe
	.zero	4
	.quad	.L.str.32
	.long	14                      # 0xe
	.zero	4
	.quad	.L.str.33
	.long	14                      # 0xe
	.zero	4
	.quad	.L.str.34
	.long	14                      # 0xe
	.zero	4
	.zero	16
	.size	mimeSubtypes, 320

	.type	.L.str.13,@object       # @.str.13
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.13:
	.asciz	"Content-Type"
	.size	.L.str.13, 13

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Content-Transfer-Encoding"
	.size	.L.str.14, 26

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Content-Disposition"
	.size	.L.str.15, 20

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"plain"
	.size	.L.str.16, 6

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"enriched"
	.size	.L.str.17, 9

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"html"
	.size	.L.str.18, 5

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"richtext"
	.size	.L.str.19, 9

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"mixed"
	.size	.L.str.20, 6

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"alternative"
	.size	.L.str.21, 12

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"digest"
	.size	.L.str.22, 7

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"signed"
	.size	.L.str.23, 7

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"parallel"
	.size	.L.str.24, 9

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"related"
	.size	.L.str.25, 8

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"report"
	.size	.L.str.26, 7

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"appledouble"
	.size	.L.str.27, 12

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"fax-message"
	.size	.L.str.28, 12

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"encrypted"
	.size	.L.str.29, 10

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"x-bfile"
	.size	.L.str.30, 8

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"knowbot"
	.size	.L.str.31, 8

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"knowbot-metadata"
	.size	.L.str.32, 17

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"knowbot-code"
	.size	.L.str.33, 13

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"knowbot-state"
	.size	.L.str.34, 14

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"parseEmailHeaders\n"
	.size	.L.str.35, 19

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"parseEmailHeaders: check '%s'\n"
	.size	.L.str.36, 31

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.zero	1
	.size	.L.str.37, 1

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"End of header information\n"
	.size	.L.str.38, 27

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"Nothing interesting in the header\n"
	.size	.L.str.39, 35

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	":"
	.size	.L.str.40, 2

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"parseEmailHeaders: inished with headers, moving body\n"
	.size	.L.str.41, 54

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"parseEmailHeaders: Fullline unparsed '%s'\n"
	.size	.L.str.42, 43

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"parseEmailHeaders: no headers found, assuming it isn't an email\n"
	.size	.L.str.43, 65

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"parseEmailHeaders: return\n"
	.size	.L.str.44, 27

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"rfc822comments: contains a comment\n"
	.size	.L.str.45, 36

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"rfc822comments '%s'=>'%s'\n"
	.size	.L.str.46, 27

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"From"
	.size	.L.str.47, 5

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"Received"
	.size	.L.str.48, 9

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"De"
	.size	.L.str.49, 3

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"parseEmailHeader '%s'\n"
	.size	.L.str.50, 23

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	":= "
	.size	.L.str.51, 4

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"=?"
	.size	.L.str.52, 3

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"?="
	.size	.L.str.53, 3

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"rfc2047 '%s'\n"
	.size	.L.str.54, 14

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"Unsupported RFC2047 encoding type '%c' - if you believe this file contains a virus, submit it to www.clamav.net\n"
	.size	.L.str.55, 113

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"quoted-printable"
	.size	.L.str.56, 17

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"base64"
	.size	.L.str.57, 7

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"Decoded as '%*.*s'\n"
	.size	.L.str.58, 20

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"rfc2047 returns '%s'\n"
	.size	.L.str.59, 22

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"parseMimeHeader: cmd='%s', arg='%s'\n"
	.size	.L.str.60, 37

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"Invalid content-type '%s' received, no subtype specified, assuming text/plain; charset=us-ascii\n"
	.size	.L.str.62, 97

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"Content-type '/' received, assuming application/octet-stream\n"
	.size	.L.str.63, 62

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"application"
	.size	.L.str.64, 12

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"octet-stream"
	.size	.L.str.65, 13

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	";"
	.size	.L.str.66, 2

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"/"
	.size	.L.str.67, 2

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	" "
	.size	.L.str.68, 2

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"mimeArgs = '%s'\n"
	.size	.L.str.69, 17

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"filename=unknown"
	.size	.L.str.70, 17

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"newline_in_header, check \"%s\"\n"
	.size	.L.str.71, 31

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"Message-Id: "
	.size	.L.str.72, 13

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"Date: "
	.size	.L.str.73, 7

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"in parseEmailBody, %u files saved so far\n"
	.size	.L.str.74, 42

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"parseEmailBody: hit maximum recursion level (%u)\n"
	.size	.L.str.75, 50

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"MIME.RecursionLimit"
	.size	.L.str.76, 20

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"parseEmailBody: number of files exceeded %u\n"
	.size	.L.str.77, 45

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"Parsing mail file\n"
	.size	.L.str.78, 19

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"text/plain: Assume no attachements\n"
	.size	.L.str.79, 36

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"rfc822-headers"
	.size	.L.str.80, 15

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"Changing message/rfc822-headers to text/rfc822-headers\n"
	.size	.L.str.81, 56

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"mimeType = %d\n"
	.size	.L.str.82, 15

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"Not a mime encoded message\n"
	.size	.L.str.83, 28

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"Content-type 'multipart' handler\n"
	.size	.L.str.84, 34

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"boundary"
	.size	.L.str.85, 9

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"Multipart/%s MIME message contains no boundary header\n"
	.size	.L.str.86, 55

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"Multipart has no subtype assuming alternative\n"
	.size	.L.str.87, 47

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"Multipart MIME message has no body\n"
	.size	.L.str.88, 36

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"Found MIME attachment before the first MIME section \"%s\"\n"
	.size	.L.str.89, 58

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"Multipart MIME message contains no boundary lines (%s)\n"
	.size	.L.str.90, 56

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"Now read in part %d\n"
	.size	.L.str.91, 21

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"Empty part\n"
	.size	.L.str.92, 12

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"Ignoring fake end of headers\n"
	.size	.L.str.94, 30

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"Content"
	.size	.L.str.95, 8

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"filename="
	.size	.L.str.96, 10

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"Multipart %d: End of header information\n"
	.size	.L.str.97, 41

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"Part %d starts with a continuation line\n"
	.size	.L.str.98, 41

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"Multipart %d: headers not terminated by blank line\n"
	.size	.L.str.99, 52

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"Multipart %d: About to parse folded header '%s'\n"
	.size	.L.str.100, 49

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"Part %d has %d lines, rc = %d\n"
	.size	.L.str.101, 31

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"multipart/knowbot parsed as multipart/mixed for now\n"
	.size	.L.str.102, 53

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"Unsupported multipart format `%s', parsed as mixed\n"
	.size	.L.str.103, 52

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"The message has %d parts\n"
	.size	.L.str.104, 26

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"Find out the multipart type (%s)\n"
	.size	.L.str.105, 34

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"Multipart related handler\n"
	.size	.L.str.106, 27

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"No HTML code found to be scanned\n"
	.size	.L.str.107, 34

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"Multipart alternative handler\n"
	.size	.L.str.108, 31

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"Mixed message with %d parts\n"
	.size	.L.str.109, 29

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"protocol"
	.size	.L.str.110, 9

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"application/pgp-encrypted"
	.size	.L.str.111, 26

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"PGP encoded attachment not scanned\n"
	.size	.L.str.112, 36

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"Unknown encryption protocol '%s' - if you believe this file contains a virus, submit it to www.clamav.net\n"
	.size	.L.str.113, 107

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"Encryption method missing protocol name\n"
	.size	.L.str.114, 41

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"Save non mime and/or text/plain part\n"
	.size	.L.str.115, 38

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"textpart"
	.size	.L.str.116, 9

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"MIME type 'message' cannot be decoded\n"
	.size	.L.str.117, 39

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"rfc822"
	.size	.L.str.118, 7

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"delivery-status"
	.size	.L.str.119, 16

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"Decode rfc822\n"
	.size	.L.str.120, 15

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"disposition-notification"
	.size	.L.str.121, 25

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"partial"
	.size	.L.str.122, 8

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"external-body"
	.size	.L.str.123, 14

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"Attempt to send Content-type message/external-body trapped"
	.size	.L.str.124, 59

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"Unsupported message format `%s' - if you believe this file contains a virus, submit it to www.clamav.net\n"
	.size	.L.str.125, 106

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"Message received with unknown mime encoding - assume application"
	.size	.L.str.126, 65

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"Saving main message as attachment\n"
	.size	.L.str.127, 35

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"Content-Type:"
	.size	.L.str.129, 14

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"text/plain"
	.size	.L.str.130, 11

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"text/html"
	.size	.L.str.131, 10

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"Non mime part bounce message is not mime encoded, so it will not be scanned\n"
	.size	.L.str.132, 77

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"multipart/"
	.size	.L.str.133, 11

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"message/rfc822"
	.size	.L.str.134, 15

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"cli_mbox: I believe it's plain text which must be clean\n"
	.size	.L.str.135, 57

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	"Save non mime part bounce message\n"
	.size	.L.str.136, 35

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"bounce"
	.size	.L.str.137, 7

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"Received: by clamd (bounce)\n"
	.size	.L.str.138, 29

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"\n"
	.size	.L.str.139, 2

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"Found the start of another bounce candidate (%s)\n"
	.size	.L.str.140, 50

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"Found a bounce message with no header at '%s'\n"
	.size	.L.str.141, 47

	.type	.L.str.142,@object      # @.str.142
.L.str.142:
	.asciz	"Saving text part to scan, rc = %d\n"
	.size	.L.str.142, 35

	.type	.L.str.143,@object      # @.str.143
.L.str.143:
	.asciz	"parseEmailBody() returning %d\n"
	.size	.L.str.143, 31

	.type	.L.str.144,@object      # @.str.144
.L.str.144:
	.asciz	"PH:Phishing found\n"
	.size	.L.str.144, 19

	.type	.L.str.145,@object      # @.str.145
.L.str.145:
	.asciz	"Viruses pointed to by URLs not scanned in large message\n"
	.size	.L.str.145, 57

	.type	.L.str.146,@object      # @.str.146
.L.str.146:
	.asciz	"getHrefs: calling html_normalise_mem\n"
	.size	.L.str.146, 38

	.type	.L.str.147,@object      # @.str.147
.L.str.147:
	.asciz	"getHrefs: html_normalise_mem returned\n"
	.size	.L.str.147, 39

	.type	.L.str.148,@object      # @.str.148
.L.str.148:
	.asciz	"http://"
	.size	.L.str.148, 8

	.type	.L.str.149,@object      # @.str.149
.L.str.149:
	.asciz	".exe"
	.size	.L.str.149, 5

	.type	.L.str.150,@object      # @.str.150
.L.str.150:
	.asciz	"swap %s %s\n"
	.size	.L.str.150, 12

	.type	.L.str.151,@object      # @.str.151
.L.str.151:
	.asciz	"URL %s already downloaded\n"
	.size	.L.str.151, 27

	.type	.L.str.152,@object      # @.str.152
.L.str.152:
	.asciz	"Possible URL spoofing attempt noticed, but not yet handled (%s)\n"
	.size	.L.str.152, 65

	.type	.L.str.153,@object      # @.str.153
.L.str.153:
	.asciz	"URL %s will not be scanned (FOLLOWURLS limit %d was reached)\n"
	.size	.L.str.153, 62

	.type	.L.str.154,@object      # @.str.154
.L.str.154:
	.asciz	"Downloading URL %s to be scanned\n"
	.size	.L.str.154, 34

	.type	getURL.default_port,@object # @getURL.default_port
	.local	getURL.default_port
	.comm	getURL.default_port,2,2
	.type	getURL.tcp,@object      # @getURL.tcp
	.local	getURL.tcp
	.comm	getURL.tcp,4,4
	.type	.L.str.155,@object      # @.str.155
.L.str.155:
	.asciz	"Ignoring long URL \"%s\"\n"
	.size	.L.str.155, 24

	.type	.L.str.156,@object      # @.str.156
.L.str.156:
	.asciz	"%s/%s"
	.size	.L.str.156, 6

	.type	.L.str.157,@object      # @.str.157
.L.str.157:
	.asciz	"wb"
	.size	.L.str.157, 3

	.type	.L.str.158,@object      # @.str.158
.L.str.158:
	.asciz	"Can't open '%s' for writing\n"
	.size	.L.str.158, 29

	.type	.L.str.159,@object      # @.str.159
.L.str.159:
	.asciz	"Saving %s to %s\n"
	.size	.L.str.159, 17

	.type	.L.str.160,@object      # @.str.160
.L.str.160:
	.asciz	"tcp"
	.size	.L.str.160, 4

	.type	.L.str.161,@object      # @.str.161
.L.str.161:
	.asciz	"Unknown prototol tcp, check /etc/protocols\n"
	.size	.L.str.161, 44

	.type	.L.str.162,@object      # @.str.162
.L.str.162:
	.asciz	"http"
	.size	.L.str.162, 5

	.type	.L.str.163,@object      # @.str.163
.L.str.163:
	.asciz	"http_proxy"
	.size	.L.str.163, 11

	.type	.L.str.164,@object      # @.str.164
.L.str.164:
	.asciz	"Unsupported proxy protocol (proxy = %s)\n"
	.size	.L.str.164, 41

	.type	.L.str.165,@object      # @.str.165
.L.str.165:
	.asciz	"Getting %s via %s\n"
	.size	.L.str.165, 19

	.type	.L.str.166,@object      # @.str.166
.L.str.166:
	.asciz	"Getting %s\n"
	.size	.L.str.166, 12

	.type	.L.str.167,@object      # @.str.167
.L.str.167:
	.asciz	"Unsupported protocol\n"
	.size	.L.str.167, 22

	.type	.L.str.168,@object      # @.str.168
.L.str.168:
	.asciz	"Unknown host %s\n"
	.size	.L.str.168, 17

	.type	.L.str.169,@object      # @.str.169
.L.str.169:
	.asciz	"getfl: %s\n"
	.size	.L.str.169, 11

	.type	.L.str.170,@object      # @.str.170
.L.str.170:
	.asciz	"setfl: %s\n"
	.size	.L.str.170, 11

	.type	.L.str.171,@object      # @.str.171
.L.str.171:
	.asciz	"f_setfl: %s\n"
	.size	.L.str.171, 13

	.type	.L.str.172,@object      # @.str.172
.L.str.172:
	.asciz	"GET %s HTTP/1.0\r\nUser-Agent: ClamAV %s\r\n\r\n"
	.size	.L.str.172, 43

	.type	.L.str.173,@object      # @.str.173
.L.str.173:
	.asciz	"devel-20071218"
	.size	.L.str.173, 15

	.type	.L.str.174,@object      # @.str.174
.L.str.174:
	.asciz	"GET /%s HTTP/1.0\r\nUser-Agent: ClamAV %s\r\n\r\n"
	.size	.L.str.174, 44

	.type	.L.str.175,@object      # @.str.175
.L.str.175:
	.asciz	"HTTP status %d\n"
	.size	.L.str.175, 16

	.type	.L.str.176,@object      # @.str.176
.L.str.176:
	.asciz	"\nLocation: "
	.size	.L.str.176, 12

	.type	.L.str.177,@object      # @.str.177
.L.str.177:
	.asciz	"URL %s will not be followed to %s (FOLLOWURLS limit %d was reached)\n"
	.size	.L.str.177, 69

	.type	.L.str.178,@object      # @.str.178
.L.str.178:
	.asciz	"Redirecting to %s\n"
	.size	.L.str.178, 19

	.type	.L.str.179,@object      # @.str.179
.L.str.179:
	.asciz	"\r\n\r\n"
	.size	.L.str.179, 5

	.type	.L.str.180,@object      # @.str.180
.L.str.180:
	.asciz	"\n\n"
	.size	.L.str.180, 3

	.type	.L.str.181,@object      # @.str.181
.L.str.181:
	.asciz	"Error writing %d bytes to %s\n"
	.size	.L.str.181, 30

	.type	.L.str.182,@object      # @.str.182
.L.str.182:
	.asciz	"%s: connect: %s\n"
	.size	.L.str.182, 17

	.type	.L.str.183,@object      # @.str.183
.L.str.183:
	.asciz	"%s: connect timeout (%d secs)\n"
	.size	.L.str.183, 31

	.type	.L.str.184,@object      # @.str.184
.L.str.184:
	.asciz	"%s: select attempt %d %s\n"
	.size	.L.str.184, 26

	.type	.L.str.185,@object      # @.str.185
.L.str.185:
	.asciz	"%s: select = %d\n"
	.size	.L.str.185, 17

	.type	.L.str.186,@object      # @.str.186
.L.str.186:
	.asciz	"timeout connecting to %s\n"
	.size	.L.str.186, 26

	.type	.L.str.187,@object      # @.str.187
.L.str.187:
	.asciz	"%s: %s\n"
	.size	.L.str.187, 8

	.type	.L.str.188,@object      # @.str.188
.L.str.188:
	.asciz	"boundaryStart: found %s in %s\n"
	.size	.L.str.188, 31

	.type	.L.str.189,@object      # @.str.189
.L.str.189:
	.asciz	"x-binhex"
	.size	.L.str.189, 9

	.type	.L.str.190,@object      # @.str.190
.L.str.190:
	.asciz	"Binhex file decoded to %s\n"
	.size	.L.str.190, 27

	.type	.L.str.191,@object      # @.str.191
.L.str.191:
	.asciz	"Couldn't decode binhex file to %s\n"
	.size	.L.str.191, 35

	.type	.L.str.192,@object      # @.str.192
.L.str.192:
	.asciz	"boundaryEnd: found %s in %s\n"
	.size	.L.str.192, 29

	.type	.L.str.193,@object      # @.str.193
.L.str.193:
	.asciz	"Mixed message part %d is of type %d\n"
	.size	.L.str.193, 37

	.type	.L.str.194,@object      # @.str.194
.L.str.194:
	.asciz	"No mime headers found in multipart part %d\n"
	.size	.L.str.194, 44

	.type	.L.str.195,@object      # @.str.195
.L.str.195:
	.asciz	"Found binhex message in multipart/mixed mainMessage\n"
	.size	.L.str.195, 53

	.type	.L.str.196,@object      # @.str.196
.L.str.196:
	.asciz	"Found binhex message in multipart/mixed non mime part\n"
	.size	.L.str.196, 55

	.type	.L.str.197,@object      # @.str.197
.L.str.197:
	.asciz	"No plain text alternative\n"
	.size	.L.str.197, 27

	.type	.L.str.198,@object      # @.str.198
.L.str.198:
	.asciz	"Mixed message text part disposition \"%s\"\n"
	.size	.L.str.198, 42

	.type	.L.str.199,@object      # @.str.199
.L.str.199:
	.asciz	"attachment"
	.size	.L.str.199, 11

	.type	.L.str.200,@object      # @.str.200
.L.str.200:
	.asciz	"inline"
	.size	.L.str.200, 7

	.type	.L.str.201,@object      # @.str.201
.L.str.201:
	.asciz	"Mime subtype \"%s\"\n"
	.size	.L.str.201, 19

	.type	.L.str.202,@object      # @.str.202
.L.str.202:
	.asciz	"Adding part to main message\n"
	.size	.L.str.202, 29

	.type	.L.str.203,@object      # @.str.203
.L.str.203:
	.asciz	"Treating inline as attachment\n"
	.size	.L.str.203, 31

	.type	.L.str.204,@object      # @.str.204
.L.str.204:
	.asciz	"filename=mixedtextportion"
	.size	.L.str.204, 26

	.type	.L.str.205,@object      # @.str.205
.L.str.205:
	.asciz	"Text type %s is not supported\n"
	.size	.L.str.205, 31

	.type	.L.str.206,@object      # @.str.206
.L.str.206:
	.asciz	"Found message inside multipart (encoding type %d)\n"
	.size	.L.str.206, 51

	.type	.L.str.207,@object      # @.str.207
.L.str.207:
	.asciz	"Unencoded multipart/message will not be scanned\n"
	.size	.L.str.207, 49

	.type	.L.str.208,@object      # @.str.208
.L.str.208:
	.asciz	"Encoded multipart/message will be scanned\n"
	.size	.L.str.208, 43

	.type	.L.str.209,@object      # @.str.209
.L.str.209:
	.asciz	"Found multipart inside multipart\n"
	.size	.L.str.209, 34

	.type	.L.str.210,@object      # @.str.210
.L.str.210:
	.asciz	"Finished recursion, rc = %d\n"
	.size	.L.str.210, 29

	.type	.L.str.211,@object      # @.str.211
.L.str.211:
	.asciz	"Only text and application attachments are fully supported, type = %d\n"
	.size	.L.str.211, 70

	.type	.L.str.212,@object      # @.str.212
.L.str.212:
	.asciz	"Adding to non mime-part\n"
	.size	.L.str.212, 25

	.type	.L.str.213,@object      # @.str.213
.L.str.213:
	.asciz	"id"
	.size	.L.str.213, 3

	.type	.L.str.214,@object      # @.str.214
.L.str.214:
	.asciz	"TMPDIR"
	.size	.L.str.214, 7

	.type	.L.str.215,@object      # @.str.215
.L.str.215:
	.asciz	"TMP"
	.size	.L.str.215, 4

	.type	.L.str.216,@object      # @.str.216
.L.str.216:
	.asciz	"TEMP"
	.size	.L.str.216, 5

	.type	.L.str.217,@object      # @.str.217
.L.str.217:
	.asciz	"/tmp"
	.size	.L.str.217, 5

	.type	.L.str.218,@object      # @.str.218
.L.str.218:
	.asciz	"%s/clamav-partial"
	.size	.L.str.218, 18

	.type	.L.str.219,@object      # @.str.219
.L.str.219:
	.asciz	"Can't create the directory '%s'\n"
	.size	.L.str.219, 33

	.type	.L.str.220,@object      # @.str.220
.L.str.220:
	.asciz	"Partial directory %s: %s\n"
	.size	.L.str.220, 26

	.type	.L.str.221,@object      # @.str.221
.L.str.221:
	.asciz	"Insecure partial directory %s (mode 0%o)\n"
	.size	.L.str.221, 42

	.type	.L.str.222,@object      # @.str.222
.L.str.222:
	.asciz	"number"
	.size	.L.str.222, 7

	.type	.L.str.223,@object      # @.str.223
.L.str.223:
	.asciz	"filename=%s%s"
	.size	.L.str.223, 14

	.type	.L.str.224,@object      # @.str.224
.L.str.224:
	.asciz	"Must reset to %s\n"
	.size	.L.str.224, 18

	.type	.L.str.225,@object      # @.str.225
.L.str.225:
	.asciz	"total"
	.size	.L.str.225, 6

	.type	.L.str.226,@object      # @.str.226
.L.str.226:
	.asciz	"rfc1341: %s, %s of %s\n"
	.size	.L.str.226, 23

	.type	.L.str.227,@object      # @.str.227
.L.str.227:
	.asciz	"?"
	.size	.L.str.227, 2

	.type	.L.str.228,@object      # @.str.228
.L.str.228:
	.asciz	"outname: %s\n"
	.size	.L.str.228, 13

	.type	.L.str.229,@object      # @.str.229
.L.str.229:
	.asciz	"Can't open '%s' for writing"
	.size	.L.str.229, 28

	.type	.L.str.230,@object      # @.str.230
.L.str.230:
	.asciz	"%s%d"
	.size	.L.str.230, 5

	.type	.L.str.231,@object      # @.str.231
.L.str.231:
	.asciz	"removed old RFC1341 file %s\n"
	.size	.L.str.231, 29

	.type	.L.str.232,@object      # @.str.232
.L.str.232:
	.asciz	"Can't open '%s' for reading"
	.size	.L.str.232, 28

	.type	.L.str.233,@object      # @.str.233
.L.str.233:
	.asciz	">From "
	.size	.L.str.233, 7

	.type	.L.str.234,@object      # @.str.234
.L.str.234:
	.asciz	"7bit"
	.size	.L.str.234, 5

	.type	.L.str.235,@object      # @.str.235
.L.str.235:
	.asciz	"8bit"
	.size	.L.str.235, 5

	.type	.L.str.236,@object      # @.str.236
.L.str.236:
	.asciz	"Found a bounce message\n"
	.size	.L.str.236, 24

	.type	.L.str.237,@object      # @.str.237
.L.str.237:
	.asciz	"Nothing new to save in the bounce message\n"
	.size	.L.str.237, 43

	.type	.L.str.238,@object      # @.str.238
.L.str.238:
	.asciz	"Not found a bounce message\n"
	.size	.L.str.238, 28

	.type	.L.str.239,@object      # @.str.239
.L.str.239:
	.asciz	"filename=textportion"
	.size	.L.str.239, 21

	.type	.L.str.240,@object      # @.str.240
.L.str.240:
	.asciz	"Saving main message\n"
	.size	.L.str.240, 21

	.type	.L.str.241,@object      # @.str.241
.L.str.241:
	.asciz	"Invalid call to getline_from_mbox(). Refer to http://www.clamav.net/bugs\n"
	.size	.L.str.241, 74

	.type	.L.str.242,@object      # @.str.242
.L.str.242:
	.asciz	"getline_from_mbox: buffer overflow stopped, line lost\n"
	.size	.L.str.242, 55

	.type	.L.str.243,@object      # @.str.243
.L.str.243:
	.asciz	"getline_from_mbox: buffer overflow stopped, line recovered\n"
	.size	.L.str.243, 60

	.type	.L.str.244,@object      # @.str.244
.L.str.244:
	.asciz	"parseEmailFile\n"
	.size	.L.str.244, 16

	.type	.L.str.245,@object      # @.str.245
.L.str.245:
	.asciz	"Found a header line with space that should be blank\n"
	.size	.L.str.245, 53

	.type	.L.str.246,@object      # @.str.246
.L.str.246:
	.asciz	"parseEmailFile: check '%s' fullline %p\n"
	.size	.L.str.246, 40

	.type	.L.str.247,@object      # @.str.247
.L.str.247:
	.asciz	"Ignoring consecutive blank lines in the body\n"
	.size	.L.str.247, 46

	.type	.L.str.248,@object      # @.str.248
.L.str.248:
	.asciz	"parseEmailFile: Fullline unparsed '%s'\n"
	.size	.L.str.248, 40

	.type	.L.str.249,@object      # @.str.249
.L.str.249:
	.asciz	"parseEmailFile: no headers found, assuming it isn't an email\n"
	.size	.L.str.249, 62

	.type	.L.str.250,@object      # @.str.250
.L.str.250:
	.asciz	"parseEmailFile: return\n"
	.size	.L.str.250, 24

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.long	4294967172              # 0xffffff84
	.long	4294967172              # 0xffffff84
	.long	4294967172              # 0xffffff84
	.long	1                       # 0x1
	.long	4294967196              # 0xffffff9c
	.long	4294967194              # 0xffffff9a
	.size	.Lswitch.table, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
