	.text
	.file	"HYPRE_struct_smg.bc"
	.globl	HYPRE_StructSMGCreate
	.p2align	4, 0x90
	.type	HYPRE_StructSMGCreate,@function
HYPRE_StructSMGCreate:                  # @HYPRE_StructSMGCreate
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	callq	hypre_SMGCreate
	movq	%rax, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	HYPRE_StructSMGCreate, .Lfunc_end0-HYPRE_StructSMGCreate
	.cfi_endproc

	.globl	HYPRE_StructSMGDestroy
	.p2align	4, 0x90
	.type	HYPRE_StructSMGDestroy,@function
HYPRE_StructSMGDestroy:                 # @HYPRE_StructSMGDestroy
	.cfi_startproc
# BB#0:
	jmp	hypre_SMGDestroy        # TAILCALL
.Lfunc_end1:
	.size	HYPRE_StructSMGDestroy, .Lfunc_end1-HYPRE_StructSMGDestroy
	.cfi_endproc

	.globl	HYPRE_StructSMGSetup
	.p2align	4, 0x90
	.type	HYPRE_StructSMGSetup,@function
HYPRE_StructSMGSetup:                   # @HYPRE_StructSMGSetup
	.cfi_startproc
# BB#0:
	jmp	hypre_SMGSetup          # TAILCALL
.Lfunc_end2:
	.size	HYPRE_StructSMGSetup, .Lfunc_end2-HYPRE_StructSMGSetup
	.cfi_endproc

	.globl	HYPRE_StructSMGSolve
	.p2align	4, 0x90
	.type	HYPRE_StructSMGSolve,@function
HYPRE_StructSMGSolve:                   # @HYPRE_StructSMGSolve
	.cfi_startproc
# BB#0:
	jmp	hypre_SMGSolve          # TAILCALL
.Lfunc_end3:
	.size	HYPRE_StructSMGSolve, .Lfunc_end3-HYPRE_StructSMGSolve
	.cfi_endproc

	.globl	HYPRE_StructSMGSetMemoryUse
	.p2align	4, 0x90
	.type	HYPRE_StructSMGSetMemoryUse,@function
HYPRE_StructSMGSetMemoryUse:            # @HYPRE_StructSMGSetMemoryUse
	.cfi_startproc
# BB#0:
	jmp	hypre_SMGSetMemoryUse   # TAILCALL
.Lfunc_end4:
	.size	HYPRE_StructSMGSetMemoryUse, .Lfunc_end4-HYPRE_StructSMGSetMemoryUse
	.cfi_endproc

	.globl	HYPRE_StructSMGSetTol
	.p2align	4, 0x90
	.type	HYPRE_StructSMGSetTol,@function
HYPRE_StructSMGSetTol:                  # @HYPRE_StructSMGSetTol
	.cfi_startproc
# BB#0:
	jmp	hypre_SMGSetTol         # TAILCALL
.Lfunc_end5:
	.size	HYPRE_StructSMGSetTol, .Lfunc_end5-HYPRE_StructSMGSetTol
	.cfi_endproc

	.globl	HYPRE_StructSMGSetMaxIter
	.p2align	4, 0x90
	.type	HYPRE_StructSMGSetMaxIter,@function
HYPRE_StructSMGSetMaxIter:              # @HYPRE_StructSMGSetMaxIter
	.cfi_startproc
# BB#0:
	jmp	hypre_SMGSetMaxIter     # TAILCALL
.Lfunc_end6:
	.size	HYPRE_StructSMGSetMaxIter, .Lfunc_end6-HYPRE_StructSMGSetMaxIter
	.cfi_endproc

	.globl	HYPRE_StructSMGSetRelChange
	.p2align	4, 0x90
	.type	HYPRE_StructSMGSetRelChange,@function
HYPRE_StructSMGSetRelChange:            # @HYPRE_StructSMGSetRelChange
	.cfi_startproc
# BB#0:
	jmp	hypre_SMGSetRelChange   # TAILCALL
.Lfunc_end7:
	.size	HYPRE_StructSMGSetRelChange, .Lfunc_end7-HYPRE_StructSMGSetRelChange
	.cfi_endproc

	.globl	HYPRE_StructSMGSetZeroGuess
	.p2align	4, 0x90
	.type	HYPRE_StructSMGSetZeroGuess,@function
HYPRE_StructSMGSetZeroGuess:            # @HYPRE_StructSMGSetZeroGuess
	.cfi_startproc
# BB#0:
	movl	$1, %esi
	jmp	hypre_SMGSetZeroGuess   # TAILCALL
.Lfunc_end8:
	.size	HYPRE_StructSMGSetZeroGuess, .Lfunc_end8-HYPRE_StructSMGSetZeroGuess
	.cfi_endproc

	.globl	HYPRE_StructSMGSetNonZeroGuess
	.p2align	4, 0x90
	.type	HYPRE_StructSMGSetNonZeroGuess,@function
HYPRE_StructSMGSetNonZeroGuess:         # @HYPRE_StructSMGSetNonZeroGuess
	.cfi_startproc
# BB#0:
	xorl	%esi, %esi
	jmp	hypre_SMGSetZeroGuess   # TAILCALL
.Lfunc_end9:
	.size	HYPRE_StructSMGSetNonZeroGuess, .Lfunc_end9-HYPRE_StructSMGSetNonZeroGuess
	.cfi_endproc

	.globl	HYPRE_StructSMGSetNumPreRelax
	.p2align	4, 0x90
	.type	HYPRE_StructSMGSetNumPreRelax,@function
HYPRE_StructSMGSetNumPreRelax:          # @HYPRE_StructSMGSetNumPreRelax
	.cfi_startproc
# BB#0:
	jmp	hypre_SMGSetNumPreRelax # TAILCALL
.Lfunc_end10:
	.size	HYPRE_StructSMGSetNumPreRelax, .Lfunc_end10-HYPRE_StructSMGSetNumPreRelax
	.cfi_endproc

	.globl	HYPRE_StructSMGSetNumPostRelax
	.p2align	4, 0x90
	.type	HYPRE_StructSMGSetNumPostRelax,@function
HYPRE_StructSMGSetNumPostRelax:         # @HYPRE_StructSMGSetNumPostRelax
	.cfi_startproc
# BB#0:
	jmp	hypre_SMGSetNumPostRelax # TAILCALL
.Lfunc_end11:
	.size	HYPRE_StructSMGSetNumPostRelax, .Lfunc_end11-HYPRE_StructSMGSetNumPostRelax
	.cfi_endproc

	.globl	HYPRE_StructSMGSetLogging
	.p2align	4, 0x90
	.type	HYPRE_StructSMGSetLogging,@function
HYPRE_StructSMGSetLogging:              # @HYPRE_StructSMGSetLogging
	.cfi_startproc
# BB#0:
	jmp	hypre_SMGSetLogging     # TAILCALL
.Lfunc_end12:
	.size	HYPRE_StructSMGSetLogging, .Lfunc_end12-HYPRE_StructSMGSetLogging
	.cfi_endproc

	.globl	HYPRE_StructSMGGetNumIterations
	.p2align	4, 0x90
	.type	HYPRE_StructSMGGetNumIterations,@function
HYPRE_StructSMGGetNumIterations:        # @HYPRE_StructSMGGetNumIterations
	.cfi_startproc
# BB#0:
	jmp	hypre_SMGGetNumIterations # TAILCALL
.Lfunc_end13:
	.size	HYPRE_StructSMGGetNumIterations, .Lfunc_end13-HYPRE_StructSMGGetNumIterations
	.cfi_endproc

	.globl	HYPRE_StructSMGGetFinalRelativeResidualNorm
	.p2align	4, 0x90
	.type	HYPRE_StructSMGGetFinalRelativeResidualNorm,@function
HYPRE_StructSMGGetFinalRelativeResidualNorm: # @HYPRE_StructSMGGetFinalRelativeResidualNorm
	.cfi_startproc
# BB#0:
	jmp	hypre_SMGGetFinalRelativeResidualNorm # TAILCALL
.Lfunc_end14:
	.size	HYPRE_StructSMGGetFinalRelativeResidualNorm, .Lfunc_end14-HYPRE_StructSMGGetFinalRelativeResidualNorm
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
