	.text
	.file	"occur.bc"
	.globl	_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	.p2align	4, 0x90
	.type	_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE,@function
_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE: # @_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%r14), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB0_2
# BB#1:
	movq	40(%r14), %r12
	jmp	.LBB0_3
.LBB0_2:
	movl	$.L.str, %edi
	movl	$63, %esi
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
                                        # implicit-def: %R12
.LBB0_3:
	movq	%rbx, 8(%r14)
	movq	8(%r12), %r15
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$172, %eax
	je	.LBB0_6
# BB#4:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$172, %eax
	jne	.LBB0_7
# BB#5:                                 # %.critedge
	movq	%rbx, 8(%r12)
	movl	24(%r14), %eax
	movl	%eax, 16(%r12)
	movq	32(%r14), %rax
	movq	%rax, 24(%r12)
.LBB0_6:                                # %.critedge207
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB0_7:
	movq	8(%r14), %rbx
	movq	8(%r12), %r15
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$184, %eax
	jne	.LBB0_10
# BB#8:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$184, %eax
	jne	.LBB0_10
# BB#9:
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$115, %eax
	je	.LBB0_6
.LBB0_10:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$184, %eax
	jne	.LBB0_13
# BB#11:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$184, %eax
	jne	.LBB0_13
# BB#12:
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$114, %eax
	je	.LBB0_6
.LBB0_13:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$184, %eax
	jne	.LBB0_16
# BB#14:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$184, %eax
	jne	.LBB0_16
# BB#15:
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$113, %eax
	je	.LBB0_6
.LBB0_16:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$184, %eax
	jne	.LBB0_27
# BB#17:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$184, %eax
	jne	.LBB0_27
# BB#18:
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$112, %eax
	jne	.LBB0_27
# BB#19:
	movq	8(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$112, %eax
	jne	.LBB0_24
# BB#20:
	movq	24(%r12), %rdi
	movq	32(%r14), %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	je	.LBB0_6
	jmp	.LBB0_21
.LBB0_27:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$184, %eax
	jne	.LBB0_30
# BB#28:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$184, %eax
	jne	.LBB0_30
# BB#29:
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$111, %eax
	je	.LBB0_6
.LBB0_30:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$185, %eax
	jne	.LBB0_34
# BB#31:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$185, %eax
	jne	.LBB0_34
# BB#32:
	movq	16(%rbx), %rax
	cmpq	16(%r15), %rax
	jne	.LBB0_6
# BB#33:
	movl	$.L.str.3, %edi
	jmp	.LBB0_22
.LBB0_34:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$183, %eax
	jne	.LBB0_37
# BB#35:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$183, %eax
	jne	.LBB0_37
# BB#36:
	movl	$.L.str.4, %edi
	jmp	.LBB0_22
.LBB0_37:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$183, %eax
	jne	.LBB0_40
# BB#38:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$182, %eax
	jne	.LBB0_40
# BB#39:
	movl	$.L.str.5, %edi
	jmp	.LBB0_22
.LBB0_40:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$182, %eax
	jne	.LBB0_43
# BB#41:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$183, %eax
	jne	.LBB0_43
# BB#42:
	movl	$.L.str.6, %edi
	jmp	.LBB0_22
.LBB0_43:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$182, %eax
	jne	.LBB0_46
# BB#44:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$182, %eax
	jne	.LBB0_46
# BB#45:
	movl	$.L.str.7, %edi
	jmp	.LBB0_22
.LBB0_46:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$180, %eax
	jne	.LBB0_49
# BB#47:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$180, %eax
	jne	.LBB0_49
# BB#48:
	movl	$.L.str.8, %edi
	jmp	.LBB0_22
.LBB0_49:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$180, %eax
	jne	.LBB0_52
# BB#50:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$179, %eax
	jne	.LBB0_52
# BB#51:
	movl	$.L.str.9, %edi
	jmp	.LBB0_22
.LBB0_52:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$179, %eax
	jne	.LBB0_55
# BB#53:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$180, %eax
	jne	.LBB0_55
# BB#54:
	movl	$.L.str.10, %edi
	jmp	.LBB0_22
.LBB0_55:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$179, %eax
	jne	.LBB0_58
# BB#56:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$179, %eax
	jne	.LBB0_58
# BB#57:
	movl	$.L.str.11, %edi
	jmp	.LBB0_22
.LBB0_24:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$111, %eax
	jne	.LBB0_25
.LBB0_21:
	movl	$.L.str.2, %edi
.LBB0_22:
	movq	%r14, %rsi
	callq	_ZN2kc12Problem1S1IDEPKcPNS_7impl_IDE
.LBB0_23:
	movq	%rax, %rbx
	movl	24(%r14), %esi
	movq	32(%r14), %rdi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN2kc8v_reportEPNS_10impl_errorE # TAILCALL
.LBB0_58:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$178, %eax
	jne	.LBB0_61
# BB#59:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$178, %eax
	jne	.LBB0_61
# BB#60:
	movl	$.L.str.12, %edi
	jmp	.LBB0_22
.LBB0_61:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$178, %eax
	jne	.LBB0_64
# BB#62:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$177, %eax
	jne	.LBB0_64
# BB#63:
	movl	$.L.str.13, %edi
	jmp	.LBB0_22
.LBB0_64:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$177, %eax
	jne	.LBB0_67
# BB#65:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$178, %eax
	jne	.LBB0_67
# BB#66:
	movl	$.L.str.14, %edi
	jmp	.LBB0_22
.LBB0_25:
	movl	$.L.str, %edi
	movl	$107, %esi
.LBB0_26:
	movl	$.L.str.1, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN2kc21kc_no_default_in_withEPKciS1_ # TAILCALL
.LBB0_67:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$177, %eax
	jne	.LBB0_70
# BB#68:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$177, %eax
	jne	.LBB0_70
# BB#69:
	movl	$.L.str.15, %edi
	jmp	.LBB0_22
.LBB0_70:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB0_73
# BB#71:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB0_73
# BB#72:
	movl	$.L.str.16, %edi
	jmp	.LBB0_22
.LBB0_73:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB0_76
# BB#74:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$175, %eax
	jne	.LBB0_76
# BB#75:
	movl	$.L.str.17, %edi
	jmp	.LBB0_22
.LBB0_76:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$175, %eax
	jne	.LBB0_79
# BB#77:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB0_79
# BB#78:
	movl	$.L.str.18, %edi
	jmp	.LBB0_22
.LBB0_79:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$175, %eax
	jne	.LBB0_82
# BB#80:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$175, %eax
	jne	.LBB0_82
# BB#81:
	movl	$.L.str.19, %edi
	jmp	.LBB0_22
.LBB0_82:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	jne	.LBB0_85
# BB#83:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	jne	.LBB0_85
# BB#84:
	movl	$.L.str.20, %edi
	jmp	.LBB0_22
.LBB0_85:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	jne	.LBB0_88
# BB#86:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	jne	.LBB0_88
# BB#87:
	movl	$.L.str.21, %edi
	jmp	.LBB0_22
.LBB0_88:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	jne	.LBB0_91
# BB#89:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	jne	.LBB0_91
# BB#90:
	movl	$.L.str.22, %edi
	jmp	.LBB0_22
.LBB0_91:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	jne	.LBB0_94
# BB#92:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	jne	.LBB0_94
# BB#93:
	movl	$.L.str.23, %edi
	jmp	.LBB0_22
.LBB0_94:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$185, %eax
	jne	.LBB0_96
# BB#95:
	movq	8(%r12), %rsi
	movl	$.L.str.24, %edi
	movl	$.L.str.25, %edx
	jmp	.LBB0_99
.LBB0_96:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$183, %eax
	jne	.LBB0_100
# BB#97:
	movq	8(%r12), %rsi
	movl	$.L.str.24, %edi
	jmp	.LBB0_98
.LBB0_100:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$182, %eax
	jne	.LBB0_102
# BB#101:
	movq	8(%r12), %rsi
	movl	$.L.str.27, %edi
.LBB0_98:
	movl	$.L.str.26, %edx
.LBB0_99:
	movq	%r14, %rcx
	callq	_ZN2kc16Problem1S1t1S1IDEPKcPNS_11impl_IDtypeES1_PNS_7impl_IDE
	jmp	.LBB0_23
.LBB0_102:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$180, %eax
	jne	.LBB0_105
# BB#103:
	movq	8(%r12), %rsi
	movl	$.L.str.24, %edi
	movl	$.L.str.28, %edx
	jmp	.LBB0_99
.LBB0_105:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$179, %eax
	jne	.LBB0_107
# BB#106:
	movq	8(%r12), %rsi
	movl	$.L.str.27, %edi
	movl	$.L.str.28, %edx
	jmp	.LBB0_99
.LBB0_107:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$184, %eax
	jne	.LBB0_109
# BB#108:
	movq	8(%r12), %rsi
	movl	$.L.str.24, %edi
	movl	$.L.str.29, %edx
	jmp	.LBB0_99
.LBB0_109:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$178, %eax
	jne	.LBB0_111
# BB#110:
	movq	8(%r12), %rsi
	movl	$.L.str.24, %edi
	movl	$.L.str.30, %edx
	jmp	.LBB0_99
.LBB0_111:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$177, %eax
	jne	.LBB0_113
# BB#112:
	movq	8(%r12), %rsi
	movl	$.L.str.27, %edi
	movl	$.L.str.31, %edx
	jmp	.LBB0_99
.LBB0_113:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB0_116
# BB#114:
	movq	8(%r12), %rsi
	movl	$.L.str.24, %edi
	movl	$.L.str.32, %edx
	jmp	.LBB0_99
.LBB0_116:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$175, %eax
	jne	.LBB0_118
# BB#117:
	movq	8(%r12), %rsi
	movl	$.L.str.27, %edi
	movl	$.L.str.32, %edx
	jmp	.LBB0_99
.LBB0_118:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	jne	.LBB0_121
# BB#119:
	movq	8(%r12), %rsi
	movl	$.L.str.24, %edi
	movl	$.L.str.33, %edx
	jmp	.LBB0_99
.LBB0_121:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	jne	.LBB0_123
# BB#122:
	movq	8(%r12), %rsi
	movl	$.L.str.27, %edi
	movl	$.L.str.33, %edx
	jmp	.LBB0_99
.LBB0_123:
	movl	$.L.str, %edi
	movl	$255, %esi
	jmp	.LBB0_26
.Lfunc_end0:
	.size	_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE, .Lfunc_end0-_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	.cfi_endproc

	.globl	_ZN2kc13v_extendoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	.p2align	4, 0x90
	.type	_ZN2kc13v_extendoccurEPNS_7impl_IDEPNS_11impl_IDtypeE,@function
_ZN2kc13v_extendoccurEPNS_7impl_IDEPNS_11impl_IDtypeE: # @_ZN2kc13v_extendoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	.cfi_startproc
# BB#0:
	movl	$1, %edx
	jmp	_ZN2kcL16do_v_extendoccurEPNS_7impl_IDEPNS_11impl_IDtypeEb # TAILCALL
.Lfunc_end1:
	.size	_ZN2kc13v_extendoccurEPNS_7impl_IDEPNS_11impl_IDtypeE, .Lfunc_end1-_ZN2kc13v_extendoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL16do_v_extendoccurEPNS_7impl_IDEPNS_11impl_IDtypeEb,@function
_ZN2kcL16do_v_extendoccurEPNS_7impl_IDEPNS_11impl_IDtypeEb: # @_ZN2kcL16do_v_extendoccurEPNS_7impl_IDEPNS_11impl_IDtypeEb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 64
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movq	%rsi, %rbp
	movq	%rdi, %r13
	movq	(%r13), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB2_2
# BB#1:
	movq	40(%r13), %r15
	jmp	.LBB2_3
.LBB2_2:
	movl	$.L.str.55, %edi
	movl	$277, %esi              # imm = 0x115
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
                                        # implicit-def: %R15
.LBB2_3:
	movq	%rbp, 8(%r13)
	movq	8(%r15), %rbx
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$172, %eax
	je	.LBB2_6
# BB#4:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$172, %eax
	jne	.LBB2_7
.LBB2_5:                                # %.critedge
	movq	%rbp, 8(%r15)
	movl	24(%r13), %eax
	movl	%eax, 16(%r15)
	movq	32(%r13), %rax
	movq	%rax, 24(%r15)
.LBB2_6:                                # %.critedge200
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_7:
	movq	8(%r13), %r14
	movq	8(%r15), %rbx
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$185, %eax
	jne	.LBB2_13
# BB#8:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$185, %eax
	jne	.LBB2_13
# BB#9:
	movq	16(%r14), %r12
	cmpq	16(%rbx), %r12
	je	.LBB2_10
# BB#12:
	movq	8(%r15), %rbx
	movq	24(%r15), %r14
	movl	16(%r15), %edi
	callq	_ZN2kc9mkintegerEi
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	%rax, %rcx
	callq	_ZN2kc17ScopeTypeFileLineEPNS_17impl_integer__IntEPNS_11impl_IDtypeEPNS_20impl_casestring__StrES1_
	movq	32(%r15), %rsi
	movq	%rax, %rdi
	callq	_ZN2kc26ConsscopetypefilelinestackEPNS_22impl_scopetypefilelineEPNS_27impl_scopetypefilelinestackE
	movq	%rax, 32(%r15)
	jmp	.LBB2_5
.LBB2_13:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$183, %eax
	jne	.LBB2_19
# BB#14:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$183, %eax
	jne	.LBB2_19
# BB#15:
	testb	%r12b, %r12b
	je	.LBB2_6
# BB#16:
	movl	$.L.str.59, %edi
	jmp	.LBB2_17
.LBB2_19:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$183, %eax
	jne	.LBB2_23
# BB#20:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$182, %eax
	jne	.LBB2_23
# BB#21:
	testb	%r12b, %r12b
	je	.LBB2_6
# BB#22:
	movl	$.L.str.60, %edi
	jmp	.LBB2_17
.LBB2_23:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$182, %eax
	jne	.LBB2_26
# BB#24:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$183, %eax
	jne	.LBB2_26
# BB#25:
	movl	$.L.str.61, %edi
	jmp	.LBB2_17
.LBB2_26:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$182, %eax
	jne	.LBB2_29
# BB#27:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$182, %eax
	jne	.LBB2_29
# BB#28:
	movl	$.L.str.62, %edi
	jmp	.LBB2_17
.LBB2_29:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$180, %eax
	jne	.LBB2_33
# BB#30:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$180, %eax
	jne	.LBB2_33
# BB#31:
	testb	%r12b, %r12b
	je	.LBB2_6
# BB#32:
	movl	$.L.str.63, %edi
	jmp	.LBB2_17
.LBB2_33:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$180, %eax
	jne	.LBB2_37
# BB#34:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$179, %eax
	jne	.LBB2_37
# BB#35:
	testb	%r12b, %r12b
	je	.LBB2_6
# BB#36:
	movl	$.L.str.64, %edi
	jmp	.LBB2_17
.LBB2_37:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$179, %eax
	jne	.LBB2_40
# BB#38:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$180, %eax
	jne	.LBB2_40
# BB#39:
	movl	$.L.str.65, %edi
	jmp	.LBB2_17
.LBB2_40:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$179, %eax
	jne	.LBB2_43
# BB#41:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$179, %eax
	jne	.LBB2_43
# BB#42:
	movl	$.L.str.66, %edi
	jmp	.LBB2_17
.LBB2_10:
	movq	8(%r14), %r14
	movq	8(%rbx), %rbp
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	jne	.LBB2_6
# BB#11:
	movl	$.L.str.56, %edi
	movl	$.L.str.57, %edx
	movl	$.L.str.58, %r8d
	movq	%r13, %rsi
	movq	%rbp, %rcx
	movq	%r14, %r9
	callq	_ZN2kc22Problem1S1ID1S1ID1S1IDEPKcPNS_7impl_IDES1_S3_S1_S3_
	jmp	.LBB2_18
.LBB2_43:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$184, %eax
	jne	.LBB2_46
# BB#44:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$184, %eax
	jne	.LBB2_46
# BB#45:
	movl	$.L.str.67, %edi
	jmp	.LBB2_17
.LBB2_46:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$178, %eax
	jne	.LBB2_48
# BB#47:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$178, %eax
	je	.LBB2_6
.LBB2_48:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$178, %eax
	jne	.LBB2_50
# BB#49:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$177, %eax
	je	.LBB2_6
.LBB2_50:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$177, %eax
	jne	.LBB2_53
# BB#51:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$178, %eax
	jne	.LBB2_53
# BB#52:
	movl	$.L.str.68, %edi
	jmp	.LBB2_17
.LBB2_53:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$177, %eax
	jne	.LBB2_56
# BB#54:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$177, %eax
	jne	.LBB2_56
# BB#55:
	movl	$.L.str.69, %edi
	jmp	.LBB2_17
.LBB2_56:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB2_59
# BB#57:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB2_59
# BB#58:
	movl	$.L.str.16, %edi
	jmp	.LBB2_17
.LBB2_59:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB2_62
# BB#60:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$175, %eax
	je	.LBB2_61
.LBB2_62:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$175, %eax
	jne	.LBB2_65
# BB#63:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB2_65
# BB#64:
	movl	$.L.str.71, %edi
	jmp	.LBB2_17
.LBB2_65:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$175, %eax
	jne	.LBB2_67
# BB#66:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$175, %eax
	jne	.LBB2_67
.LBB2_61:
	movl	$.L.str.70, %edi
.LBB2_17:
	movq	%r13, %rsi
	callq	_ZN2kc12Problem1S1IDEPKcPNS_7impl_IDE
.LBB2_18:
	movq	%rax, %rbx
	movl	24(%r13), %esi
	movq	32(%r13), %rdi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc8v_reportEPNS_10impl_errorE # TAILCALL
.LBB2_67:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	jne	.LBB2_69
# BB#68:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	je	.LBB2_6
.LBB2_69:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	jne	.LBB2_72
# BB#70:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	jne	.LBB2_72
# BB#71:
	movl	$.L.str.72, %edi
	jmp	.LBB2_17
.LBB2_72:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	jne	.LBB2_75
# BB#73:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	jne	.LBB2_75
# BB#74:
	movl	$.L.str.73, %edi
	jmp	.LBB2_17
.LBB2_75:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	jne	.LBB2_77
# BB#76:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	je	.LBB2_6
.LBB2_77:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$185, %eax
	jne	.LBB2_80
# BB#78:
	movq	8(%r15), %rsi
	movl	$.L.str.74, %edi
	movl	$.L.str.75, %edx
	jmp	.LBB2_79
.LBB2_80:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$183, %eax
	je	.LBB2_81
# BB#82:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$182, %eax
	jne	.LBB2_83
.LBB2_81:
	movq	8(%r15), %rsi
	movl	$.L.str.76, %edi
	movl	$.L.str.26, %edx
	jmp	.LBB2_79
.LBB2_83:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$180, %eax
	je	.LBB2_84
# BB#85:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$179, %eax
	jne	.LBB2_86
.LBB2_84:
	movq	8(%r15), %rsi
	movl	$.L.str.76, %edi
	movl	$.L.str.28, %edx
.LBB2_79:
	movq	%r13, %rcx
	callq	_ZN2kc16Problem1S1t1S1IDEPKcPNS_11impl_IDtypeES1_PNS_7impl_IDE
	jmp	.LBB2_18
.LBB2_86:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$184, %eax
	jne	.LBB2_88
# BB#87:
	movq	8(%r15), %rsi
	movl	$.L.str.74, %edi
	movl	$.L.str.77, %edx
	jmp	.LBB2_79
.LBB2_88:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$178, %eax
	je	.LBB2_89
# BB#90:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$177, %eax
	jne	.LBB2_91
.LBB2_89:
	movq	8(%r15), %rsi
	movl	$.L.str.76, %edi
	movl	$.L.str.31, %edx
	jmp	.LBB2_79
.LBB2_91:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	je	.LBB2_92
# BB#93:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$175, %eax
	jne	.LBB2_94
.LBB2_92:
	movq	8(%r15), %rsi
	movl	$.L.str.76, %edi
	movl	$.L.str.32, %edx
	jmp	.LBB2_79
.LBB2_94:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	jne	.LBB2_96
# BB#95:
	movq	8(%r15), %rsi
	movl	$.L.str.76, %edi
	movl	$.L.str.33, %edx
	jmp	.LBB2_79
.LBB2_96:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	jne	.LBB2_98
# BB#97:
	movq	8(%r15), %rsi
	movl	$.L.str.76, %edi
	movl	$.L.str.78, %edx
	jmp	.LBB2_79
.LBB2_98:
	movl	$.L.str.55, %edi
	movl	$445, %esi              # imm = 0x1BD
	movl	$.L.str.1, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc21kc_no_default_in_withEPKciS1_ # TAILCALL
.Lfunc_end2:
	.size	_ZN2kcL16do_v_extendoccurEPNS_7impl_IDEPNS_11impl_IDtypeEb, .Lfunc_end2-_ZN2kcL16do_v_extendoccurEPNS_7impl_IDEPNS_11impl_IDtypeEb
	.cfi_endproc

	.globl	_ZN2kc23v_extendoccur_nowarningEPNS_7impl_IDEPNS_11impl_IDtypeE
	.p2align	4, 0x90
	.type	_ZN2kc23v_extendoccur_nowarningEPNS_7impl_IDEPNS_11impl_IDtypeE,@function
_ZN2kc23v_extendoccur_nowarningEPNS_7impl_IDEPNS_11impl_IDtypeE: # @_ZN2kc23v_extendoccur_nowarningEPNS_7impl_IDEPNS_11impl_IDtypeE
	.cfi_startproc
# BB#0:
	xorl	%edx, %edx
	jmp	_ZN2kcL16do_v_extendoccurEPNS_7impl_IDEPNS_11impl_IDtypeEb # TAILCALL
.Lfunc_end3:
	.size	_ZN2kc23v_extendoccur_nowarningEPNS_7impl_IDEPNS_11impl_IDtypeE, .Lfunc_end3-_ZN2kc23v_extendoccur_nowarningEPNS_7impl_IDEPNS_11impl_IDtypeE
	.cfi_endproc

	.globl	_ZN2kc18f_useoccuroperatorEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc18f_useoccuroperatorEPNS_7impl_IDE,@function
_ZN2kc18f_useoccuroperatorEPNS_7impl_IDE: # @_ZN2kc18f_useoccuroperatorEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -24
.Lcfi26:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB4_6
# BB#1:
	movq	40(%rbx), %rax
	movq	8(%rax), %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	movl	%eax, %ecx
	movb	$1, %al
	cmpl	$176, %ecx
	je	.LBB4_8
# BB#2:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$175, %eax
	jne	.LBB4_5
# BB#3:
	movl	$.L.str.34, %edi
	movq	%rbx, %rsi
	callq	_ZN2kc12Problem1S1IDEPKcPNS_7impl_IDE
	jmp	.LBB4_4
.LBB4_6:
	movl	$.L.str.36, %edi
	movl	$471, %esi              # imm = 0x1D7
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	jmp	.LBB4_7
.LBB4_5:
	movl	$.L.str.35, %edi
	movq	%rbx, %rsi
	callq	_ZN2kc13Problem1S1tIDEPKcPNS_7impl_IDE
.LBB4_4:
	movq	%rax, %r14
	movl	24(%rbx), %esi
	movq	32(%rbx), %rdi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB4_7:
	xorl	%eax, %eax
.LBB4_8:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	_ZN2kc18f_useoccuroperatorEPNS_7impl_IDE, .Lfunc_end4-_ZN2kc18f_useoccuroperatorEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc19f_warnifnotvariableEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc19f_warnifnotvariableEPNS_7impl_IDE,@function
_ZN2kc19f_warnifnotvariableEPNS_7impl_IDE: # @_ZN2kc19f_warnifnotvariableEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 32
.Lcfi30:
	.cfi_offset %rbx, -24
.Lcfi31:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB5_4
# BB#1:
	movq	40(%rbx), %rax
	movq	8(%rax), %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$172, %eax
	je	.LBB5_5
# BB#2:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$185, %eax
	je	.LBB5_5
# BB#3:
	movl	$.L.str.37, %edi
	movq	%rbx, %rsi
	callq	_ZN2kc13Problem1S1tIDEPKcPNS_7impl_IDE
	movq	%rax, %r14
	movl	24(%rbx), %esi
	movq	32(%rbx), %rdi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	movb	$1, %al
	jmp	.LBB5_6
.LBB5_4:
	movl	$.L.str.38, %edi
	movl	$496, %esi              # imm = 0x1F0
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB5_5:
	xorl	%eax, %eax
.LBB5_6:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	_ZN2kc19f_warnifnotvariableEPNS_7impl_IDE, .Lfunc_end5-_ZN2kc19f_warnifnotvariableEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc16f_useoccurphylumEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc16f_useoccurphylumEPNS_7impl_IDE,@function
_ZN2kc16f_useoccurphylumEPNS_7impl_IDE: # @_ZN2kc16f_useoccurphylumEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB6_4
# BB#1:
	movq	40(%rbx), %rax
	movq	8(%rax), %rbp
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	movb	$1, %r14b
	cmpl	$174, %eax
	je	.LBB6_6
# BB#2:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	je	.LBB6_6
# BB#3:
	movl	$.L.str.39, %edi
	movq	%rbx, %rsi
	callq	_ZN2kc13Problem1S1tIDEPKcPNS_7impl_IDE
	movq	%rax, %rbp
	movl	24(%rbx), %esi
	movq	32(%rbx), %rdi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB6_5
.LBB6_4:
	movl	$.L.str.40, %edi
	movl	$521, %esi              # imm = 0x209
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB6_5:
	xorl	%r14d, %r14d
.LBB6_6:
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN2kc16f_useoccurphylumEPNS_7impl_IDE, .Lfunc_end6-_ZN2kc16f_useoccurphylumEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc20f_useoccurlistphylumEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc20f_useoccurlistphylumEPNS_7impl_IDE,@function
_ZN2kc20f_useoccurlistphylumEPNS_7impl_IDE: # @_ZN2kc20f_useoccurlistphylumEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 32
.Lcfi41:
	.cfi_offset %rbx, -32
.Lcfi42:
	.cfi_offset %r14, -24
.Lcfi43:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	(%r15), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB7_17
# BB#1:
	movq	40(%r15), %rax
	movq	8(%rax), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	jne	.LBB7_16
# BB#2:
	movq	8(%rbx), %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$15, %eax
	jne	.LBB7_7
# BB#3:
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$24, %eax
	jne	.LBB7_7
# BB#4:
	movl	$.L.str.41, %edi
	jmp	.LBB7_5
.LBB7_17:
	movl	$.L.str.44, %edi
	movl	$567, %esi              # imm = 0x237
.LBB7_18:
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	jmp	.LBB7_19
.LBB7_16:
	movl	$.L.str.45, %edi
	movq	%r15, %rsi
	callq	_ZN2kc13Problem1S1tIDEPKcPNS_7impl_IDE
	jmp	.LBB7_6
.LBB7_7:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$15, %eax
	jne	.LBB7_10
# BB#8:
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$23, %eax
	jne	.LBB7_10
# BB#9:
	movl	$.L.str.42, %edi
	jmp	.LBB7_5
.LBB7_10:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$15, %eax
	jne	.LBB7_12
# BB#11:
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movl	%eax, %ecx
	movb	$1, %al
	cmpl	$22, %ecx
	je	.LBB7_20
.LBB7_12:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$15, %eax
	jne	.LBB7_15
# BB#13:
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$21, %eax
	jne	.LBB7_15
# BB#14:
	movl	$.L.str.43, %edi
.LBB7_5:
	movq	%r15, %rsi
	callq	_ZN2kc12Problem1S1IDEPKcPNS_7impl_IDE
.LBB7_6:
	movq	%rax, %rbx
	movl	24(%r15), %esi
	movq	32(%r15), %rdi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB7_19:
	xorl	%eax, %eax
.LBB7_20:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB7_15:
	movl	$.L.str.44, %edi
	movl	$556, %esi              # imm = 0x22C
	jmp	.LBB7_18
.Lfunc_end7:
	.size	_ZN2kc20f_useoccurlistphylumEPNS_7impl_IDE, .Lfunc_end7-_ZN2kc20f_useoccurlistphylumEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc19f_useoccuruviewnameEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc19f_useoccuruviewnameEPNS_7impl_IDE,@function
_ZN2kc19f_useoccuruviewnameEPNS_7impl_IDE: # @_ZN2kc19f_useoccuruviewnameEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -32
.Lcfi48:
	.cfi_offset %r14, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB8_4
# BB#1:
	movq	40(%rbx), %rax
	movq	8(%rax), %rbp
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	movb	$1, %r14b
	cmpl	$180, %eax
	je	.LBB8_6
# BB#2:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$179, %eax
	je	.LBB8_6
# BB#3:
	movl	$.L.str.46, %edi
	movq	%rbx, %rsi
	callq	_ZN2kc13Problem1S1tIDEPKcPNS_7impl_IDE
	movq	%rax, %rbp
	movl	24(%rbx), %esi
	movq	32(%rbx), %rdi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB8_5
.LBB8_4:
	movl	$.L.str.47, %edi
	movl	$592, %esi              # imm = 0x250
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB8_5:
	xorl	%r14d, %r14d
.LBB8_6:
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZN2kc19f_useoccuruviewnameEPNS_7impl_IDE, .Lfunc_end8-_ZN2kc19f_useoccuruviewnameEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc19f_useoccurrviewnameEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc19f_useoccurrviewnameEPNS_7impl_IDE,@function
_ZN2kc19f_useoccurrviewnameEPNS_7impl_IDE: # @_ZN2kc19f_useoccurrviewnameEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 32
.Lcfi53:
	.cfi_offset %rbx, -32
.Lcfi54:
	.cfi_offset %r14, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB9_4
# BB#1:
	movq	40(%rbx), %rax
	movq	8(%rax), %rbp
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	movb	$1, %r14b
	cmpl	$183, %eax
	je	.LBB9_6
# BB#2:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$182, %eax
	je	.LBB9_6
# BB#3:
	movl	$.L.str.48, %edi
	movq	%rbx, %rsi
	callq	_ZN2kc13Problem1S1tIDEPKcPNS_7impl_IDE
	movq	%rax, %rbp
	movl	24(%rbx), %esi
	movq	32(%rbx), %rdi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB9_5
.LBB9_4:
	movl	$.L.str.49, %edi
	movl	$617, %esi              # imm = 0x269
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB9_5:
	xorl	%r14d, %r14d
.LBB9_6:
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end9:
	.size	_ZN2kc19f_useoccurrviewnameEPNS_7impl_IDE, .Lfunc_end9-_ZN2kc19f_useoccurrviewnameEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc22f_useoccurstorageclassEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc22f_useoccurstorageclassEPNS_7impl_IDE,@function
_ZN2kc22f_useoccurstorageclassEPNS_7impl_IDE: # @_ZN2kc22f_useoccurstorageclassEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 32
.Lcfi59:
	.cfi_offset %rbx, -32
.Lcfi60:
	.cfi_offset %r14, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB10_4
# BB#1:
	movq	40(%rbx), %rax
	movq	8(%rax), %rbp
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	movb	$1, %r14b
	cmpl	$178, %eax
	je	.LBB10_6
# BB#2:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$177, %eax
	je	.LBB10_6
# BB#3:
	movl	$.L.str.50, %edi
	movq	%rbx, %rsi
	callq	_ZN2kc13Problem1S1tIDEPKcPNS_7impl_IDE
	movq	%rax, %rbp
	movl	24(%rbx), %esi
	movq	32(%rbx), %rdi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB10_5
.LBB10_4:
	movl	$.L.str.51, %edi
	movl	$642, %esi              # imm = 0x282
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB10_5:
	xorl	%r14d, %r14d
.LBB10_6:
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_ZN2kc22f_useoccurstorageclassEPNS_7impl_IDE, .Lfunc_end10-_ZN2kc22f_useoccurstorageclassEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc22f_useoccurlanguagenameEPNS_18impl_languagenamesE
	.p2align	4, 0x90
	.type	_ZN2kc22f_useoccurlanguagenameEPNS_18impl_languagenamesE,@function
_ZN2kc22f_useoccurlanguagenameEPNS_18impl_languagenamesE: # @_ZN2kc22f_useoccurlanguagenameEPNS_18impl_languagenamesE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 32
.Lcfi65:
	.cfi_offset %rbx, -32
.Lcfi66:
	.cfi_offset %r14, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movq	(%rbp), %rax
	callq	*(%rax)
	movb	$1, %r14b
	cmpl	$164, %eax
	je	.LBB11_10
# BB#1:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$165, %eax
	jne	.LBB11_7
# BB#2:
	movq	8(%rbp), %rbx
	movq	16(%rbp), %rdi
	callq	_ZN2kc22f_useoccurlanguagenameEPNS_18impl_languagenamesE
	movb	%al, %r14b
	movq	Thelanguages(%rip), %rbp
	jmp	.LBB11_4
	.p2align	4, 0x90
.LBB11_3:                               #   in Loop: Header=BB11_4 Depth=1
	movq	16(%rbp), %rbp
.LBB11_4:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$165, %eax
	jne	.LBB11_8
# BB#5:                                 #   in Loop: Header=BB11_4 Depth=1
	movq	8(%rbp), %rsi
	movq	%rbx, %rdi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	je	.LBB11_3
# BB#6:
	movb	$1, %al
	jmp	.LBB11_9
.LBB11_7:
	movl	$.L.str.52, %edi
	movl	$694, %esi              # imm = 0x2B6
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%r14d, %r14d
	jmp	.LBB11_10
.LBB11_8:                               # %._crit_edge.i
	movq	32(%rbx), %rdi
	movl	24(%rbx), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbp
	movl	$.L.str.79, %edi
	movq	%rbx, %rsi
	callq	_ZN2kc13Problem1S1tIDEPKcPNS_7impl_IDE
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	xorl	%eax, %eax
.LBB11_9:
	andb	%al, %r14b
.LBB11_10:
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZN2kc22f_useoccurlanguagenameEPNS_18impl_languagenamesE, .Lfunc_end11-_ZN2kc22f_useoccurlanguagenameEPNS_18impl_languagenamesE
	.cfi_endproc

	.globl	_ZN2kc25f_useoccurpatternvariableEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc25f_useoccurpatternvariableEPNS_7impl_IDE,@function
_ZN2kc25f_useoccurpatternvariableEPNS_7impl_IDE: # @_ZN2kc25f_useoccurpatternvariableEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi70:
	.cfi_def_cfa_offset 32
.Lcfi71:
	.cfi_offset %rbx, -24
.Lcfi72:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB12_3
# BB#1:
	movq	40(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movl	%eax, %ecx
	movb	$1, %al
	cmpl	$185, %ecx
	je	.LBB12_5
# BB#2:
	movl	$.L.str.53, %edi
	movq	%rbx, %rsi
	callq	_ZN2kc13Problem1S1tIDEPKcPNS_7impl_IDE
	movq	%rax, %r14
	movl	24(%rbx), %esi
	movq	32(%rbx), %rdi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB12_4
.LBB12_3:
	movl	$.L.str.54, %edi
	movl	$716, %esi              # imm = 0x2CC
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB12_4:
	xorl	%eax, %eax
.LBB12_5:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end12:
	.size	_ZN2kc25f_useoccurpatternvariableEPNS_7impl_IDE, .Lfunc_end12-_ZN2kc25f_useoccurpatternvariableEPNS_7impl_IDE
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"v_defoccur"
	.size	.L.str, 11

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/kimwitu++/occur.cc"
	.size	.L.str.1, 82

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"illegal redefinition of local function:"
	.size	.L.str.2, 40

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"illegal redefinition of pattern variable:"
	.size	.L.str.3, 42

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"illegal redefinition of rewrite view:"
	.size	.L.str.4, 38

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"illegal redefinition of predefined rewrite view:"
	.size	.L.str.5, 49

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"illegal predefinition of rewrite view:"
	.size	.L.str.6, 39

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"illegal predefinition of predefined rewrite view:"
	.size	.L.str.7, 50

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"illegal redefinition of unparse view:"
	.size	.L.str.8, 38

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"illegal redefinition of predefined unparse view:"
	.size	.L.str.9, 49

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"illegal predefinition of unparse view:"
	.size	.L.str.10, 39

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"illegal predefinition of predefined unparse view:"
	.size	.L.str.11, 50

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"illegal redefinition of storage class:"
	.size	.L.str.12, 39

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"illegal redefinition of predefined storage class:"
	.size	.L.str.13, 50

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"illegal predefinition of storage class:"
	.size	.L.str.14, 40

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"illegal predefinition of predefined storage class:"
	.size	.L.str.15, 51

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"illegal redefinition of operator:"
	.size	.L.str.16, 34

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"illegal redefinition of predefined operator:"
	.size	.L.str.17, 45

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"illegal predefinition of operator:"
	.size	.L.str.18, 35

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"illegal predefinition of predefined operator:"
	.size	.L.str.19, 46

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"illegal redefinition of phylum:"
	.size	.L.str.20, 32

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"illegal redefinition of predefined phylum:"
	.size	.L.str.21, 43

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"illegal predefinition of phylum:"
	.size	.L.str.22, 33

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"illegal predefinition of predefined phylum:"
	.size	.L.str.23, 44

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"illegal redefinition of "
	.size	.L.str.24, 25

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"as pattern variable"
	.size	.L.str.25, 20

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"as rewrite view"
	.size	.L.str.26, 16

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"illegal predefinition of "
	.size	.L.str.27, 26

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"as unparse view"
	.size	.L.str.28, 16

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"as function"
	.size	.L.str.29, 12

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"as storage class"
	.size	.L.str.30, 17

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"as storage class:"
	.size	.L.str.31, 18

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"as operator:"
	.size	.L.str.32, 13

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"as phylum:"
	.size	.L.str.33, 11

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"undefined operator (it's predefined, you're not supposed to use those):"
	.size	.L.str.34, 72

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"undefined operator"
	.size	.L.str.35, 19

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"f_useoccuroperator"
	.size	.L.str.36, 19

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"variable expected:"
	.size	.L.str.37, 19

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"f_warnifnotvariable"
	.size	.L.str.38, 20

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"undefined phylum"
	.size	.L.str.39, 17

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"f_useoccurphylum"
	.size	.L.str.40, 17

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"undefined list phylum (it's a predefined phylum):"
	.size	.L.str.41, 50

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"undefined list phylum (it's a non-list phylum):"
	.size	.L.str.42, 48

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"undefined list phylum (the productionblock is empty):"
	.size	.L.str.43, 54

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"f_useoccurlistphylum"
	.size	.L.str.44, 21

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"undefined list phylum"
	.size	.L.str.45, 22

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"undefined unparse view:"
	.size	.L.str.46, 24

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"f_useoccuruviewname"
	.size	.L.str.47, 20

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"undefined rewrite view:"
	.size	.L.str.48, 24

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"f_useoccurrviewname"
	.size	.L.str.49, 20

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"undefined storage class:"
	.size	.L.str.50, 25

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"f_useoccurstorageclass"
	.size	.L.str.51, 23

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"f_useoccurlanguagename"
	.size	.L.str.52, 23

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"undefined pattern variable:"
	.size	.L.str.53, 28

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"f_useoccurpatternvariable"
	.size	.L.str.54, 26

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"do_v_extendoccur"
	.size	.L.str.55, 17

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"type mismatch in redefinition of pattern variable:"
	.size	.L.str.56, 51

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"old type"
	.size	.L.str.57, 9

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"new type"
	.size	.L.str.58, 9

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"superfluous redeclaration of rewrite view:"
	.size	.L.str.59, 43

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"superfluous redeclaration of predefined rewrite view:"
	.size	.L.str.60, 54

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"illegal extension of rewrite view:"
	.size	.L.str.61, 35

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"illegal extension of predefined rewrite view:"
	.size	.L.str.62, 46

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"superfluous redeclaration of unparse view:"
	.size	.L.str.63, 43

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"superfluous redeclaration of predefined unparse view:"
	.size	.L.str.64, 54

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"illegal extension of unparse view:"
	.size	.L.str.65, 35

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"illegal extension of predefined unparse view:"
	.size	.L.str.66, 46

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"illegal extension of function:"
	.size	.L.str.67, 31

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"illegal extension of storage class:"
	.size	.L.str.68, 36

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"illegal extension of predefined storage class:"
	.size	.L.str.69, 47

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"illegal extension of predefined operator:"
	.size	.L.str.70, 42

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"illegal extension of operator:"
	.size	.L.str.71, 31

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"illegal extension of predefined phylum:"
	.size	.L.str.72, 40

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"illegal extension of phylum:"
	.size	.L.str.73, 29

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"illegal extension of"
	.size	.L.str.74, 21

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"as pattern variable:"
	.size	.L.str.75, 21

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"illegal extension of "
	.size	.L.str.76, 22

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"as function:"
	.size	.L.str.77, 13

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"as phylum"
	.size	.L.str.78, 10

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"undefined language name:"
	.size	.L.str.79, 25


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
