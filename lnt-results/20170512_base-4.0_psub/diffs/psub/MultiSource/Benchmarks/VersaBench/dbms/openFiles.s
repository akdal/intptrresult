	.text
	.file	"openFiles.bc"
	.globl	openFiles
	.p2align	4, 0x90
	.type	openFiles,@function
openFiles:                              # @openFiles
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rcx, %rbp
	movq	%rsi, %r13
	movq	%rdi, %r12
	cmpq	$2, %r12
	jl	.LBB0_1
# BB#7:                                 # %.lr.ph.lr.ph.preheader
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%r15, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$1, %r15d
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
.LBB0_8:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_9 Depth 2
	incq	%r15
.LBB0_9:                                #   Parent Loop BB0_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%r13,%r15,8), %rbp
	movq	%rbp, %rdi
	callq	strlen
	cmpq	$2, %rax
	jne	.LBB0_28
# BB#10:                                #   in Loop: Header=BB0_9 Depth=2
	cmpb	$45, (%rbp)
	jne	.LBB0_28
# BB#11:                                #   in Loop: Header=BB0_9 Depth=2
	movzbl	1(%rbp), %eax
	addb	$-104, %al
	cmpb	$7, %al
	ja	.LBB0_27
# BB#12:                                #   in Loop: Header=BB0_9 Depth=2
	movzbl	%al, %eax
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_23:                               #   in Loop: Header=BB0_9 Depth=2
	cmpq	%r12, %r15
	jge	.LBB0_25
# BB#24:                                #   in Loop: Header=BB0_9 Depth=2
	movq	(%r13,%r15,8), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	2(%r15), %rax
	incq	%r15
	cmpq	%r12, %r15
	movq	%rax, %r15
	jl	.LBB0_9
	jmp	.LBB0_16
	.p2align	4, 0x90
.LBB0_13:                               #   in Loop: Header=BB0_8 Depth=1
	cmpq	%r12, %r15
	jge	.LBB0_18
# BB#14:                                # %.outer
                                        #   in Loop: Header=BB0_8 Depth=1
	movq	(%r13,%r15,8), %r14
	jmp	.LBB0_15
	.p2align	4, 0x90
.LBB0_20:                               #   in Loop: Header=BB0_8 Depth=1
	cmpq	%r12, %r15
	jge	.LBB0_22
# BB#21:                                # %.outer84
                                        #   in Loop: Header=BB0_8 Depth=1
	movq	(%r13,%r15,8), %rbx
.LBB0_15:                               # %.outer
                                        #   in Loop: Header=BB0_8 Depth=1
	incq	%r15
	cmpq	%r12, %r15
	jl	.LBB0_8
.LBB0_16:                               # %.outer84._crit_edge
	testq	%r14, %r14
	je	.LBB0_17
# BB#29:
	movl	$.L.str.10, %esi
	movq	%r14, %rdi
	callq	fopen
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	testq	%rax, %rax
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	jne	.LBB0_3
# BB#30:
	movl	$.L.str.11, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$openFiles.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$3, %ebx
	jmp	.LBB0_36
.LBB0_1:
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
.LBB0_2:                                # %.thread79
	movq	stdin(%rip), %rax
	movq	%rax, (%rdx)
.LBB0_3:
	testq	%rbx, %rbx
	je	.LBB0_4
# BB#31:
	movl	$.L.str.12, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, %rdi
	movq	%rdi, (%rbp)
	testq	%rdi, %rdi
	jne	.LBB0_5
# BB#32:
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$openFiles.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$4, %ebx
	jmp	.LBB0_36
.LBB0_28:
	movl	$.L.str.9, %edi
.LBB0_19:                               # %.thread
	xorl	%esi, %esi
	callq	errorMessage
	movl	$openFiles.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movq	stderr(%rip), %rdi
	movq	(%r13), %rdx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	(%r13), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$26, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$28, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$30, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$2, %ebx
	jmp	.LBB0_36
.LBB0_4:
	movq	stdout(%rip), %rdi
	movq	%rdi, (%rbp)
.LBB0_5:
	callq	initOutputBuffer
	testq	%r14, %r14
	je	.LBB0_6
# BB#33:
	movl	$.L.str.12, %esi
	movq	%r14, %rdi
	callq	fopen
	movq	%rax, (%r15)
	testq	%rax, %rax
	je	.LBB0_35
# BB#34:
	xorl	%ebx, %ebx
	jmp	.LBB0_36
.LBB0_6:
	movq	stderr(%rip), %rax
	movq	%rax, (%r15)
	xorl	%ebx, %ebx
	jmp	.LBB0_36
.LBB0_35:
	movl	$.L.str.14, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$openFiles.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$5, %ebx
	jmp	.LBB0_36
.LBB0_26:
	movq	stderr(%rip), %rdi
	movq	(%r13), %rdx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	(%r13), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$1, %ebx
	movl	$.L.str.3, %edi
	movl	$26, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$28, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$30, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_36:                               # %.thread
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_27:
	movl	$.L.str.8, %edi
	jmp	.LBB0_19
.LBB0_25:
	movl	$.L.str.7, %edi
	jmp	.LBB0_19
.LBB0_18:
	movl	$.L.str, %edi
	jmp	.LBB0_19
.LBB0_22:
	movl	$.L.str.6, %edi
	jmp	.LBB0_19
.LBB0_17:
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB0_2
.Lfunc_end0:
	.size	openFiles, .Lfunc_end0-openFiles
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_26
	.quad	.LBB0_13
	.quad	.LBB0_27
	.quad	.LBB0_27
	.quad	.LBB0_27
	.quad	.LBB0_23
	.quad	.LBB0_27
	.quad	.LBB0_20

	.type	openFiles.name,@object  # @openFiles.name
	.data
openFiles.name:
	.asciz	"openFiles"
	.size	openFiles.name, 10

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"missing argument for -i"
	.size	.L.str, 24

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Usage: %s [-h], or\n"
	.size	.L.str.1, 20

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"       %s"
	.size	.L.str.2, 10

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	" [-i <input file = stdin>]"
	.size	.L.str.3, 27

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	" [-o <output file = stdout>]"
	.size	.L.str.4, 29

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	" [-m <metrics file = stderr>]\n"
	.size	.L.str.5, 31

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"missing argument for -o"
	.size	.L.str.6, 24

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"missing argument for -m"
	.size	.L.str.7, 24

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"unknown option"
	.size	.L.str.8, 15

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"incorrect format - unknown option"
	.size	.L.str.9, 34

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"r"
	.size	.L.str.10, 2

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"error opening input file"
	.size	.L.str.11, 25

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"w"
	.size	.L.str.12, 2

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"error opening output file"
	.size	.L.str.13, 26

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"error opening metrics file"
	.size	.L.str.14, 27


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
