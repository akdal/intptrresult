	.text
	.file	"symbol.bc"
	.globl	symbol_GetIncreasedOrderingCounter
	.p2align	4, 0x90
	.type	symbol_GetIncreasedOrderingCounter,@function
symbol_GetIncreasedOrderingCounter:     # @symbol_GetIncreasedOrderingCounter
	.cfi_startproc
# BB#0:
	movl	symbol_ORDERING(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, symbol_ORDERING(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end0:
	.size	symbol_GetIncreasedOrderingCounter, .Lfunc_end0-symbol_GetIncreasedOrderingCounter
	.cfi_endproc

	.globl	symbol_MaxStringLength
	.p2align	4, 0x90
	.type	symbol_MaxStringLength,@function
symbol_MaxStringLength:                 # @symbol_MaxStringLength
	.cfi_startproc
# BB#0:
	movslq	symbol_ACTINDEX(%rip), %rcx
	xorl	%eax, %eax
	cmpq	$2, %rcx
	jl	.LBB1_13
# BB#1:                                 # %.lr.ph
	movq	symbol_SIGNATURE(%rip), %rdx
	testb	$1, %cl
	jne	.LBB1_2
# BB#3:
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.LBB1_4
# BB#5:
	movl	8(%rax), %eax
	jmp	.LBB1_6
.LBB1_2:
	xorl	%eax, %eax
	movl	$1, %esi
	cmpl	$2, %ecx
	jne	.LBB1_8
	jmp	.LBB1_13
.LBB1_4:
	xorl	%eax, %eax
.LBB1_6:                                # %.prol.loopexit
	movl	$2, %esi
	cmpl	$2, %ecx
	je	.LBB1_13
	.p2align	4, 0x90
.LBB1_8:                                # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rsi,8), %rdi
	testq	%rdi, %rdi
	je	.LBB1_10
# BB#9:                                 #   in Loop: Header=BB1_8 Depth=1
	movl	8(%rdi), %edi
	cmpl	%eax, %edi
	cmoval	%edi, %eax
.LBB1_10:                               #   in Loop: Header=BB1_8 Depth=1
	movq	8(%rdx,%rsi,8), %rdi
	testq	%rdi, %rdi
	je	.LBB1_12
# BB#11:                                #   in Loop: Header=BB1_8 Depth=1
	movl	8(%rdi), %edi
	cmpl	%eax, %edi
	cmoval	%edi, %eax
.LBB1_12:                               #   in Loop: Header=BB1_8 Depth=1
	addq	$2, %rsi
	cmpq	%rcx, %rsi
	jl	.LBB1_8
.LBB1_13:                               # %._crit_edge
	retq
.Lfunc_end1:
	.size	symbol_MaxStringLength, .Lfunc_end1-symbol_MaxStringLength
	.cfi_endproc

	.globl	symbol_CreateFunction
	.p2align	4, 0x90
	.type	symbol_CreateFunction,@function
symbol_CreateFunction:                  # @symbol_CreateFunction
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %r15d
	movl	%esi, %ebx
	movq	%rdi, %rbp
	movl	$64, %edi
	callq	memory_Malloc
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	strcpy
	xorl	%esi, %esi
	testl	%ebx, %ebx
	setne	%sil
	movq	%rax, %rdi
	movl	%ebx, %edx
	movl	%r15d, %ecx
	movq	%r14, %r8
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	symbol_SignatureCreate  # TAILCALL
.Lfunc_end2:
	.size	symbol_CreateFunction, .Lfunc_end2-symbol_CreateFunction
	.cfi_endproc

	.p2align	4, 0x90
	.type	symbol_SignatureCreate,@function
symbol_SignatureCreate:                 # @symbol_SignatureCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 64
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movl	%ecx, %ebp
	movl	%edx, %r12d
	movl	%esi, %r15d
	movq	%rdi, %r13
	cmpl	$4000, symbol_ACTINDEX(%rip) # imm = 0xFA0
	jl	.LBB3_4
# BB#1:
	movq	symbol_FREEDSYMBOLS(%rip), %rax
	testq	%rax, %rax
	je	.LBB3_2
.LBB3_4:
	movq	%r13, %rdi
	callq	strlen
	cmpq	$64, %rax
	jae	.LBB3_5
# BB#6:
	movl	$40, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movl	$1, 12(%rbx)
	movl	$0, 20(%rbx)
	movq	%r13, (%rbx)
	movq	%r13, %rdi
	callq	strlen
	movl	%eax, 8(%rbx)
	movl	%r12d, 16(%rbx)
	movq	$0, 32(%rbx)
	movq	symbol_FREEDSYMBOLS(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB3_7
# BB#8:
	movq	8(%rdi), %r13
	movslq	%r13d, %r12
	movq	%r12, %rsi
	callq	list_PointerDeleteElement
	movq	%rax, symbol_FREEDSYMBOLS(%rip)
	shll	$3, %r12d
	shll	$2, %ebp
	orl	%r15d, %ebp
	orl	%r12d, %ebp
	negl	%ebp
	movl	%ebp, 24(%rbx)
	jmp	.LBB3_9
.LBB3_7:
	movl	symbol_ACTINDEX(%rip), %r13d
	leal	(,%r13,8), %eax
	shll	$2, %ebp
	orl	%r15d, %ebp
	orl	%eax, %ebp
	negl	%ebp
	movl	%ebp, 24(%rbx)
	leal	1(%r13), %eax
	movl	%eax, symbol_ACTINDEX(%rip)
.LBB3_9:
	movq	symbol_SIGNATURE(%rip), %rax
	movslq	%r13d, %rcx
	movq	%rbx, (%rax,%rcx,8)
	movl	symbol_ORDERING(%rip), %eax
	leal	1(%rax), %ecx
	xorl	%edx, %edx
	subl	24(%rbx), %edx
	movl	%ecx, symbol_ORDERING(%rip)
	sarl	$3, %edx
	movslq	%edx, %rcx
	movl	%eax, (%r14,%rcx,4)
	movl	24(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_2:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.31, %edi
	jmp	.LBB3_3
.LBB3_5:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.32, %edi
.LBB3_3:
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end3:
	.size	symbol_SignatureCreate, .Lfunc_end3-symbol_SignatureCreate
	.cfi_endproc

	.globl	symbol_CreateSkolemFunction
	.p2align	4, 0x90
	.type	symbol_CreateSkolemFunction,@function
symbol_CreateSkolemFunction:            # @symbol_CreateSkolemFunction
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi28:
	.cfi_def_cfa_offset 96
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	%edi, %r12d
	xorl	%eax, %eax
	testl	%r12d, %r12d
	setne	%al
	movl	%eax, 12(%rsp)          # 4-byte Spill
	leaq	30(%rsp), %r13
	jmp	.LBB4_1
	.p2align	4, 0x90
.LBB4_9:                                # %symbol_Lookup.exit.us
                                        #   in Loop: Header=BB4_1 Depth=1
	cmpl	$0, 24(%r15)
	je	.LBB4_11
.LBB4_1:                                # %.outer.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_7 Depth 2
	testl	%r12d, %r12d
	je	.LBB4_3
# BB#2:                                 #   in Loop: Header=BB4_1 Depth=1
	movl	symbol_ACTSKOLEMFINDEX(%rip), %ecx
	leal	1(%rcx), %eax
	movl	%eax, symbol_ACTSKOLEMFINDEX(%rip)
	movl	$.L.str, %esi
	movl	$symbol_SKFNAME, %edx
	jmp	.LBB4_4
	.p2align	4, 0x90
.LBB4_3:                                #   in Loop: Header=BB4_1 Depth=1
	movl	symbol_ACTSKOLEMCINDEX(%rip), %ecx
	leal	1(%rcx), %eax
	movl	%eax, symbol_ACTSKOLEMCINDEX(%rip)
	movl	$.L.str, %esi
	movl	$symbol_SKCNAME, %edx
.LBB4_4:                                #   in Loop: Header=BB4_1 Depth=1
	xorl	%eax, %eax
	movq	%r13, %rdi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	sprintf
	cmpl	$0, symbol_HASSIGNATURE(%rip)
	je	.LBB4_11
# BB#5:                                 #   in Loop: Header=BB4_1 Depth=1
	movl	symbol_ACTINDEX(%rip), %eax
	cmpl	$2, %eax
	jl	.LBB4_11
# BB#6:                                 # %.lr.ph.i.us
                                        #   in Loop: Header=BB4_1 Depth=1
	movslq	%eax, %rbp
	movq	symbol_SIGNATURE(%rip), %rbx
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB4_7:                                #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%r14,8), %r15
	testq	%r15, %r15
	je	.LBB4_10
# BB#8:                                 #   in Loop: Header=BB4_7 Depth=2
	movq	(%r15), %rsi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_9
.LBB4_10:                               #   in Loop: Header=BB4_7 Depth=2
	incq	%r14
	cmpq	%rbp, %r14
	jl	.LBB4_7
.LBB4_11:                               # %symbol_Lookup.exit.thread
                                        #   in Loop: Header=BB4_1 Depth=1
	movl	$64, %edi
	callq	memory_Malloc
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	strcpy
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	%r12d, %edx
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	symbol_SignatureCreate
	testl	%eax, %eax
	je	.LBB4_1
# BB#12:                                # %.us-lcssa
	movl	%eax, %ecx
	negl	%ecx
	sarl	$3, %ecx
	movq	symbol_SIGNATURE(%rip), %rdx
	movslq	%ecx, %rcx
	movq	(%rdx,%rcx,8), %rcx
	orl	$1, 20(%rcx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	symbol_CreateSkolemFunction, .Lfunc_end4-symbol_CreateSkolemFunction
	.cfi_endproc

	.globl	symbol_Lookup
	.p2align	4, 0x90
	.type	symbol_Lookup,@function
symbol_Lookup:                          # @symbol_Lookup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 64
.Lcfi42:
	.cfi_offset %rbx, -56
.Lcfi43:
	.cfi_offset %r12, -48
.Lcfi44:
	.cfi_offset %r13, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	xorl	%r14d, %r14d
	cmpl	$0, symbol_HASSIGNATURE(%rip)
	je	.LBB5_7
# BB#1:
	movl	symbol_ACTINDEX(%rip), %eax
	cmpl	$2, %eax
	jl	.LBB5_7
# BB#2:                                 # %.lr.ph
	movslq	%eax, %r12
	movq	symbol_SIGNATURE(%rip), %r13
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB5_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbp,8), %rbx
	testq	%rbx, %rbx
	je	.LBB5_5
# BB#4:                                 #   in Loop: Header=BB5_3 Depth=1
	movq	(%rbx), %rsi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_6
.LBB5_5:                                #   in Loop: Header=BB5_3 Depth=1
	incq	%rbp
	cmpq	%r12, %rbp
	jl	.LBB5_3
	jmp	.LBB5_7
.LBB5_6:
	movl	24(%rbx), %r14d
.LBB5_7:                                # %.thread
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	symbol_Lookup, .Lfunc_end5-symbol_Lookup
	.cfi_endproc

	.globl	symbol_CreatePredicate
	.p2align	4, 0x90
	.type	symbol_CreatePredicate,@function
symbol_CreatePredicate:                 # @symbol_CreatePredicate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 48
.Lcfi53:
	.cfi_offset %rbx, -40
.Lcfi54:
	.cfi_offset %r14, -32
.Lcfi55:
	.cfi_offset %r15, -24
.Lcfi56:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %r15d
	movl	%esi, %ebx
	movq	%rdi, %rbp
	movl	$64, %edi
	callq	memory_Malloc
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	strcpy
	movl	$2, %esi
	movq	%rax, %rdi
	movl	%ebx, %edx
	movl	%r15d, %ecx
	movq	%r14, %r8
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	symbol_SignatureCreate  # TAILCALL
.Lfunc_end6:
	.size	symbol_CreatePredicate, .Lfunc_end6-symbol_CreatePredicate
	.cfi_endproc

	.globl	symbol_CreateSkolemPredicate
	.p2align	4, 0x90
	.type	symbol_CreateSkolemPredicate,@function
symbol_CreateSkolemPredicate:           # @symbol_CreateSkolemPredicate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi63:
	.cfi_def_cfa_offset 80
.Lcfi64:
	.cfi_offset %rbx, -56
.Lcfi65:
	.cfi_offset %r12, -48
.Lcfi66:
	.cfi_offset %r13, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movq	%rsi, (%rsp)            # 8-byte Spill
	movl	%edi, %r15d
	leaq	14(%rsp), %r12
	jmp	.LBB7_1
	.p2align	4, 0x90
.LBB7_9:                                # %symbol_Lookup.exit.us
                                        #   in Loop: Header=BB7_1 Depth=1
	cmpl	$0, 24(%r14)
	je	.LBB7_11
.LBB7_1:                                # %.outer.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_7 Depth 2
	testl	%r15d, %r15d
	je	.LBB7_3
# BB#2:                                 #   in Loop: Header=BB7_1 Depth=1
	movl	symbol_ACTSKOLEMPINDEX(%rip), %ecx
	leal	1(%rcx), %eax
	movl	%eax, symbol_ACTSKOLEMPINDEX(%rip)
	movl	$.L.str, %esi
	movl	$symbol_SKPNAME, %edx
	jmp	.LBB7_4
	.p2align	4, 0x90
.LBB7_3:                                #   in Loop: Header=BB7_1 Depth=1
	movl	symbol_ACTSKOLEMAINDEX(%rip), %ecx
	leal	1(%rcx), %eax
	movl	%eax, symbol_ACTSKOLEMAINDEX(%rip)
	movl	$.L.str, %esi
	movl	$symbol_SKANAME, %edx
.LBB7_4:                                #   in Loop: Header=BB7_1 Depth=1
	xorl	%eax, %eax
	movq	%r12, %rdi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	sprintf
	cmpl	$0, symbol_HASSIGNATURE(%rip)
	je	.LBB7_11
# BB#5:                                 #   in Loop: Header=BB7_1 Depth=1
	movl	symbol_ACTINDEX(%rip), %eax
	cmpl	$2, %eax
	jl	.LBB7_11
# BB#6:                                 # %.lr.ph.i.us
                                        #   in Loop: Header=BB7_1 Depth=1
	movslq	%eax, %r13
	movq	symbol_SIGNATURE(%rip), %rbx
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB7_7:                                #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rbp,8), %r14
	testq	%r14, %r14
	je	.LBB7_10
# BB#8:                                 #   in Loop: Header=BB7_7 Depth=2
	movq	(%r14), %rsi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB7_9
.LBB7_10:                               #   in Loop: Header=BB7_7 Depth=2
	incq	%rbp
	cmpq	%r13, %rbp
	jl	.LBB7_7
.LBB7_11:                               # %symbol_Lookup.exit.thread
                                        #   in Loop: Header=BB7_1 Depth=1
	movl	$64, %edi
	callq	memory_Malloc
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	strcpy
	movl	$2, %esi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movl	%r15d, %edx
	movq	(%rsp), %r8             # 8-byte Reload
	callq	symbol_SignatureCreate
	testl	%eax, %eax
	je	.LBB7_1
# BB#12:                                # %.us-lcssa
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	symbol_CreateSkolemPredicate, .Lfunc_end7-symbol_CreateSkolemPredicate
	.cfi_endproc

	.globl	symbol_CreateJunctor
	.p2align	4, 0x90
	.type	symbol_CreateJunctor,@function
symbol_CreateJunctor:                   # @symbol_CreateJunctor
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi74:
	.cfi_def_cfa_offset 48
.Lcfi75:
	.cfi_offset %rbx, -40
.Lcfi76:
	.cfi_offset %r14, -32
.Lcfi77:
	.cfi_offset %r15, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %r15d
	movl	%esi, %ebx
	movq	%rdi, %rbp
	movl	$64, %edi
	callq	memory_Malloc
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	strcpy
	movl	$3, %esi
	movq	%rax, %rdi
	movl	%ebx, %edx
	movl	%r15d, %ecx
	movq	%r14, %r8
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	symbol_SignatureCreate  # TAILCALL
.Lfunc_end8:
	.size	symbol_CreateJunctor, .Lfunc_end8-symbol_CreateJunctor
	.cfi_endproc

	.globl	symbol_IsSymbol
	.p2align	4, 0x90
	.type	symbol_IsSymbol,@function
symbol_IsSymbol:                        # @symbol_IsSymbol
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movb	$1, %al
	cmpl	$0, symbol_HASSIGNATURE(%rip)
	je	.LBB9_7
# BB#1:
	testl	%edi, %edi
	je	.LBB9_2
# BB#3:
	leal	-1(%rdi), %ecx
	cmpl	$3000, %ecx             # imm = 0xBB8
	jb	.LBB9_7
# BB#4:
	testl	%edi, %edi
	js	.LBB9_6
# BB#5:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.LBB9_2:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.LBB9_6:
	negl	%edi
	sarl	$3, %edi
	cmpl	symbol_ACTINDEX(%rip), %edi
	setl	%al
.LBB9_7:
	movzbl	%al, %eax
	retq
.Lfunc_end9:
	.size	symbol_IsSymbol, .Lfunc_end9-symbol_IsSymbol
	.cfi_endproc

	.globl	symbol_SignatureExists
	.p2align	4, 0x90
	.type	symbol_SignatureExists,@function
symbol_SignatureExists:                 # @symbol_SignatureExists
	.cfi_startproc
# BB#0:
	movl	symbol_HASSIGNATURE(%rip), %eax
	retq
.Lfunc_end10:
	.size	symbol_SignatureExists, .Lfunc_end10-symbol_SignatureExists
	.cfi_endproc

	.globl	symbol_Delete
	.p2align	4, 0x90
	.type	symbol_Delete,@function
symbol_Delete:                          # @symbol_Delete
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	jg	.LBB11_4
# BB#1:
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi81:
	.cfi_def_cfa_offset 32
.Lcfi82:
	.cfi_offset %rbx, -24
.Lcfi83:
	.cfi_offset %r14, -16
	negl	%edi
	sarl	$3, %edi
	movslq	%edi, %rbx
	movq	symbol_FREEDSYMBOLS(%rip), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, symbol_FREEDSYMBOLS(%rip)
	movq	symbol_SIGNATURE(%rip), %rcx
	movq	(%rcx,%rbx,8), %rax
	movq	$0, (%rcx,%rbx,8)
	movq	(%rax), %rcx
	movq	memory_ARRAY+512(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	movq	memory_ARRAY+512(%rip), %rdx
	movq	%rcx, (%rdx)
	movq	32(%rax), %rcx
	testq	%rcx, %rcx
	leaq	8(%rsp), %rsp
	popq	%rbx
	popq	%r14
	je	.LBB11_3
	.p2align	4, 0x90
.LBB11_2:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB11_2
.LBB11_3:                               # %symbol_FreeSignature.exit
	movq	memory_ARRAY+320(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+320(%rip), %rcx
	movq	%rax, (%rcx)
.LBB11_4:
	retq
.Lfunc_end11:
	.size	symbol_Delete, .Lfunc_end11-symbol_Delete
	.cfi_endproc

	.globl	symbol_GetAllSymbols
	.p2align	4, 0x90
	.type	symbol_GetAllSymbols,@function
symbol_GetAllSymbols:                   # @symbol_GetAllSymbols
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 32
.Lcfi87:
	.cfi_offset %rbx, -32
.Lcfi88:
	.cfi_offset %r14, -24
.Lcfi89:
	.cfi_offset %r15, -16
	xorl	%r14d, %r14d
	cmpl	$0, symbol_HASSIGNATURE(%rip)
	je	.LBB12_6
# BB#1:
	movl	symbol_ACTINDEX(%rip), %ecx
	cmpl	$2, %ecx
	jl	.LBB12_6
# BB#2:                                 # %.lr.ph.preheader
	xorl	%r14d, %r14d
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB12_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	symbol_SIGNATURE(%rip), %rax
	movq	(%rax,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB12_5
# BB#4:                                 #   in Loop: Header=BB12_3 Depth=1
	movslq	24(%rax), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r14, (%rax)
	movl	symbol_ACTINDEX(%rip), %ecx
	movq	%rax, %r14
.LBB12_5:                               #   in Loop: Header=BB12_3 Depth=1
	incq	%rbx
	movslq	%ecx, %rax
	cmpq	%rax, %rbx
	jl	.LBB12_3
.LBB12_6:                               # %.loopexit
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	symbol_GetAllSymbols, .Lfunc_end12-symbol_GetAllSymbols
	.cfi_endproc

	.globl	symbol_GetAllPredicates
	.p2align	4, 0x90
	.type	symbol_GetAllPredicates,@function
symbol_GetAllPredicates:                # @symbol_GetAllPredicates
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 32
.Lcfi93:
	.cfi_offset %rbx, -32
.Lcfi94:
	.cfi_offset %r14, -24
.Lcfi95:
	.cfi_offset %r15, -16
	xorl	%r14d, %r14d
	cmpl	$0, symbol_HASSIGNATURE(%rip)
	je	.LBB13_8
# BB#1:
	movl	symbol_ACTINDEX(%rip), %ecx
	cmpl	$2, %ecx
	jl	.LBB13_8
# BB#2:                                 # %.lr.ph.preheader
	xorl	%r14d, %r14d
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB13_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	symbol_SIGNATURE(%rip), %rax
	movq	(%rax,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB13_7
# BB#4:                                 #   in Loop: Header=BB13_3 Depth=1
	movslq	24(%rax), %r15
	testq	%r15, %r15
	jns	.LBB13_7
# BB#5:                                 # %symbol_IsPredicate.exit
                                        #   in Loop: Header=BB13_3 Depth=1
	movl	%r15d, %eax
	negl	%eax
	andl	$3, %eax
	cmpl	$2, %eax
	jne	.LBB13_7
# BB#6:                                 #   in Loop: Header=BB13_3 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r14, (%rax)
	movl	symbol_ACTINDEX(%rip), %ecx
	movq	%rax, %r14
	.p2align	4, 0x90
.LBB13_7:                               # %symbol_IsPredicate.exit.thread
                                        #   in Loop: Header=BB13_3 Depth=1
	incq	%rbx
	movslq	%ecx, %rax
	cmpq	%rax, %rbx
	jl	.LBB13_3
.LBB13_8:                               # %.loopexit
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end13:
	.size	symbol_GetAllPredicates, .Lfunc_end13-symbol_GetAllPredicates
	.cfi_endproc

	.globl	symbol_GetAllFunctions
	.p2align	4, 0x90
	.type	symbol_GetAllFunctions,@function
symbol_GetAllFunctions:                 # @symbol_GetAllFunctions
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 32
.Lcfi99:
	.cfi_offset %rbx, -32
.Lcfi100:
	.cfi_offset %r14, -24
.Lcfi101:
	.cfi_offset %r15, -16
	xorl	%r14d, %r14d
	cmpl	$0, symbol_HASSIGNATURE(%rip)
	je	.LBB14_8
# BB#1:
	movl	symbol_ACTINDEX(%rip), %ecx
	cmpl	$2, %ecx
	jl	.LBB14_8
# BB#2:                                 # %.lr.ph.preheader
	xorl	%r14d, %r14d
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB14_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	symbol_SIGNATURE(%rip), %rax
	movq	(%rax,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB14_7
# BB#4:                                 #   in Loop: Header=BB14_3 Depth=1
	movslq	24(%rax), %r15
	testq	%r15, %r15
	jns	.LBB14_7
# BB#5:                                 # %symbol_IsFunction.exit
                                        #   in Loop: Header=BB14_3 Depth=1
	movl	%r15d, %eax
	negl	%eax
	andl	$2, %eax
	orl	$1, %eax
	cmpl	$1, %eax
	jne	.LBB14_7
# BB#6:                                 #   in Loop: Header=BB14_3 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r14, (%rax)
	movl	symbol_ACTINDEX(%rip), %ecx
	movq	%rax, %r14
	.p2align	4, 0x90
.LBB14_7:                               # %symbol_IsFunction.exit.thread
                                        #   in Loop: Header=BB14_3 Depth=1
	incq	%rbx
	movslq	%ecx, %rax
	cmpq	%rax, %rbx
	jl	.LBB14_3
.LBB14_8:                               # %.loopexit
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	symbol_GetAllFunctions, .Lfunc_end14-symbol_GetAllFunctions
	.cfi_endproc

	.globl	symbol_FreeAllSymbols
	.p2align	4, 0x90
	.type	symbol_FreeAllSymbols,@function
symbol_FreeAllSymbols:                  # @symbol_FreeAllSymbols
	.cfi_startproc
# BB#0:
	cmpl	$0, symbol_HASSIGNATURE(%rip)
	je	.LBB15_13
# BB#1:                                 # %.preheader
	movl	symbol_ACTINDEX(%rip), %edx
	cmpl	$2, %edx
	jl	.LBB15_8
# BB#2:                                 # %.lr.ph.preheader
	movl	$1, %eax
	.p2align	4, 0x90
.LBB15_3:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_5 Depth 2
	movq	symbol_SIGNATURE(%rip), %rcx
	movq	(%rcx,%rax,8), %r8
	testq	%r8, %r8
	je	.LBB15_7
# BB#4:                                 #   in Loop: Header=BB15_3 Depth=1
	movq	(%r8), %rdx
	movq	memory_ARRAY+512(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	memory_ARRAY+512(%rip), %rsi
	movq	%rdx, (%rsi)
	movq	32(%r8), %rdx
	testq	%rdx, %rdx
	je	.LBB15_6
	.p2align	4, 0x90
.LBB15_5:                               # %.lr.ph.i.i
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rsi
	movq	memory_ARRAY+128(%rip), %rdi
	movslq	32(%rdi), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rdi), %rcx
	movq	%rcx, (%rdx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rdx, (%rcx)
	testq	%rsi, %rsi
	movq	%rsi, %rdx
	jne	.LBB15_5
.LBB15_6:                               # %symbol_FreeSignature.exit
                                        #   in Loop: Header=BB15_3 Depth=1
	movq	memory_ARRAY+320(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r8)
	movq	memory_ARRAY+320(%rip), %rcx
	movq	%r8, (%rcx)
	movl	symbol_ACTINDEX(%rip), %edx
.LBB15_7:                               #   in Loop: Header=BB15_3 Depth=1
	incq	%rax
	movslq	%edx, %rcx
	cmpq	%rcx, %rax
	jl	.LBB15_3
.LBB15_8:                               # %._crit_edge
	movq	symbol_SIGNATURE(%rip), %rdi
	movl	memory_ALIGN(%rip), %ecx
	movl	$32000, %esi            # imm = 0x7D00
	movl	$32000, %eax            # imm = 0x7D00
	xorl	%edx, %edx
	divl	%ecx
	addl	$32000, %ecx            # imm = 0x7D00
	subl	%edx, %ecx
	testl	%edx, %edx
	cmovel	%esi, %ecx
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rdx
	subq	%rax, %rdx
	movq	-16(%rdx), %rsi
	movq	-8(%rdx), %r8
	testq	%rsi, %rsi
	leaq	8(%rsi), %r9
	movl	$memory_BIGBLOCKS, %esi
	cmovneq	%r9, %rsi
	movq	%r8, (%rsi)
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB15_10
# BB#9:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rdx)
.LBB15_10:
	addl	memory_MARKSIZE(%rip), %ecx
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rcx), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB15_12
# BB#11:
	leaq	16(%rcx,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB15_12:                              # %memory_Free.exit
	pushq	%rax
.Lcfi102:
	.cfi_def_cfa_offset 16
	addq	$-16, %rdi
	callq	free
	addq	$8, %rsp
.LBB15_13:
	movq	symbol_VARSTRING(%rip), %rax
	movq	memory_ARRAY+512(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+512(%rip), %rcx
	movq	%rax, (%rcx)
	movq	symbol_FREEDSYMBOLS(%rip), %rax
	testq	%rax, %rax
	je	.LBB15_15
	.p2align	4, 0x90
.LBB15_14:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB15_14
.LBB15_15:                              # %list_Delete.exit
	retq
.Lfunc_end15:
	.size	symbol_FreeAllSymbols, .Lfunc_end15-symbol_FreeAllSymbols
	.cfi_endproc

	.globl	symbol_Init
	.p2align	4, 0x90
	.type	symbol_Init,@function
symbol_Init:                            # @symbol_Init
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 16
.Lcfi104:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movl	$0, symbol_ACTSKOLEMFINDEX(%rip)
	movl	$0, symbol_ACTSKOLEMCINDEX(%rip)
	movl	$0, symbol_ACTSKOLEMPINDEX(%rip)
	movl	$0, symbol_ACTSKOLEMAINDEX(%rip)
	movl	$symbol_CONTEXT, %edi
	xorl	%esi, %esi
	movl	$16000, %edx            # imm = 0x3E80
	callq	memset
	testl	%ebx, %ebx
	je	.LBB16_2
# BB#1:
	movl	$32000, %edi            # imm = 0x7D00
	callq	memory_Malloc
	movq	%rax, symbol_SIGNATURE(%rip)
.LBB16_2:
	movl	$0, symbol_STANDARDVARCOUNTER(%rip)
	movl	$2000, symbol_INDEXVARCOUNTER(%rip) # imm = 0x7D0
	movl	$1, symbol_ACTINDEX(%rip)
	movl	$1, symbol_ORDERING(%rip)
	movl	$64, %edi
	callq	memory_Malloc
	movq	%rax, symbol_VARSTRING(%rip)
	movl	%ebx, symbol_HASSIGNATURE(%rip)
	movq	$0, symbol_FREEDSYMBOLS(%rip)
	popq	%rbx
	retq
.Lfunc_end16:
	.size	symbol_Init, .Lfunc_end16-symbol_Init
	.cfi_endproc

	.globl	symbol_ReinitGenericNameCounters
	.p2align	4, 0x90
	.type	symbol_ReinitGenericNameCounters,@function
symbol_ReinitGenericNameCounters:       # @symbol_ReinitGenericNameCounters
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi105:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi106:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi107:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi109:
	.cfi_def_cfa_offset 48
.Lcfi110:
	.cfi_offset %rbx, -40
.Lcfi111:
	.cfi_offset %r12, -32
.Lcfi112:
	.cfi_offset %r14, -24
.Lcfi113:
	.cfi_offset %r15, -16
	cmpl	$0, symbol_HASSIGNATURE(%rip)
	je	.LBB17_26
# BB#1:
	cmpl	$2, symbol_ACTINDEX(%rip)
	jl	.LBB17_26
# BB#2:                                 # %.lr.ph.preheader
	movl	$1, %r12d
	jmp	.LBB17_3
.LBB17_18:                              #   in Loop: Header=BB17_3 Depth=1
	movq	%r14, %rdi
	callq	string_StringIsNumber
	testl	%eax, %eax
	je	.LBB17_25
# BB#19:                                #   in Loop: Header=BB17_3 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r14, %rdi
	callq	strtol
	cmpl	symbol_ACTSKOLEMAINDEX(%rip), %eax
	jl	.LBB17_25
# BB#20:                                #   in Loop: Header=BB17_3 Depth=1
	incl	%eax
	movl	%eax, symbol_ACTSKOLEMAINDEX(%rip)
	jmp	.LBB17_25
	.p2align	4, 0x90
.LBB17_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	symbol_SIGNATURE(%rip), %rax
	movq	(%rax,%r12,8), %rbx
	testq	%rbx, %rbx
	je	.LBB17_25
# BB#4:                                 #   in Loop: Header=BB17_3 Depth=1
	movq	(%rbx), %r15
	movq	%r15, %rdi
	callq	strlen
	cmpq	$4, %rax
	jb	.LBB17_25
# BB#5:                                 #   in Loop: Header=BB17_3 Depth=1
	leaq	3(%r15), %r14
	xorl	%eax, %eax
	subl	24(%rbx), %eax
	andb	$3, %al
	cmpb	$2, %al
	je	.LBB17_16
# BB#6:                                 #   in Loop: Header=BB17_3 Depth=1
	cmpb	$1, %al
	je	.LBB17_12
# BB#7:                                 #   in Loop: Header=BB17_3 Depth=1
	testb	%al, %al
	jne	.LBB17_25
# BB#8:                                 #   in Loop: Header=BB17_3 Depth=1
	movl	$symbol_SKCNAME, %esi
	movl	$3, %edx
	movq	%r15, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB17_25
# BB#9:                                 #   in Loop: Header=BB17_3 Depth=1
	movq	%r14, %rdi
	callq	string_StringIsNumber
	testl	%eax, %eax
	je	.LBB17_25
# BB#10:                                #   in Loop: Header=BB17_3 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r14, %rdi
	callq	strtol
	cmpl	symbol_ACTSKOLEMCINDEX(%rip), %eax
	jl	.LBB17_25
# BB#11:                                #   in Loop: Header=BB17_3 Depth=1
	incl	%eax
	movl	%eax, symbol_ACTSKOLEMCINDEX(%rip)
	jmp	.LBB17_25
.LBB17_16:                              #   in Loop: Header=BB17_3 Depth=1
	cmpl	$0, 16(%rbx)
	je	.LBB17_17
# BB#21:                                #   in Loop: Header=BB17_3 Depth=1
	movl	$symbol_SKPNAME, %esi
	movl	$3, %edx
	movq	%r15, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB17_25
# BB#22:                                #   in Loop: Header=BB17_3 Depth=1
	movq	%r14, %rdi
	callq	string_StringIsNumber
	testl	%eax, %eax
	je	.LBB17_25
# BB#23:                                #   in Loop: Header=BB17_3 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r14, %rdi
	callq	strtol
	cmpl	symbol_ACTSKOLEMPINDEX(%rip), %eax
	jl	.LBB17_25
# BB#24:                                #   in Loop: Header=BB17_3 Depth=1
	incl	%eax
	movl	%eax, symbol_ACTSKOLEMPINDEX(%rip)
	jmp	.LBB17_25
.LBB17_12:                              #   in Loop: Header=BB17_3 Depth=1
	movl	$symbol_SKFNAME, %esi
	movl	$3, %edx
	movq	%r15, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB17_25
# BB#13:                                #   in Loop: Header=BB17_3 Depth=1
	movq	%r14, %rdi
	callq	string_StringIsNumber
	testl	%eax, %eax
	je	.LBB17_25
# BB#14:                                #   in Loop: Header=BB17_3 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r14, %rdi
	callq	strtol
	cmpl	symbol_ACTSKOLEMFINDEX(%rip), %eax
	jl	.LBB17_25
# BB#15:                                #   in Loop: Header=BB17_3 Depth=1
	incl	%eax
	movl	%eax, symbol_ACTSKOLEMFINDEX(%rip)
	jmp	.LBB17_25
.LBB17_17:                              #   in Loop: Header=BB17_3 Depth=1
	movl	$symbol_SKANAME, %esi
	movl	$3, %edx
	movq	%r15, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB17_18
	.p2align	4, 0x90
.LBB17_25:                              #   in Loop: Header=BB17_3 Depth=1
	incq	%r12
	movslq	symbol_ACTINDEX(%rip), %rax
	cmpq	%rax, %r12
	jl	.LBB17_3
.LBB17_26:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end17:
	.size	symbol_ReinitGenericNameCounters, .Lfunc_end17-symbol_ReinitGenericNameCounters
	.cfi_endproc

	.globl	symbol_LowerSignature
	.p2align	4, 0x90
	.type	symbol_LowerSignature,@function
symbol_LowerSignature:                  # @symbol_LowerSignature
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi114:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi116:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi117:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi118:
	.cfi_def_cfa_offset 48
.Lcfi119:
	.cfi_offset %rbx, -40
.Lcfi120:
	.cfi_offset %r12, -32
.Lcfi121:
	.cfi_offset %r14, -24
.Lcfi122:
	.cfi_offset %r15, -16
	movl	symbol_ACTINDEX(%rip), %eax
	cmpl	$2, %eax
	jl	.LBB18_9
# BB#1:                                 # %.lr.ph.preheader
	movl	$1, %ebx
	jmp	.LBB18_2
	.p2align	4, 0x90
.LBB18_4:                               # %symbol_IsPredicate.exit
                                        #   in Loop: Header=BB18_2 Depth=1
	negl	%ecx
	movl	%ecx, %edx
	andl	$3, %edx
	cmpl	$2, %edx
	je	.LBB18_6
# BB#5:                                 # %symbol_IsFunction.exit
                                        #   in Loop: Header=BB18_2 Depth=1
	andl	$2, %ecx
	orl	$1, %ecx
	cmpl	$1, %ecx
	jne	.LBB18_8
.LBB18_6:                               #   in Loop: Header=BB18_2 Depth=1
	movq	(%r12), %r14
	movzbl	(%r14), %ecx
	addb	$-65, %cl
	cmpb	$25, %cl
	ja	.LBB18_8
# BB#7:                                 #   in Loop: Header=BB18_2 Depth=1
	movl	$64, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	leaq	2(%r15), %rdi
	movq	%r14, %rsi
	callq	strcpy
	movb	$115, 1(%r15)
	movb	$115, (%r15)
	movq	%r15, (%r12)
	movq	memory_ARRAY+512(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+512(%rip), %rax
	movq	%r14, (%rax)
	movl	symbol_ACTINDEX(%rip), %eax
	jmp	.LBB18_8
	.p2align	4, 0x90
.LBB18_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	symbol_SIGNATURE(%rip), %rcx
	movq	(%rcx,%rbx,8), %r12
	testq	%r12, %r12
	je	.LBB18_8
# BB#3:                                 #   in Loop: Header=BB18_2 Depth=1
	movl	24(%r12), %ecx
	testl	%ecx, %ecx
	js	.LBB18_4
.LBB18_8:                               # %symbol_IsFunction.exit.thread
                                        #   in Loop: Header=BB18_2 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB18_2
.LBB18_9:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end18:
	.size	symbol_LowerSignature, .Lfunc_end18-symbol_LowerSignature
	.cfi_endproc

	.globl	symbol_Dump
	.p2align	4, 0x90
	.type	symbol_Dump,@function
symbol_Dump:                            # @symbol_Dump
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi123:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi125:
	.cfi_def_cfa_offset 32
.Lcfi126:
	.cfi_offset %rbx, -24
.Lcfi127:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	cmpl	$0, symbol_HASSIGNATURE(%rip)
	je	.LBB19_6
# BB#1:
	movq	stdout(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$5, %esi
	movl	$1, %edx
	callq	fwrite
	movl	symbol_ACTINDEX(%rip), %eax
	cmpl	$2, %eax
	jl	.LBB19_6
# BB#2:                                 # %.lr.ph.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB19_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	symbol_SIGNATURE(%rip), %rcx
	movq	(%rcx,%rbx,8), %r10
	testq	%r10, %r10
	je	.LBB19_5
# BB#4:                                 #   in Loop: Header=BB19_3 Depth=1
	movl	24(%r10), %edx
	movl	12(%r10), %ecx
	movl	(%r14,%rbx,4), %r8d
	movl	20(%r10), %r9d
	movl	8(%r10), %r11d
	movl	$.L.str.2, %edi
	movl	$0, %eax
	movl	%ebx, %esi
	pushq	%r11
.Lcfi128:
	.cfi_adjust_cfa_offset 8
	pushq	(%r10)
.Lcfi129:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$16, %rsp
.Lcfi130:
	.cfi_adjust_cfa_offset -16
	movl	symbol_ACTINDEX(%rip), %eax
.LBB19_5:                               #   in Loop: Header=BB19_3 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB19_3
.LBB19_6:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end19:
	.size	symbol_Dump, .Lfunc_end19-symbol_Dump
	.cfi_endproc

	.globl	symbol_SortByPrecedence
	.p2align	4, 0x90
	.type	symbol_SortByPrecedence,@function
symbol_SortByPrecedence:                # @symbol_SortByPrecedence
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB20_7
# BB#1:                                 # %.preheader.preheader
	movq	%rdi, %r8
	.p2align	4, 0x90
.LBB20_2:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_3 Depth 2
	movq	(%r8), %rcx
	testq	%rcx, %rcx
	je	.LBB20_7
	.p2align	4, 0x90
.LBB20_3:                               #   Parent Loop BB20_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %r9
	movq	8(%r8), %r10
	movl	%r9d, %edx
	negl	%edx
	sarl	$3, %edx
	movslq	%edx, %rdx
	movl	(%rsi,%rdx,4), %edx
	movl	%r10d, %eax
	negl	%eax
	sarl	$3, %eax
	cltq
	cmpl	(%rsi,%rax,4), %edx
	jge	.LBB20_5
# BB#4:                                 #   in Loop: Header=BB20_3 Depth=2
	movq	%r9, 8(%r8)
	movq	%r10, 8(%rcx)
.LBB20_5:                               # %.backedge
                                        #   in Loop: Header=BB20_3 Depth=2
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB20_3
# BB#6:                                 # %._crit_edge
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	(%r8), %r8
	testq	%r8, %r8
	jne	.LBB20_2
.LBB20_7:                               # %._crit_edge31
	movq	%rdi, %rax
	retq
.Lfunc_end20:
	.size	symbol_SortByPrecedence, .Lfunc_end20-symbol_SortByPrecedence
	.cfi_endproc

	.globl	symbol_RearrangePrecedence
	.p2align	4, 0x90
	.type	symbol_RearrangePrecedence,@function
symbol_RearrangePrecedence:             # @symbol_RearrangePrecedence
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi131:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi133:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi134:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 48
.Lcfi136:
	.cfi_offset %rbx, -48
.Lcfi137:
	.cfi_offset %r12, -40
.Lcfi138:
	.cfi_offset %r13, -32
.Lcfi139:
	.cfi_offset %r14, -24
.Lcfi140:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	testq	%r15, %r15
	je	.LBB21_11
# BB#1:                                 # %.lr.ph32.preheader
	xorl	%r12d, %r12d
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB21_2:                               # %.lr.ph32
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	subl	8(%rbx), %eax
	sarl	$3, %eax
	cltq
	movslq	(%r14,%rax,4), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%r12, (%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %r12
	jne	.LBB21_2
# BB#3:                                 # %._crit_edge
	movq	%rax, %rdi
	callq	list_PointerSort
	testq	%r15, %r15
	je	.LBB21_8
# BB#4:                                 # %._crit_edge
	testq	%rax, %rax
	je	.LBB21_8
# BB#5:                                 # %.lr.ph.preheader
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB21_6:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	8(%rcx), %edx
	xorl	%esi, %esi
	subl	8(%r15), %esi
	sarl	$3, %esi
	movslq	%esi, %rsi
	movl	%edx, (%r14,%rsi,4)
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.LBB21_8
# BB#7:                                 # %.lr.ph
                                        #   in Loop: Header=BB21_6 Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB21_6
	jmp	.LBB21_8
.LBB21_11:                              # %._crit_edge.thread
	xorl	%edi, %edi
	callq	list_PointerSort
.LBB21_8:                               # %.critedge
	testq	%rax, %rax
	je	.LBB21_10
	.p2align	4, 0x90
.LBB21_9:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB21_9
.LBB21_10:                              # %list_Delete.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end21:
	.size	symbol_RearrangePrecedence, .Lfunc_end21-symbol_RearrangePrecedence
	.cfi_endproc

	.globl	symbol_PrintPrecedence
	.p2align	4, 0x90
	.type	symbol_PrintPrecedence,@function
symbol_PrintPrecedence:                 # @symbol_PrintPrecedence
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi141:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi142:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi143:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi144:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi145:
	.cfi_def_cfa_offset 48
.Lcfi146:
	.cfi_offset %rbx, -48
.Lcfi147:
	.cfi_offset %r12, -40
.Lcfi148:
	.cfi_offset %r13, -32
.Lcfi149:
	.cfi_offset %r14, -24
.Lcfi150:
	.cfi_offset %r15, -16
	movq	%rdi, %r13
	cmpl	$0, symbol_HASSIGNATURE(%rip)
	je	.LBB22_25
# BB#1:                                 # %.preheader
	movl	symbol_ACTINDEX(%rip), %ecx
	cmpl	$2, %ecx
	jl	.LBB22_25
# BB#2:                                 # %.lr.ph32.preheader
	xorl	%r14d, %r14d
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB22_3:                               # %.lr.ph32
                                        # =>This Inner Loop Header: Depth=1
	movq	symbol_SIGNATURE(%rip), %rax
	movq	(%rax,%r15,8), %rax
	testq	%rax, %rax
	je	.LBB22_8
# BB#4:                                 #   in Loop: Header=BB22_3 Depth=1
	movslq	24(%rax), %r12
	testq	%r12, %r12
	jns	.LBB22_8
# BB#5:                                 # %symbol_IsPredicate.exit
                                        #   in Loop: Header=BB22_3 Depth=1
	movl	%r12d, %eax
	negl	%eax
	movl	%eax, %edx
	andl	$3, %edx
	cmpl	$2, %edx
	je	.LBB22_7
# BB#6:                                 # %symbol_IsFunction.exit
                                        #   in Loop: Header=BB22_3 Depth=1
	andl	$2, %eax
	orl	$1, %eax
	cmpl	$1, %eax
	jne	.LBB22_8
.LBB22_7:                               #   in Loop: Header=BB22_3 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%r14, (%rax)
	movl	symbol_ACTINDEX(%rip), %ecx
	movq	%rax, %r14
	.p2align	4, 0x90
.LBB22_8:                               # %symbol_IsFunction.exit.thread
                                        #   in Loop: Header=BB22_3 Depth=1
	incq	%r15
	movslq	%ecx, %rax
	cmpq	%rax, %r15
	jl	.LBB22_3
# BB#9:                                 # %._crit_edge
	testq	%r14, %r14
	je	.LBB22_25
# BB#10:                                # %.preheader.i.preheader
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB22_11:                              # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_12 Depth 2
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB22_16
	.p2align	4, 0x90
.LBB22_12:                              #   Parent Loop BB22_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %rdx
	movq	8(%rax), %rsi
	movl	%edx, %edi
	negl	%edi
	sarl	$3, %edi
	movslq	%edi, %rdi
	movl	(%r13,%rdi,4), %edi
	movl	%esi, %ebx
	negl	%ebx
	sarl	$3, %ebx
	movslq	%ebx, %rbx
	cmpl	(%r13,%rbx,4), %edi
	jge	.LBB22_14
# BB#13:                                #   in Loop: Header=BB22_12 Depth=2
	movq	%rdx, 8(%rax)
	movq	%rsi, 8(%rcx)
.LBB22_14:                              # %.backedge.i
                                        #   in Loop: Header=BB22_12 Depth=2
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_12
# BB#15:                                # %._crit_edge.i
                                        #   in Loop: Header=BB22_11 Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB22_11
.LBB22_16:                              # %symbol_SortByPrecedence.exit.preheader
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.LBB22_23
# BB#17:                                # %.lr.ph.preheader
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB22_18:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	subl	8(%rbx), %eax
	sarl	$3, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	movq	(%rax), %rdi
	movq	stdout(%rip), %rsi
	callq	fputs
	cmpq	$0, (%rbx)
	je	.LBB22_19
# BB#21:                                # %symbol_SortByPrecedence.exit
                                        #   in Loop: Header=BB22_18 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$3, %esi
	movl	$1, %edx
	callq	fwrite
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB22_18
# BB#22:
	movq	%r14, %rax
.LBB22_23:                              # %symbol_SortByPrecedence.exit._crit_edge
	testq	%r14, %r14
	movq	%rax, %r14
	jne	.LBB22_20
	jmp	.LBB22_25
.LBB22_19:                              # %symbol_SortByPrecedence.exit.thread
	testq	%r14, %r14
	je	.LBB22_25
.LBB22_20:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB22_20
.LBB22_25:                              # %list_Delete.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end22:
	.size	symbol_PrintPrecedence, .Lfunc_end22-symbol_PrintPrecedence
	.cfi_endproc

	.globl	symbol_FPrintPrecedence
	.p2align	4, 0x90
	.type	symbol_FPrintPrecedence,@function
symbol_FPrintPrecedence:                # @symbol_FPrintPrecedence
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi151:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi152:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi153:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi154:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi155:
	.cfi_def_cfa_offset 48
.Lcfi156:
	.cfi_offset %rbx, -48
.Lcfi157:
	.cfi_offset %r12, -40
.Lcfi158:
	.cfi_offset %r14, -32
.Lcfi159:
	.cfi_offset %r15, -24
.Lcfi160:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	cmpl	$0, symbol_HASSIGNATURE(%rip)
	je	.LBB23_26
# BB#1:                                 # %.preheader
	movl	symbol_ACTINDEX(%rip), %ecx
	cmpl	$2, %ecx
	jl	.LBB23_27
# BB#2:                                 # %.lr.ph40.preheader
	xorl	%r15d, %r15d
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB23_3:                               # %.lr.ph40
                                        # =>This Inner Loop Header: Depth=1
	movq	symbol_SIGNATURE(%rip), %rax
	movq	(%rax,%r12,8), %rax
	testq	%rax, %rax
	je	.LBB23_8
# BB#4:                                 #   in Loop: Header=BB23_3 Depth=1
	movslq	24(%rax), %rbp
	testq	%rbp, %rbp
	jns	.LBB23_8
# BB#5:                                 # %symbol_IsPredicate.exit
                                        #   in Loop: Header=BB23_3 Depth=1
	movl	%ebp, %eax
	negl	%eax
	movl	%eax, %edx
	andl	$3, %edx
	cmpl	$2, %edx
	je	.LBB23_7
# BB#6:                                 # %symbol_IsFunction.exit
                                        #   in Loop: Header=BB23_3 Depth=1
	andl	$2, %eax
	orl	$1, %eax
	cmpl	$1, %eax
	jne	.LBB23_8
.LBB23_7:                               #   in Loop: Header=BB23_3 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r15, (%rax)
	movl	symbol_ACTINDEX(%rip), %ecx
	movq	%rax, %r15
	.p2align	4, 0x90
.LBB23_8:                               # %symbol_IsFunction.exit.thread
                                        #   in Loop: Header=BB23_3 Depth=1
	incq	%r12
	movslq	%ecx, %rax
	cmpq	%rax, %r12
	jl	.LBB23_3
# BB#9:                                 # %._crit_edge41
	testq	%r15, %r15
	je	.LBB23_27
# BB#10:                                # %.preheader.i.preheader
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB23_11:                              # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_12 Depth 2
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB23_16
	.p2align	4, 0x90
.LBB23_12:                              #   Parent Loop BB23_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %rdx
	movq	8(%rax), %rsi
	movl	%edx, %edi
	negl	%edi
	sarl	$3, %edi
	movslq	%edi, %rdi
	movl	(%rbx,%rdi,4), %edi
	movl	%esi, %ebp
	negl	%ebp
	sarl	$3, %ebp
	movslq	%ebp, %rbp
	cmpl	(%rbx,%rbp,4), %edi
	jge	.LBB23_14
# BB#13:                                #   in Loop: Header=BB23_12 Depth=2
	movq	%rdx, 8(%rax)
	movq	%rsi, 8(%rcx)
.LBB23_14:                              # %.backedge.i
                                        #   in Loop: Header=BB23_12 Depth=2
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB23_12
# BB#15:                                # %._crit_edge.i
                                        #   in Loop: Header=BB23_11 Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB23_11
.LBB23_16:                              # %symbol_SortByPrecedence.exit
	movl	$.L.str.4, %edi
	movl	$15, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	testq	%r15, %r15
	je	.LBB23_28
# BB#17:                                # %.lr.ph.preheader
	xorl	%ebp, %ebp
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB23_18:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	subl	8(%rbx), %eax
	sarl	$3, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	movq	(%rax), %rdi
	movq	%r14, %rsi
	callq	fputs
	cmpq	$0, (%rbx)
	je	.LBB23_20
# BB#19:                                #   in Loop: Header=BB23_18 Depth=1
	movl	$44, %edi
	movq	%r14, %rsi
	callq	_IO_putc
.LBB23_20:                              #   in Loop: Header=BB23_18 Depth=1
	cmpl	$16, %ebp
	jl	.LBB23_22
# BB#21:                                #   in Loop: Header=BB23_18 Depth=1
	movl	$.L.str.5, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	xorl	%ebp, %ebp
	jmp	.LBB23_23
	.p2align	4, 0x90
.LBB23_22:                              #   in Loop: Header=BB23_18 Depth=1
	incl	%ebp
.LBB23_23:                              #   in Loop: Header=BB23_18 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB23_18
# BB#24:                                # %._crit_edge
	movl	$.L.str.6, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	testq	%r15, %r15
	je	.LBB23_26
	.p2align	4, 0x90
.LBB23_25:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB23_25
.LBB23_26:                              # %list_Delete.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB23_27:                              # %symbol_SortByPrecedence.exit.thread
	movl	$.L.str.4, %edi
	movl	$15, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
.LBB23_28:                              # %._crit_edge.thread
	movl	$.L.str.6, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.Lfunc_end23:
	.size	symbol_FPrintPrecedence, .Lfunc_end23-symbol_FPrintPrecedence
	.cfi_endproc

	.globl	symbol_SetCount
	.p2align	4, 0x90
	.type	symbol_SetCount,@function
symbol_SetCount:                        # @symbol_SetCount
	.cfi_startproc
# BB#0:
	negl	%edi
	sarl	$3, %edi
	movslq	%edi, %rax
	movq	%rsi, symbol_COUNT(,%rax,8)
	retq
.Lfunc_end24:
	.size	symbol_SetCount, .Lfunc_end24-symbol_SetCount
	.cfi_endproc

	.globl	symbol_GetCount
	.p2align	4, 0x90
	.type	symbol_GetCount,@function
symbol_GetCount:                        # @symbol_GetCount
	.cfi_startproc
# BB#0:
	negl	%edi
	sarl	$3, %edi
	movslq	%edi, %rax
	movq	symbol_COUNT(,%rax,8), %rax
	retq
.Lfunc_end25:
	.size	symbol_GetCount, .Lfunc_end25-symbol_GetCount
	.cfi_endproc

	.globl	symbol_Print
	.p2align	4, 0x90
	.type	symbol_Print,@function
symbol_Print:                           # @symbol_Print
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	movq	stdout(%rip), %rdi
	movl	%eax, %esi
	jmp	symbol_FPrint           # TAILCALL
.Lfunc_end26:
	.size	symbol_Print, .Lfunc_end26-symbol_Print
	.cfi_endproc

	.globl	symbol_FPrint
	.p2align	4, 0x90
	.type	symbol_FPrint,@function
symbol_FPrint:                          # @symbol_FPrint
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi161:
	.cfi_def_cfa_offset 16
.Lcfi162:
	.cfi_offset %rbx, -16
	movl	%esi, %ecx
	movq	%rdi, %rbx
	testl	%ecx, %ecx
	je	.LBB27_14
# BB#1:
	jle	.LBB27_11
# BB#2:
	leal	-2000(%rcx), %edx
	cmpl	$2001, %ecx             # imm = 0x7D1
	cmovll	%ecx, %edx
	leal	-1(%rcx), %eax
	cmpl	$1999, %eax             # imm = 0x7CF
	ja	.LBB27_6
# BB#3:
	movq	symbol_VARSTRING(%rip), %rdi
	cmpl	$6, %ecx
	jg	.LBB27_5
# BB#4:
	addl	$84, %edx
	movb	%dl, (%rdi)
	movb	$0, 1(%rdi)
	jmp	.LBB27_9
.LBB27_14:
	movl	$.L.str.7, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	popq	%rbx
	jmp	fwrite                  # TAILCALL
.LBB27_11:
	cmpl	$0, symbol_HASSIGNATURE(%rip)
	je	.LBB27_13
# BB#12:
	negl	%ecx
	sarl	$3, %ecx
	movq	symbol_SIGNATURE(%rip), %rax
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %rdi
	jmp	.LBB27_10
.LBB27_6:
	addl	$-2001, %ecx            # imm = 0xF82F
	cmpl	$999, %ecx              # imm = 0x3E7
	ja	.LBB27_9
# BB#7:
	movq	symbol_VARSTRING(%rip), %rdi
	movl	$.L.str.10, %esi
	jmp	.LBB27_8
.LBB27_5:
	addl	$-6, %edx
	movl	$.L.str.9, %esi
.LBB27_8:
	xorl	%eax, %eax
	callq	sprintf
.LBB27_9:
	movq	symbol_VARSTRING(%rip), %rdi
.LBB27_10:
	movq	%rbx, %rsi
	popq	%rbx
	jmp	fputs                   # TAILCALL
.LBB27_13:
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ecx, %edx
	popq	%rbx
	jmp	fprintf                 # TAILCALL
.Lfunc_end27:
	.size	symbol_FPrint, .Lfunc_end27-symbol_FPrint
	.cfi_endproc

	.globl	symbol_FPrintOtter
	.p2align	4, 0x90
	.type	symbol_FPrintOtter,@function
symbol_FPrintOtter:                     # @symbol_FPrintOtter
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi163:
	.cfi_def_cfa_offset 16
.Lcfi164:
	.cfi_offset %rbx, -16
	movl	%esi, %ecx
	movq	%rdi, %rbx
	testl	%ecx, %ecx
	jle	.LBB28_4
# BB#1:
	leal	-2000(%rcx), %edx
	cmpl	$2001, %ecx             # imm = 0x7D1
	cmovll	%ecx, %edx
	leal	-1(%rcx), %eax
	cmpl	$1999, %eax             # imm = 0x7CF
	ja	.LBB28_9
# BB#2:
	movq	symbol_VARSTRING(%rip), %rdi
	cmpl	$6, %ecx
	jg	.LBB28_11
# BB#3:
	addl	$116, %edx
	movb	%dl, (%rdi)
	movb	$0, 1(%rdi)
	jmp	.LBB28_13
.LBB28_4:
	cmpl	$0, symbol_HASSIGNATURE(%rip)
	je	.LBB28_15
# BB#5:
	movl	%ecx, %eax
	negl	%eax
	testl	%ecx, %ecx
	js	.LBB28_16
# BB#6:
	xorl	%edx, %edx
.LBB28_7:                               # %symbol_IsPredicate.exit
	sarl	$3, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	movq	(%rax), %rcx
	testb	%dl, %dl
	je	.LBB28_19
# BB#8:
	movl	$.L.str.15, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	popq	%rbx
	jmp	fprintf                 # TAILCALL
.LBB28_9:
	addl	$-2001, %ecx            # imm = 0xF82F
	cmpl	$999, %ecx              # imm = 0x3E7
	ja	.LBB28_13
# BB#10:
	movq	symbol_VARSTRING(%rip), %rdi
	movl	$.L.str.10, %esi
	jmp	.LBB28_12
.LBB28_11:
	addl	$-6, %edx
	movl	$.L.str.12, %esi
.LBB28_12:
	xorl	%eax, %eax
	callq	sprintf
.LBB28_13:
	movq	symbol_VARSTRING(%rip), %rdi
.LBB28_14:
	movq	%rbx, %rsi
	popq	%rbx
	jmp	fputs                   # TAILCALL
.LBB28_15:
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ecx, %edx
	popq	%rbx
	jmp	fprintf                 # TAILCALL
.LBB28_16:                              # %symbol_IsConstant.exit
	movl	%eax, %ecx
	andl	$3, %ecx
	je	.LBB28_20
# BB#17:                                # %symbol_IsFunction.exit
	movl	%eax, %edx
	andl	$2, %edx
	orl	$1, %edx
	cmpl	$1, %edx
	jne	.LBB28_22
# BB#18:
	sarl	$3, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	movq	(%rax), %rdx
	movl	$.L.str.14, %esi
	jmp	.LBB28_21
.LBB28_19:
	movq	%rcx, %rdi
	jmp	.LBB28_14
.LBB28_20:
	sarl	$3, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	movq	(%rax), %rdx
	movl	$.L.str.13, %esi
.LBB28_21:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	popq	%rbx
	jmp	fprintf                 # TAILCALL
.LBB28_22:
	cmpl	$2, %ecx
	sete	%dl
	jmp	.LBB28_7
.Lfunc_end28:
	.size	symbol_FPrintOtter, .Lfunc_end28-symbol_FPrintOtter
	.cfi_endproc

	.globl	symbol_PrintLn
	.p2align	4, 0x90
	.type	symbol_PrintLn,@function
symbol_PrintLn:                         # @symbol_PrintLn
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi165:
	.cfi_def_cfa_offset 16
	movl	%edi, %eax
	movq	stdout(%rip), %rdi
	movl	%eax, %esi
	callq	symbol_FPrint
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	popq	%rax
	jmp	_IO_putc                # TAILCALL
.Lfunc_end29:
	.size	symbol_PrintLn, .Lfunc_end29-symbol_PrintLn
	.cfi_endproc

	.globl	symbol_PrintAll
	.p2align	4, 0x90
	.type	symbol_PrintAll,@function
symbol_PrintAll:                        # @symbol_PrintAll
	.cfi_startproc
# BB#0:
	cmpl	$0, symbol_HASSIGNATURE(%rip)
	je	.LBB30_45
# BB#1:                                 # %misc_PrintChar.exit200
	pushq	%rbp
.Lcfi166:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi167:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi168:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi169:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi170:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi171:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi172:
	.cfi_def_cfa_offset 64
.Lcfi173:
	.cfi_offset %rbx, -56
.Lcfi174:
	.cfi_offset %r12, -48
.Lcfi175:
	.cfi_offset %r13, -40
.Lcfi176:
	.cfi_offset %r14, -32
.Lcfi177:
	.cfi_offset %r15, -24
.Lcfi178:
	.cfi_offset %rbp, -16
	movq	stdout(%rip), %rcx
	movl	$.L.str.16, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$43, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$43, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$43, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$43, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$43, %edi
	callq	_IO_putc
	movl	$36, %ebx
	.p2align	4, 0x90
.LBB30_2:                               # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	decl	%ebx
	jne	.LBB30_2
# BB#3:                                 # %misc_PrintChar.exit94
	movq	stdout(%rip), %rsi
	movl	$43, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$124, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$.L.str.17, %edi
	callq	fputs
	movl	$6, %ebx
	subl	%eax, %ebx
	je	.LBB30_6
# BB#4:                                 # %.lr.ph.i.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB30_5:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	incl	%ebp
	cmpl	%ebx, %ebp
	jbe	.LBB30_5
.LBB30_6:                               # %misc_PrintChar.exit98
	movq	stdout(%rip), %rsi
	movl	$124, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$.L.str.18, %edi
	callq	fputs
	movl	$7, %ebx
	subl	%eax, %ebx
	je	.LBB30_9
# BB#7:                                 # %.lr.ph.i99.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB30_8:                               # %.lr.ph.i99
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	incl	%ebp
	cmpl	%ebx, %ebp
	jbe	.LBB30_8
.LBB30_9:                               # %misc_PrintChar.exit101
	movq	stdout(%rip), %rsi
	movl	$124, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$.L.str.19, %edi
	callq	fputs
	movl	$6, %ebx
	subl	%eax, %ebx
	je	.LBB30_12
# BB#10:                                # %.lr.ph.i102.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB30_11:                              # %.lr.ph.i102
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	incl	%ebp
	cmpl	%ebx, %ebp
	jbe	.LBB30_11
.LBB30_12:                              # %misc_PrintChar.exit104
	movq	stdout(%rip), %rsi
	movl	$124, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$.L.str.20, %edi
	callq	fputs
	movl	$8, %ebx
	subl	%eax, %ebx
	je	.LBB30_15
# BB#13:                                # %.lr.ph.i105.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB30_14:                              # %.lr.ph.i105
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	incl	%ebp
	cmpl	%ebx, %ebp
	jbe	.LBB30_14
.LBB30_15:                              # %misc_PrintChar.exit107
	movq	stdout(%rip), %rsi
	movl	$124, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$.L.str.21, %edi
	callq	fputs
	movl	$36, %ebx
	movl	$36, %r14d
	subl	%eax, %r14d
	je	.LBB30_18
# BB#16:                                # %.lr.ph.i108.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB30_17:                              # %.lr.ph.i108
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	incl	%ebp
	cmpl	%r14d, %ebp
	jbe	.LBB30_17
.LBB30_18:                              # %misc_PrintChar.exit110
	movq	stdout(%rip), %rsi
	movl	$124, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$43, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$43, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$43, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$43, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$43, %edi
	callq	_IO_putc
	.p2align	4, 0x90
.LBB30_19:                              # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	decl	%ebx
	jne	.LBB30_19
# BB#20:                                # %misc_PrintChar.exit128
	movq	stdout(%rip), %rsi
	movl	$43, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	symbol_ACTINDEX(%rip), %eax
	cmpl	$2, %eax
	jl	.LBB30_42
# BB#21:                                # %.lr.ph.preheader
	movl	$1, %r13d
	.p2align	4, 0x90
.LBB30_22:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_25 Depth 2
                                        #     Child Loop BB30_28 Depth 2
                                        #     Child Loop BB30_33 Depth 2
                                        #     Child Loop BB30_36 Depth 2
                                        #     Child Loop BB30_39 Depth 2
	movq	symbol_SIGNATURE(%rip), %rcx
	movq	(%rcx,%r13,8), %rcx
	testq	%rcx, %rcx
	je	.LBB30_41
# BB#23:                                #   in Loop: Header=BB30_22 Depth=1
	movq	stdout(%rip), %rsi
	xorl	%r12d, %r12d
	movq	%rcx, %rbp
	subl	24(%rcx), %r12d
	movl	$32, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$124, %edi
	callq	_IO_putc
	movl	%r12d, %r15d
	andl	$3, %r15d
	movq	.Lswitch.table(,%r15,8), %rdi
	movq	stdout(%rip), %rsi
	callq	fputs
	movl	$6, %ebx
	subl	%eax, %ebx
	je	.LBB30_26
# BB#24:                                # %.lr.ph.i135.preheader
                                        #   in Loop: Header=BB30_22 Depth=1
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB30_25:                              # %.lr.ph.i135
                                        #   Parent Loop BB30_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	incl	%r14d
	cmpl	%ebx, %r14d
	jbe	.LBB30_25
.LBB30_26:                              # %misc_PrintChar.exit137
                                        #   in Loop: Header=BB30_22 Depth=1
	movq	stdout(%rip), %rsi
	movl	$124, %edi
	callq	_IO_putc
	movq	%rbp, %r14
	movl	16(%r14), %esi
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$7, %ebx
	subl	%eax, %ebx
	je	.LBB30_29
# BB#27:                                # %.lr.ph.i138.preheader
                                        #   in Loop: Header=BB30_22 Depth=1
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB30_28:                              # %.lr.ph.i138
                                        #   Parent Loop BB30_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	incl	%ebp
	cmpl	%ebx, %ebp
	jbe	.LBB30_28
.LBB30_29:                              # %misc_PrintChar.exit140
                                        #   in Loop: Header=BB30_22 Depth=1
	movq	stdout(%rip), %rsi
	movl	$124, %edi
	callq	_IO_putc
	movl	$18, %eax
	cmpl	$1, %r15d
	jne	.LBB30_31
# BB#30:                                # %.sink.split
                                        #   in Loop: Header=BB30_22 Depth=1
	testb	$4, %r12b
	movq	stdout(%rip), %rsi
	movl	$.L.str.28, %edi
	movl	$.L.str.27, %eax
	cmoveq	%rax, %rdi
	callq	fputs
	addl	$18, %eax
.LBB30_31:                              #   in Loop: Header=BB30_22 Depth=1
	movl	$24, %ebx
	subl	%eax, %ebx
	je	.LBB30_34
# BB#32:                                # %.lr.ph.i144.preheader
                                        #   in Loop: Header=BB30_22 Depth=1
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB30_33:                              # %.lr.ph.i144
                                        #   Parent Loop BB30_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	incl	%ebp
	cmpl	%ebx, %ebp
	jbe	.LBB30_33
.LBB30_34:                              # %misc_PrintChar.exit146
                                        #   in Loop: Header=BB30_22 Depth=1
	movq	stdout(%rip), %rsi
	movl	$124, %edi
	callq	_IO_putc
	movl	20(%r14), %esi
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$8, %ebx
	subl	%eax, %ebx
	je	.LBB30_37
# BB#35:                                # %.lr.ph.i147.preheader
                                        #   in Loop: Header=BB30_22 Depth=1
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB30_36:                              # %.lr.ph.i147
                                        #   Parent Loop BB30_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	incl	%ebp
	cmpl	%ebx, %ebp
	jbe	.LBB30_36
.LBB30_37:                              # %misc_PrintChar.exit149
                                        #   in Loop: Header=BB30_22 Depth=1
	movq	stdout(%rip), %rsi
	movl	$124, %edi
	callq	_IO_putc
	movq	(%r14), %rsi
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$36, %ebx
	subl	%eax, %ebx
	je	.LBB30_40
# BB#38:                                # %.lr.ph.i150.preheader
                                        #   in Loop: Header=BB30_22 Depth=1
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB30_39:                              # %.lr.ph.i150
                                        #   Parent Loop BB30_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	incl	%ebp
	cmpl	%ebx, %ebp
	jbe	.LBB30_39
.LBB30_40:                              # %misc_PrintChar.exit152
                                        #   in Loop: Header=BB30_22 Depth=1
	movq	stdout(%rip), %rsi
	movl	$124, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	symbol_ACTINDEX(%rip), %eax
.LBB30_41:                              #   in Loop: Header=BB30_22 Depth=1
	incq	%r13
	movslq	%eax, %rcx
	cmpq	%rcx, %r13
	jl	.LBB30_22
.LBB30_42:                              # %.preheader.preheader
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$43, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$43, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$43, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$43, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$124, %edi
	callq	_IO_putc
	movl	$36, %ebx
	.p2align	4, 0x90
.LBB30_43:                              # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
	decl	%ebx
	jne	.LBB30_43
# BB#44:                                # %misc_PrintChar.exit134
	movq	stdout(%rip), %rsi
	movl	$43, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_IO_putc                # TAILCALL
.LBB30_45:
	retq
.Lfunc_end30:
	.size	symbol_PrintAll, .Lfunc_end30-symbol_PrintAll
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_Error,@function
misc_Error:                             # @misc_Error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi179:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$1, %edi
	callq	exit
.Lfunc_end31:
	.size	misc_Error, .Lfunc_end31-misc_Error
	.cfi_endproc

	.type	symbol_MASK,@object     # @symbol_MASK
	.section	.rodata,"a",@progbits
	.globl	symbol_MASK
	.p2align	2
symbol_MASK:
	.long	3                       # 0x3
	.size	symbol_MASK, 4

	.type	symbol_TYPEMASK,@object # @symbol_TYPEMASK
	.globl	symbol_TYPEMASK
	.p2align	2
symbol_TYPEMASK:
	.long	3                       # 0x3
	.size	symbol_TYPEMASK, 4

	.type	symbol_STATMASK,@object # @symbol_STATMASK
	.globl	symbol_STATMASK
	.p2align	2
symbol_STATMASK:
	.long	4                       # 0x4
	.size	symbol_STATMASK, 4

	.type	symbol_TYPESTATMASK,@object # @symbol_TYPESTATMASK
	.globl	symbol_TYPESTATMASK
	.p2align	2
symbol_TYPESTATMASK:
	.long	7                       # 0x7
	.size	symbol_TYPESTATMASK, 4

	.type	symbol_TYPEBITS,@object # @symbol_TYPEBITS
	.globl	symbol_TYPEBITS
	.p2align	2
symbol_TYPEBITS:
	.long	2                       # 0x2
	.size	symbol_TYPEBITS, 4

	.type	symbol_STATBITS,@object # @symbol_STATBITS
	.globl	symbol_STATBITS
	.p2align	2
symbol_STATBITS:
	.long	1                       # 0x1
	.size	symbol_STATBITS, 4

	.type	symbol_TYPESTATBITS,@object # @symbol_TYPESTATBITS
	.globl	symbol_TYPESTATBITS
	.p2align	2
symbol_TYPESTATBITS:
	.long	3                       # 0x3
	.size	symbol_TYPESTATBITS, 4

	.type	symbol_SIGTYPES,@object # @symbol_SIGTYPES
	.globl	symbol_SIGTYPES
	.p2align	2
symbol_SIGTYPES:
	.long	4                       # 0x4
	.size	symbol_SIGTYPES, 4

	.type	symbol_SKFNAME,@object  # @symbol_SKFNAME
	.globl	symbol_SKFNAME
symbol_SKFNAME:
	.asciz	"skf"
	.size	symbol_SKFNAME, 4

	.type	symbol_SKCNAME,@object  # @symbol_SKCNAME
	.globl	symbol_SKCNAME
symbol_SKCNAME:
	.asciz	"skc"
	.size	symbol_SKCNAME, 4

	.type	symbol_SKPNAME,@object  # @symbol_SKPNAME
	.globl	symbol_SKPNAME
symbol_SKPNAME:
	.asciz	"SkP"
	.size	symbol_SKPNAME, 4

	.type	symbol_SKANAME,@object  # @symbol_SKANAME
	.globl	symbol_SKANAME
symbol_SKANAME:
	.asciz	"SkC"
	.size	symbol_SKANAME, 4

	.type	symbol_SKLENGTH,@object # @symbol_SKLENGTH
	.globl	symbol_SKLENGTH
	.p2align	2
symbol_SKLENGTH:
	.long	3                       # 0x3
	.size	symbol_SKLENGTH, 4

	.type	symbol_ORDERING,@object # @symbol_ORDERING
	.comm	symbol_ORDERING,4,4
	.type	symbol_ACTINDEX,@object # @symbol_ACTINDEX
	.comm	symbol_ACTINDEX,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s%d"
	.size	.L.str, 5

	.type	symbol_ACTSKOLEMCINDEX,@object # @symbol_ACTSKOLEMCINDEX
	.comm	symbol_ACTSKOLEMCINDEX,4,4
	.type	symbol_ACTSKOLEMFINDEX,@object # @symbol_ACTSKOLEMFINDEX
	.comm	symbol_ACTSKOLEMFINDEX,4,4
	.type	symbol_ACTSKOLEMAINDEX,@object # @symbol_ACTSKOLEMAINDEX
	.comm	symbol_ACTSKOLEMAINDEX,4,4
	.type	symbol_ACTSKOLEMPINDEX,@object # @symbol_ACTSKOLEMPINDEX
	.comm	symbol_ACTSKOLEMPINDEX,4,4
	.type	symbol_FREEDSYMBOLS,@object # @symbol_FREEDSYMBOLS
	.local	symbol_FREEDSYMBOLS
	.comm	symbol_FREEDSYMBOLS,8,8
	.type	symbol_SIGNATURE,@object # @symbol_SIGNATURE
	.comm	symbol_SIGNATURE,8,8
	.type	symbol_VARSTRING,@object # @symbol_VARSTRING
	.comm	symbol_VARSTRING,8,8
	.type	symbol_STANDARDVARCOUNTER,@object # @symbol_STANDARDVARCOUNTER
	.comm	symbol_STANDARDVARCOUNTER,4,4
	.type	symbol_INDEXVARCOUNTER,@object # @symbol_INDEXVARCOUNTER
	.comm	symbol_INDEXVARCOUNTER,4,4
	.type	symbol_HASSIGNATURE,@object # @symbol_HASSIGNATURE
	.local	symbol_HASSIGNATURE
	.comm	symbol_HASSIGNATURE,4,4
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Dump:"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\n\t %4d:%4d:%4d:%4d:%4d:%s:%d"
	.size	.L.str.2, 29

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	" > "
	.size	.L.str.3, 4

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"set_precedence("
	.size	.L.str.4, 16

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\n\t"
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	")."
	.size	.L.str.6, 3

	.type	symbol_COUNT,@object    # @symbol_COUNT
	.local	symbol_COUNT
	.comm	symbol_COUNT,32000,16
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"NULL"
	.size	.L.str.7, 5

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"X%d"
	.size	.L.str.9, 4

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"I%d"
	.size	.L.str.10, 4

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%d"
	.size	.L.str.11, 3

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"x%d"
	.size	.L.str.12, 4

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"c%s"
	.size	.L.str.13, 4

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"f%s"
	.size	.L.str.14, 4

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"P%s"
	.size	.L.str.15, 4

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"\n\n"
	.size	.L.str.16, 3

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	" Type"
	.size	.L.str.17, 6

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	" Arity"
	.size	.L.str.18, 7

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	" Stat"
	.size	.L.str.19, 6

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	" Prop"
	.size	.L.str.20, 6

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	" String"
	.size	.L.str.21, 8

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	" Con"
	.size	.L.str.22, 5

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	" Fun"
	.size	.L.str.23, 5

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	" Pre"
	.size	.L.str.24, 5

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	" Jun"
	.size	.L.str.25, 5

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	" %3d"
	.size	.L.str.26, 5

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	" Lex"
	.size	.L.str.27, 5

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	" Mul"
	.size	.L.str.28, 5

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	" %u"
	.size	.L.str.29, 4

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	" %s"
	.size	.L.str.30, 4

	.type	symbol_CONTEXT,@object  # @symbol_CONTEXT
	.comm	symbol_CONTEXT,16000,16
	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"\n In symbol_SignatureCreate: No more symbols available.\n"
	.size	.L.str.31, 57

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"\n In symbol_SignatureCreate: String too long.\n"
	.size	.L.str.32, 47

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.quad	.L.str.22
	.quad	.L.str.23
	.quad	.L.str.24
	.quad	.L.str.25
	.size	.Lswitch.table, 32


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
