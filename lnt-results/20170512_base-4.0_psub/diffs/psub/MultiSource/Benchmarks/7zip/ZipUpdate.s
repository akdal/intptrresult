	.text
	.file	"ZipUpdate.bc"
	.globl	_ZN8NArchive4NZip11CThreadInfo11WaitAndCodeEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11CThreadInfo11WaitAndCodeEv,@function
_ZN8NArchive4NZip11CThreadInfo11WaitAndCodeEv: # @_ZN8NArchive4NZip11CThreadInfo11WaitAndCodeEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	16(%rbx), %r14
	movq	%r14, %rdi
	callq	Event_Wait
	cmpb	$0, 144(%rbx)
	jne	.LBB0_6
# BB#1:                                 # %.lr.ph
	leaq	192(%rbx), %r15
	leaq	376(%rbx), %r12
	leaq	384(%rbx), %r13
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movq	184(%rbx), %rsi
	movq	160(%rbx), %rcx
	movq	176(%rbx), %rdx
	movq	%r15, %rdi
	movq	%r12, %r8
	callq	_ZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultE
	movl	%eax, 368(%rbx)
	testl	%eax, %eax
	jne	.LBB0_5
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_5
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	(%rdi), %rax
	movq	%r12, %rsi
	movq	%r13, %rdx
	callq	*40(%rax)
	movl	%eax, 368(%rbx)
.LBB0_5:                                #   in Loop: Header=BB0_2 Depth=1
	movq	128(%rbx), %rdi
	callq	pthread_mutex_lock
	movb	$1, 137(%rbx)
	movq	128(%rbx), %rbp
	leaq	40(%rbp), %rdi
	callq	pthread_cond_broadcast
	movq	%rbp, %rdi
	callq	pthread_mutex_unlock
	movq	%r14, %rdi
	callq	Event_Wait
	cmpb	$0, 144(%rbx)
	je	.LBB0_2
.LBB0_6:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN8NArchive4NZip11CThreadInfo11WaitAndCodeEv, .Lfunc_end0-_ZN8NArchive4NZip11CThreadInfo11WaitAndCodeEv
	.cfi_endproc

	.globl	_ZN8NArchive4NZip17CMtProgressMixer26CreateEP9IProgressb
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip17CMtProgressMixer26CreateEP9IProgressb,@function
_ZN8NArchive4NZip17CMtProgressMixer26CreateEP9IProgressb: # @_ZN8NArchive4NZip17CMtProgressMixer26CreateEP9IProgressb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	testq	%rbp, %rbp
	je	.LBB1_2
# BB#1:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*8(%rax)
.LBB1_2:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB1_4:                                # %_ZN9CMyComPtrI9IProgressEaSEPS0_.exit
	movq	%rbp, 56(%rbx)
	leaq	64(%rbx), %rdx
	movq	(%rbp), %rax
	movl	$IID_ICompressProgressInfo, %esi
	movq	%rbp, %rdi
	callq	*(%rax)
	movb	%r14b, 72(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movq	$0, 48(%rbx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN8NArchive4NZip17CMtProgressMixer26CreateEP9IProgressb, .Lfunc_end1-_ZN8NArchive4NZip17CMtProgressMixer26CreateEP9IProgressb
	.cfi_endproc

	.globl	_ZN8NArchive4NZip17CMtProgressMixer217SetProgressOffsetEy
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip17CMtProgressMixer217SetProgressOffsetEy,@function
_ZN8NArchive4NZip17CMtProgressMixer217SetProgressOffsetEy: # @_ZN8NArchive4NZip17CMtProgressMixer217SetProgressOffsetEy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -32
.Lcfi23:
	.cfi_offset %r14, -24
.Lcfi24:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	80(%rbx), %r15
	movq	%r15, %rdi
	callq	pthread_mutex_lock
	movq	$0, 48(%rbx)
	movq	$0, 32(%rbx)
	movq	%r14, 16(%rbx)
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	pthread_mutex_unlock    # TAILCALL
.Lfunc_end2:
	.size	_ZN8NArchive4NZip17CMtProgressMixer217SetProgressOffsetEy, .Lfunc_end2-_ZN8NArchive4NZip17CMtProgressMixer217SetProgressOffsetEy
	.cfi_endproc

	.globl	_ZN8NArchive4NZip17CMtProgressMixer212SetRatioInfoEiPKyS3_
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip17CMtProgressMixer212SetRatioInfoEiPKyS3_,@function
_ZN8NArchive4NZip17CMtProgressMixer212SetRatioInfoEiPKyS3_: # @_ZN8NArchive4NZip17CMtProgressMixer212SetRatioInfoEiPKyS3_
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 64
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r12
	movl	%esi, %r13d
	movq	%rdi, %rbx
	leaq	80(%rbx), %r14
	movq	%r14, %rdi
	callq	pthread_mutex_lock
	testl	%r13d, %r13d
	je	.LBB3_1
.LBB3_4:
	testq	%r12, %r12
	je	.LBB3_6
# BB#5:
	movq	(%r12), %rax
	movslq	%r13d, %rcx
	movq	%rax, 24(%rbx,%rcx,8)
.LBB3_6:
	testq	%r15, %r15
	je	.LBB3_8
# BB#7:
	movq	(%r15), %rax
	movslq	%r13d, %rcx
	movq	%rax, 40(%rbx,%rcx,8)
.LBB3_8:
	leaq	40(%rbx), %rax
	cmpb	$0, 72(%rbx)
	leaq	24(%rbx), %rcx
	cmoveq	%rax, %rcx
	movq	(%rcx), %rax
	addq	16(%rbx), %rax
	addq	8(%rcx), %rax
	movq	%rax, (%rsp)
	movq	56(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp3:
	movq	%rsp, %rsi
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp4:
.LBB3_9:
	movq	%r14, %rdi
	callq	pthread_mutex_unlock
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_1:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_4
# BB#2:
	movq	(%rdi), %rax
.Ltmp0:
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp1:
# BB#3:
	testl	%ebp, %ebp
	jne	.LBB3_9
	jmp	.LBB3_4
.LBB3_10:
.Ltmp2:
	jmp	.LBB3_11
.LBB3_12:
.Ltmp5:
.LBB3_11:
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	pthread_mutex_unlock
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN8NArchive4NZip17CMtProgressMixer212SetRatioInfoEiPKyS3_, .Lfunc_end3-_ZN8NArchive4NZip17CMtProgressMixer212SetRatioInfoEiPKyS3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end3-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end4:
	.size	__clang_call_terminate, .Lfunc_end4-__clang_call_terminate

	.text
	.globl	_ZN8NArchive4NZip17CMtProgressMixer212SetRatioInfoEPKyS3_
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip17CMtProgressMixer212SetRatioInfoEPKyS3_,@function
_ZN8NArchive4NZip17CMtProgressMixer212SetRatioInfoEPKyS3_: # @_ZN8NArchive4NZip17CMtProgressMixer212SetRatioInfoEPKyS3_
	.cfi_startproc
# BB#0:
	movq	%rdx, %rax
	movq	%rsi, %rcx
	xorl	%esi, %esi
	movq	%rcx, %rdx
	movq	%rax, %rcx
	jmp	_ZN8NArchive4NZip17CMtProgressMixer212SetRatioInfoEiPKyS3_ # TAILCALL
.Lfunc_end5:
	.size	_ZN8NArchive4NZip17CMtProgressMixer212SetRatioInfoEPKyS3_, .Lfunc_end5-_ZN8NArchive4NZip17CMtProgressMixer212SetRatioInfoEPKyS3_
	.cfi_endproc

	.globl	_ZN8NArchive4NZip16CMtProgressMixer6CreateEP9IProgressb
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip16CMtProgressMixer6CreateEP9IProgressb,@function
_ZN8NArchive4NZip16CMtProgressMixer6CreateEP9IProgressb: # @_ZN8NArchive4NZip16CMtProgressMixer6CreateEP9IProgressb
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 48
.Lcfi43:
	.cfi_offset %rbx, -48
.Lcfi44:
	.cfi_offset %r12, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$120, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movl	$0, 8(%rbp)
	movq	$_ZTVN8NArchive4NZip17CMtProgressMixer2E+16, (%rbp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 56(%rbp)
	leaq	56(%rbp), %r12
	leaq	80(%rbp), %rdi
.Ltmp6:
	callq	CriticalSection_Init
.Ltmp7:
# BB#1:                                 # %_ZN8NArchive4NZip17CMtProgressMixer2C2Ev.exit
	movq	%rbp, 16(%rbx)
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*8(%rax)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_3
# BB#2:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB6_3:                                # %_ZN9CMyComPtrI21ICompressProgressInfoEaSEPS0_.exit
	movq	%rbp, 24(%rbx)
	movq	16(%rbx), %rbx
	testq	%r15, %r15
	je	.LBB6_5
# BB#4:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*8(%rax)
.LBB6_5:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_7
# BB#6:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB6_7:                                # %_ZN8NArchive4NZip17CMtProgressMixer26CreateEP9IProgressb.exit
	movq	%r15, 56(%rbx)
	leaq	64(%rbx), %rdx
	movq	(%r15), %rax
	movl	$IID_ICompressProgressInfo, %esi
	movq	%r15, %rdi
	callq	*(%rax)
	movb	%r14b, 72(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movq	$0, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_8:
.Ltmp8:
	movq	%rax, %rbx
	movq	64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_10
# BB#9:
	movq	(%rdi), %rax
.Ltmp9:
	callq	*16(%rax)
.Ltmp10:
.LBB6_10:                               # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit.i
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB6_12
# BB#11:
	movq	(%rdi), %rax
.Ltmp11:
	callq	*16(%rax)
.Ltmp12:
.LBB6_12:                               # %.body
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB6_13:
.Ltmp13:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN8NArchive4NZip16CMtProgressMixer6CreateEP9IProgressb, .Lfunc_end6-_ZN8NArchive4NZip16CMtProgressMixer6CreateEP9IProgressb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp6-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp9-.Ltmp7           #   Call between .Ltmp7 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 4 <<
	.long	.Ltmp12-.Ltmp9          #   Call between .Ltmp9 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin1   #     jumps to .Ltmp13
	.byte	1                       #   On action: 1
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end6-.Ltmp12     #   Call between .Ltmp12 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NZip16CMtProgressMixer12SetRatioInfoEPKyS3_
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip16CMtProgressMixer12SetRatioInfoEPKyS3_,@function
_ZN8NArchive4NZip16CMtProgressMixer12SetRatioInfoEPKyS3_: # @_ZN8NArchive4NZip16CMtProgressMixer12SetRatioInfoEPKyS3_
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 48
.Lcfi53:
	.cfi_offset %rbx, -40
.Lcfi54:
	.cfi_offset %r12, -32
.Lcfi55:
	.cfi_offset %r14, -24
.Lcfi56:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	16(%rdi), %rbx
	leaq	80(%rbx), %r14
	movq	%r14, %rdi
	callq	pthread_mutex_lock
	testq	%r12, %r12
	je	.LBB7_2
# BB#1:
	movq	(%r12), %rax
	movq	%rax, 32(%rbx)
.LBB7_2:
	testq	%r15, %r15
	je	.LBB7_4
# BB#3:
	movq	(%r15), %rax
	movq	%rax, 48(%rbx)
.LBB7_4:
	leaq	40(%rbx), %rax
	cmpb	$0, 72(%rbx)
	leaq	24(%rbx), %rcx
	cmoveq	%rax, %rcx
	movq	(%rcx), %rax
	addq	16(%rbx), %rax
	addq	8(%rcx), %rax
	movq	%rax, (%rsp)
	movq	56(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp14:
	movq	%rsp, %rsi
	callq	*48(%rax)
	movl	%eax, %ebx
.Ltmp15:
# BB#5:                                 # %_ZN8NArchive4NZip17CMtProgressMixer212SetRatioInfoEiPKyS3_.exit
	movq	%r14, %rdi
	callq	pthread_mutex_unlock
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB7_6:
.Ltmp16:
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	pthread_mutex_unlock
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZN8NArchive4NZip16CMtProgressMixer12SetRatioInfoEPKyS3_, .Lfunc_end7-_ZN8NArchive4NZip16CMtProgressMixer12SetRatioInfoEPKyS3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp14-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin2   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end7-.Ltmp15     #   Call between .Ltmp15 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NArchive4NZip15CCacheOutStream8AllocateEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip15CCacheOutStream8AllocateEv,@function
_ZN8NArchive4NZip15CCacheOutStream8AllocateEv: # @_ZN8NArchive4NZip15CCacheOutStream8AllocateEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 16
.Lcfi58:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB8_2
# BB#1:
	movl	$4194304, %edi          # imm = 0x400000
	callq	MidAlloc
	movq	%rax, 24(%rbx)
.LBB8_2:
	testq	%rax, %rax
	setne	%al
	popq	%rbx
	retq
.Lfunc_end8:
	.size	_ZN8NArchive4NZip15CCacheOutStream8AllocateEv, .Lfunc_end8-_ZN8NArchive4NZip15CCacheOutStream8AllocateEv
	.cfi_endproc

	.globl	_ZN8NArchive4NZip15CCacheOutStream4InitEP10IOutStream
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip15CCacheOutStream4InitEP10IOutStream,@function
_ZN8NArchive4NZip15CCacheOutStream4InitEP10IOutStream: # @_ZN8NArchive4NZip15CCacheOutStream4InitEP10IOutStream
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 32
.Lcfi62:
	.cfi_offset %rbx, -32
.Lcfi63:
	.cfi_offset %r14, -24
.Lcfi64:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	$0, 48(%r15)
	movq	$0, 32(%r15)
	testq	%rbx, %rbx
	je	.LBB9_2
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
.LBB9_2:
	leaq	32(%r15), %r14
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB9_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB9_4:                                # %_ZN9CMyComPtrI10IOutStreamEaSEPS0_.exit
	movq	%rbx, 16(%r15)
	movq	(%rbx), %rax
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r14, %rcx
	callq	*48(%rax)
	testl	%eax, %eax
	jne	.LBB9_8
# BB#5:
	movq	16(%r15), %rdi
	movq	(%rdi), %rax
	leaq	40(%r15), %rcx
	xorl	%esi, %esi
	movl	$2, %edx
	callq	*48(%rax)
	testl	%eax, %eax
	jne	.LBB9_8
# BB#6:
	movq	16(%r15), %rdi
	movq	32(%r15), %rsi
	movq	(%rdi), %rax
	xorl	%edx, %edx
	movq	%r14, %rcx
	callq	*48(%rax)
	testl	%eax, %eax
	jne	.LBB9_8
# BB#7:
	movups	32(%r15), %xmm0
	movups	%xmm0, 48(%r15)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 64(%r15)
	xorl	%eax, %eax
.LBB9_8:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	_ZN8NArchive4NZip15CCacheOutStream4InitEP10IOutStream, .Lfunc_end9-_ZN8NArchive4NZip15CCacheOutStream4InitEP10IOutStream
	.cfi_endproc

	.globl	_ZN8NArchive4NZip15CCacheOutStream7MyWriteEm
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip15CCacheOutStream7MyWriteEm,@function
_ZN8NArchive4NZip15CCacheOutStream7MyWriteEm: # @_ZN8NArchive4NZip15CCacheOutStream7MyWriteEm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 48
.Lcfi70:
	.cfi_offset %rbx, -48
.Lcfi71:
	.cfi_offset %r12, -40
.Lcfi72:
	.cfi_offset %r14, -32
.Lcfi73:
	.cfi_offset %r15, -24
.Lcfi74:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %rbp
	testq	%r12, %r12
	je	.LBB10_10
# BB#1:                                 # %.lr.ph
	leaq	48(%rbp), %r15
	movq	72(%rbp), %rcx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	testq	%rcx, %rcx
	je	.LBB10_13
# BB#3:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	48(%rbp), %rsi
	movq	64(%rbp), %rax
	cmpq	%rax, %rsi
	je	.LBB10_6
# BB#4:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	16(%rbp), %rdi
	movq	(%rdi), %rbx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rcx
	callq	*48(%rbx)
	testl	%eax, %eax
	jne	.LBB10_12
# BB#5:                                 # %._crit_edge
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	64(%rbp), %rsi
	movq	72(%rbp), %rcx
.LBB10_6:                               #   in Loop: Header=BB10_2 Depth=1
	andl	$4194303, %esi          # imm = 0x3FFFFF
	movl	$4194304, %ebx          # imm = 0x400000
	subq	%rsi, %rbx
	cmpq	%rcx, %rbx
	cmovaeq	%rcx, %rbx
	cmpq	%r12, %rbx
	cmovaeq	%r12, %rbx
	movq	16(%rbp), %rdi
	addq	24(%rbp), %rsi
	movq	%rbx, %rdx
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	testl	%eax, %eax
	jne	.LBB10_12
# BB#7:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	48(%rbp), %rax
	addq	%rbx, %rax
	movq	%rax, 48(%rbp)
	cmpq	%rax, 56(%rbp)
	jae	.LBB10_9
# BB#8:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	%rax, 56(%rbp)
.LBB10_9:                               # %.thread
                                        #   in Loop: Header=BB10_2 Depth=1
	addq	%rbx, 64(%rbp)
	movq	72(%rbp), %rcx
	subq	%rbx, %rcx
	movq	%rcx, 72(%rbp)
	subq	%rbx, %r12
	jne	.LBB10_2
	jmp	.LBB10_13
.LBB10_10:
	xorl	%r14d, %r14d
	jmp	.LBB10_13
.LBB10_12:
	movl	%eax, %r14d
.LBB10_13:                              # %.critedge
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_ZN8NArchive4NZip15CCacheOutStream7MyWriteEm, .Lfunc_end10-_ZN8NArchive4NZip15CCacheOutStream7MyWriteEm
	.cfi_endproc

	.globl	_ZN8NArchive4NZip15CCacheOutStream10FlushCacheEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip15CCacheOutStream10FlushCacheEv,@function
_ZN8NArchive4NZip15CCacheOutStream10FlushCacheEv: # @_ZN8NArchive4NZip15CCacheOutStream10FlushCacheEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi75:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi78:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 48
.Lcfi80:
	.cfi_offset %rbx, -48
.Lcfi81:
	.cfi_offset %r12, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	72(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB11_10
# BB#1:                                 # %.lr.ph.i
	leaq	48(%rbx), %r15
	xorl	%r14d, %r14d
	movq	%rcx, %r12
	.p2align	4, 0x90
.LBB11_2:                               # =>This Inner Loop Header: Depth=1
	testq	%rcx, %rcx
	je	.LBB11_13
# BB#3:                                 #   in Loop: Header=BB11_2 Depth=1
	movq	48(%rbx), %rsi
	movq	64(%rbx), %rax
	cmpq	%rax, %rsi
	je	.LBB11_6
# BB#4:                                 #   in Loop: Header=BB11_2 Depth=1
	movq	16(%rbx), %rdi
	movq	(%rdi), %rbp
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rcx
	callq	*48(%rbp)
	testl	%eax, %eax
	jne	.LBB11_12
# BB#5:                                 # %._crit_edge.i
                                        #   in Loop: Header=BB11_2 Depth=1
	movq	64(%rbx), %rsi
	movq	72(%rbx), %rcx
.LBB11_6:                               #   in Loop: Header=BB11_2 Depth=1
	andl	$4194303, %esi          # imm = 0x3FFFFF
	movl	$4194304, %ebp          # imm = 0x400000
	subq	%rsi, %rbp
	cmpq	%rcx, %rbp
	cmovaeq	%rcx, %rbp
	cmpq	%r12, %rbp
	cmovaeq	%r12, %rbp
	movq	16(%rbx), %rdi
	addq	24(%rbx), %rsi
	movq	%rbp, %rdx
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	testl	%eax, %eax
	jne	.LBB11_12
# BB#7:                                 #   in Loop: Header=BB11_2 Depth=1
	movq	48(%rbx), %rax
	addq	%rbp, %rax
	movq	%rax, 48(%rbx)
	cmpq	%rax, 56(%rbx)
	jae	.LBB11_9
# BB#8:                                 #   in Loop: Header=BB11_2 Depth=1
	movq	%rax, 56(%rbx)
.LBB11_9:                               # %.thread.i
                                        #   in Loop: Header=BB11_2 Depth=1
	addq	%rbp, 64(%rbx)
	movq	72(%rbx), %rcx
	subq	%rbp, %rcx
	movq	%rcx, 72(%rbx)
	subq	%rbp, %r12
	jne	.LBB11_2
	jmp	.LBB11_13
.LBB11_10:
	xorl	%r14d, %r14d
	jmp	.LBB11_13
.LBB11_12:
	movl	%eax, %r14d
.LBB11_13:                              # %_ZN8NArchive4NZip15CCacheOutStream7MyWriteEm.exit
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZN8NArchive4NZip15CCacheOutStream10FlushCacheEv, .Lfunc_end11-_ZN8NArchive4NZip15CCacheOutStream10FlushCacheEv
	.cfi_endproc

	.globl	_ZN8NArchive4NZip15CCacheOutStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip15CCacheOutStreamD2Ev,@function
_ZN8NArchive4NZip15CCacheOutStreamD2Ev: # @_ZN8NArchive4NZip15CCacheOutStreamD2Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi87:
	.cfi_def_cfa_offset 32
.Lcfi88:
	.cfi_offset %rbx, -24
.Lcfi89:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN8NArchive4NZip15CCacheOutStreamE+16, (%rbx)
.Ltmp17:
	callq	_ZN8NArchive4NZip15CCacheOutStream10FlushCacheEv
.Ltmp18:
# BB#1:
	movq	40(%rbx), %rsi
	cmpq	56(%rbx), %rsi
	je	.LBB12_3
# BB#2:
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp19:
	callq	*56(%rax)
.Ltmp20:
.LBB12_3:
	movq	32(%rbx), %rsi
	cmpq	48(%rbx), %rsi
	je	.LBB12_5
# BB#4:
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp21:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
.Ltmp22:
.LBB12_5:
	movq	24(%rbx), %rdi
.Ltmp23:
	callq	MidFree
.Ltmp24:
# BB#6:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_8
# BB#7:
	movq	(%rdi), %rax
.Ltmp29:
	callq	*16(%rax)
.Ltmp30:
.LBB12_8:                               # %_ZN9CMyComPtrI10IOutStreamED2Ev.exit4
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB12_11:
.Ltmp31:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB12_9:
.Ltmp25:
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_12
# BB#10:
	movq	(%rdi), %rax
.Ltmp26:
	callq	*16(%rax)
.Ltmp27:
.LBB12_12:                              # %_ZN9CMyComPtrI10IOutStreamED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB12_13:
.Ltmp28:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZN8NArchive4NZip15CCacheOutStreamD2Ev, .Lfunc_end12-_ZN8NArchive4NZip15CCacheOutStreamD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp17-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp24-.Ltmp17         #   Call between .Ltmp17 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin3   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp30-.Ltmp29         #   Call between .Ltmp29 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin3   #     jumps to .Ltmp31
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp26-.Ltmp30         #   Call between .Ltmp30 and .Ltmp26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin3   #     jumps to .Ltmp28
	.byte	1                       #   On action: 1
	.long	.Ltmp27-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Lfunc_end12-.Ltmp27    #   Call between .Ltmp27 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NZip15CCacheOutStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip15CCacheOutStreamD0Ev,@function
_ZN8NArchive4NZip15CCacheOutStreamD0Ev: # @_ZN8NArchive4NZip15CCacheOutStreamD0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi92:
	.cfi_def_cfa_offset 32
.Lcfi93:
	.cfi_offset %rbx, -24
.Lcfi94:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp32:
	callq	_ZN8NArchive4NZip15CCacheOutStreamD2Ev
.Ltmp33:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB13_2:
.Ltmp34:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end13:
	.size	_ZN8NArchive4NZip15CCacheOutStreamD0Ev, .Lfunc_end13-_ZN8NArchive4NZip15CCacheOutStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp32-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin4   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Lfunc_end13-.Ltmp33    #   Call between .Ltmp33 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NArchive4NZip15CCacheOutStream5WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip15CCacheOutStream5WriteEPKvjPj,@function
_ZN8NArchive4NZip15CCacheOutStream5WriteEPKvjPj: # @_ZN8NArchive4NZip15CCacheOutStream5WriteEPKvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi98:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi99:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi101:
	.cfi_def_cfa_offset 80
.Lcfi102:
	.cfi_offset %rbx, -56
.Lcfi103:
	.cfi_offset %r12, -48
.Lcfi104:
	.cfi_offset %r13, -40
.Lcfi105:
	.cfi_offset %r14, -32
.Lcfi106:
	.cfi_offset %r15, -24
.Lcfi107:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movq	%rdi, %r13
	testq	%rcx, %rcx
	je	.LBB14_2
# BB#1:
	movl	$0, (%rcx)
.LBB14_2:
	testl	%r12d, %r12d
	je	.LBB14_36
# BB#3:
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	32(%r13), %r15
	movq	72(%r13), %rax
	testq	%rax, %rax
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	je	.LBB14_4
# BB#5:
	movq	64(%r13), %rcx
	cmpq	%rcx, %r15
	jb	.LBB14_9
# BB#6:
	addq	%rax, %rcx
	cmpq	%r15, %rcx
	jae	.LBB14_7
# BB#8:
	cmpq	56(%r13), %rcx
	jae	.LBB14_10
.LBB14_9:
	movq	%r13, %rdi
	callq	_ZN8NArchive4NZip15CCacheOutStream10FlushCacheEv
	testl	%eax, %eax
	movq	%r15, %rcx
	jne	.LBB14_37
	jmp	.LBB14_10
.LBB14_4:
	movq	%r15, %rcx
	jmp	.LBB14_11
.LBB14_7:
	movq	%r15, %rcx
.LBB14_10:                              # %.thread
	movq	32(%r13), %r15
	movq	72(%r13), %rax
	testq	%rax, %rax
	jne	.LBB14_14
.LBB14_11:                              # %.thread95
	movq	56(%r13), %rdx
	cmpq	%r15, %rdx
	jae	.LBB14_12
# BB#13:
	movq	%rdx, 64(%r13)
	xorl	%eax, %eax
	movq	%rdx, %rcx
	cmpq	%r15, %rcx
	jne	.LBB14_15
	jmp	.LBB14_21
.LBB14_12:
	xorl	%eax, %eax
.LBB14_14:                              # %.thread._crit_edge
	cmpq	%r15, %rcx
	je	.LBB14_21
.LBB14_15:                              # %.preheader101
	movq	64(%r13), %rbx
	addq	%rax, %rbx
	jmp	.LBB14_16
	.p2align	4, 0x90
.LBB14_20:                              # %.thread99
                                        #   in Loop: Header=BB14_16 Depth=1
	addq	24(%r13), %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memset
	movq	%rbp, %rax
	addq	72(%r13), %rax
	movq	%rax, 72(%r13)
	movq	64(%r13), %rbx
	addq	%rax, %rbx
	movq	32(%r13), %r15
.LBB14_16:                              # %.preheader101
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_17 Depth 2
	movq	%r15, %rbp
	subq	%rbx, %rbp
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	andl	$4194303, %ebx          # imm = 0x3FFFFF
	movl	$4194304, %ecx          # imm = 0x400000
	subq	%rbx, %rcx
	cmpq	%rbp, %rcx
	cmovbeq	%rcx, %rbp
	testq	%rbp, %rbp
	jne	.LBB14_17
	jmp	.LBB14_21
	.p2align	4, 0x90
.LBB14_19:                              # %._crit_edge
                                        #   in Loop: Header=BB14_17 Depth=2
	movq	72(%r13), %rax
.LBB14_17:                              # %.preheader
                                        #   Parent Loop BB14_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$4194304, %ecx          # imm = 0x400000
	subq	%rax, %rcx
	cmpq	%rcx, %rbp
	jbe	.LBB14_20
# BB#18:                                # %.lr.ph
                                        #   in Loop: Header=BB14_17 Depth=2
	movq	%r13, %rdi
	callq	_ZN8NArchive4NZip15CCacheOutStream12MyWriteBlockEv
	testl	%eax, %eax
	je	.LBB14_19
	jmp	.LBB14_37
.LBB14_21:                              # %.thread100
	testq	%rax, %rax
	je	.LBB14_23
# BB#22:                                # %.thread100._crit_edge
	movq	64(%r13), %rcx
	jmp	.LBB14_24
.LBB14_23:
	movq	%r15, 64(%r13)
	movq	%r15, %rcx
.LBB14_24:
	movl	%r15d, %r14d
	andl	$4194303, %r14d         # imm = 0x3FFFFF
	movl	%r12d, %edx
	movl	$4194304, %ebp          # imm = 0x400000
	subq	%r14, %rbp
	cmpq	%rbp, %rdx
	cmovbq	%rdx, %rbp
	leaq	(%rax,%rcx), %rdx
	subq	%r15, %rdx
	jne	.LBB14_25
# BB#26:
	cmpq	$4194304, %rax          # imm = 0x400000
	jne	.LBB14_29
# BB#27:
	movq	%r13, %rdi
	callq	_ZN8NArchive4NZip15CCacheOutStream12MyWriteBlockEv
	testl	%eax, %eax
	jne	.LBB14_37
# BB#28:                                # %._crit_edge107
	movq	64(%r13), %rcx
.LBB14_29:
	andl	$4194303, %ecx          # imm = 0x3FFFFF
	subq	%r14, %rcx
	jbe	.LBB14_31
# BB#30:
	cmpq	%rcx, %rbp
	cmovaeq	%rcx, %rbp
.LBB14_31:
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%ebp, %eax
	addq	%rax, 72(%r13)
	jmp	.LBB14_32
.LBB14_25:
	cmpq	%rdx, %rbp
	cmovaeq	%rdx, %rbp
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB14_32:
	addq	24(%r13), %r14
	movl	%ebp, %ebx
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	movq	16(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB14_34
# BB#33:
	movl	%ebp, (%rax)
.LBB14_34:
	addq	32(%r13), %rbx
	movq	%rbx, 32(%r13)
	cmpq	%rbx, 40(%r13)
	jae	.LBB14_36
# BB#35:
	movq	%rbx, 40(%r13)
.LBB14_36:                              # %.loopexit
	xorl	%eax, %eax
.LBB14_37:                              # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	_ZN8NArchive4NZip15CCacheOutStream5WriteEPKvjPj, .Lfunc_end14-_ZN8NArchive4NZip15CCacheOutStream5WriteEPKvjPj
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip15CCacheOutStream12MyWriteBlockEv,"axG",@progbits,_ZN8NArchive4NZip15CCacheOutStream12MyWriteBlockEv,comdat
	.weak	_ZN8NArchive4NZip15CCacheOutStream12MyWriteBlockEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip15CCacheOutStream12MyWriteBlockEv,@function
_ZN8NArchive4NZip15CCacheOutStream12MyWriteBlockEv: # @_ZN8NArchive4NZip15CCacheOutStream12MyWriteBlockEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi109:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi111:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi112:
	.cfi_def_cfa_offset 48
.Lcfi113:
	.cfi_offset %rbx, -48
.Lcfi114:
	.cfi_offset %r12, -40
.Lcfi115:
	.cfi_offset %r14, -32
.Lcfi116:
	.cfi_offset %r15, -24
.Lcfi117:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	64(%rbx), %rsi
	movl	%esi, %eax
	andl	$1048575, %eax          # imm = 0xFFFFF
	movl	$1048576, %r12d         # imm = 0x100000
	subq	%rax, %r12
	je	.LBB15_10
# BB#1:                                 # %.lr.ph.i
	leaq	48(%rbx), %r15
	movq	72(%rbx), %rax
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB15_2:                               # =>This Inner Loop Header: Depth=1
	testq	%rax, %rax
	je	.LBB15_13
# BB#3:                                 #   in Loop: Header=BB15_2 Depth=1
	cmpq	%rsi, (%r15)
	je	.LBB15_6
# BB#4:                                 #   in Loop: Header=BB15_2 Depth=1
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	xorl	%edx, %edx
	movq	%r15, %rcx
	callq	*48(%rax)
	testl	%eax, %eax
	jne	.LBB15_12
# BB#5:                                 # %._crit_edge.i
                                        #   in Loop: Header=BB15_2 Depth=1
	movq	64(%rbx), %rsi
	movq	72(%rbx), %rax
.LBB15_6:                               #   in Loop: Header=BB15_2 Depth=1
	andl	$4194303, %esi          # imm = 0x3FFFFF
	movl	$4194304, %ebp          # imm = 0x400000
	subq	%rsi, %rbp
	cmpq	%rax, %rbp
	cmovaeq	%rax, %rbp
	cmpq	%r12, %rbp
	cmovaeq	%r12, %rbp
	movq	16(%rbx), %rdi
	addq	24(%rbx), %rsi
	movq	%rbp, %rdx
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	testl	%eax, %eax
	jne	.LBB15_12
# BB#7:                                 #   in Loop: Header=BB15_2 Depth=1
	movq	48(%rbx), %rax
	addq	%rbp, %rax
	movq	%rax, 48(%rbx)
	cmpq	%rax, 56(%rbx)
	jae	.LBB15_9
# BB#8:                                 #   in Loop: Header=BB15_2 Depth=1
	movq	%rax, 56(%rbx)
.LBB15_9:                               # %.thread.i
                                        #   in Loop: Header=BB15_2 Depth=1
	movq	64(%rbx), %rsi
	addq	%rbp, %rsi
	movq	%rsi, 64(%rbx)
	movq	72(%rbx), %rax
	subq	%rbp, %rax
	movq	%rax, 72(%rbx)
	subq	%rbp, %r12
	jne	.LBB15_2
	jmp	.LBB15_13
.LBB15_10:
	xorl	%r14d, %r14d
	jmp	.LBB15_13
.LBB15_12:
	movl	%eax, %r14d
.LBB15_13:                              # %_ZN8NArchive4NZip15CCacheOutStream7MyWriteEm.exit
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	_ZN8NArchive4NZip15CCacheOutStream12MyWriteBlockEv, .Lfunc_end15-_ZN8NArchive4NZip15CCacheOutStream12MyWriteBlockEv
	.cfi_endproc

	.text
	.globl	_ZN8NArchive4NZip15CCacheOutStream4SeekExjPy
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip15CCacheOutStream4SeekExjPy,@function
_ZN8NArchive4NZip15CCacheOutStream4SeekExjPy: # @_ZN8NArchive4NZip15CCacheOutStream4SeekExjPy
	.cfi_startproc
# BB#0:
	cmpl	$2, %edx
	je	.LBB16_4
# BB#1:
	cmpl	$1, %edx
	je	.LBB16_3
# BB#2:
	movl	$-2147287039, %eax      # imm = 0x80030001
	testl	%edx, %edx
	je	.LBB16_5
	jmp	.LBB16_7
.LBB16_4:
	addq	40(%rdi), %rsi
	jmp	.LBB16_5
.LBB16_3:
	addq	32(%rdi), %rsi
.LBB16_5:
	movq	%rsi, 32(%rdi)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.LBB16_7
# BB#6:
	movq	%rsi, (%rcx)
.LBB16_7:
	retq
.Lfunc_end16:
	.size	_ZN8NArchive4NZip15CCacheOutStream4SeekExjPy, .Lfunc_end16-_ZN8NArchive4NZip15CCacheOutStream4SeekExjPy
	.cfi_endproc

	.globl	_ZN8NArchive4NZip15CCacheOutStream7SetSizeEy
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip15CCacheOutStream7SetSizeEy,@function
_ZN8NArchive4NZip15CCacheOutStream7SetSizeEy: # @_ZN8NArchive4NZip15CCacheOutStream7SetSizeEy
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi118:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi119:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi120:
	.cfi_def_cfa_offset 32
.Lcfi121:
	.cfi_offset %rbx, -24
.Lcfi122:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	%rbx, 40(%r14)
	cmpq	%rbx, 56(%r14)
	jbe	.LBB17_3
# BB#1:
	movq	16(%r14), %rdi
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	callq	*56(%rax)
	testl	%eax, %eax
	jne	.LBB17_8
# BB#2:
	movq	%rbx, 56(%r14)
.LBB17_3:
	movq	64(%r14), %rcx
	cmpq	%rbx, %rcx
	jae	.LBB17_5
# BB#4:                                 # %._crit_edge
	movq	72(%r14), %rdx
	jmp	.LBB17_6
.LBB17_5:
	movq	$0, 72(%r14)
	movq	%rbx, 64(%r14)
	xorl	%edx, %edx
	movq	%rbx, %rcx
.LBB17_6:
	addq	%rcx, %rdx
	xorl	%eax, %eax
	cmpq	%rbx, %rdx
	jbe	.LBB17_8
# BB#7:
	subq	%rcx, %rbx
	movq	%rbx, 72(%r14)
.LBB17_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end17:
	.size	_ZN8NArchive4NZip15CCacheOutStream7SetSizeEy, .Lfunc_end17-_ZN8NArchive4NZip15CCacheOutStream7SetSizeEy
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI18_0:
	.zero	16
	.text
	.globl	_ZN8NArchive4NZip6UpdateERK13CObjectVectorINS0_7CItemExEERKS1_INS0_11CUpdateItemEEP20ISequentialOutStreamPNS0_10CInArchiveEPNS0_22CCompressionMethodModeEP22IArchiveUpdateCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip6UpdateERK13CObjectVectorINS0_7CItemExEERKS1_INS0_11CUpdateItemEEP20ISequentialOutStreamPNS0_10CInArchiveEPNS0_22CCompressionMethodModeEP22IArchiveUpdateCallback,@function
_ZN8NArchive4NZip6UpdateERK13CObjectVectorINS0_7CItemExEERKS1_INS0_11CUpdateItemEEP20ISequentialOutStreamPNS0_10CInArchiveEPNS0_22CCompressionMethodModeEP22IArchiveUpdateCallback: # @_ZN8NArchive4NZip6UpdateERK13CObjectVectorINS0_7CItemExEERKS1_INS0_11CUpdateItemEEP20ISequentialOutStreamPNS0_10CInArchiveEPNS0_22CCompressionMethodModeEP22IArchiveUpdateCallback
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi123:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi124:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi125:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi126:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi127:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi128:
	.cfi_def_cfa_offset 56
	subq	$2552, %rsp             # imm = 0x9F8
.Lcfi129:
	.cfi_def_cfa_offset 2608
.Lcfi130:
	.cfi_offset %rbx, -56
.Lcfi131:
	.cfi_offset %r12, -48
.Lcfi132:
	.cfi_offset %r13, -40
.Lcfi133:
	.cfi_offset %r14, -32
.Lcfi134:
	.cfi_offset %r15, -24
.Lcfi135:
	.cfi_offset %rbp, -16
	movq	%r9, %r13
	movq	%r8, %rbx
	movq	%rcx, %rbp
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	movq	$0, 1792(%rsp)
	movq	(%rdx), %rax
.Ltmp35:
	leaq	1792(%rsp), %rcx
	movl	$IID_IOutStream, %esi
	movq	%rdx, %rdi
	movq	%rcx, %rdx
	callq	*(%rax)
.Ltmp36:
# BB#1:
	cmpq	$0, 1792(%rsp)
	je	.LBB18_17
# BB#2:
.Ltmp37:
	movl	$80, %edi
	callq	_Znwm
	movq	%rax, %r12
.Ltmp38:
# BB#3:
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	$_ZTVN8NArchive4NZip15CCacheOutStreamE+16, (%r12)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 16(%r12)
	movl	$1, 8(%r12)
.Ltmp40:
	movl	$4194304, %edi          # imm = 0x400000
	callq	MidAlloc
.Ltmp41:
# BB#4:
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.LBB18_18
# BB#5:                                 # %.thread
	movq	1792(%rsp), %rbp
	movq	$0, 48(%r12)
	movq	$0, 32(%r12)
	testq	%rbp, %rbp
	je	.LBB18_7
# BB#6:
	movq	(%rbp), %rax
.Ltmp42:
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp43:
.LBB18_7:                               # %.noexc46
	leaq	16(%r12), %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB18_9
# BB#8:
	movq	(%rdi), %rax
.Ltmp44:
	callq	*16(%rax)
.Ltmp45:
.LBB18_9:                               # %_ZN9CMyComPtrI10IOutStreamEaSEPS0_.exit.i
	leaq	32(%r12), %r14
	movq	%rbp, (%rbx)
	movq	(%rbp), %rax
.Ltmp46:
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rbp, %rdi
	movq	%r14, %rcx
	callq	*48(%rax)
	movl	%eax, %r15d
.Ltmp47:
# BB#10:                                # %.noexc48
	testl	%r15d, %r15d
	jne	.LBB18_16
# BB#11:
	movq	16(%r12), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rcx
	addq	$40, %rcx
.Ltmp48:
	xorl	%esi, %esi
	movl	$2, %edx
	callq	*48(%rax)
	movl	%eax, %r15d
.Ltmp49:
# BB#12:                                # %.noexc49
	testl	%r15d, %r15d
	jne	.LBB18_16
# BB#13:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movq	(%r14), %rsi
.Ltmp50:
	xorl	%edx, %edx
	movq	%r14, %rcx
	callq	*48(%rax)
	movl	%eax, %r15d
.Ltmp51:
# BB#14:                                # %.noexc50
	testl	%r15d, %r15d
	jne	.LBB18_16
# BB#15:
	movdqu	32(%r12), %xmm0
	movdqu	%xmm0, 48(%r12)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 64(%r12)
	xorl	%r15d, %r15d
.LBB18_16:                              # %_ZN8NArchive4NZip15CCacheOutStream4InitEP10IOutStream.exit
	xorl	%ebx, %ebx
	testl	%r15d, %r15d
	setne	%bl
	jmp	.LBB18_19
.LBB18_17:
	movl	$-2147467263, %r15d     # imm = 0x80004001
	jmp	.LBB18_23
.LBB18_18:
	movl	$-2147024882, %r15d     # imm = 0x8007000E
	movl	$1, %ebx
.LBB18_19:
	movq	1792(%rsp), %rdi
	testq	%rdi, %rdi
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB18_21
# BB#20:
	movq	(%rdi), %rax
.Ltmp55:
	callq	*16(%rax)
.Ltmp56:
.LBB18_21:                              # %_ZN9CMyComPtrI10IOutStreamED2Ev.exit52
	testl	%ebx, %ebx
	je	.LBB18_24
.LBB18_22:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*16(%rax)
.LBB18_23:                              # %_ZN9CMyComPtrI10IOutStreamED2Ev.exit41
	movl	%r15d, %eax
	addq	$2552, %rsp             # imm = 0x9F8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB18_24:
	testq	%rbp, %rbp
	je	.LBB18_28
# BB#25:
	movl	$-2147467263, %r15d     # imm = 0x80004001
	cmpq	$0, 88(%rbp)
	jne	.LBB18_22
# BB#26:
	cmpq	$0, 96(%rbp)
	jne	.LBB18_22
# BB#27:
	cmpb	$0, 137(%rbp)
	je	.LBB18_22
.LBB18_28:
	movq	$0, 224(%rsp)
	movq	$0, 240(%rsp)
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 192(%rsp)
	movl	$0, 208(%rsp)
.Ltmp58:
	leaq	192(%rsp), %rdi
	movq	%r12, %rsi
	movq	%r12, 88(%rsp)          # 8-byte Spill
	callq	_ZN8NArchive4NZip11COutArchive6CreateEP10IOutStream
.Ltmp59:
# BB#29:
	movq	%r13, 48(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	testq	%rbp, %rbp
	je	.LBB18_31
# BB#30:
	movq	%rax, 96(%rsp)          # 8-byte Spill
.Ltmp61:
	movq	%rbp, %rdi
	callq	_ZN8NArchive4NZip10CInArchive12CreateStreamEv
.Ltmp62:
.LBB18_31:                              # %_ZN9CMyComPtrI9IInStreamE6AttachEPS0_.exit
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	112(%rbp), %rax
	testq	%rbp, %rbp
	cmoveq	%rbp, %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	$0, 16(%rsp)
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	12(%rax), %eax
	testl	%eax, %eax
	jle	.LBB18_41
# BB#32:                                # %.lr.ph793.i
	leaq	1780(%rsp), %r12
	xorl	%esi, %esi
	leaq	1600(%rsp), %r15
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB18_33:                              # =>This Inner Loop Header: Depth=1
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rcx
	movq	(%rcx,%r14,8), %rcx
	cmpb	$0, (%rcx)
	je	.LBB18_35
# BB#34:                                #   in Loop: Header=BB18_33 Depth=1
	movq	24(%rcx), %rcx
	addq	%rcx, %rsi
	movq	%rsi, 16(%rsp)
	addq	%rcx, %r13
	incq	%rbx
	jmp	.LBB18_40
	.p2align	4, 0x90
.LBB18_35:                              #   in Loop: Header=BB18_33 Depth=1
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	16(%rax), %rax
	movslq	8(%rcx), %rcx
	movq	(%rax,%rcx,8), %rbp
.Ltmp63:
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	_ZN8NArchive4NZip5CItemC2ERKS1_
.Ltmp64:
# BB#36:                                # %.noexc59
                                        #   in Loop: Header=BB18_33 Depth=1
	movzwl	184(%rbp), %eax
	movw	%ax, 4(%r12)
	movl	180(%rbp), %eax
	movl	%eax, (%r12)
.Ltmp65:
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%r15, %rsi
	callq	_ZN8NArchive4NZip10CInArchive28ReadLocalItemAfterCdItemFullERNS0_7CItemExE
.Ltmp66:
# BB#37:                                #   in Loop: Header=BB18_33 Depth=1
	testl	%eax, %eax
	jne	.LBB18_53
# BB#38:                                # %.critedge663.i
                                        #   in Loop: Header=BB18_33 Depth=1
	movzwl	1784(%rsp), %eax
	addl	1780(%rsp), %eax
	addq	1616(%rsp), %rax
	movzwl	1602(%rsp), %ecx
	addl	%ecx, %ecx
	andl	$16, %ecx
	addq	16(%rsp), %rax
	addq	%rcx, %rax
	movq	%rax, 16(%rsp)
.Ltmp72:
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
.Ltmp73:
# BB#39:                                # %.noexc60
                                        #   in Loop: Header=BB18_33 Depth=1
	movq	16(%rsp), %rsi
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	12(%rax), %eax
.LBB18_40:                              #   in Loop: Header=BB18_33 Depth=1
	addq	$68, %rsi
	movq	%rsi, 16(%rsp)
	incq	%r14
	movslq	%eax, %rcx
	cmpq	%rcx, %r14
	jl	.LBB18_33
	jmp	.LBB18_42
.LBB18_41:
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
.LBB18_42:                              # %._crit_edge.i
	movq	152(%rsp), %rax         # 8-byte Reload
	testq	%rax, %rax
	movq	40(%rsp), %r15          # 8-byte Reload
	je	.LBB18_44
# BB#43:
	addq	8(%rax), %rsi
	movq	%rsi, 16(%rsp)
.LBB18_44:
	incq	%rsi
	movq	%rsi, 16(%rsp)
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp75:
	callq	*40(%rax)
.Ltmp76:
# BB#45:                                # %.noexc62
.Ltmp77:
	leaq	2376(%rsp), %rdi
	movq	%r15, %rsi
	callq	_ZN8NArchive4NZip10CAddCommonC1ERKNS0_22CCompressionMethodModeE
.Ltmp78:
# BB#46:                                # %.noexc63
	movq	$0, 16(%rsp)
	movl	80(%r15), %eax
	cmpl	$1024, %eax             # imm = 0x400
	movl	$1024, %ebp             # imm = 0x400
	cmovbl	%eax, %ebp
.Ltmp79:
	leaq	1488(%rsp), %rdi
	callq	_ZN8NArchive4NZip22CCompressionMethodModeC2Ev
.Ltmp80:
# BB#47:
.Ltmp82:
	leaq	1488(%rsp), %rdi
	movq	%r15, %rsi
	callq	_ZN8NArchive4NZip22CCompressionMethodModeaSERKS1_
.Ltmp83:
# BB#48:
	cmpl	$1, %ebp
	seta	%al
	cmpq	$1, %rbx
	seta	%cl
	andb	%al, %cl
	je	.LBB18_58
# BB#49:
	movq	16(%r15), %rax
	movb	(%rax), %al
	cmpb	$14, %al
	je	.LBB18_56
# BB#50:
	cmpb	$12, %al
	je	.LBB18_55
# BB#51:
	testb	%al, %al
	jne	.LBB18_58
# BB#52:                                # %.thread.i
	movb	84(%r15), %cl
	jmp	.LBB18_58
.LBB18_53:
.Ltmp70:
	leaq	1600(%rsp), %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
.Ltmp71:
# BB#54:                                # %.noexc61
	movl	$-2147467263, %r15d     # imm = 0x80004001
	jmp	.LBB18_315
.LBB18_55:
	xorl	%edx, %edx
	movq	%r13, %rax
	divq	%rbx
	movl	68(%r15), %ecx
	testq	%rcx, %rcx
	movl	$1, %esi
	cmovneq	%rcx, %rsi
	xorl	%edx, %edx
	divq	%rsi
	cmpq	$32, %rax
	movl	$32, %ecx
	cmovbl	%eax, %ecx
	testl	%ecx, %ecx
	movl	$1, %esi
	cmovnel	%ecx, %esi
	xorl	%edx, %edx
	movl	%ebp, %eax
	divl	%esi
	movl	%esi, 1568(%rsp)
	movl	%eax, %ebp
	jmp	.LBB18_57
.LBB18_56:
	xorl	%ecx, %ecx
	cmpl	$0, 48(%r15)
	setne	%cl
	leal	1(%rcx), %eax
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shrl	%cl, %ebp
	movl	%eax, 1568(%rsp)
.LBB18_57:                              # %.thread622.i
	cmpl	$1, %ebp
	seta	%cl
.LBB18_58:                              # %.thread622.i
	movq	48(%rsp), %rbx          # 8-byte Reload
	testb	%cl, %cl
	je	.LBB18_223
# BB#59:
	movl	%ebp, 24(%rsp)          # 4-byte Spill
	movb	$0, 824(%rsp)
	leaq	736(%rsp), %rdi
	xorl	%esi, %esi
	callq	pthread_mutex_init
	leaq	776(%rsp), %r14
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	pthread_cond_init
	movb	$0, 728(%rsp)
	leaq	640(%rsp), %rdi
	xorl	%esi, %esi
	callq	pthread_mutex_init
	leaq	680(%rsp), %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	pthread_cond_init
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 168(%rsp)
	movq	$8, 184(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip5CItemEE+16, 160(%rsp)
.Ltmp85:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp86:
# BB#60:                                # %_ZN9CMyComPtrI21ICompressProgressInfoEC2EPS0_.exit.i
	movq	$_ZTVN8NArchive4NZip16CMtProgressMixerE+16, (%r13)
	movq	$0, 24(%r13)
	movl	$1, 8(%r13)
.Ltmp88:
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NArchive4NZip16CMtProgressMixer6CreateEP9IProgressb
.Ltmp89:
# BB#61:
	movq	$0, 512(%rsp)
	leaq	520(%rsp), %rbx
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 528(%rsp)
	movq	$8, 544(%rsp)
	movq	$_ZTV13CRecordVectorIyE+16, 520(%rsp)
	leaq	552(%rsp), %rbp
	movdqu	%xmm0, 560(%rsp)
	movq	$8, 576(%rsp)
	movq	$_ZTV13CRecordVectorIyE+16, 552(%rsp)
	leaq	600(%rsp), %rdi
.Ltmp91:
	callq	CriticalSection_Init
.Ltmp92:
# BB#62:                                # %_ZN24CMtCompressProgressMixerC2Ev.exit.i
	movq	24(%r13), %rdx
.Ltmp101:
	leaq	512(%rsp), %rdi
	movl	24(%rsp), %esi          # 4-byte Reload
	callq	_ZN24CMtCompressProgressMixer4InitEiP21ICompressProgressInfo
.Ltmp102:
# BB#63:
	movq	$0, 424(%rsp)
	movq	$65536, 432(%rsp)       # imm = 0x10000
	movq	$0, 440(%rsp)
	leaq	448(%rsp), %rdi
.Ltmp104:
	callq	CriticalSection_Init
.Ltmp105:
# BB#64:
	movq	$_ZTVN8NWindows16NSynchronization14CSemaphoreWFMOE+16, 488(%rsp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 496(%rsp)
	leaq	424(%rsp), %rdi
	movq	%rdi, 384(%rsp)
	movdqu	%xmm0, 400(%rsp)
	movq	$8, 416(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE+16, 392(%rsp)
	movdqu	%xmm0, 328(%rsp)
	movq	$8, 344(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip11CThreadInfoEE+16, 320(%rsp)
	movdqu	%xmm0, 360(%rsp)
	movq	$8, 376(%rsp)
	movq	$_ZTV13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE+16, 352(%rsp)
	movdqu	%xmm0, 296(%rsp)
	movq	$4, 312(%rsp)
	movq	$_ZTV13CRecordVectorIiE+16, 288(%rsp)
	movl	24(%rsp), %edx          # 4-byte Reload
	shlq	$9, %rdx
.Ltmp110:
	leaq	640(%rsp), %rsi
	xorl	%ecx, %ecx
	callq	_ZN18CMemBlockManagerMt19AllocateSpaceAlwaysEPN8NWindows16NSynchronization8CSynchroEmm
	movl	%eax, %r15d
.Ltmp111:
# BB#65:
	testl	%r15d, %r15d
	movl	$-2147467263, %eax      # imm = 0x80004001
	cmovnel	%r15d, %eax
	jne	.LBB18_292
# BB#66:                                # %.preheader675.i
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	56(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 12(%rax)
	movq	%r13, (%rsp)            # 8-byte Spill
	jle	.LBB18_73
# BB#67:                                # %.lr.ph789.i
	leaq	840(%rsp), %rbx
	xorl	%ebp, %ebp
	leaq	392(%rsp), %r14
	movl	$8, %eax
	movd	%rax, %xmm0
	movdqa	%xmm0, 64(%rsp)         # 16-byte Spill
	leaq	832(%rsp), %r12
	.p2align	4, 0x90
.LBB18_68:                              # =>This Inner Loop Header: Depth=1
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%rbx)
	movq	$_ZTV13CRecordVectorIPvE+16, 832(%rsp)
	movdqa	64(%rsp), %xmm0         # 16-byte Reload
	movdqu	%xmm0, 856(%rsp)
	movb	$1, 872(%rsp)
	movb	$0, 904(%rsp)
	movb	$0, 905(%rsp)
.Ltmp113:
	movl	$80, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp114:
# BB#69:                                # %.noexc518.i
                                        #   in Loop: Header=BB18_68 Depth=1
.Ltmp115:
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	_ZN13CRecordVectorIPvEC2ERKS1_
.Ltmp116:
# BB#70:                                #   in Loop: Header=BB18_68 Depth=1
	movq	864(%rsp), %rax
	movq	%rax, 32(%r15)
	movzbl	872(%rsp), %eax
	movb	%al, 40(%r15)
	movdqu	40(%rbx), %xmm0
	movups	50(%rbx), %xmm1
	movups	%xmm1, 58(%r15)
	movdqu	%xmm0, 48(%r15)
.Ltmp118:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp119:
# BB#71:                                #   in Loop: Header=BB18_68 Depth=1
	movq	408(%rsp), %rax
	movslq	404(%rsp), %rcx
	movq	%r15, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 404(%rsp)
.Ltmp123:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp124:
# BB#72:                                # %_ZN10CMemBlocksD2Ev.exit.i
                                        #   in Loop: Header=BB18_68 Depth=1
	incl	%ebp
	movq	56(%rsp), %rax          # 8-byte Reload
	cmpl	12(%rax), %ebp
	jl	.LBB18_68
.LBB18_73:                              # %.preheader674.i
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB18_98
# BB#74:                                # %.lr.ph787.i
	xorl	%ebp, %ebp
	leaq	1792(%rsp), %rbx
	leaq	1488(%rsp), %r15
	leaq	320(%rsp), %r12
	.p2align	4, 0x90
.LBB18_75:                              # =>This Inner Loop Header: Depth=1
.Ltmp126:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	_ZN8NArchive4NZip11CThreadInfoC2ERKNS0_22CCompressionMethodModeE
.Ltmp127:
# BB#76:                                #   in Loop: Header=BB18_75 Depth=1
.Ltmp128:
	movl	$408, %edi              # imm = 0x198
	callq	_Znwm
	movq	%rax, %r14
.Ltmp129:
# BB#77:                                # %.noexc525.i
                                        #   in Loop: Header=BB18_75 Depth=1
.Ltmp130:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NArchive4NZip11CThreadInfoC2ERKS1_
.Ltmp131:
# BB#78:                                #   in Loop: Header=BB18_75 Depth=1
.Ltmp133:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp134:
# BB#79:                                #   in Loop: Header=BB18_75 Depth=1
	movq	336(%rsp), %rax
	movslq	332(%rsp), %rcx
	movq	%r14, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 332(%rsp)
.Ltmp138:
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NZip11CThreadInfoD2Ev
.Ltmp139:
# BB#80:                                #   in Loop: Header=BB18_75 Depth=1
	incl	%ebp
	cmpl	24(%rsp), %ebp          # 4-byte Folded Reload
	jb	.LBB18_75
# BB#81:                                # %.lr.ph785.i
	xorl	%r13d, %r13d
	movq	_ZTV13COutMemStream+24(%rip), %r14
	movl	$8, %eax
	movd	%rax, %xmm0
	movdqa	%xmm0, 64(%rsp)         # 16-byte Spill
	leaq	736(%rsp), %r12
.LBB18_82:                              # =>This Inner Loop Header: Depth=1
	movq	336(%rsp), %rax
	movslq	%r13d, %rcx
	movq	(%rax,%rcx,8), %rbx
	cmpl	$0, 16(%rbx)
	jne	.LBB18_85
# BB#83:                                # %_ZN8NWindows16NSynchronization15CAutoResetEvent18CreateIfNotCreatedEv.exit.i.i
                                        #   in Loop: Header=BB18_82 Depth=1
	leaq	16(%rbx), %rdi
.Ltmp141:
	callq	AutoResetEvent_CreateNotSignaled
	movl	%eax, %r15d
.Ltmp142:
# BB#84:                                # %.noexc529.i
                                        #   in Loop: Header=BB18_82 Depth=1
	testl	%r15d, %r15d
	jne	.LBB18_291
.LBB18_85:                              # %_ZN8NWindows16NSynchronization15CAutoResetEvent18CreateIfNotCreatedEv.exit.thread.i.i
                                        #   in Loop: Header=BB18_82 Depth=1
	cmpq	$0, 128(%rbx)
	jne	.LBB18_87
# BB#86:                                #   in Loop: Header=BB18_82 Depth=1
	movq	%r12, 128(%rbx)
	movw	$0, 136(%rbx)
.LBB18_87:                              #   in Loop: Header=BB18_82 Depth=1
.Ltmp143:
	movl	$168, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp144:
# BB#88:                                #   in Loop: Header=BB18_82 Depth=1
	movl	$0, 8(%rbp)
	movq	$_ZTV13COutMemStream+16, (%rbp)
	leaq	424(%rsp), %rax
	movq	%rax, 16(%rbp)
	movq	$_ZTVN8NWindows16NSynchronization19CAutoResetEventWFMOE+16, 48(%rbp)
	movq	$_ZTVN8NWindows16NSynchronization19CAutoResetEventWFMOE+16, 72(%rbp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 112(%rbp)
	movq	$_ZTV13CRecordVectorIPvE+16, 104(%rbp)
	movaps	64(%rsp), %xmm1         # 16-byte Reload
	movups	%xmm1, 128(%rbp)
	movb	$1, 144(%rbp)
	movdqu	%xmm0, 152(%rbp)
	movq	%rbp, 168(%rbx)
	leaq	640(%rsp), %rax
	movq	%rax, 56(%rbp)
	movb	$0, 64(%rbp)
	movb	$0, 65(%rbp)
	movq	%rax, 80(%rbp)
	movw	$0, 88(%rbp)
.Ltmp145:
	movq	%rbp, %rdi
	callq	*%r14
.Ltmp146:
# BB#89:                                # %.noexc530.i
                                        #   in Loop: Header=BB18_82 Depth=1
	movq	176(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB18_91
# BB#90:                                #   in Loop: Header=BB18_82 Depth=1
	movq	(%rdi), %rax
.Ltmp147:
	callq	*16(%rax)
.Ltmp148:
.LBB18_91:                              #   in Loop: Header=BB18_82 Depth=1
	movq	%rbp, 176(%rbx)
	movb	$1, 400(%rbx)
.Ltmp149:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp150:
# BB#92:                                #   in Loop: Header=BB18_82 Depth=1
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 16(%rbp)
	movdqa	%xmm0, (%rbp)
	movq	$_ZTV19CMtCompressProgress+16, (%rbp)
	movq	%rbp, 152(%rbx)
.Ltmp151:
	movq	%rbp, %rdi
	callq	*_ZTV19CMtCompressProgress+24(%rip)
.Ltmp152:
# BB#93:                                # %.noexc532.i
                                        #   in Loop: Header=BB18_82 Depth=1
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB18_95
# BB#94:                                #   in Loop: Header=BB18_82 Depth=1
	movq	(%rdi), %rax
.Ltmp153:
	callq	*16(%rax)
.Ltmp154:
.LBB18_95:                              #   in Loop: Header=BB18_82 Depth=1
	movq	%rbp, 160(%rbx)
	movq	152(%rbx), %rax
	leaq	512(%rsp), %rcx
	movq	%rcx, 16(%rax)
	movl	%r13d, 24(%rax)
.Ltmp155:
	movl	$_ZN8NArchive4NZipL11CoderThreadEPv, %esi
	movq	%rbx, %rdi
	movq	%rbx, %rdx
	callq	Thread_Create
	movl	%eax, %r15d
.Ltmp156:
# BB#96:                                # %_ZN8NArchive4NZip11CThreadInfo12CreateThreadEv.exit.i
                                        #   in Loop: Header=BB18_82 Depth=1
	testl	%r15d, %r15d
	jne	.LBB18_291
# BB#97:                                #   in Loop: Header=BB18_82 Depth=1
	incl	%r13d
	cmpl	24(%rsp), %r13d         # 4-byte Folded Reload
	jb	.LBB18_82
.LBB18_98:                              # %.thread633.preheader.i
	leaq	1136(%rsp), %rsi
	leaq	1328(%rsp), %rbx
	xorl	%r12d, %r12d
	movl	$-1, 116(%rsp)          # 4-byte Folded Spill
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movq	32(%rsp), %r14          # 8-byte Reload
	movl	8(%rsp), %r15d          # 4-byte Reload
.LBB18_99:                              # %.thread633.outer.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_140 Depth 2
                                        #       Child Loop BB18_143 Depth 3
                                        #       Child Loop BB18_150 Depth 3
	movslq	%r12d, %r12
	movq	16(%rsp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB18_140
.LBB18_113:                             #   in Loop: Header=BB18_140 Depth=2
	incq	%r12
	jmp	.LBB18_140
.LBB18_100:                             #   in Loop: Header=BB18_140 Depth=2
	movl	%edx, 144(%rsp)         # 4-byte Spill
	movb	$0, 400(%rbx)
	movq	104(%rsp), %r14
	testq	%r14, %r14
	je	.LBB18_102
# BB#101:                               #   in Loop: Header=BB18_140 Depth=2
	movq	(%r14), %rax
.Ltmp270:
	movq	%r14, %rdi
	callq	*8(%rax)
.Ltmp271:
.LBB18_102:                             # %.noexc537.i
                                        #   in Loop: Header=BB18_140 Depth=2
	movq	184(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB18_104
# BB#103:                               #   in Loop: Header=BB18_140 Depth=2
	movq	(%rdi), %rax
.Ltmp272:
	callq	*16(%rax)
.Ltmp273:
.LBB18_104:                             #   in Loop: Header=BB18_140 Depth=2
	movq	%r14, 184(%rbx)
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_107
# BB#105:                               #   in Loop: Header=BB18_140 Depth=2
	movq	(%rdi), %rax
.Ltmp274:
	callq	*16(%rax)
.Ltmp275:
# BB#106:                               # %.noexc539.i
                                        #   in Loop: Header=BB18_140 Depth=2
	movq	$0, 104(%rsp)
.LBB18_107:                             # %_ZN9CMyComPtrI19ISequentialInStreamE7ReleaseEv.exit.i
                                        #   in Loop: Header=BB18_140 Depth=2
	movq	168(%rbx), %rdi
.Ltmp276:
	callq	_ZN13COutMemStream4InitEv
.Ltmp277:
# BB#108:                               #   in Loop: Header=BB18_140 Depth=2
	movq	152(%rbx), %rax
	movq	16(%rax), %rdi
	movl	24(%rax), %esi
.Ltmp278:
	callq	_ZN24CMtCompressProgressMixer6ReinitEi
.Ltmp279:
# BB#109:                               # %_ZN19CMtCompressProgress6ReinitEv.exit.i
                                        #   in Loop: Header=BB18_140 Depth=2
	leaq	16(%rbx), %rdi
.Ltmp280:
	callq	Event_Set
.Ltmp281:
# BB#110:                               # %_ZN8NWindows16NSynchronization10CBaseEvent3SetEv.exit.i
                                        #   in Loop: Header=BB18_140 Depth=2
	movl	144(%rsp), %eax         # 4-byte Reload
	movl	%eax, 404(%rbx)
.Ltmp282:
	leaq	352(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp283:
# BB#111:                               #   in Loop: Header=BB18_140 Depth=2
	addq	$120, %rbx
	movq	368(%rsp), %rax
	movslq	364(%rsp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 364(%rsp)
.Ltmp284:
	leaq	288(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp285:
# BB#112:                               # %_ZN13CRecordVectorIiE3AddEi.exit.i
                                        #   in Loop: Header=BB18_140 Depth=2
	movq	304(%rsp), %rax
	movslq	300(%rsp), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	300(%rsp)
	jmp	.LBB18_135
.LBB18_114:                             #   in Loop: Header=BB18_140 Depth=2
	movq	%rax, 1136(%rsp)
	movb	$0, (%rax)
	movl	$4, 1148(%rsp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 24(%rbx)
	movq	$8, 1176(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 1152(%rsp)
	movdqu	%xmm0, 96(%rbx)
	movq	$8, 1248(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 1224(%rsp)
	movq	$_ZTV7CBufferIhE+16, 1256(%rsp)
	movdqu	%xmm0, 128(%rbx)
	movb	$0, 146(%rbx)
	movw	$0, 144(%rbx)
	cmpb	$0, 1(%rbp)
	je	.LBB18_117
# BB#115:                               #   in Loop: Header=BB18_140 Depth=2
	movl	$14, %r12d
	cmpb	$0, 2(%rbp)
	je	.LBB18_123
.LBB18_116:                             #   in Loop: Header=BB18_140 Depth=2
	movq	32(%rsp), %r14          # 8-byte Reload
	jmp	.LBB18_138
.LBB18_117:                             #   in Loop: Header=BB18_140 Depth=2
	movl	%r15d, %r12d
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	16(%rax), %rax
	movslq	8(%rbp), %rcx
	movq	(%rax,%rcx,8), %rbx
.Ltmp258:
	leaq	1104(%rsp), %r15
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NArchive4NZip5CItemaSERKS1_
.Ltmp259:
# BB#118:                               #   in Loop: Header=BB18_140 Depth=2
	movzwl	184(%rbx), %eax
	leaq	1136(%rsp), %rcx
	movw	%ax, 152(%rcx)
	movl	180(%rbx), %eax
	movl	%eax, 148(%rcx)
.Ltmp260:
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%r15, %rsi
	callq	_ZN8NArchive4NZip10CInArchive28ReadLocalItemAfterCdItemFullERNS0_7CItemExE
.Ltmp261:
# BB#119:                               #   in Loop: Header=BB18_140 Depth=2
	testl	%eax, %eax
	je	.LBB18_121
# BB#120:                               #   in Loop: Header=BB18_140 Depth=2
	movl	$-2147467263, %r15d     # imm = 0x80004001
	movl	$1, %r12d
	movq	32(%rsp), %r14          # 8-byte Reload
	jmp	.LBB18_138
.LBB18_121:                             #   in Loop: Header=BB18_140 Depth=2
.Ltmp262:
	leaq	1104(%rsp), %rdi
	callq	_ZNK8NArchive4NZip5CItem5IsDirEv
.Ltmp263:
# BB#122:                               #   in Loop: Header=BB18_140 Depth=2
	testb	%al, %al
	movl	%r12d, %r15d
	movl	$14, %r12d
	jne	.LBB18_116
.LBB18_123:                             #   in Loop: Header=BB18_140 Depth=2
	movq	$0, 104(%rsp)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	16(%rax), %rbx
	addq	$80, %rbx
	movq	%rbx, %rdi
	callq	pthread_mutex_lock
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	12(%rbp), %esi
.Ltmp265:
	leaq	104(%rsp), %rdx
	callq	*72(%rax)
.Ltmp266:
# BB#124:                               #   in Loop: Header=BB18_140 Depth=2
	testl	%eax, %eax
	je	.LBB18_129
# BB#125:                               #   in Loop: Header=BB18_140 Depth=2
	cmpl	$1, %eax
	jne	.LBB18_132
# BB#126:                               #   in Loop: Header=BB18_140 Depth=2
	movq	24(%rbp), %rax
	movq	8(%rsp), %r12           # 8-byte Reload
	leaq	26(%r12,%rax), %r12
	movq	(%rsp), %rax            # 8-byte Reload
	movq	16(%rax), %rbp
	leaq	80(%rbp), %rdi
	movq	%rdi, 144(%rsp)         # 8-byte Spill
	callq	pthread_mutex_lock
	movq	$0, 48(%rbp)
	movq	$0, 32(%rbp)
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movq	%r12, 16(%rbp)
	movq	144(%rsp), %rdi         # 8-byte Reload
	callq	pthread_mutex_unlock
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp287:
	xorl	%esi, %esi
	callq	*80(%rax)
	movl	%eax, %ebp
.Ltmp288:
# BB#127:                               #   in Loop: Header=BB18_140 Depth=2
	testl	%ebp, %ebp
	je	.LBB18_133
# BB#128:                               #   in Loop: Header=BB18_140 Depth=2
	movq	32(%rsp), %r14          # 8-byte Reload
	jmp	.LBB18_131
.LBB18_129:                             #   in Loop: Header=BB18_140 Depth=2
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp268:
	xorl	%esi, %esi
	callq	*80(%rax)
	movl	%eax, %ebp
.Ltmp269:
# BB#130:                               #   in Loop: Header=BB18_140 Depth=2
	testl	%ebp, %ebp
	movq	32(%rsp), %r14          # 8-byte Reload
	je	.LBB18_148
.LBB18_131:                             #   in Loop: Header=BB18_140 Depth=2
	movq	%rbx, %rdi
	callq	pthread_mutex_unlock
	movl	$1, %r12d
	movl	%ebp, %r15d
	jmp	.LBB18_136
.LBB18_132:                             #   in Loop: Header=BB18_140 Depth=2
	movl	$1, %r12d
	movl	%eax, %r15d
	jmp	.LBB18_134
.LBB18_133:                             #   in Loop: Header=BB18_140 Depth=2
	movq	408(%rsp), %rax
	movq	(%rax,%r14,8), %rax
	movb	$1, 73(%rax)
	movl	$14, %r12d
.LBB18_134:                             # %.thread640.i
                                        #   in Loop: Header=BB18_140 Depth=2
	movq	%rbx, %rdi
	callq	pthread_mutex_unlock
.LBB18_135:                             #   in Loop: Header=BB18_140 Depth=2
	movq	32(%rsp), %r14          # 8-byte Reload
.LBB18_136:                             # %.critedge.i
                                        #   in Loop: Header=BB18_140 Depth=2
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_138
# BB#137:                               #   in Loop: Header=BB18_140 Depth=2
	movq	(%rdi), %rax
.Ltmp292:
	callq	*16(%rax)
.Ltmp293:
.LBB18_138:                             # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit.i
                                        #   in Loop: Header=BB18_140 Depth=2
	leaq	1104(%rsp), %rdi
.Ltmp297:
	callq	_ZN8NArchive4NZip5CItemD2Ev
.Ltmp298:
# BB#139:                               #   in Loop: Header=BB18_140 Depth=2
	cmpl	$14, %r12d
	leaq	1136(%rsp), %rsi
	leaq	1328(%rsp), %rbx
	movq	64(%rsp), %r12          # 8-byte Reload
	pxor	%xmm0, %xmm0
	jne	.LBB18_290
.LBB18_140:                             # %.thread633.outer668.i
                                        #   Parent Loop BB18_99 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB18_143 Depth 3
                                        #       Child Loop BB18_150 Depth 3
	movq	56(%rsp), %rax          # 8-byte Reload
	movslq	12(%rax), %rcx
	cmpq	%rcx, %r12
	jge	.LBB18_284
# BB#141:                               # %.thread633.outer668.split.i
                                        #   in Loop: Header=BB18_140 Depth=2
	movq	16(%rax), %rax
	movl	24(%rsp), %edx          # 4-byte Reload
	cmpl	%edx, 300(%rsp)
	jae	.LBB18_147
# BB#142:                               # %.thread633.us.preheader.i
                                        #   in Loop: Header=BB18_140 Depth=2
	movslq	%r13d, %r13
	.p2align	4, 0x90
.LBB18_143:                             # %.thread633.us.i
                                        #   Parent Loop BB18_99 Depth=1
                                        #     Parent Loop BB18_140 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r13, %r14
	cmpq	%rcx, %r14
	jge	.LBB18_146
# BB#144:                               #   in Loop: Header=BB18_143 Depth=3
	leaq	1(%r14), %r13
	movq	(%rax,%r14,8), %rbp
	cmpb	$0, (%rbp)
	je	.LBB18_143
# BB#145:                               # %.us-lcssa771.us.i
                                        #   in Loop: Header=BB18_140 Depth=2
	movq	%r12, 64(%rsp)          # 8-byte Spill
	movdqu	%xmm0, (%rsi)
.Ltmp255:
	movl	$4, %edi
	movq	%rsi, %rbx
	callq	_Znam
.Ltmp256:
	jmp	.LBB18_114
.LBB18_146:                             #   in Loop: Header=BB18_140 Depth=2
	movl	%r14d, %r13d
	movq	32(%rsp), %r14          # 8-byte Reload
.LBB18_147:                             # %.us-lcssa770.i
                                        #   in Loop: Header=BB18_140 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, 16(%rsp)
	movq	408(%rsp), %rcx
	movq	(%rcx,%r12,8), %rcx
	cmpb	$0, 73(%rcx)
	jne	.LBB18_113
	jmp	.LBB18_152
.LBB18_148:                             # %.thread643.i
                                        #   in Loop: Header=BB18_140 Depth=2
	movq	%rbx, %rdi
	callq	pthread_mutex_unlock
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movl	$14, %r12d
	je	.LBB18_136
# BB#149:                               # %.lr.ph.i
                                        #   in Loop: Header=BB18_140 Depth=2
	leal	-1(%r13), %edx
	xorl	%ebp, %ebp
	movq	336(%rsp), %rax
.LBB18_150:                             #   Parent Loop BB18_99 Depth=1
                                        #     Parent Loop BB18_140 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%ebp, %rcx
	movq	(%rax,%rcx,8), %rbx
	cmpb	$0, 400(%rbx)
	jne	.LBB18_100
# BB#151:                               #   in Loop: Header=BB18_150 Depth=3
	incl	%ebp
	cmpl	24(%rsp), %ebp          # 4-byte Folded Reload
	jb	.LBB18_150
	jmp	.LBB18_135
.LBB18_152:                             #   in Loop: Header=BB18_99 Depth=1
	movq	%r12, 64(%rsp)          # 8-byte Spill
	movq	(%rax,%r12,8), %rbp
	movdqu	%xmm0, (%rbx)
.Ltmp161:
	movl	$4, %edi
	callq	_Znam
.Ltmp162:
# BB#153:                               #   in Loop: Header=BB18_99 Depth=1
	movq	%rax, 1328(%rsp)
	movb	$0, (%rax)
	movl	$4, 1340(%rsp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 24(%rbx)
	movq	$8, 1368(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 1344(%rsp)
	movdqu	%xmm0, 96(%rbx)
	movq	$8, 1440(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 1416(%rsp)
	movq	$_ZTV7CBufferIhE+16, 1448(%rsp)
	movdqu	%xmm0, 128(%rbx)
	movb	$0, 146(%rbx)
	movw	$0, 144(%rbx)
	cmpb	$0, 1(%rbp)
	leaq	1296(%rsp), %r12
	je	.LBB18_164
# BB#154:                               #   in Loop: Header=BB18_99 Depth=1
	cmpb	$0, (%rbp)
	je	.LBB18_164
.LBB18_155:                             # %.thread876.thread.i
                                        #   in Loop: Header=BB18_99 Depth=1
	cmpb	$0, 2(%rbp)
	je	.LBB18_175
.LBB18_156:                             #   in Loop: Header=BB18_99 Depth=1
.Ltmp233:
	leaq	192(%rsp), %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rbx
	movq	%rbp, %rdx
	movq	%r12, %rcx
	callq	_ZN8NArchive4NZipL13SetFileHeaderERNS0_11COutArchiveERKNS0_22CCompressionMethodModeERKNS0_11CUpdateItemERNS0_5CItemE
.Ltmp234:
# BB#157:                               # %.noexc551.i
                                        #   in Loop: Header=BB18_99 Depth=1
	movq	24(%rbx), %rdx
	movq	40(%rsp), %rax          # 8-byte Reload
	movzbl	104(%rax), %ecx
	movzwl	1336(%rsp), %esi
.Ltmp235:
	leaq	192(%rsp), %rdi
	callq	_ZN8NArchive4NZip11COutArchive26PrepareWriteCompressedDataEtyb
.Ltmp236:
# BB#158:                               # %.noexc552.i
                                        #   in Loop: Header=BB18_99 Depth=1
.Ltmp237:
	leaq	192(%rsp), %rdi
	movq	%r12, %rsi
	callq	_ZN8NArchive4NZip11COutArchive16WriteLocalHeaderERKNS0_10CLocalItemE
.Ltmp238:
.LBB18_159:                             # %_ZN8NArchive4NZipL14WriteDirHeaderERNS0_11COutArchiveEPKNS0_22CCompressionMethodModeERKNS0_11CUpdateItemERNS0_7CItemExE.exit.thread.i
                                        #   in Loop: Header=BB18_99 Depth=1
.Ltmp242:
	movl	$184, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp243:
# BB#160:                               # %.noexc569.i
                                        #   in Loop: Header=BB18_99 Depth=1
.Ltmp244:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	_ZN8NArchive4NZip5CItemC2ERKS1_
.Ltmp245:
# BB#161:                               #   in Loop: Header=BB18_99 Depth=1
.Ltmp247:
	leaq	160(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp248:
# BB#162:                               #   in Loop: Header=BB18_99 Depth=1
	movq	176(%rsp), %rax
	movslq	172(%rsp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 172(%rsp)
	movq	16(%rsp), %rbp
	addq	$26, %rbp
	movq	%rbp, 16(%rsp)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	16(%rax), %rbx
	leaq	80(%rbx), %r14
	movq	%r14, %rdi
	callq	pthread_mutex_lock
	movq	$0, 48(%rbx)
	movq	$0, 32(%rbx)
	movq	%rbp, 16(%rbx)
	movq	%r14, %rdi
	callq	pthread_mutex_unlock
	incq	64(%rsp)                # 8-byte Folded Spill
	xorl	%r14d, %r14d
.LBB18_163:                             # %_ZN8NArchive4NZipL14WriteDirHeaderERNS0_11COutArchiveEPKNS0_22CCompressionMethodModeERKNS0_11CUpdateItemERNS0_7CItemExE.exit.i
                                        #   in Loop: Header=BB18_99 Depth=1
	movl	%r15d, %edx
	jmp	.LBB18_168
.LBB18_164:                             #   in Loop: Header=BB18_99 Depth=1
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	16(%rax), %rax
	movslq	8(%rbp), %rcx
	movq	(%rax,%rcx,8), %rbx
.Ltmp164:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NArchive4NZip5CItemaSERKS1_
.Ltmp165:
# BB#165:                               #   in Loop: Header=BB18_99 Depth=1
	movzwl	184(%rbx), %eax
	leaq	1328(%rsp), %rcx
	movw	%ax, 152(%rcx)
	movl	180(%rbx), %eax
	movl	%eax, 148(%rcx)
.Ltmp166:
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	_ZN8NArchive4NZip10CInArchive28ReadLocalItemAfterCdItemFullERNS0_7CItemExE
.Ltmp167:
# BB#166:                               #   in Loop: Header=BB18_99 Depth=1
	movl	$-2147467263, %edx      # imm = 0x80004001
	testl	%eax, %eax
	je	.LBB18_171
# BB#167:                               #   in Loop: Header=BB18_99 Depth=1
	movb	$1, %r14b
.LBB18_168:                             # %_ZN8NArchive4NZipL14WriteDirHeaderERNS0_11COutArchiveEPKNS0_22CCompressionMethodModeERKNS0_11CUpdateItemERNS0_7CItemExE.exit.i
                                        #   in Loop: Header=BB18_99 Depth=1
.Ltmp252:
	movl	%edx, %r15d
	movq	%r12, %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
.Ltmp253:
# BB#169:                               #   in Loop: Header=BB18_99 Depth=1
	movl	%r14d, %eax
	andb	$15, %al
	cmpb	$14, %al
	movq	32(%rsp), %r14          # 8-byte Reload
	leaq	1136(%rsp), %rsi
	leaq	1328(%rsp), %rbx
	movq	64(%rsp), %r12          # 8-byte Reload
	pxor	%xmm0, %xmm0
	je	.LBB18_99
# BB#170:                               #   in Loop: Header=BB18_99 Depth=1
	testb	%al, %al
	je	.LBB18_99
	jmp	.LBB18_291
.LBB18_171:                             #   in Loop: Header=BB18_99 Depth=1
	cmpb	$0, (%rbp)
	je	.LBB18_187
# BB#172:                               # %.thread876.i
                                        #   in Loop: Header=BB18_99 Depth=1
	cmpb	$0, 1(%rbp)
	jne	.LBB18_155
# BB#173:                               #   in Loop: Header=BB18_99 Depth=1
.Ltmp168:
	movq	%r12, %rdi
	callq	_ZNK8NArchive4NZip5CItem5IsDirEv
.Ltmp169:
# BB#174:                               #   in Loop: Header=BB18_99 Depth=1
	testb	%al, %al
	jne	.LBB18_156
.LBB18_175:                             #   in Loop: Header=BB18_99 Depth=1
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movl	116(%rsp), %eax         # 4-byte Reload
	cmpl	64(%rsp), %eax          # 4-byte Folded Reload
	jge	.LBB18_178
# BB#176:                               #   in Loop: Header=BB18_99 Depth=1
.Ltmp170:
	leaq	192(%rsp), %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%r12, %rcx
	callq	_ZN8NArchive4NZipL13SetFileHeaderERNS0_11COutArchiveERKNS0_22CCompressionMethodModeERKNS0_11CUpdateItemERNS0_5CItemE
.Ltmp171:
# BB#177:                               #   in Loop: Header=BB18_99 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	24(%rax), %rdx
	movq	40(%rsp), %rax          # 8-byte Reload
	movzbl	104(%rax), %ecx
	movzwl	1336(%rsp), %esi
.Ltmp172:
	leaq	192(%rsp), %rdi
	callq	_ZN8NArchive4NZip11COutArchive26PrepareWriteCompressedDataEtyb
.Ltmp173:
	movq	64(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 116(%rsp)         # 4-byte Spill
.LBB18_178:                             #   in Loop: Header=BB18_99 Depth=1
	movq	408(%rsp), %rax
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rbx
	cmpb	$0, 72(%rbx)
	je	.LBB18_189
# BB#179:                               #   in Loop: Header=BB18_99 Depth=1
	movq	$0, 136(%rsp)
.Ltmp174:
	leaq	192(%rsp), %rdi
	leaq	136(%rsp), %rsi
	callq	_ZN8NArchive4NZip11COutArchive26CreateStreamForCompressingEPP10IOutStream
.Ltmp175:
# BB#180:                               #   in Loop: Header=BB18_99 Depth=1
	movq	432(%rsp), %rsi
	movq	136(%rsp), %rdx
.Ltmp176:
	movq	%rbx, %rdi
	callq	_ZNK10CMemBlocks13WriteToStreamEmP20ISequentialOutStream
.Ltmp177:
# BB#181:                               #   in Loop: Header=BB18_99 Depth=1
	leaq	48(%rbx), %rdi
	movq	40(%rsp), %rax          # 8-byte Reload
	movzbl	105(%rax), %edx
	movzbl	104(%rax), %esi
.Ltmp178:
	movq	%r12, %rcx
	callq	_ZN8NArchive4NZipL32SetItemInfoFromCompressingResultERKNS0_18CCompressingResultEbhRNS0_5CItemE
.Ltmp179:
# BB#182:                               #   in Loop: Header=BB18_99 Depth=1
.Ltmp180:
	leaq	192(%rsp), %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%r12, %rcx
	callq	_ZN8NArchive4NZipL13SetFileHeaderERNS0_11COutArchiveERKNS0_22CCompressionMethodModeERKNS0_11CUpdateItemERNS0_5CItemE
.Ltmp181:
# BB#183:                               #   in Loop: Header=BB18_99 Depth=1
.Ltmp182:
	leaq	192(%rsp), %rdi
	movq	%r12, %rsi
	callq	_ZN8NArchive4NZip11COutArchive16WriteLocalHeaderERKNS0_10CLocalItemE
.Ltmp183:
# BB#184:                               #   in Loop: Header=BB18_99 Depth=1
.Ltmp184:
	movq	%rbx, %rdi
	leaq	424(%rsp), %rsi
	callq	_ZN10CMemBlocks7FreeOptEP18CMemBlockManagerMt
.Ltmp185:
# BB#185:                               #   in Loop: Header=BB18_99 Depth=1
	movq	136(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_159
# BB#186:                               #   in Loop: Header=BB18_99 Depth=1
	movq	(%rdi), %rax
.Ltmp189:
	callq	*16(%rax)
.Ltmp190:
	jmp	.LBB18_159
.LBB18_187:                             #   in Loop: Header=BB18_99 Depth=1
.Ltmp240:
	movb	$1, %r14b
	leaq	192(%rsp), %rdi
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rdx
	movq	%r12, %rcx
	movq	(%rsp), %r8             # 8-byte Reload
	leaq	16(%rsp), %r9
	callq	_ZN8NArchive4NZipL17UpdateItemOldDataERNS0_11COutArchiveEP9IInStreamRKNS0_11CUpdateItemERNS0_7CItemExEP21ICompressProgressInfoRy
	movl	%eax, %edx
.Ltmp241:
# BB#188:                               #   in Loop: Header=BB18_99 Depth=1
	testl	%edx, %edx
	jne	.LBB18_168
	jmp	.LBB18_159
.LBB18_189:                             #   in Loop: Header=BB18_99 Depth=1
	movq	304(%rsp), %rax
	movq	336(%rsp), %rcx
	movslq	(%rax), %rax
	movq	(%rcx,%rax,8), %rbp
	movq	168(%rbp), %rax
	cmpb	$0, 41(%rax)
	jne	.LBB18_201
# BB#190:                               #   in Loop: Header=BB18_99 Depth=1
	movq	$0, 128(%rsp)
.Ltmp192:
	leaq	192(%rsp), %rdi
	leaq	128(%rsp), %rsi
	callq	_ZN8NArchive4NZip11COutArchive26CreateStreamForCompressingEPP10IOutStream
.Ltmp193:
# BB#191:                               #   in Loop: Header=BB18_99 Depth=1
	movq	168(%rbp), %r14
	movq	128(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB18_193
# BB#192:                               #   in Loop: Header=BB18_99 Depth=1
	movq	(%rbx), %rax
.Ltmp194:
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp195:
.LBB18_193:                             # %.noexc557.i
                                        #   in Loop: Header=BB18_99 Depth=1
	movq	160(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB18_195
# BB#194:                               #   in Loop: Header=BB18_99 Depth=1
	movq	(%rdi), %rax
.Ltmp196:
	callq	*16(%rax)
.Ltmp197:
.LBB18_195:                             # %_ZN9CMyComPtrI10IOutStreamEaSEPS0_.exit.i.i
                                        #   in Loop: Header=BB18_99 Depth=1
	testq	%rbx, %rbx
	movq	%rbx, 160(%r14)
	je	.LBB18_197
# BB#196:                               #   in Loop: Header=BB18_99 Depth=1
	movq	(%rbx), %rax
.Ltmp198:
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp199:
.LBB18_197:                             # %.noexc559.i
                                        #   in Loop: Header=BB18_99 Depth=1
	movq	152(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB18_199
# BB#198:                               #   in Loop: Header=BB18_99 Depth=1
	movq	(%rdi), %rax
.Ltmp200:
	callq	*16(%rax)
.Ltmp201:
.LBB18_199:                             #   in Loop: Header=BB18_99 Depth=1
	movq	%rbx, 152(%r14)
	movq	168(%rbp), %rbx
	movb	$1, 41(%rbx)
	movq	80(%rbx), %rdi
	callq	pthread_mutex_lock
	movb	$1, 89(%rbx)
	movq	80(%rbx), %rbx
	leaq	40(%rbx), %rdi
	callq	pthread_cond_broadcast
	movq	%rbx, %rdi
	callq	pthread_mutex_unlock
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_201
# BB#200:                               #   in Loop: Header=BB18_99 Depth=1
	movq	(%rdi), %rax
.Ltmp205:
	callq	*16(%rax)
.Ltmp206:
.LBB18_201:                             #   in Loop: Header=BB18_99 Depth=1
	movl	364(%rsp), %edi
	movq	368(%rsp), %rsi
.Ltmp208:
	xorl	%edx, %edx
	movl	$-1, %ecx
	callq	_Z22WaitForMultipleObjectsjPKPN8NWindows16NSynchronization15CBaseHandleWFMOEij
	movl	%eax, %ebx
.Ltmp209:
# BB#202:                               #   in Loop: Header=BB18_99 Depth=1
	movq	304(%rsp), %rax
	movslq	%ebx, %rcx
	movq	336(%rsp), %rdx
	movslq	(%rax,%rcx,4), %rax
	movq	(%rdx,%rax,8), %rbp
	movq	184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_205
# BB#203:                               #   in Loop: Header=BB18_99 Depth=1
	movq	(%rdi), %rax
.Ltmp211:
	callq	*16(%rax)
.Ltmp212:
# BB#204:                               # %.noexc565.i
                                        #   in Loop: Header=BB18_99 Depth=1
	movq	$0, 184(%rbp)
.LBB18_205:                             # %_ZN9CMyComPtrI19ISequentialInStreamE7ReleaseEv.exit566.i
                                        #   in Loop: Header=BB18_99 Depth=1
	movb	$1, 400(%rbp)
	movl	368(%rbp), %edx
	movb	$1, %r14b
	testl	%edx, %edx
	jne	.LBB18_168
# BB#206:                               #   in Loop: Header=BB18_99 Depth=1
.Ltmp213:
	movl	$1, %edx
	leaq	288(%rsp), %rdi
	movl	%ebx, %esi
	callq	_ZN17CBaseRecordVector6DeleteEii
.Ltmp214:
# BB#207:                               #   in Loop: Header=BB18_99 Depth=1
.Ltmp215:
	movl	$1, %edx
	leaq	352(%rsp), %rdi
	movl	%ebx, %esi
	callq	_ZN17CBaseRecordVector6DeleteEii
.Ltmp216:
# BB#208:                               #   in Loop: Header=BB18_99 Depth=1
	testl	%ebx, %ebx
	je	.LBB18_211
# BB#209:                               #   in Loop: Header=BB18_99 Depth=1
	movq	408(%rsp), %rax
	movslq	404(%rbp), %rcx
	movq	(%rax,%rcx,8), %rbx
	movq	168(%rbp), %rdi
.Ltmp217:
	movq	%rbx, %rsi
	callq	_ZN13COutMemStream10DetachDataER14CMemLockBlocks
.Ltmp218:
# BB#210:                               # %.thread650.i
                                        #   in Loop: Header=BB18_99 Depth=1
	movq	392(%rbp), %rax
	movq	%rax, 64(%rbx)
	movdqu	376(%rbp), %xmm0
	movdqu	%xmm0, 48(%rbx)
	movb	$1, 72(%rbx)
	movb	$14, %r14b
	jmp	.LBB18_163
.LBB18_211:                             #   in Loop: Header=BB18_99 Depth=1
	movq	168(%rbp), %rdi
.Ltmp220:
	callq	_ZN13COutMemStream17WriteToRealStreamEv
.Ltmp221:
# BB#212:                               #   in Loop: Header=BB18_99 Depth=1
	movl	%eax, %edx
	testl	%eax, %eax
	je	.LBB18_214
# BB#213:                               #   in Loop: Header=BB18_99 Depth=1
	leaq	1296(%rsp), %r12
	jmp	.LBB18_168
.LBB18_214:                             #   in Loop: Header=BB18_99 Depth=1
	movq	168(%rbp), %rbx
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB18_217
# BB#215:                               #   in Loop: Header=BB18_99 Depth=1
	movq	(%rdi), %rax
.Ltmp222:
	callq	*16(%rax)
.Ltmp223:
# BB#216:                               # %.noexc567.i
                                        #   in Loop: Header=BB18_99 Depth=1
	movq	$0, 160(%rbx)
.LBB18_217:                             # %_ZN9CMyComPtrI10IOutStreamE7ReleaseEv.exit.i.i
                                        #   in Loop: Header=BB18_99 Depth=1
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB18_220
# BB#218:                               #   in Loop: Header=BB18_99 Depth=1
	movq	(%rdi), %rax
.Ltmp224:
	callq	*16(%rax)
.Ltmp225:
# BB#219:                               # %.noexc568.i
                                        #   in Loop: Header=BB18_99 Depth=1
	movq	$0, 152(%rbx)
.LBB18_220:                             # %_ZN13COutMemStream16ReleaseOutStreamEv.exit.i
                                        #   in Loop: Header=BB18_99 Depth=1
	addq	$376, %rbp              # imm = 0x178
	movq	40(%rsp), %rax          # 8-byte Reload
	movzbl	105(%rax), %edx
	movzbl	104(%rax), %esi
.Ltmp226:
	movq	%rbp, %rdi
	leaq	1296(%rsp), %r12
	movq	%r12, %rcx
	callq	_ZN8NArchive4NZipL32SetItemInfoFromCompressingResultERKNS0_18CCompressingResultEbhRNS0_5CItemE
.Ltmp227:
# BB#221:                               #   in Loop: Header=BB18_99 Depth=1
.Ltmp228:
	leaq	192(%rsp), %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%r12, %rcx
	callq	_ZN8NArchive4NZipL13SetFileHeaderERNS0_11COutArchiveERKNS0_22CCompressionMethodModeERKNS0_11CUpdateItemERNS0_5CItemE
.Ltmp229:
# BB#222:                               #   in Loop: Header=BB18_99 Depth=1
.Ltmp230:
	leaq	192(%rsp), %rdi
	movq	%r12, %rsi
	callq	_ZN8NArchive4NZip11COutArchive16WriteLocalHeaderERKNS0_10CLocalItemE
.Ltmp231:
	jmp	.LBB18_159
.LBB18_223:
.Ltmp386:
	movl	$72, %edi
	callq	_Znwm
	movq	%rax, %r12
.Ltmp387:
# BB#224:                               # %.noexc.i
.Ltmp388:
	movq	%r12, %rdi
	callq	_ZN14CLocalProgressC1Ev
.Ltmp389:
# BB#225:
	movq	(%r12), %rax
.Ltmp391:
	movq	%r12, %rdi
	callq	*8(%rax)
.Ltmp392:
# BB#226:                               # %.noexc510.i
.Ltmp393:
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r12, 24(%rsp)          # 8-byte Spill
	callq	_ZN14CLocalProgress4InitEP9IProgressb
.Ltmp394:
# BB#227:
.Ltmp395:
	leaq	2200(%rsp), %rdi
	movq	%r15, %rsi
	callq	_ZN8NArchive4NZip10CAddCommonC1ERKNS0_22CCompressionMethodModeE
.Ltmp396:
# BB#228:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 1800(%rsp)
	movq	$8, 1816(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip5CItemEE+16, 1792(%rsp)
	movq	56(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 12(%rax)
	jle	.LBB18_281
# BB#229:                               # %.lr.ph.i.i
	leaq	944(%rsp), %rbp
	xorl	%r13d, %r13d
                                        # implicit-def: %R15D
	xorl	%eax, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %r14
	.p2align	4, 0x90
.LBB18_230:                             # =>This Inner Loop Header: Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, 48(%r12)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 56(%r12)
.Ltmp398:
	movq	%r12, %rdi
	callq	_ZN14CLocalProgress6SetCurEv
.Ltmp399:
# BB#231:                               #   in Loop: Header=BB18_230 Depth=1
	testl	%eax, %eax
	movl	%eax, %r12d
	cmovel	%r15d, %r12d
	pxor	%xmm0, %xmm0
	jne	.LBB18_285
# BB#232:                               #   in Loop: Header=BB18_230 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax,%r13,8), %rbx
	movdqu	%xmm0, (%rbp)
.Ltmp401:
	movl	$4, %edi
	callq	_Znam
.Ltmp402:
# BB#233:                               #   in Loop: Header=BB18_230 Depth=1
	movq	%rax, 944(%rsp)
	movb	$0, (%rax)
	movl	$4, 956(%rsp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 24(%rbp)
	movq	$8, 984(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 960(%rsp)
	movdqu	%xmm0, 96(%rbp)
	movq	$8, 1056(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 1032(%rsp)
	movq	$_ZTV7CBufferIhE+16, 1064(%rsp)
	movdqu	%xmm0, 128(%rbp)
	movb	$0, 146(%rbp)
	movw	$0, 144(%rbp)
	cmpb	$0, 1(%rbx)
	je	.LBB18_243
# BB#234:                               #   in Loop: Header=BB18_230 Depth=1
	cmpb	$0, (%rbx)
	je	.LBB18_243
.LBB18_235:                             # %.thread250.i.thread.i
                                        #   in Loop: Header=BB18_230 Depth=1
	cmpb	$0, 2(%rbx)
	je	.LBB18_251
.LBB18_236:                             #   in Loop: Header=BB18_230 Depth=1
.Ltmp441:
	leaq	192(%rsp), %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	leaq	912(%rsp), %rcx
	callq	_ZN8NArchive4NZipL13SetFileHeaderERNS0_11COutArchiveERKNS0_22CCompressionMethodModeERKNS0_11CUpdateItemERNS0_5CItemE
.Ltmp442:
# BB#237:                               # %.noexc183.i.i
                                        #   in Loop: Header=BB18_230 Depth=1
	movq	24(%rbx), %rdx
	movq	40(%rsp), %rax          # 8-byte Reload
	movzbl	104(%rax), %ecx
	movzwl	952(%rsp), %esi
.Ltmp443:
	leaq	192(%rsp), %rdi
	callq	_ZN8NArchive4NZip11COutArchive26PrepareWriteCompressedDataEtyb
.Ltmp444:
# BB#238:                               # %.noexc184.i.i
                                        #   in Loop: Header=BB18_230 Depth=1
.Ltmp445:
	leaq	192(%rsp), %rdi
	leaq	912(%rsp), %rsi
	callq	_ZN8NArchive4NZip11COutArchive16WriteLocalHeaderERKNS0_10CLocalItemE
.Ltmp446:
.LBB18_239:                             # %.thread.i.i
                                        #   in Loop: Header=BB18_230 Depth=1
.Ltmp451:
	movl	$184, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp452:
# BB#240:                               # %.noexc197.i.i
                                        #   in Loop: Header=BB18_230 Depth=1
.Ltmp453:
	movq	%rbx, %rdi
	leaq	912(%rsp), %rsi
	callq	_ZN8NArchive4NZip5CItemC2ERKS1_
.Ltmp454:
# BB#241:                               #   in Loop: Header=BB18_230 Depth=1
.Ltmp456:
	leaq	1792(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp457:
# BB#242:                               #   in Loop: Header=BB18_230 Depth=1
	movq	1808(%rsp), %rax
	movslq	1804(%rsp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 1804(%rsp)
	movq	24(%rsp), %r12          # 8-byte Reload
	addq	$26, 40(%r12)
	xorl	%ebp, %ebp
	jmp	.LBB18_278
.LBB18_243:                             #   in Loop: Header=BB18_230 Depth=1
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	16(%rax), %rax
	movslq	8(%rbx), %rcx
	movq	(%rax,%rcx,8), %rbp
.Ltmp404:
	leaq	912(%rsp), %rdi
	movq	%rbp, %rsi
	callq	_ZN8NArchive4NZip5CItemaSERKS1_
.Ltmp405:
# BB#244:                               #   in Loop: Header=BB18_230 Depth=1
	movzwl	184(%rbp), %eax
	movq	%r14, %rcx
	movw	%ax, 152(%rcx)
	movl	180(%rbp), %eax
	movl	%eax, 148(%rcx)
.Ltmp406:
	movq	32(%rsp), %rdi          # 8-byte Reload
	leaq	912(%rsp), %rsi
	callq	_ZN8NArchive4NZip10CInArchive28ReadLocalItemAfterCdItemFullERNS0_7CItemExE
.Ltmp407:
# BB#245:                               #   in Loop: Header=BB18_230 Depth=1
	movl	$1, %ebp
	testl	%eax, %eax
	je	.LBB18_247
# BB#246:                               #   in Loop: Header=BB18_230 Depth=1
	movl	$-2147467263, %r15d     # imm = 0x80004001
	movq	24(%rsp), %r12          # 8-byte Reload
	jmp	.LBB18_278
.LBB18_247:                             #   in Loop: Header=BB18_230 Depth=1
	cmpb	$0, (%rbx)
	je	.LBB18_256
# BB#248:                               # %.thread250.i.i
                                        #   in Loop: Header=BB18_230 Depth=1
	cmpb	$0, 1(%rbx)
	jne	.LBB18_235
# BB#249:                               #   in Loop: Header=BB18_230 Depth=1
.Ltmp408:
	leaq	912(%rsp), %rdi
	callq	_ZNK8NArchive4NZip5CItem5IsDirEv
.Ltmp409:
# BB#250:                               #   in Loop: Header=BB18_230 Depth=1
	testb	%al, %al
	jne	.LBB18_236
.LBB18_251:                             #   in Loop: Header=BB18_230 Depth=1
	movq	$0, 1104(%rsp)
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	12(%rbx), %esi
.Ltmp410:
	leaq	1104(%rsp), %rdx
	callq	*72(%rax)
.Ltmp411:
# BB#252:                               #   in Loop: Header=BB18_230 Depth=1
	testl	%eax, %eax
	je	.LBB18_259
# BB#253:                               #   in Loop: Header=BB18_230 Depth=1
	cmpl	$1, %eax
	jne	.LBB18_265
# BB#254:                               #   in Loop: Header=BB18_230 Depth=1
	movq	24(%rbx), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	addq	%rax, 40(%rcx)
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp433:
	xorl	%esi, %esi
	callq	*80(%rax)
.Ltmp434:
# BB#255:                               #   in Loop: Header=BB18_230 Depth=1
	xorl	%ebp, %ebp
	testl	%eax, %eax
	setne	%bpl
	cmovnel	%eax, %r12d
	movl	$4, %eax
	cmovel	%eax, %ebp
	movl	%r12d, %r15d
	movq	24(%rsp), %r12          # 8-byte Reload
	jmp	.LBB18_275
.LBB18_256:                             #   in Loop: Header=BB18_230 Depth=1
	movq	$0, 1600(%rsp)
	movq	24(%rsp), %r12          # 8-byte Reload
	movb	$0, 64(%r12)
.Ltmp448:
	leaq	192(%rsp), %rdi
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	leaq	912(%rsp), %rcx
	movq	%r12, %r8
	leaq	1600(%rsp), %r9
	callq	_ZN8NArchive4NZipL17UpdateItemOldDataERNS0_11COutArchiveEP9IInStreamRKNS0_11CUpdateItemERNS0_7CItemExEP21ICompressProgressInfoRy
.Ltmp449:
# BB#257:                               #   in Loop: Header=BB18_230 Depth=1
	testl	%eax, %eax
	je	.LBB18_266
# BB#258:                               #   in Loop: Header=BB18_230 Depth=1
	movl	%eax, %r15d
	jmp	.LBB18_278
.LBB18_259:                             #   in Loop: Header=BB18_230 Depth=1
.Ltmp413:
	leaq	192(%rsp), %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	leaq	912(%rsp), %rcx
	callq	_ZN8NArchive4NZipL13SetFileHeaderERNS0_11COutArchiveERKNS0_22CCompressionMethodModeERKNS0_11CUpdateItemERNS0_5CItemE
.Ltmp414:
# BB#260:                               #   in Loop: Header=BB18_230 Depth=1
	movq	24(%rbx), %rdx
	movq	40(%rsp), %rax          # 8-byte Reload
	movzbl	104(%rax), %ecx
	movzwl	952(%rsp), %esi
.Ltmp415:
	leaq	192(%rsp), %rdi
	callq	_ZN8NArchive4NZip11COutArchive26PrepareWriteCompressedDataEtyb
.Ltmp416:
# BB#261:                               #   in Loop: Header=BB18_230 Depth=1
	movq	$0, 1296(%rsp)
.Ltmp417:
	leaq	192(%rsp), %rdi
	leaq	1296(%rsp), %rsi
	movq	24(%rsp), %r12          # 8-byte Reload
	callq	_ZN8NArchive4NZip11COutArchive26CreateStreamForCompressingEPP10IOutStream
.Ltmp418:
# BB#262:                               #   in Loop: Header=BB18_230 Depth=1
	movq	1104(%rsp), %rsi
	movq	1296(%rsp), %rdx
.Ltmp419:
	leaq	2200(%rsp), %rdi
	movq	%r12, %rcx
	leaq	1600(%rsp), %r8
	callq	_ZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultE
.Ltmp420:
# BB#263:                               #   in Loop: Header=BB18_230 Depth=1
	movl	$1, %ebp
	testl	%eax, %eax
	je	.LBB18_267
# BB#264:                               #   in Loop: Header=BB18_230 Depth=1
	movl	%eax, %r15d
	jmp	.LBB18_273
.LBB18_265:                             #   in Loop: Header=BB18_230 Depth=1
	movl	$1, %ebp
	movl	%eax, %r15d
	movq	24(%rsp), %r12          # 8-byte Reload
	jmp	.LBB18_275
.LBB18_266:                             # %.thread209.i.i
                                        #   in Loop: Header=BB18_230 Depth=1
	movb	$1, 64(%r12)
	movq	1600(%rsp), %rax
	addq	%rax, 40(%r12)
	jmp	.LBB18_239
.LBB18_267:                             #   in Loop: Header=BB18_230 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movzbl	105(%rax), %edx
	movzbl	104(%rax), %esi
.Ltmp421:
	leaq	1600(%rsp), %rdi
	leaq	912(%rsp), %rcx
	callq	_ZN8NArchive4NZipL32SetItemInfoFromCompressingResultERKNS0_18CCompressingResultEbhRNS0_5CItemE
.Ltmp422:
# BB#268:                               #   in Loop: Header=BB18_230 Depth=1
.Ltmp423:
	leaq	192(%rsp), %rdi
	leaq	912(%rsp), %rsi
	callq	_ZN8NArchive4NZip11COutArchive16WriteLocalHeaderERKNS0_10CLocalItemE
.Ltmp424:
# BB#269:                               #   in Loop: Header=BB18_230 Depth=1
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp425:
	xorl	%esi, %esi
	callq	*80(%rax)
.Ltmp426:
# BB#270:                               #   in Loop: Header=BB18_230 Depth=1
	testl	%eax, %eax
	je	.LBB18_272
# BB#271:                               #   in Loop: Header=BB18_230 Depth=1
	movl	%eax, %r15d
	jmp	.LBB18_273
.LBB18_272:                             #   in Loop: Header=BB18_230 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	addq	936(%rsp), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	addq	928(%rsp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%ebp, %ebp
.LBB18_273:                             #   in Loop: Header=BB18_230 Depth=1
	movq	1296(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_275
# BB#274:                               #   in Loop: Header=BB18_230 Depth=1
	movq	(%rdi), %rax
.Ltmp430:
	callq	*16(%rax)
.Ltmp431:
.LBB18_275:                             #   in Loop: Header=BB18_230 Depth=1
	movq	1104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_277
# BB#276:                               #   in Loop: Header=BB18_230 Depth=1
	movq	(%rdi), %rax
.Ltmp438:
	callq	*16(%rax)
.Ltmp439:
.LBB18_277:                             # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit.i.i
                                        #   in Loop: Header=BB18_230 Depth=1
	testl	%ebp, %ebp
	je	.LBB18_239
	.p2align	4, 0x90
.LBB18_278:                             #   in Loop: Header=BB18_230 Depth=1
.Ltmp461:
	leaq	912(%rsp), %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
.Ltmp462:
# BB#279:                               #   in Loop: Header=BB18_230 Depth=1
	orl	$4, %ebp
	cmpl	$4, %ebp
	jne	.LBB18_283
# BB#280:                               #   in Loop: Header=BB18_230 Depth=1
	incq	%r13
	movq	56(%rsp), %rax          # 8-byte Reload
	movslq	12(%rax), %rax
	cmpq	%rax, %r13
	movq	%r14, %rbp
	jl	.LBB18_230
.LBB18_281:                             # %._crit_edge.i.i
.Ltmp464:
	leaq	192(%rsp), %rdi
	leaq	1792(%rsp), %rbx
	movq	%rbx, %rsi
	movq	152(%rsp), %rdx         # 8-byte Reload
	callq	_ZN8NArchive4NZip11COutArchive15WriteCentralDirERK13CObjectVectorINS0_5CItemEEPK7CBufferIhE
.Ltmp465:
# BB#282:                               # %._crit_edge..thread211_crit_edge.i.i
	xorl	%r15d, %r15d
.LBB18_283:                             # %.thread211.i.i
	leaq	1792(%rsp), %rbx
	jmp	.LBB18_286
.LBB18_284:                             # %.thread633.outer668..split_crit_edge.i
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 16(%rsp)
	xorl	%r15d, %r15d
.Ltmp158:
	leaq	192(%rsp), %rdi
	leaq	160(%rsp), %rsi
	movq	152(%rsp), %rdx         # 8-byte Reload
	leaq	776(%rsp), %r14
	leaq	680(%rsp), %r12
	movq	(%rsp), %r13            # 8-byte Reload
	callq	_ZN8NArchive4NZip11COutArchive15WriteCentralDirERK13CObjectVectorINS0_5CItemEEPK7CBufferIhE
.Ltmp159:
	jmp	.LBB18_292
.LBB18_285:
	movl	%eax, %r15d
	leaq	1792(%rsp), %rbx
	movq	24(%rsp), %r12          # 8-byte Reload
.LBB18_286:                             # %.thread211.i.i
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip5CItemEE+16, 1792(%rsp)
.Ltmp475:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp476:
# BB#287:
.Ltmp481:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp482:
# BB#288:                               # %_ZN13CObjectVectorIN8NArchive4NZip5CItemEED2Ev.exit190.i.i
.Ltmp486:
	leaq	2200(%rsp), %rdi
	callq	_ZN8NArchive4NZip10CAddCommonD2Ev
.Ltmp487:
# BB#289:                               # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit181.i.i
	movq	(%r12), %rax
.Ltmp492:
	movq	%r12, %rdi
	callq	*16(%rax)
.Ltmp493:
	jmp	.LBB18_309
.LBB18_290:                             # %.thread631.loopexit.loopexit.i
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 16(%rsp)
.LBB18_291:                             # %.thread631.i.loopexit
	leaq	776(%rsp), %r14
	leaq	680(%rsp), %r12
	movq	(%rsp), %r13            # 8-byte Reload
.LBB18_292:                             # %.thread631.i
.Ltmp302:
	leaq	288(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp303:
# BB#293:
.Ltmp307:
	leaq	352(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp308:
# BB#294:
.Ltmp312:
	leaq	320(%rsp), %rdi
	callq	_ZN8NArchive4NZip8CThreadsD2Ev
.Ltmp313:
# BB#295:
.Ltmp317:
	leaq	384(%rsp), %rdi
	callq	_ZN8NArchive4NZip8CMemRefsD2Ev
.Ltmp318:
# BB#296:
.Ltmp328:
	leaq	424(%rsp), %rdi
	callq	_ZN18CMemBlockManagerMt9FreeSpaceEv
.Ltmp329:
# BB#297:
	leaq	448(%rsp), %rdi
	callq	pthread_mutex_destroy
.Ltmp334:
	leaq	424(%rsp), %rdi
	callq	_ZN16CMemBlockManager9FreeSpaceEv
.Ltmp335:
# BB#298:                               # %_ZN18CMemBlockManagerMtD2Ev.exit.i
	leaq	600(%rsp), %rdi
	callq	pthread_mutex_destroy
	leaq	552(%rsp), %rdi
.Ltmp350:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp351:
# BB#299:
	leaq	520(%rsp), %rdi
.Ltmp355:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp356:
# BB#300:
	movq	512(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_302
# BB#301:
	movq	(%rdi), %rax
.Ltmp361:
	callq	*16(%rax)
.Ltmp362:
.LBB18_302:
	movq	(%r13), %rax
.Ltmp366:
	movq	%r13, %rdi
	callq	*16(%rax)
.Ltmp367:
# BB#303:                               # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit.i
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip5CItemEE+16, 160(%rsp)
.Ltmp377:
	leaq	160(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp378:
# BB#304:
.Ltmp383:
	leaq	160(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp384:
# BB#305:                               # %_ZN13CObjectVectorIN8NArchive4NZip5CItemEED2Ev.exit.i
	cmpb	$0, 728(%rsp)
	je	.LBB18_307
# BB#306:
	leaq	640(%rsp), %rdi
	callq	pthread_mutex_destroy
	movq	%r12, %rdi
	callq	pthread_cond_destroy
.LBB18_307:                             # %_ZN8NWindows16NSynchronization8CSynchroD2Ev.exit.i
	cmpb	$0, 824(%rsp)
	je	.LBB18_309
# BB#308:
	leaq	736(%rsp), %rdi
	callq	pthread_mutex_destroy
	movq	%r14, %rdi
	callq	pthread_cond_destroy
.LBB18_309:                             # %_ZN8NArchive4NZipL9Update2StERNS0_11COutArchiveEPNS0_10CInArchiveEP9IInStreamRK13CObjectVectorINS0_7CItemExEERKS7_INS0_11CUpdateItemEEPKNS0_22CCompressionMethodModeEPK7CBufferIhEP22IArchiveUpdateCallback.exit.i
	movq	1576(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_311
# BB#310:
	callq	_ZdaPv
.LBB18_311:                             # %_ZN11CStringBaseIcED2Ev.exit.i607.i
	movq	1520(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_313
# BB#312:
	callq	_ZdaPv
.LBB18_313:                             # %_ZN11CStringBaseIwED2Ev.exit.i608.i
.Ltmp497:
	leaq	1488(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp498:
# BB#314:                               # %_ZN8NArchive4NZip22CCompressionMethodModeD2Ev.exit610.i
.Ltmp503:
	leaq	2376(%rsp), %rdi
	callq	_ZN8NArchive4NZip10CAddCommonD2Ev
.Ltmp504:
.LBB18_315:
	movq	96(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB18_317
# BB#316:
	movq	(%rdi), %rax
.Ltmp508:
	callq	*16(%rax)
.Ltmp509:
.LBB18_317:                             # %_ZN9CMyComPtrI9IInStreamED2Ev.exit68
	leaq	200(%rsp), %rdi
.Ltmp525:
	movq	88(%rsp), %r12          # 8-byte Reload
	callq	_ZN10COutBuffer4FreeEv
.Ltmp526:
# BB#318:
	movq	224(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_320
# BB#319:
	movq	(%rdi), %rax
.Ltmp531:
	callq	*16(%rax)
.Ltmp532:
.LBB18_320:                             # %_ZN10COutBufferD2Ev.exit.i69
	movq	192(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_22
# BB#321:
	movq	(%rdi), %rax
.Ltmp537:
	callq	*16(%rax)
.Ltmp538:
	jmp	.LBB18_22
.LBB18_322:
.Ltmp219:
	jmp	.LBB18_338
.LBB18_323:
.Ltmp207:
	jmp	.LBB18_338
.LBB18_324:
.Ltmp210:
	jmp	.LBB18_338
.LBB18_325:
.Ltmp191:
	jmp	.LBB18_338
.LBB18_326:
.Ltmp202:
	movq	%rax, %r12
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_339
# BB#327:
	movq	(%rdi), %rax
.Ltmp203:
	callq	*16(%rax)
.Ltmp204:
	jmp	.LBB18_339
.LBB18_328:
.Ltmp232:
	jmp	.LBB18_338
.LBB18_329:
.Ltmp246:
	movq	%rax, %r12
                                        # kill: %RDX<kill>
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB18_339
.LBB18_330:
.Ltmp186:
	movq	%rax, %r12
	movq	136(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_339
# BB#331:
	movq	(%rdi), %rax
.Ltmp187:
	callq	*16(%rax)
.Ltmp188:
	jmp	.LBB18_339
.LBB18_332:
.Ltmp254:
	jmp	.LBB18_449
.LBB18_333:
.Ltmp163:
	jmp	.LBB18_449
.LBB18_334:
.Ltmp239:
	jmp	.LBB18_338
.LBB18_335:
.Ltmp160:
	jmp	.LBB18_449
.LBB18_336:
.Ltmp432:
	jmp	.LBB18_358
.LBB18_337:
.Ltmp249:
.LBB18_338:
	movq	%rax, %r12
.LBB18_339:
.Ltmp250:
	leaq	1296(%rsp), %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
.Ltmp251:
	jmp	.LBB18_450
.LBB18_340:
.Ltmp286:
	movq	%rax, %r12
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 16(%rsp)
	jmp	.LBB18_354
.LBB18_341:
.Ltmp450:
	jmp	.LBB18_429
.LBB18_342:
.Ltmp466:
	movq	%rax, %r12
                                        # kill: %RDX<kill>
	jmp	.LBB18_432
.LBB18_343:
.Ltmp294:
	jmp	.LBB18_413
.LBB18_344:
.Ltmp289:
	jmp	.LBB18_353
.LBB18_345:
.Ltmp440:
	jmp	.LBB18_429
.LBB18_346:
.Ltmp488:
	jmp	.LBB18_394
.LBB18_347:
.Ltmp483:
	movq	%rax, %r12
                                        # kill: %RDX<kill>
	jmp	.LBB18_434
.LBB18_348:
.Ltmp477:
	movq	%rax, %r12
                                        # kill: %RDX<kill>
.Ltmp478:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp479:
	jmp	.LBB18_434
.LBB18_349:
.Ltmp480:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB18_350:
.Ltmp390:
	movq	%r12, %rdi
	movq	%rax, %r12
                                        # kill: %RDX<kill>
	callq	_ZdlPv
	jmp	.LBB18_467
.LBB18_351:
.Ltmp363:
	jmp	.LBB18_390
.LBB18_352:
.Ltmp267:
.LBB18_353:
	movq	%rax, %r12
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 16(%rsp)
	movq	%rbx, %rdi
	callq	pthread_mutex_unlock
.LBB18_354:
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_414
# BB#355:
	movq	(%rdi), %rax
.Ltmp290:
	callq	*16(%rax)
.Ltmp291:
	jmp	.LBB18_414
.LBB18_356:
.Ltmp435:
	jmp	.LBB18_358
.LBB18_357:
.Ltmp412:
.LBB18_358:
	movq	%rax, %r12
                                        # kill: %RDX<kill>
.LBB18_359:
	movq	1104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_430
# BB#360:
	movq	(%rdi), %rax
.Ltmp436:
	callq	*16(%rax)
.Ltmp437:
	jmp	.LBB18_430
.LBB18_361:
.Ltmp427:
	movq	%rax, %r12
                                        # kill: %RDX<kill>
	movq	1296(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_359
# BB#362:
	movq	(%rdi), %rax
.Ltmp428:
	callq	*16(%rax)
.Ltmp429:
	jmp	.LBB18_359
.LBB18_363:
.Ltmp385:
	movq	%rax, %r12
	cmpb	$0, 728(%rsp)
	jne	.LBB18_464
	jmp	.LBB18_465
.LBB18_364:
.Ltmp379:
	movq	%rax, %r12
.Ltmp380:
	leaq	160(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp381:
	jmp	.LBB18_463
.LBB18_365:
.Ltmp382:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB18_366:
.Ltmp368:
	jmp	.LBB18_392
.LBB18_367:
.Ltmp357:
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%rax, %r12
	jmp	.LBB18_369
.LBB18_368:
.Ltmp352:
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%rax, %r12
	leaq	520(%rsp), %rdi
.Ltmp353:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp354:
.LBB18_369:
	movq	512(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_460
# BB#370:
	movq	(%rdi), %rax
.Ltmp358:
	callq	*16(%rax)
.Ltmp359:
	jmp	.LBB18_460
.LBB18_371:
.Ltmp360:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB18_372:
.Ltmp336:
	jmp	.LBB18_383
.LBB18_373:
.Ltmp330:
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%rax, %r12
	leaq	448(%rsp), %rdi
	callq	pthread_mutex_destroy
.Ltmp331:
	leaq	424(%rsp), %rdi
	callq	_ZN16CMemBlockManager9FreeSpaceEv
.Ltmp332:
	jmp	.LBB18_456
.LBB18_374:
.Ltmp333:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB18_375:
.Ltmp319:
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%rax, %r12
	jmp	.LBB18_454
.LBB18_376:
.Ltmp314:
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%rax, %r12
	jmp	.LBB18_453
.LBB18_377:
.Ltmp309:
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%rax, %r12
	jmp	.LBB18_452
.LBB18_378:
.Ltmp304:
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%rax, %r12
	jmp	.LBB18_451
.LBB18_379:
.Ltmp112:
	movq	%r13, (%rsp)            # 8-byte Spill
	jmp	.LBB18_449
.LBB18_380:
.Ltmp106:
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%rax, %r12
.Ltmp107:
	leaq	424(%rsp), %rdi
	callq	_ZN16CMemBlockManager9FreeSpaceEv
.Ltmp108:
	jmp	.LBB18_456
.LBB18_381:
.Ltmp109:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB18_382:
.Ltmp103:
.LBB18_383:
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%rax, %r12
	jmp	.LBB18_456
.LBB18_384:
.Ltmp93:
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%rax, %r12
.Ltmp94:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp95:
# BB#385:
.Ltmp96:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp97:
# BB#386:
	movq	512(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_460
# BB#387:
	movq	(%rdi), %rax
.Ltmp98:
	callq	*16(%rax)
.Ltmp99:
	jmp	.LBB18_460
.LBB18_388:
.Ltmp100:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB18_389:
.Ltmp90:
.LBB18_390:
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%rax, %r12
	jmp	.LBB18_460
.LBB18_391:
.Ltmp87:
.LBB18_392:                             # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit599.i
	movq	%rax, %r12
	jmp	.LBB18_461
.LBB18_393:
.Ltmp397:
.LBB18_394:
	movq	%rax, %r12
                                        # kill: %RDX<kill>
	jmp	.LBB18_435
.LBB18_395:
.Ltmp539:
	movq	%rax, %r12
	jmp	.LBB18_503
.LBB18_396:
.Ltmp533:
	movq	%rax, %r12
	jmp	.LBB18_406
.LBB18_397:
.Ltmp510:
	jmp	.LBB18_411
.LBB18_398:
.Ltmp499:
	movq	%rax, %r12
	jmp	.LBB18_472
.LBB18_399:
.Ltmp84:
	jmp	.LBB18_403
.LBB18_400:
.Ltmp81:
	movq	%rax, %r12
	jmp	.LBB18_472
.LBB18_401:
.Ltmp455:
	movq	%rax, %r12
                                        # kill: %RDX<kill>
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB18_430
.LBB18_402:
.Ltmp494:
.LBB18_403:
	movq	%rax, %r12
	jmp	.LBB18_467
.LBB18_404:
.Ltmp527:
	movq	%rax, %r12
	movq	224(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_406
# BB#405:
	movq	(%rdi), %rax
.Ltmp528:
	callq	*16(%rax)
.Ltmp529:
.LBB18_406:                             # %.body.i72
	movq	192(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_503
# BB#407:
	movq	(%rdi), %rax
.Ltmp534:
	callq	*16(%rax)
.Ltmp535:
	jmp	.LBB18_503
.LBB18_408:
.Ltmp530:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB18_409:
.Ltmp536:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB18_410:
.Ltmp60:
.LBB18_411:                             # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	movq	%rax, %r12
	jmp	.LBB18_475
.LBB18_412:
.Ltmp264:
.LBB18_413:
	movq	%rax, %r12
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 16(%rsp)
.LBB18_414:
.Ltmp295:
	leaq	1104(%rsp), %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
.Ltmp296:
	jmp	.LBB18_450
.LBB18_415:
.Ltmp257:
	jmp	.LBB18_417
.LBB18_416:
.Ltmp299:
.LBB18_417:
	movq	%rax, %r12
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 16(%rsp)
	jmp	.LBB18_450
.LBB18_418:
.Ltmp447:
	jmp	.LBB18_429
.LBB18_419:
.Ltmp403:
	jmp	.LBB18_422
.LBB18_420:
.Ltmp463:
	jmp	.LBB18_422
.LBB18_421:
.Ltmp400:
.LBB18_422:
	movq	%rax, %r12
                                        # kill: %RDX<kill>
	jmp	.LBB18_431
.LBB18_423:                             # %.loopexit.split-lp
.Ltmp505:
	jmp	.LBB18_447
.LBB18_424:                             # %.thread317
.Ltmp57:
	movq	%r12, 88(%rsp)          # 8-byte Spill
	movq	%rax, %r12
	jmp	.LBB18_503
.LBB18_425:
.Ltmp132:
	movq	%rax, %r12
	movq	%r14, %rdi
	callq	_ZdlPv
	jmp	.LBB18_442
.LBB18_426:
.Ltmp117:
	movq	%rax, %r12
	movq	%r15, %rdi
	callq	_ZdlPv
	jmp	.LBB18_445
.LBB18_427:
.Ltmp125:
	jmp	.LBB18_449
.LBB18_428:
.Ltmp458:
.LBB18_429:
	movq	%rax, %r12
                                        # kill: %RDX<kill>
.LBB18_430:
.Ltmp459:
	leaq	912(%rsp), %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
.Ltmp460:
.LBB18_431:
	leaq	1792(%rsp), %rbx
.LBB18_432:
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip5CItemEE+16, 1792(%rsp)
.Ltmp467:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp468:
# BB#433:
.Ltmp473:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp474:
.LBB18_434:
.Ltmp484:
	leaq	2200(%rsp), %rdi
	callq	_ZN8NArchive4NZip10CAddCommonD2Ev
.Ltmp485:
.LBB18_435:
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp489:
	callq	*16(%rax)
.Ltmp490:
	jmp	.LBB18_467
.LBB18_436:
.Ltmp469:
	movq	%rax, %rbp
.Ltmp470:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp471:
	jmp	.LBB18_439
.LBB18_437:
.Ltmp472:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB18_438:
.Ltmp491:
	movq	%rax, %rbp
.LBB18_439:                             # %.body.i.i
	movq	%rbp, %rdi
	callq	__clang_call_terminate
.LBB18_440:
.Ltmp67:
	movq	%rax, %r12
.Ltmp68:
	leaq	1600(%rsp), %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
.Ltmp69:
	jmp	.LBB18_473
.LBB18_441:
.Ltmp135:
	movq	%rax, %r12
.LBB18_442:                             # %.body527.i
.Ltmp136:
	leaq	1792(%rsp), %rdi
	callq	_ZN8NArchive4NZip11CThreadInfoD2Ev
.Ltmp137:
	jmp	.LBB18_450
.LBB18_443:
.Ltmp140:
	jmp	.LBB18_449
.LBB18_444:
.Ltmp120:
	movq	%rax, %r12
.LBB18_445:                             # %.body520.i
.Ltmp121:
	leaq	832(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp122:
	jmp	.LBB18_450
.LBB18_446:                             # %.loopexit
.Ltmp74:
.LBB18_447:                             # %.body65
	movq	%rax, %r12
	cmpq	$0, 96(%rsp)            # 8-byte Folded Reload
	jne	.LBB18_474
	jmp	.LBB18_475
.LBB18_448:
.Ltmp157:
.LBB18_449:
	movq	%rax, %r12
.LBB18_450:
.Ltmp300:
	leaq	288(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp301:
.LBB18_451:
.Ltmp305:
	leaq	352(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp306:
.LBB18_452:
.Ltmp310:
	leaq	320(%rsp), %rdi
	callq	_ZN8NArchive4NZip8CThreadsD2Ev
.Ltmp311:
.LBB18_453:
.Ltmp315:
	leaq	384(%rsp), %rdi
	callq	_ZN8NArchive4NZip8CMemRefsD2Ev
.Ltmp316:
.LBB18_454:
.Ltmp320:
	leaq	424(%rsp), %rdi
	callq	_ZN18CMemBlockManagerMt9FreeSpaceEv
.Ltmp321:
# BB#455:
	leaq	448(%rsp), %rdi
	callq	pthread_mutex_destroy
.Ltmp326:
	leaq	424(%rsp), %rdi
	callq	_ZN16CMemBlockManager9FreeSpaceEv
.Ltmp327:
.LBB18_456:
	leaq	600(%rsp), %rdi
	callq	pthread_mutex_destroy
	leaq	552(%rsp), %rdi
.Ltmp337:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp338:
# BB#457:
	leaq	520(%rsp), %rdi
.Ltmp342:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp343:
# BB#458:
	movq	512(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_460
# BB#459:
	movq	(%rdi), %rax
.Ltmp348:
	callq	*16(%rax)
.Ltmp349:
.LBB18_460:
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp364:
	callq	*16(%rax)
.Ltmp365:
.LBB18_461:                             # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit599.i
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip5CItemEE+16, 160(%rsp)
.Ltmp369:
	leaq	160(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp370:
# BB#462:
.Ltmp375:
	leaq	160(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp376:
.LBB18_463:
	cmpb	$0, 728(%rsp)
	je	.LBB18_465
.LBB18_464:
	leaq	640(%rsp), %rdi
	callq	pthread_mutex_destroy
	leaq	680(%rsp), %rdi
	callq	pthread_cond_destroy
.LBB18_465:                             # %_ZN8NWindows16NSynchronization8CSynchroD2Ev.exit605.i
	cmpb	$0, 824(%rsp)
	je	.LBB18_467
# BB#466:
	leaq	736(%rsp), %rdi
	callq	pthread_mutex_destroy
	leaq	776(%rsp), %rdi
	callq	pthread_cond_destroy
.LBB18_467:
	movq	1576(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_469
# BB#468:
	callq	_ZdaPv
.LBB18_469:                             # %_ZN11CStringBaseIcED2Ev.exit.i.i
	movq	1520(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_471
# BB#470:
	callq	_ZdaPv
.LBB18_471:                             # %_ZN11CStringBaseIwED2Ev.exit.i.i
.Ltmp495:
	leaq	1488(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp496:
.LBB18_472:
.Ltmp500:
	leaq	2376(%rsp), %rdi
	callq	_ZN8NArchive4NZip10CAddCommonD2Ev
.Ltmp501:
.LBB18_473:                             # %.body65
	cmpq	$0, 96(%rsp)            # 8-byte Folded Reload
	je	.LBB18_475
.LBB18_474:
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp506:
	callq	*16(%rax)
.Ltmp507:
.LBB18_475:                             # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	leaq	200(%rsp), %rdi
.Ltmp511:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp512:
# BB#476:
	movq	224(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_478
# BB#477:
	movq	(%rdi), %rax
.Ltmp517:
	callq	*16(%rax)
.Ltmp518:
.LBB18_478:                             # %_ZN10COutBufferD2Ev.exit.i
	movq	192(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_503
# BB#479:
	movq	(%rdi), %rax
.Ltmp523:
	callq	*16(%rax)
.Ltmp524:
	jmp	.LBB18_503
.LBB18_480:
.Ltmp519:
	movq	%rax, %rbx
	jmp	.LBB18_492
.LBB18_481:
.Ltmp322:
	movq	%rax, %rbx
	leaq	448(%rsp), %rdi
	callq	pthread_mutex_destroy
.Ltmp323:
	leaq	424(%rsp), %rdi
	callq	_ZN16CMemBlockManager9FreeSpaceEv
.Ltmp324:
	jmp	.LBB18_497
.LBB18_482:
.Ltmp325:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB18_483:
.Ltmp344:
	movq	%rax, %rbx
	jmp	.LBB18_485
.LBB18_484:
.Ltmp339:
	movq	%rax, %rbx
	leaq	520(%rsp), %rdi
.Ltmp340:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp341:
.LBB18_485:
	movq	512(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_497
# BB#486:
	movq	(%rdi), %rax
.Ltmp345:
	callq	*16(%rax)
.Ltmp346:
	jmp	.LBB18_497
.LBB18_487:
.Ltmp347:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB18_488:
.Ltmp371:
	movq	%rax, %rbx
.Ltmp372:
	leaq	160(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp373:
	jmp	.LBB18_497
.LBB18_489:
.Ltmp374:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB18_490:
.Ltmp513:
	movq	%rax, %rbx
	movq	224(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_492
# BB#491:
	movq	(%rdi), %rax
.Ltmp514:
	callq	*16(%rax)
.Ltmp515:
.LBB18_492:                             # %.body.i
	movq	192(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_506
# BB#493:
	movq	(%rdi), %rax
.Ltmp520:
	callq	*16(%rax)
.Ltmp521:
	jmp	.LBB18_506
.LBB18_494:
.Ltmp516:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB18_495:
.Ltmp522:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB18_496:
.Ltmp502:
	movq	%rax, %rbx
.LBB18_497:                             # %.body583.i
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB18_498:
.Ltmp39:
	movq	%rax, %r12
	xorl	%eax, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	jmp	.LBB18_500
.LBB18_499:
.Ltmp52:
	movq	%r12, 88(%rsp)          # 8-byte Spill
	movq	%rax, %r12
.LBB18_500:
	movq	1792(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_502
# BB#501:
	movq	(%rdi), %rax
.Ltmp53:
	callq	*16(%rax)
.Ltmp54:
.LBB18_502:
	cmpq	$0, 88(%rsp)            # 8-byte Folded Reload
	je	.LBB18_504
.LBB18_503:
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp540:
	callq	*16(%rax)
.Ltmp541:
.LBB18_504:                             # %_ZN9CMyComPtrI10IOutStreamED2Ev.exit
	movq	%r12, %rdi
	callq	_Unwind_Resume
.LBB18_505:
.Ltmp542:
	movq	%rax, %rbx
.LBB18_506:                             # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end18:
	.size	_ZN8NArchive4NZip6UpdateERK13CObjectVectorINS0_7CItemExEERKS1_INS0_11CUpdateItemEEP20ISequentialOutStreamPNS0_10CInArchiveEPNS0_22CCompressionMethodModeEP22IArchiveUpdateCallback, .Lfunc_end18-_ZN8NArchive4NZip6UpdateERK13CObjectVectorINS0_7CItemExEERKS1_INS0_11CUpdateItemEEP20ISequentialOutStreamPNS0_10CInArchiveEPNS0_22CCompressionMethodModeEP22IArchiveUpdateCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\243\215"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\232\r"                # Call site table length
	.long	.Ltmp35-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp38-.Ltmp35         #   Call between .Ltmp35 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin5   #     jumps to .Ltmp39
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp51-.Ltmp40         #   Call between .Ltmp40 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin5   #     jumps to .Ltmp52
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin5   #     jumps to .Ltmp57
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin5   # >> Call Site 4 <<
	.long	.Ltmp58-.Ltmp56         #   Call between .Ltmp56 and .Ltmp58
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin5   # >> Call Site 5 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin5   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp61-.Lfunc_begin5   # >> Call Site 6 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp505-.Lfunc_begin5  #     jumps to .Ltmp505
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin5   # >> Call Site 7 <<
	.long	.Ltmp64-.Ltmp63         #   Call between .Ltmp63 and .Ltmp64
	.long	.Ltmp74-.Lfunc_begin5   #     jumps to .Ltmp74
	.byte	0                       #   On action: cleanup
	.long	.Ltmp65-.Lfunc_begin5   # >> Call Site 8 <<
	.long	.Ltmp66-.Ltmp65         #   Call between .Ltmp65 and .Ltmp66
	.long	.Ltmp67-.Lfunc_begin5   #     jumps to .Ltmp67
	.byte	0                       #   On action: cleanup
	.long	.Ltmp72-.Lfunc_begin5   # >> Call Site 9 <<
	.long	.Ltmp73-.Ltmp72         #   Call between .Ltmp72 and .Ltmp73
	.long	.Ltmp74-.Lfunc_begin5   #     jumps to .Ltmp74
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin5   # >> Call Site 10 <<
	.long	.Ltmp78-.Ltmp75         #   Call between .Ltmp75 and .Ltmp78
	.long	.Ltmp505-.Lfunc_begin5  #     jumps to .Ltmp505
	.byte	0                       #   On action: cleanup
	.long	.Ltmp79-.Lfunc_begin5   # >> Call Site 11 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin5   #     jumps to .Ltmp81
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin5   # >> Call Site 12 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin5   #     jumps to .Ltmp84
	.byte	0                       #   On action: cleanup
	.long	.Ltmp70-.Lfunc_begin5   # >> Call Site 13 <<
	.long	.Ltmp71-.Ltmp70         #   Call between .Ltmp70 and .Ltmp71
	.long	.Ltmp505-.Lfunc_begin5  #     jumps to .Ltmp505
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin5   # >> Call Site 14 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin5   #     jumps to .Ltmp87
	.byte	0                       #   On action: cleanup
	.long	.Ltmp88-.Lfunc_begin5   # >> Call Site 15 <<
	.long	.Ltmp89-.Ltmp88         #   Call between .Ltmp88 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin5   #     jumps to .Ltmp90
	.byte	0                       #   On action: cleanup
	.long	.Ltmp91-.Lfunc_begin5   # >> Call Site 16 <<
	.long	.Ltmp92-.Ltmp91         #   Call between .Ltmp91 and .Ltmp92
	.long	.Ltmp93-.Lfunc_begin5   #     jumps to .Ltmp93
	.byte	0                       #   On action: cleanup
	.long	.Ltmp101-.Lfunc_begin5  # >> Call Site 17 <<
	.long	.Ltmp102-.Ltmp101       #   Call between .Ltmp101 and .Ltmp102
	.long	.Ltmp103-.Lfunc_begin5  #     jumps to .Ltmp103
	.byte	0                       #   On action: cleanup
	.long	.Ltmp104-.Lfunc_begin5  # >> Call Site 18 <<
	.long	.Ltmp105-.Ltmp104       #   Call between .Ltmp104 and .Ltmp105
	.long	.Ltmp106-.Lfunc_begin5  #     jumps to .Ltmp106
	.byte	0                       #   On action: cleanup
	.long	.Ltmp110-.Lfunc_begin5  # >> Call Site 19 <<
	.long	.Ltmp111-.Ltmp110       #   Call between .Ltmp110 and .Ltmp111
	.long	.Ltmp112-.Lfunc_begin5  #     jumps to .Ltmp112
	.byte	0                       #   On action: cleanup
	.long	.Ltmp113-.Lfunc_begin5  # >> Call Site 20 <<
	.long	.Ltmp114-.Ltmp113       #   Call between .Ltmp113 and .Ltmp114
	.long	.Ltmp120-.Lfunc_begin5  #     jumps to .Ltmp120
	.byte	0                       #   On action: cleanup
	.long	.Ltmp115-.Lfunc_begin5  # >> Call Site 21 <<
	.long	.Ltmp116-.Ltmp115       #   Call between .Ltmp115 and .Ltmp116
	.long	.Ltmp117-.Lfunc_begin5  #     jumps to .Ltmp117
	.byte	0                       #   On action: cleanup
	.long	.Ltmp118-.Lfunc_begin5  # >> Call Site 22 <<
	.long	.Ltmp119-.Ltmp118       #   Call between .Ltmp118 and .Ltmp119
	.long	.Ltmp120-.Lfunc_begin5  #     jumps to .Ltmp120
	.byte	0                       #   On action: cleanup
	.long	.Ltmp123-.Lfunc_begin5  # >> Call Site 23 <<
	.long	.Ltmp124-.Ltmp123       #   Call between .Ltmp123 and .Ltmp124
	.long	.Ltmp125-.Lfunc_begin5  #     jumps to .Ltmp125
	.byte	0                       #   On action: cleanup
	.long	.Ltmp126-.Lfunc_begin5  # >> Call Site 24 <<
	.long	.Ltmp127-.Ltmp126       #   Call between .Ltmp126 and .Ltmp127
	.long	.Ltmp140-.Lfunc_begin5  #     jumps to .Ltmp140
	.byte	0                       #   On action: cleanup
	.long	.Ltmp128-.Lfunc_begin5  # >> Call Site 25 <<
	.long	.Ltmp129-.Ltmp128       #   Call between .Ltmp128 and .Ltmp129
	.long	.Ltmp135-.Lfunc_begin5  #     jumps to .Ltmp135
	.byte	0                       #   On action: cleanup
	.long	.Ltmp130-.Lfunc_begin5  # >> Call Site 26 <<
	.long	.Ltmp131-.Ltmp130       #   Call between .Ltmp130 and .Ltmp131
	.long	.Ltmp132-.Lfunc_begin5  #     jumps to .Ltmp132
	.byte	0                       #   On action: cleanup
	.long	.Ltmp133-.Lfunc_begin5  # >> Call Site 27 <<
	.long	.Ltmp134-.Ltmp133       #   Call between .Ltmp133 and .Ltmp134
	.long	.Ltmp135-.Lfunc_begin5  #     jumps to .Ltmp135
	.byte	0                       #   On action: cleanup
	.long	.Ltmp138-.Lfunc_begin5  # >> Call Site 28 <<
	.long	.Ltmp139-.Ltmp138       #   Call between .Ltmp138 and .Ltmp139
	.long	.Ltmp140-.Lfunc_begin5  #     jumps to .Ltmp140
	.byte	0                       #   On action: cleanup
	.long	.Ltmp141-.Lfunc_begin5  # >> Call Site 29 <<
	.long	.Ltmp156-.Ltmp141       #   Call between .Ltmp141 and .Ltmp156
	.long	.Ltmp157-.Lfunc_begin5  #     jumps to .Ltmp157
	.byte	0                       #   On action: cleanup
	.long	.Ltmp270-.Lfunc_begin5  # >> Call Site 30 <<
	.long	.Ltmp285-.Ltmp270       #   Call between .Ltmp270 and .Ltmp285
	.long	.Ltmp286-.Lfunc_begin5  #     jumps to .Ltmp286
	.byte	0                       #   On action: cleanup
	.long	.Ltmp258-.Lfunc_begin5  # >> Call Site 31 <<
	.long	.Ltmp263-.Ltmp258       #   Call between .Ltmp258 and .Ltmp263
	.long	.Ltmp264-.Lfunc_begin5  #     jumps to .Ltmp264
	.byte	0                       #   On action: cleanup
	.long	.Ltmp265-.Lfunc_begin5  # >> Call Site 32 <<
	.long	.Ltmp266-.Ltmp265       #   Call between .Ltmp265 and .Ltmp266
	.long	.Ltmp267-.Lfunc_begin5  #     jumps to .Ltmp267
	.byte	0                       #   On action: cleanup
	.long	.Ltmp287-.Lfunc_begin5  # >> Call Site 33 <<
	.long	.Ltmp269-.Ltmp287       #   Call between .Ltmp287 and .Ltmp269
	.long	.Ltmp289-.Lfunc_begin5  #     jumps to .Ltmp289
	.byte	0                       #   On action: cleanup
	.long	.Ltmp292-.Lfunc_begin5  # >> Call Site 34 <<
	.long	.Ltmp293-.Ltmp292       #   Call between .Ltmp292 and .Ltmp293
	.long	.Ltmp294-.Lfunc_begin5  #     jumps to .Ltmp294
	.byte	0                       #   On action: cleanup
	.long	.Ltmp297-.Lfunc_begin5  # >> Call Site 35 <<
	.long	.Ltmp298-.Ltmp297       #   Call between .Ltmp297 and .Ltmp298
	.long	.Ltmp299-.Lfunc_begin5  #     jumps to .Ltmp299
	.byte	0                       #   On action: cleanup
	.long	.Ltmp255-.Lfunc_begin5  # >> Call Site 36 <<
	.long	.Ltmp256-.Ltmp255       #   Call between .Ltmp255 and .Ltmp256
	.long	.Ltmp257-.Lfunc_begin5  #     jumps to .Ltmp257
	.byte	0                       #   On action: cleanup
	.long	.Ltmp161-.Lfunc_begin5  # >> Call Site 37 <<
	.long	.Ltmp162-.Ltmp161       #   Call between .Ltmp161 and .Ltmp162
	.long	.Ltmp163-.Lfunc_begin5  #     jumps to .Ltmp163
	.byte	0                       #   On action: cleanup
	.long	.Ltmp233-.Lfunc_begin5  # >> Call Site 38 <<
	.long	.Ltmp238-.Ltmp233       #   Call between .Ltmp233 and .Ltmp238
	.long	.Ltmp239-.Lfunc_begin5  #     jumps to .Ltmp239
	.byte	0                       #   On action: cleanup
	.long	.Ltmp242-.Lfunc_begin5  # >> Call Site 39 <<
	.long	.Ltmp243-.Ltmp242       #   Call between .Ltmp242 and .Ltmp243
	.long	.Ltmp249-.Lfunc_begin5  #     jumps to .Ltmp249
	.byte	0                       #   On action: cleanup
	.long	.Ltmp244-.Lfunc_begin5  # >> Call Site 40 <<
	.long	.Ltmp245-.Ltmp244       #   Call between .Ltmp244 and .Ltmp245
	.long	.Ltmp246-.Lfunc_begin5  #     jumps to .Ltmp246
	.byte	0                       #   On action: cleanup
	.long	.Ltmp247-.Lfunc_begin5  # >> Call Site 41 <<
	.long	.Ltmp167-.Ltmp247       #   Call between .Ltmp247 and .Ltmp167
	.long	.Ltmp249-.Lfunc_begin5  #     jumps to .Ltmp249
	.byte	0                       #   On action: cleanup
	.long	.Ltmp252-.Lfunc_begin5  # >> Call Site 42 <<
	.long	.Ltmp253-.Ltmp252       #   Call between .Ltmp252 and .Ltmp253
	.long	.Ltmp254-.Lfunc_begin5  #     jumps to .Ltmp254
	.byte	0                       #   On action: cleanup
	.long	.Ltmp168-.Lfunc_begin5  # >> Call Site 43 <<
	.long	.Ltmp169-.Ltmp168       #   Call between .Ltmp168 and .Ltmp169
	.long	.Ltmp249-.Lfunc_begin5  #     jumps to .Ltmp249
	.byte	0                       #   On action: cleanup
	.long	.Ltmp170-.Lfunc_begin5  # >> Call Site 44 <<
	.long	.Ltmp173-.Ltmp170       #   Call between .Ltmp170 and .Ltmp173
	.long	.Ltmp239-.Lfunc_begin5  #     jumps to .Ltmp239
	.byte	0                       #   On action: cleanup
	.long	.Ltmp174-.Lfunc_begin5  # >> Call Site 45 <<
	.long	.Ltmp185-.Ltmp174       #   Call between .Ltmp174 and .Ltmp185
	.long	.Ltmp186-.Lfunc_begin5  #     jumps to .Ltmp186
	.byte	0                       #   On action: cleanup
	.long	.Ltmp189-.Lfunc_begin5  # >> Call Site 46 <<
	.long	.Ltmp190-.Ltmp189       #   Call between .Ltmp189 and .Ltmp190
	.long	.Ltmp191-.Lfunc_begin5  #     jumps to .Ltmp191
	.byte	0                       #   On action: cleanup
	.long	.Ltmp240-.Lfunc_begin5  # >> Call Site 47 <<
	.long	.Ltmp241-.Ltmp240       #   Call between .Ltmp240 and .Ltmp241
	.long	.Ltmp249-.Lfunc_begin5  #     jumps to .Ltmp249
	.byte	0                       #   On action: cleanup
	.long	.Ltmp192-.Lfunc_begin5  # >> Call Site 48 <<
	.long	.Ltmp201-.Ltmp192       #   Call between .Ltmp192 and .Ltmp201
	.long	.Ltmp202-.Lfunc_begin5  #     jumps to .Ltmp202
	.byte	0                       #   On action: cleanup
	.long	.Ltmp205-.Lfunc_begin5  # >> Call Site 49 <<
	.long	.Ltmp206-.Ltmp205       #   Call between .Ltmp205 and .Ltmp206
	.long	.Ltmp207-.Lfunc_begin5  #     jumps to .Ltmp207
	.byte	0                       #   On action: cleanup
	.long	.Ltmp208-.Lfunc_begin5  # >> Call Site 50 <<
	.long	.Ltmp209-.Ltmp208       #   Call between .Ltmp208 and .Ltmp209
	.long	.Ltmp210-.Lfunc_begin5  #     jumps to .Ltmp210
	.byte	0                       #   On action: cleanup
	.long	.Ltmp211-.Lfunc_begin5  # >> Call Site 51 <<
	.long	.Ltmp216-.Ltmp211       #   Call between .Ltmp211 and .Ltmp216
	.long	.Ltmp232-.Lfunc_begin5  #     jumps to .Ltmp232
	.byte	0                       #   On action: cleanup
	.long	.Ltmp217-.Lfunc_begin5  # >> Call Site 52 <<
	.long	.Ltmp218-.Ltmp217       #   Call between .Ltmp217 and .Ltmp218
	.long	.Ltmp219-.Lfunc_begin5  #     jumps to .Ltmp219
	.byte	0                       #   On action: cleanup
	.long	.Ltmp220-.Lfunc_begin5  # >> Call Site 53 <<
	.long	.Ltmp231-.Ltmp220       #   Call between .Ltmp220 and .Ltmp231
	.long	.Ltmp232-.Lfunc_begin5  #     jumps to .Ltmp232
	.byte	0                       #   On action: cleanup
	.long	.Ltmp386-.Lfunc_begin5  # >> Call Site 54 <<
	.long	.Ltmp387-.Ltmp386       #   Call between .Ltmp386 and .Ltmp387
	.long	.Ltmp494-.Lfunc_begin5  #     jumps to .Ltmp494
	.byte	0                       #   On action: cleanup
	.long	.Ltmp388-.Lfunc_begin5  # >> Call Site 55 <<
	.long	.Ltmp389-.Ltmp388       #   Call between .Ltmp388 and .Ltmp389
	.long	.Ltmp390-.Lfunc_begin5  #     jumps to .Ltmp390
	.byte	0                       #   On action: cleanup
	.long	.Ltmp391-.Lfunc_begin5  # >> Call Site 56 <<
	.long	.Ltmp392-.Ltmp391       #   Call between .Ltmp391 and .Ltmp392
	.long	.Ltmp494-.Lfunc_begin5  #     jumps to .Ltmp494
	.byte	0                       #   On action: cleanup
	.long	.Ltmp393-.Lfunc_begin5  # >> Call Site 57 <<
	.long	.Ltmp396-.Ltmp393       #   Call between .Ltmp393 and .Ltmp396
	.long	.Ltmp397-.Lfunc_begin5  #     jumps to .Ltmp397
	.byte	0                       #   On action: cleanup
	.long	.Ltmp398-.Lfunc_begin5  # >> Call Site 58 <<
	.long	.Ltmp399-.Ltmp398       #   Call between .Ltmp398 and .Ltmp399
	.long	.Ltmp400-.Lfunc_begin5  #     jumps to .Ltmp400
	.byte	0                       #   On action: cleanup
	.long	.Ltmp401-.Lfunc_begin5  # >> Call Site 59 <<
	.long	.Ltmp402-.Ltmp401       #   Call between .Ltmp401 and .Ltmp402
	.long	.Ltmp403-.Lfunc_begin5  #     jumps to .Ltmp403
	.byte	0                       #   On action: cleanup
	.long	.Ltmp441-.Lfunc_begin5  # >> Call Site 60 <<
	.long	.Ltmp446-.Ltmp441       #   Call between .Ltmp441 and .Ltmp446
	.long	.Ltmp447-.Lfunc_begin5  #     jumps to .Ltmp447
	.byte	0                       #   On action: cleanup
	.long	.Ltmp451-.Lfunc_begin5  # >> Call Site 61 <<
	.long	.Ltmp452-.Ltmp451       #   Call between .Ltmp451 and .Ltmp452
	.long	.Ltmp458-.Lfunc_begin5  #     jumps to .Ltmp458
	.byte	0                       #   On action: cleanup
	.long	.Ltmp453-.Lfunc_begin5  # >> Call Site 62 <<
	.long	.Ltmp454-.Ltmp453       #   Call between .Ltmp453 and .Ltmp454
	.long	.Ltmp455-.Lfunc_begin5  #     jumps to .Ltmp455
	.byte	0                       #   On action: cleanup
	.long	.Ltmp456-.Lfunc_begin5  # >> Call Site 63 <<
	.long	.Ltmp409-.Ltmp456       #   Call between .Ltmp456 and .Ltmp409
	.long	.Ltmp458-.Lfunc_begin5  #     jumps to .Ltmp458
	.byte	0                       #   On action: cleanup
	.long	.Ltmp410-.Lfunc_begin5  # >> Call Site 64 <<
	.long	.Ltmp411-.Ltmp410       #   Call between .Ltmp410 and .Ltmp411
	.long	.Ltmp412-.Lfunc_begin5  #     jumps to .Ltmp412
	.byte	0                       #   On action: cleanup
	.long	.Ltmp433-.Lfunc_begin5  # >> Call Site 65 <<
	.long	.Ltmp434-.Ltmp433       #   Call between .Ltmp433 and .Ltmp434
	.long	.Ltmp435-.Lfunc_begin5  #     jumps to .Ltmp435
	.byte	0                       #   On action: cleanup
	.long	.Ltmp448-.Lfunc_begin5  # >> Call Site 66 <<
	.long	.Ltmp449-.Ltmp448       #   Call between .Ltmp448 and .Ltmp449
	.long	.Ltmp450-.Lfunc_begin5  #     jumps to .Ltmp450
	.byte	0                       #   On action: cleanup
	.long	.Ltmp413-.Lfunc_begin5  # >> Call Site 67 <<
	.long	.Ltmp416-.Ltmp413       #   Call between .Ltmp413 and .Ltmp416
	.long	.Ltmp435-.Lfunc_begin5  #     jumps to .Ltmp435
	.byte	0                       #   On action: cleanup
	.long	.Ltmp417-.Lfunc_begin5  # >> Call Site 68 <<
	.long	.Ltmp426-.Ltmp417       #   Call between .Ltmp417 and .Ltmp426
	.long	.Ltmp427-.Lfunc_begin5  #     jumps to .Ltmp427
	.byte	0                       #   On action: cleanup
	.long	.Ltmp430-.Lfunc_begin5  # >> Call Site 69 <<
	.long	.Ltmp431-.Ltmp430       #   Call between .Ltmp430 and .Ltmp431
	.long	.Ltmp432-.Lfunc_begin5  #     jumps to .Ltmp432
	.byte	0                       #   On action: cleanup
	.long	.Ltmp438-.Lfunc_begin5  # >> Call Site 70 <<
	.long	.Ltmp439-.Ltmp438       #   Call between .Ltmp438 and .Ltmp439
	.long	.Ltmp440-.Lfunc_begin5  #     jumps to .Ltmp440
	.byte	0                       #   On action: cleanup
	.long	.Ltmp461-.Lfunc_begin5  # >> Call Site 71 <<
	.long	.Ltmp462-.Ltmp461       #   Call between .Ltmp461 and .Ltmp462
	.long	.Ltmp463-.Lfunc_begin5  #     jumps to .Ltmp463
	.byte	0                       #   On action: cleanup
	.long	.Ltmp464-.Lfunc_begin5  # >> Call Site 72 <<
	.long	.Ltmp465-.Ltmp464       #   Call between .Ltmp464 and .Ltmp465
	.long	.Ltmp466-.Lfunc_begin5  #     jumps to .Ltmp466
	.byte	0                       #   On action: cleanup
	.long	.Ltmp158-.Lfunc_begin5  # >> Call Site 73 <<
	.long	.Ltmp159-.Ltmp158       #   Call between .Ltmp158 and .Ltmp159
	.long	.Ltmp160-.Lfunc_begin5  #     jumps to .Ltmp160
	.byte	0                       #   On action: cleanup
	.long	.Ltmp475-.Lfunc_begin5  # >> Call Site 74 <<
	.long	.Ltmp476-.Ltmp475       #   Call between .Ltmp475 and .Ltmp476
	.long	.Ltmp477-.Lfunc_begin5  #     jumps to .Ltmp477
	.byte	0                       #   On action: cleanup
	.long	.Ltmp481-.Lfunc_begin5  # >> Call Site 75 <<
	.long	.Ltmp482-.Ltmp481       #   Call between .Ltmp481 and .Ltmp482
	.long	.Ltmp483-.Lfunc_begin5  #     jumps to .Ltmp483
	.byte	0                       #   On action: cleanup
	.long	.Ltmp486-.Lfunc_begin5  # >> Call Site 76 <<
	.long	.Ltmp487-.Ltmp486       #   Call between .Ltmp486 and .Ltmp487
	.long	.Ltmp488-.Lfunc_begin5  #     jumps to .Ltmp488
	.byte	0                       #   On action: cleanup
	.long	.Ltmp492-.Lfunc_begin5  # >> Call Site 77 <<
	.long	.Ltmp493-.Ltmp492       #   Call between .Ltmp492 and .Ltmp493
	.long	.Ltmp494-.Lfunc_begin5  #     jumps to .Ltmp494
	.byte	0                       #   On action: cleanup
	.long	.Ltmp302-.Lfunc_begin5  # >> Call Site 78 <<
	.long	.Ltmp303-.Ltmp302       #   Call between .Ltmp302 and .Ltmp303
	.long	.Ltmp304-.Lfunc_begin5  #     jumps to .Ltmp304
	.byte	0                       #   On action: cleanup
	.long	.Ltmp307-.Lfunc_begin5  # >> Call Site 79 <<
	.long	.Ltmp308-.Ltmp307       #   Call between .Ltmp307 and .Ltmp308
	.long	.Ltmp309-.Lfunc_begin5  #     jumps to .Ltmp309
	.byte	0                       #   On action: cleanup
	.long	.Ltmp312-.Lfunc_begin5  # >> Call Site 80 <<
	.long	.Ltmp313-.Ltmp312       #   Call between .Ltmp312 and .Ltmp313
	.long	.Ltmp314-.Lfunc_begin5  #     jumps to .Ltmp314
	.byte	0                       #   On action: cleanup
	.long	.Ltmp317-.Lfunc_begin5  # >> Call Site 81 <<
	.long	.Ltmp318-.Ltmp317       #   Call between .Ltmp317 and .Ltmp318
	.long	.Ltmp319-.Lfunc_begin5  #     jumps to .Ltmp319
	.byte	0                       #   On action: cleanup
	.long	.Ltmp328-.Lfunc_begin5  # >> Call Site 82 <<
	.long	.Ltmp329-.Ltmp328       #   Call between .Ltmp328 and .Ltmp329
	.long	.Ltmp330-.Lfunc_begin5  #     jumps to .Ltmp330
	.byte	0                       #   On action: cleanup
	.long	.Ltmp334-.Lfunc_begin5  # >> Call Site 83 <<
	.long	.Ltmp335-.Ltmp334       #   Call between .Ltmp334 and .Ltmp335
	.long	.Ltmp336-.Lfunc_begin5  #     jumps to .Ltmp336
	.byte	0                       #   On action: cleanup
	.long	.Ltmp350-.Lfunc_begin5  # >> Call Site 84 <<
	.long	.Ltmp351-.Ltmp350       #   Call between .Ltmp350 and .Ltmp351
	.long	.Ltmp352-.Lfunc_begin5  #     jumps to .Ltmp352
	.byte	0                       #   On action: cleanup
	.long	.Ltmp355-.Lfunc_begin5  # >> Call Site 85 <<
	.long	.Ltmp356-.Ltmp355       #   Call between .Ltmp355 and .Ltmp356
	.long	.Ltmp357-.Lfunc_begin5  #     jumps to .Ltmp357
	.byte	0                       #   On action: cleanup
	.long	.Ltmp361-.Lfunc_begin5  # >> Call Site 86 <<
	.long	.Ltmp362-.Ltmp361       #   Call between .Ltmp361 and .Ltmp362
	.long	.Ltmp363-.Lfunc_begin5  #     jumps to .Ltmp363
	.byte	0                       #   On action: cleanup
	.long	.Ltmp366-.Lfunc_begin5  # >> Call Site 87 <<
	.long	.Ltmp367-.Ltmp366       #   Call between .Ltmp366 and .Ltmp367
	.long	.Ltmp368-.Lfunc_begin5  #     jumps to .Ltmp368
	.byte	0                       #   On action: cleanup
	.long	.Ltmp377-.Lfunc_begin5  # >> Call Site 88 <<
	.long	.Ltmp378-.Ltmp377       #   Call between .Ltmp377 and .Ltmp378
	.long	.Ltmp379-.Lfunc_begin5  #     jumps to .Ltmp379
	.byte	0                       #   On action: cleanup
	.long	.Ltmp383-.Lfunc_begin5  # >> Call Site 89 <<
	.long	.Ltmp384-.Ltmp383       #   Call between .Ltmp383 and .Ltmp384
	.long	.Ltmp385-.Lfunc_begin5  #     jumps to .Ltmp385
	.byte	0                       #   On action: cleanup
	.long	.Ltmp497-.Lfunc_begin5  # >> Call Site 90 <<
	.long	.Ltmp498-.Ltmp497       #   Call between .Ltmp497 and .Ltmp498
	.long	.Ltmp499-.Lfunc_begin5  #     jumps to .Ltmp499
	.byte	0                       #   On action: cleanup
	.long	.Ltmp503-.Lfunc_begin5  # >> Call Site 91 <<
	.long	.Ltmp504-.Ltmp503       #   Call between .Ltmp503 and .Ltmp504
	.long	.Ltmp505-.Lfunc_begin5  #     jumps to .Ltmp505
	.byte	0                       #   On action: cleanup
	.long	.Ltmp508-.Lfunc_begin5  # >> Call Site 92 <<
	.long	.Ltmp509-.Ltmp508       #   Call between .Ltmp508 and .Ltmp509
	.long	.Ltmp510-.Lfunc_begin5  #     jumps to .Ltmp510
	.byte	0                       #   On action: cleanup
	.long	.Ltmp525-.Lfunc_begin5  # >> Call Site 93 <<
	.long	.Ltmp526-.Ltmp525       #   Call between .Ltmp525 and .Ltmp526
	.long	.Ltmp527-.Lfunc_begin5  #     jumps to .Ltmp527
	.byte	0                       #   On action: cleanup
	.long	.Ltmp531-.Lfunc_begin5  # >> Call Site 94 <<
	.long	.Ltmp532-.Ltmp531       #   Call between .Ltmp531 and .Ltmp532
	.long	.Ltmp533-.Lfunc_begin5  #     jumps to .Ltmp533
	.byte	0                       #   On action: cleanup
	.long	.Ltmp537-.Lfunc_begin5  # >> Call Site 95 <<
	.long	.Ltmp538-.Ltmp537       #   Call between .Ltmp537 and .Ltmp538
	.long	.Ltmp539-.Lfunc_begin5  #     jumps to .Ltmp539
	.byte	0                       #   On action: cleanup
	.long	.Ltmp203-.Lfunc_begin5  # >> Call Site 96 <<
	.long	.Ltmp251-.Ltmp203       #   Call between .Ltmp203 and .Ltmp251
	.long	.Ltmp502-.Lfunc_begin5  #     jumps to .Ltmp502
	.byte	1                       #   On action: 1
	.long	.Ltmp478-.Lfunc_begin5  # >> Call Site 97 <<
	.long	.Ltmp479-.Ltmp478       #   Call between .Ltmp478 and .Ltmp479
	.long	.Ltmp480-.Lfunc_begin5  #     jumps to .Ltmp480
	.byte	1                       #   On action: 1
	.long	.Ltmp290-.Lfunc_begin5  # >> Call Site 98 <<
	.long	.Ltmp291-.Ltmp290       #   Call between .Ltmp290 and .Ltmp291
	.long	.Ltmp502-.Lfunc_begin5  #     jumps to .Ltmp502
	.byte	1                       #   On action: 1
	.long	.Ltmp436-.Lfunc_begin5  # >> Call Site 99 <<
	.long	.Ltmp429-.Ltmp436       #   Call between .Ltmp436 and .Ltmp429
	.long	.Ltmp491-.Lfunc_begin5  #     jumps to .Ltmp491
	.byte	1                       #   On action: 1
	.long	.Ltmp380-.Lfunc_begin5  # >> Call Site 100 <<
	.long	.Ltmp381-.Ltmp380       #   Call between .Ltmp380 and .Ltmp381
	.long	.Ltmp382-.Lfunc_begin5  #     jumps to .Ltmp382
	.byte	1                       #   On action: 1
	.long	.Ltmp353-.Lfunc_begin5  # >> Call Site 101 <<
	.long	.Ltmp359-.Ltmp353       #   Call between .Ltmp353 and .Ltmp359
	.long	.Ltmp360-.Lfunc_begin5  #     jumps to .Ltmp360
	.byte	1                       #   On action: 1
	.long	.Ltmp331-.Lfunc_begin5  # >> Call Site 102 <<
	.long	.Ltmp332-.Ltmp331       #   Call between .Ltmp331 and .Ltmp332
	.long	.Ltmp333-.Lfunc_begin5  #     jumps to .Ltmp333
	.byte	1                       #   On action: 1
	.long	.Ltmp107-.Lfunc_begin5  # >> Call Site 103 <<
	.long	.Ltmp108-.Ltmp107       #   Call between .Ltmp107 and .Ltmp108
	.long	.Ltmp109-.Lfunc_begin5  #     jumps to .Ltmp109
	.byte	1                       #   On action: 1
	.long	.Ltmp94-.Lfunc_begin5   # >> Call Site 104 <<
	.long	.Ltmp99-.Ltmp94         #   Call between .Ltmp94 and .Ltmp99
	.long	.Ltmp100-.Lfunc_begin5  #     jumps to .Ltmp100
	.byte	1                       #   On action: 1
	.long	.Ltmp528-.Lfunc_begin5  # >> Call Site 105 <<
	.long	.Ltmp529-.Ltmp528       #   Call between .Ltmp528 and .Ltmp529
	.long	.Ltmp530-.Lfunc_begin5  #     jumps to .Ltmp530
	.byte	1                       #   On action: 1
	.long	.Ltmp534-.Lfunc_begin5  # >> Call Site 106 <<
	.long	.Ltmp535-.Ltmp534       #   Call between .Ltmp534 and .Ltmp535
	.long	.Ltmp536-.Lfunc_begin5  #     jumps to .Ltmp536
	.byte	1                       #   On action: 1
	.long	.Ltmp295-.Lfunc_begin5  # >> Call Site 107 <<
	.long	.Ltmp296-.Ltmp295       #   Call between .Ltmp295 and .Ltmp296
	.long	.Ltmp502-.Lfunc_begin5  #     jumps to .Ltmp502
	.byte	1                       #   On action: 1
	.long	.Ltmp459-.Lfunc_begin5  # >> Call Site 108 <<
	.long	.Ltmp460-.Ltmp459       #   Call between .Ltmp459 and .Ltmp460
	.long	.Ltmp491-.Lfunc_begin5  #     jumps to .Ltmp491
	.byte	1                       #   On action: 1
	.long	.Ltmp467-.Lfunc_begin5  # >> Call Site 109 <<
	.long	.Ltmp468-.Ltmp467       #   Call between .Ltmp467 and .Ltmp468
	.long	.Ltmp469-.Lfunc_begin5  #     jumps to .Ltmp469
	.byte	1                       #   On action: 1
	.long	.Ltmp473-.Lfunc_begin5  # >> Call Site 110 <<
	.long	.Ltmp490-.Ltmp473       #   Call between .Ltmp473 and .Ltmp490
	.long	.Ltmp491-.Lfunc_begin5  #     jumps to .Ltmp491
	.byte	1                       #   On action: 1
	.long	.Ltmp470-.Lfunc_begin5  # >> Call Site 111 <<
	.long	.Ltmp471-.Ltmp470       #   Call between .Ltmp470 and .Ltmp471
	.long	.Ltmp472-.Lfunc_begin5  #     jumps to .Ltmp472
	.byte	1                       #   On action: 1
	.long	.Ltmp68-.Lfunc_begin5   # >> Call Site 112 <<
	.long	.Ltmp316-.Ltmp68        #   Call between .Ltmp68 and .Ltmp316
	.long	.Ltmp502-.Lfunc_begin5  #     jumps to .Ltmp502
	.byte	1                       #   On action: 1
	.long	.Ltmp320-.Lfunc_begin5  # >> Call Site 113 <<
	.long	.Ltmp321-.Ltmp320       #   Call between .Ltmp320 and .Ltmp321
	.long	.Ltmp322-.Lfunc_begin5  #     jumps to .Ltmp322
	.byte	1                       #   On action: 1
	.long	.Ltmp326-.Lfunc_begin5  # >> Call Site 114 <<
	.long	.Ltmp327-.Ltmp326       #   Call between .Ltmp326 and .Ltmp327
	.long	.Ltmp502-.Lfunc_begin5  #     jumps to .Ltmp502
	.byte	1                       #   On action: 1
	.long	.Ltmp337-.Lfunc_begin5  # >> Call Site 115 <<
	.long	.Ltmp338-.Ltmp337       #   Call between .Ltmp337 and .Ltmp338
	.long	.Ltmp339-.Lfunc_begin5  #     jumps to .Ltmp339
	.byte	1                       #   On action: 1
	.long	.Ltmp342-.Lfunc_begin5  # >> Call Site 116 <<
	.long	.Ltmp343-.Ltmp342       #   Call between .Ltmp342 and .Ltmp343
	.long	.Ltmp344-.Lfunc_begin5  #     jumps to .Ltmp344
	.byte	1                       #   On action: 1
	.long	.Ltmp348-.Lfunc_begin5  # >> Call Site 117 <<
	.long	.Ltmp365-.Ltmp348       #   Call between .Ltmp348 and .Ltmp365
	.long	.Ltmp502-.Lfunc_begin5  #     jumps to .Ltmp502
	.byte	1                       #   On action: 1
	.long	.Ltmp369-.Lfunc_begin5  # >> Call Site 118 <<
	.long	.Ltmp370-.Ltmp369       #   Call between .Ltmp369 and .Ltmp370
	.long	.Ltmp371-.Lfunc_begin5  #     jumps to .Ltmp371
	.byte	1                       #   On action: 1
	.long	.Ltmp375-.Lfunc_begin5  # >> Call Site 119 <<
	.long	.Ltmp501-.Ltmp375       #   Call between .Ltmp375 and .Ltmp501
	.long	.Ltmp502-.Lfunc_begin5  #     jumps to .Ltmp502
	.byte	1                       #   On action: 1
	.long	.Ltmp506-.Lfunc_begin5  # >> Call Site 120 <<
	.long	.Ltmp507-.Ltmp506       #   Call between .Ltmp506 and .Ltmp507
	.long	.Ltmp542-.Lfunc_begin5  #     jumps to .Ltmp542
	.byte	1                       #   On action: 1
	.long	.Ltmp511-.Lfunc_begin5  # >> Call Site 121 <<
	.long	.Ltmp512-.Ltmp511       #   Call between .Ltmp511 and .Ltmp512
	.long	.Ltmp513-.Lfunc_begin5  #     jumps to .Ltmp513
	.byte	1                       #   On action: 1
	.long	.Ltmp517-.Lfunc_begin5  # >> Call Site 122 <<
	.long	.Ltmp518-.Ltmp517       #   Call between .Ltmp517 and .Ltmp518
	.long	.Ltmp519-.Lfunc_begin5  #     jumps to .Ltmp519
	.byte	1                       #   On action: 1
	.long	.Ltmp523-.Lfunc_begin5  # >> Call Site 123 <<
	.long	.Ltmp524-.Ltmp523       #   Call between .Ltmp523 and .Ltmp524
	.long	.Ltmp542-.Lfunc_begin5  #     jumps to .Ltmp542
	.byte	1                       #   On action: 1
	.long	.Ltmp323-.Lfunc_begin5  # >> Call Site 124 <<
	.long	.Ltmp324-.Ltmp323       #   Call between .Ltmp323 and .Ltmp324
	.long	.Ltmp325-.Lfunc_begin5  #     jumps to .Ltmp325
	.byte	1                       #   On action: 1
	.long	.Ltmp340-.Lfunc_begin5  # >> Call Site 125 <<
	.long	.Ltmp346-.Ltmp340       #   Call between .Ltmp340 and .Ltmp346
	.long	.Ltmp347-.Lfunc_begin5  #     jumps to .Ltmp347
	.byte	1                       #   On action: 1
	.long	.Ltmp372-.Lfunc_begin5  # >> Call Site 126 <<
	.long	.Ltmp373-.Ltmp372       #   Call between .Ltmp372 and .Ltmp373
	.long	.Ltmp374-.Lfunc_begin5  #     jumps to .Ltmp374
	.byte	1                       #   On action: 1
	.long	.Ltmp514-.Lfunc_begin5  # >> Call Site 127 <<
	.long	.Ltmp515-.Ltmp514       #   Call between .Ltmp514 and .Ltmp515
	.long	.Ltmp516-.Lfunc_begin5  #     jumps to .Ltmp516
	.byte	1                       #   On action: 1
	.long	.Ltmp520-.Lfunc_begin5  # >> Call Site 128 <<
	.long	.Ltmp521-.Ltmp520       #   Call between .Ltmp520 and .Ltmp521
	.long	.Ltmp522-.Lfunc_begin5  #     jumps to .Ltmp522
	.byte	1                       #   On action: 1
	.long	.Ltmp53-.Lfunc_begin5   # >> Call Site 129 <<
	.long	.Ltmp541-.Ltmp53        #   Call between .Ltmp53 and .Ltmp541
	.long	.Ltmp542-.Lfunc_begin5  #     jumps to .Ltmp542
	.byte	1                       #   On action: 1
	.long	.Ltmp541-.Lfunc_begin5  # >> Call Site 130 <<
	.long	.Lfunc_end18-.Ltmp541   #   Call between .Ltmp541 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive4NZip17CMtProgressMixer214QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive4NZip17CMtProgressMixer214QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive4NZip17CMtProgressMixer214QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip17CMtProgressMixer214QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive4NZip17CMtProgressMixer214QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive4NZip17CMtProgressMixer214QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi136:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB19_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB19_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB19_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB19_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB19_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB19_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB19_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB19_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB19_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB19_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB19_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB19_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB19_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB19_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB19_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB19_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB19_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end19:
	.size	_ZN8NArchive4NZip17CMtProgressMixer214QueryInterfaceERK4GUIDPPv, .Lfunc_end19-_ZN8NArchive4NZip17CMtProgressMixer214QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip17CMtProgressMixer26AddRefEv,"axG",@progbits,_ZN8NArchive4NZip17CMtProgressMixer26AddRefEv,comdat
	.weak	_ZN8NArchive4NZip17CMtProgressMixer26AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip17CMtProgressMixer26AddRefEv,@function
_ZN8NArchive4NZip17CMtProgressMixer26AddRefEv: # @_ZN8NArchive4NZip17CMtProgressMixer26AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end20:
	.size	_ZN8NArchive4NZip17CMtProgressMixer26AddRefEv, .Lfunc_end20-_ZN8NArchive4NZip17CMtProgressMixer26AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip17CMtProgressMixer27ReleaseEv,"axG",@progbits,_ZN8NArchive4NZip17CMtProgressMixer27ReleaseEv,comdat
	.weak	_ZN8NArchive4NZip17CMtProgressMixer27ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip17CMtProgressMixer27ReleaseEv,@function
_ZN8NArchive4NZip17CMtProgressMixer27ReleaseEv: # @_ZN8NArchive4NZip17CMtProgressMixer27ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi137:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB21_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB21_2:
	popq	%rcx
	retq
.Lfunc_end21:
	.size	_ZN8NArchive4NZip17CMtProgressMixer27ReleaseEv, .Lfunc_end21-_ZN8NArchive4NZip17CMtProgressMixer27ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip17CMtProgressMixer2D2Ev,"axG",@progbits,_ZN8NArchive4NZip17CMtProgressMixer2D2Ev,comdat
	.weak	_ZN8NArchive4NZip17CMtProgressMixer2D2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip17CMtProgressMixer2D2Ev,@function
_ZN8NArchive4NZip17CMtProgressMixer2D2Ev: # @_ZN8NArchive4NZip17CMtProgressMixer2D2Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi138:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi139:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi140:
	.cfi_def_cfa_offset 32
.Lcfi141:
	.cfi_offset %rbx, -24
.Lcfi142:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN8NArchive4NZip17CMtProgressMixer2E+16, (%rbx)
	leaq	80(%rbx), %rdi
	callq	pthread_mutex_destroy
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp543:
	callq	*16(%rax)
.Ltmp544:
.LBB22_2:                               # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp549:
	callq	*16(%rax)
.Ltmp550:
.LBB22_4:                               # %_ZN9CMyComPtrI9IProgressED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB22_7:
.Ltmp551:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB22_5:
.Ltmp545:
	movq	%rax, %r14
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_8
# BB#6:
	movq	(%rdi), %rax
.Ltmp546:
	callq	*16(%rax)
.Ltmp547:
.LBB22_8:                               # %_ZN9CMyComPtrI9IProgressED2Ev.exit5
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB22_9:
.Ltmp548:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end22:
	.size	_ZN8NArchive4NZip17CMtProgressMixer2D2Ev, .Lfunc_end22-_ZN8NArchive4NZip17CMtProgressMixer2D2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp543-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp544-.Ltmp543       #   Call between .Ltmp543 and .Ltmp544
	.long	.Ltmp545-.Lfunc_begin6  #     jumps to .Ltmp545
	.byte	0                       #   On action: cleanup
	.long	.Ltmp549-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp550-.Ltmp549       #   Call between .Ltmp549 and .Ltmp550
	.long	.Ltmp551-.Lfunc_begin6  #     jumps to .Ltmp551
	.byte	0                       #   On action: cleanup
	.long	.Ltmp550-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp546-.Ltmp550       #   Call between .Ltmp550 and .Ltmp546
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp546-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp547-.Ltmp546       #   Call between .Ltmp546 and .Ltmp547
	.long	.Ltmp548-.Lfunc_begin6  #     jumps to .Ltmp548
	.byte	1                       #   On action: 1
	.long	.Ltmp547-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Lfunc_end22-.Ltmp547   #   Call between .Ltmp547 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive4NZip17CMtProgressMixer2D0Ev,"axG",@progbits,_ZN8NArchive4NZip17CMtProgressMixer2D0Ev,comdat
	.weak	_ZN8NArchive4NZip17CMtProgressMixer2D0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip17CMtProgressMixer2D0Ev,@function
_ZN8NArchive4NZip17CMtProgressMixer2D0Ev: # @_ZN8NArchive4NZip17CMtProgressMixer2D0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi143:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi144:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi145:
	.cfi_def_cfa_offset 32
.Lcfi146:
	.cfi_offset %rbx, -24
.Lcfi147:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN8NArchive4NZip17CMtProgressMixer2E+16, (%rbx)
	leaq	80(%rbx), %rdi
	callq	pthread_mutex_destroy
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp552:
	callq	*16(%rax)
.Ltmp553:
.LBB23_2:                               # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit.i
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp558:
	callq	*16(%rax)
.Ltmp559:
.LBB23_4:                               # %_ZN8NArchive4NZip17CMtProgressMixer2D2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB23_8:
.Ltmp560:
	movq	%rax, %r14
	jmp	.LBB23_9
.LBB23_5:
.Ltmp554:
	movq	%rax, %r14
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_9
# BB#6:
	movq	(%rdi), %rax
.Ltmp555:
	callq	*16(%rax)
.Ltmp556:
.LBB23_9:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB23_7:
.Ltmp557:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end23:
	.size	_ZN8NArchive4NZip17CMtProgressMixer2D0Ev, .Lfunc_end23-_ZN8NArchive4NZip17CMtProgressMixer2D0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp552-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp553-.Ltmp552       #   Call between .Ltmp552 and .Ltmp553
	.long	.Ltmp554-.Lfunc_begin7  #     jumps to .Ltmp554
	.byte	0                       #   On action: cleanup
	.long	.Ltmp558-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp559-.Ltmp558       #   Call between .Ltmp558 and .Ltmp559
	.long	.Ltmp560-.Lfunc_begin7  #     jumps to .Ltmp560
	.byte	0                       #   On action: cleanup
	.long	.Ltmp555-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp556-.Ltmp555       #   Call between .Ltmp555 and .Ltmp556
	.long	.Ltmp557-.Lfunc_begin7  #     jumps to .Ltmp557
	.byte	1                       #   On action: 1
	.long	.Ltmp556-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Lfunc_end23-.Ltmp556   #   Call between .Ltmp556 and .Lfunc_end23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive4NZip16CMtProgressMixer14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive4NZip16CMtProgressMixer14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive4NZip16CMtProgressMixer14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip16CMtProgressMixer14QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive4NZip16CMtProgressMixer14QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive4NZip16CMtProgressMixer14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi148:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB24_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB24_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB24_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB24_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB24_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB24_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB24_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB24_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB24_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB24_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB24_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB24_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB24_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB24_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB24_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB24_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB24_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end24:
	.size	_ZN8NArchive4NZip16CMtProgressMixer14QueryInterfaceERK4GUIDPPv, .Lfunc_end24-_ZN8NArchive4NZip16CMtProgressMixer14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip16CMtProgressMixer6AddRefEv,"axG",@progbits,_ZN8NArchive4NZip16CMtProgressMixer6AddRefEv,comdat
	.weak	_ZN8NArchive4NZip16CMtProgressMixer6AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip16CMtProgressMixer6AddRefEv,@function
_ZN8NArchive4NZip16CMtProgressMixer6AddRefEv: # @_ZN8NArchive4NZip16CMtProgressMixer6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end25:
	.size	_ZN8NArchive4NZip16CMtProgressMixer6AddRefEv, .Lfunc_end25-_ZN8NArchive4NZip16CMtProgressMixer6AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip16CMtProgressMixer7ReleaseEv,"axG",@progbits,_ZN8NArchive4NZip16CMtProgressMixer7ReleaseEv,comdat
	.weak	_ZN8NArchive4NZip16CMtProgressMixer7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip16CMtProgressMixer7ReleaseEv,@function
_ZN8NArchive4NZip16CMtProgressMixer7ReleaseEv: # @_ZN8NArchive4NZip16CMtProgressMixer7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi149:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB26_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB26_2:
	popq	%rcx
	retq
.Lfunc_end26:
	.size	_ZN8NArchive4NZip16CMtProgressMixer7ReleaseEv, .Lfunc_end26-_ZN8NArchive4NZip16CMtProgressMixer7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip16CMtProgressMixerD2Ev,"axG",@progbits,_ZN8NArchive4NZip16CMtProgressMixerD2Ev,comdat
	.weak	_ZN8NArchive4NZip16CMtProgressMixerD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip16CMtProgressMixerD2Ev,@function
_ZN8NArchive4NZip16CMtProgressMixerD2Ev: # @_ZN8NArchive4NZip16CMtProgressMixerD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTVN8NArchive4NZip16CMtProgressMixerE+16, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB27_1
# BB#2:
	movq	(%rdi), %rax
	jmpq	*16(%rax)               # TAILCALL
.LBB27_1:                               # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	retq
.Lfunc_end27:
	.size	_ZN8NArchive4NZip16CMtProgressMixerD2Ev, .Lfunc_end27-_ZN8NArchive4NZip16CMtProgressMixerD2Ev
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip16CMtProgressMixerD0Ev,"axG",@progbits,_ZN8NArchive4NZip16CMtProgressMixerD0Ev,comdat
	.weak	_ZN8NArchive4NZip16CMtProgressMixerD0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip16CMtProgressMixerD0Ev,@function
_ZN8NArchive4NZip16CMtProgressMixerD0Ev: # @_ZN8NArchive4NZip16CMtProgressMixerD0Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi150:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi151:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi152:
	.cfi_def_cfa_offset 32
.Lcfi153:
	.cfi_offset %rbx, -24
.Lcfi154:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN8NArchive4NZip16CMtProgressMixerE+16, (%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB28_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp561:
	callq	*16(%rax)
.Ltmp562:
.LBB28_2:                               # %_ZN8NArchive4NZip16CMtProgressMixerD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB28_3:
.Ltmp563:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end28:
	.size	_ZN8NArchive4NZip16CMtProgressMixerD0Ev, .Lfunc_end28-_ZN8NArchive4NZip16CMtProgressMixerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table28:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp561-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp562-.Ltmp561       #   Call between .Ltmp561 and .Ltmp562
	.long	.Ltmp563-.Lfunc_begin8  #     jumps to .Ltmp563
	.byte	0                       #   On action: cleanup
	.long	.Ltmp562-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Lfunc_end28-.Ltmp562   #   Call between .Ltmp562 and .Lfunc_end28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NArchive4NZip15CCacheOutStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive4NZip15CCacheOutStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive4NZip15CCacheOutStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip15CCacheOutStream14QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive4NZip15CCacheOutStream14QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive4NZip15CCacheOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi155:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB29_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB29_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB29_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB29_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB29_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB29_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB29_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB29_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB29_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB29_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB29_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB29_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB29_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB29_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB29_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB29_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB29_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end29:
	.size	_ZN8NArchive4NZip15CCacheOutStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end29-_ZN8NArchive4NZip15CCacheOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip15CCacheOutStream6AddRefEv,"axG",@progbits,_ZN8NArchive4NZip15CCacheOutStream6AddRefEv,comdat
	.weak	_ZN8NArchive4NZip15CCacheOutStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip15CCacheOutStream6AddRefEv,@function
_ZN8NArchive4NZip15CCacheOutStream6AddRefEv: # @_ZN8NArchive4NZip15CCacheOutStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end30:
	.size	_ZN8NArchive4NZip15CCacheOutStream6AddRefEv, .Lfunc_end30-_ZN8NArchive4NZip15CCacheOutStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip15CCacheOutStream7ReleaseEv,"axG",@progbits,_ZN8NArchive4NZip15CCacheOutStream7ReleaseEv,comdat
	.weak	_ZN8NArchive4NZip15CCacheOutStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip15CCacheOutStream7ReleaseEv,@function
_ZN8NArchive4NZip15CCacheOutStream7ReleaseEv: # @_ZN8NArchive4NZip15CCacheOutStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi156:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB31_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB31_2:
	popq	%rcx
	retq
.Lfunc_end31:
	.size	_ZN8NArchive4NZip15CCacheOutStream7ReleaseEv, .Lfunc_end31-_ZN8NArchive4NZip15CCacheOutStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip5CItemD2Ev,"axG",@progbits,_ZN8NArchive4NZip5CItemD2Ev,comdat
	.weak	_ZN8NArchive4NZip5CItemD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip5CItemD2Ev,@function
_ZN8NArchive4NZip5CItemD2Ev:            # @_ZN8NArchive4NZip5CItemD2Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r15
.Lcfi157:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi158:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi159:
	.cfi_def_cfa_offset 32
.Lcfi160:
	.cfi_offset %rbx, -32
.Lcfi161:
	.cfi_offset %r14, -24
.Lcfi162:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	$_ZTV7CBufferIhE+16, 152(%r15)
	movq	168(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB32_2
# BB#1:
	callq	_ZdaPv
.LBB32_2:                               # %_ZN7CBufferIhED2Ev.exit
	leaq	120(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 120(%r15)
.Ltmp564:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp565:
# BB#3:                                 # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev.exit.i
.Ltmp570:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp571:
# BB#4:                                 # %_ZN8NArchive4NZip11CExtraBlockD2Ev.exit
	leaq	48(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 48(%r15)
.Ltmp582:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp583:
# BB#5:                                 # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev.exit.i.i
.Ltmp588:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp589:
# BB#6:                                 # %_ZN8NArchive4NZip11CExtraBlockD2Ev.exit.i
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB32_16
# BB#7:
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZdaPv                  # TAILCALL
.LBB32_16:                              # %_ZN8NArchive4NZip10CLocalItemD2Ev.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB32_12:
.Ltmp590:
	movq	%rax, %r14
	jmp	.LBB32_13
.LBB32_10:
.Ltmp584:
	movq	%rax, %r14
.Ltmp585:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp586:
	jmp	.LBB32_13
.LBB32_11:
.Ltmp587:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB32_17:
.Ltmp572:
	movq	%rax, %r14
	jmp	.LBB32_18
.LBB32_8:
.Ltmp566:
	movq	%rax, %r14
.Ltmp567:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp568:
.LBB32_18:                              # %.body
	leaq	48(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 48(%r15)
.Ltmp573:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp574:
# BB#19:                                # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev.exit.i.i2
.Ltmp579:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp580:
.LBB32_13:                              # %.body.i
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB32_15
# BB#14:
	callq	_ZdaPv
.LBB32_15:                              # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB32_9:
.Ltmp569:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB32_22:
.Ltmp581:
	movq	%rax, %r14
	jmp	.LBB32_23
.LBB32_20:
.Ltmp575:
	movq	%rax, %r14
.Ltmp576:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp577:
.LBB32_23:                              # %.body.i5
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB32_25
# BB#24:
	callq	_ZdaPv
.LBB32_25:                              # %.body7
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB32_21:
.Ltmp578:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end32:
	.size	_ZN8NArchive4NZip5CItemD2Ev, .Lfunc_end32-_ZN8NArchive4NZip5CItemD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Ltmp564-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp565-.Ltmp564       #   Call between .Ltmp564 and .Ltmp565
	.long	.Ltmp566-.Lfunc_begin9  #     jumps to .Ltmp566
	.byte	0                       #   On action: cleanup
	.long	.Ltmp570-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp571-.Ltmp570       #   Call between .Ltmp570 and .Ltmp571
	.long	.Ltmp572-.Lfunc_begin9  #     jumps to .Ltmp572
	.byte	0                       #   On action: cleanup
	.long	.Ltmp582-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp583-.Ltmp582       #   Call between .Ltmp582 and .Ltmp583
	.long	.Ltmp584-.Lfunc_begin9  #     jumps to .Ltmp584
	.byte	0                       #   On action: cleanup
	.long	.Ltmp588-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Ltmp589-.Ltmp588       #   Call between .Ltmp588 and .Ltmp589
	.long	.Ltmp590-.Lfunc_begin9  #     jumps to .Ltmp590
	.byte	0                       #   On action: cleanup
	.long	.Ltmp585-.Lfunc_begin9  # >> Call Site 5 <<
	.long	.Ltmp586-.Ltmp585       #   Call between .Ltmp585 and .Ltmp586
	.long	.Ltmp587-.Lfunc_begin9  #     jumps to .Ltmp587
	.byte	1                       #   On action: 1
	.long	.Ltmp567-.Lfunc_begin9  # >> Call Site 6 <<
	.long	.Ltmp568-.Ltmp567       #   Call between .Ltmp567 and .Ltmp568
	.long	.Ltmp569-.Lfunc_begin9  #     jumps to .Ltmp569
	.byte	1                       #   On action: 1
	.long	.Ltmp573-.Lfunc_begin9  # >> Call Site 7 <<
	.long	.Ltmp574-.Ltmp573       #   Call between .Ltmp573 and .Ltmp574
	.long	.Ltmp575-.Lfunc_begin9  #     jumps to .Ltmp575
	.byte	1                       #   On action: 1
	.long	.Ltmp579-.Lfunc_begin9  # >> Call Site 8 <<
	.long	.Ltmp580-.Ltmp579       #   Call between .Ltmp579 and .Ltmp580
	.long	.Ltmp581-.Lfunc_begin9  #     jumps to .Ltmp581
	.byte	1                       #   On action: 1
	.long	.Ltmp580-.Lfunc_begin9  # >> Call Site 9 <<
	.long	.Ltmp576-.Ltmp580       #   Call between .Ltmp580 and .Ltmp576
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp576-.Lfunc_begin9  # >> Call Site 10 <<
	.long	.Ltmp577-.Ltmp576       #   Call between .Ltmp576 and .Ltmp577
	.long	.Ltmp578-.Lfunc_begin9  #     jumps to .Ltmp578
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI33_0:
	.zero	16
	.section	.text._ZN8NArchive4NZip22CCompressionMethodModeC2Ev,"axG",@progbits,_ZN8NArchive4NZip22CCompressionMethodModeC2Ev,comdat
	.weak	_ZN8NArchive4NZip22CCompressionMethodModeC2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip22CCompressionMethodModeC2Ev,@function
_ZN8NArchive4NZip22CCompressionMethodModeC2Ev: # @_ZN8NArchive4NZip22CCompressionMethodModeC2Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r15
.Lcfi163:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi164:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 32
.Lcfi166:
	.cfi_offset %rbx, -32
.Lcfi167:
	.cfi_offset %r14, -24
.Lcfi168:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	movq	$1, 24(%rbx)
	movq	$_ZTV13CRecordVectorIhE+16, (%rbx)
	movups	%xmm0, 32(%rbx)
.Ltmp591:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp592:
# BB#1:
	movq	%r14, 32(%rbx)
	movl	$0, (%r14)
	movl	$4, 44(%rbx)
	movb	$0, 60(%rbx)
	movb	$0, 84(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 88(%rbx)
.Ltmp594:
	movl	$4, %edi
	callq	_Znam
.Ltmp595:
# BB#2:
	movq	%rax, 88(%rbx)
	movb	$0, (%rax)
	movl	$4, 100(%rbx)
	movw	$768, 104(%rbx)         # imm = 0x300
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB33_4:
.Ltmp596:
	movq	%rax, %r15
	movq	%r14, %rdi
	callq	_ZdaPv
	jmp	.LBB33_5
.LBB33_3:
.Ltmp593:
	movq	%rax, %r15
.LBB33_5:                               # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp597:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp598:
# BB#6:
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB33_7:
.Ltmp599:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end33:
	.size	_ZN8NArchive4NZip22CCompressionMethodModeC2Ev, .Lfunc_end33-_ZN8NArchive4NZip22CCompressionMethodModeC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table33:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp591-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp592-.Ltmp591       #   Call between .Ltmp591 and .Ltmp592
	.long	.Ltmp593-.Lfunc_begin10 #     jumps to .Ltmp593
	.byte	0                       #   On action: cleanup
	.long	.Ltmp594-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp595-.Ltmp594       #   Call between .Ltmp594 and .Ltmp595
	.long	.Ltmp596-.Lfunc_begin10 #     jumps to .Ltmp596
	.byte	0                       #   On action: cleanup
	.long	.Ltmp597-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp598-.Ltmp597       #   Call between .Ltmp597 and .Ltmp598
	.long	.Ltmp599-.Lfunc_begin10 #     jumps to .Ltmp599
	.byte	1                       #   On action: 1
	.long	.Ltmp598-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Lfunc_end33-.Ltmp598   #   Call between .Ltmp598 and .Lfunc_end33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive4NZip22CCompressionMethodModeaSERKS1_,"axG",@progbits,_ZN8NArchive4NZip22CCompressionMethodModeaSERKS1_,comdat
	.weak	_ZN8NArchive4NZip22CCompressionMethodModeaSERKS1_
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip22CCompressionMethodModeaSERKS1_,@function
_ZN8NArchive4NZip22CCompressionMethodModeaSERKS1_: # @_ZN8NArchive4NZip22CCompressionMethodModeaSERKS1_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi169:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi170:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi171:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi172:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi173:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi174:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi175:
	.cfi_def_cfa_offset 64
.Lcfi176:
	.cfi_offset %rbx, -56
.Lcfi177:
	.cfi_offset %r12, -48
.Lcfi178:
	.cfi_offset %r13, -40
.Lcfi179:
	.cfi_offset %r14, -32
.Lcfi180:
	.cfi_offset %r15, -24
.Lcfi181:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	12(%r14), %r12d
	movl	12(%r15), %esi
	addl	%r12d, %esi
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%r12d, %r12d
	jle	.LBB34_3
# BB#1:                                 # %.lr.ph.i.i
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB34_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movzbl	(%rax,%rbp), %ebx
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movb	%bl, (%rax,%rcx)
	incl	12(%r15)
	incq	%rbp
	cmpq	%rbp, %r12
	jne	.LBB34_2
.LBB34_3:                               # %_ZN13CRecordVectorIhEaSERKS0_.exit
	cmpq	%r15, %r14
	je	.LBB34_4
# BB#5:
	movl	$0, 40(%r15)
	movq	32(%r15), %rbx
	movl	$0, (%rbx)
	movslq	40(%r14), %r12
	incq	%r12
	movl	44(%r15), %ebp
	cmpl	%ebp, %r12d
	je	.LBB34_10
# BB#6:
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB34_9
# BB#7:
	testl	%ebp, %ebp
	jle	.LBB34_9
# BB#8:                                 # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	40(%r15), %rax
.LBB34_9:                               # %._crit_edge16.i.i
	movq	%r13, 32(%r15)
	movl	$0, (%r13,%rax,4)
	movl	%r12d, 44(%r15)
	movq	%r13, %rbx
.LBB34_10:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	32(%r14), %rax
	.p2align	4, 0x90
.LBB34_11:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB34_11
# BB#12:                                # %_ZN11CStringBaseIwEaSERKS0_.exit
	cmpq	%r15, %r14
	movl	40(%r14), %eax
	movl	%eax, 40(%r15)
	movq	77(%r14), %rax
	movq	%rax, 77(%r15)
	movups	48(%r14), %xmm0
	movups	64(%r14), %xmm1
	movups	%xmm1, 64(%r15)
	movups	%xmm0, 48(%r15)
	je	.LBB34_42
# BB#13:
	movl	$0, 96(%r15)
	movq	88(%r15), %rax
	movb	$0, (%rax)
	movslq	96(%r14), %rax
	leaq	1(%rax), %r12
	movl	100(%r15), %ebp
	cmpl	%ebp, %r12d
	jne	.LBB34_15
# BB#14:                                # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
	movq	88(%r15), %rbx
	jmp	.LBB34_39
.LBB34_4:                               # %_ZN11CStringBaseIwEaSERKS0_.exit.thread
	movq	77(%r14), %rax
	movq	%rax, 77(%r15)
	movups	48(%r14), %xmm0
	movups	64(%r14), %xmm1
	movups	%xmm1, 64(%r15)
	movups	%xmm0, 48(%r15)
	jmp	.LBB34_42
.LBB34_15:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r12, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	testl	%ebp, %ebp
	jle	.LBB34_38
# BB#16:                                # %.preheader.i.i
	movslq	96(%r15), %rbp
	testq	%rbp, %rbp
	movq	88(%r15), %rdi
	jle	.LBB34_36
# BB#17:                                # %.lr.ph.preheader.i.i
	cmpl	$31, %ebp
	jbe	.LBB34_18
# BB#25:                                # %min.iters.checked
	movq	%rbp, %rcx
	andq	$-32, %rcx
	je	.LBB34_18
# BB#26:                                # %vector.memcheck
	leaq	(%rdi,%rbp), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB34_28
# BB#27:                                # %vector.memcheck
	leaq	(%rbx,%rbp), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB34_28
.LBB34_18:
	xorl	%ecx, %ecx
.LBB34_19:                              # %.lr.ph.i.i9.preheader
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rbp), %rax
	subq	%rcx, %rax
	andq	$7, %rsi
	je	.LBB34_22
# BB#20:                                # %.lr.ph.i.i9.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB34_21:                              # %.lr.ph.i.i9.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB34_21
.LBB34_22:                              # %.lr.ph.i.i9.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB34_37
# BB#23:                                # %.lr.ph.i.i9.preheader.new
	subq	%rcx, %rbp
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB34_24:                              # %.lr.ph.i.i9
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rbp
	jne	.LBB34_24
	jmp	.LBB34_37
.LBB34_36:                              # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB34_38
.LBB34_37:                              # %._crit_edge.thread.i.i10
	callq	_ZdaPv
.LBB34_38:                              # %._crit_edge17.i.i
	movq	%rbx, 88(%r15)
	movslq	96(%r15), %rax
	movb	$0, (%rbx,%rax)
	movl	%r12d, 100(%r15)
.LBB34_39:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
	movq	88(%r14), %rax
	.p2align	4, 0x90
.LBB34_40:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB34_40
# BB#41:                                # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i
	movl	96(%r14), %eax
	movl	%eax, 96(%r15)
.LBB34_42:                              # %_ZN11CStringBaseIcEaSERKS0_.exit
	movzwl	104(%r14), %eax
	movw	%ax, 104(%r15)
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB34_28:                              # %vector.body.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB34_29
# BB#30:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB34_31:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%rbx,%rdx)
	movups	%xmm1, 16(%rbx,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB34_31
	jmp	.LBB34_32
.LBB34_29:
	xorl	%edx, %edx
.LBB34_32:                              # %vector.body.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB34_35
# BB#33:                                # %vector.body.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbx,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
.LBB34_34:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB34_34
.LBB34_35:                              # %middle.block
	cmpq	%rcx, %rbp
	jne	.LBB34_19
	jmp	.LBB34_37
.Lfunc_end34:
	.size	_ZN8NArchive4NZip22CCompressionMethodModeaSERKS1_, .Lfunc_end34-_ZN8NArchive4NZip22CCompressionMethodModeaSERKS1_
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip11CThreadInfoC2ERKNS0_22CCompressionMethodModeE,"axG",@progbits,_ZN8NArchive4NZip11CThreadInfoC2ERKNS0_22CCompressionMethodModeE,comdat
	.weak	_ZN8NArchive4NZip11CThreadInfoC2ERKNS0_22CCompressionMethodModeE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11CThreadInfoC2ERKNS0_22CCompressionMethodModeE,@function
_ZN8NArchive4NZip11CThreadInfoC2ERKNS0_22CCompressionMethodModeE: # @_ZN8NArchive4NZip11CThreadInfoC2ERKNS0_22CCompressionMethodModeE
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r15
.Lcfi182:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi183:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi184:
	.cfi_def_cfa_offset 32
.Lcfi185:
	.cfi_offset %rbx, -32
.Lcfi186:
	.cfi_offset %r14, -24
.Lcfi187:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$0, 8(%rbx)
	leaq	16(%rbx), %r14
	movl	$0, 16(%rbx)
	movq	$0, 128(%rbx)
	movq	$_ZTVN8NWindows16NSynchronization19CAutoResetEventWFMOE+16, 120(%rbx)
	movb	$0, 144(%rbx)
	leaq	192(%rbx), %rdi
	xorps	%xmm0, %xmm0
	movups	%xmm0, 168(%rbx)
	movups	%xmm0, 152(%rbx)
	movq	$0, 184(%rbx)
.Ltmp600:
	callq	_ZN8NArchive4NZip10CAddCommonC1ERKNS0_22CCompressionMethodModeE
.Ltmp601:
# BB#1:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB35_2:
.Ltmp602:
	movq	%rax, %r15
	movq	184(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB35_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp603:
	callq	*16(%rax)
.Ltmp604:
.LBB35_4:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	176(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB35_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp605:
	callq	*16(%rax)
.Ltmp606:
.LBB35_6:                               # %_ZN9CMyComPtrI10IOutStreamED2Ev.exit
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB35_8
# BB#7:
	movq	(%rdi), %rax
.Ltmp607:
	callq	*16(%rax)
.Ltmp608:
.LBB35_8:
	movq	$_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE+16, 120(%rbx)
	movq	$0, 128(%rbx)
.Ltmp609:
	movq	%r14, %rdi
	callq	Event_Close
.Ltmp610:
# BB#9:                                 # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit
.Ltmp611:
	movq	%rbx, %rdi
	callq	Thread_Close
.Ltmp612:
# BB#10:                                # %_ZN8NWindows7CThreadD2Ev.exit
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB35_11:
.Ltmp613:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end35:
	.size	_ZN8NArchive4NZip11CThreadInfoC2ERKNS0_22CCompressionMethodModeE, .Lfunc_end35-_ZN8NArchive4NZip11CThreadInfoC2ERKNS0_22CCompressionMethodModeE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table35:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\257\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp600-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp601-.Ltmp600       #   Call between .Ltmp600 and .Ltmp601
	.long	.Ltmp602-.Lfunc_begin11 #     jumps to .Ltmp602
	.byte	0                       #   On action: cleanup
	.long	.Ltmp603-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp612-.Ltmp603       #   Call between .Ltmp603 and .Ltmp612
	.long	.Ltmp613-.Lfunc_begin11 #     jumps to .Ltmp613
	.byte	1                       #   On action: 1
	.long	.Ltmp612-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Lfunc_end35-.Ltmp612   #   Call between .Ltmp612 and .Lfunc_end35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive4NZip11CThreadInfoD2Ev,"axG",@progbits,_ZN8NArchive4NZip11CThreadInfoD2Ev,comdat
	.weak	_ZN8NArchive4NZip11CThreadInfoD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11CThreadInfoD2Ev,@function
_ZN8NArchive4NZip11CThreadInfoD2Ev:     # @_ZN8NArchive4NZip11CThreadInfoD2Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r14
.Lcfi188:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi189:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi190:
	.cfi_def_cfa_offset 32
.Lcfi191:
	.cfi_offset %rbx, -24
.Lcfi192:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	192(%rbx), %rdi
.Ltmp614:
	callq	_ZN8NArchive4NZip10CAddCommonD2Ev
.Ltmp615:
# BB#1:
	movq	184(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB36_3
# BB#2:
	movq	(%rdi), %rax
.Ltmp619:
	callq	*16(%rax)
.Ltmp620:
.LBB36_3:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	176(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB36_5
# BB#4:
	movq	(%rdi), %rax
.Ltmp624:
	callq	*16(%rax)
.Ltmp625:
.LBB36_5:                               # %_ZN9CMyComPtrI10IOutStreamED2Ev.exit
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB36_7
# BB#6:
	movq	(%rdi), %rax
.Ltmp629:
	callq	*16(%rax)
.Ltmp630:
.LBB36_7:
	movq	$_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE+16, 120(%rbx)
	movq	$0, 128(%rbx)
	leaq	16(%rbx), %rdi
.Ltmp634:
	callq	Event_Close
.Ltmp635:
# BB#8:                                 # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	Thread_Close            # TAILCALL
.LBB36_18:
.Ltmp631:
	movq	%rax, %r14
	jmp	.LBB36_19
.LBB36_14:
.Ltmp626:
	movq	%rax, %r14
	jmp	.LBB36_15
.LBB36_11:
.Ltmp621:
	movq	%rax, %r14
	jmp	.LBB36_12
.LBB36_17:
.Ltmp636:
	movq	%rax, %r14
	jmp	.LBB36_20
.LBB36_9:
.Ltmp616:
	movq	%rax, %r14
	movq	184(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB36_12
# BB#10:
	movq	(%rdi), %rax
.Ltmp617:
	callq	*16(%rax)
.Ltmp618:
.LBB36_12:                              # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit11
	movq	176(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB36_15
# BB#13:
	movq	(%rdi), %rax
.Ltmp622:
	callq	*16(%rax)
.Ltmp623:
.LBB36_15:                              # %_ZN9CMyComPtrI10IOutStreamED2Ev.exit13
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB36_19
# BB#16:
	movq	(%rdi), %rax
.Ltmp627:
	callq	*16(%rax)
.Ltmp628:
.LBB36_19:
	movq	$_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE+16, 120(%rbx)
	movq	$0, 128(%rbx)
	leaq	16(%rbx), %rdi
.Ltmp632:
	callq	Event_Close
.Ltmp633:
.LBB36_20:                              # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit18
.Ltmp637:
	movq	%rbx, %rdi
	callq	Thread_Close
.Ltmp638:
# BB#21:                                # %_ZN8NWindows7CThreadD2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB36_22:
.Ltmp639:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end36:
	.size	_ZN8NArchive4NZip11CThreadInfoD2Ev, .Lfunc_end36-_ZN8NArchive4NZip11CThreadInfoD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table36:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp614-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp615-.Ltmp614       #   Call between .Ltmp614 and .Ltmp615
	.long	.Ltmp616-.Lfunc_begin12 #     jumps to .Ltmp616
	.byte	0                       #   On action: cleanup
	.long	.Ltmp619-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp620-.Ltmp619       #   Call between .Ltmp619 and .Ltmp620
	.long	.Ltmp621-.Lfunc_begin12 #     jumps to .Ltmp621
	.byte	0                       #   On action: cleanup
	.long	.Ltmp624-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp625-.Ltmp624       #   Call between .Ltmp624 and .Ltmp625
	.long	.Ltmp626-.Lfunc_begin12 #     jumps to .Ltmp626
	.byte	0                       #   On action: cleanup
	.long	.Ltmp629-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Ltmp630-.Ltmp629       #   Call between .Ltmp629 and .Ltmp630
	.long	.Ltmp631-.Lfunc_begin12 #     jumps to .Ltmp631
	.byte	0                       #   On action: cleanup
	.long	.Ltmp634-.Lfunc_begin12 # >> Call Site 5 <<
	.long	.Ltmp635-.Ltmp634       #   Call between .Ltmp634 and .Ltmp635
	.long	.Ltmp636-.Lfunc_begin12 #     jumps to .Ltmp636
	.byte	0                       #   On action: cleanup
	.long	.Ltmp635-.Lfunc_begin12 # >> Call Site 6 <<
	.long	.Ltmp617-.Ltmp635       #   Call between .Ltmp635 and .Ltmp617
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp617-.Lfunc_begin12 # >> Call Site 7 <<
	.long	.Ltmp638-.Ltmp617       #   Call between .Ltmp617 and .Ltmp638
	.long	.Ltmp639-.Lfunc_begin12 #     jumps to .Ltmp639
	.byte	1                       #   On action: 1
	.long	.Ltmp638-.Lfunc_begin12 # >> Call Site 8 <<
	.long	.Lfunc_end36-.Ltmp638   #   Call between .Ltmp638 and .Lfunc_end36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZipL13SetFileHeaderERNS0_11COutArchiveERKNS0_22CCompressionMethodModeERKNS0_11CUpdateItemERNS0_5CItemE,@function
_ZN8NArchive4NZipL13SetFileHeaderERNS0_11COutArchiveERKNS0_22CCompressionMethodModeERKNS0_11CUpdateItemERNS0_5CItemE: # @_ZN8NArchive4NZipL13SetFileHeaderERNS0_11COutArchiveERKNS0_22CCompressionMethodModeERKNS0_11CUpdateItemERNS0_5CItemE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi193:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi194:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi195:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi196:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi197:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi198:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi199:
	.cfi_def_cfa_offset 80
.Lcfi200:
	.cfi_offset %rbx, -56
.Lcfi201:
	.cfi_offset %r12, -48
.Lcfi202:
	.cfi_offset %r13, -40
.Lcfi203:
	.cfi_offset %r14, -32
.Lcfi204:
	.cfi_offset %r15, -24
.Lcfi205:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r13
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	24(%r13), %rax
	movq	%rax, 24(%rbx)
	movw	$0, 2(%rbx)
	cmpb	$0, 1(%r13)
	je	.LBB37_32
# BB#1:
	movb	2(%r13), %bpl
	cmpq	%rbx, %r13
	je	.LBB37_31
# BB#2:
	movq	%r14, %r15
	leaq	32(%r13), %rcx
	movl	$0, 40(%rbx)
	movq	32(%rbx), %rax
	movb	$0, (%rax)
	movslq	40(%r13), %rax
	leaq	1(%rax), %rdx
	movl	44(%rbx), %r14d
	cmpl	%r14d, %edx
	movb	%bpl, 7(%rsp)           # 1-byte Spill
	jne	.LBB37_4
# BB#3:                                 # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
	leaq	32(%rbx), %rax
	movq	(%rax), %rbp
	movq	%r15, %r14
	jmp	.LBB37_28
.LBB37_32:
	movq	%rbx, %rdi
	callq	_ZNK8NArchive4NZip5CItem5IsDirEv
	movl	%eax, %ebp
	jmp	.LBB37_33
.LBB37_4:
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
	testl	%r14d, %r14d
	movq	%r15, %r14
	jle	.LBB37_27
# BB#5:                                 # %.preheader.i.i
	movslq	40(%rbx), %r9
	testq	%r9, %r9
	movq	32(%rbx), %rdi
	jle	.LBB37_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	cmpl	$31, %r9d
	jbe	.LBB37_7
# BB#14:                                # %min.iters.checked
	movq	%r9, %rcx
	andq	$-32, %rcx
	je	.LBB37_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%r9), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB37_17
# BB#16:                                # %vector.memcheck
	leaq	(%rbp,%r9), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB37_17
.LBB37_7:
	xorl	%ecx, %ecx
.LBB37_8:                               # %.lr.ph.i.i.preheader
	movl	%r9d, %esi
	subl	%ecx, %esi
	leaq	-1(%r9), %r8
	subq	%rcx, %r8
	andq	$7, %rsi
	je	.LBB37_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB37_10:                              # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbp,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB37_10
.LBB37_11:                              # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %r8
	jb	.LBB37_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %r9
	leaq	7(%rbp,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB37_13:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r9
	jne	.LBB37_13
	jmp	.LBB37_26
.LBB37_25:                              # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB37_27
.LBB37_26:                              # %._crit_edge.thread.i.i
	callq	_ZdaPv
.LBB37_27:                              # %._crit_edge17.i.i
	movq	%rbp, 32(%rbx)
	movslq	40(%rbx), %rax
	movb	$0, (%rbp,%rax)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 44(%rbx)
	movq	16(%rsp), %rcx          # 8-byte Reload
.LBB37_28:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
	movq	(%rcx), %rax
	.p2align	4, 0x90
.LBB37_29:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbp)
	incq	%rbp
	testb	%cl, %cl
	jne	.LBB37_29
# BB#30:                                # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i
	movl	40(%r13), %eax
	movl	%eax, 40(%rbx)
	movb	7(%rsp), %bpl           # 1-byte Reload
.LBB37_31:                              # %_ZN11CStringBaseIcEaSERKS0_.exit
	movzbl	4(%r13), %esi
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NZip10CLocalItem7SetUtf8Eb
	movl	16(%r13), %eax
	movl	%eax, 84(%rbx)
	movl	20(%r13), %eax
	movl	%eax, 8(%rbx)
	movq	48(%r13), %rax
	movq	%rax, 96(%rbx)
	movq	56(%r13), %rax
	movq	%rax, 104(%rbx)
	movq	64(%r13), %rax
	movq	%rax, 112(%rbx)
	movb	3(%r13), %al
	movb	%al, 178(%rbx)
.LBB37_33:
	movq	64(%r12), %rax
	movq	%rax, 88(%rbx)
	movb	$3, 81(%rbx)
	movb	$63, 80(%rbx)
	movb	$3, 1(%rbx)
	movw	$0, 82(%rbx)
	testb	%bpl, %bpl
	je	.LBB37_35
# BB#34:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NZip10CLocalItem12SetEncryptedEb
	movb	$20, (%rbx)
	movw	$0, 4(%rbx)
	movq	$0, 16(%rbx)
	movl	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB37_35:
	movzbl	84(%r14), %esi
	movq	%rbx, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN8NArchive4NZip10CLocalItem12SetEncryptedEb # TAILCALL
.LBB37_17:                              # %vector.body.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB37_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB37_20:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%rbp,%rdx)
	movups	%xmm1, 16(%rbp,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB37_20
	jmp	.LBB37_21
.LBB37_18:
	xorl	%edx, %edx
.LBB37_21:                              # %vector.body.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB37_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbp,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
.LBB37_23:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB37_23
.LBB37_24:                              # %middle.block
	cmpq	%rcx, %r9
	jne	.LBB37_8
	jmp	.LBB37_26
.Lfunc_end37:
	.size	_ZN8NArchive4NZipL13SetFileHeaderERNS0_11COutArchiveERKNS0_22CCompressionMethodModeERKNS0_11CUpdateItemERNS0_5CItemE, .Lfunc_end37-_ZN8NArchive4NZipL13SetFileHeaderERNS0_11COutArchiveERKNS0_22CCompressionMethodModeERKNS0_11CUpdateItemERNS0_5CItemE
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN8NArchive4NZipL32SetItemInfoFromCompressingResultERKNS0_18CCompressingResultEbhRNS0_5CItemE,@function
_ZN8NArchive4NZipL32SetItemInfoFromCompressingResultERKNS0_18CCompressingResultEbhRNS0_5CItemE: # @_ZN8NArchive4NZipL32SetItemInfoFromCompressingResultERKNS0_18CCompressingResultEbhRNS0_5CItemE
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%rbp
.Lcfi206:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi207:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi208:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi209:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi210:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi211:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi212:
	.cfi_def_cfa_offset 64
.Lcfi213:
	.cfi_offset %rbx, -56
.Lcfi214:
	.cfi_offset %r12, -48
.Lcfi215:
	.cfi_offset %r13, -40
.Lcfi216:
	.cfi_offset %r14, -32
.Lcfi217:
	.cfi_offset %r15, -24
.Lcfi218:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movl	%edx, %r12d
	movl	%esi, %r13d
	movq	%rdi, %rbp
	movb	22(%rbp), %al
	movb	%al, (%rbx)
	movzwl	20(%rbp), %eax
	movw	%ax, 4(%rbx)
	movl	16(%rbp), %eax
	movl	%eax, 12(%rbx)
	movq	(%rbp), %rax
	movq	%rax, 24(%rbx)
	movq	8(%rbp), %rax
	movq	%rax, 16(%rbx)
	leaq	48(%rbx), %r15
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	120(%rbx), %r14
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	testb	%r13b, %r13b
	je	.LBB38_13
# BB#1:
	movzwl	20(%rbp), %ebp
	movw	$99, 4(%rbx)
	movl	$0, 12(%rbx)
	movl	$7, %edi
	callq	_Znam
	movb	$2, (%rax)
	movb	$0, 1(%rax)
	movb	$65, 2(%rax)
	movb	$69, 3(%rax)
	movb	%r12b, 4(%rax)
	movl	%ebp, %ecx
	movb	%cl, 5(%rax)
	movq	%rax, %r12
	movb	%ch, 6(%rax)  # NOREX
.Ltmp640:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp641:
# BB#2:                                 # %_ZN7CBufferIhE11SetCapacityEm.exit.i.i.i.i
	movw	$-26367, (%rbp)         # imm = 0x9901
	movq	$_ZTV7CBufferIhE+16, 8(%rbp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbp)
.Ltmp642:
	movl	$7, %edi
	callq	_Znam
.Ltmp643:
# BB#3:                                 # %_ZN8NArchive4NZip14CExtraSubBlockC2ERKS1_.exit.i
	movq	%rax, 24(%rbp)
	movq	$7, 16(%rbp)
	movq	%r12, %rdx
	movb	6(%rdx), %cl
	movb	%cl, 6(%rax)
	movzwl	4(%rdx), %ecx
	movw	%cx, 4(%rax)
	movl	(%rdx), %ecx
	movl	%ecx, (%rax)
.Ltmp645:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp646:
# BB#4:
	movq	64(%rbx), %rax
	movslq	60(%rbx), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 60(%rbx)
.Ltmp647:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp648:
# BB#5:                                 # %_ZN7CBufferIhE11SetCapacityEm.exit.i.i.i.i22
	movw	$-26367, (%rbp)         # imm = 0x9901
	movq	$_ZTV7CBufferIhE+16, 8(%rbp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbp)
.Ltmp649:
	movl	$7, %edi
	callq	_Znam
.Ltmp650:
# BB#6:                                 # %_ZN8NArchive4NZip14CExtraSubBlockC2ERKS1_.exit.i24
	movq	%rax, 24(%rbp)
	movq	$7, 16(%rbp)
	movq	%r12, %rdx
	movb	6(%rdx), %cl
	movb	%cl, 6(%rax)
	movzwl	4(%rdx), %ecx
	movw	%cx, 4(%rax)
	movl	(%rdx), %ecx
	movl	%ecx, (%rax)
.Ltmp652:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp653:
# BB#7:                                 # %_ZN8NArchive4NZip14CExtraSubBlockD2Ev.exit30
	movq	136(%rbx), %rax
	movslq	132(%rbx), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 132(%rbx)
	movq	%r12, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZdaPv                  # TAILCALL
.LBB38_13:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB38_10:
.Ltmp651:
	jmp	.LBB38_9
.LBB38_8:
.Ltmp644:
.LBB38_9:                               # %.body.thread
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB38_12
.LBB38_11:                              # %.body.thread62
.Ltmp654:
	movq	%rax, %rbx
.LBB38_12:                              # %.body.thread
	movq	%r12, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end38:
	.size	_ZN8NArchive4NZipL32SetItemInfoFromCompressingResultERKNS0_18CCompressingResultEbhRNS0_5CItemE, .Lfunc_end38-_ZN8NArchive4NZipL32SetItemInfoFromCompressingResultERKNS0_18CCompressingResultEbhRNS0_5CItemE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table38:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin13-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp640-.Lfunc_begin13 #   Call between .Lfunc_begin13 and .Ltmp640
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp640-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp641-.Ltmp640       #   Call between .Ltmp640 and .Ltmp641
	.long	.Ltmp654-.Lfunc_begin13 #     jumps to .Ltmp654
	.byte	0                       #   On action: cleanup
	.long	.Ltmp642-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Ltmp643-.Ltmp642       #   Call between .Ltmp642 and .Ltmp643
	.long	.Ltmp644-.Lfunc_begin13 #     jumps to .Ltmp644
	.byte	0                       #   On action: cleanup
	.long	.Ltmp645-.Lfunc_begin13 # >> Call Site 4 <<
	.long	.Ltmp648-.Ltmp645       #   Call between .Ltmp645 and .Ltmp648
	.long	.Ltmp654-.Lfunc_begin13 #     jumps to .Ltmp654
	.byte	0                       #   On action: cleanup
	.long	.Ltmp649-.Lfunc_begin13 # >> Call Site 5 <<
	.long	.Ltmp650-.Ltmp649       #   Call between .Ltmp649 and .Ltmp650
	.long	.Ltmp651-.Lfunc_begin13 #     jumps to .Ltmp651
	.byte	0                       #   On action: cleanup
	.long	.Ltmp652-.Lfunc_begin13 # >> Call Site 6 <<
	.long	.Ltmp653-.Ltmp652       #   Call between .Ltmp652 and .Ltmp653
	.long	.Ltmp654-.Lfunc_begin13 #     jumps to .Ltmp654
	.byte	0                       #   On action: cleanup
	.long	.Ltmp653-.Lfunc_begin13 # >> Call Site 7 <<
	.long	.Lfunc_end38-.Ltmp653   #   Call between .Ltmp653 and .Lfunc_end38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZipL17UpdateItemOldDataERNS0_11COutArchiveEP9IInStreamRKNS0_11CUpdateItemERNS0_7CItemExEP21ICompressProgressInfoRy,@function
_ZN8NArchive4NZipL17UpdateItemOldDataERNS0_11COutArchiveEP9IInStreamRKNS0_11CUpdateItemERNS0_7CItemExEP21ICompressProgressInfoRy: # @_ZN8NArchive4NZipL17UpdateItemOldDataERNS0_11COutArchiveEP9IInStreamRKNS0_11CUpdateItemERNS0_7CItemExEP21ICompressProgressInfoRy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi219:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi220:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi221:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi222:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi223:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi224:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi225:
	.cfi_def_cfa_offset 112
.Lcfi226:
	.cfi_offset %rbx, -56
.Lcfi227:
	.cfi_offset %r12, -48
.Lcfi228:
	.cfi_offset %r13, -40
.Lcfi229:
	.cfi_offset %r14, -32
.Lcfi230:
	.cfi_offset %r15, -24
.Lcfi231:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
	movq	%r8, %r12
	movq	%rcx, %r15
	movq	%rdx, %r13
	movq	%rsi, %rbp
	movq	%rdi, %r14
	cmpb	$0, 1(%r13)
	je	.LBB39_45
# BB#1:
	movl	$-2147467263, %eax      # imm = 0x80004001
	testb	$8, 2(%r15)
	jne	.LBB39_48
# BB#2:
	movl	180(%r15), %eax
	addq	88(%r15), %rax
	movzwl	184(%r15), %ecx
	addq	%rax, %rcx
	movq	16(%r15), %rax
	movq	%rcx, 8(%rsp)
	movq	%rax, 16(%rsp)
	cmpq	%r15, %r13
	je	.LBB39_32
# BB#3:
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%r12, %rbp
	leaq	32(%r13), %rcx
	movl	$0, 40(%r15)
	movq	32(%r15), %rax
	movb	$0, (%rax)
	movslq	40(%r13), %rax
	leaq	1(%rax), %rdx
	movl	44(%r15), %r12d
	cmpl	%r12d, %edx
	movq	%r14, 48(%rsp)          # 8-byte Spill
	jne	.LBB39_5
# BB#4:                                 # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
	leaq	32(%r15), %rax
	movq	(%rax), %r14
	movq	%rbp, %r12
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB39_29
.LBB39_45:
	movq	88(%r15), %rax
	movzwl	184(%r15), %ecx
	addl	180(%r15), %ecx
	addq	16(%r15), %rcx
	movzwl	2(%r15), %edx
	addl	%edx, %edx
	andl	$16, %edx
	addq	%rcx, %rdx
	movq	%rax, 8(%rsp)
	movq	%rdx, 16(%rsp)
	movq	64(%r14), %rax
	movq	%rax, 88(%r15)
	leaq	8(%rsp), %rdx
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	%r12, %rcx
	callq	_ZN8NArchive4NZipL10WriteRangeEP9IInStreamRNS0_11COutArchiveERKNS0_12CUpdateRangeEP21ICompressProgressInfo
	testl	%eax, %eax
	jne	.LBB39_48
# BB#46:                                # %.thread64
	movq	16(%rsp), %rsi
	addq	%rsi, (%rbx)
	movq	%r14, %rdi
	callq	_ZN8NArchive4NZip11COutArchive16MoveBasePositionEy
	jmp	.LBB39_47
.LBB39_5:
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rdx, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r14
	testl	%r12d, %r12d
	movq	%rbp, %r12
	jle	.LBB39_28
# BB#6:                                 # %.preheader.i.i
	movslq	40(%r15), %rbp
	testq	%rbp, %rbp
	movq	32(%r15), %rdi
	jle	.LBB39_26
# BB#7:                                 # %.lr.ph.preheader.i.i
	cmpl	$31, %ebp
	jbe	.LBB39_8
# BB#15:                                # %min.iters.checked
	movq	%rbp, %rcx
	andq	$-32, %rcx
	je	.LBB39_8
# BB#16:                                # %vector.memcheck
	leaq	(%rdi,%rbp), %rdx
	cmpq	%rdx, %r14
	jae	.LBB39_18
# BB#17:                                # %vector.memcheck
	leaq	(%r14,%rbp), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB39_18
.LBB39_8:
	xorl	%ecx, %ecx
.LBB39_9:                               # %.lr.ph.i.i62.preheader
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rbp), %r8
	subq	%rcx, %r8
	andq	$7, %rsi
	je	.LBB39_12
# BB#10:                                # %.lr.ph.i.i62.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB39_11:                              # %.lr.ph.i.i62.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%r14,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB39_11
.LBB39_12:                              # %.lr.ph.i.i62.prol.loopexit
	cmpq	$7, %r8
	jb	.LBB39_27
# BB#13:                                # %.lr.ph.i.i62.preheader.new
	subq	%rcx, %rbp
	leaq	7(%r14,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB39_14:                              # %.lr.ph.i.i62
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rbp
	jne	.LBB39_14
	jmp	.LBB39_27
.LBB39_26:                              # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB39_28
.LBB39_27:                              # %._crit_edge.thread.i.i
	callq	_ZdaPv
.LBB39_28:                              # %._crit_edge17.i.i
	movq	%r14, 32(%r15)
	movslq	40(%r15), %rax
	movb	$0, (%r14,%rax)
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%eax, 44(%r15)
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
.LBB39_29:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
	movq	(%rcx), %rax
	.p2align	4, 0x90
.LBB39_30:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%r14)
	incq	%r14
	testb	%cl, %cl
	jne	.LBB39_30
# BB#31:                                # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i
	movl	40(%r13), %eax
	movl	%eax, 40(%r15)
	movq	48(%rsp), %r14          # 8-byte Reload
.LBB39_32:                              # %_ZN11CStringBaseIcEaSERKS0_.exit
	movzbl	4(%r13), %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip10CLocalItem7SetUtf8Eb
	movl	20(%r13), %eax
	movl	%eax, 8(%r15)
	movq	48(%r13), %rax
	movq	%rax, 96(%r15)
	movq	56(%r13), %rax
	movq	%rax, 104(%r15)
	movq	64(%r13), %rax
	movq	%rax, 112(%r15)
	movb	3(%r13), %al
	movb	%al, 178(%r15)
	leaq	120(%r15), %rdi
	callq	_ZN8NArchive4NZip11CExtraBlock22RemoveUnknownSubBlocksEv
	leaq	48(%r15), %rdi
	callq	_ZN8NArchive4NZip11CExtraBlock22RemoveUnknownSubBlocksEv
	movzwl	40(%r15), %r8d
	movq	16(%r15), %rcx
	movq	24(%r15), %rdx
	movslq	60(%r15), %rsi
	testq	%rsi, %rsi
	jle	.LBB39_33
# BB#34:                                # %.lr.ph.i.i
	movq	%rbp, %r10
	movq	%rbx, %r9
	movq	64(%r15), %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB39_35:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbp,8), %rax
	movzwl	(%rax), %ebx
	cmpl	$39169, %ebx            # imm = 0x9901
	jne	.LBB39_40
# BB#36:                                #   in Loop: Header=BB39_35 Depth=1
	cmpq	$7, 16(%rax)
	jb	.LBB39_40
# BB#37:                                #   in Loop: Header=BB39_35 Depth=1
	movq	24(%rax), %rax
	cmpb	$65, 2(%rax)
	jne	.LBB39_40
# BB#38:                                #   in Loop: Header=BB39_35 Depth=1
	cmpb	$69, 3(%rax)
	je	.LBB39_39
	.p2align	4, 0x90
.LBB39_40:                              #   in Loop: Header=BB39_35 Depth=1
	incq	%rbp
	cmpq	%rsi, %rbp
	jl	.LBB39_35
# BB#41:
	xorl	%edi, %edi
.LBB39_42:                              # %_ZNK8NArchive4NZip11CExtraBlock13HasWzAesFieldEv.exit
	movq	%r9, %rbx
	movq	%r10, %rbp
	jmp	.LBB39_43
.LBB39_33:
	xorl	%edi, %edi
.LBB39_43:                              # %_ZNK8NArchive4NZip11CExtraBlock13HasWzAesFieldEv.exit
	movzwl	%r8w, %esi
	movzbl	%dil, %r8d
	movq	%r14, %rdi
	callq	_ZN8NArchive4NZip11COutArchive27PrepareWriteCompressedData2Etyyb
	movq	64(%r14), %rax
	movq	%rax, 88(%r15)
	movq	%r14, %rdi
	callq	_ZN8NArchive4NZip11COutArchive24SeekToPackedDataPositionEv
	leaq	8(%rsp), %rdx
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	%r12, %rcx
	callq	_ZN8NArchive4NZipL10WriteRangeEP9IInStreamRNS0_11COutArchiveERKNS0_12CUpdateRangeEP21ICompressProgressInfo
	testl	%eax, %eax
	jne	.LBB39_48
# BB#44:                                # %.thread
	movq	16(%rsp), %rax
	addq	%rax, (%rbx)
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_ZN8NArchive4NZip11COutArchive16WriteLocalHeaderERKNS0_10CLocalItemE
.LBB39_47:
	xorl	%eax, %eax
.LBB39_48:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB39_39:
	movb	$1, %dil
	jmp	.LBB39_42
.LBB39_18:                              # %vector.body.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB39_19
# BB#20:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
.LBB39_21:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%r14,%rdx)
	movups	%xmm1, 16(%r14,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB39_21
	jmp	.LBB39_22
.LBB39_19:
	xorl	%edx, %edx
.LBB39_22:                              # %vector.body.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB39_25
# BB#23:                                # %vector.body.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%r14,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
.LBB39_24:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB39_24
.LBB39_25:                              # %middle.block
	cmpq	%rcx, %rbp
	jne	.LBB39_9
	jmp	.LBB39_27
.Lfunc_end39:
	.size	_ZN8NArchive4NZipL17UpdateItemOldDataERNS0_11COutArchiveEP9IInStreamRKNS0_11CUpdateItemERNS0_7CItemExEP21ICompressProgressInfoRy, .Lfunc_end39-_ZN8NArchive4NZipL17UpdateItemOldDataERNS0_11COutArchiveEP9IInStreamRKNS0_11CUpdateItemERNS0_7CItemExEP21ICompressProgressInfoRy
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip8CThreadsD2Ev,"axG",@progbits,_ZN8NArchive4NZip8CThreadsD2Ev,comdat
	.weak	_ZN8NArchive4NZip8CThreadsD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip8CThreadsD2Ev,@function
_ZN8NArchive4NZip8CThreadsD2Ev:         # @_ZN8NArchive4NZip8CThreadsD2Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r15
.Lcfi232:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi233:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi234:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi235:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi236:
	.cfi_def_cfa_offset 48
.Lcfi237:
	.cfi_offset %rbx, -48
.Lcfi238:
	.cfi_offset %r12, -40
.Lcfi239:
	.cfi_offset %r13, -32
.Lcfi240:
	.cfi_offset %r14, -24
.Lcfi241:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	cmpl	$0, 12(%r14)
	jle	.LBB40_9
# BB#1:                                 # %.lr.ph
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB40_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%r12,8), %r13
	movb	$1, 144(%r13)
	movq	168(%r13), %rbx
	testq	%rbx, %rbx
	je	.LBB40_4
# BB#3:                                 #   in Loop: Header=BB40_2 Depth=1
	movl	$-2147467260, 96(%rbx)  # imm = 0x80004004
	movq	56(%rbx), %rdi
	callq	pthread_mutex_lock
	movb	$1, 65(%rbx)
	movq	56(%rbx), %r15
	leaq	40(%r15), %rdi
	callq	pthread_cond_broadcast
	movq	%r15, %rdi
	callq	pthread_mutex_unlock
.LBB40_4:                               #   in Loop: Header=BB40_2 Depth=1
	cmpl	$0, 16(%r13)
	je	.LBB40_6
# BB#5:                                 #   in Loop: Header=BB40_2 Depth=1
	leaq	16(%r13), %rdi
.Ltmp655:
	callq	Event_Set
.Ltmp656:
.LBB40_6:                               # %.noexc
                                        #   in Loop: Header=BB40_2 Depth=1
.Ltmp657:
	movq	%r13, %rdi
	callq	Thread_Wait
.Ltmp658:
# BB#7:                                 # %.noexc8
                                        #   in Loop: Header=BB40_2 Depth=1
.Ltmp659:
	movq	%r13, %rdi
	callq	Thread_Close
.Ltmp660:
# BB#8:                                 # %_ZN8NArchive4NZip11CThreadInfo13StopWaitCloseEv.exit
                                        #   in Loop: Header=BB40_2 Depth=1
	incq	%r12
	movslq	12(%r14), %rax
	cmpq	%rax, %r12
	jl	.LBB40_2
.LBB40_9:                               # %._crit_edge
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip11CThreadInfoEE+16, (%r14)
.Ltmp671:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp672:
# BB#10:                                # %_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEED2Ev.exit
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB40_11:
.Ltmp673:
	movq	%rax, %rbx
.Ltmp674:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp675:
	jmp	.LBB40_12
.LBB40_13:
.Ltmp676:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB40_14:
.Ltmp661:
	movq	%rax, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip11CThreadInfoEE+16, (%r14)
.Ltmp662:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp663:
# BB#15:
.Ltmp668:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp669:
.LBB40_12:                              # %unwind_resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB40_18:
.Ltmp670:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB40_16:
.Ltmp664:
	movq	%rax, %rbx
.Ltmp665:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp666:
# BB#19:                                # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB40_17:
.Ltmp667:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end40:
	.size	_ZN8NArchive4NZip8CThreadsD2Ev, .Lfunc_end40-_ZN8NArchive4NZip8CThreadsD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table40:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp655-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp660-.Ltmp655       #   Call between .Ltmp655 and .Ltmp660
	.long	.Ltmp661-.Lfunc_begin14 #     jumps to .Ltmp661
	.byte	0                       #   On action: cleanup
	.long	.Ltmp671-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp672-.Ltmp671       #   Call between .Ltmp671 and .Ltmp672
	.long	.Ltmp673-.Lfunc_begin14 #     jumps to .Ltmp673
	.byte	0                       #   On action: cleanup
	.long	.Ltmp672-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp674-.Ltmp672       #   Call between .Ltmp672 and .Ltmp674
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp674-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Ltmp675-.Ltmp674       #   Call between .Ltmp674 and .Ltmp675
	.long	.Ltmp676-.Lfunc_begin14 #     jumps to .Ltmp676
	.byte	1                       #   On action: 1
	.long	.Ltmp662-.Lfunc_begin14 # >> Call Site 5 <<
	.long	.Ltmp663-.Ltmp662       #   Call between .Ltmp662 and .Ltmp663
	.long	.Ltmp664-.Lfunc_begin14 #     jumps to .Ltmp664
	.byte	1                       #   On action: 1
	.long	.Ltmp668-.Lfunc_begin14 # >> Call Site 6 <<
	.long	.Ltmp669-.Ltmp668       #   Call between .Ltmp668 and .Ltmp669
	.long	.Ltmp670-.Lfunc_begin14 #     jumps to .Ltmp670
	.byte	1                       #   On action: 1
	.long	.Ltmp669-.Lfunc_begin14 # >> Call Site 7 <<
	.long	.Ltmp665-.Ltmp669       #   Call between .Ltmp669 and .Ltmp665
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp665-.Lfunc_begin14 # >> Call Site 8 <<
	.long	.Ltmp666-.Ltmp665       #   Call between .Ltmp665 and .Ltmp666
	.long	.Ltmp667-.Lfunc_begin14 #     jumps to .Ltmp667
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive4NZip8CMemRefsD2Ev,"axG",@progbits,_ZN8NArchive4NZip8CMemRefsD2Ev,comdat
	.weak	_ZN8NArchive4NZip8CMemRefsD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip8CMemRefsD2Ev,@function
_ZN8NArchive4NZip8CMemRefsD2Ev:         # @_ZN8NArchive4NZip8CMemRefsD2Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r15
.Lcfi242:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi243:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi244:
	.cfi_def_cfa_offset 32
.Lcfi245:
	.cfi_offset %rbx, -32
.Lcfi246:
	.cfi_offset %r14, -24
.Lcfi247:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	leaq	8(%r14), %r15
	cmpl	$0, 20(%r14)
	jle	.LBB41_4
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB41_2:                               # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rsi
	movq	24(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
.Ltmp677:
	callq	_ZN10CMemBlocks7FreeOptEP18CMemBlockManagerMt
.Ltmp678:
# BB#3:                                 #   in Loop: Header=BB41_2 Depth=1
	incq	%rbx
	movslq	20(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB41_2
.LBB41_4:                               # %._crit_edge
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE+16, (%r15)
.Ltmp689:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp690:
# BB#5:                                 # %_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EED2Ev.exit
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB41_6:
.Ltmp691:
	movq	%rax, %r14
.Ltmp692:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp693:
	jmp	.LBB41_7
.LBB41_8:
.Ltmp694:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB41_9:
.Ltmp679:
	movq	%rax, %r14
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE+16, (%r15)
.Ltmp680:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp681:
# BB#10:
.Ltmp686:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp687:
.LBB41_7:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB41_13:
.Ltmp688:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB41_11:
.Ltmp682:
	movq	%rax, %rbx
.Ltmp683:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp684:
# BB#14:                                # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB41_12:
.Ltmp685:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end41:
	.size	_ZN8NArchive4NZip8CMemRefsD2Ev, .Lfunc_end41-_ZN8NArchive4NZip8CMemRefsD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table41:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp677-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp678-.Ltmp677       #   Call between .Ltmp677 and .Ltmp678
	.long	.Ltmp679-.Lfunc_begin15 #     jumps to .Ltmp679
	.byte	0                       #   On action: cleanup
	.long	.Ltmp689-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Ltmp690-.Ltmp689       #   Call between .Ltmp689 and .Ltmp690
	.long	.Ltmp691-.Lfunc_begin15 #     jumps to .Ltmp691
	.byte	0                       #   On action: cleanup
	.long	.Ltmp690-.Lfunc_begin15 # >> Call Site 3 <<
	.long	.Ltmp692-.Ltmp690       #   Call between .Ltmp690 and .Ltmp692
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp692-.Lfunc_begin15 # >> Call Site 4 <<
	.long	.Ltmp693-.Ltmp692       #   Call between .Ltmp692 and .Ltmp693
	.long	.Ltmp694-.Lfunc_begin15 #     jumps to .Ltmp694
	.byte	1                       #   On action: 1
	.long	.Ltmp680-.Lfunc_begin15 # >> Call Site 5 <<
	.long	.Ltmp681-.Ltmp680       #   Call between .Ltmp680 and .Ltmp681
	.long	.Ltmp682-.Lfunc_begin15 #     jumps to .Ltmp682
	.byte	1                       #   On action: 1
	.long	.Ltmp686-.Lfunc_begin15 # >> Call Site 6 <<
	.long	.Ltmp687-.Ltmp686       #   Call between .Ltmp686 and .Ltmp687
	.long	.Ltmp688-.Lfunc_begin15 #     jumps to .Ltmp688
	.byte	1                       #   On action: 1
	.long	.Ltmp687-.Lfunc_begin15 # >> Call Site 7 <<
	.long	.Ltmp683-.Ltmp687       #   Call between .Ltmp687 and .Ltmp683
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp683-.Lfunc_begin15 # >> Call Site 8 <<
	.long	.Ltmp684-.Ltmp683       #   Call between .Ltmp683 and .Ltmp684
	.long	.Ltmp685-.Lfunc_begin15 #     jumps to .Ltmp685
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip5CItemEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip5CItemEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip5CItemEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip5CItemEED2Ev,@function
_ZN13CObjectVectorIN8NArchive4NZip5CItemEED2Ev: # @_ZN13CObjectVectorIN8NArchive4NZip5CItemEED2Ev
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%r14
.Lcfi248:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi249:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi250:
	.cfi_def_cfa_offset 32
.Lcfi251:
	.cfi_offset %rbx, -24
.Lcfi252:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip5CItemEE+16, (%rbx)
.Ltmp695:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp696:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB42_2:
.Ltmp697:
	movq	%rax, %r14
.Ltmp698:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp699:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB42_4:
.Ltmp700:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end42:
	.size	_ZN13CObjectVectorIN8NArchive4NZip5CItemEED2Ev, .Lfunc_end42-_ZN13CObjectVectorIN8NArchive4NZip5CItemEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table42:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp695-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp696-.Ltmp695       #   Call between .Ltmp695 and .Ltmp696
	.long	.Ltmp697-.Lfunc_begin16 #     jumps to .Ltmp697
	.byte	0                       #   On action: cleanup
	.long	.Ltmp696-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Ltmp698-.Ltmp696       #   Call between .Ltmp696 and .Ltmp698
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp698-.Lfunc_begin16 # >> Call Site 3 <<
	.long	.Ltmp699-.Ltmp698       #   Call between .Ltmp698 and .Ltmp699
	.long	.Ltmp700-.Lfunc_begin16 #     jumps to .Ltmp700
	.byte	1                       #   On action: 1
	.long	.Ltmp699-.Lfunc_begin16 # >> Call Site 4 <<
	.long	.Lfunc_end42-.Ltmp699   #   Call between .Ltmp699 and .Lfunc_end42
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive4NZip10CAddCommonD2Ev,"axG",@progbits,_ZN8NArchive4NZip10CAddCommonD2Ev,comdat
	.weak	_ZN8NArchive4NZip10CAddCommonD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CAddCommonD2Ev,@function
_ZN8NArchive4NZip10CAddCommonD2Ev:      # @_ZN8NArchive4NZip10CAddCommonD2Ev
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%r14
.Lcfi253:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi254:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi255:
	.cfi_def_cfa_offset 32
.Lcfi256:
	.cfi_offset %rbx, -24
.Lcfi257:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB43_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp701:
	callq	*16(%rax)
.Ltmp702:
.LBB43_2:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	128(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB43_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp706:
	callq	*16(%rax)
.Ltmp707:
.LBB43_4:                               # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB43_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp711:
	callq	*16(%rax)
.Ltmp712:
.LBB43_6:                               # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit6
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB43_8
# BB#7:
	callq	_ZdaPv
.LBB43_8:                               # %_ZN11CStringBaseIcED2Ev.exit.i
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB43_10
# BB#9:
	callq	_ZdaPv
.LBB43_10:                              # %_ZN8NArchive4NZip22CCompressionMethodModeD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB43_13:
.Ltmp713:
	movq	%rax, %r14
	jmp	.LBB43_17
.LBB43_14:
.Ltmp708:
	movq	%rax, %r14
	jmp	.LBB43_15
.LBB43_11:
.Ltmp703:
	movq	%rax, %r14
	movq	128(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB43_15
# BB#12:
	movq	(%rdi), %rax
.Ltmp704:
	callq	*16(%rax)
.Ltmp705:
.LBB43_15:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit8
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB43_17
# BB#16:
	movq	(%rdi), %rax
.Ltmp709:
	callq	*16(%rax)
.Ltmp710:
.LBB43_17:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit10
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB43_19
# BB#18:
	callq	_ZdaPv
.LBB43_19:                              # %_ZN11CStringBaseIcED2Ev.exit.i11
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB43_21
# BB#20:
	callq	_ZdaPv
.LBB43_21:                              # %_ZN11CStringBaseIwED2Ev.exit.i
.Ltmp714:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp715:
# BB#22:                                # %_ZN8NArchive4NZip22CCompressionMethodModeD2Ev.exit13
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB43_23:
.Ltmp716:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end43:
	.size	_ZN8NArchive4NZip10CAddCommonD2Ev, .Lfunc_end43-_ZN8NArchive4NZip10CAddCommonD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table43:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp701-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp702-.Ltmp701       #   Call between .Ltmp701 and .Ltmp702
	.long	.Ltmp703-.Lfunc_begin17 #     jumps to .Ltmp703
	.byte	0                       #   On action: cleanup
	.long	.Ltmp706-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Ltmp707-.Ltmp706       #   Call between .Ltmp706 and .Ltmp707
	.long	.Ltmp708-.Lfunc_begin17 #     jumps to .Ltmp708
	.byte	0                       #   On action: cleanup
	.long	.Ltmp711-.Lfunc_begin17 # >> Call Site 3 <<
	.long	.Ltmp712-.Ltmp711       #   Call between .Ltmp711 and .Ltmp712
	.long	.Ltmp713-.Lfunc_begin17 #     jumps to .Ltmp713
	.byte	0                       #   On action: cleanup
	.long	.Ltmp712-.Lfunc_begin17 # >> Call Site 4 <<
	.long	.Ltmp704-.Ltmp712       #   Call between .Ltmp712 and .Ltmp704
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp704-.Lfunc_begin17 # >> Call Site 5 <<
	.long	.Ltmp715-.Ltmp704       #   Call between .Ltmp704 and .Ltmp715
	.long	.Ltmp716-.Lfunc_begin17 #     jumps to .Ltmp716
	.byte	1                       #   On action: 1
	.long	.Ltmp715-.Lfunc_begin17 # >> Call Site 6 <<
	.long	.Lfunc_end43-.Ltmp715   #   Call between .Ltmp715 and .Lfunc_end43
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI44_0:
	.zero	16
	.section	.text._ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_,@function
_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_: # @_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:
	pushq	%rbp
.Lcfi258:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi259:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi260:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi261:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi262:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi263:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi264:
	.cfi_def_cfa_offset 64
.Lcfi265:
	.cfi_offset %rbx, -56
.Lcfi266:
	.cfi_offset %r12, -48
.Lcfi267:
	.cfi_offset %r13, -40
.Lcfi268:
	.cfi_offset %r14, -32
.Lcfi269:
	.cfi_offset %r15, -24
.Lcfi270:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	%rbx, (%rsp)            # 8-byte Spill
	movl	12(%rbx), %r13d
	movl	12(%r15), %esi
	addl	%r13d, %esi
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%r13d, %r13d
	jle	.LBB44_6
# BB#1:                                 # %.lr.ph.i
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB44_2:                               # =>This Inner Loop Header: Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax,%rbp,8), %r14
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movzwl	(%r14), %eax
	movw	%ax, (%rbx)
	movq	$_ZTV7CBufferIhE+16, 8(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movq	16(%r14), %r12
	testq	%r12, %r12
	je	.LBB44_5
# BB#3:                                 # %_ZN7CBufferIhE11SetCapacityEm.exit.i.i.i.i
                                        #   in Loop: Header=BB44_2 Depth=1
.Ltmp717:
	movq	%r12, %rdi
	callq	_Znam
.Ltmp718:
# BB#4:                                 # %.noexc.i
                                        #   in Loop: Header=BB44_2 Depth=1
	movq	%rax, 24(%rbx)
	movq	%r12, 16(%rbx)
	movq	24(%r14), %rsi
	movq	%rax, %rdi
	movq	%r12, %rdx
	callq	memmove
.LBB44_5:                               # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE3AddERKS2_.exit
                                        #   in Loop: Header=BB44_2 Depth=1
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	incq	%rbp
	cmpq	%rbp, %r13
	jne	.LBB44_2
.LBB44_6:                               # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEpLERKS3_.exit
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB44_7:
.Ltmp719:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end44:
	.size	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_, .Lfunc_end44-_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table44:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin18-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp717-.Lfunc_begin18 #   Call between .Lfunc_begin18 and .Ltmp717
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp717-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Ltmp718-.Ltmp717       #   Call between .Ltmp717 and .Ltmp718
	.long	.Ltmp719-.Lfunc_begin18 #     jumps to .Ltmp719
	.byte	0                       #   On action: cleanup
	.long	.Ltmp718-.Lfunc_begin18 # >> Call Site 3 <<
	.long	.Lfunc_end44-.Ltmp718   #   Call between .Ltmp718 and .Lfunc_end44
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev,@function
_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev: # @_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev
.Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception19
# BB#0:
	pushq	%r14
.Lcfi271:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi272:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi273:
	.cfi_def_cfa_offset 32
.Lcfi274:
	.cfi_offset %rbx, -24
.Lcfi275:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, (%rbx)
.Ltmp720:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp721:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB45_2:
.Ltmp722:
	movq	%rax, %r14
.Ltmp723:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp724:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB45_4:
.Ltmp725:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end45:
	.size	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev, .Lfunc_end45-_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table45:
.Lexception19:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp720-.Lfunc_begin19 # >> Call Site 1 <<
	.long	.Ltmp721-.Ltmp720       #   Call between .Ltmp720 and .Ltmp721
	.long	.Ltmp722-.Lfunc_begin19 #     jumps to .Ltmp722
	.byte	0                       #   On action: cleanup
	.long	.Ltmp721-.Lfunc_begin19 # >> Call Site 2 <<
	.long	.Ltmp723-.Ltmp721       #   Call between .Ltmp721 and .Ltmp723
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp723-.Lfunc_begin19 # >> Call Site 3 <<
	.long	.Ltmp724-.Ltmp723       #   Call between .Ltmp723 and .Ltmp724
	.long	.Ltmp725-.Lfunc_begin19 #     jumps to .Ltmp725
	.byte	1                       #   On action: 1
	.long	.Ltmp724-.Lfunc_begin19 # >> Call Site 4 <<
	.long	.Lfunc_end45-.Ltmp724   #   Call between .Ltmp724 and .Lfunc_end45
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev,@function
_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev: # @_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev
.Lfunc_begin20:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception20
# BB#0:
	pushq	%r14
.Lcfi276:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi277:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi278:
	.cfi_def_cfa_offset 32
.Lcfi279:
	.cfi_offset %rbx, -24
.Lcfi280:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, (%rbx)
.Ltmp726:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp727:
# BB#1:
.Ltmp732:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp733:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB46_5:
.Ltmp734:
	movq	%rax, %r14
	jmp	.LBB46_6
.LBB46_3:
.Ltmp728:
	movq	%rax, %r14
.Ltmp729:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp730:
.LBB46_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB46_4:
.Ltmp731:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end46:
	.size	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev, .Lfunc_end46-_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table46:
.Lexception20:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp726-.Lfunc_begin20 # >> Call Site 1 <<
	.long	.Ltmp727-.Ltmp726       #   Call between .Ltmp726 and .Ltmp727
	.long	.Ltmp728-.Lfunc_begin20 #     jumps to .Ltmp728
	.byte	0                       #   On action: cleanup
	.long	.Ltmp732-.Lfunc_begin20 # >> Call Site 2 <<
	.long	.Ltmp733-.Ltmp732       #   Call between .Ltmp732 and .Ltmp733
	.long	.Ltmp734-.Lfunc_begin20 #     jumps to .Ltmp734
	.byte	0                       #   On action: cleanup
	.long	.Ltmp729-.Lfunc_begin20 # >> Call Site 3 <<
	.long	.Ltmp730-.Ltmp729       #   Call between .Ltmp729 and .Ltmp730
	.long	.Ltmp731-.Lfunc_begin20 #     jumps to .Ltmp731
	.byte	1                       #   On action: 1
	.long	.Ltmp730-.Lfunc_begin20 # >> Call Site 4 <<
	.long	.Lfunc_end46-.Ltmp730   #   Call between .Ltmp730 and .Lfunc_end46
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi281:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi282:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi283:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi284:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi285:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi286:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi287:
	.cfi_def_cfa_offset 64
.Lcfi288:
	.cfi_offset %rbx, -56
.Lcfi289:
	.cfi_offset %r12, -48
.Lcfi290:
	.cfi_offset %r13, -40
.Lcfi291:
	.cfi_offset %r14, -32
.Lcfi292:
	.cfi_offset %r15, -24
.Lcfi293:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB47_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB47_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB47_6
# BB#3:                                 #   in Loop: Header=BB47_2 Depth=1
	movq	$_ZTV7CBufferIhE+16, 8(%rbp)
	movq	24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB47_5
# BB#4:                                 #   in Loop: Header=BB47_2 Depth=1
	callq	_ZdaPv
.LBB47_5:                               # %_ZN8NArchive4NZip14CExtraSubBlockD2Ev.exit
                                        #   in Loop: Header=BB47_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB47_6:                               #   in Loop: Header=BB47_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB47_2
.LBB47_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end47:
	.size	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii, .Lfunc_end47-_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIPvED0Ev,"axG",@progbits,_ZN13CRecordVectorIPvED0Ev,comdat
	.weak	_ZN13CRecordVectorIPvED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIPvED0Ev,@function
_ZN13CRecordVectorIPvED0Ev:             # @_ZN13CRecordVectorIPvED0Ev
.Lfunc_begin21:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception21
# BB#0:
	pushq	%r14
.Lcfi294:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi295:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi296:
	.cfi_def_cfa_offset 32
.Lcfi297:
	.cfi_offset %rbx, -24
.Lcfi298:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp735:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp736:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB48_2:
.Ltmp737:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end48:
	.size	_ZN13CRecordVectorIPvED0Ev, .Lfunc_end48-_ZN13CRecordVectorIPvED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table48:
.Lexception21:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp735-.Lfunc_begin21 # >> Call Site 1 <<
	.long	.Ltmp736-.Ltmp735       #   Call between .Ltmp735 and .Ltmp736
	.long	.Ltmp737-.Lfunc_begin21 #     jumps to .Ltmp737
	.byte	0                       #   On action: cleanup
	.long	.Ltmp736-.Lfunc_begin21 # >> Call Site 2 <<
	.long	.Lfunc_end48-.Ltmp736   #   Call between .Ltmp736 and .Lfunc_end48
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN7CBufferIhED2Ev,"axG",@progbits,_ZN7CBufferIhED2Ev,comdat
	.weak	_ZN7CBufferIhED2Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED2Ev,@function
_ZN7CBufferIhED2Ev:                     # @_ZN7CBufferIhED2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV7CBufferIhE+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB49_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB49_1:
	retq
.Lfunc_end49:
	.size	_ZN7CBufferIhED2Ev, .Lfunc_end49-_ZN7CBufferIhED2Ev
	.cfi_endproc

	.section	.text._ZN7CBufferIhED0Ev,"axG",@progbits,_ZN7CBufferIhED0Ev,comdat
	.weak	_ZN7CBufferIhED0Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED0Ev,@function
_ZN7CBufferIhED0Ev:                     # @_ZN7CBufferIhED0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi299:
	.cfi_def_cfa_offset 16
.Lcfi300:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV7CBufferIhE+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB50_2
# BB#1:
	callq	_ZdaPv
.LBB50_2:                               # %_ZN7CBufferIhED2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end50:
	.size	_ZN7CBufferIhED0Ev, .Lfunc_end50-_ZN7CBufferIhED0Ev
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIhED0Ev,"axG",@progbits,_ZN13CRecordVectorIhED0Ev,comdat
	.weak	_ZN13CRecordVectorIhED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIhED0Ev,@function
_ZN13CRecordVectorIhED0Ev:              # @_ZN13CRecordVectorIhED0Ev
.Lfunc_begin22:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception22
# BB#0:
	pushq	%r14
.Lcfi301:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi302:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi303:
	.cfi_def_cfa_offset 32
.Lcfi304:
	.cfi_offset %rbx, -24
.Lcfi305:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp738:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp739:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB51_2:
.Ltmp740:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end51:
	.size	_ZN13CRecordVectorIhED0Ev, .Lfunc_end51-_ZN13CRecordVectorIhED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table51:
.Lexception22:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp738-.Lfunc_begin22 # >> Call Site 1 <<
	.long	.Ltmp739-.Ltmp738       #   Call between .Ltmp738 and .Ltmp739
	.long	.Ltmp740-.Lfunc_begin22 #     jumps to .Ltmp740
	.byte	0                       #   On action: cleanup
	.long	.Ltmp739-.Lfunc_begin22 # >> Call Site 2 <<
	.long	.Lfunc_end51-.Ltmp739   #   Call between .Ltmp739 and .Lfunc_end51
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip5CItemEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip5CItemEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip5CItemEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip5CItemEED0Ev,@function
_ZN13CObjectVectorIN8NArchive4NZip5CItemEED0Ev: # @_ZN13CObjectVectorIN8NArchive4NZip5CItemEED0Ev
.Lfunc_begin23:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception23
# BB#0:
	pushq	%r14
.Lcfi306:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi307:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi308:
	.cfi_def_cfa_offset 32
.Lcfi309:
	.cfi_offset %rbx, -24
.Lcfi310:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip5CItemEE+16, (%rbx)
.Ltmp741:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp742:
# BB#1:
.Ltmp747:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp748:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive4NZip5CItemEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB52_5:
.Ltmp749:
	movq	%rax, %r14
	jmp	.LBB52_6
.LBB52_3:
.Ltmp743:
	movq	%rax, %r14
.Ltmp744:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp745:
.LBB52_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB52_4:
.Ltmp746:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end52:
	.size	_ZN13CObjectVectorIN8NArchive4NZip5CItemEED0Ev, .Lfunc_end52-_ZN13CObjectVectorIN8NArchive4NZip5CItemEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table52:
.Lexception23:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp741-.Lfunc_begin23 # >> Call Site 1 <<
	.long	.Ltmp742-.Ltmp741       #   Call between .Ltmp741 and .Ltmp742
	.long	.Ltmp743-.Lfunc_begin23 #     jumps to .Ltmp743
	.byte	0                       #   On action: cleanup
	.long	.Ltmp747-.Lfunc_begin23 # >> Call Site 2 <<
	.long	.Ltmp748-.Ltmp747       #   Call between .Ltmp747 and .Ltmp748
	.long	.Ltmp749-.Lfunc_begin23 #     jumps to .Ltmp749
	.byte	0                       #   On action: cleanup
	.long	.Ltmp744-.Lfunc_begin23 # >> Call Site 3 <<
	.long	.Ltmp745-.Ltmp744       #   Call between .Ltmp744 and .Ltmp745
	.long	.Ltmp746-.Lfunc_begin23 #     jumps to .Ltmp746
	.byte	1                       #   On action: 1
	.long	.Ltmp745-.Lfunc_begin23 # >> Call Site 4 <<
	.long	.Lfunc_end52-.Ltmp745   #   Call between .Ltmp745 and .Lfunc_end52
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip5CItemEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip5CItemEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip5CItemEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip5CItemEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive4NZip5CItemEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive4NZip5CItemEE6DeleteEii
.Lfunc_begin24:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception24
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi311:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi312:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi313:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi314:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi315:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi316:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi317:
	.cfi_def_cfa_offset 64
.Lcfi318:
	.cfi_offset %rbx, -56
.Lcfi319:
	.cfi_offset %r12, -48
.Lcfi320:
	.cfi_offset %r13, -40
.Lcfi321:
	.cfi_offset %r14, -32
.Lcfi322:
	.cfi_offset %r15, -24
.Lcfi323:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB53_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB53_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB53_5
# BB#3:                                 #   in Loop: Header=BB53_2 Depth=1
.Ltmp750:
	movq	%rbp, %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
.Ltmp751:
# BB#4:                                 #   in Loop: Header=BB53_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB53_5:                               #   in Loop: Header=BB53_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB53_2
.LBB53_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB53_7:
.Ltmp752:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end53:
	.size	_ZN13CObjectVectorIN8NArchive4NZip5CItemEE6DeleteEii, .Lfunc_end53-_ZN13CObjectVectorIN8NArchive4NZip5CItemEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table53:
.Lexception24:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp750-.Lfunc_begin24 # >> Call Site 1 <<
	.long	.Ltmp751-.Ltmp750       #   Call between .Ltmp750 and .Ltmp751
	.long	.Ltmp752-.Lfunc_begin24 #     jumps to .Ltmp752
	.byte	0                       #   On action: cleanup
	.long	.Ltmp751-.Lfunc_begin24 # >> Call Site 2 <<
	.long	.Lfunc_end53-.Ltmp751   #   Call between .Ltmp751 and .Lfunc_end53
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIyED0Ev,"axG",@progbits,_ZN13CRecordVectorIyED0Ev,comdat
	.weak	_ZN13CRecordVectorIyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyED0Ev,@function
_ZN13CRecordVectorIyED0Ev:              # @_ZN13CRecordVectorIyED0Ev
.Lfunc_begin25:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception25
# BB#0:
	pushq	%r14
.Lcfi324:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi325:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi326:
	.cfi_def_cfa_offset 32
.Lcfi327:
	.cfi_offset %rbx, -24
.Lcfi328:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp753:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp754:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB54_2:
.Ltmp755:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end54:
	.size	_ZN13CRecordVectorIyED0Ev, .Lfunc_end54-_ZN13CRecordVectorIyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table54:
.Lexception25:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp753-.Lfunc_begin25 # >> Call Site 1 <<
	.long	.Ltmp754-.Ltmp753       #   Call between .Ltmp753 and .Ltmp754
	.long	.Ltmp755-.Lfunc_begin25 #     jumps to .Ltmp755
	.byte	0                       #   On action: cleanup
	.long	.Ltmp754-.Lfunc_begin25 # >> Call Site 2 <<
	.long	.Lfunc_end54-.Ltmp754   #   Call between .Ltmp754 and .Lfunc_end54
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NWindows16NSynchronization14CSemaphoreWFMO19IsSignaledAndUpdateEv,"axG",@progbits,_ZN8NWindows16NSynchronization14CSemaphoreWFMO19IsSignaledAndUpdateEv,comdat
	.weak	_ZN8NWindows16NSynchronization14CSemaphoreWFMO19IsSignaledAndUpdateEv
	.p2align	4, 0x90
	.type	_ZN8NWindows16NSynchronization14CSemaphoreWFMO19IsSignaledAndUpdateEv,@function
_ZN8NWindows16NSynchronization14CSemaphoreWFMO19IsSignaledAndUpdateEv: # @_ZN8NWindows16NSynchronization14CSemaphoreWFMO19IsSignaledAndUpdateEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB55_1
# BB#2:
	decl	%eax
	movl	%eax, 16(%rdi)
	movb	$1, %al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB55_1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end55:
	.size	_ZN8NWindows16NSynchronization14CSemaphoreWFMO19IsSignaledAndUpdateEv, .Lfunc_end55-_ZN8NWindows16NSynchronization14CSemaphoreWFMO19IsSignaledAndUpdateEv
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EED2Ev,@function
_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EED2Ev: # @_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EED2Ev
.Lfunc_begin26:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception26
# BB#0:
	pushq	%r14
.Lcfi329:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi330:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi331:
	.cfi_def_cfa_offset 32
.Lcfi332:
	.cfi_offset %rbx, -24
.Lcfi333:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE+16, (%rbx)
.Ltmp756:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp757:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB56_2:
.Ltmp758:
	movq	%rax, %r14
.Ltmp759:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp760:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB56_4:
.Ltmp761:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end56:
	.size	_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EED2Ev, .Lfunc_end56-_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table56:
.Lexception26:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp756-.Lfunc_begin26 # >> Call Site 1 <<
	.long	.Ltmp757-.Ltmp756       #   Call between .Ltmp756 and .Ltmp757
	.long	.Ltmp758-.Lfunc_begin26 #     jumps to .Ltmp758
	.byte	0                       #   On action: cleanup
	.long	.Ltmp757-.Lfunc_begin26 # >> Call Site 2 <<
	.long	.Ltmp759-.Ltmp757       #   Call between .Ltmp757 and .Ltmp759
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp759-.Lfunc_begin26 # >> Call Site 3 <<
	.long	.Ltmp760-.Ltmp759       #   Call between .Ltmp759 and .Ltmp760
	.long	.Ltmp761-.Lfunc_begin26 #     jumps to .Ltmp761
	.byte	1                       #   On action: 1
	.long	.Ltmp760-.Lfunc_begin26 # >> Call Site 4 <<
	.long	.Lfunc_end56-.Ltmp760   #   Call between .Ltmp760 and .Lfunc_end56
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EED0Ev,@function
_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EED0Ev: # @_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EED0Ev
.Lfunc_begin27:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception27
# BB#0:
	pushq	%r14
.Lcfi334:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi335:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi336:
	.cfi_def_cfa_offset 32
.Lcfi337:
	.cfi_offset %rbx, -24
.Lcfi338:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE+16, (%rbx)
.Ltmp762:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp763:
# BB#1:
.Ltmp768:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp769:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB57_5:
.Ltmp770:
	movq	%rax, %r14
	jmp	.LBB57_6
.LBB57_3:
.Ltmp764:
	movq	%rax, %r14
.Ltmp765:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp766:
.LBB57_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB57_4:
.Ltmp767:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end57:
	.size	_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EED0Ev, .Lfunc_end57-_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table57:
.Lexception27:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp762-.Lfunc_begin27 # >> Call Site 1 <<
	.long	.Ltmp763-.Ltmp762       #   Call between .Ltmp762 and .Ltmp763
	.long	.Ltmp764-.Lfunc_begin27 #     jumps to .Ltmp764
	.byte	0                       #   On action: cleanup
	.long	.Ltmp768-.Lfunc_begin27 # >> Call Site 2 <<
	.long	.Ltmp769-.Ltmp768       #   Call between .Ltmp768 and .Ltmp769
	.long	.Ltmp770-.Lfunc_begin27 #     jumps to .Ltmp770
	.byte	0                       #   On action: cleanup
	.long	.Ltmp765-.Lfunc_begin27 # >> Call Site 3 <<
	.long	.Ltmp766-.Ltmp765       #   Call between .Ltmp765 and .Ltmp766
	.long	.Ltmp767-.Lfunc_begin27 #     jumps to .Ltmp767
	.byte	1                       #   On action: 1
	.long	.Ltmp766-.Lfunc_begin27 # >> Call Site 4 <<
	.long	.Lfunc_end57-.Ltmp766   #   Call between .Ltmp766 and .Lfunc_end57
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE6DeleteEii
.Lfunc_begin28:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception28
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi339:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi340:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi341:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi342:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi343:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi344:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi345:
	.cfi_def_cfa_offset 64
.Lcfi346:
	.cfi_offset %rbx, -56
.Lcfi347:
	.cfi_offset %r12, -48
.Lcfi348:
	.cfi_offset %r13, -40
.Lcfi349:
	.cfi_offset %r14, -32
.Lcfi350:
	.cfi_offset %r15, -24
.Lcfi351:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB58_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB58_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB58_5
# BB#3:                                 #   in Loop: Header=BB58_2 Depth=1
.Ltmp771:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp772:
# BB#4:                                 # %_ZN10CMemBlocksD2Ev.exit
                                        #   in Loop: Header=BB58_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB58_5:                               #   in Loop: Header=BB58_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB58_2
.LBB58_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB58_7:
.Ltmp773:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end58:
	.size	_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE6DeleteEii, .Lfunc_end58-_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table58:
.Lexception28:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp771-.Lfunc_begin28 # >> Call Site 1 <<
	.long	.Ltmp772-.Ltmp771       #   Call between .Ltmp771 and .Ltmp772
	.long	.Ltmp773-.Lfunc_begin28 #     jumps to .Ltmp773
	.byte	0                       #   On action: cleanup
	.long	.Ltmp772-.Lfunc_begin28 # >> Call Site 2 <<
	.long	.Lfunc_end58-.Ltmp772   #   Call between .Ltmp772 and .Lfunc_end58
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEED2Ev,@function
_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEED2Ev: # @_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEED2Ev
.Lfunc_begin29:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception29
# BB#0:
	pushq	%r14
.Lcfi352:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi353:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi354:
	.cfi_def_cfa_offset 32
.Lcfi355:
	.cfi_offset %rbx, -24
.Lcfi356:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip11CThreadInfoEE+16, (%rbx)
.Ltmp774:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp775:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB59_2:
.Ltmp776:
	movq	%rax, %r14
.Ltmp777:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp778:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB59_4:
.Ltmp779:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end59:
	.size	_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEED2Ev, .Lfunc_end59-_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table59:
.Lexception29:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp774-.Lfunc_begin29 # >> Call Site 1 <<
	.long	.Ltmp775-.Ltmp774       #   Call between .Ltmp774 and .Ltmp775
	.long	.Ltmp776-.Lfunc_begin29 #     jumps to .Ltmp776
	.byte	0                       #   On action: cleanup
	.long	.Ltmp775-.Lfunc_begin29 # >> Call Site 2 <<
	.long	.Ltmp777-.Ltmp775       #   Call between .Ltmp775 and .Ltmp777
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp777-.Lfunc_begin29 # >> Call Site 3 <<
	.long	.Ltmp778-.Ltmp777       #   Call between .Ltmp777 and .Ltmp778
	.long	.Ltmp779-.Lfunc_begin29 #     jumps to .Ltmp779
	.byte	1                       #   On action: 1
	.long	.Ltmp778-.Lfunc_begin29 # >> Call Site 4 <<
	.long	.Lfunc_end59-.Ltmp778   #   Call between .Ltmp778 and .Lfunc_end59
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEED0Ev,@function
_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEED0Ev: # @_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEED0Ev
.Lfunc_begin30:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception30
# BB#0:
	pushq	%r14
.Lcfi357:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi358:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi359:
	.cfi_def_cfa_offset 32
.Lcfi360:
	.cfi_offset %rbx, -24
.Lcfi361:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip11CThreadInfoEE+16, (%rbx)
.Ltmp780:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp781:
# BB#1:
.Ltmp786:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp787:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB60_5:
.Ltmp788:
	movq	%rax, %r14
	jmp	.LBB60_6
.LBB60_3:
.Ltmp782:
	movq	%rax, %r14
.Ltmp783:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp784:
.LBB60_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB60_4:
.Ltmp785:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end60:
	.size	_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEED0Ev, .Lfunc_end60-_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table60:
.Lexception30:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp780-.Lfunc_begin30 # >> Call Site 1 <<
	.long	.Ltmp781-.Ltmp780       #   Call between .Ltmp780 and .Ltmp781
	.long	.Ltmp782-.Lfunc_begin30 #     jumps to .Ltmp782
	.byte	0                       #   On action: cleanup
	.long	.Ltmp786-.Lfunc_begin30 # >> Call Site 2 <<
	.long	.Ltmp787-.Ltmp786       #   Call between .Ltmp786 and .Ltmp787
	.long	.Ltmp788-.Lfunc_begin30 #     jumps to .Ltmp788
	.byte	0                       #   On action: cleanup
	.long	.Ltmp783-.Lfunc_begin30 # >> Call Site 3 <<
	.long	.Ltmp784-.Ltmp783       #   Call between .Ltmp783 and .Ltmp784
	.long	.Ltmp785-.Lfunc_begin30 #     jumps to .Ltmp785
	.byte	1                       #   On action: 1
	.long	.Ltmp784-.Lfunc_begin30 # >> Call Site 4 <<
	.long	.Lfunc_end60-.Ltmp784   #   Call between .Ltmp784 and .Lfunc_end60
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEE6DeleteEii
.Lfunc_begin31:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception31
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi362:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi363:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi364:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi365:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi366:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi367:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi368:
	.cfi_def_cfa_offset 64
.Lcfi369:
	.cfi_offset %rbx, -56
.Lcfi370:
	.cfi_offset %r12, -48
.Lcfi371:
	.cfi_offset %r13, -40
.Lcfi372:
	.cfi_offset %r14, -32
.Lcfi373:
	.cfi_offset %r15, -24
.Lcfi374:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB61_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB61_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB61_5
# BB#3:                                 #   in Loop: Header=BB61_2 Depth=1
.Ltmp789:
	movq	%rbp, %rdi
	callq	_ZN8NArchive4NZip11CThreadInfoD2Ev
.Ltmp790:
# BB#4:                                 #   in Loop: Header=BB61_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB61_5:                               #   in Loop: Header=BB61_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB61_2
.LBB61_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB61_7:
.Ltmp791:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end61:
	.size	_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEE6DeleteEii, .Lfunc_end61-_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table61:
.Lexception31:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp789-.Lfunc_begin31 # >> Call Site 1 <<
	.long	.Ltmp790-.Ltmp789       #   Call between .Ltmp789 and .Ltmp790
	.long	.Ltmp791-.Lfunc_begin31 #     jumps to .Ltmp791
	.byte	0                       #   On action: cleanup
	.long	.Ltmp790-.Lfunc_begin31 # >> Call Site 2 <<
	.long	.Lfunc_end61-.Ltmp790   #   Call between .Ltmp790 and .Lfunc_end61
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEED0Ev,"axG",@progbits,_ZN13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEED0Ev,comdat
	.weak	_ZN13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEED0Ev,@function
_ZN13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEED0Ev: # @_ZN13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEED0Ev
.Lfunc_begin32:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception32
# BB#0:
	pushq	%r14
.Lcfi375:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi376:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi377:
	.cfi_def_cfa_offset 32
.Lcfi378:
	.cfi_offset %rbx, -24
.Lcfi379:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp792:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp793:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB62_2:
.Ltmp794:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end62:
	.size	_ZN13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEED0Ev, .Lfunc_end62-_ZN13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table62:
.Lexception32:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp792-.Lfunc_begin32 # >> Call Site 1 <<
	.long	.Ltmp793-.Ltmp792       #   Call between .Ltmp792 and .Ltmp793
	.long	.Ltmp794-.Lfunc_begin32 #     jumps to .Ltmp794
	.byte	0                       #   On action: cleanup
	.long	.Ltmp793-.Lfunc_begin32 # >> Call Site 2 <<
	.long	.Lfunc_end62-.Ltmp793   #   Call between .Ltmp793 and .Lfunc_end62
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIiED0Ev,"axG",@progbits,_ZN13CRecordVectorIiED0Ev,comdat
	.weak	_ZN13CRecordVectorIiED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIiED0Ev,@function
_ZN13CRecordVectorIiED0Ev:              # @_ZN13CRecordVectorIiED0Ev
.Lfunc_begin33:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception33
# BB#0:
	pushq	%r14
.Lcfi380:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi381:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi382:
	.cfi_def_cfa_offset 32
.Lcfi383:
	.cfi_offset %rbx, -24
.Lcfi384:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp795:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp796:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB63_2:
.Ltmp797:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end63:
	.size	_ZN13CRecordVectorIiED0Ev, .Lfunc_end63-_ZN13CRecordVectorIiED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table63:
.Lexception33:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp795-.Lfunc_begin33 # >> Call Site 1 <<
	.long	.Ltmp796-.Ltmp795       #   Call between .Ltmp795 and .Ltmp796
	.long	.Ltmp797-.Lfunc_begin33 #     jumps to .Ltmp797
	.byte	0                       #   On action: cleanup
	.long	.Ltmp796-.Lfunc_begin33 # >> Call Site 2 <<
	.long	.Lfunc_end63-.Ltmp796   #   Call between .Ltmp796 and .Lfunc_end63
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIPvEC2ERKS1_,"axG",@progbits,_ZN13CRecordVectorIPvEC2ERKS1_,comdat
	.weak	_ZN13CRecordVectorIPvEC2ERKS1_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIPvEC2ERKS1_,@function
_ZN13CRecordVectorIPvEC2ERKS1_:         # @_ZN13CRecordVectorIPvEC2ERKS1_
.Lfunc_begin34:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception34
# BB#0:
	pushq	%r15
.Lcfi385:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi386:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi387:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi388:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi389:
	.cfi_def_cfa_offset 48
.Lcfi390:
	.cfi_offset %rbx, -48
.Lcfi391:
	.cfi_offset %r12, -40
.Lcfi392:
	.cfi_offset %r13, -32
.Lcfi393:
	.cfi_offset %r14, -24
.Lcfi394:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$8, 24(%r12)
	movq	$_ZTV13CRecordVectorIPvE+16, (%r12)
.Ltmp798:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp799:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp800:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp801:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB64_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB64_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %r13
.Ltmp803:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp804:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB64_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB64_4
.LBB64_6:                               # %_ZN13CRecordVectorIPvEaSERKS1_.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB64_8:                               # %.loopexit.split-lp
.Ltmp802:
	jmp	.LBB64_9
.LBB64_7:                               # %.loopexit
.Ltmp805:
.LBB64_9:
	movq	%rax, %r14
.Ltmp806:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp807:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB64_11:
.Ltmp808:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end64:
	.size	_ZN13CRecordVectorIPvEC2ERKS1_, .Lfunc_end64-_ZN13CRecordVectorIPvEC2ERKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table64:
.Lexception34:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp798-.Lfunc_begin34 # >> Call Site 1 <<
	.long	.Ltmp801-.Ltmp798       #   Call between .Ltmp798 and .Ltmp801
	.long	.Ltmp802-.Lfunc_begin34 #     jumps to .Ltmp802
	.byte	0                       #   On action: cleanup
	.long	.Ltmp803-.Lfunc_begin34 # >> Call Site 2 <<
	.long	.Ltmp804-.Ltmp803       #   Call between .Ltmp803 and .Ltmp804
	.long	.Ltmp805-.Lfunc_begin34 #     jumps to .Ltmp805
	.byte	0                       #   On action: cleanup
	.long	.Ltmp806-.Lfunc_begin34 # >> Call Site 3 <<
	.long	.Ltmp807-.Ltmp806       #   Call between .Ltmp806 and .Ltmp807
	.long	.Ltmp808-.Lfunc_begin34 #     jumps to .Ltmp808
	.byte	1                       #   On action: 1
	.long	.Ltmp807-.Lfunc_begin34 # >> Call Site 4 <<
	.long	.Lfunc_end64-.Ltmp807   #   Call between .Ltmp807 and .Lfunc_end64
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive4NZip11CThreadInfoC2ERKS1_,"axG",@progbits,_ZN8NArchive4NZip11CThreadInfoC2ERKS1_,comdat
	.weak	_ZN8NArchive4NZip11CThreadInfoC2ERKS1_
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11CThreadInfoC2ERKS1_,@function
_ZN8NArchive4NZip11CThreadInfoC2ERKS1_: # @_ZN8NArchive4NZip11CThreadInfoC2ERKS1_
.Lfunc_begin35:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception35
# BB#0:
	pushq	%r15
.Lcfi395:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi396:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi397:
	.cfi_def_cfa_offset 32
.Lcfi398:
	.cfi_offset %rbx, -32
.Lcfi399:
	.cfi_offset %r14, -24
.Lcfi400:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	112(%rbx), %rax
	movq	%rax, 112(%r15)
	movups	96(%rbx), %xmm0
	movups	%xmm0, 96(%r15)
	movups	80(%rbx), %xmm0
	movups	%xmm0, 80(%r15)
	movups	64(%rbx), %xmm0
	movups	%xmm0, 64(%r15)
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	32(%rbx), %xmm2
	movups	48(%rbx), %xmm3
	movups	%xmm3, 48(%r15)
	movups	%xmm2, 32(%r15)
	movups	%xmm1, 16(%r15)
	movups	%xmm0, (%r15)
	movq	128(%rbx), %rax
	movq	%rax, 128(%r15)
	movq	$_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE+16, 120(%r15)
	movzwl	136(%rbx), %eax
	movw	%ax, 136(%r15)
	movq	$_ZTVN8NWindows16NSynchronization19CAutoResetEventWFMOE+16, 120(%r15)
	movups	144(%rbx), %xmm0
	movups	%xmm0, 144(%r15)
	movq	160(%rbx), %rdi
	movq	%rdi, 160(%r15)
	testq	%rdi, %rdi
	je	.LBB65_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp809:
	callq	*8(%rax)
.Ltmp810:
.LBB65_2:                               # %_ZN9CMyComPtrI21ICompressProgressInfoEC2ERKS1_.exit
	movq	168(%rbx), %rax
	movq	%rax, 168(%r15)
	movq	176(%rbx), %rdi
	movq	%rdi, 176(%r15)
	testq	%rdi, %rdi
	je	.LBB65_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp812:
	callq	*8(%rax)
.Ltmp813:
.LBB65_4:                               # %_ZN9CMyComPtrI10IOutStreamEC2ERKS1_.exit
	movq	184(%rbx), %rdi
	movq	%rdi, 184(%r15)
	testq	%rdi, %rdi
	je	.LBB65_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp815:
	callq	*8(%rax)
.Ltmp816:
.LBB65_6:                               # %_ZN9CMyComPtrI19ISequentialInStreamEC2ERKS1_.exit
	leaq	192(%r15), %rdi
	leaq	192(%rbx), %rsi
.Ltmp818:
	callq	_ZN8NArchive4NZip10CAddCommonC2ERKS1_
.Ltmp819:
# BB#7:
	movq	400(%rbx), %rax
	movq	%rax, 400(%r15)
	movups	368(%rbx), %xmm0
	movups	384(%rbx), %xmm1
	movups	%xmm1, 384(%r15)
	movups	%xmm0, 368(%r15)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB65_10:
.Ltmp817:
	movq	%rax, %r14
	jmp	.LBB65_13
.LBB65_9:
.Ltmp814:
	movq	%rax, %r14
	jmp	.LBB65_15
.LBB65_8:
.Ltmp811:
	movq	%rax, %r14
	jmp	.LBB65_17
.LBB65_11:
.Ltmp820:
	movq	%rax, %r14
	movq	184(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB65_13
# BB#12:
	movq	(%rdi), %rax
.Ltmp821:
	callq	*16(%rax)
.Ltmp822:
.LBB65_13:                              # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	176(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB65_15
# BB#14:
	movq	(%rdi), %rax
.Ltmp823:
	callq	*16(%rax)
.Ltmp824:
.LBB65_15:                              # %_ZN9CMyComPtrI10IOutStreamED2Ev.exit
	movq	160(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB65_17
# BB#16:
	movq	(%rdi), %rax
.Ltmp825:
	callq	*16(%rax)
.Ltmp826:
.LBB65_17:
	movq	$_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE+16, 120(%r15)
	movq	$0, 128(%r15)
	leaq	16(%r15), %rdi
.Ltmp827:
	callq	Event_Close
.Ltmp828:
# BB#18:                                # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit
.Ltmp829:
	movq	%r15, %rdi
	callq	Thread_Close
.Ltmp830:
# BB#19:                                # %_ZN8NWindows7CThreadD2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB65_20:
.Ltmp831:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end65:
	.size	_ZN8NArchive4NZip11CThreadInfoC2ERKS1_, .Lfunc_end65-_ZN8NArchive4NZip11CThreadInfoC2ERKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table65:
.Lexception35:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp809-.Lfunc_begin35 # >> Call Site 1 <<
	.long	.Ltmp810-.Ltmp809       #   Call between .Ltmp809 and .Ltmp810
	.long	.Ltmp811-.Lfunc_begin35 #     jumps to .Ltmp811
	.byte	0                       #   On action: cleanup
	.long	.Ltmp812-.Lfunc_begin35 # >> Call Site 2 <<
	.long	.Ltmp813-.Ltmp812       #   Call between .Ltmp812 and .Ltmp813
	.long	.Ltmp814-.Lfunc_begin35 #     jumps to .Ltmp814
	.byte	0                       #   On action: cleanup
	.long	.Ltmp815-.Lfunc_begin35 # >> Call Site 3 <<
	.long	.Ltmp816-.Ltmp815       #   Call between .Ltmp815 and .Ltmp816
	.long	.Ltmp817-.Lfunc_begin35 #     jumps to .Ltmp817
	.byte	0                       #   On action: cleanup
	.long	.Ltmp818-.Lfunc_begin35 # >> Call Site 4 <<
	.long	.Ltmp819-.Ltmp818       #   Call between .Ltmp818 and .Ltmp819
	.long	.Ltmp820-.Lfunc_begin35 #     jumps to .Ltmp820
	.byte	0                       #   On action: cleanup
	.long	.Ltmp821-.Lfunc_begin35 # >> Call Site 5 <<
	.long	.Ltmp830-.Ltmp821       #   Call between .Ltmp821 and .Ltmp830
	.long	.Ltmp831-.Lfunc_begin35 #     jumps to .Ltmp831
	.byte	1                       #   On action: 1
	.long	.Ltmp830-.Lfunc_begin35 # >> Call Site 6 <<
	.long	.Lfunc_end65-.Ltmp830   #   Call between .Ltmp830 and .Lfunc_end65
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive4NZip10CAddCommonC2ERKS1_,"axG",@progbits,_ZN8NArchive4NZip10CAddCommonC2ERKS1_,comdat
	.weak	_ZN8NArchive4NZip10CAddCommonC2ERKS1_
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CAddCommonC2ERKS1_,@function
_ZN8NArchive4NZip10CAddCommonC2ERKS1_:  # @_ZN8NArchive4NZip10CAddCommonC2ERKS1_
.Lfunc_begin36:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception36
# BB#0:
	pushq	%r15
.Lcfi401:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi402:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi403:
	.cfi_def_cfa_offset 32
.Lcfi404:
	.cfi_offset %rbx, -32
.Lcfi405:
	.cfi_offset %r14, -24
.Lcfi406:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	callq	_ZN8NArchive4NZip22CCompressionMethodModeC2ERKS1_
	movq	112(%rbx), %rax
	movq	%rax, 112(%r15)
	movq	120(%rbx), %rdi
	movq	%rdi, 120(%r15)
	testq	%rdi, %rdi
	je	.LBB66_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp832:
	callq	*8(%rax)
.Ltmp833:
.LBB66_2:                               # %_ZN9CMyComPtrI14ICompressCoderEC2ERKS1_.exit
	movq	128(%rbx), %rdi
	movq	%rdi, 128(%r15)
	testq	%rdi, %rdi
	je	.LBB66_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp835:
	callq	*8(%rax)
.Ltmp836:
.LBB66_4:                               # %_ZN9CMyComPtrI14ICompressCoderEC2ERKS1_.exit12
	movups	136(%rbx), %xmm0
	movups	%xmm0, 136(%r15)
	movq	152(%rbx), %rdi
	movq	%rdi, 152(%r15)
	testq	%rdi, %rdi
	je	.LBB66_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp838:
	callq	*8(%rax)
.Ltmp839:
.LBB66_6:                               # %_ZN9CMyComPtrI20ISequentialOutStreamEC2ERKS1_.exit
	movups	160(%rbx), %xmm0
	movups	%xmm0, 160(%r15)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB66_9:
.Ltmp840:
	movq	%rax, %r14
	movq	128(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB66_11
# BB#10:
	movq	(%rdi), %rax
.Ltmp841:
	callq	*16(%rax)
.Ltmp842:
	jmp	.LBB66_11
.LBB66_8:
.Ltmp837:
	movq	%rax, %r14
.LBB66_11:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	movq	120(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB66_13
# BB#12:
	movq	(%rdi), %rax
.Ltmp843:
	callq	*16(%rax)
.Ltmp844:
	jmp	.LBB66_13
.LBB66_7:
.Ltmp834:
	movq	%rax, %r14
.LBB66_13:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit16
	movq	88(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB66_15
# BB#14:
	callq	_ZdaPv
.LBB66_15:                              # %_ZN11CStringBaseIcED2Ev.exit.i
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB66_17
# BB#16:
	callq	_ZdaPv
.LBB66_17:                              # %_ZN11CStringBaseIwED2Ev.exit.i
.Ltmp845:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp846:
# BB#18:                                # %_ZN8NArchive4NZip22CCompressionMethodModeD2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB66_19:
.Ltmp847:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end66:
	.size	_ZN8NArchive4NZip10CAddCommonC2ERKS1_, .Lfunc_end66-_ZN8NArchive4NZip10CAddCommonC2ERKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table66:
.Lexception36:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin36-.Lfunc_begin36 # >> Call Site 1 <<
	.long	.Ltmp832-.Lfunc_begin36 #   Call between .Lfunc_begin36 and .Ltmp832
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp832-.Lfunc_begin36 # >> Call Site 2 <<
	.long	.Ltmp833-.Ltmp832       #   Call between .Ltmp832 and .Ltmp833
	.long	.Ltmp834-.Lfunc_begin36 #     jumps to .Ltmp834
	.byte	0                       #   On action: cleanup
	.long	.Ltmp835-.Lfunc_begin36 # >> Call Site 3 <<
	.long	.Ltmp836-.Ltmp835       #   Call between .Ltmp835 and .Ltmp836
	.long	.Ltmp837-.Lfunc_begin36 #     jumps to .Ltmp837
	.byte	0                       #   On action: cleanup
	.long	.Ltmp838-.Lfunc_begin36 # >> Call Site 4 <<
	.long	.Ltmp839-.Ltmp838       #   Call between .Ltmp838 and .Ltmp839
	.long	.Ltmp840-.Lfunc_begin36 #     jumps to .Ltmp840
	.byte	0                       #   On action: cleanup
	.long	.Ltmp841-.Lfunc_begin36 # >> Call Site 5 <<
	.long	.Ltmp846-.Ltmp841       #   Call between .Ltmp841 and .Ltmp846
	.long	.Ltmp847-.Lfunc_begin36 #     jumps to .Ltmp847
	.byte	1                       #   On action: 1
	.long	.Ltmp846-.Lfunc_begin36 # >> Call Site 6 <<
	.long	.Lfunc_end66-.Ltmp846   #   Call between .Ltmp846 and .Lfunc_end66
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv,"axG",@progbits,_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv,comdat
	.weak	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.p2align	4, 0x90
	.type	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv,@function
_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv: # @_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.cfi_startproc
# BB#0:
	cmpb	$0, 17(%rdi)
	je	.LBB67_1
# BB#2:
	movb	$1, %al
	cmpb	$0, 16(%rdi)
	jne	.LBB67_4
# BB#3:
	movb	$0, 17(%rdi)
.LBB67_4:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB67_1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end67:
	.size	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv, .Lfunc_end67-_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip22CCompressionMethodModeC2ERKS1_,"axG",@progbits,_ZN8NArchive4NZip22CCompressionMethodModeC2ERKS1_,comdat
	.weak	_ZN8NArchive4NZip22CCompressionMethodModeC2ERKS1_
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip22CCompressionMethodModeC2ERKS1_,@function
_ZN8NArchive4NZip22CCompressionMethodModeC2ERKS1_: # @_ZN8NArchive4NZip22CCompressionMethodModeC2ERKS1_
.Lfunc_begin37:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception37
# BB#0:
	pushq	%r15
.Lcfi407:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi408:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi409:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi410:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi411:
	.cfi_def_cfa_offset 48
.Lcfi412:
	.cfi_offset %rbx, -48
.Lcfi413:
	.cfi_offset %r12, -40
.Lcfi414:
	.cfi_offset %r13, -32
.Lcfi415:
	.cfi_offset %r14, -24
.Lcfi416:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	_ZN13CRecordVectorIhEC2ERKS0_
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r14)
	movslq	40(%rbx), %r13
	leaq	1(%r13), %r12
	testl	%r12d, %r12d
	je	.LBB68_1
# BB#2:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp848:
	callq	_Znam
	movq	%rax, %r15
.Ltmp849:
# BB#3:                                 # %.noexc
	movq	%r15, 32(%r14)
	movl	$0, (%r15)
	movl	%r12d, 44(%r14)
	jmp	.LBB68_4
.LBB68_1:
	xorl	%r15d, %r15d
.LBB68_4:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	32(%rbx), %rax
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB68_5:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	addq	$4, %rax
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB68_5
# BB#6:
	movl	%r13d, 40(%r14)
	movq	77(%rbx), %rax
	movq	%rax, 77(%r14)
	movups	48(%rbx), %xmm0
	movups	64(%rbx), %xmm1
	movups	%xmm1, 64(%r14)
	movups	%xmm0, 48(%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 88(%r14)
	movslq	96(%rbx), %rax
	leaq	1(%rax), %r12
	testl	%r12d, %r12d
	je	.LBB68_7
# BB#8:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r12, %rdi
	cmovlq	%rax, %rdi
.Ltmp851:
	callq	_Znam
.Ltmp852:
# BB#9:                                 # %.noexc10
	movq	%rax, 88(%r14)
	movb	$0, (%rax)
	movl	%r12d, 100(%r14)
	jmp	.LBB68_10
.LBB68_7:
	xorl	%eax, %eax
.LBB68_10:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
	movq	88(%rbx), %rcx
	.p2align	4, 0x90
.LBB68_11:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB68_11
# BB#12:
	movl	96(%rbx), %eax
	movl	%eax, 96(%r14)
	movzwl	104(%rbx), %eax
	movw	%ax, 104(%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB68_14:
.Ltmp853:
	movq	%rax, %rbx
	testq	%r15, %r15
	je	.LBB68_16
# BB#15:
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB68_16
.LBB68_13:
.Ltmp850:
	movq	%rax, %rbx
.LBB68_16:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp854:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp855:
# BB#17:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB68_18:
.Ltmp856:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end68:
	.size	_ZN8NArchive4NZip22CCompressionMethodModeC2ERKS1_, .Lfunc_end68-_ZN8NArchive4NZip22CCompressionMethodModeC2ERKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table68:
.Lexception37:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin37-.Lfunc_begin37 # >> Call Site 1 <<
	.long	.Ltmp848-.Lfunc_begin37 #   Call between .Lfunc_begin37 and .Ltmp848
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp848-.Lfunc_begin37 # >> Call Site 2 <<
	.long	.Ltmp849-.Ltmp848       #   Call between .Ltmp848 and .Ltmp849
	.long	.Ltmp850-.Lfunc_begin37 #     jumps to .Ltmp850
	.byte	0                       #   On action: cleanup
	.long	.Ltmp851-.Lfunc_begin37 # >> Call Site 3 <<
	.long	.Ltmp852-.Ltmp851       #   Call between .Ltmp851 and .Ltmp852
	.long	.Ltmp853-.Lfunc_begin37 #     jumps to .Ltmp853
	.byte	0                       #   On action: cleanup
	.long	.Ltmp854-.Lfunc_begin37 # >> Call Site 4 <<
	.long	.Ltmp855-.Ltmp854       #   Call between .Ltmp854 and .Ltmp855
	.long	.Ltmp856-.Lfunc_begin37 #     jumps to .Ltmp856
	.byte	1                       #   On action: 1
	.long	.Ltmp855-.Lfunc_begin37 # >> Call Site 5 <<
	.long	.Lfunc_end68-.Ltmp855   #   Call between .Ltmp855 and .Lfunc_end68
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIhEC2ERKS0_,"axG",@progbits,_ZN13CRecordVectorIhEC2ERKS0_,comdat
	.weak	_ZN13CRecordVectorIhEC2ERKS0_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIhEC2ERKS0_,@function
_ZN13CRecordVectorIhEC2ERKS0_:          # @_ZN13CRecordVectorIhEC2ERKS0_
.Lfunc_begin38:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception38
# BB#0:
	pushq	%rbp
.Lcfi417:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi418:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi419:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi420:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi421:
	.cfi_def_cfa_offset 48
.Lcfi422:
	.cfi_offset %rbx, -48
.Lcfi423:
	.cfi_offset %r12, -40
.Lcfi424:
	.cfi_offset %r14, -32
.Lcfi425:
	.cfi_offset %r15, -24
.Lcfi426:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$1, 24(%r12)
	movq	$_ZTV13CRecordVectorIhE+16, (%r12)
.Ltmp857:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp858:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp859:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp860:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB69_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB69_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movzbl	(%rax,%rbx), %ebp
.Ltmp862:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp863:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB69_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movb	%bpl, (%rax,%rcx)
	incl	12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB69_4
.LBB69_6:                               # %_ZN13CRecordVectorIhEaSERKS0_.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB69_8:                               # %.loopexit.split-lp
.Ltmp861:
	jmp	.LBB69_9
.LBB69_7:                               # %.loopexit
.Ltmp864:
.LBB69_9:
	movq	%rax, %r14
.Ltmp865:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp866:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB69_11:
.Ltmp867:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end69:
	.size	_ZN13CRecordVectorIhEC2ERKS0_, .Lfunc_end69-_ZN13CRecordVectorIhEC2ERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table69:
.Lexception38:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp857-.Lfunc_begin38 # >> Call Site 1 <<
	.long	.Ltmp860-.Ltmp857       #   Call between .Ltmp857 and .Ltmp860
	.long	.Ltmp861-.Lfunc_begin38 #     jumps to .Ltmp861
	.byte	0                       #   On action: cleanup
	.long	.Ltmp862-.Lfunc_begin38 # >> Call Site 2 <<
	.long	.Ltmp863-.Ltmp862       #   Call between .Ltmp862 and .Ltmp863
	.long	.Ltmp864-.Lfunc_begin38 #     jumps to .Ltmp864
	.byte	0                       #   On action: cleanup
	.long	.Ltmp865-.Lfunc_begin38 # >> Call Site 3 <<
	.long	.Ltmp866-.Ltmp865       #   Call between .Ltmp865 and .Ltmp866
	.long	.Ltmp867-.Lfunc_begin38 #     jumps to .Ltmp867
	.byte	1                       #   On action: 1
	.long	.Ltmp866-.Lfunc_begin38 # >> Call Site 4 <<
	.long	.Lfunc_end69-.Ltmp866   #   Call between .Ltmp866 and .Lfunc_end69
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZipL11CoderThreadEPv,@function
_ZN8NArchive4NZipL11CoderThreadEPv:     # @_ZN8NArchive4NZipL11CoderThreadEPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi427:
	.cfi_def_cfa_offset 16
	callq	_ZN8NArchive4NZip11CThreadInfo11WaitAndCodeEv
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end70:
	.size	_ZN8NArchive4NZipL11CoderThreadEPv, .Lfunc_end70-_ZN8NArchive4NZipL11CoderThreadEPv
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip5CItemaSERKS1_,"axG",@progbits,_ZN8NArchive4NZip5CItemaSERKS1_,comdat
	.weak	_ZN8NArchive4NZip5CItemaSERKS1_
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip5CItemaSERKS1_,@function
_ZN8NArchive4NZip5CItemaSERKS1_:        # @_ZN8NArchive4NZip5CItemaSERKS1_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi428:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi429:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi430:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi431:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi432:
	.cfi_def_cfa_offset 48
.Lcfi433:
	.cfi_offset %rbx, -48
.Lcfi434:
	.cfi_offset %r12, -40
.Lcfi435:
	.cfi_offset %r14, -32
.Lcfi436:
	.cfi_offset %r15, -24
.Lcfi437:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movups	(%r14), %xmm0
	movups	16(%r14), %xmm1
	movups	%xmm1, 16(%r15)
	movups	%xmm0, (%r15)
	cmpq	%r15, %r14
	je	.LBB71_30
# BB#1:
	movl	$0, 40(%r15)
	movq	32(%r15), %rax
	movb	$0, (%rax)
	movslq	40(%r14), %rax
	leaq	1(%rax), %r12
	movl	44(%r15), %ebp
	cmpl	%ebp, %r12d
	jne	.LBB71_3
# BB#2:                                 # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i.i
	movq	32(%r15), %rbx
	jmp	.LBB71_27
.LBB71_3:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r12, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	testl	%ebp, %ebp
	jle	.LBB71_26
# BB#4:                                 # %.preheader.i.i.i
	movslq	40(%r15), %rbp
	testq	%rbp, %rbp
	movq	32(%r15), %rdi
	jle	.LBB71_24
# BB#5:                                 # %.lr.ph.preheader.i.i.i
	cmpl	$31, %ebp
	jbe	.LBB71_6
# BB#13:                                # %min.iters.checked
	movq	%rbp, %rcx
	andq	$-32, %rcx
	je	.LBB71_6
# BB#14:                                # %vector.memcheck
	leaq	(%rdi,%rbp), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB71_16
# BB#15:                                # %vector.memcheck
	leaq	(%rbx,%rbp), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB71_16
.LBB71_6:
	xorl	%ecx, %ecx
.LBB71_7:                               # %.lr.ph.i.i.i.preheader
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rbp), %rax
	subq	%rcx, %rax
	andq	$7, %rsi
	je	.LBB71_10
# BB#8:                                 # %.lr.ph.i.i.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB71_9:                               # %.lr.ph.i.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB71_9
.LBB71_10:                              # %.lr.ph.i.i.i.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB71_25
# BB#11:                                # %.lr.ph.i.i.i.preheader.new
	subq	%rcx, %rbp
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB71_12:                              # %.lr.ph.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rbp
	jne	.LBB71_12
	jmp	.LBB71_25
.LBB71_24:                              # %._crit_edge.i.i.i
	testq	%rdi, %rdi
	je	.LBB71_26
.LBB71_25:                              # %._crit_edge.thread.i.i.i
	callq	_ZdaPv
.LBB71_26:                              # %._crit_edge17.i.i.i
	movq	%rbx, 32(%r15)
	movslq	40(%r15), %rax
	movb	$0, (%rbx,%rax)
	movl	%r12d, 44(%r15)
.LBB71_27:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i
	movq	32(%r14), %rax
	.p2align	4, 0x90
.LBB71_28:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB71_28
# BB#29:                                # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i.i
	movl	40(%r14), %eax
	movl	%eax, 40(%r15)
.LBB71_30:                              # %_ZN8NArchive4NZip10CLocalItemaSERKS1_.exit
	leaq	48(%r15), %rdi
	leaq	48(%r14), %rsi
	callq	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_
	movq	112(%r14), %rax
	movq	%rax, 112(%r15)
	movups	80(%r14), %xmm0
	movups	96(%r14), %xmm1
	movups	%xmm1, 96(%r15)
	movups	%xmm0, 80(%r15)
	leaq	120(%r15), %rdi
	leaq	120(%r14), %rsi
	callq	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_
	movq	168(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB71_32
# BB#31:
	callq	_ZdaPv
.LBB71_32:                              # %_ZN7CBufferIhE4FreeEv.exit.i
	xorps	%xmm0, %xmm0
	movups	%xmm0, 160(%r15)
	movq	160(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB71_34
# BB#33:                                # %_ZN7CBufferIhE11SetCapacityEm.exit.i
	movq	%rbx, %rdi
	callq	_Znam
	movq	%rax, 168(%r15)
	movq	%rbx, 160(%r15)
	movq	160(%r14), %rdx
	movq	168(%r14), %rsi
	movq	%rax, %rdi
	callq	memmove
.LBB71_34:                              # %_ZN7CBufferIhEaSERKS0_.exit
	movb	178(%r14), %al
	movb	%al, 178(%r15)
	movzwl	176(%r14), %eax
	movw	%ax, 176(%r15)
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB71_16:                              # %vector.body.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB71_17
# BB#18:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB71_19:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%rbx,%rdx)
	movups	%xmm1, 16(%rbx,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB71_19
	jmp	.LBB71_20
.LBB71_17:
	xorl	%edx, %edx
.LBB71_20:                              # %vector.body.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB71_23
# BB#21:                                # %vector.body.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbx,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
	.p2align	4, 0x90
.LBB71_22:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB71_22
.LBB71_23:                              # %middle.block
	cmpq	%rcx, %rbp
	jne	.LBB71_7
	jmp	.LBB71_25
.Lfunc_end71:
	.size	_ZN8NArchive4NZip5CItemaSERKS1_, .Lfunc_end71-_ZN8NArchive4NZip5CItemaSERKS1_
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip11CExtraBlock22RemoveUnknownSubBlocksEv,"axG",@progbits,_ZN8NArchive4NZip11CExtraBlock22RemoveUnknownSubBlocksEv,comdat
	.weak	_ZN8NArchive4NZip11CExtraBlock22RemoveUnknownSubBlocksEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11CExtraBlock22RemoveUnknownSubBlocksEv,@function
_ZN8NArchive4NZip11CExtraBlock22RemoveUnknownSubBlocksEv: # @_ZN8NArchive4NZip11CExtraBlock22RemoveUnknownSubBlocksEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi438:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi439:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi440:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi441:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi442:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi443:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi444:
	.cfi_def_cfa_offset 80
.Lcfi445:
	.cfi_offset %rbx, -56
.Lcfi446:
	.cfi_offset %r12, -48
.Lcfi447:
	.cfi_offset %r13, -40
.Lcfi448:
	.cfi_offset %r14, -32
.Lcfi449:
	.cfi_offset %r15, -24
.Lcfi450:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movslq	12(%rbx), %r12
	testq	%r12, %r12
	jle	.LBB72_4
# BB#1:                                 # %.lr.ph
	leaq	-1(%r12), %rdx
	.p2align	4, 0x90
.LBB72_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB72_7 Depth 2
	leaq	-1(%r12), %rbp
	movq	16(%rbx), %rax
	movq	-8(%rax,%r12,8), %rcx
	movzwl	(%rcx), %ecx
	cmpl	$39169, %ecx            # imm = 0x9901
	je	.LBB72_3
# BB#5:                                 #   in Loop: Header=BB72_2 Depth=1
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	12(%rbx), %ecx
	movl	%ecx, %edx
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	subl	%ebp, %edx
	cmpl	%ecx, %r12d
	movl	$1, %ecx
	cmovlel	%ecx, %edx
	movl	%edx, 4(%rsp)           # 4-byte Spill
	testl	%edx, %edx
	jle	.LBB72_13
# BB#6:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB72_2 Depth=1
	movslq	4(%rsp), %r14           # 4-byte Folded Reload
	movl	$1, %r15d
	movq	8(%rsp), %r13           # 8-byte Reload
	jmp	.LBB72_7
	.p2align	4, 0x90
.LBB72_12:                              # %._crit_edge8
                                        #   in Loop: Header=BB72_7 Depth=2
	movq	16(%rbx), %rax
	incq	%r13
	incq	%r15
.LBB72_7:                               #   Parent Loop BB72_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%r13,8), %rbp
	testq	%rbp, %rbp
	je	.LBB72_11
# BB#8:                                 #   in Loop: Header=BB72_7 Depth=2
	movq	$_ZTV7CBufferIhE+16, 8(%rbp)
	movq	24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB72_10
# BB#9:                                 #   in Loop: Header=BB72_7 Depth=2
	callq	_ZdaPv
.LBB72_10:                              # %_ZN8NArchive4NZip14CExtraSubBlockD2Ev.exit.i
                                        #   in Loop: Header=BB72_7 Depth=2
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB72_11:                              #   in Loop: Header=BB72_7 Depth=2
	cmpq	%r14, %r15
	jl	.LBB72_12
.LBB72_13:                              # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii.exit
                                        #   in Loop: Header=BB72_2 Depth=1
	movq	%rbx, %rdi
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %esi
	movl	4(%rsp), %edx           # 4-byte Reload
	callq	_ZN17CBaseRecordVector6DeleteEii
	movq	8(%rsp), %rdx           # 8-byte Reload
.LBB72_3:                               # %.backedge
                                        #   in Loop: Header=BB72_2 Depth=1
	decq	%rdx
	cmpq	$1, %r12
	movq	%rbp, %r12
	jg	.LBB72_2
.LBB72_4:                               # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end72:
	.size	_ZN8NArchive4NZip11CExtraBlock22RemoveUnknownSubBlocksEv, .Lfunc_end72-_ZN8NArchive4NZip11CExtraBlock22RemoveUnknownSubBlocksEv
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZipL10WriteRangeEP9IInStreamRNS0_11COutArchiveERKNS0_12CUpdateRangeEP21ICompressProgressInfo,@function
_ZN8NArchive4NZipL10WriteRangeEP9IInStreamRNS0_11COutArchiveERKNS0_12CUpdateRangeEP21ICompressProgressInfo: # @_ZN8NArchive4NZipL10WriteRangeEP9IInStreamRNS0_11COutArchiveERKNS0_12CUpdateRangeEP21ICompressProgressInfo
.Lfunc_begin39:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception39
# BB#0:
	pushq	%rbp
.Lcfi451:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi452:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi453:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi454:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi455:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi456:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi457:
	.cfi_def_cfa_offset 80
.Lcfi458:
	.cfi_offset %rbx, -56
.Lcfi459:
	.cfi_offset %r12, -48
.Lcfi460:
	.cfi_offset %r13, -40
.Lcfi461:
	.cfi_offset %r14, -32
.Lcfi462:
	.cfi_offset %r15, -24
.Lcfi463:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	movq	(%r14), %rsi
	leaq	16(%rsp), %rcx
	xorl	%edx, %edx
	callq	*48(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB73_11
# BB#1:
	movl	$48, %edi
	callq	_Znwm
	movq	%rax, %r13
	movl	$0, 8(%r13)
	movq	$_ZTV26CLimitedSequentialInStream+16, (%r13)
	movq	$0, 16(%r13)
	movq	%r13, %rdi
	callq	*_ZTV26CLimitedSequentialInStream+24(%rip)
	movq	(%rbx), %rax
.Ltmp868:
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp869:
# BB#2:                                 # %.noexc
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB73_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp870:
	callq	*16(%rax)
.Ltmp871:
.LBB73_4:
	movq	%rbx, 16(%r13)
	movq	8(%r14), %rax
	movq	%rax, 24(%r13)
	movq	$0, 32(%r13)
	movb	$0, 40(%r13)
	movq	$0, 8(%rsp)
.Ltmp872:
	leaq	8(%rsp), %rsi
	movq	%r12, %rdi
	callq	_ZN8NArchive4NZip11COutArchive22CreateStreamForCopyingEPP20ISequentialOutStream
.Ltmp873:
# BB#5:
	movq	8(%rsp), %rsi
.Ltmp874:
	movq	%r13, %rdi
	movq	%r15, %rdx
	callq	_ZN9NCompress10CopyStreamEP19ISequentialInStreamP20ISequentialOutStreamP21ICompressProgressInfo
	movl	%eax, %ebp
.Ltmp875:
# BB#6:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB73_8
# BB#7:
	movq	(%rdi), %rax
.Ltmp880:
	callq	*16(%rax)
.Ltmp881:
.LBB73_8:
	testl	%ebp, %ebp
	jne	.LBB73_10
# BB#9:
	addq	$8, %r14
	movq	(%r15), %rax
.Ltmp882:
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%r14, %rdx
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp883:
.LBB73_10:                              # %_ZN9CMyComPtrI26CLimitedSequentialInStreamED2Ev.exit
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*16(%rax)
.LBB73_11:
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB73_12:
.Ltmp876:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB73_16
# BB#13:
	movq	(%rdi), %rax
.Ltmp877:
	callq	*16(%rax)
.Ltmp878:
	jmp	.LBB73_16
.LBB73_14:
.Ltmp879:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB73_15:
.Ltmp884:
	movq	%rax, %rbx
.LBB73_16:                              # %.body
	movq	(%r13), %rax
.Ltmp885:
	movq	%r13, %rdi
	callq	*16(%rax)
.Ltmp886:
# BB#17:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB73_18:
.Ltmp887:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end73:
	.size	_ZN8NArchive4NZipL10WriteRangeEP9IInStreamRNS0_11COutArchiveERKNS0_12CUpdateRangeEP21ICompressProgressInfo, .Lfunc_end73-_ZN8NArchive4NZipL10WriteRangeEP9IInStreamRNS0_11COutArchiveERKNS0_12CUpdateRangeEP21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table73:
.Lexception39:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Lfunc_begin39-.Lfunc_begin39 # >> Call Site 1 <<
	.long	.Ltmp868-.Lfunc_begin39 #   Call between .Lfunc_begin39 and .Ltmp868
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp868-.Lfunc_begin39 # >> Call Site 2 <<
	.long	.Ltmp871-.Ltmp868       #   Call between .Ltmp868 and .Ltmp871
	.long	.Ltmp884-.Lfunc_begin39 #     jumps to .Ltmp884
	.byte	0                       #   On action: cleanup
	.long	.Ltmp872-.Lfunc_begin39 # >> Call Site 3 <<
	.long	.Ltmp875-.Ltmp872       #   Call between .Ltmp872 and .Ltmp875
	.long	.Ltmp876-.Lfunc_begin39 #     jumps to .Ltmp876
	.byte	0                       #   On action: cleanup
	.long	.Ltmp880-.Lfunc_begin39 # >> Call Site 4 <<
	.long	.Ltmp883-.Ltmp880       #   Call between .Ltmp880 and .Ltmp883
	.long	.Ltmp884-.Lfunc_begin39 #     jumps to .Ltmp884
	.byte	0                       #   On action: cleanup
	.long	.Ltmp883-.Lfunc_begin39 # >> Call Site 5 <<
	.long	.Ltmp877-.Ltmp883       #   Call between .Ltmp883 and .Ltmp877
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp877-.Lfunc_begin39 # >> Call Site 6 <<
	.long	.Ltmp878-.Ltmp877       #   Call between .Ltmp877 and .Ltmp878
	.long	.Ltmp879-.Lfunc_begin39 #     jumps to .Ltmp879
	.byte	1                       #   On action: 1
	.long	.Ltmp885-.Lfunc_begin39 # >> Call Site 7 <<
	.long	.Ltmp886-.Ltmp885       #   Call between .Ltmp885 and .Ltmp886
	.long	.Ltmp887-.Lfunc_begin39 #     jumps to .Ltmp887
	.byte	1                       #   On action: 1
	.long	.Ltmp886-.Lfunc_begin39 # >> Call Site 8 <<
	.long	.Lfunc_end73-.Ltmp886   #   Call between .Ltmp886 and .Lfunc_end73
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI74_0:
	.zero	16
	.section	.text._ZN8NArchive4NZip5CItemC2ERKS1_,"axG",@progbits,_ZN8NArchive4NZip5CItemC2ERKS1_,comdat
	.weak	_ZN8NArchive4NZip5CItemC2ERKS1_
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip5CItemC2ERKS1_,@function
_ZN8NArchive4NZip5CItemC2ERKS1_:        # @_ZN8NArchive4NZip5CItemC2ERKS1_
.Lfunc_begin40:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception40
# BB#0:
	pushq	%r15
.Lcfi464:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi465:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi466:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi467:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi468:
	.cfi_def_cfa_offset 48
.Lcfi469:
	.cfi_offset %rbx, -48
.Lcfi470:
	.cfi_offset %r12, -40
.Lcfi471:
	.cfi_offset %r13, -32
.Lcfi472:
	.cfi_offset %r14, -24
.Lcfi473:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movups	(%r14), %xmm0
	movups	16(%r14), %xmm1
	movups	%xmm1, 16(%rbx)
	movups	%xmm0, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	movslq	40(%r14), %rax
	leaq	1(%rax), %r15
	testl	%r15d, %r15d
	je	.LBB74_1
# BB#2:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r15, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, 32(%rbx)
	movb	$0, (%rax)
	movl	%r15d, 44(%rbx)
	jmp	.LBB74_3
.LBB74_1:
	xorl	%eax, %eax
.LBB74_3:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i
	leaq	32(%rbx), %r12
	movq	32(%r14), %rcx
	.p2align	4, 0x90
.LBB74_4:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB74_4
# BB#5:                                 # %_ZN11CStringBaseIcEC2ERKS0_.exit.i
	movl	40(%r14), %eax
	movl	%eax, 40(%rbx)
	leaq	48(%rbx), %r15
	leaq	48(%r14), %rsi
	xorps	%xmm0, %xmm0
	movups	%xmm0, 56(%rbx)
	movq	$8, 72(%rbx)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 48(%rbx)
.Ltmp888:
	movq	%r15, %rdi
	callq	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_
.Ltmp889:
# BB#6:                                 # %_ZN8NArchive4NZip10CLocalItemC2ERKS1_.exit
	movq	112(%r14), %rax
	movq	%rax, 112(%rbx)
	movups	80(%r14), %xmm0
	movups	96(%r14), %xmm1
	movups	%xmm1, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	leaq	120(%rbx), %r12
	leaq	120(%r14), %rsi
	xorps	%xmm0, %xmm0
	movups	%xmm0, 128(%rbx)
	movq	$8, 144(%rbx)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 120(%rbx)
.Ltmp894:
	movq	%r12, %rdi
	callq	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_
.Ltmp895:
# BB#7:                                 # %_ZN8NArchive4NZip11CExtraBlockC2ERKS1_.exit
	movq	$_ZTV7CBufferIhE+16, 152(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 160(%rbx)
	movq	160(%r14), %r13
	testq	%r13, %r13
	je	.LBB74_10
# BB#8:                                 # %_ZN7CBufferIhE11SetCapacityEm.exit.i.i
.Ltmp900:
	movq	%r13, %rdi
	callq	_Znam
.Ltmp901:
# BB#9:                                 # %.noexc
	movq	%rax, 168(%rbx)
	movq	%r13, 160(%rbx)
	movq	160(%r14), %rdx
	movq	168(%r14), %rsi
	movq	%rax, %rdi
	callq	memmove
.LBB74_10:                              # %_ZN7CBufferIhEC2ERKS0_.exit
	movb	178(%r14), %al
	movb	%al, 178(%rbx)
	movzwl	176(%r14), %eax
	movw	%ax, 176(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB74_18:
.Ltmp902:
	movq	%rax, %r14
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, (%r12)
.Ltmp903:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp904:
# BB#19:                                # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev.exit.i
.Ltmp909:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp910:
	jmp	.LBB74_20
.LBB74_31:
.Ltmp911:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB74_16:
.Ltmp905:
	movq	%rax, %r14
.Ltmp906:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp907:
	jmp	.LBB74_32
.LBB74_17:
.Ltmp908:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB74_15:
.Ltmp896:
	movq	%rax, %r14
.Ltmp897:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp898:
.LBB74_20:                              # %_ZN8NArchive4NZip11CExtraBlockD2Ev.exit
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, (%r15)
.Ltmp912:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp913:
# BB#21:                                # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev.exit.i.i
.Ltmp918:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp919:
# BB#22:                                # %_ZN8NArchive4NZip11CExtraBlockD2Ev.exit.i
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB74_24
	jmp	.LBB74_25
.LBB74_14:
.Ltmp899:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB74_28:
.Ltmp920:
	movq	%rax, %r14
	jmp	.LBB74_29
.LBB74_26:
.Ltmp914:
	movq	%rax, %r14
.Ltmp915:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp916:
.LBB74_29:                              # %.body.i12
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB74_30
.LBB74_32:                              # %.body8
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB74_30:
	callq	_ZdaPv
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB74_27:
.Ltmp917:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB74_12:
.Ltmp890:
	movq	%rax, %r14
.Ltmp891:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp892:
# BB#13:                                # %.body.i
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB74_25
.LBB74_24:
	callq	_ZdaPv
.LBB74_25:                              # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB74_11:
.Ltmp893:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end74:
	.size	_ZN8NArchive4NZip5CItemC2ERKS1_, .Lfunc_end74-_ZN8NArchive4NZip5CItemC2ERKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table74:
.Lexception40:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\277\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\266\001"              # Call site table length
	.long	.Lfunc_begin40-.Lfunc_begin40 # >> Call Site 1 <<
	.long	.Ltmp888-.Lfunc_begin40 #   Call between .Lfunc_begin40 and .Ltmp888
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp888-.Lfunc_begin40 # >> Call Site 2 <<
	.long	.Ltmp889-.Ltmp888       #   Call between .Ltmp888 and .Ltmp889
	.long	.Ltmp890-.Lfunc_begin40 #     jumps to .Ltmp890
	.byte	0                       #   On action: cleanup
	.long	.Ltmp894-.Lfunc_begin40 # >> Call Site 3 <<
	.long	.Ltmp895-.Ltmp894       #   Call between .Ltmp894 and .Ltmp895
	.long	.Ltmp896-.Lfunc_begin40 #     jumps to .Ltmp896
	.byte	0                       #   On action: cleanup
	.long	.Ltmp900-.Lfunc_begin40 # >> Call Site 4 <<
	.long	.Ltmp901-.Ltmp900       #   Call between .Ltmp900 and .Ltmp901
	.long	.Ltmp902-.Lfunc_begin40 #     jumps to .Ltmp902
	.byte	0                       #   On action: cleanup
	.long	.Ltmp901-.Lfunc_begin40 # >> Call Site 5 <<
	.long	.Ltmp903-.Ltmp901       #   Call between .Ltmp901 and .Ltmp903
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp903-.Lfunc_begin40 # >> Call Site 6 <<
	.long	.Ltmp904-.Ltmp903       #   Call between .Ltmp903 and .Ltmp904
	.long	.Ltmp905-.Lfunc_begin40 #     jumps to .Ltmp905
	.byte	1                       #   On action: 1
	.long	.Ltmp909-.Lfunc_begin40 # >> Call Site 7 <<
	.long	.Ltmp910-.Ltmp909       #   Call between .Ltmp909 and .Ltmp910
	.long	.Ltmp911-.Lfunc_begin40 #     jumps to .Ltmp911
	.byte	1                       #   On action: 1
	.long	.Ltmp906-.Lfunc_begin40 # >> Call Site 8 <<
	.long	.Ltmp907-.Ltmp906       #   Call between .Ltmp906 and .Ltmp907
	.long	.Ltmp908-.Lfunc_begin40 #     jumps to .Ltmp908
	.byte	1                       #   On action: 1
	.long	.Ltmp897-.Lfunc_begin40 # >> Call Site 9 <<
	.long	.Ltmp898-.Ltmp897       #   Call between .Ltmp897 and .Ltmp898
	.long	.Ltmp899-.Lfunc_begin40 #     jumps to .Ltmp899
	.byte	1                       #   On action: 1
	.long	.Ltmp912-.Lfunc_begin40 # >> Call Site 10 <<
	.long	.Ltmp913-.Ltmp912       #   Call between .Ltmp912 and .Ltmp913
	.long	.Ltmp914-.Lfunc_begin40 #     jumps to .Ltmp914
	.byte	1                       #   On action: 1
	.long	.Ltmp918-.Lfunc_begin40 # >> Call Site 11 <<
	.long	.Ltmp919-.Ltmp918       #   Call between .Ltmp918 and .Ltmp919
	.long	.Ltmp920-.Lfunc_begin40 #     jumps to .Ltmp920
	.byte	1                       #   On action: 1
	.long	.Ltmp915-.Lfunc_begin40 # >> Call Site 12 <<
	.long	.Ltmp916-.Ltmp915       #   Call between .Ltmp915 and .Ltmp916
	.long	.Ltmp917-.Lfunc_begin40 #     jumps to .Ltmp917
	.byte	1                       #   On action: 1
	.long	.Ltmp891-.Lfunc_begin40 # >> Call Site 13 <<
	.long	.Ltmp892-.Ltmp891       #   Call between .Ltmp891 and .Ltmp892
	.long	.Ltmp893-.Lfunc_begin40 #     jumps to .Ltmp893
	.byte	1                       #   On action: 1
	.long	.Ltmp892-.Lfunc_begin40 # >> Call Site 14 <<
	.long	.Lfunc_end74-.Ltmp892   #   Call between .Ltmp892 and .Lfunc_end74
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.type	_ZTVN8NArchive4NZip15CCacheOutStreamE,@object # @_ZTVN8NArchive4NZip15CCacheOutStreamE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN8NArchive4NZip15CCacheOutStreamE
	.p2align	3
_ZTVN8NArchive4NZip15CCacheOutStreamE:
	.quad	0
	.quad	_ZTIN8NArchive4NZip15CCacheOutStreamE
	.quad	_ZN8NArchive4NZip15CCacheOutStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive4NZip15CCacheOutStream6AddRefEv
	.quad	_ZN8NArchive4NZip15CCacheOutStream7ReleaseEv
	.quad	_ZN8NArchive4NZip15CCacheOutStreamD2Ev
	.quad	_ZN8NArchive4NZip15CCacheOutStreamD0Ev
	.quad	_ZN8NArchive4NZip15CCacheOutStream5WriteEPKvjPj
	.quad	_ZN8NArchive4NZip15CCacheOutStream4SeekExjPy
	.quad	_ZN8NArchive4NZip15CCacheOutStream7SetSizeEy
	.size	_ZTVN8NArchive4NZip15CCacheOutStreamE, 80

	.type	_ZTVN8NArchive4NZip17CMtProgressMixer2E,@object # @_ZTVN8NArchive4NZip17CMtProgressMixer2E
	.globl	_ZTVN8NArchive4NZip17CMtProgressMixer2E
	.p2align	3
_ZTVN8NArchive4NZip17CMtProgressMixer2E:
	.quad	0
	.quad	_ZTIN8NArchive4NZip17CMtProgressMixer2E
	.quad	_ZN8NArchive4NZip17CMtProgressMixer214QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive4NZip17CMtProgressMixer26AddRefEv
	.quad	_ZN8NArchive4NZip17CMtProgressMixer27ReleaseEv
	.quad	_ZN8NArchive4NZip17CMtProgressMixer2D2Ev
	.quad	_ZN8NArchive4NZip17CMtProgressMixer2D0Ev
	.quad	_ZN8NArchive4NZip17CMtProgressMixer212SetRatioInfoEPKyS3_
	.size	_ZTVN8NArchive4NZip17CMtProgressMixer2E, 64

	.type	_ZTSN8NArchive4NZip17CMtProgressMixer2E,@object # @_ZTSN8NArchive4NZip17CMtProgressMixer2E
	.globl	_ZTSN8NArchive4NZip17CMtProgressMixer2E
	.p2align	4
_ZTSN8NArchive4NZip17CMtProgressMixer2E:
	.asciz	"N8NArchive4NZip17CMtProgressMixer2E"
	.size	_ZTSN8NArchive4NZip17CMtProgressMixer2E, 36

	.type	_ZTS21ICompressProgressInfo,@object # @_ZTS21ICompressProgressInfo
	.section	.rodata._ZTS21ICompressProgressInfo,"aG",@progbits,_ZTS21ICompressProgressInfo,comdat
	.weak	_ZTS21ICompressProgressInfo
	.p2align	4
_ZTS21ICompressProgressInfo:
	.asciz	"21ICompressProgressInfo"
	.size	_ZTS21ICompressProgressInfo, 24

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI21ICompressProgressInfo,@object # @_ZTI21ICompressProgressInfo
	.section	.rodata._ZTI21ICompressProgressInfo,"aG",@progbits,_ZTI21ICompressProgressInfo,comdat
	.weak	_ZTI21ICompressProgressInfo
	.p2align	4
_ZTI21ICompressProgressInfo:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS21ICompressProgressInfo
	.quad	_ZTI8IUnknown
	.size	_ZTI21ICompressProgressInfo, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN8NArchive4NZip17CMtProgressMixer2E,@object # @_ZTIN8NArchive4NZip17CMtProgressMixer2E
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive4NZip17CMtProgressMixer2E
	.p2align	4
_ZTIN8NArchive4NZip17CMtProgressMixer2E:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive4NZip17CMtProgressMixer2E
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI21ICompressProgressInfo
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTIN8NArchive4NZip17CMtProgressMixer2E, 56

	.type	_ZTVN8NArchive4NZip16CMtProgressMixerE,@object # @_ZTVN8NArchive4NZip16CMtProgressMixerE
	.globl	_ZTVN8NArchive4NZip16CMtProgressMixerE
	.p2align	3
_ZTVN8NArchive4NZip16CMtProgressMixerE:
	.quad	0
	.quad	_ZTIN8NArchive4NZip16CMtProgressMixerE
	.quad	_ZN8NArchive4NZip16CMtProgressMixer14QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive4NZip16CMtProgressMixer6AddRefEv
	.quad	_ZN8NArchive4NZip16CMtProgressMixer7ReleaseEv
	.quad	_ZN8NArchive4NZip16CMtProgressMixerD2Ev
	.quad	_ZN8NArchive4NZip16CMtProgressMixerD0Ev
	.quad	_ZN8NArchive4NZip16CMtProgressMixer12SetRatioInfoEPKyS3_
	.size	_ZTVN8NArchive4NZip16CMtProgressMixerE, 64

	.type	_ZTSN8NArchive4NZip16CMtProgressMixerE,@object # @_ZTSN8NArchive4NZip16CMtProgressMixerE
	.globl	_ZTSN8NArchive4NZip16CMtProgressMixerE
	.p2align	4
_ZTSN8NArchive4NZip16CMtProgressMixerE:
	.asciz	"N8NArchive4NZip16CMtProgressMixerE"
	.size	_ZTSN8NArchive4NZip16CMtProgressMixerE, 35

	.type	_ZTIN8NArchive4NZip16CMtProgressMixerE,@object # @_ZTIN8NArchive4NZip16CMtProgressMixerE
	.globl	_ZTIN8NArchive4NZip16CMtProgressMixerE
	.p2align	4
_ZTIN8NArchive4NZip16CMtProgressMixerE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive4NZip16CMtProgressMixerE
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI21ICompressProgressInfo
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTIN8NArchive4NZip16CMtProgressMixerE, 56

	.type	_ZTSN8NArchive4NZip15CCacheOutStreamE,@object # @_ZTSN8NArchive4NZip15CCacheOutStreamE
	.globl	_ZTSN8NArchive4NZip15CCacheOutStreamE
	.p2align	4
_ZTSN8NArchive4NZip15CCacheOutStreamE:
	.asciz	"N8NArchive4NZip15CCacheOutStreamE"
	.size	_ZTSN8NArchive4NZip15CCacheOutStreamE, 34

	.type	_ZTS10IOutStream,@object # @_ZTS10IOutStream
	.section	.rodata._ZTS10IOutStream,"aG",@progbits,_ZTS10IOutStream,comdat
	.weak	_ZTS10IOutStream
_ZTS10IOutStream:
	.asciz	"10IOutStream"
	.size	_ZTS10IOutStream, 13

	.type	_ZTS20ISequentialOutStream,@object # @_ZTS20ISequentialOutStream
	.section	.rodata._ZTS20ISequentialOutStream,"aG",@progbits,_ZTS20ISequentialOutStream,comdat
	.weak	_ZTS20ISequentialOutStream
	.p2align	4
_ZTS20ISequentialOutStream:
	.asciz	"20ISequentialOutStream"
	.size	_ZTS20ISequentialOutStream, 23

	.type	_ZTI20ISequentialOutStream,@object # @_ZTI20ISequentialOutStream
	.section	.rodata._ZTI20ISequentialOutStream,"aG",@progbits,_ZTI20ISequentialOutStream,comdat
	.weak	_ZTI20ISequentialOutStream
	.p2align	4
_ZTI20ISequentialOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ISequentialOutStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ISequentialOutStream, 24

	.type	_ZTI10IOutStream,@object # @_ZTI10IOutStream
	.section	.rodata._ZTI10IOutStream,"aG",@progbits,_ZTI10IOutStream,comdat
	.weak	_ZTI10IOutStream
	.p2align	4
_ZTI10IOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS10IOutStream
	.quad	_ZTI20ISequentialOutStream
	.size	_ZTI10IOutStream, 24

	.type	_ZTIN8NArchive4NZip15CCacheOutStreamE,@object # @_ZTIN8NArchive4NZip15CCacheOutStreamE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive4NZip15CCacheOutStreamE
	.p2align	4
_ZTIN8NArchive4NZip15CCacheOutStreamE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive4NZip15CCacheOutStreamE
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI10IOutStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTIN8NArchive4NZip15CCacheOutStreamE, 56

	.type	_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,@object # @_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.quad	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,@object # @_ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE:
	.asciz	"13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE"
	.size	_ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE, 50

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,@object # @_ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE, 24

	.type	_ZTV13CRecordVectorIPvE,@object # @_ZTV13CRecordVectorIPvE
	.section	.rodata._ZTV13CRecordVectorIPvE,"aG",@progbits,_ZTV13CRecordVectorIPvE,comdat
	.weak	_ZTV13CRecordVectorIPvE
	.p2align	3
_ZTV13CRecordVectorIPvE:
	.quad	0
	.quad	_ZTI13CRecordVectorIPvE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIPvED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIPvE, 40

	.type	_ZTV7CBufferIhE,@object # @_ZTV7CBufferIhE
	.section	.rodata._ZTV7CBufferIhE,"aG",@progbits,_ZTV7CBufferIhE,comdat
	.weak	_ZTV7CBufferIhE
	.p2align	3
_ZTV7CBufferIhE:
	.quad	0
	.quad	_ZTI7CBufferIhE
	.quad	_ZN7CBufferIhED2Ev
	.quad	_ZN7CBufferIhED0Ev
	.size	_ZTV7CBufferIhE, 32

	.type	_ZTS7CBufferIhE,@object # @_ZTS7CBufferIhE
	.section	.rodata._ZTS7CBufferIhE,"aG",@progbits,_ZTS7CBufferIhE,comdat
	.weak	_ZTS7CBufferIhE
_ZTS7CBufferIhE:
	.asciz	"7CBufferIhE"
	.size	_ZTS7CBufferIhE, 12

	.type	_ZTI7CBufferIhE,@object # @_ZTI7CBufferIhE
	.section	.rodata._ZTI7CBufferIhE,"aG",@progbits,_ZTI7CBufferIhE,comdat
	.weak	_ZTI7CBufferIhE
	.p2align	3
_ZTI7CBufferIhE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS7CBufferIhE
	.size	_ZTI7CBufferIhE, 16

	.type	_ZTV13CRecordVectorIhE,@object # @_ZTV13CRecordVectorIhE
	.section	.rodata._ZTV13CRecordVectorIhE,"aG",@progbits,_ZTV13CRecordVectorIhE,comdat
	.weak	_ZTV13CRecordVectorIhE
	.p2align	3
_ZTV13CRecordVectorIhE:
	.quad	0
	.quad	_ZTI13CRecordVectorIhE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIhED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIhE, 40

	.type	_ZTS13CRecordVectorIhE,@object # @_ZTS13CRecordVectorIhE
	.section	.rodata._ZTS13CRecordVectorIhE,"aG",@progbits,_ZTS13CRecordVectorIhE,comdat
	.weak	_ZTS13CRecordVectorIhE
	.p2align	4
_ZTS13CRecordVectorIhE:
	.asciz	"13CRecordVectorIhE"
	.size	_ZTS13CRecordVectorIhE, 19

	.type	_ZTI13CRecordVectorIhE,@object # @_ZTI13CRecordVectorIhE
	.section	.rodata._ZTI13CRecordVectorIhE,"aG",@progbits,_ZTI13CRecordVectorIhE,comdat
	.weak	_ZTI13CRecordVectorIhE
	.p2align	4
_ZTI13CRecordVectorIhE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIhE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIhE, 24

	.type	_ZTV13CObjectVectorIN8NArchive4NZip5CItemEE,@object # @_ZTV13CObjectVectorIN8NArchive4NZip5CItemEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive4NZip5CItemEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive4NZip5CItemEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive4NZip5CItemEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive4NZip5CItemEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive4NZip5CItemEE
	.quad	_ZN13CObjectVectorIN8NArchive4NZip5CItemEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NZip5CItemEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NZip5CItemEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive4NZip5CItemEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive4NZip5CItemEE,@object # @_ZTS13CObjectVectorIN8NArchive4NZip5CItemEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive4NZip5CItemEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive4NZip5CItemEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive4NZip5CItemEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive4NZip5CItemEE:
	.asciz	"13CObjectVectorIN8NArchive4NZip5CItemEE"
	.size	_ZTS13CObjectVectorIN8NArchive4NZip5CItemEE, 40

	.type	_ZTI13CObjectVectorIN8NArchive4NZip5CItemEE,@object # @_ZTI13CObjectVectorIN8NArchive4NZip5CItemEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive4NZip5CItemEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive4NZip5CItemEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive4NZip5CItemEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive4NZip5CItemEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive4NZip5CItemEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive4NZip5CItemEE, 24

	.type	_ZTV13CRecordVectorIyE,@object # @_ZTV13CRecordVectorIyE
	.section	.rodata._ZTV13CRecordVectorIyE,"aG",@progbits,_ZTV13CRecordVectorIyE,comdat
	.weak	_ZTV13CRecordVectorIyE
	.p2align	3
_ZTV13CRecordVectorIyE:
	.quad	0
	.quad	_ZTI13CRecordVectorIyE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIyED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIyE, 40

	.type	_ZTS13CRecordVectorIyE,@object # @_ZTS13CRecordVectorIyE
	.section	.rodata._ZTS13CRecordVectorIyE,"aG",@progbits,_ZTS13CRecordVectorIyE,comdat
	.weak	_ZTS13CRecordVectorIyE
	.p2align	4
_ZTS13CRecordVectorIyE:
	.asciz	"13CRecordVectorIyE"
	.size	_ZTS13CRecordVectorIyE, 19

	.type	_ZTI13CRecordVectorIyE,@object # @_ZTI13CRecordVectorIyE
	.section	.rodata._ZTI13CRecordVectorIyE,"aG",@progbits,_ZTI13CRecordVectorIyE,comdat
	.weak	_ZTI13CRecordVectorIyE
	.p2align	4
_ZTI13CRecordVectorIyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIyE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIyE, 24

	.type	_ZTVN8NWindows16NSynchronization14CSemaphoreWFMOE,@object # @_ZTVN8NWindows16NSynchronization14CSemaphoreWFMOE
	.section	.rodata._ZTVN8NWindows16NSynchronization14CSemaphoreWFMOE,"aG",@progbits,_ZTVN8NWindows16NSynchronization14CSemaphoreWFMOE,comdat
	.weak	_ZTVN8NWindows16NSynchronization14CSemaphoreWFMOE
	.p2align	3
_ZTVN8NWindows16NSynchronization14CSemaphoreWFMOE:
	.quad	0
	.quad	_ZTIN8NWindows16NSynchronization14CSemaphoreWFMOE
	.quad	_ZN8NWindows16NSynchronization14CSemaphoreWFMO19IsSignaledAndUpdateEv
	.size	_ZTVN8NWindows16NSynchronization14CSemaphoreWFMOE, 24

	.type	_ZTSN8NWindows16NSynchronization14CSemaphoreWFMOE,@object # @_ZTSN8NWindows16NSynchronization14CSemaphoreWFMOE
	.section	.rodata._ZTSN8NWindows16NSynchronization14CSemaphoreWFMOE,"aG",@progbits,_ZTSN8NWindows16NSynchronization14CSemaphoreWFMOE,comdat
	.weak	_ZTSN8NWindows16NSynchronization14CSemaphoreWFMOE
	.p2align	4
_ZTSN8NWindows16NSynchronization14CSemaphoreWFMOE:
	.asciz	"N8NWindows16NSynchronization14CSemaphoreWFMOE"
	.size	_ZTSN8NWindows16NSynchronization14CSemaphoreWFMOE, 46

	.type	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE,@object # @_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE
	.section	.rodata._ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE,"aG",@progbits,_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE,comdat
	.weak	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE
	.p2align	4
_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE:
	.asciz	"N8NWindows16NSynchronization15CBaseHandleWFMOE"
	.size	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE, 47

	.type	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE,@object # @_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE
	.section	.rodata._ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE,"aG",@progbits,_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE,comdat
	.weak	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE
	.p2align	3
_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE
	.size	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE, 16

	.type	_ZTIN8NWindows16NSynchronization14CSemaphoreWFMOE,@object # @_ZTIN8NWindows16NSynchronization14CSemaphoreWFMOE
	.section	.rodata._ZTIN8NWindows16NSynchronization14CSemaphoreWFMOE,"aG",@progbits,_ZTIN8NWindows16NSynchronization14CSemaphoreWFMOE,comdat
	.weak	_ZTIN8NWindows16NSynchronization14CSemaphoreWFMOE
	.p2align	4
_ZTIN8NWindows16NSynchronization14CSemaphoreWFMOE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows16NSynchronization14CSemaphoreWFMOE
	.quad	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE
	.size	_ZTIN8NWindows16NSynchronization14CSemaphoreWFMOE, 24

	.type	_ZTV13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE,@object # @_ZTV13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE
	.quad	_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE, 40

	.type	_ZTS13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE,@object # @_ZTS13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE:
	.asciz	"13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE"
	.size	_ZTS13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE, 47

	.type	_ZTI13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE,@object # @_ZTI13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive4NZip11CMemBlocks2EE, 24

	.type	_ZTV13CObjectVectorIN8NArchive4NZip11CThreadInfoEE,@object # @_ZTV13CObjectVectorIN8NArchive4NZip11CThreadInfoEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive4NZip11CThreadInfoEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive4NZip11CThreadInfoEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive4NZip11CThreadInfoEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive4NZip11CThreadInfoEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive4NZip11CThreadInfoEE
	.quad	_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NZip11CThreadInfoEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive4NZip11CThreadInfoEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive4NZip11CThreadInfoEE,@object # @_ZTS13CObjectVectorIN8NArchive4NZip11CThreadInfoEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive4NZip11CThreadInfoEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive4NZip11CThreadInfoEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive4NZip11CThreadInfoEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive4NZip11CThreadInfoEE:
	.asciz	"13CObjectVectorIN8NArchive4NZip11CThreadInfoEE"
	.size	_ZTS13CObjectVectorIN8NArchive4NZip11CThreadInfoEE, 47

	.type	_ZTI13CObjectVectorIN8NArchive4NZip11CThreadInfoEE,@object # @_ZTI13CObjectVectorIN8NArchive4NZip11CThreadInfoEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive4NZip11CThreadInfoEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive4NZip11CThreadInfoEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive4NZip11CThreadInfoEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive4NZip11CThreadInfoEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive4NZip11CThreadInfoEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive4NZip11CThreadInfoEE, 24

	.type	_ZTV13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE,@object # @_ZTV13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE
	.section	.rodata._ZTV13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE,"aG",@progbits,_ZTV13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE,comdat
	.weak	_ZTV13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE
	.p2align	3
_ZTV13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE:
	.quad	0
	.quad	_ZTI13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE, 40

	.type	_ZTS13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE,@object # @_ZTS13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE
	.section	.rodata._ZTS13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE,"aG",@progbits,_ZTS13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE,comdat
	.weak	_ZTS13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE
	.p2align	4
_ZTS13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE:
	.asciz	"13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE"
	.size	_ZTS13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE, 65

	.type	_ZTI13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE,@object # @_ZTI13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE
	.section	.rodata._ZTI13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE,"aG",@progbits,_ZTI13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE,comdat
	.weak	_ZTI13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE
	.p2align	4
_ZTI13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPN8NWindows16NSynchronization15CBaseHandleWFMOEE, 24

	.type	_ZTV13CRecordVectorIiE,@object # @_ZTV13CRecordVectorIiE
	.section	.rodata._ZTV13CRecordVectorIiE,"aG",@progbits,_ZTV13CRecordVectorIiE,comdat
	.weak	_ZTV13CRecordVectorIiE
	.p2align	3
_ZTV13CRecordVectorIiE:
	.quad	0
	.quad	_ZTI13CRecordVectorIiE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIiED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIiE, 40

	.type	_ZTS13CRecordVectorIiE,@object # @_ZTS13CRecordVectorIiE
	.section	.rodata._ZTS13CRecordVectorIiE,"aG",@progbits,_ZTS13CRecordVectorIiE,comdat
	.weak	_ZTS13CRecordVectorIiE
	.p2align	4
_ZTS13CRecordVectorIiE:
	.asciz	"13CRecordVectorIiE"
	.size	_ZTS13CRecordVectorIiE, 19

	.type	_ZTI13CRecordVectorIiE,@object # @_ZTI13CRecordVectorIiE
	.section	.rodata._ZTI13CRecordVectorIiE,"aG",@progbits,_ZTI13CRecordVectorIiE,comdat
	.weak	_ZTI13CRecordVectorIiE
	.p2align	4
_ZTI13CRecordVectorIiE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIiE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIiE, 24

	.type	_ZTVN8NWindows16NSynchronization19CAutoResetEventWFMOE,@object # @_ZTVN8NWindows16NSynchronization19CAutoResetEventWFMOE
	.section	.rodata._ZTVN8NWindows16NSynchronization19CAutoResetEventWFMOE,"aG",@progbits,_ZTVN8NWindows16NSynchronization19CAutoResetEventWFMOE,comdat
	.weak	_ZTVN8NWindows16NSynchronization19CAutoResetEventWFMOE
	.p2align	3
_ZTVN8NWindows16NSynchronization19CAutoResetEventWFMOE:
	.quad	0
	.quad	_ZTIN8NWindows16NSynchronization19CAutoResetEventWFMOE
	.quad	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.size	_ZTVN8NWindows16NSynchronization19CAutoResetEventWFMOE, 24

	.type	_ZTSN8NWindows16NSynchronization19CAutoResetEventWFMOE,@object # @_ZTSN8NWindows16NSynchronization19CAutoResetEventWFMOE
	.section	.rodata._ZTSN8NWindows16NSynchronization19CAutoResetEventWFMOE,"aG",@progbits,_ZTSN8NWindows16NSynchronization19CAutoResetEventWFMOE,comdat
	.weak	_ZTSN8NWindows16NSynchronization19CAutoResetEventWFMOE
	.p2align	4
_ZTSN8NWindows16NSynchronization19CAutoResetEventWFMOE:
	.asciz	"N8NWindows16NSynchronization19CAutoResetEventWFMOE"
	.size	_ZTSN8NWindows16NSynchronization19CAutoResetEventWFMOE, 51

	.type	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE,@object # @_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE
	.section	.rodata._ZTSN8NWindows16NSynchronization14CBaseEventWFMOE,"aG",@progbits,_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE,comdat
	.weak	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE
	.p2align	4
_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE:
	.asciz	"N8NWindows16NSynchronization14CBaseEventWFMOE"
	.size	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE, 46

	.type	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE,@object # @_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE
	.section	.rodata._ZTIN8NWindows16NSynchronization14CBaseEventWFMOE,"aG",@progbits,_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE,comdat
	.weak	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE
	.p2align	4
_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE
	.quad	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE
	.size	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE, 24

	.type	_ZTIN8NWindows16NSynchronization19CAutoResetEventWFMOE,@object # @_ZTIN8NWindows16NSynchronization19CAutoResetEventWFMOE
	.section	.rodata._ZTIN8NWindows16NSynchronization19CAutoResetEventWFMOE,"aG",@progbits,_ZTIN8NWindows16NSynchronization19CAutoResetEventWFMOE,comdat
	.weak	_ZTIN8NWindows16NSynchronization19CAutoResetEventWFMOE
	.p2align	4
_ZTIN8NWindows16NSynchronization19CAutoResetEventWFMOE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows16NSynchronization19CAutoResetEventWFMOE
	.quad	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE
	.size	_ZTIN8NWindows16NSynchronization19CAutoResetEventWFMOE, 24

	.type	_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE,@object # @_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE
	.section	.rodata._ZTVN8NWindows16NSynchronization14CBaseEventWFMOE,"aG",@progbits,_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE,comdat
	.weak	_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE
	.p2align	3
_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE:
	.quad	0
	.quad	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE
	.quad	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.size	_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE, 24


	.globl	_ZN8NArchive4NZip15CCacheOutStreamD1Ev
	.type	_ZN8NArchive4NZip15CCacheOutStreamD1Ev,@function
_ZN8NArchive4NZip15CCacheOutStreamD1Ev = _ZN8NArchive4NZip15CCacheOutStreamD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
