	.text
	.file	"grow.bc"
	.globl	hypre_GrowBoxByStencil
	.p2align	4, 0x90
	.type	hypre_GrowBoxByStencil,@function
hypre_GrowBoxByStencil:                 # @hypre_GrowBoxByStencil
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	(%r15), %rbp
	movl	8(%r15), %edi
	callq	hypre_BoxArrayCreate
	movq	%rax, %r14
	callq	hypre_BoxCreate
	cmpl	$0, 8(%r15)
	jle	.LBB0_6
# BB#1:                                 # %.lr.ph
	testl	%r12d, %r12d
	movq	(%r14), %rcx
	je	.LBB0_4
# BB#2:                                 # %.lr.ph.split.preheader
	addq	$8, %rbp
	addq	$20, %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %esi
	subl	-8(%rbp), %esi
	movl	%esi, (%rax)
	movl	12(%rbx), %edi
	subl	-8(%rbp), %edi
	movl	%edi, 12(%rax)
	movl	4(%rbx), %edi
	subl	-4(%rbp), %edi
	movl	%edi, 4(%rax)
	movl	16(%rbx), %edi
	subl	-4(%rbp), %edi
	movl	%edi, 16(%rax)
	movl	8(%rbx), %edi
	subl	(%rbp), %edi
	movl	%edi, 8(%rax)
	movl	20(%rbx), %edi
	subl	(%rbp), %edi
	movl	%edi, 20(%rax)
	movl	%esi, -20(%rcx)
	movl	4(%rax), %esi
	movl	%esi, -16(%rcx)
	movl	8(%rax), %esi
	movl	%esi, -12(%rcx)
	movl	12(%rax), %esi
	movl	%esi, -8(%rcx)
	movl	16(%rax), %esi
	movl	%esi, -4(%rcx)
	movl	20(%rax), %esi
	movl	%esi, (%rcx)
	incq	%rdx
	movslq	8(%r15), %rsi
	addq	$12, %rbp
	addq	$24, %rcx
	cmpq	%rsi, %rdx
	jl	.LBB0_3
	jmp	.LBB0_6
.LBB0_4:                                # %.lr.ph.split.us.preheader
	addq	$8, %rbp
	addq	$20, %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	-8(%rbp), %esi
	addl	(%rbx), %esi
	movl	%esi, (%rax)
	movl	-8(%rbp), %edi
	addl	12(%rbx), %edi
	movl	%edi, 12(%rax)
	movl	-4(%rbp), %edi
	addl	4(%rbx), %edi
	movl	%edi, 4(%rax)
	movl	-4(%rbp), %edi
	addl	16(%rbx), %edi
	movl	%edi, 16(%rax)
	movl	(%rbp), %edi
	addl	8(%rbx), %edi
	movl	%edi, 8(%rax)
	movl	(%rbp), %edi
	addl	20(%rbx), %edi
	movl	%edi, 20(%rax)
	movl	%esi, -20(%rcx)
	movl	4(%rax), %esi
	movl	%esi, -16(%rcx)
	movl	8(%rax), %esi
	movl	%esi, -12(%rcx)
	movl	12(%rax), %esi
	movl	%esi, -8(%rcx)
	movl	16(%rax), %esi
	movl	%esi, -4(%rcx)
	movl	20(%rax), %esi
	movl	%esi, (%rcx)
	incq	%rdx
	movslq	8(%r15), %rsi
	addq	$12, %rbp
	addq	$24, %rcx
	cmpq	%rsi, %rdx
	jl	.LBB0_5
.LBB0_6:                                # %._crit_edge
	movq	%rax, %rdi
	callq	hypre_BoxDestroy
	movq	%r14, %rdi
	callq	hypre_UnionBoxes
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_GrowBoxByStencil, .Lfunc_end0-hypre_GrowBoxByStencil
	.cfi_endproc

	.globl	hypre_GrowBoxArrayByStencil
	.p2align	4, 0x90
	.type	hypre_GrowBoxArrayByStencil,@function
hypre_GrowBoxArrayByStencil:            # @hypre_GrowBoxArrayByStencil
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 64
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	8(%r12), %edi
	callq	hypre_BoxArrayArrayCreate
	movq	%rax, %r13
	cmpl	$0, 8(%r12)
	jle	.LBB1_3
# BB#1:                                 # %.lr.ph
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	hypre_BoxArrayDestroy
	movq	(%r12), %rdi
	addq	%rbp, %rdi
	movq	%r15, %rsi
	movl	%r14d, %edx
	callq	hypre_GrowBoxByStencil
	movq	(%r13), %rcx
	movq	%rax, (%rcx,%rbx,8)
	incq	%rbx
	movslq	8(%r12), %rax
	addq	$24, %rbp
	cmpq	%rax, %rbx
	jl	.LBB1_2
.LBB1_3:                                # %._crit_edge
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	hypre_GrowBoxArrayByStencil, .Lfunc_end1-hypre_GrowBoxArrayByStencil
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
