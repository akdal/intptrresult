	.text
	.file	"zpath.bc"
	.globl	znewpath
	.p2align	4, 0x90
	.type	znewpath,@function
znewpath:                               # @znewpath
	.cfi_startproc
# BB#0:
	movq	igs(%rip), %rdi
	jmp	gs_newpath              # TAILCALL
.Lfunc_end0:
	.size	znewpath, .Lfunc_end0-znewpath
	.cfi_endproc

	.globl	zcurrentpoint
	.p2align	4, 0x90
	.type	zcurrentpoint,@function
zcurrentpoint:                          # @zcurrentpoint
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	igs(%rip), %rdi
	leaq	8(%rsp), %rsi
	callq	gs_currentpoint
	testl	%eax, %eax
	js	.LBB1_4
# BB#1:
	leaq	32(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB1_3
# BB#2:
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	jmp	.LBB1_4
.LBB1_3:
	movl	8(%rsp), %eax
	movl	%eax, 16(%rbx)
	movw	$44, 24(%rbx)
	movl	12(%rsp), %eax
	movl	%eax, 32(%rbx)
	movw	$44, 40(%rbx)
	xorl	%eax, %eax
.LBB1_4:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end1:
	.size	zcurrentpoint, .Lfunc_end1-zcurrentpoint
	.cfi_endproc

	.globl	zmoveto
	.p2align	4, 0x90
	.type	zmoveto,@function
zmoveto:                                # @zmoveto
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 16
	movq	%rsp, %rdx
	movl	$2, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB2_3
# BB#1:
	movq	igs(%rip), %rdi
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	callq	gs_moveto
	testl	%eax, %eax
	js	.LBB2_3
# BB#2:
	addq	$-32, osp(%rip)
	xorl	%eax, %eax
.LBB2_3:                                # %common_to.exit
	popq	%rcx
	retq
.Lfunc_end2:
	.size	zmoveto, .Lfunc_end2-zmoveto
	.cfi_endproc

	.globl	common_to
	.p2align	4, 0x90
	.type	common_to,@function
common_to:                              # @common_to
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 32
.Lcfi6:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	leaq	8(%rsp), %rdx
	movl	$2, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB3_3
# BB#1:
	movq	igs(%rip), %rdi
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	callq	*%rbx
	testl	%eax, %eax
	js	.LBB3_3
# BB#2:
	addq	$-32, osp(%rip)
	xorl	%eax, %eax
.LBB3_3:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end3:
	.size	common_to, .Lfunc_end3-common_to
	.cfi_endproc

	.globl	zrmoveto
	.p2align	4, 0x90
	.type	zrmoveto,@function
zrmoveto:                               # @zrmoveto
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 16
	movq	%rsp, %rdx
	movl	$2, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB4_3
# BB#1:
	movq	igs(%rip), %rdi
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	callq	gs_rmoveto
	testl	%eax, %eax
	js	.LBB4_3
# BB#2:
	addq	$-32, osp(%rip)
	xorl	%eax, %eax
.LBB4_3:                                # %common_to.exit
	popq	%rcx
	retq
.Lfunc_end4:
	.size	zrmoveto, .Lfunc_end4-zrmoveto
	.cfi_endproc

	.globl	zlineto
	.p2align	4, 0x90
	.type	zlineto,@function
zlineto:                                # @zlineto
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 16
	movq	%rsp, %rdx
	movl	$2, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB5_3
# BB#1:
	movq	igs(%rip), %rdi
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	callq	gs_lineto
	testl	%eax, %eax
	js	.LBB5_3
# BB#2:
	addq	$-32, osp(%rip)
	xorl	%eax, %eax
.LBB5_3:                                # %common_to.exit
	popq	%rcx
	retq
.Lfunc_end5:
	.size	zlineto, .Lfunc_end5-zlineto
	.cfi_endproc

	.globl	zrlineto
	.p2align	4, 0x90
	.type	zrlineto,@function
zrlineto:                               # @zrlineto
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 16
	movq	%rsp, %rdx
	movl	$2, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB6_3
# BB#1:
	movq	igs(%rip), %rdi
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	callq	gs_rlineto
	testl	%eax, %eax
	js	.LBB6_3
# BB#2:
	addq	$-32, osp(%rip)
	xorl	%eax, %eax
.LBB6_3:                                # %common_to.exit
	popq	%rcx
	retq
.Lfunc_end6:
	.size	zrlineto, .Lfunc_end6-zrlineto
	.cfi_endproc

	.globl	zarc
	.p2align	4, 0x90
	.type	zarc,@function
zarc:                                   # @zarc
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi10:
	.cfi_def_cfa_offset 32
	movq	%rsp, %rdx
	movl	$5, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB7_3
# BB#1:
	movq	igs(%rip), %rdi
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movss	12(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movss	16(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	callq	gs_arc
	testl	%eax, %eax
	js	.LBB7_3
# BB#2:
	addq	$-80, osp(%rip)
.LBB7_3:                                # %common_arc.exit
	addq	$24, %rsp
	retq
.Lfunc_end7:
	.size	zarc, .Lfunc_end7-zarc
	.cfi_endproc

	.globl	common_arc
	.p2align	4, 0x90
	.type	common_arc,@function
common_arc:                             # @common_arc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 48
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movq	%rsp, %rdx
	movl	$5, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB8_3
# BB#1:
	movq	igs(%rip), %rdi
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movss	12(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movss	16(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	callq	*%rbx
	testl	%eax, %eax
	js	.LBB8_3
# BB#2:
	addq	$-80, osp(%rip)
.LBB8_3:
	addq	$32, %rsp
	popq	%rbx
	retq
.Lfunc_end8:
	.size	common_arc, .Lfunc_end8-common_arc
	.cfi_endproc

	.globl	zarcn
	.p2align	4, 0x90
	.type	zarcn,@function
zarcn:                                  # @zarcn
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 32
	movq	%rsp, %rdx
	movl	$5, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB9_3
# BB#1:
	movq	igs(%rip), %rdi
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movss	12(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movss	16(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	callq	gs_arcn
	testl	%eax, %eax
	js	.LBB9_3
# BB#2:
	addq	$-80, osp(%rip)
.LBB9_3:                                # %common_arc.exit
	addq	$24, %rsp
	retq
.Lfunc_end9:
	.size	zarcn, .Lfunc_end9-zarcn
	.cfi_endproc

	.globl	zarct
	.p2align	4, 0x90
	.type	zarct,@function
zarct:                                  # @zarct
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 48
.Lcfi17:
	.cfi_offset %rbx, -16
	movq	%rsp, %rdx
	movl	$5, %esi
	callq	num_params
	movl	%eax, %ebx
	testl	%ebx, %ebx
	js	.LBB10_4
# BB#1:                                 # %common_arct.exit
	movq	igs(%rip), %rdi
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movss	12(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movss	16(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	callq	gs_arcto
	testl	%eax, %eax
	js	.LBB10_2
# BB#3:
	addq	$-80, osp(%rip)
	jmp	.LBB10_4
.LBB10_2:
	movl	%eax, %ebx
.LBB10_4:
	movl	%ebx, %eax
	addq	$32, %rsp
	popq	%rbx
	retq
.Lfunc_end10:
	.size	zarct, .Lfunc_end10-zarct
	.cfi_endproc

	.globl	common_arct
	.p2align	4, 0x90
	.type	common_arct,@function
common_arct:                            # @common_arct
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 48
.Lcfi20:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movq	%rsp, %rdx
	movl	$5, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB11_2
# BB#1:
	movq	igs(%rip), %rdi
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movss	12(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movss	16(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movq	%rbx, %rsi
	callq	gs_arcto
.LBB11_2:
	addq	$32, %rsp
	popq	%rbx
	retq
.Lfunc_end11:
	.size	common_arct, .Lfunc_end11-common_arct
	.cfi_endproc

	.globl	zarcto
	.p2align	4, 0x90
	.type	zarcto,@function
zarcto:                                 # @zarcto
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 16
	subq	$48, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 64
.Lcfi23:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	16(%rsp), %rdx
	movl	$5, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB12_3
# BB#1:                                 # %common_arct.exit
	movq	igs(%rip), %rdi
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	24(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movss	28(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movss	32(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movq	%rsp, %rsi
	callq	gs_arcto
	testl	%eax, %eax
	js	.LBB12_3
# BB#2:
	movl	(%rsp), %eax
	movl	%eax, -64(%rbx)
	movw	$44, -56(%rbx)
	movl	4(%rsp), %eax
	movl	%eax, -48(%rbx)
	movw	$44, -40(%rbx)
	movl	8(%rsp), %eax
	movl	%eax, -32(%rbx)
	movw	$44, -24(%rbx)
	movl	12(%rsp), %eax
	movl	%eax, -16(%rbx)
	movw	$44, -8(%rbx)
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB12_3:
	addq	$48, %rsp
	popq	%rbx
	retq
.Lfunc_end12:
	.size	zarcto, .Lfunc_end12-zarcto
	.cfi_endproc

	.globl	zcurveto
	.p2align	4, 0x90
	.type	zcurveto,@function
zcurveto:                               # @zcurveto
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 32
	movq	%rsp, %rdx
	movl	$6, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB13_3
# BB#1:
	movq	igs(%rip), %rdi
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movss	12(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movss	16(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movss	20(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	cvtss2sd	%xmm5, %xmm5
	callq	gs_curveto
	testl	%eax, %eax
	js	.LBB13_3
# BB#2:
	addq	$-96, osp(%rip)
.LBB13_3:                               # %common_curve.exit
	addq	$24, %rsp
	retq
.Lfunc_end13:
	.size	zcurveto, .Lfunc_end13-zcurveto
	.cfi_endproc

	.globl	common_curve
	.p2align	4, 0x90
	.type	common_curve,@function
common_curve:                           # @common_curve
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi26:
	.cfi_def_cfa_offset 48
.Lcfi27:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movq	%rsp, %rdx
	movl	$6, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB14_3
# BB#1:
	movq	igs(%rip), %rdi
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movss	12(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movss	16(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movss	20(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	cvtss2sd	%xmm5, %xmm5
	callq	*%rbx
	testl	%eax, %eax
	js	.LBB14_3
# BB#2:
	addq	$-96, osp(%rip)
.LBB14_3:
	addq	$32, %rsp
	popq	%rbx
	retq
.Lfunc_end14:
	.size	common_curve, .Lfunc_end14-common_curve
	.cfi_endproc

	.globl	zrcurveto
	.p2align	4, 0x90
	.type	zrcurveto,@function
zrcurveto:                              # @zrcurveto
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi28:
	.cfi_def_cfa_offset 32
	movq	%rsp, %rdx
	movl	$6, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB15_3
# BB#1:
	movq	igs(%rip), %rdi
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movss	12(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movss	16(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movss	20(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	cvtss2sd	%xmm5, %xmm5
	callq	gs_rcurveto
	testl	%eax, %eax
	js	.LBB15_3
# BB#2:
	addq	$-96, osp(%rip)
.LBB15_3:                               # %common_curve.exit
	addq	$24, %rsp
	retq
.Lfunc_end15:
	.size	zrcurveto, .Lfunc_end15-zrcurveto
	.cfi_endproc

	.globl	zclosepath
	.p2align	4, 0x90
	.type	zclosepath,@function
zclosepath:                             # @zclosepath
	.cfi_startproc
# BB#0:
	movq	igs(%rip), %rdi
	jmp	gs_closepath            # TAILCALL
.Lfunc_end16:
	.size	zclosepath, .Lfunc_end16-zclosepath
	.cfi_endproc

	.globl	zpath_op_init
	.p2align	4, 0x90
	.type	zpath_op_init,@function
zpath_op_init:                          # @zpath_op_init
	.cfi_startproc
# BB#0:
	movl	$zpath_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end17:
	.size	zpath_op_init, .Lfunc_end17-zpath_op_init
	.cfi_endproc

	.type	zpath_op_init.my_defs,@object # @zpath_op_init.my_defs
	.data
	.p2align	4
zpath_op_init.my_defs:
	.quad	.L.str
	.quad	zarc
	.quad	.L.str.1
	.quad	zarcn
	.quad	.L.str.2
	.quad	zarct
	.quad	.L.str.3
	.quad	zarcto
	.quad	.L.str.4
	.quad	zclosepath
	.quad	.L.str.5
	.quad	zcurrentpoint
	.quad	.L.str.6
	.quad	zcurveto
	.quad	.L.str.7
	.quad	zlineto
	.quad	.L.str.8
	.quad	zmoveto
	.quad	.L.str.9
	.quad	znewpath
	.quad	.L.str.10
	.quad	zrcurveto
	.quad	.L.str.11
	.quad	zrlineto
	.quad	.L.str.12
	.quad	zrmoveto
	.zero	16
	.size	zpath_op_init.my_defs, 224

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"5arc"
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"5arcn"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"5arct"
	.size	.L.str.2, 6

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"5arcto"
	.size	.L.str.3, 7

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"0closepath"
	.size	.L.str.4, 11

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"0currentpoint"
	.size	.L.str.5, 14

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"6curveto"
	.size	.L.str.6, 9

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"2lineto"
	.size	.L.str.7, 8

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"2moveto"
	.size	.L.str.8, 8

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"0newpath"
	.size	.L.str.9, 9

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"6rcurveto"
	.size	.L.str.10, 10

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"2rlineto"
	.size	.L.str.11, 9

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"2rmoveto"
	.size	.L.str.12, 9


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
