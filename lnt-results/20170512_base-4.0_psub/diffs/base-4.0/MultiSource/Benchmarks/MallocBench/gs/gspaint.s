	.text
	.file	"gspaint.bc"
	.globl	gs_erasepage
	.p2align	4, 0x90
	.type	gs_erasepage,@function
gs_erasepage:                           # @gs_erasepage
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movq	448(%rdi), %rax
	movq	(%rax), %rdi
	movq	16(%rax), %r9
	movq	8(%rdi), %rax
	movl	24(%rdi), %ecx
	movl	28(%rdi), %r8d
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	*56(%rax)
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end0:
	.size	gs_erasepage, .Lfunc_end0-gs_erasepage
	.cfi_endproc

	.globl	gs_fill
	.p2align	4, 0x90
	.type	gs_fill,@function
gs_fill:                                # @gs_fill
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 32
.Lcfi4:
	.cfi_offset %rbx, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	cmpb	$0, 437(%rbx)
	je	.LBB1_2
# BB#1:
	movq	(%rbx), %rax
	movq	256(%rbx), %rdi
	movq	256(%rax), %rsi
	callq	gx_path_merge
	jmp	.LBB1_3
.LBB1_2:
	movq	304(%rbx), %rdi
	movq	312(%rbx), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	gx_color_render
	movq	256(%rbx), %rdi
	movq	312(%rbx), %rsi
	movl	$-1, %ecx
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	gx_fill_path
.LBB1_3:
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB1_5
# BB#4:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	gs_newpath
.LBB1_5:                                # %gs_fill_trim.exit
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	gs_fill, .Lfunc_end1-gs_fill
	.cfi_endproc

	.globl	gs_fill_trim
	.p2align	4, 0x90
	.type	gs_fill_trim,@function
gs_fill_trim:                           # @gs_fill_trim
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpb	$0, 437(%rbx)
	je	.LBB2_2
# BB#1:
	movq	(%rbx), %rax
	movq	256(%rbx), %rdi
	movq	256(%rax), %rsi
	callq	gx_path_merge
	jmp	.LBB2_3
.LBB2_2:
	movq	304(%rbx), %rdi
	movq	312(%rbx), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	gx_color_render
	movq	256(%rbx), %rdi
	movq	312(%rbx), %rsi
	movl	$-1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %rdx
	movq	%r14, %r8
	callq	gx_fill_path
.LBB2_3:
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB2_5
# BB#4:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	gs_newpath
.LBB2_5:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	gs_fill_trim, .Lfunc_end2-gs_fill_trim
	.cfi_endproc

	.globl	gs_eofill
	.p2align	4, 0x90
	.type	gs_eofill,@function
gs_eofill:                              # @gs_eofill
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -24
.Lcfi16:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	304(%rbx), %rdi
	movq	312(%rbx), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	gx_color_render
	cmpb	$0, 437(%rbx)
	je	.LBB3_2
# BB#1:
	movq	(%rbx), %rax
	movq	256(%rbx), %rdi
	movq	256(%rax), %rsi
	callq	gx_path_merge
	jmp	.LBB3_3
.LBB3_2:
	movq	304(%rbx), %rdi
	movq	312(%rbx), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	gx_color_render
	movq	256(%rbx), %rdi
	movq	312(%rbx), %rsi
	movl	$1, %ecx
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	gx_fill_path
.LBB3_3:
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB3_5
# BB#4:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	gs_newpath
.LBB3_5:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end3:
	.size	gs_eofill, .Lfunc_end3-gs_eofill
	.cfi_endproc

	.globl	gs_stroke
	.p2align	4, 0x90
	.type	gs_stroke,@function
gs_stroke:                              # @gs_stroke
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 32
.Lcfi20:
	.cfi_offset %rbx, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	cmpb	$0, 437(%rbx)
	je	.LBB4_2
# BB#1:
	movq	(%rbx), %rax
	movq	256(%rbx), %rdi
	movq	256(%rax), %rsi
	callq	gx_path_merge
	jmp	.LBB4_3
.LBB4_2:
	movq	304(%rbx), %rdi
	movq	312(%rbx), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	gx_color_render
	movq	256(%rbx), %rdi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	gx_stroke_fill
.LBB4_3:
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB4_5
# BB#4:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	gs_newpath
.LBB4_5:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end4:
	.size	gs_stroke, .Lfunc_end4-gs_stroke
	.cfi_endproc

	.globl	gs_strokepath
	.p2align	4, 0x90
	.type	gs_strokepath,@function
gs_strokepath:                          # @gs_strokepath
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 32
	subq	$144, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 176
.Lcfi26:
	.cfi_offset %rbx, -32
.Lcfi27:
	.cfi_offset %r14, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	8(%rbx), %rsi
	movq	%rsp, %r14
	movq	%r14, %rdi
	callq	gx_path_init
	movq	256(%rbx), %rdi
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	gx_stroke_add
	testl	%eax, %eax
	js	.LBB5_1
# BB#2:
	movq	256(%rbx), %rdi
	callq	gx_path_release
	movq	256(%rbx), %rdi
	movq	%rsp, %rsi
	movl	$144, %edx
	callq	memcpy
	jmp	.LBB5_3
.LBB5_1:
	movl	%eax, %ebp
.LBB5_3:
	movl	%ebp, %eax
	addq	$144, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end5:
	.size	gs_strokepath, .Lfunc_end5-gs_strokepath
	.cfi_endproc

	.globl	gs_colorimage
	.p2align	4, 0x90
	.type	gs_colorimage,@function
gs_colorimage:                          # @gs_colorimage
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 48
	subq	$8384, %rsp             # imm = 0x20C0
.Lcfi34:
	.cfi_def_cfa_offset 8432
.Lcfi35:
	.cfi_offset %rbx, -48
.Lcfi36:
	.cfi_offset %r12, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebp
	movl	%ecx, %r14d
	movl	%edx, %r15d
	movl	%esi, %r12d
	movq	%rdi, %rax
	movq	%r9, (%rsp)
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	movl	%r12d, %edx
	movl	%r15d, %ecx
	movl	%r14d, %r8d
	movl	%ebp, %r9d
	callq	gs_image_init
	testl	%eax, %eax
	js	.LBB6_7
# BB#1:
	movq	8432(%rsp), %rbx
	imull	%r12d, %r14d
	testl	%ebp, %ebp
	jle	.LBB6_3
# BB#2:
	imull	%ebp, %r14d
	addl	$7, %r14d
	shrl	$3, %r14d
	imull	%r15d, %r14d
	leaq	8(%rsp), %rdi
	movq	%rbx, %rsi
	movl	%r14d, %edx
	callq	gs_image_next
	jmp	.LBB6_6
.LBB6_3:
	addl	$7, %r14d
	shrl	$3, %r14d
	imull	%r15d, %r14d
	leaq	8(%rsp), %r15
	.p2align	4, 0x90
.LBB6_4:                                # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movl	%r14d, %edx
	callq	gs_image_next
	testl	%eax, %eax
	js	.LBB6_7
# BB#5:                                 #   in Loop: Header=BB6_4 Depth=1
	addq	%r14, %rbx
	incl	%ebp
	jne	.LBB6_4
.LBB6_6:                                # %.loopexit
	movl	%eax, %ecx
	sarl	$31, %ecx
	andl	%ecx, %eax
.LBB6_7:                                # %.critedge
	addq	$8384, %rsp             # imm = 0x20C0
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	gs_colorimage, .Lfunc_end6-gs_colorimage
	.cfi_endproc

	.globl	gs_image
	.p2align	4, 0x90
	.type	gs_image,@function
gs_image:                               # @gs_image
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 40
	subq	$8392, %rsp             # imm = 0x20C8
.Lcfi44:
	.cfi_def_cfa_offset 8432
.Lcfi45:
	.cfi_offset %rbx, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movl	%ecx, %ebp
	movl	%edx, %r15d
	movl	%esi, %ebx
	movq	%rdi, %rax
	movq	%r8, (%rsp)
	leaq	16(%rsp), %rdi
	movl	$1, %r9d
	movq	%rax, %rsi
	movl	%ebx, %edx
	movl	%r15d, %ecx
	movl	%ebp, %r8d
	callq	gs_image_init
	testl	%eax, %eax
	js	.LBB7_2
# BB#1:
	imull	%ebx, %ebp
	addl	$7, %ebp
	shrl	$3, %ebp
	imull	%r15d, %ebp
	leaq	16(%rsp), %rdi
	movq	%r14, %rsi
	movl	%ebp, %edx
	callq	gs_image_next
	movl	%eax, %ecx
	sarl	$31, %eax
	andl	%ecx, %eax
.LBB7_2:                                # %gs_colorimage.exit
	addq	$8392, %rsp             # imm = 0x20C8
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	gs_image, .Lfunc_end7-gs_image
	.cfi_endproc

	.globl	gs_imagemask
	.p2align	4, 0x90
	.type	gs_imagemask,@function
gs_imagemask:                           # @gs_imagemask
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 32
	subq	$8384, %rsp             # imm = 0x20C0
.Lcfi52:
	.cfi_def_cfa_offset 8416
.Lcfi53:
	.cfi_offset %rbx, -32
.Lcfi54:
	.cfi_offset %r14, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r9
	movl	%ecx, %eax
	movl	%edx, %ebx
	movl	%esi, %ebp
	movq	%rdi, %rcx
	leaq	8(%rsp), %rdi
	movq	%rcx, %rsi
	movl	%ebp, %edx
	movl	%ebx, %ecx
	movl	%eax, %r8d
	callq	gs_imagemask_init
	testl	%eax, %eax
	js	.LBB8_2
# BB#1:
	addl	$7, %ebp
	shrl	$3, %ebp
	imull	%ebx, %ebp
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	movl	%ebp, %edx
	callq	gs_image_next
	movl	%eax, %ecx
	sarl	$31, %eax
	andl	%ecx, %eax
.LBB8_2:
	addq	$8384, %rsp             # imm = 0x20C0
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end8:
	.size	gs_imagemask, .Lfunc_end8-gs_imagemask
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
