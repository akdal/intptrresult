	.text
	.file	"huffbench.bc"
	.globl	generate_test_data
	.p2align	4, 0x90
	.type	generate_test_data,@function
generate_test_data:                     # @generate_test_data
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	malloc
	movq	%rax, %r8
	testq	%rbx, %rbx
	je	.LBB0_4
# BB#1:                                 # %.lr.ph
	movq	seed(%rip), %rsi
	movabsq	$4730756183288445817, %rdi # imm = 0x41A705AF1FE3FB79
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	xorq	$123459876, %rsi        # imm = 0x75BD924
	movq	%rsi, %rax
	imulq	%rdi
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$15, %rdx
	addq	%rax, %rdx
	imulq	$-127773, %rdx, %rax    # imm = 0xFFFE0CE3
	addq	%rsi, %rax
	imulq	$16807, %rax, %rax      # imm = 0x41A7
	imulq	$-2836, %rdx, %rdx      # imm = 0xF4EC
	leaq	2147483647(%rax,%rdx), %rsi
	addq	%rdx, %rax
	cmovnsq	%rax, %rsi
	movq	%rsi, %rax
	sarq	$63, %rax
	shrq	$59, %rax
	addq	%rsi, %rax
	andq	$-32, %rax
	negq	%rax
	movzbl	.L.str(%rsi,%rax), %eax
	xorq	$123459876, %rsi        # imm = 0x75BD924
	movb	%al, (%rcx)
	incq	%rcx
	decq	%rbx
	jne	.LBB0_2
# BB#3:                                 # %._crit_edge
	movq	%rsi, seed(%rip)
.LBB0_4:
	movq	%r8, %rax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	generate_test_data, .Lfunc_end0-generate_test_data
	.cfi_endproc

	.globl	compdecomp
	.p2align	4, 0x90
	.type	compdecomp,@function
compdecomp:                             # @compdecomp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$12824, %rsp            # imm = 0x3218
.Lcfi8:
	.cfi_def_cfa_offset 12880
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	leaq	1(%r13), %r12
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, %r15
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r12, %rdx
	callq	memset
	leaq	2576(%rsp), %rdi
	xorl	%esi, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	memset
	leaq	528(%rsp), %rdi
	xorl	%esi, %esi
	movl	$2048, %edx             # imm = 0x800
	callq	memset
	leaq	8720(%rsp), %rdi
	xorl	%esi, %esi
	movl	$2048, %edx             # imm = 0x800
	callq	memset
	leaq	10768(%rsp), %rdi
	xorl	%esi, %esi
	movl	$2048, %edx             # imm = 0x800
	callq	memset
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 256(%rsp)
	movaps	%xmm0, 240(%rsp)
	movaps	%xmm0, 224(%rsp)
	movaps	%xmm0, 208(%rsp)
	movaps	%xmm0, 192(%rsp)
	movaps	%xmm0, 176(%rsp)
	movaps	%xmm0, 160(%rsp)
	movaps	%xmm0, 144(%rsp)
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm0, 96(%rsp)
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	testq	%r13, %r13
	je	.LBB1_8
# BB#1:                                 # %.lr.ph324.preheader
	leaq	-1(%r13), %rcx
	movq	%r13, %rax
	xorl	%edx, %edx
	andq	$3, %rax
	je	.LBB1_2
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph324.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%rdx), %esi
	incq	2576(%rsp,%rsi,8)
	incq	%rdx
	cmpq	%rdx, %rax
	jne	.LBB1_3
# BB#4:                                 # %.lr.ph324.prol.loopexit.unr-lcssa
	leaq	(%r14,%rdx), %rax
	cmpq	$3, %rcx
	jae	.LBB1_6
	jmp	.LBB1_8
.LBB1_2:
	movq	%r14, %rax
	cmpq	$3, %rcx
	jb	.LBB1_8
.LBB1_6:                                # %.lr.ph324.preheader.new
	movq	%r13, %rcx
	subq	%rdx, %rcx
	.p2align	4, 0x90
.LBB1_7:                                # %.lr.ph324
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	incq	2576(%rsp,%rdx,8)
	movzbl	1(%rax), %edx
	incq	2576(%rsp,%rdx,8)
	movzbl	2(%rax), %edx
	incq	2576(%rsp,%rdx,8)
	movzbl	3(%rax), %edx
	incq	2576(%rsp,%rdx,8)
	addq	$4, %rax
	addq	$-4, %rcx
	jne	.LBB1_7
.LBB1_8:                                # %.preheader247.preheader
	xorl	%ecx, %ecx
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB1_9:                                # %.preheader247
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$0, 2576(%rsp,%rcx,8)
	je	.LBB1_11
# BB#10:                                #   in Loop: Header=BB1_9 Depth=1
	movq	%rcx, 528(%rsp,%r11,8)
	incq	%r11
.LBB1_11:                               # %.preheader247.1354
                                        #   in Loop: Header=BB1_9 Depth=1
	cmpq	$0, 2584(%rsp,%rcx,8)
	je	.LBB1_13
# BB#12:                                #   in Loop: Header=BB1_9 Depth=1
	leaq	1(%rcx), %rax
	movq	%rax, 528(%rsp,%r11,8)
	incq	%r11
.LBB1_13:                               #   in Loop: Header=BB1_9 Depth=1
	addq	$2, %rcx
	cmpq	$256, %rcx              # imm = 0x100
	jne	.LBB1_9
# BB#14:                                # %.preheader246
	testq	%r11, %r11
	je	.LBB1_15
# BB#24:                                # %.lr.ph318
	movl	%r11d, %r10d
	shrl	$31, %r10d
	addl	%r11d, %r10d
	sarl	%r10d
	movq	%r11, %r9
	.p2align	4, 0x90
.LBB1_25:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_28 Depth 2
	movslq	%r9d, %rax
	movslq	520(%rsp,%rax,8), %r8
	cmpl	%eax, %r10d
	jge	.LBB1_27
# BB#26:                                #   in Loop: Header=BB1_25 Depth=1
	movl	%r9d, %esi
	jmp	.LBB1_33
	.p2align	4, 0x90
.LBB1_27:                               # %.lr.ph.i
                                        #   in Loop: Header=BB1_25 Depth=1
	movq	2576(%rsp,%r8,8), %rdi
	movl	%r9d, %edx
	.p2align	4, 0x90
.LBB1_28:                               #   Parent Loop BB1_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rdx,%rdx), %esi
	cmpl	%r11d, %esi
	jge	.LBB1_30
# BB#29:                                #   in Loop: Header=BB1_28 Depth=2
	movslq	%esi, %rax
	movq	520(%rsp,%rax,8), %rax
	movq	2576(%rsp,%rax,8), %rax
	movl	%esi, %ebx
	orl	$1, %ebx
	movslq	%ebx, %rbx
	movq	520(%rsp,%rbx,8), %rcx
	cmpq	2576(%rsp,%rcx,8), %rax
	cmovbel	%esi, %ebx
	movl	%ebx, %esi
.LBB1_30:                               #   in Loop: Header=BB1_28 Depth=2
	movslq	%esi, %rax
	movq	520(%rsp,%rax,8), %rax
	cmpq	2576(%rsp,%rax,8), %rdi
	jb	.LBB1_31
# BB#32:                                #   in Loop: Header=BB1_28 Depth=2
	movslq	%edx, %rcx
	movq	%rax, 520(%rsp,%rcx,8)
	cmpl	%r10d, %esi
	movl	%esi, %edx
	jle	.LBB1_28
	jmp	.LBB1_33
	.p2align	4, 0x90
.LBB1_31:                               #   in Loop: Header=BB1_25 Depth=1
	movl	%edx, %esi
.LBB1_33:                               # %heap_adjust.exit
                                        #   in Loop: Header=BB1_25 Depth=1
	movslq	%esi, %rax
	movq	%r8, 520(%rsp,%rax,8)
	decq	%r9
	jne	.LBB1_25
# BB#16:                                # %.preheader245
	cmpq	$2, %r11
	jb	.LBB1_44
# BB#17:
	movq	%r14, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB1_18:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_20 Depth 2
                                        #     Child Loop BB1_37 Depth 2
	movq	%r11, %r10
	leaq	-1(%r10), %r11
	movq	528(%rsp), %r8
	movq	520(%rsp,%r10,8), %rax
	movq	%rax, 528(%rsp)
	movl	%r11d, %ecx
	shrl	$31, %ecx
	addl	%r11d, %ecx
	sarl	%ecx
	cmpl	$2, %r11d
	movslq	%eax, %r9
	movl	$1, %r14d
	movl	$1, %ebx
	jl	.LBB1_35
# BB#19:                                # %.lr.ph.i233
                                        #   in Loop: Header=BB1_18 Depth=1
	movq	2576(%rsp,%r9,8), %r12
	movl	$1, %esi
	.p2align	4, 0x90
.LBB1_20:                               #   Parent Loop BB1_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rsi,%rsi), %ebx
	cmpl	%r11d, %ebx
	jge	.LBB1_22
# BB#21:                                #   in Loop: Header=BB1_20 Depth=2
	movslq	%ebx, %rax
	movq	520(%rsp,%rax,8), %rax
	movq	2576(%rsp,%rax,8), %rax
	movl	%ebx, %ebp
	orl	$1, %ebp
	movslq	%ebp, %rbp
	movq	520(%rsp,%rbp,8), %rdi
	cmpq	2576(%rsp,%rdi,8), %rax
	cmovbel	%ebx, %ebp
	movl	%ebp, %ebx
.LBB1_22:                               #   in Loop: Header=BB1_20 Depth=2
	movslq	%ebx, %rax
	movq	520(%rsp,%rax,8), %rax
	cmpq	2576(%rsp,%rax,8), %r12
	jb	.LBB1_23
# BB#34:                                #   in Loop: Header=BB1_20 Depth=2
	movslq	%esi, %rsi
	movq	%rax, 520(%rsp,%rsi,8)
	cmpl	%ecx, %ebx
	movl	%ebx, %esi
	jle	.LBB1_20
	jmp	.LBB1_35
	.p2align	4, 0x90
.LBB1_23:                               #   in Loop: Header=BB1_18 Depth=1
	movl	%esi, %ebx
.LBB1_35:                               # %heap_adjust.exit238
                                        #   in Loop: Header=BB1_18 Depth=1
	movslq	%ebx, %rax
	movq	%r9, 520(%rsp,%rax,8)
	movq	528(%rsp), %rax
	movq	2576(%rsp,%r8,8), %rsi
	addq	2576(%rsp,%rax,8), %rsi
	movl	$-255, %edi
	subl	%r10d, %edi
	cmpl	$2, %r11d
	movq	%rsi, 4616(%rsp,%r10,8)
	leaq	255(%r10), %rsi
	movl	%esi, 8720(%rsp,%r8,4)
	movl	%edi, 8720(%rsp,%rax,4)
	movq	%rsi, 528(%rsp)
	movslq	%esi, %rsi
	jl	.LBB1_42
# BB#36:                                # %.lr.ph.i225
                                        #   in Loop: Header=BB1_18 Depth=1
	movq	2576(%rsp,%rsi,8), %rdi
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB1_37:                               #   Parent Loop BB1_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rbx,%rbx), %r14d
	cmpl	%r11d, %r14d
	jge	.LBB1_39
# BB#38:                                #   in Loop: Header=BB1_37 Depth=2
	movslq	%r14d, %rax
	movq	520(%rsp,%rax,8), %rax
	movq	2576(%rsp,%rax,8), %rax
	movl	%r14d, %edx
	orl	$1, %edx
	movslq	%edx, %rdx
	movq	520(%rsp,%rdx,8), %rbp
	cmpq	2576(%rsp,%rbp,8), %rax
	cmovbel	%r14d, %edx
	movl	%edx, %r14d
.LBB1_39:                               #   in Loop: Header=BB1_37 Depth=2
	movslq	%r14d, %rax
	movq	520(%rsp,%rax,8), %rax
	cmpq	2576(%rsp,%rax,8), %rdi
	jb	.LBB1_40
# BB#41:                                #   in Loop: Header=BB1_37 Depth=2
	movslq	%ebx, %rdx
	movq	%rax, 520(%rsp,%rdx,8)
	cmpl	%ecx, %r14d
	movl	%r14d, %ebx
	jle	.LBB1_37
	jmp	.LBB1_42
	.p2align	4, 0x90
.LBB1_40:                               #   in Loop: Header=BB1_18 Depth=1
	movl	%ebx, %r14d
.LBB1_42:                               # %heap_adjust.exit230
                                        #   in Loop: Header=BB1_18 Depth=1
	movslq	%r14d, %rax
	movq	%rsi, 520(%rsp,%rax,8)
	cmpq	$1, %r11
	ja	.LBB1_18
# BB#43:
	movl	$1, %r11d
	movq	8(%rsp), %r14           # 8-byte Reload
	jmp	.LBB1_44
.LBB1_15:
	xorl	%r11d, %r11d
.LBB1_44:                               # %._crit_edge315
	movl	$0, 9744(%rsp,%r11,4)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_45:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_50 Depth 2
	cmpq	$0, 2576(%rsp,%rdx,8)
	je	.LBB1_48
# BB#46:                                # %.preheader244
                                        #   in Loop: Header=BB1_45 Depth=1
	movl	8720(%rsp,%rdx,4), %ebp
	testl	%ebp, %ebp
	je	.LBB1_47
# BB#49:                                # %.lr.ph306.preheader
                                        #   in Loop: Header=BB1_45 Depth=1
	xorl	%edi, %edi
	xorl	%esi, %esi
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB1_50:                               # %.lr.ph306
                                        #   Parent Loop BB1_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %eax
	negl	%eax
	cmovll	%ebp, %eax
	testl	%ebp, %ebp
	movl	$0, %ebp
	cmovsq	%rbx, %rbp
	addq	%rbp, %rdi
	cltq
	addq	%rbx, %rbx
	incq	%rsi
	movl	8720(%rsp,%rax,4), %ebp
	testl	%ebp, %ebp
	jne	.LBB1_50
	jmp	.LBB1_51
	.p2align	4, 0x90
.LBB1_48:                               #   in Loop: Header=BB1_45 Depth=1
	movq	$0, 10768(%rsp,%rdx,8)
	movb	$0, 16(%rsp,%rdx)
	jmp	.LBB1_52
	.p2align	4, 0x90
.LBB1_47:                               #   in Loop: Header=BB1_45 Depth=1
	xorl	%esi, %esi
	xorl	%edi, %edi
.LBB1_51:                               # %._crit_edge307
                                        #   in Loop: Header=BB1_45 Depth=1
	movq	%rdi, 10768(%rsp,%rdx,8)
	movb	%sil, 16(%rsp,%rdx)
	cmpq	%rcx, %rdi
	cmovaq	%rdi, %rcx
	cmpq	%r8, %rsi
	cmovaq	%rsi, %r8
.LBB1_52:                               #   in Loop: Header=BB1_45 Depth=1
	incq	%rdx
	cmpq	$256, %rdx              # imm = 0x100
	jne	.LBB1_45
# BB#53:
	cmpq	$65, %r8
	jae	.LBB1_54
# BB#56:
	testq	%rcx, %rcx
	je	.LBB1_59
# BB#57:                                # %.preheader243
	movl	$-1, %edx
	testq	%r13, %r13
	je	.LBB1_58
# BB#60:                                # %.lr.ph295.preheader
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	movq	%r14, %r9
	.p2align	4, 0x90
.LBB1_61:                               # %.lr.ph295
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_63 Depth 2
	movzbl	(%r9), %r10d
	movzbl	16(%rsp,%r10), %ecx
	testl	%ecx, %ecx
	je	.LBB1_69
# BB#62:                                # %.lr.ph285.preheader
                                        #   in Loop: Header=BB1_61 Depth=1
	decl	%ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	movslq	%esi, %rcx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_63:                               # %.lr.ph285
                                        #   Parent Loop BB1_61 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$7, %edx
	jne	.LBB1_67
# BB#64:                                #   in Loop: Header=BB1_63 Depth=2
	movb	%bl, (%r15,%rax)
	incq	%rax
	cmpq	%r13, %rax
	je	.LBB1_66
# BB#65:                                # %._crit_edge
                                        #   in Loop: Header=BB1_63 Depth=2
	movb	(%r9), %r10b
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.LBB1_68
	.p2align	4, 0x90
.LBB1_67:                               #   in Loop: Header=BB1_63 Depth=2
	incl	%edx
	addb	%bl, %bl
	movb	%bl, %sil
.LBB1_68:                               #   in Loop: Header=BB1_63 Depth=2
	movzbl	%r10b, %ebp
	testq	10768(%rsp,%rbp,8), %rcx
	setne	%bl
	orb	%sil, %bl
	shrq	%rcx
	incq	%rdi
	movzbl	16(%rsp,%rbp), %esi
	cmpq	%rsi, %rdi
	jb	.LBB1_63
.LBB1_69:                               # %._crit_edge286
                                        #   in Loop: Header=BB1_61 Depth=1
	incq	%r9
	incq	%r8
	cmpq	%r13, %r8
	jb	.LBB1_61
	jmp	.LBB1_70
.LBB1_58:
	xorl	%eax, %eax
	xorl	%ebx, %ebx
.LBB1_70:                               # %._crit_edge296
	movl	$7, %ecx
	subl	%edx, %ecx
	movsbl	%bl, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	movb	%dl, (%r15,%rax)
	leaq	272(%rsp), %r12
	leaq	6672(%rsp), %rdi
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movl	$2048, %edx             # imm = 0x800
	callq	memset
	.p2align	4, 0x90
.LBB1_71:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_78 Depth 2
	movb	%bl, (%r12)
	movq	10768(%rsp,%rbx,8), %rbp
	movzbl	16(%rsp,%rbx), %edx
	movq	%rdx, %rcx
	orq	%rbp, %rcx
	je	.LBB1_80
# BB#72:                                #   in Loop: Header=BB1_71 Depth=1
	testb	%dl, %dl
	je	.LBB1_73
# BB#74:                                # %.lr.ph274
                                        #   in Loop: Header=BB1_71 Depth=1
	leal	-1(%rdx), %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	movslq	%esi, %rcx
	testb	$1, %dl
	jne	.LBB1_76
# BB#75:                                #   in Loop: Header=BB1_71 Depth=1
	xorl	%esi, %esi
	xorl	%edi, %edi
	cmpb	$1, %dl
	jne	.LBB1_78
	jmp	.LBB1_79
	.p2align	4, 0x90
.LBB1_73:                               #   in Loop: Header=BB1_71 Depth=1
	xorl	%edi, %edi
	jmp	.LBB1_79
.LBB1_76:                               #   in Loop: Header=BB1_71 Depth=1
	movq	%rbp, %rsi
	andq	%rcx, %rsi
	cmpq	$1, %rsi
	movl	$1, %edi
	sbbq	$-1, %rdi
	shrq	%rcx
	movl	$1, %esi
	cmpb	$1, %dl
	je	.LBB1_79
	.p2align	4, 0x90
.LBB1_78:                               #   Parent Loop BB1_71 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%rdi,%rdi), %rdi
	movq	%rbp, %rax
	andq	%rcx, %rax
	cmpq	$1, %rax
	sbbq	$-1, %rdi
	movq	%rcx, %rax
	shrq	%rax
	leaq	1(%rdi,%rdi), %rdi
	andq	%rbp, %rax
	cmpq	$1, %rax
	sbbq	$-1, %rdi
	shrq	$2, %rcx
	addq	$2, %rsi
	cmpq	%rdx, %rsi
	jb	.LBB1_78
.LBB1_79:                               # %._crit_edge275
                                        #   in Loop: Header=BB1_71 Depth=1
	movq	%rdi, 6672(%rsp,%rbx,8)
.LBB1_80:                               #   in Loop: Header=BB1_71 Depth=1
	incq	%r12
	incq	%rbx
	cmpq	$256, %rbx              # imm = 0x100
	jne	.LBB1_71
# BB#81:                                # %.lr.ph266.preheader.preheader
	movl	$1, %eax
	.p2align	4, 0x90
.LBB1_82:                               # %.lr.ph266.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_83 Depth 2
	movq	6672(%rsp,%rax,8), %rcx
	movb	272(%rsp,%rax), %dl
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB1_83:                               # %.lr.ph266
                                        #   Parent Loop BB1_82 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	6664(%rsp,%rsi,8), %rdi
	cmpq	%rcx, %rdi
	jbe	.LBB1_86
# BB#84:                                #   in Loop: Header=BB1_83 Depth=2
	movq	%rdi, 6672(%rsp,%rsi,8)
	movzbl	271(%rsp,%rsi), %ebx
	movb	%bl, 272(%rsp,%rsi)
	decq	%rsi
	jne	.LBB1_83
# BB#85:                                #   in Loop: Header=BB1_82 Depth=1
	xorl	%esi, %esi
.LBB1_86:                               # %.critedge
                                        #   in Loop: Header=BB1_82 Depth=1
	movq	%rcx, 6672(%rsp,%rsi,8)
	movb	%dl, 272(%rsp,%rsi)
	incq	%rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB1_82
# BB#87:                                # %.preheader241.preheader
	movq	$-1, %rax
	.p2align	4, 0x90
.LBB1_88:                               # %.preheader241
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$0, 6680(%rsp,%rax,8)
	leaq	1(%rax), %rax
	je	.LBB1_88
# BB#89:                                # %.preheader
	testq	%r13, %r13
	je	.LBB1_99
# BB#90:                                # %.lr.ph.preheader
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	movq	%r15, %r8
	xorl	%edi, %edi
	movl	$128, %ebx
	jmp	.LBB1_92
.LBB1_98:                               # %.lr.ph
                                        #   in Loop: Header=BB1_92 Depth=1
	movl	$128, %ebx
	jmp	.LBB1_92
	.p2align	4, 0x90
.LBB1_91:                               #   in Loop: Header=BB1_92 Depth=1
	shrq	%rbx
	cmpq	%r13, %rcx
	jae	.LBB1_99
.LBB1_92:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_93 Depth 2
	leaq	1(%rdi,%rdi), %rdi
	movzbl	(%r8), %edx
	andq	%rbx, %rdx
	cmpq	$1, %rdx
	sbbq	$-1, %rdi
	decq	%rsi
	.p2align	4, 0x90
.LBB1_93:                               #   Parent Loop BB1_92 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	6680(%rsp,%rsi,8), %rdx
	incq	%rsi
	cmpq	%rdi, %rdx
	jb	.LBB1_93
# BB#94:                                #   in Loop: Header=BB1_92 Depth=1
	cmpq	%rdx, %rdi
	jne	.LBB1_96
# BB#95:                                #   in Loop: Header=BB1_92 Depth=1
	movb	272(%rsp,%rsi), %dl
	movb	%dl, (%r14)
	incq	%r14
	incq	%rcx
	xorl	%edi, %edi
	movq	%rax, %rsi
.LBB1_96:                               #   in Loop: Header=BB1_92 Depth=1
	cmpq	$1, %rbx
	ja	.LBB1_91
# BB#97:                                # %.outer
                                        #   in Loop: Header=BB1_92 Depth=1
	incq	%r8
	cmpq	%r13, %rcx
	jb	.LBB1_98
.LBB1_99:                               # %.outer._crit_edge
	movq	%r15, %rdi
	callq	free
	addq	$12824, %rsp            # imm = 0x3218
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_66:
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$22, %esi
.LBB1_55:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB1_54:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$25, %esi
	jmp	.LBB1_55
.LBB1_59:
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$32, %esi
	jmp	.LBB1_55
.Lfunc_end1:
	.size	compdecomp, .Lfunc_end1-compdecomp
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	cmpl	$2, %edi
	jl	.LBB2_8
# BB#1:                                 # %.lr.ph
	movq	8(%rsi), %rax
	movb	(%rax), %cl
	movl	$1, %edx
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	cmpb	$45, %cl
	jne	.LBB2_6
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	cmpb	$103, 1(%rax)
	jne	.LBB2_6
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	cmpb	$97, 2(%rax)
	jne	.LBB2_6
# BB#5:                                 #   in Loop: Header=BB2_2 Depth=1
	cmpb	$0, 3(%rax)
	je	.LBB2_15
	.p2align	4, 0x90
.LBB2_6:                                # %.thread
                                        #   in Loop: Header=BB2_2 Depth=1
	incl	%edx
	cmpl	%edi, %edx
	jl	.LBB2_2
.LBB2_8:
	xorl	%ebp, %ebp
.LBB2_9:                                # %.loopexit
	movl	$10000000, %edi         # imm = 0x989680
	callq	malloc
	movq	%rax, %rbx
	xorl	%esi, %esi
	movq	seed(%rip), %rcx
	movabsq	$4730756183288445817, %rdi # imm = 0x41A705AF1FE3FB79
	.p2align	4, 0x90
.LBB2_10:                               # =>This Inner Loop Header: Depth=1
	xorq	$123459876, %rcx        # imm = 0x75BD924
	movq	%rcx, %rax
	imulq	%rdi
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$15, %rdx
	addq	%rax, %rdx
	imulq	$-127773, %rdx, %rax    # imm = 0xFFFE0CE3
	addq	%rcx, %rax
	imulq	$16807, %rax, %rax      # imm = 0x41A7
	imulq	$-2836, %rdx, %rdx      # imm = 0xF4EC
	leaq	2147483647(%rax,%rdx), %rcx
	addq	%rdx, %rax
	cmovnsq	%rax, %rcx
	movq	%rcx, %rax
	sarq	$63, %rax
	shrq	$59, %rax
	addq	%rcx, %rax
	andq	$-32, %rax
	negq	%rax
	movzbl	.L.str(%rcx,%rax), %eax
	xorq	$123459876, %rcx        # imm = 0x75BD924
	movb	%al, (%rbx,%rsi)
	incq	%rsi
	cmpq	$10000000, %rsi         # imm = 0x989680
	jne	.LBB2_10
# BB#11:                                # %generate_test_data.exit
	movq	%rcx, seed(%rip)
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movl	$10000000, %esi         # imm = 0x989680
	movq	%rbx, %rdi
	callq	compdecomp
	movq	%rbx, %rdi
	callq	free
	movq	stdout(%rip), %rdi
	testb	%bpl, %bpl
	je	.LBB2_13
# BB#12:
	movl	$.L.str.5, %esi
	jmp	.LBB2_14
.LBB2_13:
	movl	$.L.str.6, %esi
.LBB2_14:
	xorps	%xmm0, %xmm0
	movb	$1, %al
	callq	fprintf
	movq	stdout(%rip), %rdi
	callq	fflush
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB2_15:
	movb	$1, %bpl
	jmp	.LBB2_9
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"ABCDEFGHIJKLMNOPQRSTUVWXYZ012345"
	.size	.L.str, 33

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"error: bit code overflow\n"
	.size	.L.str.1, 26

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"error: file has only one value!\n"
	.size	.L.str.2, 33

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"error: no compression\n"
	.size	.L.str.3, 23

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"-ga"
	.size	.L.str.4, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%f"
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\nhuffbench (Std. C) run time: %f\n\n"
	.size	.L.str.6, 35

	.type	seed,@object            # @seed
	.data
	.p2align	3
seed:
	.quad	1325                    # 0x52d
	.size	seed, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
