	.text
	.file	"libclamav_mspack.bc"
	.globl	mszip_init
	.p2align	4, 0x90
	.type	mszip_init,@function
mszip_init:                             # @mszip_init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movl	%ecx, %r12d
	movl	%edx, %ebp
	movl	%esi, %r13d
	movl	%edi, %r14d
	incl	%ebp
	andl	$-2, %ebp
	je	.LBB0_7
# BB#1:
	movq	%r9, (%rsp)             # 8-byte Spill
	movl	$35760, %edi            # imm = 0x8BB0
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_7
# BB#2:
	movslq	%ebp, %rdi
	callq	cli_malloc
	movq	%rax, 40(%rbx)
	testq	%rax, %rax
	je	.LBB0_6
# BB#3:
	movl	%r14d, (%rbx)
	movl	%r13d, 4(%rbx)
	movb	$1, 8(%rbx)
	movl	%ebp, 88(%rbx)
	movl	$0, 24(%rbx)
	movl	%r12d, 28(%rbx)
	movq	$mszip_flush_window, 16(%rbx)
	movl	$0, 36(%rbx)
	movq	%rax, 56(%rbx)
	movq	%rax, 48(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	$0, 80(%rbx)
	movq	%r15, 35744(%rbx)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, 35752(%rbx)
	jmp	.LBB0_8
.LBB0_6:
	movq	%rbx, %rdi
	callq	free
.LBB0_7:
	xorl	%ebx, %ebx
.LBB0_8:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	mszip_init, .Lfunc_end0-mszip_init
	.cfi_endproc

	.p2align	4, 0x90
	.type	mszip_flush_window,@function
mszip_flush_window:                     # @mszip_flush_window
	.cfi_startproc
# BB#0:
	movl	32(%rdi), %edx
	addl	%esi, %edx
	movl	%edx, 32(%rdi)
	xorl	%eax, %eax
	cmpl	$32769, %edx            # imm = 0x8001
	jl	.LBB1_2
# BB#1:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, %eax
	addq	$8, %rsp
.LBB1_2:
	retq
.Lfunc_end1:
	.size	mszip_flush_window, .Lfunc_end1-mszip_flush_window
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.zero	16,8
.LCPI2_1:
	.zero	16,9
.LCPI2_2:
	.zero	16,7
.LCPI2_3:
	.zero	16,5
	.text
	.globl	mszip_decompress
	.p2align	4, 0x90
	.type	mszip_decompress,@function
mszip_decompress:                       # @mszip_decompress
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	subq	$760, %rsp              # imm = 0x2F8
.Lcfi20:
	.cfi_def_cfa_offset 816
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%r14, %r14
	movl	$-111, %eax
	je	.LBB2_348
# BB#1:
	testq	%rbx, %rbx
	js	.LBB2_348
# BB#2:
	movl	24(%r14), %eax
	testl	%eax, %eax
	jne	.LBB2_348
# BB#3:
	movq	64(%r14), %rsi
	movq	72(%r14), %rbp
	subq	%rsi, %rbp
	movslq	%ebp, %rax
	cmpq	%rbx, %rax
	cmovgq	%rbx, %rbp
	testl	%ebp, %ebp
	je	.LBB2_8
# BB#4:
	cmpb	$0, 8(%r14)
	je	.LBB2_7
# BB#5:
	movl	4(%r14), %edi
	movl	%ebp, %edx
	callq	cli_writen
	cmpl	%ebp, %eax
	jne	.LBB2_344
# BB#6:                                 # %._crit_edge511
	movq	64(%r14), %rsi
.LBB2_7:
	movslq	%ebp, %rax
	addq	%rax, %rsi
	movq	%rsi, 64(%r14)
	subq	%rax, %rbx
.LBB2_8:
	testq	%rbx, %rbx
	je	.LBB2_11
# BB#9:                                 # %.preheader
	jle	.LBB2_346
# BB#10:                                # %.lr.ph327
	leaq	92(%r14), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	412(%r14), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	leaq	380(%r14), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	2716(%r14), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	leaq	236(%r14), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	leaq	348(%r14), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	leaq	2972(%r14), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leaq	2973(%r14), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	2988(%r14), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	leaq	3084(%r14), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	jmp	.LBB2_33
.LBB2_11:
	xorl	%eax, %eax
	jmp	.LBB2_348
.LBB2_12:                               #   in Loop: Header=BB2_33 Depth=1
	movl	$32768, %esi            # imm = 0x8000
	subl	32(%r14), %esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movslq	32(%r14), %rax
	cmpq	$32767, %rax            # imm = 0x7FFF
	movq	120(%rsp), %rbx         # 8-byte Reload
	jg	.LBB2_14
# BB#13:                                # %.lr.ph324.preheader
                                        #   in Loop: Header=BB2_33 Depth=1
	leaq	2972(%r14,%rax), %rdi
	movl	$32767, %edx            # imm = 0x7FFF
	subl	%eax, %edx
	incq	%rdx
	xorl	%esi, %esi
	callq	memset
.LBB2_14:                               # %._crit_edge325
                                        #   in Loop: Header=BB2_33 Depth=1
	movl	$32768, 32(%r14)        # imm = 0x8000
	movl	$32768, %eax            # imm = 0x8000
.LBB2_15:                               #   in Loop: Header=BB2_33 Depth=1
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, 64(%r14)
	movslq	%eax, %rbp
	leaq	2972(%r14,%rbp), %rax
	movq	%rax, 72(%r14)
	cmpq	%rbp, %rbx
	cmovll	%ebx, %ebp
	cmpb	$0, 8(%r14)
	movq	%rcx, %rax
	je	.LBB2_18
# BB#16:                                #   in Loop: Header=BB2_33 Depth=1
	movl	4(%r14), %edi
	movq	112(%rsp), %rsi         # 8-byte Reload
	movl	%ebp, %edx
	callq	cli_writen
	cmpl	%ebp, %eax
	jne	.LBB2_344
# BB#17:                                # %._crit_edge513
                                        #   in Loop: Header=BB2_33 Depth=1
	movq	64(%r14), %rax
.LBB2_18:                               #   in Loop: Header=BB2_33 Depth=1
	movslq	%ebp, %rcx
	addq	%rcx, %rax
	movq	%rax, 64(%r14)
	subq	%rcx, %rbx
	jg	.LBB2_33
	jmp	.LBB2_345
.LBB2_19:                               #   in Loop: Header=BB2_33 Depth=1
	movl	$-3, %esi
	jmp	.LBB2_342
.LBB2_20:                               #   in Loop: Header=BB2_33 Depth=1
	movl	$-1, %esi
	jmp	.LBB2_342
.LBB2_21:                               #   in Loop: Header=BB2_33 Depth=1
	movl	$-7, %esi
	jmp	.LBB2_342
.LBB2_22:                               #   in Loop: Header=BB2_33 Depth=1
	movl	$-8, %esi
	jmp	.LBB2_342
.LBB2_23:                               #   in Loop: Header=BB2_33 Depth=1
	movl	$-4, %esi
	jmp	.LBB2_342
.LBB2_24:                               #   in Loop: Header=BB2_33 Depth=1
	movl	$-11, %esi
	jmp	.LBB2_342
.LBB2_25:                               #   in Loop: Header=BB2_33 Depth=1
	movl	$-2, %esi
	jmp	.LBB2_342
.LBB2_26:                               #   in Loop: Header=BB2_33 Depth=1
	movl	12(%r14), %esi
	testl	%esi, %esi
	je	.LBB2_28
# BB#27:                                #   in Loop: Header=BB2_33 Depth=1
	movq	%r14, %rdi
	callq	*16(%r14)
	movl	$-3, %esi
	testl	%eax, %eax
	jne	.LBB2_342
.LBB2_28:                               # %mszip_inflate.exit
                                        #   in Loop: Header=BB2_33 Depth=1
	movq	%r13, 48(%r14)
	movq	%rbp, 56(%r14)
	movl	%r15d, 80(%r14)
	movl	%r12d, 84(%r14)
	movl	32(%r14), %eax
	movq	120(%rsp), %rbx         # 8-byte Reload
	jmp	.LBB2_15
.LBB2_29:                               #   in Loop: Header=BB2_33 Depth=1
	movl	$-12, %esi
	jmp	.LBB2_342
.LBB2_31:                               #   in Loop: Header=BB2_33 Depth=1
	movl	$-3, %esi
	jmp	.LBB2_342
.LBB2_32:                               # %.lr.ph747.i.4
                                        #   in Loop: Header=BB2_33 Depth=1
	movl	$-4, %esi
	jmp	.LBB2_342
.LBB2_33:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_34 Depth 2
                                        #       Child Loop BB2_35 Depth 3
                                        #     Child Loop BB2_49 Depth 2
                                        #       Child Loop BB2_50 Depth 3
                                        #       Child Loop BB2_61 Depth 3
                                        #       Child Loop BB2_82 Depth 3
                                        #       Child Loop BB2_92 Depth 3
                                        #       Child Loop BB2_102 Depth 3
                                        #       Child Loop BB2_114 Depth 3
                                        #         Child Loop BB2_115 Depth 4
                                        #       Child Loop BB2_128 Depth 3
                                        #       Child Loop BB2_131 Depth 3
                                        #       Child Loop BB2_171 Depth 3
                                        #         Child Loop BB2_172 Depth 4
                                        #         Child Loop BB2_187 Depth 4
                                        #         Child Loop BB2_209 Depth 4
                                        #         Child Loop BB2_198 Depth 4
                                        #         Child Loop BB2_165 Depth 4
                                        #         Child Loop BB2_168 Depth 4
                                        #         Child Loop BB2_227 Depth 4
                                        #         Child Loop BB2_230 Depth 4
                                        #       Child Loop BB2_246 Depth 3
                                        #         Child Loop BB2_251 Depth 4
                                        #         Child Loop BB2_260 Depth 4
                                        #           Child Loop BB2_262 Depth 5
                                        #           Child Loop BB2_269 Depth 5
                                        #           Child Loop BB2_279 Depth 5
                                        #           Child Loop BB2_290 Depth 5
                                        #           Child Loop BB2_296 Depth 5
                                        #           Child Loop BB2_307 Depth 5
                                        #         Child Loop BB2_320 Depth 4
                                        #           Child Loop BB2_313 Depth 5
                                        #           Child Loop BB2_317 Depth 5
                                        #           Child Loop BB2_328 Depth 5
                                        #           Child Loop BB2_330 Depth 5
                                        #       Child Loop BB2_138 Depth 3
                                        #       Child Loop BB2_149 Depth 3
	movq	%rbx, 120(%rsp)         # 8-byte Spill
	movq	48(%r14), %r13
	movq	56(%r14), %rbp
	movl	80(%r14), %r15d
	movl	84(%r14), %r12d
	movl	%r12d, %ecx
	andl	$7, %ecx
	shrl	%cl, %r15d
	subl	%ecx, %r12d
	xorl	%eax, %eax
.LBB2_34:                               # %.thread
                                        #   Parent Loop BB2_33 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_35 Depth 3
	movl	%eax, %ebx
	cmpl	$7, %r12d
	jg	.LBB2_44
	.p2align	4, 0x90
.LBB2_35:                               # %.lr.ph
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_34 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbp, %r13
	jb	.LBB2_43
# BB#36:                                #   in Loop: Header=BB2_35 Depth=3
	movq	35752(%r14), %rax
	testq	%rax, %rax
	movq	40(%r14), %rsi
	movl	88(%r14), %edx
	je	.LBB2_38
# BB#37:                                #   in Loop: Header=BB2_35 Depth=3
	movq	35744(%r14), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB2_39
	jmp	.LBB2_344
.LBB2_38:                               #   in Loop: Header=BB2_35 Depth=3
	movl	(%r14), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB2_344
.LBB2_39:                               #   in Loop: Header=BB2_35 Depth=3
	jne	.LBB2_42
# BB#40:                                #   in Loop: Header=BB2_35 Depth=3
	cmpl	$0, 36(%r14)
	jne	.LBB2_343
# BB#41:                                #   in Loop: Header=BB2_35 Depth=3
	movq	40(%r14), %rax
	movb	$0, (%rax)
	movl	$1, 36(%r14)
	movl	$1, %eax
.LBB2_42:                               #   in Loop: Header=BB2_35 Depth=3
	movq	40(%r14), %r13
	movq	%r13, 48(%r14)
	movslq	%eax, %rbp
	addq	%r13, %rbp
	movq	%rbp, 56(%r14)
.LBB2_43:                               #   in Loop: Header=BB2_35 Depth=3
	movzbl	(%r13), %eax
	incq	%r13
	movl	%r12d, %ecx
	shll	%cl, %eax
	orl	%eax, %r15d
	leal	8(%r12), %eax
	testl	%r12d, %r12d
	movl	%eax, %r12d
	js	.LBB2_35
	jmp	.LBB2_45
.LBB2_44:                               #   in Loop: Header=BB2_34 Depth=2
	movl	%r12d, %eax
.LBB2_45:                               # %._crit_edge
                                        #   in Loop: Header=BB2_34 Depth=2
	movzbl	%r15b, %ecx
	shrl	$8, %r15d
	movl	%eax, %r12d
	addl	$-8, %r12d
	movl	$1, %eax
	cmpl	$67, %ecx
	je	.LBB2_34
# BB#46:                                #   in Loop: Header=BB2_34 Depth=2
	cmpl	$1, %ebx
	movl	$0, %eax
	jne	.LBB2_34
# BB#47:                                #   in Loop: Header=BB2_34 Depth=2
	cmpl	$75, %ecx
	movl	$0, %eax
	jne	.LBB2_34
# BB#48:                                #   in Loop: Header=BB2_33 Depth=1
	movl	$0, 12(%r14)
	movl	$0, 32(%r14)
	movq	%r13, 48(%r14)
	movq	%rbp, 56(%r14)
	movl	%r15d, 80(%r14)
	movl	%r12d, 84(%r14)
.LBB2_49:                               #   Parent Loop BB2_33 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_50 Depth 3
                                        #       Child Loop BB2_61 Depth 3
                                        #       Child Loop BB2_82 Depth 3
                                        #       Child Loop BB2_92 Depth 3
                                        #       Child Loop BB2_102 Depth 3
                                        #       Child Loop BB2_114 Depth 3
                                        #         Child Loop BB2_115 Depth 4
                                        #       Child Loop BB2_128 Depth 3
                                        #       Child Loop BB2_131 Depth 3
                                        #       Child Loop BB2_171 Depth 3
                                        #         Child Loop BB2_172 Depth 4
                                        #         Child Loop BB2_187 Depth 4
                                        #         Child Loop BB2_209 Depth 4
                                        #         Child Loop BB2_198 Depth 4
                                        #         Child Loop BB2_165 Depth 4
                                        #         Child Loop BB2_168 Depth 4
                                        #         Child Loop BB2_227 Depth 4
                                        #         Child Loop BB2_230 Depth 4
                                        #       Child Loop BB2_246 Depth 3
                                        #         Child Loop BB2_251 Depth 4
                                        #         Child Loop BB2_260 Depth 4
                                        #           Child Loop BB2_262 Depth 5
                                        #           Child Loop BB2_269 Depth 5
                                        #           Child Loop BB2_279 Depth 5
                                        #           Child Loop BB2_290 Depth 5
                                        #           Child Loop BB2_296 Depth 5
                                        #           Child Loop BB2_307 Depth 5
                                        #         Child Loop BB2_320 Depth 4
                                        #           Child Loop BB2_313 Depth 5
                                        #           Child Loop BB2_317 Depth 5
                                        #           Child Loop BB2_328 Depth 5
                                        #           Child Loop BB2_330 Depth 5
                                        #       Child Loop BB2_138 Depth 3
                                        #       Child Loop BB2_149 Depth 3
                                        # kill: %R15D<def> %R15D<kill> %R15<kill>
	testl	%r12d, %r12d
	jg	.LBB2_59
.LBB2_50:                               # %.lr.ph.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbp, %r13
	jb	.LBB2_58
# BB#51:                                #   in Loop: Header=BB2_50 Depth=3
	movq	35752(%r14), %rax
	testq	%rax, %rax
	movq	40(%r14), %rsi
	movl	88(%r14), %edx
	je	.LBB2_53
# BB#52:                                #   in Loop: Header=BB2_50 Depth=3
	movq	35744(%r14), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB2_54
	jmp	.LBB2_338
.LBB2_53:                               #   in Loop: Header=BB2_50 Depth=3
	movl	(%r14), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB2_338
.LBB2_54:                               #   in Loop: Header=BB2_50 Depth=3
	jne	.LBB2_57
# BB#55:                                #   in Loop: Header=BB2_50 Depth=3
	cmpl	$0, 36(%r14)
	jne	.LBB2_337
# BB#56:                                #   in Loop: Header=BB2_50 Depth=3
	movq	40(%r14), %rax
	movb	$0, (%rax)
	movl	$1, 36(%r14)
	movl	$1, %eax
.LBB2_57:                               #   in Loop: Header=BB2_50 Depth=3
	movq	40(%r14), %r13
	movq	%r13, 48(%r14)
	movslq	%eax, %rbp
	addq	%r13, %rbp
	movq	%rbp, 56(%r14)
.LBB2_58:                               #   in Loop: Header=BB2_50 Depth=3
	movzbl	(%r13), %eax
	incq	%r13
	movl	%r12d, %ecx
	shll	%cl, %eax
	orl	%eax, %r15d
	addl	$8, %r12d
	jle	.LBB2_50
.LBB2_59:                               # %._crit_edge.i
                                        #   in Loop: Header=BB2_49 Depth=2
	movl	%r15d, %eax
	shrl	%eax
	leal	-1(%r12), %ebx
	cmpl	$2, %r12d
	jg	.LBB2_70
# BB#60:                                # %.lr.ph671.i.preheader
                                        #   in Loop: Header=BB2_49 Depth=2
	movq	%rax, %r12
.LBB2_61:                               # %.lr.ph671.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbp, %r13
	jb	.LBB2_69
# BB#62:                                #   in Loop: Header=BB2_61 Depth=3
	movq	35752(%r14), %rax
	testq	%rax, %rax
	movq	40(%r14), %rsi
	movl	88(%r14), %edx
	je	.LBB2_64
# BB#63:                                #   in Loop: Header=BB2_61 Depth=3
	movq	35744(%r14), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB2_65
	jmp	.LBB2_338
.LBB2_64:                               #   in Loop: Header=BB2_61 Depth=3
	movl	(%r14), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB2_338
.LBB2_65:                               #   in Loop: Header=BB2_61 Depth=3
	jne	.LBB2_68
# BB#66:                                #   in Loop: Header=BB2_61 Depth=3
	cmpl	$0, 36(%r14)
	jne	.LBB2_337
# BB#67:                                #   in Loop: Header=BB2_61 Depth=3
	movq	40(%r14), %rax
	movb	$0, (%rax)
	movl	$1, 36(%r14)
	movl	$1, %eax
.LBB2_68:                               #   in Loop: Header=BB2_61 Depth=3
	movq	40(%r14), %r13
	movq	%r13, 48(%r14)
	movslq	%eax, %rbp
	addq	%r13, %rbp
	movq	%rbp, 56(%r14)
.LBB2_69:                               #   in Loop: Header=BB2_61 Depth=3
	movzbl	(%r13), %eax
	incq	%r13
	movl	%ebx, %ecx
	shll	%cl, %eax
	orl	%eax, %r12d
	addl	$8, %ebx
	cmpl	$2, %ebx
	jl	.LBB2_61
	jmp	.LBB2_71
.LBB2_70:                               #   in Loop: Header=BB2_49 Depth=2
	movq	%rax, %r12
.LBB2_71:                               # %._crit_edge672.i
                                        #   in Loop: Header=BB2_49 Depth=2
	movl	%r15d, 76(%rsp)         # 4-byte Spill
	movl	%r12d, %r15d
	shrl	$2, %r15d
	movq	%r12, %rcx
	movl	%ebx, %r12d
	addl	$-2, %r12d
	andl	$3, %ecx
	je	.LBB2_75
# BB#72:                                #   in Loop: Header=BB2_49 Depth=2
	leal	-1(%rcx), %eax
	cmpl	$1, %eax
	ja	.LBB2_20
# BB#73:                                #   in Loop: Header=BB2_49 Depth=2
	cmpl	$1, %ecx
	movq	64(%rsp), %rdx          # 8-byte Reload
	jne	.LBB2_81
# BB#74:                                # %.preheader463.preheader.i
                                        #   in Loop: Header=BB2_49 Depth=2
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8]
	movups	%xmm0, 128(%rdx)
	movups	%xmm0, 112(%rdx)
	movups	%xmm0, 96(%rdx)
	movups	%xmm0, 80(%rdx)
	movups	%xmm0, 64(%rdx)
	movups	%xmm0, 48(%rdx)
	movups	%xmm0, 32(%rdx)
	movups	%xmm0, 16(%rdx)
	movups	%xmm0, (%rdx)
	movq	152(%rsp), %rax         # 8-byte Reload
	movaps	.LCPI2_1(%rip), %xmm0   # xmm0 = [9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9]
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	144(%rsp), %rax         # 8-byte Reload
	movabsq	$506381209866536711, %rcx # imm = 0x707070707070707
	movq	%rcx, 16(%rax)
	movaps	.LCPI2_2(%rip), %xmm0   # xmm0 = [7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7]
	movups	%xmm0, (%rax)
	movabsq	$578721382704613384, %rax # imm = 0x808080808080808
	movq	%rax, 372(%r14)
	movq	56(%rsp), %rax          # 8-byte Reload
	movdqa	.LCPI2_3(%rip), %xmm0   # xmm0 = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
	movdqu	%xmm0, 16(%rax)
	movdqu	%xmm0, (%rax)
	jmp	.LBB2_242
.LBB2_75:                               #   in Loop: Header=BB2_49 Depth=2
	movl	%r12d, %ecx
	andl	$7, %ecx
	shrl	%cl, %r15d
	subl	%ecx, %r12d
	xorl	%eax, %eax
	cmpl	$8, %r12d
	jl	.LBB2_135
# BB#76:                                #   in Loop: Header=BB2_49 Depth=2
	movb	%r15b, 176(%rsp)
	movl	%r15d, %ecx
	shrl	$8, %ecx
	leal	-8(%r12), %edx
	movl	$1, %eax
	cmpl	$7, %edx
	jle	.LBB2_134
# BB#77:                                #   in Loop: Header=BB2_49 Depth=2
	movb	%cl, 177(%rsp)
	movl	%r15d, %ecx
	shrl	$16, %ecx
	leal	-16(%r12), %edx
	movl	$2, %eax
	cmpl	$8, %edx
	jl	.LBB2_134
# BB#78:                                #   in Loop: Header=BB2_49 Depth=2
	movb	%cl, 178(%rsp)
	shrl	$24, %r15d
	leal	-24(%r12), %ecx
	movl	$3, %eax
	cmpl	$8, %ecx
	jl	.LBB2_162
# BB#79:                                #   in Loop: Header=BB2_49 Depth=2
	movb	%r15b, 179(%rsp)
	addl	$-32, %r12d
	cmpl	$8, %r12d
	jge	.LBB2_32
# BB#80:                                #   in Loop: Header=BB2_49 Depth=2
	xorl	%r15d, %r15d
	movl	$4, %eax
	testl	%r12d, %r12d
	je	.LBB2_136
	jmp	.LBB2_23
.LBB2_81:                               #   in Loop: Header=BB2_49 Depth=2
	movq	%r13, 48(%r14)
	movq	%rbp, 56(%r14)
	movl	%r15d, 80(%r14)
	movl	%r12d, 84(%r14)
	cmpl	$4, %r12d
	jg	.LBB2_91
.LBB2_82:                               # %.lr.ph519.i.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbp, %r13
	jb	.LBB2_90
# BB#83:                                #   in Loop: Header=BB2_82 Depth=3
	movq	35752(%r14), %rax
	testq	%rax, %rax
	movq	40(%r14), %rsi
	movl	88(%r14), %edx
	je	.LBB2_85
# BB#84:                                #   in Loop: Header=BB2_82 Depth=3
	movq	35744(%r14), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB2_86
	jmp	.LBB2_239
.LBB2_85:                               #   in Loop: Header=BB2_82 Depth=3
	movl	(%r14), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB2_239
.LBB2_86:                               #   in Loop: Header=BB2_82 Depth=3
	jne	.LBB2_89
# BB#87:                                #   in Loop: Header=BB2_82 Depth=3
	cmpl	$0, 36(%r14)
	jne	.LBB2_238
# BB#88:                                #   in Loop: Header=BB2_82 Depth=3
	movq	40(%r14), %rax
	movb	$0, (%rax)
	movl	$1, 36(%r14)
	movl	$1, %eax
.LBB2_89:                               #   in Loop: Header=BB2_82 Depth=3
	movq	40(%r14), %r13
	movq	%r13, 48(%r14)
	movslq	%eax, %rbp
	addq	%r13, %rbp
	movq	%rbp, 56(%r14)
.LBB2_90:                               #   in Loop: Header=BB2_82 Depth=3
	movzbl	(%r13), %eax
	incq	%r13
	movl	%r12d, %ecx
	shll	%cl, %eax
	orl	%eax, %r15d
	addl	$8, %r12d
	cmpl	$5, %r12d
	jl	.LBB2_82
.LBB2_91:                               # %._crit_edge520.i.i
                                        #   in Loop: Header=BB2_49 Depth=2
	movl	%r15d, %ebx
	shrl	$5, %ebx
	addl	$-5, %r12d
	cmpl	$4, %r12d
	jg	.LBB2_101
.LBB2_92:                               # %.lr.ph508.i.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbp, %r13
	jb	.LBB2_100
# BB#93:                                #   in Loop: Header=BB2_92 Depth=3
	movq	35752(%r14), %rax
	testq	%rax, %rax
	movq	40(%r14), %rsi
	movl	88(%r14), %edx
	je	.LBB2_95
# BB#94:                                #   in Loop: Header=BB2_92 Depth=3
	movq	35744(%r14), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB2_96
	jmp	.LBB2_239
.LBB2_95:                               #   in Loop: Header=BB2_92 Depth=3
	movl	(%r14), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB2_239
.LBB2_96:                               #   in Loop: Header=BB2_92 Depth=3
	jne	.LBB2_99
# BB#97:                                #   in Loop: Header=BB2_92 Depth=3
	cmpl	$0, 36(%r14)
	jne	.LBB2_238
# BB#98:                                #   in Loop: Header=BB2_92 Depth=3
	movq	40(%r14), %rax
	movb	$0, (%rax)
	movl	$1, 36(%r14)
	movl	$1, %eax
.LBB2_99:                               #   in Loop: Header=BB2_92 Depth=3
	movq	40(%r14), %r13
	movq	%r13, 48(%r14)
	movslq	%eax, %rbp
	addq	%r13, %rbp
	movq	%rbp, 56(%r14)
.LBB2_100:                              #   in Loop: Header=BB2_92 Depth=3
	movzbl	(%r13), %eax
	incq	%r13
	movl	%r12d, %ecx
	shll	%cl, %eax
	orl	%eax, %ebx
	addl	$8, %r12d
	cmpl	$5, %r12d
	jl	.LBB2_92
.LBB2_101:                              # %._crit_edge509.i.i
                                        #   in Loop: Header=BB2_49 Depth=2
	movq	%rbx, 8(%rsp)           # 8-byte Spill
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill>
	shrl	$5, %ebx
	addl	$-5, %r12d
	cmpl	$3, %r12d
	jg	.LBB2_111
.LBB2_102:                              # %.lr.ph497.i.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbp, %r13
	jb	.LBB2_110
# BB#103:                               #   in Loop: Header=BB2_102 Depth=3
	movq	35752(%r14), %rax
	testq	%rax, %rax
	movq	40(%r14), %rsi
	movl	88(%r14), %edx
	je	.LBB2_105
# BB#104:                               #   in Loop: Header=BB2_102 Depth=3
	movq	35744(%r14), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB2_106
	jmp	.LBB2_239
.LBB2_105:                              #   in Loop: Header=BB2_102 Depth=3
	movl	(%r14), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB2_239
.LBB2_106:                              #   in Loop: Header=BB2_102 Depth=3
	jne	.LBB2_109
# BB#107:                               #   in Loop: Header=BB2_102 Depth=3
	cmpl	$0, 36(%r14)
	jne	.LBB2_238
# BB#108:                               #   in Loop: Header=BB2_102 Depth=3
	movq	40(%r14), %rax
	movb	$0, (%rax)
	movl	$1, 36(%r14)
	movl	$1, %eax
.LBB2_109:                              #   in Loop: Header=BB2_102 Depth=3
	movq	40(%r14), %r13
	movq	%r13, 48(%r14)
	movslq	%eax, %rbp
	addq	%r13, %rbp
	movq	%rbp, 56(%r14)
.LBB2_110:                              #   in Loop: Header=BB2_102 Depth=3
	movzbl	(%r13), %eax
	incq	%r13
	movl	%r12d, %ecx
	shll	%cl, %eax
	orl	%eax, %ebx
	addl	$8, %r12d
	cmpl	$4, %r12d
	jl	.LBB2_102
.LBB2_111:                              # %._crit_edge498.i.i
                                        #   in Loop: Header=BB2_49 Depth=2
	andl	$31, %r15d
	leal	257(%r15), %eax
	cmpl	$288, %eax              # imm = 0x120
	movl	$-5, %esi
	ja	.LBB2_240
# BB#112:                               # %._crit_edge498.i.i
                                        #   in Loop: Header=BB2_49 Depth=2
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	andl	$31, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leal	1(%rax), %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpl	$32, %eax
	ja	.LBB2_240
# BB#113:                               # %.preheader296.i.i
                                        #   in Loop: Header=BB2_49 Depth=2
	movl	%ebx, %eax
	andl	$15, %eax
	addl	$4, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	addl	$-4, %r12d
	shrl	$4, %ebx
	xorl	%eax, %eax
.LBB2_114:                              # %.preheader294.i.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_115 Depth 4
	movq	%rax, %rdx
	cmpl	$2, %r12d
	movq	%rax, (%rsp)            # 8-byte Spill
	jg	.LBB2_124
.LBB2_115:                              # %.lr.ph481.i.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_114 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	%rbp, %r13
	jb	.LBB2_123
# BB#116:                               #   in Loop: Header=BB2_115 Depth=4
	movq	35752(%r14), %rax
	testq	%rax, %rax
	movq	40(%r14), %rsi
	movl	88(%r14), %edx
	je	.LBB2_118
# BB#117:                               #   in Loop: Header=BB2_115 Depth=4
	movq	35744(%r14), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB2_119
	jmp	.LBB2_239
.LBB2_118:                              #   in Loop: Header=BB2_115 Depth=4
	movl	(%r14), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB2_239
.LBB2_119:                              #   in Loop: Header=BB2_115 Depth=4
	movq	(%rsp), %rdx            # 8-byte Reload
	jne	.LBB2_122
# BB#120:                               #   in Loop: Header=BB2_115 Depth=4
	cmpl	$0, 36(%r14)
	jne	.LBB2_238
# BB#121:                               #   in Loop: Header=BB2_115 Depth=4
	movq	40(%r14), %rax
	movb	$0, (%rax)
	movl	$1, 36(%r14)
	movl	$1, %eax
.LBB2_122:                              #   in Loop: Header=BB2_115 Depth=4
	movq	40(%r14), %r13
	movq	%r13, 48(%r14)
	movslq	%eax, %rbp
	addq	%r13, %rbp
	movq	%rbp, 56(%r14)
.LBB2_123:                              #   in Loop: Header=BB2_115 Depth=4
	movzbl	(%r13), %eax
	incq	%r13
	movl	%r12d, %ecx
	shll	%cl, %eax
	orl	%eax, %ebx
	addl	$8, %r12d
	cmpl	$3, %r12d
	jl	.LBB2_115
.LBB2_124:                              # %._crit_edge482.i.i
                                        #   in Loop: Header=BB2_114 Depth=3
	movl	%ebx, %eax
	andb	$7, %al
	movzbl	mszip_bitlen_order(%rdx), %ecx
	movb	%al, 80(%rsp,%rcx)
	shrl	$3, %ebx
	addl	$-3, %r12d
	leaq	1(%rdx), %rax
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	jb	.LBB2_114
# BB#125:                               # %.preheader293.i.i
                                        #   in Loop: Header=BB2_49 Depth=2
	cmpl	$18, %eax
	ja	.LBB2_132
# BB#126:                               # %.lr.ph475.preheader.i.i
                                        #   in Loop: Header=BB2_49 Depth=2
	movl	%eax, %ecx
	movl	$18, %esi
	subl	(%rsp), %esi            # 4-byte Folded Reload
	movl	$18, %edx
	subq	%rcx, %rdx
	testb	$3, %sil
	je	.LBB2_130
# BB#127:                               # %.lr.ph475.i.i.prol.preheader
                                        #   in Loop: Header=BB2_49 Depth=2
	movl	%eax, %ecx
	notb	%cl
	movzbl	%cl, %ecx
	andl	$3, %ecx
	xorl	%esi, %esi
.LBB2_128:                              # %.lr.ph475.i.i.prol
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	mszip_bitlen_order(%rsi,%rax), %edi
	movb	$0, 80(%rsp,%rdi)
	incq	%rsi
	cmpq	%rsi, %rcx
	jne	.LBB2_128
# BB#129:                               # %.lr.ph475.i.i.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB2_49 Depth=2
	addq	%rsi, %rax
	movq	%rax, %rcx
.LBB2_130:                              # %.lr.ph475.i.i.prol.loopexit
                                        #   in Loop: Header=BB2_49 Depth=2
	cmpq	$3, %rdx
	jb	.LBB2_132
.LBB2_131:                              # %.lr.ph475.i.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	mszip_bitlen_order(%rcx), %eax
	movb	$0, 80(%rsp,%rax)
	movzbl	mszip_bitlen_order+1(%rcx), %eax
	movb	$0, 80(%rsp,%rax)
	movzbl	mszip_bitlen_order+2(%rcx), %eax
	movb	$0, 80(%rsp,%rax)
	movzbl	mszip_bitlen_order+3(%rcx), %eax
	addq	$4, %rcx
	movb	$0, 80(%rsp,%rax)
	cmpq	$19, %rcx
	jne	.LBB2_131
.LBB2_132:                              # %._crit_edge476.i.i
                                        #   in Loop: Header=BB2_49 Depth=2
	movl	$19, %edi
	movl	$7, %esi
	leaq	80(%rsp), %rdx
	leaq	496(%rsp), %rcx
	callq	mszip_make_decode_table
	movl	$-6, %esi
	testl	%eax, %eax
	jne	.LBB2_240
# BB#133:                               # %.preheader291.i.i
                                        #   in Loop: Header=BB2_49 Depth=2
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	leal	(%rax,%rcx), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	$0, 52(%rsp)            # 4-byte Folded Spill
	jmp	.LBB2_171
.LBB2_134:                              #   in Loop: Header=BB2_49 Depth=2
	movl	%ecx, %r15d
	movl	%edx, %r12d
.LBB2_135:                              # %._crit_edge748.i
                                        #   in Loop: Header=BB2_49 Depth=2
	testl	%r12d, %r12d
	jne	.LBB2_23
.LBB2_136:                              # %.preheader459.i
                                        #   in Loop: Header=BB2_49 Depth=2
	cmpl	$3, %eax
	ja	.LBB2_147
# BB#137:                               # %.lr.ph755.preheader.i
                                        #   in Loop: Header=BB2_49 Depth=2
	movl	%eax, %ebx
.LBB2_138:                              # %.lr.ph755.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbp, %r13
	jb	.LBB2_146
# BB#139:                               #   in Loop: Header=BB2_138 Depth=3
	movq	35752(%r14), %rax
	testq	%rax, %rax
	movq	40(%r14), %rsi
	movl	88(%r14), %edx
	je	.LBB2_141
# BB#140:                               #   in Loop: Header=BB2_138 Depth=3
	movq	35744(%r14), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB2_142
	jmp	.LBB2_338
.LBB2_141:                              #   in Loop: Header=BB2_138 Depth=3
	movl	(%r14), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB2_338
.LBB2_142:                              #   in Loop: Header=BB2_138 Depth=3
	jne	.LBB2_145
# BB#143:                               #   in Loop: Header=BB2_138 Depth=3
	cmpl	$0, 36(%r14)
	jne	.LBB2_337
# BB#144:                               #   in Loop: Header=BB2_138 Depth=3
	movq	40(%r14), %rax
	movb	$0, (%rax)
	movl	$1, 36(%r14)
	movl	$1, %eax
.LBB2_145:                              #   in Loop: Header=BB2_138 Depth=3
	movq	40(%r14), %r13
	movq	%r13, 48(%r14)
	movslq	%eax, %rbp
	addq	%r13, %rbp
	movq	%rbp, 56(%r14)
.LBB2_146:                              #   in Loop: Header=BB2_138 Depth=3
	movzbl	(%r13), %eax
	incq	%r13
	movb	%al, 176(%rsp,%rbx)
	incq	%rbx
	cmpq	$4, %rbx
	jb	.LBB2_138
.LBB2_147:                              # %._crit_edge756.i
                                        #   in Loop: Header=BB2_49 Depth=2
	movzbl	176(%rsp), %eax
	movzbl	177(%rsp), %ebx
	shll	$8, %ebx
	orl	%eax, %ebx
	movzbl	178(%rsp), %eax
	movzbl	179(%rsp), %ecx
	shll	$8, %ecx
	orl	%eax, %ecx
	xorl	$65535, %ecx            # imm = 0xFFFF
	cmpl	%ecx, %ebx
	jne	.LBB2_25
# BB#148:                               # %.preheader456.i
                                        #   in Loop: Header=BB2_49 Depth=2
	testl	%ebx, %ebx
	je	.LBB2_161
.LBB2_149:                              # %.lr.ph762.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbp, %r13
	jb	.LBB2_157
# BB#150:                               #   in Loop: Header=BB2_149 Depth=3
	movq	35752(%r14), %rax
	testq	%rax, %rax
	movq	40(%r14), %rsi
	movl	88(%r14), %edx
	je	.LBB2_152
# BB#151:                               #   in Loop: Header=BB2_149 Depth=3
	movq	35744(%r14), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB2_153
	jmp	.LBB2_338
.LBB2_152:                              #   in Loop: Header=BB2_149 Depth=3
	movl	(%r14), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB2_338
.LBB2_153:                              #   in Loop: Header=BB2_149 Depth=3
	jne	.LBB2_156
# BB#154:                               #   in Loop: Header=BB2_149 Depth=3
	cmpl	$0, 36(%r14)
	jne	.LBB2_337
# BB#155:                               #   in Loop: Header=BB2_149 Depth=3
	movq	40(%r14), %rax
	movb	$0, (%rax)
	movl	$1, 36(%r14)
	movl	$1, %eax
.LBB2_156:                              #   in Loop: Header=BB2_149 Depth=3
	movq	40(%r14), %r13
	movq	%r13, 48(%r14)
	movslq	%eax, %rbp
	addq	%r13, %rbp
	movq	%rbp, 56(%r14)
.LBB2_157:                              #   in Loop: Header=BB2_149 Depth=3
	movl	%ebp, %eax
	subl	%r13d, %eax
	cmpl	%eax, %ebx
	movl	%ebx, (%rsp)            # 4-byte Spill
	cmovbel	%ebx, %eax
	movl	12(%r14), %r12d
	movl	$32768, %ebx            # imm = 0x8000
	subl	%r12d, %ebx
	cmpl	%ebx, %eax
	cmovbel	%eax, %ebx
	leaq	2972(%r14,%r12), %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	leal	(%rbx,%r12), %eax
	movl	%eax, 12(%r14)
	cmpl	$32768, %eax            # imm = 0x8000
	jne	.LBB2_160
# BB#158:                               #   in Loop: Header=BB2_149 Depth=3
	movl	$32768, %esi            # imm = 0x8000
	movq	%r14, %rdi
	callq	*16(%r14)
	testl	%eax, %eax
	jne	.LBB2_19
# BB#159:                               #   in Loop: Header=BB2_149 Depth=3
	movl	$0, 12(%r14)
.LBB2_160:                              # %.backedge458.i
                                        #   in Loop: Header=BB2_149 Depth=3
	addq	%rbx, %r13
	movl	(%rsp), %eax            # 4-byte Reload
	subl	%ebx, %eax
	movl	%eax, %ebx
	jne	.LBB2_149
.LBB2_161:                              # %._crit_edge763.i
                                        #   in Loop: Header=BB2_49 Depth=2
	xorl	%r12d, %r12d
	andl	$1, 76(%rsp)            # 4-byte Folded Spill
	je	.LBB2_49
	jmp	.LBB2_26
.LBB2_162:                              #   in Loop: Header=BB2_49 Depth=2
	movl	%ecx, %r12d
	testl	%r12d, %r12d
	je	.LBB2_136
	jmp	.LBB2_23
.LBB2_163:                              # %vector.ph901
                                        #   in Loop: Header=BB2_171 Depth=3
	movzbl	%r8b, %eax
	movd	%eax, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leal	-32(%rdi), %r9d
	movl	%r9d, %eax
	shrl	$5, %eax
	incl	%eax
	andl	$3, %eax
	je	.LBB2_166
# BB#164:                               # %vector.body892.prol.preheader
                                        #   in Loop: Header=BB2_171 Depth=3
	negl	%eax
	xorl	%ecx, %ecx
.LBB2_165:                              # %vector.body892.prol
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_171 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rsp), %rdx            # 8-byte Reload
	leal	(%rdx,%rcx), %r11d
	movdqu	%xmm0, 176(%rsp,%r11)
	movdqu	%xmm0, 192(%rsp,%r11)
	addl	$32, %ecx
	incl	%eax
	jne	.LBB2_165
	jmp	.LBB2_167
.LBB2_166:                              #   in Loop: Header=BB2_171 Depth=3
	xorl	%ecx, %ecx
.LBB2_167:                              # %vector.body892.prol.loopexit
                                        #   in Loop: Header=BB2_171 Depth=3
	cmpl	$96, %r9d
	jb	.LBB2_169
.LBB2_168:                              # %vector.body892
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_171 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, %rdx
	leal	(%rdx,%rcx), %eax
	movdqu	%xmm0, 176(%rsp,%rax)
	movdqu	%xmm0, 192(%rsp,%rax)
	leal	32(%rdx,%rcx), %eax
	movdqu	%xmm0, 176(%rsp,%rax)
	movdqu	%xmm0, 192(%rsp,%rax)
	leal	64(%rdx,%rcx), %eax
	movdqu	%xmm0, 176(%rsp,%rax)
	movdqu	%xmm0, 192(%rsp,%rax)
	leal	96(%rdx,%rcx), %eax
	movdqu	%xmm0, 176(%rsp,%rax)
	movdqu	%xmm0, 192(%rsp,%rax)
	subl	$-128, %ecx
	cmpl	%ecx, %edi
	jne	.LBB2_168
.LBB2_169:                              # %middle.block893
                                        #   in Loop: Header=BB2_171 Depth=3
	cmpl	%edi, %esi
	je	.LBB2_231
# BB#170:                               #   in Loop: Header=BB2_171 Depth=3
	subl	%edi, %esi
	addl	(%rsp), %edi            # 4-byte Folded Reload
	jmp	.LBB2_225
.LBB2_171:                              # %.preheader289.i.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_172 Depth 4
                                        #         Child Loop BB2_187 Depth 4
                                        #         Child Loop BB2_209 Depth 4
                                        #         Child Loop BB2_198 Depth 4
                                        #         Child Loop BB2_165 Depth 4
                                        #         Child Loop BB2_168 Depth 4
                                        #         Child Loop BB2_227 Depth 4
                                        #         Child Loop BB2_230 Depth 4
	cmpl	$6, %r12d
	jg	.LBB2_181
.LBB2_172:                              # %.lr.ph430.i.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_171 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	%rbp, %r13
	jb	.LBB2_180
# BB#173:                               #   in Loop: Header=BB2_172 Depth=4
	movq	35752(%r14), %rax
	testq	%rax, %rax
	movq	40(%r14), %rsi
	movl	88(%r14), %edx
	je	.LBB2_175
# BB#174:                               #   in Loop: Header=BB2_172 Depth=4
	movq	35744(%r14), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB2_176
	jmp	.LBB2_239
.LBB2_175:                              #   in Loop: Header=BB2_172 Depth=4
	movl	(%r14), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB2_239
.LBB2_176:                              #   in Loop: Header=BB2_172 Depth=4
	jne	.LBB2_179
# BB#177:                               #   in Loop: Header=BB2_172 Depth=4
	cmpl	$0, 36(%r14)
	jne	.LBB2_238
# BB#178:                               #   in Loop: Header=BB2_172 Depth=4
	movq	40(%r14), %rax
	movb	$0, (%rax)
	movl	$1, 36(%r14)
	movl	$1, %eax
.LBB2_179:                              #   in Loop: Header=BB2_172 Depth=4
	movq	40(%r14), %r13
	movq	%r13, 48(%r14)
	movslq	%eax, %rbp
	addq	%r13, %rbp
	movq	%rbp, 56(%r14)
.LBB2_180:                              #   in Loop: Header=BB2_172 Depth=4
	movzbl	(%r13), %eax
	incq	%r13
	movl	%r12d, %ecx
	shll	%cl, %eax
	orl	%eax, %ebx
	addl	$8, %r12d
	cmpl	$7, %r12d
	jl	.LBB2_172
.LBB2_181:                              # %._crit_edge431.i.i
                                        #   in Loop: Header=BB2_171 Depth=3
	movl	%ebx, %eax
	andl	$127, %eax
	movzwl	496(%rsp,%rax,2), %esi
	movzbl	80(%rsp,%rsi), %ecx
	shrl	%cl, %ebx
	subl	%ecx, %r12d
	cmpq	$15, %rsi
	ja	.LBB2_183
# BB#182:                               #   in Loop: Header=BB2_171 Depth=3
	movl	(%rsp), %eax            # 4-byte Reload
	movb	%sil, 176(%rsp,%rax)
	movl	%esi, %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	jmp	.LBB2_232
.LBB2_183:                              #   in Loop: Header=BB2_171 Depth=3
	cmpl	$16, %esi
	je	.LBB2_197
# BB#184:                               #   in Loop: Header=BB2_171 Depth=3
	movzwl	%si, %eax
	cmpl	$17, %eax
	je	.LBB2_208
# BB#185:                               #   in Loop: Header=BB2_171 Depth=3
	cmpl	$18, %eax
	jne	.LBB2_335
# BB#186:                               # %.preheader287.i.i
                                        #   in Loop: Header=BB2_171 Depth=3
	cmpl	$6, %r12d
	jg	.LBB2_196
.LBB2_187:                              # %.lr.ph439.i.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_171 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	%rbp, %r13
	jb	.LBB2_195
# BB#188:                               #   in Loop: Header=BB2_187 Depth=4
	movq	35752(%r14), %rax
	testq	%rax, %rax
	movq	40(%r14), %rsi
	movl	88(%r14), %edx
	je	.LBB2_190
# BB#189:                               #   in Loop: Header=BB2_187 Depth=4
	movq	35744(%r14), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB2_191
	jmp	.LBB2_239
.LBB2_190:                              #   in Loop: Header=BB2_187 Depth=4
	movl	(%r14), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB2_239
.LBB2_191:                              #   in Loop: Header=BB2_187 Depth=4
	jne	.LBB2_194
# BB#192:                               #   in Loop: Header=BB2_187 Depth=4
	cmpl	$0, 36(%r14)
	jne	.LBB2_238
# BB#193:                               #   in Loop: Header=BB2_187 Depth=4
	movq	40(%r14), %rax
	movb	$0, (%rax)
	movl	$1, 36(%r14)
	movl	$1, %eax
.LBB2_194:                              #   in Loop: Header=BB2_187 Depth=4
	movq	40(%r14), %r13
	movq	%r13, 48(%r14)
	movslq	%eax, %rbp
	addq	%r13, %rbp
	movq	%rbp, 56(%r14)
.LBB2_195:                              #   in Loop: Header=BB2_187 Depth=4
	movzbl	(%r13), %eax
	incq	%r13
	movl	%r12d, %ecx
	shll	%cl, %eax
	orl	%eax, %ebx
	addl	$8, %r12d
	cmpl	$7, %r12d
	jl	.LBB2_187
.LBB2_196:                              # %._crit_edge440.i.i
                                        #   in Loop: Header=BB2_171 Depth=3
	movl	%ebx, %r10d
	andl	$127, %r10d
	shrl	$7, %ebx
	addl	$-7, %r12d
	xorl	%r8d, %r8d
	movl	$11, %eax
	jmp	.LBB2_219
.LBB2_197:                              # %.preheader284.i.i
                                        #   in Loop: Header=BB2_171 Depth=3
	cmpl	$1, %r12d
	jg	.LBB2_207
.LBB2_198:                              # %.lr.ph459.i.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_171 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	%rbp, %r13
	jb	.LBB2_206
# BB#199:                               #   in Loop: Header=BB2_198 Depth=4
	movq	35752(%r14), %rax
	testq	%rax, %rax
	movq	40(%r14), %rsi
	movl	88(%r14), %edx
	je	.LBB2_201
# BB#200:                               #   in Loop: Header=BB2_198 Depth=4
	movq	35744(%r14), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB2_202
	jmp	.LBB2_239
.LBB2_201:                              #   in Loop: Header=BB2_198 Depth=4
	movl	(%r14), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB2_239
.LBB2_202:                              #   in Loop: Header=BB2_198 Depth=4
	jne	.LBB2_205
# BB#203:                               #   in Loop: Header=BB2_198 Depth=4
	cmpl	$0, 36(%r14)
	jne	.LBB2_238
# BB#204:                               #   in Loop: Header=BB2_198 Depth=4
	movq	40(%r14), %rax
	movb	$0, (%rax)
	movl	$1, 36(%r14)
	movl	$1, %eax
.LBB2_205:                              #   in Loop: Header=BB2_198 Depth=4
	movq	40(%r14), %r13
	movq	%r13, 48(%r14)
	movslq	%eax, %rbp
	addq	%r13, %rbp
	movq	%rbp, 56(%r14)
.LBB2_206:                              #   in Loop: Header=BB2_198 Depth=4
	movzbl	(%r13), %eax
	incq	%r13
	movl	%r12d, %ecx
	shll	%cl, %eax
	orl	%eax, %ebx
	addl	$8, %r12d
	cmpl	$2, %r12d
	jl	.LBB2_198
.LBB2_207:                              # %._crit_edge460.i.i
                                        #   in Loop: Header=BB2_171 Depth=3
	movl	%ebx, %r10d
	andl	$3, %r10d
	shrl	$2, %ebx
	addl	$-2, %r12d
	movl	$3, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	52(%rsp), %r8d          # 4-byte Reload
	jmp	.LBB2_220
.LBB2_208:                              # %.preheader285.i.i
                                        #   in Loop: Header=BB2_171 Depth=3
	cmpl	$2, %r12d
	jg	.LBB2_218
.LBB2_209:                              # %.lr.ph449.i.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_171 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	%rbp, %r13
	jb	.LBB2_217
# BB#210:                               #   in Loop: Header=BB2_209 Depth=4
	movq	35752(%r14), %rax
	testq	%rax, %rax
	movq	40(%r14), %rsi
	movl	88(%r14), %edx
	je	.LBB2_212
# BB#211:                               #   in Loop: Header=BB2_209 Depth=4
	movq	35744(%r14), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB2_213
	jmp	.LBB2_239
.LBB2_212:                              #   in Loop: Header=BB2_209 Depth=4
	movl	(%r14), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB2_239
.LBB2_213:                              #   in Loop: Header=BB2_209 Depth=4
	jne	.LBB2_216
# BB#214:                               #   in Loop: Header=BB2_209 Depth=4
	cmpl	$0, 36(%r14)
	jne	.LBB2_238
# BB#215:                               #   in Loop: Header=BB2_209 Depth=4
	movq	40(%r14), %rax
	movb	$0, (%rax)
	movl	$1, 36(%r14)
	movl	$1, %eax
.LBB2_216:                              #   in Loop: Header=BB2_209 Depth=4
	movq	40(%r14), %r13
	movq	%r13, 48(%r14)
	movslq	%eax, %rbp
	addq	%r13, %rbp
	movq	%rbp, 56(%r14)
.LBB2_217:                              #   in Loop: Header=BB2_209 Depth=4
	movzbl	(%r13), %eax
	incq	%r13
	movl	%r12d, %ecx
	shll	%cl, %eax
	orl	%eax, %ebx
	addl	$8, %r12d
	cmpl	$3, %r12d
	jl	.LBB2_209
.LBB2_218:                              # %._crit_edge450.i.i
                                        #   in Loop: Header=BB2_171 Depth=3
	movl	%ebx, %r10d
	andl	$7, %r10d
	shrl	$3, %ebx
	addl	$-3, %r12d
	xorl	%r8d, %r8d
	movl	$3, %eax
.LBB2_219:                              #   in Loop: Header=BB2_171 Depth=3
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB2_220:                              #   in Loop: Header=BB2_171 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	(%r10,%rax), %esi
	movq	(%rsp), %rax            # 8-byte Reload
	leal	(%rsi,%rax), %eax
	cmpl	32(%rsp), %eax          # 4-byte Folded Reload
	ja	.LBB2_336
# BB#221:                               # %.preheader.i.i
                                        #   in Loop: Header=BB2_171 Depth=3
	cmpl	$32, %esi
	jb	.LBB2_224
# BB#222:                               # %min.iters.checked896
                                        #   in Loop: Header=BB2_171 Depth=3
	movl	%esi, %edi
	andl	$224, %edi
	je	.LBB2_224
# BB#223:                               # %vector.scevcheck
                                        #   in Loop: Header=BB2_171 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	-1(%r10,%rax), %eax
	addl	(%rsp), %eax            # 4-byte Folded Reload
	jae	.LBB2_163
.LBB2_224:                              #   in Loop: Header=BB2_171 Depth=3
	movq	(%rsp), %rdi            # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
.LBB2_225:                              # %scalar.ph894.preheader
                                        #   in Loop: Header=BB2_171 Depth=3
	leal	-1(%rsi), %r9d
	movl	%esi, %eax
	andl	$7, %eax
	je	.LBB2_228
# BB#226:                               # %scalar.ph894.prol.preheader
                                        #   in Loop: Header=BB2_171 Depth=3
	negl	%eax
.LBB2_227:                              # %scalar.ph894.prol
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_171 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	decl	%esi
	movl	%edi, %ecx
	incl	%edi
	movb	%r8b, 176(%rsp,%rcx)
	incl	%eax
	jne	.LBB2_227
.LBB2_228:                              # %scalar.ph894.prol.loopexit
                                        #   in Loop: Header=BB2_171 Depth=3
	cmpl	$7, %r9d
	jb	.LBB2_231
# BB#229:                               # %scalar.ph894.preheader.new
                                        #   in Loop: Header=BB2_171 Depth=3
	xorl	%ecx, %ecx
.LBB2_230:                              # %scalar.ph894
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_171 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leal	(%rdi,%rcx), %eax
	leal	1(%rdi,%rcx), %edx
	movb	%r8b, 176(%rsp,%rax)
	leal	2(%rdi,%rcx), %eax
	movb	%r8b, 176(%rsp,%rdx)
	leal	3(%rdi,%rcx), %edx
	movb	%r8b, 176(%rsp,%rax)
	leal	4(%rdi,%rcx), %eax
	movb	%r8b, 176(%rsp,%rdx)
	leal	5(%rdi,%rcx), %edx
	movb	%r8b, 176(%rsp,%rax)
	leal	6(%rdi,%rcx), %eax
	movb	%r8b, 176(%rsp,%rdx)
	leal	7(%rdi,%rcx), %edx
	movb	%r8b, 176(%rsp,%rax)
	movb	%r8b, 176(%rsp,%rdx)
	addl	$8, %ecx
	cmpl	%ecx, %esi
	jne	.LBB2_230
.LBB2_231:                              # %.loopexit921
                                        #   in Loop: Header=BB2_171 Depth=3
	movq	(%rsp), %rax            # 8-byte Reload
	addl	16(%rsp), %eax          # 4-byte Folded Reload
	leal	-1(%r10,%rax), %eax
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB2_232:                              #   in Loop: Header=BB2_171 Depth=3
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, %rcx
	incl	%ecx
	movq	%rcx, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	cmpl	32(%rsp), %ecx          # 4-byte Folded Reload
	jb	.LBB2_171
# BB#233:                               #   in Loop: Header=BB2_49 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, %edx
	movq	64(%rsp), %rdi          # 8-byte Reload
	leaq	176(%rsp), %rsi
	movq	%rdx, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	callq	memcpy
	cmpl	$287, 24(%rsp)          # 4-byte Folded Reload
                                        # imm = 0x11F
	ja	.LBB2_235
# BB#234:                               # %.lr.ph424.preheader.i.i
                                        #   in Loop: Header=BB2_49 Depth=2
	movl	%r15d, %eax
	leaq	349(%r14,%rax), %rdi
	movl	$30, %edx
	subl	%r15d, %edx
	incq	%rdx
	xorl	%esi, %esi
	callq	memset
.LBB2_235:                              # %._crit_edge425.i.i
                                        #   in Loop: Header=BB2_49 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	176(%rsp,%rax), %rsi
	movq	40(%rsp), %r15          # 8-byte Reload
	movl	%r15d, %edx
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	memcpy
	cmpl	$31, %r15d
	ja	.LBB2_237
# BB#236:                               # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB2_49 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%ecx, %eax
	leaq	381(%r14,%rax), %rdi
	movl	$30, %edx
	subl	%ecx, %edx
	incq	%rdx
	xorl	%esi, %esi
	callq	memset
.LBB2_237:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB2_49 Depth=2
	movq	%r13, 48(%r14)
	movq	%rbp, 56(%r14)
	movl	%ebx, 80(%r14)
	movl	%r12d, 84(%r14)
	xorl	%esi, %esi
	testl	%esi, %esi
	je	.LBB2_241
	jmp	.LBB2_342
.LBB2_238:                              #   in Loop: Header=BB2_49 Depth=2
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB2_239:                              # %.loopexit299.i.i
                                        #   in Loop: Header=BB2_49 Depth=2
	movl	$-123, 24(%r14)
	movl	$-123, %esi
.LBB2_240:                              # %mszip_read_lens.exit.i
                                        #   in Loop: Header=BB2_49 Depth=2
	testl	%esi, %esi
	jne	.LBB2_342
.LBB2_241:                              #   in Loop: Header=BB2_49 Depth=2
	movq	48(%r14), %r13
	movq	56(%r14), %rbp
	movl	80(%r14), %r15d
	movl	84(%r14), %r12d
	movq	64(%rsp), %rdx          # 8-byte Reload
.LBB2_242:                              # %.loopexit464.i
                                        #   in Loop: Header=BB2_49 Depth=2
	movl	$288, %edi              # imm = 0x120
	movl	$9, %esi
	movq	168(%rsp), %rcx         # 8-byte Reload
	callq	mszip_make_decode_table
	testl	%eax, %eax
	jne	.LBB2_21
# BB#243:                               #   in Loop: Header=BB2_49 Depth=2
	movl	$32, %edi
	movl	$6, %esi
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	callq	mszip_make_decode_table
	testl	%eax, %eax
	jne	.LBB2_22
# BB#244:                               #   in Loop: Header=BB2_49 Depth=2
	movl	12(%r14), %eax
	jmp	.LBB2_246
.LBB2_245:                              #   in Loop: Header=BB2_246 Depth=3
	movl	%edi, %eax
.LBB2_246:                              # %.thread432.outer.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_251 Depth 4
                                        #         Child Loop BB2_260 Depth 4
                                        #           Child Loop BB2_262 Depth 5
                                        #           Child Loop BB2_269 Depth 5
                                        #           Child Loop BB2_279 Depth 5
                                        #           Child Loop BB2_290 Depth 5
                                        #           Child Loop BB2_296 Depth 5
                                        #           Child Loop BB2_307 Depth 5
                                        #         Child Loop BB2_320 Depth 4
                                        #           Child Loop BB2_313 Depth 5
                                        #           Child Loop BB2_317 Depth 5
                                        #           Child Loop BB2_328 Depth 5
                                        #           Child Loop BB2_330 Depth 5
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpl	$15, %r12d
	jg	.LBB2_260
	jmp	.LBB2_251
.LBB2_249:                              #   in Loop: Header=BB2_260 Depth=4
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	cmpl	$15, %r12d
	jg	.LBB2_260
	jmp	.LBB2_251
.LBB2_247:                              #   in Loop: Header=BB2_260 Depth=4
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	%edx, %ecx
	incl	%edx
	movb	%al, 2972(%r14,%rcx)
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB2_250
# BB#248:                               #   in Loop: Header=BB2_260 Depth=4
	movl	$32768, %esi            # imm = 0x8000
	movq	%r14, %rdi
	callq	*16(%r14)
	movl	$-3, %esi
	xorl	%ecx, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	testl	%eax, %eax
	jne	.LBB2_342
.LBB2_250:                              # %.thread432.i
                                        #   in Loop: Header=BB2_260 Depth=4
	cmpl	$15, %r12d
	jg	.LBB2_260
.LBB2_251:                              # %.lr.ph691.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_246 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	%rbp, %r13
	jb	.LBB2_259
# BB#252:                               #   in Loop: Header=BB2_251 Depth=4
	movq	35752(%r14), %rax
	testq	%rax, %rax
	movq	40(%r14), %rsi
	movl	88(%r14), %edx
	je	.LBB2_254
# BB#253:                               #   in Loop: Header=BB2_251 Depth=4
	movq	35744(%r14), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB2_255
	jmp	.LBB2_338
.LBB2_254:                              #   in Loop: Header=BB2_251 Depth=4
	movl	(%r14), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB2_338
.LBB2_255:                              #   in Loop: Header=BB2_251 Depth=4
	jne	.LBB2_258
# BB#256:                               #   in Loop: Header=BB2_251 Depth=4
	cmpl	$0, 36(%r14)
	jne	.LBB2_337
# BB#257:                               #   in Loop: Header=BB2_251 Depth=4
	movq	40(%r14), %rax
	movb	$0, (%rax)
	movl	$1, 36(%r14)
	movl	$1, %eax
.LBB2_258:                              #   in Loop: Header=BB2_251 Depth=4
	movq	40(%r14), %r13
	movq	%r13, 48(%r14)
	movslq	%eax, %rbp
	addq	%r13, %rbp
	movq	%rbp, 56(%r14)
.LBB2_259:                              #   in Loop: Header=BB2_251 Depth=4
	movzbl	(%r13), %eax
	incq	%r13
	movl	%r12d, %ecx
	shll	%cl, %eax
	orl	%eax, %r15d
	addl	$8, %r12d
	cmpl	$16, %r12d
	jl	.LBB2_251
.LBB2_260:                              # %._crit_edge692.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_246 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB2_262 Depth 5
                                        #           Child Loop BB2_269 Depth 5
                                        #           Child Loop BB2_279 Depth 5
                                        #           Child Loop BB2_290 Depth 5
                                        #           Child Loop BB2_296 Depth 5
                                        #           Child Loop BB2_307 Depth 5
	movl	%r15d, %eax
	andl	$511, %eax              # imm = 0x1FF
	movzwl	412(%r14,%rax,2), %eax
	cmpl	$288, %eax              # imm = 0x120
	jb	.LBB2_265
# BB#261:                               # %.preheader453.i.preheader
                                        #   in Loop: Header=BB2_260 Depth=4
	movl	$8, %ecx
.LBB2_262:                              # %.preheader453.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_246 Depth=3
                                        #         Parent Loop BB2_260 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cmpl	$17, %ecx
	jae	.LBB2_339
# BB#263:                               #   in Loop: Header=BB2_262 Depth=5
	movzwl	%ax, %eax
	addl	%eax, %eax
	movl	%eax, %edx
	andl	$65408, %edx            # imm = 0xFF80
	cmpl	$1152, %edx             # imm = 0x480
	jae	.LBB2_340
# BB#264:                               #   in Loop: Header=BB2_262 Depth=5
	incl	%ecx
	movl	%r15d, %edx
	shrl	%cl, %edx
	andl	$1, %edx
	andl	$65534, %eax            # imm = 0xFFFE
	orl	%edx, %eax
	movzwl	412(%r14,%rax,2), %eax
	cmpl	$287, %eax              # imm = 0x11F
	ja	.LBB2_262
.LBB2_265:                              # %.loopexit454.i
                                        #   in Loop: Header=BB2_260 Depth=4
	movzwl	%ax, %eax
	movzbl	92(%r14,%rax), %ecx
	shrl	%cl, %r15d
	subl	%ecx, %r12d
	cmpl	$255, %eax
	jbe	.LBB2_247
# BB#266:                               #   in Loop: Header=BB2_260 Depth=4
	cmpl	$256, %eax              # imm = 0x100
	je	.LBB2_334
# BB#267:                               #   in Loop: Header=BB2_260 Depth=4
	addl	$-257, %eax             # imm = 0xFEFF
	cmpl	$29, %eax
	ja	.LBB2_24
# BB#268:                               # %.preheader451.i
                                        #   in Loop: Header=BB2_260 Depth=4
	movl	%eax, %ebx
	movzbl	mszip_lit_extrabits(%rbx), %ecx
	movq	%rcx, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	cmpl	%ecx, %r12d
	jge	.LBB2_278
.LBB2_269:                              # %.lr.ph701.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_246 Depth=3
                                        #         Parent Loop BB2_260 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cmpq	%rbp, %r13
	jb	.LBB2_277
# BB#270:                               #   in Loop: Header=BB2_269 Depth=5
	movq	35752(%r14), %rax
	testq	%rax, %rax
	movq	40(%r14), %rsi
	movl	88(%r14), %edx
	je	.LBB2_272
# BB#271:                               #   in Loop: Header=BB2_269 Depth=5
	movq	35744(%r14), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB2_273
	jmp	.LBB2_338
.LBB2_272:                              #   in Loop: Header=BB2_269 Depth=5
	movl	(%r14), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB2_338
.LBB2_273:                              #   in Loop: Header=BB2_269 Depth=5
	jne	.LBB2_276
# BB#274:                               #   in Loop: Header=BB2_269 Depth=5
	cmpl	$0, 36(%r14)
	jne	.LBB2_337
# BB#275:                               #   in Loop: Header=BB2_269 Depth=5
	movq	40(%r14), %rax
	movb	$0, (%rax)
	movl	$1, 36(%r14)
	movl	$1, %eax
.LBB2_276:                              #   in Loop: Header=BB2_269 Depth=5
	movq	40(%r14), %r13
	movq	%r13, 48(%r14)
	movslq	%eax, %rbp
	addq	%r13, %rbp
	movq	%rbp, 56(%r14)
.LBB2_277:                              #   in Loop: Header=BB2_269 Depth=5
	movzbl	(%r13), %eax
	incq	%r13
	movl	%r12d, %ecx
	shll	%cl, %eax
	orl	%eax, %r15d
	addl	$8, %r12d
	cmpl	(%rsp), %r12d           # 4-byte Folded Reload
	jl	.LBB2_269
.LBB2_278:                              # %._crit_edge702.i
                                        #   in Loop: Header=BB2_260 Depth=4
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movl	%r15d, %ebx
	movq	(%rsp), %rcx            # 8-byte Reload
	shrl	%cl, %ebx
	subl	%ecx, %r12d
	cmpl	$15, %r12d
	jg	.LBB2_288
.LBB2_279:                              # %.lr.ph715.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_246 Depth=3
                                        #         Parent Loop BB2_260 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cmpq	%rbp, %r13
	jb	.LBB2_287
# BB#280:                               #   in Loop: Header=BB2_279 Depth=5
	movq	35752(%r14), %rax
	testq	%rax, %rax
	movq	40(%r14), %rsi
	movl	88(%r14), %edx
	je	.LBB2_282
# BB#281:                               #   in Loop: Header=BB2_279 Depth=5
	movq	35744(%r14), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB2_283
	jmp	.LBB2_338
.LBB2_282:                              #   in Loop: Header=BB2_279 Depth=5
	movl	(%r14), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB2_338
.LBB2_283:                              #   in Loop: Header=BB2_279 Depth=5
	jne	.LBB2_286
# BB#284:                               #   in Loop: Header=BB2_279 Depth=5
	cmpl	$0, 36(%r14)
	jne	.LBB2_337
# BB#285:                               #   in Loop: Header=BB2_279 Depth=5
	movq	40(%r14), %rax
	movb	$0, (%rax)
	movl	$1, 36(%r14)
	movl	$1, %eax
.LBB2_286:                              #   in Loop: Header=BB2_279 Depth=5
	movq	40(%r14), %r13
	movq	%r13, 48(%r14)
	movslq	%eax, %rbp
	addq	%r13, %rbp
	movq	%rbp, 56(%r14)
.LBB2_287:                              #   in Loop: Header=BB2_279 Depth=5
	movzbl	(%r13), %eax
	incq	%r13
	movl	%r12d, %ecx
	shll	%cl, %eax
	orl	%eax, %ebx
	addl	$8, %r12d
	cmpl	$16, %r12d
	jl	.LBB2_279
.LBB2_288:                              # %._crit_edge716.i
                                        #   in Loop: Header=BB2_260 Depth=4
	movl	%ebx, %eax
	andl	$63, %eax
	movzwl	2716(%r14,%rax,2), %eax
	cmpl	$32, %eax
	jb	.LBB2_293
# BB#289:                               # %.preheader448.i.preheader
                                        #   in Loop: Header=BB2_260 Depth=4
	movl	$5, %ecx
.LBB2_290:                              # %.preheader448.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_246 Depth=3
                                        #         Parent Loop BB2_260 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cmpl	$17, %ecx
	jae	.LBB2_339
# BB#291:                               #   in Loop: Header=BB2_290 Depth=5
	movzwl	%ax, %eax
	addl	%eax, %eax
	movl	%eax, %edx
	andl	$65408, %edx            # imm = 0xFF80
	cmpl	$128, %edx
	jae	.LBB2_340
# BB#292:                               #   in Loop: Header=BB2_290 Depth=5
	incl	%ecx
	movl	%ebx, %edx
	shrl	%cl, %edx
	andl	$1, %edx
	andl	$65534, %eax            # imm = 0xFFFE
	orl	%edx, %eax
	movzwl	2716(%r14,%rax,2), %eax
	cmpl	$31, %eax
	ja	.LBB2_290
.LBB2_293:                              # %.loopexit449.i
                                        #   in Loop: Header=BB2_260 Depth=4
	movzwl	%ax, %eax
	movzbl	380(%r14,%rax), %ecx
	shrl	%cl, %ebx
	cmpl	$30, %eax
	ja	.LBB2_29
# BB#294:                               # %.preheader447.i
                                        #   in Loop: Header=BB2_260 Depth=4
	subl	%ecx, %r12d
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movzbl	mszip_dist_extrabits(%rax), %esi
	cmpl	%esi, %r12d
	jge	.LBB2_304
.LBB2_296:                              # %.lr.ph725.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_246 Depth=3
                                        #         Parent Loop BB2_260 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cmpq	%rbp, %r13
	jb	.LBB2_295
# BB#297:                               #   in Loop: Header=BB2_296 Depth=5
	movq	%rsi, %rbp
	movq	35752(%r14), %rax
	testq	%rax, %rax
	movq	40(%r14), %rsi
	movl	88(%r14), %edx
	je	.LBB2_299
# BB#298:                               #   in Loop: Header=BB2_296 Depth=5
	movq	35744(%r14), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB2_300
	jmp	.LBB2_338
.LBB2_299:                              #   in Loop: Header=BB2_296 Depth=5
	movl	(%r14), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB2_338
.LBB2_300:                              #   in Loop: Header=BB2_296 Depth=5
	movq	%rbp, %rsi
	jne	.LBB2_303
# BB#301:                               #   in Loop: Header=BB2_296 Depth=5
	cmpl	$0, 36(%r14)
	jne	.LBB2_337
# BB#302:                               #   in Loop: Header=BB2_296 Depth=5
	movq	40(%r14), %rax
	movb	$0, (%rax)
	movl	$1, 36(%r14)
	movl	$1, %eax
.LBB2_303:                              #   in Loop: Header=BB2_296 Depth=5
	movq	40(%r14), %r13
	movq	%r13, 48(%r14)
	movslq	%eax, %rbp
	addq	%r13, %rbp
	movq	%rbp, 56(%r14)
.LBB2_295:                              #   in Loop: Header=BB2_296 Depth=5
	movzbl	(%r13), %eax
	incq	%r13
	movl	%r12d, %ecx
	shll	%cl, %eax
	orl	%eax, %ebx
	addl	$8, %r12d
	cmpl	%esi, %r12d
	jl	.LBB2_296
.LBB2_304:                              # %._crit_edge726.i
                                        #   in Loop: Header=BB2_260 Depth=4
	movq	(%rsp), %rax            # 8-byte Reload
	movzwl	mszip_bit_mask_tab(%rax,%rax), %edi
	andl	%r15d, %edi
	movq	32(%rsp), %rax          # 8-byte Reload
	movzwl	mszip_lit_lengths(%rax,%rax), %eax
	leal	(%rdi,%rax), %edx
	movl	%ebx, %r15d
	movl	%esi, %ecx
	shrl	%cl, %r15d
	subl	%esi, %r12d
	cmpl	$12, %edx
	jae	.LBB2_310
# BB#305:                               # %.preheader.i
                                        #   in Loop: Header=BB2_260 Depth=4
	testl	%edx, %edx
	je	.LBB2_250
# BB#306:                               # %.lr.ph741.preheader.i
                                        #   in Loop: Header=BB2_260 Depth=4
	movzwl	mszip_bit_mask_tab(%rsi,%rsi), %ecx
	andl	%ebx, %ecx
	movq	24(%rsp), %rdx          # 8-byte Reload
	movzwl	mszip_dist_offsets(%rdx,%rdx), %edx
	addl	%ecx, %edx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%esi, %ecx
	subl	%edx, %ecx
	sbbl	%ebx, %ebx
	andl	$32768, %ebx            # imm = 0x8000
	addl	%ecx, %ebx
	addl	%eax, %edi
	negl	%edi
.LBB2_307:                              # %.lr.ph741.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_246 Depth=3
                                        #         Parent Loop BB2_260 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movl	%ebx, %eax
	movzbl	2972(%r14,%rax), %eax
	movl	%esi, %ecx
	incl	%esi
	movb	%al, 2972(%r14,%rcx)
	cmpl	$32768, %esi            # imm = 0x8000
	jne	.LBB2_309
# BB#308:                               #   in Loop: Header=BB2_307 Depth=5
	movl	$32768, %esi            # imm = 0x8000
	movq	%rdi, (%rsp)            # 8-byte Spill
	movq	%r14, %rdi
	callq	*16(%r14)
	movq	(%rsp), %rdi            # 8-byte Reload
	xorl	%esi, %esi
	testl	%eax, %eax
	jne	.LBB2_31
.LBB2_309:                              # %.backedge.i
                                        #   in Loop: Header=BB2_307 Depth=5
	incl	%ebx
	andl	$32767, %ebx            # imm = 0x7FFF
	incl	%edi
	jne	.LBB2_307
	jmp	.LBB2_249
.LBB2_310:                              # %.preheader444.preheader.i
                                        #   in Loop: Header=BB2_246 Depth=3
	movzwl	mszip_bit_mask_tab(%rsi,%rsi), %eax
	andl	%ebx, %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movzwl	mszip_dist_offsets(%rcx,%rcx), %ecx
	addl	%eax, %ecx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%esi, %eax
	subl	%ecx, %eax
	sbbl	%ebx, %ebx
	andl	$32768, %ebx            # imm = 0x8000
	addl	%eax, %ebx
	jmp	.LBB2_320
.LBB2_311:                              # %vector.body.preheader
                                        #   in Loop: Header=BB2_320 Depth=4
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	-32(%rax), %rbx
	movl	%ebx, %ecx
	shrl	$5, %ecx
	incl	%ecx
	andq	$3, %rcx
	je	.LBB2_314
# BB#312:                               # %vector.body.prol.preheader
                                        #   in Loop: Header=BB2_320 Depth=4
	movq	136(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi), %r11
	leaq	(%rax,%rdi), %r8
	negq	%rcx
	xorl	%eax, %eax
.LBB2_313:                              # %vector.body.prol
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_246 Depth=3
                                        #         Parent Loop BB2_320 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movdqu	-16(%r11,%rax), %xmm0
	movups	(%r11,%rax), %xmm1
	movdqu	%xmm0, -16(%r8,%rax)
	movups	%xmm1, (%r8,%rax)
	addq	$32, %rax
	incq	%rcx
	jne	.LBB2_313
	jmp	.LBB2_315
.LBB2_314:                              #   in Loop: Header=BB2_320 Depth=4
	xorl	%eax, %eax
.LBB2_315:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB2_320 Depth=4
	cmpq	$96, %rbx
	jb	.LBB2_318
# BB#316:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB2_320 Depth=4
	movq	16(%rsp), %rcx          # 8-byte Reload
	subq	%rax, %rcx
	addq	%rax, %rsi
	movq	128(%rsp), %rbx         # 8-byte Reload
	addq	%rbx, %rsi
	addq	%rax, %rdi
	addq	%rbx, %rdi
.LBB2_317:                              # %vector.body
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_246 Depth=3
                                        #         Parent Loop BB2_320 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdi)
	movups	%xmm1, -96(%rdi)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdi)
	movups	%xmm1, -64(%rdi)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdi)
	movups	%xmm1, -32(%rdi)
	movdqu	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movdqu	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-128, %rcx
	jne	.LBB2_317
.LBB2_318:                              # %middle.block
                                        #   in Loop: Header=BB2_320 Depth=4
	cmpq	16(%rsp), %r10          # 8-byte Folded Reload
	movq	24(%rsp), %rbx          # 8-byte Reload
	movl	40(%rsp), %edi          # 4-byte Reload
	je	.LBB2_331
# BB#319:                               #   in Loop: Header=BB2_320 Depth=4
	movq	16(%rsp), %rax          # 8-byte Reload
	addq	%rax, %rdx
	addq	%rax, %r9
	movq	(%rsp), %rcx            # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	subl	%eax, %ecx
	jmp	.LBB2_326
.LBB2_320:                              # %.preheader444.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_246 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB2_313 Depth 5
                                        #           Child Loop BB2_317 Depth 5
                                        #           Child Loop BB2_328 Depth 5
                                        #           Child Loop BB2_330 Depth 5
	leal	(%rbx,%rdx), %r11d
	movl	$32768, %eax            # imm = 0x8000
	subl	%ebx, %eax
	cmpl	$32768, %r11d           # imm = 0x8000
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	cmovbel	%edx, %eax
	leal	(%rax,%rsi), %edx
	movl	$32768, %ecx            # imm = 0x8000
	subl	%esi, %ecx
	cmpl	$32768, %edx            # imm = 0x8000
	cmovbel	%eax, %ecx
	leal	(%rcx,%rsi), %edi
	movq	%rcx, (%rsp)            # 8-byte Spill
	testl	%ecx, %ecx
	movq	%rsi, %rcx
	je	.LBB2_331
# BB#321:                               # %.lr.ph737.preheader.i
                                        #   in Loop: Header=BB2_320 Depth=4
	movl	%edi, 40(%rsp)          # 4-byte Spill
	movl	%ebx, %esi
	leaq	2972(%r14,%rsi), %r9
	movl	%ecx, %edi
	leaq	2972(%r14,%rdi), %rdx
	movl	$-2, %r8d
	subl	%ecx, %r8d
	notl	%r11d
	cmpl	$-32769, %r11d          # imm = 0xFFFF7FFF
	movl	$-32769, %eax           # imm = 0xFFFF7FFF
	cmoval	%r11d, %eax
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	addl	%ebx, %eax
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	subl	%ecx, %eax
	cmpl	$-32769, %eax           # imm = 0xFFFF7FFF
	movl	$-32769, %ebx           # imm = 0xFFFF7FFF
	cmovbel	%ebx, %eax
	movl	%r8d, %r10d
	subl	%eax, %r10d
	incq	%r10
	cmpq	$32, %r10
	jb	.LBB2_325
# BB#322:                               # %min.iters.checked
                                        #   in Loop: Header=BB2_320 Depth=4
	movq	%r10, %rcx
	movabsq	$8589934560, %rax       # imm = 0x1FFFFFFE0
	andq	%rax, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	je	.LBB2_325
# BB#323:                               # %vector.memcheck
                                        #   in Loop: Header=BB2_320 Depth=4
	cmpl	$-32769, %r11d          # imm = 0xFFFF7FFF
	movl	$-32769, %eax           # imm = 0xFFFF7FFF
	cmovbel	%eax, %r11d
	addl	24(%rsp), %r11d         # 4-byte Folded Reload
	subl	8(%rsp), %r11d          # 4-byte Folded Reload
	cmpl	$-32769, %r11d          # imm = 0xFFFF7FFF
	cmovbel	%eax, %r11d
	subl	%r11d, %r8d
	leaq	(%rsi,%r8), %rcx
	addq	104(%rsp), %rcx         # 8-byte Folded Reload
	cmpq	%rcx, %rdx
	jae	.LBB2_311
# BB#324:                               # %vector.memcheck
                                        #   in Loop: Header=BB2_320 Depth=4
	addq	%rdi, %r8
	addq	104(%rsp), %r8          # 8-byte Folded Reload
	cmpq	%r8, %r9
	jae	.LBB2_311
.LBB2_325:                              #   in Loop: Header=BB2_320 Depth=4
	movq	(%rsp), %rcx            # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
.LBB2_326:                              # %.lr.ph737.i.preheader
                                        #   in Loop: Header=BB2_320 Depth=4
	leal	-1(%rcx), %esi
	movl	%ecx, %edi
	andl	$7, %edi
	je	.LBB2_329
# BB#327:                               # %.lr.ph737.i.prol.preheader
                                        #   in Loop: Header=BB2_320 Depth=4
	negl	%edi
.LBB2_328:                              # %.lr.ph737.i.prol
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_246 Depth=3
                                        #         Parent Loop BB2_320 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	decl	%ecx
	movzbl	(%r9), %eax
	incq	%r9
	movb	%al, (%rdx)
	incq	%rdx
	incl	%edi
	jne	.LBB2_328
.LBB2_329:                              # %.lr.ph737.i.prol.loopexit
                                        #   in Loop: Header=BB2_320 Depth=4
	cmpl	$7, %esi
	movq	24(%rsp), %rbx          # 8-byte Reload
	movl	40(%rsp), %edi          # 4-byte Reload
	jb	.LBB2_331
.LBB2_330:                              # %.lr.ph737.i
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        #       Parent Loop BB2_246 Depth=3
                                        #         Parent Loop BB2_320 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzbl	(%r9), %eax
	movb	%al, (%rdx)
	movzbl	1(%r9), %eax
	movb	%al, 1(%rdx)
	movzbl	2(%r9), %eax
	movb	%al, 2(%rdx)
	movzbl	3(%r9), %eax
	movb	%al, 3(%rdx)
	movzbl	4(%r9), %eax
	movb	%al, 4(%rdx)
	movzbl	5(%r9), %eax
	movb	%al, 5(%rdx)
	movzbl	6(%r9), %eax
	movb	%al, 6(%rdx)
	addl	$-8, %ecx
	movzbl	7(%r9), %eax
	leaq	8(%r9), %r9
	movb	%al, 7(%rdx)
	leaq	8(%rdx), %rdx
	jne	.LBB2_330
.LBB2_331:                              # %._crit_edge738.i
                                        #   in Loop: Header=BB2_320 Depth=4
	cmpl	$32768, %edi            # imm = 0x8000
	jne	.LBB2_333
# BB#332:                               #   in Loop: Header=BB2_320 Depth=4
	movl	$32768, %esi            # imm = 0x8000
	movq	%r14, %rdi
	callq	*16(%r14)
	xorl	%edi, %edi
	testl	%eax, %eax
	jne	.LBB2_31
.LBB2_333:                              #   in Loop: Header=BB2_320 Depth=4
	movq	(%rsp), %rax            # 8-byte Reload
	addl	%eax, %ebx
	movq	32(%rsp), %rdx          # 8-byte Reload
	subl	%eax, %edx
	cmpl	$32768, %ebx            # imm = 0x8000
	movl	$0, %eax
	cmovel	%eax, %ebx
	testl	%edx, %edx
	movl	%edi, %esi
	jne	.LBB2_320
	jmp	.LBB2_245
.LBB2_334:                              #   in Loop: Header=BB2_49 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 12(%r14)
	andl	$1, 76(%rsp)            # 4-byte Folded Spill
	je	.LBB2_49
	jmp	.LBB2_26
.LBB2_335:                              #   in Loop: Header=BB2_49 Depth=2
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movl	$-10, %esi
	testl	%esi, %esi
	je	.LBB2_241
	jmp	.LBB2_342
.LBB2_336:                              #   in Loop: Header=BB2_49 Depth=2
	movl	$-9, %esi
	testl	%esi, %esi
	je	.LBB2_241
	jmp	.LBB2_342
.LBB2_337:                              #   in Loop: Header=BB2_33 Depth=1
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB2_338:                              # %.loopexit470.i
                                        #   in Loop: Header=BB2_33 Depth=1
	movl	$-123, 24(%r14)
	movl	$-123, %esi
	jmp	.LBB2_342
.LBB2_339:                              #   in Loop: Header=BB2_33 Depth=1
	movl	$.L.str.21, %edi
	jmp	.LBB2_341
.LBB2_340:                              #   in Loop: Header=BB2_33 Depth=1
	movl	$.L.str.22, %edi
.LBB2_341:                              # %.loopexit
                                        #   in Loop: Header=BB2_33 Depth=1
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-14, %esi
.LBB2_342:                              # %.loopexit
                                        #   in Loop: Header=BB2_33 Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpl	$0, 28(%r14)
	jne	.LBB2_12
	jmp	.LBB2_347
.LBB2_343:
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB2_344:
	movl	$-123, 24(%r14)
	movl	$-123, %eax
	jmp	.LBB2_348
.LBB2_345:                              # %._crit_edge328
	testq	%rbx, %rbx
	je	.LBB2_349
.LBB2_346:                              # %._crit_edge328.thread
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB2_347:
	movl	$-124, 24(%r14)
	movl	$-124, %eax
.LBB2_348:
	addq	$760, %rsp              # imm = 0x2F8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_349:
	xorl	%eax, %eax
	jmp	.LBB2_348
.Lfunc_end2:
	.size	mszip_decompress, .Lfunc_end2-mszip_decompress
	.cfi_endproc

	.globl	mszip_free
	.p2align	4, 0x90
	.type	mszip_free,@function
mszip_free:                             # @mszip_free
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 16
.Lcfi28:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB3_1
# BB#2:
	movq	40(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB3_1:
	popq	%rbx
	retq
.Lfunc_end3:
	.size	mszip_free, .Lfunc_end3-mszip_free
	.cfi_endproc

	.globl	lzx_init
	.p2align	4, 0x90
	.type	lzx_init,@function
lzx_init:                               # @lzx_init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 80
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebp
	movl	%ecx, %ebx
	movl	%edx, %r14d
	movl	%edi, %r15d
	movl	$1, %r12d
	movl	%r14d, %ecx
	shll	%cl, %r12d
	leal	-15(%r14), %eax
	cmpl	$6, %eax
	ja	.LBB4_20
# BB#2:
	incl	%ebp
	andl	$-2, %ebp
	je	.LBB4_20
# BB#3:
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movl	$1, %edi
	movl	$54896, %esi            # imm = 0xD670
	callq	cli_calloc
	movq	%rax, %r13
	xorl	%ecx, %ecx
	testq	%r13, %r13
	je	.LBB4_20
# BB#4:                                 # %.preheader87
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	movl	$22059, %eax            # imm = 0x562B
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_5:                                # =>This Inner Loop Header: Depth=1
	movb	%cl, -3(%r13,%rax)
	movb	%cl, -2(%r13,%rax)
	cmpq	$22059, %rax            # imm = 0x562B
	setne	%sil
	cmpl	$17, %ecx
	setl	%bl
	andb	%sil, %bl
	movzbl	%bl, %esi
	addl	%ecx, %esi
	movq	%rdx, %rcx
	orq	$2, %rcx
	movb	%sil, -1(%r13,%rax)
	cmpq	$50, %rcx
	je	.LBB4_7
# BB#6:                                 #   in Loop: Header=BB4_5 Depth=1
	movb	%sil, (%r13,%rax)
.LBB4_7:                                #   in Loop: Header=BB4_5 Depth=1
	xorl	%ecx, %ecx
	cmpl	$17, %esi
	setl	%cl
	addl	%esi, %ecx
	addq	$4, %rdx
	addq	$4, %rax
	cmpq	$51, %rdx
	jl	.LBB4_5
# BB#8:                                 # %.preheader
	xorl	%edx, %edx
	movl	$22058, %eax            # imm = 0x562A
	.p2align	4, 0x90
.LBB4_9:                                # =>This Inner Loop Header: Depth=1
	movl	%edx, -66380(%r13,%rax,4)
	movzbl	-2(%r13,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	addl	%edx, %esi
	movl	%esi, -66376(%r13,%rax,4)
	movzbl	-1(%r13,%rax), %ecx
	movl	$1, %edi
	shll	%cl, %edi
	addl	%esi, %edi
	movl	%edi, -66372(%r13,%rax,4)
	movzbl	(%r13,%rax), %ecx
	movl	$1, %edx
	shll	%cl, %edx
	addl	%edi, %edx
	addq	$3, %rax
	cmpq	$22109, %rax            # imm = 0x565D
	jne	.LBB4_9
# BB#10:
	movl	%r12d, %esi
	movl	$1, %edi
	callq	cli_calloc
	movq	%rax, 32(%r13)
	testq	%rax, %rax
	movl	8(%rsp), %ebx           # 4-byte Reload
	je	.LBB4_19
# BB#11:
	movslq	%ebp, %rsi
	movl	$1, %edi
	callq	cli_calloc
	movq	%rax, 104(%r13)
	testq	%rax, %rax
	je	.LBB4_18
# BB#12:
	movl	%r15d, (%r13)
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 4(%r13)
	movb	$1, 8(%r13)
	movq	$0, 16(%r13)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 24(%r13)
	movq	80(%rsp), %rcx
	movq	%rcx, 54880(%r13)
	movq	88(%rsp), %rcx
	movq	%rcx, 54888(%r13)
	movl	%ebp, 152(%r13)
	movl	%r12d, 40(%r13)
	movl	$0, 44(%r13)
	movl	$0, 48(%r13)
	movl	$0, 52(%r13)
	movl	%ebx, 56(%r13)
	movl	$0, 80(%r13)
	movl	$0, 84(%r13)
	movb	$50, %cl
	cmpl	$21, %r14d
	je	.LBB4_15
# BB#13:
	cmpl	$20, %r14d
	movb	$42, %cl
	je	.LBB4_15
# BB#14:
	addb	%r14b, %r14b
	movl	%r14d, %ecx
.LBB4_15:
	movb	%cl, 91(%r13)
	movb	$0, 88(%r13)
	movb	$0, 92(%r13)
	movl	$0, 96(%r13)
	movq	%rax, 120(%r13)
	movq	%rax, 112(%r13)
	leaq	22107(%r13), %rax
	movq	%rax, 136(%r13)
	movq	%rax, 128(%r13)
	movl	$0, 148(%r13)
	movl	$0, 144(%r13)
	movl	$1, 60(%r13)
	movl	$1, 64(%r13)
	movl	$1, 68(%r13)
	movb	$0, 90(%r13)
	movl	$0, 76(%r13)
	movb	$0, 89(%r13)
	movq	%r13, %rdi
	addq	$240, %rdi
	xorl	%esi, %esi
	movl	$656, %edx              # imm = 0x290
	callq	memset
	xorps	%xmm0, %xmm0
	movups	%xmm0, 1194(%r13)
	movups	%xmm0, 1184(%r13)
	movups	%xmm0, 1168(%r13)
	movups	%xmm0, 1152(%r13)
	movups	%xmm0, 1136(%r13)
	movups	%xmm0, 1120(%r13)
	movups	%xmm0, 1104(%r13)
	movups	%xmm0, 1088(%r13)
	movups	%xmm0, 1072(%r13)
	movups	%xmm0, 1056(%r13)
	movups	%xmm0, 1040(%r13)
	movups	%xmm0, 1024(%r13)
	movups	%xmm0, 1008(%r13)
	movups	%xmm0, 992(%r13)
	movups	%xmm0, 976(%r13)
	movups	%xmm0, 960(%r13)
	jmp	.LBB4_21
.LBB4_18:
	movq	32(%r13), %rdi
	callq	free
.LBB4_19:
	movq	%r13, %rdi
	callq	free
.LBB4_20:
	xorl	%r13d, %r13d
.LBB4_21:
	movq	%r13, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	lzx_init, .Lfunc_end4-lzx_init
	.cfi_endproc

	.globl	lzx_set_output_length
	.p2align	4, 0x90
	.type	lzx_set_output_length,@function
lzx_set_output_length:                  # @lzx_set_output_length
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB5_2
# BB#1:
	movq	%rsi, 24(%rdi)
.LBB5_2:
	retq
.Lfunc_end5:
	.size	lzx_set_output_length, .Lfunc_end5-lzx_set_output_length
	.cfi_endproc

	.globl	lzx_decompress
	.p2align	4, 0x90
	.type	lzx_decompress,@function
lzx_decompress:                         # @lzx_decompress
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi48:
	.cfi_def_cfa_offset 336
.Lcfi49:
	.cfi_offset %rbx, -56
.Lcfi50:
	.cfi_offset %r12, -48
.Lcfi51:
	.cfi_offset %r13, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	movl	$-111, %eax
	je	.LBB6_537
# BB#1:
	testq	%r12, %r12
	js	.LBB6_537
# BB#2:
	movl	96(%rbx), %eax
	testl	%eax, %eax
	je	.LBB6_3
.LBB6_537:
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_3:
	movq	128(%rbx), %rsi
	movq	136(%rbx), %rbp
	subq	%rsi, %rbp
	movslq	%ebp, %rax
	cmpq	%r12, %rax
	cmovgq	%r12, %rbp
	testl	%ebp, %ebp
	je	.LBB6_8
# BB#4:
	cmpb	$0, 8(%rbx)
	je	.LBB6_7
# BB#5:
	movl	4(%rbx), %edi
	movl	%ebp, %edx
	callq	cli_writen
	cmpl	%ebp, %eax
	jne	.LBB6_535
# BB#6:                                 # %._crit_edge3568
	movq	128(%rbx), %rsi
.LBB6_7:
	movslq	%ebp, %rax
	addq	%rax, %rsi
	movq	%rsi, 128(%rbx)
	addq	%rax, 16(%rbx)
	subq	%rax, %r12
.LBB6_8:
	testq	%r12, %r12
	je	.LBB6_523
# BB#9:
	movq	16(%rbx), %rax
	addq	%r12, %rax
	movq	%rax, %r14
	sarq	$63, %r14
	shrq	$49, %r14
	addq	%rax, %r14
	shrq	$15, %r14
	incl	%r14d
	movl	52(%rbx), %eax
	cmpl	%r14d, %eax
	movq	%rbx, %rbp
	jae	.LBB6_56
# BB#10:                                # %.lr.ph3159
	movq	32(%rbp), %rcx
	movl	68(%rbp), %edx
	movl	%edx, 96(%rsp)          # 4-byte Spill
	movl	64(%rbp), %edx
	movl	%edx, 72(%rsp)          # 4-byte Spill
	movl	44(%rbp), %edx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movl	60(%rbp), %edx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movl	148(%rbp), %ebx
	movl	144(%rbp), %r13d
	movq	120(%rbp), %r8
	movq	112(%rbp), %r15
	leaq	22107(%rbp), %rdx
	movq	%rdx, 208(%rsp)         # 8-byte Spill
	leaq	240(%rbp), %rdx
	movq	%rdx, 224(%rsp)         # 8-byte Spill
	leaq	1554(%rbp), %rdx
	movq	%rdx, 256(%rsp)         # 8-byte Spill
	leaq	960(%rbp), %rdx
	movq	%rdx, 216(%rsp)         # 8-byte Spill
	leaq	12370(%rbp), %rdx
	movq	%rdx, 248(%rsp)         # 8-byte Spill
	leaq	1274(%rbp), %rdx
	movq	%rdx, 240(%rsp)         # 8-byte Spill
	leaq	21562(%rbp), %rdx
	movq	%rdx, 232(%rsp)         # 8-byte Spill
	leaq	1(%rcx), %rdx
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	leaq	16(%rcx), %rdx
	movq	%rdx, 200(%rsp)         # 8-byte Spill
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	leaq	112(%rcx), %rcx
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	movq	%r14, 264(%rsp)         # 8-byte Spill
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	jmp	.LBB6_11
.LBB6_17:                               #   in Loop: Header=BB6_11 Depth=1
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%r12, %r13
	leal	-1(%r14), %eax
	cmpl	$14, %eax
	ja	.LBB6_28
# BB#18:                                # %.preheader1264.preheader
                                        #   in Loop: Header=BB6_11 Depth=1
	movl	$16, %ebp
	subl	%r14d, %ebp
	.p2align	4, 0x90
.LBB6_19:                               # %.preheader1264
                                        #   Parent Loop BB6_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%r15), %rax
	cmpq	%r8, %rax
	jb	.LBB6_27
# BB#20:                                #   in Loop: Header=BB6_19 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	54888(%rcx), %rax
	testq	%rax, %rax
	movq	104(%rcx), %rsi
	movl	152(%rcx), %edx
	je	.LBB6_22
# BB#21:                                #   in Loop: Header=BB6_19 Depth=2
	movq	54880(%rcx), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB6_23
	jmp	.LBB6_515
.LBB6_22:                               #   in Loop: Header=BB6_19 Depth=2
	movl	(%rcx), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB6_515
.LBB6_23:                               #   in Loop: Header=BB6_19 Depth=2
	jne	.LBB6_26
# BB#24:                                #   in Loop: Header=BB6_19 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpb	$0, 92(%rcx)
	jne	.LBB6_514
# BB#25:                                #   in Loop: Header=BB6_19 Depth=2
	movq	104(%rcx), %rax
	movb	$0, 1(%rax)
	movq	104(%rcx), %rax
	movb	$0, (%rax)
	movb	$1, 92(%rcx)
	movl	$2, %eax
.LBB6_26:                               #   in Loop: Header=BB6_19 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	104(%rcx), %r15
	movq	%r15, 112(%rcx)
	movslq	%eax, %r8
	addq	%r15, %r8
	movq	%r8, 120(%rcx)
.LBB6_27:                               #   in Loop: Header=BB6_19 Depth=2
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	orl	%edx, %r13d
	leal	16(%r14), %ebx
	addq	$2, %r15
	addl	$-16, %ebp
	testl	%r14d, %r14d
	movl	%ebx, %r14d
	js	.LBB6_19
	jmp	.LBB6_29
.LBB6_28:                               #   in Loop: Header=BB6_11 Depth=1
	movl	%r14d, %ebx
.LBB6_29:                               # %.loopexit1266
                                        #   in Loop: Header=BB6_11 Depth=1
	movl	%ebx, %ecx
	andl	$15, %ecx
	shll	%cl, %r13d
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	128(%r14), %rax
	movq	136(%r14), %rsi
	cmpq	%rsi, %rax
	jne	.LBB6_516
# BB#30:                                #   in Loop: Header=BB6_11 Depth=1
	cmpb	$0, 88(%r14)
	movl	80(%r14), %r12d
	je	.LBB6_44
# BB#31:                                #   in Loop: Header=BB6_11 Depth=1
	testl	%r12d, %r12d
	je	.LBB6_45
# BB#32:                                #   in Loop: Header=BB6_11 Depth=1
	cmpl	$11, 48(%rsp)           # 4-byte Folded Reload
	movq	8(%rsp), %r14           # 8-byte Reload
	jb	.LBB6_46
# BB#33:                                #   in Loop: Header=BB6_11 Depth=1
	cmpl	$32769, 52(%r14)        # imm = 0x8001
	jae	.LBB6_46
# BB#34:                                #   in Loop: Header=BB6_11 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	leal	-10(%rax), %edx
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movl	84(%r14), %ebp
	movq	208(%rsp), %rdi         # 8-byte Reload
	movq	%rdi, 128(%r14)
	movl	48(%r14), %esi
	addq	32(%r14), %rsi
	movl	%eax, %edx
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	callq	memcpy
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	16(%rsp), %ecx          # 4-byte Reload
	movq	64(%rsp), %r8           # 8-byte Reload
	testl	%eax, %eax
	movq	208(%rsp), %rsi         # 8-byte Reload
	je	.LBB6_48
# BB#35:                                # %.lr.ph3148.preheader
                                        #   in Loop: Header=BB6_11 Depth=1
	leaq	22107(%r14,%rax), %rax
	movq	208(%rsp), %rdi         # 8-byte Reload
.LBB6_36:                               # %.lr.ph3148
                                        #   Parent Loop BB6_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$-24, (%rdi)
	jne	.LBB6_41
# BB#37:                                #   in Loop: Header=BB6_36 Depth=2
	movzbl	1(%rdi), %edx
	movzbl	2(%rdi), %esi
	shll	$8, %esi
	orl	%edx, %esi
	movzbl	3(%rdi), %edx
	shll	$16, %edx
	orl	%esi, %edx
	movzbl	4(%rdi), %esi
	shll	$24, %esi
	orl	%edx, %esi
	movl	%ebp, %edx
	negl	%edx
	cmpl	%edx, %esi
	jl	.LBB6_40
# BB#38:                                #   in Loop: Header=BB6_36 Depth=2
	cmpl	%r12d, %esi
	jge	.LBB6_40
# BB#39:                                #   in Loop: Header=BB6_36 Depth=2
	testl	%esi, %esi
	cmovsl	%r12d, %edx
	addl	%esi, %edx
	movb	%dl, 1(%rdi)
	movb	%dh, 2(%rdi)  # NOREX
	movl	%edx, %esi
	shrl	$16, %esi
	movb	%sil, 3(%rdi)
	shrl	$24, %edx
	movb	%dl, 4(%rdi)
.LBB6_40:                               #   in Loop: Header=BB6_36 Depth=2
	addq	$5, %rdi
	movl	$5, %edx
	jmp	.LBB6_42
.LBB6_41:                               #   in Loop: Header=BB6_36 Depth=2
	incq	%rdi
	movl	$1, %edx
.LBB6_42:                               # %.backedge
                                        #   in Loop: Header=BB6_36 Depth=2
	addl	%edx, %ebp
	cmpq	%rax, %rdi
	jb	.LBB6_36
# BB#43:                                # %.sink.split.loopexit
                                        #   in Loop: Header=BB6_11 Depth=1
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	128(%r14), %rsi
	jmp	.LBB6_47
.LBB6_44:                               #   in Loop: Header=BB6_11 Depth=1
	movl	48(%r14), %esi
	addq	32(%r14), %rsi
	movq	%rsi, 128(%r14)
	testl	%r12d, %r12d
	movq	48(%rsp), %rdi          # 8-byte Reload
	jne	.LBB6_48
	jmp	.LBB6_49
.LBB6_45:                               # %.thread
                                        #   in Loop: Header=BB6_11 Depth=1
	movq	8(%rsp), %r14           # 8-byte Reload
	movl	48(%r14), %esi
	addq	32(%r14), %rsi
	movq	%rsi, 128(%r14)
	movq	48(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB6_49
.LBB6_46:                               # %.thread3576
                                        #   in Loop: Header=BB6_11 Depth=1
	movl	48(%r14), %esi
	addq	32(%r14), %rsi
	movq	%rsi, 128(%r14)
.LBB6_47:                               # %.sink.split
                                        #   in Loop: Header=BB6_11 Depth=1
	movq	48(%rsp), %rdi          # 8-byte Reload
.LBB6_48:                               # %.sink.split
                                        #   in Loop: Header=BB6_11 Depth=1
	addl	%edi, 84(%r14)
.LBB6_49:                               #   in Loop: Header=BB6_11 Depth=1
	movl	%edi, %eax
	leaq	(%rsi,%rax), %rdx
	movq	%rdx, 136(%r14)
	movq	272(%rsp), %r12         # 8-byte Reload
	cmpq	%rax, %r12
	movl	%r12d, %ebp
	cmovgel	%edi, %ebp
	cmpb	$0, 8(%r14)
	je	.LBB6_52
# BB#50:                                #   in Loop: Header=BB6_11 Depth=1
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movl	4(%r14), %edi
	movl	%ebp, %edx
	callq	cli_writen
	cmpl	%ebp, %eax
	jne	.LBB6_520
# BB#51:                                # %._crit_edge3573
                                        #   in Loop: Header=BB6_11 Depth=1
	movq	128(%r14), %rsi
	movq	64(%rsp), %r8           # 8-byte Reload
	movl	16(%rsp), %ecx          # 4-byte Reload
.LBB6_52:                               #   in Loop: Header=BB6_11 Depth=1
	movslq	%ebp, %r9
	addq	%r9, %rsi
	movq	%rsi, 128(%r14)
	addq	%r9, 16(%r14)
	movq	48(%rsp), %rdi          # 8-byte Reload
	addl	48(%r14), %edi
	movl	%edi, 48(%r14)
	movl	52(%r14), %eax
	incl	%eax
	movl	%eax, 52(%r14)
	movl	40(%r14), %edx
	movq	40(%rsp), %rbp          # 8-byte Reload
	cmpl	%edx, %ebp
	movl	$0, %esi
	cmovel	%esi, %ebp
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	cmpl	%edx, %edi
	jne	.LBB6_54
# BB#53:                                #   in Loop: Header=BB6_11 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	$0, 48(%rdx)
.LBB6_54:                               # %.backedge1274
                                        #   in Loop: Header=BB6_11 Depth=1
	subl	%ecx, %ebx
	subq	%r9, %r12
	movq	264(%rsp), %r14         # 8-byte Reload
	cmpl	%r14d, %eax
	movq	8(%rsp), %rbp           # 8-byte Reload
	jb	.LBB6_11
	jmp	.LBB6_55
.LBB6_57:                               #   in Loop: Header=BB6_146 Depth=2
	movl	(%r13), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB6_509
.LBB6_58:                               #   in Loop: Header=BB6_146 Depth=2
	movl	%ebx, %ecx
	movq	%rbp, %rbx
	jne	.LBB6_61
# BB#59:                                #   in Loop: Header=BB6_146 Depth=2
	cmpb	$0, 92(%r13)
	jne	.LBB6_508
# BB#60:                                #   in Loop: Header=BB6_146 Depth=2
	movq	104(%r13), %rax
	movb	$0, 1(%rax)
	movq	104(%r13), %rax
	movb	$0, (%rax)
	movb	$1, 92(%r13)
	movl	$2, %eax
.LBB6_61:                               #   in Loop: Header=BB6_146 Depth=2
	movq	104(%r13), %r15
	movq	%r15, 112(%r13)
	movslq	%eax, %r8
	addq	%r15, %r8
	movq	%r8, 120(%r13)
.LBB6_62:                               #   in Loop: Header=BB6_146 Depth=2
	incq	%r15
.LBB6_63:                               # %.preheader1262
                                        #   in Loop: Header=BB6_146 Depth=2
	movl	%ecx, 112(%rsp)         # 4-byte Spill
	cmpl	$2, %r14d
	jg	.LBB6_74
# BB#64:                                # %.lr.ph2845.preheader
                                        #   in Loop: Header=BB6_146 Depth=2
	movl	$16, %ebp
	subl	%r14d, %ebp
	.p2align	4, 0x90
.LBB6_65:                               # %.lr.ph2845
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	1(%r15), %rax
	cmpq	%r8, %rax
	jb	.LBB6_73
# BB#66:                                #   in Loop: Header=BB6_65 Depth=3
	movq	54888(%r13), %rax
	testq	%rax, %rax
	movq	104(%r13), %rsi
	movl	152(%r13), %edx
	je	.LBB6_68
# BB#67:                                #   in Loop: Header=BB6_65 Depth=3
	movq	54880(%r13), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB6_69
	jmp	.LBB6_509
.LBB6_68:                               #   in Loop: Header=BB6_65 Depth=3
	movl	(%r13), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB6_509
.LBB6_69:                               #   in Loop: Header=BB6_65 Depth=3
	jne	.LBB6_72
# BB#70:                                #   in Loop: Header=BB6_65 Depth=3
	cmpb	$0, 92(%r13)
	jne	.LBB6_508
# BB#71:                                #   in Loop: Header=BB6_65 Depth=3
	movq	104(%r13), %rax
	movb	$0, 1(%rax)
	movq	104(%r13), %rax
	movb	$0, (%rax)
	movb	$1, 92(%r13)
	movl	$2, %eax
.LBB6_72:                               #   in Loop: Header=BB6_65 Depth=3
	movq	104(%r13), %r15
	movq	%r15, 112(%r13)
	movslq	%eax, %r8
	addq	%r15, %r8
	movq	%r8, 120(%r13)
.LBB6_73:                               #   in Loop: Header=BB6_65 Depth=3
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	orl	%edx, %r12d
	addl	$16, %r14d
	addq	$2, %r15
	addl	$-16, %ebp
	cmpl	$3, %r14d
	jl	.LBB6_65
.LBB6_74:                               # %._crit_edge2846
                                        #   in Loop: Header=BB6_146 Depth=2
	movl	%r12d, %eax
	shrl	$29, %eax
	movb	%al, 89(%r13)
	shll	$3, %r12d
	leal	-3(%r14), %eax
	cmpl	$15, %eax
	jg	.LBB6_86
# BB#75:                                # %.lr.ph2856.preheader
                                        #   in Loop: Header=BB6_146 Depth=2
	movl	$19, %ebp
	subl	%r14d, %ebp
	addl	$-19, %r14d
.LBB6_76:                               # %.lr.ph2856
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	1(%r15), %rax
	cmpq	%r8, %rax
	jb	.LBB6_84
# BB#77:                                #   in Loop: Header=BB6_76 Depth=3
	movq	54888(%r13), %rax
	testq	%rax, %rax
	movq	104(%r13), %rsi
	movl	152(%r13), %edx
	je	.LBB6_79
# BB#78:                                #   in Loop: Header=BB6_76 Depth=3
	movq	54880(%r13), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB6_80
	jmp	.LBB6_509
.LBB6_79:                               #   in Loop: Header=BB6_76 Depth=3
	movl	(%r13), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB6_509
.LBB6_80:                               #   in Loop: Header=BB6_76 Depth=3
	jne	.LBB6_83
# BB#81:                                #   in Loop: Header=BB6_76 Depth=3
	cmpb	$0, 92(%r13)
	jne	.LBB6_508
# BB#82:                                #   in Loop: Header=BB6_76 Depth=3
	movq	104(%r13), %rax
	movb	$0, 1(%rax)
	movq	104(%r13), %rax
	movb	$0, (%rax)
	movb	$1, 92(%r13)
	movl	$2, %eax
.LBB6_83:                               #   in Loop: Header=BB6_76 Depth=3
	movq	104(%r13), %r15
	movq	%r15, 112(%r13)
	movslq	%eax, %r8
	addq	%r15, %r8
	movq	%r8, 120(%r13)
.LBB6_84:                               #   in Loop: Header=BB6_76 Depth=3
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	orl	%edx, %r12d
	addq	$2, %r15
	addl	$-16, %ebp
	addl	$16, %r14d
	js	.LBB6_76
# BB#85:                                # %._crit_edge2857.loopexit
                                        #   in Loop: Header=BB6_146 Depth=2
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	addl	$16, %r14d
	movl	%r14d, %eax
	jmp	.LBB6_87
.LBB6_86:                               #   in Loop: Header=BB6_146 Depth=2
	movq	%rbx, 40(%rsp)          # 8-byte Spill
.LBB6_87:                               # %._crit_edge2857
                                        #   in Loop: Header=BB6_146 Depth=2
	movl	%r12d, %ebx
	shll	$16, %ebx
	leal	-16(%rax), %r14d
	cmpl	$7, %r14d
	jg	.LBB6_98
# BB#88:                                # %.lr.ph2867.preheader
                                        #   in Loop: Header=BB6_146 Depth=2
	movl	$32, %ebp
	subl	%eax, %ebp
.LBB6_89:                               # %.lr.ph2867
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	1(%r15), %rax
	cmpq	%r8, %rax
	jb	.LBB6_97
# BB#90:                                #   in Loop: Header=BB6_89 Depth=3
	movq	54888(%r13), %rax
	testq	%rax, %rax
	movq	104(%r13), %rsi
	movl	152(%r13), %edx
	je	.LBB6_92
# BB#91:                                #   in Loop: Header=BB6_89 Depth=3
	movq	54880(%r13), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB6_93
	jmp	.LBB6_509
.LBB6_92:                               #   in Loop: Header=BB6_89 Depth=3
	movl	(%r13), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB6_509
.LBB6_93:                               #   in Loop: Header=BB6_89 Depth=3
	jne	.LBB6_96
# BB#94:                                #   in Loop: Header=BB6_89 Depth=3
	cmpb	$0, 92(%r13)
	jne	.LBB6_508
# BB#95:                                #   in Loop: Header=BB6_89 Depth=3
	movq	104(%r13), %rax
	movb	$0, 1(%rax)
	movq	104(%r13), %rax
	movb	$0, (%rax)
	movb	$1, 92(%r13)
	movl	$2, %eax
.LBB6_96:                               #   in Loop: Header=BB6_89 Depth=3
	movq	104(%r13), %r15
	movq	%r15, 112(%r13)
	movslq	%eax, %r8
	addq	%r15, %r8
	movq	%r8, 120(%r13)
.LBB6_97:                               #   in Loop: Header=BB6_89 Depth=3
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	orl	%edx, %ebx
	addl	$16, %r14d
	addq	$2, %r15
	addl	$-16, %ebp
	cmpl	$8, %r14d
	jl	.LBB6_89
.LBB6_98:                               # %._crit_edge2868
                                        #   in Loop: Header=BB6_146 Depth=2
	shrl	$16, %r12d
	movq	%r12, %rax
	leal	-8(%r14), %r12d
	shldl	$8, %ebx, %eax
	shll	$8, %ebx
	movl	%eax, 72(%r13)
	movl	%eax, 76(%r13)
	movzbl	89(%r13), %esi
	cmpl	$1, %esi
	je	.LBB6_115
# BB#99:                                # %._crit_edge2868
                                        #   in Loop: Header=BB6_146 Depth=2
	cmpb	$3, %sil
	je	.LBB6_123
# BB#100:                               # %._crit_edge2868
                                        #   in Loop: Header=BB6_146 Depth=2
	cmpb	$2, %sil
	jne	.LBB6_519
# BB#101:                               # %.preheader1247.preheader
                                        #   in Loop: Header=BB6_146 Depth=2
	xorl	%r14d, %r14d
.LBB6_102:                              # %.preheader1247
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB6_104 Depth 4
	cmpl	$2, %r12d
	jg	.LBB6_113
# BB#103:                               # %.lr.ph2890.preheader
                                        #   in Loop: Header=BB6_102 Depth=3
	movl	$16, %ebp
	subl	%r12d, %ebp
.LBB6_104:                              # %.lr.ph2890
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_102 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leaq	1(%r15), %rax
	cmpq	%r8, %rax
	jb	.LBB6_112
# BB#105:                               #   in Loop: Header=BB6_104 Depth=4
	movq	54888(%r13), %rax
	testq	%rax, %rax
	movq	104(%r13), %rsi
	movl	152(%r13), %edx
	je	.LBB6_107
# BB#106:                               #   in Loop: Header=BB6_104 Depth=4
	movq	54880(%r13), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB6_108
	jmp	.LBB6_509
.LBB6_107:                              #   in Loop: Header=BB6_104 Depth=4
	movl	(%r13), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB6_509
.LBB6_108:                              #   in Loop: Header=BB6_104 Depth=4
	jne	.LBB6_111
# BB#109:                               #   in Loop: Header=BB6_104 Depth=4
	cmpb	$0, 92(%r13)
	jne	.LBB6_508
# BB#110:                               #   in Loop: Header=BB6_104 Depth=4
	movq	104(%r13), %rax
	movb	$0, 1(%rax)
	movq	104(%r13), %rax
	movb	$0, (%rax)
	movb	$1, 92(%r13)
	movl	$2, %eax
.LBB6_111:                              #   in Loop: Header=BB6_104 Depth=4
	movq	104(%r13), %r15
	movq	%r15, 112(%r13)
	movslq	%eax, %r8
	addq	%r15, %r8
	movq	%r8, 120(%r13)
.LBB6_112:                              #   in Loop: Header=BB6_104 Depth=4
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	orl	%edx, %ebx
	addl	$16, %r12d
	addq	$2, %r15
	addl	$-16, %ebp
	cmpl	$3, %r12d
	jl	.LBB6_104
.LBB6_113:                              # %._crit_edge2891
                                        #   in Loop: Header=BB6_102 Depth=3
	movl	%ebx, %eax
	shrl	$29, %eax
	shll	$3, %ebx
	addl	$-3, %r12d
	movb	%al, 1274(%r13,%r14)
	incq	%r14
	cmpq	$8, %r14
	jl	.LBB6_102
# BB#114:                               #   in Loop: Header=BB6_146 Depth=2
	movl	$8, %edi
	movl	$7, %esi
	movq	240(%rsp), %rdx         # 8-byte Reload
	movq	232(%rsp), %rcx         # 8-byte Reload
	movq	%r8, %rbp
	callq	lzx_make_decode_table
	movq	%rbp, %r8
	testl	%eax, %eax
	jne	.LBB6_528
.LBB6_115:                              #   in Loop: Header=BB6_146 Depth=2
	movq	%r15, 112(%r13)
	movq	%r8, 120(%r13)
	movl	%ebx, 144(%r13)
	movl	%r12d, 148(%r13)
	xorl	%edx, %edx
	movl	$256, %ecx              # imm = 0x100
	movq	%r13, %rdi
	movq	224(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rsi
	callq	lzx_read_lens
	testl	%eax, %eax
	jne	.LBB6_524
# BB#116:                               #   in Loop: Header=BB6_146 Depth=2
	movzbl	91(%r13), %eax
	leal	256(,%rax,8), %ecx
	movl	$256, %edx              # imm = 0x100
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	lzx_read_lens
	testl	%eax, %eax
	movq	48(%rsp), %rbp          # 8-byte Reload
	jne	.LBB6_524
# BB#117:                               #   in Loop: Header=BB6_146 Depth=2
	movups	112(%r13), %xmm0
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movl	144(%r13), %r14d
	movl	148(%r13), %r15d
	movl	$656, %edi              # imm = 0x290
	movl	$12, %esi
	movq	%rbx, %rdx
	movq	256(%rsp), %rcx         # 8-byte Reload
	callq	lzx_make_decode_table
	testl	%eax, %eax
	jne	.LBB6_525
# BB#118:                               #   in Loop: Header=BB6_146 Depth=2
	cmpb	$0, 472(%r13)
	movq	216(%rsp), %rbx         # 8-byte Reload
	je	.LBB6_120
# BB#119:                               #   in Loop: Header=BB6_146 Depth=2
	movb	$1, 88(%r13)
.LBB6_120:                              #   in Loop: Header=BB6_146 Depth=2
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	movups	%xmm0, 112(%r13)
	movl	%r14d, 144(%r13)
	movl	%r15d, 148(%r13)
	xorl	%edx, %edx
	movl	$249, %ecx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	lzx_read_lens
	testl	%eax, %eax
	jne	.LBB6_526
# BB#121:                               #   in Loop: Header=BB6_146 Depth=2
	movq	112(%r13), %r15
	movq	120(%r13), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	144(%r13), %r12d
	movl	148(%r13), %r14d
	movl	$250, %edi
	movl	$12, %esi
	movq	%rbx, %rdx
	movq	248(%rsp), %rcx         # 8-byte Reload
	callq	lzx_make_decode_table
	movq	64(%rsp), %r8           # 8-byte Reload
	testl	%eax, %eax
	movl	112(%rsp), %ecx         # 4-byte Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
	je	.LBB6_147
	jmp	.LBB6_122
.LBB6_123:                              #   in Loop: Header=BB6_146 Depth=2
	movb	$1, 88(%r13)
	cmpl	$15, %r12d
	jg	.LBB6_135
# BB#124:                               # %.lr.ph2877.preheader
                                        #   in Loop: Header=BB6_146 Depth=2
	addl	$-24, %r14d
.LBB6_125:                              # %.lr.ph2877
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	1(%r15), %rax
	cmpq	%r8, %rax
	jb	.LBB6_133
# BB#126:                               #   in Loop: Header=BB6_125 Depth=3
	movq	54888(%r13), %rax
	testq	%rax, %rax
	movq	104(%r13), %rsi
	movl	152(%r13), %edx
	je	.LBB6_128
# BB#127:                               #   in Loop: Header=BB6_125 Depth=3
	movq	54880(%r13), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB6_129
	jmp	.LBB6_509
.LBB6_128:                              #   in Loop: Header=BB6_125 Depth=3
	movl	(%r13), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB6_509
.LBB6_129:                              #   in Loop: Header=BB6_125 Depth=3
	jne	.LBB6_132
# BB#130:                               #   in Loop: Header=BB6_125 Depth=3
	cmpb	$0, 92(%r13)
	jne	.LBB6_508
# BB#131:                               #   in Loop: Header=BB6_125 Depth=3
	movq	104(%r13), %rax
	movb	$0, 1(%rax)
	movq	104(%r13), %rax
	movb	$0, (%rax)
	movb	$1, 92(%r13)
	movl	$2, %eax
.LBB6_132:                              #   in Loop: Header=BB6_125 Depth=3
	movq	104(%r13), %r15
	movq	%r15, 112(%r13)
	movslq	%eax, %r8
	addq	%r15, %r8
	movq	%r8, 120(%r13)
.LBB6_133:                              #   in Loop: Header=BB6_125 Depth=3
	addq	$2, %r15
	addl	$16, %r14d
	js	.LBB6_125
# BB#134:                               # %._crit_edge2878.loopexit
                                        #   in Loop: Header=BB6_146 Depth=2
	addl	$16, %r14d
	movl	%r14d, %r12d
.LBB6_135:                              # %._crit_edge2878
                                        #   in Loop: Header=BB6_146 Depth=2
	leaq	-2(%r15), %rax
	cmpl	$16, %r12d
	cmovneq	%rax, %r15
	xorl	%ebx, %ebx
	movq	48(%rsp), %rbp          # 8-byte Reload
.LBB6_136:                              #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%r8, %r15
	jne	.LBB6_144
# BB#137:                               #   in Loop: Header=BB6_136 Depth=3
	movq	54888(%r13), %rax
	testq	%rax, %rax
	movq	104(%r13), %rsi
	movl	152(%r13), %edx
	je	.LBB6_139
# BB#138:                               #   in Loop: Header=BB6_136 Depth=3
	movq	54880(%r13), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB6_140
	jmp	.LBB6_509
.LBB6_139:                              #   in Loop: Header=BB6_136 Depth=3
	movl	(%r13), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB6_509
.LBB6_140:                              #   in Loop: Header=BB6_136 Depth=3
	jne	.LBB6_143
# BB#141:                               #   in Loop: Header=BB6_136 Depth=3
	cmpb	$0, 92(%r13)
	jne	.LBB6_508
# BB#142:                               #   in Loop: Header=BB6_136 Depth=3
	movq	104(%r13), %rax
	movb	$0, 1(%rax)
	movq	104(%r13), %rax
	movb	$0, (%rax)
	movb	$1, 92(%r13)
	movl	$2, %eax
.LBB6_143:                              #   in Loop: Header=BB6_136 Depth=3
	movq	104(%r13), %r15
	movq	%r15, 112(%r13)
	movslq	%eax, %r8
	addq	%r15, %r8
	movq	%r8, 120(%r13)
.LBB6_144:                              #   in Loop: Header=BB6_136 Depth=3
	movzbl	(%r15), %eax
	incq	%r15
	movb	%al, 156(%rsp,%rbx)
	incq	%rbx
	cmpl	$12, %ebx
	jl	.LBB6_136
# BB#145:                               #   in Loop: Header=BB6_146 Depth=2
	movzbl	156(%rsp), %eax
	movzbl	157(%rsp), %ecx
	shll	$8, %ecx
	orl	%eax, %ecx
	movzbl	158(%rsp), %eax
	shll	$16, %eax
	orl	%ecx, %eax
	movzbl	159(%rsp), %ecx
	shll	$24, %ecx
	orl	%eax, %ecx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movzbl	160(%rsp), %eax
	movzbl	161(%rsp), %ecx
	shll	$8, %ecx
	orl	%eax, %ecx
	movzbl	162(%rsp), %eax
	shll	$16, %eax
	orl	%ecx, %eax
	movzbl	163(%rsp), %ecx
	shll	$24, %ecx
	orl	%eax, %ecx
	movl	%ecx, 72(%rsp)          # 4-byte Spill
	movzbl	164(%rsp), %eax
	movzbl	165(%rsp), %ecx
	shll	$8, %ecx
	orl	%eax, %ecx
	movzbl	166(%rsp), %eax
	shll	$16, %eax
	orl	%ecx, %eax
	movzbl	167(%rsp), %ecx
	shll	$24, %ecx
	orl	%eax, %ecx
	movl	%ecx, 96(%rsp)          # 4-byte Spill
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	movl	112(%rsp), %ecx         # 4-byte Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB6_147
.LBB6_349:                              #   in Loop: Header=BB6_146 Depth=2
	movl	%eax, %edi
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	movl	56(%rsp), %eax          # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	40(%rsp), %eax          # 4-byte Reload
	movl	%eax, 72(%rsp)          # 4-byte Spill
	testl	%edi, %edi
	jns	.LBB6_461
	jmp	.LBB6_459
.LBB6_171:                              #   in Loop: Header=BB6_146 Depth=2
	movl	%edi, %ebx
	movl	88(%rsp), %eax          # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	40(%rsp), %eax          # 4-byte Reload
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	%rsi, %rdi
	movq	16(%rsp), %r12          # 8-byte Reload
	testl	%edi, %edi
	jns	.LBB6_461
	jmp	.LBB6_459
.LBB6_146:                              # %.lr.ph3135
                                        #   Parent Loop BB6_11 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_65 Depth 3
                                        #       Child Loop BB6_76 Depth 3
                                        #       Child Loop BB6_89 Depth 3
                                        #       Child Loop BB6_102 Depth 3
                                        #         Child Loop BB6_104 Depth 4
                                        #       Child Loop BB6_125 Depth 3
                                        #       Child Loop BB6_136 Depth 3
                                        #       Child Loop BB6_151 Depth 3
                                        #         Child Loop BB6_152 Depth 4
                                        #           Child Loop BB6_154 Depth 5
                                        #           Child Loop BB6_166 Depth 5
                                        #         Child Loop BB6_175 Depth 4
                                        #         Child Loop BB6_187 Depth 4
                                        #         Child Loop BB6_245 Depth 4
                                        #         Child Loop BB6_214 Depth 4
                                        #         Child Loop BB6_257 Depth 4
                                        #         Child Loop BB6_201 Depth 4
                                        #         Child Loop BB6_226 Depth 4
                                        #         Child Loop BB6_238 Depth 4
                                        #         Child Loop BB6_287 Depth 4
                                        #         Child Loop BB6_291 Depth 4
                                        #         Child Loop BB6_295 Depth 4
                                        #         Child Loop BB6_276 Depth 4
                                        #         Child Loop BB6_299 Depth 4
                                        #         Child Loop BB6_303 Depth 4
                                        #         Child Loop BB6_312 Depth 4
                                        #         Child Loop BB6_316 Depth 4
                                        #         Child Loop BB6_320 Depth 4
                                        #       Child Loop BB6_447 Depth 3
                                        #       Child Loop BB6_329 Depth 3
                                        #         Child Loop BB6_330 Depth 4
                                        #           Child Loop BB6_332 Depth 5
                                        #           Child Loop BB6_344 Depth 5
                                        #         Child Loop BB6_353 Depth 4
                                        #         Child Loop BB6_365 Depth 4
                                        #         Child Loop BB6_374 Depth 4
                                        #         Child Loop BB6_409 Depth 4
                                        #         Child Loop BB6_413 Depth 4
                                        #         Child Loop BB6_417 Depth 4
                                        #         Child Loop BB6_398 Depth 4
                                        #         Child Loop BB6_421 Depth 4
                                        #         Child Loop BB6_425 Depth 4
                                        #         Child Loop BB6_434 Depth 4
                                        #         Child Loop BB6_438 Depth 4
                                        #         Child Loop BB6_442 Depth 4
	cmpl	$0, 76(%r13)
	je	.LBB6_322
.LBB6_147:                              #   in Loop: Header=BB6_146 Depth=2
	movl	76(%r13), %eax
	cmpl	%ecx, %eax
	movl	%eax, %edx
	cmovgl	%ecx, %edx
	subl	%edx, %eax
	movl	%eax, 76(%r13)
	movb	89(%r13), %al
	cmpb	$1, %al
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	je	.LBB6_327
# BB#148:                               #   in Loop: Header=BB6_146 Depth=2
	cmpb	$3, %al
	je	.LBB6_444
# BB#149:                               #   in Loop: Header=BB6_146 Depth=2
	cmpb	$2, %al
	jne	.LBB6_513
# BB#150:                               # %.preheader1251
                                        #   in Loop: Header=BB6_146 Depth=2
	testl	%edx, %edx
	movl	%edx, %esi
	movl	%edx, %edi
	jle	.LBB6_458
.LBB6_151:                              # %.preheader1245.lr.ph
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB6_152 Depth 4
                                        #           Child Loop BB6_154 Depth 5
                                        #           Child Loop BB6_166 Depth 5
                                        #         Child Loop BB6_175 Depth 4
                                        #         Child Loop BB6_187 Depth 4
                                        #         Child Loop BB6_245 Depth 4
                                        #         Child Loop BB6_214 Depth 4
                                        #         Child Loop BB6_257 Depth 4
                                        #         Child Loop BB6_201 Depth 4
                                        #         Child Loop BB6_226 Depth 4
                                        #         Child Loop BB6_238 Depth 4
                                        #         Child Loop BB6_287 Depth 4
                                        #         Child Loop BB6_291 Depth 4
                                        #         Child Loop BB6_295 Depth 4
                                        #         Child Loop BB6_276 Depth 4
                                        #         Child Loop BB6_299 Depth 4
                                        #         Child Loop BB6_303 Depth 4
                                        #         Child Loop BB6_312 Depth 4
                                        #         Child Loop BB6_316 Depth 4
                                        #         Child Loop BB6_320 Depth 4
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movl	%ebx, %edx
	movq	56(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 88(%rsp)          # 4-byte Spill
	movl	72(%rsp), %eax          # 4-byte Reload
	movl	%eax, 40(%rsp)          # 4-byte Spill
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	movq	%rdx, 184(%rsp)         # 8-byte Spill
	movl	%edx, %edi
	movq	%rsi, %r12
	movl	%ecx, 112(%rsp)         # 4-byte Spill
.LBB6_152:                              # %.preheader1245
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_151 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB6_154 Depth 5
                                        #           Child Loop BB6_166 Depth 5
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	movl	%eax, 128(%rsp)         # 4-byte Spill
	cmpl	$15, %r14d
	jg	.LBB6_163
# BB#153:                               # %.lr.ph2922.preheader
                                        #   in Loop: Header=BB6_152 Depth=4
	movl	$16, %ebp
	subl	%r14d, %ebp
.LBB6_154:                              # %.lr.ph2922
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_151 Depth=3
                                        #         Parent Loop BB6_152 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	leaq	1(%r15), %rax
	cmpq	%r8, %rax
	jb	.LBB6_162
# BB#155:                               #   in Loop: Header=BB6_154 Depth=5
	movq	54888(%r13), %rax
	testq	%rax, %rax
	movq	104(%r13), %rsi
	movl	152(%r13), %edx
	je	.LBB6_157
# BB#156:                               #   in Loop: Header=BB6_154 Depth=5
	movq	54880(%r13), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB6_158
	jmp	.LBB6_509
.LBB6_157:                              #   in Loop: Header=BB6_154 Depth=5
	movl	(%r13), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB6_509
.LBB6_158:                              #   in Loop: Header=BB6_154 Depth=5
	jne	.LBB6_161
# BB#159:                               #   in Loop: Header=BB6_154 Depth=5
	cmpb	$0, 92(%r13)
	jne	.LBB6_508
# BB#160:                               #   in Loop: Header=BB6_154 Depth=5
	movq	104(%r13), %rax
	movb	$0, 1(%rax)
	movq	104(%r13), %rax
	movb	$0, (%rax)
	movb	$1, 92(%r13)
	movl	$2, %eax
.LBB6_161:                              #   in Loop: Header=BB6_154 Depth=5
	movq	104(%r13), %r15
	movq	%r15, 112(%r13)
	movslq	%eax, %r8
	addq	%r15, %r8
	movq	%r8, 120(%r13)
.LBB6_162:                              #   in Loop: Header=BB6_154 Depth=5
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	movq	16(%rsp), %rax          # 8-byte Reload
	orl	%edx, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leal	16(%r14), %eax
	addq	$2, %r15
	addl	$-16, %ebp
	testl	%r14d, %r14d
	movl	%eax, %r14d
	js	.LBB6_154
	jmp	.LBB6_164
.LBB6_163:                              #   in Loop: Header=BB6_152 Depth=4
	movl	%r14d, %eax
.LBB6_164:                              # %._crit_edge2923
                                        #   in Loop: Header=BB6_152 Depth=4
	movq	16(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	shrl	$20, %ecx
	movzwl	1554(%r13,%rcx,2), %edx
	cmpl	$656, %edx              # imm = 0x290
	jb	.LBB6_169
# BB#165:                               # %.preheader1243.preheader
                                        #   in Loop: Header=BB6_152 Depth=4
	movl	$1048576, %ecx          # imm = 0x100000
.LBB6_166:                              # %.preheader1243
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_151 Depth=3
                                        #         Parent Loop BB6_152 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	sarl	%ecx
	je	.LBB6_510
# BB#167:                               #   in Loop: Header=BB6_166 Depth=5
	addl	%edx, %edx
	movzwl	%dx, %esi
	cmpl	$5408, %esi             # imm = 0x1520
	jae	.LBB6_511
# BB#168:                               #   in Loop: Header=BB6_166 Depth=5
	xorl	%esi, %esi
	testl	16(%rsp), %ecx          # 4-byte Folded Reload
	setne	%sil
	orl	%esi, %edx
	movzwl	%dx, %edx
	movzwl	1554(%r13,%rdx,2), %edx
	cmpl	$655, %edx              # imm = 0x28F
	ja	.LBB6_166
.LBB6_169:                              # %.loopexit1244
                                        #   in Loop: Header=BB6_152 Depth=4
	movq	%rbx, 136(%rsp)         # 8-byte Spill
	movzwl	%dx, %ebx
	movzbl	240(%r13,%rbx), %ebp
	movl	%ebp, %ecx
	movq	16(%rsp), %rsi          # 8-byte Reload
	shll	%cl, %esi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	%eax, %r14d
	subl	%ebp, %r14d
	cmpl	$255, %ebx
	ja	.LBB6_172
# BB#170:                               #   in Loop: Header=BB6_152 Depth=4
	movq	80(%rsp), %rdi          # 8-byte Reload
	movl	%edi, %eax
	incl	%edi
	movq	104(%rsp), %rcx         # 8-byte Reload
	movb	%dl, (%rcx,%rax)
	leal	-1(%r12), %esi
	movl	128(%rsp), %eax         # 4-byte Reload
	incl	%eax
	movq	136(%rsp), %rbx         # 8-byte Reload
	decl	%ebx
	cmpl	$1, %r12d
	movl	%esi, %r12d
	movl	112(%rsp), %ecx         # 4-byte Reload
	movq	168(%rsp), %rdx         # 8-byte Reload
	jg	.LBB6_152
	jmp	.LBB6_171
.LBB6_172:                              #   in Loop: Header=BB6_151 Depth=3
	addl	$-256, %ebx
	movl	%ebx, %edi
	andl	$7, %edi
	cmpl	$7, %edi
	movq	%r12, 176(%rsp)         # 8-byte Spill
	jne	.LBB6_191
# BB#173:                               # %.preheader1241
                                        #   in Loop: Header=BB6_151 Depth=3
	cmpl	$15, %r14d
	jg	.LBB6_185
# BB#174:                               # %.lr.ph2956.preheader
                                        #   in Loop: Header=BB6_151 Depth=3
	leal	-16(%rax), %r14d
	subl	%ebp, %r14d
	addl	$16, %ebp
	subl	%eax, %ebp
.LBB6_175:                              # %.lr.ph2956
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_151 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leaq	1(%r15), %rax
	cmpq	%r8, %rax
	jb	.LBB6_183
# BB#176:                               #   in Loop: Header=BB6_175 Depth=4
	movq	54888(%r13), %rax
	testq	%rax, %rax
	movq	104(%r13), %rsi
	movl	152(%r13), %edx
	je	.LBB6_178
# BB#177:                               #   in Loop: Header=BB6_175 Depth=4
	movq	54880(%r13), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB6_179
	jmp	.LBB6_509
.LBB6_178:                              #   in Loop: Header=BB6_175 Depth=4
	movl	(%r13), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB6_509
.LBB6_179:                              #   in Loop: Header=BB6_175 Depth=4
	jne	.LBB6_182
# BB#180:                               #   in Loop: Header=BB6_175 Depth=4
	cmpb	$0, 92(%r13)
	jne	.LBB6_508
# BB#181:                               #   in Loop: Header=BB6_175 Depth=4
	movq	104(%r13), %rax
	movb	$0, 1(%rax)
	movq	104(%r13), %rax
	movb	$0, (%rax)
	movb	$1, 92(%r13)
	movl	$2, %eax
.LBB6_182:                              #   in Loop: Header=BB6_175 Depth=4
	movq	104(%r13), %r15
	movq	%r15, 112(%r13)
	movslq	%eax, %r8
	addq	%r15, %r8
	movq	%r8, 120(%r13)
.LBB6_183:                              #   in Loop: Header=BB6_175 Depth=4
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	movq	16(%rsp), %rax          # 8-byte Reload
	orl	%edx, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	addq	$2, %r15
	addl	$-16, %ebp
	addl	$16, %r14d
	js	.LBB6_175
# BB#184:                               # %._crit_edge2957.loopexit
                                        #   in Loop: Header=BB6_151 Depth=3
	addl	$16, %r14d
.LBB6_185:                              # %._crit_edge2957
                                        #   in Loop: Header=BB6_151 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	shrl	$20, %eax
	movzwl	12370(%r13,%rax,2), %eax
	cmpl	$250, %eax
	jb	.LBB6_190
# BB#186:                               # %.preheader1239.preheader
                                        #   in Loop: Header=BB6_151 Depth=3
	movl	$1048576, %ecx          # imm = 0x100000
.LBB6_187:                              # %.preheader1239
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_151 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	sarl	%ecx
	je	.LBB6_510
# BB#188:                               #   in Loop: Header=BB6_187 Depth=4
	addl	%eax, %eax
	movzwl	%ax, %edx
	cmpl	$4596, %edx             # imm = 0x11F4
	jae	.LBB6_511
# BB#189:                               #   in Loop: Header=BB6_187 Depth=4
	xorl	%edx, %edx
	testl	16(%rsp), %ecx          # 4-byte Folded Reload
	setne	%dl
	orl	%edx, %eax
	movzwl	%ax, %eax
	movzwl	12370(%r13,%rax,2), %eax
	cmpl	$249, %eax
	ja	.LBB6_187
.LBB6_190:                              # %.loopexit1240
                                        #   in Loop: Header=BB6_151 Depth=3
	movzwl	%ax, %edi
	movzbl	960(%r13,%rdi), %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	shll	%cl, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	subl	%ecx, %r14d
	addl	$7, %edi
.LBB6_191:                              #   in Loop: Header=BB6_151 Depth=3
	sarl	$3, %ebx
	movq	%r8, 64(%rsp)           # 8-byte Spill
	je	.LBB6_195
# BB#192:                               #   in Loop: Header=BB6_151 Depth=3
	cmpl	$2, %ebx
	je	.LBB6_197
# BB#193:                               #   in Loop: Header=BB6_151 Depth=3
	cmpl	$1, %ebx
	jne	.LBB6_198
# BB#194:                               #   in Loop: Header=BB6_151 Depth=3
	movl	40(%rsp), %eax          # 4-byte Reload
	movl	%eax, %esi
	movl	88(%rsp), %eax          # 4-byte Reload
	jmp	.LBB6_196
.LBB6_195:                              #   in Loop: Header=BB6_151 Depth=3
	movl	88(%rsp), %eax          # 4-byte Reload
	movl	%eax, %esi
	movl	40(%rsp), %eax          # 4-byte Reload
.LBB6_196:                              #   in Loop: Header=BB6_151 Depth=3
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movq	80(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB6_265
.LBB6_197:                              #   in Loop: Header=BB6_151 Depth=3
	movl	96(%rsp), %eax          # 4-byte Reload
	movl	%eax, %esi
	movl	40(%rsp), %eax          # 4-byte Reload
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movl	88(%rsp), %eax          # 4-byte Reload
	movl	%eax, 96(%rsp)          # 4-byte Spill
	movq	80(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB6_265
.LBB6_198:                              #   in Loop: Header=BB6_151 Depth=3
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	movl	%ebx, %eax
	movzbl	22056(%r13,%rax), %ecx
	movl	21852(%r13,%rax,4), %eax
	addl	$-2, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%rcx, %r12
	cmpl	$4, %ecx
	jb	.LBB6_210
# BB#199:                               #   in Loop: Header=BB6_151 Depth=3
	leal	-3(%r12), %eax
	movl	%eax, %r13d
	cmpl	%eax, %r14d
	jge	.LBB6_223
# BB#200:                               # %.lr.ph2987.preheader
                                        #   in Loop: Header=BB6_151 Depth=3
	movl	$16, %ebp
	subl	%r14d, %ebp
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
.LBB6_201:                              # %.lr.ph2987
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_151 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leaq	1(%r15), %rax
	cmpq	%rsi, %rax
	jb	.LBB6_209
# BB#202:                               #   in Loop: Header=BB6_201 Depth=4
	movq	54888(%rbx), %rax
	testq	%rax, %rax
	movq	104(%rbx), %rsi
	movl	152(%rbx), %edx
	je	.LBB6_204
# BB#203:                               #   in Loop: Header=BB6_201 Depth=4
	movq	54880(%rbx), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB6_205
	jmp	.LBB6_535
.LBB6_204:                              #   in Loop: Header=BB6_201 Depth=4
	movl	(%rbx), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB6_535
.LBB6_205:                              #   in Loop: Header=BB6_201 Depth=4
	jne	.LBB6_208
# BB#206:                               #   in Loop: Header=BB6_201 Depth=4
	cmpb	$0, 92(%rbx)
	jne	.LBB6_534
# BB#207:                               #   in Loop: Header=BB6_201 Depth=4
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	104(%rbx), %rax
	movb	$0, 1(%rax)
	movq	104(%rbx), %rax
	movb	$0, (%rax)
	movb	$1, 92(%rbx)
	movl	$2, %eax
.LBB6_208:                              #   in Loop: Header=BB6_201 Depth=4
	movq	104(%rbx), %r15
	movq	%r15, 112(%rbx)
	movslq	%eax, %rsi
	addq	%r15, %rsi
	movq	%rsi, 120(%rbx)
.LBB6_209:                              #   in Loop: Header=BB6_201 Depth=4
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	movq	16(%rsp), %rax          # 8-byte Reload
	orl	%edx, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	addl	$16, %r14d
	addq	$2, %r15
	addl	$-16, %ebp
	cmpl	%r13d, %r14d
	jl	.LBB6_201
	jmp	.LBB6_224
.LBB6_210:                              #   in Loop: Header=BB6_151 Depth=3
	testb	%r12b, %r12b
	je	.LBB6_242
# BB#211:                               #   in Loop: Header=BB6_151 Depth=3
	cmpb	$3, %r12b
	jne	.LBB6_243
# BB#212:                               # %.preheader1237
                                        #   in Loop: Header=BB6_151 Depth=3
	cmpl	$15, %r14d
	jg	.LBB6_254
# BB#213:                               # %.lr.ph2966.preheader
                                        #   in Loop: Header=BB6_151 Depth=3
	movl	$16, %ebp
	subl	%r14d, %ebp
	movq	64(%rsp), %rsi          # 8-byte Reload
.LBB6_214:                              # %.lr.ph2966
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_151 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leaq	1(%r15), %rax
	cmpq	%rsi, %rax
	jb	.LBB6_222
# BB#215:                               #   in Loop: Header=BB6_214 Depth=4
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	54888(%rcx), %rax
	testq	%rax, %rax
	movq	104(%rcx), %rsi
	movl	152(%rcx), %edx
	je	.LBB6_217
# BB#216:                               #   in Loop: Header=BB6_214 Depth=4
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	54880(%rcx), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB6_218
	jmp	.LBB6_515
.LBB6_217:                              #   in Loop: Header=BB6_214 Depth=4
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB6_515
.LBB6_218:                              #   in Loop: Header=BB6_214 Depth=4
	jne	.LBB6_221
# BB#219:                               #   in Loop: Header=BB6_214 Depth=4
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpb	$0, 92(%rax)
	jne	.LBB6_514
# BB#220:                               #   in Loop: Header=BB6_214 Depth=4
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	104(%rcx), %rax
	movb	$0, 1(%rax)
	movq	104(%rcx), %rax
	movb	$0, (%rax)
	movb	$1, 92(%rcx)
	movl	$2, %eax
.LBB6_221:                              #   in Loop: Header=BB6_214 Depth=4
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	104(%rcx), %r15
	movq	%r15, 112(%rcx)
	movslq	%eax, %rsi
	addq	%r15, %rsi
	movq	%rsi, 120(%rcx)
.LBB6_222:                              #   in Loop: Header=BB6_214 Depth=4
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	movq	16(%rsp), %rax          # 8-byte Reload
	orl	%edx, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leal	16(%r14), %eax
	addq	$2, %r15
	addl	$-16, %ebp
	testl	%r14d, %r14d
	movl	%eax, %r14d
	js	.LBB6_214
	jmp	.LBB6_255
.LBB6_223:                              #   in Loop: Header=BB6_151 Depth=3
	movq	64(%rsp), %rsi          # 8-byte Reload
.LBB6_224:                              # %._crit_edge2988
                                        #   in Loop: Header=BB6_151 Depth=3
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movl	$35, %ecx
	subl	%r12d, %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movl	%r13d, %ecx
	shll	%cl, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%r14d, %ebx
	subl	%r13d, %ebx
	cmpl	$15, %ebx
	jg	.LBB6_236
# BB#225:                               # %.lr.ph2998.preheader
                                        #   in Loop: Header=BB6_151 Depth=3
	leal	-13(%r14), %ebx
	subl	%r12d, %ebx
	addl	$13, %r12d
	subl	%r14d, %r12d
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
.LBB6_226:                              # %.lr.ph2998
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_151 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leaq	1(%r15), %rax
	cmpq	%rsi, %rax
	jb	.LBB6_234
# BB#227:                               #   in Loop: Header=BB6_226 Depth=4
	movq	54888(%rdi), %rax
	testq	%rax, %rax
	movq	104(%rdi), %rsi
	movl	152(%rdi), %edx
	je	.LBB6_229
# BB#228:                               #   in Loop: Header=BB6_226 Depth=4
	movq	54880(%rdi), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB6_230
	jmp	.LBB6_515
.LBB6_229:                              #   in Loop: Header=BB6_226 Depth=4
	movl	(%rdi), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB6_515
.LBB6_230:                              #   in Loop: Header=BB6_226 Depth=4
	movq	8(%rsp), %rdi           # 8-byte Reload
	jne	.LBB6_233
# BB#231:                               #   in Loop: Header=BB6_226 Depth=4
	cmpb	$0, 92(%rdi)
	jne	.LBB6_514
# BB#232:                               #   in Loop: Header=BB6_226 Depth=4
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	104(%rdi), %rax
	movb	$0, 1(%rax)
	movq	104(%rdi), %rax
	movb	$0, (%rax)
	movb	$1, 92(%rdi)
	movl	$2, %eax
.LBB6_233:                              #   in Loop: Header=BB6_226 Depth=4
	movq	104(%rdi), %r15
	movq	%r15, 112(%rdi)
	movslq	%eax, %rsi
	addq	%r15, %rsi
	movq	%rsi, 120(%rdi)
.LBB6_234:                              #   in Loop: Header=BB6_226 Depth=4
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%r12d, %ecx
	shll	%cl, %edx
	movq	16(%rsp), %rax          # 8-byte Reload
	orl	%edx, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	addq	$2, %r15
	addl	$-16, %r12d
	addl	$16, %ebx
	js	.LBB6_226
# BB#235:                               # %._crit_edge2999.loopexit
                                        #   in Loop: Header=BB6_151 Depth=3
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	addl	$16, %ebx
.LBB6_236:                              # %._crit_edge2999
                                        #   in Loop: Header=BB6_151 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	shrl	$25, %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	21562(%rcx,%rax,2), %eax
	cmpl	$8, %eax
	jb	.LBB6_241
# BB#237:                               # %.preheader1229.preheader
                                        #   in Loop: Header=BB6_151 Depth=3
	movl	$33554432, %ecx         # imm = 0x2000000
.LBB6_238:                              # %.preheader1229
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_151 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	sarl	%ecx
	je	.LBB6_531
# BB#239:                               #   in Loop: Header=BB6_238 Depth=4
	addl	%eax, %eax
	movzwl	%ax, %edx
	cmpl	$144, %edx
	jae	.LBB6_532
# BB#240:                               #   in Loop: Header=BB6_238 Depth=4
	xorl	%edx, %edx
	testl	16(%rsp), %ecx          # 4-byte Folded Reload
	setne	%dl
	orl	%edx, %eax
	movzwl	%ax, %eax
	movq	8(%rsp), %rdx           # 8-byte Reload
	movzwl	21562(%rdx,%rax,2), %eax
	cmpl	$7, %eax
	ja	.LBB6_238
.LBB6_241:                              # %.loopexit1230
                                        #   in Loop: Header=BB6_151 Depth=3
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	72(%rsp), %rcx          # 8-byte Reload
	leal	(%r12,%rcx,8), %r12d
	movq	%r12, %rsi
	movzwl	%ax, %eax
	movq	8(%rsp), %r13           # 8-byte Reload
	movzbl	1274(%r13,%rax), %ecx
	movq	16(%rsp), %rdx          # 8-byte Reload
	shll	%cl, %edx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	subl	%ecx, %ebx
	addl	%eax, %esi
	movl	88(%rsp), %eax          # 4-byte Reload
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movl	40(%rsp), %eax          # 4-byte Reload
	movl	%eax, 96(%rsp)          # 4-byte Spill
	movl	%ebx, %r14d
	jmp	.LBB6_264
.LBB6_242:                              #   in Loop: Header=BB6_151 Depth=3
	movl	$1, %esi
	jmp	.LBB6_263
.LBB6_243:                              # %.preheader1233
                                        #   in Loop: Header=BB6_151 Depth=3
	cmpl	%r12d, %r14d
	jge	.LBB6_261
# BB#244:                               # %.lr.ph2976.preheader
                                        #   in Loop: Header=BB6_151 Depth=3
	movl	$16, %ebp
	subl	%r14d, %ebp
	movq	64(%rsp), %rsi          # 8-byte Reload
.LBB6_245:                              # %.lr.ph2976
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_151 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leaq	1(%r15), %rax
	cmpq	%rsi, %rax
	jb	.LBB6_253
# BB#246:                               #   in Loop: Header=BB6_245 Depth=4
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	54888(%rcx), %rax
	testq	%rax, %rax
	movq	104(%rcx), %rsi
	movl	152(%rcx), %edx
	je	.LBB6_248
# BB#247:                               #   in Loop: Header=BB6_245 Depth=4
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	54880(%rcx), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB6_249
	jmp	.LBB6_515
.LBB6_248:                              #   in Loop: Header=BB6_245 Depth=4
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB6_515
.LBB6_249:                              #   in Loop: Header=BB6_245 Depth=4
	jne	.LBB6_252
# BB#250:                               #   in Loop: Header=BB6_245 Depth=4
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpb	$0, 92(%rax)
	jne	.LBB6_514
# BB#251:                               #   in Loop: Header=BB6_245 Depth=4
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	104(%rcx), %rax
	movb	$0, 1(%rax)
	movq	104(%rcx), %rax
	movb	$0, (%rax)
	movb	$1, 92(%rcx)
	movl	$2, %eax
.LBB6_252:                              #   in Loop: Header=BB6_245 Depth=4
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	104(%rcx), %r15
	movq	%r15, 112(%rcx)
	movslq	%eax, %rsi
	addq	%r15, %rsi
	movq	%rsi, 120(%rcx)
.LBB6_253:                              #   in Loop: Header=BB6_245 Depth=4
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	movq	16(%rsp), %rax          # 8-byte Reload
	orl	%edx, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	addl	$16, %r14d
	addq	$2, %r15
	addl	$-16, %ebp
	cmpl	%r12d, %r14d
	jl	.LBB6_245
	jmp	.LBB6_262
.LBB6_254:                              #   in Loop: Header=BB6_151 Depth=3
	movl	%r14d, %eax
	movq	64(%rsp), %rsi          # 8-byte Reload
.LBB6_255:                              # %._crit_edge2967
                                        #   in Loop: Header=BB6_151 Depth=3
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	shrl	$25, %ecx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movzwl	21562(%rdx,%rcx,2), %ecx
	cmpl	$8, %ecx
	jb	.LBB6_260
# BB#256:                               # %.preheader1235.preheader
                                        #   in Loop: Header=BB6_151 Depth=3
	movl	$33554432, %edx         # imm = 0x2000000
.LBB6_257:                              # %.preheader1235
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_151 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	sarl	%edx
	je	.LBB6_531
# BB#258:                               #   in Loop: Header=BB6_257 Depth=4
	addl	%ecx, %ecx
	movzwl	%cx, %esi
	cmpl	$144, %esi
	jae	.LBB6_532
# BB#259:                               #   in Loop: Header=BB6_257 Depth=4
	xorl	%esi, %esi
	testl	16(%rsp), %edx          # 4-byte Folded Reload
	setne	%sil
	orl	%esi, %ecx
	movzwl	%cx, %ecx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movzwl	21562(%rsi,%rcx,2), %ecx
	cmpl	$7, %ecx
	ja	.LBB6_257
.LBB6_260:                              # %.loopexit1236
                                        #   in Loop: Header=BB6_151 Depth=3
	movzwl	%cx, %edx
	movq	8(%rsp), %r13           # 8-byte Reload
	movzbl	1274(%r13,%rdx), %ecx
	movq	16(%rsp), %rsi          # 8-byte Reload
	shll	%cl, %esi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	subl	%ecx, %eax
	movq	56(%rsp), %rsi          # 8-byte Reload
	addl	%edx, %esi
	movl	88(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 72(%rsp)          # 4-byte Spill
	movl	40(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 96(%rsp)          # 4-byte Spill
	movl	%eax, %r14d
	jmp	.LBB6_264
.LBB6_261:                              #   in Loop: Header=BB6_151 Depth=3
	movq	64(%rsp), %rsi          # 8-byte Reload
.LBB6_262:                              # %._crit_edge2977
                                        #   in Loop: Header=BB6_151 Depth=3
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movl	$32, %ecx
	subl	%r12d, %ecx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	%edx, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	movl	%r12d, %ecx
	shll	%cl, %edx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	subl	%r12d, %r14d
	addl	56(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, %esi
.LBB6_263:                              #   in Loop: Header=BB6_151 Depth=3
	movl	88(%rsp), %eax          # 4-byte Reload
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movl	40(%rsp), %eax          # 4-byte Reload
	movl	%eax, 96(%rsp)          # 4-byte Spill
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB6_264:                              #   in Loop: Header=BB6_151 Depth=3
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	120(%rsp), %rdi         # 8-byte Reload
.LBB6_265:                              #   in Loop: Header=BB6_151 Depth=3
	leal	2(%rdi), %r10d
	leal	(%r10,%rdx), %eax
	movl	40(%r13), %r8d
	cmpl	%r8d, %eax
	ja	.LBB6_527
# BB#266:                               #   in Loop: Header=BB6_151 Depth=3
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%edx, %r11d
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11), %rcx
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movl	%esi, %ebp
	subl	%edx, %ebp
	jbe	.LBB6_277
# BB#267:                               #   in Loop: Header=BB6_151 Depth=3
	movl	%r8d, %eax
	subl	%ebp, %eax
	jl	.LBB6_530
# BB#268:                               #   in Loop: Header=BB6_151 Depth=3
	movq	104(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax), %rdx
	movl	%r10d, %esi
	subl	%ebp, %esi
	jle	.LBB6_280
# BB#269:                               #   in Loop: Header=BB6_151 Depth=3
	movq	%r10, %r13
	testl	%ebp, %ebp
	jle	.LBB6_304
# BB#270:                               # %.lr.ph3013.preheader
                                        #   in Loop: Header=BB6_151 Depth=3
	movq	80(%rsp), %r12          # 8-byte Reload
	leal	-1(%r12), %r9d
	movq	56(%rsp), %r10          # 8-byte Reload
	subl	%r10d, %r9d
	cmpl	$-3, %r9d
	movl	$-2, %ebx
	cmovgl	%r9d, %ebx
	leal	1(%r10,%rbx), %ebx
	subl	%r12d, %ebx
	incq	%rbx
	movq	%rbx, 88(%rsp)          # 8-byte Spill
	cmpq	$31, %rbx
	jbe	.LBB6_302
# BB#271:                               # %min.iters.checked4501
                                        #   in Loop: Header=BB6_151 Depth=3
	movq	88(%rsp), %rdi          # 8-byte Reload
	movabsq	$8589934560, %rbx       # imm = 0x1FFFFFFE0
	andq	%rbx, %rdi
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	je	.LBB6_302
# BB#272:                               # %vector.memcheck4515
                                        #   in Loop: Header=BB6_151 Depth=3
	cmpl	$-3, %r9d
	movl	$-2, %ebx
	cmovlel	%ebx, %r9d
	movq	56(%rsp), %rbx          # 8-byte Reload
	leal	1(%rbx,%r9), %r9d
	subl	80(%rsp), %r9d          # 4-byte Folded Reload
	addq	%r9, %rax
	addq	144(%rsp), %rax         # 8-byte Folded Reload
	cmpq	%rax, %rcx
	jae	.LBB6_274
# BB#273:                               # %vector.memcheck4515
                                        #   in Loop: Header=BB6_151 Depth=3
	addq	%r11, %r9
	addq	144(%rsp), %r9          # 8-byte Folded Reload
	cmpq	%r9, %rdx
	jb	.LBB6_302
.LBB6_274:                              # %vector.body4496.preheader
                                        #   in Loop: Header=BB6_151 Depth=3
	movq	120(%rsp), %rax         # 8-byte Reload
	leaq	-32(%rax), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	shrl	$5, %eax
	incl	%eax
	testb	$3, %al
	je	.LBB6_296
# BB#275:                               # %vector.body4496.prol.preheader
                                        #   in Loop: Header=BB6_151 Depth=3
	movq	184(%rsp), %rdi         # 8-byte Reload
	leal	(%rdi,%r8), %ebx
	movq	56(%rsp), %r12          # 8-byte Reload
	subl	%r12d, %ebx
	addl	128(%rsp), %ebx         # 4-byte Folded Reload
	movq	200(%rsp), %rax         # 8-byte Reload
	addq	%rax, %rbx
	leaq	(%r11,%rax), %r10
	leal	-1(%rdi), %r9d
	subl	%r12d, %r9d
	addl	128(%rsp), %r9d         # 4-byte Folded Reload
	cmpl	$-3, %r9d
	movl	$-2, %eax
	cmovlel	%eax, %r9d
	leal	1(%r12), %eax
	subl	%edi, %eax
	addl	136(%rsp), %eax         # 4-byte Folded Reload
	leal	1(%r9,%rax), %r9d
	andl	$96, %r9d
	addl	$-32, %r9d
	shrl	$5, %r9d
	incl	%r9d
	andl	$3, %r9d
	negq	%r9
	xorl	%eax, %eax
.LBB6_276:                              # %vector.body4496.prol
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_151 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	-16(%rbx,%rax), %xmm0
	movups	(%rbx,%rax), %xmm1
	movups	%xmm0, -16(%r10,%rax)
	movups	%xmm1, (%r10,%rax)
	addq	$32, %rax
	incq	%r9
	jne	.LBB6_276
	jmp	.LBB6_297
.LBB6_277:                              #   in Loop: Header=BB6_151 Depth=3
	cmpl	$-1, %edi
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	jl	.LBB6_321
# BB#278:                               # %.lr.ph3008.preheader
                                        #   in Loop: Header=BB6_151 Depth=3
	movq	%r10, %r8
	movl	56(%rsp), %esi          # 4-byte Reload
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	movl	$-3, %eax
	subl	%edi, %eax
	cmpl	$-3, %eax
	movl	$-2, %ebp
	cmovgl	%eax, %ebp
	movq	%rdi, %rbx
	leal	3(%rdi,%rbp), %r10d
	incq	%r10
	cmpq	$32, %r10
	jae	.LBB6_281
# BB#279:                               #   in Loop: Header=BB6_151 Depth=3
	movl	%r8d, %eax
	jmp	.LBB6_294
.LBB6_280:                              #   in Loop: Header=BB6_151 Depth=3
	movl	%r10d, %esi
	jmp	.LBB6_305
.LBB6_281:                              # %min.iters.checked4543
                                        #   in Loop: Header=BB6_151 Depth=3
	movq	%r10, %r9
	movabsq	$8589934560, %rbp       # imm = 0x1FFFFFFE0
	andq	%rbp, %r9
	je	.LBB6_284
# BB#282:                               # %vector.memcheck4557
                                        #   in Loop: Header=BB6_151 Depth=3
	cmpl	$-3, %eax
	movl	$-2, %ebp
	cmovlel	%ebp, %eax
	leal	3(%rbx,%rax), %ebp
	addq	%r11, %rbp
	movq	%rbp, %rax
	subq	%rsi, %rax
	addq	144(%rsp), %rax         # 8-byte Folded Reload
	cmpq	%rax, %rcx
	jae	.LBB6_285
# BB#283:                               # %vector.memcheck4557
                                        #   in Loop: Header=BB6_151 Depth=3
	addq	144(%rsp), %rbp         # 8-byte Folded Reload
	cmpq	%rbp, %rdx
	jae	.LBB6_285
.LBB6_284:                              #   in Loop: Header=BB6_151 Depth=3
	movl	%r8d, %eax
.LBB6_294:                              # %.lr.ph3008.preheader4586
                                        #   in Loop: Header=BB6_151 Depth=3
	incl	%eax
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	%r8, %r10
.LBB6_295:                              # %.lr.ph3008
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_151 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rdx), %ebx
	incq	%rdx
	movb	%bl, (%rcx)
	incq	%rcx
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB6_295
	jmp	.LBB6_321
.LBB6_285:                              # %vector.body4538.preheader
                                        #   in Loop: Header=BB6_151 Depth=3
	negq	%rsi
	leaq	-32(%r9), %rdi
	movl	%edi, %eax
	shrl	$5, %eax
	incl	%eax
	andq	$3, %rax
	je	.LBB6_288
# BB#286:                               # %vector.body4538.prol.preheader
                                        #   in Loop: Header=BB6_151 Depth=3
	movq	200(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rsi), %r12
	addq	%r11, %r12
	leaq	(%rbx,%r11), %rbp
	negq	%rax
	xorl	%ebx, %ebx
.LBB6_287:                              # %vector.body4538.prol
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_151 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	-16(%r12,%rbx), %xmm0
	movups	(%r12,%rbx), %xmm1
	movups	%xmm0, -16(%rbp,%rbx)
	movups	%xmm1, (%rbp,%rbx)
	addq	$32, %rbx
	incq	%rax
	jne	.LBB6_287
	jmp	.LBB6_289
.LBB6_288:                              #   in Loop: Header=BB6_151 Depth=3
	xorl	%ebx, %ebx
.LBB6_289:                              # %vector.body4538.prol.loopexit
                                        #   in Loop: Header=BB6_151 Depth=3
	cmpq	$96, %rdi
	jb	.LBB6_292
# BB#290:                               # %vector.body4538.preheader.new
                                        #   in Loop: Header=BB6_151 Depth=3
	movq	%r9, %rax
	subq	%rbx, %rax
	addq	192(%rsp), %rbx         # 8-byte Folded Reload
	addq	%rbx, %r11
.LBB6_291:                              # %vector.body4538
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_151 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	-112(%rsi,%r11), %xmm0
	movups	-96(%rsi,%r11), %xmm1
	movups	%xmm0, -112(%r11)
	movups	%xmm1, -96(%r11)
	movups	-80(%rsi,%r11), %xmm0
	movups	-64(%rsi,%r11), %xmm1
	movups	%xmm0, -80(%r11)
	movups	%xmm1, -64(%r11)
	movups	-48(%rsi,%r11), %xmm0
	movups	-32(%rsi,%r11), %xmm1
	movups	%xmm0, -48(%r11)
	movups	%xmm1, -32(%r11)
	movups	-16(%rsi,%r11), %xmm0
	movups	(%rsi,%r11), %xmm1
	movups	%xmm0, -16(%r11)
	movups	%xmm1, (%r11)
	subq	$-128, %r11
	addq	$-128, %rax
	jne	.LBB6_291
.LBB6_292:                              # %middle.block4539
                                        #   in Loop: Header=BB6_151 Depth=3
	cmpq	%r9, %r10
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	%r8, %r10
	je	.LBB6_321
# BB#293:                               #   in Loop: Header=BB6_151 Depth=3
	movl	%r10d, %eax
	subl	%r9d, %eax
	addq	%r9, %rcx
	addq	%r9, %rdx
	jmp	.LBB6_294
.LBB6_296:                              #   in Loop: Header=BB6_151 Depth=3
	xorl	%eax, %eax
.LBB6_297:                              # %vector.body4496.prol.loopexit
                                        #   in Loop: Header=BB6_151 Depth=3
	cmpq	$96, 80(%rsp)           # 8-byte Folded Reload
	movq	136(%rsp), %r12         # 8-byte Reload
	jb	.LBB6_300
# BB#298:                               # %vector.body4496.preheader.new
                                        #   in Loop: Header=BB6_151 Depth=3
	movq	184(%rsp), %rdi         # 8-byte Reload
	leal	-1(%rdi), %r9d
	movq	56(%rsp), %r10          # 8-byte Reload
	subl	%r10d, %r9d
	addl	128(%rsp), %r9d         # 4-byte Folded Reload
	cmpl	$-3, %r9d
	movl	$-2, %ebx
	cmovlel	%ebx, %r9d
	leal	1(%r10), %ebx
	subl	%edi, %ebx
	addl	%ebx, %r12d
	addl	%r9d, %r12d
	incq	%r12
	movabsq	$8589934560, %rbx       # imm = 0x1FFFFFFE0
	andq	%rbx, %r12
	subq	%rax, %r12
	addq	192(%rsp), %rax         # 8-byte Folded Reload
	addl	%edi, %r8d
	subl	%r10d, %r8d
	addl	128(%rsp), %r8d         # 4-byte Folded Reload
	addq	%rax, %r8
	addq	%rax, %r11
.LBB6_299:                              # %vector.body4496
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_151 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	-112(%r8), %xmm0
	movups	-96(%r8), %xmm1
	movups	%xmm0, -112(%r11)
	movups	%xmm1, -96(%r11)
	movups	-80(%r8), %xmm0
	movups	-64(%r8), %xmm1
	movups	%xmm0, -80(%r11)
	movups	%xmm1, -64(%r11)
	movups	-48(%r8), %xmm0
	movups	-32(%r8), %xmm1
	movups	%xmm0, -48(%r11)
	movups	%xmm1, -32(%r11)
	movups	-16(%r8), %xmm0
	movups	(%r8), %xmm1
	movups	%xmm0, -16(%r11)
	movups	%xmm1, (%r11)
	subq	$-128, %r8
	subq	$-128, %r11
	addq	$-128, %r12
	jne	.LBB6_299
.LBB6_300:                              # %middle.block4497
                                        #   in Loop: Header=BB6_151 Depth=3
	movq	120(%rsp), %rax         # 8-byte Reload
	addq	%rax, %rcx
	cmpq	%rax, 88(%rsp)          # 8-byte Folded Reload
	je	.LBB6_304
# BB#301:                               #   in Loop: Header=BB6_151 Depth=3
	movq	120(%rsp), %rax         # 8-byte Reload
	addq	%rax, %rdx
	subl	%eax, %ebp
.LBB6_302:                              # %.lr.ph3013.preheader4585
                                        #   in Loop: Header=BB6_151 Depth=3
	incl	%ebp
.LBB6_303:                              # %.lr.ph3013
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_151 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rdx), %eax
	incq	%rdx
	movb	%al, (%rcx)
	incq	%rcx
	decl	%ebp
	cmpl	$1, %ebp
	jg	.LBB6_303
.LBB6_304:                              #   in Loop: Header=BB6_151 Depth=3
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	%r13, %r10
.LBB6_305:                              # %.preheader1226
                                        #   in Loop: Header=BB6_151 Depth=3
	testl	%esi, %esi
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
	jle	.LBB6_321
# BB#306:                               # %.lr.ph3018.preheader
                                        #   in Loop: Header=BB6_151 Depth=3
	movq	%r10, %r9
	movl	%esi, %eax
	notl	%eax
	cmpl	$-3, %eax
	movl	$-2, %edi
	cmovgl	%eax, %edi
	leal	1(%rsi,%rdi), %edi
	incq	%rdi
	cmpq	$31, %rdi
	jbe	.LBB6_319
# BB#307:                               # %min.iters.checked4459
                                        #   in Loop: Header=BB6_151 Depth=3
	movq	%rdi, %r8
	movabsq	$8589934560, %rbp       # imm = 0x1FFFFFFE0
	andq	%rbp, %r8
	je	.LBB6_319
# BB#308:                               # %vector.memcheck4473
                                        #   in Loop: Header=BB6_151 Depth=3
	cmpl	$-3, %eax
	movl	$-2, %ebp
	cmovlel	%ebp, %eax
	leal	1(%rsi,%rax), %eax
	leaq	1(%rdx,%rax), %rbp
	cmpq	%rbp, %rcx
	jae	.LBB6_310
# BB#309:                               # %vector.memcheck4473
                                        #   in Loop: Header=BB6_151 Depth=3
	leaq	1(%rcx,%rax), %rax
	cmpq	%rax, %rdx
	jb	.LBB6_319
.LBB6_310:                              # %vector.body4454.preheader
                                        #   in Loop: Header=BB6_151 Depth=3
	leaq	-32(%r8), %rbx
	movl	%ebx, %eax
	shrl	$5, %eax
	incl	%eax
	andq	$3, %rax
	je	.LBB6_313
# BB#311:                               # %vector.body4454.prol.preheader
                                        #   in Loop: Header=BB6_151 Depth=3
	negq	%rax
	xorl	%ebp, %ebp
.LBB6_312:                              # %vector.body4454.prol
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_151 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	(%rdx,%rbp), %xmm0
	movups	16(%rdx,%rbp), %xmm1
	movups	%xmm0, (%rcx,%rbp)
	movups	%xmm1, 16(%rcx,%rbp)
	addq	$32, %rbp
	incq	%rax
	jne	.LBB6_312
	jmp	.LBB6_314
.LBB6_313:                              #   in Loop: Header=BB6_151 Depth=3
	xorl	%ebp, %ebp
.LBB6_314:                              # %vector.body4454.prol.loopexit
                                        #   in Loop: Header=BB6_151 Depth=3
	cmpq	$96, %rbx
	jb	.LBB6_317
# BB#315:                               # %vector.body4454.preheader.new
                                        #   in Loop: Header=BB6_151 Depth=3
	movq	%r8, %rax
	subq	%rbp, %rax
	leaq	112(%rdx,%rbp), %rbx
	leaq	112(%rcx,%rbp), %rbp
.LBB6_316:                              # %vector.body4454
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_151 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rbp)
	movups	%xmm1, -96(%rbp)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rbp)
	movups	%xmm1, -64(%rbp)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rbp)
	movups	%xmm1, -32(%rbp)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rbp)
	movups	%xmm1, (%rbp)
	subq	$-128, %rbx
	subq	$-128, %rbp
	addq	$-128, %rax
	jne	.LBB6_316
.LBB6_317:                              # %middle.block4455
                                        #   in Loop: Header=BB6_151 Depth=3
	cmpq	%r8, %rdi
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	%r9, %r10
	je	.LBB6_321
# BB#318:                               #   in Loop: Header=BB6_151 Depth=3
	subl	%r8d, %esi
	addq	%r8, %rcx
	addq	%r8, %rdx
.LBB6_319:                              # %.lr.ph3018.preheader4584
                                        #   in Loop: Header=BB6_151 Depth=3
	incl	%esi
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	%r9, %r10
.LBB6_320:                              # %.lr.ph3018
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_151 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rdx), %eax
	incq	%rdx
	movb	%al, (%rcx)
	incq	%rcx
	decl	%esi
	cmpl	$1, %esi
	jg	.LBB6_320
.LBB6_321:                              # %.outer1253
                                        #   in Loop: Header=BB6_151 Depth=3
	movq	176(%rsp), %rax         # 8-byte Reload
	subl	%r10d, %eax
	movq	%rax, %rsi
	movl	%eax, %edi
	movl	112(%rsp), %ecx         # 4-byte Reload
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	168(%rsp), %rdx         # 8-byte Reload
	jg	.LBB6_151
	jmp	.LBB6_458
.LBB6_322:                              #   in Loop: Header=BB6_146 Depth=2
	cmpb	$3, 89(%r13)
	jne	.LBB6_63
# BB#323:                               #   in Loop: Header=BB6_146 Depth=2
	testb	$1, 72(%r13)
	je	.LBB6_63
# BB#324:                               #   in Loop: Header=BB6_146 Depth=2
	cmpq	%r8, %r15
	jne	.LBB6_62
# BB#325:                               #   in Loop: Header=BB6_146 Depth=2
	movq	%rbx, %rbp
	movl	%ecx, %ebx
	movq	54888(%r13), %rax
	testq	%rax, %rax
	movq	104(%r13), %rsi
	movl	152(%r13), %edx
	je	.LBB6_57
# BB#326:                               #   in Loop: Header=BB6_146 Depth=2
	movq	54880(%r13), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB6_58
	jmp	.LBB6_509
.LBB6_327:                              # %.preheader1249
                                        #   in Loop: Header=BB6_146 Depth=2
	testl	%edx, %edx
	jle	.LBB6_456
# BB#328:                               # %.preheader1224.lr.ph.preheader
                                        #   in Loop: Header=BB6_146 Depth=2
	movl	%edx, %edi
	movl	%ecx, 112(%rsp)         # 4-byte Spill
.LBB6_329:                              # %.preheader1224.lr.ph
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB6_330 Depth 4
                                        #           Child Loop BB6_332 Depth 5
                                        #           Child Loop BB6_344 Depth 5
                                        #         Child Loop BB6_353 Depth 4
                                        #         Child Loop BB6_365 Depth 4
                                        #         Child Loop BB6_374 Depth 4
                                        #         Child Loop BB6_409 Depth 4
                                        #         Child Loop BB6_413 Depth 4
                                        #         Child Loop BB6_417 Depth 4
                                        #         Child Loop BB6_398 Depth 4
                                        #         Child Loop BB6_421 Depth 4
                                        #         Child Loop BB6_425 Depth 4
                                        #         Child Loop BB6_434 Depth 4
                                        #         Child Loop BB6_438 Depth 4
                                        #         Child Loop BB6_442 Depth 4
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	movl	%ebx, %edx
	movq	56(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 56(%rsp)          # 4-byte Spill
	movl	72(%rsp), %eax          # 4-byte Reload
	movl	%eax, 40(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	$0, 136(%rsp)           # 4-byte Folded Spill
	movq	%rdx, 88(%rsp)          # 8-byte Spill
.LBB6_330:                              # %.preheader1224
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_329 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB6_332 Depth 5
                                        #           Child Loop BB6_344 Depth 5
	cmpl	$15, %r14d
	jg	.LBB6_341
# BB#331:                               # %.lr.ph3041.preheader
                                        #   in Loop: Header=BB6_330 Depth=4
	movl	$16, %ebp
	subl	%r14d, %ebp
.LBB6_332:                              # %.lr.ph3041
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_329 Depth=3
                                        #         Parent Loop BB6_330 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	leaq	1(%r15), %rax
	cmpq	%r8, %rax
	jb	.LBB6_340
# BB#333:                               #   in Loop: Header=BB6_332 Depth=5
	movq	54888(%r13), %rax
	testq	%rax, %rax
	movq	104(%r13), %rsi
	movl	152(%r13), %edx
	je	.LBB6_335
# BB#334:                               #   in Loop: Header=BB6_332 Depth=5
	movq	54880(%r13), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB6_336
	jmp	.LBB6_509
.LBB6_335:                              #   in Loop: Header=BB6_332 Depth=5
	movl	(%r13), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB6_509
.LBB6_336:                              #   in Loop: Header=BB6_332 Depth=5
	jne	.LBB6_339
# BB#337:                               #   in Loop: Header=BB6_332 Depth=5
	cmpb	$0, 92(%r13)
	jne	.LBB6_508
# BB#338:                               #   in Loop: Header=BB6_332 Depth=5
	movq	104(%r13), %rax
	movb	$0, 1(%rax)
	movq	104(%r13), %rax
	movb	$0, (%rax)
	movb	$1, 92(%r13)
	movl	$2, %eax
.LBB6_339:                              #   in Loop: Header=BB6_332 Depth=5
	movq	104(%r13), %r15
	movq	%r15, 112(%r13)
	movslq	%eax, %r8
	addq	%r15, %r8
	movq	%r8, 120(%r13)
.LBB6_340:                              #   in Loop: Header=BB6_332 Depth=5
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	orl	%edx, %r12d
	leal	16(%r14), %eax
	addq	$2, %r15
	addl	$-16, %ebp
	testl	%r14d, %r14d
	movl	%eax, %r14d
	js	.LBB6_332
	jmp	.LBB6_342
.LBB6_341:                              #   in Loop: Header=BB6_330 Depth=4
	movl	%r14d, %eax
.LBB6_342:                              # %._crit_edge3042
                                        #   in Loop: Header=BB6_330 Depth=4
	movl	%r12d, %ecx
	shrl	$20, %ecx
	movzwl	1554(%r13,%rcx,2), %edx
	cmpl	$656, %edx              # imm = 0x290
	jb	.LBB6_347
# BB#343:                               # %.preheader1222.preheader
                                        #   in Loop: Header=BB6_330 Depth=4
	movl	$1048576, %ecx          # imm = 0x100000
.LBB6_344:                              # %.preheader1222
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_329 Depth=3
                                        #         Parent Loop BB6_330 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	sarl	%ecx
	je	.LBB6_510
# BB#345:                               #   in Loop: Header=BB6_344 Depth=5
	addl	%edx, %edx
	movzwl	%dx, %esi
	cmpl	$5408, %esi             # imm = 0x1520
	jae	.LBB6_511
# BB#346:                               #   in Loop: Header=BB6_344 Depth=5
	xorl	%esi, %esi
	testl	%r12d, %ecx
	setne	%sil
	orl	%esi, %edx
	movzwl	%dx, %edx
	movzwl	1554(%r13,%rdx,2), %edx
	cmpl	$655, %edx              # imm = 0x28F
	ja	.LBB6_344
.LBB6_347:                              # %.loopexit1223
                                        #   in Loop: Header=BB6_330 Depth=4
	movq	%r12, %rsi
	movzwl	%dx, %r12d
	movzbl	240(%r13,%r12), %ebp
	movl	%ebp, %ecx
	shll	%cl, %esi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	%eax, %r14d
	subl	%ebp, %r14d
	cmpl	$255, %r12d
	ja	.LBB6_350
# BB#348:                               #   in Loop: Header=BB6_330 Depth=4
	movl	%ebx, %eax
	incl	%ebx
	movq	104(%rsp), %rcx         # 8-byte Reload
	movb	%dl, (%rcx,%rax)
	movq	80(%rsp), %rcx          # 8-byte Reload
	leal	-1(%rcx), %eax
	incl	136(%rsp)               # 4-byte Folded Spill
	movq	128(%rsp), %rdx         # 8-byte Reload
	decl	%edx
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	cmpl	$1, %ecx
	movl	%eax, %ecx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	112(%rsp), %ecx         # 4-byte Reload
	movq	168(%rsp), %rdx         # 8-byte Reload
	jg	.LBB6_330
	jmp	.LBB6_349
.LBB6_350:                              #   in Loop: Header=BB6_329 Depth=3
	addl	$-256, %r12d
	movl	%r12d, %esi
	andl	$7, %esi
	cmpl	$7, %esi
	jne	.LBB6_369
# BB#351:                               # %.preheader1220
                                        #   in Loop: Header=BB6_329 Depth=3
	cmpl	$15, %r14d
	jg	.LBB6_363
# BB#352:                               # %.lr.ph3075.preheader
                                        #   in Loop: Header=BB6_329 Depth=3
	leal	-16(%rax), %r14d
	subl	%ebp, %r14d
	addl	$16, %ebp
	subl	%eax, %ebp
.LBB6_353:                              # %.lr.ph3075
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_329 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leaq	1(%r15), %rax
	cmpq	%r8, %rax
	jb	.LBB6_361
# BB#354:                               #   in Loop: Header=BB6_353 Depth=4
	movq	54888(%r13), %rax
	testq	%rax, %rax
	movq	104(%r13), %rsi
	movl	152(%r13), %edx
	je	.LBB6_356
# BB#355:                               #   in Loop: Header=BB6_353 Depth=4
	movq	54880(%r13), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB6_357
	jmp	.LBB6_509
.LBB6_356:                              #   in Loop: Header=BB6_353 Depth=4
	movl	(%r13), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB6_509
.LBB6_357:                              #   in Loop: Header=BB6_353 Depth=4
	jne	.LBB6_360
# BB#358:                               #   in Loop: Header=BB6_353 Depth=4
	cmpb	$0, 92(%r13)
	jne	.LBB6_508
# BB#359:                               #   in Loop: Header=BB6_353 Depth=4
	movq	104(%r13), %rax
	movb	$0, 1(%rax)
	movq	104(%r13), %rax
	movb	$0, (%rax)
	movb	$1, 92(%r13)
	movl	$2, %eax
.LBB6_360:                              #   in Loop: Header=BB6_353 Depth=4
	movq	104(%r13), %r15
	movq	%r15, 112(%r13)
	movslq	%eax, %r8
	addq	%r15, %r8
	movq	%r8, 120(%r13)
.LBB6_361:                              #   in Loop: Header=BB6_353 Depth=4
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	movq	16(%rsp), %rax          # 8-byte Reload
	orl	%edx, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	addq	$2, %r15
	addl	$-16, %ebp
	addl	$16, %r14d
	js	.LBB6_353
# BB#362:                               # %._crit_edge3076.loopexit
                                        #   in Loop: Header=BB6_329 Depth=3
	addl	$16, %r14d
.LBB6_363:                              # %._crit_edge3076
                                        #   in Loop: Header=BB6_329 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	shrl	$20, %eax
	movzwl	12370(%r13,%rax,2), %eax
	cmpl	$250, %eax
	jb	.LBB6_368
# BB#364:                               # %.preheader1218.preheader
                                        #   in Loop: Header=BB6_329 Depth=3
	movl	$1048576, %ecx          # imm = 0x100000
.LBB6_365:                              # %.preheader1218
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_329 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	sarl	%ecx
	je	.LBB6_510
# BB#366:                               #   in Loop: Header=BB6_365 Depth=4
	addl	%eax, %eax
	movzwl	%ax, %edx
	cmpl	$4596, %edx             # imm = 0x11F4
	jae	.LBB6_511
# BB#367:                               #   in Loop: Header=BB6_365 Depth=4
	xorl	%edx, %edx
	testl	16(%rsp), %ecx          # 4-byte Folded Reload
	setne	%dl
	orl	%edx, %eax
	movzwl	%ax, %eax
	movzwl	12370(%r13,%rax,2), %eax
	cmpl	$249, %eax
	ja	.LBB6_365
.LBB6_368:                              # %.loopexit1219
                                        #   in Loop: Header=BB6_329 Depth=3
	movzwl	%ax, %esi
	movzbl	960(%r13,%rsi), %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	shll	%cl, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	subl	%ecx, %r14d
	addl	$7, %esi
.LBB6_369:                              #   in Loop: Header=BB6_329 Depth=3
	sarl	$3, %r12d
	cmpl	$3, %r12d
	ja	.LBB6_372
# BB#370:                               #   in Loop: Header=BB6_329 Depth=3
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movl	56(%rsp), %edi          # 4-byte Reload
	movl	%edi, %edx
	movl	40(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %eax
	jmpq	*.LJTI6_0(,%r12,8)
.LBB6_371:                              #   in Loop: Header=BB6_329 Depth=3
	movl	%ecx, %edx
	movl	%edi, %eax
	jmp	.LBB6_387
.LBB6_372:                              #   in Loop: Header=BB6_329 Depth=3
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movl	%r12d, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movzbl	22056(%r13,%rax), %eax
	movl	%eax, %r12d
	cmpl	%eax, %r14d
	jge	.LBB6_385
# BB#373:                               # %.lr.ph3086.preheader
                                        #   in Loop: Header=BB6_329 Depth=3
	movl	$16, %ebp
	subl	%r14d, %ebp
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	%r8, %rsi
.LBB6_374:                              # %.lr.ph3086
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_329 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leaq	1(%r15), %rax
	cmpq	%rsi, %rax
	jb	.LBB6_382
# BB#375:                               #   in Loop: Header=BB6_374 Depth=4
	movq	54888(%r13), %rax
	testq	%rax, %rax
	movq	104(%r13), %rsi
	movl	152(%r13), %edx
	je	.LBB6_377
# BB#376:                               #   in Loop: Header=BB6_374 Depth=4
	movq	54880(%r13), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB6_378
	jmp	.LBB6_509
.LBB6_377:                              #   in Loop: Header=BB6_374 Depth=4
	movl	(%r13), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB6_509
.LBB6_378:                              #   in Loop: Header=BB6_374 Depth=4
	jne	.LBB6_381
# BB#379:                               #   in Loop: Header=BB6_374 Depth=4
	cmpb	$0, 92(%r13)
	jne	.LBB6_508
# BB#380:                               #   in Loop: Header=BB6_374 Depth=4
	movq	104(%r13), %rax
	movb	$0, 1(%rax)
	movq	104(%r13), %rax
	movb	$0, (%rax)
	movb	$1, 92(%r13)
	movl	$2, %eax
.LBB6_381:                              #   in Loop: Header=BB6_374 Depth=4
	movq	104(%r13), %r15
	movq	%r15, 112(%r13)
	movslq	%eax, %rsi
	addq	%r15, %rsi
	movq	%rsi, 120(%r13)
.LBB6_382:                              #   in Loop: Header=BB6_374 Depth=4
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	movq	16(%rsp), %rax          # 8-byte Reload
	orl	%edx, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	addl	$16, %r14d
	addq	$2, %r15
	addl	$-16, %ebp
	cmpl	%r12d, %r14d
	jl	.LBB6_374
	jmp	.LBB6_386
.LBB6_383:                              #   in Loop: Header=BB6_329 Depth=3
	movl	96(%rsp), %eax          # 4-byte Reload
	movl	%eax, %edx
	movl	%ecx, %eax
	movl	%edi, 96(%rsp)          # 4-byte Spill
	jmp	.LBB6_387
.LBB6_384:                              #   in Loop: Header=BB6_329 Depth=3
	movl	$1, %edx
	movl	%edi, %eax
	movl	%ecx, 96(%rsp)          # 4-byte Spill
	jmp	.LBB6_387
.LBB6_385:                              #   in Loop: Header=BB6_329 Depth=3
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	%r8, %rsi
.LBB6_386:                              # %._crit_edge3087
                                        #   in Loop: Header=BB6_329 Depth=3
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movl	$32, %ecx
	subl	%r12d, %ecx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	%edx, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	movl	%r12d, %ecx
	shll	%cl, %edx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	subl	%r12d, %r14d
	movq	96(%rsp), %rcx          # 8-byte Reload
	movl	21852(%r13,%rcx,4), %ecx
	leal	-2(%rax,%rcx), %edx
	movl	56(%rsp), %eax          # 4-byte Reload
	movl	40(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 96(%rsp)          # 4-byte Spill
	movq	72(%rsp), %rsi          # 8-byte Reload
.LBB6_387:                              #   in Loop: Header=BB6_329 Depth=3
	movl	%eax, 72(%rsp)          # 4-byte Spill
	leal	2(%rsi), %r8d
	leal	(%r8,%rbx), %eax
	movl	40(%r13), %r10d
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpl	%r10d, %eax
	ja	.LBB6_527
# BB#388:                               #   in Loop: Header=BB6_329 Depth=3
	movl	%ebx, %r9d
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r9), %rcx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movl	%edx, %ebp
	subl	%ebx, %ebp
	jbe	.LBB6_399
# BB#389:                               #   in Loop: Header=BB6_329 Depth=3
	movl	%r10d, %eax
	subl	%ebp, %eax
	jl	.LBB6_530
# BB#390:                               #   in Loop: Header=BB6_329 Depth=3
	movq	104(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax), %rdx
	movl	%r8d, %esi
	subl	%ebp, %esi
	jle	.LBB6_402
# BB#391:                               #   in Loop: Header=BB6_329 Depth=3
	testl	%ebp, %ebp
	jle	.LBB6_426
# BB#392:                               # %.lr.ph3101.preheader
                                        #   in Loop: Header=BB6_329 Depth=3
	leal	-1(%rbx), %r11d
	movq	56(%rsp), %r12          # 8-byte Reload
	subl	%r12d, %r11d
	cmpl	$-3, %r11d
	movl	$-2, %edi
	cmovgl	%r11d, %edi
	leal	1(%r12,%rdi), %edi
	subl	%ebx, %edi
	incq	%rdi
	movq	%rdi, 184(%rsp)         # 8-byte Spill
	cmpq	$31, %rdi
	jbe	.LBB6_424
# BB#393:                               # %min.iters.checked4375
                                        #   in Loop: Header=BB6_329 Depth=3
	movq	184(%rsp), %r12         # 8-byte Reload
	movabsq	$8589934560, %rdi       # imm = 0x1FFFFFFE0
	andq	%rdi, %r12
	movq	%r12, 176(%rsp)         # 8-byte Spill
	je	.LBB6_424
# BB#394:                               # %vector.memcheck4389
                                        #   in Loop: Header=BB6_329 Depth=3
	cmpl	$-3, %r11d
	movl	$-2, %edi
	cmovlel	%edi, %r11d
	movq	56(%rsp), %rdi          # 8-byte Reload
	leal	1(%rdi,%r11), %r11d
	subl	%ebx, %r11d
	addq	%r11, %rax
	addq	144(%rsp), %rax         # 8-byte Folded Reload
	cmpq	%rax, %rcx
	jae	.LBB6_396
# BB#395:                               # %vector.memcheck4389
                                        #   in Loop: Header=BB6_329 Depth=3
	addq	%r9, %r11
	addq	144(%rsp), %r11         # 8-byte Folded Reload
	cmpq	%r11, %rdx
	jb	.LBB6_424
.LBB6_396:                              # %vector.body4370.preheader
                                        #   in Loop: Header=BB6_329 Depth=3
	movq	176(%rsp), %rax         # 8-byte Reload
	leaq	-32(%rax), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	shrl	$5, %eax
	incl	%eax
	testb	$3, %al
	je	.LBB6_418
# BB#397:                               # %vector.body4370.prol.preheader
                                        #   in Loop: Header=BB6_329 Depth=3
	movq	88(%rsp), %r13          # 8-byte Reload
	leal	(%r13,%r10), %r12d
	movq	56(%rsp), %rbx          # 8-byte Reload
	subl	%ebx, %r12d
	movl	136(%rsp), %edi         # 4-byte Reload
	addl	%edi, %r12d
	movq	200(%rsp), %rax         # 8-byte Reload
	addq	%rax, %r12
	leaq	(%r9,%rax), %r11
	leal	-1(%r13), %eax
	subl	%ebx, %eax
	addl	%edi, %eax
	cmpl	$-3, %eax
	movl	$-2, %edi
	cmovlel	%edi, %eax
	leal	1(%rbx), %ebx
	subl	%r13d, %ebx
	addl	128(%rsp), %ebx         # 4-byte Folded Reload
	leal	1(%rax,%rbx), %eax
	andl	$96, %eax
	addl	$-32, %eax
	shrl	$5, %eax
	incl	%eax
	andl	$3, %eax
	negq	%rax
	xorl	%ebx, %ebx
.LBB6_398:                              # %vector.body4370.prol
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_329 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	-16(%r12,%rbx), %xmm0
	movups	(%r12,%rbx), %xmm1
	movups	%xmm0, -16(%r11,%rbx)
	movups	%xmm1, (%r11,%rbx)
	addq	$32, %rbx
	incq	%rax
	jne	.LBB6_398
	jmp	.LBB6_419
.LBB6_399:                              #   in Loop: Header=BB6_329 Depth=3
	cmpl	$-1, %esi
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	jl	.LBB6_443
# BB#400:                               # %.lr.ph3096.preheader
                                        #   in Loop: Header=BB6_329 Depth=3
	movl	56(%rsp), %edx          # 4-byte Reload
	movq	%rcx, %rax
	subq	%rdx, %rax
	movl	$-3, %ebx
	subl	%esi, %ebx
	cmpl	$-3, %ebx
	movl	$-2, %edi
	cmovgl	%ebx, %edi
	movq	%rsi, %rbp
	leal	3(%rsi,%rdi), %r11d
	incq	%r11
	cmpq	$32, %r11
	jae	.LBB6_403
# BB#401:                               #   in Loop: Header=BB6_329 Depth=3
	movl	%r8d, %edx
	jmp	.LBB6_416
.LBB6_402:                              #   in Loop: Header=BB6_329 Depth=3
	movl	%r8d, %esi
	jmp	.LBB6_427
.LBB6_403:                              # %min.iters.checked4417
                                        #   in Loop: Header=BB6_329 Depth=3
	movq	%r11, %r10
	movabsq	$8589934560, %rdi       # imm = 0x1FFFFFFE0
	andq	%rdi, %r10
	je	.LBB6_406
# BB#404:                               # %vector.memcheck4431
                                        #   in Loop: Header=BB6_329 Depth=3
	cmpl	$-3, %ebx
	movl	$-2, %edi
	cmovlel	%edi, %ebx
	leal	3(%rbp,%rbx), %esi
	addq	%r9, %rsi
	movq	%rsi, %rdi
	subq	%rdx, %rdi
	addq	144(%rsp), %rdi         # 8-byte Folded Reload
	cmpq	%rdi, %rcx
	jae	.LBB6_407
# BB#405:                               # %vector.memcheck4431
                                        #   in Loop: Header=BB6_329 Depth=3
	addq	144(%rsp), %rsi         # 8-byte Folded Reload
	cmpq	%rsi, %rax
	jae	.LBB6_407
.LBB6_406:                              #   in Loop: Header=BB6_329 Depth=3
	movl	%r8d, %edx
.LBB6_416:                              # %.lr.ph3096.preheader4582
                                        #   in Loop: Header=BB6_329 Depth=3
	incl	%edx
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB6_417:                              # %.lr.ph3096
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_329 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rax), %ebx
	incq	%rax
	movb	%bl, (%rcx)
	incq	%rcx
	decl	%edx
	cmpl	$1, %edx
	jg	.LBB6_417
	jmp	.LBB6_443
.LBB6_407:                              # %vector.body4412.preheader
                                        #   in Loop: Header=BB6_329 Depth=3
	negq	%rdx
	leaq	-32(%r10), %rdi
	movl	%edi, %ebp
	shrl	$5, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB6_410
# BB#408:                               # %vector.body4412.prol.preheader
                                        #   in Loop: Header=BB6_329 Depth=3
	movq	200(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx), %r12
	addq	%r9, %r12
	leaq	(%rsi,%r9), %rsi
	negq	%rbp
	xorl	%ebx, %ebx
.LBB6_409:                              # %vector.body4412.prol
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_329 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	-16(%r12,%rbx), %xmm0
	movups	(%r12,%rbx), %xmm1
	movups	%xmm0, -16(%rsi,%rbx)
	movups	%xmm1, (%rsi,%rbx)
	addq	$32, %rbx
	incq	%rbp
	jne	.LBB6_409
	jmp	.LBB6_411
.LBB6_410:                              #   in Loop: Header=BB6_329 Depth=3
	xorl	%ebx, %ebx
.LBB6_411:                              # %vector.body4412.prol.loopexit
                                        #   in Loop: Header=BB6_329 Depth=3
	cmpq	$96, %rdi
	jb	.LBB6_414
# BB#412:                               # %vector.body4412.preheader.new
                                        #   in Loop: Header=BB6_329 Depth=3
	movq	%r10, %rbp
	subq	%rbx, %rbp
	addq	192(%rsp), %rbx         # 8-byte Folded Reload
	addq	%rbx, %r9
.LBB6_413:                              # %vector.body4412
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_329 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	-112(%rdx,%r9), %xmm0
	movups	-96(%rdx,%r9), %xmm1
	movups	%xmm0, -112(%r9)
	movups	%xmm1, -96(%r9)
	movups	-80(%rdx,%r9), %xmm0
	movups	-64(%rdx,%r9), %xmm1
	movups	%xmm0, -80(%r9)
	movups	%xmm1, -64(%r9)
	movups	-48(%rdx,%r9), %xmm0
	movups	-32(%rdx,%r9), %xmm1
	movups	%xmm0, -48(%r9)
	movups	%xmm1, -32(%r9)
	movups	-16(%rdx,%r9), %xmm0
	movups	(%rdx,%r9), %xmm1
	movups	%xmm0, -16(%r9)
	movups	%xmm1, (%r9)
	subq	$-128, %r9
	addq	$-128, %rbp
	jne	.LBB6_413
.LBB6_414:                              # %middle.block4413
                                        #   in Loop: Header=BB6_329 Depth=3
	cmpq	%r10, %r11
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
	je	.LBB6_443
# BB#415:                               #   in Loop: Header=BB6_329 Depth=3
	movl	%r8d, %edx
	subl	%r10d, %edx
	addq	%r10, %rcx
	addq	%r10, %rax
	jmp	.LBB6_416
.LBB6_418:                              #   in Loop: Header=BB6_329 Depth=3
	xorl	%ebx, %ebx
.LBB6_419:                              # %vector.body4370.prol.loopexit
                                        #   in Loop: Header=BB6_329 Depth=3
	cmpq	$96, 120(%rsp)          # 8-byte Folded Reload
	movq	128(%rsp), %rdi         # 8-byte Reload
	jb	.LBB6_422
# BB#420:                               # %vector.body4370.preheader.new
                                        #   in Loop: Header=BB6_329 Depth=3
	movq	88(%rsp), %r13          # 8-byte Reload
	leal	-1(%r13), %r11d
	movq	56(%rsp), %r12          # 8-byte Reload
	subl	%r12d, %r11d
	addl	136(%rsp), %r11d        # 4-byte Folded Reload
	cmpl	$-3, %r11d
	movl	$-2, %eax
	cmovlel	%eax, %r11d
	leal	1(%r12), %eax
	subl	%r13d, %eax
	addl	%eax, %edi
	addl	%r11d, %edi
	incq	%rdi
	movabsq	$8589934560, %rax       # imm = 0x1FFFFFFE0
	andq	%rax, %rdi
	subq	%rbx, %rdi
	addq	192(%rsp), %rbx         # 8-byte Folded Reload
	addl	%r13d, %r10d
	subl	%r12d, %r10d
	addl	136(%rsp), %r10d        # 4-byte Folded Reload
	addq	%rbx, %r10
	addq	%rbx, %r9
.LBB6_421:                              # %vector.body4370
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_329 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	-112(%r10), %xmm0
	movups	-96(%r10), %xmm1
	movups	%xmm0, -112(%r9)
	movups	%xmm1, -96(%r9)
	movups	-80(%r10), %xmm0
	movups	-64(%r10), %xmm1
	movups	%xmm0, -80(%r9)
	movups	%xmm1, -64(%r9)
	movups	-48(%r10), %xmm0
	movups	-32(%r10), %xmm1
	movups	%xmm0, -48(%r9)
	movups	%xmm1, -32(%r9)
	movups	-16(%r10), %xmm0
	movups	(%r10), %xmm1
	movups	%xmm0, -16(%r9)
	movups	%xmm1, (%r9)
	subq	$-128, %r10
	subq	$-128, %r9
	addq	$-128, %rdi
	jne	.LBB6_421
.LBB6_422:                              # %middle.block4371
                                        #   in Loop: Header=BB6_329 Depth=3
	movq	176(%rsp), %rax         # 8-byte Reload
	addq	%rax, %rcx
	cmpq	%rax, 184(%rsp)         # 8-byte Folded Reload
	je	.LBB6_426
# BB#423:                               #   in Loop: Header=BB6_329 Depth=3
	movq	176(%rsp), %rax         # 8-byte Reload
	addq	%rax, %rdx
	subl	%eax, %ebp
.LBB6_424:                              # %.lr.ph3101.preheader4581
                                        #   in Loop: Header=BB6_329 Depth=3
	incl	%ebp
.LBB6_425:                              # %.lr.ph3101
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_329 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rdx), %eax
	incq	%rdx
	movb	%al, (%rcx)
	incq	%rcx
	decl	%ebp
	cmpl	$1, %ebp
	jg	.LBB6_425
.LBB6_426:                              #   in Loop: Header=BB6_329 Depth=3
	movq	104(%rsp), %rdx         # 8-byte Reload
.LBB6_427:                              # %.preheader
                                        #   in Loop: Header=BB6_329 Depth=3
	testl	%esi, %esi
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
	jle	.LBB6_443
# BB#428:                               # %.lr.ph3106.preheader
                                        #   in Loop: Header=BB6_329 Depth=3
	movl	%esi, %eax
	notl	%eax
	cmpl	$-3, %eax
	movl	$-2, %edi
	cmovgl	%eax, %edi
	leal	1(%rsi,%rdi), %edi
	incq	%rdi
	cmpq	$31, %rdi
	jbe	.LBB6_441
# BB#429:                               # %min.iters.checked
                                        #   in Loop: Header=BB6_329 Depth=3
	movq	%rdi, %r9
	movabsq	$8589934560, %rbp       # imm = 0x1FFFFFFE0
	andq	%rbp, %r9
	je	.LBB6_441
# BB#430:                               # %vector.memcheck
                                        #   in Loop: Header=BB6_329 Depth=3
	cmpl	$-3, %eax
	movl	$-2, %ebp
	cmovlel	%ebp, %eax
	leal	1(%rsi,%rax), %eax
	leaq	1(%rdx,%rax), %rbp
	cmpq	%rbp, %rcx
	jae	.LBB6_432
# BB#431:                               # %vector.memcheck
                                        #   in Loop: Header=BB6_329 Depth=3
	leaq	1(%rcx,%rax), %rax
	cmpq	%rax, %rdx
	jb	.LBB6_441
.LBB6_432:                              # %vector.body.preheader
                                        #   in Loop: Header=BB6_329 Depth=3
	leaq	-32(%r9), %rbp
	movl	%ebp, %eax
	shrl	$5, %eax
	incl	%eax
	andq	$3, %rax
	je	.LBB6_435
# BB#433:                               # %vector.body.prol.preheader
                                        #   in Loop: Header=BB6_329 Depth=3
	negq	%rax
	xorl	%ebx, %ebx
.LBB6_434:                              # %vector.body.prol
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_329 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	(%rdx,%rbx), %xmm0
	movups	16(%rdx,%rbx), %xmm1
	movups	%xmm0, (%rcx,%rbx)
	movups	%xmm1, 16(%rcx,%rbx)
	addq	$32, %rbx
	incq	%rax
	jne	.LBB6_434
	jmp	.LBB6_436
.LBB6_435:                              #   in Loop: Header=BB6_329 Depth=3
	xorl	%ebx, %ebx
.LBB6_436:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB6_329 Depth=3
	cmpq	$96, %rbp
	jb	.LBB6_439
# BB#437:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB6_329 Depth=3
	movq	%r9, %rax
	subq	%rbx, %rax
	leaq	112(%rdx,%rbx), %rbp
	leaq	112(%rcx,%rbx), %rbx
.LBB6_438:                              # %vector.body
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_329 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbp
	subq	$-128, %rbx
	addq	$-128, %rax
	jne	.LBB6_438
.LBB6_439:                              # %middle.block
                                        #   in Loop: Header=BB6_329 Depth=3
	cmpq	%r9, %rdi
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
	je	.LBB6_443
# BB#440:                               #   in Loop: Header=BB6_329 Depth=3
	subl	%r9d, %esi
	addq	%r9, %rcx
	addq	%r9, %rdx
.LBB6_441:                              # %.lr.ph3106.preheader4580
                                        #   in Loop: Header=BB6_329 Depth=3
	incl	%esi
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB6_442:                              # %.lr.ph3106
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        #       Parent Loop BB6_329 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rdx), %eax
	incq	%rdx
	movb	%al, (%rcx)
	incq	%rcx
	decl	%esi
	cmpl	$1, %esi
	jg	.LBB6_442
.LBB6_443:                              # %.outer
                                        #   in Loop: Header=BB6_329 Depth=3
	movq	80(%rsp), %rdi          # 8-byte Reload
	subl	%r8d, %edi
	movl	112(%rsp), %ecx         # 4-byte Reload
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	168(%rsp), %rdx         # 8-byte Reload
	jg	.LBB6_329
	jmp	.LBB6_458
.LBB6_444:                              #   in Loop: Header=BB6_146 Depth=2
	movq	%r12, 16(%rsp)          # 8-byte Spill
	leal	(%rdx,%rbx), %eax
	testl	%edx, %edx
	jle	.LBB6_457
# BB#445:                               # %.lr.ph2904.preheader
                                        #   in Loop: Header=BB6_146 Depth=2
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movl	%ecx, 112(%rsp)         # 4-byte Spill
	movl	%ebx, %ebp
	addq	104(%rsp), %rbp         # 8-byte Folded Reload
	movl	%edx, %ebx
	jmp	.LBB6_447
.LBB6_446:                              # %.outer1256
                                        #   in Loop: Header=BB6_447 Depth=3
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	104(%rcx), %r15
	movq	%r15, 112(%rcx)
	movslq	%eax, %r8
	addq	%r15, %r8
	movq	%r8, 120(%rcx)
	.p2align	4, 0x90
.LBB6_447:                              #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r8d, %eax
	subl	%r15d, %eax
	je	.LBB6_450
# BB#448:                               #   in Loop: Header=BB6_447 Depth=3
	cmpl	%ebx, %eax
	cmovgl	%ebx, %eax
	movslq	%eax, %r12
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%r8, %r13
	callq	memcpy
	movq	%r13, %r8
	addq	%r12, %rbp
	addq	%r12, %r15
	subl	%r12d, %ebx
	jg	.LBB6_447
	jmp	.LBB6_449
.LBB6_450:                              #   in Loop: Header=BB6_447 Depth=3
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	54888(%rcx), %rax
	testq	%rax, %rax
	movq	104(%rcx), %rsi
	movl	152(%rcx), %edx
	je	.LBB6_452
# BB#451:                               #   in Loop: Header=BB6_447 Depth=3
	movq	54880(%rcx), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB6_453
	jmp	.LBB6_515
.LBB6_452:                              #   in Loop: Header=BB6_447 Depth=3
	movl	(%rcx), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB6_515
.LBB6_453:                              #   in Loop: Header=BB6_447 Depth=3
	jne	.LBB6_446
# BB#454:                               #   in Loop: Header=BB6_447 Depth=3
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpb	$0, 92(%rcx)
	jne	.LBB6_514
# BB#455:                               #   in Loop: Header=BB6_447 Depth=3
	movq	104(%rcx), %rax
	movb	$0, 1(%rax)
	movq	104(%rcx), %rax
	movb	$0, (%rax)
	movb	$1, 92(%rcx)
	movl	$2, %eax
	jmp	.LBB6_446
.LBB6_449:                              #   in Loop: Header=BB6_146 Depth=2
	movl	40(%rsp), %eax          # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
	movl	112(%rsp), %ecx         # 4-byte Reload
	movq	168(%rsp), %rdx         # 8-byte Reload
	movq	%rbx, %rdi
	movq	%rax, %rbx
	testl	%edi, %edi
	jns	.LBB6_461
	jmp	.LBB6_459
.LBB6_456:                              #   in Loop: Header=BB6_146 Depth=2
	movl	%edx, %edi
	testl	%edi, %edi
	jns	.LBB6_461
	jmp	.LBB6_459
.LBB6_457:                              #   in Loop: Header=BB6_146 Depth=2
	movl	%edx, %edi
	movl	%eax, %ebx
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB6_458:                              # %.loopexit1250
                                        #   in Loop: Header=BB6_146 Depth=2
	testl	%edi, %edi
	jns	.LBB6_461
.LBB6_459:                              #   in Loop: Header=BB6_146 Depth=2
	movl	%edi, %esi
	negl	%esi
	movl	76(%r13), %edx
	cmpl	%esi, %edx
	jb	.LBB6_521
# BB#460:                               #   in Loop: Header=BB6_146 Depth=2
	addl	%edi, %edx
	movl	%edx, 76(%r13)
	movq	168(%rsp), %rdx         # 8-byte Reload
.LBB6_461:                              # %.backedge1268
                                        #   in Loop: Header=BB6_146 Depth=2
	subl	%edx, %ecx
	testl	%ecx, %ecx
	jg	.LBB6_146
# BB#462:                               # %._crit_edge3136.loopexit
                                        #   in Loop: Header=BB6_11 Depth=1
	movl	48(%r13), %eax
	jmp	.LBB6_506
.LBB6_11:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_465 Depth 2
                                        #     Child Loop BB6_478 Depth 2
                                        #     Child Loop BB6_490 Depth 2
                                        #     Child Loop BB6_146 Depth 2
                                        #       Child Loop BB6_65 Depth 3
                                        #       Child Loop BB6_76 Depth 3
                                        #       Child Loop BB6_89 Depth 3
                                        #       Child Loop BB6_102 Depth 3
                                        #         Child Loop BB6_104 Depth 4
                                        #       Child Loop BB6_125 Depth 3
                                        #       Child Loop BB6_136 Depth 3
                                        #       Child Loop BB6_151 Depth 3
                                        #         Child Loop BB6_152 Depth 4
                                        #           Child Loop BB6_154 Depth 5
                                        #           Child Loop BB6_166 Depth 5
                                        #         Child Loop BB6_175 Depth 4
                                        #         Child Loop BB6_187 Depth 4
                                        #         Child Loop BB6_245 Depth 4
                                        #         Child Loop BB6_214 Depth 4
                                        #         Child Loop BB6_257 Depth 4
                                        #         Child Loop BB6_201 Depth 4
                                        #         Child Loop BB6_226 Depth 4
                                        #         Child Loop BB6_238 Depth 4
                                        #         Child Loop BB6_287 Depth 4
                                        #         Child Loop BB6_291 Depth 4
                                        #         Child Loop BB6_295 Depth 4
                                        #         Child Loop BB6_276 Depth 4
                                        #         Child Loop BB6_299 Depth 4
                                        #         Child Loop BB6_303 Depth 4
                                        #         Child Loop BB6_312 Depth 4
                                        #         Child Loop BB6_316 Depth 4
                                        #         Child Loop BB6_320 Depth 4
                                        #       Child Loop BB6_447 Depth 3
                                        #       Child Loop BB6_329 Depth 3
                                        #         Child Loop BB6_330 Depth 4
                                        #           Child Loop BB6_332 Depth 5
                                        #           Child Loop BB6_344 Depth 5
                                        #         Child Loop BB6_353 Depth 4
                                        #         Child Loop BB6_365 Depth 4
                                        #         Child Loop BB6_374 Depth 4
                                        #         Child Loop BB6_409 Depth 4
                                        #         Child Loop BB6_413 Depth 4
                                        #         Child Loop BB6_417 Depth 4
                                        #         Child Loop BB6_398 Depth 4
                                        #         Child Loop BB6_421 Depth 4
                                        #         Child Loop BB6_425 Depth 4
                                        #         Child Loop BB6_434 Depth 4
                                        #         Child Loop BB6_438 Depth 4
                                        #         Child Loop BB6_442 Depth 4
                                        #     Child Loop BB6_19 Depth 2
                                        #     Child Loop BB6_36 Depth 2
	movl	56(%rbp), %ecx
	testl	%ecx, %ecx
	je	.LBB6_15
# BB#12:                                #   in Loop: Header=BB6_11 Depth=1
	xorl	%edx, %edx
	divl	%ecx
	testl	%edx, %edx
	jne	.LBB6_15
# BB#13:                                #   in Loop: Header=BB6_11 Depth=1
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	76(%rbp), %esi
	testl	%esi, %esi
	jne	.LBB6_517
# BB#14:                                #   in Loop: Header=BB6_11 Depth=1
	movl	$1, 60(%rbp)
	movl	$1, 64(%rbp)
	movl	$1, 68(%rbp)
	movb	$0, 90(%rbp)
	movl	$0, 76(%rbp)
	movb	$0, 89(%rbp)
	xorl	%esi, %esi
	movl	$656, %edx              # imm = 0x290
	movq	224(%rsp), %rdi         # 8-byte Reload
	movq	%r8, %rbp
	callq	memset
	movq	%rbp, %r8
	movq	216(%rsp), %rax         # 8-byte Reload
	xorps	%xmm0, %xmm0
	movups	%xmm0, 234(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
.LBB6_15:                               #   in Loop: Header=BB6_11 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpb	$0, 90(%rax)
	movq	%r12, 272(%rsp)         # 8-byte Spill
	je	.LBB6_463
# BB#16:                                #   in Loop: Header=BB6_11 Depth=1
	movl	%ebx, %r14d
	movq	%r13, %r12
	movq	8(%rsp), %r13           # 8-byte Reload
	jmp	.LBB6_502
.LBB6_463:                              # %.preheader1272
                                        #   in Loop: Header=BB6_11 Depth=1
	testl	%ebx, %ebx
	jg	.LBB6_474
# BB#464:                               # %.lr.ph.preheader
                                        #   in Loop: Header=BB6_11 Depth=1
	movl	$16, %ebp
	subl	%ebx, %ebp
.LBB6_465:                              # %.lr.ph
                                        #   Parent Loop BB6_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%r15), %rax
	cmpq	%r8, %rax
	jb	.LBB6_473
# BB#466:                               #   in Loop: Header=BB6_465 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	54888(%rcx), %rax
	testq	%rax, %rax
	movq	104(%rcx), %rsi
	movl	152(%rcx), %edx
	je	.LBB6_468
# BB#467:                               #   in Loop: Header=BB6_465 Depth=2
	movq	54880(%rcx), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB6_469
	jmp	.LBB6_515
.LBB6_468:                              #   in Loop: Header=BB6_465 Depth=2
	movl	(%rcx), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB6_515
.LBB6_469:                              #   in Loop: Header=BB6_465 Depth=2
	jne	.LBB6_472
# BB#470:                               #   in Loop: Header=BB6_465 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpb	$0, 92(%rcx)
	jne	.LBB6_514
# BB#471:                               #   in Loop: Header=BB6_465 Depth=2
	movq	104(%rcx), %rax
	movb	$0, 1(%rax)
	movq	104(%rcx), %rax
	movb	$0, (%rax)
	movb	$1, 92(%rcx)
	movl	$2, %eax
.LBB6_472:                              #   in Loop: Header=BB6_465 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	104(%rcx), %r15
	movq	%r15, 112(%rcx)
	movslq	%eax, %r8
	addq	%r15, %r8
	movq	%r8, 120(%rcx)
.LBB6_473:                              #   in Loop: Header=BB6_465 Depth=2
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	orl	%edx, %r13d
	addq	$2, %r15
	addl	$-16, %ebp
	addl	$16, %ebx
	jle	.LBB6_465
.LBB6_474:                              # %._crit_edge
                                        #   in Loop: Header=BB6_11 Depth=1
	leal	(%r13,%r13), %r12d
	leal	-1(%rbx), %r14d
	xorl	%eax, %eax
	testl	%r13d, %r13d
	js	.LBB6_476
# BB#475:                               #   in Loop: Header=BB6_11 Depth=1
	xorl	%edx, %edx
	movq	8(%rsp), %r13           # 8-byte Reload
	jmp	.LBB6_501
.LBB6_476:                              # %.preheader1270
                                        #   in Loop: Header=BB6_11 Depth=1
	cmpl	$16, %ebx
	movq	8(%rsp), %r13           # 8-byte Reload
	jg	.LBB6_488
# BB#477:                               # %.lr.ph2824.preheader
                                        #   in Loop: Header=BB6_11 Depth=1
	movl	$17, %ebp
	subl	%ebx, %ebp
	addl	$-17, %ebx
.LBB6_478:                              # %.lr.ph2824
                                        #   Parent Loop BB6_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%r15), %rax
	cmpq	%r8, %rax
	jb	.LBB6_486
# BB#479:                               #   in Loop: Header=BB6_478 Depth=2
	movq	54888(%r13), %rax
	testq	%rax, %rax
	movq	104(%r13), %rsi
	movl	152(%r13), %edx
	je	.LBB6_481
# BB#480:                               #   in Loop: Header=BB6_478 Depth=2
	movq	54880(%r13), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB6_482
	jmp	.LBB6_509
.LBB6_481:                              #   in Loop: Header=BB6_478 Depth=2
	movl	(%r13), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB6_509
.LBB6_482:                              #   in Loop: Header=BB6_478 Depth=2
	jne	.LBB6_485
# BB#483:                               #   in Loop: Header=BB6_478 Depth=2
	cmpb	$0, 92(%r13)
	jne	.LBB6_508
# BB#484:                               #   in Loop: Header=BB6_478 Depth=2
	movq	104(%r13), %rax
	movb	$0, 1(%rax)
	movq	104(%r13), %rax
	movb	$0, (%rax)
	movb	$1, 92(%r13)
	movl	$2, %eax
.LBB6_485:                              #   in Loop: Header=BB6_478 Depth=2
	movq	104(%r13), %r15
	movq	%r15, 112(%r13)
	movslq	%eax, %r8
	addq	%r15, %r8
	movq	%r8, 120(%r13)
.LBB6_486:                              #   in Loop: Header=BB6_478 Depth=2
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	orl	%edx, %r12d
	addq	$2, %r15
	addl	$-16, %ebp
	addl	$16, %ebx
	js	.LBB6_478
# BB#487:                               # %._crit_edge2825.loopexit
                                        #   in Loop: Header=BB6_11 Depth=1
	addl	$16, %ebx
	movl	%ebx, %r14d
.LBB6_488:                              # %._crit_edge2825
                                        #   in Loop: Header=BB6_11 Depth=1
	movl	%r12d, %ebx
	shll	$16, %ebx
	leal	-16(%r14), %ecx
	cmpl	$15, %ecx
	jg	.LBB6_500
# BB#489:                               # %.lr.ph2835.preheader
                                        #   in Loop: Header=BB6_11 Depth=1
	movl	$32, %ebp
	subl	%r14d, %ebp
	addl	$-32, %r14d
.LBB6_490:                              # %.lr.ph2835
                                        #   Parent Loop BB6_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%r15), %rax
	cmpq	%r8, %rax
	jb	.LBB6_498
# BB#491:                               #   in Loop: Header=BB6_490 Depth=2
	movq	54888(%r13), %rax
	testq	%rax, %rax
	movq	104(%r13), %rsi
	movl	152(%r13), %edx
	je	.LBB6_493
# BB#492:                               #   in Loop: Header=BB6_490 Depth=2
	movq	54880(%r13), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB6_494
	jmp	.LBB6_509
.LBB6_493:                              #   in Loop: Header=BB6_490 Depth=2
	movl	(%r13), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB6_509
.LBB6_494:                              #   in Loop: Header=BB6_490 Depth=2
	jne	.LBB6_497
# BB#495:                               #   in Loop: Header=BB6_490 Depth=2
	cmpb	$0, 92(%r13)
	jne	.LBB6_508
# BB#496:                               #   in Loop: Header=BB6_490 Depth=2
	movq	104(%r13), %rax
	movb	$0, 1(%rax)
	movq	104(%r13), %rax
	movb	$0, (%rax)
	movb	$1, 92(%r13)
	movl	$2, %eax
.LBB6_497:                              #   in Loop: Header=BB6_490 Depth=2
	movq	104(%r13), %r15
	movq	%r15, 112(%r13)
	movslq	%eax, %r8
	addq	%r15, %r8
	movq	%r8, 120(%r13)
.LBB6_498:                              #   in Loop: Header=BB6_490 Depth=2
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	orl	%edx, %ebx
	addq	$2, %r15
	addl	$-16, %ebp
	addl	$16, %r14d
	js	.LBB6_490
# BB#499:                               # %._crit_edge2836.loopexit
                                        #   in Loop: Header=BB6_11 Depth=1
	addl	$16, %r14d
	movl	%r14d, %ecx
.LBB6_500:                              # %._crit_edge2836
                                        #   in Loop: Header=BB6_11 Depth=1
	andl	$-65536, %r12d          # imm = 0xFFFF0000
	movl	%ebx, %eax
	shrl	$16, %eax
	shll	$16, %ebx
	addl	$-16, %ecx
	movl	%r12d, %edx
	movl	%ecx, %r14d
	movl	%ebx, %r12d
.LBB6_501:                              #   in Loop: Header=BB6_11 Depth=1
	orl	%edx, %eax
	movl	%eax, 80(%r13)
	movb	$1, 90(%r13)
                                        # kill: %R12D<def> %R12D<kill> %R12<def>
.LBB6_502:                              #   in Loop: Header=BB6_11 Depth=1
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	24(%r13), %rbp
	testq	%rbp, %rbp
	je	.LBB6_504
# BB#503:                               #   in Loop: Header=BB6_11 Depth=1
	subq	16(%r13), %rbp
	cmpq	$32768, %rbp            # imm = 0x8000
	movl	$32768, %eax            # imm = 0x8000
	cmovgel	%eax, %ebp
	jmp	.LBB6_505
.LBB6_504:                              #   in Loop: Header=BB6_11 Depth=1
	movl	$32768, %ebp            # imm = 0x8000
.LBB6_505:                              #   in Loop: Header=BB6_11 Depth=1
	movl	48(%r13), %eax
	movl	%ebp, %ecx
	subl	%ebx, %ecx
	addl	%eax, %ecx
	testl	%ecx, %ecx
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	jg	.LBB6_146
.LBB6_506:                              # %._crit_edge3136
                                        #   in Loop: Header=BB6_11 Depth=1
	movl	%ebx, %esi
	subl	%eax, %esi
	cmpl	%ebp, %esi
	je	.LBB6_17
# BB#507:
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	cli_dbgmsg
	jmp	.LBB6_513
.LBB6_508:
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB6_509:                              # %.loopexit1271
	movl	$-123, 96(%r13)
	jmp	.LBB6_536
.LBB6_510:
	movl	$.L.str.9, %edi
	jmp	.LBB6_512
.LBB6_511:
	movl	$.L.str.10, %edi
.LBB6_512:
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB6_513:
	movl	$-124, 96(%r13)
	movl	$-124, %eax
	jmp	.LBB6_537
.LBB6_514:
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB6_515:                              # %.loopexit1273
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$-123, 96(%rax)
	jmp	.LBB6_536
.LBB6_516:
	subq	%rax, %rsi
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	movq	48(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_dbgmsg
	movl	$-124, 96(%r14)
	movl	$-124, %eax
	jmp	.LBB6_537
.LBB6_55:                               # %._crit_edge3160
	testq	%r12, %r12
	je	.LBB6_522
.LBB6_56:                               # %._crit_edge3160.thread
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB6_518:
	movl	$-124, 96(%rbp)
	movl	$-124, %eax
	jmp	.LBB6_537
.LBB6_517:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB6_518
.LBB6_519:
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB6_513
.LBB6_520:
	movl	$-123, 96(%r14)
	jmp	.LBB6_536
.LBB6_521:
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB6_513
.LBB6_524:
	movl	96(%r13), %eax
	jmp	.LBB6_537
.LBB6_522:
	movq	%r15, 112(%rbp)
	movq	%r8, 120(%rbp)
	movl	%r13d, 144(%rbp)
	movl	%ebx, 148(%rbp)
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%eax, 44(%rbp)
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	%eax, 60(%rbp)
	movl	72(%rsp), %eax          # 4-byte Reload
	movl	%eax, 64(%rbp)
	movl	96(%rsp), %eax          # 4-byte Reload
	movl	%eax, 68(%rbp)
.LBB6_523:
	xorl	%eax, %eax
	jmp	.LBB6_537
.LBB6_525:
	movl	$.L.str.4, %edi
	movl	$.L.str.6, %esi
	jmp	.LBB6_529
.LBB6_526:
	movl	96(%r13), %eax
	jmp	.LBB6_537
.LBB6_122:
	movl	$.L.str.4, %edi
	movl	$.L.str.7, %esi
.LBB6_529:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB6_513
.LBB6_527:
	movl	$.L.str.11, %edi
	jmp	.LBB6_512
.LBB6_528:
	movl	$.L.str.4, %edi
	movl	$.L.str.5, %esi
	jmp	.LBB6_529
.LBB6_530:
	movl	$.L.str.12, %edi
	jmp	.LBB6_533
.LBB6_531:
	movl	$.L.str.9, %edi
	jmp	.LBB6_533
.LBB6_532:
	movl	$.L.str.10, %edi
.LBB6_533:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$-124, 96(%rax)
	movl	$-124, %eax
	jmp	.LBB6_537
.LBB6_534:
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB6_535:
	movl	$-123, 96(%rbx)
.LBB6_536:
	movl	$-123, %eax
	jmp	.LBB6_537
.Lfunc_end6:
	.size	lzx_decompress, .Lfunc_end6-lzx_decompress
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI6_0:
	.quad	.LBB6_387
	.quad	.LBB6_371
	.quad	.LBB6_383
	.quad	.LBB6_384

	.text
	.p2align	4, 0x90
	.type	lzx_make_decode_table,@function
lzx_make_decode_table:                  # @lzx_make_decode_table
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi58:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi59:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 56
.Lcfi61:
	.cfi_offset %rbx, -56
.Lcfi62:
	.cfi_offset %r12, -48
.Lcfi63:
	.cfi_offset %r13, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movq	%rcx, %r8
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	$1, %r13d
	movl	%esi, %ecx
	shll	%cl, %r13d
	movl	%r13d, %r10d
	shrl	%r10d
	xorl	%r9d, %r9d
	testl	%esi, %esi
	je	.LBB7_39
# BB#1:                                 # %.preheader110.lr.ph
	testl	%edi, %edi
	je	.LBB7_37
# BB#2:                                 # %.preheader110.us.preheader
	movb	$1, %r11b
	xorl	%r9d, %r9d
	movl	%r10d, %r15d
	movl	%r13d, -32(%rsp)        # 4-byte Spill
	movq	%rsi, -8(%rsp)          # 8-byte Spill
.LBB7_3:                                # %.preheader110.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_5 Depth 2
                                        #       Child Loop BB7_15 Depth 3
                                        #       Child Loop BB7_19 Depth 3
                                        #       Child Loop BB7_24 Depth 3
                                        #       Child Loop BB7_28 Depth 3
                                        #     Child Loop BB7_32 Depth 2
	testl	%r15d, %r15d
	je	.LBB7_31
# BB#4:                                 # %.lr.ph177..lr.ph177.split_crit_edge.us.preheader
                                        #   in Loop: Header=BB7_3 Depth=1
	leal	-1(%r15), %eax
	movl	%eax, -12(%rsp)         # 4-byte Spill
	movl	%r15d, %ebx
	andl	$2147483632, %ebx       # imm = 0x7FFFFFF0
	leal	-16(%rbx), %eax
	movl	%eax, -16(%rsp)         # 4-byte Spill
	shrl	$4, %eax
	incl	%eax
	movl	%r15d, %ecx
	subl	%ebx, %ecx
	movl	%ecx, -28(%rsp)         # 4-byte Spill
	andl	$3, %eax
	movl	%eax, -20(%rsp)         # 4-byte Spill
	negl	%eax
	movl	%eax, -24(%rsp)         # 4-byte Spill
	xorl	%r14d, %r14d
	jmp	.LBB7_5
.LBB7_13:                               # %vector.ph
                                        #   in Loop: Header=BB7_5 Depth=2
	cmpl	$0, -20(%rsp)           # 4-byte Folded Reload
	movd	%r14d, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	je	.LBB7_16
# BB#14:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB7_5 Depth=2
	xorl	%eax, %eax
	movl	-24(%rsp), %ecx         # 4-byte Reload
	.p2align	4, 0x90
.LBB7_15:                               # %vector.body.prol
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%r9,%rax), %ebp
	movdqu	%xmm0, (%r8,%rbp,2)
	movdqu	%xmm0, 16(%r8,%rbp,2)
	addl	$16, %eax
	incl	%ecx
	jne	.LBB7_15
	jmp	.LBB7_17
.LBB7_16:                               #   in Loop: Header=BB7_5 Depth=2
	xorl	%eax, %eax
.LBB7_17:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB7_5 Depth=2
	cmpl	$48, -16(%rsp)          # 4-byte Folded Reload
	jb	.LBB7_20
# BB#18:                                # %vector.ph.new
                                        #   in Loop: Header=BB7_5 Depth=2
	movl	%r9d, %ecx
	movl	%ebx, %ebp
	.p2align	4, 0x90
.LBB7_19:                               # %vector.body
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rax,%rcx), %esi
	movdqu	%xmm0, (%r8,%rsi,2)
	movdqu	%xmm0, 16(%r8,%rsi,2)
	leal	16(%rax,%rcx), %esi
	movdqu	%xmm0, (%r8,%rsi,2)
	movdqu	%xmm0, 16(%r8,%rsi,2)
	leal	32(%rax,%rcx), %esi
	movdqu	%xmm0, (%r8,%rsi,2)
	movdqu	%xmm0, 16(%r8,%rsi,2)
	leal	48(%rax,%rcx), %esi
	movdqu	%xmm0, (%r8,%rsi,2)
	movdqu	%xmm0, 16(%r8,%rsi,2)
	addl	$-64, %ebp
	addl	$64, %ecx
	cmpl	%ebp, %eax
	jne	.LBB7_19
.LBB7_20:                               # %middle.block
                                        #   in Loop: Header=BB7_5 Depth=2
	cmpl	%ebx, %r15d
	je	.LBB7_29
# BB#21:                                #   in Loop: Header=BB7_5 Depth=2
	addl	%ebx, %r9d
	movl	-28(%rsp), %eax         # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	jmp	.LBB7_22
	.p2align	4, 0x90
.LBB7_5:                                # %.lr.ph177..lr.ph177.split_crit_edge.us
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_15 Depth 3
                                        #       Child Loop BB7_19 Depth 3
                                        #       Child Loop BB7_24 Depth 3
                                        #       Child Loop BB7_28 Depth 3
	movzwl	%r14w, %eax
	cmpb	%r11b, (%rdx,%rax)
	jne	.LBB7_30
# BB#6:                                 #   in Loop: Header=BB7_5 Depth=2
	leal	(%r9,%r15), %r12d
	cmpl	%r13d, %r12d
	ja	.LBB7_83
# BB#7:                                 # %.lr.ph172.us198.preheader
                                        #   in Loop: Header=BB7_5 Depth=2
	cmpl	$16, %r15d
	jae	.LBB7_9
# BB#8:                                 #   in Loop: Header=BB7_5 Depth=2
	movl	%r15d, %eax
	jmp	.LBB7_22
	.p2align	4, 0x90
.LBB7_9:                                # %min.iters.checked
                                        #   in Loop: Header=BB7_5 Depth=2
	testl	%ebx, %ebx
	je	.LBB7_12
# BB#10:                                # %vector.scevcheck
                                        #   in Loop: Header=BB7_5 Depth=2
	movl	%r9d, %eax
	addl	-12(%rsp), %eax         # 4-byte Folded Reload
	jae	.LBB7_13
.LBB7_12:                               #   in Loop: Header=BB7_5 Depth=2
	movl	%r15d, %eax
.LBB7_22:                               # %.lr.ph172.us198.preheader270
                                        #   in Loop: Header=BB7_5 Depth=2
	leal	-1(%rax), %r13d
	movl	%eax, %ecx
	andl	$7, %ecx
	je	.LBB7_25
# BB#23:                                # %.lr.ph172.us198.prol.preheader
                                        #   in Loop: Header=BB7_5 Depth=2
	negl	%ecx
	.p2align	4, 0x90
.LBB7_24:                               # %.lr.ph172.us198.prol
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	decl	%eax
	movl	%r9d, %esi
	incl	%r9d
	movw	%r14w, (%r8,%rsi,2)
	incl	%ecx
	jne	.LBB7_24
.LBB7_25:                               # %.lr.ph172.us198.prol.loopexit
                                        #   in Loop: Header=BB7_5 Depth=2
	cmpl	$7, %r13d
	jae	.LBB7_27
# BB#26:                                #   in Loop: Header=BB7_5 Depth=2
	movl	%r12d, %r9d
	movl	-32(%rsp), %r13d        # 4-byte Reload
	jmp	.LBB7_30
	.p2align	4, 0x90
.LBB7_27:                               # %.lr.ph172.us198.preheader270.new
                                        #   in Loop: Header=BB7_5 Depth=2
	xorl	%ebp, %ebp
	movl	-32(%rsp), %r13d        # 4-byte Reload
	.p2align	4, 0x90
.LBB7_28:                               # %.lr.ph172.us198
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%r9,%rbp), %ecx
	leal	1(%r9,%rbp), %esi
	movw	%r14w, (%r8,%rcx,2)
	leal	2(%r9,%rbp), %ecx
	movw	%r14w, (%r8,%rsi,2)
	leal	3(%r9,%rbp), %esi
	movw	%r14w, (%r8,%rcx,2)
	leal	4(%r9,%rbp), %ecx
	movw	%r14w, (%r8,%rsi,2)
	leal	5(%r9,%rbp), %esi
	movw	%r14w, (%r8,%rcx,2)
	leal	6(%r9,%rbp), %ecx
	movw	%r14w, (%r8,%rsi,2)
	leal	7(%r9,%rbp), %esi
	movw	%r14w, (%r8,%rcx,2)
	movw	%r14w, (%r8,%rsi,2)
	addl	$8, %ebp
	cmpl	%ebp, %eax
	jne	.LBB7_28
.LBB7_29:                               #   in Loop: Header=BB7_5 Depth=2
	movl	%r12d, %r9d
.LBB7_30:                               # %..loopexit109_crit_edge.us199
                                        #   in Loop: Header=BB7_5 Depth=2
	incl	%r14d
	movzwl	%r14w, %eax
	cmpl	%edi, %eax
	jb	.LBB7_5
	jmp	.LBB7_35
	.p2align	4, 0x90
.LBB7_31:                               # %.lr.ph177.split.us.us.preheader
                                        #   in Loop: Header=BB7_3 Depth=1
	movw	$1, %ax
	.p2align	4, 0x90
.LBB7_32:                               # %.lr.ph177.split.us.us
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rax), %ecx
	movzwl	%cx, %ecx
	cmpb	%r11b, (%rdx,%rcx)
	jne	.LBB7_34
# BB#33:                                # %.lr.ph177.split.us.us
                                        #   in Loop: Header=BB7_32 Depth=2
	cmpl	%r13d, %r9d
	ja	.LBB7_83
.LBB7_34:                               # %.loopexit109.us.us
                                        #   in Loop: Header=BB7_32 Depth=2
	movzwl	%ax, %ecx
	leal	1(%rax), %eax
	cmpl	%edi, %ecx
	jb	.LBB7_32
.LBB7_35:                               # %._crit_edge178.us
                                        #   in Loop: Header=BB7_3 Depth=1
	shrl	%r15d
	incb	%r11b
	movzbl	%r11b, %eax
	movq	-8(%rsp), %rsi          # 8-byte Reload
	cmpl	%esi, %eax
	jbe	.LBB7_3
# BB#36:                                # %._crit_edge187
	xorl	%eax, %eax
	cmpl	%r13d, %r9d
	jne	.LBB7_39
	jmp	.LBB7_84
.LBB7_37:                               # %.preheader110.preheader
	movb	$2, %al
	.p2align	4, 0x90
.LBB7_38:                               # %.preheader110
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%al, %ecx
	movl	%ecx, %eax
	incb	%al
	cmpl	%esi, %ecx
	jbe	.LBB7_38
.LBB7_39:                               # %._crit_edge187.thread
	movzwl	%r9w, %eax
	cmpl	%r13d, %eax
	jae	.LBB7_60
# BB#40:                                # %.lr.ph168.preheader
	leal	1(%r9), %eax
	movzwl	%ax, %eax
	cmpl	%eax, %r13d
	movl	%eax, %r15d
	cmoval	%r13d, %r15d
	incl	%r15d
	subl	%eax, %r15d
	cmpl	$16, %r15d
	jae	.LBB7_42
# BB#41:
	movw	%r9w, %bx
	jmp	.LBB7_59
.LBB7_42:                               # %min.iters.checked242
	movl	%r15d, %r11d
	andl	$-16, %r11d
	je	.LBB7_50
# BB#43:                                # %vector.scevcheck254
	leal	1(%r9), %eax
	movzwl	%ax, %ebx
	cmpl	%ebx, %r13d
	movl	%ebx, %eax
	cmoval	%r13d, %eax
	subl	%ebx, %eax
	movl	%r9d, %ecx
	addw	%ax, %cx
	setb	%cl
	addw	%ax, %bx
	jb	.LBB7_56
# BB#44:                                # %vector.scevcheck254
	cmpl	$65535, %eax            # imm = 0xFFFF
	ja	.LBB7_57
# BB#45:                                # %vector.scevcheck254
	testb	%cl, %cl
	jne	.LBB7_58
# BB#46:                                # %vector.scevcheck254
	cmpl	$65535, %eax            # imm = 0xFFFF
	movw	%r9w, %bx
	ja	.LBB7_59
# BB#47:                                # %vector.body238.preheader
	leal	-16(%r11), %ebp
	movl	%ebp, %ebx
	shrl	$4, %ebx
	incl	%ebx
	andl	$3, %ebx
	je	.LBB7_51
# BB#48:                                # %vector.body238.prol.preheader
	negl	%ebx
	xorl	%eax, %eax
	pcmpeqd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB7_49:                               # %vector.body238.prol
                                        # =>This Inner Loop Header: Depth=1
	leal	(%r9,%rax), %ecx
	movzwl	%cx, %ecx
	movdqu	%xmm0, (%r8,%rcx,2)
	movdqu	%xmm0, 16(%r8,%rcx,2)
	addl	$16, %eax
	incl	%ebx
	jne	.LBB7_49
	jmp	.LBB7_52
.LBB7_50:
	movw	%r9w, %bx
	jmp	.LBB7_59
.LBB7_51:
	xorl	%eax, %eax
.LBB7_52:                               # %vector.body238.prol.loopexit
	leal	(%r9,%r11), %ebx
	cmpl	$48, %ebp
	jb	.LBB7_55
# BB#53:                                # %vector.body238.preheader.new
	movl	%r11d, %r14d
	subl	%eax, %r14d
	leal	48(%r9,%rax), %ebp
	xorl	%eax, %eax
	pcmpeqd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB7_54:                               # %vector.body238
                                        # =>This Inner Loop Header: Depth=1
	leal	-48(%rbp,%rax), %ecx
	movzwl	%cx, %ecx
	movdqu	%xmm0, (%r8,%rcx,2)
	movdqu	%xmm0, 16(%r8,%rcx,2)
	leal	-32(%rbp,%rax), %ecx
	movzwl	%cx, %ecx
	movdqu	%xmm0, (%r8,%rcx,2)
	movdqu	%xmm0, 16(%r8,%rcx,2)
	leal	-16(%rbp,%rax), %ecx
	movzwl	%cx, %ecx
	movdqu	%xmm0, (%r8,%rcx,2)
	movdqu	%xmm0, 16(%r8,%rcx,2)
	leal	(%rbp,%rax), %ecx
	movzwl	%cx, %ecx
	movdqu	%xmm0, (%r8,%rcx,2)
	movdqu	%xmm0, 16(%r8,%rcx,2)
	addl	$64, %eax
	cmpl	%eax, %r14d
	jne	.LBB7_54
.LBB7_55:                               # %middle.block239
	cmpl	%r11d, %r15d
	jne	.LBB7_59
	jmp	.LBB7_60
.LBB7_56:
	movw	%r9w, %bx
	jmp	.LBB7_59
.LBB7_57:
	movw	%r9w, %bx
	jmp	.LBB7_59
.LBB7_58:
	movw	%r9w, %bx
	.p2align	4, 0x90
.LBB7_59:                               # %.lr.ph168
                                        # =>This Inner Loop Header: Depth=1
	movzwl	%bx, %eax
	movw	$-1, (%r8,%rax,2)
	leal	1(%rbx), %ebx
	movzwl	%bx, %eax
	cmpl	%r13d, %eax
	jb	.LBB7_59
.LBB7_60:                               # %._crit_edge169
	shll	$16, %r9d
	shll	$16, %r13d
	leal	1(%rsi), %eax
	cmpb	$16, %al
	ja	.LBB7_77
# BB#61:                                # %.preheader106.lr.ph
	testl	%edi, %edi
	je	.LBB7_82
# BB#62:                                # %.preheader106.us.preheader
	movq	%rsi, %rcx
	movzbl	%al, %r14d
	movl	%r14d, %ebp
	subl	%ecx, %ebp
	movl	$32768, %r11d           # imm = 0x8000
.LBB7_63:                               # %.preheader106.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_69 Depth 2
                                        #       Child Loop BB7_71 Depth 3
                                        #     Child Loop BB7_65 Depth 2
	movq	%rcx, %r12
	cmpl	%ecx, %r14d
	jne	.LBB7_68
# BB#64:                                # %.lr.ph130..lr.ph130.split_crit_edge.us.preheader
                                        #   in Loop: Header=BB7_63 Depth=1
	movw	$1, %ax
	.p2align	4, 0x90
.LBB7_65:                               # %.lr.ph130..lr.ph130.split_crit_edge.us
                                        #   Parent Loop BB7_63 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rax), %ecx
	movzwl	%cx, %esi
	movzbl	(%rdx,%rsi), %esi
	cmpl	%r14d, %esi
	jne	.LBB7_67
# BB#66:                                #   in Loop: Header=BB7_65 Depth=2
	movl	%r9d, %esi
	shrl	$16, %esi
	movw	%cx, (%r8,%rsi,2)
	addl	%r11d, %r9d
	cmpl	%r13d, %r9d
	ja	.LBB7_83
.LBB7_67:                               #   in Loop: Header=BB7_65 Depth=2
	movzwl	%ax, %ecx
	leal	1(%rax), %eax
	cmpl	%edi, %ecx
	jb	.LBB7_65
	jmp	.LBB7_76
	.p2align	4, 0x90
.LBB7_68:                               # %.lr.ph130.split.us.us.preheader
                                        #   in Loop: Header=BB7_63 Depth=1
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB7_69:                               # %.lr.ph130.split.us.us
                                        #   Parent Loop BB7_63 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_71 Depth 3
	movzwl	%r15w, %eax
	movzbl	(%rdx,%rax), %eax
	cmpl	%r14d, %eax
	jne	.LBB7_75
# BB#70:                                # %.lr.ph122.us.us
                                        #   in Loop: Header=BB7_69 Depth=2
	movl	%r9d, %eax
	shrl	$16, %eax
	leaq	(%r8,%rax,2), %rbx
	movl	$15, %eax
	.p2align	4, 0x90
.LBB7_71:                               #   Parent Loop BB7_63 Depth=1
                                        #     Parent Loop BB7_69 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	(%rbx), %ecx
	cmpl	$65535, %ecx            # imm = 0xFFFF
	jne	.LBB7_73
# BB#72:                                #   in Loop: Header=BB7_71 Depth=3
	leal	(%r10,%r10), %ecx
	movw	$-1, (%r8,%rcx,2)
	leal	1(%r10,%r10), %ecx
	movw	$-1, (%r8,%rcx,2)
	movw	%r10w, (%rbx)
	movw	%r10w, %cx
	leal	1(%r10), %esi
	movl	%esi, %r10d
.LBB7_73:                               #   in Loop: Header=BB7_71 Depth=3
	movzwl	%cx, %ecx
	btl	%eax, %r9d
	sbbq	%rsi, %rsi
	andl	$1, %esi
	leaq	(%rsi,%rcx,2), %rcx
	leaq	(%r8,%rcx,2), %rbx
	leal	-1(%rbp,%rax), %ecx
	leal	-1(%rax), %eax
	cmpl	$15, %ecx
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	jne	.LBB7_71
# BB#74:                                # %._crit_edge.us.us
                                        #   in Loop: Header=BB7_69 Depth=2
	movw	%r15w, (%rbx)
	addl	%r11d, %r9d
	cmpl	%r13d, %r9d
	ja	.LBB7_83
.LBB7_75:                               #   in Loop: Header=BB7_69 Depth=2
	incl	%r15d
	movzwl	%r15w, %eax
	cmpl	%edi, %eax
	jb	.LBB7_69
.LBB7_76:                               # %._crit_edge131.us
                                        #   in Loop: Header=BB7_63 Depth=1
	shrl	%r11d
	incl	%r14d
	incl	%ebp
	cmpl	$17, %r14d
	movq	%r12, %rcx
	jb	.LBB7_63
.LBB7_77:                               # %._crit_edge142
	xorl	%eax, %eax
	cmpl	%r13d, %r9d
	je	.LBB7_84
# BB#78:                                # %._crit_edge142
	testl	%edi, %edi
	je	.LBB7_84
# BB#79:                                # %.lr.ph.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_80:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	%ax, %ecx
	cmpb	$0, (%rdx,%rcx)
	jne	.LBB7_83
# BB#81:                                #   in Loop: Header=BB7_80 Depth=1
	incl	%eax
	movzwl	%ax, %ecx
	cmpl	%edi, %ecx
	jb	.LBB7_80
.LBB7_82:
	xorl	%eax, %eax
	jmp	.LBB7_84
.LBB7_83:
	movl	$1, %eax
.LBB7_84:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	lzx_make_decode_table, .Lfunc_end7-lzx_make_decode_table
	.cfi_endproc

	.p2align	4, 0x90
	.type	lzx_read_lens,@function
lzx_read_lens:                          # @lzx_read_lens
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi70:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi71:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi73:
	.cfi_def_cfa_offset 80
.Lcfi74:
	.cfi_offset %rbx, -56
.Lcfi75:
	.cfi_offset %r12, -48
.Lcfi76:
	.cfi_offset %r13, -40
.Lcfi77:
	.cfi_offset %r14, -32
.Lcfi78:
	.cfi_offset %r15, -24
.Lcfi79:
	.cfi_offset %rbp, -16
	movl	%ecx, 12(%rsp)          # 4-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdi, %r13
	movq	112(%r13), %r15
	movq	120(%r13), %rsi
	movl	144(%r13), %ebx
	movl	148(%r13), %r14d
	xorl	%r12d, %r12d
.LBB8_1:                                # %.preheader285
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_3 Depth 2
	cmpl	$3, %r14d
	jg	.LBB8_15
# BB#2:                                 # %.lr.ph470.preheader
                                        #   in Loop: Header=BB8_1 Depth=1
	movl	$16, %ebp
	subl	%r14d, %ebp
	.p2align	4, 0x90
.LBB8_3:                                # %.lr.ph470
                                        #   Parent Loop BB8_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%r15), %rax
	cmpq	%rsi, %rax
	jb	.LBB8_14
# BB#4:                                 #   in Loop: Header=BB8_3 Depth=2
	movq	54888(%r13), %rax
	testq	%rax, %rax
	movq	104(%r13), %rsi
	movl	152(%r13), %edx
	je	.LBB8_6
# BB#5:                                 #   in Loop: Header=BB8_3 Depth=2
	movq	54880(%r13), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB8_8
	jmp	.LBB8_11
.LBB8_6:                                #   in Loop: Header=BB8_3 Depth=2
	movl	(%r13), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB8_11
.LBB8_8:                                #   in Loop: Header=BB8_3 Depth=2
	jne	.LBB8_13
# BB#9:                                 #   in Loop: Header=BB8_3 Depth=2
	cmpb	$0, 92(%r13)
	jne	.LBB8_10
# BB#12:                                #   in Loop: Header=BB8_3 Depth=2
	movq	104(%r13), %rax
	movb	$0, 1(%rax)
	movq	104(%r13), %rax
	movb	$0, (%rax)
	movb	$1, 92(%r13)
	movl	$2, %eax
.LBB8_13:                               #   in Loop: Header=BB8_3 Depth=2
	movq	104(%r13), %r15
	movq	%r15, 112(%r13)
	movslq	%eax, %rsi
	addq	%r15, %rsi
	movq	%rsi, 120(%r13)
.LBB8_14:                               #   in Loop: Header=BB8_3 Depth=2
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	orl	%edx, %ebx
	addl	$16, %r14d
	addq	$2, %r15
	addl	$-16, %ebp
	cmpl	$4, %r14d
	jl	.LBB8_3
.LBB8_15:                               # %._crit_edge471
                                        #   in Loop: Header=BB8_1 Depth=1
	movl	%ebx, %eax
	shrl	$28, %eax
	shll	$4, %ebx
	addl	$-4, %r14d
	movb	%al, 156(%r13,%r12)
	incq	%r12
	cmpq	$20, %r12
	jb	.LBB8_1
# BB#16:
	movq	%rsi, %rbp
	leaq	156(%r13), %rdx
	leaq	1346(%r13), %rcx
	movl	$20, %edi
	movl	$6, %esi
	callq	lzx_make_decode_table
	testl	%eax, %eax
	je	.LBB8_17
# BB#19:
	movl	$.L.str.4, %edi
	movl	$.L.str.25, %esi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB8_39:
	movl	$-124, 96(%r13)
	movl	$-124, %eax
	jmp	.LBB8_134
.LBB8_17:                               # %.preheader284
	movl	12(%rsp), %eax          # 4-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	cmpl	%eax, %r12d
	jae	.LBB8_18
# BB#20:
	xorps	%xmm0, %xmm0
	movq	%rbp, %r10
.LBB8_21:                               # %.preheader282
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_24 Depth 2
                                        #     Child Loop BB8_36 Depth 2
                                        #     Child Loop BB8_48 Depth 2
                                        #     Child Loop BB8_113 Depth 2
                                        #     Child Loop BB8_126 Depth 2
                                        #     Child Loop BB8_53 Depth 2
                                        #     Child Loop BB8_90 Depth 2
                                        #     Child Loop BB8_93 Depth 2
                                        #     Child Loop BB8_98 Depth 2
                                        #     Child Loop BB8_101 Depth 2
                                        #     Child Loop BB8_58 Depth 2
                                        #     Child Loop BB8_71 Depth 2
                                        #     Child Loop BB8_74 Depth 2
	cmpl	$15, %r14d
	jg	.LBB8_22
# BB#23:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB8_21 Depth=1
	movl	$16, %ebp
	subl	%r14d, %ebp
	.p2align	4, 0x90
.LBB8_24:                               # %.lr.ph
                                        #   Parent Loop BB8_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%r15), %rax
	cmpq	%r10, %rax
	jb	.LBB8_33
# BB#25:                                #   in Loop: Header=BB8_24 Depth=2
	movq	54888(%r13), %rax
	testq	%rax, %rax
	movq	104(%r13), %rsi
	movl	152(%r13), %edx
	je	.LBB8_27
# BB#26:                                #   in Loop: Header=BB8_24 Depth=2
	movq	54880(%r13), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB8_29
	jmp	.LBB8_11
.LBB8_27:                               #   in Loop: Header=BB8_24 Depth=2
	movl	(%r13), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB8_11
.LBB8_29:                               #   in Loop: Header=BB8_24 Depth=2
	xorps	%xmm0, %xmm0
	jne	.LBB8_32
# BB#30:                                #   in Loop: Header=BB8_24 Depth=2
	cmpb	$0, 92(%r13)
	jne	.LBB8_10
# BB#31:                                #   in Loop: Header=BB8_24 Depth=2
	movq	104(%r13), %rax
	movb	$0, 1(%rax)
	movq	104(%r13), %rax
	movb	$0, (%rax)
	movb	$1, 92(%r13)
	movl	$2, %eax
.LBB8_32:                               #   in Loop: Header=BB8_24 Depth=2
	movq	104(%r13), %r15
	movq	%r15, 112(%r13)
	movslq	%eax, %r10
	addq	%r15, %r10
	movq	%r10, 120(%r13)
.LBB8_33:                               #   in Loop: Header=BB8_24 Depth=2
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	orl	%edx, %ebx
	leal	16(%r14), %eax
	addq	$2, %r15
	addl	$-16, %ebp
	testl	%r14d, %r14d
	movl	%eax, %r14d
	js	.LBB8_24
	jmp	.LBB8_34
.LBB8_22:                               #   in Loop: Header=BB8_21 Depth=1
	movl	%r14d, %eax
.LBB8_34:                               # %._crit_edge
                                        #   in Loop: Header=BB8_21 Depth=1
	movl	%ebx, %ecx
	shrl	$26, %ecx
	movzwl	1346(%r13,%rcx,2), %ecx
	cmpl	$20, %ecx
	jb	.LBB8_43
# BB#35:                                # %.preheader280.preheader
                                        #   in Loop: Header=BB8_21 Depth=1
	movl	$67108864, %edx         # imm = 0x4000000
.LBB8_36:                               # %.preheader280
                                        #   Parent Loop BB8_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	sarl	%edx
	je	.LBB8_37
# BB#40:                                #   in Loop: Header=BB8_36 Depth=2
	addl	%ecx, %ecx
	movzwl	%cx, %esi
	cmpl	$104, %esi
	jae	.LBB8_41
# BB#42:                                #   in Loop: Header=BB8_36 Depth=2
	xorl	%esi, %esi
	testl	%ebx, %edx
	setne	%sil
	orl	%esi, %ecx
	movzwl	%cx, %ecx
	movzwl	1346(%r13,%rcx,2), %ecx
	cmpl	$19, %ecx
	ja	.LBB8_36
.LBB8_43:                               # %.loopexit281
                                        #   in Loop: Header=BB8_21 Depth=1
	movzwl	%cx, %edx
	movzbl	156(%r13,%rdx), %ebp
	movl	%ebp, %ecx
	shll	%cl, %ebx
	movl	%eax, %r14d
	subl	%ebp, %r14d
	cmpl	$17, %edx
	je	.LBB8_56
# BB#44:                                # %.loopexit281
                                        #   in Loop: Header=BB8_21 Depth=1
	cmpl	$18, %edx
	movq	%r12, %rcx
	je	.LBB8_51
# BB#45:                                # %.loopexit281
                                        #   in Loop: Header=BB8_21 Depth=1
	cmpl	$19, %edx
	jne	.LBB8_135
# BB#46:                                # %.preheader278
                                        #   in Loop: Header=BB8_21 Depth=1
	testl	%r14d, %r14d
	jg	.LBB8_110
# BB#47:                                # %.lr.ph413.preheader
                                        #   in Loop: Header=BB8_21 Depth=1
	addl	$16, %ebp
	subl	%eax, %ebp
.LBB8_48:                               # %.lr.ph413
                                        #   Parent Loop BB8_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%r15), %rax
	cmpq	%r10, %rax
	jb	.LBB8_109
# BB#49:                                #   in Loop: Header=BB8_48 Depth=2
	movq	54888(%r13), %rax
	testq	%rax, %rax
	movq	104(%r13), %rsi
	movl	152(%r13), %edx
	je	.LBB8_103
# BB#50:                                #   in Loop: Header=BB8_48 Depth=2
	movq	54880(%r13), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB8_105
	jmp	.LBB8_11
.LBB8_103:                              #   in Loop: Header=BB8_48 Depth=2
	movl	(%r13), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB8_11
.LBB8_105:                              #   in Loop: Header=BB8_48 Depth=2
	xorps	%xmm0, %xmm0
	jne	.LBB8_108
# BB#106:                               #   in Loop: Header=BB8_48 Depth=2
	cmpb	$0, 92(%r13)
	jne	.LBB8_10
# BB#107:                               #   in Loop: Header=BB8_48 Depth=2
	movq	104(%r13), %rax
	movb	$0, 1(%rax)
	movq	104(%r13), %rax
	movb	$0, (%rax)
	movb	$1, 92(%r13)
	movl	$2, %eax
.LBB8_108:                              #   in Loop: Header=BB8_48 Depth=2
	movq	104(%r13), %r15
	movq	%r15, 112(%r13)
	movslq	%eax, %r10
	addq	%r15, %r10
	movq	%r10, 120(%r13)
.LBB8_109:                              #   in Loop: Header=BB8_48 Depth=2
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	orl	%edx, %ebx
	addq	$2, %r15
	addl	$-16, %ebp
	addl	$16, %r14d
	jle	.LBB8_48
.LBB8_110:                              # %._crit_edge414
                                        #   in Loop: Header=BB8_21 Depth=1
	leal	(%rbx,%rbx), %edi
	cmpl	$16, %r14d
	jg	.LBB8_111
# BB#112:                               # %.lr.ph424.preheader
                                        #   in Loop: Header=BB8_21 Depth=1
	movl	$17, %ebp
	subl	%r14d, %ebp
	addl	$-17, %r14d
.LBB8_113:                              # %.lr.ph424
                                        #   Parent Loop BB8_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%r15), %rax
	cmpq	%r10, %rax
	jb	.LBB8_122
# BB#114:                               #   in Loop: Header=BB8_113 Depth=2
	movl	%edi, %r15d
	movq	54888(%r13), %rax
	testq	%rax, %rax
	movq	104(%r13), %rsi
	movl	152(%r13), %edx
	je	.LBB8_116
# BB#115:                               #   in Loop: Header=BB8_113 Depth=2
	movq	54880(%r13), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB8_118
	jmp	.LBB8_11
.LBB8_116:                              #   in Loop: Header=BB8_113 Depth=2
	movl	(%r13), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB8_11
.LBB8_118:                              #   in Loop: Header=BB8_113 Depth=2
	xorps	%xmm0, %xmm0
	movl	%r15d, %edi
	jne	.LBB8_121
# BB#119:                               #   in Loop: Header=BB8_113 Depth=2
	cmpb	$0, 92(%r13)
	jne	.LBB8_10
# BB#120:                               #   in Loop: Header=BB8_113 Depth=2
	movq	104(%r13), %rax
	movb	$0, 1(%rax)
	movq	104(%r13), %rax
	movb	$0, (%rax)
	movb	$1, 92(%r13)
	movl	$2, %eax
.LBB8_121:                              #   in Loop: Header=BB8_113 Depth=2
	movq	104(%r13), %r15
	movq	%r15, 112(%r13)
	movslq	%eax, %r10
	addq	%r15, %r10
	movq	%r10, 120(%r13)
.LBB8_122:                              #   in Loop: Header=BB8_113 Depth=2
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	orl	%edx, %edi
	addq	$2, %r15
	addl	$-16, %ebp
	addl	$16, %r14d
	js	.LBB8_113
# BB#123:                               # %._crit_edge425.loopexit
                                        #   in Loop: Header=BB8_21 Depth=1
	addl	$16, %r14d
	jmp	.LBB8_124
.LBB8_56:                               # %.preheader
                                        #   in Loop: Header=BB8_21 Depth=1
	cmpl	$3, %r14d
	jg	.LBB8_68
# BB#57:                                # %.lr.ph448.preheader
                                        #   in Loop: Header=BB8_21 Depth=1
	addl	$16, %ebp
	subl	%eax, %ebp
.LBB8_58:                               # %.lr.ph448
                                        #   Parent Loop BB8_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%r15), %rax
	cmpq	%r10, %rax
	jb	.LBB8_67
# BB#59:                                #   in Loop: Header=BB8_58 Depth=2
	movq	54888(%r13), %rax
	testq	%rax, %rax
	movq	104(%r13), %rsi
	movl	152(%r13), %edx
	je	.LBB8_61
# BB#60:                                #   in Loop: Header=BB8_58 Depth=2
	movq	54880(%r13), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB8_63
	jmp	.LBB8_11
.LBB8_61:                               #   in Loop: Header=BB8_58 Depth=2
	movl	(%r13), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB8_11
.LBB8_63:                               #   in Loop: Header=BB8_58 Depth=2
	xorps	%xmm0, %xmm0
	jne	.LBB8_66
# BB#64:                                #   in Loop: Header=BB8_58 Depth=2
	cmpb	$0, 92(%r13)
	jne	.LBB8_10
# BB#65:                                #   in Loop: Header=BB8_58 Depth=2
	movq	104(%r13), %rax
	movb	$0, 1(%rax)
	movq	104(%r13), %rax
	movb	$0, (%rax)
	movb	$1, 92(%r13)
	movl	$2, %eax
.LBB8_66:                               #   in Loop: Header=BB8_58 Depth=2
	movq	104(%r13), %r15
	movq	%r15, 112(%r13)
	movslq	%eax, %r10
	addq	%r15, %r10
	movq	%r10, 120(%r13)
.LBB8_67:                               #   in Loop: Header=BB8_58 Depth=2
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	orl	%edx, %ebx
	addl	$16, %r14d
	addq	$2, %r15
	addl	$-16, %ebp
	cmpl	$4, %r14d
	jl	.LBB8_58
.LBB8_68:                               # %._crit_edge449
                                        #   in Loop: Header=BB8_21 Depth=1
	movl	%ebx, %r9d
	shrl	$28, %r9d
	leal	4(%r9), %ecx
	leal	3(%r9), %r8d
	movl	%ecx, %edi
	andl	$7, %edi
	je	.LBB8_69
# BB#70:                                # %.prol.preheader
                                        #   in Loop: Header=BB8_21 Depth=1
	negl	%edi
	movl	%r12d, %edx
.LBB8_71:                               #   Parent Loop BB8_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%ecx
	movl	%edx, %ebp
	incl	%edx
	movq	(%rsp), %rsi            # 8-byte Reload
	movb	$0, (%rsi,%rbp)
	incl	%edi
	jne	.LBB8_71
	jmp	.LBB8_72
.LBB8_51:                               # %.preheader272
                                        #   in Loop: Header=BB8_21 Depth=1
	cmpl	$4, %r14d
	jg	.LBB8_83
# BB#52:                                # %.lr.ph436.preheader
                                        #   in Loop: Header=BB8_21 Depth=1
	addl	$16, %ebp
	subl	%eax, %ebp
.LBB8_53:                               # %.lr.ph436
                                        #   Parent Loop BB8_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%r15), %rax
	cmpq	%r10, %rax
	jb	.LBB8_82
# BB#54:                                #   in Loop: Header=BB8_53 Depth=2
	movq	54888(%r13), %rax
	testq	%rax, %rax
	movq	104(%r13), %rsi
	movl	152(%r13), %edx
	je	.LBB8_76
# BB#55:                                #   in Loop: Header=BB8_53 Depth=2
	movq	54880(%r13), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB8_78
	jmp	.LBB8_11
.LBB8_76:                               #   in Loop: Header=BB8_53 Depth=2
	movl	(%r13), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB8_11
.LBB8_78:                               #   in Loop: Header=BB8_53 Depth=2
	xorps	%xmm0, %xmm0
	jne	.LBB8_81
# BB#79:                                #   in Loop: Header=BB8_53 Depth=2
	cmpb	$0, 92(%r13)
	jne	.LBB8_10
# BB#80:                                #   in Loop: Header=BB8_53 Depth=2
	movq	104(%r13), %rax
	movb	$0, 1(%rax)
	movq	104(%r13), %rax
	movb	$0, (%rax)
	movb	$1, 92(%r13)
	movl	$2, %eax
.LBB8_81:                               #   in Loop: Header=BB8_53 Depth=2
	movq	104(%r13), %r15
	movq	%r15, 112(%r13)
	movslq	%eax, %r10
	addq	%r15, %r10
	movq	%r10, 120(%r13)
.LBB8_82:                               #   in Loop: Header=BB8_53 Depth=2
	movzbl	1(%r15), %eax
	shll	$8, %eax
	movzbl	(%r15), %edx
	orl	%eax, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	movq	%r12, %rcx
	orl	%edx, %ebx
	addl	$16, %r14d
	addq	$2, %r15
	addl	$-16, %ebp
	cmpl	$5, %r14d
	jl	.LBB8_53
.LBB8_83:                               # %._crit_edge437
                                        #   in Loop: Header=BB8_21 Depth=1
	movq	%r10, %r9
	movl	%ebx, %r11d
	shrl	$27, %r11d
	leal	20(%r11), %edx
	leal	20(%rcx), %r10d
	cmpl	$32, %edx
	jae	.LBB8_85
# BB#84:                                #   in Loop: Header=BB8_21 Depth=1
	movq	(%rsp), %rbp            # 8-byte Reload
	jmp	.LBB8_96
.LBB8_135:                              #   in Loop: Header=BB8_21 Depth=1
	movl	%ecx, %eax
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rax), %ecx
	movl	%ecx, %esi
	subl	%edx, %esi
	addl	$17, %esi
	subl	%edx, %ecx
	cmovsl	%esi, %ecx
	incl	%r12d
	movb	%cl, (%rdi,%rax)
	movl	%ebx, %edi
	jmp	.LBB8_132
.LBB8_111:                              #   in Loop: Header=BB8_21 Depth=1
	decl	%r14d
.LBB8_124:                              # %._crit_edge425
                                        #   in Loop: Header=BB8_21 Depth=1
	movl	%edi, %eax
	shrl	$26, %eax
	movzwl	1346(%r13,%rax,2), %eax
	cmpl	$20, %eax
	movq	(%rsp), %rbp            # 8-byte Reload
	jb	.LBB8_129
# BB#125:                               # %.preheader275.preheader
                                        #   in Loop: Header=BB8_21 Depth=1
	movl	$67108864, %ecx         # imm = 0x4000000
.LBB8_126:                              # %.preheader275
                                        #   Parent Loop BB8_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	sarl	%ecx
	je	.LBB8_37
# BB#127:                               #   in Loop: Header=BB8_126 Depth=2
	addl	%eax, %eax
	movzwl	%ax, %edx
	cmpl	$104, %edx
	jae	.LBB8_41
# BB#128:                               #   in Loop: Header=BB8_126 Depth=2
	xorl	%edx, %edx
	testl	%edi, %ecx
	setne	%dl
	orl	%edx, %eax
	movzwl	%ax, %eax
	movzwl	1346(%r13,%rax,2), %eax
	cmpl	$19, %eax
	ja	.LBB8_126
.LBB8_129:                              #   in Loop: Header=BB8_21 Depth=1
	shrl	$31, %ebx
	orl	$4, %ebx
	movzwl	%ax, %ecx
	movl	%r12d, %edx
	movzbl	(%rbp,%rdx), %eax
	movl	%eax, %esi
	subl	%ecx, %esi
	addl	$17, %esi
	subl	%ecx, %eax
	movzbl	156(%r13,%rcx), %ecx
	cmovsl	%esi, %eax
	shll	%cl, %edi
	movb	%al, (%rbp,%rdx)
	leal	1(%r12), %edx
	movb	%al, (%rbp,%rdx)
	leal	2(%r12), %edx
	movb	%al, (%rbp,%rdx)
	leal	3(%r12), %edx
	movb	%al, (%rbp,%rdx)
	cmpl	$4, %ebx
	je	.LBB8_131
# BB#130:                               #   in Loop: Header=BB8_21 Depth=1
	leal	4(%r12), %edx
	movb	%al, (%rbp,%rdx)
.LBB8_131:                              # %.backedge.loopexit482
                                        #   in Loop: Header=BB8_21 Depth=1
	subl	%ecx, %r14d
	addl	%ebx, %r12d
	jmp	.LBB8_132
.LBB8_85:                               # %min.iters.checked561
                                        #   in Loop: Header=BB8_21 Depth=1
	movl	%edx, %r8d
	andl	$32, %r8d
	movq	(%rsp), %rbp            # 8-byte Reload
	je	.LBB8_96
# BB#86:                                # %vector.scevcheck569
                                        #   in Loop: Header=BB8_21 Depth=1
	leal	19(%r11), %esi
	addl	%ecx, %esi
	jb	.LBB8_96
# BB#87:                                # %vector.body557.preheader
                                        #   in Loop: Header=BB8_21 Depth=1
	leal	-32(%r8), %esi
	movl	%esi, 16(%rsp)          # 4-byte Spill
	shrl	$5, %esi
	incl	%esi
	andl	$3, %esi
	je	.LBB8_88
# BB#89:                                # %vector.body557.prol.preheader
                                        #   in Loop: Header=BB8_21 Depth=1
	negl	%esi
	xorl	%edi, %edi
	movq	%rbp, %rcx
	movq	%r12, %rax
.LBB8_90:                               # %vector.body557.prol
                                        #   Parent Loop BB8_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rax,%rdi), %ebp
	movups	%xmm0, (%rcx,%rbp)
	movups	%xmm0, 16(%rcx,%rbp)
	addl	$32, %edi
	incl	%esi
	jne	.LBB8_90
	jmp	.LBB8_91
.LBB8_69:                               #   in Loop: Header=BB8_21 Depth=1
	movl	%r12d, %edx
.LBB8_72:                               # %.prol.loopexit
                                        #   in Loop: Header=BB8_21 Depth=1
	addl	$4, %r12d
	cmpl	$7, %r8d
	jb	.LBB8_75
# BB#73:                                # %._crit_edge449.new
                                        #   in Loop: Header=BB8_21 Depth=1
	xorl	%esi, %esi
.LBB8_74:                               #   Parent Loop BB8_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rdx,%rsi), %edi
	leal	1(%rdx,%rsi), %ebp
	movq	(%rsp), %rax            # 8-byte Reload
	movb	$0, (%rax,%rdi)
	leal	2(%rdx,%rsi), %edi
	movq	(%rsp), %rax            # 8-byte Reload
	movb	$0, (%rax,%rbp)
	leal	3(%rdx,%rsi), %ebp
	movq	(%rsp), %rax            # 8-byte Reload
	movb	$0, (%rax,%rdi)
	leal	4(%rdx,%rsi), %edi
	movq	(%rsp), %rax            # 8-byte Reload
	movb	$0, (%rax,%rbp)
	leal	5(%rdx,%rsi), %ebp
	movq	(%rsp), %rax            # 8-byte Reload
	movb	$0, (%rax,%rdi)
	leal	6(%rdx,%rsi), %edi
	movq	(%rsp), %rax            # 8-byte Reload
	movb	$0, (%rax,%rbp)
	leal	7(%rdx,%rsi), %ebp
	movq	(%rsp), %rax            # 8-byte Reload
	movb	$0, (%rax,%rdi)
	movq	(%rsp), %rax            # 8-byte Reload
	movb	$0, (%rax,%rbp)
	addl	$8, %esi
	cmpl	%esi, %ecx
	jne	.LBB8_74
.LBB8_75:                               # %.backedge.loopexit
                                        #   in Loop: Header=BB8_21 Depth=1
	shll	$4, %ebx
	addl	$-4, %r14d
	addl	%r9d, %r12d
	movl	%ebx, %edi
	jmp	.LBB8_132
.LBB8_88:                               #   in Loop: Header=BB8_21 Depth=1
	xorl	%edi, %edi
	movq	%rbp, %rcx
.LBB8_91:                               # %vector.body557.prol.loopexit
                                        #   in Loop: Header=BB8_21 Depth=1
	cmpl	$96, 16(%rsp)           # 4-byte Folded Reload
	jb	.LBB8_94
# BB#92:                                # %vector.body557.preheader.new
                                        #   in Loop: Header=BB8_21 Depth=1
	movl	%r12d, %ebp
	movl	%r8d, %esi
.LBB8_93:                               # %vector.body557
                                        #   Parent Loop BB8_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rdi,%rbp), %eax
	movups	%xmm0, (%rcx,%rax)
	movups	%xmm0, 16(%rcx,%rax)
	leal	32(%rdi,%rbp), %eax
	movups	%xmm0, (%rcx,%rax)
	movups	%xmm0, 16(%rcx,%rax)
	leal	64(%rdi,%rbp), %eax
	movups	%xmm0, (%rcx,%rax)
	movups	%xmm0, 16(%rcx,%rax)
	leal	96(%rdi,%rbp), %eax
	movups	%xmm0, (%rcx,%rax)
	movups	%xmm0, 16(%rcx,%rax)
	addl	$-128, %esi
	subl	$-128, %ebp
	cmpl	%esi, %edi
	jne	.LBB8_93
.LBB8_94:                               # %middle.block558
                                        #   in Loop: Header=BB8_21 Depth=1
	cmpl	%r8d, %edx
	movq	%rcx, %rbp
	movq	%r12, %rcx
	je	.LBB8_102
# BB#95:                                #   in Loop: Header=BB8_21 Depth=1
	subl	%r8d, %edx
	addl	%r8d, %ecx
.LBB8_96:                               # %scalar.ph559.preheader
                                        #   in Loop: Header=BB8_21 Depth=1
	leal	-1(%rdx), %esi
	movl	%edx, %edi
	andl	$7, %edi
	je	.LBB8_99
# BB#97:                                # %scalar.ph559.prol.preheader
                                        #   in Loop: Header=BB8_21 Depth=1
	negl	%edi
.LBB8_98:                               # %scalar.ph559.prol
                                        #   Parent Loop BB8_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%edx
	movl	%ecx, %eax
	incl	%ecx
	movb	$0, (%rbp,%rax)
	incl	%edi
	jne	.LBB8_98
.LBB8_99:                               # %scalar.ph559.prol.loopexit
                                        #   in Loop: Header=BB8_21 Depth=1
	cmpl	$7, %esi
	jb	.LBB8_102
# BB#100:                               # %scalar.ph559.preheader.new
                                        #   in Loop: Header=BB8_21 Depth=1
	xorl	%esi, %esi
.LBB8_101:                              # %scalar.ph559
                                        #   Parent Loop BB8_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rcx,%rsi), %eax
	leal	1(%rcx,%rsi), %edi
	movb	$0, (%rbp,%rax)
	leal	2(%rcx,%rsi), %eax
	movb	$0, (%rbp,%rdi)
	leal	3(%rcx,%rsi), %edi
	movb	$0, (%rbp,%rax)
	leal	4(%rcx,%rsi), %eax
	movb	$0, (%rbp,%rdi)
	leal	5(%rcx,%rsi), %edi
	movb	$0, (%rbp,%rax)
	leal	6(%rcx,%rsi), %eax
	movb	$0, (%rbp,%rdi)
	leal	7(%rcx,%rsi), %edi
	movb	$0, (%rbp,%rax)
	movb	$0, (%rbp,%rdi)
	addl	$8, %esi
	cmpl	%esi, %edx
	jne	.LBB8_101
.LBB8_102:                              # %.backedge.loopexit481
                                        #   in Loop: Header=BB8_21 Depth=1
	shll	$5, %ebx
	addl	$-5, %r14d
	addl	%r11d, %r10d
	movl	%ebx, %edi
	movl	%r10d, %r12d
	movq	%r9, %r10
.LBB8_132:                              # %.backedge
                                        #   in Loop: Header=BB8_21 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, %r12d
	movl	%edi, %ebx
	jb	.LBB8_21
	jmp	.LBB8_133
.LBB8_10:
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB8_11:                               # %.loopexit286
	movl	$-123, 96(%r13)
	movl	$-123, %eax
.LBB8_134:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_18:
	movl	%ebx, %edi
	movq	%rbp, %r10
.LBB8_133:                              # %._crit_edge461
	movq	%r15, 112(%r13)
	movq	%r10, 120(%r13)
	movl	%edi, 144(%r13)
	movl	%r14d, 148(%r13)
	xorl	%eax, %eax
	jmp	.LBB8_134
.LBB8_37:
	movl	$.L.str.9, %edi
	jmp	.LBB8_38
.LBB8_41:
	movl	$.L.str.10, %edi
.LBB8_38:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB8_39
.Lfunc_end8:
	.size	lzx_read_lens, .Lfunc_end8-lzx_read_lens
	.cfi_endproc

	.globl	lzx_free
	.p2align	4, 0x90
	.type	lzx_free,@function
lzx_free:                               # @lzx_free
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 16
.Lcfi81:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB9_1
# BB#2:
	movq	104(%rbx), %rdi
	callq	free
	movq	32(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB9_1:
	popq	%rbx
	retq
.Lfunc_end9:
	.size	lzx_free, .Lfunc_end9-lzx_free
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI10_0:
	.short	0                       # 0x0
	.short	27                      # 0x1b
	.short	1                       # 0x1
	.short	26                      # 0x1a
	.short	2                       # 0x2
	.short	25                      # 0x19
	.short	3                       # 0x3
	.short	24                      # 0x18
.LCPI10_1:
	.short	21                      # 0x15
	.short	7                       # 0x7
	.short	20                      # 0x14
	.short	8                       # 0x8
	.short	19                      # 0x13
	.short	9                       # 0x9
	.short	18                      # 0x12
	.short	10                      # 0xa
.LCPI10_2:
	.short	17                      # 0x11
	.short	11                      # 0xb
	.short	16                      # 0x10
	.short	12                      # 0xc
	.short	15                      # 0xf
	.short	13                      # 0xd
	.short	14                      # 0xe
	.short	14                      # 0xe
.LCPI10_3:
	.short	13                      # 0xd
	.short	15                      # 0xf
	.short	12                      # 0xc
	.short	16                      # 0x10
	.short	11                      # 0xb
	.short	17                      # 0x11
	.short	10                      # 0xa
	.short	18                      # 0x12
.LCPI10_4:
	.short	9                       # 0x9
	.short	19                      # 0x13
	.short	8                       # 0x8
	.short	20                      # 0x14
	.short	7                       # 0x7
	.short	21                      # 0x15
	.short	6                       # 0x6
	.short	22                      # 0x16
.LCPI10_5:
	.short	5                       # 0x5
	.short	23                      # 0x17
	.short	4                       # 0x4
	.short	24                      # 0x18
	.short	3                       # 0x3
	.short	25                      # 0x19
	.short	2                       # 0x2
	.short	26                      # 0x1a
.LCPI10_6:
	.short	1                       # 0x1
	.short	6                       # 0x6
	.short	2                       # 0x2
	.short	5                       # 0x5
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	3                       # 0x3
	.text
	.globl	qtm_init
	.p2align	4, 0x90
	.type	qtm_init,@function
qtm_init:                               # @qtm_init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi85:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi88:
	.cfi_def_cfa_offset 80
.Lcfi89:
	.cfi_offset %rbx, -56
.Lcfi90:
	.cfi_offset %r12, -48
.Lcfi91:
	.cfi_offset %r13, -40
.Lcfi92:
	.cfi_offset %r14, -32
.Lcfi93:
	.cfi_offset %r15, -24
.Lcfi94:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%esi, %r13d
	movl	%edi, %r14d
	movl	$1, %r15d
	movl	%edx, %ecx
	shll	%cl, %r15d
	leal	-15(%rdx), %eax
	cmpl	$6, %eax
	ja	.LBB10_49
# BB#2:
	incl	%r12d
	andl	$-2, %r12d
	cmpl	$2, %r12d
	jl	.LBB10_49
# BB#4:
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movl	$2136, %edi             # imm = 0x858
	callq	cli_malloc
	movq	%rax, %rbx
	xorl	%edi, %edi
	testq	%rbx, %rbx
	je	.LBB10_49
# BB#5:                                 # %.preheader151
	movb	$-1, %dl
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB10_6:                               # =>This Inner Loop Header: Depth=1
	movl	%edi, 100(%rbx,%rsi,4)
	xorl	%eax, %eax
	cmpq	$2, %rsi
	movl	$0, %ecx
	jl	.LBB10_8
# BB#7:                                 #   in Loop: Header=BB10_6 Depth=1
	movb	%dl, %cl
.LBB10_8:                               #   in Loop: Header=BB10_6 Depth=1
	movb	%cl, 268(%rbx,%rsi)
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	addl	%edi, %ebp
	movl	%ebp, 104(%rbx,%rsi,4)
	leaq	1(%rsi), %rcx
	cmpq	$2, %rcx
	jl	.LBB10_10
# BB#9:                                 #   in Loop: Header=BB10_6 Depth=1
	movb	%dl, %al
.LBB10_10:                              #   in Loop: Header=BB10_6 Depth=1
	movb	%al, 269(%rbx,%rsi)
	movl	$1, %edi
	movl	%eax, %ecx
	shll	%cl, %edi
	addl	%ebp, %edi
	incb	%dl
	addq	$2, %rsi
	cmpq	$42, %rsi
	jne	.LBB10_6
# BB#11:                                # %.preheader
	xorl	%edx, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB10_12:                              # =>This Inner Loop Header: Depth=1
	movb	%sil, 310(%rbx,%rdx)
	xorl	%eax, %eax
	cmpq	$2, %rdx
	movl	$0, %ecx
	jl	.LBB10_14
# BB#13:                                #   in Loop: Header=BB10_12 Depth=1
	leal	1022(%rdx), %ecx
	shrl	$2, %ecx
.LBB10_14:                              #   in Loop: Header=BB10_12 Depth=1
	movb	%cl, 337(%rbx,%rdx)
	movl	$1, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	addl	%esi, %edi
	movb	%dil, 311(%rbx,%rdx)
	leaq	1(%rdx), %rcx
	cmpq	$2, %rcx
	jl	.LBB10_16
# BB#15:                                #   in Loop: Header=BB10_12 Depth=1
	leal	1023(%rdx), %eax
	shrl	$2, %eax
.LBB10_16:                              #   in Loop: Header=BB10_12 Depth=1
	movb	%al, 338(%rbx,%rdx)
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	addl	%edi, %esi
	addq	$2, %rdx
	cmpq	$26, %rdx
	jne	.LBB10_12
# BB#17:
	movb	$-2, 336(%rbx)
	movb	$0, 363(%rbx)
	movl	%r15d, %edi
	callq	cli_malloc
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.LBB10_48
# BB#18:
	movslq	%r12d, %rdi
	callq	cli_malloc
	movq	%rax, 48(%rbx)
	testq	%rax, %rax
	je	.LBB10_47
# BB#19:
	movl	%r14d, (%rbx)
	movl	%r13d, 4(%rbx)
	movb	$1, 8(%rbx)
	movl	%r12d, 92(%rbx)
	movl	%r15d, 24(%rbx)
	movl	$0, 28(%rbx)
	movl	$0, 32(%rbx)
	movb	$0, 42(%rbx)
	movl	$0, 44(%rbx)
	movq	%rax, 64(%rbx)
	movq	%rax, 56(%rbx)
	movq	16(%rbx), %rax
	movq	%rax, 80(%rbx)
	movq	%rax, 72(%rbx)
	movb	$0, 96(%rbx)
	movl	$0, 88(%rbx)
	leaq	512(%rbx), %rax
	movl	$4, 368(%rbx)
	movl	$64, 372(%rbx)
	movq	%rax, 376(%rbx)
	movw	$60, %ax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB10_20:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movw	%cx, 512(%rbx,%rcx,4)
	leal	4(%rax), %edx
	movw	%dx, 514(%rbx,%rcx,4)
	leaq	1(%rcx), %rdx
	movw	%dx, 516(%rbx,%rcx,4)
	leal	3(%rax), %esi
	movw	%si, 518(%rbx,%rcx,4)
	incl	%edx
	movw	%dx, 520(%rbx,%rcx,4)
	leal	2(%rax), %edx
	movw	%dx, 522(%rbx,%rcx,4)
	leaq	3(%rcx), %rdx
	movw	%dx, 524(%rbx,%rcx,4)
	leal	1(%rax), %esi
	movw	%si, 526(%rbx,%rcx,4)
	incl	%edx
	movw	%dx, 528(%rbx,%rcx,4)
	movw	%ax, 530(%rbx,%rcx,4)
	leal	-5(%rax), %eax
	addq	$5, %rcx
	cmpq	$65, %rcx
	jne	.LBB10_20
# BB#21:                                # %qtm_init_model.exit
	movq	(%rsp), %r10            # 8-byte Reload
	leal	(%r10,%r10), %r9d
	leaq	772(%rbx), %rcx
	movl	$4, 384(%rbx)
	movl	$64, 388(%rbx)
	movq	%rcx, 392(%rbx)
	movw	$60, %cx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB10_22:                              # %.lr.ph.i149
                                        # =>This Inner Loop Header: Depth=1
	leal	64(%rdx), %esi
	movw	%si, 772(%rbx,%rdx,4)
	leal	4(%rcx), %esi
	movw	%si, 774(%rbx,%rdx,4)
	leal	65(%rdx), %esi
	movw	%si, 776(%rbx,%rdx,4)
	leal	3(%rcx), %esi
	movw	%si, 778(%rbx,%rdx,4)
	leal	66(%rdx), %esi
	movw	%si, 780(%rbx,%rdx,4)
	leal	2(%rcx), %esi
	movw	%si, 782(%rbx,%rdx,4)
	leal	67(%rdx), %esi
	movw	%si, 784(%rbx,%rdx,4)
	leal	1(%rcx), %esi
	movw	%si, 786(%rbx,%rdx,4)
	leal	68(%rdx), %esi
	movw	%si, 788(%rbx,%rdx,4)
	movw	%cx, 790(%rbx,%rdx,4)
	addq	$5, %rdx
	leal	-5(%rcx), %ecx
	cmpq	$65, %rdx
	jne	.LBB10_22
# BB#23:                                # %qtm_init_model.exit150
	leaq	1032(%rbx), %rcx
	movl	$4, 400(%rbx)
	movl	$64, 404(%rbx)
	movq	%rcx, 408(%rbx)
	movq	$-65, %rcx
	movw	$60, %dx
	.p2align	4, 0x90
.LBB10_24:                              # %.lr.ph.i143
                                        # =>This Inner Loop Header: Depth=1
	leal	193(%rcx), %esi
	movw	%si, 1292(%rbx,%rcx,4)
	leal	4(%rdx), %esi
	movw	%si, 1294(%rbx,%rcx,4)
	leal	194(%rcx), %esi
	movw	%si, 1296(%rbx,%rcx,4)
	leal	3(%rdx), %esi
	movw	%si, 1298(%rbx,%rcx,4)
	leal	195(%rcx), %esi
	movw	%si, 1300(%rbx,%rcx,4)
	leal	2(%rdx), %esi
	movw	%si, 1302(%rbx,%rcx,4)
	leal	196(%rcx), %esi
	movw	%si, 1304(%rbx,%rcx,4)
	leal	1(%rdx), %esi
	movw	%si, 1306(%rbx,%rcx,4)
	leal	197(%rcx), %esi
	movw	%si, 1308(%rbx,%rcx,4)
	movw	%dx, 1310(%rbx,%rcx,4)
	leal	-5(%rdx), %edx
	addq	$5, %rcx
	jne	.LBB10_24
# BB#25:                                # %qtm_init_model.exit144
	leaq	1292(%rbx), %rcx
	movl	$4, 416(%rbx)
	movl	$64, 420(%rbx)
	movq	%rcx, 424(%rbx)
	movq	$-65, %rcx
	movw	$60, %dx
	.p2align	4, 0x90
.LBB10_26:                              # %.lr.ph.i137
                                        # =>This Inner Loop Header: Depth=1
	leal	257(%rcx), %esi
	movw	%si, 1552(%rbx,%rcx,4)
	leal	4(%rdx), %esi
	movw	%si, 1554(%rbx,%rcx,4)
	leal	258(%rcx), %esi
	movw	%si, 1556(%rbx,%rcx,4)
	leal	3(%rdx), %esi
	movw	%si, 1558(%rbx,%rcx,4)
	leal	259(%rcx), %esi
	movw	%si, 1560(%rbx,%rcx,4)
	leal	2(%rdx), %esi
	movw	%si, 1562(%rbx,%rcx,4)
	leal	260(%rcx), %esi
	movw	%si, 1564(%rbx,%rcx,4)
	leal	1(%rdx), %esi
	movw	%si, 1566(%rbx,%rcx,4)
	leal	261(%rcx), %esi
	movw	%si, 1568(%rbx,%rcx,4)
	movw	%dx, 1570(%rbx,%rcx,4)
	leal	-5(%rdx), %edx
	addq	$5, %rcx
	jne	.LBB10_26
# BB#27:                                # %qtm_init_model.exit138
	leaq	1552(%rbx), %rcx
	cmpl	$25, %r9d
	movl	$24, %edx
	cmovll	%r9d, %edx
	movl	$4, 432(%rbx)
	movl	%edx, 436(%rbx)
	movq	%rcx, 440(%rbx)
	testl	%edx, %edx
	js	.LBB10_33
# BB#28:                                # %.lr.ph.i131.prol.preheader
	movl	%edx, %ecx
	movl	%edx, %r8d
	orl	$1, %r8d
	leaq	-1(%r8), %rdi
	movl	%r8d, %ebp
	andl	$3, %ebp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB10_29:                              # %.lr.ph.i131.prol
                                        # =>This Inner Loop Header: Depth=1
	movw	%si, 1552(%rbx,%rsi,4)
	movw	%dx, 1554(%rbx,%rsi,4)
	incq	%rsi
	decl	%edx
	cmpq	%rsi, %rbp
	jne	.LBB10_29
# BB#30:                                # %.lr.ph.i131.prol.loopexit
	cmpq	$3, %rdi
	jb	.LBB10_33
# BB#31:                                # %.lr.ph.preheader.i127.new
	leal	-3(%rcx), %edi
	subl	%esi, %edi
	leal	-2(%rcx), %ebp
	subl	%esi, %ebp
	leal	-1(%rcx), %edx
	subl	%esi, %edx
	subl	%esi, %ecx
	.p2align	4, 0x90
.LBB10_32:                              # %.lr.ph.i131
                                        # =>This Inner Loop Header: Depth=1
	movw	%si, 1552(%rbx,%rsi,4)
	movw	%cx, 1554(%rbx,%rsi,4)
	leal	1(%rsi), %eax
	movw	%ax, 1556(%rbx,%rsi,4)
	movw	%dx, 1558(%rbx,%rsi,4)
	leaq	2(%rsi), %rax
	movw	%ax, 1560(%rbx,%rsi,4)
	movw	%bp, 1562(%rbx,%rsi,4)
	incl	%eax
	movw	%ax, 1564(%rbx,%rsi,4)
	movw	%di, 1566(%rbx,%rsi,4)
	addl	$-4, %edi
	addl	$-4, %ebp
	addl	$-4, %edx
	addl	$-4, %ecx
	addq	$4, %rsi
	cmpq	%r8, %rsi
	jne	.LBB10_32
.LBB10_33:                              # %qtm_init_model.exit132
	leaq	1652(%rbx), %rax
	cmpl	$37, %r9d
	movl	$36, %edx
	cmovll	%r9d, %edx
	movl	$4, 448(%rbx)
	movl	%edx, 452(%rbx)
	movq	%rax, 456(%rbx)
	testl	%edx, %edx
	js	.LBB10_39
# BB#34:                                # %.lr.ph.i124.prol.preheader
	movl	%edx, %ecx
	movl	%edx, %r8d
	orl	$1, %r8d
	leaq	-1(%r8), %rdi
	movl	%r8d, %ebp
	andl	$3, %ebp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB10_35:                              # %.lr.ph.i124.prol
                                        # =>This Inner Loop Header: Depth=1
	movw	%si, 1652(%rbx,%rsi,4)
	movw	%dx, 1654(%rbx,%rsi,4)
	incq	%rsi
	decl	%edx
	cmpq	%rsi, %rbp
	jne	.LBB10_35
# BB#36:                                # %.lr.ph.i124.prol.loopexit
	cmpq	$3, %rdi
	jb	.LBB10_39
# BB#37:                                # %.lr.ph.preheader.i120.new
	leal	-3(%rcx), %edi
	subl	%esi, %edi
	leal	-2(%rcx), %ebp
	subl	%esi, %ebp
	leal	-1(%rcx), %edx
	subl	%esi, %edx
	subl	%esi, %ecx
	.p2align	4, 0x90
.LBB10_38:                              # %.lr.ph.i124
                                        # =>This Inner Loop Header: Depth=1
	movw	%si, 1652(%rbx,%rsi,4)
	movw	%cx, 1654(%rbx,%rsi,4)
	leal	1(%rsi), %eax
	movw	%ax, 1656(%rbx,%rsi,4)
	movw	%dx, 1658(%rbx,%rsi,4)
	leaq	2(%rsi), %rax
	movw	%ax, 1660(%rbx,%rsi,4)
	movw	%bp, 1662(%rbx,%rsi,4)
	incl	%eax
	movw	%ax, 1664(%rbx,%rsi,4)
	movw	%di, 1666(%rbx,%rsi,4)
	addl	$-4, %edi
	addl	$-4, %ebp
	addl	$-4, %edx
	addl	$-4, %ecx
	addq	$4, %rsi
	cmpq	%r8, %rsi
	jne	.LBB10_38
.LBB10_39:                              # %qtm_init_model.exit125
	leaq	1800(%rbx), %rax
	movl	$4, 464(%rbx)
	movl	%r9d, 468(%rbx)
	movq	%rax, 472(%rbx)
	testl	%r10d, %r10d
	js	.LBB10_45
# BB#40:                                # %.lr.ph.i117.prol.preheader
	movl	%r9d, %r8d
	orl	$1, %r8d
	leaq	-1(%r8), %rsi
	movl	%r8d, %edi
	andl	$3, %edi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB10_41:                              # %.lr.ph.i117.prol
                                        # =>This Inner Loop Header: Depth=1
	movw	%dx, 1800(%rbx,%rdx,4)
	movw	%r9w, 1802(%rbx,%rdx,4)
	incq	%rdx
	decl	%r9d
	cmpq	%rdx, %rdi
	jne	.LBB10_41
# BB#42:                                # %.lr.ph.i117.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB10_45
# BB#43:                                # %.lr.ph.preheader.i.new
	leal	-1(%r8), %eax
	leal	-4(%r8), %esi
	subl	%edx, %esi
	leal	-3(%r8), %edi
	subl	%edx, %edi
	leal	-2(%r8), %ebp
	subl	%edx, %ebp
	subl	%edx, %eax
	.p2align	4, 0x90
.LBB10_44:                              # %.lr.ph.i117
                                        # =>This Inner Loop Header: Depth=1
	movw	%dx, 1800(%rbx,%rdx,4)
	movw	%ax, 1802(%rbx,%rdx,4)
	leal	1(%rdx), %ecx
	movw	%cx, 1804(%rbx,%rdx,4)
	movw	%bp, 1806(%rbx,%rdx,4)
	leaq	2(%rdx), %rcx
	movw	%cx, 1808(%rbx,%rdx,4)
	movw	%di, 1810(%rbx,%rdx,4)
	incl	%ecx
	movw	%cx, 1812(%rbx,%rdx,4)
	movw	%si, 1814(%rbx,%rdx,4)
	addl	$-4, %esi
	addl	$-4, %edi
	addl	$-4, %ebp
	addl	$-4, %eax
	addq	$4, %rdx
	cmpq	%r8, %rdx
	jne	.LBB10_44
.LBB10_45:                              # %qtm_init_model.exit118
	leaq	1972(%rbx), %rax
	movl	$4, 480(%rbx)
	movl	$27, 484(%rbx)
	movq	%rax, 488(%rbx)
	movaps	.LCPI10_0(%rip), %xmm0  # xmm0 = [0,27,1,26,2,25,3,24]
	movups	%xmm0, 1972(%rbx)
	movw	$4, 1988(%rbx)
	movw	$23, 1990(%rbx)
	movw	$5, 1992(%rbx)
	movw	$22, 1994(%rbx)
	movw	$6, 1996(%rbx)
	movaps	.LCPI10_1(%rip), %xmm0  # xmm0 = [21,7,20,8,19,9,18,10]
	movups	%xmm0, 1998(%rbx)
	movaps	.LCPI10_2(%rip), %xmm0  # xmm0 = [17,11,16,12,15,13,14,14]
	movups	%xmm0, 2014(%rbx)
	movaps	.LCPI10_3(%rip), %xmm0  # xmm0 = [13,15,12,16,11,17,10,18]
	movups	%xmm0, 2030(%rbx)
	movaps	.LCPI10_4(%rip), %xmm0  # xmm0 = [9,19,8,20,7,21,6,22]
	movups	%xmm0, 2046(%rbx)
	movaps	.LCPI10_5(%rip), %xmm0  # xmm0 = [5,23,4,24,3,25,2,26]
	movups	%xmm0, 2062(%rbx)
	movw	$1, 2078(%rbx)
	movw	$27, 2080(%rbx)
	movw	$0, 2082(%rbx)
	movq	%rbx, %rax
	addq	$2084, %rax             # imm = 0x824
	movl	$4, 496(%rbx)
	movl	$7, 500(%rbx)
	movq	%rax, 504(%rbx)
	movw	$0, 2084(%rbx)
	movw	$7, 2086(%rbx)
	movaps	.LCPI10_6(%rip), %xmm0  # xmm0 = [1,6,2,5,3,4,4,3]
	movups	%xmm0, 2088(%rbx)
	movw	$5, 2104(%rbx)
	movw	$2, 2106(%rbx)
	movw	$6, 2108(%rbx)
	movw	$1, 2110(%rbx)
	movw	$7, 2112(%rbx)
	movw	$0, 2114(%rbx)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 2120(%rbx)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 2128(%rbx)
	jmp	.LBB10_50
.LBB10_47:
	movq	16(%rbx), %rdi
	callq	free
.LBB10_48:
	movq	%rbx, %rdi
	callq	free
.LBB10_49:
	xorl	%ebx, %ebx
.LBB10_50:
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	qtm_init, .Lfunc_end10-qtm_init
	.cfi_endproc

	.globl	qtm_decompress
	.p2align	4, 0x90
	.type	qtm_decompress,@function
qtm_decompress:                         # @qtm_decompress
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi98:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi99:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi101:
	.cfi_def_cfa_offset 256
.Lcfi102:
	.cfi_offset %rbx, -56
.Lcfi103:
	.cfi_offset %r12, -48
.Lcfi104:
	.cfi_offset %r13, -40
.Lcfi105:
	.cfi_offset %r14, -32
.Lcfi106:
	.cfi_offset %r15, -24
.Lcfi107:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r11
	testq	%r11, %r11
	movl	$-111, %eax
	je	.LBB11_303
# BB#1:
	testq	%rbx, %rbx
	js	.LBB11_303
# BB#2:
	movl	44(%r11), %eax
	testl	%eax, %eax
	jne	.LBB11_303
# BB#3:
	movq	72(%r11), %rsi
	movq	80(%r11), %rbp
	subq	%rsi, %rbp
	movslq	%ebp, %rax
	cmpq	%rbx, %rax
	cmovgq	%rbx, %rbp
	testl	%ebp, %ebp
	je	.LBB11_8
# BB#4:
	cmpb	$0, 8(%r11)
	je	.LBB11_7
# BB#5:
	movl	4(%r11), %edi
	movl	%ebp, %edx
	movq	%r11, %r14
	callq	cli_writen
	cmpl	%ebp, %eax
	jne	.LBB11_10
# BB#6:                                 # %._crit_edge1745
	movq	72(%r14), %rsi
	movq	%r14, %r11
.LBB11_7:
	movslq	%ebp, %rax
	addq	%rax, %rsi
	movq	%rsi, 72(%r11)
	subq	%rax, %rbx
.LBB11_8:
	testq	%rbx, %rbx
	je	.LBB11_302
# BB#9:
	movq	56(%r11), %r10
	movq	64(%r11), %r9
	movl	88(%r11), %r15d
	movb	96(%r11), %r12b
	movq	16(%r11), %rsi
	movl	28(%r11), %ebp
	movl	32(%r11), %edi
	movw	36(%r11), %dx
	movw	38(%r11), %ax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movw	40(%r11), %r14w
	leaq	496(%r11), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	leaq	400(%r11), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	leaq	416(%r11), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	leaq	368(%r11), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	leaq	384(%r11), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	leaq	432(%r11), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	leaq	448(%r11), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	leaq	480(%r11), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	leaq	464(%r11), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	leaq	1(%rsi), %rax
	leaq	16(%rsi), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	leaq	112(%rsi), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	%r11, (%rsp)            # 8-byte Spill
	movq	%rax, 96(%rsp)          # 8-byte Spill
	jmp	.LBB11_14
.LBB11_10:
	movl	$-123, 44(%r14)
	movl	$-123, %eax
	jmp	.LBB11_303
.LBB11_11:                              #   in Loop: Header=BB11_14 Depth=1
	movq	%r9, 40(%rsp)           # 8-byte Spill
	movq	%r10, 16(%rsp)          # 8-byte Spill
	movq	72(%r11), %rsi
	movq	80(%r11), %rbx
	subq	%rsi, %rbx
	cmpb	$0, 8(%r11)
	je	.LBB11_13
# BB#12:                                #   in Loop: Header=BB11_14 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movl	4(%rax), %edi
	movl	%ebx, %edx
	callq	cli_writen
	cmpl	%ebx, %eax
	jne	.LBB11_295
.LBB11_13:                              #   in Loop: Header=BB11_14 Depth=1
	movslq	%ebx, %rax
	movq	64(%rsp), %rbx          # 8-byte Reload
	subq	%rax, %rbx
	movq	(%rsp), %r11            # 8-byte Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	%rax, 72(%r11)
	movq	%rax, 80(%r11)
	xorl	%ebp, %ebp
	xorl	%edi, %edi
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	40(%rsp), %r9           # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
.LBB11_14:                              # %.outer.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_15 Depth 2
                                        #       Child Loop BB11_16 Depth 3
                                        #         Child Loop BB11_20 Depth 4
                                        #         Child Loop BB11_33 Depth 4
                                        #           Child Loop BB11_35 Depth 5
                                        #           Child Loop BB11_38 Depth 5
                                        #           Child Loop BB11_42 Depth 5
                                        #           Child Loop BB11_84 Depth 5
                                        #           Child Loop BB11_87 Depth 5
                                        #           Child Loop BB11_92 Depth 5
                                        #           Child Loop BB11_147 Depth 5
                                        #           Child Loop BB11_126 Depth 5
                                        #           Child Loop BB11_129 Depth 5
                                        #           Child Loop BB11_134 Depth 5
                                        #           Child Loop BB11_192 Depth 5
                                        #           Child Loop BB11_106 Depth 5
                                        #           Child Loop BB11_109 Depth 5
                                        #           Child Loop BB11_113 Depth 5
                                        #           Child Loop BB11_158 Depth 5
                                        #           Child Loop BB11_170 Depth 5
                                        #           Child Loop BB11_174 Depth 5
                                        #           Child Loop BB11_179 Depth 5
                                        #           Child Loop BB11_204 Depth 5
                                        #           Child Loop BB11_242 Depth 5
                                        #           Child Loop BB11_246 Depth 5
                                        #           Child Loop BB11_236 Depth 5
                                        #           Child Loop BB11_227 Depth 5
                                        #           Child Loop BB11_252 Depth 5
                                        #           Child Loop BB11_257 Depth 5
                                        #           Child Loop BB11_266 Depth 5
                                        #           Child Loop BB11_270 Depth 5
                                        #           Child Loop BB11_274 Depth 5
                                        #           Child Loop BB11_59 Depth 5
                                        #           Child Loop BB11_63 Depth 5
                                        #           Child Loop BB11_68 Depth 5
                                        #       Child Loop BB11_282 Depth 3
                                        #         Child Loop BB11_283 Depth 4
	movq	%rbx, 64(%rsp)          # 8-byte Spill
.LBB11_15:                              # %.outer
                                        #   Parent Loop BB11_14 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB11_16 Depth 3
                                        #         Child Loop BB11_20 Depth 4
                                        #         Child Loop BB11_33 Depth 4
                                        #           Child Loop BB11_35 Depth 5
                                        #           Child Loop BB11_38 Depth 5
                                        #           Child Loop BB11_42 Depth 5
                                        #           Child Loop BB11_84 Depth 5
                                        #           Child Loop BB11_87 Depth 5
                                        #           Child Loop BB11_92 Depth 5
                                        #           Child Loop BB11_147 Depth 5
                                        #           Child Loop BB11_126 Depth 5
                                        #           Child Loop BB11_129 Depth 5
                                        #           Child Loop BB11_134 Depth 5
                                        #           Child Loop BB11_192 Depth 5
                                        #           Child Loop BB11_106 Depth 5
                                        #           Child Loop BB11_109 Depth 5
                                        #           Child Loop BB11_113 Depth 5
                                        #           Child Loop BB11_158 Depth 5
                                        #           Child Loop BB11_170 Depth 5
                                        #           Child Loop BB11_174 Depth 5
                                        #           Child Loop BB11_179 Depth 5
                                        #           Child Loop BB11_204 Depth 5
                                        #           Child Loop BB11_242 Depth 5
                                        #           Child Loop BB11_246 Depth 5
                                        #           Child Loop BB11_236 Depth 5
                                        #           Child Loop BB11_227 Depth 5
                                        #           Child Loop BB11_252 Depth 5
                                        #           Child Loop BB11_257 Depth 5
                                        #           Child Loop BB11_266 Depth 5
                                        #           Child Loop BB11_270 Depth 5
                                        #           Child Loop BB11_274 Depth 5
                                        #           Child Loop BB11_59 Depth 5
                                        #           Child Loop BB11_63 Depth 5
                                        #           Child Loop BB11_68 Depth 5
                                        #       Child Loop BB11_282 Depth 3
                                        #         Child Loop BB11_283 Depth 4
	movq	%rdi, 104(%rsp)         # 8-byte Spill
	leal	32768(%rdi), %eax
	movl	%eax, 76(%rsp)          # 4-byte Spill
	movq	80(%r11), %rax
	movq	64(%rsp), %r13          # 8-byte Reload
	movl	32(%rsp), %ebx          # 4-byte Reload
.LBB11_16:                              #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB11_20 Depth 4
                                        #         Child Loop BB11_33 Depth 4
                                        #           Child Loop BB11_35 Depth 5
                                        #           Child Loop BB11_38 Depth 5
                                        #           Child Loop BB11_42 Depth 5
                                        #           Child Loop BB11_84 Depth 5
                                        #           Child Loop BB11_87 Depth 5
                                        #           Child Loop BB11_92 Depth 5
                                        #           Child Loop BB11_147 Depth 5
                                        #           Child Loop BB11_126 Depth 5
                                        #           Child Loop BB11_129 Depth 5
                                        #           Child Loop BB11_134 Depth 5
                                        #           Child Loop BB11_192 Depth 5
                                        #           Child Loop BB11_106 Depth 5
                                        #           Child Loop BB11_109 Depth 5
                                        #           Child Loop BB11_113 Depth 5
                                        #           Child Loop BB11_158 Depth 5
                                        #           Child Loop BB11_170 Depth 5
                                        #           Child Loop BB11_174 Depth 5
                                        #           Child Loop BB11_179 Depth 5
                                        #           Child Loop BB11_204 Depth 5
                                        #           Child Loop BB11_242 Depth 5
                                        #           Child Loop BB11_246 Depth 5
                                        #           Child Loop BB11_236 Depth 5
                                        #           Child Loop BB11_227 Depth 5
                                        #           Child Loop BB11_252 Depth 5
                                        #           Child Loop BB11_257 Depth 5
                                        #           Child Loop BB11_266 Depth 5
                                        #           Child Loop BB11_270 Depth 5
                                        #           Child Loop BB11_274 Depth 5
                                        #           Child Loop BB11_59 Depth 5
                                        #           Child Loop BB11_63 Depth 5
                                        #           Child Loop BB11_68 Depth 5
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%r13, %rbp
	movq	%rdx, %r13
	movq	72(%r11), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rsi
	subq	%rdx, %rcx
	movq	%rbp, %rdi
	cmpq	%rbp, %rcx
	jge	.LBB11_296
# BB#17:                                #   in Loop: Header=BB11_16 Depth=3
	cmpb	$0, 42(%r11)
	movq	%r13, %rdx
	je	.LBB11_19
# BB#18:                                #   in Loop: Header=BB11_16 Depth=3
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rdi, %r13
	jmp	.LBB11_31
.LBB11_19:                              # %.preheader998.preheader
                                        #   in Loop: Header=BB11_16 Depth=3
	xorl	%r14d, %r14d
	movb	$16, %bl
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rdi, %r13
	.p2align	4, 0x90
.LBB11_20:                              # %.preheader998
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpb	$16, %r12b
	ja	.LBB11_27
# BB#21:                                #   in Loop: Header=BB11_20 Depth=4
	cmpq	%r9, %r10
	jb	.LBB11_26
# BB#22:                                #   in Loop: Header=BB11_20 Depth=4
	movq	2128(%r11), %rax
	testq	%rax, %rax
	movq	48(%r11), %rsi
	movl	92(%r11), %edx
	je	.LBB11_24
# BB#23:                                #   in Loop: Header=BB11_20 Depth=4
	movq	2120(%r11), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB11_25
	jmp	.LBB11_295
.LBB11_24:                              #   in Loop: Header=BB11_20 Depth=4
	movl	(%r11), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB11_295
.LBB11_25:                              #   in Loop: Header=BB11_20 Depth=4
	movq	(%rsp), %r11            # 8-byte Reload
	movq	48(%r11), %r10
	movq	%r10, 56(%r11)
	movslq	%eax, %r9
	addq	%r10, %r9
	movq	%r9, 64(%r11)
.LBB11_26:                              #   in Loop: Header=BB11_20 Depth=4
	movzbl	%r12b, %eax
	movzbl	(%r10), %ecx
	shll	$8, %ecx
	movzbl	1(%r10), %edx
	orl	%ecx, %edx
	movl	$16, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%edx, %r15d
	addb	$16, %r12b
	addq	$2, %r10
.LBB11_27:                              #   in Loop: Header=BB11_20 Depth=4
	cmpb	%bl, %r12b
	movl	%r12d, %eax
	jb	.LBB11_29
# BB#28:                                #   in Loop: Header=BB11_20 Depth=4
	movl	%ebx, %eax
.LBB11_29:                              #   in Loop: Header=BB11_20 Depth=4
	movzbl	%al, %eax
	movzwl	%r14w, %edx
	movl	%eax, %ecx
	shll	%cl, %edx
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%r15d, %r14d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r14d
	orl	%edx, %r14d
	movl	%eax, %ecx
	shll	%cl, %r15d
	subb	%al, %r12b
	subb	%al, %bl
	jne	.LBB11_20
# BB#30:                                #   in Loop: Header=BB11_16 Depth=3
	movb	$1, 42(%r11)
	movq	72(%r11), %rsi
	movq	80(%r11), %rax
	movw	$-1, %dx
	xorl	%ebx, %ebx
.LBB11_31:                              #   in Loop: Header=BB11_16 Depth=3
	leal	(%r13,%rbp), %ecx
	subl	%eax, %ecx
	addl	%ecx, %esi
	movl	76(%rsp), %eax          # 4-byte Reload
	cmpl	%esi, %eax
	cmovbl	%eax, %esi
	movq	%rsi, %rax
	cmpl	%eax, %ebp
	jae	.LBB11_277
# BB#32:                                # %.lr.ph1473.preheader
                                        #   in Loop: Header=BB11_16 Depth=3
	movl	%ebp, %ecx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rax, 168(%rsp)         # 8-byte Spill
.LBB11_33:                              # %.lr.ph1473
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB11_35 Depth 5
                                        #           Child Loop BB11_38 Depth 5
                                        #           Child Loop BB11_42 Depth 5
                                        #           Child Loop BB11_84 Depth 5
                                        #           Child Loop BB11_87 Depth 5
                                        #           Child Loop BB11_92 Depth 5
                                        #           Child Loop BB11_147 Depth 5
                                        #           Child Loop BB11_126 Depth 5
                                        #           Child Loop BB11_129 Depth 5
                                        #           Child Loop BB11_134 Depth 5
                                        #           Child Loop BB11_192 Depth 5
                                        #           Child Loop BB11_106 Depth 5
                                        #           Child Loop BB11_109 Depth 5
                                        #           Child Loop BB11_113 Depth 5
                                        #           Child Loop BB11_158 Depth 5
                                        #           Child Loop BB11_170 Depth 5
                                        #           Child Loop BB11_174 Depth 5
                                        #           Child Loop BB11_179 Depth 5
                                        #           Child Loop BB11_204 Depth 5
                                        #           Child Loop BB11_242 Depth 5
                                        #           Child Loop BB11_246 Depth 5
                                        #           Child Loop BB11_236 Depth 5
                                        #           Child Loop BB11_227 Depth 5
                                        #           Child Loop BB11_252 Depth 5
                                        #           Child Loop BB11_257 Depth 5
                                        #           Child Loop BB11_266 Depth 5
                                        #           Child Loop BB11_270 Depth 5
                                        #           Child Loop BB11_274 Depth 5
                                        #           Child Loop BB11_59 Depth 5
                                        #           Child Loop BB11_63 Depth 5
                                        #           Child Loop BB11_68 Depth 5
	movzwl	%dx, %edi
	movzwl	%bx, %esi
	subl	%esi, %edi
	movslq	500(%r11), %rbx
	cmpq	$2, %rbx
	movq	504(%r11), %rcx
	movzwl	2(%rcx), %r8d
	movl	$1, %eax
	jl	.LBB11_37
# BB#34:                                # %.lr.ph
                                        #   in Loop: Header=BB11_33 Depth=4
	movl	$1, %edx
	subl	%esi, %edx
	movzwl	%r14w, %eax
	addl	%edx, %eax
	imull	%r8d, %eax
	decl	%eax
	movzwl	%di, %ebp
	incl	%ebp
	xorl	%edx, %edx
	divl	%ebp
	movzwl	%ax, %edx
	movl	$1, %eax
	.p2align	4, 0x90
.LBB11_35:                              #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	2(%rcx,%rax,4), %ebp
	cmpl	%edx, %ebp
	jbe	.LBB11_37
# BB#36:                                #   in Loop: Header=BB11_35 Depth=5
	incq	%rax
	cmpq	%rbx, %rax
	jl	.LBB11_35
.LBB11_37:                              # %._crit_edge
                                        #   in Loop: Header=BB11_33 Depth=4
	movslq	%eax, %rbx
	movzwl	-4(%rcx,%rbx,4), %eax
	movw	%ax, 8(%rsp)            # 2-byte Spill
	incl	%edi
	movzwl	-2(%rcx,%rbx,4), %eax
	imull	%edi, %eax
	xorl	%edx, %edx
	divl	%r8d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	65535(%rsi,%rax), %r13d
	movzwl	2(%rcx,%rbx,4), %eax
	imull	%edi, %eax
	xorl	%edx, %edx
	divl	%r8d
	movl	%eax, %ebp
	addl	%esi, %ebp
	incq	%rbx
	.p2align	4, 0x90
.LBB11_38:                              #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	addw	$8, -6(%rcx,%rbx,4)
	decq	%rbx
	cmpq	$1, %rbx
	jg	.LBB11_38
# BB#39:                                #   in Loop: Header=BB11_33 Depth=4
	movzwl	2(%rcx), %eax
	cmpl	$3801, %eax             # imm = 0xED9
	jb	.LBB11_42
# BB#40:                                #   in Loop: Header=BB11_33 Depth=4
	movq	192(%rsp), %rdi         # 8-byte Reload
	movq	%r10, %rbx
	movq	%r9, 40(%rsp)           # 8-byte Spill
	callq	qtm_update_model
	movq	40(%rsp), %r9           # 8-byte Reload
	movq	%rbx, %r10
	movq	(%rsp), %r11            # 8-byte Reload
	jmp	.LBB11_42
	.p2align	4, 0x90
.LBB11_41:                              #   in Loop: Header=BB11_42 Depth=5
	addl	%ebp, %ebp
	leal	1(%r13,%r13), %r13d
	shldl	$1, %r15d, %r14d
	addl	%r15d, %r15d
	decb	%r12b
.LBB11_42:                              # %.preheader996
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movl	%ebp, %eax
	xorw	%r13w, %ax
	jns	.LBB11_46
# BB#43:                                #   in Loop: Header=BB11_42 Depth=5
	movzwl	%bp, %ecx
	movzwl	%r13w, %ebx
	testb	$64, %bh
	jne	.LBB11_53
# BB#44:                                #   in Loop: Header=BB11_42 Depth=5
	movl	%ecx, %eax
	andl	$16384, %eax            # imm = 0x4000
	je	.LBB11_53
# BB#45:                                #   in Loop: Header=BB11_42 Depth=5
	xorl	$16384, %r14d           # imm = 0x4000
	andl	$16383, %ebp            # imm = 0x3FFF
	orl	$16384, %r13d           # imm = 0x4000
.LBB11_46:                              #   in Loop: Header=BB11_42 Depth=5
	cmpb	$16, %r12b
	ja	.LBB11_41
# BB#47:                                #   in Loop: Header=BB11_42 Depth=5
	cmpq	%r9, %r10
	jb	.LBB11_52
# BB#48:                                #   in Loop: Header=BB11_42 Depth=5
	movq	2128(%r11), %rax
	testq	%rax, %rax
	movq	48(%r11), %rsi
	movl	92(%r11), %edx
	je	.LBB11_50
# BB#49:                                #   in Loop: Header=BB11_42 Depth=5
	movq	2120(%r11), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB11_51
	jmp	.LBB11_295
.LBB11_50:                              #   in Loop: Header=BB11_42 Depth=5
	movl	(%r11), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB11_295
.LBB11_51:                              #   in Loop: Header=BB11_42 Depth=5
	movq	(%rsp), %r11            # 8-byte Reload
	movq	48(%r11), %r10
	movq	%r10, 56(%r11)
	movslq	%eax, %r9
	addq	%r10, %r9
	movq	%r9, 64(%r11)
.LBB11_52:                              #   in Loop: Header=BB11_42 Depth=5
	movzbl	%r12b, %eax
	movzbl	(%r10), %ecx
	shll	$8, %ecx
	movzbl	1(%r10), %edx
	orl	%ecx, %edx
	movl	$16, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%edx, %r15d
	addb	$16, %r12b
	addq	$2, %r10
	jmp	.LBB11_41
.LBB11_53:                              #   in Loop: Header=BB11_33 Depth=4
	movzwl	8(%rsp), %edx           # 2-byte Folded Reload
	movzwl	%dx, %eax
	cmpl	$3, %eax
	ja	.LBB11_79
# BB#54:                                #   in Loop: Header=BB11_33 Depth=4
	movq	%r10, 16(%rsp)          # 8-byte Spill
	testw	%dx, %dx
	movq	184(%rsp), %rdi         # 8-byte Reload
	je	.LBB11_57
# BB#55:                                #   in Loop: Header=BB11_33 Depth=4
	cmpl	$1, %eax
	movq	176(%rsp), %rdi         # 8-byte Reload
	je	.LBB11_57
# BB#56:                                #   in Loop: Header=BB11_33 Depth=4
	cmpl	$2, %eax
	movq	152(%rsp), %rdi         # 8-byte Reload
	cmoveq	160(%rsp), %rdi         # 8-byte Folded Reload
.LBB11_57:                              #   in Loop: Header=BB11_33 Depth=4
	subl	%ecx, %ebx
	movslq	4(%rdi), %r10
	cmpq	$2, %r10
	movq	8(%rdi), %rsi
	movzwl	2(%rsi), %r8d
	movl	$1, %eax
	jl	.LBB11_62
# BB#58:                                # %.lr.ph1459
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	%r9, %r13
	movl	$1, %edx
	subl	%ecx, %edx
	movzwl	%r14w, %eax
	addl	%edx, %eax
	imull	%r8d, %eax
	decl	%eax
	movzwl	%bx, %r9d
	incl	%r9d
	xorl	%edx, %edx
	divl	%r9d
	movzwl	%ax, %edx
	movl	$1, %eax
	.p2align	4, 0x90
.LBB11_59:                              #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	2(%rsi,%rax,4), %ebp
	cmpl	%edx, %ebp
	jbe	.LBB11_61
# BB#60:                                #   in Loop: Header=BB11_59 Depth=5
	incq	%rax
	cmpq	%r10, %rax
	jl	.LBB11_59
.LBB11_61:                              # %.._crit_edge1460.loopexit_crit_edge
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	%r13, %r9
.LBB11_62:                              # %._crit_edge1460
                                        #   in Loop: Header=BB11_33 Depth=4
	movslq	%eax, %rbp
	movzwl	-4(%rsi,%rbp,4), %eax
	movw	%ax, 8(%rsp)            # 2-byte Spill
	incl	%ebx
	movzwl	-2(%rsi,%rbp,4), %eax
	imull	%ebx, %eax
	xorl	%edx, %edx
	divl	%r8d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	65535(%rcx,%rax), %r13d
	movzwl	2(%rsi,%rbp,4), %eax
	imull	%ebx, %eax
	xorl	%edx, %edx
	divl	%r8d
	movl	%eax, %ebx
	addl	%ecx, %ebx
	incq	%rbp
	.p2align	4, 0x90
.LBB11_63:                              #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	addw	$8, -6(%rsi,%rbp,4)
	decq	%rbp
	cmpq	$1, %rbp
	jg	.LBB11_63
# BB#64:                                #   in Loop: Header=BB11_33 Depth=4
	movzwl	2(%rsi), %eax
	cmpl	$3801, %eax             # imm = 0xED9
	jb	.LBB11_66
# BB#65:                                #   in Loop: Header=BB11_33 Depth=4
	movq	%r9, %rbp
	callq	qtm_update_model
	movq	%rbp, %r9
	movq	(%rsp), %r11            # 8-byte Reload
.LBB11_66:                              # %.preheader.preheader
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	%r13, %rdx
	jmp	.LBB11_68
	.p2align	4, 0x90
.LBB11_67:                              #   in Loop: Header=BB11_68 Depth=5
	addl	%ebx, %ebx
	leal	1(%rdx,%rdx), %edx
	shldl	$1, %r15d, %r14d
	addl	%r15d, %r15d
	decb	%r12b
.LBB11_68:                              # %.preheader
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movl	%ebx, %eax
	xorw	%dx, %ax
	jns	.LBB11_72
# BB#69:                                #   in Loop: Header=BB11_68 Depth=5
	testb	$64, %dh
	jne	.LBB11_103
# BB#70:                                #   in Loop: Header=BB11_68 Depth=5
	movl	%ebx, %eax
	andl	$16384, %eax            # imm = 0x4000
	testw	%ax, %ax
	je	.LBB11_103
# BB#71:                                #   in Loop: Header=BB11_68 Depth=5
	xorl	$16384, %r14d           # imm = 0x4000
	andl	$16383, %ebx            # imm = 0x3FFF
	orl	$16384, %edx            # imm = 0x4000
.LBB11_72:                              #   in Loop: Header=BB11_68 Depth=5
	cmpb	$16, %r12b
	ja	.LBB11_67
# BB#73:                                #   in Loop: Header=BB11_68 Depth=5
	movq	%rdx, %rbp
	cmpq	%r9, %r10
	jb	.LBB11_78
# BB#74:                                #   in Loop: Header=BB11_68 Depth=5
	movq	2128(%r11), %rax
	testq	%rax, %rax
	movq	48(%r11), %rsi
	movl	92(%r11), %edx
	je	.LBB11_76
# BB#75:                                #   in Loop: Header=BB11_68 Depth=5
	movq	2120(%r11), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB11_77
	jmp	.LBB11_295
.LBB11_76:                              #   in Loop: Header=BB11_68 Depth=5
	movl	(%r11), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB11_295
.LBB11_77:                              #   in Loop: Header=BB11_68 Depth=5
	movq	(%rsp), %r11            # 8-byte Reload
	movq	48(%r11), %r10
	movq	%r10, 56(%r11)
	movslq	%eax, %r9
	addq	%r10, %r9
	movq	%r9, 64(%r11)
.LBB11_78:                              #   in Loop: Header=BB11_68 Depth=5
	movzbl	%r12b, %eax
	movzbl	(%r10), %ecx
	shll	$8, %ecx
	movzbl	1(%r10), %edx
	orl	%ecx, %edx
	movl	$16, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%edx, %r15d
	addb	$16, %r12b
	addq	$2, %r10
	movq	%rbp, %rdx
	jmp	.LBB11_67
.LBB11_79:                              #   in Loop: Header=BB11_33 Depth=4
	cmpl	$6, %eax
	je	.LBB11_104
# BB#80:                                #   in Loop: Header=BB11_33 Depth=4
	cmpl	$5, %eax
	je	.LBB11_124
# BB#81:                                #   in Loop: Header=BB11_33 Depth=4
	cmpl	$4, %eax
	jne	.LBB11_305
# BB#82:                                #   in Loop: Header=BB11_33 Depth=4
	subl	%ecx, %ebx
	movslq	436(%r11), %rbp
	cmpq	$2, %rbp
	movq	440(%r11), %rsi
	movzwl	2(%rsi), %r8d
	movl	$1, %eax
	jl	.LBB11_86
# BB#83:                                # %.lr.ph1424
                                        #   in Loop: Header=BB11_33 Depth=4
	movl	$1, %edx
	subl	%ecx, %edx
	movzwl	%r14w, %eax
	addl	%edx, %eax
	imull	%r8d, %eax
	decl	%eax
	movzwl	%bx, %edi
	incl	%edi
	xorl	%edx, %edx
	divl	%edi
	movzwl	%ax, %edx
	movl	$1, %eax
.LBB11_84:                              #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	2(%rsi,%rax,4), %edi
	cmpl	%edx, %edi
	jbe	.LBB11_86
# BB#85:                                #   in Loop: Header=BB11_84 Depth=5
	incq	%rax
	cmpq	%rbp, %rax
	jl	.LBB11_84
.LBB11_86:                              # %._crit_edge1425
                                        #   in Loop: Header=BB11_33 Depth=4
	movslq	%eax, %rbp
	movzwl	-4(%rsi,%rbp,4), %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	incl	%ebx
	movzwl	-2(%rsi,%rbp,4), %eax
	imull	%ebx, %eax
	xorl	%edx, %edx
	divl	%r8d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	65535(%rcx,%rax), %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movzwl	2(%rsi,%rbp,4), %eax
	imull	%ebx, %eax
	xorl	%edx, %edx
	divl	%r8d
	addl	%ecx, %eax
	incq	%rbp
	.p2align	4, 0x90
.LBB11_87:                              #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	addw	$8, -6(%rsi,%rbp,4)
	decq	%rbp
	cmpq	$1, %rbp
	jg	.LBB11_87
# BB#88:                                #   in Loop: Header=BB11_33 Depth=4
	movzwl	2(%rsi), %ecx
	cmpl	$3801, %ecx             # imm = 0xED9
	jb	.LBB11_90
# BB#89:                                #   in Loop: Header=BB11_33 Depth=4
	movq	144(%rsp), %rdi         # 8-byte Reload
	movl	%eax, %r13d
	movq	%r10, %rbx
	movq	%r9, %rbp
	callq	qtm_update_model
	movq	%rbp, %r9
	movq	%rbx, %r10
	movl	%r13d, %eax
	movq	(%rsp), %r11            # 8-byte Reload
.LBB11_90:                              # %.preheader990.preheader
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	64(%rsp), %r13          # 8-byte Reload
	jmp	.LBB11_92
	.p2align	4, 0x90
.LBB11_91:                              #   in Loop: Header=BB11_92 Depth=5
	addl	%eax, %eax
	leal	1(%rcx,%rcx), %ecx
	shldl	$1, %r15d, %r14d
	addl	%r15d, %r15d
	decb	%r12b
.LBB11_92:                              # %.preheader990
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movl	%eax, %edx
	xorw	%cx, %dx
	jns	.LBB11_96
# BB#93:                                #   in Loop: Header=BB11_92 Depth=5
	testb	$64, %ch
	jne	.LBB11_145
# BB#94:                                #   in Loop: Header=BB11_92 Depth=5
	movl	%eax, %edx
	andl	$16384, %edx            # imm = 0x4000
	testw	%dx, %dx
	je	.LBB11_145
# BB#95:                                #   in Loop: Header=BB11_92 Depth=5
	xorl	$16384, %r14d           # imm = 0x4000
	andl	$16383, %eax            # imm = 0x3FFF
	orl	$16384, %ecx            # imm = 0x4000
.LBB11_96:                              #   in Loop: Header=BB11_92 Depth=5
	cmpb	$16, %r12b
	ja	.LBB11_91
# BB#97:                                #   in Loop: Header=BB11_92 Depth=5
	movl	%eax, %ebp
	movq	%rcx, %rbx
	cmpq	%r9, %r10
	jb	.LBB11_102
# BB#98:                                #   in Loop: Header=BB11_92 Depth=5
	movq	2128(%r11), %rax
	testq	%rax, %rax
	movq	48(%r11), %rsi
	movl	92(%r11), %edx
	je	.LBB11_100
# BB#99:                                #   in Loop: Header=BB11_92 Depth=5
	movq	2120(%r11), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB11_101
	jmp	.LBB11_295
.LBB11_100:                             #   in Loop: Header=BB11_92 Depth=5
	movl	(%r11), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB11_295
.LBB11_101:                             #   in Loop: Header=BB11_92 Depth=5
	movq	(%rsp), %r11            # 8-byte Reload
	movq	48(%r11), %r10
	movq	%r10, 56(%r11)
	movslq	%eax, %r9
	addq	%r10, %r9
	movq	%r9, 64(%r11)
.LBB11_102:                             #   in Loop: Header=BB11_92 Depth=5
	movzbl	%r12b, %eax
	movzbl	(%r10), %ecx
	shll	$8, %ecx
	movzbl	1(%r10), %edx
	orl	%ecx, %edx
	movl	$16, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%edx, %r15d
	addb	$16, %r12b
	addq	$2, %r10
	movq	%rbx, %rcx
	movl	%ebp, %eax
	jmp	.LBB11_91
.LBB11_103:                             # %.thread
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %eax
	incl	%ecx
	movq	48(%rsp), %rsi          # 8-byte Reload
	movzwl	8(%rsp), %edi           # 2-byte Folded Reload
	movb	%dil, (%rsi,%rax)
	movl	%ecx, %ebp
	movq	64(%rsp), %r13          # 8-byte Reload
	jmp	.LBB11_276
.LBB11_104:                             #   in Loop: Header=BB11_33 Depth=4
	subl	%ecx, %ebx
	movslq	484(%r11), %rbp
	cmpq	$2, %rbp
	movq	488(%r11), %rsi
	movzwl	2(%rsi), %r8d
	movl	$1, %eax
	jl	.LBB11_108
# BB#105:                               # %.lr.ph1363
                                        #   in Loop: Header=BB11_33 Depth=4
	movl	$1, %edx
	subl	%ecx, %edx
	movzwl	%r14w, %eax
	addl	%edx, %eax
	imull	%r8d, %eax
	decl	%eax
	movzwl	%bx, %edi
	incl	%edi
	xorl	%edx, %edx
	divl	%edi
	movzwl	%ax, %edx
	movl	$1, %eax
.LBB11_106:                             #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	2(%rsi,%rax,4), %edi
	cmpl	%edx, %edi
	jbe	.LBB11_108
# BB#107:                               #   in Loop: Header=BB11_106 Depth=5
	incq	%rax
	cmpq	%rbp, %rax
	jl	.LBB11_106
.LBB11_108:                             # %._crit_edge1364
                                        #   in Loop: Header=BB11_33 Depth=4
	movslq	%eax, %rdi
	movzwl	-4(%rsi,%rdi,4), %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	incl	%ebx
	movzwl	-2(%rsi,%rdi,4), %eax
	imull	%ebx, %eax
	xorl	%edx, %edx
	divl	%r8d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	65535(%rcx,%rax), %r13d
	movzwl	2(%rsi,%rdi,4), %eax
	imull	%ebx, %eax
	xorl	%edx, %edx
	divl	%r8d
	movl	%eax, %ebp
	addl	%ecx, %ebp
	incq	%rdi
	.p2align	4, 0x90
.LBB11_109:                             #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	addw	$8, -6(%rsi,%rdi,4)
	decq	%rdi
	cmpq	$1, %rdi
	jg	.LBB11_109
# BB#110:                               #   in Loop: Header=BB11_33 Depth=4
	movzwl	2(%rsi), %eax
	cmpl	$3801, %eax             # imm = 0xED9
	jb	.LBB11_113
# BB#111:                               #   in Loop: Header=BB11_33 Depth=4
	movq	128(%rsp), %rdi         # 8-byte Reload
	movq	%r10, %rbx
	movq	%r9, 40(%rsp)           # 8-byte Spill
	callq	qtm_update_model
	movq	40(%rsp), %r9           # 8-byte Reload
	movq	%rbx, %r10
	movq	(%rsp), %r11            # 8-byte Reload
	jmp	.LBB11_113
	.p2align	4, 0x90
.LBB11_112:                             #   in Loop: Header=BB11_113 Depth=5
	addl	%ebp, %ebp
	leal	1(%r13,%r13), %r13d
	shldl	$1, %r15d, %r14d
	addl	%r15d, %r15d
	decb	%r12b
.LBB11_113:                             # %.preheader995
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movl	%ebp, %eax
	xorw	%r13w, %ax
	jns	.LBB11_117
# BB#114:                               #   in Loop: Header=BB11_113 Depth=5
	movzwl	%bp, %ecx
	movzwl	%r13w, %edx
	testb	$64, %dh
	jne	.LBB11_157
# BB#115:                               #   in Loop: Header=BB11_113 Depth=5
	movl	%ecx, %eax
	andl	$16384, %eax            # imm = 0x4000
	je	.LBB11_157
# BB#116:                               #   in Loop: Header=BB11_113 Depth=5
	xorl	$16384, %r14d           # imm = 0x4000
	andl	$16383, %ebp            # imm = 0x3FFF
	orl	$16384, %r13d           # imm = 0x4000
.LBB11_117:                             #   in Loop: Header=BB11_113 Depth=5
	cmpb	$16, %r12b
	ja	.LBB11_112
# BB#118:                               #   in Loop: Header=BB11_113 Depth=5
	cmpq	%r9, %r10
	jb	.LBB11_123
# BB#119:                               #   in Loop: Header=BB11_113 Depth=5
	movq	2128(%r11), %rax
	testq	%rax, %rax
	movq	48(%r11), %rsi
	movl	92(%r11), %edx
	je	.LBB11_121
# BB#120:                               #   in Loop: Header=BB11_113 Depth=5
	movq	2120(%r11), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB11_122
	jmp	.LBB11_295
.LBB11_121:                             #   in Loop: Header=BB11_113 Depth=5
	movl	(%r11), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB11_295
.LBB11_122:                             #   in Loop: Header=BB11_113 Depth=5
	movq	(%rsp), %r11            # 8-byte Reload
	movq	48(%r11), %r10
	movq	%r10, 56(%r11)
	movslq	%eax, %r9
	addq	%r10, %r9
	movq	%r9, 64(%r11)
.LBB11_123:                             #   in Loop: Header=BB11_113 Depth=5
	movzbl	%r12b, %eax
	movzbl	(%r10), %ecx
	shll	$8, %ecx
	movzbl	1(%r10), %edx
	orl	%ecx, %edx
	movl	$16, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%edx, %r15d
	addb	$16, %r12b
	addq	$2, %r10
	jmp	.LBB11_112
.LBB11_124:                             #   in Loop: Header=BB11_33 Depth=4
	subl	%ecx, %ebx
	movslq	452(%r11), %rbp
	cmpq	$2, %rbp
	movq	456(%r11), %rsi
	movzwl	2(%rsi), %r8d
	movl	$1, %eax
	jl	.LBB11_128
# BB#125:                               # %.lr.ph1404
                                        #   in Loop: Header=BB11_33 Depth=4
	movl	$1, %edx
	subl	%ecx, %edx
	movzwl	%r14w, %eax
	addl	%edx, %eax
	imull	%r8d, %eax
	decl	%eax
	movzwl	%bx, %edi
	incl	%edi
	xorl	%edx, %edx
	divl	%edi
	movzwl	%ax, %edx
	movl	$1, %eax
.LBB11_126:                             #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	2(%rsi,%rax,4), %edi
	cmpl	%edx, %edi
	jbe	.LBB11_128
# BB#127:                               #   in Loop: Header=BB11_126 Depth=5
	incq	%rax
	cmpq	%rbp, %rax
	jl	.LBB11_126
.LBB11_128:                             # %._crit_edge1405
                                        #   in Loop: Header=BB11_33 Depth=4
	movslq	%eax, %rbp
	movzwl	-4(%rsi,%rbp,4), %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	incl	%ebx
	movzwl	-2(%rsi,%rbp,4), %eax
	imull	%ebx, %eax
	xorl	%edx, %edx
	divl	%r8d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	65535(%rcx,%rax), %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movzwl	2(%rsi,%rbp,4), %eax
	imull	%ebx, %eax
	xorl	%edx, %edx
	divl	%r8d
	addl	%ecx, %eax
	incq	%rbp
	.p2align	4, 0x90
.LBB11_129:                             #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	addw	$8, -6(%rsi,%rbp,4)
	decq	%rbp
	cmpq	$1, %rbp
	jg	.LBB11_129
# BB#130:                               #   in Loop: Header=BB11_33 Depth=4
	movzwl	2(%rsi), %ecx
	cmpl	$3801, %ecx             # imm = 0xED9
	jb	.LBB11_132
# BB#131:                               #   in Loop: Header=BB11_33 Depth=4
	movq	136(%rsp), %rdi         # 8-byte Reload
	movl	%eax, %r13d
	movq	%r10, %rbx
	movq	%r9, %rbp
	callq	qtm_update_model
	movq	%rbp, %r9
	movq	%rbx, %r10
	movl	%r13d, %eax
	movq	(%rsp), %r11            # 8-byte Reload
.LBB11_132:                             # %.preheader992.preheader
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	64(%rsp), %r13          # 8-byte Reload
	jmp	.LBB11_134
	.p2align	4, 0x90
.LBB11_133:                             #   in Loop: Header=BB11_134 Depth=5
	addl	%eax, %eax
	leal	1(%rcx,%rcx), %ecx
	shldl	$1, %r15d, %r14d
	addl	%r15d, %r15d
	decb	%r12b
.LBB11_134:                             # %.preheader992
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movl	%eax, %edx
	xorw	%cx, %dx
	jns	.LBB11_138
# BB#135:                               #   in Loop: Header=BB11_134 Depth=5
	testb	$64, %ch
	jne	.LBB11_190
# BB#136:                               #   in Loop: Header=BB11_134 Depth=5
	movl	%eax, %edx
	andl	$16384, %edx            # imm = 0x4000
	testw	%dx, %dx
	je	.LBB11_190
# BB#137:                               #   in Loop: Header=BB11_134 Depth=5
	xorl	$16384, %r14d           # imm = 0x4000
	andl	$16383, %eax            # imm = 0x3FFF
	orl	$16384, %ecx            # imm = 0x4000
.LBB11_138:                             #   in Loop: Header=BB11_134 Depth=5
	cmpb	$16, %r12b
	ja	.LBB11_133
# BB#139:                               #   in Loop: Header=BB11_134 Depth=5
	movl	%eax, %ebp
	movq	%rcx, %rbx
	cmpq	%r9, %r10
	jb	.LBB11_144
# BB#140:                               #   in Loop: Header=BB11_134 Depth=5
	movq	2128(%r11), %rax
	testq	%rax, %rax
	movq	48(%r11), %rsi
	movl	92(%r11), %edx
	je	.LBB11_142
# BB#141:                               #   in Loop: Header=BB11_134 Depth=5
	movq	2120(%r11), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB11_143
	jmp	.LBB11_295
.LBB11_142:                             #   in Loop: Header=BB11_134 Depth=5
	movl	(%r11), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB11_295
.LBB11_143:                             #   in Loop: Header=BB11_134 Depth=5
	movq	(%rsp), %r11            # 8-byte Reload
	movq	48(%r11), %r10
	movq	%r10, 56(%r11)
	movslq	%eax, %r9
	addq	%r10, %r9
	movq	%r9, 64(%r11)
.LBB11_144:                             #   in Loop: Header=BB11_134 Depth=5
	movzbl	%r12b, %eax
	movzbl	(%r10), %ecx
	shll	$8, %ecx
	movzbl	1(%r10), %edx
	orl	%ecx, %edx
	movl	$16, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%edx, %r15d
	addb	$16, %r12b
	addq	$2, %r10
	movq	%rbx, %rcx
	movl	%ebp, %eax
	jmp	.LBB11_133
.LBB11_145:                             #   in Loop: Header=BB11_33 Depth=4
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movb	268(%r11,%rax), %bl
	movl	$3, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testb	%bl, %bl
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	je	.LBB11_216
# BB#146:                               # %.lr.ph1436.preheader
                                        #   in Loop: Header=BB11_33 Depth=4
	xorl	%ebp, %ebp
.LBB11_147:                             # %.lr.ph1436
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cmpb	$16, %r12b
	ja	.LBB11_154
# BB#148:                               #   in Loop: Header=BB11_147 Depth=5
	cmpq	%r9, %r10
	jb	.LBB11_153
# BB#149:                               #   in Loop: Header=BB11_147 Depth=5
	movq	2128(%r11), %rax
	testq	%rax, %rax
	movq	48(%r11), %rsi
	movl	92(%r11), %edx
	je	.LBB11_151
# BB#150:                               #   in Loop: Header=BB11_147 Depth=5
	movq	2120(%r11), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB11_152
	jmp	.LBB11_295
.LBB11_151:                             #   in Loop: Header=BB11_147 Depth=5
	movl	(%r11), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB11_295
.LBB11_152:                             #   in Loop: Header=BB11_147 Depth=5
	movq	(%rsp), %r11            # 8-byte Reload
	movq	48(%r11), %r10
	movq	%r10, 56(%r11)
	movslq	%eax, %r9
	addq	%r10, %r9
	movq	%r9, 64(%r11)
.LBB11_153:                             #   in Loop: Header=BB11_147 Depth=5
	movzbl	%r12b, %eax
	movzbl	(%r10), %ecx
	shll	$8, %ecx
	movzbl	1(%r10), %edx
	orl	%ecx, %edx
	movl	$16, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%edx, %r15d
	addb	$16, %r12b
	addq	$2, %r10
.LBB11_154:                             #   in Loop: Header=BB11_147 Depth=5
	cmpb	%bl, %r12b
	movl	%r12d, %eax
	jb	.LBB11_156
# BB#155:                               #   in Loop: Header=BB11_147 Depth=5
	movl	%ebx, %eax
.LBB11_156:                             #   in Loop: Header=BB11_147 Depth=5
	movzbl	%al, %eax
	movl	%ebp, %edx
	movl	%eax, %ecx
	shll	%cl, %edx
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%r15d, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebp
	orl	%edx, %ebp
	movl	%eax, %ecx
	shll	%cl, %r15d
	subb	%al, %r12b
	subb	%al, %bl
	jne	.LBB11_147
	jmp	.LBB11_217
.LBB11_157:                             #   in Loop: Header=BB11_33 Depth=4
	movl	%edx, 24(%rsp)          # 4-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	movb	337(%r11,%rax), %bl
	xorl	%ebp, %ebp
	testb	%bl, %bl
	je	.LBB11_168
.LBB11_158:                             # %.lr.ph1375
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cmpb	$16, %r12b
	ja	.LBB11_165
# BB#159:                               #   in Loop: Header=BB11_158 Depth=5
	cmpq	%r9, %r10
	jb	.LBB11_164
# BB#160:                               #   in Loop: Header=BB11_158 Depth=5
	movq	2128(%r11), %rax
	testq	%rax, %rax
	movq	48(%r11), %rsi
	movl	92(%r11), %edx
	je	.LBB11_162
# BB#161:                               #   in Loop: Header=BB11_158 Depth=5
	movq	2120(%r11), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB11_163
	jmp	.LBB11_295
.LBB11_162:                             #   in Loop: Header=BB11_158 Depth=5
	movl	(%r11), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB11_295
.LBB11_163:                             #   in Loop: Header=BB11_158 Depth=5
	movq	(%rsp), %r11            # 8-byte Reload
	movq	48(%r11), %r10
	movq	%r10, 56(%r11)
	movslq	%eax, %r9
	addq	%r10, %r9
	movq	%r9, 64(%r11)
.LBB11_164:                             #   in Loop: Header=BB11_158 Depth=5
	movzbl	%r12b, %eax
	movzbl	(%r10), %ecx
	shll	$8, %ecx
	movzbl	1(%r10), %edx
	orl	%ecx, %edx
	movl	$16, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%edx, %r15d
	addb	$16, %r12b
	addq	$2, %r10
.LBB11_165:                             #   in Loop: Header=BB11_158 Depth=5
	cmpb	%bl, %r12b
	movl	%r12d, %eax
	jb	.LBB11_167
# BB#166:                               #   in Loop: Header=BB11_158 Depth=5
	movl	%ebx, %eax
.LBB11_167:                             #   in Loop: Header=BB11_158 Depth=5
	movzbl	%al, %eax
	movl	%eax, %ecx
	shll	%cl, %ebp
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%r15d, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	orl	%edx, %ebp
	movl	%eax, %ecx
	shll	%cl, %r15d
	subb	%al, %r12b
	subb	%al, %bl
	jne	.LBB11_158
.LBB11_168:                             # %._crit_edge1376
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	8(%rsp), %rax           # 8-byte Reload
	movzbl	310(%r11,%rax), %edx
	movq	32(%rsp), %r13          # 8-byte Reload
	movl	24(%rsp), %r8d          # 4-byte Reload
	subl	%r13d, %r8d
	movslq	468(%r11), %rbx
	cmpq	$2, %rbx
	movq	472(%r11), %rcx
	movzwl	2(%rcx), %esi
	movl	$1, %eax
	jl	.LBB11_173
# BB#169:                               # %.lr.ph1384
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	$1, %edx
	subl	%r13d, %edx
	movzwl	%r14w, %eax
	addl	%edx, %eax
	imull	%esi, %eax
	decl	%eax
	movl	%r8d, 24(%rsp)          # 4-byte Spill
	movzwl	%r8w, %edi
	incl	%edi
	xorl	%edx, %edx
	divl	%edi
	movzwl	%ax, %edx
	movl	$1, %eax
.LBB11_170:                             #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	2(%rcx,%rax,4), %edi
	cmpl	%edx, %edi
	jbe	.LBB11_172
# BB#171:                               #   in Loop: Header=BB11_170 Depth=5
	incq	%rax
	cmpq	%rbx, %rax
	jl	.LBB11_170
.LBB11_172:                             # %.._crit_edge1385.loopexit_crit_edge
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	32(%rsp), %r13          # 8-byte Reload
	movl	24(%rsp), %r8d          # 4-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
.LBB11_173:                             # %._crit_edge1385
                                        #   in Loop: Header=BB11_33 Depth=4
	leal	5(%rbp,%rdx), %ebp
	movslq	%eax, %rdi
	movzwl	-4(%rcx,%rdi,4), %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	incl	%r8d
	movzwl	-2(%rcx,%rdi,4), %eax
	imull	%r8d, %eax
	xorl	%edx, %edx
	divl	%esi
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	65535(%r13,%rax), %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movzwl	2(%rcx,%rdi,4), %eax
	imull	%r8d, %eax
	xorl	%edx, %edx
	divl	%esi
	addl	%r13d, %eax
	incq	%rdi
.LBB11_174:                             #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	addw	$8, -6(%rcx,%rdi,4)
	decq	%rdi
	cmpq	$1, %rdi
	jg	.LBB11_174
# BB#175:                               #   in Loop: Header=BB11_33 Depth=4
	movzwl	2(%rcx), %ecx
	cmpl	$3801, %ecx             # imm = 0xED9
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	jb	.LBB11_177
# BB#176:                               #   in Loop: Header=BB11_33 Depth=4
	movq	120(%rsp), %rdi         # 8-byte Reload
	movl	%eax, %r13d
	movq	%r10, %rbx
	movq	%r9, %rbp
	callq	qtm_update_model
	movq	%rbp, %r9
	movq	%rbx, %r10
	movl	%r13d, %eax
	movq	(%rsp), %r11            # 8-byte Reload
.LBB11_177:                             # %.preheader994.preheader
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	64(%rsp), %r13          # 8-byte Reload
	jmp	.LBB11_179
	.p2align	4, 0x90
.LBB11_178:                             #   in Loop: Header=BB11_179 Depth=5
	addl	%eax, %eax
	leal	1(%rcx,%rcx), %ecx
	shldl	$1, %r15d, %r14d
	addl	%r15d, %r15d
	decb	%r12b
.LBB11_179:                             # %.preheader994
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movl	%eax, %edx
	xorw	%cx, %dx
	jns	.LBB11_183
# BB#180:                               #   in Loop: Header=BB11_179 Depth=5
	testb	$64, %ch
	jne	.LBB11_202
# BB#181:                               #   in Loop: Header=BB11_179 Depth=5
	movl	%eax, %edx
	andl	$16384, %edx            # imm = 0x4000
	testw	%dx, %dx
	je	.LBB11_202
# BB#182:                               #   in Loop: Header=BB11_179 Depth=5
	xorl	$16384, %r14d           # imm = 0x4000
	andl	$16383, %eax            # imm = 0x3FFF
	orl	$16384, %ecx            # imm = 0x4000
.LBB11_183:                             #   in Loop: Header=BB11_179 Depth=5
	cmpb	$16, %r12b
	ja	.LBB11_178
# BB#184:                               #   in Loop: Header=BB11_179 Depth=5
	movl	%eax, %ebp
	movq	%rcx, %rbx
	cmpq	%r9, %r10
	jb	.LBB11_189
# BB#185:                               #   in Loop: Header=BB11_179 Depth=5
	movq	2128(%r11), %rax
	testq	%rax, %rax
	movq	48(%r11), %rsi
	movl	92(%r11), %edx
	je	.LBB11_187
# BB#186:                               #   in Loop: Header=BB11_179 Depth=5
	movq	2120(%r11), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB11_188
	jmp	.LBB11_295
.LBB11_187:                             #   in Loop: Header=BB11_179 Depth=5
	movl	(%r11), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB11_295
.LBB11_188:                             #   in Loop: Header=BB11_179 Depth=5
	movq	(%rsp), %r11            # 8-byte Reload
	movq	48(%r11), %r10
	movq	%r10, 56(%r11)
	movslq	%eax, %r9
	addq	%r10, %r9
	movq	%r9, 64(%r11)
.LBB11_189:                             #   in Loop: Header=BB11_179 Depth=5
	movzbl	%r12b, %eax
	movzbl	(%r10), %ecx
	shll	$8, %ecx
	movzbl	1(%r10), %edx
	orl	%ecx, %edx
	movl	$16, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%edx, %r15d
	addb	$16, %r12b
	addq	$2, %r10
	movq	%rbx, %rcx
	movl	%ebp, %eax
	jmp	.LBB11_178
.LBB11_190:                             #   in Loop: Header=BB11_33 Depth=4
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movb	268(%r11,%rax), %bl
	movl	$4, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testb	%bl, %bl
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	je	.LBB11_216
# BB#191:                               # %.lr.ph1416.preheader
                                        #   in Loop: Header=BB11_33 Depth=4
	xorl	%ebp, %ebp
.LBB11_192:                             # %.lr.ph1416
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cmpb	$16, %r12b
	ja	.LBB11_199
# BB#193:                               #   in Loop: Header=BB11_192 Depth=5
	cmpq	%r9, %r10
	jb	.LBB11_198
# BB#194:                               #   in Loop: Header=BB11_192 Depth=5
	movq	2128(%r11), %rax
	testq	%rax, %rax
	movq	48(%r11), %rsi
	movl	92(%r11), %edx
	je	.LBB11_196
# BB#195:                               #   in Loop: Header=BB11_192 Depth=5
	movq	2120(%r11), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB11_197
	jmp	.LBB11_295
.LBB11_196:                             #   in Loop: Header=BB11_192 Depth=5
	movl	(%r11), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB11_295
.LBB11_197:                             #   in Loop: Header=BB11_192 Depth=5
	movq	(%rsp), %r11            # 8-byte Reload
	movq	48(%r11), %r10
	movq	%r10, 56(%r11)
	movslq	%eax, %r9
	addq	%r10, %r9
	movq	%r9, 64(%r11)
.LBB11_198:                             #   in Loop: Header=BB11_192 Depth=5
	movzbl	%r12b, %eax
	movzbl	(%r10), %ecx
	shll	$8, %ecx
	movzbl	1(%r10), %edx
	orl	%ecx, %edx
	movl	$16, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%edx, %r15d
	addb	$16, %r12b
	addq	$2, %r10
.LBB11_199:                             #   in Loop: Header=BB11_192 Depth=5
	cmpb	%bl, %r12b
	movl	%r12d, %eax
	jb	.LBB11_201
# BB#200:                               #   in Loop: Header=BB11_192 Depth=5
	movl	%ebx, %eax
.LBB11_201:                             #   in Loop: Header=BB11_192 Depth=5
	movzbl	%al, %eax
	movl	%ebp, %edx
	movl	%eax, %ecx
	shll	%cl, %edx
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%r15d, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebp
	orl	%edx, %ebp
	movl	%eax, %ecx
	shll	%cl, %r15d
	subb	%al, %r12b
	subb	%al, %bl
	jne	.LBB11_192
	jmp	.LBB11_217
.LBB11_202:                             #   in Loop: Header=BB11_33 Depth=4
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movb	268(%r11,%rax), %bl
	testb	%bl, %bl
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	je	.LBB11_216
# BB#203:                               # %.lr.ph1396.preheader
                                        #   in Loop: Header=BB11_33 Depth=4
	xorl	%ebp, %ebp
.LBB11_204:                             # %.lr.ph1396
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cmpb	$16, %r12b
	ja	.LBB11_211
# BB#205:                               #   in Loop: Header=BB11_204 Depth=5
	cmpq	%r9, %r10
	jb	.LBB11_210
# BB#206:                               #   in Loop: Header=BB11_204 Depth=5
	movq	2128(%r11), %rax
	testq	%rax, %rax
	movq	48(%r11), %rsi
	movl	92(%r11), %edx
	je	.LBB11_208
# BB#207:                               #   in Loop: Header=BB11_204 Depth=5
	movq	2120(%r11), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB11_209
	jmp	.LBB11_295
.LBB11_208:                             #   in Loop: Header=BB11_204 Depth=5
	movl	(%r11), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB11_295
.LBB11_209:                             #   in Loop: Header=BB11_204 Depth=5
	movq	(%rsp), %r11            # 8-byte Reload
	movq	48(%r11), %r10
	movq	%r10, 56(%r11)
	movslq	%eax, %r9
	addq	%r10, %r9
	movq	%r9, 64(%r11)
.LBB11_210:                             #   in Loop: Header=BB11_204 Depth=5
	movzbl	%r12b, %eax
	movzbl	(%r10), %ecx
	shll	$8, %ecx
	movzbl	1(%r10), %edx
	orl	%ecx, %edx
	movl	$16, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%edx, %r15d
	addb	$16, %r12b
	addq	$2, %r10
.LBB11_211:                             #   in Loop: Header=BB11_204 Depth=5
	cmpb	%bl, %r12b
	movl	%r12d, %eax
	jb	.LBB11_213
# BB#212:                               #   in Loop: Header=BB11_204 Depth=5
	movl	%ebx, %eax
.LBB11_213:                             #   in Loop: Header=BB11_204 Depth=5
	movzbl	%al, %eax
	movl	%ebp, %edx
	movl	%eax, %ecx
	shll	%cl, %edx
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%r15d, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebp
	orl	%edx, %ebp
	movl	%eax, %ecx
	shll	%cl, %r15d
	subb	%al, %r12b
	subb	%al, %bl
	jne	.LBB11_204
	jmp	.LBB11_217
.LBB11_216:                             #   in Loop: Header=BB11_33 Depth=4
	xorl	%ebp, %ebp
.LBB11_217:                             # %.loopexit989
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	%r9, 40(%rsp)           # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	100(%r11,%rax,4), %r8d
	leal	1(%rbp,%r8), %ecx
	movq	56(%rsp), %rdx          # 8-byte Reload
	movl	%edx, %esi
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi), %rax
	movl	%ecx, %edi
	subl	%edx, %edi
	jbe	.LBB11_228
# BB#218:                               #   in Loop: Header=BB11_33 Depth=4
	movl	24(%r11), %r9d
	subl	%edi, %r9d
	jl	.LBB11_306
# BB#219:                               #   in Loop: Header=BB11_33 Depth=4
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r9), %rcx
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	%ebx, %edx
	subl	%edi, %edx
	jle	.LBB11_237
# BB#220:                               #   in Loop: Header=BB11_33 Depth=4
	testl	%edi, %edi
	jle	.LBB11_238
# BB#221:                               # %.lr.ph1451.preheader
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	%r10, 16(%rsp)          # 8-byte Spill
	movq	56(%rsp), %rbx          # 8-byte Reload
	leal	-2(%rbx), %r10d
	subl	%ebp, %r10d
	addl	%r8d, %ebp
	subl	%r8d, %r10d
	cmpl	$-3, %r10d
	movl	$-2, %ebx
	cmovgl	%r10d, %ebx
	leal	2(%rbx,%rbp), %r8d
	subl	56(%rsp), %r8d          # 4-byte Folded Reload
	incq	%r8
	cmpq	$31, %r8
	jbe	.LBB11_256
# BB#222:                               # %min.iters.checked2252
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	%r8, 112(%rsp)          # 8-byte Spill
	movabsq	$8589934560, %rbx       # imm = 0x1FFFFFFE0
	andq	%rbx, %r8
	je	.LBB11_256
# BB#223:                               # %vector.memcheck2266
                                        #   in Loop: Header=BB11_33 Depth=4
	cmpl	$-3, %r10d
	movl	$-2, %ebx
	cmovlel	%ebx, %r10d
	leal	2(%r10,%rbp), %ebp
	subl	56(%rsp), %ebp          # 4-byte Folded Reload
	leaq	(%r9,%rbp), %rbx
	movq	96(%rsp), %r10          # 8-byte Reload
	addq	%r10, %rbx
	cmpq	%rbx, %rax
	jae	.LBB11_225
# BB#224:                               # %vector.memcheck2266
                                        #   in Loop: Header=BB11_33 Depth=4
	addq	%rsi, %rbp
	addq	%r10, %rbp
	cmpq	%rbp, %rcx
	jb	.LBB11_256
.LBB11_225:                             # %vector.body2247.preheader
                                        #   in Loop: Header=BB11_33 Depth=4
	leaq	-32(%r8), %rbx
	movl	%ebx, %r11d
	shrl	$5, %r11d
	incl	%r11d
	andq	$3, %r11
	je	.LBB11_249
# BB#226:                               # %vector.body2247.prol.preheader
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	88(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%r9), %r13
	leaq	(%rbp,%rsi), %r10
	negq	%r11
	xorl	%ebp, %ebp
.LBB11_227:                             # %vector.body2247.prol
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movups	-16(%r13,%rbp), %xmm0
	movups	(%r13,%rbp), %xmm1
	movups	%xmm0, -16(%r10,%rbp)
	movups	%xmm1, (%r10,%rbp)
	addq	$32, %rbp
	incq	%r11
	jne	.LBB11_227
	jmp	.LBB11_250
.LBB11_228:                             #   in Loop: Header=BB11_33 Depth=4
	movq	8(%rsp), %rbp           # 8-byte Reload
	testl	%ebp, %ebp
	jle	.LBB11_275
# BB#229:                               # %.lr.ph1446.preheader
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	%r10, %r8
	movl	%ecx, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	movl	%ebp, %edi
	notl	%edi
	cmpl	$-3, %edi
	movl	$-2, %ebp
	cmovgl	%edi, %ebp
	movq	8(%rsp), %rbx           # 8-byte Reload
	leal	1(%rbx,%rbp), %r10d
	movq	8(%rsp), %rbp           # 8-byte Reload
	incq	%r10
	cmpq	$32, %r10
	jb	.LBB11_234
# BB#230:                               # %min.iters.checked2294
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	%r10, %r9
	movabsq	$8589934560, %rbp       # imm = 0x1FFFFFFE0
	andq	%rbp, %r9
	je	.LBB11_233
# BB#231:                               # %vector.memcheck2308
                                        #   in Loop: Header=BB11_33 Depth=4
	cmpl	$-3, %edi
	movl	$-2, %ebp
	cmovlel	%ebp, %edi
	movq	8(%rsp), %rbx           # 8-byte Reload
	leal	1(%rbx,%rdi), %ebp
	addq	%rsi, %rbp
	movq	%rbp, %rdi
	subq	%rdx, %rdi
	movq	96(%rsp), %rbx          # 8-byte Reload
	addq	%rbx, %rdi
	cmpq	%rdi, %rax
	jae	.LBB11_240
# BB#232:                               # %vector.memcheck2308
                                        #   in Loop: Header=BB11_33 Depth=4
	addq	%rbx, %rbp
	cmpq	%rbp, %rcx
	jae	.LBB11_240
.LBB11_233:                             #   in Loop: Header=BB11_33 Depth=4
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB11_234:                             #   in Loop: Header=BB11_33 Depth=4
	movl	%ebp, %edx
	movq	%r8, %r10
.LBB11_235:                             # %.lr.ph1446.preheader2333
                                        #   in Loop: Header=BB11_33 Depth=4
	incl	%edx
.LBB11_236:                             # %.lr.ph1446
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzbl	(%rcx), %ebx
	incq	%rcx
	movb	%bl, (%rax)
	incq	%rax
	decl	%edx
	cmpl	$1, %edx
	jg	.LBB11_236
	jmp	.LBB11_275
.LBB11_237:                             #   in Loop: Header=BB11_33 Depth=4
	movl	%ebx, %edx
	movq	%rbx, %rbp
	testl	%edx, %edx
	jg	.LBB11_259
	jmp	.LBB11_275
.LBB11_238:                             #   in Loop: Header=BB11_33 Depth=4
	movq	48(%rsp), %rcx          # 8-byte Reload
.LBB11_239:                             # %.preheader987
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	8(%rsp), %rbp           # 8-byte Reload
	testl	%edx, %edx
	jg	.LBB11_259
	jmp	.LBB11_275
.LBB11_240:                             # %vector.body2289.preheader
                                        #   in Loop: Header=BB11_33 Depth=4
	leaq	-32(%r9), %rbx
	movq	%rbx, 16(%rsp)          # 8-byte Spill
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	shrl	$5, %ebx
	incl	%ebx
	andq	$3, %rbx
	je	.LBB11_243
# BB#241:                               # %vector.body2289.prol.preheader
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	%rsi, %r11
	subq	%rdx, %r11
	movq	88(%rsp), %rdi          # 8-byte Reload
	addq	%rdi, %r11
	leaq	(%rdi,%rsi), %rbp
	negq	%rbx
	xorl	%edi, %edi
.LBB11_242:                             # %vector.body2289.prol
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movups	-16(%r11,%rdi), %xmm0
	movups	(%r11,%rdi), %xmm1
	movups	%xmm0, -16(%rbp,%rdi)
	movups	%xmm1, (%rbp,%rdi)
	addq	$32, %rdi
	incq	%rbx
	jne	.LBB11_242
	jmp	.LBB11_244
.LBB11_243:                             #   in Loop: Header=BB11_33 Depth=4
	xorl	%edi, %edi
.LBB11_244:                             # %vector.body2289.prol.loopexit
                                        #   in Loop: Header=BB11_33 Depth=4
	cmpq	$96, 16(%rsp)           # 8-byte Folded Reload
	jb	.LBB11_247
# BB#245:                               # %vector.body2289.preheader.new
                                        #   in Loop: Header=BB11_33 Depth=4
	negq	%rdx
	movq	%r9, %rbp
	subq	%rdi, %rbp
	addq	%rdi, %rsi
	addq	80(%rsp), %rsi          # 8-byte Folded Reload
.LBB11_246:                             # %vector.body2289
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movups	-112(%rdx,%rsi), %xmm0
	movups	-96(%rdx,%rsi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx,%rsi), %xmm0
	movups	-64(%rdx,%rsi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx,%rsi), %xmm0
	movups	-32(%rdx,%rsi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx,%rsi), %xmm0
	movups	(%rdx,%rsi), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-128, %rbp
	jne	.LBB11_246
.LBB11_247:                             # %middle.block2290
                                        #   in Loop: Header=BB11_33 Depth=4
	cmpq	%r9, %r10
	movq	(%rsp), %r11            # 8-byte Reload
	movq	%r8, %r10
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB11_275
# BB#248:                               #   in Loop: Header=BB11_33 Depth=4
	movl	%ebp, %edx
	subl	%r9d, %edx
	addq	%r9, %rax
	addq	%r9, %rcx
	jmp	.LBB11_235
.LBB11_249:                             #   in Loop: Header=BB11_33 Depth=4
	xorl	%ebp, %ebp
.LBB11_250:                             # %vector.body2247.prol.loopexit
                                        #   in Loop: Header=BB11_33 Depth=4
	cmpq	$96, %rbx
	jb	.LBB11_253
# BB#251:                               # %vector.body2247.preheader.new
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	%r8, %r10
	subq	%rbp, %r10
	addq	%rbp, %r9
	movq	80(%rsp), %rbx          # 8-byte Reload
	addq	%rbx, %r9
	addq	%rbp, %rsi
	addq	%rbx, %rsi
.LBB11_252:                             # %vector.body2247
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movups	-112(%r9), %xmm0
	movups	-96(%r9), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%r9), %xmm0
	movups	-64(%r9), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%r9), %xmm0
	movups	-32(%r9), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%r9), %xmm0
	movups	(%r9), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %r9
	subq	$-128, %rsi
	addq	$-128, %r10
	jne	.LBB11_252
.LBB11_253:                             # %middle.block2248
                                        #   in Loop: Header=BB11_33 Depth=4
	addq	%r8, %rax
	cmpq	%r8, 112(%rsp)          # 8-byte Folded Reload
	movq	(%rsp), %r11            # 8-byte Reload
	movq	64(%rsp), %r13          # 8-byte Reload
	jne	.LBB11_255
# BB#254:                               #   in Loop: Header=BB11_33 Depth=4
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %r10          # 8-byte Reload
	jmp	.LBB11_239
.LBB11_255:                             #   in Loop: Header=BB11_33 Depth=4
	subl	%r8d, %edi
	addq	%r8, %rcx
.LBB11_256:                             #   in Loop: Header=BB11_33 Depth=4
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	incl	%edi
.LBB11_257:                             # %.lr.ph1451
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzbl	(%rcx), %ebx
	incq	%rcx
	movb	%bl, (%rax)
	incq	%rax
	decl	%edi
	cmpl	$1, %edi
	jg	.LBB11_257
# BB#258:                               #   in Loop: Header=BB11_33 Depth=4
	movq	48(%rsp), %rcx          # 8-byte Reload
	testl	%edx, %edx
	jle	.LBB11_275
.LBB11_259:                             # %.lr.ph1456.preheader
                                        #   in Loop: Header=BB11_33 Depth=4
	movl	%edx, %esi
	notl	%esi
	cmpl	$-3, %esi
	movl	$-2, %edi
	cmovgl	%esi, %edi
	leal	1(%rdx,%rdi), %edi
	incq	%rdi
	cmpq	$31, %rdi
	jbe	.LBB11_273
# BB#260:                               # %min.iters.checked
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	%rdi, %r8
	movabsq	$8589934560, %rbx       # imm = 0x1FFFFFFE0
	andq	%rbx, %r8
	je	.LBB11_273
# BB#261:                               # %vector.memcheck
                                        #   in Loop: Header=BB11_33 Depth=4
	cmpl	$-3, %esi
	movl	$-2, %ebp
	cmovlel	%ebp, %esi
	leal	1(%rdx,%rsi), %esi
	leaq	1(%rcx,%rsi), %rbp
	cmpq	%rbp, %rax
	jae	.LBB11_264
# BB#262:                               # %vector.memcheck
                                        #   in Loop: Header=BB11_33 Depth=4
	leaq	1(%rax,%rsi), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB11_264
# BB#263:                               #   in Loop: Header=BB11_33 Depth=4
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB11_273
.LBB11_264:                             # %vector.body.preheader
                                        #   in Loop: Header=BB11_33 Depth=4
	leaq	-32(%r8), %rbp
	movl	%ebp, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB11_267
# BB#265:                               # %vector.body.prol.preheader
                                        #   in Loop: Header=BB11_33 Depth=4
	negq	%rsi
	xorl	%ebx, %ebx
.LBB11_266:                             # %vector.body.prol
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movups	(%rcx,%rbx), %xmm0
	movups	16(%rcx,%rbx), %xmm1
	movups	%xmm0, (%rax,%rbx)
	movups	%xmm1, 16(%rax,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB11_266
	jmp	.LBB11_268
.LBB11_267:                             #   in Loop: Header=BB11_33 Depth=4
	xorl	%ebx, %ebx
.LBB11_268:                             # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB11_33 Depth=4
	cmpq	$96, %rbp
	jb	.LBB11_271
# BB#269:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	%r8, %rsi
	subq	%rbx, %rsi
	leaq	112(%rcx,%rbx), %rbp
	leaq	112(%rax,%rbx), %rbx
.LBB11_270:                             # %vector.body
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbp
	subq	$-128, %rbx
	addq	$-128, %rsi
	jne	.LBB11_270
.LBB11_271:                             # %middle.block
                                        #   in Loop: Header=BB11_33 Depth=4
	cmpq	%r8, %rdi
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB11_275
# BB#272:                               #   in Loop: Header=BB11_33 Depth=4
	subl	%r8d, %edx
	addq	%r8, %rax
	addq	%r8, %rcx
.LBB11_273:                             # %.lr.ph1456.preheader2331
                                        #   in Loop: Header=BB11_33 Depth=4
	incl	%edx
.LBB11_274:                             # %.lr.ph1456
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_16 Depth=3
                                        #         Parent Loop BB11_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzbl	(%rcx), %ebx
	incq	%rcx
	movb	%bl, (%rax)
	incq	%rax
	decl	%edx
	cmpl	$1, %edx
	jg	.LBB11_274
.LBB11_275:                             # %.loopexit
                                        #   in Loop: Header=BB11_33 Depth=4
	addl	56(%rsp), %ebp          # 4-byte Folded Reload
	movq	40(%rsp), %r9           # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	movl	32(%rsp), %ebx          # 4-byte Reload
.LBB11_276:                             # %.backedge
                                        #   in Loop: Header=BB11_33 Depth=4
	movq	168(%rsp), %rax         # 8-byte Reload
	cmpl	%eax, %ebp
	movl	%ebp, %ecx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	jb	.LBB11_33
.LBB11_277:                             # %._crit_edge1474
                                        #   in Loop: Header=BB11_16 Depth=3
	movl	%ebp, %eax
	addq	48(%rsp), %rax          # 8-byte Folded Reload
	movq	%rax, 80(%r11)
	movl	%ebp, %ecx
	subl	104(%rsp), %ecx         # 4-byte Folded Reload
	cmpl	$32768, %ecx            # imm = 0x8000
	jb	.LBB11_16
# BB#278:                               #   in Loop: Header=BB11_15 Depth=2
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	jne	.LBB11_307
# BB#279:                               #   in Loop: Header=BB11_15 Depth=2
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movl	%ebx, 32(%rsp)          # 4-byte Spill
	movzbl	%r12b, %eax
	movl	%eax, %edx
	andl	$7, %edx
	movl	%eax, %ecx
	andl	$7, %ecx
	je	.LBB11_281
# BB#280:                               #   in Loop: Header=BB11_15 Depth=2
	subl	%edx, %eax
	movl	%eax, %r12d
.LBB11_281:                             #   in Loop: Header=BB11_15 Depth=2
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r15d
.LBB11_282:                             # %.preheader997
                                        #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB11_283 Depth 4
	xorl	%ebp, %ebp
	movb	$8, %bl
	.p2align	4, 0x90
.LBB11_283:                             #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        #       Parent Loop BB11_282 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpb	$16, %r12b
	ja	.LBB11_290
# BB#284:                               #   in Loop: Header=BB11_283 Depth=4
	cmpq	%r9, %r10
	jb	.LBB11_289
# BB#285:                               #   in Loop: Header=BB11_283 Depth=4
	movq	2128(%r11), %rax
	testq	%rax, %rax
	movq	48(%r11), %rsi
	movl	92(%r11), %edx
	je	.LBB11_287
# BB#286:                               #   in Loop: Header=BB11_283 Depth=4
	movq	2120(%r11), %rdi
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB11_288
	jmp	.LBB11_295
.LBB11_287:                             #   in Loop: Header=BB11_283 Depth=4
	movl	(%r11), %edi
	callq	cli_readn
	testl	%eax, %eax
	js	.LBB11_295
.LBB11_288:                             #   in Loop: Header=BB11_283 Depth=4
	movq	(%rsp), %r11            # 8-byte Reload
	movq	48(%r11), %r10
	movq	%r10, 56(%r11)
	movslq	%eax, %r9
	addq	%r10, %r9
	movq	%r9, 64(%r11)
.LBB11_289:                             #   in Loop: Header=BB11_283 Depth=4
	movzbl	%r12b, %eax
	movzbl	(%r10), %ecx
	shll	$8, %ecx
	movzbl	1(%r10), %edx
	orl	%ecx, %edx
	movl	$16, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%edx, %r15d
	addb	$16, %r12b
	addq	$2, %r10
.LBB11_290:                             #   in Loop: Header=BB11_283 Depth=4
	cmpb	%bl, %r12b
	movl	%r12d, %eax
	jb	.LBB11_292
# BB#291:                               #   in Loop: Header=BB11_283 Depth=4
	movl	%ebx, %eax
.LBB11_292:                             #   in Loop: Header=BB11_283 Depth=4
	movzbl	%al, %eax
	movl	%ebp, %edx
	movl	%eax, %ecx
	shll	%cl, %edx
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%r15d, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebp
	orl	%edx, %ebp
	movl	%eax, %ecx
	shll	%cl, %r15d
	subb	%al, %r12b
	subb	%al, %bl
	movq	24(%rsp), %rdx          # 8-byte Reload
	jne	.LBB11_283
# BB#293:                               #   in Loop: Header=BB11_282 Depth=3
	cmpl	$255, %ebp
	jne	.LBB11_282
# BB#294:                               #   in Loop: Header=BB11_15 Depth=2
	movb	$0, 42(%r11)
	movq	8(%rsp), %rbp           # 8-byte Reload
	cmpl	24(%r11), %ebp
	movl	%ebp, %edi
	jne	.LBB11_15
	jmp	.LBB11_11
.LBB11_295:
	movq	(%rsp), %rax            # 8-byte Reload
	movl	$-123, 44(%rax)
	movl	$-123, %eax
	jmp	.LBB11_303
.LBB11_296:
	testq	%rdi, %rdi
	movq	%r13, %rcx
	je	.LBB11_301
# BB#297:
	cmpb	$0, 8(%r11)
	je	.LBB11_300
# BB#298:
	movl	%ebx, 32(%rsp)          # 4-byte Spill
	movq	%r9, 40(%rsp)           # 8-byte Spill
	movq	%r10, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	movl	4(%r11), %edi
	movl	%ebx, %edx
	movq	%r11, %rbp
	callq	cli_writen
	movq	%rbx, %rdx
	cmpl	%ebx, %eax
	jne	.LBB11_304
# BB#299:                               # %._crit_edge1747
	movq	72(%rbp), %rsi
	movq	%rbp, %r11
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	40(%rsp), %r9           # 8-byte Reload
	movq	%r13, %rcx
	movq	%rdx, %rdi
	movl	32(%rsp), %ebx          # 4-byte Reload
.LBB11_300:
	movslq	%edi, %rax
	addq	%rsi, %rax
	movq	%rax, 72(%r11)
.LBB11_301:
	movq	%r10, 56(%r11)
	movq	%r9, 64(%r11)
	movl	%r15d, 88(%r11)
	movb	%r12b, 96(%r11)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 28(%r11)
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	%eax, 32(%r11)
	movw	%cx, 36(%r11)
	movw	%bx, 38(%r11)
	movw	%r14w, 40(%r11)
.LBB11_302:
	xorl	%eax, %eax
.LBB11_303:
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_304:
	movl	$-123, 44(%rbp)
	movl	$-123, %eax
	jmp	.LBB11_303
.LBB11_305:
	movl	$-124, 44(%r11)
	movl	$-124, %eax
	jmp	.LBB11_303
.LBB11_306:
	movl	$.L.str.17, %edi
	jmp	.LBB11_308
.LBB11_307:
	movl	$.L.str.18, %edi
.LBB11_308:
	xorl	%eax, %eax
	movq	%r11, %rbx
	callq	cli_dbgmsg
	movl	$-124, 44(%rbx)
	movl	$-124, %eax
	jmp	.LBB11_303
.Lfunc_end11:
	.size	qtm_decompress, .Lfunc_end11-qtm_decompress
	.cfi_endproc

	.p2align	4, 0x90
	.type	qtm_update_model,@function
qtm_update_model:                       # @qtm_update_model
	.cfi_startproc
# BB#0:
	decl	(%rdi)
	je	.LBB12_5
# BB#1:
	movslq	4(%rdi), %rax
	testq	%rax, %rax
	jle	.LBB12_4
# BB#2:                                 # %.lr.ph72
	movq	8(%rdi), %rcx
	movw	2(%rcx,%rax,4), %dx
	incq	%rax
	.p2align	4, 0x90
.LBB12_3:                               # %.backedge
                                        # =>This Inner Loop Header: Depth=1
	movzwl	-6(%rcx,%rax,4), %esi
	shrl	%esi
	cmpw	%dx, %si
	leal	1(%rdx), %edx
	cmovaw	%si, %dx
	movw	%dx, -6(%rcx,%rax,4)
	decq	%rax
	cmpq	$1, %rax
                                        # kill: %DX<def> %DX<kill> %EDX<kill> %RDX<def>
	jg	.LBB12_3
	jmp	.LBB12_4
.LBB12_5:
	movl	$50, (%rdi)
	movslq	4(%rdi), %r11
	testq	%r11, %r11
	jle	.LBB12_6
# BB#7:                                 # %.lr.ph67
	movq	8(%rdi), %r10
	movw	2(%r10), %cx
	leaq	-1(%r11), %r8
	movq	%r11, %r9
	xorl	%edx, %edx
	andq	$3, %r9
	je	.LBB12_10
# BB#8:                                 # %.prol.preheader
	movw	%cx, %si
	.p2align	4, 0x90
.LBB12_9:                               # =>This Inner Loop Header: Depth=1
	movzwl	6(%r10,%rdx,4), %ecx
	subl	%ecx, %esi
	incl	%esi
	andl	$65534, %esi            # imm = 0xFFFE
	shrl	%esi
	movw	%si, 2(%r10,%rdx,4)
	incq	%rdx
	cmpq	%rdx, %r9
	movw	%cx, %si
	jne	.LBB12_9
.LBB12_10:                              # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB12_11
	.p2align	4, 0x90
.LBB12_13:                              # =>This Inner Loop Header: Depth=1
	movzwl	6(%r10,%rdx,4), %esi
	subl	%esi, %ecx
	incl	%ecx
	andl	$65534, %ecx            # imm = 0xFFFE
	shrl	%ecx
	movw	%cx, 2(%r10,%rdx,4)
	movzwl	10(%r10,%rdx,4), %ecx
	subl	%ecx, %esi
	incl	%esi
	andl	$65534, %esi            # imm = 0xFFFE
	shrl	%esi
	movw	%si, 6(%r10,%rdx,4)
	movzwl	14(%r10,%rdx,4), %esi
	subl	%esi, %ecx
	incl	%ecx
	andl	$65534, %ecx            # imm = 0xFFFE
	shrl	%ecx
	movw	%cx, 10(%r10,%rdx,4)
	movzwl	18(%r10,%rdx,4), %ecx
	subl	%ecx, %esi
	incl	%esi
	andl	$65534, %esi            # imm = 0xFFFE
	shrl	%esi
	movw	%si, 14(%r10,%rdx,4)
	leaq	4(%rdx), %rdx
	cmpq	%r11, %rdx
	jl	.LBB12_13
.LBB12_11:                              # %.preheader58
	cmpl	$2, %r11d
	jl	.LBB12_12
# BB#18:                                # %.lr.ph65
	movl	$1, %r9d
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB12_19:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_21 Depth 2
	movq	%r8, %rsi
	leaq	1(%rsi), %r8
	movslq	%r11d, %rcx
	cmpq	%rcx, %r8
	jge	.LBB12_14
# BB#20:                                # %.lr.ph63.preheader
                                        #   in Loop: Header=BB12_19 Depth=1
	movq	%r9, %rcx
	.p2align	4, 0x90
.LBB12_21:                              # %.lr.ph63
                                        #   Parent Loop BB12_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rdi), %rdx
	movzwl	2(%rdx,%rsi,4), %eax
	cmpw	2(%rdx,%rcx,4), %ax
	jae	.LBB12_23
# BB#22:                                #   in Loop: Header=BB12_21 Depth=2
	movl	(%rdx,%rsi,4), %r10d
	movl	(%rdx,%rcx,4), %eax
	movl	%eax, (%rdx,%rsi,4)
	movq	8(%rdi), %rax
	movl	%r10d, (%rax,%rcx,4)
	movl	4(%rdi), %r11d
.LBB12_23:                              #   in Loop: Header=BB12_21 Depth=2
	incq	%rcx
	movslq	%r11d, %rax
	cmpq	%rax, %rcx
	jl	.LBB12_21
.LBB12_14:                              # %.loopexit57
                                        #   in Loop: Header=BB12_19 Depth=1
	leal	-1(%r11), %ecx
	movslq	%ecx, %rdx
	incq	%r9
	cmpq	%rdx, %r8
	jl	.LBB12_19
	jmp	.LBB12_15
.LBB12_6:                               # %.preheader58.thread
	leal	-1(%r11), %ecx
	testl	%ecx, %ecx
	jns	.LBB12_16
	jmp	.LBB12_4
.LBB12_12:
	leal	-1(%r11), %ecx
.LBB12_15:                              # %.preheader
	testl	%ecx, %ecx
	js	.LBB12_4
.LBB12_16:                              # %.lr.ph
	movq	8(%rdi), %rax
	movslq	%ecx, %rcx
	incq	%rcx
	.p2align	4, 0x90
.LBB12_17:                              # =>This Inner Loop Header: Depth=1
	movzwl	2(%rax,%rcx,4), %edx
	addw	%dx, -2(%rax,%rcx,4)
	decq	%rcx
	jg	.LBB12_17
.LBB12_4:                               # %.loopexit
	retq
.Lfunc_end12:
	.size	qtm_update_model, .Lfunc_end12-qtm_update_model
	.cfi_endproc

	.globl	qtm_free
	.p2align	4, 0x90
	.type	qtm_free,@function
qtm_free:                               # @qtm_free
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 16
.Lcfi109:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB13_1
# BB#2:
	movq	16(%rbx), %rdi
	callq	free
	movq	48(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB13_1:
	popq	%rbx
	retq
.Lfunc_end13:
	.size	qtm_free, .Lfunc_end13-qtm_free
	.cfi_endproc

	.p2align	4, 0x90
	.type	mszip_make_decode_table,@function
mszip_make_decode_table:                # @mszip_make_decode_table
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi110:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi111:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi113:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi114:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 56
.Lcfi116:
	.cfi_offset %rbx, -56
.Lcfi117:
	.cfi_offset %r12, -48
.Lcfi118:
	.cfi_offset %r13, -40
.Lcfi119:
	.cfi_offset %r14, -32
.Lcfi120:
	.cfi_offset %r15, -24
.Lcfi121:
	.cfi_offset %rbp, -16
	movq	%rcx, %r9
	movq	%rdx, -40(%rsp)         # 8-byte Spill
	movl	%esi, %r13d
	movl	$1, %r11d
	movl	%r13d, %ecx
	shll	%cl, %r11d
	movl	%r11d, %ebp
	shrl	%ebp
	xorl	%ebx, %ebx
	testl	%r13d, %r13d
	movl	%edi, -64(%rsp)         # 4-byte Spill
	movq	%rbp, -16(%rsp)         # 8-byte Spill
	je	.LBB14_27
# BB#1:                                 # %.preheader140.lr.ph
	testl	%edi, %edi
	je	.LBB14_25
# BB#2:                                 # %.preheader140.us.preheader
	xorl	%ebx, %ebx
	movl	$1, %ecx
	movl	%ebp, %edx
	movq	%r13, -8(%rsp)          # 8-byte Spill
	movl	%r11d, -60(%rsp)        # 4-byte Spill
.LBB14_3:                               # %.preheader140.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_4 Depth 2
                                        #       Child Loop BB14_7 Depth 3
                                        #       Child Loop BB14_10 Depth 3
                                        #       Child Loop BB14_14 Depth 3
                                        #       Child Loop BB14_19 Depth 3
	movl	$1, %eax
	shll	%cl, %eax
	movzwl	%ax, %ebp
	leal	-1(%rdx), %eax
	movl	%eax, -20(%rsp)         # 4-byte Spill
	movq	%rdx, -56(%rsp)         # 8-byte Spill
	movl	%edx, %eax
	andl	$7, %eax
	movl	%eax, -24(%rsp)         # 4-byte Spill
	negl	%eax
	movl	%eax, -28(%rsp)         # 4-byte Spill
	leal	(,%rbp,8), %eax
	subl	%ebp, %eax
	movl	%eax, -32(%rsp)         # 4-byte Spill
	leal	(%rbp,%rbp), %r12d
	leal	(%r12,%r12,2), %r10d
	leal	(%rbp,%rbp,4), %r14d
	leal	(,%rbp,4), %r15d
	leal	(%rbp,%rbp,2), %edx
	xorl	%r13d, %r13d
	movl	%ecx, -44(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB14_4:                               #   Parent Loop BB14_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB14_7 Depth 3
                                        #       Child Loop BB14_10 Depth 3
                                        #       Child Loop BB14_14 Depth 3
                                        #       Child Loop BB14_19 Depth 3
	movzwl	%r13w, %esi
	movq	-40(%rsp), %rax         # 8-byte Reload
	movzbl	(%rax,%rsi), %esi
	cmpb	%cl, %sil
	jne	.LBB14_21
# BB#5:                                 #   in Loop: Header=BB14_4 Depth=2
	movq	-8(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%esi, %ecx
	movl	%ebx, %r11d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r11d
	leal	-1(%rsi), %eax
	movl	%esi, %ecx
	movl	%ebx, %edi
	andl	$3, %ecx
	je	.LBB14_8
# BB#6:                                 # %.prol.preheader230
                                        #   in Loop: Header=BB14_4 Depth=2
	negl	%ecx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB14_7:                               #   Parent Loop BB14_3 Depth=1
                                        #     Parent Loop BB14_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r11d, %r8d
	andl	$1, %r8d
	leal	(%r8,%rbx,2), %ebx
	shrl	%r11d
	decl	%esi
	incl	%ecx
	jne	.LBB14_7
	jmp	.LBB14_9
.LBB14_8:                               #   in Loop: Header=BB14_4 Depth=2
	xorl	%ebx, %ebx
.LBB14_9:                               # %.prol.loopexit231
                                        #   in Loop: Header=BB14_4 Depth=2
	cmpl	$3, %eax
	jb	.LBB14_11
	.p2align	4, 0x90
.LBB14_10:                              #   Parent Loop BB14_3 Depth=1
                                        #     Parent Loop BB14_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r11d, %ecx
	andl	$1, %ecx
	leal	(%rcx,%rbx,2), %ecx
	movl	%r11d, %ebx
	andl	$2, %ebx
	leal	(%rbx,%rcx,4), %ecx
	movl	%r11d, %ebx
	shrl	$2, %ebx
	andl	$1, %ebx
	orl	%ecx, %ebx
	movl	%r11d, %ecx
	shrl	$3, %ecx
	andl	$1, %ecx
	leal	(%rcx,%rbx,2), %ebx
	shrl	$4, %r11d
	addl	$-4, %esi
	jne	.LBB14_10
.LBB14_11:                              #   in Loop: Header=BB14_4 Depth=2
	movq	-56(%rsp), %rsi         # 8-byte Reload
	addl	%esi, %edi
	movl	-60(%rsp), %r11d        # 4-byte Reload
	movl	%edi, %r8d
	cmpl	%r11d, %edi
	ja	.LBB14_67
# BB#12:                                # %.preheader188.preheader
                                        #   in Loop: Header=BB14_4 Depth=2
	cmpl	$0, -24(%rsp)           # 4-byte Folded Reload
	je	.LBB14_15
# BB#13:                                # %.preheader188.prol.preheader
                                        #   in Loop: Header=BB14_4 Depth=2
	movl	-28(%rsp), %ecx         # 4-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	-64(%rsp), %edi         # 4-byte Reload
	.p2align	4, 0x90
.LBB14_14:                              # %.preheader188.prol
                                        #   Parent Loop BB14_3 Depth=1
                                        #     Parent Loop BB14_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ebx, %eax
	movw	%r13w, (%r9,%rax,2)
	addl	%ebp, %ebx
	decl	%esi
	incl	%ecx
	jne	.LBB14_14
	jmp	.LBB14_16
.LBB14_15:                              #   in Loop: Header=BB14_4 Depth=2
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	-64(%rsp), %edi         # 4-byte Reload
.LBB14_16:                              # %.preheader188.prol.loopexit
                                        #   in Loop: Header=BB14_4 Depth=2
	cmpl	$7, -20(%rsp)           # 4-byte Folded Reload
	jae	.LBB14_18
# BB#17:                                #   in Loop: Header=BB14_4 Depth=2
	movl	%r8d, %ebx
	movl	-44(%rsp), %ecx         # 4-byte Reload
	jmp	.LBB14_21
	.p2align	4, 0x90
.LBB14_18:                              # %.preheader188.preheader.new
                                        #   in Loop: Header=BB14_4 Depth=2
	movl	-32(%rsp), %r11d        # 4-byte Reload
	.p2align	4, 0x90
.LBB14_19:                              # %.preheader188
                                        #   Parent Loop BB14_3 Depth=1
                                        #     Parent Loop BB14_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ebx, %eax
	movw	%r13w, (%r9,%rax,2)
	leal	(%rbx,%rbp), %ecx
	movw	%r13w, (%r9,%rcx,2)
	addl	%ebp, %ecx
	leal	(%r12,%rbx), %eax
	movw	%r13w, (%r9,%rax,2)
	addl	%ebp, %ecx
	leal	(%rdx,%rbx), %eax
	movw	%r13w, (%r9,%rax,2)
	addl	%ebp, %ecx
	leal	(%r15,%rbx), %eax
	movw	%r13w, (%r9,%rax,2)
	addl	%ebp, %ecx
	leal	(%r14,%rbx), %eax
	movw	%r13w, (%r9,%rax,2)
	addl	%ebp, %ecx
	leal	(%r10,%rbx), %eax
	movw	%r13w, (%r9,%rax,2)
	addl	%ebp, %ecx
	addl	%r11d, %ebx
	movw	%r13w, (%r9,%rbx,2)
	addl	%ebp, %ecx
	addl	$-8, %esi
	movl	%ecx, %ebx
	jne	.LBB14_19
# BB#20:                                #   in Loop: Header=BB14_4 Depth=2
	movl	%r8d, %ebx
	movl	-44(%rsp), %ecx         # 4-byte Reload
	movl	-60(%rsp), %r11d        # 4-byte Reload
.LBB14_21:                              # %.loopexit139.us
                                        #   in Loop: Header=BB14_4 Depth=2
	incl	%r13d
	movzwl	%r13w, %eax
	cmpl	%edi, %eax
	jb	.LBB14_4
# BB#22:                                # %._crit_edge177.us
                                        #   in Loop: Header=BB14_3 Depth=1
	movq	-56(%rsp), %rdx         # 8-byte Reload
	shrl	%edx
	incb	%cl
	movzbl	%cl, %ecx
	movq	-8(%rsp), %r13          # 8-byte Reload
	cmpl	%r13d, %ecx
	jbe	.LBB14_3
# BB#23:                                # %._crit_edge184
	xorl	%eax, %eax
	cmpl	%r11d, %ebx
	movq	-16(%rsp), %rbp         # 8-byte Reload
	jne	.LBB14_27
	jmp	.LBB14_68
.LBB14_25:                              # %.preheader140.preheader
	movb	$2, %al
	.p2align	4, 0x90
.LBB14_26:                              # %.preheader140
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%al, %ecx
	movl	%ecx, %eax
	incb	%al
	cmpl	%r13d, %ecx
	jbe	.LBB14_26
.LBB14_27:                              # %._crit_edge184.thread
	movzwl	%bx, %eax
	cmpl	%r11d, %eax
	jae	.LBB14_36
# BB#28:                                # %.preheader138.preheader
	leal	-1(%r13), %r10d
	movl	%r13d, %edx
	andl	$3, %edx
	movl	%edx, %r8d
	negl	%r8d
	movl	%ebx, %r14d
	.p2align	4, 0x90
.LBB14_29:                              # %.preheader138
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_31 Depth 2
                                        #     Child Loop BB14_34 Depth 2
	xorl	%esi, %esi
	testl	%edx, %edx
	je	.LBB14_32
# BB#30:                                # %.prol.preheader225
                                        #   in Loop: Header=BB14_29 Depth=1
	movl	%r8d, %ebx
	movl	%eax, %edi
	movl	%r13d, %ebp
	.p2align	4, 0x90
.LBB14_31:                              #   Parent Loop BB14_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, %ecx
	andl	$1, %ecx
	leal	(%rcx,%rsi,2), %esi
	shrl	%edi
	decl	%ebp
	incl	%ebx
	jne	.LBB14_31
	jmp	.LBB14_33
	.p2align	4, 0x90
.LBB14_32:                              #   in Loop: Header=BB14_29 Depth=1
	movl	%eax, %edi
	movl	%r13d, %ebp
.LBB14_33:                              # %.prol.loopexit226
                                        #   in Loop: Header=BB14_29 Depth=1
	cmpl	$3, %r10d
	movl	%r14d, %ebx
	jb	.LBB14_35
	.p2align	4, 0x90
.LBB14_34:                              #   Parent Loop BB14_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, %ecx
	andl	$1, %ecx
	leal	(%rcx,%rsi,2), %ecx
	movl	%edi, %esi
	andl	$2, %esi
	leal	(%rsi,%rcx,4), %ecx
	movl	%edi, %esi
	shrl	$2, %esi
	andl	$1, %esi
	orl	%ecx, %esi
	movl	%edi, %ecx
	shrl	$3, %ecx
	andl	$1, %ecx
	leal	(%rcx,%rsi,2), %esi
	shrl	$4, %edi
	addl	$-4, %ebp
	jne	.LBB14_34
.LBB14_35:                              #   in Loop: Header=BB14_29 Depth=1
	movl	%esi, %ecx
	movw	$-1, (%r9,%rcx,2)
	incl	%eax
	movzwl	%ax, %eax
	cmpl	%r11d, %eax
	movq	-16(%rsp), %rbp         # 8-byte Reload
	jb	.LBB14_29
.LBB14_36:                              # %._crit_edge172
	shll	$16, %ebx
	shll	$16, %r11d
	leal	1(%r13), %eax
	cmpb	$16, %al
	ja	.LBB14_65
# BB#37:                                # %.preheader137.lr.ph
	movzbl	%al, %r8d
	movl	-64(%rsp), %eax         # 4-byte Reload
	cmpl	%eax, %ebp
	cmovbw	%ax, %bp
	movl	%r8d, %eax
	subl	%r13d, %eax
	leal	-1(%r13), %ecx
	movl	%ecx, -60(%rsp)         # 4-byte Spill
	movl	%r13d, %r12d
	andl	$3, %r12d
	movl	%r12d, %ecx
	negl	%ecx
	movl	%ecx, -56(%rsp)         # 4-byte Spill
	movl	$32768, %r10d           # imm = 0x8000
.LBB14_38:                              # %.preheader137
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_51 Depth 2
                                        #       Child Loop BB14_54 Depth 3
                                        #       Child Loop BB14_57 Depth 3
                                        #       Child Loop BB14_59 Depth 3
                                        #     Child Loop BB14_41 Depth 2
                                        #       Child Loop BB14_44 Depth 3
                                        #       Child Loop BB14_47 Depth 3
	cmpl	$0, -64(%rsp)           # 4-byte Folded Reload
	je	.LBB14_64
# BB#39:                                # %.lr.ph157
                                        #   in Loop: Header=BB14_38 Depth=1
	cmpl	%r13d, %r8d
	jne	.LBB14_50
# BB#40:                                # %.lr.ph157.split.preheader
                                        #   in Loop: Header=BB14_38 Depth=1
	xorl	%ecx, %ecx
	movq	%rbp, %r14
	.p2align	4, 0x90
.LBB14_41:                              # %.lr.ph157.split
                                        #   Parent Loop BB14_38 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB14_44 Depth 3
                                        #       Child Loop BB14_47 Depth 3
	movzwl	%cx, %edx
	movq	-40(%rsp), %rsi         # 8-byte Reload
	movzbl	(%rsi,%rdx), %edx
	cmpl	%r8d, %edx
	jne	.LBB14_49
# BB#42:                                #   in Loop: Header=BB14_41 Depth=2
	movl	%ebx, %edx
	shrl	$16, %edx
	movl	%ebx, %r15d
	xorl	%edi, %edi
	testl	%r12d, %r12d
	je	.LBB14_45
# BB#43:                                # %.prol.preheader219
                                        #   in Loop: Header=BB14_41 Depth=2
	movl	-56(%rsp), %ebx         # 4-byte Reload
	movl	%r13d, %esi
	.p2align	4, 0x90
.LBB14_44:                              #   Parent Loop BB14_38 Depth=1
                                        #     Parent Loop BB14_41 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%edx, %ebp
	andl	$1, %ebp
	leal	(%rbp,%rdi,2), %edi
	shrl	%edx
	decl	%esi
	incl	%ebx
	jne	.LBB14_44
	jmp	.LBB14_46
.LBB14_45:                              #   in Loop: Header=BB14_41 Depth=2
	movl	%r13d, %esi
.LBB14_46:                              # %.prol.loopexit220
                                        #   in Loop: Header=BB14_41 Depth=2
	cmpl	$3, -60(%rsp)           # 4-byte Folded Reload
	jb	.LBB14_48
	.p2align	4, 0x90
.LBB14_47:                              #   Parent Loop BB14_38 Depth=1
                                        #     Parent Loop BB14_41 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%edx, %ebp
	andl	$1, %ebp
	leal	(%rbp,%rdi,2), %edi
	movl	%edx, %ebp
	andl	$2, %ebp
	leal	(%rbp,%rdi,4), %edi
	movl	%edx, %ebp
	shrl	$2, %ebp
	andl	$1, %ebp
	orl	%edi, %ebp
	movl	%edx, %edi
	shrl	$3, %edi
	andl	$1, %edi
	leal	(%rdi,%rbp,2), %edi
	shrl	$4, %edx
	addl	$-4, %esi
	jne	.LBB14_47
.LBB14_48:                              # %.preheader
                                        #   in Loop: Header=BB14_41 Depth=2
	movl	%edi, %edx
	movw	%cx, (%r9,%rdx,2)
	movl	%r15d, %ebx
	addl	%r10d, %ebx
	cmpl	%r11d, %ebx
	movq	%r14, %rbp
	ja	.LBB14_67
.LBB14_49:                              #   in Loop: Header=BB14_41 Depth=2
	incl	%ecx
	movzwl	%cx, %edx
	cmpl	-64(%rsp), %edx         # 4-byte Folded Reload
	jb	.LBB14_41
	jmp	.LBB14_64
	.p2align	4, 0x90
.LBB14_50:                              # %.lr.ph157.split.us.preheader
                                        #   in Loop: Header=BB14_38 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB14_51:                              # %.lr.ph157.split.us
                                        #   Parent Loop BB14_38 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB14_54 Depth 3
                                        #       Child Loop BB14_57 Depth 3
                                        #       Child Loop BB14_59 Depth 3
	movzwl	%dx, %ecx
	movq	-40(%rsp), %rsi         # 8-byte Reload
	movzbl	(%rsi,%rcx), %ecx
	cmpl	%r8d, %ecx
	jne	.LBB14_63
# BB#52:                                #   in Loop: Header=BB14_51 Depth=2
	movl	%ebx, %ecx
	shrl	$16, %ecx
	testl	%r12d, %r12d
	movl	%ebx, %r15d
	movq	%rbp, %r14
	je	.LBB14_55
# BB#53:                                # %.prol.preheader
                                        #   in Loop: Header=BB14_51 Depth=2
	xorl	%edi, %edi
	movl	-56(%rsp), %ebx         # 4-byte Reload
	movl	%r13d, %esi
	.p2align	4, 0x90
.LBB14_54:                              #   Parent Loop BB14_38 Depth=1
                                        #     Parent Loop BB14_51 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ecx, %ebp
	andl	$1, %ebp
	leal	(%rbp,%rdi,2), %edi
	shrl	%ecx
	decl	%esi
	incl	%ebx
	jne	.LBB14_54
	jmp	.LBB14_56
.LBB14_55:                              #   in Loop: Header=BB14_51 Depth=2
	xorl	%edi, %edi
	movl	%r13d, %esi
.LBB14_56:                              # %.prol.loopexit
                                        #   in Loop: Header=BB14_51 Depth=2
	cmpl	$3, -60(%rsp)           # 4-byte Folded Reload
	jb	.LBB14_58
	.p2align	4, 0x90
.LBB14_57:                              #   Parent Loop BB14_38 Depth=1
                                        #     Parent Loop BB14_51 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ecx, %ebp
	andl	$1, %ebp
	leal	(%rbp,%rdi,2), %edi
	movl	%ecx, %ebp
	andl	$2, %ebp
	leal	(%rbp,%rdi,4), %edi
	movl	%ecx, %ebp
	shrl	$2, %ebp
	andl	$1, %ebp
	orl	%edi, %ebp
	movl	%ecx, %edi
	shrl	$3, %edi
	andl	$1, %edi
	leal	(%rdi,%rbp,2), %edi
	shrl	$4, %ecx
	addl	$-4, %esi
	jne	.LBB14_57
.LBB14_58:                              # %.preheader.us
                                        #   in Loop: Header=BB14_51 Depth=2
	movl	%edi, %ecx
	leaq	(%r9,%rcx,2), %rsi
	movl	$15, %ecx
	movl	%r15d, %ebx
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB14_59:                              #   Parent Loop BB14_38 Depth=1
                                        #     Parent Loop BB14_51 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	(%rsi), %edi
	cmpl	$65535, %edi            # imm = 0xFFFF
	jne	.LBB14_61
# BB#60:                                #   in Loop: Header=BB14_59 Depth=3
	movq	%rbp, %rbx
	movzwl	%bx, %ebp
	leal	(%rbp,%rbp), %edi
	movw	$-1, (%r9,%rdi,2)
	leal	1(%rbp,%rbp), %edi
	movw	$-1, (%r9,%rdi,2)
	movw	%bx, %di
	leal	1(%rbx), %ebx
	movw	%bp, (%rsi)
	movw	%bx, %bp
	movl	%r15d, %ebx
.LBB14_61:                              #   in Loop: Header=BB14_59 Depth=3
	movzwl	%di, %esi
	movl	%ebx, %edi
	shrl	%cl, %edi
	andl	$1, %edi
	leal	(%rdi,%rsi,2), %esi
	leaq	(%r9,%rsi,2), %rsi
	leal	-1(%rax,%rcx), %edi
	leal	-1(%rcx), %ecx
	cmpl	$15, %edi
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	jne	.LBB14_59
# BB#62:                                # %._crit_edge.us
                                        #   in Loop: Header=BB14_51 Depth=2
	movw	%dx, (%rsi)
	addl	%r10d, %ebx
	cmpl	%r11d, %ebx
	ja	.LBB14_67
.LBB14_63:                              #   in Loop: Header=BB14_51 Depth=2
	incl	%edx
	movzwl	%dx, %ecx
	cmpl	-64(%rsp), %ecx         # 4-byte Folded Reload
	jb	.LBB14_51
.LBB14_64:                              # %._crit_edge158
                                        #   in Loop: Header=BB14_38 Depth=1
	shrl	%r10d
	incl	%r8d
	incl	%eax
	cmpl	$17, %r8d
	jb	.LBB14_38
.LBB14_65:                              # %._crit_edge169
	xorl	%eax, %eax
	cmpl	%r11d, %ebx
	setne	%al
	jmp	.LBB14_68
.LBB14_67:
	movl	$1, %eax
.LBB14_68:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	mszip_make_decode_table, .Lfunc_end14-mszip_make_decode_table
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"mszip_decompress: inflate error %d\n"
	.size	.L.str, 36

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"mszip_decompress: MSZIP error, %u bytes of data lost\n"
	.size	.L.str.1, 54

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"mszip_decompress: bytes left to output\n"
	.size	.L.str.2, 40

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"lzx_decompress: %d bytes remaining at reset interval\n"
	.size	.L.str.3, 54

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"lzx: failed to build %s table\n"
	.size	.L.str.4, 31

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"ALIGNED"
	.size	.L.str.5, 8

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"MAINTREE"
	.size	.L.str.6, 9

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"LENGTH"
	.size	.L.str.7, 7

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"lzx_decompress: bad block type (0x%x)\n"
	.size	.L.str.8, 39

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"lzx: out of bits in huffman decode\n"
	.size	.L.str.9, 36

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"lzx: index out of table\n"
	.size	.L.str.10, 25

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"lzx_decompress: match ran over window wrap\n"
	.size	.L.str.11, 44

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"lzx_decompress: match offset beyond window boundaries\n"
	.size	.L.str.12, 55

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"lzx_decompress: overrun went past end of block by %d (%d remaining)\n"
	.size	.L.str.13, 69

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"lzx_decompress: decode beyond output frame limits! %d != %d\n"
	.size	.L.str.14, 61

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"lzx_decompress: %d avail bytes, new %d frame\n"
	.size	.L.str.15, 46

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"lzx_decompress: bytes left to output\n"
	.size	.L.str.16, 38

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"qtm_decompress: match offset beyond window boundaries\n"
	.size	.L.str.17, 55

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"qtm_decompress: overshot frame alignment\n"
	.size	.L.str.18, 42

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"mszip_flush_window: overflow: %u bytes flushed, total is now %u\n"
	.size	.L.str.19, 65

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"mszip_read_input: out of input bytes\n"
	.size	.L.str.20, 38

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"zip_inflate: out of bits in huffman decode\n"
	.size	.L.str.21, 44

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"zip_inflate: index out of table\n"
	.size	.L.str.22, 33

	.type	mszip_lit_extrabits,@object # @mszip_lit_extrabits
	.section	.rodata,"a",@progbits
	.p2align	4
mszip_lit_extrabits:
	.asciz	"\000\000\000\000\000\000\000\000\001\001\001\001\002\002\002\002\003\003\003\003\004\004\004\004\005\005\005\005"
	.size	mszip_lit_extrabits, 29

	.type	mszip_bit_mask_tab,@object # @mszip_bit_mask_tab
	.p2align	4
mszip_bit_mask_tab:
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	3                       # 0x3
	.short	7                       # 0x7
	.short	15                      # 0xf
	.short	31                      # 0x1f
	.short	63                      # 0x3f
	.short	127                     # 0x7f
	.short	255                     # 0xff
	.short	511                     # 0x1ff
	.short	1023                    # 0x3ff
	.short	2047                    # 0x7ff
	.short	4095                    # 0xfff
	.short	8191                    # 0x1fff
	.short	16383                   # 0x3fff
	.short	32767                   # 0x7fff
	.short	65535                   # 0xffff
	.size	mszip_bit_mask_tab, 34

	.type	mszip_lit_lengths,@object # @mszip_lit_lengths
	.p2align	4
mszip_lit_lengths:
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	8                       # 0x8
	.short	9                       # 0x9
	.short	10                      # 0xa
	.short	11                      # 0xb
	.short	13                      # 0xd
	.short	15                      # 0xf
	.short	17                      # 0x11
	.short	19                      # 0x13
	.short	23                      # 0x17
	.short	27                      # 0x1b
	.short	31                      # 0x1f
	.short	35                      # 0x23
	.short	43                      # 0x2b
	.short	51                      # 0x33
	.short	59                      # 0x3b
	.short	67                      # 0x43
	.short	83                      # 0x53
	.short	99                      # 0x63
	.short	115                     # 0x73
	.short	131                     # 0x83
	.short	163                     # 0xa3
	.short	195                     # 0xc3
	.short	227                     # 0xe3
	.short	258                     # 0x102
	.size	mszip_lit_lengths, 58

	.type	mszip_dist_extrabits,@object # @mszip_dist_extrabits
	.p2align	4
mszip_dist_extrabits:
	.ascii	"\000\000\000\000\001\001\002\002\003\003\004\004\005\005\006\006\007\007\b\b\t\t\n\n\013\013\f\f\r\r"
	.size	mszip_dist_extrabits, 30

	.type	mszip_dist_offsets,@object # @mszip_dist_offsets
	.p2align	4
mszip_dist_offsets:
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	7                       # 0x7
	.short	9                       # 0x9
	.short	13                      # 0xd
	.short	17                      # 0x11
	.short	25                      # 0x19
	.short	33                      # 0x21
	.short	49                      # 0x31
	.short	65                      # 0x41
	.short	97                      # 0x61
	.short	129                     # 0x81
	.short	193                     # 0xc1
	.short	257                     # 0x101
	.short	385                     # 0x181
	.short	513                     # 0x201
	.short	769                     # 0x301
	.short	1025                    # 0x401
	.short	1537                    # 0x601
	.short	2049                    # 0x801
	.short	3073                    # 0xc01
	.short	4097                    # 0x1001
	.short	6145                    # 0x1801
	.short	8193                    # 0x2001
	.short	12289                   # 0x3001
	.short	16385                   # 0x4001
	.short	24577                   # 0x6001
	.size	mszip_dist_offsets, 60

	.type	mszip_bitlen_order,@object # @mszip_bitlen_order
	.p2align	4
mszip_bitlen_order:
	.ascii	"\020\021\022\000\b\007\t\006\n\005\013\004\f\003\r\002\016\001\017"
	.size	mszip_bitlen_order, 19

	.type	.L.str.23,@object       # @.str.23
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.23:
	.asciz	"zip_read_lens: bad code!: %u\n"
	.size	.L.str.23, 30

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"lzx_read_input: out of input bytes\n"
	.size	.L.str.24, 36

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"PRETREE"
	.size	.L.str.25, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
