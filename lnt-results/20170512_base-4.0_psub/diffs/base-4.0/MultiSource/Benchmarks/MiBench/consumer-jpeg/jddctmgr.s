	.text
	.file	"jddctmgr.bc"
	.globl	jinit_inverse_dct
	.p2align	4, 0x90
	.type	jinit_inverse_dct,@function
jinit_inverse_dct:                      # @jinit_inverse_dct
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$128, %edx
	callq	*(%rax)
	movq	%rax, %r14
	movq	%r14, 584(%r15)
	movq	$start_pass, (%r14)
	cmpl	$0, 48(%r15)
	jle	.LBB0_3
# BB#1:                                 # %.lr.ph
	movq	296(%r15), %r12
	addq	$88, %r12
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$256, %edx              # imm = 0x100
	movq	%r15, %rdi
	callq	*(%rax)
	movq	%rax, (%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movl	$-1, 88(%r14,%rbx,4)
	incq	%rbx
	movslq	48(%r15), %rax
	addq	$96, %r12
	cmpq	%rax, %rbx
	jl	.LBB0_2
.LBB0_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	jinit_inverse_dct, .Lfunc_end0-jinit_inverse_dct
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.zero	8
	.quad	4608925491301736943     # double 1.3870398450000001
.LCPI1_1:
	.quad	4608563055654957002     # double 1.3065629649999999
	.quad	4607974492095648170     # double 1.1758756020000001
.LCPI1_2:
	.zero	8
	.quad	4605252130585427771     # double 0.785694958
.LCPI1_3:
	.quad	4603049880653865147     # double 0.54119609999999996
	.quad	4598641781279730525     # double 0.275899379
.LCPI1_4:
	.quad	2048                    # 0x800
	.quad	2048                    # 0x800
	.text
	.p2align	4, 0x90
	.type	start_pass,@function
start_pass:                             # @start_pass
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 80
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	cmpl	$0, 48(%rdi)
	jle	.LBB1_24
# BB#1:                                 # %.lr.ph
	movq	584(%rdi), %r8
	movq	296(%rdi), %r12
	xorl	%ebx, %ebx
	movapd	.LCPI1_0(%rip), %xmm5   # xmm5 = <u,1.3870398450000001>
	movapd	.LCPI1_1(%rip), %xmm6   # xmm6 = [1.306563e+00,1.175876e+00]
	movapd	.LCPI1_2(%rip), %xmm7   # xmm7 = <u,0.785694958>
	movapd	.LCPI1_3(%rip), %xmm8   # xmm8 = [5.411961e-01,2.758994e-01]
	pxor	%xmm9, %xmm9
	movdqa	.LCPI1_4(%rip), %xmm10  # xmm10 = [2048,2048]
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB1_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_19 Depth 2
                                        #     Child Loop BB1_21 Depth 2
	movq	%r15, %rbp
	movl	%r13d, %r14d
	movl	36(%r12), %eax
	leal	-1(%rax), %ecx
	cmpl	$7, %ecx
	ja	.LBB1_9
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	movl	$jpeg_idct_1x1, %r15d
	xorl	%r13d, %r13d
	jmpq	*.LJTI1_0(,%rcx,8)
.LBB1_4:                                #   in Loop: Header=BB1_2 Depth=1
	movl	$jpeg_idct_2x2, %r15d
	xorl	%r13d, %r13d
	jmp	.LBB1_11
	.p2align	4, 0x90
.LBB1_9:                                #   in Loop: Header=BB1_2 Depth=1
	movq	(%rdi), %rcx
	movl	$6, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	*(%rcx)
	jmp	.LBB1_8
	.p2align	4, 0x90
.LBB1_5:                                #   in Loop: Header=BB1_2 Depth=1
	movl	$jpeg_idct_4x4, %r15d
	xorl	%r13d, %r13d
	jmp	.LBB1_11
	.p2align	4, 0x90
.LBB1_6:                                #   in Loop: Header=BB1_2 Depth=1
	movslq	88(%rdi), %r13
	cmpq	$3, %r13
	jae	.LBB1_7
# BB#10:                                # %switch.lookup
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	.Lswitch.table(,%r13,8), %r15
	jmp	.LBB1_11
.LBB1_7:                                #   in Loop: Header=BB1_2 Depth=1
	movq	(%rdi), %rax
	movl	$47, 40(%rax)
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	*(%rax)
.LBB1_8:                                #   in Loop: Header=BB1_2 Depth=1
	movdqa	.LCPI1_4(%rip), %xmm10  # xmm10 = [2048,2048]
	pxor	%xmm9, %xmm9
	movapd	.LCPI1_3(%rip), %xmm8   # xmm8 = [5.411961e-01,2.758994e-01]
	movapd	.LCPI1_2(%rip), %xmm7   # xmm7 = <u,0.785694958>
	movapd	.LCPI1_1(%rip), %xmm6   # xmm6 = [1.306563e+00,1.175876e+00]
	movapd	.LCPI1_0(%rip), %xmm5   # xmm5 = <u,1.3870398450000001>
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rbp, %r15
	movl	%r14d, %r13d
.LBB1_11:                               #   in Loop: Header=BB1_2 Depth=1
	movq	%r15, 8(%r8,%rbx,8)
	cmpl	$0, 48(%r12)
	je	.LBB1_23
# BB#12:                                #   in Loop: Header=BB1_2 Depth=1
	cmpl	%r13d, 88(%r8,%rbx,4)
	je	.LBB1_23
# BB#13:                                #   in Loop: Header=BB1_2 Depth=1
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.LBB1_23
# BB#14:                                #   in Loop: Header=BB1_2 Depth=1
	movl	%r13d, 88(%r8,%rbx,4)
	cmpl	$2, %r13d
	je	.LBB1_20
# BB#15:                                #   in Loop: Header=BB1_2 Depth=1
	cmpl	$1, %r13d
	je	.LBB1_18
# BB#16:                                #   in Loop: Header=BB1_2 Depth=1
	testl	%r13d, %r13d
	jne	.LBB1_22
# BB#17:                                # %vector.body
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	88(%r12), %rcx
	movq	(%rax), %xmm0           # xmm0 = mem[0],zero
	punpcklwd	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1],xmm0[2],xmm9[2],xmm0[3],xmm9[3]
	movdqu	%xmm0, (%rcx)
	movq	8(%rax), %xmm0          # xmm0 = mem[0],zero
	punpcklwd	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1],xmm0[2],xmm9[2],xmm0[3],xmm9[3]
	movdqu	%xmm0, 16(%rcx)
	movq	16(%rax), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1],xmm0[2],xmm9[2],xmm0[3],xmm9[3]
	movdqu	%xmm0, 32(%rcx)
	movq	24(%rax), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1],xmm0[2],xmm9[2],xmm0[3],xmm9[3]
	movdqu	%xmm0, 48(%rcx)
	movq	32(%rax), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1],xmm0[2],xmm9[2],xmm0[3],xmm9[3]
	movdqu	%xmm0, 64(%rcx)
	movq	40(%rax), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1],xmm0[2],xmm9[2],xmm0[3],xmm9[3]
	movdqu	%xmm0, 80(%rcx)
	movq	48(%rax), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1],xmm0[2],xmm9[2],xmm0[3],xmm9[3]
	movdqu	%xmm0, 96(%rcx)
	movq	56(%rax), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1],xmm0[2],xmm9[2],xmm0[3],xmm9[3]
	movdqu	%xmm0, 112(%rcx)
	movq	64(%rax), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1],xmm0[2],xmm9[2],xmm0[3],xmm9[3]
	movdqu	%xmm0, 128(%rcx)
	movq	72(%rax), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1],xmm0[2],xmm9[2],xmm0[3],xmm9[3]
	movdqu	%xmm0, 144(%rcx)
	movq	80(%rax), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1],xmm0[2],xmm9[2],xmm0[3],xmm9[3]
	movdqu	%xmm0, 160(%rcx)
	movq	88(%rax), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1],xmm0[2],xmm9[2],xmm0[3],xmm9[3]
	movdqu	%xmm0, 176(%rcx)
	movq	96(%rax), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1],xmm0[2],xmm9[2],xmm0[3],xmm9[3]
	movdqu	%xmm0, 192(%rcx)
	movq	104(%rax), %xmm0        # xmm0 = mem[0],zero
	punpcklwd	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1],xmm0[2],xmm9[2],xmm0[3],xmm9[3]
	movdqu	%xmm0, 208(%rcx)
	movq	112(%rax), %xmm0        # xmm0 = mem[0],zero
	punpcklwd	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1],xmm0[2],xmm9[2],xmm0[3],xmm9[3]
	movdqu	%xmm0, 224(%rcx)
	movq	120(%rax), %xmm0        # xmm0 = mem[0],zero
	punpcklwd	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1],xmm0[2],xmm9[2],xmm0[3],xmm9[3]
	movdqu	%xmm0, 240(%rcx)
	jmp	.LBB1_23
.LBB1_20:                               #   in Loop: Header=BB1_2 Depth=1
	movq	88(%r12), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_21:                               # %.preheader
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	start_pass.aanscalefactor(%rdx), %xmm0 # xmm0 = mem[0],zero
	movzwl	(%rax,%rdx,2), %esi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	movzwl	2(%rax,%rdx,2), %esi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%esi, %xmm2
	mulsd	%xmm0, %xmm2
	movzwl	4(%rax,%rdx,2), %esi
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%esi, %xmm3
	mulsd	%xmm0, %xmm3
	movzwl	6(%rax,%rdx,2), %esi
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%esi, %xmm4
	mulsd	%xmm0, %xmm4
	unpcklpd	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0]
	unpcklpd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0]
	movapd	%xmm5, %xmm2
	movsd	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1]
	mulpd	%xmm1, %xmm2
	mulpd	%xmm6, %xmm3
	cvtpd2ps	%xmm3, %xmm1
	cvtpd2ps	%xmm2, %xmm2
	unpcklpd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	movupd	%xmm2, (%rcx,%rdx,4)
	movzwl	8(%rax,%rdx,2), %esi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	movzwl	10(%rax,%rdx,2), %esi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%esi, %xmm2
	mulsd	%xmm0, %xmm2
	movzwl	12(%rax,%rdx,2), %esi
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%esi, %xmm3
	mulsd	%xmm0, %xmm3
	movzwl	14(%rax,%rdx,2), %esi
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%esi, %xmm4
	mulsd	%xmm0, %xmm4
	unpcklpd	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0]
	unpcklpd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0]
	movapd	%xmm7, %xmm2
	movsd	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1]
	mulpd	%xmm1, %xmm2
	mulpd	%xmm8, %xmm3
	cvtpd2ps	%xmm3, %xmm0
	cvtpd2ps	%xmm2, %xmm1
	unpcklpd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movupd	%xmm1, 16(%rcx,%rdx,4)
	addq	$8, %rdx
	cmpq	$64, %rdx
	jne	.LBB1_21
	jmp	.LBB1_23
.LBB1_18:                               # %min.iters.checked122
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	88(%r12), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_19:                               # %vector.body119
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movd	(%rax,%rdx), %xmm0      # xmm0 = mem[0],zero,zero,zero
	punpcklwd	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1],xmm0[2],xmm9[2],xmm0[3],xmm9[3]
	punpckldq	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1]
	movd	start_pass.aanscales(%rdx), %xmm1 # xmm1 = mem[0],zero,zero,zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	movdqa	%xmm1, %xmm2
	psrad	$31, %xmm2
	psrad	$16, %xmm1
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movdqa	%xmm1, %xmm2
	pmuludq	%xmm0, %xmm2
	psrlq	$32, %xmm1
	pmuludq	%xmm0, %xmm1
	psllq	$32, %xmm1
	paddq	%xmm10, %xmm2
	paddq	%xmm1, %xmm2
	psrlq	$12, %xmm2
	pshufd	$232, %xmm2, %xmm0      # xmm0 = xmm2[0,2,2,3]
	movq	%xmm0, (%rcx,%rdx,2)
	movd	4(%rax,%rdx), %xmm0     # xmm0 = mem[0],zero,zero,zero
	punpcklwd	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1],xmm0[2],xmm9[2],xmm0[3],xmm9[3]
	punpckldq	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1]
	movd	start_pass.aanscales+4(%rdx), %xmm1 # xmm1 = mem[0],zero,zero,zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	movdqa	%xmm1, %xmm2
	psrad	$31, %xmm2
	psrad	$16, %xmm1
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movdqa	%xmm1, %xmm2
	pmuludq	%xmm0, %xmm2
	psrlq	$32, %xmm1
	pmuludq	%xmm0, %xmm1
	psllq	$32, %xmm1
	paddq	%xmm10, %xmm2
	paddq	%xmm1, %xmm2
	psrlq	$12, %xmm2
	pshufd	$232, %xmm2, %xmm0      # xmm0 = xmm2[0,2,2,3]
	movq	%xmm0, 8(%rcx,%rdx,2)
	addq	$8, %rdx
	cmpq	$128, %rdx
	jne	.LBB1_19
	jmp	.LBB1_23
.LBB1_22:                               #   in Loop: Header=BB1_2 Depth=1
	movq	(%rdi), %rax
	movl	$47, 40(%rax)
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	*(%rax)
	movdqa	.LCPI1_4(%rip), %xmm10  # xmm10 = [2048,2048]
	pxor	%xmm9, %xmm9
	movapd	.LCPI1_3(%rip), %xmm8   # xmm8 = [5.411961e-01,2.758994e-01]
	movapd	.LCPI1_2(%rip), %xmm7   # xmm7 = <u,0.785694958>
	movapd	.LCPI1_1(%rip), %xmm6   # xmm6 = [1.306563e+00,1.175876e+00]
	movapd	.LCPI1_0(%rip), %xmm5   # xmm5 = <u,1.3870398450000001>
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	.p2align	4, 0x90
.LBB1_23:                               # %.loopexit
                                        #   in Loop: Header=BB1_2 Depth=1
	incq	%rbx
	addq	$96, %r12
	movslq	48(%rdi), %rax
	cmpq	%rax, %rbx
	jl	.LBB1_2
.LBB1_24:                               # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	start_pass, .Lfunc_end1-start_pass
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_11
	.quad	.LBB1_4
	.quad	.LBB1_9
	.quad	.LBB1_5
	.quad	.LBB1_9
	.quad	.LBB1_9
	.quad	.LBB1_9
	.quad	.LBB1_6

	.type	start_pass.aanscales,@object # @start_pass.aanscales
	.p2align	4
start_pass.aanscales:
	.short	16384                   # 0x4000
	.short	22725                   # 0x58c5
	.short	21407                   # 0x539f
	.short	19266                   # 0x4b42
	.short	16384                   # 0x4000
	.short	12873                   # 0x3249
	.short	8867                    # 0x22a3
	.short	4520                    # 0x11a8
	.short	22725                   # 0x58c5
	.short	31521                   # 0x7b21
	.short	29692                   # 0x73fc
	.short	26722                   # 0x6862
	.short	22725                   # 0x58c5
	.short	17855                   # 0x45bf
	.short	12299                   # 0x300b
	.short	6270                    # 0x187e
	.short	21407                   # 0x539f
	.short	29692                   # 0x73fc
	.short	27969                   # 0x6d41
	.short	25172                   # 0x6254
	.short	21407                   # 0x539f
	.short	16819                   # 0x41b3
	.short	11585                   # 0x2d41
	.short	5906                    # 0x1712
	.short	19266                   # 0x4b42
	.short	26722                   # 0x6862
	.short	25172                   # 0x6254
	.short	22654                   # 0x587e
	.short	19266                   # 0x4b42
	.short	15137                   # 0x3b21
	.short	10426                   # 0x28ba
	.short	5315                    # 0x14c3
	.short	16384                   # 0x4000
	.short	22725                   # 0x58c5
	.short	21407                   # 0x539f
	.short	19266                   # 0x4b42
	.short	16384                   # 0x4000
	.short	12873                   # 0x3249
	.short	8867                    # 0x22a3
	.short	4520                    # 0x11a8
	.short	12873                   # 0x3249
	.short	17855                   # 0x45bf
	.short	16819                   # 0x41b3
	.short	15137                   # 0x3b21
	.short	12873                   # 0x3249
	.short	10114                   # 0x2782
	.short	6967                    # 0x1b37
	.short	3552                    # 0xde0
	.short	8867                    # 0x22a3
	.short	12299                   # 0x300b
	.short	11585                   # 0x2d41
	.short	10426                   # 0x28ba
	.short	8867                    # 0x22a3
	.short	6967                    # 0x1b37
	.short	4799                    # 0x12bf
	.short	2446                    # 0x98e
	.short	4520                    # 0x11a8
	.short	6270                    # 0x187e
	.short	5906                    # 0x1712
	.short	5315                    # 0x14c3
	.short	4520                    # 0x11a8
	.short	3552                    # 0xde0
	.short	2446                    # 0x98e
	.short	1247                    # 0x4df
	.size	start_pass.aanscales, 128

	.type	start_pass.aanscalefactor,@object # @start_pass.aanscalefactor
	.p2align	4
start_pass.aanscalefactor:
	.quad	4607182418800017408     # double 1
	.quad	4608925491301736943     # double 1.3870398450000001
	.quad	4608563055654957002     # double 1.3065629649999999
	.quad	4607974492095648170     # double 1.1758756020000001
	.quad	4607182418800017408     # double 1
	.quad	4605252130585427771     # double 0.785694958
	.quad	4603049880653865147     # double 0.54119609999999996
	.quad	4598641781279730525     # double 0.275899379
	.size	start_pass.aanscalefactor, 64

	.type	.Lswitch.table,@object  # @switch.table
	.p2align	4
.Lswitch.table:
	.quad	jpeg_idct_islow
	.quad	jpeg_idct_ifast
	.quad	jpeg_idct_float
	.size	.Lswitch.table, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
