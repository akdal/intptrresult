	.text
	.file	"transform8x8.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4618441417868443648     # double 6
.LCPI0_1:
	.quad	4602677017732795964     # double 0.49990000000000001
	.text
	.globl	Mode_Decision_for_new_Intra8x8Macroblock
	.p2align	4, 0x90
	.type	Mode_Decision_for_new_Intra8x8Macroblock,@function
Mode_Decision_for_new_Intra8x8Macroblock: # @Mode_Decision_for_new_Intra8x8Macroblock
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 48
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movapd	%xmm0, %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	addsd	.LCPI0_1(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	movl	%eax, (%r15)
	leaq	4(%rsp), %r14
	xorl	%edi, %edi
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%r14, %rsi
	callq	Mode_Decision_for_new_8x8IntraBlocks
	xorl	%ebx, %ebx
	testl	%eax, %eax
	setne	%bl
	movl	4(%rsp), %eax
	addl	%eax, (%r15)
	movl	$1, %edi
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%r14, %rsi
	callq	Mode_Decision_for_new_8x8IntraBlocks
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	leal	(%rbx,%rcx,2), %ebx
	movl	4(%rsp), %eax
	addl	%eax, (%r15)
	movl	$2, %edi
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%r14, %rsi
	callq	Mode_Decision_for_new_8x8IntraBlocks
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	leal	(%rbx,%rcx,4), %ebx
	movl	4(%rsp), %eax
	addl	%eax, (%r15)
	movl	$3, %edi
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%r14, %rsi
	callq	Mode_Decision_for_new_8x8IntraBlocks
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	leal	(%rbx,%rcx,8), %eax
	movl	4(%rsp), %ecx
	addl	%ecx, (%r15)
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	Mode_Decision_for_new_Intra8x8Macroblock, .Lfunc_end0-Mode_Decision_for_new_Intra8x8Macroblock
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4616189618054758400     # double 4
.LCPI1_1:
	.quad	5055640609639927018     # double 1.0E+30
	.text
	.globl	Mode_Decision_for_new_8x8IntraBlocks
	.p2align	4, 0x90
	.type	Mode_Decision_for_new_8x8IntraBlocks,@function
Mode_Decision_for_new_8x8IntraBlocks:   # @Mode_Decision_for_new_8x8IntraBlocks
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 56
	subq	$2536, %rsp             # imm = 0x9E8
.Lcfi13:
	.cfi_def_cfa_offset 2592
.Lcfi14:
	.cfi_offset %rbx, -56
.Lcfi15:
	.cfi_offset %r12, -48
.Lcfi16:
	.cfi_offset %r13, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%rsi, 160(%rsp)         # 8-byte Spill
	movsd	%xmm0, 168(%rsp)        # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movl	%edi, %ebx
	andl	$1, %ebx
	leal	(,%rbx,8), %r13d
	movq	%rdi, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	%edi, %ebp
	sarl	%ebp
	leal	(,%rbp,8), %eax
	movq	img(%rip), %rcx
	movslq	176(%rcx), %rdi
	addq	%r13, %rdi
	movslq	180(%rcx), %rsi
	movslq	%eax, %rdx
	addq	%rdx, %rsi
	movslq	192(%rcx), %r12
	movslq	196(%rcx), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rdi, %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%edi, %r14d
	sarl	$31, %r14d
	movq	%rsi, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	%esi, %r15d
	sarl	$31, %r15d
	movq	imgY_org(%rip), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	14224(%rcx), %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movl	12(%rcx), %edi
	leal	-1(,%rbx,8), %esi
	leaq	200(%rsp), %rcx
	movl	%edi, 16(%rsp)          # 4-byte Spill
	movq	%rdx, 32(%rsp)          # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	getLuma4x4Neighbour
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	leal	-1(,%rbp,8), %edx
	leaq	176(%rsp), %rcx
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movl	%r13d, %esi
	callq	getLuma4x4Neighbour
	movq	input(%rip), %rax
	cmpl	$0, 272(%rax)
	movl	176(%rsp), %eax
	je	.LBB1_6
# BB#1:
	xorl	%ecx, %ecx
	testl	%eax, %eax
	movl	$0, %eax
	je	.LBB1_3
# BB#2:
	movq	img(%rip), %rax
	movq	14240(%rax), %rax
	movslq	180(%rsp), %rdx
	movl	(%rax,%rdx,4), %eax
.LBB1_3:
	movl	%eax, 176(%rsp)
	cmpl	$0, 200(%rsp)
	je	.LBB1_5
# BB#4:
	movq	img(%rip), %rcx
	movq	14240(%rcx), %rcx
	movslq	204(%rsp), %rdx
	movl	(%rcx,%rdx,4), %ecx
.LBB1_5:
	movl	%ecx, 200(%rsp)
.LBB1_6:                                # %._crit_edge344
	shrl	$30, %r14d
	shrl	$30, %r15d
	testl	%ebp, %ebp
	je	.LBB1_9
# BB#7:
	testl	%eax, %eax
	je	.LBB1_13
# BB#8:
	movq	img(%rip), %rax
	movq	136(%rax), %rax
	jmp	.LBB1_11
.LBB1_9:
	testl	%eax, %eax
	je	.LBB1_13
# BB#10:
	movq	img(%rip), %rax
	movq	128(%rax), %rax
.LBB1_11:
	movslq	196(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	192(%rsp), %rcx
	movb	(%rax,%rcx), %al
	jmp	.LBB1_14
.LBB1_13:
	movb	$-1, %al
.LBB1_14:
	addl	56(%rsp), %r14d         # 4-byte Folded Reload
	addl	48(%rsp), %r15d         # 4-byte Folded Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	addq	%rcx, 64(%rsp)          # 8-byte Folded Spill
	testl	%ebx, %ebx
	movl	200(%rsp), %ecx
	movq	%rbx, 288(%rsp)         # 8-byte Spill
	movq	%rbp, 280(%rsp)         # 8-byte Spill
	jne	.LBB1_17
# BB#15:
	testl	%ecx, %ecx
	je	.LBB1_23
# BB#16:
	movl	$128, %ecx
	jmp	.LBB1_19
.LBB1_17:
	testl	%ecx, %ecx
	je	.LBB1_23
# BB#18:
	movl	$136, %ecx
.LBB1_19:
	addq	img(%rip), %rcx
	movq	(%rcx), %rcx
	movslq	220(%rsp), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movslq	216(%rsp), %rdx
	movb	(%rcx,%rdx), %cl
	movl	%ecx, %edx
	orb	%al, %dl
	js	.LBB1_23
# BB#20:
	cmpb	%cl, %al
	jle	.LBB1_22
# BB#21:
	movl	%ecx, %eax
.LBB1_22:
	movsbl	%al, %r13d
	jmp	.LBB1_24
.LBB1_23:
	movl	$2, %r13d
.LBB1_24:                               # %.thread
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	16(%rsp), %eax          # 4-byte Reload
	addq	%rbx, %r12
	sarl	$2, %r14d
	movl	%r14d, 124(%rsp)        # 4-byte Spill
	sarl	$2, %r15d
	movl	%r15d, 120(%rsp)        # 4-byte Spill
	cltq
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movq	160(%rsp), %rax         # 8-byte Reload
	movl	$2147483647, (%rax)     # imm = 0x7FFFFFFF
	leaq	152(%rsp), %rdx
	leaq	148(%rsp), %rcx
	leaq	144(%rsp), %r8
	movq	56(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	48(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	intrapred_luma8x8
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	mulsd	168(%rsp), %xmm0        # 8-byte Folded Reload
	movsd	%xmm0, 320(%rsp)        # 8-byte Spill
	movl	%ebx, %edx
	movslq	72(%rsp), %r15          # 4-byte Folded Reload
	movl	148(%rsp), %eax
	movl	%eax, 140(%rsp)         # 4-byte Spill
	movl	152(%rsp), %eax
	movl	%eax, 136(%rsp)         # 4-byte Spill
	movl	144(%rsp), %eax
	movl	%eax, 132(%rsp)         # 4-byte Spill
	movl	%r13d, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %rax
	shlq	$6, %rax
	leaq	1504(%rsp,%rax), %rbp
	movq	%rcx, %rax
	orq	$1, %rax
	movl	%ecx, %esi
	orl	$7, %esi
	movl	%esi, 128(%rsp)         # 4-byte Spill
	movq	%rax, 80(%rsp)          # 8-byte Spill
	shlq	$6, %rax
	leaq	1504(%rsp,%rax), %rbx
	movq	%rcx, %rax
	orq	$3, %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	shlq	$6, %rax
	leaq	1504(%rsp,%rax), %rdi
	movq	%rcx, %rax
	orq	$7, %rax
	movq	%rax, %rsi
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	shlq	$6, %rax
	leaq	1504(%rsp,%rax), %rax
	shlq	$5, %rcx
	movsd	.LCPI1_1(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	pxor	%xmm2, %xmm2
	movq	%rbp, 232(%rsp)         # 8-byte Spill
	leaq	(%rbp,%rdx,4), %rsi
	movq	%rsi, 328(%rsp)         # 8-byte Spill
	movq	%rbx, 224(%rsp)         # 8-byte Spill
	leaq	(%rbx,%rdx,4), %rsi
	movq	%rsi, 312(%rsp)         # 8-byte Spill
	movq	%rdi, 240(%rsp)         # 8-byte Spill
	leaq	(%rdi,%rdx,4), %rsi
	movq	%rsi, 304(%rsp)         # 8-byte Spill
	movq	%rax, 248(%rsp)         # 8-byte Spill
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rcx, 256(%rsp)         # 8-byte Spill
	leaq	12624(%rcx,%rdx,2), %r14
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, 336(%rsp)         # 8-byte Spill
	xorl	%r13d, %r13d
	movl	$0, 24(%rsp)            # 4-byte Folded Spill
	movl	$0, 28(%rsp)            # 4-byte Folded Spill
	movq	%r14, 104(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_25:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_32 Depth 2
	cmpq	$2, %r13
	je	.LBB1_30
# BB#26:                                #   in Loop: Header=BB1_25 Depth=1
	testq	%r13, %r13
	sete	%al
	movl	%r13d, %ecx
	orl	$4, %ecx
	cmpl	$7, %ecx
	sete	%cl
	orb	%al, %cl
	cmpb	$1, %cl
	jne	.LBB1_28
# BB#27:                                #   in Loop: Header=BB1_25 Depth=1
	cmpl	$0, 140(%rsp)           # 4-byte Folded Reload
	jne	.LBB1_30
.LBB1_28:                               #   in Loop: Header=BB1_25 Depth=1
	cmpl	$0, 136(%rsp)           # 4-byte Folded Reload
	setne	%al
	cmpq	$1, %r13
	sete	%cl
	cmpq	$8, %r13
	sete	%dl
	orb	%cl, %dl
	testb	%al, %dl
	jne	.LBB1_30
# BB#29:                                #   in Loop: Header=BB1_25 Depth=1
	cmpl	$0, 132(%rsp)           # 4-byte Folded Reload
	je	.LBB1_45
	.p2align	4, 0x90
.LBB1_30:                               #   in Loop: Header=BB1_25 Depth=1
	movq	input(%rip), %rax
	cmpl	$0, 4168(%rax)
	movq	img(%rip), %r8
	je	.LBB1_39
# BB#31:                                # %.preheader268.preheader
                                        #   in Loop: Header=BB1_25 Depth=1
	movq	%r13, %rcx
	shlq	$7, %rcx
	leaq	7376(%r8,%rcx), %rcx
	leaq	(%r8,%r14), %rdx
	movq	336(%rsp), %rsi         # 8-byte Reload
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_32:                               # %.preheader268
                                        #   Parent Loop BB1_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rcx,%rdi), %xmm0
	movups	%xmm0, (%rdx)
	movq	(%rsi), %rbp
	movzwl	(%rbp,%r12,2), %ebx
	movzwl	(%rcx,%rdi), %eax
	subl	%eax, %ebx
	movl	%ebx, 13136(%r8,%rdi,4)
	movzwl	2(%rbp,%r12,2), %eax
	movzwl	2(%rcx,%rdi), %ebx
	subl	%ebx, %eax
	movl	%eax, 13140(%r8,%rdi,4)
	movzwl	4(%rbp,%r12,2), %eax
	movzwl	4(%rcx,%rdi), %ebx
	subl	%ebx, %eax
	movl	%eax, 13144(%r8,%rdi,4)
	movzwl	6(%rbp,%r12,2), %eax
	movzwl	6(%rcx,%rdi), %ebx
	subl	%ebx, %eax
	movl	%eax, 13148(%r8,%rdi,4)
	movq	8(%rbp,%r12,2), %xmm0   # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	8(%rcx,%rdi), %xmm1     # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqu	%xmm0, 13152(%r8,%rdi,4)
	addq	$16, %rdi
	addq	$32, %rdx
	addq	$8, %rsi
	cmpq	$128, %rdi
	jne	.LBB1_32
# BB#33:                                #   in Loop: Header=BB1_25 Depth=1
	leaq	156(%rsp), %rdi
	movq	72(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r13d, %edx
	movsd	168(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	8(%rsp), %rcx           # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	RDCost_for_8x8IntraBlocks
	movapd	%xmm0, %xmm1
	movq	16(%rsp), %xmm0         # 8-byte Folded Reload
                                        # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB1_38
# BB#34:                                # %.preheader262.preheader
                                        #   in Loop: Header=BB1_25 Depth=1
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movq	cofAC8x8(%rip), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	cofAC8x8(%rip), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	cofAC8x8(%rip), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	cofAC8x8(%rip), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	cofAC8x8(%rip), %rax
	movq	(%rax,%r15,8), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	cofAC8x8(%rip), %rax
	movq	(%rax,%r15,8), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	cofAC8x8(%rip), %rax
	movq	(%rax,%r15,8), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	cofAC8x8(%rip), %rax
	movq	(%rax,%r15,8), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %rax
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	(%rax,%rsi,8), %rcx
	movq	56(%rsp), %rdx          # 8-byte Reload
	movups	(%rcx,%rdx,2), %xmm0
	movaps	%xmm0, 352(%rsp)
	movq	8(%rax,%rsi,8), %rcx
	movups	(%rcx,%rdx,2), %xmm0
	leaq	368(%rsp), %rcx
	movq	%rcx, %rdi
	movups	%xmm0, (%rdi)
	movq	16(%rax,%rsi,8), %rcx
	movups	(%rcx,%rdx,2), %xmm0
	movups	%xmm0, 16(%rdi)
	movq	24(%rax,%rsi,8), %rcx
	movups	(%rcx,%rdx,2), %xmm0
	movups	%xmm0, 32(%rdi)
	movq	32(%rax,%rsi,8), %rcx
	movups	(%rcx,%rdx,2), %xmm0
	movups	%xmm0, 48(%rdi)
	movq	40(%rax,%rsi,8), %rcx
	movups	(%rcx,%rdx,2), %xmm0
	movups	%xmm0, 64(%rdi)
	movq	48(%rax,%rsi,8), %rcx
	movups	(%rcx,%rdx,2), %xmm0
	movups	%xmm0, 80(%rdi)
	movq	56(%rax,%rsi,8), %rax
	movdqu	(%rax,%rdx,2), %xmm0
	movdqu	%xmm0, 96(%rdi)
	movq	img(%rip), %rax
	cmpl	$0, 15260(%rax)
	je	.LBB1_37
# BB#35:                                # %.preheader264
                                        #   in Loop: Header=BB1_25 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	128(%rsp), %ecx         # 4-byte Folded Reload
	movq	14184(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rcx,8), %rcx
	movq	40(%rsp), %rdx          # 8-byte Reload
	movups	16(%rcx,%rdx,4), %xmm0
	movq	328(%rsp), %rsi         # 8-byte Reload
	movups	%xmm0, 16(%rsi)
	movdqu	(%rcx,%rdx,4), %xmm0
	movdqu	%xmm0, (%rsi)
	jge	.LBB1_37
# BB#36:                                #   in Loop: Header=BB1_25 Depth=1
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	(%rax,%rsi,8), %rcx
	movq	40(%rsp), %rdx          # 8-byte Reload
	movups	16(%rcx,%rdx,4), %xmm0
	movq	312(%rsp), %rdi         # 8-byte Reload
	movups	%xmm0, 16(%rdi)
	movups	(%rcx,%rdx,4), %xmm0
	movups	%xmm0, (%rdi)
	movq	8(%rax,%rsi,8), %rcx
	movups	16(%rcx,%rdx,4), %xmm0
	movups	%xmm0, 80(%rdi)
	movups	(%rcx,%rdx,4), %xmm0
	movups	%xmm0, 64(%rdi)
	movq	88(%rsp), %rsi          # 8-byte Reload
	movq	(%rax,%rsi,8), %rcx
	movups	16(%rcx,%rdx,4), %xmm0
	movq	304(%rsp), %rdi         # 8-byte Reload
	movups	%xmm0, 16(%rdi)
	movups	(%rcx,%rdx,4), %xmm0
	movups	%xmm0, (%rdi)
	movq	8(%rax,%rsi,8), %rcx
	movups	16(%rcx,%rdx,4), %xmm0
	movups	%xmm0, 80(%rdi)
	movups	(%rcx,%rdx,4), %xmm0
	movups	%xmm0, 64(%rdi)
	movq	16(%rax,%rsi,8), %rcx
	movups	16(%rcx,%rdx,4), %xmm0
	movups	%xmm0, 144(%rdi)
	movups	(%rcx,%rdx,4), %xmm0
	movups	%xmm0, 128(%rdi)
	movq	24(%rax,%rsi,8), %rcx
	movups	16(%rcx,%rdx,4), %xmm0
	movups	%xmm0, 208(%rdi)
	movups	(%rcx,%rdx,4), %xmm0
	movups	%xmm0, 192(%rdi)
	movq	96(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movups	16(%rax,%rdx,4), %xmm0
	movq	296(%rsp), %rcx         # 8-byte Reload
	movups	%xmm0, 16(%rcx)
	movdqu	(%rax,%rdx,4), %xmm0
	movdqu	%xmm0, (%rcx)
.LBB1_37:                               # %.loopexit265
                                        #   in Loop: Header=BB1_25 Depth=1
	movl	156(%rsp), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movl	%r13d, %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
.LBB1_38:                               #   in Loop: Header=BB1_25 Depth=1
	callq	reset_coding_state_cs_cm
	jmp	.LBB1_44
	.p2align	4, 0x90
.LBB1_39:                               # %.preheader261
                                        #   in Loop: Header=BB1_25 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi,%rax,8), %rdx
	movq	%r13, %rcx
	shlq	$7, %rcx
	movq	(%rdx,%r12,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	7376(%r8,%rcx), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff64(%rip)
	movq	8(%rdx,%r12,2), %xmm0   # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	7384(%r8,%rcx), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff64+16(%rip)
	movq	8(%rsi,%rax,8), %rdx
	movq	(%rdx,%r12,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	7392(%r8,%rcx), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff64+32(%rip)
	movq	8(%rdx,%r12,2), %xmm0   # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	7400(%r8,%rcx), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff64+48(%rip)
	movq	16(%rsi,%rax,8), %rdx
	movq	(%rdx,%r12,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	7408(%r8,%rcx), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff64+64(%rip)
	movq	8(%rdx,%r12,2), %xmm0   # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	7416(%r8,%rcx), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff64+80(%rip)
	movq	24(%rsi,%rax,8), %rdx
	movq	(%rdx,%r12,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	7424(%r8,%rcx), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff64+96(%rip)
	movq	8(%rdx,%r12,2), %xmm0   # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	7432(%r8,%rcx), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff64+112(%rip)
	movq	32(%rsi,%rax,8), %rdx
	movq	(%rdx,%r12,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	7440(%r8,%rcx), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff64+128(%rip)
	movq	8(%rdx,%r12,2), %xmm0   # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	7448(%r8,%rcx), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff64+144(%rip)
	movq	40(%rsi,%rax,8), %rdx
	movq	(%rdx,%r12,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	7456(%r8,%rcx), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff64+160(%rip)
	movq	8(%rdx,%r12,2), %xmm0   # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	7464(%r8,%rcx), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff64+176(%rip)
	movq	48(%rsi,%rax,8), %rdx
	movq	(%rdx,%r12,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	7472(%r8,%rcx), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff64+192(%rip)
	movq	8(%rdx,%r12,2), %xmm0   # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	7480(%r8,%rcx), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff64+208(%rip)
	movq	56(%rsi,%rax,8), %rdx
	movq	(%rdx,%r12,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	7488(%r8,%rcx), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff64+224(%rip)
	movq	8(%rdx,%r12,2), %xmm0   # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	7496(%r8,%rcx), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff64+240(%rip)
	xorl	%r14d, %r14d
	cmpq	8(%rsp), %r13           # 8-byte Folded Reload
	je	.LBB1_41
# BB#40:                                #   in Loop: Header=BB1_25 Depth=1
	movq	320(%rsp), %xmm0        # 8-byte Folded Reload
                                        # xmm0 = mem[0],zero
	callq	floor
	cvttsd2si	%xmm0, %r14d
.LBB1_41:                               #   in Loop: Header=BB1_25 Depth=1
	movl	$diff64, %edi
	callq	distortion8x8
	addl	%r14d, %eax
	movq	160(%rsp), %rcx         # 8-byte Reload
	cmpl	(%rcx), %eax
	jge	.LBB1_43
# BB#42:                                #   in Loop: Header=BB1_25 Depth=1
	movl	%eax, (%rcx)
	movl	%r13d, %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
.LBB1_43:                               #   in Loop: Header=BB1_25 Depth=1
	movq	104(%rsp), %r14         # 8-byte Reload
.LBB1_44:                               #   in Loop: Header=BB1_25 Depth=1
	pxor	%xmm2, %xmm2
.LBB1_45:                               #   in Loop: Header=BB1_25 Depth=1
	incq	%r13
	cmpq	$9, %r13
	jne	.LBB1_25
# BB#46:
	movq	img(%rip), %rax
	movq	136(%rax), %rax
	movslq	120(%rsp), %rcx         # 4-byte Folded Reload
	movq	(%rax,%rcx,8), %rax
	movslq	124(%rsp), %rcx         # 4-byte Folded Reload
	movl	24(%rsp), %r13d         # 4-byte Reload
	movb	%r13b, (%rax,%rcx)
	xorl	%ecx, %ecx
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	%eax, %r13d
	setge	%dl
	cmpl	%r13d, %eax
	movb	$-1, %al
	je	.LBB1_48
# BB#47:
	movb	%dl, %cl
	movl	%r13d, %eax
	subl	%ecx, %eax
.LBB1_48:
	movq	72(%rsp), %rcx          # 8-byte Reload
	leal	(,%rcx,4), %ecx
	movslq	%ecx, %rcx
	imulq	$536, 264(%rsp), %rdx   # 8-byte Folded Reload
                                        # imm = 0x218
	movq	272(%rsp), %rsi         # 8-byte Reload
	addq	%rdx, %rsi
	movb	%al, 348(%rcx,%rsi)
	movq	img(%rip), %rax
	movl	164(%rax), %ecx
	leal	(,%rcx,4), %esi
	movq	280(%rsp), %rdi         # 8-byte Reload
	leal	(%rdi,%rdi), %edx
	leal	(%rdx,%rcx,4), %edx
	leal	2(%rsi,%rdi,2), %ecx
	cmpl	%ecx, %edx
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	288(%rsp), %rbp         # 8-byte Reload
	jge	.LBB1_51
# BB#49:                                # %.lr.ph
	leal	2(%rdi,%rdi), %ecx
	addl	%ebp, %ebp
	movslq	%edx, %rdx
	movzbl	%r13b, %edi
	movl	%edi, %esi
	shll	$8, %esi
	addl	%edi, %esi
	.p2align	4, 0x90
.LBB1_50:                               # =>This Inner Loop Header: Depth=1
	movq	136(%rax), %rdi
	movq	(%rdi,%rdx,8), %rdi
	movl	160(%rax), %eax
	shll	$2, %eax
	orl	%ebp, %eax
	cltq
	movw	%si, (%rdi,%rax)
	incq	%rdx
	movq	img(%rip), %rax
	movl	164(%rax), %edi
	leal	(%rcx,%rdi,4), %edi
	movslq	%edi, %rdi
	cmpq	%rdi, %rdx
	jl	.LBB1_50
.LBB1_51:                               # %._crit_edge
	movq	input(%rip), %rcx
	cmpl	$0, 4168(%rcx)
	je	.LBB1_54
# BB#52:                                # %.preheader260
	movq	14160(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	cofAC8x8(%rip), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	movq	cofAC8x8(%rip), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	movq	cofAC8x8(%rip), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	movq	cofAC8x8(%rip), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rdi
	movq	cofAC8x8(%rip), %rax
	movq	(%rax,%r15,8), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	movq	cofAC8x8(%rip), %rax
	movq	(%rax,%r15,8), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	movq	cofAC8x8(%rip), %rax
	movq	(%rax,%r15,8), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %rdi
	movq	cofAC8x8(%rip), %rax
	movq	(%rax,%r15,8), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	cmpl	$0, 15260(%rax)
	je	.LBB1_57
# BB#53:                                # %.preheader258
	movq	14184(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	232(%rsp), %rcx         # 8-byte Reload
	movups	16(%rcx,%rbx,4), %xmm0
	movups	%xmm0, 16(%rax,%rbx,4)
	movups	(%rcx,%rbx,4), %xmm0
	movups	%xmm0, (%rax,%rbx,4)
	movq	img(%rip), %rax
	movq	14184(%rax), %rax
	movq	8(%rax), %rax
	movq	80(%rsp), %r11          # 8-byte Reload
	movq	(%rax,%r11,8), %rax
	movq	224(%rsp), %rcx         # 8-byte Reload
	movups	16(%rcx,%rbx,4), %xmm0
	movups	%xmm0, 16(%rax,%rbx,4)
	movups	(%rcx,%rbx,4), %xmm0
	movups	%xmm0, (%rax,%rbx,4)
	movq	img(%rip), %rax
	movq	14184(%rax), %rax
	movq	8(%rax), %rax
	movq	%r14, %rcx
	orq	$2, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	%rcx, %rdx
	shlq	$6, %rdx
	leaq	1504(%rsp,%rdx), %rdx
	movups	16(%rdx,%rbx,4), %xmm0
	movups	%xmm0, 16(%rax,%rbx,4)
	movups	(%rdx,%rbx,4), %xmm0
	movups	%xmm0, (%rax,%rbx,4)
	movq	img(%rip), %rax
	movq	14184(%rax), %rax
	movq	8(%rax), %rax
	movq	88(%rsp), %r10          # 8-byte Reload
	movq	(%rax,%r10,8), %rax
	movq	240(%rsp), %rdx         # 8-byte Reload
	movups	16(%rdx,%rbx,4), %xmm0
	movups	%xmm0, 16(%rax,%rbx,4)
	movups	(%rdx,%rbx,4), %xmm0
	movups	%xmm0, (%rax,%rbx,4)
	movq	img(%rip), %rax
	movq	14184(%rax), %rax
	movq	8(%rax), %rdx
	movq	%r14, %r15
	orq	$4, %r15
	movq	(%rdx,%r15,8), %rdx
	movq	%r15, %rsi
	shlq	$6, %rsi
	leaq	1504(%rsp,%rsi), %rsi
	movups	16(%rsi,%rbx,4), %xmm0
	movups	%xmm0, 16(%rdx,%rbx,4)
	movups	(%rsi,%rbx,4), %xmm0
	movups	%xmm0, (%rdx,%rbx,4)
	movq	img(%rip), %rdx
	movq	14184(%rdx), %rdx
	movq	8(%rdx), %rsi
	movq	%r14, %r8
	orq	$5, %r8
	movq	(%rsi,%r8,8), %rsi
	movq	%r8, %rdi
	shlq	$6, %rdi
	leaq	1504(%rsp,%rdi), %rdi
	movups	16(%rdi,%rbx,4), %xmm0
	movups	%xmm0, 16(%rsi,%rbx,4)
	movups	(%rdi,%rbx,4), %xmm0
	movups	%xmm0, (%rsi,%rbx,4)
	movq	img(%rip), %rsi
	movq	14184(%rsi), %rsi
	movq	8(%rsi), %rsi
	orq	$6, %r14
	movq	(%rsi,%r14,8), %rsi
	movq	%r14, %rdi
	shlq	$6, %rdi
	leaq	1504(%rsp,%rdi), %rdi
	movups	16(%rdi,%rbx,4), %xmm0
	movups	%xmm0, 16(%rsi,%rbx,4)
	movups	(%rdi,%rbx,4), %xmm0
	movups	%xmm0, (%rsi,%rbx,4)
	movq	img(%rip), %rsi
	movq	14184(%rsi), %rsi
	movq	8(%rsi), %rsi
	movq	96(%rsp), %r9           # 8-byte Reload
	movq	(%rsi,%r9,8), %rsi
	movq	248(%rsp), %rax         # 8-byte Reload
	movups	16(%rax,%rbx,4), %xmm0
	movups	%xmm0, 16(%rsi,%rbx,4)
	movdqu	(%rax,%rbx,4), %xmm0
	movdqu	%xmm0, (%rsi,%rbx,4)
	jmp	.LBB1_58
.LBB1_54:                               # %.preheader
	movslq	%r13d, %rcx
	shlq	$7, %rcx
	leaq	7376(%rax,%rcx), %r8
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	112(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	104(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_55:                               # =>This Inner Loop Header: Depth=1
	movups	(%r8,%rsi), %xmm1
	movups	%xmm1, (%rax,%rcx)
	movq	(%rdx), %rdi
	movzwl	(%rdi,%r12,2), %ebp
	movzwl	(%r8,%rsi), %ebx
	subl	%ebx, %ebp
	movl	%ebp, 13136(%rax,%rsi,4)
	movzwl	2(%rdi,%r12,2), %ebp
	movzwl	2(%r8,%rsi), %ebx
	subl	%ebx, %ebp
	movl	%ebp, 13140(%rax,%rsi,4)
	movzwl	4(%rdi,%r12,2), %ebp
	movzwl	4(%r8,%rsi), %ebx
	subl	%ebx, %ebp
	movl	%ebp, 13144(%rax,%rsi,4)
	movzwl	6(%rdi,%r12,2), %ebp
	movzwl	6(%r8,%rsi), %ebx
	subl	%ebx, %ebp
	movl	%ebp, 13148(%rax,%rsi,4)
	movq	8(%rdi,%r12,2), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movq	8(%r8,%rsi), %xmm2      # xmm2 = mem[0],zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	psubd	%xmm2, %xmm1
	movdqu	%xmm1, 13152(%rax,%rsi,4)
	addq	$16, %rsi
	addq	$32, %rcx
	addq	$8, %rdx
	cmpq	$128, %rsi
	jne	.LBB1_55
# BB#56:
	leaq	348(%rsp), %rsi
	movl	$1, %edx
	movq	72(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	dct_luma8x8
	jmp	.LBB1_59
.LBB1_57:                               # %.preheader260..preheader257_crit_edge
	movq	%r14, %rcx
	orq	$2, %rcx
	movq	%r14, %r15
	orq	$4, %r15
	movq	%r14, %r8
	orq	$5, %r8
	orq	$6, %r14
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	96(%rsp), %r9           # 8-byte Reload
	movq	88(%rsp), %r10          # 8-byte Reload
	movq	80(%rsp), %r11          # 8-byte Reload
.LBB1_58:                               # %.preheader257
	movslq	%r13d, %rsi
	movq	enc_picture(%rip), %rdi
	movq	6440(%rdi), %rdi
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	(%rdi,%rdx,8), %rdi
	movaps	352(%rsp), %xmm0
	movq	56(%rsp), %rbp          # 8-byte Reload
	movups	%xmm0, (%rdi,%rbp,2)
	movq	img(%rip), %rdi
	movq	256(%rsp), %rax         # 8-byte Reload
	addq	%rdi, %rax
	shlq	$7, %rsi
	movups	7376(%rdi,%rsi), %xmm0
	movups	%xmm0, 12624(%rax,%rbx,2)
	movq	enc_picture(%rip), %rdi
	movq	6440(%rdi), %rdi
	movq	8(%rdi,%rdx,8), %rdi
	movaps	368(%rsp), %xmm0
	movups	%xmm0, (%rdi,%rbp,2)
	movq	img(%rip), %rdi
	shlq	$5, %r11
	addq	%rdi, %r11
	movups	7392(%rdi,%rsi), %xmm0
	movups	%xmm0, 12624(%r11,%rbx,2)
	movq	enc_picture(%rip), %rdi
	movq	6440(%rdi), %rdi
	movq	16(%rdi,%rdx,8), %rdi
	movaps	384(%rsp), %xmm0
	movups	%xmm0, (%rdi,%rbp,2)
	movq	img(%rip), %rdi
	shlq	$5, %rcx
	addq	%rdi, %rcx
	movups	7408(%rdi,%rsi), %xmm0
	movups	%xmm0, 12624(%rcx,%rbx,2)
	movq	enc_picture(%rip), %rcx
	movq	6440(%rcx), %rcx
	movq	24(%rcx,%rdx,8), %rcx
	movaps	400(%rsp), %xmm0
	movups	%xmm0, (%rcx,%rbp,2)
	movq	img(%rip), %rcx
	shlq	$5, %r10
	addq	%rcx, %r10
	movups	7424(%rcx,%rsi), %xmm0
	movups	%xmm0, 12624(%r10,%rbx,2)
	movq	enc_picture(%rip), %rcx
	movq	6440(%rcx), %rcx
	movq	32(%rcx,%rdx,8), %rcx
	movaps	416(%rsp), %xmm0
	movups	%xmm0, (%rcx,%rbp,2)
	movq	img(%rip), %rcx
	shlq	$5, %r15
	addq	%rcx, %r15
	movups	7440(%rcx,%rsi), %xmm0
	movups	%xmm0, 12624(%r15,%rbx,2)
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %rax
	movq	40(%rax,%rdx,8), %rax
	movaps	432(%rsp), %xmm0
	movups	%xmm0, (%rax,%rbp,2)
	movq	img(%rip), %rax
	shlq	$5, %r8
	addq	%rax, %r8
	movups	7456(%rax,%rsi), %xmm0
	movups	%xmm0, 12624(%r8,%rbx,2)
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %rax
	movq	48(%rax,%rdx,8), %rax
	movaps	448(%rsp), %xmm0
	movups	%xmm0, (%rax,%rbp,2)
	movq	img(%rip), %rax
	shlq	$5, %r14
	addq	%rax, %r14
	movups	7472(%rax,%rsi), %xmm0
	movups	%xmm0, 12624(%r14,%rbx,2)
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %rax
	movq	56(%rax,%rdx,8), %rax
	movaps	464(%rsp), %xmm0
	movups	%xmm0, (%rax,%rbp,2)
	movq	img(%rip), %rax
	shlq	$5, %r9
	addq	%rax, %r9
	movdqu	7488(%rax,%rsi), %xmm0
	movdqu	%xmm0, 12624(%r9,%rbx,2)
	movl	28(%rsp), %eax          # 4-byte Reload
.LBB1_59:                               # %.loopexit
	addq	$2536, %rsp             # imm = 0x9E8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	Mode_Decision_for_new_8x8IntraBlocks, .Lfunc_end1-Mode_Decision_for_new_8x8IntraBlocks
	.cfi_endproc

	.globl	intrapred_luma8x8
	.p2align	4, 0x90
	.type	intrapred_luma8x8,@function
intrapred_luma8x8:                      # @intrapred_luma8x8
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi26:
	.cfi_def_cfa_offset 352
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	%esi, %ebp
	movl	%edi, %r14d
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	andl	$15, %r14d
	andl	$15, %ebp
	movq	img(%rip), %rax
	movl	12(%rax), %ebx
	leal	-1(%r14), %r15d
	leaq	80(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%ebx, %edi
	movl	%r15d, %esi
	movl	%ebp, %edx
	callq	*getNeighbour(%rip)
	leal	1(%rbp), %edx
	leaq	104(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%ebx, %edi
	movl	%r15d, %esi
	callq	*getNeighbour(%rip)
	leal	2(%rbp), %edx
	leaq	128(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%ebx, %edi
	movl	%r15d, %esi
	callq	*getNeighbour(%rip)
	leal	3(%rbp), %edx
	leaq	152(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%ebx, %edi
	movl	%r15d, %esi
	callq	*getNeighbour(%rip)
	leal	4(%rbp), %edx
	leaq	176(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%ebx, %edi
	movl	%r15d, %esi
	callq	*getNeighbour(%rip)
	leal	5(%rbp), %edx
	leaq	200(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%ebx, %edi
	movl	%r15d, %esi
	callq	*getNeighbour(%rip)
	leal	6(%rbp), %edx
	leaq	224(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%ebx, %edi
	movl	%r15d, %esi
	callq	*getNeighbour(%rip)
	leal	7(%rbp), %edx
	leaq	248(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%ebx, %edi
	movl	%r15d, %esi
	callq	*getNeighbour(%rip)
	leal	-1(%rbp), %r13d
	leaq	56(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%ebx, %edi
	movl	%r14d, %esi
	movl	%r13d, %edx
	callq	*getNeighbour(%rip)
	leal	8(%r14), %esi
	leaq	32(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%ebx, %edi
	movl	%r13d, %edx
	callq	*getNeighbour(%rip)
	leaq	272(%rsp), %r12
	xorl	%ecx, %ecx
	movl	%ebx, %edi
	movl	%r15d, %esi
	movl	%r13d, %edx
	movq	%r12, %r8
	callq	*getNeighbour(%rip)
	cmpl	$0, 32(%rsp)
	je	.LBB2_1
# BB#2:
	cmpl	$8, %r14d
	setne	%al
	cmpl	$8, %ebp
	setne	%cl
	orb	%al, %cl
	jmp	.LBB2_3
.LBB2_1:
	xorl	%ecx, %ecx
.LBB2_3:
	movzbl	%cl, %r8d
	movl	%r8d, 32(%rsp)
	movq	input(%rip), %rax
	cmpl	$0, 272(%rax)
	je	.LBB2_27
# BB#4:                                 # %.preheader558
	movq	img(%rip), %rax
	xorl	%edi, %edi
	cmpl	$0, 80(%rsp)
	movl	$0, %esi
	je	.LBB2_6
# BB#5:
	movq	14240(%rax), %rdx
	movslq	84(%rsp), %rsi
	movl	(%rdx,%rsi,4), %esi
.LBB2_6:
	cmpl	$0, 104(%rsp)
	je	.LBB2_8
# BB#7:
	movq	14240(%rax), %rdx
	movslq	108(%rsp), %rdi
	movl	(%rdx,%rdi,4), %edi
.LBB2_8:
	xorl	%edx, %edx
	cmpl	$0, 128(%rsp)
	movl	$0, %ebp
	je	.LBB2_10
# BB#9:
	movq	14240(%rax), %rbp
	movslq	132(%rsp), %rbx
	movl	(%rbp,%rbx,4), %ebp
.LBB2_10:
	andl	$1, %esi
	cmpl	$0, 152(%rsp)
	je	.LBB2_12
# BB#11:
	movq	14240(%rax), %rdx
	movslq	156(%rsp), %rbx
	movl	(%rdx,%rbx,4), %edx
.LBB2_12:
	andl	%esi, %edi
	xorl	%esi, %esi
	cmpl	$0, 176(%rsp)
	movl	$0, %ebx
	je	.LBB2_14
# BB#13:
	movq	14240(%rax), %rbx
	movslq	180(%rsp), %rcx
	movl	(%rbx,%rcx,4), %ebx
.LBB2_14:
	andl	%edi, %ebp
	cmpl	$0, 200(%rsp)
	je	.LBB2_16
# BB#15:
	movq	14240(%rax), %rcx
	movslq	204(%rsp), %rsi
	movl	(%rcx,%rsi,4), %esi
.LBB2_16:
	andl	%ebp, %edx
	xorl	%r14d, %r14d
	cmpl	$0, 224(%rsp)
	movl	$0, %edi
	je	.LBB2_18
# BB#17:
	movq	14240(%rax), %rcx
	movslq	228(%rsp), %rdi
	movl	(%rcx,%rdi,4), %edi
.LBB2_18:
	andl	%edx, %ebx
	cmpl	$0, 248(%rsp)
	je	.LBB2_20
# BB#19:
	movq	14240(%rax), %rcx
	movslq	252(%rsp), %rdx
	movl	(%rcx,%rdx,4), %r14d
.LBB2_20:
	andl	%ebx, %esi
	xorl	%edx, %edx
	cmpl	$0, 56(%rsp)
	movl	$0, %r13d
	je	.LBB2_22
# BB#21:
	movq	14240(%rax), %rbp
	movslq	60(%rsp), %rbx
	movl	(%rbp,%rbx,4), %r13d
.LBB2_22:
	andl	%esi, %edi
	testl	%r8d, %r8d
	je	.LBB2_24
# BB#23:
	movq	14240(%rax), %rcx
	movslq	36(%rsp), %rdx
	movl	(%rcx,%rdx,4), %edx
.LBB2_24:
	andl	%edi, %r14d
	cmpl	$0, 272(%rsp)
	movq	(%rsp), %rbp            # 8-byte Reload
	je	.LBB2_25
# BB#26:
	movslq	276(%rsp), %r12
	shlq	$2, %r12
	addq	14240(%rax), %r12
	jmp	.LBB2_28
.LBB2_27:
	movl	80(%rsp), %r14d
	movl	56(%rsp), %r13d
	movl	%r8d, %edx
	movq	(%rsp), %rbp            # 8-byte Reload
.LBB2_28:                               # %.sink.split
	movl	(%r12), %esi
	jmp	.LBB2_29
.LBB2_25:
	xorl	%esi, %esi
.LBB2_29:
	testl	%r13d, %r13d
	setne	%al
	testl	%r14d, %r14d
	setne	%bl
	andb	%al, %bl
	testl	%esi, %esi
	setne	%r15b
	andb	%bl, %r15b
	testl	%r13d, %r13d
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%r14d, (%rax)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%r13d, (%rax)
	movzbl	%r15b, %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	je	.LBB2_31
# BB#30:
	movslq	76(%rsp), %rax
	movq	(%rbp,%rax,8), %rax
	movslq	72(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %edi
	movw	%di, intrapred_luma8x8.PredPel+2(%rip)
	movzwl	2(%rax,%rcx,2), %edi
	movw	%di, intrapred_luma8x8.PredPel+4(%rip)
	movzwl	4(%rax,%rcx,2), %edi
	movw	%di, intrapred_luma8x8.PredPel+6(%rip)
	movzwl	6(%rax,%rcx,2), %edi
	movw	%di, intrapred_luma8x8.PredPel+8(%rip)
	movzwl	8(%rax,%rcx,2), %edi
	movw	%di, intrapred_luma8x8.PredPel+10(%rip)
	movzwl	10(%rax,%rcx,2), %edi
	movw	%di, intrapred_luma8x8.PredPel+12(%rip)
	movzwl	12(%rax,%rcx,2), %edi
	movw	%di, intrapred_luma8x8.PredPel+14(%rip)
	movw	14(%rax,%rcx,2), %ax
	movw	%ax, intrapred_luma8x8.PredPel+16(%rip)
	testl	%edx, %edx
	jne	.LBB2_33
	jmp	.LBB2_34
.LBB2_31:
	movq	img(%rip), %rax
	movl	15512(%rax), %eax
	movd	%eax, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, intrapred_luma8x8.PredPel+2(%rip)
	testl	%edx, %edx
	je	.LBB2_34
.LBB2_33:
	movslq	52(%rsp), %rax
	movq	(%rbp,%rax,8), %rax
	movslq	48(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %edx
	movw	%dx, intrapred_luma8x8.PredPel+18(%rip)
	movzwl	2(%rax,%rcx,2), %edx
	movw	%dx, intrapred_luma8x8.PredPel+20(%rip)
	movzwl	4(%rax,%rcx,2), %edx
	movw	%dx, intrapred_luma8x8.PredPel+22(%rip)
	movzwl	6(%rax,%rcx,2), %edx
	movw	%dx, intrapred_luma8x8.PredPel+24(%rip)
	movzwl	8(%rax,%rcx,2), %edx
	movw	%dx, intrapred_luma8x8.PredPel+26(%rip)
	movzwl	10(%rax,%rcx,2), %edx
	movw	%dx, intrapred_luma8x8.PredPel+28(%rip)
	movzwl	12(%rax,%rcx,2), %edx
	movw	%dx, intrapred_luma8x8.PredPel+30(%rip)
	movzwl	14(%rax,%rcx,2), %eax
	movw	%ax, intrapred_luma8x8.PredPel+32(%rip)
	testl	%r14d, %r14d
	jne	.LBB2_36
	jmp	.LBB2_37
.LBB2_34:
	movd	%eax, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, intrapred_luma8x8.PredPel+18(%rip)
	testl	%r14d, %r14d
	je	.LBB2_37
.LBB2_36:
	movslq	100(%rsp), %rax
	movq	(%rbp,%rax,8), %rax
	movslq	96(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, intrapred_luma8x8.PredPel+34(%rip)
	movslq	124(%rsp), %rax
	movq	(%rbp,%rax,8), %rax
	movslq	120(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, intrapred_luma8x8.PredPel+36(%rip)
	movslq	148(%rsp), %rax
	movq	(%rbp,%rax,8), %rax
	movslq	144(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, intrapred_luma8x8.PredPel+38(%rip)
	movslq	172(%rsp), %rax
	movq	(%rbp,%rax,8), %rax
	movslq	168(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, intrapred_luma8x8.PredPel+40(%rip)
	movslq	196(%rsp), %rax
	movq	(%rbp,%rax,8), %rax
	movslq	192(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, intrapred_luma8x8.PredPel+42(%rip)
	movslq	220(%rsp), %rax
	movq	(%rbp,%rax,8), %rax
	movslq	216(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, intrapred_luma8x8.PredPel+44(%rip)
	movslq	244(%rsp), %rax
	movq	(%rbp,%rax,8), %rax
	movslq	240(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, intrapred_luma8x8.PredPel+46(%rip)
	movslq	268(%rsp), %rax
	movq	(%rbp,%rax,8), %rax
	movslq	264(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, intrapred_luma8x8.PredPel+48(%rip)
	testl	%esi, %esi
	jne	.LBB2_39
	jmp	.LBB2_40
.LBB2_37:
	movq	img(%rip), %rax
	movd	15512(%rax), %xmm0      # xmm0 = mem[0],zero,zero,zero
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, intrapred_luma8x8.PredPel+34(%rip)
	testl	%esi, %esi
	je	.LBB2_40
.LBB2_39:
	movslq	292(%rsp), %rax
	movq	(%rbp,%rax,8), %rax
	movslq	288(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %ecx
	movq	img(%rip), %rax
	jmp	.LBB2_41
.LBB2_40:
	movq	img(%rip), %rax
	movzwl	15512(%rax), %ecx
.LBB2_41:
	movw	%cx, intrapred_luma8x8.PredPel(%rip)
	movw	$-1, 7376(%rax)
	movw	$-1, 7504(%rax)
	movw	$-1, 7632(%rax)
	movw	$-1, 7760(%rax)
	movw	$-1, 7888(%rax)
	movw	$-1, 8016(%rax)
	movw	$-1, 8144(%rax)
	movw	$-1, 8272(%rax)
	movw	$-1, 8400(%rax)
	movl	$intrapred_luma8x8.PredPel, %edi
	movl	%r13d, %edx
	movl	%r14d, %ecx
	callq	LowPassForIntra8x8Pred
	testb	%bl, %bl
	je	.LBB2_43
# BB#42:
	movzwl	intrapred_luma8x8.PredPel+2(%rip), %ecx
	movzwl	intrapred_luma8x8.PredPel+4(%rip), %eax
	addl	%ecx, %eax
	movzwl	intrapred_luma8x8.PredPel+6(%rip), %ecx
	addl	%ecx, %eax
	movzwl	intrapred_luma8x8.PredPel+8(%rip), %ecx
	addl	%ecx, %eax
	movzwl	intrapred_luma8x8.PredPel+10(%rip), %ecx
	addl	%ecx, %eax
	movzwl	intrapred_luma8x8.PredPel+12(%rip), %ecx
	addl	%ecx, %eax
	movzwl	intrapred_luma8x8.PredPel+14(%rip), %ecx
	addl	%ecx, %eax
	movzwl	intrapred_luma8x8.PredPel+16(%rip), %ecx
	addl	%ecx, %eax
	movzwl	intrapred_luma8x8.PredPel+34(%rip), %ecx
	addl	%ecx, %eax
	movzwl	intrapred_luma8x8.PredPel+36(%rip), %ecx
	addl	%ecx, %eax
	movzwl	intrapred_luma8x8.PredPel+38(%rip), %ecx
	addl	%ecx, %eax
	movzwl	intrapred_luma8x8.PredPel+40(%rip), %ecx
	addl	%ecx, %eax
	movzwl	intrapred_luma8x8.PredPel+42(%rip), %ecx
	addl	%ecx, %eax
	movzwl	intrapred_luma8x8.PredPel+44(%rip), %ecx
	addl	%ecx, %eax
	movzwl	intrapred_luma8x8.PredPel+46(%rip), %ecx
	addl	%ecx, %eax
	movzwl	intrapred_luma8x8.PredPel+48(%rip), %ecx
	leal	8(%rcx,%rax), %eax
	sarl	$4, %eax
	jmp	.LBB2_51
.LBB2_43:
	testl	%r13d, %r13d
	jne	.LBB2_47
# BB#44:
	testl	%r14d, %r14d
	je	.LBB2_47
# BB#45:
	movzwl	intrapred_luma8x8.PredPel+34(%rip), %eax
	movzwl	intrapred_luma8x8.PredPel+36(%rip), %ecx
	movzwl	intrapred_luma8x8.PredPel+38(%rip), %edx
	movzwl	intrapred_luma8x8.PredPel+40(%rip), %esi
	movzwl	intrapred_luma8x8.PredPel+42(%rip), %edi
	movzwl	intrapred_luma8x8.PredPel+44(%rip), %ebx
	movzwl	intrapred_luma8x8.PredPel+46(%rip), %r8d
	movzwl	intrapred_luma8x8.PredPel+48(%rip), %r9d
	jmp	.LBB2_46
.LBB2_47:
	testl	%r13d, %r13d
	je	.LBB2_50
# BB#48:
	testl	%r14d, %r14d
	jne	.LBB2_50
# BB#49:
	movzwl	intrapred_luma8x8.PredPel+2(%rip), %eax
	movzwl	intrapred_luma8x8.PredPel+4(%rip), %ecx
	movzwl	intrapred_luma8x8.PredPel+6(%rip), %edx
	movzwl	intrapred_luma8x8.PredPel+8(%rip), %esi
	movzwl	intrapred_luma8x8.PredPel+10(%rip), %edi
	movzwl	intrapred_luma8x8.PredPel+12(%rip), %ebx
	movzwl	intrapred_luma8x8.PredPel+14(%rip), %r8d
	movzwl	intrapred_luma8x8.PredPel+16(%rip), %r9d
.LBB2_46:                               # %.preheader
	addl	%eax, %ecx
	addl	%edx, %ecx
	addl	%esi, %ecx
	addl	%edi, %ecx
	addl	%ebx, %ecx
	addl	%r8d, %ecx
	leal	4(%r9,%rcx), %eax
	sarl	$3, %eax
	jmp	.LBB2_51
.LBB2_50:
	movq	img(%rip), %rax
	movl	15512(%rax), %eax
.LBB2_51:                               # %.preheader
	testl	%r13d, %r13d
	movq	img(%rip), %rcx
	movw	%ax, 7632(%rcx)
	movw	%ax, 7648(%rcx)
	movw	%ax, 7664(%rcx)
	movw	%ax, 7680(%rcx)
	movw	%ax, 7696(%rcx)
	movw	%ax, 7712(%rcx)
	movw	%ax, 7728(%rcx)
	movw	%ax, 7744(%rcx)
	movw	%ax, 7634(%rcx)
	movw	%ax, 7650(%rcx)
	movw	%ax, 7666(%rcx)
	movw	%ax, 7682(%rcx)
	movw	%ax, 7698(%rcx)
	movw	%ax, 7714(%rcx)
	movw	%ax, 7730(%rcx)
	movw	%ax, 7746(%rcx)
	movw	%ax, 7636(%rcx)
	movw	%ax, 7652(%rcx)
	movw	%ax, 7668(%rcx)
	movw	%ax, 7684(%rcx)
	movw	%ax, 7700(%rcx)
	movw	%ax, 7716(%rcx)
	movw	%ax, 7732(%rcx)
	movw	%ax, 7748(%rcx)
	movw	%ax, 7638(%rcx)
	movw	%ax, 7654(%rcx)
	movw	%ax, 7670(%rcx)
	movw	%ax, 7686(%rcx)
	movw	%ax, 7702(%rcx)
	movw	%ax, 7718(%rcx)
	movw	%ax, 7734(%rcx)
	movw	%ax, 7750(%rcx)
	movw	%ax, 7640(%rcx)
	movw	%ax, 7656(%rcx)
	movw	%ax, 7672(%rcx)
	movw	%ax, 7688(%rcx)
	movw	%ax, 7704(%rcx)
	movw	%ax, 7720(%rcx)
	movw	%ax, 7736(%rcx)
	movw	%ax, 7752(%rcx)
	movw	%ax, 7642(%rcx)
	movw	%ax, 7658(%rcx)
	movw	%ax, 7674(%rcx)
	movw	%ax, 7690(%rcx)
	movw	%ax, 7706(%rcx)
	movw	%ax, 7722(%rcx)
	movw	%ax, 7738(%rcx)
	movw	%ax, 7754(%rcx)
	movw	%ax, 7644(%rcx)
	movw	%ax, 7660(%rcx)
	movw	%ax, 7676(%rcx)
	movw	%ax, 7692(%rcx)
	movw	%ax, 7708(%rcx)
	movw	%ax, 7724(%rcx)
	movw	%ax, 7740(%rcx)
	movw	%ax, 7756(%rcx)
	movw	%ax, 7646(%rcx)
	movw	%ax, 7662(%rcx)
	movw	%ax, 7678(%rcx)
	movw	%ax, 7694(%rcx)
	movw	%ax, 7710(%rcx)
	movw	%ax, 7726(%rcx)
	movw	%ax, 7742(%rcx)
	movw	%ax, 7758(%rcx)
	movq	img(%rip), %rax
	movzwl	intrapred_luma8x8.PredPel+2(%rip), %ecx
	movw	%cx, 7488(%rax)
	movw	%cx, 7472(%rax)
	movw	%cx, 7456(%rax)
	movw	%cx, 7440(%rax)
	movw	%cx, 7424(%rax)
	movw	%cx, 7408(%rax)
	movw	%cx, 7392(%rax)
	movw	%cx, 7376(%rax)
	movzwl	intrapred_luma8x8.PredPel+4(%rip), %ecx
	movw	%cx, 7490(%rax)
	movw	%cx, 7474(%rax)
	movw	%cx, 7458(%rax)
	movw	%cx, 7442(%rax)
	movw	%cx, 7426(%rax)
	movw	%cx, 7410(%rax)
	movw	%cx, 7394(%rax)
	movw	%cx, 7378(%rax)
	movzwl	intrapred_luma8x8.PredPel+6(%rip), %ecx
	movw	%cx, 7492(%rax)
	movw	%cx, 7476(%rax)
	movw	%cx, 7460(%rax)
	movw	%cx, 7444(%rax)
	movw	%cx, 7428(%rax)
	movw	%cx, 7412(%rax)
	movw	%cx, 7396(%rax)
	movw	%cx, 7380(%rax)
	movzwl	intrapred_luma8x8.PredPel+8(%rip), %ecx
	movw	%cx, 7494(%rax)
	movw	%cx, 7478(%rax)
	movw	%cx, 7462(%rax)
	movw	%cx, 7446(%rax)
	movw	%cx, 7430(%rax)
	movw	%cx, 7414(%rax)
	movw	%cx, 7398(%rax)
	movw	%cx, 7382(%rax)
	movzwl	intrapred_luma8x8.PredPel+10(%rip), %ecx
	movw	%cx, 7496(%rax)
	movw	%cx, 7480(%rax)
	movw	%cx, 7464(%rax)
	movw	%cx, 7448(%rax)
	movw	%cx, 7432(%rax)
	movw	%cx, 7416(%rax)
	movw	%cx, 7400(%rax)
	movw	%cx, 7384(%rax)
	movzwl	intrapred_luma8x8.PredPel+12(%rip), %ecx
	movw	%cx, 7498(%rax)
	movw	%cx, 7482(%rax)
	movw	%cx, 7466(%rax)
	movw	%cx, 7450(%rax)
	movw	%cx, 7434(%rax)
	movw	%cx, 7418(%rax)
	movw	%cx, 7402(%rax)
	movw	%cx, 7386(%rax)
	movzwl	intrapred_luma8x8.PredPel+14(%rip), %ecx
	movw	%cx, 7500(%rax)
	movw	%cx, 7484(%rax)
	movw	%cx, 7468(%rax)
	movw	%cx, 7452(%rax)
	movw	%cx, 7436(%rax)
	movw	%cx, 7420(%rax)
	movw	%cx, 7404(%rax)
	movw	%cx, 7388(%rax)
	movzwl	intrapred_luma8x8.PredPel+16(%rip), %ecx
	movw	%cx, 7502(%rax)
	movw	%cx, 7486(%rax)
	movw	%cx, 7470(%rax)
	movw	%cx, 7454(%rax)
	movw	%cx, 7438(%rax)
	movw	%cx, 7422(%rax)
	movw	%cx, 7406(%rax)
	movw	%cx, 7390(%rax)
	jne	.LBB2_53
# BB#52:
	movw	$-1, 7376(%rax)
.LBB2_53:
	testl	%r14d, %r14d
	movq	img(%rip), %rax
	movzwl	intrapred_luma8x8.PredPel+34(%rip), %ecx
	movd	%ecx, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, 7504(%rax)
	movd	intrapred_luma8x8.PredPel+36(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, 7520(%rax)
	movzwl	intrapred_luma8x8.PredPel+38(%rip), %ecx
	movd	%ecx, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, 7536(%rax)
	movd	intrapred_luma8x8.PredPel+40(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, 7552(%rax)
	movzwl	intrapred_luma8x8.PredPel+42(%rip), %ecx
	movd	%ecx, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, 7568(%rax)
	movd	intrapred_luma8x8.PredPel+44(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, 7584(%rax)
	movzwl	intrapred_luma8x8.PredPel+46(%rip), %ecx
	movd	%ecx, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, 7600(%rax)
	movd	intrapred_luma8x8.PredPel+48(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, 7616(%rax)
	jne	.LBB2_55
# BB#54:
	movw	$-1, 7504(%rax)
.LBB2_55:
	testl	%r13d, %r13d
	je	.LBB2_57
# BB#56:
	movq	img(%rip), %rax
	movzwl	intrapred_luma8x8.PredPel+2(%rip), %ecx
	movzwl	intrapred_luma8x8.PredPel+6(%rip), %edx
	movzwl	intrapred_luma8x8.PredPel+4(%rip), %esi
	addl	%edx, %ecx
	leal	2(%rcx,%rsi,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 7760(%rax)
	movzwl	intrapred_luma8x8.PredPel+8(%rip), %edi
	addl	%edi, %esi
	leal	2(%rsi,%rdx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 7776(%rax)
	movw	%cx, 7762(%rax)
	movzwl	intrapred_luma8x8.PredPel+10(%rip), %esi
	addl	%esi, %edx
	leal	2(%rdx,%rdi,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 7792(%rax)
	movw	%cx, 7778(%rax)
	movw	%cx, 7764(%rax)
	movzwl	intrapred_luma8x8.PredPel+12(%rip), %ecx
	addl	%ecx, %edi
	leal	2(%rdi,%rsi,2), %edx
	shrl	$2, %edx
	movw	%dx, 7808(%rax)
	movw	%dx, 7794(%rax)
	movw	%dx, 7780(%rax)
	movw	%dx, 7766(%rax)
	movzwl	intrapred_luma8x8.PredPel+14(%rip), %edx
	addl	%edx, %esi
	leal	2(%rsi,%rcx,2), %esi
	shrl	$2, %esi
	movw	%si, 7824(%rax)
	movw	%si, 7810(%rax)
	movw	%si, 7796(%rax)
	movw	%si, 7782(%rax)
	movw	%si, 7768(%rax)
	movzwl	intrapred_luma8x8.PredPel+16(%rip), %esi
	addl	%esi, %ecx
	leal	2(%rcx,%rdx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 7840(%rax)
	movw	%cx, 7826(%rax)
	movw	%cx, 7812(%rax)
	movw	%cx, 7798(%rax)
	movw	%cx, 7784(%rax)
	movw	%cx, 7770(%rax)
	movzwl	intrapred_luma8x8.PredPel+18(%rip), %ecx
	addl	%ecx, %edx
	leal	2(%rdx,%rsi,2), %edx
	shrl	$2, %edx
	movw	%dx, 7856(%rax)
	movw	%dx, 7842(%rax)
	movw	%dx, 7828(%rax)
	movw	%dx, 7814(%rax)
	movw	%dx, 7800(%rax)
	movw	%dx, 7786(%rax)
	movw	%dx, 7772(%rax)
	movzwl	intrapred_luma8x8.PredPel+20(%rip), %edx
	addl	%edx, %esi
	leal	2(%rsi,%rcx,2), %esi
	shrl	$2, %esi
	movw	%si, 7872(%rax)
	movw	%si, 7858(%rax)
	movw	%si, 7844(%rax)
	movw	%si, 7830(%rax)
	movw	%si, 7816(%rax)
	movw	%si, 7802(%rax)
	movw	%si, 7788(%rax)
	movw	%si, 7774(%rax)
	movzwl	intrapred_luma8x8.PredPel+22(%rip), %esi
	addl	%esi, %ecx
	leal	2(%rcx,%rdx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 7874(%rax)
	movw	%cx, 7860(%rax)
	movw	%cx, 7846(%rax)
	movw	%cx, 7832(%rax)
	movw	%cx, 7818(%rax)
	movw	%cx, 7804(%rax)
	movw	%cx, 7790(%rax)
	movzwl	intrapred_luma8x8.PredPel+24(%rip), %ecx
	addl	%ecx, %edx
	leal	2(%rdx,%rsi,2), %edx
	shrl	$2, %edx
	movw	%dx, 7876(%rax)
	movw	%dx, 7862(%rax)
	movw	%dx, 7848(%rax)
	movw	%dx, 7834(%rax)
	movw	%dx, 7820(%rax)
	movw	%dx, 7806(%rax)
	movzwl	intrapred_luma8x8.PredPel+26(%rip), %edx
	addl	%edx, %esi
	leal	2(%rsi,%rcx,2), %esi
	shrl	$2, %esi
	movw	%si, 7878(%rax)
	movw	%si, 7864(%rax)
	movw	%si, 7850(%rax)
	movw	%si, 7836(%rax)
	movw	%si, 7822(%rax)
	movzwl	intrapred_luma8x8.PredPel+28(%rip), %esi
	addl	%esi, %ecx
	leal	2(%rcx,%rdx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 7880(%rax)
	movw	%cx, 7866(%rax)
	movw	%cx, 7852(%rax)
	movw	%cx, 7838(%rax)
	movzwl	intrapred_luma8x8.PredPel+30(%rip), %ecx
	addl	%ecx, %edx
	leal	2(%rdx,%rsi,2), %edx
	shrl	$2, %edx
	movw	%dx, 7882(%rax)
	movw	%dx, 7868(%rax)
	movw	%dx, 7854(%rax)
	movzwl	intrapred_luma8x8.PredPel+32(%rip), %edx
	addl	%edx, %esi
	leal	2(%rsi,%rcx,2), %esi
	shrl	$2, %esi
	movw	%si, 7884(%rax)
	movw	%si, 7870(%rax)
	leal	(%rdx,%rdx,2), %edx
	leal	2(%rcx,%rdx), %ecx
	shrl	$2, %ecx
	movw	%cx, 7886(%rax)
	movq	img(%rip), %rax
	movzwl	intrapred_luma8x8.PredPel+2(%rip), %ecx
	movzwl	intrapred_luma8x8.PredPel+4(%rip), %edx
	leal	1(%rcx,%rdx), %ecx
	shrl	%ecx
	movw	%cx, 8272(%rax)
	movzwl	intrapred_luma8x8.PredPel+6(%rip), %ecx
	leal	1(%rdx,%rcx), %edx
	shrl	%edx
	movw	%dx, 8304(%rax)
	movw	%dx, 8274(%rax)
	movzwl	intrapred_luma8x8.PredPel+8(%rip), %edx
	leal	1(%rcx,%rdx), %ecx
	shrl	%ecx
	movw	%cx, 8336(%rax)
	movw	%cx, 8306(%rax)
	movw	%cx, 8276(%rax)
	movzwl	intrapred_luma8x8.PredPel+10(%rip), %ecx
	leal	1(%rdx,%rcx), %edx
	shrl	%edx
	movw	%dx, 8368(%rax)
	movw	%dx, 8338(%rax)
	movw	%dx, 8308(%rax)
	movw	%dx, 8278(%rax)
	movzwl	intrapred_luma8x8.PredPel+12(%rip), %edx
	leal	1(%rcx,%rdx), %ecx
	shrl	%ecx
	movw	%cx, 8370(%rax)
	movw	%cx, 8340(%rax)
	movw	%cx, 8310(%rax)
	movw	%cx, 8280(%rax)
	movzwl	intrapred_luma8x8.PredPel+14(%rip), %ecx
	leal	1(%rdx,%rcx), %edx
	shrl	%edx
	movw	%dx, 8372(%rax)
	movw	%dx, 8342(%rax)
	movw	%dx, 8312(%rax)
	movw	%dx, 8282(%rax)
	movzwl	intrapred_luma8x8.PredPel+16(%rip), %edx
	leal	1(%rcx,%rdx), %ecx
	shrl	%ecx
	movw	%cx, 8374(%rax)
	movw	%cx, 8344(%rax)
	movw	%cx, 8314(%rax)
	movw	%cx, 8284(%rax)
	movzwl	intrapred_luma8x8.PredPel+18(%rip), %ecx
	leal	1(%rdx,%rcx), %edx
	shrl	%edx
	movw	%dx, 8376(%rax)
	movw	%dx, 8346(%rax)
	movw	%dx, 8316(%rax)
	movw	%dx, 8286(%rax)
	movzwl	intrapred_luma8x8.PredPel+20(%rip), %edx
	leal	1(%rcx,%rdx), %ecx
	shrl	%ecx
	movw	%cx, 8378(%rax)
	movw	%cx, 8348(%rax)
	movw	%cx, 8318(%rax)
	movzwl	intrapred_luma8x8.PredPel+22(%rip), %ecx
	leal	1(%rdx,%rcx), %edx
	shrl	%edx
	movw	%dx, 8380(%rax)
	movw	%dx, 8350(%rax)
	movzwl	intrapred_luma8x8.PredPel+24(%rip), %edx
	leal	1(%rcx,%rdx), %ecx
	shrl	%ecx
	movw	%cx, 8382(%rax)
	movzwl	intrapred_luma8x8.PredPel+2(%rip), %ecx
	movzwl	intrapred_luma8x8.PredPel+6(%rip), %edx
	movzwl	intrapred_luma8x8.PredPel+4(%rip), %esi
	addl	%edx, %ecx
	leal	2(%rcx,%rsi,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8288(%rax)
	movzwl	intrapred_luma8x8.PredPel+8(%rip), %edi
	addl	%edi, %esi
	leal	2(%rsi,%rdx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8320(%rax)
	movw	%cx, 8290(%rax)
	movzwl	intrapred_luma8x8.PredPel+10(%rip), %esi
	addl	%esi, %edx
	leal	2(%rdx,%rdi,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8352(%rax)
	movw	%cx, 8322(%rax)
	movw	%cx, 8292(%rax)
	movzwl	intrapred_luma8x8.PredPel+12(%rip), %ecx
	addl	%ecx, %edi
	leal	2(%rdi,%rsi,2), %edx
	shrl	$2, %edx
	movw	%dx, 8384(%rax)
	movw	%dx, 8354(%rax)
	movw	%dx, 8324(%rax)
	movw	%dx, 8294(%rax)
	movzwl	intrapred_luma8x8.PredPel+14(%rip), %edx
	addl	%edx, %esi
	leal	2(%rsi,%rcx,2), %esi
	shrl	$2, %esi
	movw	%si, 8386(%rax)
	movw	%si, 8356(%rax)
	movw	%si, 8326(%rax)
	movw	%si, 8296(%rax)
	movzwl	intrapred_luma8x8.PredPel+16(%rip), %esi
	addl	%esi, %ecx
	leal	2(%rcx,%rdx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8388(%rax)
	movw	%cx, 8358(%rax)
	movw	%cx, 8328(%rax)
	movw	%cx, 8298(%rax)
	movzwl	intrapred_luma8x8.PredPel+18(%rip), %ecx
	addl	%ecx, %edx
	leal	2(%rdx,%rsi,2), %edx
	shrl	$2, %edx
	movw	%dx, 8390(%rax)
	movw	%dx, 8360(%rax)
	movw	%dx, 8330(%rax)
	movw	%dx, 8300(%rax)
	movzwl	intrapred_luma8x8.PredPel+20(%rip), %edx
	addl	%edx, %esi
	leal	2(%rsi,%rcx,2), %esi
	shrl	$2, %esi
	movw	%si, 8392(%rax)
	movw	%si, 8362(%rax)
	movw	%si, 8332(%rax)
	movw	%si, 8302(%rax)
	movzwl	intrapred_luma8x8.PredPel+22(%rip), %esi
	addl	%esi, %ecx
	leal	2(%rcx,%rdx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8394(%rax)
	movw	%cx, 8364(%rax)
	movw	%cx, 8334(%rax)
	movzwl	intrapred_luma8x8.PredPel+24(%rip), %ecx
	addl	%ecx, %edx
	leal	2(%rdx,%rsi,2), %edx
	shrl	$2, %edx
	movw	%dx, 8396(%rax)
	movw	%dx, 8366(%rax)
	movzwl	intrapred_luma8x8.PredPel+26(%rip), %edx
	addl	%esi, %edx
	leal	2(%rdx,%rcx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8398(%rax)
.LBB2_57:
	testb	%r15b, %r15b
	je	.LBB2_59
# BB#58:
	movq	img(%rip), %rax
	movzwl	intrapred_luma8x8.PredPel+48(%rip), %ecx
	movzwl	intrapred_luma8x8.PredPel+44(%rip), %edx
	movzwl	intrapred_luma8x8.PredPel+46(%rip), %esi
	addl	%edx, %ecx
	leal	2(%rcx,%rsi,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8000(%rax)
	movzwl	intrapred_luma8x8.PredPel+42(%rip), %edi
	addl	%edi, %esi
	leal	2(%rsi,%rdx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8002(%rax)
	movw	%cx, 7984(%rax)
	movzwl	intrapred_luma8x8.PredPel+40(%rip), %esi
	addl	%esi, %edx
	leal	2(%rdx,%rdi,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8004(%rax)
	movw	%cx, 7986(%rax)
	movw	%cx, 7968(%rax)
	movzwl	intrapred_luma8x8.PredPel+38(%rip), %ecx
	addl	%ecx, %edi
	leal	2(%rdi,%rsi,2), %edx
	shrl	$2, %edx
	movw	%dx, 8006(%rax)
	movw	%dx, 7988(%rax)
	movw	%dx, 7970(%rax)
	movw	%dx, 7952(%rax)
	movzwl	intrapred_luma8x8.PredPel+36(%rip), %edx
	addl	%edx, %esi
	leal	2(%rsi,%rcx,2), %esi
	shrl	$2, %esi
	movw	%si, 8008(%rax)
	movw	%si, 7990(%rax)
	movw	%si, 7972(%rax)
	movw	%si, 7954(%rax)
	movw	%si, 7936(%rax)
	movzwl	intrapred_luma8x8.PredPel+34(%rip), %esi
	addl	%esi, %ecx
	leal	2(%rcx,%rdx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8010(%rax)
	movw	%cx, 7992(%rax)
	movw	%cx, 7974(%rax)
	movw	%cx, 7956(%rax)
	movw	%cx, 7938(%rax)
	movw	%cx, 7920(%rax)
	movzwl	intrapred_luma8x8.PredPel(%rip), %ecx
	addl	%ecx, %edx
	leal	2(%rdx,%rsi,2), %edx
	shrl	$2, %edx
	movw	%dx, 8012(%rax)
	movw	%dx, 7994(%rax)
	movw	%dx, 7976(%rax)
	movw	%dx, 7958(%rax)
	movw	%dx, 7940(%rax)
	movw	%dx, 7922(%rax)
	movw	%dx, 7904(%rax)
	movzwl	intrapred_luma8x8.PredPel+2(%rip), %edx
	addl	%edx, %esi
	leal	2(%rsi,%rcx,2), %esi
	shrl	$2, %esi
	movw	%si, 8014(%rax)
	movw	%si, 7996(%rax)
	movw	%si, 7978(%rax)
	movw	%si, 7960(%rax)
	movw	%si, 7942(%rax)
	movw	%si, 7924(%rax)
	movw	%si, 7906(%rax)
	movw	%si, 7888(%rax)
	movzwl	intrapred_luma8x8.PredPel+4(%rip), %esi
	addl	%esi, %ecx
	leal	2(%rcx,%rdx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 7998(%rax)
	movw	%cx, 7980(%rax)
	movw	%cx, 7962(%rax)
	movw	%cx, 7944(%rax)
	movw	%cx, 7926(%rax)
	movw	%cx, 7908(%rax)
	movw	%cx, 7890(%rax)
	movzwl	intrapred_luma8x8.PredPel+6(%rip), %ecx
	addl	%ecx, %edx
	leal	2(%rdx,%rsi,2), %edx
	shrl	$2, %edx
	movw	%dx, 7982(%rax)
	movw	%dx, 7964(%rax)
	movw	%dx, 7946(%rax)
	movw	%dx, 7928(%rax)
	movw	%dx, 7910(%rax)
	movw	%dx, 7892(%rax)
	movzwl	intrapred_luma8x8.PredPel+8(%rip), %edx
	addl	%edx, %esi
	leal	2(%rsi,%rcx,2), %esi
	shrl	$2, %esi
	movw	%si, 7966(%rax)
	movw	%si, 7948(%rax)
	movw	%si, 7930(%rax)
	movw	%si, 7912(%rax)
	movw	%si, 7894(%rax)
	movzwl	intrapred_luma8x8.PredPel+10(%rip), %esi
	addl	%esi, %ecx
	leal	2(%rcx,%rdx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 7950(%rax)
	movw	%cx, 7932(%rax)
	movw	%cx, 7914(%rax)
	movw	%cx, 7896(%rax)
	movzwl	intrapred_luma8x8.PredPel+12(%rip), %ecx
	addl	%ecx, %edx
	leal	2(%rdx,%rsi,2), %edx
	shrl	$2, %edx
	movw	%dx, 7934(%rax)
	movw	%dx, 7916(%rax)
	movw	%dx, 7898(%rax)
	movzwl	intrapred_luma8x8.PredPel+14(%rip), %edx
	addl	%edx, %esi
	leal	2(%rsi,%rcx,2), %esi
	shrl	$2, %esi
	movw	%si, 7918(%rax)
	movw	%si, 7900(%rax)
	movzwl	intrapred_luma8x8.PredPel+16(%rip), %esi
	addl	%ecx, %esi
	leal	2(%rsi,%rdx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 7902(%rax)
	movq	img(%rip), %rax
	movzwl	intrapred_luma8x8.PredPel(%rip), %ecx
	movzwl	intrapred_luma8x8.PredPel+2(%rip), %edx
	leal	1(%rcx,%rdx), %ecx
	shrl	%ecx
	movw	%cx, 8118(%rax)
	movw	%cx, 8084(%rax)
	movw	%cx, 8050(%rax)
	movw	%cx, 8016(%rax)
	movzwl	intrapred_luma8x8.PredPel+4(%rip), %ecx
	leal	1(%rdx,%rcx), %edx
	shrl	%edx
	movw	%dx, 8120(%rax)
	movw	%dx, 8086(%rax)
	movw	%dx, 8052(%rax)
	movw	%dx, 8018(%rax)
	movzwl	intrapred_luma8x8.PredPel+6(%rip), %edx
	leal	1(%rcx,%rdx), %ecx
	shrl	%ecx
	movw	%cx, 8122(%rax)
	movw	%cx, 8088(%rax)
	movw	%cx, 8054(%rax)
	movw	%cx, 8020(%rax)
	movzwl	intrapred_luma8x8.PredPel+8(%rip), %ecx
	leal	1(%rdx,%rcx), %edx
	shrl	%edx
	movw	%dx, 8124(%rax)
	movw	%dx, 8090(%rax)
	movw	%dx, 8056(%rax)
	movw	%dx, 8022(%rax)
	movzwl	intrapred_luma8x8.PredPel+10(%rip), %edx
	leal	1(%rcx,%rdx), %ecx
	shrl	%ecx
	movw	%cx, 8126(%rax)
	movw	%cx, 8092(%rax)
	movw	%cx, 8058(%rax)
	movw	%cx, 8024(%rax)
	movzwl	intrapred_luma8x8.PredPel+12(%rip), %ecx
	leal	1(%rdx,%rcx), %edx
	shrl	%edx
	movw	%dx, 8094(%rax)
	movw	%dx, 8060(%rax)
	movw	%dx, 8026(%rax)
	movzwl	intrapred_luma8x8.PredPel+14(%rip), %edx
	leal	1(%rcx,%rdx), %ecx
	shrl	%ecx
	movw	%cx, 8062(%rax)
	movw	%cx, 8028(%rax)
	movzwl	intrapred_luma8x8.PredPel+16(%rip), %ecx
	leal	1(%rdx,%rcx), %ecx
	shrl	%ecx
	movw	%cx, 8030(%rax)
	movzwl	intrapred_luma8x8.PredPel+34(%rip), %edx
	movzwl	intrapred_luma8x8.PredPel+2(%rip), %ecx
	movzwl	intrapred_luma8x8.PredPel(%rip), %esi
	addl	%ecx, %edx
	leal	2(%rdx,%rsi,2), %edx
	shrl	$2, %edx
	movw	%dx, 8134(%rax)
	movw	%dx, 8100(%rax)
	movw	%dx, 8066(%rax)
	movw	%dx, 8032(%rax)
	movzwl	intrapred_luma8x8.PredPel+4(%rip), %edx
	addl	%edx, %esi
	leal	2(%rsi,%rcx,2), %esi
	shrl	$2, %esi
	movw	%si, 8136(%rax)
	movw	%si, 8102(%rax)
	movw	%si, 8068(%rax)
	movw	%si, 8034(%rax)
	movzwl	intrapred_luma8x8.PredPel+6(%rip), %esi
	addl	%esi, %ecx
	leal	2(%rcx,%rdx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8138(%rax)
	movw	%cx, 8104(%rax)
	movw	%cx, 8070(%rax)
	movw	%cx, 8036(%rax)
	movzwl	intrapred_luma8x8.PredPel+8(%rip), %ecx
	addl	%ecx, %edx
	leal	2(%rdx,%rsi,2), %edx
	shrl	$2, %edx
	movw	%dx, 8140(%rax)
	movw	%dx, 8106(%rax)
	movw	%dx, 8072(%rax)
	movw	%dx, 8038(%rax)
	movzwl	intrapred_luma8x8.PredPel+10(%rip), %edx
	addl	%edx, %esi
	leal	2(%rsi,%rcx,2), %esi
	shrl	$2, %esi
	movw	%si, 8142(%rax)
	movw	%si, 8108(%rax)
	movw	%si, 8074(%rax)
	movw	%si, 8040(%rax)
	movzwl	intrapred_luma8x8.PredPel+12(%rip), %esi
	addl	%esi, %ecx
	leal	2(%rcx,%rdx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8110(%rax)
	movw	%cx, 8076(%rax)
	movw	%cx, 8042(%rax)
	movzwl	intrapred_luma8x8.PredPel+14(%rip), %ecx
	addl	%ecx, %edx
	leal	2(%rdx,%rsi,2), %edx
	shrl	$2, %edx
	movw	%dx, 8078(%rax)
	movw	%dx, 8044(%rax)
	movzwl	intrapred_luma8x8.PredPel+16(%rip), %edx
	addl	%esi, %edx
	leal	2(%rdx,%rcx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8046(%rax)
	movzwl	intrapred_luma8x8.PredPel+36(%rip), %edx
	movzwl	intrapred_luma8x8.PredPel(%rip), %edi
	movzwl	intrapred_luma8x8.PredPel+34(%rip), %esi
	leal	(%rdx,%rdi), %ecx
	leal	2(%rcx,%rsi,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8116(%rax)
	movw	%cx, 8082(%rax)
	movw	%cx, 8048(%rax)
	movzwl	intrapred_luma8x8.PredPel+38(%rip), %r8d
	leal	1(%rsi,%rdi), %edi
	leal	1(%rdx,%rsi), %ebp
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	addl	%r8d, %esi
	leal	2(%rsi,%rdx,2), %esi
	shrl	$2, %esi
	movw	%si, 8132(%rax)
	movw	%si, 8098(%rax)
	movw	%si, 8064(%rax)
	movzwl	intrapred_luma8x8.PredPel+40(%rip), %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	addl	%esi, %edx
	leal	2(%rdx,%r8,2), %edx
	shrl	$2, %edx
	movw	%dx, 8114(%rax)
	movw	%dx, 8080(%rax)
	movzwl	intrapred_luma8x8.PredPel+42(%rip), %ebx
	movl	%r8d, %edx
	addl	%ebx, %edx
	leal	2(%rdx,%rsi,2), %edx
	shrl	$2, %edx
	movw	%dx, 8130(%rax)
	movw	%dx, 8096(%rax)
	movzwl	intrapred_luma8x8.PredPel+44(%rip), %r9d
	leal	1(%rsi,%r8), %ecx
	leal	1(%rbx,%rsi), %edx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	addl	%r9d, %esi
	leal	2(%rsi,%rbx,2), %esi
	shrl	$2, %esi
	movw	%si, 8112(%rax)
	movzwl	intrapred_luma8x8.PredPel+46(%rip), %r10d
	leal	1(%r9,%rbx), %esi
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	addl	%r10d, %ebx
	leal	2(%rbx,%r9,2), %ebx
	shrl	$2, %ebx
	movw	%bx, 8128(%rax)
	movq	img(%rip), %rax
	shrl	%edi
	movw	%di, 8204(%rax)
	movw	%di, 8184(%rax)
	movw	%di, 8164(%rax)
	movw	%di, 8144(%rax)
	shrl	%ebp
	movw	%bp, 8220(%rax)
	movw	%bp, 8200(%rax)
	movw	%bp, 8180(%rax)
	movw	%bp, 8160(%rax)
	movzwl	intrapred_luma8x8.PredPel+36(%rip), %edi
	leal	1(%r8,%rdi), %ebp
	shrl	%ebp
	movw	%bp, 8236(%rax)
	movw	%bp, 8216(%rax)
	movw	%bp, 8196(%rax)
	movw	%bp, 8176(%rax)
	shrl	%ecx
	movw	%cx, 8252(%rax)
	movw	%cx, 8232(%rax)
	movw	%cx, 8212(%rax)
	movw	%cx, 8192(%rax)
	shrl	%edx
	movw	%dx, 8268(%rax)
	movw	%dx, 8248(%rax)
	movw	%dx, 8228(%rax)
	movw	%dx, 8208(%rax)
	shrl	%esi
	movw	%si, 8264(%rax)
	movw	%si, 8244(%rax)
	movw	%si, 8224(%rax)
	leal	1(%r10,%r9), %ecx
	shrl	%ecx
	movw	%cx, 8260(%rax)
	movw	%cx, 8240(%rax)
	movzwl	intrapred_luma8x8.PredPel+48(%rip), %ecx
	leal	1(%rcx,%r10), %ecx
	shrl	%ecx
	movw	%cx, 8256(%rax)
	movzwl	intrapred_luma8x8.PredPel+34(%rip), %edx
	movzwl	intrapred_luma8x8.PredPel+2(%rip), %ecx
	movzwl	intrapred_luma8x8.PredPel(%rip), %esi
	leal	2(%rdx,%rcx), %ecx
	leal	(%rcx,%rsi,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8206(%rax)
	movw	%cx, 8186(%rax)
	movw	%cx, 8166(%rax)
	movw	%cx, 8146(%rax)
	addl	%edi, %esi
	leal	2(%rsi,%rdx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8222(%rax)
	movw	%cx, 8202(%rax)
	movw	%cx, 8182(%rax)
	movw	%cx, 8162(%rax)
	movzwl	intrapred_luma8x8.PredPel+38(%rip), %ecx
	movzwl	intrapred_luma8x8.PredPel+36(%rip), %esi
	leal	2(%rdx,%rcx), %edx
	leal	(%rdx,%rsi,2), %edx
	shrl	$2, %edx
	movw	%dx, 8238(%rax)
	movw	%dx, 8218(%rax)
	movw	%dx, 8198(%rax)
	movw	%dx, 8178(%rax)
	movzwl	intrapred_luma8x8.PredPel+40(%rip), %edx
	addl	%edx, %esi
	leal	2(%rsi,%rcx,2), %esi
	shrl	$2, %esi
	movw	%si, 8254(%rax)
	movw	%si, 8234(%rax)
	movw	%si, 8214(%rax)
	movw	%si, 8194(%rax)
	movzwl	intrapred_luma8x8.PredPel+42(%rip), %esi
	addl	%esi, %ecx
	leal	2(%rcx,%rdx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8270(%rax)
	movw	%cx, 8250(%rax)
	movw	%cx, 8230(%rax)
	movw	%cx, 8210(%rax)
	movzwl	intrapred_luma8x8.PredPel+44(%rip), %ecx
	addl	%ecx, %edx
	leal	2(%rdx,%rsi,2), %edx
	shrl	$2, %edx
	movw	%dx, 8266(%rax)
	movw	%dx, 8246(%rax)
	movw	%dx, 8226(%rax)
	movzwl	intrapred_luma8x8.PredPel+46(%rip), %edx
	addl	%edx, %esi
	leal	2(%rsi,%rcx,2), %esi
	shrl	$2, %esi
	movw	%si, 8262(%rax)
	movw	%si, 8242(%rax)
	movzwl	intrapred_luma8x8.PredPel+48(%rip), %esi
	addl	%ecx, %esi
	leal	2(%rsi,%rdx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8258(%rax)
	movzwl	intrapred_luma8x8.PredPel(%rip), %ecx
	movzwl	intrapred_luma8x8.PredPel+4(%rip), %edx
	movzwl	intrapred_luma8x8.PredPel+2(%rip), %esi
	addl	%edx, %ecx
	leal	2(%rcx,%rsi,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8188(%rax)
	movw	%cx, 8168(%rax)
	movw	%cx, 8148(%rax)
	movzwl	intrapred_luma8x8.PredPel+6(%rip), %ecx
	addl	%ecx, %esi
	leal	2(%rsi,%rdx,2), %esi
	shrl	$2, %esi
	movw	%si, 8190(%rax)
	movw	%si, 8170(%rax)
	movw	%si, 8150(%rax)
	movzwl	intrapred_luma8x8.PredPel+8(%rip), %esi
	addl	%esi, %edx
	leal	2(%rdx,%rcx,2), %edx
	shrl	$2, %edx
	movw	%dx, 8172(%rax)
	movw	%dx, 8152(%rax)
	movzwl	intrapred_luma8x8.PredPel+10(%rip), %edx
	addl	%edx, %ecx
	leal	2(%rcx,%rsi,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8174(%rax)
	movw	%cx, 8154(%rax)
	movzwl	intrapred_luma8x8.PredPel+12(%rip), %ecx
	addl	%ecx, %esi
	leal	2(%rsi,%rdx,2), %esi
	shrl	$2, %esi
	movw	%si, 8156(%rax)
	movzwl	intrapred_luma8x8.PredPel+14(%rip), %esi
	addl	%edx, %esi
	leal	2(%rsi,%rcx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8158(%rax)
.LBB2_59:
	testl	%r14d, %r14d
	je	.LBB2_61
# BB#60:
	movq	img(%rip), %rax
	movzwl	intrapred_luma8x8.PredPel+34(%rip), %ecx
	movzwl	intrapred_luma8x8.PredPel+36(%rip), %edx
	leal	1(%rcx,%rdx), %ecx
	shrl	%ecx
	movw	%cx, 8400(%rax)
	movzwl	intrapred_luma8x8.PredPel+38(%rip), %ecx
	leal	1(%rdx,%rcx), %edx
	shrl	%edx
	movw	%dx, 8404(%rax)
	movw	%dx, 8416(%rax)
	movzwl	intrapred_luma8x8.PredPel+40(%rip), %edx
	leal	1(%rcx,%rdx), %ecx
	shrl	%ecx
	movw	%cx, 8408(%rax)
	movw	%cx, 8420(%rax)
	movw	%cx, 8432(%rax)
	movzwl	intrapred_luma8x8.PredPel+42(%rip), %ecx
	leal	1(%rdx,%rcx), %edx
	shrl	%edx
	movw	%dx, 8412(%rax)
	movw	%dx, 8424(%rax)
	movw	%dx, 8436(%rax)
	movw	%dx, 8448(%rax)
	movzwl	intrapred_luma8x8.PredPel+44(%rip), %edx
	leal	1(%rcx,%rdx), %ecx
	shrl	%ecx
	movw	%cx, 8428(%rax)
	movw	%cx, 8440(%rax)
	movw	%cx, 8452(%rax)
	movw	%cx, 8464(%rax)
	movzwl	intrapred_luma8x8.PredPel+46(%rip), %ecx
	leal	1(%rdx,%rcx), %esi
	shrl	%esi
	movw	%si, 8444(%rax)
	movw	%si, 8456(%rax)
	movw	%si, 8468(%rax)
	movw	%si, 8480(%rax)
	movzwl	intrapred_luma8x8.PredPel+48(%rip), %esi
	leal	1(%rcx,%rsi), %edi
	shrl	%edi
	movw	%di, 8460(%rax)
	movw	%di, 8472(%rax)
	movw	%di, 8484(%rax)
	movw	%di, 8496(%rax)
	movw	%si, 8526(%rax)
	movw	%si, 8524(%rax)
	movd	%esi, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, 8508(%rax)
	movw	%si, 8506(%rax)
	movw	%si, 8504(%rax)
	movw	%si, 8502(%rax)
	movw	%si, 8500(%rax)
	movw	%si, 8494(%rax)
	movw	%si, 8492(%rax)
	movw	%si, 8490(%rax)
	movw	%si, 8488(%rax)
	movw	%si, 8478(%rax)
	movw	%si, 8476(%rax)
	leal	(%rsi,%rsi,2), %edi
	leal	2(%rcx,%rdi), %edi
	shrl	$2, %edi
	movw	%di, 8462(%rax)
	movw	%di, 8474(%rax)
	movw	%di, 8486(%rax)
	movw	%di, 8498(%rax)
	addl	%edx, %esi
	leal	2(%rsi,%rcx,2), %edx
	shrl	$2, %edx
	movw	%dx, 8446(%rax)
	movw	%dx, 8458(%rax)
	movw	%dx, 8470(%rax)
	movw	%dx, 8482(%rax)
	movzwl	intrapred_luma8x8.PredPel+42(%rip), %edx
	movzwl	intrapred_luma8x8.PredPel+44(%rip), %esi
	leal	2(%rcx,%rdx), %ecx
	leal	(%rcx,%rsi,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8430(%rax)
	movw	%cx, 8442(%rax)
	movw	%cx, 8454(%rax)
	movw	%cx, 8466(%rax)
	movzwl	intrapred_luma8x8.PredPel+40(%rip), %ecx
	addl	%ecx, %esi
	leal	2(%rsi,%rdx,2), %esi
	shrl	$2, %esi
	movw	%si, 8414(%rax)
	movw	%si, 8426(%rax)
	movw	%si, 8438(%rax)
	movw	%si, 8450(%rax)
	movzwl	intrapred_luma8x8.PredPel+38(%rip), %esi
	addl	%esi, %edx
	leal	2(%rdx,%rcx,2), %edx
	shrl	$2, %edx
	movw	%dx, 8410(%rax)
	movw	%dx, 8422(%rax)
	movw	%dx, 8434(%rax)
	movzwl	intrapred_luma8x8.PredPel+36(%rip), %edx
	addl	%edx, %ecx
	leal	2(%rcx,%rsi,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8406(%rax)
	movw	%cx, 8418(%rax)
	movzwl	intrapred_luma8x8.PredPel+34(%rip), %ecx
	addl	%esi, %ecx
	leal	2(%rcx,%rdx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 8402(%rax)
.LBB2_61:
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	intrapred_luma8x8, .Lfunc_end2-intrapred_luma8x8
	.cfi_endproc

	.globl	RDCost_for_8x8IntraBlocks
	.p2align	4, 0x90
	.type	RDCost_for_8x8IntraBlocks,@function
RDCost_for_8x8IntraBlocks:              # @RDCost_for_8x8IntraBlocks
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 160
.Lcfi40:
	.cfi_offset %rbx, -56
.Lcfi41:
	.cfi_offset %r12, -48
.Lcfi42:
	.cfi_offset %r13, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movl	%esi, %ebp
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	leal	(,%rbp,8), %r13d
	andl	$8, %r13d
	leal	(,%rbp,4), %eax
	andl	$-8, %eax
	movq	img(%rip), %rcx
	movslq	176(%rcx), %rdx
	addq	%rdx, %r13
	movslq	180(%rcx), %rbx
	cltq
	addq	%rax, %rbx
	movslq	196(%rcx), %r14
	addq	%rax, %r14
	movq	imgY_org(%rip), %r15
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %r12
	movq	14216(%rcx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	input(%rip), %rax
	movslq	4016(%rax), %rax
	movq	assignSE2partition(,%rax,8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$0, 20(%rsp)
	leaq	20(%rsp), %rsi
	movl	$1, %edx
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movl	%ebp, %edi
	callq	dct_luma8x8
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movq	img(%rip), %r8
	movq	14232(%r8), %rcx
	leaq	(%r15,%r14,8), %r9
	leaq	(%r12,%rbx,8), %r10
	xorl	%edi, %edi
	xorpd	%xmm0, %xmm0
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	movq	(%r9,%rdi,8), %rax
	movq	(%r10,%rdi,8), %rbp
	movdqu	(%rax,%r13,2), %xmm1
	movdqa	%xmm1, %xmm2
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	punpckhwd	%xmm0, %xmm1    # xmm1 = xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	movdqu	(%rbp,%r13,2), %xmm3
	movdqa	%xmm3, %xmm4
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	punpckhwd	%xmm0, %xmm3    # xmm3 = xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	psubd	%xmm3, %xmm1
	pshufd	$78, %xmm1, %xmm3       # xmm3 = xmm1[2,3,0,1]
	movd	%xmm3, %rbp
	movd	%xmm1, %rax
	psubd	%xmm4, %xmm2
	pshufd	$78, %xmm2, %xmm1       # xmm1 = xmm2[2,3,0,1]
	movd	%xmm1, %rdx
	movd	%xmm2, %rsi
	movslq	%esi, %rbx
	movslq	(%rcx,%rbx,4), %rbx
	addq	%r15, %rbx
	sarq	$32, %rsi
	movslq	(%rcx,%rsi,4), %rsi
	addq	%rbx, %rsi
	movslq	%edx, %rbx
	movslq	(%rcx,%rbx,4), %rbx
	addq	%rsi, %rbx
	sarq	$32, %rdx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rbx, %rdx
	movslq	%eax, %rsi
	movslq	(%rcx,%rsi,4), %rsi
	addq	%rdx, %rsi
	sarq	$32, %rax
	movslq	(%rcx,%rax,4), %rax
	addq	%rsi, %rax
	movslq	%ebp, %rdx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rax, %rdx
	sarq	$32, %rbp
	movslq	(%rcx,%rbp,4), %r15
	addq	%rdx, %r15
	incq	%rdi
	cmpq	$8, %rdi
	jne	.LBB3_1
# BB#2:
	xorl	%eax, %eax
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
	cmpl	%edx, %esi
	setge	%al
	movl	%esi, %ecx
	subl	%eax, %ecx
	cmpl	%esi, %edx
	movl	$-1, %eax
	cmovnel	%ecx, %eax
	movl	%eax, 68(%rsp)
	movq	48(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, 88(%rsp)
	movl	$4, 64(%rsp)
	cmpl	$1, 20(%r8)
	movl	$16, %eax
	movl	$4, %ecx
	cmoveq	%rax, %rcx
	movq	32(%rsp), %rax          # 8-byte Reload
	movslq	(%rax,%rcx,4), %rax
	imulq	$104, %rax, %rsi
	movq	40(%rsp), %rax          # 8-byte Reload
	addq	24(%rax), %rsi
	leaq	64(%rsp), %rdi
	callq	*writeIntraPredMode(%rip)
	movl	76(%rsp), %r14d
	movq	input(%rip), %rax
	cmpl	$0, 4008(%rax)
	je	.LBB3_3
# BB#4:
	movl	$1, %esi
	movl	%ebx, %edi
	callq	writeLumaCoeff8x8_CABAC
	addl	%r14d, %eax
	jmp	.LBB3_5
.LBB3_3:                                # %.preheader.preheader
	xorl	%edi, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	%ebx, %esi
	callq	writeCoeff4x4_CAVLC
	movl	%eax, %ebp
	addl	%r14d, %ebp
	xorl	%edi, %edi
	movl	$1, %edx
	xorl	%ecx, %ecx
	movl	%ebx, %esi
	callq	writeCoeff4x4_CAVLC
	movl	%eax, %r14d
	addl	%ebp, %r14d
	xorl	%edi, %edi
	movl	$2, %edx
	xorl	%ecx, %ecx
	movl	%ebx, %esi
	callq	writeCoeff4x4_CAVLC
	movl	%eax, %ebp
	addl	%r14d, %ebp
	xorl	%edi, %edi
	movl	$3, %edx
	xorl	%ecx, %ecx
	movl	%ebx, %esi
	callq	writeCoeff4x4_CAVLC
	addl	%ebp, %eax
.LBB3_5:                                # %.loopexit
	cvtsi2sdq	%r15, %xmm1
	cvtsi2sdl	%eax, %xmm0
	mulsd	56(%rsp), %xmm0         # 8-byte Folded Reload
	addsd	%xmm1, %xmm0
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	RDCost_for_8x8IntraBlocks, .Lfunc_end3-RDCost_for_8x8IntraBlocks
	.cfi_endproc

	.globl	dct_luma8x8
	.p2align	4, 0x90
	.type	dct_luma8x8,@function
dct_luma8x8:                            # @dct_luma8x8
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 56
	subq	$328, %rsp              # imm = 0x148
.Lcfi52:
	.cfi_def_cfa_offset 384
.Lcfi53:
	.cfi_offset %rbx, -56
.Lcfi54:
	.cfi_offset %r12, -48
.Lcfi55:
	.cfi_offset %r13, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movq	%rsi, -80(%rsp)         # 8-byte Spill
	movl	%edi, %r13d
	leal	(,%r13,8), %r11d
	movq	img(%rip), %r15
	movq	14160(%r15), %rax
	movslq	%r13d, %rcx
	shll	$2, %r13d
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %rax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	movq	(%rax), %rax
	movq	(%rax), %rcx
	movq	%rcx, -88(%rsp)         # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	14224(%r15), %rbp
	movslq	12(%r15), %rcx
	movslq	44(%r15), %rax
	testq	%rax, %rax
	je	.LBB4_2
# BB#1:
	xorl	%esi, %esi
	jmp	.LBB4_3
.LBB4_2:
	cmpl	$1, 15540(%r15)
	sete	%sil
.LBB4_3:
	andl	$8, %r11d
	andl	$-8, %r13d
	imulq	$536, %rcx, %rbx        # imm = 0x218
	cmpl	$0, 428(%rbp,%rbx)
	movl	$FIELD_SCAN8x8, %ecx
	movl	$SNGL_SCAN8x8, %r8d
	cmovneq	%rcx, %r8
	movslq	%edx, %r9
	movl	%esi, -68(%rsp)         # 4-byte Spill
	testb	%sil, %sil
	movq	%r15, -112(%rsp)        # 8-byte Spill
	movq	%r8, -24(%rsp)          # 8-byte Spill
	je	.LBB4_4
# BB#27:
	pcmpeqd	%xmm0, %xmm0
	movdqa	%xmm0, -48(%rsp)
	leaq	472(%rbp,%rbx), %rax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, -64(%rsp)
	movl	$-1, %r10d
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	movl	$0, -104(%rsp)          # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB4_28:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%r8,%rax,2), %r14d
	movzbl	1(%r8,%rax,2), %ebx
	movl	%eax, %esi
	andl	$3, %esi
	movl	-48(%rsp,%rsi,4), %ebp
	incl	%ebp
	movl	%ebp, -48(%rsp,%rsi,4)
	movq	%rbx, %rcx
	shlq	$6, %rcx
	addq	%r15, %rcx
	movl	13136(%rcx,%r14,4), %edi
	movl	%edi, %edx
	negl	%edx
	cmovll	%edi, %edx
	cmpl	$0, 15260(%r15)
	je	.LBB4_30
# BB#29:                                #   in Loop: Header=BB4_28 Depth=1
	movq	14184(%r15), %rdi
	movq	(%rdi,%r9,8), %r8
	movslq	%r13d, %rdi
	addq	%rdi, %rbx
	movq	(%r8,%rbx,8), %rdi
	movq	-24(%rsp), %r8          # 8-byte Reload
	movl	%r11d, %ebx
	addq	%r14, %rbx
	movl	$0, (%rdi,%rbx,4)
.LBB4_30:                               #   in Loop: Header=BB4_28 Depth=1
	incl	%r10d
	testl	%edx, %edx
	je	.LBB4_36
# BB#31:                                #   in Loop: Header=BB4_28 Depth=1
	leaq	13136(%rcx,%r14,4), %rcx
	movq	-120(%rsp), %rdi        # 8-byte Reload
	cmpl	$0, (%rdi)
	je	.LBB4_34
# BB#32:                                #   in Loop: Header=BB4_28 Depth=1
	movq	input(%rip), %rdi
	cmpl	$0, 4008(%rdi)
	je	.LBB4_33
.LBB4_34:                               #   in Loop: Header=BB4_28 Depth=1
	movq	-80(%rsp), %rsi         # 8-byte Reload
	addl	$999999, (%rsi)         # imm = 0xF423F
	movl	%edx, %esi
	negl	%esi
	cmpl	$0, (%rcx)
	cmovnsl	%edx, %esi
	movslq	%r12d, %r12
	movq	-88(%rsp), %rcx         # 8-byte Reload
	movl	%esi, (%rcx,%r12,4)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%r10d, (%rcx,%r12,4)
	incl	%r12d
	movl	$-1, %r10d
	jmp	.LBB4_35
.LBB4_33:                               #   in Loop: Header=BB4_28 Depth=1
	movq	-80(%rsp), %rdi         # 8-byte Reload
	addl	$999999, (%rdi)         # imm = 0xF423F
	movl	%edx, %edi
	negl	%edi
	cmpl	$0, (%rcx)
	cmovnsl	%edx, %edi
	movq	-96(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rsi,8), %rcx
	movq	(%rcx), %rdx
	movslq	-64(%rsp,%rsi,4), %rbx
	movl	%edi, (%rdx,%rbx,4)
	movq	8(%rcx), %rcx
	movl	%ebp, (%rcx,%rbx,4)
	leal	1(%rbx), %ecx
	movl	%ecx, -64(%rsp,%rsi,4)
	movl	$-1, -48(%rsp,%rsi,4)
.LBB4_35:                               #   in Loop: Header=BB4_28 Depth=1
	movl	$1, -104(%rsp)          # 4-byte Folded Spill
.LBB4_36:                               #   in Loop: Header=BB4_28 Depth=1
	incq	%rax
	cmpq	$64, %rax
	jne	.LBB4_28
	jmp	.LBB4_37
.LBB4_4:                                # %.preheader549.preheader
	movq	%rbx, -32(%rsp)         # 8-byte Spill
	movq	%rbp, -104(%rsp)        # 8-byte Spill
	movq	%r11, 8(%rsp)           # 8-byte Spill
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	qp_per_matrix(%rip), %rcx
	movslq	(%rcx,%rax,4), %rsi
	movq	qp_rem_matrix(%rip), %rcx
	leal	16(%rsi), %edx
	movl	%edx, -96(%rsp)         # 4-byte Spill
	movq	LevelScale8x8Luma(%rip), %rdx
	movq	(%rdx,%r9,8), %rdx
	movslq	(%rcx,%rax,4), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	LevelOffset8x8Luma(%rip), %rcx
	movq	(%rcx,%r9,8), %rcx
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	(%rcx,%rsi,8), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	InvLevelScale8x8Luma(%rip), %rcx
	movq	%r9, 40(%rsp)           # 8-byte Spill
	movq	(%rcx,%r9,8), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	13164(%r15), %r13
	movq	$-8, %rax
	.p2align	4, 0x90
.LBB4_5:                                # %.preheader549
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r13), %edx
	movl	-28(%r13), %ecx
	movl	-24(%r13), %esi
	leal	(%rdx,%rcx), %r12d
	movl	-4(%r13), %ebp
	leal	(%rbp,%rsi), %r14d
	movl	-20(%r13), %ebx
	movl	-8(%r13), %r8d
	leal	(%r8,%rbx), %r11d
	movl	-16(%r13), %r9d
	movl	-12(%r13), %edi
	leal	(%rdi,%r9), %r10d
	leal	(%r10,%r12), %r15d
	subl	%r10d, %r12d
	leal	(%r11,%r14), %r10d
	subl	%r11d, %r14d
	subl	%edx, %ecx
	subl	%ebp, %esi
	subl	%r8d, %ebx
	subl	%edi, %r9d
	movl	%ecx, %ebp
	sarl	%ebp
	addl	%ecx, %ebp
	movl	%ecx, %edx
	subl	%r9d, %edx
	movl	%ebx, %edi
	sarl	%edi
	addl	%ebx, %edi
	subl	%edi, %edx
	movl	%esi, %edi
	sarl	%edi
	subl	%esi, %ecx
	subl	%edi, %ecx
	addl	%esi, %ebp
	addl	%ebx, %ebp
	subl	%ebx, %esi
	addl	%r9d, %ecx
	addl	%r9d, %esi
	movl	%r9d, %edi
	sarl	%edi
	addl	%edi, %esi
	leal	(%r15,%r10), %edi
	movl	%edi, 96(%rsp,%rax,4)
	movl	%r14d, %edi
	sarl	%edi
	addl	%r12d, %edi
	movl	%edi, 160(%rsp,%rax,4)
	subl	%r10d, %r15d
	movl	%r15d, 224(%rsp,%rax,4)
	sarl	%r12d
	subl	%r14d, %r12d
	movl	%r12d, 288(%rsp,%rax,4)
	movl	%esi, %edi
	sarl	$2, %edi
	addl	%ebp, %edi
	movl	%edi, 128(%rsp,%rax,4)
	movl	%ecx, %edi
	sarl	$2, %edi
	addl	%edx, %edi
	movl	%edi, 192(%rsp,%rax,4)
	sarl	$2, %edx
	subl	%edx, %ecx
	movl	%ecx, 256(%rsp,%rax,4)
	sarl	$2, %ebp
	subl	%esi, %ebp
	movl	%ebp, 320(%rsp,%rax,4)
	addq	$64, %r13
	incq	%rax
	jne	.LBB4_5
# BB#6:                                 # %.preheader548.preheader
	xorl	%eax, %eax
	movq	-112(%rsp), %r10        # 8-byte Reload
	.p2align	4, 0x90
.LBB4_7:                                # %.preheader548
                                        # =>This Inner Loop Header: Depth=1
	movl	64(%rsp,%rax,8), %esi
	movl	92(%rsp,%rax,8), %r8d
	leal	(%r8,%rsi), %r12d
	movl	68(%rsp,%rax,8), %ecx
	movl	88(%rsp,%rax,8), %r11d
	leal	(%r11,%rcx), %r14d
	movl	72(%rsp,%rax,8), %r9d
	movl	84(%rsp,%rax,8), %r13d
	leal	(%r13,%r9), %edx
	movl	76(%rsp,%rax,8), %ebx
	movl	80(%rsp,%rax,8), %edi
	leal	(%rdi,%rbx), %ebp
	leal	(%rbp,%r12), %r15d
	subl	%ebp, %r12d
	leal	(%rdx,%r14), %ebp
	movq	%rbp, -120(%rsp)        # 8-byte Spill
	subl	%edx, %r14d
	subl	%r8d, %esi
	subl	%r11d, %ecx
	subl	%r13d, %r9d
	subl	%edi, %ebx
	movl	%esi, %ebp
	sarl	%ebp
	addl	%esi, %ebp
	movl	%esi, %edx
	subl	%ebx, %edx
	movl	%r9d, %edi
	sarl	%edi
	addl	%r9d, %edi
	subl	%edi, %edx
	movl	%ecx, %edi
	sarl	%edi
	subl	%ecx, %esi
	subl	%edi, %esi
	addl	%ecx, %ebp
	addl	%r9d, %ebp
	subl	%r9d, %ecx
	addl	%ebx, %esi
	addl	%ebx, %ecx
	movl	%ebx, %edi
	sarl	%edi
	addl	%edi, %ecx
	movq	-120(%rsp), %rbx        # 8-byte Reload
	leal	(%r15,%rbx), %edi
	movl	%edi, 13136(%r10,%rax)
	movl	%r14d, %edi
	sarl	%edi
	addl	%r12d, %edi
	movl	%edi, 13264(%r10,%rax)
	subl	%ebx, %r15d
	movl	%r15d, 13392(%r10,%rax)
	sarl	%r12d
	subl	%r14d, %r12d
	movl	%r12d, 13520(%r10,%rax)
	movl	%ecx, %edi
	sarl	$2, %edi
	addl	%ebp, %edi
	movl	%edi, 13200(%r10,%rax)
	movl	%esi, %edi
	sarl	$2, %edi
	addl	%edx, %edi
	movl	%edi, 13328(%r10,%rax)
	sarl	$2, %edx
	subl	%edx, %esi
	movl	%esi, 13456(%r10,%rax)
	sarl	$2, %ebp
	subl	%ecx, %ebp
	movl	%ebp, 13584(%r10,%rax)
	addq	$4, %rax
	cmpq	$32, %rax
	jne	.LBB4_7
# BB#8:
	pcmpeqd	%xmm0, %xmm0
	movdqa	%xmm0, -48(%rsp)
	movq	-104(%rsp), %rax        # 8-byte Reload
	movq	-32(%rsp), %rcx         # 8-byte Reload
	leaq	472(%rax,%rcx), %rax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	movq	(%rsp), %rax            # 8-byte Reload
	leal	17(%rax), %eax
	movl	%eax, -32(%rsp)         # 4-byte Spill
	movl	$1, %eax
	movl	-96(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movl	%eax, -12(%rsp)         # 4-byte Spill
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, -64(%rsp)
	movl	$-1, %r11d
	xorl	%r9d, %r9d
	xorl	%r12d, %r12d
	movl	$0, -104(%rsp)          # 4-byte Folded Spill
	movq	-24(%rsp), %r8          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_9:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%r8,%r9,2), %ebx
	movzbl	1(%r8,%r9,2), %r13d
	movl	%r9d, %esi
	andl	$3, %esi
	movslq	-48(%rsp,%rsi,4), %r15
	incq	%r15
	movl	%r15d, -48(%rsp,%rsi,4)
	movq	%r13, %rbp
	shlq	$6, %rbp
	movq	-112(%rsp), %rax        # 8-byte Reload
	addq	%rax, %rbp
	movl	13136(%rbp,%rbx,4), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%r13,8), %rcx
	imull	(%rcx,%rbx,4), %edx
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%r13,8), %rcx
	movl	(%rcx,%rbx,4), %r10d
	addl	%edx, %r10d
	movl	-96(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %r10d
	cmpl	$0, 15260(%rax)
	je	.LBB4_14
# BB#10:                                #   in Loop: Header=BB4_9 Depth=1
	testl	%r10d, %r10d
	je	.LBB4_11
# BB#12:                                #   in Loop: Header=BB4_9 Depth=1
	movl	%r10d, %edi
	movl	-96(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	subl	%edi, %edx
	imull	AdaptRndWeight(%rip), %edx
	addl	-12(%rsp), %edx         # 4-byte Folded Reload
	movl	-32(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edx
	jmp	.LBB4_13
	.p2align	4, 0x90
.LBB4_11:                               #   in Loop: Header=BB4_9 Depth=1
	xorl	%edx, %edx
.LBB4_13:                               #   in Loop: Header=BB4_9 Depth=1
	movq	-112(%rsp), %rax        # 8-byte Reload
	movq	14184(%rax), %rcx
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rcx,%rax,8), %rcx
	movslq	16(%rsp), %rdi          # 4-byte Folded Reload
	addq	%r13, %rdi
	movq	(%rcx,%rdi,8), %rcx
	movl	8(%rsp), %edi           # 4-byte Reload
	addq	%rbx, %rdi
	movl	%edx, (%rcx,%rdi,4)
.LBB4_14:                               #   in Loop: Header=BB4_9 Depth=1
	incl	%r11d
	leaq	13136(%rbp,%rbx,4), %r14
	testl	%r10d, %r10d
	je	.LBB4_15
# BB#16:                                #   in Loop: Header=BB4_9 Depth=1
	movq	-120(%rsp), %rax        # 8-byte Reload
	cmpl	$0, (%rax)
	je	.LBB4_21
# BB#17:                                #   in Loop: Header=BB4_9 Depth=1
	movq	input(%rip), %rcx
	cmpl	$0, 4008(%rcx)
	je	.LBB4_18
.LBB4_21:                               #   in Loop: Header=BB4_9 Depth=1
	movl	$999999, %ecx           # imm = 0xF423F
	cmpl	$1, %r10d
	jg	.LBB4_23
# BB#22:                                #   in Loop: Header=BB4_9 Depth=1
	movq	input(%rip), %rax
	movslq	4180(%rax), %rax
	movslq	%r11d, %rcx
	shlq	$6, %rax
	movzbl	COEFF_COST8x8(%rax,%rcx), %ecx
.LBB4_23:                               #   in Loop: Header=BB4_9 Depth=1
	movq	-80(%rsp), %rax         # 8-byte Reload
	addl	%ecx, (%rax)
	movl	%r10d, %ecx
	negl	%ecx
	cmovll	%r10d, %ecx
	movl	%ecx, %ebp
	negl	%ebp
	cmpl	$0, (%r14)
	movl	%ecx, %eax
	cmovsl	%ebp, %eax
	movslq	%r12d, %r12
	movq	-88(%rsp), %rdx         # 8-byte Reload
	movl	%eax, (%rdx,%r12,4)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%r11d, (%rax,%r12,4)
	incl	%r12d
	movl	$-1, %r11d
	jmp	.LBB4_24
	.p2align	4, 0x90
.LBB4_15:                               #   in Loop: Header=BB4_9 Depth=1
	xorl	%ebp, %ebp
	jmp	.LBB4_25
.LBB4_18:                               #   in Loop: Header=BB4_9 Depth=1
	movl	$999999, %edx           # imm = 0xF423F
	cmpl	$1, %r10d
	jg	.LBB4_20
# BB#19:                                #   in Loop: Header=BB4_9 Depth=1
	movslq	4180(%rcx), %rcx
	shlq	$6, %rcx
	movzbl	COEFF_COST8x8(%rcx,%r15), %edx
.LBB4_20:                               #   in Loop: Header=BB4_9 Depth=1
	movq	-80(%rsp), %rax         # 8-byte Reload
	addl	%edx, (%rax)
	movl	%r10d, %ecx
	negl	%ecx
	cmovll	%r10d, %ecx
	movl	%ecx, %ebp
	negl	%ebp
	cmpl	$0, (%r14)
	movl	%ecx, %edx
	cmovsl	%ebp, %edx
	movq	-112(%rsp), %rax        # 8-byte Reload
	movq	14160(%rax), %rdi
	movq	-8(%rsp), %rax          # 8-byte Reload
	movq	(%rdi,%rax,8), %rdi
	movq	(%rdi,%rsi,8), %rdi
	movq	(%rdi), %r8
	movslq	-64(%rsp,%rsi,4), %rax
	movl	%edx, (%r8,%rax,4)
	movq	8(%rdi), %rdx
	movl	%r15d, (%rdx,%rax,4)
	leal	1(%rax), %eax
	movl	%eax, -64(%rsp,%rsi,4)
	movl	$-1, -48(%rsp,%rsi,4)
	movq	-24(%rsp), %r8          # 8-byte Reload
.LBB4_24:                               #   in Loop: Header=BB4_9 Depth=1
	cmpl	$0, (%r14)
	cmovnsl	%ecx, %ebp
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r13,8), %rax
	imull	(%rax,%rbx,4), %ebp
	movq	(%rsp), %rcx            # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %ebp
	addl	$32, %ebp
	sarl	$6, %ebp
	movl	$1, -104(%rsp)          # 4-byte Folded Spill
.LBB4_25:                               #   in Loop: Header=BB4_9 Depth=1
	movl	%ebp, (%r14)
	incq	%r9
	cmpq	$64, %r9
	jne	.LBB4_9
# BB#26:
	movq	16(%rsp), %r13          # 8-byte Reload
	movq	8(%rsp), %r11           # 8-byte Reload
	movq	-112(%rsp), %r15        # 8-byte Reload
.LBB4_37:
	movl	-68(%rsp), %esi         # 4-byte Reload
	movq	-120(%rsp), %rax        # 8-byte Reload
	cmpl	$0, (%rax)
	je	.LBB4_40
# BB#38:
	movq	input(%rip), %rax
	cmpl	$0, 4008(%rax)
	jne	.LBB4_40
# BB#39:                                # %.preheader544
	movq	14160(%r15), %rax
	movq	-8(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %rcx
	movq	(%rcx), %rcx
	movslq	-64(%rsp), %rdx
	movl	$0, (%rcx,%rdx,4)
	movq	8(%rax), %rcx
	movq	(%rcx), %rcx
	movslq	-60(%rsp), %rdx
	movl	$0, (%rcx,%rdx,4)
	movq	16(%rax), %rcx
	movq	(%rcx), %rcx
	movslq	-56(%rsp), %rdx
	movl	$0, (%rcx,%rdx,4)
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	movl	-52(%rsp), %r12d
.LBB4_40:                               # %.loopexit545
	movslq	%r12d, %rax
	movq	-88(%rsp), %rcx         # 8-byte Reload
	movl	$0, (%rcx,%rax,4)
	testb	%sil, %sil
	je	.LBB4_41
# BB#47:                                # %.preheader
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %rax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	movl	%r11d, %eax
	movslq	%r13d, %rsi
	movl	176(%r15), %ebp
	movl	%r11d, %ecx
	orl	$1, %ecx
	movq	%rcx, -112(%rsp)        # 8-byte Spill
	movl	%r11d, %r10d
	orl	$2, %r10d
	movl	%r11d, %r14d
	orl	$3, %r14d
	movl	%r11d, %r9d
	orl	$4, %r9d
	movl	%r11d, %r12d
	orl	$5, %r12d
	movq	%r11, %r8
	orl	$6, %r11d
	movl	%r8d, %edx
	orl	$7, %edx
	shlq	$5, %rsi
	leaq	(%rsi,%rax,2), %rax
	leaq	12638(%r15,%rax), %rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_48:                               # =>This Inner Loop Header: Depth=1
	movl	180(%r15), %eax
	addl	%r13d, %eax
	cltq
	movq	-120(%rsp), %rcx        # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movzwl	-14(%rsi,%rbx), %edi
	addl	13136(%r15,%rbx,2), %edi
	movl	%edi, 13136(%r15,%rbx,2)
	leal	(%rbp,%r8), %ecx
	movslq	%ecx, %rcx
	movw	%di, (%rax,%rcx,2)
	movzwl	-12(%rsi,%rbx), %ecx
	addl	13140(%r15,%rbx,2), %ecx
	movl	%ecx, 13140(%r15,%rbx,2)
	movq	-112(%rsp), %rdi        # 8-byte Reload
	leal	(%rbp,%rdi), %edi
	movslq	%edi, %rdi
	movw	%cx, (%rax,%rdi,2)
	movzwl	-10(%rsi,%rbx), %ecx
	addl	13144(%r15,%rbx,2), %ecx
	movl	%ecx, 13144(%r15,%rbx,2)
	leal	(%rbp,%r10), %edi
	movslq	%edi, %rdi
	movw	%cx, (%rax,%rdi,2)
	movzwl	-8(%rsi,%rbx), %ecx
	addl	13148(%r15,%rbx,2), %ecx
	movl	%ecx, 13148(%r15,%rbx,2)
	leal	(%rbp,%r14), %edi
	movslq	%edi, %rdi
	movw	%cx, (%rax,%rdi,2)
	movzwl	-6(%rsi,%rbx), %ecx
	addl	13152(%r15,%rbx,2), %ecx
	movl	%ecx, 13152(%r15,%rbx,2)
	leal	(%rbp,%r9), %edi
	movslq	%edi, %rdi
	movw	%cx, (%rax,%rdi,2)
	movzwl	-4(%rsi,%rbx), %ecx
	addl	13156(%r15,%rbx,2), %ecx
	movl	%ecx, 13156(%r15,%rbx,2)
	addl	%r12d, %ebp
	movslq	%ebp, %rdi
	movw	%cx, (%rax,%rdi,2)
	movzwl	-2(%rsi,%rbx), %ecx
	addl	13160(%r15,%rbx,2), %ecx
	movl	%ecx, 13160(%r15,%rbx,2)
	movl	176(%r15), %ebp
	leal	(%rbp,%r11), %edi
	movslq	%edi, %rdi
	movw	%cx, (%rax,%rdi,2)
	movzwl	(%rsi,%rbx), %ecx
	addl	13164(%r15,%rbx,2), %ecx
	movl	%ecx, 13164(%r15,%rbx,2)
	leal	(%rbp,%rdx), %edi
	movslq	%edi, %rdi
	movw	%cx, (%rax,%rdi,2)
	addq	$32, %rbx
	incl	%r13d
	cmpq	$256, %rbx              # imm = 0x100
	jne	.LBB4_48
	jmp	.LBB4_49
.LBB4_41:                               # %.preheader543.preheader
	movq	%r11, %r12
	leaq	13164(%r15), %r15
	movq	$-8, %rcx
	.p2align	4, 0x90
.LBB4_42:                               # %.preheader543
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%r15), %eax
	movl	-28(%r15), %r14d
	movl	-24(%r15), %edx
	leal	(%rax,%r14), %r10d
	subl	%eax, %r14d
	movl	-20(%r15), %eax
	movl	%eax, %esi
	sarl	%esi
	movl	-4(%r15), %edi
	subl	%edi, %esi
	sarl	%edi
	addl	%eax, %edi
	leal	(%rdi,%r10), %r8d
	leal	(%rsi,%r14), %r9d
	subl	%esi, %r14d
	subl	%edi, %r10d
	movl	-16(%r15), %r11d
	movl	-8(%r15), %eax
	movl	%eax, %esi
	subl	%r11d, %esi
	movl	(%r15), %edi
	subl	%edi, %esi
	movl	%edi, %ebp
	sarl	%ebp
	subl	%ebp, %esi
	movl	%r11d, %ebp
	sarl	%ebp
	addl	%r11d, %ebp
	movl	%edi, %ebx
	subl	%ebp, %ebx
	addl	%edx, %ebx
	movl	%eax, %ebp
	sarl	%ebp
	addl	%eax, %edi
	addl	%ebp, %edi
	subl	%edx, %edi
	addl	%r11d, %eax
	addl	%edx, %eax
	sarl	%edx
	addl	%eax, %edx
	movl	%edx, %eax
	sarl	$2, %eax
	addl	%esi, %eax
	sarl	$2, %esi
	subl	%esi, %edx
	movl	%edi, %esi
	sarl	$2, %esi
	addl	%ebx, %esi
	sarl	$2, %ebx
	subl	%edi, %ebx
	leal	(%rdx,%r8), %edi
	movl	%edi, 96(%rsp,%rcx,4)
	leal	(%rbx,%r9), %edi
	movl	%edi, 128(%rsp,%rcx,4)
	leal	(%rsi,%r14), %edi
	movl	%edi, 160(%rsp,%rcx,4)
	leal	(%rax,%r10), %edi
	movl	%edi, 192(%rsp,%rcx,4)
	subl	%eax, %r10d
	movl	%r10d, 224(%rsp,%rcx,4)
	subl	%esi, %r14d
	movl	%r14d, 256(%rsp,%rcx,4)
	subl	%ebx, %r9d
	movl	%r9d, 288(%rsp,%rcx,4)
	subl	%edx, %r8d
	movl	%r8d, 320(%rsp,%rcx,4)
	addq	$64, %r15
	incq	%rcx
	jne	.LBB4_42
# BB#43:                                # %.preheader542.preheader
	xorl	%eax, %eax
	movq	-112(%rsp), %r15        # 8-byte Reload
	.p2align	4, 0x90
.LBB4_44:                               # %.preheader542
                                        # =>This Inner Loop Header: Depth=1
	movl	64(%rsp,%rax,8), %r14d
	movl	80(%rsp,%rax,8), %ecx
	leal	(%rcx,%r14), %r10d
	subl	%ecx, %r14d
	movl	72(%rsp,%rax,8), %ecx
	movl	%ecx, %edx
	sarl	%edx
	movl	88(%rsp,%rax,8), %esi
	subl	%esi, %edx
	sarl	%esi
	addl	%ecx, %esi
	leal	(%rsi,%r10), %r8d
	leal	(%rdx,%r14), %r9d
	subl	%edx, %r14d
	subl	%esi, %r10d
	movl	76(%rsp,%rax,8), %r11d
	movl	84(%rsp,%rax,8), %ecx
	movl	%ecx, %edx
	subl	%r11d, %edx
	movl	92(%rsp,%rax,8), %ebp
	subl	%ebp, %edx
	movl	%ebp, %esi
	sarl	%esi
	subl	%esi, %edx
	movl	68(%rsp,%rax,8), %ebx
	movl	%r11d, %edi
	sarl	%edi
	addl	%r11d, %edi
	movl	%ebp, %esi
	subl	%edi, %esi
	addl	%ebx, %esi
	movl	%ecx, %edi
	sarl	%edi
	addl	%ecx, %ebp
	addl	%edi, %ebp
	subl	%ebx, %ebp
	addl	%r11d, %ecx
	addl	%ebx, %ecx
	sarl	%ebx
	addl	%ecx, %ebx
	movl	%ebx, %ecx
	sarl	$2, %ecx
	addl	%edx, %ecx
	sarl	$2, %edx
	subl	%edx, %ebx
	movl	%ebp, %edx
	sarl	$2, %edx
	addl	%esi, %edx
	sarl	$2, %esi
	subl	%ebp, %esi
	leal	(%rbx,%r8), %edi
	movl	%edi, 13136(%r15,%rax)
	leal	(%rsi,%r9), %edi
	movl	%edi, 13200(%r15,%rax)
	leal	(%rdx,%r14), %edi
	movl	%edi, 13264(%r15,%rax)
	leal	(%rcx,%r10), %edi
	movl	%edi, 13328(%r15,%rax)
	subl	%ecx, %r10d
	movl	%r10d, 13392(%r15,%rax)
	subl	%edx, %r14d
	movl	%r14d, 13456(%r15,%rax)
	subl	%esi, %r9d
	movl	%r9d, 13520(%r15,%rax)
	subl	%ebx, %r8d
	movl	%r8d, 13584(%r15,%rax)
	addq	$4, %rax
	cmpq	$32, %rax
	jne	.LBB4_44
# BB#45:                                # %.preheader540
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %rax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	movl	%r12d, %eax
	movslq	%r13d, %rdx
	movl	15520(%r15), %ebx
	movl	176(%r15), %ebp
	movl	%r12d, %ecx
	orl	$1, %ecx
	movq	%rcx, -96(%rsp)         # 8-byte Spill
	movl	%r12d, %ecx
	orl	$2, %ecx
	movq	%rcx, -80(%rsp)         # 8-byte Spill
	movl	%r12d, %ecx
	orl	$3, %ecx
	movl	%ecx, -88(%rsp)         # 4-byte Spill
	movl	%r12d, %r14d
	orl	$4, %r14d
	movl	%r12d, %r9d
	orl	$5, %r9d
	movl	%r12d, %r11d
	orl	$6, %r11d
	movl	%r12d, %esi
	orl	$7, %esi
	shlq	$5, %rdx
	leaq	(%rdx,%rax,2), %rax
	leaq	12638(%r15,%rax), %rdx
	xorl	%eax, %eax
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB4_46:                               # =>This Inner Loop Header: Depth=1
	movl	180(%r15), %edi
	addl	%r13d, %edi
	movslq	%edi, %rdi
	movq	-120(%rsp), %rcx        # 8-byte Reload
	movq	(%rcx,%rdi,8), %r8
	movl	13136(%r15,%r10,2), %edi
	movzwl	-14(%rdx,%r10), %ecx
	shll	$6, %ecx
	leal	32(%rdi,%rcx), %ecx
	sarl	$6, %ecx
	cmovsl	%eax, %ecx
	cmpl	%ebx, %ecx
	cmovgl	%ebx, %ecx
	movl	%ecx, 13136(%r15,%r10,2)
	leal	(%rbp,%r12), %edi
	movslq	%edi, %rdi
	movw	%cx, (%r8,%rdi,2)
	movl	13140(%r15,%r10,2), %ecx
	movzwl	-12(%rdx,%r10), %edi
	shll	$6, %edi
	leal	32(%rcx,%rdi), %ecx
	sarl	$6, %ecx
	cmovsl	%eax, %ecx
	cmpl	%ebx, %ecx
	cmovgl	%ebx, %ecx
	movl	%ecx, 13140(%r15,%r10,2)
	movq	-96(%rsp), %rdi         # 8-byte Reload
	leal	(%rbp,%rdi), %edi
	movslq	%edi, %rdi
	movw	%cx, (%r8,%rdi,2)
	movl	13144(%r15,%r10,2), %ecx
	movzwl	-10(%rdx,%r10), %edi
	shll	$6, %edi
	leal	32(%rcx,%rdi), %ecx
	sarl	$6, %ecx
	cmovsl	%eax, %ecx
	cmpl	%ebx, %ecx
	cmovgl	%ebx, %ecx
	movl	%ecx, 13144(%r15,%r10,2)
	movq	-80(%rsp), %rdi         # 8-byte Reload
	leal	(%rbp,%rdi), %edi
	movslq	%edi, %rdi
	movw	%cx, (%r8,%rdi,2)
	movl	13148(%r15,%r10,2), %ecx
	movzwl	-8(%rdx,%r10), %edi
	shll	$6, %edi
	leal	32(%rcx,%rdi), %ecx
	sarl	$6, %ecx
	cmovsl	%eax, %ecx
	cmpl	%ebx, %ecx
	cmovgl	%ebx, %ecx
	movl	%ecx, 13148(%r15,%r10,2)
	addl	-88(%rsp), %ebp         # 4-byte Folded Reload
	movslq	%ebp, %rdi
	movw	%cx, (%r8,%rdi,2)
	movl	13152(%r15,%r10,2), %ecx
	movzwl	-6(%rdx,%r10), %edi
	shll	$6, %edi
	leal	32(%rcx,%rdi), %ecx
	sarl	$6, %ecx
	cmovsl	%eax, %ecx
	cmpl	%ebx, %ecx
	cmovgl	%ebx, %ecx
	movl	%ecx, 13152(%r15,%r10,2)
	movl	176(%r15), %ebp
	leal	(%rbp,%r14), %edi
	movslq	%edi, %rdi
	movw	%cx, (%r8,%rdi,2)
	movl	15520(%r15), %ebx
	movl	13156(%r15,%r10,2), %ecx
	movzwl	-4(%rdx,%r10), %edi
	shll	$6, %edi
	leal	32(%rcx,%rdi), %ecx
	sarl	$6, %ecx
	cmovsl	%eax, %ecx
	cmpl	%ebx, %ecx
	cmovgl	%ebx, %ecx
	movl	%ecx, 13156(%r15,%r10,2)
	leal	(%rbp,%r9), %edi
	movslq	%edi, %rdi
	movw	%cx, (%r8,%rdi,2)
	movl	13160(%r15,%r10,2), %ecx
	movzwl	-2(%rdx,%r10), %edi
	shll	$6, %edi
	leal	32(%rcx,%rdi), %ecx
	sarl	$6, %ecx
	cmovsl	%eax, %ecx
	cmpl	%ebx, %ecx
	cmovgl	%ebx, %ecx
	movl	%ecx, 13160(%r15,%r10,2)
	leal	(%rbp,%r11), %edi
	movslq	%edi, %rdi
	movw	%cx, (%r8,%rdi,2)
	movl	13164(%r15,%r10,2), %ecx
	movzwl	(%rdx,%r10), %edi
	shll	$6, %edi
	leal	32(%rcx,%rdi), %ecx
	sarl	$6, %ecx
	cmovsl	%eax, %ecx
	cmpl	%ebx, %ecx
	cmovgl	%ebx, %ecx
	movl	%ecx, 13164(%r15,%r10,2)
	leal	(%rbp,%rsi), %edi
	movslq	%edi, %rdi
	movw	%cx, (%r8,%rdi,2)
	addq	$32, %r10
	incl	%r13d
	cmpq	$256, %r10              # imm = 0x100
	jne	.LBB4_46
.LBB4_49:                               # %.loopexit
	movl	-104(%rsp), %eax        # 4-byte Reload
	addq	$328, %rsp              # imm = 0x148
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	dct_luma8x8, .Lfunc_end4-dct_luma8x8
	.cfi_endproc

	.globl	LowPassForIntra8x8Pred
	.p2align	4, 0x90
	.type	LowPassForIntra8x8Pred,@function
LowPassForIntra8x8Pred:                 # @LowPassForIntra8x8Pred
	.cfi_startproc
# BB#0:
	movzwl	48(%rdi), %eax
	movw	%ax, -8(%rsp)
	movups	(%rdi), %xmm0
	movups	16(%rdi), %xmm1
	movups	32(%rdi), %xmm2
	movaps	%xmm2, -24(%rsp)
	movaps	%xmm1, -40(%rsp)
	movaps	%xmm0, -56(%rsp)
	testl	%edx, %edx
	je	.LBB5_7
# BB#1:
	testl	%esi, %esi
	movw	2(%rdi), %r8w
	movzwl	%r8w, %r9d
	je	.LBB5_3
# BB#2:
	movzwl	(%rdi), %eax
	leal	(%rax,%r9,2), %eax
	jmp	.LBB5_4
.LBB5_3:
	leal	(%r9,%r9,2), %eax
.LBB5_4:                                # %.preheader102
	movzwl	4(%rdi), %r9d
	leal	2(%rax,%r9), %eax
	shrl	$2, %eax
	movw	%ax, -54(%rsp)
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB5_5:                                # =>This Inner Loop Header: Depth=1
	movzwl	%r8w, %r8d
	movzwl	%r9w, %r9d
	leal	(%r8,%r9,2), %r10d
	movzwl	6(%rdi,%r11,2), %r8d
	leal	2(%r8,%r10), %eax
	shrl	$2, %eax
	movw	%ax, -52(%rsp,%r11,2)
	leal	(%r9,%r8,2), %eax
	movzwl	8(%rdi,%r11,2), %r9d
	leal	2(%r9,%rax), %eax
	shrl	$2, %eax
	movw	%ax, -50(%rsp,%r11,2)
	addq	$2, %r11
	cmpq	$14, %r11
	jne	.LBB5_5
# BB#6:
	movzwl	32(%rdi), %eax
	leal	(%rax,%rax,2), %r8d
	movzwl	30(%rdi), %eax
	leal	2(%rax,%r8), %eax
	shrl	$2, %eax
	movw	%ax, -24(%rsp)
.LBB5_7:
	testl	%esi, %esi
	je	.LBB5_13
# BB#8:
	testl	%edx, %edx
	je	.LBB5_11
# BB#9:
	testl	%ecx, %ecx
	je	.LBB5_11
# BB#10:
	movzwl	34(%rdi), %ecx
	movzwl	(%rdi), %eax
	leal	(%rcx,%rax,2), %ecx
	movl	$1, %edx
	jmp	.LBB5_18
.LBB5_11:
	testl	%edx, %edx
	je	.LBB5_16
# BB#12:
	movzwl	(%rdi), %eax
	leal	(%rax,%rax,2), %eax
	movzwl	2(%rdi), %edx
	leal	2(%rdx,%rax), %eax
	shrl	$2, %eax
	movw	%ax, -56(%rsp)
.LBB5_13:
	testl	%ecx, %ecx
	je	.LBB5_22
# BB#14:
	testl	%esi, %esi
	je	.LBB5_20
# BB#15:                                # %..thread94_crit_edge
	movw	(%rdi), %ax
	jmp	.LBB5_19
.LBB5_20:
	movzwl	34(%rdi), %eax
	leal	(%rax,%rax,2), %ecx
	jmp	.LBB5_21
.LBB5_16:
	testl	%ecx, %ecx
	je	.LBB5_22
# BB#17:
	movzwl	(%rdi), %eax
	leal	(%rax,%rax,2), %ecx
	movl	$17, %edx
.LBB5_18:                               # %.thread94.sink.split
	movzwl	(%rdi,%rdx,2), %edx
	leal	2(%rcx,%rdx), %ecx
	shrl	$2, %ecx
	movw	%cx, -56(%rsp)
.LBB5_19:                               # %.thread94
	movzwl	%ax, %ecx
	movzwl	34(%rdi), %eax
	leal	(%rcx,%rax,2), %ecx
.LBB5_21:                               # %.preheader
	movzwl	36(%rdi), %edx
	leal	2(%rcx,%rdx), %ecx
	shrl	$2, %ecx
	movw	%cx, -22(%rsp)
	movzwl	%ax, %eax
	leal	(%rax,%rdx,2), %eax
	movzwl	38(%rdi), %ecx
	leal	2(%rcx,%rax), %eax
	shrl	$2, %eax
	movw	%ax, -20(%rsp)
	leal	(%rdx,%rcx,2), %eax
	movzwl	40(%rdi), %edx
	leal	2(%rdx,%rax), %eax
	shrl	$2, %eax
	movw	%ax, -18(%rsp)
	leal	(%rcx,%rdx,2), %eax
	movzwl	42(%rdi), %ecx
	leal	2(%rcx,%rax), %eax
	shrl	$2, %eax
	movw	%ax, -16(%rsp)
	leal	(%rdx,%rcx,2), %eax
	movzwl	44(%rdi), %edx
	leal	2(%rdx,%rax), %eax
	shrl	$2, %eax
	movw	%ax, -14(%rsp)
	leal	(%rcx,%rdx,2), %eax
	movzwl	46(%rdi), %ecx
	leal	2(%rcx,%rax), %eax
	shrl	$2, %eax
	movw	%ax, -12(%rsp)
	leal	(%rdx,%rcx,2), %eax
	movzwl	48(%rdi), %edx
	leal	2(%rdx,%rax), %eax
	shrl	$2, %eax
	movw	%ax, -10(%rsp)
	addl	%edx, %ecx
	leal	2(%rcx,%rdx,2), %eax
	shrl	$2, %eax
	movw	%ax, -8(%rsp)
.LBB5_22:                               # %.thread93
	movzwl	-8(%rsp), %eax
	movw	%ax, 48(%rdi)
	movaps	-56(%rsp), %xmm0
	movaps	-40(%rsp), %xmm1
	movaps	-24(%rsp), %xmm2
	movups	%xmm2, 32(%rdi)
	movups	%xmm1, 16(%rdi)
	movups	%xmm0, (%rdi)
	retq
.Lfunc_end5:
	.size	LowPassForIntra8x8Pred, .Lfunc_end5-LowPassForIntra8x8Pred
	.cfi_endproc

	.type	quant_coef8,@object     # @quant_coef8
	.section	.rodata,"a",@progbits
	.globl	quant_coef8
	.p2align	4
quant_coef8:
	.long	13107                   # 0x3333
	.long	12222                   # 0x2fbe
	.long	16777                   # 0x4189
	.long	12222                   # 0x2fbe
	.long	13107                   # 0x3333
	.long	12222                   # 0x2fbe
	.long	16777                   # 0x4189
	.long	12222                   # 0x2fbe
	.long	12222                   # 0x2fbe
	.long	11428                   # 0x2ca4
	.long	15481                   # 0x3c79
	.long	11428                   # 0x2ca4
	.long	12222                   # 0x2fbe
	.long	11428                   # 0x2ca4
	.long	15481                   # 0x3c79
	.long	11428                   # 0x2ca4
	.long	16777                   # 0x4189
	.long	15481                   # 0x3c79
	.long	20972                   # 0x51ec
	.long	15481                   # 0x3c79
	.long	16777                   # 0x4189
	.long	15481                   # 0x3c79
	.long	20972                   # 0x51ec
	.long	15481                   # 0x3c79
	.long	12222                   # 0x2fbe
	.long	11428                   # 0x2ca4
	.long	15481                   # 0x3c79
	.long	11428                   # 0x2ca4
	.long	12222                   # 0x2fbe
	.long	11428                   # 0x2ca4
	.long	15481                   # 0x3c79
	.long	11428                   # 0x2ca4
	.long	13107                   # 0x3333
	.long	12222                   # 0x2fbe
	.long	16777                   # 0x4189
	.long	12222                   # 0x2fbe
	.long	13107                   # 0x3333
	.long	12222                   # 0x2fbe
	.long	16777                   # 0x4189
	.long	12222                   # 0x2fbe
	.long	12222                   # 0x2fbe
	.long	11428                   # 0x2ca4
	.long	15481                   # 0x3c79
	.long	11428                   # 0x2ca4
	.long	12222                   # 0x2fbe
	.long	11428                   # 0x2ca4
	.long	15481                   # 0x3c79
	.long	11428                   # 0x2ca4
	.long	16777                   # 0x4189
	.long	15481                   # 0x3c79
	.long	20972                   # 0x51ec
	.long	15481                   # 0x3c79
	.long	16777                   # 0x4189
	.long	15481                   # 0x3c79
	.long	20972                   # 0x51ec
	.long	15481                   # 0x3c79
	.long	12222                   # 0x2fbe
	.long	11428                   # 0x2ca4
	.long	15481                   # 0x3c79
	.long	11428                   # 0x2ca4
	.long	12222                   # 0x2fbe
	.long	11428                   # 0x2ca4
	.long	15481                   # 0x3c79
	.long	11428                   # 0x2ca4
	.long	11916                   # 0x2e8c
	.long	11058                   # 0x2b32
	.long	14980                   # 0x3a84
	.long	11058                   # 0x2b32
	.long	11916                   # 0x2e8c
	.long	11058                   # 0x2b32
	.long	14980                   # 0x3a84
	.long	11058                   # 0x2b32
	.long	11058                   # 0x2b32
	.long	10826                   # 0x2a4a
	.long	14290                   # 0x37d2
	.long	10826                   # 0x2a4a
	.long	11058                   # 0x2b32
	.long	10826                   # 0x2a4a
	.long	14290                   # 0x37d2
	.long	10826                   # 0x2a4a
	.long	14980                   # 0x3a84
	.long	14290                   # 0x37d2
	.long	19174                   # 0x4ae6
	.long	14290                   # 0x37d2
	.long	14980                   # 0x3a84
	.long	14290                   # 0x37d2
	.long	19174                   # 0x4ae6
	.long	14290                   # 0x37d2
	.long	11058                   # 0x2b32
	.long	10826                   # 0x2a4a
	.long	14290                   # 0x37d2
	.long	10826                   # 0x2a4a
	.long	11058                   # 0x2b32
	.long	10826                   # 0x2a4a
	.long	14290                   # 0x37d2
	.long	10826                   # 0x2a4a
	.long	11916                   # 0x2e8c
	.long	11058                   # 0x2b32
	.long	14980                   # 0x3a84
	.long	11058                   # 0x2b32
	.long	11916                   # 0x2e8c
	.long	11058                   # 0x2b32
	.long	14980                   # 0x3a84
	.long	11058                   # 0x2b32
	.long	11058                   # 0x2b32
	.long	10826                   # 0x2a4a
	.long	14290                   # 0x37d2
	.long	10826                   # 0x2a4a
	.long	11058                   # 0x2b32
	.long	10826                   # 0x2a4a
	.long	14290                   # 0x37d2
	.long	10826                   # 0x2a4a
	.long	14980                   # 0x3a84
	.long	14290                   # 0x37d2
	.long	19174                   # 0x4ae6
	.long	14290                   # 0x37d2
	.long	14980                   # 0x3a84
	.long	14290                   # 0x37d2
	.long	19174                   # 0x4ae6
	.long	14290                   # 0x37d2
	.long	11058                   # 0x2b32
	.long	10826                   # 0x2a4a
	.long	14290                   # 0x37d2
	.long	10826                   # 0x2a4a
	.long	11058                   # 0x2b32
	.long	10826                   # 0x2a4a
	.long	14290                   # 0x37d2
	.long	10826                   # 0x2a4a
	.long	10082                   # 0x2762
	.long	9675                    # 0x25cb
	.long	12710                   # 0x31a6
	.long	9675                    # 0x25cb
	.long	10082                   # 0x2762
	.long	9675                    # 0x25cb
	.long	12710                   # 0x31a6
	.long	9675                    # 0x25cb
	.long	9675                    # 0x25cb
	.long	8943                    # 0x22ef
	.long	11985                   # 0x2ed1
	.long	8943                    # 0x22ef
	.long	9675                    # 0x25cb
	.long	8943                    # 0x22ef
	.long	11985                   # 0x2ed1
	.long	8943                    # 0x22ef
	.long	12710                   # 0x31a6
	.long	11985                   # 0x2ed1
	.long	15978                   # 0x3e6a
	.long	11985                   # 0x2ed1
	.long	12710                   # 0x31a6
	.long	11985                   # 0x2ed1
	.long	15978                   # 0x3e6a
	.long	11985                   # 0x2ed1
	.long	9675                    # 0x25cb
	.long	8943                    # 0x22ef
	.long	11985                   # 0x2ed1
	.long	8943                    # 0x22ef
	.long	9675                    # 0x25cb
	.long	8943                    # 0x22ef
	.long	11985                   # 0x2ed1
	.long	8943                    # 0x22ef
	.long	10082                   # 0x2762
	.long	9675                    # 0x25cb
	.long	12710                   # 0x31a6
	.long	9675                    # 0x25cb
	.long	10082                   # 0x2762
	.long	9675                    # 0x25cb
	.long	12710                   # 0x31a6
	.long	9675                    # 0x25cb
	.long	9675                    # 0x25cb
	.long	8943                    # 0x22ef
	.long	11985                   # 0x2ed1
	.long	8943                    # 0x22ef
	.long	9675                    # 0x25cb
	.long	8943                    # 0x22ef
	.long	11985                   # 0x2ed1
	.long	8943                    # 0x22ef
	.long	12710                   # 0x31a6
	.long	11985                   # 0x2ed1
	.long	15978                   # 0x3e6a
	.long	11985                   # 0x2ed1
	.long	12710                   # 0x31a6
	.long	11985                   # 0x2ed1
	.long	15978                   # 0x3e6a
	.long	11985                   # 0x2ed1
	.long	9675                    # 0x25cb
	.long	8943                    # 0x22ef
	.long	11985                   # 0x2ed1
	.long	8943                    # 0x22ef
	.long	9675                    # 0x25cb
	.long	8943                    # 0x22ef
	.long	11985                   # 0x2ed1
	.long	8943                    # 0x22ef
	.long	9362                    # 0x2492
	.long	8931                    # 0x22e3
	.long	11984                   # 0x2ed0
	.long	8931                    # 0x22e3
	.long	9362                    # 0x2492
	.long	8931                    # 0x22e3
	.long	11984                   # 0x2ed0
	.long	8931                    # 0x22e3
	.long	8931                    # 0x22e3
	.long	8228                    # 0x2024
	.long	11259                   # 0x2bfb
	.long	8228                    # 0x2024
	.long	8931                    # 0x22e3
	.long	8228                    # 0x2024
	.long	11259                   # 0x2bfb
	.long	8228                    # 0x2024
	.long	11984                   # 0x2ed0
	.long	11259                   # 0x2bfb
	.long	14913                   # 0x3a41
	.long	11259                   # 0x2bfb
	.long	11984                   # 0x2ed0
	.long	11259                   # 0x2bfb
	.long	14913                   # 0x3a41
	.long	11259                   # 0x2bfb
	.long	8931                    # 0x22e3
	.long	8228                    # 0x2024
	.long	11259                   # 0x2bfb
	.long	8228                    # 0x2024
	.long	8931                    # 0x22e3
	.long	8228                    # 0x2024
	.long	11259                   # 0x2bfb
	.long	8228                    # 0x2024
	.long	9362                    # 0x2492
	.long	8931                    # 0x22e3
	.long	11984                   # 0x2ed0
	.long	8931                    # 0x22e3
	.long	9362                    # 0x2492
	.long	8931                    # 0x22e3
	.long	11984                   # 0x2ed0
	.long	8931                    # 0x22e3
	.long	8931                    # 0x22e3
	.long	8228                    # 0x2024
	.long	11259                   # 0x2bfb
	.long	8228                    # 0x2024
	.long	8931                    # 0x22e3
	.long	8228                    # 0x2024
	.long	11259                   # 0x2bfb
	.long	8228                    # 0x2024
	.long	11984                   # 0x2ed0
	.long	11259                   # 0x2bfb
	.long	14913                   # 0x3a41
	.long	11259                   # 0x2bfb
	.long	11984                   # 0x2ed0
	.long	11259                   # 0x2bfb
	.long	14913                   # 0x3a41
	.long	11259                   # 0x2bfb
	.long	8931                    # 0x22e3
	.long	8228                    # 0x2024
	.long	11259                   # 0x2bfb
	.long	8228                    # 0x2024
	.long	8931                    # 0x22e3
	.long	8228                    # 0x2024
	.long	11259                   # 0x2bfb
	.long	8228                    # 0x2024
	.long	8192                    # 0x2000
	.long	7740                    # 0x1e3c
	.long	10486                   # 0x28f6
	.long	7740                    # 0x1e3c
	.long	8192                    # 0x2000
	.long	7740                    # 0x1e3c
	.long	10486                   # 0x28f6
	.long	7740                    # 0x1e3c
	.long	7740                    # 0x1e3c
	.long	7346                    # 0x1cb2
	.long	9777                    # 0x2631
	.long	7346                    # 0x1cb2
	.long	7740                    # 0x1e3c
	.long	7346                    # 0x1cb2
	.long	9777                    # 0x2631
	.long	7346                    # 0x1cb2
	.long	10486                   # 0x28f6
	.long	9777                    # 0x2631
	.long	13159                   # 0x3367
	.long	9777                    # 0x2631
	.long	10486                   # 0x28f6
	.long	9777                    # 0x2631
	.long	13159                   # 0x3367
	.long	9777                    # 0x2631
	.long	7740                    # 0x1e3c
	.long	7346                    # 0x1cb2
	.long	9777                    # 0x2631
	.long	7346                    # 0x1cb2
	.long	7740                    # 0x1e3c
	.long	7346                    # 0x1cb2
	.long	9777                    # 0x2631
	.long	7346                    # 0x1cb2
	.long	8192                    # 0x2000
	.long	7740                    # 0x1e3c
	.long	10486                   # 0x28f6
	.long	7740                    # 0x1e3c
	.long	8192                    # 0x2000
	.long	7740                    # 0x1e3c
	.long	10486                   # 0x28f6
	.long	7740                    # 0x1e3c
	.long	7740                    # 0x1e3c
	.long	7346                    # 0x1cb2
	.long	9777                    # 0x2631
	.long	7346                    # 0x1cb2
	.long	7740                    # 0x1e3c
	.long	7346                    # 0x1cb2
	.long	9777                    # 0x2631
	.long	7346                    # 0x1cb2
	.long	10486                   # 0x28f6
	.long	9777                    # 0x2631
	.long	13159                   # 0x3367
	.long	9777                    # 0x2631
	.long	10486                   # 0x28f6
	.long	9777                    # 0x2631
	.long	13159                   # 0x3367
	.long	9777                    # 0x2631
	.long	7740                    # 0x1e3c
	.long	7346                    # 0x1cb2
	.long	9777                    # 0x2631
	.long	7346                    # 0x1cb2
	.long	7740                    # 0x1e3c
	.long	7346                    # 0x1cb2
	.long	9777                    # 0x2631
	.long	7346                    # 0x1cb2
	.long	7282                    # 0x1c72
	.long	6830                    # 0x1aae
	.long	9118                    # 0x239e
	.long	6830                    # 0x1aae
	.long	7282                    # 0x1c72
	.long	6830                    # 0x1aae
	.long	9118                    # 0x239e
	.long	6830                    # 0x1aae
	.long	6830                    # 0x1aae
	.long	6428                    # 0x191c
	.long	8640                    # 0x21c0
	.long	6428                    # 0x191c
	.long	6830                    # 0x1aae
	.long	6428                    # 0x191c
	.long	8640                    # 0x21c0
	.long	6428                    # 0x191c
	.long	9118                    # 0x239e
	.long	8640                    # 0x21c0
	.long	11570                   # 0x2d32
	.long	8640                    # 0x21c0
	.long	9118                    # 0x239e
	.long	8640                    # 0x21c0
	.long	11570                   # 0x2d32
	.long	8640                    # 0x21c0
	.long	6830                    # 0x1aae
	.long	6428                    # 0x191c
	.long	8640                    # 0x21c0
	.long	6428                    # 0x191c
	.long	6830                    # 0x1aae
	.long	6428                    # 0x191c
	.long	8640                    # 0x21c0
	.long	6428                    # 0x191c
	.long	7282                    # 0x1c72
	.long	6830                    # 0x1aae
	.long	9118                    # 0x239e
	.long	6830                    # 0x1aae
	.long	7282                    # 0x1c72
	.long	6830                    # 0x1aae
	.long	9118                    # 0x239e
	.long	6830                    # 0x1aae
	.long	6830                    # 0x1aae
	.long	6428                    # 0x191c
	.long	8640                    # 0x21c0
	.long	6428                    # 0x191c
	.long	6830                    # 0x1aae
	.long	6428                    # 0x191c
	.long	8640                    # 0x21c0
	.long	6428                    # 0x191c
	.long	9118                    # 0x239e
	.long	8640                    # 0x21c0
	.long	11570                   # 0x2d32
	.long	8640                    # 0x21c0
	.long	9118                    # 0x239e
	.long	8640                    # 0x21c0
	.long	11570                   # 0x2d32
	.long	8640                    # 0x21c0
	.long	6830                    # 0x1aae
	.long	6428                    # 0x191c
	.long	8640                    # 0x21c0
	.long	6428                    # 0x191c
	.long	6830                    # 0x1aae
	.long	6428                    # 0x191c
	.long	8640                    # 0x21c0
	.long	6428                    # 0x191c
	.size	quant_coef8, 1536

	.type	dequant_coef8,@object   # @dequant_coef8
	.globl	dequant_coef8
	.p2align	4
dequant_coef8:
	.long	20                      # 0x14
	.long	19                      # 0x13
	.long	25                      # 0x19
	.long	19                      # 0x13
	.long	20                      # 0x14
	.long	19                      # 0x13
	.long	25                      # 0x19
	.long	19                      # 0x13
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	25                      # 0x19
	.long	24                      # 0x18
	.long	32                      # 0x20
	.long	24                      # 0x18
	.long	25                      # 0x19
	.long	24                      # 0x18
	.long	32                      # 0x20
	.long	24                      # 0x18
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	20                      # 0x14
	.long	19                      # 0x13
	.long	25                      # 0x19
	.long	19                      # 0x13
	.long	20                      # 0x14
	.long	19                      # 0x13
	.long	25                      # 0x19
	.long	19                      # 0x13
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	25                      # 0x19
	.long	24                      # 0x18
	.long	32                      # 0x20
	.long	24                      # 0x18
	.long	25                      # 0x19
	.long	24                      # 0x18
	.long	32                      # 0x20
	.long	24                      # 0x18
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	22                      # 0x16
	.long	21                      # 0x15
	.long	28                      # 0x1c
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	21                      # 0x15
	.long	28                      # 0x1c
	.long	21                      # 0x15
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	22                      # 0x16
	.long	21                      # 0x15
	.long	28                      # 0x1c
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	21                      # 0x15
	.long	28                      # 0x1c
	.long	21                      # 0x15
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	24                      # 0x18
	.long	33                      # 0x21
	.long	24                      # 0x18
	.long	26                      # 0x1a
	.long	24                      # 0x18
	.long	33                      # 0x21
	.long	24                      # 0x18
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	33                      # 0x21
	.long	31                      # 0x1f
	.long	42                      # 0x2a
	.long	31                      # 0x1f
	.long	33                      # 0x21
	.long	31                      # 0x1f
	.long	42                      # 0x2a
	.long	31                      # 0x1f
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	26                      # 0x1a
	.long	24                      # 0x18
	.long	33                      # 0x21
	.long	24                      # 0x18
	.long	26                      # 0x1a
	.long	24                      # 0x18
	.long	33                      # 0x21
	.long	24                      # 0x18
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	33                      # 0x21
	.long	31                      # 0x1f
	.long	42                      # 0x2a
	.long	31                      # 0x1f
	.long	33                      # 0x21
	.long	31                      # 0x1f
	.long	42                      # 0x2a
	.long	31                      # 0x1f
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	35                      # 0x23
	.long	33                      # 0x21
	.long	45                      # 0x2d
	.long	33                      # 0x21
	.long	35                      # 0x23
	.long	33                      # 0x21
	.long	45                      # 0x2d
	.long	33                      # 0x21
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	35                      # 0x23
	.long	33                      # 0x21
	.long	45                      # 0x2d
	.long	33                      # 0x21
	.long	35                      # 0x23
	.long	33                      # 0x21
	.long	45                      # 0x2d
	.long	33                      # 0x21
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	32                      # 0x20
	.long	30                      # 0x1e
	.long	40                      # 0x28
	.long	30                      # 0x1e
	.long	32                      # 0x20
	.long	30                      # 0x1e
	.long	40                      # 0x28
	.long	30                      # 0x1e
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	40                      # 0x28
	.long	38                      # 0x26
	.long	51                      # 0x33
	.long	38                      # 0x26
	.long	40                      # 0x28
	.long	38                      # 0x26
	.long	51                      # 0x33
	.long	38                      # 0x26
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	32                      # 0x20
	.long	30                      # 0x1e
	.long	40                      # 0x28
	.long	30                      # 0x1e
	.long	32                      # 0x20
	.long	30                      # 0x1e
	.long	40                      # 0x28
	.long	30                      # 0x1e
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	40                      # 0x28
	.long	38                      # 0x26
	.long	51                      # 0x33
	.long	38                      # 0x26
	.long	40                      # 0x28
	.long	38                      # 0x26
	.long	51                      # 0x33
	.long	38                      # 0x26
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	36                      # 0x24
	.long	34                      # 0x22
	.long	46                      # 0x2e
	.long	34                      # 0x22
	.long	36                      # 0x24
	.long	34                      # 0x22
	.long	46                      # 0x2e
	.long	34                      # 0x22
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.long	46                      # 0x2e
	.long	43                      # 0x2b
	.long	58                      # 0x3a
	.long	43                      # 0x2b
	.long	46                      # 0x2e
	.long	43                      # 0x2b
	.long	58                      # 0x3a
	.long	43                      # 0x2b
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.long	36                      # 0x24
	.long	34                      # 0x22
	.long	46                      # 0x2e
	.long	34                      # 0x22
	.long	36                      # 0x24
	.long	34                      # 0x22
	.long	46                      # 0x2e
	.long	34                      # 0x22
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.long	46                      # 0x2e
	.long	43                      # 0x2b
	.long	58                      # 0x3a
	.long	43                      # 0x2b
	.long	46                      # 0x2e
	.long	43                      # 0x2b
	.long	58                      # 0x3a
	.long	43                      # 0x2b
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.size	dequant_coef8, 1536

	.type	SNGL_SCAN8x8,@object    # @SNGL_SCAN8x8
	.globl	SNGL_SCAN8x8
	.p2align	4
SNGL_SCAN8x8:
	.zero	2
	.asciz	"\001"
	.ascii	"\000\001"
	.ascii	"\000\002"
	.zero	2,1
	.asciz	"\002"
	.asciz	"\003"
	.ascii	"\002\001"
	.ascii	"\001\002"
	.ascii	"\000\003"
	.ascii	"\000\004"
	.ascii	"\001\003"
	.zero	2,2
	.ascii	"\003\001"
	.asciz	"\004"
	.asciz	"\005"
	.ascii	"\004\001"
	.ascii	"\003\002"
	.ascii	"\002\003"
	.ascii	"\001\004"
	.ascii	"\000\005"
	.ascii	"\000\006"
	.ascii	"\001\005"
	.ascii	"\002\004"
	.zero	2,3
	.ascii	"\004\002"
	.ascii	"\005\001"
	.asciz	"\006"
	.asciz	"\007"
	.ascii	"\006\001"
	.ascii	"\005\002"
	.ascii	"\004\003"
	.ascii	"\003\004"
	.ascii	"\002\005"
	.ascii	"\001\006"
	.ascii	"\000\007"
	.ascii	"\001\007"
	.ascii	"\002\006"
	.ascii	"\003\005"
	.zero	2,4
	.ascii	"\005\003"
	.ascii	"\006\002"
	.ascii	"\007\001"
	.ascii	"\007\002"
	.ascii	"\006\003"
	.ascii	"\005\004"
	.ascii	"\004\005"
	.ascii	"\003\006"
	.ascii	"\002\007"
	.ascii	"\003\007"
	.ascii	"\004\006"
	.zero	2,5
	.ascii	"\006\004"
	.ascii	"\007\003"
	.ascii	"\007\004"
	.ascii	"\006\005"
	.ascii	"\005\006"
	.ascii	"\004\007"
	.ascii	"\005\007"
	.zero	2,6
	.ascii	"\007\005"
	.ascii	"\007\006"
	.ascii	"\006\007"
	.zero	2,7
	.size	SNGL_SCAN8x8, 128

	.type	FIELD_SCAN8x8,@object   # @FIELD_SCAN8x8
	.globl	FIELD_SCAN8x8
	.p2align	4
FIELD_SCAN8x8:
	.zero	2
	.ascii	"\000\001"
	.ascii	"\000\002"
	.asciz	"\001"
	.zero	2,1
	.ascii	"\000\003"
	.ascii	"\000\004"
	.ascii	"\001\002"
	.asciz	"\002"
	.ascii	"\001\003"
	.ascii	"\000\005"
	.ascii	"\000\006"
	.ascii	"\000\007"
	.ascii	"\001\004"
	.ascii	"\002\001"
	.asciz	"\003"
	.zero	2,2
	.ascii	"\001\005"
	.ascii	"\001\006"
	.ascii	"\001\007"
	.ascii	"\002\003"
	.ascii	"\003\001"
	.asciz	"\004"
	.ascii	"\003\002"
	.ascii	"\002\004"
	.ascii	"\002\005"
	.ascii	"\002\006"
	.ascii	"\002\007"
	.zero	2,3
	.ascii	"\004\001"
	.asciz	"\005"
	.ascii	"\004\002"
	.ascii	"\003\004"
	.ascii	"\003\005"
	.ascii	"\003\006"
	.ascii	"\003\007"
	.ascii	"\004\003"
	.ascii	"\005\001"
	.asciz	"\006"
	.ascii	"\005\002"
	.zero	2,4
	.ascii	"\004\005"
	.ascii	"\004\006"
	.ascii	"\004\007"
	.ascii	"\005\003"
	.ascii	"\006\001"
	.ascii	"\006\002"
	.ascii	"\005\004"
	.zero	2,5
	.ascii	"\005\006"
	.ascii	"\005\007"
	.ascii	"\006\003"
	.asciz	"\007"
	.ascii	"\007\001"
	.ascii	"\006\004"
	.ascii	"\006\005"
	.zero	2,6
	.ascii	"\006\007"
	.ascii	"\007\002"
	.ascii	"\007\003"
	.ascii	"\007\004"
	.ascii	"\007\005"
	.ascii	"\007\006"
	.zero	2,7
	.size	FIELD_SCAN8x8, 128

	.type	COEFF_COST8x8,@object   # @COEFF_COST8x8
	.globl	COEFF_COST8x8
	.p2align	4
COEFF_COST8x8:
	.asciz	"\003\003\003\003\002\002\002\002\002\002\002\002\001\001\001\001\001\001\001\001\001\001\001\001\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.zero	64,9
	.size	COEFF_COST8x8, 128

	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	diff64,@object          # @diff64
	.local	diff64
	.comm	diff64,256,16
	.type	intrapred_luma8x8.PredPel,@object # @intrapred_luma8x8.PredPel
	.local	intrapred_luma8x8.PredPel
	.comm	intrapred_luma8x8.PredPel,50,16
	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	cofAC8x8_chroma,@object # @cofAC8x8_chroma
	.comm	cofAC8x8_chroma,1152,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
