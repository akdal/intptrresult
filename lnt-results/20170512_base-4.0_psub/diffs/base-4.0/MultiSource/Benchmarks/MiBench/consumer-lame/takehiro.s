	.text
	.file	"takehiro.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4665736910537228288     # double 8206
	.text
	.globl	count_bits
	.p2align	4, 0x90
	.type	count_bits,@function
count_bits:                             # @count_bits
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rsi, %r13
	movl	$0, 4(%rsp)
	movl	12(%r14), %eax
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	divsd	ipow20(,%rax,8), %xmm0
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movsd	(%rdx,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	movl	$100000, %eax           # imm = 0x186A0
	ucomisd	%xmm0, %xmm1
	ja	.LBB0_35
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	movsd	8(%rdx,%rcx,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB0_35
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	movsd	16(%rdx,%rcx,8), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB0_35
# BB#4:                                 #   in Loop: Header=BB0_1 Depth=1
	movsd	24(%rdx,%rcx,8), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB0_35
# BB#5:                                 #   in Loop: Header=BB0_1 Depth=1
	movsd	32(%rdx,%rcx,8), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB0_35
# BB#6:                                 #   in Loop: Header=BB0_1 Depth=1
	movsd	40(%rdx,%rcx,8), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB0_35
# BB#7:                                 #   in Loop: Header=BB0_1 Depth=1
	addq	$6, %rcx
	cmpq	$576, %rcx              # imm = 0x240
	jl	.LBB0_1
# BB#8:
	cmpl	$0, 260(%rdi)
	je	.LBB0_10
# BB#9:
	movq	%rdx, %rdi
	movq	%r13, %rsi
	movq	%r14, %rdx
	callq	quantize_xrpow
	jmp	.LBB0_11
.LBB0_10:
	movq	%rdx, %rdi
	movq	%r13, %rsi
	movq	%r14, %rdx
	callq	quantize_xrpow_ISO
.LBB0_11:
	movl	24(%r14), %r8d
	cmpl	$2, %r8d
	jne	.LBB0_13
# BB#12:
	leaq	144(%r13), %r15
	leaq	4(%rsp), %r12
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	choose_table_short
	movl	%eax, 32(%r14)
	addq	$2304, %r13             # imm = 0x900
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	callq	choose_table_short
	movl	%eax, 36(%r14)
	movl	$288, 4(%r14)           # imm = 0x120
	movl	4(%rsp), %eax
	jmp	.LBB0_35
.LBB0_13:
	movl	$0, (%rsp)
	movl	$576, %eax              # imm = 0x240
	.p2align	4, 0x90
.LBB0_14:                               # =>This Inner Loop Header: Depth=1
	movq	%rax, %rcx
	cmpq	$2, %rcx
	jl	.LBB0_16
# BB#15:                                #   in Loop: Header=BB0_14 Depth=1
	leaq	-2(%rcx), %rax
	movl	-8(%r13,%rcx,4), %edx
	orl	-4(%r13,%rcx,4), %edx
	je	.LBB0_14
.LBB0_16:
	movl	%ecx, 8(%r14)
	xorl	%edi, %edi
	cmpl	$3, %ecx
	jle	.LBB0_17
# BB#18:                                # %.lr.ph.i
	movq	ht+784(%rip), %r9
	movq	%rcx, %r12
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB0_19:                               # =>This Inner Loop Header: Depth=1
	movl	-4(%r13,%r12,4), %ebp
	movl	-8(%r13,%r12,4), %ebx
	movl	%ebx, %esi
	orl	%ebp, %esi
	movl	-12(%r13,%r12,4), %edx
	movl	-16(%r13,%r12,4), %r11d
	movl	%edx, %eax
	orl	%r11d, %eax
	orl	%esi, %eax
	cmpl	$1, %eax
	ja	.LBB0_21
# BB#20:                                # %.thread.i
                                        #   in Loop: Header=BB0_19 Depth=1
	addq	$-4, %r12
	addl	%ebp, %edi
	leal	2(%rbp), %eax
	xorl	%esi, %esi
	testl	%ebx, %ebx
	setne	%sil
	cmovel	%ebp, %eax
	addl	%edi, %esi
	leal	4(%rax), %ebx
	xorl	%ebp, %ebp
	testl	%edx, %edx
	setne	%bpl
	cmovel	%eax, %ebx
	addl	%esi, %ebp
	leal	8(%rbx), %eax
	xorl	%edi, %edi
	testl	%r11d, %r11d
	setne	%dil
	cmovel	%ebx, %eax
	addl	%ebp, %edi
	cltq
	movzbl	(%r9,%rax), %eax
	addl	%eax, %r10d
	cmpq	$3, %r12
	jg	.LBB0_19
.LBB0_21:                               # %._crit_edge116.i
	movl	%edi, (%rsp)
	jmp	.LBB0_22
.LBB0_17:                               # %._crit_edge
	movl	%ecx, %r12d
	xorl	%r10d, %r10d
.LBB0_22:
	movl	%ecx, %eax
	subl	%r12d, %eax
	xorl	%edx, %edx
	cmpl	%eax, %r10d
	cmovlel	%r10d, %eax
	setge	%dl
	addl	%edi, %eax
	movl	%eax, (%rsp)
	movl	%edx, 72(%r14)
	movl	%eax, 88(%r14)
	movl	%r12d, 4(%r14)
	testl	%r12d, %r12d
	je	.LBB0_23
# BB#24:
	testl	%r8d, %r8d
	je	.LBB0_25
# BB#32:
	movabsq	$55834574855, %rax      # imm = 0xD00000007
	movq	%rax, 56(%r14)
	movl	scalefac_band+32(%rip), %ebp
	cmpl	%r12d, %ebp
	cmovgl	%r12d, %ebp
	jmp	.LBB0_33
.LBB0_23:                               # %.count_bits_long.exit_crit_edge
	xorl	%edx, %edx
	jmp	.LBB0_34
.LBB0_25:                               # %.preheader.i.preheader
	movl	$4, %eax
	.p2align	4, 0x90
.LBB0_26:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rcx
	leaq	4(%rcx), %rax
	cmpl	%r12d, scalefac_band(%rcx)
	jl	.LBB0_26
# BB#27:
	movslq	subdv_table(%rcx,%rcx), %rax
	leal	3(%rax), %edx
	addq	$2, %rax
	.p2align	4, 0x90
.LBB0_28:                               # =>This Inner Loop Header: Depth=1
	decl	%edx
	cmpl	%r12d, scalefac_band-4(,%rax,4)
	leaq	-1(%rax), %rax
	jg	.LBB0_28
# BB#29:
	leal	-2(%rdx), %esi
	movl	%esi, 56(%r14)
	movl	subdv_table+4(%rcx,%rcx), %ecx
	leal	1(%rcx), %esi
	addl	%edx, %ecx
	.p2align	4, 0x90
.LBB0_30:                               # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edx
	decl	%esi
	decl	%ecx
	cmpl	%r12d, scalefac_band(,%rdx,4)
	jg	.LBB0_30
# BB#31:
	movl	%esi, 60(%r14)
	movl	%eax, %eax
	movl	scalefac_band(,%rax,4), %ebp
	movslq	scalefac_band(,%rdx,4), %rbx
	leaq	(%r13,%rbx,4), %rdi
	movslq	%r12d, %rax
	leaq	(%r13,%rax,4), %rsi
	movq	%rsp, %rdx
	callq	choose_table
	movl	%eax, 40(%r14)
	movl	%ebx, %r12d
.LBB0_33:
	movslq	%ebp, %rax
	leaq	(%r13,%rax,4), %r15
	movq	%rsp, %rbx
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	choose_table
	movl	%eax, 32(%r14)
	movslq	%r12d, %rax
	leaq	(%r13,%rax,4), %rsi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	choose_table
	movl	%eax, 36(%r14)
	movl	(%rsp), %eax
	movl	4(%r14), %edx
	movl	8(%r14), %ecx
.LBB0_34:                               # %count_bits_long.exit
	movl	%eax, 4(%rsp)
	subl	%edx, %ecx
	shrl	$2, %ecx
	movl	%ecx, 8(%r14)
	shrl	%edx
	movl	%edx, 4(%r14)
.LBB0_35:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	count_bits, .Lfunc_end0-count_bits
	.cfi_endproc

	.p2align	4, 0x90
	.type	choose_table_short,@function
choose_table_short:                     # @choose_table_short
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
.Lcfi19:
	.cfi_offset %rbx, -56
.Lcfi20:
	.cfi_offset %r12, -48
.Lcfi21:
	.cfi_offset %r13, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	jae	.LBB1_60
# BB#1:                                 # %.lr.ph.i.preheader
	movq	%rdi, %r8
	notq	%r8
	addq	%rsi, %r8
	movl	%r8d, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB1_2
# BB#3:                                 # %.lr.ph.i.prol.preheader
	negq	%rdx
	xorl	%ecx, %ecx
	movq	%rdi, %rax
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ebx
	movl	4(%rax), %ebp
	cmpl	%ebx, %ecx
	cmovll	%ebx, %ecx
	addq	$8, %rax
	cmpl	%ebp, %ecx
	cmovll	%ebp, %ecx
	incq	%rdx
	jne	.LBB1_4
	jmp	.LBB1_5
.LBB1_2:
	xorl	%ecx, %ecx
	movq	%rdi, %rax
.LBB1_5:                                # %.lr.ph.i.prol.loopexit
	cmpq	$24, %r8
	jb	.LBB1_7
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	movl	4(%rax), %ebp
	cmpl	%edx, %ecx
	cmovll	%edx, %ecx
	cmpl	%ebp, %ecx
	cmovll	%ebp, %ecx
	movl	8(%rax), %edx
	cmpl	%edx, %ecx
	cmovll	%edx, %ecx
	movl	12(%rax), %edx
	cmpl	%edx, %ecx
	cmovll	%edx, %ecx
	movl	16(%rax), %edx
	cmpl	%edx, %ecx
	cmovll	%edx, %ecx
	movl	20(%rax), %edx
	cmpl	%edx, %ecx
	cmovll	%edx, %ecx
	movl	24(%rax), %edx
	cmpl	%edx, %ecx
	cmovll	%edx, %ecx
	movl	28(%rax), %edx
	cmpl	%edx, %ecx
	cmovll	%edx, %ecx
	addq	$32, %rax
	cmpq	%rsi, %rax
	jb	.LBB1_6
.LBB1_7:                                # %ix_max.exit
	cmpl	$8207, %ecx             # imm = 0x200F
	jl	.LBB1_9
# BB#8:
	movl	$100000, (%r12)         # imm = 0x186A0
	movl	$-1, %eax
	jmp	.LBB1_60
.LBB1_9:
	cmpl	$15, %ecx
	jg	.LBB1_37
# BB#10:
	testl	%ecx, %ecx
	je	.LBB1_11
# BB#12:
	movslq	%ecx, %rax
	movl	huf_tbl_noESC-4(,%rax,4), %eax
	leaq	(%rax,%rax,2), %rcx
	movl	$cb_esc_buf, %r9d
	movq	ht+16(,%rcx,8), %r10
	xorl	%r14d, %r14d
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB1_13:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi), %ebp
	movl	12(%rdi), %r11d
	movl	%ebp, %edx
	shll	$4, %edx
	xorl	%ebx, %ebx
	testl	%ebp, %ebp
	setne	%bl
	cmovel	%ebp, %edx
	cmpl	$1, %r11d
	sbbl	$-1, %r14d
	addl	%ebx, %r14d
	addl	%r11d, %edx
	movl	%edx, (%r9)
	movslq	%edx, %rdx
	movzbl	(%r10,%rdx), %r11d
	addl	%r8d, %r11d
	movl	4(%rdi), %ebx
	movl	16(%rdi), %r8d
	movl	%ebx, %edx
	shll	$4, %edx
	xorl	%ebp, %ebp
	testl	%ebx, %ebx
	setne	%bpl
	cmovel	%ebx, %edx
	xorl	%ebx, %ebx
	testl	%r8d, %r8d
	setne	%bl
	addl	%r14d, %ebx
	addl	%ebp, %ebx
	addl	%r8d, %edx
	movl	%edx, 4(%r9)
	movslq	%edx, %rcx
	movzbl	(%r10,%rcx), %r15d
	addl	%r11d, %r15d
	movl	8(%rdi), %ebp
	movl	20(%rdi), %r8d
	movl	%ebp, %ecx
	shll	$4, %ecx
	xorl	%edx, %edx
	testl	%ebp, %ebp
	setne	%dl
	cmovel	%ebp, %ecx
	xorl	%r14d, %r14d
	testl	%r8d, %r8d
	setne	%r14b
	addl	%r8d, %ecx
	movl	%ecx, 8(%r9)
	leaq	12(%r9), %r9
	addl	%ebx, %r14d
	addl	%edx, %r14d
	movslq	%ecx, %rcx
	movzbl	(%r10,%rcx), %r8d
	addl	%r15d, %r8d
	addq	$24, %rdi
	cmpq	%rsi, %rdi
	jb	.LBB1_13
# BB#14:                                # %count_bit_short_noESC.exit
	movl	%r14d, cb_esc_sign(%rip)
	movq	%r9, cb_esc_end(%rip)
	addl	%r14d, %r8d
	leal	-2(%rax), %edx
	cmpl	$11, %edx
	ja	.LBB1_36
# BB#15:                                # %count_bit_short_noESC.exit
	movl	%eax, %r10d
	jmpq	*.LJTI1_0(,%rdx,8)
.LBB1_16:
	leal	1(%rax), %r10d
	leaq	(%r10,%r10,2), %rcx
	movq	ht+16(,%rcx,8), %rdi
	movl	$cb_esc_buf, %ebp
	movl	$cb_esc_buf+4, %ecx
	cmpq	%rcx, %r9
	cmovaq	%r9, %rcx
	movl	$cb_esc_buf, %esi
	notq	%rsi
	addq	%rcx, %rsi
	movl	%esi, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB1_17
# BB#18:                                # %.prol.preheader123
	movl	$cb_esc_buf, %ebp
	negq	%rdx
	movl	%r14d, %ebx
	.p2align	4, 0x90
.LBB1_19:                               # =>This Inner Loop Header: Depth=1
	movslq	(%rbp), %rcx
	addq	$4, %rbp
	movzbl	(%rdi,%rcx), %ecx
	addl	%ecx, %ebx
	incq	%rdx
	jne	.LBB1_19
	jmp	.LBB1_20
.LBB1_37:
	addl	$-15, %ecx
	movl	$24, %ebp
	cmpl	%ecx, ht+580(%rip)
	movq	%r12, -16(%rsp)         # 8-byte Spill
	movq	%rsi, -8(%rsp)          # 8-byte Spill
	jge	.LBB1_39
# BB#38:
	movl	$25, %ebp
	cmpl	%ecx, ht+604(%rip)
	jge	.LBB1_39
# BB#61:
	movl	$26, %ebp
	cmpl	%ecx, ht+628(%rip)
	jge	.LBB1_39
# BB#62:
	movl	$27, %ebp
	cmpl	%ecx, ht+652(%rip)
	jge	.LBB1_39
# BB#63:
	movl	$28, %ebp
	cmpl	%ecx, ht+676(%rip)
	jge	.LBB1_39
# BB#64:
	movl	$29, %ebp
	cmpl	%ecx, ht+700(%rip)
	jge	.LBB1_39
# BB#65:
	movl	$30, %ebp
	cmpl	%ecx, ht+724(%rip)
	jge	.LBB1_39
# BB#66:
	movl	$31, %ebp
	cmpl	%ecx, ht+748(%rip)
	jge	.LBB1_39
# BB#67:
	movl	$24, %eax
	movl	$32, %ebp
	jmp	.LBB1_42
.LBB1_39:                               # %.lr.ph.preheader
	leal	-8(%rbp), %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	ht+4(,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB1_40:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%ecx, (%rdx)
	jge	.LBB1_42
# BB#41:                                #   in Loop: Header=BB1_40 Depth=1
	incq	%rax
	addq	$24, %rdx
	cmpq	$24, %rax
	jl	.LBB1_40
.LBB1_42:                               # %._crit_edge
	movslq	%eax, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movl	ht(,%rcx,8), %r9d
	movq	%rbp, -24(%rsp)         # 8-byte Spill
	movl	%ebp, %ecx
	leaq	(%rcx,%rcx,2), %rcx
	movl	ht(,%rcx,8), %r10d
	movq	ht+400(%rip), %rsi
	xorl	%r13d, %r13d
	movq	ht+592(%rip), %r12
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_43:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi), %ecx
	movl	12(%rdi), %r8d
	testl	%ecx, %ecx
	je	.LBB1_44
# BB#45:                                #   in Loop: Header=BB1_43 Depth=1
	incl	%r13d
	movl	%ecx, %r15d
	shll	$4, %r15d
	cmpl	$14, %ecx
	movl	$0, %ecx
	cmovgl	%r9d, %ecx
	movl	$0, %r14d
	cmovgl	%r10d, %r14d
	movl	$240, %edx
	cmovgl	%edx, %r15d
	addl	%ecx, %ebp
	addl	%r14d, %ebx
	testl	%r8d, %r8d
	jne	.LBB1_47
	jmp	.LBB1_48
	.p2align	4, 0x90
.LBB1_44:                               #   in Loop: Header=BB1_43 Depth=1
	xorl	%r15d, %r15d
	testl	%r8d, %r8d
	je	.LBB1_48
.LBB1_47:                               #   in Loop: Header=BB1_43 Depth=1
	incl	%r13d
	cmpl	$14, %r8d
	movl	$0, %r11d
	cmovgl	%r9d, %r11d
	movl	$0, %edx
	cmovgl	%r10d, %edx
	movl	$15, %ecx
	cmovgl	%ecx, %r8d
	addl	%r11d, %ebp
	addl	%edx, %ebx
	addl	%r8d, %r15d
.LBB1_48:                               #   in Loop: Header=BB1_43 Depth=1
	movslq	%r15d, %rdx
	movzbl	(%rsi,%rdx), %r14d
	addl	%ebp, %r14d
	movzbl	(%r12,%rdx), %r15d
	addl	%ebx, %r15d
	movl	4(%rdi), %edx
	movl	16(%rdi), %r8d
	testl	%edx, %edx
	je	.LBB1_49
# BB#50:                                #   in Loop: Header=BB1_43 Depth=1
	incl	%r13d
	movl	%edx, %ebx
	shll	$4, %ebx
	cmpl	$14, %edx
	movl	$0, %edx
	cmovgl	%r9d, %edx
	movl	$0, %ecx
	cmovgl	%r10d, %ecx
	movl	$240, %ebp
	cmovgl	%ebp, %ebx
	addl	%edx, %r14d
	addl	%ecx, %r15d
	testl	%r8d, %r8d
	jne	.LBB1_52
	jmp	.LBB1_53
	.p2align	4, 0x90
.LBB1_49:                               #   in Loop: Header=BB1_43 Depth=1
	xorl	%ebx, %ebx
	testl	%r8d, %r8d
	je	.LBB1_53
.LBB1_52:                               #   in Loop: Header=BB1_43 Depth=1
	incl	%r13d
	cmpl	$14, %r8d
	movl	$0, %ecx
	cmovgl	%r9d, %ecx
	movl	$0, %edx
	cmovgl	%r10d, %edx
	movl	$15, %ebp
	cmovgl	%ebp, %r8d
	addl	%ecx, %r14d
	addl	%edx, %r15d
	addl	%r8d, %ebx
.LBB1_53:                               #   in Loop: Header=BB1_43 Depth=1
	movslq	%ebx, %rcx
	movzbl	(%rsi,%rcx), %r11d
	addl	%r14d, %r11d
	movzbl	(%r12,%rcx), %r14d
	addl	%r15d, %r14d
	movl	8(%rdi), %ebp
	movl	20(%rdi), %r8d
	testl	%ebp, %ebp
	je	.LBB1_54
# BB#55:                                #   in Loop: Header=BB1_43 Depth=1
	incl	%r13d
	movl	%ebp, %edx
	shll	$4, %edx
	cmpl	$14, %ebp
	movl	$0, %ebp
	cmovgl	%r9d, %ebp
	movl	$0, %ecx
	cmovgl	%r10d, %ecx
	movl	$240, %ebx
	cmovgl	%ebx, %edx
	addl	%ebp, %r11d
	addl	%ecx, %r14d
	testl	%r8d, %r8d
	jne	.LBB1_57
	jmp	.LBB1_58
	.p2align	4, 0x90
.LBB1_54:                               #   in Loop: Header=BB1_43 Depth=1
	xorl	%edx, %edx
	testl	%r8d, %r8d
	je	.LBB1_58
.LBB1_57:                               #   in Loop: Header=BB1_43 Depth=1
	incl	%r13d
	cmpl	$14, %r8d
	movl	$0, %ecx
	cmovgl	%r9d, %ecx
	movl	$0, %ebp
	cmovgl	%r10d, %ebp
	movl	$15, %ebx
	cmovgl	%ebx, %r8d
	addl	%ecx, %r11d
	addl	%ebp, %r14d
	addl	%r8d, %edx
.LBB1_58:                               #   in Loop: Header=BB1_43 Depth=1
	movslq	%edx, %rcx
	movzbl	(%rsi,%rcx), %ebp
	addl	%r11d, %ebp
	movzbl	(%r12,%rcx), %ebx
	addl	%r14d, %ebx
	addq	$24, %rdi
	cmpq	-8(%rsp), %rdi          # 8-byte Folded Reload
	jb	.LBB1_43
# BB#59:                                # %count_bit_short_ESC.exit
	cmpl	%ebx, %ebp
	cmovgl	-24(%rsp), %eax         # 4-byte Folded Reload
	cmovgl	%ebx, %ebp
	addl	%r13d, %ebp
	movq	-16(%rsp), %rcx         # 8-byte Reload
	addl	%ebp, (%rcx)
	jmp	.LBB1_60
.LBB1_11:
	xorl	%eax, %eax
	jmp	.LBB1_60
.LBB1_29:
	movq	ht+376(%rip), %rax
	movl	$cb_esc_buf, %esi
	movl	$cb_esc_buf+4, %ecx
	cmpq	%rcx, %r9
	cmovaq	%r9, %rcx
	movl	$cb_esc_buf, %edx
	notq	%rdx
	addq	%rcx, %rdx
	movl	%edx, %edi
	shrl	$2, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB1_32
# BB#30:                                # %.prol.preheader134
	movl	$cb_esc_buf, %esi
	negq	%rdi
	.p2align	4, 0x90
.LBB1_31:                               # =>This Inner Loop Header: Depth=1
	movslq	(%rsi), %rcx
	addq	$4, %rsi
	movzbl	(%rax,%rcx), %ecx
	addl	%ecx, %r14d
	incq	%rdi
	jne	.LBB1_31
.LBB1_32:                               # %.prol.loopexit135
	cmpq	$12, %rdx
	jb	.LBB1_34
	.p2align	4, 0x90
.LBB1_33:                               # =>This Inner Loop Header: Depth=1
	movslq	(%rsi), %rcx
	movzbl	(%rax,%rcx), %ecx
	addl	%r14d, %ecx
	movslq	4(%rsi), %rdx
	movzbl	(%rax,%rdx), %edx
	addl	%ecx, %edx
	movslq	8(%rsi), %rcx
	movzbl	(%rax,%rcx), %ecx
	addl	%edx, %ecx
	movslq	12(%rsi), %rdx
	movzbl	(%rax,%rdx), %r14d
	addl	%ecx, %r14d
	addq	$16, %rsi
	cmpq	%r9, %rsi
	jb	.LBB1_33
.LBB1_34:                               # %count_bit_noESC2.exit
	xorl	%eax, %eax
	cmpl	%r14d, %r8d
	setg	%al
	leal	13(%rax,%rax), %eax
	jmp	.LBB1_35
.LBB1_17:
	movl	%r14d, %ebx
.LBB1_20:                               # %.prol.loopexit124
	cmpq	$12, %rsi
	jb	.LBB1_22
	.p2align	4, 0x90
.LBB1_21:                               # =>This Inner Loop Header: Depth=1
	movslq	(%rbp), %rcx
	movzbl	(%rdi,%rcx), %ecx
	addl	%ebx, %ecx
	movslq	4(%rbp), %rdx
	movzbl	(%rdi,%rdx), %edx
	addl	%ecx, %edx
	movslq	8(%rbp), %rcx
	movzbl	(%rdi,%rcx), %ecx
	addl	%edx, %ecx
	movslq	12(%rbp), %rdx
	movzbl	(%rdi,%rdx), %ebx
	addl	%ecx, %ebx
	addq	$16, %rbp
	cmpq	%r9, %rbp
	jb	.LBB1_21
.LBB1_22:                               # %count_bit_noESC2.exit70
	cmpl	%ebx, %r8d
	cmovgl	%r10d, %eax
	cmovgl	%ebx, %r8d
.LBB1_23:
	incl	%r10d
	leaq	(%r10,%r10,2), %rcx
	movq	ht+16(,%rcx,8), %rdi
	movl	$cb_esc_buf, %ebp
	movl	$cb_esc_buf+4, %ecx
	cmpq	%rcx, %r9
	cmovaq	%r9, %rcx
	movl	$cb_esc_buf, %edx
	notq	%rdx
	addq	%rcx, %rdx
	movl	%edx, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB1_26
# BB#24:                                # %.prol.preheader
	movl	$cb_esc_buf, %ebp
	negq	%rsi
	.p2align	4, 0x90
.LBB1_25:                               # =>This Inner Loop Header: Depth=1
	movslq	(%rbp), %rcx
	addq	$4, %rbp
	movzbl	(%rdi,%rcx), %ecx
	addl	%ecx, %r14d
	incq	%rsi
	jne	.LBB1_25
.LBB1_26:                               # %.prol.loopexit
	cmpq	$12, %rdx
	jb	.LBB1_28
	.p2align	4, 0x90
.LBB1_27:                               # =>This Inner Loop Header: Depth=1
	movslq	(%rbp), %rcx
	movzbl	(%rdi,%rcx), %ecx
	addl	%r14d, %ecx
	movslq	4(%rbp), %rdx
	movzbl	(%rdi,%rdx), %edx
	addl	%ecx, %edx
	movslq	8(%rbp), %rcx
	movzbl	(%rdi,%rcx), %ecx
	addl	%edx, %ecx
	movslq	12(%rbp), %rdx
	movzbl	(%rdi,%rdx), %r14d
	addl	%ecx, %r14d
	addq	$16, %rbp
	cmpq	%r9, %rbp
	jb	.LBB1_27
.LBB1_28:                               # %count_bit_noESC2.exit67
	cmpl	%r14d, %r8d
	cmovgl	%r10d, %eax
.LBB1_35:
	cmovgl	%r14d, %r8d
.LBB1_36:
	addl	%r8d, (%r12)
.LBB1_60:                               # %.thread84
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	choose_table_short, .Lfunc_end1-choose_table_short
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_23
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_23
	.quad	.LBB1_36
	.quad	.LBB1_16
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_16
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_29

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	100000                  # 0x186a0
	.long	100000                  # 0x186a0
	.long	100000                  # 0x186a0
	.long	100000                  # 0x186a0
	.text
	.globl	best_huffman_divide
	.p2align	4, 0x90
	.type	best_huffman_divide,@function
best_huffman_divide:                    # @best_huffman_divide
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	subq	$376, %rsp              # imm = 0x178
.Lcfi31:
	.cfi_def_cfa_offset 432
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	112(%rbx), %rax
	movq	%rax, 144(%rsp)
	movups	96(%rbx), %xmm0
	movaps	%xmm0, 128(%rsp)
	movups	80(%rbx), %xmm0
	movaps	%xmm0, 112(%rsp)
	movups	64(%rbx), %xmm0
	movaps	%xmm0, 96(%rsp)
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	32(%rbx), %xmm2
	movups	48(%rbx), %xmm3
	movaps	%xmm3, 80(%rsp)
	movaps	%xmm2, 64(%rsp)
	movaps	%xmm1, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movl	36(%rsp), %eax
	movl	108(%rsp), %r13d
	addl	%eax, %eax
	addl	120(%rsp), %r13d
	movl	%eax, 8(%rsp)           # 4-byte Spill
	cltq
	leaq	(%r14,%rax,4), %r15
	leaq	168(%rsp), %r12
	movl	$2, %ebp
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movslq	scalefac_band(,%rbp,4), %rax
	cmpl	8(%rsp), %eax           # 4-byte Folded Reload
	jg	.LBB2_2
# BB#16:                                #   in Loop: Header=BB2_1 Depth=1
	movl	%r13d, (%r12)
	leaq	(%r14,%rax,4), %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	choose_table
	movl	%eax, 272(%rsp,%rbp,4)
	incq	%rbp
	addq	$4, %r12
	cmpq	$23, %rbp
	jl	.LBB2_1
.LBB2_2:                                # %.preheader58
	cmpl	$24, %ebp
	jg	.LBB2_15
# BB#3:                                 # %.lr.ph.preheader
	movslq	%ebp, %rax
	movl	$25, %ecx
	subq	%rax, %rcx
	cmpq	$7, %rcx
	jbe	.LBB2_14
# BB#4:                                 # %min.iters.checked
	movq	%rcx, %r9
	andq	$-8, %r9
	movq	%rcx, %rsi
	andq	$-8, %rsi
	je	.LBB2_14
# BB#5:                                 # %vector.body.preheader
	leaq	-8(%rsi), %r8
	movl	%r8d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB2_6
# BB#7:                                 # %vector.body.prol.preheader
	leaq	176(%rsp,%rax,4), %rdx
	negq	%rdi
	xorl	%ebp, %ebp
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [100000,100000,100000,100000]
	.p2align	4, 0x90
.LBB2_8:                                # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -16(%rdx,%rbp,4)
	movups	%xmm0, (%rdx,%rbp,4)
	addq	$8, %rbp
	incq	%rdi
	jne	.LBB2_8
	jmp	.LBB2_9
.LBB2_6:
	xorl	%ebp, %ebp
.LBB2_9:                                # %vector.body.prol.loopexit
	cmpq	$24, %r8
	jb	.LBB2_12
# BB#10:                                # %vector.body.preheader.new
	movq	%rsi, %rdi
	subq	%rbp, %rdi
	addq	%rax, %rbp
	leaq	272(%rsp,%rbp,4), %rbp
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [100000,100000,100000,100000]
	.p2align	4, 0x90
.LBB2_11:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -112(%rbp)
	movups	%xmm0, -96(%rbp)
	movups	%xmm0, -80(%rbp)
	movups	%xmm0, -64(%rbp)
	movups	%xmm0, -48(%rbp)
	movups	%xmm0, -32(%rbp)
	movups	%xmm0, -16(%rbp)
	movups	%xmm0, (%rbp)
	subq	$-128, %rbp
	addq	$-32, %rdi
	jne	.LBB2_11
.LBB2_12:                               # %middle.block
	cmpq	%rsi, %rcx
	je	.LBB2_15
# BB#13:
	addq	%r9, %rax
	.p2align	4, 0x90
.LBB2_14:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$100000, 160(%rsp,%rax,4) # imm = 0x186A0
	incq	%rax
	cmpq	$25, %rax
	jne	.LBB2_14
.LBB2_15:                               # %.preheader57
	xorl	%r12d, %r12d
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB2_18:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_21 Depth 2
	movslq	scalefac_band+4(,%r12,4), %rax
	cmpl	8(%rsp), %eax           # 4-byte Folded Reload
	jg	.LBB2_26
# BB#19:                                #   in Loop: Header=BB2_18 Depth=1
	movl	%r12d, 88(%rsp)
	movl	$0, 12(%rsp)
	leaq	(%r14,%rax,4), %r13
	movq	%r14, %rdi
	movq	%r13, %rsi
	leaq	12(%rsp), %rdx
	callq	choose_table
	movl	%eax, 64(%rsp)
	movl	(%rbx), %eax
	movl	12(%rsp), %r15d
	cmpl	%r15d, %eax
	jl	.LBB2_26
# BB#20:                                # %.preheader
                                        #   in Loop: Header=BB2_18 Depth=1
	incq	%r12
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	jmp	.LBB2_21
	.p2align	4, 0x90
.LBB2_25:                               # %._crit_edge
                                        #   in Loop: Header=BB2_21 Depth=2
	incq	%r12
	movl	(%rbx), %eax
	addq	$4, %rbp
.LBB2_21:                               #   Parent Loop BB2_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	160(%rsp,%rbp), %ecx
	addl	%r15d, %ecx
	movl	%ecx, 32(%rsp)
	cmpl	%ecx, %eax
	jl	.LBB2_24
# BB#22:                                #   in Loop: Header=BB2_21 Depth=2
	movslq	scalefac_band(%rbp), %rax
	leaq	(%r14,%rax,4), %rsi
	movq	%r13, %rdi
	leaq	32(%rsp), %rdx
	callq	choose_table
	movl	%eax, 68(%rsp)
	movl	(%rbx), %eax
	cmpl	32(%rsp), %eax
	jl	.LBB2_24
# BB#23:                                #   in Loop: Header=BB2_21 Depth=2
	movl	%r12d, 92(%rsp)
	movl	272(%rsp,%rbp), %eax
	movl	%eax, 72(%rsp)
	movq	144(%rsp), %rax
	movq	%rax, 112(%rbx)
	movaps	128(%rsp), %xmm0
	movups	%xmm0, 96(%rbx)
	movaps	112(%rsp), %xmm0
	movups	%xmm0, 80(%rbx)
	movaps	96(%rsp), %xmm0
	movups	%xmm0, 64(%rbx)
	movaps	32(%rsp), %xmm0
	movaps	48(%rsp), %xmm1
	movaps	64(%rsp), %xmm2
	movaps	80(%rsp), %xmm3
	movups	%xmm3, 48(%rbx)
	movups	%xmm2, 32(%rbx)
	movups	%xmm1, 16(%rbx)
	movups	%xmm0, (%rbx)
.LBB2_24:                               #   in Loop: Header=BB2_21 Depth=2
	cmpq	$7, %r12
	jne	.LBB2_25
# BB#17:                                # %.loopexit
                                        #   in Loop: Header=BB2_18 Depth=1
	movq	16(%rsp), %rbp          # 8-byte Reload
	addq	$4, %rbp
	movq	24(%rsp), %r12          # 8-byte Reload
	cmpq	$15, %r12
	jle	.LBB2_18
.LBB2_26:
	addq	$376, %rsp              # imm = 0x178
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	best_huffman_divide, .Lfunc_end2-best_huffman_divide
	.cfi_endproc

	.p2align	4, 0x90
	.type	choose_table,@function
choose_table:                           # @choose_table
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
.Lcfi44:
	.cfi_offset %rbx, -56
.Lcfi45:
	.cfi_offset %r12, -48
.Lcfi46:
	.cfi_offset %r13, -40
.Lcfi47:
	.cfi_offset %r14, -32
.Lcfi48:
	.cfi_offset %r15, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	jae	.LBB3_50
# BB#1:                                 # %.lr.ph.i.preheader
	movq	%rdi, %r8
	notq	%r8
	leaq	(%rsi,%r8), %r9
	movl	%r9d, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB3_2
# BB#3:                                 # %.lr.ph.i.prol.preheader
	negq	%rdx
	xorl	%ecx, %ecx
	movq	%rdi, %rax
	.p2align	4, 0x90
.LBB3_4:                                # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ebx
	movl	4(%rax), %ebp
	cmpl	%ebx, %ecx
	cmovll	%ebx, %ecx
	addq	$8, %rax
	cmpl	%ebp, %ecx
	cmovll	%ebp, %ecx
	incq	%rdx
	jne	.LBB3_4
	jmp	.LBB3_5
.LBB3_2:
	xorl	%ecx, %ecx
	movq	%rdi, %rax
.LBB3_5:                                # %.lr.ph.i.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB3_7
	.p2align	4, 0x90
.LBB3_6:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	movl	4(%rax), %ebp
	cmpl	%edx, %ecx
	cmovll	%edx, %ecx
	cmpl	%ebp, %ecx
	cmovll	%ebp, %ecx
	movl	8(%rax), %edx
	cmpl	%edx, %ecx
	cmovll	%edx, %ecx
	movl	12(%rax), %edx
	cmpl	%edx, %ecx
	cmovll	%edx, %ecx
	movl	16(%rax), %edx
	cmpl	%edx, %ecx
	cmovll	%edx, %ecx
	movl	20(%rax), %edx
	cmpl	%edx, %ecx
	cmovll	%edx, %ecx
	movl	24(%rax), %edx
	cmpl	%edx, %ecx
	cmovll	%edx, %ecx
	movl	28(%rax), %edx
	cmpl	%edx, %ecx
	cmovll	%edx, %ecx
	addq	$32, %rax
	cmpq	%rsi, %rax
	jb	.LBB3_6
.LBB3_7:                                # %ix_max.exit
	cmpl	$8207, %ecx             # imm = 0x200F
	jl	.LBB3_9
# BB#8:
	movl	$100000, (%r15)         # imm = 0x186A0
	movl	$-1, %eax
	jmp	.LBB3_50
.LBB3_9:
	cmpl	$15, %ecx
	jg	.LBB3_37
# BB#10:
	testl	%ecx, %ecx
	je	.LBB3_11
# BB#12:
	movslq	%ecx, %rax
	movl	huf_tbl_noESC-4(,%rax,4), %eax
	leaq	(%rax,%rax,2), %rcx
	movq	ht+16(,%rcx,8), %r10
	leaq	8(%rdi), %r9
	cmpq	%rsi, %r9
	cmovbq	%rsi, %r9
	addq	%r8, %r9
	shrq	$3, %r9
	xorl	%edx, %edx
	xorl	%r14d, %r14d
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB3_13:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rdx,2), %ecx
	movl	4(%rdi,%rdx,2), %r11d
	movl	%ecx, %ebx
	shll	$4, %ebx
	xorl	%ebp, %ebp
	testl	%ecx, %ecx
	setne	%bpl
	cmovel	%ecx, %ebx
	addl	%r14d, %ebp
	xorl	%r14d, %r14d
	testl	%r11d, %r11d
	setne	%r14b
	addl	%ebp, %r14d
	addl	%r11d, %ebx
	movl	%ebx, cb_esc_buf(%rdx)
	movslq	%ebx, %rcx
	movzbl	(%r10,%rcx), %ecx
	addl	%ecx, %r8d
	leaq	8(%rdi,%rdx,2), %rcx
	addq	$4, %rdx
	cmpq	%rsi, %rcx
	jb	.LBB3_13
# BB#14:                                # %count_bit_noESC.exit
	leaq	cb_esc_buf+4(,%r9,4), %rsi
	movl	%r14d, cb_esc_sign(%rip)
	movq	%rsi, cb_esc_end(%rip)
	addl	%r14d, %r8d
	leal	-2(%rax), %edx
	cmpl	$11, %edx
	ja	.LBB3_36
# BB#15:                                # %count_bit_noESC.exit
	incq	%r9
	movl	%eax, %r10d
	jmpq	*.LJTI3_0(,%rdx,8)
.LBB3_16:
	leal	1(%rax), %r10d
	leaq	(%r10,%r10,2), %rcx
	movq	ht+16(,%rcx,8), %rbp
	movl	$cb_esc_buf, %ebx
	leaq	cb_esc_buf(,%r9,4), %rcx
	movl	$cb_esc_buf+4, %edx
	cmpq	%rdx, %rcx
	cmovaq	%rcx, %rdx
	movl	$cb_esc_buf, %r11d
	notq	%r11
	addq	%rdx, %r11
	movl	%r11d, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB3_17
# BB#18:                                # %.prol.preheader117
	movl	$cb_esc_buf, %ebx
	negq	%rdx
	movl	%r14d, %edi
	.p2align	4, 0x90
.LBB3_19:                               # =>This Inner Loop Header: Depth=1
	movslq	(%rbx), %rcx
	addq	$4, %rbx
	movzbl	(%rbp,%rcx), %ecx
	addl	%ecx, %edi
	incq	%rdx
	jne	.LBB3_19
	jmp	.LBB3_20
.LBB3_37:
	movq	%r15, -8(%rsp)          # 8-byte Spill
	addl	$-15, %ecx
	movl	$24, %ebx
	cmpl	%ecx, ht+580(%rip)
	jge	.LBB3_39
# BB#38:
	movl	$25, %ebx
	cmpl	%ecx, ht+604(%rip)
	jge	.LBB3_39
# BB#51:
	movl	$26, %ebx
	cmpl	%ecx, ht+628(%rip)
	jge	.LBB3_39
# BB#52:
	movl	$27, %ebx
	cmpl	%ecx, ht+652(%rip)
	jge	.LBB3_39
# BB#53:
	movl	$28, %ebx
	cmpl	%ecx, ht+676(%rip)
	jge	.LBB3_39
# BB#54:
	movl	$29, %ebx
	cmpl	%ecx, ht+700(%rip)
	jge	.LBB3_39
# BB#55:
	movl	$30, %ebx
	cmpl	%ecx, ht+724(%rip)
	jge	.LBB3_39
# BB#56:
	movl	$31, %ebx
	cmpl	%ecx, ht+748(%rip)
	jge	.LBB3_39
# BB#57:
	movl	$24, %eax
	movl	$32, %ebx
	jmp	.LBB3_42
.LBB3_39:                               # %.lr.ph.preheader
	leal	-8(%rbx), %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	ht+4(,%rdx,8), %rbp
	.p2align	4, 0x90
.LBB3_40:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%ecx, (%rbp)
	jge	.LBB3_42
# BB#41:                                #   in Loop: Header=BB3_40 Depth=1
	incq	%rax
	addq	$24, %rbp
	cmpq	$24, %rax
	jl	.LBB3_40
.LBB3_42:                               # %.lr.ph.i64
	movslq	%eax, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movl	ht(,%rcx,8), %r9d
	movq	%rbx, -16(%rsp)         # 8-byte Spill
	movl	%ebx, %ecx
	leaq	(%rcx,%rcx,2), %rcx
	movl	ht(,%rcx,8), %r10d
	movq	ht+400(%rip), %r14
	xorl	%ebp, %ebp
	movq	ht+592(%rip), %r13
	xorl	%ecx, %ecx
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB3_43:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi), %ebx
	movl	4(%rdi), %r8d
	testl	%ebx, %ebx
	je	.LBB3_44
# BB#45:                                #   in Loop: Header=BB3_43 Depth=1
	incl	%r11d
	movl	%ebx, %r12d
	shll	$4, %r12d
	cmpl	$14, %ebx
	movl	$0, %ebx
	cmovgl	%r9d, %ebx
	movl	$0, %r15d
	cmovgl	%r10d, %r15d
	movl	$240, %edx
	cmovgl	%edx, %r12d
	addl	%ebx, %ecx
	addl	%r15d, %ebp
	testl	%r8d, %r8d
	jne	.LBB3_47
	jmp	.LBB3_48
	.p2align	4, 0x90
.LBB3_44:                               #   in Loop: Header=BB3_43 Depth=1
	xorl	%r12d, %r12d
	testl	%r8d, %r8d
	je	.LBB3_48
.LBB3_47:                               #   in Loop: Header=BB3_43 Depth=1
	incl	%r11d
	cmpl	$14, %r8d
	movl	$0, %ebx
	cmovgl	%r9d, %ebx
	movl	$0, %r15d
	cmovgl	%r10d, %r15d
	movl	$15, %edx
	cmovgl	%edx, %r8d
	addl	%ebx, %ecx
	addl	%r15d, %ebp
	addl	%r8d, %r12d
.LBB3_48:                               #   in Loop: Header=BB3_43 Depth=1
	movl	%ebp, %edx
	movl	%ecx, %ebp
	movslq	%r12d, %rbx
	movzbl	(%r14,%rbx), %ecx
	addl	%ebp, %ecx
	movzbl	(%r13,%rbx), %ebp
	addl	%edx, %ebp
	addq	$8, %rdi
	cmpq	%rsi, %rdi
	jb	.LBB3_43
# BB#49:                                # %count_bit_ESC.exit
	cmpl	%ebp, %ecx
	cmovgl	-16(%rsp), %eax         # 4-byte Folded Reload
	cmovgl	%ebp, %ecx
	movq	-8(%rsp), %rdx          # 8-byte Reload
	addl	(%rdx), %r11d
	addl	%ecx, %r11d
	movl	%r11d, (%rdx)
	jmp	.LBB3_50
.LBB3_11:
	xorl	%eax, %eax
	jmp	.LBB3_50
.LBB3_29:
	movq	ht+376(%rip), %rax
	movl	$cb_esc_buf, %edi
	leaq	cb_esc_buf(,%r9,4), %rcx
	movl	$cb_esc_buf+4, %edx
	cmpq	%rdx, %rcx
	cmovaq	%rcx, %rdx
	movl	$cb_esc_buf, %ebp
	notq	%rbp
	addq	%rdx, %rbp
	movl	%ebp, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB3_32
# BB#30:                                # %.prol.preheader130
	movl	$cb_esc_buf, %edi
	negq	%rdx
	.p2align	4, 0x90
.LBB3_31:                               # =>This Inner Loop Header: Depth=1
	movslq	(%rdi), %rcx
	addq	$4, %rdi
	movzbl	(%rax,%rcx), %ecx
	addl	%ecx, %r14d
	incq	%rdx
	jne	.LBB3_31
.LBB3_32:                               # %.prol.loopexit131
	cmpq	$12, %rbp
	jb	.LBB3_34
	.p2align	4, 0x90
.LBB3_33:                               # =>This Inner Loop Header: Depth=1
	movslq	(%rdi), %rcx
	movzbl	(%rax,%rcx), %ecx
	addl	%r14d, %ecx
	movslq	4(%rdi), %rdx
	movzbl	(%rax,%rdx), %edx
	addl	%ecx, %edx
	movslq	8(%rdi), %rcx
	movzbl	(%rax,%rcx), %ecx
	addl	%edx, %ecx
	movslq	12(%rdi), %rdx
	movzbl	(%rax,%rdx), %r14d
	addl	%ecx, %r14d
	addq	$16, %rdi
	cmpq	%rsi, %rdi
	jb	.LBB3_33
.LBB3_34:                               # %count_bit_noESC2.exit
	xorl	%eax, %eax
	cmpl	%r14d, %r8d
	setg	%al
	leal	13(%rax,%rax), %eax
	jmp	.LBB3_35
.LBB3_17:
	movl	%r14d, %edi
.LBB3_20:                               # %.prol.loopexit118
	cmpq	$12, %r11
	jb	.LBB3_22
	.p2align	4, 0x90
.LBB3_21:                               # =>This Inner Loop Header: Depth=1
	movslq	(%rbx), %rcx
	movzbl	(%rbp,%rcx), %ecx
	addl	%edi, %ecx
	movslq	4(%rbx), %rdx
	movzbl	(%rbp,%rdx), %edx
	addl	%ecx, %edx
	movslq	8(%rbx), %rcx
	movzbl	(%rbp,%rcx), %ecx
	addl	%edx, %ecx
	movslq	12(%rbx), %rdx
	movzbl	(%rbp,%rdx), %edi
	addl	%ecx, %edi
	addq	$16, %rbx
	cmpq	%rsi, %rbx
	jb	.LBB3_21
.LBB3_22:                               # %count_bit_noESC2.exit72
	cmpl	%edi, %r8d
	cmovgl	%r10d, %eax
	cmovgl	%edi, %r8d
.LBB3_23:
	incl	%r10d
	leaq	(%r10,%r10,2), %rcx
	movq	ht+16(,%rcx,8), %rbp
	movl	$cb_esc_buf, %ebx
	leaq	cb_esc_buf(,%r9,4), %rcx
	movl	$cb_esc_buf+4, %edi
	cmpq	%rdi, %rcx
	cmovaq	%rcx, %rdi
	movl	$cb_esc_buf, %edx
	notq	%rdx
	addq	%rdi, %rdx
	movl	%edx, %edi
	shrl	$2, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB3_26
# BB#24:                                # %.prol.preheader
	movl	$cb_esc_buf, %ebx
	negq	%rdi
	.p2align	4, 0x90
.LBB3_25:                               # =>This Inner Loop Header: Depth=1
	movslq	(%rbx), %rcx
	addq	$4, %rbx
	movzbl	(%rbp,%rcx), %ecx
	addl	%ecx, %r14d
	incq	%rdi
	jne	.LBB3_25
.LBB3_26:                               # %.prol.loopexit
	cmpq	$12, %rdx
	jb	.LBB3_28
	.p2align	4, 0x90
.LBB3_27:                               # =>This Inner Loop Header: Depth=1
	movslq	(%rbx), %rcx
	movzbl	(%rbp,%rcx), %ecx
	addl	%r14d, %ecx
	movslq	4(%rbx), %rdx
	movzbl	(%rbp,%rdx), %edx
	addl	%ecx, %edx
	movslq	8(%rbx), %rcx
	movzbl	(%rbp,%rcx), %ecx
	addl	%edx, %ecx
	movslq	12(%rbx), %rdx
	movzbl	(%rbp,%rdx), %r14d
	addl	%ecx, %r14d
	addq	$16, %rbx
	cmpq	%rsi, %rbx
	jb	.LBB3_27
.LBB3_28:                               # %count_bit_noESC2.exit69
	cmpl	%r14d, %r8d
	cmovgl	%r10d, %eax
.LBB3_35:
	cmovgl	%r14d, %r8d
.LBB3_36:
	addl	%r8d, (%r15)
.LBB3_50:                               # %.thread78
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	choose_table, .Lfunc_end3-choose_table
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_23
	.quad	.LBB3_36
	.quad	.LBB3_36
	.quad	.LBB3_23
	.quad	.LBB3_36
	.quad	.LBB3_16
	.quad	.LBB3_36
	.quad	.LBB3_36
	.quad	.LBB3_16
	.quad	.LBB3_36
	.quad	.LBB3_36
	.quad	.LBB3_29

	.text
	.globl	best_scalefac_store
	.p2align	4, 0x90
	.type	best_scalefac_store,@function
best_scalefac_store:                    # @best_scalefac_store
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi54:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi56:
	.cfi_def_cfa_offset 96
.Lcfi57:
	.cfi_offset %rbx, -56
.Lcfi58:
	.cfi_offset %r12, -48
.Lcfi59:
	.cfi_offset %r13, -40
.Lcfi60:
	.cfi_offset %r14, -32
.Lcfi61:
	.cfi_offset %r15, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r12
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movl	%esi, 20(%rsp)          # 4-byte Spill
	movslq	%esi, %rsi
	movslq	%edx, %rax
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	imulq	$240, %rsi, %r11
	addq	%r12, %r11
	movq	%rax, (%rsp)            # 8-byte Spill
	imulq	$120, %rax, %r15
	leaq	128(%r15,%r11), %r10
	movl	128(%r15,%r11), %r9d
	testl	%r9d, %r9d
	je	.LBB4_11
# BB#1:                                 # %.lr.ph190.preheader
	movq	8(%rsp), %rdi           # 8-byte Reload
	leaq	(%rdi,%rdi,8), %rdx
	shlq	$9, %rdx
	addq	%rcx, %rdx
	movq	(%rsp), %rsi            # 8-byte Reload
	leaq	(%rsi,%rsi,8), %rax
	shlq	$8, %rax
	addq	%rdx, %rax
	xorl	%ebx, %ebx
	imulq	$488, %rdi, %rdx        # imm = 0x1E8
	addq	%r14, %rdx
	imulq	$244, %rsi, %rbp
	addq	%rdx, %rbp
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph190
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_6 Depth 2
	cmpl	$0, (%rbp,%rbx,4)
	jle	.LBB4_3
# BB#4:                                 #   in Loop: Header=BB4_2 Depth=1
	movl	scalefac_band(,%rbx,4), %edx
	movslq	scalefac_band+4(,%rbx,4), %rsi
	cmpl	%esi, %edx
	jge	.LBB4_8
# BB#5:                                 # %.lr.ph183.preheader
                                        #   in Loop: Header=BB4_2 Depth=1
	movslq	%edx, %rdx
	.p2align	4, 0x90
.LBB4_6:                                # %.lr.ph183
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, (%rax,%rdx,4)
	jne	.LBB4_8
# BB#7:                                 #   in Loop: Header=BB4_6 Depth=2
	incq	%rdx
	cmpq	%rsi, %rdx
	jl	.LBB4_6
.LBB4_8:                                # %._crit_edge184
                                        #   in Loop: Header=BB4_2 Depth=1
	leaq	1(%rbx), %rdi
	cmpl	%esi, %edx
	jne	.LBB4_10
# BB#9:                                 #   in Loop: Header=BB4_2 Depth=1
	leaq	(%rbp,%rbx,4), %rdx
	movl	$0, (%rdx)
	movl	(%r10), %r9d
	jmp	.LBB4_10
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph190._crit_edge
                                        #   in Loop: Header=BB4_2 Depth=1
	incq	%rbx
	movq	%rbx, %rdi
.LBB4_10:                               #   in Loop: Header=BB4_2 Depth=1
	movl	%r9d, %edx
	cmpq	%rdx, %rdi
	movq	%rdi, %rbx
	jb	.LBB4_2
.LBB4_11:                               # %.preheader153
	leaq	48(%r15,%r11), %r8
	movslq	132(%r15,%r11), %rax
	cmpq	$12, %rax
	jge	.LBB4_148
# BB#12:                                # %.lr.ph177
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movq	%r8, %r15
	movq	%r14, %rsi
	movq	%r12, %r14
	movq	8(%rsp), %rbp           # 8-byte Reload
	leaq	(%rbp,%rbp,8), %rdx
	shlq	$9, %rdx
	addq	%rdx, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	leaq	(%rdi,%rdi,8), %r9
	shlq	$8, %r9
	leaq	(%rcx,%r9), %r12
	imulq	$488, %rbp, %rdx        # imm = 0x1E8
	movq	%rsi, %r8
	addq	%rsi, %rdx
	imulq	$244, %rdi, %rdi
	addq	%rdx, %rdi
	.p2align	4, 0x90
.LBB4_13:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_17 Depth 2
	leaq	(%rax,%rax,2), %rbp
	cmpl	$0, 88(%rdi,%rbp,4)
	jle	.LBB4_14
# BB#15:                                #   in Loop: Header=BB4_13 Depth=1
	movl	scalefac_band+92(,%rax,4), %esi
	movslq	scalefac_band+96(,%rax,4), %rdx
	cmpl	%edx, %esi
	jge	.LBB4_19
# BB#16:                                # %.lr.ph170.preheader
                                        #   in Loop: Header=BB4_13 Depth=1
	movslq	%esi, %rsi
	leaq	(%rsi,%rsi,2), %rbx
	leaq	(%r12,%rbx,4), %r13
	.p2align	4, 0x90
.LBB4_17:                               # %.lr.ph170
                                        #   Parent Loop BB4_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, (%r13)
	jne	.LBB4_19
# BB#18:                                #   in Loop: Header=BB4_17 Depth=2
	incq	%rsi
	addq	$12, %r13
	cmpq	%rdx, %rsi
	jl	.LBB4_17
.LBB4_19:                               # %._crit_edge171
                                        #   in Loop: Header=BB4_13 Depth=1
	incq	%rax
	cmpl	%edx, %esi
	jne	.LBB4_21
# BB#20:                                #   in Loop: Header=BB4_13 Depth=1
	leaq	88(%rdi,%rbp,4), %rdx
	movl	$0, (%rdx)
	cmpq	$12, %rax
	jne	.LBB4_13
	jmp	.LBB4_22
	.p2align	4, 0x90
.LBB4_14:                               # %._crit_edge230
                                        #   in Loop: Header=BB4_13 Depth=1
	incq	%rax
.LBB4_21:                               #   in Loop: Header=BB4_13 Depth=1
	cmpq	$12, %rax
	jne	.LBB4_13
.LBB4_22:                               # %._crit_edge178
	movslq	84(%r15), %rax
	cmpq	$12, %rax
	movq	%r14, %r12
	movq	%r8, %r14
	movq	%r15, %r8
	movq	32(%rsp), %r15          # 8-byte Reload
	jge	.LBB4_148
# BB#23:                                # %.lr.ph177.1
	leaq	4(%r9,%rcx), %r13
	.p2align	4, 0x90
.LBB4_24:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_132 Depth 2
	leaq	(%rax,%rax,2), %rbp
	cmpl	$0, 92(%rdi,%rbp,4)
	jle	.LBB4_25
# BB#130:                               #   in Loop: Header=BB4_24 Depth=1
	movl	scalefac_band+92(,%rax,4), %esi
	movslq	scalefac_band+96(,%rax,4), %rdx
	cmpl	%edx, %esi
	jge	.LBB4_134
# BB#131:                               # %.lr.ph170.preheader.1
                                        #   in Loop: Header=BB4_24 Depth=1
	movslq	%esi, %rsi
	leaq	(%rsi,%rsi,2), %rbx
	leaq	(%r13,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB4_132:                              # %.lr.ph170.1
                                        #   Parent Loop BB4_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, (%rbx)
	jne	.LBB4_134
# BB#133:                               #   in Loop: Header=BB4_132 Depth=2
	incq	%rsi
	addq	$12, %rbx
	cmpq	%rdx, %rsi
	jl	.LBB4_132
.LBB4_134:                              # %._crit_edge171.1
                                        #   in Loop: Header=BB4_24 Depth=1
	incq	%rax
	cmpl	%edx, %esi
	jne	.LBB4_136
# BB#135:                               #   in Loop: Header=BB4_24 Depth=1
	leaq	92(%rdi,%rbp,4), %rdx
	movl	$0, (%rdx)
	cmpq	$12, %rax
	jne	.LBB4_24
	jmp	.LBB4_137
	.p2align	4, 0x90
.LBB4_25:                               # %._crit_edge229
                                        #   in Loop: Header=BB4_24 Depth=1
	incq	%rax
.LBB4_136:                              #   in Loop: Header=BB4_24 Depth=1
	cmpq	$12, %rax
	jne	.LBB4_24
.LBB4_137:                              # %._crit_edge178.1
	movslq	84(%r8), %rax
	cmpq	$11, %rax
	jg	.LBB4_148
# BB#138:                               # %.lr.ph177.2
	leaq	8(%r9,%rcx), %rcx
	.p2align	4, 0x90
.LBB4_139:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_143 Depth 2
	leaq	(%rax,%rax,2), %rbp
	cmpl	$0, 96(%rdi,%rbp,4)
	jle	.LBB4_140
# BB#141:                               #   in Loop: Header=BB4_139 Depth=1
	movl	scalefac_band+92(,%rax,4), %esi
	movslq	scalefac_band+96(,%rax,4), %rdx
	cmpl	%edx, %esi
	jge	.LBB4_145
# BB#142:                               # %.lr.ph170.preheader.2
                                        #   in Loop: Header=BB4_139 Depth=1
	movslq	%esi, %rsi
	leaq	(%rsi,%rsi,2), %rbx
	leaq	(%rcx,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB4_143:                              # %.lr.ph170.2
                                        #   Parent Loop BB4_139 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, (%rbx)
	jne	.LBB4_145
# BB#144:                               #   in Loop: Header=BB4_143 Depth=2
	incq	%rsi
	addq	$12, %rbx
	cmpq	%rdx, %rsi
	jl	.LBB4_143
.LBB4_145:                              # %._crit_edge171.2
                                        #   in Loop: Header=BB4_139 Depth=1
	incq	%rax
	cmpl	%edx, %esi
	jne	.LBB4_147
# BB#146:                               #   in Loop: Header=BB4_139 Depth=1
	leaq	96(%rdi,%rbp,4), %rdx
	movl	$0, (%rdx)
	cmpq	$12, %rax
	jne	.LBB4_139
	jmp	.LBB4_148
	.p2align	4, 0x90
.LBB4_140:                              # %._crit_edge228
                                        #   in Loop: Header=BB4_139 Depth=1
	incq	%rax
.LBB4_147:                              #   in Loop: Header=BB4_139 Depth=1
	cmpq	$12, %rax
	jne	.LBB4_139
.LBB4_148:                              # %._crit_edge178.2
	leaq	124(%r15,%r11), %r13
	movl	124(%r15,%r11), %eax
	subl	%eax, (%r8)
	cmpl	$0, 116(%r15,%r11)
	jne	.LBB4_57
# BB#26:
	cmpl	$0, 112(%r15,%r11)
	jne	.LBB4_57
# BB#27:                                # %.preheader152
	movl	(%r10), %ecx
	testq	%rcx, %rcx
	je	.LBB4_28
# BB#29:                                # %.lr.ph165
	cmpl	$8, %ecx
	jb	.LBB4_30
# BB#31:                                # %min.iters.checked
	movl	%ecx, %ebp
	andl	$7, %ebp
	movq	%rcx, %rax
	subq	%rbp, %rax
	je	.LBB4_30
# BB#32:                                # %vector.body.preheader
	imulq	$488, 8(%rsp), %rdx     # 8-byte Folded Reload
                                        # imm = 0x1E8
	addq	%r14, %rdx
	imulq	$244, (%rsp), %rsi      # 8-byte Folded Reload
	leaq	16(%rsi,%rdx), %rdx
	pxor	%xmm0, %xmm0
	movq	%rax, %rsi
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB4_33:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rdx), %xmm2
	movdqu	(%rdx), %xmm3
	por	%xmm2, %xmm0
	por	%xmm3, %xmm1
	addq	$32, %rdx
	addq	$-8, %rsi
	jne	.LBB4_33
# BB#34:                                # %middle.block
	por	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	por	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	por	%xmm0, %xmm1
	movd	%xmm1, %edi
	testl	%ebp, %ebp
	jne	.LBB4_35
	jmp	.LBB4_37
.LBB4_30:
	xorl	%eax, %eax
	xorl	%edi, %edi
.LBB4_35:                               # %scalar.ph.preheader
	imulq	$488, 8(%rsp), %rsi     # 8-byte Folded Reload
                                        # imm = 0x1E8
	addq	%r14, %rsi
	imulq	$244, (%rsp), %rdx      # 8-byte Folded Reload
	addq	%rsi, %rdx
	.p2align	4, 0x90
.LBB4_36:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	orl	(%rdx,%rax,4), %edi
	incq	%rax
	cmpq	%rcx, %rax
	jb	.LBB4_36
	jmp	.LBB4_37
.LBB4_28:
	xorl	%edi, %edi
.LBB4_37:                               # %._crit_edge166
	movl	84(%r8), %ebp
	cmpq	$11, %rbp
	ja	.LBB4_44
# BB#38:                                # %.preheader151.preheader
	testb	$1, %bpl
	jne	.LBB4_40
# BB#39:
	movq	%rbp, %rdx
	cmpl	$11, %ebp
	jne	.LBB4_42
	jmp	.LBB4_44
.LBB4_40:                               # %.preheader151.prol
	imulq	$488, 8(%rsp), %rax     # 8-byte Folded Reload
                                        # imm = 0x1E8
	addq	%r14, %rax
	imulq	$244, (%rsp), %rdx      # 8-byte Folded Reload
	addq	%rax, %rdx
	leaq	(%rbp,%rbp,2), %rax
	orl	88(%rdx,%rax,4), %edi
	orl	92(%rdx,%rax,4), %edi
	orl	96(%rdx,%rax,4), %edi
	movq	%rbp, %rdx
	incq	%rdx
	cmpl	$11, %ebp
	je	.LBB4_44
.LBB4_42:                               # %.preheader151.preheader.new
	movl	$12, %eax
	subq	%rdx, %rax
	leaq	(%rdx,%rdx,2), %rdx
	imulq	$488, 8(%rsp), %rsi     # 8-byte Folded Reload
                                        # imm = 0x1E8
	addq	%r14, %rsi
	imulq	$244, (%rsp), %rbx      # 8-byte Folded Reload
	addq	%rsi, %rbx
	leaq	108(%rbx,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB4_43:                               # %.preheader151
                                        # =>This Inner Loop Header: Depth=1
	orl	-20(%rdx), %edi
	orl	-16(%rdx), %edi
	orl	-12(%rdx), %edi
	orl	-8(%rdx), %edi
	orl	-4(%rdx), %edi
	orl	(%rdx), %edi
	addq	$24, %rdx
	addq	$-2, %rax
	jne	.LBB4_43
.LBB4_44:                               # %._crit_edge162
	testl	%edi, %edi
	je	.LBB4_57
# BB#45:                                # %._crit_edge162
	andl	$1, %edi
	jne	.LBB4_57
# BB#46:                                # %.preheader150
	testl	%ecx, %ecx
	je	.LBB4_50
# BB#47:                                # %.lr.ph.preheader
	xorl	%eax, %eax
	imulq	$488, 8(%rsp), %rdx     # 8-byte Folded Reload
                                        # imm = 0x1E8
	addq	%r14, %rdx
	imulq	$244, (%rsp), %rcx      # 8-byte Folded Reload
	addq	%rdx, %rcx
	.p2align	4, 0x90
.LBB4_48:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %edx
	movl	(%rcx,%rdx,4), %esi
	movl	%esi, %edi
	shrl	$31, %edi
	addl	%esi, %edi
	sarl	%edi
	movl	%edi, (%rcx,%rdx,4)
	incl	%eax
	cmpl	(%r10), %eax
	jb	.LBB4_48
# BB#49:                                # %._crit_edge157.loopexit
	movl	84(%r8), %ebp
.LBB4_50:                               # %._crit_edge157
	leaq	116(%r15,%r11), %rax
	cmpl	$11, %ebp
	ja	.LBB4_53
# BB#51:                                # %.preheader.preheader
	movl	%ebp, %esi
	imulq	$488, 8(%rsp), %rcx     # 8-byte Folded Reload
                                        # imm = 0x1E8
	addq	%r14, %rcx
	imulq	$244, (%rsp), %rdx      # 8-byte Folded Reload
	addq	%rcx, %rdx
	leaq	(%rsi,%rsi,2), %rcx
	leaq	96(%rdx,%rcx,4), %rcx
	movl	$12, %edx
	subq	%rsi, %rdx
	.p2align	4, 0x90
.LBB4_52:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	-8(%rcx), %esi
	movl	%esi, %edi
	shrl	$31, %edi
	addl	%esi, %edi
	sarl	%edi
	movl	%edi, -8(%rcx)
	movl	-4(%rcx), %esi
	movl	%esi, %edi
	shrl	$31, %edi
	addl	%esi, %edi
	sarl	%edi
	movl	%edi, -4(%rcx)
	movl	(%rcx), %esi
	movl	%esi, %edi
	shrl	$31, %edi
	addl	%esi, %edi
	sarl	%edi
	movl	%edi, (%rcx)
	addq	$12, %rcx
	decq	%rdx
	jne	.LBB4_52
.LBB4_53:                               # %._crit_edge
	movl	$1, (%rax)
	movl	$99999999, (%r13)       # imm = 0x5F5E0FF
	imulq	$488, 8(%rsp), %rax     # 8-byte Folded Reload
                                        # imm = 0x1E8
	addq	%r14, %rax
	imulq	$244, (%rsp), %rdi      # 8-byte Folded Reload
	addq	%rax, %rdi
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpl	$2, 200(%rax)
	jne	.LBB4_55
# BB#54:
	movq	%r8, %rsi
	movq	%r8, %rbx
	callq	scale_bitcount
	jmp	.LBB4_56
.LBB4_55:
	movq	%r8, %rsi
	movq	%r8, %rbx
	callq	scale_bitcount_lsf
.LBB4_56:
	movq	%rbx, %r8
.LBB4_57:
	cmpl	$1, 20(%rsp)            # 4-byte Folded Reload
	jne	.LBB4_93
# BB#58:
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpl	$2, 200(%rax)
	jne	.LBB4_93
# BB#59:
	cmpl	$2, 72(%r12,%r15)
	je	.LBB4_93
# BB#60:
	cmpl	$2, 312(%r12,%r15)
	je	.LBB4_93
# BB#61:
	movl	116(%r12,%r15), %eax
	cmpl	356(%r12,%r15), %eax
	jne	.LBB4_93
# BB#62:
	movl	112(%r12,%r15), %eax
	cmpl	352(%r12,%r15), %eax
	jne	.LBB4_93
# BB#63:                                # %.lr.ph111.i
	movq	(%rsp), %rcx            # 8-byte Reload
	imulq	$244, %rcx, %rax
	shlq	$4, %rcx
	pxor	%xmm0, %xmm0
	movq	%rcx, %rdx
	movdqu	%xmm0, 12(%r12,%rcx)
	movl	(%r14,%rax), %ecx
	cmpl	488(%r14,%rax), %ecx
	jne	.LBB4_65
# BB#64:                                # %.lr.ph111.i.1259262
	movl	4(%r14,%rax), %ecx
	cmpl	492(%r14,%rax), %ecx
	jne	.LBB4_65
# BB#161:                               # %.lr.ph111.i.2260263
	movl	8(%r14,%rax), %ecx
	cmpl	496(%r14,%rax), %ecx
	jne	.LBB4_65
# BB#162:                               # %.lr.ph111.i.3261264
	movl	12(%r14,%rax), %ecx
	cmpl	500(%r14,%rax), %ecx
	jne	.LBB4_65
# BB#163:                               # %.lr.ph111.i.4265
	movl	16(%r14,%rax), %ecx
	cmpl	504(%r14,%rax), %ecx
	jne	.LBB4_65
# BB#164:                               # %.lr.ph111.i.5266
	movl	20(%r14,%rax), %ecx
	cmpl	508(%r14,%rax), %ecx
	jne	.LBB4_65
# BB#165:
	movb	$1, %cl
	testb	%cl, %cl
	jne	.LBB4_67
	jmp	.LBB4_68
.LBB4_65:
	xorl	%ecx, %ecx
	testb	%cl, %cl
	je	.LBB4_68
.LBB4_67:                               # %._crit_edge122.i
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 488(%r14,%rax)
	movq	$-1, 504(%r14,%rax)
	movl	$1, 12(%r12,%rdx)
.LBB4_68:                               # %.lr.ph111.i.1
	movl	24(%r14,%rax), %ecx
	cmpl	512(%r14,%rax), %ecx
	jne	.LBB4_70
# BB#69:                                # %.lr.ph111.i.1.1255
	movl	28(%r14,%rax), %ecx
	cmpl	516(%r14,%rax), %ecx
	jne	.LBB4_70
# BB#157:                               # %.lr.ph111.i.1.2256
	movl	32(%r14,%rax), %ecx
	cmpl	520(%r14,%rax), %ecx
	jne	.LBB4_70
# BB#158:                               # %.lr.ph111.i.1.3257
	movl	36(%r14,%rax), %ecx
	cmpl	524(%r14,%rax), %ecx
	jne	.LBB4_70
# BB#159:                               # %.lr.ph111.i.1.4258
	movl	40(%r14,%rax), %ecx
	cmpl	528(%r14,%rax), %ecx
	jne	.LBB4_70
# BB#160:
	movb	$1, %cl
	testb	%cl, %cl
	jne	.LBB4_72
	jmp	.LBB4_73
.LBB4_70:
	xorl	%ecx, %ecx
	testb	%cl, %cl
	je	.LBB4_73
.LBB4_72:                               # %._crit_edge122.i.1
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 512(%r14,%rax)
	movl	$-1, 528(%r14,%rax)
	movl	$1, 16(%r12,%rdx)
.LBB4_73:                               # %.lr.ph111.i.2
	movl	44(%r14,%rax), %ecx
	cmpl	532(%r14,%rax), %ecx
	jne	.LBB4_75
# BB#74:                                # %.lr.ph111.i.2.1251
	movl	48(%r14,%rax), %ecx
	cmpl	536(%r14,%rax), %ecx
	jne	.LBB4_75
# BB#153:                               # %.lr.ph111.i.2.2252
	movl	52(%r14,%rax), %ecx
	cmpl	540(%r14,%rax), %ecx
	jne	.LBB4_75
# BB#154:                               # %.lr.ph111.i.2.3253
	movl	56(%r14,%rax), %ecx
	cmpl	544(%r14,%rax), %ecx
	jne	.LBB4_75
# BB#155:                               # %.lr.ph111.i.2.4254
	movl	60(%r14,%rax), %ecx
	cmpl	548(%r14,%rax), %ecx
	jne	.LBB4_75
# BB#156:
	movb	$1, %cl
	testb	%cl, %cl
	jne	.LBB4_77
	jmp	.LBB4_78
.LBB4_75:
	xorl	%ecx, %ecx
	testb	%cl, %cl
	je	.LBB4_78
.LBB4_77:                               # %._crit_edge122.i.2
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 532(%r14,%rax)
	movl	$-1, 548(%r14,%rax)
	movl	$1, 20(%r12,%rdx)
.LBB4_78:                               # %.lr.ph111.i.3
	movl	64(%r14,%rax), %ecx
	cmpl	552(%r14,%rax), %ecx
	jne	.LBB4_80
# BB#79:                                # %.lr.ph111.i.3.1247
	movl	68(%r14,%rax), %ecx
	cmpl	556(%r14,%rax), %ecx
	jne	.LBB4_80
# BB#149:                               # %.lr.ph111.i.3.2248
	movl	72(%r14,%rax), %ecx
	cmpl	560(%r14,%rax), %ecx
	jne	.LBB4_80
# BB#150:                               # %.lr.ph111.i.3.3249
	movl	76(%r14,%rax), %ecx
	cmpl	564(%r14,%rax), %ecx
	jne	.LBB4_80
# BB#151:                               # %.lr.ph111.i.3.4250
	movl	80(%r14,%rax), %ecx
	cmpl	568(%r14,%rax), %ecx
	jne	.LBB4_80
# BB#152:
	movb	$1, %cl
	testb	%cl, %cl
	jne	.LBB4_82
	jmp	.LBB4_83
.LBB4_80:
	xorl	%ecx, %ecx
	testb	%cl, %cl
	je	.LBB4_83
.LBB4_82:                               # %._crit_edge122.i.3
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 552(%r14,%rax)
	movl	$-1, 568(%r14,%rax)
	movl	$1, 24(%r12,%rdx)
.LBB4_83:                               # %.backedge.i.3
	movl	488(%r14,%rax), %r9d
	movl	492(%r14,%rax), %esi
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	testl	%r9d, %r9d
	setns	%bl
	cmovnsl	%r9d, %ecx
	testl	%esi, %esi
	js	.LBB4_84
# BB#94:
	sarl	$31, %r9d
	addl	$2, %r9d
	cmpl	%esi, %ecx
	cmovll	%esi, %ecx
	jmp	.LBB4_95
.LBB4_84:
	movb	%bl, %dil
	movl	%edi, %r9d
.LBB4_95:                               # %.preheader99.2132.i
	movl	496(%r14,%rax), %esi
	testl	%esi, %esi
	js	.LBB4_97
# BB#96:
	incl	%r9d
	cmpl	%esi, %ecx
	cmovll	%esi, %ecx
.LBB4_97:                               # %.preheader99.3133.i
	movl	500(%r14,%rax), %esi
	testl	%esi, %esi
	js	.LBB4_99
# BB#98:
	incl	%r9d
	cmpl	%esi, %ecx
	cmovll	%esi, %ecx
.LBB4_99:                               # %.preheader99.4134.i
	movl	504(%r14,%rax), %esi
	testl	%esi, %esi
	js	.LBB4_101
# BB#100:
	incl	%r9d
	cmpl	%esi, %ecx
	cmovll	%esi, %ecx
.LBB4_101:                              # %.preheader99.5135.i
	movl	508(%r14,%rax), %esi
	testl	%esi, %esi
	js	.LBB4_103
# BB#102:
	incl	%r9d
	cmpl	%esi, %ecx
	cmovll	%esi, %ecx
.LBB4_103:                              # %.preheader99.6136.i
	movl	512(%r14,%rax), %esi
	testl	%esi, %esi
	js	.LBB4_105
# BB#104:
	incl	%r9d
	cmpl	%esi, %ecx
	cmovll	%esi, %ecx
.LBB4_105:                              # %.preheader99.7137.i
	movl	516(%r14,%rax), %esi
	testl	%esi, %esi
	js	.LBB4_107
# BB#106:
	incl	%r9d
	cmpl	%esi, %ecx
	cmovll	%esi, %ecx
.LBB4_107:                              # %.preheader99.8138.i
	movl	520(%r14,%rax), %esi
	testl	%esi, %esi
	js	.LBB4_109
# BB#108:
	incl	%r9d
	cmpl	%esi, %ecx
	cmovll	%esi, %ecx
.LBB4_109:                              # %.preheader99.9139.i
	movl	524(%r14,%rax), %esi
	testl	%esi, %esi
	js	.LBB4_111
# BB#110:
	incl	%r9d
	cmpl	%esi, %ecx
	cmovll	%esi, %ecx
.LBB4_111:                              # %.preheader99.10140.i
	movl	528(%r14,%rax), %esi
	movq	%r8, %rdx
	testl	%esi, %esi
	js	.LBB4_86
# BB#85:
	incl	%r9d
	cmpl	%esi, %ecx
	cmovll	%esi, %ecx
.LBB4_86:                               # %.lr.ph.preheader.i
	movl	532(%r14,%rax), %edi
	movl	536(%r14,%rax), %ebp
	xorl	%esi, %esi
	xorl	%ebx, %ebx
	testl	%edi, %edi
	cmovnsl	%edi, %esi
	setns	%r8b
	testl	%ebp, %ebp
	js	.LBB4_87
# BB#112:
	sarl	$31, %edi
	addl	$2, %edi
	cmpl	%ebp, %esi
	cmovll	%ebp, %esi
	movl	%edi, %ebx
	jmp	.LBB4_113
.LBB4_87:
	movb	%r8b, %bl
.LBB4_113:                              # %.lr.ph.i.2194
	movq	%rdx, %r8
	movl	540(%r14,%rax), %edi
	testl	%edi, %edi
	js	.LBB4_115
# BB#114:
	incl	%ebx
	cmpl	%edi, %esi
	cmovll	%edi, %esi
.LBB4_115:                              # %.lr.ph.i.3195
	movl	544(%r14,%rax), %edi
	testl	%edi, %edi
	js	.LBB4_117
# BB#116:
	incl	%ebx
	cmpl	%edi, %esi
	cmovll	%edi, %esi
.LBB4_117:                              # %.lr.ph.i.4196
	movl	548(%r14,%rax), %edi
	testl	%edi, %edi
	js	.LBB4_119
# BB#118:
	incl	%ebx
	cmpl	%edi, %esi
	cmovll	%edi, %esi
.LBB4_119:                              # %.lr.ph.i.5197
	movl	552(%r14,%rax), %edi
	testl	%edi, %edi
	js	.LBB4_121
# BB#120:
	incl	%ebx
	cmpl	%edi, %esi
	cmovll	%edi, %esi
.LBB4_121:                              # %.lr.ph.i.6198
	movl	556(%r14,%rax), %edi
	testl	%edi, %edi
	js	.LBB4_123
# BB#122:
	incl	%ebx
	cmpl	%edi, %esi
	cmovll	%edi, %esi
.LBB4_123:                              # %.lr.ph.i.7199
	movl	560(%r14,%rax), %edi
	testl	%edi, %edi
	js	.LBB4_125
# BB#124:
	incl	%ebx
	cmpl	%edi, %esi
	cmovll	%edi, %esi
.LBB4_125:                              # %.lr.ph.i.8200
	movl	564(%r14,%rax), %edi
	testl	%edi, %edi
	js	.LBB4_127
# BB#126:
	incl	%ebx
	cmpl	%edi, %esi
	cmovll	%edi, %esi
.LBB4_127:                              # %.lr.ph.i.9201
	movl	568(%r14,%rax), %eax
	testl	%eax, %eax
	js	.LBB4_129
# BB#128:
	incl	%ebx
	cmpl	%eax, %esi
	cmovll	%eax, %esi
.LBB4_129:                              # %.preheader.i202
	leaq	364(%r12,%r15), %rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_88:                               # =>This Inner Loop Header: Depth=1
	cmpl	scfsi_calc.slen1_n(,%rbp,4), %ecx
	jge	.LBB4_92
# BB#89:                                #   in Loop: Header=BB4_88 Depth=1
	cmpl	scfsi_calc.slen2_n(,%rbp,4), %esi
	jge	.LBB4_92
# BB#90:                                #   in Loop: Header=BB4_88 Depth=1
	movl	scfsi_calc.slen1_tab(,%rbp,4), %edx
	imull	%r9d, %edx
	movl	scfsi_calc.slen2_tab(,%rbp,4), %edi
	imull	%ebx, %edi
	addl	%edx, %edi
	cmpl	%edi, (%rax)
	jle	.LBB4_92
# BB#91:                                #   in Loop: Header=BB4_88 Depth=1
	movl	%edi, (%rax)
	movl	%ebp, -60(%rax)
	.p2align	4, 0x90
.LBB4_92:                               #   in Loop: Header=BB4_88 Depth=1
	incq	%rbp
	cmpq	$16, %rbp
	jne	.LBB4_88
.LBB4_93:                               # %scfsi_calc.exit
	movl	(%r13), %eax
	addl	%eax, (%r8)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	best_scalefac_store, .Lfunc_end4-best_scalefac_store
	.cfi_endproc

	.type	subdv_table,@object     # @subdv_table
	.data
	.globl	subdv_table
	.p2align	4
subdv_table:
	.zero	8
	.zero	8
	.zero	8
	.zero	8
	.zero	8
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	7                       # 0x7
	.size	subdv_table, 184

	.type	huf_tbl_noESC,@object   # @huf_tbl_noESC
	.section	.rodata,"a",@progbits
	.p2align	4
huf_tbl_noESC:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	5                       # 0x5
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	13                      # 0xd
	.size	huf_tbl_noESC, 60

	.type	cb_esc_buf,@object      # @cb_esc_buf
	.local	cb_esc_buf
	.comm	cb_esc_buf,1152,16
	.type	cb_esc_sign,@object     # @cb_esc_sign
	.local	cb_esc_sign
	.comm	cb_esc_sign,4,4
	.type	cb_esc_end,@object      # @cb_esc_end
	.local	cb_esc_end
	.comm	cb_esc_end,8,8
	.type	scfsi_calc.slen1_n,@object # @scfsi_calc.slen1_n
	.p2align	4
scfsi_calc.slen1_n:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	8                       # 0x8
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	16                      # 0x10
	.long	16                      # 0x10
	.size	scfsi_calc.slen1_n, 64

	.type	scfsi_calc.slen2_n,@object # @scfsi_calc.slen2_n
	.p2align	4
scfsi_calc.slen2_n:
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	4                       # 0x4
	.long	8                       # 0x8
	.size	scfsi_calc.slen2_n, 64

	.type	scfsi_calc.slen1_tab,@object # @scfsi_calc.slen1_tab
	.p2align	4
scfsi_calc.slen1_tab:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.size	scfsi_calc.slen1_tab, 64

	.type	scfsi_calc.slen2_tab,@object # @scfsi_calc.slen2_tab
	.p2align	4
scfsi_calc.slen2_tab:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.size	scfsi_calc.slen2_tab, 64


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
