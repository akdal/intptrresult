	.text
	.file	"vector.bc"
	.globl	hypre_SeqVectorCreate
	.p2align	4, 0x90
	.type	hypre_SeqVectorCreate,@function
hypre_SeqVectorCreate:                  # @hypre_SeqVectorCreate
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movl	$1, %edi
	movl	$32, %esi
	callq	hypre_CAlloc
	movq	$0, (%rax)
	movl	%ebx, 8(%rax)
	movl	$1, 16(%rax)
	movl	$0, 20(%rax)
	movl	$1, 12(%rax)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	hypre_SeqVectorCreate, .Lfunc_end0-hypre_SeqVectorCreate
	.cfi_endproc

	.globl	hypre_SeqMultiVectorCreate
	.p2align	4, 0x90
	.type	hypre_SeqMultiVectorCreate,@function
hypre_SeqMultiVectorCreate:             # @hypre_SeqMultiVectorCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movl	%edi, %ebp
	movl	$1, %edi
	movl	$32, %esi
	callq	hypre_CAlloc
	movq	$0, (%rax)
	movl	%ebp, 8(%rax)
	movl	$0, 20(%rax)
	movl	$1, 12(%rax)
	movl	%ebx, 16(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	hypre_SeqMultiVectorCreate, .Lfunc_end1-hypre_SeqMultiVectorCreate
	.cfi_endproc

	.globl	hypre_SeqVectorDestroy
	.p2align	4, 0x90
	.type	hypre_SeqVectorDestroy,@function
hypre_SeqVectorDestroy:                 # @hypre_SeqVectorDestroy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 16
.Lcfi8:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB2_4
# BB#1:
	cmpl	$0, 12(%rbx)
	je	.LBB2_3
# BB#2:
	movq	(%rbx), %rdi
	callq	hypre_Free
	movq	$0, (%rbx)
.LBB2_3:
	movq	%rbx, %rdi
	callq	hypre_Free
.LBB2_4:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end2:
	.size	hypre_SeqVectorDestroy, .Lfunc_end2-hypre_SeqVectorDestroy
	.cfi_endproc

	.globl	hypre_SeqVectorInitialize
	.p2align	4, 0x90
	.type	hypre_SeqVectorInitialize,@function
hypre_SeqVectorInitialize:              # @hypre_SeqVectorInitialize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	8(%rbx), %r14d
	movl	16(%rbx), %r15d
	movl	20(%rbx), %ebp
	cmpq	$0, (%rbx)
	jne	.LBB3_2
# BB#1:
	movl	%r15d, %edi
	imull	%r14d, %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rax, (%rbx)
.LBB3_2:
	movl	$1, %eax
	cmpl	$1, %ebp
	je	.LBB3_3
# BB#4:
	testl	%ebp, %ebp
	jne	.LBB3_7
# BB#5:
	movl	$1, %r15d
	jmp	.LBB3_6
.LBB3_3:
	movl	$1, %r14d
.LBB3_6:                                # %.sink.split
	movl	%r14d, 24(%rbx)
	movl	%r15d, 28(%rbx)
	xorl	%eax, %eax
.LBB3_7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	hypre_SeqVectorInitialize, .Lfunc_end3-hypre_SeqVectorInitialize
	.cfi_endproc

	.globl	hypre_SeqVectorSetDataOwner
	.p2align	4, 0x90
	.type	hypre_SeqVectorSetDataOwner,@function
hypre_SeqVectorSetDataOwner:            # @hypre_SeqVectorSetDataOwner
	.cfi_startproc
# BB#0:
	movl	%esi, 12(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end4:
	.size	hypre_SeqVectorSetDataOwner, .Lfunc_end4-hypre_SeqVectorSetDataOwner
	.cfi_endproc

	.globl	hypre_SeqVectorRead
	.p2align	4, 0x90
	.type	hypre_SeqVectorRead,@function
hypre_SeqVectorRead:                    # @hypre_SeqVectorRead
	.cfi_startproc
# BB#0:                                 # %hypre_SeqVectorInitialize.exit
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 48
.Lcfi23:
	.cfi_offset %rbx, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movl	$.L.str, %esi
	callq	fopen
	movq	%rax, %r15
	leaq	4(%rsp), %rdx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fscanf
	movl	4(%rsp), %ebp
	movl	$1, %edi
	movl	$32, %esi
	callq	hypre_CAlloc
	movq	%rax, %r14
	movq	$0, (%r14)
	movl	%ebp, 8(%r14)
	movl	$1, 16(%r14)
	movl	$0, 20(%r14)
	movl	$1, 12(%r14)
	movl	$8, %esi
	movl	%ebp, %edi
	callq	hypre_CAlloc
	movq	%rax, %rbx
	movq	%rbx, (%r14)
	movl	%ebp, 24(%r14)
	movl	$1, 28(%r14)
	cmpl	$0, 4(%rsp)
	jle	.LBB5_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	fscanf
	incq	%rbp
	movslq	4(%rsp), %rax
	addq	$8, %rbx
	cmpq	%rax, %rbp
	jl	.LBB5_2
.LBB5_3:                                # %._crit_edge
	movq	%r15, %rdi
	callq	fclose
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	hypre_SeqVectorRead, .Lfunc_end5-hypre_SeqVectorRead
	.cfi_endproc

	.globl	hypre_SeqVectorPrint
	.p2align	4, 0x90
	.type	hypre_SeqVectorPrint,@function
hypre_SeqVectorPrint:                   # @hypre_SeqVectorPrint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 80
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rsi, %rax
	movq	%rdi, %r12
	movl	16(%r12), %r14d
	movslq	24(%r12), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movslq	28(%r12), %r13
	movq	(%r12), %r15
	movl	8(%r12), %ebp
	movl	$.L.str.3, %esi
	movq	%rax, %rdi
	callq	fopen
	movq	%rax, %rbx
	cmpl	$1, 16(%r12)
	jne	.LBB6_2
# BB#1:
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	fprintf
	cmpl	$1, %r14d
	jg	.LBB6_6
	jmp	.LBB6_4
.LBB6_2:
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%r14d, %edx
	movl	%ebp, %ecx
	callq	fprintf
	cmpl	$1, %r14d
	jle	.LBB6_4
.LBB6_6:                                # %.lr.ph55
	testl	%ebp, %ebp
	jle	.LBB6_7
# BB#9:                                 # %.lr.ph55.split.us.preheader
	shlq	$3, (%rsp)              # 8-byte Folded Spill
	shlq	$3, %r13
	xorl	%r12d, %r12d
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%r14, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB6_10:                               # %.lr.ph55.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_11 Depth 2
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%r12d, %edx
	callq	fprintf
	movq	%rbp, %r14
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB6_11:                               #   Parent Loop BB6_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	movl	$.L.str.7, %esi
	movb	$1, %al
	movq	%rbx, %rdi
	callq	fprintf
	addq	%r13, %rbp
	decq	%r14
	jne	.LBB6_11
# BB#12:                                # %._crit_edge.us
                                        #   in Loop: Header=BB6_10 Depth=1
	incq	%r12
	addq	(%rsp), %r15            # 8-byte Folded Reload
	movq	8(%rsp), %r14           # 8-byte Reload
	cmpq	%r14, %r12
	movq	16(%rsp), %rbp          # 8-byte Reload
	jne	.LBB6_10
	jmp	.LBB6_13
.LBB6_4:                                # %.preheader51
	testl	%ebp, %ebp
	jle	.LBB6_13
	.p2align	4, 0x90
.LBB6_5:                                # %.lr.ph57
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	movl	$.L.str.7, %esi
	movb	$1, %al
	movq	%rbx, %rdi
	callq	fprintf
	addq	$8, %r15
	decq	%rbp
	jne	.LBB6_5
	jmp	.LBB6_13
.LBB6_7:                                # %.lr.ph55.split.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_8:                                # %.lr.ph55.split
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	fprintf
	incl	%ebp
	cmpl	%ebp, %r14d
	jne	.LBB6_8
.LBB6_13:                               # %.loopexit
	movq	%rbx, %rdi
	callq	fclose
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	hypre_SeqVectorPrint, .Lfunc_end6-hypre_SeqVectorPrint
	.cfi_endproc

	.globl	hypre_SeqVectorSetConstantValues
	.p2align	4, 0x90
	.type	hypre_SeqVectorSetConstantValues,@function
hypre_SeqVectorSetConstantValues:       # @hypre_SeqVectorSetConstantValues
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %edx
	imull	8(%rdi), %edx
	testl	%edx, %edx
	jle	.LBB7_10
# BB#1:                                 # %.lr.ph.preheader
	movq	(%rdi), %rcx
	movl	%edx, %eax
	cmpl	$4, %edx
	jb	.LBB7_7
# BB#3:                                 # %min.iters.checked
	andl	$3, %edx
	movq	%rax, %r8
	subq	%rdx, %r8
	je	.LBB7_7
# BB#4:                                 # %vector.ph
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	leaq	16(%rcx), %rdi
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB7_5:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm1, -16(%rdi)
	movups	%xmm1, (%rdi)
	addq	$32, %rdi
	addq	$-4, %rsi
	jne	.LBB7_5
# BB#6:                                 # %middle.block
	testl	%edx, %edx
	jne	.LBB7_8
	jmp	.LBB7_10
.LBB7_7:
	xorl	%r8d, %r8d
.LBB7_8:                                # %.lr.ph.preheader21
	leaq	(%rcx,%r8,8), %rcx
	subq	%r8, %rax
	.p2align	4, 0x90
.LBB7_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm0, (%rcx)
	addq	$8, %rcx
	decq	%rax
	jne	.LBB7_9
.LBB7_10:                               # %._crit_edge
	xorl	%eax, %eax
	retq
.Lfunc_end7:
	.size	hypre_SeqVectorSetConstantValues, .Lfunc_end7-hypre_SeqVectorSetConstantValues
	.cfi_endproc

	.globl	hypre_SeqVectorCopy
	.p2align	4, 0x90
	.type	hypre_SeqVectorCopy,@function
hypre_SeqVectorCopy:                    # @hypre_SeqVectorCopy
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %r8d
	imull	8(%rdi), %r8d
	testl	%r8d, %r8d
	jle	.LBB8_17
# BB#1:                                 # %.lr.ph.preheader
	movq	(%rdi), %r9
	movq	(%rsi), %r10
	movl	%r8d, %edi
	cmpl	$4, %r8d
	jae	.LBB8_3
# BB#2:
	xorl	%esi, %esi
	jmp	.LBB8_11
.LBB8_3:                                # %min.iters.checked
	andl	$3, %r8d
	movq	%rdi, %rsi
	subq	%r8, %rsi
	je	.LBB8_7
# BB#4:                                 # %vector.memcheck
	leaq	(%r9,%rdi,8), %rax
	cmpq	%rax, %r10
	jae	.LBB8_8
# BB#5:                                 # %vector.memcheck
	leaq	(%r10,%rdi,8), %rax
	cmpq	%rax, %r9
	jae	.LBB8_8
.LBB8_7:
	xorl	%esi, %esi
	jmp	.LBB8_11
.LBB8_8:                                # %vector.body.preheader
	leaq	16(%r9), %rax
	leaq	16(%r10), %rcx
	movq	%rsi, %rdx
	.p2align	4, 0x90
.LBB8_9:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rax), %xmm0
	movups	(%rax), %xmm1
	movups	%xmm0, -16(%rcx)
	movups	%xmm1, (%rcx)
	addq	$32, %rax
	addq	$32, %rcx
	addq	$-4, %rdx
	jne	.LBB8_9
# BB#10:                                # %middle.block
	testl	%r8d, %r8d
	je	.LBB8_17
.LBB8_11:                               # %.lr.ph.preheader27
	movl	%edi, %eax
	subl	%esi, %eax
	leaq	-1(%rdi), %r8
	subq	%rsi, %r8
	andq	$7, %rax
	je	.LBB8_14
# BB#12:                                # %.lr.ph.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB8_13:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r9,%rsi,8), %rcx
	movq	%rcx, (%r10,%rsi,8)
	incq	%rsi
	incq	%rax
	jne	.LBB8_13
.LBB8_14:                               # %.lr.ph.prol.loopexit
	cmpq	$7, %r8
	jb	.LBB8_17
# BB#15:                                # %.lr.ph.preheader27.new
	subq	%rsi, %rdi
	leaq	56(%r10,%rsi,8), %rdx
	leaq	56(%r9,%rsi,8), %rcx
	.p2align	4, 0x90
.LBB8_16:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rax
	movq	%rax, -56(%rdx)
	movq	-48(%rcx), %rax
	movq	%rax, -48(%rdx)
	movq	-40(%rcx), %rax
	movq	%rax, -40(%rdx)
	movq	-32(%rcx), %rax
	movq	%rax, -32(%rdx)
	movq	-24(%rcx), %rax
	movq	%rax, -24(%rdx)
	movq	-16(%rcx), %rax
	movq	%rax, -16(%rdx)
	movq	-8(%rcx), %rax
	movq	%rax, -8(%rdx)
	movq	(%rcx), %rax
	movq	%rax, (%rdx)
	addq	$64, %rdx
	addq	$64, %rcx
	addq	$-8, %rdi
	jne	.LBB8_16
.LBB8_17:                               # %._crit_edge
	xorl	%eax, %eax
	retq
.Lfunc_end8:
	.size	hypre_SeqVectorCopy, .Lfunc_end8-hypre_SeqVectorCopy
	.cfi_endproc

	.globl	hypre_SeqVectorCloneDeep
	.p2align	4, 0x90
	.type	hypre_SeqVectorCloneDeep,@function
hypre_SeqVectorCloneDeep:               # @hypre_SeqVectorCloneDeep
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 64
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	8(%rbx), %r15d
	movl	16(%rbx), %r13d
	movl	$1, %r12d
	movl	$1, %edi
	movl	$32, %esi
	callq	hypre_CAlloc
	movq	%rax, %r14
	movq	$0, (%r14)
	movl	%r15d, 8(%r14)
	movl	$0, 20(%r14)
	movl	$1, 12(%r14)
	movl	%r13d, 16(%r14)
	movl	20(%rbx), %ebp
	movl	%ebp, 20(%r14)
	movl	24(%rbx), %eax
	movl	%eax, 24(%r14)
	movl	28(%rbx), %eax
	movl	%eax, 28(%r14)
	movl	%r13d, %edi
	imull	%r15d, %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rax, (%r14)
	cmpl	$1, %ebp
	je	.LBB9_3
# BB#1:
	testl	%ebp, %ebp
	jne	.LBB9_4
# BB#2:
	movl	$1, %r13d
	movl	%r15d, %r12d
.LBB9_3:                                # %.sink.split.i
	movl	%r12d, 24(%r14)
	movl	%r13d, 28(%r14)
.LBB9_4:                                # %hypre_SeqVectorInitialize.exit
	movl	16(%rbx), %r8d
	imull	8(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.LBB9_21
# BB#5:                                 # %.lr.ph.preheader.i
	movq	(%rbx), %rdx
	movl	%r8d, %ebp
	cmpl	$4, %r8d
	jae	.LBB9_7
# BB#6:
	xorl	%esi, %esi
	jmp	.LBB9_15
.LBB9_7:                                # %min.iters.checked
	andl	$3, %r8d
	movq	%rbp, %rsi
	subq	%r8, %rsi
	je	.LBB9_11
# BB#8:                                 # %vector.memcheck
	leaq	(%rdx,%rbp,8), %rcx
	cmpq	%rcx, %rax
	jae	.LBB9_12
# BB#9:                                 # %vector.memcheck
	leaq	(%rax,%rbp,8), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB9_12
.LBB9_11:
	xorl	%esi, %esi
	jmp	.LBB9_15
.LBB9_12:                               # %vector.body.preheader
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rbx
	movq	%rsi, %rdi
	.p2align	4, 0x90
.LBB9_13:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	addq	$32, %rcx
	addq	$32, %rbx
	addq	$-4, %rdi
	jne	.LBB9_13
# BB#14:                                # %middle.block
	testl	%r8d, %r8d
	je	.LBB9_21
.LBB9_15:                               # %.lr.ph.i.preheader
	movl	%ebp, %ecx
	subl	%esi, %ecx
	leaq	-1(%rbp), %rdi
	subq	%rsi, %rdi
	andq	$7, %rcx
	je	.LBB9_18
# BB#16:                                # %.lr.ph.i.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB9_17:                               # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rsi,8), %rbx
	movq	%rbx, (%rax,%rsi,8)
	incq	%rsi
	incq	%rcx
	jne	.LBB9_17
.LBB9_18:                               # %.lr.ph.i.prol.loopexit
	cmpq	$7, %rdi
	jb	.LBB9_21
# BB#19:                                # %.lr.ph.i.preheader.new
	subq	%rsi, %rbp
	leaq	56(%rax,%rsi,8), %rax
	leaq	56(%rdx,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB9_20:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rdx), %rcx
	movq	%rcx, -56(%rax)
	movq	-48(%rdx), %rcx
	movq	%rcx, -48(%rax)
	movq	-40(%rdx), %rcx
	movq	%rcx, -40(%rax)
	movq	-32(%rdx), %rcx
	movq	%rcx, -32(%rax)
	movq	-24(%rdx), %rcx
	movq	%rcx, -24(%rax)
	movq	-16(%rdx), %rcx
	movq	%rcx, -16(%rax)
	movq	-8(%rdx), %rcx
	movq	%rcx, -8(%rax)
	movq	(%rdx), %rcx
	movq	%rcx, (%rax)
	addq	$64, %rax
	addq	$64, %rdx
	addq	$-8, %rbp
	jne	.LBB9_20
.LBB9_21:                               # %hypre_SeqVectorCopy.exit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	hypre_SeqVectorCloneDeep, .Lfunc_end9-hypre_SeqVectorCloneDeep
	.cfi_endproc

	.globl	hypre_SeqVectorCloneShallow
	.p2align	4, 0x90
	.type	hypre_SeqVectorCloneShallow,@function
hypre_SeqVectorCloneShallow:            # @hypre_SeqVectorCloneShallow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi59:
	.cfi_def_cfa_offset 64
.Lcfi60:
	.cfi_offset %rbx, -56
.Lcfi61:
	.cfi_offset %r12, -48
.Lcfi62:
	.cfi_offset %r13, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	8(%r14), %r15d
	movl	16(%r14), %r13d
	movl	$1, %r12d
	movl	$1, %edi
	movl	$32, %esi
	callq	hypre_CAlloc
	movq	%rax, %rbx
	movq	$0, (%rbx)
	movl	%r15d, 8(%rbx)
	movl	$0, 20(%rbx)
	movl	%r13d, 16(%rbx)
	movl	20(%r14), %ebp
	movl	%ebp, 20(%rbx)
	movl	24(%r14), %eax
	movl	%eax, 24(%rbx)
	movl	28(%r14), %eax
	movl	%eax, 28(%rbx)
	movq	(%r14), %rax
	movq	%rax, (%rbx)
	movl	$0, 12(%rbx)
	testq	%rax, %rax
	jne	.LBB10_2
# BB#1:
	movl	%r13d, %edi
	imull	%r15d, %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rax, (%rbx)
.LBB10_2:
	cmpl	$1, %ebp
	je	.LBB10_5
# BB#3:
	testl	%ebp, %ebp
	jne	.LBB10_6
# BB#4:
	movl	$1, %r13d
	movl	%r15d, %r12d
.LBB10_5:                               # %.sink.split.i
	movl	%r12d, 24(%rbx)
	movl	%r13d, 28(%rbx)
.LBB10_6:                               # %hypre_SeqVectorInitialize.exit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	hypre_SeqVectorCloneShallow, .Lfunc_end10-hypre_SeqVectorCloneShallow
	.cfi_endproc

	.globl	hypre_SeqVectorScale
	.p2align	4, 0x90
	.type	hypre_SeqVectorScale,@function
hypre_SeqVectorScale:                   # @hypre_SeqVectorScale
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %edx
	imull	8(%rdi), %edx
	testl	%edx, %edx
	jle	.LBB11_10
# BB#1:                                 # %.lr.ph.preheader
	movq	(%rdi), %r8
	movl	%edx, %eax
	cmpl	$4, %edx
	jb	.LBB11_7
# BB#3:                                 # %min.iters.checked
	andl	$3, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB11_7
# BB#4:                                 # %vector.ph
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	leaq	16(%r8), %rdi
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB11_5:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rdi), %xmm2
	movupd	(%rdi), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	%xmm2, -16(%rdi)
	movupd	%xmm3, (%rdi)
	addq	$32, %rdi
	addq	$-4, %rsi
	jne	.LBB11_5
# BB#6:                                 # %middle.block
	testl	%edx, %edx
	jne	.LBB11_8
	jmp	.LBB11_10
.LBB11_7:
	xorl	%ecx, %ecx
.LBB11_8:                               # %.lr.ph.preheader22
	leaq	(%r8,%rcx,8), %rdx
	subq	%rcx, %rax
	.p2align	4, 0x90
.LBB11_9:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdx)
	addq	$8, %rdx
	decq	%rax
	jne	.LBB11_9
.LBB11_10:                              # %._crit_edge
	xorl	%eax, %eax
	retq
.Lfunc_end11:
	.size	hypre_SeqVectorScale, .Lfunc_end11-hypre_SeqVectorScale
	.cfi_endproc

	.globl	hypre_SeqVectorAxpy
	.p2align	4, 0x90
	.type	hypre_SeqVectorAxpy,@function
hypre_SeqVectorAxpy:                    # @hypre_SeqVectorAxpy
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %r8d
	imull	8(%rdi), %r8d
	testl	%r8d, %r8d
	jle	.LBB12_17
# BB#1:                                 # %.lr.ph.preheader
	movq	(%rdi), %r9
	movq	(%rsi), %r10
	movl	%r8d, %eax
	cmpl	$4, %r8d
	jae	.LBB12_3
# BB#2:
	xorl	%esi, %esi
	jmp	.LBB12_11
.LBB12_3:                               # %min.iters.checked
	andl	$3, %r8d
	movq	%rax, %rsi
	subq	%r8, %rsi
	je	.LBB12_7
# BB#4:                                 # %vector.memcheck
	leaq	(%r9,%rax,8), %rcx
	cmpq	%rcx, %r10
	jae	.LBB12_8
# BB#5:                                 # %vector.memcheck
	leaq	(%r10,%rax,8), %rcx
	cmpq	%rcx, %r9
	jae	.LBB12_8
.LBB12_7:
	xorl	%esi, %esi
	jmp	.LBB12_11
.LBB12_8:                               # %vector.ph
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	leaq	16(%r9), %rdi
	leaq	16(%r10), %rcx
	movq	%rsi, %rdx
	.p2align	4, 0x90
.LBB12_9:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rdi), %xmm2
	movupd	(%rdi), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	-16(%rcx), %xmm4
	movupd	(%rcx), %xmm5
	addpd	%xmm2, %xmm4
	addpd	%xmm3, %xmm5
	movupd	%xmm4, -16(%rcx)
	movupd	%xmm5, (%rcx)
	addq	$32, %rdi
	addq	$32, %rcx
	addq	$-4, %rdx
	jne	.LBB12_9
# BB#10:                                # %middle.block
	testl	%r8d, %r8d
	je	.LBB12_17
.LBB12_11:                              # %.lr.ph.preheader32
	movl	%eax, %ecx
	subl	%esi, %ecx
	leaq	-1(%rax), %rdi
	subq	%rsi, %rdi
	andq	$3, %rcx
	je	.LBB12_14
# BB#12:                                # %.lr.ph.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB12_13:                              # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r9,%rsi,8), %xmm1     # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	(%r10,%rsi,8), %xmm1
	movsd	%xmm1, (%r10,%rsi,8)
	incq	%rsi
	incq	%rcx
	jne	.LBB12_13
.LBB12_14:                              # %.lr.ph.prol.loopexit
	cmpq	$3, %rdi
	jb	.LBB12_17
# BB#15:                                # %.lr.ph.preheader32.new
	subq	%rsi, %rax
	leaq	24(%r10,%rsi,8), %rdx
	leaq	24(%r9,%rsi,8), %rcx
	.p2align	4, 0x90
.LBB12_16:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	-24(%rcx), %xmm1        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	-24(%rdx), %xmm1
	movsd	%xmm1, -24(%rdx)
	movsd	-16(%rcx), %xmm1        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	-16(%rdx), %xmm1
	movsd	%xmm1, -16(%rdx)
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	-8(%rdx), %xmm1
	movsd	%xmm1, -8(%rdx)
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	(%rdx), %xmm1
	movsd	%xmm1, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-4, %rax
	jne	.LBB12_16
.LBB12_17:                              # %._crit_edge
	xorl	%eax, %eax
	retq
.Lfunc_end12:
	.size	hypre_SeqVectorAxpy, .Lfunc_end12-hypre_SeqVectorAxpy
	.cfi_endproc

	.globl	hypre_SeqVectorInnerProd
	.p2align	4, 0x90
	.type	hypre_SeqVectorInnerProd,@function
hypre_SeqVectorInnerProd:               # @hypre_SeqVectorInnerProd
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	imull	8(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB13_1
# BB#2:                                 # %.lr.ph.preheader
	movq	(%rdi), %rcx
	movq	(%rsi), %rdx
	movl	%eax, %eax
	leaq	-1(%rax), %r8
	movq	%rax, %rdi
	andq	$3, %rdi
	je	.LBB13_3
# BB#4:                                 # %.lr.ph.prol.preheader
	xorpd	%xmm0, %xmm0
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB13_5:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdx,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	(%rcx,%rsi,8), %xmm1
	addsd	%xmm1, %xmm0
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB13_5
	jmp	.LBB13_6
.LBB13_1:
	xorps	%xmm0, %xmm0
	retq
.LBB13_3:
	xorl	%esi, %esi
	xorpd	%xmm0, %xmm0
.LBB13_6:                               # %.lr.ph.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB13_9
# BB#7:                                 # %.lr.ph.preheader.new
	subq	%rsi, %rax
	leaq	24(%rcx,%rsi,8), %rcx
	leaq	24(%rdx,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB13_8:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	-24(%rdx), %xmm1        # xmm1 = mem[0],zero
	movsd	-16(%rdx), %xmm2        # xmm2 = mem[0],zero
	mulsd	-24(%rcx), %xmm1
	addsd	%xmm0, %xmm1
	mulsd	-16(%rcx), %xmm2
	addsd	%xmm1, %xmm2
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	mulsd	-8(%rcx), %xmm1
	addsd	%xmm2, %xmm1
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%rcx), %xmm0
	addsd	%xmm1, %xmm0
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-4, %rax
	jne	.LBB13_8
.LBB13_9:                               # %._crit_edge
	retq
.Lfunc_end13:
	.size	hypre_SeqVectorInnerProd, .Lfunc_end13-hypre_SeqVectorInnerProd
	.cfi_endproc

	.globl	hypre_VectorSumElts
	.p2align	4, 0x90
	.type	hypre_VectorSumElts,@function
hypre_VectorSumElts:                    # @hypre_VectorSumElts
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB14_1
# BB#2:                                 # %.lr.ph.preheader
	movq	(%rdi), %rcx
	leaq	-1(%rax), %rdx
	movq	%rax, %rdi
	andq	$7, %rdi
	je	.LBB14_3
# BB#4:                                 # %.lr.ph.prol.preheader
	xorpd	%xmm0, %xmm0
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB14_5:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	addsd	(%rcx,%rsi,8), %xmm0
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB14_5
	jmp	.LBB14_6
.LBB14_1:
	xorps	%xmm0, %xmm0
	retq
.LBB14_3:
	xorl	%esi, %esi
	xorpd	%xmm0, %xmm0
.LBB14_6:                               # %.lr.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB14_9
# BB#7:                                 # %.lr.ph.preheader.new
	subq	%rsi, %rax
	leaq	56(%rcx,%rsi,8), %rcx
	.p2align	4, 0x90
.LBB14_8:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	addsd	-56(%rcx), %xmm0
	addsd	-48(%rcx), %xmm0
	addsd	-40(%rcx), %xmm0
	addsd	-32(%rcx), %xmm0
	addsd	-24(%rcx), %xmm0
	addsd	-16(%rcx), %xmm0
	addsd	-8(%rcx), %xmm0
	addsd	(%rcx), %xmm0
	addq	$64, %rcx
	addq	$-8, %rax
	jne	.LBB14_8
.LBB14_9:                               # %._crit_edge
	retq
.Lfunc_end14:
	.size	hypre_VectorSumElts, .Lfunc_end14-hypre_VectorSumElts
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"r"
	.size	.L.str, 2

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%d"
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%le"
	.size	.L.str.2, 4

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"w"
	.size	.L.str.3, 2

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%d\n"
	.size	.L.str.4, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%d vectors of size %d\n"
	.size	.L.str.5, 23

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"vector %d\n"
	.size	.L.str.6, 11

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%.14e\n"
	.size	.L.str.7, 7


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
