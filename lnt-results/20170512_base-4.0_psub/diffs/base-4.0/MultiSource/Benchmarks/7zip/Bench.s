	.text
	.file	"Bench.bc"
	.globl	_ZN18CBenchmarkInStream4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN18CBenchmarkInStream4ReadEPvjPj,@function
_ZN18CBenchmarkInStream4ReadEPvjPj:     # @_ZN18CBenchmarkInStream4ReadEPvjPj
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %r9
	movq	32(%rdi), %r8
	subq	%r9, %r8
	cmpl	$1048576, %edx          # imm = 0x100000
	movl	$1048576, %eax          # imm = 0x100000
	cmovbl	%edx, %eax
	cmpq	%r8, %rax
	cmovbel	%eax, %r8d
	testl	%r8d, %r8d
	je	.LBB0_1
# BB#2:                                 # %.lr.ph
	movl	%r8d, %r10d
	movq	16(%rdi), %rax
	movb	(%rax,%r9), %al
	movb	%al, (%rsi)
	cmpl	$1, %r8d
	je	.LBB0_9
# BB#3:                                 # %._crit_edge23.preheader
	leal	3(%r10), %r11d
	leaq	-2(%r10), %r9
	andq	$3, %r11
	je	.LBB0_4
# BB#5:                                 # %._crit_edge23.prol.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_6:                                # %._crit_edge23.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rdi), %rdx
	addq	24(%rdi), %rdx
	movzbl	1(%rax,%rdx), %edx
	movb	%dl, 1(%rsi,%rax)
	incq	%rax
	cmpq	%rax, %r11
	jne	.LBB0_6
# BB#7:                                 # %._crit_edge23.prol.loopexit.unr-lcssa
	incq	%rax
	cmpq	$3, %r9
	jae	.LBB0_13
	jmp	.LBB0_9
.LBB0_1:
	xorl	%r10d, %r10d
	jmp	.LBB0_10
.LBB0_4:
	movl	$1, %eax
	cmpq	$3, %r9
	jb	.LBB0_9
	.p2align	4, 0x90
.LBB0_13:                               # %._crit_edge23
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rdi), %rdx
	addq	24(%rdi), %rdx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, (%rsi,%rax)
	movq	16(%rdi), %rdx
	addq	24(%rdi), %rdx
	movzbl	1(%rax,%rdx), %edx
	movb	%dl, 1(%rsi,%rax)
	movq	16(%rdi), %rdx
	addq	24(%rdi), %rdx
	movzbl	2(%rax,%rdx), %edx
	movb	%dl, 2(%rsi,%rax)
	movq	16(%rdi), %rdx
	addq	24(%rdi), %rdx
	movzbl	3(%rax,%rdx), %edx
	movb	%dl, 3(%rsi,%rax)
	addq	$4, %rax
	cmpq	%rax, %r10
	jne	.LBB0_13
.LBB0_9:                                # %._crit_edge.loopexit
	movq	24(%rdi), %r9
.LBB0_10:                               # %._crit_edge
	addq	%r9, %r10
	movq	%r10, 24(%rdi)
	testq	%rcx, %rcx
	je	.LBB0_12
# BB#11:
	movl	%r8d, (%rcx)
.LBB0_12:
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	_ZN18CBenchmarkInStream4ReadEPvjPj, .Lfunc_end0-_ZN18CBenchmarkInStream4ReadEPvjPj
	.cfi_endproc

	.globl	_ZN19CBenchmarkOutStream5WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZN19CBenchmarkOutStream5WriteEPKvjPj,@function
_ZN19CBenchmarkOutStream5WriteEPKvjPj:  # @_ZN19CBenchmarkOutStream5WriteEPKvjPj
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r13, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdi, %r15
	movq	16(%r15), %r12
	movl	36(%r15), %edi
	subq	%rdi, %r12
	movl	%edx, %r13d
	cmpq	%r13, %r12
	movq	%r12, %rbx
	cmovaq	%r13, %rbx
	addq	24(%r15), %rdi
	movq	%rbx, %rdx
	callq	memcpy
	addl	%ebx, 36(%r15)
	testq	%r14, %r14
	je	.LBB1_2
# BB#1:
	movl	%ebx, (%r14)
.LBB1_2:
	xorl	%ecx, %ecx
	cmpq	%r12, %r13
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovbel	%ecx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	_ZN19CBenchmarkOutStream5WriteEPKvjPj, .Lfunc_end1-_ZN19CBenchmarkOutStream5WriteEPKvjPj
	.cfi_endproc

	.globl	_ZN13CCrcOutStream5WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZN13CCrcOutStream5WriteEPKvjPj,@function
_ZN13CCrcOutStream5WriteEPKvjPj:        # @_ZN13CCrcOutStream5WriteEPKvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %ebp
	movq	%rdi, %rbx
	movl	12(%rbx), %edi
	movl	%ebp, %edx
	callq	CrcUpdate
	movl	%eax, 12(%rbx)
	testq	%r14, %r14
	je	.LBB2_2
# BB#1:
	movl	%ebp, (%r14)
.LBB2_2:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN13CCrcOutStream5WriteEPKvjPj, .Lfunc_end2-_ZN13CCrcOutStream5WriteEPKvjPj
	.cfi_endproc

	.globl	_ZN18CBenchProgressInfo12SetRatioInfoEPKyS1_
	.p2align	4, 0x90
	.type	_ZN18CBenchProgressInfo12SetRatioInfoEPKyS1_,@function
_ZN18CBenchProgressInfo12SetRatioInfoEPKyS1_: # @_ZN18CBenchProgressInfo12SetRatioInfoEPKyS1_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 48
	subq	$80, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 128
.Lcfi22:
	.cfi_offset %rbx, -48
.Lcfi23:
	.cfi_offset %r12, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	movq	16(%r12), %rbx
	movq	%rbx, %rdi
	callq	pthread_mutex_lock
	movl	40(%rbx), %ebp
	movq	%rbx, %rdi
	callq	pthread_mutex_unlock
	testl	%ebp, %ebp
	je	.LBB3_1
.LBB3_11:
	movl	%ebp, %eax
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_1:
	cmpq	$0, 88(%r12)
	je	.LBB3_2
# BB#3:
	movq	72(%r12), %rax
	movq	%rax, 48(%rsp)
	movdqu	24(%r12), %xmm0
	movdqu	40(%r12), %xmm1
	movdqu	56(%r12), %xmm2
	movdqa	%xmm2, 32(%rsp)
	movdqa	%xmm1, 16(%rsp)
	movdqa	%xmm0, (%rsp)
	movq	$1000000, 8(%rsp)       # imm = 0xF4240
	movq	$1000000, 24(%rsp)      # imm = 0xF4240
	leaq	64(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	testl	%eax, %eax
	je	.LBB3_4
# BB#5:
	xorl	%edi, %edi
	callq	time
	imulq	$1000000, %rax, %rax    # imm = 0xF4240
	jmp	.LBB3_6
.LBB3_2:
	xorl	%ebp, %ebp
	jmp	.LBB3_11
.LBB3_4:
	imulq	$1000000, 64(%rsp), %rax # imm = 0xF4240
	addq	72(%rsp), %rax
.LBB3_6:                                # %_ZL13SetFinishTimeRK10CBenchInfoRS_.exit
	subq	24(%r12), %rax
	movq	%rax, (%rsp)
	callq	clock
	subq	40(%r12), %rax
	movq	%rax, 16(%rsp)
	movq	16(%r12), %rax
	cmpb	$0, 44(%rax)
	je	.LBB3_8
# BB#7:
	movq	(%r15), %rax
	movq	%rax, 32(%rsp)
	movq	(%r14), %rax
	movq	%rax, 40(%rsp)
	movq	88(%r12), %rdi
	movq	(%rdi), %rax
	movq	%rsp, %rsi
	xorl	%edx, %edx
	callq	*(%rax)
	jmp	.LBB3_9
.LBB3_8:
	movdqu	56(%r12), %xmm0
	movq	(%r15), %xmm1           # xmm1 = mem[0],zero
	movq	(%r14), %xmm2           # xmm2 = mem[0],zero
	punpcklqdq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	paddq	%xmm0, %xmm2
	movdqa	%xmm2, 32(%rsp)
	movq	88(%r12), %rdi
	movq	(%rdi), %rax
	movq	%rsp, %rsi
	xorl	%edx, %edx
	callq	*8(%rax)
.LBB3_9:
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB3_11
# BB#10:
	movq	16(%r12), %rbx
	movq	%rbx, %rdi
	callq	pthread_mutex_lock
	movl	%ebp, 40(%rbx)
	movq	%rbx, %rdi
	callq	pthread_mutex_unlock
	jmp	.LBB3_11
.Lfunc_end3:
	.size	_ZN18CBenchProgressInfo12SetRatioInfoEPKyS1_, .Lfunc_end3-_ZN18CBenchProgressInfo12SetRatioInfoEPKyS1_
	.cfi_endproc

	.globl	_Z8GetUsageRK10CBenchInfo
	.p2align	4, 0x90
	.type	_Z8GetUsageRK10CBenchInfo,@function
_Z8GetUsageRK10CBenchInfo:              # @_Z8GetUsageRK10CBenchInfo
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rsi
	movq	(%rdi), %r8
	movq	8(%rdi), %rdx
	cmpq	$1000001, %rcx          # imm = 0xF4241
	jb	.LBB4_1
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	shrq	%rax
	shrq	%rsi
	cmpq	$2000001, %rcx          # imm = 0x1E8481
	movq	%rax, %rcx
	ja	.LBB4_2
	jmp	.LBB4_3
.LBB4_1:
	movq	%rcx, %rax
.LBB4_3:                                # %_ZL13NormalizeValsRyS_.exit
	cmpq	$1000001, %rdx          # imm = 0xF4241
	jb	.LBB4_4
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph.i4
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rdi
	shrq	%rdi
	shrq	%r8
	cmpq	$2000001, %rdx          # imm = 0x1E8481
	movq	%rdi, %rdx
	ja	.LBB4_5
	jmp	.LBB4_6
.LBB4_4:
	movq	%rdx, %rdi
.LBB4_6:                                # %_ZL13NormalizeValsRyS_.exit5
	testq	%rsi, %rsi
	movl	$1, %ecx
	cmoveq	%rcx, %rsi
	testq	%r8, %r8
	cmoveq	%rcx, %r8
	imulq	%rdi, %rax
	imulq	$1000000, %rax, %rax    # imm = 0xF4240
	xorl	%edx, %edx
	divq	%rsi
	xorl	%edx, %edx
	divq	%r8
	retq
.Lfunc_end4:
	.size	_Z8GetUsageRK10CBenchInfo, .Lfunc_end4-_Z8GetUsageRK10CBenchInfo
	.cfi_endproc

	.globl	_Z17GetRatingPerUsageRK10CBenchInfoy
	.p2align	4, 0x90
	.type	_Z17GetRatingPerUsageRK10CBenchInfoy,@function
_Z17GetRatingPerUsageRK10CBenchInfoy:   # @_Z17GetRatingPerUsageRK10CBenchInfoy
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	movq	(%rdi), %rcx
	movq	8(%rdi), %rdi
	cmpq	$1000001, %rax          # imm = 0xF4241
	jb	.LBB5_1
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rdx
	shrq	%rdx
	shrq	%r8
	cmpq	$2000001, %rax          # imm = 0x1E8481
	movq	%rdx, %rax
	ja	.LBB5_2
	jmp	.LBB5_3
.LBB5_1:
	movq	%rax, %rdx
.LBB5_3:                                # %_ZL13NormalizeValsRyS_.exit
	cmpq	$1000001, %rcx          # imm = 0xF4241
	jb	.LBB5_4
	.p2align	4, 0x90
.LBB5_5:                                # %.lr.ph.i4
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	shrq	%rax
	shrq	%rdi
	cmpq	$2000001, %rcx          # imm = 0x1E8481
	movq	%rax, %rcx
	ja	.LBB5_5
	jmp	.LBB5_6
.LBB5_4:
	movq	%rcx, %rax
.LBB5_6:                                # %_ZL13NormalizeValsRyS_.exit5
	testq	%rdi, %rdi
	movl	$1, %ecx
	cmoveq	%rcx, %rdi
	testq	%r8, %r8
	cmoveq	%rcx, %r8
	imulq	%rdx, %rax
	xorl	%edx, %edx
	divq	%rdi
	imulq	%rsi, %rax
	xorl	%edx, %edx
	divq	%r8
	retq
.Lfunc_end5:
	.size	_Z17GetRatingPerUsageRK10CBenchInfoy, .Lfunc_end5-_Z17GetRatingPerUsageRK10CBenchInfoy
	.cfi_endproc

	.globl	_Z17GetCompressRatingjyyy
	.p2align	4, 0x90
	.type	_Z17GetCompressRatingjyyy,@function
_Z17GetCompressRatingjyyy:              # @_Z17GetCompressRatingjyyy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 24
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rcx, %r8
	movl	$8, %r10d
	movl	$3584, %r9d             # imm = 0xE00
.LBB6_1:                                # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_2 Depth 2
	movl	$1, %r11d
	movl	%r10d, %ecx
	shll	%cl, %r11d
	leal	-8(%r10), %ecx
	xorl	%eax, %eax
	movl	$3, %ebx
	.p2align	4, 0x90
.LBB6_2:                                #   Parent Loop BB6_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-3(%rbx), %ebp
	shll	%cl, %ebp
	addl	%r11d, %ebp
	cmpl	%edi, %ebp
	jae	.LBB6_5
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=2
	leal	-2(%rbx), %ebp
	shll	%cl, %ebp
	addl	%r11d, %ebp
	cmpl	%edi, %ebp
	jae	.LBB6_4
# BB#12:                                #   in Loop: Header=BB6_2 Depth=2
	leal	-1(%rbx), %ebp
	shll	%cl, %ebp
	addl	%r11d, %ebp
	cmpl	%edi, %ebp
	jae	.LBB6_13
# BB#14:                                #   in Loop: Header=BB6_2 Depth=2
	movl	%ebx, %ebp
	shll	%cl, %ebp
	addl	%r11d, %ebp
	cmpl	%edi, %ebp
	jae	.LBB6_15
# BB#6:                                 #   in Loop: Header=BB6_2 Depth=2
	addl	$4, %eax
	leal	4(%rbx), %ebp
	incl	%ebx
	cmpl	$256, %ebx              # imm = 0x100
	movl	%ebp, %ebx
	jb	.LBB6_2
# BB#7:                                 #   in Loop: Header=BB6_1 Depth=1
	incl	%r10d
	cmpl	$32, %r10d
	jl	.LBB6_1
	jmp	.LBB6_8
.LBB6_4:
	orl	$1, %eax
	jmp	.LBB6_5
.LBB6_13:
	orl	$2, %eax
	jmp	.LBB6_5
.LBB6_15:
	movl	%ebx, %eax
.LBB6_5:
	shll	$8, %r10d
	leal	-4608(%r10,%rax), %r9d
.LBB6_8:                                # %_ZL10GetLogSizej.exit
	imulq	%r9, %r9
	leaq	(%r9,%r9,4), %rax
	shrq	$16, %rax
	addq	$870, %rax              # imm = 0x366
	imulq	%r8, %rax
	cmpq	$1000001, %rdx          # imm = 0xF4241
	jb	.LBB6_9
	.p2align	4, 0x90
.LBB6_10:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	shrq	%rcx
	shrq	%rsi
	cmpq	$2000001, %rdx          # imm = 0x1E8481
	movq	%rcx, %rdx
	ja	.LBB6_10
	jmp	.LBB6_11
.LBB6_9:
	movq	%rdx, %rcx
.LBB6_11:                               # %_ZL11MyMultDiv64yyy.exit
	testq	%rsi, %rsi
	movl	$1, %edi
	cmovneq	%rsi, %rdi
	imulq	%rcx, %rax
	xorl	%edx, %edx
	divq	%rdi
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_Z17GetCompressRatingjyyy, .Lfunc_end6-_Z17GetCompressRatingjyyy
	.cfi_endproc

	.globl	_Z19GetDecompressRatingyyyyj
	.p2align	4, 0x90
	.type	_Z19GetDecompressRatingyyyyj,@function
_Z19GetDecompressRatingyyyyj:           # @_Z19GetDecompressRatingyyyyj
	.cfi_startproc
# BB#0:
	imulq	$200, %rcx, %rax
	leaq	(%rax,%rdx,4), %rcx
	movl	%r8d, %eax
	imulq	%rcx, %rax
	cmpq	$1000001, %rsi          # imm = 0xF4241
	jb	.LBB7_1
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rcx
	shrq	%rcx
	shrq	%rdi
	cmpq	$2000001, %rsi          # imm = 0x1E8481
	movq	%rcx, %rsi
	ja	.LBB7_2
	jmp	.LBB7_3
.LBB7_1:
	movq	%rsi, %rcx
.LBB7_3:                                # %_ZL11MyMultDiv64yyy.exit
	testq	%rdi, %rdi
	movl	$1, %esi
	cmovneq	%rdi, %rsi
	imulq	%rcx, %rax
	xorl	%edx, %edx
	divq	%rsi
	retq
.Lfunc_end7:
	.size	_Z19GetDecompressRatingyyyyj, .Lfunc_end7-_Z19GetDecompressRatingyyyyj
	.cfi_endproc

	.globl	_ZN12CEncoderInfo4InitEjjP20CBaseRandomGenerator
	.p2align	4, 0x90
	.type	_ZN12CEncoderInfo4InitEjjP20CBaseRandomGenerator,@function
_ZN12CEncoderInfo4InitEjjP20CBaseRandomGenerator: # @_ZN12CEncoderInfo4InitEjjP20CBaseRandomGenerator
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 128
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %r15d
	movq	%rdi, %r13
	movq	%rcx, 200(%r13)
	leal	65536(%r15), %ebp
	movl	%ebp, 164(%r13)
	movq	192(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB8_2
# BB#1:
	cmpq	%rbp, 184(%r13)
	je	.LBB8_3
.LBB8_2:                                # %_ZN12CBenchBuffer5AllocEm.exit
	callq	MidFree
	movq	$0, 192(%r13)
	movq	%rbp, %rdi
	callq	MidAlloc
	movq	%rax, 192(%r13)
	movq	%rbp, 184(%r13)
	testq	%rax, %rax
	je	.LBB8_11
.LBB8_3:                                # %_ZN12CBenchBuffer5AllocEm.exit49
	leaq	176(%r13), %rdi
	shrl	%ebp
	addl	$1024, %ebp             # imm = 0x400
	callq	_ZN21CBenchRandomGenerator8GenerateEv
	movq	184(%r13), %rsi
	movq	192(%r13), %rdi
	callq	CrcCalc
	movl	%eax, 160(%r13)
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	$0, 24(%rbx)
	movl	$0, 32(%rbx)
	movl	$_ZTV19CBenchmarkOutStream+80, %eax
	movd	%rax, %xmm0
	movl	$_ZTV19CBenchmarkOutStream+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	%rbx, 136(%r13)
	movl	%ebp, %ebp
	xorl	%edi, %edi
	callq	MidFree
	movq	$0, 24(%rbx)
	movq	%rbp, %rdi
	callq	MidAlloc
	movq	%rax, 24(%rbx)
	movq	%rbp, 16(%rbx)
	movl	$-2147024882, %r12d     # imm = 0x8007000E
	testq	%rax, %rax
	je	.LBB8_27
# BB#4:                                 # %_ZN12CBenchBuffer5AllocEm.exit49.thread
	movq	136(%r13), %rbp
	testq	%rbp, %rbp
	je	.LBB8_6
# BB#5:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*8(%rax)
.LBB8_6:
	movq	144(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB8_8
# BB#7:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB8_8:                                # %_ZN9CMyComPtrI20ISequentialOutStreamEaSEPS0_.exit
	movq	%rbp, 144(%r13)
	movq	$0, 208(%r13)
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movq	$0, 24(%rbp)
	movl	$_ZTV19CBenchmarkOutStream+80, %eax
	movd	%rax, %xmm0
	movl	$_ZTV19CBenchmarkOutStream+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbp)
	movq	%rbp, 208(%r13)
	movl	$1, 32(%rbp)
	movq	216(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB8_12
# BB#9:                                 # %_ZN9CMyComPtrI20ISequentialOutStreamEaSEPS0_.exit50
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	208(%r13), %rax
	leaq	24(%rax), %rbx
	movq	24(%rax), %rdi
	movq	%rbp, 216(%r13)
	leaq	16(%rax), %rbp
	testq	%rdi, %rdi
	je	.LBB8_13
# BB#10:
	cmpq	$5, 16(%rax)
	jne	.LBB8_14
	jmp	.LBB8_16
.LBB8_11:
	movl	$-2147024882, %r12d     # imm = 0x8007000E
	jmp	.LBB8_27
.LBB8_12:                               # %_ZN9CMyComPtrI20ISequentialOutStreamEaSEPS0_.exit50.thread
	movq	%rbp, 216(%r13)
	leaq	24(%rbp), %rbx
	addq	$16, %rbp
.LBB8_13:
	xorl	%edi, %edi
.LBB8_14:                               # %_ZN12CBenchBuffer5AllocEm.exit54
	callq	MidFree
	movq	$0, (%rbx)
	movl	$5, %edi
	callq	MidAlloc
	movq	%rax, (%rbx)
	movq	$5, (%rbp)
	testq	%rax, %rax
	je	.LBB8_27
# BB#15:                                # %_ZN12CBenchBuffer5AllocEm.exit54._ZN12CBenchBuffer5AllocEm.exit54.thread_crit_edge
	movq	208(%r13), %rax
.LBB8_16:                               # %_ZN12CBenchBuffer5AllocEm.exit54.thread
	movl	$0, 36(%rax)
	movabsq	$55834574849, %rax      # imm = 0xD00000001
	movq	%rax, 24(%rsp)
	movw	$19, 32(%rsp)
	movl	%r15d, 40(%rsp)
	movw	$19, 48(%rsp)
	movl	%r14d, 56(%rsp)
	movq	$0, 16(%rsp)
	movq	32(%r13), %rdi
	movq	(%rdi), %rax
.Ltmp0:
	leaq	16(%rsp), %rdx
	movl	$IID_ICompressSetCoderProperties, %esi
	callq	*(%rax)
	movl	%eax, %r12d
.Ltmp1:
# BB#17:                                # %_ZNK9CMyComPtrI14ICompressCoderE14QueryInterfaceI27ICompressSetCoderPropertiesEEiRK4GUIDPPT_.exit
	testl	%r12d, %r12d
	je	.LBB8_21
.LBB8_18:
	movl	$1, %ebx
.LBB8_19:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_26
# BB#20:
	movq	(%rdi), %rax
	callq	*16(%rax)
	jmp	.LBB8_26
.LBB8_21:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_25
# BB#22:
	movq	(%rdi), %rax
.Ltmp2:
	leaq	24(%rsp), %rsi
	leaq	32(%rsp), %rdx
	movl	$2, %ecx
	callq	*40(%rax)
	movl	%eax, %r12d
.Ltmp3:
# BB#23:
	testl	%r12d, %r12d
	jne	.LBB8_18
# BB#28:
	movq	$0, 8(%rsp)
	movq	32(%r13), %rdi
	movq	(%rdi), %rax
.Ltmp5:
	leaq	8(%rsp), %rdx
	movl	$IID_ICompressWriteCoderProperties, %esi
	callq	*(%rax)
.Ltmp6:
# BB#29:                                # %_ZNK9CMyComPtrI14ICompressCoderE14QueryInterfaceI29ICompressWriteCoderPropertiesEEiRK4GUIDPPT_.exit
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_33
# BB#30:
	movq	(%rdi), %rax
	movq	216(%r13), %rsi
.Ltmp7:
	callq	*40(%rax)
	movl	%eax, %r12d
.Ltmp8:
# BB#31:
	testl	%r12d, %r12d
	je	.LBB8_33
# BB#32:
	movl	$1, %ebx
	jmp	.LBB8_34
.LBB8_25:
	movl	$1, %ebx
	movl	$-2147467259, %r12d     # imm = 0x80004005
.LBB8_26:                               # %_ZN9CMyComPtrI27ICompressSetCoderPropertiesED2Ev.exit56
	testl	%ebx, %ebx
	cmovel	%ebx, %r12d
.LBB8_27:
	movl	%r12d, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_33:
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
.LBB8_34:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_19
# BB#35:
	movq	(%rdi), %rax
.Ltmp12:
	callq	*16(%rax)
.Ltmp13:
	jmp	.LBB8_19
.LBB8_36:
.Ltmp14:
	jmp	.LBB8_40
.LBB8_37:
.Ltmp9:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_41
# BB#38:
	movq	(%rdi), %rax
.Ltmp10:
	callq	*16(%rax)
.Ltmp11:
	jmp	.LBB8_41
.LBB8_39:
.Ltmp4:
.LBB8_40:
	movq	%rax, %rbx
.LBB8_41:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_43
# BB#42:
	movq	(%rdi), %rax
.Ltmp15:
	callq	*16(%rax)
.Ltmp16:
.LBB8_43:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB8_44:
.Ltmp17:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN12CEncoderInfo4InitEjjP20CBaseRandomGenerator, .Lfunc_end8-_ZN12CEncoderInfo4InitEjjP20CBaseRandomGenerator
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp2-.Ltmp1           #   Call between .Ltmp1 and .Ltmp2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp2-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp3-.Ltmp2           #   Call between .Ltmp2 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp8-.Ltmp5           #   Call between .Ltmp5 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp16-.Ltmp10         #   Call between .Ltmp10 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Lfunc_end8-.Ltmp16     #   Call between .Ltmp16 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN21CBenchRandomGenerator8GenerateEv,"axG",@progbits,_ZN21CBenchRandomGenerator8GenerateEv,comdat
	.weak	_ZN21CBenchRandomGenerator8GenerateEv
	.p2align	4, 0x90
	.type	_ZN21CBenchRandomGenerator8GenerateEv,@function
_ZN21CBenchRandomGenerator8GenerateEv:  # @_ZN21CBenchRandomGenerator8GenerateEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 56
.Lcfi50:
	.cfi_offset %rbx, -56
.Lcfi51:
	.cfi_offset %r12, -48
.Lcfi52:
	.cfi_offset %r13, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movq	8(%rdi), %r11
	testq	%r11, %r11
	je	.LBB9_7
# BB#1:                                 # %.lr.ph.lr.ph
	movl	$1, %r9d
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB9_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_10 Depth 2
                                        #     Child Loop BB9_16 Depth 2
	movq	24(%rdi), %r14
	movl	(%r14), %ebp
	movzwl	%bp, %ecx
	imull	$36969, %ecx, %ecx      # imm = 0x9069
	shrl	$16, %ebp
	addl	%ecx, %ebp
	movl	%ebp, (%r14)
	movl	%ebp, %r13d
	shll	$16, %r13d
	movl	4(%r14), %edx
	movzwl	%dx, %ecx
	imull	$18000, %ecx, %ecx      # imm = 0x4650
	shrl	$16, %edx
	addl	%ecx, %edx
	movl	%edx, 4(%r14)
	addl	%edx, %r13d
	movl	%r13d, %ecx
	shrl	$2, %ecx
	cmpl	$1024, %r12d            # imm = 0x400
	jb	.LBB9_6
# BB#5:                                 #   in Loop: Header=BB9_4 Depth=1
	movl	%r13d, %esi
	andl	$2, %esi
	jne	.LBB9_8
.LBB9_6:                                # %.critedge
                                        #   in Loop: Header=BB9_4 Depth=1
	movq	16(%rdi), %rdx
	incl	%r12d
	movb	%cl, (%rdx,%rax)
	movq	8(%rdi), %r11
	cmpq	%r11, %r12
	movq	%r12, %rax
	jb	.LBB9_4
	jmp	.LBB9_7
.LBB9_8:                                #   in Loop: Header=BB9_4 Depth=1
	andl	$3, %ecx
	shrl	$4, %r13d
	leal	1(%rcx), %eax
	movl	$2, %r10d
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %r10d
	addl	$268435455, %r10d       # imm = 0xFFFFFFF
	andl	%r13d, %r10d
	movl	%eax, %ecx
	shrl	%cl, %r13d
	incl	%r10d
	testb	$7, %r13b
	je	.LBB9_14
# BB#9:                                 #   in Loop: Header=BB9_4 Depth=1
	movl	%r13d, %ecx
	shrl	$3, %ecx
	andl	$3, %ecx
	shrl	$5, %r13d
	leal	1(%rcx), %r8d
	movl	$2, %r15d
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %r15d
	addl	$4194303, %r15d         # imm = 0x3FFFFF
	movl	%r13d, %eax
	movl	%r8d, %ecx
	shrl	%cl, %eax
	.p2align	4, 0x90
.LBB9_10:                               # %._crit_edge
                                        #   Parent Loop BB9_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	andl	$31, %eax
	leal	6(%rax), %ecx
	movzwl	%bp, %esi
	imull	$36969, %esi, %esi      # imm = 0x9069
	shrl	$16, %ebp
	addl	%esi, %ebp
	movl	%ebp, %ebx
	shll	$16, %ebx
	movzwl	%dx, %esi
	imull	$18000, %esi, %esi      # imm = 0x4650
	shrl	$16, %edx
	addl	%esi, %edx
	addl	%edx, %ebx
	cmpl	$30, %ecx
	ja	.LBB9_12
# BB#11:                                #   in Loop: Header=BB9_10 Depth=2
	movl	$64, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	decl	%esi
	movl	%ebx, %r9d
	andl	%esi, %r9d
	movzwl	%bp, %eax
	imull	$36969, %eax, %eax      # imm = 0x9069
	shrl	$16, %ebp
	addl	%eax, %ebp
	movl	%ebp, %ebx
	shll	$16, %ebx
	movzwl	%dx, %eax
	imull	$18000, %eax, %eax      # imm = 0x4650
	shrl	$16, %edx
	addl	%eax, %edx
	addl	%edx, %ebx
.LBB9_12:                               #   in Loop: Header=BB9_10 Depth=2
	cmpl	%r12d, %r9d
	movl	%ebx, %eax
	jae	.LBB9_10
# BB#13:                                #   in Loop: Header=BB9_4 Depth=1
	movl	%ebp, (%r14)
	movl	%edx, 4(%r14)
	andl	%r13d, %r15d
	addl	%r15d, %r10d
	incl	%r9d
.LBB9_14:                               #   in Loop: Header=BB9_4 Depth=1
	movl	%r12d, %edx
	cmpq	%r11, %rdx
	jae	.LBB9_3
# BB#15:                                # %.lr.ph106.preheader
                                        #   in Loop: Header=BB9_4 Depth=1
	movl	%r12d, %eax
	subl	%r9d, %eax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB9_16:                               # %.lr.ph106
                                        #   Parent Loop BB9_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %ecx
	movq	16(%rdi), %rsi
	leal	(%rax,%rcx), %ebp
	movzbl	(%rsi,%rbp), %ebx
	movb	%bl, (%rsi,%rdx)
	leal	1(%rcx), %esi
	movq	8(%rdi), %r11
	cmpl	%r10d, %esi
	jae	.LBB9_2
# BB#17:                                # %._crit_edge60
                                        #   in Loop: Header=BB9_16 Depth=2
	leal	1(%r12,%rcx), %edx
	movl	%edx, %edx
	cmpq	%r11, %rdx
	jb	.LBB9_16
.LBB9_2:                                # %.critedge.loopexit.loopexit
                                        #   in Loop: Header=BB9_4 Depth=1
	leal	1(%r12,%rcx), %r12d
.LBB9_3:                                # %.critedge.loopexit
                                        #   in Loop: Header=BB9_4 Depth=1
	movl	%r12d, %eax
	cmpq	%r11, %rax
	jb	.LBB9_4
.LBB9_7:                                # %.critedge.outer._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	_ZN21CBenchRandomGenerator8GenerateEv, .Lfunc_end9-_ZN21CBenchRandomGenerator8GenerateEv
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end10:
	.size	__clang_call_terminate, .Lfunc_end10-__clang_call_terminate

	.text
	.globl	_ZN12CEncoderInfo6EncodeEv
	.p2align	4, 0x90
	.type	_ZN12CEncoderInfo6EncodeEv,@function
_ZN12CEncoderInfo6EncodeEv:             # @_ZN12CEncoderInfo6EncodeEv
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 32
.Lcfi59:
	.cfi_offset %rbx, -32
.Lcfi60:
	.cfi_offset %r14, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %r14
	movq	$_ZTV18CBenchmarkInStream+16, (%r14)
	movl	$1, 8(%r14)
	movq	184(%rbx), %rax
	movq	192(%rbx), %rcx
	movq	%rcx, 16(%r14)
	movq	%rax, 32(%r14)
	movq	$0, 24(%r14)
	movq	136(%rbx), %rax
	movl	$0, 36(%rax)
	movq	32(%rbx), %rdi
	movq	56(%rbx), %r9
	movq	(%rdi), %rax
	movq	144(%rbx), %rdx
.Ltmp18:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp19:
# BB#1:
	testl	%ebp, %ebp
	jne	.LBB11_5
# BB#2:
	movq	136(%rbx), %rax
	movl	36(%rax), %eax
	movl	%eax, 168(%rbx)
	movq	32(%rbx), %rdi
	xorl	%ebp, %ebp
	testq	%rdi, %rdi
	je	.LBB11_5
# BB#3:
	movq	(%rdi), %rax
.Ltmp20:
	callq	*16(%rax)
.Ltmp21:
# BB#4:                                 # %.noexc
	movq	$0, 32(%rbx)
.LBB11_5:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*16(%rax)
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB11_6:
.Ltmp22:
	movq	%rax, %rbx
	movq	(%r14), %rax
.Ltmp23:
	movq	%r14, %rdi
	callq	*16(%rax)
.Ltmp24:
# BB#7:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB11_8:
.Ltmp25:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end11:
	.size	_ZN12CEncoderInfo6EncodeEv, .Lfunc_end11-_ZN12CEncoderInfo6EncodeEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp18-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp21-.Ltmp18         #   Call between .Ltmp18 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin1   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp23-.Ltmp21         #   Call between .Ltmp21 and .Ltmp23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin1   #     jumps to .Ltmp25
	.byte	1                       #   On action: 1
	.long	.Ltmp24-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end11-.Ltmp24    #   Call between .Ltmp24 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI12_0:
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.text
	.globl	_ZN12CEncoderInfo6DecodeEj
	.p2align	4, 0x90
	.type	_ZN12CEncoderInfo6DecodeEj,@function
_ZN12CEncoderInfo6DecodeEj:             # @_ZN12CEncoderInfo6DecodeEj
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi65:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi66:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi68:
	.cfi_def_cfa_offset 80
.Lcfi69:
	.cfi_offset %rbx, -56
.Lcfi70:
	.cfi_offset %r12, -48
.Lcfi71:
	.cfi_offset %r13, -40
.Lcfi72:
	.cfi_offset %r14, -32
.Lcfi73:
	.cfi_offset %r15, -24
.Lcfi74:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %r14
	movq	$_ZTV18CBenchmarkInStream+16, (%r14)
	movl	$1, 8(%r14)
	movl	%ebp, %eax
	movq	$0, (%rsp)
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	112(%rbx,%rax,8), %rdi
	movq	(%rdi), %rax
.Ltmp26:
	movq	%rsp, %rdx
	movl	$IID_ICompressSetDecoderProperties2, %esi
	callq	*(%rax)
.Ltmp27:
# BB#1:                                 # %_ZNK9CMyComPtrI14ICompressCoderE14QueryInterfaceI30ICompressSetDecoderProperties2EEiRK4GUIDPPT_.exit
	movq	(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB12_2
# BB#3:
.Ltmp28:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp29:
# BB#4:                                 # %_ZN9CMyComPtrI20ISequentialOutStreamEC2EPS0_.exit
	movq	$_ZTV13CCrcOutStream+16, (%r15)
	movl	$1, 8(%r15)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	40(%rbx,%rax,8), %r12
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 56(%r12)
	cmpl	$0, 72(%rbx)
	je	.LBB12_14
# BB#5:                                 # %.lr.ph
	addq	$56, %r12
	movl	168(%rbx), %eax
	movl	$1, %r13d
	jmp	.LBB12_6
	.p2align	4, 0x90
.LBB12_13:                              # %.thread._crit_edge
                                        #   in Loop: Header=BB12_6 Depth=1
	movq	(%rsp), %rbp
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %eax
	incl	%r13d
.LBB12_6:                               # =>This Inner Loop Header: Depth=1
	movq	136(%rbx), %rcx
	movq	24(%rcx), %rcx
	movl	%eax, %eax
	movq	%rcx, 16(%r14)
	movq	%rax, 32(%r14)
	movq	$0, 24(%r14)
	movl	$-1, 12(%r15)
	movq	(%rbp), %rax
	movq	208(%rbx), %rcx
	movq	24(%rcx), %rsi
	movl	36(%rcx), %edx
.Ltmp31:
	movq	%rbp, %rdi
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp32:
# BB#7:                                 #   in Loop: Header=BB12_6 Depth=1
	testl	%ebp, %ebp
	jne	.LBB12_17
# BB#8:                                 #   in Loop: Header=BB12_6 Depth=1
	movl	164(%rbx), %eax
	movq	%rax, 16(%rsp)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	112(%rbx,%rcx,8), %rdi
	movq	(%rdi), %rax
	movq	56(%rbx,%rcx,8), %r9
.Ltmp34:
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r15, %rdx
	leaq	16(%rsp), %r8
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp35:
# BB#9:                                 #   in Loop: Header=BB12_6 Depth=1
	testl	%ebp, %ebp
	jne	.LBB12_17
# BB#10:                                #   in Loop: Header=BB12_6 Depth=1
	movl	12(%r15), %eax
	notl	%eax
	cmpl	%eax, 160(%rbx)
	jne	.LBB12_11
# BB#12:                                # %.thread
                                        #   in Loop: Header=BB12_6 Depth=1
	movq	164(%rbx), %xmm0        # xmm0 = mem[0],zero
	pshufd	$212, %xmm0, %xmm0      # xmm0 = xmm0[0,1,1,3]
	movdqa	%xmm0, %xmm1
	pand	.LCPI12_0(%rip), %xmm1
	movdqu	(%r12), %xmm2
	paddq	%xmm1, %xmm2
	movdqu	%xmm2, (%r12)
	cmpl	72(%rbx), %r13d
	jb	.LBB12_13
.LBB12_14:                              # %._crit_edge
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	112(%rbx,%rax,8), %rdi
	xorl	%ebp, %ebp
	testq	%rdi, %rdi
	je	.LBB12_17
# BB#15:
	movq	(%rdi), %rax
.Ltmp37:
	callq	*16(%rax)
.Ltmp38:
# BB#16:                                # %.noexc78
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	$0, 112(%rbx,%rax,8)
	jmp	.LBB12_17
.LBB12_2:
	movl	$-2147467259, %ebp      # imm = 0x80004005
	jmp	.LBB12_20
.LBB12_11:                              # %.thread90
	movl	$1, %ebp
.LBB12_17:                              # %_ZN9CMyComPtrI14ICompressCoderE7ReleaseEv.exit
	movq	(%r15), %rax
.Ltmp42:
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp43:
# BB#18:                                # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit77
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_20
# BB#19:
	movq	(%rdi), %rax
.Ltmp47:
	callq	*16(%rax)
.Ltmp48:
.LBB12_20:                              # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit71
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*16(%rax)
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_33:
.Ltmp39:
	jmp	.LBB12_22
.LBB12_24:
.Ltmp49:
	movq	%rax, %rbx
	jmp	.LBB12_29
.LBB12_23:
.Ltmp44:
	jmp	.LBB12_26
.LBB12_25:
.Ltmp30:
.LBB12_26:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	%rax, %rbx
	jmp	.LBB12_27
.LBB12_32:
.Ltmp36:
	jmp	.LBB12_22
.LBB12_21:
.Ltmp33:
.LBB12_22:
	movq	%rax, %rbx
	movq	(%r15), %rax
.Ltmp40:
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp41:
.LBB12_27:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_29
# BB#28:
	movq	(%rdi), %rax
.Ltmp45:
	callq	*16(%rax)
.Ltmp46:
.LBB12_29:
	movq	(%r14), %rax
.Ltmp50:
	movq	%r14, %rdi
	callq	*16(%rax)
.Ltmp51:
# BB#30:                                # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB12_31:
.Ltmp52:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZN12CEncoderInfo6DecodeEj, .Lfunc_end12-_ZN12CEncoderInfo6DecodeEj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp26-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp29-.Ltmp26         #   Call between .Ltmp26 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin2   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin2   #     jumps to .Ltmp33
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin2   #     jumps to .Ltmp36
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin2   #     jumps to .Ltmp39
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin2   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin2   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp40-.Ltmp48         #   Call between .Ltmp48 and .Ltmp40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Ltmp51-.Ltmp40         #   Call between .Ltmp40 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin2   #     jumps to .Ltmp52
	.byte	1                       #   On action: 1
	.long	.Ltmp51-.Lfunc_begin2   # >> Call Site 10 <<
	.long	.Lfunc_end12-.Ltmp51    #   Call between .Ltmp51 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_Z9LzmaBenchjjP14IBenchCallback
	.p2align	4, 0x90
	.type	_Z9LzmaBenchjjP14IBenchCallback,@function
_Z9LzmaBenchjjP14IBenchCallback:        # @_Z9LzmaBenchjjP14IBenchCallback
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi75:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi78:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi79:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi81:
	.cfi_def_cfa_offset 256
.Lcfi82:
	.cfi_offset %rbx, -56
.Lcfi83:
	.cfi_offset %r12, -48
.Lcfi84:
	.cfi_offset %r13, -40
.Lcfi85:
	.cfi_offset %r14, -32
.Lcfi86:
	.cfi_offset %r15, -24
.Lcfi87:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%edi, %edx
	shrl	%edx
	xorl	%r13d, %r13d
	cmpl	$1, %edi
	seta	%al
	movl	$1, %ecx
	cmoval	%edx, %ecx
	testl	%edi, %edi
	movl	$-2147024809, %r15d     # imm = 0x80070057
	je	.LBB13_84
# BB#1:
	cmpl	$262144, %esi           # imm = 0x40000
	jb	.LBB13_84
# BB#2:
	cmpl	$65536, %ecx            # imm = 0x10000
	ja	.LBB13_84
# BB#3:
	movl	%esi, 72(%rsp)          # 4-byte Spill
	movl	%edi, 24(%rsp)          # 4-byte Spill
	movl	%edx, 84(%rsp)          # 4-byte Spill
	movb	%al, %r13b
	incl	%r13d
	movl	%ecx, %r12d
	movl	%ecx, %r15d
	imulq	$224, %r12, %rbp
	leaq	8(%rbp), %rdi
	callq	_Znam
	movq	%rax, %rbx
	movq	%r12, (%rbx)
	leaq	8(%rbx), %rdx
	movl	%r15d, 12(%rsp)         # 4-byte Spill
	testl	%r15d, %r15d
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	je	.LBB13_20
# BB#4:
	movq	%r12, 16(%rsp)          # 8-byte Spill
	leaq	216(%rbx), %rax
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB13_5:                               # =>This Inner Loop Header: Depth=1
	movl	$0, -200(%rax)
	movl	$0, -184(%rax)
	movq	$0, -176(%rax)
	movdqu	%xmm0, -152(%rax)
	movdqu	%xmm0, -96(%rax)
	movq	$0, -16(%rax)
	movdqu	%xmm0, -72(%rax)
	movq	$0, -56(%rax)
	movq	$_ZTV21CBenchRandomGenerator+16, -32(%rax)
	movdqu	%xmm0, (%rax)
	addq	$224, %rax
	addq	$-224, %rbp
	jne	.LBB13_5
# BB#6:                                 # %.lr.ph455.preheader
	movl	%r13d, 8(%rsp)          # 4-byte Spill
	movl	%r13d, %r12d
	leaq	120(%rbx), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
.LBB13_7:                               # %.lr.ph455
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_11 Depth 2
	testq	%r13, %r13
	movl	$0, %eax
	cmoveq	%r14, %rax
	imulq	$224, %r13, %rcx
	movq	%rax, 152(%rdx,%rcx)
	leaq	32(%rdx,%rcx), %rbp
.Ltmp53:
	movl	$196865, %edi           # imm = 0x30101
	movl	$1, %edx
	movq	%rbp, %rsi
	callq	_Z11CreateCoderyR9CMyComPtrI14ICompressCoderEb
	movl	%eax, %r15d
.Ltmp54:
# BB#8:                                 #   in Loop: Header=BB13_7 Depth=1
	testl	%r15d, %r15d
	jne	.LBB13_79
# BB#9:                                 #   in Loop: Header=BB13_7 Depth=1
	movl	$-2147467263, %r15d     # imm = 0x80004001
	cmpq	$0, (%rbp)
	je	.LBB13_79
# BB#10:                                # %.preheader394.preheader
                                        #   in Loop: Header=BB13_7 Depth=1
	movq	48(%rsp), %r14          # 8-byte Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_11:                              # %.preheader394
                                        #   Parent Loop BB13_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
.Ltmp56:
	movl	$196865, %edi           # imm = 0x30101
	xorl	%edx, %edx
	movq	%r14, %rsi
	callq	_Z11CreateCoderyR9CMyComPtrI14ICompressCoderEb
.Ltmp57:
# BB#12:                                #   in Loop: Header=BB13_11 Depth=2
	testl	%eax, %eax
	jne	.LBB13_28
# BB#13:                                #   in Loop: Header=BB13_11 Depth=2
	cmpq	$0, (%r14)
	je	.LBB13_79
# BB#14:                                #   in Loop: Header=BB13_11 Depth=2
	incq	%rbp
	addq	$8, %r14
	cmpq	%r12, %rbp
	jb	.LBB13_11
# BB#15:                                # %.thread
                                        #   in Loop: Header=BB13_7 Depth=1
	incq	%r13
	addq	$224, 48(%rsp)          # 8-byte Folded Spill
	cmpq	16(%rsp), %r13          # 8-byte Folded Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	jb	.LBB13_7
# BB#16:                                # %.lr.ph450.preheader
	movabsq	$2238917613694113253, %rax # imm = 0x1F123BB5159A55E5
	movq	%rax, 144(%rsp)
	xorl	%ebp, %ebp
	movq	%rdx, %r14
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	8(%rsp), %r13d          # 4-byte Reload
.LBB13_17:                              # %.lr.ph450
                                        # =>This Inner Loop Header: Depth=1
.Ltmp59:
	movq	%r14, %rdi
	movl	72(%rsp), %esi          # 4-byte Reload
	movl	24(%rsp), %edx          # 4-byte Reload
	leaq	144(%rsp), %rcx
	callq	_ZN12CEncoderInfo4InitEjjP20CBaseRandomGenerator
	movl	%eax, %r15d
.Ltmp60:
# BB#18:                                #   in Loop: Header=BB13_17 Depth=1
	testl	%r15d, %r15d
	jne	.LBB13_79
# BB#19:                                #   in Loop: Header=BB13_17 Depth=1
	incq	%rbp
	addq	$224, %r14
	cmpq	%r12, %rbp
	jb	.LBB13_17
	jmp	.LBB13_21
.LBB13_20:                              # %_ZN14CBenchEncodersC2Ej.exit._crit_edge.thread
	movabsq	$2238917613694113253, %rax # imm = 0x1F123BB5159A55E5
	movq	%rax, 144(%rsp)
.LBB13_21:                              # %._crit_edge451
.Ltmp62:
	leaq	152(%rsp), %r15
	movq	%r15, %rdi
	callq	CriticalSection_Init
.Ltmp63:
# BB#22:                                # %_ZN20CBenchProgressStatusC2Ev.exit
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	movl	$0, 192(%rsp)
	movb	$1, 196(%rsp)
	je	.LBB13_63
# BB#23:                                # %.lr.ph441
	cmpl	$1, 12(%rsp)            # 4-byte Folded Reload
	jne	.LBB13_29
# BB#24:                                # %.lr.ph441.split
.Ltmp80:
	movl	$96, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp81:
	movq	32(%rsp), %r14          # 8-byte Reload
# BB#25:                                # %.noexc
	movq	$_ZTV18CBenchProgressInfo+16, (%rbp)
	movl	$0, 72(%rbp)
	movq	$0, 88(%rbp)
	movq	%rbp, 48(%rbx)
	movl	$1, 8(%rbp)
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB13_52
# BB#26:
	movq	(%rdi), %rax
.Ltmp82:
	callq	*16(%rax)
.Ltmp83:
# BB#27:                                # %._crit_edge575
	movq	48(%rbx), %rax
	jmp	.LBB13_53
.LBB13_28:
	movl	%eax, %r15d
	jmp	.LBB13_79
.LBB13_29:                              # %.lr.ph441.split.us.preheader
	movl	%r13d, 8(%rsp)          # 4-byte Spill
	leaq	72(%rbx), %r14
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB13_30:                              # %.lr.ph441.split.us
                                        # =>This Inner Loop Header: Depth=1
.Ltmp65:
	movl	$96, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp66:
# BB#31:                                # %.noexc.us
                                        #   in Loop: Header=BB13_30 Depth=1
	movq	$_ZTV18CBenchProgressInfo+16, (%rbp)
	movl	$0, 72(%rbp)
	movq	$0, 88(%rbp)
	movq	%rbp, -24(%r14)
	movl	$1, 8(%rbp)
	movq	-8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB13_34
# BB#32:                                #   in Loop: Header=BB13_30 Depth=1
	movq	(%rdi), %rax
.Ltmp67:
	callq	*16(%rax)
.Ltmp68:
# BB#33:                                # %._crit_edge572
                                        #   in Loop: Header=BB13_30 Depth=1
	movq	-24(%r14), %rax
	jmp	.LBB13_35
.LBB13_34:                              #   in Loop: Header=BB13_30 Depth=1
	movq	%rbp, %rax
.LBB13_35:                              #   in Loop: Header=BB13_30 Depth=1
	movq	%rbp, -8(%r14)
	movq	%r15, 16(%rax)
.Ltmp69:
	movl	$96, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp70:
# BB#36:                                # %.noexc.us.1
                                        #   in Loop: Header=BB13_30 Depth=1
	movq	$_ZTV18CBenchProgressInfo+16, (%rbp)
	movl	$0, 72(%rbp)
	movq	$0, 88(%rbp)
	movq	%rbp, -16(%r14)
	movl	$1, 8(%rbp)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB13_39
# BB#37:                                #   in Loop: Header=BB13_30 Depth=1
	movq	(%rdi), %rax
.Ltmp71:
	callq	*16(%rax)
.Ltmp72:
# BB#38:                                # %._crit_edge573
                                        #   in Loop: Header=BB13_30 Depth=1
	movq	-16(%r14), %rax
	jmp	.LBB13_40
.LBB13_39:                              #   in Loop: Header=BB13_30 Depth=1
	movq	%rbp, %rax
.LBB13_40:                              #   in Loop: Header=BB13_30 Depth=1
	movq	%rbp, (%r14)
	movq	%r15, 16(%rax)
	testq	%r13, %r13
	jne	.LBB13_45
# BB#41:                                #   in Loop: Header=BB13_30 Depth=1
	movq	48(%rbx), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 88(%rax)
	movq	48(%rbx), %rbp
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, 72(%rbp)
	movq	$1000000, 32(%rbp)      # imm = 0xF4240
	movq	$1000000, 48(%rbp)      # imm = 0xF4240
	xorl	%esi, %esi
	leaq	88(%rsp), %rdi
	callq	gettimeofday
	testl	%eax, %eax
	je	.LBB13_43
# BB#42:                                #   in Loop: Header=BB13_30 Depth=1
	xorl	%edi, %edi
	callq	time
	imulq	$1000000, %rax, %rax    # imm = 0xF4240
	jmp	.LBB13_44
.LBB13_43:                              #   in Loop: Header=BB13_30 Depth=1
	imulq	$1000000, 88(%rsp), %rax # imm = 0xF4240
	addq	96(%rsp), %rax
.LBB13_44:                              # %_ZL12SetStartTimeR10CBenchInfo.exit.us
                                        #   in Loop: Header=BB13_30 Depth=1
	movq	%rax, 24(%rbp)
	callq	clock
	movq	%rax, 40(%rbp)
.LBB13_45:                              #   in Loop: Header=BB13_30 Depth=1
	movq	%r15, %rbp
	leaq	-64(%r14), %rdi
.Ltmp74:
	movl	$_ZN12CEncoderInfo20EncodeThreadFunctionEPv, %esi
	movq	%rdi, %rdx
	callq	Thread_Create
	movl	%eax, %r15d
.Ltmp75:
# BB#46:                                # %_ZN12CEncoderInfo19CreateEncoderThreadEv.exit.us
                                        #   in Loop: Header=BB13_30 Depth=1
	testl	%r15d, %r15d
	jne	.LBB13_78
# BB#47:                                # %.thread367.us
                                        #   in Loop: Header=BB13_30 Depth=1
	incq	%r13
	addq	$224, %r14
	cmpq	%r12, %r13
	movq	%rbp, %r15
	jb	.LBB13_30
# BB#48:                                # %._crit_edge442
	cmpl	$2, 12(%rsp)            # 4-byte Folded Reload
	movl	8(%rsp), %r13d          # 4-byte Reload
	jb	.LBB13_63
# BB#49:                                # %.lr.ph438.preheader
	xorl	%r14d, %r14d
	movq	40(%rsp), %rbp          # 8-byte Reload
.LBB13_50:                              # %.lr.ph438
                                        # =>This Inner Loop Header: Depth=1
.Ltmp77:
	movq	%rbp, %rdi
	callq	Thread_Wait
.Ltmp78:
# BB#51:                                # %_ZN8NWindows7CThread4WaitEv.exit
                                        #   in Loop: Header=BB13_50 Depth=1
	incq	%r14
	addq	$224, %rbp
	cmpq	%r12, %r14
	jb	.LBB13_50
	jmp	.LBB13_63
.LBB13_52:
	movq	%rbp, %rax
.LBB13_53:
	movq	%rbp, 64(%rbx)
	movq	%r15, 16(%rax)
.Ltmp84:
	movl	$96, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp85:
# BB#54:                                # %.noexc.1
	movq	$_ZTV18CBenchProgressInfo+16, (%rbp)
	movl	$0, 72(%rbp)
	movq	$0, 88(%rbp)
	movq	%rbp, 56(%rbx)
	movl	$1, 8(%rbp)
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB13_57
# BB#55:
	movq	(%rdi), %rax
.Ltmp86:
	callq	*16(%rax)
.Ltmp87:
# BB#56:                                # %._crit_edge577
	movq	56(%rbx), %rax
	jmp	.LBB13_58
.LBB13_57:
	movq	%rbp, %rax
.LBB13_58:
	movq	%rbp, 72(%rbx)
	movq	%r15, 16(%rax)
	movq	48(%rbx), %rax
	movq	%r14, 88(%rax)
	movq	48(%rbx), %rbp
	movl	$1, 72(%rbp)
	movq	$1000000, 32(%rbp)      # imm = 0xF4240
	movq	$1000000, 48(%rbp)      # imm = 0xF4240
	leaq	88(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	testl	%eax, %eax
	je	.LBB13_60
# BB#59:
	xorl	%edi, %edi
	callq	time
	imulq	$1000000, %rax, %rax    # imm = 0xF4240
	jmp	.LBB13_61
.LBB13_60:
	imulq	$1000000, 88(%rsp), %rax # imm = 0xF4240
	addq	96(%rsp), %rax
.LBB13_61:
	movq	%rax, 24(%rbp)
	callq	clock
	movq	%rax, 40(%rbp)
.Ltmp89:
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	_ZN12CEncoderInfo6EncodeEv
	movl	%eax, %r15d
.Ltmp90:
# BB#62:
	testl	%r15d, %r15d
	jne	.LBB13_78
.LBB13_63:                              # %.loopexit390
	movl	192(%rsp), %r15d
	testl	%r15d, %r15d
	jne	.LBB13_78
# BB#64:
	movl	$0, 136(%rsp)
	movq	48(%rbx), %rbp
	movq	$1000000, 96(%rsp)      # imm = 0xF4240
	movq	$1000000, 112(%rsp)     # imm = 0xF4240
	leaq	56(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	testl	%eax, %eax
	je	.LBB13_66
# BB#65:
	xorl	%edi, %edi
	callq	time
	imulq	$1000000, %rax, %rax    # imm = 0xF4240
	jmp	.LBB13_67
.LBB13_66:
	imulq	$1000000, 56(%rsp), %rax # imm = 0xF4240
	addq	64(%rsp), %rax
.LBB13_67:
	movq	32(%rsp), %r14          # 8-byte Reload
	subq	24(%rbp), %rax
	movq	%rax, 88(%rsp)
	callq	clock
	subq	40(%rbp), %rax
	movl	12(%rsp), %ebp          # 4-byte Reload
	testl	%ebp, %ebp
	movq	%rax, 104(%rsp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 120(%rsp)
	movl	$1, 136(%rsp)
	je	.LBB13_76
# BB#68:                                # %.lr.ph431.preheader
	leaq	-1(%r12), %rax
	movq	%r12, %rdx
	andq	$3, %rdx
	je	.LBB13_71
# BB#69:                                # %.lr.ph431.prol.preheader
	leaq	172(%rbx), %rsi
	pxor	%xmm1, %xmm1
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB13_70:                              # %.lr.ph431.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %xmm2           # xmm2 = mem[0],zero
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	paddq	%xmm2, %xmm0
	incq	%rcx
	addq	$224, %rsi
	cmpq	%rcx, %rdx
	jne	.LBB13_70
	jmp	.LBB13_72
.LBB13_71:
	xorl	%ecx, %ecx
.LBB13_72:                              # %.lr.ph431.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB13_75
# BB#73:                                # %.lr.ph431.preheader.new
	movq	%r12, %rsi
	subq	%rcx, %rsi
	imulq	$224, %rcx, %rax
	leaq	844(%rbx,%rax), %rcx
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB13_74:                              # %.lr.ph431
                                        # =>This Inner Loop Header: Depth=1
	movq	-672(%rcx), %xmm2       # xmm2 = mem[0],zero
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	paddq	%xmm0, %xmm2
	movq	-448(%rcx), %xmm0       # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movq	-224(%rcx), %xmm3       # xmm3 = mem[0],zero
	punpckldq	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	paddq	%xmm0, %xmm3
	paddq	%xmm2, %xmm3
	movq	(%rcx), %xmm0           # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	paddq	%xmm3, %xmm0
	addq	$896, %rcx              # imm = 0x380
	addq	$-4, %rsi
	jne	.LBB13_74
.LBB13_75:                              # %._crit_edge432
	movdqu	%xmm0, 120(%rsp)
.LBB13_76:
	movq	(%r14), %rax
.Ltmp92:
	leaq	88(%rsp), %rsi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	*(%rax)
	movl	%eax, %r15d
.Ltmp93:
# BB#77:
	testl	%r15d, %r15d
	je	.LBB13_85
.LBB13_78:                              # %.loopexit391
	leaq	152(%rsp), %rdi
	callq	pthread_mutex_destroy
.LBB13_79:                              # %.thread379
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.LBB13_83
# BB#80:                                # %.preheader2.preheader.i353
	imulq	$224, %rax, %rbp
	.p2align	4, 0x90
.LBB13_81:                              # %.preheader2.i354
                                        # =>This Inner Loop Header: Depth=1
	leaq	-216(%rbx,%rbp), %rdi
.Ltmp115:
	callq	_ZN12CEncoderInfoD2Ev
.Ltmp116:
# BB#82:                                #   in Loop: Header=BB13_81 Depth=1
	addq	$-224, %rbp
	jne	.LBB13_81
.LBB13_83:                              # %.loopexit3.i355
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB13_84:                              # %_ZN14CBenchEncodersD2Ev.exit358
	movl	%r15d, %eax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_85:
	movl	84(%rsp), %eax          # 4-byte Reload
	addl	%eax, %eax
	cmpl	$1, 24(%rsp)            # 4-byte Folded Reload
	movl	$1, %ecx
	cmoval	%eax, %ecx
	testl	%ebp, %ebp
	movl	$0, 192(%rsp)
	movb	$0, 196(%rsp)
	je	.LBB13_108
# BB#86:                                # %.lr.ph424
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	cmpl	$1, %ecx
	movl	%r13d, 8(%rsp)          # 4-byte Spill
	jbe	.LBB13_98
# BB#87:                                # %.lr.ph424.split.us.preheader
	movl	%r13d, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	88(%rbx), %rbp
	xorl	%r12d, %r12d
.LBB13_88:                              # %.lr.ph424.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_94 Depth 2
	imulq	$224, %r12, %r14
	movl	$67108864, %eax         # imm = 0x4000000
	xorl	%edx, %edx
	movq	40(%rsp), %rcx          # 8-byte Reload
	divl	164(%rcx,%r14)
	addl	$2, %eax
	movl	%eax, 72(%rcx,%r14)
	testq	%r12, %r12
	jne	.LBB13_93
# BB#89:                                #   in Loop: Header=BB13_88 Depth=1
	movq	%rcx, %r13
	movq	48(%rbx), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 88(%rax)
	movq	48(%rbx), %r15
	movl	24(%rsp), %eax          # 4-byte Reload
	movl	%eax, 72(%r15)
	movq	$1000000, 32(%r15)      # imm = 0xF4240
	movq	$1000000, 48(%r15)      # imm = 0xF4240
	xorl	%esi, %esi
	leaq	56(%rsp), %rdi
	callq	gettimeofday
	testl	%eax, %eax
	je	.LBB13_91
# BB#90:                                #   in Loop: Header=BB13_88 Depth=1
	xorl	%edi, %edi
	callq	time
	imulq	$1000000, %rax, %rax    # imm = 0xF4240
	jmp	.LBB13_92
.LBB13_91:                              #   in Loop: Header=BB13_88 Depth=1
	imulq	$1000000, 56(%rsp), %rax # imm = 0xF4240
	addq	64(%rsp), %rax
.LBB13_92:                              # %_ZL12SetStartTimeR10CBenchInfo.exit347.us
                                        #   in Loop: Header=BB13_88 Depth=1
	movq	%rax, 24(%r15)
	callq	clock
	movq	%rax, 40(%r15)
	movq	%r13, %rcx
.LBB13_93:                              # %.preheader387.us.preheader
                                        #   in Loop: Header=BB13_88 Depth=1
	addq	%rcx, %r14
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	movq	%rbp, %r13
	xorl	%ebp, %ebp
.LBB13_94:                              # %.preheader387.us
                                        #   Parent Loop BB13_88 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, 8(%r13)
	movl	%r12d, %eax
	orl	%ebp, %eax
	movq	%r14, (%r13)
	sete	12(%r13)
	leaq	-80(%r13), %rdi
.Ltmp98:
	movl	$_ZN12CEncoderInfo20DecodeThreadFunctionEPv, %esi
	movq	%r13, %rdx
	callq	Thread_Create
	movl	%eax, %r15d
.Ltmp99:
# BB#95:                                # %_ZN12CEncoderInfo19CreateDecoderThreadEib.exit.us
                                        #   in Loop: Header=BB13_94 Depth=2
	testl	%r15d, %r15d
	jne	.LBB13_78
# BB#96:                                #   in Loop: Header=BB13_94 Depth=2
	incq	%rbp
	addq	$16, %r13
	cmpq	48(%rsp), %rbp          # 8-byte Folded Reload
	jb	.LBB13_94
# BB#97:                                # %.thread372.us
                                        #   in Loop: Header=BB13_88 Depth=1
	incq	%r12
	movq	72(%rsp), %rbp          # 8-byte Reload
	addq	$224, %rbp
	cmpq	16(%rsp), %r12          # 8-byte Folded Reload
	jb	.LBB13_88
	jmp	.LBB13_107
.LBB13_98:                              # %.lr.ph424.split.preheader
	leaq	172(%rbx), %rbp
	xorl	%r13d, %r13d
	leaq	56(%rsp), %r12
.LBB13_99:                              # %.lr.ph424.split
                                        # =>This Inner Loop Header: Depth=1
	leaq	-164(%rbp), %r14
	movl	$67108864, %eax         # imm = 0x4000000
	xorl	%edx, %edx
	divl	(%rbp)
	addl	$2, %eax
	movl	%eax, -92(%rbp)
	testq	%r13, %r13
	jne	.LBB13_104
# BB#100:                               #   in Loop: Header=BB13_99 Depth=1
	movq	48(%rbx), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 88(%rax)
	movq	48(%rbx), %r15
	movl	24(%rsp), %eax          # 4-byte Reload
	movl	%eax, 72(%r15)
	movq	$1000000, 32(%r15)      # imm = 0xF4240
	movq	$1000000, 48(%r15)      # imm = 0xF4240
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	gettimeofday
	testl	%eax, %eax
	je	.LBB13_102
# BB#101:                               #   in Loop: Header=BB13_99 Depth=1
	xorl	%edi, %edi
	callq	time
	imulq	$1000000, %rax, %rax    # imm = 0xF4240
	jmp	.LBB13_103
.LBB13_102:                             #   in Loop: Header=BB13_99 Depth=1
	imulq	$1000000, 56(%rsp), %rax # imm = 0xF4240
	addq	64(%rsp), %rax
.LBB13_103:                             # %_ZL12SetStartTimeR10CBenchInfo.exit347
                                        #   in Loop: Header=BB13_99 Depth=1
	movq	%rax, 24(%r15)
	callq	clock
	movq	%rax, 40(%r15)
.LBB13_104:                             #   in Loop: Header=BB13_99 Depth=1
.Ltmp95:
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	_ZN12CEncoderInfo6DecodeEj
	movl	%eax, %r15d
.Ltmp96:
# BB#105:                               #   in Loop: Header=BB13_99 Depth=1
	testl	%r15d, %r15d
	jne	.LBB13_78
# BB#106:                               # %.thread372
                                        #   in Loop: Header=BB13_99 Depth=1
	incq	%r13
	addq	$224, %rbp
	cmpq	16(%rsp), %r13          # 8-byte Folded Reload
	jb	.LBB13_99
.LBB13_107:                             # %._crit_edge425
	cmpl	$2, 24(%rsp)            # 4-byte Folded Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	8(%rsp), %r13d          # 4-byte Reload
	jae	.LBB13_116
.LBB13_108:                             # %.thread375
	movl	192(%rsp), %r15d
	testl	%r15d, %r15d
	jne	.LBB13_78
# BB#109:
	leaq	120(%rsp), %r14
	movq	48(%rbx), %rbp
	movq	$1000000, 96(%rsp)      # imm = 0xF4240
	movq	$1000000, 112(%rsp)     # imm = 0xF4240
	leaq	56(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	testl	%eax, %eax
	je	.LBB13_111
# BB#110:
	xorl	%edi, %edi
	callq	time
	imulq	$1000000, %rax, %rax    # imm = 0xF4240
	jmp	.LBB13_112
.LBB13_111:
	imulq	$1000000, 56(%rsp), %rax # imm = 0xF4240
	addq	64(%rsp), %rax
.LBB13_112:
	subq	24(%rbp), %rax
	movq	%rax, 88(%rsp)
	callq	clock
	subq	40(%rbp), %rax
	movq	%rax, 104(%rsp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%r14)
	imull	80(%rbx), %r13d
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	movl	%r13d, 136(%rsp)
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB13_127
# BB#113:                               # %.lr.ph.preheader
	leaq	-1(%r12), %rax
	movq	%r12, %rdx
	andq	$3, %rdx
	je	.LBB13_122
# BB#114:                               # %.lr.ph.prol.preheader
	leaq	172(%rbx), %rsi
	pxor	%xmm1, %xmm1
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
.LBB13_115:                             # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %xmm2           # xmm2 = mem[0],zero
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	paddq	%xmm2, %xmm0
	incq	%rcx
	addq	$224, %rsi
	cmpq	%rcx, %rdx
	jne	.LBB13_115
	jmp	.LBB13_123
.LBB13_116:                             # %.preheader.preheader
	movl	%r13d, %ebp
	leaq	136(%rbx), %r14
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	movq	40(%rsp), %r12          # 8-byte Reload
.LBB13_117:                             # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_118 Depth 2
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movq	%r14, 24(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
.LBB13_118:                             #   Parent Loop BB13_117 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
.Ltmp101:
	movq	%r12, %rdi
	callq	Thread_Wait
.Ltmp102:
# BB#119:                               # %_ZN8NWindows7CThread4WaitEv.exit350
                                        #   in Loop: Header=BB13_118 Depth=2
	movl	(%r14), %eax
	testl	%eax, %eax
	cmovnel	%eax, %r15d
	incq	%r13
	addq	$4, %r14
	addq	$16, %r12
	cmpq	%rbp, %r13
	jb	.LBB13_118
# BB#120:                               #   in Loop: Header=BB13_117 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rsi
	incq	%rsi
	movq	24(%rsp), %r14          # 8-byte Reload
	addq	$224, %r14
	movq	40(%rsp), %r12          # 8-byte Reload
	addq	$224, %r12
	movq	%rsi, %rdx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	cmpq	16(%rsp), %rsi          # 8-byte Folded Reload
	jb	.LBB13_117
# BB#121:                               # %._crit_edge416
	testl	%r15d, %r15d
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	8(%rsp), %r13d          # 4-byte Reload
	jne	.LBB13_78
	jmp	.LBB13_108
.LBB13_122:
	xorl	%ecx, %ecx
.LBB13_123:                             # %.lr.ph.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB13_126
# BB#124:                               # %.lr.ph.preheader.new
	subq	%rcx, %r12
	imulq	$224, %rcx, %rax
	leaq	844(%rbx,%rax), %rcx
	pxor	%xmm1, %xmm1
.LBB13_125:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-672(%rcx), %xmm2       # xmm2 = mem[0],zero
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	paddq	%xmm0, %xmm2
	movq	-448(%rcx), %xmm0       # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movq	-224(%rcx), %xmm3       # xmm3 = mem[0],zero
	punpckldq	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	paddq	%xmm0, %xmm3
	paddq	%xmm2, %xmm3
	movq	(%rcx), %xmm0           # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	paddq	%xmm3, %xmm0
	addq	$896, %rcx              # imm = 0x380
	addq	$-4, %r12
	jne	.LBB13_125
.LBB13_126:                             # %._crit_edge
	movdqu	%xmm0, 120(%rsp)
.LBB13_127:
	movq	(%rbp), %rax
.Ltmp104:
	leaq	88(%rsp), %rsi
	xorl	%edx, %edx
	movq	%rbp, %rdi
	callq	*8(%rax)
	movl	%eax, %r15d
.Ltmp105:
# BB#128:
	testl	%r15d, %r15d
	jne	.LBB13_78
# BB#129:
	movq	(%rbp), %rax
.Ltmp106:
	leaq	88(%rsp), %rsi
	movl	$1, %edx
	movq	%rbp, %rdi
	callq	*8(%rax)
	movl	%eax, %r15d
.Ltmp107:
	jmp	.LBB13_78
.LBB13_130:
.Ltmp108:
	jmp	.LBB13_143
.LBB13_131:
.Ltmp94:
	jmp	.LBB13_143
.LBB13_132:                             # %.us-lcssa443
.Ltmp91:
	jmp	.LBB13_143
.LBB13_133:                             # %.loopexit.split-lp
.Ltmp64:
	jmp	.LBB13_145
.LBB13_134:                             # %.us-lcssa445
.Ltmp88:
	jmp	.LBB13_143
.LBB13_135:
.Ltmp103:
	jmp	.LBB13_143
.LBB13_136:                             # %.loopexit392
.Ltmp61:
	jmp	.LBB13_145
.LBB13_137:                             # %.us-lcssa
.Ltmp97:
	jmp	.LBB13_143
.LBB13_138:                             # %.us-lcssa427.us
.Ltmp100:
	jmp	.LBB13_143
.LBB13_139:
.Ltmp79:
	jmp	.LBB13_143
.LBB13_140:                             # %.us-lcssa443.us
.Ltmp76:
	jmp	.LBB13_143
.LBB13_141:
.Ltmp55:
	jmp	.LBB13_145
.LBB13_142:                             # %.us-lcssa445.us
.Ltmp73:
.LBB13_143:
	movq	%rax, %r14
	leaq	152(%rsp), %rdi
	callq	pthread_mutex_destroy
	jmp	.LBB13_146
.LBB13_144:
.Ltmp58:
.LBB13_145:
	movq	%rax, %r14
.LBB13_146:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.LBB13_153
# BB#147:                               # %.preheader2.preheader.i
	imulq	$224, %rax, %rbp
	.p2align	4, 0x90
.LBB13_148:                             # %.preheader2.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	-216(%rbx,%rbp), %rdi
.Ltmp109:
	callq	_ZN12CEncoderInfoD2Ev
.Ltmp110:
# BB#149:                               #   in Loop: Header=BB13_148 Depth=1
	addq	$-224, %rbp
	jne	.LBB13_148
	jmp	.LBB13_153
.LBB13_150:
.Ltmp117:
	movq	%rax, %r14
	cmpq	$224, %rbp
	je	.LBB13_153
	.p2align	4, 0x90
.LBB13_152:                             # %.preheader.i356
                                        # =>This Inner Loop Header: Depth=1
	leaq	-440(%rbx,%rbp), %rdi
.Ltmp118:
	callq	_ZN12CEncoderInfoD2Ev
.Ltmp119:
# BB#151:                               #   in Loop: Header=BB13_152 Depth=1
	addq	$-224, %rbp
	cmpq	$224, %rbp
	jne	.LBB13_152
.LBB13_153:                             # %.loopexit.i357
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB13_154:
.Ltmp120:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_155:
.Ltmp111:
	movq	%rax, %r14
	cmpq	$224, %rbp
	je	.LBB13_158
	.p2align	4, 0x90
.LBB13_157:                             # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	-440(%rbx,%rbp), %rdi
.Ltmp112:
	callq	_ZN12CEncoderInfoD2Ev
.Ltmp113:
# BB#156:                               #   in Loop: Header=BB13_157 Depth=1
	addq	$-224, %rbp
	cmpq	$224, %rbp
	jne	.LBB13_157
.LBB13_158:                             # %.body
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB13_159:
.Ltmp114:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end13:
	.size	_Z9LzmaBenchjjP14IBenchCallback, .Lfunc_end13-_Z9LzmaBenchjjP14IBenchCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\232\202\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\221\002"              # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp53-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp53
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp54-.Ltmp53         #   Call between .Ltmp53 and .Ltmp54
	.long	.Ltmp55-.Lfunc_begin3   #     jumps to .Ltmp55
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin3   #     jumps to .Ltmp58
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp60-.Ltmp59         #   Call between .Ltmp59 and .Ltmp60
	.long	.Ltmp61-.Lfunc_begin3   #     jumps to .Ltmp61
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp63-.Ltmp62         #   Call between .Ltmp62 and .Ltmp63
	.long	.Ltmp64-.Lfunc_begin3   #     jumps to .Ltmp64
	.byte	0                       #   On action: cleanup
	.long	.Ltmp80-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp83-.Ltmp80         #   Call between .Ltmp80 and .Ltmp83
	.long	.Ltmp88-.Lfunc_begin3   #     jumps to .Ltmp88
	.byte	0                       #   On action: cleanup
	.long	.Ltmp65-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp72-.Ltmp65         #   Call between .Ltmp65 and .Ltmp72
	.long	.Ltmp73-.Lfunc_begin3   #     jumps to .Ltmp73
	.byte	0                       #   On action: cleanup
	.long	.Ltmp74-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Ltmp75-.Ltmp74         #   Call between .Ltmp74 and .Ltmp75
	.long	.Ltmp76-.Lfunc_begin3   #     jumps to .Ltmp76
	.byte	0                       #   On action: cleanup
	.long	.Ltmp77-.Lfunc_begin3   # >> Call Site 9 <<
	.long	.Ltmp78-.Ltmp77         #   Call between .Ltmp77 and .Ltmp78
	.long	.Ltmp79-.Lfunc_begin3   #     jumps to .Ltmp79
	.byte	0                       #   On action: cleanup
	.long	.Ltmp84-.Lfunc_begin3   # >> Call Site 10 <<
	.long	.Ltmp87-.Ltmp84         #   Call between .Ltmp84 and .Ltmp87
	.long	.Ltmp88-.Lfunc_begin3   #     jumps to .Ltmp88
	.byte	0                       #   On action: cleanup
	.long	.Ltmp89-.Lfunc_begin3   # >> Call Site 11 <<
	.long	.Ltmp90-.Ltmp89         #   Call between .Ltmp89 and .Ltmp90
	.long	.Ltmp91-.Lfunc_begin3   #     jumps to .Ltmp91
	.byte	0                       #   On action: cleanup
	.long	.Ltmp92-.Lfunc_begin3   # >> Call Site 12 <<
	.long	.Ltmp93-.Ltmp92         #   Call between .Ltmp92 and .Ltmp93
	.long	.Ltmp94-.Lfunc_begin3   #     jumps to .Ltmp94
	.byte	0                       #   On action: cleanup
	.long	.Ltmp115-.Lfunc_begin3  # >> Call Site 13 <<
	.long	.Ltmp116-.Ltmp115       #   Call between .Ltmp115 and .Ltmp116
	.long	.Ltmp117-.Lfunc_begin3  #     jumps to .Ltmp117
	.byte	0                       #   On action: cleanup
	.long	.Ltmp98-.Lfunc_begin3   # >> Call Site 14 <<
	.long	.Ltmp99-.Ltmp98         #   Call between .Ltmp98 and .Ltmp99
	.long	.Ltmp100-.Lfunc_begin3  #     jumps to .Ltmp100
	.byte	0                       #   On action: cleanup
	.long	.Ltmp95-.Lfunc_begin3   # >> Call Site 15 <<
	.long	.Ltmp96-.Ltmp95         #   Call between .Ltmp95 and .Ltmp96
	.long	.Ltmp97-.Lfunc_begin3   #     jumps to .Ltmp97
	.byte	0                       #   On action: cleanup
	.long	.Ltmp101-.Lfunc_begin3  # >> Call Site 16 <<
	.long	.Ltmp102-.Ltmp101       #   Call between .Ltmp101 and .Ltmp102
	.long	.Ltmp103-.Lfunc_begin3  #     jumps to .Ltmp103
	.byte	0                       #   On action: cleanup
	.long	.Ltmp104-.Lfunc_begin3  # >> Call Site 17 <<
	.long	.Ltmp107-.Ltmp104       #   Call between .Ltmp104 and .Ltmp107
	.long	.Ltmp108-.Lfunc_begin3  #     jumps to .Ltmp108
	.byte	0                       #   On action: cleanup
	.long	.Ltmp109-.Lfunc_begin3  # >> Call Site 18 <<
	.long	.Ltmp110-.Ltmp109       #   Call between .Ltmp109 and .Ltmp110
	.long	.Ltmp111-.Lfunc_begin3  #     jumps to .Ltmp111
	.byte	1                       #   On action: 1
	.long	.Ltmp118-.Lfunc_begin3  # >> Call Site 19 <<
	.long	.Ltmp119-.Ltmp118       #   Call between .Ltmp118 and .Ltmp119
	.long	.Ltmp120-.Lfunc_begin3  #     jumps to .Ltmp120
	.byte	1                       #   On action: 1
	.long	.Ltmp119-.Lfunc_begin3  # >> Call Site 20 <<
	.long	.Ltmp112-.Ltmp119       #   Call between .Ltmp119 and .Ltmp112
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp112-.Lfunc_begin3  # >> Call Site 21 <<
	.long	.Ltmp113-.Ltmp112       #   Call between .Ltmp112 and .Ltmp113
	.long	.Ltmp114-.Lfunc_begin3  #     jumps to .Ltmp114
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_Z19GetBenchMemoryUsagejj
	.p2align	4, 0x90
	.type	_Z19GetBenchMemoryUsagejj,@function
_Z19GetBenchMemoryUsagejj:              # @_Z19GetBenchMemoryUsagejj
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movl	%esi, %eax
	shrl	%eax
	cmpl	$1, %edi
	seta	%cl
	movl	$9437184, %r8d          # imm = 0x900000
	movl	$3145728, %r9d          # imm = 0x300000
	cmovaq	%r8, %r9
	shrl	%cl, %edi
	addl	%esi, %eax
	leal	-1(%rsi), %edx
	movl	%edx, %ecx
	shrl	%ecx
	orl	%edx, %ecx
	movl	%ecx, %edx
	shrl	$2, %edx
	orl	%ecx, %edx
	movl	%edx, %ecx
	shrl	$4, %ecx
	orl	%edx, %ecx
	movl	%ecx, %edx
	shrl	$8, %edx
	orl	%ecx, %edx
	shrl	%edx
	orl	$65535, %edx            # imm = 0xFFFF
	cmpl	$16777216, %edx         # imm = 0x1000000
	seta	%cl
	shrl	%cl, %edx
	movl	%esi, %ecx
	leaq	(%rdx,%rcx,2), %rdx
	leaq	(%rcx,%rcx,2), %rcx
	shrq	%rcx
	addq	%r9, %rcx
	addq	%rax, %rcx
	leaq	262148(%rcx,%rdx,4), %rax
	imulq	%rdi, %rax
	retq
.Lfunc_end14:
	.size	_Z19GetBenchMemoryUsagejj, .Lfunc_end14-_Z19GetBenchMemoryUsagejj
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI15_0:
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	9                       # 0x9
	.byte	10                      # 0xa
	.byte	11                      # 0xb
	.byte	12                      # 0xc
	.byte	13                      # 0xd
	.byte	14                      # 0xe
	.byte	15                      # 0xf
.LCPI15_1:
	.byte	16                      # 0x10
	.byte	17                      # 0x11
	.byte	18                      # 0x12
	.byte	19                      # 0x13
	.byte	20                      # 0x14
	.byte	21                      # 0x15
	.byte	22                      # 0x16
	.byte	23                      # 0x17
	.byte	24                      # 0x18
	.byte	25                      # 0x19
	.byte	26                      # 0x1a
	.byte	27                      # 0x1b
	.byte	28                      # 0x1c
	.byte	29                      # 0x1d
	.byte	30                      # 0x1e
	.byte	31                      # 0x1f
.LCPI15_2:
	.byte	32                      # 0x20
	.byte	33                      # 0x21
	.byte	34                      # 0x22
	.byte	35                      # 0x23
	.byte	36                      # 0x24
	.byte	37                      # 0x25
	.byte	38                      # 0x26
	.byte	39                      # 0x27
	.byte	40                      # 0x28
	.byte	41                      # 0x29
	.byte	42                      # 0x2a
	.byte	43                      # 0x2b
	.byte	44                      # 0x2c
	.byte	45                      # 0x2d
	.byte	46                      # 0x2e
	.byte	47                      # 0x2f
.LCPI15_3:
	.byte	48                      # 0x30
	.byte	49                      # 0x31
	.byte	50                      # 0x32
	.byte	51                      # 0x33
	.byte	52                      # 0x34
	.byte	53                      # 0x35
	.byte	54                      # 0x36
	.byte	55                      # 0x37
	.byte	56                      # 0x38
	.byte	57                      # 0x39
	.byte	58                      # 0x3a
	.byte	59                      # 0x3b
	.byte	60                      # 0x3c
	.byte	61                      # 0x3d
	.byte	62                      # 0x3e
	.byte	63                      # 0x3f
.LCPI15_4:
	.byte	64                      # 0x40
	.byte	65                      # 0x41
	.byte	66                      # 0x42
	.byte	67                      # 0x43
	.byte	68                      # 0x44
	.byte	69                      # 0x45
	.byte	70                      # 0x46
	.byte	71                      # 0x47
	.byte	72                      # 0x48
	.byte	73                      # 0x49
	.byte	74                      # 0x4a
	.byte	75                      # 0x4b
	.byte	76                      # 0x4c
	.byte	77                      # 0x4d
	.byte	78                      # 0x4e
	.byte	79                      # 0x4f
.LCPI15_5:
	.byte	80                      # 0x50
	.byte	81                      # 0x51
	.byte	82                      # 0x52
	.byte	83                      # 0x53
	.byte	84                      # 0x54
	.byte	85                      # 0x55
	.byte	86                      # 0x56
	.byte	87                      # 0x57
	.byte	88                      # 0x58
	.byte	89                      # 0x59
	.byte	90                      # 0x5a
	.byte	91                      # 0x5b
	.byte	92                      # 0x5c
	.byte	93                      # 0x5d
	.byte	94                      # 0x5e
	.byte	95                      # 0x5f
.LCPI15_6:
	.byte	96                      # 0x60
	.byte	97                      # 0x61
	.byte	98                      # 0x62
	.byte	99                      # 0x63
	.byte	100                     # 0x64
	.byte	101                     # 0x65
	.byte	102                     # 0x66
	.byte	103                     # 0x67
	.byte	104                     # 0x68
	.byte	105                     # 0x69
	.byte	106                     # 0x6a
	.byte	107                     # 0x6b
	.byte	108                     # 0x6c
	.byte	109                     # 0x6d
	.byte	110                     # 0x6e
	.byte	111                     # 0x6f
.LCPI15_7:
	.byte	112                     # 0x70
	.byte	113                     # 0x71
	.byte	114                     # 0x72
	.byte	115                     # 0x73
	.byte	116                     # 0x74
	.byte	117                     # 0x75
	.byte	118                     # 0x76
	.byte	119                     # 0x77
	.byte	120                     # 0x78
	.byte	121                     # 0x79
	.byte	122                     # 0x7a
	.byte	123                     # 0x7b
	.byte	124                     # 0x7c
	.byte	125                     # 0x7d
	.byte	126                     # 0x7e
	.byte	127                     # 0x7f
.LCPI15_8:
	.byte	128                     # 0x80
	.byte	129                     # 0x81
	.byte	130                     # 0x82
	.byte	131                     # 0x83
	.byte	132                     # 0x84
	.byte	133                     # 0x85
	.byte	134                     # 0x86
	.byte	135                     # 0x87
	.byte	136                     # 0x88
	.byte	137                     # 0x89
	.byte	138                     # 0x8a
	.byte	139                     # 0x8b
	.byte	140                     # 0x8c
	.byte	141                     # 0x8d
	.byte	142                     # 0x8e
	.byte	143                     # 0x8f
.LCPI15_9:
	.byte	144                     # 0x90
	.byte	145                     # 0x91
	.byte	146                     # 0x92
	.byte	147                     # 0x93
	.byte	148                     # 0x94
	.byte	149                     # 0x95
	.byte	150                     # 0x96
	.byte	151                     # 0x97
	.byte	152                     # 0x98
	.byte	153                     # 0x99
	.byte	154                     # 0x9a
	.byte	155                     # 0x9b
	.byte	156                     # 0x9c
	.byte	157                     # 0x9d
	.byte	158                     # 0x9e
	.byte	159                     # 0x9f
.LCPI15_10:
	.byte	160                     # 0xa0
	.byte	161                     # 0xa1
	.byte	162                     # 0xa2
	.byte	163                     # 0xa3
	.byte	164                     # 0xa4
	.byte	165                     # 0xa5
	.byte	166                     # 0xa6
	.byte	167                     # 0xa7
	.byte	168                     # 0xa8
	.byte	169                     # 0xa9
	.byte	170                     # 0xaa
	.byte	171                     # 0xab
	.byte	172                     # 0xac
	.byte	173                     # 0xad
	.byte	174                     # 0xae
	.byte	175                     # 0xaf
.LCPI15_11:
	.byte	176                     # 0xb0
	.byte	177                     # 0xb1
	.byte	178                     # 0xb2
	.byte	179                     # 0xb3
	.byte	180                     # 0xb4
	.byte	181                     # 0xb5
	.byte	182                     # 0xb6
	.byte	183                     # 0xb7
	.byte	184                     # 0xb8
	.byte	185                     # 0xb9
	.byte	186                     # 0xba
	.byte	187                     # 0xbb
	.byte	188                     # 0xbc
	.byte	189                     # 0xbd
	.byte	190                     # 0xbe
	.byte	191                     # 0xbf
.LCPI15_12:
	.byte	192                     # 0xc0
	.byte	193                     # 0xc1
	.byte	194                     # 0xc2
	.byte	195                     # 0xc3
	.byte	196                     # 0xc4
	.byte	197                     # 0xc5
	.byte	198                     # 0xc6
	.byte	199                     # 0xc7
	.byte	200                     # 0xc8
	.byte	201                     # 0xc9
	.byte	202                     # 0xca
	.byte	203                     # 0xcb
	.byte	204                     # 0xcc
	.byte	205                     # 0xcd
	.byte	206                     # 0xce
	.byte	207                     # 0xcf
.LCPI15_13:
	.byte	208                     # 0xd0
	.byte	209                     # 0xd1
	.byte	210                     # 0xd2
	.byte	211                     # 0xd3
	.byte	212                     # 0xd4
	.byte	213                     # 0xd5
	.byte	214                     # 0xd6
	.byte	215                     # 0xd7
	.byte	216                     # 0xd8
	.byte	217                     # 0xd9
	.byte	218                     # 0xda
	.byte	219                     # 0xdb
	.byte	220                     # 0xdc
	.byte	221                     # 0xdd
	.byte	222                     # 0xde
	.byte	223                     # 0xdf
.LCPI15_14:
	.byte	224                     # 0xe0
	.byte	225                     # 0xe1
	.byte	226                     # 0xe2
	.byte	227                     # 0xe3
	.byte	228                     # 0xe4
	.byte	229                     # 0xe5
	.byte	230                     # 0xe6
	.byte	231                     # 0xe7
	.byte	232                     # 0xe8
	.byte	233                     # 0xe9
	.byte	234                     # 0xea
	.byte	235                     # 0xeb
	.byte	236                     # 0xec
	.byte	237                     # 0xed
	.byte	238                     # 0xee
	.byte	239                     # 0xef
.LCPI15_15:
	.byte	240                     # 0xf0
	.byte	241                     # 0xf1
	.byte	242                     # 0xf2
	.byte	243                     # 0xf3
	.byte	244                     # 0xf4
	.byte	245                     # 0xf5
	.byte	246                     # 0xf6
	.byte	247                     # 0xf7
	.byte	248                     # 0xf8
	.byte	249                     # 0xf9
	.byte	250                     # 0xfa
	.byte	251                     # 0xfb
	.byte	252                     # 0xfc
	.byte	253                     # 0xfd
	.byte	254                     # 0xfe
	.byte	255                     # 0xff
	.text
	.globl	_Z15CrcInternalTestv
	.p2align	4, 0x90
	.type	_Z15CrcInternalTestv,@function
_Z15CrcInternalTestv:                   # @_Z15CrcInternalTestv
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:                                 # %._crit_edge.i
	pushq	%rbp
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi91:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi92:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi94:
	.cfi_def_cfa_offset 64
.Lcfi95:
	.cfi_offset %rbx, -56
.Lcfi96:
	.cfi_offset %r12, -48
.Lcfi97:
	.cfi_offset %r13, -40
.Lcfi98:
	.cfi_offset %r14, -32
.Lcfi99:
	.cfi_offset %r15, -24
.Lcfi100:
	.cfi_offset %rbp, -16
.Ltmp121:
	xorl	%edi, %edi
	callq	MidFree
.Ltmp122:
# BB#1:                                 # %.noexc
.Ltmp123:
	movl	$1280, %edi             # imm = 0x500
	callq	MidAlloc
	movq	%rax, %r15
.Ltmp124:
# BB#2:
	testq	%r15, %r15
	je	.LBB15_23
# BB#3:                                 # %vector.body
	movaps	.LCPI15_0(%rip), %xmm0  # xmm0 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
	movups	%xmm0, (%r15)
	movaps	.LCPI15_1(%rip), %xmm0  # xmm0 = [16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
	movups	%xmm0, 16(%r15)
	movaps	.LCPI15_2(%rip), %xmm0  # xmm0 = [32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47]
	movups	%xmm0, 32(%r15)
	movaps	.LCPI15_3(%rip), %xmm0  # xmm0 = [48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63]
	movups	%xmm0, 48(%r15)
	movaps	.LCPI15_4(%rip), %xmm0  # xmm0 = [64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79]
	movups	%xmm0, 64(%r15)
	movaps	.LCPI15_5(%rip), %xmm0  # xmm0 = [80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95]
	movups	%xmm0, 80(%r15)
	movaps	.LCPI15_6(%rip), %xmm0  # xmm0 = [96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111]
	movups	%xmm0, 96(%r15)
	movaps	.LCPI15_7(%rip), %xmm0  # xmm0 = [112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127]
	movups	%xmm0, 112(%r15)
	movaps	.LCPI15_8(%rip), %xmm0  # xmm0 = [128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143]
	movups	%xmm0, 128(%r15)
	movaps	.LCPI15_9(%rip), %xmm0  # xmm0 = [144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159]
	movups	%xmm0, 144(%r15)
	movaps	.LCPI15_10(%rip), %xmm0 # xmm0 = [160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175]
	movups	%xmm0, 160(%r15)
	movaps	.LCPI15_11(%rip), %xmm0 # xmm0 = [176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191]
	movups	%xmm0, 176(%r15)
	movaps	.LCPI15_12(%rip), %xmm0 # xmm0 = [192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207]
	movups	%xmm0, 192(%r15)
	movaps	.LCPI15_13(%rip), %xmm0 # xmm0 = [208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223]
	movups	%xmm0, 208(%r15)
	movaps	.LCPI15_14(%rip), %xmm0 # xmm0 = [224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239]
	movups	%xmm0, 224(%r15)
	movaps	.LCPI15_15(%rip), %xmm0 # xmm0 = [240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255]
	movups	%xmm0, 240(%r15)
	movl	$-1, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB15_4:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r15,%rcx), %edx
	movzbl	%al, %esi
	xorl	%edx, %esi
	shrl	$8, %eax
	xorl	g_CrcTable(,%rsi,4), %eax
	movzbl	1(%r15,%rcx), %edx
	movzbl	%al, %esi
	xorl	%edx, %esi
	shrl	$8, %eax
	xorl	g_CrcTable(,%rsi,4), %eax
	addq	$2, %rcx
	cmpq	$256, %rcx              # imm = 0x100
	jne	.LBB15_4
# BB#5:                                 # %_ZL8CrcCalc1PKhj.exit
	cmpl	$-688229492, %eax       # imm = 0xD6FA738C
	jne	.LBB15_23
# BB#6:
	movl	$521288629, %eax        # imm = 0x1F123BB5
	movl	$259, %ecx              # imm = 0x103
	.p2align	4, 0x90
.LBB15_7:                               # =>This Inner Loop Header: Depth=1
	movzwl	%ax, %edx
	imull	$18000, %edx, %edx      # imm = 0x4650
	shrl	$16, %eax
	addl	%edx, %eax
	movb	%al, -3(%r15,%rcx)
	movzwl	%ax, %edx
	imull	$18000, %edx, %edx      # imm = 0x4650
	shrl	$16, %eax
	addl	%edx, %eax
	movb	%al, -2(%r15,%rcx)
	movzwl	%ax, %edx
	imull	$18000, %edx, %edx      # imm = 0x4650
	shrl	$16, %eax
	addl	%edx, %eax
	movb	%al, -1(%r15,%rcx)
	movzwl	%ax, %edx
	imull	$18000, %edx, %edx      # imm = 0x4650
	shrl	$16, %eax
	addl	%edx, %eax
	movb	%al, (%r15,%rcx)
	addq	$4, %rcx
	cmpq	$1283, %rcx             # imm = 0x503
	jne	.LBB15_7
# BB#8:                                 # %.preheader.preheader
	leaq	1(%r15), %rax
	xorl	%r13d, %r13d
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB15_9:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_10 Depth 2
                                        #       Child Loop BB15_15 Depth 3
	leaq	(%r15,%r13), %r14
	leaq	(%rax,%r13), %r12
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB15_10:                              #   Parent Loop BB15_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB15_15 Depth 3
	testq	%rbx, %rbx
	je	.LBB15_13
# BB#11:                                # %.lr.ph.i64.preheader
                                        #   in Loop: Header=BB15_10 Depth=2
	testb	$1, %bl
	jne	.LBB15_14
# BB#12:                                #   in Loop: Header=BB15_10 Depth=2
	movl	$-1, %ebp
	xorl	%eax, %eax
	cmpq	$1, %rbx
	jne	.LBB15_15
	jmp	.LBB15_16
	.p2align	4, 0x90
.LBB15_13:                              #   in Loop: Header=BB15_10 Depth=2
	xorl	%ebp, %ebp
	jmp	.LBB15_17
	.p2align	4, 0x90
.LBB15_14:                              # %.lr.ph.i64.prol
                                        #   in Loop: Header=BB15_10 Depth=2
	movzbl	(%r14), %eax
	xorq	$255, %rax
	movl	g_CrcTable(,%rax,4), %ebp
	movl	$16777215, %eax         # imm = 0xFFFFFF
	xorl	%eax, %ebp
	movl	$1, %eax
	cmpq	$1, %rbx
	je	.LBB15_16
	.p2align	4, 0x90
.LBB15_15:                              # %.lr.ph.i64
                                        #   Parent Loop BB15_9 Depth=1
                                        #     Parent Loop BB15_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	-1(%r12,%rax), %ecx
	movzbl	%bpl, %edx
	xorl	%ecx, %edx
	shrl	$8, %ebp
	xorl	g_CrcTable(,%rdx,4), %ebp
	movzbl	(%r12,%rax), %ecx
	movzbl	%bpl, %edx
	xorl	%ecx, %edx
	shrl	$8, %ebp
	xorl	g_CrcTable(,%rdx,4), %ebp
	addq	$2, %rax
	cmpq	%rax, %rbx
	jne	.LBB15_15
.LBB15_16:                              # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB15_10 Depth=2
	notl	%ebp
.LBB15_17:                              # %_ZL8CrcCalc1PKhj.exit65
                                        #   in Loop: Header=BB15_10 Depth=2
.Ltmp126:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	CrcCalc
.Ltmp127:
# BB#18:                                #   in Loop: Header=BB15_10 Depth=2
	cmpl	%eax, %ebp
	jne	.LBB15_23
# BB#19:                                #   in Loop: Header=BB15_10 Depth=2
	incq	%rbx
	cmpq	$31, %rbx
	jbe	.LBB15_10
# BB#20:                                # %.critedge
                                        #   in Loop: Header=BB15_9 Depth=1
	incq	%r13
	movb	$1, %bl
	cmpq	$1248, %r13             # imm = 0x4E0
	movq	(%rsp), %rax            # 8-byte Reload
	jb	.LBB15_9
	jmp	.LBB15_24
.LBB15_23:
	xorl	%ebx, %ebx
.LBB15_24:                              # %.loopexit
	movq	%r15, %rdi
	callq	MidFree
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_25:
.Ltmp125:
	movq	%rax, %rbx
	xorl	%r15d, %r15d
	jmp	.LBB15_27
.LBB15_26:
.Ltmp128:
	movq	%rax, %rbx
.LBB15_27:
.Ltmp129:
	movq	%r15, %rdi
	callq	MidFree
.Ltmp130:
# BB#28:                                # %_ZN12CBenchBufferD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB15_29:
.Ltmp131:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end15:
	.size	_Z15CrcInternalTestv, .Lfunc_end15-_Z15CrcInternalTestv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp121-.Lfunc_begin4  # >> Call Site 1 <<
	.long	.Ltmp124-.Ltmp121       #   Call between .Ltmp121 and .Ltmp124
	.long	.Ltmp125-.Lfunc_begin4  #     jumps to .Ltmp125
	.byte	0                       #   On action: cleanup
	.long	.Ltmp126-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp127-.Ltmp126       #   Call between .Ltmp126 and .Ltmp127
	.long	.Ltmp128-.Lfunc_begin4  #     jumps to .Ltmp128
	.byte	0                       #   On action: cleanup
	.long	.Ltmp127-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp129-.Ltmp127       #   Call between .Ltmp127 and .Ltmp129
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp129-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Ltmp130-.Ltmp129       #   Call between .Ltmp129 and .Ltmp130
	.long	.Ltmp131-.Lfunc_begin4  #     jumps to .Ltmp131
	.byte	1                       #   On action: 1
	.long	.Ltmp130-.Lfunc_begin4  # >> Call Site 5 <<
	.long	.Lfunc_end15-.Ltmp130   #   Call between .Ltmp130 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_Z8CrcBenchjjRy
	.p2align	4, 0x90
	.type	_Z8CrcBenchjjRy,@function
_Z8CrcBenchjjRy:                        # @_Z8CrcBenchjjRy
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:                                 # %._crit_edge.i
	pushq	%rbp
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi104:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi105:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi107:
	.cfi_def_cfa_offset 128
.Lcfi108:
	.cfi_offset %rbx, -56
.Lcfi109:
	.cfi_offset %r12, -48
.Lcfi110:
	.cfi_offset %r13, -40
.Lcfi111:
	.cfi_offset %r14, -32
.Lcfi112:
	.cfi_offset %r15, -24
.Lcfi113:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movl	%esi, %r13d
	testl	%edi, %edi
	movl	$1, %ebx
	cmovnel	%edi, %ebx
.Ltmp132:
	xorl	%edi, %edi
	callq	MidFree
.Ltmp133:
# BB#1:                                 # %.noexc
	movl	%r13d, %r15d
	movq	%rbx, %rbp
	imulq	%r15, %rbp
.Ltmp134:
	movq	%rbp, %rdi
	callq	MidAlloc
	movq	%rax, %r14
.Ltmp135:
# BB#2:
	movq	%rbx, (%rsp)            # 8-byte Spill
	testq	%r14, %r14
	je	.LBB16_21
# BB#3:
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movq	%r12, 64(%rsp)          # 8-byte Spill
	movl	%r13d, %ecx
	shrl	$2, %ecx
	incl	%ecx
	xorl	%ebp, %ebp
	movl	$1073741824, %eax       # imm = 0x40000000
	xorl	%edx, %edx
	divl	%ecx
	movl	%eax, %r12d
	incl	%r12d
	movl	$0, 8(%rsp)
	movq	$0, 16(%rsp)
	movq	(%rsp), %rbx            # 8-byte Reload
	cmpl	$2, %ebx
	jb	.LBB16_22
# BB#4:
	leaq	(%rbx,%rbx,4), %rax
	leaq	8(,%rax,8), %rdi
.Ltmp140:
	callq	_Znam
.Ltmp141:
# BB#5:
	movq	%rbx, (%rax)
	leaq	8(%rax), %r11
	leaq	16(%rax), %rcx
	leaq	(,%rbx,8), %rdx
	leaq	(%rdx,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB16_6:                               # =>This Inner Loop Header: Depth=1
	movl	$0, (%rcx)
	addq	$40, %rcx
	addq	$-40, %rdx
	jne	.LBB16_6
# BB#7:                                 # %.lr.ph176
	movq	%r11, 16(%rsp)
	testl	%r13d, %r13d
	movl	%r12d, 28(%rsp)         # 4-byte Spill
	movq	%r14, 48(%rsp)          # 8-byte Spill
	je	.LBB16_26
# BB#8:                                 # %.lr.ph176.split.preheader
	leaq	-1(%r15), %r9
	movl	%r15d, %edi
	andl	$3, %edi
	movl	%r15d, %r8d
	andl	$1, %r8d
	movl	$521288629, %ebp        # imm = 0x1F123BB5
	xorl	%esi, %esi
	jmp	.LBB16_10
	.p2align	4, 0x90
.LBB16_9:                               # %._crit_edge.loopexit.i.i..lr.ph176.split_crit_edge
                                        #   in Loop: Header=BB16_10 Depth=1
	movq	16(%rsp), %r11
	addq	%r15, %r14
	movl	%ebx, %r13d
.LBB16_10:                              # %.lr.ph176.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_12 Depth 2
                                        #     Child Loop BB16_15 Depth 2
                                        #     Child Loop BB16_19 Depth 2
	movq	%rsi, %rax
	imulq	%r15, %rax
	addq	48(%rsp), %rax          # 8-byte Folded Reload
	testq	%rdi, %rdi
	leaq	(%rsi,%rsi,4), %r10
	movq	%rax, 16(%r11,%r10,8)
	movl	%r12d, 28(%r11,%r10,8)
	movl	%r13d, 24(%r11,%r10,8)
	je	.LBB16_13
# BB#11:                                # %.lr.ph.i.i.prol.preheader
                                        #   in Loop: Header=BB16_10 Depth=1
	movq	%r14, %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB16_12:                              # %.lr.ph.i.i.prol
                                        #   Parent Loop BB16_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	%bp, %ebx
	imull	$18000, %ebx, %ebx      # imm = 0x4650
	shrl	$16, %ebp
	addl	%ebx, %ebp
	movb	%bpl, (%rcx)
	incq	%rdx
	incq	%rcx
	cmpq	%rdx, %rdi
	jne	.LBB16_12
	jmp	.LBB16_14
	.p2align	4, 0x90
.LBB16_13:                              #   in Loop: Header=BB16_10 Depth=1
	xorl	%edx, %edx
.LBB16_14:                              # %.lr.ph.i.i.prol.loopexit
                                        #   in Loop: Header=BB16_10 Depth=1
	cmpq	$3, %r9
	jb	.LBB16_16
	.p2align	4, 0x90
.LBB16_15:                              # %.lr.ph.i.i
                                        #   Parent Loop BB16_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	%bp, %ecx
	imull	$18000, %ecx, %ecx      # imm = 0x4650
	shrl	$16, %ebp
	addl	%ecx, %ebp
	movb	%bpl, (%r14,%rdx)
	movzwl	%bp, %ecx
	imull	$18000, %ecx, %ecx      # imm = 0x4650
	shrl	$16, %ebp
	addl	%ecx, %ebp
	movb	%bpl, 1(%r14,%rdx)
	movzwl	%bp, %ecx
	imull	$18000, %ecx, %ecx      # imm = 0x4650
	shrl	$16, %ebp
	addl	%ecx, %ebp
	movb	%bpl, 2(%r14,%rdx)
	movzwl	%bp, %ecx
	imull	$18000, %ecx, %ecx      # imm = 0x4650
	shrl	$16, %ebp
	addl	%ecx, %ebp
	movb	%bpl, 3(%r14,%rdx)
	addq	$4, %rdx
	cmpq	%rdx, %r15
	jne	.LBB16_15
.LBB16_16:                              # %.lr.ph.i8.i.preheader
                                        #   in Loop: Header=BB16_10 Depth=1
	movl	%r13d, %ebx
	testq	%r8, %r8
	jne	.LBB16_18
# BB#17:                                #   in Loop: Header=BB16_10 Depth=1
	movl	$-1, %r13d
	xorl	%r12d, %r12d
	testq	%r9, %r9
	jne	.LBB16_19
	jmp	.LBB16_20
	.p2align	4, 0x90
.LBB16_18:                              # %.lr.ph.i8.i.prol
                                        #   in Loop: Header=BB16_10 Depth=1
	movzbl	(%rax), %eax
	xorq	$255, %rax
	movl	g_CrcTable(,%rax,4), %r13d
	movl	$16777215, %eax         # imm = 0xFFFFFF
	xorl	%eax, %r13d
	movl	$1, %r12d
	testq	%r9, %r9
	je	.LBB16_20
	.p2align	4, 0x90
.LBB16_19:                              # %.lr.ph.i8.i
                                        #   Parent Loop BB16_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r14,%r12), %eax
	movzbl	%r13b, %ecx
	xorl	%eax, %ecx
	shrl	$8, %r13d
	xorl	g_CrcTable(,%rcx,4), %r13d
	movzbl	1(%r14,%r12), %eax
	movzbl	%r13b, %ecx
	xorl	%eax, %ecx
	shrl	$8, %r13d
	xorl	g_CrcTable(,%rcx,4), %r13d
	addq	$2, %r12
	cmpq	%r12, %r15
	jne	.LBB16_19
.LBB16_20:                              # %._crit_edge.loopexit.i.i
                                        #   in Loop: Header=BB16_10 Depth=1
	notl	%r13d
	movl	%r13d, 32(%r11,%r10,8)
	incq	%rsi
	cmpq	(%rsp), %rsi            # 8-byte Folded Reload
	movl	28(%rsp), %r12d         # 4-byte Reload
	jb	.LBB16_9
	jmp	.LBB16_50
.LBB16_21:
	movl	$-2147024882, %r12d     # imm = 0x8007000E
	jmp	.LBB16_71
.LBB16_22:
	testl	%r13d, %r13d
	je	.LBB16_38
# BB#23:                                # %.lr.ph.i.i120.preheader
	leaq	-1(%r15), %rcx
	movq	%r15, %rsi
	andq	$3, %rsi
	je	.LBB16_28
# BB#24:                                # %.lr.ph.i.i120.prol.preheader
	movl	$521288629, %eax        # imm = 0x1F123BB5
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB16_25:                              # %.lr.ph.i.i120.prol
                                        # =>This Inner Loop Header: Depth=1
	movzwl	%ax, %edi
	imull	$18000, %edi, %edi      # imm = 0x4650
	shrl	$16, %eax
	addl	%edi, %eax
	movb	%al, (%r14,%rdx)
	incq	%rdx
	cmpq	%rdx, %rsi
	jne	.LBB16_25
	jmp	.LBB16_29
.LBB16_26:                              # %.lr.ph176.split.us.preheader
	testb	$1, (%rsp)              # 1-byte Folded Reload
	jne	.LBB16_47
# BB#27:
	xorl	%ecx, %ecx
	cmpl	$1, (%rsp)              # 4-byte Folded Reload
	jne	.LBB16_48
	jmp	.LBB16_50
.LBB16_28:
	xorl	%edx, %edx
	movl	$521288629, %eax        # imm = 0x1F123BB5
.LBB16_29:                              # %.lr.ph.i.i120.prol.loopexit
	cmpq	$3, %rcx
	jb	.LBB16_32
# BB#30:                                # %.lr.ph.i.i120.preheader.new
	movq	%r15, %rcx
	subq	%rdx, %rcx
	leaq	3(%r14,%rdx), %rdx
	.p2align	4, 0x90
.LBB16_31:                              # %.lr.ph.i.i120
                                        # =>This Inner Loop Header: Depth=1
	movzwl	%ax, %esi
	imull	$18000, %esi, %esi      # imm = 0x4650
	shrl	$16, %eax
	addl	%esi, %eax
	movb	%al, -3(%rdx)
	movzwl	%ax, %esi
	imull	$18000, %esi, %esi      # imm = 0x4650
	shrl	$16, %eax
	addl	%esi, %eax
	movb	%al, -2(%rdx)
	movzwl	%ax, %esi
	imull	$18000, %esi, %esi      # imm = 0x4650
	shrl	$16, %eax
	addl	%esi, %eax
	movb	%al, -1(%rdx)
	movzwl	%ax, %esi
	imull	$18000, %esi, %esi      # imm = 0x4650
	shrl	$16, %eax
	addl	%esi, %eax
	movb	%al, (%rdx)
	addq	$4, %rdx
	addq	$-4, %rcx
	jne	.LBB16_31
.LBB16_32:                              # %.lr.ph.i8.i131.preheader
	testb	$1, %r15b
	jne	.LBB16_34
# BB#33:
	movl	$-1, %ebp
	xorl	%ecx, %ecx
	cmpl	$1, %r13d
	jne	.LBB16_35
	jmp	.LBB16_37
.LBB16_34:                              # %.lr.ph.i8.i131.prol
	movzbl	(%r14), %eax
	xorq	$255, %rax
	movl	$16777215, %ebp         # imm = 0xFFFFFF
	xorl	g_CrcTable(,%rax,4), %ebp
	movl	$1, %ecx
	cmpl	$1, %r13d
	je	.LBB16_37
.LBB16_35:                              # %.lr.ph.i8.i131.preheader.new
	movq	%r15, %rax
	subq	%rcx, %rax
	leaq	1(%r14,%rcx), %rcx
	.p2align	4, 0x90
.LBB16_36:                              # %.lr.ph.i8.i131
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rcx), %edx
	movzbl	%bpl, %esi
	xorl	%edx, %esi
	shrl	$8, %ebp
	xorl	g_CrcTable(,%rsi,4), %ebp
	movzbl	(%rcx), %edx
	movzbl	%bpl, %esi
	xorl	%edx, %esi
	shrl	$8, %ebp
	xorl	g_CrcTable(,%rsi,4), %ebp
	addq	$2, %rcx
	addq	$-2, %rax
	jne	.LBB16_36
.LBB16_37:                              # %._crit_edge.loopexit.i.i125
	notl	%ebp
.LBB16_38:                              # %_ZL10RandGenCrcPhjR20CBaseRandomGenerator.exit133
	leaq	32(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	testl	%eax, %eax
	je	.LBB16_40
# BB#39:
	xorl	%edi, %edi
	callq	time
	imulq	$1000000, %rax, %r13    # imm = 0xF4240
	jmp	.LBB16_41
.LBB16_40:
	imulq	$1000000, 32(%rsp), %r13 # imm = 0xF4240
	addq	40(%rsp), %r13
.LBB16_41:                              # %_ZL12GetTimeCountv.exit135
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_42:                              # %.lr.ph.i136
                                        # =>This Inner Loop Header: Depth=1
.Ltmp137:
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	CrcCalc
.Ltmp138:
# BB#43:                                # %.noexc138
                                        #   in Loop: Header=BB16_42 Depth=1
	cmpl	%ebp, %eax
	jne	.LBB16_69
# BB#44:                                #   in Loop: Header=BB16_42 Depth=1
	incl	%ebx
	cmpl	%r12d, %ebx
	jb	.LBB16_42
# BB#45:
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	56(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB16_65
.LBB16_47:                              # %.lr.ph176.split.us.prol
	movq	%r14, 24(%rax)
	movl	%r12d, 36(%rax)
	movl	$0, 32(%rax)
	movl	$0, 40(%rax)
	movl	$1, %ecx
	cmpl	$1, (%rsp)              # 4-byte Folded Reload
	je	.LBB16_50
.LBB16_48:                              # %.lr.ph176.split.us.preheader.new
	movq	%rcx, %rdx
	imulq	%r15, %rdx
	leaq	(%rcx,%rcx,4), %rsi
	leaq	80(%rax,%rsi,8), %rax
	leaq	1(%rcx), %rsi
	imulq	%r15, %rsi
	addq	%r15, %r15
	movq	48(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB16_49:                              # %.lr.ph176.split.us
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%rdi,%rdx), %rbp
	movq	%rbp, -56(%rax)
	movl	%r12d, -44(%rax)
	movl	$0, -48(%rax)
	movl	$0, -40(%rax)
	leaq	(%rdi,%rsi), %rbp
	movq	%rbp, -16(%rax)
	movl	%r12d, -4(%rax)
	movl	$0, -8(%rax)
	movl	$0, (%rax)
	addq	$2, %rcx
	addq	%r15, %rdi
	addq	$80, %rax
	cmpq	(%rsp), %rcx            # 8-byte Folded Reload
	jb	.LBB16_49
.LBB16_50:                              # %._crit_edge177
	leaq	32(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	testl	%eax, %eax
	je	.LBB16_52
# BB#51:
	xorl	%edi, %edi
	callq	time
	imulq	$1000000, %rax, %r13    # imm = 0xF4240
	jmp	.LBB16_53
.LBB16_52:
	imulq	$1000000, 32(%rsp), %r13 # imm = 0xF4240
	addq	40(%rsp), %r13
.LBB16_53:                              # %.lr.ph171.preheader
	movq	(%rsp), %r15            # 8-byte Reload
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	movq	48(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB16_54:                              # %.lr.ph171
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rdi
	addq	%rbp, %rdi
.Ltmp143:
	movl	$_ZL17CrcThreadFunctionPv, %esi
	movq	%rdi, %rdx
	callq	Thread_Create
	movl	%eax, %r12d
.Ltmp144:
# BB#55:                                # %_ZN8NWindows7CThread6CreateEPFjPvES1_.exit
                                        #   in Loop: Header=BB16_54 Depth=1
	testl	%r12d, %r12d
	jne	.LBB16_70
# BB#56:                                # %.thread
                                        #   in Loop: Header=BB16_54 Depth=1
	movl	8(%rsp), %eax
	incl	%eax
	movl	%eax, 8(%rsp)
	incq	%rbx
	addq	$40, %rbp
	cmpq	%r15, %rbx
	jb	.LBB16_54
# BB#57:                                # %._crit_edge
	testl	%eax, %eax
	movl	28(%rsp), %r12d         # 4-byte Reload
	je	.LBB16_62
# BB#58:                                # %.lr.ph.i.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB16_59:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %eax
	leaq	(%rax,%rax,4), %rbx
	shlq	$3, %rbx
	addq	16(%rsp), %rbx
.Ltmp146:
	movq	%rbx, %rdi
	callq	Thread_Wait
.Ltmp147:
# BB#60:                                # %.noexc117
                                        #   in Loop: Header=BB16_59 Depth=1
.Ltmp148:
	movq	%rbx, %rdi
	callq	Thread_Close
.Ltmp149:
# BB#61:                                # %.noexc118
                                        #   in Loop: Header=BB16_59 Depth=1
	incl	%ebp
	cmpl	8(%rsp), %ebp
	jb	.LBB16_59
.LBB16_62:                              # %.lr.ph
	movl	$0, 8(%rsp)
	movq	16(%rsp), %rax
	addq	$36, %rax
	xorl	%ecx, %ecx
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	56(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB16_63:                              # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rax)
	je	.LBB16_69
# BB#64:                                #   in Loop: Header=BB16_63 Depth=1
	incl	%ecx
	addq	$40, %rax
	cmpl	%r15d, %ecx
	jb	.LBB16_63
.LBB16_65:                              # %.thread159
	leaq	32(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	testl	%eax, %eax
	je	.LBB16_67
# BB#66:
	xorl	%edi, %edi
	callq	time
	imulq	$1000000, %rax, %rax    # imm = 0xF4240
	jmp	.LBB16_68
.LBB16_69:
	movl	$1, %r12d
	jmp	.LBB16_70
.LBB16_67:
	imulq	$1000000, 32(%rsp), %rax # imm = 0xF4240
	addq	40(%rsp), %rax
.LBB16_68:                              # %_ZL12GetTimeCountv.exit140
	subq	%r13, %rax
	movl	$1, %ecx
	cmovneq	%rax, %rcx
	movl	%r12d, %eax
	imulq	%rax, %rbp
	imulq	$1000000, %rbp, %rax    # imm = 0xF4240
	xorl	%r12d, %r12d
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, (%rbx)
.LBB16_70:                              # %_ZL6CrcBigPKvjjj.exit
.Ltmp153:
	leaq	8(%rsp), %rdi
	callq	_ZN11CCrcThreadsD2Ev
.Ltmp154:
.LBB16_71:
	movq	%r14, %rdi
	callq	MidFree
	movl	%r12d, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB16_72:
.Ltmp142:
	jmp	.LBB16_78
.LBB16_73:
.Ltmp155:
	movq	%rax, %rbx
	jmp	.LBB16_79
.LBB16_74:
.Ltmp136:
	movq	%rax, %rbx
	xorl	%r14d, %r14d
	jmp	.LBB16_79
.LBB16_75:
.Ltmp145:
	jmp	.LBB16_78
.LBB16_76:
.Ltmp139:
	jmp	.LBB16_78
.LBB16_77:
.Ltmp150:
.LBB16_78:
	movq	%rax, %rbx
.Ltmp151:
	leaq	8(%rsp), %rdi
	callq	_ZN11CCrcThreadsD2Ev
.Ltmp152:
.LBB16_79:
.Ltmp156:
	movq	%r14, %rdi
	callq	MidFree
.Ltmp157:
# BB#80:                                # %_ZN12CBenchBufferD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB16_81:
.Ltmp158:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end16:
	.size	_Z8CrcBenchjjRy, .Lfunc_end16-_Z8CrcBenchjjRy
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	125                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Ltmp132-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp135-.Ltmp132       #   Call between .Ltmp132 and .Ltmp135
	.long	.Ltmp136-.Lfunc_begin5  #     jumps to .Ltmp136
	.byte	0                       #   On action: cleanup
	.long	.Ltmp140-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp141-.Ltmp140       #   Call between .Ltmp140 and .Ltmp141
	.long	.Ltmp142-.Lfunc_begin5  #     jumps to .Ltmp142
	.byte	0                       #   On action: cleanup
	.long	.Ltmp137-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp138-.Ltmp137       #   Call between .Ltmp137 and .Ltmp138
	.long	.Ltmp139-.Lfunc_begin5  #     jumps to .Ltmp139
	.byte	0                       #   On action: cleanup
	.long	.Ltmp143-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp144-.Ltmp143       #   Call between .Ltmp143 and .Ltmp144
	.long	.Ltmp145-.Lfunc_begin5  #     jumps to .Ltmp145
	.byte	0                       #   On action: cleanup
	.long	.Ltmp146-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp149-.Ltmp146       #   Call between .Ltmp146 and .Ltmp149
	.long	.Ltmp150-.Lfunc_begin5  #     jumps to .Ltmp150
	.byte	0                       #   On action: cleanup
	.long	.Ltmp153-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp154-.Ltmp153       #   Call between .Ltmp153 and .Ltmp154
	.long	.Ltmp155-.Lfunc_begin5  #     jumps to .Ltmp155
	.byte	0                       #   On action: cleanup
	.long	.Ltmp154-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Ltmp151-.Ltmp154       #   Call between .Ltmp154 and .Ltmp151
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp151-.Lfunc_begin5  # >> Call Site 8 <<
	.long	.Ltmp157-.Ltmp151       #   Call between .Ltmp151 and .Ltmp157
	.long	.Ltmp158-.Lfunc_begin5  #     jumps to .Ltmp158
	.byte	1                       #   On action: 1
	.long	.Ltmp157-.Lfunc_begin5  # >> Call Site 9 <<
	.long	.Lfunc_end16-.Ltmp157   #   Call between .Ltmp157 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZL17CrcThreadFunctionPv,@function
_ZL17CrcThreadFunctionPv:               # @_ZL17CrcThreadFunctionPv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi114:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi115:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi116:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi117:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi118:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi119:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi120:
	.cfi_def_cfa_offset 64
.Lcfi121:
	.cfi_offset %rbx, -56
.Lcfi122:
	.cfi_offset %r12, -48
.Lcfi123:
	.cfi_offset %r13, -40
.Lcfi124:
	.cfi_offset %r14, -32
.Lcfi125:
	.cfi_offset %r15, -24
.Lcfi126:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	28(%r14), %r13d
	movb	$1, %al
	testl	%r13d, %r13d
	je	.LBB17_6
# BB#1:                                 # %.lr.ph.i.preheader
	movq	16(%r14), %r15
	movl	32(%r14), %ebp
	movl	24(%r14), %ebx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB17_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	CrcCalc
	cmpl	%ebp, %eax
	jne	.LBB17_5
# BB#3:                                 #   in Loop: Header=BB17_2 Depth=1
	incl	%r12d
	cmpl	%r13d, %r12d
	jb	.LBB17_2
# BB#4:
	movb	$1, %al
	jmp	.LBB17_6
.LBB17_5:
	xorl	%eax, %eax
.LBB17_6:                               # %_ZL6CrcBigPKvjjj.exit
	movb	%al, 36(%r14)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	_ZL17CrcThreadFunctionPv, .Lfunc_end17-_ZL17CrcThreadFunctionPv
	.cfi_endproc

	.section	.text._ZN11CCrcThreadsD2Ev,"axG",@progbits,_ZN11CCrcThreadsD2Ev,comdat
	.weak	_ZN11CCrcThreadsD2Ev
	.p2align	4, 0x90
	.type	_ZN11CCrcThreadsD2Ev,@function
_ZN11CCrcThreadsD2Ev:                   # @_ZN11CCrcThreadsD2Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi127:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi128:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi129:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi130:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi131:
	.cfi_def_cfa_offset 48
.Lcfi132:
	.cfi_offset %rbx, -48
.Lcfi133:
	.cfi_offset %r12, -40
.Lcfi134:
	.cfi_offset %r14, -32
.Lcfi135:
	.cfi_offset %r15, -24
.Lcfi136:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpl	$0, (%r14)
	je	.LBB18_3
# BB#1:                                 # %.lr.ph.i.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB18_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r14), %rax
	movl	%ebp, %ecx
	leaq	(%rcx,%rcx,4), %rcx
	leaq	(%rax,%rcx,8), %rbx
	movq	%rbx, %rdi
	callq	Thread_Wait
	movq	%rbx, %rdi
	callq	Thread_Close
	incl	%ebp
	cmpl	(%r14), %ebp
	jb	.LBB18_2
.LBB18_3:                               # %_ZN11CCrcThreads7WaitAllEv.exit
	movl	$0, (%r14)
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.LBB18_9
# BB#4:
	leaq	-8(%r12), %r15
	movq	-8(%r12), %rax
	testq	%rax, %rax
	je	.LBB18_8
# BB#5:                                 # %.preheader3.preheader
	shlq	$3, %rax
	leaq	(%rax,%rax,4), %rbx
	.p2align	4, 0x90
.LBB18_6:                               # %.preheader3
                                        # =>This Inner Loop Header: Depth=1
	leaq	-40(%r12,%rbx), %rdi
.Ltmp159:
	callq	Thread_Close
.Ltmp160:
# BB#7:                                 # %_ZN8CCrcInfoD2Ev.exit
                                        #   in Loop: Header=BB18_6 Depth=1
	addq	$-40, %rbx
	jne	.LBB18_6
.LBB18_8:                               # %.loopexit4
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZdaPv                  # TAILCALL
.LBB18_9:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB18_10:
.Ltmp161:
	movq	%rax, %r14
	cmpq	$40, %rbx
	jne	.LBB18_12
	jmp	.LBB18_14
	.p2align	4, 0x90
.LBB18_13:                              # %_ZN8CCrcInfoD2Ev.exit2
                                        #   in Loop: Header=BB18_12 Depth=1
	addq	$-40, %rbx
	cmpq	$40, %rbx
	je	.LBB18_14
.LBB18_12:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	leaq	-80(%r12,%rbx), %rdi
.Ltmp162:
	callq	Thread_Close
.Ltmp163:
	jmp	.LBB18_13
.LBB18_14:                              # %.loopexit
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB18_15:
.Ltmp164:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end18:
	.size	_ZN11CCrcThreadsD2Ev, .Lfunc_end18-_ZN11CCrcThreadsD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp159-.Lfunc_begin6  #   Call between .Lfunc_begin6 and .Ltmp159
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp159-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp160-.Ltmp159       #   Call between .Ltmp159 and .Ltmp160
	.long	.Ltmp161-.Lfunc_begin6  #     jumps to .Ltmp161
	.byte	0                       #   On action: cleanup
	.long	.Ltmp162-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp163-.Ltmp162       #   Call between .Ltmp162 and .Ltmp163
	.long	.Ltmp164-.Lfunc_begin6  #     jumps to .Ltmp164
	.byte	1                       #   On action: 1
	.long	.Ltmp163-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Lfunc_end18-.Ltmp163   #   Call between .Ltmp163 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN18CBenchmarkInStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN18CBenchmarkInStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN18CBenchmarkInStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN18CBenchmarkInStream14QueryInterfaceERK4GUIDPPv,@function
_ZN18CBenchmarkInStream14QueryInterfaceERK4GUIDPPv: # @_ZN18CBenchmarkInStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi137:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB19_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB19_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB19_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB19_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB19_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB19_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB19_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB19_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB19_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB19_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB19_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB19_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB19_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB19_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB19_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB19_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB19_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end19:
	.size	_ZN18CBenchmarkInStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end19-_ZN18CBenchmarkInStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN18CBenchmarkInStream6AddRefEv,"axG",@progbits,_ZN18CBenchmarkInStream6AddRefEv,comdat
	.weak	_ZN18CBenchmarkInStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN18CBenchmarkInStream6AddRefEv,@function
_ZN18CBenchmarkInStream6AddRefEv:       # @_ZN18CBenchmarkInStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end20:
	.size	_ZN18CBenchmarkInStream6AddRefEv, .Lfunc_end20-_ZN18CBenchmarkInStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN18CBenchmarkInStream7ReleaseEv,"axG",@progbits,_ZN18CBenchmarkInStream7ReleaseEv,comdat
	.weak	_ZN18CBenchmarkInStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN18CBenchmarkInStream7ReleaseEv,@function
_ZN18CBenchmarkInStream7ReleaseEv:      # @_ZN18CBenchmarkInStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi138:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB21_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB21_2:
	popq	%rcx
	retq
.Lfunc_end21:
	.size	_ZN18CBenchmarkInStream7ReleaseEv, .Lfunc_end21-_ZN18CBenchmarkInStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8IUnknownD2Ev,"axG",@progbits,_ZN8IUnknownD2Ev,comdat
	.weak	_ZN8IUnknownD2Ev
	.p2align	4, 0x90
	.type	_ZN8IUnknownD2Ev,@function
_ZN8IUnknownD2Ev:                       # @_ZN8IUnknownD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end22:
	.size	_ZN8IUnknownD2Ev, .Lfunc_end22-_ZN8IUnknownD2Ev
	.cfi_endproc

	.section	.text._ZN18CBenchmarkInStreamD0Ev,"axG",@progbits,_ZN18CBenchmarkInStreamD0Ev,comdat
	.weak	_ZN18CBenchmarkInStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN18CBenchmarkInStreamD0Ev,@function
_ZN18CBenchmarkInStreamD0Ev:            # @_ZN18CBenchmarkInStreamD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end23:
	.size	_ZN18CBenchmarkInStreamD0Ev, .Lfunc_end23-_ZN18CBenchmarkInStreamD0Ev
	.cfi_endproc

	.section	.text._ZN19CBenchmarkOutStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN19CBenchmarkOutStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN19CBenchmarkOutStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN19CBenchmarkOutStream14QueryInterfaceERK4GUIDPPv,@function
_ZN19CBenchmarkOutStream14QueryInterfaceERK4GUIDPPv: # @_ZN19CBenchmarkOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi139:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB24_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB24_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB24_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB24_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB24_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB24_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB24_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB24_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB24_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB24_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB24_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB24_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB24_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB24_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB24_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB24_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB24_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end24:
	.size	_ZN19CBenchmarkOutStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end24-_ZN19CBenchmarkOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN19CBenchmarkOutStream6AddRefEv,"axG",@progbits,_ZN19CBenchmarkOutStream6AddRefEv,comdat
	.weak	_ZN19CBenchmarkOutStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN19CBenchmarkOutStream6AddRefEv,@function
_ZN19CBenchmarkOutStream6AddRefEv:      # @_ZN19CBenchmarkOutStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	32(%rdi), %eax
	incl	%eax
	movl	%eax, 32(%rdi)
	retq
.Lfunc_end25:
	.size	_ZN19CBenchmarkOutStream6AddRefEv, .Lfunc_end25-_ZN19CBenchmarkOutStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN19CBenchmarkOutStream7ReleaseEv,"axG",@progbits,_ZN19CBenchmarkOutStream7ReleaseEv,comdat
	.weak	_ZN19CBenchmarkOutStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN19CBenchmarkOutStream7ReleaseEv,@function
_ZN19CBenchmarkOutStream7ReleaseEv:     # @_ZN19CBenchmarkOutStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi140:
	.cfi_def_cfa_offset 16
	movl	32(%rdi), %eax
	decl	%eax
	movl	%eax, 32(%rdi)
	jne	.LBB26_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB26_2:
	popq	%rcx
	retq
.Lfunc_end26:
	.size	_ZN19CBenchmarkOutStream7ReleaseEv, .Lfunc_end26-_ZN19CBenchmarkOutStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN19CBenchmarkOutStreamD2Ev,"axG",@progbits,_ZN19CBenchmarkOutStreamD2Ev,comdat
	.weak	_ZN19CBenchmarkOutStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN19CBenchmarkOutStreamD2Ev,@function
_ZN19CBenchmarkOutStreamD2Ev:           # @_ZN19CBenchmarkOutStreamD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi141:
	.cfi_def_cfa_offset 16
.Lcfi142:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV12CBenchBuffer+16, 8(%rbx)
	movq	24(%rbx), %rdi
	callq	MidFree
	movq	$0, 24(%rbx)
	popq	%rbx
	retq
.Lfunc_end27:
	.size	_ZN19CBenchmarkOutStreamD2Ev, .Lfunc_end27-_ZN19CBenchmarkOutStreamD2Ev
	.cfi_endproc

	.section	.text._ZN19CBenchmarkOutStreamD0Ev,"axG",@progbits,_ZN19CBenchmarkOutStreamD0Ev,comdat
	.weak	_ZN19CBenchmarkOutStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN19CBenchmarkOutStreamD0Ev,@function
_ZN19CBenchmarkOutStreamD0Ev:           # @_ZN19CBenchmarkOutStreamD0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi143:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi144:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi145:
	.cfi_def_cfa_offset 32
.Lcfi146:
	.cfi_offset %rbx, -24
.Lcfi147:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV12CBenchBuffer+16, 8(%rbx)
	movq	24(%rbx), %rdi
.Ltmp165:
	callq	MidFree
.Ltmp166:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB28_2:
.Ltmp167:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end28:
	.size	_ZN19CBenchmarkOutStreamD0Ev, .Lfunc_end28-_ZN19CBenchmarkOutStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table28:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp165-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp166-.Ltmp165       #   Call between .Ltmp165 and .Ltmp166
	.long	.Ltmp167-.Lfunc_begin7  #     jumps to .Ltmp167
	.byte	0                       #   On action: cleanup
	.long	.Ltmp166-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Lfunc_end28-.Ltmp166   #   Call between .Ltmp166 and .Lfunc_end28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N19CBenchmarkOutStreamD1Ev,"axG",@progbits,_ZThn8_N19CBenchmarkOutStreamD1Ev,comdat
	.weak	_ZThn8_N19CBenchmarkOutStreamD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N19CBenchmarkOutStreamD1Ev,@function
_ZThn8_N19CBenchmarkOutStreamD1Ev:      # @_ZThn8_N19CBenchmarkOutStreamD1Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi148:
	.cfi_def_cfa_offset 16
.Lcfi149:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV12CBenchBuffer+16, (%rbx)
	movq	16(%rbx), %rdi
	callq	MidFree
	movq	$0, 16(%rbx)
	popq	%rbx
	retq
.Lfunc_end29:
	.size	_ZThn8_N19CBenchmarkOutStreamD1Ev, .Lfunc_end29-_ZThn8_N19CBenchmarkOutStreamD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N19CBenchmarkOutStreamD0Ev,"axG",@progbits,_ZThn8_N19CBenchmarkOutStreamD0Ev,comdat
	.weak	_ZThn8_N19CBenchmarkOutStreamD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N19CBenchmarkOutStreamD0Ev,@function
_ZThn8_N19CBenchmarkOutStreamD0Ev:      # @_ZThn8_N19CBenchmarkOutStreamD0Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi150:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi151:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi152:
	.cfi_def_cfa_offset 32
.Lcfi153:
	.cfi_offset %rbx, -24
.Lcfi154:
	.cfi_offset %r14, -16
	movq	$_ZTV12CBenchBuffer+16, (%rdi)
	movq	16(%rdi), %rax
	leaq	-8(%rdi), %rbx
.Ltmp168:
	movq	%rax, %rdi
	callq	MidFree
.Ltmp169:
# BB#1:                                 # %_ZN19CBenchmarkOutStreamD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB30_2:
.Ltmp170:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end30:
	.size	_ZThn8_N19CBenchmarkOutStreamD0Ev, .Lfunc_end30-_ZThn8_N19CBenchmarkOutStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table30:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp168-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp169-.Ltmp168       #   Call between .Ltmp168 and .Ltmp169
	.long	.Ltmp170-.Lfunc_begin8  #     jumps to .Ltmp170
	.byte	0                       #   On action: cleanup
	.long	.Ltmp169-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Lfunc_end30-.Ltmp169   #   Call between .Ltmp169 and .Lfunc_end30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CCrcOutStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN13CCrcOutStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN13CCrcOutStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN13CCrcOutStream14QueryInterfaceERK4GUIDPPv,@function
_ZN13CCrcOutStream14QueryInterfaceERK4GUIDPPv: # @_ZN13CCrcOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi155:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB31_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB31_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB31_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB31_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB31_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB31_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB31_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB31_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB31_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB31_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB31_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB31_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB31_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB31_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB31_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB31_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB31_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end31:
	.size	_ZN13CCrcOutStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end31-_ZN13CCrcOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN13CCrcOutStream6AddRefEv,"axG",@progbits,_ZN13CCrcOutStream6AddRefEv,comdat
	.weak	_ZN13CCrcOutStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN13CCrcOutStream6AddRefEv,@function
_ZN13CCrcOutStream6AddRefEv:            # @_ZN13CCrcOutStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end32:
	.size	_ZN13CCrcOutStream6AddRefEv, .Lfunc_end32-_ZN13CCrcOutStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN13CCrcOutStream7ReleaseEv,"axG",@progbits,_ZN13CCrcOutStream7ReleaseEv,comdat
	.weak	_ZN13CCrcOutStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN13CCrcOutStream7ReleaseEv,@function
_ZN13CCrcOutStream7ReleaseEv:           # @_ZN13CCrcOutStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi156:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB33_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB33_2:
	popq	%rcx
	retq
.Lfunc_end33:
	.size	_ZN13CCrcOutStream7ReleaseEv, .Lfunc_end33-_ZN13CCrcOutStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN13CCrcOutStreamD0Ev,"axG",@progbits,_ZN13CCrcOutStreamD0Ev,comdat
	.weak	_ZN13CCrcOutStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN13CCrcOutStreamD0Ev,@function
_ZN13CCrcOutStreamD0Ev:                 # @_ZN13CCrcOutStreamD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end34:
	.size	_ZN13CCrcOutStreamD0Ev, .Lfunc_end34-_ZN13CCrcOutStreamD0Ev
	.cfi_endproc

	.section	.text._ZN18CBenchProgressInfo14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN18CBenchProgressInfo14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN18CBenchProgressInfo14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN18CBenchProgressInfo14QueryInterfaceERK4GUIDPPv,@function
_ZN18CBenchProgressInfo14QueryInterfaceERK4GUIDPPv: # @_ZN18CBenchProgressInfo14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi157:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB35_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB35_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB35_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB35_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB35_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB35_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB35_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB35_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB35_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB35_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB35_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB35_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB35_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB35_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB35_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB35_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB35_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end35:
	.size	_ZN18CBenchProgressInfo14QueryInterfaceERK4GUIDPPv, .Lfunc_end35-_ZN18CBenchProgressInfo14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN18CBenchProgressInfo6AddRefEv,"axG",@progbits,_ZN18CBenchProgressInfo6AddRefEv,comdat
	.weak	_ZN18CBenchProgressInfo6AddRefEv
	.p2align	4, 0x90
	.type	_ZN18CBenchProgressInfo6AddRefEv,@function
_ZN18CBenchProgressInfo6AddRefEv:       # @_ZN18CBenchProgressInfo6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end36:
	.size	_ZN18CBenchProgressInfo6AddRefEv, .Lfunc_end36-_ZN18CBenchProgressInfo6AddRefEv
	.cfi_endproc

	.section	.text._ZN18CBenchProgressInfo7ReleaseEv,"axG",@progbits,_ZN18CBenchProgressInfo7ReleaseEv,comdat
	.weak	_ZN18CBenchProgressInfo7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN18CBenchProgressInfo7ReleaseEv,@function
_ZN18CBenchProgressInfo7ReleaseEv:      # @_ZN18CBenchProgressInfo7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi158:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB37_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB37_2:
	popq	%rcx
	retq
.Lfunc_end37:
	.size	_ZN18CBenchProgressInfo7ReleaseEv, .Lfunc_end37-_ZN18CBenchProgressInfo7ReleaseEv
	.cfi_endproc

	.section	.text._ZN18CBenchProgressInfoD0Ev,"axG",@progbits,_ZN18CBenchProgressInfoD0Ev,comdat
	.weak	_ZN18CBenchProgressInfoD0Ev
	.p2align	4, 0x90
	.type	_ZN18CBenchProgressInfoD0Ev,@function
_ZN18CBenchProgressInfoD0Ev:            # @_ZN18CBenchProgressInfoD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end38:
	.size	_ZN18CBenchProgressInfoD0Ev, .Lfunc_end38-_ZN18CBenchProgressInfoD0Ev
	.cfi_endproc

	.section	.text._ZN12CBenchBufferD0Ev,"axG",@progbits,_ZN12CBenchBufferD0Ev,comdat
	.weak	_ZN12CBenchBufferD0Ev
	.p2align	4, 0x90
	.type	_ZN12CBenchBufferD0Ev,@function
_ZN12CBenchBufferD0Ev:                  # @_ZN12CBenchBufferD0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi159:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi160:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi161:
	.cfi_def_cfa_offset 32
.Lcfi162:
	.cfi_offset %rbx, -24
.Lcfi163:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV12CBenchBuffer+16, (%rbx)
	movq	16(%rbx), %rdi
.Ltmp171:
	callq	MidFree
.Ltmp172:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB39_2:
.Ltmp173:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end39:
	.size	_ZN12CBenchBufferD0Ev, .Lfunc_end39-_ZN12CBenchBufferD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table39:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp171-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp172-.Ltmp171       #   Call between .Ltmp171 and .Ltmp172
	.long	.Ltmp173-.Lfunc_begin9  #     jumps to .Ltmp173
	.byte	0                       #   On action: cleanup
	.long	.Ltmp172-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Lfunc_end39-.Ltmp172   #   Call between .Ltmp172 and .Lfunc_end39
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN12CEncoderInfoD2Ev,"axG",@progbits,_ZN12CEncoderInfoD2Ev,comdat
	.weak	_ZN12CEncoderInfoD2Ev
	.p2align	4, 0x90
	.type	_ZN12CEncoderInfoD2Ev,@function
_ZN12CEncoderInfoD2Ev:                  # @_ZN12CEncoderInfoD2Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r15
.Lcfi164:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi165:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi166:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi167:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi168:
	.cfi_def_cfa_offset 48
.Lcfi169:
	.cfi_offset %rbx, -40
.Lcfi170:
	.cfi_offset %r12, -32
.Lcfi171:
	.cfi_offset %r14, -24
.Lcfi172:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	216(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB40_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp174:
	callq	*16(%rax)
.Ltmp175:
.LBB40_2:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	$_ZTV12CBenchBuffer+16, 176(%r15)
	movq	192(%r15), %rdi
.Ltmp179:
	callq	MidFree
.Ltmp180:
# BB#3:
	movq	$0, 192(%r15)
	movq	144(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB40_5
# BB#4:
	movq	(%rdi), %rax
.Ltmp184:
	callq	*16(%rax)
.Ltmp185:
.LBB40_5:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit10
	leaq	112(%r15), %r12
	movq	120(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB40_7
# BB#6:
	leaq	120(%r15), %rbx
	movq	(%rdi), %rax
.Ltmp192:
	callq	*16(%rax)
.Ltmp193:
.LBB40_7:                               # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	movq	112(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB40_9
# BB#8:
	movq	%r12, %rbx
	movq	(%rdi), %rax
.Ltmp194:
	callq	*16(%rax)
.Ltmp195:
.LBB40_9:                               # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit.1
	leaq	56(%r15), %r12
	movq	64(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB40_11
# BB#10:
	leaq	64(%r15), %rbx
	movq	(%rdi), %rax
.Ltmp205:
	callq	*16(%rax)
.Ltmp206:
.LBB40_11:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	movq	56(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB40_13
# BB#12:
	movq	%r12, %rbx
	movq	(%rdi), %rax
.Ltmp207:
	callq	*16(%rax)
.Ltmp208:
.LBB40_13:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit.1
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB40_15
# BB#14:
	movq	(%rdi), %rax
.Ltmp216:
	callq	*16(%rax)
.Ltmp217:
.LBB40_15:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit14
	leaq	16(%r15), %rbx
.Ltmp224:
	movq	%rbx, %rdi
	callq	Thread_Close
.Ltmp225:
# BB#16:                                # %_ZN8NWindows7CThreadD2Ev.exit
.Ltmp226:
	movq	%r15, %rbx
	movq	%r15, %rdi
	callq	Thread_Close
.Ltmp227:
# BB#17:                                # %_ZN8NWindows7CThreadD2Ev.exit.1
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB40_47:
.Ltmp218:
	movq	%rax, %r14
	jmp	.LBB40_48
.LBB40_32:
.Ltmp186:
	movq	%rax, %r14
	jmp	.LBB40_33
.LBB40_18:
.Ltmp176:
	movq	%rax, %r14
	movq	$_ZTV12CBenchBuffer+16, 176(%r15)
	movq	192(%r15), %rdi
.Ltmp177:
	callq	MidFree
.Ltmp178:
# BB#19:                                # %_ZN12CBenchBufferD2Ev.exit17
	movq	$0, 192(%r15)
	jmp	.LBB40_21
.LBB40_20:
.Ltmp181:
	movq	%rax, %r14
.LBB40_21:
	movq	144(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB40_33
# BB#22:
	movq	(%rdi), %rax
.Ltmp182:
	callq	*16(%rax)
.Ltmp183:
.LBB40_33:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit19
	movq	120(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB40_35
# BB#34:
	movq	(%rdi), %rax
.Ltmp187:
	callq	*16(%rax)
.Ltmp188:
.LBB40_35:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit23
	movq	112(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB40_28
# BB#36:
	movq	(%rdi), %rax
.Ltmp189:
	callq	*16(%rax)
.Ltmp190:
	jmp	.LBB40_28
.LBB40_56:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp191:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB40_37:
.Ltmp209:
	movq	%rax, %r14
	cmpq	%rbx, %r12
	je	.LBB40_42
# BB#38:                                # %.preheader39.preheader
	addq	$-56, %rbx
	.p2align	4, 0x90
.LBB40_39:                              # %.preheader39
                                        # =>This Inner Loop Header: Depth=1
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB40_41
# BB#40:                                #   in Loop: Header=BB40_39 Depth=1
	movq	(%rdi), %rax
.Ltmp210:
	callq	*16(%rax)
.Ltmp211:
.LBB40_41:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit25
                                        #   in Loop: Header=BB40_39 Depth=1
	addq	$-8, %rbx
	cmpq	%rbx, %r15
	jne	.LBB40_39
	jmp	.LBB40_42
.LBB40_53:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp212:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB40_23:
.Ltmp196:
	movq	%rax, %r14
	cmpq	%rbx, %r12
	je	.LBB40_28
# BB#24:                                # %.preheader47.preheader
	addq	$-112, %rbx
	.p2align	4, 0x90
.LBB40_25:                              # %.preheader47
                                        # =>This Inner Loop Header: Depth=1
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB40_27
# BB#26:                                #   in Loop: Header=BB40_25 Depth=1
	movq	(%rdi), %rax
.Ltmp197:
	callq	*16(%rax)
.Ltmp198:
.LBB40_27:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit21
                                        #   in Loop: Header=BB40_25 Depth=1
	addq	$-8, %rbx
	cmpq	%rbx, %r15
	jne	.LBB40_25
.LBB40_28:                              # %.loopexit50
	movq	64(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB40_30
# BB#29:
	movq	(%rdi), %rax
.Ltmp200:
	callq	*16(%rax)
.Ltmp201:
.LBB40_30:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit27
	movq	56(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB40_42
# BB#31:
	movq	(%rdi), %rax
.Ltmp202:
	callq	*16(%rax)
.Ltmp203:
.LBB40_42:                              # %.loopexit42
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB40_48
# BB#43:
	movq	(%rdi), %rax
.Ltmp213:
	callq	*16(%rax)
.Ltmp214:
.LBB40_48:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit29
	leaq	16(%r15), %rdi
.Ltmp219:
	callq	Thread_Close
.Ltmp220:
# BB#49:                                # %_ZN8NWindows7CThreadD2Ev.exit33
.Ltmp221:
	movq	%r15, %rdi
	callq	Thread_Close
.Ltmp222:
.LBB40_50:                              # %.loopexit34
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB40_54:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp204:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB40_57:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp
.Ltmp215:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB40_52:                              # %.loopexit.split-lp.loopexit
.Ltmp223:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB40_55:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp199:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB40_44:
.Ltmp228:
	movq	%rax, %r14
	.p2align	4, 0x90
.LBB40_45:                              # =>This Inner Loop Header: Depth=1
	cmpq	%rbx, %r15
	je	.LBB40_50
# BB#46:                                # %.preheader
                                        #   in Loop: Header=BB40_45 Depth=1
	addq	$-16, %rbx
.Ltmp229:
	movq	%rbx, %rdi
	callq	Thread_Close
.Ltmp230:
	jmp	.LBB40_45
.LBB40_51:                              # %.loopexit
.Ltmp231:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end40:
	.size	_ZN12CEncoderInfoD2Ev, .Lfunc_end40-_ZN12CEncoderInfoD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table40:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\331\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\320\001"              # Call site table length
	.long	.Ltmp174-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp175-.Ltmp174       #   Call between .Ltmp174 and .Ltmp175
	.long	.Ltmp176-.Lfunc_begin10 #     jumps to .Ltmp176
	.byte	0                       #   On action: cleanup
	.long	.Ltmp179-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp180-.Ltmp179       #   Call between .Ltmp179 and .Ltmp180
	.long	.Ltmp181-.Lfunc_begin10 #     jumps to .Ltmp181
	.byte	0                       #   On action: cleanup
	.long	.Ltmp184-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp185-.Ltmp184       #   Call between .Ltmp184 and .Ltmp185
	.long	.Ltmp186-.Lfunc_begin10 #     jumps to .Ltmp186
	.byte	0                       #   On action: cleanup
	.long	.Ltmp192-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Ltmp195-.Ltmp192       #   Call between .Ltmp192 and .Ltmp195
	.long	.Ltmp196-.Lfunc_begin10 #     jumps to .Ltmp196
	.byte	0                       #   On action: cleanup
	.long	.Ltmp205-.Lfunc_begin10 # >> Call Site 5 <<
	.long	.Ltmp208-.Ltmp205       #   Call between .Ltmp205 and .Ltmp208
	.long	.Ltmp209-.Lfunc_begin10 #     jumps to .Ltmp209
	.byte	0                       #   On action: cleanup
	.long	.Ltmp216-.Lfunc_begin10 # >> Call Site 6 <<
	.long	.Ltmp217-.Ltmp216       #   Call between .Ltmp216 and .Ltmp217
	.long	.Ltmp218-.Lfunc_begin10 #     jumps to .Ltmp218
	.byte	0                       #   On action: cleanup
	.long	.Ltmp224-.Lfunc_begin10 # >> Call Site 7 <<
	.long	.Ltmp227-.Ltmp224       #   Call between .Ltmp224 and .Ltmp227
	.long	.Ltmp228-.Lfunc_begin10 #     jumps to .Ltmp228
	.byte	0                       #   On action: cleanup
	.long	.Ltmp177-.Lfunc_begin10 # >> Call Site 8 <<
	.long	.Ltmp183-.Ltmp177       #   Call between .Ltmp177 and .Ltmp183
	.long	.Ltmp215-.Lfunc_begin10 #     jumps to .Ltmp215
	.byte	1                       #   On action: 1
	.long	.Ltmp187-.Lfunc_begin10 # >> Call Site 9 <<
	.long	.Ltmp190-.Ltmp187       #   Call between .Ltmp187 and .Ltmp190
	.long	.Ltmp191-.Lfunc_begin10 #     jumps to .Ltmp191
	.byte	1                       #   On action: 1
	.long	.Ltmp210-.Lfunc_begin10 # >> Call Site 10 <<
	.long	.Ltmp211-.Ltmp210       #   Call between .Ltmp210 and .Ltmp211
	.long	.Ltmp212-.Lfunc_begin10 #     jumps to .Ltmp212
	.byte	1                       #   On action: 1
	.long	.Ltmp197-.Lfunc_begin10 # >> Call Site 11 <<
	.long	.Ltmp198-.Ltmp197       #   Call between .Ltmp197 and .Ltmp198
	.long	.Ltmp199-.Lfunc_begin10 #     jumps to .Ltmp199
	.byte	1                       #   On action: 1
	.long	.Ltmp200-.Lfunc_begin10 # >> Call Site 12 <<
	.long	.Ltmp203-.Ltmp200       #   Call between .Ltmp200 and .Ltmp203
	.long	.Ltmp204-.Lfunc_begin10 #     jumps to .Ltmp204
	.byte	1                       #   On action: 1
	.long	.Ltmp213-.Lfunc_begin10 # >> Call Site 13 <<
	.long	.Ltmp214-.Ltmp213       #   Call between .Ltmp213 and .Ltmp214
	.long	.Ltmp215-.Lfunc_begin10 #     jumps to .Ltmp215
	.byte	1                       #   On action: 1
	.long	.Ltmp219-.Lfunc_begin10 # >> Call Site 14 <<
	.long	.Ltmp222-.Ltmp219       #   Call between .Ltmp219 and .Ltmp222
	.long	.Ltmp223-.Lfunc_begin10 #     jumps to .Ltmp223
	.byte	1                       #   On action: 1
	.long	.Ltmp222-.Lfunc_begin10 # >> Call Site 15 <<
	.long	.Ltmp229-.Ltmp222       #   Call between .Ltmp222 and .Ltmp229
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp229-.Lfunc_begin10 # >> Call Site 16 <<
	.long	.Ltmp230-.Ltmp229       #   Call between .Ltmp229 and .Ltmp230
	.long	.Ltmp231-.Lfunc_begin10 #     jumps to .Ltmp231
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN12CBenchBufferD2Ev,"axG",@progbits,_ZN12CBenchBufferD2Ev,comdat
	.weak	_ZN12CBenchBufferD2Ev
	.p2align	4, 0x90
	.type	_ZN12CBenchBufferD2Ev,@function
_ZN12CBenchBufferD2Ev:                  # @_ZN12CBenchBufferD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi173:
	.cfi_def_cfa_offset 16
.Lcfi174:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV12CBenchBuffer+16, (%rbx)
	movq	16(%rbx), %rdi
	callq	MidFree
	movq	$0, 16(%rbx)
	popq	%rbx
	retq
.Lfunc_end41:
	.size	_ZN12CBenchBufferD2Ev, .Lfunc_end41-_ZN12CBenchBufferD2Ev
	.cfi_endproc

	.section	.text._ZN21CBenchRandomGeneratorD0Ev,"axG",@progbits,_ZN21CBenchRandomGeneratorD0Ev,comdat
	.weak	_ZN21CBenchRandomGeneratorD0Ev
	.p2align	4, 0x90
	.type	_ZN21CBenchRandomGeneratorD0Ev,@function
_ZN21CBenchRandomGeneratorD0Ev:         # @_ZN21CBenchRandomGeneratorD0Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi175:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi176:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi177:
	.cfi_def_cfa_offset 32
.Lcfi178:
	.cfi_offset %rbx, -24
.Lcfi179:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV12CBenchBuffer+16, (%rbx)
	movq	16(%rbx), %rdi
.Ltmp232:
	callq	MidFree
.Ltmp233:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB42_2:
.Ltmp234:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end42:
	.size	_ZN21CBenchRandomGeneratorD0Ev, .Lfunc_end42-_ZN21CBenchRandomGeneratorD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table42:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp232-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp233-.Ltmp232       #   Call between .Ltmp232 and .Ltmp233
	.long	.Ltmp234-.Lfunc_begin11 #     jumps to .Ltmp234
	.byte	0                       #   On action: cleanup
	.long	.Ltmp233-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Lfunc_end42-.Ltmp233   #   Call between .Ltmp233 and .Lfunc_end42
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN12CEncoderInfo20EncodeThreadFunctionEPv,"axG",@progbits,_ZN12CEncoderInfo20EncodeThreadFunctionEPv,comdat
	.weak	_ZN12CEncoderInfo20EncodeThreadFunctionEPv
	.p2align	4, 0x90
	.type	_ZN12CEncoderInfo20EncodeThreadFunctionEPv,@function
_ZN12CEncoderInfo20EncodeThreadFunctionEPv: # @_ZN12CEncoderInfo20EncodeThreadFunctionEPv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi180:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi181:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi182:
	.cfi_def_cfa_offset 32
.Lcfi183:
	.cfi_offset %rbx, -24
.Lcfi184:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	callq	_ZN12CEncoderInfo6EncodeEv
	movl	%eax, %ebp
	movl	%ebp, 128(%rbx)
	testl	%ebp, %ebp
	je	.LBB43_2
# BB#1:
	movq	40(%rbx), %rax
	movq	16(%rax), %rbx
	movq	%rbx, %rdi
	callq	pthread_mutex_lock
	movl	%ebp, 40(%rbx)
	movq	%rbx, %rdi
	callq	pthread_mutex_unlock
.LBB43_2:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end43:
	.size	_ZN12CEncoderInfo20EncodeThreadFunctionEPv, .Lfunc_end43-_ZN12CEncoderInfo20EncodeThreadFunctionEPv
	.cfi_endproc

	.section	.text._ZN12CEncoderInfo20DecodeThreadFunctionEPv,"axG",@progbits,_ZN12CEncoderInfo20DecodeThreadFunctionEPv,comdat
	.weak	_ZN12CEncoderInfo20DecodeThreadFunctionEPv
	.p2align	4, 0x90
	.type	_ZN12CEncoderInfo20DecodeThreadFunctionEPv,@function
_ZN12CEncoderInfo20DecodeThreadFunctionEPv: # @_ZN12CEncoderInfo20DecodeThreadFunctionEPv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi185:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi186:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi187:
	.cfi_def_cfa_offset 32
.Lcfi188:
	.cfi_offset %rbx, -24
.Lcfi189:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	(%rbx), %r14
	movl	8(%rbx), %esi
	movq	%r14, %rdi
	callq	_ZN12CEncoderInfo6DecodeEj
	movl	8(%rbx), %ecx
	movl	%eax, 128(%r14,%rcx,4)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end44:
	.size	_ZN12CEncoderInfo20DecodeThreadFunctionEPv, .Lfunc_end44-_ZN12CEncoderInfo20DecodeThreadFunctionEPv
	.cfi_endproc

	.type	_ZTV18CBenchmarkInStream,@object # @_ZTV18CBenchmarkInStream
	.section	.rodata,"a",@progbits
	.globl	_ZTV18CBenchmarkInStream
	.p2align	3
_ZTV18CBenchmarkInStream:
	.quad	0
	.quad	_ZTI18CBenchmarkInStream
	.quad	_ZN18CBenchmarkInStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN18CBenchmarkInStream6AddRefEv
	.quad	_ZN18CBenchmarkInStream7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN18CBenchmarkInStreamD0Ev
	.quad	_ZN18CBenchmarkInStream4ReadEPvjPj
	.size	_ZTV18CBenchmarkInStream, 64

	.type	_ZTS18CBenchmarkInStream,@object # @_ZTS18CBenchmarkInStream
	.globl	_ZTS18CBenchmarkInStream
	.p2align	4
_ZTS18CBenchmarkInStream:
	.asciz	"18CBenchmarkInStream"
	.size	_ZTS18CBenchmarkInStream, 21

	.type	_ZTS19ISequentialInStream,@object # @_ZTS19ISequentialInStream
	.section	.rodata._ZTS19ISequentialInStream,"aG",@progbits,_ZTS19ISequentialInStream,comdat
	.weak	_ZTS19ISequentialInStream
	.p2align	4
_ZTS19ISequentialInStream:
	.asciz	"19ISequentialInStream"
	.size	_ZTS19ISequentialInStream, 22

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI19ISequentialInStream,@object # @_ZTI19ISequentialInStream
	.section	.rodata._ZTI19ISequentialInStream,"aG",@progbits,_ZTI19ISequentialInStream,comdat
	.weak	_ZTI19ISequentialInStream
	.p2align	4
_ZTI19ISequentialInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19ISequentialInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI19ISequentialInStream, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI18CBenchmarkInStream,@object # @_ZTI18CBenchmarkInStream
	.section	.rodata,"a",@progbits
	.globl	_ZTI18CBenchmarkInStream
	.p2align	4
_ZTI18CBenchmarkInStream:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS18CBenchmarkInStream
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI19ISequentialInStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI18CBenchmarkInStream, 56

	.type	_ZTV19CBenchmarkOutStream,@object # @_ZTV19CBenchmarkOutStream
	.globl	_ZTV19CBenchmarkOutStream
	.p2align	3
_ZTV19CBenchmarkOutStream:
	.quad	0
	.quad	_ZTI19CBenchmarkOutStream
	.quad	_ZN19CBenchmarkOutStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN19CBenchmarkOutStream6AddRefEv
	.quad	_ZN19CBenchmarkOutStream7ReleaseEv
	.quad	_ZN19CBenchmarkOutStreamD2Ev
	.quad	_ZN19CBenchmarkOutStreamD0Ev
	.quad	_ZN19CBenchmarkOutStream5WriteEPKvjPj
	.quad	-8
	.quad	_ZTI19CBenchmarkOutStream
	.quad	_ZThn8_N19CBenchmarkOutStreamD1Ev
	.quad	_ZThn8_N19CBenchmarkOutStreamD0Ev
	.size	_ZTV19CBenchmarkOutStream, 96

	.type	_ZTS19CBenchmarkOutStream,@object # @_ZTS19CBenchmarkOutStream
	.globl	_ZTS19CBenchmarkOutStream
	.p2align	4
_ZTS19CBenchmarkOutStream:
	.asciz	"19CBenchmarkOutStream"
	.size	_ZTS19CBenchmarkOutStream, 22

	.type	_ZTS20ISequentialOutStream,@object # @_ZTS20ISequentialOutStream
	.section	.rodata._ZTS20ISequentialOutStream,"aG",@progbits,_ZTS20ISequentialOutStream,comdat
	.weak	_ZTS20ISequentialOutStream
	.p2align	4
_ZTS20ISequentialOutStream:
	.asciz	"20ISequentialOutStream"
	.size	_ZTS20ISequentialOutStream, 23

	.type	_ZTI20ISequentialOutStream,@object # @_ZTI20ISequentialOutStream
	.section	.rodata._ZTI20ISequentialOutStream,"aG",@progbits,_ZTI20ISequentialOutStream,comdat
	.weak	_ZTI20ISequentialOutStream
	.p2align	4
_ZTI20ISequentialOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ISequentialOutStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ISequentialOutStream, 24

	.type	_ZTS12CBenchBuffer,@object # @_ZTS12CBenchBuffer
	.section	.rodata._ZTS12CBenchBuffer,"aG",@progbits,_ZTS12CBenchBuffer,comdat
	.weak	_ZTS12CBenchBuffer
_ZTS12CBenchBuffer:
	.asciz	"12CBenchBuffer"
	.size	_ZTS12CBenchBuffer, 15

	.type	_ZTI12CBenchBuffer,@object # @_ZTI12CBenchBuffer
	.section	.rodata._ZTI12CBenchBuffer,"aG",@progbits,_ZTI12CBenchBuffer,comdat
	.weak	_ZTI12CBenchBuffer
	.p2align	3
_ZTI12CBenchBuffer:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS12CBenchBuffer
	.size	_ZTI12CBenchBuffer, 16

	.type	_ZTI19CBenchmarkOutStream,@object # @_ZTI19CBenchmarkOutStream
	.section	.rodata,"a",@progbits
	.globl	_ZTI19CBenchmarkOutStream
	.p2align	4
_ZTI19CBenchmarkOutStream:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS19CBenchmarkOutStream
	.long	0                       # 0x0
	.long	3                       # 0x3
	.quad	_ZTI20ISequentialOutStream
	.quad	2                       # 0x2
	.quad	_ZTI12CBenchBuffer
	.quad	2050                    # 0x802
	.quad	_ZTI13CMyUnknownImp
	.quad	8194                    # 0x2002
	.size	_ZTI19CBenchmarkOutStream, 72

	.type	_ZTV13CCrcOutStream,@object # @_ZTV13CCrcOutStream
	.globl	_ZTV13CCrcOutStream
	.p2align	3
_ZTV13CCrcOutStream:
	.quad	0
	.quad	_ZTI13CCrcOutStream
	.quad	_ZN13CCrcOutStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN13CCrcOutStream6AddRefEv
	.quad	_ZN13CCrcOutStream7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN13CCrcOutStreamD0Ev
	.quad	_ZN13CCrcOutStream5WriteEPKvjPj
	.size	_ZTV13CCrcOutStream, 64

	.type	_ZTS13CCrcOutStream,@object # @_ZTS13CCrcOutStream
	.globl	_ZTS13CCrcOutStream
_ZTS13CCrcOutStream:
	.asciz	"13CCrcOutStream"
	.size	_ZTS13CCrcOutStream, 16

	.type	_ZTI13CCrcOutStream,@object # @_ZTI13CCrcOutStream
	.globl	_ZTI13CCrcOutStream
	.p2align	4
_ZTI13CCrcOutStream:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS13CCrcOutStream
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI20ISequentialOutStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI13CCrcOutStream, 56

	.type	_ZTV18CBenchProgressInfo,@object # @_ZTV18CBenchProgressInfo
	.globl	_ZTV18CBenchProgressInfo
	.p2align	3
_ZTV18CBenchProgressInfo:
	.quad	0
	.quad	_ZTI18CBenchProgressInfo
	.quad	_ZN18CBenchProgressInfo14QueryInterfaceERK4GUIDPPv
	.quad	_ZN18CBenchProgressInfo6AddRefEv
	.quad	_ZN18CBenchProgressInfo7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN18CBenchProgressInfoD0Ev
	.quad	_ZN18CBenchProgressInfo12SetRatioInfoEPKyS1_
	.size	_ZTV18CBenchProgressInfo, 64

	.type	_ZTS18CBenchProgressInfo,@object # @_ZTS18CBenchProgressInfo
	.globl	_ZTS18CBenchProgressInfo
	.p2align	4
_ZTS18CBenchProgressInfo:
	.asciz	"18CBenchProgressInfo"
	.size	_ZTS18CBenchProgressInfo, 21

	.type	_ZTS21ICompressProgressInfo,@object # @_ZTS21ICompressProgressInfo
	.section	.rodata._ZTS21ICompressProgressInfo,"aG",@progbits,_ZTS21ICompressProgressInfo,comdat
	.weak	_ZTS21ICompressProgressInfo
	.p2align	4
_ZTS21ICompressProgressInfo:
	.asciz	"21ICompressProgressInfo"
	.size	_ZTS21ICompressProgressInfo, 24

	.type	_ZTI21ICompressProgressInfo,@object # @_ZTI21ICompressProgressInfo
	.section	.rodata._ZTI21ICompressProgressInfo,"aG",@progbits,_ZTI21ICompressProgressInfo,comdat
	.weak	_ZTI21ICompressProgressInfo
	.p2align	4
_ZTI21ICompressProgressInfo:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS21ICompressProgressInfo
	.quad	_ZTI8IUnknown
	.size	_ZTI21ICompressProgressInfo, 24

	.type	_ZTI18CBenchProgressInfo,@object # @_ZTI18CBenchProgressInfo
	.section	.rodata,"a",@progbits
	.globl	_ZTI18CBenchProgressInfo
	.p2align	4
_ZTI18CBenchProgressInfo:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS18CBenchProgressInfo
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI21ICompressProgressInfo
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI18CBenchProgressInfo, 56

	.type	_ZTV12CBenchBuffer,@object # @_ZTV12CBenchBuffer
	.section	.rodata._ZTV12CBenchBuffer,"aG",@progbits,_ZTV12CBenchBuffer,comdat
	.weak	_ZTV12CBenchBuffer
	.p2align	3
_ZTV12CBenchBuffer:
	.quad	0
	.quad	_ZTI12CBenchBuffer
	.quad	_ZN12CBenchBufferD2Ev
	.quad	_ZN12CBenchBufferD0Ev
	.size	_ZTV12CBenchBuffer, 32

	.type	_ZTV21CBenchRandomGenerator,@object # @_ZTV21CBenchRandomGenerator
	.section	.rodata._ZTV21CBenchRandomGenerator,"aG",@progbits,_ZTV21CBenchRandomGenerator,comdat
	.weak	_ZTV21CBenchRandomGenerator
	.p2align	3
_ZTV21CBenchRandomGenerator:
	.quad	0
	.quad	_ZTI21CBenchRandomGenerator
	.quad	_ZN12CBenchBufferD2Ev
	.quad	_ZN21CBenchRandomGeneratorD0Ev
	.size	_ZTV21CBenchRandomGenerator, 32

	.type	_ZTS21CBenchRandomGenerator,@object # @_ZTS21CBenchRandomGenerator
	.section	.rodata._ZTS21CBenchRandomGenerator,"aG",@progbits,_ZTS21CBenchRandomGenerator,comdat
	.weak	_ZTS21CBenchRandomGenerator
	.p2align	4
_ZTS21CBenchRandomGenerator:
	.asciz	"21CBenchRandomGenerator"
	.size	_ZTS21CBenchRandomGenerator, 24

	.type	_ZTI21CBenchRandomGenerator,@object # @_ZTI21CBenchRandomGenerator
	.section	.rodata._ZTI21CBenchRandomGenerator,"aG",@progbits,_ZTI21CBenchRandomGenerator,comdat
	.weak	_ZTI21CBenchRandomGenerator
	.p2align	4
_ZTI21CBenchRandomGenerator:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS21CBenchRandomGenerator
	.quad	_ZTI12CBenchBuffer
	.size	_ZTI21CBenchRandomGenerator, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
