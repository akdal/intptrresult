	.text
	.file	"struct_vector.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	hypre_StructVectorCreate
	.p2align	4, 0x90
	.type	hypre_StructVectorCreate,@function
hypre_StructVectorCreate:               # @hypre_StructVectorCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %ebp
	movl	$1, %edi
	movl	$80, %esi
	callq	hypre_CAlloc
	movq	%rax, %rbx
	movl	%ebp, (%rbx)
	leaq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	hypre_StructGridRef
	movl	$1, 32(%rbx)
	movl	$1, 76(%rbx)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [1,1,1,1]
	movups	%xmm0, 48(%rbx)
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 64(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_StructVectorCreate, .Lfunc_end0-hypre_StructVectorCreate
	.cfi_endproc

	.globl	hypre_StructVectorRef
	.p2align	4, 0x90
	.type	hypre_StructVectorRef,@function
hypre_StructVectorRef:                  # @hypre_StructVectorRef
	.cfi_startproc
# BB#0:
	incl	76(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end1:
	.size	hypre_StructVectorRef, .Lfunc_end1-hypre_StructVectorRef
	.cfi_endproc

	.globl	hypre_StructVectorDestroy
	.p2align	4, 0x90
	.type	hypre_StructVectorDestroy,@function
hypre_StructVectorDestroy:              # @hypre_StructVectorDestroy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB2_5
# BB#1:
	decl	76(%rbx)
	jne	.LBB2_5
# BB#2:
	cmpl	$0, 32(%rbx)
	je	.LBB2_4
# BB#3:
	movq	24(%rbx), %rdi
	callq	hypre_Free
	movq	$0, 24(%rbx)
.LBB2_4:
	movq	40(%rbx), %rdi
	callq	hypre_Free
	movq	$0, 40(%rbx)
	movq	16(%rbx), %rdi
	callq	hypre_BoxArrayDestroy
	movq	8(%rbx), %rdi
	callq	hypre_StructGridDestroy
	movq	%rbx, %rdi
	callq	hypre_Free
.LBB2_5:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end2:
	.size	hypre_StructVectorDestroy, .Lfunc_end2-hypre_StructVectorDestroy
	.cfi_endproc

	.globl	hypre_StructVectorInitializeShell
	.p2align	4, 0x90
	.type	hypre_StructVectorInitializeShell,@function
hypre_StructVectorInitializeShell:      # @hypre_StructVectorInitializeShell
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 48
.Lcfi13:
	.cfi_offset %rbx, -48
.Lcfi14:
	.cfi_offset %r12, -40
.Lcfi15:
	.cfi_offset %r13, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rdi, %r13
	movq	8(%r13), %r15
	movq	16(%r13), %r14
	testq	%r14, %r14
	jne	.LBB3_5
# BB#1:
	movq	8(%r15), %r12
	movl	8(%r12), %edi
	callq	hypre_BoxArrayCreate
	movq	%rax, %r14
	cmpl	$0, 8(%r12)
	jle	.LBB3_4
# BB#2:                                 # %.lr.ph95
	movq	(%r12), %rax
	movq	(%r14), %rcx
	addq	$20, %rax
	addq	$20, %rcx
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movl	-20(%rax), %esi
	movl	%esi, -20(%rcx)
	movl	-16(%rax), %edi
	movl	%edi, -16(%rcx)
	movl	-12(%rax), %r9d
	movl	%r9d, -12(%rcx)
	movl	-8(%rax), %edx
	movl	%edx, -8(%rcx)
	movl	-4(%rax), %ebx
	movl	%ebx, -4(%rcx)
	movl	(%rax), %r10d
	movl	%r10d, (%rcx)
	subl	48(%r13), %esi
	movl	%esi, -20(%rcx)
	addl	52(%r13), %edx
	movl	%edx, -8(%rcx)
	subl	56(%r13), %edi
	movl	%edi, -16(%rcx)
	addl	60(%r13), %ebx
	movl	%ebx, -4(%rcx)
	subl	64(%r13), %r9d
	movl	%r9d, -12(%rcx)
	addl	68(%r13), %r10d
	movl	%r10d, (%rcx)
	incq	%r8
	movslq	8(%r12), %rdx
	addq	$24, %rax
	addq	$24, %rcx
	cmpq	%rdx, %r8
	jl	.LBB3_3
.LBB3_4:                                # %._crit_edge96
	movq	%r14, 16(%r13)
.LBB3_5:
	cmpq	$0, 40(%r13)
	jne	.LBB3_11
# BB#6:
	movl	8(%r14), %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	cmpl	$0, 8(%r14)
	jle	.LBB3_7
# BB#8:                                 # %.lr.ph
	movq	(%r14), %rcx
	addq	$20, %rcx
	xorl	%r8d, %r8d
	xorl	%edi, %edi
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB3_9:                                # =>This Inner Loop Header: Depth=1
	movl	%r9d, (%rax,%rdi,4)
	movl	(%rcx), %edx
	movl	-12(%rcx), %esi
	movl	%edx, %ebx
	subl	%esi, %ebx
	incl	%ebx
	cmpl	%esi, %edx
	movl	-8(%rcx), %r10d
	movl	-4(%rcx), %r11d
	movl	-16(%rcx), %edx
	cmovsl	%r8d, %ebx
	movl	%r11d, %esi
	subl	%edx, %esi
	incl	%esi
	cmpl	%edx, %r11d
	movl	-20(%rcx), %r11d
	cmovsl	%r8d, %esi
	movl	%r10d, %edx
	subl	%r11d, %edx
	incl	%edx
	cmpl	%r11d, %r10d
	cmovsl	%r8d, %edx
	imull	%esi, %edx
	imull	%ebx, %edx
	addl	%edx, %r9d
	incq	%rdi
	movslq	8(%r14), %rdx
	addq	$24, %rcx
	cmpq	%rdx, %rdi
	jl	.LBB3_9
	jmp	.LBB3_10
.LBB3_7:
	xorl	%r9d, %r9d
.LBB3_10:                               # %._crit_edge
	movq	%rax, 40(%r13)
	movl	%r9d, 36(%r13)
.LBB3_11:
	movl	52(%r15), %eax
	movl	%eax, 72(%r13)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	hypre_StructVectorInitializeShell, .Lfunc_end3-hypre_StructVectorInitializeShell
	.cfi_endproc

	.globl	hypre_StructVectorInitializeData
	.p2align	4, 0x90
	.type	hypre_StructVectorInitializeData,@function
hypre_StructVectorInitializeData:       # @hypre_StructVectorInitializeData
	.cfi_startproc
# BB#0:
	movq	%rsi, 24(%rdi)
	movl	$0, 32(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end4:
	.size	hypre_StructVectorInitializeData, .Lfunc_end4-hypre_StructVectorInitializeData
	.cfi_endproc

	.globl	hypre_StructVectorInitialize
	.p2align	4, 0x90
	.type	hypre_StructVectorInitialize,@function
hypre_StructVectorInitialize:           # @hypre_StructVectorInitialize
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
.Lcfi19:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	hypre_StructVectorInitializeShell
	movl	36(%rbx), %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rax, 24(%rbx)
	movl	$1, 32(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	hypre_StructVectorInitialize, .Lfunc_end5-hypre_StructVectorInitialize
	.cfi_endproc

	.globl	hypre_StructVectorSetValues
	.p2align	4, 0x90
	.type	hypre_StructVectorSetValues,@function
hypre_StructVectorSetValues:            # @hypre_StructVectorSetValues
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	8(%rdi), %rax
	movq	8(%rax), %rax
	movslq	8(%rax), %r8
	testq	%r8, %r8
	jle	.LBB6_20
# BB#1:                                 # %.lr.ph
	movq	(%rax), %rcx
	movl	(%rsi), %r10d
	testl	%edx, %edx
	je	.LBB6_11
# BB#2:                                 # %.lr.ph.split.preheader
	movl	$4, %edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_3:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	cmpl	-16(%rcx,%rdx,4), %r10d
	jl	.LBB6_10
# BB#4:                                 #   in Loop: Header=BB6_3 Depth=1
	cmpl	-4(%rcx,%rdx,4), %r10d
	jg	.LBB6_10
# BB#5:                                 #   in Loop: Header=BB6_3 Depth=1
	movl	4(%rsi), %r11d
	cmpl	-12(%rcx,%rdx,4), %r11d
	jl	.LBB6_10
# BB#6:                                 #   in Loop: Header=BB6_3 Depth=1
	cmpl	(%rcx,%rdx,4), %r11d
	jg	.LBB6_10
# BB#7:                                 #   in Loop: Header=BB6_3 Depth=1
	movl	8(%rsi), %ebx
	cmpl	-8(%rcx,%rdx,4), %ebx
	jl	.LBB6_10
# BB#8:                                 #   in Loop: Header=BB6_3 Depth=1
	cmpl	4(%rcx,%rdx,4), %ebx
	jg	.LBB6_10
# BB#9:                                 #   in Loop: Header=BB6_3 Depth=1
	movq	16(%rdi), %r9
	movq	40(%rdi), %rbp
	movq	%rbp, -16(%rsp)         # 8-byte Spill
	movq	(%r9), %r13
	movl	-16(%r13,%rdx,4), %ebp
	movl	%ebp, -20(%rsp)         # 4-byte Spill
	movl	-4(%r13,%rdx,4), %r9d
	movl	%r9d, %r12d
	subl	%ebp, %r12d
	incl	%r12d
	cmpl	%ebp, %r9d
	movl	-12(%r13,%rdx,4), %r15d
	movl	(%r13,%rdx,4), %r14d
	movl	$0, %ebp
	cmovsl	%ebp, %r12d
	movl	%r14d, %r9d
	subl	%r15d, %r9d
	incl	%r9d
	cmpl	%r15d, %r14d
	movq	-16(%rsp), %rbp         # 8-byte Reload
	movslq	(%rbp,%rax,4), %r14
	movl	$0, %ebp
	cmovsl	%ebp, %r9d
	shlq	$3, %r14
	addq	24(%rdi), %r14
	movq	%rdi, -16(%rsp)         # 8-byte Spill
	movl	%r10d, %edi
	subl	-20(%rsp), %edi         # 4-byte Folded Reload
	subl	%r15d, %r11d
	subl	-8(%r13,%rdx,4), %ebx
	imull	%r9d, %ebx
	addl	%r11d, %ebx
	imull	%r12d, %ebx
	addl	%edi, %ebx
	movq	-16(%rsp), %rdi         # 8-byte Reload
	movslq	%ebx, %rbp
	movsd	(%r14,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%r14,%rbp,8)
	.p2align	4, 0x90
.LBB6_10:                               #   in Loop: Header=BB6_3 Depth=1
	incq	%rax
	addq	$6, %rdx
	cmpq	%r8, %rax
	jl	.LBB6_3
	jmp	.LBB6_20
.LBB6_11:                               # %.lr.ph.split.us.preheader
	xorl	%r12d, %r12d
	movl	$4, %eax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB6_12:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	cmpl	-16(%rcx,%rax,4), %r10d
	jl	.LBB6_19
# BB#13:                                #   in Loop: Header=BB6_12 Depth=1
	cmpl	-4(%rcx,%rax,4), %r10d
	jg	.LBB6_19
# BB#14:                                #   in Loop: Header=BB6_12 Depth=1
	movl	4(%rsi), %r11d
	cmpl	-12(%rcx,%rax,4), %r11d
	jl	.LBB6_19
# BB#15:                                #   in Loop: Header=BB6_12 Depth=1
	cmpl	(%rcx,%rax,4), %r11d
	jg	.LBB6_19
# BB#16:                                #   in Loop: Header=BB6_12 Depth=1
	movl	8(%rsi), %ebx
	cmpl	-8(%rcx,%rax,4), %ebx
	jl	.LBB6_19
# BB#17:                                #   in Loop: Header=BB6_12 Depth=1
	cmpl	4(%rcx,%rax,4), %ebx
	jg	.LBB6_19
# BB#18:                                #   in Loop: Header=BB6_12 Depth=1
	movq	16(%rdi), %r9
	movq	40(%rdi), %rbp
	movq	%rbp, -16(%rsp)         # 8-byte Spill
	movq	(%r9), %r14
	movl	-16(%r14,%rax,4), %ebp
	movl	%ebp, -20(%rsp)         # 4-byte Spill
	movl	-4(%r14,%rax,4), %r9d
	movl	%r9d, %r13d
	subl	%ebp, %r13d
	incl	%r13d
	cmpl	%ebp, %r9d
	movl	-12(%r14,%rax,4), %ebp
	movl	%ebp, -4(%rsp)          # 4-byte Spill
	movl	(%r14,%rax,4), %r15d
	cmovsl	%r12d, %r13d
	movl	%r15d, %r9d
	subl	%ebp, %r9d
	incl	%r9d
	cmpl	%ebp, %r15d
	movq	-16(%rsp), %rbp         # 8-byte Reload
	movslq	(%rbp,%rdx,4), %r15
	cmovsl	%r12d, %r9d
	shlq	$3, %r15
	addq	24(%rdi), %r15
	movl	%r10d, %r12d
	subl	-20(%rsp), %r12d        # 4-byte Folded Reload
	subl	-4(%rsp), %r11d         # 4-byte Folded Reload
	subl	-8(%r14,%rax,4), %ebx
	imull	%r9d, %ebx
	addl	%r11d, %ebx
	imull	%r13d, %ebx
	addl	%r12d, %ebx
	xorl	%r12d, %r12d
	movslq	%ebx, %rbp
	movsd	%xmm0, (%r15,%rbp,8)
	.p2align	4, 0x90
.LBB6_19:                               #   in Loop: Header=BB6_12 Depth=1
	incq	%rdx
	addq	$6, %rax
	cmpq	%r8, %rdx
	jl	.LBB6_12
.LBB6_20:                               # %._crit_edge
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	hypre_StructVectorSetValues, .Lfunc_end6-hypre_StructVectorSetValues
	.cfi_endproc

	.globl	hypre_StructVectorSetBoxValues
	.p2align	4, 0x90
	.type	hypre_StructVectorSetBoxValues,@function
hypre_StructVectorSetBoxValues:         # @hypre_StructVectorSetBoxValues
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi38:
	.cfi_def_cfa_offset 272
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movl	%ecx, 132(%rsp)         # 4-byte Spill
	movq	%rdx, %rbx
	movq	%rsi, %r13
	movq	%rdi, 144(%rsp)         # 8-byte Spill
	movq	8(%rdi), %rax
	movq	8(%rax), %r14
	movl	8(%r14), %edi
	callq	hypre_BoxArrayCreate
	movq	%rax, %r12
	callq	hypre_BoxCreate
	movq	%rax, %r15
	cmpl	$0, 8(%r14)
	jle	.LBB7_4
# BB#1:                                 # %.lr.ph642
	xorl	%ebp, %ebp
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	%r12, %r13
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB7_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rsi
	addq	%rbp, %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r15, %rdx
	callq	hypre_IntersectBoxes
	movl	(%r15), %eax
	movq	(%r13), %rcx
	movl	%eax, (%rcx,%rbp)
	movl	4(%r15), %eax
	movl	%eax, 4(%rcx,%rbp)
	movl	8(%r15), %eax
	movl	%eax, 8(%rcx,%rbp)
	movl	12(%r15), %eax
	movl	%eax, 12(%rcx,%rbp)
	movl	16(%r15), %eax
	movl	%eax, 16(%rcx,%rbp)
	movl	20(%r15), %eax
	movl	%eax, 20(%rcx,%rbp)
	incq	%r12
	movslq	8(%r14), %rax
	addq	$24, %rbp
	cmpq	%rax, %r12
	jl	.LBB7_2
# BB#3:                                 # %._crit_edge643.thread
	movq	%r15, %rdi
	callq	hypre_BoxDestroy
	movq	%r13, %r12
	movq	16(%rsp), %r13          # 8-byte Reload
	jmp	.LBB7_5
.LBB7_4:                                # %._crit_edge643
	movq	%r15, %rdi
	callq	hypre_BoxDestroy
	testq	%r12, %r12
	je	.LBB7_28
.LBB7_5:
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	16(%rax), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movq	%r13, %rdi
	callq	hypre_BoxDuplicate
	movq	%rax, 136(%rsp)         # 8-byte Spill
	cmpl	$0, 8(%r12)
	jle	.LBB7_27
# BB#6:                                 # %.lr.ph638
	leaq	8(%rbx), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	48(%rbx), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	leaq	24(%rbx), %rax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rbx, 168(%rsp)         # 8-byte Spill
	movq	%r12, 200(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB7_7:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_13 Depth 2
                                        #       Child Loop BB7_15 Depth 3
                                        #         Child Loop BB7_37 Depth 4
                                        #         Child Loop BB7_22 Depth 4
                                        #     Child Loop BB7_44 Depth 2
                                        #       Child Loop BB7_46 Depth 3
                                        #         Child Loop BB7_65 Depth 4
                                        #         Child Loop BB7_50 Depth 4
                                        #         Child Loop BB7_53 Depth 4
	movq	(%r12), %r13
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rcx,2), %rbp
	shlq	$3, %rbp
	addq	%r13, %rbp
	je	.LBB7_26
# BB#8:                                 #   in Loop: Header=BB7_7 Depth=1
	movq	192(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %r15
	movl	(%rbp), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	152(%rsp), %rcx         # 8-byte Reload
	leaq	(,%rcx,8), %rax
	leaq	(%rax,%rax,2), %r12
	movl	4(%r13,%r12), %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movl	8(%r13,%r12), %r14d
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	24(%rax), %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	40(%rax), %rax
	movslq	(%rax,%rcx,4), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	leaq	180(%rsp), %rsi
	callq	hypre_BoxGetSize
	movq	136(%rsp), %r9          # 8-byte Reload
	movl	(%r9), %esi
	movl	%esi, 80(%rsp)          # 4-byte Spill
	movl	4(%r9), %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movl	12(%r9), %eax
	movl	%eax, %edx
	subl	%esi, %edx
	incl	%edx
	cmpl	%esi, %eax
	movl	16(%r9), %eax
	movl	$0, %esi
	cmovsl	%esi, %edx
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movl	%eax, %esi
	subl	%ecx, %esi
	incl	%esi
	cmpl	%ecx, %eax
	movq	%r15, %r10
	movl	(%r10,%r12), %r11d
	movl	4(%r10,%r12), %r8d
	movl	12(%r10,%r12), %ecx
	movl	$0, %r15d
	cmovsl	%r15d, %esi
	movl	%ecx, %eax
	subl	%r11d, %eax
	incl	%eax
	cmpl	%r11d, %ecx
	movl	16(%r10,%r12), %edi
	cmovsl	%r15d, %eax
	movl	%edi, %ecx
	subl	%r8d, %ecx
	incl	%ecx
	cmpl	%r8d, %edi
	movl	8(%r13,%r12), %r15d
	movl	$0, %edi
	cmovsl	%edi, %ecx
	subl	8(%r10,%r12), %r15d
	movl	4(%r13,%r12), %edi
	movl	(%rbp), %r12d
	subl	%r11d, %r12d
	movq	64(%rsp), %r11          # 8-byte Reload
	subl	%r8d, %edi
	imull	%ecx, %r15d
	addl	%edi, %r15d
	imull	%eax, %r15d
	movl	%r15d, 8(%rsp)          # 4-byte Spill
	movq	72(%rsp), %r15          # 8-byte Reload
	movl	16(%rsp), %edi          # 4-byte Reload
	subl	80(%rsp), %edi          # 4-byte Folded Reload
	movl	%edi, %r10d
	movl	40(%rsp), %edi          # 4-byte Reload
	subl	4(%rsp), %edi           # 4-byte Folded Reload
	subl	8(%r9), %r14d
	imull	%esi, %r14d
	addl	%edi, %r14d
	imull	%edx, %r14d
	movl	%r14d, 4(%rsp)          # 4-byte Spill
	movl	%eax, 16(%rsp)          # 4-byte Spill
	imull	%eax, %ecx
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	imull	%edx, %esi
	movl	%esi, 32(%rsp)          # 4-byte Spill
	movl	180(%rsp), %r8d
	movl	184(%rsp), %eax
	movl	188(%rsp), %edx
	cmpl	%r8d, %eax
	movl	%r8d, %ecx
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmovgel	%eax, %ecx
	cmpl	%ecx, %edx
	movl	%edx, 36(%rsp)          # 4-byte Spill
	cmovgel	%edx, %ecx
	cmpl	$0, 132(%rsp)           # 4-byte Folded Reload
	leaq	(%r11,%r15,8), %rbp
	je	.LBB7_40
# BB#9:                                 #   in Loop: Header=BB7_7 Depth=1
	testl	%ecx, %ecx
	jle	.LBB7_26
# BB#10:                                # %.lr.ph594
                                        #   in Loop: Header=BB7_7 Depth=1
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	jle	.LBB7_26
# BB#11:                                # %.lr.ph594
                                        #   in Loop: Header=BB7_7 Depth=1
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jle	.LBB7_26
# BB#12:                                # %.preheader559.us.preheader
                                        #   in Loop: Header=BB7_7 Depth=1
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	%edx, %esi
	subl	%r8d, %esi
	movl	12(%rsp), %r9d          # 4-byte Reload
	movl	%r9d, %edi
	subl	%r8d, %edi
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	imull	%edx, %ecx
	movl	%ecx, 52(%rsp)          # 4-byte Spill
	subl	%ecx, 28(%rsp)          # 4-byte Folded Spill
	movl	%r9d, %ecx
	imull	%eax, %ecx
	movl	%ecx, 48(%rsp)          # 4-byte Spill
	subl	%ecx, 32(%rsp)          # 4-byte Folded Spill
	leal	-1(%rax), %eax
	imull	%eax, %esi
	addl	%edx, %esi
	subl	%r8d, %esi
	movl	%esi, 60(%rsp)          # 4-byte Spill
	imull	%eax, %edi
	addl	%r9d, %edi
	subl	%r8d, %edi
	movl	%edi, 56(%rsp)          # 4-byte Spill
	leal	-1(%r8), %ecx
	addl	%r10d, 4(%rsp)          # 4-byte Folded Spill
	addl	%r12d, 8(%rsp)          # 4-byte Folded Spill
	leaq	(%r15,%rcx), %rax
	leaq	8(%r11,%rax,8), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	leaq	1(%rcx), %r10
	movq	%r10, %r12
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	andq	%rax, %r12
	leaq	-4(%r12), %rax
	shrq	$2, %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	8(%r11,%r15,8), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB7_13:                               # %.preheader559.us
                                        #   Parent Loop BB7_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_15 Depth 3
                                        #         Child Loop BB7_37 Depth 4
                                        #         Child Loop BB7_22 Depth 4
	testl	%r8d, %r8d
	movl	56(%rsp), %eax          # 4-byte Reload
	movl	60(%rsp), %ecx          # 4-byte Reload
	jle	.LBB7_25
# BB#14:                                # %.preheader557.us.us.preheader
                                        #   in Loop: Header=BB7_13 Depth=2
	movl	%edx, 64(%rsp)          # 4-byte Spill
	xorl	%r11d, %r11d
	movl	4(%rsp), %r13d          # 4-byte Reload
	movl	8(%rsp), %eax           # 4-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	movl	12(%rsp), %r14d         # 4-byte Reload
	jmp	.LBB7_15
.LBB7_32:                               # %vector.body713.preheader
                                        #   in Loop: Header=BB7_15 Depth=3
	cmpq	$0, 96(%rsp)            # 8-byte Folded Reload
	jne	.LBB7_33
# BB#34:                                # %vector.body713.prol
                                        #   in Loop: Header=BB7_15 Depth=3
	movupd	(%rbx,%r15,8), %xmm0
	movupd	16(%rbx,%r15,8), %xmm1
	movupd	(%rbp,%rdx,8), %xmm2
	movupd	16(%rbp,%rdx,8), %xmm3
	addpd	%xmm0, %xmm2
	addpd	%xmm1, %xmm3
	movupd	%xmm2, (%rbp,%rdx,8)
	movupd	%xmm3, 16(%rbp,%rdx,8)
	movl	$4, %r9d
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	jne	.LBB7_36
	jmp	.LBB7_38
.LBB7_33:                               #   in Loop: Header=BB7_15 Depth=3
	xorl	%r9d, %r9d
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	je	.LBB7_38
.LBB7_36:                               # %vector.body713.preheader.new
                                        #   in Loop: Header=BB7_15 Depth=3
	leaq	(%rbp,%rdx,8), %rsi
	movq	160(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r15,8), %r14
	.p2align	4, 0x90
.LBB7_37:                               # %vector.body713
                                        #   Parent Loop BB7_7 Depth=1
                                        #     Parent Loop BB7_13 Depth=2
                                        #       Parent Loop BB7_15 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movupd	-48(%r14,%r9,8), %xmm0
	movupd	-32(%r14,%r9,8), %xmm1
	movupd	(%rsi,%r9,8), %xmm2
	movupd	16(%rsi,%r9,8), %xmm3
	addpd	%xmm0, %xmm2
	addpd	%xmm1, %xmm3
	movupd	%xmm2, (%rsi,%r9,8)
	movupd	%xmm3, 16(%rsi,%r9,8)
	movupd	-16(%r14,%r9,8), %xmm0
	movupd	(%r14,%r9,8), %xmm1
	movupd	32(%rsi,%r9,8), %xmm2
	movupd	48(%rsi,%r9,8), %xmm3
	addpd	%xmm0, %xmm2
	addpd	%xmm1, %xmm3
	movupd	%xmm2, 32(%rsi,%r9,8)
	movupd	%xmm3, 48(%rsi,%r9,8)
	addq	$8, %r9
	cmpq	%r9, %r12
	jne	.LBB7_37
.LBB7_38:                               # %middle.block714
                                        #   in Loop: Header=BB7_15 Depth=3
	cmpq	%r12, %r10
	movq	88(%rsp), %r9           # 8-byte Reload
	movl	12(%rsp), %r14d         # 4-byte Reload
	je	.LBB7_23
# BB#39:                                #   in Loop: Header=BB7_15 Depth=3
	addq	%r12, %r15
	addq	%r12, %rdx
	movl	%r12d, %esi
	jmp	.LBB7_17
	.p2align	4, 0x90
.LBB7_15:                               # %.preheader557.us.us
                                        #   Parent Loop BB7_7 Depth=1
                                        #     Parent Loop BB7_13 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB7_37 Depth 4
                                        #         Child Loop BB7_22 Depth 4
	movslq	%eax, %rdx
	movslq	%r13d, %r15
	cmpq	$3, %r10
	jbe	.LBB7_16
# BB#29:                                # %min.iters.checked717
                                        #   in Loop: Header=BB7_15 Depth=3
	testq	%r12, %r12
	je	.LBB7_16
# BB#30:                                # %vector.memcheck739
                                        #   in Loop: Header=BB7_15 Depth=3
	movl	16(%rsp), %ecx          # 4-byte Reload
	imull	%r11d, %ecx
	addl	8(%rsp), %ecx           # 4-byte Folded Reload
	movslq	%ecx, %rcx
	leaq	(%rbp,%rcx,8), %rdi
	movl	%r14d, %esi
	imull	%r11d, %esi
	addl	4(%rsp), %esi           # 4-byte Folded Reload
	movslq	%esi, %rsi
	movq	120(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rsi,8), %rbx
	cmpq	%rbx, %rdi
	movq	168(%rsp), %rbx         # 8-byte Reload
	jae	.LBB7_32
# BB#31:                                # %vector.memcheck739
                                        #   in Loop: Header=BB7_15 Depth=3
	movq	112(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rcx,8), %rcx
	leaq	(%rbx,%rsi,8), %rsi
	cmpq	%rcx, %rsi
	jae	.LBB7_32
	.p2align	4, 0x90
.LBB7_16:                               #   in Loop: Header=BB7_15 Depth=3
	xorl	%esi, %esi
.LBB7_17:                               # %scalar.ph715.preheader
                                        #   in Loop: Header=BB7_15 Depth=3
	movl	%r8d, %ecx
	subl	%esi, %ecx
	testb	$1, %cl
	jne	.LBB7_19
# BB#18:                                #   in Loop: Header=BB7_15 Depth=3
	movl	%esi, %edi
	cmpl	%esi, 80(%rsp)          # 4-byte Folded Reload
	jne	.LBB7_21
	jmp	.LBB7_23
	.p2align	4, 0x90
.LBB7_19:                               # %scalar.ph715.prol
                                        #   in Loop: Header=BB7_15 Depth=3
	movsd	(%rbx,%r15,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%rbp,%rdx,8), %xmm0
	movsd	%xmm0, (%rbp,%rdx,8)
	incq	%rdx
	incq	%r15
	leal	1(%rsi), %edi
	cmpl	%esi, 80(%rsp)          # 4-byte Folded Reload
	je	.LBB7_23
.LBB7_21:                               # %scalar.ph715.preheader.new
                                        #   in Loop: Header=BB7_15 Depth=3
	leaq	(%r9,%r15,8), %rsi
	movq	72(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rdx
	movl	%r8d, %ecx
	subl	%edi, %ecx
	.p2align	4, 0x90
.LBB7_22:                               # %scalar.ph715
                                        #   Parent Loop BB7_7 Depth=1
                                        #     Parent Loop BB7_13 Depth=2
                                        #       Parent Loop BB7_15 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	-8(%rsi), %xmm0         # xmm0 = mem[0],zero
	addsd	-8(%rdx), %xmm0
	movsd	%xmm0, -8(%rdx)
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	addsd	(%rdx), %xmm0
	movsd	%xmm0, (%rdx)
	addq	$16, %rsi
	addq	$16, %rdx
	addl	$-2, %ecx
	jne	.LBB7_22
.LBB7_23:                               # %._crit_edge.us.us
                                        #   in Loop: Header=BB7_15 Depth=3
	addl	16(%rsp), %eax          # 4-byte Folded Reload
	addl	%r14d, %r13d
	incl	%r11d
	cmpl	40(%rsp), %r11d         # 4-byte Folded Reload
	jne	.LBB7_15
# BB#24:                                #   in Loop: Header=BB7_13 Depth=2
	movl	48(%rsp), %eax          # 4-byte Reload
	movl	52(%rsp), %ecx          # 4-byte Reload
	movl	64(%rsp), %edx          # 4-byte Reload
.LBB7_25:                               # %._crit_edge568.us
                                        #   in Loop: Header=BB7_13 Depth=2
	addl	8(%rsp), %ecx           # 4-byte Folded Reload
	addl	4(%rsp), %eax           # 4-byte Folded Reload
	addl	28(%rsp), %ecx          # 4-byte Folded Reload
	addl	32(%rsp), %eax          # 4-byte Folded Reload
	incl	%edx
	cmpl	36(%rsp), %edx          # 4-byte Folded Reload
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	%eax, 4(%rsp)           # 4-byte Spill
	jne	.LBB7_13
	jmp	.LBB7_26
	.p2align	4, 0x90
.LBB7_40:                               #   in Loop: Header=BB7_7 Depth=1
	testl	%ecx, %ecx
	jle	.LBB7_26
# BB#41:                                # %.lr.ph631
                                        #   in Loop: Header=BB7_7 Depth=1
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	jle	.LBB7_26
# BB#42:                                # %.lr.ph631
                                        #   in Loop: Header=BB7_7 Depth=1
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jle	.LBB7_26
# BB#43:                                # %.preheader558.us.preheader
                                        #   in Loop: Header=BB7_7 Depth=1
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	%edx, %esi
	subl	%r8d, %esi
	movl	12(%rsp), %r9d          # 4-byte Reload
	movl	%r9d, %edi
	subl	%r8d, %edi
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	imull	%edx, %ecx
	movl	%ecx, 52(%rsp)          # 4-byte Spill
	subl	%ecx, 28(%rsp)          # 4-byte Folded Spill
	movl	%r9d, %ecx
	imull	%eax, %ecx
	movl	%ecx, 48(%rsp)          # 4-byte Spill
	subl	%ecx, 32(%rsp)          # 4-byte Folded Spill
	leal	-1(%rax), %eax
	imull	%eax, %esi
	addl	%edx, %esi
	subl	%r8d, %esi
	movl	%esi, 60(%rsp)          # 4-byte Spill
	imull	%eax, %edi
	addl	%r9d, %edi
	subl	%r8d, %edi
	movl	%edi, 56(%rsp)          # 4-byte Spill
	leal	-1(%r8), %ecx
	addl	%r10d, 4(%rsp)          # 4-byte Folded Spill
	addl	%r12d, 8(%rsp)          # 4-byte Folded Spill
	leaq	(%r15,%rcx), %rax
	leaq	8(%r11,%rax,8), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	leaq	1(%rcx), %r12
	movq	%r12, %r14
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	andq	%rax, %r14
	leaq	-4(%r14), %rax
	shrq	$2, %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	24(%r11,%r15,8), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB7_44:                               # %.preheader558.us
                                        #   Parent Loop BB7_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_46 Depth 3
                                        #         Child Loop BB7_65 Depth 4
                                        #         Child Loop BB7_50 Depth 4
                                        #         Child Loop BB7_53 Depth 4
	testl	%r8d, %r8d
	movl	56(%rsp), %eax          # 4-byte Reload
	movl	60(%rsp), %ecx          # 4-byte Reload
	jle	.LBB7_56
# BB#45:                                # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB7_44 Depth=2
	movl	%edx, 64(%rsp)          # 4-byte Spill
	xorl	%esi, %esi
	movl	4(%rsp), %r15d          # 4-byte Reload
	movl	8(%rsp), %r9d           # 4-byte Reload
	jmp	.LBB7_46
.LBB7_60:                               # %vector.body.preheader
                                        #   in Loop: Header=BB7_46 Depth=3
	cmpq	$0, 96(%rsp)            # 8-byte Folded Reload
	jne	.LBB7_61
# BB#62:                                # %vector.body.prol
                                        #   in Loop: Header=BB7_46 Depth=3
	movupd	(%rbx,%r10,8), %xmm0
	movupd	16(%rbx,%r10,8), %xmm1
	movupd	%xmm0, (%rbp,%rax,8)
	movupd	%xmm1, 16(%rbp,%rax,8)
	movl	$4, %ecx
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	jne	.LBB7_64
	jmp	.LBB7_66
.LBB7_61:                               #   in Loop: Header=BB7_46 Depth=3
	xorl	%ecx, %ecx
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	je	.LBB7_66
.LBB7_64:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB7_46 Depth=3
	leaq	(%rbp,%rax,8), %rdx
	movq	160(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%r10,8), %rdi
	.p2align	4, 0x90
.LBB7_65:                               # %vector.body
                                        #   Parent Loop BB7_7 Depth=1
                                        #     Parent Loop BB7_44 Depth=2
                                        #       Parent Loop BB7_46 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	-48(%rdi,%rcx,8), %xmm0
	movups	-32(%rdi,%rcx,8), %xmm1
	movups	%xmm0, (%rdx,%rcx,8)
	movups	%xmm1, 16(%rdx,%rcx,8)
	movupd	-16(%rdi,%rcx,8), %xmm0
	movupd	(%rdi,%rcx,8), %xmm1
	movupd	%xmm0, 32(%rdx,%rcx,8)
	movupd	%xmm1, 48(%rdx,%rcx,8)
	addq	$8, %rcx
	cmpq	%rcx, %r14
	jne	.LBB7_65
.LBB7_66:                               # %middle.block
                                        #   in Loop: Header=BB7_46 Depth=3
	cmpq	%r14, %r12
	je	.LBB7_54
# BB#67:                                #   in Loop: Header=BB7_46 Depth=3
	addq	%r14, %r10
	addq	%r14, %rax
	movl	%r14d, %r13d
	jmp	.LBB7_48
	.p2align	4, 0x90
.LBB7_46:                               # %.preheader.us.us
                                        #   Parent Loop BB7_7 Depth=1
                                        #     Parent Loop BB7_44 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB7_65 Depth 4
                                        #         Child Loop BB7_50 Depth 4
                                        #         Child Loop BB7_53 Depth 4
	movslq	%r9d, %rax
	movslq	%r15d, %r10
	cmpq	$3, %r12
	jbe	.LBB7_47
# BB#57:                                # %min.iters.checked
                                        #   in Loop: Header=BB7_46 Depth=3
	testq	%r14, %r14
	je	.LBB7_47
# BB#58:                                # %vector.memcheck
                                        #   in Loop: Header=BB7_46 Depth=3
	movl	16(%rsp), %ecx          # 4-byte Reload
	imull	%esi, %ecx
	addl	8(%rsp), %ecx           # 4-byte Folded Reload
	movslq	%ecx, %rcx
	leaq	(%rbp,%rcx,8), %rdi
	movl	12(%rsp), %edx          # 4-byte Reload
	imull	%esi, %edx
	addl	4(%rsp), %edx           # 4-byte Folded Reload
	movslq	%edx, %rdx
	movq	120(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rdx,8), %rbx
	cmpq	%rbx, %rdi
	movq	168(%rsp), %rbx         # 8-byte Reload
	jae	.LBB7_60
# BB#59:                                # %vector.memcheck
                                        #   in Loop: Header=BB7_46 Depth=3
	movq	112(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rcx,8), %rcx
	leaq	(%rbx,%rdx,8), %rdx
	cmpq	%rcx, %rdx
	jae	.LBB7_60
	.p2align	4, 0x90
.LBB7_47:                               #   in Loop: Header=BB7_46 Depth=3
	xorl	%r13d, %r13d
.LBB7_48:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB7_46 Depth=3
	movl	%r8d, %edx
	subl	%r13d, %edx
	movq	80(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %r11d
	subl	%r13d, %r11d
	andl	$3, %edx
	je	.LBB7_51
# BB#49:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB7_46 Depth=3
	negl	%edx
	.p2align	4, 0x90
.LBB7_50:                               # %scalar.ph.prol
                                        #   Parent Loop BB7_7 Depth=1
                                        #     Parent Loop BB7_44 Depth=2
                                        #       Parent Loop BB7_46 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rbx,%r10,8), %rcx
	movq	%rcx, (%rbp,%rax,8)
	incq	%rax
	incq	%r10
	incl	%r13d
	incl	%edx
	jne	.LBB7_50
.LBB7_51:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB7_46 Depth=3
	cmpl	$3, %r11d
	jb	.LBB7_54
# BB#52:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB7_46 Depth=3
	movq	208(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r10,8), %rdx
	movq	72(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	movl	%r8d, %ecx
	subl	%r13d, %ecx
	.p2align	4, 0x90
.LBB7_53:                               # %scalar.ph
                                        #   Parent Loop BB7_7 Depth=1
                                        #     Parent Loop BB7_44 Depth=2
                                        #       Parent Loop BB7_46 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	-24(%rdx), %rdi
	movq	%rdi, -24(%rax)
	movq	-16(%rdx), %rdi
	movq	%rdi, -16(%rax)
	movq	-8(%rdx), %rdi
	movq	%rdi, -8(%rax)
	movq	(%rdx), %rdi
	movq	%rdi, (%rax)
	addq	$32, %rdx
	addq	$32, %rax
	addl	$-4, %ecx
	jne	.LBB7_53
.LBB7_54:                               # %._crit_edge599.us.us
                                        #   in Loop: Header=BB7_46 Depth=3
	addl	16(%rsp), %r9d          # 4-byte Folded Reload
	addl	12(%rsp), %r15d         # 4-byte Folded Reload
	incl	%esi
	cmpl	40(%rsp), %esi          # 4-byte Folded Reload
	jne	.LBB7_46
# BB#55:                                #   in Loop: Header=BB7_44 Depth=2
	movl	48(%rsp), %eax          # 4-byte Reload
	movl	52(%rsp), %ecx          # 4-byte Reload
	movl	64(%rsp), %edx          # 4-byte Reload
.LBB7_56:                               # %._crit_edge605.us
                                        #   in Loop: Header=BB7_44 Depth=2
	addl	8(%rsp), %ecx           # 4-byte Folded Reload
	addl	4(%rsp), %eax           # 4-byte Folded Reload
	addl	28(%rsp), %ecx          # 4-byte Folded Reload
	addl	32(%rsp), %eax          # 4-byte Folded Reload
	incl	%edx
	cmpl	36(%rsp), %edx          # 4-byte Folded Reload
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	%eax, 4(%rsp)           # 4-byte Spill
	jne	.LBB7_44
	.p2align	4, 0x90
.LBB7_26:                               # %.loopexit
                                        #   in Loop: Header=BB7_7 Depth=1
	movq	152(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	movq	200(%rsp), %r12         # 8-byte Reload
	movslq	8(%r12), %rax
	cmpq	%rax, %rcx
	jl	.LBB7_7
.LBB7_27:                               # %._crit_edge639
	movq	136(%rsp), %rdi         # 8-byte Reload
	callq	hypre_BoxDestroy
.LBB7_28:
	movq	%r12, %rdi
	callq	hypre_BoxArrayDestroy
	xorl	%eax, %eax
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	hypre_StructVectorSetBoxValues, .Lfunc_end7-hypre_StructVectorSetBoxValues
	.cfi_endproc

	.globl	hypre_StructVectorGetValues
	.p2align	4, 0x90
	.type	hypre_StructVectorGetValues,@function
hypre_StructVectorGetValues:            # @hypre_StructVectorGetValues
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
.Lcfi51:
	.cfi_offset %rbx, -56
.Lcfi52:
	.cfi_offset %r12, -48
.Lcfi53:
	.cfi_offset %r13, -40
.Lcfi54:
	.cfi_offset %r14, -32
.Lcfi55:
	.cfi_offset %r15, -24
.Lcfi56:
	.cfi_offset %rbp, -16
	movq	8(%rdi), %rax
	movq	8(%rax), %rax
	movslq	8(%rax), %r8
	testq	%r8, %r8
	jle	.LBB8_1
# BB#2:                                 # %.lr.ph
	movq	(%rax), %r11
	movl	(%rsi), %r10d
	movl	$4, %eax
	xorl	%ecx, %ecx
                                        # implicit-def: %XMM0
	.p2align	4, 0x90
.LBB8_3:                                # =>This Inner Loop Header: Depth=1
	cmpl	-16(%r11,%rax,4), %r10d
	jl	.LBB8_10
# BB#4:                                 #   in Loop: Header=BB8_3 Depth=1
	cmpl	-4(%r11,%rax,4), %r10d
	jg	.LBB8_10
# BB#5:                                 #   in Loop: Header=BB8_3 Depth=1
	movl	4(%rsi), %ebx
	cmpl	-12(%r11,%rax,4), %ebx
	jl	.LBB8_10
# BB#6:                                 #   in Loop: Header=BB8_3 Depth=1
	cmpl	(%r11,%rax,4), %ebx
	jg	.LBB8_10
# BB#7:                                 #   in Loop: Header=BB8_3 Depth=1
	movl	8(%rsi), %r12d
	cmpl	-8(%r11,%rax,4), %r12d
	jl	.LBB8_10
# BB#8:                                 #   in Loop: Header=BB8_3 Depth=1
	cmpl	4(%r11,%rax,4), %r12d
	jg	.LBB8_10
# BB#9:                                 #   in Loop: Header=BB8_3 Depth=1
	movq	16(%rdi), %r14
	movq	40(%rdi), %rbp
	movq	%rbp, -16(%rsp)         # 8-byte Spill
	movq	(%r14), %r14
	movl	-16(%r14,%rax,4), %ebp
	movl	%ebp, -20(%rsp)         # 4-byte Spill
	movl	-4(%r14,%rax,4), %r9d
	movl	%r9d, %r13d
	subl	%ebp, %r13d
	incl	%r13d
	cmpl	%ebp, %r9d
	movl	-12(%r14,%rax,4), %ebp
	movl	%ebp, -24(%rsp)         # 4-byte Spill
	movq	%rdx, -8(%rsp)          # 8-byte Spill
	movl	(%r14,%rax,4), %r15d
	movl	$0, %edx
	cmovsl	%edx, %r13d
	movl	%r15d, %edx
	subl	%ebp, %edx
	incl	%edx
	cmpl	%ebp, %r15d
	movq	-16(%rsp), %rbp         # 8-byte Reload
	movslq	(%rbp,%rcx,4), %r9
	movl	$0, %ebp
	cmovsl	%ebp, %edx
	shlq	$3, %r9
	addq	24(%rdi), %r9
	movl	%r10d, %r15d
	subl	-20(%rsp), %r15d        # 4-byte Folded Reload
	subl	-24(%rsp), %ebx         # 4-byte Folded Reload
	subl	-8(%r14,%rax,4), %r12d
	imull	%edx, %r12d
	addl	%ebx, %r12d
	imull	%r13d, %r12d
	addl	%r15d, %r12d
	movslq	%r12d, %rdx
	movsd	(%r9,%rdx,8), %xmm0     # xmm0 = mem[0],zero
	movq	-8(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB8_10:                               #   in Loop: Header=BB8_3 Depth=1
	incq	%rcx
	addq	$6, %rax
	cmpq	%r8, %rcx
	jl	.LBB8_3
	jmp	.LBB8_11
.LBB8_1:
                                        # implicit-def: %XMM0
.LBB8_11:                               # %._crit_edge
	movsd	%xmm0, (%rdx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	hypre_StructVectorGetValues, .Lfunc_end8-hypre_StructVectorGetValues
	.cfi_endproc

	.globl	hypre_StructVectorGetBoxValues
	.p2align	4, 0x90
	.type	hypre_StructVectorGetBoxValues,@function
hypre_StructVectorGetBoxValues:         # @hypre_StructVectorGetBoxValues
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi63:
	.cfi_def_cfa_offset 256
.Lcfi64:
	.cfi_offset %rbx, -56
.Lcfi65:
	.cfi_offset %r12, -48
.Lcfi66:
	.cfi_offset %r13, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r12
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	movq	8(%rdi), %rax
	movq	8(%rax), %r14
	movl	8(%r14), %edi
	callq	hypre_BoxArrayCreate
	movq	%rax, %r13
	callq	hypre_BoxCreate
	movq	%rax, %r15
	cmpl	$0, 8(%r14)
	jle	.LBB9_4
# BB#1:                                 # %.lr.ph366
	xorl	%ebp, %ebp
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	%r13, %r12
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rsi
	addq	%rbp, %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r15, %rdx
	callq	hypre_IntersectBoxes
	movl	(%r15), %eax
	movq	(%r12), %rcx
	movl	%eax, (%rcx,%rbp)
	movl	4(%r15), %eax
	movl	%eax, 4(%rcx,%rbp)
	movl	8(%r15), %eax
	movl	%eax, 8(%rcx,%rbp)
	movl	12(%r15), %eax
	movl	%eax, 12(%rcx,%rbp)
	movl	16(%r15), %eax
	movl	%eax, 16(%rcx,%rbp)
	movl	20(%r15), %eax
	movl	%eax, 20(%rcx,%rbp)
	incq	%r13
	movslq	8(%r14), %rax
	addq	$24, %rbp
	cmpq	%rax, %r13
	jl	.LBB9_2
# BB#3:                                 # %._crit_edge367.thread
	movq	%r15, %rdi
	callq	hypre_BoxDestroy
	movq	%r12, %r13
	movq	16(%rsp), %r12          # 8-byte Reload
	jmp	.LBB9_5
.LBB9_4:                                # %._crit_edge367
	movq	%r15, %rdi
	callq	hypre_BoxDestroy
	testq	%r13, %r13
	je	.LBB9_27
.LBB9_5:
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	16(%rax), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	%r12, %rdi
	callq	hypre_BoxDuplicate
	movq	%rax, 56(%rsp)          # 8-byte Spill
	cmpl	$0, 8(%r13)
	jle	.LBB9_26
# BB#6:                                 # %.lr.ph362
	leaq	8(%rbx), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	leaq	48(%rbx), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	leaq	24(%rbx), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%r13, 176(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB9_7:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_12 Depth 2
                                        #       Child Loop BB9_14 Depth 3
                                        #         Child Loop BB9_37 Depth 4
                                        #         Child Loop BB9_18 Depth 4
                                        #         Child Loop BB9_21 Depth 4
	movq	(%r13), %r13
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rcx,2), %r12
	leaq	(,%r12,8), %rbp
	addq	%r13, %rbp
	je	.LBB9_25
# BB#8:                                 #   in Loop: Header=BB9_7 Depth=1
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %r15
	movl	(%rbp), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	136(%rsp), %rcx         # 8-byte Reload
	leaq	(,%rcx,8), %rax
	leaq	(%rax,%rax,2), %r14
	movl	4(%r13,%r14), %eax
	movl	%eax, 88(%rsp)          # 4-byte Spill
	movl	8(%r13,%r14), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	24(%rax), %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	40(%rax), %rax
	movslq	(%rax,%rcx,4), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	leaq	148(%rsp), %rsi
	callq	hypre_BoxGetSize
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %r10d
	movl	4(%rcx), %esi
	movl	12(%rcx), %eax
	movl	%eax, %edx
	subl	%r10d, %edx
	incl	%edx
	cmpl	%r10d, %eax
	movl	16(%rcx), %eax
	movl	$0, %ecx
	cmovsl	%ecx, %edx
	movl	%eax, %r11d
	subl	%esi, %r11d
	incl	%r11d
	movl	%esi, 80(%rsp)          # 4-byte Spill
	cmpl	%esi, %eax
	movl	(%r15,%r14), %edi
	movl	4(%r15,%r14), %r9d
	movl	12(%r15,%r14), %eax
	cmovsl	%ecx, %r11d
	movl	%eax, %esi
	subl	%edi, %esi
	incl	%esi
	cmpl	%edi, %eax
	movl	16(%r15,%r14), %eax
	cmovsl	%ecx, %esi
	movl	%esi, 32(%rsp)          # 4-byte Spill
	movl	%eax, %r8d
	subl	%r9d, %r8d
	incl	%r8d
	cmpl	%r9d, %eax
	cmovsl	%ecx, %r8d
	movl	148(%rsp), %eax
	movl	152(%rsp), %ecx
	movl	156(%rsp), %esi
	cmpl	%eax, %ecx
	movq	%rax, 16(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	cmovgel	%ecx, %eax
	cmpl	%eax, %esi
	movl	%esi, 36(%rsp)          # 4-byte Spill
	cmovgel	%esi, %eax
	testl	%eax, %eax
	jle	.LBB9_25
# BB#9:                                 # %.lr.ph357
                                        #   in Loop: Header=BB9_7 Depth=1
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	jle	.LBB9_25
# BB#10:                                # %.lr.ph357
                                        #   in Loop: Header=BB9_7 Depth=1
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	jle	.LBB9_25
# BB#11:                                # %.preheader323.us.preheader
                                        #   in Loop: Header=BB9_7 Depth=1
	leaq	4(%r13,%r14), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	4(%rax), %r13d
	subl	8(%r15,%r12,8), %r13d
	movl	32(%rsp), %r14d         # 4-byte Reload
	movl	%r14d, %esi
	movq	16(%rsp), %r12          # 8-byte Reload
	subl	%r12d, %esi
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movl	%edx, %ebp
	subl	%r12d, %ebp
	movq	96(%rsp), %r15          # 8-byte Reload
	leal	-1(%r15), %eax
	imull	%eax, %esi
	imull	%eax, %ebp
	movl	%edi, 28(%rsp)          # 4-byte Spill
	movl	%r10d, %eax
	movl	8(%rsp), %r10d          # 4-byte Reload
	subl	%eax, %r10d
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rdi
	movl	%r9d, 8(%rsp)           # 4-byte Spill
	movl	88(%rsp), %r9d          # 4-byte Reload
	subl	80(%rsp), %r9d          # 4-byte Folded Reload
	movl	%r8d, %eax
	subl	%r15d, %eax
	imull	%r14d, %eax
	movl	%eax, 116(%rsp)         # 4-byte Spill
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	12(%rsp), %ecx          # 4-byte Reload
	subl	8(%rax), %ecx
	imull	%r11d, %ecx
	subl	%r15d, %r11d
	imull	%edx, %r11d
	movl	%r11d, 112(%rsp)        # 4-byte Spill
	addl	%r14d, %esi
	subl	%r12d, %esi
	movl	%esi, 124(%rsp)         # 4-byte Spill
	addl	%edx, %ebp
	subl	%r12d, %ebp
	movl	%ebp, 120(%rsp)         # 4-byte Spill
	addl	%r9d, %ecx
	leal	-1(%r12), %esi
	imull	%edx, %ecx
	addl	%r10d, %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	%r14d, %eax
	imull	%r15d, %eax
	movl	%eax, 108(%rsp)         # 4-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	subl	28(%rsp), %eax          # 4-byte Folded Reload
	movl	%edx, %ecx
	imull	%r15d, %ecx
	movl	%ecx, 64(%rsp)          # 4-byte Spill
	movq	72(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %ecx
	subl	8(%rsp), %ecx           # 4-byte Folded Reload
	imull	%r8d, %r13d
	addl	%ecx, %r13d
	imull	%r14d, %r13d
	addl	%eax, %r13d
	movl	%r13d, 8(%rsp)          # 4-byte Spill
	movq	160(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	48(%rsp), %rax          # 8-byte Reload
	addq	%rsi, %rax
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	leaq	1(%rsi), %r9
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	8(%rcx,%rax,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%r9, %r11
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	andq	%rax, %r11
	leaq	-4(%r11), %rax
	shrq	$2, %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB9_12:                               # %.preheader323.us
                                        #   Parent Loop BB9_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_14 Depth 3
                                        #         Child Loop BB9_37 Depth 4
                                        #         Child Loop BB9_18 Depth 4
                                        #         Child Loop BB9_21 Depth 4
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	movl	120(%rsp), %eax         # 4-byte Reload
	movl	124(%rsp), %esi         # 4-byte Reload
	jle	.LBB9_24
# BB#13:                                # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB9_12 Depth=2
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	xorl	%r10d, %r10d
	movl	12(%rsp), %r15d         # 4-byte Reload
	movl	8(%rsp), %r8d           # 4-byte Reload
	jmp	.LBB9_14
.LBB9_32:                               # %vector.body.preheader
                                        #   in Loop: Header=BB9_14 Depth=3
	cmpq	$0, 72(%rsp)            # 8-byte Folded Reload
	jne	.LBB9_33
# BB#34:                                # %vector.body.prol
                                        #   in Loop: Header=BB9_14 Depth=3
	movups	(%rdi,%rbp,8), %xmm0
	movups	16(%rdi,%rbp,8), %xmm1
	movups	%xmm0, (%rbx,%r14,8)
	movups	%xmm1, 16(%rbx,%r14,8)
	movl	$4, %eax
	jmp	.LBB9_35
.LBB9_33:                               #   in Loop: Header=BB9_14 Depth=3
	xorl	%eax, %eax
.LBB9_35:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB9_14 Depth=3
	movl	%r12d, %edx
	cmpq	$0, 80(%rsp)            # 8-byte Folded Reload
	je	.LBB9_38
# BB#36:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB9_14 Depth=3
	leaq	(%rdi,%rbp,8), %rsi
	movq	184(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r14,8), %rcx
	.p2align	4, 0x90
.LBB9_37:                               # %vector.body
                                        #   Parent Loop BB9_7 Depth=1
                                        #     Parent Loop BB9_12 Depth=2
                                        #       Parent Loop BB9_14 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	(%rsi,%rax,8), %xmm0
	movups	16(%rsi,%rax,8), %xmm1
	movups	%xmm0, -48(%rcx,%rax,8)
	movups	%xmm1, -32(%rcx,%rax,8)
	movups	32(%rsi,%rax,8), %xmm0
	movups	48(%rsi,%rax,8), %xmm1
	movups	%xmm0, -16(%rcx,%rax,8)
	movups	%xmm1, (%rcx,%rax,8)
	addq	$8, %rax
	cmpq	%rax, %r11
	jne	.LBB9_37
.LBB9_38:                               # %middle.block
                                        #   in Loop: Header=BB9_14 Depth=3
	cmpq	%r11, %r9
	je	.LBB9_22
# BB#39:                                #   in Loop: Header=BB9_14 Depth=3
	addq	%r11, %r14
	addq	%r11, %rbp
	movl	%r11d, %r13d
	jmp	.LBB9_16
	.p2align	4, 0x90
.LBB9_14:                               # %.preheader.us.us
                                        #   Parent Loop BB9_7 Depth=1
                                        #     Parent Loop BB9_12 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB9_37 Depth 4
                                        #         Child Loop BB9_18 Depth 4
                                        #         Child Loop BB9_21 Depth 4
	movslq	%r8d, %rbp
	movslq	%r15d, %r14
	cmpq	$3, %r9
	jbe	.LBB9_15
# BB#28:                                # %min.iters.checked
                                        #   in Loop: Header=BB9_14 Depth=3
	testq	%r11, %r11
	je	.LBB9_15
# BB#29:                                # %vector.memcheck
                                        #   in Loop: Header=BB9_14 Depth=3
	movl	%edx, %r12d
	movl	%edx, %eax
	imull	%r10d, %eax
	addl	12(%rsp), %eax          # 4-byte Folded Reload
	cltq
	leaq	(%rbx,%rax,8), %rsi
	movl	32(%rsp), %ecx          # 4-byte Reload
	imull	%r10d, %ecx
	addl	8(%rsp), %ecx           # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	cmpq	%rdx, %rsi
	jae	.LBB9_32
# BB#30:                                # %vector.memcheck
                                        #   in Loop: Header=BB9_14 Depth=3
	movq	88(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,8), %rax
	leaq	(%rdi,%rcx,8), %rcx
	cmpq	%rax, %rcx
	jae	.LBB9_32
# BB#31:                                #   in Loop: Header=BB9_14 Depth=3
	xorl	%r13d, %r13d
	movl	%r12d, %edx
	jmp	.LBB9_16
	.p2align	4, 0x90
.LBB9_15:                               #   in Loop: Header=BB9_14 Depth=3
	xorl	%r13d, %r13d
.LBB9_16:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB9_14 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r12d
	subl	%r13d, %r12d
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	%eax, %esi
	subl	%r13d, %esi
	andl	$3, %r12d
	je	.LBB9_19
# BB#17:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB9_14 Depth=3
	negl	%r12d
	.p2align	4, 0x90
.LBB9_18:                               # %scalar.ph.prol
                                        #   Parent Loop BB9_7 Depth=1
                                        #     Parent Loop BB9_12 Depth=2
                                        #       Parent Loop BB9_14 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rdi,%rbp,8), %rax
	movq	%rax, (%rbx,%r14,8)
	incq	%rbp
	incq	%r14
	incl	%r13d
	incl	%r12d
	jne	.LBB9_18
.LBB9_19:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB9_14 Depth=3
	cmpl	$3, %esi
	jb	.LBB9_22
# BB#20:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB9_14 Depth=3
	movq	192(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r14,8), %rsi
	leaq	24(%rdi,%rbp,8), %rbp
	movq	16(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%r13d, %eax
	.p2align	4, 0x90
.LBB9_21:                               # %scalar.ph
                                        #   Parent Loop BB9_7 Depth=1
                                        #     Parent Loop BB9_12 Depth=2
                                        #       Parent Loop BB9_14 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	-24(%rbp), %rcx
	movq	%rcx, -24(%rsi)
	movq	-16(%rbp), %rcx
	movq	%rcx, -16(%rsi)
	movq	-8(%rbp), %rcx
	movq	%rcx, -8(%rsi)
	movq	(%rbp), %rcx
	movq	%rcx, (%rsi)
	addq	$32, %rsi
	addq	$32, %rbp
	addl	$-4, %eax
	jne	.LBB9_21
.LBB9_22:                               # %._crit_edge.us.us
                                        #   in Loop: Header=BB9_14 Depth=3
	addl	32(%rsp), %r8d          # 4-byte Folded Reload
	addl	%edx, %r15d
	incl	%r10d
	cmpl	96(%rsp), %r10d         # 4-byte Folded Reload
	jne	.LBB9_14
# BB#23:                                #   in Loop: Header=BB9_12 Depth=2
	movl	64(%rsp), %eax          # 4-byte Reload
	movl	108(%rsp), %esi         # 4-byte Reload
	movl	28(%rsp), %ecx          # 4-byte Reload
.LBB9_24:                               # %._crit_edge331.us
                                        #   in Loop: Header=BB9_12 Depth=2
	addl	8(%rsp), %esi           # 4-byte Folded Reload
	addl	12(%rsp), %eax          # 4-byte Folded Reload
	addl	116(%rsp), %esi         # 4-byte Folded Reload
	addl	112(%rsp), %eax         # 4-byte Folded Reload
	incl	%ecx
	cmpl	36(%rsp), %ecx          # 4-byte Folded Reload
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jne	.LBB9_12
	.p2align	4, 0x90
.LBB9_25:                               # %.loopexit
                                        #   in Loop: Header=BB9_7 Depth=1
	movq	136(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	movq	176(%rsp), %r13         # 8-byte Reload
	movslq	8(%r13), %rax
	cmpq	%rax, %rcx
	jl	.LBB9_7
.LBB9_26:                               # %._crit_edge363
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	hypre_BoxDestroy
.LBB9_27:
	movq	%r13, %rdi
	callq	hypre_BoxArrayDestroy
	xorl	%eax, %eax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	hypre_StructVectorGetBoxValues, .Lfunc_end9-hypre_StructVectorGetBoxValues
	.cfi_endproc

	.globl	hypre_StructVectorSetNumGhost
	.p2align	4, 0x90
	.type	hypre_StructVectorSetNumGhost,@function
hypre_StructVectorSetNumGhost:          # @hypre_StructVectorSetNumGhost
	.cfi_startproc
# BB#0:
	movl	(%rsi), %eax
	movl	%eax, 48(%rdi)
	movl	4(%rsi), %eax
	movl	%eax, 52(%rdi)
	movl	8(%rsi), %eax
	movl	%eax, 56(%rdi)
	movl	12(%rsi), %eax
	movl	%eax, 60(%rdi)
	movl	16(%rsi), %eax
	movl	%eax, 64(%rdi)
	movl	20(%rsi), %eax
	movl	%eax, 68(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end10:
	.size	hypre_StructVectorSetNumGhost, .Lfunc_end10-hypre_StructVectorSetNumGhost
	.cfi_endproc

	.globl	hypre_StructVectorAssemble
	.p2align	4, 0x90
	.type	hypre_StructVectorAssemble,@function
hypre_StructVectorAssemble:             # @hypre_StructVectorAssemble
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end11:
	.size	hypre_StructVectorAssemble, .Lfunc_end11-hypre_StructVectorAssemble
	.cfi_endproc

	.globl	hypre_StructVectorSetConstantValues
	.p2align	4, 0x90
	.type	hypre_StructVectorSetConstantValues,@function
hypre_StructVectorSetConstantValues:    # @hypre_StructVectorSetConstantValues
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi76:
	.cfi_def_cfa_offset 176
.Lcfi77:
	.cfi_offset %rbx, -56
.Lcfi78:
	.cfi_offset %r12, -48
.Lcfi79:
	.cfi_offset %r13, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	8(%rdi), %rax
	movq	8(%rax), %rcx
	cmpl	$0, 8(%rcx)
	jle	.LBB12_14
# BB#1:                                 # %.lr.ph265
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	xorl	%edx, %edx
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movaps	%xmm1, 80(%rsp)         # 16-byte Spill
	.p2align	4, 0x90
.LBB12_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_7 Depth 2
                                        #       Child Loop BB12_8 Depth 3
                                        #         Child Loop BB12_19 Depth 4
                                        #         Child Loop BB12_22 Depth 4
                                        #         Child Loop BB12_10 Depth 4
	movq	(%rcx), %r15
	leaq	(,%rdx,8), %rax
	leaq	(%rax,%rax,2), %rbp
	leaq	(%r15,%rbp), %r14
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rax
	movq	24(%rcx), %rsi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	(%rax), %rbx
	movq	40(%rcx), %rax
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movslq	(%rax,%rdx,4), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r14, %rdi
	leaq	44(%rsp), %rsi
	callq	hypre_BoxGetSize
	movaps	80(%rsp), %xmm1         # 16-byte Reload
	movapd	96(%rsp), %xmm0         # 16-byte Reload
	movl	(%rbx,%rbp), %edi
	movl	4(%rbx,%rbp), %ecx
	movl	12(%rbx,%rbp), %eax
	movl	%eax, %r12d
	subl	%edi, %r12d
	incl	%r12d
	cmpl	%edi, %eax
	movl	16(%rbx,%rbp), %eax
	movl	$0, %edx
	cmovsl	%edx, %r12d
	movl	%eax, %esi
	subl	%ecx, %esi
	incl	%esi
	cmpl	%ecx, %eax
	cmovsl	%edx, %esi
	movl	%esi, %r8d
	movl	44(%rsp), %r13d
	movl	48(%rsp), %esi
	movl	52(%rsp), %edx
	cmpl	%r13d, %esi
	movl	%r13d, %eax
	cmovgel	%esi, %eax
	cmpl	%eax, %edx
	movl	%edx, 12(%rsp)          # 4-byte Spill
	cmovgel	%edx, %eax
	testl	%eax, %eax
	jle	.LBB12_13
# BB#3:                                 # %.lr.ph227
                                        #   in Loop: Header=BB12_2 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB12_13
# BB#4:                                 # %.lr.ph227.split.us.preheader
                                        #   in Loop: Header=BB12_2 Depth=1
	testl	%esi, %esi
	jle	.LBB12_13
# BB#5:                                 # %.lr.ph227.split.us.preheader
                                        #   in Loop: Header=BB12_2 Depth=1
	testl	%r13d, %r13d
	jle	.LBB12_13
# BB#6:                                 # %.preheader203.us.us.us.preheader
                                        #   in Loop: Header=BB12_2 Depth=1
	movl	(%r14), %eax
	subl	%edi, %eax
	movl	4(%r15,%rbp), %edi
	movl	8(%r15,%rbp), %r10d
	subl	%ecx, %edi
	subl	8(%rbx,%rbp), %r10d
	imull	%r8d, %r10d
	addl	%edi, %r10d
	imull	%r12d, %r10d
	addl	%eax, %r10d
	leal	-1(%r13), %edi
	incq	%rdi
	movq	%rdi, %rbp
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	andq	%rax, %rbp
	leaq	-4(%rbp), %r15
	movl	%r15d, %r11d
	shrl	$2, %r11d
	incl	%r11d
	andl	$3, %r11d
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	16(%rax,%rcx,8), %rbx
	imull	%r12d, %r8d
	movl	%r8d, 36(%rsp)          # 4-byte Spill
	movq	%r11, %rax
	negq	%rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB12_7:                               # %.preheader203.us.us.us
                                        #   Parent Loop BB12_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_8 Depth 3
                                        #         Child Loop BB12_19 Depth 4
                                        #         Child Loop BB12_22 Depth 4
                                        #         Child Loop BB12_10 Depth 4
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movl	%r10d, 16(%rsp)         # 4-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB12_8:                               # %.preheader.us.us.us.us
                                        #   Parent Loop BB12_2 Depth=1
                                        #     Parent Loop BB12_7 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB12_19 Depth 4
                                        #         Child Loop BB12_22 Depth 4
                                        #         Child Loop BB12_10 Depth 4
	movslq	%r10d, %r8
	xorl	%ecx, %ecx
	cmpq	$3, %rdi
	jbe	.LBB12_9
# BB#15:                                # %min.iters.checked
                                        #   in Loop: Header=BB12_8 Depth=3
	testq	%rbp, %rbp
	je	.LBB12_9
# BB#16:                                # %vector.ph
                                        #   in Loop: Header=BB12_8 Depth=3
	testq	%r11, %r11
	je	.LBB12_17
# BB#18:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB12_8 Depth=3
	leaq	(%rbx,%r8,8), %rcx
	movq	24(%rsp), %rax          # 8-byte Reload
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB12_19:                              # %vector.body.prol
                                        #   Parent Loop BB12_2 Depth=1
                                        #     Parent Loop BB12_7 Depth=2
                                        #       Parent Loop BB12_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	%xmm1, -16(%rcx,%r9,8)
	movups	%xmm1, (%rcx,%r9,8)
	addq	$4, %r9
	incq	%rax
	jne	.LBB12_19
	jmp	.LBB12_20
.LBB12_17:                              #   in Loop: Header=BB12_8 Depth=3
	xorl	%r9d, %r9d
.LBB12_20:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB12_8 Depth=3
	cmpq	$12, %r15
	jb	.LBB12_23
# BB#21:                                # %vector.ph.new
                                        #   in Loop: Header=BB12_8 Depth=3
	leaq	-16(%rbx,%r8,8), %rcx
	.p2align	4, 0x90
.LBB12_22:                              # %vector.body
                                        #   Parent Loop BB12_2 Depth=1
                                        #     Parent Loop BB12_7 Depth=2
                                        #       Parent Loop BB12_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	%xmm1, (%rcx,%r9,8)
	movups	%xmm1, 16(%rcx,%r9,8)
	movups	%xmm1, 32(%rcx,%r9,8)
	movups	%xmm1, 48(%rcx,%r9,8)
	movups	%xmm1, 64(%rcx,%r9,8)
	movups	%xmm1, 80(%rcx,%r9,8)
	movups	%xmm1, 96(%rcx,%r9,8)
	movups	%xmm1, 112(%rcx,%r9,8)
	addq	$16, %r9
	cmpq	%r9, %rbp
	jne	.LBB12_22
.LBB12_23:                              # %middle.block
                                        #   in Loop: Header=BB12_8 Depth=3
	cmpq	%rbp, %rdi
	je	.LBB12_11
# BB#24:                                #   in Loop: Header=BB12_8 Depth=3
	addq	%rbp, %r8
	movl	%ebp, %ecx
	.p2align	4, 0x90
.LBB12_9:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB12_8 Depth=3
	leaq	-16(%rbx,%r8,8), %rdx
	movl	%r13d, %eax
	subl	%ecx, %eax
	.p2align	4, 0x90
.LBB12_10:                              # %scalar.ph
                                        #   Parent Loop BB12_2 Depth=1
                                        #     Parent Loop BB12_7 Depth=2
                                        #       Parent Loop BB12_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	%xmm0, (%rdx)
	addq	$8, %rdx
	decl	%eax
	jne	.LBB12_10
.LBB12_11:                              # %._crit_edge.us.us.us.us
                                        #   in Loop: Header=BB12_8 Depth=3
	addl	%r12d, %r10d
	incl	%r14d
	cmpl	%esi, %r14d
	jne	.LBB12_8
# BB#12:                                # %._crit_edge208.us-lcssa.us.us.us.us
                                        #   in Loop: Header=BB12_7 Depth=2
	movl	40(%rsp), %eax          # 4-byte Reload
	incl	%eax
	movl	16(%rsp), %r10d         # 4-byte Reload
	addl	36(%rsp), %r10d         # 4-byte Folded Reload
	cmpl	12(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB12_7
	.p2align	4, 0x90
.LBB12_13:                              # %._crit_edge228
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	movq	56(%rsp), %rcx          # 8-byte Reload
	movslq	8(%rcx), %rax
	cmpq	%rax, %rdx
	jl	.LBB12_2
.LBB12_14:                              # %._crit_edge266
	xorl	%eax, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	hypre_StructVectorSetConstantValues, .Lfunc_end12-hypre_StructVectorSetConstantValues
	.cfi_endproc

	.globl	hypre_StructVectorClearGhostValues
	.p2align	4, 0x90
	.type	hypre_StructVectorClearGhostValues,@function
hypre_StructVectorClearGhostValues:     # @hypre_StructVectorClearGhostValues
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi86:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi87:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi89:
	.cfi_def_cfa_offset 208
.Lcfi90:
	.cfi_offset %rbx, -56
.Lcfi91:
	.cfi_offset %r12, -48
.Lcfi92:
	.cfi_offset %r13, -40
.Lcfi93:
	.cfi_offset %r14, -32
.Lcfi94:
	.cfi_offset %r15, -24
.Lcfi95:
	.cfi_offset %rbp, -16
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	movq	8(%rdi), %rax
	movq	8(%rax), %rbx
	xorl	%edi, %edi
	callq	hypre_BoxArrayCreate
	movq	%rax, %rbp
	cmpl	$0, 8(%rbx)
	jle	.LBB13_19
# BB#1:                                 # %.lr.ph292
	xorl	%edx, %edx
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB13_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_4 Depth 2
                                        #       Child Loop BB13_9 Depth 3
                                        #         Child Loop BB13_12 Depth 4
                                        #         Child Loop BB13_15 Depth 4
	leaq	(,%rdx,8), %rax
	leaq	(%rax,%rax,2), %rdi
	movq	(%rbx), %rsi
	addq	%rdi, %rsi
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rax
	movq	24(%rcx), %r14
	movq	(%rax), %rbx
	addq	%rbx, %rdi
	movq	40(%rcx), %rax
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movslq	(%rax,%rdx,4), %r15
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movq	%rbp, %rdx
	callq	hypre_SubtractBoxes
	cmpl	$0, 8(%rbp)
	jle	.LBB13_18
# BB#3:                                 # %.lr.ph284
                                        #   in Loop: Header=BB13_2 Depth=1
	leaq	(%r14,%r15,8), %r14
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rax,2), %rax
	leaq	4(%rbx,%rax,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB13_4:                               #   Parent Loop BB13_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB13_9 Depth 3
                                        #         Child Loop BB13_12 Depth 4
                                        #         Child Loop BB13_15 Depth 4
	movq	(%rbp), %r13
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rcx,2), %rbx
	leaq	(%r13,%rbx,8), %r15
	movq	%r15, %rdi
	leaq	60(%rsp), %rsi
	callq	hypre_BoxGetSize
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	8(%rsi), %ecx
	movl	%ecx, %r12d
	subl	%eax, %r12d
	incl	%r12d
	cmpl	%eax, %ecx
	movl	(%rsi), %edx
	movl	12(%rsi), %ecx
	movl	$0, %esi
	cmovsl	%esi, %r12d
	movl	%ecx, %edi
	subl	%edx, %edi
	incl	%edi
	cmpl	%edx, %ecx
	cmovsl	%esi, %edi
	movl	%edi, %r8d
	movl	60(%rsp), %ecx
	movl	64(%rsp), %edi
	movl	68(%rsp), %ebp
	cmpl	%ecx, %edi
	movl	%ecx, %esi
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	cmovgel	%edi, %esi
	cmpl	%esi, %ebp
	movl	%ebp, (%rsp)            # 4-byte Spill
	cmovgel	%ebp, %esi
	testl	%esi, %esi
	jle	.LBB13_17
# BB#5:                                 # %.lr.ph244
                                        #   in Loop: Header=BB13_4 Depth=2
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB13_17
# BB#6:                                 # %.lr.ph244.split.us.preheader
                                        #   in Loop: Header=BB13_4 Depth=2
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB13_17
# BB#7:                                 # %.lr.ph244.split.us.preheader
                                        #   in Loop: Header=BB13_4 Depth=2
	testl	%ecx, %ecx
	jle	.LBB13_17
# BB#8:                                 # %.preheader220.us.us.us.preheader
                                        #   in Loop: Header=BB13_4 Depth=2
	movl	4(%r13,%rbx,8), %esi
	subl	%edx, %esi
	movl	8(%r13,%rbx,8), %edx
	movq	40(%rsp), %rdi          # 8-byte Reload
	subl	4(%rdi), %edx
	imull	%r8d, %edx
	addl	%esi, %edx
	imull	%r12d, %edx
	movl	(%r15), %esi
	subl	%eax, %esi
	addl	%edx, %esi
	decl	%ecx
	leaq	8(,%rcx,8), %rbx
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	-1(%rax), %rcx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movl	%eax, %r13d
	andl	$3, %r13d
	imull	%r12d, %r8d
	movl	%r8d, 24(%rsp)          # 4-byte Spill
	leal	(,%r12,4), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movq	%r13, 112(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB13_9:                               # %.preheader220.us.us.us
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_4 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB13_12 Depth 4
                                        #         Child Loop BB13_15 Depth 4
	testq	%r13, %r13
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movl	%eax, 28(%rsp)          # 4-byte Spill
	je	.LBB13_10
# BB#11:                                # %.preheader.us.us.us.us.prol.preheader
                                        #   in Loop: Header=BB13_9 Depth=3
	movl	%esi, %ebp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB13_12:                              # %.preheader.us.us.us.us.prol
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_4 Depth=2
                                        #       Parent Loop BB13_9 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movslq	%ebp, %rbp
	leaq	(%r14,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	incq	%r15
	addl	%r12d, %ebp
	cmpq	%r15, %r13
	jne	.LBB13_12
	jmp	.LBB13_13
	.p2align	4, 0x90
.LBB13_10:                              #   in Loop: Header=BB13_9 Depth=3
	xorl	%r15d, %r15d
.LBB13_13:                              # %.preheader.us.us.us.us.prol.loopexit
                                        #   in Loop: Header=BB13_9 Depth=3
	cmpq	$3, 120(%rsp)           # 8-byte Folded Reload
	movl	20(%rsp), %r13d         # 4-byte Reload
	jb	.LBB13_16
# BB#14:                                # %.preheader220.us.us.us.new
                                        #   in Loop: Header=BB13_9 Depth=3
	movq	8(%rsp), %r12           # 8-byte Reload
	subq	%r15, %r12
	leal	3(%r15), %edx
	movq	48(%rsp), %rax          # 8-byte Reload
	imull	%eax, %edx
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	leal	2(%r15), %edx
	imull	%eax, %edx
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	movl	%eax, %edx
	imull	%r15d, %edx
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	incl	%r15d
	imull	%eax, %r15d
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, %ebp
	.p2align	4, 0x90
.LBB13_15:                              # %.preheader.us.us.us.us
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_4 Depth=2
                                        #       Parent Loop BB13_9 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	128(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rbp), %eax
	cltq
	leaq	(%r14,%rax,8), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	leal	(%r15,%rbp), %eax
	cltq
	leaq	(%r14,%rax,8), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	136(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rbp), %eax
	cltq
	leaq	(%r14,%rax,8), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	144(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rbp), %eax
	cltq
	leaq	(%r14,%rax,8), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	addl	%r13d, %ebp
	addq	$-4, %r12
	jne	.LBB13_15
.LBB13_16:                              # %._crit_edge225.us-lcssa.us.us.us.us
                                        #   in Loop: Header=BB13_9 Depth=3
	movl	28(%rsp), %eax          # 4-byte Reload
	incl	%eax
	movl	4(%rsp), %esi           # 4-byte Reload
	addl	24(%rsp), %esi          # 4-byte Folded Reload
	cmpl	(%rsp), %eax            # 4-byte Folded Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	112(%rsp), %r13         # 8-byte Reload
	jne	.LBB13_9
	.p2align	4, 0x90
.LBB13_17:                              # %._crit_edge245
                                        #   in Loop: Header=BB13_4 Depth=2
	movq	104(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	movq	88(%rsp), %rbp          # 8-byte Reload
	movslq	8(%rbp), %rax
	cmpq	%rax, %rcx
	jl	.LBB13_4
.LBB13_18:                              # %._crit_edge285
                                        #   in Loop: Header=BB13_2 Depth=1
	movq	32(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	movq	72(%rsp), %rbx          # 8-byte Reload
	movslq	8(%rbx), %rax
	cmpq	%rax, %rdx
	jl	.LBB13_2
.LBB13_19:                              # %._crit_edge293
	movq	%rbp, %rdi
	callq	hypre_BoxArrayDestroy
	xorl	%eax, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	hypre_StructVectorClearGhostValues, .Lfunc_end13-hypre_StructVectorClearGhostValues
	.cfi_endproc

	.globl	hypre_StructVectorClearAllValues
	.p2align	4, 0x90
	.type	hypre_StructVectorClearAllValues,@function
hypre_StructVectorClearAllValues:       # @hypre_StructVectorClearAllValues
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi99:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi100:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi102:
	.cfi_def_cfa_offset 208
.Lcfi103:
	.cfi_offset %rbx, -56
.Lcfi104:
	.cfi_offset %r12, -48
.Lcfi105:
	.cfi_offset %r13, -40
.Lcfi106:
	.cfi_offset %r14, -32
.Lcfi107:
	.cfi_offset %r15, -24
.Lcfi108:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	callq	hypre_BoxCreate
	movq	%rax, %rbp
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 36(%rsp)
	movl	$1, 44(%rsp)
	movl	36(%rbx), %eax
	movl	%eax, 84(%rsp)
	movl	$1, 88(%rsp)
	movl	$1, 92(%rsp)
	leaq	36(%rsp), %rsi
	leaq	84(%rsp), %rdx
	movq	%rbp, %rdi
	callq	hypre_BoxSetExtents
	movq	24(%rbx), %rbx
	leaq	72(%rsp), %rsi
	movq	%rbp, %rdi
	callq	hypre_BoxGetSize
	movl	(%rbp), %r10d
	movl	4(%rbp), %r8d
	movl	12(%rbp), %eax
	movl	%eax, %r14d
	subl	%r10d, %r14d
	incl	%r14d
	xorl	%esi, %esi
	cmpl	%r10d, %eax
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movl	16(%rbp), %eax
	cmovsl	%esi, %r14d
	movl	%eax, %edx
	subl	%r8d, %edx
	incl	%edx
	cmpl	%r8d, %eax
	cmovsl	%esi, %edx
	movl	72(%rsp), %eax
	movl	76(%rsp), %ebp
	movl	80(%rsp), %edi
	cmpl	%eax, %ebp
	movl	%eax, %esi
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	cmovgel	%ebp, %esi
	cmpl	%esi, %edi
	movl	%edi, (%rsp)            # 4-byte Spill
	cmovgel	%edi, %esi
	testl	%esi, %esi
	jle	.LBB14_16
# BB#1:                                 # %.lr.ph202
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB14_16
# BB#2:                                 # %.lr.ph202.split.us.preheader
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB14_16
# BB#3:                                 # %.lr.ph202.split.us.preheader
	testl	%eax, %eax
	jle	.LBB14_16
# BB#4:                                 # %.preheader178.us.us.us.preheader
	movslq	36(%rsp), %rsi
	movl	40(%rsp), %ebp
	movl	44(%rsp), %r11d
	movl	%r14d, %edi
	imull	%ebp, %edi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movl	%edx, %ecx
	imull	%r11d, %ecx
	imull	%r14d, %ecx
	movl	%esi, %r9d
	subl	%r10d, %r9d
	subl	%r8d, %ebp
	movq	48(%rsp), %rdi          # 8-byte Reload
	subl	8(%rdi), %r11d
	imull	%edx, %r11d
	addl	%ebp, %r11d
	imull	%r14d, %r11d
	addl	%r9d, %r11d
	leal	-1(%rax), %edx
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	incq	%rdx
	movabsq	$8589934588, %rdi       # imm = 0x1FFFFFFFC
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	andq	%rdx, %rdi
	leaq	-4(%rdi), %rdx
	movq	%rdx, 136(%rsp)         # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	shrl	$2, %edx
	incl	%edx
	movq	%rsi, %rbp
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	imulq	%rdi, %rbp
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	andl	$3, %edx
	leaq	16(%rbx), %rdi
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	movq	%rsi, %r8
	shlq	$5, %r8
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	negq	%rdx
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movq	%rsi, %r10
	shlq	$7, %r10
	movq	%rsi, %rbp
	shlq	$6, %rbp
	xorps	%xmm0, %xmm0
	leaq	(,%rsi,8), %r14
	xorl	%edx, %edx
	movl	%r11d, 24(%rsp)         # 4-byte Spill
	movl	%r11d, %edi
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB14_5:                               # %.preheader178.us.us.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_6 Depth 2
                                        #       Child Loop BB14_22 Depth 3
                                        #       Child Loop BB14_25 Depth 3
                                        #       Child Loop BB14_10 Depth 3
                                        #       Child Loop BB14_13 Depth 3
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	imull	%edx, %ecx
	addl	24(%rsp), %ecx          # 4-byte Folded Reload
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	movl	%edi, 28(%rsp)          # 4-byte Spill
	movl	%edi, %ecx
	xorl	%r12d, %r12d
	jmp	.LBB14_6
.LBB14_20:                              #   in Loop: Header=BB14_6 Depth=2
	xorl	%edx, %edx
.LBB14_23:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB14_6 Depth=2
	cmpq	$12, 136(%rsp)          # 8-byte Folded Reload
	jb	.LBB14_26
# BB#24:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB14_6 Depth=2
	movq	8(%rsp), %rdi           # 8-byte Reload
	subq	%rdx, %rdi
	imulq	%rsi, %rdx
	addq	%r15, %rdx
	leaq	(%rbx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB14_25:                              # %vector.body
                                        #   Parent Loop BB14_5 Depth=1
                                        #     Parent Loop BB14_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	%xmm0, (%rdx)
	movups	%xmm0, 16(%rdx)
	leaq	(%rdx,%r8), %rcx
	movups	%xmm0, (%rdx,%r8)
	movups	%xmm0, 16(%rdx,%r8)
	movups	%xmm0, (%r8,%rcx)
	movups	%xmm0, 16(%r8,%rcx)
	leaq	(%rcx,%r8), %rcx
	movups	%xmm0, (%r8,%rcx)
	movups	%xmm0, 16(%r8,%rcx)
	addq	%r10, %rdx
	addq	$-16, %rdi
	jne	.LBB14_25
.LBB14_26:                              # %middle.block
                                        #   in Loop: Header=BB14_6 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpq	%rcx, 64(%rsp)          # 8-byte Folded Reload
	je	.LBB14_14
# BB#27:                                #   in Loop: Header=BB14_6 Depth=2
	movq	56(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	imull	%r12d, %ecx
	addl	32(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %r9
	addq	104(%rsp), %r9          # 8-byte Folded Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%ecx, %r13d
	jmp	.LBB14_8
	.p2align	4, 0x90
.LBB14_6:                               # %.preheader.us.us.us.us
                                        #   Parent Loop BB14_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB14_22 Depth 3
                                        #       Child Loop BB14_25 Depth 3
                                        #       Child Loop BB14_10 Depth 3
                                        #       Child Loop BB14_13 Depth 3
	movslq	%ecx, %r15
	xorl	%r13d, %r13d
	cmpq	$3, 64(%rsp)            # 8-byte Folded Reload
	jbe	.LBB14_7
# BB#17:                                # %min.iters.checked
                                        #   in Loop: Header=BB14_6 Depth=2
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB14_7
# BB#18:                                # %vector.scevcheck
                                        #   in Loop: Header=BB14_6 Depth=2
	cmpl	$1, %esi
	jne	.LBB14_7
# BB#19:                                # %vector.body.preheader
                                        #   in Loop: Header=BB14_6 Depth=2
	cmpq	$0, 128(%rsp)           # 8-byte Folded Reload
	je	.LBB14_20
# BB#21:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB14_6 Depth=2
	movq	120(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r15,8), %rdi
	movq	112(%rsp), %rcx         # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB14_22:                              # %vector.body.prol
                                        #   Parent Loop BB14_5 Depth=1
                                        #     Parent Loop BB14_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	%xmm0, -16(%rdi)
	movups	%xmm0, (%rdi)
	addq	$4, %rdx
	addq	%r8, %rdi
	incq	%rcx
	jne	.LBB14_22
	jmp	.LBB14_23
	.p2align	4, 0x90
.LBB14_7:                               #   in Loop: Header=BB14_6 Depth=2
	movq	%r15, %r9
.LBB14_8:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB14_6 Depth=2
	movl	%eax, %edx
	subl	%r13d, %edx
	movq	144(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, %r11d
	subl	%r13d, %r11d
	andl	$7, %edx
	je	.LBB14_11
# BB#9:                                 # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB14_6 Depth=2
	negl	%edx
	.p2align	4, 0x90
.LBB14_10:                              # %scalar.ph.prol
                                        #   Parent Loop BB14_5 Depth=1
                                        #     Parent Loop BB14_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	$0, (%rbx,%r9,8)
	addq	%rsi, %r9
	incl	%r13d
	incl	%edx
	jne	.LBB14_10
.LBB14_11:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB14_6 Depth=2
	cmpl	$7, %r11d
	jb	.LBB14_14
# BB#12:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB14_6 Depth=2
	leaq	(%rbx,%r9,8), %rdx
	movl	%eax, %edi
	subl	%r13d, %edi
	.p2align	4, 0x90
.LBB14_13:                              # %scalar.ph
                                        #   Parent Loop BB14_5 Depth=1
                                        #     Parent Loop BB14_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	$0, (%rdx)
	leaq	(%rdx,%r14), %rcx
	movq	$0, (%rdx,%rsi,8)
	movq	$0, (%rcx,%rsi,8)
	addq	%r14, %rcx
	movq	$0, (%rcx,%rsi,8)
	addq	%r14, %rcx
	movq	$0, (%rcx,%rsi,8)
	addq	%r14, %rcx
	movq	$0, (%rcx,%rsi,8)
	addq	%r14, %rcx
	movq	$0, (%rcx,%rsi,8)
	addq	%r14, %rcx
	movq	$0, (%rcx,%rsi,8)
	addq	%rbp, %rdx
	addl	$-8, %edi
	jne	.LBB14_13
.LBB14_14:                              # %._crit_edge.us.us.us.us
                                        #   in Loop: Header=BB14_6 Depth=2
	movq	56(%rsp), %rcx          # 8-byte Reload
	leal	(%r15,%rcx), %ecx
	incl	%r12d
	cmpl	4(%rsp), %r12d          # 4-byte Folded Reload
	jne	.LBB14_6
# BB#15:                                # %._crit_edge183.us-lcssa.us.us.us.us
                                        #   in Loop: Header=BB14_5 Depth=1
	movq	96(%rsp), %rdx          # 8-byte Reload
	incl	%edx
	movl	20(%rsp), %ecx          # 4-byte Reload
	movl	28(%rsp), %edi          # 4-byte Reload
	addl	%ecx, %edi
	cmpl	(%rsp), %edx            # 4-byte Folded Reload
	jne	.LBB14_5
.LBB14_16:                              # %._crit_edge203
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	hypre_BoxDestroy
	xorl	%eax, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	hypre_StructVectorClearAllValues, .Lfunc_end14-hypre_StructVectorClearAllValues
	.cfi_endproc

	.globl	hypre_StructVectorGetMigrateCommPkg
	.p2align	4, 0x90
	.type	hypre_StructVectorGetMigrateCommPkg,@function
hypre_StructVectorGetMigrateCommPkg:    # @hypre_StructVectorGetMigrateCommPkg
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi110:
	.cfi_def_cfa_offset 24
	subq	$56, %rsp
.Lcfi111:
	.cfi_def_cfa_offset 80
.Lcfi112:
	.cfi_offset %rbx, -24
.Lcfi113:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 44(%rsp)
	movl	$1, 52(%rsp)
	movq	8(%rbx), %rdi
	movq	8(%r14), %rsi
	leaq	32(%rsp), %rdx
	leaq	24(%rsp), %rcx
	leaq	16(%rsp), %r8
	leaq	8(%rsp), %r9
	callq	hypre_CreateCommInfoFromGrids
	movq	32(%rsp), %rdi
	movq	24(%rsp), %rsi
	movq	16(%rbx), %r8
	movq	16(%r14), %r9
	movl	(%rbx), %eax
	movq	8(%rbx), %rbx
	addq	$56, %rbx
	subq	$8, %rsp
.Lcfi114:
	.cfi_adjust_cfa_offset 8
	leaq	52(%rsp), %rdx
	movq	%rdx, %rcx
	pushq	%rbx
.Lcfi115:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi116:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi117:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rsp)
.Lcfi118:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)
.Lcfi119:
	.cfi_adjust_cfa_offset 8
	callq	hypre_CommPkgCreate
	addq	$104, %rsp
.Lcfi120:
	.cfi_adjust_cfa_offset -48
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end15:
	.size	hypre_StructVectorGetMigrateCommPkg, .Lfunc_end15-hypre_StructVectorGetMigrateCommPkg
	.cfi_endproc

	.globl	hypre_StructVectorMigrate
	.p2align	4, 0x90
	.type	hypre_StructVectorMigrate,@function
hypre_StructVectorMigrate:              # @hypre_StructVectorMigrate
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi121:
	.cfi_def_cfa_offset 16
	movq	24(%rsi), %rsi
	movq	24(%rdx), %rdx
	movq	%rsp, %rcx
	callq	hypre_InitializeCommunication
	movq	(%rsp), %rdi
	callq	hypre_FinalizeCommunication
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end16:
	.size	hypre_StructVectorMigrate, .Lfunc_end16-hypre_StructVectorMigrate
	.cfi_endproc

	.globl	hypre_StructVectorPrint
	.p2align	4, 0x90
	.type	hypre_StructVectorPrint,@function
hypre_StructVectorPrint:                # @hypre_StructVectorPrint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi122:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi123:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi124:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi125:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 48
	subq	$272, %rsp              # imm = 0x110
.Lcfi127:
	.cfi_def_cfa_offset 320
.Lcfi128:
	.cfi_offset %rbx, -48
.Lcfi129:
	.cfi_offset %r12, -40
.Lcfi130:
	.cfi_offset %r14, -32
.Lcfi131:
	.cfi_offset %r15, -24
.Lcfi132:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	(%r14), %edi
	leaq	12(%rsp), %rsi
	callq	hypre_MPI_Comm_rank
	movl	12(%rsp), %ecx
	leaq	16(%rsp), %rbp
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	sprintf
	movl	$.L.str.1, %esi
	movq	%rbp, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB17_4
# BB#1:
	movl	$.L.str.3, %edi
	movl	$13, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.4, %edi
	movl	$7, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	8(%r14), %rbp
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	hypre_StructGridPrint
	movq	16(%r14), %r12
	testl	%r15d, %r15d
	movq	%r12, %r15
	jne	.LBB17_3
# BB#2:
	movq	8(%rbp), %r15
.LBB17_3:
	movl	$.L.str.5, %edi
	movl	$7, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	24(%r14), %r8
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	hypre_PrintBoxArrayData
	movq	%rbx, %rdi
	callq	fflush
	movq	%rbx, %rdi
	callq	fclose
	xorl	%eax, %eax
	addq	$272, %rsp              # imm = 0x110
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_4:
	leaq	16(%rsp), %rsi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$1, %edi
	callq	exit
.Lfunc_end17:
	.size	hypre_StructVectorPrint, .Lfunc_end17-hypre_StructVectorPrint
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI18_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	hypre_StructVectorRead
	.p2align	4, 0x90
	.type	hypre_StructVectorRead,@function
hypre_StructVectorRead:                 # @hypre_StructVectorRead
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi133:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi134:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi135:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi136:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi137:
	.cfi_def_cfa_offset 48
	subq	$272, %rsp              # imm = 0x110
.Lcfi138:
	.cfi_def_cfa_offset 320
.Lcfi139:
	.cfi_offset %rbx, -48
.Lcfi140:
	.cfi_offset %r12, -40
.Lcfi141:
	.cfi_offset %r14, -32
.Lcfi142:
	.cfi_offset %r15, -24
.Lcfi143:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movl	%edi, %r15d
	leaq	4(%rsp), %rsi
	callq	hypre_MPI_Comm_rank
	movl	4(%rsp), %ecx
	leaq	16(%rsp), %rbp
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	sprintf
	movl	$.L.str.6, %esi
	movq	%rbp, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB18_2
# BB#1:
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	leaq	8(%rsp), %rdx
	movl	%r15d, %edi
	movq	%rbx, %rsi
	callq	hypre_StructGridRead
	movq	8(%rsp), %r12
	movl	$1, %edi
	movl	$80, %esi
	callq	hypre_CAlloc
	movq	%rax, %rbp
	movl	%r15d, (%rbp)
	leaq	8(%rbp), %rsi
	movq	%r12, %rdi
	callq	hypre_StructGridRef
	movl	$1, 32(%rbp)
	movl	$1, 76(%rbp)
	movaps	.LCPI18_0(%rip), %xmm0  # xmm0 = [1,1,1,1]
	movups	%xmm0, 48(%rbp)
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 64(%rbp)
	movl	(%r14), %eax
	movl	%eax, 48(%rbp)
	movl	4(%r14), %eax
	movl	%eax, 52(%rbp)
	movl	8(%r14), %eax
	movl	%eax, 56(%rbp)
	movl	12(%r14), %eax
	movl	%eax, 60(%rbp)
	movl	16(%r14), %eax
	movl	%eax, 64(%rbp)
	movl	20(%r14), %eax
	movl	%eax, 68(%rbp)
	movq	%rbp, %rdi
	callq	hypre_StructVectorInitializeShell
	movl	36(%rbp), %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rax, 24(%rbp)
	movl	$1, 32(%rbp)
	movq	8(%rsp), %rax
	movq	8(%rax), %r14
	movq	16(%rbp), %r15
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movq	24(%rbp), %r8
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	hypre_ReadBoxArrayData
	movq	%rbx, %rdi
	callq	fclose
	movq	%rbp, %rax
	addq	$272, %rsp              # imm = 0x110
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB18_2:
	leaq	16(%rsp), %rsi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$1, %edi
	callq	exit
.Lfunc_end18:
	.size	hypre_StructVectorRead, .Lfunc_end18-hypre_StructVectorRead
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s.%05d"
	.size	.L.str, 8

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"w"
	.size	.L.str.1, 2

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Error: can't open output file %s\n"
	.size	.L.str.2, 34

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"StructVector\n"
	.size	.L.str.3, 14

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\nGrid:\n"
	.size	.L.str.4, 8

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\nData:\n"
	.size	.L.str.5, 8

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"r"
	.size	.L.str.6, 2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
