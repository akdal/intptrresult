	.text
	.file	"list.bc"
	.globl	addList
	.p2align	4, 0x90
	.type	addList,@function
addList:                                # @addList
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	testq	%rdi, %rdi
	je	.LBB0_1
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_2
	jmp	.LBB0_3
.LBB0_1:
                                        # implicit-def: %RBX
.LBB0_3:                                # %._crit_edge
	movl	$24, %edi
	callq	malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	addList, .Lfunc_end0-addList
	.cfi_endproc

	.globl	removeList
	.p2align	4, 0x90
	.type	removeList,@function
removeList:                             # @removeList
	.cfi_startproc
# BB#0:
	cmpq	%rsi, 8(%rdi)
	je	.LBB1_3
	.p2align	4, 0x90
.LBB1_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rdi
	cmpq	%rsi, 8(%rdi)
	jne	.LBB1_1
.LBB1_3:                                # %._crit_edge
	movq	(%rdi), %rcx
	movq	16(%rdi), %rax
	movq	%rcx, (%rax)
	movq	(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB1_5
# BB#4:
	movq	%rax, 16(%rcx)
.LBB1_5:
	retq
.Lfunc_end1:
	.size	removeList, .Lfunc_end1-removeList
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
