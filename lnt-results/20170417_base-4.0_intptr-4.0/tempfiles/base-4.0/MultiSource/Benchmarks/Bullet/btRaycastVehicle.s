	.text
	.file	"btRaycastVehicle.bc"
	.section	.text._ZN11btRigidBodyD2Ev,"axG",@progbits,_ZN11btRigidBodyD2Ev,comdat
	.weak	_ZN11btRigidBodyD2Ev
	.p2align	4, 0x90
	.type	_ZN11btRigidBodyD2Ev,@function
_ZN11btRigidBodyD2Ev:                   # @_ZN11btRigidBodyD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV11btRigidBody+16, (%rbx)
	movq	536(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_4
# BB#1:
	cmpb	$0, 544(%rbx)
	je	.LBB0_3
# BB#2:
.Ltmp0:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp1:
.LBB0_3:                                # %.noexc
	movq	$0, 536(%rbx)
.LBB0_4:
	movb	$1, 544(%rbx)
	movq	$0, 536(%rbx)
	movq	$0, 524(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17btCollisionObjectD2Ev # TAILCALL
.LBB0_5:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_ZN17btCollisionObjectD2Ev
.Ltmp4:
# BB#6:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_7:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN11btRigidBodyD2Ev, .Lfunc_end0-_ZN11btRigidBodyD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN16btRaycastVehicleC2ERKNS_15btVehicleTuningEP11btRigidBodyP18btVehicleRaycaster
	.p2align	4, 0x90
	.type	_ZN16btRaycastVehicleC2ERKNS_15btVehicleTuningEP11btRigidBodyP18btVehicleRaycaster,@function
_ZN16btRaycastVehicleC2ERKNS_15btVehicleTuningEP11btRigidBodyP18btVehicleRaycaster: # @_ZN16btRaycastVehicleC2ERKNS_15btVehicleTuningEP11btRigidBodyP18btVehicleRaycaster
	.cfi_startproc
# BB#0:
	movq	$_ZTV16btRaycastVehicle+16, (%rdi)
	movb	$1, 32(%rdi)
	movq	$0, 24(%rdi)
	movl	$0, 12(%rdi)
	movl	$0, 16(%rdi)
	movb	$1, 64(%rdi)
	movq	$0, 56(%rdi)
	movl	$0, 44(%rdi)
	movl	$0, 48(%rdi)
	movb	$1, 96(%rdi)
	movq	$0, 88(%rdi)
	movl	$0, 76(%rdi)
	movl	$0, 80(%rdi)
	movb	$1, 128(%rdi)
	movq	$0, 120(%rdi)
	movl	$0, 108(%rdi)
	movl	$0, 112(%rdi)
	movq	%rcx, 144(%rdi)
	movl	$0, 152(%rdi)
	movb	$1, 216(%rdi)
	movq	$0, 208(%rdi)
	movl	$0, 196(%rdi)
	movl	$0, 200(%rdi)
	movq	%rdx, 168(%rdi)
	movl	$0, 176(%rdi)
	movl	$2, 180(%rdi)
	movl	$1, 184(%rdi)
	movq	$0, 156(%rdi)
	retq
.Lfunc_end1:
	.size	_ZN16btRaycastVehicleC2ERKNS_15btVehicleTuningEP11btRigidBodyP18btVehicleRaycaster, .Lfunc_end1-_ZN16btRaycastVehicleC2ERKNS_15btVehicleTuningEP11btRigidBodyP18btVehicleRaycaster
	.cfi_endproc

	.globl	_ZN16btRaycastVehicle11defaultInitERKNS_15btVehicleTuningE
	.p2align	4, 0x90
	.type	_ZN16btRaycastVehicle11defaultInitERKNS_15btVehicleTuningE,@function
_ZN16btRaycastVehicle11defaultInitERKNS_15btVehicleTuningE: # @_ZN16btRaycastVehicle11defaultInitERKNS_15btVehicleTuningE
	.cfi_startproc
# BB#0:
	movq	$0, 156(%rdi)
	retq
.Lfunc_end2:
	.size	_ZN16btRaycastVehicle11defaultInitERKNS_15btVehicleTuningE, .Lfunc_end2-_ZN16btRaycastVehicle11defaultInitERKNS_15btVehicleTuningE
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end3:
	.size	__clang_call_terminate, .Lfunc_end3-__clang_call_terminate

	.text
	.globl	_ZN16btRaycastVehicleD2Ev
	.p2align	4, 0x90
	.type	_ZN16btRaycastVehicleD2Ev,@function
_ZN16btRaycastVehicleD2Ev:              # @_ZN16btRaycastVehicleD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV16btRaycastVehicle+16, (%rbx)
	movq	208(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_4
# BB#1:
	cmpb	$0, 216(%rbx)
	je	.LBB4_3
# BB#2:
.Ltmp6:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp7:
.LBB4_3:                                # %.noexc
	movq	$0, 208(%rbx)
.LBB4_4:
	movb	$1, 216(%rbx)
	movq	$0, 208(%rbx)
	movq	$0, 196(%rbx)
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_8
# BB#5:
	cmpb	$0, 128(%rbx)
	je	.LBB4_7
# BB#6:
.Ltmp11:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp12:
.LBB4_7:                                # %.noexc7
	movq	$0, 120(%rbx)
.LBB4_8:
	movb	$1, 128(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 108(%rbx)
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_12
# BB#9:
	cmpb	$0, 96(%rbx)
	je	.LBB4_11
# BB#10:
.Ltmp16:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp17:
.LBB4_11:                               # %.noexc9
	movq	$0, 88(%rbx)
.LBB4_12:
	movb	$1, 96(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 76(%rbx)
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_16
# BB#13:
	cmpb	$0, 64(%rbx)
	je	.LBB4_15
# BB#14:
.Ltmp21:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp22:
.LBB4_15:                               # %.noexc12
	movq	$0, 56(%rbx)
.LBB4_16:
	movb	$1, 64(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 44(%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_20
# BB#17:
	cmpb	$0, 32(%rbx)
	je	.LBB4_19
# BB#18:
.Ltmp27:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp28:
.LBB4_19:                               # %.noexc14
	movq	$0, 24(%rbx)
.LBB4_20:
	movb	$1, 32(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 12(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB4_38:
.Ltmp29:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB4_39:
.Ltmp23:
	movq	%rax, %r14
	jmp	.LBB4_40
.LBB4_32:
.Ltmp18:
	movq	%rax, %r14
	jmp	.LBB4_33
.LBB4_26:
.Ltmp13:
	movq	%rax, %r14
	jmp	.LBB4_27
.LBB4_21:
.Ltmp8:
	movq	%rax, %r14
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_25
# BB#22:
	cmpb	$0, 128(%rbx)
	je	.LBB4_24
# BB#23:
.Ltmp9:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp10:
.LBB4_24:                               # %.noexc17
	movq	$0, 120(%rbx)
.LBB4_25:                               # %_ZN20btAlignedObjectArrayIfED2Ev.exit18
	movb	$1, 128(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 108(%rbx)
.LBB4_27:
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_31
# BB#28:
	cmpb	$0, 96(%rbx)
	je	.LBB4_30
# BB#29:
.Ltmp14:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp15:
.LBB4_30:                               # %.noexc20
	movq	$0, 88(%rbx)
.LBB4_31:                               # %_ZN20btAlignedObjectArrayIfED2Ev.exit21
	movb	$1, 96(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 76(%rbx)
.LBB4_33:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_37
# BB#34:
	cmpb	$0, 64(%rbx)
	je	.LBB4_36
# BB#35:
.Ltmp19:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp20:
.LBB4_36:                               # %.noexc23
	movq	$0, 56(%rbx)
.LBB4_37:                               # %_ZN20btAlignedObjectArrayI9btVector3ED2Ev.exit24
	movb	$1, 64(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 44(%rbx)
.LBB4_40:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_44
# BB#41:
	cmpb	$0, 32(%rbx)
	je	.LBB4_43
# BB#42:
.Ltmp24:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp25:
.LBB4_43:                               # %.noexc26
	movq	$0, 24(%rbx)
.LBB4_44:                               # %_ZN20btAlignedObjectArrayI9btVector3ED2Ev.exit27
	movb	$1, 32(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 12(%rbx)
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB4_46:
.Ltmp26:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN16btRaycastVehicleD2Ev, .Lfunc_end4-_ZN16btRaycastVehicleD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin1   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin1   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin1   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin1   #     jumps to .Ltmp29
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp9-.Ltmp28          #   Call between .Ltmp28 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 7 <<
	.long	.Ltmp25-.Ltmp9          #   Call between .Ltmp9 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin1   #     jumps to .Ltmp26
	.byte	1                       #   On action: 1
	.long	.Ltmp25-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Lfunc_end4-.Ltmp25     #   Call between .Ltmp25 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN16btRaycastVehicleD0Ev
	.p2align	4, 0x90
	.type	_ZN16btRaycastVehicleD0Ev,@function
_ZN16btRaycastVehicleD0Ev:              # @_ZN16btRaycastVehicleD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp30:
	callq	_ZN16btRaycastVehicleD2Ev
.Ltmp31:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB5_2:
.Ltmp32:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZN16btRaycastVehicleD0Ev, .Lfunc_end5-_ZN16btRaycastVehicleD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp30-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin2   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end5-.Ltmp31     #   Call between .Ltmp31 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.long	0                       # float 0
	.long	0                       # float 0
	.long	0                       # float 0
	.long	1036831949              # float 0.100000001
	.text
	.globl	_ZN16btRaycastVehicle8addWheelERK9btVector3S2_S2_ffRKNS_15btVehicleTuningEb
	.p2align	4, 0x90
	.type	_ZN16btRaycastVehicle8addWheelERK9btVector3S2_S2_ffRKNS_15btVehicleTuningEb,@function
_ZN16btRaycastVehicle8addWheelERK9btVector3S2_S2_ffRKNS_15btVehicleTuningEb: # @_ZN16btRaycastVehicle8addWheelERK9btVector3S2_S2_ffRKNS_15btVehicleTuningEb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$328, %rsp              # imm = 0x148
.Lcfi21:
	.cfi_def_cfa_offset 384
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movaps	%xmm0, %xmm5
	movq	%rdi, %rbx
	movups	(%rsi), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	(%rdx), %xmm0
	movaps	%xmm0, 48(%rsp)
	movups	(%rcx), %xmm0
	movaps	%xmm0, 64(%rsp)
	movl	(%r8), %esi
	movl	4(%r8), %edx
	movl	8(%r8), %edi
	movl	16(%r8), %r14d
	movl	12(%r8), %r8d
	leaq	280(%rsp), %r15
	movaps	32(%rsp), %xmm0
	movups	%xmm0, 280(%rsp)
	movaps	48(%rsp), %xmm0
	movups	%xmm0, 296(%rsp)
	movaps	64(%rsp), %xmm0
	movups	%xmm0, 312(%rsp)
	movl	196(%rbx), %ecx
	cmpl	200(%rbx), %ecx
	jne	.LBB6_13
# BB#1:
	leal	(%rcx,%rcx), %eax
	testl	%ecx, %ecx
	movl	$1, %r12d
	cmovnel	%eax, %r12d
	cmpl	%r12d, %ecx
	jge	.LBB6_13
# BB#2:
	movl	%edi, 24(%rsp)          # 4-byte Spill
	movl	%edx, 28(%rsp)          # 4-byte Spill
	testl	%r12d, %r12d
	movss	%xmm1, 16(%rsp)         # 4-byte Spill
	movss	%xmm5, 12(%rsp)         # 4-byte Spill
	je	.LBB6_3
# BB#4:
	movslq	%r12d, %rax
	shlq	$5, %rax
	leaq	(%rax,%rax,8), %rdi
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	$16, %esi
	movl	%r9d, %ebp
	movl	%r8d, %r13d
	callq	_Z22btAlignedAllocInternalmi
	movl	%r13d, %r8d
	movl	8(%rsp), %esi           # 4-byte Reload
	movss	12(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movl	%ebp, %r9d
	movq	%rax, %rbp
	movl	196(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.LBB6_6
	jmp	.LBB6_8
.LBB6_3:
	xorl	%ebp, %ebp
	testl	%ecx, %ecx
	jle	.LBB6_8
.LBB6_6:                                # %.lr.ph.i.i
	movslq	%ecx, %rax
	movl	$160, %ecx
	.p2align	4, 0x90
.LBB6_7:                                # =>This Inner Loop Header: Depth=1
	movq	208(%rbx), %rdx
	movups	-80(%rdx,%rcx), %xmm0
	movups	%xmm0, -80(%rbp,%rcx)
	movups	-96(%rdx,%rcx), %xmm0
	movups	%xmm0, -96(%rbp,%rcx)
	movups	-160(%rdx,%rcx), %xmm0
	movups	-144(%rdx,%rcx), %xmm4
	movups	-128(%rdx,%rcx), %xmm2
	movups	-112(%rdx,%rcx), %xmm3
	movups	%xmm3, -112(%rbp,%rcx)
	movups	%xmm2, -128(%rbp,%rcx)
	movups	%xmm4, -144(%rbp,%rcx)
	movups	%xmm0, -160(%rbp,%rcx)
	movups	-64(%rdx,%rcx), %xmm0
	movups	%xmm0, -64(%rbp,%rcx)
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%rbp,%rcx)
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%rbp,%rcx)
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%rbp,%rcx)
	movups	112(%rdx,%rcx), %xmm0
	movups	%xmm0, 112(%rbp,%rcx)
	movups	96(%rdx,%rcx), %xmm0
	movups	%xmm0, 96(%rbp,%rcx)
	movups	80(%rdx,%rcx), %xmm0
	movups	%xmm0, 80(%rbp,%rcx)
	movups	64(%rdx,%rcx), %xmm0
	movups	%xmm0, 64(%rbp,%rcx)
	movups	(%rdx,%rcx), %xmm0
	movups	16(%rdx,%rcx), %xmm4
	movups	32(%rdx,%rcx), %xmm2
	movups	48(%rdx,%rcx), %xmm3
	movups	%xmm3, 48(%rbp,%rcx)
	movups	%xmm2, 32(%rbp,%rcx)
	movups	%xmm4, 16(%rbp,%rcx)
	movups	%xmm0, (%rbp,%rcx)
	addq	$288, %rcx              # imm = 0x120
	decq	%rax
	jne	.LBB6_7
.LBB6_8:                                # %_ZNK20btAlignedObjectArrayI11btWheelInfoE4copyEiiPS0_.exit.i
	movq	208(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_12
# BB#9:
	cmpb	$0, 216(%rbx)
	je	.LBB6_11
# BB#10:
	movl	%r9d, 8(%rsp)           # 4-byte Spill
	movl	%esi, %r13d
	movl	%r14d, 20(%rsp)         # 4-byte Spill
	movl	%r8d, %r14d
	callq	_Z21btAlignedFreeInternalPv
	movl	%r14d, %r8d
	movl	20(%rsp), %r14d         # 4-byte Reload
	movl	%r13d, %esi
	movss	12(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movl	8(%rsp), %r9d           # 4-byte Reload
.LBB6_11:
	movq	$0, 208(%rbx)
.LBB6_12:                               # %_ZN20btAlignedObjectArrayI11btWheelInfoE10deallocateEv.exit.i
	movb	$1, 216(%rbx)
	movq	%rbp, 208(%rbx)
	movl	%r12d, 200(%rbx)
	movl	196(%rbx), %ecx
	movl	28(%rsp), %edx          # 4-byte Reload
	movl	24(%rsp), %edi          # 4-byte Reload
.LBB6_13:                               # %_ZN20btAlignedObjectArrayI11btWheelInfoE9push_backERKS0_.exit
	movq	208(%rbx), %rax
	movslq	%ecx, %rcx
	leaq	(%rcx,%rcx,8), %rcx
	shlq	$5, %rcx
	movups	200(%rsp), %xmm0
	movups	%xmm0, 80(%rax,%rcx)
	movups	184(%rsp), %xmm0
	movups	%xmm0, 64(%rax,%rcx)
	movups	120(%rsp), %xmm0
	movups	136(%rsp), %xmm4
	movups	152(%rsp), %xmm2
	movups	168(%rsp), %xmm3
	movups	%xmm3, 48(%rax,%rcx)
	movups	%xmm2, 32(%rax,%rcx)
	movups	%xmm4, 16(%rax,%rcx)
	movups	%xmm0, (%rax,%rcx)
	movups	216(%rsp), %xmm0
	movups	%xmm0, 96(%rax,%rcx)
	movups	232(%rsp), %xmm0
	movups	%xmm0, 112(%rax,%rcx)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 128(%rax,%rcx)
	movups	264(%rsp), %xmm0
	movups	%xmm0, 144(%rax,%rcx)
	movups	(%r15), %xmm0
	movups	16(%r15), %xmm3
	movups	32(%r15), %xmm2
	movups	%xmm2, 192(%rax,%rcx)
	movups	%xmm3, 176(%rax,%rcx)
	movups	%xmm0, 160(%rax,%rcx)
	movss	%xmm5, 208(%rax,%rcx)
	movl	%r8d, 212(%rax,%rcx)
	movss	%xmm1, 216(%rax,%rcx)
	movl	%esi, 220(%rax,%rcx)
	movl	%edx, 224(%rax,%rcx)
	movl	%edi, 228(%rax,%rcx)
	movl	%r14d, 232(%rax,%rcx)
	movaps	.LCPI6_0(%rip), %xmm0   # xmm0 = [0.000000e+00,0.000000e+00,0.000000e+00,1.000000e-01]
	movups	%xmm0, 236(%rax,%rcx)
	movl	$0, 252(%rax,%rcx)
	movl	$0, 256(%rax,%rcx)
	movb	%r9b, 260(%rax,%rcx)
	movups	93(%rsp), %xmm0
	movups	104(%rsp), %xmm1
	movups	%xmm1, 272(%rax,%rcx)
	movups	%xmm0, 261(%rax,%rcx)
	movslq	196(%rbx), %rax
	leal	1(%rax), %ecx
	movl	%ecx, 196(%rbx)
	leaq	(%rax,%rax,8), %rbp
	shlq	$5, %rbp
	addq	208(%rbx), %rbp
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	_ZN16btRaycastVehicle23updateWheelTransformsWSER11btWheelInfob
	movl	196(%rbx), %esi
	decl	%esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	_ZN16btRaycastVehicle20updateWheelTransformEib
	movq	%rbp, %rax
	addq	$328, %rsp              # imm = 0x148
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN16btRaycastVehicle8addWheelERK9btVector3S2_S2_ffRKNS_15btVehicleTuningEb, .Lfunc_end6-_ZN16btRaycastVehicle8addWheelERK9btVector3S2_S2_ffRKNS_15btVehicleTuningEb
	.cfi_endproc

	.globl	_ZN16btRaycastVehicle23updateWheelTransformsWSER11btWheelInfob
	.p2align	4, 0x90
	.type	_ZN16btRaycastVehicle23updateWheelTransformsWSER11btWheelInfob,@function
_ZN16btRaycastVehicle23updateWheelTransformsWSER11btWheelInfob: # @_ZN16btRaycastVehicle23updateWheelTransformsWSER11btWheelInfob
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 16
	subq	$64, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 80
.Lcfi30:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movb	$0, 84(%rbx)
	movq	168(%rdi), %rax
	movups	8(%rax), %xmm0
	movaps	%xmm0, (%rsp)
	movups	24(%rax), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	40(%rax), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	56(%rax), %xmm0
	movaps	%xmm0, 48(%rsp)
	testb	%dl, %dl
	je	.LBB7_3
# BB#1:
	movq	512(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB7_3
# BB#2:
	movq	(%rdi), %rax
	movq	%rsp, %rsi
	callq	*16(%rax)
.LBB7_3:
	movss	160(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	164(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	168(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	(%rsp), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm8          # xmm8 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movaps	%xmm5, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm1, %xmm2
	movss	20(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm8    # xmm8 = xmm8[0],xmm3[0],xmm8[1],xmm3[1]
	movaps	%xmm6, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm8, %xmm3
	addps	%xmm2, %xmm3
	movss	24(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1]
	movaps	%xmm0, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm2, %xmm7
	addps	%xmm3, %xmm7
	addps	48(%rsp), %xmm7
	movss	32(%rsp), %xmm9         # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm5
	movss	36(%rsp), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm6
	addss	%xmm5, %xmm6
	movss	40(%rsp), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm0
	addss	%xmm6, %xmm0
	addss	56(%rsp), %xmm0
	xorps	%xmm6, %xmm6
	xorps	%xmm3, %xmm3
	movss	%xmm0, %xmm3            # xmm3 = xmm0[0],xmm3[1,2,3]
	movlps	%xmm7, 36(%rbx)
	movlps	%xmm3, 44(%rbx)
	movss	176(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	180(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	184(%rbx), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm1, %xmm0
	movaps	%xmm5, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm8, %xmm3
	addps	%xmm0, %xmm3
	movaps	%xmm7, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	addps	%xmm3, %xmm0
	mulss	%xmm9, %xmm4
	mulss	%xmm10, %xmm5
	addss	%xmm4, %xmm5
	mulss	%xmm11, %xmm7
	addss	%xmm5, %xmm7
	xorps	%xmm3, %xmm3
	movss	%xmm7, %xmm3            # xmm3 = xmm7[0],xmm3[1,2,3]
	movlps	%xmm0, 52(%rbx)
	movlps	%xmm3, 60(%rbx)
	movss	192(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	196(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	200(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm1, %xmm5
	movaps	%xmm3, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm8, %xmm1
	addps	%xmm5, %xmm1
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm2, %xmm5
	addps	%xmm1, %xmm5
	mulss	%xmm9, %xmm0
	mulss	%xmm10, %xmm3
	addss	%xmm0, %xmm3
	mulss	%xmm11, %xmm4
	addss	%xmm3, %xmm4
	movss	%xmm4, %xmm6            # xmm6 = xmm4[0],xmm6[1,2,3]
	movlps	%xmm5, 68(%rbx)
	movlps	%xmm6, 76(%rbx)
	addq	$64, %rsp
	popq	%rbx
	retq
.Lfunc_end7:
	.size	_ZN16btRaycastVehicle23updateWheelTransformsWSER11btWheelInfob, .Lfunc_end7-_ZN16btRaycastVehicle23updateWheelTransformsWSER11btWheelInfob
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI8_1:
	.long	1065353216              # float 1
.LCPI8_2:
	.long	1056964608              # float 0.5
.LCPI8_3:
	.long	1073741824              # float 2
.LCPI8_4:
	.long	3204448256              # float -0.5
	.text
	.globl	_ZN16btRaycastVehicle20updateWheelTransformEib
	.p2align	4, 0x90
	.type	_ZN16btRaycastVehicle20updateWheelTransformEib,@function
_ZN16btRaycastVehicle20updateWheelTransformEib: # @_ZN16btRaycastVehicle20updateWheelTransformEib
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 40
	subq	$168, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 208
.Lcfi36:
	.cfi_offset %rbx, -40
.Lcfi37:
	.cfi_offset %r12, -32
.Lcfi38:
	.cfi_offset %r14, -24
.Lcfi39:
	.cfi_offset %r15, -16
	movq	208(%rdi), %r14
	movslq	%esi, %rax
	leaq	(%rax,%rax,8), %rbx
	shlq	$5, %rbx
	leaq	(%r14,%rbx), %rsi
	movzbl	%dl, %edx
	callq	_ZN16btRaycastVehicle23updateWheelTransformsWSER11btWheelInfob
	movss	52(%r14,%rbx), %xmm8    # xmm8 = mem[0],zero,zero,zero
	movss	56(%r14,%rbx), %xmm7    # xmm7 = mem[0],zero,zero,zero
	movaps	.LCPI8_0(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm8, %xmm1
	xorps	%xmm0, %xmm1
	movaps	%xmm1, 128(%rsp)        # 16-byte Spill
	movaps	%xmm7, %xmm1
	xorps	%xmm0, %xmm1
	movaps	%xmm1, 112(%rsp)        # 16-byte Spill
	movss	60(%r14,%rbx), %xmm6    # xmm6 = mem[0],zero,zero,zero
	xorps	%xmm6, %xmm0
	movaps	%xmm0, 144(%rsp)        # 16-byte Spill
	movss	76(%r14,%rbx), %xmm4    # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm0
	mulss	%xmm7, %xmm0
	movss	72(%r14,%rbx), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm5
	mulss	%xmm6, %xmm5
	subss	%xmm0, %xmm5
	movss	68(%r14,%rbx), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm6, %xmm0
	mulss	%xmm8, %xmm4
	subss	%xmm0, %xmm4
	mulss	%xmm8, %xmm1
	mulss	%xmm7, %xmm3
	subss	%xmm1, %xmm3
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm4, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB8_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movss	%xmm3, 20(%rsp)         # 4-byte Spill
	movss	%xmm4, 16(%rsp)         # 4-byte Spill
	movss	%xmm5, 12(%rsp)         # 4-byte Spill
	movaps	%xmm6, 32(%rsp)         # 16-byte Spill
	movaps	%xmm7, 48(%rsp)         # 16-byte Spill
	movaps	%xmm8, 96(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	96(%rsp), %xmm8         # 16-byte Reload
	movaps	48(%rsp), %xmm7         # 16-byte Reload
	movaps	32(%rsp), %xmm6         # 16-byte Reload
	movss	12(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
.LBB8_2:                                # %.split
	leaq	76(%r14,%rbx), %r12
	movss	.LCPI8_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movss	236(%r14,%rbx), %xmm2   # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm8
	mulss	%xmm7, %xmm7
	addss	%xmm8, %xmm7
	mulss	%xmm6, %xmm6
	addss	%xmm7, %xmm6
	xorps	%xmm0, %xmm0
	sqrtss	%xmm6, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB8_4
# BB#3:                                 # %call.sqrt220
	movss	%xmm2, 32(%rsp)         # 4-byte Spill
	movaps	%xmm6, %xmm0
	movss	%xmm3, 20(%rsp)         # 4-byte Spill
	movss	%xmm4, 16(%rsp)         # 4-byte Spill
	movss	%xmm5, 12(%rsp)         # 4-byte Spill
	movss	%xmm1, 48(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	48(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
.LBB8_4:                                # %.split.split
	movss	%xmm0, 48(%rsp)         # 4-byte Spill
	leaq	52(%r14,%rbx), %r15
	mulss	%xmm1, %xmm5
	movss	%xmm5, 12(%rsp)         # 4-byte Spill
	mulss	%xmm1, %xmm4
	movss	%xmm4, 16(%rsp)         # 4-byte Spill
	mulss	%xmm1, %xmm3
	movss	%xmm3, 20(%rsp)         # 4-byte Spill
	mulss	.LCPI8_2(%rip), %xmm2
	movaps	%xmm2, %xmm0
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	callq	sinf
	divss	48(%rsp), %xmm0         # 4-byte Folded Reload
	movaps	%xmm0, %xmm1
	mulss	128(%rsp), %xmm1        # 16-byte Folded Reload
	movss	%xmm1, 48(%rsp)         # 4-byte Spill
	movaps	%xmm0, %xmm1
	mulss	112(%rsp), %xmm1        # 16-byte Folded Reload
	movss	%xmm1, 96(%rsp)         # 4-byte Spill
	mulss	144(%rsp), %xmm0        # 16-byte Folded Reload
	movss	%xmm0, 24(%rsp)         # 4-byte Spill
	movss	32(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	cosf
	movss	48(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	mulss	%xmm1, %xmm1
	movss	96(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movss	24(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	movaps	%xmm0, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movss	.LCPI8_3(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	divss	%xmm2, %xmm3
	movaps	%xmm5, %xmm1
	mulss	%xmm3, %xmm1
	movaps	%xmm6, %xmm2
	movaps	%xmm6, %xmm7
	mulss	%xmm3, %xmm2
	mulss	%xmm4, %xmm3
	movaps	%xmm4, %xmm6
	movaps	%xmm0, %xmm8
	mulss	%xmm1, %xmm8
	movaps	%xmm0, %xmm9
	mulss	%xmm2, %xmm9
	mulss	%xmm3, %xmm0
	mulss	%xmm5, %xmm1
	movaps	%xmm5, %xmm4
	mulss	%xmm2, %xmm4
	mulss	%xmm3, %xmm5
	movaps	%xmm5, %xmm10
	movaps	%xmm7, %xmm5
	mulss	%xmm5, %xmm2
	mulss	%xmm3, %xmm5
	movaps	%xmm5, %xmm11
	mulss	%xmm6, %xmm3
	movaps	%xmm2, %xmm6
	addss	%xmm3, %xmm6
	movss	.LCPI8_1(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm5
	subss	%xmm6, %xmm5
	movss	%xmm5, 68(%rsp)         # 4-byte Spill
	movaps	%xmm4, %xmm6
	subss	%xmm0, %xmm6
	movss	%xmm6, 72(%rsp)         # 4-byte Spill
	movaps	%xmm10, %xmm6
	addss	%xmm9, %xmm6
	movss	%xmm6, 92(%rsp)         # 4-byte Spill
	addss	%xmm0, %xmm4
	movss	%xmm4, 84(%rsp)         # 4-byte Spill
	addss	%xmm1, %xmm3
	movaps	%xmm7, %xmm0
	subss	%xmm3, %xmm0
	movss	%xmm0, 76(%rsp)         # 4-byte Spill
	movaps	%xmm11, %xmm0
	subss	%xmm8, %xmm0
	movss	%xmm0, 88(%rsp)         # 4-byte Spill
	subss	%xmm9, %xmm10
	movss	%xmm10, 48(%rsp)        # 4-byte Spill
	addss	%xmm8, %xmm11
	movss	%xmm11, 96(%rsp)        # 4-byte Spill
	addss	%xmm1, %xmm2
	subss	%xmm2, %xmm7
	movss	%xmm7, 80(%rsp)         # 4-byte Spill
	movss	240(%r14,%rbx), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movss	-8(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	movss	-4(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	(%r12), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB8_6
# BB#5:                                 # %call.sqrt221
	movss	%xmm2, 8(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB8_6:                                # %.split.split.split
	movss	%xmm1, 32(%rsp)         # 4-byte Spill
	mulss	.LCPI8_4(%rip), %xmm2
	movss	%xmm2, 8(%rsp)          # 4-byte Spill
	movaps	%xmm2, %xmm0
	callq	sinf
	divss	32(%rsp), %xmm0         # 4-byte Folded Reload
	movss	-8(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 24(%rsp)         # 4-byte Spill
	movss	-4(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 32(%rsp)         # 4-byte Spill
	mulss	(%r12), %xmm0
	movss	%xmm0, 28(%rsp)         # 4-byte Spill
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	cosf
	movss	24(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	mulss	%xmm1, %xmm1
	movss	32(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movss	28(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	movaps	%xmm0, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movss	.LCPI8_3(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	divss	%xmm2, %xmm7
	movaps	%xmm5, %xmm1
	mulss	%xmm7, %xmm1
	movaps	%xmm6, %xmm3
	movaps	%xmm6, %xmm9
	mulss	%xmm7, %xmm3
	mulss	%xmm4, %xmm7
	movaps	%xmm4, %xmm8
	movaps	%xmm0, %xmm4
	mulss	%xmm1, %xmm4
	movaps	%xmm0, %xmm6
	mulss	%xmm3, %xmm6
	mulss	%xmm7, %xmm0
	mulss	%xmm5, %xmm1
	movaps	%xmm5, %xmm2
	mulss	%xmm3, %xmm2
	mulss	%xmm7, %xmm5
	movaps	%xmm5, %xmm15
	movaps	%xmm9, %xmm5
	mulss	%xmm5, %xmm3
	mulss	%xmm7, %xmm5
	movaps	%xmm5, %xmm9
	mulss	%xmm8, %xmm7
	movaps	%xmm3, %xmm5
	addss	%xmm7, %xmm5
	movss	.LCPI8_1(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm12
	subss	%xmm5, %xmm12
	movaps	%xmm2, %xmm5
	subss	%xmm0, %xmm5
	movaps	%xmm5, %xmm10
	movss	%xmm10, 28(%rsp)        # 4-byte Spill
	addss	%xmm0, %xmm2
	addss	%xmm1, %xmm7
	movaps	%xmm11, %xmm5
	subss	%xmm7, %xmm5
	movaps	%xmm15, %xmm0
	addss	%xmm6, %xmm0
	movaps	%xmm0, %xmm8
	movss	%xmm8, 8(%rsp)          # 4-byte Spill
	subss	%xmm6, %xmm15
	movaps	%xmm9, %xmm0
	movaps	%xmm0, %xmm14
	subss	%xmm4, %xmm14
	addss	%xmm4, %xmm0
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	addss	%xmm1, %xmm3
	subss	%xmm3, %xmm11
	movss	68(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm12, %xmm0
	movss	72(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm1
	mulss	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	movss	92(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm9
	mulss	%xmm15, %xmm9
	addss	%xmm1, %xmm9
	movaps	%xmm7, %xmm1
	mulss	%xmm10, %xmm1
	movaps	%xmm6, %xmm4
	mulss	%xmm5, %xmm4
	addss	%xmm1, %xmm4
	movaps	%xmm13, %xmm3
	movss	32(%rsp), %xmm10        # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm3
	addss	%xmm4, %xmm3
	mulss	%xmm8, %xmm7
	mulss	%xmm14, %xmm6
	addss	%xmm7, %xmm6
	mulss	%xmm11, %xmm13
	addss	%xmm6, %xmm13
	movss	84(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm1
	mulss	%xmm12, %xmm1
	movss	76(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm4
	mulss	%xmm2, %xmm4
	addss	%xmm1, %xmm4
	movss	88(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm8
	mulss	%xmm15, %xmm8
	addss	%xmm4, %xmm8
	movaps	%xmm6, %xmm4
	mulss	28(%rsp), %xmm4         # 4-byte Folded Reload
	movaps	%xmm0, %xmm7
	mulss	%xmm5, %xmm7
	addss	%xmm4, %xmm7
	movaps	%xmm1, %xmm4
	mulss	%xmm10, %xmm4
	addss	%xmm7, %xmm4
	mulss	8(%rsp), %xmm6          # 4-byte Folded Reload
	mulss	%xmm14, %xmm0
	addss	%xmm6, %xmm0
	mulss	%xmm11, %xmm1
	addss	%xmm0, %xmm1
	movss	48(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm12
	movss	96(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	addss	%xmm12, %xmm2
	movss	80(%rsp), %xmm10        # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm15
	addss	%xmm2, %xmm15
	movss	%xmm15, 24(%rsp)        # 4-byte Spill
	movss	28(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	mulss	%xmm6, %xmm5
	addss	%xmm0, %xmm5
	movss	32(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm15
	addss	%xmm5, %xmm15
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	mulss	%xmm6, %xmm14
	addss	%xmm0, %xmm14
	mulss	%xmm10, %xmm11
	addss	%xmm14, %xmm11
	movss	-8(%r12), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm2
	mulss	%xmm9, %xmm2
	movss	-4(%r12), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm6
	mulss	%xmm3, %xmm6
	addss	%xmm2, %xmm6
	movss	(%r12), %xmm10          # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm0
	mulss	%xmm13, %xmm0
	addss	%xmm6, %xmm0
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	movss	12(%rsp), %xmm14        # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm6
	mulss	%xmm9, %xmm6
	movss	16(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm5
	mulss	%xmm3, %xmm5
	addss	%xmm6, %xmm5
	movss	20(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	mulss	%xmm13, %xmm0
	addss	%xmm5, %xmm0
	movss	%xmm0, 48(%rsp)         # 4-byte Spill
	mulss	128(%rsp), %xmm9        # 16-byte Folded Reload
	mulss	112(%rsp), %xmm3        # 16-byte Folded Reload
	addss	%xmm9, %xmm3
	mulss	144(%rsp), %xmm13       # 16-byte Folded Reload
	addss	%xmm3, %xmm13
	movaps	%xmm12, %xmm0
	mulss	%xmm8, %xmm0
	movaps	%xmm7, %xmm3
	mulss	%xmm4, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm10, %xmm0
	mulss	%xmm1, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm14, %xmm3
	mulss	%xmm8, %xmm3
	movaps	%xmm2, %xmm5
	mulss	%xmm4, %xmm5
	addss	%xmm3, %xmm5
	movaps	%xmm6, %xmm3
	mulss	%xmm1, %xmm3
	addss	%xmm5, %xmm3
	movaps	128(%rsp), %xmm5        # 16-byte Reload
	mulss	%xmm5, %xmm8
	movaps	112(%rsp), %xmm9        # 16-byte Reload
	mulss	%xmm9, %xmm4
	addss	%xmm8, %xmm4
	movaps	144(%rsp), %xmm8        # 16-byte Reload
	mulss	%xmm8, %xmm1
	addss	%xmm4, %xmm1
	movss	24(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm12
	mulss	%xmm15, %xmm7
	addss	%xmm12, %xmm7
	mulss	%xmm11, %xmm10
	addss	%xmm7, %xmm10
	mulss	%xmm4, %xmm14
	mulss	%xmm15, %xmm2
	addss	%xmm14, %xmm2
	mulss	%xmm11, %xmm6
	addss	%xmm2, %xmm6
	mulss	%xmm5, %xmm4
	mulss	%xmm9, %xmm15
	addss	%xmm4, %xmm15
	mulss	%xmm8, %xmm11
	addss	%xmm15, %xmm11
	movss	32(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 96(%r14,%rbx)
	movss	48(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 100(%r14,%rbx)
	movss	%xmm13, 104(%r14,%rbx)
	movl	$0, 108(%r14,%rbx)
	movss	%xmm0, 112(%r14,%rbx)
	movss	%xmm3, 116(%r14,%rbx)
	movss	%xmm1, 120(%r14,%rbx)
	movl	$0, 124(%r14,%rbx)
	movss	%xmm10, 128(%r14,%rbx)
	movss	%xmm6, 132(%r14,%rbx)
	movss	%xmm11, 136(%r14,%rbx)
	movl	$0, 140(%r14,%rbx)
	movss	32(%r14,%rbx), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movsd	(%r15), %xmm1           # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm1, %xmm2
	mulss	8(%r15), %xmm0
	movsd	36(%r14,%rbx), %xmm1    # xmm1 = mem[0],zero
	addps	%xmm2, %xmm1
	addss	44(%r14,%rbx), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, 144(%r14,%rbx)
	movlps	%xmm2, 152(%r14,%rbx)
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	_ZN16btRaycastVehicle20updateWheelTransformEib, .Lfunc_end8-_ZN16btRaycastVehicle20updateWheelTransformEib
	.cfi_endproc

	.globl	_ZNK16btRaycastVehicle19getWheelTransformWSEi
	.p2align	4, 0x90
	.type	_ZNK16btRaycastVehicle19getWheelTransformWSEi,@function
_ZNK16btRaycastVehicle19getWheelTransformWSEi: # @_ZNK16btRaycastVehicle19getWheelTransformWSEi
	.cfi_startproc
# BB#0:
	movq	208(%rdi), %rax
	movslq	%esi, %rcx
	leaq	(%rcx,%rcx,8), %rcx
	shlq	$5, %rcx
	leaq	96(%rax,%rcx), %rax
	retq
.Lfunc_end9:
	.size	_ZNK16btRaycastVehicle19getWheelTransformWSEi, .Lfunc_end9-_ZNK16btRaycastVehicle19getWheelTransformWSEi
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI10_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN16btRaycastVehicle15resetSuspensionEv
	.p2align	4, 0x90
	.type	_ZN16btRaycastVehicle15resetSuspensionEv,@function
_ZN16btRaycastVehicle15resetSuspensionEv: # @_ZN16btRaycastVehicle15resetSuspensionEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 48
.Lcfi45:
	.cfi_offset %rbx, -40
.Lcfi46:
	.cfi_offset %r12, -32
.Lcfi47:
	.cfi_offset %r14, -24
.Lcfi48:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	cmpl	$0, 196(%r14)
	jle	.LBB10_3
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	movq	208(%r14), %r12
	leaq	(%r12,%rbx), %rdi
	callq	_ZNK11btWheelInfo23getSuspensionRestLengthEv
	movss	%xmm0, 32(%r12,%rbx)
	movl	$0, 276(%r12,%rbx)
	movsd	52(%r12,%rbx), %xmm0    # xmm0 = mem[0],zero
	movaps	.LCPI10_0(%rip), %xmm1  # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm1, %xmm2
	xorps	%xmm2, %xmm0
	movss	60(%r12,%rbx), %xmm1    # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, (%r12,%rbx)
	movlps	%xmm2, 8(%r12,%rbx)
	movl	$1065353216, 272(%r12,%rbx) # imm = 0x3F800000
	incq	%r15
	movslq	196(%r14), %rax
	addq	$288, %rbx              # imm = 0x120
	cmpq	%rax, %r15
	jl	.LBB10_2
.LBB10_3:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end10:
	.size	_ZN16btRaycastVehicle15resetSuspensionEv, .Lfunc_end10-_ZN16btRaycastVehicle15resetSuspensionEv
	.cfi_endproc

	.globl	_ZNK16btRaycastVehicle24getChassisWorldTransformEv
	.p2align	4, 0x90
	.type	_ZNK16btRaycastVehicle24getChassisWorldTransformEv,@function
_ZNK16btRaycastVehicle24getChassisWorldTransformEv: # @_ZNK16btRaycastVehicle24getChassisWorldTransformEv
	.cfi_startproc
# BB#0:
	movq	168(%rdi), %rax
	addq	$8, %rax
	retq
.Lfunc_end11:
	.size	_ZNK16btRaycastVehicle24getChassisWorldTransformEv, .Lfunc_end11-_ZNK16btRaycastVehicle24getChassisWorldTransformEv
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI12_0:
	.long	1008981770              # float 0.00999999977
.LCPI12_1:
	.long	1092616192              # float 10
.LCPI12_2:
	.long	3184315597              # float -0.100000001
.LCPI12_3:
	.long	3212836864              # float -1
.LCPI12_5:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI12_4:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN16btRaycastVehicle7rayCastER11btWheelInfo
	.p2align	4, 0x90
	.type	_ZN16btRaycastVehicle7rayCastER11btWheelInfo,@function
_ZN16btRaycastVehicle7rayCastER11btWheelInfo: # @_ZN16btRaycastVehicle7rayCastER11btWheelInfo
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 32
	subq	$80, %rsp
.Lcfi52:
	.cfi_def_cfa_offset 112
.Lcfi53:
	.cfi_offset %rbx, -32
.Lcfi54:
	.cfi_offset %r14, -24
.Lcfi55:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	xorl	%edx, %edx
	callq	_ZN16btRaycastVehicle23updateWheelTransformsWSER11btWheelInfob
	movq	%rbx, %rdi
	callq	_ZNK11btWheelInfo23getSuspensionRestLengthEv
	addss	216(%rbx), %xmm0
	movsd	52(%rbx), %xmm2         # xmm2 = mem[0],zero
	movaps	%xmm0, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	movss	60(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	mulss	%xmm0, %xmm3
	leaq	36(%rbx), %rsi
	movsd	36(%rbx), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	addss	44(%rbx), %xmm3
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	leaq	16(%rbx), %r15
	movlps	%xmm2, 16(%rbx)
	movlps	%xmm1, 24(%rbx)
	movl	$-1082130432, 72(%rsp)  # imm = 0xBF800000
	movq	144(%r14), %rdi
	movq	(%rdi), %rax
	leaq	40(%rsp), %rcx
	movq	%r15, %rdx
	callq	*16(%rax)
	movq	$0, 88(%rbx)
	testq	%rax, %rax
	je	.LBB12_9
# BB#1:
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	mulss	72(%rsp), %xmm1
	movups	56(%rsp), %xmm0
	movups	%xmm0, (%rbx)
	movb	$1, 84(%rbx)
	movq	$_ZL13s_fixedObject, 88(%rbx)
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	movaps	%xmm1, %xmm0
	subss	216(%rbx), %xmm0
	movss	%xmm0, 32(%rbx)
	movq	%rbx, %rdi
	callq	_ZNK11btWheelInfo23getSuspensionRestLengthEv
	movss	212(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	.LCPI12_0(%rip), %xmm1
	subss	%xmm1, %xmm0
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	%rbx, %rdi
	callq	_ZNK11btWheelInfo23getSuspensionRestLengthEv
	movss	12(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI12_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	mulss	212(%rbx), %xmm1
	addss	%xmm0, %xmm1
	movss	32(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm2
	jbe	.LBB12_3
# BB#2:
	movss	%xmm2, 32(%rbx)
	movaps	%xmm2, %xmm0
.LBB12_3:
	ucomiss	%xmm1, %xmm0
	jbe	.LBB12_5
# BB#4:
	movss	%xmm1, 32(%rbx)
.LBB12_5:
	movups	40(%rsp), %xmm0
	movups	%xmm0, (%r15)
	movss	(%rbx), %xmm10          # xmm10 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	52(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm0
	movss	56(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	addss	%xmm0, %xmm4
	movss	8(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	60(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	addss	%xmm4, %xmm3
	ucomiss	.LCPI12_2(%rip), %xmm3
	jae	.LBB12_6
# BB#7:
	movq	168(%r14), %rax
	movss	16(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	56(%rax), %xmm4
	movss	20(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	60(%rax), %xmm5
	movss	24(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	subss	64(%rax), %xmm6
	movss	344(%rax), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movss	348(%rax), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movss	352(%rax), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	mulss	%xmm7, %xmm0
	mulss	%xmm4, %xmm7
	mulss	%xmm9, %xmm4
	mulss	%xmm6, %xmm9
	subss	%xmm0, %xmm9
	addss	328(%rax), %xmm9
	mulss	%xmm9, %xmm10
	mulss	%xmm8, %xmm6
	subss	%xmm6, %xmm7
	addss	332(%rax), %xmm7
	mulss	%xmm7, %xmm1
	addss	%xmm10, %xmm1
	mulss	%xmm8, %xmm5
	subss	%xmm4, %xmm5
	addss	336(%rax), %xmm5
	mulss	%xmm5, %xmm2
	addss	%xmm1, %xmm2
	movss	.LCPI12_3(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	%xmm3, %xmm1
	mulss	%xmm1, %xmm2
	jmp	.LBB12_8
.LBB12_9:
	movq	%rbx, %rdi
	callq	_ZNK11btWheelInfo23getSuspensionRestLengthEv
	movss	%xmm0, 32(%rbx)
	movl	$0, 276(%rbx)
	movsd	52(%rbx), %xmm0         # xmm0 = mem[0],zero
	movaps	.LCPI12_4(%rip), %xmm1  # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm1, %xmm0
	movss	60(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movlps	%xmm0, (%rbx)
	movlps	%xmm1, 8(%rbx)
	movss	.LCPI12_3(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movss	.LCPI12_5(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	jmp	.LBB12_10
.LBB12_6:
	movss	.LCPI12_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
.LBB12_8:
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	movss	%xmm2, 276(%rbx)
.LBB12_10:
	movss	%xmm1, 272(%rbx)
	addq	$80, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	_ZN16btRaycastVehicle7rayCastER11btWheelInfo, .Lfunc_end12-_ZN16btRaycastVehicle7rayCastER11btWheelInfo
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI13_0:
	.long	1080452710              # float 3.5999999
.LCPI13_2:
	.long	1065353216              # float 1
.LCPI13_3:
	.long	1169915904              # float 6000
.LCPI13_4:
	.long	1065185444              # float 0.990000009
.LCPI13_5:
	.long	0                       # float 0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI13_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN16btRaycastVehicle13updateVehicleEf
	.p2align	4, 0x90
	.type	_ZN16btRaycastVehicle13updateVehicleEf,@function
_ZN16btRaycastVehicle13updateVehicleEf: # @_ZN16btRaycastVehicle13updateVehicleEf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 48
	subq	$64, %rsp
.Lcfi61:
	.cfi_def_cfa_offset 112
.Lcfi62:
	.cfi_offset %rbx, -48
.Lcfi63:
	.cfi_offset %r12, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movaps	%xmm0, (%rsp)           # 16-byte Spill
	movq	%rdi, %r12
	cmpl	$0, 196(%r12)
	jle	.LBB13_3
# BB#1:                                 # %.lr.ph131.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_2:                               # %.lr.ph131
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	%ebp, %esi
	callq	_ZN16btRaycastVehicle20updateWheelTransformEib
	incl	%ebp
	cmpl	196(%r12), %ebp
	jl	.LBB13_2
.LBB13_3:                               # %._crit_edge132
	movq	168(%r12), %rax
	movss	328(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	332(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movss	336(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB13_5
# BB#4:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	callq	sqrtf
.LBB13_5:                               # %._crit_edge132.split
	mulss	.LCPI13_0(%rip), %xmm0
	movss	%xmm0, 160(%r12)
	movq	168(%r12), %rax
	movslq	184(%r12), %rcx
	movss	8(%rax,%rcx,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	24(%rax,%rcx,4), %xmm2  # xmm2 = mem[0],zero,zero,zero
	movss	40(%rax,%rcx,4), %xmm3  # xmm3 = mem[0],zero,zero,zero
	mulss	328(%rax), %xmm1
	mulss	332(%rax), %xmm2
	addss	%xmm1, %xmm2
	mulss	336(%rax), %xmm3
	addss	%xmm2, %xmm3
	xorps	%xmm1, %xmm1
	ucomiss	%xmm3, %xmm1
	jbe	.LBB13_7
# BB#6:
	xorps	.LCPI13_1(%rip), %xmm0
	movss	%xmm0, 160(%r12)
.LBB13_7:                               # %.preheader
	cmpl	$0, 196(%r12)
	movaps	(%rsp), %xmm3           # 16-byte Reload
	jle	.LBB13_18
# BB#8:                                 # %.lr.ph127
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_9:                               # =>This Inner Loop Header: Depth=1
	movq	208(%r12), %rsi
	addq	%rbx, %rsi
	movq	%r12, %rdi
	callq	_ZN16btRaycastVehicle7rayCastER11btWheelInfo
	incq	%rbp
	movslq	196(%r12), %rax
	addq	$288, %rbx              # imm = 0x120
	cmpq	%rax, %rbp
	jl	.LBB13_9
# BB#10:                                # %._crit_edge128
	testl	%eax, %eax
	movaps	(%rsp), %xmm3           # 16-byte Reload
	jle	.LBB13_18
# BB#11:                                # %.lr.ph.i
	movq	168(%r12), %rax
	movss	.LCPI13_2(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	360(%rax), %xmm0
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	xorl	%r14d, %r14d
	movl	$224, %ebx
	.p2align	4, 0x90
.LBB13_12:                              # =>This Inner Loop Header: Depth=1
	movq	208(%r12), %rbp
	cmpb	$0, -140(%rbp,%rbx)
	xorps	%xmm1, %xmm1
	je	.LBB13_14
# BB#13:                                #   in Loop: Header=BB13_12 Depth=1
	leaq	(%rbp,%rbx), %r15
	leaq	-224(%rbp,%rbx), %rdi
	callq	_ZNK11btWheelInfo23getSuspensionRestLengthEv
	movaps	(%rsp), %xmm3           # 16-byte Reload
	subss	-192(%rbp,%rbx), %xmm0
	mulss	-4(%rbp,%rbx), %xmm0
	mulss	48(%rbp,%rbx), %xmm0
	movss	52(%rbp,%rbx), %xmm1    # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	ucomiss	%xmm1, %xmm2
	leaq	4(%rbp,%rbx), %rax
	cmovaq	%r15, %rax
	mulss	(%rax), %xmm1
	subss	%xmm1, %xmm0
	mulss	16(%rsp), %xmm0         # 4-byte Folded Reload
	xorps	%xmm1, %xmm1
	maxss	%xmm0, %xmm1
.LBB13_14:                              #   in Loop: Header=BB13_12 Depth=1
	movss	%xmm1, 56(%rbp,%rbx)
	incq	%r14
	movslq	196(%r12), %rax
	addq	$288, %rbx              # imm = 0x120
	cmpq	%rax, %r14
	jl	.LBB13_12
# BB#15:                                # %_ZN16btRaycastVehicle16updateSuspensionEf.exit.preheader
	testl	%eax, %eax
	jle	.LBB13_18
# BB#16:                                # %.lr.ph125
	movaps	%xmm3, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	xorl	%ebp, %ebp
	leaq	48(%rsp), %r14
	leaq	32(%rsp), %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB13_17:                              # %_ZN16btRaycastVehicle16updateSuspensionEf.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	208(%r12), %rax
	movss	.LCPI13_3(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	minss	280(%rax,%rbp), %xmm0
	movsd	(%rax,%rbp), %xmm1      # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm1, %xmm2
	mulss	8(%rax,%rbp), %xmm0
	mulps	16(%rsp), %xmm2         # 16-byte Folded Reload
	mulss	%xmm3, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, 48(%rsp)
	movlps	%xmm1, 56(%rsp)
	movq	168(%r12), %rdi
	movsd	16(%rax,%rbp), %xmm0    # xmm0 = mem[0],zero
	movsd	56(%rdi), %xmm1         # xmm1 = mem[0],zero
	subps	%xmm1, %xmm0
	movss	24(%rax,%rbp), %xmm1    # xmm1 = mem[0],zero,zero,zero
	subss	64(%rdi), %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 32(%rsp)
	movlps	%xmm2, 40(%rsp)
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	_ZN11btRigidBody12applyImpulseERK9btVector3S2_
	movaps	(%rsp), %xmm3           # 16-byte Reload
	incq	%rbx
	movslq	196(%r12), %rax
	addq	$288, %rbp              # imm = 0x120
	cmpq	%rax, %rbx
	jl	.LBB13_17
.LBB13_18:                              # %_ZN16btRaycastVehicle16updateSuspensionEf.exit._crit_edge
	movq	(%r12), %rax
	movq	%r12, %rdi
	movaps	%xmm3, %xmm0
	callq	*40(%rax)
	movslq	196(%r12), %rax
	testq	%rax, %rax
	jle	.LBB13_24
# BB#19:                                # %.lr.ph
	movq	168(%r12), %rcx
	movq	208(%r12), %rdx
	leaq	244(%rdx), %rsi
	xorl	%edi, %edi
	movss	.LCPI13_4(%rip), %xmm8  # xmm8 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB13_20:                              # =>This Inner Loop Header: Depth=1
	cmpb	$0, -160(%rsi)
	je	.LBB13_22
# BB#21:                                #   in Loop: Header=BB13_20 Depth=1
	movss	-208(%rsi), %xmm3       # xmm3 = mem[0],zero,zero,zero
	movss	-204(%rsi), %xmm1       # xmm1 = mem[0],zero,zero,zero
	subss	56(%rcx), %xmm3
	subss	60(%rcx), %xmm1
	movss	-200(%rsi), %xmm4       # xmm4 = mem[0],zero,zero,zero
	subss	64(%rcx), %xmm4
	movss	344(%rcx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	348(%rcx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	352(%rcx), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm7
	mulss	%xmm9, %xmm7
	mulss	%xmm3, %xmm9
	mulss	%xmm5, %xmm3
	mulss	%xmm4, %xmm5
	subss	%xmm7, %xmm5
	addss	328(%rcx), %xmm5
	mulss	%xmm6, %xmm4
	subss	%xmm4, %xmm9
	addss	332(%rcx), %xmm9
	mulss	%xmm6, %xmm1
	subss	%xmm3, %xmm1
	addss	336(%rcx), %xmm1
	movslq	184(%r12), %rbp
	movss	8(%rcx,%rbp,4), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movss	24(%rcx,%rbp,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movss	40(%rcx,%rbp,4), %xmm7  # xmm7 = mem[0],zero,zero,zero
	movss	-244(%rsi), %xmm11      # xmm11 = mem[0],zero,zero,zero
	movss	-240(%rsi), %xmm10      # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm6
	mulss	%xmm11, %xmm6
	movaps	%xmm0, %xmm4
	mulss	%xmm10, %xmm4
	addss	%xmm6, %xmm4
	movss	-236(%rsi), %xmm6       # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm3
	mulss	%xmm6, %xmm3
	addss	%xmm4, %xmm3
	mulss	%xmm3, %xmm11
	mulss	%xmm3, %xmm10
	mulss	%xmm6, %xmm3
	subss	%xmm11, %xmm2
	subss	%xmm10, %xmm0
	subss	%xmm3, %xmm7
	mulss	%xmm5, %xmm2
	mulss	%xmm9, %xmm0
	addss	%xmm2, %xmm0
	mulss	%xmm1, %xmm7
	addss	%xmm0, %xmm7
	mulss	(%rsp), %xmm7           # 16-byte Folded Reload
	divss	-28(%rsi), %xmm7
	movss	%xmm7, (%rsi)
	movq	%rsi, %rbp
	jmp	.LBB13_23
	.p2align	4, 0x90
.LBB13_22:                              #   in Loop: Header=BB13_20 Depth=1
	leaq	(%rdi,%rdi,8), %rbp
	shlq	$5, %rbp
	leaq	244(%rdx,%rbp), %rbp
	movss	(%rsi), %xmm7           # xmm7 = mem[0],zero,zero,zero
.LBB13_23:                              #   in Loop: Header=BB13_20 Depth=1
	movss	-4(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm0
	movss	%xmm0, -4(%rsi)
	mulss	%xmm8, %xmm7
	movss	%xmm7, (%rbp)
	incq	%rdi
	addq	$288, %rsi              # imm = 0x120
	cmpq	%rax, %rdi
	jl	.LBB13_20
.LBB13_24:                              # %._crit_edge
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	_ZN16btRaycastVehicle13updateVehicleEf, .Lfunc_end13-_ZN16btRaycastVehicle13updateVehicleEf
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI14_0:
	.long	1065353216              # float 1
.LCPI14_1:
	.long	0                       # float 0
	.text
	.globl	_ZN16btRaycastVehicle16updateSuspensionEf
	.p2align	4, 0x90
	.type	_ZN16btRaycastVehicle16updateSuspensionEf,@function
_ZN16btRaycastVehicle16updateSuspensionEf: # @_ZN16btRaycastVehicle16updateSuspensionEf
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi69:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi70:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi72:
	.cfi_def_cfa_offset 64
.Lcfi73:
	.cfi_offset %rbx, -48
.Lcfi74:
	.cfi_offset %r12, -40
.Lcfi75:
	.cfi_offset %r13, -32
.Lcfi76:
	.cfi_offset %r14, -24
.Lcfi77:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	cmpl	$0, 196(%r14)
	jle	.LBB14_5
# BB#1:                                 # %.lr.ph
	movq	168(%r14), %rax
	movss	.LCPI14_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	360(%rax), %xmm0
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	xorl	%r15d, %r15d
	movl	$224, %ebx
	.p2align	4, 0x90
.LBB14_2:                               # =>This Inner Loop Header: Depth=1
	movq	208(%r14), %r12
	cmpb	$0, -140(%r12,%rbx)
	xorps	%xmm1, %xmm1
	je	.LBB14_4
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=1
	leaq	(%r12,%rbx), %r13
	leaq	-224(%r12,%rbx), %rdi
	callq	_ZNK11btWheelInfo23getSuspensionRestLengthEv
	subss	-192(%r12,%rbx), %xmm0
	mulss	-4(%r12,%rbx), %xmm0
	mulss	48(%r12,%rbx), %xmm0
	movss	52(%r12,%rbx), %xmm1    # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	ucomiss	%xmm1, %xmm2
	leaq	4(%r12,%rbx), %rax
	cmovaq	%r13, %rax
	mulss	(%rax), %xmm1
	subss	%xmm1, %xmm0
	mulss	12(%rsp), %xmm0         # 4-byte Folded Reload
	xorps	%xmm1, %xmm1
	maxss	%xmm0, %xmm1
.LBB14_4:                               #   in Loop: Header=BB14_2 Depth=1
	movss	%xmm1, 56(%r12,%rbx)
	incq	%r15
	movslq	196(%r14), %rax
	addq	$288, %rbx              # imm = 0x120
	cmpq	%rax, %r15
	jl	.LBB14_2
.LBB14_5:                               # %._crit_edge
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	_ZN16btRaycastVehicle16updateSuspensionEf, .Lfunc_end14-_ZN16btRaycastVehicle16updateSuspensionEf
	.cfi_endproc

	.section	.text._ZN11btRigidBody12applyImpulseERK9btVector3S2_,"axG",@progbits,_ZN11btRigidBody12applyImpulseERK9btVector3S2_,comdat
	.weak	_ZN11btRigidBody12applyImpulseERK9btVector3S2_
	.p2align	4, 0x90
	.type	_ZN11btRigidBody12applyImpulseERK9btVector3S2_,@function
_ZN11btRigidBody12applyImpulseERK9btVector3S2_: # @_ZN11btRigidBody12applyImpulseERK9btVector3S2_
	.cfi_startproc
# BB#0:
	movss	360(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm2
	jne	.LBB15_1
	jnp	.LBB15_2
.LBB15_1:
	movss	380(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	384(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	movss	4(%rsi), %xmm5          # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	movss	388(%rdi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	mulss	%xmm2, %xmm4
	mulss	%xmm2, %xmm5
	mulss	%xmm2, %xmm6
	addss	328(%rdi), %xmm4
	movss	%xmm4, 328(%rdi)
	addss	332(%rdi), %xmm5
	movss	%xmm5, 332(%rdi)
	addss	336(%rdi), %xmm6
	movss	%xmm6, 336(%rdi)
	mulss	(%rsi), %xmm1
	mulss	4(%rsi), %xmm0
	mulss	8(%rsi), %xmm3
	movss	(%rdx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm6
	mulss	%xmm2, %xmm6
	mulss	%xmm1, %xmm2
	mulss	%xmm4, %xmm1
	mulss	%xmm3, %xmm4
	subss	%xmm6, %xmm4
	mulss	%xmm5, %xmm3
	subss	%xmm3, %xmm2
	mulss	%xmm5, %xmm0
	subss	%xmm1, %xmm0
	movss	280(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	movss	284(%rdi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	addss	%xmm1, %xmm3
	movss	288(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	%xmm3, %xmm1
	movss	296(%rdi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	movss	300(%rdi), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	movss	304(%rdi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm5, %xmm3
	mulss	312(%rdi), %xmm4
	mulss	316(%rdi), %xmm2
	addss	%xmm4, %xmm2
	mulss	320(%rdi), %xmm0
	addss	%xmm2, %xmm0
	mulss	364(%rdi), %xmm1
	mulss	368(%rdi), %xmm3
	mulss	372(%rdi), %xmm0
	addss	344(%rdi), %xmm1
	movss	%xmm1, 344(%rdi)
	addss	348(%rdi), %xmm3
	movss	%xmm3, 348(%rdi)
	addss	352(%rdi), %xmm0
	movss	%xmm0, 352(%rdi)
.LBB15_2:
	retq
.Lfunc_end15:
	.size	_ZN11btRigidBody12applyImpulseERK9btVector3S2_, .Lfunc_end15-_ZN11btRigidBody12applyImpulseERK9btVector3S2_
	.cfi_endproc

	.text
	.globl	_ZN16btRaycastVehicle16setSteeringValueEfi
	.p2align	4, 0x90
	.type	_ZN16btRaycastVehicle16setSteeringValueEfi,@function
_ZN16btRaycastVehicle16setSteeringValueEfi: # @_ZN16btRaycastVehicle16setSteeringValueEfi
	.cfi_startproc
# BB#0:
	movq	208(%rdi), %rax
	movslq	%esi, %rcx
	leaq	(%rcx,%rcx,8), %rcx
	shlq	$5, %rcx
	movss	%xmm0, 236(%rax,%rcx)
	retq
.Lfunc_end16:
	.size	_ZN16btRaycastVehicle16setSteeringValueEfi, .Lfunc_end16-_ZN16btRaycastVehicle16setSteeringValueEfi
	.cfi_endproc

	.globl	_ZN16btRaycastVehicle12getWheelInfoEi
	.p2align	4, 0x90
	.type	_ZN16btRaycastVehicle12getWheelInfoEi,@function
_ZN16btRaycastVehicle12getWheelInfoEi:  # @_ZN16btRaycastVehicle12getWheelInfoEi
	.cfi_startproc
# BB#0:
	movslq	%esi, %rax
	leaq	(%rax,%rax,8), %rax
	shlq	$5, %rax
	addq	208(%rdi), %rax
	retq
.Lfunc_end17:
	.size	_ZN16btRaycastVehicle12getWheelInfoEi, .Lfunc_end17-_ZN16btRaycastVehicle12getWheelInfoEi
	.cfi_endproc

	.globl	_ZNK16btRaycastVehicle16getSteeringValueEi
	.p2align	4, 0x90
	.type	_ZNK16btRaycastVehicle16getSteeringValueEi,@function
_ZNK16btRaycastVehicle16getSteeringValueEi: # @_ZNK16btRaycastVehicle16getSteeringValueEi
	.cfi_startproc
# BB#0:
	movq	208(%rdi), %rax
	movslq	%esi, %rcx
	leaq	(%rcx,%rcx,8), %rcx
	shlq	$5, %rcx
	movss	236(%rax,%rcx), %xmm0   # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end18:
	.size	_ZNK16btRaycastVehicle16getSteeringValueEi, .Lfunc_end18-_ZNK16btRaycastVehicle16getSteeringValueEi
	.cfi_endproc

	.globl	_ZNK16btRaycastVehicle12getWheelInfoEi
	.p2align	4, 0x90
	.type	_ZNK16btRaycastVehicle12getWheelInfoEi,@function
_ZNK16btRaycastVehicle12getWheelInfoEi: # @_ZNK16btRaycastVehicle12getWheelInfoEi
	.cfi_startproc
# BB#0:
	movslq	%esi, %rax
	leaq	(%rax,%rax,8), %rax
	shlq	$5, %rax
	addq	208(%rdi), %rax
	retq
.Lfunc_end19:
	.size	_ZNK16btRaycastVehicle12getWheelInfoEi, .Lfunc_end19-_ZNK16btRaycastVehicle12getWheelInfoEi
	.cfi_endproc

	.globl	_ZN16btRaycastVehicle16applyEngineForceEfi
	.p2align	4, 0x90
	.type	_ZN16btRaycastVehicle16applyEngineForceEfi,@function
_ZN16btRaycastVehicle16applyEngineForceEfi: # @_ZN16btRaycastVehicle16applyEngineForceEfi
	.cfi_startproc
# BB#0:
	movq	208(%rdi), %rax
	movslq	%esi, %rcx
	leaq	(%rcx,%rcx,8), %rcx
	shlq	$5, %rcx
	movss	%xmm0, 252(%rax,%rcx)
	retq
.Lfunc_end20:
	.size	_ZN16btRaycastVehicle16applyEngineForceEfi, .Lfunc_end20-_ZN16btRaycastVehicle16applyEngineForceEfi
	.cfi_endproc

	.globl	_ZN16btRaycastVehicle8setBrakeEfi
	.p2align	4, 0x90
	.type	_ZN16btRaycastVehicle8setBrakeEfi,@function
_ZN16btRaycastVehicle8setBrakeEfi:      # @_ZN16btRaycastVehicle8setBrakeEfi
	.cfi_startproc
# BB#0:
	movq	208(%rdi), %rax
	movslq	%esi, %rcx
	leaq	(%rcx,%rcx,8), %rcx
	shlq	$5, %rcx
	movss	%xmm0, 256(%rax,%rcx)
	retq
.Lfunc_end21:
	.size	_ZN16btRaycastVehicle8setBrakeEfi, .Lfunc_end21-_ZN16btRaycastVehicle8setBrakeEfi
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI22_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_Z19calcRollingFrictionR19btWheelContactPoint
	.p2align	4, 0x90
	.type	_Z19calcRollingFrictionR19btWheelContactPoint,@function
_Z19calcRollingFrictionR19btWheelContactPoint: # @_Z19calcRollingFrictionR19btWheelContactPoint
	.cfi_startproc
# BB#0:                                 # %_Z8btSetMinIfEvRT_RKS0_.exit
	movss	16(%rdi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movq	(%rdi), %rcx
	movq	8(%rdi), %rax
	movaps	%xmm8, %xmm7
	subss	56(%rcx), %xmm7
	movss	20(%rdi), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm2
	subss	60(%rcx), %xmm2
	movss	24(%rdi), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm6
	subss	64(%rcx), %xmm6
	subss	56(%rax), %xmm8
	subss	60(%rax), %xmm12
	subss	64(%rax), %xmm11
	movss	52(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	344(%rcx), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movss	348(%rcx), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm5
	mulss	%xmm10, %xmm5
	movss	352(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm1
	mulss	%xmm3, %xmm1
	subss	%xmm1, %xmm5
	mulss	%xmm7, %xmm3
	mulss	%xmm9, %xmm6
	subss	%xmm6, %xmm3
	mulss	%xmm9, %xmm2
	mulss	%xmm10, %xmm7
	subss	%xmm7, %xmm2
	addss	328(%rcx), %xmm5
	addss	332(%rcx), %xmm3
	addss	336(%rcx), %xmm2
	movss	344(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	348(%rax), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm7
	mulss	%xmm9, %xmm7
	movss	352(%rax), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm4
	mulss	%xmm6, %xmm4
	subss	%xmm4, %xmm7
	mulss	%xmm8, %xmm6
	mulss	%xmm1, %xmm11
	subss	%xmm11, %xmm6
	mulss	%xmm1, %xmm12
	mulss	%xmm9, %xmm8
	subss	%xmm8, %xmm12
	addss	328(%rax), %xmm7
	addss	332(%rax), %xmm6
	addss	336(%rax), %xmm12
	subss	%xmm7, %xmm5
	subss	%xmm6, %xmm3
	subss	%xmm12, %xmm2
	mulss	32(%rdi), %xmm5
	mulss	36(%rdi), %xmm3
	addss	%xmm5, %xmm3
	mulss	40(%rdi), %xmm2
	addss	%xmm3, %xmm2
	mulss	48(%rdi), %xmm2
	movaps	.LCPI22_0(%rip), %xmm1  # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm1, %xmm2
	movaps	%xmm0, %xmm3
	minss	%xmm2, %xmm3
	xorps	%xmm1, %xmm0
	maxss	%xmm3, %xmm0
	retq
.Lfunc_end22:
	.size	_Z19calcRollingFrictionR19btWheelContactPoint, .Lfunc_end22-_Z19calcRollingFrictionR19btWheelContactPoint
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI23_0:
	.long	1065353216              # float 1
.LCPI23_2:
	.long	1056964608              # float 0.5
.LCPI23_3:
	.long	0                       # float 0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI23_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN16btRaycastVehicle14updateFrictionEf
	.p2align	4, 0x90
	.type	_ZN16btRaycastVehicle14updateFrictionEf,@function
_ZN16btRaycastVehicle14updateFrictionEf: # @_ZN16btRaycastVehicle14updateFrictionEf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi80:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi81:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi82:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi84:
	.cfi_def_cfa_offset 192
.Lcfi85:
	.cfi_offset %rbx, -56
.Lcfi86:
	.cfi_offset %r12, -48
.Lcfi87:
	.cfi_offset %r13, -40
.Lcfi88:
	.cfi_offset %r14, -32
.Lcfi89:
	.cfi_offset %r15, -24
.Lcfi90:
	.cfi_offset %rbp, -16
	movq	%rdi, %r9
	movslq	196(%r9), %r13
	testq	%r13, %r13
	je	.LBB23_149
# BB#1:
	movslq	12(%r9), %rbp
	cmpl	%r13d, %ebp
	movq	%r9, (%rsp)             # 8-byte Spill
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	jge	.LBB23_23
# BB#2:
	cmpl	%r13d, 16(%r9)
	jge	.LBB23_7
# BB#3:                                 # %_ZN20btAlignedObjectArrayI9btVector3E8allocateEi.exit.i.i
	movq	%r13, %rdi
	shlq	$4, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	(%rsp), %r9             # 8-byte Reload
	movq	%rax, %r14
	movslq	12(%r9), %rax
	leaq	24(%r9), %rbx
	testq	%rax, %rax
	jle	.LBB23_12
# BB#4:                                 # %.lr.ph.i.i.i
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB23_8
# BB#5:                                 # %.prol.preheader421
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB23_6:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	movups	(%rdx,%rdi), %xmm0
	movups	%xmm0, (%r14,%rdi)
	incq	%rcx
	addq	$16, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB23_6
	jmp	.LBB23_9
.LBB23_7:                               # %..lr.ph.i_crit_edge
	leaq	24(%r9), %rbx
	jmp	.LBB23_17
.LBB23_8:
	xorl	%ecx, %ecx
.LBB23_9:                               # %.prol.loopexit422
	cmpq	$3, %r8
	jb	.LBB23_12
# BB#10:                                # %.lr.ph.i.i.i.new
	subq	%rcx, %rax
	shlq	$4, %rcx
	addq	$48, %rcx
	.p2align	4, 0x90
.LBB23_11:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%r14,%rcx)
	movq	(%rbx), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%r14,%rcx)
	movq	(%rbx), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%r14,%rcx)
	movq	(%rbx), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%r14,%rcx)
	addq	$64, %rcx
	addq	$-4, %rax
	jne	.LBB23_11
.LBB23_12:                              # %_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_.exit.i.i
	movq	24(%r9), %rdi
	testq	%rdi, %rdi
	je	.LBB23_16
# BB#13:
	cmpb	$0, 32(%r9)
	je	.LBB23_15
# BB#14:
	callq	_Z21btAlignedFreeInternalPv
	movq	(%rsp), %r9             # 8-byte Reload
.LBB23_15:
	movq	$0, (%rbx)
.LBB23_16:                              # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.preheader.i
	movb	$1, 32(%r9)
	movq	%r14, 24(%r9)
	movl	%r13d, 16(%r9)
.LBB23_17:                              # %.lr.ph.i
	movl	%r13d, %ecx
	subl	%ebp, %ecx
	leaq	-1(%r13), %rax
	subq	%rbp, %rax
	andq	$3, %rcx
	je	.LBB23_20
# BB#18:                                # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i.prol.preheader
	movq	%rbp, %rdx
	shlq	$4, %rdx
	negq	%rcx
	.p2align	4, 0x90
.LBB23_19:                              # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movups	16(%rsp), %xmm0
	movups	%xmm0, (%rsi,%rdx)
	incq	%rbp
	addq	$16, %rdx
	incq	%rcx
	jne	.LBB23_19
.LBB23_20:                              # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB23_23
# BB#21:                                # %.lr.ph.i.new
	movq	%r13, %rax
	subq	%rbp, %rax
	shlq	$4, %rbp
	addq	$48, %rbp
	.p2align	4, 0x90
.LBB23_22:                              # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movups	16(%rsp), %xmm0
	movups	%xmm0, -48(%rcx,%rbp)
	movq	(%rbx), %rcx
	movups	16(%rsp), %xmm0
	movups	%xmm0, -32(%rcx,%rbp)
	movq	(%rbx), %rcx
	movups	16(%rsp), %xmm0
	movups	%xmm0, -16(%rcx,%rbp)
	movq	(%rbx), %rcx
	movups	16(%rsp), %xmm0
	movups	%xmm0, (%rcx,%rbp)
	addq	$64, %rbp
	addq	$-4, %rax
	jne	.LBB23_22
.LBB23_23:                              # %_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_.exit
	movl	%r13d, 12(%r9)
	movslq	44(%r9), %rbp
	cmpl	%r13d, %ebp
	jge	.LBB23_45
# BB#24:
	cmpl	%r13d, 48(%r9)
	jge	.LBB23_29
# BB#25:                                # %_ZN20btAlignedObjectArrayI9btVector3E8allocateEi.exit.i.i179
	movq	%r13, %rdi
	shlq	$4, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	(%rsp), %r9             # 8-byte Reload
	movq	%rax, %r14
	movslq	44(%r9), %rax
	leaq	56(%r9), %rbx
	testq	%rax, %rax
	jle	.LBB23_34
# BB#26:                                # %.lr.ph.i.i.i181
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB23_30
# BB#27:                                # %.prol.preheader
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB23_28:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	movups	(%rdx,%rdi), %xmm0
	movups	%xmm0, (%r14,%rdi)
	incq	%rcx
	addq	$16, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB23_28
	jmp	.LBB23_31
.LBB23_29:                              # %..lr.ph.i189_crit_edge
	leaq	56(%r9), %rbx
	jmp	.LBB23_39
.LBB23_30:
	xorl	%ecx, %ecx
.LBB23_31:                              # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB23_34
# BB#32:                                # %.lr.ph.i.i.i181.new
	subq	%rcx, %rax
	shlq	$4, %rcx
	addq	$48, %rcx
	.p2align	4, 0x90
.LBB23_33:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%r14,%rcx)
	movq	(%rbx), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%r14,%rcx)
	movq	(%rbx), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%r14,%rcx)
	movq	(%rbx), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%r14,%rcx)
	addq	$64, %rcx
	addq	$-4, %rax
	jne	.LBB23_33
.LBB23_34:                              # %_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_.exit.i.i186
	movq	56(%r9), %rdi
	testq	%rdi, %rdi
	je	.LBB23_38
# BB#35:
	cmpb	$0, 64(%r9)
	je	.LBB23_37
# BB#36:
	callq	_Z21btAlignedFreeInternalPv
	movq	(%rsp), %r9             # 8-byte Reload
.LBB23_37:
	movq	$0, (%rbx)
.LBB23_38:                              # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.preheader.i187
	movb	$1, 64(%r9)
	movq	%r14, 56(%r9)
	movl	%r13d, 48(%r9)
.LBB23_39:                              # %.lr.ph.i189
	movl	%r13d, %ecx
	subl	%ebp, %ecx
	leaq	-1(%r13), %rax
	subq	%rbp, %rax
	andq	$3, %rcx
	je	.LBB23_42
# BB#40:                                # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i193.prol.preheader
	movq	%rbp, %rdx
	shlq	$4, %rdx
	negq	%rcx
	.p2align	4, 0x90
.LBB23_41:                              # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i193.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movups	16(%rsp), %xmm0
	movups	%xmm0, (%rsi,%rdx)
	incq	%rbp
	addq	$16, %rdx
	incq	%rcx
	jne	.LBB23_41
.LBB23_42:                              # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i193.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB23_45
# BB#43:                                # %.lr.ph.i189.new
	movq	%r13, %rax
	subq	%rbp, %rax
	shlq	$4, %rbp
	addq	$48, %rbp
	.p2align	4, 0x90
.LBB23_44:                              # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i193
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movups	16(%rsp), %xmm0
	movups	%xmm0, -48(%rcx,%rbp)
	movq	(%rbx), %rcx
	movups	16(%rsp), %xmm0
	movups	%xmm0, -32(%rcx,%rbp)
	movq	(%rbx), %rcx
	movups	16(%rsp), %xmm0
	movups	%xmm0, -16(%rcx,%rbp)
	movq	(%rbx), %rcx
	movups	16(%rsp), %xmm0
	movups	%xmm0, (%rcx,%rbp)
	addq	$64, %rbp
	addq	$-4, %rax
	jne	.LBB23_44
.LBB23_45:                              # %_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_.exit194
	movl	%r13d, 44(%r9)
	movslq	76(%r9), %rbx
	cmpl	%r13d, %ebx
	jge	.LBB23_76
# BB#46:
	cmpl	%r13d, 80(%r9)
	jge	.LBB23_50
# BB#47:                                # %_ZN20btAlignedObjectArrayIfE8allocateEi.exit.i.i
	leaq	(,%r13,4), %r14
	movl	$16, %esi
	movq	%r14, %rdi
	callq	_Z22btAlignedAllocInternalmi
	movq	(%rsp), %r8             # 8-byte Reload
	movq	%rax, %r15
	movslq	76(%r8), %rax
	testq	%rax, %rax
	movq	88(%r8), %rdi
	jle	.LBB23_51
# BB#48:                                # %.lr.ph.i.i.i198
	cmpl	$8, %eax
	jae	.LBB23_53
# BB#49:
	xorl	%ecx, %ecx
	jmp	.LBB23_66
.LBB23_50:                              # %..lr.ph.i204_crit_edge
	movq	88(%r9), %r15
	leaq	(,%r13,4), %r14
	jmp	.LBB23_75
.LBB23_51:                              # %_ZNK20btAlignedObjectArrayIfE4copyEiiPf.exit.i.i
	testq	%rdi, %rdi
	jne	.LBB23_72
# BB#52:                                # %_ZN20btAlignedObjectArrayIfE7reserveEi.exit.preheader.thread26.i
	leaq	96(%r8), %rbp
	jmp	.LBB23_74
.LBB23_53:                              # %min.iters.checked
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB23_57
# BB#54:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r15
	jae	.LBB23_58
# BB#55:                                # %vector.memcheck
	leaq	(%r15,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB23_58
.LBB23_57:
	xorl	%ecx, %ecx
.LBB23_66:                              # %scalar.ph.preheader
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB23_69
# BB#67:                                # %scalar.ph.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB23_68:                              # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %ebp
	movl	%ebp, (%r15,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB23_68
.LBB23_69:                              # %scalar.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB23_72
# BB#70:                                # %scalar.ph.preheader.new
	subq	%rcx, %rax
	leaq	28(%rdi,%rcx,4), %rdx
	leaq	28(%r15,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB23_71:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rdx), %esi
	movl	%esi, -28(%rcx)
	movl	-24(%rdx), %esi
	movl	%esi, -24(%rcx)
	movl	-20(%rdx), %esi
	movl	%esi, -20(%rcx)
	movl	-16(%rdx), %esi
	movl	%esi, -16(%rcx)
	movl	-12(%rdx), %esi
	movl	%esi, -12(%rcx)
	movl	-8(%rdx), %esi
	movl	%esi, -8(%rcx)
	movl	-4(%rdx), %esi
	movl	%esi, -4(%rcx)
	movl	(%rdx), %esi
	movl	%esi, (%rcx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB23_71
.LBB23_72:                              # %_ZNK20btAlignedObjectArrayIfE4copyEiiPf.exit.thread.i.i
	leaq	96(%r8), %rbp
	cmpb	$0, 96(%r8)
	je	.LBB23_74
# BB#73:
	callq	_Z21btAlignedFreeInternalPv
	movq	(%rsp), %r8             # 8-byte Reload
.LBB23_74:                              # %.lr.ph.i204.sink.split
	movb	$1, (%rbp)
	movq	%r15, 88(%r8)
	movl	%r13d, 80(%r8)
.LBB23_75:                              # %.lr.ph.i204
	leaq	(%r15,%rbx,4), %rdi
	shlq	$2, %rbx
	subq	%rbx, %r14
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	movq	(%rsp), %r9             # 8-byte Reload
.LBB23_76:                              # %_ZN20btAlignedObjectArrayIfE6resizeEiRKf.exit
	movl	%r13d, 76(%r9)
	movslq	108(%r9), %rbx
	cmpl	%r13d, %ebx
	jge	.LBB23_107
# BB#77:
	cmpl	%r13d, 112(%r9)
	jge	.LBB23_81
# BB#78:                                # %_ZN20btAlignedObjectArrayIfE8allocateEi.exit.i.i210
	movq	%r13, %r14
	shlq	$2, %r14
	movl	$16, %esi
	movq	%r14, %rdi
	callq	_Z22btAlignedAllocInternalmi
	movq	(%rsp), %r8             # 8-byte Reload
	movq	%rax, %r15
	movslq	108(%r8), %rax
	testq	%rax, %rax
	movq	120(%r8), %rdi
	jle	.LBB23_82
# BB#79:                                # %.lr.ph.i.i.i212
	cmpl	$8, %eax
	jae	.LBB23_84
# BB#80:
	xorl	%ecx, %ecx
	jmp	.LBB23_97
.LBB23_81:                              # %..lr.ph.i222_crit_edge
	movq	120(%r9), %r15
	movq	%r13, %r14
	shlq	$2, %r14
	jmp	.LBB23_106
.LBB23_82:                              # %_ZNK20btAlignedObjectArrayIfE4copyEiiPf.exit.i.i216
	testq	%rdi, %rdi
	jne	.LBB23_103
# BB#83:                                # %_ZN20btAlignedObjectArrayIfE7reserveEi.exit.preheader.thread26.i218
	movq	%r8, %rbp
	subq	$-128, %rbp
	jmp	.LBB23_105
.LBB23_84:                              # %min.iters.checked348
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB23_88
# BB#85:                                # %vector.memcheck360
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r15
	jae	.LBB23_89
# BB#86:                                # %vector.memcheck360
	leaq	(%r15,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB23_89
.LBB23_88:
	xorl	%ecx, %ecx
.LBB23_97:                              # %scalar.ph346.preheader
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB23_100
# BB#98:                                # %scalar.ph346.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB23_99:                              # %scalar.ph346.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %ebp
	movl	%ebp, (%r15,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB23_99
.LBB23_100:                             # %scalar.ph346.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB23_103
# BB#101:                               # %scalar.ph346.preheader.new
	subq	%rcx, %rax
	leaq	28(%rdi,%rcx,4), %rdx
	leaq	28(%r15,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB23_102:                             # %scalar.ph346
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rdx), %esi
	movl	%esi, -28(%rcx)
	movl	-24(%rdx), %esi
	movl	%esi, -24(%rcx)
	movl	-20(%rdx), %esi
	movl	%esi, -20(%rcx)
	movl	-16(%rdx), %esi
	movl	%esi, -16(%rcx)
	movl	-12(%rdx), %esi
	movl	%esi, -12(%rcx)
	movl	-8(%rdx), %esi
	movl	%esi, -8(%rcx)
	movl	-4(%rdx), %esi
	movl	%esi, -4(%rcx)
	movl	(%rdx), %esi
	movl	%esi, (%rcx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB23_102
.LBB23_103:                             # %_ZNK20btAlignedObjectArrayIfE4copyEiiPf.exit.thread.i.i219
	movq	%r8, %rbp
	subq	$-128, %rbp
	cmpb	$0, 128(%r8)
	je	.LBB23_105
# BB#104:
	callq	_Z21btAlignedFreeInternalPv
	movq	(%rsp), %r8             # 8-byte Reload
.LBB23_105:                             # %.lr.ph.i222.sink.split
	movb	$1, (%rbp)
	movq	%r15, 120(%r8)
	movl	%r13d, 112(%r8)
.LBB23_106:                             # %.lr.ph.i222
	leaq	(%r15,%rbx,4), %rdi
	shlq	$2, %rbx
	subq	%rbx, %r14
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	movq	(%rsp), %r9             # 8-byte Reload
.LBB23_107:                             # %_ZN20btAlignedObjectArrayIfE6resizeEiRKf.exit227
	movl	%r13d, 108(%r9)
	movslq	196(%r9), %rax
	testq	%rax, %rax
	jle	.LBB23_149
# BB#108:                               # %.lr.ph305
	movq	88(%r9), %rcx
	movq	120(%r9), %rdx
	cmpl	$7, %eax
	jbe	.LBB23_112
# BB#109:                               # %min.iters.checked376
	movq	%rax, %rsi
	andq	$-8, %rsi
	je	.LBB23_112
# BB#110:                               # %vector.memcheck389
	leaq	(%rcx,%rax,4), %rdi
	cmpq	%rdi, %rdx
	jae	.LBB23_150
# BB#111:                               # %vector.memcheck389
	leaq	(%rdx,%rax,4), %rdi
	cmpq	%rdi, %rcx
	jae	.LBB23_150
.LBB23_112:
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB23_113:                             # %scalar.ph374
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, (%rdx,%rsi,4)
	movl	$0, (%rcx,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB23_113
.LBB23_114:                             # %.preheader295
	testl	%eax, %eax
	jle	.LBB23_149
# BB#115:                               # %.lr.ph303
	xorl	%r13d, %r13d
	movl	$128, %r14d
	movss	.LCPI23_0(%rip), %xmm8  # xmm8 = mem[0],zero,zero,zero
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB23_116:                             # =>This Inner Loop Header: Depth=1
	movq	208(%r9), %r12
	movq	-40(%r12,%r14), %rdx
	testq	%rdx, %rdx
	je	.LBB23_122
# BB#117:                               #   in Loop: Header=BB23_116 Depth=1
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movups	-32(%r12,%r14), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	-16(%r12,%r14), %xmm0
	leaq	32(%rsp), %rax
	movups	%xmm0, (%rax)
	movups	(%r12,%r14), %xmm0
	movups	%xmm0, 16(%rax)
	movslq	176(%r9), %rax
	movl	16(%rsp,%rax,4), %ecx
	movl	32(%rsp,%rax,4), %edi
	movl	48(%rsp,%rax,4), %eax
	movq	56(%r9), %rsi
	movl	%ecx, (%rsi,%rbp)
	movl	%edi, 4(%rsi,%rbp)
	movl	%eax, 8(%rsi,%rbp)
	movl	$0, 12(%rsi,%rbp)
	movq	56(%r9), %r15
	movss	(%r15,%rbp), %xmm2      # xmm2 = mem[0],zero,zero,zero
	movss	4(%r15,%rbp), %xmm1     # xmm1 = mem[0],zero,zero,zero
	movss	-128(%r12,%r14), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movss	-124(%r12,%r14), %xmm4  # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm0
	mulss	%xmm3, %xmm0
	movaps	%xmm1, %xmm5
	mulss	%xmm4, %xmm5
	addss	%xmm0, %xmm5
	movss	8(%r15,%rbp), %xmm0     # xmm0 = mem[0],zero,zero,zero
	movss	-120(%r12,%r14), %xmm6  # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm7
	mulss	%xmm6, %xmm7
	addss	%xmm5, %xmm7
	mulss	%xmm7, %xmm3
	mulss	%xmm7, %xmm4
	mulss	%xmm6, %xmm7
	subss	%xmm3, %xmm2
	movss	%xmm2, (%r15,%rbp)
	subss	%xmm4, %xmm1
	movss	%xmm1, 4(%r15,%rbp)
	subss	%xmm7, %xmm0
	movss	%xmm0, 8(%r15,%rbp)
	mulss	%xmm2, %xmm2
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB23_119
# BB#118:                               # %call.sqrt
                                        #   in Loop: Header=BB23_116 Depth=1
	movq	%rdx, %rbx
	callq	sqrtf
	movq	%rbx, %rdx
	movss	.LCPI23_0(%rip), %xmm8  # xmm8 = mem[0],zero,zero,zero
	movq	(%rsp), %r9             # 8-byte Reload
	movaps	%xmm0, %xmm1
.LBB23_119:                             # %.split
                                        #   in Loop: Header=BB23_116 Depth=1
	movaps	%xmm8, %xmm0
	divss	%xmm1, %xmm0
	movss	(%r15,%rbp), %xmm1      # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%r15,%rbp)
	movss	4(%r15,%rbp), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 4(%r15,%rbp)
	mulss	8(%r15,%rbp), %xmm0
	movss	%xmm0, 8(%r15,%rbp)
	movq	56(%r9), %rax
	movups	(%r15,%rbp), %xmm0
	movups	%xmm0, (%rax,%rbp)
	movq	24(%r9), %rax
	movq	56(%r9), %rcx
	movsd	-124(%r12,%r14), %xmm0  # xmm0 = mem[0],zero
	movss	(%rcx,%rbp), %xmm1      # xmm1 = mem[0],zero,zero,zero
	movss	8(%rcx,%rbp), %xmm2     # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	mulps	%xmm0, %xmm2
	movss	-128(%r12,%r14), %xmm3  # xmm3 = mem[0],zero,zero,zero
	pshufd	$229, %xmm0, %xmm4      # xmm4 = xmm0[1,1,2,3]
	movaps	%xmm3, %xmm5
	shufps	$0, %xmm4, %xmm5        # xmm5 = xmm5[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm5      # xmm5 = xmm5[2,0],xmm4[2,3]
	movsd	4(%rcx,%rbp), %xmm4     # xmm4 = mem[0],zero
	mulps	%xmm5, %xmm4
	subps	%xmm4, %xmm2
	mulss	4(%rcx,%rbp), %xmm3
	mulss	%xmm1, %xmm0
	subss	%xmm0, %xmm3
	xorps	%xmm0, %xmm0
	movss	%xmm3, %xmm0            # xmm0 = xmm3[0],xmm0[1,2,3]
	movlps	%xmm2, (%rax,%rbp)
	movlps	%xmm0, 8(%rax,%rbp)
	movq	24(%r9), %rbx
	movss	(%rbx,%rbp), %xmm0      # xmm0 = mem[0],zero,zero,zero
	movss	4(%rbx,%rbp), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%rbx,%rbp), %xmm0     # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB23_121
# BB#120:                               # %call.sqrt875
                                        #   in Loop: Header=BB23_116 Depth=1
	movq	%rdx, %r15
	callq	sqrtf
	movq	%r15, %rdx
	movss	.LCPI23_0(%rip), %xmm8  # xmm8 = mem[0],zero,zero,zero
	movq	(%rsp), %r9             # 8-byte Reload
	movaps	%xmm0, %xmm1
.LBB23_121:                             # %.split.split
                                        #   in Loop: Header=BB23_116 Depth=1
	movaps	%xmm8, %xmm0
	divss	%xmm1, %xmm0
	movss	(%rbx,%rbp), %xmm1      # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rbx,%rbp)
	movss	4(%rbx,%rbp), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 4(%rbx,%rbp)
	mulss	8(%rbx,%rbp), %xmm0
	movss	%xmm0, 8(%rbx,%rbp)
	movq	168(%r9), %rdi
	leaq	-112(%r12,%r14), %rsi
	movq	56(%r9), %r8
	addq	%rbp, %r8
	movq	(%rsp), %rax            # 8-byte Reload
	movq	120(%rax), %r9
	addq	%r13, %r9
	xorps	%xmm0, %xmm0
	movq	%rsi, %rcx
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	callq	_Z22resolveSingleBilateralR11btRigidBodyRK9btVector3S0_S3_fS3_Rff
	movss	.LCPI23_0(%rip), %xmm8  # xmm8 = mem[0],zero,zero,zero
	movq	(%rsp), %r9             # 8-byte Reload
	movss	sideFrictionStiffness2(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movq	120(%r9), %rax
	mulss	(%rax,%r13), %xmm0
	movss	%xmm0, (%rax,%r13)
	movl	196(%r9), %eax
	movq	80(%rsp), %rsi          # 8-byte Reload
.LBB23_122:                             #   in Loop: Header=BB23_116 Depth=1
	incq	%rsi
	movslq	%eax, %rcx
	addq	$16, %rbp
	addq	$4, %r13
	addq	$288, %r14              # imm = 0x120
	cmpq	%rcx, %rsi
	jl	.LBB23_116
# BB#123:                               # %.preheader294
	testl	%eax, %eax
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	jle	.LBB23_149
# BB#124:                               # %.lr.ph301
	movq	208(%r9), %r13
	xorl	%r15d, %r15d
	movl	$284, %ebp              # imm = 0x11C
	xorps	%xmm3, %xmm3
	leaq	16(%rsp), %r12
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB23_125:                             # =>This Inner Loop Header: Depth=1
	movq	-196(%r13,%rbp), %rdx
	testq	%rdx, %rdx
	je	.LBB23_128
# BB#126:                               #   in Loop: Header=BB23_125 Depth=1
	movss	-32(%r13,%rbp), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm1
	jne	.LBB23_127
	jnp	.LBB23_129
.LBB23_127:                             #   in Loop: Header=BB23_125 Depth=1
	mulss	%xmm4, %xmm1
	movq	%r13, %rax
	jmp	.LBB23_130
	.p2align	4, 0x90
.LBB23_128:                             #   in Loop: Header=BB23_125 Depth=1
	movq	88(%r9), %rax
	movl	$0, (%rax,%rbx,4)
	movl	$1065353216, (%r13,%rbp) # imm = 0x3F800000
	jmp	.LBB23_134
	.p2align	4, 0x90
.LBB23_129:                             #   in Loop: Header=BB23_125 Depth=1
	movss	-28(%r13,%rbp), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	cmpeqss	%xmm3, %xmm0
	andnps	%xmm1, %xmm0
	movq	168(%r9), %rsi
	leaq	-268(%r13,%rbp), %rcx
	movq	24(%r9), %r8
	addq	%r15, %r8
	movq	%r12, %rdi
	callq	_ZN19btWheelContactPointC2EP11btRigidBodyS1_RK9btVector3S4_f
	movq	(%rsp), %r9             # 8-byte Reload
	movss	32(%rsp), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movq	16(%rsp), %rcx
	movq	24(%rsp), %rax
	movaps	%xmm8, %xmm7
	subss	56(%rcx), %xmm7
	movss	36(%rsp), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm2
	subss	60(%rcx), %xmm2
	movss	40(%rsp), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm6
	subss	64(%rcx), %xmm6
	subss	56(%rax), %xmm8
	subss	60(%rax), %xmm11
	subss	64(%rax), %xmm10
	movss	68(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	348(%rcx), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm5
	mulss	%xmm9, %xmm5
	movss	352(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm0
	mulss	%xmm3, %xmm0
	subss	%xmm0, %xmm5
	mulss	%xmm7, %xmm3
	movss	344(%rcx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	subss	%xmm6, %xmm3
	mulss	%xmm0, %xmm2
	mulss	%xmm9, %xmm7
	subss	%xmm7, %xmm2
	addss	328(%rcx), %xmm5
	addss	332(%rcx), %xmm3
	addss	336(%rcx), %xmm2
	movss	348(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm6
	mulss	%xmm0, %xmm6
	movss	352(%rax), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm4
	mulss	%xmm7, %xmm4
	subss	%xmm4, %xmm6
	mulss	%xmm8, %xmm7
	movss	344(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm10
	subss	%xmm10, %xmm7
	mulss	%xmm4, %xmm11
	mulss	%xmm0, %xmm8
	subss	%xmm8, %xmm11
	movss	.LCPI23_0(%rip), %xmm8  # xmm8 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	addss	328(%rax), %xmm6
	addss	332(%rax), %xmm7
	addss	336(%rax), %xmm11
	subss	%xmm6, %xmm5
	subss	%xmm7, %xmm3
	subss	%xmm11, %xmm2
	mulss	48(%rsp), %xmm5
	mulss	52(%rsp), %xmm3
	addss	%xmm5, %xmm3
	mulss	56(%rsp), %xmm2
	addss	%xmm3, %xmm2
	mulss	64(%rsp), %xmm2
	movaps	.LCPI23_1(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm0, %xmm3
	xorps	%xmm3, %xmm2
	movaps	%xmm1, %xmm0
	minss	%xmm2, %xmm0
	xorps	%xmm3, %xmm1
	xorps	%xmm3, %xmm3
	maxss	%xmm0, %xmm1
	movq	208(%r9), %rax
.LBB23_130:                             #   in Loop: Header=BB23_125 Depth=1
	movq	88(%r9), %rcx
	movl	$0, (%rcx,%rbx,4)
	movl	$1065353216, (%rax,%rbp) # imm = 0x3F800000
	movss	-4(%r13,%rbp), %xmm5    # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm5
	mulss	-52(%r13,%rbp), %xmm5
	movaps	%xmm5, %xmm2
	mulss	%xmm2, %xmm2
	movss	%xmm1, (%rcx,%rbx,4)
	mulss	.LCPI23_2(%rip), %xmm1
	movq	120(%r9), %rcx
	movss	(%rcx,%rbx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	ucomiss	%xmm2, %xmm0
	movq	%rax, %r13
	jbe	.LBB23_134
# BB#131:                               #   in Loop: Header=BB23_125 Depth=1
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB23_133
# BB#132:                               # %call.sqrt877
                                        #   in Loop: Header=BB23_125 Depth=1
	movss	%xmm5, 80(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	80(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm3
	movss	.LCPI23_0(%rip), %xmm8  # xmm8 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movq	(%rsp), %r9             # 8-byte Reload
	movaps	%xmm0, %xmm1
.LBB23_133:                             # %.split876
                                        #   in Loop: Header=BB23_125 Depth=1
	divss	%xmm1, %xmm5
	movq	208(%r9), %r13
	mulss	(%r13,%rbp), %xmm5
	movss	%xmm5, (%r13,%rbp)
	movb	$1, %r14b
.LBB23_134:                             #   in Loop: Header=BB23_125 Depth=1
	incq	%rbx
	movslq	196(%r9), %rax
	addq	$288, %rbp              # imm = 0x120
	addq	$16, %r15
	cmpq	%rax, %rbx
	jl	.LBB23_125
# BB#135:                               # %._crit_edge
	testb	$1, %r14b
	je	.LBB23_142
# BB#136:                               # %.preheader293
	testl	%eax, %eax
	jle	.LBB23_149
# BB#137:                               # %.lr.ph298
	movq	120(%r9), %rcx
	xorl	%edx, %edx
	movl	$71, %esi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB23_138:                             # =>This Inner Loop Header: Depth=1
	movss	(%rcx,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB23_139
	jnp	.LBB23_141
.LBB23_139:                             #   in Loop: Header=BB23_138 Depth=1
	movq	208(%r9), %rdi
	movss	(%rdi,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm8
	jbe	.LBB23_141
# BB#140:                               #   in Loop: Header=BB23_138 Depth=1
	movq	88(%r9), %rbp
	mulss	(%rbp,%rdx,4), %xmm1
	movss	%xmm1, (%rbp,%rdx,4)
	movss	(%rdi,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	(%rcx,%rdx,4), %xmm1
	movss	%xmm1, (%rcx,%rdx,4)
.LBB23_141:                             #   in Loop: Header=BB23_138 Depth=1
	incq	%rdx
	addq	$72, %rsi
	cmpq	%rax, %rdx
	jl	.LBB23_138
.LBB23_142:                             # %.preheader
	testl	%eax, %eax
	jle	.LBB23_149
# BB#143:                               # %.lr.ph
	xorl	%ebx, %ebx
	movl	$8, %r15d
	leaq	104(%rsp), %r12
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB23_144:                             # =>This Inner Loop Header: Depth=1
	movq	208(%r9), %r14
	movq	168(%r9), %rdi
	movsd	16(%r14,%rbx), %xmm0    # xmm0 = mem[0],zero
	movsd	56(%rdi), %xmm1         # xmm1 = mem[0],zero
	subps	%xmm1, %xmm0
	movss	24(%r14,%rbx), %xmm1    # xmm1 = mem[0],zero,zero,zero
	subss	64(%rdi), %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 16(%rsp)
	movlps	%xmm2, 24(%rsp)
	movq	88(%r9), %rax
	movss	(%rax,%r13,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	ucomiss	.LCPI23_3, %xmm0
	jne	.LBB23_145
	jnp	.LBB23_146
.LBB23_145:                             #   in Loop: Header=BB23_144 Depth=1
	movq	24(%r9), %rax
	movsd	-8(%rax,%r15), %xmm1    # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm1, %xmm2
	mulss	(%rax,%r15), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, 104(%rsp)
	movlps	%xmm1, 112(%rsp)
	movq	%r12, %rsi
	leaq	16(%rsp), %rdx
	callq	_ZN11btRigidBody12applyImpulseERK9btVector3S2_
	movq	(%rsp), %r9             # 8-byte Reload
.LBB23_146:                             #   in Loop: Header=BB23_144 Depth=1
	movq	120(%r9), %rax
	movss	(%rax,%r13,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	ucomiss	.LCPI23_3, %xmm0
	jne	.LBB23_147
	jnp	.LBB23_148
.LBB23_147:                             #   in Loop: Header=BB23_144 Depth=1
	movq	208(%r9), %rcx
	movq	88(%rcx,%rbx), %rbp
	movsd	16(%r14,%rbx), %xmm0    # xmm0 = mem[0],zero
	movsd	56(%rbp), %xmm1         # xmm1 = mem[0],zero
	subps	%xmm1, %xmm0
	movss	24(%r14,%rbx), %xmm1    # xmm1 = mem[0],zero,zero,zero
	subss	64(%rbp), %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 104(%rsp)
	movlps	%xmm2, 112(%rsp)
	movq	56(%r9), %rcx
	movss	(%rax,%r13,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movsd	-8(%rcx,%r15), %xmm1    # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm1, %xmm2
	mulss	(%rcx,%r15), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, 88(%rsp)
	movlps	%xmm1, 96(%rsp)
	movss	248(%r14,%rbx), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movslq	180(%r9), %rax
	mulss	16(%rsp,%rax,4), %xmm0
	movss	%xmm0, 16(%rsp,%rax,4)
	movq	168(%r9), %rdi
	leaq	88(%rsp), %rsi
	leaq	16(%rsp), %rdx
	callq	_ZN11btRigidBody12applyImpulseERK9btVector3S2_
	movsd	88(%rsp), %xmm0         # xmm0 = mem[0],zero
	movaps	.LCPI23_1(%rip), %xmm1  # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm1, %xmm2
	xorps	%xmm2, %xmm0
	movss	96(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 120(%rsp)
	movlps	%xmm2, 128(%rsp)
	movq	%rbp, %rdi
	leaq	120(%rsp), %rsi
	movq	%r12, %rdx
	callq	_ZN11btRigidBody12applyImpulseERK9btVector3S2_
	movq	(%rsp), %r9             # 8-byte Reload
.LBB23_148:                             #   in Loop: Header=BB23_144 Depth=1
	incq	%r13
	movslq	196(%r9), %rax
	addq	$288, %rbx              # imm = 0x120
	addq	$16, %r15
	cmpq	%rax, %r13
	jl	.LBB23_144
.LBB23_149:                             # %.loopexit
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB23_150:                             # %vector.body372.preheader
	leaq	-8(%rsi), %rbp
	movl	%ebp, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB23_153
# BB#151:                               # %vector.body372.prol.preheader
	negq	%rdi
	xorl	%ebx, %ebx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB23_152:                             # %vector.body372.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, (%rdx,%rbx,4)
	movups	%xmm0, 16(%rdx,%rbx,4)
	movups	%xmm0, (%rcx,%rbx,4)
	movups	%xmm0, 16(%rcx,%rbx,4)
	addq	$8, %rbx
	incq	%rdi
	jne	.LBB23_152
	jmp	.LBB23_154
.LBB23_153:
	xorl	%ebx, %ebx
.LBB23_154:                             # %vector.body372.prol.loopexit
	cmpq	$24, %rbp
	jb	.LBB23_157
# BB#155:                               # %vector.body372.preheader.new
	movq	%rsi, %rdi
	subq	%rbx, %rdi
	leaq	112(%rcx,%rbx,4), %rbp
	leaq	112(%rdx,%rbx,4), %rbx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB23_156:                             # %vector.body372
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -112(%rbx)
	movups	%xmm0, -96(%rbx)
	movups	%xmm0, -112(%rbp)
	movups	%xmm0, -96(%rbp)
	movups	%xmm0, -80(%rbx)
	movups	%xmm0, -64(%rbx)
	movups	%xmm0, -80(%rbp)
	movups	%xmm0, -64(%rbp)
	movups	%xmm0, -48(%rbx)
	movups	%xmm0, -32(%rbx)
	movups	%xmm0, -48(%rbp)
	movups	%xmm0, -32(%rbp)
	movups	%xmm0, -16(%rbx)
	movups	%xmm0, (%rbx)
	movups	%xmm0, -16(%rbp)
	movups	%xmm0, (%rbp)
	subq	$-128, %rbp
	subq	$-128, %rbx
	addq	$-32, %rdi
	jne	.LBB23_156
.LBB23_157:                             # %middle.block373
	cmpq	%rsi, %rax
	jne	.LBB23_113
	jmp	.LBB23_114
.LBB23_58:                              # %vector.body.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB23_61
# BB#59:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB23_60:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp,4), %xmm0
	movups	16(%rdi,%rbp,4), %xmm1
	movups	%xmm0, (%r15,%rbp,4)
	movups	%xmm1, 16(%r15,%rbp,4)
	addq	$8, %rbp
	incq	%rsi
	jne	.LBB23_60
	jmp	.LBB23_62
.LBB23_89:                              # %vector.body344.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB23_92
# BB#90:                                # %vector.body344.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB23_91:                              # %vector.body344.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp,4), %xmm0
	movups	16(%rdi,%rbp,4), %xmm1
	movups	%xmm0, (%r15,%rbp,4)
	movups	%xmm1, 16(%r15,%rbp,4)
	addq	$8, %rbp
	incq	%rsi
	jne	.LBB23_91
	jmp	.LBB23_93
.LBB23_61:
	xorl	%ebp, %ebp
.LBB23_62:                              # %vector.body.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB23_65
# BB#63:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%rdi,%rbp,4), %rsi
	leaq	112(%r15,%rbp,4), %rbp
	.p2align	4, 0x90
.LBB23_64:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rbp)
	movups	%xmm1, -96(%rbp)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rbp)
	movups	%xmm1, -64(%rbp)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rbp)
	movups	%xmm1, -32(%rbp)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rbp)
	movups	%xmm1, (%rbp)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-32, %rdx
	jne	.LBB23_64
.LBB23_65:                              # %middle.block
	cmpq	%rcx, %rax
	je	.LBB23_72
	jmp	.LBB23_66
.LBB23_92:
	xorl	%ebp, %ebp
.LBB23_93:                              # %vector.body344.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB23_96
# BB#94:                                # %vector.body344.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%rdi,%rbp,4), %rsi
	leaq	112(%r15,%rbp,4), %rbp
	.p2align	4, 0x90
.LBB23_95:                              # %vector.body344
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rbp)
	movups	%xmm1, -96(%rbp)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rbp)
	movups	%xmm1, -64(%rbp)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rbp)
	movups	%xmm1, -32(%rbp)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rbp)
	movups	%xmm1, (%rbp)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-32, %rdx
	jne	.LBB23_95
.LBB23_96:                              # %middle.block345
	cmpq	%rcx, %rax
	je	.LBB23_103
	jmp	.LBB23_97
.Lfunc_end23:
	.size	_ZN16btRaycastVehicle14updateFrictionEf, .Lfunc_end23-_ZN16btRaycastVehicle14updateFrictionEf
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI24_0:
	.long	1065353216              # float 1
	.section	.text._ZN19btWheelContactPointC2EP11btRigidBodyS1_RK9btVector3S4_f,"axG",@progbits,_ZN19btWheelContactPointC2EP11btRigidBodyS1_RK9btVector3S4_f,comdat
	.weak	_ZN19btWheelContactPointC2EP11btRigidBodyS1_RK9btVector3S4_f
	.p2align	4, 0x90
	.type	_ZN19btWheelContactPointC2EP11btRigidBodyS1_RK9btVector3S4_f,@function
_ZN19btWheelContactPointC2EP11btRigidBodyS1_RK9btVector3S4_f: # @_ZN19btWheelContactPointC2EP11btRigidBodyS1_RK9btVector3S4_f
	.cfi_startproc
# BB#0:
	movq	%rsi, (%rdi)
	movq	%rdx, 8(%rdi)
	movups	(%rcx), %xmm1
	movups	%xmm1, 16(%rdi)
	movups	(%r8), %xmm1
	movups	%xmm1, 32(%rdi)
	movss	%xmm0, 52(%rdi)
	movss	(%rcx), %xmm11          # xmm11 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm12         # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm14
	subss	56(%rsi), %xmm14
	movaps	%xmm12, %xmm1
	subss	60(%rsi), %xmm1
	movss	8(%rcx), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm4
	subss	64(%rsi), %xmm4
	movss	8(%r8), %xmm8           # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	mulss	%xmm8, %xmm2
	movss	(%r8), %xmm10           # xmm10 = mem[0],zero,zero,zero
	movss	4(%r8), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm3
	mulss	%xmm9, %xmm3
	subss	%xmm3, %xmm2
	movaps	%xmm4, %xmm3
	mulss	%xmm10, %xmm3
	movaps	%xmm14, %xmm6
	mulss	%xmm8, %xmm6
	subss	%xmm6, %xmm3
	movaps	%xmm14, %xmm7
	mulss	%xmm9, %xmm7
	movaps	%xmm1, %xmm6
	mulss	%xmm10, %xmm6
	subss	%xmm6, %xmm7
	movss	280(%rsi), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm6
	movss	296(%rsi), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm6, %xmm5
	movss	312(%rsi), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm6
	addss	%xmm5, %xmm6
	movss	284(%rsi), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	movss	300(%rsi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	addss	%xmm5, %xmm0
	movss	316(%rsi), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm5
	addss	%xmm0, %xmm5
	mulss	288(%rsi), %xmm2
	mulss	304(%rsi), %xmm3
	addss	%xmm2, %xmm3
	mulss	320(%rsi), %xmm7
	addss	%xmm3, %xmm7
	movaps	%xmm4, %xmm0
	mulss	%xmm5, %xmm0
	mulss	%xmm6, %xmm4
	mulss	%xmm1, %xmm6
	mulss	%xmm7, %xmm1
	subss	%xmm1, %xmm0
	mulss	%xmm14, %xmm7
	subss	%xmm4, %xmm7
	mulss	%xmm14, %xmm5
	subss	%xmm5, %xmm6
	mulss	%xmm10, %xmm0
	mulss	%xmm9, %xmm7
	addss	%xmm0, %xmm7
	mulss	%xmm8, %xmm6
	addss	%xmm7, %xmm6
	addss	360(%rsi), %xmm6
	subss	56(%rdx), %xmm11
	subss	60(%rdx), %xmm12
	subss	64(%rdx), %xmm13
	movaps	%xmm8, %xmm1
	mulss	%xmm12, %xmm1
	movaps	%xmm9, %xmm0
	mulss	%xmm13, %xmm0
	subss	%xmm0, %xmm1
	movaps	%xmm10, %xmm3
	mulss	%xmm13, %xmm3
	movaps	%xmm8, %xmm0
	mulss	%xmm11, %xmm0
	subss	%xmm0, %xmm3
	movaps	%xmm9, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm10, %xmm2
	mulss	%xmm12, %xmm2
	subss	%xmm2, %xmm0
	movss	280(%rdx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	movss	296(%rdx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	addss	%xmm2, %xmm4
	movss	312(%rdx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	addss	%xmm4, %xmm2
	movss	284(%rdx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	movss	300(%rdx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm4, %xmm5
	movss	316(%rdx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm5, %xmm4
	mulss	288(%rdx), %xmm1
	mulss	304(%rdx), %xmm3
	addss	%xmm1, %xmm3
	mulss	320(%rdx), %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm13, %xmm1
	mulss	%xmm4, %xmm1
	mulss	%xmm2, %xmm13
	mulss	%xmm12, %xmm2
	mulss	%xmm0, %xmm12
	subss	%xmm12, %xmm1
	mulss	%xmm11, %xmm0
	subss	%xmm13, %xmm0
	mulss	%xmm11, %xmm4
	subss	%xmm4, %xmm2
	mulss	%xmm10, %xmm1
	mulss	%xmm9, %xmm0
	addss	%xmm1, %xmm0
	mulss	%xmm8, %xmm2
	addss	%xmm0, %xmm2
	addss	360(%rdx), %xmm2
	addss	%xmm6, %xmm2
	movss	.LCPI24_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm2, %xmm0
	movss	%xmm0, 48(%rdi)
	retq
.Lfunc_end24:
	.size	_ZN19btWheelContactPointC2EP11btRigidBodyS1_RK9btVector3S4_f, .Lfunc_end24-_ZN19btWheelContactPointC2EP11btRigidBodyS1_RK9btVector3S4_f
	.cfi_endproc

	.text
	.globl	_ZN16btRaycastVehicle9debugDrawEP12btIDebugDraw
	.p2align	4, 0x90
	.type	_ZN16btRaycastVehicle9debugDrawEP12btIDebugDraw,@function
_ZN16btRaycastVehicle9debugDrawEP12btIDebugDraw: # @_ZN16btRaycastVehicle9debugDrawEP12btIDebugDraw
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi94:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi95:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi97:
	.cfi_def_cfa_offset 112
.Lcfi98:
	.cfi_offset %rbx, -56
.Lcfi99:
	.cfi_offset %r12, -48
.Lcfi100:
	.cfi_offset %r13, -40
.Lcfi101:
	.cfi_offset %r14, -32
.Lcfi102:
	.cfi_offset %r15, -24
.Lcfi103:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	cmpl	$0, 196(%r15)
	jle	.LBB25_3
# BB#1:                                 # %.lr.ph
	xorl	%ebp, %ebp
	movl	$144, %r12d
	.p2align	4, 0x90
.LBB25_2:                               # =>This Inner Loop Header: Depth=1
	movq	208(%r15), %rax
	cmpb	$0, -60(%rax,%r12)
	movl	$0, %ecx
	movl	$1132396544, %edx       # imm = 0x437F0000
	cmovel	%edx, %ecx
	movl	%ecx, (%rsp)
	movl	$0, 4(%rsp)
	movl	$1132396544, 8(%rsp)    # imm = 0x437F0000
	movl	$0, 12(%rsp)
	movups	(%rax,%r12), %xmm0
	movaps	%xmm0, 16(%rsp)
	movslq	176(%r15), %rcx
	leaq	(%rax,%rcx,4), %rax
	movss	-16(%r12,%rax), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movq	(%rbx), %rcx
	movq	40(%rcx), %r8
	movss	-48(%r12,%rax), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	-32(%r12,%rax), %xmm2   # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	addps	16(%rsp), %xmm1
	addss	24(%rsp), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, 40(%rsp)
	movlps	%xmm2, 48(%rsp)
	movq	%rbx, %rdi
	leaq	16(%rsp), %r13
	movq	%r13, %rsi
	leaq	40(%rsp), %rdx
	movq	%rsp, %r14
	movq	%r14, %rcx
	callq	*%r8
	movq	(%rbx), %rax
	movq	208(%r15), %rcx
	leaq	-128(%rcx,%r12), %rdx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%r14, %rcx
	callq	*40(%rax)
	incq	%rbp
	movslq	196(%r15), %rax
	addq	$288, %r12              # imm = 0x120
	cmpq	%rax, %rbp
	jl	.LBB25_2
.LBB25_3:                               # %._crit_edge
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	_ZN16btRaycastVehicle9debugDrawEP12btIDebugDraw, .Lfunc_end25-_ZN16btRaycastVehicle9debugDrawEP12btIDebugDraw
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI26_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN25btDefaultVehicleRaycaster7castRayERK9btVector3S2_RN18btVehicleRaycaster24btVehicleRaycasterResultE
	.p2align	4, 0x90
	.type	_ZN25btDefaultVehicleRaycaster7castRayERK9btVector3S2_RN18btVehicleRaycaster24btVehicleRaycasterResultE,@function
_ZN25btDefaultVehicleRaycaster7castRayERK9btVector3S2_RN18btVehicleRaycaster24btVehicleRaycasterResultE: # @_ZN25btDefaultVehicleRaycaster7castRayERK9btVector3S2_RN18btVehicleRaycaster24btVehicleRaycasterResultE
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 24
	subq	$104, %rsp
.Lcfi106:
	.cfi_def_cfa_offset 128
.Lcfi107:
	.cfi_offset %rbx, -24
.Lcfi108:
	.cfi_offset %r14, -16
	movq	%rcx, %r14
	movl	$1065353216, 16(%rsp)   # imm = 0x3F800000
	movq	$0, 24(%rsp)
	movw	$1, 32(%rsp)
	movw	$-1, 34(%rsp)
	movl	$0, 36(%rsp)
	movq	$_ZTVN16btCollisionWorld24ClosestRayResultCallbackE+16, 8(%rsp)
	movups	(%rsi), %xmm0
	movups	%xmm0, 40(%rsp)
	movups	(%rdx), %xmm0
	movups	%xmm0, 56(%rsp)
	movq	8(%rdi), %rdi
.Ltmp33:
	leaq	8(%rsp), %rcx
	callq	_ZNK16btCollisionWorld7rayTestERK9btVector3S2_RNS_17RayResultCallbackE
.Ltmp34:
# BB#1:
	movq	24(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB26_9
# BB#2:
	cmpl	$2, 256(%rbx)
	jne	.LBB26_9
# BB#3:
	testb	$4, 216(%rbx)
	jne	.LBB26_9
# BB#4:
	movups	88(%rsp), %xmm0
	movups	%xmm0, (%r14)
	movups	72(%rsp), %xmm0
	movups	%xmm0, 16(%r14)
	movss	16(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	24(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB26_6
# BB#5:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB26_6:                               # %.split
	movss	.LCPI26_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	16(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 16(%r14)
	movss	20(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 20(%r14)
	mulss	24(%r14), %xmm0
	movss	%xmm0, 24(%r14)
	movl	16(%rsp), %eax
	movl	%eax, 32(%r14)
	jmp	.LBB26_10
.LBB26_9:
	xorl	%ebx, %ebx
.LBB26_10:                              # %.thread
	movq	%rbx, %rax
	addq	$104, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB26_11:
.Ltmp35:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end26:
	.size	_ZN25btDefaultVehicleRaycaster7castRayERK9btVector3S2_RN18btVehicleRaycaster24btVehicleRaycasterResultE, .Lfunc_end26-_ZN25btDefaultVehicleRaycaster7castRayERK9btVector3S2_RN18btVehicleRaycaster24btVehicleRaycasterResultE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp33-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin3   #     jumps to .Ltmp35
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Lfunc_end26-.Ltmp34    #   Call between .Ltmp34 and .Lfunc_end26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN18btVehicleRaycasterD2Ev,"axG",@progbits,_ZN18btVehicleRaycasterD2Ev,comdat
	.weak	_ZN18btVehicleRaycasterD2Ev
	.p2align	4, 0x90
	.type	_ZN18btVehicleRaycasterD2Ev,@function
_ZN18btVehicleRaycasterD2Ev:            # @_ZN18btVehicleRaycasterD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end27:
	.size	_ZN18btVehicleRaycasterD2Ev, .Lfunc_end27-_ZN18btVehicleRaycasterD2Ev
	.cfi_endproc

	.section	.text._ZN25btDefaultVehicleRaycasterD0Ev,"axG",@progbits,_ZN25btDefaultVehicleRaycasterD0Ev,comdat
	.weak	_ZN25btDefaultVehicleRaycasterD0Ev
	.p2align	4, 0x90
	.type	_ZN25btDefaultVehicleRaycasterD0Ev,@function
_ZN25btDefaultVehicleRaycasterD0Ev:     # @_ZN25btDefaultVehicleRaycasterD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end28:
	.size	_ZN25btDefaultVehicleRaycasterD0Ev, .Lfunc_end28-_ZN25btDefaultVehicleRaycasterD0Ev
	.cfi_endproc

	.section	.text._ZN16btRaycastVehicle12updateActionEP16btCollisionWorldf,"axG",@progbits,_ZN16btRaycastVehicle12updateActionEP16btCollisionWorldf,comdat
	.weak	_ZN16btRaycastVehicle12updateActionEP16btCollisionWorldf
	.p2align	4, 0x90
	.type	_ZN16btRaycastVehicle12updateActionEP16btCollisionWorldf,@function
_ZN16btRaycastVehicle12updateActionEP16btCollisionWorldf: # @_ZN16btRaycastVehicle12updateActionEP16btCollisionWorldf
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end29:
	.size	_ZN16btRaycastVehicle12updateActionEP16btCollisionWorldf, .Lfunc_end29-_ZN16btRaycastVehicle12updateActionEP16btCollisionWorldf
	.cfi_endproc

	.section	.text._ZN16btRaycastVehicle19setCoordinateSystemEiii,"axG",@progbits,_ZN16btRaycastVehicle19setCoordinateSystemEiii,comdat
	.weak	_ZN16btRaycastVehicle19setCoordinateSystemEiii
	.p2align	4, 0x90
	.type	_ZN16btRaycastVehicle19setCoordinateSystemEiii,@function
_ZN16btRaycastVehicle19setCoordinateSystemEiii: # @_ZN16btRaycastVehicle19setCoordinateSystemEiii
	.cfi_startproc
# BB#0:
	movl	%esi, 176(%rdi)
	movl	%edx, 180(%rdi)
	movl	%ecx, 184(%rdi)
	retq
.Lfunc_end30:
	.size	_ZN16btRaycastVehicle19setCoordinateSystemEiii, .Lfunc_end30-_ZN16btRaycastVehicle19setCoordinateSystemEiii
	.cfi_endproc

	.section	.text._ZN16btCollisionWorld24ClosestRayResultCallbackD0Ev,"axG",@progbits,_ZN16btCollisionWorld24ClosestRayResultCallbackD0Ev,comdat
	.weak	_ZN16btCollisionWorld24ClosestRayResultCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld24ClosestRayResultCallbackD0Ev,@function
_ZN16btCollisionWorld24ClosestRayResultCallbackD0Ev: # @_ZN16btCollisionWorld24ClosestRayResultCallbackD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end31:
	.size	_ZN16btCollisionWorld24ClosestRayResultCallbackD0Ev, .Lfunc_end31-_ZN16btCollisionWorld24ClosestRayResultCallbackD0Ev
	.cfi_endproc

	.section	.text._ZNK16btCollisionWorld17RayResultCallback14needsCollisionEP17btBroadphaseProxy,"axG",@progbits,_ZNK16btCollisionWorld17RayResultCallback14needsCollisionEP17btBroadphaseProxy,comdat
	.weak	_ZNK16btCollisionWorld17RayResultCallback14needsCollisionEP17btBroadphaseProxy
	.p2align	4, 0x90
	.type	_ZNK16btCollisionWorld17RayResultCallback14needsCollisionEP17btBroadphaseProxy,@function
_ZNK16btCollisionWorld17RayResultCallback14needsCollisionEP17btBroadphaseProxy: # @_ZNK16btCollisionWorld17RayResultCallback14needsCollisionEP17btBroadphaseProxy
	.cfi_startproc
# BB#0:
	movzwl	26(%rdi), %eax
	testw	8(%rsi), %ax
	je	.LBB32_1
# BB#2:
	movzwl	10(%rsi), %eax
	testw	24(%rdi), %ax
	setne	%al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB32_1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end32:
	.size	_ZNK16btCollisionWorld17RayResultCallback14needsCollisionEP17btBroadphaseProxy, .Lfunc_end32-_ZNK16btCollisionWorld17RayResultCallback14needsCollisionEP17btBroadphaseProxy
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI33_0:
	.long	1065353216              # float 1
	.section	.text._ZN16btCollisionWorld24ClosestRayResultCallback15addSingleResultERNS_14LocalRayResultEb,"axG",@progbits,_ZN16btCollisionWorld24ClosestRayResultCallback15addSingleResultERNS_14LocalRayResultEb,comdat
	.weak	_ZN16btCollisionWorld24ClosestRayResultCallback15addSingleResultERNS_14LocalRayResultEb
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld24ClosestRayResultCallback15addSingleResultERNS_14LocalRayResultEb,@function
_ZN16btCollisionWorld24ClosestRayResultCallback15addSingleResultERNS_14LocalRayResultEb: # @_ZN16btCollisionWorld24ClosestRayResultCallback15addSingleResultERNS_14LocalRayResultEb
	.cfi_startproc
# BB#0:
	movl	32(%rsi), %eax
	movl	%eax, 8(%rdi)
	movq	(%rsi), %rax
	movq	%rax, 16(%rdi)
	testb	%dl, %dl
	je	.LBB33_2
# BB#1:
	movups	16(%rsi), %xmm0
	movups	%xmm0, 64(%rdi)
	jmp	.LBB33_3
.LBB33_2:
	movss	16(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	20(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	24(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	24(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	movss	28(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	addps	%xmm3, %xmm4
	movss	32(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm4, %xmm3
	mulss	40(%rax), %xmm2
	mulss	44(%rax), %xmm1
	addss	%xmm2, %xmm1
	mulss	48(%rax), %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm3, 64(%rdi)
	movlps	%xmm1, 72(%rdi)
.LBB33_3:
	movss	32(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	.LCPI33_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	32(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	movss	48(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm2, %xmm3
	movss	%xmm3, 80(%rdi)
	movss	36(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	movss	52(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm2, %xmm3
	movss	%xmm3, 84(%rdi)
	mulss	40(%rdi), %xmm1
	mulss	56(%rdi), %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, 88(%rdi)
	movss	32(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end33:
	.size	_ZN16btCollisionWorld24ClosestRayResultCallback15addSingleResultERNS_14LocalRayResultEb, .Lfunc_end33-_ZN16btCollisionWorld24ClosestRayResultCallback15addSingleResultERNS_14LocalRayResultEb
	.cfi_endproc

	.section	.text._ZN16btCollisionWorld17RayResultCallbackD2Ev,"axG",@progbits,_ZN16btCollisionWorld17RayResultCallbackD2Ev,comdat
	.weak	_ZN16btCollisionWorld17RayResultCallbackD2Ev
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld17RayResultCallbackD2Ev,@function
_ZN16btCollisionWorld17RayResultCallbackD2Ev: # @_ZN16btCollisionWorld17RayResultCallbackD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end34:
	.size	_ZN16btCollisionWorld17RayResultCallbackD2Ev, .Lfunc_end34-_ZN16btCollisionWorld17RayResultCallbackD2Ev
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_btRaycastVehicle.ii,@function
_GLOBAL__sub_I_btRaycastVehicle.ii:     # @_GLOBAL__sub_I_btRaycastVehicle.ii
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi109:
	.cfi_def_cfa_offset 32
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movq	%rsp, %rcx
	movl	$_ZL13s_fixedObject, %edi
	xorps	%xmm0, %xmm0
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	_ZN11btRigidBodyC1EfP13btMotionStateP16btCollisionShapeRK9btVector3
	movl	$_ZN11btRigidBodyD2Ev, %edi
	movl	$_ZL13s_fixedObject, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	addq	$24, %rsp
	retq
.Lfunc_end35:
	.size	_GLOBAL__sub_I_btRaycastVehicle.ii, .Lfunc_end35-_GLOBAL__sub_I_btRaycastVehicle.ii
	.cfi_endproc

	.type	_ZL13s_fixedObject,@object # @_ZL13s_fixedObject
	.local	_ZL13s_fixedObject
	.comm	_ZL13s_fixedObject,568,8
	.type	_ZTV16btRaycastVehicle,@object # @_ZTV16btRaycastVehicle
	.section	.rodata,"a",@progbits
	.globl	_ZTV16btRaycastVehicle
	.p2align	3
_ZTV16btRaycastVehicle:
	.quad	0
	.quad	_ZTI16btRaycastVehicle
	.quad	_ZN16btRaycastVehicleD2Ev
	.quad	_ZN16btRaycastVehicleD0Ev
	.quad	_ZN16btRaycastVehicle12updateActionEP16btCollisionWorldf
	.quad	_ZN16btRaycastVehicle9debugDrawEP12btIDebugDraw
	.quad	_ZN16btRaycastVehicle13updateVehicleEf
	.quad	_ZN16btRaycastVehicle14updateFrictionEf
	.quad	_ZN16btRaycastVehicle19setCoordinateSystemEiii
	.size	_ZTV16btRaycastVehicle, 72

	.type	sideFrictionStiffness2,@object # @sideFrictionStiffness2
	.data
	.globl	sideFrictionStiffness2
	.p2align	2
sideFrictionStiffness2:
	.long	1065353216              # float 1
	.size	sideFrictionStiffness2, 4

	.type	_ZTV25btDefaultVehicleRaycaster,@object # @_ZTV25btDefaultVehicleRaycaster
	.section	.rodata,"a",@progbits
	.globl	_ZTV25btDefaultVehicleRaycaster
	.p2align	3
_ZTV25btDefaultVehicleRaycaster:
	.quad	0
	.quad	_ZTI25btDefaultVehicleRaycaster
	.quad	_ZN18btVehicleRaycasterD2Ev
	.quad	_ZN25btDefaultVehicleRaycasterD0Ev
	.quad	_ZN25btDefaultVehicleRaycaster7castRayERK9btVector3S2_RN18btVehicleRaycaster24btVehicleRaycasterResultE
	.size	_ZTV25btDefaultVehicleRaycaster, 40

	.type	_ZTS25btDefaultVehicleRaycaster,@object # @_ZTS25btDefaultVehicleRaycaster
	.globl	_ZTS25btDefaultVehicleRaycaster
	.p2align	4
_ZTS25btDefaultVehicleRaycaster:
	.asciz	"25btDefaultVehicleRaycaster"
	.size	_ZTS25btDefaultVehicleRaycaster, 28

	.type	_ZTS18btVehicleRaycaster,@object # @_ZTS18btVehicleRaycaster
	.section	.rodata._ZTS18btVehicleRaycaster,"aG",@progbits,_ZTS18btVehicleRaycaster,comdat
	.weak	_ZTS18btVehicleRaycaster
	.p2align	4
_ZTS18btVehicleRaycaster:
	.asciz	"18btVehicleRaycaster"
	.size	_ZTS18btVehicleRaycaster, 21

	.type	_ZTI18btVehicleRaycaster,@object # @_ZTI18btVehicleRaycaster
	.section	.rodata._ZTI18btVehicleRaycaster,"aG",@progbits,_ZTI18btVehicleRaycaster,comdat
	.weak	_ZTI18btVehicleRaycaster
	.p2align	3
_ZTI18btVehicleRaycaster:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS18btVehicleRaycaster
	.size	_ZTI18btVehicleRaycaster, 16

	.type	_ZTI25btDefaultVehicleRaycaster,@object # @_ZTI25btDefaultVehicleRaycaster
	.section	.rodata,"a",@progbits
	.globl	_ZTI25btDefaultVehicleRaycaster
	.p2align	4
_ZTI25btDefaultVehicleRaycaster:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS25btDefaultVehicleRaycaster
	.quad	_ZTI18btVehicleRaycaster
	.size	_ZTI25btDefaultVehicleRaycaster, 24

	.type	_ZTS16btRaycastVehicle,@object # @_ZTS16btRaycastVehicle
	.globl	_ZTS16btRaycastVehicle
	.p2align	4
_ZTS16btRaycastVehicle:
	.asciz	"16btRaycastVehicle"
	.size	_ZTS16btRaycastVehicle, 19

	.type	_ZTS17btActionInterface,@object # @_ZTS17btActionInterface
	.section	.rodata._ZTS17btActionInterface,"aG",@progbits,_ZTS17btActionInterface,comdat
	.weak	_ZTS17btActionInterface
	.p2align	4
_ZTS17btActionInterface:
	.asciz	"17btActionInterface"
	.size	_ZTS17btActionInterface, 20

	.type	_ZTI17btActionInterface,@object # @_ZTI17btActionInterface
	.section	.rodata._ZTI17btActionInterface,"aG",@progbits,_ZTI17btActionInterface,comdat
	.weak	_ZTI17btActionInterface
	.p2align	3
_ZTI17btActionInterface:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS17btActionInterface
	.size	_ZTI17btActionInterface, 16

	.type	_ZTI16btRaycastVehicle,@object # @_ZTI16btRaycastVehicle
	.section	.rodata,"a",@progbits
	.globl	_ZTI16btRaycastVehicle
	.p2align	4
_ZTI16btRaycastVehicle:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS16btRaycastVehicle
	.quad	_ZTI17btActionInterface
	.size	_ZTI16btRaycastVehicle, 24

	.type	_ZTVN16btCollisionWorld24ClosestRayResultCallbackE,@object # @_ZTVN16btCollisionWorld24ClosestRayResultCallbackE
	.section	.rodata._ZTVN16btCollisionWorld24ClosestRayResultCallbackE,"aG",@progbits,_ZTVN16btCollisionWorld24ClosestRayResultCallbackE,comdat
	.weak	_ZTVN16btCollisionWorld24ClosestRayResultCallbackE
	.p2align	3
_ZTVN16btCollisionWorld24ClosestRayResultCallbackE:
	.quad	0
	.quad	_ZTIN16btCollisionWorld24ClosestRayResultCallbackE
	.quad	_ZN16btCollisionWorld17RayResultCallbackD2Ev
	.quad	_ZN16btCollisionWorld24ClosestRayResultCallbackD0Ev
	.quad	_ZNK16btCollisionWorld17RayResultCallback14needsCollisionEP17btBroadphaseProxy
	.quad	_ZN16btCollisionWorld24ClosestRayResultCallback15addSingleResultERNS_14LocalRayResultEb
	.size	_ZTVN16btCollisionWorld24ClosestRayResultCallbackE, 48

	.type	_ZTSN16btCollisionWorld24ClosestRayResultCallbackE,@object # @_ZTSN16btCollisionWorld24ClosestRayResultCallbackE
	.section	.rodata._ZTSN16btCollisionWorld24ClosestRayResultCallbackE,"aG",@progbits,_ZTSN16btCollisionWorld24ClosestRayResultCallbackE,comdat
	.weak	_ZTSN16btCollisionWorld24ClosestRayResultCallbackE
	.p2align	4
_ZTSN16btCollisionWorld24ClosestRayResultCallbackE:
	.asciz	"N16btCollisionWorld24ClosestRayResultCallbackE"
	.size	_ZTSN16btCollisionWorld24ClosestRayResultCallbackE, 47

	.type	_ZTSN16btCollisionWorld17RayResultCallbackE,@object # @_ZTSN16btCollisionWorld17RayResultCallbackE
	.section	.rodata._ZTSN16btCollisionWorld17RayResultCallbackE,"aG",@progbits,_ZTSN16btCollisionWorld17RayResultCallbackE,comdat
	.weak	_ZTSN16btCollisionWorld17RayResultCallbackE
	.p2align	4
_ZTSN16btCollisionWorld17RayResultCallbackE:
	.asciz	"N16btCollisionWorld17RayResultCallbackE"
	.size	_ZTSN16btCollisionWorld17RayResultCallbackE, 40

	.type	_ZTIN16btCollisionWorld17RayResultCallbackE,@object # @_ZTIN16btCollisionWorld17RayResultCallbackE
	.section	.rodata._ZTIN16btCollisionWorld17RayResultCallbackE,"aG",@progbits,_ZTIN16btCollisionWorld17RayResultCallbackE,comdat
	.weak	_ZTIN16btCollisionWorld17RayResultCallbackE
	.p2align	3
_ZTIN16btCollisionWorld17RayResultCallbackE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN16btCollisionWorld17RayResultCallbackE
	.size	_ZTIN16btCollisionWorld17RayResultCallbackE, 16

	.type	_ZTIN16btCollisionWorld24ClosestRayResultCallbackE,@object # @_ZTIN16btCollisionWorld24ClosestRayResultCallbackE
	.section	.rodata._ZTIN16btCollisionWorld24ClosestRayResultCallbackE,"aG",@progbits,_ZTIN16btCollisionWorld24ClosestRayResultCallbackE,comdat
	.weak	_ZTIN16btCollisionWorld24ClosestRayResultCallbackE
	.p2align	4
_ZTIN16btCollisionWorld24ClosestRayResultCallbackE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN16btCollisionWorld24ClosestRayResultCallbackE
	.quad	_ZTIN16btCollisionWorld17RayResultCallbackE
	.size	_ZTIN16btCollisionWorld24ClosestRayResultCallbackE, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_btRaycastVehicle.ii

	.globl	_ZN16btRaycastVehicleC1ERKNS_15btVehicleTuningEP11btRigidBodyP18btVehicleRaycaster
	.type	_ZN16btRaycastVehicleC1ERKNS_15btVehicleTuningEP11btRigidBodyP18btVehicleRaycaster,@function
_ZN16btRaycastVehicleC1ERKNS_15btVehicleTuningEP11btRigidBodyP18btVehicleRaycaster = _ZN16btRaycastVehicleC2ERKNS_15btVehicleTuningEP11btRigidBodyP18btVehicleRaycaster
	.globl	_ZN16btRaycastVehicleD1Ev
	.type	_ZN16btRaycastVehicleD1Ev,@function
_ZN16btRaycastVehicleD1Ev = _ZN16btRaycastVehicleD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
