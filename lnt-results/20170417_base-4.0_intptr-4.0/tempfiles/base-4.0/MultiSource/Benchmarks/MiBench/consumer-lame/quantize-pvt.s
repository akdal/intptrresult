	.text
	.file	"quantize-pvt.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4608683618675807573     # double 1.3333333333333333
.LCPI0_1:
	.quad	4602678819172646912     # double 0.5
.LCPI0_2:
	.quad	4604930618986332160     # double 0.75
.LCPI0_3:
	.quad	-4627448617123184640    # double -0.1875
.LCPI0_4:
	.quad	4598175219545276416     # double 0.25
	.text
	.globl	iteration_init
	.p2align	4, 0x90
	.type	iteration_init,@function
iteration_init:                         # @iteration_init
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$0, 8(%r15)
	cmpq	$0, 168(%r14)
	jne	.LBB0_11
# BB#1:                                 # %.preheader79
	movq	$-88, %rax
	jmp	.LBB0_2
	.p2align	4, 0x90
.LBB0_29:                               #   in Loop: Header=BB0_2 Depth=1
	movslq	192(%r14), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movslq	224(%r14), %rdx
	addq	%rcx, %rdx
	imulq	$148, %rdx, %rcx
	movl	sfBandIndex+92(%rcx,%rax), %ecx
	movl	%ecx, scalefac_band+92(%rax)
	addq	$8, %rax
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movslq	192(%r14), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movslq	224(%r14), %rdx
	addq	%rcx, %rdx
	imulq	$148, %rdx, %rcx
	movl	sfBandIndex+88(%rcx,%rax), %ecx
	movl	%ecx, scalefac_band+88(%rax)
	testq	%rax, %rax
	jne	.LBB0_29
# BB#3:                                 # %.preheader78
	movslq	192(%r14), %rax
	leaq	(%rax,%rax,2), %rax
	movslq	224(%r14), %rcx
	addq	%rax, %rcx
	imulq	$148, %rcx, %rax
	movups	sfBandIndex+92(%rax), %xmm0
	movups	%xmm0, scalefac_band+92(%rip)
	movups	sfBandIndex+108(%rax), %xmm0
	movups	%xmm0, scalefac_band+108(%rip)
	movupd	sfBandIndex+124(%rax), %xmm0
	movupd	%xmm0, scalefac_band+124(%rip)
	movl	sfBandIndex+140(%rax), %ecx
	movl	%ecx, scalefac_band+140(%rip)
	movl	sfBandIndex+144(%rax), %eax
	movl	%eax, scalefac_band+144(%rip)
	movl	$0, (%r15)
	movl	$ATH_l, %esi
	movl	$ATH_s, %edx
	movq	%r14, %rdi
	callq	compute_ath
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_4:                                # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	movsd	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	movsd	%xmm0, pow43(,%rbx,8)
	leal	1(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	movsd	%xmm0, pow43+8(,%rbx,8)
	leal	2(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	movsd	%xmm0, pow43+16(,%rbx,8)
	leal	3(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	movsd	%xmm0, pow43+24(,%rbx,8)
	leal	4(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	movsd	%xmm0, pow43+32(,%rbx,8)
	leal	5(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	movsd	%xmm0, pow43+40(,%rbx,8)
	addq	$6, %rbx
	cmpq	$8208, %rbx             # imm = 0x2010
	jne	.LBB0_4
# BB#5:                                 # %.preheader77.preheader
	movq	$-8206, %rbx            # imm = 0xDFF2
	jmp	.LBB0_6
	.p2align	4, 0x90
.LBB0_28:                               # %.preheader77.1
                                        #   in Loop: Header=BB0_6 Depth=1
	leal	8208(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	pow43+65656(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	addsd	pow43+65664(,%rbx,8), %xmm0
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	.LCPI0_2(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, adj43+65656(,%rbx,8)
	addq	$2, %rbx
.LBB0_6:                                # %.preheader77
                                        # =>This Inner Loop Header: Depth=1
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	leal	8207(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	pow43+65648(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	addsd	pow43+65656(,%rbx,8), %xmm0
	mulsd	%xmm1, %xmm0
	movsd	.LCPI0_2(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, adj43+65648(,%rbx,8)
	testq	%rbx, %rbx
	jne	.LBB0_28
# BB#7:
	movabsq	$4602678819172646912, %rax # imm = 0x3FE0000000000000
	movq	%rax, adj43+65656(%rip)
	movq	$-65648, %rbx           # imm = 0xFFFEFF90
	jmp	.LBB0_8
	.p2align	4, 0x90
.LBB0_27:                               #   in Loop: Header=BB0_8 Depth=1
	movsd	pow43+65656(%rbx), %xmm0 # xmm0 = mem[0],zero
	addsd	pow43+65664(%rbx), %xmm0
	mulsd	.LCPI0_1(%rip), %xmm0
	movsd	.LCPI0_2(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	addq	$16, %rbx
.LBB0_8:                                # =>This Inner Loop Header: Depth=1
	movsd	pow43+65648(%rbx), %xmm0 # xmm0 = mem[0],zero
	addsd	pow43+65656(%rbx), %xmm0
	mulsd	.LCPI0_1(%rip), %xmm0
	movsd	.LCPI0_2(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	testq	%rbx, %rbx
	jne	.LBB0_27
# BB#9:                                 # %.preheader76.preheader
	movq	$-2048, %rbx            # imm = 0xF800
	movl	$-210, %r12d
	.p2align	4, 0x90
.LBB0_10:                               # %.preheader76
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	.LCPI0_3(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	callq	exp2
	movsd	%xmm0, ipow20+2048(%rbx)
	movsd	.LCPI0_4(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	callq	exp2
	movsd	%xmm0, pow20+2048(%rbx)
	leal	1(%r12), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	mulsd	.LCPI0_3(%rip), %xmm0
	callq	exp2
	movsd	%xmm0, ipow20+2056(%rbx)
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI0_4(%rip), %xmm0
	callq	exp2
	movsd	%xmm0, pow20+2056(%rbx)
	addl	$2, %r12d
	addq	$16, %rbx
	jne	.LBB0_10
.LBB0_11:                               # %.loopexit
	movl	$0, convert_mdct(%rip)
	movl	$0, reduce_sidechannel(%rip)
	cmpl	$2, 228(%r14)
	jne	.LBB0_13
# BB#12:
	movl	$1, convert_mdct(%rip)
	movl	$1, reduce_sidechannel(%rip)
.LBB0_13:                               # %.preheader75
	movslq	200(%r14), %r8
	testq	%r8, %r8
	movslq	204(%r14), %rax
	jle	.LBB0_23
# BB#14:                                # %.preheader74.lr.ph
	testl	%eax, %eax
	jle	.LBB0_26
# BB#15:                                # %.preheader74.us.preheader
	movl	%eax, %r9d
	andl	$1, %r9d
	movq	%r15, %r11
	subq	$-128, %r11
	xorl	%edi, %edi
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_16:                               # %.preheader74.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_21 Depth 2
	testq	%r9, %r9
	jne	.LBB0_18
# BB#17:                                #   in Loop: Header=BB0_16 Depth=1
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	jne	.LBB0_20
	jmp	.LBB0_22
	.p2align	4, 0x90
.LBB0_18:                               #   in Loop: Header=BB0_16 Depth=1
	imulq	$240, %r12, %r10
	cmpl	$2, 72(%r15,%r10)
	movl	$21, %edx
	cmovel	%edi, %edx
	movl	$12, %ecx
	cmovel	%edi, %ecx
	movl	%edx, 128(%r15,%r10)
	movl	%ecx, 132(%r15,%r10)
	movl	$1, %ecx
	cmpl	$1, %eax
	je	.LBB0_22
.LBB0_20:                               # %.preheader74.us.new
                                        #   in Loop: Header=BB0_16 Depth=1
	imulq	$120, %rcx, %rdx
	addq	%r11, %rdx
	.p2align	4, 0x90
.LBB0_21:                               #   Parent Loop BB0_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$2, -56(%rdx)
	movl	$21, %esi
	cmovel	%edi, %esi
	movl	$12, %ebx
	cmovel	%edi, %ebx
	movl	%esi, (%rdx)
	movl	%ebx, 4(%rdx)
	cmpl	$2, 64(%rdx)
	movl	$21, %esi
	cmovel	%edi, %esi
	movl	$12, %ebx
	cmovel	%edi, %ebx
	movl	%esi, 120(%rdx)
	movl	%ebx, 124(%rdx)
	addq	$2, %rcx
	addq	$240, %rdx
	cmpq	%rax, %rcx
	jl	.LBB0_21
.LBB0_22:                               # %._crit_edge83.us
                                        #   in Loop: Header=BB0_16 Depth=1
	incq	%r12
	addq	$240, %r11
	cmpq	%r8, %r12
	jl	.LBB0_16
.LBB0_23:                               # %.preheader73
	testl	%eax, %eax
	jle	.LBB0_26
# BB#24:                                # %.preheader.preheader
	addq	$12, %r15
	xorl	%eax, %eax
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_25:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm0, (%r15)
	incq	%rax
	movslq	204(%r14), %rcx
	addq	$16, %r15
	cmpq	%rcx, %rax
	jl	.LBB0_25
.LBB0_26:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	iteration_init, .Lfunc_end0-iteration_init
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4652007308841189376     # double 1000
.LCPI1_1:
	.quad	4652781365027143680     # double 1152
.LCPI1_2:
	.quad	4581421828931458171     # double 0.02
.LCPI1_3:
	.quad	-4617991057905706598    # double -0.80000000000000004
.LCPI1_4:
	.quad	4615378970121831711     # double 3.6400000000000001
.LCPI1_5:
	.quad	-4608758678669597082    # double -3.2999999999999998
.LCPI1_6:
	.quad	-4619792497756654797    # double -0.59999999999999998
.LCPI1_7:
	.quad	-4604367669032910848    # double -6.5
.LCPI1_8:
	.quad	4616189618054758400     # double 4
.LCPI1_9:
	.quad	4562254508917369340     # double 0.001
.LCPI1_11:
	.quad	4621819117588971520     # double 10
.LCPI1_12:
	.quad	4645463015632666624     # double 384
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	3
.LCPI1_10:
	.quad	-4582131145872769024    # double -200
	.quad	-4585649583081652224    # double -114
	.text
	.globl	compute_ath
	.p2align	4, 0x90
	.type	compute_ath,@function
compute_ath:                            # @compute_ath
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 96
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	cvtsi2sdl	16(%r14), %xmm4
	divsd	.LCPI1_0(%rip), %xmm4
	xorl	%eax, %eax
	movsd	.LCPI1_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI1_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI1_3(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	%xmm4, 24(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_4 Depth 2
	movq	%rax, %rbp
	movl	scalefac_band(,%rbp,4), %r12d
	leaq	1(%rbp), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	scalefac_band+4(,%rbp,4), %r13d
	movabsq	$6088095589093318446, %rax # imm = 0x547D42AEA2879F2E
	movq	%rax, (%rbx,%rbp,8)
	cmpl	%r13d, %r12d
	jge	.LBB1_2
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph62
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%r12d, %xmm5
	mulsd	%xmm4, %xmm5
	divsd	%xmm3, %xmm5
	movapd	%xmm2, %xmm0
	maxsd	%xmm5, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	callq	pow
	mulsd	.LCPI1_4(%rip), %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	.LCPI1_5(%rip), %xmm0
	mulsd	%xmm0, %xmm0
	mulsd	.LCPI1_6(%rip), %xmm0
	callq	exp
	mulsd	.LCPI1_7(%rip), %xmm0
	addsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	.LCPI1_8(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	movapd	%xmm0, %xmm1
	mulsd	.LCPI1_9(%rip), %xmm1
	xorl	%eax, %eax
	cmpl	$0, 148(%r14)
	addsd	8(%rsp), %xmm1          # 8-byte Folded Reload
	sete	%al
	addsd	.LCPI1_10(,%rax,8), %xmm1
	movsd	.LCPI1_11(%rip), %xmm0  # xmm0 = mem[0],zero
	divsd	%xmm0, %xmm1
	callq	pow
	movsd	.LCPI1_3(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI1_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI1_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	(%rbx,%rbp,8), %xmm4    # xmm4 = mem[0],zero
	minsd	%xmm0, %xmm4
	movsd	%xmm4, (%rbx,%rbp,8)
	movsd	24(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	incl	%r12d
	cmpl	%r12d, %r13d
	jne	.LBB1_4
.LBB1_2:                                # %.loopexit58
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpq	$21, %rax
	jne	.LBB1_1
# BB#3:                                 # %.preheader.preheader
	xorl	%r12d, %r12d
	movsd	.LCPI1_12(%rip), %xmm1  # xmm1 = mem[0],zero
	movsd	.LCPI1_3(%rip), %xmm3   # xmm3 = mem[0],zero
	.p2align	4, 0x90
.LBB1_6:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_7 Depth 2
	movq	%r12, %rbx
	movl	scalefac_band+92(,%rbx,4), %ebp
	leaq	1(%rbx), %r12
	movl	scalefac_band+96(,%rbx,4), %r13d
	movabsq	$6088095589093318446, %rax # imm = 0x547D42AEA2879F2E
	movq	%rax, (%r15,%rbx,8)
	cmpl	%r13d, %ebp
	jge	.LBB1_5
	.p2align	4, 0x90
.LBB1_7:                                # %.lr.ph
                                        #   Parent Loop BB1_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%ebp, %xmm5
	mulsd	%xmm4, %xmm5
	divsd	%xmm1, %xmm5
	movapd	%xmm2, %xmm0
	maxsd	%xmm5, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movaps	%xmm3, %xmm1
	callq	pow
	mulsd	.LCPI1_4(%rip), %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	.LCPI1_5(%rip), %xmm0
	mulsd	%xmm0, %xmm0
	mulsd	.LCPI1_6(%rip), %xmm0
	callq	exp
	mulsd	.LCPI1_7(%rip), %xmm0
	addsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	.LCPI1_8(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	movapd	%xmm0, %xmm1
	mulsd	.LCPI1_9(%rip), %xmm1
	xorl	%eax, %eax
	cmpl	$0, 148(%r14)
	addsd	8(%rsp), %xmm1          # 8-byte Folded Reload
	sete	%al
	addsd	.LCPI1_10(,%rax,8), %xmm1
	movsd	.LCPI1_11(%rip), %xmm0  # xmm0 = mem[0],zero
	divsd	%xmm0, %xmm1
	callq	pow
	movsd	.LCPI1_3(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI1_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	(%r15,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	minsd	%xmm0, %xmm1
	movsd	%xmm1, (%r15,%rbx,8)
	movsd	.LCPI1_12(%rip), %xmm1  # xmm1 = mem[0],zero
	movsd	24(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	incl	%ebp
	cmpl	%ebp, %r13d
	jne	.LBB1_7
.LBB1_5:                                # %.loopexit
                                        #   in Loop: Header=BB1_6 Depth=1
	cmpq	$12, %r12
	jne	.LBB1_6
# BB#8:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	compute_ath, .Lfunc_end1-compute_ath
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4581421828931458171     # double 0.02
.LCPI2_1:
	.quad	-4617991057905706598    # double -0.80000000000000004
.LCPI2_2:
	.quad	4615378970121831711     # double 3.6400000000000001
.LCPI2_3:
	.quad	-4608758678669597082    # double -3.2999999999999998
.LCPI2_4:
	.quad	-4619792497756654797    # double -0.59999999999999998
.LCPI2_5:
	.quad	-4604367669032910848    # double -6.5
.LCPI2_6:
	.quad	4616189618054758400     # double 4
.LCPI2_7:
	.quad	4562254508917369340     # double 0.001
.LCPI2_9:
	.quad	4621819117588971520     # double 10
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	3
.LCPI2_8:
	.quad	-4582131145872769024    # double -200
	.quad	-4585649583081652224    # double -114
	.text
	.globl	ATHformula
	.p2align	4, 0x90
	.type	ATHformula,@function
ATHformula:                             # @ATHformula
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movsd	.LCPI2_0(%rip), %xmm2   # xmm2 = mem[0],zero
	maxsd	%xmm0, %xmm2
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	movsd	.LCPI2_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm2, %xmm0
	callq	pow
	mulsd	.LCPI2_2(%rip), %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	.LCPI2_3(%rip), %xmm0   # xmm0 = mem[0],zero
	addsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	mulsd	%xmm0, %xmm0
	mulsd	.LCPI2_4(%rip), %xmm0
	callq	exp
	mulsd	.LCPI2_5(%rip), %xmm0
	addsd	(%rsp), %xmm0           # 8-byte Folded Reload
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	.LCPI2_6(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	pow
	movapd	%xmm0, %xmm1
	mulsd	.LCPI2_7(%rip), %xmm1
	addsd	(%rsp), %xmm1           # 8-byte Folded Reload
	xorl	%eax, %eax
	cmpl	$0, 148(%rbx)
	sete	%al
	addsd	.LCPI2_8(,%rax,8), %xmm1
	movsd	.LCPI2_9(%rip), %xmm0   # xmm0 = mem[0],zero
	divsd	%xmm0, %xmm1
	addq	$16, %rsp
	popq	%rbx
	jmp	pow                     # TAILCALL
.Lfunc_end2:
	.size	ATHformula, .Lfunc_end2-ATHformula
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.quad	4604544271217802189     # double 0.70710678118654757
	.quad	4604544271217802189     # double 0.70710678118654757
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_1:
	.quad	4604544271217802189     # double 0.70710678118654757
	.text
	.globl	ms_convert
	.p2align	4, 0x90
	.type	ms_convert,@function
ms_convert:                             # @ms_convert
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	leaq	9216(%rsi), %rax
	cmpq	%rdi, %rax
	jbe	.LBB3_2
# BB#1:                                 # %min.iters.checked
	leaq	9216(%rdi), %rax
	cmpq	%rsi, %rax
	jbe	.LBB3_2
# BB#4:                                 # %scalar.ph.preheader
	movq	$-576, %rax             # imm = 0xFDC0
	movsd	.LCPI3_1(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB3_5:                                # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	4608(%rsi,%rax,8), %xmm1 # xmm1 = mem[0],zero
	movsd	9216(%rsi,%rax,8), %xmm2 # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm3
	addsd	%xmm2, %xmm3
	mulsd	%xmm0, %xmm3
	movsd	%xmm3, 4608(%rdi,%rax,8)
	subsd	%xmm2, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 9216(%rdi,%rax,8)
	movsd	4616(%rsi,%rax,8), %xmm1 # xmm1 = mem[0],zero
	movsd	9224(%rsi,%rax,8), %xmm2 # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm3
	addsd	%xmm2, %xmm3
	mulsd	%xmm0, %xmm3
	movsd	%xmm3, 4616(%rdi,%rax,8)
	subsd	%xmm2, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 9224(%rdi,%rax,8)
	addq	$2, %rax
	jne	.LBB3_5
	jmp	.LBB3_6
.LBB3_2:                                # %vector.body.preheader
	movq	$-576, %rax             # imm = 0xFDC0
	movapd	.LCPI3_0(%rip), %xmm0   # xmm0 = [7.071068e-01,7.071068e-01]
	.p2align	4, 0x90
.LBB3_3:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	4608(%rsi,%rax,8), %xmm1
	movupd	9216(%rsi,%rax,8), %xmm2
	movapd	%xmm1, %xmm3
	addpd	%xmm2, %xmm3
	mulpd	%xmm0, %xmm3
	movupd	%xmm3, 4608(%rdi,%rax,8)
	subpd	%xmm2, %xmm1
	mulpd	%xmm0, %xmm1
	movupd	%xmm1, 9216(%rdi,%rax,8)
	movupd	4624(%rsi,%rax,8), %xmm1
	movupd	9232(%rsi,%rax,8), %xmm2
	movapd	%xmm1, %xmm3
	addpd	%xmm2, %xmm3
	mulpd	%xmm0, %xmm3
	movupd	%xmm3, 4624(%rdi,%rax,8)
	subpd	%xmm2, %xmm1
	mulpd	%xmm0, %xmm1
	movupd	%xmm1, 9232(%rdi,%rax,8)
	addq	$4, %rax
	jne	.LBB3_3
.LBB3_6:                                # %middle.block
	retq
.Lfunc_end3:
	.size	ms_convert, .Lfunc_end3-ms_convert
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	-4573563751269138432    # double -750
.LCPI4_1:
	.quad	4609659398595071181     # double 1.55
	.text
	.globl	on_pe
	.p2align	4, 0x90
	.type	on_pe,@function
on_pe:                                  # @on_pe
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 64
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movq	%rcx, %r13
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %r15
	leaq	4(%rsp), %rsi
	movq	%rsp, %rdx
	movl	%r8d, %edi
	movl	%ebp, %ecx
	callq	ResvMaxBits
	movl	204(%r15), %ecx
	testl	%ecx, %ecx
	jle	.LBB4_4
# BB#1:                                 # %.lr.ph
	movslq	%ebp, %rax
	movl	4(%rsp), %r10d
	movl	(%rsp), %esi
	imulq	$240, %rax, %rdx
	shlq	$4, %rax
	addq	%rax, %r14
	leaq	72(%r12,%rdx), %rdi
	xorl	%r8d, %r8d
	movsd	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI4_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movl	$500, %r9d              # imm = 0x1F4
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movl	%r10d, %eax
	cltd
	idivl	%ecx
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movl	%eax, (%r13,%rbp,4)
	movsd	(%r14,%rbp,8), %xmm2    # xmm2 = mem[0],zero
	addsd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	cvttsd2si	%xmm2, %ecx
	cmpl	$500, %ecx              # imm = 0x1F4
	movl	%ecx, %edx
	cmovll	%r9d, %edx
	cmpl	$2, (%rdi)
	cmovnel	%ecx, %edx
	testl	%edx, %edx
	cmovsl	%r8d, %edx
	cmpl	%esi, %edx
	cmovgl	%esi, %edx
	leal	(%rdx,%rax), %ecx
	movl	$4095, %ebx             # imm = 0xFFF
	subl	%eax, %ebx
	cmpl	$4095, %ecx             # imm = 0xFFF
	cmovlel	%edx, %ebx
	addl	%ebx, %eax
	movl	%eax, (%r13,%rbp,4)
	subl	%ebx, %esi
	incq	%rbp
	movslq	204(%r15), %rcx
	addq	$120, %rdi
	cmpq	%rcx, %rbp
	jl	.LBB4_2
# BB#3:                                 # %._crit_edge
	movl	%esi, (%rsp)
.LBB4_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	on_pe, .Lfunc_end4-on_pe
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4602678819172646912     # double 0.5
.LCPI5_1:
	.quad	4599616371426034975     # double 0.33000000000000002
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_2:
	.long	1123680256              # float 125
	.text
	.globl	reduce_side
	.p2align	4, 0x90
	.type	reduce_side,@function
reduce_side:                            # @reduce_side
	.cfi_startproc
# BB#0:
	movl	4(%rdi), %eax
	cmpl	$124, %eax
	jle	.LBB5_1
# BB#2:
	movsd	.LCPI5_0(%rip), %xmm1   # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	mulsd	.LCPI5_1(%rip), %xmm1
	addsd	%xmm1, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	xorpd	%xmm0, %xmm0
	maxss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%eax, %xmm1
	mulss	%xmm1, %xmm0
	subss	%xmm0, %xmm1
	ucomiss	.LCPI5_2(%rip), %xmm1
	jbe	.LBB5_4
# BB#3:
	cvtsi2ssl	(%rdi), %xmm2
	addss	%xmm2, %xmm0
	cvttss2si	%xmm0, %r8d
	movl	%r8d, (%rdi)
	cvttss2si	%xmm1, %eax
	jmp	.LBB5_5
.LBB5_1:                                # %..preheader_crit_edge
	movl	(%rdi), %r8d
	jmp	.LBB5_6
.LBB5_4:
	movl	(%rdi), %ecx
	leal	-125(%rax,%rcx), %r8d
	movl	%r8d, (%rdi)
	movl	$125, %eax
.LBB5_5:                                # %.sink.split
	movl	%eax, 4(%rdi)
.LBB5_6:                                # %.preheader
	movl	%esi, %ecx
	shrl	$31, %ecx
	addl	%esi, %ecx
	sarl	%ecx
	addl	$1200, %ecx             # imm = 0x4B0
	cmpl	$4096, %ecx             # imm = 0x1000
	movl	$4095, %edx             # imm = 0xFFF
	cmovll	%ecx, %edx
	cmpl	%edx, %r8d
	jle	.LBB5_8
# BB#7:
	movl	%edx, (%rdi)
.LBB5_8:
	cmpl	%edx, %eax
	jle	.LBB5_10
# BB#9:
	movl	%edx, 4(%rdi)
.LBB5_10:
	retq
.Lfunc_end5:
	.size	reduce_side, .Lfunc_end5-reduce_side
	.cfi_endproc

	.globl	inner_loop
	.p2align	4, 0x90
	.type	inner_loop,@function
inner_loop:                             # @inner_loop
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 48
.Lcfi43:
	.cfi_offset %rbx, -48
.Lcfi44:
	.cfi_offset %r12, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movq	%r8, %rbx
	movl	%ecx, %r14d
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbp
	jmp	.LBB6_2
	.p2align	4, 0x90
.LBB6_1:                                # %._crit_edge
                                        #   in Loop: Header=BB6_2 Depth=1
	incl	12(%rbx)
.LBB6_2:                                # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%rbx, %rcx
	callq	count_bits
	cmpl	%r14d, %eax
	jg	.LBB6_1
# BB#3:                                 # %._crit_edge9
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	inner_loop, .Lfunc_end6-inner_loop
	.cfi_endproc

	.globl	scale_bitcount
	.p2align	4, 0x90
	.type	scale_bitcount,@function
scale_bitcount:                         # @scale_bitcount
	.cfi_startproc
# BB#0:
	cmpl	$2, 24(%rsi)
	jne	.LBB7_1
# BB#25:                                # %.preheader93.preheader
	movl	88(%rdi), %r10d
	movl	92(%rdi), %eax
	xorl	%r9d, %r9d
	testl	%r10d, %r10d
	cmovsl	%r9d, %r10d
	movl	100(%rdi), %ecx
	cmpl	%r10d, %ecx
	cmovgel	%ecx, %r10d
	movl	112(%rdi), %ecx
	cmpl	%r10d, %ecx
	cmovgel	%ecx, %r10d
	movl	124(%rdi), %ecx
	cmpl	%r10d, %ecx
	cmovgel	%ecx, %r10d
	movl	136(%rdi), %ecx
	cmpl	%r10d, %ecx
	cmovgel	%ecx, %r10d
	movl	148(%rdi), %ecx
	movl	160(%rdi), %edx
	testl	%edx, %edx
	cmovnsl	%edx, %r9d
	movl	172(%rdi), %edx
	cmpl	%r9d, %edx
	cmovgel	%edx, %r9d
	movl	184(%rdi), %edx
	cmpl	%r9d, %edx
	cmovgel	%edx, %r9d
	movl	196(%rdi), %edx
	cmpl	%r9d, %edx
	cmovgel	%edx, %r9d
	movl	208(%rdi), %edx
	cmpl	%r9d, %edx
	cmovgel	%edx, %r9d
	movl	220(%rdi), %edx
	cmpl	%r9d, %edx
	cmovgel	%edx, %r9d
	cmpl	%r10d, %ecx
	cmovgel	%ecx, %r10d
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	movl	104(%rdi), %eax
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	movl	116(%rdi), %eax
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	movl	128(%rdi), %eax
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	movl	140(%rdi), %eax
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	movl	152(%rdi), %eax
	movl	164(%rdi), %ecx
	cmpl	%r9d, %ecx
	cmovgel	%ecx, %r9d
	movl	176(%rdi), %ecx
	cmpl	%r9d, %ecx
	cmovgel	%ecx, %r9d
	movl	188(%rdi), %ecx
	cmpl	%r9d, %ecx
	cmovgel	%ecx, %r9d
	movl	200(%rdi), %ecx
	cmpl	%r9d, %ecx
	cmovgel	%ecx, %r9d
	movl	212(%rdi), %ecx
	cmpl	%r9d, %ecx
	cmovgel	%ecx, %r9d
	movl	224(%rdi), %ecx
	cmpl	%r9d, %ecx
	cmovgel	%ecx, %r9d
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	movl	96(%rdi), %eax
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	movl	108(%rdi), %eax
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	movl	120(%rdi), %eax
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	movl	132(%rdi), %eax
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	movl	144(%rdi), %eax
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	movl	156(%rdi), %eax
	movl	168(%rdi), %ecx
	cmpl	%r9d, %ecx
	cmovgel	%ecx, %r9d
	movl	180(%rdi), %ecx
	cmpl	%r9d, %ecx
	cmovgel	%ecx, %r9d
	movl	192(%rdi), %ecx
	cmpl	%r9d, %ecx
	cmovgel	%ecx, %r9d
	movl	204(%rdi), %ecx
	cmpl	%r9d, %ecx
	cmovgel	%ecx, %r9d
	movl	216(%rdi), %ecx
	cmpl	%r9d, %ecx
	cmovgel	%ecx, %r9d
	movl	228(%rdi), %ecx
	cmpl	%r9d, %ecx
	cmovgel	%ecx, %r9d
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	movl	$scale_bitcount.slen1_tab, %r8d
	jmp	.LBB7_14
.LBB7_1:                                # %.preheader98.preheader
	movl	(%rdi), %r10d
	movl	4(%rdi), %eax
	xorl	%r9d, %r9d
	testl	%r10d, %r10d
	cmovsl	%r9d, %r10d
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	movl	8(%rdi), %eax
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	movl	12(%rdi), %eax
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	movl	16(%rdi), %eax
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	movl	20(%rdi), %eax
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	movl	24(%rdi), %eax
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	movl	28(%rdi), %eax
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	movl	32(%rdi), %eax
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	movl	36(%rdi), %eax
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	movl	40(%rdi), %eax
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	cmpl	$0, 64(%rsi)
	movl	44(%rdi), %eax
	jne	.LBB7_13
# BB#2:                                 # %.preheader96.preheader
	movl	pretab+44(%rip), %edx
	cmpl	%edx, %eax
	jl	.LBB7_13
# BB#3:                                 # %.preheader96.1143
	movl	48(%rdi), %ecx
	cmpl	pretab+48(%rip), %ecx
	jl	.LBB7_13
# BB#4:                                 # %.preheader96.2144
	movl	52(%rdi), %ecx
	cmpl	pretab+52(%rip), %ecx
	jl	.LBB7_13
# BB#5:                                 # %.preheader96.3145
	movl	56(%rdi), %ecx
	cmpl	pretab+56(%rip), %ecx
	jl	.LBB7_13
# BB#6:                                 # %.preheader96.4146
	movl	60(%rdi), %ecx
	cmpl	pretab+60(%rip), %ecx
	jl	.LBB7_13
# BB#7:                                 # %.preheader96.5147
	movl	64(%rdi), %ecx
	cmpl	pretab+64(%rip), %ecx
	jl	.LBB7_13
# BB#8:                                 # %.preheader96.6148
	movl	68(%rdi), %ecx
	cmpl	pretab+68(%rip), %ecx
	jl	.LBB7_13
# BB#9:                                 # %.preheader96.7149
	movl	72(%rdi), %ecx
	cmpl	pretab+72(%rip), %ecx
	jl	.LBB7_13
# BB#10:                                # %.preheader96.8150
	movl	76(%rdi), %ecx
	cmpl	pretab+76(%rip), %ecx
	jl	.LBB7_13
# BB#11:                                # %.preheader96.9151
	movl	80(%rdi), %ecx
	cmpl	pretab+80(%rip), %ecx
	jl	.LBB7_13
# BB#12:                                # %.thread.preheader.loopexit140
	movl	$1, 64(%rsi)
	movl	44(%rdi), %eax
	subl	%edx, %eax
	movl	%eax, 44(%rdi)
	movl	pretab+48(%rip), %ecx
	subl	%ecx, 48(%rdi)
	movl	pretab+52(%rip), %ecx
	subl	%ecx, 52(%rdi)
	movl	pretab+56(%rip), %ecx
	subl	%ecx, 56(%rdi)
	movl	pretab+60(%rip), %ecx
	subl	%ecx, 60(%rdi)
	movl	pretab+64(%rip), %ecx
	subl	%ecx, 64(%rdi)
	movl	pretab+68(%rip), %ecx
	subl	%ecx, 68(%rdi)
	movl	pretab+72(%rip), %ecx
	subl	%ecx, 72(%rdi)
	movl	pretab+76(%rip), %ecx
	subl	%ecx, 76(%rdi)
	movl	pretab+80(%rip), %ecx
	subl	%ecx, 80(%rdi)
.LBB7_13:                               # %.thread.preheader
	testl	%eax, %eax
	cmovnsl	%eax, %r9d
	movl	48(%rdi), %eax
	movl	52(%rdi), %ecx
	cmpl	%r9d, %eax
	cmovgel	%eax, %r9d
	cmpl	%r9d, %ecx
	cmovgel	%ecx, %r9d
	movl	56(%rdi), %eax
	cmpl	%r9d, %eax
	cmovgel	%eax, %r9d
	movl	60(%rdi), %eax
	cmpl	%r9d, %eax
	cmovgel	%eax, %r9d
	movl	64(%rdi), %eax
	cmpl	%r9d, %eax
	cmovgel	%eax, %r9d
	movl	68(%rdi), %eax
	cmpl	%r9d, %eax
	cmovgel	%eax, %r9d
	movl	72(%rdi), %eax
	cmpl	%r9d, %eax
	cmovgel	%eax, %r9d
	movl	76(%rdi), %eax
	cmpl	%r9d, %eax
	cmovgel	%eax, %r9d
	movl	80(%rdi), %eax
	cmpl	%r9d, %eax
	cmovgel	%eax, %r9d
	movl	$scale_bitcount.slen2_tab, %r8d
.LBB7_14:                               # %.loopexit
	movl	$100000, 76(%rsi)       # imm = 0x186A0
	movl	$2, %eax
	movl	$100000, %ecx           # imm = 0x186A0
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB7_15:                               # =>This Inner Loop Header: Depth=1
	cmpl	scale_bitcount.slen1(,%rdi,4), %r10d
	jge	.LBB7_19
# BB#16:                                #   in Loop: Header=BB7_15 Depth=1
	cmpl	scale_bitcount.slen2(,%rdi,4), %r9d
	jge	.LBB7_19
# BB#17:                                #   in Loop: Header=BB7_15 Depth=1
	movl	(%r8,%rdi,4), %edx
	cmpl	%edx, %ecx
	jle	.LBB7_19
# BB#18:                                #   in Loop: Header=BB7_15 Depth=1
	movl	%edx, 76(%rsi)
	movl	%edi, 16(%rsi)
	xorl	%eax, %eax
	movl	%edx, %ecx
	.p2align	4, 0x90
.LBB7_19:                               #   in Loop: Header=BB7_15 Depth=1
	cmpl	scale_bitcount.slen1+4(,%rdi,4), %r10d
	jge	.LBB7_23
# BB#20:                                #   in Loop: Header=BB7_15 Depth=1
	cmpl	scale_bitcount.slen2+4(,%rdi,4), %r9d
	jge	.LBB7_23
# BB#21:                                #   in Loop: Header=BB7_15 Depth=1
	movl	4(%r8,%rdi,4), %edx
	cmpl	%edx, %ecx
	jle	.LBB7_23
# BB#22:                                #   in Loop: Header=BB7_15 Depth=1
	movl	%edx, 76(%rsi)
	leal	1(%rdi), %eax
	movl	%eax, 16(%rsi)
	xorl	%eax, %eax
	movl	%edx, %ecx
	.p2align	4, 0x90
.LBB7_23:                               #   in Loop: Header=BB7_15 Depth=1
	addq	$2, %rdi
	cmpq	$16, %rdi
	jne	.LBB7_15
# BB#24:
	retq
.Lfunc_end7:
	.size	scale_bitcount, .Lfunc_end7-scale_bitcount
	.cfi_endproc

	.globl	scale_bitcount_lsf
	.p2align	4, 0x90
	.type	scale_bitcount_lsf,@function
scale_bitcount_lsf:                     # @scale_bitcount_lsf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 56
.Lcfi54:
	.cfi_offset %rbx, -56
.Lcfi55:
	.cfi_offset %r12, -48
.Lcfi56:
	.cfi_offset %r13, -40
.Lcfi57:
	.cfi_offset %r14, -32
.Lcfi58:
	.cfi_offset %r15, -24
.Lcfi59:
	.cfi_offset %rbp, -16
	movl	64(%rsi), %r13d
	xorl	%r9d, %r9d
	testl	%r13d, %r13d
	setne	%r9b
	addq	%r9, %r9
	cmpl	$2, 24(%rsi)
	jne	.LBB8_1
# BB#18:                                # %.preheader123.preheader
	leaq	(%r9,%r9,2), %r8
	shlq	$4, %r8
	movl	nr_of_sfb_block+16(%r8), %ecx
	xorl	%r11d, %r11d
	cmpq	$3, %rcx
	movl	$0, %r15d
	movl	$0, %r14d
	jb	.LBB8_21
# BB#19:                                # %.preheader.preheader
	movl	$2863311531, %eax       # imm = 0xAAAAAAAB
	imulq	%rax, %rcx
	shrq	$33, %rcx
	leaq	96(%rdi), %rax
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB8_20:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	-8(%rax), %edx
	movl	-4(%rax), %ebx
	cmpl	%r15d, %edx
	cmovgel	%edx, %r15d
	cmpl	%r15d, %ebx
	cmovgel	%ebx, %r15d
	movl	(%rax), %edx
	cmpl	%r15d, %edx
	cmovgel	%edx, %r15d
	incl	%r14d
	addq	$12, %rax
	cmpl	%ecx, %r14d
	jl	.LBB8_20
.LBB8_21:                               # %.preheader123.1155
	movl	nr_of_sfb_block+20(%r8), %r10d
	cmpq	$2, %r10
	jbe	.LBB8_25
# BB#22:                                # %.preheader.lr.ph.1
	movl	$2863311531, %ecx       # imm = 0xAAAAAAAB
	imulq	%rcx, %r10
	shrq	$33, %r10
	movslq	%r14d, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	96(%rdi,%rcx,4), %rcx
	xorl	%r11d, %r11d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_23:                               # %.preheader.1
                                        # =>This Inner Loop Header: Depth=1
	movl	-8(%rcx), %edx
	movl	-4(%rcx), %eax
	cmpl	%r11d, %edx
	cmovgel	%edx, %r11d
	cmpl	%r11d, %eax
	cmovgel	%eax, %r11d
	movl	(%rcx), %eax
	cmpl	%r11d, %eax
	cmovgel	%eax, %r11d
	incl	%ebx
	addq	$12, %rcx
	cmpl	%r10d, %ebx
	jl	.LBB8_23
# BB#24:                                # %._crit_edge.1
	addl	%ebx, %r14d
.LBB8_25:                               # %.preheader123.2156
	movl	nr_of_sfb_block+24(%r8), %r10d
	xorl	%r12d, %r12d
	cmpq	$3, %r10
	jb	.LBB8_26
# BB#34:                                # %.preheader.lr.ph.2
	movl	$2863311531, %eax       # imm = 0xAAAAAAAB
	imulq	%rax, %r10
	shrq	$33, %r10
	movslq	%r14d, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	96(%rdi,%rax,4), %rax
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB8_35:                               # %.preheader.2
                                        # =>This Inner Loop Header: Depth=1
	movl	-8(%rax), %ebp
	movl	-4(%rax), %ecx
	cmpl	%ebx, %ebp
	cmovgel	%ebp, %ebx
	cmpl	%ebx, %ecx
	cmovgel	%ecx, %ebx
	movl	(%rax), %ecx
	cmpl	%ebx, %ecx
	cmovgel	%ecx, %ebx
	incl	%edx
	addq	$12, %rax
	cmpl	%r10d, %edx
	jl	.LBB8_35
# BB#36:                                # %._crit_edge.2
	addl	%edx, %r14d
	jmp	.LBB8_37
.LBB8_1:                                # %.preheader124.preheader
	movabsq	$8589934584, %r10       # imm = 0x1FFFFFFF8
	leaq	(%r9,%r9,2), %r14
	shlq	$4, %r14
	movl	nr_of_sfb_block(%r14), %eax
	xorl	%r11d, %r11d
	testl	%eax, %eax
	movl	$0, %r15d
	movl	$0, %r8d
	jle	.LBB8_7
# BB#2:                                 # %.lr.ph.preheader
	leal	-1(%rax), %ebx
	incq	%rbx
	cmpq	$7, %rbx
	jbe	.LBB8_3
# BB#10:                                # %min.iters.checked
	movq	%rbx, %rcx
	andq	%r10, %rcx
	je	.LBB8_3
# BB#11:                                # %vector.body.preheader
	leaq	-8(%rcx), %rdx
	movq	%rdx, %rbp
	shrq	$3, %rbp
	btl	$3, %edx
	jb	.LBB8_12
# BB#13:                                # %vector.body.prol
	movdqu	(%rdi), %xmm2
	movdqu	16(%rdi), %xmm3
	pxor	%xmm4, %xmm4
	movdqa	%xmm2, %xmm0
	pcmpgtd	%xmm4, %xmm0
	movdqa	%xmm3, %xmm1
	pcmpgtd	%xmm4, %xmm1
	pand	%xmm2, %xmm0
	pand	%xmm3, %xmm1
	movl	$8, %r8d
	testq	%rbp, %rbp
	jne	.LBB8_15
	jmp	.LBB8_17
.LBB8_26:
	xorl	%ebx, %ebx
.LBB8_37:                               # %.preheader123.3157
	movl	nr_of_sfb_block+28(%r8), %eax
	cmpq	$3, %rax
	jb	.LBB8_38
# BB#39:                                # %.preheader.lr.ph.3
	movl	$2863311531, %ecx       # imm = 0xAAAAAAAB
	imulq	%rcx, %rax
	shrq	$33, %rax
	movslq	%r14d, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	96(%rdi,%rcx,4), %rdi
	xorl	%r12d, %r12d
	movl	$1, %r8d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB8_40:                               # %.preheader.3
                                        # =>This Inner Loop Header: Depth=1
	movl	-8(%rdi), %ecx
	movl	-4(%rdi), %ebp
	cmpl	%r12d, %ecx
	cmovgel	%ecx, %r12d
	cmpl	%r12d, %ebp
	cmovgel	%ebp, %r12d
	movl	(%rdi), %ecx
	cmpl	%r12d, %ecx
	cmovgel	%ecx, %r12d
	incl	%edx
	addq	$12, %rdi
	cmpl	%eax, %edx
	jl	.LBB8_40
	jmp	.LBB8_28
.LBB8_38:
	movl	$1, %r8d
	jmp	.LBB8_28
.LBB8_3:
	xorl	%ecx, %ecx
	xorl	%r15d, %r15d
	xorl	%ebp, %ebp
.LBB8_4:                                # %.lr.ph.preheader309
	leaq	(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB8_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	cmpl	%r15d, %edx
	cmovgel	%edx, %r15d
	incl	%ebp
	addq	$4, %rcx
	cmpl	%ebp, %eax
	jne	.LBB8_5
.LBB8_6:
	movl	%eax, %r8d
.LBB8_7:                                # %.preheader124.1164
	movl	nr_of_sfb_block+4(%r14), %eax
	testl	%eax, %eax
	jle	.LBB8_56
# BB#8:                                 # %.lr.ph.1
	movslq	%r8d, %rdx
	leal	-1(%rax), %ebp
	incq	%rbp
	xorl	%r11d, %r11d
	movl	%r13d, %r12d
	cmpq	$8, %rbp
	jae	.LBB8_41
# BB#9:
	xorl	%r13d, %r13d
	jmp	.LBB8_53
.LBB8_41:                               # %min.iters.checked190
	movq	%r10, %rcx
	movq	%rbp, %r10
	andq	%rcx, %r10
	movq	%rbp, %r13
	andq	%rcx, %r13
	je	.LBB8_42
# BB#43:                                # %vector.body186.preheader
	leaq	-8(%r13), %rcx
	movq	%rcx, %rbx
	shrq	$3, %rbx
	btl	$3, %ecx
	jb	.LBB8_44
# BB#45:                                # %vector.body186.prol
	movdqu	(%rdi,%rdx,4), %xmm2
	movdqu	16(%rdi,%rdx,4), %xmm3
	pxor	%xmm4, %xmm4
	movdqa	%xmm2, %xmm0
	pcmpgtd	%xmm4, %xmm0
	movdqa	%xmm3, %xmm1
	pcmpgtd	%xmm4, %xmm1
	pand	%xmm2, %xmm0
	pand	%xmm3, %xmm1
	movl	$8, %ecx
	testq	%rbx, %rbx
	jne	.LBB8_47
	jmp	.LBB8_49
.LBB8_42:
	xorl	%r13d, %r13d
	jmp	.LBB8_52
.LBB8_12:
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	pxor	%xmm1, %xmm1
	testq	%rbp, %rbp
	je	.LBB8_17
.LBB8_15:                               # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%r8, %rdx
	leaq	48(%rdi,%r8,4), %rbp
	.p2align	4, 0x90
.LBB8_16:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-48(%rbp), %xmm2
	movdqu	-32(%rbp), %xmm3
	movdqu	-16(%rbp), %xmm4
	movdqu	(%rbp), %xmm5
	movdqa	%xmm2, %xmm6
	pcmpgtd	%xmm0, %xmm6
	movdqa	%xmm3, %xmm7
	pcmpgtd	%xmm1, %xmm7
	pand	%xmm6, %xmm2
	pandn	%xmm0, %xmm6
	por	%xmm2, %xmm6
	pand	%xmm7, %xmm3
	pandn	%xmm1, %xmm7
	por	%xmm3, %xmm7
	movdqa	%xmm4, %xmm0
	pcmpgtd	%xmm6, %xmm0
	movdqa	%xmm5, %xmm1
	pcmpgtd	%xmm7, %xmm1
	pand	%xmm0, %xmm4
	pandn	%xmm6, %xmm0
	por	%xmm4, %xmm0
	pand	%xmm1, %xmm5
	pandn	%xmm7, %xmm1
	por	%xmm5, %xmm1
	addq	$64, %rbp
	addq	$-16, %rdx
	jne	.LBB8_16
.LBB8_17:                               # %middle.block
	movdqa	%xmm0, %xmm2
	pcmpgtd	%xmm1, %xmm2
	pand	%xmm2, %xmm0
	pandn	%xmm1, %xmm2
	por	%xmm0, %xmm2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	movdqa	%xmm2, %xmm1
	pcmpgtd	%xmm0, %xmm1
	pand	%xmm1, %xmm2
	pandn	%xmm0, %xmm1
	por	%xmm2, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	movd	%xmm1, %edx
	pcmpgtd	%xmm0, %xmm1
	movdqa	%xmm1, -24(%rsp)
	movd	%xmm0, %r15d
	testb	$1, -24(%rsp)
	cmovnel	%edx, %r15d
	cmpq	%rcx, %rbx
	movl	%ecx, %ebp
	jne	.LBB8_4
	jmp	.LBB8_6
.LBB8_44:
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	pxor	%xmm1, %xmm1
	testq	%rbx, %rbx
	je	.LBB8_49
.LBB8_47:                               # %vector.body186.preheader.new
	movq	%r13, %rbx
	subq	%rcx, %rbx
	addq	%rdx, %rcx
	leaq	48(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB8_48:                               # %vector.body186
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-48(%rcx), %xmm2
	movdqu	-32(%rcx), %xmm3
	movdqu	-16(%rcx), %xmm4
	movdqu	(%rcx), %xmm5
	movdqa	%xmm2, %xmm6
	pcmpgtd	%xmm0, %xmm6
	movdqa	%xmm3, %xmm7
	pcmpgtd	%xmm1, %xmm7
	pand	%xmm6, %xmm2
	pandn	%xmm0, %xmm6
	por	%xmm2, %xmm6
	pand	%xmm7, %xmm3
	pandn	%xmm1, %xmm7
	por	%xmm3, %xmm7
	movdqa	%xmm4, %xmm0
	pcmpgtd	%xmm6, %xmm0
	movdqa	%xmm5, %xmm1
	pcmpgtd	%xmm7, %xmm1
	pand	%xmm0, %xmm4
	pandn	%xmm6, %xmm0
	por	%xmm4, %xmm0
	pand	%xmm1, %xmm5
	pandn	%xmm7, %xmm1
	por	%xmm5, %xmm1
	addq	$64, %rcx
	addq	$-16, %rbx
	jne	.LBB8_48
.LBB8_49:                               # %middle.block187
	movdqa	%xmm0, %xmm2
	pcmpgtd	%xmm1, %xmm2
	pand	%xmm2, %xmm0
	pandn	%xmm1, %xmm2
	por	%xmm0, %xmm2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	movdqa	%xmm2, %xmm1
	pcmpgtd	%xmm0, %xmm1
	pand	%xmm1, %xmm2
	pandn	%xmm0, %xmm1
	por	%xmm2, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	movd	%xmm1, %ecx
	pcmpgtd	%xmm0, %xmm1
	movdqa	%xmm1, -40(%rsp)
	movd	%xmm0, %r11d
	testb	$1, -40(%rsp)
	cmovnel	%ecx, %r11d
	cmpq	%r13, %rbp
	jne	.LBB8_51
# BB#50:
	movabsq	$8589934584, %r10       # imm = 0x1FFFFFFF8
	jmp	.LBB8_55
.LBB8_51:
	addq	%r10, %rdx
.LBB8_52:                               # %scalar.ph188.preheader
	movabsq	$8589934584, %r10       # imm = 0x1FFFFFFF8
.LBB8_53:                               # %scalar.ph188.preheader
	leaq	(%rdi,%rdx,4), %rcx
	movl	%eax, %edx
	subl	%r13d, %edx
	.p2align	4, 0x90
.LBB8_54:                               # %scalar.ph188
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %ebp
	cmpl	%r11d, %ebp
	cmovgel	%ebp, %r11d
	addq	$4, %rcx
	decl	%edx
	jne	.LBB8_54
.LBB8_55:                               # %._crit_edge140.1
	addl	%eax, %r8d
	movl	%r12d, %r13d
.LBB8_56:                               # %.preheader124.2165
	movl	nr_of_sfb_block+8(%r14), %eax
	xorl	%r12d, %r12d
	testl	%eax, %eax
	jle	.LBB8_57
# BB#58:                                # %.lr.ph.2
	movslq	%r8d, %rdx
	leal	-1(%rax), %ebp
	incq	%rbp
	xorl	%ebx, %ebx
	cmpq	$8, %rbp
	movl	%r13d, -76(%rsp)        # 4-byte Spill
	jae	.LBB8_60
# BB#59:
	xorl	%r13d, %r13d
	jmp	.LBB8_71
.LBB8_57:
	xorl	%ebx, %ebx
	jmp	.LBB8_74
.LBB8_60:                               # %min.iters.checked230
	movq	%rbp, %rcx
	andq	%r10, %rcx
	movq	%rbp, %r13
	andq	%r10, %r13
	je	.LBB8_61
# BB#62:                                # %vector.body226.preheader
	movq	%rcx, -88(%rsp)         # 8-byte Spill
	leaq	-8(%r13), %rbx
	movq	%rbx, %rcx
	shrq	$3, %rcx
	btl	$3, %ebx
	jb	.LBB8_63
# BB#64:                                # %vector.body226.prol
	movdqu	(%rdi,%rdx,4), %xmm2
	movdqu	16(%rdi,%rdx,4), %xmm3
	pxor	%xmm4, %xmm4
	movdqa	%xmm2, %xmm0
	pcmpgtd	%xmm4, %xmm0
	movdqa	%xmm3, %xmm1
	pcmpgtd	%xmm4, %xmm1
	pand	%xmm2, %xmm0
	pand	%xmm3, %xmm1
	movl	$8, %r10d
	testq	%rcx, %rcx
	jne	.LBB8_66
	jmp	.LBB8_68
.LBB8_61:
	xorl	%r13d, %r13d
	jmp	.LBB8_71
.LBB8_63:
	pxor	%xmm0, %xmm0
	xorl	%r10d, %r10d
	pxor	%xmm1, %xmm1
	testq	%rcx, %rcx
	je	.LBB8_68
.LBB8_66:                               # %vector.body226.preheader.new
	movq	%r13, %rbx
	subq	%r10, %rbx
	addq	%rdx, %r10
	leaq	48(%rdi,%r10,4), %r10
	.p2align	4, 0x90
.LBB8_67:                               # %vector.body226
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-48(%r10), %xmm2
	movdqu	-32(%r10), %xmm3
	movdqu	-16(%r10), %xmm4
	movdqu	(%r10), %xmm5
	movdqa	%xmm2, %xmm6
	pcmpgtd	%xmm0, %xmm6
	movdqa	%xmm3, %xmm7
	pcmpgtd	%xmm1, %xmm7
	pand	%xmm6, %xmm2
	pandn	%xmm0, %xmm6
	por	%xmm2, %xmm6
	pand	%xmm7, %xmm3
	pandn	%xmm1, %xmm7
	por	%xmm3, %xmm7
	movdqa	%xmm4, %xmm0
	pcmpgtd	%xmm6, %xmm0
	movdqa	%xmm5, %xmm1
	pcmpgtd	%xmm7, %xmm1
	pand	%xmm0, %xmm4
	pandn	%xmm6, %xmm0
	por	%xmm4, %xmm0
	pand	%xmm1, %xmm5
	pandn	%xmm7, %xmm1
	por	%xmm5, %xmm1
	addq	$64, %r10
	addq	$-16, %rbx
	jne	.LBB8_67
.LBB8_68:                               # %middle.block227
	movdqa	%xmm0, %xmm2
	pcmpgtd	%xmm1, %xmm2
	pand	%xmm2, %xmm0
	pandn	%xmm1, %xmm2
	por	%xmm0, %xmm2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	movdqa	%xmm2, %xmm1
	pcmpgtd	%xmm0, %xmm1
	pand	%xmm1, %xmm2
	pandn	%xmm0, %xmm1
	por	%xmm2, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	movd	%xmm1, %ecx
	pcmpgtd	%xmm0, %xmm1
	movdqa	%xmm1, -56(%rsp)
	movd	%xmm0, %ebx
	testb	$1, -56(%rsp)
	cmovnel	%ecx, %ebx
	cmpq	%r13, %rbp
	jne	.LBB8_70
# BB#69:
	movabsq	$8589934584, %r10       # imm = 0x1FFFFFFF8
	jmp	.LBB8_73
.LBB8_70:
	addq	-88(%rsp), %rdx         # 8-byte Folded Reload
	movabsq	$8589934584, %r10       # imm = 0x1FFFFFFF8
.LBB8_71:                               # %scalar.ph228.preheader
	leaq	(%rdi,%rdx,4), %rdx
	movl	%eax, %ebp
	subl	%r13d, %ebp
	.p2align	4, 0x90
.LBB8_72:                               # %scalar.ph228
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdx), %ecx
	cmpl	%ebx, %ecx
	cmovgel	%ecx, %ebx
	addq	$4, %rdx
	decl	%ebp
	jne	.LBB8_72
.LBB8_73:                               # %._crit_edge140.2
	addl	%eax, %r8d
	movl	-76(%rsp), %r13d        # 4-byte Reload
.LBB8_74:                               # %.preheader124.3166
	movl	nr_of_sfb_block+12(%r14), %eax
	testl	%eax, %eax
	jle	.LBB8_27
# BB#75:                                # %.lr.ph.3
	movq	%r10, %rcx
	movslq	%r8d, %r14
	leal	-1(%rax), %r10d
	incq	%r10
	xorl	%r12d, %r12d
	cmpq	$8, %r10
	jae	.LBB8_77
# BB#76:
	xorl	%ecx, %ecx
	jmp	.LBB8_87
.LBB8_77:                               # %min.iters.checked270
	movq	%r10, %r8
	andq	%rcx, %r8
	andq	%r10, %rcx
	je	.LBB8_78
# BB#79:                                # %vector.body266.preheader
	movq	%rcx, -88(%rsp)         # 8-byte Spill
	leaq	-8(%rcx), %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB8_80
# BB#81:                                # %vector.body266.prol
	movdqu	(%rdi,%r14,4), %xmm2
	movdqu	16(%rdi,%r14,4), %xmm3
	pxor	%xmm4, %xmm4
	movdqa	%xmm2, %xmm0
	pcmpgtd	%xmm4, %xmm0
	movdqa	%xmm3, %xmm1
	pcmpgtd	%xmm4, %xmm1
	pand	%xmm2, %xmm0
	pand	%xmm3, %xmm1
	movl	$8, %ebp
	testq	%rcx, %rcx
	jne	.LBB8_83
	jmp	.LBB8_85
.LBB8_78:
	xorl	%ecx, %ecx
	jmp	.LBB8_87
.LBB8_80:
	pxor	%xmm0, %xmm0
	xorl	%ebp, %ebp
	pxor	%xmm1, %xmm1
	testq	%rcx, %rcx
	je	.LBB8_85
.LBB8_83:                               # %vector.body266.preheader.new
	movq	-88(%rsp), %rcx         # 8-byte Reload
	subq	%rbp, %rcx
	addq	%r14, %rbp
	leaq	48(%rdi,%rbp,4), %rbp
	.p2align	4, 0x90
.LBB8_84:                               # %vector.body266
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-48(%rbp), %xmm2
	movdqu	-32(%rbp), %xmm3
	movdqu	-16(%rbp), %xmm4
	movdqu	(%rbp), %xmm5
	movdqa	%xmm2, %xmm6
	pcmpgtd	%xmm0, %xmm6
	movdqa	%xmm3, %xmm7
	pcmpgtd	%xmm1, %xmm7
	pand	%xmm6, %xmm2
	pandn	%xmm0, %xmm6
	por	%xmm2, %xmm6
	pand	%xmm7, %xmm3
	pandn	%xmm1, %xmm7
	por	%xmm3, %xmm7
	movdqa	%xmm4, %xmm0
	pcmpgtd	%xmm6, %xmm0
	movdqa	%xmm5, %xmm1
	pcmpgtd	%xmm7, %xmm1
	pand	%xmm0, %xmm4
	pandn	%xmm6, %xmm0
	por	%xmm4, %xmm0
	pand	%xmm1, %xmm5
	pandn	%xmm7, %xmm1
	por	%xmm5, %xmm1
	addq	$64, %rbp
	addq	$-16, %rcx
	jne	.LBB8_84
.LBB8_85:                               # %middle.block267
	movdqa	%xmm0, %xmm2
	pcmpgtd	%xmm1, %xmm2
	pand	%xmm2, %xmm0
	pandn	%xmm1, %xmm2
	por	%xmm0, %xmm2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	movdqa	%xmm2, %xmm1
	pcmpgtd	%xmm0, %xmm1
	pand	%xmm1, %xmm2
	pandn	%xmm0, %xmm1
	por	%xmm2, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	movd	%xmm1, %ecx
	pcmpgtd	%xmm0, %xmm1
	movdqa	%xmm1, -72(%rsp)
	movd	%xmm0, %r12d
	testb	$1, -72(%rsp)
	cmovnel	%ecx, %r12d
	movq	-88(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, %r10
	jne	.LBB8_86
.LBB8_27:
	xorl	%r8d, %r8d
	jmp	.LBB8_28
.LBB8_86:
	addq	%r8, %r14
.LBB8_87:                               # %scalar.ph268.preheader
	leaq	(%rdi,%r14,4), %rdx
	subl	%ecx, %eax
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB8_88:                               # %scalar.ph268
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdx), %ecx
	cmpl	%r12d, %ecx
	cmovgel	%ecx, %r12d
	addq	$4, %rdx
	decl	%eax
	jne	.LBB8_88
.LBB8_28:                               # %.loopexit
	movq	%r9, %rdx
	shlq	$4, %rdx
	xorl	%eax, %eax
	cmpl	max_range_sfac_tab(%rdx), %r15d
	setg	%al
	xorl	%edi, %edi
	cmpl	max_range_sfac_tab+4(%rdx), %r11d
	setg	%dil
	addl	%eax, %edi
	xorl	%ebp, %ebp
	cmpl	max_range_sfac_tab+8(%rdx), %ebx
	setg	%bpl
	addl	%edi, %ebp
	xorl	%eax, %eax
	cmpl	max_range_sfac_tab+12(%rdx), %r12d
	setg	%al
	addl	%ebp, %eax
	jne	.LBB8_33
# BB#29:
	shlq	$4, %r8
	leaq	(%r9,%r9,2), %r9
	shlq	$4, %r9
	leaq	nr_of_sfb_block(%r9,%r8), %r10
	movq	%r10, 96(%rsi)
	movslq	%r15d, %rdx
	movl	scale_bitcount_lsf.log2tab(,%rdx,4), %ebp
	movl	%ebp, 104(%rsi)
	movslq	%r11d, %rdx
	movl	scale_bitcount_lsf.log2tab(,%rdx,4), %edi
	movl	%edi, 108(%rsi)
	movslq	%ebx, %rdx
	movl	scale_bitcount_lsf.log2tab(,%rdx,4), %edx
	movl	%edx, 112(%rsi)
	movslq	%r12d, %rcx
	movl	scale_bitcount_lsf.log2tab(,%rcx,4), %ecx
	movl	%ecx, 116(%rsi)
	testl	%r13d, %r13d
	je	.LBB8_30
# BB#31:
	leal	(%rbp,%rbp,2), %r11d
	movl	$500, %ebx              # imm = 0x1F4
	movl	%edi, %r14d
	jmp	.LBB8_32
.LBB8_30:
	leal	(%rbp,%rbp,4), %ebx
	addl	%edi, %ebx
	shll	$4, %ebx
	leal	(,%rdx,4), %r11d
	movl	%ecx, %r14d
.LBB8_32:                               # %.critedge.loopexit150
	addl	%r11d, %ebx
	addl	%r14d, %ebx
	movl	%ebx, 16(%rsi)
	movl	$0, 76(%rsi)
	imull	(%r10), %ebp
	movl	%ebp, 76(%rsi)
	imull	nr_of_sfb_block+4(%r9,%r8), %edi
	addl	%ebp, %edi
	movl	%edi, 76(%rsi)
	imull	nr_of_sfb_block+8(%r9,%r8), %edx
	addl	%edi, %edx
	movl	%edx, 76(%rsi)
	imull	nr_of_sfb_block+12(%r9,%r8), %ecx
	addl	%edx, %ecx
	movl	%ecx, 76(%rsi)
.LBB8_33:                               # %.critedge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	scale_bitcount_lsf, .Lfunc_end8-scale_bitcount_lsf
	.cfi_endproc

	.globl	calc_xmin
	.p2align	4, 0x90
	.type	calc_xmin,@function
calc_xmin:                              # @calc_xmin
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi64:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 56
.Lcfi66:
	.cfi_offset %rbx, -56
.Lcfi67:
	.cfi_offset %r12, -48
.Lcfi68:
	.cfi_offset %r13, -40
.Lcfi69:
	.cfi_offset %r14, -32
.Lcfi70:
	.cfi_offset %r15, -24
.Lcfi71:
	.cfi_offset %rbp, -16
	cmpl	$0, 144(%rdi)
	movl	84(%rcx), %r10d
	je	.LBB9_4
# BB#1:                                 # %.preheader127
	cmpl	$11, %r10d
	ja	.LBB9_59
# BB#2:                                 # %.preheader126.preheader
	testb	$1, %r10b
	jne	.LBB9_56
# BB#3:
	movq	%r10, %rax
	cmpl	$11, %r10d
	jne	.LBB9_57
	jmp	.LBB9_59
.LBB9_4:                                # %.preheader123
	movq	%rcx, -40(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	cmpl	$11, %r10d
	movq	%rdx, -64(%rsp)         # 8-byte Spill
	ja	.LBB9_43
# BB#5:                                 # %.lr.ph144
	movss	masking_lower(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	scalefac_band+92(,%r10,4), %ecx
	leaq	8(%rsi), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	leaq	80(%rsi), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	leaq	16(%rsi), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	leaq	88(%rsi), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	xorps	%xmm1, %xmm1
	movq	%r8, -48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB9_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_9 Depth 2
                                        #     Child Loop BB9_20 Depth 2
                                        #     Child Loop BB9_25 Depth 2
                                        #     Child Loop BB9_29 Depth 2
                                        #     Child Loop BB9_34 Depth 2
                                        #     Child Loop BB9_38 Depth 2
	movq	%r10, %r12
	movl	%ecx, %r13d
	leaq	1(%r12), %r10
	movl	scalefac_band+96(,%r12,4), %ecx
	movl	%ecx, %ebx
	subl	%r13d, %ebx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%ebx, %xmm2
	jle	.LBB9_10
# BB#7:                                 # %.preheader121.us.preheader
                                        #   in Loop: Header=BB9_6 Depth=1
	movl	%ecx, -68(%rsp)         # 4-byte Spill
	movslq	%ecx, %rdi
	movslq	%r13d, %r11
	leaq	-1(%rdi), %r9
	subq	%r11, %r9
	andq	$3, %rbx
	je	.LBB9_17
# BB#8:                                 # %.prol.preheader
                                        #   in Loop: Header=BB9_6 Depth=1
	leaq	(%r11,%r11,2), %rcx
	leaq	(%rsi,%rcx,8), %rbp
	negq	%rbx
	xorpd	%xmm4, %xmm4
	movq	%r11, %rcx
	.p2align	4, 0x90
.LBB9_9:                                #   Parent Loop BB9_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbp), %xmm3           # xmm3 = mem[0],zero
	mulsd	%xmm3, %xmm3
	addsd	%xmm3, %xmm4
	incq	%rcx
	addq	$24, %rbp
	incq	%rbx
	jne	.LBB9_9
	jmp	.LBB9_18
	.p2align	4, 0x90
.LBB9_10:                               # %.preheader121.preheader
                                        #   in Loop: Header=BB9_6 Depth=1
	xorpd	%xmm3, %xmm3
	divsd	%xmm2, %xmm3
	leaq	(%r12,%r12,2), %rbx
	movsd	664(%rdx,%rbx,8), %xmm2 # xmm2 = mem[0],zero
	ucomisd	%xmm1, %xmm2
	jbe	.LBB9_12
# BB#11:                                #   in Loop: Header=BB9_6 Depth=1
	movsd	176(%rdx,%rbx,8), %xmm4 # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	mulsd	%xmm0, %xmm4
	divsd	%xmm2, %xmm4
	movapd	%xmm4, %xmm2
.LBB9_12:                               # %.preheader121.1158
                                        #   in Loop: Header=BB9_6 Depth=1
	movsd	ATH_s(,%r12,8), %xmm4   # xmm4 = mem[0],zero
	maxsd	%xmm2, %xmm4
	leaq	(,%r12,8), %rdi
	leaq	(%rdi,%rdi,2), %rbp
	movsd	%xmm4, 176(%r8,%rbp)
	movsd	ATH_s(,%r12,8), %xmm2   # xmm2 = mem[0],zero
	xorl	%edi, %edi
	ucomisd	%xmm2, %xmm3
	seta	%dil
	movsd	672(%rdx,%rbp), %xmm4   # xmm4 = mem[0],zero
	ucomisd	%xmm1, %xmm4
	jbe	.LBB9_14
# BB#13:                                #   in Loop: Header=BB9_6 Depth=1
	movsd	184(%rdx,%rbx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm3, %xmm5
	mulsd	%xmm0, %xmm5
	divsd	%xmm4, %xmm5
	movapd	%xmm5, %xmm4
.LBB9_14:                               # %.preheader121.2159
                                        #   in Loop: Header=BB9_6 Depth=1
	addl	%eax, %edi
	maxsd	%xmm4, %xmm2
	movsd	%xmm2, 184(%r8,%rbp)
	movsd	ATH_s(,%r12,8), %xmm4   # xmm4 = mem[0],zero
	xorl	%r9d, %r9d
	ucomisd	%xmm4, %xmm3
	seta	%r9b
	movsd	680(%rdx,%rbp), %xmm2   # xmm2 = mem[0],zero
	ucomisd	%xmm1, %xmm2
	jbe	.LBB9_16
# BB#15:                                #   in Loop: Header=BB9_6 Depth=1
	movsd	192(%rdx,%rbx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm3, %xmm5
	mulsd	%xmm0, %xmm5
	divsd	%xmm2, %xmm5
	movapd	%xmm5, %xmm2
.LBB9_16:                               # %.loopexit122.loopexit150160
                                        #   in Loop: Header=BB9_6 Depth=1
	addl	%edi, %r9d
	jmp	.LBB9_42
.LBB9_17:                               #   in Loop: Header=BB9_6 Depth=1
	xorpd	%xmm4, %xmm4
	movq	%r11, %rcx
.LBB9_18:                               # %.prol.loopexit
                                        #   in Loop: Header=BB9_6 Depth=1
	movq	%r10, -56(%rsp)         # 8-byte Spill
	cmpq	$3, %r9
	jb	.LBB9_21
# BB#19:                                # %.preheader121.us.preheader.new
                                        #   in Loop: Header=BB9_6 Depth=1
	leaq	(%rcx,%rcx,2), %rbp
	leaq	(%rsi,%rbp,8), %rbp
	.p2align	4, 0x90
.LBB9_20:                               #   Parent Loop BB9_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbp), %xmm3           # xmm3 = mem[0],zero
	movsd	24(%rbp), %xmm5         # xmm5 = mem[0],zero
	mulsd	%xmm3, %xmm3
	addsd	%xmm4, %xmm3
	mulsd	%xmm5, %xmm5
	addsd	%xmm3, %xmm5
	movsd	48(%rbp), %xmm3         # xmm3 = mem[0],zero
	mulsd	%xmm3, %xmm3
	addsd	%xmm5, %xmm3
	movsd	72(%rbp), %xmm4         # xmm4 = mem[0],zero
	mulsd	%xmm4, %xmm4
	addsd	%xmm3, %xmm4
	addq	$4, %rcx
	addq	$96, %rbp
	cmpq	%rcx, %rdi
	jne	.LBB9_20
.LBB9_21:                               # %._crit_edge137.us
                                        #   in Loop: Header=BB9_6 Depth=1
	divsd	%xmm2, %xmm4
	leaq	(%r12,%r12,2), %r10
	movsd	664(%rdx,%r10,8), %xmm3 # xmm3 = mem[0],zero
	ucomisd	%xmm1, %xmm3
	jbe	.LBB9_23
# BB#22:                                #   in Loop: Header=BB9_6 Depth=1
	movsd	176(%rdx,%r10,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm4, %xmm5
	mulsd	%xmm0, %xmm5
	divsd	%xmm3, %xmm5
	movapd	%xmm5, %xmm3
.LBB9_23:                               # %.preheader121.us.1168
                                        #   in Loop: Header=BB9_6 Depth=1
	movsd	ATH_s(,%r12,8), %xmm5   # xmm5 = mem[0],zero
	maxsd	%xmm3, %xmm5
	movsd	%xmm5, 176(%r8,%r10,8)
	movsd	ATH_s(,%r12,8), %xmm3   # xmm3 = mem[0],zero
	xorl	%r8d, %r8d
	ucomisd	%xmm3, %xmm4
	seta	-69(%rsp)               # 1-byte Folded Spill
	movl	%ecx, %r14d
	subl	%r13d, %r14d
	leaq	-1(%rcx), %r15
	subq	%r11, %r15
	andq	$3, %r14
	je	.LBB9_26
# BB#24:                                # %.prol.preheader214
                                        #   in Loop: Header=BB9_6 Depth=1
	leaq	(%r11,%r11,2), %rdi
	movq	-8(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rdi
	movq	%r14, %rbx
	negq	%rbx
	xorpd	%xmm5, %xmm5
	movq	%r11, %rbp
	.p2align	4, 0x90
.LBB9_25:                               #   Parent Loop BB9_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi), %xmm4           # xmm4 = mem[0],zero
	mulsd	%xmm4, %xmm4
	addsd	%xmm4, %xmm5
	incq	%rbp
	addq	$24, %rdi
	incq	%rbx
	jne	.LBB9_25
	jmp	.LBB9_27
.LBB9_26:                               #   in Loop: Header=BB9_6 Depth=1
	xorpd	%xmm5, %xmm5
	movq	%r11, %rbp
.LBB9_27:                               # %.prol.loopexit215
                                        #   in Loop: Header=BB9_6 Depth=1
	cmpq	$3, %r15
	jb	.LBB9_30
# BB#28:                                # %.preheader121.us.1168.new
                                        #   in Loop: Header=BB9_6 Depth=1
	leaq	(%rbp,%rbp,2), %rdi
	movq	-24(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rbx
	movq	%rcx, %rdi
	subq	%rbp, %rdi
	.p2align	4, 0x90
.LBB9_29:                               #   Parent Loop BB9_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-72(%rbx), %xmm4        # xmm4 = mem[0],zero
	movsd	-48(%rbx), %xmm6        # xmm6 = mem[0],zero
	mulsd	%xmm4, %xmm4
	addsd	%xmm5, %xmm4
	mulsd	%xmm6, %xmm6
	addsd	%xmm4, %xmm6
	movsd	-24(%rbx), %xmm4        # xmm4 = mem[0],zero
	mulsd	%xmm4, %xmm4
	addsd	%xmm6, %xmm4
	movsd	(%rbx), %xmm5           # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm5
	addsd	%xmm4, %xmm5
	addq	$96, %rbx
	addq	$-4, %rdi
	jne	.LBB9_29
.LBB9_30:                               # %._crit_edge137.us.1
                                        #   in Loop: Header=BB9_6 Depth=1
	divsd	%xmm2, %xmm5
	movq	-64(%rsp), %rdx         # 8-byte Reload
	movsd	672(%rdx,%r10,8), %xmm4 # xmm4 = mem[0],zero
	ucomisd	%xmm1, %xmm4
	jbe	.LBB9_32
# BB#31:                                #   in Loop: Header=BB9_6 Depth=1
	movsd	184(%rdx,%r10,8), %xmm6 # xmm6 = mem[0],zero
	mulsd	%xmm5, %xmm6
	mulsd	%xmm0, %xmm6
	divsd	%xmm4, %xmm6
	movapd	%xmm6, %xmm4
.LBB9_32:                               # %.preheader121.us.2169
                                        #   in Loop: Header=BB9_6 Depth=1
	maxsd	%xmm4, %xmm3
	movq	-48(%rsp), %rdi         # 8-byte Reload
	movsd	%xmm3, 184(%rdi,%r10,8)
	movsd	ATH_s(,%r12,8), %xmm4   # xmm4 = mem[0],zero
	xorl	%r9d, %r9d
	ucomisd	%xmm4, %xmm5
	seta	%bl
	movl	%ecx, %edi
	subl	%r13d, %edi
	testb	$3, %dil
	je	.LBB9_35
# BB#33:                                # %.prol.preheader219
                                        #   in Loop: Header=BB9_6 Depth=1
	leaq	(%r11,%r11,2), %rdi
	movq	-16(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rdi
	negq	%r14
	xorpd	%xmm3, %xmm3
	.p2align	4, 0x90
.LBB9_34:                               #   Parent Loop BB9_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi), %xmm5           # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm5
	addsd	%xmm5, %xmm3
	incq	%r11
	addq	$24, %rdi
	incq	%r14
	jne	.LBB9_34
	jmp	.LBB9_36
.LBB9_35:                               #   in Loop: Header=BB9_6 Depth=1
	xorpd	%xmm3, %xmm3
.LBB9_36:                               # %.prol.loopexit220
                                        #   in Loop: Header=BB9_6 Depth=1
	movb	-69(%rsp), %dl          # 1-byte Reload
	movb	%dl, %r8b
	cmpq	$3, %r15
	jb	.LBB9_39
# BB#37:                                # %.preheader121.us.2169.new
                                        #   in Loop: Header=BB9_6 Depth=1
	leaq	(%r11,%r11,2), %rdi
	movq	-32(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rdi
	subq	%r11, %rcx
	.p2align	4, 0x90
.LBB9_38:                               #   Parent Loop BB9_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-72(%rdi), %xmm5        # xmm5 = mem[0],zero
	movsd	-48(%rdi), %xmm6        # xmm6 = mem[0],zero
	mulsd	%xmm5, %xmm5
	addsd	%xmm3, %xmm5
	mulsd	%xmm6, %xmm6
	addsd	%xmm5, %xmm6
	movsd	-24(%rdi), %xmm5        # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm5
	addsd	%xmm6, %xmm5
	movsd	(%rdi), %xmm3           # xmm3 = mem[0],zero
	mulsd	%xmm3, %xmm3
	addsd	%xmm5, %xmm3
	addq	$96, %rdi
	addq	$-4, %rcx
	jne	.LBB9_38
.LBB9_39:                               # %._crit_edge137.us.2
                                        #   in Loop: Header=BB9_6 Depth=1
	addl	%eax, %r8d
	movl	%r8d, %eax
	movb	%bl, %r9b
	divsd	%xmm2, %xmm3
	movq	-64(%rsp), %rdx         # 8-byte Reload
	movsd	680(%rdx,%r10,8), %xmm2 # xmm2 = mem[0],zero
	ucomisd	%xmm1, %xmm2
	movq	-48(%rsp), %r8          # 8-byte Reload
	jbe	.LBB9_41
# BB#40:                                #   in Loop: Header=BB9_6 Depth=1
	movsd	192(%rdx,%r10,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm3, %xmm5
	mulsd	%xmm0, %xmm5
	divsd	%xmm2, %xmm5
	movapd	%xmm5, %xmm2
.LBB9_41:                               # %.loopexit122.loopexit170
                                        #   in Loop: Header=BB9_6 Depth=1
	movq	-56(%rsp), %r10         # 8-byte Reload
	movl	-68(%rsp), %ecx         # 4-byte Reload
	addl	%eax, %r9d
.LBB9_42:                               # %.loopexit122
                                        #   in Loop: Header=BB9_6 Depth=1
	maxsd	%xmm2, %xmm4
	leaq	(%r12,%r12,2), %rax
	movsd	%xmm4, 192(%r8,%rax,8)
	xorl	%eax, %eax
	ucomisd	ATH_s(,%r12,8), %xmm3
	seta	%al
	addl	%r9d, %eax
	cmpq	$12, %r10
	jne	.LBB9_6
.LBB9_43:                               # %.preheader
	movq	-40(%rsp), %rcx         # 8-byte Reload
	movl	80(%rcx), %r10d
	testq	%r10, %r10
	je	.LBB9_68
# BB#44:                                # %.lr.ph132
	movss	masking_lower(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	scalefac_band(%rip), %edi
	leaq	24(%rsi), %r9
	xorl	%ebp, %ebp
	xorps	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB9_45:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_48 Depth 2
                                        #     Child Loop BB9_52 Depth 2
	movl	scalefac_band+4(,%rbp,4), %r11d
	movl	%r11d, %r15d
	subl	%edi, %r15d
	xorpd	%xmm2, %xmm2
	jle	.LBB9_53
# BB#46:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB9_45 Depth=1
	movslq	%r11d, %rcx
	movslq	%edi, %rbx
	leaq	-1(%rcx), %r14
	subq	%rbx, %r14
	movq	%r15, %rdi
	andq	$3, %rdi
	je	.LBB9_49
# BB#47:                                # %.lr.ph.prol.preheader
                                        #   in Loop: Header=BB9_45 Depth=1
	negq	%rdi
	xorpd	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB9_48:                               # %.lr.ph.prol
                                        #   Parent Loop BB9_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rsi,%rbx,8), %xmm3    # xmm3 = mem[0],zero
	mulsd	%xmm3, %xmm3
	addsd	%xmm3, %xmm2
	incq	%rbx
	incq	%rdi
	jne	.LBB9_48
	jmp	.LBB9_50
.LBB9_49:                               #   in Loop: Header=BB9_45 Depth=1
	xorpd	%xmm2, %xmm2
.LBB9_50:                               # %.lr.ph.prol.loopexit
                                        #   in Loop: Header=BB9_45 Depth=1
	cmpq	$3, %r14
	movq	-64(%rsp), %rdx         # 8-byte Reload
	jb	.LBB9_53
# BB#51:                                # %.lr.ph.preheader.new
                                        #   in Loop: Header=BB9_45 Depth=1
	subq	%rbx, %rcx
	leaq	(%r9,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB9_52:                               # %.lr.ph
                                        #   Parent Loop BB9_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-24(%rbx), %xmm3        # xmm3 = mem[0],zero
	movsd	-16(%rbx), %xmm4        # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm3
	addsd	%xmm2, %xmm3
	mulsd	%xmm4, %xmm4
	addsd	%xmm3, %xmm4
	movsd	-8(%rbx), %xmm3         # xmm3 = mem[0],zero
	mulsd	%xmm3, %xmm3
	addsd	%xmm4, %xmm3
	movsd	(%rbx), %xmm2           # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm2
	addsd	%xmm3, %xmm2
	addq	$32, %rbx
	addq	$-4, %rcx
	jne	.LBB9_52
.LBB9_53:                               # %._crit_edge
                                        #   in Loop: Header=BB9_45 Depth=1
	leaq	1(%rbp), %rcx
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%r15d, %xmm3
	divsd	%xmm3, %xmm2
	movsd	488(%rdx,%rbp,8), %xmm3 # xmm3 = mem[0],zero
	ucomisd	%xmm1, %xmm3
	jbe	.LBB9_55
# BB#54:                                #   in Loop: Header=BB9_45 Depth=1
	movsd	(%rdx,%rbp,8), %xmm4    # xmm4 = mem[0],zero
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	divsd	%xmm3, %xmm4
	movapd	%xmm4, %xmm3
.LBB9_55:                               #   in Loop: Header=BB9_45 Depth=1
	movsd	ATH_l(,%rbp,8), %xmm4   # xmm4 = mem[0],zero
	maxsd	%xmm3, %xmm4
	movsd	%xmm4, (%r8,%rbp,8)
	xorl	%edi, %edi
	ucomisd	ATH_l(,%rbp,8), %xmm2
	seta	%dil
	addl	%edi, %eax
	cmpq	%r10, %rcx
	movl	%r11d, %edi
	movq	%rcx, %rbp
	jb	.LBB9_45
	jmp	.LBB9_68
.LBB9_56:                               # %.preheader126.prol
	movq	ATH_s(,%r10,8), %rax
	leaq	(%r10,%r10,2), %rdx
	movq	%rax, 176(%r8,%rdx,8)
	movq	ATH_s(,%r10,8), %rax
	movq	%rax, 184(%r8,%rdx,8)
	movq	ATH_s(,%r10,8), %rax
	movq	%rax, 192(%r8,%rdx,8)
	leaq	1(%r10), %rax
	cmpl	$11, %r10d
	je	.LBB9_59
.LBB9_57:                               # %.preheader126.preheader.new
	leaq	(%rax,%rax,2), %rdx
	leaq	-12(%rax), %rax
	leaq	216(%r8,%rdx,8), %rsi
	.p2align	4, 0x90
.LBB9_58:                               # %.preheader126
                                        # =>This Inner Loop Header: Depth=1
	movq	ATH_s+96(,%rax,8), %rdx
	movq	%rdx, -40(%rsi)
	movq	ATH_s+96(,%rax,8), %rdx
	movq	%rdx, -32(%rsi)
	movq	ATH_s+96(,%rax,8), %rdx
	movq	%rdx, -24(%rsi)
	movq	ATH_s+104(,%rax,8), %rdx
	movq	%rdx, -16(%rsi)
	movq	ATH_s+104(,%rax,8), %rdx
	movq	%rdx, -8(%rsi)
	movq	ATH_s+104(,%rax,8), %rdx
	movq	%rdx, (%rsi)
	addq	$48, %rsi
	addq	$2, %rax
	jne	.LBB9_58
.LBB9_59:                               # %.preheader124
	movl	80(%rcx), %eax
	testq	%rax, %rax
	je	.LBB9_67
# BB#60:                                # %.lr.ph147
	cmpl	$4, %eax
	jb	.LBB9_64
# BB#61:                                # %min.iters.checked
	movl	%eax, %edx
	andl	$3, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB9_64
# BB#62:                                # %vector.memcheck
	leaq	ATH_l(,%rax,8), %rsi
	cmpq	%r8, %rsi
	jbe	.LBB9_69
# BB#63:                                # %vector.memcheck
	leaq	(%r8,%rax,8), %rsi
	movl	$ATH_l, %edi
	cmpq	%rdi, %rsi
	jbe	.LBB9_69
.LBB9_64:
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB9_65:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	ATH_l(,%rcx,8), %rdx
	movq	%rdx, (%r8,%rcx,8)
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB9_65
.LBB9_67:
	xorl	%eax, %eax
.LBB9_68:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_69:                               # %vector.body.preheader
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB9_70:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movapd	ATH_l(,%rsi,8), %xmm0
	movaps	ATH_l+16(,%rsi,8), %xmm1
	movupd	%xmm0, (%r8,%rsi,8)
	movups	%xmm1, 16(%r8,%rsi,8)
	addq	$4, %rsi
	cmpq	%rsi, %rcx
	jne	.LBB9_70
# BB#71:                                # %middle.block
	testl	%edx, %edx
	jne	.LBB9_65
	jmp	.LBB9_67
.Lfunc_end9:
	.size	calc_xmin, .Lfunc_end9-calc_xmin
	.cfi_endproc

	.globl	loop_break
	.p2align	4, 0x90
	.type	loop_break,@function
loop_break:                             # @loop_break
	.cfi_startproc
# BB#0:
	movl	80(%rsi), %ecx
	testl	%ecx, %ecx
	je	.LBB10_4
# BB#1:                                 # %.lr.ph.preheader
	xorl	%eax, %eax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB10_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%rdi,%rdx,4)
	je	.LBB10_11
# BB#2:                                 #   in Loop: Header=BB10_3 Depth=1
	incq	%rdx
	cmpl	%ecx, %edx
	jb	.LBB10_3
.LBB10_4:                               # %._crit_edge
	movl	84(%rsi), %ecx
	cmpq	$11, %rcx
	ja	.LBB10_10
# BB#5:                                 # %.preheader.preheader
	leaq	(%rcx,%rcx,2), %rax
	leaq	96(%rdi,%rax,4), %rdx
	.p2align	4, 0x90
.LBB10_6:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	cmpl	$0, -8(%rdx)
	je	.LBB10_11
# BB#7:                                 #   in Loop: Header=BB10_6 Depth=1
	cmpl	$0, -4(%rdx)
	je	.LBB10_11
# BB#8:                                 #   in Loop: Header=BB10_6 Depth=1
	cmpl	$0, (%rdx)
	je	.LBB10_11
# BB#9:                                 #   in Loop: Header=BB10_6 Depth=1
	incq	%rcx
	addq	$12, %rdx
	cmpq	$12, %rcx
	jb	.LBB10_6
.LBB10_10:
	movl	$1, %eax
.LBB10_11:                              # %.loopexit
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end10:
	.size	loop_break, .Lfunc_end10-loop_break
	.cfi_endproc

	.globl	bin_search_StepSize2
	.p2align	4, 0x90
	.type	bin_search_StepSize2,@function
bin_search_StepSize2:                   # @bin_search_StepSize2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi75:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi78:
	.cfi_def_cfa_offset 80
.Lcfi79:
	.cfi_offset %rbx, -56
.Lcfi80:
	.cfi_offset %r12, -48
.Lcfi81:
	.cfi_offset %r13, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
	movl	%edx, %r14d
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movq	%rdi, %r13
	movl	%r14d, 12(%rbx)
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rcx, %rsi
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%r8, %rdx
	movq	%rbx, %rcx
	callq	count_bits
	movl	bin_search_StepSize2.CurrentStep(%rip), %ecx
	cmpl	$1, %ecx
	movl	%r14d, (%rsp)           # 4-byte Spill
	je	.LBB11_15
# BB#1:                                 # %.lr.ph.preheader
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB11_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testl	%r12d, %r12d
	je	.LBB11_4
# BB#3:                                 #   in Loop: Header=BB11_2 Depth=1
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	movl	%edx, bin_search_StepSize2.CurrentStep(%rip)
	movl	%edx, %ecx
.LBB11_4:                               #   in Loop: Header=BB11_2 Depth=1
	cmpl	4(%rsp), %eax           # 4-byte Folded Reload
	jle	.LBB11_10
# BB#5:                                 #   in Loop: Header=BB11_2 Depth=1
	testl	%r12d, %r12d
	jne	.LBB11_8
# BB#6:                                 #   in Loop: Header=BB11_2 Depth=1
	cmpl	$2, %r15d
	jne	.LBB11_8
# BB#7:                                 #   in Loop: Header=BB11_2 Depth=1
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	movl	%edx, bin_search_StepSize2.CurrentStep(%rip)
	movl	$1, %r12d
	movl	%edx, %ecx
.LBB11_8:                               #   in Loop: Header=BB11_2 Depth=1
	addl	%r14d, %ecx
	movl	$1, %r15d
	cmpl	$255, %ecx
	movl	%ecx, %r14d
	jle	.LBB11_9
	jmp	.LBB11_15
	.p2align	4, 0x90
.LBB11_10:                              #   in Loop: Header=BB11_2 Depth=1
	jge	.LBB11_15
# BB#11:                                #   in Loop: Header=BB11_2 Depth=1
	testl	%r12d, %r12d
	jne	.LBB11_14
# BB#12:                                #   in Loop: Header=BB11_2 Depth=1
	cmpl	$1, %r15d
	jne	.LBB11_14
# BB#13:                                #   in Loop: Header=BB11_2 Depth=1
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	movl	%edx, bin_search_StepSize2.CurrentStep(%rip)
	movl	$1, %r12d
	movl	%edx, %ecx
.LBB11_14:                              #   in Loop: Header=BB11_2 Depth=1
	subl	%ecx, %r14d
	movl	$2, %r15d
	movl	%r14d, %ecx
	js	.LBB11_15
.LBB11_9:                               # %.backedge
                                        #   in Loop: Header=BB11_2 Depth=1
	movl	%ecx, %r14d
	movl	%r14d, 12(%rbx)
	movq	%r13, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	callq	count_bits
	movl	bin_search_StepSize2.CurrentStep(%rip), %ecx
	cmpl	$1, %ecx
	jne	.LBB11_2
.LBB11_15:                              # %._crit_edge
	movl	(%rsp), %edx            # 4-byte Reload
	subl	%r14d, %edx
	movl	%edx, %ecx
	negl	%ecx
	cmovll	%edx, %ecx
	xorl	%edx, %edx
	cmpl	$3, %ecx
	setg	%dl
	leal	2(%rdx,%rdx), %ecx
	movl	%ecx, bin_search_StepSize2.CurrentStep(%rip)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	bin_search_StepSize2, .Lfunc_end11-bin_search_StepSize2
	.cfi_endproc

	.globl	quantize_xrpow
	.p2align	4, 0x90
	.type	quantize_xrpow,@function
quantize_xrpow:                         # @quantize_xrpow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 40
.Lcfi89:
	.cfi_offset %rbx, -40
.Lcfi90:
	.cfi_offset %r14, -32
.Lcfi91:
	.cfi_offset %r15, -24
.Lcfi92:
	.cfi_offset %rbp, -16
	movl	12(%rdx), %eax
	movsd	ipow20(,%rax,8), %xmm0  # xmm0 = mem[0],zero
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movl	$73, %eax
	.p2align	4, 0x90
.LBB12_1:                               # =>This Inner Loop Header: Depth=1
	movupd	(%rdi), %xmm4
	movupd	16(%rdi), %xmm3
	movupd	32(%rdi), %xmm1
	movupd	48(%rdi), %xmm2
	mulpd	%xmm0, %xmm3
	mulpd	%xmm0, %xmm4
	cvttsd2si	%xmm4, %ecx
	movapd	%xmm4, %xmm5
	movhlps	%xmm5, %xmm5            # xmm5 = xmm5[1,1]
	cvttsd2si	%xmm5, %r9d
	cvttsd2si	%xmm3, %r8d
	movapd	%xmm3, %xmm5
	movhlps	%xmm5, %xmm5            # xmm5 = xmm5[1,1]
	cvttsd2si	%xmm5, %edx
	mulpd	%xmm0, %xmm2
	mulpd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %r10d
	movapd	%xmm1, %xmm5
	movhlps	%xmm5, %xmm5            # xmm5 = xmm5[1,1]
	cvttsd2si	%xmm5, %r11d
	cvttsd2si	%xmm2, %r14d
	movslq	%ecx, %rcx
	movapd	%xmm2, %xmm5
	movhlps	%xmm5, %xmm5            # xmm5 = xmm5[1,1]
	cvttsd2si	%xmm5, %r15d
	movslq	%r9d, %rbx
	movslq	%r8d, %rbp
	movslq	%edx, %rdx
	movsd	adj43(,%rbp,8), %xmm5   # xmm5 = mem[0],zero
	movhpd	adj43(,%rdx,8), %xmm5   # xmm5 = xmm5[0],mem[0]
	movsd	adj43(,%rcx,8), %xmm6   # xmm6 = mem[0],zero
	movhpd	adj43(,%rbx,8), %xmm6   # xmm6 = xmm6[0],mem[0]
	addpd	%xmm4, %xmm6
	addpd	%xmm3, %xmm5
	movslq	%r10d, %rcx
	cvttpd2dq	%xmm5, %xmm3
	cvttpd2dq	%xmm6, %xmm4
	punpcklqdq	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0]
	movsd	adj43(,%rcx,8), %xmm3   # xmm3 = mem[0],zero
	movdqu	%xmm4, (%rsi)
	movslq	%r11d, %rcx
	movslq	%r14d, %rdx
	movslq	%r15d, %rbp
	movhpd	adj43(,%rcx,8), %xmm3   # xmm3 = xmm3[0],mem[0]
	movsd	adj43(,%rdx,8), %xmm4   # xmm4 = mem[0],zero
	movhpd	adj43(,%rbp,8), %xmm4   # xmm4 = xmm4[0],mem[0]
	addpd	%xmm2, %xmm4
	addpd	%xmm1, %xmm3
	cvttpd2dq	%xmm3, %xmm1
	cvttpd2dq	%xmm4, %xmm2
	punpcklqdq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0]
	movdqu	%xmm1, 16(%rsi)
	decl	%eax
	addq	$64, %rdi
	addq	$32, %rsi
	cmpl	$1, %eax
	jg	.LBB12_1
# BB#2:
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	quantize_xrpow, .Lfunc_end12-quantize_xrpow
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI13_0:
	.quad	4603530900222145410     # double 0.59460000000000002
.LCPI13_1:
	.quad	4600974657073649916     # double 0.40539999999999998
	.text
	.globl	quantize_xrpow_ISO
	.p2align	4, 0x90
	.type	quantize_xrpow_ISO,@function
quantize_xrpow_ISO:                     # @quantize_xrpow_ISO
	.cfi_startproc
# BB#0:
	movl	12(%rdx), %eax
	movsd	ipow20(,%rax,8), %xmm0  # xmm0 = mem[0],zero
	movsd	.LCPI13_0(%rip), %xmm1  # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movl	$577, %eax              # imm = 0x241
	movsd	.LCPI13_1(%rip), %xmm2  # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB13_1:                               # =>This Inner Loop Header: Depth=1
	movsd	(%rdi), %xmm3           # xmm3 = mem[0],zero
	xorl	%ecx, %ecx
	ucomisd	%xmm3, %xmm1
	movl	$0, %edx
	ja	.LBB13_3
# BB#2:                                 #   in Loop: Header=BB13_1 Depth=1
	mulsd	%xmm0, %xmm3
	addsd	%xmm2, %xmm3
	cvttsd2si	%xmm3, %edx
.LBB13_3:                               #   in Loop: Header=BB13_1 Depth=1
	movl	%edx, (%rsi)
	movsd	8(%rdi), %xmm3          # xmm3 = mem[0],zero
	ucomisd	%xmm3, %xmm1
	ja	.LBB13_5
# BB#4:                                 #   in Loop: Header=BB13_1 Depth=1
	mulsd	%xmm0, %xmm3
	addsd	%xmm2, %xmm3
	cvttsd2si	%xmm3, %ecx
.LBB13_5:                               #   in Loop: Header=BB13_1 Depth=1
	movl	%ecx, 4(%rsi)
	addl	$-2, %eax
	addq	$16, %rdi
	addq	$8, %rsi
	cmpl	$1, %eax
	jg	.LBB13_1
# BB#6:
	retq
.Lfunc_end13:
	.size	quantize_xrpow_ISO, .Lfunc_end13-quantize_xrpow_ISO
	.cfi_endproc

	.type	masking_lower,@object   # @masking_lower
	.data
	.globl	masking_lower
	.p2align	2
masking_lower:
	.long	1065353216              # float 1
	.size	masking_lower, 4

	.type	nr_of_sfb_block,@object # @nr_of_sfb_block
	.globl	nr_of_sfb_block
	.p2align	4
nr_of_sfb_block:
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	6                       # 0x6
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	7                       # 0x7
	.long	3                       # 0x3
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	12                      # 0xc
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	9                       # 0x9
	.long	12                      # 0xc
	.long	6                       # 0x6
	.long	11                      # 0xb
	.long	10                      # 0xa
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	18                      # 0x12
	.long	18                      # 0x12
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	15                      # 0xf
	.long	18                      # 0x12
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	0                       # 0x0
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	15                      # 0xf
	.long	12                      # 0xc
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	3                       # 0x3
	.long	12                      # 0xc
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	12                      # 0xc
	.long	9                       # 0x9
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	15                      # 0xf
	.long	12                      # 0xc
	.long	9                       # 0x9
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	18                      # 0x12
	.long	9                       # 0x9
	.long	0                       # 0x0
	.size	nr_of_sfb_block, 288

	.type	pretab,@object          # @pretab
	.globl	pretab
	.p2align	4
pretab:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.size	pretab, 84

	.type	sfBandIndex,@object     # @sfBandIndex
	.globl	sfBandIndex
	.p2align	4
sfBandIndex:
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	12                      # 0xc
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	30                      # 0x1e
	.long	36                      # 0x24
	.long	44                      # 0x2c
	.long	54                      # 0x36
	.long	66                      # 0x42
	.long	80                      # 0x50
	.long	96                      # 0x60
	.long	116                     # 0x74
	.long	140                     # 0x8c
	.long	168                     # 0xa8
	.long	200                     # 0xc8
	.long	238                     # 0xee
	.long	284                     # 0x11c
	.long	336                     # 0x150
	.long	396                     # 0x18c
	.long	464                     # 0x1d0
	.long	522                     # 0x20a
	.long	576                     # 0x240
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	32                      # 0x20
	.long	42                      # 0x2a
	.long	56                      # 0x38
	.long	74                      # 0x4a
	.long	100                     # 0x64
	.long	132                     # 0x84
	.long	174                     # 0xae
	.long	192                     # 0xc0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	12                      # 0xc
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	30                      # 0x1e
	.long	36                      # 0x24
	.long	44                      # 0x2c
	.long	54                      # 0x36
	.long	66                      # 0x42
	.long	80                      # 0x50
	.long	96                      # 0x60
	.long	114                     # 0x72
	.long	136                     # 0x88
	.long	162                     # 0xa2
	.long	194                     # 0xc2
	.long	232                     # 0xe8
	.long	278                     # 0x116
	.long	332                     # 0x14c
	.long	394                     # 0x18a
	.long	464                     # 0x1d0
	.long	540                     # 0x21c
	.long	576                     # 0x240
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	18                      # 0x12
	.long	26                      # 0x1a
	.long	36                      # 0x24
	.long	48                      # 0x30
	.long	62                      # 0x3e
	.long	80                      # 0x50
	.long	104                     # 0x68
	.long	136                     # 0x88
	.long	180                     # 0xb4
	.long	192                     # 0xc0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	12                      # 0xc
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	30                      # 0x1e
	.long	36                      # 0x24
	.long	44                      # 0x2c
	.long	54                      # 0x36
	.long	66                      # 0x42
	.long	80                      # 0x50
	.long	96                      # 0x60
	.long	116                     # 0x74
	.long	140                     # 0x8c
	.long	168                     # 0xa8
	.long	200                     # 0xc8
	.long	238                     # 0xee
	.long	284                     # 0x11c
	.long	336                     # 0x150
	.long	396                     # 0x18c
	.long	464                     # 0x1d0
	.long	522                     # 0x20a
	.long	576                     # 0x240
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	18                      # 0x12
	.long	26                      # 0x1a
	.long	36                      # 0x24
	.long	48                      # 0x30
	.long	62                      # 0x3e
	.long	80                      # 0x50
	.long	104                     # 0x68
	.long	134                     # 0x86
	.long	174                     # 0xae
	.long	192                     # 0xc0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	24                      # 0x18
	.long	30                      # 0x1e
	.long	36                      # 0x24
	.long	44                      # 0x2c
	.long	52                      # 0x34
	.long	62                      # 0x3e
	.long	74                      # 0x4a
	.long	90                      # 0x5a
	.long	110                     # 0x6e
	.long	134                     # 0x86
	.long	162                     # 0xa2
	.long	196                     # 0xc4
	.long	238                     # 0xee
	.long	288                     # 0x120
	.long	342                     # 0x156
	.long	418                     # 0x1a2
	.long	576                     # 0x240
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	16                      # 0x10
	.long	22                      # 0x16
	.long	30                      # 0x1e
	.long	40                      # 0x28
	.long	52                      # 0x34
	.long	66                      # 0x42
	.long	84                      # 0x54
	.long	106                     # 0x6a
	.long	136                     # 0x88
	.long	192                     # 0xc0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	24                      # 0x18
	.long	30                      # 0x1e
	.long	36                      # 0x24
	.long	42                      # 0x2a
	.long	50                      # 0x32
	.long	60                      # 0x3c
	.long	72                      # 0x48
	.long	88                      # 0x58
	.long	106                     # 0x6a
	.long	128                     # 0x80
	.long	156                     # 0x9c
	.long	190                     # 0xbe
	.long	230                     # 0xe6
	.long	276                     # 0x114
	.long	330                     # 0x14a
	.long	384                     # 0x180
	.long	576                     # 0x240
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	16                      # 0x10
	.long	22                      # 0x16
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	50                      # 0x32
	.long	64                      # 0x40
	.long	80                      # 0x50
	.long	100                     # 0x64
	.long	126                     # 0x7e
	.long	192                     # 0xc0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	24                      # 0x18
	.long	30                      # 0x1e
	.long	36                      # 0x24
	.long	44                      # 0x2c
	.long	54                      # 0x36
	.long	66                      # 0x42
	.long	82                      # 0x52
	.long	102                     # 0x66
	.long	126                     # 0x7e
	.long	156                     # 0x9c
	.long	194                     # 0xc2
	.long	240                     # 0xf0
	.long	296                     # 0x128
	.long	364                     # 0x16c
	.long	448                     # 0x1c0
	.long	550                     # 0x226
	.long	576                     # 0x240
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	16                      # 0x10
	.long	22                      # 0x16
	.long	30                      # 0x1e
	.long	42                      # 0x2a
	.long	58                      # 0x3a
	.long	78                      # 0x4e
	.long	104                     # 0x68
	.long	138                     # 0x8a
	.long	180                     # 0xb4
	.long	192                     # 0xc0
	.size	sfBandIndex, 888

	.type	scalefac_band,@object   # @scalefac_band
	.comm	scalefac_band,148,4
	.type	ATH_l,@object           # @ATH_l
	.local	ATH_l
	.comm	ATH_l,168,16
	.type	ATH_s,@object           # @ATH_s
	.local	ATH_s
	.comm	ATH_s,168,16
	.type	pow43,@object           # @pow43
	.comm	pow43,65664,16
	.type	adj43,@object           # @adj43
	.local	adj43
	.comm	adj43,65664,16
	.type	ipow20,@object          # @ipow20
	.comm	ipow20,2048,16
	.type	pow20,@object           # @pow20
	.comm	pow20,2048,16
	.type	convert_mdct,@object    # @convert_mdct
	.comm	convert_mdct,4,4
	.type	reduce_sidechannel,@object # @reduce_sidechannel
	.comm	reduce_sidechannel,4,4
	.type	scale_bitcount.slen1,@object # @scale_bitcount.slen1
	.section	.rodata,"a",@progbits
	.p2align	4
scale_bitcount.slen1:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	8                       # 0x8
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	16                      # 0x10
	.long	16                      # 0x10
	.size	scale_bitcount.slen1, 64

	.type	scale_bitcount.slen2,@object # @scale_bitcount.slen2
	.p2align	4
scale_bitcount.slen2:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	4                       # 0x4
	.long	8                       # 0x8
	.size	scale_bitcount.slen2, 64

	.type	scale_bitcount.slen1_tab,@object # @scale_bitcount.slen1_tab
	.p2align	4
scale_bitcount.slen1_tab:
	.long	0                       # 0x0
	.long	18                      # 0x12
	.long	36                      # 0x24
	.long	54                      # 0x36
	.long	54                      # 0x36
	.long	36                      # 0x24
	.long	54                      # 0x36
	.long	72                      # 0x48
	.long	54                      # 0x36
	.long	72                      # 0x48
	.long	90                      # 0x5a
	.long	72                      # 0x48
	.long	90                      # 0x5a
	.long	108                     # 0x6c
	.long	108                     # 0x6c
	.long	126                     # 0x7e
	.size	scale_bitcount.slen1_tab, 64

	.type	scale_bitcount.slen2_tab,@object # @scale_bitcount.slen2_tab
	.p2align	4
scale_bitcount.slen2_tab:
	.long	0                       # 0x0
	.long	10                      # 0xa
	.long	20                      # 0x14
	.long	30                      # 0x1e
	.long	33                      # 0x21
	.long	21                      # 0x15
	.long	31                      # 0x1f
	.long	41                      # 0x29
	.long	32                      # 0x20
	.long	42                      # 0x2a
	.long	52                      # 0x34
	.long	43                      # 0x2b
	.long	53                      # 0x35
	.long	63                      # 0x3f
	.long	64                      # 0x40
	.long	74                      # 0x4a
	.size	scale_bitcount.slen2_tab, 64

	.type	max_range_sfac_tab,@object # @max_range_sfac_tab
	.p2align	4
max_range_sfac_tab:
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	7                       # 0x7
	.long	0                       # 0x0
	.long	7                       # 0x7
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	15                      # 0xf
	.long	31                      # 0x1f
	.long	31                      # 0x1f
	.long	0                       # 0x0
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	max_range_sfac_tab, 96

	.type	scale_bitcount_lsf.log2tab,@object # @scale_bitcount_lsf.log2tab
	.p2align	4
scale_bitcount_lsf.log2tab:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.size	scale_bitcount_lsf.log2tab, 64

	.type	bin_search_StepSize2.CurrentStep,@object # @bin_search_StepSize2.CurrentStep
	.data
	.p2align	2
bin_search_StepSize2.CurrentStep:
	.long	4                       # 0x4
	.size	bin_search_StepSize2.CurrentStep, 4

	.type	ATH_mdct_long,@object   # @ATH_mdct_long
	.comm	ATH_mdct_long,4608,16
	.type	ATH_mdct_short,@object  # @ATH_mdct_short
	.comm	ATH_mdct_short,1536,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
