	.text
	.file	"picmp.bc"
	.globl	picmp
	.p2align	4, 0x90
	.type	picmp,@function
picmp:                                  # @picmp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB0_2
# BB#1:
	incw	(%rbx)
.LBB0_2:
	cmpb	$0, 6(%rbx)
	je	.LBB0_8
# BB#3:
	movl	$-1, %r14d
	testl	%ebp, %ebp
	jns	.LBB0_13
# BB#4:
	cmpl	$-65536, %ebp           # imm = 0xFFFF0000
	jg	.LBB0_6
# BB#5:
	movl	$4, %edi
	movl	$.L.str, %esi
	movl	$cmpError, %edx
	callq	errorp
.LBB0_6:
	movzwl	4(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB0_13
# BB#7:
	movzwl	8(%rbx), %r14d
	addl	%ebp, %r14d
	negl	%r14d
	decw	(%rbx)
	jne	.LBB0_15
	jmp	.LBB0_14
.LBB0_8:
	movl	$1, %r14d
	testl	%ebp, %ebp
	js	.LBB0_13
# BB#9:
	cmpl	$65536, %ebp            # imm = 0x10000
	jl	.LBB0_11
# BB#10:
	movl	$4, %edi
	movl	$.L.str, %esi
	movl	$cmpError, %edx
	callq	errorp
.LBB0_11:
	movzwl	4(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB0_13
# BB#12:
	movzwl	8(%rbx), %r14d
	subl	%ebp, %r14d
.LBB0_13:
	decw	(%rbx)
	jne	.LBB0_15
.LBB0_14:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	pfree
.LBB0_15:
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	picmp, .Lfunc_end0-picmp
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"picmp"
	.size	.L.str, 6

	.type	cmpError,@object        # @cmpError
	.data
	.p2align	4
cmpError:
	.asciz	"Second arg not single digit"
	.size	cmpError, 28


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
