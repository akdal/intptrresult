	.text
	.file	"query.bc"
	.globl	query
	.p2align	4, 0x90
	.type	query,@function
query:                                  # @query
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, %rbp
	movq	%rdx, %r14
	movq	%rsi, %r13
	movq	%rdi, %r15
	cmpb	$1, %cl
	jne	.LBB0_6
# BB#1:
	movq	%r13, %rdi
	callq	validIndexKey
	testb	%al, %al
	je	.LBB0_2
# BB#3:
	movq	%r14, %rdi
	callq	validAttributes
	testb	%al, %al
	je	.LBB0_4
.LBB0_6:
	cmpq	$0, (%r15)
	movq	8(%r15), %rbx
	jg	.LBB0_27
# BB#7:                                 # %.preheader48
	testq	%rbx, %rbx
	je	.LBB0_19
# BB#8:                                 # %.lr.ph57
	testq	%r14, %r14
	je	.LBB0_9
# BB#13:
	movq	%rbp, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB0_14:                               # %.lr.ph57.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_20 Depth 2
	leaq	8(%rbx), %rdi
	movq	%r13, %rsi
	callq	consistentKey
	cmpb	$1, %al
	jne	.LBB0_18
# BB#15:                                #   in Loop: Header=BB0_14 Depth=1
	movq	(%rbx), %rbp
	movl	(%rbp), %eax
	decl	%eax
	cmpl	$2, %eax
	ja	.LBB0_16
# BB#25:                                # %switch.lookup.us
                                        #   in Loop: Header=BB0_14 Depth=1
	cltq
	movq	.Lswitch.table(,%rax,8), %r15
	movq	%r14, %r12
	jmp	.LBB0_20
	.p2align	4, 0x90
.LBB0_16:                               #   in Loop: Header=BB0_14 Depth=1
	xorl	%r15d, %r15d
	movq	%r14, %r12
	.p2align	4, 0x90
.LBB0_20:                               #   Parent Loop BB0_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12), %rcx
	movb	$1, %al
	cmpq	%r15, %rcx
	jge	.LBB0_22
# BB#21:                                #   in Loop: Header=BB0_20 Depth=2
	movq	8(%rbp), %rax
	movq	(%rax,%rcx,8), %rdi
	movq	8(%r12), %rsi
	callq	consistentNonKey
.LBB0_22:                               #   in Loop: Header=BB0_20 Depth=2
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.LBB0_24
# BB#23:                                #   in Loop: Header=BB0_20 Depth=2
	cmpb	$1, %al
	je	.LBB0_20
.LBB0_24:                               # %..critedge_crit_edge.us
                                        #   in Loop: Header=BB0_14 Depth=1
	cmpb	$1, %al
	movq	(%rsp), %rax            # 8-byte Reload
	jne	.LBB0_18
# BB#17:                                #   in Loop: Header=BB0_14 Depth=1
	movq	(%rbx), %rdi
	callq	*%rax
.LBB0_18:                               #   in Loop: Header=BB0_14 Depth=1
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_14
	jmp	.LBB0_19
	.p2align	4, 0x90
.LBB0_30:                               #   in Loop: Header=BB0_27 Depth=1
	movq	40(%rbx), %rbx
.LBB0_27:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	je	.LBB0_19
# BB#28:                                # %.lr.ph
                                        #   in Loop: Header=BB0_27 Depth=1
	leaq	8(%rbx), %rdi
	movq	%r13, %rsi
	callq	consistentKey
	cmpb	$1, %al
	jne	.LBB0_30
# BB#29:                                #   in Loop: Header=BB0_27 Depth=1
	movq	(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%rbp, %r8
	callq	query
	jmp	.LBB0_30
.LBB0_19:
	xorl	%r14d, %r14d
	jmp	.LBB0_5
.LBB0_2:
	movl	$.L.str, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$query.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$1, %r14d
	jmp	.LBB0_5
.LBB0_4:
	movl	$.L.str.1, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$query.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$2, %r14d
	jmp	.LBB0_5
.LBB0_9:
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_10:                               # %.lr.ph57.split
                                        # =>This Inner Loop Header: Depth=1
	leaq	8(%rbx), %rdi
	movq	%r13, %rsi
	callq	consistentKey
	cmpb	$1, %al
	jne	.LBB0_12
# BB#11:                                # %.critedge
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	(%rbx), %rdi
	callq	*%rbp
.LBB0_12:                               #   in Loop: Header=BB0_10 Depth=1
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_10
.LBB0_5:                                # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	query, .Lfunc_end0-query
	.cfi_endproc

	.type	query.name,@object      # @query.name
	.data
query.name:
	.asciz	"query"
	.size	query.name, 6

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"invalid index key search values"
	.size	.L.str, 32

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"invalid non-key search values"
	.size	.L.str.1, 30

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.quad	18                      # 0x12
	.quad	25                      # 0x19
	.quad	51                      # 0x33
	.size	.Lswitch.table, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
