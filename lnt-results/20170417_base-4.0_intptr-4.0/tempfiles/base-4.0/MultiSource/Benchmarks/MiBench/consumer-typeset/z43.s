	.text
	.file	"z43.bc"
	.globl	LanguageInit
	.p2align	4, 0x90
	.type	LanguageInit,@function
LanguageInit:                           # @LanguageInit
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	$808, %edi              # imm = 0x328
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB0_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$43, %edi
	movl	$1, %esi
	movl	$.L.str.16, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB0_2:                                # %ltab_new.exit
	movl	$100, (%rbx)
	movq	%rbx, %rdi
	addq	$4, %rdi
	xorl	%esi, %esi
	movl	$804, %edx              # imm = 0x324
	callq	memset
	movq	%rbx, names_tab(%rip)
	movl	$0, lang_count(%rip)
	movl	$100, lang_tabsize(%rip)
	movl	$800, %edi              # imm = 0x320
	callq	malloc
	movq	%rax, hyph_tab(%rip)
	movl	$800, %edi              # imm = 0x320
	callq	malloc
	movq	%rax, canonical_tab(%rip)
	movl	$LanguageSentenceEnds, %edi
	xorl	%esi, %esi
	movl	$1024, %edx             # imm = 0x400
	callq	memset
	popq	%rbx
	retq
.Lfunc_end0:
	.size	LanguageInit, .Lfunc_end0-LanguageInit
	.cfi_endproc

	.globl	LanguageDefine
	.p2align	4, 0x90
	.type	LanguageDefine,@function
LanguageDefine:                         # @LanguageDefine
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 64
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB1_2
# BB#1:
	cmpb	$17, 32(%r15)
	je	.LBB1_3
.LBB1_2:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_3:
	cmpq	%r15, 8(%r15)
	jne	.LBB1_5
# BB#4:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_5:
	movl	lang_count(%rip), %eax
	incl	%eax
	movl	%eax, lang_count(%rip)
	movslq	lang_tabsize(%rip), %rsi
	cmpl	%esi, %eax
	jl	.LBB1_7
# BB#6:
	leal	(%rsi,%rsi), %eax
	movl	%eax, lang_tabsize(%rip)
	movq	hyph_tab(%rip), %rdi
	shlq	$4, %rsi
	callq	realloc
	movq	%rax, hyph_tab(%rip)
	movq	canonical_tab(%rip), %rdi
	movslq	lang_tabsize(%rip), %rsi
	shlq	$3, %rsi
	callq	realloc
	movq	%rax, canonical_tab(%rip)
.LBB1_7:                                # %.preheader156
	movq	8(%r15), %r12
	cmpq	%r15, %r12
	je	.LBB1_15
# BB#8:
	movl	$-528482305, %ebp       # imm = 0xE07FFFFF
	.p2align	4, 0x90
.LBB1_9:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_10 Depth 2
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB1_10:                               #   Parent Loop BB1_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB1_10
# BB#11:                                #   in Loop: Header=BB1_9 Depth=1
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB1_13
# BB#12:                                #   in Loop: Header=BB1_9 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.3, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_13:                               # %.loopexit155
                                        #   in Loop: Header=BB1_9 Depth=1
	movl	lang_count(%rip), %eax
	andl	$63, %eax
	shll	$23, %eax
	movl	40(%rbx), %ecx
	andl	%ebp, %ecx
	orl	%eax, %ecx
	movl	%ecx, 40(%rbx)
	movl	$names_tab, %esi
	movq	%rbx, %rdi
	callq	ltab_insert
	movq	8(%r12), %r12
	cmpq	%r15, %r12
	jne	.LBB1_9
# BB#14:                                # %._crit_edge169.loopexit
	movq	8(%r15), %r12
.LBB1_15:                               # %._crit_edge169
	addq	$16, %r12
	.p2align	4, 0x90
.LBB1_16:                               # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	leaq	16(%rax), %r12
	cmpb	$0, 32(%rax)
	je	.LBB1_16
# BB#17:
	movq	canonical_tab(%rip), %rcx
	movslq	lang_count(%rip), %rdx
	movq	%rax, (%rcx,%rdx,8)
	cmpb	$17, 32(%r14)
	jne	.LBB1_19
# BB#18:
	movq	%r14, %r15
	jmp	.LBB1_30
.LBB1_19:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r15
	testq	%r15, %r15
	je	.LBB1_20
# BB#21:
	movq	%r15, zz_hold(%rip)
	movq	(%r15), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_22
.LBB1_20:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r15
	movq	%r15, zz_hold(%rip)
.LBB1_22:
	movb	$17, 32(%r15)
	movq	%r15, 24(%r15)
	movq	%r15, 16(%r15)
	movq	%r15, 8(%r15)
	movq	%r15, (%r15)
	movzwl	34(%r14), %eax
	movw	%ax, 34(%r15)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r14), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%r15), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%r15)
	andl	36(%r14), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%r15)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_23
# BB#24:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_25
.LBB1_23:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_25:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB1_28
# BB#26:
	testq	%rax, %rax
	je	.LBB1_28
# BB#27:
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_28:
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_30
# BB#29:
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_30:
	movq	8(%r15), %rcx
	leaq	16(%rcx), %rdx
	.p2align	4, 0x90
.LBB1_31:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rbx
	movzbl	32(%rbx), %eax
	leaq	16(%rbx), %rdx
	testb	%al, %al
	je	.LBB1_31
# BB#32:
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	24(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB1_34
# BB#33:
	movq	%rdx, zz_res(%rip)
	movq	16(%rcx), %rsi
	movq	%rsi, 16(%rdx)
	movq	16(%rcx), %rsi
	movq	%rdx, 24(%rsi)
	movq	%rcx, 24(%rcx)
	movq	%rcx, 16(%rcx)
.LBB1_34:
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB1_36
# BB#35:
	movq	%rdx, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdx)
	movq	zz_res(%rip), %rcx
	movq	zz_hold(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rsi)
	movq	%rdx, 8(%rdx)
	movq	%rdx, (%rdx)
	movq	xx_link(%rip), %rcx
.LBB1_36:
	movq	%rcx, %rbp
	movq	%rcx, zz_hold(%rip)
	movzbl	32(%rbp), %esi
	leaq	zz_lengths(%rsi), %rdi
	movl	%esi, %edx
	addb	$-11, %dl
	addq	$33, %rcx
	cmpb	$2, %dl
	cmovbq	%rcx, %rdi
	movzbl	(%rdi), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rbp)
	movq	zz_hold(%rip), %rdx
	movq	%rdx, zz_free(,%rcx,8)
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB1_38
# BB#37:
	leaq	32(%r15), %r8
	movl	$43, %edi
	movl	$3, %esi
	movl	$.L.str.5, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB1_38:
	movb	64(%rbx), %al
	testb	%al, %al
	je	.LBB1_41
# BB#39:
	cmpb	$45, %al
	jne	.LBB1_42
# BB#40:
	cmpb	$0, 65(%rbx)
	jne	.LBB1_42
.LBB1_41:
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbx), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	xorl	%ebx, %ebx
.LBB1_42:                               # %.thread
	movq	hyph_tab(%rip), %rax
	movslq	lang_count(%rip), %rcx
	movq	%rbx, (%rax,%rcx,8)
	movq	%r15, lang_ends(,%rcx,8)
	movq	8(%r15), %r12
	cmpq	%r15, %r12
	je	.LBB1_60
# BB#43:                                # %.lr.ph.preheader
	movabsq	$-4294967296, %r13      # imm = 0xFFFFFFFF00000000
	jmp	.LBB1_44
.LBB1_49:                               #   in Loop: Header=BB1_44 Depth=1
	xorl	%ecx, %ecx
.LBB1_51:                               #   in Loop: Header=BB1_44 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_53
# BB#52:                                #   in Loop: Header=BB1_44 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_53:                               #   in Loop: Header=BB1_44 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_59
# BB#54:                                #   in Loop: Header=BB1_44 Depth=1
	callq	DisposeObject
	jmp	.LBB1_59
	.p2align	4, 0x90
.LBB1_44:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_45 Depth 2
	leaq	16(%r12), %rbp
	jmp	.LBB1_45
	.p2align	4, 0x90
.LBB1_63:                               #   in Loop: Header=BB1_45 Depth=2
	addq	$16, %rbp
.LBB1_45:                               #   Parent Loop BB1_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB1_63
# BB#46:                                #   in Loop: Header=BB1_44 Depth=1
	leaq	32(%rbp), %r14
	movl	%eax, %ecx
	addb	$-11, %cl
	cmpb	$2, %cl
	jb	.LBB1_56
# BB#47:                                #   in Loop: Header=BB1_44 Depth=1
	cmpb	$1, %al
	jne	.LBB1_55
# BB#48:                                #   in Loop: Header=BB1_44 Depth=1
	movq	(%r12), %r12
	movq	8(%r12), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_49
# BB#50:                                #   in Loop: Header=BB1_44 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB1_51
	.p2align	4, 0x90
.LBB1_55:                               #   in Loop: Header=BB1_44 Depth=1
	movl	$43, %edi
	movl	$4, %esi
	movl	$.L.str.8, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	callq	Error
.LBB1_56:                               # %.loopexit
                                        #   in Loop: Header=BB1_44 Depth=1
	leaq	64(%rbp), %rdi
	callq	strlen
	movq	%rax, %rbx
	testl	%ebx, %ebx
	jne	.LBB1_58
# BB#57:                                #   in Loop: Header=BB1_44 Depth=1
	movl	$43, %edi
	movl	$5, %esi
	movl	$.L.str.9, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	callq	Error
.LBB1_58:                               #   in Loop: Header=BB1_44 Depth=1
	shlq	$32, %rbx
	addq	%r13, %rbx
	sarq	$32, %rbx
	movzbl	64(%rbp,%rbx), %eax
	movl	$1, LanguageSentenceEnds(,%rax,4)
.LBB1_59:                               #   in Loop: Header=BB1_44 Depth=1
	movq	8(%r12), %r12
	cmpq	%r15, %r12
	jne	.LBB1_44
.LBB1_60:                               # %._crit_edge
	cmpl	$0, InitializeAll(%rip)
	je	.LBB1_62
# BB#61:
	movq	hyph_tab(%rip), %rax
	movslq	lang_count(%rip), %rdi
	cmpq	$0, (%rax,%rdi,8)
	je	.LBB1_62
# BB#64:
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	ReadHyphTable           # TAILCALL
.LBB1_62:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	LanguageDefine, .Lfunc_end1-LanguageDefine
	.cfi_endproc

	.p2align	4, 0x90
	.type	ltab_insert,@function
ltab_insert:                            # @ltab_insert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 96
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r15
	movq	(%r13), %r12
	movslq	(%r12), %rbp
	leal	-1(%rbp), %eax
	cmpl	%eax, 4(%r12)
	movq	%r13, 16(%rsp)          # 8-byte Spill
	jne	.LBB2_12
# BB#1:
	leal	(%rbp,%rbp), %r14d
	movq	%rbp, %rdi
	shlq	$4, %rdi
	orq	$8, %rdi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB2_3
# BB#2:
	movq	no_fpos(%rip), %r8
	movl	$43, %edi
	movl	$1, %esi
	movl	$.L.str.16, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB2_3:
	movl	%r14d, (%rbx)
	movl	$0, 4(%rbx)
	testl	%ebp, %ebp
	jle	.LBB2_5
# BB#4:                                 # %.lr.ph.i.i
	movq	%rbx, %rdi
	addq	$8, %rdi
	decl	%r14d
	leaq	8(,%r14,8), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB2_5:                                # %ltab_new.exit.i
	movq	%rbx, 32(%rsp)
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.LBB2_11
# BB#6:                                 # %.lr.ph63.preheader
	xorl	%ebp, %ebp
	leaq	32(%rsp), %rbx
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph63
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r12,%rbp,8), %rdi
	testq	%rdi, %rdi
	je	.LBB2_9
# BB#8:                                 #   in Loop: Header=BB2_7 Depth=1
	movq	%rbx, %rsi
	callq	ltab_insert
	movl	(%r12), %eax
.LBB2_9:                                #   in Loop: Header=BB2_7 Depth=1
	movslq	%eax, %rcx
	incq	%rbp
	cmpq	%rcx, %rbp
	jl	.LBB2_7
# BB#10:                                # %ltab_rehash.exit.loopexit
	movq	32(%rsp), %rbx
.LBB2_11:                               # %ltab_rehash.exit
	movq	%r12, %rdi
	callq	free
	movq	%rbx, (%r13)
	movq	%rbx, %r12
.LBB2_12:
	leaq	64(%r15), %r13
	movzbl	64(%r15), %edx
	leaq	65(%r15), %rcx
	.p2align	4, 0x90
.LBB2_13:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, %eax
	movzbl	(%rcx), %esi
	leal	(%rsi,%rax), %edx
	incq	%rcx
	testl	%esi, %esi
	jne	.LBB2_13
# BB#14:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	cltd
	idivl	(%r12)
	movslq	%edx, %rbp
	cmpq	$0, 8(%r12,%rbp,8)
	jne	.LBB2_19
# BB#15:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_16
# BB#17:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_18
.LBB2_16:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_18:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rcx
	movq	%rax, 8(%rcx,%rbp,8)
	movq	(%rdx), %r12
.LBB2_19:
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	8(%r12,%rbp,8), %r14
	movq	8(%r14), %r15
	cmpq	%r14, %r15
	je	.LBB2_25
# BB#20:                                # %.lr.ph
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	32(%rax), %r12
	.p2align	4, 0x90
.LBB2_21:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_22 Depth 2
	leaq	16(%r15), %rax
	.p2align	4, 0x90
.LBB2_22:                               #   Parent Loop BB2_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rbx
	leaq	16(%rbx), %rax
	cmpb	$0, 32(%rbx)
	je	.LBB2_22
# BB#23:                                #   in Loop: Header=BB2_21 Depth=1
	leaq	64(%rbx), %rsi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB2_24
# BB#35:                                #   in Loop: Header=BB2_21 Depth=1
	addq	$32, %rbx
	movq	%rbx, %rdi
	callq	EchoFilePos
	movq	%rax, (%rsp)
	movl	$43, %edi
	movl	$2, %esi
	movl	$.L.str.17, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r12, %r8
	movq	%r13, %r9
	callq	Error
.LBB2_24:                               # %.backedge
                                        #   in Loop: Header=BB2_21 Depth=1
	movq	8(%r15), %r15
	cmpq	%r14, %r15
	jne	.LBB2_21
.LBB2_25:                               # %._crit_edge
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_26
# BB#27:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_28
.LBB2_26:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_28:
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	8(%rcx,%rbp,8), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_31
# BB#29:
	testq	%rcx, %rcx
	je	.LBB2_31
# BB#30:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_31:
	movq	%rax, zz_res(%rip)
	movq	%rsi, zz_hold(%rip)
	testq	%rsi, %rsi
	je	.LBB2_34
# BB#32:
	testq	%rax, %rax
	je	.LBB2_34
# BB#33:
	movq	16(%rsi), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
	movq	16(%rax), %rdx
	movq	%rsi, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_34:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	ltab_insert, .Lfunc_end2-ltab_insert
	.cfi_endproc

	.globl	LanguageWordEndsSentence
	.p2align	4, 0x90
	.type	LanguageWordEndsSentence,@function
LanguageWordEndsSentence:               # @LanguageWordEndsSentence
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 64
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	%rdi, %r12
	movb	32(%r12), %al
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB3_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.10, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_2:
	movl	40(%r12), %eax
	shrl	$23, %eax
	andl	$63, %eax
	movq	lang_ends(,%rax,8), %r14
	movq	8(%r14), %r15
	xorl	%ebp, %ebp
	cmpq	%r14, %r15
	je	.LBB3_18
# BB#3:                                 # %.lr.ph
	leaq	64(%r12), %r13
	testl	%ebx, %ebx
	je	.LBB3_12
# BB#4:
	leaq	32(%r12), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_6 Depth 2
	leaq	16(%r15), %rax
	.p2align	4, 0x90
.LBB3_6:                                #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rbx
	leaq	16(%rbx), %rax
	cmpb	$0, 32(%rbx)
	je	.LBB3_6
# BB#7:                                 #   in Loop: Header=BB3_5 Depth=1
	addq	$64, %rbx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	StringEndsWith
	testl	%eax, %eax
	je	.LBB3_10
# BB#8:                                 #   in Loop: Header=BB3_5 Depth=1
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	strlen
	subq	%rax, %rbp
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	addq	%rax, %rbp
	testl	%ebp, %ebp
	js	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_5 Depth=1
	movslq	%ebp, %rax
	movl	40(%r12), %edi
	movl	$4095, %ecx             # imm = 0xFFF
	andl	%ecx, %edi
	movzbl	64(%r12,%rax), %ebx
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	FontMapping
	movl	%ebx, %edi
	movl	%eax, %esi
	callq	MapIsLowerCase
	testl	%eax, %eax
	jne	.LBB3_17
.LBB3_10:                               # %.backedge
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	8(%r15), %r15
	cmpq	%r14, %r15
	jne	.LBB3_5
# BB#11:
	xorl	%ebp, %ebp
	jmp	.LBB3_18
	.p2align	4, 0x90
.LBB3_12:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_13 Depth 2
	leaq	16(%r15), %rax
	.p2align	4, 0x90
.LBB3_13:                               #   Parent Loop BB3_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rsi
	leaq	16(%rsi), %rax
	cmpb	$0, 32(%rsi)
	je	.LBB3_13
# BB#14:                                #   in Loop: Header=BB3_12 Depth=1
	addq	$64, %rsi
	movq	%r13, %rdi
	callq	StringEndsWith
	testl	%eax, %eax
	jne	.LBB3_17
# BB#15:                                # %.backedge.us
                                        #   in Loop: Header=BB3_12 Depth=1
	movq	8(%r15), %r15
	cmpq	%r14, %r15
	jne	.LBB3_12
	jmp	.LBB3_18
.LBB3_17:
	movl	$1, %ebp
.LBB3_18:                               # %._crit_edge
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	LanguageWordEndsSentence, .Lfunc_end3-LanguageWordEndsSentence
	.cfi_endproc

	.globl	LanguageChange
	.p2align	4, 0x90
	.type	LanguageChange,@function
LanguageChange:                         # @LanguageChange
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi45:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi47:
	.cfi_def_cfa_offset 64
.Lcfi48:
	.cfi_offset %rbx, -56
.Lcfi49:
	.cfi_offset %r12, -48
.Lcfi50:
	.cfi_offset %r13, -40
.Lcfi51:
	.cfi_offset %r14, -32
.Lcfi52:
	.cfi_offset %r15, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	leaq	32(%rsi), %r15
	movb	32(%rsi), %al
	addb	$-11, %al
	cmpb	$2, %al
	jae	.LBB4_14
# BB#1:
	movzbl	64(%rsi), %edx
	testl	%edx, %edx
	je	.LBB4_13
# BB#2:
	movq	names_tab(%rip), %rcx
	leaq	64(%rsi), %r12
	addq	$65, %rsi
	.p2align	4, 0x90
.LBB4_3:                                # =>This Inner Loop Header: Depth=1
	movl	%edx, %eax
	movzbl	(%rsi), %edi
	leal	(%rdi,%rax), %edx
	incq	%rsi
	testl	%edi, %edi
	jne	.LBB4_3
# BB#4:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	cltd
	idivl	(%rcx)
	movslq	%edx, %rax
	movq	8(%rcx,%rax,8), %r13
	testq	%r13, %r13
	je	.LBB4_11
# BB#5:                                 # %.preheader39.i.preheader
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB4_6:                                # %.preheader39.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_8 Depth 2
	movq	8(%rbx), %rbx
	cmpq	%r13, %rbx
	je	.LBB4_11
# BB#7:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB4_6 Depth=1
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB4_8:                                # %.preheader.i
                                        #   Parent Loop BB4_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	cmpb	$0, 32(%rbp)
	je	.LBB4_8
# BB#9:                                 #   in Loop: Header=BB4_6 Depth=1
	leaq	64(%rbp), %rsi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB4_6
# BB#10:                                # %ltab_retrieve.exit
	testq	%rbp, %rbp
	je	.LBB4_11
# BB#12:
	movl	40(%rbp), %eax
	andl	$528482304, %eax        # imm = 0x1F800000
	movl	$-1056964609, %ecx      # imm = 0xC0FFFFFF
	andl	12(%r14), %ecx
	leal	(%rcx,%rax,2), %eax
	movl	%eax, 12(%r14)
	jmp	.LBB4_13
.LBB4_14:
	movl	$43, %edi
	movl	$6, %esi
	movl	$.L.str.11, %edx
	movl	$2, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	movq	%r15, %r8
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	Error                   # TAILCALL
.LBB4_11:                               # %ltab_retrieve.exit.thread
	movq	%r12, (%rsp)
	movl	$43, %edi
	movl	$7, %esi
	movl	$.L.str.13, %edx
	movl	$2, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	movq	%r15, %r8
	callq	Error
.LBB4_13:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	LanguageChange, .Lfunc_end4-LanguageChange
	.cfi_endproc

	.globl	LanguageString
	.p2align	4, 0x90
	.type	LanguageString,@function
LanguageString:                         # @LanguageString
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 16
.Lcfi55:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	leal	-1(%rbx), %eax
	cmpl	lang_count(%rip), %eax
	jb	.LBB5_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.14, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB5_2:
	movq	canonical_tab(%rip), %rax
	movl	%ebx, %ecx
	movq	(%rax,%rcx,8), %rax
	addq	$64, %rax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	LanguageString, .Lfunc_end5-LanguageString
	.cfi_endproc

	.globl	LanguageHyph
	.p2align	4, 0x90
	.type	LanguageHyph,@function
LanguageHyph:                           # @LanguageHyph
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 16
.Lcfi57:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	leal	-1(%rbx), %eax
	cmpl	lang_count(%rip), %eax
	jb	.LBB6_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.15, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_2:
	movq	hyph_tab(%rip), %rax
	movl	%ebx, %ecx
	movq	(%rax,%rcx,8), %rax
	popq	%rbx
	retq
.Lfunc_end6:
	.size	LanguageHyph, .Lfunc_end6-LanguageHyph
	.cfi_endproc

	.type	names_tab,@object       # @names_tab
	.local	names_tab
	.comm	names_tab,8,8
	.type	lang_count,@object      # @lang_count
	.local	lang_count
	.comm	lang_count,4,4
	.type	lang_tabsize,@object    # @lang_tabsize
	.local	lang_tabsize
	.comm	lang_tabsize,4,4
	.type	hyph_tab,@object        # @hyph_tab
	.local	hyph_tab
	.comm	hyph_tab,8,8
	.type	canonical_tab,@object   # @canonical_tab
	.local	canonical_tab
	.comm	canonical_tab,8,8
	.type	LanguageSentenceEnds,@object # @LanguageSentenceEnds
	.comm	LanguageSentenceEnds,1024,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"assert failed in %s"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"LanguageDefine: names!"
	.size	.L.str.1, 23

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"LanguageDefine: names is empty!"
	.size	.L.str.2, 32

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"LanguageDefine: type(y) != WORD!"
	.size	.L.str.3, 33

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"hyphenation file name expected here"
	.size	.L.str.5, 36

	.type	lang_ends,@object       # @lang_ends
	.local	lang_ends
	.comm	lang_ends,512,16
	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"expected word ending pattern here"
	.size	.L.str.8, 34

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"empty word ending pattern"
	.size	.L.str.9, 26

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"LanguageWordEndsSentence: wd!"
	.size	.L.str.10, 30

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%s ignored (illegal left parameter)"
	.size	.L.str.11, 36

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"@Language"
	.size	.L.str.12, 10

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"%s ignored (unknown language %s)"
	.size	.L.str.13, 33

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"LanguageString: unknown number"
	.size	.L.str.14, 31

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"LanguageHyph: unknown number"
	.size	.L.str.15, 29

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"run out of memory enlarging language table"
	.size	.L.str.16, 43

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"language name %s used twice (first at%s)"
	.size	.L.str.17, 41


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
