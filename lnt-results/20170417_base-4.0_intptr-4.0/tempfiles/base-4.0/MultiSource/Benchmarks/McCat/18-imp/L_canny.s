	.text
	.file	"L_canny.bc"
	.globl	L_canny
	.p2align	4, 0x90
	.type	L_canny,@function
L_canny:                                # @L_canny
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, %rbp
	movq	%rcx, %r13
	movl	%edx, %r14d
	movl	%esi, %ebx
	movq	%rdi, %r15
	cvtss2sd	%xmm0, %xmm0
	leaq	36(%rsp), %rdi
	leaq	64(%rsp), %rsi
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movq	%rbp, %rdx
	callq	GaussianMask
	movl	$1, %r12d
	testl	%eax, %eax
	jne	.LBB0_21
# BB#1:
	leaq	32(%rsp), %rdi
	leaq	56(%rsp), %rsi
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%rbp, %rdx
	callq	DGaussianMask
	testl	%eax, %eax
	jne	.LBB0_21
# BB#2:
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	64(%rsp), %rsi
	movq	%r14, %rax
	movl	%ebx, %r9d
	movq	56(%rsp), %r13
	movl	36(%rsp), %ebx
	movl	32(%rsp), %r14d
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	leaq	56(%rsp), %r10
	movq	%r15, %rdi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%r13, %rdx
	movl	%ebx, %ecx
	movl	%r14d, %r8d
	movl	%r9d, 12(%rsp)          # 4-byte Spill
	pushq	%rbp
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	movq	%rax, 40(%rsp)          # 8-byte Spill
	pushq	%rax
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	dfilter
	addq	$32, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB0_21
# BB#3:
	subq	$8, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	leaq	48(%rsp), %rax
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	%r14d, %ecx
	movl	%ebx, %r8d
	movl	12(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %r9d
	pushq	%rbp
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	movq	40(%rsp), %r13          # 8-byte Reload
	pushq	%r13
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	callq	dfilter
	addq	$32, %rsp
.Lcfi22:
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB0_21
# BB#4:
	movl	%r13d, %r14d
	imull	%ebx, %r14d
	movslq	%r14d, %rdi
	movl	$4, %esi
	callq	calloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_22
# BB#5:                                 # %.preheader
	testl	%r14d, %r14d
	jle	.LBB0_6
# BB#7:                                 # %.lr.ph
	movq	40(%rsp), %rax
	movq	48(%rsp), %r12
	movl	%r14d, %r13d
	testb	$1, %r13b
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jne	.LBB0_9
# BB#8:
	xorl	%eax, %eax
	cmpl	$1, %r14d
	jne	.LBB0_13
	jmp	.LBB0_19
.LBB0_22:
	movabsq	$34184295084289312, %rax # imm = 0x79726F6D656D20
	movq	%rax, 6(%rbp)
	movabsq	$7863397576860792143, %rax # imm = 0x6D20666F2074754F
	movq	%rax, (%rbp)
	jmp	.LBB0_21
.LBB0_6:                                # %.preheader.._crit_edge_crit_edge
	movq	48(%rsp), %r12
	movq	40(%rsp), %rdx
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB0_20
.LBB0_9:
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	movss	(%r12), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB0_11
# BB#10:                                # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB0_11:                               # %.split
	movss	%xmm1, (%r15)
	movl	$1, %eax
	cmpl	$1, %r14d
	je	.LBB0_19
.LBB0_13:                               # %.lr.ph.new
	subq	%rax, %r13
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	4(%rcx,%rax,4), %rbp
	leaq	4(%r12,%rax,4), %rbx
	leaq	4(%r15,%rax,4), %r14
.LBB0_14:                               # =>This Inner Loop Header: Depth=1
	movss	-4(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	movss	-4(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB0_16
# BB#15:                                # %call.sqrt49
                                        #   in Loop: Header=BB0_14 Depth=1
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB0_16:                               # %.split48
                                        #   in Loop: Header=BB0_14 Depth=1
	movss	%xmm1, -4(%r14)
	movss	(%rbp), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB0_18
# BB#17:                                # %call.sqrt50
                                        #   in Loop: Header=BB0_14 Depth=1
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB0_18:                               # %.split48.split
                                        #   in Loop: Header=BB0_14 Depth=1
	movss	%xmm1, (%r14)
	addq	$8, %rbp
	addq	$8, %rbx
	addq	$8, %r14
	addq	$-2, %r13
	jne	.LBB0_14
.LBB0_19:
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r13          # 8-byte Reload
	movl	4(%rsp), %ebx           # 4-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
.LBB0_20:                               # %._crit_edge
	movq	%r15, %rdi
	movq	%r12, %rsi
	movl	%ebx, %ecx
	movl	%r13d, %r8d
	callq	dnon_max
	movq	%rax, (%rbp)
	xorl	%r12d, %r12d
.LBB0_21:
	movl	%r12d, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	L_canny, .Lfunc_end0-L_canny
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	1086324736              # float 6
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_1:
	.quad	4609047870845172685     # double 1.4142135623730951
.LCPI1_2:
	.quad	4602678819172646912     # double 0.5
.LCPI1_3:
	.quad	-4620693217682128896    # double -0.5
.LCPI1_4:
	.quad	4607182418800017408     # double 1
	.text
	.globl	GaussianMask
	.p2align	4, 0x90
	.type	GaussianMask,@function
GaussianMask:                           # @GaussianMask
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 80
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %r13
	cvtsd2ss	%xmm0, %xmm1
	movss	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	%xmm1, (%rsp)           # 4-byte Spill
	mulss	%xmm1, %xmm0
	cvttss2si	%xmm0, %eax
	cwtl
	movl	%eax, %ebp
	notl	%ebp
	andl	$1, %ebp
	addl	%eax, %ebp
	movl	%ebp, (%r13)
	movslq	%ebp, %rdi
	movl	$4, %esi
	callq	calloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB1_1
# BB#2:
	movl	%ebp, %eax
	shrl	$31, %eax
	addl	%ebp, %eax
	sarl	%eax
	movl	%eax, %r12d
	negl	%r12d
	movswl	%r12w, %ecx
	cmpl	%eax, %ecx
	jg	.LBB1_11
# BB#3:                                 # %.lr.ph
	movss	(%rsp), %xmm0           # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	mulsd	.LCPI1_1(%rip), %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	incl	%r12d
	movsd	.LCPI1_2(%rip), %xmm1   # xmm1 = mem[0],zero
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB1_4:                                # =>This Inner Loop Header: Depth=1
	leal	-1(%r12), %edx
	addl	%ecx, %eax
	je	.LBB1_5
# BB#6:                                 #   in Loop: Header=BB1_4 Depth=1
	decl	%ebp
	movswl	%dx, %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	cmpl	%ebp, %eax
	jne	.LBB1_8
# BB#7:                                 #   in Loop: Header=BB1_4 Depth=1
	addsd	.LCPI1_3(%rip), %xmm0
	divsd	(%rsp), %xmm0           # 8-byte Folded Reload
	callq	erf
	movsd	.LCPI1_2(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI1_4(%rip), %xmm2   # xmm2 = mem[0],zero
	jmp	.LBB1_9
	.p2align	4, 0x90
.LBB1_5:                                #   in Loop: Header=BB1_4 Depth=1
	movswl	%dx, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	addsd	%xmm1, %xmm0
	divsd	(%rsp), %xmm0           # 8-byte Folded Reload
	callq	erf
	movsd	.LCPI1_2(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	addsd	.LCPI1_4(%rip), %xmm2
	jmp	.LBB1_10
	.p2align	4, 0x90
.LBB1_8:                                #   in Loop: Header=BB1_4 Depth=1
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	addsd	%xmm1, %xmm0
	divsd	(%rsp), %xmm0           # 8-byte Folded Reload
	callq	erf
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	.LCPI1_3(%rip), %xmm0
	divsd	(%rsp), %xmm0           # 8-byte Folded Reload
	callq	erf
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	.LCPI1_2(%rip), %xmm1   # xmm1 = mem[0],zero
.LBB1_9:                                #   in Loop: Header=BB1_4 Depth=1
	subsd	%xmm0, %xmm2
.LBB1_10:                               #   in Loop: Header=BB1_4 Depth=1
	mulsd	%xmm1, %xmm2
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm2, %xmm0
	movss	%xmm0, (%rbx)
	addq	$4, %rbx
	movswl	%r12w, %ecx
	movl	(%r13), %ebp
	movl	%ebp, %eax
	shrl	$31, %eax
	addl	%ebp, %eax
	sarl	%eax
	leal	1(%r12), %r12d
	cmpl	%eax, %ecx
	jle	.LBB1_4
.LBB1_11:                               # %._crit_edge
	movq	%r15, (%r14)
	xorl	%eax, %eax
	jmp	.LBB1_12
.LBB1_1:
	movabsq	$34184295084289312, %rax # imm = 0x79726F6D656D20
	movq	%rax, 6(%r12)
	movabsq	$7863397576860792143, %rax # imm = 0x6D20666F2074754F
	movq	%rax, (%r12)
	movl	$1, %eax
.LBB1_12:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	GaussianMask, .Lfunc_end1-GaussianMask
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4622558669727284173     # double 11.313708498984761
.LCPI2_1:
	.quad	4612826843881809669     # double 2.5066282746310002
.LCPI2_2:
	.quad	4607182418800017408     # double 1
.LCPI2_4:
	.quad	4602678819172646912     # double 0.5
.LCPI2_6:
	.quad	-4620693217682128896    # double -0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_3:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI2_5:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	DGaussianMask
	.p2align	4, 0x90
	.type	DGaussianMask,@function
DGaussianMask:                          # @DGaussianMask
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 128
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %r13
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm1
	movsd	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cwtl
	movl	%eax, %ebp
	notl	%ebp
	andl	$1, %ebp
	addl	%eax, %ebp
	movl	%ebp, (%r13)
	movslq	%ebp, %rdi
	movl	$4, %esi
	callq	calloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB2_1
# BB#2:
	movl	%ebp, %eax
	shrl	$31, %eax
	addl	%ebp, %eax
	sarl	%eax
	movl	%eax, %r12d
	negl	%r12d
	movswl	%r12w, %ecx
	cmpl	%eax, %ecx
	jg	.LBB2_10
# BB#3:                                 # %.lr.ph
	movsd	.LCPI2_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movsd	.LCPI2_2(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	divsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm0, %xmm1
	movsd	%xmm1, 32(%rsp)         # 8-byte Spill
	movapd	%xmm2, %xmm1
	addsd	%xmm1, %xmm1
	mulsd	%xmm2, %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	xorps	.LCPI2_3(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	incl	%r12d
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB2_4:                                # =>This Inner Loop Header: Depth=1
	leal	-1(%r12), %edx
	addl	%ecx, %eax
	je	.LBB2_5
# BB#6:                                 #   in Loop: Header=BB2_4 Depth=1
	decl	%ebp
	movswl	%dx, %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	cmpl	%ebp, %eax
	jne	.LBB2_8
# BB#7:                                 #   in Loop: Header=BB2_4 Depth=1
	addsd	.LCPI2_6(%rip), %xmm0
	mulsd	%xmm0, %xmm0
	xorpd	.LCPI2_5(%rip), %xmm0
	divsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	callq	exp
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	jmp	.LBB2_9
	.p2align	4, 0x90
.LBB2_5:                                #   in Loop: Header=BB2_4 Depth=1
	movswl	%dx, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	addsd	.LCPI2_4(%rip), %xmm0
	mulsd	%xmm0, %xmm0
	xorpd	.LCPI2_5(%rip), %xmm0
	divsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	callq	exp
	mulsd	32(%rsp), %xmm0         # 8-byte Folded Reload
	jmp	.LBB2_9
	.p2align	4, 0x90
.LBB2_8:                                #   in Loop: Header=BB2_4 Depth=1
	movapd	%xmm0, 48(%rsp)         # 16-byte Spill
	addsd	.LCPI2_4(%rip), %xmm0
	mulsd	%xmm0, %xmm0
	movapd	.LCPI2_5(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm1, %xmm0
	divsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	callq	exp
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	movapd	48(%rsp), %xmm0         # 16-byte Reload
	addsd	.LCPI2_6(%rip), %xmm0
	mulsd	%xmm0, %xmm0
	xorpd	.LCPI2_5(%rip), %xmm0
	divsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	callq	exp
	movsd	40(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	divsd	24(%rsp), %xmm0         # 8-byte Folded Reload
.LBB2_9:                                #   in Loop: Header=BB2_4 Depth=1
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rbx)
	addq	$4, %rbx
	movswl	%r12w, %ecx
	movl	(%r13), %ebp
	movl	%ebp, %eax
	shrl	$31, %eax
	addl	%ebp, %eax
	sarl	%eax
	leal	1(%r12), %r12d
	cmpl	%eax, %ecx
	jle	.LBB2_4
.LBB2_10:                               # %._crit_edge
	movq	%r15, (%r14)
	xorl	%eax, %eax
	jmp	.LBB2_11
.LBB2_1:
	movabsq	$34184295084289312, %rax # imm = 0x79726F6D656D20
	movq	%rax, 6(%r12)
	movabsq	$7863397576860792143, %rax # imm = 0x6D20666F2074754F
	movq	%rax, (%r12)
	movl	$1, %eax
.LBB2_11:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	DGaussianMask, .Lfunc_end2-DGaussianMask
	.cfi_endproc

	.globl	dfilter
	.p2align	4, 0x90
	.type	dfilter,@function
dfilter:                                # @dfilter
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi55:
	.cfi_def_cfa_offset 128
.Lcfi56:
	.cfi_offset %rbx, -56
.Lcfi57:
	.cfi_offset %r12, -48
.Lcfi58:
	.cfi_offset %r13, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebx
	movl	%r8d, %r13d
	movl	%ecx, (%rsp)            # 4-byte Spill
	movq	%rdx, %rbp
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	128(%rsp), %r12d
	movl	%r12d, %eax
	imull	%ebx, %eax
	movslq	%eax, %rdi
	movl	$4, %esi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	callq	calloc
	testq	%rax, %rax
	je	.LBB3_34
# BB#1:                                 # %.preheader111
	movl	%r13d, 40(%rsp)         # 4-byte Spill
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movl	%r12d, %eax
	shll	$16, %eax
	movswl	%r12w, %r13d
	movl	%eax, 44(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	jle	.LBB3_5
# BB#2:                                 # %.preheader110.lr.ph
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %eax
	shll	$16, %eax
	testl	%eax, %eax
	jle	.LBB3_3
# BB#11:                                # %.preheader110.us.preheader
	movswl	%cx, %r11d
	movl	(%rsp), %eax            # 4-byte Reload
	movl	%eax, %r12d
	shrl	$31, %r12d
	addl	%eax, %r12d
	sarl	%r12d
	movl	%r12d, %eax
	negl	%eax
	cwtl
	movq	%rax, (%rsp)            # 8-byte Spill
	leal	1(%rax), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	%r13, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_12:                               # %.preheader110.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_14 Depth 2
                                        #       Child Loop BB3_15 Depth 3
                                        #     Child Loop BB3_21 Depth 2
	cmpl	(%rsp), %r12d           # 4-byte Folded Reload
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jge	.LBB3_13
# BB#20:                                # %.lr.ph135.split.us.us.preheader
                                        #   in Loop: Header=BB3_12 Depth=1
	movw	$1, %ax
	.p2align	4, 0x90
.LBB3_21:                               # %.lr.ph135.split.us.us
                                        #   Parent Loop BB3_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$4, %rdi
	movswl	%ax, %ecx
	leal	1(%rax), %eax
	cmpl	%r11d, %ecx
	jl	.LBB3_21
	jmp	.LBB3_19
	.p2align	4, 0x90
.LBB3_13:                               # %.lr.ph131.us146.preheader
                                        #   in Loop: Header=BB3_12 Depth=1
	movl	%eax, %esi
	imull	%r11d, %esi
	movl	%eax, %edx
	incl	%edx
	imull	%r11d, %edx
	decl	%edx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_14:                               # %.lr.ph131.us146
                                        #   Parent Loop BB3_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_15 Depth 3
	leal	(%rbx,%rsi), %eax
	movl	16(%rsp), %ecx          # 4-byte Reload
	movw	%cx, %r8w
	movq	(%rsp), %r10            # 8-byte Reload
                                        # kill: %R10D<def> %R10D<kill> %R10<kill> %R10<def>
	.p2align	4, 0x90
.LBB3_15:                               #   Parent Loop BB3_12 Depth=1
                                        #     Parent Loop BB3_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r10d, %r13d
	addl	%ebx, %r13d
	movl	%esi, %r9d
	js	.LBB3_17
# BB#16:                                #   in Loop: Header=BB3_15 Depth=3
	leal	(%rax,%r10), %r9d
	cmpl	%r11d, %r13d
	cmovgel	%edx, %r9d
.LBB3_17:                               #   in Loop: Header=BB3_15 Depth=3
	movslq	%r9d, %rbp
	movss	(%r15,%rbp,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movl	%r12d, %ecx
	subl	%r10d, %ecx
	movslq	%ecx, %rcx
	mulss	(%r14,%rcx,4), %xmm0
	addss	(%rdi), %xmm0
	movss	%xmm0, (%rdi)
	movswl	%r8w, %r10d
	leal	1(%r8), %r8d
	cmpl	%r10d, %r12d
	jge	.LBB3_15
# BB#18:                                # %._crit_edge132.us147
                                        #   in Loop: Header=BB3_14 Depth=2
	addq	$4, %rdi
	leal	1(%rbx), %eax
	movswl	%ax, %ebx
	cmpl	%r11d, %ebx
	jl	.LBB3_14
.LBB3_19:                               # %._crit_edge136.us
                                        #   in Loop: Header=BB3_12 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	leal	1(%rax), %eax
	cwtl
	movq	64(%rsp), %r13          # 8-byte Reload
	cmpl	%r13d, %eax
	jl	.LBB3_12
	jmp	.LBB3_5
.LBB3_3:                                # %.preheader110.preheader
	movw	$1, %ax
	.p2align	4, 0x90
.LBB3_4:                                # %.preheader110
                                        # =>This Inner Loop Header: Depth=1
	movswl	%ax, %ecx
	leal	1(%rax), %eax
	cmpl	%r13d, %ecx
	jl	.LBB3_4
.LBB3_5:                                # %._crit_edge139
	movl	$4, %esi
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	calloc
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB3_34
# BB#6:                                 # %.preheader109
	movl	12(%rsp), %edx          # 4-byte Reload
	movl	%edx, %eax
	shll	$16, %eax
	testl	%eax, %eax
	movq	56(%rsp), %r11          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	jle	.LBB3_10
# BB#7:                                 # %.preheader.lr.ph
	movswl	%dx, %r15d
	cmpl	$0, 44(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_8
# BB#22:                                # %.preheader.us.preheader
	movl	40(%rsp), %eax          # 4-byte Reload
	movl	%eax, %esi
	shrl	$31, %esi
	addl	%eax, %esi
	sarl	%esi
	movl	%esi, %eax
	negl	%eax
	leal	-1(%r13), %edi
	imull	%edx, %edi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	cwtl
	movq	%rax, (%rsp)            # 8-byte Spill
	leal	1(%rax), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_23:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_25 Depth 2
                                        #       Child Loop BB3_26 Depth 3
                                        #     Child Loop BB3_33 Depth 2
	cmpl	(%rsp), %esi            # 4-byte Folded Reload
	movswq	%r12w, %rax
	jge	.LBB3_24
# BB#32:                                # %.lr.ph115.split.us.us.preheader
                                        #   in Loop: Header=BB3_23 Depth=1
	movw	$1, %di
	.p2align	4, 0x90
.LBB3_33:                               # %.lr.ph115.split.us.us
                                        #   Parent Loop BB3_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	%di, %edx
	leal	1(%rdi), %edi
	cmpl	%r13d, %edx
	jl	.LBB3_33
	jmp	.LBB3_31
	.p2align	4, 0x90
.LBB3_24:                               # %.lr.ph.us123.preheader
                                        #   in Loop: Header=BB3_23 Depth=1
	movq	24(%rsp), %rdx          # 8-byte Reload
	leal	(%r12,%rdx), %edx
	movslq	%edx, %rbp
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_25:                               # %.lr.ph.us123
                                        #   Parent Loop BB3_23 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_26 Depth 3
	movl	%edx, %edi
	imull	%r15d, %edi
	movslq	%edi, %rdi
	movq	16(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rdi,4), %r14
	movss	(%r14,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movl	32(%rsp), %edi          # 4-byte Reload
	movw	%di, %r8w
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%edi, %r10d
	.p2align	4, 0x90
.LBB3_26:                               #   Parent Loop BB3_23 Depth=1
                                        #     Parent Loop BB3_25 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movaps	%xmm1, %xmm0
	movl	%r10d, %r9d
	addl	%edx, %r9d
	movq	%rax, %rdi
	js	.LBB3_29
# BB#27:                                #   in Loop: Header=BB3_26 Depth=3
	cmpl	%r13d, %r9d
	movq	%rbp, %rdi
	jge	.LBB3_29
# BB#28:                                #   in Loop: Header=BB3_26 Depth=3
	imull	%r15d, %r9d
	addl	%r12d, %r9d
	movslq	%r9d, %rdi
.LBB3_29:                               #   in Loop: Header=BB3_26 Depth=3
	movss	(%rcx,%rdi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movl	%esi, %edi
	subl	%r10d, %edi
	movslq	%edi, %rdi
	mulss	(%r11,%rdi,4), %xmm1
	addss	%xmm0, %xmm1
	movswl	%r8w, %r10d
	leal	1(%r8), %r8d
	cmpl	%r10d, %esi
	jge	.LBB3_26
# BB#30:                                # %._crit_edge.us125
                                        #   in Loop: Header=BB3_25 Depth=2
	movss	%xmm1, (%r14,%rax,4)
	leal	1(%rdx), %edx
	movswl	%dx, %edx
	cmpl	%r13d, %edx
	jl	.LBB3_25
.LBB3_31:                               # %._crit_edge116.us
                                        #   in Loop: Header=BB3_23 Depth=1
	incl	%eax
	movswl	%ax, %r12d
	cmpl	%r15d, %r12d
	jl	.LBB3_23
	jmp	.LBB3_10
.LBB3_34:
	movabsq	$34184295084289312, %rax # imm = 0x79726F6D656D20
	movq	144(%rsp), %rcx
	movq	%rax, 6(%rcx)
	movabsq	$7863397576860792143, %rax # imm = 0x6D20666F2074754F
	movq	%rax, (%rcx)
	movl	$1, %eax
	jmp	.LBB3_35
.LBB3_8:                                # %.preheader.preheader
	movw	$1, %ax
	.p2align	4, 0x90
.LBB3_9:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movswl	%ax, %ecx
	leal	1(%rax), %eax
	cmpl	%r15d, %ecx
	jl	.LBB3_9
.LBB3_10:                               # %._crit_edge118
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	136(%rsp), %rcx
	movq	%rax, (%rcx)
	xorl	%eax, %eax
.LBB3_35:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	dfilter, .Lfunc_end3-dfilter
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4487126258331716666     # double 1.0E-8
.LCPI4_1:
	.quad	-4736245778523059142    # double -1.0E-8
.LCPI4_2:
	.quad	4600877379321698714     # double 0.40000000000000002
.LCPI4_3:
	.quad	-4622494657533077094    # double -0.40000000000000002
.LCPI4_4:
	.quad	4607182418800017408     # double 1
	.text
	.globl	dnon_max
	.p2align	4, 0x90
	.type	dnon_max,@function
dnon_max:                               # @dnon_max
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi65:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi66:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi68:
	.cfi_def_cfa_offset 96
.Lcfi69:
	.cfi_offset %rbx, -56
.Lcfi70:
	.cfi_offset %r12, -48
.Lcfi71:
	.cfi_offset %r13, -40
.Lcfi72:
	.cfi_offset %r14, -32
.Lcfi73:
	.cfi_offset %r15, -24
.Lcfi74:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebp
	movl	%ecx, %r12d
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r13
	movl	%ebp, %eax
	imull	%r12d, %eax
	movslq	%eax, %rdi
	movl	$4, %esi
	callq	calloc
	movq	%rax, 8(%rsp)           # 8-byte Spill
	addl	$-2, %ebp
	cmpl	$2, %ebp
	jl	.LBB4_19
# BB#1:                                 # %.preheader.lr.ph
	movslq	%r12d, %r11
	addl	$-2, %r12d
	movl	%ebp, %r10d
	addq	$8, %r13
	addq	$4, 16(%rsp)            # 8-byte Folded Spill
	movq	8(%rsp), %r9            # 8-byte Reload
	addq	$4, %r9
	leaq	(,%r11,4), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	addq	$4, 24(%rsp)            # 8-byte Folded Spill
	leaq	-1(%r12), %rbp
	movl	$1, %edi
	movsd	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero
	xorps	%xmm1, %xmm1
	movsd	.LCPI4_2(%rip), %xmm9   # xmm9 = mem[0],zero
	movsd	.LCPI4_4(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	.LCPI4_3(%rip), %xmm8   # xmm8 = mem[0],zero
	movsd	.LCPI4_1(%rip), %xmm5   # xmm5 = mem[0],zero
	.p2align	4, 0x90
.LBB4_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_5 Depth 2
	cmpl	$1, %r12d
	jle	.LBB4_3
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	%rdi, %rax
	imulq	%r11, %rax
	leaq	1(%rdi), %r15
	movq	%r15, %r8
	imulq	%r11, %r8
	leaq	-1(%rdi), %rsi
	imulq	%r11, %rsi
	leaq	(%r13,%rax,4), %rdx
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rbx
	imulq	32(%rsp), %rdi          # 8-byte Folded Reload
	addq	%r9, %rdi
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rcx
	leaq	(%r13,%r8,4), %r8
	leaq	(%r13,%rsi,4), %rax
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_5:                                #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rbx,%r14,4), %xmm6    # xmm6 = mem[0],zero,zero,zero
	xorps	%xmm7, %xmm7
	cvtss2sd	%xmm6, %xmm7
	ucomisd	%xmm7, %xmm0
	jb	.LBB4_8
# BB#6:                                 #   in Loop: Header=BB4_5 Depth=2
	ucomisd	%xmm5, %xmm7
	jb	.LBB4_8
# BB#7:                                 #   in Loop: Header=BB4_5 Depth=2
	movss	(%rdx,%r14,4), %xmm7    # xmm7 = mem[0],zero,zero,zero
	movss	-8(%rdx,%r14,4), %xmm6  # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm7, %xmm6    # xmm6 = xmm6[0],xmm7[0],xmm6[1],xmm7[1]
	jmp	.LBB4_14
	.p2align	4, 0x90
.LBB4_8:                                #   in Loop: Header=BB4_5 Depth=2
	movss	(%rcx,%r14,4), %xmm7    # xmm7 = mem[0],zero,zero,zero
	divss	%xmm6, %xmm7
	xorps	%xmm6, %xmm6
	cvtss2sd	%xmm7, %xmm6
	ucomiss	%xmm1, %xmm7
	jb	.LBB4_11
# BB#9:                                 #   in Loop: Header=BB4_5 Depth=2
	ucomisd	%xmm6, %xmm9
	jbe	.LBB4_11
# BB#10:                                #   in Loop: Header=BB4_5 Depth=2
	movapd	%xmm4, %xmm3
	subsd	%xmm6, %xmm3
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	movss	-8(%rax,%r14,4), %xmm6  # xmm6 = mem[0],zero,zero,zero
	movss	(%r8,%r14,4), %xmm2     # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1]
	mulps	%xmm7, %xmm2
	cvtps2pd	%xmm2, %xmm2
	movss	-4(%rax,%r14,4), %xmm6  # xmm6 = mem[0],zero,zero,zero
	movss	-4(%r8,%r14,4), %xmm7   # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm7    # xmm7 = xmm7[0],xmm6[0],xmm7[1],xmm6[1]
	cvtps2pd	%xmm7, %xmm6
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	mulpd	%xmm6, %xmm3
	addpd	%xmm2, %xmm3
	cvtpd2ps	%xmm3, %xmm6
	jmp	.LBB4_14
	.p2align	4, 0x90
.LBB4_11:                               #   in Loop: Header=BB4_5 Depth=2
	ucomiss	%xmm7, %xmm1
	jb	.LBB4_20
# BB#12:                                #   in Loop: Header=BB4_5 Depth=2
	ucomisd	%xmm8, %xmm6
	jbe	.LBB4_20
# BB#13:                                #   in Loop: Header=BB4_5 Depth=2
	addsd	%xmm4, %xmm6
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	movss	(%rax,%r14,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movss	-8(%r8,%r14,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	mulps	%xmm7, %xmm3
	cvtps2pd	%xmm3, %xmm2
	movss	-4(%rax,%r14,4), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movss	-4(%r8,%r14,4), %xmm7   # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm7    # xmm7 = xmm7[0],xmm3[0],xmm7[1],xmm3[1]
	cvtps2pd	%xmm7, %xmm3
	movlhps	%xmm6, %xmm6            # xmm6 = xmm6[0,0]
	mulpd	%xmm3, %xmm6
	subpd	%xmm2, %xmm6
	cvtpd2ps	%xmm6, %xmm6
	.p2align	4, 0x90
.LBB4_14:                               #   in Loop: Header=BB4_5 Depth=2
	movss	-4(%rdx,%r14,4), %xmm2  # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm7, %xmm7
	cvtss2sd	%xmm2, %xmm7
	xorps	%xmm2, %xmm2
	cvtss2sd	%xmm6, %xmm2
	addsd	%xmm0, %xmm2
	ucomisd	%xmm2, %xmm7
	jbe	.LBB4_17
# BB#15:                                #   in Loop: Header=BB4_5 Depth=2
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	xorps	%xmm2, %xmm2
	cvtss2sd	%xmm6, %xmm2
	addsd	%xmm0, %xmm2
	ucomisd	%xmm2, %xmm7
	jbe	.LBB4_17
# BB#16:                                #   in Loop: Header=BB4_5 Depth=2
	movl	$1132396544, (%rdi,%r14,4) # imm = 0x437F0000
	jmp	.LBB4_17
.LBB4_20:                               #   in Loop: Header=BB4_5 Depth=2
	movl	$0, (%rdi,%r14,4)
	.p2align	4, 0x90
.LBB4_17:                               #   in Loop: Header=BB4_5 Depth=2
	incq	%r14
	cmpq	%r14, %rbp
	jne	.LBB4_5
	jmp	.LBB4_18
	.p2align	4, 0x90
.LBB4_3:                                # %.preheader.._crit_edge_crit_edge
                                        #   in Loop: Header=BB4_2 Depth=1
	incq	%rdi
	movq	%rdi, %r15
.LBB4_18:                               # %._crit_edge
                                        #   in Loop: Header=BB4_2 Depth=1
	cmpq	%r10, %r15
	movq	%r15, %rdi
	jne	.LBB4_2
.LBB4_19:                               # %._crit_edge112
	movq	8(%rsp), %rax           # 8-byte Reload
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	dnon_max, .Lfunc_end4-dnon_max
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Out of memory"
	.size	.L.str, 14


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
