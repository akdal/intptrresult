	.text
	.file	"kbo.bc"
	.globl	kbo_Compare
	.p2align	4, 0x90
	.type	kbo_Compare,@function
kbo_Compare:                            # @kbo_Compare
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 48
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	12(%rsp), %rsi
	leaq	8(%rsp), %rcx
	movq	%r14, %rdx
	callq	kbo_CompVarCondAndWeight
	movl	%eax, %ebp
	movl	12(%rsp), %edx
	testl	%edx, %edx
	movl	8(%rsp), %ecx
	je	.LBB0_3
# BB#1:
	testl	%ecx, %ecx
	jne	.LBB0_3
# BB#2:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	%ebp, %edx
	callq	kbo_CompareStruc
	jmp	.LBB0_9
.LBB0_3:
	testl	%edx, %edx
	jne	.LBB0_5
# BB#4:
	testl	%ecx, %ecx
	je	.LBB0_5
.LBB0_8:
	negl	%ebp
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movl	%ebp, %edx
	callq	kbo_CompareStruc
	movl	%eax, %edi
	callq	ord_Not
	jmp	.LBB0_9
.LBB0_5:
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.LBB0_9
# BB#6:
	testl	%ecx, %ecx
	je	.LBB0_9
# BB#7:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	%ebp, %edx
	callq	kbo_CompareStruc
	testl	%eax, %eax
	je	.LBB0_8
.LBB0_9:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	kbo_Compare, .Lfunc_end0-kbo_Compare
	.cfi_endproc

	.p2align	4, 0x90
	.type	kbo_CompVarCondAndWeight,@function
kbo_CompVarCondAndWeight:               # @kbo_CompVarCondAndWeight
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 64
.Lcfi14:
	.cfi_offset %rbx, -56
.Lcfi15:
	.cfi_offset %r12, -48
.Lcfi16:
	.cfi_offset %r13, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r13
	movq	%rdi, %rbp
	movl	$1, (%r14)
	movl	$1, (%r13)
	callq	term_MaxVar
	movl	%eax, %r12d
	movq	%r15, %rdi
	callq	term_MaxVar
	movl	stack_POINTER(%rip), %ebx
	cmpl	%eax, %r12d
	cmovll	%eax, %r12d
	testl	%r12d, %r12d
	js	.LBB1_2
# BB#1:                                 # %.lr.ph129.preheader
	movl	%r12d, %eax
	leaq	8(,%rax,8), %rdx
	movl	$ord_VARCOUNT, %edi
	xorl	%esi, %esi
	callq	memset
.LBB1_2:                                # %._crit_edge130
	movslq	(%rbp), %rax
	leal	-1(%rax), %ecx
	cmpl	$1999, %ecx             # imm = 0x7CF
	ja	.LBB1_4
# BB#3:
	incl	ord_VARCOUNT(,%rax,8)
	movl	$1, %eax
	jmp	.LBB1_14
.LBB1_4:
	negl	%eax
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	movl	12(%rax), %eax
	movq	16(%rbp), %rcx
	testq	%rcx, %rcx
	je	.LBB1_14
# BB#5:                                 # %.critedge.preheader
	leal	1(%rbx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rcx, stack_STACK(,%rbx,8)
	movl	symbol_TYPESTATBITS(%rip), %ecx
.LBB1_7:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_12 Depth 2
	leal	-1(%rdx), %edi
	movq	stack_STACK(,%rdi,8), %rsi
	movq	(%rsi), %rbp
	movq	8(%rsi), %rsi
	movq	%rbp, stack_STACK(,%rdi,8)
	movslq	(%rsi), %rdi
	leal	-1(%rdi), %ebp
	cmpl	$1999, %ebp             # imm = 0x7CF
	ja	.LBB1_9
# BB#8:                                 #   in Loop: Header=BB1_7 Depth=1
	incl	%eax
	incl	ord_VARCOUNT(,%rdi,8)
	cmpl	%ebx, %edx
	jne	.LBB1_12
	jmp	.LBB1_14
	.p2align	4, 0x90
.LBB1_9:                                #   in Loop: Header=BB1_7 Depth=1
	negl	%edi
	sarl	%cl, %edi
	movq	symbol_SIGNATURE(%rip), %rbp
	movslq	%edi, %rdi
	movq	(%rbp,%rdi,8), %rdi
	addl	12(%rdi), %eax
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB1_11
# BB#10:                                #   in Loop: Header=BB1_7 Depth=1
	movl	%edx, %edi
	leal	1(%rdx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rsi, stack_STACK(,%rdi,8)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
.LBB1_11:                               # %.preheader113
                                        #   in Loop: Header=BB1_7 Depth=1
	cmpl	%ebx, %edx
	je	.LBB1_14
	.p2align	4, 0x90
.LBB1_12:                               # %.lr.ph121
                                        #   Parent Loop BB1_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rdx), %esi
	cmpq	$0, stack_STACK(,%rsi,8)
	jne	.LBB1_6
# BB#13:                                #   in Loop: Header=BB1_12 Depth=2
	movl	%esi, stack_POINTER(%rip)
	cmpl	%esi, %ebx
	movl	%esi, %edx
	jne	.LBB1_12
	jmp	.LBB1_14
	.p2align	4, 0x90
.LBB1_6:                                # %.critedge.loopexit
                                        #   in Loop: Header=BB1_7 Depth=1
	cmpl	%edx, %ebx
	jne	.LBB1_7
.LBB1_14:                               # %.critedge._crit_edge
	movslq	(%r15), %rdx
	leal	-1(%rdx), %ecx
	cmpl	$1999, %ecx             # imm = 0x7CF
	ja	.LBB1_16
# BB#15:
	decl	%eax
	incl	ord_VARCOUNT+4(,%rdx,8)
	testl	%r12d, %r12d
	jns	.LBB1_22
	jmp	.LBB1_29
.LBB1_16:
	negl	%edx
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %edx
	movq	symbol_SIGNATURE(%rip), %rcx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rcx
	subl	12(%rcx), %eax
	movq	16(%r15), %rcx
	testq	%rcx, %rcx
	je	.LBB1_21
# BB#17:                                # %.critedge1.preheader
	leal	1(%rbx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rcx, stack_STACK(,%rbx,8)
	movl	symbol_TYPESTATBITS(%rip), %ecx
.LBB1_18:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_33 Depth 2
	leal	-1(%rdx), %edi
	movq	stack_STACK(,%rdi,8), %rsi
	movq	(%rsi), %rbp
	movq	8(%rsi), %rsi
	movq	%rbp, stack_STACK(,%rdi,8)
	movslq	(%rsi), %rdi
	leal	-1(%rdi), %ebp
	cmpl	$1999, %ebp             # imm = 0x7CF
	ja	.LBB1_30
# BB#19:                                #   in Loop: Header=BB1_18 Depth=1
	decl	%eax
	incl	ord_VARCOUNT+4(,%rdi,8)
	cmpl	%ebx, %edx
	jne	.LBB1_33
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_30:                               #   in Loop: Header=BB1_18 Depth=1
	negl	%edi
	sarl	%cl, %edi
	movq	symbol_SIGNATURE(%rip), %rbp
	movslq	%edi, %rdi
	movq	(%rbp,%rdi,8), %rdi
	subl	12(%rdi), %eax
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB1_32
# BB#31:                                #   in Loop: Header=BB1_18 Depth=1
	movl	%edx, %edi
	leal	1(%rdx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rsi, stack_STACK(,%rdi,8)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
.LBB1_32:                               # %.preheader112
                                        #   in Loop: Header=BB1_18 Depth=1
	cmpl	%ebx, %edx
	je	.LBB1_21
	.p2align	4, 0x90
.LBB1_33:                               # %.lr.ph117
                                        #   Parent Loop BB1_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rdx), %esi
	cmpq	$0, stack_STACK(,%rsi,8)
	jne	.LBB1_20
# BB#34:                                #   in Loop: Header=BB1_33 Depth=2
	movl	%esi, stack_POINTER(%rip)
	cmpl	%esi, %ebx
	movl	%esi, %edx
	jne	.LBB1_33
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_20:                               # %.critedge1.loopexit
                                        #   in Loop: Header=BB1_18 Depth=1
	cmpl	%edx, %ebx
	jne	.LBB1_18
.LBB1_21:                               # %.preheader
	testl	%r12d, %r12d
	js	.LBB1_29
.LBB1_22:                               # %.lr.ph.preheader
	movslq	%r12d, %rcx
	movq	$-1, %rdx
	.p2align	4, 0x90
.LBB1_23:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	ord_VARCOUNT+8(,%rdx,8), %esi
	movl	ord_VARCOUNT+12(,%rdx,8), %edi
	cmpl	%edi, %esi
	jae	.LBB1_26
# BB#24:                                #   in Loop: Header=BB1_23 Depth=1
	movl	$0, (%r13)
	cmpl	$0, (%r14)
	je	.LBB1_29
# BB#25:                                # %._crit_edge134
                                        #   in Loop: Header=BB1_23 Depth=1
	movl	ord_VARCOUNT+8(,%rdx,8), %esi
	movl	ord_VARCOUNT+12(,%rdx,8), %edi
.LBB1_26:                               #   in Loop: Header=BB1_23 Depth=1
	cmpl	%edi, %esi
	jbe	.LBB1_28
# BB#27:                                #   in Loop: Header=BB1_23 Depth=1
	movl	$0, (%r14)
	cmpl	$0, (%r13)
	je	.LBB1_29
.LBB1_28:                               #   in Loop: Header=BB1_23 Depth=1
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB1_23
.LBB1_29:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	kbo_CompVarCondAndWeight, .Lfunc_end1-kbo_CompVarCondAndWeight
	.cfi_endproc

	.p2align	4, 0x90
	.type	kbo_CompareStruc,@function
kbo_CompareStruc:                       # @kbo_CompareStruc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 64
.Lcfi26:
	.cfi_offset %rbx, -48
.Lcfi27:
	.cfi_offset %r12, -40
.Lcfi28:
	.cfi_offset %r13, -32
.Lcfi29:
	.cfi_offset %r14, -24
.Lcfi30:
	.cfi_offset %r15, -16
	movl	$3, %ebx
	testl	%edx, %edx
	jg	.LBB2_21
# BB#1:
	testl	%edx, %edx
	je	.LBB2_3
# BB#2:
	xorl	%ebx, %ebx
	jmp	.LBB2_21
.LBB2_3:
	movl	(%rdi), %r9d
	movl	(%rsi), %edx
	leal	-1(%r9), %eax
	leal	-1(%rdx), %ecx
	cmpl	$1999, %eax             # imm = 0x7CF
	ja	.LBB2_5
# BB#4:
	cmpl	$2000, %ecx             # imm = 0x7D0
	sbbl	%ebx, %ebx
	andl	$2, %ebx
	jmp	.LBB2_21
.LBB2_5:
	cmpl	$2000, %ecx             # imm = 0x7D0
	jb	.LBB2_21
# BB#6:
	movq	ord_PRECEDENCE(%rip), %r10
	movl	%r9d, %eax
	negl	%eax
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %eax
	movslq	%eax, %r8
	movl	(%r10,%r8,4), %r11d
	movl	%edx, %eax
	negl	%eax
	sarl	%cl, %eax
	cltq
	cmpl	(%r10,%rax,4), %r11d
	jl	.LBB2_21
# BB#7:
	cmpl	%edx, %r9d
	movl	$0, %ebx
	jne	.LBB2_21
# BB#8:
	movq	16(%rdi), %r15
	movq	16(%rsi), %r14
	movq	symbol_SIGNATURE(%rip), %rax
	movq	(%rax,%r8,8), %rax
	testb	$8, 20(%rax)
	jne	.LBB2_13
# BB#9:                                 # %.preheader
	movl	$2, %ebx
	testq	%r15, %r15
	jne	.LBB2_11
	jmp	.LBB2_21
.LBB2_12:                               #   in Loop: Header=BB2_11 Depth=1
	movq	(%r15), %r15
	movq	(%r14), %r14
	testq	%r15, %r15
	je	.LBB2_21
.LBB2_11:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r15), %rdi
	movq	8(%r14), %rsi
	callq	term_Equal
	testl	%eax, %eax
	jne	.LBB2_12
# BB#17:
	movq	8(%r15), %r15
	movq	8(%r14), %r14
	jmp	.LBB2_18
.LBB2_13:
	movl	16(%rax), %r12d
	movl	$2, %ebx
	testl	%r12d, %r12d
	jle	.LBB2_21
.LBB2_14:                               # %.lr.ph82
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movl	%r12d, %esi
	callq	list_NthElement
	movq	%rax, %r13
	movq	%r14, %rdi
	movl	%r12d, %esi
	callq	list_NthElement
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	term_Equal
	testl	%eax, %eax
	je	.LBB2_16
# BB#15:                                #   in Loop: Header=BB2_14 Depth=1
	cmpl	$1, %r12d
	leal	-1(%r12), %eax
	movl	%eax, %r12d
	jg	.LBB2_14
	jmp	.LBB2_21
.LBB2_16:                               # %.critedge63.thread
	movq	%r15, %rdi
	movl	%r12d, %esi
	callq	list_NthElement
	movq	%rax, %r15
	movq	%r14, %rdi
	movl	%r12d, %esi
	callq	list_NthElement
	movq	%rax, %r14
.LBB2_18:
	leaq	8(%rsp), %rsi
	leaq	12(%rsp), %rcx
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	kbo_CompVarCondAndWeight
	xorl	%ebx, %ebx
	testl	%eax, %eax
	js	.LBB2_21
# BB#19:
	movl	8(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB2_21
# BB#20:
	movq	%r15, %rdi
	movq	%r14, %rsi
	movl	%eax, %edx
	callq	kbo_CompareStruc
	movl	%eax, %ebx
.LBB2_21:
	movl	%ebx, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	kbo_CompareStruc, .Lfunc_end2-kbo_CompareStruc
	.cfi_endproc

	.globl	kbo_ContCompare
	.p2align	4, 0x90
	.type	kbo_ContCompare,@function
kbo_ContCompare:                        # @kbo_ContCompare
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 64
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbp
	movq	%rdx, %r12
	movq	%rsi, %r15
	movq	%rdi, %r13
	callq	cont_TermMaxVar
	movl	%eax, %ebx
	movq	%r12, %rdi
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%rbp, %rsi
	callq	cont_TermMaxVar
	cmpl	%eax, %ebx
	cmovll	%eax, %ebx
	testl	%ebx, %ebx
	js	.LBB3_2
# BB#1:                                 # %.lr.ph47.preheader.i
	movl	%ebx, %eax
	leaq	8(,%rax,8), %rdx
	movl	$ord_VARCOUNT, %edi
	xorl	%esi, %esi
	callq	memset
.LBB3_2:                                # %._crit_edge48.i
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	kbo_ContCompVarCondAndWeightIntern
	movl	%eax, %ebp
	movl	$1, %r14d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	kbo_ContCompVarCondAndWeightIntern
	subl	%eax, %ebp
	testl	%ebx, %ebx
	js	.LBB3_13
# BB#3:                                 # %.lr.ph.preheader.i
	movslq	%ebx, %rdx
	movl	$1, %eax
	movq	$-1, %rsi
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB3_4:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	ord_VARCOUNT+12(,%rsi,8), %edi
	cmpl	%edi, ord_VARCOUNT+8(,%rsi,8)
	jae	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_4 Depth=1
	xorl	%eax, %eax
	testl	%ecx, %ecx
	movl	$0, %r14d
	jne	.LBB3_9
	jmp	.LBB3_13
	.p2align	4, 0x90
.LBB3_6:                                #   in Loop: Header=BB3_4 Depth=1
	jbe	.LBB3_9
# BB#7:                                 #   in Loop: Header=BB3_4 Depth=1
	xorl	%ecx, %ecx
	testl	%eax, %eax
	movl	$0, %r14d
	je	.LBB3_13
.LBB3_9:                                #   in Loop: Header=BB3_4 Depth=1
	incq	%rsi
	cmpq	%rdx, %rsi
	jl	.LBB3_4
# BB#10:                                # %kbo_ContCompVarCondAndWeight.exit
	testl	%ecx, %ecx
	setne	%dl
	testl	%eax, %eax
	sete	%bl
	je	.LBB3_14
# BB#11:                                # %kbo_ContCompVarCondAndWeight.exit
	testl	%ecx, %ecx
	jne	.LBB3_14
# BB#12:
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%ebp, %r8d
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	kbo_ContCompareStruc    # TAILCALL
.LBB3_13:                               # %kbo_ContCompVarCondAndWeight.exit.thread
	testl	%r14d, %r14d
	sete	%bl
	setne	%dl
	movl	%r14d, %eax
.LBB3_14:
	testb	%dl, %dl
	je	.LBB3_16
# BB#15:
	testb	%bl, %bl
	je	.LBB3_16
.LBB3_19:
	negl	%ebp
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%r13, %rdx
	movq	%r15, %rcx
	movl	%ebp, %r8d
	callq	kbo_ContCompareStruc
	movl	%eax, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	ord_Not                 # TAILCALL
.LBB3_16:
	testl	%eax, %eax
	setne	%al
	andb	%al, %dl
	xorl	%eax, %eax
	cmpb	$1, %dl
	jne	.LBB3_18
# BB#17:
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%ebp, %r8d
	callq	kbo_ContCompareStruc
	testl	%eax, %eax
	je	.LBB3_19
.LBB3_18:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	kbo_ContCompare, .Lfunc_end3-kbo_ContCompare
	.cfi_endproc

	.p2align	4, 0x90
	.type	kbo_ContCompareStruc,@function
kbo_ContCompareStruc:                   # @kbo_ContCompareStruc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 64
.Lcfi51:
	.cfi_offset %rbx, -56
.Lcfi52:
	.cfi_offset %r12, -48
.Lcfi53:
	.cfi_offset %r13, -40
.Lcfi54:
	.cfi_offset %r14, -32
.Lcfi55:
	.cfi_offset %r15, -24
.Lcfi56:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r14
	movq	%rdi, %r13
	movl	symbol_TYPESTATBITS(%rip), %ecx
.LBB4_1:                                # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_3 Depth 2
                                        #     Child Loop BB4_8 Depth 2
                                        #     Child Loop BB4_23 Depth 2
                                        #     Child Loop BB4_33 Depth 2
                                        #     Child Loop BB4_38 Depth 2
                                        #     Child Loop BB4_20 Depth 2
                                        #     Child Loop BB4_28 Depth 2
                                        #     Child Loop BB4_45 Depth 2
                                        #     Child Loop BB4_53 Depth 2
	movl	(%rsi), %edi
	testl	%edi, %edi
	jle	.LBB4_6
# BB#2:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	cont_INSTANCECONTEXT(%rip), %rax
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph.i
                                        #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r13
	je	.LBB4_6
# BB#4:                                 #   in Loop: Header=BB4_3 Depth=2
	movslq	%edi, %rdi
	shlq	$5, %rdi
	movq	8(%r13,%rdi), %rdx
	testq	%rdx, %rdx
	je	.LBB4_6
# BB#5:                                 # %.thread.i
                                        #   in Loop: Header=BB4_3 Depth=2
	movq	16(%r13,%rdi), %r13
	movl	(%rdx), %edi
	testl	%edi, %edi
	movq	%rdx, %rsi
	jg	.LBB4_3
.LBB4_6:                                # %cont_Deref.exit
                                        #   in Loop: Header=BB4_1 Depth=1
	movl	(%r15), %eax
	testl	%eax, %eax
	jle	.LBB4_12
# BB#7:                                 # %.lr.ph.i77.preheader
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	cont_INSTANCECONTEXT(%rip), %rdi
	.p2align	4, 0x90
.LBB4_8:                                # %.lr.ph.i77
                                        #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rdi, %r14
	je	.LBB4_11
# BB#9:                                 #   in Loop: Header=BB4_8 Depth=2
	cltq
	shlq	$5, %rax
	movq	8(%r14,%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB4_11
# BB#10:                                # %.thread.i82
                                        #   in Loop: Header=BB4_8 Depth=2
	movq	16(%r14,%rax), %r14
	movl	(%rdx), %eax
	testl	%eax, %eax
	movq	%rdx, %r15
	jg	.LBB4_8
.LBB4_11:                               # %cont_Deref.exit84.loopexit
                                        #   in Loop: Header=BB4_1 Depth=1
	movl	(%r15), %eax
.LBB4_12:                               # %cont_Deref.exit84
                                        #   in Loop: Header=BB4_1 Depth=1
	movl	$3, %edi
	testl	%r8d, %r8d
	jg	.LBB4_60
# BB#13:                                #   in Loop: Header=BB4_1 Depth=1
	testl	%r8d, %r8d
	jne	.LBB4_61
# BB#14:                                #   in Loop: Header=BB4_1 Depth=1
	movl	(%rsi), %edx
	leal	-1(%rdx), %ebp
	leal	-1(%rax), %ebx
	cmpl	$1999, %ebp             # imm = 0x7CF
	jbe	.LBB4_62
# BB#15:                                #   in Loop: Header=BB4_1 Depth=1
	cmpl	$2000, %ebx             # imm = 0x7D0
	jb	.LBB4_60
# BB#16:                                #   in Loop: Header=BB4_1 Depth=1
	movq	ord_PRECEDENCE(%rip), %r9
	movl	%edx, %ebp
	negl	%ebp
	sarl	%cl, %ebp
	movslq	%ebp, %rbx
	movl	(%r9,%rbx,4), %r8d
	movl	%eax, %ebp
	negl	%ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %ebp
	movslq	%ebp, %rcx
	cmpl	(%r9,%rcx,4), %r8d
	jl	.LBB4_60
# BB#17:                                #   in Loop: Header=BB4_1 Depth=1
	cmpl	%eax, %edx
	jne	.LBB4_64
# BB#18:                                #   in Loop: Header=BB4_1 Depth=1
	movq	16(%rsi), %rbp
	movq	16(%r15), %r12
	movq	symbol_SIGNATURE(%rip), %rax
	movq	(%rax,%rbx,8), %rax
	testb	$8, 20(%rax)
	jne	.LBB4_22
# BB#19:                                # %.preheader
                                        #   in Loop: Header=BB4_1 Depth=1
	testq	%rbp, %rbp
	je	.LBB4_25
	.p2align	4, 0x90
.LBB4_20:                               #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rsi
	movq	8(%r12), %rcx
	movq	%r13, %rdi
	movq	%r14, %rdx
	callq	cont_TermEqual
	testl	%eax, %eax
	je	.LBB4_26
# BB#21:                                #   in Loop: Header=BB4_20 Depth=2
	movq	(%rbp), %rbp
	movq	(%r12), %r12
	testq	%rbp, %rbp
	jne	.LBB4_20
	jmp	.LBB4_25
	.p2align	4, 0x90
.LBB4_22:                               #   in Loop: Header=BB4_1 Depth=1
	movl	16(%rax), %ebx
	movl	$2, %edi
	testl	%ebx, %ebx
	jle	.LBB4_60
	.p2align	4, 0x90
.LBB4_23:                               #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, %rdi
	movl	%ebx, %esi
	callq	list_NthElement
	movq	%rax, %r15
	movq	%r12, %rdi
	movl	%ebx, %esi
	callq	list_NthElement
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	%rax, %rcx
	callq	cont_TermEqual
	testl	%eax, %eax
	je	.LBB4_31
# BB#24:                                #   in Loop: Header=BB4_23 Depth=2
	cmpl	$1, %ebx
	leal	-1(%rbx), %eax
	movl	%eax, %ebx
	jg	.LBB4_23
	jmp	.LBB4_25
	.p2align	4, 0x90
.LBB4_26:                               #   in Loop: Header=BB4_1 Depth=1
	movq	8(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jle	.LBB4_42
# BB#27:                                # %.lr.ph.i101.preheader
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	cont_INSTANCECONTEXT(%rip), %rcx
	.p2align	4, 0x90
.LBB4_28:                               # %.lr.ph.i101
                                        #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r13
	je	.LBB4_42
# BB#29:                                #   in Loop: Header=BB4_28 Depth=2
	movslq	%edx, %rdx
	shlq	$5, %rdx
	movq	8(%r13,%rdx), %rsi
	testq	%rsi, %rsi
	je	.LBB4_42
# BB#30:                                # %.thread.i106
                                        #   in Loop: Header=BB4_28 Depth=2
	movq	16(%r13,%rdx), %r13
	movl	(%rsi), %edx
	testl	%edx, %edx
	movq	%rsi, %rax
	jg	.LBB4_28
	jmp	.LBB4_43
	.p2align	4, 0x90
.LBB4_31:                               # %.critedge
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	%rbp, %rdi
	movl	%ebx, %esi
	callq	list_NthElement
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jle	.LBB4_36
# BB#32:                                # %.lr.ph.i123.preheader
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	cont_INSTANCECONTEXT(%rip), %rsi
	.p2align	4, 0x90
.LBB4_33:                               # %.lr.ph.i123
                                        #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rsi, %r13
	je	.LBB4_36
# BB#34:                                #   in Loop: Header=BB4_33 Depth=2
	movslq	%ecx, %rcx
	shlq	$5, %rcx
	movq	8(%r13,%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB4_36
# BB#35:                                # %.thread.i128
                                        #   in Loop: Header=BB4_33 Depth=2
	movq	16(%r13,%rcx), %r13
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	movq	%rdx, %rax
	jg	.LBB4_33
	.p2align	4, 0x90
.LBB4_36:                               # %cont_Deref.exit130
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	%rax, %rbp
	movq	%r12, %rdi
	movl	%ebx, %esi
	callq	list_NthElement
	movq	%rax, %r15
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jle	.LBB4_41
# BB#37:                                # %.lr.ph.i112.preheader
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	cont_INSTANCECONTEXT(%rip), %rax
	movq	%rbp, %rsi
	.p2align	4, 0x90
.LBB4_38:                               # %.lr.ph.i112
                                        #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r14
	je	.LBB4_49
# BB#39:                                #   in Loop: Header=BB4_38 Depth=2
	movslq	%ecx, %rcx
	shlq	$5, %rcx
	movq	8(%r14,%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB4_49
# BB#40:                                # %.thread.i117
                                        #   in Loop: Header=BB4_38 Depth=2
	movq	16(%r14,%rcx), %r14
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	movq	%rdx, %r15
	jg	.LBB4_38
	jmp	.LBB4_48
.LBB4_42:                               #   in Loop: Header=BB4_1 Depth=1
	movq	%rax, %rsi
.LBB4_43:                               # %cont_Deref.exit108
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	8(%r12), %r15
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jle	.LBB4_49
# BB#44:                                # %.lr.ph.i90.preheader
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	cont_INSTANCECONTEXT(%rip), %rax
	.p2align	4, 0x90
.LBB4_45:                               # %.lr.ph.i90
                                        #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r14
	je	.LBB4_49
# BB#46:                                #   in Loop: Header=BB4_45 Depth=2
	movslq	%ecx, %rcx
	shlq	$5, %rcx
	movq	8(%r14,%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB4_49
# BB#47:                                # %.thread.i95
                                        #   in Loop: Header=BB4_45 Depth=2
	movq	16(%r14,%rcx), %r14
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	movq	%rdx, %r15
	jg	.LBB4_45
.LBB4_48:                               #   in Loop: Header=BB4_1 Depth=1
	movq	%rdx, %r15
	jmp	.LBB4_49
.LBB4_41:                               #   in Loop: Header=BB4_1 Depth=1
	movq	%rbp, %rsi
	.p2align	4, 0x90
.LBB4_49:                               # %cont_Deref.exit97
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	%r13, %rdi
	movq	%rsi, %r12
	callq	cont_TermMaxVar
	movl	%eax, %ebx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	cont_TermMaxVar
	cmpl	%eax, %ebx
	cmovll	%eax, %ebx
	testl	%ebx, %ebx
	js	.LBB4_51
# BB#50:                                # %.lr.ph47.preheader.i
                                        #   in Loop: Header=BB4_1 Depth=1
	movl	%ebx, %eax
	leaq	8(,%rax,8), %rdx
	movl	$ord_VARCOUNT, %edi
	xorl	%esi, %esi
	callq	memset
.LBB4_51:                               # %._crit_edge48.i
                                        #   in Loop: Header=BB4_1 Depth=1
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	kbo_ContCompVarCondAndWeightIntern
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	$1, %ebp
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	kbo_ContCompVarCondAndWeightIntern
	movl	4(%rsp), %r8d           # 4-byte Reload
	movq	%r12, %rsi
	testl	%ebx, %ebx
	movl	symbol_TYPESTATBITS(%rip), %ecx
	js	.LBB4_58
# BB#52:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB4_1 Depth=1
	movslq	%ebx, %r9
	movl	$1, %ebp
	movq	$-1, %rdx
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB4_53:                               # %.lr.ph.i85
                                        #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	ord_VARCOUNT+12(,%rdx,8), %edi
	cmpl	%edi, ord_VARCOUNT+8(,%rdx,8)
	jae	.LBB4_55
# BB#54:                                #   in Loop: Header=BB4_53 Depth=2
	xorl	%ebp, %ebp
	testl	%ebx, %ebx
	movl	$0, %edi
	jne	.LBB4_57
	jmp	.LBB4_60
	.p2align	4, 0x90
.LBB4_55:                               #   in Loop: Header=BB4_53 Depth=2
	jbe	.LBB4_57
# BB#56:                                #   in Loop: Header=BB4_53 Depth=2
	xorl	%ebx, %ebx
	testl	%ebp, %ebp
	movl	$0, %edi
	je	.LBB4_60
.LBB4_57:                               #   in Loop: Header=BB4_53 Depth=2
	incq	%rdx
	cmpq	%r9, %rdx
	jl	.LBB4_53
.LBB4_58:                               # %kbo_ContCompVarCondAndWeight.exit
                                        #   in Loop: Header=BB4_1 Depth=1
	subl	%eax, %r8d
	js	.LBB4_64
# BB#59:                                # %kbo_ContCompVarCondAndWeight.exit
                                        #   in Loop: Header=BB4_1 Depth=1
	testl	%ebp, %ebp
	movl	$0, %edi
	jne	.LBB4_1
	jmp	.LBB4_60
.LBB4_25:
	movl	$2, %edi
.LBB4_60:                               # %.critedge65
	movl	%edi, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_64:
	xorl	%edi, %edi
	jmp	.LBB4_60
.LBB4_61:
	xorl	%edi, %edi
	jmp	.LBB4_60
.LBB4_62:
	cmpl	$2000, %ebx             # imm = 0x7D0
	sbbl	%edi, %edi
	andl	$2, %edi
	jmp	.LBB4_60
.Lfunc_end4:
	.size	kbo_ContCompareStruc, .Lfunc_end4-kbo_ContCompareStruc
	.cfi_endproc

	.globl	kbo_ContGreater
	.p2align	4, 0x90
	.type	kbo_ContGreater,@function
kbo_ContGreater:                        # @kbo_ContGreater
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi63:
	.cfi_def_cfa_offset 64
.Lcfi64:
	.cfi_offset %rbx, -56
.Lcfi65:
	.cfi_offset %r12, -48
.Lcfi66:
	.cfi_offset %r13, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r14
	movq	%rsi, %rbp
	movq	%rdi, %r13
	callq	cont_TermMaxVar
	movl	%eax, %ebx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	cont_TermMaxVar
	cmpl	%eax, %ebx
	cmovll	%eax, %ebx
	testl	%ebx, %ebx
	js	.LBB5_2
# BB#1:                                 # %.lr.ph47.preheader.i
	movl	%ebx, %eax
	leaq	8(,%rax,8), %rdx
	movl	$ord_VARCOUNT, %edi
	xorl	%esi, %esi
	callq	memset
.LBB5_2:                                # %._crit_edge48.i
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%rbp, %rsi
	callq	kbo_ContCompVarCondAndWeightIntern
	movl	%eax, %r12d
	movl	$1, %ebp
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	kbo_ContCompVarCondAndWeightIntern
	movl	%eax, %ecx
	testl	%ebx, %ebx
	js	.LBB5_10
# BB#3:                                 # %.lr.ph.preheader.i
	movslq	%ebx, %rdx
	movq	$-1, %rsi
	movl	$1, %edi
	.p2align	4, 0x90
.LBB5_4:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	ord_VARCOUNT+12(,%rsi,8), %eax
	cmpl	%eax, ord_VARCOUNT+8(,%rsi,8)
	jae	.LBB5_6
# BB#5:                                 #   in Loop: Header=BB5_4 Depth=1
	xorl	%ebp, %ebp
	testl	%edi, %edi
	movl	$0, %eax
	jne	.LBB5_8
	jmp	.LBB5_73
	.p2align	4, 0x90
.LBB5_6:                                #   in Loop: Header=BB5_4 Depth=1
	jbe	.LBB5_8
# BB#7:                                 #   in Loop: Header=BB5_4 Depth=1
	xorl	%edi, %edi
	testl	%ebp, %ebp
	movl	$0, %eax
	je	.LBB5_73
.LBB5_8:                                #   in Loop: Header=BB5_4 Depth=1
	incq	%rsi
	cmpq	%rdx, %rsi
	jl	.LBB5_4
# BB#9:                                 # %kbo_ContCompVarCondAndWeight.exit
	testl	%ebp, %ebp
	je	.LBB5_72
.LBB5_10:                               # %kbo_ContCompVarCondAndWeight.exit.thread20
	subl	%ecx, %r12d
	movl	$1, %eax
	testl	%r12d, %r12d
	jg	.LBB5_73
# BB#11:
	xorl	%eax, %eax
	testl	%r12d, %r12d
	jne	.LBB5_73
# BB#12:
	movl	symbol_TYPESTATBITS(%rip), %r12d
.LBB5_13:                               # %tailrecurse.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_15 Depth 2
                                        #     Child Loop BB5_20 Depth 2
                                        #     Child Loop BB5_33 Depth 2
                                        #     Child Loop BB5_42 Depth 2
                                        #     Child Loop BB5_47 Depth 2
                                        #     Child Loop BB5_30 Depth 2
                                        #     Child Loop BB5_37 Depth 2
                                        #     Child Loop BB5_56 Depth 2
                                        #     Child Loop BB5_64 Depth 2
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.LBB5_18
# BB#14:                                # %.lr.ph.i.preheader.i
                                        #   in Loop: Header=BB5_13 Depth=1
	movq	cont_INSTANCECONTEXT(%rip), %rax
	.p2align	4, 0x90
.LBB5_15:                               # %.lr.ph.i.i
                                        #   Parent Loop BB5_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r13
	je	.LBB5_18
# BB#16:                                #   in Loop: Header=BB5_15 Depth=2
	movslq	%ecx, %rcx
	shlq	$5, %rcx
	movq	8(%r13,%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB5_18
# BB#17:                                # %.thread.i.i
                                        #   in Loop: Header=BB5_15 Depth=2
	movq	16(%r13,%rcx), %r13
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	movq	%rdx, %rdi
	jg	.LBB5_15
.LBB5_18:                               # %cont_Deref.exit.i
                                        #   in Loop: Header=BB5_13 Depth=1
	movl	(%r15), %edx
	testl	%edx, %edx
	jle	.LBB5_24
# BB#19:                                # %.lr.ph.i77.preheader.i
                                        #   in Loop: Header=BB5_13 Depth=1
	movq	cont_INSTANCECONTEXT(%rip), %rax
	.p2align	4, 0x90
.LBB5_20:                               # %.lr.ph.i77.i
                                        #   Parent Loop BB5_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r14
	je	.LBB5_23
# BB#21:                                #   in Loop: Header=BB5_20 Depth=2
	movslq	%edx, %rdx
	shlq	$5, %rdx
	movq	8(%r14,%rdx), %rcx
	testq	%rcx, %rcx
	je	.LBB5_23
# BB#22:                                # %.thread.i82.i
                                        #   in Loop: Header=BB5_20 Depth=2
	movq	16(%r14,%rdx), %r14
	movl	(%rcx), %edx
	testl	%edx, %edx
	movq	%rcx, %r15
	jg	.LBB5_20
.LBB5_23:                               # %cont_Deref.exit84.loopexit.i
                                        #   in Loop: Header=BB5_13 Depth=1
	movl	(%r15), %edx
.LBB5_24:                               # %cont_Deref.exit84.i
                                        #   in Loop: Header=BB5_13 Depth=1
	movl	(%rdi), %esi
	leal	-1(%rsi), %eax
	cmpl	$2000, %eax             # imm = 0x7D0
	jb	.LBB5_72
# BB#25:                                #   in Loop: Header=BB5_13 Depth=1
	leal	-1(%rdx), %ecx
	movl	$1, %eax
	cmpl	$2000, %ecx             # imm = 0x7D0
	jb	.LBB5_73
# BB#26:                                #   in Loop: Header=BB5_13 Depth=1
	movq	%rdi, %rbp
	movq	ord_PRECEDENCE(%rip), %r9
	movl	%esi, %edi
	negl	%edi
	movl	%r12d, %ecx
	sarl	%cl, %edi
	movslq	%edi, %rdi
	movl	(%r9,%rdi,4), %r8d
	movl	%edx, %ebx
	negl	%ebx
	sarl	%cl, %ebx
	movslq	%ebx, %rcx
	cmpl	(%r9,%rcx,4), %r8d
	jl	.LBB5_73
# BB#27:                                #   in Loop: Header=BB5_13 Depth=1
	cmpl	%edx, %esi
	jne	.LBB5_72
# BB#28:                                #   in Loop: Header=BB5_13 Depth=1
	movq	16(%rbp), %rbp
	movq	16(%r15), %r15
	movq	symbol_SIGNATURE(%rip), %rax
	movq	(%rax,%rdi,8), %rax
	testb	$8, 20(%rax)
	jne	.LBB5_32
# BB#29:                                # %.preheader.i
                                        #   in Loop: Header=BB5_13 Depth=1
	testq	%rbp, %rbp
	je	.LBB5_72
	.p2align	4, 0x90
.LBB5_30:                               #   Parent Loop BB5_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rsi
	movq	8(%r15), %rcx
	movq	%r13, %rdi
	movq	%r14, %rdx
	callq	cont_TermEqual
	testl	%eax, %eax
	je	.LBB5_35
# BB#31:                                #   in Loop: Header=BB5_30 Depth=2
	movq	(%rbp), %rbp
	movq	(%r15), %r15
	testq	%rbp, %rbp
	jne	.LBB5_30
	jmp	.LBB5_72
.LBB5_32:                               #   in Loop: Header=BB5_13 Depth=1
	movl	16(%rax), %r12d
	testl	%r12d, %r12d
	jle	.LBB5_72
	.p2align	4, 0x90
.LBB5_33:                               #   Parent Loop BB5_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, %rdi
	movl	%r12d, %esi
	callq	list_NthElement
	movq	%rax, %rbx
	movq	%r15, %rdi
	movl	%r12d, %esi
	callq	list_NthElement
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	%rax, %rcx
	callq	cont_TermEqual
	testl	%eax, %eax
	je	.LBB5_40
# BB#34:                                #   in Loop: Header=BB5_33 Depth=2
	cmpl	$1, %r12d
	leal	-1(%r12), %eax
	movl	%eax, %r12d
	jg	.LBB5_33
	jmp	.LBB5_72
.LBB5_35:                               #   in Loop: Header=BB5_13 Depth=1
	movq	8(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jle	.LBB5_53
# BB#36:                                # %.lr.ph.i101.preheader.i
                                        #   in Loop: Header=BB5_13 Depth=1
	movq	cont_INSTANCECONTEXT(%rip), %rcx
.LBB5_37:                               # %.lr.ph.i101.i
                                        #   Parent Loop BB5_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r13
	je	.LBB5_53
# BB#38:                                #   in Loop: Header=BB5_37 Depth=2
	movslq	%edx, %rdx
	shlq	$5, %rdx
	movq	8(%r13,%rdx), %rbp
	testq	%rbp, %rbp
	je	.LBB5_53
# BB#39:                                # %.thread.i106.i
                                        #   in Loop: Header=BB5_37 Depth=2
	movq	16(%r13,%rdx), %r13
	movl	(%rbp), %edx
	testl	%edx, %edx
	movq	%rbp, %rax
	jg	.LBB5_37
	jmp	.LBB5_54
.LBB5_40:                               # %.critedge.i
                                        #   in Loop: Header=BB5_13 Depth=1
	movq	%rbp, %rdi
	movl	%r12d, %esi
	callq	list_NthElement
	movq	%rax, %rbp
	movl	(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.LBB5_45
# BB#41:                                # %.lr.ph.i123.preheader.i
                                        #   in Loop: Header=BB5_13 Depth=1
	movq	cont_INSTANCECONTEXT(%rip), %rax
.LBB5_42:                               # %.lr.ph.i123.i
                                        #   Parent Loop BB5_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r13
	je	.LBB5_45
# BB#43:                                #   in Loop: Header=BB5_42 Depth=2
	movslq	%ecx, %rcx
	shlq	$5, %rcx
	movq	8(%r13,%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB5_45
# BB#44:                                # %.thread.i128.i
                                        #   in Loop: Header=BB5_42 Depth=2
	movq	16(%r13,%rcx), %r13
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	movq	%rdx, %rbp
	jg	.LBB5_42
.LBB5_45:                               # %cont_Deref.exit130.i
                                        #   in Loop: Header=BB5_13 Depth=1
	movq	%r15, %rdi
	movl	%r12d, %esi
	callq	list_NthElement
	movq	%rax, %r15
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jle	.LBB5_51
# BB#46:                                # %.lr.ph.i112.preheader.i
                                        #   in Loop: Header=BB5_13 Depth=1
	movq	cont_INSTANCECONTEXT(%rip), %rax
	movl	symbol_TYPESTATBITS(%rip), %r12d
.LBB5_47:                               # %.lr.ph.i112.i
                                        #   Parent Loop BB5_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r14
	je	.LBB5_60
# BB#48:                                #   in Loop: Header=BB5_47 Depth=2
	movslq	%ecx, %rcx
	shlq	$5, %rcx
	movq	8(%r14,%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB5_60
# BB#49:                                # %.thread.i117.i
                                        #   in Loop: Header=BB5_47 Depth=2
	movq	16(%r14,%rcx), %r14
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	movq	%rdx, %r15
	jg	.LBB5_47
	jmp	.LBB5_59
.LBB5_53:                               #   in Loop: Header=BB5_13 Depth=1
	movq	%rax, %rbp
.LBB5_54:                               # %cont_Deref.exit108.i
                                        #   in Loop: Header=BB5_13 Depth=1
	movq	8(%r15), %r15
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jle	.LBB5_60
# BB#55:                                # %.lr.ph.i90.preheader.i
                                        #   in Loop: Header=BB5_13 Depth=1
	movq	cont_INSTANCECONTEXT(%rip), %rax
.LBB5_56:                               # %.lr.ph.i90.i
                                        #   Parent Loop BB5_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r14
	je	.LBB5_60
# BB#57:                                #   in Loop: Header=BB5_56 Depth=2
	movslq	%ecx, %rcx
	shlq	$5, %rcx
	movq	8(%r14,%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB5_60
# BB#58:                                # %.thread.i95.i
                                        #   in Loop: Header=BB5_56 Depth=2
	movq	16(%r14,%rcx), %r14
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	movq	%rdx, %r15
	jg	.LBB5_56
.LBB5_59:                               #   in Loop: Header=BB5_13 Depth=1
	movq	%rdx, %r15
	jmp	.LBB5_60
.LBB5_51:                               #   in Loop: Header=BB5_13 Depth=1
	movl	symbol_TYPESTATBITS(%rip), %r12d
.LBB5_60:                               # %cont_Deref.exit97.i
                                        #   in Loop: Header=BB5_13 Depth=1
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	cont_TermMaxVar
	movl	%eax, %ebx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	cont_TermMaxVar
	cmpl	%eax, %ebx
	cmovll	%eax, %ebx
	testl	%ebx, %ebx
	js	.LBB5_62
# BB#61:                                # %.lr.ph47.preheader.i.i
                                        #   in Loop: Header=BB5_13 Depth=1
	movl	%ebx, %eax
	leaq	8(,%rax,8), %rdx
	movl	$ord_VARCOUNT, %edi
	xorl	%esi, %esi
	callq	memset
.LBB5_62:                               # %._crit_edge48.i.i
                                        #   in Loop: Header=BB5_13 Depth=1
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%rbp, %rsi
	callq	kbo_ContCompVarCondAndWeightIntern
	movl	%eax, %ebp
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	kbo_ContCompVarCondAndWeightIntern
	movl	%eax, %ecx
	testl	%ebx, %ebx
	js	.LBB5_70
# BB#63:                                # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB5_13 Depth=1
	movslq	%ebx, %rdx
	movl	$1, %esi
	movq	$-1, %rdi
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB5_64:                               # %.lr.ph.i85.i
                                        #   Parent Loop BB5_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	ord_VARCOUNT+12(,%rdi,8), %eax
	cmpl	%eax, ord_VARCOUNT+8(,%rdi,8)
	jae	.LBB5_66
# BB#65:                                #   in Loop: Header=BB5_64 Depth=2
	xorl	%esi, %esi
	testl	%ebx, %ebx
	movl	$0, %eax
	jne	.LBB5_68
	jmp	.LBB5_73
	.p2align	4, 0x90
.LBB5_66:                               #   in Loop: Header=BB5_64 Depth=2
	jbe	.LBB5_68
# BB#67:                                #   in Loop: Header=BB5_64 Depth=2
	xorl	%ebx, %ebx
	testl	%esi, %esi
	movl	$0, %eax
	je	.LBB5_73
.LBB5_68:                               #   in Loop: Header=BB5_64 Depth=2
	incq	%rdi
	cmpq	%rdx, %rdi
	jl	.LBB5_64
# BB#69:                                # %kbo_ContCompVarCondAndWeight.exit.i
                                        #   in Loop: Header=BB5_13 Depth=1
	testl	%esi, %esi
	je	.LBB5_72
.LBB5_70:                               # %kbo_ContCompVarCondAndWeight.exit.thread149.i
                                        #   in Loop: Header=BB5_13 Depth=1
	subl	%ecx, %ebp
	testl	%ebp, %ebp
	movl	$1, %eax
	jg	.LBB5_73
# BB#71:                                #   in Loop: Header=BB5_13 Depth=1
	movl	$0, %eax
	je	.LBB5_13
	jmp	.LBB5_73
.LBB5_72:
	xorl	%eax, %eax
.LBB5_73:                               # %kbo_ContGreaterCompareStruc.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	kbo_ContGreater, .Lfunc_end5-kbo_ContGreater
	.cfi_endproc

	.p2align	4, 0x90
	.type	kbo_ContCompVarCondAndWeightIntern,@function
kbo_ContCompVarCondAndWeightIntern:     # @kbo_ContCompVarCondAndWeightIntern
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi74:
	.cfi_def_cfa_offset 48
.Lcfi75:
	.cfi_offset %rbx, -40
.Lcfi76:
	.cfi_offset %r14, -32
.Lcfi77:
	.cfi_offset %r15, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rdi, %r15
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.LBB6_6
# BB#1:                                 # %.lr.ph.i.preheader
	movq	cont_INSTANCECONTEXT(%rip), %rcx
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rcx, %r15
	je	.LBB6_5
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	cltq
	shlq	$5, %rax
	movq	8(%r15,%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB6_5
# BB#4:                                 # %.thread.i
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	16(%r15,%rax), %r15
	movl	(%rdx), %eax
	testl	%eax, %eax
	movq	%rdx, %rsi
	jg	.LBB6_2
.LBB6_5:                                # %cont_Deref.exit.loopexit
	movl	(%rsi), %eax
.LBB6_6:                                # %cont_Deref.exit
	leal	-1(%rax), %ecx
	cmpl	$1999, %ecx             # imm = 0x7CF
	ja	.LBB6_8
# BB#7:
	cltq
	movslq	%r14d, %rcx
	shlq	$2, %rcx
	incl	ord_VARCOUNT(%rcx,%rax,8)
	movl	$1, %ebp
	jmp	.LBB6_11
.LBB6_8:
	negl	%eax
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	movl	12(%rax), %ebp
	movq	16(%rsi), %rbx
	testq	%rbx, %rbx
	je	.LBB6_11
	.p2align	4, 0x90
.LBB6_9:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	movl	%r14d, %edx
	callq	kbo_ContCompVarCondAndWeightIntern
	addl	%eax, %ebp
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB6_9
.LBB6_11:                               # %.loopexit
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	kbo_ContCompVarCondAndWeightIntern, .Lfunc_end6-kbo_ContCompVarCondAndWeightIntern
	.cfi_endproc

	.type	kbo_MINWEIGHT,@object   # @kbo_MINWEIGHT
	.section	.rodata,"a",@progbits
	.globl	kbo_MINWEIGHT
	.p2align	2
kbo_MINWEIGHT:
	.long	1                       # 0x1
	.size	kbo_MINWEIGHT, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
