	.text
	.file	"LzFindMt.bc"
	.globl	MtSync_Construct
	.p2align	4, 0x90
	.type	MtSync_Construct,@function
MtSync_Construct:                       # @MtSync_Construct
	.cfi_startproc
# BB#0:
	movl	$0, (%rdi)
	movl	$0, 552(%rdi)
	movl	$0, 556(%rdi)
	movl	$0, 24(%rdi)
	movl	$0, 32(%rdi)
	movl	$0, 136(%rdi)
	movl	$0, 240(%rdi)
	movl	$0, 344(%rdi)
	movl	$0, 448(%rdi)
	retq
.Lfunc_end0:
	.size	MtSync_Construct, .Lfunc_end0-MtSync_Construct
	.cfi_endproc

	.globl	MtSync_GetNextBlock
	.p2align	4, 0x90
	.type	MtSync_GetNextBlock,@function
MtSync_GetNextBlock:                    # @MtSync_GetNextBlock
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	cmpl	$0, 4(%rbx)
	je	.LBB1_2
# BB#1:
	movl	$1, 600(%rbx)
	movl	$0, 12(%rbx)
	movq	$0, 4(%rbx)
	leaq	136(%rbx), %r14
	movq	%r14, %rdi
	callq	Event_Reset
	leaq	240(%rbx), %rdi
	callq	Event_Reset
	leaq	32(%rbx), %rdi
	callq	Event_Set
	movq	%r14, %rdi
	callq	Event_Wait
	leaq	560(%rbx), %r14
	jmp	.LBB1_3
.LBB1_2:
	leaq	560(%rbx), %r14
	movq	%r14, %rdi
	callq	pthread_mutex_unlock
	movl	$0, 556(%rbx)
	incl	600(%rbx)
	leaq	344(%rbx), %rdi
	movl	$1, %esi
	callq	Semaphore_ReleaseN
.LBB1_3:
	leaq	448(%rbx), %rdi
	callq	Semaphore_Wait
	movq	%r14, %rdi
	callq	pthread_mutex_lock
	movl	$1, 556(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	MtSync_GetNextBlock, .Lfunc_end1-MtSync_GetNextBlock
	.cfi_endproc

	.globl	MtSync_StopWriting
	.p2align	4, 0x90
	.type	MtSync_StopWriting,@function
MtSync_StopWriting:                     # @MtSync_StopWriting
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 48
.Lcfi10:
	.cfi_offset %rbx, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	cmpl	$0, 24(%rbx)
	je	.LBB2_8
# BB#1:
	cmpl	$0, 4(%rbx)
	jne	.LBB2_8
# BB#2:
	movl	600(%rbx), %ebp
	movl	$1, 12(%rbx)
	cmpl	$0, 556(%rbx)
	je	.LBB2_4
# BB#3:
	leaq	560(%rbx), %rdi
	callq	pthread_mutex_unlock
	movl	$0, 556(%rbx)
.LBB2_4:
	leaq	344(%rbx), %r14
	movl	$1, %esi
	movq	%r14, %rdi
	callq	Semaphore_ReleaseN
	leaq	240(%rbx), %rdi
	callq	Event_Wait
	cmpl	600(%rbx), %ebp
	je	.LBB2_7
# BB#5:                                 # %.lr.ph
	leaq	448(%rbx), %r15
	.p2align	4, 0x90
.LBB2_6:                                # =>This Inner Loop Header: Depth=1
	incl	%ebp
	movq	%r15, %rdi
	callq	Semaphore_Wait
	movl	$1, %esi
	movq	%r14, %rdi
	callq	Semaphore_ReleaseN
	cmpl	600(%rbx), %ebp
	jne	.LBB2_6
.LBB2_7:                                # %._crit_edge
	movl	$1, 4(%rbx)
.LBB2_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	MtSync_StopWriting, .Lfunc_end2-MtSync_StopWriting
	.cfi_endproc

	.globl	MtSync_Destruct
	.p2align	4, 0x90
	.type	MtSync_Destruct,@function
MtSync_Destruct:                        # @MtSync_Destruct
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -24
.Lcfi18:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	cmpl	$0, 24(%rbx)
	je	.LBB3_4
# BB#1:
	leaq	16(%rbx), %r14
	movq	%rbx, %rdi
	callq	MtSync_StopWriting
	movl	$1, 8(%rbx)
	cmpl	$0, 4(%rbx)
	je	.LBB3_3
# BB#2:
	leaq	32(%rbx), %rdi
	callq	Event_Set
.LBB3_3:
	movq	%r14, %rdi
	callq	Thread_Wait
	movq	%r14, %rdi
	callq	Thread_Close
.LBB3_4:
	cmpl	$0, 552(%rbx)
	je	.LBB3_6
# BB#5:
	leaq	560(%rbx), %rdi
	callq	pthread_mutex_destroy
	movl	$0, 552(%rbx)
.LBB3_6:
	leaq	32(%rbx), %rdi
	callq	Event_Close
	leaq	136(%rbx), %rdi
	callq	Event_Close
	leaq	240(%rbx), %rdi
	callq	Event_Close
	leaq	344(%rbx), %rdi
	callq	Semaphore_Close
	leaq	448(%rbx), %rdi
	callq	Semaphore_Close
	movl	$0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	MtSync_Destruct, .Lfunc_end3-MtSync_Destruct
	.cfi_endproc

	.globl	MtSync_Init
	.p2align	4, 0x90
	.type	MtSync_Init,@function
MtSync_Init:                            # @MtSync_Init
	.cfi_startproc
# BB#0:
	movl	$1, 4(%rdi)
	retq
.Lfunc_end4:
	.size	MtSync_Init, .Lfunc_end4-MtSync_Init
	.cfi_endproc

	.globl	HashThreadFunc
	.p2align	4, 0x90
	.type	HashThreadFunc,@function
HashThreadFunc:                         # @HashThreadFunc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 112
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	leaq	904(%r15), %r14
	movq	%r14, %rdi
	callq	Event_Wait
	leaq	1008(%r15), %rdi
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	callq	Event_Set
	cmpl	$0, 880(%r15)
	jne	.LBB5_11
# BB#1:                                 # %.lr.ph.lr.ph.lr.ph
	leaq	624(%r15), %r12
	leaq	1432(%r15), %r13
	leaq	1216(%r15), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	1320(%r15), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	1112(%r15), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
.LBB5_2:                                # %.lr.ph.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_3 Depth 2
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB5_3:                                #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, 884(%r15)
	jne	.LBB5_4
# BB#5:                                 #   in Loop: Header=BB5_3 Depth=2
	movq	1488(%r15), %rbp
	movq	%rbp, %rdi
	callq	MatchFinder_NeedMove
	testl	%eax, %eax
	je	.LBB5_6
# BB#12:                                #   in Loop: Header=BB5_3 Depth=2
	movq	%r12, %rdi
	callq	pthread_mutex_lock
	movq	%r13, %rdi
	callq	pthread_mutex_lock
	movq	%rbp, %rdi
	callq	MatchFinder_GetPointerToCurrentPos
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	MatchFinder_MoveBlock
	movq	%rbp, %rdi
	callq	MatchFinder_GetPointerToCurrentPos
	subq	%rax, %rbx
	subq	%rbx, (%r15)
	subq	%rbx, 848(%r15)
	movq	%r12, %rdi
	callq	pthread_mutex_unlock
	movq	%r13, %rdi
	callq	pthread_mutex_unlock
	cmpl	$0, 880(%r15)
	je	.LBB5_3
	jmp	.LBB5_11
.LBB5_6:                                #   in Loop: Header=BB5_3 Depth=2
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	Semaphore_Wait
	movq	%rbp, %rdi
	callq	MatchFinder_ReadIfRequired
	movl	8(%rbp), %r14d
	cmpl	$-8192, %r14d           # imm = 0xE000
	jb	.LBB5_8
# BB#7:                                 #   in Loop: Header=BB5_3 Depth=2
	subl	120(%rbp), %r14d
	decl	%r14d
	movq	%rbp, %rdi
	movl	%r14d, %esi
	callq	MatchFinder_ReduceOffsets
	movl	56(%rbp), %edx
	movl	124(%rbp), %esi
	shlq	$2, %rsi
	addq	40(%rbp), %rsi
	incl	%edx
	movl	%r14d, %edi
	callq	MatchFinder_Normalize3
	movl	8(%rbp), %r14d
.LBB5_8:                                #   in Loop: Header=BB5_3 Depth=2
	movq	800(%r15), %rax
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %edx
	andl	$7, %edx
	shll	$13, %edx
	movl	16(%rbp), %ecx
	subl	%r14d, %ecx
	movl	$2, (%rax,%rdx,4)
	movl	%ecx, 4(%rax,%rdx,4)
	movl	%ecx, %r14d
	subl	96(%rbp), %r14d
	jb	.LBB5_10
# BB#9:                                 #   in Loop: Header=BB5_3 Depth=2
	leaq	(%rax,%rdx,4), %rbx
	incl	%r14d
	cmpl	$8190, %r14d            # imm = 0x1FFE
	movl	$8190, %eax             # imm = 0x1FFE
	cmovael	%eax, %r14d
	movq	(%rbp), %rdi
	movl	8(%rbp), %esi
	movl	124(%rbp), %edx
	shlq	$2, %rdx
	addq	40(%rbp), %rdx
	movl	56(%rbp), %ecx
	leaq	8(%rbx), %r8
	leaq	140(%rbp), %rax
	movq	%rax, (%rsp)
	movl	%r14d, %r9d
	callq	*1480(%r15)
	addl	%r14d, (%rbx)
	movl	%r14d, %ecx
.LBB5_10:                               # %.outer
                                        #   in Loop: Header=BB5_3 Depth=2
	addl	%ecx, 8(%rbp)
	incl	12(%rsp)                # 4-byte Folded Spill
	movl	%ecx, %eax
	addq	%rax, (%rbp)
	movl	$1, %esi
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	Semaphore_ReleaseN
	cmpl	$0, 880(%r15)
	movq	16(%rsp), %r14          # 8-byte Reload
	je	.LBB5_3
	jmp	.LBB5_11
	.p2align	4, 0x90
.LBB5_4:                                # %.critedge
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, 1472(%r15)
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	Event_Set
	movq	%r14, %rdi
	callq	Event_Wait
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	Event_Set
	cmpl	$0, 880(%r15)
	je	.LBB5_2
.LBB5_11:                               # %.outer._crit_edge
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	HashThreadFunc, .Lfunc_end5-HashThreadFunc
	.cfi_endproc

	.globl	MatchFinderMt_GetNextBlock_Hash
	.p2align	4, 0x90
	.type	MatchFinderMt_GetNextBlock_Hash,@function
MatchFinderMt_GetNextBlock_Hash:        # @MatchFinderMt_GetNextBlock_Hash
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 16
.Lcfi33:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	872(%rbx), %rdi
	callq	MtSync_GetNextBlock
	movl	1472(%rbx), %eax
	shll	$13, %eax
	addl	$57344, %eax            # imm = 0xE000
	movzwl	%ax, %eax
	movl	%eax, 812(%rbx)
	movq	800(%rbx), %rcx
	leal	1(%rax), %edx
	movl	%edx, 808(%rbx)
	addl	(%rcx,%rax,4), %eax
	movl	%eax, 812(%rbx)
	leal	1(%rdx), %eax
	movl	%eax, 808(%rbx)
	movl	(%rcx,%rdx,4), %eax
	movl	%eax, 816(%rbx)
	popq	%rbx
	retq
.Lfunc_end6:
	.size	MatchFinderMt_GetNextBlock_Hash, .Lfunc_end6-MatchFinderMt_GetNextBlock_Hash
	.cfi_endproc

	.globl	BtGetMatches
	.p2align	4, 0x90
	.type	BtGetMatches,@function
BtGetMatches:                           # @BtGetMatches
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi38:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi40:
	.cfi_def_cfa_offset 96
.Lcfi41:
	.cfi_offset %rbx, -56
.Lcfi42:
	.cfi_offset %r12, -48
.Lcfi43:
	.cfi_offset %r13, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	816(%r12), %eax
	movl	832(%r12), %ecx
	addl	%ecx, %ecx
	movl	$16384, %edx            # imm = 0x4000
	subl	%ecx, %edx
	movl	%eax, 4(%r15)
	movl	$2, %ebx
	movl	%edx, (%rsp)            # 4-byte Spill
	cmpl	$3, %edx
	jb	.LBB7_14
# BB#1:                                 # %.outer.split.us.preheader.preheader
	leaq	872(%r12), %r14
	movl	$2, %ebx
	xorl	%r13d, %r13d
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB7_2:                                # %.outer.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_9 Depth 2
	movl	808(%r12), %edi
	movl	812(%r12), %ebp
	movl	%ebp, %eax
	subl	%edi, %eax
	jne	.LBB7_6
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	movq	%r14, %rdi
	callq	MtSync_GetNextBlock
	movl	1472(%r12), %eax
	shll	$13, %eax
	addl	$57344, %eax            # imm = 0xE000
	movzwl	%ax, %eax
	movl	%eax, 812(%r12)
	movq	800(%r12), %rcx
	leal	1(%rax), %edx
	movl	%edx, 808(%r12)
	addl	(%rcx,%rax,4), %eax
	movl	%eax, 812(%r12)
	leal	1(%rdx), %eax
	movl	%eax, 808(%r12)
	movl	(%rcx,%rdx,4), %eax
	movl	%eax, 816(%r12)
	addl	4(%rsp), %eax           # 4-byte Folded Reload
	movl	%eax, 4(%r15)
	movl	816(%r12), %eax
	cmpl	836(%r12), %eax
	jae	.LBB7_2
	jmp	.LBB7_4
.LBB7_6:                                # %.us-lcssa.us
                                        #   in Loop: Header=BB7_2 Depth=1
	movl	816(%r12), %r11d
	movl	832(%r12), %r8d
	movl	840(%r12), %edx
	movl	856(%r12), %r9d
	cmpl	%r11d, %r8d
	movl	%r11d, %esi
	cmovbl	%r8d, %esi
	movl	%r11d, %ecx
	movl	%esi, 16(%rsp)          # 4-byte Spill
	subl	%esi, %ecx
	incl	%ecx
	cmpl	%eax, %ecx
	cmovael	%eax, %ecx
	movl	860(%r12), %r10d
	movl	%r10d, %eax
	subl	%r9d, %eax
	cmpl	%ecx, %eax
	cmovael	%ecx, %eax
	testl	%eax, %eax
	je	.LBB7_7
# BB#8:                                 # %.lr.ph114.preheader
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	848(%r12), %rcx
	leal	-1(%rdi), %eax
	subl	%ebp, %eax
	leal	-1(%r9), %ebp
	subl	%r10d, %ebp
	cmpl	%ebp, %eax
	cmoval	%eax, %ebp
	movl	$-3, %eax
	subl	%r11d, %eax
	notl	%r8d
	notl	%r11d
	cmpl	%r11d, %r8d
	cmoval	%r8d, %r11d
	subl	%r11d, %eax
	cmpl	%eax, %ebp
	cmoval	%ebp, %eax
	addl	$2, %eax
	jmp	.LBB7_9
	.p2align	4, 0x90
.LBB7_11:                               # %.lr.ph114..lr.ph114_crit_edge
                                        #   in Loop: Header=BB7_9 Depth=2
	movq	%r13, %r9
	incl	%r9d
	movl	12(%rsp), %edx          # 4-byte Reload
	incl	%edx
	movl	808(%r12), %edi
	movl	860(%r12), %r10d
	incl	%eax
.LBB7_9:                                # %.lr.ph114
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	%ebx, %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	%eax, %r14d
	leaq	(%r15,%r14,4), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	800(%r12), %rax
	leal	1(%rdi), %esi
	movl	%esi, 808(%r12)
	movl	%edi, %edi
	movl	%edx, %esi
	subl	(%rax,%rdi,4), %esi
	movq	824(%r12), %r8
	movl	864(%r12), %ebx
	leaq	4(%r15,%r14,4), %rax
	movl	836(%r12), %ebp
	decl	%ebp
	movl	16(%rsp), %edi          # 4-byte Reload
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movq	%r9, %r13
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	%rbp
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	callq	GetMatchesSpec1
	addq	$32, %rsp
.Lcfi51:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	subq	32(%rsp), %rbx          # 8-byte Folded Reload
	shrq	$2, %rbx
	leal	-1(%rbx), %eax
	movl	%eax, (%r15,%r14,4)
	addl	20(%rsp), %ebx          # 4-byte Folded Reload
	movq	848(%r12), %rcx
	incq	%rcx
	movq	%rcx, 848(%r12)
	movl	8(%rsp), %eax           # 4-byte Reload
	testl	%eax, %eax
	je	.LBB7_12
# BB#10:                                # %.lr.ph114
                                        #   in Loop: Header=BB7_9 Depth=2
	cmpl	(%rsp), %ebx            # 4-byte Folded Reload
	jb	.LBB7_11
.LBB7_12:                               # %.critedge.loopexit
                                        #   in Loop: Header=BB7_2 Depth=1
	movl	816(%r12), %r11d
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	840(%r12), %eax
	movl	860(%r12), %r10d
	movq	%r13, %r9
	incl	%r9d
	incl	%ecx
	movl	%eax, %edx
	movq	24(%rsp), %r14          # 8-byte Reload
	xorl	%r13d, %r13d
	jmp	.LBB7_13
.LBB7_7:                                #   in Loop: Header=BB7_2 Depth=1
	movl	%edx, %ecx
.LBB7_13:                               # %.critedge
                                        #   in Loop: Header=BB7_2 Depth=1
	movl	%ecx, %eax
	subl	%edx, %eax
	addl	%eax, 4(%rsp)           # 4-byte Folded Spill
	subl	%eax, %r11d
	movl	%r11d, 816(%r12)
	movl	%ecx, 840(%r12)
	cmpl	%r10d, %r9d
	cmovel	%r13d, %r9d
	movl	%r9d, 856(%r12)
	cmpl	(%rsp), %ebx            # 4-byte Folded Reload
	jb	.LBB7_2
	jmp	.LBB7_14
.LBB7_4:                                # %.preheader
	testl	%eax, %eax
	je	.LBB7_14
	.p2align	4, 0x90
.LBB7_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %eax
	incl	%ebx
	movl	$0, (%r15,%rax,4)
	decl	816(%r12)
	jne	.LBB7_5
.LBB7_14:                               # %.loopexit
	movl	%ebx, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	BtGetMatches, .Lfunc_end7-BtGetMatches
	.cfi_endproc

	.globl	BtFillBlock
	.p2align	4, 0x90
	.type	BtFillBlock,@function
BtFillBlock:                            # @BtFillBlock
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi54:
	.cfi_def_cfa_offset 32
.Lcfi55:
	.cfi_offset %rbx, -24
.Lcfi56:
	.cfi_offset %r14, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	cmpl	$0, 876(%r14)
	jne	.LBB8_2
# BB#1:
	leaq	1432(%r14), %rdi
	callq	pthread_mutex_lock
	movl	$1, 1428(%r14)
.LBB8_2:
	movq	8(%r14), %rax
	andl	$63, %ebx
	shll	$14, %ebx
	leaq	(%rax,%rbx,4), %rsi
	movq	%r14, %rdi
	callq	BtGetMatches
	movl	840(%r14), %ebx
	cmpl	$-16384, %ebx           # imm = 0xC000
	jb	.LBB8_4
# BB#3:
	movl	860(%r14), %edx
	subl	%edx, %ebx
	movq	824(%r14), %rsi
	addl	%edx, %edx
	movl	%ebx, %edi
	callq	MatchFinder_Normalize3
	subl	%ebx, 840(%r14)
.LBB8_4:
	cmpl	$0, 876(%r14)
	jne	.LBB8_6
# BB#5:
	leaq	1432(%r14), %rdi
	callq	pthread_mutex_unlock
	movl	$0, 1428(%r14)
.LBB8_6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	BtFillBlock, .Lfunc_end8-BtFillBlock
	.cfi_endproc

	.globl	BtThreadFunc
	.p2align	4, 0x90
	.type	BtThreadFunc,@function
BtThreadFunc:                           # @BtThreadFunc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi63:
	.cfi_def_cfa_offset 96
.Lcfi64:
	.cfi_offset %rbx, -56
.Lcfi65:
	.cfi_offset %r12, -48
.Lcfi66:
	.cfi_offset %r13, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	96(%rbx), %rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	callq	Event_Wait
	leaq	200(%rbx), %rdi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	callq	Event_Set
	cmpl	$0, 72(%rbx)
	jne	.LBB9_12
# BB#1:                                 # %.lr.ph.lr.ph
	leaq	408(%rbx), %r12
	leaq	1432(%rbx), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	872(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	512(%rbx), %rbp
	leaq	304(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB9_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_3 Depth 2
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB9_3:                                #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, 76(%rbx)
	jne	.LBB9_4
# BB#5:                                 #   in Loop: Header=BB9_3 Depth=2
	movq	%r12, %rdi
	callq	Semaphore_Wait
	cmpl	$0, 876(%rbx)
	jne	.LBB9_7
# BB#6:                                 #   in Loop: Header=BB9_3 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	pthread_mutex_lock
	movl	$1, 1428(%rbx)
.LBB9_7:                                #   in Loop: Header=BB9_3 Depth=2
	movq	8(%rbx), %rax
	movl	%r15d, %ecx
	andl	$1032192, %ecx          # imm = 0xFC000
	leaq	(%rax,%rcx,4), %rsi
	movq	%rbx, %rdi
	callq	BtGetMatches
	movl	840(%rbx), %r14d
	cmpl	$-16384, %r14d          # imm = 0xC000
	jb	.LBB9_9
# BB#8:                                 #   in Loop: Header=BB9_3 Depth=2
	movl	860(%rbx), %edx
	subl	%edx, %r14d
	movq	824(%rbx), %rsi
	addl	%edx, %edx
	movl	%r14d, %edi
	callq	MatchFinder_Normalize3
	subl	%r14d, 840(%rbx)
.LBB9_9:                                #   in Loop: Header=BB9_3 Depth=2
	cmpl	$0, 876(%rbx)
	jne	.LBB9_11
# BB#10:                                #   in Loop: Header=BB9_3 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	pthread_mutex_unlock
	movl	$0, 1428(%rbx)
.LBB9_11:                               # %BtFillBlock.exit
                                        #   in Loop: Header=BB9_3 Depth=2
	incl	%r13d
	movl	$1, %esi
	movq	%rbp, %rdi
	callq	Semaphore_ReleaseN
	addl	$16384, %r15d           # imm = 0x4000
	cmpl	$0, 72(%rbx)
	je	.LBB9_3
	jmp	.LBB9_12
	.p2align	4, 0x90
.LBB9_4:                                # %.critedge
                                        #   in Loop: Header=BB9_2 Depth=1
	movl	%r13d, 664(%rbx)
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	MtSync_StopWriting
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	Event_Set
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	Event_Wait
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	Event_Set
	cmpl	$0, 72(%rbx)
	je	.LBB9_2
.LBB9_12:                               # %._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	BtThreadFunc, .Lfunc_end9-BtThreadFunc
	.cfi_endproc

	.globl	MatchFinderMt_Construct
	.p2align	4, 0x90
	.type	MatchFinderMt_Construct,@function
MatchFinderMt_Construct:                # @MatchFinderMt_Construct
	.cfi_startproc
# BB#0:
	movq	$0, 800(%rdi)
	movl	$0, 872(%rdi)
	movl	$0, 1424(%rdi)
	movl	$0, 1428(%rdi)
	movl	$0, 896(%rdi)
	movl	$0, 904(%rdi)
	movl	$0, 1008(%rdi)
	movl	$0, 1112(%rdi)
	movl	$0, 1216(%rdi)
	movl	$0, 1320(%rdi)
	movl	$0, 64(%rdi)
	movl	$0, 616(%rdi)
	movl	$0, 620(%rdi)
	movl	$0, 88(%rdi)
	movl	$0, 96(%rdi)
	movl	$0, 200(%rdi)
	movl	$0, 304(%rdi)
	movl	$0, 408(%rdi)
	movl	$0, 512(%rdi)
	retq
.Lfunc_end10:
	.size	MatchFinderMt_Construct, .Lfunc_end10-MatchFinderMt_Construct
	.cfi_endproc

	.globl	MatchFinderMt_FreeMem
	.p2align	4, 0x90
	.type	MatchFinderMt_FreeMem,@function
MatchFinderMt_FreeMem:                  # @MatchFinderMt_FreeMem
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 16
.Lcfi71:
	.cfi_offset %rbx, -16
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movq	800(%rbx), %rsi
	movq	%rax, %rdi
	callq	*8(%rax)
	movq	$0, 800(%rbx)
	popq	%rbx
	retq
.Lfunc_end11:
	.size	MatchFinderMt_FreeMem, .Lfunc_end11-MatchFinderMt_FreeMem
	.cfi_endproc

	.globl	MatchFinderMt_Destruct
	.p2align	4, 0x90
	.type	MatchFinderMt_Destruct,@function
MatchFinderMt_Destruct:                 # @MatchFinderMt_Destruct
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi74:
	.cfi_def_cfa_offset 32
.Lcfi75:
	.cfi_offset %rbx, -24
.Lcfi76:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	872(%rbx), %rdi
	callq	MtSync_Destruct
	leaq	64(%rbx), %rdi
	callq	MtSync_Destruct
	movq	800(%rbx), %rsi
	movq	%r14, %rdi
	callq	*8(%r14)
	movq	$0, 800(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end12:
	.size	MatchFinderMt_Destruct, .Lfunc_end12-MatchFinderMt_Destruct
	.cfi_endproc

	.globl	MatchFinderMt_Create
	.p2align	4, 0x90
	.type	MatchFinderMt_Create,@function
MatchFinderMt_Create:                   # @MatchFinderMt_Create
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi80:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi81:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi83:
	.cfi_def_cfa_offset 64
.Lcfi84:
	.cfi_offset %rbx, -56
.Lcfi85:
	.cfi_offset %r12, -48
.Lcfi86:
	.cfi_offset %r13, -40
.Lcfi87:
	.cfi_offset %r14, -32
.Lcfi88:
	.cfi_offset %r15, -24
.Lcfi89:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movl	%r8d, %r15d
	movl	%ecx, %r13d
	movl	%edx, %r12d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	1488(%rbx), %rdi
	movl	%ebp, 44(%rbx)
	leal	(,%r13,4), %ecx
	movl	$5, %eax
	cmpl	$16383, %ecx            # imm = 0x3FFF
	ja	.LBB13_9
# BB#1:
	cmpq	$0, 800(%rbx)
	jne	.LBB13_4
# BB#2:
	movq	%rdi, (%rsp)            # 8-byte Spill
	movl	$4456448, %esi          # imm = 0x440000
	movq	%r14, %rdi
	callq	*(%r14)
	movq	%rax, 800(%rbx)
	testq	%rax, %rax
	je	.LBB13_8
# BB#3:
	addq	$262144, %rax           # imm = 0x40000
	movq	%rax, 8(%rbx)
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB13_4:
	addl	$1114112, %r12d         # imm = 0x110000
	addl	$8192, %r15d            # imm = 0x2000
	movl	%ebp, %esi
	movl	%r12d, %edx
	movl	%r13d, %ecx
	movl	%r15d, %r8d
	movq	%r14, %r9
	callq	MatchFinder_Create
	testl	%eax, %eax
	je	.LBB13_8
# BB#5:
	leaq	872(%rbx), %rdi
	movl	$HashThreadFunc2, %esi
	movl	$8, %ecx
	movq	%rbx, %rdx
	callq	MtSync_Create
	testl	%eax, %eax
	jne	.LBB13_9
# BB#6:
	leaq	64(%rbx), %rdi
	movl	$BtThreadFunc2, %esi
	movl	$64, %ecx
	movq	%rbx, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	MtSync_Create           # TAILCALL
.LBB13_8:
	movl	$2, %eax
.LBB13_9:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	MatchFinderMt_Create, .Lfunc_end13-MatchFinderMt_Create
	.cfi_endproc

	.p2align	4, 0x90
	.type	MtSync_Create,@function
MtSync_Create:                          # @MtSync_Create
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi92:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi94:
	.cfi_def_cfa_offset 48
.Lcfi95:
	.cfi_offset %rbx, -40
.Lcfi96:
	.cfi_offset %r14, -32
.Lcfi97:
	.cfi_offset %r15, -24
.Lcfi98:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	xorl	%eax, %eax
	cmpl	$0, (%rbx)
	jne	.LBB14_10
# BB#1:
	leaq	560(%rbx), %rdi
	callq	CriticalSection_Init
	testl	%eax, %eax
	jne	.LBB14_9
# BB#2:
	movl	$1, 552(%rbx)
	leaq	32(%rbx), %rdi
	callq	AutoResetEvent_CreateNotSignaled
	testl	%eax, %eax
	jne	.LBB14_9
# BB#3:
	leaq	136(%rbx), %rdi
	callq	AutoResetEvent_CreateNotSignaled
	testl	%eax, %eax
	jne	.LBB14_9
# BB#4:
	leaq	240(%rbx), %rdi
	callq	AutoResetEvent_CreateNotSignaled
	testl	%eax, %eax
	jne	.LBB14_9
# BB#5:
	leaq	344(%rbx), %rdi
	movl	%ebp, %esi
	movl	%ebp, %edx
	callq	Semaphore_Create
	testl	%eax, %eax
	jne	.LBB14_9
# BB#6:
	leaq	448(%rbx), %rdi
	xorl	%esi, %esi
	movl	%ebp, %edx
	callq	Semaphore_Create
	testl	%eax, %eax
	jne	.LBB14_9
# BB#7:
	movl	$1, 4(%rbx)
	leaq	16(%rbx), %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	Thread_Create
	testl	%eax, %eax
	je	.LBB14_8
.LBB14_9:
	movq	%rbx, %rdi
	callq	MtSync_Destruct
	movl	$12, %eax
.LBB14_10:                              # %MtSync_Create2.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_8:
	movl	$1, (%rbx)
	xorl	%eax, %eax
	jmp	.LBB14_10
.Lfunc_end14:
	.size	MtSync_Create, .Lfunc_end14-MtSync_Create
	.cfi_endproc

	.p2align	4, 0x90
	.type	HashThreadFunc2,@function
HashThreadFunc2:                        # @HashThreadFunc2
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi99:
	.cfi_def_cfa_offset 16
	callq	HashThreadFunc
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end15:
	.size	HashThreadFunc2, .Lfunc_end15-HashThreadFunc2
	.cfi_endproc

	.p2align	4, 0x90
	.type	BtThreadFunc2,@function
BtThreadFunc2:                          # @BtThreadFunc2
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi100:
	.cfi_def_cfa_offset 16
	callq	BtThreadFunc
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end16:
	.size	BtThreadFunc2, .Lfunc_end16-BtThreadFunc2
	.cfi_endproc

	.globl	MatchFinderMt_Init
	.p2align	4, 0x90
	.type	MatchFinderMt_Init,@function
MatchFinderMt_Init:                     # @MatchFinderMt_Init
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi103:
	.cfi_def_cfa_offset 32
.Lcfi104:
	.cfi_offset %rbx, -24
.Lcfi105:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	1488(%rbx), %r14
	movq	$0, 16(%rbx)
	movq	$0, 808(%rbx)
	movq	%r14, %rdi
	callq	MatchFinder_Init
	movq	%r14, %rdi
	callq	MatchFinder_GetPointerToCurrentPos
	movq	%rax, (%rbx)
	movl	$0, 28(%rbx)
	movl	44(%rbx), %eax
	incl	%eax
	movl	%eax, 24(%rbx)
	movq	40(%r14), %rax
	movq	%rax, 32(%rbx)
	movl	124(%r14), %eax
	movl	%eax, 40(%rbx)
	leaq	140(%r14), %rax
	movq	%rax, 48(%rbx)
	movq	48(%r14), %rax
	movq	%rax, 824(%rbx)
	movl	32(%r14), %eax
	movl	%eax, 832(%rbx)
	movl	96(%r14), %eax
	movl	%eax, 836(%rbx)
	movl	8(%r14), %eax
	movl	%eax, 840(%rbx)
	movq	(%r14), %rax
	movq	%rax, 848(%rbx)
	movl	24(%r14), %eax
	movl	%eax, 856(%rbx)
	movl	28(%r14), %eax
	movl	%eax, 860(%rbx)
	movl	60(%r14), %eax
	movl	%eax, 864(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end17:
	.size	MatchFinderMt_Init, .Lfunc_end17-MatchFinderMt_Init
	.cfi_endproc

	.globl	MatchFinderMt_ReleaseStream
	.p2align	4, 0x90
	.type	MatchFinderMt_ReleaseStream,@function
MatchFinderMt_ReleaseStream:            # @MatchFinderMt_ReleaseStream
	.cfi_startproc
# BB#0:
	addq	$64, %rdi
	jmp	MtSync_StopWriting      # TAILCALL
.Lfunc_end18:
	.size	MatchFinderMt_ReleaseStream, .Lfunc_end18-MatchFinderMt_ReleaseStream
	.cfi_endproc

	.globl	MatchFinderMt_Normalize
	.p2align	4, 0x90
	.type	MatchFinderMt_Normalize,@function
MatchFinderMt_Normalize:                # @MatchFinderMt_Normalize
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 16
.Lcfi107:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	24(%rbx), %edi
	decl	%edi
	subl	44(%rbx), %edi
	movq	32(%rbx), %rsi
	movl	40(%rbx), %edx
	callq	MatchFinder_Normalize3
	movl	44(%rbx), %eax
	incl	%eax
	movl	%eax, 24(%rbx)
	popq	%rbx
	retq
.Lfunc_end19:
	.size	MatchFinderMt_Normalize, .Lfunc_end19-MatchFinderMt_Normalize
	.cfi_endproc

	.globl	MatchFinderMt_GetNextBlock_Bt
	.p2align	4, 0x90
	.type	MatchFinderMt_GetNextBlock_Bt,@function
MatchFinderMt_GetNextBlock_Bt:          # @MatchFinderMt_GetNextBlock_Bt
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 16
.Lcfi109:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	64(%rbx), %rdi
	callq	MtSync_GetNextBlock
	movl	664(%rbx), %eax
	shll	$14, %eax
	addl	$1032192, %eax          # imm = 0xFC000
	andl	$1032192, %eax          # imm = 0xFC000
	movl	%eax, 20(%rbx)
	movq	8(%rbx), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%rbx)
	addl	(%rcx,%rax,4), %eax
	movl	%eax, 20(%rbx)
	leal	1(%rdx), %eax
	movl	%eax, 16(%rbx)
	movl	(%rcx,%rdx,4), %eax
	movl	%eax, 28(%rbx)
	movl	24(%rbx), %edi
	cmpl	$-16385, %edi           # imm = 0xBFFF
	jb	.LBB20_2
# BB#1:
	decl	%edi
	subl	44(%rbx), %edi
	movq	32(%rbx), %rsi
	movl	40(%rbx), %edx
	callq	MatchFinder_Normalize3
	movl	44(%rbx), %eax
	incl	%eax
	movl	%eax, 24(%rbx)
.LBB20_2:
	popq	%rbx
	retq
.Lfunc_end20:
	.size	MatchFinderMt_GetNextBlock_Bt, .Lfunc_end20-MatchFinderMt_GetNextBlock_Bt
	.cfi_endproc

	.globl	MatchFinderMt_GetPointerToCurrentPos
	.p2align	4, 0x90
	.type	MatchFinderMt_GetPointerToCurrentPos,@function
MatchFinderMt_GetPointerToCurrentPos:   # @MatchFinderMt_GetPointerToCurrentPos
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	retq
.Lfunc_end21:
	.size	MatchFinderMt_GetPointerToCurrentPos, .Lfunc_end21-MatchFinderMt_GetPointerToCurrentPos
	.cfi_endproc

	.globl	MatchFinderMt_GetNumAvailableBytes
	.p2align	4, 0x90
	.type	MatchFinderMt_GetNumAvailableBytes,@function
MatchFinderMt_GetNumAvailableBytes:     # @MatchFinderMt_GetNumAvailableBytes
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi112:
	.cfi_def_cfa_offset 32
.Lcfi113:
	.cfi_offset %rbx, -24
.Lcfi114:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	16(%rbx), %eax
	cmpl	20(%rbx), %eax
	jne	.LBB22_1
# BB#2:
	leaq	64(%rbx), %rdi
	callq	MtSync_GetNextBlock
	movl	664(%rbx), %eax
	shll	$14, %eax
	addl	$1032192, %eax          # imm = 0xFC000
	andl	$1032192, %eax          # imm = 0xFC000
	movl	%eax, 20(%rbx)
	movq	8(%rbx), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%rbx)
	addl	(%rcx,%rax,4), %eax
	movl	%eax, 20(%rbx)
	leal	1(%rdx), %eax
	movl	%eax, 16(%rbx)
	movl	(%rcx,%rdx,4), %eax
	leaq	28(%rbx), %r14
	movl	%eax, 28(%rbx)
	movl	24(%rbx), %edi
	cmpl	$-16385, %edi           # imm = 0xBFFF
	jb	.LBB22_4
# BB#3:
	decl	%edi
	subl	44(%rbx), %edi
	movq	32(%rbx), %rsi
	movl	40(%rbx), %edx
	callq	MatchFinder_Normalize3
	movl	44(%rbx), %eax
	incl	%eax
	movl	%eax, 24(%rbx)
	jmp	.LBB22_4
.LBB22_1:                               # %.MatchFinderMt_GetNextBlock_Bt.exit_crit_edge
	addq	$28, %rbx
	movq	%rbx, %r14
.LBB22_4:                               # %MatchFinderMt_GetNextBlock_Bt.exit
	movl	(%r14), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end22:
	.size	MatchFinderMt_GetNumAvailableBytes, .Lfunc_end22-MatchFinderMt_GetNumAvailableBytes
	.cfi_endproc

	.globl	MatchFinderMt_GetIndexByte
	.p2align	4, 0x90
	.type	MatchFinderMt_GetIndexByte,@function
MatchFinderMt_GetIndexByte:             # @MatchFinderMt_GetIndexByte
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movslq	%esi, %rcx
	movb	(%rax,%rcx), %al
	retq
.Lfunc_end23:
	.size	MatchFinderMt_GetIndexByte, .Lfunc_end23-MatchFinderMt_GetIndexByte
	.cfi_endproc

	.globl	MixMatches2
	.p2align	4, 0x90
	.type	MixMatches2,@function
MixMatches2:                            # @MixMatches2
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %rax
	movq	(%rdi), %rcx
	movl	24(%rdi), %r8d
	movq	48(%rdi), %r9
	movzbl	(%rcx), %r11d
	movzbl	1(%rcx), %r10d
	movl	$1023, %edi             # imm = 0x3FF
	andl	(%r9,%r11,4), %edi
	xorl	%r10d, %edi
	movl	(%rax,%rdi,4), %r9d
	movl	%r8d, (%rax,%rdi,4)
	cmpl	%esi, %r9d
	jb	.LBB24_3
# BB#1:
	movq	%r9, %rax
	subq	%r8, %rax
	movb	(%rcx,%rax), %al
	cmpb	(%rcx), %al
	jne	.LBB24_3
# BB#2:
	movl	$2, (%rdx)
	decl	%r8d
	subl	%r9d, %r8d
	movl	%r8d, 4(%rdx)
	addq	$8, %rdx
.LBB24_3:
	movq	%rdx, %rax
	retq
.Lfunc_end24:
	.size	MixMatches2, .Lfunc_end24-MixMatches2
	.cfi_endproc

	.globl	MixMatches3
	.p2align	4, 0x90
	.type	MixMatches3,@function
MixMatches3:                            # @MixMatches3
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 16
.Lcfi116:
	.cfi_offset %rbx, -16
	movq	32(%rdi), %rbx
	movq	(%rdi), %r10
	movl	24(%rdi), %r9d
	movq	48(%rdi), %r8
	movzbl	(%r10), %edi
	movzbl	1(%r10), %ecx
	xorl	(%r8,%rdi,4), %ecx
	movzwl	%cx, %edi
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	andl	$1023, %ecx             # imm = 0x3FF
	movzbl	2(%r10), %eax
	shll	$8, %eax
	xorl	%eax, %edi
	movl	(%rbx,%rcx,4), %r11d
	movl	4096(%rbx,%rdi,4), %r8d
	movl	%r9d, 4096(%rbx,%rdi,4)
	movl	%r9d, (%rbx,%rcx,4)
	cmpl	%esi, %r11d
	jb	.LBB25_5
# BB#1:
	movq	%r11, %rax
	subq	%r9, %rax
	movb	(%r10,%rax), %cl
	cmpb	(%r10), %cl
	jne	.LBB25_5
# BB#2:
	leal	-1(%r9), %ecx
	subl	%r11d, %ecx
	movl	%ecx, 4(%rdx)
	movb	2(%r10,%rax), %al
	cmpb	2(%r10), %al
	jne	.LBB25_4
# BB#3:
	movl	$3, (%rdx)
	jmp	.LBB25_8
.LBB25_4:
	movl	$2, (%rdx)
	addq	$8, %rdx
.LBB25_5:
	cmpl	%esi, %r8d
	jb	.LBB25_9
# BB#6:
	movq	%r8, %rax
	subq	%r9, %rax
	movb	(%r10,%rax), %al
	cmpb	(%r10), %al
	jne	.LBB25_9
# BB#7:
	movl	$3, (%rdx)
	decl	%r9d
	subl	%r8d, %r9d
	movl	%r9d, 4(%rdx)
.LBB25_8:
	addq	$8, %rdx
.LBB25_9:
	movq	%rdx, %rax
	popq	%rbx
	retq
.Lfunc_end25:
	.size	MixMatches3, .Lfunc_end25-MixMatches3
	.cfi_endproc

	.globl	MatchFinderMt2_GetMatches
	.p2align	4, 0x90
	.type	MatchFinderMt2_GetMatches,@function
MatchFinderMt2_GetMatches:              # @MatchFinderMt2_GetMatches
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %r8
	movl	16(%rdi), %edx
	movl	(%r8,%rdx,4), %eax
	leal	1(%rdx,%rax), %ecx
	movl	%ecx, 16(%rdi)
	decl	28(%rdi)
	testl	%eax, %eax
	je	.LBB26_3
# BB#1:                                 # %.lr.ph.preheader
	leaq	8(%r8,%rdx,4), %r8
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB26_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r8,%rdx,4), %ecx
	movl	%ecx, (%rsi,%rdx,4)
	movl	(%r8,%rdx,4), %ecx
	movl	%ecx, 4(%rsi,%rdx,4)
	addq	$2, %rdx
	cmpl	%eax, %edx
	jb	.LBB26_2
.LBB26_3:                               # %._crit_edge
	incl	24(%rdi)
	incq	(%rdi)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end26:
	.size	MatchFinderMt2_GetMatches, .Lfunc_end26-MatchFinderMt2_GetMatches
	.cfi_endproc

	.globl	MatchFinderMt_GetMatches
	.p2align	4, 0x90
	.type	MatchFinderMt_GetMatches,@function
MatchFinderMt_GetMatches:               # @MatchFinderMt_GetMatches
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi117:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi118:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi119:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi120:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi121:
	.cfi_def_cfa_offset 48
.Lcfi122:
	.cfi_offset %rbx, -40
.Lcfi123:
	.cfi_offset %r12, -32
.Lcfi124:
	.cfi_offset %r14, -24
.Lcfi125:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	8(%r15), %rax
	movl	16(%r15), %ecx
	movl	(%rax,%rcx,4), %r12d
	leal	1(%rcx,%r12), %edx
	movl	%edx, 16(%r15)
	movl	28(%r15), %edx
	leal	-1(%rdx), %esi
	movl	%esi, 28(%r15)
	testl	%r12d, %r12d
	je	.LBB27_1
# BB#3:
	leaq	4(%rax,%rcx,4), %rbx
	movl	24(%r15), %esi
	subl	4(%rbx), %esi
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	*56(%r15)
	.p2align	4, 0x90
.LBB27_4:                               # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	movl	%ecx, (%rax)
	movl	4(%rbx), %ecx
	movl	%ecx, 4(%rax)
	addq	$8, %rax
	addq	$8, %rbx
	addl	$-2, %r12d
	jne	.LBB27_4
	jmp	.LBB27_5
.LBB27_1:
	xorl	%eax, %eax
	cmpl	$4, %edx
	jb	.LBB27_6
# BB#2:
	movl	24(%r15), %esi
	subl	44(%r15), %esi
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	*56(%r15)
.LBB27_5:                               # %.sink.split
	subq	%r14, %rax
	shrq	$2, %rax
.LBB27_6:
	incl	24(%r15)
	incq	(%r15)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end27:
	.size	MatchFinderMt_GetMatches, .Lfunc_end27-MatchFinderMt_GetMatches
	.cfi_endproc

	.globl	MatchFinderMt0_Skip
	.p2align	4, 0x90
	.type	MatchFinderMt0_Skip,@function
MatchFinderMt0_Skip:                    # @MatchFinderMt0_Skip
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi126:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi127:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi128:
	.cfi_def_cfa_offset 32
.Lcfi129:
	.cfi_offset %rbx, -32
.Lcfi130:
	.cfi_offset %r14, -24
.Lcfi131:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	leaq	64(%rbx), %r14
	movl	16(%rbx), %eax
	.p2align	4, 0x90
.LBB28_1:                               # =>This Inner Loop Header: Depth=1
	cmpl	20(%rbx), %eax
	jne	.LBB28_2
# BB#3:                                 #   in Loop: Header=BB28_1 Depth=1
	movq	%r14, %rdi
	callq	MtSync_GetNextBlock
	movl	664(%rbx), %eax
	shll	$14, %eax
	addl	$1032192, %eax          # imm = 0xFC000
	andl	$1032192, %eax          # imm = 0xFC000
	movl	%eax, 20(%rbx)
	movq	8(%rbx), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%rbx)
	addl	(%rcx,%rax,4), %eax
	movl	%eax, 20(%rbx)
	leal	1(%rdx), %eax
	movl	%eax, 16(%rbx)
	movl	(%rcx,%rdx,4), %ecx
	movl	%ecx, 28(%rbx)
	movl	24(%rbx), %edi
	cmpl	$-16385, %edi           # imm = 0xBFFF
	jb	.LBB28_5
# BB#4:                                 #   in Loop: Header=BB28_1 Depth=1
	decl	%edi
	subl	44(%rbx), %edi
	movq	32(%rbx), %rsi
	movl	40(%rbx), %edx
	callq	MatchFinder_Normalize3
	movl	44(%rbx), %edi
	incl	%edi
	movl	%edi, 24(%rbx)
	movl	16(%rbx), %eax
	jmp	.LBB28_5
	.p2align	4, 0x90
.LBB28_2:                               # %.MatchFinderMt_GetNextBlock_Bt.exit_crit_edge
                                        #   in Loop: Header=BB28_1 Depth=1
	movl	24(%rbx), %edi
.LBB28_5:                               # %MatchFinderMt_GetNextBlock_Bt.exit
                                        #   in Loop: Header=BB28_1 Depth=1
	decl	28(%rbx)
	incl	%edi
	movl	%edi, 24(%rbx)
	incq	(%rbx)
	movq	8(%rbx), %rcx
	movl	%eax, %edx
	movl	(%rcx,%rdx,4), %ecx
	leal	1(%rax,%rcx), %eax
	movl	%eax, 16(%rbx)
	decl	%ebp
	jne	.LBB28_1
# BB#6:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end28:
	.size	MatchFinderMt0_Skip, .Lfunc_end28-MatchFinderMt0_Skip
	.cfi_endproc

	.globl	MatchFinderMt2_Skip
	.p2align	4, 0x90
	.type	MatchFinderMt2_Skip,@function
MatchFinderMt2_Skip:                    # @MatchFinderMt2_Skip
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi132:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi133:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi134:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi136:
	.cfi_def_cfa_offset 48
.Lcfi137:
	.cfi_offset %rbx, -40
.Lcfi138:
	.cfi_offset %r14, -32
.Lcfi139:
	.cfi_offset %r15, -24
.Lcfi140:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	leaq	64(%rbx), %r14
	movl	16(%rbx), %eax
	movl	$1023, %r15d            # imm = 0x3FF
	.p2align	4, 0x90
.LBB29_1:                               # =>This Inner Loop Header: Depth=1
	cmpl	20(%rbx), %eax
	jne	.LBB29_4
# BB#2:                                 #   in Loop: Header=BB29_1 Depth=1
	movq	%r14, %rdi
	callq	MtSync_GetNextBlock
	movl	664(%rbx), %eax
	shll	$14, %eax
	addl	$1032192, %eax          # imm = 0xFC000
	andl	$1032192, %eax          # imm = 0xFC000
	movl	%eax, 20(%rbx)
	movq	8(%rbx), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%rbx)
	addl	(%rcx,%rax,4), %eax
	movl	%eax, 20(%rbx)
	leal	1(%rdx), %eax
	movl	%eax, 16(%rbx)
	movl	(%rcx,%rdx,4), %ecx
	movl	%ecx, 28(%rbx)
	movl	24(%rbx), %edi
	cmpl	$-16385, %edi           # imm = 0xBFFF
	jb	.LBB29_5
# BB#3:                                 #   in Loop: Header=BB29_1 Depth=1
	decl	%edi
	subl	44(%rbx), %edi
	movq	32(%rbx), %rsi
	movl	40(%rbx), %edx
	callq	MatchFinder_Normalize3
	movl	44(%rbx), %eax
	incl	%eax
	movl	%eax, 24(%rbx)
.LBB29_4:                               # %MatchFinderMt_GetNextBlock_Bt.exitthread-pre-split
                                        #   in Loop: Header=BB29_1 Depth=1
	movl	28(%rbx), %ecx
.LBB29_5:                               # %MatchFinderMt_GetNextBlock_Bt.exit
                                        #   in Loop: Header=BB29_1 Depth=1
	leal	-1(%rcx), %eax
	movl	%eax, 28(%rbx)
	movq	(%rbx), %rax
	cmpl	$2, %ecx
	jb	.LBB29_7
# BB#6:                                 #   in Loop: Header=BB29_1 Depth=1
	movq	32(%rbx), %rcx
	movq	48(%rbx), %rdx
	movzbl	(%rax), %esi
	movzbl	1(%rax), %edi
	movl	(%rdx,%rsi,4), %edx
	andl	%r15d, %edx
	xorl	%edi, %edx
	movl	24(%rbx), %esi
	movl	%esi, (%rcx,%rdx,4)
.LBB29_7:                               # %MatchFinderMt_GetNextBlock_Bt.exit._crit_edge
                                        #   in Loop: Header=BB29_1 Depth=1
	incl	24(%rbx)
	incq	%rax
	movq	%rax, (%rbx)
	movq	8(%rbx), %rax
	movl	16(%rbx), %ecx
	movl	(%rax,%rcx,4), %eax
	leal	1(%rcx,%rax), %eax
	movl	%eax, 16(%rbx)
	decl	%ebp
	jne	.LBB29_1
# BB#8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end29:
	.size	MatchFinderMt2_Skip, .Lfunc_end29-MatchFinderMt2_Skip
	.cfi_endproc

	.globl	MatchFinderMt3_Skip
	.p2align	4, 0x90
	.type	MatchFinderMt3_Skip,@function
MatchFinderMt3_Skip:                    # @MatchFinderMt3_Skip
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi141:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi142:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi143:
	.cfi_def_cfa_offset 32
.Lcfi144:
	.cfi_offset %rbx, -32
.Lcfi145:
	.cfi_offset %r14, -24
.Lcfi146:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	leaq	64(%rbx), %r14
	movl	16(%rbx), %eax
	.p2align	4, 0x90
.LBB30_1:                               # =>This Inner Loop Header: Depth=1
	cmpl	20(%rbx), %eax
	jne	.LBB30_4
# BB#2:                                 #   in Loop: Header=BB30_1 Depth=1
	movq	%r14, %rdi
	callq	MtSync_GetNextBlock
	movl	664(%rbx), %eax
	shll	$14, %eax
	addl	$1032192, %eax          # imm = 0xFC000
	andl	$1032192, %eax          # imm = 0xFC000
	movl	%eax, 20(%rbx)
	movq	8(%rbx), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%rbx)
	addl	(%rcx,%rax,4), %eax
	movl	%eax, 20(%rbx)
	leal	1(%rdx), %eax
	movl	%eax, 16(%rbx)
	movl	(%rcx,%rdx,4), %ecx
	movl	%ecx, 28(%rbx)
	movl	24(%rbx), %edi
	cmpl	$-16385, %edi           # imm = 0xBFFF
	jb	.LBB30_5
# BB#3:                                 #   in Loop: Header=BB30_1 Depth=1
	decl	%edi
	subl	44(%rbx), %edi
	movq	32(%rbx), %rsi
	movl	40(%rbx), %edx
	callq	MatchFinder_Normalize3
	movl	44(%rbx), %eax
	incl	%eax
	movl	%eax, 24(%rbx)
.LBB30_4:                               # %MatchFinderMt_GetNextBlock_Bt.exitthread-pre-split
                                        #   in Loop: Header=BB30_1 Depth=1
	movl	28(%rbx), %ecx
.LBB30_5:                               # %MatchFinderMt_GetNextBlock_Bt.exit
                                        #   in Loop: Header=BB30_1 Depth=1
	leal	-1(%rcx), %eax
	movl	%eax, 28(%rbx)
	movq	(%rbx), %rax
	cmpl	$3, %ecx
	jb	.LBB30_7
# BB#6:                                 #   in Loop: Header=BB30_1 Depth=1
	movq	32(%rbx), %rcx
	movq	48(%rbx), %rdx
	movzbl	(%rax), %esi
	movzbl	1(%rax), %edi
	xorl	(%rdx,%rsi,4), %edi
	movzwl	%di, %edx
	movl	%edi, %esi
	andl	$1023, %esi             # imm = 0x3FF
	movzbl	2(%rax), %edi
	shll	$8, %edi
	xorl	%edi, %edx
	movl	24(%rbx), %edi
	movl	%edi, (%rcx,%rsi,4)
	movl	%edi, 4096(%rcx,%rdx,4)
.LBB30_7:                               # %MatchFinderMt_GetNextBlock_Bt.exit._crit_edge
                                        #   in Loop: Header=BB30_1 Depth=1
	incl	24(%rbx)
	incq	%rax
	movq	%rax, (%rbx)
	movq	8(%rbx), %rax
	movl	16(%rbx), %ecx
	movl	(%rax,%rcx,4), %eax
	leal	1(%rcx,%rax), %eax
	movl	%eax, 16(%rbx)
	decl	%ebp
	jne	.LBB30_1
# BB#8:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end30:
	.size	MatchFinderMt3_Skip, .Lfunc_end30-MatchFinderMt3_Skip
	.cfi_endproc

	.globl	MatchFinderMt_CreateVTable
	.p2align	4, 0x90
	.type	MatchFinderMt_CreateVTable,@function
MatchFinderMt_CreateVTable:             # @MatchFinderMt_CreateVTable
	.cfi_startproc
# BB#0:
	movq	$MatchFinderMt_Init, (%rsi)
	movq	$MatchFinderMt_GetIndexByte, 8(%rsi)
	movq	$MatchFinderMt_GetNumAvailableBytes, 16(%rsi)
	movq	$MatchFinderMt_GetPointerToCurrentPos, 24(%rsi)
	movq	$MatchFinderMt_GetMatches, 32(%rsi)
	movq	1488(%rdi), %rax
	movl	96(%rax), %ecx
	cmpl	$3, %ecx
	je	.LBB31_3
# BB#1:
	cmpl	$2, %ecx
	jne	.LBB31_4
# BB#2:
	movq	$GetHeads2, 1480(%rdi)
	movq	$0, 56(%rdi)
	movq	$MatchFinderMt0_Skip, 40(%rsi)
	movq	$MatchFinderMt2_GetMatches, 32(%rsi)
	retq
.LBB31_3:
	movq	$GetHeads3, 1480(%rdi)
	movq	$MixMatches2, 56(%rdi)
	movq	$MatchFinderMt2_Skip, 40(%rsi)
	retq
.LBB31_4:
	cmpl	$0, 116(%rax)
	movl	$GetHeads4b, %eax
	movl	$GetHeads4, %ecx
	cmovneq	%rax, %rcx
	movq	%rcx, 1480(%rdi)
	movq	$MixMatches3, 56(%rdi)
	movq	$MatchFinderMt3_Skip, 40(%rsi)
	retq
.Lfunc_end31:
	.size	MatchFinderMt_CreateVTable, .Lfunc_end31-MatchFinderMt_CreateVTable
	.cfi_endproc

	.p2align	4, 0x90
	.type	GetHeads2,@function
GetHeads2:                              # @GetHeads2
	.cfi_startproc
# BB#0:
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	testl	%r9d, %r9d
	je	.LBB32_6
# BB#1:                                 # %.lr.ph.preheader
	testb	$1, %r9b
	jne	.LBB32_3
# BB#2:
	movl	%r9d, %r10d
	cmpl	$1, %r9d
	jne	.LBB32_5
	jmp	.LBB32_6
.LBB32_3:                               # %.lr.ph.prol
	movzbl	(%rdi), %eax
	movzbl	1(%rdi), %ecx
	incq	%rdi
	shlq	$8, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	subl	(%rdx,%rcx,4), %eax
	movl	%eax, (%r8)
	addq	$4, %r8
	movl	%esi, (%rdx,%rcx,4)
	incl	%esi
	leal	-1(%r9), %r10d
	cmpl	$1, %r9d
	je	.LBB32_6
	.p2align	4, 0x90
.LBB32_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %r9d
	movzbl	1(%rdi), %ecx
	shlq	$8, %rcx
	orq	%r9, %rcx
	movl	%esi, %eax
	subl	(%rdx,%rcx,4), %eax
	movl	%eax, (%r8)
	movl	%esi, (%rdx,%rcx,4)
	movzbl	1(%rdi), %eax
	movzbl	2(%rdi), %ecx
	addq	$2, %rdi
	shlq	$8, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	subl	(%rdx,%rcx,4), %eax
	incl	%eax
	movl	%eax, 4(%r8)
	leal	1(%rsi), %eax
	movl	%eax, (%rdx,%rcx,4)
	addq	$8, %r8
	addl	$2, %esi
	addl	$-2, %r10d
	jne	.LBB32_5
.LBB32_6:                               # %._crit_edge
	retq
.Lfunc_end32:
	.size	GetHeads2, .Lfunc_end32-GetHeads2
	.cfi_endproc

	.p2align	4, 0x90
	.type	GetHeads3,@function
GetHeads3:                              # @GetHeads3
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi147:
	.cfi_def_cfa_offset 16
.Lcfi148:
	.cfi_offset %rbx, -16
	testl	%r9d, %r9d
	je	.LBB33_3
# BB#1:                                 # %.lr.ph.preheader
	movq	16(%rsp), %r10
	.p2align	4, 0x90
.LBB33_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %r11d
	movzbl	1(%rdi), %ebx
	xorl	(%r10,%r11,4), %ebx
	movzbl	2(%rdi), %eax
	incq	%rdi
	shll	$8, %eax
	xorl	%ebx, %eax
	andl	%ecx, %eax
	movl	%esi, %ebx
	subl	(%rdx,%rax,4), %ebx
	movl	%ebx, (%r8)
	addq	$4, %r8
	movl	%esi, (%rdx,%rax,4)
	incl	%esi
	decl	%r9d
	jne	.LBB33_2
.LBB33_3:                               # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end33:
	.size	GetHeads3, .Lfunc_end33-GetHeads3
	.cfi_endproc

	.p2align	4, 0x90
	.type	GetHeads4b,@function
GetHeads4b:                             # @GetHeads4b
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi149:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi150:
	.cfi_def_cfa_offset 24
.Lcfi151:
	.cfi_offset %rbx, -24
.Lcfi152:
	.cfi_offset %rbp, -16
	testl	%r9d, %r9d
	je	.LBB34_3
# BB#1:                                 # %.lr.ph.preheader
	movq	24(%rsp), %r10
	.p2align	4, 0x90
.LBB34_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %r11d
	movzbl	1(%rdi), %ebp
	xorl	(%r10,%r11,4), %ebp
	movzbl	2(%rdi), %eax
	shll	$8, %eax
	movzbl	3(%rdi), %ebx
	incq	%rdi
	shll	$16, %ebx
	xorl	%eax, %ebx
	xorl	%ebp, %ebx
	andl	%ecx, %ebx
	movl	%esi, %eax
	subl	(%rdx,%rbx,4), %eax
	movl	%eax, (%r8)
	addq	$4, %r8
	movl	%esi, (%rdx,%rbx,4)
	incl	%esi
	decl	%r9d
	jne	.LBB34_2
.LBB34_3:                               # %._crit_edge
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end34:
	.size	GetHeads4b, .Lfunc_end34-GetHeads4b
	.cfi_endproc

	.p2align	4, 0x90
	.type	GetHeads4,@function
GetHeads4:                              # @GetHeads4
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi153:
	.cfi_def_cfa_offset 16
.Lcfi154:
	.cfi_offset %rbx, -16
	testl	%r9d, %r9d
	je	.LBB35_3
# BB#1:                                 # %.lr.ph.preheader
	movq	16(%rsp), %r10
	.p2align	4, 0x90
.LBB35_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %r11d
	movzbl	1(%rdi), %ebx
	xorl	(%r10,%r11,4), %ebx
	movzbl	2(%rdi), %eax
	shll	$8, %eax
	xorl	%ebx, %eax
	movzbl	3(%rdi), %ebx
	incq	%rdi
	movl	(%r10,%rbx,4), %ebx
	shll	$5, %ebx
	xorl	%eax, %ebx
	andl	%ecx, %ebx
	movl	%esi, %eax
	subl	(%rdx,%rbx,4), %eax
	movl	%eax, (%r8)
	addq	$4, %r8
	movl	%esi, (%rdx,%rbx,4)
	incl	%esi
	decl	%r9d
	jne	.LBB35_2
.LBB35_3:                               # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end35:
	.size	GetHeads4, .Lfunc_end35-GetHeads4
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
