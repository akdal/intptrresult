	.text
	.file	"UserInputUtils.bc"
	.globl	_Z20ScanUserYesNoAllQuitP13CStdOutStream
	.p2align	4, 0x90
	.type	_Z20ScanUserYesNoAllQuitP13CStdOutStream,@function
_Z20ScanUserYesNoAllQuitP13CStdOutStream: # @_Z20ScanUserYesNoAllQuitP13CStdOutStream
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 64
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	$.L.str.4, %esi
	callq	_ZN13CStdOutStreamlsEPKc
	movq	%rsp, %r14
                                        # implicit-def: %EBP
	jmp	.LBB0_1
.LBB0_9:                                #   in Loop: Header=BB0_1 Depth=1
	movl	$1, %r12d
	xorl	%ebx, %ebx
	jmp	.LBB0_15
.LBB0_13:                               #   in Loop: Header=BB0_1 Depth=1
	movl	$5, %r12d
	xorl	%ebx, %ebx
	jmp	.LBB0_15
.LBB0_11:                               #   in Loop: Header=BB0_1 Depth=1
	movl	$3, %r12d
	xorl	%ebx, %ebx
	jmp	.LBB0_15
.LBB0_12:                               #   in Loop: Header=BB0_1 Depth=1
	movl	$4, %r12d
	xorl	%ebx, %ebx
	jmp	.LBB0_15
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movl	$.L.str.5, %esi
	movq	%r15, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
	movq	%r15, %rdi
	callq	_ZN13CStdOutStream5FlushEv
	movl	$g_StdIn, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	_ZN12CStdInStream22ScanStringUntilNewLineEb
.Ltmp0:
	movq	%r14, %rdi
	callq	_ZN11CStringBaseIcE4TrimEv
.Ltmp1:
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	cmpl	$0, 8(%rsp)
	je	.LBB0_14
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	movq	(%rsp), %rax
	movsbl	(%rax), %edi
.Ltmp2:
	callq	_Z11MyCharUpperw
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
.Ltmp3:
# BB#4:                                 #   in Loop: Header=BB0_1 Depth=1
	addl	$-65, %eax
	cmpl	$24, %eax
	ja	.LBB0_14
# BB#5:                                 #   in Loop: Header=BB0_1 Depth=1
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_10:                               #   in Loop: Header=BB0_1 Depth=1
	movl	$2, %r12d
	xorl	%ebx, %ebx
	jmp	.LBB0_15
	.p2align	4, 0x90
.LBB0_14:                               #   in Loop: Header=BB0_1 Depth=1
	movb	$1, %bl
	movl	%ebp, %r12d
.LBB0_15:                               #   in Loop: Header=BB0_1 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_1 Depth=1
	callq	_ZdaPv
.LBB0_17:                               # %_ZN11CStringBaseIcED2Ev.exit
                                        #   in Loop: Header=BB0_1 Depth=1
	testb	%bl, %bl
	movl	%r12d, %ebp
	jne	.LBB0_1
# BB#18:
	movl	%r12d, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_6:
.Ltmp4:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_8
# BB#7:
	callq	_ZdaPv
.LBB0_8:                                # %_ZN11CStringBaseIcED2Ev.exit7
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z20ScanUserYesNoAllQuitP13CStdOutStream, .Lfunc_end0-_Z20ScanUserYesNoAllQuitP13CStdOutStream
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_10
	.quad	.LBB0_14
	.quad	.LBB0_14
	.quad	.LBB0_14
	.quad	.LBB0_14
	.quad	.LBB0_14
	.quad	.LBB0_14
	.quad	.LBB0_14
	.quad	.LBB0_14
	.quad	.LBB0_14
	.quad	.LBB0_14
	.quad	.LBB0_14
	.quad	.LBB0_14
	.quad	.LBB0_9
	.quad	.LBB0_14
	.quad	.LBB0_14
	.quad	.LBB0_13
	.quad	.LBB0_14
	.quad	.LBB0_11
	.quad	.LBB0_14
	.quad	.LBB0_12
	.quad	.LBB0_14
	.quad	.LBB0_14
	.quad	.LBB0_14
	.quad	.LBB0_15
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp3      #   Call between .Ltmp3 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIcE4TrimEv,"axG",@progbits,_ZN11CStringBaseIcE4TrimEv,comdat
	.weak	_ZN11CStringBaseIcE4TrimEv
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcE4TrimEv,@function
_ZN11CStringBaseIcE4TrimEv:             # @_ZN11CStringBaseIcE4TrimEv
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 80
.Lcfi17:
	.cfi_offset %rbx, -48
.Lcfi18:
	.cfi_offset %r12, -40
.Lcfi19:
	.cfi_offset %r13, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	$0, 8(%rsp)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, (%rsp)
	movb	$0, (%rax)
	movl	$4, 12(%rsp)
.Ltmp5:
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp6:
# BB#1:
.Ltmp7:
	movq	%rsp, %rdi
	movl	$10, %esi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp8:
# BB#2:
.Ltmp9:
	movq	%rsp, %rdi
	movl	$9, %esi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp10:
# BB#3:                                 # %_ZN11CStringBaseIcE21GetTrimDefaultCharSetEv.exit
	movq	(%r14), %r15
	movb	(%r15), %bl
	testb	%bl, %bl
	je	.LBB1_16
# BB#4:                                 # %.lr.ph.i.preheader
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_6 Depth 2
	movq	(%rsp), %rcx
	movb	(%rcx), %dl
	cmpb	%bl, %dl
	movq	%rcx, %rax
	je	.LBB1_10
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph.i.i.i
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	%dl, %dl
	je	.LBB1_7
# BB#30:                                #   in Loop: Header=BB1_6 Depth=2
.Ltmp12:
	movq	%rax, %rdi
	callq	_Z9CharNextAPKc
.Ltmp13:
# BB#31:                                # %.noexc
                                        #   in Loop: Header=BB1_6 Depth=2
	movzbl	(%rax), %edx
	cmpb	%bl, %dl
	jne	.LBB1_6
# BB#9:                                 # %._crit_edge.loopexit.i.i.i
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	(%rsp), %rcx
.LBB1_10:                               # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB1_5 Depth=1
	subl	%ecx, %eax
	jmp	.LBB1_11
	.p2align	4, 0x90
.LBB1_7:                                #   in Loop: Header=BB1_5 Depth=1
	movl	$-1, %eax
.LBB1_11:                               # %_ZNK11CStringBaseIcE4FindEc.exit.i
                                        #   in Loop: Header=BB1_5 Depth=1
	testq	%r13, %r13
	cmoveq	%r15, %r13
	testl	%eax, %eax
	cmovsq	%r12, %r13
.Ltmp15:
	movq	%r15, %rdi
	callq	_Z9CharNextAPKc
	movq	%rax, %r15
.Ltmp16:
# BB#12:                                # %.noexc2
                                        #   in Loop: Header=BB1_5 Depth=1
	movb	(%r15), %bl
	testb	%bl, %bl
	jne	.LBB1_5
# BB#13:                                # %._crit_edge.i
	testq	%r13, %r13
	je	.LBB1_16
# BB#14:
	movq	(%r14), %rax
	subq	%rax, %r13
	movslq	8(%r14), %rcx
	movl	%ecx, %edx
	subl	%r13d, %edx
	jle	.LBB1_16
# BB#15:
	movslq	%r13d, %rsi
	movb	(%rax,%rcx), %cl
	movb	%cl, (%rax,%rsi)
	subl	%edx, 8(%r14)
.LBB1_16:                               # %_ZN11CStringBaseIcE20TrimRightWithCharSetERKS0_.exit
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_18
# BB#17:
	callq	_ZdaPv
.LBB1_18:                               # %_ZN11CStringBaseIcE9TrimRightEv.exit
	movq	$0, 24(%rsp)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, 16(%rsp)
	movb	$0, (%rax)
	movl	$4, 28(%rsp)
.Ltmp18:
	leaq	16(%rsp), %rdi
	movl	$32, %esi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp19:
# BB#19:
.Ltmp20:
	leaq	16(%rsp), %rdi
	movl	$10, %esi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp21:
# BB#20:
.Ltmp22:
	leaq	16(%rsp), %rdi
	movl	$9, %esi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp23:
# BB#21:                                # %_ZN11CStringBaseIcE21GetTrimDefaultCharSetEv.exit4
	movq	(%r14), %r15
.LBB1_22:                               # %.noexc12
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_23 Depth 2
	movb	(%r15), %bl
	movq	16(%rsp), %rax
	movb	(%rax), %cl
	cmpb	%bl, %cl
	movq	%rax, %rdx
	je	.LBB1_27
	.p2align	4, 0x90
.LBB1_23:                               # %.lr.ph.i.i.i8
                                        #   Parent Loop BB1_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	%cl, %cl
	je	.LBB1_36
# BB#24:                                #   in Loop: Header=BB1_23 Depth=2
.Ltmp25:
	movq	%rax, %rdi
	callq	_Z9CharNextAPKc
.Ltmp26:
# BB#25:                                # %.noexc11
                                        #   in Loop: Header=BB1_23 Depth=2
	movzbl	(%rax), %ecx
	cmpb	%bl, %cl
	jne	.LBB1_23
# BB#26:                                # %._crit_edge.loopexit.i.i.i6
                                        #   in Loop: Header=BB1_22 Depth=1
	movq	16(%rsp), %rdx
.LBB1_27:                               # %_ZNK11CStringBaseIcE4FindEc.exit.i10
                                        #   in Loop: Header=BB1_22 Depth=1
	cmpl	%edx, %eax
	js	.LBB1_36
# BB#28:                                #   in Loop: Header=BB1_22 Depth=1
	cmpb	$0, (%r15)
	je	.LBB1_36
# BB#29:                                #   in Loop: Header=BB1_22 Depth=1
.Ltmp28:
	movq	%r15, %rdi
	callq	_Z9CharNextAPKc
	movq	%rax, %r15
.Ltmp29:
	jmp	.LBB1_22
.LBB1_36:                               # %.critedge.i
	movq	(%r14), %rdi
	subl	%edi, %r15d
	movl	8(%r14), %eax
	cmpl	%eax, %r15d
	cmovgl	%eax, %r15d
	testl	%r15d, %r15d
	jle	.LBB1_38
# BB#37:
	movslq	%r15d, %rsi
	addq	%rdi, %rsi
	incl	%eax
	subl	%r15d, %eax
	movslq	%eax, %rdx
	callq	memmove
	subl	%r15d, 8(%r14)
.LBB1_38:                               # %_ZN11CStringBaseIcE19TrimLeftWithCharSetERKS0_.exit
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_40
# BB#39:
	callq	_ZdaPv
.LBB1_40:                               # %_ZN11CStringBaseIcE8TrimLeftEv.exit
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB1_42:                               # %.loopexit.split-lp
.Ltmp30:
	jmp	.LBB1_43
.LBB1_35:
.Ltmp24:
	jmp	.LBB1_43
.LBB1_8:
.Ltmp11:
	jmp	.LBB1_34
.LBB1_33:                               # %.loopexit.split-lp15
.Ltmp17:
	jmp	.LBB1_34
.LBB1_41:                               # %.loopexit
.Ltmp27:
.LBB1_43:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_45
	jmp	.LBB1_46
.LBB1_32:                               # %.loopexit14
.Ltmp14:
.LBB1_34:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_46
.LBB1_45:
	callq	_ZdaPv
.LBB1_46:                               # %unwind_resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_ZN11CStringBaseIcE4TrimEv, .Lfunc_end1-_ZN11CStringBaseIcE4TrimEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\367\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp5-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp10-.Ltmp5          #   Call between .Ltmp5 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin1   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp18-.Ltmp16         #   Call between .Ltmp16 and .Ltmp18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp23-.Ltmp18         #   Call between .Ltmp18 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin1   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin1   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin1   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Lfunc_end1-.Ltmp29     #   Call between .Ltmp29 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z11GetPasswordP13CStdOutStreamb
	.p2align	4, 0x90
	.type	_Z11GetPasswordP13CStdOutStreamb,@function
_Z11GetPasswordP13CStdOutStreamb:       # @_Z11GetPasswordP13CStdOutStreamb
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi28:
	.cfi_def_cfa_offset 80
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %r12
	movq	%rdi, %r14
	movl	$.L.str, %esi
	movq	%r12, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
	movq	%r12, %rdi
	callq	_ZN13CStdOutStream5FlushEv
	movl	$.L.str.1, %edi
	callq	getpass
	movq	%rax, %rbp
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$-1, %ebx
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	incl	%ebx
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB2_1
# BB#2:                                 # %_Z11MyStringLenIcEiPKT_.exit.i
	leal	1(%rbx), %eax
	movslq	%eax, %r13
	cmpl	$-1, %ebx
	movq	$-1, %rdi
	cmovgeq	%r13, %rdi
	callq	_Znam
	movq	%rax, (%rsp)
	movb	$0, (%rax)
	movl	%r13d, 12(%rsp)
	.p2align	4, 0x90
.LBB2_3:                                # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp), %ecx
	incq	%rbp
	movb	%cl, (%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB2_3
# BB#4:                                 # %_ZN11CStringBaseIcEC2EPKc.exit
	movl	%ebx, 8(%rsp)
	testb	%r15b, %r15b
	je	.LBB2_18
# BB#5:
.Ltmp31:
	movl	$.L.str.2, %esi
	movq	%r12, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp32:
# BB#6:
.Ltmp33:
	movq	%r12, %rdi
	callq	_ZN13CStdOutStream5FlushEv
.Ltmp34:
# BB#7:
.Ltmp35:
	movl	$.L.str.1, %edi
	callq	getpass
	movq	%rax, %rbp
.Ltmp36:
# BB#8:                                 # %.preheader.preheader
	movl	$-1, %ecx
	xorl	%eax, %eax
	movabsq	$4294967296, %rdx       # imm = 0x100000000
	movq	%rbp, %rsi
	.p2align	4, 0x90
.LBB2_9:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	addq	%rdx, %rax
	incl	%ecx
	cmpb	$0, (%rsi)
	leaq	1(%rsi), %rsi
	jne	.LBB2_9
# BB#10:                                # %_Z11MyStringLenIcEiPKT_.exit.i9
	sarq	$32, %rax
	cmpl	$-1, %ecx
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
.Ltmp37:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp38:
# BB#11:                                # %.noexc
	movb	$0, (%rbx)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_12:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i12
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp,%rax), %ecx
	movb	%cl, (%rbx,%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB2_12
# BB#13:                                # %_ZN11CStringBaseIcEC2EPKc.exit13
	movq	(%rsp), %rdi
.Ltmp39:
	movq	%rbx, %rsi
	callq	_Z15MyStringComparePKcS0_
.Ltmp40:
# BB#14:
	testl	%eax, %eax
	jne	.LBB2_15
# BB#17:                                # %_ZN11CStringBaseIcED2Ev.exit15
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB2_18:
.Ltmp44:
	movq	%rsp, %rsi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp45:
# BB#19:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_21
# BB#20:
	callq	_ZdaPv
.LBB2_21:                               # %_ZN11CStringBaseIcED2Ev.exit16
	movq	%r14, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_15:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.3, (%rax)
.Ltmp41:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp42:
# BB#26:
.LBB2_16:                               # %_ZN11CStringBaseIcED2Ev.exit
.Ltmp43:
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	_ZdaPv
	jmp	.LBB2_23
.LBB2_22:
.Ltmp46:
	movq	%rax, %rbp
.LBB2_23:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_25
# BB#24:
	callq	_ZdaPv
.LBB2_25:                               # %_ZN11CStringBaseIcED2Ev.exit17
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_Z11GetPasswordP13CStdOutStreamb, .Lfunc_end2-_Z11GetPasswordP13CStdOutStreamb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp31-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp38-.Ltmp31         #   Call between .Ltmp31 and .Ltmp38
	.long	.Ltmp46-.Lfunc_begin2   #     jumps to .Ltmp46
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp43-.Lfunc_begin2   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin2   #     jumps to .Ltmp46
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp41-.Ltmp45         #   Call between .Ltmp45 and .Ltmp41
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin2   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Lfunc_end2-.Ltmp42     #   Call between .Ltmp42 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIcEpLEc,"axG",@progbits,_ZN11CStringBaseIcEpLEc,comdat
	.weak	_ZN11CStringBaseIcEpLEc
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcEpLEc,@function
_ZN11CStringBaseIcEpLEc:                # @_ZN11CStringBaseIcEpLEc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 64
.Lcfi42:
	.cfi_offset %rbx, -56
.Lcfi43:
	.cfi_offset %r12, -48
.Lcfi44:
	.cfi_offset %r13, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r13
	movl	8(%r13), %ebp
	movl	12(%r13), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	$1, %eax
	jg	.LBB3_28
# BB#1:
	leal	-1(%rax), %ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB3_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB3_3:                                # %select.end
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rsi,%rbx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB3_28
# BB#4:
	addl	%ebx, %esi
	movslq	%r15d, %rax
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB3_27
# BB#5:                                 # %.preheader.i.i
	movq	(%r13), %rdi
	testl	%ebp, %ebp
	jle	.LBB3_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	movslq	%ebp, %rax
	cmpl	$31, %ebp
	jbe	.LBB3_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB3_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r12
	jae	.LBB3_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB3_17
.LBB3_7:
	xorl	%ecx, %ecx
.LBB3_8:                                # %.lr.ph.i.i.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB3_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB3_10:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r12,%rcx)
	incq	%rcx
	incq	%rbp
	jne	.LBB3_10
.LBB3_11:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB3_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r12,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB3_13:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB3_13
	jmp	.LBB3_26
.LBB3_25:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB3_27
.LBB3_26:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movl	8(%r13), %ebp
.LBB3_27:
	movq	%r12, (%r13)
	movslq	%ebp, %rax
	movb	$0, (%r12,%rax)
	movl	%r15d, 12(%r13)
.LBB3_28:                               # %_ZN11CStringBaseIcE10GrowLengthEi.exit
	movq	(%r13), %rax
	movslq	%ebp, %rcx
	movb	%r14b, (%rax,%rcx)
	movq	(%r13), %rax
	movslq	8(%r13), %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%r13)
	movb	$0, 1(%rax,%rcx)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_17:                               # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB3_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_20:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx), %xmm0
	movups	16(%rdi,%rbx), %xmm1
	movups	%xmm0, (%r12,%rbx)
	movups	%xmm1, 16(%r12,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB3_20
	jmp	.LBB3_21
.LBB3_18:
	xorl	%ebx, %ebx
.LBB3_21:                               # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB3_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB3_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB3_23
.LBB3_24:                               # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB3_8
	jmp	.LBB3_26
.Lfunc_end3:
	.size	_ZN11CStringBaseIcEpLEc, .Lfunc_end3-_ZN11CStringBaseIcEpLEc
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\nEnter password (will not be echoed) :"
	.size	.L.str, 39

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.zero	1
	.size	.L.str.1, 1

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Verify password (will not be echoed) :"
	.size	.L.str.2, 39

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"password verification failed"
	.size	.L.str.3, 29

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"?\n"
	.size	.L.str.4, 3

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"(Y)es / (N)o / (A)lways / (S)kip all / A(u)to rename all / (Q)uit? "
	.size	.L.str.5, 68


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
