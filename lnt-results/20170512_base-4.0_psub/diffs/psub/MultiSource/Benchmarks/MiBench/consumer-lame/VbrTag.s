	.text
	.file	"VbrTag.bc"
	.globl	AddVbrFrame
	.p2align	4, 0x90
	.type	AddVbrFrame,@function
AddVbrFrame:                            # @AddVbrFrame
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movq	pVbrFrames(%rip), %rax
	testq	%rax, %rax
	je	.LBB0_2
# BB#1:
	movl	nVbrFrameBufferSize(%rip), %ecx
	testl	%ecx, %ecx
	jne	.LBB0_3
.LBB0_2:
	movl	$100, nVbrFrameBufferSize(%rip)
	movl	$400, %edi              # imm = 0x190
	callq	malloc
	movq	%rax, pVbrFrames(%rip)
	movl	$100, %ecx
.LBB0_3:
	movl	nVbrNumFrames(%rip), %edx
	cmpl	%ecx, %edx
	jne	.LBB0_5
# BB#4:
	addl	%ecx, %ecx
	movl	%ecx, nVbrFrameBufferSize(%rip)
	movslq	%ecx, %rsi
	shlq	$2, %rsi
	movq	%rax, %rdi
	callq	realloc
	movq	%rax, pVbrFrames(%rip)
	movl	nVbrNumFrames(%rip), %edx
.LBB0_5:
	leal	1(%rdx), %ecx
	movl	%ecx, nVbrNumFrames(%rip)
	movslq	%edx, %rcx
	movl	%ebx, (%rax,%rcx,4)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	AddVbrFrame, .Lfunc_end0-AddVbrFrame
	.cfi_endproc

	.globl	CreateI4
	.p2align	4, 0x90
	.type	CreateI4,@function
CreateI4:                               # @CreateI4
	.cfi_startproc
# BB#0:
	movl	%esi, %eax
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, (%rdi)
	movl	%eax, %ecx
	shrl	$16, %ecx
	movb	%cl, 1(%rdi)
	movb	%ah, 2(%rdi)  # NOREX
	movb	%al, 3(%rdi)
	retq
.Lfunc_end1:
	.size	CreateI4, .Lfunc_end1-CreateI4
	.cfi_endproc

	.globl	CheckVbrTag
	.p2align	4, 0x90
	.type	CheckVbrTag,@function
CheckVbrTag:                            # @CheckVbrTag
	.cfi_startproc
# BB#0:
	movb	3(%rdi), %al
	cmpb	$-64, %al
	sbbq	%rcx, %rcx
	cmpb	$-64, %al
	movl	$36, %eax
	movl	$21, %edx
	cmovbq	%rax, %rdx
	andl	$8, %ecx
	addq	$13, %rcx
	testb	$8, 1(%rdi)
	cmovneq	%rdx, %rcx
	xorl	%eax, %eax
	cmpb	$88, (%rdi,%rcx)
	jne	.LBB2_4
# BB#1:
	cmpb	$105, 1(%rdi,%rcx)
	jne	.LBB2_4
# BB#2:
	cmpb	$110, 2(%rdi,%rcx)
	jne	.LBB2_4
# BB#3:
	xorl	%eax, %eax
	cmpb	$103, 3(%rdi,%rcx)
	sete	%al
.LBB2_4:
	retq
.Lfunc_end2:
	.size	CheckVbrTag, .Lfunc_end2-CheckVbrTag
	.cfi_endproc

	.globl	GetVbrTag
	.p2align	4, 0x90
	.type	GetVbrTag,@function
GetVbrTag:                              # @GetVbrTag
	.cfi_startproc
# BB#0:
	movl	$0, 8(%rdi)
	movb	3(%rsi), %al
	cmpb	$-64, %al
	sbbq	%rdx, %rdx
	cmpb	$-64, %al
	movzbl	1(%rsi), %ecx
	movl	$36, %r8d
	movl	$21, %eax
	cmovbq	%r8, %rax
	shrl	$3, %ecx
	andl	$8, %edx
	addq	$13, %rdx
	andl	$1, %ecx
	cmovneq	%rax, %rdx
	xorl	%eax, %eax
	cmpb	$88, (%rsi,%rdx)
	jne	.LBB3_22
# BB#1:
	addq	%rsi, %rdx
	cmpb	$105, 1(%rdx)
	jne	.LBB3_22
# BB#2:
	cmpb	$110, 2(%rdx)
	jne	.LBB3_22
# BB#3:
	cmpb	$103, 3(%rdx)
	jne	.LBB3_22
# BB#4:
	movzbl	2(%rsi), %eax
	shrl	$2, %eax
	andl	$3, %eax
	movl	%ecx, (%rdi)
	movl	GetVbrTag.sr_table(,%rax,4), %eax
	xorl	$1, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %eax
	movl	%eax, 4(%rdi)
	movzbl	4(%rdx), %eax
	shll	$8, %eax
	movzbl	5(%rdx), %ecx
	orl	%eax, %ecx
	shll	$8, %ecx
	movzbl	6(%rdx), %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	7(%rdx), %r9d
	orl	%r9d, %eax
	movl	%eax, 8(%rdi)
	testb	$1, %r9b
	jne	.LBB3_6
# BB#5:
	addq	$8, %rdx
	testb	$2, %r9b
	jne	.LBB3_8
	jmp	.LBB3_9
.LBB3_6:
	movzbl	8(%rdx), %eax
	shll	$8, %eax
	movzbl	9(%rdx), %ecx
	orl	%eax, %ecx
	shll	$8, %ecx
	movzbl	10(%rdx), %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	11(%rdx), %ecx
	orl	%eax, %ecx
	movl	%ecx, 12(%rdi)
	addq	$12, %rdx
	testb	$2, %r9b
	je	.LBB3_9
.LBB3_8:
	movzbl	(%rdx), %eax
	shll	$8, %eax
	movzbl	1(%rdx), %ecx
	orl	%eax, %ecx
	shll	$8, %ecx
	movzbl	2(%rdx), %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	3(%rdx), %ecx
	orl	%eax, %ecx
	movl	%ecx, 16(%rdi)
	addq	$4, %rdx
.LBB3_9:
	testb	$4, %r9b
	jne	.LBB3_11
# BB#10:
	movq	%rdx, %r8
	jmp	.LBB3_20
.LBB3_11:                               # %vector.memcheck
	leaq	24(%rdi), %rax
	leaq	100(%rdx), %r8
	cmpq	%r8, %rax
	jae	.LBB3_14
# BB#12:                                # %vector.memcheck
	leaq	124(%rdi), %rax
	cmpq	%rax, %rdx
	jae	.LBB3_14
# BB#13:
	xorl	%eax, %eax
	jmp	.LBB3_15
.LBB3_14:                               # %vector.body
	movups	(%rdx), %xmm0
	movups	%xmm0, 24(%rdi)
	movups	16(%rdx), %xmm0
	movups	%xmm0, 40(%rdi)
	movups	32(%rdx), %xmm0
	movups	%xmm0, 56(%rdi)
	movups	48(%rdx), %xmm0
	movups	%xmm0, 72(%rdi)
	movups	64(%rdx), %xmm0
	movups	%xmm0, 88(%rdi)
	movups	80(%rdx), %xmm0
	movups	%xmm0, 104(%rdi)
	movl	$96, %eax
.LBB3_15:                               # %.preheader.prol.preheader
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movl	$99, %r10d
	subq	%rax, %r10
	leaq	24(%rdi,%rax), %r11
	leaq	(%rdx,%rax), %rsi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_16:                               # %.preheader.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi,%rcx), %ebx
	movb	%bl, (%r11,%rcx)
	incq	%rcx
	cmpq	$4, %rcx
	jne	.LBB3_16
# BB#17:                                # %.preheader.prol.loopexit
	cmpq	$7, %r10
	popq	%rbx
	jb	.LBB3_20
# BB#18:                                # %.preheader.preheader.new
	addq	%rcx, %rax
	.p2align	4, 0x90
.LBB3_19:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdx,%rax), %ecx
	movb	%cl, 24(%rdi,%rax)
	movzbl	1(%rdx,%rax), %ecx
	movb	%cl, 25(%rdi,%rax)
	movzbl	2(%rdx,%rax), %ecx
	movb	%cl, 26(%rdi,%rax)
	movzbl	3(%rdx,%rax), %ecx
	movb	%cl, 27(%rdi,%rax)
	movzbl	4(%rdx,%rax), %ecx
	movb	%cl, 28(%rdi,%rax)
	movzbl	5(%rdx,%rax), %ecx
	movb	%cl, 29(%rdi,%rax)
	movzbl	6(%rdx,%rax), %ecx
	movb	%cl, 30(%rdi,%rax)
	movzbl	7(%rdx,%rax), %ecx
	movb	%cl, 31(%rdi,%rax)
	addq	$8, %rax
	cmpq	$100, %rax
	jne	.LBB3_19
.LBB3_20:
	movl	$-1, 20(%rdi)
	movl	$1, %eax
	testb	$8, %r9b
	je	.LBB3_22
# BB#21:
	movzbl	(%r8), %ecx
	shll	$8, %ecx
	movzbl	1(%r8), %edx
	orl	%ecx, %edx
	shll	$8, %edx
	movzbl	2(%r8), %ecx
	orl	%edx, %ecx
	shll	$8, %ecx
	movzbl	3(%r8), %edx
	orl	%ecx, %edx
	movl	%edx, 20(%rdi)
.LBB3_22:
	retq
.Lfunc_end3:
	.size	GetVbrTag, .Lfunc_end3-GetVbrTag
	.cfi_endproc

	.globl	InitVbrTag
	.p2align	4, 0x90
	.type	InitVbrTag,@function
InitVbrTag:                             # @InitVbrTag
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi4:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 32
.Lcfi7:
	.cfi_offset %rbx, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	$0, pVbrFrames(%rip)
	movl	$0, nVbrNumFrames(%rip)
	movl	$0, nVbrFrameBufferSize(%rip)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, pbtStreamBuffer+192(%rip)
	movaps	%xmm0, pbtStreamBuffer+176(%rip)
	movaps	%xmm0, pbtStreamBuffer+160(%rip)
	movaps	%xmm0, pbtStreamBuffer+144(%rip)
	movaps	%xmm0, pbtStreamBuffer+128(%rip)
	movaps	%xmm0, pbtStreamBuffer+112(%rip)
	movaps	%xmm0, pbtStreamBuffer+96(%rip)
	movaps	%xmm0, pbtStreamBuffer+80(%rip)
	movaps	%xmm0, pbtStreamBuffer+64(%rip)
	movaps	%xmm0, pbtStreamBuffer+48(%rip)
	movaps	%xmm0, pbtStreamBuffer+32(%rip)
	movaps	%xmm0, pbtStreamBuffer+16(%rip)
	movaps	%xmm0, pbtStreamBuffer(%rip)
	movq	$0, pbtStreamBuffer+208(%rip)
	xorl	%eax, %eax
	cmpl	$3, %edx
	sete	%al
	movslq	%esi, %rdx
	shlq	$2, %rax
	movl	SizeOfEmptyFrame(%rax,%rdx,8), %eax
	leal	4(%rax), %edx
	movl	%edx, nZeroStreamSize(%rip)
	cmpl	$3, %ecx
	jge	.LBB4_1
# BB#3:
	movslq	%ecx, %rcx
	movl	InitVbrTag.framesize(,%rcx,4), %ecx
	movl	%ecx, TotalFrameSize(%rip)
	addl	$144, %eax
	cmpl	%eax, %ecx
	jl	.LBB4_7
# BB#4:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	movl	$8, %edx
	movq	%rbx, %rdi
	callq	putbits
	incl	%ebp
	cmpl	TotalFrameSize(%rip), %ebp
	jl	.LBB4_5
# BB#6:                                 # %._crit_edge
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB4_1:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	jmp	.LBB4_2
.LBB4_7:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
.LBB4_2:
	movl	$33, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$-1, %edi
	callq	exit
.Lfunc_end4:
	.size	InitVbrTag, .Lfunc_end4-InitVbrTag
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4576918229304087675     # double 0.01
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_1:
	.long	1132462080              # float 256
.LCPI5_2:
	.long	1132396544              # float 255
	.text
	.globl	PutVbrTag
	.p2align	4, 0x90
	.type	PutVbrTag,@function
PutVbrTag:                              # @PutVbrTag
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 272
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movl	%esi, %ebx
	cmpl	$0, nVbrNumFrames(%rip)
	movl	$-1, %r14d
	je	.LBB5_12
# BB#1:
	movq	pVbrFrames(%rip), %rax
	testq	%rax, %rax
	je	.LBB5_12
# BB#2:
	movl	$.L.str.2, %esi
	callq	fopen
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB5_12
# BB#3:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, pbtStreamBuffer+192(%rip)
	movaps	%xmm0, pbtStreamBuffer+176(%rip)
	movaps	%xmm0, pbtStreamBuffer+160(%rip)
	movaps	%xmm0, pbtStreamBuffer+144(%rip)
	movaps	%xmm0, pbtStreamBuffer+128(%rip)
	movaps	%xmm0, pbtStreamBuffer+112(%rip)
	movaps	%xmm0, pbtStreamBuffer+96(%rip)
	movaps	%xmm0, pbtStreamBuffer+80(%rip)
	movaps	%xmm0, pbtStreamBuffer+64(%rip)
	movaps	%xmm0, pbtStreamBuffer+48(%rip)
	movaps	%xmm0, pbtStreamBuffer+32(%rip)
	movaps	%xmm0, pbtStreamBuffer+16(%rip)
	movaps	%xmm0, pbtStreamBuffer(%rip)
	movq	$0, pbtStreamBuffer+208(%rip)
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%r15, %rdi
	callq	fseek
	movq	%r15, %rdi
	callq	ftell
	movq	%rax, %r13
	testq	%rax, %rax
	je	.LBB5_12
# BB#4:
	movl	%ebx, (%rsp)            # 4-byte Spill
	movslq	TotalFrameSize(%rip), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	fseek
	movl	$1, %ebx
	movl	$pbtStreamBuffer, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fread
	movb	$-1, pbtStreamBuffer(%rip)
	testl	%ebp, %ebp
	movb	$-5, %al
	je	.LBB5_6
# BB#5:
	movb	$-13, %al
.LBB5_6:
	movb	$80, %cl
	je	.LBB5_8
# BB#7:
	movb	$-128, %cl
.LBB5_8:
	movb	%al, pbtStreamBuffer+1(%rip)
	movb	pbtStreamBuffer+2(%rip), %al
	andb	$12, %al
	orb	%cl, %al
	movb	%al, pbtStreamBuffer+2(%rip)
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	fseek
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 96(%rsp)
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movl	$0, 112(%rsp)
	movl	nVbrNumFrames(%rip), %eax
	movl	%eax, %r12d
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movq	pVbrFrames(%rip), %rbp
	movq	%r13, %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB5_9:                                # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	mulsd	.LCPI5_0(%rip), %xmm0
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	callq	floor
	cvttsd2si	%xmm0, %eax
	cltq
	xorps	%xmm0, %xmm0
	cvtsi2ssl	(%rbp,%rax,4), %xmm0
	mulss	.LCPI5_1(%rip), %xmm0
	divss	4(%rsp), %xmm0          # 4-byte Folded Reload
	movss	.LCPI5_2(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	minss	%xmm0, %xmm1
	cvttss2si	%xmm1, %eax
	movb	%al, 16(%rsp,%rbx)
	incq	%rbx
	cmpq	$100, %rbx
	jne	.LBB5_9
# BB#10:
	movslq	nZeroStreamSize(%rip), %rax
	movb	$88, pbtStreamBuffer(%rax)
	movb	$105, pbtStreamBuffer+1(%rax)
	movb	$110, pbtStreamBuffer+2(%rax)
	movb	$103, pbtStreamBuffer+3(%rax)
	movb	$0, pbtStreamBuffer+4(%rax)
	movb	$0, pbtStreamBuffer+5(%rax)
	movb	$0, pbtStreamBuffer+6(%rax)
	movb	$15, pbtStreamBuffer+7(%rax)
	movl	%r12d, %edx
	movl	%edx, %ecx
	shrl	$24, %ecx
	movb	%cl, pbtStreamBuffer+8(%rax)
	movl	%edx, %ecx
	shrl	$16, %ecx
	movb	%cl, pbtStreamBuffer+9(%rax)
	movb	%dh, pbtStreamBuffer+10(%rax)  # NOREX
	movb	%dl, pbtStreamBuffer+11(%rax)
	movq	%r13, %rdx
	movl	%edx, %ecx
	shrl	$24, %ecx
	movb	%cl, pbtStreamBuffer+12(%rax)
	movl	%edx, %ecx
	shrl	$16, %ecx
	movb	%cl, pbtStreamBuffer+13(%rax)
	movb	%dh, pbtStreamBuffer+14(%rax)  # NOREX
	movb	%dl, pbtStreamBuffer+15(%rax)
	movl	112(%rsp), %ecx
	movl	%ecx, pbtStreamBuffer+112(%rax)
	movaps	96(%rsp), %xmm0
	movups	%xmm0, pbtStreamBuffer+96(%rax)
	movaps	80(%rsp), %xmm0
	movups	%xmm0, pbtStreamBuffer+80(%rax)
	movaps	16(%rsp), %xmm0
	movaps	32(%rsp), %xmm1
	movaps	48(%rsp), %xmm2
	movaps	64(%rsp), %xmm3
	movups	%xmm3, pbtStreamBuffer+64(%rax)
	movups	%xmm2, pbtStreamBuffer+48(%rax)
	movups	%xmm1, pbtStreamBuffer+32(%rax)
	movups	%xmm0, pbtStreamBuffer+16(%rax)
	leal	116(%rax), %ecx
	movslq	%ecx, %rcx
	movl	(%rsp), %ebx            # 4-byte Reload
	movl	%ebx, %edx
	shrl	$24, %edx
	movb	%dl, pbtStreamBuffer(%rcx)
	movl	%ebx, %edx
	shrl	$16, %edx
	movb	%dl, pbtStreamBuffer+1(%rcx)
	movb	%bh, pbtStreamBuffer+2(%rcx)  # NOREX
	movb	%bl, pbtStreamBuffer+3(%rcx)
	leal	120(%rax), %ebp
	callq	get_lame_version
	movq	%rax, %rcx
	leaq	128(%rsp), %rbx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	sprintf
	movslq	%ebp, %rax
	leaq	pbtStreamBuffer(%rax), %rdi
	movl	$20, %edx
	movq	%rbx, %rsi
	callq	strncpy
	movslq	TotalFrameSize(%rip), %rsi
	movl	$pbtStreamBuffer, %edi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	cmpq	$1, %rax
	jne	.LBB5_12
# BB#11:
	movq	%r15, %rdi
	callq	fclose
	movq	pVbrFrames(%rip), %rdi
	callq	free
	movq	$0, pVbrFrames(%rip)
	xorl	%r14d, %r14d
.LBB5_12:
	movl	%r14d, %eax
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	PutVbrTag, .Lfunc_end5-PutVbrTag
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_0:
	.long	1120403456              # float 100
.LCPI6_1:
	.long	1132462080              # float 256
.LCPI6_2:
	.long	998244352               # float 0.00390625
	.text
	.globl	SeekPoint
	.p2align	4, 0x90
	.type	SeekPoint,@function
SeekPoint:                              # @SeekPoint
	.cfi_startproc
# BB#0:
	xorps	%xmm1, %xmm1
	maxss	%xmm0, %xmm1
	movss	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	minss	%xmm1, %xmm0
	cvttss2si	%xmm0, %edx
	cmpl	$100, %edx
	movl	$99, %eax
	cmovll	%edx, %eax
	movslq	%eax, %r8
	movzbl	(%rdi,%r8), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ecx, %xmm1
	cmpl	$98, %edx
	jg	.LBB6_1
# BB#2:
	movzbl	1(%rdi,%r8), %ecx
	cvtsi2ssl	%ecx, %xmm2
	jmp	.LBB6_3
.LBB6_1:
	movss	.LCPI6_1(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
.LBB6_3:
	subss	%xmm1, %xmm2
	cvtsi2ssl	%eax, %xmm3
	subss	%xmm3, %xmm0
	mulss	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	mulss	.LCPI6_2(%rip), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%esi, %xmm1
	mulss	%xmm0, %xmm1
	cvttss2si	%xmm1, %eax
	retq
.Lfunc_end6:
	.size	SeekPoint, .Lfunc_end6-SeekPoint
	.cfi_endproc

	.type	SizeOfEmptyFrame,@object # @SizeOfEmptyFrame
	.data
	.globl	SizeOfEmptyFrame
	.p2align	4
SizeOfEmptyFrame:
	.long	32                      # 0x20
	.long	17                      # 0x11
	.long	17                      # 0x11
	.long	9                       # 0x9
	.size	SizeOfEmptyFrame, 16

	.type	pVbrFrames,@object      # @pVbrFrames
	.bss
	.globl	pVbrFrames
	.p2align	3
pVbrFrames:
	.quad	0
	.size	pVbrFrames, 8

	.type	nVbrNumFrames,@object   # @nVbrNumFrames
	.globl	nVbrNumFrames
	.p2align	2
nVbrNumFrames:
	.long	0                       # 0x0
	.size	nVbrNumFrames, 4

	.type	nVbrFrameBufferSize,@object # @nVbrFrameBufferSize
	.globl	nVbrFrameBufferSize
	.p2align	2
nVbrFrameBufferSize:
	.long	0                       # 0x0
	.size	nVbrFrameBufferSize, 4

	.type	GetVbrTag.sr_table,@object # @GetVbrTag.sr_table
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
GetVbrTag.sr_table:
	.long	44100                   # 0xac44
	.long	48000                   # 0xbb80
	.long	32000                   # 0x7d00
	.long	99999                   # 0x1869f
	.size	GetVbrTag.sr_table, 16

	.type	pbtStreamBuffer,@object # @pbtStreamBuffer
	.local	pbtStreamBuffer
	.comm	pbtStreamBuffer,216,16
	.type	nZeroStreamSize,@object # @nZeroStreamSize
	.local	nZeroStreamSize
	.comm	nZeroStreamSize,4,4
	.type	InitVbrTag.framesize,@object # @InitVbrTag.framesize
	.section	.rodata,"a",@progbits
	.p2align	2
InitVbrTag.framesize:
	.long	208                     # 0xd0
	.long	192                     # 0xc0
	.long	288                     # 0x120
	.size	InitVbrTag.framesize, 12

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"illegal sampling frequency index\n"
	.size	.L.str, 34

	.type	TotalFrameSize,@object  # @TotalFrameSize
	.local	TotalFrameSize
	.comm	TotalFrameSize,4,4
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Xing VBR header problem...use -t\n"
	.size	.L.str.1, 34

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"rb+"
	.size	.L.str.2, 4

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"LAME%s"
	.size	.L.str.3, 7


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
