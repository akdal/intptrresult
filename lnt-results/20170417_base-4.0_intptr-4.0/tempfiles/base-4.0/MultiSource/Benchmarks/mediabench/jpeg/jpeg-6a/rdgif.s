	.text
	.file	"rdgif.bc"
	.globl	jinit_read_gif
	.p2align	4, 0x90
	.type	jinit_read_gif,@function
jinit_read_gif:                         # @jinit_read_gif
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$440, %edx              # imm = 0x1B8
	callq	*(%rax)
	movq	%rbx, 48(%rax)
	movq	$start_input_gif, (%rax)
	movq	$finish_input_gif, 16(%rax)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	jinit_read_gif, .Lfunc_end0-jinit_read_gif
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_input_gif,@function
start_input_gif:                        # @start_input_gif
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi8:
	.cfi_def_cfa_offset 352
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	8(%r14), %rax
	movl	$1, %esi
	movl	$256, %edx              # imm = 0x100
	movl	$3, %ecx
	callq	*16(%rax)
	movq	%rax, 56(%rbx)
	movq	24(%rbx), %rcx
	leaq	14(%rsp), %rdi
	movl	$1, %esi
	movl	$6, %edx
	callq	fread
	cmpq	$6, %rax
	je	.LBB1_2
# BB#1:
	movq	(%r14), %rax
	movl	$1016, 40(%rax)         # imm = 0x3F8
	movq	%r14, %rdi
	callq	*(%rax)
.LBB1_2:
	cmpb	$71, 14(%rsp)
	jne	.LBB1_5
# BB#3:
	cmpb	$73, 15(%rsp)
	jne	.LBB1_5
# BB#4:
	cmpb	$70, 16(%rsp)
	je	.LBB1_6
.LBB1_5:
	movq	(%r14), %rax
	movl	$1016, 40(%rax)         # imm = 0x3F8
	movq	%r14, %rdi
	callq	*(%rax)
.LBB1_6:
	movsbl	17(%rsp), %eax
	cmpl	$56, %eax
	movb	18(%rsp), %cl
	jne	.LBB1_12
# BB#7:
	cmpb	$57, %cl
	je	.LBB1_10
# BB#8:
	cmpb	$55, %cl
	jne	.LBB1_12
# BB#9:
	movb	$55, %cl
	cmpb	$97, 19(%rsp)
	jne	.LBB1_12
	jmp	.LBB1_13
.LBB1_10:
	movb	$57, %cl
	cmpb	$97, 19(%rsp)
	je	.LBB1_13
.LBB1_12:                               # %.thread
	movq	(%r14), %rdx
	movl	%eax, 44(%rdx)
	movsbl	%cl, %eax
	movl	%eax, 48(%rdx)
	movsbl	19(%rsp), %eax
	movl	%eax, 52(%rdx)
	movl	$1018, 40(%rdx)         # imm = 0x3FA
	movl	$1, %esi
	movq	%r14, %rdi
	callq	*8(%rdx)
.LBB1_13:
	movq	24(%rbx), %rcx
	leaq	14(%rsp), %rdi
	movl	$1, %esi
	movl	$7, %edx
	callq	fread
	cmpq	$7, %rax
	je	.LBB1_15
# BB#14:
	movq	(%r14), %rax
	movl	$42, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB1_15:
	movb	18(%rsp), %bpl
	movb	20(%rsp), %al
	movl	%ebp, %ecx
	andb	$7, %cl
	movl	$2, %r15d
	shll	%cl, %r15d
	testb	%al, %al
	je	.LBB1_18
# BB#16:
	cmpb	$49, %al
	je	.LBB1_18
# BB#17:
	movq	(%r14), %rax
	movl	$1020, 40(%rax)         # imm = 0x3FC
	movl	$1, %esi
	movq	%r14, %rdi
	callq	*8(%rax)
.LBB1_18:
	testb	%bpl, %bpl
	jns	.LBB1_20
# BB#19:
	movq	56(%rbx), %rdx
	movq	%rbx, %rdi
	movl	%r15d, %esi
	callq	ReadColorMap
.LBB1_20:                               # %.preheader
	leaq	32(%rsp), %r12
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_43:                               #   in Loop: Header=BB1_21 Depth=1
	movq	(%r14), %rax
	movl	$1022, 40(%rax)         # imm = 0x3FE
	movl	%ebp, 44(%rax)
	movl	$-1, %esi
	movq	%r14, %rdi
	callq	*8(%rax)
.LBB1_21:                               # %.backedge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_27 Depth 2
	movq	24(%rbx), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$43, %ebp
	jg	.LBB1_29
# BB#22:                                # %.backedge
                                        #   in Loop: Header=BB1_21 Depth=1
	cmpl	$-1, %ebp
	je	.LBB1_41
# BB#23:                                # %.backedge
                                        #   in Loop: Header=BB1_21 Depth=1
	cmpl	$33, %ebp
	jne	.LBB1_43
# BB#24:                                #   in Loop: Header=BB1_21 Depth=1
	movq	24(%rbx), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB1_26
# BB#25:                                #   in Loop: Header=BB1_21 Depth=1
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB1_26:                               # %ReadByte.exit.i
                                        #   in Loop: Header=BB1_21 Depth=1
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$1019, 40(%rax)         # imm = 0x3FB
	movl	%ebp, 44(%rax)
	movl	$1, %esi
	callq	*8(%rax)
	jmp	.LBB1_27
	.p2align	4, 0x90
.LBB1_40:                               #   in Loop: Header=BB1_27 Depth=2
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB1_27:                               # %ReadByte.exit.i
                                        #   Parent Loop BB1_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rbx), %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB1_28
# BB#38:                                # %ReadByte.exit.i.i.i
                                        #   in Loop: Header=BB1_27 Depth=2
	testl	%eax, %eax
	jle	.LBB1_21
# BB#39:                                #   in Loop: Header=BB1_27 Depth=2
	movslq	%eax, %rbp
	movq	24(%rbx), %rcx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rbp, %rdx
	callq	fread
	cmpq	%rbp, %rax
	je	.LBB1_27
	jmp	.LBB1_40
	.p2align	4, 0x90
.LBB1_29:                               # %.backedge
                                        #   in Loop: Header=BB1_21 Depth=1
	cmpl	$59, %ebp
	jne	.LBB1_30
# BB#37:                                #   in Loop: Header=BB1_21 Depth=1
	movq	(%r14), %rax
	movl	$1015, 40(%rax)         # imm = 0x3F7
	movq	%r14, %rdi
	jmp	.LBB1_42
	.p2align	4, 0x90
.LBB1_41:                               # %ReadByte.exit.thread
                                        #   in Loop: Header=BB1_21 Depth=1
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
.LBB1_42:                               #   in Loop: Header=BB1_21 Depth=1
	callq	*(%rax)
	jmp	.LBB1_43
.LBB1_28:                               # %ReadByte.exit.thread.i.i.i
                                        #   in Loop: Header=BB1_21 Depth=1
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
	jmp	.LBB1_21
.LBB1_30:                               # %.backedge
                                        #   in Loop: Header=BB1_21 Depth=1
	cmpl	$44, %ebp
	jne	.LBB1_43
# BB#31:
	movq	24(%rbx), %rcx
	leaq	14(%rsp), %rdi
	movl	$1, %esi
	movl	$9, %edx
	callq	fread
	cmpq	$9, %rax
	je	.LBB1_33
# BB#32:
	movq	(%r14), %rax
	movl	$42, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB1_33:
	movzbl	19(%rsp), %r13d
	movzbl	18(%rsp), %ebp
	movzbl	21(%rsp), %r12d
	movzbl	20(%rsp), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movzbl	22(%rsp), %ecx
	movl	%ecx, %eax
	andl	$64, %eax
	movl	%eax, 408(%rbx)
	testb	%cl, %cl
	jns	.LBB1_35
# BB#34:
	andb	$7, %cl
	movl	$2, %r15d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r15d
	movq	56(%rbx), %rdx
	movq	%rbx, %rdi
	movl	%r15d, %esi
	callq	ReadColorMap
.LBB1_35:
	shll	$8, %r13d
	shll	$8, %r12d
	movq	24(%rbx), %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	movl	%r15d, 24(%rsp)         # 4-byte Spill
	je	.LBB1_36
# BB#44:                                # %ReadByte.exit121
	leaq	340(%rbx), %r15
	movl	%eax, 340(%rbx)
	movl	%eax, %ecx
	addl	$-2, %ecx
	cmpl	$10, %ecx
	jae	.LBB1_45
	jmp	.LBB1_46
.LBB1_36:                               # %ReadByte.exit121.thread
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
	leaq	340(%rbx), %r15
	movl	$-1, 340(%rbx)
	movl	$-1, %eax
.LBB1_45:
	movq	(%r14), %rcx
	movl	$1013, 40(%rcx)         # imm = 0x3F5
	movl	%eax, 44(%rcx)
	movq	%r14, %rdi
	callq	*(%rcx)
.LBB1_46:
	orl	%ebp, %r13d
	addl	28(%rsp), %r12d         # 4-byte Folded Reload
	movq	8(%r14), %rax
	movl	$1, %ebp
	movl	$1, %esi
	movl	$8192, %edx             # imm = 0x2000
	movq	%r14, %rdi
	callq	*8(%rax)
	movq	%rax, 376(%rbx)
	movq	8(%r14), %rax
	movl	$1, %esi
	movl	$4096, %edx             # imm = 0x1000
	movq	%r14, %rdi
	callq	*8(%rax)
	movq	%rax, 384(%rbx)
	movq	8(%r14), %rax
	movl	$1, %esi
	movl	$4096, %edx             # imm = 0x1000
	movq	%r14, %rdi
	callq	*8(%rax)
	movq	%rax, 392(%rbx)
	movl	$2, %ecx
	movd	%ecx, %xmm0
	movdqu	%xmm0, 324(%rbx)
	movl	(%r15), %ecx
	shll	%cl, %ebp
	movl	%ebp, 344(%rbx)
	leal	1(%rbp), %edx
	movl	%edx, 348(%rbx)
	movl	$1, 364(%rbx)
	incl	%ecx
	movl	%ecx, 352(%rbx)
	leal	(%rbp,%rbp), %ecx
	movl	%ecx, 356(%rbx)
	addl	$2, %ebp
	movl	%ebp, 360(%rbx)
	movq	%rax, 400(%rbx)
	cmpl	$0, 408(%rbx)
	je	.LBB1_47
# BB#48:
	movq	8(%r14), %rax
	movl	$1, %esi
	xorl	%edx, %edx
	movl	$1, %r9d
	movq	%r14, %rdi
	movl	%r13d, %ecx
	movl	%r12d, %r8d
	callq	*32(%rax)
	movq	%rax, 416(%rbx)
	movq	16(%r14), %rax
	testq	%rax, %rax
	je	.LBB1_50
# BB#49:
	incl	36(%rax)
.LBB1_50:
	movl	$load_interlaced_image, %eax
	jmp	.LBB1_51
.LBB1_47:
	movl	$get_pixel_rows, %eax
.LBB1_51:
	movl	24(%rsp), %ebp          # 4-byte Reload
	movq	%rax, 8(%rbx)
	movq	8(%r14), %rax
	leal	(%r13,%r13,2), %edx
	movl	$1, %esi
	movl	$1, %ecx
	movq	%r14, %rdi
	callq	*16(%rax)
	movq	%rax, 32(%rbx)
	movl	$1, 40(%rbx)
	movl	$2, 52(%r14)
	movl	$3, 48(%r14)
	movl	$8, 64(%r14)
	movl	%r13d, 40(%r14)
	movl	%r12d, 44(%r14)
	movq	(%r14), %rax
	movl	%r13d, 44(%rax)
	movl	%r12d, 48(%rax)
	movl	%ebp, 52(%rax)
	movl	$1017, 40(%rax)         # imm = 0x3F9
	movl	$1, %esi
	movq	%r14, %rdi
	callq	*8(%rax)
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	start_input_gif, .Lfunc_end1-start_input_gif
	.cfi_endproc

	.p2align	4, 0x90
	.type	finish_input_gif,@function
finish_input_gif:                       # @finish_input_gif
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end2:
	.size	finish_input_gif, .Lfunc_end2-finish_input_gif
	.cfi_endproc

	.p2align	4, 0x90
	.type	ReadColorMap,@function
ReadColorMap:                           # @ReadColorMap
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 48
.Lcfi20:
	.cfi_offset %rbx, -48
.Lcfi21:
	.cfi_offset %r12, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rdi, %r12
	testl	%esi, %esi
	jle	.LBB3_9
# BB#1:                                 # %.lr.ph
	movl	%esi, %r15d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movq	24(%r12), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB3_4
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	48(%r12), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB3_4:                                # %ReadByte.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	(%r14), %rax
	movb	%bpl, (%rax,%rbx)
	movq	24(%r12), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	48(%r12), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB3_6:                                # %ReadByte.exit12
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	8(%r14), %rax
	movb	%bpl, (%rax,%rbx)
	movq	24(%r12), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB3_8
# BB#7:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	48(%r12), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB3_8:                                # %ReadByte.exit13
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	16(%r14), %rax
	movb	%bpl, (%rax,%rbx)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB3_2
.LBB3_9:                                # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	ReadColorMap, .Lfunc_end3-ReadColorMap
	.cfi_endproc

	.p2align	4, 0x90
	.type	load_interlaced_image,@function
load_interlaced_image:                  # @load_interlaced_image
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 64
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	movq	16(%r14), %r15
	movl	44(%r14), %eax
	testl	%eax, %eax
	je	.LBB4_1
# BB#2:                                 # %.lr.ph52
	xorl	%r12d, %r12d
	testq	%r15, %r15
	je	.LBB4_3
	.p2align	4, 0x90
.LBB4_7:                                # %.lr.ph52.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_9 Depth 2
	movl	%r12d, %ecx
	movq	%rcx, 8(%r15)
	movl	%eax, %eax
	movq	%rax, 16(%r15)
	movq	%r14, %rdi
	callq	*(%r15)
	movq	8(%r14), %rax
	movq	416(%r13), %rsi
	movl	$1, %ecx
	movl	$1, %r8d
	movq	%r14, %rdi
	movl	%r12d, %edx
	callq	*56(%rax)
	movl	40(%r14), %ebp
	testl	%ebp, %ebp
	je	.LBB4_10
# BB#8:                                 # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB4_7 Depth=1
	movq	(%rax), %rbx
	.p2align	4, 0x90
.LBB4_9:                                # %.lr.ph.us
                                        #   Parent Loop BB4_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	LZWReadByte
	movb	%al, (%rbx)
	incq	%rbx
	decl	%ebp
	jne	.LBB4_9
.LBB4_10:                               # %._crit_edge.us
                                        #   in Loop: Header=BB4_7 Depth=1
	incl	%r12d
	movl	44(%r14), %eax
	cmpl	%eax, %r12d
	jb	.LBB4_7
	jmp	.LBB4_11
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph52.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_5 Depth 2
	movq	8(%r14), %rax
	movq	416(%r13), %rsi
	movl	$1, %ecx
	movl	$1, %r8d
	movq	%r14, %rdi
	movl	%r12d, %edx
	callq	*56(%rax)
	movl	40(%r14), %ebp
	testl	%ebp, %ebp
	je	.LBB4_6
# BB#4:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	(%rax), %rbx
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	LZWReadByte
	movb	%al, (%rbx)
	incq	%rbx
	decl	%ebp
	jne	.LBB4_5
.LBB4_6:                                # %._crit_edge
                                        #   in Loop: Header=BB4_3 Depth=1
	incl	%r12d
	movl	44(%r14), %eax
	cmpl	%eax, %r12d
	jb	.LBB4_3
	jmp	.LBB4_11
.LBB4_1:
	xorl	%eax, %eax
.LBB4_11:                               # %._crit_edge53
	testq	%r15, %r15
	je	.LBB4_13
# BB#12:
	incl	32(%r15)
.LBB4_13:
	movq	$get_interlaced_row, 8(%r13)
	movl	$0, 424(%r13)
	leal	7(%rax), %ecx
	shrl	$3, %ecx
	movl	%ecx, 428(%r13)
	leal	3(%rax), %edx
	shrl	$3, %edx
	addl	%ecx, %edx
	movl	%edx, 432(%r13)
	incl	%eax
	shrl	$2, %eax
	addl	%edx, %eax
	movl	%eax, 436(%r13)
	movq	56(%r13), %rbp
	movq	416(%r13), %rsi
	movq	8(%r14), %rax
	xorl	%edx, %edx
	movl	$1, %ecx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	callq	*56(%rax)
	movl	40(%r14), %esi
	testl	%esi, %esi
	je	.LBB4_19
# BB#14:                                # %.lr.ph.i
	movq	32(%r13), %rcx
	movq	(%rcx), %rcx
	movq	(%rax), %rax
	testb	$1, %sil
	jne	.LBB4_16
# BB#15:
	movl	%esi, %edx
	cmpl	$1, %esi
	jne	.LBB4_18
	jmp	.LBB4_19
.LBB4_16:
	movq	(%rbp), %rdx
	movzbl	(%rax), %edi
	incq	%rax
	movb	(%rdx,%rdi), %dl
	movb	%dl, (%rcx)
	movq	8(%rbp), %rdx
	movb	(%rdx,%rdi), %dl
	movb	%dl, 1(%rcx)
	movq	16(%rbp), %rdx
	movb	(%rdx,%rdi), %dl
	movb	%dl, 2(%rcx)
	addq	$3, %rcx
	leal	-1(%rsi), %edx
	cmpl	$1, %esi
	je	.LBB4_19
	.p2align	4, 0x90
.LBB4_18:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rsi
	movzbl	(%rax), %edi
	movzbl	(%rsi,%rdi), %ebx
	movb	%bl, (%rcx)
	movq	8(%rbp), %rsi
	movzbl	(%rsi,%rdi), %ebx
	movb	%bl, 1(%rcx)
	movq	16(%rbp), %rsi
	movzbl	(%rsi,%rdi), %ebx
	movb	%bl, 2(%rcx)
	movq	(%rbp), %rsi
	movzbl	1(%rax), %edi
	movzbl	(%rsi,%rdi), %ebx
	movb	%bl, 3(%rcx)
	movq	8(%rbp), %rsi
	movzbl	(%rsi,%rdi), %ebx
	movb	%bl, 4(%rcx)
	movq	16(%rbp), %rsi
	movzbl	(%rsi,%rdi), %ebx
	movb	%bl, 5(%rcx)
	addq	$2, %rax
	addq	$6, %rcx
	addl	$-2, %edx
	jne	.LBB4_18
.LBB4_19:                               # %get_interlaced_row.exit
	incl	424(%r13)
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	load_interlaced_image, .Lfunc_end4-load_interlaced_image
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_pixel_rows,@function
get_pixel_rows:                         # @get_pixel_rows
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 48
.Lcfi43:
	.cfi_offset %rbx, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	40(%rdi), %ebp
	testl	%ebp, %ebp
	je	.LBB5_3
# BB#1:                                 # %.lr.ph
	movq	56(%r14), %r15
	movq	32(%r14), %rax
	movq	(%rax), %rbx
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	LZWReadByte
	movq	(%r15), %rcx
	cltq
	movzbl	(%rcx,%rax), %ecx
	movb	%cl, (%rbx)
	movq	8(%r15), %rcx
	movzbl	(%rcx,%rax), %ecx
	movb	%cl, 1(%rbx)
	movq	16(%r15), %rcx
	movzbl	(%rcx,%rax), %eax
	movb	%al, 2(%rbx)
	addq	$3, %rbx
	decl	%ebp
	jne	.LBB5_2
.LBB5_3:                                # %._crit_edge
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	get_pixel_rows, .Lfunc_end5-get_pixel_rows
	.cfi_endproc

	.p2align	4, 0x90
	.type	LZWReadByte,@function
LZWReadByte:                            # @LZWReadByte
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 40
	subq	$264, %rsp              # imm = 0x108
.Lcfi51:
	.cfi_def_cfa_offset 304
.Lcfi52:
	.cfi_offset %rbx, -40
.Lcfi53:
	.cfi_offset %r12, -32
.Lcfi54:
	.cfi_offset %r14, -24
.Lcfi55:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	cmpl	$0, 364(%rbx)
	je	.LBB6_2
# BB#1:                                 # %.thread
	movl	$0, 364(%rbx)
.LBB6_5:
	movl	340(%rbx), %eax
	incl	%eax
	movl	%eax, 352(%rbx)
	movl	344(%rbx), %eax
	leal	(%rax,%rax), %ecx
	movl	%ecx, 356(%rbx)
	addl	$2, %eax
	movl	%eax, 360(%rbx)
	movq	392(%rbx), %rax
	movq	%rax, 400(%rbx)
	.p2align	4, 0x90
.LBB6_6:                                # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	GetCode
	cmpl	344(%rbx), %eax
	je	.LBB6_6
# BB#7:
	jle	.LBB6_9
# BB#8:
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$1021, 40(%rax)         # imm = 0x3FD
	movl	$-1, %esi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB6_9:
	movl	%eax, 368(%rbx)
	movl	%eax, 372(%rbx)
	jmp	.LBB6_33
.LBB6_2:
	movq	400(%rbx), %rax
	cmpq	392(%rbx), %rax
	jbe	.LBB6_4
# BB#3:
	leaq	-1(%rax), %rcx
	movq	%rcx, 400(%rbx)
	movzbl	-1(%rax), %eax
.LBB6_33:
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB6_4:
	movq	%rbx, %rdi
	callq	GetCode
	cmpl	344(%rbx), %eax
	je	.LBB6_5
# BB#10:
	cmpl	348(%rbx), %eax
	jne	.LBB6_21
# BB#11:
	cmpl	$0, 336(%rbx)
	je	.LBB6_13
# BB#12:                                # %._crit_edge74
	addq	$48, %rbx
	movq	%rbx, %r12
	jmp	.LBB6_20
.LBB6_21:
	cmpl	360(%rbx), %eax
	jge	.LBB6_23
# BB#22:
	movl	%eax, %ecx
	cmpl	344(%rbx), %eax
	jge	.LBB6_26
	jmp	.LBB6_28
.LBB6_23:
	jle	.LBB6_25
# BB#24:
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$1021, 40(%rax)         # imm = 0x3FD
	movl	$-1, %esi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB6_25:
	movl	%eax, %ecx
	movb	372(%rbx), %al
	movq	400(%rbx), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 400(%rbx)
	movb	%al, (%rdx)
	movl	368(%rbx), %eax
	cmpl	344(%rbx), %eax
	jl	.LBB6_28
	.p2align	4, 0x90
.LBB6_26:                               # =>This Inner Loop Header: Depth=1
	cltq
	movq	384(%rbx), %rdx
	movq	400(%rbx), %rsi
	movzbl	(%rdx,%rax), %edx
	leaq	1(%rsi), %rdi
	movq	%rdi, 400(%rbx)
	movb	%dl, (%rsi)
	movq	376(%rbx), %rdx
	movzwl	(%rdx,%rax,2), %eax
	cmpl	344(%rbx), %eax
	jge	.LBB6_26
.LBB6_28:                               # %._crit_edge
	movl	%eax, 372(%rbx)
	movslq	360(%rbx), %rdx
	cmpq	$4095, %rdx             # imm = 0xFFF
	jg	.LBB6_32
# BB#29:
	movzwl	368(%rbx), %esi
	movq	376(%rbx), %rdi
	movw	%si, (%rdi,%rdx,2)
	movq	384(%rbx), %rsi
	movb	%al, (%rsi,%rdx)
	movl	360(%rbx), %edx
	incl	%edx
	movl	%edx, 360(%rbx)
	movl	356(%rbx), %eax
	cmpl	%eax, %edx
	jl	.LBB6_32
# BB#30:
	movl	352(%rbx), %edx
	cmpl	$11, %edx
	jg	.LBB6_32
# BB#31:
	incl	%edx
	movl	%edx, 352(%rbx)
	addl	%eax, %eax
	movl	%eax, 356(%rbx)
.LBB6_32:                               # %._crit_edge._crit_edge
	movl	%ecx, 368(%rbx)
	movl	372(%rbx), %eax
	jmp	.LBB6_33
.LBB6_13:
	movq	24(%rbx), %rdi
	callq	_IO_getc
	leaq	48(%rbx), %r12
	cmpl	$-1, %eax
	je	.LBB6_18
# BB#14:
	movq	%rsp, %r14
.LBB6_15:                               # %ReadByte.exit.i.i
                                        # =>This Inner Loop Header: Depth=1
	testl	%eax, %eax
	jle	.LBB6_19
# BB#16:                                #   in Loop: Header=BB6_15 Depth=1
	movslq	%eax, %r15
	movq	24(%rbx), %rcx
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	fread
	cmpq	%r15, %rax
	je	.LBB6_17
# BB#34:                                #   in Loop: Header=BB6_15 Depth=1
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB6_17:                               # %GetDataBlock.exit.thread.backedge.i
                                        #   in Loop: Header=BB6_15 Depth=1
	movq	24(%rbx), %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB6_15
.LBB6_18:                               # %ReadByte.exit.thread.i.i
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB6_19:                               # %SkipDataBlocks.exit
	movl	$1, 336(%rbx)
.LBB6_20:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movl	$1023, 40(%rax)         # imm = 0x3FF
	movl	$-1, %esi
	callq	*8(%rax)
	xorl	%eax, %eax
	jmp	.LBB6_33
.Lfunc_end6:
	.size	LZWReadByte, .Lfunc_end6-LZWReadByte
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_interlaced_row,@function
get_interlaced_row:                     # @get_interlaced_row
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 48
.Lcfi61:
	.cfi_offset %rbx, -40
.Lcfi62:
	.cfi_offset %r12, -32
.Lcfi63:
	.cfi_offset %r14, -24
.Lcfi64:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	56(%r14), %r12
	movl	424(%r14), %edx
	movl	%edx, %eax
	andb	$7, %al
	cmpb	$7, %al
	je	.LBB7_5
# BB#1:
	movl	%edx, %eax
	andl	$7, %eax
	jmpq	*.LJTI7_0(,%rax,8)
.LBB7_4:
	shrl	$2, %edx
	addl	432(%r14), %edx
	jmp	.LBB7_6
.LBB7_2:
	shrl	$3, %edx
	jmp	.LBB7_6
.LBB7_3:
	shrl	$3, %edx
	addl	428(%r14), %edx
	jmp	.LBB7_6
.LBB7_5:
	shrl	%edx
	addl	436(%r14), %edx
.LBB7_6:
	movq	8(%r15), %rax
	movq	416(%r14), %rsi
	movl	$1, %ecx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	*56(%rax)
	movl	40(%r15), %esi
	testl	%esi, %esi
	je	.LBB7_12
# BB#7:                                 # %.lr.ph
	movq	32(%r14), %rcx
	movq	(%rcx), %rcx
	movq	(%rax), %rax
	testb	$1, %sil
	jne	.LBB7_9
# BB#8:
	movl	%esi, %edx
	cmpl	$1, %esi
	jne	.LBB7_11
	jmp	.LBB7_12
.LBB7_9:
	movq	(%r12), %rdx
	movzbl	(%rax), %edi
	incq	%rax
	movb	(%rdx,%rdi), %dl
	movb	%dl, (%rcx)
	movq	8(%r12), %rdx
	movb	(%rdx,%rdi), %dl
	movb	%dl, 1(%rcx)
	movq	16(%r12), %rdx
	movb	(%rdx,%rdi), %dl
	movb	%dl, 2(%rcx)
	addq	$3, %rcx
	leal	-1(%rsi), %edx
	cmpl	$1, %esi
	je	.LBB7_12
	.p2align	4, 0x90
.LBB7_11:                               # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rsi
	movzbl	(%rax), %edi
	movzbl	(%rsi,%rdi), %ebx
	movb	%bl, (%rcx)
	movq	8(%r12), %rsi
	movzbl	(%rsi,%rdi), %ebx
	movb	%bl, 1(%rcx)
	movq	16(%r12), %rsi
	movzbl	(%rsi,%rdi), %ebx
	movb	%bl, 2(%rcx)
	movq	(%r12), %rsi
	movzbl	1(%rax), %edi
	movzbl	(%rsi,%rdi), %ebx
	movb	%bl, 3(%rcx)
	movq	8(%r12), %rsi
	movzbl	(%rsi,%rdi), %ebx
	movb	%bl, 4(%rcx)
	movq	16(%r12), %rsi
	movzbl	(%rsi,%rdi), %ebx
	movb	%bl, 5(%rcx)
	addq	$2, %rax
	addq	$6, %rcx
	addl	$-2, %edx
	jne	.LBB7_11
.LBB7_12:                               # %._crit_edge
	incl	424(%r14)
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	get_interlaced_row, .Lfunc_end7-get_interlaced_row
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI7_0:
	.quad	.LBB7_2
	.quad	.LBB7_5
	.quad	.LBB7_4
	.quad	.LBB7_5
	.quad	.LBB7_3
	.quad	.LBB7_5
	.quad	.LBB7_4

	.text
	.p2align	4, 0x90
	.type	GetCode,@function
GetCode:                                # @GetCode
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi69:
	.cfi_def_cfa_offset 48
.Lcfi70:
	.cfi_offset %rbx, -40
.Lcfi71:
	.cfi_offset %r12, -32
.Lcfi72:
	.cfi_offset %r14, -24
.Lcfi73:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	332(%rbx), %ecx
	movl	352(%rbx), %edx
	leal	(%rdx,%rcx), %esi
	cmpl	328(%rbx), %esi
	jle	.LBB8_10
# BB#1:                                 # %.lr.ph
	leaq	66(%rbx), %r14
	.p2align	4, 0x90
.LBB8_2:                                # =>This Inner Loop Header: Depth=1
	cmpl	$0, 336(%rbx)
	jne	.LBB8_3
# BB#5:                                 #   in Loop: Header=BB8_2 Depth=1
	movslq	324(%rbx), %rax
	movzbl	62(%rbx,%rax), %ecx
	movb	%cl, 64(%rbx)
	movzbl	63(%rbx,%rax), %eax
	movb	%al, 65(%rbx)
	movq	24(%rbx), %rdi
	callq	_IO_getc
	movl	%eax, %r15d
	cmpl	$-1, %r15d
	je	.LBB8_6
# BB#7:                                 # %ReadByte.exit.i
                                        #   in Loop: Header=BB8_2 Depth=1
	testl	%r15d, %r15d
	jle	.LBB8_11
# BB#8:                                 #   in Loop: Header=BB8_2 Depth=1
	movslq	%r15d, %r12
	movq	24(%rbx), %rcx
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%r12, %rdx
	callq	fread
	cmpq	%r12, %rax
	je	.LBB8_9
.LBB8_6:                                # %ReadByte.exit.thread.i
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
	jmp	.LBB8_9
	.p2align	4, 0x90
.LBB8_11:                               # %GetDataBlock.exit
                                        #   in Loop: Header=BB8_2 Depth=1
	je	.LBB8_12
.LBB8_9:                                # %GetDataBlock.exit.thread
                                        #   in Loop: Header=BB8_2 Depth=1
	movl	332(%rbx), %ecx
	addl	$16, %ecx
	subl	328(%rbx), %ecx
	movl	%ecx, 332(%rbx)
	movl	%r15d, %eax
	addl	$2, %eax
	movl	%eax, 324(%rbx)
	leal	16(,%r15,8), %eax
	movl	%eax, 328(%rbx)
	movl	352(%rbx), %edx
	leal	(%rdx,%rcx), %esi
	cmpl	%eax, %esi
	jg	.LBB8_2
.LBB8_10:                               # %._crit_edge
	movl	%ecx, %eax
	sarl	$3, %eax
	movslq	%eax, %r8
	movzbl	66(%rbx,%r8), %edi
	shlq	$8, %rdi
	movzbl	65(%rbx,%r8), %eax
	orq	%rdi, %rax
	shlq	$8, %rax
	movzbl	64(%rbx,%r8), %edi
	orq	%rax, %rdi
	andb	$7, %cl
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shrq	%cl, %rdi
	movl	$1, %eax
	movl	%edx, %ecx
	shll	%cl, %eax
	decl	%eax
	andl	%edi, %eax
	movl	%esi, 332(%rbx)
	jmp	.LBB8_4
.LBB8_12:
	movl	$1, 336(%rbx)
.LBB8_3:
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$1024, 40(%rax)         # imm = 0x400
	movl	$-1, %esi
	callq	*8(%rax)
	movl	348(%rbx), %eax
.LBB8_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	GetCode, .Lfunc_end8-GetCode
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
