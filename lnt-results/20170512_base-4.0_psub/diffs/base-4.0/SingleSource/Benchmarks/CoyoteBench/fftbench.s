	.text
	.file	"fftbench.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4482207529143112665     # double 4.6566128752499998E-9
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$80, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 128
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	cmpl	$2, %edi
	jl	.LBB0_1
# BB#2:                                 # %.preheader
	movq	8(%rsi), %rdi
	movl	$.L.str, %esi
	callq	strcmp
	testl	%eax, %eax
	sete	%bpl
	jmp	.LBB0_3
.LBB0_1:
	xorl	%ebp, %ebp
.LBB0_3:                                # %.loopexit
	movq	$_ZTV10polynomialIdE+16, 56(%rsp)
	movq	$524288, 72(%rsp)       # imm = 0x80000
	movl	$4194304, %edi          # imm = 0x400000
	callq	_Znam
	movq	%rax, %r15
	movq	%r15, 64(%rsp)
	movq	$_ZTV10polynomialIdE+16, 8(%rsp)
	movq	$0, 16(%rsp)
	movq	$524288, 24(%rsp)       # imm = 0x80000
.Ltmp0:
	movl	$4194304, %edi          # imm = 0x400000
	callq	_Znam
	movq	%rax, %rbx
.Ltmp1:
# BB#4:
	movq	%rbx, 16(%rsp)
.Ltmp3:
	movl	$8388600, %edi          # imm = 0x7FFFF8
	callq	_Znam
	movq	%rax, %r14
.Ltmp4:
# BB#5:                                 # %_ZN10polynomialIdEC2Em.exit30.preheader
	movq	_ZZL13random_doublevE4seed(%rip), %rcx
	xorl	%esi, %esi
	movabsq	$4730756183288445817, %rdi # imm = 0x41A705AF1FE3FB79
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB0_6:                                # %_ZN10polynomialIdEC2Em.exit30
                                        # =>This Inner Loop Header: Depth=1
	xorq	$123459876, %rcx        # imm = 0x75BD924
	movq	%rcx, %rax
	imulq	%rdi
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$15, %rdx
	addq	%rax, %rdx
	imulq	$-127773, %rdx, %rax    # imm = 0xFFFE0CE3
	addq	%rcx, %rax
	imulq	$16807, %rax, %rax      # imm = 0x41A7
	imulq	$-2836, %rdx, %rdx      # imm = 0xF4EC
	leaq	2147483647(%rax,%rdx), %rcx
	addq	%rdx, %rax
	cmovnsq	%rax, %rcx
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rcx, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, (%r15,%rsi,8)
	movq	%rcx, %rax
	imulq	%rdi
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$15, %rdx
	addq	%rax, %rdx
	imulq	$-127773, %rdx, %rax    # imm = 0xFFFE0CE3
	addq	%rcx, %rax
	imulq	$16807, %rax, %rax      # imm = 0x41A7
	imulq	$-2836, %rdx, %rdx      # imm = 0xF4EC
	leaq	2147483647(%rax,%rdx), %rcx
	addq	%rdx, %rax
	cmovnsq	%rax, %rcx
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rcx, %xmm1
	mulsd	%xmm0, %xmm1
	xorq	$123459876, %rcx        # imm = 0x75BD924
	movsd	%xmm1, (%rbx,%rsi,8)
	incq	%rsi
	cmpq	$524288, %rsi           # imm = 0x80000
	jne	.LBB0_6
# BB#7:
	movq	%rcx, _ZZL13random_doublevE4seed(%rip)
.Ltmp6:
	leaq	32(%rsp), %rdi
	leaq	56(%rsp), %rsi
	leaq	8(%rsp), %rdx
	callq	_ZNK10polynomialIdEmlERKS0_
.Ltmp7:
# BB#8:
	cmpq	$1048575, 48(%rsp)      # imm = 0xFFFFF
	jne	.LBB0_12
# BB#9:                                 # %.thread
	movq	40(%rsp), %rdi
	movl	$1048575, %ebx          # imm = 0xFFFFF
	movq	%r14, %r15
	cmpq	$4, %rbx
	jb	.LBB0_15
	jmp	.LBB0_16
.LBB0_12:                               # %_ZN10polynomialIdE7releaseEv.exit.i
	movq	%r14, %rdi
	callq	_ZdaPv
	movq	48(%rsp), %rbx
	movl	$8, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp9:
	callq	_Znam
	movq	%rax, %r15
.Ltmp10:
# BB#13:
	movq	40(%rsp), %rdi
	testq	%rbx, %rbx
	je	.LBB0_36
# BB#14:                                # %.lr.ph.i.i
	cmpq	$4, %rbx
	jae	.LBB0_16
.LBB0_15:
	xorl	%eax, %eax
	jmp	.LBB0_29
.LBB0_16:                               # %min.iters.checked
	movq	%rbx, %rax
	andq	$-4, %rax
	je	.LBB0_17
# BB#18:                                # %vector.memcheck
	leaq	(%rdi,%rbx,8), %rcx
	cmpq	%rcx, %r15
	jae	.LBB0_21
# BB#19:                                # %vector.memcheck
	leaq	(%r15,%rbx,8), %rcx
	cmpq	%rcx, %rdi
	jae	.LBB0_21
# BB#20:
	xorl	%eax, %eax
	jmp	.LBB0_29
.LBB0_36:                               # %_ZN10polynomialIdEaSERKS0_.exit
	movq	$_ZTV10polynomialIdE+16, 32(%rsp)
	testq	%rdi, %rdi
	movq	%r15, %r14
	jne	.LBB0_37
	jmp	.LBB0_38
.LBB0_17:
	xorl	%eax, %eax
	jmp	.LBB0_29
.LBB0_21:                               # %vector.body.preheader
	leaq	-4(%rax), %rcx
	movl	%ecx, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB0_22
# BB#23:                                # %vector.body.prol.preheader
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_24:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rdi,%rsi,8), %xmm0
	movupd	16(%rdi,%rsi,8), %xmm1
	movupd	%xmm0, (%r15,%rsi,8)
	movupd	%xmm1, 16(%r15,%rsi,8)
	addq	$4, %rsi
	incq	%rdx
	jne	.LBB0_24
	jmp	.LBB0_25
.LBB0_22:
	xorl	%esi, %esi
.LBB0_25:                               # %vector.body.prol.loopexit
	cmpq	$12, %rcx
	jb	.LBB0_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%r15,%rsi,8), %rdx
	leaq	112(%rdi,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB0_27:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movupd	-16(%rsi), %xmm0
	movupd	(%rsi), %xmm1
	movupd	%xmm0, -16(%rdx)
	movupd	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-16, %rcx
	jne	.LBB0_27
.LBB0_28:                               # %middle.block
	cmpq	%rax, %rbx
	je	.LBB0_35
.LBB0_29:                               # %scalar.ph.preheader
	movl	%ebx, %edx
	subl	%eax, %edx
	leaq	-1(%rbx), %rcx
	subq	%rax, %rcx
	andq	$7, %rdx
	je	.LBB0_32
# BB#30:                                # %scalar.ph.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB0_31:                               # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rax,8), %rsi
	movq	%rsi, (%r15,%rax,8)
	incq	%rax
	incq	%rdx
	jne	.LBB0_31
.LBB0_32:                               # %scalar.ph.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB0_35
# BB#33:                                # %scalar.ph.preheader.new
	subq	%rax, %rbx
	leaq	56(%r15,%rax,8), %rcx
	leaq	56(%rdi,%rax,8), %rax
	.p2align	4, 0x90
.LBB0_34:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rax), %rdx
	movq	%rdx, -56(%rcx)
	movq	-48(%rax), %rdx
	movq	%rdx, -48(%rcx)
	movq	-40(%rax), %rdx
	movq	%rdx, -40(%rcx)
	movq	-32(%rax), %rdx
	movq	%rdx, -32(%rcx)
	movq	-24(%rax), %rdx
	movq	%rdx, -24(%rcx)
	movq	-16(%rax), %rdx
	movq	%rdx, -16(%rcx)
	movq	-8(%rax), %rdx
	movq	%rdx, -8(%rcx)
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	addq	$64, %rcx
	addq	$64, %rax
	addq	$-8, %rbx
	jne	.LBB0_34
.LBB0_35:                               # %_ZN10polynomialIdEaSERKS0_.exit.thread
	movq	$_ZTV10polynomialIdE+16, 32(%rsp)
.LBB0_37:
	callq	_ZdaPv
	movq	%r15, %r14
.LBB0_38:                               # %_ZN10polynomialIdED2Ev.exit35
	testb	%bpl, %bpl
	je	.LBB0_43
# BB#39:
.Ltmp12:
	movl	$_ZSt4cout, %edi
	xorpd	%xmm0, %xmm0
	callq	_ZNSo9_M_insertIdEERSoT_
.Ltmp13:
	jmp	.LBB0_46
.LBB0_43:
.Ltmp14:
	movl	$_ZSt4cout, %edi
	movl	$.L.str.1, %esi
	movl	$31, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp15:
# BB#44:                                # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit
.Ltmp16:
	movl	$_ZSt4cout, %edi
	xorpd	%xmm0, %xmm0
	callq	_ZNSo9_M_insertIdEERSoT_
.Ltmp17:
# BB#45:                                # %_ZNSolsEd.exit41
.Ltmp18:
	movl	$.L.str.2, %esi
	movl	$2, %edx
	movq	%rax, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp19:
.LBB0_46:                               # %_ZNSolsEd.exit
.Ltmp20:
	movl	$_ZSt4cout, %edi
	callq	_ZNSo5flushEv
.Ltmp21:
# BB#47:
	movq	%r14, %rdi
	callq	_ZdaPv
	movq	$_ZTV10polynomialIdE+16, 8(%rsp)
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_49
# BB#48:
	callq	_ZdaPv
.LBB0_49:                               # %_ZN10polynomialIdED2Ev.exit47
	movq	$_ZTV10polynomialIdE+16, 56(%rsp)
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_51
# BB#50:
	callq	_ZdaPv
.LBB0_51:                               # %_ZN10polynomialIdED2Ev.exit33
	xorl	%eax, %eax
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_41:
.Ltmp11:
	movq	%rax, %r12
	movq	$_ZTV10polynomialIdE+16, 32(%rsp)
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_54
# BB#42:
	callq	_ZdaPv
	jmp	.LBB0_54
.LBB0_40:
.Ltmp8:
	jmp	.LBB0_53
.LBB0_11:                               # %.thread68
.Ltmp5:
	movq	%rax, %r12
	movq	$_ZTV10polynomialIdE+16, 8(%rsp)
	jmp	.LBB0_55
.LBB0_10:                               # %.thread73
.Ltmp2:
	movq	%rax, %r12
	movq	$_ZTV10polynomialIdE+16, 56(%rsp)
	jmp	.LBB0_57
.LBB0_52:
.Ltmp22:
.LBB0_53:
	movq	%rax, %r12
.LBB0_54:
	movq	%r14, %rdi
	callq	_ZdaPv
	movq	16(%rsp), %rbx
	movq	$_ZTV10polynomialIdE+16, 8(%rsp)
	testq	%rbx, %rbx
	je	.LBB0_56
.LBB0_55:
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB0_56:
	movq	64(%rsp), %r15
	movq	$_ZTV10polynomialIdE+16, 56(%rsp)
	testq	%r15, %r15
	je	.LBB0_58
.LBB0_57:
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB0_58:                               # %_ZN10polynomialIdED2Ev.exit
	movq	%r12, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp21-.Ltmp12         #   Call between .Ltmp12 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin0   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Lfunc_end0-.Ltmp21     #   Call between .Ltmp21 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNK10polynomialIdEmlERKS0_,"axG",@progbits,_ZNK10polynomialIdEmlERKS0_,comdat
	.weak	_ZNK10polynomialIdEmlERKS0_
	.p2align	4, 0x90
	.type	_ZNK10polynomialIdEmlERKS0_,@function
_ZNK10polynomialIdEmlERKS0_:            # @_ZNK10polynomialIdEmlERKS0_
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 176
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %rbp
	movq	%rdi, %r14
	movq	$_ZTV10polynomialIdE+16, (%rsp)
	movq	16(%rbp), %r12
	movq	%r12, 16(%rsp)
	movl	$8, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %r13
	cmovoq	%r13, %rax
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	movq	%rbx, 8(%rsp)
	testq	%r12, %r12
	je	.LBB1_22
# BB#1:                                 # %.lr.ph.i.i.preheader
	movq	8(%rbp), %rax
	cmpq	$4, %r12
	jae	.LBB1_3
# BB#2:
	xorl	%ecx, %ecx
	jmp	.LBB1_16
.LBB1_3:                                # %min.iters.checked
	movq	%r12, %rcx
	andq	$-4, %rcx
	je	.LBB1_7
# BB#4:                                 # %vector.memcheck
	leaq	(%rax,%r12,8), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB1_8
# BB#5:                                 # %vector.memcheck
	leaq	(%rbx,%r12,8), %rdx
	cmpq	%rdx, %rax
	jae	.LBB1_8
.LBB1_7:
	xorl	%ecx, %ecx
.LBB1_16:                               # %.lr.ph.i.i.preheader167
	movl	%r12d, %esi
	subl	%ecx, %esi
	leaq	-1(%r12), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB1_19
# BB#17:                                # %.lr.ph.i.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB1_18:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rcx,8), %rdi
	movq	%rdi, (%rbx,%rcx,8)
	incq	%rcx
	incq	%rsi
	jne	.LBB1_18
.LBB1_19:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB1_22
# BB#20:                                # %.lr.ph.i.i.preheader167.new
	movq	%r12, %rdx
	subq	%rcx, %rdx
	leaq	56(%rbx,%rcx,8), %rsi
	leaq	56(%rax,%rcx,8), %rax
	.p2align	4, 0x90
.LBB1_21:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rax), %rcx
	movq	%rcx, -56(%rsi)
	movq	-48(%rax), %rcx
	movq	%rcx, -48(%rsi)
	movq	-40(%rax), %rcx
	movq	%rcx, -40(%rsi)
	movq	-32(%rax), %rcx
	movq	%rcx, -32(%rsi)
	movq	-24(%rax), %rcx
	movq	%rcx, -24(%rsi)
	movq	-16(%rax), %rcx
	movq	%rcx, -16(%rsi)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rsi)
	movq	(%rax), %rcx
	movq	%rcx, (%rsi)
	addq	$64, %rsi
	addq	$64, %rax
	addq	$-8, %rdx
	jne	.LBB1_21
.LBB1_22:                               # %_ZN10polynomialIdEC2ERKS0_.exit
	movq	$_ZTV10polynomialIdE+16, 24(%rsp)
	movq	$0, 32(%rsp)
	movq	16(%r15), %rbp
	movq	%rbp, 40(%rsp)
	movl	$8, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	cmovnoq	%rax, %r13
.Ltmp23:
	movq	%r13, %rdi
	callq	_Znam
.Ltmp24:
# BB#23:                                # %.noexc
	movq	%rax, 32(%rsp)
	testq	%rbp, %rbp
	je	.LBB1_45
# BB#24:                                # %.lr.ph.i.i32.preheader
	movq	8(%r15), %rcx
	cmpq	$4, %rbp
	jae	.LBB1_26
# BB#25:
	xorl	%edx, %edx
	jmp	.LBB1_39
.LBB1_26:                               # %min.iters.checked84
	movq	%rbp, %rdx
	andq	$-4, %rdx
	je	.LBB1_30
# BB#27:                                # %vector.memcheck96
	leaq	(%rcx,%rbp,8), %rsi
	cmpq	%rsi, %rax
	jae	.LBB1_31
# BB#28:                                # %vector.memcheck96
	leaq	(%rax,%rbp,8), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB1_31
.LBB1_30:
	xorl	%edx, %edx
.LBB1_39:                               # %.lr.ph.i.i32.preheader166
	movl	%ebp, %edi
	subl	%edx, %edi
	leaq	-1(%rbp), %rsi
	subq	%rdx, %rsi
	andq	$7, %rdi
	je	.LBB1_42
# BB#40:                                # %.lr.ph.i.i32.prol.preheader
	negq	%rdi
	.p2align	4, 0x90
.LBB1_41:                               # %.lr.ph.i.i32.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rbx
	movq	%rbx, (%rax,%rdx,8)
	incq	%rdx
	incq	%rdi
	jne	.LBB1_41
.LBB1_42:                               # %.lr.ph.i.i32.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB1_45
# BB#43:                                # %.lr.ph.i.i32.preheader166.new
	movq	%rbp, %rsi
	subq	%rdx, %rsi
	leaq	56(%rax,%rdx,8), %rax
	leaq	56(%rcx,%rdx,8), %rcx
	.p2align	4, 0x90
.LBB1_44:                               # %.lr.ph.i.i32
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rdx
	movq	%rdx, -56(%rax)
	movq	-48(%rcx), %rdx
	movq	%rdx, -48(%rax)
	movq	-40(%rcx), %rdx
	movq	%rdx, -40(%rax)
	movq	-32(%rcx), %rdx
	movq	%rdx, -32(%rax)
	movq	-24(%rcx), %rdx
	movq	%rdx, -24(%rax)
	movq	-16(%rcx), %rdx
	movq	%rdx, -16(%rax)
	movq	-8(%rcx), %rdx
	movq	%rdx, -8(%rax)
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	addq	$64, %rax
	addq	$64, %rcx
	addq	$-8, %rsi
	jne	.LBB1_44
.LBB1_45:                               # %_ZN10polynomialIdEC2ERKS0_.exit33
	cmpq	%rbp, %r12
	jbe	.LBB1_61
# BB#46:
.Ltmp30:
	movq	%rsp, %rdi
	callq	_ZN10polynomialIdE11stretch_fftEv
	movq	%rax, %rbx
.Ltmp31:
# BB#47:
	testq	%rbx, %rbx
	je	.LBB1_80
# BB#48:
	movq	32(%rsp), %rbp
	movq	40(%rsp), %r15
	addq	%r15, %rbx
	movq	%rbx, 40(%rsp)
	movl	$8, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp32:
	callq	_Znam
.Ltmp33:
# BB#49:                                # %.noexc35
	movq	%rax, 32(%rsp)
	testq	%r15, %r15
	je	.LBB1_76
# BB#50:                                # %.lr.ph18.i.preheader
	cmpq	$3, %r15
	jbe	.LBB1_54
# BB#51:                                # %min.iters.checked140
	movq	%r15, %rcx
	andq	$-4, %rcx
	je	.LBB1_54
# BB#52:                                # %vector.memcheck152
	leaq	(%rbp,%r15,8), %rdx
	cmpq	%rdx, %rax
	jae	.LBB1_84
# BB#53:                                # %vector.memcheck152
	leaq	(%rax,%r15,8), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB1_84
.LBB1_54:
	xorl	%ecx, %ecx
.LBB1_55:                               # %.lr.ph18.i.preheader164
	movl	%r15d, %esi
	subl	%ecx, %esi
	leaq	-1(%r15), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB1_58
# BB#56:                                # %.lr.ph18.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB1_57:                               # %.lr.ph18.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp,%rcx,8), %rdi
	movq	%rdi, (%rax,%rcx,8)
	incq	%rcx
	incq	%rsi
	jne	.LBB1_57
.LBB1_58:                               # %.lr.ph18.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB1_78
# BB#59:                                # %.lr.ph18.i.preheader164.new
	movq	%r15, %rdx
	subq	%rcx, %rdx
	leaq	56(%rax,%rcx,8), %rsi
	leaq	56(%rbp,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB1_60:                               # %.lr.ph18.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rdi
	movq	%rdi, -56(%rsi)
	movq	-48(%rcx), %rdi
	movq	%rdi, -48(%rsi)
	movq	-40(%rcx), %rdi
	movq	%rdi, -40(%rsi)
	movq	-32(%rcx), %rdi
	movq	%rdi, -32(%rsi)
	movq	-24(%rcx), %rdi
	movq	%rdi, -24(%rsi)
	movq	-16(%rcx), %rdi
	movq	%rdi, -16(%rsi)
	movq	-8(%rcx), %rdi
	movq	%rdi, -8(%rsi)
	movq	(%rcx), %rdi
	movq	%rdi, (%rsi)
	addq	$64, %rsi
	addq	$64, %rcx
	addq	$-8, %rdx
	jne	.LBB1_60
	jmp	.LBB1_78
.LBB1_61:
.Ltmp26:
	leaq	24(%rsp), %rdi
	callq	_ZN10polynomialIdE11stretch_fftEv
	movq	%rax, %rbx
.Ltmp27:
# BB#62:
	testq	%rbx, %rbx
	je	.LBB1_80
# BB#63:
	movq	8(%rsp), %rbp
	movq	16(%rsp), %r15
	addq	%r15, %rbx
	movq	%rbx, 16(%rsp)
	movl	$8, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp28:
	callq	_Znam
.Ltmp29:
# BB#64:                                # %.noexc45
	movq	%rax, 8(%rsp)
	testq	%r15, %r15
	je	.LBB1_77
# BB#65:                                # %.lr.ph18.i44.preheader
	cmpq	$3, %r15
	jbe	.LBB1_69
# BB#66:                                # %min.iters.checked112
	movq	%r15, %rcx
	andq	$-4, %rcx
	je	.LBB1_69
# BB#67:                                # %vector.memcheck124
	leaq	(%rbp,%r15,8), %rdx
	cmpq	%rdx, %rax
	jae	.LBB1_87
# BB#68:                                # %vector.memcheck124
	leaq	(%rax,%r15,8), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB1_87
.LBB1_69:
	xorl	%ecx, %ecx
.LBB1_70:                               # %.lr.ph18.i44.preheader165
	movl	%r15d, %esi
	subl	%ecx, %esi
	leaq	-1(%r15), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB1_73
# BB#71:                                # %.lr.ph18.i44.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB1_72:                               # %.lr.ph18.i44.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp,%rcx,8), %rdi
	movq	%rdi, (%rax,%rcx,8)
	incq	%rcx
	incq	%rsi
	jne	.LBB1_72
.LBB1_73:                               # %.lr.ph18.i44.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB1_78
# BB#74:                                # %.lr.ph18.i44.preheader165.new
	movq	%r15, %rdx
	subq	%rcx, %rdx
	leaq	56(%rax,%rcx,8), %rsi
	leaq	56(%rbp,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB1_75:                               # %.lr.ph18.i44
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rdi
	movq	%rdi, -56(%rsi)
	movq	-48(%rcx), %rdi
	movq	%rdi, -48(%rsi)
	movq	-40(%rcx), %rdi
	movq	%rdi, -40(%rsi)
	movq	-32(%rcx), %rdi
	movq	%rdi, -32(%rsi)
	movq	-24(%rcx), %rdi
	movq	%rdi, -24(%rsi)
	movq	-16(%rcx), %rdi
	movq	%rdi, -16(%rsi)
	movq	-8(%rcx), %rdi
	movq	%rdi, -8(%rsi)
	movq	(%rcx), %rdi
	movq	%rdi, (%rsi)
	addq	$64, %rsi
	addq	$64, %rcx
	addq	$-8, %rdx
	jne	.LBB1_75
	jmp	.LBB1_78
.LBB1_76:
	xorl	%r15d, %r15d
	cmpq	%rbx, %r15
	jb	.LBB1_79
	jmp	.LBB1_80
.LBB1_77:
	xorl	%r15d, %r15d
.LBB1_78:                               # %.preheader.i
	cmpq	%rbx, %r15
	jae	.LBB1_80
.LBB1_79:                               # %.lr.ph.i
	leaq	(%rax,%r15,8), %rdi
	leaq	1(%r15), %rdx
	cmpq	%rdx, %rbx
	cmovaq	%rbx, %rdx
	subq	%r15, %rdx
	shlq	$3, %rdx
	xorl	%esi, %esi
	callq	memset
.LBB1_80:                               # %_ZN10polynomialIdE7stretchEm.exit
.Ltmp34:
	leaq	96(%rsp), %rdi
	movq	%rsp, %rsi
	callq	_ZN10polynomialIdE3fftERKS0_
.Ltmp35:
# BB#81:
.Ltmp37:
	leaq	48(%rsp), %rdi
	leaq	24(%rsp), %rsi
	callq	_ZN10polynomialIdE3fftERKS0_
.Ltmp38:
# BB#82:
	movq	16(%rsp), %r15
	testq	%r15, %r15
	je	.LBB1_104
# BB#83:                                # %.lr.ph67
	xorl	%ebx, %ebx
	movq	%r15, %rbp
	jmp	.LBB1_101
.LBB1_8:                                # %vector.body.preheader
	leaq	-4(%rcx), %rdx
	movl	%edx, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB1_11
# BB#9:                                 # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_10:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rax,%rdi,8), %xmm0
	movupd	16(%rax,%rdi,8), %xmm1
	movupd	%xmm0, (%rbx,%rdi,8)
	movupd	%xmm1, 16(%rbx,%rdi,8)
	addq	$4, %rdi
	incq	%rsi
	jne	.LBB1_10
	jmp	.LBB1_12
.LBB1_31:                               # %vector.body80.preheader
	leaq	-4(%rdx), %rsi
	movl	%esi, %edi
	shrl	$2, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB1_34
# BB#32:                                # %vector.body80.prol.preheader
	negq	%rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_33:                               # %vector.body80.prol
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rcx,%rbx,8), %xmm0
	movupd	16(%rcx,%rbx,8), %xmm1
	movupd	%xmm0, (%rax,%rbx,8)
	movupd	%xmm1, 16(%rax,%rbx,8)
	addq	$4, %rbx
	incq	%rdi
	jne	.LBB1_33
	jmp	.LBB1_35
.LBB1_11:
	xorl	%edi, %edi
.LBB1_12:                               # %vector.body.prol.loopexit
	cmpq	$12, %rdx
	jb	.LBB1_15
# BB#13:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%rbx,%rdi,8), %rsi
	leaq	112(%rax,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB1_14:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movupd	-16(%rdi), %xmm0
	movupd	(%rdi), %xmm1
	movupd	%xmm0, -16(%rsi)
	movupd	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-16, %rdx
	jne	.LBB1_14
.LBB1_15:                               # %middle.block
	cmpq	%rcx, %r12
	je	.LBB1_22
	jmp	.LBB1_16
.LBB1_34:
	xorl	%ebx, %ebx
.LBB1_35:                               # %vector.body80.prol.loopexit
	cmpq	$12, %rsi
	jb	.LBB1_38
# BB#36:                                # %vector.body80.preheader.new
	movq	%rdx, %rsi
	subq	%rbx, %rsi
	leaq	112(%rax,%rbx,8), %rdi
	leaq	112(%rcx,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB1_37:                               # %vector.body80
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rdi)
	movups	%xmm1, -96(%rdi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rdi)
	movups	%xmm1, -64(%rdi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rdi)
	movups	%xmm1, -32(%rdi)
	movupd	-16(%rbx), %xmm0
	movupd	(%rbx), %xmm1
	movupd	%xmm0, -16(%rdi)
	movupd	%xmm1, (%rdi)
	subq	$-128, %rdi
	subq	$-128, %rbx
	addq	$-16, %rsi
	jne	.LBB1_37
.LBB1_38:                               # %middle.block81
	cmpq	%rdx, %rbp
	je	.LBB1_45
	jmp	.LBB1_39
.LBB1_84:                               # %vector.body136.preheader
	leaq	-4(%rcx), %rdx
	movl	%edx, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB1_90
# BB#85:                                # %vector.body136.prol.preheader
	negq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_86:                               # %vector.body136.prol
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rbp,%rdi,8), %xmm0
	movupd	16(%rbp,%rdi,8), %xmm1
	movupd	%xmm0, (%rax,%rdi,8)
	movupd	%xmm1, 16(%rax,%rdi,8)
	addq	$4, %rdi
	incq	%rsi
	jne	.LBB1_86
	jmp	.LBB1_91
.LBB1_87:                               # %vector.body108.preheader
	leaq	-4(%rcx), %rdx
	movl	%edx, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB1_95
# BB#88:                                # %vector.body108.prol.preheader
	negq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_89:                               # %vector.body108.prol
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rbp,%rdi,8), %xmm0
	movupd	16(%rbp,%rdi,8), %xmm1
	movupd	%xmm0, (%rax,%rdi,8)
	movupd	%xmm1, 16(%rax,%rdi,8)
	addq	$4, %rdi
	incq	%rsi
	jne	.LBB1_89
	jmp	.LBB1_96
.LBB1_90:
	xorl	%edi, %edi
.LBB1_91:                               # %vector.body136.prol.loopexit
	cmpq	$12, %rdx
	jb	.LBB1_94
# BB#92:                                # %vector.body136.preheader.new
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%rax,%rdi,8), %rsi
	leaq	112(%rbp,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB1_93:                               # %vector.body136
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movupd	-16(%rdi), %xmm0
	movupd	(%rdi), %xmm1
	movupd	%xmm0, -16(%rsi)
	movupd	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-16, %rdx
	jne	.LBB1_93
.LBB1_94:                               # %middle.block137
	cmpq	%rcx, %r15
	jne	.LBB1_55
	jmp	.LBB1_78
.LBB1_95:
	xorl	%edi, %edi
.LBB1_96:                               # %vector.body108.prol.loopexit
	cmpq	$12, %rdx
	jb	.LBB1_99
# BB#97:                                # %vector.body108.preheader.new
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%rax,%rdi,8), %rsi
	leaq	112(%rbp,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB1_98:                               # %vector.body108
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movupd	-16(%rdi), %xmm0
	movupd	(%rdi), %xmm1
	movupd	%xmm0, -16(%rsi)
	movupd	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-16, %rdx
	jne	.LBB1_98
.LBB1_99:                               # %middle.block109
	cmpq	%rcx, %r15
	jne	.LBB1_70
	jmp	.LBB1_78
.LBB1_100:                              #   in Loop: Header=BB1_101 Depth=1
	movapd	%xmm2, %xmm3
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	movapd	%xmm4, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	movapd	%xmm4, %xmm0
	callq	__muldc3
	jmp	.LBB1_103
	.p2align	4, 0x90
.LBB1_101:                              # =>This Inner Loop Header: Depth=1
	movq	56(%rsp), %rax
	movq	104(%rsp), %r12
	movupd	(%rax,%rbx), %xmm2
	movupd	(%r12,%rbx), %xmm4
	movapd	%xmm2, %xmm0
	mulpd	%xmm4, %xmm0
	movapd	%xmm2, %xmm3
	shufpd	$1, %xmm3, %xmm3        # xmm3 = xmm3[1,0]
	mulpd	%xmm4, %xmm3
	movapd	%xmm0, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	subsd	%xmm1, %xmm0
	movapd	%xmm3, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	addsd	%xmm3, %xmm1
	ucomisd	%xmm0, %xmm0
	jnp	.LBB1_103
# BB#102:                               #   in Loop: Header=BB1_101 Depth=1
	ucomisd	%xmm1, %xmm1
	jp	.LBB1_100
.LBB1_103:                              # %_ZNSt7complexIdEmLIdEERS0_RKS_IT_E.exit
                                        #   in Loop: Header=BB1_101 Depth=1
	movsd	%xmm0, (%r12,%rbx)
	movsd	%xmm1, 8(%r12,%rbx)
	addq	$16, %rbx
	decq	%rbp
	jne	.LBB1_101
.LBB1_104:                              # %._crit_edge68
.Ltmp40:
	leaq	72(%rsp), %rdi
	leaq	96(%rsp), %rsi
	callq	_ZN10polynomialIdE11inverse_fftERKS_ISt7complexIdEE
.Ltmp41:
# BB#105:
	movq	64(%rsp), %rax
	movq	88(%rsp), %rbx
	cmpq	%rbx, %rax
	je	.LBB1_112
# BB#106:
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_108
# BB#107:
	callq	_ZdaPv
	movq	88(%rsp), %rbx
.LBB1_108:                              # %_ZN10polynomialISt7complexIdEE7releaseEv.exit.i
	movq	%rbx, 64(%rsp)
	movl	$16, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp43:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp44:
# BB#109:                               # %.noexc51
	testq	%rbx, %rbx
	je	.LBB1_111
# BB#110:                               # %.loopexit.loopexit.i.i
	movq	%rbx, %rdx
	shlq	$4, %rdx
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	memset
.LBB1_111:                              # %_ZN10polynomialISt7complexIdEE7acquireEv.exit.i
	movq	%rbp, 56(%rsp)
	movq	%rbx, %rax
.LBB1_112:
	movq	80(%rsp), %rdi
	testq	%rax, %rax
	je	.LBB1_116
# BB#113:                               # %.lr.ph.i.i49
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_114:                              # =>This Inner Loop Header: Depth=1
	movq	56(%rsp), %rdx
	movupd	(%rdi,%rax), %xmm0
	movupd	%xmm0, (%rdx,%rax)
	incq	%rcx
	addq	$16, %rax
	cmpq	64(%rsp), %rcx
	jb	.LBB1_114
# BB#115:                               # %_ZN10polynomialISt7complexIdEEaSERKS2_.exit.loopexit
	movq	80(%rsp), %rdi
.LBB1_116:                              # %_ZN10polynomialISt7complexIdEEaSERKS2_.exit
	movq	$_ZTV10polynomialISt7complexIdEE+16, 72(%rsp)
	testq	%rdi, %rdi
	je	.LBB1_118
# BB#117:
	callq	_ZdaPv
.LBB1_118:                              # %_ZN10polynomialISt7complexIdEED2Ev.exit53
	leaq	-1(%r15), %rbx
	movq	$_ZTV10polynomialIdE+16, (%r14)
	movq	$0, 8(%r14)
	movq	%rbx, 16(%r14)
	movl	$8, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp46:
	callq	_Znam
.Ltmp47:
# BB#119:                               # %_ZN10polynomialIdEC2Em.exit
	movq	%rax, 8(%r14)
	testq	%rbx, %rbx
	movq	56(%rsp), %rdi
	je	.LBB1_127
# BB#120:                               # %.lr.ph.preheader
	addq	$-2, %r15
	movq	%rbx, %rdx
	xorl	%ecx, %ecx
	andq	$3, %rdx
	je	.LBB1_123
# BB#121:                               # %.lr.ph.prol.preheader
	movq	%rdi, %rsi
	.p2align	4, 0x90
.LBB1_122:                              # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rbp
	movq	%rbp, (%rax,%rcx,8)
	incq	%rcx
	addq	$16, %rsi
	cmpq	%rcx, %rdx
	jne	.LBB1_122
.LBB1_123:                              # %.lr.ph.prol.loopexit
	cmpq	$3, %r15
	jb	.LBB1_126
# BB#124:                               # %.lr.ph.preheader.new
	subq	%rcx, %rbx
	leaq	24(%rax,%rcx,8), %rax
	shlq	$4, %rcx
	leaq	48(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB1_125:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-48(%rcx), %rdx
	movq	%rdx, -24(%rax)
	movq	-32(%rcx), %rdx
	movq	%rdx, -16(%rax)
	movq	-16(%rcx), %rdx
	movq	%rdx, -8(%rax)
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	addq	$32, %rax
	addq	$64, %rcx
	addq	$-4, %rbx
	jne	.LBB1_125
.LBB1_126:                              # %._crit_edge.thread
	movq	$_ZTV10polynomialISt7complexIdEE+16, 48(%rsp)
	jmp	.LBB1_128
.LBB1_127:                              # %._crit_edge
	movq	$_ZTV10polynomialISt7complexIdEE+16, 48(%rsp)
	testq	%rdi, %rdi
	je	.LBB1_129
.LBB1_128:
	callq	_ZdaPv
.LBB1_129:                              # %_ZN10polynomialISt7complexIdEED2Ev.exit61
	movq	$_ZTV10polynomialISt7complexIdEE+16, 96(%rsp)
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_131
# BB#130:
	callq	_ZdaPv
.LBB1_131:                              # %_ZN10polynomialISt7complexIdEED2Ev.exit63
	movq	$_ZTV10polynomialIdE+16, 24(%rsp)
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_133
# BB#132:
	callq	_ZdaPv
.LBB1_133:                              # %_ZN10polynomialIdED2Ev.exit59
	movq	$_ZTV10polynomialIdE+16, (%rsp)
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_135
# BB#134:
	callq	_ZdaPv
.LBB1_135:                              # %_ZN10polynomialIdED2Ev.exit48
	movq	%r14, %rax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_136:
.Ltmp45:
	movq	%rax, %rbp
	movq	$_ZTV10polynomialISt7complexIdEE+16, 72(%rsp)
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_141
# BB#137:
	callq	_ZdaPv
	jmp	.LBB1_141
.LBB1_138:
.Ltmp48:
	jmp	.LBB1_140
.LBB1_139:
.Ltmp42:
.LBB1_140:
	movq	%rax, %rbp
.LBB1_141:
	movq	$_ZTV10polynomialISt7complexIdEE+16, 48(%rsp)
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_144
# BB#142:
	callq	_ZdaPv
	jmp	.LBB1_144
.LBB1_143:
.Ltmp39:
	movq	%rax, %rbp
.LBB1_144:
	movq	$_ZTV10polynomialISt7complexIdEE+16, 96(%rsp)
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_148
# BB#145:
	callq	_ZdaPv
	jmp	.LBB1_148
.LBB1_146:                              # %.thread
.Ltmp25:
	movq	%rax, %rbp
	movq	$_ZTV10polynomialIdE+16, (%rsp)
	jmp	.LBB1_151
.LBB1_147:
.Ltmp36:
	movq	%rax, %rbp
.LBB1_148:
	movq	$_ZTV10polynomialIdE+16, 24(%rsp)
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_150
# BB#149:
	callq	_ZdaPv
.LBB1_150:
	movq	8(%rsp), %rbx
	movq	$_ZTV10polynomialIdE+16, (%rsp)
	testq	%rbx, %rbx
	je	.LBB1_152
.LBB1_151:
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB1_152:                              # %_ZN10polynomialIdED2Ev.exit
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_ZNK10polynomialIdEmlERKS0_, .Lfunc_end1-_ZNK10polynomialIdEmlERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\237\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\234\001"              # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp23-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin1   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp29-.Ltmp30         #   Call between .Ltmp30 and .Ltmp29
	.long	.Ltmp36-.Lfunc_begin1   #     jumps to .Ltmp36
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp34-.Ltmp29         #   Call between .Ltmp29 and .Ltmp34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin1   #     jumps to .Ltmp36
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin1   #     jumps to .Ltmp39
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp40-.Ltmp38         #   Call between .Ltmp38 and .Ltmp40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin1   #     jumps to .Ltmp42
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin1   #     jumps to .Ltmp45
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Ltmp46-.Ltmp44         #   Call between .Ltmp44 and .Ltmp46
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin1   # >> Call Site 11 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin1   #     jumps to .Ltmp48
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin1   # >> Call Site 12 <<
	.long	.Lfunc_end1-.Ltmp47     #   Call between .Ltmp47 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN10polynomialIdED2Ev,"axG",@progbits,_ZN10polynomialIdED2Ev,comdat
	.weak	_ZN10polynomialIdED2Ev
	.p2align	4, 0x90
	.type	_ZN10polynomialIdED2Ev,@function
_ZN10polynomialIdED2Ev:                 # @_ZN10polynomialIdED2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV10polynomialIdE+16, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB2_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB2_1:                                # %_ZN10polynomialIdE7releaseEv.exit
	retq
.Lfunc_end2:
	.size	_ZN10polynomialIdED2Ev, .Lfunc_end2-_ZN10polynomialIdED2Ev
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4618760256179416344     # double 6.2831853071795862
	.section	.text.startup,"axG",@progbits,_ZN10polynomialIdE4PI2IE,comdat
	.p2align	4, 0x90
	.type	__cxx_global_var_init.3,@function
__cxx_global_var_init.3:                # @__cxx_global_var_init.3
	.cfi_startproc
# BB#0:
	cmpb	$0, _ZGVN10polynomialIdE4PI2IE(%rip)
	jne	.LBB3_2
# BB#1:
	movq	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqu	%xmm0, _ZN10polynomialIdE4PI2IE(%rip)
	movq	$1, _ZGVN10polynomialIdE4PI2IE(%rip)
.LBB3_2:
	retq
.Lfunc_end3:
	.size	__cxx_global_var_init.3, .Lfunc_end3-__cxx_global_var_init.3
	.cfi_endproc

	.section	.text._ZN10polynomialIdED0Ev,"axG",@progbits,_ZN10polynomialIdED0Ev,comdat
	.weak	_ZN10polynomialIdED0Ev
	.p2align	4, 0x90
	.type	_ZN10polynomialIdED0Ev,@function
_ZN10polynomialIdED0Ev:                 # @_ZN10polynomialIdED0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 16
.Lcfi25:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV10polynomialIdE+16, (%rbx)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_2
# BB#1:
	callq	_ZdaPv
.LBB4_2:                                # %_ZN10polynomialIdED2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end4:
	.size	_ZN10polynomialIdED0Ev, .Lfunc_end4-_ZN10polynomialIdED0Ev
	.cfi_endproc

	.section	.text._ZN10polynomialIdE11stretch_fftEv,"axG",@progbits,_ZN10polynomialIdE11stretch_fftEv,comdat
	.weak	_ZN10polynomialIdE11stretch_fftEv
	.p2align	4, 0x90
	.type	_ZN10polynomialIdE11stretch_fftEv,@function
_ZN10polynomialIdE11stretch_fftEv:      # @_ZN10polynomialIdE11stretch_fftEv
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 96
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	16(%r15), %r12
	movl	$1, %eax
.LBB5_1:                                # =>This Inner Loop Header: Depth=1
	leaq	(%rax,%rax), %rbx
	cmpq	%rax, %r12
	jbe	.LBB5_4
# BB#2:                                 #   in Loop: Header=BB5_1 Depth=1
	leaq	(,%rax,4), %rcx
	cmpq	%rbx, %r12
	jbe	.LBB5_3
# BB#40:                                #   in Loop: Header=BB5_1 Depth=1
	leaq	(,%rax,8), %rbx
	cmpq	%rcx, %r12
	jbe	.LBB5_4
# BB#41:                                #   in Loop: Header=BB5_1 Depth=1
	movq	%rax, %rcx
	shlq	$4, %rcx
	cmpq	%rbx, %r12
	jbe	.LBB5_3
# BB#42:                                #   in Loop: Header=BB5_1 Depth=1
	movq	%rax, %rbx
	shlq	$5, %rbx
	cmpq	%rcx, %r12
	jbe	.LBB5_4
# BB#43:                                #   in Loop: Header=BB5_1 Depth=1
	movq	%rax, %rcx
	shlq	$6, %rcx
	cmpq	%rbx, %r12
	jbe	.LBB5_3
# BB#44:                                #   in Loop: Header=BB5_1 Depth=1
	movq	%rax, %rbx
	shlq	$7, %rbx
	cmpq	%rcx, %r12
	jbe	.LBB5_4
# BB#45:                                #   in Loop: Header=BB5_1 Depth=1
	shlq	$8, %rax
	cmpq	%rbx, %r12
	jbe	.LBB5_46
# BB#7:                                 #   in Loop: Header=BB5_1 Depth=1
	testq	%rax, %rax
	jne	.LBB5_1
# BB#8:                                 # %.noexc7.i
	movl	$16, %edi
	callq	__cxa_allocate_exception
	movq	%rax, %rbx
	leaq	24(%rsp), %r15
	movq	%r15, 8(%rsp)
	movq	$34, (%rsp)
.Ltmp49:
	leaq	8(%rsp), %rdi
	movq	%rsp, %rsi
	xorl	%edx, %edx
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm
.Ltmp50:
# BB#9:                                 # %.noexc
	movq	%rax, 8(%rsp)
	movq	(%rsp), %rcx
	movq	%rcx, 24(%rsp)
	movups	.L.str.4+16(%rip), %xmm0
	movups	%xmm0, 16(%rax)
	movups	.L.str.4(%rip), %xmm0
	movups	%xmm0, (%rax)
	movw	$26723, 32(%rax)        # imm = 0x6863
	movq	%rcx, 16(%rsp)
	movb	$0, (%rax,%rcx)
	movb	$1, %bpl
.Ltmp52:
	leaq	8(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_ZNSt14overflow_errorC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp53:
# BB#10:
	xorl	%ebp, %ebp
.Ltmp54:
	movl	$_ZTISt14overflow_error, %esi
	movl	$_ZNSt14overflow_errorD1Ev, %edx
	movq	%rbx, %rdi
	callq	__cxa_throw
.Ltmp55:
# BB#39:
.LBB5_3:
	movq	%rcx, %rbx
	jmp	.LBB5_4
.LBB5_46:
	movq	%rax, %rbx
.LBB5_4:
	movq	%rbx, %r14
	subq	%r12, %r14
	je	.LBB5_38
# BB#5:
	movq	8(%r15), %r13
	movq	%rbx, 16(%r15)
	movl	$8, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, 8(%r15)
	testq	%r12, %r12
	je	.LBB5_6
# BB#17:                                # %.lr.ph18.i.preheader
	cmpq	$3, %r12
	jbe	.LBB5_18
# BB#25:                                # %min.iters.checked
	movq	%r12, %rcx
	andq	$-4, %rcx
	je	.LBB5_18
# BB#26:                                # %vector.memcheck
	leaq	(%r13,%r12,8), %rdx
	cmpq	%rdx, %rax
	jae	.LBB5_28
# BB#27:                                # %vector.memcheck
	leaq	(%rax,%r12,8), %rdx
	cmpq	%rdx, %r13
	jae	.LBB5_28
.LBB5_18:
	xorl	%ecx, %ecx
.LBB5_19:                               # %.lr.ph18.i.preheader31
	movl	%r12d, %esi
	subl	%ecx, %esi
	leaq	-1(%r12), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB5_22
# BB#20:                                # %.lr.ph18.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB5_21:                               # %.lr.ph18.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rcx,8), %rdi
	movq	%rdi, (%rax,%rcx,8)
	incq	%rcx
	incq	%rsi
	jne	.LBB5_21
.LBB5_22:                               # %.lr.ph18.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB5_36
# BB#23:                                # %.lr.ph18.i.preheader31.new
	movq	%r12, %rdx
	subq	%rcx, %rdx
	leaq	56(%rax,%rcx,8), %rsi
	leaq	56(%r13,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB5_24:                               # %.lr.ph18.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rdi
	movq	%rdi, -56(%rsi)
	movq	-48(%rcx), %rdi
	movq	%rdi, -48(%rsi)
	movq	-40(%rcx), %rdi
	movq	%rdi, -40(%rsi)
	movq	-32(%rcx), %rdi
	movq	%rdi, -32(%rsi)
	movq	-24(%rcx), %rdi
	movq	%rdi, -24(%rsi)
	movq	-16(%rcx), %rdi
	movq	%rdi, -16(%rsi)
	movq	-8(%rcx), %rdi
	movq	%rdi, -8(%rsi)
	movq	(%rcx), %rdi
	movq	%rdi, (%rsi)
	addq	$64, %rsi
	addq	$64, %rcx
	addq	$-8, %rdx
	jne	.LBB5_24
	jmp	.LBB5_36
.LBB5_6:
	xorl	%r12d, %r12d
.LBB5_36:                               # %.preheader.i
	cmpq	%rbx, %r12
	jae	.LBB5_38
# BB#37:                                # %.lr.ph.i
	leaq	(%rax,%r12,8), %rdi
	leaq	1(%r12), %rdx
	cmpq	%rdx, %rbx
	cmovaq	%rbx, %rdx
	subq	%r12, %rdx
	shlq	$3, %rdx
	xorl	%esi, %esi
	callq	memset
.LBB5_38:                               # %_ZN10polynomialIdE7stretchEm.exit
	movq	%r14, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_28:                               # %vector.body.preheader
	leaq	-4(%rcx), %rdx
	movl	%edx, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB5_29
# BB#30:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_31:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r13,%rdi,8), %xmm0
	movups	16(%r13,%rdi,8), %xmm1
	movups	%xmm0, (%rax,%rdi,8)
	movups	%xmm1, 16(%rax,%rdi,8)
	addq	$4, %rdi
	incq	%rsi
	jne	.LBB5_31
	jmp	.LBB5_32
.LBB5_29:
	xorl	%edi, %edi
.LBB5_32:                               # %vector.body.prol.loopexit
	cmpq	$12, %rdx
	jb	.LBB5_35
# BB#33:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%rax,%rdi,8), %rsi
	leaq	112(%r13,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB5_34:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-16, %rdx
	jne	.LBB5_34
.LBB5_35:                               # %middle.block
	cmpq	%rcx, %r12
	jne	.LBB5_19
	jmp	.LBB5_36
.LBB5_11:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit.thread
.Ltmp51:
	movq	%rax, %r14
	jmp	.LBB5_15
.LBB5_12:
.Ltmp56:
	movq	%rax, %r14
	movq	8(%rsp), %rdi
	cmpq	%r15, %rdi
	je	.LBB5_14
# BB#13:
	callq	_ZdlPv
.LBB5_14:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit
	testb	%bpl, %bpl
	je	.LBB5_16
.LBB5_15:
	movq	%rbx, %rdi
	callq	__cxa_free_exception
.LBB5_16:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZN10polynomialIdE11stretch_fftEv, .Lfunc_end5-_ZN10polynomialIdE11stretch_fftEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp49-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp49
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin2   #     jumps to .Ltmp51
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp55-.Ltmp52         #   Call between .Ltmp52 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin2   #     jumps to .Ltmp56
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp55     #   Call between .Ltmp55 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI6_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_2:
	.quad	4607182418800017408     # double 1
	.section	.text._ZN10polynomialIdE3fftERKS0_,"axG",@progbits,_ZN10polynomialIdE3fftERKS0_,comdat
	.weak	_ZN10polynomialIdE3fftERKS0_
	.p2align	4, 0x90
	.type	_ZN10polynomialIdE3fftERKS0_,@function
_ZN10polynomialIdE3fftERKS0_:           # @_ZN10polynomialIdE3fftERKS0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 160
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movq	16(%rsi), %r12
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	$1, %eax
	movl	$3, %ecx
	.p2align	4, 0x90
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	cmpq	%r12, %rax
	jae	.LBB6_4
# BB#2:                                 #   in Loop: Header=BB6_1 Depth=1
	leaq	(%rax,%rax), %rdx
	cmpq	%r12, %rdx
	jae	.LBB6_3
# BB#34:                                #   in Loop: Header=BB6_1 Depth=1
	leaq	(,%rax,4), %rdx
	cmpq	%r12, %rdx
	jae	.LBB6_35
# BB#36:                                #   in Loop: Header=BB6_1 Depth=1
	leaq	(,%rax,8), %rdx
	cmpq	%r12, %rdx
	jae	.LBB6_37
# BB#38:                                #   in Loop: Header=BB6_1 Depth=1
	addq	$4, (%rsp)              # 8-byte Folded Spill
	shlq	$4, %rax
	addq	$4, %rcx
	testq	%rax, %rax
	jne	.LBB6_1
	jmp	.LBB6_4
.LBB6_3:
	orq	$1, (%rsp)              # 8-byte Folded Spill
	jmp	.LBB6_4
.LBB6_35:
	orq	$2, (%rsp)              # 8-byte Folded Spill
	jmp	.LBB6_4
.LBB6_37:
	movq	%rcx, (%rsp)            # 8-byte Spill
.LBB6_4:                                # %_ZN10polynomialIdE4log2Em.exit
	xorl	%r13d, %r13d
	movl	$1, %eax
	movl	$3, %ecx
	.p2align	4, 0x90
.LBB6_5:                                # =>This Inner Loop Header: Depth=1
	cmpq	%r12, %rax
	jae	.LBB6_8
# BB#6:                                 #   in Loop: Header=BB6_5 Depth=1
	leaq	(%rax,%rax), %rdx
	cmpq	%r12, %rdx
	jae	.LBB6_7
# BB#29:                                #   in Loop: Header=BB6_5 Depth=1
	leaq	(,%rax,4), %rdx
	cmpq	%r12, %rdx
	jae	.LBB6_30
# BB#31:                                #   in Loop: Header=BB6_5 Depth=1
	leaq	(,%rax,8), %rdx
	cmpq	%r12, %rdx
	jae	.LBB6_32
# BB#33:                                #   in Loop: Header=BB6_5 Depth=1
	addq	$4, %r13
	shlq	$4, %rax
	addq	$4, %rcx
	testq	%rax, %rax
	jne	.LBB6_5
	jmp	.LBB6_8
.LBB6_7:
	orq	$1, %r13
	jmp	.LBB6_8
.LBB6_30:
	orq	$2, %r13
	jmp	.LBB6_8
.LBB6_32:
	movq	%rcx, %r13
.LBB6_8:                                # %_ZN10polynomialIdE4log2Em.exit.i
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	$_ZTV10polynomialISt7complexIdEE+16, (%rbp)
	movq	$0, 8(%rbp)
	movq	%r12, 16(%rbp)
	movl	$16, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	movq	%rbp, %r14
	callq	_Znam
	movq	%rax, %rbx
	testq	%r12, %r12
	je	.LBB6_9
# BB#10:                                # %.lr.ph.i
	movq	%r12, %rdx
	shlq	$4, %rdx
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	memset
	movq	%rbx, 8(%r14)
	decl	%r13d
	movl	$1, %eax
	movl	%r13d, %ecx
	shll	%cl, %eax
	movslq	%eax, %r8
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	8(%rax), %r9
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB6_11:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_12 Depth 2
	movq	(%r9,%rbp,8), %r10
	xorl	%esi, %esi
	movl	$1, %ecx
	movq	%r8, %rdx
	.p2align	4, 0x90
.LBB6_12:                               #   Parent Loop BB6_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rdi
	andq	%rbp, %rdi
	cmovneq	%rdx, %rdi
	orq	%rdi, %rsi
	shrq	%rdx
	addq	%rcx, %rcx
	testq	%rdx, %rdx
	jne	.LBB6_12
# BB#13:                                # %_ZN10polynomialIdE9flip_bitsEmm.exit.i
                                        #   in Loop: Header=BB6_11 Depth=1
	shlq	$4, %rsi
	movq	%r10, (%rbx,%rsi)
	movq	$0, 8(%rbx,%rsi)
	incq	%rbp
	cmpq	%r12, %rbp
	jb	.LBB6_11
	jmp	.LBB6_14
.LBB6_9:                                # %_ZN10polynomialISt7complexIdEEC2Em.exit.thread.i
	movq	%rbx, 8(%r14)
	movq	%r14, %rax
.LBB6_14:                               # %_ZN10polynomialIdE11bit_reverseERKS0_.exit.preheader
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	je	.LBB6_28
# BB#15:                                # %.lr.ph130.preheader
	xorl	%ecx, %ecx
	movl	$1, %r12d
	movl	$2, %esi
	movq	%rax, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_16:                               # %.lr.ph130
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_17 Depth 2
                                        #       Child Loop BB6_19 Depth 3
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movd	%rsi, %xmm0
	punpckldq	.LCPI6_0(%rip), %xmm0 # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	subpd	.LCPI6_1(%rip), %xmm0
	pshufd	$78, %xmm0, %xmm2       # xmm2 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm2
	movsd	_ZN10polynomialIdE4PI2IE(%rip), %xmm0 # xmm0 = mem[0],zero
	movsd	_ZN10polynomialIdE4PI2IE+8(%rip), %xmm1 # xmm1 = mem[0],zero
	xorpd	%xmm3, %xmm3
	callq	__divdc3
	callq	cexp
	xorpd	%xmm6, %xmm6
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	movapd	%xmm0, %xmm8
	movapd	%xmm1, %xmm9
	leaq	-1(%r12), %r8
	movq	%rsi, %rbp
	shlq	$4, %rbp
	movq	%r12, 88(%rsp)          # 8-byte Spill
	shlq	$4, %r12
	movl	$8, %r9d
	xorl	%edi, %edi
	movsd	.LCPI6_2(%rip), %xmm7   # xmm7 = mem[0],zero
	jmp	.LBB6_17
.LBB6_25:                               #   in Loop: Header=BB6_17 Depth=2
	movapd	%xmm7, %xmm0
	movapd	%xmm6, %xmm1
	movapd	%xmm8, %xmm2
	movapd	%xmm9, %xmm3
	movsd	%xmm8, 24(%rsp)         # 8-byte Spill
	movsd	%xmm9, 16(%rsp)         # 8-byte Spill
	movq	%r8, %r14
	movq	%r9, %r15
	movq	%rdi, %r13
	callq	__muldc3
	movq	%r13, %rdi
	movq	%r15, %r9
	movq	%r14, %r8
	movsd	16(%rsp), %xmm9         # 8-byte Reload
                                        # xmm9 = mem[0],zero
	movsd	24(%rsp), %xmm8         # 8-byte Reload
                                        # xmm8 = mem[0],zero
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	jmp	.LBB6_26
	.p2align	4, 0x90
.LBB6_17:                               # %.preheader
                                        #   Parent Loop BB6_16 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_19 Depth 3
	movq	16(%rdx), %rcx
	decq	%rcx
	cmpq	%rcx, %rdi
	ja	.LBB6_23
# BB#18:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB6_17 Depth=2
	movq	%rbx, %r15
	addq	%r12, %r15
	movq	%r9, %r14
	movq	%rdi, %r13
	jmp	.LBB6_19
.LBB6_21:                               #   in Loop: Header=BB6_19 Depth=3
	movapd	%xmm7, %xmm0
	movapd	%xmm6, %xmm1
	movsd	%xmm8, 24(%rsp)         # 8-byte Spill
	movsd	%xmm9, 16(%rsp)         # 8-byte Spill
	movq	%r8, 80(%rsp)           # 8-byte Spill
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movsd	%xmm6, 64(%rsp)         # 8-byte Spill
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movsd	%xmm7, 48(%rsp)         # 8-byte Spill
	callq	__muldc3
	movsd	48(%rsp), %xmm7         # 8-byte Reload
                                        # xmm7 = mem[0],zero
	movq	56(%rsp), %rdi          # 8-byte Reload
	movsd	64(%rsp), %xmm6         # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movq	72(%rsp), %r9           # 8-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movsd	16(%rsp), %xmm9         # 8-byte Reload
                                        # xmm9 = mem[0],zero
	movsd	24(%rsp), %xmm8         # 8-byte Reload
                                        # xmm8 = mem[0],zero
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	jmp	.LBB6_22
	.p2align	4, 0x90
.LBB6_19:                               # %.lr.ph
                                        #   Parent Loop BB6_16 Depth=1
                                        #     Parent Loop BB6_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%r15,%r14), %xmm2    # xmm2 = mem[0],zero
	movsd	(%r15,%r14), %xmm3      # xmm3 = mem[0],zero
	movapd	%xmm7, %xmm0
	mulsd	%xmm2, %xmm0
	movapd	%xmm6, %xmm4
	mulsd	%xmm3, %xmm4
	movapd	%xmm7, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm6, %xmm1
	mulsd	%xmm2, %xmm1
	subsd	%xmm4, %xmm0
	addsd	%xmm5, %xmm1
	ucomisd	%xmm0, %xmm0
	jnp	.LBB6_22
# BB#20:                                # %.lr.ph
                                        #   in Loop: Header=BB6_19 Depth=3
	ucomisd	%xmm1, %xmm1
	jp	.LBB6_21
.LBB6_22:                               #   in Loop: Header=BB6_19 Depth=3
	movsd	-8(%rbx,%r14), %xmm2    # xmm2 = mem[0],zero
	movsd	(%rbx,%r14), %xmm3      # xmm3 = mem[0],zero
	movapd	%xmm0, %xmm4
	addsd	%xmm2, %xmm4
	movapd	%xmm1, %xmm5
	addsd	%xmm3, %xmm5
	movsd	%xmm4, -8(%rbx,%r14)
	movsd	%xmm5, (%rbx,%r14)
	subsd	%xmm0, %xmm2
	subsd	%xmm1, %xmm3
	movq	8(%rax), %rbx
	leaq	(%rbx,%r12), %rcx
	movsd	%xmm2, -8(%r14,%rcx)
	movsd	%xmm3, (%r14,%rcx)
	addq	%rsi, %r13
	movq	16(%rdx), %rcx
	decq	%rcx
	addq	%rbp, %r14
	cmpq	%rcx, %r13
	jbe	.LBB6_19
.LBB6_23:                               # %._crit_edge
                                        #   in Loop: Header=BB6_17 Depth=2
	movapd	%xmm8, %xmm0
	mulsd	%xmm7, %xmm0
	movapd	%xmm9, %xmm2
	mulsd	%xmm6, %xmm2
	movapd	%xmm9, %xmm3
	mulsd	%xmm7, %xmm3
	movapd	%xmm8, %xmm1
	mulsd	%xmm6, %xmm1
	subsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm1
	ucomisd	%xmm0, %xmm0
	jnp	.LBB6_26
# BB#24:                                # %._crit_edge
                                        #   in Loop: Header=BB6_17 Depth=2
	ucomisd	%xmm1, %xmm1
	jp	.LBB6_25
.LBB6_26:                               # %_ZNSt7complexIdEmLIdEERS0_RKS_IT_E.exit
                                        #   in Loop: Header=BB6_17 Depth=2
	incq	%rdi
	addq	$16, %r9
	cmpq	%r8, %rdi
	movapd	%xmm1, %xmm6
	movapd	%xmm0, %xmm7
	jbe	.LBB6_17
# BB#27:                                # %_ZN10polynomialIdE11bit_reverseERKS0_.exit
                                        #   in Loop: Header=BB6_16 Depth=1
	addq	%rsi, %rsi
	movq	88(%rsp), %r12          # 8-byte Reload
	addq	%r12, %r12
	movq	96(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	cmpq	(%rsp), %rcx            # 8-byte Folded Reload
	jne	.LBB6_16
.LBB6_28:                               # %_ZN10polynomialIdE11bit_reverseERKS0_.exit._crit_edge
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN10polynomialIdE3fftERKS0_, .Lfunc_end6-_ZN10polynomialIdE3fftERKS0_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI7_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
.LCPI7_1:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI7_2:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI7_3:
	.quad	4607182418800017408     # double 1
	.section	.text._ZN10polynomialIdE11inverse_fftERKS_ISt7complexIdEE,"axG",@progbits,_ZN10polynomialIdE11inverse_fftERKS_ISt7complexIdEE,comdat
	.weak	_ZN10polynomialIdE11inverse_fftERKS_ISt7complexIdEE
	.p2align	4, 0x90
	.type	_ZN10polynomialIdE11inverse_fftERKS_ISt7complexIdEE,@function
_ZN10polynomialIdE11inverse_fftERKS_ISt7complexIdEE: # @_ZN10polynomialIdE11inverse_fftERKS_ISt7complexIdEE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi58:
	.cfi_def_cfa_offset 160
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movq	16(%rsi), %r12
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	$1, %eax
	movl	$3, %ecx
	.p2align	4, 0x90
.LBB7_1:                                # =>This Inner Loop Header: Depth=1
	cmpq	%r12, %rax
	jae	.LBB7_4
# BB#2:                                 #   in Loop: Header=BB7_1 Depth=1
	leaq	(%rax,%rax), %rdx
	cmpq	%r12, %rdx
	jae	.LBB7_3
# BB#38:                                #   in Loop: Header=BB7_1 Depth=1
	leaq	(,%rax,4), %rdx
	cmpq	%r12, %rdx
	jae	.LBB7_39
# BB#40:                                #   in Loop: Header=BB7_1 Depth=1
	leaq	(,%rax,8), %rdx
	cmpq	%r12, %rdx
	jae	.LBB7_41
# BB#42:                                #   in Loop: Header=BB7_1 Depth=1
	addq	$4, (%rsp)              # 8-byte Folded Spill
	shlq	$4, %rax
	addq	$4, %rcx
	testq	%rax, %rax
	jne	.LBB7_1
	jmp	.LBB7_4
.LBB7_3:
	orq	$1, (%rsp)              # 8-byte Folded Spill
	jmp	.LBB7_4
.LBB7_39:
	orq	$2, (%rsp)              # 8-byte Folded Spill
	jmp	.LBB7_4
.LBB7_41:
	movq	%rcx, (%rsp)            # 8-byte Spill
.LBB7_4:                                # %_ZN10polynomialIdE4log2Em.exit
	xorl	%r13d, %r13d
	movl	$1, %eax
	movl	$3, %ecx
	.p2align	4, 0x90
.LBB7_5:                                # =>This Inner Loop Header: Depth=1
	cmpq	%r12, %rax
	jae	.LBB7_8
# BB#6:                                 #   in Loop: Header=BB7_5 Depth=1
	leaq	(%rax,%rax), %rdx
	cmpq	%r12, %rdx
	jae	.LBB7_7
# BB#33:                                #   in Loop: Header=BB7_5 Depth=1
	leaq	(,%rax,4), %rdx
	cmpq	%r12, %rdx
	jae	.LBB7_34
# BB#35:                                #   in Loop: Header=BB7_5 Depth=1
	leaq	(,%rax,8), %rdx
	cmpq	%r12, %rdx
	jae	.LBB7_36
# BB#37:                                #   in Loop: Header=BB7_5 Depth=1
	addq	$4, %r13
	shlq	$4, %rax
	addq	$4, %rcx
	testq	%rax, %rax
	jne	.LBB7_5
	jmp	.LBB7_8
.LBB7_7:
	orq	$1, %r13
	jmp	.LBB7_8
.LBB7_34:
	orq	$2, %r13
	jmp	.LBB7_8
.LBB7_36:
	movq	%rcx, %r13
.LBB7_8:                                # %_ZN10polynomialIdE4log2Em.exit.i
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	$_ZTV10polynomialISt7complexIdEE+16, (%rbp)
	movq	$0, 8(%rbp)
	movq	%r12, 16(%rbp)
	movl	$16, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	movq	%rbp, %r14
	callq	_Znam
	movq	%rax, %rbx
	testq	%r12, %r12
	je	.LBB7_9
# BB#10:                                # %.lr.ph.i
	movq	%r12, %rdx
	shlq	$4, %rdx
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	memset
	movq	%rbx, 8(%r14)
	decl	%r13d
	movl	$1, %eax
	movl	%r13d, %ecx
	shll	%cl, %eax
	movslq	%eax, %r8
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	8(%r10), %r9
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB7_11:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_12 Depth 2
	movq	%rbp, %rdx
	shlq	$4, %rdx
	movupd	(%r9,%rdx), %xmm0
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r8, %rdi
	.p2align	4, 0x90
.LBB7_12:                               #   Parent Loop BB7_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rsi
	andq	%rbp, %rsi
	cmovneq	%rdi, %rsi
	orq	%rsi, %rdx
	shrq	%rdi
	addq	%rcx, %rcx
	testq	%rdi, %rdi
	jne	.LBB7_12
# BB#13:                                # %_ZN10polynomialIdE9flip_bitsEmm.exit.i
                                        #   in Loop: Header=BB7_11 Depth=1
	shlq	$4, %rdx
	movupd	%xmm0, (%rbx,%rdx)
	incq	%rbp
	cmpq	%r12, %rbp
	jb	.LBB7_11
	jmp	.LBB7_14
.LBB7_9:                                # %_ZN10polynomialISt7complexIdEEC2Em.exit.thread.i
	movq	%rbx, 8(%r14)
	movq	%r14, %rax
	movq	8(%rsp), %r10           # 8-byte Reload
.LBB7_14:                               # %_ZN10polynomialIdE11bit_reverseERKS_ISt7complexIdEE.exit.preheader
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	je	.LBB7_29
# BB#15:                                # %.lr.ph152.preheader
	xorl	%ecx, %ecx
	movl	$1, %r12d
	movl	$2, %edx
	movq	%rax, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB7_16:                               # %.lr.ph152
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_17 Depth 2
                                        #       Child Loop BB7_19 Depth 3
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movsd	_ZN10polynomialIdE4PI2IE(%rip), %xmm0 # xmm0 = mem[0],zero
	movapd	.LCPI7_0(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00]
	movapd	%xmm1, %xmm2
	xorpd	%xmm2, %xmm0
	movsd	_ZN10polynomialIdE4PI2IE+8(%rip), %xmm1 # xmm1 = mem[0],zero
	xorpd	%xmm2, %xmm1
	movd	%rdx, %xmm3
	punpckldq	.LCPI7_1(%rip), %xmm3 # xmm3 = xmm3[0],mem[0],xmm3[1],mem[1]
	subpd	.LCPI7_2(%rip), %xmm3
	pshufd	$78, %xmm3, %xmm2       # xmm2 = xmm3[2,3,0,1]
	addpd	%xmm3, %xmm2
	xorpd	%xmm3, %xmm3
	callq	__divdc3
	callq	cexp
	xorpd	%xmm6, %xmm6
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	movapd	%xmm0, %xmm8
	movapd	%xmm1, %xmm9
	leaq	-1(%r12), %r8
	movq	%rdx, %rbp
	shlq	$4, %rbp
	movq	%r12, 88(%rsp)          # 8-byte Spill
	shlq	$4, %r12
	movl	$8, %edi
	xorl	%esi, %esi
	movsd	.LCPI7_3(%rip), %xmm7   # xmm7 = mem[0],zero
	jmp	.LBB7_17
.LBB7_25:                               #   in Loop: Header=BB7_17 Depth=2
	movapd	%xmm7, %xmm0
	movapd	%xmm6, %xmm1
	movapd	%xmm8, %xmm2
	movapd	%xmm9, %xmm3
	movsd	%xmm8, 24(%rsp)         # 8-byte Spill
	movsd	%xmm9, 16(%rsp)         # 8-byte Spill
	movq	%r8, %r14
	movq	%rdi, %r15
	movq	%rsi, %r13
	callq	__muldc3
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r14, %r8
	movsd	16(%rsp), %xmm9         # 8-byte Reload
                                        # xmm9 = mem[0],zero
	movsd	24(%rsp), %xmm8         # 8-byte Reload
                                        # xmm8 = mem[0],zero
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	jmp	.LBB7_26
	.p2align	4, 0x90
.LBB7_17:                               # %.preheader139
                                        #   Parent Loop BB7_16 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_19 Depth 3
	movq	16(%r10), %rcx
	decq	%rcx
	cmpq	%rcx, %rsi
	ja	.LBB7_23
# BB#18:                                # %.lr.ph142.preheader
                                        #   in Loop: Header=BB7_17 Depth=2
	movq	%rbx, %r14
	addq	%r12, %r14
	movq	%rdi, %r15
	movq	%rsi, %r13
	jmp	.LBB7_19
.LBB7_21:                               #   in Loop: Header=BB7_19 Depth=3
	movapd	%xmm7, %xmm0
	movapd	%xmm6, %xmm1
	movsd	%xmm8, 24(%rsp)         # 8-byte Spill
	movsd	%xmm9, 16(%rsp)         # 8-byte Spill
	movq	%r8, 80(%rsp)           # 8-byte Spill
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	movsd	%xmm6, 64(%rsp)         # 8-byte Spill
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movsd	%xmm7, 48(%rsp)         # 8-byte Spill
	callq	__muldc3
	movsd	48(%rsp), %xmm7         # 8-byte Reload
                                        # xmm7 = mem[0],zero
	movq	56(%rsp), %rsi          # 8-byte Reload
	movsd	64(%rsp), %xmm6         # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movsd	16(%rsp), %xmm9         # 8-byte Reload
                                        # xmm9 = mem[0],zero
	movsd	24(%rsp), %xmm8         # 8-byte Reload
                                        # xmm8 = mem[0],zero
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	jmp	.LBB7_22
	.p2align	4, 0x90
.LBB7_19:                               # %.lr.ph142
                                        #   Parent Loop BB7_16 Depth=1
                                        #     Parent Loop BB7_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%r14,%r15), %xmm2    # xmm2 = mem[0],zero
	movsd	(%r14,%r15), %xmm3      # xmm3 = mem[0],zero
	movapd	%xmm7, %xmm0
	mulsd	%xmm2, %xmm0
	movapd	%xmm6, %xmm4
	mulsd	%xmm3, %xmm4
	movapd	%xmm7, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm6, %xmm1
	mulsd	%xmm2, %xmm1
	subsd	%xmm4, %xmm0
	addsd	%xmm5, %xmm1
	ucomisd	%xmm0, %xmm0
	jnp	.LBB7_22
# BB#20:                                # %.lr.ph142
                                        #   in Loop: Header=BB7_19 Depth=3
	ucomisd	%xmm1, %xmm1
	jp	.LBB7_21
.LBB7_22:                               #   in Loop: Header=BB7_19 Depth=3
	movsd	-8(%rbx,%r15), %xmm2    # xmm2 = mem[0],zero
	movsd	(%rbx,%r15), %xmm3      # xmm3 = mem[0],zero
	movapd	%xmm0, %xmm4
	addsd	%xmm2, %xmm4
	movapd	%xmm1, %xmm5
	addsd	%xmm3, %xmm5
	movsd	%xmm4, -8(%rbx,%r15)
	movsd	%xmm5, (%rbx,%r15)
	subsd	%xmm0, %xmm2
	subsd	%xmm1, %xmm3
	movq	8(%rax), %rbx
	leaq	(%rbx,%r12), %rcx
	movsd	%xmm2, -8(%r15,%rcx)
	movsd	%xmm3, (%r15,%rcx)
	addq	%rdx, %r13
	movq	16(%r10), %rcx
	decq	%rcx
	addq	%rbp, %r15
	cmpq	%rcx, %r13
	jbe	.LBB7_19
.LBB7_23:                               # %._crit_edge143
                                        #   in Loop: Header=BB7_17 Depth=2
	movapd	%xmm8, %xmm0
	mulsd	%xmm7, %xmm0
	movapd	%xmm9, %xmm2
	mulsd	%xmm6, %xmm2
	movapd	%xmm9, %xmm3
	mulsd	%xmm7, %xmm3
	movapd	%xmm8, %xmm1
	mulsd	%xmm6, %xmm1
	subsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm1
	ucomisd	%xmm0, %xmm0
	jnp	.LBB7_26
# BB#24:                                # %._crit_edge143
                                        #   in Loop: Header=BB7_17 Depth=2
	ucomisd	%xmm1, %xmm1
	jp	.LBB7_25
.LBB7_26:                               # %_ZNSt7complexIdEmLIdEERS0_RKS_IT_E.exit
                                        #   in Loop: Header=BB7_17 Depth=2
	incq	%rsi
	addq	$16, %rdi
	cmpq	%r8, %rsi
	movapd	%xmm1, %xmm6
	movapd	%xmm0, %xmm7
	jbe	.LBB7_17
# BB#27:                                # %_ZN10polynomialIdE11bit_reverseERKS_ISt7complexIdEE.exit
                                        #   in Loop: Header=BB7_16 Depth=1
	addq	%rdx, %rdx
	movq	88(%rsp), %r12          # 8-byte Reload
	addq	%r12, %r12
	movq	96(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	cmpq	(%rsp), %rcx            # 8-byte Folded Reload
	jne	.LBB7_16
# BB#28:                                # %.preheader.loopexit
	movq	16(%r10), %r12
.LBB7_29:                               # %.preheader
	testq	%r12, %r12
	je	.LBB7_32
# BB#30:                                # %.lr.ph.preheader
	xorl	%ecx, %ecx
	movdqa	.LCPI7_1(%rip), %xmm0   # xmm0 = [1127219200,1160773632,0,0]
	movapd	.LCPI7_2(%rip), %xmm1   # xmm1 = [4.503600e+15,1.934281e+25]
	.p2align	4, 0x90
.LBB7_31:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movd	%r12, %xmm2
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	subpd	%xmm1, %xmm2
	pshufd	$78, %xmm2, %xmm3       # xmm3 = xmm2[2,3,0,1]
	addpd	%xmm2, %xmm3
	movupd	(%rbx), %xmm2
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	divpd	%xmm3, %xmm2
	movupd	%xmm2, (%rbx)
	incq	%rcx
	movq	16(%r10), %r12
	addq	$16, %rbx
	cmpq	%r12, %rcx
	jb	.LBB7_31
.LBB7_32:                               # %._crit_edge
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_ZN10polynomialIdE11inverse_fftERKS_ISt7complexIdEE, .Lfunc_end7-_ZN10polynomialIdE11inverse_fftERKS_ISt7complexIdEE
	.cfi_endproc

	.section	.text._ZN10polynomialISt7complexIdEED2Ev,"axG",@progbits,_ZN10polynomialISt7complexIdEED2Ev,comdat
	.weak	_ZN10polynomialISt7complexIdEED2Ev
	.p2align	4, 0x90
	.type	_ZN10polynomialISt7complexIdEED2Ev,@function
_ZN10polynomialISt7complexIdEED2Ev:     # @_ZN10polynomialISt7complexIdEED2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV10polynomialISt7complexIdEE+16, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB8_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB8_1:                                # %_ZN10polynomialISt7complexIdEE7releaseEv.exit
	retq
.Lfunc_end8:
	.size	_ZN10polynomialISt7complexIdEED2Ev, .Lfunc_end8-_ZN10polynomialISt7complexIdEED2Ev
	.cfi_endproc

	.section	.text._ZN10polynomialISt7complexIdEED0Ev,"axG",@progbits,_ZN10polynomialISt7complexIdEED0Ev,comdat
	.weak	_ZN10polynomialISt7complexIdEED0Ev
	.p2align	4, 0x90
	.type	_ZN10polynomialISt7complexIdEED0Ev,@function
_ZN10polynomialISt7complexIdEED0Ev:     # @_ZN10polynomialISt7complexIdEED0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 16
.Lcfi66:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV10polynomialISt7complexIdEE+16, (%rbx)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_2
# BB#1:
	callq	_ZdaPv
.LBB9_2:                                # %_ZN10polynomialISt7complexIdEED2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end9:
	.size	_ZN10polynomialISt7complexIdEED0Ev, .Lfunc_end9-_ZN10polynomialISt7complexIdEED0Ev
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_fftbench.ii,@function
_GLOBAL__sub_I_fftbench.ii:             # @_GLOBAL__sub_I_fftbench.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi67:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end10:
	.size	_GLOBAL__sub_I_fftbench.ii, .Lfunc_end10-_GLOBAL__sub_I_fftbench.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"-ga"
	.size	.L.str, 4

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\nfftbench (Std. C++) run time: "
	.size	.L.str.1, 32

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\n\n"
	.size	.L.str.2, 3

	.type	_ZN10polynomialIdE4PI2IE,@object # @_ZN10polynomialIdE4PI2IE
	.section	.bss._ZN10polynomialIdE4PI2IE,"aGw",@nobits,_ZN10polynomialIdE4PI2IE,comdat
	.weak	_ZN10polynomialIdE4PI2IE
	.p2align	3
_ZN10polynomialIdE4PI2IE:
	.zero	16
	.size	_ZN10polynomialIdE4PI2IE, 16

	.type	_ZGVN10polynomialIdE4PI2IE,@object # @_ZGVN10polynomialIdE4PI2IE
	.section	.bss._ZGVN10polynomialIdE4PI2IE,"aGw",@nobits,_ZN10polynomialIdE4PI2IE,comdat
	.weak	_ZGVN10polynomialIdE4PI2IE
	.p2align	3
_ZGVN10polynomialIdE4PI2IE:
	.quad	0                       # 0x0
	.size	_ZGVN10polynomialIdE4PI2IE, 8

	.type	_ZZL13random_doublevE4seed,@object # @_ZZL13random_doublevE4seed
	.data
	.p2align	3
_ZZL13random_doublevE4seed:
	.quad	1325                    # 0x52d
	.size	_ZZL13random_doublevE4seed, 8

	.type	_ZTV10polynomialIdE,@object # @_ZTV10polynomialIdE
	.section	.rodata._ZTV10polynomialIdE,"aG",@progbits,_ZTV10polynomialIdE,comdat
	.weak	_ZTV10polynomialIdE
	.p2align	3
_ZTV10polynomialIdE:
	.quad	0
	.quad	_ZTI10polynomialIdE
	.quad	_ZN10polynomialIdED2Ev
	.quad	_ZN10polynomialIdED0Ev
	.size	_ZTV10polynomialIdE, 32

	.type	_ZTS10polynomialIdE,@object # @_ZTS10polynomialIdE
	.section	.rodata._ZTS10polynomialIdE,"aG",@progbits,_ZTS10polynomialIdE,comdat
	.weak	_ZTS10polynomialIdE
_ZTS10polynomialIdE:
	.asciz	"10polynomialIdE"
	.size	_ZTS10polynomialIdE, 16

	.type	_ZTI10polynomialIdE,@object # @_ZTI10polynomialIdE
	.section	.rodata._ZTI10polynomialIdE,"aG",@progbits,_ZTI10polynomialIdE,comdat
	.weak	_ZTI10polynomialIdE
	.p2align	3
_ZTI10polynomialIdE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS10polynomialIdE
	.size	_ZTI10polynomialIdE, 16

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.4:
	.asciz	"overflow in fft polynomial stretch"
	.size	.L.str.4, 35

	.type	_ZTV10polynomialISt7complexIdEE,@object # @_ZTV10polynomialISt7complexIdEE
	.section	.rodata._ZTV10polynomialISt7complexIdEE,"aG",@progbits,_ZTV10polynomialISt7complexIdEE,comdat
	.weak	_ZTV10polynomialISt7complexIdEE
	.p2align	3
_ZTV10polynomialISt7complexIdEE:
	.quad	0
	.quad	_ZTI10polynomialISt7complexIdEE
	.quad	_ZN10polynomialISt7complexIdEED2Ev
	.quad	_ZN10polynomialISt7complexIdEED0Ev
	.size	_ZTV10polynomialISt7complexIdEE, 32

	.type	_ZTS10polynomialISt7complexIdEE,@object # @_ZTS10polynomialISt7complexIdEE
	.section	.rodata._ZTS10polynomialISt7complexIdEE,"aG",@progbits,_ZTS10polynomialISt7complexIdEE,comdat
	.weak	_ZTS10polynomialISt7complexIdEE
	.p2align	4
_ZTS10polynomialISt7complexIdEE:
	.asciz	"10polynomialISt7complexIdEE"
	.size	_ZTS10polynomialISt7complexIdEE, 28

	.type	_ZTI10polynomialISt7complexIdEE,@object # @_ZTI10polynomialISt7complexIdEE
	.section	.rodata._ZTI10polynomialISt7complexIdEE,"aG",@progbits,_ZTI10polynomialISt7complexIdEE,comdat
	.weak	_ZTI10polynomialISt7complexIdEE
	.p2align	3
_ZTI10polynomialISt7complexIdEE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS10polynomialISt7complexIdEE
	.size	_ZTI10polynomialISt7complexIdEE, 16

	.section	.init_array,"aGw",@init_array,_ZN10polynomialIdE4PI2IE,comdat
	.p2align	3
	.quad	__cxx_global_var_init.3
	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_fftbench.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
