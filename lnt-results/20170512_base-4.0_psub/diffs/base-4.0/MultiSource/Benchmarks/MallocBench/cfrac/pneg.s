	.text
	.file	"pneg.bc"
	.globl	pneg
	.p2align	4, 0x90
	.type	pneg,@function
pneg:                                   # @pneg
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB0_2
# BB#1:
	incw	(%rbx)
.LBB0_2:
	movzwl	4(%rbx), %edi
	xorl	%eax, %eax
	callq	palloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_8
# BB#3:
	movb	6(%rbx), %al
	movb	%al, 6(%r14)
	movq	%rbx, %rdi
	callq	pcmpz
	testl	%eax, %eax
	je	.LBB0_5
# BB#4:
	cmpb	$0, 6(%r14)
	sete	6(%r14)
.LBB0_5:
	movq	%r14, %rdi
	addq	$8, %rdi
	leaq	8(%rbx), %rsi
	movzwl	4(%rbx), %edx
	addq	%rdx, %rdx
	callq	memcpy
	decw	(%rbx)
	jne	.LBB0_7
# BB#6:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	pfree
.LBB0_7:
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	presult                 # TAILCALL
.LBB0_8:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	pneg, .Lfunc_end0-pneg
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
