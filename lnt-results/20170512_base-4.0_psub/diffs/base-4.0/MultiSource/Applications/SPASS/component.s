	.text
	.file	"component.bc"
	.globl	literal_Create
	.p2align	4, 0x90
	.type	literal_Create,@function
literal_Create:                         # @literal_Create
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %ebp
	movl	%edi, %ebx
	movl	$16, %edi
	callq	memory_Malloc
	movl	%ebx, (%rax)
	movl	%ebp, 4(%rax)
	movq	%r14, 8(%rax)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	literal_Create, .Lfunc_end0-literal_Create
	.cfi_endproc

	.globl	literal_Delete
	.p2align	4, 0x90
	.type	literal_Delete,@function
literal_Delete:                         # @literal_Delete
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.LBB1_2
	.p2align	4, 0x90
.LBB1_1:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB1_1
.LBB1_2:                                # %list_Delete.exit
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdi)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rdi, (%rax)
	retq
.Lfunc_end1:
	.size	literal_Delete, .Lfunc_end1-literal_Delete
	.cfi_endproc

	.globl	litptr_Create
	.p2align	4, 0x90
	.type	litptr_Create,@function
litptr_Create:                          # @litptr_Create
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 64
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	callq	list_Length
	movl	%eax, %ebp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movl	%ebp, 8(%r14)
	testl	%ebp, %ebp
	jle	.LBB2_4
# BB#1:
	shll	$3, %ebp
	movl	%ebp, %edi
	callq	memory_Malloc
	movq	%rax, (%r14)
	testq	%rbx, %rbx
	je	.LBB2_5
# BB#2:                                 # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	movl	8(%rbx), %r12d
	movq	(%r15), %r13
	movq	8(%r15), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movl	$0, (%rax)
	movl	%r12d, 4(%rax)
	movq	%r15, 8(%rax)
	movq	(%r14), %rcx
	movq	%rax, (%rcx,%rbp)
	movq	(%rbx), %rbx
	addq	$8, %rbp
	testq	%rbx, %rbx
	movq	%r13, %r15
	jne	.LBB2_3
	jmp	.LBB2_5
.LBB2_4:
	movq	$0, (%r14)
.LBB2_5:                                # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	litptr_Create, .Lfunc_end2-litptr_Create
	.cfi_endproc

	.globl	litptr_Delete
	.p2align	4, 0x90
	.type	litptr_Delete,@function
litptr_Delete:                          # @litptr_Delete
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -24
.Lcfi23:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	8(%r14), %r8d
	testl	%r8d, %r8d
	jle	.LBB3_12
# BB#1:                                 # %.lr.ph
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_3 Depth 2
	movq	(%r14), %rdx
	movq	(%rdx,%rax,8), %rdx
	movq	8(%rdx), %rsi
	testq	%rsi, %rsi
	je	.LBB3_4
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph.i.i
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rdi
	movq	memory_ARRAY+128(%rip), %rbx
	movslq	32(%rbx), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rbx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rdi, %rdi
	movq	%rdi, %rsi
	jne	.LBB3_3
.LBB3_4:                                # %literal_Delete.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rdx, (%rcx)
	incq	%rax
	cmpq	%r8, %rax
	jne	.LBB3_2
# BB#5:                                 # %._crit_edge
	movq	(%r14), %rdi
	shll	$3, %r8d
	cmpl	$1024, %r8d             # imm = 0x400
	jae	.LBB3_6
# BB#11:
	movl	%r8d, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB3_12
.LBB3_6:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%r8d, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%r8d, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebx
	cmovneq	%rdx, %rbx
	movq	%r8, (%rbx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB3_8
# BB#7:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB3_8:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB3_10
# BB#9:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB3_10:
	addq	$-16, %rdi
	callq	free
.LBB3_12:                               # %memory_Free.exit
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r14, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	litptr_Delete, .Lfunc_end3-litptr_Delete
	.cfi_endproc

	.globl	litptr_Print
	.p2align	4, 0x90
	.type	litptr_Print,@function
litptr_Print:                           # @litptr_Print
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -32
.Lcfi28:
	.cfi_offset %r14, -24
.Lcfi29:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movl	8(%r15), %r14d
	testl	%r14d, %r14d
	jle	.LBB4_7
# BB#1:                                 # %.lr.ph
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movl	$.L.str.2, %edi
	callq	puts
	movq	stdout(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$7, %esi
	movl	$1, %edx
	callq	fwrite
	movq	(%r15), %rax
	movq	(%rax,%rbx,8), %rax
	cmpl	$0, (%rax)
	je	.LBB4_4
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	movl	$.L.str.4, %edi
	jmp	.LBB4_5
	.p2align	4, 0x90
.LBB4_4:                                #   in Loop: Header=BB4_2 Depth=1
	movl	$.L.str.5, %edi
.LBB4_5:                                #   in Loop: Header=BB4_2 Depth=1
	callq	puts
	movq	(%r15), %rax
	movq	(%rax,%rbx,8), %rax
	movl	4(%rax), %esi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	printf
	movq	stdout(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$12, %esi
	movl	$1, %edx
	callq	fwrite
	movq	(%r15), %rax
	movq	(%rax,%rbx,8), %rax
	movq	8(%rax), %rsi
	movl	$symbol_Print, %edi
	callq	list_Apply
	movl	$.L.str.8, %edi
	callq	puts
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB4_2
# BB#6:                                 # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB4_7:
	movl	$.L.str.9, %edi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	puts                    # TAILCALL
.Lfunc_end4:
	.size	litptr_Print, .Lfunc_end4-litptr_Print
	.cfi_endproc

	.globl	litptr_AllUsed
	.p2align	4, 0x90
	.type	litptr_AllUsed,@function
litptr_AllUsed:                         # @litptr_AllUsed
	.cfi_startproc
# BB#0:
	movslq	8(%rdi), %rcx
	testq	%rcx, %rcx
	movl	$1, %eax
	jle	.LBB5_5
# BB#1:                                 # %.lr.ph
	movq	(%rdi), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rsi,8), %rdi
	cmpl	$0, (%rdi)
	je	.LBB5_4
# BB#2:                                 #   in Loop: Header=BB5_3 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB5_3
	jmp	.LBB5_5
.LBB5_4:
	xorl	%eax, %eax
.LBB5_5:                                # %._crit_edge
	retq
.Lfunc_end5:
	.size	litptr_AllUsed, .Lfunc_end5-litptr_AllUsed
	.cfi_endproc

	.globl	subs_CompList
	.p2align	4, 0x90
	.type	subs_CompList,@function
subs_CompList:                          # @subs_CompList
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi36:
	.cfi_def_cfa_offset 80
.Lcfi37:
	.cfi_offset %rbx, -56
.Lcfi38:
	.cfi_offset %r12, -48
.Lcfi39:
	.cfi_offset %r13, -40
.Lcfi40:
	.cfi_offset %r14, -32
.Lcfi41:
	.cfi_offset %r15, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movl	8(%r13), %r14d
	testl	%r14d, %r14d
	jle	.LBB6_9
# BB#1:                                 # %.lr.ph114
	leal	1(%r14), %r15d
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
	movq	(%r13), %rax
	movslq	%ebp, %rbx
	movq	(%rax,%rbx,8), %rax
	cmpl	$0, (%rax)
	jne	.LBB6_4
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rax, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	$0, (%rax)
	movq	(%r13), %rax
	movq	(%rax,%rbx,8), %rax
	movslq	4(%rax), %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r12, 8(%rbp)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, (%rbp)
	movq	(%r13), %rax
	movq	(%rax,%rbx,8), %rax
	movl	$1, (%rax)
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	printf
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movl	%r15d, %ebp
.LBB6_4:                                #   in Loop: Header=BB6_2 Depth=1
	incl	%ebp
	cmpl	%r14d, %ebp
	jl	.LBB6_2
# BB#5:                                 # %._crit_edge115
	cmpl	%r14d, %ebp
	jne	.LBB6_10
# BB#6:
	movq	16(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB6_23
# BB#7:                                 # %.lr.ph.i89.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	.p2align	4, 0x90
.LBB6_8:                                # %.lr.ph.i89
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rdi, (%rcx)
	testq	%rsi, %rsi
	movq	%rsi, %rdi
	jne	.LBB6_8
	jmp	.LBB6_24
.LBB6_9:
	xorl	%eax, %eax
	jmp	.LBB6_24
.LBB6_10:
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_11:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_13 Depth 2
                                        #       Child Loop BB6_14 Depth 3
	testq	%rbp, %rbp
	je	.LBB6_23
# BB#12:                                # %.lr.ph104.split.us.preheader
                                        #   in Loop: Header=BB6_11 Depth=1
	xorl	%r15d, %r15d
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB6_13:                               # %.lr.ph104.split.us
                                        #   Parent Loop BB6_11 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_14 Depth 3
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	8(%rax), %rax
	movslq	%eax, %r12
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_14:                               #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r13), %rax
	movq	(%rax,%rbx,8), %rax
	cmpl	$0, (%rax)
	jne	.LBB6_17
# BB#15:                                #   in Loop: Header=BB6_14 Depth=3
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	printf
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movq	(%r13), %rax
	movq	(%rax,%r12,8), %rcx
	movq	8(%rcx), %rdi
	movq	(%rax,%rbx,8), %rax
	movq	8(%rax), %rsi
	callq	list_HasIntersection
	testl	%eax, %eax
	je	.LBB6_17
# BB#16:                                #   in Loop: Header=BB6_14 Depth=3
	movl	$.L.str.14, %edi
	callq	puts
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, %rcx
	movq	%rax, %rbp
	movq	%rbx, 8(%rbp)
	movq	%rcx, (%rbp)
	movq	(%r13), %rax
	movq	(%rax,%rbx,8), %rax
	movslq	4(%rax), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, (%rax)
	movq	(%r13), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movl	$1, (%rcx)
	movl	$1, %r15d
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB6_17:                               #   in Loop: Header=BB6_14 Depth=3
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB6_14
# BB#18:                                # %._crit_edge.us
                                        #   in Loop: Header=BB6_13 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB6_13
# BB#19:                                # %._crit_edge105
                                        #   in Loop: Header=BB6_11 Depth=1
	testl	%r15d, %r15d
	jne	.LBB6_11
# BB#20:
	testq	%rbp, %rbp
	movq	8(%rsp), %rax           # 8-byte Reload
	je	.LBB6_24
	.p2align	4, 0x90
.LBB6_21:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rsi, %rsi
	movq	%rsi, %rbp
	jne	.LBB6_21
	jmp	.LBB6_24
.LBB6_23:
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB6_24:                               # %list_Delete.exit90
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	subs_CompList, .Lfunc_end6-subs_CompList
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\nlength of LITPTR: %d\n"
	.size	.L.str, 23

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Entries of literal %d : \n"
	.size	.L.str.1, 26

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"----------------------"
	.size	.L.str.2, 23

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"used:\t\t"
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"TRUE"
	.size	.L.str.4, 5

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"FALSE"
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"litindex:\t%d\n"
	.size	.L.str.6, 14

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"litvarlist:\t"
	.size	.L.str.7, 13

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"\n"
	.size	.L.str.8, 2

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"No entries in litptr structure"
	.size	.L.str.9, 31

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"\nj = %d\n"
	.size	.L.str.10, 9

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"\nj == %d\n"
	.size	.L.str.11, 10

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"lit = %d\n"
	.size	.L.str.12, 10

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"i   = %d\n"
	.size	.L.str.13, 10

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"hasinter = TRUE"
	.size	.L.str.14, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
