	.text
	.file	"timer.bc"
	.globl	time_getWallclockSeconds
	.p2align	4, 0x90
	.type	time_getWallclockSeconds,@function
time_getWallclockSeconds:               # @time_getWallclockSeconds
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	retq
.Lfunc_end0:
	.size	time_getWallclockSeconds, .Lfunc_end0-time_getWallclockSeconds
	.cfi_endproc

	.globl	time_getCPUSeconds
	.p2align	4, 0x90
	.type	time_getCPUSeconds,@function
time_getCPUSeconds:                     # @time_getCPUSeconds
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	retq
.Lfunc_end1:
	.size	time_getCPUSeconds, .Lfunc_end1-time_getCPUSeconds
	.cfi_endproc

	.globl	time_get_wallclock_seconds_
	.p2align	4, 0x90
	.type	time_get_wallclock_seconds_,@function
time_get_wallclock_seconds_:            # @time_get_wallclock_seconds_
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	retq
.Lfunc_end2:
	.size	time_get_wallclock_seconds_, .Lfunc_end2-time_get_wallclock_seconds_
	.cfi_endproc

	.globl	time_get_cpu_seconds_
	.p2align	4, 0x90
	.type	time_get_cpu_seconds_,@function
time_get_cpu_seconds_:                  # @time_get_cpu_seconds_
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	retq
.Lfunc_end3:
	.size	time_get_cpu_seconds_, .Lfunc_end3-time_get_cpu_seconds_
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
