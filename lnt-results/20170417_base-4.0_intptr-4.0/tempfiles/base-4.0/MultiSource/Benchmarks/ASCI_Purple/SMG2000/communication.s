	.text
	.file	"communication.bc"
	.globl	hypre_CommPkgCreate
	.p2align	4, 0x90
	.type	hypre_CommPkgCreate,@function
hypre_CommPkgCreate:                    # @hypre_CommPkgCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 144
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rdx, %rbp
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	152(%rsp), %r14
	movq	144(%rsp), %r12
	movl	$1, %edi
	movl	$80, %esi
	callq	hypre_CAlloc
	movq	%rax, %r13
	movl	160(%rsp), %r8d
	movl	%r8d, (%r13)
	movl	168(%rsp), %r9d
	movl	%r9d, 4(%r13)
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	leaq	56(%rsp), %rax
	leaq	80(%rsp), %r10
	leaq	88(%rsp), %r11
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%r12, %rcx
	pushq	%rax
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	leaq	44(%rsp), %rax
	pushq	%rax
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	callq	hypre_CommPkgCreateInfo
	addq	$48, %rsp
.Lcfi19:
	.cfi_adjust_cfa_offset -48
	movl	12(%rsp), %eax
	movl	%eax, 8(%r13)
	movq	80(%rsp), %rax
	movq	%rax, 16(%r13)
	movq	72(%rsp), %rax
	movq	%rax, 32(%r13)
	movq	48(%rsp), %rax
	movq	%rax, 64(%r13)
	subq	$8, %rsp
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	leaq	48(%rsp), %rax
	leaq	64(%rsp), %rbp
	leaq	72(%rsp), %r10
	leaq	16(%rsp), %r11
	movq	%r15, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	%r14, %rcx
	movl	168(%rsp), %r8d
	movl	176(%rsp), %r9d
	pushq	%rax
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	callq	hypre_CommPkgCreateInfo
	addq	$48, %rsp
.Lcfi26:
	.cfi_adjust_cfa_offset -48
	movl	8(%rsp), %eax
	movl	%eax, 12(%r13)
	movq	64(%rsp), %rax
	movq	%rax, 24(%r13)
	movq	56(%rsp), %rax
	movq	%rax, 40(%r13)
	movq	40(%rsp), %rax
	movq	%rax, 72(%r13)
	cmpl	$0, 8(%rbx)
	jle	.LBB0_3
# BB#1:                                 # %.lr.ph86.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph86
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbp,8), %rdi
	callq	hypre_Free
	movq	$0, (%r12,%rbp,8)
	incq	%rbp
	movslq	8(%rbx), %rax
	cmpq	%rax, %rbp
	jl	.LBB0_2
.LBB0_3:                                # %._crit_edge87
	movq	%rbx, %rdi
	callq	hypre_BoxArrayArrayDestroy
	movq	%r12, %rdi
	callq	hypre_Free
	cmpl	$0, 8(%r15)
	jle	.LBB0_6
# BB#4:                                 # %.lr.ph82.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph82
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbx,8), %rdi
	callq	hypre_Free
	movq	$0, (%r14,%rbx,8)
	incq	%rbx
	movslq	8(%r15), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_5
.LBB0_6:                                # %._crit_edge83
	movq	%r15, %rdi
	callq	hypre_BoxArrayArrayDestroy
	movq	%r14, %rdi
	callq	hypre_Free
	movl	8(%r13), %edi
	shll	$2, %edi
	callq	hypre_MAlloc
	movq	%rax, 48(%r13)
	movl	8(%r13), %edi
	movq	32(%r13), %rdx
	movq	%rax, %rcx
	callq	hypre_CommTypeBuildMPI
	movl	12(%r13), %edi
	shll	$2, %edi
	callq	hypre_MAlloc
	movq	%rax, 56(%r13)
	movl	12(%r13), %edi
	movq	40(%r13), %rdx
	movq	%rax, %rcx
	callq	hypre_CommTypeBuildMPI
	movl	8(%r13), %eax
	movq	32(%r13), %rdi
	testl	%eax, %eax
	jle	.LBB0_19
# BB#7:                                 # %.lr.ph77.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph77
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_12 Depth 2
	movq	(%rdi,%r14,8), %rbx
	testq	%rbx, %rbx
	je	.LBB0_18
# BB#9:                                 #   in Loop: Header=BB0_8 Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_17
# BB#10:                                # %.preheader.i
                                        #   in Loop: Header=BB0_8 Depth=1
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB0_17
# BB#11:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB0_8 Depth=1
	movl	$1, %ebp
	jmp	.LBB0_12
	.p2align	4, 0x90
.LBB0_15:                               # %hypre_CommTypeEntryDestroy.exit..lr.ph_crit_edge.i
                                        #   in Loop: Header=BB0_12 Depth=2
	movq	(%rbx), %rdi
	incq	%rbp
.LBB0_12:                               # %.lr.ph.i
                                        #   Parent Loop BB0_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rdi,%rbp,8), %rdi
	testq	%rdi, %rdi
	je	.LBB0_14
# BB#13:                                #   in Loop: Header=BB0_12 Depth=2
	callq	hypre_Free
	movl	8(%rbx), %eax
.LBB0_14:                               # %hypre_CommTypeEntryDestroy.exit.i
                                        #   in Loop: Header=BB0_12 Depth=2
	movslq	%eax, %rcx
	cmpq	%rcx, %rbp
	jl	.LBB0_15
# BB#16:                                # %.loopexit.loopexit.i
                                        #   in Loop: Header=BB0_8 Depth=1
	movq	(%rbx), %rdi
.LBB0_17:                               # %.loopexit.i
                                        #   in Loop: Header=BB0_8 Depth=1
	callq	hypre_Free
	movq	$0, (%rbx)
	movq	%rbx, %rdi
	callq	hypre_Free
	movl	8(%r13), %eax
	movq	32(%r13), %rdi
.LBB0_18:                               # %hypre_CommTypeDestroy.exit
                                        #   in Loop: Header=BB0_8 Depth=1
	incq	%r14
	movslq	%eax, %rcx
	cmpq	%rcx, %r14
	jl	.LBB0_8
.LBB0_19:                               # %._crit_edge78
	callq	hypre_Free
	movq	$0, 32(%r13)
	movl	12(%r13), %eax
	movq	40(%r13), %rdi
	testl	%eax, %eax
	jle	.LBB0_32
# BB#20:                                # %.lr.ph.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_21:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_25 Depth 2
	movq	(%rdi,%r14,8), %rbx
	testq	%rbx, %rbx
	je	.LBB0_31
# BB#22:                                #   in Loop: Header=BB0_21 Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_30
# BB#23:                                # %.preheader.i60
                                        #   in Loop: Header=BB0_21 Depth=1
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB0_30
# BB#24:                                # %.lr.ph.i62.preheader
                                        #   in Loop: Header=BB0_21 Depth=1
	movl	$1, %ebp
	jmp	.LBB0_25
	.p2align	4, 0x90
.LBB0_28:                               # %hypre_CommTypeEntryDestroy.exit..lr.ph_crit_edge.i67
                                        #   in Loop: Header=BB0_25 Depth=2
	movq	(%rbx), %rdi
	incq	%rbp
.LBB0_25:                               # %.lr.ph.i62
                                        #   Parent Loop BB0_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rdi,%rbp,8), %rdi
	testq	%rdi, %rdi
	je	.LBB0_27
# BB#26:                                #   in Loop: Header=BB0_25 Depth=2
	callq	hypre_Free
	movl	8(%rbx), %eax
.LBB0_27:                               # %hypre_CommTypeEntryDestroy.exit.i65
                                        #   in Loop: Header=BB0_25 Depth=2
	movslq	%eax, %rcx
	cmpq	%rcx, %rbp
	jl	.LBB0_28
# BB#29:                                # %.loopexit.loopexit.i70
                                        #   in Loop: Header=BB0_21 Depth=1
	movq	(%rbx), %rdi
.LBB0_30:                               # %.loopexit.i71
                                        #   in Loop: Header=BB0_21 Depth=1
	callq	hypre_Free
	movq	$0, (%rbx)
	movq	%rbx, %rdi
	callq	hypre_Free
	movl	12(%r13), %eax
	movq	40(%r13), %rdi
.LBB0_31:                               # %hypre_CommTypeDestroy.exit72
                                        #   in Loop: Header=BB0_21 Depth=1
	incq	%r14
	movslq	%eax, %rcx
	cmpq	%rcx, %r14
	jl	.LBB0_21
.LBB0_32:                               # %._crit_edge
	callq	hypre_Free
	movq	$0, 40(%r13)
	movq	%r13, %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_CommPkgCreate, .Lfunc_end0-hypre_CommPkgCreate
	.cfi_endproc

	.globl	hypre_CommPkgCreateInfo
	.p2align	4, 0x90
	.type	hypre_CommPkgCreateInfo,@function
hypre_CommPkgCreateInfo:                # @hypre_CommPkgCreateInfo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 192
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebx
	movl	%r8d, 48(%rsp)          # 4-byte Spill
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	movq	%rdi, %rbp
	leaq	52(%rsp), %rsi
	movl	%ebx, %edi
	callq	hypre_MPI_Comm_size
	leaq	4(%rsp), %rsi
	movl	%ebx, %edi
	callq	hypre_MPI_Comm_rank
	movl	52(%rsp), %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movl	8(%rbp), %edx
	testl	%edx, %edx
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	jle	.LBB1_1
# BB#2:                                 # %.lr.ph196
	movq	(%rbp), %r8
	xorl	%r14d, %r14d
	xorl	%r9d, %r9d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB1_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_5 Depth 2
	movq	(%r8,%r9,8), %r11
	movl	8(%r11), %r15d
	testl	%r15d, %r15d
	jle	.LBB1_9
# BB#4:                                 # %.lr.ph190
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	(%r11), %rdi
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%r9,8), %r10
	addq	$20, %rdi
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB1_5:                                #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %esi
	movl	-12(%rdi), %ebp
	movl	%esi, %ebx
	subl	%ebp, %ebx
	incl	%ebx
	cmpl	%ebp, %esi
	movl	-8(%rdi), %esi
	movl	-4(%rdi), %edx
	movl	-16(%rdi), %ecx
	cmovsl	%r14d, %ebx
	movl	%edx, %ebp
	subl	%ecx, %ebp
	incl	%ebp
	cmpl	%ecx, %edx
	movl	-20(%rdi), %ecx
	cmovsl	%r14d, %ebp
	movl	%esi, %edx
	subl	%ecx, %edx
	incl	%edx
	cmpl	%ecx, %esi
	cmovsl	%r14d, %edx
	imull	%ebp, %edx
	imull	%ebx, %edx
	testl	%edx, %edx
	je	.LBB1_7
# BB#6:                                 #   in Loop: Header=BB1_5 Depth=2
	movslq	(%r10,%r12,4), %rcx
	movl	(%rax,%rcx,4), %edx
	leal	1(%rdx), %esi
	movl	%esi, (%rax,%rcx,4)
	testl	%edx, %edx
	sete	%dl
	cmpl	4(%rsp), %ecx
	setne	%cl
	andb	%dl, %cl
	movzbl	%cl, %ecx
	addl	%ecx, %r13d
	movl	8(%r11), %r15d
.LBB1_7:                                #   in Loop: Header=BB1_5 Depth=2
	incq	%r12
	movslq	%r15d, %rcx
	addq	$24, %rdi
	cmpq	%rcx, %r12
	jl	.LBB1_5
# BB#8:                                 # %._crit_edge191.loopexit
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	64(%rsp), %rbp          # 8-byte Reload
	movl	8(%rbp), %edx
.LBB1_9:                                # %._crit_edge191
                                        #   in Loop: Header=BB1_3 Depth=1
	incq	%r9
	movslq	%edx, %rcx
	cmpq	%rcx, %r9
	jl	.LBB1_3
	jmp	.LBB1_10
.LBB1_1:
	xorl	%r13d, %r13d
.LBB1_10:                               # %._crit_edge197
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	52(%rsp), %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rax, %rbx
	leal	(,%r13,4), %edi
	callq	hypre_MAlloc
	movq	%rbx, %r8
	movq	%rax, 56(%rsp)          # 8-byte Spill
	cmpl	$0, 8(%rbp)
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	%r8, 32(%rsp)           # 8-byte Spill
	jle	.LBB1_22
# BB#11:                                # %.lr.ph185
	xorl	%r10d, %r10d
	xorl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	$0, 44(%rsp)            # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB1_12:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_14 Depth 2
	movq	(%rbp), %rax
	movq	(%rax,%r10,8), %r11
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	leaq	(%r10,%r10,2), %rdx
	leaq	(%rax,%rdx,8), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	8(%r11), %r9d
	testl	%r9d, %r9d
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	jle	.LBB1_21
# BB#13:                                # %.lr.ph179
                                        #   in Loop: Header=BB1_12 Depth=1
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	movq	%r10, 128(%rsp)         # 8-byte Spill
	movq	%r11, 120(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_14:                               #   Parent Loop BB1_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r11), %r14
	movl	20(%r14,%r15), %ecx
	movl	8(%r14,%r15), %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	incl	%esi
	cmpl	%edx, %ecx
	movl	12(%r14,%r15), %ebx
	movl	16(%r14,%r15), %ebp
	movl	4(%r14,%r15), %edi
	movl	$0, %eax
	cmovsl	%eax, %esi
	movl	%ebp, %edx
	subl	%edi, %edx
	incl	%edx
	cmpl	%edi, %ebp
	movl	(%r14,%r15), %edi
	cmovsl	%eax, %edx
	movl	%ebx, %ecx
	subl	%edi, %ecx
	incl	%ecx
	cmpl	%edi, %ebx
	cmovsl	%eax, %ecx
	imull	%edx, %ecx
	imull	%esi, %ecx
	testl	%ecx, %ecx
	je	.LBB1_20
# BB#15:                                #   in Loop: Header=BB1_14 Depth=2
	addq	%r15, %r14
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r10,8), %rax
	movl	(%rax,%r12,4), %ebp
	movslq	%ebp, %rbx
	cmpq	$0, (%r8,%rbx,8)
	jne	.LBB1_19
# BB#16:                                #   in Loop: Header=BB1_14 Depth=2
	movq	8(%rsp), %r13           # 8-byte Reload
	movl	(%r13,%rbx,4), %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx,%rbx,8)
	movl	$0, (%r13,%rbx,4)
	cmpl	4(%rsp), %ebx
	je	.LBB1_18
# BB#17:                                #   in Loop: Header=BB1_14 Depth=2
	movq	72(%rsp), %rcx          # 8-byte Reload
	movslq	%ecx, %rcx
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	%ebp, (%rax,%rcx,4)
	incl	%ecx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
.LBB1_18:                               # %._crit_edge210
                                        #   in Loop: Header=BB1_14 Depth=2
	movq	16(%rsp), %r13          # 8-byte Reload
.LBB1_19:                               # %._crit_edge210
                                        #   in Loop: Header=BB1_14 Depth=2
	movq	%r14, %rdi
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	movl	48(%rsp), %ecx          # 4-byte Reload
	movl	44(%rsp), %r8d          # 4-byte Reload
	callq	hypre_CommTypeEntryCreate
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	(%r8,%rbx,8), %rcx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movslq	(%rsi,%rbx,4), %rdx
	movq	%rax, (%rcx,%rdx,8)
	leal	1(%rdx), %eax
	movl	%eax, (%rsi,%rbx,4)
	movq	120(%rsp), %r11         # 8-byte Reload
	movl	8(%r11), %r9d
	movq	128(%rsp), %r10         # 8-byte Reload
.LBB1_20:                               #   in Loop: Header=BB1_14 Depth=2
	incq	%r12
	movslq	%r9d, %rcx
	addq	$24, %r15
	cmpq	%rcx, %r12
	jl	.LBB1_14
.LBB1_21:                               # %._crit_edge180
                                        #   in Loop: Header=BB1_12 Depth=1
	movq	104(%rsp), %rdi         # 8-byte Reload
	movq	96(%rsp), %rbx          # 8-byte Reload
	movl	20(%rdi,%rbx,8), %eax
	movl	8(%rdi,%rbx,8), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	incl	%edx
	cmpl	%ecx, %eax
	movl	16(%rdi,%rbx,8), %eax
	movl	4(%rdi,%rbx,8), %ecx
	movl	$0, %ebp
	cmovsl	%ebp, %edx
	movl	%eax, %esi
	subl	%ecx, %esi
	incl	%esi
	cmpl	%ecx, %eax
	movl	12(%rdi,%rbx,8), %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %ecx
	cmovsl	%ebp, %esi
	movl	%eax, %edi
	subl	%ecx, %edi
	incl	%edi
	cmpl	%ecx, %eax
	cmovsl	%ebp, %edi
	imull	48(%rsp), %edi          # 4-byte Folded Reload
	imull	%edx, %esi
	imull	%edi, %esi
	addl	%esi, 44(%rsp)          # 4-byte Folded Spill
	incq	%r10
	movq	64(%rsp), %rbp          # 8-byte Reload
	movslq	8(%rbp), %rax
	cmpq	%rax, %r10
	jl	.LBB1_12
.LBB1_22:                               # %._crit_edge186
	movq	192(%rsp), %r12
	leal	(,%r13,8), %edi
	callq	hypre_MAlloc
	movq	%rax, 24(%rsp)          # 8-byte Spill
	testl	%r13d, %r13d
	movq	8(%rsp), %rdx           # 8-byte Reload
	jle	.LBB1_25
# BB#23:                                # %.lr.ph.preheader
	movl	16(%rsp), %r15d         # 4-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_24:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	(%rbx), %rax
	movq	%r12, %r13
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %r12
	movl	(%rdx,%rax,4), %r14d
	movl	$16, %edi
	callq	hypre_MAlloc
	movq	%r12, (%rax)
	movq	%r13, %r12
	movl	%r14d, 8(%rax)
	movq	%rax, (%rbp)
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	hypre_CommTypeSort
	movq	8(%rsp), %rdx           # 8-byte Reload
	addq	$4, %rbx
	addq	$8, %rbp
	decq	%r15
	jne	.LBB1_24
.LBB1_25:                               # %._crit_edge
	movq	224(%rsp), %r13
	movq	200(%rsp), %r14
	movslq	4(%rsp), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %rbp
	testq	%rbp, %rbp
	je	.LBB1_27
# BB#26:
	movq	8(%rsp), %r15           # 8-byte Reload
	movl	(%r15,%rax,4), %ebx
	movl	$16, %edi
	callq	hypre_MAlloc
	movq	%r12, %rsi
	movq	%rax, %r12
	movq	%rbp, (%r12)
	movl	%ebx, 8(%r12)
	movq	%r12, %rdi
	callq	hypre_CommTypeSort
	jmp	.LBB1_28
.LBB1_27:
	movl	$16, %edi
	callq	hypre_MAlloc
	movq	%rax, %r12
	movq	$0, (%r12)
	movl	$0, 8(%r12)
	movq	8(%rsp), %r15           # 8-byte Reload
.LBB1_28:
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	hypre_Free
	movq	%r15, %rdi
	callq	hypre_Free
	movl	%ebx, (%r14)
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	208(%rsp), %rcx
	movq	%rax, (%rcx)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	216(%rsp), %rcx
	movq	%rax, (%rcx)
	movq	%r12, (%r13)
	xorl	%eax, %eax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	hypre_CommPkgCreateInfo, .Lfunc_end1-hypre_CommPkgCreateInfo
	.cfi_endproc

	.globl	hypre_CommPkgCommit
	.p2align	4, 0x90
	.type	hypre_CommPkgCommit,@function
hypre_CommPkgCommit:                    # @hypre_CommPkgCommit
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 16
.Lcfi41:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	8(%rbx), %edi
	shll	$2, %edi
	callq	hypre_MAlloc
	movq	%rax, 48(%rbx)
	movl	8(%rbx), %edi
	movq	32(%rbx), %rdx
	movq	%rax, %rcx
	callq	hypre_CommTypeBuildMPI
	movl	12(%rbx), %edi
	shll	$2, %edi
	callq	hypre_MAlloc
	movq	%rax, 56(%rbx)
	movl	12(%rbx), %edi
	movq	40(%rbx), %rdx
	movq	%rax, %rcx
	callq	hypre_CommTypeBuildMPI
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end2:
	.size	hypre_CommPkgCommit, .Lfunc_end2-hypre_CommPkgCommit
	.cfi_endproc

	.globl	hypre_CommTypeDestroy
	.p2align	4, 0x90
	.type	hypre_CommTypeDestroy,@function
hypre_CommTypeDestroy:                  # @hypre_CommTypeDestroy
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 32
.Lcfi45:
	.cfi_offset %rbx, -24
.Lcfi46:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB3_10
# BB#1:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB3_9
# BB#2:                                 # %.preheader
	movl	8(%r14), %eax
	testl	%eax, %eax
	jle	.LBB3_9
# BB#3:                                 # %.lr.ph.preheader
	movl	$1, %ebx
	jmp	.LBB3_4
	.p2align	4, 0x90
.LBB3_7:                                # %hypre_CommTypeEntryDestroy.exit..lr.ph_crit_edge
                                        #   in Loop: Header=BB3_4 Depth=1
	movq	(%r14), %rdi
	incq	%rbx
.LBB3_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rdi,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_4 Depth=1
	callq	hypre_Free
	movl	8(%r14), %eax
.LBB3_6:                                # %hypre_CommTypeEntryDestroy.exit
                                        #   in Loop: Header=BB3_4 Depth=1
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB3_7
# BB#8:                                 # %.loopexit.loopexit
	movq	(%r14), %rdi
.LBB3_9:                                # %.loopexit
	callq	hypre_Free
	movq	$0, (%r14)
	movq	%r14, %rdi
	callq	hypre_Free
.LBB3_10:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	hypre_CommTypeDestroy, .Lfunc_end3-hypre_CommTypeDestroy
	.cfi_endproc

	.globl	hypre_CommPkgDestroy
	.p2align	4, 0x90
	.type	hypre_CommPkgDestroy,@function
hypre_CommPkgDestroy:                   # @hypre_CommPkgDestroy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 48
.Lcfi52:
	.cfi_offset %rbx, -40
.Lcfi53:
	.cfi_offset %r12, -32
.Lcfi54:
	.cfi_offset %r14, -24
.Lcfi55:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB4_32
# BB#1:
	movq	48(%r14), %r15
	testq	%r15, %r15
	je	.LBB4_6
# BB#2:                                 # %.preheader20.i
	cmpl	$0, 8(%r14)
	jle	.LBB4_5
# BB#3:                                 # %.lr.ph23.i.preheader
	xorl	%r12d, %r12d
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB4_4:                                # %.lr.ph23.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	hypre_MPI_Type_free
	incq	%r12
	movslq	8(%r14), %rax
	addq	$4, %rbx
	cmpq	%rax, %r12
	jl	.LBB4_4
.LBB4_5:                                # %._crit_edge24.i
	movq	%r15, %rdi
	callq	hypre_Free
.LBB4_6:
	movq	56(%r14), %r15
	testq	%r15, %r15
	je	.LBB4_11
# BB#7:                                 # %.preheader.i
	cmpl	$0, 12(%r14)
	jle	.LBB4_10
# BB#8:                                 # %.lr.ph.i.preheader
	xorl	%r12d, %r12d
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB4_9:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	hypre_MPI_Type_free
	incq	%r12
	movslq	12(%r14), %rax
	addq	$4, %rbx
	cmpq	%rax, %r12
	jl	.LBB4_9
.LBB4_10:                               # %._crit_edge.i
	movq	%r15, %rdi
	callq	hypre_Free
.LBB4_11:                               # %hypre_CommPkgUnCommit.exit
	movq	16(%r14), %rdi
	callq	hypre_Free
	movq	$0, 16(%r14)
	movq	24(%r14), %rdi
	callq	hypre_Free
	movq	$0, 24(%r14)
	movq	64(%r14), %r15
	testq	%r15, %r15
	je	.LBB4_21
# BB#12:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB4_20
# BB#13:                                # %.preheader.i10
	movl	8(%r15), %eax
	testl	%eax, %eax
	jle	.LBB4_20
# BB#14:                                # %.lr.ph.i12.preheader
	movl	$1, %ebx
	jmp	.LBB4_15
	.p2align	4, 0x90
.LBB4_18:                               # %hypre_CommTypeEntryDestroy.exit..lr.ph_crit_edge.i
                                        #   in Loop: Header=BB4_15 Depth=1
	movq	(%r15), %rdi
	incq	%rbx
.LBB4_15:                               # %.lr.ph.i12
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rdi,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB4_17
# BB#16:                                #   in Loop: Header=BB4_15 Depth=1
	callq	hypre_Free
	movl	8(%r15), %eax
.LBB4_17:                               # %hypre_CommTypeEntryDestroy.exit.i
                                        #   in Loop: Header=BB4_15 Depth=1
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB4_18
# BB#19:                                # %.loopexit.loopexit.i
	movq	(%r15), %rdi
.LBB4_20:                               # %.loopexit.i
	callq	hypre_Free
	movq	$0, (%r15)
	movq	%r15, %rdi
	callq	hypre_Free
.LBB4_21:                               # %hypre_CommTypeDestroy.exit
	movq	72(%r14), %r15
	testq	%r15, %r15
	je	.LBB4_31
# BB#22:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB4_30
# BB#23:                                # %.preheader.i14
	movl	8(%r15), %eax
	testl	%eax, %eax
	jle	.LBB4_30
# BB#24:                                # %.lr.ph.i16.preheader
	movl	$1, %ebx
	jmp	.LBB4_25
	.p2align	4, 0x90
.LBB4_28:                               # %hypre_CommTypeEntryDestroy.exit..lr.ph_crit_edge.i21
                                        #   in Loop: Header=BB4_25 Depth=1
	movq	(%r15), %rdi
	incq	%rbx
.LBB4_25:                               # %.lr.ph.i16
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rdi,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB4_27
# BB#26:                                #   in Loop: Header=BB4_25 Depth=1
	callq	hypre_Free
	movl	8(%r15), %eax
.LBB4_27:                               # %hypre_CommTypeEntryDestroy.exit.i19
                                        #   in Loop: Header=BB4_25 Depth=1
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB4_28
# BB#29:                                # %.loopexit.loopexit.i24
	movq	(%r15), %rdi
.LBB4_30:                               # %.loopexit.i25
	callq	hypre_Free
	movq	$0, (%r15)
	movq	%r15, %rdi
	callq	hypre_Free
.LBB4_31:                               # %hypre_CommTypeDestroy.exit26
	movq	%r14, %rdi
	callq	hypre_Free
.LBB4_32:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	hypre_CommPkgDestroy, .Lfunc_end4-hypre_CommPkgDestroy
	.cfi_endproc

	.globl	hypre_CommPkgUnCommit
	.p2align	4, 0x90
	.type	hypre_CommPkgUnCommit,@function
hypre_CommPkgUnCommit:                  # @hypre_CommPkgUnCommit
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 48
.Lcfi61:
	.cfi_offset %rbx, -40
.Lcfi62:
	.cfi_offset %r12, -32
.Lcfi63:
	.cfi_offset %r14, -24
.Lcfi64:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB5_11
# BB#1:
	movq	48(%r15), %r14
	testq	%r14, %r14
	je	.LBB5_6
# BB#2:                                 # %.preheader20
	cmpl	$0, 8(%r15)
	jle	.LBB5_5
# BB#3:                                 # %.lr.ph23.preheader
	xorl	%r12d, %r12d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB5_4:                                # %.lr.ph23
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	hypre_MPI_Type_free
	incq	%r12
	movslq	8(%r15), %rax
	addq	$4, %rbx
	cmpq	%rax, %r12
	jl	.LBB5_4
.LBB5_5:                                # %._crit_edge24
	movq	%r14, %rdi
	callq	hypre_Free
.LBB5_6:
	movq	56(%r15), %r14
	testq	%r14, %r14
	je	.LBB5_11
# BB#7:                                 # %.preheader
	cmpl	$0, 12(%r15)
	jle	.LBB5_10
# BB#8:                                 # %.lr.ph.preheader
	xorl	%r12d, %r12d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB5_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	hypre_MPI_Type_free
	incq	%r12
	movslq	12(%r15), %rax
	addq	$4, %rbx
	cmpq	%rax, %r12
	jl	.LBB5_9
.LBB5_10:                               # %._crit_edge
	movq	%r14, %rdi
	callq	hypre_Free
.LBB5_11:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	hypre_CommPkgUnCommit, .Lfunc_end5-hypre_CommPkgUnCommit
	.cfi_endproc

	.globl	hypre_InitializeCommunication
	.p2align	4, 0x90
	.type	hypre_InitializeCommunication,@function
hypre_InitializeCommunication:          # @hypre_InitializeCommunication
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi69:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi71:
	.cfi_def_cfa_offset 128
.Lcfi72:
	.cfi_offset %rbx, -56
.Lcfi73:
	.cfi_offset %r12, -48
.Lcfi74:
	.cfi_offset %r13, -40
.Lcfi75:
	.cfi_offset %r14, -32
.Lcfi76:
	.cfi_offset %r15, -24
.Lcfi77:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, %r13
	movq	%rdi, %r12
	movl	4(%r12), %r14d
	movl	8(%r12), %eax
	movl	12(%r12), %ebp
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leal	(%rbp,%rax), %ebx
	movl	$4, %esi
	movl	%ebx, %edi
	callq	hypre_CAlloc
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$4, %esi
	movl	%ebx, 20(%rsp)          # 4-byte Spill
	movl	%ebx, %edi
	callq	hypre_CAlloc
	movq	%rax, 64(%rsp)          # 8-byte Spill
	testl	%ebp, %ebp
	jle	.LBB6_1
# BB#7:                                 # %.lr.ph72
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	%r15, 56(%rsp)          # 8-byte Spill
	leal	-1(%rbp), %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	24(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_8:                                # =>This Inner Loop Header: Depth=1
	movq	24(%r12), %rax
	movq	56(%r12), %rcx
	movl	(%rcx,%r15,4), %edx
	movl	(%rax,%r15,4), %ecx
	incq	%r15
	movq	%rbx, (%rsp)
	movl	$1, %esi
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movl	%r14d, %r9d
	callq	hypre_MPI_Irecv
	addq	$4, %rbx
	cmpq	%r15, %rbp
	jne	.LBB6_8
# BB#2:                                 # %.preheader.loopexit
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	shlq	$32, %rcx
	movabsq	$4294967296, %rax       # imm = 0x100000000
	addq	%rcx, %rax
	sarq	$32, %rax
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	48(%rsp), %r13          # 8-byte Reload
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jg	.LBB6_4
	jmp	.LBB6_6
.LBB6_1:
	xorl	%eax, %eax
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB6_6
.LBB6_4:                                # %.lr.ph
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rcx,%rax,4), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_5:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	movq	48(%r12), %rcx
	movl	(%rcx,%rbx,4), %edx
	movl	(%rax,%rbx,4), %ecx
	movq	%rbp, (%rsp)
	movl	$1, %esi
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movl	%r14d, %r9d
	callq	hypre_MPI_Isend
	incq	%rbx
	addq	$4, %rbp
	cmpq	%rbx, 32(%rsp)          # 8-byte Folded Reload
	jne	.LBB6_5
.LBB6_6:                                # %._crit_edge
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	callq	hypre_ExchangeLocalData
	movl	$48, %edi
	callq	hypre_MAlloc
	movq	%r12, (%rax)
	movq	%r13, 8(%rax)
	movq	%rbx, 16(%rax)
	movl	20(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 24(%rax)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, 32(%rax)
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 40(%rax)
	movq	%rax, (%r15)
	xorl	%eax, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	hypre_InitializeCommunication, .Lfunc_end6-hypre_InitializeCommunication
	.cfi_endproc

	.globl	hypre_ExchangeLocalData
	.p2align	4, 0x90
	.type	hypre_ExchangeLocalData,@function
hypre_ExchangeLocalData:                # @hypre_ExchangeLocalData
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi80:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi81:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi82:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 56
.Lcfi84:
	.cfi_offset %rbx, -56
.Lcfi85:
	.cfi_offset %r12, -48
.Lcfi86:
	.cfi_offset %r13, -40
.Lcfi87:
	.cfi_offset %r14, -32
.Lcfi88:
	.cfi_offset %r15, -24
.Lcfi89:
	.cfi_offset %rbp, -16
	movq	%rdx, -16(%rsp)         # 8-byte Spill
	movq	%rsi, -24(%rsp)         # 8-byte Spill
	movq	64(%rdi), %rax
	movslq	8(%rax), %rcx
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	testq	%rcx, %rcx
	jle	.LBB7_18
# BB#1:                                 # %.lr.ph111
	movq	72(%rdi), %rcx
	movq	(%rax), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	movq	(%rcx), %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB7_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_5 Depth 2
                                        #       Child Loop BB7_8 Depth 3
                                        #         Child Loop BB7_10 Depth 4
                                        #           Child Loop BB7_19 Depth 5
	movq	-40(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rcx,8), %rdx
	movq	-48(%rsp), %rax         # 8-byte Reload
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %rsi
	movq	%rdx, -112(%rsp)        # 8-byte Spill
	movslq	24(%rdx), %rax
	movq	-24(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rsi, -56(%rsp)         # 8-byte Spill
	movslq	24(%rsi), %rax
	movq	-16(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,8), %rbx
	cmpq	%rcx, %rbx
	je	.LBB7_17
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	movq	-112(%rsp), %rax        # 8-byte Reload
	movl	44(%rax), %eax
	movl	%eax, -96(%rsp)         # 4-byte Spill
	testl	%eax, %eax
	jle	.LBB7_17
# BB#4:                                 # %.preheader89.lr.ph
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	-112(%rsp), %rax        # 8-byte Reload
	movl	40(%rax), %eax
	movl	%eax, -100(%rsp)        # 4-byte Spill
	movl	$0, -120(%rsp)          # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB7_5:                                # %.preheader89
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_8 Depth 3
                                        #         Child Loop BB7_10 Depth 4
                                        #           Child Loop BB7_19 Depth 5
	cmpl	$0, -100(%rsp)          # 4-byte Folded Reload
	jle	.LBB7_16
# BB#6:                                 # %.preheader.lr.ph
                                        #   in Loop: Header=BB7_5 Depth=2
	movq	-112(%rsp), %rax        # 8-byte Reload
	movl	36(%rax), %eax
	movl	%eax, -64(%rsp)         # 4-byte Spill
	testl	%eax, %eax
	jle	.LBB7_16
# BB#7:                                 # %.preheader.us.preheader
                                        #   in Loop: Header=BB7_5 Depth=2
	movq	-112(%rsp), %rax        # 8-byte Reload
	movl	56(%rax), %edx
	movl	%edx, -80(%rsp)         # 4-byte Spill
	movl	32(%rax), %r8d
	movl	52(%rax), %edx
	movl	%edx, -68(%rsp)         # 4-byte Spill
	movq	-56(%rsp), %rdx         # 8-byte Reload
	movl	52(%rdx), %esi
	movl	%esi, -72(%rsp)         # 4-byte Spill
	movl	56(%rdx), %esi
	movl	%esi, -84(%rsp)         # 4-byte Spill
	movl	60(%rax), %esi
	movl	-120(%rsp), %eax        # 4-byte Reload
	imull	%eax, %esi
	movl	%esi, -88(%rsp)         # 4-byte Spill
	movl	60(%rdx), %edx
	imull	%eax, %edx
	movl	%edx, -92(%rsp)         # 4-byte Spill
	movl	%r8d, %eax
	andl	$1, %eax
	movl	%eax, -76(%rsp)         # 4-byte Spill
	movl	$0, -116(%rsp)          # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB7_8:                                # %.preheader.us
                                        #   Parent Loop BB7_2 Depth=1
                                        #     Parent Loop BB7_5 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB7_10 Depth 4
                                        #           Child Loop BB7_19 Depth 5
	testl	%r8d, %r8d
	jle	.LBB7_15
# BB#9:                                 # %.lr.ph.us.us.preheader
                                        #   in Loop: Header=BB7_8 Depth=3
	movl	-80(%rsp), %edx         # 4-byte Reload
	movl	-116(%rsp), %eax        # 4-byte Reload
	imull	%eax, %edx
	addl	-88(%rsp), %edx         # 4-byte Folded Reload
	movl	%edx, -60(%rsp)         # 4-byte Spill
	movl	-84(%rsp), %r9d         # 4-byte Reload
	imull	%eax, %r9d
	addl	-92(%rsp), %r9d         # 4-byte Folded Reload
	movq	-112(%rsp), %rax        # 8-byte Reload
	movl	48(%rax), %edi
	movq	-56(%rsp), %rax         # 8-byte Reload
	movl	48(%rax), %r12d
	leal	(%rdi,%rdi), %r10d
	leal	(%r12,%r12), %r11d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB7_10:                               # %.lr.ph.us.us
                                        #   Parent Loop BB7_2 Depth=1
                                        #     Parent Loop BB7_5 Depth=2
                                        #       Parent Loop BB7_8 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB7_19 Depth 5
	movl	-68(%rsp), %eax         # 4-byte Reload
	movl	%eax, %r15d
	imull	%r13d, %r15d
	addl	-60(%rsp), %r15d        # 4-byte Folded Reload
	movl	-72(%rsp), %eax         # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	imull	%r13d, %eax
	addl	%r9d, %eax
	cmpl	$0, -76(%rsp)           # 4-byte Folded Reload
	jne	.LBB7_12
# BB#11:                                #   in Loop: Header=BB7_10 Depth=4
	xorl	%edx, %edx
	cmpl	$1, %r8d
	jne	.LBB7_19
	jmp	.LBB7_14
	.p2align	4, 0x90
.LBB7_12:                               #   in Loop: Header=BB7_10 Depth=4
	movslq	%r15d, %r15
	movq	(%rcx,%r15,8), %rdx
	cltq
	movq	%rdx, (%rbx,%rax,8)
	addl	%edi, %r15d
	addl	%r12d, %eax
	movl	$1, %edx
	cmpl	$1, %r8d
	je	.LBB7_14
	.p2align	4, 0x90
.LBB7_19:                               #   Parent Loop BB7_2 Depth=1
                                        #     Parent Loop BB7_5 Depth=2
                                        #       Parent Loop BB7_8 Depth=3
                                        #         Parent Loop BB7_10 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movslq	%r15d, %r15
	movq	(%rcx,%r15,8), %rsi
	cltq
	movq	%rsi, (%rbx,%rax,8)
	leal	(%rdi,%r15), %esi
	leal	(%r12,%rax), %r14d
	movslq	%esi, %rsi
	movq	(%rcx,%rsi,8), %rsi
	movslq	%r14d, %rbp
	movq	%rsi, (%rbx,%rbp,8)
	addl	$2, %edx
	addl	%r10d, %r15d
	addl	%r11d, %eax
	cmpl	%r8d, %edx
	jl	.LBB7_19
.LBB7_14:                               # %._crit_edge.us.us
                                        #   in Loop: Header=BB7_10 Depth=4
	incl	%r13d
	cmpl	-64(%rsp), %r13d        # 4-byte Folded Reload
	jl	.LBB7_10
.LBB7_15:                               # %._crit_edge95.us
                                        #   in Loop: Header=BB7_8 Depth=3
	movl	-116(%rsp), %eax        # 4-byte Reload
	incl	%eax
	movl	%eax, -116(%rsp)        # 4-byte Spill
	cmpl	-100(%rsp), %eax        # 4-byte Folded Reload
	jl	.LBB7_8
.LBB7_16:                               # %._crit_edge97
                                        #   in Loop: Header=BB7_5 Depth=2
	movl	-120(%rsp), %eax        # 4-byte Reload
	incl	%eax
	movl	%eax, -120(%rsp)        # 4-byte Spill
	cmpl	-96(%rsp), %eax         # 4-byte Folded Reload
	jl	.LBB7_5
.LBB7_17:                               # %.loopexit
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	-8(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	cmpq	-32(%rsp), %rcx         # 8-byte Folded Reload
	jl	.LBB7_2
.LBB7_18:                               # %._crit_edge112
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	hypre_ExchangeLocalData, .Lfunc_end7-hypre_ExchangeLocalData
	.cfi_endproc

	.globl	hypre_FinalizeCommunication
	.p2align	4, 0x90
	.type	hypre_FinalizeCommunication,@function
hypre_FinalizeCommunication:            # @hypre_FinalizeCommunication
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi92:
	.cfi_def_cfa_offset 32
.Lcfi93:
	.cfi_offset %rbx, -24
.Lcfi94:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	24(%rbx), %edi
	leaq	40(%rbx), %r14
	testl	%edi, %edi
	je	.LBB8_2
# BB#1:
	movq	32(%rbx), %rsi
	movq	40(%rbx), %rdx
	callq	hypre_MPI_Waitall
.LBB8_2:
	movq	32(%rbx), %rdi
	callq	hypre_Free
	movq	$0, 32(%rbx)
	movq	(%r14), %rdi
	callq	hypre_Free
	movq	$0, 40(%rbx)
	movq	%rbx, %rdi
	callq	hypre_Free
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	hypre_FinalizeCommunication, .Lfunc_end8-hypre_FinalizeCommunication
	.cfi_endproc

	.globl	hypre_CommTypeCreate
	.p2align	4, 0x90
	.type	hypre_CommTypeCreate,@function
hypre_CommTypeCreate:                   # @hypre_CommTypeCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi97:
	.cfi_def_cfa_offset 32
.Lcfi98:
	.cfi_offset %rbx, -24
.Lcfi99:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	$16, %edi
	callq	hypre_MAlloc
	movq	%rbx, (%rax)
	movl	%ebp, 8(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end9:
	.size	hypre_CommTypeCreate, .Lfunc_end9-hypre_CommTypeCreate
	.cfi_endproc

	.globl	hypre_CommTypeEntryDestroy
	.p2align	4, 0x90
	.type	hypre_CommTypeEntryDestroy,@function
hypre_CommTypeEntryDestroy:             # @hypre_CommTypeEntryDestroy
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB10_2
# BB#1:
	pushq	%rax
.Lcfi100:
	.cfi_def_cfa_offset 16
	callq	hypre_Free
	addq	$8, %rsp
.LBB10_2:
	xorl	%eax, %eax
	retq
.Lfunc_end10:
	.size	hypre_CommTypeEntryDestroy, .Lfunc_end10-hypre_CommTypeEntryDestroy
	.cfi_endproc

	.globl	hypre_CommTypeEntryCreate
	.p2align	4, 0x90
	.type	hypre_CommTypeEntryCreate,@function
hypre_CommTypeEntryCreate:              # @hypre_CommTypeEntryCreate
	.cfi_startproc
# BB#0:                                 # %.lr.ph124.2
	pushq	%rbp
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi104:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi105:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi107:
	.cfi_def_cfa_offset 96
.Lcfi108:
	.cfi_offset %rbx, -56
.Lcfi109:
	.cfi_offset %r12, -48
.Lcfi110:
	.cfi_offset %r13, -40
.Lcfi111:
	.cfi_offset %r14, -32
.Lcfi112:
	.cfi_offset %r15, -24
.Lcfi113:
	.cfi_offset %rbp, -16
	movl	%r8d, %r13d
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movl	$64, %edi
	callq	hypre_MAlloc
	movq	%rax, %rbx
	movl	(%rbp), %eax
	movl	%eax, (%rbx)
	movl	4(%rbp), %eax
	movl	%eax, 4(%rbx)
	movl	8(%rbp), %eax
	movl	%eax, 8(%rbx)
	movl	12(%rbp), %eax
	movl	%eax, 12(%rbx)
	movl	16(%rbp), %eax
	movl	%eax, 16(%rbx)
	movl	20(%rbp), %eax
	movl	%eax, 20(%rbx)
	movl	(%r14), %r8d
	movl	4(%r14), %ecx
	movl	12(%r14), %edx
	movl	%edx, %esi
	subl	%r8d, %esi
	incl	%esi
	xorl	%r12d, %r12d
	cmpl	%r8d, %edx
	movl	16(%r14), %edx
	cmovsl	%r12d, %esi
	movl	%edx, %edi
	subl	%ecx, %edi
	incl	%edi
	cmpl	%ecx, %edx
	movl	4(%rbp), %edx
	movl	8(%rbp), %eax
	cmovsl	%r12d, %edi
	subl	%ecx, %edx
	subl	8(%r14), %eax
	imull	%edi, %eax
	addl	%edx, %eax
	imull	%esi, %eax
	addl	(%rbp), %r13d
	subl	%r8d, %r13d
	addl	%eax, %r13d
	movl	%r13d, 24(%rbx)
	leaq	20(%rsp), %rdx
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	hypre_BoxGetStrideSize
	movl	20(%rsp), %eax
	movl	%eax, 32(%rbx)
	movl	24(%rsp), %eax
	movl	%eax, 36(%rbx)
	movl	28(%rsp), %eax
	movl	%eax, 40(%rbx)
	movl	8(%rsp), %eax           # 4-byte Reload
	movl	%eax, 44(%rbx)
	movl	(%r15), %eax
	movl	%eax, 48(%rbx)
	movl	4(%r15), %eax
	movl	%eax, 52(%rbx)
	movl	12(%r14), %ecx
	movl	(%r14), %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	incl	%esi
	cmpl	%edx, %ecx
	cmovsl	%r12d, %esi
	imull	%eax, %esi
	movl	%esi, 52(%rbx)
	movl	8(%r15), %eax
	xorl	%ebp, %ebp
	movl	%eax, 56(%rbx)
	movl	12(%r14), %ecx
	movl	(%r14), %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	incl	%esi
	cmpl	%edx, %ecx
	cmovsl	%ebp, %esi
	imull	%eax, %esi
	movl	%esi, 56(%rbx)
	movl	16(%r14), %eax
	movl	4(%r14), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	incl	%edx
	cmpl	%ecx, %eax
	cmovsl	%ebp, %edx
	imull	%esi, %edx
	movl	%edx, 56(%rbx)
	movl	20(%r14), %eax
	movl	8(%r14), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	incl	%edx
	cmpl	%ecx, %eax
	movl	12(%r14), %eax
	movl	16(%r14), %ecx
	movl	4(%r14), %esi
	cmovsl	%ebp, %edx
	movl	%ecx, %edi
	subl	%esi, %edi
	incl	%edi
	cmpl	%esi, %ecx
	movl	(%r14), %ecx
	cmovsl	%ebp, %edi
	movl	%eax, %esi
	subl	%ecx, %esi
	incl	%esi
	cmpl	%ecx, %eax
	cmovsl	%ebp, %esi
	imull	%edi, %esi
	imull	%edx, %esi
	movl	%esi, 60(%rbx)
	movq	%rbx, %rax
	addq	$52, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$4, %r14d
	xorl	%r11d, %r11d
.LBB11_1:                               # %.lr.ph116
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_2 Depth 2
                                        #       Child Loop BB11_6 Depth 3
                                        #       Child Loop BB11_9 Depth 3
	movq	%r11, %r8
	notq	%r8
	leal	-1(%r14), %edi
	movslq	%r14d, %r14
	movq	%rdi, %r9
	subq	%r11, %r9
	addq	%rdi, %r8
	movl	%edi, %r13d
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	addb	%bpl, %r13b
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_2:                               #   Parent Loop BB11_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB11_6 Depth 3
                                        #       Child Loop BB11_9 Depth 3
	cmpl	$1, 32(%rbx,%r11,4)
	jne	.LBB11_11
# BB#3:                                 # %.preheader
                                        #   in Loop: Header=BB11_2 Depth=2
	leaq	-1(%r14), %r15
	cmpq	%r15, %r11
	jge	.LBB11_12
# BB#4:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB11_2 Depth=2
	movq	%r9, %rax
	subq	%rbp, %rax
	movq	%r8, %r10
	subq	%rbp, %r10
	testb	$3, %al
	movq	%r11, %rsi
	je	.LBB11_8
# BB#5:                                 # %.lr.ph.prol.preheader
                                        #   in Loop: Header=BB11_2 Depth=2
	movl	%r13d, %eax
	andb	$3, %al
	movzbl	%al, %esi
	negq	%rsi
	xorl	%ecx, %ecx
	movq	8(%rsp), %rax           # 8-byte Reload
	.p2align	4, 0x90
.LBB11_6:                               # %.lr.ph.prol
                                        #   Parent Loop BB11_1 Depth=1
                                        #     Parent Loop BB11_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-16(%rax), %r12d
	movl	%r12d, -20(%rax)
	movl	(%rax), %edx
	movl	%edx, -4(%rax)
	addq	$4, %rax
	decq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB11_6
# BB#7:                                 # %.lr.ph.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB11_2 Depth=2
	movq	%r11, %rsi
	subq	%rcx, %rsi
.LBB11_8:                               # %.lr.ph.prol.loopexit
                                        #   in Loop: Header=BB11_2 Depth=2
	cmpq	$3, %r10
	jb	.LBB11_10
	.p2align	4, 0x90
.LBB11_9:                               # %.lr.ph
                                        #   Parent Loop BB11_1 Depth=1
                                        #     Parent Loop BB11_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	36(%rbx,%rsi,4), %eax
	movl	%eax, 32(%rbx,%rsi,4)
	movl	52(%rbx,%rsi,4), %eax
	movl	%eax, 48(%rbx,%rsi,4)
	movl	40(%rbx,%rsi,4), %eax
	movl	%eax, 36(%rbx,%rsi,4)
	movl	56(%rbx,%rsi,4), %eax
	movl	%eax, 52(%rbx,%rsi,4)
	movl	44(%rbx,%rsi,4), %eax
	movl	%eax, 40(%rbx,%rsi,4)
	movl	60(%rbx,%rsi,4), %eax
	movl	%eax, 56(%rbx,%rsi,4)
	movl	48(%rbx,%rsi,4), %eax
	movl	%eax, 44(%rbx,%rsi,4)
	movl	64(%rbx,%rsi,4), %eax
	movl	%eax, 60(%rbx,%rsi,4)
	leaq	4(%rsi), %rsi
	cmpq	%rsi, %rdi
	jne	.LBB11_9
.LBB11_10:                              # %._crit_edge
                                        #   in Loop: Header=BB11_2 Depth=2
	movl	$1, 28(%rbx,%r14,4)
	movl	$1, 44(%rbx,%r14,4)
	decq	%rdi
	incq	%rbp
	addb	$3, %r13b
	cmpq	%r15, %r11
	movq	%r15, %r14
	jl	.LBB11_2
	jmp	.LBB11_13
	.p2align	4, 0x90
.LBB11_11:                              # %.outer
                                        #   in Loop: Header=BB11_1 Depth=1
	incq	%r11
	movslq	%r14d, %rax
	addq	$4, 8(%rsp)             # 8-byte Folded Spill
	movq	32(%rsp), %rbp          # 8-byte Reload
	addb	$3, %bpl
	cmpq	%rax, %r11
	jl	.LBB11_1
	jmp	.LBB11_14
.LBB11_12:                              # %._crit_edge.thread
	movl	$1, 28(%rbx,%r14,4)
	movl	$1, 44(%rbx,%r14,4)
.LBB11_13:                              # %.outer._crit_edge.loopexit
	movl	%r15d, %r14d
.LBB11_14:                              # %.outer._crit_edge
	testl	%r14d, %r14d
	movl	$1, %eax
	cmovnel	%r14d, %eax
	movl	%eax, 28(%rbx)
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	hypre_CommTypeEntryCreate, .Lfunc_end11-hypre_CommTypeEntryCreate
	.cfi_endproc

	.globl	hypre_CommTypeSort
	.p2align	4, 0x90
	.type	hypre_CommTypeSort,@function
hypre_CommTypeSort:                     # @hypre_CommTypeSort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi114:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi115:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi116:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi117:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi118:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi119:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi120:
	.cfi_def_cfa_offset 128
.Lcfi121:
	.cfi_offset %rbx, -56
.Lcfi122:
	.cfi_offset %r12, -48
.Lcfi123:
	.cfi_offset %r13, -40
.Lcfi124:
	.cfi_offset %r14, -32
.Lcfi125:
	.cfi_offset %r15, -24
.Lcfi126:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movslq	8(%rdi), %rcx
	movq	%rcx, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	cmpq	$2, %rcx
	jl	.LBB12_31
# BB#1:                                 # %.critedge.preheader.preheader
	movq	(%rdi), %r13
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %eax
	movl	%eax, 44(%rsp)          # 4-byte Spill
	movl	%eax, %r12d
	movq	%rbp, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB12_2:                               # %.critedge.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_4 Depth 2
	testl	%r12d, %r12d
	jle	.LBB12_8
# BB#3:                                 # %.lr.ph152.preheader
                                        #   in Loop: Header=BB12_2 Depth=1
	xorl	%eax, %eax
	movq	%r12, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB12_4:                               # %.lr.ph152
                                        #   Parent Loop BB12_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %rbx
	movq	(%r13,%rbx,8), %r14
	leaq	1(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	8(%r13,%rbx,8), %r15
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	hypre_IModPeriodZ
	movq	%rbp, %rsi
	movl	%eax, %ebp
	movq	%r15, %rdi
	callq	hypre_IModPeriodZ
	cmpl	%eax, %ebp
	jle	.LBB12_5
.LBB12_13:                              # %.critedge139
                                        #   in Loop: Header=BB12_4 Depth=2
	movq	(%r13,%rbx,8), %rax
	movq	8(%r13,%rbx,8), %rcx
	movq	%rcx, (%r13,%rbx,8)
	movq	%rax, 8(%r13,%rbx,8)
	jmp	.LBB12_6
	.p2align	4, 0x90
.LBB12_5:                               #   in Loop: Header=BB12_4 Depth=2
	movq	%r14, %rdi
	movq	(%rsp), %r12            # 8-byte Reload
	movq	%r12, %rsi
	callq	hypre_IModPeriodZ
	movl	%eax, %ebp
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	16(%rsp), %r12          # 8-byte Reload
	callq	hypre_IModPeriodZ
	cmpl	%eax, %ebp
	jne	.LBB12_6
# BB#10:                                #   in Loop: Header=BB12_4 Depth=2
	movq	%r14, %rdi
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	%rbp, %rsi
	callq	hypre_IModPeriodY
	movl	%eax, %r12d
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	hypre_IModPeriodY
	cmpl	%eax, %r12d
	movq	16(%rsp), %r12          # 8-byte Reload
	jg	.LBB12_13
# BB#11:                                #   in Loop: Header=BB12_4 Depth=2
	movq	%r14, %rdi
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	%rbp, %rsi
	callq	hypre_IModPeriodY
	movl	%eax, %r12d
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	hypre_IModPeriodY
	cmpl	%eax, %r12d
	movq	16(%rsp), %r12          # 8-byte Reload
	jne	.LBB12_6
# BB#12:                                #   in Loop: Header=BB12_4 Depth=2
	movq	%r14, %rdi
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	%rbp, %rsi
	callq	hypre_IModPeriodX
	movl	%eax, %r14d
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	hypre_IModPeriodX
	cmpl	%eax, %r14d
	jg	.LBB12_13
	.p2align	4, 0x90
.LBB12_6:                               # %.critedge.backedge
                                        #   in Loop: Header=BB12_4 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpq	%rax, %r12
	movq	(%rsp), %rbp            # 8-byte Reload
	jne	.LBB12_4
# BB#7:                                 # %.critedge._crit_edge
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	%r12, %rax
	decq	%rax
	cmpl	$1, %r12d
	movq	%rax, %r12
	jg	.LBB12_2
.LBB12_8:                               # %.preheader146
	cmpl	$2, 24(%rsp)            # 4-byte Folded Reload
	jl	.LBB12_31
# BB#9:                                 # %.lr.ph150.preheader
	leaq	8(%r13), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB12_15:                              # %.lr.ph150
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_16 Depth 2
                                        #     Child Loop BB12_22 Depth 2
                                        #       Child Loop BB12_23 Depth 3
	movl	%ecx, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movslq	%ecx, %rdx
	movq	(%r13,%rdx,8), %r15
	movq	%rdx, %rcx
	shlq	$32, %rcx
	leaq	1(%rdx), %r12
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	leaq	(%rax,%rdx,8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB12_16:                              #   Parent Loop BB12_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %r14
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	leaq	(%r12,%r14), %rax
	cmpq	24(%rsp), %rax          # 8-byte Folded Reload
	jge	.LBB12_20
# BB#17:                                #   in Loop: Header=BB12_16 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r14,8), %rbx
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	hypre_IModPeriodX
	movl	%eax, %ebp
	movq	%rbx, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	hypre_IModPeriodX
	cmpl	%eax, %ebp
	movq	(%rsp), %rbp            # 8-byte Reload
	jne	.LBB12_20
# BB#18:                                #   in Loop: Header=BB12_16 Depth=2
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	hypre_IModPeriodY
	movl	%eax, %ebp
	movq	%rbx, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	hypre_IModPeriodY
	cmpl	%eax, %ebp
	movq	(%rsp), %rbp            # 8-byte Reload
	jne	.LBB12_20
# BB#19:                                #   in Loop: Header=BB12_16 Depth=2
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	hypre_IModPeriodZ
	movl	%eax, %ebp
	movq	%rbx, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	hypre_IModPeriodZ
	leaq	1(%r14), %rdx
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	(%rsi,%rcx), %rcx
	cmpl	%eax, %ebp
	movq	(%rsp), %rbp            # 8-byte Reload
	je	.LBB12_16
.LBB12_20:                              # %.preheader
                                        #   in Loop: Header=BB12_15 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	1(%rax,%r14), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	56(%rsp), %rcx          # 8-byte Reload
	addl	%r14d, %ecx
	cmpl	%eax, %ecx
	jle	.LBB12_14
# BB#21:                                # %.critedge142.preheader.preheader
                                        #   in Loop: Header=BB12_15 Depth=1
	sarq	$32, 8(%rsp)            # 8-byte Folded Spill
	.p2align	4, 0x90
.LBB12_22:                              # %.lr.ph.preheader
                                        #   Parent Loop BB12_15 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_23 Depth 3
	movq	64(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB12_23:                              # %.lr.ph
                                        #   Parent Loop BB12_15 Depth=1
                                        #     Parent Loop BB12_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r14, %r12
	movq	(%r13,%r12,8), %rbx
	addq	$12, %rbx
	leaq	1(%r12), %r14
	movq	8(%r13,%r12,8), %r15
	addq	$12, %r15
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	hypre_IModPeriodZ
	movl	%eax, %ebp
	movq	%r15, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	hypre_IModPeriodZ
	cmpl	%eax, %ebp
	movq	(%rsp), %rbp            # 8-byte Reload
	jle	.LBB12_24
.LBB12_30:                              # %.critedge144
                                        #   in Loop: Header=BB12_23 Depth=3
	movq	(%r13,%r12,8), %rax
	movq	8(%r13,%r12,8), %rcx
	movq	%rcx, (%r13,%r12,8)
	movq	%rax, 8(%r13,%r12,8)
	cmpq	%r14, 8(%rsp)           # 8-byte Folded Reload
	jne	.LBB12_23
	jmp	.LBB12_26
	.p2align	4, 0x90
.LBB12_24:                              #   in Loop: Header=BB12_23 Depth=3
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	hypre_IModPeriodZ
	movl	%eax, %ebp
	movq	%r15, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	hypre_IModPeriodZ
	cmpl	%eax, %ebp
	movq	(%rsp), %rbp            # 8-byte Reload
	jne	.LBB12_25
# BB#27:                                #   in Loop: Header=BB12_23 Depth=3
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	hypre_IModPeriodY
	movl	%eax, %ebp
	movq	%r15, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	hypre_IModPeriodY
	cmpl	%eax, %ebp
	movq	(%rsp), %rbp            # 8-byte Reload
	jg	.LBB12_30
# BB#28:                                #   in Loop: Header=BB12_23 Depth=3
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	hypre_IModPeriodY
	movl	%eax, %ebp
	movq	%r15, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	hypre_IModPeriodY
	cmpl	%eax, %ebp
	movq	(%rsp), %rbp            # 8-byte Reload
	jne	.LBB12_25
# BB#29:                                #   in Loop: Header=BB12_23 Depth=3
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	hypre_IModPeriodX
	movl	%eax, %ebx
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	hypre_IModPeriodX
	cmpl	%eax, %ebx
	jg	.LBB12_30
	.p2align	4, 0x90
.LBB12_25:                              # %.critedge142.backedge
                                        #   in Loop: Header=BB12_23 Depth=3
	cmpq	%r14, 8(%rsp)           # 8-byte Folded Reload
	jne	.LBB12_23
.LBB12_26:                              # %.critedge142._crit_edge
                                        #   in Loop: Header=BB12_22 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	decq	%rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpl	16(%rsp), %eax          # 4-byte Folded Reload
	jg	.LBB12_22
.LBB12_14:                              # %.loopexit
                                        #   in Loop: Header=BB12_15 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpl	44(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, %ecx
	jl	.LBB12_15
.LBB12_31:                              # %._crit_edge
	xorl	%eax, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	hypre_CommTypeSort, .Lfunc_end12-hypre_CommTypeSort
	.cfi_endproc

	.globl	hypre_CommTypeBuildMPI
	.p2align	4, 0x90
	.type	hypre_CommTypeBuildMPI,@function
hypre_CommTypeBuildMPI:                 # @hypre_CommTypeBuildMPI
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi127:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi128:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi129:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi130:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi131:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi132:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi133:
	.cfi_def_cfa_offset 96
.Lcfi134:
	.cfi_offset %rbx, -56
.Lcfi135:
	.cfi_offset %r12, -48
.Lcfi136:
	.cfi_offset %r13, -40
.Lcfi137:
	.cfi_offset %r14, -32
.Lcfi138:
	.cfi_offset %r15, -24
.Lcfi139:
	.cfi_offset %rbp, -16
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	testl	%edi, %edi
	jle	.LBB13_10
# BB#1:                                 # %.lr.ph63.preheader
	movl	%edi, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB13_2:                               # %.lr.ph63
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_4 Depth 2
                                        #     Child Loop BB13_7 Depth 2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r14,8), %r13
	movl	8(%r13), %ebp
	leal	(,%rbp,4), %ebx
	movl	%ebx, %edi
	callq	hypre_MAlloc
	movq	%rax, %r12
	movl	%ebx, %edi
	callq	hypre_MAlloc
	movq	%rax, %r15
	movl	%ebx, %edi
	callq	hypre_MAlloc
	movq	%rax, %rbx
	testl	%ebp, %ebp
	jle	.LBB13_8
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB13_2 Depth=1
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	%rbx, %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB13_4:                               #   Parent Loop BB13_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rax
	movq	(%rax,%rbx,8), %rdi
	movl	$1, (%r12,%rbx,4)
	movl	24(%rdi), %eax
	shll	$3, %eax
	movl	%eax, (%r15,%rbx,4)
	movq	%r14, %rsi
	callq	hypre_CommTypeEntryBuildMPI
	incq	%rbx
	addq	$4, %r14
	cmpq	%rbx, %rbp
	jne	.LBB13_4
# BB#5:                                 # %._crit_edge
                                        #   in Loop: Header=BB13_2 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	leaq	(%rax,%r14,4), %rbx
	movl	%ebp, %edi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rcx
	movq	%rbx, %r8
	callq	hypre_MPI_Type_struct
	movq	%rbx, %rdi
	callq	hypre_MPI_Type_commit
	testl	%ebp, %ebp
	jle	.LBB13_9
# BB#6:                                 # %.lr.ph59.preheader
                                        #   in Loop: Header=BB13_2 Depth=1
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB13_7:                               # %.lr.ph59
                                        #   Parent Loop BB13_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	hypre_MPI_Type_free
	addq	$4, %rbx
	decq	%rbp
	jne	.LBB13_7
	jmp	.LBB13_9
	.p2align	4, 0x90
.LBB13_8:                               # %._crit_edge60.critedge
                                        #   in Loop: Header=BB13_2 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%rax,%r14,4), %r13
	movl	%ebp, %edi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%r13, %r8
	callq	hypre_MPI_Type_struct
	movq	%r13, %rdi
	callq	hypre_MPI_Type_commit
	movq	%rbx, %r13
.LBB13_9:                               # %._crit_edge60
                                        #   in Loop: Header=BB13_2 Depth=1
	movq	%r12, %rdi
	callq	hypre_Free
	movq	%r15, %rdi
	callq	hypre_Free
	movq	%r13, %rdi
	callq	hypre_Free
	incq	%r14
	cmpq	8(%rsp), %r14           # 8-byte Folded Reload
	jne	.LBB13_2
.LBB13_10:                              # %._crit_edge64
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	hypre_CommTypeBuildMPI, .Lfunc_end13-hypre_CommTypeBuildMPI
	.cfi_endproc

	.globl	hypre_CommTypeEntryBuildMPI
	.p2align	4, 0x90
	.type	hypre_CommTypeEntryBuildMPI,@function
hypre_CommTypeEntryBuildMPI:            # @hypre_CommTypeEntryBuildMPI
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi140:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi141:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi142:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi143:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi144:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi145:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi146:
	.cfi_def_cfa_offset 96
.Lcfi147:
	.cfi_offset %rbx, -56
.Lcfi148:
	.cfi_offset %r12, -48
.Lcfi149:
	.cfi_offset %r13, -40
.Lcfi150:
	.cfi_offset %r14, -32
.Lcfi151:
	.cfi_offset %r15, -24
.Lcfi152:
	.cfi_offset %rbp, -16
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movl	28(%rbx), %r14d
	cmpl	$1, %r14d
	jne	.LBB14_2
# BB#1:
	movl	32(%rbx), %edi
	movl	48(%rbx), %edx
	shll	$3, %edx
	movl	$1, %esi
	xorl	%ecx, %ecx
	movq	%rax, %r8
	callq	hypre_MPI_Type_hvector
	jmp	.LBB14_7
.LBB14_2:
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$1, %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movq	%rax, %rbp
	movl	$1, %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movq	%rax, %r13
	movl	32(%rbx), %edi
	movl	48(%rbx), %edx
	shll	$3, %edx
	movl	$1, %esi
	xorl	%ecx, %ecx
	movq	%rbp, %r8
	callq	hypre_MPI_Type_hvector
	movq	%rbp, %r8
	decl	%r14d
	movl	36(%rbx), %edi
	movl	52(%rbx), %edx
	shll	$3, %edx
	movl	(%r8), %ecx
	cmpl	$2, %r14d
	jl	.LBB14_3
# BB#4:                                 # %.lr.ph.preheader
	movl	%r14d, %r12d
	decq	%r12
	addq	$56, %rbx
	movq	%r8, %rax
	movq	%r8, %rsi
	movq	%r13, %r15
	movq	%r13, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB14_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movq	%r15, %r14
	movq	%rsi, %r13
	movq	%r8, %rbp
	movq	%rax, %r15
	movl	$1, %esi
	movq	%r14, %r8
	callq	hypre_MPI_Type_hvector
	movq	%r15, %rdi
	callq	hypre_MPI_Type_free
	movl	-16(%rbx), %edi
	movl	(%rbx), %edx
	shll	$3, %edx
	movl	(%r14), %ecx
	addq	$4, %rbx
	decq	%r12
	movq	%r14, %rax
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	jne	.LBB14_5
	jmp	.LBB14_6
.LBB14_3:
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%r8, %r14
.LBB14_6:                               # %._crit_edge
	movl	$1, %esi
	movq	32(%rsp), %r8           # 8-byte Reload
	callq	hypre_MPI_Type_hvector
	movq	%r14, %rdi
	callq	hypre_MPI_Type_free
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	hypre_Free
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	hypre_Free
.LBB14_7:
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	hypre_CommTypeEntryBuildMPI, .Lfunc_end14-hypre_CommTypeEntryBuildMPI
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
