	.text
	.file	"coarsen.bc"
	.globl	hypre_StructMapFineToCoarse
	.p2align	4, 0x90
	.type	hypre_StructMapFineToCoarse,@function
hypre_StructMapFineToCoarse:            # @hypre_StructMapFineToCoarse
	.cfi_startproc
# BB#0:
	movq	%rdx, %r8
	movl	(%rdi), %eax
	subl	(%rsi), %eax
	cltd
	idivl	(%r8)
	movl	%eax, (%rcx)
	movl	4(%rdi), %eax
	subl	4(%rsi), %eax
	cltd
	idivl	4(%r8)
	movl	%eax, 4(%rcx)
	movl	8(%rdi), %eax
	subl	8(%rsi), %eax
	cltd
	idivl	8(%r8)
	movl	%eax, 8(%rcx)
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	hypre_StructMapFineToCoarse, .Lfunc_end0-hypre_StructMapFineToCoarse
	.cfi_endproc

	.globl	hypre_StructMapCoarseToFine
	.p2align	4, 0x90
	.type	hypre_StructMapCoarseToFine,@function
hypre_StructMapCoarseToFine:            # @hypre_StructMapCoarseToFine
	.cfi_startproc
# BB#0:
	movl	(%rdx), %eax
	imull	(%rdi), %eax
	addl	(%rsi), %eax
	movl	%eax, (%rcx)
	movl	4(%rdx), %eax
	imull	4(%rdi), %eax
	addl	4(%rsi), %eax
	movl	%eax, 4(%rcx)
	movl	8(%rdx), %eax
	imull	8(%rdi), %eax
	addl	8(%rsi), %eax
	movl	%eax, 8(%rcx)
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	hypre_StructMapCoarseToFine, .Lfunc_end1-hypre_StructMapCoarseToFine
	.cfi_endproc

	.globl	hypre_StructCoarsen
	.p2align	4, 0x90
	.type	hypre_StructCoarsen,@function
hypre_StructCoarsen:                    # @hypre_StructCoarsen
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi6:
	.cfi_def_cfa_offset 352
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, 288(%rsp)          # 8-byte Spill
	movl	%ecx, %r12d
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rdi, %r13
	movl	(%r13), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	4(%r13), %eax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movq	24(%r13), %r14
	movq	(%r14), %rdi
	callq	hypre_BoxArrayDuplicate
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	8(%rax), %ebp
	movq	8(%r14), %rbx
	leal	(,%rbp,4), %r15d
	movl	%r15d, %edi
	callq	hypre_MAlloc
	movq	%rax, 72(%rsp)          # 8-byte Spill
	testl	%ebp, %ebp
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movl	%r15d, 96(%rsp)         # 4-byte Spill
	movl	%r12d, 224(%rsp)        # 4-byte Spill
	jle	.LBB2_18
# BB#1:                                 # %.lr.ph722.preheader
	cmpl	$7, %ebp
	jbe	.LBB2_2
# BB#9:                                 # %min.iters.checked
	movl	%ebp, %ecx
	andl	$7, %ecx
	movq	%rbp, %rax
	subq	%rcx, %rax
	je	.LBB2_2
# BB#10:                                # %vector.memcheck
	leaq	(%rbx,%rbp,4), %rdx
	movq	72(%rsp), %rsi          # 8-byte Reload
	cmpq	%rdx, %rsi
	jae	.LBB2_12
# BB#11:                                # %vector.memcheck
	leaq	(%rsi,%rbp,4), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB2_12
.LBB2_2:
	xorl	%eax, %eax
.LBB2_3:                                # %.lr.ph722.preheader862
	movl	%ebp, %edx
	subl	%eax, %edx
	leaq	-1(%rbp), %rcx
	subq	%rax, %rcx
	andq	$7, %rdx
	je	.LBB2_6
# BB#4:                                 # %.lr.ph722.prol.preheader
	negq	%rdx
	movq	72(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph722.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rax,4), %esi
	movl	%esi, (%rdi,%rax,4)
	incq	%rax
	incq	%rdx
	jne	.LBB2_5
.LBB2_6:                                # %.lr.ph722.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB2_15
# BB#7:                                 # %.lr.ph722.preheader862.new
	movq	%rbp, %rcx
	subq	%rax, %rcx
	movq	72(%rsp), %rdx          # 8-byte Reload
	leaq	28(%rdx,%rax,4), %rdx
	leaq	28(%rbx,%rax,4), %rax
	.p2align	4, 0x90
.LBB2_8:                                # %.lr.ph722
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rax), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rax), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rax), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rax), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rax), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rax), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rax), %esi
	movl	%esi, -4(%rdx)
	movl	(%rax), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rax
	addq	$-8, %rcx
	jne	.LBB2_8
.LBB2_15:                               # %._crit_edge723
	movq	16(%r14), %rbx
	movl	96(%rsp), %edi          # 4-byte Reload
	callq	hypre_MAlloc
	movq	%rax, 88(%rsp)          # 8-byte Spill
	testl	%ebp, %ebp
	jle	.LBB2_32
# BB#16:                                # %.lr.ph718.preheader
	cmpl	$8, %ebp
	jb	.LBB2_17
# BB#19:                                # %min.iters.checked836
	movl	%ebp, %ecx
	andl	$7, %ecx
	movq	%rbp, %rax
	subq	%rcx, %rax
	je	.LBB2_17
# BB#20:                                # %vector.memcheck848
	leaq	(%rbx,%rbp,4), %rdx
	movq	88(%rsp), %rsi          # 8-byte Reload
	cmpq	%rdx, %rsi
	jae	.LBB2_23
# BB#21:                                # %vector.memcheck848
	leaq	(%rsi,%rbp,4), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB2_23
# BB#22:
	xorl	%eax, %eax
	movq	%rsi, %rdi
	jmp	.LBB2_26
.LBB2_18:                               # %._crit_edge723.thread
	movl	%r15d, %edi
	callq	hypre_MAlloc
	movq	%rax, 88(%rsp)          # 8-byte Spill
	jmp	.LBB2_32
.LBB2_17:
	xorl	%eax, %eax
	movq	88(%rsp), %rdi          # 8-byte Reload
.LBB2_26:                               # %.lr.ph718.preheader861
	movl	%ebp, %edx
	subl	%eax, %edx
	leaq	-1(%rbp), %rcx
	subq	%rax, %rcx
	andq	$7, %rdx
	je	.LBB2_29
# BB#27:                                # %.lr.ph718.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB2_28:                               # %.lr.ph718.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rax,4), %esi
	movl	%esi, (%rdi,%rax,4)
	incq	%rax
	incq	%rdx
	jne	.LBB2_28
.LBB2_29:                               # %.lr.ph718.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB2_32
# BB#30:                                # %.lr.ph718.preheader861.new
	movq	%rbp, %rcx
	subq	%rax, %rcx
	leaq	28(%rdi,%rax,4), %rdx
	leaq	28(%rbx,%rax,4), %rax
	.p2align	4, 0x90
.LBB2_31:                               # %.lr.ph718
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rax), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rax), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rax), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rax), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rax), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rax), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rax), %esi
	movl	%esi, -4(%rdx)
	movl	(%rax), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rax
	addq	$-8, %rcx
	jne	.LBB2_31
.LBB2_32:
	movq	120(%rsp), %r15         # 8-byte Reload
	movl	20(%rsp), %ebx          # 4-byte Reload
	movl	24(%r14), %r12d
	movl	28(%r14), %eax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	32(%r14), %eax
	movl	%eax, 84(%rsp)          # 4-byte Spill
	movl	32(%r13), %eax
	movl	%eax, 220(%rsp)         # 4-byte Spill
	movq	40(%r13), %rdi
	callq	hypre_BoxDuplicate
	movq	%rax, %rbp
	movl	56(%r13), %eax
	movl	%eax, 188(%rsp)
	movl	60(%r13), %eax
	movl	%eax, 192(%rsp)
	movl	64(%r13), %eax
	movl	%eax, 196(%rsp)
	leaq	228(%rsp), %rsi
	movl	%ebx, %edi
	callq	hypre_MPI_Comm_rank
	movq	%rbp, %rdi
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r15, %rdx
	callq	hypre_ProjectBox
	movl	(%rbp), %eax
	subl	(%rbx), %eax
	cltd
	idivl	(%r15)
	movl	%eax, (%rbp)
	movl	4(%rbp), %eax
	subl	4(%rbx), %eax
	cltd
	idivl	4(%r15)
	movl	%eax, 4(%rbp)
	movl	8(%rbp), %eax
	subl	8(%rbx), %eax
	cltd
	idivl	8(%r15)
	movl	%eax, 8(%rbp)
	movl	12(%rbp), %eax
	subl	(%rbx), %eax
	cltd
	idivl	(%r15)
	movl	%eax, 12(%rbp)
	movl	16(%rbp), %eax
	subl	4(%rbx), %eax
	cltd
	idivl	4(%r15)
	movl	%eax, 16(%rbp)
	movl	20(%rbp), %eax
	subl	8(%rbx), %eax
	cltd
	idivl	8(%r15)
	movq	%rbp, 272(%rsp)         # 8-byte Spill
	movl	%eax, 20(%rbp)
	callq	hypre_BoxCreate
	movq	%rax, %r13
	callq	hypre_BoxCreate
	movq	%rax, %r14
	cmpl	$0, 56(%rsp)            # 4-byte Folded Reload
	movq	%r12, 64(%rsp)          # 8-byte Spill
	jle	.LBB2_33
# BB#34:                                # %.lr.ph708
	movslq	%r12d, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	20(,%rax,8), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movq	72(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_35:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_38 Depth 2
	movl	(%r15,%rsi,4), %eax
	cmpl	228(%rsp), %eax
	je	.LBB2_47
# BB#36:                                # %.preheader637
                                        #   in Loop: Header=BB2_35 Depth=1
	movq	144(%rsp), %rax         # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB2_47
# BB#37:                                # %.lr.ph695.preheader
                                        #   in Loop: Header=BB2_35 Depth=1
	movq	160(%rsp), %rbx         # 8-byte Reload
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_38:                               # %.lr.ph695
                                        #   Parent Loop BB2_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rbp
	movl	-20(%rbp,%rbx), %eax
	movl	%eax, (%r13)
	movl	-16(%rbp,%rbx), %eax
	movl	%eax, 4(%r13)
	movl	-12(%rbp,%rbx), %eax
	movl	%eax, 8(%r13)
	movl	-8(%rbp,%rbx), %eax
	movl	%eax, 12(%r13)
	movl	-4(%rbp,%rbx), %eax
	movl	%eax, 16(%r13)
	movl	(%rbp,%rbx), %eax
	movl	%eax, 20(%r13)
	movq	%r13, %rdi
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rsi
	movq	120(%rsp), %r15         # 8-byte Reload
	movq	%r15, %rdx
	callq	hypre_ProjectBox
	movl	(%r13), %eax
	subl	(%r12), %eax
	cltd
	idivl	(%r15)
	movl	%eax, (%r13)
	movl	4(%r13), %eax
	subl	4(%r12), %eax
	cltd
	idivl	4(%r15)
	movl	%eax, 4(%r13)
	movl	8(%r13), %eax
	subl	8(%r12), %eax
	cltd
	idivl	8(%r15)
	movl	%eax, 8(%r13)
	movl	12(%r13), %eax
	subl	(%r12), %eax
	cltd
	idivl	(%r15)
	movl	%eax, 12(%r13)
	movl	16(%r13), %eax
	subl	4(%r12), %eax
	cltd
	idivl	4(%r15)
	movl	%eax, 16(%r13)
	movl	20(%r13), %eax
	subl	8(%r12), %eax
	cltd
	idivl	8(%r15)
	movl	%eax, 20(%r13)
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rax,2), %rax
	movl	(%rbp,%rax,8), %ecx
	movl	%ecx, (%r14)
	movl	4(%rbp,%rax,8), %ecx
	movl	%ecx, 4(%r14)
	movl	8(%rbp,%rax,8), %ecx
	movl	%ecx, 8(%r14)
	movl	12(%rbp,%rax,8), %ecx
	movl	%ecx, 12(%r14)
	movl	16(%rbp,%rax,8), %ecx
	movl	%ecx, 16(%r14)
	movl	20(%rbp,%rax,8), %eax
	movl	%eax, 20(%r14)
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	hypre_ProjectBox
	movl	(%r14), %eax
	subl	(%r12), %eax
	cltd
	idivl	(%r15)
	movl	%eax, (%r14)
	movl	4(%r14), %eax
	subl	4(%r12), %eax
	cltd
	idivl	4(%r15)
	movl	%eax, 4(%r14)
	movl	8(%r14), %eax
	subl	8(%r12), %eax
	cltd
	idivl	8(%r15)
	movl	%eax, 8(%r14)
	movl	12(%r14), %eax
	subl	(%r12), %eax
	cltd
	idivl	(%r15)
	movl	%eax, 12(%r14)
	movl	16(%r14), %eax
	subl	4(%r12), %eax
	cltd
	idivl	4(%r15)
	movl	%eax, 16(%r14)
	movl	20(%r14), %eax
	subl	8(%r12), %eax
	cltd
	idivl	8(%r15)
	movl	%eax, 20(%r14)
	movq	136(%rsp), %rbp         # 8-byte Reload
	testl	%ebp, %ebp
	je	.LBB2_39
# BB#40:                                #   in Loop: Header=BB2_38 Depth=2
	movq	72(%rsp), %r15          # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	(%r15,%rsi,4), %edx
	movslq	%ebp, %rcx
	movq	128(%rsp), %rax         # 8-byte Reload
	cmpl	-4(%rax,%rcx,4), %edx
	movq	24(%rsp), %r12          # 8-byte Reload
	jne	.LBB2_41
	jmp	.LBB2_42
	.p2align	4, 0x90
.LBB2_39:                               #   in Loop: Header=BB2_38 Depth=2
	movl	96(%rsp), %edi          # 4-byte Reload
	callq	hypre_MAlloc
	movq	72(%rsp), %r15          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	(%r15,%rcx,4), %edx
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	%rcx, %rsi
.LBB2_41:                               # %.sink.split
                                        #   in Loop: Header=BB2_38 Depth=2
	movslq	%ebp, %rcx
	movl	%edx, (%rax,%rcx,4)
	incl	%ebp
.LBB2_42:                               #   in Loop: Header=BB2_38 Depth=2
	testl	%r12d, %r12d
	movq	%rbp, 136(%rsp)         # 8-byte Spill
	movq	%rax, 128(%rsp)         # 8-byte Spill
	je	.LBB2_43
# BB#44:                                #   in Loop: Header=BB2_38 Depth=2
	movl	(%r15,%rsi,4), %eax
	movslq	%r12d, %rcx
	movq	112(%rsp), %rdx         # 8-byte Reload
	cmpl	-4(%rdx,%rcx,4), %eax
	jne	.LBB2_45
	jmp	.LBB2_46
	.p2align	4, 0x90
.LBB2_43:                               #   in Loop: Header=BB2_38 Depth=2
	movl	96(%rsp), %edi          # 4-byte Reload
	movq	%rsi, %rbp
	callq	hypre_MAlloc
	movq	%rbp, %rsi
	movq	%rax, %rdx
	movl	(%r15,%rsi,4), %eax
	movq	%rdx, 200(%rsp)         # 8-byte Spill
.LBB2_45:                               # %.sink.split621
                                        #   in Loop: Header=BB2_38 Depth=2
	movslq	%r12d, %rcx
	movl	%eax, (%rdx,%rcx,4)
	incl	%r12d
.LBB2_46:                               #   in Loop: Header=BB2_38 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	addq	$24, %rbx
	decq	%rax
	jne	.LBB2_38
.LBB2_47:                               # %.loopexit638
                                        #   in Loop: Header=BB2_35 Depth=1
	incq	%rsi
	cmpq	56(%rsp), %rsi          # 8-byte Folded Reload
	jne	.LBB2_35
	jmp	.LBB2_48
.LBB2_33:
	xorl	%eax, %eax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
.LBB2_48:                               # %._crit_edge709
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movq	%r13, %rdi
	callq	hypre_BoxDestroy
	movq	%r14, %rdi
	callq	hypre_BoxDestroy
	movq	56(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	movq	120(%rsp), %r15         # 8-byte Reload
	movq	104(%rsp), %r12         # 8-byte Reload
	movq	32(%rsp), %r13          # 8-byte Reload
	jle	.LBB2_51
# BB#49:                                # %.lr.ph688
	xorl	%ebx, %ebx
	movq	%rax, %r14
	.p2align	4, 0x90
.LBB2_50:                               # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rbp
	leaq	(%rbp,%rbx), %rdi
	movq	%r13, %rsi
	movq	%r15, %rdx
	callq	hypre_ProjectBox
	movl	(%rbp,%rbx), %eax
	subl	(%r13), %eax
	cltd
	idivl	(%r15)
	movl	%eax, (%rbp,%rbx)
	movl	4(%rbp,%rbx), %eax
	subl	4(%r13), %eax
	cltd
	idivl	4(%r15)
	movl	%eax, 4(%rbp,%rbx)
	movl	8(%rbp,%rbx), %eax
	subl	8(%r13), %eax
	cltd
	idivl	8(%r15)
	movl	%eax, 8(%rbp,%rbx)
	movl	12(%rbp,%rbx), %eax
	subl	(%r13), %eax
	cltd
	idivl	(%r15)
	movl	%eax, 12(%rbp,%rbx)
	movl	16(%rbp,%rbx), %eax
	subl	4(%r13), %eax
	cltd
	idivl	4(%r15)
	movl	%eax, 16(%rbp,%rbx)
	movl	20(%rbp,%rbx), %eax
	subl	8(%r13), %eax
	cltd
	idivl	8(%r15)
	movl	%eax, 20(%rbp,%rbx)
	addq	$24, %rbx
	decq	%r14
	jne	.LBB2_50
.LBB2_51:                               # %._crit_edge689
	movq	136(%rsp), %rbx         # 8-byte Reload
	testl	%ebx, %ebx
	movl	20(%rsp), %r12d         # 4-byte Reload
	je	.LBB2_52
# BB#53:
	leal	(,%rbx,4), %r14d
	movl	%r14d, %edi
	callq	hypre_MAlloc
	movq	%rax, %r13
	movl	%r14d, %edi
	callq	hypre_MAlloc
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%r14d, %edi
	callq	hypre_MAlloc
	movq	%rax, %r15
	testl	%ebx, %ebx
	jle	.LBB2_56
# BB#54:                                # %.lr.ph685.preheader
	movl	%ebx, %ebx
	xorl	%ebp, %ebp
	movq	128(%rsp), %r14         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_55:                               # %.lr.ph685
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%r15,%rbp), %rdi
	movl	(%r14,%rbp), %ecx
	leaq	(%r13,%rbp), %rax
	movq	%rax, (%rsp)
	movl	$1, %esi
	movl	$1, %edx
	xorl	%r8d, %r8d
	movl	%r12d, %r9d
	callq	hypre_MPI_Irecv
	addq	$4, %rbp
	decq	%rbx
	jne	.LBB2_55
	jmp	.LBB2_56
.LBB2_52:
                                        # implicit-def: %R13
                                        # implicit-def: %RAX
	movq	%rax, 32(%rsp)          # 8-byte Spill
                                        # implicit-def: %R15
.LBB2_56:
	movq	24(%rsp), %rbp          # 8-byte Reload
	testl	%ebp, %ebp
	movq	%r13, 48(%rsp)          # 8-byte Spill
	je	.LBB2_57
# BB#58:
	leal	(,%rbp,4), %r13d
	movl	%r13d, %edi
	callq	hypre_MAlloc
	movq	%rax, %r14
	movl	%r13d, %edi
	callq	hypre_MAlloc
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	8(%rax), %eax
	shll	$3, %eax
	movl	%eax, 172(%rsp)
	testl	%ebp, %ebp
	jle	.LBB2_59
# BB#60:                                # %.lr.ph682.preheader
	movl	%ebp, %ebx
	xorl	%ebp, %ebp
	movl	20(%rsp), %r12d         # 4-byte Reload
	movq	%r14, %r13
	movq	112(%rsp), %r14         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_61:                               # %.lr.ph682
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14,%rbp), %ecx
	leaq	(%r13,%rbp), %rax
	movq	%rax, (%rsp)
	movl	$1, %esi
	movl	$1, %edx
	xorl	%r8d, %r8d
	leaq	172(%rsp), %rdi
	movl	%r12d, %r9d
	callq	hypre_MPI_Isend
	addq	$4, %rbp
	decq	%rbx
	jne	.LBB2_61
# BB#62:
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	%r13, %r14
	movq	%rax, %r13
	jmp	.LBB2_63
.LBB2_57:
                                        # implicit-def: %R14
                                        # implicit-def: %RAX
	movq	%rax, 160(%rsp)         # 8-byte Spill
	jmp	.LBB2_63
.LBB2_59:
	movq	48(%rsp), %r13          # 8-byte Reload
.LBB2_63:                               # %.loopexit635
	movq	136(%rsp), %rdi         # 8-byte Reload
	testl	%edi, %edi
	je	.LBB2_65
# BB#64:
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r13, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	hypre_MPI_Waitall
.LBB2_65:
	testl	%ebp, %ebp
	movl	20(%rsp), %r13d         # 4-byte Reload
	je	.LBB2_67
# BB#66:
	movl	%ebp, %edi
	movq	%r14, %rsi
	movq	160(%rsp), %rdx         # 8-byte Reload
	callq	hypre_MPI_Waitall
.LBB2_67:
	movq	136(%rsp), %rbx         # 8-byte Reload
	testl	%ebx, %ebx
	je	.LBB2_68
# BB#69:
	leal	(,%rbx,8), %edi
	callq	hypre_MAlloc
	testl	%ebx, %ebx
	jle	.LBB2_70
# BB#71:                                # %.lr.ph679.preheader
	movq	%r14, 152(%rsp)         # 8-byte Spill
	movl	%ebx, %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	128(%rsp), %r14         # 8-byte Reload
	movl	%r13d, %r12d
	movq	%rax, %r13
	.p2align	4, 0x90
.LBB2_72:                               # %.lr.ph679
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rbp,4), %edi
	shll	$2, %edi
	callq	hypre_MAlloc
	movq	%rax, (%r13,%rbp,8)
	movl	(%r15,%rbp,4), %esi
	movl	(%r14,%rbp,4), %ecx
	movq	%rbx, (%rsp)
	movl	$1, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rdi
	movl	%r12d, %r9d
	callq	hypre_MPI_Irecv
	incq	%rbp
	addq	$4, %rbx
	cmpq	%rbp, 40(%rsp)          # 8-byte Folded Reload
	jne	.LBB2_72
# BB#73:
	movq	%r13, %rcx
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	152(%rsp), %r14         # 8-byte Reload
	movq	%r13, %rax
	movl	%r12d, %r13d
	jmp	.LBB2_74
.LBB2_68:
                                        # implicit-def: %RAX
                                        # implicit-def: %RCX
	jmp	.LBB2_74
.LBB2_70:
	movq	%rax, %rcx
.LBB2_74:                               # %.loopexit634
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	testl	%ebp, %ebp
	je	.LBB2_75
# BB#76:
	movl	172(%rsp), %edi
	shll	$2, %edi
	callq	hypre_MAlloc
	movq	%rax, %r12
	cmpl	$0, 56(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_79
# BB#77:                                # %.lr.ph676
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	addq	$20, %rax
	movl	$5, %ecx
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	88(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_78:                               # =>This Inner Loop Header: Depth=1
	leal	-5(%rcx), %ebp
	movl	(%rdi), %ebx
	leal	-4(%rcx), %edx
	movslq	%ebp, %rbp
	movl	%ebx, (%r12,%rbp,4)
	movl	(%rsi), %ebp
	movslq	%edx, %rdx
	movl	%ebp, (%r12,%rdx,4)
	leal	-3(%rcx), %edx
	movslq	%edx, %rdx
	movl	-20(%rax), %ebp
	leal	-2(%rcx), %ebx
	movl	%ebp, (%r12,%rdx,4)
	movl	-8(%rax), %ebp
	movslq	%ebx, %rbx
	movl	%ebp, (%r12,%rbx,4)
	movl	-16(%rax), %ebp
	movl	%ebp, 8(%r12,%rdx,4)
	movl	-4(%rax), %ebp
	movslq	%ecx, %rcx
	movl	%ebp, (%r12,%rcx,4)
	leaq	2(%rdx), %rbp
	orq	$4, %rdx
	movl	-12(%rax), %ebx
	movl	%ebx, (%r12,%rdx,4)
	movl	(%rax), %edx
	movslq	%ebp, %rbp
	shlq	$2, %rbp
	orq	$12, %rbp
	movl	%edx, (%r12,%rbp)
	addq	$4, %rdi
	addq	$4, %rsi
	addq	$24, %rax
	addl	$8, %ecx
	decq	%r8
	jne	.LBB2_78
.LBB2_79:                               # %.preheader633
	movq	24(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB2_83
# BB#80:                                # %.lr.ph668.preheader
	movl	%eax, %ebx
	movq	%r14, 152(%rsp)         # 8-byte Spill
	movq	%r14, %rbp
	movq	112(%rsp), %r14         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_81:                               # %.lr.ph668
                                        # =>This Inner Loop Header: Depth=1
	movl	172(%rsp), %esi
	movl	(%r14), %ecx
	movq	%rbp, (%rsp)
	movl	$1, %edx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	%r13d, %r9d
	callq	hypre_MPI_Isend
	addq	$4, %r14
	addq	$4, %rbp
	decq	%rbx
	jne	.LBB2_81
# BB#82:
	movq	152(%rsp), %r14         # 8-byte Reload
	jmp	.LBB2_83
.LBB2_75:
                                        # implicit-def: %R12
.LBB2_83:                               # %.loopexit
	movq	136(%rsp), %rdi         # 8-byte Reload
	testl	%edi, %edi
	je	.LBB2_85
# BB#84:
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	callq	hypre_MPI_Waitall
	movq	%rbx, %rdi
	callq	hypre_Free
	movq	%rbp, %rdi
	callq	hypre_Free
.LBB2_85:
	movq	24(%rsp), %rdi          # 8-byte Reload
	testl	%edi, %edi
	je	.LBB2_87
# BB#86:
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r14, %rsi
	movq	160(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdx
	callq	hypre_MPI_Waitall
	movq	%r14, %rdi
	callq	hypre_Free
	movq	%rbx, %rdi
	callq	hypre_Free
	movq	%r12, %rdi
	callq	hypre_Free
.LBB2_87:
	movq	136(%rsp), %rbx         # 8-byte Reload
	testl	%ebx, %ebx
	je	.LBB2_88
# BB#89:
	movq	56(%rsp), %r13          # 8-byte Reload
	movl	%r13d, %edi
	callq	hypre_BoxArrayCreate
	movq	%rax, %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	hypre_BoxArraySetSize
	movl	96(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %edi
	callq	hypre_MAlloc
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%ebp, %edi
	callq	hypre_MAlloc
	movq	%rax, 24(%rsp)          # 8-byte Spill
	callq	hypre_BoxCreate
	movq	%rax, 232(%rsp)         # 8-byte Spill
	movl	$4, %esi
	movl	%ebx, %edi
	callq	hypre_CAlloc
	movq	%rax, %r14
	testl	%ebx, %ebx
	movq	%r12, 32(%rsp)          # 8-byte Spill
	jle	.LBB2_90
# BB#92:                                # %.split.us.preheader
	movl	%ebx, %ebx
	xorl	%r8d, %r8d
                                        # implicit-def: %EAX
	movl	%eax, 152(%rsp)         # 4-byte Spill
	movl	%r13d, %edi
                                        # implicit-def: %EBP
	movq	104(%rsp), %r13         # 8-byte Reload
	xorl	%r9d, %r9d
	jmp	.LBB2_93
	.p2align	4, 0x90
.LBB2_110:                              # %._crit_edge660.us
                                        #   in Loop: Header=BB2_93 Depth=1
	cmpl	$-2, %r12d
	jle	.LBB2_111
# BB#100:                               #   in Loop: Header=BB2_93 Depth=1
	movl	%edi, %eax
	cmpq	%rax, %r8
	movq	%r9, 48(%rsp)           # 8-byte Spill
	jne	.LBB2_101
# BB#102:                               #   in Loop: Header=BB2_93 Depth=1
	addl	56(%rsp), %edi          # 4-byte Folded Reload
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	leal	(,%rdi,4), %r13d
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	%r13d, %esi
	movq	%r8, 96(%rsp)           # 8-byte Spill
	callq	hypre_ReAlloc
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%r13d, %esi
	movq	104(%rsp), %r13         # 8-byte Reload
	callq	hypre_ReAlloc
	movq	48(%rsp), %r9           # 8-byte Reload
	movq	96(%rsp), %r8           # 8-byte Reload
	jmp	.LBB2_103
	.p2align	4, 0x90
.LBB2_101:                              #   in Loop: Header=BB2_93 Depth=1
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	movq	24(%rsp), %rax          # 8-byte Reload
.LBB2_103:                              #   in Loop: Header=BB2_93 Depth=1
	cmpl	$-1, %r12d
	movq	%rax, 24(%rsp)          # 8-byte Spill
	je	.LBB2_105
# BB#104:                               #   in Loop: Header=BB2_93 Depth=1
	movslq	%r12d, %rdx
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movslq	(%r14,%rdx,4), %rdi
	movq	128(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rdx,8), %rcx
	movl	(%rcx,%rdi,4), %edx
	movl	%edx, (%rax,%r8,4)
	movl	4(%rcx,%rdi,4), %edx
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	%edx, (%rsi,%r8,4)
	movl	8(%rcx,%rdi,4), %edx
	movl	%edx, 252(%rsp)
	movl	12(%rcx,%rdi,4), %edx
	movl	%edx, 240(%rsp)
	movl	16(%rcx,%rdi,4), %edx
	movl	%edx, 256(%rsp)
	movl	20(%rcx,%rdi,4), %edx
	movl	%edx, 244(%rsp)
	movl	24(%rcx,%rdi,4), %edx
	leaq	4(%rdi), %rsi
	movl	%edx, 260(%rsp)
	shlq	$32, %rsi
	movabsq	$12884901888, %rax      # imm = 0x300000000
	addq	%rax, %rsi
	sarq	$30, %rsi
	movl	(%rcx,%rsi), %ecx
	movl	%ecx, 248(%rsp)
	leal	8(%rdi), %eax
	movl	%eax, 160(%rsp)         # 4-byte Spill
	movq	232(%rsp), %r13         # 8-byte Reload
	movq	%r13, %rdi
	leaq	252(%rsp), %rsi
	leaq	240(%rsp), %rdx
	movq	%r8, %r12
	callq	hypre_BoxSetExtents
	movq	%r13, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	hypre_AppendBox
	movq	48(%rsp), %r9           # 8-byte Reload
	movq	%r12, %r8
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	160(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, (%r14,%rax,4)
	movq	104(%rsp), %r13         # 8-byte Reload
	jmp	.LBB2_106
	.p2align	4, 0x90
.LBB2_105:                              #   in Loop: Header=BB2_93 Depth=1
	movslq	%r9d, %rsi
	movq	72(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%rsi,4), %ecx
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	%ecx, (%rdx,%r8,4)
	movq	88(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%rsi,4), %ecx
	movl	%ecx, (%rax,%r8,4)
	movq	(%r13), %rcx
	leaq	(%rsi,%rsi,2), %rax
	leaq	(%rcx,%rax,8), %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%r8, %r12
	callq	hypre_AppendBox
	movq	48(%rsp), %r9           # 8-byte Reload
	movq	%r12, %r8
	cmpl	64(%rsp), %r9d          # 4-byte Folded Reload
	movl	152(%rsp), %eax         # 4-byte Reload
	cmovel	%r8d, %eax
	movl	%eax, 152(%rsp)         # 4-byte Spill
	incl	%r9d
.LBB2_106:                              # %.split.us
                                        #   in Loop: Header=BB2_93 Depth=1
	incq	%r8
	movq	112(%rsp), %rdi         # 8-byte Reload
.LBB2_93:                               # %.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_96 Depth 2
	movl	$-2, %r12d
	cmpl	56(%rsp), %r9d          # 4-byte Folded Reload
	jge	.LBB2_95
# BB#94:                                #   in Loop: Header=BB2_93 Depth=1
	movslq	%r9d, %rax
	movq	88(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%rax,4), %ebp
	movl	$-1, %r12d
.LBB2_95:                               # %.lr.ph659.us.preheader
                                        #   in Loop: Header=BB2_93 Depth=1
	xorl	%eax, %eax
	movq	128(%rsp), %rsi         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_96:                               # %.lr.ph659.us
                                        #   Parent Loop BB2_93 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%r14,%rax,4), %rcx
	cmpl	(%r15,%rax,4), %ecx
	jge	.LBB2_109
# BB#97:                                #   in Loop: Header=BB2_96 Depth=2
	cmpl	$-2, %r12d
	movq	(%rsi,%rax,8), %rdx
	movl	(%rdx,%rcx,4), %edx
	je	.LBB2_99
# BB#98:                                #   in Loop: Header=BB2_96 Depth=2
	cmpl	%ebp, %edx
	jl	.LBB2_99
# BB#107:                               #   in Loop: Header=BB2_96 Depth=2
	jne	.LBB2_109
# BB#108:                               #   in Loop: Header=BB2_96 Depth=2
	addl	$8, %ecx
	movl	%ecx, (%r14,%rax,4)
	jmp	.LBB2_109
	.p2align	4, 0x90
.LBB2_99:                               # %._crit_edge902
                                        #   in Loop: Header=BB2_96 Depth=2
	movl	%eax, %r12d
	movl	%edx, %ebp
.LBB2_109:                              #   in Loop: Header=BB2_96 Depth=2
	incq	%rax
	cmpq	%rax, %rbx
	jne	.LBB2_96
	jmp	.LBB2_110
.LBB2_111:
	movl	152(%rsp), %eax         # 4-byte Reload
	movl	%eax, %ebp
	jmp	.LBB2_112
.LBB2_88:
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jmp	.LBB2_116
.LBB2_90:                               # %.split.preheader
	testl	%r13d, %r13d
	jle	.LBB2_91
# BB#118:                               # %.preheader632.preheader
	xorl	%r12d, %r12d
	movq	64(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 112(%rsp)         # 4-byte Spill
	movl	%r13d, %ecx
                                        # implicit-def: %EBP
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB2_119:                              # %.preheader632
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %eax
	cmpq	%rax, %r8
	jne	.LBB2_120
# BB#121:                               #   in Loop: Header=BB2_119 Depth=1
	addl	56(%rsp), %ecx          # 4-byte Folded Reload
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leal	(,%rcx,4), %ebp
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %esi
	movq	%r8, 96(%rsp)           # 8-byte Spill
	callq	hypre_ReAlloc
	movq	%rax, %r13
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %esi
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	callq	hypre_ReAlloc
	movq	96(%rsp), %r8           # 8-byte Reload
	movq	%rax, %rcx
	jmp	.LBB2_122
	.p2align	4, 0x90
.LBB2_120:                              #   in Loop: Header=BB2_119 Depth=1
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
.LBB2_122:                              # %.split
                                        #   in Loop: Header=BB2_119 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r8,4), %eax
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movl	%eax, (%r13,%r8,4)
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r8,4), %eax
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	%eax, (%rcx,%r8,4)
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rdi
	addq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r8, %rbx
	callq	hypre_AppendBox
	movq	%rbx, %r8
	cmpl	%r8d, 112(%rsp)         # 4-byte Folded Reload
	cmovel	%r8d, %ebp
	incq	%r8
	addq	$24, %r12
	cmpl	56(%rsp), %r8d          # 4-byte Folded Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	jl	.LBB2_119
	jmp	.LBB2_112
.LBB2_12:                               # %vector.body.preheader
	leaq	16(%rbx), %rdx
	leaq	16(%rsi), %rsi
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB2_13:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	addq	$32, %rdx
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB2_13
# BB#14:                                # %middle.block
	testl	%ecx, %ecx
	jne	.LBB2_3
	jmp	.LBB2_15
.LBB2_91:
	xorl	%r8d, %r8d
                                        # implicit-def: %EBP
.LBB2_112:                              # %.preheader631
	movq	%r8, %r12
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	136(%rsp), %rax         # 8-byte Reload
	testl	%eax, %eax
	movq	128(%rsp), %rcx         # 8-byte Reload
	jle	.LBB2_115
# BB#113:                               # %.lr.ph654.preheader
	movl	%eax, %ebx
	.p2align	4, 0x90
.LBB2_114:                              # %.lr.ph654
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rbp
	movq	(%rbp), %rdi
	callq	hypre_Free
	movq	%rbp, %rcx
	movq	$0, (%rcx)
	addq	$8, %rcx
	decq	%rbx
	jne	.LBB2_114
.LBB2_115:                              # %._crit_edge655
	movq	264(%rsp), %rdi         # 8-byte Reload
	callq	hypre_Free
	movq	%r15, %rdi
	callq	hypre_Free
	movq	232(%rsp), %rdi         # 8-byte Reload
	callq	hypre_BoxDestroy
	movq	%r14, %rdi
	callq	hypre_Free
	movq	104(%rsp), %rdi         # 8-byte Reload
	callq	hypre_BoxArrayDestroy
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	hypre_Free
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	hypre_Free
	movl	%r12d, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	20(%rsp), %r13d         # 4-byte Reload
.LBB2_116:
	movq	144(%rsp), %rbp         # 8-byte Reload
	movq	200(%rsp), %rdi         # 8-byte Reload
	callq	hypre_Free
	movq	208(%rsp), %rdi         # 8-byte Reload
	callq	hypre_Free
	cmpl	$0, 224(%rsp)           # 4-byte Folded Reload
	je	.LBB2_117
# BB#123:                               # %.preheader
	movq	56(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB2_124
# BB#125:                               # %.lr.ph648
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %r15
	movq	64(%rsp), %rcx          # 8-byte Reload
	addl	%ecx, %ebp
	movl	84(%rsp), %edx          # 4-byte Reload
	addl	%ebp, %edx
	movslq	%edx, %r8
	movslq	%ebp, %r10
	movslq	%ecx, %r9
	movl	%eax, %r11d
	leaq	20(%r15), %rbx
	movl	$-1, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	xorl	%esi, %esi
	movl	$0, 84(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_126:                              # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	movl	-12(%rbx), %ebp
	movl	%ecx, %edx
	subl	%ebp, %edx
	incl	%edx
	cmpl	%ebp, %ecx
	movl	-4(%rbx), %edi
	movl	-20(%rbx), %r13d
	movl	-16(%rbx), %ebp
	cmovsl	%r14d, %edx
	movl	%edi, %eax
	subl	%ebp, %eax
	incl	%eax
	cmpl	%ebp, %edi
	movl	-8(%rbx), %ecx
	cmovsl	%r14d, %eax
	movl	%ecx, %edi
	subl	%r13d, %edi
	incl	%edi
	cmpl	%r13d, %ecx
	cmovsl	%r14d, %edi
	imull	%eax, %edi
	imull	%edx, %edi
	testl	%edi, %edi
	je	.LBB2_127
# BB#128:                               #   in Loop: Header=BB2_126 Depth=1
	movslq	%esi, %rax
	leaq	(%rax,%rax,2), %rcx
	movl	%r13d, (%r15,%rcx,8)
	movl	%ebp, 4(%r15,%rcx,8)
	movl	-12(%rbx), %edx
	movl	%edx, 8(%r15,%rcx,8)
	movl	-8(%rbx), %edx
	movl	%edx, 12(%r15,%rcx,8)
	movl	-4(%rbx), %edx
	movl	%edx, 16(%r15,%rcx,8)
	movl	(%rbx), %edx
	movl	%edx, 20(%r15,%rcx,8)
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx,%r12,4), %ecx
	movl	%ecx, (%rdx,%rax,4)
	movq	24(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx,%r12,4), %ecx
	movl	%ecx, (%rdx,%rax,4)
	cmpq	%r9, %r12
	jl	.LBB2_131
# BB#129:                               #   in Loop: Header=BB2_126 Depth=1
	cmpq	%r10, %r12
	jge	.LBB2_131
# BB#130:                               #   in Loop: Header=BB2_126 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	cmpl	$-1, %eax
	cmovel	%esi, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	144(%rsp), %rax         # 8-byte Reload
	incl	%eax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	jmp	.LBB2_132
	.p2align	4, 0x90
.LBB2_127:                              #   in Loop: Header=BB2_126 Depth=1
	movl	20(%rsp), %r13d         # 4-byte Reload
	jmp	.LBB2_133
	.p2align	4, 0x90
.LBB2_131:                              #   in Loop: Header=BB2_126 Depth=1
	cmpq	%r10, %r12
	setge	%al
	cmpq	%r8, %r12
	setl	%cl
	andb	%al, %cl
	movzbl	%cl, %eax
	addl	%eax, 84(%rsp)          # 4-byte Folded Spill
.LBB2_132:                              #   in Loop: Header=BB2_126 Depth=1
	movl	20(%rsp), %r13d         # 4-byte Reload
	incl	%esi
.LBB2_133:                              #   in Loop: Header=BB2_126 Depth=1
	incq	%r12
	addq	$24, %rbx
	cmpq	%r12, %r11
	jne	.LBB2_126
	jmp	.LBB2_134
.LBB2_117:
	movq	32(%rsp), %r14          # 8-byte Reload
	jmp	.LBB2_135
.LBB2_124:
	xorl	%eax, %eax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	$-1, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	$0, 84(%rsp)            # 4-byte Folded Spill
	xorl	%esi, %esi
.LBB2_134:                              # %._crit_edge649
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	callq	hypre_BoxArraySetSize
	movq	144(%rsp), %rbp         # 8-byte Reload
.LBB2_135:
	leaq	176(%rsp), %rdx
	movl	%r13d, %edi
	movq	280(%rsp), %rbx         # 8-byte Reload
	movl	%ebx, %esi
	callq	hypre_StructGridCreate
	movq	176(%rsp), %rdi
	movq	272(%rsp), %rax         # 8-byte Reload
	movq	%rax, 8(%rsp)
	movl	84(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rsp)
	movq	%r14, %rsi
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	64(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%ebp, %r9d
	callq	hypre_StructGridSetHood
	movq	176(%rsp), %rdi
	movl	220(%rsp), %esi         # 4-byte Reload
	callq	hypre_StructGridSetHoodInfo
	testl	%ebx, %ebx
	jle	.LBB2_148
# BB#136:                               # %.lr.ph.preheader
	testb	$1, %bl
	jne	.LBB2_138
# BB#137:
	xorl	%eax, %eax
	cmpl	$1, %ebx
	jne	.LBB2_142
	jmp	.LBB2_148
.LBB2_138:                              # %.lr.ph.prol
	movl	188(%rsp), %eax
	testl	%eax, %eax
	jle	.LBB2_140
# BB#139:
	cltd
	movq	120(%rsp), %rcx         # 8-byte Reload
	idivl	(%rcx)
	movl	%eax, 188(%rsp)
.LBB2_140:                              # %.lr.ph.prol.loopexit
	movl	$1, %eax
	cmpl	$1, %ebx
	je	.LBB2_148
.LBB2_142:                              # %.lr.ph.preheader.new
	subq	%rax, %rbx
	leaq	192(%rsp,%rax,4), %rcx
	movq	120(%rsp), %rdx         # 8-byte Reload
	leaq	4(%rdx,%rax,4), %rsi
	.p2align	4, 0x90
.LBB2_143:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rcx), %eax
	testl	%eax, %eax
	jle	.LBB2_145
# BB#144:                               #   in Loop: Header=BB2_143 Depth=1
	cltd
	idivl	-4(%rsi)
	movl	%eax, -4(%rcx)
.LBB2_145:                              # %.lr.ph.1866
                                        #   in Loop: Header=BB2_143 Depth=1
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.LBB2_147
# BB#146:                               #   in Loop: Header=BB2_143 Depth=1
	cltd
	idivl	(%rsi)
	movl	%eax, (%rcx)
.LBB2_147:                              #   in Loop: Header=BB2_143 Depth=1
	addq	$8, %rcx
	addq	$8, %rsi
	addq	$-2, %rbx
	jne	.LBB2_143
.LBB2_148:                              # %._crit_edge
	movq	176(%rsp), %rdi
	leaq	188(%rsp), %rsi
	callq	hypre_StructGridSetPeriodic
	movq	176(%rsp), %rdi
	callq	hypre_StructGridAssemble
	movq	176(%rsp), %rax
	movq	288(%rsp), %rcx         # 8-byte Reload
	movq	%rax, (%rcx)
	xorl	%eax, %eax
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_23:                               # %vector.body832.preheader
	xorl	%edx, %edx
	movq	%rsi, %rdi
	.p2align	4, 0x90
.LBB2_24:                               # %vector.body832
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbx,%rdx,4), %xmm0
	movups	16(%rbx,%rdx,4), %xmm1
	movups	%xmm0, (%rdi,%rdx,4)
	movups	%xmm1, 16(%rdi,%rdx,4)
	addq	$8, %rdx
	cmpq	%rdx, %rax
	jne	.LBB2_24
# BB#25:                                # %middle.block833
	testl	%ecx, %ecx
	jne	.LBB2_26
	jmp	.LBB2_32
.Lfunc_end2:
	.size	hypre_StructCoarsen, .Lfunc_end2-hypre_StructCoarsen
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
