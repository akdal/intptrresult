	.text
	.file	"btCylinderShape.bc"
	.globl	_ZN15btCylinderShapeC2ERK9btVector3
	.p2align	4, 0x90
	.type	_ZN15btCylinderShapeC2ERK9btVector3,@function
_ZN15btCylinderShapeC2ERK9btVector3:    # @_ZN15btCylinderShapeC2ERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	_ZN21btConvexInternalShapeC2Ev
	movq	$_ZTV15btCylinderShape+16, (%rbx)
	movl	$1, 64(%rbx)
	movss	56(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movsd	(%r14), %xmm1           # xmm1 = mem[0],zero
	movsd	24(%rbx), %xmm2         # xmm2 = mem[0],zero
	mulps	%xmm1, %xmm2
	movss	8(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	32(%rbx), %xmm1
	subss	%xmm0, %xmm1
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	subps	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movlps	%xmm2, 40(%rbx)
	movlps	%xmm0, 48(%rbx)
	movl	$13, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	_ZN15btCylinderShapeC2ERK9btVector3, .Lfunc_end0-_ZN15btCylinderShapeC2ERK9btVector3
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN16btCylinderShapeXC2ERK9btVector3
	.p2align	4, 0x90
	.type	_ZN16btCylinderShapeXC2ERK9btVector3,@function
_ZN16btCylinderShapeXC2ERK9btVector3:   # @_ZN16btCylinderShapeXC2ERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	_ZN21btConvexInternalShapeC2Ev
	movss	56(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movsd	(%r14), %xmm1           # xmm1 = mem[0],zero
	movsd	24(%rbx), %xmm2         # xmm2 = mem[0],zero
	mulps	%xmm1, %xmm2
	movss	8(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	32(%rbx), %xmm1
	subss	%xmm0, %xmm1
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	subps	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movlps	%xmm2, 40(%rbx)
	movlps	%xmm0, 48(%rbx)
	movl	$13, 8(%rbx)
	movq	$_ZTV16btCylinderShapeX+16, (%rbx)
	movl	$0, 64(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	_ZN16btCylinderShapeXC2ERK9btVector3, .Lfunc_end2-_ZN16btCylinderShapeXC2ERK9btVector3
	.cfi_endproc

	.globl	_ZN16btCylinderShapeZC2ERK9btVector3
	.p2align	4, 0x90
	.type	_ZN16btCylinderShapeZC2ERK9btVector3,@function
_ZN16btCylinderShapeZC2ERK9btVector3:   # @_ZN16btCylinderShapeZC2ERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	_ZN21btConvexInternalShapeC2Ev
	movss	56(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movsd	(%r14), %xmm1           # xmm1 = mem[0],zero
	movsd	24(%rbx), %xmm2         # xmm2 = mem[0],zero
	mulps	%xmm1, %xmm2
	movss	8(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	32(%rbx), %xmm1
	subss	%xmm0, %xmm1
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	subps	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movlps	%xmm2, 40(%rbx)
	movlps	%xmm0, 48(%rbx)
	movl	$13, 8(%rbx)
	movq	$_ZTV16btCylinderShapeZ+16, (%rbx)
	movl	$2, 64(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	_ZN16btCylinderShapeZC2ERK9btVector3, .Lfunc_end3-_ZN16btCylinderShapeZC2ERK9btVector3
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.globl	_ZNK15btCylinderShape7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK15btCylinderShape7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK15btCylinderShape7getAabbERK11btTransformR9btVector3S4_: # @_ZNK15btCylinderShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 48
.Lcfi20:
	.cfi_offset %rbx, -40
.Lcfi21:
	.cfi_offset %r12, -32
.Lcfi22:
	.cfi_offset %r14, -24
.Lcfi23:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	(%r12), %rax
	callq	*88(%rax)
	movss	40(%r12), %xmm10        # xmm10 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm10
	movss	44(%r12), %xmm11        # xmm11 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm11
	addss	48(%r12), %xmm0
	movss	16(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	movaps	.LCPI4_0(%rip), %xmm2   # xmm2 = [nan,nan,nan,nan]
	andps	%xmm2, %xmm4
	movss	20(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	andps	%xmm2, %xmm3
	movss	24(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm6    # xmm6 = xmm6[0],xmm1[0],xmm6[1],xmm1[1]
	andps	%xmm2, %xmm6
	movss	32(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	andps	%xmm2, %xmm7
	movss	36(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	andps	%xmm2, %xmm1
	movss	40(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	andps	%xmm2, %xmm5
	movsd	48(%rbx), %xmm9         # xmm9 = mem[0],zero
	movss	56(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm7
	shufps	$224, %xmm10, %xmm10    # xmm10 = xmm10[0,0,2,3]
	mulps	%xmm4, %xmm10
	mulss	%xmm11, %xmm1
	shufps	$224, %xmm11, %xmm11    # xmm11 = xmm11[0,0,2,3]
	mulps	%xmm3, %xmm11
	addps	%xmm10, %xmm11
	mulss	%xmm0, %xmm5
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm6, %xmm0
	addps	%xmm11, %xmm0
	addss	%xmm7, %xmm1
	addss	%xmm1, %xmm5
	movaps	%xmm9, %xmm1
	subps	%xmm0, %xmm1
	movaps	%xmm8, %xmm2
	subss	%xmm5, %xmm2
	xorps	%xmm3, %xmm3
	xorps	%xmm4, %xmm4
	movss	%xmm2, %xmm4            # xmm4 = xmm2[0],xmm4[1,2,3]
	movlps	%xmm1, (%r15)
	movlps	%xmm4, 8(%r15)
	addps	%xmm9, %xmm0
	addss	%xmm8, %xmm5
	movss	%xmm5, %xmm3            # xmm3 = xmm5[0],xmm3[1,2,3]
	movlps	%xmm0, (%r14)
	movlps	%xmm3, 8(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	_ZNK15btCylinderShape7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end4-_ZNK15btCylinderShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	1094713344              # float 12
	.text
	.globl	_ZNK15btCylinderShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btCylinderShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK15btCylinderShape21calculateLocalInertiaEfR9btVector3: # @_ZNK15btCylinderShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi26:
	.cfi_def_cfa_offset 64
.Lcfi27:
	.cfi_offset %rbx, -24
.Lcfi28:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	%rdi, %rbx
	movsd	40(%rbx), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movss	48(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	callq	*88(%rax)
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movss	%xmm0, (%rsp)           # 4-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movss	4(%rsp), %xmm4          # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	addss	%xmm1, %xmm4
	pshufd	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	addss	(%rsp), %xmm1           # 4-byte Folded Reload
	addss	8(%rsp), %xmm0          # 4-byte Folded Reload
	addss	%xmm4, %xmm4
	addss	%xmm1, %xmm1
	addss	%xmm0, %xmm0
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	divss	.LCPI5_0(%rip), %xmm3
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	movaps	%xmm1, %xmm2
	addss	%xmm0, %xmm2
	mulss	%xmm3, %xmm2
	mulss	%xmm4, %xmm4
	addss	%xmm4, %xmm0
	mulss	%xmm3, %xmm0
	addss	%xmm1, %xmm4
	mulss	%xmm3, %xmm4
	movss	%xmm2, (%r14)
	movss	%xmm0, 4(%r14)
	movss	%xmm4, 8(%r14)
	movl	$0, 12(%r14)
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	_ZNK15btCylinderShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end5-_ZNK15btCylinderShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZNK16btCylinderShapeX37localGetSupportingVertexWithoutMarginERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK16btCylinderShapeX37localGetSupportingVertexWithoutMarginERK9btVector3,@function
_ZNK16btCylinderShapeX37localGetSupportingVertexWithoutMarginERK9btVector3: # @_ZNK16btCylinderShapeX37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 48
.Lcfi31:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movss	40(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	44(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB6_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	movaps	%xmm4, (%rsp)           # 16-byte Spill
	callq	sqrtf
	movaps	(%rsp), %xmm4           # 16-byte Reload
	movaps	16(%rsp), %xmm3         # 16-byte Reload
.LBB6_2:                                # %.split
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB6_3
	jnp	.LBB6_4
.LBB6_3:
	divss	%xmm0, %xmm4
	movss	4(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	cmpltss	%xmm2, %xmm1
	movaps	%xmm1, %xmm2
	andnps	%xmm3, %xmm2
	xorps	.LCPI6_0(%rip), %xmm3
	andps	%xmm1, %xmm3
	orps	%xmm2, %xmm3
	movaps	%xmm4, %xmm1
	mulss	8(%rbx), %xmm1
	movaps	%xmm0, %xmm4
	jmp	.LBB6_5
.LBB6_4:
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	cmpltss	%xmm1, %xmm0
	movaps	%xmm0, %xmm2
	andnps	%xmm3, %xmm2
	xorps	.LCPI6_0(%rip), %xmm3
	andps	%xmm0, %xmm3
	orps	%xmm2, %xmm3
.LBB6_5:                                # %_Z21CylinderLocalSupportXRK9btVector3S1_.exit
	unpcklps	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	movaps	%xmm3, %xmm0
	addq	$32, %rsp
	popq	%rbx
	retq
.Lfunc_end6:
	.size	_ZNK16btCylinderShapeX37localGetSupportingVertexWithoutMarginERK9btVector3, .Lfunc_end6-_ZNK16btCylinderShapeX37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI7_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZNK16btCylinderShapeZ37localGetSupportingVertexWithoutMarginERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK16btCylinderShapeZ37localGetSupportingVertexWithoutMarginERK9btVector3,@function
_ZNK16btCylinderShapeZ37localGetSupportingVertexWithoutMarginERK9btVector3: # @_ZNK16btCylinderShapeZ37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 48
.Lcfi34:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movss	40(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	48(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	sqrtss	%xmm2, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB7_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm2, %xmm0
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	movaps	%xmm4, (%rsp)           # 16-byte Spill
	callq	sqrtf
	movaps	(%rsp), %xmm4           # 16-byte Reload
	movaps	16(%rsp), %xmm1         # 16-byte Reload
.LBB7_2:                                # %.split
	movss	8(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm0
	jne	.LBB7_3
	jnp	.LBB7_4
.LBB7_3:
	divss	%xmm0, %xmm4
	cmpltss	%xmm2, %xmm3
	movaps	%xmm3, %xmm0
	andnps	%xmm1, %xmm0
	xorps	.LCPI7_0(%rip), %xmm1
	andps	%xmm3, %xmm1
	orps	%xmm0, %xmm1
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm0, %xmm4
	jmp	.LBB7_5
.LBB7_4:
	cmpltss	%xmm2, %xmm3
	movaps	%xmm3, %xmm0
	andnps	%xmm1, %xmm0
	xorps	.LCPI7_0(%rip), %xmm1
	andps	%xmm3, %xmm1
	orps	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movaps	%xmm0, %xmm4
.LBB7_5:                                # %_Z21CylinderLocalSupportZRK9btVector3S1_.exit
	movaps	%xmm4, %xmm0
	addq	$32, %rsp
	popq	%rbx
	retq
.Lfunc_end7:
	.size	_ZNK16btCylinderShapeZ37localGetSupportingVertexWithoutMarginERK9btVector3, .Lfunc_end7-_ZNK16btCylinderShapeZ37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZNK15btCylinderShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btCylinderShape37localGetSupportingVertexWithoutMarginERK9btVector3,@function
_ZNK15btCylinderShape37localGetSupportingVertexWithoutMarginERK9btVector3: # @_ZNK15btCylinderShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi36:
	.cfi_def_cfa_offset 48
.Lcfi37:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movss	40(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	44(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB8_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	movaps	%xmm4, (%rsp)           # 16-byte Spill
	callq	sqrtf
	movaps	(%rsp), %xmm4           # 16-byte Reload
	movaps	16(%rsp), %xmm3         # 16-byte Reload
.LBB8_2:                                # %.split
	movss	4(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB8_3
	jnp	.LBB8_4
.LBB8_3:
	divss	%xmm0, %xmm3
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	xorps	%xmm1, %xmm1
	cmpltss	%xmm1, %xmm2
	movaps	%xmm2, %xmm1
	andnps	%xmm4, %xmm1
	xorps	.LCPI8_0(%rip), %xmm4
	andps	%xmm2, %xmm4
	orps	%xmm1, %xmm4
	movaps	%xmm3, %xmm1
	mulss	8(%rbx), %xmm1
	movaps	%xmm0, %xmm3
	jmp	.LBB8_5
.LBB8_4:
	cmpltss	%xmm1, %xmm2
	movaps	%xmm2, %xmm0
	andnps	%xmm4, %xmm0
	xorps	.LCPI8_0(%rip), %xmm4
	andps	%xmm2, %xmm4
	orps	%xmm0, %xmm4
.LBB8_5:                                # %_Z21CylinderLocalSupportYRK9btVector3S1_.exit
	unpcklps	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	movaps	%xmm3, %xmm0
	addq	$32, %rsp
	popq	%rbx
	retq
.Lfunc_end8:
	.size	_ZNK15btCylinderShape37localGetSupportingVertexWithoutMarginERK9btVector3, .Lfunc_end8-_ZNK15btCylinderShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZNK15btCylinderShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.p2align	4, 0x90
	.type	_ZNK15btCylinderShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,@function
_ZNK15btCylinderShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i: # @_ZNK15btCylinderShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 40
	subq	$56, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 96
.Lcfi43:
	.cfi_offset %rbx, -40
.Lcfi44:
	.cfi_offset %r12, -32
.Lcfi45:
	.cfi_offset %r14, -24
.Lcfi46:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testl	%ecx, %ecx
	jle	.LBB9_8
# BB#1:                                 # %.lr.ph
	movl	%ecx, %r12d
	addq	$8, %r14
	addq	$8, %rbx
	xorps	%xmm3, %xmm3
	movaps	.LCPI9_0(%rip), %xmm4   # xmm4 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
                                        # implicit-def: %XMM5
	.p2align	4, 0x90
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	movss	40(%r15), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	44(%r15), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	-8(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB9_4
# BB#3:                                 # %call.sqrt
                                        #   in Loop: Header=BB9_2 Depth=1
	movaps	%xmm1, %xmm0
	movaps	%xmm5, 32(%rsp)         # 16-byte Spill
	movaps	%xmm6, 16(%rsp)         # 16-byte Spill
	movaps	%xmm7, (%rsp)           # 16-byte Spill
	callq	sqrtf
	movaps	(%rsp), %xmm7           # 16-byte Reload
	movaps	16(%rsp), %xmm6         # 16-byte Reload
	movaps	32(%rsp), %xmm5         # 16-byte Reload
	movaps	.LCPI9_0(%rip), %xmm4   # xmm4 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm3, %xmm3
.LBB9_4:                                # %.split
                                        #   in Loop: Header=BB9_2 Depth=1
	movss	-4(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm0
	jne	.LBB9_5
	jnp	.LBB9_6
.LBB9_5:                                #   in Loop: Header=BB9_2 Depth=1
	divss	%xmm0, %xmm6
	movss	-8(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	cmpltss	%xmm3, %xmm1
	movaps	%xmm1, %xmm0
	andnps	%xmm7, %xmm0
	xorps	%xmm4, %xmm7
	andps	%xmm1, %xmm7
	orps	%xmm0, %xmm7
	movaps	%xmm6, %xmm0
	mulss	(%rbx), %xmm0
	movaps	%xmm2, %xmm6
	jmp	.LBB9_7
	.p2align	4, 0x90
.LBB9_6:                                #   in Loop: Header=BB9_2 Depth=1
	xorps	%xmm0, %xmm0
	cmpltss	%xmm0, %xmm1
	movaps	%xmm1, %xmm2
	andnps	%xmm7, %xmm2
	xorps	%xmm4, %xmm7
	andps	%xmm1, %xmm7
	orps	%xmm2, %xmm7
.LBB9_7:                                # %_Z21CylinderLocalSupportYRK9btVector3S1_.exit
                                        #   in Loop: Header=BB9_2 Depth=1
	unpcklps	%xmm7, %xmm6    # xmm6 = xmm6[0],xmm7[0],xmm6[1],xmm7[1]
	movss	%xmm0, %xmm5            # xmm5 = xmm0[0],xmm5[1,2,3]
	movlps	%xmm6, -8(%r14)
	movlps	%xmm5, (%r14)
	addq	$16, %r14
	addq	$16, %rbx
	decq	%r12
	jne	.LBB9_2
.LBB9_8:                                # %._crit_edge
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	_ZNK15btCylinderShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i, .Lfunc_end9-_ZNK15btCylinderShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI10_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZNK16btCylinderShapeZ49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.p2align	4, 0x90
	.type	_ZNK16btCylinderShapeZ49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,@function
_ZNK16btCylinderShapeZ49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i: # @_ZNK16btCylinderShapeZ49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 40
	subq	$56, %rsp
.Lcfi51:
	.cfi_def_cfa_offset 96
.Lcfi52:
	.cfi_offset %rbx, -40
.Lcfi53:
	.cfi_offset %r12, -32
.Lcfi54:
	.cfi_offset %r14, -24
.Lcfi55:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testl	%ecx, %ecx
	jle	.LBB10_8
# BB#1:                                 # %.lr.ph
	movl	%ecx, %r12d
	addq	$8, %r14
	xorps	%xmm2, %xmm2
	movaps	.LCPI10_0(%rip), %xmm3  # xmm3 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
                                        # implicit-def: %XMM4
	.p2align	4, 0x90
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	movss	40(%r15), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	48(%r15), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB10_4
# BB#3:                                 # %call.sqrt
                                        #   in Loop: Header=BB10_2 Depth=1
	movaps	%xmm1, %xmm0
	movaps	%xmm4, 32(%rsp)         # 16-byte Spill
	movaps	%xmm5, 16(%rsp)         # 16-byte Spill
	movaps	%xmm6, (%rsp)           # 16-byte Spill
	callq	sqrtf
	movaps	(%rsp), %xmm6           # 16-byte Reload
	movaps	16(%rsp), %xmm5         # 16-byte Reload
	movaps	32(%rsp), %xmm4         # 16-byte Reload
	movaps	.LCPI10_0(%rip), %xmm3  # xmm3 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm2, %xmm2
.LBB10_4:                               # %.split
                                        #   in Loop: Header=BB10_2 Depth=1
	movss	8(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	jne	.LBB10_5
	jnp	.LBB10_6
.LBB10_5:                               #   in Loop: Header=BB10_2 Depth=1
	divss	%xmm0, %xmm5
	cmpltss	%xmm2, %xmm1
	movaps	%xmm1, %xmm0
	andnps	%xmm6, %xmm0
	xorps	%xmm3, %xmm6
	andps	%xmm1, %xmm6
	orps	%xmm0, %xmm6
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm0, %xmm5
	jmp	.LBB10_7
	.p2align	4, 0x90
.LBB10_6:                               #   in Loop: Header=BB10_2 Depth=1
	cmpltss	%xmm2, %xmm1
	movaps	%xmm1, %xmm0
	andnps	%xmm6, %xmm0
	xorps	%xmm3, %xmm6
	andps	%xmm1, %xmm6
	orps	%xmm0, %xmm6
	xorps	%xmm0, %xmm0
	movss	%xmm5, %xmm0            # xmm0 = xmm5[0],xmm0[1,2,3]
	movaps	%xmm0, %xmm5
.LBB10_7:                               # %_Z21CylinderLocalSupportZRK9btVector3S1_.exit
                                        #   in Loop: Header=BB10_2 Depth=1
	movss	%xmm6, %xmm4            # xmm4 = xmm6[0],xmm4[1,2,3]
	movlps	%xmm5, -8(%r14)
	movlps	%xmm4, (%r14)
	addq	$16, %r14
	addq	$16, %rbx
	decq	%r12
	jne	.LBB10_2
.LBB10_8:                               # %._crit_edge
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end10:
	.size	_ZNK16btCylinderShapeZ49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i, .Lfunc_end10-_ZNK16btCylinderShapeZ49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZNK16btCylinderShapeX49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.p2align	4, 0x90
	.type	_ZNK16btCylinderShapeX49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,@function
_ZNK16btCylinderShapeX49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i: # @_ZNK16btCylinderShapeX49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 40
	subq	$56, %rsp
.Lcfi60:
	.cfi_def_cfa_offset 96
.Lcfi61:
	.cfi_offset %rbx, -40
.Lcfi62:
	.cfi_offset %r12, -32
.Lcfi63:
	.cfi_offset %r14, -24
.Lcfi64:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testl	%ecx, %ecx
	jle	.LBB11_8
# BB#1:                                 # %.lr.ph
	movl	%ecx, %r12d
	addq	$8, %r14
	addq	$8, %rbx
	xorps	%xmm3, %xmm3
	movaps	.LCPI11_0(%rip), %xmm4  # xmm4 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
                                        # implicit-def: %XMM5
	.p2align	4, 0x90
.LBB11_2:                               # =>This Inner Loop Header: Depth=1
	movss	40(%r15), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	44(%r15), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	-4(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB11_4
# BB#3:                                 # %call.sqrt
                                        #   in Loop: Header=BB11_2 Depth=1
	movaps	%xmm1, %xmm0
	movaps	%xmm5, 32(%rsp)         # 16-byte Spill
	movaps	%xmm6, 16(%rsp)         # 16-byte Spill
	movaps	%xmm7, (%rsp)           # 16-byte Spill
	callq	sqrtf
	movaps	(%rsp), %xmm7           # 16-byte Reload
	movaps	16(%rsp), %xmm6         # 16-byte Reload
	movaps	32(%rsp), %xmm5         # 16-byte Reload
	movaps	.LCPI11_0(%rip), %xmm4  # xmm4 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm3, %xmm3
.LBB11_4:                               # %.split
                                        #   in Loop: Header=BB11_2 Depth=1
	ucomiss	%xmm3, %xmm0
	jne	.LBB11_5
	jnp	.LBB11_6
.LBB11_5:                               #   in Loop: Header=BB11_2 Depth=1
	divss	%xmm0, %xmm7
	movss	-4(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm1
	movss	-8(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cmpltss	%xmm3, %xmm0
	movaps	%xmm0, %xmm2
	andnps	%xmm6, %xmm2
	xorps	%xmm4, %xmm6
	andps	%xmm0, %xmm6
	orps	%xmm2, %xmm6
	movaps	%xmm7, %xmm0
	mulss	(%rbx), %xmm0
	movaps	%xmm1, %xmm7
	jmp	.LBB11_7
	.p2align	4, 0x90
.LBB11_6:                               #   in Loop: Header=BB11_2 Depth=1
	movss	-8(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	cmpltss	%xmm0, %xmm1
	movaps	%xmm1, %xmm2
	andnps	%xmm6, %xmm2
	xorps	%xmm4, %xmm6
	andps	%xmm1, %xmm6
	orps	%xmm2, %xmm6
.LBB11_7:                               # %_Z21CylinderLocalSupportXRK9btVector3S1_.exit
                                        #   in Loop: Header=BB11_2 Depth=1
	unpcklps	%xmm7, %xmm6    # xmm6 = xmm6[0],xmm7[0],xmm6[1],xmm7[1]
	movss	%xmm0, %xmm5            # xmm5 = xmm0[0],xmm5[1,2,3]
	movlps	%xmm6, -8(%r14)
	movlps	%xmm5, (%r14)
	addq	$16, %r14
	addq	$16, %rbx
	decq	%r12
	jne	.LBB11_2
.LBB11_8:                               # %._crit_edge
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	_ZNK16btCylinderShapeX49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i, .Lfunc_end11-_ZNK16btCylinderShapeX49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_endproc

	.section	.text._ZN15btCylinderShapeD0Ev,"axG",@progbits,_ZN15btCylinderShapeD0Ev,comdat
	.weak	_ZN15btCylinderShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN15btCylinderShapeD0Ev,@function
_ZN15btCylinderShapeD0Ev:               # @_ZN15btCylinderShapeD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi67:
	.cfi_def_cfa_offset 32
.Lcfi68:
	.cfi_offset %rbx, -24
.Lcfi69:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp0:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp1:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB12_2:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB12_4:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZN15btCylinderShapeD0Ev, .Lfunc_end12-_ZN15btCylinderShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end12-.Ltmp4     #   Call between .Ltmp4 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK21btConvexInternalShape15getLocalScalingEv,"axG",@progbits,_ZNK21btConvexInternalShape15getLocalScalingEv,comdat
	.weak	_ZNK21btConvexInternalShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape15getLocalScalingEv,@function
_ZNK21btConvexInternalShape15getLocalScalingEv: # @_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	24(%rdi), %rax
	retq
.Lfunc_end13:
	.size	_ZNK21btConvexInternalShape15getLocalScalingEv, .Lfunc_end13-_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_endproc

	.section	.text._ZNK15btCylinderShape7getNameEv,"axG",@progbits,_ZNK15btCylinderShape7getNameEv,comdat
	.weak	_ZNK15btCylinderShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK15btCylinderShape7getNameEv,@function
_ZNK15btCylinderShape7getNameEv:        # @_ZNK15btCylinderShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end14:
	.size	_ZNK15btCylinderShape7getNameEv, .Lfunc_end14-_ZNK15btCylinderShape7getNameEv
	.cfi_endproc

	.section	.text._ZN15btCylinderShape9setMarginEf,"axG",@progbits,_ZN15btCylinderShape9setMarginEf,comdat
	.weak	_ZN15btCylinderShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN15btCylinderShape9setMarginEf,@function
_ZN15btCylinderShape9setMarginEf:       # @_ZN15btCylinderShape9setMarginEf
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 16
	subq	$64, %rsp
.Lcfi71:
	.cfi_def_cfa_offset 80
.Lcfi72:
	.cfi_offset %rbx, -16
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	%xmm0, (%rsp)           # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movsd	40(%rbx), %xmm2         # xmm2 = mem[0],zero
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	unpcklps	(%rsp), %xmm1   # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	addps	%xmm2, %xmm1
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	addss	48(%rbx), %xmm0
	movss	%xmm0, (%rsp)           # 4-byte Spill
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 56(%rbx)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	unpcklps	48(%rsp), %xmm1 # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	movaps	32(%rsp), %xmm2         # 16-byte Reload
	subps	%xmm1, %xmm2
	movss	(%rsp), %xmm1           # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movlps	%xmm2, 40(%rbx)
	movlps	%xmm0, 48(%rbx)
	addq	$64, %rsp
	popq	%rbx
	retq
.Lfunc_end15:
	.size	_ZN15btCylinderShape9setMarginEf, .Lfunc_end15-_ZN15btCylinderShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape9getMarginEv,"axG",@progbits,_ZNK21btConvexInternalShape9getMarginEv,comdat
	.weak	_ZNK21btConvexInternalShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape9getMarginEv,@function
_ZNK21btConvexInternalShape9getMarginEv: # @_ZNK21btConvexInternalShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	56(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end16:
	.size	_ZNK21btConvexInternalShape9getMarginEv, .Lfunc_end16-_ZNK21btConvexInternalShape9getMarginEv
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI17_0:
	.long	679477248               # float 1.42108547E-14
.LCPI17_2:
	.long	3212836864              # float -1
.LCPI17_3:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI17_1:
	.long	3212836864              # float -1
	.long	3212836864              # float -1
	.zero	4
	.zero	4
	.section	.text._ZNK15btCylinderShape24localGetSupportingVertexERK9btVector3,"axG",@progbits,_ZNK15btCylinderShape24localGetSupportingVertexERK9btVector3,comdat
	.weak	_ZNK15btCylinderShape24localGetSupportingVertexERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btCylinderShape24localGetSupportingVertexERK9btVector3,@function
_ZNK15btCylinderShape24localGetSupportingVertexERK9btVector3: # @_ZNK15btCylinderShape24localGetSupportingVertexERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 24
	subq	$72, %rsp
.Lcfi75:
	.cfi_def_cfa_offset 96
.Lcfi76:
	.cfi_offset %rbx, -24
.Lcfi77:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%r14), %rax
	callq	*104(%rax)
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*88(%rax)
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB17_2
	jnp	.LBB17_1
.LBB17_2:
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movss	8(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	mulss	%xmm1, %xmm1
	pshufd	$229, %xmm0, %xmm2      # xmm2 = xmm0[1,1,2,3]
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	movss	.LCPI17_0(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	xorl	%eax, %eax
	ucomiss	%xmm1, %xmm2
	seta	%al
	movd	%eax, %xmm1
	pshufd	$80, %xmm1, %xmm2       # xmm2 = xmm1[0,0,1,1]
	pslld	$31, %xmm2
	psrad	$31, %xmm2
	movdqa	%xmm2, %xmm1
	pandn	%xmm0, %xmm1
	pand	.LCPI17_1(%rip), %xmm2
	por	%xmm1, %xmm2
	pshufd	$229, %xmm2, %xmm0      # xmm0 = xmm2[1,1,2,3]
	movdqa	%xmm2, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	jbe	.LBB17_4
# BB#3:
	movss	.LCPI17_2(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
.LBB17_4:
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB17_6
# BB#5:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	movdqa	%xmm2, (%rsp)           # 16-byte Spill
	callq	sqrtf
	movdqa	(%rsp), %xmm2           # 16-byte Reload
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
.LBB17_6:                               # %.split
	movss	.LCPI17_3(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	mulss	%xmm3, %xmm1
	movaps	%xmm1, (%rsp)           # 16-byte Spill
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*88(%rax)
	movaps	(%rsp), %xmm2           # 16-byte Reload
	mulss	%xmm0, %xmm2
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	16(%rsp), %xmm0         # 16-byte Folded Reload
	movaps	48(%rsp), %xmm1         # 16-byte Reload
	addps	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	addss	%xmm2, %xmm1
	jmp	.LBB17_7
.LBB17_1:
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	movaps	32(%rsp), %xmm1         # 16-byte Reload
.LBB17_7:
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end17:
	.size	_ZNK15btCylinderShape24localGetSupportingVertexERK9btVector3, .Lfunc_end17-_ZNK15btCylinderShape24localGetSupportingVertexERK9btVector3
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,"axG",@progbits,_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,comdat
	.weak	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,@function
_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv: # @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end18:
	.size	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv, .Lfunc_end18-_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,"axG",@progbits,_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,comdat
	.weak	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,@function
_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3: # @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end19:
	.size	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3, .Lfunc_end19-_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_endproc

	.section	.text._ZNK15btCylinderShape9getRadiusEv,"axG",@progbits,_ZNK15btCylinderShape9getRadiusEv,comdat
	.weak	_ZNK15btCylinderShape9getRadiusEv
	.p2align	4, 0x90
	.type	_ZNK15btCylinderShape9getRadiusEv,@function
_ZNK15btCylinderShape9getRadiusEv:      # @_ZNK15btCylinderShape9getRadiusEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi79:
	.cfi_def_cfa_offset 32
.Lcfi80:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	movss	40(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	callq	*88(%rax)
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addss	12(%rsp), %xmm0         # 4-byte Folded Reload
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end20:
	.size	_ZNK15btCylinderShape9getRadiusEv, .Lfunc_end20-_ZNK15btCylinderShape9getRadiusEv
	.cfi_endproc

	.section	.text._ZN16btCylinderShapeXD0Ev,"axG",@progbits,_ZN16btCylinderShapeXD0Ev,comdat
	.weak	_ZN16btCylinderShapeXD0Ev
	.p2align	4, 0x90
	.type	_ZN16btCylinderShapeXD0Ev,@function
_ZN16btCylinderShapeXD0Ev:              # @_ZN16btCylinderShapeXD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi83:
	.cfi_def_cfa_offset 32
.Lcfi84:
	.cfi_offset %rbx, -24
.Lcfi85:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp6:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp7:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB21_2:
.Ltmp8:
	movq	%rax, %r14
.Ltmp9:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp10:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB21_4:
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end21:
	.size	_ZN16btCylinderShapeXD0Ev, .Lfunc_end21-_ZN16btCylinderShapeXD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp9-.Ltmp7           #   Call between .Ltmp7 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end21-.Ltmp10    #   Call between .Ltmp10 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK16btCylinderShapeX7getNameEv,"axG",@progbits,_ZNK16btCylinderShapeX7getNameEv,comdat
	.weak	_ZNK16btCylinderShapeX7getNameEv
	.p2align	4, 0x90
	.type	_ZNK16btCylinderShapeX7getNameEv,@function
_ZNK16btCylinderShapeX7getNameEv:       # @_ZNK16btCylinderShapeX7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str.1, %eax
	retq
.Lfunc_end22:
	.size	_ZNK16btCylinderShapeX7getNameEv, .Lfunc_end22-_ZNK16btCylinderShapeX7getNameEv
	.cfi_endproc

	.section	.text._ZNK16btCylinderShapeX9getRadiusEv,"axG",@progbits,_ZNK16btCylinderShapeX9getRadiusEv,comdat
	.weak	_ZNK16btCylinderShapeX9getRadiusEv
	.p2align	4, 0x90
	.type	_ZNK16btCylinderShapeX9getRadiusEv,@function
_ZNK16btCylinderShapeX9getRadiusEv:     # @_ZNK16btCylinderShapeX9getRadiusEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi87:
	.cfi_def_cfa_offset 48
.Lcfi88:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movsd	40(%rbx), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	callq	*88(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	pshufd	$229, 16(%rsp), %xmm1   # 16-byte Folded Reload
                                        # xmm1 = mem[1,1,2,3]
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	addq	$32, %rsp
	popq	%rbx
	retq
.Lfunc_end23:
	.size	_ZNK16btCylinderShapeX9getRadiusEv, .Lfunc_end23-_ZNK16btCylinderShapeX9getRadiusEv
	.cfi_endproc

	.section	.text._ZN16btCylinderShapeZD0Ev,"axG",@progbits,_ZN16btCylinderShapeZD0Ev,comdat
	.weak	_ZN16btCylinderShapeZD0Ev
	.p2align	4, 0x90
	.type	_ZN16btCylinderShapeZD0Ev,@function
_ZN16btCylinderShapeZD0Ev:              # @_ZN16btCylinderShapeZD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi91:
	.cfi_def_cfa_offset 32
.Lcfi92:
	.cfi_offset %rbx, -24
.Lcfi93:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp12:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp13:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB24_2:
.Ltmp14:
	movq	%rax, %r14
.Ltmp15:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp16:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB24_4:
.Ltmp17:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end24:
	.size	_ZN16btCylinderShapeZD0Ev, .Lfunc_end24-_ZN16btCylinderShapeZD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table24:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp12-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin2   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp15-.Ltmp13         #   Call between .Ltmp13 and .Ltmp15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin2   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end24-.Ltmp16    #   Call between .Ltmp16 and .Lfunc_end24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK16btCylinderShapeZ7getNameEv,"axG",@progbits,_ZNK16btCylinderShapeZ7getNameEv,comdat
	.weak	_ZNK16btCylinderShapeZ7getNameEv
	.p2align	4, 0x90
	.type	_ZNK16btCylinderShapeZ7getNameEv,@function
_ZNK16btCylinderShapeZ7getNameEv:       # @_ZNK16btCylinderShapeZ7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str.2, %eax
	retq
.Lfunc_end25:
	.size	_ZNK16btCylinderShapeZ7getNameEv, .Lfunc_end25-_ZNK16btCylinderShapeZ7getNameEv
	.cfi_endproc

	.section	.text._ZNK16btCylinderShapeZ9getRadiusEv,"axG",@progbits,_ZNK16btCylinderShapeZ9getRadiusEv,comdat
	.weak	_ZNK16btCylinderShapeZ9getRadiusEv
	.p2align	4, 0x90
	.type	_ZNK16btCylinderShapeZ9getRadiusEv,@function
_ZNK16btCylinderShapeZ9getRadiusEv:     # @_ZNK16btCylinderShapeZ9getRadiusEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi95:
	.cfi_def_cfa_offset 32
.Lcfi96:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	movss	40(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	callq	*88(%rax)
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addss	12(%rsp), %xmm0         # 4-byte Folded Reload
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end26:
	.size	_ZNK16btCylinderShapeZ9getRadiusEv, .Lfunc_end26-_ZNK16btCylinderShapeZ9getRadiusEv
	.cfi_endproc

	.section	.text._ZNK16btCylinderShapeZ9getUpAxisEv,"axG",@progbits,_ZNK16btCylinderShapeZ9getUpAxisEv,comdat
	.weak	_ZNK16btCylinderShapeZ9getUpAxisEv
	.p2align	4, 0x90
	.type	_ZNK16btCylinderShapeZ9getUpAxisEv,@function
_ZNK16btCylinderShapeZ9getUpAxisEv:     # @_ZNK16btCylinderShapeZ9getUpAxisEv
	.cfi_startproc
# BB#0:
	movl	$2, %eax
	retq
.Lfunc_end27:
	.size	_ZNK16btCylinderShapeZ9getUpAxisEv, .Lfunc_end27-_ZNK16btCylinderShapeZ9getUpAxisEv
	.cfi_endproc

	.type	_ZTV15btCylinderShape,@object # @_ZTV15btCylinderShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV15btCylinderShape
	.p2align	3
_ZTV15btCylinderShape:
	.quad	0
	.quad	_ZTI15btCylinderShape
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN15btCylinderShapeD0Ev
	.quad	_ZNK15btCylinderShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN21btConvexInternalShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK15btCylinderShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK15btCylinderShape7getNameEv
	.quad	_ZN15btCylinderShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK15btCylinderShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK15btCylinderShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK15btCylinderShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.quad	_ZNK15btCylinderShape9getRadiusEv
	.size	_ZTV15btCylinderShape, 168

	.type	_ZTV16btCylinderShapeX,@object # @_ZTV16btCylinderShapeX
	.globl	_ZTV16btCylinderShapeX
	.p2align	3
_ZTV16btCylinderShapeX:
	.quad	0
	.quad	_ZTI16btCylinderShapeX
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN16btCylinderShapeXD0Ev
	.quad	_ZNK15btCylinderShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN21btConvexInternalShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK15btCylinderShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK16btCylinderShapeX7getNameEv
	.quad	_ZN15btCylinderShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK15btCylinderShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK16btCylinderShapeX37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK16btCylinderShapeX49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.quad	_ZNK16btCylinderShapeX9getRadiusEv
	.size	_ZTV16btCylinderShapeX, 168

	.type	_ZTV16btCylinderShapeZ,@object # @_ZTV16btCylinderShapeZ
	.globl	_ZTV16btCylinderShapeZ
	.p2align	3
_ZTV16btCylinderShapeZ:
	.quad	0
	.quad	_ZTI16btCylinderShapeZ
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN16btCylinderShapeZD0Ev
	.quad	_ZNK15btCylinderShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN21btConvexInternalShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK15btCylinderShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK16btCylinderShapeZ7getNameEv
	.quad	_ZN15btCylinderShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK15btCylinderShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK16btCylinderShapeZ37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK16btCylinderShapeZ49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.quad	_ZNK16btCylinderShapeZ9getRadiusEv
	.quad	_ZNK16btCylinderShapeZ9getUpAxisEv
	.size	_ZTV16btCylinderShapeZ, 176

	.type	_ZTS15btCylinderShape,@object # @_ZTS15btCylinderShape
	.globl	_ZTS15btCylinderShape
	.p2align	4
_ZTS15btCylinderShape:
	.asciz	"15btCylinderShape"
	.size	_ZTS15btCylinderShape, 18

	.type	_ZTI15btCylinderShape,@object # @_ZTI15btCylinderShape
	.globl	_ZTI15btCylinderShape
	.p2align	4
_ZTI15btCylinderShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15btCylinderShape
	.quad	_ZTI21btConvexInternalShape
	.size	_ZTI15btCylinderShape, 24

	.type	_ZTS16btCylinderShapeX,@object # @_ZTS16btCylinderShapeX
	.globl	_ZTS16btCylinderShapeX
	.p2align	4
_ZTS16btCylinderShapeX:
	.asciz	"16btCylinderShapeX"
	.size	_ZTS16btCylinderShapeX, 19

	.type	_ZTI16btCylinderShapeX,@object # @_ZTI16btCylinderShapeX
	.globl	_ZTI16btCylinderShapeX
	.p2align	4
_ZTI16btCylinderShapeX:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS16btCylinderShapeX
	.quad	_ZTI15btCylinderShape
	.size	_ZTI16btCylinderShapeX, 24

	.type	_ZTS16btCylinderShapeZ,@object # @_ZTS16btCylinderShapeZ
	.globl	_ZTS16btCylinderShapeZ
	.p2align	4
_ZTS16btCylinderShapeZ:
	.asciz	"16btCylinderShapeZ"
	.size	_ZTS16btCylinderShapeZ, 19

	.type	_ZTI16btCylinderShapeZ,@object # @_ZTI16btCylinderShapeZ
	.globl	_ZTI16btCylinderShapeZ
	.p2align	4
_ZTI16btCylinderShapeZ:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS16btCylinderShapeZ
	.quad	_ZTI15btCylinderShape
	.size	_ZTI16btCylinderShapeZ, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"CylinderY"
	.size	.L.str, 10

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"CylinderX"
	.size	.L.str.1, 10

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"CylinderZ"
	.size	.L.str.2, 10


	.globl	_ZN15btCylinderShapeC1ERK9btVector3
	.type	_ZN15btCylinderShapeC1ERK9btVector3,@function
_ZN15btCylinderShapeC1ERK9btVector3 = _ZN15btCylinderShapeC2ERK9btVector3
	.globl	_ZN16btCylinderShapeXC1ERK9btVector3
	.type	_ZN16btCylinderShapeXC1ERK9btVector3,@function
_ZN16btCylinderShapeXC1ERK9btVector3 = _ZN16btCylinderShapeXC2ERK9btVector3
	.globl	_ZN16btCylinderShapeZC1ERK9btVector3
	.type	_ZN16btCylinderShapeZC1ERK9btVector3,@function
_ZN16btCylinderShapeZC1ERK9btVector3 = _ZN16btCylinderShapeZC2ERK9btVector3
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
