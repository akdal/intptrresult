	.text
	.file	"d1-pushc.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	4                       # 0x4
	.long	39                      # 0x27
	.text
	.globl	_Z7pushimmP9Classfile
	.p2align	4, 0x90
	.type	_Z7pushimmP9Classfile,@function
_Z7pushimmP9Classfile:                  # @_Z7pushimmP9Classfile
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movl	currpc(%rip), %r12d
	leal	1(%r12), %eax
	movl	%eax, currpc(%rip)
	movl	bufflength(%rip), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, bufflength(%rip)
	movq	inbuff(%rip), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, inbuff(%rip)
	movsbl	(%rcx), %ebx
	cmpl	$17, ch(%rip)
	jne	.LBB0_2
# BB#1:
	shll	$8, %ebx
	leal	2(%r12), %edx
	movl	%edx, currpc(%rip)
	addl	$-2, %eax
	movl	%eax, bufflength(%rip)
	leaq	2(%rcx), %rax
	movq	%rax, inbuff(%rip)
	movzbl	1(%rcx), %eax
	orl	%eax, %ebx
.LBB0_2:
	movl	$32, %edi
	callq	_Znam
	movq	%rax, %r14
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%ebx, %edx
	callq	sprintf
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp0:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp1:
# BB#3:                                 # %.noexc
	decl	%r12d
	movq	%r14, (%rbx)
	movl	$4, 8(%rbx)
	movl	$1, 12(%rbx)
	movl	$0, 16(%rbx)
	movl	$1, 8(%r15)
	movl	%r12d, 16(%r15)
	movl	%r12d, 12(%r15)
.Ltmp2:
	movl	$24, %edi
	callq	_Znwm
.Ltmp3:
# BB#4:
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,1,4,39]
	movups	%xmm0, (%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, (%r15)
	movq	stkptr(%rip), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, stkptr(%rip)
	movq	%r15, (%rax)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB0_5:
.Ltmp4:
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z7pushimmP9Classfile, .Lfunc_end0-_Z7pushimmP9Classfile
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp3      #   Call between .Ltmp3 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z9pushconstP9Classfile
	.p2align	4, 0x90
	.type	_Z9pushconstP9Classfile,@function
_Z9pushconstP9Classfile:                # @_Z9pushconstP9Classfile
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$1032, %rsp             # imm = 0x408
.Lcfi15:
	.cfi_def_cfa_offset 1088
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	currpc(%rip), %r12d
	leal	1(%r12), %eax
	movl	%eax, currpc(%rip)
	movl	bufflength(%rip), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, bufflength(%rip)
	movq	inbuff(%rip), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, inbuff(%rip)
	movzbl	(%rcx), %r13d
	cmpl	$18, ch(%rip)
	je	.LBB1_2
# BB#1:
	shll	$8, %r13d
	leal	2(%r12), %edx
	movl	%edx, currpc(%rip)
	addl	$-2, %eax
	movl	%eax, bufflength(%rip)
	leaq	2(%rcx), %rax
	movq	%rax, inbuff(%rip)
	movzbl	1(%rcx), %eax
	orl	%r13d, %eax
	movl	%eax, %r13d
.LBB1_2:
	movq	40(%rdi), %rax
	movq	%r13, %rcx
	shlq	$4, %rcx
	movzbl	(%rax,%rcx), %edx
	movl	%edx, %ebx
	addb	$-3, %bl
	cmpb	$5, %bl
	ja	.LBB1_16
# BB#3:
	movzbl	%bl, %esi
	jmpq	*.LJTI1_0(,%rsi,8)
.LBB1_4:
	movq	8(%rax,%rcx), %rdx
	movq	%rsp, %rdi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	sprintf
	movl	$4, %ebp
	jmp	.LBB1_12
.LBB1_5:
	movss	8(%rax,%rcx), %xmm0     # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movq	%rsp, %rdi
	movl	$.L.str.4, %esi
	movb	$1, %al
	callq	sprintf
	movl	$6, %ebp
	jmp	.LBB1_12
.LBB1_6:
	leaq	(%rax,%rcx), %rsi
	movq	8(%rax,%rcx), %rdx
	testq	%rdx, %rdx
	movq	24(%rsi), %rcx
	movq	%rsp, %rdi
	je	.LBB1_10
# BB#7:
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	sprintf
	jmp	.LBB1_11
.LBB1_8:
	movsd	8(%rax,%rcx), %xmm0     # xmm0 = mem[0],zero
	movq	%rsp, %rdi
	movl	$.L.str.5, %esi
	movb	$1, %al
	callq	sprintf
	movl	$7, %ebp
	jmp	.LBB1_12
.LBB1_9:
	movzwl	8(%rax,%rcx), %ecx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %rdx
	movq	%rsp, %rdi
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	callq	sprintf
	movl	$8, %ebp
	jmp	.LBB1_12
.LBB1_10:
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	sprintf
.LBB1_11:
	movl	$5, %ebp
.LBB1_12:
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, %r15
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp5:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp6:
# BB#13:                                # %.noexc
	decl	%r12d
	movq	%r15, (%rbx)
	movl	%ebp, 8(%rbx)
	movl	$2, 12(%rbx)
	movl	%r13d, 16(%rbx)
	movl	$1, 8(%r14)
	movl	%r12d, 16(%r14)
	movl	%r12d, 12(%r14)
.Ltmp7:
	movl	$24, %edi
	callq	_Znwm
.Ltmp8:
# BB#14:
	movl	$0, (%rax)
	movl	$1, 4(%rax)
	movl	%ebp, 8(%rax)
	movl	$39, 12(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, (%r14)
	movq	stkptr(%rip), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, stkptr(%rip)
	movq	%r14, (%rax)
	xorl	%eax, %eax
.LBB1_15:
	addq	$1032, %rsp             # imm = 0x408
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_16:
	movq	stderr(%rip), %rdi
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$-1, %eax
	jmp	.LBB1_15
.LBB1_17:
.Ltmp9:
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_Z9pushconstP9Classfile, .Lfunc_end1-_Z9pushconstP9Classfile
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_4
	.quad	.LBB1_5
	.quad	.LBB1_6
	.quad	.LBB1_8
	.quad	.LBB1_16
	.quad	.LBB1_9
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp5-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp8-.Ltmp5           #   Call between .Ltmp5 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin1    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Lfunc_end1-.Ltmp8      #   Call between .Ltmp8 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z7pushimpP9Classfile
	.p2align	4, 0x90
	.type	_Z7pushimpP9Classfile,@function
_Z7pushimpP9Classfile:                  # @_Z7pushimpP9Classfile
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 16
	movl	$64, %edi
	callq	_Znwm
	movl	currpc(%rip), %ecx
	decl	%ecx
	movl	ch(%rip), %edx
	decl	%edx
	movl	$1, 8(%rax)
	movl	%ecx, 16(%rax)
	movl	%ecx, 12(%rax)
	leaq	(%rdx,%rdx,2), %rcx
	leaq	std_exps(,%rcx,8), %rcx
	movq	%rcx, (%rax)
	movq	stkptr(%rip), %rcx
	leaq	8(%rcx), %rdx
	movq	%rdx, stkptr(%rip)
	movq	%rax, (%rcx)
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end2:
	.size	_Z7pushimpP9Classfile, .Lfunc_end2-_Z7pushimpP9Classfile
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%i"
	.size	.L.str, 3

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"0x%lX"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"0x%lX%08lXL"
	.size	.L.str.2, 12

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"0x%lXL"
	.size	.L.str.3, 7

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%.25Gf"
	.size	.L.str.4, 7

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%.25Gd"
	.size	.L.str.5, 7

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\"%s\""
	.size	.L.str.6, 5

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Unkown tag %d on constant\n"
	.size	.L.str.7, 27


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
