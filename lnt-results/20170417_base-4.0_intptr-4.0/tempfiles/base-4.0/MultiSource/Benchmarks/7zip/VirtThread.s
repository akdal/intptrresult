	.text
	.file	"VirtThread.bc"
	.globl	_ZN11CVirtThread6CreateEv
	.p2align	4, 0x90
	.type	_ZN11CVirtThread6CreateEv,@function
_ZN11CVirtThread6CreateEv:              # @_ZN11CVirtThread6CreateEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	leaq	8(%rbx), %r14
	cmpl	$0, 8(%rbx)
	je	.LBB0_1
.LBB0_2:                                # %_ZN8NWindows16NSynchronization15CAutoResetEvent18CreateIfNotCreatedEv.exit.thread
	leaq	112(%rbx), %r15
	cmpl	$0, 112(%rbx)
	je	.LBB0_3
.LBB0_4:                                # %_ZN8NWindows16NSynchronization15CAutoResetEvent18CreateIfNotCreatedEv.exit16.thread
	movq	%r14, %rdi
	callq	Event_Reset
	movq	%r15, %rdi
	callq	Event_Reset
	movb	$0, 232(%rbx)
	xorl	%eax, %eax
	cmpl	$0, 224(%rbx)
	jne	.LBB0_5
# BB#6:
	leaq	216(%rbx), %rdi
	movl	$_ZL11CoderThreadPv, %esi
	movq	%rbx, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	Thread_Create           # TAILCALL
.LBB0_1:                                # %_ZN8NWindows16NSynchronization15CAutoResetEvent18CreateIfNotCreatedEv.exit
	movq	%r14, %rdi
	callq	AutoResetEvent_CreateNotSignaled
	testl	%eax, %eax
	jne	.LBB0_5
	jmp	.LBB0_2
.LBB0_3:                                # %_ZN8NWindows16NSynchronization15CAutoResetEvent18CreateIfNotCreatedEv.exit16
	movq	%r15, %rdi
	callq	AutoResetEvent_CreateNotSignaled
	testl	%eax, %eax
	je	.LBB0_4
.LBB0_5:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	_ZN11CVirtThread6CreateEv, .Lfunc_end0-_ZN11CVirtThread6CreateEv
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL11CoderThreadPv,@function
_ZL11CoderThreadPv:                     # @_ZL11CoderThreadPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	leaq	8(%rbx), %r14
	movq	%r14, %rdi
	callq	Event_Wait
	cmpb	$0, 232(%rbx)
	jne	.LBB1_3
# BB#1:                                 # %.critedge.lr.ph
	leaq	112(%rbx), %r15
	.p2align	4, 0x90
.LBB1_2:                                # %.critedge
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%r15, %rdi
	callq	Event_Set
	movq	%r14, %rdi
	callq	Event_Wait
	cmpb	$0, 232(%rbx)
	je	.LBB1_2
.LBB1_3:                                # %._crit_edge
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	_ZL11CoderThreadPv, .Lfunc_end1-_ZL11CoderThreadPv
	.cfi_endproc

	.globl	_ZN11CVirtThread5StartEv
	.p2align	4, 0x90
	.type	_ZN11CVirtThread5StartEv,@function
_ZN11CVirtThread5StartEv:               # @_ZN11CVirtThread5StartEv
	.cfi_startproc
# BB#0:
	movb	$0, 232(%rdi)
	addq	$8, %rdi
	jmp	Event_Set               # TAILCALL
.Lfunc_end2:
	.size	_ZN11CVirtThread5StartEv, .Lfunc_end2-_ZN11CVirtThread5StartEv
	.cfi_endproc

	.globl	_ZN11CVirtThreadD2Ev
	.p2align	4, 0x90
	.type	_ZN11CVirtThreadD2Ev,@function
_ZN11CVirtThreadD2Ev:                   # @_ZN11CVirtThreadD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	$_ZTV11CVirtThread+16, (%rbx)
	movb	$1, 232(%rbx)
	leaq	8(%rbx), %r14
	cmpl	$0, 8(%rbx)
	je	.LBB3_2
# BB#1:
.Ltmp0:
	movq	%r14, %rdi
	callq	Event_Set
.Ltmp1:
.LBB3_2:                                # %_ZN8NWindows16NSynchronization10CBaseEvent3SetEv.exit
	leaq	216(%rbx), %r15
	cmpl	$0, 224(%rbx)
	je	.LBB3_4
# BB#3:
.Ltmp2:
	movq	%r15, %rdi
	callq	Thread_Wait
.Ltmp3:
.LBB3_4:                                # %_ZN8NWindows7CThread4WaitEv.exit
.Ltmp7:
	movq	%r15, %rdi
	callq	Thread_Close
.Ltmp8:
# BB#5:                                 # %_ZN8NWindows7CThreadD2Ev.exit4
	addq	$112, %rbx
.Ltmp12:
	movq	%rbx, %rdi
	callq	Event_Close
.Ltmp13:
# BB#6:                                 # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit5
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	Event_Close             # TAILCALL
.LBB3_8:
.Ltmp14:
	movq	%rax, %r15
	jmp	.LBB3_11
.LBB3_9:
.Ltmp9:
	movq	%rax, %r15
	jmp	.LBB3_10
.LBB3_7:
.Ltmp4:
	movq	%rax, %r15
	leaq	216(%rbx), %rdi
.Ltmp5:
	callq	Thread_Close
.Ltmp6:
.LBB3_10:                               # %_ZN8NWindows7CThreadD2Ev.exit
	addq	$112, %rbx
.Ltmp10:
	movq	%rbx, %rdi
	callq	Event_Close
.Ltmp11:
.LBB3_11:                               # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit6
.Ltmp15:
	movq	%r14, %rdi
	callq	Event_Close
.Ltmp16:
# BB#12:                                # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB3_13:
.Ltmp17:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN11CVirtThreadD2Ev, .Lfunc_end3-_ZN11CVirtThreadD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp8-.Ltmp7           #   Call between .Ltmp7 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp5-.Ltmp13          #   Call between .Ltmp13 and .Ltmp5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp16-.Ltmp5          #   Call between .Ltmp5 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Lfunc_end3-.Ltmp16     #   Call between .Ltmp16 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end4:
	.size	__clang_call_terminate, .Lfunc_end4-__clang_call_terminate

	.type	_ZTV11CVirtThread,@object # @_ZTV11CVirtThread
	.section	.rodata._ZTV11CVirtThread,"aG",@progbits,_ZTV11CVirtThread,comdat
	.weak	_ZTV11CVirtThread
	.p2align	3
_ZTV11CVirtThread:
	.quad	0
	.quad	_ZTI11CVirtThread
	.quad	__cxa_pure_virtual
	.size	_ZTV11CVirtThread, 24

	.type	_ZTS11CVirtThread,@object # @_ZTS11CVirtThread
	.section	.rodata._ZTS11CVirtThread,"aG",@progbits,_ZTS11CVirtThread,comdat
	.weak	_ZTS11CVirtThread
_ZTS11CVirtThread:
	.asciz	"11CVirtThread"
	.size	_ZTS11CVirtThread, 14

	.type	_ZTI11CVirtThread,@object # @_ZTI11CVirtThread
	.section	.rodata._ZTI11CVirtThread,"aG",@progbits,_ZTI11CVirtThread,comdat
	.weak	_ZTI11CVirtThread
	.p2align	3
_ZTI11CVirtThread:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS11CVirtThread
	.size	_ZTI11CVirtThread, 16


	.globl	_ZN11CVirtThreadD1Ev
	.type	_ZN11CVirtThreadD1Ev,@function
_ZN11CVirtThreadD1Ev = _ZN11CVirtThreadD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
