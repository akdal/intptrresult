	.text
	.file	"lists.bc"
	.globl	_Z10test_listsv
	.p2align	4, 0x90
	.type	_Z10test_listsv,@function
_Z10test_listsv:                        # @_Z10test_listsv
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	leaq	48(%rsp), %rbx
	movq	%rbx, 48(%rsp)
	movq	%rbx, 56(%rsp)
	movq	$0, 64(%rsp)
	movq	$-10000, %rbp           # imm = 0xD8F0
	.p2align	4, 0x90
.LBB0_1:                                # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
.Ltmp0:
	movl	$24, %edi
	callq	_Znwm
.Ltmp1:
# BB#2:                                 # %.noexc.i
                                        #   in Loop: Header=BB0_1 Depth=1
	movq	$0, 16(%rax)
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	_ZNSt8__detail15_List_node_base7_M_hookEPS0_
	incq	64(%rsp)
	incq	%rbp
	jne	.LBB0_1
# BB#3:
	movq	48(%rsp), %r15
	cmpq	%rbx, %r15
	je	.LBB0_6
# BB#4:                                 # %.lr.ph.preheader.i
	movl	$1, %eax
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rdx
	movq	%rax, 16(%rcx)
	incq	%rax
	cmpq	%rdx, %rbx
	movq	%rdx, %rcx
	jne	.LBB0_5
.LBB0_6:                                # %_Z4iotaISt14_List_iteratorImEiEvT_S2_T0_.exit
	cmpq	%rbx, %r15
	leaq	24(%rsp), %r13
	movq	%r13, 24(%rsp)
	movq	%r13, 32(%rsp)
	movq	$0, 40(%rsp)
	je	.LBB0_7
# BB#10:                                # %.lr.ph.i.i19
	leaq	24(%rsp), %r14
	.p2align	4, 0x90
.LBB0_11:                               # =>This Inner Loop Header: Depth=1
.Ltmp3:
	movl	$24, %edi
	callq	_Znwm
.Ltmp4:
# BB#12:                                # %.noexc.i20
                                        #   in Loop: Header=BB0_11 Depth=1
	movq	16(%r15), %rcx
	movq	%rcx, 16(%rax)
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	_ZNSt8__detail15_List_node_base7_M_hookEPS0_
	movq	40(%rsp), %rbp
	incq	%rbp
	movq	%rbp, 40(%rsp)
	movq	(%r15), %r15
	cmpq	%r15, %rbx
	jne	.LBB0_11
	jmp	.LBB0_13
.LBB0_7:
	xorl	%ebp, %ebp
.LBB0_13:                               # %.loopexit44
	movq	%rsp, %rax
	movq	%rax, (%rsp)
	movq	%rax, 8(%rsp)
	movq	$0, 16(%rsp)
	testq	%rbp, %rbp
	je	.LBB0_21
# BB#14:                                # %.lr.ph46
	movq	%rsp, %r14
	.p2align	4, 0x90
.LBB0_15:                               # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %r15
.Ltmp6:
	movl	$24, %edi
	callq	_Znwm
.Ltmp7:
# BB#16:                                #   in Loop: Header=BB0_15 Depth=1
	decq	%rbp
	movq	16(%r15), %rcx
	movq	%rcx, 16(%rax)
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	_ZNSt8__detail15_List_node_base7_M_hookEPS0_
	incq	16(%rsp)
	movq	24(%rsp), %r15
	decq	40(%rsp)
	movq	%r15, %rdi
	callq	_ZNSt8__detail15_List_node_base9_M_unhookEv
	movq	%r15, %rdi
	callq	_ZdlPv
	testq	%rbp, %rbp
	jne	.LBB0_15
# BB#17:                                # %._crit_edge47
	movq	16(%rsp), %r15
	testq	%r15, %r15
	jne	.LBB0_19
	jmp	.LBB0_21
	.p2align	4, 0x90
.LBB0_20:                               #   in Loop: Header=BB0_19 Depth=1
	decq	%r15
	movq	16(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	%rax, %rdi
	leaq	24(%rsp), %rsi
	callq	_ZNSt8__detail15_List_node_base7_M_hookEPS0_
	incq	40(%rsp)
	movq	8(%rsp), %r12
	decq	16(%rsp)
	movq	%r12, %rdi
	callq	_ZNSt8__detail15_List_node_base9_M_unhookEv
	movq	%r12, %rdi
	callq	_ZdlPv
	testq	%r15, %r15
	je	.LBB0_21
.LBB0_19:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rbp
.Ltmp9:
	movl	$24, %edi
	callq	_Znwm
.Ltmp10:
	jmp	.LBB0_20
.LBB0_21:                               # %._crit_edge
	leaq	48(%rsp), %r15
	movq	%r15, %rdi
	callq	_ZNSt8__detail15_List_node_base10_M_reverseEv
	movq	48(%rsp), %rcx
	cmpq	$10000, 16(%rcx)        # imm = 0x2710
	jne	.LBB0_22
# BB#25:
	movq	24(%rsp), %rax
	movq	%rsp, %rbx
	cmpq	%rax, %r13
	jne	.LBB0_27
	jmp	.LBB0_30
	.p2align	4, 0x90
.LBB0_29:                               #   in Loop: Header=BB0_27 Depth=1
	movq	(%rax), %rax
	movq	(%rcx), %rcx
	cmpq	%rax, %r13
	je	.LBB0_30
.LBB0_27:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rcx, %r15
	je	.LBB0_30
# BB#28:                                # %.lr.ph.i33
                                        #   in Loop: Header=BB0_27 Depth=1
	movq	16(%rcx), %rdx
	cmpq	16(%rax), %rdx
	je	.LBB0_29
.LBB0_30:                               # %.loopexit
	xorl	%edx, %edx
	cmpq	%rcx, %r15
	movq	64(%rsp), %r14
	cmovneq	%rdx, %r14
	cmpq	%rax, %r13
	cmovneq	%rdx, %r14
	jmp	.LBB0_31
.LBB0_22:
	xorl	%r14d, %r14d
	movq	%rsp, %rbx
.LBB0_31:
	movq	(%rsp), %rdi
	cmpq	%rbx, %rdi
	je	.LBB0_33
	.p2align	4, 0x90
.LBB0_32:                               # %.lr.ph.i.i38
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbp
	callq	_ZdlPv
	cmpq	%rbx, %rbp
	movq	%rbp, %rdi
	jne	.LBB0_32
.LBB0_33:                               # %_ZNSt7__cxx1110_List_baseImSaImEED2Ev.exit39
	movq	24(%rsp), %rdi
	cmpq	%r13, %rdi
	je	.LBB0_35
	.p2align	4, 0x90
.LBB0_34:                               # %.lr.ph.i.i35
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbp
	callq	_ZdlPv
	cmpq	%r13, %rbp
	movq	%rbp, %rdi
	jne	.LBB0_34
.LBB0_35:                               # %_ZNSt7__cxx1110_List_baseImSaImEED2Ev.exit36
	movq	48(%rsp), %rdi
	cmpq	%r15, %rdi
	je	.LBB0_37
	.p2align	4, 0x90
.LBB0_36:                               # %.lr.ph.i.i27
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbp
	callq	_ZdlPv
	cmpq	%r15, %rbp
	movq	%rbp, %rdi
	jne	.LBB0_36
.LBB0_37:                               # %_ZNSt7__cxx1110_List_baseImSaImEED2Ev.exit28
	movq	%r14, %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_38:                               # %.loopexit43
.Ltmp11:
	jmp	.LBB0_40
.LBB0_39:                               # %.loopexit.split-lp
.Ltmp8:
.LBB0_40:
	movq	%rax, %r15
	movq	(%rsp), %rdi
	cmpq	%r14, %rdi
	je	.LBB0_42
	.p2align	4, 0x90
.LBB0_41:                               # %.lr.ph.i.i30
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbp
	callq	_ZdlPv
	cmpq	%r14, %rbp
	movq	%rbp, %rdi
	jne	.LBB0_41
.LBB0_42:                               # %.loopexit42
	movq	24(%rsp), %rdi
	cmpq	%r13, %rdi
	je	.LBB0_44
	.p2align	4, 0x90
.LBB0_43:                               # %.lr.ph.i.i17
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbp
	callq	_ZdlPv
	cmpq	%r13, %rbp
	movq	%rbp, %rdi
	jne	.LBB0_43
	jmp	.LBB0_44
.LBB0_23:
.Ltmp5:
	movq	%rax, %r15
	movq	24(%rsp), %rdi
	cmpq	%r14, %rdi
	je	.LBB0_44
	.p2align	4, 0x90
.LBB0_24:                               # %.lr.ph.i.i.i22
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbp
	callq	_ZdlPv
	cmpq	%r14, %rbp
	movq	%rbp, %rdi
	jne	.LBB0_24
.LBB0_44:
	movq	48(%rsp), %rdi
	cmpq	%rbx, %rdi
	je	.LBB0_46
	.p2align	4, 0x90
.LBB0_45:                               # %.lr.ph.i.i15
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbp
	callq	_ZdlPv
	cmpq	%rbx, %rbp
	movq	%rbp, %rdi
	jne	.LBB0_45
	jmp	.LBB0_46
.LBB0_8:
.Ltmp2:
	movq	%rax, %r15
	movq	48(%rsp), %rdi
	cmpq	%rbx, %rdi
	je	.LBB0_46
	.p2align	4, 0x90
.LBB0_9:                                # %.lr.ph.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbp
	callq	_ZdlPv
	cmpq	%rbx, %rbp
	movq	%rbp, %rdi
	jne	.LBB0_9
.LBB0_46:
	movq	%r15, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z10test_listsv, .Lfunc_end0-_Z10test_listsv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Lfunc_end0-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	cmpl	$2, %edi
	jne	.LBB1_1
# BB#2:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	testl	%eax, %eax
	jle	.LBB1_3
# BB#5:                                 # %select.unfold.preheader
	movslq	%eax, %rbx
	testq	%rbx, %rbx
	jne	.LBB1_4
# BB#6:
	xorl	%eax, %eax
	jmp	.LBB1_7
.LBB1_1:
	movl	$3000, %ebx             # imm = 0xBB8
	jmp	.LBB1_4
.LBB1_3:
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB1_4:                                # %select.unfold
                                        # =>This Inner Loop Header: Depth=1
	callq	_Z10test_listsv
	decq	%rbx
	jne	.LBB1_4
.LBB1_7:                                # %select.unfold._crit_edge
	movl	$_ZSt4cout, %edi
	movq	%rax, %rsi
	callq	_ZNSo9_M_insertImEERSoT_
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	-24(%rax), %rax
	movq	240(%r14,%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB1_12
# BB#8:                                 # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit
	cmpb	$0, 56(%rbx)
	je	.LBB1_10
# BB#9:
	movb	67(%rbx), %al
	jmp	.LBB1_11
.LBB1_10:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB1_11:                               # %_ZNKSt5ctypeIcE5widenEc.exit
	movsbl	%al, %esi
	movq	%r14, %rdi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_12:
	callq	_ZSt16__throw_bad_castv
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_lists.ii,@function
_GLOBAL__sub_I_lists.ii:                # @_GLOBAL__sub_I_lists.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end2:
	.size	_GLOBAL__sub_I_lists.ii, .Lfunc_end2-_GLOBAL__sub_I_lists.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_lists.ii

	.ident	"clang version 5.0.0 (https://github.com/aqjune/clang-intptr.git 8e2e88f063bfae95e3be980dbf26473d5a086d27) (https://github.com/aqjune/llvm-intptr.git 51d41dcad277119de970b737044ce4a7e91ad33d)"
	.section	".note.GNU-stack","",@progbits
