	.text
	.file	"StreamBinder.bc"
	.globl	_ZN28CSequentialInStreamForBinder4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN28CSequentialInStreamForBinder4ReadEPvjPj,@function
_ZN28CSequentialInStreamForBinder4ReadEPvjPj: # @_ZN28CSequentialInStreamForBinder4ReadEPvjPj
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rdi
	jmp	_ZN13CStreamBinder4ReadEPvjPj # TAILCALL
.Lfunc_end0:
	.size	_ZN28CSequentialInStreamForBinder4ReadEPvjPj, .Lfunc_end0-_ZN28CSequentialInStreamForBinder4ReadEPvjPj
	.cfi_endproc

	.globl	_ZN13CStreamBinder4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN13CStreamBinder4ReadEPvjPj,@function
_ZN13CStreamBinder4ReadEPvjPj:          # @_ZN13CStreamBinder4ReadEPvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %ebp
	movq	%rsi, %r12
	movq	%rdi, %rbx
	testl	%ebp, %ebp
	je	.LBB1_1
# BB#2:
	leaq	24(%rbx), %r15
	movq	%r15, %rdi
	callq	Event_Wait
	testl	%eax, %eax
	jne	.LBB1_9
# BB#3:
	movl	160(%rbx), %eax
	cmpl	%ebp, %eax
	cmovbl	%eax, %ebp
	testl	%eax, %eax
	je	.LBB1_6
# BB#4:
	movq	168(%rbx), %rsi
	movl	%ebp, %r13d
	movq	%r12, %rdi
	movq	%r13, %rdx
	callq	memcpy
	addq	%r13, 168(%rbx)
	movl	160(%rbx), %eax
	subl	%ebp, %eax
	movl	%eax, 160(%rbx)
	jne	.LBB1_6
# BB#5:
	movq	%r15, %rdi
	callq	Event_Reset
	movq	8(%rbx), %rdi
	callq	pthread_mutex_lock
	movb	$1, 17(%rbx)
	movq	8(%rbx), %r15
	leaq	40(%r15), %rdi
	callq	pthread_cond_broadcast
	movq	%r15, %rdi
	callq	pthread_mutex_unlock
	testq	%r14, %r14
	jne	.LBB1_7
	jmp	.LBB1_8
.LBB1_1:
	xorl	%ebp, %ebp
.LBB1_6:
	testq	%r14, %r14
	je	.LBB1_8
.LBB1_7:
	movl	%ebp, (%r14)
.LBB1_8:
	movl	%ebp, %eax
	addq	%rax, 176(%rbx)
	xorl	%eax, %eax
.LBB1_9:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN13CStreamBinder4ReadEPvjPj, .Lfunc_end1-_ZN13CStreamBinder4ReadEPvjPj
	.cfi_endproc

	.globl	_ZN29CSequentialOutStreamForBinder5WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZN29CSequentialOutStreamForBinder5WriteEPKvjPj,@function
_ZN29CSequentialOutStreamForBinder5WriteEPKvjPj: # @_ZN29CSequentialOutStreamForBinder5WriteEPKvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 48
.Lcfi17:
	.cfi_offset %rbx, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %ebp
	testl	%ebp, %ebp
	je	.LBB2_2
# BB#1:
	movq	16(%rdi), %rbx
	movq	%rsi, 168(%rbx)
	movl	%ebp, 160(%rbx)
	movq	8(%rbx), %rdi
	callq	pthread_mutex_lock
	movb	$0, 17(%rbx)
	movq	8(%rbx), %rdi
	callq	pthread_mutex_unlock
	leaq	24(%rbx), %rdi
	callq	Event_Set
	movq	%rbx, (%rsp)
	subq	$-128, %rbx
	movq	%rbx, 8(%rsp)
	movq	%rsp, %rsi
	movl	$2, %edi
	xorl	%edx, %edx
	movl	$-1, %ecx
	callq	_Z22WaitForMultipleObjectsjPKPN8NWindows16NSynchronization15CBaseHandleWFMOEij
	movl	%eax, %ecx
	movl	$1, %eax
	testl	%ecx, %ecx
	jne	.LBB2_4
.LBB2_2:
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.LBB2_4
# BB#3:
	movl	%ebp, (%r14)
.LBB2_4:                                # %_ZN13CStreamBinder5WriteEPKvjPj.exit
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN29CSequentialOutStreamForBinder5WriteEPKvjPj, .Lfunc_end2-_ZN29CSequentialOutStreamForBinder5WriteEPKvjPj
	.cfi_endproc

	.globl	_ZN13CStreamBinder5WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZN13CStreamBinder5WriteEPKvjPj,@function
_ZN13CStreamBinder5WriteEPKvjPj:        # @_ZN13CStreamBinder5WriteEPKvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 48
.Lcfi24:
	.cfi_offset %rbx, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %ebp
	movq	%rdi, %rbx
	testl	%ebp, %ebp
	je	.LBB3_2
# BB#1:
	movq	%rsi, 168(%rbx)
	movl	%ebp, 160(%rbx)
	movq	8(%rbx), %rdi
	callq	pthread_mutex_lock
	movb	$0, 17(%rbx)
	movq	8(%rbx), %rdi
	callq	pthread_mutex_unlock
	leaq	24(%rbx), %rdi
	callq	Event_Set
	movq	%rbx, (%rsp)
	subq	$-128, %rbx
	movq	%rbx, 8(%rsp)
	movq	%rsp, %rsi
	movl	$2, %edi
	xorl	%edx, %edx
	movl	$-1, %ecx
	callq	_Z22WaitForMultipleObjectsjPKPN8NWindows16NSynchronization15CBaseHandleWFMOEij
	movl	%eax, %ecx
	movl	$1, %eax
	testl	%ecx, %ecx
	jne	.LBB3_4
.LBB3_2:
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.LBB3_4
# BB#3:
	movl	%ebp, (%r14)
.LBB3_4:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZN13CStreamBinder5WriteEPKvjPj, .Lfunc_end3-_ZN13CStreamBinder5WriteEPKvjPj
	.cfi_endproc

	.globl	_ZN13CStreamBinder12CreateEventsEv
	.p2align	4, 0x90
	.type	_ZN13CStreamBinder12CreateEventsEv,@function
_ZN13CStreamBinder12CreateEventsEv:     # @_ZN13CStreamBinder12CreateEventsEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 32
.Lcfi30:
	.cfi_offset %rbx, -24
.Lcfi31:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$96, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movb	$0, 88(%rbx)
	movq	%rbx, 152(%r14)
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	pthread_mutex_init
	leaq	40(%rbx), %rdi
	xorl	%esi, %esi
	callq	pthread_cond_init
	movq	152(%r14), %rax
	movq	%rax, 8(%r14)
	movw	$257, 16(%r14)          # imm = 0x101
	leaq	24(%r14), %rdi
	xorl	%esi, %esi
	callq	ManualResetEvent_Create
	testl	%eax, %eax
	jne	.LBB4_2
# BB#1:
	movq	152(%r14), %rax
	movq	%rax, 136(%r14)
	movw	$1, 144(%r14)
	xorl	%eax, %eax
.LBB4_2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	_ZN13CStreamBinder12CreateEventsEv, .Lfunc_end4-_ZN13CStreamBinder12CreateEventsEv
	.cfi_endproc

	.globl	_ZN13CStreamBinder6ReInitEv
	.p2align	4, 0x90
	.type	_ZN13CStreamBinder6ReInitEv,@function
_ZN13CStreamBinder6ReInitEv:            # @_ZN13CStreamBinder6ReInitEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 16
.Lcfi33:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	24(%rbx), %rdi
	callq	Event_Reset
	movq	136(%rbx), %rdi
	callq	pthread_mutex_lock
	movb	$0, 145(%rbx)
	movq	136(%rbx), %rdi
	callq	pthread_mutex_unlock
	movq	$0, 176(%rbx)
	popq	%rbx
	retq
.Lfunc_end5:
	.size	_ZN13CStreamBinder6ReInitEv, .Lfunc_end5-_ZN13CStreamBinder6ReInitEv
	.cfi_endproc

	.globl	_ZN13CStreamBinder13CreateStreamsEPP19ISequentialInStreamPP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZN13CStreamBinder13CreateStreamsEPP19ISequentialInStreamPP20ISequentialOutStream,@function
_ZN13CStreamBinder13CreateStreamsEPP19ISequentialInStreamPP20ISequentialOutStream: # @_ZN13CStreamBinder13CreateStreamsEPP19ISequentialInStreamPP20ISequentialOutStream
	.cfi_startproc
# BB#0:                                 # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 32
.Lcfi37:
	.cfi_offset %rbx, -32
.Lcfi38:
	.cfi_offset %r14, -24
.Lcfi39:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$24, %edi
	callq	_Znwm
	movq	$_ZTV28CSequentialInStreamForBinder+16, (%rax)
	movl	$1, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, (%r15)
	movl	$24, %edi
	callq	_Znwm
	movq	$_ZTV29CSequentialOutStreamForBinder+16, (%rax)
	movl	$1, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, (%r14)
	movl	$0, 160(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 168(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	_ZN13CStreamBinder13CreateStreamsEPP19ISequentialInStreamPP20ISequentialOutStream, .Lfunc_end6-_ZN13CStreamBinder13CreateStreamsEPP19ISequentialInStreamPP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZN13CStreamBinder9CloseReadEv
	.p2align	4, 0x90
	.type	_ZN13CStreamBinder9CloseReadEv,@function
_ZN13CStreamBinder9CloseReadEv:         # @_ZN13CStreamBinder9CloseReadEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 16
.Lcfi41:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	136(%rbx), %rdi
	callq	pthread_mutex_lock
	movb	$1, 145(%rbx)
	movq	136(%rbx), %rbx
	leaq	40(%rbx), %rdi
	callq	pthread_cond_broadcast
	movq	%rbx, %rdi
	popq	%rbx
	jmp	pthread_mutex_unlock    # TAILCALL
.Lfunc_end7:
	.size	_ZN13CStreamBinder9CloseReadEv, .Lfunc_end7-_ZN13CStreamBinder9CloseReadEv
	.cfi_endproc

	.globl	_ZN13CStreamBinder10CloseWriteEv
	.p2align	4, 0x90
	.type	_ZN13CStreamBinder10CloseWriteEv,@function
_ZN13CStreamBinder10CloseWriteEv:       # @_ZN13CStreamBinder10CloseWriteEv
	.cfi_startproc
# BB#0:
	addq	$24, %rdi
	jmp	Event_Set               # TAILCALL
.Lfunc_end8:
	.size	_ZN13CStreamBinder10CloseWriteEv, .Lfunc_end8-_ZN13CStreamBinder10CloseWriteEv
	.cfi_endproc

	.section	.text._ZN28CSequentialInStreamForBinder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN28CSequentialInStreamForBinder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN28CSequentialInStreamForBinder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN28CSequentialInStreamForBinder14QueryInterfaceERK4GUIDPPv,@function
_ZN28CSequentialInStreamForBinder14QueryInterfaceERK4GUIDPPv: # @_ZN28CSequentialInStreamForBinder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB9_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB9_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB9_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB9_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB9_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB9_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB9_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB9_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB9_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB9_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB9_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB9_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB9_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB9_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB9_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB9_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB9_17:                               # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end9:
	.size	_ZN28CSequentialInStreamForBinder14QueryInterfaceERK4GUIDPPv, .Lfunc_end9-_ZN28CSequentialInStreamForBinder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN28CSequentialInStreamForBinder6AddRefEv,"axG",@progbits,_ZN28CSequentialInStreamForBinder6AddRefEv,comdat
	.weak	_ZN28CSequentialInStreamForBinder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN28CSequentialInStreamForBinder6AddRefEv,@function
_ZN28CSequentialInStreamForBinder6AddRefEv: # @_ZN28CSequentialInStreamForBinder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end10:
	.size	_ZN28CSequentialInStreamForBinder6AddRefEv, .Lfunc_end10-_ZN28CSequentialInStreamForBinder6AddRefEv
	.cfi_endproc

	.section	.text._ZN28CSequentialInStreamForBinder7ReleaseEv,"axG",@progbits,_ZN28CSequentialInStreamForBinder7ReleaseEv,comdat
	.weak	_ZN28CSequentialInStreamForBinder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN28CSequentialInStreamForBinder7ReleaseEv,@function
_ZN28CSequentialInStreamForBinder7ReleaseEv: # @_ZN28CSequentialInStreamForBinder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB11_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB11_2:
	popq	%rcx
	retq
.Lfunc_end11:
	.size	_ZN28CSequentialInStreamForBinder7ReleaseEv, .Lfunc_end11-_ZN28CSequentialInStreamForBinder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN28CSequentialInStreamForBinderD2Ev,"axG",@progbits,_ZN28CSequentialInStreamForBinderD2Ev,comdat
	.weak	_ZN28CSequentialInStreamForBinderD2Ev
	.p2align	4, 0x90
	.type	_ZN28CSequentialInStreamForBinderD2Ev,@function
_ZN28CSequentialInStreamForBinderD2Ev:  # @_ZN28CSequentialInStreamForBinderD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 16
.Lcfi45:
	.cfi_offset %rbx, -16
	movq	$_ZTV28CSequentialInStreamForBinder+16, (%rdi)
	movq	16(%rdi), %rbx
	movq	136(%rbx), %rdi
	callq	pthread_mutex_lock
	movb	$1, 145(%rbx)
	movq	136(%rbx), %rbx
	leaq	40(%rbx), %rdi
	callq	pthread_cond_broadcast
	movq	%rbx, %rdi
	popq	%rbx
	jmp	pthread_mutex_unlock    # TAILCALL
.Lfunc_end12:
	.size	_ZN28CSequentialInStreamForBinderD2Ev, .Lfunc_end12-_ZN28CSequentialInStreamForBinderD2Ev
	.cfi_endproc

	.section	.text._ZN28CSequentialInStreamForBinderD0Ev,"axG",@progbits,_ZN28CSequentialInStreamForBinderD0Ev,comdat
	.weak	_ZN28CSequentialInStreamForBinderD0Ev
	.p2align	4, 0x90
	.type	_ZN28CSequentialInStreamForBinderD0Ev,@function
_ZN28CSequentialInStreamForBinderD0Ev:  # @_ZN28CSequentialInStreamForBinderD0Ev
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi48:
	.cfi_def_cfa_offset 32
.Lcfi49:
	.cfi_offset %rbx, -24
.Lcfi50:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	$_ZTV28CSequentialInStreamForBinder+16, (%r14)
	movq	16(%r14), %rbx
	movq	136(%rbx), %rdi
	callq	pthread_mutex_lock
	movb	$1, 145(%rbx)
	movq	136(%rbx), %rbx
	leaq	40(%rbx), %rdi
	callq	pthread_cond_broadcast
	movq	%rbx, %rdi
	callq	pthread_mutex_unlock
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end13:
	.size	_ZN28CSequentialInStreamForBinderD0Ev, .Lfunc_end13-_ZN28CSequentialInStreamForBinderD0Ev
	.cfi_endproc

	.section	.text._ZN29CSequentialOutStreamForBinder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN29CSequentialOutStreamForBinder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN29CSequentialOutStreamForBinder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN29CSequentialOutStreamForBinder14QueryInterfaceERK4GUIDPPv,@function
_ZN29CSequentialOutStreamForBinder14QueryInterfaceERK4GUIDPPv: # @_ZN29CSequentialOutStreamForBinder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB14_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB14_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB14_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB14_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB14_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB14_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB14_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB14_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB14_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB14_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB14_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB14_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB14_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB14_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB14_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB14_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB14_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end14:
	.size	_ZN29CSequentialOutStreamForBinder14QueryInterfaceERK4GUIDPPv, .Lfunc_end14-_ZN29CSequentialOutStreamForBinder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN29CSequentialOutStreamForBinder6AddRefEv,"axG",@progbits,_ZN29CSequentialOutStreamForBinder6AddRefEv,comdat
	.weak	_ZN29CSequentialOutStreamForBinder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN29CSequentialOutStreamForBinder6AddRefEv,@function
_ZN29CSequentialOutStreamForBinder6AddRefEv: # @_ZN29CSequentialOutStreamForBinder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end15:
	.size	_ZN29CSequentialOutStreamForBinder6AddRefEv, .Lfunc_end15-_ZN29CSequentialOutStreamForBinder6AddRefEv
	.cfi_endproc

	.section	.text._ZN29CSequentialOutStreamForBinder7ReleaseEv,"axG",@progbits,_ZN29CSequentialOutStreamForBinder7ReleaseEv,comdat
	.weak	_ZN29CSequentialOutStreamForBinder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN29CSequentialOutStreamForBinder7ReleaseEv,@function
_ZN29CSequentialOutStreamForBinder7ReleaseEv: # @_ZN29CSequentialOutStreamForBinder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB16_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB16_2:
	popq	%rcx
	retq
.Lfunc_end16:
	.size	_ZN29CSequentialOutStreamForBinder7ReleaseEv, .Lfunc_end16-_ZN29CSequentialOutStreamForBinder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN29CSequentialOutStreamForBinderD2Ev,"axG",@progbits,_ZN29CSequentialOutStreamForBinderD2Ev,comdat
	.weak	_ZN29CSequentialOutStreamForBinderD2Ev
	.p2align	4, 0x90
	.type	_ZN29CSequentialOutStreamForBinderD2Ev,@function
_ZN29CSequentialOutStreamForBinderD2Ev: # @_ZN29CSequentialOutStreamForBinderD2Ev
	.cfi_startproc
# BB#0:                                 # %_ZN13CStreamBinder10CloseWriteEv.exit
	movq	$_ZTV29CSequentialOutStreamForBinder+16, (%rdi)
	movq	16(%rdi), %rdi
	addq	$24, %rdi
	jmp	Event_Set               # TAILCALL
.Lfunc_end17:
	.size	_ZN29CSequentialOutStreamForBinderD2Ev, .Lfunc_end17-_ZN29CSequentialOutStreamForBinderD2Ev
	.cfi_endproc

	.section	.text._ZN29CSequentialOutStreamForBinderD0Ev,"axG",@progbits,_ZN29CSequentialOutStreamForBinderD0Ev,comdat
	.weak	_ZN29CSequentialOutStreamForBinderD0Ev
	.p2align	4, 0x90
	.type	_ZN29CSequentialOutStreamForBinderD0Ev,@function
_ZN29CSequentialOutStreamForBinderD0Ev: # @_ZN29CSequentialOutStreamForBinderD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi55:
	.cfi_def_cfa_offset 32
.Lcfi56:
	.cfi_offset %rbx, -24
.Lcfi57:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV29CSequentialOutStreamForBinder+16, (%rbx)
	movq	16(%rbx), %rdi
	addq	$24, %rdi
.Ltmp0:
	callq	Event_Set
.Ltmp1:
# BB#1:                                 # %_ZN29CSequentialOutStreamForBinderD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB18_2:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end18:
	.size	_ZN29CSequentialOutStreamForBinderD0Ev, .Lfunc_end18-_ZN29CSequentialOutStreamForBinderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end18-.Ltmp1     #   Call between .Ltmp1 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTV28CSequentialInStreamForBinder,@object # @_ZTV28CSequentialInStreamForBinder
	.section	.rodata,"a",@progbits
	.globl	_ZTV28CSequentialInStreamForBinder
	.p2align	3
_ZTV28CSequentialInStreamForBinder:
	.quad	0
	.quad	_ZTI28CSequentialInStreamForBinder
	.quad	_ZN28CSequentialInStreamForBinder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN28CSequentialInStreamForBinder6AddRefEv
	.quad	_ZN28CSequentialInStreamForBinder7ReleaseEv
	.quad	_ZN28CSequentialInStreamForBinderD2Ev
	.quad	_ZN28CSequentialInStreamForBinderD0Ev
	.quad	_ZN28CSequentialInStreamForBinder4ReadEPvjPj
	.size	_ZTV28CSequentialInStreamForBinder, 64

	.type	_ZTS28CSequentialInStreamForBinder,@object # @_ZTS28CSequentialInStreamForBinder
	.globl	_ZTS28CSequentialInStreamForBinder
	.p2align	4
_ZTS28CSequentialInStreamForBinder:
	.asciz	"28CSequentialInStreamForBinder"
	.size	_ZTS28CSequentialInStreamForBinder, 31

	.type	_ZTS19ISequentialInStream,@object # @_ZTS19ISequentialInStream
	.section	.rodata._ZTS19ISequentialInStream,"aG",@progbits,_ZTS19ISequentialInStream,comdat
	.weak	_ZTS19ISequentialInStream
	.p2align	4
_ZTS19ISequentialInStream:
	.asciz	"19ISequentialInStream"
	.size	_ZTS19ISequentialInStream, 22

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI19ISequentialInStream,@object # @_ZTI19ISequentialInStream
	.section	.rodata._ZTI19ISequentialInStream,"aG",@progbits,_ZTI19ISequentialInStream,comdat
	.weak	_ZTI19ISequentialInStream
	.p2align	4
_ZTI19ISequentialInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19ISequentialInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI19ISequentialInStream, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI28CSequentialInStreamForBinder,@object # @_ZTI28CSequentialInStreamForBinder
	.section	.rodata,"a",@progbits
	.globl	_ZTI28CSequentialInStreamForBinder
	.p2align	4
_ZTI28CSequentialInStreamForBinder:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS28CSequentialInStreamForBinder
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI19ISequentialInStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI28CSequentialInStreamForBinder, 56

	.type	_ZTV29CSequentialOutStreamForBinder,@object # @_ZTV29CSequentialOutStreamForBinder
	.globl	_ZTV29CSequentialOutStreamForBinder
	.p2align	3
_ZTV29CSequentialOutStreamForBinder:
	.quad	0
	.quad	_ZTI29CSequentialOutStreamForBinder
	.quad	_ZN29CSequentialOutStreamForBinder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN29CSequentialOutStreamForBinder6AddRefEv
	.quad	_ZN29CSequentialOutStreamForBinder7ReleaseEv
	.quad	_ZN29CSequentialOutStreamForBinderD2Ev
	.quad	_ZN29CSequentialOutStreamForBinderD0Ev
	.quad	_ZN29CSequentialOutStreamForBinder5WriteEPKvjPj
	.size	_ZTV29CSequentialOutStreamForBinder, 64

	.type	_ZTS29CSequentialOutStreamForBinder,@object # @_ZTS29CSequentialOutStreamForBinder
	.globl	_ZTS29CSequentialOutStreamForBinder
	.p2align	4
_ZTS29CSequentialOutStreamForBinder:
	.asciz	"29CSequentialOutStreamForBinder"
	.size	_ZTS29CSequentialOutStreamForBinder, 32

	.type	_ZTS20ISequentialOutStream,@object # @_ZTS20ISequentialOutStream
	.section	.rodata._ZTS20ISequentialOutStream,"aG",@progbits,_ZTS20ISequentialOutStream,comdat
	.weak	_ZTS20ISequentialOutStream
	.p2align	4
_ZTS20ISequentialOutStream:
	.asciz	"20ISequentialOutStream"
	.size	_ZTS20ISequentialOutStream, 23

	.type	_ZTI20ISequentialOutStream,@object # @_ZTI20ISequentialOutStream
	.section	.rodata._ZTI20ISequentialOutStream,"aG",@progbits,_ZTI20ISequentialOutStream,comdat
	.weak	_ZTI20ISequentialOutStream
	.p2align	4
_ZTI20ISequentialOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ISequentialOutStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ISequentialOutStream, 24

	.type	_ZTI29CSequentialOutStreamForBinder,@object # @_ZTI29CSequentialOutStreamForBinder
	.section	.rodata,"a",@progbits
	.globl	_ZTI29CSequentialOutStreamForBinder
	.p2align	4
_ZTI29CSequentialOutStreamForBinder:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS29CSequentialOutStreamForBinder
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI20ISequentialOutStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI29CSequentialOutStreamForBinder, 56


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
