	.text
	.file	"XzDec.bc"
	.globl	Xz_ReadVarInt
	.p2align	4, 0x90
	.type	Xz_ReadVarInt,@function
Xz_ReadVarInt:                          # @Xz_ReadVarInt
	.cfi_startproc
# BB#0:
	movq	$0, (%rdx)
	cmpq	$9, %rsi
	movl	$9, %r8d
	cmovbq	%rsi, %r8
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB0_1:                                # %.thread
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r8, %r11
	jge	.LBB0_4
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	movzbl	(%rdi,%r11), %r10d
	movl	%r10d, %esi
	andl	$127, %esi
	incq	%r11
	shlq	%cl, %rsi
	orq	%rsi, %r9
	movq	%r9, (%rdx)
	addq	$7, %rcx
	testb	%r10b, %r10b
	js	.LBB0_1
# BB#3:
	xorl	%eax, %eax
	testb	%r10b, %r10b
	cmovnel	%r11d, %eax
	cmpl	$1, %r11d
	cmovel	%r11d, %eax
.LBB0_4:                                # %.loopexit
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end0:
	.size	Xz_ReadVarInt, .Lfunc_end0-Xz_ReadVarInt
	.cfi_endproc

	.globl	BraState_Free
	.p2align	4, 0x90
	.type	BraState_Free,@function
BraState_Free:                          # @BraState_Free
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movq	8(%rsi), %rcx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmpq	*%rcx                   # TAILCALL
.Lfunc_end1:
	.size	BraState_Free, .Lfunc_end1-BraState_Free
	.cfi_endproc

	.globl	BraState_SetProps
	.p2align	4, 0x90
	.type	BraState_SetProps,@function
BraState_SetProps:                      # @BraState_SetProps
	.cfi_startproc
# BB#0:
	movl	$0, 28(%rdi)
	movl	$0, 36(%rdi)
	movl	24(%rdi), %ecx
	cmpl	$3, %ecx
	jne	.LBB2_3
# BB#1:
	movl	$4, %eax
	cmpq	$1, %rdx
	jne	.LBB2_12
# BB#2:
	movzbl	(%rsi), %eax
	incl	%eax
	movl	%eax, 32(%rdi)
	jmp	.LBB2_11
.LBB2_3:
	testq	%rdx, %rdx
	je	.LBB2_11
# BB#4:
	movl	$4, %eax
	cmpq	$4, %rdx
	jne	.LBB2_12
# BB#5:
	movl	(%rsi), %edx
	addl	$-5, %ecx
	cmpl	$4, %ecx
	ja	.LBB2_10
# BB#6:
	jmpq	*.LJTI2_0(,%rcx,8)
.LBB2_7:
	testb	$3, %dl
	jne	.LBB2_12
	jmp	.LBB2_10
.LBB2_9:
	testb	$15, %dl
	jne	.LBB2_12
	jmp	.LBB2_10
.LBB2_8:
	testb	$1, %dl
	jne	.LBB2_12
.LBB2_10:                               # %.critedge
	movl	%edx, 36(%rdi)
.LBB2_11:
	xorl	%eax, %eax
.LBB2_12:
	retq
.Lfunc_end2:
	.size	BraState_SetProps, .Lfunc_end2-BraState_SetProps
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_7
	.quad	.LBB2_9
	.quad	.LBB2_7
	.quad	.LBB2_8
	.quad	.LBB2_7

	.text
	.globl	BraState_Init
	.p2align	4, 0x90
	.type	BraState_Init,@function
BraState_Init:                          # @BraState_Init
	.cfi_startproc
# BB#0:
	movl	$0, 40(%rdi)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	movq	$0, 16(%rdi)
	cmpl	$3, 24(%rdi)
	jne	.LBB3_1
# BB#2:
	addq	$44, %rdi
	jmp	Delta_Init              # TAILCALL
.LBB3_1:
	retq
.Lfunc_end3:
	.size	BraState_Init, .Lfunc_end3-BraState_Init
	.cfi_endproc

	.globl	BraState_SetFromMethod
	.p2align	4, 0x90
	.type	BraState_SetFromMethod,@function
BraState_SetFromMethod:                 # @BraState_SetFromMethod
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	-3(%r14), %rcx
	movl	$4, %eax
	cmpq	$6, %rcx
	ja	.LBB4_4
# BB#1:
	movq	$0, (%rbx)
	movl	$16688, %esi            # imm = 0x4130
	movq	%rdx, %rdi
	callq	*(%rdx)
	testq	%rax, %rax
	je	.LBB4_2
# BB#3:
	movl	%r14d, 24(%rax)
	movq	%rax, (%rbx)
	movq	$BraState_Free, 8(%rbx)
	movq	$BraState_SetProps, 16(%rbx)
	movq	$BraState_Init, 24(%rbx)
	movq	$BraState_Code, 32(%rbx)
	xorl	%eax, %eax
	jmp	.LBB4_4
.LBB4_2:
	movl	$2, %eax
.LBB4_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	BraState_SetFromMethod, .Lfunc_end4-BraState_SetFromMethod
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.zero	16
	.text
	.p2align	4, 0x90
	.type	BraState_Code,@function
BraState_Code:                          # @BraState_Code
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi11:
	.cfi_def_cfa_offset 144
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rsi, %rbp
	movq	%rdi, %r12
	movq	152(%rsp), %rsi
	movq	(%rdx), %r15
	movq	(%r8), %rcx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	$0, (%rdx)
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movq	$0, (%r8)
	movl	$0, (%rsi)
	testq	%r15, %r15
	je	.LBB5_22
# BB#1:                                 # %.lr.ph.lr.ph
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	%r9d, 12(%rsp)          # 4-byte Spill
	leaq	300(%r12), %r14
	leaq	16(%r12), %r13
	leaq	44(%r12), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	40(%r12), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	jmp	.LBB5_19
	.p2align	4, 0x90
.LBB5_2:                                #   in Loop: Header=BB5_20 Depth=2
	cmpq	%r15, %rbx
	cmovaq	%r15, %rbx
	addq	%r14, %rsi
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	addq	%rbx, (%r12)
	movq	80(%rsp), %rax          # 8-byte Reload
	addq	%rbx, (%rax)
	addq	%rbx, %rbp
	subq	%rbx, %r15
	jne	.LBB5_20
	jmp	.LBB5_21
	.p2align	4, 0x90
.LBB5_18:                               #   in Loop: Header=BB5_19 Depth=1
	movq	16(%r12), %rax
	movq	%rax, 8(%r12)
	jmp	.LBB5_19
	.p2align	4, 0x90
.LBB5_3:                                #   in Loop: Header=BB5_19 Depth=1
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	16(%r12), %rbx
	subq	%rsi, %rbx
	movq	%rbx, 16(%r12)
	addq	%r14, %rsi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	memmove
	movq	%r14, %rax
	movl	$16384, %r14d           # imm = 0x4000
	subq	%rbx, %r14
	movq	24(%rsp), %r13          # 8-byte Reload
	cmpq	%r13, %r14
	cmovaq	%r13, %r14
	movq	%rax, 16(%rsp)          # 8-byte Spill
	addq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	movq	%rbx, %rdi
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	memcpy
	movq	64(%rsp), %rax          # 8-byte Reload
	addq	%r14, (%rax)
	subq	%r14, %r13
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	16(%r12), %rax
	movq	%r14, 56(%rsp)          # 8-byte Spill
	addq	%r14, %rax
	movq	%rax, 16(%r12)
	je	.LBB5_28
# BB#4:                                 #   in Loop: Header=BB5_19 Depth=1
	movl	24(%r12), %ecx
	addl	$-3, %ecx
	cmpl	$6, %ecx
	movq	40(%rsp), %r13          # 8-byte Reload
	ja	.LBB5_29
# BB#5:                                 #   in Loop: Header=BB5_19 Depth=1
	jmpq	*.LJTI5_0(,%rcx,8)
.LBB5_6:                                #   in Loop: Header=BB5_19 Depth=1
	cmpl	$0, 28(%r12)
	movl	32(%r12), %esi
	movq	16(%rsp), %r14          # 8-byte Reload
	je	.LBB5_14
# BB#7:                                 #   in Loop: Header=BB5_19 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rdx
	movq	%rax, %rcx
	callq	Delta_Encode
	jmp	.LBB5_15
.LBB5_8:                                #   in Loop: Header=BB5_19 Depth=1
	movl	28(%r12), %r8d
	movl	36(%r12), %edx
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	x86_Convert
	jmp	.LBB5_16
.LBB5_9:                                #   in Loop: Header=BB5_19 Depth=1
	movl	28(%r12), %ecx
	movl	36(%r12), %edx
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	PPC_Convert
	jmp	.LBB5_16
.LBB5_10:                               #   in Loop: Header=BB5_19 Depth=1
	movl	28(%r12), %ecx
	movl	36(%r12), %edx
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	IA64_Convert
	jmp	.LBB5_16
.LBB5_11:                               #   in Loop: Header=BB5_19 Depth=1
	movl	28(%r12), %ecx
	movl	36(%r12), %edx
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	ARM_Convert
	jmp	.LBB5_16
.LBB5_12:                               #   in Loop: Header=BB5_19 Depth=1
	movl	28(%r12), %ecx
	movl	36(%r12), %edx
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	ARMT_Convert
	jmp	.LBB5_16
.LBB5_13:                               #   in Loop: Header=BB5_19 Depth=1
	movl	28(%r12), %ecx
	movl	36(%r12), %edx
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	SPARC_Convert
	jmp	.LBB5_16
.LBB5_14:                               #   in Loop: Header=BB5_19 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rdx
	movq	%rax, %rcx
	callq	Delta_Decode
.LBB5_15:                               #   in Loop: Header=BB5_19 Depth=1
	movq	(%r13), %rax
	.p2align	4, 0x90
.LBB5_16:                               #   in Loop: Header=BB5_19 Depth=1
	addq	56(%rsp), %rbx          # 8-byte Folded Reload
	movq	%rax, 8(%r12)
	addl	%eax, 36(%r12)
	testq	%rax, %rax
	jne	.LBB5_19
# BB#17:                                #   in Loop: Header=BB5_19 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jne	.LBB5_18
	jmp	.LBB5_21
	.p2align	4, 0x90
.LBB5_19:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_20 Depth 2
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB5_20:                               #   Parent Loop BB5_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12), %rsi
	movq	8(%r12), %rbx
	subq	%rsi, %rbx
	jne	.LBB5_2
	jmp	.LBB5_3
.LBB5_21:
	movq	152(%rsp), %rdx
	movq	%rdx, %rsi
	movl	12(%rsp), %r9d          # 4-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB5_23
.LBB5_22:                               # %..loopexit122_crit_edge
	leaq	16(%r12), %r13
.LBB5_23:                               # %.loopexit122
	xorl	%eax, %eax
	testl	%r9d, %r9d
	je	.LBB5_27
# BB#24:                                # %.loopexit122
	testq	%rcx, %rcx
	jne	.LBB5_27
# BB#25:                                # %.loopexit122
	movq	(%r12), %rcx
	cmpq	%rcx, (%r13)
	jne	.LBB5_27
# BB#26:
	movl	$1, (%rsi)
.LBB5_27:                               # %.loopexit
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_28:
	movq	152(%rsp), %rdx
	movq	%rdx, %rsi
	movl	12(%rsp), %r9d          # 4-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %r13          # 8-byte Reload
	jmp	.LBB5_23
.LBB5_29:                               # %.loopexit.loopexit
	movl	$4, %eax
	jmp	.LBB5_27
.Lfunc_end5:
	.size	BraState_Code, .Lfunc_end5-BraState_Code
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI5_0:
	.quad	.LBB5_6
	.quad	.LBB5_8
	.quad	.LBB5_9
	.quad	.LBB5_10
	.quad	.LBB5_11
	.quad	.LBB5_12
	.quad	.LBB5_13

	.text
	.globl	MixCoder_Construct
	.p2align	4, 0x90
	.type	MixCoder_Construct,@function
MixCoder_Construct:                     # @MixCoder_Construct
	.cfi_startproc
# BB#0:
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movl	$0, 16(%rdi)
	movq	$0, 112(%rdi)
	movq	$0, 152(%rdi)
	movq	$0, 192(%rdi)
	movq	$0, 232(%rdi)
	retq
.Lfunc_end6:
	.size	MixCoder_Construct, .Lfunc_end6-MixCoder_Construct
	.cfi_endproc

	.globl	MixCoder_Free
	.p2align	4, 0x90
	.type	MixCoder_Free,@function
MixCoder_Free:                          # @MixCoder_Free
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -32
.Lcfi22:
	.cfi_offset %r14, -24
.Lcfi23:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	16(%r14), %eax
	testl	%eax, %eax
	jle	.LBB7_6
# BB#1:                                 # %.lr.ph
	leaq	120(%r14), %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB7_5
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	movq	-8(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB7_5
# BB#4:                                 #   in Loop: Header=BB7_2 Depth=1
	callq	*(%r15)
	movl	16(%r14), %eax
.LBB7_5:                                #   in Loop: Header=BB7_2 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	addq	$40, %r15
	cmpq	%rcx, %rbx
	jl	.LBB7_2
.LBB7_6:                                # %._crit_edge
	movl	$0, 16(%r14)
	movq	8(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB7_7
# BB#8:
	movq	(%r14), %rdi
	movq	8(%rdi), %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.LBB7_7:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	MixCoder_Free, .Lfunc_end7-MixCoder_Free
	.cfi_endproc

	.globl	MixCoder_Init
	.p2align	4, 0x90
	.type	MixCoder_Init,@function
MixCoder_Init:                          # @MixCoder_Init
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -32
.Lcfi28:
	.cfi_offset %r14, -24
.Lcfi29:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	16(%r14), %eax
	cmpl	$2, %eax
	jl	.LBB8_3
# BB#1:                                 # %.lr.ph21.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_2:                                # %.lr.ph21
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, 56(%r14,%rcx,8)
	movq	$0, 32(%r14,%rcx,8)
	movl	$0, 20(%r14,%rcx,4)
	incq	%rcx
	movslq	16(%r14), %rax
	leaq	-1(%rax), %rdx
	cmpq	%rdx, %rcx
	jl	.LBB8_2
.LBB8_3:                                # %.preheader
	testl	%eax, %eax
	jle	.LBB8_6
# BB#4:                                 # %.lr.ph.preheader
	leaq	136(%r14), %rbx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB8_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-24(%rbx), %rdi
	callq	*(%rbx)
	incq	%r15
	movslq	16(%r14), %rax
	addq	$40, %rbx
	cmpq	%rax, %r15
	jl	.LBB8_5
.LBB8_6:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	MixCoder_Init, .Lfunc_end8-MixCoder_Init
	.cfi_endproc

	.globl	MixCoder_SetFromMethod
	.p2align	4, 0x90
	.type	MixCoder_SetFromMethod,@function
MixCoder_SetFromMethod:                 # @MixCoder_SetFromMethod
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 48
.Lcfi35:
	.cfi_offset %rbx, -40
.Lcfi36:
	.cfi_offset %r12, -32
.Lcfi37:
	.cfi_offset %r14, -24
.Lcfi38:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movslq	%esi, %rax
	leaq	(%rax,%rax,4), %r15
	leaq	112(%rbx,%r15,8), %r12
	movq	%r14, 80(%rbx,%rax,8)
	cmpq	$33, %r14
	jne	.LBB9_3
# BB#1:
	movq	(%rbx), %rdi
	movl	$168, %esi
	callq	*(%rdi)
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.LBB9_9
# BB#2:
	movq	$Lzma2State_Free, 120(%rbx,%r15,8)
	movq	$Lzma2State_SetProps, 128(%rbx,%r15,8)
	movq	$Lzma2State_Init, 136(%rbx,%r15,8)
	movq	$Lzma2State_Code, 144(%rbx,%r15,8)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	jmp	.LBB9_7
.LBB9_3:
	movl	$4, %eax
	testl	%esi, %esi
	je	.LBB9_10
# BB#4:
	leaq	-3(%r14), %rcx
	cmpq	$6, %rcx
	ja	.LBB9_10
# BB#5:
	movq	(%rbx), %rdi
	movq	$0, (%r12)
	movl	$16688, %esi            # imm = 0x4130
	callq	*(%rdi)
	testq	%rax, %rax
	je	.LBB9_9
# BB#6:
	movl	%r14d, 24(%rax)
	movq	%rax, (%r12)
	movq	$BraState_Free, 120(%rbx,%r15,8)
	movq	$BraState_SetProps, 128(%rbx,%r15,8)
	movq	$BraState_Init, 136(%rbx,%r15,8)
	movq	$BraState_Code, 144(%rbx,%r15,8)
.LBB9_7:                                # %Lzma2State_SetFromMethod.exit
	xorl	%eax, %eax
	jmp	.LBB9_10
.LBB9_9:
	movl	$2, %eax
.LBB9_10:                               # %Lzma2State_SetFromMethod.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	MixCoder_SetFromMethod, .Lfunc_end9-MixCoder_SetFromMethod
	.cfi_endproc

	.globl	MixCoder_Code
	.p2align	4, 0x90
	.type	MixCoder_Code,@function
MixCoder_Code:                          # @MixCoder_Code
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 144
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movl	%r9d, 24(%rsp)          # 4-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movq	152(%rsp), %rax
	movq	(%rdx), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	(%r8), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	$0, (%rdx)
	movq	%r8, 40(%rsp)           # 8-byte Spill
	movq	$0, (%r8)
	movl	$2, (%rax)
	cmpq	$0, 8(%rbp)
	jne	.LBB10_3
# BB#1:
	movq	(%rbp), %rdi
	movl	$393216, %esi           # imm = 0x60000
	callq	*(%rdi)
	movq	%rax, 8(%rbp)
	testq	%rax, %rax
	je	.LBB10_2
.LBB10_3:
	movl	16(%rbp), %eax
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	movl	144(%rsp), %edx
	cmovnel	%ecx, %edx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movl	$1, %r15d
                                        # implicit-def: %ECX
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	jg	.LBB10_6
	.p2align	4, 0x90
.LBB10_5:
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
.LBB10_26:                              # %._crit_edge
	xorl	%edx, %edx
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	sete	%dl
	addl	%edx, %edx
.LBB10_27:                              # %.loopexit
	testl	%edx, %edx
	jne	.LBB10_28
# BB#33:                                # %.loopexit._crit_edge
	movl	16(%rbp), %eax
	testl	%eax, %eax
	jle	.LBB10_5
.LBB10_6:                               # %.lr.ph.preheader
	xorl	%r12d, %r12d
	movl	$-131072, %r13d         # imm = 0xFFFE0000
	movl	$36, %r14d
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB10_7:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testq	%r12, %r12
	je	.LBB10_8
# BB#9:                                 #   in Loop: Header=BB10_7 Depth=1
	movslq	%r13d, %rcx
	addq	8(%rbp), %rcx
	movq	24(%rbp,%r12,8), %rdx
	addq	%rdx, %rcx
	movq	48(%rbp,%r12,8), %rsi
	subq	%rdx, %rsi
	movq	%rsi, 16(%rsp)
	movl	16(%rbp,%r12,4), %r9d
	jmp	.LBB10_10
	.p2align	4, 0x90
.LBB10_8:                               #   in Loop: Header=BB10_7 Depth=1
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	subq	(%rdx), %rcx
	movq	%rcx, 16(%rsp)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	24(%rsp), %r9d          # 4-byte Reload
.LBB10_10:                              #   in Loop: Header=BB10_7 Depth=1
	decl	%eax
	cmpq	%rax, %r12
	jne	.LBB10_12
# BB#11:                                #   in Loop: Header=BB10_7 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	subq	(%rdx), %rax
	movq	%rbx, %rsi
	jmp	.LBB10_14
	.p2align	4, 0x90
.LBB10_12:                              #   in Loop: Header=BB10_7 Depth=1
	movq	32(%rbp,%r12,8), %rax
	movl	$6, %edx
	cmpq	56(%rbp,%r12,8), %rax
	jne	.LBB10_23
# BB#13:                                #   in Loop: Header=BB10_7 Depth=1
	leal	131072(%r13), %eax
	movslq	%eax, %rsi
	addq	8(%rbp), %rsi
	movl	$131072, %eax           # imm = 0x20000
.LBB10_14:                              #   in Loop: Header=BB10_7 Depth=1
	movq	%rax, 56(%rsp)
	movq	-32(%rbp,%r14,4), %rdi
	leaq	56(%rsp), %rdx
	leaq	16(%rsp), %r8
	leaq	28(%rsp), %rax
	pushq	%rax
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	pushq	88(%rsp)                # 8-byte Folded Reload
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	callq	*(%rbp,%r14,4)
	addq	$16, %rsp
.Lcfi54:
	.cfi_adjust_cfa_offset -16
	movl	28(%rsp), %edx
	testl	%edx, %edx
	cmovel	%edx, %r15d
	movq	16(%rsp), %rcx
	testq	%r12, %r12
	je	.LBB10_15
# BB#16:                                #   in Loop: Header=BB10_7 Depth=1
	addq	%rcx, 24(%rbp,%r12,8)
	jmp	.LBB10_17
	.p2align	4, 0x90
.LBB10_15:                              #   in Loop: Header=BB10_7 Depth=1
	movq	40(%rsp), %rsi          # 8-byte Reload
	addq	%rcx, (%rsi)
	addq	%rcx, 32(%rsp)          # 8-byte Folded Spill
.LBB10_17:                              #   in Loop: Header=BB10_7 Depth=1
	movl	16(%rbp), %edi
	decl	%edi
	movq	56(%rsp), %rsi
	cmpq	%rdi, %r12
	jne	.LBB10_19
# BB#18:                                #   in Loop: Header=BB10_7 Depth=1
	movq	48(%rsp), %rdx          # 8-byte Reload
	addq	%rsi, (%rdx)
	addq	%rsi, %rbx
	testl	%eax, %eax
	jne	.LBB10_21
	jmp	.LBB10_22
	.p2align	4, 0x90
.LBB10_19:                              #   in Loop: Header=BB10_7 Depth=1
	movq	%rsi, 56(%rbp,%r12,8)
	movq	$0, 32(%rbp,%r12,8)
	movl	%edx, 20(%rbp,%r12,4)
	testl	%eax, %eax
	je	.LBB10_22
.LBB10_21:                              #   in Loop: Header=BB10_7 Depth=1
	movl	$1, %edx
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jmp	.LBB10_23
	.p2align	4, 0x90
.LBB10_22:                              #   in Loop: Header=BB10_7 Depth=1
	orq	%rsi, %rcx
	movl	$1, %eax
	movl	8(%rsp), %ecx           # 4-byte Reload
	cmovnel	%eax, %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	xorl	%edx, %edx
.LBB10_23:                              #   in Loop: Header=BB10_7 Depth=1
	movl	%edx, %eax
	andb	$7, %al
	cmpb	$6, %al
	je	.LBB10_25
# BB#24:                                #   in Loop: Header=BB10_7 Depth=1
	testb	%al, %al
	jne	.LBB10_27
.LBB10_25:                              #   in Loop: Header=BB10_7 Depth=1
	incq	%r12
	movslq	16(%rbp), %rax
	addq	$10, %r14
	addl	$131072, %r13d          # imm = 0x20000
	cmpq	%rax, %r12
	jl	.LBB10_7
	jmp	.LBB10_26
.LBB10_28:                              # %.loopexit
	cmpl	$2, %edx
	jne	.LBB10_31
# BB#29:
	xorl	%ecx, %ecx
	testl	%r15d, %r15d
	je	.LBB10_32
# BB#30:
	movq	152(%rsp), %rax
	movl	$1, (%rax)
	jmp	.LBB10_32
.LBB10_31:                              # %.loopexit116.loopexit
	movl	12(%rsp), %ecx          # 4-byte Reload
	jmp	.LBB10_32
.LBB10_2:
	movl	$2, %ecx
.LBB10_32:                              # %.loopexit116
	movl	%ecx, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	MixCoder_Code, .Lfunc_end10-MixCoder_Code
	.cfi_endproc

	.globl	Xz_ParseHeader
	.p2align	4, 0x90
	.type	Xz_ParseHeader,@function
Xz_ParseHeader:                         # @Xz_ParseHeader
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi57:
	.cfi_def_cfa_offset 32
.Lcfi58:
	.cfi_offset %rbx, -24
.Lcfi59:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	leaq	6(%rbx), %rdi
	movzbl	6(%rbx), %eax
	shll	$8, %eax
	movzbl	7(%rbx), %ecx
	orl	%eax, %ecx
	movw	%cx, (%r14)
	movl	$2, %esi
	callq	CrcCalc
	movl	%eax, %ecx
	movl	$17, %eax
	cmpl	8(%rbx), %ecx
	jne	.LBB11_2
# BB#1:
	movzwl	(%r14), %ecx
	xorl	%eax, %eax
	cmpl	$15, %ecx
	seta	%al
	shll	$2, %eax
.LBB11_2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end11:
	.size	Xz_ParseHeader, .Lfunc_end11-Xz_ParseHeader
	.cfi_endproc

	.globl	XzBlock_Parse
	.p2align	4, 0x90
	.type	XzBlock_Parse,@function
XzBlock_Parse:                          # @XzBlock_Parse
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi64:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi66:
	.cfi_def_cfa_offset 96
.Lcfi67:
	.cfi_offset %rbx, -56
.Lcfi68:
	.cfi_offset %r12, -48
.Lcfi69:
	.cfi_offset %r13, -40
.Lcfi70:
	.cfi_offset %r14, -32
.Lcfi71:
	.cfi_offset %r15, -24
.Lcfi72:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r12
	movzbl	(%r13), %ebx
	leaq	(,%rbx,4), %r14
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	CrcCalc
	movl	$16, %r10d
	cmpl	(%r13,%rbx,4), %eax
	jne	.LBB12_32
# BB#1:
	movb	1(%r13), %r8b
	movb	%r8b, 16(%r12)
	movl	$2, %ebx
	testb	$64, %r8b
	jne	.LBB12_2
.LBB12_9:                               # %thread-pre-split
	testb	%r8b, %r8b
	js	.LBB12_11
# BB#10:
	movq	%r14, 8(%rsp)           # 8-byte Spill
.LBB12_16:
	andb	$3, %r8b
	incb	%r8b
	movzbl	%r8b, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
.LBB12_17:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_18 Depth 2
                                        #     Child Loop BB12_22 Depth 2
	movl	%ebx, %r15d
	movl	%r15d, %r9d
	addq	%r13, %r9
	movq	8(%rsp), %r11           # 8-byte Reload
	movl	%r11d, %ecx
	subl	%r15d, %ecx
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	shlq	$5, %r8
	leaq	24(%r12,%r8), %rsi
	movq	$0, 24(%r12,%r8)
	cmpl	$9, %ecx
	movl	$9, %ebp
	cmovbq	%rcx, %rbp
	xorl	%ecx, %ecx
	xorl	%ebx, %ebx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB12_18:                              # %.thread.i114
                                        #   Parent Loop BB12_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbp, %rdi
	jge	.LBB12_32
# BB#19:                                #   in Loop: Header=BB12_18 Depth=2
	movzbl	(%r9,%rdi), %edx
	movl	%edx, %eax
	andl	$127, %eax
	incq	%rdi
	shlq	%cl, %rax
	orq	%rax, %rbx
	movq	%rbx, (%rsi)
	addq	$7, %rcx
	testb	%dl, %dl
	js	.LBB12_18
# BB#20:                                # %Xz_ReadVarInt.exit118
                                        #   in Loop: Header=BB12_17 Depth=1
	movl	%edi, %r14d
	movl	$0, %eax
	cmovel	%eax, %r14d
	cmpl	$1, %edi
	cmovel	%edi, %r14d
	testl	%r14d, %r14d
	je	.LBB12_32
# BB#21:                                #   in Loop: Header=BB12_17 Depth=1
	leal	(%r14,%r15), %ebx
	movl	%ebx, %r9d
	addq	%r13, %r9
	movl	%r11d, %eax
	subl	%ebx, %eax
	cmpl	$9, %eax
	movl	$9, %edi
	cmovbq	%rax, %rdi
	xorl	%ecx, %ecx
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB12_22:                              # %.thread.i108
                                        #   Parent Loop BB12_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rdi, %rsi
	jge	.LBB12_32
# BB#23:                                #   in Loop: Header=BB12_22 Depth=2
	movzbl	(%r9,%rsi), %edx
	movl	%edx, %eax
	andl	$127, %eax
	incq	%rsi
	shlq	%cl, %rax
	orq	%rax, %rbp
	addq	$7, %rcx
	testb	%dl, %dl
	js	.LBB12_22
# BB#24:                                # %Xz_ReadVarInt.exit112
                                        #   in Loop: Header=BB12_17 Depth=1
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movl	%esi, %r15d
	movl	$0, %eax
	cmovel	%eax, %r15d
	cmpl	$1, %esi
	cmovel	%esi, %r15d
	testl	%r15d, %r15d
	je	.LBB12_32
# BB#25:                                #   in Loop: Header=BB12_17 Depth=1
	cmpq	$20, %rbp
	ja	.LBB12_32
# BB#26:                                #   in Loop: Header=BB12_17 Depth=1
	addl	%r15d, %ebx
	movq	8(%rsp), %rax           # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	subl	%ebx, %eax
	cmpq	%rax, %rbp
	ja	.LBB12_32
# BB#27:                                #   in Loop: Header=BB12_17 Depth=1
	movl	%ebp, 32(%r12,%r8)
	leaq	36(%r12,%r8), %rdi
	movl	%ebx, %esi
	addq	%r13, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movl	$16, %r10d
	addl	%ebp, %ebx
	movq	32(%rsp), %rax          # 8-byte Reload
	incq	%rax
	cmpq	24(%rsp), %rax          # 8-byte Folded Reload
	jl	.LBB12_17
# BB#28:                                # %.preheader.preheader
	addl	%r15d, %r14d
	addl	16(%rsp), %r14d         # 4-byte Folded Reload
	addl	%r14d, %ebp
	addq	%rbp, %r13
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB12_29:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%eax, %ebx
	jae	.LBB12_30
# BB#31:                                #   in Loop: Header=BB12_29 Depth=1
	incl	%ebx
	cmpb	$0, (%r13)
	leaq	1(%r13), %r13
	je	.LBB12_29
	jmp	.LBB12_32
.LBB12_2:
	leal	-2(%r14), %eax
	movq	$0, (%r12)
	cmpl	$9, %eax
	movl	$9, %esi
	cmovbq	%rax, %rsi
	xorl	%ecx, %ecx
	movl	$2, %ebx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB12_3:                               # %.thread.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	-2(%rbx), %rax
	cmpq	%rsi, %rax
	jge	.LBB12_32
# BB#4:                                 #   in Loop: Header=BB12_3 Depth=1
	movzbl	(%r13,%rbx), %eax
	movl	%eax, %edi
	andl	$127, %edi
	shlq	%cl, %rdi
	orq	%rdi, %rdx
	movq	%rdx, (%r12)
	addq	$7, %rcx
	incq	%rbx
	testb	%al, %al
	js	.LBB12_3
# BB#5:                                 # %Xz_ReadVarInt.exit
	sete	%sil
	cmpl	$7, %ecx
	setne	%al
	testb	%sil, %al
	jne	.LBB12_32
# BB#6:                                 # %Xz_ReadVarInt.exit
	testl	%ecx, %ecx
	je	.LBB12_32
# BB#7:
	testq	%rdx, %rdx
	je	.LBB12_32
# BB#8:
	addq	%r14, %rdx
	jns	.LBB12_9
	jmp	.LBB12_32
.LBB12_11:
	movl	%ebx, %r9d
	addq	%r13, %r9
	movl	%r14d, %eax
	subl	%ebx, %eax
	movq	$0, 8(%r12)
	cmpl	$9, %eax
	movl	$9, %edi
	cmovbq	%rax, %rdi
	xorl	%ecx, %ecx
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB12_12:                              # %.thread.i120
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rdi, %rsi
	jge	.LBB12_32
# BB#13:                                #   in Loop: Header=BB12_12 Depth=1
	movzbl	(%r9,%rsi), %edx
	movl	%edx, %eax
	andl	$127, %eax
	incq	%rsi
	shlq	%cl, %rax
	orq	%rax, %rbp
	movq	%rbp, 8(%r12)
	addq	$7, %rcx
	testb	%dl, %dl
	js	.LBB12_12
# BB#14:                                # %Xz_ReadVarInt.exit124
	xorl	%ecx, %ecx
	testb	%dl, %dl
	cmovnel	%esi, %ecx
	cmpl	$1, %esi
	cmovel	%esi, %ecx
	testl	%ecx, %ecx
	je	.LBB12_32
# BB#15:
	movq	%r14, 8(%rsp)           # 8-byte Spill
	addl	%ebx, %ecx
	movl	%ecx, %ebx
	jmp	.LBB12_16
.LBB12_30:
	xorl	%r10d, %r10d
.LBB12_32:                              # %Xz_ReadVarInt.exit.thread
	movl	%r10d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	XzBlock_Parse, .Lfunc_end12-XzBlock_Parse
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI13_0:
	.zero	16
	.text
	.globl	XzDec_Init
	.p2align	4, 0x90
	.type	XzDec_Init,@function
XzDec_Init:                             # @XzDec_Init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi76:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi77:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi79:
	.cfi_def_cfa_offset 80
.Lcfi80:
	.cfi_offset %rbx, -56
.Lcfi81:
	.cfi_offset %r12, -48
.Lcfi82:
	.cfi_offset %r13, -40
.Lcfi83:
	.cfi_offset %r14, -32
.Lcfi84:
	.cfi_offset %r15, -24
.Lcfi85:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movb	16(%rsi), %r12b
	andb	$3, %r12b
	movl	%r12d, %eax
	incb	%al
	movzbl	%al, %r14d
	movl	16(%r15), %eax
	cmpl	%eax, %r14d
	jne	.LBB13_10
# BB#1:                                 # %.preheader84
	movzbl	%r12b, %r13d
	movq	80(%r15), %rdi
	movq	%r13, %rdx
	shlq	$5, %rdx
	xorl	%ecx, %ecx
	cmpq	24(%rsi,%rdx), %rdi
	jne	.LBB13_9
# BB#2:
	movl	$1, %ecx
	testb	%r12b, %r12b
	je	.LBB13_9
# BB#3:                                 # %.preheader84.1
	movq	88(%r15), %rdi
	cmpq	-8(%rsi,%rdx), %rdi
	jne	.LBB13_9
# BB#4:
	movl	$2, %ecx
	cmpb	$2, %r12b
	jb	.LBB13_9
# BB#5:                                 # %.preheader84.2
	movq	96(%r15), %rdi
	cmpq	-40(%rsi,%rdx), %rdi
	jne	.LBB13_9
# BB#6:
	movl	$3, %ecx
	cmpb	$3, %r12b
	jne	.LBB13_9
# BB#7:                                 # %.preheader84.3
	movq	104(%r15), %rdi
	cmpq	-72(%rsi,%rdx), %rdi
	jne	.LBB13_9
# BB#8:
	movl	$4, %ecx
.LBB13_9:
	cmpl	%r14d, %ecx
	je	.LBB13_28
.LBB13_10:                              # %.critedge
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	testl	%eax, %eax
	jle	.LBB13_16
# BB#11:                                # %.lr.ph.i
	leaq	120(%r15), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB13_12:                              # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rsi
	testq	%rsi, %rsi
	je	.LBB13_15
# BB#13:                                #   in Loop: Header=BB13_12 Depth=1
	movq	-8(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_15
# BB#14:                                #   in Loop: Header=BB13_12 Depth=1
	callq	*(%rbp)
	movl	16(%r15), %eax
.LBB13_15:                              #   in Loop: Header=BB13_12 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	addq	$40, %rbp
	cmpq	%rcx, %rbx
	jl	.LBB13_12
.LBB13_16:                              # %._crit_edge.i
	movl	$0, 16(%r15)
	movq	8(%r15), %rsi
	testq	%rsi, %rsi
	je	.LBB13_18
# BB#17:
	movq	(%r15), %rdi
	callq	*8(%rdi)
.LBB13_18:                              # %MixCoder_Free.exit
	movl	%r14d, 16(%r15)
	movzbl	%r12b, %r13d
	movq	%r13, %rax
	shlq	$5, %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	24(%rcx,%rax), %rbx
	leaq	112(%r15), %rbp
	movq	$-1, %r14
	movq	%r13, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB13_19:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %r13
	movq	%r13, 88(%r15,%r14,8)
	cmpq	$33, %r13
	jne	.LBB13_22
# BB#20:                                #   in Loop: Header=BB13_19 Depth=1
	movq	(%r15), %rdi
	movl	$168, %esi
	callq	*(%rdi)
	movq	%rax, (%rbp)
	testq	%rax, %rax
	je	.LBB13_38
# BB#21:                                #   in Loop: Header=BB13_19 Depth=1
	movq	$Lzma2State_Free, 8(%rbp)
	movq	$Lzma2State_SetProps, 16(%rbp)
	movq	$Lzma2State_Init, 24(%rbp)
	movq	$Lzma2State_Code, 32(%rbp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	jmp	.LBB13_26
	.p2align	4, 0x90
.LBB13_22:                              #   in Loop: Header=BB13_19 Depth=1
	movl	$4, %r12d
	cmpq	$-1, %r14
	je	.LBB13_40
# BB#23:                                #   in Loop: Header=BB13_19 Depth=1
	leaq	-3(%r13), %rax
	cmpq	$6, %rax
	ja	.LBB13_40
# BB#24:                                #   in Loop: Header=BB13_19 Depth=1
	movq	(%r15), %rdi
	movq	$0, (%rbp)
	movl	$16688, %esi            # imm = 0x4130
	callq	*(%rdi)
	testq	%rax, %rax
	je	.LBB13_38
# BB#25:                                #   in Loop: Header=BB13_19 Depth=1
	movl	%r13d, 24(%rax)
	movq	%rax, (%rbp)
	movq	$BraState_Free, 8(%rbp)
	movq	$BraState_SetProps, 16(%rbp)
	movq	$BraState_Init, 24(%rbp)
	movq	$BraState_Code, 32(%rbp)
.LBB13_26:                              #   in Loop: Header=BB13_19 Depth=1
	movq	16(%rsp), %r13          # 8-byte Reload
	incq	%r14
	addq	$-32, %rbx
	addq	$40, %rbp
	cmpq	%r14, %r13
	jg	.LBB13_19
# BB#27:
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB13_28:                              # %.preheader
	movq	%r13, %rax
	shlq	$5, %rax
	leaq	36(%rsi,%rax), %rbx
	movq	%r15, %rbp
	subq	$-128, %rbp
	movq	$-1, %r14
	.p2align	4, 0x90
.LBB13_29:                              # =>This Inner Loop Header: Depth=1
	movq	-16(%rbp), %rdi
	movl	-4(%rbx), %edx
	movq	(%r15), %rcx
	movq	%rbx, %rsi
	callq	*(%rbp)
	movl	%eax, %r12d
	testl	%r12d, %r12d
	jne	.LBB13_40
# BB#30:                                #   in Loop: Header=BB13_29 Depth=1
	incq	%r14
	addq	$-32, %rbx
	addq	$40, %rbp
	cmpq	%r14, %r13
	jg	.LBB13_29
# BB#31:
	movl	16(%r15), %eax
	cmpl	$2, %eax
	jl	.LBB13_34
# BB#32:                                # %.lr.ph21.i.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB13_33:                              # %.lr.ph21.i
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, 56(%r15,%rcx,8)
	movq	$0, 32(%r15,%rcx,8)
	movl	$0, 20(%r15,%rcx,4)
	incq	%rcx
	movslq	16(%r15), %rax
	leaq	-1(%rax), %rdx
	cmpq	%rdx, %rcx
	jl	.LBB13_33
.LBB13_34:                              # %.preheader.i
	testl	%eax, %eax
	jle	.LBB13_39
# BB#35:                                # %.lr.ph.i77.preheader
	leaq	136(%r15), %rbx
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_36:                              # %.lr.ph.i77
                                        # =>This Inner Loop Header: Depth=1
	movq	-24(%rbx), %rdi
	callq	*(%rbx)
	incq	%rbp
	movslq	16(%r15), %rax
	addq	$40, %rbx
	cmpq	%rax, %rbp
	jl	.LBB13_36
	jmp	.LBB13_40
.LBB13_38:
	movl	$2, %r12d
	jmp	.LBB13_40
.LBB13_39:
	xorl	%r12d, %r12d
.LBB13_40:                              # %MixCoder_Init.exit
	movl	%r12d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	XzDec_Init, .Lfunc_end13-XzDec_Init
	.cfi_endproc

	.globl	XzUnpacker_Create
	.p2align	4, 0x90
	.type	XzUnpacker_Create,@function
XzUnpacker_Create:                      # @XzUnpacker_Create
	.cfi_startproc
# BB#0:
	movq	%rsi, 88(%rdi)
	movq	$0, 96(%rdi)
	movl	$0, 104(%rdi)
	movq	$0, 200(%rdi)
	movq	$0, 240(%rdi)
	movq	$0, 280(%rdi)
	movq	$0, 320(%rdi)
	movl	$0, (%rdi)
	movl	$0, 4(%rdi)
	movq	$0, 72(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end14:
	.size	XzUnpacker_Create, .Lfunc_end14-XzUnpacker_Create
	.cfi_endproc

	.globl	XzUnpacker_Free
	.p2align	4, 0x90
	.type	XzUnpacker_Free,@function
XzUnpacker_Free:                        # @XzUnpacker_Free
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 32
.Lcfi89:
	.cfi_offset %rbx, -32
.Lcfi90:
	.cfi_offset %r14, -24
.Lcfi91:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	104(%r14), %eax
	testl	%eax, %eax
	jle	.LBB15_6
# BB#1:                                 # %.lr.ph.i
	leaq	208(%r14), %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB15_2:                               # =>This Inner Loop Header: Depth=1
	movq	88(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB15_5
# BB#3:                                 #   in Loop: Header=BB15_2 Depth=1
	movq	-8(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB15_5
# BB#4:                                 #   in Loop: Header=BB15_2 Depth=1
	callq	*(%r15)
	movl	104(%r14), %eax
.LBB15_5:                               #   in Loop: Header=BB15_2 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	addq	$40, %r15
	cmpq	%rcx, %rbx
	jl	.LBB15_2
.LBB15_6:                               # %._crit_edge.i
	movl	$0, 104(%r14)
	movq	96(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB15_7
# BB#8:
	movq	88(%r14), %rdi
	movq	8(%rdi), %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.LBB15_7:                               # %MixCoder_Free.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end15:
	.size	XzUnpacker_Free, .Lfunc_end15-XzUnpacker_Free
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI16_0:
	.quad	1                       # 0x1
	.quad	1                       # 0x1
.LCPI16_1:
	.zero	16
	.text
	.globl	XzUnpacker_Code
	.p2align	4, 0x90
	.type	XzUnpacker_Code,@function
XzUnpacker_Code:                        # @XzUnpacker_Code
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi95:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi96:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi98:
	.cfi_def_cfa_offset 320
.Lcfi99:
	.cfi_offset %rbx, -56
.Lcfi100:
	.cfi_offset %r12, -48
.Lcfi101:
	.cfi_offset %r13, -40
.Lcfi102:
	.cfi_offset %r14, -32
.Lcfi103:
	.cfi_offset %r15, -24
.Lcfi104:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
	movq	%r9, 152(%rsp)          # 8-byte Spill
	movq	%r8, %r13
	movq	%rcx, %rbp
	movq	%rsi, %r15
	movq	%rdi, %r12
	movq	320(%rsp), %rax
	movq	(%rdx), %rcx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movq	(%r13), %rbx
	movq	$0, (%rdx)
	movq	$0, (%r13)
	movl	$0, (%rax)
	leaq	88(%r12), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	512(%r12), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	24(%r12), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	632(%r12), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	40(%r12), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movl	$1, %eax
	movd	%rax, %xmm0
	movdqa	%xmm0, 176(%rsp)        # 16-byte Spill
	leaq	774(%r12), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	leaq	768(%r12), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	769(%r12), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	736(%r12), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	360(%r12), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	772(%r12), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	leaq	778(%r12), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	jmp	.LBB16_3
	.p2align	4, 0x90
.LBB16_2:                               #   in Loop: Header=BB16_3 Depth=1
	movq	168(%rsp), %rbp         # 8-byte Reload
	addq	%r15, %rbp
	movq	24(%rsp), %r15          # 8-byte Reload
	addq	%rbx, %r15
	movq	16(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB16_3
	.p2align	4, 0x90
.LBB16_1:                               #   in Loop: Header=BB16_3 Depth=1
	movq	%rbx, %rax
	orq	%r15, %rax
	jne	.LBB16_2
	jmp	.LBB16_57
	.p2align	4, 0x90
.LBB16_3:                               # %.thread317thread-pre-split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_9 Depth 2
	movl	(%r12), %ecx
	movq	(%r13), %rax
	movq	%rbx, %r14
	subq	%rax, %r14
	cmpl	$6, %ecx
	jne	.LBB16_9
# BB#4:                                 #   in Loop: Header=BB16_3 Depth=1
	movq	160(%rsp), %rax         # 8-byte Reload
	subq	(%rdx), %rax
	movq	%rax, 80(%rsp)
	movq	%r14, 72(%rsp)
	orq	%r14, %rax
	je	.LBB16_59
# BB#5:                                 #   in Loop: Header=BB16_3 Depth=1
	movl	$0, %r9d
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%r15, %rsi
	leaq	80(%rsp), %rdx
	movq	%rbp, 168(%rsp)         # 8-byte Spill
	movq	%rbp, %rcx
	leaq	72(%rsp), %r8
	movq	320(%rsp), %rbp
	pushq	%rbp
.Lcfi105:
	.cfi_adjust_cfa_offset 8
	pushq	160(%rsp)               # 8-byte Folded Reload
.Lcfi106:
	.cfi_adjust_cfa_offset 8
	callq	MixCoder_Code
	addq	$16, %rsp
.Lcfi107:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %r14d
	movq	80(%rsp), %rbx
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	XzCheck_Update
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	72(%rsp), %r15
	addq	%r15, (%r13)
	movq	24(%r12), %rax
	addq	%r15, %rax
	movq	%rax, 24(%r12)
	addq	%rbx, (%rdx)
	addq	%rbx, 32(%r12)
	testl	%r14d, %r14d
	jne	.LBB16_58
# BB#6:                                 #   in Loop: Header=BB16_3 Depth=1
	cmpl	$1, (%rbp)
	jne	.LBB16_1
# BB#7:                                 #   in Loop: Header=BB16_3 Depth=1
	movq	%r13, 144(%rsp)         # 8-byte Spill
	movl	20(%r12), %ebp
	addq	%rax, %rbp
	movzwl	16(%r12), %edi
	callq	XzFlags_GetCheckSize
	movl	%eax, %esi
	addq	%rbp, %rsi
	leaq	192(%rsp), %rbp
	movq	%rbp, %rdi
	callq	Xz_WriteVarInt
	movl	%eax, %r14d
	movl	%r14d, %eax
	leaq	192(%rsp,%rax), %rdi
	movq	32(%r12), %rsi
	callq	Xz_WriteVarInt
	movl	%eax, %r13d
	addl	%r14d, %r13d
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rsi
	movq	%r13, %rdx
	callq	Sha256_Update
	movq	8(%rsp), %rdx           # 8-byte Reload
	movdqu	40(%r12), %xmm0
	movd	%r13, %xmm1
	movq	144(%rsp), %r13         # 8-byte Reload
	movdqa	176(%rsp), %xmm2        # 16-byte Reload
	punpcklqdq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	paddq	%xmm0, %xmm2
	movdqu	%xmm2, 40(%r12)
	movq	$7, (%r12)
	movl	$0, 8(%r12)
	jmp	.LBB16_2
	.p2align	4, 0x90
.LBB16_8:                               # %.thread317
                                        #   in Loop: Header=BB16_9 Depth=2
	movq	$0, (%r12)
	movq	(%r13), %rax
	movq	%rbx, %r14
	subq	%rax, %r14
	xorl	%ecx, %ecx
.LBB16_9:                               # %.lr.ph
                                        #   Parent Loop BB16_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%r14, %r14
	je	.LBB16_56
# BB#10:                                #   in Loop: Header=BB16_9 Depth=2
	cmpl	$7, %ecx
	ja	.LBB16_3
# BB#11:                                #   in Loop: Header=BB16_9 Depth=2
	movl	%ecx, %ecx
	jmpq	*.LJTI16_0(,%rcx,8)
.LBB16_12:                              #   in Loop: Header=BB16_9 Depth=2
	cmpb	$0, (%rbp)
	je	.LBB16_15
# BB#13:                                #   in Loop: Header=BB16_9 Depth=2
	testb	$3, 64(%r12)
	je	.LBB16_8
	jmp	.LBB16_14
.LBB16_15:                              #   in Loop: Header=BB16_3 Depth=1
	incq	%rax
	movq	%rax, (%r13)
	incq	%rbp
	incq	64(%r12)
	jmp	.LBB16_3
.LBB16_16:                              #   in Loop: Header=BB16_3 Depth=1
	movl	4(%r12), %eax
	cmpq	$11, %rax
	ja	.LBB16_36
# BB#17:                                #   in Loop: Header=BB16_3 Depth=1
	movb	(%rbp), %cl
	cmpl	$5, %eax
	ja	.LBB16_19
# BB#18:                                #   in Loop: Header=BB16_3 Depth=1
	cmpb	XZ_SIG(%rax), %cl
	jne	.LBB16_14
.LBB16_19:                              # %._crit_edge
                                        #   in Loop: Header=BB16_3 Depth=1
	incq	%rbp
	leal	1(%rax), %esi
	movl	%esi, 4(%r12)
	movb	%cl, 768(%r12,%rax)
	incq	(%r13)
	jmp	.LBB16_3
.LBB16_20:                              #   in Loop: Header=BB16_3 Depth=1
	movl	4(%r12), %ecx
	cmpl	12(%r12), %ecx
	jae	.LBB16_39
# BB#21:                                #   in Loop: Header=BB16_3 Depth=1
	incq	%rax
	movq	%rax, (%r13)
	movb	(%rbp), %al
	incq	%rbp
	leal	1(%rcx), %esi
	movl	%esi, 4(%r12)
	cmpb	768(%r12,%rcx), %al
	je	.LBB16_3
	jmp	.LBB16_55
.LBB16_22:                              #   in Loop: Header=BB16_3 Depth=1
	movl	4(%r12), %ecx
	cmpq	$3, %rcx
	ja	.LBB16_41
# BB#23:                                #   in Loop: Header=BB16_3 Depth=1
	incq	%rax
	movq	%rax, (%r13)
	movb	(%rbp), %al
	incq	%rbp
	leal	1(%rcx), %esi
	movl	%esi, 4(%r12)
	movb	%al, 768(%r12,%rcx)
	jmp	.LBB16_3
.LBB16_24:                              #   in Loop: Header=BB16_3 Depth=1
	movl	4(%r12), %ebx
	movl	$12, %eax
	subl	%ebx, %eax
	cmpq	%r14, %rax
	cmovbel	%eax, %r14d
	leaq	768(%r12,%rbx), %rdi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	memcpy
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	%r14d, %eax
	addl	%ebx, %eax
	movq	16(%rsp), %rbx          # 8-byte Reload
	movl	%eax, 4(%r12)
	addq	%r14, (%r13)
	addq	%r14, %rbp
	cmpl	$12, %eax
	jne	.LBB16_3
# BB#25:                                #   in Loop: Header=BB16_3 Depth=1
	movl	$4, (%r12)
	incq	72(%r12)
	movq	$0, 64(%r12)
	movl	772(%r12), %eax
	leaq	4(,%rax,4), %rax
	movl	$3, %r14d
	cmpq	48(%r12), %rax
	jne	.LBB16_58
# BB#26:                                #   in Loop: Header=BB16_3 Depth=1
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	%rbp, %r15
	movq	%r13, %rbp
	movzwl	16(%r12), %ebx
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r13d
	movl	$6, %esi
	movq	128(%rsp), %rdi         # 8-byte Reload
	callq	CrcCalc
	cmpl	%eax, %r13d
	jne	.LBB16_58
# BB#27:                                #   in Loop: Header=BB16_3 Depth=1
	movzbl	776(%r12), %eax
	shll	$8, %eax
	movzbl	777(%r12), %ecx
	orl	%eax, %ecx
	cmpl	%ecx, %ebx
	movq	%rbp, %r13
	movq	%r15, %rbp
	movq	8(%rsp), %rdx           # 8-byte Reload
	jne	.LBB16_58
# BB#28:                                # %Xz_CheckFooter.exit
                                        #   in Loop: Header=BB16_3 Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	movzwl	(%rax), %eax
	cmpw	XZ_FOOTER_SIG(%rip), %ax
	movq	24(%rsp), %r15          # 8-byte Reload
	jmp	.LBB16_33
.LBB16_29:                              #   in Loop: Header=BB16_3 Depth=1
	movl	4(%r12), %ebx
	testq	%rbx, %rbx
	je	.LBB16_42
# BB#30:                                #   in Loop: Header=BB16_3 Depth=1
	movl	20(%r12), %eax
	cmpl	%ebx, %eax
	jne	.LBB16_47
# BB#31:                                #   in Loop: Header=BB16_3 Depth=1
	movq	104(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	XzBlock_Parse
	movl	%eax, %r14d
	testl	%r14d, %r14d
	jne	.LBB16_58
# BB#32:                                #   in Loop: Header=BB16_3 Depth=1
	movl	$6, (%r12)
	movq	96(%rsp), %rax          # 8-byte Reload
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%rax)
	movzwl	16(%r12), %esi
	andl	$15, %esi
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	XzCheck_Init
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	XzDec_Init
	movl	%eax, %r14d
	testl	%r14d, %r14d
	movq	8(%rsp), %rdx           # 8-byte Reload
.LBB16_33:                              # %Xz_CheckFooter.exit
                                        #   in Loop: Header=BB16_3 Depth=1
	movq	16(%rsp), %rbx          # 8-byte Reload
	je	.LBB16_3
	jmp	.LBB16_58
.LBB16_34:                              #   in Loop: Header=BB16_3 Depth=1
	movl	8(%r12), %ecx
	movl	24(%r12), %esi
	addl	%ecx, %esi
	testb	$3, %sil
	je	.LBB16_45
# BB#35:                                #   in Loop: Header=BB16_3 Depth=1
	incq	%rax
	movq	%rax, (%r13)
	incl	%ecx
	movl	%ecx, 8(%r12)
	cmpb	$0, (%rbp)
	leaq	1(%rbp), %rbp
	je	.LBB16_3
	jmp	.LBB16_55
.LBB16_36:                              #   in Loop: Header=BB16_3 Depth=1
	movzbl	774(%r12), %eax
	shll	$8, %eax
	movzbl	775(%r12), %ecx
	orl	%eax, %ecx
	movw	%cx, 16(%r12)
	movl	$2, %esi
	movq	136(%rsp), %rdi         # 8-byte Reload
	callq	CrcCalc
	cmpl	776(%r12), %eax
	jne	.LBB16_14
# BB#37:                                #   in Loop: Header=BB16_3 Depth=1
	movzwl	16(%r12), %eax
	cmpl	$15, %eax
	ja	.LBB16_61
# BB#38:                                #   in Loop: Header=BB16_3 Depth=1
	movl	$5, (%r12)
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	Sha256_Init
	movl	$0, 4(%r12)
	movq	120(%rsp), %rax         # 8-byte Reload
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%rax)
	movq	8(%rsp), %rdx           # 8-byte Reload
	jmp	.LBB16_3
.LBB16_39:                              #   in Loop: Header=BB16_3 Depth=1
	movdqu	48(%r12), %xmm0
	movd	%xmm0, %rcx
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	movd	%xmm1, %rdx
	movq	%rcx, %rsi
	subq	%rdx, %rsi
	jbe	.LBB16_48
# BB#40:                                #   in Loop: Header=BB16_3 Depth=1
	cmpq	%rsi, %r14
	cmovaq	%rsi, %r14
	movl	80(%r12), %edi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	CrcUpdate
	movl	%eax, 80(%r12)
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	Sha256_Update
	addq	%r14, (%r13)
	addq	%r14, %rbp
	addq	%r14, 56(%r12)
	movq	8(%rsp), %rdx           # 8-byte Reload
	jmp	.LBB16_3
.LBB16_41:                              #   in Loop: Header=BB16_3 Depth=1
	movq	$3, (%r12)
	movl	80(%r12), %eax
	notl	%eax
	cmpl	%eax, 768(%r12)
	je	.LBB16_3
	jmp	.LBB16_55
.LBB16_42:                              #   in Loop: Header=BB16_3 Depth=1
	movb	(%rbp), %al
	movl	$1, 4(%r12)
	movb	%al, 768(%r12)
	incq	(%r13)
	movb	768(%r12), %al
	testb	%al, %al
	jne	.LBB16_44
# BB#43:                                #   in Loop: Header=BB16_3 Depth=1
	movq	40(%r12), %rsi
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	Xz_WriteVarInt
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	incl	%eax
	movl	%eax, 12(%r12)
	movq	%rax, 56(%r12)
	addq	%rax, 48(%r12)
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	callq	Sha256_Final
	movq	%rbx, %rdi
	callq	Sha256_Init
	movl	12(%r12), %edx
	movl	$-1, %edi
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	CrcUpdate
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	%eax, 80(%r12)
	movl	$1, (%r12)
	movb	768(%r12), %al
.LBB16_44:                              #   in Loop: Header=BB16_3 Depth=1
	incq	%rbp
	movzbl	%al, %eax
	leal	4(,%rax,4), %eax
	movl	%eax, 20(%r12)
	movq	16(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB16_3
.LBB16_45:                              #   in Loop: Header=BB16_3 Depth=1
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movzwl	16(%r12), %edi
	callq	XzFlags_GetCheckSize
	movl	%eax, %r15d
	movl	4(%r12), %ebx
	subl	%ebx, %eax
	je	.LBB16_50
# BB#46:                                #   in Loop: Header=BB16_3 Depth=1
	movl	%eax, %ecx
	cmpq	%r14, %rcx
	cmovbel	%eax, %r14d
	leaq	768(%r12,%rbx), %rdi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	memcpy
	movl	%r14d, %eax
	addl	%ebx, %eax
	movl	%eax, 4(%r12)
	addq	%r14, (%r13)
	addq	%r14, %rbp
	jmp	.LBB16_52
.LBB16_47:                              #   in Loop: Header=BB16_3 Depth=1
	subl	%ebx, %eax
	cmpq	%r14, %rax
	cmovbel	%eax, %r14d
	leaq	768(%r12,%rbx), %rdi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	memcpy
	movl	%r14d, %eax
	addl	%ebx, %eax
	movl	%eax, 4(%r12)
	addq	%r14, (%r13)
	addq	%r14, %rbp
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB16_3
.LBB16_48:                              #   in Loop: Header=BB16_3 Depth=1
	testb	$3, %dl
	je	.LBB16_53
# BB#49:                                #   in Loop: Header=BB16_3 Depth=1
	movl	80(%r12), %ecx
	movzbl	(%rbp), %edx
	incq	%rbp
	movzbl	%cl, %esi
	xorl	%edx, %esi
	shrl	$8, %ecx
	xorl	g_CrcTable(,%rsi,4), %ecx
	incq	%rax
	testl	%edx, %edx
	movl	%ecx, 80(%r12)
	movq	%rax, (%r13)
	paddq	.LCPI16_0(%rip), %xmm0
	movdqu	%xmm0, 48(%r12)
	movq	8(%rsp), %rdx           # 8-byte Reload
	je	.LBB16_3
	jmp	.LBB16_55
.LBB16_50:                              #   in Loop: Header=BB16_3 Depth=1
	movq	$5, (%r12)
	movq	48(%rsp), %rdi          # 8-byte Reload
	leaq	192(%rsp), %rbx
	movq	%rbx, %rsi
	callq	XzCheck_Final
	testl	%eax, %eax
	je	.LBB16_52
# BB#51:                                #   in Loop: Header=BB16_3 Depth=1
	movl	%r15d, %edx
	movq	%rbx, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB16_55
.LBB16_52:                              # %.thread
                                        #   in Loop: Header=BB16_3 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB16_3
.LBB16_53:                              #   in Loop: Header=BB16_3 Depth=1
	movl	$2, (%r12)
	addq	$4, %rcx
	movq	%rcx, 48(%r12)
	movl	$0, 4(%r12)
	movq	32(%rsp), %rdi          # 8-byte Reload
	leaq	192(%rsp), %r14
	movq	%r14, %rsi
	callq	Sha256_Final
	movl	$32, %edx
	movq	%r14, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	callq	memcmp
	testl	%eax, %eax
	movq	8(%rsp), %rdx           # 8-byte Reload
	je	.LBB16_3
.LBB16_55:
	movl	$3, %r14d
	jmp	.LBB16_58
.LBB16_56:
	movq	320(%rsp), %rax
	movl	$3, (%rax)
.LBB16_57:                              # %.thread315
	xorl	%r14d, %r14d
	jmp	.LBB16_58
.LBB16_14:
	movl	$17, %r14d
.LBB16_58:                              # %.thread315
	movl	%r14d, %eax
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB16_59:
	movq	320(%rsp), %rax
	movl	$2, (%rax)
	jmp	.LBB16_57
.LBB16_61:
	movl	$4, %r14d
	jmp	.LBB16_58
.Lfunc_end16:
	.size	XzUnpacker_Code, .Lfunc_end16-XzUnpacker_Code
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI16_0:
	.quad	.LBB16_16
	.quad	.LBB16_20
	.quad	.LBB16_22
	.quad	.LBB16_24
	.quad	.LBB16_12
	.quad	.LBB16_29
	.quad	.LBB16_3
	.quad	.LBB16_34

	.text
	.globl	XzUnpacker_IsStreamWasFinished
	.p2align	4, 0x90
	.type	XzUnpacker_IsStreamWasFinished,@function
XzUnpacker_IsStreamWasFinished:         # @XzUnpacker_IsStreamWasFinished
	.cfi_startproc
# BB#0:
	cmpl	$4, (%rdi)
	jne	.LBB17_1
# BB#2:
	testb	$3, 64(%rdi)
	sete	%al
	movzbl	%al, %eax
	retq
.LBB17_1:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.Lfunc_end17:
	.size	XzUnpacker_IsStreamWasFinished, .Lfunc_end17-XzUnpacker_IsStreamWasFinished
	.cfi_endproc

	.p2align	4, 0x90
	.type	Lzma2State_Free,@function
Lzma2State_Free:                        # @Lzma2State_Free
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi109:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi110:
	.cfi_def_cfa_offset 32
.Lcfi111:
	.cfi_offset %rbx, -24
.Lcfi112:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	LzmaDec_Free
	movq	8(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*%rax                   # TAILCALL
.Lfunc_end18:
	.size	Lzma2State_Free, .Lfunc_end18-Lzma2State_Free
	.cfi_endproc

	.p2align	4, 0x90
	.type	Lzma2State_SetProps,@function
Lzma2State_SetProps:                    # @Lzma2State_SetProps
	.cfi_startproc
# BB#0:
	cmpq	$1, %rdx
	jne	.LBB19_1
# BB#2:
	movzbl	(%rsi), %esi
	movq	%rcx, %rdx
	jmp	Lzma2Dec_Allocate       # TAILCALL
.LBB19_1:
	movl	$4, %eax
	retq
.Lfunc_end19:
	.size	Lzma2State_SetProps, .Lfunc_end19-Lzma2State_SetProps
	.cfi_endproc

	.p2align	4, 0x90
	.type	Lzma2State_Init,@function
Lzma2State_Init:                        # @Lzma2State_Init
	.cfi_startproc
# BB#0:
	jmp	Lzma2Dec_Init           # TAILCALL
.Lfunc_end20:
	.size	Lzma2State_Init, .Lfunc_end20-Lzma2State_Init
	.cfi_endproc

	.p2align	4, 0x90
	.type	Lzma2State_Code,@function
Lzma2State_Code:                        # @Lzma2State_Code
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi114:
	.cfi_def_cfa_offset 32
.Lcfi115:
	.cfi_offset %rbx, -16
	movq	40(%rsp), %rbx
	movl	32(%rsp), %r9d
	leaq	12(%rsp), %rax
	movq	%rax, (%rsp)
	callq	Lzma2Dec_DecodeToBuf
	xorl	%ecx, %ecx
	cmpl	$1, 12(%rsp)
	sete	%cl
	movl	%ecx, (%rbx)
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end21:
	.size	Lzma2State_Code, .Lfunc_end21-Lzma2State_Code
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
