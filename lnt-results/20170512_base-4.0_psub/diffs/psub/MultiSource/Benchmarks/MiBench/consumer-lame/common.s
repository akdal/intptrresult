	.text
	.file	"common.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	3                       # 0x3
	.text
	.globl	decode_header
	.p2align	4, 0x90
	.type	decode_header,@function
decode_header:                          # @decode_header
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	$1, %ecx
	testl	$1048576, %esi          # imm = 0x100000
	movl	$1, %r8d
	je	.LBB0_2
# BB#1:
	movl	%esi, %ecx
	shrl	$19, %ecx
	notl	%ecx
	andl	$1, %ecx
	xorl	%r8d, %r8d
.LBB0_2:
	movl	%ecx, 12(%rdi)
	movl	%r8d, 16(%rdi)
	movl	%esi, %eax
	shrl	$17, %eax
	andl	$3, %eax
	movl	$4, %r9d
	subq	%rax, %r9
	movl	%r9d, 24(%rdi)
	movl	%esi, %eax
	shrl	$10, %eax
	andl	$3, %eax
	cmpl	$3, %eax
	je	.LBB0_17
# BB#3:
	testl	%r8d, %r8d
	leal	(%rcx,%rcx,2), %edx
	movl	$6, %r10d
	cmoveq	%rdx, %r10
	addq	%rax, %r10
	movl	%esi, %edx
	shrl	$16, %edx
	notl	%edx
	andl	$1, %edx
	movl	%esi, %r11d
	shrl	$12, %r11d
	andl	$15, %r11d
	testl	%r8d, %r8d
	movl	%r10d, 36(%rdi)
	movl	%edx, 28(%rdi)
	je	.LBB0_5
# BB#4:
	movl	%r11d, 32(%rdi)
.LBB0_5:                                # %._crit_edge
	movl	%r11d, 32(%rdi)
	movl	%esi, %r8d
	shrl	$9, %r8d
	andl	$1, %r8d
	movl	%r8d, 40(%rdi)
	movl	%esi, %edx
	shrl	$8, %edx
	andl	$1, %edx
	movl	%edx, 44(%rdi)
	movd	%esi, %xmm0
	movl	%esi, %edx
	shrl	$6, %edx
	andl	$3, %edx
	movl	%edx, 48(%rdi)
	movq	%rsi, %rax
	shrq	$4, %rax
	movq	%rsi, %rbx
	shrq	$3, %rbx
	shrq	$2, %rsi
	movd	%ebx, %xmm1
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movd	%esi, %xmm0
	movd	%eax, %xmm2
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	pand	.LCPI0_0(%rip), %xmm2
	movdqu	%xmm2, 52(%rdi)
	xorl	%eax, %eax
	cmpl	$3, %edx
	setne	%al
	incl	%eax
	movl	%eax, (%rdi)
	testl	%r11d, %r11d
	je	.LBB0_6
# BB#7:
	andb	$7, %r9b
	cmpb	$3, %r9b
	jne	.LBB0_8
# BB#13:
	movl	%r11d, %eax
	leaq	(%rcx,%rcx,2), %rdx
	shlq	$6, %rdx
	imull	$144000, tabsel_123+128(%rdx,%rax,4), %eax # imm = 0x23280
	movslq	%r10d, %rdx
	movq	freqs(,%rdx,8), %rsi
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rsi
	cltq
	cqto
	idivq	%rsi
	leal	-4(%rax,%r8), %eax
	movl	%eax, 68(%rdi)
	movl	$1, %eax
	popq	%rbx
	retq
.LBB0_6:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	jmp	.LBB0_15
.LBB0_8:
	cmpb	$2, %r9b
	je	.LBB0_12
# BB#9:
	cmpb	$1, %r9b
	jne	.LBB0_14
# BB#10:
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	jmp	.LBB0_11
.LBB0_12:
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
.LBB0_11:
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %eax
	popq	%rbx
	retq
.LBB0_14:
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
.LBB0_15:
	movl	$27, %esi
	movl	$1, %edx
	callq	fwrite
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB0_17:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	decode_header, .Lfunc_end0-decode_header
	.cfi_endproc

	.globl	print_header
	.p2align	4, 0x90
	.type	print_header,@function
print_header:                           # @print_header
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	stderr(%rip), %rdi
	cmpl	$0, 16(%rbx)
	je	.LBB1_2
# BB#1:
	movl	$.L.str.14, %edx
	jmp	.LBB1_3
.LBB1_2:
	cmpl	$0, 12(%rbx)
	movl	$.L.str.15, %eax
	movl	$.L.str.16, %edx
	cmovneq	%rax, %rdx
.LBB1_3:                                # %._crit_edge
	movslq	24(%rbx), %rax
	movq	print_header_compact.layers(,%rax,8), %rcx
	movslq	36(%rbx), %rax
	movq	freqs(,%rax,8), %r8
	movslq	48(%rbx), %rax
	movq	print_header.modes(,%rax,8), %r9
	movl	52(%rbx), %r10d
	movl	68(%rbx), %r11d
	addl	$4, %r11d
	movl	$.L.str.13, %esi
	movl	$0, %eax
	pushq	%r11
.Lcfi4:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi5:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$16, %rsp
.Lcfi6:
	.cfi_adjust_cfa_offset -16
	movq	stderr(%rip), %rdi
	cmpl	$0, 56(%rbx)
	movl	$.L.str.18, %eax
	movl	$.L.str.19, %ecx
	cmovneq	%rax, %rcx
	cmpl	$0, 60(%rbx)
	movl	$.L.str.19, %r8d
	cmovneq	%rax, %r8
	cmpl	$0, 28(%rbx)
	movl	$.L.str.19, %r9d
	cmovneq	%rax, %r9
	movl	(%rbx), %edx
	movl	64(%rbx), %r10d
	subq	$8, %rsp
.Lcfi7:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.17, %esi
	xorl	%eax, %eax
	pushq	%r10
.Lcfi8:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$16, %rsp
.Lcfi9:
	.cfi_adjust_cfa_offset -16
	movq	stderr(%rip), %rdi
	movslq	12(%rbx), %rax
	movslq	24(%rbx), %rcx
	shlq	$6, %rcx
	movslq	32(%rbx), %rdx
	leaq	(%rax,%rax,2), %rax
	shlq	$6, %rax
	addq	%rcx, %rax
	movl	tabsel_123-64(%rax,%rdx,4), %edx
	movl	44(%rbx), %ecx
	movl	$.L.str.20, %esi
	xorl	%eax, %eax
	popq	%rbx
	jmp	fprintf                 # TAILCALL
.Lfunc_end1:
	.size	print_header, .Lfunc_end1-print_header
	.cfi_endproc

	.globl	print_header_compact
	.p2align	4, 0x90
	.type	print_header_compact,@function
print_header_compact:                   # @print_header_compact
	.cfi_startproc
# BB#0:                                 # %._crit_edge
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 16
	movq	%rdi, %r10
	movq	stderr(%rip), %rdi
	movslq	12(%r10), %r8
	testq	%r8, %r8
	movl	$.L.str.15, %edx
	movl	$.L.str.16, %ecx
	cmovneq	%rdx, %rcx
	cmpl	$0, 16(%r10)
	movl	$.L.str.14, %edx
	cmoveq	%rcx, %rdx
	movslq	24(%r10), %rsi
	movq	print_header_compact.layers(,%rsi,8), %rcx
	shlq	$6, %rsi
	movslq	32(%r10), %r9
	leaq	(%r8,%r8,2), %rax
	shlq	$6, %rax
	addq	%rsi, %rax
	movl	tabsel_123-64(%rax,%r9,4), %r8d
	movslq	36(%r10), %rax
	movq	freqs(,%rax,8), %r9
	movslq	48(%r10), %rax
	movq	print_header_compact.modes(,%rax,8), %rax
	movq	%rax, (%rsp)
	movl	$.L.str.25, %esi
	xorl	%eax, %eax
	callq	fprintf
	popq	%rax
	retq
.Lfunc_end2:
	.size	print_header_compact, .Lfunc_end2-print_header_compact
	.cfi_endproc

	.globl	getbits
	.p2align	4, 0x90
	.type	getbits,@function
getbits:                                # @getbits
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	je	.LBB3_1
# BB#2:
	movq	wordpointer(%rip), %rsi
	movzbl	(%rsi), %eax
	shlq	$8, %rax
	movzbl	1(%rsi), %ecx
	orq	%rax, %rcx
	shlq	$8, %rcx
	movzbl	2(%rsi), %eax
	orq	%rcx, %rax
	movl	bitindex(%rip), %edx
	movl	%edx, %ecx
	shlq	%cl, %rax
	andl	$16777215, %eax         # imm = 0xFFFFFF
	addl	%edi, %edx
	movl	$24, %ecx
	subl	%edi, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrq	%cl, %rax
	movl	%edx, %ecx
	sarl	$3, %ecx
	movslq	%ecx, %rcx
	addq	%rsi, %rcx
	movq	%rcx, wordpointer(%rip)
	andl	$7, %edx
	movl	%edx, bitindex(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.LBB3_1:
	xorl	%eax, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end3:
	.size	getbits, .Lfunc_end3-getbits
	.cfi_endproc

	.globl	getbits_fast
	.p2align	4, 0x90
	.type	getbits_fast,@function
getbits_fast:                           # @getbits_fast
	.cfi_startproc
# BB#0:
	movq	wordpointer(%rip), %r8
	movzbl	(%r8), %eax
	shlq	$8, %rax
	movzbl	1(%r8), %esi
	orq	%rax, %rsi
	movl	bitindex(%rip), %edx
	movl	%edx, %ecx
	shlq	%cl, %rsi
	movzwl	%si, %eax
	addl	%edi, %edx
	movl	$16, %ecx
	subl	%edi, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrq	%cl, %rax
	movl	%edx, %ecx
	sarl	$3, %ecx
	movslq	%ecx, %rcx
	addq	%r8, %rcx
	movq	%rcx, wordpointer(%rip)
	andl	$7, %edx
	movl	%edx, bitindex(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end4:
	.size	getbits_fast, .Lfunc_end4-getbits_fast
	.cfi_endproc

	.type	param,@object           # @param
	.data
	.globl	param
	.p2align	2
param:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	param, 16

	.type	tabsel_123,@object      # @tabsel_123
	.globl	tabsel_123
	.p2align	4
tabsel_123:
	.long	0                       # 0x0
	.long	32                      # 0x20
	.long	64                      # 0x40
	.long	96                      # 0x60
	.long	128                     # 0x80
	.long	160                     # 0xa0
	.long	192                     # 0xc0
	.long	224                     # 0xe0
	.long	256                     # 0x100
	.long	288                     # 0x120
	.long	320                     # 0x140
	.long	352                     # 0x160
	.long	384                     # 0x180
	.long	416                     # 0x1a0
	.long	448                     # 0x1c0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	32                      # 0x20
	.long	48                      # 0x30
	.long	56                      # 0x38
	.long	64                      # 0x40
	.long	80                      # 0x50
	.long	96                      # 0x60
	.long	112                     # 0x70
	.long	128                     # 0x80
	.long	160                     # 0xa0
	.long	192                     # 0xc0
	.long	224                     # 0xe0
	.long	256                     # 0x100
	.long	320                     # 0x140
	.long	384                     # 0x180
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	32                      # 0x20
	.long	40                      # 0x28
	.long	48                      # 0x30
	.long	56                      # 0x38
	.long	64                      # 0x40
	.long	80                      # 0x50
	.long	96                      # 0x60
	.long	112                     # 0x70
	.long	128                     # 0x80
	.long	160                     # 0xa0
	.long	192                     # 0xc0
	.long	224                     # 0xe0
	.long	256                     # 0x100
	.long	320                     # 0x140
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	32                      # 0x20
	.long	48                      # 0x30
	.long	56                      # 0x38
	.long	64                      # 0x40
	.long	80                      # 0x50
	.long	96                      # 0x60
	.long	112                     # 0x70
	.long	128                     # 0x80
	.long	144                     # 0x90
	.long	160                     # 0xa0
	.long	176                     # 0xb0
	.long	192                     # 0xc0
	.long	224                     # 0xe0
	.long	256                     # 0x100
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	8                       # 0x8
	.long	16                      # 0x10
	.long	24                      # 0x18
	.long	32                      # 0x20
	.long	40                      # 0x28
	.long	48                      # 0x30
	.long	56                      # 0x38
	.long	64                      # 0x40
	.long	80                      # 0x50
	.long	96                      # 0x60
	.long	112                     # 0x70
	.long	128                     # 0x80
	.long	144                     # 0x90
	.long	160                     # 0xa0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	8                       # 0x8
	.long	16                      # 0x10
	.long	24                      # 0x18
	.long	32                      # 0x20
	.long	40                      # 0x28
	.long	48                      # 0x30
	.long	56                      # 0x38
	.long	64                      # 0x40
	.long	80                      # 0x50
	.long	96                      # 0x60
	.long	112                     # 0x70
	.long	128                     # 0x80
	.long	144                     # 0x90
	.long	160                     # 0xa0
	.long	0                       # 0x0
	.size	tabsel_123, 384

	.type	freqs,@object           # @freqs
	.globl	freqs
	.p2align	4
freqs:
	.quad	44100                   # 0xac44
	.quad	48000                   # 0xbb80
	.quad	32000                   # 0x7d00
	.quad	22050                   # 0x5622
	.quad	24000                   # 0x5dc0
	.quad	16000                   # 0x3e80
	.quad	11025                   # 0x2b11
	.quad	12000                   # 0x2ee0
	.quad	8000                    # 0x1f40
	.size	freqs, 72

	.type	pcm_point,@object       # @pcm_point
	.bss
	.globl	pcm_point
	.p2align	2
pcm_point:
	.long	0                       # 0x0
	.size	pcm_point, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Stream error\n"
	.size	.L.str, 14

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Free format not supported.\n"
	.size	.L.str.1, 28

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"layer=1 Not supported!\n"
	.size	.L.str.2, 24

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"layer=2 Not supported!\n"
	.size	.L.str.3, 24

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Sorry, unknown layer type.\n"
	.size	.L.str.4, 28

	.type	print_header.modes,@object # @print_header.modes
	.section	.rodata,"a",@progbits
	.p2align	4
print_header.modes:
	.quad	.L.str.5
	.quad	.L.str.6
	.quad	.L.str.7
	.quad	.L.str.8
	.size	print_header.modes, 32

	.type	.L.str.5,@object        # @.str.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.5:
	.asciz	"Stereo"
	.size	.L.str.5, 7

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Joint-Stereo"
	.size	.L.str.6, 13

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Dual-Channel"
	.size	.L.str.7, 13

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Single-Channel"
	.size	.L.str.8, 15

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Unknown"
	.size	.L.str.9, 8

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"I"
	.size	.L.str.10, 2

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"II"
	.size	.L.str.11, 3

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"III"
	.size	.L.str.12, 4

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"MPEG %s, Layer: %s, Freq: %ld, mode: %s, modext: %d, BPF : %d\n"
	.size	.L.str.13, 63

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"2.5"
	.size	.L.str.14, 4

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"2.0"
	.size	.L.str.15, 4

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"1.0"
	.size	.L.str.16, 4

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Channels: %d, copyright: %s, original: %s, CRC: %s, emphasis: %d.\n"
	.size	.L.str.17, 67

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Yes"
	.size	.L.str.18, 4

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"No"
	.size	.L.str.19, 3

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Bitrate: %d Kbits/s, Extension value: %d\n"
	.size	.L.str.20, 42

	.type	print_header_compact.modes,@object # @print_header_compact.modes
	.section	.rodata,"a",@progbits
	.p2align	4
print_header_compact.modes:
	.quad	.L.str.21
	.quad	.L.str.22
	.quad	.L.str.23
	.quad	.L.str.24
	.size	print_header_compact.modes, 32

	.type	.L.str.21,@object       # @.str.21
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.21:
	.asciz	"stereo"
	.size	.L.str.21, 7

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"joint-stereo"
	.size	.L.str.22, 13

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"dual-channel"
	.size	.L.str.23, 13

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"mono"
	.size	.L.str.24, 5

	.type	print_header_compact.layers,@object # @print_header_compact.layers
	.section	.rodata,"a",@progbits
	.p2align	4
print_header_compact.layers:
	.quad	.L.str.9
	.quad	.L.str.10
	.quad	.L.str.11
	.quad	.L.str.12
	.size	print_header_compact.layers, 32

	.type	.L.str.25,@object       # @.str.25
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.25:
	.asciz	"MPEG %s layer %s, %d kbit/s, %ld Hz %s\n"
	.size	.L.str.25, 40

	.type	wordpointer,@object     # @wordpointer
	.comm	wordpointer,8,8
	.type	bitindex,@object        # @bitindex
	.comm	bitindex,4,4
	.type	pcm_sample,@object      # @pcm_sample
	.comm	pcm_sample,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
