	.text
	.file	"md_high.bc"
	.globl	encode_one_macroblock_high
	.p2align	4, 0x90
	.type	encode_one_macroblock_high,@function
encode_one_macroblock_high:             # @encode_one_macroblock_high
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$392, %rsp              # imm = 0x188
.Lcfi6:
	.cfi_def_cfa_offset 448
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movw	$-256, 130(%rsp)
	movl	.Lencode_one_macroblock_high.bmcost+16(%rip), %eax
	movl	%eax, 368(%rsp)
	movapd	.Lencode_one_macroblock_high.bmcost(%rip), %xmm0
	movapd	%xmm0, 352(%rsp)
	movl	$0, 136(%rsp)
	movl	$0, 132(%rsp)
	movl	$0, 292(%rsp)
	movl	$0, 288(%rsp)
	movq	img(%rip), %rax
	movl	20(%rax), %r13d
	movl	$0, 144(%rsp)           # 4-byte Folded Spill
	cmpl	$1, %r13d
	sete	%bl
	testl	%r13d, %r13d
	sete	%cl
	cmpl	$3, %r13d
	setne	%dl
	cmpl	$2, %r13d
	sete	%r15b
	je	.LBB0_5
# BB#1:
	xorb	%dl, %cl
	jne	.LBB0_5
# BB#2:
	movl	164(%rax), %ecx
	cmpl	112(%rax), %ecx
	jne	.LBB0_3
# BB#4:
	cmpl	116(%rax), %ecx
	setne	%r15b
	jmp	.LBB0_5
.LBB0_3:
	xorl	%r15d, %r15d
.LBB0_5:
	movq	14224(%rax), %r14
	movl	12(%rax), %eax
	movslq	%eax, %rdi
	imulq	$536, %rdi, %rax        # imm = 0x218
	movq	%rax, 152(%rsp)         # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	FmoGetPreviousMBNr
	testl	%eax, %eax
	js	.LBB0_6
# BB#7:
	movq	img(%rip), %rcx
	cltq
	imulq	$536, %rax, %rax        # imm = 0x218
	addq	14224(%rcx), %rax
	jmp	.LBB0_8
.LBB0_6:
	xorl	%eax, %eax
.LBB0_8:
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movl	144(%rsp), %eax         # 4-byte Reload
	movb	%bl, %al
	movl	%eax, 144(%rsp)         # 4-byte Spill
	movq	152(%rsp), %r12         # 8-byte Reload
	leaq	(%r14,%r12), %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movw	$0, 286(%rsp)
	movq	$0, 384(%rsp)
	movq	input(%rip), %rax
	movl	5244(%rax), %eax
	cmpl	$2, %eax
	je	.LBB0_11
# BB#9:
	cmpl	$1, %eax
	jne	.LBB0_12
# BB#10:
	callq	UMHEX_decide_intrabk_SAD
	jmp	.LBB0_12
.LBB0_11:
	callq	smpUMHEX_decide_intrabk_SAD
.LBB0_12:
	xorl	%ebp, %ebp
	cmpl	$1, %r13d
	sete	%bpl
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	callq	RandomIntra
	movzbl	%r15b, %ecx
	orl	%eax, %ecx
	movl	%ecx, 148(%rsp)         # 4-byte Spill
	movswl	%cx, %ebx
	leaq	160(%rsp), %rsi
	movq	272(%rsp), %rdi         # 8-byte Reload
	movl	%ebx, %edx
	movl	%ebp, %ecx
	callq	init_enc_mb_params
	movl	$0, 416(%r14,%r12)
	movq	cs_cm(%rip), %rdi
	callq	store_coding_state
	movl	%ebx, 316(%rsp)         # 4-byte Spill
	testw	%bx, %bx
	movq	%r14, 256(%rsp)         # 8-byte Spill
	jne	.LBB0_64
# BB#13:
	movw	$1, best_mode(%rip)
	cmpl	$1, %r13d
	jne	.LBB0_15
# BB#14:
	callq	Get_Direct_Motion_Vectors
.LBB0_15:
	movl	%ebp, 304(%rsp)         # 4-byte Spill
	movq	input(%rip), %rax
	cmpl	$1, 4172(%rax)
	jne	.LBB0_17
# BB#16:
	callq	get_initial_mb16x16_cost
.LBB0_17:                               # %.preheader331
	movl	$2147483647, 140(%rsp)  # 4-byte Folded Spill
                                        # imm = 0x7FFFFFFF
	movl	$1, %ebp
	leaq	352(%rsp), %r14
	leaq	130(%rsp), %r15
	.p2align	4, 0x90
.LBB0_18:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_20 Depth 2
	movw	$0, bi_pred_me(%rip)
	movq	img(%rip), %rax
	movw	$0, 14408(%rax,%rbp,2)
	cmpw	$0, 204(%rsp,%rbp,2)
	je	.LBB0_52
# BB#19:                                #   in Loop: Header=BB0_18 Depth=1
	movl	$0, 136(%rsp)
	xorl	%ebx, %ebx
	cmpq	$1, %rbp
	setne	%bl
	incq	%rbx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_20:                               # %.preheader330
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	input(%rip), %rcx
	movl	192(%rsp), %eax
	cmpl	$0, 4172(%rcx)
	je	.LBB0_21
# BB#23:                                #   in Loop: Header=BB0_20 Depth=2
	cvtsi2sdl	%eax, %xmm2
	movsd	lambda_mf_factor(%rip), %xmm1 # xmm1 = mem[0],zero
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_25
# BB#24:                                # %call.sqrt
                                        #   in Loop: Header=BB0_20 Depth=2
	movapd	%xmm1, %xmm0
	movsd	%xmm2, 264(%rsp)        # 8-byte Spill
	callq	sqrt
	movsd	264(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB0_25:                               # %.split
                                        #   in Loop: Header=BB0_20 Depth=2
	mulsd	%xmm0, %xmm2
	cvttsd2si	%xmm2, %eax
	movq	input(%rip), %rcx
	cmpl	$0, 4172(%rcx)
	movl	%eax, 332(%rsp)
	movl	196(%rsp), %eax
	je	.LBB0_22
# BB#26:                                #   in Loop: Header=BB0_20 Depth=2
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movsd	lambda_mf_factor(%rip), %xmm1 # xmm1 = mem[0],zero
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_28
# BB#27:                                # %call.sqrt429
                                        #   in Loop: Header=BB0_20 Depth=2
	movapd	%xmm1, %xmm0
	movsd	%xmm2, 264(%rsp)        # 8-byte Spill
	callq	sqrt
	movsd	264(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB0_28:                               # %.split428
                                        #   in Loop: Header=BB0_20 Depth=2
	mulsd	%xmm0, %xmm2
	cvttsd2si	%xmm2, %eax
	movq	input(%rip), %rcx
	cmpl	$0, 4172(%rcx)
	movl	%eax, 336(%rsp)
	movl	200(%rsp), %eax
	je	.LBB0_32
# BB#29:                                #   in Loop: Header=BB0_20 Depth=2
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movsd	lambda_mf_factor(%rip), %xmm1 # xmm1 = mem[0],zero
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_31
# BB#30:                                # %call.sqrt431
                                        #   in Loop: Header=BB0_20 Depth=2
	movapd	%xmm1, %xmm0
	movsd	%xmm2, 264(%rsp)        # 8-byte Spill
	callq	sqrt
	movsd	264(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB0_31:                               # %.split430
                                        #   in Loop: Header=BB0_20 Depth=2
	mulsd	%xmm0, %xmm2
	cvttsd2si	%xmm2, %eax
	jmp	.LBB0_32
	.p2align	4, 0x90
.LBB0_21:                               # %.thread
                                        #   in Loop: Header=BB0_20 Depth=2
	movl	%eax, 332(%rsp)
	movl	196(%rsp), %eax
.LBB0_22:                               # %.thread376
                                        #   in Loop: Header=BB0_20 Depth=2
	movl	%eax, 336(%rsp)
	movl	200(%rsp), %eax
.LBB0_32:                               #   in Loop: Header=BB0_20 Depth=2
	movl	%eax, 340(%rsp)
	movl	%ebp, %edi
	movl	%r12d, %esi
	leaq	332(%rsp), %rdx
	callq	PartitionMotionSearch
	movl	$2147483647, 352(%rsp)  # imm = 0x7FFFFFFF
	movups	240(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	224(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	160(%rsp), %xmm0
	movups	176(%rsp), %xmm1
	movups	192(%rsp), %xmm2
	movups	208(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movl	$0, %edi
	movl	%r12d, %esi
	movl	%ebp, %edx
	movq	%r14, %rcx
	movq	%r15, %r8
	callq	list_prediction_cost
	cmpl	$1, %r13d
	jne	.LBB0_38
# BB#33:                                #   in Loop: Header=BB0_20 Depth=2
	movl	$2147483647, 356(%rsp)  # imm = 0x7FFFFFFF
	movups	240(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	224(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	160(%rsp), %xmm0
	movups	176(%rsp), %xmm1
	movups	192(%rsp), %xmm2
	movups	208(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movl	$1, %edi
	movl	%r12d, %esi
	movl	%ebp, %edx
	movq	%r14, %rcx
	movq	%r15, %r8
	callq	list_prediction_cost
	movups	240(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	224(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	160(%rsp), %xmm0
	movups	176(%rsp), %xmm1
	movups	192(%rsp), %xmm2
	movups	208(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movl	$2, %edi
	movl	%r12d, %esi
	movl	%ebp, %edx
	movq	%r14, %rcx
	movq	%r15, %r8
	callq	list_prediction_cost
	cmpq	$1, %rbp
	jne	.LBB0_36
# BB#34:                                #   in Loop: Header=BB0_20 Depth=2
	movq	input(%rip), %rax
	movl	2120(%rax), %eax
	testl	%eax, %eax
	je	.LBB0_36
# BB#35:                                #   in Loop: Header=BB0_20 Depth=2
	movups	240(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	224(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	160(%rsp), %xmm0
	movups	176(%rsp), %xmm1
	movups	192(%rsp), %xmm2
	movups	208(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movl	$3, %edi
	movl	$1, %edx
	xorl	%r8d, %r8d
	movl	%r12d, %esi
	movq	%r14, %rcx
	callq	list_prediction_cost
	movups	240(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	224(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	160(%rsp), %xmm0
	movups	176(%rsp), %xmm1
	movups	192(%rsp), %xmm2
	movups	208(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movl	$4, %edi
	movl	$1, %edx
	xorl	%r8d, %r8d
	movl	%r12d, %esi
	movq	%r14, %rcx
	callq	list_prediction_cost
	jmp	.LBB0_37
	.p2align	4, 0x90
.LBB0_38:                               #   in Loop: Header=BB0_20 Depth=2
	movb	$0, 129(%rsp)
	movl	352(%rsp), %eax
	addl	%eax, 136(%rsp)
	xorl	%eax, %eax
	jmp	.LBB0_39
	.p2align	4, 0x90
.LBB0_36:                               #   in Loop: Header=BB0_20 Depth=2
	movabsq	$9223372034707292159, %rax # imm = 0x7FFFFFFF7FFFFFFF
	movq	%rax, 364(%rsp)
.LBB0_37:                               #   in Loop: Header=BB0_20 Depth=2
	movl	$bi_pred_me, %r9d
	movl	%ebp, %edi
	movq	%r14, %rsi
	movq	%r15, %rdx
	leaq	129(%rsp), %rcx
	leaq	136(%rsp), %r8
	callq	determine_prediction_list
	movb	129(%rsp), %al
.LBB0_39:                               #   in Loop: Header=BB0_20 Depth=2
	movswl	234(%rsp), %ecx
	movsbl	130(%rsp), %r8d
	movsbl	131(%rsp), %r9d
	movl	304(%rsp), %edx         # 4-byte Reload
	movl	%edx, (%rsp)
	movsbl	%al, %esi
	movl	%ebp, %edi
	movl	%r12d, %edx
	callq	assign_enc_picture_params
	movzbl	130(%rsp), %eax
	cmpl	$2, %ebp
	je	.LBB0_42
# BB#40:                                #   in Loop: Header=BB0_20 Depth=2
	cmpl	$3, %ebp
	jne	.LBB0_43
# BB#41:                                #   in Loop: Header=BB0_20 Depth=2
	movb	%al, best8x8fwref+14(%r12)
	movb	%al, best8x8fwref+12(%r12)
	movb	129(%rsp), %dl
	movb	%dl, best8x8pdir+14(%r12)
	movb	%dl, best8x8pdir+12(%r12)
	movb	131(%rsp), %sil
	movb	%sil, best8x8bwref+14(%r12)
	movb	%sil, best8x8bwref+12(%r12)
	cmpq	$2, %rbp
	jge	.LBB0_45
	jmp	.LBB0_47
	.p2align	4, 0x90
.LBB0_42:                               #   in Loop: Header=BB0_20 Depth=2
	movb	%al, best8x8fwref+9(%r12,%r12)
	movb	%al, best8x8fwref+8(%r12,%r12)
	movb	129(%rsp), %dl
	movb	%dl, best8x8pdir+9(%r12,%r12)
	movb	%dl, best8x8pdir+8(%r12,%r12)
	movb	131(%rsp), %sil
	movb	%sil, best8x8bwref+9(%r12,%r12)
	movb	%sil, best8x8bwref+8(%r12,%r12)
	cmpq	$2, %rbp
	jge	.LBB0_45
	jmp	.LBB0_47
	.p2align	4, 0x90
.LBB0_43:                               #   in Loop: Header=BB0_20 Depth=2
	movzbl	%al, %ecx
	imull	$16843009, %ecx, %ecx   # imm = 0x1010101
	movl	%ecx, best8x8fwref+4(%rip)
	movzbl	131(%rsp), %esi
	imull	$16843009, %esi, %ecx   # imm = 0x1010101
	movl	%ecx, best8x8bwref+4(%rip)
	movzbl	129(%rsp), %edx
	imull	$16843009, %edx, %ecx   # imm = 0x1010101
	movl	%ecx, best8x8pdir+4(%rip)
	cmpq	$2, %rbp
	jl	.LBB0_47
.LBB0_45:                               #   in Loop: Header=BB0_20 Depth=2
	testq	%r12, %r12
	jne	.LBB0_47
# BB#46:                                #   in Loop: Header=BB0_20 Depth=2
	movsbl	%al, %ecx
	movsbl	%dl, %edx
	movsbl	%sil, %r8d
	xorl	%edi, %edi
	movl	%ebp, %esi
	callq	SetRefAndMotionVectors
.LBB0_47:                               #   in Loop: Header=BB0_20 Depth=2
	incq	%r12
	cmpq	%rbx, %r12
	jl	.LBB0_20
# BB#48:                                #   in Loop: Header=BB0_18 Depth=1
	movl	136(%rsp), %ebx
	cmpl	140(%rsp), %ebx         # 4-byte Folded Reload
	jge	.LBB0_52
# BB#49:                                #   in Loop: Header=BB0_18 Depth=1
	movw	%bp, best_mode(%rip)
	movq	input(%rip), %rax
	cmpl	$1, 4172(%rax)
	jne	.LBB0_51
# BB#50:                                #   in Loop: Header=BB0_18 Depth=1
	movl	%ebx, %edi
	callq	adjust_mb16x16_cost
.LBB0_51:                               #   in Loop: Header=BB0_18 Depth=1
	movl	%ebx, 140(%rsp)         # 4-byte Spill
.LBB0_52:                               #   in Loop: Header=BB0_18 Depth=1
	incq	%rbp
	cmpq	$4, %rbp
	jne	.LBB0_18
# BB#53:
	cmpw	$0, 220(%rsp)
	je	.LBB0_60
# BB#54:
	movl	$1, giRDOpt_B8OnlyFlag(%rip)
	movl	$2147483647, tr8x8(%rip) # imm = 0x7FFFFFFF
	movl	$2147483647, tr4x4(%rip) # imm = 0x7FFFFFFF
	movq	cs_mb(%rip), %rdi
	callq	store_coding_state
	movq	256(%rsp), %rax         # 8-byte Reload
	movq	152(%rsp), %rcx         # 8-byte Reload
	movl	$-1, 468(%rax,%rcx)
	movq	input(%rip), %rax
	cmpl	$0, 5100(%rax)
	je	.LBB0_56
# BB#55:
	movl	$0, tr8x8(%rip)
	movl	$0, cnt_nonz_8x8(%rip)
	movl	$0, cbp_blk8x8(%rip)
	movl	$0, cbp8x8(%rip)
	movl	$0, 132(%rsp)
	movq	cofAC_8x8ts(%rip), %rax
	movq	(%rax), %rdx
	movups	240(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	224(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	160(%rsp), %xmm0
	movups	176(%rsp), %xmm1
	movups	192(%rsp), %xmm2
	movups	208(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	288(%rsp), %r12
	movq	%r12, 112(%rsp)
	leaq	136(%rsp), %rbx
	movq	%rbx, 104(%rsp)
	leaq	132(%rsp), %rax
	movq	%rax, 96(%rsp)
	movl	$1, 120(%rsp)
	movzwl	144(%rsp), %r15d        # 2-byte Folded Reload
	leaq	292(%rsp), %r14
	movl	$tr8x8, %edi
	xorl	%r9d, %r9d
	movq	272(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rsi
	movq	%r14, %rcx
	movl	%r15d, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr8x8+6148(%rip), %eax
	movw	%ax, best8x8mode(%rip)
	movb	tr8x8+6156(%rip), %al
	movb	%al, best8x8pdir+32(%rip)
	movb	tr8x8+6160(%rip), %al
	movb	%al, best8x8fwref+32(%rip)
	movb	tr8x8+6164(%rip), %al
	movb	%al, best8x8bwref+32(%rip)
	movq	cofAC_8x8ts(%rip), %rax
	movq	8(%rax), %rdx
	movups	240(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	224(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	160(%rsp), %xmm0
	movups	176(%rsp), %xmm1
	movups	192(%rsp), %xmm2
	movups	208(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r12, 112(%rsp)
	movq	%rbx, 104(%rsp)
	leaq	132(%rsp), %rax
	movq	%rax, 96(%rsp)
	movl	$1, 120(%rsp)
	movl	$tr8x8, %edi
	movl	$1, %r9d
	movq	%rbp, %rsi
	movq	%r14, %rcx
	movl	%r15d, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr8x8+6150(%rip), %eax
	movw	%ax, best8x8mode+2(%rip)
	movb	tr8x8+6157(%rip), %al
	movb	%al, best8x8pdir+33(%rip)
	movb	tr8x8+6161(%rip), %al
	movb	%al, best8x8fwref+33(%rip)
	movb	tr8x8+6165(%rip), %al
	movb	%al, best8x8bwref+33(%rip)
	movq	cofAC_8x8ts(%rip), %rax
	movq	16(%rax), %rdx
	movups	240(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	224(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	160(%rsp), %xmm0
	movups	176(%rsp), %xmm1
	movups	192(%rsp), %xmm2
	movups	208(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r12, 112(%rsp)
	movq	%rbx, 104(%rsp)
	leaq	132(%rsp), %rax
	movq	%rax, 96(%rsp)
	movl	$1, 120(%rsp)
	movl	$tr8x8, %edi
	movl	$2, %r9d
	movq	%rbp, %rsi
	movq	%r14, %rcx
	movl	%r15d, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr8x8+6152(%rip), %eax
	movw	%ax, best8x8mode+4(%rip)
	movb	tr8x8+6158(%rip), %al
	movb	%al, best8x8pdir+34(%rip)
	movb	tr8x8+6162(%rip), %al
	movb	%al, best8x8fwref+34(%rip)
	movb	tr8x8+6166(%rip), %al
	movb	%al, best8x8bwref+34(%rip)
	movq	cofAC_8x8ts(%rip), %rax
	movq	24(%rax), %rdx
	movups	240(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	224(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	160(%rsp), %xmm0
	movups	176(%rsp), %xmm1
	movups	192(%rsp), %xmm2
	movups	208(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movq	%r12, 112(%rsp)
	movq	%rbx, 104(%rsp)
	leaq	132(%rsp), %rax
	movq	%rax, 96(%rsp)
	movl	$1, 120(%rsp)
	movl	$tr8x8, %edi
	movl	$3, %r9d
	movq	%rbp, %rsi
	movq	%r14, %rcx
	movq	152(%rsp), %r12         # 8-byte Reload
	movq	256(%rsp), %r14         # 8-byte Reload
	movl	%r15d, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr8x8+6154(%rip), %eax
	movw	%ax, best8x8mode+6(%rip)
	movb	tr8x8+6159(%rip), %al
	movb	%al, best8x8pdir+35(%rip)
	movb	tr8x8+6163(%rip), %al
	movb	%al, best8x8fwref+35(%rip)
	movb	tr8x8+6167(%rip), %al
	movb	%al, best8x8bwref+35(%rip)
	movl	cbp8x8(%rip), %eax
	movl	%eax, cbp8_8x8ts(%rip)
	movslq	cbp_blk8x8(%rip), %rax
	movq	%rax, cbp_blk8_8x8ts(%rip)
	movl	cnt_nonz_8x8(%rip), %eax
	movl	%eax, cnt_nonz8_8x8ts(%rip)
	movl	$0, 472(%r14,%r12)
	movq	input(%rip), %rax
	cmpl	$2, 5100(%rax)
	je	.LBB0_57
.LBB0_56:                               # %.loopexit329.loopexit350
	movl	$0, tr4x4(%rip)
	movl	$0, cnt_nonz_8x8(%rip)
	movl	$0, cbp_blk8x8(%rip)
	movl	$0, cbp8x8(%rip)
	movl	$0, 132(%rsp)
	movq	cofAC8x8(%rip), %rax
	movq	(%rax), %rdx
	movups	240(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	224(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	160(%rsp), %xmm0
	movups	176(%rsp), %xmm1
	movups	192(%rsp), %xmm2
	movups	208(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	288(%rsp), %r12
	movq	%r12, 112(%rsp)
	leaq	136(%rsp), %rbx
	movq	%rbx, 104(%rsp)
	leaq	132(%rsp), %rax
	movq	%rax, 96(%rsp)
	movl	$0, 120(%rsp)
	movzwl	144(%rsp), %r15d        # 2-byte Folded Reload
	leaq	292(%rsp), %r14
	movl	$tr4x4, %edi
	xorl	%r9d, %r9d
	movq	272(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rsi
	movq	%r14, %rcx
	movl	%r15d, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr4x4+6148(%rip), %eax
	movw	%ax, best8x8mode(%rip)
	movb	tr4x4+6156(%rip), %al
	movb	%al, best8x8pdir+32(%rip)
	movb	tr4x4+6160(%rip), %al
	movb	%al, best8x8fwref+32(%rip)
	movb	tr4x4+6164(%rip), %al
	movb	%al, best8x8bwref+32(%rip)
	movq	cofAC8x8(%rip), %rax
	movq	8(%rax), %rdx
	movups	240(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	224(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	160(%rsp), %xmm0
	movups	176(%rsp), %xmm1
	movups	192(%rsp), %xmm2
	movups	208(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r12, 112(%rsp)
	movq	%rbx, 104(%rsp)
	leaq	132(%rsp), %rax
	movq	%rax, 96(%rsp)
	movl	$0, 120(%rsp)
	movl	$tr4x4, %edi
	movl	$1, %r9d
	movq	%rbp, %rsi
	movq	%r14, %rcx
	movl	%r15d, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr4x4+6150(%rip), %eax
	movw	%ax, best8x8mode+2(%rip)
	movb	tr4x4+6157(%rip), %al
	movb	%al, best8x8pdir+33(%rip)
	movb	tr4x4+6161(%rip), %al
	movb	%al, best8x8fwref+33(%rip)
	movb	tr4x4+6165(%rip), %al
	movb	%al, best8x8bwref+33(%rip)
	movq	cofAC8x8(%rip), %rax
	movq	16(%rax), %rdx
	movups	240(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	224(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	160(%rsp), %xmm0
	movups	176(%rsp), %xmm1
	movups	192(%rsp), %xmm2
	movups	208(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r12, 112(%rsp)
	movq	%rbx, 104(%rsp)
	leaq	132(%rsp), %rax
	movq	%rax, 96(%rsp)
	movl	$0, 120(%rsp)
	movl	$tr4x4, %edi
	movl	$2, %r9d
	movq	%rbp, %rsi
	movq	%r14, %rcx
	movl	%r15d, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr4x4+6152(%rip), %eax
	movw	%ax, best8x8mode+4(%rip)
	movb	tr4x4+6158(%rip), %al
	movb	%al, best8x8pdir+34(%rip)
	movb	tr4x4+6162(%rip), %al
	movb	%al, best8x8fwref+34(%rip)
	movb	tr4x4+6166(%rip), %al
	movb	%al, best8x8bwref+34(%rip)
	movq	cofAC8x8(%rip), %rax
	movq	24(%rax), %rdx
	movups	240(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	224(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	160(%rsp), %xmm0
	movups	176(%rsp), %xmm1
	movups	192(%rsp), %xmm2
	movups	208(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movq	%r12, 112(%rsp)
	movq	%rbx, 104(%rsp)
	leaq	132(%rsp), %rax
	movq	%rax, 96(%rsp)
	movl	$0, 120(%rsp)
	movl	$tr4x4, %edi
	movl	$3, %r9d
	movq	%rbp, %rsi
	movq	%r14, %rcx
	movq	152(%rsp), %r12         # 8-byte Reload
	movq	256(%rsp), %r14         # 8-byte Reload
	movl	%r15d, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr4x4+6154(%rip), %eax
	movw	%ax, best8x8mode+6(%rip)
	movb	tr4x4+6159(%rip), %al
	movb	%al, best8x8pdir+35(%rip)
	movb	tr4x4+6163(%rip), %al
	movb	%al, best8x8fwref+35(%rip)
	movb	tr4x4+6167(%rip), %al
	movb	%al, best8x8bwref+35(%rip)
.LBB0_57:                               # %.loopexit329
	movq	cs_mb(%rip), %rdi
	callq	reset_coding_state
	movq	input(%rip), %rax
	cmpl	$0, 5116(%rax)
	je	.LBB0_59
# BB#58:
	movq	img(%rip), %rdx
	movl	192(%rdx), %edi
	movl	196(%rdx), %esi
	addq	$12624, %rdx            # imm = 0x3150
	callq	rc_store_diff
.LBB0_59:
	movl	$0, giRDOpt_B8OnlyFlag(%rip)
	cmpl	$3, %r13d
	jne	.LBB0_62
	jmp	.LBB0_63
.LBB0_60:
	movl	$2147483647, tr4x4(%rip) # imm = 0x7FFFFFFF
	movq	256(%rsp), %r14         # 8-byte Reload
	movq	152(%rsp), %r12         # 8-byte Reload
	cmpl	$3, %r13d
	je	.LBB0_63
.LBB0_62:
	testl	%r13d, %r13d
	jne	.LBB0_64
.LBB0_63:
	callq	FindSkipModeMotionVector
.LBB0_64:
	movabsq	$5055640609639927018, %rax # imm = 0x46293E5939A08CEA
	movq	%rax, 344(%rsp)
	movq	input(%rip), %rax
	cmpl	$0, 2120(%rax)
	movq	img(%rip), %rax
	je	.LBB0_66
# BB#65:
	movw	$0, 14410(%rax)
.LBB0_66:                               # %._crit_edge367
	leaq	416(%r14,%r12), %rbp
	xorl	%ebx, %ebx
	cmpl	$0, 15536(%rax)
	je	.LBB0_67
# BB#68:
	leaq	328(%rsp), %rdi
	leaq	324(%rsp), %rsi
	leaq	320(%rsp), %rdx
	callq	IntraChromaPrediction
	movq	input(%rip), %rax
	cmpl	$0, 4176(%rax)
	je	.LBB0_69
# BB#70:
	movups	240(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	224(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	160(%rsp), %xmm0
	movups	176(%rsp), %xmm1
	movups	192(%rsp), %xmm2
	movups	208(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	callq	IntraChromaRDDecision
	movswl	(%rbp), %ebx
	movl	%ebx, %edx
	jmp	.LBB0_71
.LBB0_67:
	xorl	%edx, %edx
	jmp	.LBB0_71
.LBB0_69:
	movl	$3, %edx
.LBB0_71:
	movl	148(%rsp), %r11d        # 4-byte Reload
	movl	%ebx, (%rbp)
	cmpl	%edx, %ebx
	jg	.LBB0_120
# BB#72:                                # %.lr.ph
	movq	256(%rsp), %rax         # 8-byte Reload
	movq	152(%rsp), %rcx         # 8-byte Reload
	leaq	364(%rax,%rcx), %rax
	movq	%rax, 304(%rsp)         # 8-byte Spill
	movq	%rbp, 264(%rsp)         # 8-byte Spill
	movl	%edx, 140(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB0_73:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_87 Depth 2
                                        #       Child Loop BB0_104 Depth 3
                                        #       Child Loop BB0_152 Depth 3
                                        #       Child Loop BB0_156 Depth 3
                                        #       Child Loop BB0_160 Depth 3
	movq	img(%rip), %rax
	cmpl	$0, 15536(%rax)
	je	.LBB0_86
# BB#74:                                #   in Loop: Header=BB0_73 Depth=1
	movq	input(%rip), %rax
	testw	%r11w, %r11w
	je	.LBB0_76
# BB#75:                                #   in Loop: Header=BB0_73 Depth=1
	cmpl	$0, 4048(%rax)
	jne	.LBB0_78
.LBB0_76:                               # %._crit_edge369
                                        #   in Loop: Header=BB0_73 Depth=1
	cmpl	$1, 4072(%rax)
	jne	.LBB0_78
# BB#77:                                #   in Loop: Header=BB0_73 Depth=1
	testl	%ebx, %ebx
	jne	.LBB0_119
	jmp	.LBB0_86
	.p2align	4, 0x90
.LBB0_78:                               #   in Loop: Header=BB0_73 Depth=1
	cmpl	$2, %ebx
	movl	328(%rsp), %eax
	jne	.LBB0_80
# BB#79:                                #   in Loop: Header=BB0_73 Depth=1
	testl	%eax, %eax
	je	.LBB0_119
.LBB0_80:                               #   in Loop: Header=BB0_73 Depth=1
	cmpl	$1, %ebx
	movl	324(%rsp), %ecx
	jne	.LBB0_82
# BB#81:                                #   in Loop: Header=BB0_73 Depth=1
	testl	%ecx, %ecx
	je	.LBB0_119
.LBB0_82:                               #   in Loop: Header=BB0_73 Depth=1
	cmpl	$3, %ebx
	jne	.LBB0_86
# BB#83:                                #   in Loop: Header=BB0_73 Depth=1
	testl	%eax, %eax
	je	.LBB0_119
# BB#84:                                #   in Loop: Header=BB0_73 Depth=1
	testl	%ecx, %ecx
	je	.LBB0_119
# BB#85:                                #   in Loop: Header=BB0_73 Depth=1
	movl	320(%rsp), %eax
	testl	%eax, %eax
	je	.LBB0_119
	.p2align	4, 0x90
.LBB0_86:                               # %.thread324.preheader.preheader
                                        #   in Loop: Header=BB0_73 Depth=1
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	jmp	.LBB0_87
.LBB0_105:                              # %.thread319
                                        #   in Loop: Header=BB0_87 Depth=2
	cmpl	$2, %r14d
	jne	.LBB0_118
# BB#106:                               # %.thread319
                                        #   in Loop: Header=BB0_87 Depth=2
	movl	2120(%r10), %eax
	testl	%eax, %eax
	je	.LBB0_118
# BB#107:                               #   in Loop: Header=BB0_87 Depth=2
	cmpl	$1, %ebx
	jne	.LBB0_118
# BB#108:                               #   in Loop: Header=BB0_87 Depth=2
	movq	img(%rip), %rax
	movw	14408(%rax,%rbx,2), %cx
	movswl	%cx, %edx
	cmpl	$1, %edx
	jle	.LBB0_117
	jmp	.LBB0_118
	.p2align	4, 0x90
.LBB0_87:                               # %.thread324.preheader
                                        #   Parent Loop BB0_73 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_104 Depth 3
                                        #       Child Loop BB0_152 Depth 3
                                        #       Child Loop BB0_156 Depth 3
                                        #       Child Loop BB0_160 Depth 3
	movslq	%r12d, %rax
	movl	mb_mode_table(,%rax,4), %r15d
	movslq	%r15d, %rbx
	cmpl	$1, %r13d
	jne	.LBB0_89
# BB#88:                                # %.thread324.preheader
                                        #   in Loop: Header=BB0_87 Depth=2
	cmpl	$1, %r15d
	jne	.LBB0_89
# BB#90:                                #   in Loop: Header=BB0_87 Depth=2
	movzbl	%r14b, %eax
	imull	$16843009, %eax, %eax   # imm = 0x1010101
	movl	%eax, best8x8pdir+4(%rip)
	movq	input(%rip), %r10
	cmpl	$2, %r14d
	jne	.LBB0_93
# BB#91:                                #   in Loop: Header=BB0_87 Depth=2
	movl	2120(%r10), %ecx
	testl	%ecx, %ecx
	je	.LBB0_93
# BB#92:                                #   in Loop: Header=BB0_87 Depth=2
	movq	img(%rip), %rcx
	movswl	14410(%rcx), %ecx
	xorl	%edx, %edx
	cmpl	$2, %ecx
	setl	%dl
	movl	$2, %r14d
	subl	%edx, %r14d
.LBB0_93:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%ecx, %ecx
	cmpl	$2, %r14d
	setl	%cl
	subl	%ecx, %r12d
	incl	%r14d
	jmp	.LBB0_94
	.p2align	4, 0x90
.LBB0_89:                               # %.thread324.preheader._crit_edge
                                        #   in Loop: Header=BB0_87 Depth=2
	movq	input(%rip), %r10
.LBB0_94:                               #   in Loop: Header=BB0_87 Depth=2
	testw	%r11w, %r11w
	sete	%cl
	cmpl	$0, 2960(%r10)
	setne	%dl
	movswl	best_mode(%rip), %esi
	cmpl	$3, %esi
	jg	.LBB0_98
# BB#95:                                #   in Loop: Header=BB0_87 Depth=2
	cmpl	$10, %ebx
	jl	.LBB0_98
# BB#96:                                #   in Loop: Header=BB0_87 Depth=2
	andb	%dl, %cl
	je	.LBB0_98
# BB#97:                                #   in Loop: Header=BB0_87 Depth=2
	movq	304(%rsp), %rax         # 8-byte Reload
	cmpl	$0, (%rax)
	je	.LBB0_118
.LBB0_98:                               #   in Loop: Header=BB0_87 Depth=2
	cmpl	$1, %r13d
	jne	.LBB0_109
# BB#99:                                #   in Loop: Header=BB0_87 Depth=2
	cmpl	$7, %r15d
	jg	.LBB0_109
# BB#100:                               #   in Loop: Header=BB0_87 Depth=2
	movq	active_pps(%rip), %rcx
	cmpl	$1, 196(%rcx)
	jne	.LBB0_109
# BB#101:                               # %.preheader326
                                        #   in Loop: Header=BB0_87 Depth=2
	movq	wbp_weight(%rip), %r8
	movq	active_sps(%rip), %rcx
	cmpb	$2, best8x8pdir(,%rbx,4)
	jne	.LBB0_150
# BB#102:                               # %.preheader
                                        #   in Loop: Header=BB0_87 Depth=2
	movsbq	best8x8fwref(,%rbx,4), %rdx
	movq	(%r8), %rsi
	movq	8(%r8), %r9
	movq	(%rsi,%rdx,8), %rsi
	movsbq	best8x8bwref(,%rbx,4), %rdi
	movq	(%rsi,%rdi,8), %rsi
	movq	(%r9,%rdx,8), %rdx
	movq	(%rdx,%rdi,8), %rdi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_104:                              #   Parent Loop BB0_73 Depth=1
                                        #     Parent Loop BB0_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rsi,%rdx,4), %ebp
	movl	(%rdi,%rdx,4), %eax
	leal	128(%rbp,%rax), %eax
	cmpl	$255, %eax
	ja	.LBB0_105
# BB#103:                               #   in Loop: Header=BB0_104 Depth=3
	incq	%rdx
	xorl	%ebp, %ebp
	cmpl	$0, 32(%rcx)
	setne	%bpl
	leaq	1(%rbp,%rbp), %rbp
	cmpq	%rbp, %rdx
	jl	.LBB0_104
.LBB0_150:                              # %.loopexit
                                        #   in Loop: Header=BB0_87 Depth=2
	cmpb	$2, best8x8pdir+1(,%rbx,4)
	jne	.LBB0_154
# BB#151:                               # %.preheader.1
                                        #   in Loop: Header=BB0_87 Depth=2
	movsbq	best8x8fwref+1(,%rbx,4), %rax
	movq	(%r8), %rdx
	movq	8(%r8), %rdi
	movq	(%rdx,%rax,8), %rdx
	movsbq	best8x8bwref+1(,%rbx,4), %rbp
	movq	(%rdx,%rbp,8), %rsi
	movq	(%rdi,%rax,8), %rax
	movq	(%rax,%rbp,8), %rdi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_152:                              #   Parent Loop BB0_73 Depth=1
                                        #     Parent Loop BB0_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rsi,%rdx,4), %eax
	movl	(%rdi,%rdx,4), %ebp
	leal	128(%rax,%rbp), %eax
	cmpl	$255, %eax
	ja	.LBB0_105
# BB#153:                               #   in Loop: Header=BB0_152 Depth=3
	incq	%rdx
	xorl	%eax, %eax
	cmpl	$0, 32(%rcx)
	setne	%al
	leaq	1(%rax,%rax), %rax
	cmpq	%rax, %rdx
	jl	.LBB0_152
.LBB0_154:                              # %.loopexit.1
                                        #   in Loop: Header=BB0_87 Depth=2
	cmpb	$2, best8x8pdir+2(,%rbx,4)
	jne	.LBB0_158
# BB#155:                               # %.preheader.2
                                        #   in Loop: Header=BB0_87 Depth=2
	movsbq	best8x8fwref+2(,%rbx,4), %rax
	movq	(%r8), %rdx
	movq	8(%r8), %rdi
	movq	(%rdx,%rax,8), %rdx
	movsbq	best8x8bwref+2(,%rbx,4), %rbp
	movq	(%rdx,%rbp,8), %rsi
	movq	(%rdi,%rax,8), %rax
	movq	(%rax,%rbp,8), %rdi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_156:                              #   Parent Loop BB0_73 Depth=1
                                        #     Parent Loop BB0_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rsi,%rdx,4), %eax
	movl	(%rdi,%rdx,4), %ebp
	leal	128(%rax,%rbp), %eax
	cmpl	$255, %eax
	ja	.LBB0_105
# BB#157:                               #   in Loop: Header=BB0_156 Depth=3
	incq	%rdx
	xorl	%eax, %eax
	cmpl	$0, 32(%rcx)
	setne	%al
	leaq	1(%rax,%rax), %rax
	cmpq	%rax, %rdx
	jl	.LBB0_156
.LBB0_158:                              # %.loopexit.2
                                        #   in Loop: Header=BB0_87 Depth=2
	cmpb	$2, best8x8pdir+3(,%rbx,4)
	jne	.LBB0_109
# BB#159:                               # %.preheader.3
                                        #   in Loop: Header=BB0_87 Depth=2
	movsbq	best8x8fwref+3(,%rbx,4), %rax
	movq	(%r8), %rdx
	movq	8(%r8), %rsi
	movq	(%rdx,%rax,8), %rdx
	movsbq	best8x8bwref+3(,%rbx,4), %rdi
	movq	(%rdx,%rdi,8), %rdx
	movq	(%rsi,%rax,8), %rax
	movq	(%rax,%rdi,8), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_160:                              #   Parent Loop BB0_73 Depth=1
                                        #     Parent Loop BB0_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdx,%rdi,4), %eax
	movl	(%rsi,%rdi,4), %ebp
	leal	128(%rax,%rbp), %eax
	cmpl	$255, %eax
	ja	.LBB0_105
# BB#161:                               #   in Loop: Header=BB0_160 Depth=3
	incq	%rdi
	xorl	%eax, %eax
	cmpl	$0, 32(%rcx)
	setne	%al
	leaq	1(%rax,%rax), %rax
	cmpq	%rax, %rdi
	jl	.LBB0_160
	.p2align	4, 0x90
.LBB0_109:                              # %.loopexit327
                                        #   in Loop: Header=BB0_87 Depth=2
	cmpw	$0, 204(%rsp,%rbx,2)
	je	.LBB0_111
# BB#110:                               #   in Loop: Header=BB0_87 Depth=2
	movups	240(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	224(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	160(%rsp), %xmm0
	movups	176(%rsp), %xmm1
	movups	192(%rsp), %xmm2
	movups	208(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	leaq	286(%rsp), %rax
	movq	%rax, 96(%rsp)
	movzwl	144(%rsp), %r9d         # 2-byte Folded Reload
	xorl	%r8d, %r8d
	movl	%ebx, %edi
	movq	272(%rsp), %rsi         # 8-byte Reload
	leaq	344(%rsp), %rdx
	leaq	384(%rsp), %rcx
	callq	compute_mode_RD_cost
	movl	148(%rsp), %r11d        # 4-byte Reload
	movq	input(%rip), %r10
.LBB0_111:                              #   in Loop: Header=BB0_87 Depth=2
	cmpl	$1, %r13d
	jne	.LBB0_118
# BB#112:                               #   in Loop: Header=BB0_87 Depth=2
	cmpl	$2, %r14d
	jne	.LBB0_118
# BB#113:                               #   in Loop: Header=BB0_87 Depth=2
	movl	2120(%r10), %eax
	testl	%eax, %eax
	je	.LBB0_118
# BB#114:                               #   in Loop: Header=BB0_87 Depth=2
	cmpl	$1, %r15d
	jne	.LBB0_118
# BB#115:                               #   in Loop: Header=BB0_87 Depth=2
	movq	img(%rip), %rax
	movw	14408(%rax,%rbx,2), %cx
	movswl	%cx, %edx
	cmpl	$1, %edx
	jg	.LBB0_118
# BB#116:                               #   in Loop: Header=BB0_87 Depth=2
	cmpb	$2, best8x8pdir+4(%rip)
	jne	.LBB0_118
.LBB0_117:                              #   in Loop: Header=BB0_87 Depth=2
	incl	%ecx
	movw	%cx, 14408(%rax,%rbx,2)
.LBB0_118:                              # %.critedge301
                                        #   in Loop: Header=BB0_87 Depth=2
	incl	%r12d
	cmpl	$9, %r12d
	jl	.LBB0_87
.LBB0_119:                              # %.loopexit328
                                        #   in Loop: Header=BB0_73 Depth=1
	movq	264(%rsp), %rbp         # 8-byte Reload
	movl	(%rbp), %eax
	leal	1(%rax), %ebx
	movl	%ebx, (%rbp)
	movl	140(%rsp), %edx         # 4-byte Reload
	cmpl	%edx, %eax
	jl	.LBB0_73
.LBB0_120:                              # %._crit_edge
	movq	256(%rsp), %rbp         # 8-byte Reload
	movq	152(%rsp), %r14         # 8-byte Reload
	movl	72(%rbp,%r14), %ecx
	addl	$-9, %ecx
	cmpl	$6, %ecx
	sbbb	%r15b, %r15b
	movb	$51, %r12b
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %r12b
	movl	cbp(%rip), %ecx
	movzwl	best_mode(%rip), %eax
	testl	%ecx, %ecx
	sete	%dl
	cmpl	$10, %eax
	setne	%bl
	cmpl	$14, %eax
	je	.LBB0_122
# BB#121:                               # %._crit_edge
	andb	%bl, %dl
	movl	$1, %edx
	je	.LBB0_127
.LBB0_122:
	movzwl	%ax, %eax
	testl	%ecx, %ecx
	je	.LBB0_123
# BB#125:
	cmpl	$14, %eax
	je	.LBB0_126
	jmp	.LBB0_128
.LBB0_123:
	cmpl	$14, %eax
	je	.LBB0_126
# BB#124:
	movq	input(%rip), %rax
	movl	5116(%rax), %eax
	testl	%eax, %eax
	jne	.LBB0_128
.LBB0_126:
	movl	$0, 4(%rbp,%r14)
	movl	496(%rbp,%r14), %eax
	movl	%eax, 8(%rbp,%r14)
	movq	272(%rsp), %rdi         # 8-byte Reload
	callq	set_chroma_qp
	movl	8(%rbp,%r14), %eax
	movq	img(%rip), %rcx
	movl	%eax, 36(%rcx)
	xorl	%edx, %edx
.LBB0_127:                              # %.sink.split
	movl	%edx, 504(%rbp,%r14)
.LBB0_128:
	callq	set_stored_macroblock_parameters
	movq	input(%rip), %rax
	cmpl	$0, 5116(%rax)
	je	.LBB0_130
# BB#129:
	movswl	best_mode(%rip), %esi
	movq	272(%rsp), %rdi         # 8-byte Reload
	callq	update_rc
.LBB0_130:
	leaq	72(%rbp,%r14), %r14
	movq	344(%rsp), %rax
	movq	rdopt(%rip), %rcx
	movq	%rax, (%rcx)
	movq	img(%rip), %rax
	cmpl	$0, 15268(%rax)
	je	.LBB0_140
# BB#131:
	testb	$1, 12(%rax)
	je	.LBB0_140
# BB#132:
	cmpl	$0, (%r14)
	jne	.LBB0_140
# BB#133:
	cmpl	$1, %r13d
	jne	.LBB0_135
# BB#134:
	movq	256(%rsp), %rax         # 8-byte Reload
	movq	152(%rsp), %rcx         # 8-byte Reload
	cmpl	$0, 364(%rax,%rcx)
	jne	.LBB0_140
.LBB0_135:
	movq	296(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 72(%rax)
	je	.LBB0_136
.LBB0_140:
	movq	input(%rip), %rax
	cmpl	$0, 4732(%rax)
	je	.LBB0_142
# BB#141:
	andb	%r12b, %r15b
	andb	$1, %r15b
	movzbl	%r15b, %esi
	movl	316(%rsp), %edi         # 4-byte Reload
	movq	272(%rsp), %rdx         # 8-byte Reload
	callq	update_refresh_map
	movq	input(%rip), %rax
.LBB0_142:
	movl	5244(%rax), %eax
	cmpl	$2, %eax
	je	.LBB0_145
# BB#143:
	cmpl	$1, %eax
	jne	.LBB0_146
# BB#144:
	movswl	best_mode(%rip), %edi
	movswq	234(%rsp), %rax
	movl	listXsize(,%rax,4), %esi
	callq	UMHEX_skip_intrabk_SAD
	jmp	.LBB0_146
.LBB0_145:
	movswl	best_mode(%rip), %edi
	movswq	234(%rsp), %rax
	movl	listXsize(,%rax,4), %esi
	callq	smpUMHEX_skip_intrabk_SAD
.LBB0_146:
	movq	input(%rip), %rax
	cmpl	$0, 272(%rax)
	je	.LBB0_149
# BB#147:
	movq	img(%rip), %rax
	cmpl	$1, 20(%rax)
	ja	.LBB0_149
# BB#148:
	movl	(%r14), %ecx
	addl	$-9, %ecx
	cmpl	$6, %ecx
	sbbb	%dl, %dl
	movb	$51, %bl
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %bl
	andb	%dl, %bl
	andb	$1, %bl
	movzbl	%bl, %ecx
	movq	14240(%rax), %rdx
	movslq	12(%rax), %rax
	movl	%ecx, (%rdx,%rax,4)
.LBB0_149:
	addq	$392, %rsp              # imm = 0x188
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_136:
	cmpl	$1, %r13d
	jne	.LBB0_138
# BB#137:
	movq	296(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 364(%rax)
	jne	.LBB0_140
.LBB0_138:
	callq	field_flag_inference
	movswl	238(%rsp), %ecx
	cmpl	%ecx, %eax
	je	.LBB0_140
# BB#139:
	movq	rdopt(%rip), %rax
	movabsq	$5055640609639927018, %rcx # imm = 0x46293E5939A08CEA
	movq	%rcx, (%rax)
	jmp	.LBB0_140
.Lfunc_end0:
	.size	encode_one_macroblock_high, .Lfunc_end0-encode_one_macroblock_high
	.cfi_endproc

	.type	.Lencode_one_macroblock_high.bmcost,@object # @encode_one_macroblock_high.bmcost
	.section	.rodata,"a",@progbits
	.p2align	4
.Lencode_one_macroblock_high.bmcost:
	.long	2147483647              # 0x7fffffff
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	.Lencode_one_macroblock_high.bmcost, 20

	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	diffy,@object           # @diffy
	.comm	diffy,1024,16
	.type	qp_mbaff,@object        # @qp_mbaff
	.comm	qp_mbaff,16,16
	.type	delta_qp_mbaff,@object  # @delta_qp_mbaff
	.comm	delta_qp_mbaff,16,16
	.type	generic_RC,@object      # @generic_RC
	.comm	generic_RC,8,8
	.type	generic_RC_init,@object # @generic_RC_init
	.comm	generic_RC_init,8,8
	.type	generic_RC_best,@object # @generic_RC_best
	.comm	generic_RC_best,8,8
	.type	McostState,@object      # @McostState
	.comm	McostState,8,8
	.type	SearchState,@object     # @SearchState
	.comm	SearchState,8,8
	.type	fastme_ref_cost,@object # @fastme_ref_cost
	.comm	fastme_ref_cost,8,8
	.type	fastme_l0_cost,@object  # @fastme_l0_cost
	.comm	fastme_l0_cost,8,8
	.type	fastme_l1_cost,@object  # @fastme_l1_cost
	.comm	fastme_l1_cost,8,8
	.type	fastme_l0_cost_bipred,@object # @fastme_l0_cost_bipred
	.comm	fastme_l0_cost_bipred,8,8
	.type	fastme_l1_cost_bipred,@object # @fastme_l1_cost_bipred
	.comm	fastme_l1_cost_bipred,8,8
	.type	bipred_flag,@object     # @bipred_flag
	.comm	bipred_flag,4,4
	.type	fastme_best_cost,@object # @fastme_best_cost
	.comm	fastme_best_cost,8,8
	.type	pred_SAD,@object        # @pred_SAD
	.comm	pred_SAD,4,4
	.type	pred_MV_ref,@object     # @pred_MV_ref
	.comm	pred_MV_ref,8,4
	.type	pred_MV_uplayer,@object # @pred_MV_uplayer
	.comm	pred_MV_uplayer,8,4
	.type	UMHEX_blocktype,@object # @UMHEX_blocktype
	.comm	UMHEX_blocktype,4,4
	.type	predict_point,@object   # @predict_point
	.comm	predict_point,40,16
	.type	SAD_a,@object           # @SAD_a
	.comm	SAD_a,4,4
	.type	SAD_b,@object           # @SAD_b
	.comm	SAD_b,4,4
	.type	SAD_c,@object           # @SAD_c
	.comm	SAD_c,4,4
	.type	SAD_d,@object           # @SAD_d
	.comm	SAD_d,4,4
	.type	Threshold_DSR_MB,@object # @Threshold_DSR_MB
	.comm	Threshold_DSR_MB,32,16
	.type	Bsize,@object           # @Bsize
	.comm	Bsize,32,16
	.type	AlphaFourth_1,@object   # @AlphaFourth_1
	.comm	AlphaFourth_1,32,16
	.type	AlphaFourth_2,@object   # @AlphaFourth_2
	.comm	AlphaFourth_2,32,16
	.type	flag_intra,@object      # @flag_intra
	.comm	flag_intra,8,8
	.type	flag_intra_SAD,@object  # @flag_intra_SAD
	.comm	flag_intra_SAD,4,4
	.type	SymmetricalCrossSearchThreshold1,@object # @SymmetricalCrossSearchThreshold1
	.comm	SymmetricalCrossSearchThreshold1,2,2
	.type	SymmetricalCrossSearchThreshold2,@object # @SymmetricalCrossSearchThreshold2
	.comm	SymmetricalCrossSearchThreshold2,2,2
	.type	ConvergeThreshold,@object # @ConvergeThreshold
	.comm	ConvergeThreshold,2,2
	.type	SubPelThreshold1,@object # @SubPelThreshold1
	.comm	SubPelThreshold1,2,2
	.type	SubPelThreshold3,@object # @SubPelThreshold3
	.comm	SubPelThreshold3,2,2
	.type	smpUMHEX_SearchState,@object # @smpUMHEX_SearchState
	.comm	smpUMHEX_SearchState,8,8
	.type	smpUMHEX_l0_cost,@object # @smpUMHEX_l0_cost
	.comm	smpUMHEX_l0_cost,8,8
	.type	smpUMHEX_l1_cost,@object # @smpUMHEX_l1_cost
	.comm	smpUMHEX_l1_cost,8,8
	.type	smpUMHEX_flag_intra,@object # @smpUMHEX_flag_intra
	.comm	smpUMHEX_flag_intra,8,8
	.type	smpUMHEX_flag_intra_SAD,@object # @smpUMHEX_flag_intra_SAD
	.comm	smpUMHEX_flag_intra_SAD,4,4
	.type	smpUMHEX_pred_SAD_uplayer,@object # @smpUMHEX_pred_SAD_uplayer
	.comm	smpUMHEX_pred_SAD_uplayer,4,4
	.type	smpUMHEX_pred_MV_uplayer_X,@object # @smpUMHEX_pred_MV_uplayer_X
	.comm	smpUMHEX_pred_MV_uplayer_X,2,2
	.type	smpUMHEX_pred_MV_uplayer_Y,@object # @smpUMHEX_pred_MV_uplayer_Y
	.comm	smpUMHEX_pred_MV_uplayer_Y,2,2

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
