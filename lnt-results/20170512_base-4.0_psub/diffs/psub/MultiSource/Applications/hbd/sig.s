	.text
	.file	"sig.bc"
	.globl	_Z8sig2typePc
	.p2align	4, 0x90
	.type	_Z8sig2typePc,@function
_Z8sig2typePc:                          # @_Z8sig2typePc
	.cfi_startproc
# BB#0:
	movsbl	(%rdi), %ecx
	addl	$-40, %ecx
	cmpl	$51, %ecx
	ja	.LBB0_12
# BB#1:
	movl	$1, %eax
	jmpq	*.LJTI0_0(,%rcx,8)
.LBB0_10:
	movl	$11, %eax
	retq
.LBB0_13:
	movl	$2, %eax
.LBB0_14:
	retq
.LBB0_2:
	movl	$7, %eax
	retq
.LBB0_3:
	movl	$6, %eax
	retq
.LBB0_4:
	movl	$4, %eax
	retq
.LBB0_5:
	movl	$5, %eax
	retq
.LBB0_6:
	movl	$8, %eax
	retq
.LBB0_7:
	movl	$3, %eax
	retq
.LBB0_11:
	xorl	%eax, %eax
	retq
.LBB0_8:
	movl	$10, %eax
	retq
.LBB0_9:
	movl	$8, %eax
	retq
.LBB0_12:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rcx
	movl	$.L.str.9, %edi
	movl	$38, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	_Z8sig2typePc, .Lfunc_end0-_Z8sig2typePc
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_10
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_14
	.quad	.LBB0_13
	.quad	.LBB0_2
	.quad	.LBB0_12
	.quad	.LBB0_3
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_4
	.quad	.LBB0_5
	.quad	.LBB0_12
	.quad	.LBB0_6
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_7
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_11
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_8
	.quad	.LBB0_9

	.text
	.globl	_Z12printsignameP9ClassfileP8_IO_FILERPcS3_Pv
	.p2align	4, 0x90
	.type	_Z12printsignameP9ClassfileP8_IO_FILERPcS3_Pv,@function
_Z12printsignameP9ClassfileP8_IO_FILERPcS3_Pv: # @_Z12printsignameP9ClassfileP8_IO_FILERPcS3_Pv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 96
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rcx, %r13
	movq	%rdx, %rbp
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	(%rbp), %rax
	leaq	1(%rax), %r14
	movq	%r14, (%rbp)
	movsbl	(%rax), %ecx
	addl	$-40, %ecx
	cmpl	$51, %ecx
	ja	.LBB1_31
# BB#1:
	jmpq	*.LJTI1_0(,%rcx,8)
.LBB1_2:
	testq	%r15, %r15
	je	.LBB1_41
# BB#3:
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	%r14, 8(%rsp)
	.p2align	4, 0x90
.LBB1_4:                                # =>This Inner Loop Header: Depth=1
	movq	%r14, %rax
	leaq	1(%rax), %r14
	movq	%r14, (%rbp)
	cmpb	$41, (%rax)
	jne	.LBB1_4
# BB#5:
	movl	$.L.str.23, %esi
	movq	%r13, %rdi
	callq	strcmp
	xorl	%r12d, %r12d
	testl	%eax, %eax
	je	.LBB1_25
# BB#6:
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 120(%r15)
	movq	(%rbp), %rsi
	movq	%rax, %rdi
	callq	strcpy
	movq	120(%r15), %rax
	movsbl	(%rax), %ecx
	addl	$-40, %ecx
	cmpl	$51, %ecx
	ja	.LBB1_91
# BB#7:
	movl	$1, %eax
	jmpq	*.LJTI1_1(,%rcx,8)
.LBB1_51:
	movl	$8, %eax
	jmp	.LBB1_52
.LBB1_9:
	xorl	%r12d, %r12d
	movl	$.L.str.10, %esi
	jmp	.LBB1_24
.LBB1_10:
	xorl	%r12d, %r12d
	movl	$.L.str.11, %esi
	jmp	.LBB1_24
.LBB1_11:
	xorl	%r12d, %r12d
	movl	$.L.str.12, %esi
	jmp	.LBB1_24
.LBB1_12:
	xorl	%r12d, %r12d
	movl	$.L.str.13, %esi
	jmp	.LBB1_24
.LBB1_13:
	xorl	%r12d, %r12d
	movl	$.L.str.14, %esi
	jmp	.LBB1_24
.LBB1_14:
	xorl	%r12d, %r12d
	movl	$.L.str.15, %esi
	jmp	.LBB1_24
.LBB1_15:
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	%r14, 8(%rsp)
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB1_16:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rax
	leaq	1(%rax), %rbx
	movq	%rbx, (%rbp)
	cmpb	$59, (%rax)
	jne	.LBB1_16
# BB#17:
	subq	%r14, %rbx
	cmpq	$-2, %rbx
	movq	$-1, %rdi
	cmovgq	%rbx, %rdi
	callq	_Znam
	movq	%rax, %r15
	decq	%rbx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	strncpy
	movq	(%rbp), %rax
	subq	%r14, %rax
	movb	$0, -1(%r15,%rax)
	movq	%r15, 8(%rsp)
	movl	$.L.str.16, %esi
	movl	$10, %edx
	movq	%r15, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB1_36
# BB#18:                                # %.preheader
	movl	$47, %esi
	movq	%r15, %rdi
	jmp	.LBB1_20
	.p2align	4, 0x90
.LBB1_19:                               # %.lr.ph
                                        #   in Loop: Header=BB1_20 Depth=1
	movb	$46, (%rax)
	movl	$47, %esi
	movq	%rax, %rdi
.LBB1_20:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	callq	strchr
	testq	%rax, %rax
	jne	.LBB1_19
	jmp	.LBB1_37
.LBB1_21:
	xorl	%r12d, %r12d
	movl	$.L.str.18, %esi
	jmp	.LBB1_24
.LBB1_22:
	xorl	%r12d, %r12d
	movl	$.L.str.33, %esi
	jmp	.LBB1_24
.LBB1_23:
	xorl	%r12d, %r12d
	movl	$.L.str.19, %esi
.LBB1_24:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r13, %rdx
	callq	fprintf
.LBB1_25:
	movl	%r12d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_26:                               # %.preheader151
	movb	(%r14), %cl
	addb	$-48, %cl
	cmpb	$9, %cl
	ja	.LBB1_33
# BB#27:                                # %.lr.ph155.preheader
	movq	%rbx, %r14
	addq	$2, %rax
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_28:                               # %.lr.ph155
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rbx,%rbx,4), %ecx
	movq	%rax, (%rbp)
	movsbl	-1(%rax), %edx
	leal	-48(%rdx,%rcx,2), %ebx
	movzbl	(%rax), %ecx
	addb	$-48, %cl
	incq	%rax
	cmpb	$10, %cl
	jb	.LBB1_28
# BB#29:                                # %.critedge
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%rbp, %rdx
	movq	%r13, %rcx
	movq	%r15, %r8
	callq	_Z12printsignameP9ClassfileP8_IO_FILERPcS3_Pv
	testl	%ebx, %ebx
	je	.LBB1_34
# BB#30:
	xorl	%r12d, %r12d
	movl	$.L.str.20, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%ebx, %edx
	callq	fprintf
	jmp	.LBB1_25
.LBB1_31:
	movq	stderr(%rip), %rcx
	movl	$.L.str.34, %edi
	movl	$30, %esi
.LBB1_32:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %r12d
	jmp	.LBB1_25
.LBB1_33:                               # %.critedge.thread
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	movq	%r13, %rcx
	movq	%r15, %r8
	callq	_Z12printsignameP9ClassfileP8_IO_FILERPcS3_Pv
	movq	%rbx, %r14
.LBB1_34:
	movl	$.L.str.21, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r14, %rcx
.LBB1_35:
	callq	fwrite
	xorl	%r12d, %r12d
	jmp	.LBB1_25
.LBB1_36:
	addq	$10, %r15
	movq	%r15, 8(%rsp)
.LBB1_37:                               # %.loopexit
	movq	56(%r12), %rbx
	testq	%rbx, %rbx
	je	.LBB1_40
# BB#38:
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	8(%rsp), %r14
	movslq	%ebp, %rdx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB1_40
# BB#39:
	shlq	$32, %rbp
	movabsq	$4294967296, %rax       # imm = 0x100000000
	addq	%rbp, %rax
	sarq	$32, %rax
	addq	%rax, %r14
	movq	%r14, 8(%rsp)
.LBB1_40:                               # %.thread
	movq	8(%rsp), %rdx
	xorl	%r12d, %r12d
	movl	$.L.str.17, %esi
	xorl	%eax, %eax
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rcx
	callq	fprintf
	jmp	.LBB1_25
.LBB1_41:
	movq	stderr(%rip), %rcx
	movl	$.L.str.22, %edi
	movl	$32, %esi
	movl	$1, %edx
	jmp	.LBB1_35
.LBB1_8:
	movl	$11, %eax
	jmp	.LBB1_52
.LBB1_42:
	movl	$2, %eax
	jmp	.LBB1_52
.LBB1_43:
	movl	$7, %eax
	jmp	.LBB1_52
.LBB1_44:
	movl	$6, %eax
	jmp	.LBB1_52
.LBB1_45:
	movl	$4, %eax
	jmp	.LBB1_52
.LBB1_46:
	movl	$5, %eax
	jmp	.LBB1_52
.LBB1_48:
	movl	$3, %eax
	jmp	.LBB1_52
.LBB1_49:
	xorl	%eax, %eax
	jmp	.LBB1_52
.LBB1_50:
	movl	$10, %eax
.LBB1_52:                               # %_Z8sig2typePc.exit149
	movl	%eax, 128(%r15)
	movl	$.L.str.24, %esi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_54
# BB#53:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	movq	%r13, %rcx
	movq	%r15, %r8
	callq	_Z12printsignameP9ClassfileP8_IO_FILERPcS3_Pv
	jmp	.LBB1_55
.LBB1_54:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	64(%rax), %rdi
	movq	%rbx, %rsi
	callq	fputs
.LBB1_55:
	movl	$40, %edi
	movq	%rbx, %rsi
	callq	fputc
	movb	25(%r15), %r13b
	incb	%r13b
	movb	%r13b, 25(%r15)
	movzwl	72(%r15), %ebp
	testw	%bp, %bp
	jne	.LBB1_64
# BB#56:
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movzbl	%r13b, %r14d
	leaq	(,%r14,8), %rbx
	movq	%rbx, %rdi
	callq	_Znam
	movq	%rax, 88(%r15)
	movq	%rbx, %rdi
	callq	_Znam
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rax, 96(%r15)
	leaq	(,%r14,4), %rbx
	movq	%rbx, %rdi
	callq	_Znam
	movq	%rax, 112(%r15)
	movq	%rbx, %rdi
	callq	_Znam
	movq	%rax, 104(%r15)
	testb	%r13b, %r13b
	je	.LBB1_62
# BB#57:                                # %.lr.ph159
	leaq	-1(%r14), %rcx
	movl	$0, -4(%rax,%r14,4)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	$0, -8(%rax,%r14,8)
	movq	88(%r15), %rax
	movq	$0, -8(%rax,%r14,8)
	movq	112(%r15), %rax
	movl	$0, -4(%rax,%r14,4)
	testl	%ecx, %ecx
	je	.LBB1_62
# BB#58:                                # %._crit_edge163.preheader
	testb	$1, %cl
	movq	%rcx, %rax
	je	.LBB1_60
# BB#59:                                # %._crit_edge163.prol
	movq	96(%r15), %rax
	movq	104(%r15), %rdx
	movl	$0, -8(%rdx,%r14,4)
	movq	$0, -16(%rax,%r14,8)
	movq	88(%r15), %rax
	movq	$0, -16(%rax,%r14,8)
	movq	112(%r15), %rax
	movl	$0, -8(%rax,%r14,4)
	leaq	-2(%r14), %rax
.LBB1_60:                               # %._crit_edge163.prol.loopexit
	cmpl	$1, %ecx
	je	.LBB1_62
.LBB1_61:                               # %._crit_edge163
                                        # =>This Inner Loop Header: Depth=1
	movq	96(%r15), %rcx
	movq	104(%r15), %rdx
	movl	$0, -4(%rdx,%rax,4)
	movq	$0, -8(%rcx,%rax,8)
	movq	88(%r15), %rcx
	movq	$0, -8(%rcx,%rax,8)
	movq	112(%r15), %rcx
	movl	$0, -4(%rcx,%rax,4)
	movq	96(%r15), %rcx
	movq	104(%r15), %rdx
	movl	$0, -8(%rdx,%rax,4)
	movq	$0, -16(%rcx,%rax,8)
	movq	88(%r15), %rcx
	movq	$0, -16(%rcx,%rax,8)
	movq	112(%r15), %rcx
	movl	$0, -8(%rcx,%rax,4)
	leaq	-2(%rax), %rax
	testl	%eax, %eax
	jne	.LBB1_61
.LBB1_62:                               # %._crit_edge160
	testb	$8, (%r15)
	movq	24(%rsp), %rbx          # 8-byte Reload
	jne	.LBB1_64
# BB#63:
	movq	88(%r15), %rax
	movq	$.L.str.27, (%rax)
	movq	96(%r15), %rax
	movq	$.L.str.28, (%rax)
	movq	112(%r15), %rax
	movl	$8, (%rax)
	movq	104(%r15), %rax
	movl	$0, (%rax)
.LBB1_64:
	movq	8(%rsp), %rdi
	cmpb	$41, (%rdi)
	je	.LBB1_89
# BB#65:                                # %.lr.ph157
	movzwl	(%r15), %r13d
	shrl	$3, %r13d
	notl	%r13d
	andl	$1, %r13d
	jmp	.LBB1_67
	.p2align	4, 0x90
.LBB1_66:                               # %.backedge._crit_edge
                                        #   in Loop: Header=BB1_67 Depth=1
	incl	%r13d
	movzwl	72(%r15), %ebp
.LBB1_67:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, %r14
	testw	%bp, %bp
	je	.LBB1_70
# BB#68:                                #   in Loop: Header=BB1_67 Depth=1
	movq	96(%r15), %rax
	movslq	%r13d, %rbx
	movq	(%rax,%rbx,8), %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB1_90
# BB#69:                                #   in Loop: Header=BB1_67 Depth=1
	movq	88(%r15), %rax
	movq	(%rax,%rbx,8), %rcx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rbx
	movq	%rbx, %rsi
	leaq	8(%rsp), %rdx
	movq	%r15, %r8
	callq	_Z12printsignameP9ClassfileP8_IO_FILERPcS3_Pv
	jmp	.LBB1_84
.LBB1_70:                               #   in Loop: Header=BB1_67 Depth=1
	movl	$6, %edi
	callq	_Znam
	movq	88(%r15), %rcx
	movslq	%r13d, %rbp
	movq	%rax, (%rcx,%rbp,8)
	movq	88(%r15), %rax
	movq	(%rax,%rbp,8), %rdi
	movl	$.L.str.30, %esi
	xorl	%eax, %eax
	movl	%r13d, %edx
	callq	sprintf
	movq	8(%rsp), %rbx
	movq	88(%r15), %rax
	movq	(%rax,%rbp,8), %rcx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rsi
	leaq	8(%rsp), %rdx
	movq	%r15, %r8
	callq	_Z12printsignameP9ClassfileP8_IO_FILERPcS3_Pv
	movq	8(%rsp), %rax
	subq	%rbx, %rax
	cmpq	$-1, %rax
	leaq	1(%rax), %rdi
	movq	$-1, %rax
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	96(%r15), %rcx
	movq	%rax, (%rcx,%rbp,8)
	movq	96(%r15), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	8(%rsp), %rdx
	subq	%rbx, %rdx
	movq	%rbx, %rsi
	callq	strncpy
	movq	96(%r15), %rax
	movq	(%rax,%rbp,8), %rax
	movq	8(%rsp), %rcx
	subq	%rbx, %rcx
	movb	$0, (%rax,%rcx)
	movq	96(%r15), %rax
	movq	(%rax,%rbp,8), %rax
	movsbl	(%rax), %ecx
	addl	$-40, %ecx
	cmpl	$51, %ecx
	ja	.LBB1_91
# BB#71:                                #   in Loop: Header=BB1_67 Depth=1
	movl	$1, %eax
	jmpq	*.LJTI1_2(,%rcx,8)
.LBB1_82:                               #   in Loop: Header=BB1_67 Depth=1
	movl	$8, %eax
	jmp	.LBB1_83
.LBB1_72:                               #   in Loop: Header=BB1_67 Depth=1
	movl	$11, %eax
	jmp	.LBB1_83
.LBB1_73:                               #   in Loop: Header=BB1_67 Depth=1
	movl	$2, %eax
	jmp	.LBB1_83
.LBB1_74:                               #   in Loop: Header=BB1_67 Depth=1
	movl	$7, %eax
	jmp	.LBB1_83
.LBB1_75:                               #   in Loop: Header=BB1_67 Depth=1
	movl	$6, %eax
	jmp	.LBB1_83
.LBB1_76:                               #   in Loop: Header=BB1_67 Depth=1
	movl	$4, %eax
	jmp	.LBB1_83
.LBB1_77:                               #   in Loop: Header=BB1_67 Depth=1
	movl	$5, %eax
	jmp	.LBB1_83
.LBB1_79:                               #   in Loop: Header=BB1_67 Depth=1
	movl	$3, %eax
	jmp	.LBB1_83
.LBB1_80:                               #   in Loop: Header=BB1_67 Depth=1
	xorl	%eax, %eax
	jmp	.LBB1_83
.LBB1_81:                               #   in Loop: Header=BB1_67 Depth=1
	movl	$10, %eax
.LBB1_83:                               # %_Z8sig2typePc.exit
                                        #   in Loop: Header=BB1_67 Depth=1
	movq	112(%r15), %rcx
	movl	%eax, (%rcx,%rbp,4)
	movq	104(%r15), %rax
	movl	$0, (%rax,%rbp,4)
	movq	%r14, %rbx
.LBB1_84:                               #   in Loop: Header=BB1_67 Depth=1
	movq	8(%rsp), %rax
	movzbl	-1(%rax), %ecx
	cmpb	$74, %cl
	je	.LBB1_86
# BB#85:                                #   in Loop: Header=BB1_67 Depth=1
	cmpb	$68, %cl
	jne	.LBB1_87
.LBB1_86:                               #   in Loop: Header=BB1_67 Depth=1
	incl	%r13d
.LBB1_87:                               #   in Loop: Header=BB1_67 Depth=1
	cmpb	$41, (%rax)
	je	.LBB1_89
# BB#88:                                # %.backedge
                                        #   in Loop: Header=BB1_67 Depth=1
	movl	$.L.str.31, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	8(%rsp), %rdi
	cmpb	$41, (%rdi)
	jne	.LBB1_66
.LBB1_89:                               # %._crit_edge
	movl	$41, %edi
	movq	%rbx, %rsi
	callq	fputc
	jmp	.LBB1_25
.LBB1_90:
	movq	stderr(%rip), %rcx
	movl	$.L.str.29, %edi
	movl	$33, %esi
	jmp	.LBB1_32
.LBB1_91:
	movq	stderr(%rip), %rcx
	movl	$.L.str.9, %edi
	movl	$38, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	_Z12printsignameP9ClassfileP8_IO_FILERPcS3_Pv, .Lfunc_end1-_Z12printsignameP9ClassfileP8_IO_FILERPcS3_Pv
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_2
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_9
	.quad	.LBB1_10
	.quad	.LBB1_11
	.quad	.LBB1_31
	.quad	.LBB1_12
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_13
	.quad	.LBB1_14
	.quad	.LBB1_31
	.quad	.LBB1_15
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_21
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_22
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_23
	.quad	.LBB1_26
.LJTI1_1:
	.quad	.LBB1_8
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_52
	.quad	.LBB1_42
	.quad	.LBB1_43
	.quad	.LBB1_91
	.quad	.LBB1_44
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_45
	.quad	.LBB1_46
	.quad	.LBB1_91
	.quad	.LBB1_51
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_48
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_49
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_50
	.quad	.LBB1_51
.LJTI1_2:
	.quad	.LBB1_72
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_83
	.quad	.LBB1_73
	.quad	.LBB1_74
	.quad	.LBB1_91
	.quad	.LBB1_75
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_76
	.quad	.LBB1_77
	.quad	.LBB1_91
	.quad	.LBB1_82
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_79
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_80
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_91
	.quad	.LBB1_81
	.quad	.LBB1_82

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"void"
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"byte"
	.size	.L.str.1, 5

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"char"
	.size	.L.str.2, 5

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"short"
	.size	.L.str.3, 6

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"int"
	.size	.L.str.4, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"long"
	.size	.L.str.5, 5

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"float"
	.size	.L.str.6, 6

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"double"
	.size	.L.str.7, 7

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"object"
	.size	.L.str.8, 7

	.type	type2str,@object        # @type2str
	.data
	.globl	type2str
	.p2align	4
type2str:
	.quad	.L.str
	.quad	.L.str.1
	.quad	.L.str.2
	.quad	.L.str.3
	.quad	.L.str.4
	.quad	.L.str.5
	.quad	.L.str.6
	.quad	.L.str.7
	.quad	.L.str.8
	.size	type2str, 72

	.type	.L.str.9,@object        # @.str.9
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.9:
	.asciz	"Error converting signature to a type.\n"
	.size	.L.str.9, 39

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"byte %s"
	.size	.L.str.10, 8

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"char %s"
	.size	.L.str.11, 8

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"double %s"
	.size	.L.str.12, 10

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"float %s"
	.size	.L.str.13, 9

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"int %s"
	.size	.L.str.14, 7

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"long %s"
	.size	.L.str.15, 8

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"java/lang/"
	.size	.L.str.16, 11

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"%s %s"
	.size	.L.str.17, 6

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"short %s"
	.size	.L.str.18, 9

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"boolean %s"
	.size	.L.str.19, 11

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"[%d]"
	.size	.L.str.20, 5

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"[]"
	.size	.L.str.21, 3

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"Non-function with function sig!\n"
	.size	.L.str.22, 33

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"<clinit>"
	.size	.L.str.23, 9

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"<init>"
	.size	.L.str.24, 7

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"this"
	.size	.L.str.27, 5

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"L"
	.size	.L.str.28, 2

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"Function Parameter type mismatch\n"
	.size	.L.str.29, 34

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"var%d"
	.size	.L.str.30, 6

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	", "
	.size	.L.str.31, 3

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"void %s"
	.size	.L.str.33, 8

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"Error reading type signature!\n"
	.size	.L.str.34, 31


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
