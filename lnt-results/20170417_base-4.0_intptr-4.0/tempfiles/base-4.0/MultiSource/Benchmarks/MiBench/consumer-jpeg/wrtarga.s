	.text
	.file	"wrtarga.bc"
	.globl	jinit_write_targa
	.p2align	4, 0x90
	.type	jinit_write_targa,@function
jinit_write_targa:                      # @jinit_write_targa
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$64, %edx
	callq	*(%rax)
	movq	%rax, %r14
	movq	$start_output_tga, (%r14)
	movq	$finish_output_tga, 16(%r14)
	movq	%rbx, %rdi
	callq	jpeg_calc_output_dimensions
	movl	140(%rbx), %edx
	imull	128(%rbx), %edx
	movl	%edx, 56(%r14)
	movq	8(%rbx), %rax
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, 48(%r14)
	movq	8(%rbx), %rax
	movl	56(%r14), %edx
	movl	$1, %esi
	movl	$1, %ecx
	movq	%rbx, %rdi
	callq	*16(%rax)
	movq	%rax, 32(%r14)
	movl	$1, 40(%r14)
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	jinit_write_targa, .Lfunc_end0-jinit_write_targa
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_output_tga,@function
start_output_tga:                       # @start_output_tga
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi11:
	.cfi_def_cfa_offset 80
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	56(%r15), %eax
	cmpl	$2, %eax
	je	.LBB1_5
# BB#1:
	cmpl	$1, %eax
	jne	.LBB1_28
# BB#2:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movw	$0, 16(%rsp)
	movl	128(%r15), %eax
	movb	%al, 12(%rsp)
	movb	%ah, 13(%rsp)  # NOREX
	movl	132(%r15), %eax
	movb	%al, 14(%rsp)
	movb	%ah, 15(%rsp)  # NOREX
	movb	$32, 17(%rsp)
	movb	$3, 2(%rsp)
	movb	$8, 16(%rsp)
	movq	24(%r14), %rcx
	movq	%rsp, %rdi
	movl	$1, %esi
	movl	$18, %edx
	callq	fwrite
	cmpq	$18, %rax
	je	.LBB1_4
# BB#3:
	movq	(%r15), %rax
	movl	$36, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB1_4:                                # %write_header.exit
	cmpl	$0, 100(%r15)
	movl	$put_demapped_gray, %eax
	movl	$put_gray_rows, %ecx
	cmovneq	%rax, %rcx
	movq	%rcx, 8(%r14)
	jmp	.LBB1_27
.LBB1_5:
	cmpl	$0, 100(%r15)
	je	.LBB1_23
# BB#6:
	movl	148(%r15), %ebx
	cmpl	$257, %ebx              # imm = 0x101
	jl	.LBB1_9
# BB#7:                                 # %.thread
	movq	(%r15), %rax
	movl	$1039, 40(%rax)         # imm = 0x40F
	movl	%ebx, 44(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
	movq	%rsp, %rdi
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movw	$0, 16(%rsp)
	movl	56(%r15), %eax
	jmp	.LBB1_8
.LBB1_28:
	movq	(%r15), %rax
	movl	$1034, 40(%rax)         # imm = 0x40A
	movq	%r15, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*(%rax)                 # TAILCALL
.LBB1_23:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movw	$0, 16(%rsp)
	movl	128(%r15), %eax
	movb	%al, 12(%rsp)
	movb	%ah, 13(%rsp)  # NOREX
	movl	132(%r15), %eax
	movb	%al, 14(%rsp)
	movb	%ah, 15(%rsp)  # NOREX
	movb	$32, 17(%rsp)
	movb	$2, 2(%rsp)
	movb	$24, 16(%rsp)
	movq	24(%r14), %rcx
	movq	%rsp, %rdi
	movl	$1, %esi
	movl	$18, %edx
	callq	fwrite
	cmpq	$18, %rax
	je	.LBB1_25
# BB#24:
	movq	(%r15), %rax
	movl	$36, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB1_25:                               # %write_header.exit47
	movl	$put_pixel_rows, %r12d
	jmp	.LBB1_26
.LBB1_9:
	movq	%rsp, %rdi
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movw	$0, 16(%rsp)
	movl	$2, %eax
	testl	%ebx, %ebx
	jle	.LBB1_10
.LBB1_8:
	movb	$1, 1(%rsp)
	movb	%bl, 5(%rsp)
	movb	%bh, 6(%rsp)  # NOREX
	movb	$24, 7(%rsp)
	movb	$1, %cl
	jmp	.LBB1_11
.LBB1_10:
	xorl	%ecx, %ecx
.LBB1_11:
	movq	%rbx, %r12
	testb	%cl, %cl
	sete	%dl
	incb	%dl
	cmpl	$1, %eax
	movb	$3, %al
	je	.LBB1_13
# BB#12:
	movl	%edx, %eax
.LBB1_13:
	movb	$8, %dl
	movb	$8, %bl
	je	.LBB1_15
# BB#14:
	movb	$24, %bl
.LBB1_15:
	testb	%cl, %cl
	movl	128(%r15), %ecx
	movb	%cl, 12(%rsp)
	movb	%ch, 13(%rsp)  # NOREX
	movl	132(%r15), %ecx
	movb	%cl, 14(%rsp)
	movb	%ch, 15(%rsp)  # NOREX
	movb	$32, 17(%rsp)
	jne	.LBB1_17
# BB#16:
	movl	%ebx, %edx
.LBB1_17:
	movb	%al, 2(%rsp)
	movb	%dl, 16(%rsp)
	movq	24(%r14), %rcx
	movl	$1, %esi
	movl	$18, %edx
	callq	fwrite
	cmpq	$18, %rax
	je	.LBB1_19
# BB#18:
	movq	(%r15), %rax
	movl	$36, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB1_19:                               # %write_header.exit44
	testl	%r12d, %r12d
	jle	.LBB1_20
# BB#21:                                # %.lr.ph
	movq	24(%r14), %r13
	movq	%r12, %rbp
	xorl	%ebx, %ebx
	movl	$put_gray_rows, %r12d
	.p2align	4, 0x90
.LBB1_22:                               # =>This Inner Loop Header: Depth=1
	movq	152(%r15), %rax
	movq	16(%rax), %rax
	movzbl	(%rax,%rbx), %edi
	movq	%r13, %rsi
	callq	_IO_putc
	movq	152(%r15), %rax
	movq	8(%rax), %rax
	movzbl	(%rax,%rbx), %edi
	movq	%r13, %rsi
	callq	_IO_putc
	movq	152(%r15), %rax
	movq	(%rax), %rax
	movzbl	(%rax,%rbx), %edi
	movq	%r13, %rsi
	callq	_IO_putc
	incq	%rbx
	cmpq	%rbx, %rbp
	jne	.LBB1_22
	jmp	.LBB1_26
.LBB1_20:
	movl	$put_gray_rows, %r12d
.LBB1_26:                               # %.loopexit
	movq	%r12, 8(%r14)
.LBB1_27:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	start_output_tga, .Lfunc_end1-start_output_tga
	.cfi_endproc

	.p2align	4, 0x90
	.type	finish_output_tga,@function
finish_output_tga:                      # @finish_output_tga
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	24(%rbx), %rdi
	callq	fflush
	movq	24(%rbx), %rdi
	callq	ferror
	testl	%eax, %eax
	je	.LBB2_1
# BB#2:
	movq	(%r14), %rax
	movl	$36, 40(%rax)
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*(%rax)                 # TAILCALL
.LBB2_1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	finish_output_tga, .Lfunc_end2-finish_output_tga
	.cfi_endproc

	.p2align	4, 0x90
	.type	put_demapped_gray,@function
put_demapped_gray:                      # @put_demapped_gray
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movq	48(%rsi), %rdi
	movl	128(%rax), %r11d
	testl	%r11d, %r11d
	je	.LBB3_7
# BB#1:                                 # %.lr.ph.preheader
	movq	152(%rax), %rax
	movq	(%rax), %r9
	movq	32(%rsi), %rax
	movq	(%rax), %rdx
	leal	-1(%r11), %r8d
	movl	%r11d, %eax
	andl	$3, %eax
	je	.LBB3_4
# BB#2:                                 # %.lr.ph.prol.preheader
	negl	%eax
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdx), %r10d
	incq	%rdx
	movzbl	(%r9,%r10), %ecx
	movb	%cl, (%rdi)
	incq	%rdi
	decl	%r11d
	incl	%eax
	jne	.LBB3_3
.LBB3_4:                                # %.lr.ph.prol.loopexit
	cmpl	$3, %r8d
	jb	.LBB3_6
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdx), %eax
	movzbl	(%r9,%rax), %eax
	movb	%al, (%rdi)
	movzbl	1(%rdx), %eax
	movzbl	(%r9,%rax), %eax
	movb	%al, 1(%rdi)
	movzbl	2(%rdx), %eax
	movzbl	(%r9,%rax), %eax
	movb	%al, 2(%rdi)
	movzbl	3(%rdx), %eax
	movzbl	(%r9,%rax), %eax
	movb	%al, 3(%rdi)
	addq	$4, %rdx
	addq	$4, %rdi
	addl	$-4, %r11d
	jne	.LBB3_5
.LBB3_6:                                # %._crit_edge.loopexit
	movq	48(%rsi), %rdi
.LBB3_7:                                # %._crit_edge
	movl	56(%rsi), %edx
	movq	24(%rsi), %rcx
	movl	$1, %esi
	jmp	fwrite                  # TAILCALL
.Lfunc_end3:
	.size	put_demapped_gray, .Lfunc_end3-put_demapped_gray
	.cfi_endproc

	.p2align	4, 0x90
	.type	put_gray_rows,@function
put_gray_rows:                          # @put_gray_rows
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 16
.Lcfi24:
	.cfi_offset %rbx, -16
	movq	%rdi, %rax
	movq	48(%rsi), %rdi
	movl	128(%rax), %ebx
	testl	%ebx, %ebx
	je	.LBB4_20
# BB#1:                                 # %.lr.ph.preheader
	movq	32(%rsi), %rcx
	movq	(%rcx), %rcx
	leal	-1(%rbx), %r9d
	leaq	1(%r9), %r10
	cmpq	$32, %r10
	jb	.LBB4_14
# BB#2:                                 # %min.iters.checked
	movabsq	$8589934560, %r8        # imm = 0x1FFFFFFE0
	andq	%r10, %r8
	je	.LBB4_14
# BB#3:                                 # %vector.memcheck
	leaq	1(%rcx,%r9), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB4_5
# BB#4:                                 # %vector.memcheck
	leaq	1(%rdi,%r9), %rdx
	cmpq	%rdx, %rcx
	jb	.LBB4_14
.LBB4_5:                                # %vector.body.preheader
	leaq	-32(%r8), %r9
	movl	%r9d, %r11d
	shrl	$5, %r11d
	incl	%r11d
	andq	$3, %r11
	je	.LBB4_6
# BB#7:                                 # %vector.body.prol.preheader
	negq	%r11
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_8:                                # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rcx,%rdx), %xmm0
	movups	16(%rcx,%rdx), %xmm1
	movups	%xmm0, (%rdi,%rdx)
	movups	%xmm1, 16(%rdi,%rdx)
	addq	$32, %rdx
	incq	%r11
	jne	.LBB4_8
	jmp	.LBB4_9
.LBB4_6:
	xorl	%edx, %edx
.LBB4_9:                                # %vector.body.prol.loopexit
	cmpq	$96, %r9
	jb	.LBB4_12
# BB#10:                                # %vector.body.preheader.new
	movq	%r8, %r9
	subq	%rdx, %r9
	leaq	112(%rcx,%rdx), %r11
	leaq	112(%rdi,%rdx), %rdx
	.p2align	4, 0x90
.LBB4_11:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%r11), %xmm0
	movups	-96(%r11), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%r11), %xmm0
	movups	-64(%r11), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%r11), %xmm0
	movups	-32(%r11), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%r11), %xmm0
	movups	(%r11), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %r11
	subq	$-128, %rdx
	addq	$-128, %r9
	jne	.LBB4_11
.LBB4_12:                               # %middle.block
	cmpq	%r8, %r10
	je	.LBB4_19
# BB#13:
	subl	%r8d, %ebx
	addq	%r8, %rdi
	addq	%r8, %rcx
.LBB4_14:                               # %.lr.ph.preheader32
	leal	-1(%rbx), %r8d
	movl	%ebx, %edx
	andl	$7, %edx
	je	.LBB4_17
# BB#15:                                # %.lr.ph.prol.preheader
	negl	%edx
	.p2align	4, 0x90
.LBB4_16:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %eax
	incq	%rcx
	movb	%al, (%rdi)
	incq	%rdi
	decl	%ebx
	incl	%edx
	jne	.LBB4_16
.LBB4_17:                               # %.lr.ph.prol.loopexit
	cmpl	$7, %r8d
	jb	.LBB4_19
	.p2align	4, 0x90
.LBB4_18:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %eax
	movb	%al, (%rdi)
	movzbl	1(%rcx), %eax
	movb	%al, 1(%rdi)
	movzbl	2(%rcx), %eax
	movb	%al, 2(%rdi)
	movzbl	3(%rcx), %eax
	movb	%al, 3(%rdi)
	movzbl	4(%rcx), %eax
	movb	%al, 4(%rdi)
	movzbl	5(%rcx), %eax
	movb	%al, 5(%rdi)
	movzbl	6(%rcx), %eax
	movb	%al, 6(%rdi)
	movzbl	7(%rcx), %eax
	movb	%al, 7(%rdi)
	addq	$8, %rcx
	addq	$8, %rdi
	addl	$-8, %ebx
	jne	.LBB4_18
.LBB4_19:                               # %._crit_edge.loopexit
	movq	48(%rsi), %rdi
.LBB4_20:                               # %._crit_edge
	movl	56(%rsi), %edx
	movq	24(%rsi), %rcx
	movl	$1, %esi
	popq	%rbx
	jmp	fwrite                  # TAILCALL
.Lfunc_end4:
	.size	put_gray_rows, .Lfunc_end4-put_gray_rows
	.cfi_endproc

	.p2align	4, 0x90
	.type	put_pixel_rows,@function
put_pixel_rows:                         # @put_pixel_rows
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movq	48(%rsi), %rdi
	movl	128(%rax), %edx
	testl	%edx, %edx
	je	.LBB5_8
# BB#1:                                 # %.lr.ph.preheader
	movq	32(%rsi), %rax
	movq	(%rax), %rax
	testb	$1, %dl
	jne	.LBB5_3
# BB#2:
	movl	%edx, %ecx
	cmpl	$1, %edx
	jne	.LBB5_5
	jmp	.LBB5_7
.LBB5_3:                                # %.lr.ph.prol
	movb	2(%rax), %cl
	movb	%cl, (%rdi)
	movb	1(%rax), %cl
	movb	%cl, 1(%rdi)
	movb	(%rax), %cl
	movb	%cl, 2(%rdi)
	addq	$3, %rax
	addq	$3, %rdi
	leal	-1(%rdx), %ecx
	cmpl	$1, %edx
	je	.LBB5_7
.LBB5_5:                                # %.lr.ph.preheader.new
	addq	$5, %rax
	.p2align	4, 0x90
.LBB5_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rax), %edx
	movb	%dl, (%rdi)
	movzbl	-4(%rax), %edx
	movb	%dl, 1(%rdi)
	movzbl	-5(%rax), %edx
	movb	%dl, 2(%rdi)
	movzbl	(%rax), %edx
	movb	%dl, 3(%rdi)
	movzbl	-1(%rax), %edx
	movb	%dl, 4(%rdi)
	movzbl	-2(%rax), %edx
	movb	%dl, 5(%rdi)
	addq	$6, %rax
	addq	$6, %rdi
	addl	$-2, %ecx
	jne	.LBB5_6
.LBB5_7:                                # %._crit_edge.loopexit
	movq	48(%rsi), %rdi
.LBB5_8:                                # %._crit_edge
	movl	56(%rsi), %edx
	movq	24(%rsi), %rcx
	movl	$1, %esi
	jmp	fwrite                  # TAILCALL
.Lfunc_end5:
	.size	put_pixel_rows, .Lfunc_end5-put_pixel_rows
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
