	.text
	.file	"TarOut.bc"
	.globl	_ZN8NArchive4NTar11COutArchive10WriteBytesEPKvj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar11COutArchive10WriteBytesEPKvj,@function
_ZN8NArchive4NTar11COutArchive10WriteBytesEPKvj: # @_ZN8NArchive4NTar11COutArchive10WriteBytesEPKvj
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rdi
	movl	%edx, %edx
	jmp	_Z11WriteStreamP20ISequentialOutStreamPKvm # TAILCALL
.Lfunc_end0:
	.size	_ZN8NArchive4NTar11COutArchive10WriteBytesEPKvj, .Lfunc_end0-_ZN8NArchive4NTar11COutArchive10WriteBytesEPKvj
	.cfi_endproc

	.globl	_ZN8NArchive4NTar11COutArchive6CreateEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar11COutArchive6CreateEP20ISequentialOutStream,@function
_ZN8NArchive4NTar11COutArchive6CreateEP20ISequentialOutStream: # @_ZN8NArchive4NTar11COutArchive6CreateEP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB1_2
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
.LBB1_2:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB1_4:                                # %_ZN9CMyComPtrI20ISequentialOutStreamEaSEPS0_.exit
	movq	%rbx, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	_ZN8NArchive4NTar11COutArchive6CreateEP20ISequentialOutStream, .Lfunc_end1-_ZN8NArchive4NTar11COutArchive6CreateEP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZN8NArchive4NTar11COutArchive15WriteHeaderRealERKNS0_5CItemE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar11COutArchive15WriteHeaderRealERKNS0_5CItemE,@function
_ZN8NArchive4NTar11COutArchive15WriteHeaderRealERKNS0_5CItemE: # @_ZN8NArchive4NTar11COutArchive15WriteHeaderRealERKNS0_5CItemE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	subq	$536, %rsp              # imm = 0x218
.Lcfi11:
	.cfi_def_cfa_offset 592
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	leaq	16(%rsp), %rdi
	xorl	%esi, %esi
	movl	$512, %edx              # imm = 0x200
	callq	memset
	movl	$-2147467259, %r12d     # imm = 0x80004005
	cmpl	$100, 8(%r14)
	jg	.LBB2_92
# BB#1:
	movq	(%r14), %rax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rax,%rcx), %edx
	movb	%dl, 15(%rsp,%rcx)
	testb	%dl, %dl
	je	.LBB2_4
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	cmpq	$100, %rcx
	leaq	1(%rcx), %rcx
	jl	.LBB2_2
.LBB2_4:                                # %_ZN8NArchive4NTarL9MyStrNCpyEPcPKci.exit
	movl	24(%r14), %esi
	movq	%rsp, %rdi
	callq	_ZN8NArchive4NTarL15MakeOctalStringEy
	movl	8(%rsp), %eax
	cmpl	$8, %eax
	jl	.LBB2_6
# BB#5:                                 # %._Z12MyStringCopyIcEPT_S1_PKS0_.exit_crit_edge.i66
	xorl	%ebx, %ebx
	jmp	.LBB2_11
.LBB2_6:
	leaq	116(%rsp), %rbp
	movl	$7, %ebx
	subl	%eax, %ebx
	testl	%ebx, %ebx
	jle	.LBB2_8
# BB#7:                                 # %.lr.ph.preheader.i67
	movl	$6, %edx
	subl	%eax, %edx
	incq	%rdx
	movl	$32, %esi
	movq	%rbp, %rdi
	callq	memset
.LBB2_8:                                # %._crit_edge.i68
	movslq	%ebx, %rax
	addq	%rax, %rbp
	movq	(%rsp), %rax
	.p2align	4, 0x90
.LBB2_9:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbp)
	incq	%rbp
	testb	%cl, %cl
	jne	.LBB2_9
# BB#10:
	movb	$1, %bl
.LBB2_11:                               # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i73
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_13
# BB#12:
	callq	_ZdaPv
.LBB2_13:                               # %_ZN8NArchive4NTarL16MakeOctalString8EPcj.exit74
	testb	%bl, %bl
	je	.LBB2_92
# BB#14:
	movl	28(%r14), %esi
	movq	%rsp, %rdi
	callq	_ZN8NArchive4NTarL15MakeOctalStringEy
	movl	8(%rsp), %eax
	cmpl	$8, %eax
	jl	.LBB2_16
# BB#15:                                # %._Z12MyStringCopyIcEPT_S1_PKS0_.exit_crit_edge.i76
	xorl	%ebx, %ebx
	jmp	.LBB2_21
.LBB2_16:
	leaq	124(%rsp), %rbp
	movl	$7, %ebx
	subl	%eax, %ebx
	testl	%ebx, %ebx
	jle	.LBB2_18
# BB#17:                                # %.lr.ph.preheader.i77
	movl	$6, %edx
	subl	%eax, %edx
	incq	%rdx
	movl	$32, %esi
	movq	%rbp, %rdi
	callq	memset
.LBB2_18:                               # %._crit_edge.i78
	movslq	%ebx, %rax
	addq	%rax, %rbp
	movq	(%rsp), %rax
	.p2align	4, 0x90
.LBB2_19:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbp)
	incq	%rbp
	testb	%cl, %cl
	jne	.LBB2_19
# BB#20:
	movb	$1, %bl
.LBB2_21:                               # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i83
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_23
# BB#22:
	callq	_ZdaPv
.LBB2_23:                               # %_ZN8NArchive4NTarL16MakeOctalString8EPcj.exit84
	testb	%bl, %bl
	je	.LBB2_92
# BB#24:
	movl	32(%r14), %esi
	movq	%rsp, %rdi
	callq	_ZN8NArchive4NTarL15MakeOctalStringEy
	movl	8(%rsp), %eax
	cmpl	$8, %eax
	jl	.LBB2_26
# BB#25:                                # %._Z12MyStringCopyIcEPT_S1_PKS0_.exit_crit_edge.i86
	xorl	%ebx, %ebx
	jmp	.LBB2_31
.LBB2_26:
	leaq	132(%rsp), %rbp
	movl	$7, %ebx
	subl	%eax, %ebx
	testl	%ebx, %ebx
	jle	.LBB2_28
# BB#27:                                # %.lr.ph.preheader.i87
	movl	$6, %edx
	subl	%eax, %edx
	incq	%rdx
	movl	$32, %esi
	movq	%rbp, %rdi
	callq	memset
.LBB2_28:                               # %._crit_edge.i88
	movslq	%ebx, %rax
	addq	%rax, %rbp
	movq	(%rsp), %rax
	.p2align	4, 0x90
.LBB2_29:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbp)
	incq	%rbp
	testb	%cl, %cl
	jne	.LBB2_29
# BB#30:
	movb	$1, %bl
.LBB2_31:                               # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i93
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_33
# BB#32:
	callq	_ZdaPv
.LBB2_33:                               # %_ZN8NArchive4NTarL16MakeOctalString8EPcj.exit94
	testb	%bl, %bl
	je	.LBB2_92
# BB#34:
	movq	%r15, %rbx
	movq	16(%r14), %rsi
	movq	%rsp, %rdi
	movq	%rsi, %rbp
	callq	_ZN8NArchive4NTarL15MakeOctalStringEy
	movslq	8(%rsp), %r13
	cmpq	$13, %r13
	jl	.LBB2_36
# BB#35:                                # %.loopexit.loopexit29.i
	movb	$-128, 140(%rsp)
	movb	$0, 143(%rsp)
	movb	$0, 142(%rsp)
	movb	$0, 141(%rsp)
	movq	%rbp, %rcx
	movq	%rcx, %rax
	shrq	$56, %rax
	movb	%al, 144(%rsp)
	movq	%rcx, %rax
	shrq	$48, %rax
	movb	%al, 145(%rsp)
	movq	%rcx, %rax
	shrq	$40, %rax
	movb	%al, 146(%rsp)
	movq	%rcx, %rax
	shrq	$32, %rax
	movb	%al, 147(%rsp)
	movq	%rcx, %rax
	shrq	$24, %rax
	movb	%al, 148(%rsp)
	movq	%rcx, %rax
	shrq	$16, %rax
	movb	%al, 149(%rsp)
	movb	%ch, 150(%rsp)  # NOREX
	movb	%cl, 151(%rsp)
	movq	(%rsp), %rbp
	testq	%rbp, %rbp
	jne	.LBB2_40
	jmp	.LBB2_41
.LBB2_36:
	leaq	140(%rsp), %r15
	movl	$12, %ebp
	subq	%r13, %rbp
	testl	%ebp, %ebp
	jle	.LBB2_38
# BB#37:                                # %.lr.ph.preheader.i96
	movl	$11, %edx
	subl	%r13d, %edx
	incq	%rdx
	movl	$32, %esi
	movq	%r15, %rdi
	callq	memset
.LBB2_38:                               # %._crit_edge.i97
	addq	%rbp, %r15
	movq	(%rsp), %rbp
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	callq	memmove
	testq	%rbp, %rbp
	je	.LBB2_41
.LBB2_40:
	movq	%rbp, %rdi
	callq	_ZdaPv
.LBB2_41:                               # %_ZN8NArchive4NTarL17MakeOctalString12EPcy.exit
	movl	36(%r14), %esi
	movq	%rsp, %rdi
	movq	%rsi, %rbp
	callq	_ZN8NArchive4NTarL15MakeOctalStringEy
	movslq	8(%rsp), %r13
	cmpq	$13, %r13
	jl	.LBB2_43
# BB#42:                                # %.loopexit.loopexit29.i100
	movb	$-128, 152(%rsp)
	movq	%rbp, %rcx
	movl	%ecx, %eax
	shrl	$24, %eax
	movb	$0, 159(%rsp)
	movw	$0, 157(%rsp)
	movl	$0, 153(%rsp)
	movb	%al, 160(%rsp)
	movl	%ecx, %eax
	shrl	$16, %eax
	movb	%al, 161(%rsp)
	movb	%ch, 162(%rsp)  # NOREX
	movb	%cl, 163(%rsp)
	movq	(%rsp), %rbp
	jmp	.LBB2_46
.LBB2_43:
	leaq	152(%rsp), %r15
	movl	$12, %ebp
	subq	%r13, %rbp
	testl	%ebp, %ebp
	jle	.LBB2_45
# BB#44:                                # %.lr.ph.preheader.i101
	movl	$11, %edx
	subl	%r13d, %edx
	incq	%rdx
	movl	$32, %esi
	movq	%r15, %rdi
	callq	memset
.LBB2_45:                               # %._crit_edge.i102
	addq	%rbp, %r15
	movq	(%rsp), %rbp
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	callq	memmove
.LBB2_46:                               # %.loopexit.i103
	testq	%rbp, %rbp
	movq	%rbx, %r13
	je	.LBB2_48
# BB#47:
	movq	%rbp, %rdi
	callq	_ZdaPv
.LBB2_48:                               # %_ZN8NArchive4NTarL17MakeOctalString12EPcy.exit104
	movq	_ZN8NArchive4NTar11NFileHeader15kCheckSumBlanksE(%rip), %rax
	movq	(%rax), %rax
	movq	%rax, 164(%rsp)
	movb	104(%r14), %al
	movb	%al, 172(%rsp)
	cmpl	$99, 56(%r14)
	jg	.LBB2_92
# BB#49:                                # %.preheader.i.preheader
	leaq	164(%rsp), %r15
	movq	48(%r14), %rax
	leaq	173(%rsp), %rcx
	.p2align	4, 0x90
.LBB2_50:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	incq	%rax
	movb	%dl, (%rcx)
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB2_50
# BB#51:
	movq	96(%r14), %rax
	movq	%rax, 273(%rsp)
	cmpl	$31, 72(%r14)
	jg	.LBB2_92
# BB#52:                                # %.preheader.i110.preheader
	movq	64(%r14), %rax
	leaq	281(%rsp), %rcx
	.p2align	4, 0x90
.LBB2_53:                               # %.preheader.i110
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	incq	%rax
	movb	%dl, (%rcx)
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB2_53
# BB#54:
	cmpl	$31, 88(%r14)
	jg	.LBB2_92
# BB#55:                                # %.preheader.i116.preheader
	movq	80(%r14), %rax
	leaq	313(%rsp), %rcx
	.p2align	4, 0x90
.LBB2_56:                               # %.preheader.i116
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	incq	%rax
	movb	%dl, (%rcx)
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB2_56
# BB#57:
	cmpb	$0, 105(%r14)
	je	.LBB2_68
# BB#58:
	movl	40(%r14), %esi
	movq	%rsp, %rdi
	callq	_ZN8NArchive4NTarL15MakeOctalStringEy
	movl	8(%rsp), %eax
	cmpl	$8, %eax
	jl	.LBB2_60
# BB#59:                                # %._Z12MyStringCopyIcEPT_S1_PKS0_.exit_crit_edge.i121
	xorl	%ebx, %ebx
	jmp	.LBB2_65
.LBB2_60:
	leaq	345(%rsp), %rbp
	movl	$7, %ebx
	subl	%eax, %ebx
	testl	%ebx, %ebx
	jle	.LBB2_62
# BB#61:                                # %.lr.ph.preheader.i122
	movl	$6, %edx
	subl	%eax, %edx
	incq	%rdx
	movl	$32, %esi
	movq	%rbp, %rdi
	callq	memset
.LBB2_62:                               # %._crit_edge.i123
	movslq	%ebx, %rax
	addq	%rax, %rbp
	movq	(%rsp), %rax
.LBB2_63:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbp)
	incq	%rbp
	testb	%cl, %cl
	jne	.LBB2_63
# BB#64:
	movb	$1, %bl
.LBB2_65:                               # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i128
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_67
# BB#66:
	callq	_ZdaPv
.LBB2_67:                               # %_ZN8NArchive4NTarL16MakeOctalString8EPcj.exit129
	testb	%bl, %bl
	je	.LBB2_92
.LBB2_68:
	cmpb	$0, 106(%r14)
	je	.LBB2_79
# BB#69:
	movl	44(%r14), %esi
	movq	%rsp, %rdi
	callq	_ZN8NArchive4NTarL15MakeOctalStringEy
	movl	8(%rsp), %eax
	cmpl	$8, %eax
	jl	.LBB2_71
# BB#70:                                # %._Z12MyStringCopyIcEPT_S1_PKS0_.exit_crit_edge.i131
	xorl	%ebx, %ebx
	jmp	.LBB2_76
.LBB2_71:
	leaq	353(%rsp), %rbx
	movl	$7, %ebp
	subl	%eax, %ebp
	testl	%ebp, %ebp
	jle	.LBB2_73
# BB#72:                                # %.lr.ph.preheader.i132
	movl	$6, %edx
	subl	%eax, %edx
	incq	%rdx
	movl	$32, %esi
	movq	%rbx, %rdi
	callq	memset
.LBB2_73:                               # %._crit_edge.i133
	movslq	%ebp, %rax
	addq	%rax, %rbx
	movq	(%rsp), %rax
.LBB2_74:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB2_74
# BB#75:
	movb	$1, %bl
.LBB2_76:                               # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i138
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_78
# BB#77:
	callq	_ZdaPv
.LBB2_78:                               # %_ZN8NArchive4NTarL16MakeOctalString8EPcj.exit139
	testb	%bl, %bl
	je	.LBB2_92
.LBB2_79:                               # %vector.body.preheader
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	pxor	%xmm2, %xmm2
	pxor	%xmm1, %xmm1
.LBB2_80:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	16(%rsp,%rax), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movd	20(%rsp,%rax), %xmm4    # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	punpcklbw	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3],xmm4[4],xmm0[4],xmm4[5],xmm0[5],xmm4[6],xmm0[6],xmm4[7],xmm0[7]
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	paddd	%xmm2, %xmm3
	paddd	%xmm1, %xmm4
	movd	24(%rsp,%rax), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movd	28(%rsp,%rax), %xmm1    # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	paddd	%xmm3, %xmm2
	paddd	%xmm4, %xmm1
	addq	$16, %rax
	cmpq	$512, %rax              # imm = 0x200
	jne	.LBB2_80
# BB#81:                                # %middle.block
	paddd	%xmm2, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %esi
	movq	%rsp, %rdi
	callq	_ZN8NArchive4NTarL15MakeOctalStringEy
	movl	8(%rsp), %eax
	cmpl	$8, %eax
	jl	.LBB2_83
# BB#82:                                # %._Z12MyStringCopyIcEPT_S1_PKS0_.exit_crit_edge.i
	xorl	%ebx, %ebx
	jmp	.LBB2_88
.LBB2_83:
	movl	$7, %ebx
	subl	%eax, %ebx
	testl	%ebx, %ebx
	jle	.LBB2_85
# BB#84:                                # %.lr.ph.preheader.i
	movl	$6, %edx
	subl	%eax, %edx
	incq	%rdx
	movl	$32, %esi
	movq	%r15, %rdi
	callq	memset
.LBB2_85:                               # %._crit_edge.i
	movslq	%ebx, %rax
	addq	%rax, %r15
	movq	(%rsp), %rax
.LBB2_86:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%r15)
	incq	%r15
	testb	%cl, %cl
	jne	.LBB2_86
# BB#87:
	movb	$1, %bl
.LBB2_88:                               # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_90
# BB#89:
	callq	_ZdaPv
.LBB2_90:                               # %_ZN8NArchive4NTarL16MakeOctalString8EPcj.exit
	testb	%bl, %bl
	je	.LBB2_92
# BB#91:
	movq	(%r13), %rdi
	leaq	16(%rsp), %rsi
	movl	$512, %edx              # imm = 0x200
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	movl	%eax, %r12d
.LBB2_92:                               # %_ZN8NArchive4NTarL10CopyStringEPcRK11CStringBaseIcEi.exit
	movl	%r12d, %eax
	addq	$536, %rsp              # imm = 0x218
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN8NArchive4NTar11COutArchive15WriteHeaderRealERKNS0_5CItemE, .Lfunc_end2-_ZN8NArchive4NTar11COutArchive15WriteHeaderRealERKNS0_5CItemE
	.cfi_endproc

	.globl	_ZN8NArchive4NTar11COutArchive11WriteHeaderERKNS0_5CItemE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar11COutArchive11WriteHeaderERKNS0_5CItemE,@function
_ZN8NArchive4NTar11COutArchive11WriteHeaderERKNS0_5CItemE: # @_ZN8NArchive4NTar11COutArchive11WriteHeaderERKNS0_5CItemE
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	subq	$648, %rsp              # imm = 0x288
.Lcfi24:
	.cfi_def_cfa_offset 704
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movslq	8(%r15), %r12
	cmpq	$99, %r12
	jg	.LBB3_1
# BB#98:
	movq	%r15, %rsi
	addq	$648, %rsp              # imm = 0x288
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN8NArchive4NTar11COutArchive15WriteHeaderRealERKNS0_5CItemE # TAILCALL
.LBB3_1:
	movq	%rdi, (%rsp)            # 8-byte Spill
	leaq	16(%rsp), %rdi
	movq	%r15, %rsi
	callq	_ZN8NArchive4NTar5CItemC2ERKS1_
	incq	%r12
	movq	%r12, 32(%rsp)
	movb	$76, 120(%rsp)
	movq	_ZN8NArchive4NTar11NFileHeader9kLongLinkE(%rip), %r13
	movl	$0, 24(%rsp)
	movq	16(%rsp), %rax
	movb	$0, (%rax)
	xorl	%r14d, %r14d
	movq	%r13, %rax
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	incl	%r14d
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB3_2
# BB#3:                                 # %_Z11MyStringLenIcEiPKT_.exit.i
	leal	-1(%r14), %ecx
	movl	28(%rsp), %ebp
	cmpl	%r14d, %ebp
	jne	.LBB3_5
# BB#4:                                 # %_Z11MyStringLenIcEiPKT_.exit._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
	movq	16(%rsp), %rbx
	jmp	.LBB3_30
.LBB3_5:
	movslq	%r14d, %rax
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	cmpl	$-1, %ecx
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
.Ltmp0:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp1:
# BB#6:                                 # %.noexc
	testl	%ebp, %ebp
	jle	.LBB3_29
# BB#7:                                 # %.preheader.i.i
	movslq	24(%rsp), %r8
	testq	%r8, %r8
	movq	16(%rsp), %rdi
	jle	.LBB3_27
# BB#8:                                 # %.lr.ph.preheader.i.i
	cmpl	$31, %r8d
	jbe	.LBB3_9
# BB#16:                                # %min.iters.checked
	movq	%r8, %rcx
	andq	$-32, %rcx
	je	.LBB3_9
# BB#17:                                # %vector.memcheck
	leaq	(%rdi,%r8), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB3_19
# BB#18:                                # %vector.memcheck
	leaq	(%rbx,%r8), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB3_19
.LBB3_9:
	xorl	%ecx, %ecx
.LBB3_10:                               # %.lr.ph.i.i.preheader
	movl	%r8d, %esi
	subl	%ecx, %esi
	leaq	-1(%r8), %rbp
	subq	%rcx, %rbp
	andq	$7, %rsi
	je	.LBB3_13
# BB#11:                                # %.lr.ph.i.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB3_12:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB3_12
.LBB3_13:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rbp
	jb	.LBB3_28
# BB#14:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %r8
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB3_15:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r8
	jne	.LBB3_15
	jmp	.LBB3_28
.LBB3_27:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB3_29
.LBB3_28:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
.LBB3_29:                               # %._crit_edge17.i.i
	movq	%rbx, 16(%rsp)
	movslq	24(%rsp), %rax
	movb	$0, (%rbx,%rax)
	movl	%r14d, 28(%rsp)
	movl	12(%rsp), %ecx          # 4-byte Reload
	.p2align	4, 0x90
.LBB3_30:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r13), %eax
	incq	%r13
	movb	%al, (%rbx)
	incq	%rbx
	testb	%al, %al
	jne	.LBB3_30
# BB#31:
	movl	%ecx, 24(%rsp)
	movl	$0, 72(%rsp)
	movq	64(%rsp), %rax
	movb	$0, (%rax)
.Ltmp2:
	leaq	16(%rsp), %rsi
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN8NArchive4NTar11COutArchive15WriteHeaderRealERKNS0_5CItemE
	movl	%eax, %ebx
.Ltmp3:
# BB#32:
	testl	%ebx, %ebx
	jne	.LBB3_75
# BB#33:
	movq	(%r15), %rsi
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %rdi
	movl	%r12d, %edx
.Ltmp4:
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	movl	%eax, %ebx
.Ltmp5:
# BB#34:                                # %_ZN8NArchive4NTar11COutArchive10WriteBytesEPKvj.exit
	testl	%ebx, %ebx
	jne	.LBB3_75
# BB#35:
	andl	$511, %r12d             # imm = 0x1FF
	je	.LBB3_41
# BB#36:
	movl	$512, %eax              # imm = 0x200
	subl	%r12d, %eax
	je	.LBB3_37
# BB#38:                                # %.lr.ph.preheader.i
	movl	%eax, %ebx
	leaq	128(%rsp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	jmp	.LBB3_39
.LBB3_37:
	xorl	%ebx, %ebx
.LBB3_39:                               # %._crit_edge.i
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %rdi
.Ltmp6:
	leaq	128(%rsp), %rsi
	movq	%rbx, %rdx
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	movl	%eax, %ebx
.Ltmp7:
# BB#40:                                # %_ZN8NArchive4NTar11COutArchive16FillDataResidualEy.exit
	testl	%ebx, %ebx
	jne	.LBB3_75
.LBB3_41:                               # %_ZN8NArchive4NTar11COutArchive16FillDataResidualEy.exit.thread
.Ltmp8:
	leaq	16(%rsp), %rdi
	movq	%r15, %rsi
	callq	_ZN8NArchive4NTar5CItemaSERKS1_
.Ltmp9:
# BB#42:
.Ltmp10:
	leaq	128(%rsp), %rdi
	xorl	%edx, %edx
	movl	$99, %ecx
	movq	%r15, %rsi
	callq	_ZNK11CStringBaseIcE3MidEii
.Ltmp11:
# BB#43:                                # %_ZNK11CStringBaseIcE4LeftEi.exit
	movl	$0, 24(%rsp)
	movq	16(%rsp), %rax
	movb	$0, (%rax)
	movslq	136(%rsp), %rax
	leaq	1(%rax), %r14
	movl	28(%rsp), %ebp
	cmpl	%ebp, %r14d
	jne	.LBB3_45
# BB#44:                                # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
	movq	16(%rsp), %rbx
	movq	(%rsp), %r15            # 8-byte Reload
	jmp	.LBB3_70
.LBB3_45:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r14, %rdi
	cmovlq	%rax, %rdi
.Ltmp13:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp14:
# BB#46:                                # %.noexc62
	testl	%ebp, %ebp
	movq	(%rsp), %r15            # 8-byte Reload
	jle	.LBB3_69
# BB#47:                                # %.preheader.i.i50
	movslq	24(%rsp), %r8
	testq	%r8, %r8
	movq	16(%rsp), %rdi
	jle	.LBB3_67
# BB#48:                                # %.lr.ph.preheader.i.i51
	cmpl	$31, %r8d
	jbe	.LBB3_49
# BB#56:                                # %min.iters.checked77
	movq	%r8, %rcx
	andq	$-32, %rcx
	je	.LBB3_49
# BB#57:                                # %vector.memcheck88
	leaq	(%rdi,%r8), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB3_59
# BB#58:                                # %vector.memcheck88
	leaq	(%rbx,%r8), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB3_59
.LBB3_49:
	xorl	%ecx, %ecx
.LBB3_50:                               # %.lr.ph.i.i56.preheader
	movl	%r8d, %esi
	subl	%ecx, %esi
	leaq	-1(%r8), %rbp
	subq	%rcx, %rbp
	andq	$7, %rsi
	je	.LBB3_53
# BB#51:                                # %.lr.ph.i.i56.prol.preheader
	negq	%rsi
.LBB3_52:                               # %.lr.ph.i.i56.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB3_52
.LBB3_53:                               # %.lr.ph.i.i56.prol.loopexit
	cmpq	$7, %rbp
	jb	.LBB3_68
# BB#54:                                # %.lr.ph.i.i56.preheader.new
	subq	%rcx, %r8
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
.LBB3_55:                               # %.lr.ph.i.i56
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r8
	jne	.LBB3_55
	jmp	.LBB3_68
.LBB3_19:                               # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB3_20
# BB#21:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_22:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp), %xmm0
	movups	16(%rdi,%rbp), %xmm1
	movups	%xmm0, (%rbx,%rbp)
	movups	%xmm1, 16(%rbx,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB3_22
	jmp	.LBB3_23
.LBB3_20:
	xorl	%ebp, %ebp
.LBB3_23:                               # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB3_26
# BB#24:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%rbx,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
	.p2align	4, 0x90
.LBB3_25:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB3_25
.LBB3_26:                               # %middle.block
	cmpq	%rcx, %r8
	jne	.LBB3_10
	jmp	.LBB3_28
.LBB3_67:                               # %._crit_edge.i.i52
	testq	%rdi, %rdi
	je	.LBB3_69
.LBB3_68:                               # %._crit_edge.thread.i.i57
	callq	_ZdaPv
.LBB3_69:                               # %._crit_edge17.i.i58
	movq	%rbx, 16(%rsp)
	movslq	24(%rsp), %rax
	movb	$0, (%rbx,%rax)
	movl	%r14d, 28(%rsp)
.LBB3_70:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i59
	movq	128(%rsp), %rax
	.p2align	4, 0x90
.LBB3_71:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB3_71
# BB#72:
	movl	136(%rsp), %eax
	movl	%eax, 24(%rsp)
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_74
# BB#73:
	callq	_ZdaPv
.LBB3_74:                               # %_ZN11CStringBaseIcED2Ev.exit
.Ltmp16:
	leaq	16(%rsp), %rsi
	movq	%r15, %rdi
	callq	_ZN8NArchive4NTar11COutArchive15WriteHeaderRealERKNS0_5CItemE
	movl	%eax, %ebx
.Ltmp17:
.LBB3_75:
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_77
# BB#76:
	callq	_ZdaPv
.LBB3_77:                               # %_ZN11CStringBaseIcED2Ev.exit.i64
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_79
# BB#78:
	callq	_ZdaPv
.LBB3_79:                               # %_ZN11CStringBaseIcED2Ev.exit1.i65
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_81
# BB#80:
	callq	_ZdaPv
.LBB3_81:                               # %_ZN11CStringBaseIcED2Ev.exit2.i66
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_83
# BB#82:
	callq	_ZdaPv
.LBB3_83:
	movl	%ebx, %eax
	addq	$648, %rsp              # imm = 0x288
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_59:                               # %vector.body73.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB3_60
# BB#61:                                # %vector.body73.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
.LBB3_62:                               # %vector.body73.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp), %xmm0
	movups	16(%rdi,%rbp), %xmm1
	movups	%xmm0, (%rbx,%rbp)
	movups	%xmm1, 16(%rbx,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB3_62
	jmp	.LBB3_63
.LBB3_60:
	xorl	%ebp, %ebp
.LBB3_63:                               # %vector.body73.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB3_66
# BB#64:                                # %vector.body73.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%rbx,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
.LBB3_65:                               # %vector.body73
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB3_65
.LBB3_66:                               # %middle.block74
	cmpq	%rcx, %r8
	jne	.LBB3_50
	jmp	.LBB3_68
.LBB3_85:
.Ltmp15:
	movq	%rax, %rbx
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_89
# BB#86:
	callq	_ZdaPv
	jmp	.LBB3_89
.LBB3_84:
.Ltmp12:
	jmp	.LBB3_88
.LBB3_87:
.Ltmp18:
.LBB3_88:
	movq	%rax, %rbx
.LBB3_89:
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_91
# BB#90:
	callq	_ZdaPv
.LBB3_91:                               # %_ZN11CStringBaseIcED2Ev.exit.i
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_93
# BB#92:
	callq	_ZdaPv
.LBB3_93:                               # %_ZN11CStringBaseIcED2Ev.exit1.i
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_95
# BB#94:
	callq	_ZdaPv
.LBB3_95:                               # %_ZN11CStringBaseIcED2Ev.exit2.i
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_97
# BB#96:
	callq	_ZdaPv
.LBB3_97:                               # %_ZN8NArchive4NTar5CItemD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN8NArchive4NTar11COutArchive11WriteHeaderERKNS0_5CItemE, .Lfunc_end3-_ZN8NArchive4NTar11COutArchive11WriteHeaderERKNS0_5CItemE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\352\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp5-.Ltmp0           #   Call between .Ltmp0 and .Ltmp5
	.long	.Ltmp18-.Lfunc_begin0   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp9-.Ltmp6           #   Call between .Ltmp6 and .Ltmp9
	.long	.Ltmp18-.Lfunc_begin0   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp11-.Ltmp10         #   Call between .Ltmp10 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin0   #     jumps to .Ltmp15
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin0   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Lfunc_end3-.Ltmp17     #   Call between .Ltmp17 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NArchive4NTar5CItemC2ERKS1_,"axG",@progbits,_ZN8NArchive4NTar5CItemC2ERKS1_,comdat
	.weak	_ZN8NArchive4NTar5CItemC2ERKS1_
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar5CItemC2ERKS1_,@function
_ZN8NArchive4NTar5CItemC2ERKS1_:        # @_ZN8NArchive4NTar5CItemC2ERKS1_
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 64
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movslq	8(%rbp), %rax
	leaq	1(%rax), %rbx
	testl	%ebx, %ebx
	je	.LBB4_1
# BB#2:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%rbx, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r14)
	movb	$0, (%rax)
	movl	%ebx, 12(%r14)
	jmp	.LBB4_3
.LBB4_1:
	xorl	%eax, %eax
.LBB4_3:                                # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
	movq	(%rbp), %rcx
	.p2align	4, 0x90
.LBB4_4:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB4_4
# BB#5:                                 # %_ZN11CStringBaseIcEC2ERKS0_.exit
	movl	8(%rbp), %eax
	movl	%eax, 8(%r14)
	movups	16(%rbp), %xmm0
	movups	32(%rbp), %xmm1
	movups	%xmm1, 32(%r14)
	movups	%xmm0, 16(%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%r14)
	movslq	56(%rbp), %rax
	leaq	1(%rax), %r15
	testl	%r15d, %r15d
	je	.LBB4_6
# BB#7:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r15, %rdi
	cmovlq	%rax, %rdi
.Ltmp19:
	callq	_Znam
.Ltmp20:
# BB#8:                                 # %.noexc
	movq	%rax, 48(%r14)
	movb	$0, (%rax)
	movl	%r15d, 60(%r14)
	jmp	.LBB4_9
.LBB4_6:
	xorl	%eax, %eax
.LBB4_9:                                # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i10
	leaq	48(%r14), %r15
	movq	48(%rbp), %rcx
	.p2align	4, 0x90
.LBB4_10:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB4_10
# BB#11:
	movl	56(%rbp), %eax
	movl	%eax, 56(%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 64(%r14)
	movslq	72(%rbp), %rax
	leaq	1(%rax), %r12
	testl	%r12d, %r12d
	je	.LBB4_12
# BB#13:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r12, %rdi
	cmovlq	%rax, %rdi
.Ltmp22:
	callq	_Znam
.Ltmp23:
# BB#14:                                # %.noexc17
	movq	%rax, 64(%r14)
	movb	$0, (%rax)
	movl	%r12d, 76(%r14)
	jmp	.LBB4_15
.LBB4_12:
	xorl	%eax, %eax
.LBB4_15:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i14
	leaq	64(%r14), %r12
	movq	64(%rbp), %rcx
	.p2align	4, 0x90
.LBB4_16:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB4_16
# BB#17:
	movl	72(%rbp), %eax
	movl	%eax, 72(%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 80(%r14)
	movslq	88(%rbp), %rax
	leaq	1(%rax), %r13
	testl	%r13d, %r13d
	je	.LBB4_18
# BB#19:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r13, %rdi
	cmovlq	%rax, %rdi
.Ltmp25:
	callq	_Znam
.Ltmp26:
# BB#20:                                # %.noexc22
	movq	%rax, 80(%r14)
	movb	$0, (%rax)
	movl	%r13d, 92(%r14)
	jmp	.LBB4_21
.LBB4_18:
	xorl	%eax, %eax
.LBB4_21:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i19
	movq	80(%rbp), %rcx
	.p2align	4, 0x90
.LBB4_22:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB4_22
# BB#23:
	movl	88(%rbp), %eax
	movl	%eax, 88(%r14)
	movb	106(%rbp), %al
	movb	%al, 106(%r14)
	movzwl	104(%rbp), %eax
	movw	%ax, 104(%r14)
	movq	96(%rbp), %rax
	movq	%rax, 96(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_26:
.Ltmp27:
	movq	%rax, %rbx
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB4_28
# BB#27:
	callq	_ZdaPv
	jmp	.LBB4_28
.LBB4_25:
.Ltmp24:
	movq	%rax, %rbx
.LBB4_28:                               # %_ZN11CStringBaseIcED2Ev.exit
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB4_30
# BB#29:
	callq	_ZdaPv
	jmp	.LBB4_30
.LBB4_24:
.Ltmp21:
	movq	%rax, %rbx
.LBB4_30:                               # %_ZN11CStringBaseIcED2Ev.exit24
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB4_32
# BB#31:
	callq	_ZdaPv
.LBB4_32:                               # %_ZN11CStringBaseIcED2Ev.exit25
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN8NArchive4NTar5CItemC2ERKS1_, .Lfunc_end4-_ZN8NArchive4NTar5CItemC2ERKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp19-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin1   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin1   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin1   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end4-.Ltmp26     #   Call between .Ltmp26 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NArchive4NTar11COutArchive16FillDataResidualEy
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar11COutArchive16FillDataResidualEy,@function
_ZN8NArchive4NTar11COutArchive16FillDataResidualEy: # @_ZN8NArchive4NTar11COutArchive16FillDataResidualEy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 24
	subq	$520, %rsp              # imm = 0x208
.Lcfi46:
	.cfi_def_cfa_offset 544
.Lcfi47:
	.cfi_offset %rbx, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	andl	$511, %esi              # imm = 0x1FF
	je	.LBB5_1
# BB#2:
	movl	$512, %ebp              # imm = 0x200
	subl	%esi, %ebp
	je	.LBB5_4
# BB#3:                                 # %.lr.ph.preheader
	movl	%ebp, %edx
	movq	%rsp, %rdi
	xorl	%esi, %esi
	callq	memset
.LBB5_4:                                # %._crit_edge
	movq	(%rbx), %rdi
	movl	%ebp, %edx
	movq	%rsp, %rsi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	jmp	.LBB5_5
.LBB5_1:
	xorl	%eax, %eax
.LBB5_5:
	addq	$520, %rsp              # imm = 0x208
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN8NArchive4NTar11COutArchive16FillDataResidualEy, .Lfunc_end5-_ZN8NArchive4NTar11COutArchive16FillDataResidualEy
	.cfi_endproc

	.section	.text._ZN8NArchive4NTar5CItemaSERKS1_,"axG",@progbits,_ZN8NArchive4NTar5CItemaSERKS1_,comdat
	.weak	_ZN8NArchive4NTar5CItemaSERKS1_
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar5CItemaSERKS1_,@function
_ZN8NArchive4NTar5CItemaSERKS1_:        # @_ZN8NArchive4NTar5CItemaSERKS1_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 48
.Lcfi54:
	.cfi_offset %rbx, -48
.Lcfi55:
	.cfi_offset %r12, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	cmpq	%r15, %r14
	je	.LBB6_1
# BB#2:
	movl	$0, 8(%r15)
	movq	(%r15), %rax
	movb	$0, (%rax)
	movslq	8(%r14), %rax
	leaq	1(%rax), %r12
	movl	12(%r15), %ebp
	cmpl	%ebp, %r12d
	jne	.LBB6_4
# BB#3:                                 # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
	movq	(%r15), %rbx
	jmp	.LBB6_28
.LBB6_1:                                # %_ZN11CStringBaseIcEaSERKS0_.exit.thread
	movups	16(%r14), %xmm0
	movups	32(%r14), %xmm1
	movups	%xmm1, 32(%r15)
	movups	%xmm0, 16(%r15)
	jmp	.LBB6_118
.LBB6_4:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r12, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	testl	%ebp, %ebp
	jle	.LBB6_27
# BB#5:                                 # %.preheader.i.i
	movslq	8(%r15), %rbp
	testq	%rbp, %rbp
	movq	(%r15), %rdi
	jle	.LBB6_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	cmpl	$31, %ebp
	jbe	.LBB6_7
# BB#14:                                # %min.iters.checked
	movq	%rbp, %rcx
	andq	$-32, %rcx
	je	.LBB6_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rbp), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB6_17
# BB#16:                                # %vector.memcheck
	leaq	(%rbx,%rbp), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB6_17
.LBB6_7:
	xorl	%ecx, %ecx
.LBB6_8:                                # %.lr.ph.i.i.preheader
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rbp), %rax
	subq	%rcx, %rax
	andq	$7, %rsi
	je	.LBB6_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB6_10:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB6_10
.LBB6_11:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB6_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rbp
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB6_13:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rbp
	jne	.LBB6_13
	jmp	.LBB6_26
.LBB6_25:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB6_27
.LBB6_26:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
.LBB6_27:                               # %._crit_edge17.i.i
	movq	%rbx, (%r15)
	movslq	8(%r15), %rax
	movb	$0, (%rbx,%rax)
	movl	%r12d, 12(%r15)
.LBB6_28:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
	movq	(%r14), %rax
	.p2align	4, 0x90
.LBB6_29:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB6_29
# BB#30:                                # %_ZN11CStringBaseIcEaSERKS0_.exit
	cmpq	%r15, %r14
	movl	8(%r14), %eax
	movl	%eax, 8(%r15)
	movups	16(%r14), %xmm0
	movups	32(%r14), %xmm1
	movups	%xmm1, 32(%r15)
	movups	%xmm0, 16(%r15)
	je	.LBB6_118
# BB#31:
	movl	$0, 56(%r15)
	movq	48(%r15), %rax
	movb	$0, (%rax)
	movslq	56(%r14), %rax
	leaq	1(%rax), %r12
	movl	60(%r15), %ebp
	cmpl	%ebp, %r12d
	jne	.LBB6_33
# BB#32:                                # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i8
	movq	48(%r15), %rbx
	jmp	.LBB6_57
.LBB6_33:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r12, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	testl	%ebp, %ebp
	jle	.LBB6_56
# BB#34:                                # %.preheader.i.i9
	movslq	56(%r15), %rbp
	testq	%rbp, %rbp
	movq	48(%r15), %rdi
	jle	.LBB6_54
# BB#35:                                # %.lr.ph.preheader.i.i10
	cmpl	$31, %ebp
	jbe	.LBB6_36
# BB#43:                                # %min.iters.checked62
	movq	%rbp, %rcx
	andq	$-32, %rcx
	je	.LBB6_36
# BB#44:                                # %vector.memcheck73
	leaq	(%rdi,%rbp), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB6_46
# BB#45:                                # %vector.memcheck73
	leaq	(%rbx,%rbp), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB6_46
.LBB6_36:
	xorl	%ecx, %ecx
.LBB6_37:                               # %.lr.ph.i.i15.preheader
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rbp), %rax
	subq	%rcx, %rax
	andq	$7, %rsi
	je	.LBB6_40
# BB#38:                                # %.lr.ph.i.i15.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB6_39:                               # %.lr.ph.i.i15.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB6_39
.LBB6_40:                               # %.lr.ph.i.i15.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB6_55
# BB#41:                                # %.lr.ph.i.i15.preheader.new
	subq	%rcx, %rbp
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB6_42:                               # %.lr.ph.i.i15
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rbp
	jne	.LBB6_42
	jmp	.LBB6_55
.LBB6_54:                               # %._crit_edge.i.i11
	testq	%rdi, %rdi
	je	.LBB6_56
.LBB6_55:                               # %._crit_edge.thread.i.i16
	callq	_ZdaPv
.LBB6_56:                               # %._crit_edge17.i.i17
	movq	%rbx, 48(%r15)
	movslq	56(%r15), %rax
	movb	$0, (%rbx,%rax)
	movl	%r12d, 60(%r15)
.LBB6_57:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i18
	movq	48(%r14), %rax
	.p2align	4, 0x90
.LBB6_58:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB6_58
# BB#59:                                # %_ZN11CStringBaseIcEaSERKS0_.exit22
	cmpq	%r15, %r14
	movl	56(%r14), %eax
	movl	%eax, 56(%r15)
	je	.LBB6_118
# BB#60:
	movl	$0, 72(%r15)
	movq	64(%r15), %rax
	movb	$0, (%rax)
	movslq	72(%r14), %rax
	leaq	1(%rax), %r12
	movl	76(%r15), %ebp
	cmpl	%ebp, %r12d
	jne	.LBB6_62
# BB#61:                                # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i24
	movq	64(%r15), %rbx
	jmp	.LBB6_86
.LBB6_62:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r12, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	testl	%ebp, %ebp
	jle	.LBB6_85
# BB#63:                                # %.preheader.i.i25
	movslq	72(%r15), %rbp
	testq	%rbp, %rbp
	movq	64(%r15), %rdi
	jle	.LBB6_83
# BB#64:                                # %.lr.ph.preheader.i.i26
	cmpl	$31, %ebp
	jbe	.LBB6_65
# BB#72:                                # %min.iters.checked89
	movq	%rbp, %rcx
	andq	$-32, %rcx
	je	.LBB6_65
# BB#73:                                # %vector.memcheck100
	leaq	(%rdi,%rbp), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB6_75
# BB#74:                                # %vector.memcheck100
	leaq	(%rbx,%rbp), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB6_75
.LBB6_65:
	xorl	%ecx, %ecx
.LBB6_66:                               # %.lr.ph.i.i31.preheader
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rbp), %rax
	subq	%rcx, %rax
	andq	$7, %rsi
	je	.LBB6_69
# BB#67:                                # %.lr.ph.i.i31.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB6_68:                               # %.lr.ph.i.i31.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB6_68
.LBB6_69:                               # %.lr.ph.i.i31.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB6_84
# BB#70:                                # %.lr.ph.i.i31.preheader.new
	subq	%rcx, %rbp
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB6_71:                               # %.lr.ph.i.i31
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rbp
	jne	.LBB6_71
	jmp	.LBB6_84
.LBB6_83:                               # %._crit_edge.i.i27
	testq	%rdi, %rdi
	je	.LBB6_85
.LBB6_84:                               # %._crit_edge.thread.i.i32
	callq	_ZdaPv
.LBB6_85:                               # %._crit_edge17.i.i33
	movq	%rbx, 64(%r15)
	movslq	72(%r15), %rax
	movb	$0, (%rbx,%rax)
	movl	%r12d, 76(%r15)
.LBB6_86:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i34
	movq	64(%r14), %rax
	.p2align	4, 0x90
.LBB6_87:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB6_87
# BB#88:                                # %_ZN11CStringBaseIcEaSERKS0_.exit38
	cmpq	%r15, %r14
	movl	72(%r14), %eax
	movl	%eax, 72(%r15)
	je	.LBB6_118
# BB#89:
	movl	$0, 88(%r15)
	movq	80(%r15), %rax
	movb	$0, (%rax)
	movslq	88(%r14), %rax
	leaq	1(%rax), %r12
	movl	92(%r15), %ebp
	cmpl	%ebp, %r12d
	jne	.LBB6_91
# BB#90:                                # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i40
	movq	80(%r15), %rbx
	jmp	.LBB6_115
.LBB6_91:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r12, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	testl	%ebp, %ebp
	jle	.LBB6_114
# BB#92:                                # %.preheader.i.i41
	movslq	88(%r15), %rbp
	testq	%rbp, %rbp
	movq	80(%r15), %rdi
	jle	.LBB6_112
# BB#93:                                # %.lr.ph.preheader.i.i42
	cmpl	$31, %ebp
	jbe	.LBB6_94
# BB#101:                               # %min.iters.checked116
	movq	%rbp, %rcx
	andq	$-32, %rcx
	je	.LBB6_94
# BB#102:                               # %vector.memcheck127
	leaq	(%rdi,%rbp), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB6_104
# BB#103:                               # %vector.memcheck127
	leaq	(%rbx,%rbp), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB6_104
.LBB6_94:
	xorl	%ecx, %ecx
.LBB6_95:                               # %.lr.ph.i.i47.preheader
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rbp), %rax
	subq	%rcx, %rax
	andq	$7, %rsi
	je	.LBB6_98
# BB#96:                                # %.lr.ph.i.i47.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB6_97:                               # %.lr.ph.i.i47.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB6_97
.LBB6_98:                               # %.lr.ph.i.i47.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB6_113
# BB#99:                                # %.lr.ph.i.i47.preheader.new
	subq	%rcx, %rbp
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB6_100:                              # %.lr.ph.i.i47
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rbp
	jne	.LBB6_100
	jmp	.LBB6_113
.LBB6_17:                               # %vector.body.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB6_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB6_20:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%rbx,%rdx)
	movups	%xmm1, 16(%rbx,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB6_20
	jmp	.LBB6_21
.LBB6_112:                              # %._crit_edge.i.i43
	testq	%rdi, %rdi
	je	.LBB6_114
.LBB6_113:                              # %._crit_edge.thread.i.i48
	callq	_ZdaPv
.LBB6_114:                              # %._crit_edge17.i.i49
	movq	%rbx, 80(%r15)
	movslq	88(%r15), %rax
	movb	$0, (%rbx,%rax)
	movl	%r12d, 92(%r15)
.LBB6_115:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i50
	movq	80(%r14), %rax
	.p2align	4, 0x90
.LBB6_116:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB6_116
# BB#117:                               # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i53
	movl	88(%r14), %eax
	movl	%eax, 88(%r15)
.LBB6_118:                              # %_ZN11CStringBaseIcEaSERKS0_.exit54
	movb	106(%r14), %al
	movb	%al, 106(%r15)
	movzwl	104(%r14), %eax
	movw	%ax, 104(%r15)
	movq	96(%r14), %rax
	movq	%rax, 96(%r15)
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_46:                               # %vector.body58.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB6_47
# BB#48:                                # %vector.body58.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB6_49:                               # %vector.body58.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%rbx,%rdx)
	movups	%xmm1, 16(%rbx,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB6_49
	jmp	.LBB6_50
.LBB6_75:                               # %vector.body85.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB6_76
# BB#77:                                # %vector.body85.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
.LBB6_78:                               # %vector.body85.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%rbx,%rdx)
	movups	%xmm1, 16(%rbx,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB6_78
	jmp	.LBB6_79
.LBB6_18:
	xorl	%edx, %edx
.LBB6_21:                               # %vector.body.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB6_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbx,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
	.p2align	4, 0x90
.LBB6_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB6_23
.LBB6_24:                               # %middle.block
	cmpq	%rcx, %rbp
	jne	.LBB6_8
	jmp	.LBB6_26
.LBB6_104:                              # %vector.body112.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB6_105
# BB#106:                               # %vector.body112.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
.LBB6_107:                              # %vector.body112.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%rbx,%rdx)
	movups	%xmm1, 16(%rbx,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB6_107
	jmp	.LBB6_108
.LBB6_47:
	xorl	%edx, %edx
.LBB6_50:                               # %vector.body58.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB6_53
# BB#51:                                # %vector.body58.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbx,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
.LBB6_52:                               # %vector.body58
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB6_52
.LBB6_53:                               # %middle.block59
	cmpq	%rcx, %rbp
	jne	.LBB6_37
	jmp	.LBB6_55
.LBB6_76:
	xorl	%edx, %edx
.LBB6_79:                               # %vector.body85.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB6_82
# BB#80:                                # %vector.body85.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbx,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
.LBB6_81:                               # %vector.body85
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB6_81
.LBB6_82:                               # %middle.block86
	cmpq	%rcx, %rbp
	jne	.LBB6_66
	jmp	.LBB6_84
.LBB6_105:
	xorl	%edx, %edx
.LBB6_108:                              # %vector.body112.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB6_111
# BB#109:                               # %vector.body112.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbx,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
.LBB6_110:                              # %vector.body112
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB6_110
.LBB6_111:                              # %middle.block113
	cmpq	%rcx, %rbp
	jne	.LBB6_95
	jmp	.LBB6_113
.Lfunc_end6:
	.size	_ZN8NArchive4NTar5CItemaSERKS1_, .Lfunc_end6-_ZN8NArchive4NTar5CItemaSERKS1_
	.cfi_endproc

	.text
	.globl	_ZN8NArchive4NTar11COutArchive17WriteFinishHeaderEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar11COutArchive17WriteFinishHeaderEv,@function
_ZN8NArchive4NTar11COutArchive17WriteFinishHeaderEv: # @_ZN8NArchive4NTar11COutArchive17WriteFinishHeaderEv
	.cfi_startproc
# BB#0:                                 # %.preheader
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 24
	subq	$520, %rsp              # imm = 0x208
.Lcfi61:
	.cfi_def_cfa_offset 544
.Lcfi62:
	.cfi_offset %rbx, -24
.Lcfi63:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	%rsp, %r14
	xorl	%esi, %esi
	movl	$512, %edx              # imm = 0x200
	movq	%r14, %rdi
	callq	memset
	movq	(%rbx), %rdi
	movl	$512, %edx              # imm = 0x200
	movq	%r14, %rsi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	testl	%eax, %eax
	jne	.LBB7_2
# BB#1:
	movq	(%rbx), %rdi
	movq	%rsp, %rsi
	movl	$512, %edx              # imm = 0x200
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
.LBB7_2:
	addq	$520, %rsp              # imm = 0x208
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	_ZN8NArchive4NTar11COutArchive17WriteFinishHeaderEv, .Lfunc_end7-_ZN8NArchive4NTar11COutArchive17WriteFinishHeaderEv
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN8NArchive4NTarL15MakeOctalStringEy,@function
_ZN8NArchive4NTarL15MakeOctalStringEy:  # @_ZN8NArchive4NTarL15MakeOctalStringEy
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi69:
	.cfi_def_cfa_offset 80
.Lcfi70:
	.cfi_offset %rbx, -48
.Lcfi71:
	.cfi_offset %r12, -40
.Lcfi72:
	.cfi_offset %r14, -32
.Lcfi73:
	.cfi_offset %r15, -24
.Lcfi74:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	%rsp, %rbx
	movl	$8, %edx
	movq	%rsi, %rdi
	movq	%rbx, %rsi
	callq	_Z21ConvertUInt64ToStringyPcj
	movl	$-1, %r12d
	.p2align	4, 0x90
.LBB8_1:                                # =>This Inner Loop Header: Depth=1
	incl	%r12d
	cmpb	$0, (%rbx)
	leaq	1(%rbx), %rbx
	jne	.LBB8_1
# BB#2:                                 # %_Z11MyStringLenIcEiPKT_.exit.i
	leal	1(%r12), %ebp
	movslq	%ebp, %rax
	cmpl	$-1, %r12d
	movq	$-1, %r15
	cmovgeq	%rax, %r15
	movq	%r15, %rdi
	callq	_Znam
	movq	%rax, %rbx
	movb	$0, (%rbx)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_3:                                # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rsp,%rax), %ecx
	movb	%cl, (%rbx,%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB8_3
# BB#4:                                 # %_ZN11CStringBaseIcEC2EPKc.exit
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
.Ltmp28:
	movq	%r15, %rdi
	callq	_Znam
.Ltmp29:
# BB#5:                                 # %.noexc
	movq	%rax, (%r14)
	movb	$0, (%rax)
	movl	%ebp, 12(%r14)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_6:                                # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx,%rcx), %edx
	movb	%dl, (%rax,%rcx)
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB8_6
# BB#7:                                 # %_ZN11CStringBaseIcEC2ERKS0_.exit.i
	movl	%r12d, 8(%r14)
.Ltmp31:
	movl	$32, %esi
	movq	%r14, %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp32:
# BB#8:                                 # %_ZN11CStringBaseIcED2Ev.exit
	movq	%rbx, %rdi
	callq	_ZdaPv
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_9:
.Ltmp33:
	movq	%rax, %r15
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB8_12
# BB#10:
	callq	_ZdaPv
	jmp	.LBB8_12
.LBB8_11:
.Ltmp30:
	movq	%rax, %r15
.LBB8_12:                               # %_ZN11CStringBaseIcED2Ev.exit2
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	%r15, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN8NArchive4NTarL15MakeOctalStringEy, .Lfunc_end8-_ZN8NArchive4NTarL15MakeOctalStringEy
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp28-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin2   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin2   #     jumps to .Ltmp33
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end8-.Ltmp32     #   Call between .Ltmp32 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIcEpLEc,"axG",@progbits,_ZN11CStringBaseIcEpLEc,comdat
	.weak	_ZN11CStringBaseIcEpLEc
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcEpLEc,@function
_ZN11CStringBaseIcEpLEc:                # @_ZN11CStringBaseIcEpLEc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi75:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi78:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi79:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi81:
	.cfi_def_cfa_offset 64
.Lcfi82:
	.cfi_offset %rbx, -56
.Lcfi83:
	.cfi_offset %r12, -48
.Lcfi84:
	.cfi_offset %r13, -40
.Lcfi85:
	.cfi_offset %r14, -32
.Lcfi86:
	.cfi_offset %r15, -24
.Lcfi87:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r13
	movl	8(%r13), %ebp
	movl	12(%r13), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	$1, %eax
	jg	.LBB9_28
# BB#1:
	leal	-1(%rax), %ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB9_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB9_3:                                # %select.end
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rsi,%rbx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB9_28
# BB#4:
	addl	%ebx, %esi
	movslq	%r15d, %rax
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB9_27
# BB#5:                                 # %.preheader.i.i
	movq	(%r13), %rdi
	testl	%ebp, %ebp
	jle	.LBB9_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	movslq	%ebp, %rax
	cmpl	$31, %ebp
	jbe	.LBB9_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB9_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r12
	jae	.LBB9_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB9_17
.LBB9_7:
	xorl	%ecx, %ecx
.LBB9_8:                                # %.lr.ph.i.i.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB9_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB9_10:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r12,%rcx)
	incq	%rcx
	incq	%rbp
	jne	.LBB9_10
.LBB9_11:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB9_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r12,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB9_13:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB9_13
	jmp	.LBB9_26
.LBB9_25:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB9_27
.LBB9_26:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movl	8(%r13), %ebp
.LBB9_27:
	movq	%r12, (%r13)
	movslq	%ebp, %rax
	movb	$0, (%r12,%rax)
	movl	%r15d, 12(%r13)
.LBB9_28:                               # %_ZN11CStringBaseIcE10GrowLengthEi.exit
	movq	(%r13), %rax
	movslq	%ebp, %rcx
	movb	%r14b, (%rax,%rcx)
	movq	(%r13), %rax
	movslq	8(%r13), %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%r13)
	movb	$0, 1(%rax,%rcx)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_17:                               # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB9_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_20:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx), %xmm0
	movups	16(%rdi,%rbx), %xmm1
	movups	%xmm0, (%r12,%rbx)
	movups	%xmm1, 16(%r12,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB9_20
	jmp	.LBB9_21
.LBB9_18:
	xorl	%ebx, %ebx
.LBB9_21:                               # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB9_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB9_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB9_23
.LBB9_24:                               # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB9_8
	jmp	.LBB9_26
.Lfunc_end9:
	.size	_ZN11CStringBaseIcEpLEc, .Lfunc_end9-_ZN11CStringBaseIcEpLEc
	.cfi_endproc

	.section	.text._ZNK11CStringBaseIcE3MidEii,"axG",@progbits,_ZNK11CStringBaseIcE3MidEii,comdat
	.weak	_ZNK11CStringBaseIcE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIcE3MidEii,@function
_ZNK11CStringBaseIcE3MidEii:            # @_ZNK11CStringBaseIcE3MidEii
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi91:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi92:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi94:
	.cfi_def_cfa_offset 64
.Lcfi95:
	.cfi_offset %rbx, -56
.Lcfi96:
	.cfi_offset %r12, -48
.Lcfi97:
	.cfi_offset %r13, -40
.Lcfi98:
	.cfi_offset %r14, -32
.Lcfi99:
	.cfi_offset %r15, -24
.Lcfi100:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %rbp
	leal	(%rcx,%r12), %edx
	movl	8(%r15), %eax
	movl	%eax, %r14d
	subl	%r12d, %r14d
	cmpl	%eax, %edx
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB10_9
# BB#1:
	leal	(%r14,%r12), %ecx
	cmpl	%eax, %ecx
	jne	.LBB10_9
# BB#2:
	movslq	%eax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB10_3
# BB#4:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%rbx, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%rbp)
	movb	$0, (%rax)
	movl	%ebx, 12(%rbp)
	jmp	.LBB10_5
.LBB10_9:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %rbx
	movb	$0, (%rbx)
	leal	1(%r14), %r13d
	cmpl	$4, %r13d
	je	.LBB10_13
# BB#10:
	movslq	%r13d, %rax
	cmpl	$-1, %r14d
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
.Ltmp34:
	callq	_Znam
	movl	%r13d, %ecx
	movq	%rax, %r13
.Ltmp35:
# BB#11:                                # %_ZN11CStringBaseIcE11SetCapacityEi.exit
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movq	%rbx, %rdi
	callq	_ZdaPv
	movb	$0, (%r13)
	testl	%r14d, %r14d
	jle	.LBB10_33
# BB#12:
	movq	%r13, %rbx
	movl	4(%rsp), %r13d          # 4-byte Reload
.LBB10_13:                              # %.lr.ph
	movslq	%r12d, %r9
	movslq	%r14d, %rax
	movq	(%r15), %rcx
	testq	%rax, %rax
	movl	$1, %r10d
	cmovgq	%rax, %r10
	cmpq	$31, %r10
	jbe	.LBB10_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775776, %rsi # imm = 0x7FFFFFFFFFFFFFE0
	andq	%r10, %rsi
	je	.LBB10_14
# BB#18:                                # %vector.memcheck
	testq	%rax, %rax
	movl	$1, %edx
	cmovgq	%rax, %rdx
	leaq	(%rdx,%r9), %rdi
	addq	%rcx, %rdi
	cmpq	%rdi, %rbx
	jae	.LBB10_20
# BB#19:                                # %vector.memcheck
	addq	%rbx, %rdx
	leaq	(%rcx,%r9), %rdi
	cmpq	%rdx, %rdi
	jae	.LBB10_20
.LBB10_14:
	xorl	%esi, %esi
.LBB10_15:                              # %scalar.ph.preheader
	addq	%r9, %rcx
	.p2align	4, 0x90
.LBB10_16:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx,%rsi), %edx
	movb	%dl, (%rbx,%rsi)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB10_16
	jmp	.LBB10_28
.LBB10_3:
	xorl	%eax, %eax
.LBB10_5:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB10_6:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB10_6
# BB#7:
	movl	8(%r15), %eax
	movl	%eax, 8(%rbp)
	jmp	.LBB10_8
.LBB10_33:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.._crit_edge_crit_edge
	movslq	%r14d, %rax
	movq	%r13, %rbx
	movl	4(%rsp), %r13d          # 4-byte Reload
	jmp	.LBB10_28
.LBB10_20:                              # %vector.body.preheader
	movq	%rbp, %r11
	leaq	-32(%rsi), %r8
	movl	%r8d, %edx
	shrl	$5, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB10_21
# BB#22:                                # %vector.body.prol.preheader
	leaq	16(%rcx,%r9), %rdi
	negq	%rdx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB10_23:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdi,%rbp), %xmm0
	movups	(%rdi,%rbp), %xmm1
	movups	%xmm0, (%rbx,%rbp)
	movups	%xmm1, 16(%rbx,%rbp)
	addq	$32, %rbp
	incq	%rdx
	jne	.LBB10_23
	jmp	.LBB10_24
.LBB10_21:
	xorl	%ebp, %ebp
.LBB10_24:                              # %vector.body.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB10_27
# BB#25:                                # %vector.body.preheader.new
	movq	%rsi, %rdx
	subq	%rbp, %rdx
	leaq	112(%rbx,%rbp), %rdi
	addq	%r9, %rbp
	leaq	112(%rcx,%rbp), %rbp
	.p2align	4, 0x90
.LBB10_26:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rdi)
	movups	%xmm1, -96(%rdi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rdi)
	movups	%xmm1, -64(%rdi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rdi)
	movups	%xmm1, -32(%rdi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	subq	$-128, %rdi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB10_26
.LBB10_27:                              # %middle.block
	cmpq	%rsi, %r10
	movq	%r11, %rbp
	jne	.LBB10_15
.LBB10_28:                              # %._crit_edge
	movb	$0, (%rbx,%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	movslq	%r13d, %rax
	cmpl	$-1, %r14d
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
.Ltmp36:
	callq	_Znam
.Ltmp37:
# BB#29:                                # %.noexc23
	movq	%rax, (%rbp)
	movb	$0, (%rax)
	movl	%r13d, 12(%rbp)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB10_30:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i20
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx,%rcx), %edx
	movb	%dl, (%rax,%rcx)
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB10_30
# BB#31:                                # %_ZN11CStringBaseIcED2Ev.exit25
	movl	%r14d, 8(%rbp)
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB10_8:
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_32:                              # %_ZN11CStringBaseIcED2Ev.exit
.Ltmp38:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZNK11CStringBaseIcE3MidEii, .Lfunc_end10-_ZNK11CStringBaseIcE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp34-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp37-.Ltmp34         #   Call between .Ltmp34 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin3   #     jumps to .Ltmp38
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Lfunc_end10-.Ltmp37    #   Call between .Ltmp37 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
