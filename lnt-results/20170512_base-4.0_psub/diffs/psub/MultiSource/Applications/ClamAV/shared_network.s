	.text
	.file	"shared_network.bc"
	.globl	r_gethostbyname
	.p2align	4, 0x90
	.type	r_gethostbyname,@function
r_gethostbyname:                        # @r_gethostbyname
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	testq	%rdi, %rdi
	movl	$-1, %eax
	je	.LBB0_5
# BB#1:
	testq	%rbx, %rbx
	je	.LBB0_5
# BB#2:
	callq	gethostbyname
	testq	%rax, %rax
	je	.LBB0_3
# BB#4:
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movups	%xmm1, 16(%rbx)
	movups	%xmm0, (%rbx)
	xorl	%eax, %eax
.LBB0_5:
	popq	%rbx
	retq
.LBB0_3:
	callq	__h_errno_location
	movl	(%rax), %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	r_gethostbyname, .Lfunc_end0-r_gethostbyname
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
