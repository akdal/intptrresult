	.text
	.file	"mandel-2.bc"
	.globl	sqr
	.p2align	4, 0x90
	.type	sqr,@function
sqr:                                    # @sqr
	.cfi_startproc
# BB#0:
	mulsd	%xmm0, %xmm0
	retq
.Lfunc_end0:
	.size	sqr, .Lfunc_end0-sqr
	.cfi_endproc

	.globl	cnorm2
	.p2align	4, 0x90
	.type	cnorm2,@function
cnorm2:                                 # @cnorm2
	.cfi_startproc
# BB#0:
	mulsd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm1
	addsd	%xmm1, %xmm0
	retq
.Lfunc_end1:
	.size	cnorm2, .Lfunc_end1-cnorm2
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4616189618054758400     # double 4
	.text
	.globl	loop
	.p2align	4, 0x90
	.type	loop,@function
loop:                                   # @loop
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movapd	%xmm1, %xmm5
	movapd	%xmm0, %xmm6
	mulsd	%xmm0, %xmm0
	movapd	%xmm5, %xmm4
	mulsd	%xmm4, %xmm4
	movapd	%xmm0, %xmm1
	addsd	%xmm4, %xmm1
	movl	$1, %ebx
	movsd	.LCPI2_0(%rip), %xmm7   # xmm7 = mem[0],zero
	ucomisd	%xmm1, %xmm7
	jb	.LBB2_7
# BB#1:                                 # %.lr.ph.preheader
	movl	$2, %eax
	movapd	%xmm5, %xmm3
	movapd	%xmm6, %xmm2
	jmp	.LBB2_2
	.p2align	4, 0x90
.LBB2_3:                                #   in Loop: Header=BB2_2 Depth=1
	movapd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm1
	subsd	%xmm4, %xmm0
	addsd	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm0
	jnp	.LBB2_6
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	ucomisd	%xmm1, %xmm1
	jp	.LBB2_5
.LBB2_6:                                #   in Loop: Header=BB2_2 Depth=1
	movapd	%xmm0, %xmm2
	addsd	%xmm6, %xmm2
	addsd	%xmm5, %xmm1
	movapd	%xmm2, %xmm0
	mulsd	%xmm0, %xmm0
	movapd	%xmm1, %xmm4
	mulsd	%xmm4, %xmm4
	movapd	%xmm0, %xmm3
	addsd	%xmm4, %xmm3
	leal	1(%rbx), %eax
	ucomisd	%xmm3, %xmm7
	movapd	%xmm1, %xmm3
	jae	.LBB2_2
	jmp	.LBB2_7
.LBB2_5:                                #   in Loop: Header=BB2_2 Depth=1
	movapd	%xmm2, %xmm0
	movapd	%xmm3, %xmm1
	movsd	%xmm5, 8(%rsp)          # 8-byte Spill
	movsd	%xmm6, (%rsp)           # 8-byte Spill
	callq	__muldc3
	movsd	.LCPI2_0(%rip), %xmm7   # xmm7 = mem[0],zero
	movsd	(%rsp), %xmm6           # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movsd	8(%rsp), %xmm5          # 8-byte Reload
                                        # xmm5 = mem[0],zero
	jmp	.LBB2_6
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebx
	leal	-1(%rbx), %eax
	cmpl	max_i(%rip), %eax
	jl	.LBB2_3
.LBB2_7:                                # %.critedge
	movl	%ebx, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end2:
	.size	loop, .Lfunc_end2-loop
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	-4592686457499418624    # double -39
.LCPI3_1:
	.quad	4630826316843712512     # double 40
.LCPI3_2:
	.quad	-4620693217682128896    # double -0.5
.LCPI3_3:
	.quad	4616189618054758400     # double 4
.LCPI3_4:
	.quad	4607182418800017408     # double 1
.LCPI3_5:
	.quad	0                       # double 0
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 80
.Lcfi8:
	.cfi_offset %rbx, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movl	$-39, %r14d
	movsd	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movl	$.L.str, %r15d
	.p2align	4, 0x90
.LBB3_1:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_2 Depth 2
                                        #       Child Loop BB3_4 Depth 3
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	divsd	.LCPI3_1(%rip), %xmm0
	addsd	.LCPI3_2(%rip), %xmm0
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movsd	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movl	$-39, %ebp
	.p2align	4, 0x90
.LBB3_2:                                #   Parent Loop BB3_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_4 Depth 3
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	movapd	%xmm0, %xmm6
	divsd	.LCPI3_1(%rip), %xmm6
	movapd	%xmm6, %xmm7
	mulsd	.LCPI3_5, %xmm7
	addsd	24(%rsp), %xmm7         # 8-byte Folded Reload
	movapd	%xmm7, %xmm0
	mulsd	%xmm0, %xmm0
	movapd	%xmm6, %xmm4
	mulsd	%xmm4, %xmm4
	movapd	%xmm4, %xmm1
	addsd	%xmm0, %xmm1
	movl	$1, %ebx
	movsd	.LCPI3_3(%rip), %xmm5   # xmm5 = mem[0],zero
	ucomisd	%xmm1, %xmm5
	jb	.LBB3_10
# BB#3:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB3_2 Depth=2
	movl	$1, %eax
	movapd	%xmm6, %xmm3
	movapd	%xmm7, %xmm2
	jmp	.LBB3_4
.LBB3_7:                                #   in Loop: Header=BB3_4 Depth=3
	movapd	%xmm2, %xmm0
	movapd	%xmm3, %xmm1
	movsd	%xmm6, 8(%rsp)          # 8-byte Spill
	movsd	%xmm7, (%rsp)           # 8-byte Spill
	callq	__muldc3
	movsd	(%rsp), %xmm7           # 8-byte Reload
                                        # xmm7 = mem[0],zero
	movsd	8(%rsp), %xmm6          # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movsd	.LCPI3_3(%rip), %xmm5   # xmm5 = mem[0],zero
	jmp	.LBB3_8
	.p2align	4, 0x90
.LBB3_4:                                # %.lr.ph.i
                                        #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, %ebx
	cmpl	max_i(%rip), %ebx
	jge	.LBB3_9
# BB#5:                                 #   in Loop: Header=BB3_4 Depth=3
	movapd	%xmm3, %xmm1
	mulsd	%xmm2, %xmm1
	subsd	%xmm4, %xmm0
	addsd	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm0
	jnp	.LBB3_8
# BB#6:                                 #   in Loop: Header=BB3_4 Depth=3
	ucomisd	%xmm1, %xmm1
	jp	.LBB3_7
.LBB3_8:                                #   in Loop: Header=BB3_4 Depth=3
	leal	1(%rbx), %eax
	movapd	%xmm0, %xmm2
	addsd	%xmm7, %xmm2
	addsd	%xmm6, %xmm1
	movapd	%xmm2, %xmm0
	mulsd	%xmm0, %xmm0
	movapd	%xmm1, %xmm4
	mulsd	%xmm4, %xmm4
	movapd	%xmm0, %xmm3
	addsd	%xmm4, %xmm3
	ucomisd	%xmm3, %xmm5
	movapd	%xmm1, %xmm3
	jae	.LBB3_4
.LBB3_9:                                # %loop.exit.loopexit
                                        #   in Loop: Header=BB3_2 Depth=2
	incl	%ebx
.LBB3_10:                               # %loop.exit
                                        #   in Loop: Header=BB3_2 Depth=2
	cmpl	max_i(%rip), %ebx
	movl	$.L.str.1, %edi
	cmovgq	%r15, %rdi
	xorl	%eax, %eax
	callq	printf
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	.LCPI3_4(%rip), %xmm0
	incl	%ebp
	cmpl	$39, %ebp
	jne	.LBB3_2
# BB#11:                                #   in Loop: Header=BB3_1 Depth=1
	movl	$10, %edi
	callq	putchar
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	.LCPI3_4(%rip), %xmm0
	incl	%r14d
	cmpl	$39, %r14d
	jne	.LBB3_1
# BB#12:
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	main, .Lfunc_end3-main
	.cfi_endproc

	.type	max_i,@object           # @max_i
	.data
	.globl	max_i
	.p2align	2
max_i:
	.long	65536                   # 0x10000
	.size	max_i, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"*"
	.size	.L.str, 2

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	" "
	.size	.L.str.1, 2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
