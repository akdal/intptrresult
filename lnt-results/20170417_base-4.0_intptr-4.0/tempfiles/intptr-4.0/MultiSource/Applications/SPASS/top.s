	.text
	.file	"top.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	4294967254              # 0xffffffd6
	.long	4294967254              # 0xffffffd6
	.long	4294967254              # 0xffffffd6
	.long	4294967254              # 0xffffffd6
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$392, %rsp              # imm = 0x188
.Lcfi6:
	.cfi_def_cfa_offset 448
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movl	%edi, %r14d
	callq	clock_Init
	movl	$1, %edi
	callq	clock_StartCounter
	movq	$-1, %rdi
	callq	memory_Init
	movl	$memory_FreeAllMem, %edi
	callq	atexit
	movl	$1, %edi
	callq	symbol_Init
	movl	$0, stack_POINTER(%rip)
	callq	hash_Init
	callq	sort_Init
	callq	term_Init
	movl	$16000, %edi            # imm = 0x3E80
	callq	memory_Malloc
	movl	$36, %ecx
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [4294967254,4294967254,4294967254,4294967254]
	.p2align	4, 0x90
.LBB0_1:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -144(%rax,%rcx,4)
	movups	%xmm0, -128(%rax,%rcx,4)
	movups	%xmm0, -112(%rax,%rcx,4)
	movups	%xmm0, -96(%rax,%rcx,4)
	movups	%xmm0, -80(%rax,%rcx,4)
	movups	%xmm0, -64(%rax,%rcx,4)
	movups	%xmm0, -48(%rax,%rcx,4)
	movups	%xmm0, -32(%rax,%rcx,4)
	movups	%xmm0, -16(%rax,%rcx,4)
	movups	%xmm0, (%rax,%rcx,4)
	addq	$40, %rcx
	cmpq	$4036, %rcx             # imm = 0xFC4
	jne	.LBB0_1
# BB#2:                                 # %vector.body903
	movl	$1, %r12d
	movl	$1, %edi
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	%rax, %rsi
	callq	fol_Init
	callq	cont_Init
	callq	unify_Init
	callq	flag_Init
	callq	subs_Init
	callq	clause_Init
	callq	red_Init
	callq	ren_Init
	callq	dp_Init
	callq	opts_Init
	movq	$0, ana_FINITEMONADICPREDICATES(%rip)
	callq	cc_Init
	callq	prfs_Create
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	$384, %edi              # imm = 0x180
	callq	memory_Malloc
	movl	flag_CLEAN(%rip), %ecx
	movd	%ecx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, (%rax)
	movdqu	%xmm0, 16(%rax)
	movdqu	%xmm0, 32(%rax)
	movdqu	%xmm0, 48(%rax)
	movdqu	%xmm0, 64(%rax)
	movdqu	%xmm0, 80(%rax)
	movdqu	%xmm0, 96(%rax)
	movdqu	%xmm0, 112(%rax)
	movdqu	%xmm0, 128(%rax)
	movdqu	%xmm0, 144(%rax)
	movdqu	%xmm0, 160(%rax)
	movdqu	%xmm0, 176(%rax)
	movdqu	%xmm0, 192(%rax)
	movdqu	%xmm0, 208(%rax)
	movdqu	%xmm0, 224(%rax)
	movdqu	%xmm0, 240(%rax)
	movdqu	%xmm0, 256(%rax)
	movdqu	%xmm0, 272(%rax)
	movdqu	%xmm0, 288(%rax)
	movdqu	%xmm0, 304(%rax)
	movdqu	%xmm0, 320(%rax)
	movdqu	%xmm0, 336(%rax)
	movdqu	%xmm0, 352(%rax)
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movdqa	%xmm0, 80(%rsp)         # 16-byte Spill
	movdqu	%xmm0, 368(%rax)
	callq	opts_DeclareSPASSFlagsAsOptions
	movl	$.L.str, %edi
	xorl	%esi, %esi
	callq	opts_Declare
	movl	%eax, top_RemoveFileOptId(%rip)
	movl	%r14d, %edi
	movq	%r13, %rsi
	callq	opts_Read
	testl	%eax, %eax
	je	.LBB0_487
# BB#3:                                 # %.preheader538.preheader
	xorl	%ebp, %ebp
	movq	96(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_4:                                # %.preheader538
                                        # =>This Inner Loop Header: Depth=1
	callq	flag_DefaultStore
	movl	(%rax,%rbp,4), %edx
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	flag_SetFlagValue
	incq	%rbp
	cmpq	$96, %rbp
	jne	.LBB0_4
# BB#5:                                 # %flag_InitStoreByDefaults.exit
	movabsq	$-30064771072, %r15     # imm = 0xFFFFFFF900000000
	movq	%rbx, %rdi
	callq	opts_SetFlags
	callq	opts_Indicator
	cmpl	%r14d, %eax
	jl	.LBB0_7
# BB#6:
	cmpl	$0, 4(%rbx)
	je	.LBB0_56
.LBB0_7:                                # %flag_InitStoreByDefaults.exit._crit_edge
	movq	$0, 232(%rsp)
	movq	$0, 248(%rsp)
	movq	$0, 216(%rsp)
	movq	$0, 128(%rsp)
	movq	$0, 168(%rsp)
	cmpl	$0, 4(%rbx)
	je	.LBB0_9
# BB#8:
	movq	$0, top_InputFile(%rip)
	movq	stdin(%rip), %rbp
	jmp	.LBB0_10
.LBB0_9:
	callq	opts_Indicator
	cltq
	movq	(%r13,%rax,8), %rdi
	movq	%rdi, top_InputFile(%rip)
	movl	$.L.str.6, %esi
	callq	misc_OpenFile
	movq	%rax, %rbp
.LBB0_10:                               # %vector.body917
	movl	$2, %edi
	callq	clock_StartCounter
	movq	96(%rsp), %rbx          # 8-byte Reload
	movdqa	80(%rsp), %xmm0         # 16-byte Reload
	movdqu	%xmm0, (%rbx)
	movdqu	%xmm0, 16(%rbx)
	movdqu	%xmm0, 32(%rbx)
	movdqu	%xmm0, 48(%rbx)
	movdqu	%xmm0, 64(%rbx)
	movdqu	%xmm0, 80(%rbx)
	movdqu	%xmm0, 96(%rbx)
	movdqu	%xmm0, 112(%rbx)
	movdqu	%xmm0, 128(%rbx)
	movdqu	%xmm0, 144(%rbx)
	movdqu	%xmm0, 160(%rbx)
	movdqu	%xmm0, 176(%rbx)
	movdqu	%xmm0, 192(%rbx)
	movdqu	%xmm0, 208(%rbx)
	movdqu	%xmm0, 224(%rbx)
	movdqu	%xmm0, 240(%rbx)
	movdqu	%xmm0, 256(%rbx)
	movdqu	%xmm0, 272(%rbx)
	movdqu	%xmm0, 288(%rbx)
	movdqu	%xmm0, 304(%rbx)
	movdqu	%xmm0, 320(%rbx)
	movdqu	%xmm0, 336(%rbx)
	movdqu	%xmm0, 352(%rbx)
	movdqu	%xmm0, 368(%rbx)
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	leaq	240(%rsp), %rax
	leaq	176(%rsp), %rcx
	leaq	136(%rsp), %r8
	leaq	224(%rsp), %r9
	movq	%rbp, 272(%rsp)         # 8-byte Spill
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	160(%rsp), %rdx         # 8-byte Reload
	pushq	%rax
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	dfg_DFGParser
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	movq	%rax, 32(%rsp)
	movq	%rbx, %rdi
	callq	opts_SetFlags
	movq	%rbx, %rax
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	104(%rcx), %rsi
	movq	112(%rcx), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	xorl	%ebx, %ebx
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_11:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rbx,4), %edx
	cmpl	flag_CLEAN(%rip), %edx
	je	.LBB0_13
# BB#12:                                #   in Loop: Header=BB0_11 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%ebx, %esi
	callq	flag_SetFlagValue
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	96(%rsp), %rax          # 8-byte Reload
.LBB0_13:                               #   in Loop: Header=BB0_11 Depth=1
	incq	%rbx
	cmpq	$96, %rbx
	jne	.LBB0_11
# BB#14:                                # %vector.memcheck
	leaq	16000(%rsi), %rax
	movq	152(%rsp), %rdx         # 8-byte Reload
	leaq	16000(%rdx), %rcx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	cmpq	%rcx, %rsi
	movq	%rax, 72(%rsp)          # 8-byte Spill
	jae	.LBB0_18
# BB#15:                                # %vector.memcheck
	cmpq	%rax, %rdx
	jae	.LBB0_18
# BB#16:                                # %flag_TransferSetFlags.exit.preheader
	movl	$7, %eax
	.p2align	4, 0x90
.LBB0_17:                               # %flag_TransferSetFlags.exit
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rdx,%rax,4), %ecx
	movl	%ecx, -28(%rsi,%rax,4)
	movl	-24(%rdx,%rax,4), %ecx
	movl	%ecx, -24(%rsi,%rax,4)
	movl	-20(%rdx,%rax,4), %ecx
	movl	%ecx, -20(%rsi,%rax,4)
	movl	-16(%rdx,%rax,4), %ecx
	movl	%ecx, -16(%rsi,%rax,4)
	movl	-12(%rdx,%rax,4), %ecx
	movl	%ecx, -12(%rsi,%rax,4)
	movl	-8(%rdx,%rax,4), %ecx
	movl	%ecx, -8(%rsi,%rax,4)
	movl	-4(%rdx,%rax,4), %ecx
	movl	%ecx, -4(%rsi,%rax,4)
	movl	(%rdx,%rax,4), %ecx
	movl	%ecx, (%rsi,%rax,4)
	addq	$8, %rax
	cmpq	$4007, %rax             # imm = 0xFA7
	jne	.LBB0_17
	jmp	.LBB0_20
.LBB0_18:                               # %vector.body931.preheader
	movl	$36, %eax
	.p2align	4, 0x90
.LBB0_19:                               # %vector.body931
                                        # =>This Inner Loop Header: Depth=1
	movups	-144(%rdx,%rax,4), %xmm0
	movups	-128(%rdx,%rax,4), %xmm1
	movups	%xmm0, -144(%rsi,%rax,4)
	movups	%xmm1, -128(%rsi,%rax,4)
	movups	-112(%rdx,%rax,4), %xmm0
	movups	-96(%rdx,%rax,4), %xmm1
	movups	%xmm0, -112(%rsi,%rax,4)
	movups	%xmm1, -96(%rsi,%rax,4)
	movups	-80(%rdx,%rax,4), %xmm0
	movups	-64(%rdx,%rax,4), %xmm1
	movups	%xmm0, -80(%rsi,%rax,4)
	movups	%xmm1, -64(%rsi,%rax,4)
	movups	-48(%rdx,%rax,4), %xmm0
	movups	-32(%rdx,%rax,4), %xmm1
	movups	%xmm0, -48(%rsi,%rax,4)
	movups	%xmm1, -32(%rsi,%rax,4)
	movdqu	-16(%rdx,%rax,4), %xmm0
	movups	(%rdx,%rax,4), %xmm1
	movdqu	%xmm0, -16(%rsi,%rax,4)
	movups	%xmm1, (%rsi,%rax,4)
	addq	$40, %rax
	cmpq	$4036, %rax             # imm = 0xFC4
	jne	.LBB0_19
.LBB0_20:                               # %symbol_TransferPrecedence.exit
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 8(%rax)
	jne	.LBB0_24
# BB#21:                                # %symbol_TransferPrecedence.exit
	movq	168(%rsp), %rax
	testq	%rax, %rax
	jne	.LBB0_24
# BB#22:                                # %symbol_TransferPrecedence.exit
	movq	128(%rsp), %rax
	testq	%rax, %rax
	jne	.LBB0_24
# BB#23:                                # %symbol_TransferPrecedence.exit
	movq	32(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_490
.LBB0_24:
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rdi
	callq	cnf_Init
	cmpl	$0, 8(%rbx)
	je	.LBB0_26
# BB#25:
	movl	$9, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
.LBB0_26:
	cmpl	$0, 36(%rbx)
	movq	%r13, 80(%rsp)          # 8-byte Spill
	je	.LBB0_29
# BB#27:
	callq	sharing_IndexCreate
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rax, 88(%rcx)
	cmpl	$0, 36(%rbx)
	je	.LBB0_29
# BB#28:
	callq	hsh_Create
	movq	%rax, 160(%rsp)         # 8-byte Spill
	callq	hsh_Create
	movq	%rax, 64(%rsp)          # 8-byte Spill
	jmp	.LBB0_30
.LBB0_29:
	xorl	%eax, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 160(%rsp)         # 8-byte Spill
.LBB0_30:
	movq	128(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB0_32
	.p2align	4, 0x90
.LBB0_31:                               # %.lr.ph657
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rbx
	movl	fol_NOT(%rip), %r12d
	movq	(%rbx), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	$0, (%rax)
	movl	%r12d, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, (%rbx)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB0_31
.LBB0_32:                               # %._crit_edge658
	movl	$2, %edi
	callq	clock_StopPassedTime
	movq	top_InputFile(%rip), %rsi
	testq	%rsi, %rsi
	je	.LBB0_35
# BB#33:
	movq	264(%rsp), %rdi         # 8-byte Reload
	callq	misc_CloseFile
	movl	top_RemoveFileOptId(%rip), %edi
	callq	opts_IsSet
	testl	%eax, %eax
	je	.LBB0_35
# BB#34:
	movq	top_InputFile(%rip), %rdi
	callq	remove
.LBB0_35:
	movl	$3, %edi
	callq	clock_StartCounter
	movq	168(%rsp), %rbx
	cmpq	$0, 32(%rsp)
	movq	80(%rsp), %r13          # 8-byte Reload
	je	.LBB0_42
# BB#36:
	movq	%rbx, %rdi
	callq	dfg_DeleteFormulaPairList
	movq	216(%rsp), %rdi
	callq	dfg_DeleteFormulaPairList
	movq	128(%rsp), %rdi
	callq	dfg_DeleteFormulaPairList
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 204(%rax)
	je	.LBB0_40
# BB#37:
	movq	32(%rsp), %rsi
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rbx, %rdi
	callq	def_ExtractDefsFromClauselist
	movq	%rbx, %rdi
	callq	def_FlattenDefinitionsDestructive
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB0_40
# BB#38:                                # %.lr.ph654.preheader
	movq	32(%rsp), %rax
	xorl	%ecx, %ecx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movq	(%rsp), %rbp            # 8-byte Reload
	.p2align	4, 0x90
.LBB0_39:                               # %.lr.ph654
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movl	$1, %ecx
	movq	%rbp, %rdi
	movq	%rax, %rdx
	callq	def_ApplyDefToClauselist
	movq	%rax, 32(%rsp)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_39
	jmp	.LBB0_41
.LBB0_40:
	xorl	%eax, %eax
	movq	%rax, 144(%rsp)         # 8-byte Spill
.LBB0_41:
	xorl	%eax, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	jmp	.LBB0_73
.LBB0_42:                               # %.preheader536
	testq	%rbx, %rbx
	je	.LBB0_49
# BB#43:                                # %.lr.ph650
	leaq	272(%rsp), %r12
	xorl	%ebp, %ebp
	jmp	.LBB0_44
	.p2align	4, 0x90
.LBB0_45:                               #   in Loop: Header=BB0_44 Depth=1
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movl	%ebp, %edx
	callq	sprintf
	movq	%r12, %rdi
	callq	string_StringCopy
	movq	%rax, %rcx
	movq	8(%rbx), %rax
	movq	%rcx, 8(%rax)
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 36(%rax)
	je	.LBB0_48
# BB#46:                                #   in Loop: Header=BB0_44 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 100(%rax)
	je	.LBB0_48
# BB#47:                                #   in Loop: Header=BB0_44 Depth=1
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	callq	fol_PrettyPrintDFG
	jmp	.LBB0_48
	.p2align	4, 0x90
.LBB0_44:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	cmpq	$0, 8(%rax)
	je	.LBB0_45
.LBB0_48:                               #   in Loop: Header=BB0_44 Depth=1
	movq	(%rbx), %rbx
	incl	%ebp
	testq	%rbx, %rbx
	jne	.LBB0_44
.LBB0_49:                               # %.preheader535
	movq	216(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB0_59
# BB#50:                                # %.lr.ph645
	leaq	272(%rsp), %r12
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_51:                               # =>This Inner Loop Header: Depth=1
	movl	$.L.str.10, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sprintf
	movq	%r12, %rdi
	callq	string_StringCopy
	movq	%rax, %rbp
	movq	8(%rbx), %rax
	movq	%rbp, 8(%rax)
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 36(%rax)
	je	.LBB0_54
# BB#52:                                #   in Loop: Header=BB0_51 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 100(%rax)
	je	.LBB0_54
# BB#53:                                #   in Loop: Header=BB0_51 Depth=1
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	printf
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	callq	fol_PrettyPrintDFG
.LBB0_54:                               #   in Loop: Header=BB0_51 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r13, (%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %r13
	jne	.LBB0_51
# BB#55:                                # %._crit_edge646.loopexit
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	216(%rsp), %rcx
	movq	80(%rsp), %r13          # 8-byte Reload
	jmp	.LBB0_60
.LBB0_56:
	movq	(%r13), %rsi
	xorl	%ebp, %ebp
	movl	$.L.str.1, %edi
	movl	$.L.str.2, %edx
	xorl	%eax, %eax
	callq	printf
	movq	(%r13), %r14
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rcx
	shlq	$32, %rcx
	addq	%r15, %rcx
	sarq	$32, %rcx
	cmpl	$7, %eax
	cmovgq	%rcx, %rbp
	addq	%r14, %rbp
	movl	$.L.str.46, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_58
# BB#57:
	movq	96(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 12(%rax)
	je	.LBB0_488
.LBB0_58:
	movl	$.L.str.3, %edi
	jmp	.LBB0_489
.LBB0_59:
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
.LBB0_60:                               # %._crit_edge646
	movq	168(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_65
# BB#61:
	testq	%rcx, %rcx
	je	.LBB0_66
# BB#62:                                # %.preheader.i420.preheader
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB0_63:                               # %.preheader.i420
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB0_63
# BB#64:
	movq	%rcx, (%rdx)
	jmp	.LBB0_66
.LBB0_65:
	movq	%rcx, %rax
.LBB0_66:                               # %list_Nconc.exit422
	movq	%rax, 168(%rsp)
	movq	8(%rsp), %r12           # 8-byte Reload
	cmpl	$0, 204(%r12)
	movq	128(%rsp), %rbp
	je	.LBB0_68
# BB#67:
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rbp, %rdx
	callq	def_ExtractDefsFromTermlist
	movq	(%rbx), %rdi
	movq	128(%rsp), %rsi
	movq	%r12, %rdx
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rcx
	callq	def_ApplyDefinitionToTermList
	movq	%rax, %rbp
	movq	%rbp, 128(%rsp)
	movq	168(%rsp), %rax
	jmp	.LBB0_69
.LBB0_68:
	movq	48(%rsp), %rbx          # 8-byte Reload
.LBB0_69:                               # %list_Nconc.exit422._crit_edge
	movq	$0, 240(%rsp)
	subq	$8, %rsp
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	leaq	248(%rsp), %r10
	leaq	40(%rsp), %rdx
	leaq	256(%rsp), %rcx
	movq	%rax, %rdi
	movq	%rbp, %rsi
	movq	168(%rsp), %r8          # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	pushq	%r10
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	callq	cnf_Flotter
	addq	$32, %rsp
.Lcfi20:
	.cfi_adjust_cfa_offset -32
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	32(%rsp), %rdi
	callq	clause_ListSortWeighed
	movq	%rax, 32(%rsp)
	movl	$1, clause_CLAUSECOUNTER(%rip)
	testq	%rax, %rax
	je	.LBB0_73
# BB#70:                                # %.lr.ph641.preheader
	movq	8(%rax), %rcx
	movl	$2, clause_CLAUSECOUNTER(%rip)
	movl	$1, (%rcx)
	jmp	.LBB0_72
	.p2align	4, 0x90
.LBB0_71:                               # %.lr.ph641..lr.ph641_crit_edge
                                        #   in Loop: Header=BB0_72 Depth=1
	movl	clause_CLAUSECOUNTER(%rip), %ecx
	movq	8(%rax), %rdx
	leal	1(%rcx), %esi
	movl	%esi, clause_CLAUSECOUNTER(%rip)
	movl	%ecx, (%rdx)
.LBB0_72:                               # %.lr.ph641.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB0_71
.LBB0_73:                               # %.loopexit
	movl	$3, %edi
	callq	clock_StopPassedTime
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rcx
	shlq	$32, %rcx
	addq	%r15, %rcx
	sarq	$32, %rcx
	xorl	%edi, %edi
	cmpl	$7, %eax
	cmovgq	%rcx, %rdi
	addq	%rbx, %rdi
	movl	$.L.str.46, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_75
# BB#74:
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 12(%rax)
	je	.LBB0_88
.LBB0_75:
	movq	32(%rsp), %rbx
	callq	opts_Indicator
	addl	$2, %eax
	cmpl	%r14d, %eax
	movq	%rbx, 192(%rsp)         # 8-byte Spill
	jle	.LBB0_77
# BB#76:
	movq	stdout(%rip), %r12
	jmp	.LBB0_78
.LBB0_77:
	callq	opts_Indicator
	cltq
	movq	8(%r13,%rax,8), %rdi
	movl	$.L.str.34, %esi
	callq	misc_OpenFile
	movq	%rax, %r12
.LBB0_78:
	callq	dfg_ProblemDescription
	movq	%rax, %rdi
	callq	strlen
	movq	%rax, %rbp
	movslq	%ebp, %rbx
	leal	35(%rbp), %r14d
	movl	%r14d, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	callq	dfg_ProblemDescription
	shlq	$32, %rbp
	movabsq	$-12884901888, %rdx     # imm = 0xFFFFFFFD00000000
	addq	%rbp, %rdx
	sarq	$32, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	strncpy
	movups	.L.str.49+16(%rip), %xmm0
	movups	%xmm0, 13(%r13,%rbx)
	movdqu	.L.str.49(%rip), %xmm0
	movdqu	%xmm0, -3(%r13,%rbx)
	movl	$8202784, 29(%r13,%rbx) # imm = 0x7D2A20
	callq	dfg_ProblemName
	movq	%rax, %rbp
	callq	dfg_ProblemAuthor
	movq	%rax, %r15
	callq	dfg_ProblemStatusString
	movq	%r12, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%rax, %rcx
	movq	%r13, %r8
	movq	192(%rsp), %r9          # 8-byte Reload
	callq	clause_FPrintCnfDFGProblem
	movq	stdout(%rip), %rcx
	movl	$.L.str.50, %edi
	movl	$16, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$2, %edi
	callq	clock_PrintTime
	movl	$.L.str.51, %edi
	callq	puts
	movq	stdout(%rip), %rcx
	movl	$.L.str.52, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$3, %edi
	callq	clock_PrintTime
	movq	stdout(%rip), %rcx
	movl	$.L.str.53, %edi
	movl	$26, %esi
	movl	$1, %edx
	callq	fwrite
	cmpq	stdout(%rip), %r12
	je	.LBB0_80
# BB#79:
	callq	opts_Indicator
	cltq
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	8(%rcx,%rax,8), %rsi
	movq	%r12, %rdi
	callq	misc_CloseFile
.LBB0_80:
	cmpl	$1024, %r14d            # imm = 0x400
	jae	.LBB0_82
# BB#81:
	movl	%r14d, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%r13, (%rax)
	movq	8(%rsp), %rbx           # 8-byte Reload
	jmp	.LBB0_87
.LBB0_82:
	movl	memory_ALIGN(%rip), %ecx
	xorl	%edx, %edx
	movl	%r14d, %eax
	divl	%ecx
	subl	%edx, %ecx
	testl	%edx, %edx
	cmovel	%edx, %ecx
	addl	%r14d, %ecx
	movl	memory_OFFSET(%rip), %eax
	movq	%r13, %rdx
	subq	%rax, %rdx
	movq	-16(%rdx), %rsi
	movq	-8(%rdx), %rdi
	testq	%rsi, %rsi
	leaq	8(%rsi), %rsi
	movl	$memory_BIGBLOCKS, %ebp
	cmovneq	%rsi, %rbp
	movq	%rdi, (%rbp)
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB0_84
# BB#83:
	negq	%rax
	movq	-16(%r13,%rax), %rax
	movq	%rax, (%rdx)
.LBB0_84:
	addl	memory_MARKSIZE(%rip), %ecx
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rcx), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	movq	8(%rsp), %rbx           # 8-byte Reload
	js	.LBB0_86
# BB#85:
	leaq	16(%rcx,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB0_86:
	addq	$-16, %r13
	movq	%r13, %rdi
	callq	free
.LBB0_87:                               # %top_Flotter.exit
	movl	$7, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	movl	$2, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	movl	$27, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
.LBB0_88:
	movq	8(%rsp), %rbx           # 8-byte Reload
	movslq	24(%rbx), %rdi
	callq	memory_Restrict
	movl	8(%rbx), %eax
	movq	120(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, 48(%rsp)          # 8-byte Folded Reload
	sbbb	%cl, %cl
	movq	72(%rsp), %rdx          # 8-byte Reload
	cmpq	%rdx, 152(%rsp)         # 8-byte Folded Reload
	sbbb	%dl, %dl
	andb	%cl, %dl
	andb	$1, %dl
	movb	%dl, 30(%rsp)           # 1-byte Spill
	movq	(%rsp), %r14            # 8-byte Reload
	.p2align	4, 0x90
.LBB0_89:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_93 Depth 2
                                        #       Child Loop BB0_94 Depth 3
                                        #       Child Loop BB0_96 Depth 3
                                        #       Child Loop BB0_103 Depth 3
                                        #     Child Loop BB0_110 Depth 2
                                        #       Child Loop BB0_111 Depth 3
                                        #       Child Loop BB0_113 Depth 3
                                        #       Child Loop BB0_120 Depth 3
                                        #     Child Loop BB0_125 Depth 2
                                        #       Child Loop BB0_126 Depth 3
                                        #       Child Loop BB0_133 Depth 3
                                        #       Child Loop BB0_137 Depth 3
                                        #     Child Loop BB0_144 Depth 2
                                        #     Child Loop BB0_147 Depth 2
                                        #     Child Loop BB0_149 Depth 2
                                        #     Child Loop BB0_153 Depth 2
                                        #     Child Loop BB0_158 Depth 2
                                        #     Child Loop BB0_161 Depth 2
                                        #     Child Loop BB0_169 Depth 2
                                        #     Child Loop BB0_177 Depth 2
                                        #     Child Loop BB0_183 Depth 2
                                        #     Child Loop BB0_185 Depth 2
                                        #     Child Loop BB0_191 Depth 2
                                        #     Child Loop BB0_194 Depth 2
                                        #     Child Loop BB0_202 Depth 2
                                        #       Child Loop BB0_203 Depth 3
                                        #         Child Loop BB0_212 Depth 4
                                        #           Child Loop BB0_222 Depth 5
                                        #           Child Loop BB0_243 Depth 5
                                        #           Child Loop BB0_238 Depth 5
                                        #           Child Loop BB0_236 Depth 5
                                        #           Child Loop BB0_304 Depth 5
                                        #             Child Loop BB0_305 Depth 6
                                        #           Child Loop BB0_314 Depth 5
                                        #             Child Loop BB0_315 Depth 6
                                        #           Child Loop BB0_345 Depth 5
                                        #           Child Loop BB0_350 Depth 5
                                        #           Child Loop BB0_247 Depth 5
                                        #             Child Loop BB0_249 Depth 6
                                        #             Child Loop BB0_256 Depth 6
                                        #               Child Loop BB0_257 Depth 7
                                        #             Child Loop BB0_265 Depth 6
                                        #               Child Loop BB0_266 Depth 7
                                        #           Child Loop BB0_290 Depth 5
                                        #           Child Loop BB0_295 Depth 5
                                        #           Child Loop BB0_335 Depth 5
                                        #           Child Loop BB0_366 Depth 5
                                        #           Child Loop BB0_370 Depth 5
                                        #     Child Loop BB0_418 Depth 2
                                        #       Child Loop BB0_420 Depth 3
                                        #       Child Loop BB0_427 Depth 3
                                        #     Child Loop BB0_432 Depth 2
                                        #     Child Loop BB0_434 Depth 2
                                        #     Child Loop BB0_438 Depth 2
                                        #     Child Loop BB0_447 Depth 2
                                        #       Child Loop BB0_449 Depth 3
                                        #       Child Loop BB0_451 Depth 3
                                        #     Child Loop BB0_442 Depth 2
                                        #     Child Loop BB0_454 Depth 2
                                        #     Child Loop BB0_445 Depth 2
	testl	%eax, %eax
	je	.LBB0_107
# BB#90:                                #   in Loop: Header=BB0_89 Depth=1
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	ia_GetNextRequest
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_492
# BB#91:                                #   in Loop: Header=BB0_89 Depth=1
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.LBB0_108
# BB#92:                                # %.lr.ph616.preheader
                                        #   in Loop: Header=BB0_89 Depth=1
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_93:                               # %.lr.ph616
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_94 Depth 3
                                        #       Child Loop BB0_96 Depth 3
                                        #       Child Loop BB0_103 Depth 3
	movq	8(%r12), %r13
	movq	%r13, %rdi
	callq	strlen
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_94:                               #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_93 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsbq	(%r13,%rdx), %rsi
	addq	%rsi, %rcx
	incq	%rdx
	cmpq	%rax, %rdx
	jbe	.LBB0_94
# BB#95:                                # %hsh_StringHashKey.exit.i
                                        #   in Loop: Header=BB0_93 Depth=2
	movq	%rcx, %rax
	movabsq	$1908283869694091547, %rdx # imm = 0x1A7B9611A7B9611B
	mulq	%rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rdx, %rax
	shrq	$4, %rax
	imulq	$29, %rax, %rax
	subq	%rax, %rcx
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rcx,8), %rbp
	testq	%rbp, %rbp
	je	.LBB0_98
	.p2align	4, 0x90
.LBB0_96:                               # %.lr.ph.i429
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_93 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbp), %rbx
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_99
# BB#97:                                #   in Loop: Header=BB0_96 Depth=3
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB0_96
.LBB0_98:                               #   in Loop: Header=BB0_93 Depth=2
	xorl	%edi, %edi
	jmp	.LBB0_100
	.p2align	4, 0x90
.LBB0_99:                               #   in Loop: Header=BB0_93 Depth=2
	movq	(%rbx), %rdi
.LBB0_100:                              # %hsh_GetWithCompareFunc.exit
                                        #   in Loop: Header=BB0_93 Depth=2
	callq	list_Copy
	movq	%rax, %rdi
	callq	list_PointerDeleteDuplicates
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_105
# BB#101:                               #   in Loop: Header=BB0_93 Depth=2
	testq	%r15, %r15
	je	.LBB0_106
# BB#102:                               # %.preheader.i437.preheader
                                        #   in Loop: Header=BB0_93 Depth=2
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB0_103:                              # %.preheader.i437
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_93 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_103
# BB#104:                               #   in Loop: Header=BB0_93 Depth=2
	movq	%r15, (%rax)
	jmp	.LBB0_106
	.p2align	4, 0x90
.LBB0_105:                              #   in Loop: Header=BB0_93 Depth=2
	movq	%r15, %rbx
.LBB0_106:                              # %list_Nconc.exit439
                                        #   in Loop: Header=BB0_93 Depth=2
	movq	(%r12), %r12
	testq	%r12, %r12
	movq	%rbx, %r15
	jne	.LBB0_93
	jmp	.LBB0_109
	.p2align	4, 0x90
.LBB0_107:                              #   in Loop: Header=BB0_89 Depth=1
	movq	32(%rsp), %rbp
	movq	$0, 32(%rsp)
	xorl	%eax, %eax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	jmp	.LBB0_151
	.p2align	4, 0x90
.LBB0_108:                              #   in Loop: Header=BB0_89 Depth=1
	xorl	%ebx, %ebx
.LBB0_109:                              # %.preheader534
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	%r14, %r13
	movq	136(%rsp), %rax         # 8-byte Reload
	testq	%rax, %rax
	movq	%rax, %r15
	je	.LBB0_124
	.p2align	4, 0x90
.LBB0_110:                              # %.lr.ph619
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_111 Depth 3
                                        #       Child Loop BB0_113 Depth 3
                                        #       Child Loop BB0_120 Depth 3
	movq	%rbx, %r14
	movq	8(%r15), %r12
	movq	%r12, %rdi
	callq	strlen
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_111:                              #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_110 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsbq	(%r12,%rdx), %rsi
	addq	%rsi, %rcx
	incq	%rdx
	cmpq	%rax, %rdx
	jbe	.LBB0_111
# BB#112:                               # %hsh_StringHashKey.exit.i443
                                        #   in Loop: Header=BB0_110 Depth=2
	movq	%rcx, %rax
	movabsq	$1908283869694091547, %rdx # imm = 0x1A7B9611A7B9611B
	mulq	%rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rdx, %rax
	shrq	$4, %rax
	imulq	$29, %rax, %rax
	subq	%rax, %rcx
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rcx,8), %rbp
	testq	%rbp, %rbp
	je	.LBB0_115
	.p2align	4, 0x90
.LBB0_113:                              # %.lr.ph.i451
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_110 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbp), %rbx
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_116
# BB#114:                               #   in Loop: Header=BB0_113 Depth=3
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB0_113
.LBB0_115:                              #   in Loop: Header=BB0_110 Depth=2
	xorl	%edi, %edi
	jmp	.LBB0_117
	.p2align	4, 0x90
.LBB0_116:                              #   in Loop: Header=BB0_110 Depth=2
	movq	(%rbx), %rdi
.LBB0_117:                              # %hsh_GetWithCompareFunc.exit456
                                        #   in Loop: Header=BB0_110 Depth=2
	callq	list_Copy
	movq	%rax, %rdi
	callq	list_PointerDeleteDuplicates
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_122
# BB#118:                               #   in Loop: Header=BB0_110 Depth=2
	testq	%r14, %r14
	je	.LBB0_123
# BB#119:                               # %.preheader.i460.preheader
                                        #   in Loop: Header=BB0_110 Depth=2
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB0_120:                              # %.preheader.i460
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_110 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_120
# BB#121:                               #   in Loop: Header=BB0_110 Depth=2
	movq	%r14, (%rcx)
	jmp	.LBB0_123
	.p2align	4, 0x90
.LBB0_122:                              #   in Loop: Header=BB0_110 Depth=2
	movq	%r14, %rbx
.LBB0_123:                              # %list_Nconc.exit462
                                        #   in Loop: Header=BB0_110 Depth=2
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB0_110
.LBB0_124:                              # %.preheader533
                                        #   in Loop: Header=BB0_89 Depth=1
	testq	%rbx, %rbx
	movq	%rbx, %r12
	movq	%rbx, 176(%rsp)         # 8-byte Spill
	movq	%r13, %r14
	je	.LBB0_141
	.p2align	4, 0x90
.LBB0_125:                              # %.lr.ph622
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_126 Depth 3
                                        #       Child Loop BB0_133 Depth 3
                                        #       Child Loop BB0_137 Depth 3
	movq	8(%r12), %rbp
	movq	%rbp, %rax
	movabsq	$1908283869694091547, %rcx # imm = 0x1A7B9611A7B9611B
	mulq	%rcx
	movq	%rbp, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rdx, %rax
	shrq	$4, %rax
	imulq	$29, %rax, %rax
	movq	%rbp, %rcx
	subq	%rax, %rcx
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	testq	%rax, %rax
	je	.LBB0_128
	.p2align	4, 0x90
.LBB0_126:                              # %.lr.ph.i471
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_125 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rax), %rcx
	cmpq	%rbp, 8(%rcx)
	je	.LBB0_129
# BB#127:                               #   in Loop: Header=BB0_126 Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB0_126
.LBB0_128:                              #   in Loop: Header=BB0_125 Depth=2
	xorl	%edi, %edi
	jmp	.LBB0_130
	.p2align	4, 0x90
.LBB0_129:                              #   in Loop: Header=BB0_125 Depth=2
	movq	(%rcx), %rdi
.LBB0_130:                              # %hsh_Get.exit476
                                        #   in Loop: Header=BB0_125 Depth=2
	callq	list_Copy
	movl	$cnf_LabelEqual, %esi
	movq	%rax, %rdi
	callq	list_DeleteDuplicates
	movq	%rax, %r15
	movq	%rbp, %rdi
	callq	clause_Copy
	movq	%rax, %rbp
	movq	%rbp, 8(%r12)
	movabsq	$1908283869694091547, %rcx # imm = 0x1A7B9611A7B9611B
	mulq	%rcx
	movq	%rbp, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rdx, %rax
	shrq	$4, %rax
	imulq	$29, %rax, %rax
	movq	%rbp, %rbx
	subq	%rax, %rbx
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rcx
	testq	%rcx, %rcx
	je	.LBB0_131
	.p2align	4, 0x90
.LBB0_133:                              # %.lr.ph.i478
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_125 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rcx), %rax
	cmpq	%rbp, 8(%rax)
	je	.LBB0_134
# BB#132:                               #   in Loop: Header=BB0_133 Depth=3
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_133
.LBB0_131:                              # %._crit_edge.i487
                                        #   in Loop: Header=BB0_125 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	movq	%rbp, 8(%r13)
	movq	%r15, (%r13)
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rbp
	movq	(%rbp,%rbx,8), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, (%rbp,%rbx,8)
	movq	176(%rsp), %rbx         # 8-byte Reload
	jmp	.LBB0_140
	.p2align	4, 0x90
.LBB0_134:                              #   in Loop: Header=BB0_125 Depth=2
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB0_491
# BB#135:                               #   in Loop: Header=BB0_125 Depth=2
	testq	%r15, %r15
	movq	176(%rsp), %rbx         # 8-byte Reload
	je	.LBB0_139
# BB#136:                               # %.preheader.i.i484.preheader
                                        #   in Loop: Header=BB0_125 Depth=2
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB0_137:                              # %.preheader.i.i484
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_125 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB0_137
# BB#138:                               #   in Loop: Header=BB0_125 Depth=2
	movq	%r15, (%rdx)
	jmp	.LBB0_139
.LBB0_491:                              #   in Loop: Header=BB0_125 Depth=2
	movq	%r15, %rcx
	movq	176(%rsp), %rbx         # 8-byte Reload
.LBB0_139:                              # %list_Nconc.exit.i486
                                        #   in Loop: Header=BB0_125 Depth=2
	movq	%rcx, (%rax)
.LBB0_140:                              # %hsh_PutList.exit
                                        #   in Loop: Header=BB0_125 Depth=2
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB0_125
.LBB0_141:                              # %._crit_edge623
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	8(%r14), %rsi
	movq	144(%rsp), %rdi         # 8-byte Reload
	leaq	240(%rsp), %rdx
	callq	cnf_QueryFlotter
	testq	%rax, %rax
	movq	%rax, %rbp
	cmoveq	%rbx, %rbp
	je	.LBB0_146
# BB#142:                               # %._crit_edge623
                                        #   in Loop: Header=BB0_89 Depth=1
	testq	%rbx, %rbx
	je	.LBB0_146
# BB#143:                               # %.preheader.i491.preheader
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB0_144:                              # %.preheader.i491
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_144
# BB#145:                               #   in Loop: Header=BB0_89 Depth=1
	movq	%rbx, (%rcx)
	movq	%rax, %rbp
.LBB0_146:                              # %list_Nconc.exit493
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB0_150
	.p2align	4, 0x90
.LBB0_147:                              # %.lr.ph626
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	callq	string_StringFree
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_147
# BB#148:                               # %._crit_edge627
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.LBB0_150
	.p2align	4, 0x90
.LBB0_149:                              # %.lr.ph.i497
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB0_149
.LBB0_150:                              # %list_Delete.exit499
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r14, (%rax)
	movl	$1, %edi
	callq	clock_InitCounter
	movl	$1, %edi
	callq	clock_StartCounter
	movq	(%rsp), %r14            # 8-byte Reload
.LBB0_151:                              #   in Loop: Header=BB0_89 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	20(%rcx), %eax
	movl	%eax, 136(%r14)
	movl	44(%rcx), %eax
	movl	%eax, 148(%r14)
	movl	$0, 152(%r14)
	movl	$-1, 40(%rsp)
	movq	%r14, %rbx
	movq	232(%rsp), %r14
	movq	$0, 272(%rsp)
	movq	104(%rbx), %r15
	movq	112(%rbx), %r12
	movl	$4, %edi
	callq	clock_InitCounter
	xorl	%edi, %edi
	callq	clock_InitCounter
	movl	$5, %edi
	callq	clock_InitCounter
	movq	%rbx, %rdi
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movq	%rbp, %rsi
	callq	ana_AnalyzeProblem
	cmpl	$0, (%r12)
	movl	flag_CLEAN(%rip), %ebx
	je	.LBB0_156
# BB#152:                               #   in Loop: Header=BB0_89 Depth=1
	movq	ana_FINITEMONADICPREDICATES(%rip), %rdx
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	56(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rsi
	callq	prfs_InstallFiniteMonadicPredicates
	movq	%rbp, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	ana_AutoConfiguration
	movq	96(%rsp), %rax          # 8-byte Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_153:                              #   Parent Loop BB0_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax,%rbp,4), %edx
	cmpl	%ebx, %edx
	je	.LBB0_155
# BB#154:                               #   in Loop: Header=BB0_153 Depth=2
	movq	%r12, %rdi
	movl	%ebp, %esi
	callq	flag_SetFlagValue
	movq	96(%rsp), %rax          # 8-byte Reload
.LBB0_155:                              #   in Loop: Header=BB0_153 Depth=2
	incq	%rbp
	cmpq	$96, %rbp
	jne	.LBB0_153
.LBB0_156:                              # %flag_TransferSetFlags.exit.i
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	symbol_RearrangePrecedence
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB0_159
# BB#157:                               # %.lr.ph438.i.preheader
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	56(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_158:                              # %.lr.ph438.i
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rbp
	movq	%rbp, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	clause_OrientEqualities
	movq	%rbp, %rdi
	callq	clause_Normalize
	movq	%rbp, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	clause_SetMaxLitFlags
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	clause_ComputeWeight
	movl	%eax, 4(%rbp)
	movq	%rbp, %rdi
	callq	clause_UpdateMaxVar
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_158
.LBB0_159:                              # %._crit_edge439.i
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	ana_AnalyzeSortStructure
	cmpl	$0, (%r12)
	movq	(%rsp), %r14            # 8-byte Reload
	movl	flag_CLEAN(%rip), %ebx
	je	.LBB0_164
# BB#160:                               #   in Loop: Header=BB0_89 Depth=1
	movq	%r12, %rdi
	callq	ana_ExploitSortAnalysis
	movq	96(%rsp), %rax          # 8-byte Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_161:                              #   Parent Loop BB0_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax,%rbp,4), %edx
	cmpl	%ebx, %edx
	je	.LBB0_163
# BB#162:                               #   in Loop: Header=BB0_161 Depth=2
	movq	%r12, %rdi
	movl	%ebp, %esi
	callq	flag_SetFlagValue
	movq	96(%rsp), %rax          # 8-byte Reload
.LBB0_163:                              #   in Loop: Header=BB0_161 Depth=2
	incq	%rbp
	cmpq	$96, %rbp
	jne	.LBB0_161
.LBB0_164:                              # %flag_TransferSetFlags.exit235.i
                                        #   in Loop: Header=BB0_89 Depth=1
	movl	20(%r12), %eax
	movl	%eax, 136(%r14)
	movl	192(%r12), %r13d
	movl	196(%r12), %eax
	movl	%eax, 108(%rsp)         # 4-byte Spill
	movl	200(%r12), %eax
	movl	%eax, 260(%rsp)         # 4-byte Spill
	movl	$-1, 40(%rsp)
	cmpl	$0, 108(%r12)
	je	.LBB0_166
# BB#165:                               #   in Loop: Header=BB0_89 Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$.L.str.55, %edi
	callq	puts
	movl	$.L.str.56, %edi
	callq	puts
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	clause_ListPrint
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	ana_Print
.LBB0_166:                              #   in Loop: Header=BB0_89 Depth=1
	movl	160(%r12), %eax
	testl	%eax, %eax
	je	.LBB0_170
# BB#167:                               #   in Loop: Header=BB0_89 Depth=1
	xorl	%ebp, %ebp
	cmpl	$2, %eax
	sete	%al
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB0_170
# BB#168:                               # %.lr.ph436.i.preheader
                                        #   in Loop: Header=BB0_89 Depth=1
	movb	%al, %bpl
	movq	56(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_169:                              # %.lr.ph436.i
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movl	%ebp, %esi
	movq	%r12, %rdx
	movq	%r15, %rcx
	callq	clause_SetSortConstraint
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_169
.LBB0_170:                              # %.loopexit365.i
                                        #   in Loop: Header=BB0_89 Depth=1
	cmpl	$0, 156(%r12)
	je	.LBB0_176
# BB#171:                               #   in Loop: Header=BB0_89 Depth=1
	movl	$4, %edi
	callq	clock_StartCounter
	movq	%r14, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	callq	red_ReduceInput
	movq	%rax, %rbp
	movq	%rbp, 272(%rsp)
	movl	$4, %edi
	callq	clock_StopAddPassedTime
	testq	%rbp, %rbp
	jne	.LBB0_178
# BB#172:                               #   in Loop: Header=BB0_89 Depth=1
	cmpl	$0, 164(%r12)
	je	.LBB0_178
# BB#173:                               #   in Loop: Header=BB0_89 Depth=1
	movq	%r14, %rdi
	callq	red_SatInput
	movq	%rax, 272(%rsp)
	cmpl	$0, ana_SORTRES(%rip)
	jne	.LBB0_179
	jmp	.LBB0_174
	.p2align	4, 0x90
.LBB0_176:                              # %.preheader363.i
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.LBB0_178
	.p2align	4, 0x90
.LBB0_177:                              # %.lr.ph433.i
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	prfs_InsertUsableClause
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_177
.LBB0_178:                              # %.loopexit364.i
                                        #   in Loop: Header=BB0_89 Depth=1
	cmpl	$0, ana_SORTRES(%rip)
	je	.LBB0_174
.LBB0_179:                              #   in Loop: Header=BB0_89 Depth=1
	cmpl	$0, 356(%r12)
	je	.LBB0_181
# BB#180:                               #   in Loop: Header=BB0_89 Depth=1
	movq	56(%r14), %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	sort_ApproxStaticSortTheory
	movq	%rax, 64(%r14)
.LBB0_181:                              #   in Loop: Header=BB0_89 Depth=1
	callq	sort_TheoryCreate
	movq	%rax, 80(%r14)
.LBB0_182:                              #   in Loop: Header=BB0_89 Depth=1
	movq	56(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB0_187
	.p2align	4, 0x90
.LBB0_183:                              # %.lr.ph431.i
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rbp
	movq	48(%r14), %rsi
	movq	%rbp, %rdi
	callq	clause_MakeUnshared
	movq	%rbp, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	clause_FixLiteralOrder
	movq	112(%r14), %rdx
	movq	48(%r14), %rsi
	movq	104(%r14), %rcx
	movq	%rbp, %rdi
	callq	clause_InsertIntoSharing
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_183
# BB#184:                               # %.preheader362.i
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	56(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB0_187
	.p2align	4, 0x90
.LBB0_185:                              # %.lr.ph426.i
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	callq	clause_CountSymbols
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_185
# BB#186:                               # %._crit_edge427.loopexit.i
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	56(%r14), %rdi
	jmp	.LBB0_188
	.p2align	4, 0x90
.LBB0_187:                              #   in Loop: Header=BB0_89 Depth=1
	xorl	%edi, %edi
.LBB0_188:                              # %._crit_edge427.i
                                        #   in Loop: Header=BB0_89 Depth=1
	movl	$clause_CompareAbstractLEQ, %esi
	callq	list_Sort
	movq	%rax, 56(%r14)
	cmpl	$0, 16(%r12)
	je	.LBB0_195
# BB#189:                               #   in Loop: Header=BB0_89 Depth=1
	movq	%rax, %rdi
	callq	list_Copy
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_195
# BB#190:                               # %.lr.ph422.i.preheader
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB0_191:                              # %.lr.ph422.i
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rsi
	testb	$8, 48(%rsi)
	jne	.LBB0_193
# BB#192:                               #   in Loop: Header=BB0_191 Depth=2
	movq	%r14, %rdi
	callq	prfs_MoveUsableWorkedOff
.LBB0_193:                              #   in Loop: Header=BB0_191 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_191
	.p2align	4, 0x90
.LBB0_194:                              # %.lr.ph.i.i414
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB0_194
.LBB0_195:                              # %list_Delete.exit.i415
                                        #   in Loop: Header=BB0_89 Depth=1
	cmpl	$0, 108(%r12)
	je	.LBB0_197
# BB#196:                               #   in Loop: Header=BB0_89 Depth=1
	movl	$.L.str.57, %edi
	callq	puts
	movl	$.L.str.58, %edi
	callq	puts
	movq	40(%r14), %rdi
	callq	clause_ListPrint
	movl	$.L.str.59, %edi
	callq	puts
	movq	56(%r14), %rdi
	callq	clause_ListPrint
.LBB0_197:                              # %.preheader361.i
                                        #   in Loop: Header=BB0_89 Depth=1
	cmpl	$2, %r13d
	movl	$.L.str.65, %ecx
	movl	$.L.str.64, %eax
	cmoveq	%rax, %rcx
	movq	%rcx, 384(%rsp)         # 8-byte Spill
	movl	$1, 72(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	leaq	272(%rsp), %rbp
	movl	%r13d, %ebx
	movq	%rbp, %r13
	xorl	%ebp, %ebp
	movq	%r12, 192(%rsp)         # 8-byte Spill
	movl	%ebx, 204(%rsp)         # 4-byte Spill
	jmp	.LBB0_202
	.p2align	4, 0x90
.LBB0_174:                              #   in Loop: Header=BB0_89 Depth=1
	cmpl	$0, ana_USORTRES(%rip)
	je	.LBB0_182
# BB#175:                               #   in Loop: Header=BB0_89 Depth=1
	cmpl	$2, 160(%r12)
	je	.LBB0_179
	jmp	.LBB0_182
	.p2align	4, 0x90
.LBB0_199:                              #   in Loop: Header=BB0_202 Depth=2
	decl	260(%rsp)               # 4-byte Folded Spill
	movl	40(%rsp), %eax
	movl	%eax, %edx
	movl	$-1, %ecx
	cmovel	%ecx, %edx
	movl	%edx, 108(%rsp)         # 4-byte Spill
	cmpl	$-1, %eax
	movl	204(%rsp), %ebx         # 4-byte Reload
	je	.LBB0_202
# BB#200:                               #   in Loop: Header=BB0_202 Depth=2
	movq	%r14, %rdi
	callq	prfs_SwapIndexes
	cmpl	$0, 144(%r12)
	jne	.LBB0_201
	.p2align	4, 0x90
.LBB0_202:                              # %.outer.i
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_203 Depth 3
                                        #         Child Loop BB0_212 Depth 4
                                        #           Child Loop BB0_222 Depth 5
                                        #           Child Loop BB0_243 Depth 5
                                        #           Child Loop BB0_238 Depth 5
                                        #           Child Loop BB0_236 Depth 5
                                        #           Child Loop BB0_304 Depth 5
                                        #             Child Loop BB0_305 Depth 6
                                        #           Child Loop BB0_314 Depth 5
                                        #             Child Loop BB0_315 Depth 6
                                        #           Child Loop BB0_345 Depth 5
                                        #           Child Loop BB0_350 Depth 5
                                        #           Child Loop BB0_247 Depth 5
                                        #             Child Loop BB0_249 Depth 6
                                        #             Child Loop BB0_256 Depth 6
                                        #               Child Loop BB0_257 Depth 7
                                        #             Child Loop BB0_265 Depth 6
                                        #               Child Loop BB0_266 Depth 7
                                        #           Child Loop BB0_290 Depth 5
                                        #           Child Loop BB0_295 Depth 5
                                        #           Child Loop BB0_335 Depth 5
                                        #           Child Loop BB0_366 Depth 5
                                        #           Child Loop BB0_370 Depth 5
	testl	%ebx, %ebx
	setne	%al
	cmpl	$-1, 108(%rsp)          # 4-byte Folded Reload
	setne	%cl
	andb	%al, %cl
	movb	%cl, 31(%rsp)           # 1-byte Spill
	jmp	.LBB0_203
.LBB0_201:                              #   in Loop: Header=BB0_202 Depth=2
	movl	$.L.str.63, %edi
	xorl	%eax, %eax
	movq	384(%rsp), %rsi         # 8-byte Reload
	movl	108(%rsp), %edx         # 4-byte Reload
	callq	printf
	jmp	.LBB0_202
	.p2align	4, 0x90
.LBB0_203:                              #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_212 Depth 4
                                        #           Child Loop BB0_222 Depth 5
                                        #           Child Loop BB0_243 Depth 5
                                        #           Child Loop BB0_238 Depth 5
                                        #           Child Loop BB0_236 Depth 5
                                        #           Child Loop BB0_304 Depth 5
                                        #             Child Loop BB0_305 Depth 6
                                        #           Child Loop BB0_314 Depth 5
                                        #             Child Loop BB0_315 Depth 6
                                        #           Child Loop BB0_345 Depth 5
                                        #           Child Loop BB0_350 Depth 5
                                        #           Child Loop BB0_247 Depth 5
                                        #             Child Loop BB0_249 Depth 6
                                        #             Child Loop BB0_256 Depth 6
                                        #               Child Loop BB0_257 Depth 7
                                        #             Child Loop BB0_265 Depth 6
                                        #               Child Loop BB0_266 Depth 7
                                        #           Child Loop BB0_290 Depth 5
                                        #           Child Loop BB0_295 Depth 5
                                        #           Child Loop BB0_335 Depth 5
                                        #           Child Loop BB0_366 Depth 5
                                        #           Child Loop BB0_370 Depth 5
	movq	272(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_205
# BB#204:                               #   in Loop: Header=BB0_203 Depth=3
	cmpq	$0, 120(%r14)
	je	.LBB0_382
.LBB0_205:                              #   in Loop: Header=BB0_203 Depth=3
	cmpl	$0, 148(%r14)
	je	.LBB0_382
# BB#206:                               #   in Loop: Header=BB0_203 Depth=3
	cmpl	$-1, 40(%rsp)
	jne	.LBB0_208
# BB#207:                               #   in Loop: Header=BB0_203 Depth=3
	cmpq	$0, 56(%r14)
	je	.LBB0_382
.LBB0_208:                              #   in Loop: Header=BB0_203 Depth=3
	movl	28(%r12), %eax
	cmpl	$-1, %eax
	je	.LBB0_210
# BB#209:                               #   in Loop: Header=BB0_203 Depth=3
	cvtsi2ssl	%eax, %xmm0
	movd	%xmm0, 80(%rsp)         # 4-byte Folded Spill
	movl	$1, %edi
	callq	clock_GetSeconds
	movss	80(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB0_381
.LBB0_210:                              # %.critedge3.i
                                        #   in Loop: Header=BB0_203 Depth=3
	movq	$0, 16(%rsp)
	movl	$-1, 40(%rsp)
	jmp	.LBB0_212
	.p2align	4, 0x90
.LBB0_211:                              #   in Loop: Header=BB0_212 Depth=4
	decl	148(%r14)
.LBB0_212:                              #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_222 Depth 5
                                        #           Child Loop BB0_243 Depth 5
                                        #           Child Loop BB0_238 Depth 5
                                        #           Child Loop BB0_236 Depth 5
                                        #           Child Loop BB0_304 Depth 5
                                        #             Child Loop BB0_305 Depth 6
                                        #           Child Loop BB0_314 Depth 5
                                        #             Child Loop BB0_315 Depth 6
                                        #           Child Loop BB0_345 Depth 5
                                        #           Child Loop BB0_350 Depth 5
                                        #           Child Loop BB0_247 Depth 5
                                        #             Child Loop BB0_249 Depth 6
                                        #             Child Loop BB0_256 Depth 6
                                        #               Child Loop BB0_257 Depth 7
                                        #             Child Loop BB0_265 Depth 6
                                        #               Child Loop BB0_266 Depth 7
                                        #           Child Loop BB0_290 Depth 5
                                        #           Child Loop BB0_295 Depth 5
                                        #           Child Loop BB0_335 Depth 5
                                        #           Child Loop BB0_366 Depth 5
                                        #           Child Loop BB0_370 Depth 5
	movq	272(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_214
# BB#213:                               #   in Loop: Header=BB0_212 Depth=4
	cmpq	$0, 120(%r14)
	je	.LBB0_198
.LBB0_214:                              #   in Loop: Header=BB0_212 Depth=4
	cmpl	$0, 148(%r14)
	je	.LBB0_198
# BB#215:                               #   in Loop: Header=BB0_212 Depth=4
	movq	56(%r14), %rcx
	orq	%rax, %rcx
	je	.LBB0_198
# BB#216:                               #   in Loop: Header=BB0_212 Depth=4
	movl	28(%r12), %ecx
	cmpl	$-1, %ecx
	je	.LBB0_219
# BB#217:                               #   in Loop: Header=BB0_212 Depth=4
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ecx, %xmm0
	movd	%xmm0, 80(%rsp)         # 4-byte Folded Spill
	movl	$1, %edi
	callq	clock_GetSeconds
	movss	80(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB0_198
# BB#218:                               # %..critedge7_crit_edge.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movq	272(%rsp), %rax
.LBB0_219:                              # %.critedge7.i
                                        #   in Loop: Header=BB0_212 Depth=4
	testq	%rax, %rax
	je	.LBB0_230
# BB#220:                               #   in Loop: Header=BB0_212 Depth=4
	xorl	%edi, %edi
	callq	clock_StartCounter
	movq	%r14, %rdi
	movq	%rbp, %rsi
	leaq	16(%rsp), %rdx
	callq	split_Backtrack
	movq	%rax, %r15
	xorl	%edi, %edi
	callq	clock_StopAddPassedTime
	movq	%r15, %rdi
	callq	list_Length
	addl	%eax, 152(%r14)
	cmpq	$0, 120(%r14)
	movq	272(%rsp), %r12
	je	.LBB0_240
# BB#221:                               # %.preheader360.i
                                        #   in Loop: Header=BB0_212 Depth=4
	testq	%r12, %r12
	je	.LBB0_225
	.p2align	4, 0x90
.LBB0_222:                              #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        #         Parent Loop BB0_212 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	8(%r12), %rdi
	cmpq	%rbp, %rdi
	je	.LBB0_224
# BB#223:                               #   in Loop: Header=BB0_222 Depth=5
	callq	clause_Delete
	movq	272(%rsp), %r12
.LBB0_224:                              #   in Loop: Header=BB0_222 Depth=5
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	movq	%rax, 272(%rsp)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB0_222
.LBB0_225:                              # %._crit_edge.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	prfs_InsertDocProofClause
	movq	%rbp, %rbx
	movq	192(%rsp), %rbp         # 8-byte Reload
	cmpl	$0, 36(%rbp)
	je	.LBB0_227
# BB#226:                               #   in Loop: Header=BB0_212 Depth=4
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	224(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 224(%rsp)         # 8-byte Spill
.LBB0_227:                              #   in Loop: Header=BB0_212 Depth=4
	cmpl	$0, 40(%rbp)
	je	.LBB0_229
# BB#228:                               #   in Loop: Header=BB0_212 Depth=4
	movl	128(%r14), %esi
	movl	$.L.str.60, %edi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB0_229
	.p2align	4, 0x90
.LBB0_230:                              #   in Loop: Header=BB0_212 Depth=4
	cmpl	$0, 176(%r12)
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	je	.LBB0_246
# BB#231:                               #   in Loop: Header=BB0_212 Depth=4
	movq	104(%r14), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	112(%r14), %r15
	movq	16(%rsp), %r13
	testq	%r13, %r13
	movl	72(%rsp), %r12d         # 4-byte Reload
	je	.LBB0_233
# BB#232:                               #   in Loop: Header=BB0_212 Depth=4
	movq	56(%r14), %rax
	testq	%rax, %rax
	je	.LBB0_233
	.p2align	4, 0x90
.LBB0_238:                              # %.lr.ph.i87.i.i
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        #         Parent Loop BB0_212 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cmpq	%r13, 8(%rax)
	je	.LBB0_239
# BB#237:                               #   in Loop: Header=BB0_238 Depth=5
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB0_238
.LBB0_233:                              # %.loopexit.i.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movq	$0, 16(%rsp)
	cmpl	$0, 136(%r14)
	je	.LBB0_300
# BB#234:                               #   in Loop: Header=BB0_212 Depth=4
	movq	%r14, %rdi
	leaq	44(%rsp), %rsi
	callq	top_GetPowerfulSplitClause
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB0_300
# BB#235:                               #   in Loop: Header=BB0_212 Depth=4
	movq	56(%r13), %rax
	movslq	44(%rsp), %rcx
	movq	(%rax,%rcx,8), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%rbx, 8(%rbp)
	movq	$0, (%rbp)
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	prfs_DoSplitting
	movq	%rax, 16(%rsp)
	testq	%rbp, %rbp
	je	.LBB0_341
	.p2align	4, 0x90
.LBB0_236:                              # %.lr.ph.i92.i.i
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        #         Parent Loop BB0_212 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB0_236
	jmp	.LBB0_340
	.p2align	4, 0x90
.LBB0_240:                              #   in Loop: Header=BB0_212 Depth=4
	testq	%r12, %r12
	je	.LBB0_229
# BB#241:                               #   in Loop: Header=BB0_212 Depth=4
	testq	%r15, %r15
	je	.LBB0_245
# BB#242:                               # %.preheader.i330.i.preheader
                                        #   in Loop: Header=BB0_212 Depth=4
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB0_243:                              # %.preheader.i330.i
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        #         Parent Loop BB0_212 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_243
# BB#244:                               #   in Loop: Header=BB0_212 Depth=4
	movq	%r15, (%rax)
	jmp	.LBB0_245
	.p2align	4, 0x90
.LBB0_229:                              # %list_Nconc.exit332.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movq	%r15, %r12
.LBB0_245:                              # %list_Nconc.exit332.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movq	$0, 272(%rsp)
	xorl	%r15d, %r15d
	testq	%r12, %r12
	jne	.LBB0_364
	jmp	.LBB0_367
.LBB0_246:                              #   in Loop: Header=BB0_212 Depth=4
	movq	104(%r14), %rax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movq	112(%r14), %r13
	movl	red_WORKEDOFF(%rip), %r12d
	movl	72(%rsp), %ebx          # 4-byte Reload
	movq	%r13, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_247:                              #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        #         Parent Loop BB0_212 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB0_249 Depth 6
                                        #             Child Loop BB0_256 Depth 6
                                        #               Child Loop BB0_257 Depth 7
                                        #             Child Loop BB0_265 Depth 6
                                        #               Child Loop BB0_266 Depth 7
	movq	56(%r14), %rax
	testq	%rax, %rax
	je	.LBB0_311
# BB#248:                               #   in Loop: Header=BB0_247 Depth=5
	movq	16(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB0_251
	.p2align	4, 0x90
.LBB0_249:                              # %.lr.ph.i98.i.i
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        #         Parent Loop BB0_212 Depth=4
                                        #           Parent Loop BB0_247 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	cmpq	%rbp, 8(%rax)
	je	.LBB0_263
# BB#250:                               #   in Loop: Header=BB0_249 Depth=6
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB0_249
.LBB0_251:                              # %.loopexit506.i
                                        #   in Loop: Header=BB0_247 Depth=5
	movq	$0, 16(%rsp)
	cmpl	$0, 136(%r14)
	je	.LBB0_253
# BB#252:                               #   in Loop: Header=BB0_247 Depth=5
	movq	%r14, %rdi
	leaq	44(%rsp), %rsi
	callq	top_GetPowerfulSplitClause
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB0_281
.LBB0_253:                              # %.thread108.i.i
                                        #   in Loop: Header=BB0_247 Depth=5
	movl	%ebx, %eax
	cltd
	idivl	168(%r13)
	testl	%edx, %edx
	movl	%ebx, 72(%rsp)          # 4-byte Spill
	je	.LBB0_264
# BB#254:                               #   in Loop: Header=BB0_247 Depth=5
	cmpl	$1, 172(%r13)
	movq	56(%r14), %r15
	jne	.LBB0_278
# BB#255:                               #   in Loop: Header=BB0_247 Depth=5
	movq	8(%r15), %rbp
	movl	4(%rbp), %r14d
	movq	%rbp, %rdi
	callq	clause_NumberOfVarOccs
	movl	%eax, %r12d
	movq	(%r15), %rcx
	testq	%rcx, %rcx
	je	.LBB0_280
.LBB0_256:                              # %.lr.ph.i102.i.i
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        #         Parent Loop BB0_212 Depth=4
                                        #           Parent Loop BB0_247 Depth=5
                                        # =>          This Loop Header: Depth=6
                                        #               Child Loop BB0_257 Depth 7
	movq	%rcx, %rbx
	movq	80(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_257:                              #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        #         Parent Loop BB0_212 Depth=4
                                        #           Parent Loop BB0_247 Depth=5
                                        #             Parent Loop BB0_256 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movq	8(%rbx), %rdi
	cmpl	%r14d, 4(%rdi)
	jne	.LBB0_280
# BB#258:                               #   in Loop: Header=BB0_257 Depth=7
	callq	clause_NumberOfVarOccs
	cmpl	$0, 188(%r15)
	je	.LBB0_261
# BB#259:                               #   in Loop: Header=BB0_257 Depth=7
	cmpl	%eax, %r12d
	jae	.LBB0_262
	jmp	.LBB0_260
	.p2align	4, 0x90
.LBB0_261:                              #   in Loop: Header=BB0_257 Depth=7
	cmpl	%eax, %r12d
	ja	.LBB0_260
.LBB0_262:                              # %.backedge.i.i.i
                                        #   in Loop: Header=BB0_257 Depth=7
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_257
	jmp	.LBB0_280
	.p2align	4, 0x90
.LBB0_260:                              # %.outer.backedge.i.i.i
                                        #   in Loop: Header=BB0_256 Depth=6
	movq	(%rbx), %rcx
	movq	8(%rbx), %rbp
	movl	4(%rbp), %r14d
	testq	%rcx, %rcx
	movl	%eax, %r12d
	jne	.LBB0_256
	jmp	.LBB0_280
	.p2align	4, 0x90
.LBB0_263:                              # %list_PointerMember.exit.thread.i.i
                                        #   in Loop: Header=BB0_247 Depth=5
	movq	$0, 16(%rsp)
	jmp	.LBB0_281
.LBB0_264:                              #   in Loop: Header=BB0_247 Depth=5
	movq	56(%r14), %rbx
	movq	8(%rbx), %r14
	movl	4(%r14), %r12d
	movl	8(%r14), %r13d
	movq	%r14, %rdi
	callq	clause_NumberOfVarOccs
	movl	%eax, 120(%rsp)         # 4-byte Spill
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB0_279
.LBB0_265:                              # %.lr.ph.i104.i.i
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        #         Parent Loop BB0_212 Depth=4
                                        #           Parent Loop BB0_247 Depth=5
                                        # =>          This Loop Header: Depth=6
                                        #               Child Loop BB0_266 Depth 7
	movq	%r14, 184(%rsp)         # 8-byte Spill
	movl	%r13d, %r15d
	.p2align	4, 0x90
.LBB0_266:                              #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        #         Parent Loop BB0_212 Depth=4
                                        #           Parent Loop BB0_247 Depth=5
                                        #             Parent Loop BB0_265 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movq	8(%rbx), %rbp
	movl	8(%rbp), %r13d
	cmpl	%r15d, %r13d
	ja	.LBB0_274
# BB#267:                               #   in Loop: Header=BB0_266 Depth=7
	movl	4(%rbp), %r14d
	jb	.LBB0_276
# BB#268:                               #   in Loop: Header=BB0_266 Depth=7
	cmpl	%r12d, %r14d
	jb	.LBB0_276
# BB#269:                               #   in Loop: Header=BB0_266 Depth=7
	jne	.LBB0_274
# BB#270:                               #   in Loop: Header=BB0_266 Depth=7
	movq	%rbp, %rdi
	callq	clause_NumberOfVarOccs
	movq	80(%rsp), %rcx          # 8-byte Reload
	cmpl	$0, 188(%rcx)
	je	.LBB0_273
# BB#271:                               #   in Loop: Header=BB0_266 Depth=7
	cmpl	%eax, 120(%rsp)         # 4-byte Folded Reload
	jae	.LBB0_274
	jmp	.LBB0_272
.LBB0_273:                              #   in Loop: Header=BB0_266 Depth=7
	cmpl	%eax, 120(%rsp)         # 4-byte Folded Reload
	ja	.LBB0_272
	.p2align	4, 0x90
.LBB0_274:                              # %.backedge.i105.i.i
                                        #   in Loop: Header=BB0_266 Depth=7
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_266
	jmp	.LBB0_275
	.p2align	4, 0x90
.LBB0_276:                              # %.loopexit.i.i.i
                                        #   in Loop: Header=BB0_265 Depth=6
	movq	%rbp, %rdi
	callq	clause_NumberOfVarOccs
	jmp	.LBB0_277
.LBB0_272:                              #   in Loop: Header=BB0_265 Depth=6
	movq	8(%rbx), %rbp
	movl	4(%rbp), %r14d
.LBB0_277:                              # %.outer.backedge.i106.i.i
                                        #   in Loop: Header=BB0_265 Depth=6
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movl	%r14d, %r12d
	movq	%rbp, %r14
	movl	%eax, 120(%rsp)         # 4-byte Spill
	jne	.LBB0_265
	jmp	.LBB0_280
.LBB0_278:                              #   in Loop: Header=BB0_247 Depth=5
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	top_SelectMinimalConWeightClause
	movq	%rax, %rbp
	jmp	.LBB0_280
.LBB0_275:                              #   in Loop: Header=BB0_247 Depth=5
	movq	184(%rsp), %rbp         # 8-byte Reload
	jmp	.LBB0_280
.LBB0_279:                              #   in Loop: Header=BB0_247 Depth=5
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB0_280:                              # %top_SelectClauseDepth.exit.i.i
                                        #   in Loop: Header=BB0_247 Depth=5
	movl	72(%rsp), %ebx          # 4-byte Reload
	incl	%ebx
	movq	(%rsp), %r14            # 8-byte Reload
	movl	red_WORKEDOFF(%rip), %r12d
	movq	80(%rsp), %r13          # 8-byte Reload
.LBB0_281:                              #   in Loop: Header=BB0_247 Depth=5
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	prfs_ExtractUsable
	movl	$4, %edi
	callq	clock_StartCounter
	movq	%r14, %rdi
	movq	%rbp, %rsi
	movl	%r12d, %edx
	callq	red_CompleteReductionOnDerivedClause
	movq	%rax, %r15
	movl	$4, %edi
	callq	clock_StopAddPassedTime
	testq	%r15, %r15
	je	.LBB0_247
# BB#282:                               # %.critedge87.i.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movl	%ebx, 72(%rsp)          # 4-byte Spill
	cmpl	$0, 68(%r15)
	leaq	272(%rsp), %r13
	jne	.LBB0_285
# BB#283:                               #   in Loop: Header=BB0_212 Depth=4
	cmpl	$0, 72(%r15)
	jne	.LBB0_285
# BB#284:                               # %clause_IsEmptyClause.exit.i.i
                                        #   in Loop: Header=BB0_212 Depth=4
	cmpl	$0, 64(%r15)
	je	.LBB0_376
.LBB0_285:                              # %clause_IsEmptyClause.exit.thread.i.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movl	$4, %edi
	callq	clock_StartCounter
	movq	%r14, %rdi
	movq	%r15, %rsi
	movl	red_WORKEDOFF(%rip), %edx
	callq	red_BackReduction
	movq	%rax, %r12
	movl	$4, %edi
	callq	clock_StopAddPassedTime
	movq	80(%rsp), %r13          # 8-byte Reload
	cmpl	$0, 96(%r13)
	je	.LBB0_287
# BB#286:                               #   in Loop: Header=BB0_212 Depth=4
	movq	stdout(%rip), %rcx
	movl	$.L.str.66, %edi
	movl	$16, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r15, %rdi
	callq	clause_Print
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB0_287:                              #   in Loop: Header=BB0_212 Depth=4
	cmpl	$0, 136(%r14)
	je	.LBB0_296
# BB#288:                               #   in Loop: Header=BB0_212 Depth=4
	movq	%r15, %rdi
	callq	top_GetLiteralsForSplitting
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_327
# BB#289:                               # %.lr.ph.i97.i.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movl	$-1, %r14d
	movl	$-1, %ebp
	.p2align	4, 0x90
.LBB0_290:                              #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        #         Parent Loop BB0_212 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movslq	8(%rbx), %rax
	movq	56(%r15), %rcx
	movq	(%rcx,%rax,8), %rsi
	xorl	%edx, %edx
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	prfs_GetNumberOfInstances
	cmpl	%ebp, %eax
	jbe	.LBB0_292
# BB#291:                               #   in Loop: Header=BB0_290 Depth=5
	movl	8(%rbx), %r14d
	movl	%eax, %ebp
.LBB0_292:                              #   in Loop: Header=BB0_290 Depth=5
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB0_290
# BB#293:                               # %top_GetOptimalSplitLiteralIndex.exit.i.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movl	%r14d, 44(%rsp)
	testl	%r14d, %r14d
	js	.LBB0_328
# BB#294:                               #   in Loop: Header=BB0_212 Depth=4
	movq	56(%r15), %rax
	movslq	%r14d, %rcx
	movq	(%rax,%rcx,8), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbp, 8(%rbx)
	movq	$0, (%rbx)
	movq	(%rsp), %r14            # 8-byte Reload
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	prfs_DoSplitting
	movq	%rax, %rbp
	movq	%rbp, 16(%rsp)
	testq	%rbx, %rbx
	je	.LBB0_329
	.p2align	4, 0x90
.LBB0_295:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        #         Parent Loop BB0_212 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB0_295
.LBB0_296:                              # %list_Delete.exitthread-pre-split.i.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movq	16(%rsp), %rbp
	testq	%rbp, %rbp
	jne	.LBB0_330
	jmp	.LBB0_297
.LBB0_300:                              # %.thread99.i.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movl	%r12d, %eax
	cltd
	idivl	168(%r15)
	testl	%edx, %edx
	movq	%r15, 80(%rsp)          # 8-byte Spill
	je	.LBB0_312
# BB#301:                               #   in Loop: Header=BB0_212 Depth=4
	cmpl	$1, 172(%r15)
	movq	56(%r14), %r14
	jne	.LBB0_337
# BB#302:                               #   in Loop: Header=BB0_212 Depth=4
	movq	8(%r14), %r13
	movl	4(%r13), %r15d
	movq	%r13, %rdi
	callq	clause_NumberOfVarOccs
	movl	%eax, %ebp
	movq	(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB0_338
# BB#303:                               # %.lr.ph.lr.ph.i.i.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movq	(%rsp), %r14            # 8-byte Reload
.LBB0_304:                              # %.lr.ph.i88.i.i
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        #         Parent Loop BB0_212 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB0_305 Depth 6
	movq	%rcx, %rbx
	.p2align	4, 0x90
.LBB0_305:                              #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        #         Parent Loop BB0_212 Depth=4
                                        #           Parent Loop BB0_304 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	8(%rbx), %rdi
	cmpl	%r15d, 4(%rdi)
	jne	.LBB0_339
# BB#306:                               #   in Loop: Header=BB0_305 Depth=6
	callq	clause_NumberOfVarOccs
	movq	80(%rsp), %rcx          # 8-byte Reload
	cmpl	$0, 188(%rcx)
	je	.LBB0_309
# BB#307:                               #   in Loop: Header=BB0_305 Depth=6
	cmpl	%eax, %ebp
	jae	.LBB0_310
	jmp	.LBB0_308
	.p2align	4, 0x90
.LBB0_309:                              #   in Loop: Header=BB0_305 Depth=6
	cmpl	%eax, %ebp
	ja	.LBB0_308
.LBB0_310:                              # %.backedge.i.i295.i
                                        #   in Loop: Header=BB0_305 Depth=6
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_305
	jmp	.LBB0_339
	.p2align	4, 0x90
.LBB0_308:                              # %.outer.backedge.i.i301.i
                                        #   in Loop: Header=BB0_304 Depth=5
	movq	(%rbx), %rcx
	movq	8(%rbx), %r13
	movl	4(%r13), %r15d
	testq	%rcx, %rcx
	movl	%eax, %ebp
	jne	.LBB0_304
	jmp	.LBB0_339
.LBB0_311:                              #   in Loop: Header=BB0_212 Depth=4
	movl	%ebx, 72(%rsp)          # 4-byte Spill
	xorl	%r12d, %r12d
	jmp	.LBB0_362
.LBB0_239:                              # %.list_Delete.exit93.thread_crit_edge.i.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movq	$0, 16(%rsp)
	cmpl	$0, 136(%r14)
	jne	.LBB0_343
	jmp	.LBB0_353
.LBB0_312:                              #   in Loop: Header=BB0_212 Depth=4
	movq	56(%r14), %rbp
	movq	8(%rbp), %r13
	movl	4(%r13), %r12d
	movl	8(%r13), %r15d
	movq	%r13, %rdi
	callq	clause_NumberOfVarOccs
	movl	%eax, 184(%rsp)         # 4-byte Spill
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB0_339
# BB#313:                               # %.lr.ph.lr.ph.i94.i.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movq	%r13, 208(%rsp)         # 8-byte Spill
.LBB0_314:                              # %.lr.ph.i95.i.i
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        #         Parent Loop BB0_212 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB0_315 Depth 6
	movl	%r15d, %r14d
	.p2align	4, 0x90
.LBB0_315:                              #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        #         Parent Loop BB0_212 Depth=4
                                        #           Parent Loop BB0_314 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	8(%rbp), %r13
	movl	8(%r13), %r15d
	cmpl	%r14d, %r15d
	ja	.LBB0_323
# BB#316:                               #   in Loop: Header=BB0_315 Depth=6
	movl	4(%r13), %ebx
	jb	.LBB0_325
# BB#317:                               #   in Loop: Header=BB0_315 Depth=6
	cmpl	%r12d, %ebx
	jb	.LBB0_325
# BB#318:                               #   in Loop: Header=BB0_315 Depth=6
	jne	.LBB0_323
# BB#319:                               #   in Loop: Header=BB0_315 Depth=6
	movq	%r13, %rdi
	callq	clause_NumberOfVarOccs
	movq	80(%rsp), %rcx          # 8-byte Reload
	cmpl	$0, 188(%rcx)
	je	.LBB0_322
# BB#320:                               #   in Loop: Header=BB0_315 Depth=6
	cmpl	%eax, 184(%rsp)         # 4-byte Folded Reload
	jae	.LBB0_323
	jmp	.LBB0_321
.LBB0_322:                              #   in Loop: Header=BB0_315 Depth=6
	cmpl	%eax, 184(%rsp)         # 4-byte Folded Reload
	ja	.LBB0_321
	.p2align	4, 0x90
.LBB0_323:                              # %.backedge.i96.i.i
                                        #   in Loop: Header=BB0_315 Depth=6
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB0_315
	jmp	.LBB0_324
	.p2align	4, 0x90
.LBB0_325:                              # %.loopexit.i.i266.i
                                        #   in Loop: Header=BB0_314 Depth=5
	movq	%r13, %rdi
	callq	clause_NumberOfVarOccs
	jmp	.LBB0_326
.LBB0_321:                              #   in Loop: Header=BB0_314 Depth=5
	movq	8(%rbp), %r13
	movl	4(%r13), %ebx
.LBB0_326:                              # %.outer.backedge.i97.i.i
                                        #   in Loop: Header=BB0_314 Depth=5
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	movl	%ebx, %r12d
	movq	%r13, 208(%rsp)         # 8-byte Spill
	movl	%eax, 184(%rsp)         # 4-byte Spill
	movq	(%rsp), %r14            # 8-byte Reload
	jne	.LBB0_314
	jmp	.LBB0_339
.LBB0_337:                              #   in Loop: Header=BB0_212 Depth=4
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	top_SelectMinimalConWeightClause
	movq	%rax, %r13
	jmp	.LBB0_338
.LBB0_324:                              #   in Loop: Header=BB0_212 Depth=4
	movq	208(%rsp), %r13         # 8-byte Reload
.LBB0_338:                              #   in Loop: Header=BB0_212 Depth=4
	movq	(%rsp), %r14            # 8-byte Reload
.LBB0_339:                              # %top_SelectClauseDepth.exit.i302.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movl	72(%rsp), %r12d         # 4-byte Reload
	incl	%r12d
	movq	80(%rsp), %r15          # 8-byte Reload
.LBB0_340:                              # %list_Delete.exit93thread-pre-split.i.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movq	16(%rsp), %rax
.LBB0_341:                              # %list_Delete.exit93.i.i
                                        #   in Loop: Header=BB0_212 Depth=4
	testq	%rax, %rax
	jne	.LBB0_353
# BB#342:                               # %list_Delete.exit93.thread.i.i
                                        #   in Loop: Header=BB0_212 Depth=4
	cmpl	$0, 136(%r14)
	je	.LBB0_353
.LBB0_343:                              #   in Loop: Header=BB0_212 Depth=4
	movq	%r13, %rdi
	callq	top_GetLiteralsForSplitting
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_351
# BB#344:                               # %.lr.ph.i86.i.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movl	$-1, %r14d
	movl	$-1, %ebx
	.p2align	4, 0x90
.LBB0_345:                              #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        #         Parent Loop BB0_212 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movslq	8(%rbp), %rax
	movq	56(%r13), %rcx
	movq	(%rcx,%rax,8), %rsi
	xorl	%edx, %edx
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	prfs_GetNumberOfInstances
	cmpl	%ebx, %eax
	jbe	.LBB0_347
# BB#346:                               #   in Loop: Header=BB0_345 Depth=5
	movl	8(%rbp), %r14d
	movl	%eax, %ebx
.LBB0_347:                              #   in Loop: Header=BB0_345 Depth=5
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB0_345
# BB#348:                               # %top_GetOptimalSplitLiteralIndex.exit.i319.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movl	%r14d, 44(%rsp)
	testl	%r14d, %r14d
	js	.LBB0_352
# BB#349:                               #   in Loop: Header=BB0_212 Depth=4
	movq	56(%r13), %rax
	movslq	%r14d, %rcx
	movq	(%rax,%rcx,8), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%rbx, 8(%rbp)
	movq	$0, (%rbp)
	movq	(%rsp), %r14            # 8-byte Reload
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	prfs_DoSplitting
	movq	%rax, 16(%rsp)
	testq	%rbp, %rbp
	je	.LBB0_353
	.p2align	4, 0x90
.LBB0_350:                              # %.lr.ph.i.i323.i
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        #         Parent Loop BB0_212 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB0_350
	jmp	.LBB0_353
.LBB0_351:                              # %top_GetOptimalSplitLiteralIndex.exit.thread.i305.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movl	$-1, 44(%rsp)
.LBB0_352:                              #   in Loop: Header=BB0_212 Depth=4
	movq	(%rsp), %r14            # 8-byte Reload
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	prfs_PerformSplitting
	movq	%rax, 16(%rsp)
	.p2align	4, 0x90
.LBB0_353:                              # %list_Delete.exit.i324.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	prfs_ExtractUsable
	cmpl	$0, 96(%r15)
	je	.LBB0_355
# BB#354:                               #   in Loop: Header=BB0_212 Depth=4
	movq	stdout(%rip), %rcx
	movl	$.L.str.66, %edi
	movl	$16, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r13, %rdi
	callq	clause_Print
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB0_355:                              #   in Loop: Header=BB0_212 Depth=4
	movl	%r12d, 72(%rsp)         # 4-byte Spill
	movq	16(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB0_357
# BB#356:                               #   in Loop: Header=BB0_212 Depth=4
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	%rbx, 8(%r12)
	movq	$0, (%r12)
	jmp	.LBB0_361
.LBB0_357:                              #   in Loop: Header=BB0_212 Depth=4
	cmpl	$0, 348(%r15)
	je	.LBB0_360
# BB#358:                               #   in Loop: Header=BB0_212 Depth=4
	movl	$4, %edi
	callq	clock_StartCounter
	movl	348(%r15), %esi
	movq	32(%r14), %rdx
	movq	48(%r14), %rcx
	movq	%r13, %rdi
	movq	%r15, %r8
	movq	120(%rsp), %r9          # 8-byte Reload
	callq	red_Terminator
	movq	%rax, %rbp
	movl	$4, %edi
	callq	clock_StopAddPassedTime
	testq	%rbp, %rbp
	je	.LBB0_360
# BB#359:                               #   in Loop: Header=BB0_212 Depth=4
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	%rbp, 8(%r12)
	movq	$0, (%r12)
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	prfs_InsertUsableClause
	testq	%r12, %r12
	jne	.LBB0_361
.LBB0_360:                              # %.thread103.i.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	clause_SelectLiteral
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	prfs_InsertWorkedOffClause
	movl	$5, %edi
	callq	clock_StartCounter
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	inf_DerivableClauses
	movq	%rax, %r12
	movl	$5, %edi
	callq	clock_StopAddPassedTime
.LBB0_361:                              # %top_FullReductionSelectGivenComputeDerivables.exit.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movq	%r12, %rdi
	callq	list_Length
	addl	%eax, 144(%r14)
.LBB0_362:                              #   in Loop: Header=BB0_212 Depth=4
	leaq	272(%rsp), %r13
.LBB0_363:                              #   in Loop: Header=BB0_212 Depth=4
	movq	112(%rsp), %r15         # 8-byte Reload
	testq	%r12, %r12
	je	.LBB0_367
.LBB0_364:                              #   in Loop: Header=BB0_212 Depth=4
	movq	192(%rsp), %rax         # 8-byte Reload
	movl	92(%rax), %eax
	testl	%eax, %eax
	je	.LBB0_367
# BB#365:                               # %.lr.ph419.i.preheader
                                        #   in Loop: Header=BB0_212 Depth=4
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB0_366:                              # %.lr.ph419.i
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        #         Parent Loop BB0_212 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	stdout(%rip), %rcx
	movl	$.L.str.61, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	movq	8(%rbx), %rdi
	callq	clause_Print
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_366
.LBB0_367:                              # %.loopexit.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	split_ExtractEmptyClauses
	movq	%rax, %rbx
	movl	$4, %edi
	callq	clock_StartCounter
	movq	192(%rsp), %r12         # 8-byte Reload
	cmpl	$0, 176(%r12)
	movq	272(%rsp), %rbp
	movl	red_ALL(%rip), %edx
	cmovel	red_WORKEDOFF(%rip), %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movl	108(%rsp), %ecx         # 4-byte Reload
	movl	204(%rsp), %r8d         # 4-byte Reload
	leaq	40(%rsp), %r9
	callq	red_CompleteReductionOnDerivedClauses
	testq	%rbp, %rbp
	je	.LBB0_372
# BB#368:                               #   in Loop: Header=BB0_212 Depth=4
	testq	%rax, %rax
	je	.LBB0_373
# BB#369:                               # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB0_212 Depth=4
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB0_370:                              # %.preheader.i.i
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        #         Parent Loop BB0_212 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_370
# BB#371:                               #   in Loop: Header=BB0_212 Depth=4
	movq	%rax, (%rcx)
	jmp	.LBB0_373
	.p2align	4, 0x90
.LBB0_372:                              #   in Loop: Header=BB0_212 Depth=4
	movq	%rax, %rbp
.LBB0_373:                              # %list_Nconc.exit.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movq	%rbp, 272(%rsp)
	movl	$4, %edi
	callq	clock_StopAddPassedTime
	movq	272(%rsp), %rdi
	testq	%rdi, %rdi
	leaq	272(%rsp), %r13
	movq	%r15, %rbp
	je	.LBB0_211
# BB#374:                               #   in Loop: Header=BB0_212 Depth=4
	callq	split_SmallestSplitLevelClause
	movq	%rax, %rbp
	cmpl	$0, 112(%r12)
	je	.LBB0_211
# BB#375:                               #   in Loop: Header=BB0_212 Depth=4
	movq	stdout(%rip), %rcx
	movl	$.L.str.62, %edi
	movl	$14, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbp, %rdi
	callq	clause_Print
	jmp	.LBB0_211
.LBB0_327:                              # %top_GetOptimalSplitLiteralIndex.exit.thread.i.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movl	$-1, 44(%rsp)
.LBB0_328:                              #   in Loop: Header=BB0_212 Depth=4
	movq	(%rsp), %r14            # 8-byte Reload
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	prfs_PerformSplitting
	movq	%rax, %rbp
	movq	%rbp, 16(%rsp)
.LBB0_329:                              # %list_Delete.exit.i.i
                                        #   in Loop: Header=BB0_212 Depth=4
	testq	%rbp, %rbp
	je	.LBB0_297
.LBB0_330:                              #   in Loop: Header=BB0_212 Depth=4
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, %r12
.LBB0_331:                              #   in Loop: Header=BB0_212 Depth=4
	leaq	272(%rsp), %r13
	movq	112(%rsp), %r15         # 8-byte Reload
	jmp	.LBB0_380
.LBB0_297:                              #   in Loop: Header=BB0_212 Depth=4
	movl	348(%r13), %esi
	testl	%esi, %esi
	je	.LBB0_332
# BB#298:                               #   in Loop: Header=BB0_212 Depth=4
	movq	32(%r14), %rdx
	movq	48(%r14), %rcx
	movq	%r15, %rdi
	movq	%r13, %r8
	movq	208(%rsp), %r9          # 8-byte Reload
	callq	red_Terminator
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_332
# BB#299:                               #   in Loop: Header=BB0_212 Depth=4
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%rbx, 8(%rbp)
	movq	%r12, (%rbp)
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	prfs_InsertUsableClause
	movq	%rbp, %r12
	jmp	.LBB0_331
.LBB0_332:                              #   in Loop: Header=BB0_212 Depth=4
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	clause_SelectLiteral
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	prfs_InsertWorkedOffClause
	movl	$5, %edi
	callq	clock_StartCounter
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	inf_DerivableClauses
	testq	%r12, %r12
	je	.LBB0_377
# BB#333:                               #   in Loop: Header=BB0_212 Depth=4
	testq	%rax, %rax
	leaq	272(%rsp), %r13
	je	.LBB0_378
# BB#334:                               # %.preheader.i.i.i.preheader
                                        #   in Loop: Header=BB0_212 Depth=4
	movq	%r12, %rdx
	movq	112(%rsp), %r15         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_335:                              # %.preheader.i.i.i
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_202 Depth=2
                                        #       Parent Loop BB0_203 Depth=3
                                        #         Parent Loop BB0_212 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_335
# BB#336:                               #   in Loop: Header=BB0_212 Depth=4
	movq	%rax, (%rcx)
	jmp	.LBB0_379
.LBB0_377:                              #   in Loop: Header=BB0_212 Depth=4
	movq	%rax, %r12
	leaq	272(%rsp), %r13
.LBB0_378:                              #   in Loop: Header=BB0_212 Depth=4
	movq	112(%rsp), %r15         # 8-byte Reload
.LBB0_379:                              # %list_Nconc.exit.i.i
                                        #   in Loop: Header=BB0_212 Depth=4
	movl	$5, %edi
	callq	clock_StopAddPassedTime
.LBB0_380:                              #   in Loop: Header=BB0_212 Depth=4
	movq	%r12, %rdi
	callq	list_Length
	addl	%eax, 144(%r14)
	testq	%r12, %r12
	jne	.LBB0_364
	jmp	.LBB0_367
.LBB0_376:                              #   in Loop: Header=BB0_212 Depth=4
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	%r15, 8(%r12)
	movq	$0, (%r12)
	jmp	.LBB0_363
	.p2align	4, 0x90
.LBB0_198:                              # %.critedge4.i
                                        #   in Loop: Header=BB0_203 Depth=3
	cmpb	$0, 31(%rsp)            # 1-byte Folded Reload
	je	.LBB0_203
	jmp	.LBB0_199
	.p2align	4, 0x90
.LBB0_381:                              # %..critedge_crit_edge.i
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	272(%rsp), %rax
.LBB0_382:                              # %top_ProofSearch.exit
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	%rax, 8(%r14)
	movq	224(%rsp), %rax         # 8-byte Reload
	movq	%rax, 16(%r14)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	28(%rax), %eax
	cmpl	$-1, %eax
	je	.LBB0_384
# BB#383:                               #   in Loop: Header=BB0_89 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	movd	%xmm0, 80(%rsp)         # 4-byte Folded Spill
	movl	$1, %edi
	callq	clock_GetSeconds
	movl	$2, %r14d
	movss	80(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB0_388
.LBB0_384:                              #   in Loop: Header=BB0_89 Depth=1
	movl	$2, %r14d
	movq	(%rsp), %rax            # 8-byte Reload
	cmpl	$0, 148(%rax)
	je	.LBB0_388
# BB#385:                               #   in Loop: Header=BB0_89 Depth=1
	movq	8(%rax), %rax
	cmpl	$-1, 40(%rsp)
	je	.LBB0_387
# BB#386:                               #   in Loop: Header=BB0_89 Depth=1
	testq	%rax, %rax
	je	.LBB0_388
.LBB0_387:                              # %._crit_edge741
                                        #   in Loop: Header=BB0_89 Depth=1
	xorl	%r14d, %r14d
	testq	%rax, %rax
	sete	%r14b
.LBB0_388:                              #   in Loop: Header=BB0_89 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 28(%rax)
	je	.LBB0_440
# BB#389:                               #   in Loop: Header=BB0_89 Depth=1
	movl	$.L.str.12, %edi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	printf
	movq	stdout(%rip), %rcx
	movl	$.L.str.13, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movl	%r14d, %eax
	andb	$3, %al
	je	.LBB0_393
# BB#390:                               #   in Loop: Header=BB0_89 Depth=1
	cmpb	$2, %al
	jne	.LBB0_394
# BB#391:                               #   in Loop: Header=BB0_89 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	cmpl	$0, 148(%rax)
	movq	stdout(%rip), %rcx
	je	.LBB0_395
# BB#392:                               #   in Loop: Header=BB0_89 Depth=1
	movl	$.L.str.14, %edi
	movl	$16, %esi
	jmp	.LBB0_396
	.p2align	4, 0x90
.LBB0_393:                              #   in Loop: Header=BB0_89 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.16, %edi
	movl	$12, %esi
	jmp	.LBB0_396
	.p2align	4, 0x90
.LBB0_394:                              #   in Loop: Header=BB0_89 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.17, %edi
	movl	$17, %esi
	jmp	.LBB0_396
.LBB0_395:                              #   in Loop: Header=BB0_89 Depth=1
	movl	$.L.str.15, %edi
	movl	$33, %esi
	.p2align	4, 0x90
.LBB0_396:                              #   in Loop: Header=BB0_89 Depth=1
	movl	$1, %edx
	callq	fwrite
	movq	top_InputFile(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB0_398
# BB#397:                               #   in Loop: Header=BB0_89 Depth=1
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rsi
	cmoveq	%rbx, %rsi
	jmp	.LBB0_399
	.p2align	4, 0x90
.LBB0_398:                              #   in Loop: Header=BB0_89 Depth=1
	movl	$.L.str.19, %esi
.LBB0_399:                              #   in Loop: Header=BB0_89 Depth=1
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	printf
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 116(%rax)
	je	.LBB0_401
# BB#400:                               #   in Loop: Header=BB0_89 Depth=1
	movl	$1, %edi
	callq	clock_StopPassedTime
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	144(%rbx), %esi
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	printf
	movl	152(%rbx), %esi
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	printf
	movl	140(%rbx), %esi
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	callq	printf
	movq	stdout(%rip), %rcx
	movl	$.L.str.23, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	clock_PrintTime
	movq	stdout(%rip), %rcx
	movl	$.L.str.24, %edi
	movl	$19, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$2, %edi
	callq	clock_PrintTime
	movq	stdout(%rip), %rcx
	movl	$.L.str.25, %edi
	movl	$18, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$3, %edi
	callq	clock_PrintTime
	movq	stdout(%rip), %rcx
	movl	$.L.str.26, %edi
	movl	$36, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$5, %edi
	callq	clock_PrintTime
	movq	stdout(%rip), %rcx
	movl	$.L.str.27, %edi
	movl	$19, %esi
	movl	$1, %edx
	callq	fwrite
	xorl	%edi, %edi
	callq	clock_PrintTime
	movq	stdout(%rip), %rcx
	movl	$.L.str.28, %edi
	movl	$25, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$4, %edi
	callq	clock_PrintTime
	movl	$.L.str.29, %edi
	callq	puts
.LBB0_401:                              #   in Loop: Header=BB0_89 Depth=1
	testl	%r14d, %r14d
	je	.LBB0_410
# BB#402:                               #   in Loop: Header=BB0_89 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 120(%rax)
	je	.LBB0_410
# BB#403:                               #   in Loop: Header=BB0_89 Depth=1
	movq	top_InputFile(%rip), %rsi
	testq	%rsi, %rsi
	movl	$.L.str.31, %eax
	cmoveq	%rax, %rsi
	movq	%r13, %rdi
	callq	strcpy
	movq	%r13, %rdi
	callq	strlen
	leaq	272(%rsp,%rax), %rax
	cmpl	$1, %r14d
	jne	.LBB0_405
# BB#404:                               #   in Loop: Header=BB0_89 Depth=1
	movb	$0, 6(%rax)
	movw	$27749, 4(%rax)         # imm = 0x6C65
	movl	$1685024046, (%rax)     # imm = 0x646F6D2E
	jmp	.LBB0_406
.LBB0_405:                              #   in Loop: Header=BB0_89 Depth=1
	movabsq	$8315179234992349998, %rcx # imm = 0x73657375616C632E
	movq	%rcx, (%rax)
	movb	$0, 8(%rax)
.LBB0_406:                              #   in Loop: Header=BB0_89 Depth=1
	movl	$.L.str.34, %esi
	movq	%r13, %rdi
	callq	misc_OpenFile
	movq	%rax, %r15
	xorl	%ebp, %ebp
	movq	8(%rsp), %r12           # 8-byte Reload
	cmpl	$2, 120(%r12)
	sete	%bpl
	callq	dfg_ProblemDescription
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	40(%rbx), %r10
	cmpl	$1, %r14d
	jne	.LBB0_408
# BB#407:                               #   in Loop: Header=BB0_89 Depth=1
	movl	$.L.str.35, %edx
	movl	$.L.str.30, %ecx
	movl	$.L.str.36, %r8d
	movq	%r15, %rdi
	movl	%ebp, %esi
	movq	%rax, %r9
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	callq	clause_FPrintCnfFormulasDFGProblem
	addq	$32, %rsp
.Lcfi25:
	.cfi_adjust_cfa_offset -32
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	misc_CloseFile
	movl	$.L.str.39, %edi
	jmp	.LBB0_409
.LBB0_408:                              #   in Loop: Header=BB0_89 Depth=1
	movl	$.L.str.37, %edx
	movl	$.L.str.30, %ecx
	movl	$.L.str.38, %r8d
	movq	%r15, %rdi
	movl	%ebp, %esi
	movq	%rax, %r9
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rbx)
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	callq	clause_FPrintCnfFormulasDFGProblem
	addq	$32, %rsp
.Lcfi30:
	.cfi_adjust_cfa_offset -32
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	misc_CloseFile
	movl	$.L.str.40, %edi
.LBB0_409:                              #   in Loop: Header=BB0_89 Depth=1
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	printf
.LBB0_410:                              #   in Loop: Header=BB0_89 Depth=1
	cmpl	$2, %r14d
	je	.LBB0_440
# BB#411:                               #   in Loop: Header=BB0_89 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	36(%rax), %eax
	testl	%eax, %eax
	je	.LBB0_440
# BB#412:                               #   in Loop: Header=BB0_89 Depth=1
	cmpl	$1, %r14d
	jne	.LBB0_414
# BB#413:                               #   in Loop: Header=BB0_89 Depth=1
	movl	$.L.str.41, %edi
	callq	puts
	movq	(%rsp), %rax            # 8-byte Reload
	movq	40(%rax), %rdi
	callq	clause_ListPrint
	jmp	.LBB0_440
.LBB0_414:                              #   in Loop: Header=BB0_89 Depth=1
	movq	top_InputFile(%rip), %rdx
	testq	%rdx, %rdx
	movq	(%rsp), %rdi            # 8-byte Reload
	jne	.LBB0_416
# BB#415:                               #   in Loop: Header=BB0_89 Depth=1
	movq	$.L.str.31, top_InputFile(%rip)
	movl	$.L.str.31, %edx
.LBB0_416:                              #   in Loop: Header=BB0_89 Depth=1
	movq	8(%rdi), %rsi
	callq	dp_PrintProof
	movq	%rax, %rbx
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	je	.LBB0_433
# BB#417:                               # %.lr.ph631.preheader
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB0_418:                              # %.lr.ph631
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_420 Depth 3
                                        #       Child Loop BB0_427 Depth 3
	movq	8(%rbp), %rcx
	cmpl	$16, 76(%rcx)
	jne	.LBB0_430
# BB#419:                               #   in Loop: Header=BB0_418 Depth=2
	movq	%rcx, %rax
	movabsq	$1908283869694091547, %rdx # imm = 0x1A7B9611A7B9611B
	mulq	%rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rdx, %rax
	shrq	$4, %rax
	imulq	$29, %rax, %rax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.LBB0_422
	.p2align	4, 0x90
.LBB0_420:                              # %.lr.ph.i402
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_418 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rax), %rdx
	cmpq	%rcx, 8(%rdx)
	je	.LBB0_423
# BB#421:                               #   in Loop: Header=BB0_420 Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB0_420
.LBB0_422:                              #   in Loop: Header=BB0_418 Depth=2
	xorl	%edi, %edi
	jmp	.LBB0_424
.LBB0_423:                              #   in Loop: Header=BB0_418 Depth=2
	movq	(%rdx), %rdi
.LBB0_424:                              # %hsh_Get.exit
                                        #   in Loop: Header=BB0_418 Depth=2
	callq	list_Copy
	movl	$cnf_LabelEqual, %esi
	movq	%rax, %rdi
	callq	list_DeleteDuplicates
	testq	%r14, %r14
	je	.LBB0_429
# BB#425:                               #   in Loop: Header=BB0_418 Depth=2
	testq	%rax, %rax
	je	.LBB0_430
# BB#426:                               # %.preheader.i.preheader
                                        #   in Loop: Header=BB0_418 Depth=2
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB0_427:                              # %.preheader.i
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_418 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_427
# BB#428:                               #   in Loop: Header=BB0_418 Depth=2
	movq	%rax, (%rcx)
	jmp	.LBB0_430
.LBB0_429:                              #   in Loop: Header=BB0_418 Depth=2
	movq	%rax, %r14
	.p2align	4, 0x90
.LBB0_430:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB0_418 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB0_418
# BB#431:                               # %._crit_edge632
                                        #   in Loop: Header=BB0_89 Depth=1
	testq	%rbx, %rbx
	je	.LBB0_433
	.p2align	4, 0x90
.LBB0_432:                              # %.lr.ph.i400
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB0_432
.LBB0_433:                              # %list_Delete.exit401
                                        #   in Loop: Header=BB0_89 Depth=1
	movl	$cnf_LabelEqual, %esi
	movq	%r14, %rdi
	callq	list_DeleteDuplicates
	movq	%rax, %r14
	movq	stdout(%rip), %rcx
	movl	$.L.str.42, %edi
	movl	$29, %esi
	movl	$1, %edx
	callq	fwrite
	testq	%r14, %r14
	movq	%r14, %rbx
	je	.LBB0_439
	.p2align	4, 0x90
.LBB0_434:                              # %.lr.ph635
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rbp
	movl	$.L.str.43, %esi
	movl	$6, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB0_436
# BB#435:                               #   in Loop: Header=BB0_434 Depth=2
	movl	$.L.str.44, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	printf
.LBB0_436:                              #   in Loop: Header=BB0_434 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_434
# BB#437:                               # %._crit_edge636
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	testq	%r14, %r14
	je	.LBB0_440
	.p2align	4, 0x90
.LBB0_438:                              # %.lr.ph.i395
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB0_438
	jmp	.LBB0_440
.LBB0_439:                              # %._crit_edge636.thread
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	.p2align	4, 0x90
.LBB0_440:                              # %list_Delete.exit396.preheader
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	176(%rsp), %rdi         # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB0_441
	.p2align	4, 0x90
.LBB0_447:                              # %.lr.ph638
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_449 Depth 3
                                        #       Child Loop BB0_451 Depth 3
	movq	8(%rdi), %rcx
	movq	%rcx, %rax
	movabsq	$1908283869694091547, %rdx # imm = 0x1A7B9611A7B9611B
	mulq	%rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rdx, %rax
	shrq	$4, %rax
	imulq	$29, %rax, %rax
	movq	%rcx, %rbx
	subq	%rax, %rbx
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB0_446
	.p2align	4, 0x90
.LBB0_449:                              # %.lr.ph.i387
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_447 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rax), %rsi
	cmpq	%rcx, 8(%rsi)
	je	.LBB0_450
# BB#448:                               #   in Loop: Header=BB0_449 Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB0_449
	jmp	.LBB0_446
	.p2align	4, 0x90
.LBB0_450:                              #   in Loop: Header=BB0_447 Depth=2
	movq	%rdi, %r14
	movq	(%rsi), %rax
	testq	%rax, %rax
	je	.LBB0_452
	.p2align	4, 0x90
.LBB0_451:                              # %.lr.ph.i.i391
                                        #   Parent Loop BB0_89 Depth=1
                                        #     Parent Loop BB0_447 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB0_451
.LBB0_452:                              # %list_Delete.exit.i
                                        #   in Loop: Header=BB0_447 Depth=2
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rsi, (%rax)
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rbp
	movq	(%rbp,%rbx,8), %rdi
	callq	list_PointerDeleteElement
	movq	%rax, (%rbp,%rbx,8)
	movq	%r14, %rdi
.LBB0_446:                              # %hsh_DelItem.exit
                                        #   in Loop: Header=BB0_447 Depth=2
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_447
.LBB0_441:                              # %list_Delete.exit396._crit_edge
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	56(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	movq	(%rsp), %r14            # 8-byte Reload
	je	.LBB0_443
	.p2align	4, 0x90
.LBB0_442:                              # %.lr.ph.i385
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB0_442
.LBB0_443:                              # %list_Delete.exit386
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	(%r14), %rbx
	movq	$0, (%r14)
	movq	%r14, %rdi
	callq	prfs_Clean
	movq	%rbx, (%r14)
	cmpb	$0, 30(%rsp)            # 1-byte Folded Reload
	je	.LBB0_453
# BB#444:                               # %scalar.ph951.preheader
                                        #   in Loop: Header=BB0_89 Depth=1
	movl	$7, %eax
	movq	152(%rsp), %rdx         # 8-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_445:                              # %scalar.ph951
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rdx,%rax,4), %ecx
	movl	%ecx, -28(%r12,%rax,4)
	movl	-24(%rdx,%rax,4), %ecx
	movl	%ecx, -24(%r12,%rax,4)
	movl	-20(%rdx,%rax,4), %ecx
	movl	%ecx, -20(%r12,%rax,4)
	movl	-16(%rdx,%rax,4), %ecx
	movl	%ecx, -16(%r12,%rax,4)
	movl	-12(%rdx,%rax,4), %ecx
	movl	%ecx, -12(%r12,%rax,4)
	movl	-8(%rdx,%rax,4), %ecx
	movl	%ecx, -8(%r12,%rax,4)
	movl	-4(%rdx,%rax,4), %ecx
	movl	%ecx, -4(%r12,%rax,4)
	movl	(%rdx,%rax,4), %ecx
	movl	%ecx, (%r12,%rax,4)
	addq	$8, %rax
	cmpq	$4007, %rax             # imm = 0xFA7
	jne	.LBB0_445
	jmp	.LBB0_455
	.p2align	4, 0x90
.LBB0_453:                              # %vector.body949.preheader
                                        #   in Loop: Header=BB0_89 Depth=1
	movl	$36, %eax
	movq	152(%rsp), %rcx         # 8-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_454:                              # %vector.body949
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-144(%rcx,%rax,4), %xmm0
	movups	-128(%rcx,%rax,4), %xmm1
	movups	%xmm0, -144(%r12,%rax,4)
	movups	%xmm1, -128(%r12,%rax,4)
	movups	-112(%rcx,%rax,4), %xmm0
	movups	-96(%rcx,%rax,4), %xmm1
	movups	%xmm0, -112(%r12,%rax,4)
	movups	%xmm1, -96(%r12,%rax,4)
	movups	-80(%rcx,%rax,4), %xmm0
	movups	-64(%rcx,%rax,4), %xmm1
	movups	%xmm0, -80(%r12,%rax,4)
	movups	%xmm1, -64(%r12,%rax,4)
	movups	-48(%rcx,%rax,4), %xmm0
	movups	-32(%rcx,%rax,4), %xmm1
	movups	%xmm0, -48(%r12,%rax,4)
	movups	%xmm1, -32(%r12,%rax,4)
	movdqu	-16(%rcx,%rax,4), %xmm0
	movups	(%rcx,%rax,4), %xmm1
	movdqu	%xmm0, -16(%r12,%rax,4)
	movups	%xmm1, (%r12,%rax,4)
	addq	$40, %rax
	cmpq	$4036, %rax             # imm = 0xFC4
	jne	.LBB0_454
.LBB0_455:                              # %symbol_TransferPrecedence.exit381
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 108(%rax)
	je	.LBB0_457
# BB#456:                               #   in Loop: Header=BB0_89 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.45, %edi
	movl	$67, %esi
	movl	$1, %edx
	callq	fwrite
	movq	48(%rsp), %r12          # 8-byte Reload
.LBB0_457:                              #   in Loop: Header=BB0_89 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	movl	8(%r15), %eax
	testl	%eax, %eax
	je	.LBB0_459
# BB#458:                               #   in Loop: Header=BB0_89 Depth=1
	cmpl	$0, 28(%r15)
	jne	.LBB0_89
	jmp	.LBB0_459
.LBB0_492:
	movq	(%rsp), %r14            # 8-byte Reload
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
.LBB0_459:                              # %.critedge.preheader
	movq	32(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB0_461
	.p2align	4, 0x90
.LBB0_460:                              # %.critedge
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rbx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	clause_OrientEqualities
	movq	%rbx, %rdi
	callq	clause_Normalize
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	clause_SetMaxLitFlags
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	clause_ComputeWeight
	movl	%eax, 4(%rbx)
	movq	%rbx, %rdi
	callq	clause_UpdateMaxVar
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB0_460
.LBB0_461:                              # %.critedge._crit_edge
	cmpl	$0, 8(%r15)
	movq	136(%rsp), %rbp         # 8-byte Reload
	je	.LBB0_468
# BB#462:
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 36(%rax)
	movq	240(%rsp), %rdi
	je	.LBB0_465
# BB#463:
	testq	%rdi, %rdi
	je	.LBB0_466
	.p2align	4, 0x90
.LBB0_464:                              # %.lr.ph.i370
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rdi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rdi
	jne	.LBB0_464
	jmp	.LBB0_466
.LBB0_465:
	movl	$symbol_Delete, %esi
	callq	list_DeleteWithElement
.LBB0_466:                              # %list_Delete.exit371
	cmpq	$0, 144(%rsp)           # 8-byte Folded Reload
	je	.LBB0_468
# BB#467:
	movq	144(%rsp), %rdi         # 8-byte Reload
	callq	prfs_Delete
.LBB0_468:
	movq	8(%rsp), %rbx           # 8-byte Reload
	cmpl	$0, 128(%rbx)
	je	.LBB0_470
# BB#469:
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movq	%rbx, %rdi
	callq	flag_Print
.LBB0_470:
	cmpl	$0, 36(%rbx)
	je	.LBB0_472
# BB#471:
	movq	160(%rsp), %rdi         # 8-byte Reload
	callq	hsh_Delete
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	hsh_Delete
.LBB0_472:                              # %.preheader
	movq	248(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB0_476
	.p2align	4, 0x90
.LBB0_473:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	string_StringFree
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_473
# BB#474:                               # %._crit_edge
	movq	248(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_476
	.p2align	4, 0x90
.LBB0_475:                              # %.lr.ph.i362
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB0_475
.LBB0_476:                              # %list_Delete.exit363
	testq	%rbp, %rbp
	je	.LBB0_478
	.p2align	4, 0x90
.LBB0_477:                              # %.lr.ph.i357
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB0_477
.LBB0_478:                              # %list_Delete.exit358
	movq	232(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_480
	.p2align	4, 0x90
.LBB0_479:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB0_479
.LBB0_480:                              # %list_Delete.exit
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	cnf_Free
	movq	%r14, %rdi
	callq	prfs_Delete
	movq	32(%rsp), %rdi
	callq	clause_DeleteClauseList
	movq	memory_ARRAY+3072(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	96(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	movq	memory_ARRAY+3072(%rip), %rax
	movq	%rcx, (%rax)
	movl	memory_ALIGN(%rip), %ecx
	movl	$16000, %esi            # imm = 0x3E80
	movl	$16000, %eax            # imm = 0x3E80
	xorl	%edx, %edx
	divl	%ecx
	addl	$16000, %ecx            # imm = 0x3E80
	subl	%edx, %ecx
	testl	%edx, %edx
	cmovel	%esi, %ecx
	movl	memory_OFFSET(%rip), %eax
	movq	152(%rsp), %rdi         # 8-byte Reload
	movq	%rdi, %rdx
	subq	%rax, %rdx
	movq	-16(%rdx), %rsi
	movq	-8(%rdx), %rbx
	testq	%rsi, %rsi
	leaq	8(%rsi), %rsi
	movl	$memory_BIGBLOCKS, %ebp
	cmovneq	%rsi, %rbp
	movq	%rbx, (%rbp)
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB0_482
# BB#481:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rdx)
.LBB0_482:
	addl	memory_MARKSIZE(%rip), %ecx
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rcx), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB0_484
# BB#483:
	leaq	16(%rcx,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB0_484:                              # %symbol_DeletePrecedence.exit
	addq	$-16, %rdi
	callq	free
	callq	cc_Free
	movq	ana_FINITEMONADICPREDICATES(%rip), %rax
	testq	%rax, %rax
	je	.LBB0_486
	.p2align	4, 0x90
.LBB0_485:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB0_485
.LBB0_486:                              # %ana_Free.exit
	callq	sort_Free
	callq	unify_Free
	callq	cont_Free
	callq	fol_Free
	callq	symbol_FreeAllSymbols
	callq	dfg_Free
	callq	opts_Free
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	xorl	%r12d, %r12d
.LBB0_487:
	movl	%r12d, %eax
	addq	$392, %rsp              # imm = 0x188
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_488:
	movl	$.L.str.4, %edi
.LBB0_489:
	callq	puts
	movl	$.L.str.5, %edi
	callq	puts
	callq	opts_PrintSPASSNames
	jmp	.LBB0_487
.LBB0_490:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_Error,@function
misc_Error:                             # @misc_Error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	misc_Error, .Lfunc_end1-misc_Error
	.cfi_endproc

	.p2align	4, 0x90
	.type	flag_SetFlagValue,@function
flag_SetFlagValue:                      # @flag_SetFlagValue
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	%ebp, %edi
	callq	flag_Minimum
	cmpl	%ebx, %eax
	jge	.LBB2_1
# BB#3:
	movl	%ebp, %edi
	callq	flag_Maximum
	cmpl	%ebx, %eax
	jle	.LBB2_4
# BB#5:                                 # %flag_CheckFlagValueInRange.exit
	movl	%ebp, %eax
	movl	%ebx, (%r14,%rax,4)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB2_1:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	%ebp, %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	$.L.str.47, %edi
	jmp	.LBB2_2
.LBB2_4:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	%ebp, %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	$.L.str.48, %edi
.LBB2_2:
	xorl	%eax, %eax
	movl	%ebx, %esi
	movq	%rcx, %rdx
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end2:
	.size	flag_SetFlagValue, .Lfunc_end2-flag_SetFlagValue
	.cfi_endproc

	.p2align	4, 0x90
	.type	cnf_LabelEqual,@function
cnf_LabelEqual:                         # @cnf_LabelEqual
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 16
	callq	strcmp
	xorl	%ecx, %ecx
	testl	%eax, %eax
	sete	%cl
	movl	%ecx, %eax
	popq	%rcx
	retq
.Lfunc_end3:
	.size	cnf_LabelEqual, .Lfunc_end3-cnf_LabelEqual
	.cfi_endproc

	.p2align	4, 0x90
	.type	clause_CompareAbstractLEQ,@function
clause_CompareAbstractLEQ:              # @clause_CompareAbstractLEQ
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 16
	callq	clause_CompareAbstract
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setle	%cl
	movl	%ecx, %eax
	popq	%rcx
	retq
.Lfunc_end4:
	.size	clause_CompareAbstractLEQ, .Lfunc_end4-clause_CompareAbstractLEQ
	.cfi_endproc

	.p2align	4, 0x90
	.type	top_GetPowerfulSplitClause,@function
top_GetPowerfulSplitClause:             # @top_GetPowerfulSplitClause
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 64
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	%rsi, (%rsp)            # 8-byte Spill
	movl	$-1, (%rsi)
	movq	32(%r15), %rbx
	movl	56016(%rbx), %edi
	callq	term_StampOverflow
	testl	%eax, %eax
	je	.LBB5_2
# BB#1:
	movq	%rbx, %rdi
	callq	sharing_ResetAllTermStamps
.LBB5_2:
	incl	term_STAMP(%rip)
	movq	56(%r15), %rbp
	testq	%rbp, %rbp
	je	.LBB5_18
# BB#3:                                 # %.lr.ph23.preheader
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB5_4:                                # %.lr.ph23
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_7 Depth 2
	movq	8(%rbp), %r13
	movq	%r13, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB5_16
# BB#5:                                 #   in Loop: Header=BB5_4 Depth=1
	movq	%r13, %rdi
	callq	clause_IsHornClause
	testl	%eax, %eax
	jne	.LBB5_16
# BB#6:                                 #   in Loop: Header=BB5_4 Depth=1
	movq	%r13, %rdi
	callq	top_GetLiteralsForSplitting
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB5_16
	.p2align	4, 0x90
.LBB5_7:                                #   Parent Loop BB5_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	8(%rbx), %rax
	movq	56(%r13), %rcx
	movq	(%rcx,%rax,8), %rsi
	movq	24(%rsi), %rax
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rax), %ecx
	jne	.LBB5_9
# BB#8:                                 #   in Loop: Header=BB5_7 Depth=2
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB5_9:                                # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB5_7 Depth=2
	movl	term_STAMP(%rip), %ecx
	cmpl	24(%rax), %ecx
	je	.LBB5_15
# BB#10:                                #   in Loop: Header=BB5_7 Depth=2
	movl	%ecx, 24(%rax)
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	prfs_GetNumberOfInstances
	testq	%r12, %r12
	je	.LBB5_14
# BB#11:                                #   in Loop: Header=BB5_7 Depth=2
	cmpl	%r14d, %eax
	ja	.LBB5_14
# BB#12:                                #   in Loop: Header=BB5_7 Depth=2
	jne	.LBB5_15
# BB#13:                                #   in Loop: Header=BB5_7 Depth=2
	movl	68(%r13), %ecx
	addl	64(%r13), %ecx
	addl	72(%r13), %ecx
	movl	68(%r12), %edx
	addl	64(%r12), %edx
	addl	72(%r12), %edx
	cmpl	%edx, %ecx
	jge	.LBB5_15
	.p2align	4, 0x90
.LBB5_14:                               #   in Loop: Header=BB5_7 Depth=2
	movl	8(%rbx), %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	%ecx, (%rdx)
	movq	%r13, %r12
	movl	%eax, %r14d
.LBB5_15:                               #   in Loop: Header=BB5_7 Depth=2
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB5_7
	.p2align	4, 0x90
.LBB5_16:                               # %.loopexit
                                        #   in Loop: Header=BB5_4 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB5_4
# BB#17:                                # %._crit_edge
	testl	%r14d, %r14d
	jne	.LBB5_19
.LBB5_18:                               # %._crit_edge.thread
	movq	(%rsp), %rax            # 8-byte Reload
	movl	$-1, (%rax)
	xorl	%r12d, %r12d
.LBB5_19:
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	top_GetPowerfulSplitClause, .Lfunc_end5-top_GetPowerfulSplitClause
	.cfi_endproc

	.p2align	4, 0x90
	.type	top_SelectMinimalConWeightClause,@function
top_SelectMinimalConWeightClause:       # @top_SelectMinimalConWeightClause
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi59:
	.cfi_def_cfa_offset 80
.Lcfi60:
	.cfi_offset %rbx, -56
.Lcfi61:
	.cfi_offset %r12, -48
.Lcfi62:
	.cfi_offset %r13, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rbp
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	172(%rsi), %ecx
	movl	4(%rbp), %r13d
	testb	$8, 48(%rbp)
	jne	.LBB6_2
# BB#1:
	movl	%ecx, %r15d
	jmp	.LBB6_3
.LBB6_2:
	xorl	%edx, %edx
	movl	%r13d, %eax
	movl	%ecx, %r15d
	divl	%ecx
	movl	%eax, %r13d
.LBB6_3:
	movq	%rbp, %rdi
	callq	clause_NumberOfVarOccs
	movl	%eax, %r12d
	movq	(%rbx), %r14
	testq	%r14, %r14
	je	.LBB6_4
# BB#5:                                 # %.lr.ph.lr.ph
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB6_6:                                # =>This Inner Loop Header: Depth=1
	movq	8(%r14), %rbx
	movl	4(%rbx), %ebp
	testb	$8, 48(%rbx)
	je	.LBB6_8
# BB#7:                                 #   in Loop: Header=BB6_6 Depth=1
	xorl	%edx, %edx
	movl	%ebp, %eax
	divl	%r15d
	movl	%eax, %ebp
.LBB6_8:                                #   in Loop: Header=BB6_6 Depth=1
	cmpl	%r13d, %ebp
	jb	.LBB6_9
# BB#10:                                #   in Loop: Header=BB6_6 Depth=1
	jne	.LBB6_11
# BB#13:                                #   in Loop: Header=BB6_6 Depth=1
	movq	%rbx, %rdi
	callq	clause_NumberOfVarOccs
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	$0, 188(%rcx)
	je	.LBB6_15
# BB#14:                                #   in Loop: Header=BB6_6 Depth=1
	cmpl	%eax, %r12d
	jae	.LBB6_11
	jmp	.LBB6_16
.LBB6_15:                               #   in Loop: Header=BB6_6 Depth=1
	cmpl	%eax, %r12d
	ja	.LBB6_16
	.p2align	4, 0x90
.LBB6_11:                               # %.backedge
                                        #   in Loop: Header=BB6_6 Depth=1
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB6_6
	jmp	.LBB6_12
.LBB6_9:                                #   in Loop: Header=BB6_6 Depth=1
	movq	%rbx, %rdi
	callq	clause_NumberOfVarOccs
	jmp	.LBB6_17
.LBB6_16:                               #   in Loop: Header=BB6_6 Depth=1
	movq	8(%r14), %rbx
	movl	%r13d, %ebp
.LBB6_17:                               # %.outer.backedge
                                        #   in Loop: Header=BB6_6 Depth=1
	movq	(%r14), %r14
	testq	%r14, %r14
	movl	%eax, %r12d
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movl	%ebp, %r13d
	jne	.LBB6_6
	jmp	.LBB6_18
.LBB6_12:
	movq	8(%rsp), %rbx           # 8-byte Reload
	jmp	.LBB6_18
.LBB6_4:
	movq	%rbp, %rbx
.LBB6_18:                               # %.outer._crit_edge
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	top_SelectMinimalConWeightClause, .Lfunc_end6-top_SelectMinimalConWeightClause
	.cfi_endproc

	.p2align	4, 0x90
	.type	top_GetLiteralsForSplitting,@function
top_GetLiteralsForSplitting:            # @top_GetLiteralsForSplitting
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi69:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi70:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi72:
	.cfi_def_cfa_offset 80
.Lcfi73:
	.cfi_offset %rbx, -56
.Lcfi74:
	.cfi_offset %r12, -48
.Lcfi75:
	.cfi_offset %r13, -40
.Lcfi76:
	.cfi_offset %r14, -32
.Lcfi77:
	.cfi_offset %r15, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	callq	clause_IsHornClause
	testl	%eax, %eax
	je	.LBB7_3
.LBB7_1:
	xorl	%r15d, %r15d
.LBB7_2:                                # %list_Delete.exit
	movq	%r15, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_3:
	movl	64(%r13), %ecx
	movl	68(%r13), %edi
	movl	72(%r13), %eax
	cmpl	$0, 52(%r13)
	je	.LBB7_11
# BB#4:
	addl	%ecx, %edi
	addl	%eax, %edi
	shll	$3, %edi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	memory_Malloc
	movq	%rax, %r14
	movl	64(%r13), %ecx
	movl	68(%r13), %eax
	movl	72(%r13), %edx
	leal	(%rcx,%rax), %esi
	addl	%edx, %esi
	decl	%esi
	js	.LBB7_9
# BB#5:                                 # %.lr.ph147
	movq	$-1, %rbx
	.p2align	4, 0x90
.LBB7_6:                                # =>This Inner Loop Header: Depth=1
	movq	56(%r13), %rax
	movq	8(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB7_8
# BB#7:                                 #   in Loop: Header=BB7_6 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB7_8:                                # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB7_6 Depth=1
	callq	term_VariableSymbols
	movq	%rax, 8(%r14,%rbx,8)
	movl	64(%r13), %ecx
	movl	68(%r13), %eax
	movl	72(%r13), %edx
	leal	(%rcx,%rax), %esi
	leal	-1(%rdx,%rsi), %esi
	movslq	%esi, %rdi
	incq	%rbx
	cmpq	%rdi, %rbx
	jl	.LBB7_6
.LBB7_9:                                # %.preheader108
	leal	(%rax,%rcx), %edi
	cmpl	%edi, %esi
	jge	.LBB7_13
# BB#10:
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	jmp	.LBB7_34
.LBB7_11:
	addl	%ecx, %edi
	leal	-1(%rax,%rdi), %eax
	cmpl	%edi, %eax
	jl	.LBB7_1
# BB#48:                                # %.lr.ph.preheader
	movslq	%eax, %rbx
	incq	%rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB7_49:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	decq	%rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbx, 8(%r15)
	movq	%rbp, (%r15)
	movslq	68(%r13), %rax
	movslq	64(%r13), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rbx
	movq	%r15, %rbp
	jg	.LBB7_49
	jmp	.LBB7_2
.LBB7_13:                               # %.lr.ph139.preheader
	movslq	%esi, %rbp
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	movq	%r14, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB7_14:                               # %.lr.ph139
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_17 Depth 2
                                        #     Child Loop BB7_24 Depth 2
	cmpq	$0, (%r14,%rbp,8)
	je	.LBB7_20
# BB#15:                                #   in Loop: Header=BB7_14 Depth=1
	testq	%r12, %r12
	je	.LBB7_21
# BB#16:                                # %.lr.ph.i105.preheader
                                        #   in Loop: Header=BB7_14 Depth=1
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB7_17:                               # %.lr.ph.i105
                                        #   Parent Loop BB7_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbp, 8(%rdx)
	je	.LBB7_32
# BB#18:                                #   in Loop: Header=BB7_17 Depth=2
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB7_17
# BB#19:                                #   in Loop: Header=BB7_14 Depth=1
	movq	%r15, 8(%rsp)           # 8-byte Spill
	jmp	.LBB7_22
	.p2align	4, 0x90
.LBB7_20:                               #   in Loop: Header=BB7_14 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, %r15
	jmp	.LBB7_32
	.p2align	4, 0x90
.LBB7_21:                               #   in Loop: Header=BB7_14 Depth=1
	movq	%r15, 8(%rsp)           # 8-byte Spill
	xorl	%r12d, %r12d
.LBB7_22:                               # %.preheader107
                                        #   in Loop: Header=BB7_14 Depth=1
	addl	%eax, %ecx
	addl	72(%r13), %ecx
	decl	%ecx
	js	.LBB7_30
# BB#23:                                # %.lr.ph128.preheader
                                        #   in Loop: Header=BB7_14 Depth=1
	movl	%ebp, %r15d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB7_24:                               # %.lr.ph128
                                        #   Parent Loop BB7_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$1, %bl
	cmpq	%r14, %r15
	je	.LBB7_27
# BB#25:                                #   in Loop: Header=BB7_24 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbp,8), %rdi
	movq	(%rax,%r14,8), %rsi
	callq	list_HasIntersection
	testl	%eax, %eax
	je	.LBB7_27
# BB#26:                                #   in Loop: Header=BB7_24 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbp, 8(%rbx)
	movq	%r12, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	%r14, 8(%r12)
	movq	%rbx, (%r12)
	xorl	%ebx, %ebx
.LBB7_27:                               #   in Loop: Header=BB7_24 Depth=2
	testb	%bl, %bl
	je	.LBB7_29
# BB#28:                                #   in Loop: Header=BB7_24 Depth=2
	movl	64(%r13), %eax
	movl	72(%r13), %ecx
	addl	68(%r13), %eax
	leal	-1(%rcx,%rax), %eax
	cltq
	cmpq	%rax, %r14
	leaq	1(%r14), %r14
	jl	.LBB7_24
.LBB7_29:                               # %.critedge
                                        #   in Loop: Header=BB7_14 Depth=1
	testb	%bl, %bl
	movq	16(%rsp), %r14          # 8-byte Reload
	je	.LBB7_31
.LBB7_30:                               # %.critedge.thread
                                        #   in Loop: Header=BB7_14 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbp, 8(%r15)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, (%r15)
	jmp	.LBB7_32
.LBB7_31:                               #   in Loop: Header=BB7_14 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	.p2align	4, 0x90
.LBB7_32:                               # %list_PointerMember.exit
                                        #   in Loop: Header=BB7_14 Depth=1
	movl	64(%r13), %ecx
	movl	68(%r13), %eax
	leal	(%rax,%rcx), %edx
	movslq	%edx, %rdx
	cmpq	%rdx, %rbp
	leaq	-1(%rbp), %rbp
	jg	.LBB7_14
# BB#33:                                # %.preheader.loopexit
	movl	72(%r13), %edx
.LBB7_34:                               # %.preheader
	leal	(%rcx,%rax), %esi
	addl	%edx, %esi
	decl	%esi
	js	.LBB7_40
# BB#35:                                # %.lr.ph118.preheader
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB7_36:                               # %.lr.ph118
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_37 Depth 2
	movq	(%r14,%rsi,8), %rdi
	testq	%rdi, %rdi
	je	.LBB7_39
	.p2align	4, 0x90
.LBB7_37:                               # %.lr.ph.i103
                                        #   Parent Loop BB7_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rdi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rdi
	jne	.LBB7_37
# BB#38:                                # %list_Delete.exit104.loopexit
                                        #   in Loop: Header=BB7_36 Depth=1
	movl	64(%r13), %ecx
	movl	68(%r13), %eax
	movl	72(%r13), %edx
.LBB7_39:                               # %list_Delete.exit104
                                        #   in Loop: Header=BB7_36 Depth=1
	leal	(%rcx,%rax), %edi
	leal	-1(%rdx,%rdi), %edi
	movslq	%edi, %rdi
	cmpq	%rdi, %rsi
	leaq	1(%rsi), %rsi
	jl	.LBB7_36
.LBB7_40:                               # %._crit_edge
	addl	%eax, %ecx
	addl	%edx, %ecx
	shll	$3, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB7_42
# BB#41:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%r14, (%rax)
	testq	%r12, %r12
	jne	.LBB7_47
	jmp	.LBB7_2
.LBB7_42:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%r14, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %rdi
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebp
	cmovneq	%rdx, %rbp
	movq	%rdi, (%rbp)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB7_44
# BB#43:
	negq	%rax
	movq	-16(%r14,%rax), %rax
	movq	%rax, (%rcx)
.LBB7_44:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB7_46
# BB#45:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB7_46:
	addq	$-16, %r14
	movq	%r14, %rdi
	callq	free
	testq	%r12, %r12
	je	.LBB7_2
	.p2align	4, 0x90
.LBB7_47:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB7_47
	jmp	.LBB7_2
.Lfunc_end7:
	.size	top_GetLiteralsForSplitting, .Lfunc_end7-top_GetLiteralsForSplitting
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"rf"
	.size	.L.str, 3

	.type	top_RemoveFileOptId,@object # @top_RemoveFileOptId
	.local	top_RemoveFileOptId
	.comm	top_RemoveFileOptId,4,4
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\n\t          %s %s"
	.size	.L.str.1, 18

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"V 2.1"
	.size	.L.str.2, 6

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n\t       Usage: FLOTTER [options] [<input-file>] [<output-file>]\n"
	.size	.L.str.3, 66

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\n\t       Usage: SPASS [options] [<input-file>] \n"
	.size	.L.str.4, 49

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Possible options:\n"
	.size	.L.str.5, 19

	.type	top_InputFile,@object   # @top_InputFile
	.local	top_InputFile
	.comm	top_InputFile,8,8
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"r"
	.size	.L.str.6, 2

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\n No formulae and clauses found in input file!\n"
	.size	.L.str.7, 48

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"axiom%d"
	.size	.L.str.8, 8

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"\nAdded label %s for axiom \n"
	.size	.L.str.9, 28

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"declaration%d"
	.size	.L.str.10, 14

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"\nAdded label %s for declaration \n"
	.size	.L.str.11, 34

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"\nSPASS %s "
	.size	.L.str.12, 11

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"\nSPASS beiseite: "
	.size	.L.str.13, 18

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Ran out of time."
	.size	.L.str.14, 17

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Maximal number of loops exceeded."
	.size	.L.str.15, 34

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Proof found."
	.size	.L.str.16, 13

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Completion found."
	.size	.L.str.17, 18

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"\nProblem: %s "
	.size	.L.str.18, 14

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Read from stdin."
	.size	.L.str.19, 17

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"\nSPASS derived %d clauses,"
	.size	.L.str.20, 27

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	" backtracked %d clauses"
	.size	.L.str.21, 24

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	" and kept %d clauses."
	.size	.L.str.22, 22

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"\nSPASS spent\t"
	.size	.L.str.23, 14

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	" on the problem.\n\t\t"
	.size	.L.str.24, 20

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	" for the input.\n\t\t"
	.size	.L.str.25, 19

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	" for the FLOTTER CNF translation.\n\t\t"
	.size	.L.str.26, 37

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	" for inferences.\n\t\t"
	.size	.L.str.27, 20

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	" for the backtracking.\n\t\t"
	.size	.L.str.28, 26

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	" for the reduction."
	.size	.L.str.29, 20

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"{*SPASS V 2.1 *}"
	.size	.L.str.30, 17

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"SPASS"
	.size	.L.str.31, 6

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	".model"
	.size	.L.str.32, 7

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	".clauses"
	.size	.L.str.33, 9

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"w"
	.size	.L.str.34, 2

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"{*Completion_by_SPASS*}"
	.size	.L.str.35, 24

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"satisfiable"
	.size	.L.str.36, 12

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"{*Clauses_generated_by_SPASS*}"
	.size	.L.str.37, 31

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"unknown"
	.size	.L.str.38, 8

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"\nCompletion printed to: %s\n"
	.size	.L.str.39, 28

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"\nClauses printed to: %s\n"
	.size	.L.str.40, 25

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"\n\n The saturated set of worked-off clauses is :"
	.size	.L.str.41, 48

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"\nFormulae used in the proof :"
	.size	.L.str.42, 30

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"_SORT_"
	.size	.L.str.43, 7

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	" %s"
	.size	.L.str.44, 4

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"\n--------------------------SPASS-STOP------------------------------"
	.size	.L.str.45, 68

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"FLOTTER"
	.size	.L.str.46, 8

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"\n Error: Flag value %d is too small for flag %s.\n"
	.size	.L.str.47, 50

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"\n Error: Flag value %d is too large for flag %s.\n"
	.size	.L.str.48, 50

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"\n\tCNF generated by FLOTTER V 2.1 *}"
	.size	.L.str.49, 36

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"\nFLOTTER needed\t"
	.size	.L.str.50, 17

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	" for the input."
	.size	.L.str.51, 16

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"\t\t"
	.size	.L.str.52, 3

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	" for the  CNF translation."
	.size	.L.str.53, 27

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"--------------------------SPASS-START-----------------------------"
	.size	.L.str.55, 67

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"Input Problem:"
	.size	.L.str.56, 15

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"\nProcessed Problem:"
	.size	.L.str.57, 20

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"\nWorked Off Clauses:"
	.size	.L.str.58, 21

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"\nUsable Clauses:"
	.size	.L.str.59, 17

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"\n\t Split Backtracking level %d:"
	.size	.L.str.60, 32

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"\nDerived: "
	.size	.L.str.61, 11

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"\nEmptyClause: "
	.size	.L.str.62, 15

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"\n\n\t ---- New Clause %s Bound: %2d ----\n"
	.size	.L.str.63, 40

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"Term Depth"
	.size	.L.str.64, 11

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"Weight"
	.size	.L.str.65, 7

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"\n\tGiven clause: "
	.size	.L.str.66, 17


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
