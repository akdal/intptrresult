	.text
	.file	"libclamav_upx.bc"
	.globl	upx_inflate2b
	.p2align	4, 0x90
	.type	upx_inflate2b,@function
upx_inflate2b:                          # @upx_inflate2b
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 160
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, %r12d
	movl	%r8d, %r10d
	movl	160(%rsp), %r8d
	movaps	.Lupx_inflate2b.magic(%rip), %xmm0
	movaps	%xmm0, 80(%rsp)
	movl	%esi, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	(%rdi,%rax), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$-1, 4(%rsp)            # 4-byte Folded Spill
	xorl	%r13d, %r13d
	xorl	%r9d, %r9d
	xorl	%r15d, %r15d
	cmpl	$4, %esi
	jae	.LBB0_8
	jmp	.LBB0_2
.LBB0_85:                               # %._crit_edge
	addl	%r15d, %r14d
	movl	%r14d, %r15d
	movq	32(%rsp), %r12          # 8-byte Reload
	movl	28(%rsp), %r10d         # 4-byte Reload
	movl	160(%rsp), %r8d
	cmpl	$4, %esi
	jae	.LBB0_8
	.p2align	4, 0x90
.LBB0_2:                                # %.split.preheader
	movl	$-1, %eax
	testl	$2147483647, %r13d      # imm = 0x7FFFFFFF
	je	.LBB0_87
# BB#3:                                 # %doubleebx.exit.lr.ph
	movl	%r15d, %r15d
	movl	%r9d, %r9d
	.p2align	4, 0x90
.LBB0_4:                                # %doubleebx.exit
                                        # =>This Inner Loop Header: Depth=1
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	testl	%r13d, %r13d
	leal	(%r13,%r13), %r13d
	jns	.LBB0_15
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=1
	cmpq	72(%rsp), %r9           # 8-byte Folded Reload
	jae	.LBB0_87
# BB#6:                                 #   in Loop: Header=BB0_4 Depth=1
	movl	(%rcx), %ebp
	cmpq	%rbp, %r15
	jae	.LBB0_87
# BB#7:                                 # %.split
                                        #   in Loop: Header=BB0_4 Depth=1
	movzbl	(%rdi,%r9), %ebx
	movb	%bl, (%rdx,%r15)
	incq	%r9
	incq	%r15
	testl	$2147483646, %r13d      # imm = 0x7FFFFFFE
	jne	.LBB0_4
	jmp	.LBB0_87
	.p2align	4, 0x90
.LBB0_22:                               #   in Loop: Header=BB0_8 Depth=1
	movl	%r9d, %eax
	incl	%r9d
	movzbl	(%rdi,%rax), %eax
	movl	%r15d, %ebp
	incl	%r15d
	movb	%al, (%rdx,%rbp)
	movl	%ebx, %r13d
.LBB0_8:                                # %.split.us
                                        # =>This Inner Loop Header: Depth=1
	testl	$2147483647, %r13d      # imm = 0x7FFFFFFF
	je	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_8 Depth=1
	leal	(%r13,%r13), %ebx
	testl	%r13d, %r13d
	js	.LBB0_20
	jmp	.LBB0_14
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_8 Depth=1
	movl	%r9d, %ebp
	leaq	4(%rdi,%rbp), %rbx
	movl	$-1, %eax
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	ja	.LBB0_87
# BB#11:                                #   in Loop: Header=BB0_8 Depth=1
	cmpq	%rdi, %rbx
	jbe	.LBB0_87
# BB#12:                                #   in Loop: Header=BB0_8 Depth=1
	addq	%rdi, %rbp
	movl	(%rbp), %r13d
	leal	1(%r13,%r13), %ebx
	addl	$4, %r9d
	testl	%r13d, %r13d
	jns	.LBB0_14
.LBB0_20:                               #   in Loop: Header=BB0_8 Depth=1
	movl	$-1, %eax
	cmpl	%esi, %r9d
	jae	.LBB0_87
# BB#21:                                #   in Loop: Header=BB0_8 Depth=1
	cmpl	(%rcx), %r15d
	jb	.LBB0_22
	jmp	.LBB0_87
.LBB0_14:
	movl	%ebx, %r13d
.LBB0_15:                               # %.preheader198
	movl	$1, %r11d
	cmpl	$3, %esi
	jbe	.LBB0_16
	.p2align	4, 0x90
.LBB0_23:                               # %.preheader198.split.us
                                        # =>This Inner Loop Header: Depth=1
	testl	$2147483647, %r13d      # imm = 0x7FFFFFFF
	je	.LBB0_25
# BB#24:                                #   in Loop: Header=BB0_23 Depth=1
	leal	(%r13,%r13), %eax
	testl	$2147483647, %eax       # imm = 0x7FFFFFFF
	jne	.LBB0_29
	jmp	.LBB0_30
	.p2align	4, 0x90
.LBB0_25:                               #   in Loop: Header=BB0_23 Depth=1
	movl	%r9d, %ebp
	leaq	4(%rdi,%rbp), %rbx
	movl	$-1, %eax
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	ja	.LBB0_87
# BB#26:                                #   in Loop: Header=BB0_23 Depth=1
	cmpq	%rdi, %rbx
	jbe	.LBB0_87
# BB#27:                                #   in Loop: Header=BB0_23 Depth=1
	addq	%rdi, %rbp
	movl	(%rbp), %r13d
	leal	1(%r13,%r13), %eax
	addl	$4, %r9d
	testl	$2147483647, %eax       # imm = 0x7FFFFFFF
	je	.LBB0_30
.LBB0_29:                               #   in Loop: Header=BB0_23 Depth=1
	leal	(%rax,%rax), %r14d
	jmp	.LBB0_33
	.p2align	4, 0x90
.LBB0_30:                               #   in Loop: Header=BB0_23 Depth=1
	movl	%r9d, %ebp
	leaq	4(%rdi,%rbp), %rbx
	movl	$-1, %eax
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	ja	.LBB0_87
# BB#31:                                #   in Loop: Header=BB0_23 Depth=1
	cmpq	%rdi, %rbx
	jbe	.LBB0_87
# BB#32:                                #   in Loop: Header=BB0_23 Depth=1
	addq	%rdi, %rbp
	movl	(%rbp), %eax
	leal	1(%rax,%rax), %r14d
	addl	$4, %r9d
.LBB0_33:                               # %doubleebx.exit148.us
                                        #   in Loop: Header=BB0_23 Depth=1
	shldl	$1, %r13d, %r11d
	testl	%eax, %eax
	movl	%r14d, %r13d
	jns	.LBB0_23
	jmp	.LBB0_34
	.p2align	4, 0x90
.LBB0_16:                               # %.preheader198.split.split
                                        # =>This Inner Loop Header: Depth=1
	movl	$-1, %eax
	testl	$2147483647, %r13d      # imm = 0x7FFFFFFF
	je	.LBB0_87
# BB#17:                                # %doubleebx.exit128
                                        #   in Loop: Header=BB0_16 Depth=1
	leal	(%r13,%r13), %ebp
	testl	$2147483646, %ebp       # imm = 0x7FFFFFFE
	je	.LBB0_87
# BB#18:                                # %doubleebx.exit148
                                        #   in Loop: Header=BB0_16 Depth=1
	shldl	$1, %r13d, %r11d
	shll	$2, %r13d
	testl	%ebp, %ebp
	jns	.LBB0_16
# BB#19:
	movl	%r13d, %r14d
.LBB0_34:                               # %.us-lcssa.us
	addl	$-3, %r11d
	js	.LBB0_35
# BB#36:
	cmpl	%esi, %r9d
	jae	.LBB0_37
# BB#38:
	shll	$8, %r11d
	movl	%r9d, %eax
	movzbl	(%rdi,%rax), %eax
	orl	%r11d, %eax
	cmpl	$-1, %eax
	je	.LBB0_86
# BB#39:
	incl	%r9d
	notl	%eax
	jmp	.LBB0_40
.LBB0_35:
	movl	4(%rsp), %eax           # 4-byte Reload
.LBB0_40:
	movl	%eax, 4(%rsp)           # 4-byte Spill
	testl	$2147483647, %r14d      # imm = 0x7FFFFFFF
	je	.LBB0_42
# BB#41:
	leal	(%r14,%r14), %eax
	testl	$2147483647, %eax       # imm = 0x7FFFFFFF
	jne	.LBB0_47
	jmp	.LBB0_48
.LBB0_42:
	movl	$-1, %eax
	cmpl	$4, %esi
	jb	.LBB0_87
# BB#43:
	movl	%r9d, %ebp
	leaq	4(%rdi,%rbp), %rbx
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	ja	.LBB0_87
# BB#44:
	cmpq	%rdi, %rbx
	jbe	.LBB0_87
# BB#45:
	addq	%rdi, %rbp
	movl	(%rbp), %r14d
	leal	1(%r14,%r14), %eax
	addl	$4, %r9d
	testl	$2147483647, %eax       # imm = 0x7FFFFFFF
	je	.LBB0_48
.LBB0_47:
	leal	(%rax,%rax), %r13d
	jmp	.LBB0_52
.LBB0_48:
	movl	$-1, %eax
	cmpl	$4, %esi
	jb	.LBB0_87
# BB#49:
	movl	%r9d, %ebx
	leaq	4(%rdi,%rbx), %rbp
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	ja	.LBB0_87
# BB#50:
	cmpq	%rdi, %rbp
	jbe	.LBB0_87
# BB#51:
	addq	%rdi, %rbx
	movl	(%rbx), %eax
	leal	1(%rax,%rax), %r13d
	addl	$4, %r9d
.LBB0_52:                               # %doubleebx.exit140
	shrl	$31, %r14d
	shrl	$31, %eax
	addl	%r14d, %r14d
	orl	%eax, %r14d
	jne	.LBB0_70
# BB#53:                                # %.preheader197
	movl	$1, %r14d
	cmpl	$3, %esi
	jbe	.LBB0_54
	.p2align	4, 0x90
.LBB0_58:                               # %.preheader197.split.us
                                        # =>This Inner Loop Header: Depth=1
	testl	$2147483647, %r13d      # imm = 0x7FFFFFFF
	je	.LBB0_60
# BB#59:                                #   in Loop: Header=BB0_58 Depth=1
	leal	(%r13,%r13), %eax
	testl	$2147483647, %eax       # imm = 0x7FFFFFFF
	jne	.LBB0_64
	jmp	.LBB0_65
.LBB0_60:                               #   in Loop: Header=BB0_58 Depth=1
	movl	%r9d, %ebx
	leaq	4(%rdi,%rbx), %rbp
	movl	$-1, %eax
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	ja	.LBB0_87
# BB#61:                                #   in Loop: Header=BB0_58 Depth=1
	cmpq	%rdi, %rbp
	jbe	.LBB0_87
# BB#62:                                #   in Loop: Header=BB0_58 Depth=1
	addq	%rdi, %rbx
	movl	(%rbx), %r13d
	leal	1(%r13,%r13), %eax
	addl	$4, %r9d
	testl	$2147483647, %eax       # imm = 0x7FFFFFFF
	je	.LBB0_65
.LBB0_64:                               #   in Loop: Header=BB0_58 Depth=1
	leal	(%rax,%rax), %ebx
	jmp	.LBB0_68
.LBB0_65:                               #   in Loop: Header=BB0_58 Depth=1
	movl	%r9d, %ebx
	leaq	4(%rdi,%rbx), %rbp
	movl	$-1, %eax
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	ja	.LBB0_87
# BB#66:                                #   in Loop: Header=BB0_58 Depth=1
	cmpq	%rdi, %rbp
	jbe	.LBB0_87
# BB#67:                                #   in Loop: Header=BB0_58 Depth=1
	addq	%rdi, %rbx
	movl	(%rbx), %eax
	leal	1(%rax,%rax), %ebx
	addl	$4, %r9d
.LBB0_68:                               # %doubleebx.exit132.us
                                        #   in Loop: Header=BB0_58 Depth=1
	shldl	$1, %r13d, %r14d
	testl	%eax, %eax
	movl	%ebx, %r13d
	jns	.LBB0_58
	jmp	.LBB0_69
	.p2align	4, 0x90
.LBB0_54:                               # %.preheader197.split.split
                                        # =>This Inner Loop Header: Depth=1
	movl	$-1, %eax
	testl	$2147483647, %r13d      # imm = 0x7FFFFFFF
	je	.LBB0_87
# BB#55:                                # %doubleebx.exit136
                                        #   in Loop: Header=BB0_54 Depth=1
	leal	(%r13,%r13), %ebx
	testl	$2147483646, %ebx       # imm = 0x7FFFFFFE
	je	.LBB0_87
# BB#56:                                # %doubleebx.exit132
                                        #   in Loop: Header=BB0_54 Depth=1
	shldl	$1, %r13d, %r14d
	shll	$2, %r13d
	testl	%ebx, %ebx
	jns	.LBB0_54
# BB#57:
	movl	%r13d, %ebx
.LBB0_69:                               # %.us-lcssa221.us
	addl	$2, %r14d
	movl	%ebx, %r13d
.LBB0_70:
	movl	4(%rsp), %ebx           # 4-byte Reload
	cmpl	$-3328, %ebx            # imm = 0xF300
	adcl	$0, %r14d
	movl	(%rcx), %ebp
	movl	$-1, %eax
	cmpl	%ebp, %r14d
	jae	.LBB0_87
# BB#71:
	movl	%r10d, 28(%rsp)         # 4-byte Spill
	movl	%r15d, %r10d
	leaq	(%rdx,%r10), %r11
	movslq	%ebx, %rbx
	addq	%r11, %rbx
	cmpq	%rdx, %rbx
	jb	.LBB0_87
# BB#72:
	incl	%r14d
	movl	%r14d, %r8d
	addq	%r8, %rbx
	addq	%rdx, %rbp
	cmpq	%rbp, %rbx
	ja	.LBB0_87
# BB#73:
	cmpq	%rdx, %rbx
	jbe	.LBB0_87
# BB#74:
	addq	%r8, %r11
	cmpq	%rbp, %r11
	ja	.LBB0_87
# BB#75:
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jns	.LBB0_87
# BB#76:
	cmpq	%rdx, %r11
	jbe	.LBB0_87
# BB#77:                                # %.preheader
	movq	%r12, 32(%rsp)          # 8-byte Spill
	testl	%r14d, %r14d
	movq	%r8, %rax
	je	.LBB0_85
# BB#78:                                # %.lr.ph
	leaq	-1(%rax), %rbp
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movq	%rax, %r11
	andq	$3, %r11
	je	.LBB0_79
# BB#80:                                # %.prol.preheader
	movl	4(%rsp), %r12d          # 4-byte Reload
	addq	%r10, %r12
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_81:                               # =>This Inner Loop Header: Depth=1
	leal	(%r12,%rax), %ebp
	movzbl	(%rdx,%rbp), %ebx
	leal	(%r10,%rax), %ebp
	movb	%bl, (%rdx,%rbp)
	incq	%rax
	cmpq	%rax, %r11
	jne	.LBB0_81
	jmp	.LBB0_82
.LBB0_79:
	xorl	%eax, %eax
.LBB0_82:                               # %.prol.loopexit
	cmpq	$3, 16(%rsp)            # 8-byte Folded Reload
	movq	%r8, 16(%rsp)           # 8-byte Spill
	jb	.LBB0_85
# BB#83:                                # %.lr.ph.new
	subq	%rax, 16(%rsp)          # 8-byte Folded Spill
	leal	3(%r15,%rax), %r8d
	movl	4(%rsp), %ebx           # 4-byte Reload
	leal	2(%r15,%rax), %r10d
	movl	%eax, %r12d
	addl	%r15d, %r12d
	leal	1(%r15,%rax), %eax
	movq	%r8, 64(%rsp)           # 8-byte Spill
	leaq	(%rbx,%r8), %rbp
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	%r10, 56(%rsp)          # 8-byte Spill
	leaq	(%rbx,%r10), %rbp
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	leaq	(%rbx,%r12), %r11
	movq	%rax, %r10
	leaq	(%rbx,%rax), %r8
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_84:                               # =>This Inner Loop Header: Depth=1
	leal	(%r11,%rbx), %ebp
	movzbl	(%rdx,%rbp), %eax
	leal	(%r12,%rbx), %ebp
	movb	%al, (%rdx,%rbp)
	leal	(%r8,%rbx), %eax
	movzbl	(%rdx,%rax), %eax
	leal	(%r10,%rbx), %ebp
	movb	%al, (%rdx,%rbp)
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%rbx), %eax
	movzbl	(%rdx,%rax), %eax
	movq	56(%rsp), %rbp          # 8-byte Reload
	leal	(%rbp,%rbx), %ebp
	movb	%al, (%rdx,%rbp)
	movq	48(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%rbx), %eax
	movzbl	(%rdx,%rax), %eax
	movq	64(%rsp), %rbp          # 8-byte Reload
	leal	(%rbp,%rbx), %ebp
	movb	%al, (%rdx,%rbp)
	addq	$4, %rbx
	cmpq	%rbx, 16(%rsp)          # 8-byte Folded Reload
	jne	.LBB0_84
	jmp	.LBB0_85
.LBB0_37:
	movl	$-1, %eax
	jmp	.LBB0_87
.LBB0_86:
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	leaq	88(%rsp), %rax
	movl	%r10d, %r9d
	pushq	%r15
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	pefromupx
	addq	$32, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -32
.LBB0_87:                               # %doubleebx.exit.thread
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	upx_inflate2b, .Lfunc_end0-upx_inflate2b
	.cfi_endproc

	.p2align	4, 0x90
	.type	pefromupx,@function
pefromupx:                              # @pefromupx
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 112
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
	movq	%rcx, %r13
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %rbx
	xorl	%r12d, %r12d
	testq	%rbx, %rbx
	je	.LBB1_84
# BB#1:
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	testq	%rdx, %rdx
	je	.LBB1_84
# BB#2:                                 # %.preheader294
	movq	120(%rsp), %rax
	movl	112(%rsp), %ebp
	movl	(%rax), %r14d
	movl	%r8d, %r15d
	subl	%ebp, %r15d
	testl	%r14d, %r14d
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	%r9, 16(%rsp)           # 8-byte Spill
	je	.LBB1_8
# BB#3:                                 # %.lr.ph363
	leal	-5(%rsi), %ecx
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB1_4:                                # =>This Inner Loop Header: Depth=1
	leal	(%r14,%r15), %edx
	cmpl	%ecx, %edx
	ja	.LBB1_7
# BB#5:                                 #   in Loop: Header=BB1_4 Depth=1
	leal	-2(%rdx), %edi
	cmpb	$-115, (%rbx,%rdi)
	jne	.LBB1_7
# BB#6:                                 #   in Loop: Header=BB1_4 Depth=1
	decl	%edx
	cmpb	$-66, (%rbx,%rdx)
	je	.LBB1_71
.LBB1_7:                                # %.backedge296
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	%r12d, %edx
	incl	%r12d
	movl	(%rax,%rdx,4), %r14d
	testl	%r14d, %r14d
	jne	.LBB1_4
	jmp	.LBB1_9
.LBB1_8:
	movl	$1, %r12d
.LBB1_9:                                # %._crit_edge364
	subl	$-128, %r15d
	leal	-8(%rsi), %eax
	cmpl	%eax, %r15d
	jae	.LBB1_16
# BB#10:
	movl	%r8d, 44(%rsp)          # 4-byte Spill
	movl	%r15d, %eax
	addq	%rbx, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%rsi, %rbp
	callq	cli_dbgmsg
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movl	%ebp, %eax
	movl	$4294967288, %r13d      # imm = 0xFFFFFFF8
	addq	%rax, %r13
	movl	%r13d, %esi
	subl	%r15d, %esi
	movl	$.L.str.1, %edx
	movl	$2, %ecx
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	cli_memstr
	testq	%rax, %rax
	je	.LBB1_15
	.p2align	4, 0x90
.LBB1_11:                               # %.lr.ph362
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$-117, 6(%rax)
	jne	.LBB1_13
# BB#12:                                #   in Loop: Header=BB1_11 Depth=1
	cmpb	$7, 7(%rax)
	je	.LBB1_70
.LBB1_13:                               #   in Loop: Header=BB1_11 Depth=1
	incq	%rax
	movl	%eax, %ecx
	subl	%ebx, %ecx
	movl	%r13d, %esi
	subl	%ecx, %esi
	movl	$.L.str.1, %edx
	movl	$2, %ecx
	movq	%rax, %rdi
	callq	cli_memstr
	testq	%rax, %rax
	jne	.LBB1_11
# BB#14:
	xorl	%r14d, %r14d
.LBB1_15:
	xorl	%r10d, %r10d
	xorl	%r15d, %r15d
                                        # implicit-def: %RBP
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	jmp	.LBB1_19
.LBB1_16:
	xorl	%r14d, %r14d
.LBB1_17:
	xorl	%r10d, %r10d
.LBB1_18:
	xorl	%r15d, %r15d
                                        # implicit-def: %RBP
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB1_19:                               # %.thread
	movl	128(%rsp), %ecx
	cmpl	$289, %ecx              # imm = 0x121
	jb	.LBB1_43
.LBB1_20:                               # %.thread
	testq	%r15, %r15
	jne	.LBB1_43
# BB#21:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	128(%rsp), %ecx
	leal	-288(%rcx), %edx
	leaq	(%rbx,%rdx), %r15
	testl	%edx, %edx
	je	.LBB1_25
# BB#22:                                # %.lr.ph329
	movl	(%r13), %eax
	cmpq	$248, %rax
	jae	.LBB1_26
# BB#23:                                # %.lr.ph329.split.us.preheader
	xorl	%ebp, %ebp
	movq	16(%rsp), %r9           # 8-byte Reload
	.p2align	4, 0x90
.LBB1_24:                               # %.lr.ph329.split.us
                                        # =>This Inner Loop Header: Depth=1
	decq	%r15
	cmpq	%rbx, %r15
	ja	.LBB1_24
	jmp	.LBB1_42
.LBB1_25:
	movq	16(%rsp), %r9           # 8-byte Reload
	jmp	.LBB1_42
.LBB1_26:                               # %.lr.ph329.split.split.preheader
	movq	%rax, %rcx
	addq	%rbx, %rcx
	leaq	248(%rbx,%rdx), %r15
	movq	16(%rsp), %r9           # 8-byte Reload
	.p2align	4, 0x90
.LBB1_27:                               # %.lr.ph329.split.split
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rbp
	leaq	-248(%rbp), %r15
	cmpq	%rbx, %r15
	jb	.LBB1_39
# BB#28:                                #   in Loop: Header=BB1_27 Depth=1
	cmpq	%rbx, %rbp
	jbe	.LBB1_39
# BB#29:                                #   in Loop: Header=BB1_27 Depth=1
	cmpq	%rcx, %rbp
	ja	.LBB1_39
# BB#30:                                #   in Loop: Header=BB1_27 Depth=1
	cmpl	$17744, -248(%rbp)      # imm = 0x4550
	jne	.LBB1_39
# BB#31:                                #   in Loop: Header=BB1_27 Depth=1
	movl	-192(%rbp), %r14d
	testl	%r14d, %r14d
	je	.LBB1_37
# BB#32:                                #   in Loop: Header=BB1_27 Depth=1
	movzbl	-242(%rbp), %edx
	movzbl	-241(%rbp), %r12d
	shll	$8, %r12d
	orl	%edx, %r12d
	je	.LBB1_38
# BB#33:                                #   in Loop: Header=BB1_27 Depth=1
	cmpq	%rbx, %rbp
	jb	.LBB1_39
# BB#34:                                #   in Loop: Header=BB1_27 Depth=1
	leal	(,%r12,8), %edx
	leal	(%rdx,%rdx,4), %edx
	cmpl	%eax, %edx
	ja	.LBB1_39
# BB#35:                                #   in Loop: Header=BB1_27 Depth=1
	movl	%edx, %edx
	addq	%rbp, %rdx
	cmpq	%rcx, %rdx
	ja	.LBB1_39
# BB#36:                                #   in Loop: Header=BB1_27 Depth=1
	cmpq	%rbx, %rdx
	jbe	.LBB1_39
	jmp	.LBB1_41
.LBB1_37:                               #   in Loop: Header=BB1_27 Depth=1
	xorl	%r14d, %r14d
	jmp	.LBB1_39
.LBB1_38:                               #   in Loop: Header=BB1_27 Depth=1
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB1_39:                               #   in Loop: Header=BB1_27 Depth=1
	leaq	-1(%rbp), %r15
	addq	$-249, %rbp
	cmpq	%rbx, %rbp
	ja	.LBB1_27
# BB#40:                                # %checkpe.exit254.loopexit444split
	addq	$-248, %r15
	xorl	%ebp, %ebp
.LBB1_41:                               # %.checkpe.exit254.loopexit444_crit_edge
	movl	128(%rsp), %ecx
.LBB1_42:                               # %checkpe.exit254
	movl	%r15d, %r10d
	subl	%ebx, %r10d
	je	.LBB1_54
.LBB1_43:
	testq	%r15, %r15
	je	.LBB1_54
# BB#44:
	leal	(,%r12,8), %r11d
	testl	%r14d, %r14d
	leal	456(%r11,%r11,4), %ecx
	je	.LBB1_46
# BB#45:
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%r14d
	cmpl	$1, %edx
	movl	%eax, %ecx
	sbbl	$-1, %ecx
	imull	%r14d, %ecx
.LBB1_46:                               # %.preheader
	testl	%r12d, %r12d
	je	.LBB1_56
# BB#47:                                # %.lr.ph323
	leal	(%r10,%r9), %r8d
	testl	%r14d, %r14d
	je	.LBB1_61
# BB#48:                                # %.lr.ph323.split.preheader
	addq	$8, %rbp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_49:                               # %.lr.ph323.split
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %eax
	movl	4(%rbp), %edi
	xorl	%edx, %edx
	divl	%r14d
	movl	%eax, %ebx
	cmpl	$1, %edx
	sbbl	$-1, %ebx
	imull	%r14d, %ebx
	xorl	%edx, %edx
	movl	%edi, %eax
	divl	%r14d
	leal	-1(%rbx), %eax
	cmpl	%r10d, %eax
	jae	.LBB1_67
# BB#50:                                # %.lr.ph323.split
                                        #   in Loop: Header=BB1_49 Depth=1
	subl	%edx, %edi
	cmpl	%r9d, %edi
	jb	.LBB1_67
# BB#51:                                #   in Loop: Header=BB1_49 Depth=1
	leal	(%rdi,%rbx), %eax
	cmpl	%r8d, %eax
	ja	.LBB1_67
# BB#52:                                #   in Loop: Header=BB1_49 Depth=1
	cmpl	%r9d, %eax
	jbe	.LBB1_67
# BB#53:                                #   in Loop: Header=BB1_49 Depth=1
	movl	%ebx, (%rbp)
	movl	%edi, 4(%rbp)
	movl	%ebx, 8(%rbp)
	movl	%ecx, 12(%rbp)
	addl	%ecx, %ebx
	incl	%esi
	addq	$40, %rbp
	cmpl	%r12d, %esi
	movl	%ebx, %ecx
	jb	.LBB1_49
	jmp	.LBB1_57
.LBB1_54:                               # %.thread283
	xorl	%ebx, %ebx
	testw	$4095, %cx              # imm = 0xFFF
	setne	%bl
	shll	$12, %ebx
	addl	%ecx, %ebx
	andl	$-4096, %ebx            # imm = 0xF000
	xorl	%r12d, %r12d
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movq	%rcx, %r13
	callq	cli_dbgmsg
	leal	512(%rbx), %r14d
	movl	$1, %esi
	movq	%r14, %rdi
	callq	cli_calloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB1_69
# BB#55:
	movl	$.L.str.6, %esi
	movl	$208, %edx
	movq	%r15, %rdi
	callq	memcpy
	leaq	208(%r15), %rdi
	movl	$.L.str.7, %esi
	movl	$288, %edx              # imm = 0x120
	callq	memcpy
	movq	%r15, %rdi
	addq	$512, %rdi              # imm = 0x200
	movl	%r13d, %edx
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rsi
	callq	memcpy
	addl	$512, %r13d             # imm = 0x200
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	memcpy
	movq	%r15, %rdi
	callq	free
	leal	4096(%rbx), %eax
	movl	%eax, 288(%rbp)
	movl	%ebx, 464(%rbp)
	movl	%ebx, 472(%rbp)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%r14d, (%rax)
	movl	$.L.str.8, %edi
	jmp	.LBB1_83
.LBB1_56:
	movl	%ecx, %ebx
.LBB1_57:                               # %._crit_edge324
	movq	%r11, %r13
	movl	$1296124995, 8(%r15)    # imm = 0x4D414C43
	movl	%r14d, 60(%r15)
	movl	%ebx, %edi
	movl	$1, %esi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	callq	cli_calloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB1_68
# BB#58:
	leal	(%r13,%r13,4), %ebp
	movl	$.L.str.6, %esi
	movl	$208, %edx
	movq	%r14, %rdi
	callq	memcpy
	leaq	208(%r14), %rdi
	addl	$248, %ebp
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	testl	%r12d, %r12d
	je	.LBB1_80
# BB#59:                                # %.lr.ph
	movl	16(%rsp), %ebp          # 4-byte Reload
	negq	%rbp
	testb	$1, %r12b
	jne	.LBB1_76
# BB#60:
	addq	$248, %r15
	xorl	%eax, %eax
	jmp	.LBB1_77
.LBB1_61:                               # %.lr.ph323.split.us.preheader
	addq	$20, %rbp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_62:                               # %.lr.ph323.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rbp), %ebx
	leal	-1(%rbx), %eax
	cmpl	%r10d, %eax
	jae	.LBB1_67
# BB#63:                                # %.lr.ph323.split.us
                                        #   in Loop: Header=BB1_62 Depth=1
	movl	-8(%rbp), %eax
	cmpl	%r9d, %eax
	jb	.LBB1_67
# BB#64:                                #   in Loop: Header=BB1_62 Depth=1
	addl	%ebx, %eax
	cmpl	%r8d, %eax
	ja	.LBB1_67
# BB#65:                                #   in Loop: Header=BB1_62 Depth=1
	cmpl	%r9d, %eax
	jbe	.LBB1_67
# BB#66:                                #   in Loop: Header=BB1_62 Depth=1
	movl	%ebx, -4(%rbp)
	movl	%ecx, (%rbp)
	addl	%ecx, %ebx
	incl	%esi
	addq	$40, %rbp
	cmpl	%r12d, %esi
	movl	%ebx, %ecx
	jb	.LBB1_62
	jmp	.LBB1_57
.LBB1_67:                               # %.us-lcssa.us
	xorl	%r12d, %r12d
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB1_84
.LBB1_68:
	xorl	%r12d, %r12d
.LBB1_69:
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB1_84
.LBB1_70:
	subl	%ebx, %eax
	movl	$2, %r14d
	movl	44(%rsp), %r8d          # 4-byte Reload
	subl	%r8d, %r14d
	movl	112(%rsp), %ebp
	addl	%ebp, %r14d
	addl	%eax, %r14d
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
.LBB1_71:                               # %.loopexit293
	xorl	%r10d, %r10d
	cmpl	$4, %esi
	movl	128(%rsp), %ecx
	jb	.LBB1_75
# BB#72:                                # %.loopexit293
	testl	%r14d, %r14d
	je	.LBB1_75
# BB#73:
	movl	%r8d, %ecx
	addq	%rbx, %rcx
	movl	%ebp, %eax
	subq	%rax, %rcx
	movl	%r14d, %eax
	addq	%rcx, %rax
	cmpq	%rbx, %rax
	jb	.LBB1_17
# BB#85:
	leaq	4(%rax), %rcx
	movl	%esi, %edx
	addq	%rbx, %rdx
	xorl	%r10d, %r10d
	cmpq	%rdx, %rcx
	ja	.LBB1_18
# BB#86:
	cmpq	%rbx, %rcx
	jbe	.LBB1_18
# BB#87:
	movl	(%rax), %r10d
	movl	(%r13), %eax
	cmpl	%eax, %r10d
	movq	8(%rsp), %rbx           # 8-byte Reload
	jae	.LBB1_109
# BB#88:
	movslq	%r10d, %rdx
	addq	%rbx, %rdx
	testl	%r10d, %r10d
	js	.LBB1_114
# BB#89:
	cmpl	$8, %eax
	jb	.LBB1_114
# BB#90:                                # %.lr.ph354.preheader
	leaq	(%rbx,%rax), %rcx
.LBB1_91:                               # %.lr.ph354
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_95 Depth 2
                                        #       Child Loop BB1_102 Depth 3
	leaq	8(%rdx), %rbp
	cmpq	%rbx, %rbp
	jbe	.LBB1_114
# BB#92:                                # %.lr.ph354
                                        #   in Loop: Header=BB1_91 Depth=1
	cmpq	%rcx, %rbp
	ja	.LBB1_114
# BB#93:                                #   in Loop: Header=BB1_91 Depth=1
	cmpl	$0, (%rdx)
	jne	.LBB1_95
	jmp	.LBB1_114
.LBB1_94:                               # %.critedge10
                                        #   in Loop: Header=BB1_95 Depth=2
	incq	%rdi
	movq	%rdi, %rbp
.LBB1_95:                               # %.backedge
                                        #   Parent Loop BB1_91 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_102 Depth 3
	testl	%eax, %eax
	je	.LBB1_107
# BB#96:                                # %.backedge
                                        #   in Loop: Header=BB1_95 Depth=2
	cmpl	$1, %eax
	je	.LBB1_107
# BB#97:                                #   in Loop: Header=BB1_95 Depth=2
	cmpq	%rbx, %rbp
	jb	.LBB1_106
# BB#98:                                #   in Loop: Header=BB1_95 Depth=2
	leaq	2(%rbp), %rsi
	cmpq	%rcx, %rsi
	ja	.LBB1_106
# BB#99:                                #   in Loop: Header=BB1_95 Depth=2
	cmpq	%rbx, %rsi
	jbe	.LBB1_106
# BB#100:                               #   in Loop: Header=BB1_95 Depth=2
	leaq	1(%rbp), %rsi
	cmpb	$0, (%rbp)
	je	.LBB1_108
# BB#101:                               # %.preheader289
                                        #   in Loop: Header=BB1_95 Depth=2
	movq	%rsi, %rdi
	.p2align	4, 0x90
.LBB1_102:                              # %.preheader289.split.split
                                        #   Parent Loop BB1_91 Depth=1
                                        #     Parent Loop BB1_95 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbx, %rsi
	jb	.LBB1_94
# BB#103:                               #   in Loop: Header=BB1_102 Depth=3
	leaq	2(%rdi), %rbp
	cmpq	%rcx, %rbp
	ja	.LBB1_94
# BB#104:                               #   in Loop: Header=BB1_102 Depth=3
	cmpq	%rbx, %rbp
	jbe	.LBB1_94
# BB#105:                               #   in Loop: Header=BB1_102 Depth=3
	leaq	1(%rdi), %rbp
	cmpb	$0, (%rdi)
	movq	%rbp, %rdi
	jne	.LBB1_102
	jmp	.LBB1_95
.LBB1_106:                              # %.critedge
                                        #   in Loop: Header=BB1_91 Depth=1
	incq	%rbp
	movq	%rbp, %rsi
	jmp	.LBB1_108
.LBB1_107:                              # %.critedge9
                                        #   in Loop: Header=BB1_91 Depth=1
	addq	$9, %rdx
	movq	%rdx, %rsi
.LBB1_108:                              # %.backedge292
                                        #   in Loop: Header=BB1_91 Depth=1
	cmpq	%rbx, %rsi
	movq	%rsi, %rdx
	jae	.LBB1_91
	jmp	.LBB1_115
.LBB1_75:
	xorl	%r15d, %r15d
                                        # implicit-def: %RBP
	movq	8(%rsp), %rbx           # 8-byte Reload
	cmpl	$289, %ecx              # imm = 0x121
	jae	.LBB1_20
	jmp	.LBB1_43
.LBB1_76:
	movslq	268(%r15), %rdi
	addq	%r14, %rdi
	movslq	260(%r15), %rsi
	addq	8(%rsp), %rsi           # 8-byte Folded Reload
	addq	%rbp, %rsi
	movslq	264(%r15), %rdx
	callq	memcpy
	addq	$288, %r15              # imm = 0x120
	movl	$1, %eax
.LBB1_77:                               # %.prol.loopexit
	cmpl	$1, %r12d
	movq	8(%rsp), %r13           # 8-byte Reload
	je	.LBB1_80
# BB#78:                                # %.lr.ph.new
	addq	$60, %r15
	subl	%eax, %r12d
	.p2align	4, 0x90
.LBB1_79:                               # =>This Inner Loop Header: Depth=1
	movslq	-40(%r15), %rdi
	addq	%r14, %rdi
	movslq	-48(%r15), %rsi
	addq	%r13, %rsi
	addq	%rbp, %rsi
	movslq	-44(%r15), %rdx
	callq	memcpy
	movslq	(%r15), %rdi
	addq	%r14, %rdi
	movslq	-8(%r15), %rsi
	addq	%r13, %rsi
	addq	%rbp, %rsi
	movslq	-4(%r15), %rdx
	callq	memcpy
	addq	$80, %r15
	addl	$-2, %r12d
	jne	.LBB1_79
.LBB1_80:                               # %._crit_edge
	movl	$8192, %eax             # imm = 0x2000
	movq	24(%rsp), %rbp          # 8-byte Reload
	addl	(%rbp), %eax
	cmpl	%eax, %ebx
	jbe	.LBB1_82
# BB#81:
	xorl	%r12d, %r12d
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r14, %rdi
	callq	free
	jmp	.LBB1_84
.LBB1_82:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r14, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movl	%ebx, (%rbp)
	movq	%r14, %rdi
	callq	free
	movl	$.L.str.11, %edi
.LBB1_83:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, %r12d
.LBB1_84:
	movl	%r12d, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_109:
	xorl	%r15d, %r15d
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%r10, %rbp
	callq	cli_dbgmsg
	movq	%rbp, %r10
	movq	16(%rsp), %r9           # 8-byte Reload
                                        # implicit-def: %RBP
	jmp	.LBB1_19
.LBB1_114:
	movq	%rdx, %rsi
.LBB1_115:                              # %.critedge243
	leaq	4(%rsi), %r15
	xorl	%ebp, %ebp
	cmpq	%rbx, %r15
	jb	.LBB1_127
# BB#116:                               # %.critedge243
	cmpl	$248, %eax
	jb	.LBB1_127
# BB#117:
	leaq	252(%rsi), %rcx
	xorl	%ebp, %ebp
	cmpq	%rbx, %rcx
	jbe	.LBB1_127
# BB#118:
	leaq	(%rbx,%rax), %rdx
	cmpq	%rdx, %rcx
	ja	.LBB1_127
# BB#119:
	xorl	%ebp, %ebp
	cmpl	$17744, (%r15)          # imm = 0x4550
	jne	.LBB1_127
# BB#120:
	movl	60(%rsi), %r14d
	testl	%r14d, %r14d
	je	.LBB1_126
# BB#121:
	movzbl	10(%rsi), %edi
	movzbl	11(%rsi), %r12d
	shll	$8, %r12d
	orl	%edi, %r12d
	xorl	%ebp, %ebp
	testl	%eax, %eax
	je	.LBB1_127
# BB#122:
	testl	%r12d, %r12d
	je	.LBB1_127
# BB#123:
	xorl	%ebp, %ebp
	cmpq	%rbx, %rcx
	jb	.LBB1_127
# BB#124:
	leal	(,%r12,8), %esi
	leal	(%rsi,%rsi,4), %esi
	cmpl	%eax, %esi
	ja	.LBB1_127
# BB#125:
	movl	%esi, %eax
	addq	%rcx, %rax
	xorl	%esi, %esi
	cmpq	%rbx, %rax
	cmovbeq	%rsi, %rcx
	cmpq	%rdx, %rax
	cmovaq	%rsi, %rcx
	movq	%rcx, %rbp
	jmp	.LBB1_127
.LBB1_126:
	xorl	%r14d, %r14d
.LBB1_127:                              # %checkpe.exit
	testq	%rbp, %rbp
	cmoveq	%rbp, %r15
	jmp	.LBB1_19
.Lfunc_end1:
	.size	pefromupx, .Lfunc_end1-pefromupx
	.cfi_endproc

	.globl	upx_inflate2d
	.p2align	4, 0x90
	.type	upx_inflate2d,@function
upx_inflate2d:                          # @upx_inflate2d
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 160
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movl	%r9d, %r12d
	movl	%r8d, %r11d
	movl	.Lupx_inflate2d.magic+8(%rip), %eax
	movl	%eax, 96(%rsp)
	movq	.Lupx_inflate2d.magic(%rip), %rax
	movq	%rax, 88(%rsp)
	movl	%esi, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leaq	(%rdi,%rax), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$-1, 4(%rsp)            # 4-byte Folded Spill
	xorl	%r14d, %r14d
	xorl	%r10d, %r10d
	xorl	%r15d, %r15d
	cmpl	$4, %esi
	jae	.LBB2_8
	jmp	.LBB2_2
.LBB2_94:                               # %._crit_edge
	addl	%r15d, %r13d
	movl	%r13d, %r15d
	movq	32(%rsp), %r12          # 8-byte Reload
	movl	28(%rsp), %r11d         # 4-byte Reload
	cmpl	$4, %esi
	jae	.LBB2_8
	.p2align	4, 0x90
.LBB2_2:                                # %.split.preheader
	movl	$-1, %eax
	testl	$2147483647, %r14d      # imm = 0x7FFFFFFF
	je	.LBB2_96
# BB#3:                                 # %doubleebx.exit.lr.ph
	movl	%r15d, %r15d
	movl	%r10d, %r10d
	.p2align	4, 0x90
.LBB2_4:                                # %doubleebx.exit
                                        # =>This Inner Loop Header: Depth=1
                                        # kill: %R14D<def> %R14D<kill> %R14<kill> %R14<def>
	testl	%r14d, %r14d
	leal	(%r14,%r14), %r14d
	jns	.LBB2_15
# BB#5:                                 #   in Loop: Header=BB2_4 Depth=1
	cmpq	80(%rsp), %r10          # 8-byte Folded Reload
	jae	.LBB2_96
# BB#6:                                 #   in Loop: Header=BB2_4 Depth=1
	movl	(%rcx), %ebp
	cmpq	%rbp, %r15
	jae	.LBB2_96
# BB#7:                                 # %.split
                                        #   in Loop: Header=BB2_4 Depth=1
	movzbl	(%rdi,%r10), %ebx
	movb	%bl, (%rdx,%r15)
	incq	%r10
	incq	%r15
	testl	$2147483646, %r14d      # imm = 0x7FFFFFFE
	jne	.LBB2_4
	jmp	.LBB2_96
	.p2align	4, 0x90
.LBB2_21:                               #   in Loop: Header=BB2_8 Depth=1
	movl	%r10d, %eax
	incl	%r10d
	movzbl	(%rdi,%rax), %eax
	movl	%r15d, %ebp
	incl	%r15d
	movb	%al, (%rdx,%rbp)
	movl	%ebx, %r14d
.LBB2_8:                                # %.split.us
                                        # =>This Inner Loop Header: Depth=1
	testl	$2147483647, %r14d      # imm = 0x7FFFFFFF
	je	.LBB2_10
# BB#9:                                 #   in Loop: Header=BB2_8 Depth=1
	leal	(%r14,%r14), %ebx
	testl	%r14d, %r14d
	js	.LBB2_19
	jmp	.LBB2_14
	.p2align	4, 0x90
.LBB2_10:                               #   in Loop: Header=BB2_8 Depth=1
	movl	%r10d, %ebp
	leaq	4(%rdi,%rbp), %rbx
	movl	$-1, %eax
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	ja	.LBB2_96
# BB#11:                                #   in Loop: Header=BB2_8 Depth=1
	cmpq	%rdi, %rbx
	jbe	.LBB2_96
# BB#12:                                #   in Loop: Header=BB2_8 Depth=1
	addq	%rdi, %rbp
	movl	(%rbp), %r14d
	leal	1(%r14,%r14), %ebx
	addl	$4, %r10d
	testl	%r14d, %r14d
	jns	.LBB2_14
.LBB2_19:                               #   in Loop: Header=BB2_8 Depth=1
	movl	$-1, %eax
	cmpl	%esi, %r10d
	jae	.LBB2_96
# BB#20:                                #   in Loop: Header=BB2_8 Depth=1
	cmpl	(%rcx), %r15d
	jb	.LBB2_21
	jmp	.LBB2_96
.LBB2_14:
	movl	%ebx, %r14d
.LBB2_15:                               # %.preheader216
	cmpl	$4, %esi
	jb	.LBB2_22
# BB#16:                                # %.preheader216.split.us.preheader
	movl	$1, %ebx
	testl	$2147483647, %r14d      # imm = 0x7FFFFFFF
	jne	.LBB2_18
	jmp	.LBB2_29
	.p2align	4, 0x90
.LBB2_43:                               # %doubleebx.exit155.us
	shrl	$31, %r9d
	leal	-2(%r9,%rbx,2), %ebx
	testl	$2147483647, %r14d      # imm = 0x7FFFFFFF
	je	.LBB2_29
.LBB2_18:
	leal	(%r14,%r14), %eax
	testl	$2147483647, %eax       # imm = 0x7FFFFFFF
	jne	.LBB2_33
	jmp	.LBB2_34
	.p2align	4, 0x90
.LBB2_29:
	movl	%r10d, %ebp
	leaq	4(%rdi,%rbp), %r8
	movl	$-1, %eax
	cmpq	8(%rsp), %r8            # 8-byte Folded Reload
	ja	.LBB2_96
# BB#30:
	cmpq	%rdi, %r8
	jbe	.LBB2_96
# BB#31:
	addq	%rdi, %rbp
	movl	(%rbp), %r14d
	leal	1(%r14,%r14), %eax
	addl	$4, %r10d
	testl	$2147483647, %eax       # imm = 0x7FFFFFFF
	je	.LBB2_34
.LBB2_33:
	leal	(%rax,%rax), %r9d
	jmp	.LBB2_37
	.p2align	4, 0x90
.LBB2_34:
	movl	%r10d, %ebp
	leaq	4(%rdi,%rbp), %r8
	movl	$-1, %eax
	cmpq	8(%rsp), %r8            # 8-byte Folded Reload
	ja	.LBB2_96
# BB#35:
	cmpq	%rdi, %r8
	jbe	.LBB2_96
# BB#36:
	addq	%rdi, %rbp
	movl	(%rbp), %eax
	leal	1(%rax,%rax), %r9d
	addl	$4, %r10d
.LBB2_37:                               # %doubleebx.exit159.us
	shldl	$1, %r14d, %ebx
	testl	%eax, %eax
	js	.LBB2_26
# BB#38:
	testl	$2147483647, %r9d       # imm = 0x7FFFFFFF
	je	.LBB2_40
# BB#39:
	leal	(%r9,%r9), %r14d
	jmp	.LBB2_43
	.p2align	4, 0x90
.LBB2_40:
	movl	%r10d, %ebp
	leaq	4(%rdi,%rbp), %r8
	movl	$-1, %eax
	cmpq	8(%rsp), %r8            # 8-byte Folded Reload
	ja	.LBB2_96
# BB#41:
	cmpq	%rdi, %r8
	jbe	.LBB2_96
# BB#42:
	addq	%rdi, %rbp
	movl	(%rbp), %r9d
	leal	1(%r9,%r9), %r14d
	addl	$4, %r10d
	jmp	.LBB2_43
.LBB2_22:                               # %.preheader216.split.preheader
	movl	$-1, %eax
	testl	$2147483647, %r14d      # imm = 0x7FFFFFFF
	je	.LBB2_96
# BB#23:                                # %doubleebx.exit135.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB2_24:                               # %doubleebx.exit135
                                        # =>This Inner Loop Header: Depth=1
	leal	(%r14,%r14), %ebp
	testl	$2147483646, %ebp       # imm = 0x7FFFFFFE
	je	.LBB2_96
# BB#25:                                # %doubleebx.exit159
                                        #   in Loop: Header=BB2_24 Depth=1
	shldl	$1, %r14d, %ebx
	leal	(,%r14,4), %r9d
	testl	%ebp, %ebp
	js	.LBB2_26
# BB#44:                                #   in Loop: Header=BB2_24 Depth=1
	testl	$2147483644, %r9d       # imm = 0x7FFFFFFC
	je	.LBB2_96
# BB#45:                                # %doubleebx.exit155
                                        #   in Loop: Header=BB2_24 Depth=1
	leal	(,%r14,8), %ebp
	shrl	$29, %r14d
	andl	$1, %r14d
	leal	-2(%r14,%rbx,2), %ebx
	testl	$2147483640, %ebp       # imm = 0x7FFFFFF8
	movl	%ebp, %r14d
	jne	.LBB2_24
	jmp	.LBB2_96
.LBB2_26:
	movl	4(%rsp), %ebp           # 4-byte Reload
	addl	$-3, %ebx
	js	.LBB2_48
# BB#27:
	cmpl	%esi, %r10d
	jae	.LBB2_28
# BB#46:
	shll	$8, %ebx
	movl	%r10d, %eax
	movzbl	(%rdi,%rax), %ebp
	orl	%ebx, %ebp
	cmpl	$-1, %ebp
	je	.LBB2_95
# BB#47:
	notl	%ebp
	incl	%r10d
	movl	%ebp, %r13d
	andl	$1, %r13d
	sarl	%ebp
	testl	$2147483647, %r9d       # imm = 0x7FFFFFFF
	jne	.LBB2_56
	jmp	.LBB2_57
.LBB2_48:
	testl	$2147483647, %r9d       # imm = 0x7FFFFFFF
	je	.LBB2_50
# BB#49:
	leal	(%r9,%r9), %eax
	jmp	.LBB2_54
.LBB2_50:
	movl	$-1, %eax
	cmpl	$4, %esi
	jb	.LBB2_96
# BB#51:
	movl	%r10d, %ebp
	leaq	4(%rdi,%rbp), %rbx
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	ja	.LBB2_96
# BB#52:
	cmpq	%rdi, %rbx
	jbe	.LBB2_96
# BB#53:
	addq	%rdi, %rbp
	movl	(%rbp), %r9d
	leal	1(%r9,%r9), %eax
	addl	$4, %r10d
	movl	4(%rsp), %ebp           # 4-byte Reload
.LBB2_54:                               # %doubleebx.exit151
	movl	%r9d, %r13d
	shrl	$31, %r13d
	movl	%eax, %r9d
	testl	$2147483647, %r9d       # imm = 0x7FFFFFFF
	je	.LBB2_57
.LBB2_56:
	movl	%ebp, %r8d
	leal	(%r9,%r9), %r14d
	jmp	.LBB2_61
.LBB2_57:
	movl	$-1, %eax
	cmpl	$4, %esi
	jb	.LBB2_96
# BB#58:
	movl	%r10d, %ebx
	leaq	4(%rdi,%rbx), %r9
	cmpq	8(%rsp), %r9            # 8-byte Folded Reload
	ja	.LBB2_96
# BB#59:
	movl	%ebp, %r8d
	cmpq	%rdi, %r9
	jbe	.LBB2_96
# BB#60:
	addq	%rdi, %rbx
	movl	(%rbx), %r9d
	leal	1(%r9,%r9), %r14d
	addl	$4, %r10d
.LBB2_61:                               # %doubleebx.exit147
	shrl	$31, %r9d
	addl	%r13d, %r13d
	orl	%r9d, %r13d
	jne	.LBB2_79
# BB#62:                                # %.preheader215
	movl	$1, %r13d
	cmpl	$3, %esi
	jbe	.LBB2_63
	.p2align	4, 0x90
.LBB2_67:                               # %.preheader215.split.us
                                        # =>This Inner Loop Header: Depth=1
	testl	$2147483647, %r14d      # imm = 0x7FFFFFFF
	je	.LBB2_69
# BB#68:                                #   in Loop: Header=BB2_67 Depth=1
	leal	(%r14,%r14), %eax
	testl	$2147483647, %eax       # imm = 0x7FFFFFFF
	jne	.LBB2_73
	jmp	.LBB2_74
.LBB2_69:                               #   in Loop: Header=BB2_67 Depth=1
	movl	%r10d, %ebp
	leaq	4(%rdi,%rbp), %rbx
	movl	$-1, %eax
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	ja	.LBB2_96
# BB#70:                                #   in Loop: Header=BB2_67 Depth=1
	cmpq	%rdi, %rbx
	jbe	.LBB2_96
# BB#71:                                #   in Loop: Header=BB2_67 Depth=1
	addq	%rdi, %rbp
	movl	(%rbp), %r14d
	leal	1(%r14,%r14), %eax
	addl	$4, %r10d
	testl	$2147483647, %eax       # imm = 0x7FFFFFFF
	je	.LBB2_74
.LBB2_73:                               #   in Loop: Header=BB2_67 Depth=1
	leal	(%rax,%rax), %ebx
	jmp	.LBB2_77
.LBB2_74:                               #   in Loop: Header=BB2_67 Depth=1
	movl	%r10d, %ebp
	leaq	4(%rdi,%rbp), %rbx
	movl	$-1, %eax
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	ja	.LBB2_96
# BB#75:                                #   in Loop: Header=BB2_67 Depth=1
	cmpq	%rdi, %rbx
	jbe	.LBB2_96
# BB#76:                                #   in Loop: Header=BB2_67 Depth=1
	addq	%rdi, %rbp
	movl	(%rbp), %eax
	leal	1(%rax,%rax), %ebx
	addl	$4, %r10d
.LBB2_77:                               # %doubleebx.exit139.us
                                        #   in Loop: Header=BB2_67 Depth=1
	shldl	$1, %r14d, %r13d
	testl	%eax, %eax
	movl	%ebx, %r14d
	jns	.LBB2_67
	jmp	.LBB2_78
	.p2align	4, 0x90
.LBB2_63:                               # %.preheader215.split.split
                                        # =>This Inner Loop Header: Depth=1
	movl	$-1, %eax
	testl	$2147483647, %r14d      # imm = 0x7FFFFFFF
	je	.LBB2_96
# BB#64:                                # %doubleebx.exit143
                                        #   in Loop: Header=BB2_63 Depth=1
	leal	(%r14,%r14), %ebp
	testl	$2147483646, %ebp       # imm = 0x7FFFFFFE
	je	.LBB2_96
# BB#65:                                # %doubleebx.exit139
                                        #   in Loop: Header=BB2_63 Depth=1
	shldl	$1, %r14d, %r13d
	shll	$2, %r14d
	testl	%ebp, %ebp
	jns	.LBB2_63
# BB#66:
	movl	%r14d, %ebx
.LBB2_78:                               # %.us-lcssa225.us
	addl	$2, %r13d
	movl	%ebx, %r14d
.LBB2_79:
	movl	%r8d, %ebx
	cmpl	$-1280, %ebx            # imm = 0xFB00
	adcl	$0, %r13d
	movl	(%rcx), %r8d
	movl	$-1, %eax
	cmpl	%r8d, %r13d
	jae	.LBB2_96
# BB#80:
	movl	%r11d, 28(%rsp)         # 4-byte Spill
	movl	%r15d, %r9d
	leaq	(%rdx,%r9), %r11
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	movslq	%ebx, %rbx
	addq	%r11, %rbx
	cmpq	%rdx, %rbx
	jb	.LBB2_96
# BB#81:
	incl	%r13d
	movl	%r13d, %ebp
	addq	%rbp, %rbx
	addq	%rdx, %r8
	cmpq	%r8, %rbx
	ja	.LBB2_96
# BB#82:
	cmpq	%rdx, %rbx
	jbe	.LBB2_96
# BB#83:
	addq	%rbp, %r11
	cmpq	%r8, %r11
	ja	.LBB2_96
# BB#84:
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jns	.LBB2_96
# BB#85:
	cmpq	%rdx, %r11
	jbe	.LBB2_96
# BB#86:                                # %.preheader
	movq	%r12, 32(%rsp)          # 8-byte Spill
	testl	%r13d, %r13d
	je	.LBB2_94
# BB#87:                                # %.lr.ph
	leaq	-1(%rbp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rbp, %r8
	movq	%rbp, %r11
	andq	$3, %r11
	je	.LBB2_88
# BB#89:                                # %.prol.preheader
	movl	4(%rsp), %r12d          # 4-byte Reload
	addq	%r9, %r12
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_90:                               # =>This Inner Loop Header: Depth=1
	leal	(%r12,%rax), %ebp
	movzbl	(%rdx,%rbp), %ebx
	leal	(%r9,%rax), %ebp
	movb	%bl, (%rdx,%rbp)
	incq	%rax
	cmpq	%rax, %r11
	jne	.LBB2_90
	jmp	.LBB2_91
.LBB2_88:
	xorl	%eax, %eax
.LBB2_91:                               # %.prol.loopexit
	cmpq	$3, 16(%rsp)            # 8-byte Folded Reload
	movq	%r8, %rbp
	jb	.LBB2_94
# BB#92:                                # %.lr.ph.new
	subq	%rax, %rbp
	leal	3(%r15,%rax), %r8d
	movl	4(%rsp), %ebx           # 4-byte Reload
	leal	2(%r15,%rax), %r9d
	movl	%eax, %r12d
	addl	%r15d, %r12d
	leal	1(%r15,%rax), %r11d
	movq	%r8, 16(%rsp)           # 8-byte Spill
	leaq	(%rbx,%r8), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%r9, 72(%rsp)           # 8-byte Spill
	leaq	(%rbx,%r9), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	(%rbx,%r12), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%r11, %r8
	leaq	(%rbx,%r11), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB2_93:                               # =>This Inner Loop Header: Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r11), %r9d
	movzbl	(%rdx,%r9), %eax
	movq	%rbp, %rbx
	leal	(%r12,%r11), %ebp
	movb	%al, (%rdx,%rbp)
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r11), %eax
	movzbl	(%rdx,%rax), %eax
	leal	(%r8,%r11), %ebp
	movb	%al, (%rdx,%rbp)
	movq	56(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r11), %eax
	movzbl	(%rdx,%rax), %eax
	movq	72(%rsp), %rbp          # 8-byte Reload
	leal	(%rbp,%r11), %ebp
	movb	%al, (%rdx,%rbp)
	movq	64(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r11), %eax
	movzbl	(%rdx,%rax), %eax
	movq	16(%rsp), %rbp          # 8-byte Reload
	leal	(%rbp,%r11), %ebp
	movb	%al, (%rdx,%rbp)
	movq	%rbx, %rbp
	addq	$4, %r11
	cmpq	%r11, %rbp
	jne	.LBB2_93
	jmp	.LBB2_94
.LBB2_28:
	movl	$-1, %eax
	jmp	.LBB2_96
.LBB2_95:
	subq	$8, %rsp
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	leaq	96(%rsp), %rax
	movl	168(%rsp), %r8d
	movl	%r11d, %r9d
	pushq	%r15
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	callq	pefromupx
	addq	$32, %rsp
.Lcfi48:
	.cfi_adjust_cfa_offset -32
.LBB2_96:                               # %doubleebx.exit.thread
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	upx_inflate2d, .Lfunc_end2-upx_inflate2d
	.cfi_endproc

	.globl	upx_inflate2e
	.p2align	4, 0x90
	.type	upx_inflate2e,@function
upx_inflate2e:                          # @upx_inflate2e
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi55:
	.cfi_def_cfa_offset 160
.Lcfi56:
	.cfi_offset %rbx, -56
.Lcfi57:
	.cfi_offset %r12, -48
.Lcfi58:
	.cfi_offset %r13, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movl	%r9d, %r12d
	movl	.Lupx_inflate2e.magic+8(%rip), %eax
	movl	%eax, 96(%rsp)
	movq	.Lupx_inflate2e.magic(%rip), %rax
	movq	%rax, 88(%rsp)
	movl	%esi, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leaq	(%rdi,%rax), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$-1, 4(%rsp)            # 4-byte Folded Spill
	xorl	%r11d, %r11d
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	cmpl	$4, %esi
	jae	.LBB3_9
	jmp	.LBB3_2
	.p2align	4, 0x90
.LBB3_3:                                # %doubleebx.exit.lr.ph
	movl	%r15d, %r15d
	movl	%r14d, %r14d
	.p2align	4, 0x90
.LBB3_4:                                # %doubleebx.exit
                                        # =>This Inner Loop Header: Depth=1
                                        # kill: %R11D<def> %R11D<kill> %R11<kill> %R11<def>
	testl	%r11d, %r11d
	leal	(%r11,%r11), %r11d
	jns	.LBB3_17
# BB#5:                                 #   in Loop: Header=BB3_4 Depth=1
	cmpq	80(%rsp), %r14          # 8-byte Folded Reload
	jae	.LBB3_102
# BB#6:                                 #   in Loop: Header=BB3_4 Depth=1
	movl	(%rcx), %ebp
	cmpq	%rbp, %r15
	jae	.LBB3_102
# BB#7:                                 # %.split
                                        #   in Loop: Header=BB3_4 Depth=1
	movzbl	(%rdi,%r14), %ebx
	movb	%bl, (%rdx,%r15)
	incq	%r14
	incq	%r15
	testl	$2147483646, %r11d      # imm = 0x7FFFFFFE
	jne	.LBB3_4
	jmp	.LBB3_102
.LBB3_1:                                # %._crit_edge
	addl	%r15d, %r13d
	movl	%r13d, %r15d
	movq	32(%rsp), %r12          # 8-byte Reload
	movl	28(%rsp), %r8d          # 4-byte Reload
	cmpl	$4, %esi
	jae	.LBB3_9
	jmp	.LBB3_2
	.p2align	4, 0x90
.LBB3_8:                                #   in Loop: Header=BB3_9 Depth=1
	movl	%r14d, %eax
	incl	%r14d
	movzbl	(%rdi,%rax), %eax
	movl	%r15d, %ebp
	incl	%r15d
	movb	%al, (%rdx,%rbp)
	movl	%ebx, %r11d
.LBB3_9:                                # %.split.us
                                        # =>This Inner Loop Header: Depth=1
	testl	$2147483647, %r11d      # imm = 0x7FFFFFFF
	je	.LBB3_11
# BB#10:                                #   in Loop: Header=BB3_9 Depth=1
	leal	(%r11,%r11), %ebx
	testl	%r11d, %r11d
	js	.LBB3_14
	jmp	.LBB3_16
	.p2align	4, 0x90
.LBB3_11:                               #   in Loop: Header=BB3_9 Depth=1
	movl	%r14d, %ebp
	leaq	4(%rdi,%rbp), %rbx
	movl	$-1, %eax
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	ja	.LBB3_102
# BB#12:                                #   in Loop: Header=BB3_9 Depth=1
	cmpq	%rdi, %rbx
	jbe	.LBB3_102
# BB#13:                                #   in Loop: Header=BB3_9 Depth=1
	addq	%rdi, %rbp
	movl	(%rbp), %r11d
	leal	1(%r11,%r11), %ebx
	addl	$4, %r14d
	testl	%r11d, %r11d
	jns	.LBB3_16
.LBB3_14:                               #   in Loop: Header=BB3_9 Depth=1
	movl	$-1, %eax
	cmpl	%esi, %r14d
	jae	.LBB3_102
# BB#15:                                #   in Loop: Header=BB3_9 Depth=1
	cmpl	(%rcx), %r15d
	jb	.LBB3_8
	jmp	.LBB3_102
.LBB3_16:
	movl	%ebx, %r11d
.LBB3_17:                               # %.preheader227
	cmpl	$4, %esi
	jb	.LBB3_34
# BB#18:                                # %.preheader227.split.us.preheader
	movl	$1, %r10d
	testl	$2147483647, %r11d      # imm = 0x7FFFFFFF
	jne	.LBB3_20
	jmp	.LBB3_21
	.p2align	4, 0x90
.LBB3_29:
	testl	$2147483647, %r13d      # imm = 0x7FFFFFFF
	je	.LBB3_31
# BB#30:
	leal	(%r13,%r13), %r11d
	jmp	.LBB3_19
	.p2align	4, 0x90
.LBB3_31:
	movl	%r14d, %ebp
	leaq	4(%rdi,%rbp), %rbx
	movl	$-1, %eax
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	ja	.LBB3_102
# BB#32:
	cmpq	%rdi, %rbx
	jbe	.LBB3_102
# BB#33:
	addq	%rdi, %rbp
	movl	(%rbp), %r13d
	leal	1(%r13,%r13), %r11d
	addl	$4, %r14d
.LBB3_19:                               # %doubleebx.exit161.us
	shrl	$31, %r13d
	leal	-2(%r13,%r10,2), %r10d
	testl	$2147483647, %r11d      # imm = 0x7FFFFFFF
	je	.LBB3_21
.LBB3_20:
	leal	(%r11,%r11), %eax
	testl	$2147483647, %eax       # imm = 0x7FFFFFFF
	jne	.LBB3_24
	jmp	.LBB3_25
	.p2align	4, 0x90
.LBB3_21:
	movl	%r14d, %ebp
	leaq	4(%rdi,%rbp), %rbx
	movl	$-1, %eax
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	ja	.LBB3_102
# BB#22:
	cmpq	%rdi, %rbx
	jbe	.LBB3_102
# BB#23:
	addq	%rdi, %rbp
	movl	(%rbp), %r11d
	leal	1(%r11,%r11), %eax
	addl	$4, %r14d
	testl	$2147483647, %eax       # imm = 0x7FFFFFFF
	je	.LBB3_25
.LBB3_24:
	leal	(%rax,%rax), %r13d
	jmp	.LBB3_28
	.p2align	4, 0x90
.LBB3_25:
	movl	%r14d, %ebx
	leaq	4(%rdi,%rbx), %rbp
	movl	$-1, %eax
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	ja	.LBB3_102
# BB#26:
	cmpq	%rdi, %rbp
	jbe	.LBB3_102
# BB#27:
	addq	%rdi, %rbx
	movl	(%rbx), %eax
	leal	1(%rax,%rax), %r13d
	addl	$4, %r14d
.LBB3_28:                               # %doubleebx.exit165.us
	shldl	$1, %r11d, %r10d
	testl	%eax, %eax
	jns	.LBB3_29
	jmp	.LBB3_40
.LBB3_34:                               # %.preheader227.split.preheader
	movl	$-1, %eax
	testl	$2147483647, %r11d      # imm = 0x7FFFFFFF
	je	.LBB3_102
# BB#35:                                # %doubleebx.exit137.preheader
	movl	$1, %r10d
	.p2align	4, 0x90
.LBB3_36:                               # %doubleebx.exit137
                                        # =>This Inner Loop Header: Depth=1
	leal	(%r11,%r11), %ebx
	testl	$2147483646, %ebx       # imm = 0x7FFFFFFE
	je	.LBB3_102
# BB#37:                                # %doubleebx.exit165
                                        #   in Loop: Header=BB3_36 Depth=1
	shldl	$1, %r11d, %r10d
	leal	(,%r11,4), %r13d
	testl	%ebx, %ebx
	js	.LBB3_40
# BB#38:                                #   in Loop: Header=BB3_36 Depth=1
	testl	$2147483644, %r13d      # imm = 0x7FFFFFFC
	je	.LBB3_102
# BB#39:                                # %doubleebx.exit161
                                        #   in Loop: Header=BB3_36 Depth=1
	leal	(,%r11,8), %ebp
	shrl	$29, %r11d
	andl	$1, %r11d
	leal	-2(%r11,%r10,2), %r10d
	testl	$2147483640, %ebp       # imm = 0x7FFFFFF8
	movl	%ebp, %r11d
	jne	.LBB3_36
	jmp	.LBB3_102
.LBB3_40:                               # %.us-lcssa.us
	addl	$-3, %r10d
	js	.LBB3_44
# BB#41:
	cmpl	%esi, %r14d
	jae	.LBB3_101
# BB#42:
	shll	$8, %r10d
	movl	%r14d, %eax
	movzbl	(%rdi,%rax), %ebp
	orl	%r10d, %ebp
	cmpl	$-1, %ebp
	je	.LBB3_100
# BB#43:
	notl	%ebp
	incl	%r14d
	movl	%ebp, %eax
	andl	$1, %eax
	sarl	%ebp
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	jmp	.LBB3_51
.LBB3_44:
	testl	$2147483647, %r13d      # imm = 0x7FFFFFFF
	je	.LBB3_46
# BB#45:
	leal	(%r13,%r13), %ebp
	jmp	.LBB3_50
.LBB3_46:
	movl	$-1, %eax
	cmpl	$4, %esi
	jb	.LBB3_102
# BB#47:
	movl	%r14d, %ebp
	leaq	4(%rdi,%rbp), %rbx
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	ja	.LBB3_102
# BB#48:
	cmpq	%rdi, %rbx
	jbe	.LBB3_102
# BB#49:
	addq	%rdi, %rbp
	movl	(%rbp), %r13d
	leal	1(%r13,%r13), %ebp
	addl	$4, %r14d
.LBB3_50:                               # %doubleebx.exit157
	movl	%r13d, %eax
	shrl	$31, %eax
	movl	%ebp, %r13d
.LBB3_51:
	leal	(%r13,%r13), %r11d
	testl	$2147483647, %r13d      # imm = 0x7FFFFFFF
	movl	%r8d, 28(%rsp)          # 4-byte Spill
	je	.LBB3_53
.LBB3_52:
	shrl	$31, %r13d
	testl	%eax, %eax
	jne	.LBB3_58
	jmp	.LBB3_59
.LBB3_53:
	movl	$-1, %r13d
	cmpl	$4, %esi
	jb	.LBB3_57
# BB#54:
	movl	%r14d, %ebx
	leaq	4(%rdi,%rbx), %rbp
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	ja	.LBB3_57
# BB#55:
	cmpq	%rdi, %rbp
	jbe	.LBB3_57
# BB#56:
	addq	%rdi, %rbx
	movl	(%rbx), %r13d
	leal	1(%r13,%r13), %r11d
	addl	$4, %r14d
	jmp	.LBB3_52
.LBB3_57:                               # %doubleebx.exit153
	testl	%eax, %eax
	je	.LBB3_59
.LBB3_58:
	cmpl	$-1, %r13d
	jne	.LBB3_84
	jmp	.LBB3_101
.LBB3_59:
	movl	$-1, %eax
	cmpl	$-1, %r13d
	je	.LBB3_102
# BB#60:
	testl	%r13d, %r13d
	je	.LBB3_63
# BB#61:
	testl	$2147483647, %r11d      # imm = 0x7FFFFFFF
	je	.LBB3_79
# BB#62:
	leal	(%r11,%r11), %eax
	jmp	.LBB3_83
.LBB3_63:                               # %.preheader226
	movl	$1, %r13d
	cmpl	$3, %esi
	jbe	.LBB3_74
.LBB3_64:                               # %.preheader226.split.us
                                        # =>This Inner Loop Header: Depth=1
	testl	$2147483647, %r11d      # imm = 0x7FFFFFFF
	je	.LBB3_66
# BB#65:                                #   in Loop: Header=BB3_64 Depth=1
	leal	(%r11,%r11), %ebx
	testl	$2147483647, %ebx       # imm = 0x7FFFFFFF
	jne	.LBB3_69
	jmp	.LBB3_70
.LBB3_66:                               #   in Loop: Header=BB3_64 Depth=1
	movl	%r14d, %ebp
	leaq	4(%rdi,%rbp), %rbx
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	ja	.LBB3_102
# BB#67:                                #   in Loop: Header=BB3_64 Depth=1
	cmpq	%rdi, %rbx
	jbe	.LBB3_102
# BB#68:                                #   in Loop: Header=BB3_64 Depth=1
	addq	%rdi, %rbp
	movl	(%rbp), %r11d
	leal	1(%r11,%r11), %ebx
	addl	$4, %r14d
	testl	$2147483647, %ebx       # imm = 0x7FFFFFFF
	je	.LBB3_70
.LBB3_69:                               #   in Loop: Header=BB3_64 Depth=1
	leal	(%rbx,%rbx), %r8d
	jmp	.LBB3_73
.LBB3_70:                               #   in Loop: Header=BB3_64 Depth=1
	movl	%r14d, %ebx
	leaq	4(%rdi,%rbx), %rbp
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	ja	.LBB3_102
# BB#71:                                #   in Loop: Header=BB3_64 Depth=1
	cmpq	%rdi, %rbp
	jbe	.LBB3_102
# BB#72:                                #   in Loop: Header=BB3_64 Depth=1
	addq	%rdi, %rbx
	movl	(%rbx), %ebx
	leal	1(%rbx,%rbx), %r8d
	addl	$4, %r14d
.LBB3_73:                               # %doubleebx.exit141.us
                                        #   in Loop: Header=BB3_64 Depth=1
	shldl	$1, %r11d, %r13d
	testl	%ebx, %ebx
	movl	%r8d, %r11d
	jns	.LBB3_64
	jmp	.LBB3_78
.LBB3_74:                               # %.preheader226.split.split
                                        # =>This Inner Loop Header: Depth=1
	testl	$2147483647, %r11d      # imm = 0x7FFFFFFF
	je	.LBB3_102
# BB#75:                                # %doubleebx.exit145
                                        #   in Loop: Header=BB3_74 Depth=1
	leal	(%r11,%r11), %ebx
	testl	$2147483646, %ebx       # imm = 0x7FFFFFFE
	je	.LBB3_102
# BB#76:                                # %doubleebx.exit141
                                        #   in Loop: Header=BB3_74 Depth=1
	shldl	$1, %r11d, %r13d
	shll	$2, %r11d
	testl	%ebx, %ebx
	jns	.LBB3_74
# BB#77:
	movl	%r11d, %r8d
.LBB3_78:                               # %.us-lcssa236.us
	addl	$2, %r13d
	movl	%r8d, %r11d
	jmp	.LBB3_84
.LBB3_79:
	cmpl	$4, %esi
	jb	.LBB3_102
# BB#80:
	movl	%r14d, %ebp
	leaq	4(%rdi,%rbp), %rbx
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	ja	.LBB3_102
# BB#81:
	cmpq	%rdi, %rbx
	jbe	.LBB3_102
# BB#82:
	addq	%rdi, %rbp
	movl	(%rbp), %r11d
	leal	1(%r11,%r11), %eax
	addl	$4, %r14d
.LBB3_83:                               # %doubleebx.exit149
	shrl	$31, %r11d
	movl	%r11d, %r13d
	orl	$2, %r13d
	movl	%eax, %r11d
.LBB3_84:
	movl	4(%rsp), %ebx           # 4-byte Reload
	cmpl	$-1280, %ebx            # imm = 0xFB00
	adcl	$0, %r13d
	movl	(%rcx), %r8d
	leal	1(%r13), %ebp
	movl	$-1, %eax
	cmpl	%r8d, %ebp
	jae	.LBB3_102
# BB#85:
	movl	%r15d, %r9d
	leaq	(%rdx,%r9), %r10
	movslq	%ebx, %rbx
	addq	%r10, %rbx
	cmpq	%rdx, %rbx
	jb	.LBB3_102
# BB#86:
	addl	$2, %r13d
	movl	%r13d, %ebp
	addq	%rbp, %rbx
	addq	%rdx, %r8
	cmpq	%r8, %rbx
	ja	.LBB3_102
# BB#87:
	cmpq	%rdx, %rbx
	jbe	.LBB3_102
# BB#88:
	addq	%rbp, %r10
	cmpq	%r8, %r10
	ja	.LBB3_102
# BB#89:
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jns	.LBB3_102
# BB#90:
	movq	%rbp, %r8
	cmpq	%rdx, %r10
	jbe	.LBB3_102
# BB#91:                                # %.preheader
	movq	%r12, 32(%rsp)          # 8-byte Spill
	testl	%r13d, %r13d
	movq	%r8, %rax
	je	.LBB3_1
# BB#92:                                # %.lr.ph
	leaq	-1(%rax), %rbp
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rax, %r10
	andq	$3, %r10
	je	.LBB3_95
# BB#93:                                # %.prol.preheader
	movl	4(%rsp), %r12d          # 4-byte Reload
	addq	%r9, %r12
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_94:                               # =>This Inner Loop Header: Depth=1
	leal	(%r12,%rax), %ebp
	movzbl	(%rdx,%rbp), %ebx
	leal	(%r9,%rax), %ebp
	movb	%bl, (%rdx,%rbp)
	incq	%rax
	cmpq	%rax, %r10
	jne	.LBB3_94
	jmp	.LBB3_96
.LBB3_95:
	xorl	%eax, %eax
.LBB3_96:                               # %.prol.loopexit
	cmpq	$3, 16(%rsp)            # 8-byte Folded Reload
	movq	%r8, 16(%rsp)           # 8-byte Spill
	jb	.LBB3_1
# BB#97:                                # %.lr.ph.new
	subq	%rax, 16(%rsp)          # 8-byte Folded Spill
	leal	3(%r15,%rax), %ebp
	movl	4(%rsp), %r9d           # 4-byte Reload
	leal	2(%r15,%rax), %ebx
	movl	%eax, %r12d
	addl	%r15d, %r12d
	leal	1(%r15,%rax), %eax
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	leaq	(%r9,%rbp), %rbp
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	leaq	(%r9,%rbx), %rbp
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	leaq	(%r9,%r12), %rbp
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	%rax, %r10
	leaq	(%r9,%rax), %r9
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_98:                               # =>This Inner Loop Header: Depth=1
	movq	40(%rsp), %rbp          # 8-byte Reload
	leal	(%rbp,%rax), %ebx
	movzbl	(%rdx,%rbx), %ebx
	leal	(%r12,%rax), %r8d
	movb	%bl, (%rdx,%r8)
	leal	(%r9,%rax), %ebx
	movzbl	(%rdx,%rbx), %ebx
	leal	(%r10,%rax), %ebp
	movb	%bl, (%rdx,%rbp)
	movq	48(%rsp), %rbp          # 8-byte Reload
	leal	(%rbp,%rax), %ebp
	movzbl	(%rdx,%rbp), %ebx
	movq	64(%rsp), %rbp          # 8-byte Reload
	leal	(%rbp,%rax), %ebp
	movb	%bl, (%rdx,%rbp)
	movq	56(%rsp), %rbp          # 8-byte Reload
	leal	(%rbp,%rax), %ebp
	movzbl	(%rdx,%rbp), %ebx
	movq	72(%rsp), %rbp          # 8-byte Reload
	leal	(%rbp,%rax), %ebp
	movb	%bl, (%rdx,%rbp)
	addq	$4, %rax
	cmpq	%rax, 16(%rsp)          # 8-byte Folded Reload
	jne	.LBB3_98
	jmp	.LBB3_1
	.p2align	4, 0x90
.LBB3_2:                                # %.split.preheader
	movl	$-1, %eax
	testl	$2147483647, %r11d      # imm = 0x7FFFFFFF
	jne	.LBB3_3
.LBB3_102:                              # %doubleebx.exit.thread
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_101:
	movl	$-1, %eax
	jmp	.LBB3_102
.LBB3_100:
	subq	$8, %rsp
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	leaq	96(%rsp), %rax
	movl	%r8d, %r9d
	movl	168(%rsp), %r8d
	pushq	%r15
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	callq	pefromupx
	addq	$32, %rsp
.Lcfi66:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB3_102
.Lfunc_end3:
	.size	upx_inflate2e, .Lfunc_end3-upx_inflate2e
	.cfi_endproc

	.type	.Lupx_inflate2b.magic,@object # @upx_inflate2b.magic
	.section	.rodata.str4.16,"aMS",@progbits,4
	.p2align	4
.Lupx_inflate2b.magic:
	.long	264                     # 0x108
	.long	272                     # 0x110
	.long	213                     # 0xd5
	.long	0                       # 0x0
	.size	.Lupx_inflate2b.magic, 16

	.type	.Lupx_inflate2d.magic,@object # @upx_inflate2d.magic
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.Lupx_inflate2d.magic:
	.long	284                     # 0x11c
	.long	292                     # 0x124
	.long	0                       # 0x0
	.size	.Lupx_inflate2d.magic, 12

	.type	.Lupx_inflate2e.magic,@object # @upx_inflate2e.magic
	.p2align	2
.Lupx_inflate2e.magic:
	.long	296                     # 0x128
	.long	304                     # 0x130
	.long	0                       # 0x0
	.size	.Lupx_inflate2e.magic, 12

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"UPX: bad magic - scanning for imports\n"
	.size	.L.str, 39

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\215\276"
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"UPX: wrong realstuff size\n"
	.size	.L.str.2, 27

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"UPX: no luck - scanning for PE\n"
	.size	.L.str.3, 32

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"UPX: no luck - brutally crafing a reasonable PE\n"
	.size	.L.str.4, 49

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"UPX: malloc failed - giving up rebuild\n"
	.size	.L.str.5, 40

	.type	.L.str.6,@object        # @.str.6
	.section	.rodata,"a",@progbits
.L.str.6:
	.asciz	"MZ\220\000\002\000\000\000\004\000\017\000\377\377\000\000\260\000\000\000\000\000\000\000@\000\032\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\320\000\000\000\016\037\264\t\272\r\000\315!\264L\315!This file was created by ClamAV for internal use and should not be run.\r\nClamAV - A GPL virus scanner - http://www.clamav.net\r\n$\000\000\000"
	.size	.L.str.6, 209

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"PE\000\000L\001\001\000CLAM\000\000\000\000\000\000\000\000\340\000\203\217\013\001\000\000\000\020\000\000\000\020\000\000\000\000\000\000\000\020\000\000\000\020\000\000\000\020\000\000\000\000@\000\000\020\000\000\000\002\000\000\001\000\000\000\000\000\000\000\003\000\n\000\000\000\000\000\377\377\377\377\000\002\000\000\000\000\000\000\002\000\000\000\000\000\020\000\000\020\000\000\000\000\020\000\000\020\000\000\000\000\000\000\020\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000.clam01\000\377\377\377\377\000\020\000\000\377\377\377\377\000\002\000\000\000\000\000\000\000\000\000\000\000\000\000\000\377\377\377\377"
	.size	.L.str.7, 289

	.type	.L.str.8,@object        # @.str.8
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.8:
	.asciz	"UPX: PE structure added to uncompressed data\n"
	.size	.L.str.8, 46

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"UPX: Sect %d out of bounds - giving up rebuild\n"
	.size	.L.str.9, 48

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"UPX: wrong raw size - giving up rebuild\n"
	.size	.L.str.10, 41

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"UPX: PE structure rebuilt from compressed file\n"
	.size	.L.str.11, 48


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
