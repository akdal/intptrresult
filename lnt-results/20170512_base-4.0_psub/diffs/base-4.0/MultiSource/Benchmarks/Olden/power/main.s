	.text
	.file	"main.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4594212051873190380     # double 0.14000000000000001
.LCPI0_1:
	.quad	4666723172467343360     # double 1.0E+4
.LCPI0_3:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
.LCPI0_4:
	.quad	-4619342137793917747    # double -0.65000000000000002
.LCPI0_5:
	.quad	4576918229304087675     # double 0.01
.LCPI0_7:
	.quad	-4586634745500139520    # double -100
.LCPI0_8:
	.quad	4607182418800017408     # double 1
.LCPI0_9:
	.quad	-4629520272951775068    # double -0.13
.LCPI0_10:
	.quad	4566758108544739836     # double 0.002
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_2:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
.LCPI0_6:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
.LCPI0_11:
	.quad	4666723172467343360     # double 1.0E+4
	.quad	4626322717216342016     # double 20
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 64
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movl	$.Lstr, %edi
	callq	puts
	callq	build_tree
	movq	%rax, %rbx
	movl	$.Lstr.1, %edi
	callq	puts
	movq	%rbx, %rdi
	callq	Compute_Tree
	movl	$.Lstr.2, %edi
	callq	puts
	movups	(%rbx), %xmm0
	movups	%xmm0, 32(%rbx)
	movups	16(%rbx), %xmm0
	movups	%xmm0, 48(%rbx)
	movabsq	$4604480259023595110, %rax # imm = 0x3FE6666666666666
	movq	%rax, 16(%rbx)
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	xorl	%r14d, %r14d
	movl	$35, %ebp
	jmp	.LBB0_1
	.p2align	4, 0x90
.LBB0_4:                                #   in Loop: Header=BB0_1 Depth=1
	movapd	%xmm0, %xmm3
	addsd	.LCPI0_4(%rip), %xmm3
	divsd	.LCPI0_5(%rip), %xmm3
	cvttsd2si	%xmm3, %eax
	testl	%eax, %eax
	cmovsl	%r14d, %eax
	cmpl	$36, %eax
	cmovgel	%ebp, %eax
	subsd	%xmm2, %xmm0
	movapd	.LCPI0_6(%rip), %xmm3   # xmm3 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm3, %xmm0
	movsd	map_P+8(,%rax,8), %xmm2 # xmm2 = mem[0],zero
	subsd	map_P(,%rax,8), %xmm2
	divsd	.LCPI0_7(%rip), %xmm2
	movsd	.LCPI0_8(%rip), %xmm4   # xmm4 = mem[0],zero
	addsd	%xmm4, %xmm2
	divsd	%xmm2, %xmm0
	movapd	%xmm0, (%rsp)           # 16-byte Spill
	movapd	%xmm7, %xmm2
	addsd	.LCPI0_9(%rip), %xmm2
	divsd	.LCPI0_10(%rip), %xmm2
	cvttsd2si	%xmm2, %eax
	testl	%eax, %eax
	cmovsl	%r14d, %eax
	cmpl	$36, %eax
	cmovgel	%ebp, %eax
	movsd	map_Q+8(,%rax,8), %xmm2 # xmm2 = mem[0],zero
	subsd	map_Q(,%rax,8), %xmm2
	unpcklpd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0]
	divpd	.LCPI0_11(%rip), %xmm1
	unpcklpd	%xmm4, %xmm7    # xmm7 = xmm7[0],xmm4[0]
	subpd	%xmm1, %xmm7
	movapd	%xmm7, %xmm1
	xorpd	%xmm3, %xmm1
	movhlps	%xmm7, %xmm7            # xmm7 = xmm7[1,1]
	divsd	%xmm7, %xmm1
	movapd	%xmm1, 16(%rsp)         # 16-byte Spill
	movl	$.L.str.4, %edi
	movb	$2, %al
	callq	printf
	movups	(%rbx), %xmm0
	movups	%xmm0, 32(%rbx)
	movq	16(%rbx), %rax
	movq	%rax, 48(%rbx)
	movq	24(%rbx), %rcx
	movq	%rcx, 56(%rbx)
	movd	%rax, %xmm0
	addsd	(%rsp), %xmm0           # 16-byte Folded Reload
	movsd	%xmm0, 16(%rbx)
	movd	%rcx, %xmm0
	addsd	16(%rsp), %xmm0         # 16-byte Folded Reload
.LBB0_1:                                # %.sink.split
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm0, 24(%rbx)
	movq	%rbx, %rdi
	callq	Compute_Tree
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	movsd	24(%rbx), %xmm1         # xmm1 = mem[0],zero
	movsd	(%rbx), %xmm2           # xmm2 = mem[0],zero
	movsd	8(%rbx), %xmm3          # xmm3 = mem[0],zero
	movl	$.L.str.3, %edi
	movb	$4, %al
	callq	printf
	movsd	(%rbx), %xmm2           # xmm2 = mem[0],zero
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	movsd	.LCPI0_1(%rip), %xmm4   # xmm4 = mem[0],zero
	divsd	%xmm4, %xmm2
	movapd	%xmm2, %xmm1
	subsd	%xmm0, %xmm1
	movapd	.LCPI0_2(%rip), %xmm5   # xmm5 = [nan,nan]
	andpd	%xmm5, %xmm1
	movsd	.LCPI0_3(%rip), %xmm6   # xmm6 = mem[0],zero
	ucomisd	%xmm1, %xmm6
	jbe	.LBB0_2
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	movsd	8(%rbx), %xmm1          # xmm1 = mem[0],zero
	movsd	24(%rbx), %xmm7         # xmm7 = mem[0],zero
	movapd	%xmm1, %xmm3
	divsd	%xmm4, %xmm3
	subsd	%xmm7, %xmm3
	andpd	%xmm5, %xmm3
	ucomisd	%xmm3, %xmm6
	jbe	.LBB0_4
	jmp	.LBB0_5
	.p2align	4, 0x90
.LBB0_2:                                # %.sink.split._crit_edge
                                        #   in Loop: Header=BB0_1 Depth=1
	movsd	8(%rbx), %xmm1          # xmm1 = mem[0],zero
	movsd	24(%rbx), %xmm7         # xmm7 = mem[0],zero
	jmp	.LBB0_4
.LBB0_5:                                # %.critedge
	xorl	%eax, %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	map_P,@object           # @map_P
	.data
	.globl	map_P
	.p2align	4
map_P:
	.quad	4666037197108432731     # double 8752.2180910480001
	.quad	4665868910575242774     # double 8446.1066704160003
	.quad	4665636844731963463     # double 8107.9906802830001
	.quad	4665272027756832980     # double 7776.1915742849997
	.quad	4664919886007261842     # double 7455.9205187770003
	.quad	4664579786898578714     # double 7146.6021813520001
	.quad	4664251150399700434     # double 6847.7090268129996
	.quad	4663933419221909420     # double 6558.7342040240001
	.quad	4663626082828208484     # double 6279.213382291
	.quad	4663328652637820703     # double 6008.7021999859999
	.quad	4663040672929476682     # double 5746.7861810289996
	.quad	4662761718116392633     # double 5493.0782564949996
	.quad	4662491379077672883     # double 5247.2063330970004
	.quad	4662229279404882798     # double 5008.828069358
	.quad	4661975058842914394     # double 4777.6158151660002
	.quad	4661728375625487565     # double 4553.2587358999999
	.quad	4661488914380513348     # double 4335.4700023160003
	.quad	4661256369368200781     # double 4123.9715456940003
	.quad	4660835291965993313     # double 3918.5019396749999
	.quad	4660396181500043936     # double 3718.8176185379998
	.quad	4659969276335319911     # double 3524.6836257999998
	.quad	4659554085235497234     # double 3335.876573044
	.quad	4659150151189454026     # double 3152.1886356730001
	.quad	4658757037918488248     # double 2973.4214171029998
	.quad	4658374321919642436     # double 2799.3823304859998
	.quad	4658001609934539930     # double 2629.8925426169999
	.quad	4657638529836128928     # double 2464.782829705
	.quad	4657284720632021722     # double 2303.8890314179998
	.quad	4656939837598140879     # double 2147.0543853949998
	.quad	4656485104124281394     # double 1994.1327713989999
	.quad	4655829146816139813     # double 1844.9853473129999
	.quad	4655189185775318580     # double 1699.4750533209999
	.quad	4654564658624379979     # double 1557.474019598
	.quad	4653955029825950288     # double 1418.8604790429999
	.quad	4653359796661323057     # double 1283.520126656
	.quad	4652778453538895493     # double 1151.3380042159999
	.size	map_P, 288

	.type	map_Q,@object           # @map_Q
	.globl	map_Q
	.p2align	4
map_Q:
	.quad	4655494285021015208     # double 1768.8465901899999
	.quad	4655218892102191439     # double 1706.2294900459999
	.quad	4654915534130638479     # double 1637.253873079
	.quad	4654618153964160581     # double 1569.6374516230001
	.quad	4654331322490579186     # double 1504.419525242
	.quad	4654054502356017415     # double 1441.47791381
	.quad	4653787201168905391     # double 1380.700660446
	.quad	4653528946910335073     # double 1321.980440476
	.quad	4653279307376803534     # double 1265.218982201
	.quad	4653037869763333166     # double 1210.3224246360001
	.quad	4652804249409748029     # double 1157.203306183
	.quad	4652578087441262637     # double 1105.780028163
	.quad	4652359039517971117     # double 1055.9742967459999
	.quad	4652075162817371645     # double 1007.714103979
	.quad	4651663651150396104     # double 960.93064387499999
	.quad	4651264555511865795     # double 915.55872278200002
	.quad	4650877346900154801     # double 871.53820017800001
	.quad	4650501513434924411     # double 828.81088200600004
	.quad	4650136574234420011     # double 787.32209834000003
	.quad	4649782081508492625     # double 747.02094133399999
	.quad	4649437603942708827     # double 707.85837621400003
	.quad	4649102731874526026     # double 669.787829741
	.quad	4648777084308572480     # double 632.76598775599996
	.quad	4648460297925515646     # double 596.75154563299998
	.quad	4648152020558263867     # double 561.70446660899995
	.quad	4647851925255168695     # double 527.58758058499996
	.quad	4647404590246981284     # double 494.36573905099999
	.quad	4646835292182077030     # double 462.00489069100001
	.quad	4646280569319924544     # double 430.47254668599999
	.quad	4645739889007129525     # double 399.73842919600003
	.quad	4645212745457330485     # double 369.77378759499999
	.quad	4644698640200404273     # double 340.550287137
	.quad	4644197108244492029     # double 312.04149609500001
	.quad	4643707707079106099     # double 284.22226066000002
	.quad	4643230021392175655     # double 257.06897307399998
	.quad	4642316052852823420     # double 230.557938283
	.size	map_Q, 288

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.3:
	.asciz	"TR=%4.2f, TI=%4.2f, P0=%4.2f, Q0=%4.2f\n"
	.size	.L.str.3, 40

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"D TR-%4.2f, TI=%4.2f\n"
	.size	.L.str.4, 22

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Past initialization"
	.size	.Lstr, 20

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.1:
	.asciz	"Built tree"
	.size	.Lstr.1, 11

	.type	.Lstr.2,@object         # @str.2
.Lstr.2:
	.asciz	"COMPUTED TREE"
	.size	.Lstr.2, 14


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
