	.text
	.file	"btSphereShape.bc"
	.globl	_ZNK13btSphereShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK13btSphereShape37localGetSupportingVertexWithoutMarginERK9btVector3,@function
_ZNK13btSphereShape37localGetSupportingVertexWithoutMarginERK9btVector3: # @_ZNK13btSphereShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	retq
.Lfunc_end0:
	.size	_ZNK13btSphereShape37localGetSupportingVertexWithoutMarginERK9btVector3, .Lfunc_end0-_ZNK13btSphereShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_endproc

	.globl	_ZNK13btSphereShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.p2align	4, 0x90
	.type	_ZNK13btSphereShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,@function
_ZNK13btSphereShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i: # @_ZNK13btSphereShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_startproc
# BB#0:
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	testl	%ecx, %ecx
	jle	.LBB1_2
# BB#1:                                 # %.lr.ph.preheader
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	decl	%ecx
	shlq	$4, %rcx
	addq	$16, %rcx
	xorl	%esi, %esi
	movq	%rdx, %rdi
	movq	%rcx, %rdx
	callq	memset
	addq	$8, %rsp
.LBB1_2:                                # %._crit_edge
	retq
.Lfunc_end1:
	.size	_ZNK13btSphereShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i, .Lfunc_end1-_ZNK13btSphereShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	679477248               # float 1.42108547E-14
.LCPI2_2:
	.long	3212836864              # float -1
.LCPI2_3:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_1:
	.long	3212836864              # float -1
	.long	3212836864              # float -1
	.zero	4
	.zero	4
	.text
	.globl	_ZNK13btSphereShape24localGetSupportingVertexERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK13btSphereShape24localGetSupportingVertexERK9btVector3,@function
_ZNK13btSphereShape24localGetSupportingVertexERK9btVector3: # @_ZNK13btSphereShape24localGetSupportingVertexERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 24
	subq	$72, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 96
.Lcfi4:
	.cfi_offset %rbx, -24
.Lcfi5:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%r14), %rax
	callq	*104(%rax)
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movaps	%xmm1, 48(%rsp)         # 16-byte Spill
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movss	8(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	mulss	%xmm1, %xmm1
	pshufd	$229, %xmm0, %xmm2      # xmm2 = xmm0[1,1,2,3]
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	movss	.LCPI2_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	xorl	%eax, %eax
	ucomiss	%xmm1, %xmm2
	seta	%al
	movd	%eax, %xmm1
	pshufd	$80, %xmm1, %xmm2       # xmm2 = xmm1[0,0,1,1]
	pslld	$31, %xmm2
	psrad	$31, %xmm2
	movdqa	%xmm2, %xmm1
	pandn	%xmm0, %xmm1
	pand	.LCPI2_1(%rip), %xmm2
	por	%xmm1, %xmm2
	pshufd	$229, %xmm2, %xmm0      # xmm0 = xmm2[1,1,2,3]
	movdqa	%xmm2, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	jbe	.LBB2_2
# BB#1:
	movss	.LCPI2_2(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
.LBB2_2:
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB2_4
# BB#3:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	movdqa	%xmm2, (%rsp)           # 16-byte Spill
	callq	sqrtf
	movdqa	(%rsp), %xmm2           # 16-byte Reload
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
.LBB2_4:                                # %.split
	movss	.LCPI2_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	mulss	%xmm3, %xmm1
	movaps	%xmm1, (%rsp)           # 16-byte Spill
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*88(%rax)
	movaps	(%rsp), %xmm2           # 16-byte Reload
	mulss	%xmm0, %xmm2
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	16(%rsp), %xmm0         # 16-byte Folded Reload
	addps	32(%rsp), %xmm0         # 16-byte Folded Reload
	movaps	48(%rsp), %xmm1         # 16-byte Reload
	addss	%xmm2, %xmm1
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	_ZNK13btSphereShape24localGetSupportingVertexERK9btVector3, .Lfunc_end2-_ZNK13btSphereShape24localGetSupportingVertexERK9btVector3
	.cfi_endproc

	.globl	_ZNK13btSphereShape7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK13btSphereShape7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK13btSphereShape7getAabbERK11btTransformR9btVector3S4_: # @_ZNK13btSphereShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi10:
	.cfi_def_cfa_offset 80
.Lcfi11:
	.cfi_offset %rbx, -40
.Lcfi12:
	.cfi_offset %r12, -32
.Lcfi13:
	.cfi_offset %r14, -24
.Lcfi14:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	%xmm0, (%rsp)           # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movsd	48(%r12), %xmm1         # xmm1 = mem[0],zero
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	unpcklps	(%rsp), %xmm2   # 16-byte Folded Reload
                                        # xmm2 = xmm2[0],mem[0],xmm2[1],mem[1]
	subps	%xmm2, %xmm1
	movaps	%xmm2, %xmm5
	movss	56(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm2
	xorps	%xmm3, %xmm3
	xorps	%xmm4, %xmm4
	movss	%xmm2, %xmm4            # xmm4 = xmm2[0],xmm4[1,2,3]
	movlps	%xmm1, (%r15)
	movlps	%xmm4, 8(%r15)
	movsd	48(%r12), %xmm1         # xmm1 = mem[0],zero
	addps	%xmm5, %xmm1
	addss	56(%r12), %xmm0
	movss	%xmm0, %xmm3            # xmm3 = xmm0[0],xmm3[1,2,3]
	movlps	%xmm1, (%r14)
	movlps	%xmm3, 8(%r14)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	_ZNK13btSphereShape7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end3-_ZNK13btSphereShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1053609165              # float 0.400000006
	.text
	.globl	_ZNK13btSphereShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK13btSphereShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK13btSphereShape21calculateLocalInertiaEfR9btVector3: # @_ZNK13btSphereShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	mulss	.LCPI4_0(%rip), %xmm0
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movq	(%rbx), %rax
	callq	*88(%rax)
	mulss	4(%rsp), %xmm0          # 4-byte Folded Reload
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	mulss	4(%rsp), %xmm0          # 4-byte Folded Reload
	movss	%xmm0, (%r14)
	movss	%xmm0, 4(%r14)
	movss	%xmm0, 8(%r14)
	movl	$0, 12(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	_ZNK13btSphereShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end4-_ZNK13btSphereShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.section	.text._ZN13btSphereShapeD0Ev,"axG",@progbits,_ZN13btSphereShapeD0Ev,comdat
	.weak	_ZN13btSphereShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN13btSphereShapeD0Ev,@function
_ZN13btSphereShapeD0Ev:                 # @_ZN13btSphereShapeD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 32
.Lcfi23:
	.cfi_offset %rbx, -24
.Lcfi24:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp0:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp1:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB5_2:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
# BB#3:                                 # %_ZN13btSphereShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_4:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN13btSphereShapeD0Ev, .Lfunc_end5-_ZN13btSphereShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK21btConvexInternalShape15getLocalScalingEv,"axG",@progbits,_ZNK21btConvexInternalShape15getLocalScalingEv,comdat
	.weak	_ZNK21btConvexInternalShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape15getLocalScalingEv,@function
_ZNK21btConvexInternalShape15getLocalScalingEv: # @_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	24(%rdi), %rax
	retq
.Lfunc_end6:
	.size	_ZNK21btConvexInternalShape15getLocalScalingEv, .Lfunc_end6-_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_endproc

	.section	.text._ZNK13btSphereShape7getNameEv,"axG",@progbits,_ZNK13btSphereShape7getNameEv,comdat
	.weak	_ZNK13btSphereShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK13btSphereShape7getNameEv,@function
_ZNK13btSphereShape7getNameEv:          # @_ZNK13btSphereShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end7:
	.size	_ZNK13btSphereShape7getNameEv, .Lfunc_end7-_ZNK13btSphereShape7getNameEv
	.cfi_endproc

	.section	.text._ZN13btSphereShape9setMarginEf,"axG",@progbits,_ZN13btSphereShape9setMarginEf,comdat
	.weak	_ZN13btSphereShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN13btSphereShape9setMarginEf,@function
_ZN13btSphereShape9setMarginEf:         # @_ZN13btSphereShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 56(%rdi)
	retq
.Lfunc_end8:
	.size	_ZN13btSphereShape9setMarginEf, .Lfunc_end8-_ZN13btSphereShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK13btSphereShape9getMarginEv,"axG",@progbits,_ZNK13btSphereShape9getMarginEv,comdat
	.weak	_ZNK13btSphereShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK13btSphereShape9getMarginEv,@function
_ZNK13btSphereShape9getMarginEv:        # @_ZNK13btSphereShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	40(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	24(%rdi), %xmm0
	retq
.Lfunc_end9:
	.size	_ZNK13btSphereShape9getMarginEv, .Lfunc_end9-_ZNK13btSphereShape9getMarginEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,"axG",@progbits,_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,comdat
	.weak	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,@function
_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv: # @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end10:
	.size	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv, .Lfunc_end10-_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,"axG",@progbits,_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,comdat
	.weak	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,@function
_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3: # @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end11:
	.size	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3, .Lfunc_end11-_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end12:
	.size	__clang_call_terminate, .Lfunc_end12-__clang_call_terminate

	.type	_ZTV13btSphereShape,@object # @_ZTV13btSphereShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV13btSphereShape
	.p2align	3
_ZTV13btSphereShape:
	.quad	0
	.quad	_ZTI13btSphereShape
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN13btSphereShapeD0Ev
	.quad	_ZNK13btSphereShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN21btConvexInternalShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK13btSphereShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK13btSphereShape7getNameEv
	.quad	_ZN13btSphereShape9setMarginEf
	.quad	_ZNK13btSphereShape9getMarginEv
	.quad	_ZNK13btSphereShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK13btSphereShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK13btSphereShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.size	_ZTV13btSphereShape, 160

	.type	_ZTS13btSphereShape,@object # @_ZTS13btSphereShape
	.globl	_ZTS13btSphereShape
_ZTS13btSphereShape:
	.asciz	"13btSphereShape"
	.size	_ZTS13btSphereShape, 16

	.type	_ZTI13btSphereShape,@object # @_ZTI13btSphereShape
	.globl	_ZTI13btSphereShape
	.p2align	4
_ZTI13btSphereShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13btSphereShape
	.quad	_ZTI21btConvexInternalShape
	.size	_ZTI13btSphereShape, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"SPHERE"
	.size	.L.str, 7


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
