	.text
	.file	"jctrans.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	128                     # 0x80
	.quad	256                     # 0x100
.LCPI0_1:
	.quad	384                     # 0x180
	.quad	512                     # 0x200
.LCPI0_2:
	.quad	640                     # 0x280
	.quad	768                     # 0x300
.LCPI0_3:
	.quad	896                     # 0x380
	.quad	1024                    # 0x400
	.text
	.globl	jpeg_write_coefficients
	.p2align	4, 0x90
	.type	jpeg_write_coefficients,@function
jpeg_write_coefficients:                # @jpeg_write_coefficients
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	28(%r15), %eax
	cmpl	$100, %eax
	je	.LBB0_2
# BB#1:
	movq	(%r15), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%r15, %rdi
	callq	*(%rcx)
.LBB0_2:
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	jpeg_suppress_tables
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*32(%rax)
	movq	32(%r15), %rax
	movq	%r15, %rdi
	callq	*16(%rax)
	movl	$1, 48(%r15)
	movl	$1, %esi
	movq	%r15, %rdi
	callq	jinit_c_master_control
	cmpl	$0, 252(%r15)
	je	.LBB0_4
# BB#3:
	movq	(%r15), %rax
	movl	$1, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
	jmp	.LBB0_7
.LBB0_4:
	cmpl	$0, 300(%r15)
	je	.LBB0_6
# BB#5:
	movq	%r15, %rdi
	callq	jinit_phuff_encoder
	jmp	.LBB0_7
.LBB0_6:
	movq	%r15, %rdi
	callq	jinit_huff_encoder
.LBB0_7:                                # %transencode_master_selection.exit
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$120, %edx
	movq	%r15, %rdi
	callq	*(%rax)
	movq	%rax, %rbx
	movq	%rbx, 448(%r15)
	movq	$start_pass_coef, (%rbx)
	movq	$compress_output, 8(%rbx)
	movq	%r14, 32(%rbx)
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$1280, %edx             # imm = 0x500
	movq	%r15, %rdi
	callq	*8(%rax)
	movq	%rax, %r14
	movl	$1280, %esi             # imm = 0x500
	movq	%r14, %rdi
	callq	jzero_far
	movq	%r14, 40(%rbx)
	movd	%r14, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqa	.LCPI0_0(%rip), %xmm1   # xmm1 = [128,256]
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 48(%rbx)
	movdqa	.LCPI0_1(%rip), %xmm1   # xmm1 = [384,512]
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 64(%rbx)
	movdqa	.LCPI0_2(%rip), %xmm1   # xmm1 = [640,768]
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 80(%rbx)
	paddq	.LCPI0_3(%rip), %xmm0
	movdqu	%xmm0, 96(%rbx)
	addq	$1152, %r14             # imm = 0x480
	movq	%r14, 112(%rbx)
	movq	%r15, %rdi
	callq	jinit_marker_writer
	movq	8(%r15), %rax
	movq	%r15, %rdi
	callq	*48(%rax)
	movq	456(%r15), %rax
	movq	%r15, %rdi
	callq	*8(%rax)
	movl	$0, 296(%r15)
	movl	$103, 28(%r15)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	jpeg_write_coefficients, .Lfunc_end0-jpeg_write_coefficients
	.cfi_endproc

	.globl	jpeg_copy_critical_parameters
	.p2align	4, 0x90
	.type	jpeg_copy_critical_parameters,@function
jpeg_copy_critical_parameters:          # @jpeg_copy_critical_parameters
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 80
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%rsi, %rdx
	movq	%rdi, (%rsp)            # 8-byte Spill
	movl	28(%rdx), %eax
	cmpl	$100, %eax
	je	.LBB1_2
# BB#1:
	movq	(%rdx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	callq	*(%rcx)
	movq	%rbx, %rdx
.LBB1_2:
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	(%rsp), %rbx            # 8-byte Reload
	movups	40(%rbx), %xmm0
	movups	%xmm0, 40(%rdx)
	movq	%rdx, %rdi
	callq	jpeg_set_defaults
	movl	52(%rbx), %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	jpeg_set_colorspace
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	288(%rbx), %eax
	movl	%eax, 64(%rdi)
	movl	384(%rbx), %eax
	movl	%eax, 260(%rdi)
	movq	192(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB1_6
# BB#3:
	movq	88(%rdi), %rax
	testq	%rax, %rax
	jne	.LBB1_5
# BB#4:
	callq	jpeg_alloc_quant_table
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rax, 88(%rdi)
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	192(%rcx), %rcx
.LBB1_5:                                # %._crit_edge120
	movups	112(%rcx), %xmm0
	movups	%xmm0, 112(%rax)
	movups	96(%rcx), %xmm0
	movups	%xmm0, 96(%rax)
	movups	80(%rcx), %xmm0
	movups	%xmm0, 80(%rax)
	movups	64(%rcx), %xmm0
	movups	%xmm0, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	88(%rdi), %rax
	movl	$0, 128(%rax)
.LBB1_6:
	movq	(%rsp), %rax            # 8-byte Reload
	movq	200(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB1_10
# BB#7:
	movq	96(%rdi), %rax
	testq	%rax, %rax
	jne	.LBB1_9
# BB#8:
	callq	jpeg_alloc_quant_table
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rax, 96(%rdi)
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	200(%rcx), %rcx
.LBB1_9:                                # %._crit_edge121
	movups	112(%rcx), %xmm0
	movups	%xmm0, 112(%rax)
	movups	96(%rcx), %xmm0
	movups	%xmm0, 96(%rax)
	movups	80(%rcx), %xmm0
	movups	%xmm0, 80(%rax)
	movups	64(%rcx), %xmm0
	movups	%xmm0, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	96(%rdi), %rax
	movl	$0, 128(%rax)
.LBB1_10:
	movq	(%rsp), %rax            # 8-byte Reload
	movq	208(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB1_14
# BB#11:
	movq	104(%rdi), %rax
	testq	%rax, %rax
	jne	.LBB1_13
# BB#12:
	callq	jpeg_alloc_quant_table
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rax, 104(%rdi)
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	208(%rcx), %rcx
.LBB1_13:                               # %._crit_edge122
	movups	112(%rcx), %xmm0
	movups	%xmm0, 112(%rax)
	movups	96(%rcx), %xmm0
	movups	%xmm0, 96(%rax)
	movups	80(%rcx), %xmm0
	movups	%xmm0, 80(%rax)
	movups	64(%rcx), %xmm0
	movups	%xmm0, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	104(%rdi), %rax
	movl	$0, 128(%rax)
.LBB1_14:
	movq	(%rsp), %rax            # 8-byte Reload
	movq	216(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB1_18
# BB#15:
	movq	112(%rdi), %rax
	testq	%rax, %rax
	jne	.LBB1_17
# BB#16:
	callq	jpeg_alloc_quant_table
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rax, 112(%rdi)
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	216(%rcx), %rcx
.LBB1_17:                               # %._crit_edge123
	movups	112(%rcx), %xmm0
	movups	%xmm0, 112(%rax)
	movups	96(%rcx), %xmm0
	movups	%xmm0, 96(%rax)
	movups	80(%rcx), %xmm0
	movups	%xmm0, 80(%rax)
	movups	64(%rcx), %xmm0
	movups	%xmm0, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	112(%rdi), %rax
	movl	$0, 128(%rax)
.LBB1_18:
	movq	(%rsp), %rax            # 8-byte Reload
	movl	48(%rax), %eax
	movl	%eax, 68(%rdi)
	leal	-1(%rax), %ecx
	cmpl	$9, %ecx
	jbe	.LBB1_20
# BB#19:
	movq	(%rdi), %rcx
	movl	$24, 40(%rcx)
	movl	%eax, 44(%rcx)
	movl	$10, 48(%rcx)
	callq	*(%rcx)
	movq	8(%rsp), %rdi           # 8-byte Reload
	cmpl	$0, 68(%rdi)
	jle	.LBB1_30
.LBB1_20:                               # %.lr.ph
	movq	80(%rdi), %r12
	movq	(%rsp), %rax            # 8-byte Reload
	movq	296(%rax), %rbx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_21:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_26 Depth 2
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	(%rbx), %eax
	movl	%eax, (%r12)
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movl	12(%rbx), %eax
	movl	%eax, 12(%r12)
	movslq	16(%rbx), %rbp
	cmpq	$3, %rbp
	movl	%ebp, 16(%r12)
	ja	.LBB1_23
# BB#22:                                #   in Loop: Header=BB1_21 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	192(%rax,%rbp,8), %r14
	testq	%r14, %r14
	jne	.LBB1_24
.LBB1_23:                               # %._crit_edge124
                                        #   in Loop: Header=BB1_21 Depth=1
	movq	(%rdi), %rax
	movl	$51, 40(%rax)
	movl	%ebp, 44(%rax)
	callq	*(%rax)
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rsp), %rax            # 8-byte Reload
	movq	192(%rax,%rbp,8), %r14
.LBB1_24:                               #   in Loop: Header=BB1_21 Depth=1
	movq	80(%rbx), %r13
	testq	%r13, %r13
	je	.LBB1_29
# BB#25:                                # %.preheader.preheader
                                        #   in Loop: Header=BB1_21 Depth=1
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB1_26:                               # %.preheader
                                        #   Parent Loop BB1_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%r13,%r15,2), %eax
	cmpw	(%r14,%r15,2), %ax
	je	.LBB1_28
# BB#27:                                #   in Loop: Header=BB1_26 Depth=2
	movq	(%rdi), %rax
	movl	$43, 40(%rax)
	movl	%ebp, 44(%rax)
	callq	*(%rax)
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB1_28:                               #   in Loop: Header=BB1_26 Depth=2
	incq	%r15
	cmpq	$64, %r15
	jne	.LBB1_26
.LBB1_29:                               # %.loopexit
                                        #   in Loop: Header=BB1_21 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	incl	%eax
	addq	$96, %rbx
	addq	$96, %r12
	cmpl	68(%rdi), %eax
	jl	.LBB1_21
.LBB1_30:                               # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	jpeg_copy_critical_parameters, .Lfunc_end1-jpeg_copy_critical_parameters
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_pass_coef,@function
start_pass_coef:                        # @start_pass_coef
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -24
.Lcfi23:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	448(%rbx), %r14
	cmpl	$2, %esi
	movq	%r14, %rax
	je	.LBB2_2
# BB#1:
	movq	(%rbx), %rax
	movl	$4, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	448(%rbx), %rax
.LBB2_2:
	movl	$0, 16(%r14)
	movl	$1, %ecx
	cmpl	$1, 316(%rbx)
	jg	.LBB2_6
# BB#3:
	movl	312(%rbx), %edx
	decl	%edx
	movq	320(%rbx), %rcx
	cmpl	%edx, 16(%rax)
	jae	.LBB2_5
# BB#4:
	movl	12(%rcx), %ecx
	jmp	.LBB2_6
.LBB2_5:
	movl	72(%rcx), %ecx
.LBB2_6:                                # %start_iMCU_row.exit
	movl	%ecx, 28(%rax)
	movq	$0, 20(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	start_pass_coef, .Lfunc_end2-start_pass_coef
	.cfi_endproc

	.p2align	4, 0x90
	.type	compress_output,@function
compress_output:                        # @compress_output
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 288
.Lcfi31:
	.cfi_offset %rbx, -56
.Lcfi32:
	.cfi_offset %r12, -48
.Lcfi33:
	.cfi_offset %r13, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	448(%r12), %rbx
	movl	312(%r12), %r14d
	movl	352(%r12), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	316(%r12), %ecx
	testl	%ecx, %ecx
	jle	.LBB3_4
# BB#1:                                 # %.lr.ph133
	movl	%r14d, %r15d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movq	320(%r12,%r14,8), %rax
	movq	8(%r12), %r9
	movq	32(%rbx), %rcx
	movslq	4(%rax), %rdx
	movq	(%rcx,%rdx,8), %rsi
	movl	12(%rax), %ecx
	movl	16(%rbx), %edx
	imull	%ecx, %edx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	callq	*64(%r9)
	movq	%rax, 192(%rsp,%r14,8)
	incq	%r14
	movslq	316(%r12), %rcx
	cmpq	%rcx, %r14
	jl	.LBB3_2
# BB#3:
	movl	%r15d, %r14d
.LBB3_4:                                # %._crit_edge134
	movslq	24(%rbx), %r9
	movl	28(%rbx), %eax
	cmpl	%eax, %r9d
	jge	.LBB3_5
# BB#6:                                 # %.lr.ph129
	decl	12(%rsp)                # 4-byte Folded Spill
	decl	%r14d
	leaq	16(%rbx), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	20(%rbx), %r15d
	movl	352(%r12), %ecx
	movq	%r12, 64(%rsp)          # 8-byte Spill
.LBB3_7:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_9 Depth 2
                                        #       Child Loop BB3_11 Depth 3
                                        #         Child Loop BB3_14 Depth 4
                                        #           Child Loop BB3_18 Depth 5
                                        #         Child Loop BB3_25 Depth 4
                                        #           Child Loop BB3_32 Depth 5
                                        #           Child Loop BB3_29 Depth 5
                                        #           Child Loop BB3_41 Depth 5
	cmpl	%ecx, %r15d
	jae	.LBB3_44
# BB#8:                                 # %.preheader101.preheader
                                        #   in Loop: Header=BB3_7 Depth=1
	movq	%r9, 16(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB3_9:                                # %.preheader101
                                        #   Parent Loop BB3_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_11 Depth 3
                                        #         Child Loop BB3_14 Depth 4
                                        #           Child Loop BB3_18 Depth 5
                                        #         Child Loop BB3_25 Depth 4
                                        #           Child Loop BB3_32 Depth 5
                                        #           Child Loop BB3_29 Depth 5
                                        #           Child Loop BB3_41 Depth 5
	movslq	316(%r12), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	jle	.LBB3_22
# BB#10:                                # %.lr.ph122
                                        #   in Loop: Header=BB3_9 Depth=2
	xorl	%r11d, %r11d
	xorl	%edx, %edx
	movl	%r15d, 28(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB3_11:                               #   Parent Loop BB3_7 Depth=1
                                        #     Parent Loop BB3_9 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_14 Depth 4
                                        #           Child Loop BB3_18 Depth 5
                                        #         Child Loop BB3_25 Depth 4
                                        #           Child Loop BB3_32 Depth 5
                                        #           Child Loop BB3_29 Depth 5
                                        #           Child Loop BB3_41 Depth 5
	movq	320(%r12,%r11,8), %rbp
	leaq	52(%rbp), %rcx
	leaq	68(%rbp), %rax
	cmpl	12(%rsp), %r15d         # 4-byte Folded Reload
	cmovbq	%rcx, %rax
	movslq	56(%rbp), %rsi
	testq	%rsi, %rsi
	jle	.LBB3_21
# BB#12:                                # %.lr.ph117
                                        #   in Loop: Header=BB3_11 Depth=3
	movl	(%rax), %r8d
	movl	52(%rbp), %edi
	testl	%r8d, %r8d
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	jle	.LBB3_13
# BB#24:                                # %.lr.ph117.split.us.preheader
                                        #   in Loop: Header=BB3_11 Depth=3
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	movl	%edi, %ecx
	imull	%r15d, %ecx
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	leal	-1(%r8), %r13d
	incq	%r13
	leal	-1(%rdi), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movq	%r13, 56(%rsp)          # 8-byte Spill
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	andq	%rax, %r13
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	shlq	$6, %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	movq	%r11, 88(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_25:                               # %.lr.ph117.split.us
                                        #   Parent Loop BB3_7 Depth=1
                                        #     Parent Loop BB3_9 Depth=2
                                        #       Parent Loop BB3_11 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB3_32 Depth 5
                                        #           Child Loop BB3_29 Depth 5
                                        #           Child Loop BB3_41 Depth 5
	leaq	(%r12,%r9), %rax
	cmpl	%r14d, 36(%rsp)         # 4-byte Folded Reload
	jb	.LBB3_27
# BB#26:                                #   in Loop: Header=BB3_25 Depth=4
	movq	96(%rsp), %rcx          # 8-byte Reload
	movslq	72(%rcx), %rcx
	xorl	%ebp, %ebp
	cmpq	%rcx, %rax
	jge	.LBB3_35
.LBB3_27:                               # %.lr.ph.us
                                        #   in Loop: Header=BB3_25 Depth=4
	movq	192(%rsp,%r11,8), %rcx
	movq	(%rcx,%rax,8), %rcx
	movq	104(%rsp), %r10         # 8-byte Reload
	shlq	$7, %r10
	addq	%rcx, %r10
	movslq	%edx, %r15
	xorl	%eax, %eax
	cmpq	$3, 56(%rsp)            # 8-byte Folded Reload
	jbe	.LBB3_28
# BB#30:                                # %min.iters.checked
                                        #   in Loop: Header=BB3_25 Depth=4
	testq	%r13, %r13
	je	.LBB3_28
# BB#31:                                # %vector.body.preheader
                                        #   in Loop: Header=BB3_25 Depth=4
	movl	%r14d, %esi
	movq	%r13, %rax
	shlq	$7, %rax
	addq	%rax, %r10
	movq	80(%rsp), %rax          # 8-byte Reload
	leaq	384(%rcx,%rax,2), %rax
	leaq	128(%rsp), %rcx
	leaq	(%rcx,%r15,8), %rbp
	addq	%r13, %r15
	movq	%r13, %r14
	.p2align	4, 0x90
.LBB3_32:                               # %vector.body
                                        #   Parent Loop BB3_7 Depth=1
                                        #     Parent Loop BB3_9 Depth=2
                                        #       Parent Loop BB3_11 Depth=3
                                        #         Parent Loop BB3_25 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	leaq	-384(%rax), %rcx
	leaq	-256(%rax), %r9
	leaq	-128(%rax), %r11
	movd	%r9, %xmm0
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movd	%rax, %xmm0
	movd	%r11, %xmm2
	punpcklqdq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0]
	movdqu	%xmm1, -16(%rbp)
	movdqu	%xmm2, (%rbp)
	addq	$512, %rax              # imm = 0x200
	addq	$32, %rbp
	addq	$-4, %r14
	jne	.LBB3_32
# BB#33:                                # %middle.block
                                        #   in Loop: Header=BB3_25 Depth=4
	cmpq	%r13, 56(%rsp)          # 8-byte Folded Reload
	movl	%r13d, %eax
	movl	%esi, %r14d
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	88(%rsp), %r11          # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	je	.LBB3_34
	.p2align	4, 0x90
.LBB3_28:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB3_25 Depth=4
	leaq	112(%rsp,%r15,8), %rbp
	movl	%r8d, %ecx
	subl	%eax, %ecx
	.p2align	4, 0x90
.LBB3_29:                               # %scalar.ph
                                        #   Parent Loop BB3_7 Depth=1
                                        #     Parent Loop BB3_9 Depth=2
                                        #       Parent Loop BB3_11 Depth=3
                                        #         Parent Loop BB3_25 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	%r10, (%rbp)
	subq	$-128, %r10
	addq	$8, %rbp
	decl	%ecx
	jne	.LBB3_29
.LBB3_34:                               # %.preheader.us.loopexit
                                        #   in Loop: Header=BB3_25 Depth=4
	addl	%r8d, %edx
	movl	%r8d, %ebp
.LBB3_35:                               # %.preheader.us
                                        #   in Loop: Header=BB3_25 Depth=4
	movl	%edi, %eax
	subl	%ebp, %eax
	jle	.LBB3_36
# BB#37:                                # %.lr.ph111.us
                                        #   in Loop: Header=BB3_25 Depth=4
	movslq	%edx, %rdx
	testb	$1, %al
	jne	.LBB3_39
# BB#38:                                #   in Loop: Header=BB3_25 Depth=4
	movl	%ebp, %eax
	cmpl	%ebp, 32(%rsp)          # 4-byte Folded Reload
	jne	.LBB3_41
	jmp	.LBB3_36
	.p2align	4, 0x90
.LBB3_39:                               #   in Loop: Header=BB3_25 Depth=4
	movq	40(%rbx,%rdx,8), %rax
	movq	%rax, 112(%rsp,%rdx,8)
	movq	104(%rsp,%rdx,8), %rcx
	movzwl	(%rcx), %ecx
	movw	%cx, (%rax)
	incq	%rdx
	leal	1(%rbp), %eax
	cmpl	%ebp, 32(%rsp)          # 4-byte Folded Reload
	je	.LBB3_36
	.p2align	4, 0x90
.LBB3_41:                               #   Parent Loop BB3_7 Depth=1
                                        #     Parent Loop BB3_9 Depth=2
                                        #       Parent Loop BB3_11 Depth=3
                                        #         Parent Loop BB3_25 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	40(%rbx,%rdx,8), %rcx
	movq	%rcx, 112(%rsp,%rdx,8)
	movq	104(%rsp,%rdx,8), %rbp
	movzwl	(%rbp), %ebp
	movw	%bp, (%rcx)
	movq	48(%rbx,%rdx,8), %rcx
	movq	%rcx, 120(%rsp,%rdx,8)
	movq	112(%rsp,%rdx,8), %rbp
	movzwl	(%rbp), %ebp
	movw	%bp, (%rcx)
	addq	$2, %rdx
	addl	$2, %eax
	cmpl	%edi, %eax
	jl	.LBB3_41
.LBB3_36:                               # %._crit_edge.us
                                        #   in Loop: Header=BB3_25 Depth=4
	incq	%r12
	cmpq	%rsi, %r12
	jl	.LBB3_25
	jmp	.LBB3_21
	.p2align	4, 0x90
.LBB3_13:                               # %.lr.ph117.split.preheader
                                        #   in Loop: Header=BB3_11 Depth=3
	movl	%edi, %r8d
	andl	$1, %r8d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_14:                               # %.lr.ph117.split
                                        #   Parent Loop BB3_7 Depth=1
                                        #     Parent Loop BB3_9 Depth=2
                                        #       Parent Loop BB3_11 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB3_18 Depth 5
	testl	%edi, %edi
	jle	.LBB3_20
# BB#15:                                # %.lr.ph111
                                        #   in Loop: Header=BB3_14 Depth=4
	xorl	%ebp, %ebp
	testl	%r8d, %r8d
	movslq	%edx, %rdx
	je	.LBB3_17
# BB#16:                                #   in Loop: Header=BB3_14 Depth=4
	movq	40(%rbx,%rdx,8), %rbp
	movq	%rbp, 112(%rsp,%rdx,8)
	movq	104(%rsp,%rdx,8), %rax
	movzwl	(%rax), %eax
	movw	%ax, (%rbp)
	incq	%rdx
	movl	$1, %ebp
.LBB3_17:                               # %.prol.loopexit
                                        #   in Loop: Header=BB3_14 Depth=4
	cmpl	$1, %edi
	je	.LBB3_19
	.p2align	4, 0x90
.LBB3_18:                               #   Parent Loop BB3_7 Depth=1
                                        #     Parent Loop BB3_9 Depth=2
                                        #       Parent Loop BB3_11 Depth=3
                                        #         Parent Loop BB3_14 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	40(%rbx,%rdx,8), %rax
	movq	%rax, 112(%rsp,%rdx,8)
	movq	104(%rsp,%rdx,8), %rsi
	movzwl	(%rsi), %esi
	movw	%si, (%rax)
	movq	48(%rbx,%rdx,8), %rax
	movq	%rax, 120(%rsp,%rdx,8)
	movq	112(%rsp,%rdx,8), %rsi
	movzwl	(%rsi), %esi
	movw	%si, (%rax)
	addq	$2, %rdx
	addl	$2, %ebp
	cmpl	%edi, %ebp
	jl	.LBB3_18
.LBB3_19:                               # %._crit_edge.loopexit
                                        #   in Loop: Header=BB3_14 Depth=4
	movq	48(%rsp), %rsi          # 8-byte Reload
.LBB3_20:                               # %._crit_edge
                                        #   in Loop: Header=BB3_14 Depth=4
	incl	%ecx
	cmpl	%esi, %ecx
	jl	.LBB3_14
.LBB3_21:                               # %._crit_edge118
                                        #   in Loop: Header=BB3_11 Depth=3
	incq	%r11
	cmpq	72(%rsp), %r11          # 8-byte Folded Reload
	movq	64(%rsp), %r12          # 8-byte Reload
	movl	28(%rsp), %r15d         # 4-byte Reload
	jl	.LBB3_11
.LBB3_22:                               # %._crit_edge123
                                        #   in Loop: Header=BB3_9 Depth=2
	movq	488(%r12), %rax
	movq	%r12, %rdi
	leaq	112(%rsp), %rsi
	callq	*8(%rax)
	testl	%eax, %eax
	je	.LBB3_23
# BB#42:                                #   in Loop: Header=BB3_9 Depth=2
	incl	%r15d
	movl	352(%r12), %ecx
	cmpl	%ecx, %r15d
	movq	16(%rsp), %r9           # 8-byte Reload
	jb	.LBB3_9
# BB#43:                                # %._crit_edge125.loopexit
                                        #   in Loop: Header=BB3_7 Depth=1
	movl	28(%rbx), %eax
.LBB3_44:                               # %._crit_edge125
                                        #   in Loop: Header=BB3_7 Depth=1
	movl	$0, 20(%rbx)
	incq	%r9
	movslq	%eax, %rdx
	xorl	%r15d, %r15d
	cmpq	%rdx, %r9
	jl	.LBB3_7
# BB#45:                                # %._crit_edge130.loopexit
	movl	316(%r12), %ecx
	movq	40(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB3_46
.LBB3_5:                                # %._crit_edge134.._crit_edge130_crit_edge
	addq	$16, %rbx
.LBB3_46:                               # %._crit_edge130
	incl	(%rbx)
	movq	448(%r12), %rdx
	movl	$1, %eax
	cmpl	$1, %ecx
	movl	$1, %ecx
	jg	.LBB3_50
# BB#47:
	movl	312(%r12), %esi
	decl	%esi
	movq	320(%r12), %rcx
	cmpl	%esi, 16(%rdx)
	jae	.LBB3_49
# BB#48:
	movl	12(%rcx), %ecx
	jmp	.LBB3_50
.LBB3_23:
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, 24(%rbx)
	movl	%r15d, 20(%rbx)
	xorl	%eax, %eax
	jmp	.LBB3_51
.LBB3_49:
	movl	72(%rcx), %ecx
.LBB3_50:                               # %start_iMCU_row.exit
	movl	%ecx, 28(%rdx)
	movq	$0, 20(%rdx)
.LBB3_51:
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	compress_output, .Lfunc_end3-compress_output
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
