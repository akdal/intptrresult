	.text
	.file	"libclamav_entconv.bc"
	.globl	entity_norm
	.p2align	4, 0x90
	.type	entity_norm,@function
entity_norm:                            # @entity_norm
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	24(%rdi), %r14
	movq	%rbx, %rdi
	callq	strlen
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	hashtab_find
	testq	%rax, %rax
	je	.LBB0_6
# BB#1:
	cmpq	$0, (%rax)
	je	.LBB0_6
# BB#2:
	movq	8(%rax), %r14
	cmpl	$62, %r14d
	je	.LBB0_8
# BB#3:
	cmpl	$60, %r14d
	jne	.LBB0_10
# BB#4:
	movl	$.L.str, %edi
	jmp	.LBB0_9
.LBB0_8:
	movl	$.L.str.1, %edi
.LBB0_9:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	cli_strdup              # TAILCALL
.LBB0_10:
	cmpl	$126, %r14d
	jg	.LBB0_13
# BB#11:
	movl	$2, %edi
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_6
# BB#12:
	movb	%r14b, (%rbx)
	movb	$0, 1(%rbx)
	jmp	.LBB0_7
.LBB0_13:
	cmpl	$160, %r14d
	jne	.LBB0_15
# BB#14:
	movl	$.L.str.2, %edi
	jmp	.LBB0_9
.LBB0_15:
	movl	$10, %edi
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_6
# BB#16:
	movl	$9, %esi
	movl	$.L.str.3, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%r14d, %ecx
	callq	snprintf
	movb	$0, 9(%rbx)
	jmp	.LBB0_7
.LBB0_6:
	xorl	%ebx, %ebx
.LBB0_7:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	entity_norm, .Lfunc_end0-entity_norm
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.zero	16
	.text
	.globl	init_entity_converter
	.p2align	4, 0x90
	.type	init_entity_converter,@function
init_entity_converter:                  # @init_entity_converter
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	cmpq	$31, %r14
	ja	.LBB1_2
# BB#1:
	movl	$.L.str.4, %edi
	movl	$32, %edx
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_warnmsg
	movl	$-111, %ebp
	jmp	.LBB1_11
.LBB1_2:
	testq	%rbx, %rbx
	je	.LBB1_7
# BB#3:
	movl	$.L.str.5, %edi
	callq	cli_strdup
	movq	%rax, (%rbx)
	movq	$.L.str.6, 8(%rbx)
	movb	$0, 35(%rbx)
	movq	$0, 56(%rbx)
	movb	$0, 34(%rbx)
	movl	$0, 36(%rbx)
	movb	$0, 64(%rbx)
	movq	%r14, 48(%rbx)
	movl	$0, 16(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 104(%rbx)
	movq	%r14, %rdi
	callq	cli_malloc
	movq	%rax, 96(%rbx)
	movl	$-114, %ebp
	testq	%rax, %rax
	je	.LBB1_11
# BB#4:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 128(%rbx)
	movq	%r14, %rdi
	callq	cli_malloc
	movq	%rax, 120(%rbx)
	testq	%rax, %rax
	je	.LBB1_8
# BB#5:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 152(%rbx)
	movq	%r14, %rdi
	callq	cli_malloc
	movq	%rax, 144(%rbx)
	testq	%rax, %rax
	je	.LBB1_9
# BB#6:
	movq	$entities_htable, 24(%rbx)
	movl	$0, 168(%rbx)
	xorl	%ebp, %ebp
	jmp	.LBB1_11
.LBB1_7:
	movl	$-111, %ebp
	jmp	.LBB1_11
.LBB1_8:
	movq	96(%rbx), %rdi
	jmp	.LBB1_10
.LBB1_9:
	movq	96(%rbx), %rdi
	callq	free
	movq	120(%rbx), %rdi
.LBB1_10:
	callq	free
.LBB1_11:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	init_entity_converter, .Lfunc_end1-init_entity_converter
	.cfi_endproc

	.globl	process_encoding_set
	.p2align	4, 0x90
	.type	process_encoding_set,@function
process_encoding_set:                   # @process_encoding_set
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 64
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	movq	%rbx, %rdx
	movl	%ebp, %ecx
	callq	cli_dbgmsg
	movl	$.L.str.6, %eax
	cmpq	%rax, %rbx
	je	.LBB2_32
# BB#1:
	movl	16(%r15), %eax
	cmpl	$1, %eax
	je	.LBB2_32
# BB#2:
	cmpl	$3, %ebp
	jne	.LBB2_4
# BB#3:
	cmpl	$2, %eax
	jne	.LBB2_4
.LBB2_32:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_4:
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r12
	leaq	1(%r12), %rdi
	callq	cli_malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB2_5
# BB#6:
	movq	%r12, (%rsp)            # 8-byte Spill
	movzbl	(%rbx), %r12d
	testq	%r12, %r12
	je	.LBB2_10
# BB#7:                                 # %.lr.ph.i
	callq	__ctype_toupper_loc
	movq	%rax, %r13
	movq	(%r13), %rax
	movb	(%rax,%r12,4), %al
	movb	%al, (%r14)
	movq	%rbx, %rdi
	callq	strlen
	cmpq	$2, %rax
	jb	.LBB2_10
# BB#8:                                 # %._crit_edge26.i.preheader
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB2_9:                                # %._crit_edge26.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movzbl	(%rbx,%r12), %ecx
	movzbl	(%rax,%rcx,4), %eax
	movb	%al, (%r14,%r12)
	incq	%r12
	movq	%rbx, %rdi
	callq	strlen
	cmpq	%rax, %r12
	jb	.LBB2_9
.LBB2_10:                               # %._crit_edge.i
	movq	(%rsp), %rax            # 8-byte Reload
	movb	$0, (%r14,%rax)
	cmpl	$5, %ebp
	je	.LBB2_12
	jmp	.LBB2_31
.LBB2_5:
	xorl	%r14d, %r14d
	cmpl	$5, %ebp
	jne	.LBB2_31
.LBB2_12:
	movq	(%r15), %rbx
	movl	$.L.str.1682, %eax
	cmpq	%rax, %rbx
	je	.LBB2_13
# BB#15:
	movl	$.L.str.1666, %eax
	cmpq	%rax, %rbx
	je	.LBB2_16
# BB#17:
	movq	%rbx, %rdi
	callq	strlen
	movl	$aliases_htable, %edi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	hashtab_find
	testq	%rax, %rax
	je	.LBB2_20
# BB#18:
	cmpq	$0, (%rax)
	je	.LBB2_20
# BB#19:                                # %thread-pre-split.i
	movq	8(%rax), %rax
	cmpl	$7, %eax
	jbe	.LBB2_14
.LBB2_20:
	movl	$1, %ebp
	jmp	.LBB2_21
.LBB2_13:
	movl	$4, %eax
	jmp	.LBB2_14
.LBB2_16:
	movl	$5, %eax
.LBB2_14:                               # %switch.lookup.i
	cltq
	movq	.Lswitch.table(,%rax,8), %rbp
.LBB2_21:                               # %encoding_bytes.exit
	movl	$.L.str.1682, %eax
	cmpq	%rax, %r14
	je	.LBB2_22
# BB#24:
	movl	$.L.str.1666, %eax
	cmpq	%rax, %r14
	je	.LBB2_25
# BB#26:
	movq	%r14, %rdi
	callq	strlen
	movl	$aliases_htable, %edi
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	hashtab_find
	testq	%rax, %rax
	je	.LBB2_29
# BB#27:
	cmpq	$0, (%rax)
	je	.LBB2_29
# BB#28:                                # %thread-pre-split.i29
	movq	8(%rax), %rax
	cmpl	$7, %eax
	jbe	.LBB2_23
.LBB2_29:
	movl	$1, %r8d
	cmpq	%r8, %rbp
	je	.LBB2_31
	jmp	.LBB2_33
.LBB2_22:
	movl	$4, %eax
	jmp	.LBB2_23
.LBB2_25:
	movl	$5, %eax
.LBB2_23:                               # %switch.lookup.i34
	cltq
	movq	.Lswitch.table(,%rax,8), %r8
	cmpq	%r8, %rbp
	jne	.LBB2_33
.LBB2_31:                               # %normalize_encoding.exit._crit_edge
	movq	(%r15), %rdi
	callq	free
	movq	%r14, (%r15)
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	movq	%r14, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	cli_dbgmsg              # TAILCALL
.LBB2_33:
	movq	(%r15), %rsi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	movq	%r14, %rcx
	callq	cli_dbgmsg
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end2:
	.size	process_encoding_set, .Lfunc_end2-process_encoding_set
	.cfi_endproc

	.globl	entity_norm_done
	.p2align	4, 0x90
	.type	entity_norm_done,@function
entity_norm_done:                       # @entity_norm_done
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 16
.Lcfi25:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_2
# BB#1:
	callq	free
	movq	$0, (%rbx)
.LBB3_2:
	movq	$0, 48(%rbx)
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_4
# BB#3:
	callq	free
	movq	$0, 96(%rbx)
.LBB3_4:
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_6
# BB#5:
	callq	free
	movq	$0, 120(%rbx)
.LBB3_6:
	movq	144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_8
# BB#7:
	callq	free
	movq	$0, 144(%rbx)
.LBB3_8:                                # %encoding_norm_done.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end3:
	.size	entity_norm_done, .Lfunc_end3-entity_norm_done
	.cfi_endproc

	.globl	encoding_norm_readline
	.p2align	4, 0x90
	.type	encoding_norm_readline,@function
encoding_norm_readline:                 # @encoding_norm_readline
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 160
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rdx, %r13
	movq	%rsi, %r14
	testq	%rdi, %rdi
	je	.LBB4_12
# BB#1:
	movq	120(%rdi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB4_12
# BB#2:
	cmpq	$2, %rcx
	jb	.LBB4_12
# BB#3:
	movq	96(%rdi), %rbp
	testq	%rbp, %rbp
	je	.LBB4_12
# BB#4:
	movq	48(%rdi), %r12
	movq	104(%rdi), %rsi
	movq	112(%rdi), %rbx
	movq	%rsi, %r15
	subq	%rbx, %r15
	movq	%r12, 16(%rsp)          # 8-byte Spill
	subq	%r15, %r12
	cmpq	%rcx, %r12
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	cmovaq	%rcx, %r12
	movq	128(%rdi), %rax
	movq	136(%rdi), %rdx
	xorl	%ecx, %ecx
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	subq	%rdx, %rax
	cmovgeq	%rax, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	152(%rdi), %rax
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	subq	160(%rdi), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	movq	%rsi, %rdx
	subq	%rbx, %rdx
	je	.LBB4_6
# BB#5:
	leaq	(%rbp,%rbx), %rsi
	movq	%rbp, %rdi
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	callq	memmove
	movq	32(%rsp), %rdx          # 8-byte Reload
.LBB4_6:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	$0, 112(%rax)
	testq	%r13, %r13
	je	.LBB4_13
# BB#7:
	movq	8(%r13), %r10
	movq	16(%r13), %rax
	movq	%r10, %r15
	subq	%rax, %r15
	jle	.LBB4_20
# BB#8:
	movslq	%r12d, %rcx
	leaq	(%rax,%rcx), %r9
	cmpq	%r9, %r10
	cmovgq	%rcx, %r15
	movq	(%r13), %r8
	leaq	(%r15,%rax), %rcx
	movq	%rcx, 16(%r13)
	testq	%r15, %r15
	je	.LBB4_22
# BB#9:                                 # %.lr.ph68.preheader.i
	leaq	2(%r8,%rax), %rdi
	movq	88(%rsp), %rcx          # 8-byte Reload
	addq	$2, %rcx
	subq	%rbx, %rcx
	addq	%rcx, %rbp
	xorl	%esi, %esi
	xorl	%ebx, %ebx
	movq	8(%rsp), %r11           # 8-byte Reload
	.p2align	4, 0x90
.LBB4_10:                               # %.lr.ph68.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-2(%rdi,%rbx), %ecx
	cmpb	$10, %cl
	je	.LBB4_23
# BB#11:                                #   in Loop: Header=BB4_10 Depth=1
	movb	%cl, -2(%rbp,%rbx)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB4_10
	jmp	.LBB4_25
.LBB4_12:
	xorl	%eax, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_13:
	movq	%rdx, %rbx
	testq	%r14, %r14
	movq	56(%rsp), %r13          # 8-byte Reload
	je	.LBB4_21
# BB#14:
	addq	%r15, %rbp
	movslq	%r12d, %rdx
	movl	$1, %esi
	movq	%rbp, %rdi
	movq	%r14, %rcx
	callq	fread
	movq	%rax, %r15
	movq	%r14, %rdi
	callq	ferror
	testl	%eax, %eax
	je	.LBB4_16
# BB#15:
	movl	$.L.str.1684, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB4_16:                               # %.preheader.i
	testq	%r15, %r15
	movq	8(%rsp), %r11           # 8-byte Reload
	je	.LBB4_26
# BB#17:                                # %.lr.ph.i.preheader
	xorl	%eax, %eax
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB4_18:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$10, (%rbp,%rax)
	je	.LBB4_27
# BB#19:                                #   in Loop: Header=BB4_18 Depth=1
	incq	%rax
	cmpq	%r15, %rax
	jb	.LBB4_18
	jmp	.LBB4_28
.LBB4_20:
	xorl	%r15d, %r15d
	movq	8(%rsp), %r11           # 8-byte Reload
	movq	56(%rsp), %r13          # 8-byte Reload
	jmp	.LBB4_28
.LBB4_21:
	xorl	%r15d, %r15d
	movl	$.L.str.1683, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	8(%rsp), %r11           # 8-byte Reload
	movq	%rbx, %rdx
	jmp	.LBB4_28
.LBB4_22:
	xorl	%esi, %esi
	movq	8(%rsp), %r11           # 8-byte Reload
	jmp	.LBB4_25
.LBB4_23:                               # %.critedge.i
	movq	%r15, %rsi
	subq	%rbx, %rsi
	cmpq	$4, %rsi
	jb	.LBB4_25
# BB#24:
	addq	%rax, %r8
	movb	$10, -2(%rbp,%rbx)
	movb	1(%rbx,%r8), %cl
	movb	%cl, -1(%rbx,%rbp)
	movb	2(%rbx,%r8), %cl
	movb	%cl, (%rbx,%rbp)
	movq	$-4, %rsi
	subq	%rax, %rsi
	notq	%r10
	notq	%r9
	cmpq	%r9, %r10
	cmovgeq	%r10, %r9
	subq	%r9, %rsi
	subq	%rbx, %rsi
.LBB4_25:                               # %.critedge.thread.i
	subq	%rsi, 16(%r13)
	subq	%rsi, %r15
	movq	56(%rsp), %r13          # 8-byte Reload
	jmp	.LBB4_28
.LBB4_26:
	xorl	%r15d, %r15d
	movq	%rbx, %rdx
	jmp	.LBB4_28
.LBB4_27:
	addq	$3, %rax
	cmpq	%r15, %rax
	cmovbeq	%rax, %r15
.LBB4_28:                               # %read_raw.exit
	addq	%rdx, %r15
	movq	%r15, 104(%r11)
	testq	%r13, %r13
	je	.LBB4_30
# BB#29:
	movq	120(%r11), %rdi
	movq	136(%r11), %rsi
	addq	%rdi, %rsi
	movq	%r13, %rdx
	callq	memmove
	movq	8(%rsp), %r11           # 8-byte Reload
	movq	104(%r11), %r15
.LBB4_30:
	movq	$0, 136(%r11)
	movq	96(%r11), %r14
	cmpq	$4, %r15
	jl	.LBB4_90
# BB#31:
	movb	35(%r11), %al
	testb	%al, %al
	jne	.LBB4_90
# BB#32:
	movl	(%r14), %ebp
	movl	%ebp, 40(%r11)
	movl	%ebp, %ebx
	shrl	$8, %ebx
	movl	%ebp, %edi
	shrl	$16, %edi
	movl	%ebp, %edx
	shrl	$24, %edx
	movl	$.L.str.6, %esi
	movb	$4, %r8b
	xorl	%ecx, %ecx
	movl	%ebp, %eax
	addb	$17, %al
	cmpb	$17, %al
	ja	.LBB4_36
# BB#33:
	movzbl	%al, %eax
	jmpq	*.LJTI4_0(,%rax,8)
.LBB4_34:
	cmpb	$-69, %bl
	jne	.LBB4_79
# BB#35:
	cmpb	$-65, %dil
	sete	%cl
	movl	$.L.str.1679, %eax
	movl	$.L.str.6, %esi
	cmoveq	%rax, %rsi
	jmp	.LBB4_81
.LBB4_36:
	cmpb	$60, %bpl
	je	.LBB4_57
# BB#37:
	cmpb	$76, %bpl
	jne	.LBB4_81
# BB#38:
	movl	$.L.str.6, %esi
	xorl	%ecx, %ecx
	cmpb	$111, %bl
	jne	.LBB4_81
# BB#39:
	cmpb	$-89, %dil
	jne	.LBB4_81
# BB#40:
	cmpb	$-108, %dl
	movl	$.L.str.1685, %eax
	movl	$.L.str.6, %esi
	cmoveq	%rax, %rsi
	movb	$1, %r8b
	je	.LBB4_80
# BB#41:
	movb	$4, %r8b
	jmp	.LBB4_80
.LBB4_42:
	cmpb	$-1, %bl
	jne	.LBB4_79
# BB#43:
	orb	%dl, %dil
	movl	$.L.str.1666, %eax
	movl	$.L.str.1668, %esi
	cmoveq	%rax, %rsi
	movb	$4, %r8b
	je	.LBB4_45
# BB#44:
	movb	$2, %r8b
.LBB4_45:
	movb	$1, %cl
	jmp	.LBB4_81
.LBB4_46:
	cmpb	$-2, %bl
	jne	.LBB4_79
# BB#47:
	orb	%dl, %dil
	movl	$.L.str.1674, %eax
	movl	$.L.str.1664, %esi
	cmoveq	%rax, %rsi
	movb	$4, %r8b
	je	.LBB4_49
# BB#48:
	movb	$2, %r8b
.LBB4_49:
	movb	$1, %cl
	jmp	.LBB4_81
.LBB4_50:
	cmpb	$60, %bl
	je	.LBB4_62
# BB#51:
	testb	%bl, %bl
	jne	.LBB4_79
# BB#52:
	movl	$.L.str.6, %esi
	xorl	%ecx, %ecx
	testb	%dil, %dil
	jns	.LBB4_69
# BB#53:
	cmpb	$-2, %dil
	je	.LBB4_75
# BB#54:
	cmpb	$-1, %dil
	jne	.LBB4_81
# BB#55:
	cmpb	$-2, %dl
	jne	.LBB4_79
# BB#56:
	movl	$.L.str.1682, %esi
	movb	$1, %cl
	jmp	.LBB4_81
.LBB4_57:
	cmpb	$63, %bl
	je	.LBB4_66
# BB#58:
	testb	%bl, %bl
	jne	.LBB4_79
# BB#59:
	cmpb	$63, %dil
	je	.LBB4_73
# BB#60:
	testb	%dil, %dil
	jne	.LBB4_79
# BB#61:
	testb	%dl, %dl
	movl	$.L.str.1674, %eax
	jmp	.LBB4_72
.LBB4_62:
	testb	%dil, %dil
	jne	.LBB4_79
# BB#63:
	testb	%dl, %dl
	je	.LBB4_188
# BB#64:
	cmpb	$63, %dl
	jne	.LBB4_79
# BB#65:
	movl	$.L.str.1668, %esi
	movb	$2, %r8b
	jmp	.LBB4_80
.LBB4_66:
	cmpb	$120, %dil
	jne	.LBB4_79
# BB#67:
	cmpb	$109, %dl
	movl	$.L.str.5, %eax
	movl	$.L.str.6, %esi
	cmoveq	%rax, %rsi
	movb	$1, %r8b
	je	.LBB4_80
# BB#68:
	movb	$4, %r8b
	jmp	.LBB4_80
.LBB4_69:
	je	.LBB4_77
# BB#70:
	cmpb	$60, %dil
	jne	.LBB4_81
# BB#71:
	testb	%dl, %dl
	movl	$.L.str.1682, %eax
.LBB4_72:                               # %process_bom.exit
	movl	$.L.str.6, %esi
	cmoveq	%rax, %rsi
	jmp	.LBB4_80
.LBB4_73:
	testb	%dl, %dl
	movl	$.L.str.1664, %eax
	movl	$.L.str.6, %esi
	cmoveq	%rax, %rsi
	movb	$2, %r8b
	je	.LBB4_80
# BB#74:
	movb	$4, %r8b
	jmp	.LBB4_80
.LBB4_75:
	cmpb	$-1, %dl
	jne	.LBB4_79
# BB#76:
	movl	$.L.str.1665, %esi
	movb	$1, %cl
	jmp	.LBB4_81
.LBB4_77:
	cmpb	$60, %dl
	jne	.LBB4_79
# BB#78:
	movl	$.L.str.1665, %esi
	jmp	.LBB4_80
.LBB4_79:                               # %.fold.split.i
	movl	$.L.str.6, %esi
.LBB4_80:                               # %process_bom.exit
	xorl	%ecx, %ecx
.LBB4_81:                               # %process_bom.exit
	movq	%rsi, 8(%r11)
	movb	%r8b, 33(%r11)
	movb	%cl, 32(%r11)
	xorl	%edx, %edx
	testb	%cl, %cl
	sete	%dl
	orl	$2, %edx
	movq	%r11, %rdi
	callq	process_encoding_set
	movq	8(%rsp), %r11           # 8-byte Reload
	cmpb	$0, 32(%r11)
	je	.LBB4_89
# BB#82:
	movb	33(%r11), %al
	cmpb	$4, %al
	je	.LBB4_87
# BB#83:
	cmpb	$2, %al
	je	.LBB4_88
# BB#84:
	cmpb	$1, %al
	jne	.LBB4_89
# BB#85:
	movl	$.L.str.1679, %eax
	cmpq	%rax, 8(%r11)
	jne	.LBB4_89
# BB#86:
	addq	$3, %r14
	addq	$-3, %r15
	jmp	.LBB4_89
.LBB4_87:
	addq	$4, %r14
	addq	$-4, %r15
	jmp	.LBB4_89
.LBB4_88:
	addq	$2, %r14
	addq	$-2, %r15
.LBB4_89:                               # %output_first.exit
	incb	35(%r11)
.LBB4_90:
	movl	%r15d, %edi
	andl	$3, %edi
	movq	%r15, %rbp
	subq	%rdi, %rbp
	andb	$3, %r15b
	je	.LBB4_93
# BB#91:
	testq	%rbp, %rbp
	jne	.LBB4_93
# BB#92:                                # %.preheader242
	movl	$4, %ebp
	movl	$4, %edx
	subq	%rdi, %rdx
	addq	%r14, %rdi
	xorl	%esi, %esi
	callq	memset
	movq	8(%rsp), %r11           # 8-byte Reload
	movb	$-4, %r15b
.LBB4_93:                               # %.loopexit243
	movq	(%r11), %rax
	testq	%rax, %rax
	movl	$.L.str.5, %edi
	cmovneq	%rax, %rdi
	movq	%r11, %rbx
	callq	iconv_open_cached
	cmpq	$-1, %rax
	jne	.LBB4_95
# BB#94:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	movl	$.L.str.5, %eax
	cmoveq	%rax, %rsi
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	(%rbx), %rdi
	callq	free
	movl	$.L.str.5, %edi
	callq	cli_strdup
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	callq	iconv_open_cached
	cmpq	$-1, %rax
	je	.LBB4_103
.LBB4_95:
	movq	64(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%r13), %rbx
	xorl	%esi, %esi
	testq	%rbp, %rbp
	je	.LBB4_101
# BB#96:
	movq	16(%rsp), %rdx          # 8-byte Reload
	subq	%r13, %rdx
	movq	8(%rsp), %r11           # 8-byte Reload
	movq	48(%r11), %rcx
	shrq	%rcx
	cmpq	%rcx, %rdx
	movq	%rdx, %rcx
	jbe	.LBB4_102
# BB#97:
	cmpq	%rcx, %rbp
	movq	%rcx, %rsi
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	cmovaq	%rsi, %rbp
	xorl	%r13d, %r13d
	subq	8(%rax), %r13
	andq	%rbp, %r13
	movl	(%rax), %eax
	cmpq	$10, %rax
	ja	.LBB4_179
# BB#98:
	jmpq	*.LJTI4_1(,%rax,8)
.LBB4_99:                               # %.preheader.i175
	testq	%r13, %r13
	je	.LBB4_179
# BB#100:                               # %.lr.ph.i176.preheader
	xorl	%ebp, %ebp
	jmp	.LBB4_105
.LBB4_101:
	xorl	%ebp, %ebp
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	8(%rsp), %r11           # 8-byte Reload
	jmp	.LBB4_203
.LBB4_102:
	movq	24(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB4_203
.LBB4_103:
	addq	$96, %rbx
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rbx, %r11
	jmp	.LBB4_242
.LBB4_104:                              #   in Loop: Header=BB4_105 Depth=1
	movzbl	1(%r14,%rbp), %ecx
	movq	%rbp, %rax
	shrq	%rax
	movb	%cl, (%rbx,%rax)
	movzbl	(%r14,%rbp), %ecx
	jmp	.LBB4_108
.LBB4_105:                              # %.lr.ph.i176
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, 2(%r14,%rbp)
	jne	.LBB4_107
# BB#106:                               #   in Loop: Header=BB4_105 Depth=1
	cmpb	$0, 3(%r14,%rbp)
	je	.LBB4_104
.LBB4_107:                              #   in Loop: Header=BB4_105 Depth=1
	movl	$.L.str.1693, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	8(%rsp), %r11           # 8-byte Reload
	movq	%rbp, %rax
	shrq	%rax
	movb	$-1, (%rbx,%rax)
	movb	$-1, %cl
.LBB4_108:                              #   in Loop: Header=BB4_105 Depth=1
	orq	$1, %rax
	movb	%cl, (%rbx,%rax)
	addq	$4, %rbp
	cmpq	%r13, %rbp
	jb	.LBB4_105
	jmp	.LBB4_179
.LBB4_109:                              # %.preheader5.i
	testq	%r13, %r13
	je	.LBB4_179
# BB#110:                               # %.lr.ph28.i.preheader
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, %rax
	cmovlq	%rcx, %rax
	incq	%rax
	subq	%rcx, %rax
	addq	%rax, %rdi
	xorl	%eax, %eax
.LBB4_111:                              # %.lr.ph28.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	1(%r14,%rax), %ecx
	movb	%cl, -1(%rdi,%rax)
	movzbl	(%r14,%rax), %ecx
	movb	%cl, (%rdi,%rax)
	addq	$2, %rax
	cmpq	%r13, %rax
	jb	.LBB4_111
	jmp	.LBB4_179
.LBB4_112:
	movq	16(%rsp), %rcx          # 8-byte Reload
	shrq	%rcx
	movq	32(%rsp), %rbp          # 8-byte Reload
	cmpq	%rcx, %rbp
	movq	%rbp, %rax
	cmovaq	%rcx, %rax
	testq	%rax, %rax
	movq	56(%rsp), %r13          # 8-byte Reload
	je	.LBB4_200
# BB#113:                               # %.lr.ph30.i.preheader
	cmpq	$16, %rax
	jae	.LBB4_152
# BB#114:
	xorl	%edx, %edx
	jmp	.LBB4_198
.LBB4_115:
	movq	%r13, %rax
	shrq	%rax
	je	.LBB4_179
# BB#116:                               # %.lr.ph21.i.preheader
	leaq	-1(%rax), %rcx
	movq	%rcx, %rdx
	shrq	%rdx
	btl	$1, %ecx
	jb	.LBB4_165
# BB#117:                               # %.lr.ph21.i.prol
	movw	$-1, %cx
	cmpw	$0, (%r14)
	jne	.LBB4_119
# BB#118:
	movzwl	2(%r14), %ecx
.LBB4_119:
	movw	%cx, (%rbx)
	movl	$2, %ecx
	testq	%rdx, %rdx
	jne	.LBB4_166
	jmp	.LBB4_179
.LBB4_120:
	movq	%r13, %rax
	shrq	%rax
	je	.LBB4_179
# BB#121:                               # %.lr.ph23.i.preheader
	leaq	-1(%rax), %rcx
	movq	%rcx, %rdx
	shrq	%rdx
	btl	$1, %ecx
	jb	.LBB4_172
# BB#122:                               # %.lr.ph23.i.prol
	movw	$-1, %cx
	cmpw	$0, 2(%r14)
	jne	.LBB4_124
# BB#123:
	movzwl	(%r14), %ecx
.LBB4_124:
	movw	%cx, (%rbx)
	movl	$2, %ecx
	testq	%rdx, %rdx
	jne	.LBB4_173
	jmp	.LBB4_179
.LBB4_125:                              # %.preheader3.i
	testq	%r13, %r13
	je	.LBB4_179
# BB#126:                               # %.lr.ph26.i.preheader
	xorl	%eax, %eax
	jmp	.LBB4_128
.LBB4_127:                              #   in Loop: Header=BB4_128 Depth=1
	movzbl	3(%r14,%rax), %edx
	movq	%rax, %rcx
	shrq	%rcx
	movb	%dl, (%rbx,%rcx)
	movzbl	2(%r14,%rax), %edx
	jmp	.LBB4_131
.LBB4_128:                              # %.lr.ph26.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%r14,%rax)
	jne	.LBB4_130
# BB#129:                               #   in Loop: Header=BB4_128 Depth=1
	cmpb	$0, 1(%r14,%rax)
	je	.LBB4_127
.LBB4_130:                              #   in Loop: Header=BB4_128 Depth=1
	movq	%rax, %rcx
	shrq	%rcx
	movb	$-1, (%rbx,%rcx)
	movb	$-1, %dl
.LBB4_131:                              #   in Loop: Header=BB4_128 Depth=1
	orq	$1, %rcx
	movb	%dl, (%rbx,%rcx)
	addq	$4, %rax
	cmpq	%r13, %rax
	jb	.LBB4_128
	jmp	.LBB4_179
.LBB4_132:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	callq	memcpy
	movq	8(%rsp), %r11           # 8-byte Reload
	jmp	.LBB4_179
.LBB4_133:                              # %.lr.ph34.i.preheader
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, %rax
	cmovlq	%rcx, %rax
	incq	%rax
	subq	%rcx, %rax
	addq	%rax, %rdi
	xorl	%ebp, %ebp
	xorl	%r13d, %r13d
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rdi, 64(%rsp)          # 8-byte Spill
.LBB4_134:                              # %.lr.ph34.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%r13), %eax
	cmpb	$126, %al
	ja	.LBB4_136
# BB#135:                               #   in Loop: Header=BB4_134 Depth=1
	movb	$0, -1(%rdi,%rbp)
	movzbl	(%r14,%r13), %eax
	incq	%r13
	movb	%al, (%rdi,%rbp)
	jmp	.LBB4_150
.LBB4_136:                              #   in Loop: Header=BB4_134 Depth=1
	movl	%eax, %ecx
	andb	$-32, %cl
	cmpb	$-32, %cl
	je	.LBB4_140
# BB#137:                               #   in Loop: Header=BB4_134 Depth=1
	cmpb	$-64, %cl
	jne	.LBB4_143
# BB#138:                               #   in Loop: Header=BB4_134 Depth=1
	movzbl	1(%r14,%r13), %ecx
	andb	$-64, %cl
	cmpb	$-128, %cl
	jne	.LBB4_160
# BB#139:                               #   in Loop: Header=BB4_134 Depth=1
	shrb	$2, %al
	andb	$7, %al
	movb	%al, -1(%rdi,%rbp)
	movzbl	(%r14,%r13), %eax
	movzbl	1(%r14,%r13), %ecx
	shlb	$6, %al
	andb	$63, %cl
	orb	%al, %cl
	movb	%cl, (%rdi,%rbp)
	addq	$2, %r13
	jmp	.LBB4_149
.LBB4_140:                              #   in Loop: Header=BB4_134 Depth=1
	movzbl	1(%r14,%r13), %ecx
	movl	%ecx, %edx
	andb	$-64, %dl
	cmpb	$-128, %dl
	jne	.LBB4_160
# BB#141:                               #   in Loop: Header=BB4_134 Depth=1
	movzbl	2(%r14,%r13), %edx
	andb	$-64, %dl
	cmpb	$-128, %dl
	jne	.LBB4_160
# BB#142:                               #   in Loop: Header=BB4_134 Depth=1
	shlb	$4, %al
	shrb	$2, %cl
	andb	$15, %cl
	orb	%al, %cl
	movb	%cl, -1(%rdi,%rbp)
	movzbl	1(%r14,%r13), %eax
	movzbl	2(%r14,%r13), %ecx
	shlb	$6, %al
	andb	$63, %cl
	orb	%al, %cl
	movb	%cl, (%rdi,%rbp)
	addq	$3, %r13
	jmp	.LBB4_148
.LBB4_143:                              #   in Loop: Header=BB4_134 Depth=1
	andb	$-8, %al
	cmpb	$-16, %al
	jne	.LBB4_160
# BB#144:                               #   in Loop: Header=BB4_134 Depth=1
	movzbl	1(%r14,%r13), %eax
	andb	$-64, %al
	cmpb	$-128, %al
	jne	.LBB4_160
# BB#145:                               #   in Loop: Header=BB4_134 Depth=1
	movzbl	2(%r14,%r13), %eax
	andb	$-64, %al
	cmpb	$-128, %al
	jne	.LBB4_160
# BB#146:                               #   in Loop: Header=BB4_134 Depth=1
	movzbl	3(%r14,%r13), %eax
	andb	$-64, %al
	cmpb	$-128, %al
	jne	.LBB4_160
# BB#147:                               #   in Loop: Header=BB4_134 Depth=1
	movl	$.L.str.1695, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	64(%rsp), %rdi          # 8-byte Reload
	movw	$-1, -1(%rdi,%rbp)
	addq	$4, %r13
.LBB4_148:                              # %.backedge.i
                                        #   in Loop: Header=BB4_134 Depth=1
	movq	24(%rsp), %rdx          # 8-byte Reload
.LBB4_149:                              # %.backedge.i
                                        #   in Loop: Header=BB4_134 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
.LBB4_150:                              # %.backedge.i
                                        #   in Loop: Header=BB4_134 Depth=1
	addq	$2, %rbp
	cmpq	%rcx, %rbp
	jae	.LBB4_161
# BB#151:                               # %.backedge.i
                                        #   in Loop: Header=BB4_134 Depth=1
	cmpq	32(%rsp), %r13          # 8-byte Folded Reload
	jb	.LBB4_134
	jmp	.LBB4_161
.LBB4_152:                              # %min.iters.checked
	movabsq	$9223372036854775792, %r9 # imm = 0x7FFFFFFFFFFFFFF0
	andq	%rax, %r9
	je	.LBB4_186
# BB#153:                               # %vector.scevcheck
	cmpq	%rbp, %rcx
	movq	%rcx, %r8
	movq	%rdi, %rsi
	movq	%rcx, %rdi
	cmovaq	%rbp, %rdi
	decq	%rdi
	addq	%rdi, %rdi
	setb	%r10b
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, %rbp
	movq	%rcx, %rdx
	cmovgeq	%rbp, %rdx
	incq	%rdx
	subq	%rcx, %rdx
	addq	%rsi, %rdx
	addq	%rdi, %rdx
	setb	%sil
	xorl	%edx, %edx
	addq	%rbx, %rdi
	jb	.LBB4_189
# BB#154:                               # %vector.scevcheck
	testb	%r10b, %r10b
	jne	.LBB4_189
# BB#155:                               # %vector.scevcheck
	testb	%sil, %sil
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	jne	.LBB4_198
# BB#156:                               # %vector.scevcheck
	testb	%r10b, %r10b
	jne	.LBB4_198
# BB#157:                               # %vector.memcheck
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	cmpq	%r10, %rdx
	movq	%r8, %rsi
	movq	%r10, %rcx
	cmovgeq	%rdx, %rcx
	cmpq	%rbp, %rsi
	cmovaq	%rbp, %rsi
	leaq	(%r14,%rax), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB4_190
# BB#158:                               # %vector.memcheck
	addq	$-2, %rcx
	subq	%r10, %rcx
	addq	%rsi, %rsi
	xorq	$-2, %rsi
	subq	%rsi, %rcx
	addq	%rdi, %rcx
	cmpq	%rcx, %r14
	jae	.LBB4_190
# BB#159:
	xorl	%edx, %edx
	movq	32(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB4_198
.LBB4_160:
	movl	$.L.str.1694, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
.LBB4_161:                              # %.critedge.i178
	addq	%r13, %r14
	addq	%rbp, %rbx
	subq	%r13, 32(%rsp)          # 8-byte Folded Spill
	je	.LBB4_164
# BB#162:
	subq	%rbp, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	callq	__errno_location
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	movq	56(%rsp), %r13          # 8-byte Reload
	je	.LBB4_187
# BB#163:
	movl	$84, (%rax)
	movq	32(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB4_181
.LBB4_164:
	xorl	%ebp, %ebp
	movq	56(%rsp), %r13          # 8-byte Reload
	jmp	.LBB4_183
.LBB4_165:
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	je	.LBB4_179
.LBB4_166:                              # %.lr.ph21.i.preheader.new
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	cmpq	%r8, %rbp
	cmovlq	%r8, %rbp
	leaq	2(%rcx), %rdx
	andq	$-2, %rdx
	addq	%rbp, %rdx
	subq	%r8, %rdx
	addq	%rdi, %rdx
	movq	%rcx, %rsi
	andq	$-2, %rsi
	addq	%rbp, %rsi
	subq	%r8, %rsi
	addq	%rdi, %rsi
.LBB4_167:                              # %.lr.ph21.i
                                        # =>This Inner Loop Header: Depth=1
	movw	$-1, %di
	cmpw	$0, (%r14,%rcx,2)
	movw	$-1, %bp
	jne	.LBB4_169
# BB#168:                               #   in Loop: Header=BB4_167 Depth=1
	movzwl	2(%r14,%rcx,2), %ebp
.LBB4_169:                              # %.lr.ph21.i.1390
                                        #   in Loop: Header=BB4_167 Depth=1
	movw	%bp, (%rsi)
	cmpw	$0, 4(%r14,%rcx,2)
	jne	.LBB4_171
# BB#170:                               #   in Loop: Header=BB4_167 Depth=1
	movzwl	6(%r14,%rcx,2), %edi
.LBB4_171:                              #   in Loop: Header=BB4_167 Depth=1
	movw	%di, (%rdx)
	addq	$4, %rcx
	addq	$4, %rdx
	addq	$4, %rsi
	cmpq	%rax, %rcx
	jb	.LBB4_167
	jmp	.LBB4_179
.LBB4_172:
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	je	.LBB4_179
.LBB4_173:                              # %.lr.ph23.i.preheader.new
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	cmpq	%r8, %rbp
	cmovlq	%r8, %rbp
	leaq	2(%rcx), %rdx
	andq	$-2, %rdx
	addq	%rbp, %rdx
	subq	%r8, %rdx
	addq	%rdi, %rdx
	movq	%rcx, %rsi
	andq	$-2, %rsi
	addq	%rbp, %rsi
	subq	%r8, %rsi
	addq	%rdi, %rsi
.LBB4_174:                              # %.lr.ph23.i
                                        # =>This Inner Loop Header: Depth=1
	movw	$-1, %di
	cmpw	$0, 2(%r14,%rcx,2)
	movw	$-1, %bp
	jne	.LBB4_176
# BB#175:                               #   in Loop: Header=BB4_174 Depth=1
	movzwl	(%r14,%rcx,2), %ebp
.LBB4_176:                              # %.lr.ph23.i.1393
                                        #   in Loop: Header=BB4_174 Depth=1
	movw	%bp, (%rsi)
	cmpw	$0, 6(%r14,%rcx,2)
	jne	.LBB4_178
# BB#177:                               #   in Loop: Header=BB4_174 Depth=1
	movzwl	4(%r14,%rcx,2), %edi
.LBB4_178:                              #   in Loop: Header=BB4_174 Depth=1
	movw	%di, (%rdx)
	addq	$4, %rcx
	addq	$4, %rdx
	addq	$4, %rsi
	cmpq	%rax, %rcx
	jb	.LBB4_174
.LBB4_179:                              # %.loopexit.i
	addq	%r13, %r14
	addq	%r13, %rbx
	movq	32(%rsp), %rbp          # 8-byte Reload
	subq	%r13, %rbp
	je	.LBB4_184
# BB#180:
	subq	%r13, 16(%rsp)          # 8-byte Folded Spill
	callq	__errno_location
	movl	$7, (%rax)
	movq	56(%rsp), %r13          # 8-byte Reload
.LBB4_181:
	callq	__errno_location
	movl	(%rax), %edi
	cmpl	$7, %edi
	jne	.LBB4_185
# BB#182:
	movq	24(%rsp), %rdx          # 8-byte Reload
.LBB4_183:                              # %.thread
	xorl	%esi, %esi
	movq	8(%rsp), %r11           # 8-byte Reload
	jmp	.LBB4_203
.LBB4_184:
	xorl	%ebp, %ebp
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %r13          # 8-byte Reload
	jmp	.LBB4_202
.LBB4_185:
	callq	strerror
	movq	%rax, %rsi
	movq	%rbx, %rdx
	movq	8(%rsp), %r12           # 8-byte Reload
	subq	120(%r12), %rdx
	movq	%r14, %rcx
	subq	96(%r12), %rcx
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	movq	%rbp, %r8
	movq	16(%rsp), %r9           # 8-byte Reload
	callq	cli_dbgmsg
	movq	%r12, %r11
	movb	$0, (%rbx)
	movb	(%r14), %al
	incq	%r14
	movb	%al, 1(%rbx)
	addq	$2, %rbx
	decq	%rbp
	jmp	.LBB4_201
.LBB4_186:
	xorl	%edx, %edx
	jmp	.LBB4_198
.LBB4_187:
	movl	$7, (%rax)
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB4_181
.LBB4_188:
	movl	$.L.str.1666, %esi
	jmp	.LBB4_80
.LBB4_189:
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB4_198
.LBB4_190:                              # %vector.body.preheader
	leaq	-16(%r9), %rdx
	movl	%edx, %esi
	shrl	$4, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB4_193
# BB#191:                               # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edi, %edi
	movq	40(%rsp), %rcx          # 8-byte Reload
.LBB4_192:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%r14,%rdi), %xmm0
	pxor	%xmm1, %xmm1
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	pxor	%xmm2, %xmm2
	punpckhbw	%xmm0, %xmm2    # xmm2 = xmm2[8],xmm0[8],xmm2[9],xmm0[9],xmm2[10],xmm0[10],xmm2[11],xmm0[11],xmm2[12],xmm0[12],xmm2[13],xmm0[13],xmm2[14],xmm0[14],xmm2[15],xmm0[15]
	movdqu	%xmm2, 16(%rbx,%rdi,2)
	movdqu	%xmm1, (%rbx,%rdi,2)
	addq	$16, %rdi
	incq	%rsi
	jne	.LBB4_192
	jmp	.LBB4_194
.LBB4_193:
	xorl	%edi, %edi
	movq	40(%rsp), %rcx          # 8-byte Reload
.LBB4_194:                              # %vector.body.prol.loopexit
	cmpq	$48, %rdx
	jb	.LBB4_197
# BB#195:                               # %vector.body.preheader.new
	movq	%r9, %rdx
	subq	%rdi, %rdx
	leaq	48(%r14,%rdi), %rsi
	movq	48(%rsp), %rbp          # 8-byte Reload
	cmpq	%rcx, %rbp
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmovgeq	%rbp, %rcx
	leaq	96(%rcx,%rdi,2), %rdi
	subq	40(%rsp), %rdi          # 8-byte Folded Reload
	addq	64(%rsp), %rdi          # 8-byte Folded Reload
.LBB4_196:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-48(%rsi), %xmm0
	pxor	%xmm1, %xmm1
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	pxor	%xmm2, %xmm2
	punpckhbw	%xmm0, %xmm2    # xmm2 = xmm2[8],xmm0[8],xmm2[9],xmm0[9],xmm2[10],xmm0[10],xmm2[11],xmm0[11],xmm2[12],xmm0[12],xmm2[13],xmm0[13],xmm2[14],xmm0[14],xmm2[15],xmm0[15]
	movdqu	%xmm2, -80(%rdi)
	movdqu	%xmm1, -96(%rdi)
	movdqu	-32(%rsi), %xmm0
	pxor	%xmm1, %xmm1
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	pxor	%xmm2, %xmm2
	punpckhbw	%xmm0, %xmm2    # xmm2 = xmm2[8],xmm0[8],xmm2[9],xmm0[9],xmm2[10],xmm0[10],xmm2[11],xmm0[11],xmm2[12],xmm0[12],xmm2[13],xmm0[13],xmm2[14],xmm0[14],xmm2[15],xmm0[15]
	movdqu	%xmm2, -48(%rdi)
	movdqu	%xmm1, -64(%rdi)
	movdqu	-16(%rsi), %xmm0
	pxor	%xmm1, %xmm1
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	pxor	%xmm2, %xmm2
	punpckhbw	%xmm0, %xmm2    # xmm2 = xmm2[8],xmm0[8],xmm2[9],xmm0[9],xmm2[10],xmm0[10],xmm2[11],xmm0[11],xmm2[12],xmm0[12],xmm2[13],xmm0[13],xmm2[14],xmm0[14],xmm2[15],xmm0[15]
	movdqu	%xmm2, -16(%rdi)
	movdqu	%xmm1, -32(%rdi)
	movdqu	(%rsi), %xmm0
	pxor	%xmm1, %xmm1
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	pxor	%xmm2, %xmm2
	punpckhbw	%xmm0, %xmm2    # xmm2 = xmm2[8],xmm0[8],xmm2[9],xmm0[9],xmm2[10],xmm0[10],xmm2[11],xmm0[11],xmm2[12],xmm0[12],xmm2[13],xmm0[13],xmm2[14],xmm0[14],xmm2[15],xmm0[15]
	movdqu	%xmm2, 16(%rdi)
	movdqu	%xmm1, (%rdi)
	addq	$64, %rsi
	subq	$-128, %rdi
	addq	$-64, %rdx
	jne	.LBB4_196
.LBB4_197:                              # %middle.block
	cmpq	%r9, %rax
	movq	%r9, %rdx
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB4_200
.LBB4_198:                              # %.lr.ph30.i.preheader373
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	cmpq	%rsi, %rcx
	cmovlq	%rsi, %rcx
	incq	%rcx
	subq	%rsi, %rcx
	addq	%rcx, %rdi
.LBB4_199:                              # %.lr.ph30.i
                                        # =>This Inner Loop Header: Depth=1
	movb	$0, -1(%rdi,%rdx,2)
	movzbl	(%r14,%rdx), %ecx
	movb	%cl, (%rdi,%rdx,2)
	incq	%rdx
	cmpq	%rax, %rdx
	jb	.LBB4_199
.LBB4_200:                              # %._crit_edge.i
	subq	%rax, %rbp
	addq	%rax, %r14
	leaq	(%rbx,%rax,2), %rbx
.LBB4_201:                              # %.thread
	movq	24(%rsp), %rdx          # 8-byte Reload
.LBB4_202:                              # %.thread
	xorl	%esi, %esi
.LBB4_203:                              # %.thread
	movsbl	%r15b, %eax
	testl	%eax, %eax
	cmovnsl	%eax, %esi
	addq	%rbp, %rsi
	movq	120(%r11), %rbp
	subq	%r13, %rbx
	subq	%rbp, %rbx
	movq	%rbx, 128(%r11)
	subq	96(%r11), %r14
	movq	%r14, 112(%r11)
	addq	%rsi, %r14
	movq	%r14, 104(%r11)
	testq	%rdx, %rdx
	je	.LBB4_206
# BB#204:
	movq	48(%r11), %rax
	movq	144(%r11), %r13
	shrq	%rax
	cmpq	%rax, %rdx
	jae	.LBB4_207
# BB#205:
	movq	160(%r11), %rsi
	addq	%r13, %rsi
	movq	%r13, %rdi
	movq	24(%rsp), %rdx          # 8-byte Reload
	callq	memmove
	movq	8(%rsp), %r11           # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	$0, 160(%r11)
	movq	144(%r11), %r13
	addq	%r13, %rdx
	movq	120(%r11), %rbp
	movq	128(%r11), %rbx
	jmp	.LBB4_208
.LBB4_206:
	movq	$0, 160(%r11)
	movq	144(%r11), %r13
	movq	%r13, %rdx
	jmp	.LBB4_208
.LBB4_207:
	movq	152(%r11), %rdx
	addq	%r13, %rdx
.LBB4_208:
	addq	48(%r11), %r13
	testq	%rbx, %rbx
	jle	.LBB4_212
# BB#209:
	cmpb	$-1, (%rbp)
	jne	.LBB4_212
# BB#210:
	cmpb	$-2, 1(%rbp)
	jne	.LBB4_212
# BB#211:
	movl	$2, %r14d
	cmpq	%rbx, %r14
	jl	.LBB4_213
	jmp	.LBB4_241
.LBB4_212:
	xorl	%r14d, %r14d
	cmpq	%rbx, %r14
	jge	.LBB4_241
.LBB4_213:                              # %.lr.ph
	leaq	78(%rsp), %r12
	testb	%r15b, %r15b
	js	.LBB4_228
	.p2align	4, 0x90
.LBB4_214:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp,%r14), %eax
	shll	$8, %eax
	movzbl	1(%rbp,%r14), %ebx
	orw	%bx, %ax
	je	.LBB4_220
# BB#215:                               #   in Loop: Header=BB4_214 Depth=1
	movzwl	%ax, %ecx
	cmpl	$128, %ecx
	jae	.LBB4_222
# BB#216:                               #   in Loop: Header=BB4_214 Depth=1
	cmpq	%r13, %rdx
	jae	.LBB4_241
# BB#217:                               #   in Loop: Header=BB4_214 Depth=1
	testb	%bl, %bl
	jne	.LBB4_219
# BB#218:                               #   in Loop: Header=BB4_214 Depth=1
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	movq	%rdx, %r15
	callq	cli_dbgmsg
	movq	8(%rsp), %r11           # 8-byte Reload
	leaq	78(%rsp), %r12
	movq	%r15, %rdx
.LBB4_219:                              #   in Loop: Header=BB4_214 Depth=1
	movb	%bl, (%rdx)
	incq	%rdx
	jmp	.LBB4_227
	.p2align	4, 0x90
.LBB4_220:                              #   in Loop: Header=BB4_214 Depth=1
	cmpl	$0, 168(%r11)
	jne	.LBB4_227
# BB#221:                               #   in Loop: Header=BB4_214 Depth=1
	movl	$1, 168(%r11)
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	movq	%rdx, %rbx
	callq	cli_dbgmsg
	movq	8(%rsp), %r11           # 8-byte Reload
	leaq	78(%rsp), %r12
	movq	%rbx, %rdx
	jmp	.LBB4_227
	.p2align	4, 0x90
.LBB4_222:                              #   in Loop: Header=BB4_214 Depth=1
	cmpl	$160, %ecx
	jne	.LBB4_225
# BB#223:                               #   in Loop: Header=BB4_214 Depth=1
	cmpq	%r13, %rdx
	jae	.LBB4_241
# BB#224:                               #   in Loop: Header=BB4_214 Depth=1
	movb	$32, (%rdx)
	incq	%rdx
	jmp	.LBB4_227
.LBB4_225:                              #   in Loop: Header=BB4_214 Depth=1
	movl	$9, %esi
	movq	%rdx, %r15
	movl	$.L.str.3, %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	snprintf
	movb	$0, 87(%rsp)
	movq	%r12, %rdi
	callq	strlen
	movq	%r15, %rdx
	movq	%r13, %rcx
	subq	%rdx, %rcx
	movslq	%eax, %rbx
	cmpq	%rbx, %rcx
	jle	.LBB4_240
# BB#226:                               # %.thread228.us
                                        #   in Loop: Header=BB4_214 Depth=1
	movq	%rdx, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%r15, %rdx
	addq	%rbx, %rdx
	movq	8(%rsp), %r11           # 8-byte Reload
	.p2align	4, 0x90
.LBB4_227:                              #   in Loop: Header=BB4_214 Depth=1
	addq	$2, %r14
	cmpq	128(%r11), %r14
	jl	.LBB4_214
	jmp	.LBB4_241
	.p2align	4, 0x90
.LBB4_228:                              # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp,%r14), %eax
	shll	$8, %eax
	movzbl	1(%rbp,%r14), %ebx
	orw	%bx, %ax
	je	.LBB4_239
# BB#229:                               #   in Loop: Header=BB4_228 Depth=1
	movzwl	%ax, %ecx
	cmpl	$127, %ecx
	ja	.LBB4_234
# BB#230:                               #   in Loop: Header=BB4_228 Depth=1
	cmpq	%r13, %rdx
	jae	.LBB4_241
# BB#231:                               #   in Loop: Header=BB4_228 Depth=1
	testb	%bl, %bl
	jne	.LBB4_233
# BB#232:                               #   in Loop: Header=BB4_228 Depth=1
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	movq	%rdx, %r15
	callq	cli_dbgmsg
	movq	8(%rsp), %r11           # 8-byte Reload
	movq	%r15, %rdx
.LBB4_233:                              #   in Loop: Header=BB4_228 Depth=1
	movb	%bl, (%rdx)
	incq	%rdx
	jmp	.LBB4_239
.LBB4_234:                              #   in Loop: Header=BB4_228 Depth=1
	cmpl	$160, %ecx
	jne	.LBB4_237
# BB#235:                               #   in Loop: Header=BB4_228 Depth=1
	cmpq	%r13, %rdx
	jae	.LBB4_241
# BB#236:                               #   in Loop: Header=BB4_228 Depth=1
	movb	$32, (%rdx)
	incq	%rdx
	jmp	.LBB4_239
.LBB4_237:                              #   in Loop: Header=BB4_228 Depth=1
	movl	$9, %esi
	movq	%rdx, %r15
	movl	$.L.str.3, %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	snprintf
	movb	$0, 87(%rsp)
	movq	%r12, %rdi
	callq	strlen
	movq	%r15, %rdx
	movq	%r13, %rcx
	subq	%rdx, %rcx
	movslq	%eax, %rbx
	cmpq	%rbx, %rcx
	jle	.LBB4_240
# BB#238:                               # %.thread228
                                        #   in Loop: Header=BB4_228 Depth=1
	movq	%rdx, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%r15, %rdx
	addq	%rbx, %rdx
	movq	8(%rsp), %r11           # 8-byte Reload
	.p2align	4, 0x90
.LBB4_239:                              #   in Loop: Header=BB4_228 Depth=1
	addq	$2, %r14
	cmpq	128(%r11), %r14
	jl	.LBB4_228
	jmp	.LBB4_241
.LBB4_240:                              # %.us-lcssa.us
	movq	8(%rsp), %r11           # 8-byte Reload
.LBB4_241:                              # %.loopexit
	movq	%r14, 136(%r11)
	subq	144(%r11), %rdx
	movq	%rdx, 152(%r11)
	leaq	144(%r11), %r11
.LBB4_242:
	xorl	%edi, %edi
	movq	%r11, %rsi
	movq	96(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	cli_readline            # TAILCALL
.Lfunc_end4:
	.size	encoding_norm_readline, .Lfunc_end4-encoding_norm_readline
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_34
	.quad	.LBB4_81
	.quad	.LBB4_81
	.quad	.LBB4_81
	.quad	.LBB4_81
	.quad	.LBB4_81
	.quad	.LBB4_81
	.quad	.LBB4_81
	.quad	.LBB4_81
	.quad	.LBB4_81
	.quad	.LBB4_81
	.quad	.LBB4_81
	.quad	.LBB4_81
	.quad	.LBB4_81
	.quad	.LBB4_81
	.quad	.LBB4_42
	.quad	.LBB4_46
	.quad	.LBB4_50
.LJTI4_1:
	.quad	.LBB4_99
	.quad	.LBB4_109
	.quad	.LBB4_99
	.quad	.LBB4_115
	.quad	.LBB4_120
	.quad	.LBB4_125
	.quad	.LBB4_132
	.quad	.LBB4_109
	.quad	.LBB4_133
	.quad	.LBB4_112
	.quad	.LBB4_112

	.text
	.p2align	4, 0x90
	.type	iconv_open_cached,@function
iconv_open_cached:                      # @iconv_open_cached
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi45:
	.cfi_def_cfa_offset 64
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	callq	strlen
	movq	%rax, %r14
	movb	iconv_global_inited(%rip), %al
	testb	%al, %al
	jne	.LBB5_3
# BB#1:
	movl	$1, %edi
	movl	$56, %esi
	callq	cli_calloc
	movq	%rax, %rbx
	movq	%rbx, global_iconv_cache(%rip)
	testq	%rbx, %rbx
	je	.LBB5_4
# BB#2:
	movl	$.L.str.1690, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	addq	$24, %rbx
	movl	$32, %esi
	movq	%rbx, %rdi
	callq	hashtab_init
	movl	$iconv_cache_cleanup_main, %edi
	callq	atexit
	movb	$1, iconv_global_inited(%rip)
.LBB5_3:                                # %init_iconv_pool_ifneeded.exitthread-pre-split
	movq	global_iconv_cache(%rip), %rbx
.LBB5_4:                                # %init_iconv_pool_ifneeded.exit
	testq	%rbx, %rbx
	je	.LBB5_5
# BB#6:
	leaq	24(%rbx), %r12
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	hashtab_find
	testq	%rax, %rax
	je	.LBB5_10
# BB#7:
	movq	8(%rax), %rax
	testq	%rax, %rax
	js	.LBB5_10
# BB#8:
	cmpq	8(%rbx), %rax
	jbe	.LBB5_9
.LBB5_10:
	movl	$.L.str.1687, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	movl	$16, %edi
	callq	cli_malloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB5_11
# BB#12:
	movl	$10, (%r13)
	movl	$.L.str.1682, %eax
	cmpq	%rax, %r15
	je	.LBB5_13
# BB#16:
	movl	$.L.str.1666, %eax
	cmpq	%rax, %r15
	je	.LBB5_17
# BB#18:
	movq	%r15, %rdi
	callq	strlen
	movl	$aliases_htable, %edi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	hashtab_find
	testq	%rax, %rax
	je	.LBB5_21
# BB#19:
	cmpq	$0, (%rax)
	je	.LBB5_21
# BB#20:
	movl	8(%rax), %eax
	movl	%eax, (%r13)
	cmpl	$8, %eax
	jb	.LBB5_15
	jmp	.LBB5_23
.LBB5_5:
	movl	$.L.str.1686, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	callq	__errno_location
	movl	$22, (%rax)
	movq	$-1, %rbp
	jmp	.LBB5_30
.LBB5_11:
	xorl	%r13d, %r13d
	jmp	.LBB5_25
.LBB5_9:
	movq	(%rbx), %rcx
	movq	(%rcx,%rax,8), %rbp
	jmp	.LBB5_30
.LBB5_13:
	movl	$4, %eax
	jmp	.LBB5_14
.LBB5_17:
	movl	$5, %eax
.LBB5_14:                               # %switch.lookup.sink.split.i.i
	movl	%eax, (%r13)
	jmp	.LBB5_15
.LBB5_21:                               # %thread-pre-split.i.i
	movl	(%r13), %eax
	cmpl	$8, %eax
	jae	.LBB5_23
.LBB5_15:                               # %switch.lookup.i.i
	cltq
	movq	.Lswitch.table(,%rax,8), %rax
.LBB5_24:                               # %encoding_bytes.exit.i
	movq	%rax, 8(%r13)
.LBB5_25:                               # %iconv_open.exit
	movq	$-1, %rbp
	cmpq	$-1, %r13
	je	.LBB5_30
# BB#26:
	movq	%r14, (%rsp)            # 8-byte Spill
	movq	16(%rbx), %r14
	leaq	1(%r14), %rax
	movq	%rax, 16(%rbx)
	movq	8(%rbx), %rax
	cmpq	%rax, %r14
	jb	.LBB5_29
# BB#27:
	leaq	16(%rax), %rcx
	movq	%rcx, 8(%rbx)
	movq	(%rbx), %rdi
	leaq	128(,%rax,8), %rsi
	callq	cli_realloc2
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.LBB5_28
.LBB5_29:
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	%r14, %rcx
	callq	hashtab_insert
	movq	(%rbx), %rax
	movq	%r13, (%rax,%r14,8)
	movq	(%rbx), %rax
	movq	(%rax,%r14,8), %rdx
	movl	$.L.str.1689, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	movq	(%rbx), %rax
	movq	(%rax,%r14,8), %rbp
.LBB5_30:
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_23:
	movl	$1, %eax
	jmp	.LBB5_24
.LBB5_28:
	movl	$.L.str.1688, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	callq	__errno_location
	movl	$12, (%rax)
	jmp	.LBB5_30
.Lfunc_end5:
	.size	iconv_open_cached, .Lfunc_end5-iconv_open_cached
	.cfi_endproc

	.p2align	4, 0x90
	.type	iconv_cache_cleanup_main,@function
iconv_cache_cleanup_main:               # @iconv_cache_cleanup_main
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi54:
	.cfi_def_cfa_offset 32
.Lcfi55:
	.cfi_offset %rbx, -24
.Lcfi56:
	.cfi_offset %r14, -16
	movq	global_iconv_cache(%rip), %r14
	movl	$.L.str.1691, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	cmpq	$0, 16(%r14)
	je	.LBB6_5
# BB#1:                                 # %.lr.ph.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	(%rax,%rbx,8), %rsi
	movl	$.L.str.1692, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB6_4
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	callq	free
.LBB6_4:                                # %iconv_close.exit.i
                                        #   in Loop: Header=BB6_2 Depth=1
	incq	%rbx
	cmpq	16(%r14), %rbx
	jb	.LBB6_2
.LBB6_5:                                # %iconv_cache_destroy.exit
	leaq	24(%r14), %rdi
	callq	hashtab_clear
	movq	24(%r14), %rdi
	callq	free
	movq	(%r14), %rdi
	callq	free
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.Lfunc_end6:
	.size	iconv_cache_cleanup_main, .Lfunc_end6-iconv_cache_cleanup_main
	.cfi_endproc

	.type	entities_htable_elements,@object # @entities_htable_elements
	.data
	.p2align	4
entities_htable_elements:
	.quad	.L.str.15
	.quad	8733                    # 0x221d
	.quad	.L.str.16
	.quad	8775                    # 0x2247
	.quad	.L.str.17
	.quad	96                      # 0x60
	.zero	16
	.quad	.L.str.18
	.quad	10536                   # 0x2928
	.quad	.L.str.19
	.quad	8614                    # 0x21a6
	.quad	.L.str.20
	.quad	8777                    # 0x2249
	.quad	.L.str.21
	.quad	10885                   # 0x2a85
	.zero	16
	.quad	.L.str.22
	.quad	10886                   # 0x2a86
	.zero	16
	.quad	.L.str.23
	.quad	8608                    # 0x21a0
	.zero	16
	.quad	.L.str.24
	.quad	8920                    # 0x22d8
	.quad	.L.str.25
	.quad	10815                   # 0x2a3f
	.quad	.L.str.26
	.quad	10878                   # 0x2a7e
	.zero	16
	.quad	.L.str.27
	.quad	8886                    # 0x22b6
	.quad	.L.str.28
	.quad	9                       # 0x9
	.zero	16
	.quad	.L.str.29
	.quad	10877                   # 0x2a7d
	.quad	.L.str.30
	.quad	8781                    # 0x224d
	.zero	16
	.zero	16
	.quad	.L.str.31
	.quad	10038                   # 0x2736
	.quad	.L.str.32
	.quad	8896                    # 0x22c0
	.quad	.L.str.33
	.quad	8851                    # 0x2293
	.quad	.L.str.34
	.quad	12312                   # 0x3018
	.quad	.L.str.35
	.quad	8817                    # 0x2271
	.zero	16
	.quad	.L.str.36
	.quad	8820                    # 0x2274
	.quad	.L.str.37
	.quad	8662                    # 0x21d6
	.zero	16
	.quad	.L.str.38
	.quad	12313                   # 0x3019
	.quad	.L.str.39
	.quad	916                     # 0x394
	.zero	16
	.quad	.L.str.40
	.quad	8665                    # 0x21d9
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.41
	.quad	8518                    # 0x2146
	.quad	.L.str.42
	.quad	8823                    # 0x2277
	.quad	.L.str.43
	.quad	9674                    # 0x25ca
	.quad	.L.str.44
	.quad	8243                    # 0x2033
	.quad	.L.str.45
	.quad	8594                    # 0x2192
	.quad	.L.str.46
	.quad	962                     # 0x3c2
	.quad	.L.str.47
	.quad	8810                    # 0x226a
	.quad	.L.str.48
	.quad	8478                    # 0x211e
	.zero	16
	.zero	16
	.quad	.L.str.49
	.quad	9416                    # 0x24c8
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.50
	.quad	8716                    # 0x220c
	.zero	16
	.quad	.L.str.51
	.quad	8743                    # 0x2227
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.52
	.quad	8649                    # 0x21c9
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.53
	.quad	948                     # 0x3b4
	.zero	16
	.zero	16
	.quad	.L.str.54
	.quad	64260                   # 0xfb04
	.zero	16
	.zero	16
	.quad	.L.str.55
	.quad	8492                    # 0x212c
	.quad	.L.str.56
	.quad	1106                    # 0x452
	.zero	16
	.quad	.L.str.57
	.quad	1107                    # 0x453
	.quad	.L.str.58
	.quad	8242                    # 0x2032
	.zero	16
	.zero	16
	.quad	.L.str.59
	.quad	1116                    # 0x45c
	.quad	.L.str.60
	.quad	1113                    # 0x459
	.quad	.L.str.61
	.quad	9136                    # 0x23b0
	.quad	.L.str.62
	.quad	1114                    # 0x45a
	.quad	.L.str.63
	.quad	9558                    # 0x2556
	.quad	.L.str.64
	.quad	9827                    # 0x2663
	.quad	.L.str.65
	.quad	8747                    # 0x222b
	.zero	16
	.quad	.L.str.66
	.quad	9137                    # 0x23b1
	.quad	.L.str.67
	.quad	47                      # 0x2f
	.quad	.L.str.68
	.quad	8847                    # 0x228f
	.zero	16
	.quad	.L.str.69
	.quad	9645                    # 0x25ad
	.quad	.L.str.70
	.quad	167                     # 0xa7
	.quad	.L.str.71
	.quad	8202                    # 0x200a
	.quad	.L.str.72
	.quad	8818                    # 0x2272
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.73
	.quad	65080                   # 0xfe38
	.zero	16
	.quad	.L.str.74
	.quad	8705                    # 0x2201
	.quad	.L.str.75
	.quad	8613                    # 0x21a5
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.76
	.quad	710                     # 0x2c6
	.quad	.L.str.77
	.quad	8705                    # 0x2201
	.quad	.L.str.78
	.quad	9837                    # 0x266d
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.79
	.quad	8601                    # 0x2199
	.zero	16
	.quad	.L.str.80
	.quad	8927                    # 0x22df
	.quad	.L.str.81
	.quad	10928                   # 0x2ab0
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.82
	.quad	8364                    # 0x20ac
	.quad	.L.str.83
	.quad	8829                    # 0x227d
	.quad	.L.str.84
	.quad	8968                    # 0x2308
	.quad	.L.str.85
	.quad	8733                    # 0x221d
	.zero	16
	.zero	16
	.quad	.L.str.86
	.quad	8708                    # 0x2204
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.87
	.quad	8916                    # 0x22d4
	.quad	.L.str.88
	.quad	8650                    # 0x21ca
	.zero	16
	.quad	.L.str.89
	.quad	1038                    # 0x40e
	.zero	16
	.zero	16
	.quad	.L.str.90
	.quad	8850                    # 0x2292
	.quad	.L.str.91
	.quad	10723                   # 0x29e3
	.zero	16
	.quad	.L.str.92
	.quad	8647                    # 0x21c7
	.zero	16
	.zero	16
	.quad	.L.str.93
	.quad	10584                   # 0x2958
	.quad	.L.str.94
	.quad	8850                    # 0x2292
	.zero	16
	.quad	.L.str.95
	.quad	8649                    # 0x21c9
	.zero	16
	.zero	16
	.quad	.L.str.96
	.quad	8648                    # 0x21c8
	.quad	.L.str.97
	.quad	8794                    # 0x225a
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.98
	.quad	198                     # 0xc6
	.quad	.L.str.99
	.quad	8958                    # 0x22fe
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.100
	.quad	8862                    # 0x229e
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.101
	.quad	8872                    # 0x22a8
	.quad	.L.str.102
	.quad	8931                    # 0x22e3
	.quad	.L.str.103
	.quad	8618                    # 0x21aa
	.quad	.L.str.104
	.quad	1118                    # 0x45e
	.zero	16
	.quad	.L.str.105
	.quad	338                     # 0x152
	.zero	16
	.quad	.L.str.106
	.quad	8821                    # 0x2275
	.quad	.L.str.107
	.quad	64257                   # 0xfb01
	.quad	.L.str.108
	.quad	8814                    # 0x226e
	.quad	.L.str.109
	.quad	913                     # 0x391
	.quad	.L.str.110
	.quad	8805                    # 0x2265
	.quad	.L.str.111
	.quad	8230                    # 0x2026
	.quad	.L.str.112
	.quad	8229                    # 0x2025
	.quad	.L.str.113
	.quad	8972                    # 0x230c
	.quad	.L.str.114
	.quad	916                     # 0x394
	.quad	.L.str.115
	.quad	914                     # 0x392
	.quad	.L.str.116
	.quad	921                     # 0x399
	.quad	.L.str.117
	.quad	8737                    # 0x2221
	.quad	.L.str.118
	.quad	8796                    # 0x225c
	.quad	.L.str.119
	.quad	922                     # 0x39a
	.quad	.L.str.120
	.quad	915                     # 0x393
	.quad	.L.str.121
	.quad	924                     # 0x39c
	.quad	.L.str.122
	.quad	917                     # 0x395
	.quad	.L.str.123
	.quad	927                     # 0x39f
	.quad	.L.str.124
	.quad	925                     # 0x39d
	.quad	.L.str.125
	.quad	923                     # 0x39b
	.quad	.L.str.126
	.quad	928                     # 0x3a0
	.quad	.L.str.127
	.quad	931                     # 0x3a3
	.quad	.L.str.128
	.quad	8773                    # 0x2245
	.quad	.L.str.129
	.quad	8785                    # 0x2251
	.quad	.L.str.130
	.quad	8974                    # 0x230e
	.quad	.L.str.131
	.quad	929                     # 0x3a1
	.quad	.L.str.132
	.quad	926                     # 0x39e
	.quad	.L.str.133
	.quad	932                     # 0x3a4
	.quad	.L.str.134
	.quad	933                     # 0x3a5
	.zero	16
	.quad	.L.str.135
	.quad	8814                    # 0x226e
	.quad	.L.str.136
	.quad	8867                    # 0x22a3
	.quad	.L.str.137
	.quad	175                     # 0xaf
	.quad	.L.str.138
	.quad	918                     # 0x396
	.quad	.L.str.139
	.quad	945                     # 0x3b1
	.quad	.L.str.140
	.quad	946                     # 0x3b2
	.quad	.L.str.141
	.quad	981                     # 0x3d5
	.quad	.L.str.142
	.quad	948                     # 0x3b4
	.quad	.L.str.143
	.quad	10775                   # 0x2a17
	.quad	.L.str.144
	.quad	8862                    # 0x229e
	.quad	.L.str.145
	.quad	947                     # 0x3b3
	.quad	.L.str.146
	.quad	949                     # 0x3b5
	.quad	.L.str.147
	.quad	8941                    # 0x22ed
	.quad	.L.str.148
	.quad	10601                   # 0x2969
	.quad	.L.str.149
	.quad	8991                    # 0x231f
	.quad	.L.str.150
	.quad	953                     # 0x3b9
	.quad	.L.str.151
	.quad	955                     # 0x3bb
	.quad	.L.str.152
	.quad	9604                    # 0x2584
	.quad	.L.str.153
	.quad	957                     # 0x3bd
	.quad	.L.str.154
	.quad	956                     # 0x3bc
	.quad	.L.str.155
	.quad	954                     # 0x3ba
	.quad	.L.str.156
	.quad	959                     # 0x3bf
	.quad	.L.str.157
	.quad	8989                    # 0x231d
	.quad	.L.str.158
	.quad	963                     # 0x3c3
	.quad	.L.str.159
	.quad	9565                    # 0x255d
	.quad	.L.str.160
	.quad	10772                   # 0x2a14
	.quad	.L.str.161
	.quad	9600                    # 0x2580
	.quad	.L.str.162
	.quad	961                     # 0x3c1
	.quad	.L.str.163
	.quad	10556                   # 0x293c
	.quad	.L.str.164
	.quad	960                     # 0x3c0
	.quad	.L.str.165
	.quad	10600                   # 0x2968
	.quad	.L.str.166
	.quad	12308                   # 0x3014
	.quad	.L.str.167
	.quad	950                     # 0x3b6
	.quad	.L.str.168
	.quad	964                     # 0x3c4
	.quad	.L.str.169
	.quad	965                     # 0x3c5
	.zero	16
	.quad	.L.str.170
	.quad	12309                   # 0x3015
	.quad	.L.str.171
	.quad	8978                    # 0x2312
	.zero	16
	.zero	16
	.quad	.L.str.172
	.quad	8207                    # 0x200f
	.quad	.L.str.173
	.quad	65079                   # 0xfe37
	.quad	.L.str.174
	.quad	958                     # 0x3be
	.quad	.L.str.175
	.quad	8249                    # 0x2039
	.zero	16
	.quad	.L.str.176
	.quad	8598                    # 0x2196
	.quad	.L.str.177
	.quad	8652                    # 0x21cc
	.zero	16
	.quad	.L.str.178
	.quad	8250                    # 0x203a
	.zero	16
	.quad	.L.str.179
	.quad	8601                    # 0x2199
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.180
	.quad	8826                    # 0x227a
	.quad	.L.str.181
	.quad	8884                    # 0x22b4
	.quad	.L.str.182
	.quad	8643                    # 0x21c3
	.quad	.L.str.183
	.quad	8739                    # 0x2223
	.zero	16
	.zero	16
	.quad	.L.str.184
	.quad	8622                    # 0x21ae
	.quad	.L.str.185
	.quad	8885                    # 0x22b5
	.quad	.L.str.186
	.quad	8465                    # 0x2111
	.quad	.L.str.187
	.quad	8994                    # 0x2322
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.188
	.quad	8241                    # 0x2031
	.zero	16
	.zero	16
	.quad	.L.str.189
	.quad	8822                    # 0x2276
	.quad	.L.str.190
	.quad	8897                    # 0x22c1
	.quad	.L.str.191
	.quad	8839                    # 0x2287
	.quad	.L.str.192
	.quad	8639                    # 0x21bf
	.zero	16
	.quad	.L.str.193
	.quad	10233                   # 0x27f9
	.quad	.L.str.194
	.quad	8646                    # 0x21c6
	.zero	16
	.quad	.L.str.195
	.quad	8768                    # 0x2240
	.zero	16
	.quad	.L.str.196
	.quad	8661                    # 0x21d5
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.197
	.quad	8918                    # 0x22d6
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.198
	.quad	8653                    # 0x21cd
	.quad	.L.str.199
	.quad	1068                    # 0x42c
	.zero	16
	.quad	.L.str.200
	.quad	8966                    # 0x2306
	.quad	.L.str.201
	.quad	8639                    # 0x21bf
	.zero	16
	.zero	16
	.quad	.L.str.202
	.quad	9642                    # 0x25aa
	.zero	16
	.zero	16
	.quad	.L.str.203
	.quad	8720                    # 0x2210
	.quad	.L.str.204
	.quad	177                     # 0xb1
	.zero	16
	.quad	.L.str.205
	.quad	8653                    # 0x21cd
	.zero	16
	.quad	.L.str.206
	.quad	1029                    # 0x405
	.quad	.L.str.207
	.quad	10232                   # 0x27f8
	.quad	.L.str.208
	.quad	10230                   # 0x27f6
	.zero	16
	.zero	16
	.quad	.L.str.209
	.quad	8838                    # 0x2286
	.zero	16
	.quad	.L.str.210
	.quad	8597                    # 0x2195
	.quad	.L.str.211
	.quad	8909                    # 0x22cd
	.zero	16
	.zero	16
	.quad	.L.str.212
	.quad	8843                    # 0x228b
	.zero	16
	.zero	16
	.quad	.L.str.213
	.quad	8538                    # 0x215a
	.zero	16
	.quad	.L.str.214
	.quad	1062                    # 0x426
	.zero	16
	.quad	.L.str.215
	.quad	8488                    # 0x2128
	.quad	.L.str.216
	.quad	731                     # 0x2db
	.quad	.L.str.217
	.quad	8965                    # 0x2305
	.quad	.L.str.218
	.quad	8772                    # 0x2244
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.219
	.quad	10891                   # 0x2a8b
	.quad	.L.str.220
	.quad	8651                    # 0x21cb
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.221
	.quad	8863                    # 0x229f
	.quad	.L.str.222
	.quad	8778                    # 0x224a
	.zero	16
	.quad	.L.str.223
	.quad	8903                    # 0x22c7
	.quad	.L.str.224
	.quad	10533                   # 0x2925
	.quad	.L.str.225
	.quad	9573                    # 0x2565
	.quad	.L.str.226
	.quad	8816                    # 0x2270
	.quad	.L.str.227
	.quad	8735                    # 0x221f
	.quad	.L.str.228
	.quad	8902                    # 0x22c6
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.229
	.quad	8842                    # 0x228a
	.quad	.L.str.230
	.quad	8609                    # 0x21a1
	.zero	16
	.quad	.L.str.231
	.quad	8631                    # 0x21b7
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.232
	.quad	8476                    # 0x211c
	.quad	.L.str.233
	.quad	8606                    # 0x219e
	.quad	.L.str.234
	.quad	8857                    # 0x2299
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.235
	.quad	8855                    # 0x2297
	.quad	.L.str.236
	.quad	8608                    # 0x21a0
	.quad	.L.str.237
	.quad	982                     # 0x3d6
	.quad	.L.str.238
	.quad	8715                    # 0x220b
	.quad	.L.str.239
	.quad	8607                    # 0x219f
	.quad	.L.str.240
	.quad	10724                   # 0x29e4
	.zero	16
	.quad	.L.str.241
	.quad	10839                   # 0x2a57
	.quad	.L.str.242
	.quad	8911                    # 0x22cf
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.243
	.quad	728                     # 0x2d8
	.quad	.L.str.244
	.quad	8598                    # 0x2196
	.zero	16
	.quad	.L.str.245
	.quad	10843                   # 0x2a5b
	.quad	.L.str.246
	.quad	8205                    # 0x200d
	.quad	.L.str.247
	.quad	8601                    # 0x2199
	.zero	16
	.quad	.L.str.248
	.quad	8595                    # 0x2193
	.quad	.L.str.249
	.quad	8919                    # 0x22d7
	.quad	.L.str.250
	.quad	10933                   # 0x2ab5
	.quad	.L.str.251
	.quad	8822                    # 0x2276
	.quad	.L.str.252
	.quad	8596                    # 0x2194
	.quad	.L.str.253
	.quad	8598                    # 0x2196
	.quad	.L.str.254
	.quad	8214                    # 0x2016
	.quad	.L.str.255
	.quad	8621                    # 0x21ad
	.quad	.L.str.256
	.quad	8776                    # 0x2248
	.quad	.L.str.257
	.quad	8592                    # 0x2190
	.zero	16
	.quad	.L.str.258
	.quad	8831                    # 0x227f
	.quad	.L.str.259
	.quad	8605                    # 0x219d
	.zero	16
	.quad	.L.str.260
	.quad	8594                    # 0x2192
	.quad	.L.str.261
	.quad	10933                   # 0x2ab5
	.quad	.L.str.262
	.quad	94                      # 0x5e
	.quad	.L.str.263
	.quad	8593                    # 0x2191
	.quad	.L.str.264
	.quad	8597                    # 0x2195
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.265
	.quad	9566                    # 0x255e
	.zero	16
	.quad	.L.str.266
	.quad	181                     # 0xb5
	.quad	.L.str.267
	.quad	728                     # 0x2d8
	.zero	16
	.quad	.L.str.268
	.quad	8802                    # 0x2262
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.269
	.quad	124                     # 0x7c
	.quad	.L.str.270
	.quad	10629                   # 0x2985
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.271
	.quad	8535                    # 0x2157
	.quad	.L.str.272
	.quad	8594                    # 0x2192
	.quad	.L.str.273
	.quad	169                     # 0xa9
	.quad	.L.str.274
	.quad	10630                   # 0x2986
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.275
	.quad	8840                    # 0x2288
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.276
	.quad	10498                   # 0x2902
	.zero	16
	.zero	16
	.quad	.L.str.277
	.quad	8726                    # 0x2216
	.quad	.L.str.278
	.quad	9578                    # 0x256a
	.quad	.L.str.279
	.quad	8784                    # 0x2250
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.280
	.quad	8882                    # 0x22b2
	.zero	16
	.quad	.L.str.281
	.quad	10704                   # 0x29d0
	.quad	.L.str.282
	.quad	173                     # 0xad
	.quad	.L.str.283
	.quad	10580                   # 0x2954
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.284
	.quad	922                     # 0x39a
	.zero	16
	.quad	.L.str.285
	.quad	8646                    # 0x21c6
	.quad	.L.str.286
	.quad	8848                    # 0x2290
	.quad	.L.str.287
	.quad	8474                    # 0x211a
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.288
	.quad	162                     # 0xa2
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.289
	.quad	12314                   # 0x301a
	.zero	16
	.zero	16
	.quad	.L.str.290
	.quad	9140                    # 0x23b4
	.zero	16
	.zero	16
	.quad	.L.str.291
	.quad	12315                   # 0x301b
	.quad	.L.str.292
	.quad	8497                    # 0x2131
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.293
	.quad	9666                    # 0x25c2
	.quad	.L.str.294
	.quad	8783                    # 0x224f
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.295
	.quad	954                     # 0x3ba
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.296
	.quad	8472                    # 0x2118
	.zero	16
	.quad	.L.str.297
	.quad	8890                    # 0x22ba
	.quad	.L.str.298
	.quad	10547                   # 0x2933
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.299
	.quad	8749                    # 0x222d
	.zero	16
	.quad	.L.str.300
	.quad	9484                    # 0x250c
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.301
	.quad	37                      # 0x25
	.quad	.L.str.302
	.quad	8868                    # 0x22a4
	.zero	16
	.quad	.L.str.303
	.quad	64259                   # 0xfb03
	.quad	.L.str.304
	.quad	8957                    # 0x22fd
	.zero	16
	.zero	16
	.quad	.L.str.305
	.quad	9830                    # 0x2666
	.zero	16
	.quad	.L.str.306
	.quad	10016                   # 0x2720
	.quad	.L.str.307
	.quad	8883                    # 0x22b3
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.308
	.quad	65077                   # 0xfe35
	.quad	.L.str.309
	.quad	10536                   # 0x2928
	.quad	.L.str.310
	.quad	8806                    # 0x2266
	.zero	16
	.quad	.L.str.311
	.quad	8597                    # 0x2195
	.quad	.L.str.312
	.quad	8737                    # 0x2221
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.313
	.quad	8656                    # 0x21d0
	.quad	.L.str.314
	.quad	8782                    # 0x224e
	.quad	.L.str.315
	.quad	188                     # 0xbc
	.zero	16
	.quad	.L.str.316
	.quad	8639                    # 0x21bf
	.quad	.L.str.317
	.quad	10754                   # 0x2a02
	.quad	.L.str.318
	.quad	8799                    # 0x225f
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.319
	.quad	8807                    # 0x2267
	.zero	16
	.quad	.L.str.320
	.quad	8712                    # 0x2208
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.321
	.quad	8711                    # 0x2207
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.322
	.quad	1063                    # 0x427
	.quad	.L.str.323
	.quad	8843                    # 0x228b
	.zero	16
	.quad	.L.str.324
	.quad	8592                    # 0x2190
	.quad	.L.str.325
	.quad	10878                   # 0x2a7e
	.quad	.L.str.326
	.quad	8976                    # 0x2310
	.quad	.L.str.327
	.quad	8888                    # 0x22b8
	.quad	.L.str.328
	.quad	8884                    # 0x22b4
	.quad	.L.str.329
	.quad	1061                    # 0x425
	.quad	.L.str.330
	.quad	10877                   # 0x2a7d
	.quad	.L.str.331
	.quad	9651                    # 0x25b3
	.zero	16
	.quad	.L.str.332
	.quad	711                     # 0x2c7
	.zero	16
	.quad	.L.str.333
	.quad	8770                    # 0x2242
	.quad	.L.str.334
	.quad	818                     # 0x332
	.quad	.L.str.335
	.quad	1064                    # 0x428
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.336
	.quad	1046                    # 0x416
	.quad	.L.str.337
	.quad	8923                    # 0x22db
	.quad	.L.str.338
	.quad	8897                    # 0x22c1
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.339
	.quad	8842                    # 0x228a
	.zero	16
	.quad	.L.str.340
	.quad	8815                    # 0x226f
	.quad	.L.str.341
	.quad	8948                    # 0x22f4
	.zero	16
	.zero	16
	.quad	.L.str.342
	.quad	8736                    # 0x2220
	.zero	16
	.zero	16
	.quad	.L.str.343
	.quad	8201                    # 0x2009
	.quad	.L.str.344
	.quad	8720                    # 0x2210
	.zero	16
	.quad	.L.str.345
	.quad	10950                   # 0x2ac6
	.quad	.L.str.346
	.quad	10928                   # 0x2ab0
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.347
	.quad	8744                    # 0x2228
	.zero	16
	.quad	.L.str.348
	.quad	8778                    # 0x224a
	.quad	.L.str.349
	.quad	8610                    # 0x21a2
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.350
	.quad	10597                   # 0x2965
	.quad	.L.str.351
	.quad	10837                   # 0x2a55
	.quad	.L.str.352
	.quad	8601                    # 0x2199
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.353
	.quad	8791                    # 0x2257
	.zero	16
	.quad	.L.str.354
	.quad	8624                    # 0x21b0
	.quad	.L.str.355
	.quad	171                     # 0xab
	.quad	.L.str.356
	.quad	8788                    # 0x2254
	.quad	.L.str.357
	.quad	8647                    # 0x21c7
	.quad	.L.str.358
	.quad	8748                    # 0x222c
	.quad	.L.str.359
	.quad	8602                    # 0x219a
	.quad	.L.str.360
	.quad	8625                    # 0x21b1
	.quad	.L.str.361
	.quad	949                     # 0x3b5
	.quad	.L.str.362
	.quad	8634                    # 0x21ba
	.quad	.L.str.363
	.quad	187                     # 0xbb
	.quad	.L.str.364
	.quad	8592                    # 0x2190
	.quad	.L.str.365
	.quad	8644                    # 0x21c4
	.quad	.L.str.366
	.quad	9560                    # 0x2558
	.quad	.L.str.367
	.quad	8722                    # 0x2212
	.quad	.L.str.368
	.quad	10229                   # 0x27f5
	.quad	.L.str.369
	.quad	10594                   # 0x2962
	.quad	.L.str.370
	.quad	10596                   # 0x2964
	.quad	.L.str.371
	.quad	10949                   # 0x2ac5
	.zero	16
	.quad	.L.str.372
	.quad	10595                   # 0x2963
	.quad	.L.str.373
	.quad	1013                    # 0x3f5
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.374
	.quad	8726                    # 0x2216
	.quad	.L.str.375
	.quad	61                      # 0x3d
	.zero	16
	.quad	.L.str.376
	.quad	8708                    # 0x2204
	.quad	.L.str.377
	.quad	8936                    # 0x22e8
	.quad	.L.str.378
	.quad	8624                    # 0x21b0
	.quad	.L.str.379
	.quad	8927                    # 0x22df
	.zero	16
	.zero	16
	.quad	.L.str.380
	.quad	8747                    # 0x222b
	.zero	16
	.quad	.L.str.381
	.quad	8625                    # 0x21b1
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.382
	.quad	10877                   # 0x2a7d
	.quad	.L.str.383
	.quad	729                     # 0x2d9
	.zero	16
	.quad	.L.str.384
	.quad	8877                    # 0x22ad
	.quad	.L.str.385
	.quad	10937                   # 0x2ab9
	.quad	.L.str.386
	.quad	8203                    # 0x200b
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.387
	.quad	8853                    # 0x2295
	.zero	16
	.zero	16
	.quad	.L.str.388
	.quad	183                     # 0xb7
	.zero	16
	.quad	.L.str.389
	.quad	8709                    # 0x2205
	.quad	.L.str.390
	.quad	8846                    # 0x228e
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.391
	.quad	9524                    # 0x2534
	.zero	16
	.zero	16
	.quad	.L.str.392
	.quad	1100                    # 0x44c
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.393
	.quad	9642                    # 0x25aa
	.quad	.L.str.394
	.quad	8411                    # 0x20db
	.quad	.L.str.395
	.quad	8868                    # 0x22a4
	.quad	.L.str.396
	.quad	9675                    # 0x25cb
	.quad	.L.str.397
	.quad	8496                    # 0x2130
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.398
	.quad	10768                   # 0x2a10
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.399
	.quad	8994                    # 0x2322
	.quad	.L.str.400
	.quad	8939                    # 0x22eb
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.401
	.quad	8954                    # 0x22fa
	.quad	.L.str.402
	.quad	8208                    # 0x2010
	.zero	16
	.zero	16
	.quad	.L.str.403
	.quad	10500                   # 0x2904
	.zero	16
	.zero	16
	.quad	.L.str.404
	.quad	8259                    # 0x2043
	.quad	.L.str.405
	.quad	8677                    # 0x21e5
	.quad	.L.str.406
	.quad	8621                    # 0x21ad
	.quad	.L.str.407
	.quad	8863                    # 0x229f
	.zero	16
	.quad	.L.str.408
	.quad	8540                    # 0x215c
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.409
	.quad	183                     # 0xb7
	.quad	.L.str.410
	.quad	10535                   # 0x2927
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.411
	.quad	10553                   # 0x2939
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.412
	.quad	8617                    # 0x21a9
	.quad	.L.str.413
	.quad	8230                    # 0x2026
	.quad	.L.str.414
	.quad	10565                   # 0x2945
	.quad	.L.str.415
	.quad	8739                    # 0x2223
	.zero	16
	.quad	.L.str.416
	.quad	8859                    # 0x229b
	.quad	.L.str.417
	.quad	8777                    # 0x2249
	.zero	16
	.quad	.L.str.418
	.quad	8769                    # 0x2241
	.quad	.L.str.419
	.quad	974                     # 0x3ce
	.quad	.L.str.420
	.quad	9646                    # 0x25ae
	.quad	.L.str.421
	.quad	9001                    # 0x2329
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.422
	.quad	8942                    # 0x22ee
	.quad	.L.str.423
	.quad	8941                    # 0x22ed
	.quad	.L.str.424
	.quad	9002                    # 0x232a
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.425
	.quad	8935                    # 0x22e7
	.quad	.L.str.426
	.quad	10016                   # 0x2720
	.quad	.L.str.427
	.quad	215                     # 0xd7
	.quad	.L.str.428
	.quad	10589                   # 0x295d
	.zero	16
	.quad	.L.str.429
	.quad	8934                    # 0x22e6
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.430
	.quad	10927                   # 0x2aaf
	.quad	.L.str.431
	.quad	8801                    # 0x2261
	.quad	.L.str.432
	.quad	8807                    # 0x2267
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.433
	.quad	921                     # 0x399
	.quad	.L.str.434
	.quad	8806                    # 0x2266
	.zero	16
	.quad	.L.str.435
	.quad	8926                    # 0x22de
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.436
	.quad	8864                    # 0x22a0
	.quad	.L.str.437
	.quad	8470                    # 0x2116
	.quad	.L.str.438
	.quad	8726                    # 0x2216
	.zero	16
	.quad	.L.str.439
	.quad	8704                    # 0x2200
	.zero	16
	.quad	.L.str.440
	.quad	33                      # 0x21
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.441
	.quad	92                      # 0x5c
	.quad	.L.str.442
	.quad	8887                    # 0x22b7
	.quad	.L.str.443
	.quad	10742                   # 0x29f6
	.quad	.L.str.444
	.quad	8291                    # 0x2063
	.quad	.L.str.445
	.quad	8715                    # 0x220b
	.quad	.L.str.446
	.quad	8707                    # 0x2203
	.zero	16
	.quad	.L.str.447
	.quad	11005                   # 0x2afd
	.quad	.L.str.448
	.quad	8245                    # 0x2035
	.quad	.L.str.449
	.quad	199                     # 0xc7
	.quad	.L.str.450
	.quad	8827                    # 0x227b
	.quad	.L.str.451
	.quad	8733                    # 0x221d
	.zero	16
	.quad	.L.str.452
	.quad	8856                    # 0x2298
	.quad	.L.str.453
	.quad	10579                   # 0x2953
	.quad	.L.str.454
	.quad	953                     # 0x3b9
	.quad	.L.str.455
	.quad	9556                    # 0x2554
	.zero	16
	.zero	16
	.quad	.L.str.456
	.quad	920                     # 0x398
	.quad	.L.str.457
	.quad	8636                    # 0x21bc
	.quad	.L.str.458
	.quad	8757                    # 0x2235
	.quad	.L.str.459
	.quad	8279                    # 0x2057
	.quad	.L.str.460
	.quad	10610                   # 0x2972
	.zero	16
	.quad	.L.str.461
	.quad	8244                    # 0x2034
	.quad	.L.str.462
	.quad	8640                    # 0x21c0
	.quad	.L.str.463
	.quad	8242                    # 0x2032
	.quad	.L.str.464
	.quad	1070                    # 0x42e
	.quad	.L.str.465
	.quad	177                     # 0xb1
	.zero	16
	.quad	.L.str.466
	.quad	8462                    # 0x210e
	.quad	.L.str.467
	.quad	1031                    # 0x407
	.zero	16
	.quad	.L.str.468
	.quad	8939                    # 0x22eb
	.quad	.L.str.469
	.quad	8757                    # 0x2235
	.zero	16
	.quad	.L.str.470
	.quad	10927                   # 0x2aaf
	.zero	16
	.zero	16
	.quad	.L.str.471
	.quad	8841                    # 0x2289
	.quad	.L.str.472
	.quad	231                     # 0xe7
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.473
	.quad	1103                    # 0x44f
	.quad	.L.str.474
	.quad	8937                    # 0x22e9
	.quad	.L.str.475
	.quad	952                     # 0x3b8
	.quad	.L.str.476
	.quad	8757                    # 0x2235
	.quad	.L.str.477
	.quad	10927                   # 0x2aaf
	.zero	16
	.quad	.L.str.478
	.quad	9839                    # 0x266f
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.479
	.quad	8864                    # 0x22a0
	.quad	.L.str.480
	.quad	10232                   # 0x27f8
	.quad	.L.str.481
	.quad	8730                    # 0x221a
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.482
	.quad	8214                    # 0x2016
	.zero	16
	.quad	.L.str.483
	.quad	929                     # 0x3a1
	.zero	16
	.quad	.L.str.484
	.quad	10593                   # 0x2961
	.quad	.L.str.485
	.quad	8787                    # 0x2253
	.zero	16
	.quad	.L.str.486
	.quad	8484                    # 0x2124
	.quad	.L.str.487
	.quad	168                     # 0xa8
	.quad	.L.str.488
	.quad	10725                   # 0x29e5
	.zero	16
	.zero	16
	.quad	.L.str.489
	.quad	8786                    # 0x2252
	.zero	16
	.quad	.L.str.490
	.quad	10                      # 0xa
	.quad	.L.str.491
	.quad	8218                    # 0x201a
	.zero	16
	.quad	.L.str.492
	.quad	8900                    # 0x22c4
	.quad	.L.str.493
	.quad	10988                   # 0x2aec
	.zero	16
	.quad	.L.str.494
	.quad	919                     # 0x397
	.quad	.L.str.495
	.quad	10229                   # 0x27f5
	.zero	16
	.zero	16
	.quad	.L.str.496
	.quad	9733                    # 0x2605
	.quad	.L.str.497
	.quad	10608                   # 0x2970
	.quad	.L.str.498
	.quad	9002                    # 0x232a
	.zero	16
	.quad	.L.str.499
	.quad	9830                    # 0x2666
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.500
	.quad	124                     # 0x7c
	.quad	.L.str.501
	.quad	240                     # 0xf0
	.quad	.L.str.502
	.quad	8716                    # 0x220c
	.quad	.L.str.503
	.quad	10234                   # 0x27fa
	.quad	.L.str.504
	.quad	8995                    # 0x2323
	.quad	.L.str.505
	.quad	8869                    # 0x22a5
	.quad	.L.str.506
	.quad	8839                    # 0x2287
	.quad	.L.str.507
	.quad	729                     # 0x2d9
	.quad	.L.str.508
	.quad	10773                   # 0x2a15
	.quad	.L.str.509
	.quad	8890                    # 0x22ba
	.zero	16
	.zero	16
	.quad	.L.str.510
	.quad	961                     # 0x3c1
	.zero	16
	.quad	.L.str.511
	.quad	8473                    # 0x2119
	.quad	.L.str.512
	.quad	64258                   # 0xfb02
	.zero	16
	.quad	.L.str.513
	.quad	8900                    # 0x22c4
	.quad	.L.str.514
	.quad	172                     # 0xac
	.quad	.L.str.515
	.quad	10935                   # 0x2ab7
	.quad	.L.str.516
	.quad	951                     # 0x3b7
	.zero	16
	.quad	.L.str.517
	.quad	8245                    # 0x2035
	.quad	.L.str.518
	.quad	8923                    # 0x22db
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.519
	.quad	8614                    # 0x21a6
	.quad	.L.str.520
	.quad	9572                    # 0x2564
	.quad	.L.str.521
	.quad	988                     # 0x3dc
	.zero	16
	.zero	16
	.quad	.L.str.522
	.quad	8788                    # 0x2254
	.quad	.L.str.523
	.quad	9642                    # 0x25aa
	.quad	.L.str.524
	.quad	9084                    # 0x237c
	.quad	.L.str.525
	.quad	10231                   # 0x27f7
	.zero	16
	.quad	.L.str.526
	.quad	163                     # 0xa3
	.quad	.L.str.527
	.quad	9014                    # 0x2336
	.quad	.L.str.528
	.quad	8606                    # 0x219e
	.quad	.L.str.529
	.quad	8801                    # 0x2261
	.quad	.L.str.530
	.quad	8742                    # 0x2226
	.quad	.L.str.531
	.quad	8646                    # 0x21c6
	.zero	16
	.quad	.L.str.532
	.quad	8838                    # 0x2286
	.zero	16
	.quad	.L.str.533
	.quad	8459                    # 0x210b
	.zero	16
	.zero	16
	.quad	.L.str.534
	.quad	10568                   # 0x2948
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.535
	.quad	9553                    # 0x2551
	.zero	16
	.zero	16
	.quad	.L.str.536
	.quad	989                     # 0x3dd
	.quad	.L.str.537
	.quad	8220                    # 0x201c
	.zero	16
	.quad	.L.str.538
	.quad	8982                    # 0x2316
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.539
	.quad	10758                   # 0x2a06
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.540
	.quad	9567                    # 0x255f
	.zero	16
	.zero	16
	.quad	.L.str.541
	.quad	951                     # 0x3b7
	.quad	.L.str.542
	.quad	10769                   # 0x2a11
	.quad	.L.str.543
	.quad	905                     # 0x389
	.quad	.L.str.544
	.quad	8753                    # 0x2231
	.zero	16
	.zero	16
	.quad	.L.str.545
	.quad	8640                    # 0x21c0
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.546
	.quad	8874                    # 0x22aa
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.547
	.quad	193                     # 0xc1
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.548
	.quad	201                     # 0xc9
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.549
	.quad	205                     # 0xcd
	.quad	.L.str.550
	.quad	9577                    # 0x2569
	.quad	.L.str.551
	.quad	8504                    # 0x2138
	.quad	.L.str.552
	.quad	10683                   # 0x29bb
	.quad	.L.str.553
	.quad	8790                    # 0x2256
	.zero	16
	.quad	.L.str.554
	.quad	211                     # 0xd3
	.quad	.L.str.555
	.quad	10752                   # 0x2a00
	.quad	.L.str.556
	.quad	8713                    # 0x2209
	.quad	.L.str.557
	.quad	8596                    # 0x2194
	.quad	.L.str.558
	.quad	9674                    # 0x25ca
	.zero	16
	.quad	.L.str.559
	.quad	218                     # 0xda
	.quad	.L.str.560
	.quad	8876                    # 0x22ac
	.zero	16
	.zero	16
	.quad	.L.str.561
	.quad	221                     # 0xdd
	.quad	.L.str.562
	.quad	9569                    # 0x2561
	.quad	.L.str.563
	.quad	8492                    # 0x212c
	.zero	16
	.zero	16
	.quad	.L.str.564
	.quad	8496                    # 0x2130
	.quad	.L.str.565
	.quad	8497                    # 0x2131
	.quad	.L.str.566
	.quad	711                     # 0x2c7
	.quad	.L.str.567
	.quad	8459                    # 0x210b
	.quad	.L.str.568
	.quad	8464                    # 0x2110
	.quad	.L.str.569
	.quad	9579                    # 0x256b
	.quad	.L.str.570
	.quad	225                     # 0xe1
	.quad	.L.str.571
	.quad	8466                    # 0x2112
	.quad	.L.str.572
	.quad	8499                    # 0x2133
	.quad	.L.str.573
	.quad	8659                    # 0x21d3
	.quad	.L.str.574
	.quad	233                     # 0xe9
	.quad	.L.str.575
	.quad	237                     # 0xed
	.quad	.L.str.576
	.quad	124                     # 0x7c
	.quad	.L.str.577
	.quad	8475                    # 0x211b
	.quad	.L.str.578
	.quad	8776                    # 0x2248
	.quad	.L.str.579
	.quad	8203                    # 0x200b
	.quad	.L.str.580
	.quad	8818                    # 0x2272
	.quad	.L.str.581
	.quad	243                     # 0xf3
	.quad	.L.str.582
	.quad	9618                    # 0x2592
	.quad	.L.str.583
	.quad	8534                    # 0x2156
	.quad	.L.str.584
	.quad	8723                    # 0x2213
	.quad	.L.str.585
	.quad	402                     # 0x192
	.quad	.L.str.586
	.quad	8751                    # 0x222f
	.quad	.L.str.587
	.quad	250                     # 0xfa
	.quad	.L.str.588
	.quad	8518                    # 0x2146
	.quad	.L.str.589
	.quad	8831                    # 0x227f
	.quad	.L.str.590
	.quad	8833                    # 0x2281
	.quad	.L.str.591
	.quad	253                     # 0xfd
	.zero	16
	.zero	16
	.quad	.L.str.592
	.quad	8472                    # 0x2118
	.quad	.L.str.593
	.quad	8734                    # 0x221e
	.quad	.L.str.594
	.quad	8495                    # 0x212f
	.zero	16
	.quad	.L.str.595
	.quad	1026                    # 0x402
	.quad	.L.str.596
	.quad	8458                    # 0x210a
	.quad	.L.str.597
	.quad	8600                    # 0x2198
	.quad	.L.str.598
	.quad	1027                    # 0x403
	.quad	.L.str.599
	.quad	1119                    # 0x45f
	.quad	.L.str.600
	.quad	166                     # 0xa6
	.quad	.L.str.601
	.quad	8852                    # 0x2294
	.quad	.L.str.602
	.quad	1036                    # 0x40c
	.quad	.L.str.603
	.quad	1033                    # 0x409
	.quad	.L.str.604
	.quad	8500                    # 0x2134
	.quad	.L.str.605
	.quad	1034                    # 0x40a
	.quad	.L.str.606
	.quad	8225                    # 0x2021
	.quad	.L.str.607
	.quad	192                     # 0xc0
	.quad	.L.str.608
	.quad	8595                    # 0x2193
	.zero	16
	.quad	.L.str.609
	.quad	10607                   # 0x296f
	.quad	.L.str.610
	.quad	200                     # 0xc8
	.quad	.L.str.611
	.quad	216                     # 0xd8
	.zero	16
	.quad	.L.str.612
	.quad	8828                    # 0x227c
	.quad	.L.str.613
	.quad	204                     # 0xcc
	.quad	.L.str.614
	.quad	8750                    # 0x222e
	.quad	.L.str.615
	.quad	10512                   # 0x2910
	.quad	.L.str.616
	.quad	10591                   # 0x295f
	.quad	.L.str.617
	.quad	8857                    # 0x2299
	.zero	16
	.quad	.L.str.618
	.quad	210                     # 0xd2
	.quad	.L.str.619
	.quad	8918                    # 0x22d6
	.quad	.L.str.620
	.quad	9001                    # 0x2329
	.quad	.L.str.621
	.quad	8411                    # 0x20db
	.quad	.L.str.622
	.quad	912                     # 0x390
	.quad	.L.str.623
	.quad	8901                    # 0x22c5
	.quad	.L.str.624
	.quad	217                     # 0xd9
	.quad	.L.str.625
	.quad	9667                    # 0x25c3
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.626
	.quad	8900                    # 0x22c4
	.quad	.L.str.627
	.quad	64                      # 0x40
	.quad	.L.str.628
	.quad	944                     # 0x3b0
	.quad	.L.str.629
	.quad	8676                    # 0x21e4
	.quad	.L.str.630
	.quad	8463                    # 0x210f
	.quad	.L.str.631
	.quad	8644                    # 0x21c4
	.quad	.L.str.632
	.quad	10938                   # 0x2aba
	.quad	.L.str.633
	.quad	224                     # 0xe0
	.zero	16
	.quad	.L.str.634
	.quad	10902                   # 0x2a96
	.zero	16
	.quad	.L.str.635
	.quad	232                     # 0xe8
	.quad	.L.str.636
	.quad	248                     # 0xf8
	.zero	16
	.quad	.L.str.637
	.quad	8817                    # 0x2271
	.quad	.L.str.638
	.quad	236                     # 0xec
	.quad	.L.str.639
	.quad	8463                    # 0x210f
	.zero	16
	.quad	.L.str.640
	.quad	8810                    # 0x226a
	.zero	16
	.quad	.L.str.641
	.quad	8708                    # 0x2204
	.quad	.L.str.642
	.quad	242                     # 0xf2
	.zero	16
	.quad	.L.str.643
	.quad	8222                    # 0x201e
	.zero	16
	.zero	16
	.quad	.L.str.644
	.quad	8221                    # 0x201d
	.quad	.L.str.645
	.quad	249                     # 0xf9
	.quad	.L.str.646
	.quad	247                     # 0xf7
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.647
	.quad	9662                    # 0x25be
	.quad	.L.str.648
	.quad	8593                    # 0x2191
	.zero	16
	.zero	16
	.quad	.L.str.649
	.quad	8715                    # 0x220b
	.quad	.L.str.650
	.quad	10603                   # 0x296b
	.quad	.L.str.651
	.quad	9488                    # 0x2510
	.quad	.L.str.652
	.quad	982                     # 0x3d6
	.zero	16
	.quad	.L.str.653
	.quad	8832                    # 0x2280
	.quad	.L.str.654
	.quad	10508                   # 0x290c
	.zero	16
	.quad	.L.str.655
	.quad	10844                   # 0x2a5c
	.quad	.L.str.656
	.quad	8896                    # 0x22c0
	.quad	.L.str.657
	.quad	8290                    # 0x2062
	.zero	16
	.quad	.L.str.658
	.quad	10509                   # 0x290d
	.zero	16
	.quad	.L.str.659
	.quad	8289                    # 0x2061
	.quad	.L.str.660
	.quad	8869                    # 0x22a5
	.quad	.L.str.661
	.quad	8755                    # 0x2233
	.quad	.L.str.662
	.quad	8754                    # 0x2232
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.663
	.quad	10662                   # 0x29a6
	.quad	.L.str.664
	.quad	8677                    # 0x21e5
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.665
	.quad	10514                   # 0x2912
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.666
	.quad	8475                    # 0x211b
	.quad	.L.str.667
	.quad	10663                   # 0x29a7
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.668
	.quad	95                      # 0x5f
	.zero	16
	.zero	16
	.quad	.L.str.669
	.quad	8493                    # 0x212d
	.zero	16
	.quad	.L.str.670
	.quad	9733                    # 0x2605
	.quad	.L.str.671
	.quad	8712                    # 0x2208
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.672
	.quad	1014                    # 0x3f6
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.673
	.quad	8216                    # 0x2018
	.zero	16
	.quad	.L.str.674
	.quad	10799                   # 0x2a2f
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.675
	.quad	8922                    # 0x22da
	.quad	.L.str.676
	.quad	978                     # 0x3d2
	.quad	.L.str.677
	.quad	8658                    # 0x21d2
	.zero	16
	.zero	16
	.quad	.L.str.678
	.quad	8466                    # 0x2112
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.679
	.quad	9711                    # 0x25ef
	.zero	16
	.zero	16
	.quad	.L.str.680
	.quad	1013                    # 0x3f5
	.quad	.L.str.681
	.quad	10586                   # 0x295a
	.zero	16
	.zero	16
	.quad	.L.str.682
	.quad	8661                    # 0x21d5
	.zero	16
	.quad	.L.str.683
	.quad	96                      # 0x60
	.quad	.L.str.684
	.quad	9653                    # 0x25b5
	.zero	16
	.quad	.L.str.685
	.quad	8240                    # 0x2030
	.zero	16
	.quad	.L.str.686
	.quad	10656                   # 0x29a0
	.quad	.L.str.687
	.quad	10606                   # 0x296e
	.quad	.L.str.688
	.quad	8909                    # 0x22cd
	.quad	.L.str.689
	.quad	8913                    # 0x22d1
	.quad	.L.str.690
	.quad	9561                    # 0x2559
	.quad	.L.str.691
	.quad	8829                    # 0x227d
	.quad	.L.str.692
	.quad	10644                   # 0x2994
	.quad	.L.str.693
	.quad	10007                   # 0x2717
	.quad	.L.str.694
	.quad	965                     # 0x3c5
	.zero	16
	.quad	.L.str.695
	.quad	8630                    # 0x21b6
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.696
	.quad	1008                    # 0x3f0
	.quad	.L.str.697
	.quad	189                     # 0xbd
	.zero	16
	.zero	16
	.quad	.L.str.698
	.quad	962                     # 0x3c2
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.699
	.quad	8520                    # 0x2148
	.zero	16
	.zero	16
	.quad	.L.str.700
	.quad	932                     # 0x3a4
	.quad	.L.str.701
	.quad	8492                    # 0x212c
	.quad	.L.str.702
	.quad	8656                    # 0x21d0
	.zero	16
	.zero	16
	.quad	.L.str.703
	.quad	8835                    # 0x2283
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.704
	.quad	8287                    # 0x205f
	.zero	16
	.quad	.L.str.705
	.quad	8912                    # 0x22d0
	.quad	.L.str.706
	.quad	8203                    # 0x200b
	.zero	16
	.zero	16
	.quad	.L.str.707
	.quad	9563                    # 0x255b
	.zero	16
	.zero	16
	.quad	.L.str.708
	.quad	8713                    # 0x2209
	.zero	16
	.quad	.L.str.709
	.quad	8476                    # 0x211c
	.zero	16
	.quad	.L.str.710
	.quad	8816                    # 0x2270
	.quad	.L.str.711
	.quad	8654                    # 0x21ce
	.zero	16
	.zero	16
	.quad	.L.str.712
	.quad	914                     # 0x392
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.713
	.quad	36                      # 0x24
	.quad	.L.str.714
	.quad	8719                    # 0x220f
	.quad	.L.str.715
	.quad	964                     # 0x3c4
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.716
	.quad	8460                    # 0x210c
	.quad	.L.str.717
	.quad	8519                    # 0x2147
	.quad	.L.str.718
	.quad	183                     # 0xb7
	.quad	.L.str.719
	.quad	1032                    # 0x408
	.quad	.L.str.720
	.quad	8805                    # 0x2265
	.quad	.L.str.721
	.quad	10518                   # 0x2916
	.quad	.L.str.722
	.quad	8834                    # 0x2282
	.zero	16
	.quad	.L.str.723
	.quad	8804                    # 0x2264
	.zero	16
	.quad	.L.str.724
	.quad	8800                    # 0x2260
	.quad	.L.str.725
	.quad	8612                    # 0x21a4
	.quad	.L.str.726
	.quad	8929                    # 0x22e1
	.quad	.L.str.727
	.quad	978                     # 0x3d2
	.quad	.L.str.728
	.quad	184                     # 0xb8
	.quad	.L.str.729
	.quad	918                     # 0x396
	.zero	16
	.zero	16
	.quad	.L.str.730
	.quad	39                      # 0x27
	.quad	.L.str.731
	.quad	8840                    # 0x2288
	.quad	.L.str.732
	.quad	946                     # 0x3b2
	.quad	.L.str.733
	.quad	8542                    # 0x215e
	.quad	.L.str.734
	.quad	8938                    # 0x22ea
	.quad	.L.str.735
	.quad	10764                   # 0x2a0c
	.zero	16
	.quad	.L.str.736
	.quad	8891                    # 0x22bb
	.zero	16
	.zero	16
	.quad	.L.str.737
	.quad	10587                   # 0x295b
	.quad	.L.str.738
	.quad	8225                    # 0x2021
	.quad	.L.str.739
	.quad	8882                    # 0x22b2
	.quad	.L.str.740
	.quad	8610                    # 0x21a2
	.quad	.L.str.741
	.quad	1105                    # 0x451
	.quad	.L.str.742
	.quad	8867                    # 0x22a3
	.quad	.L.str.743
	.quad	189                     # 0xbd
	.quad	.L.str.744
	.quad	8823                    # 0x2277
	.quad	.L.str.745
	.quad	1112                    # 0x458
	.quad	.L.str.746
	.quad	8611                    # 0x21a3
	.quad	.L.str.747
	.quad	8254                    # 0x203e
	.quad	.L.str.748
	.quad	179                     # 0xb3
	.quad	.L.str.749
	.quad	8196                    # 0x2004
	.quad	.L.str.750
	.quad	8776                    # 0x2248
	.zero	16
	.zero	16
	.quad	.L.str.751
	.quad	950                     # 0x3b6
	.quad	.L.str.752
	.quad	8599                    # 0x2197
	.zero	16
	.quad	.L.str.753
	.quad	8726                    # 0x2216
	.quad	.L.str.754
	.quad	10592                   # 0x2960
	.zero	16
	.zero	16
	.quad	.L.str.755
	.quad	8224                    # 0x2020
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.756
	.quad	8643                    # 0x21c3
	.quad	.L.str.757
	.quad	8771                    # 0x2243
	.quad	.L.str.758
	.quad	8830                    # 0x227e
	.quad	.L.str.759
	.quad	8723                    # 0x2213
	.zero	16
	.zero	16
	.quad	.L.str.760
	.quad	8636                    # 0x21bc
	.zero	16
	.quad	.L.str.761
	.quad	9006                    # 0x232e
	.quad	.L.str.762
	.quad	9663                    # 0x25bf
	.quad	.L.str.763
	.quad	8812                    # 0x226c
	.zero	16
	.zero	16
	.quad	.L.str.764
	.quad	8208                    # 0x2010
	.zero	16
	.quad	.L.str.765
	.quad	8203                    # 0x200b
	.quad	.L.str.766
	.quad	8650                    # 0x21ca
	.quad	.L.str.767
	.quad	10236                   # 0x27fc
	.quad	.L.str.768
	.quad	9667                    # 0x25c3
	.quad	.L.str.769
	.quad	8222                    # 0x201e
	.quad	.L.str.770
	.quad	8951                    # 0x22f7
	.zero	16
	.zero	16
	.quad	.L.str.771
	.quad	9657                    # 0x25b9
	.zero	16
	.zero	16
	.quad	.L.str.772
	.quad	8703                    # 0x21ff
	.quad	.L.str.773
	.quad	8724                    # 0x2214
	.quad	.L.str.774
	.quad	8220                    # 0x201c
	.quad	.L.str.775
	.quad	10753                   # 0x2a01
	.quad	.L.str.776
	.quad	8701                    # 0x21fd
	.quad	.L.str.777
	.quad	9838                    # 0x266e
	.quad	.L.str.778
	.quad	9653                    # 0x25b5
	.zero	16
	.quad	.L.str.779
	.quad	8221                    # 0x201d
	.zero	16
	.quad	.L.str.780
	.quad	8702                    # 0x21fe
	.quad	.L.str.781
	.quad	8739                    # 0x2223
	.zero	16
	.zero	16
	.quad	.L.str.782
	.quad	8928                    # 0x22e0
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.783
	.quad	437                     # 0x1b5
	.quad	.L.str.784
	.quad	8201                    # 0x2009
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.785
	.quad	8659                    # 0x21d3
	.quad	.L.str.786
	.quad	8990                    # 0x231e
	.zero	16
	.quad	.L.str.787
	.quad	10577                   # 0x2951
	.quad	.L.str.788
	.quad	8660                    # 0x21d4
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.789
	.quad	8656                    # 0x21d0
	.quad	.L.str.790
	.quad	8905                    # 0x22c9
	.zero	16
	.quad	.L.str.791
	.quad	8641                    # 0x21c1
	.quad	.L.str.792
	.quad	8855                    # 0x2297
	.quad	.L.str.793
	.quad	8866                    # 0x22a2
	.quad	.L.str.794
	.quad	8658                    # 0x21d2
	.quad	.L.str.795
	.quad	8906                    # 0x22ca
	.quad	.L.str.796
	.quad	962                     # 0x3c2
	.quad	.L.str.797
	.quad	8657                    # 0x21d1
	.quad	.L.str.798
	.quad	8988                    # 0x231c
	.quad	.L.str.799
	.quad	8661                    # 0x21d5
	.quad	.L.str.800
	.quad	223                     # 0xdf
	.quad	.L.str.801
	.quad	8873                    # 0x22a9
	.quad	.L.str.802
	.quad	8742                    # 0x2226
	.quad	.L.str.803
	.quad	8765                    # 0x223d
	.quad	.L.str.804
	.quad	10703                   # 0x29cf
	.zero	16
	.zero	16
	.quad	.L.str.805
	.quad	8819                    # 0x2273
	.quad	.L.str.806
	.quad	123                     # 0x7b
	.zero	16
	.zero	16
	.quad	.L.str.807
	.quad	8953                    # 0x22f9
	.quad	.L.str.808
	.quad	8818                    # 0x2272
	.zero	16
	.quad	.L.str.809
	.quad	125                     # 0x7d
	.quad	.L.str.810
	.quad	9552                    # 0x2550
	.zero	16
	.quad	.L.str.811
	.quad	8769                    # 0x2241
	.quad	.L.str.812
	.quad	10509                   # 0x290d
	.quad	.L.str.813
	.quad	8212                    # 0x2014
	.quad	.L.str.814
	.quad	8861                    # 0x229d
	.quad	.L.str.815
	.quad	9827                    # 0x2663
	.quad	.L.str.816
	.quad	8899                    # 0x22c3
	.quad	.L.str.817
	.quad	10531                   # 0x2923
	.quad	.L.str.818
	.quad	9559                    # 0x2557
	.quad	.L.str.819
	.quad	8211                    # 0x2013
	.zero	16
	.quad	.L.str.820
	.quad	8866                    # 0x22a2
	.quad	.L.str.821
	.quad	10534                   # 0x2926
	.zero	16
	.zero	16
	.quad	.L.str.822
	.quad	10731                   # 0x29eb
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.823
	.quad	8660                    # 0x21d4
	.zero	16
	.quad	.L.str.824
	.quad	8469                    # 0x2115
	.quad	.L.str.825
	.quad	8453                    # 0x2105
	.zero	16
	.quad	.L.str.826
	.quad	10575                   # 0x294f
	.zero	16
	.quad	.L.str.827
	.quad	8204                    # 0x200c
	.quad	.L.str.828
	.quad	8832                    # 0x2280
	.zero	16
	.zero	16
	.quad	.L.str.829
	.quad	8859                    # 0x229b
	.zero	16
	.quad	.L.str.830
	.quad	9829                    # 0x2665
	.quad	.L.str.831
	.quad	8784                    # 0x2250
	.quad	.L.str.832
	.quad	9661                    # 0x25bd
	.quad	.L.str.833
	.quad	8657                    # 0x21d1
	.quad	.L.str.834
	.quad	8919                    # 0x22d7
	.quad	.L.str.835
	.quad	8720                    # 0x2210
	.zero	16
	.quad	.L.str.836
	.quad	8858                    # 0x229a
	.quad	.L.str.837
	.quad	8750                    # 0x222e
	.quad	.L.str.838
	.quad	10764                   # 0x2a0c
	.zero	16
	.quad	.L.str.839
	.quad	8626                    # 0x21b2
	.quad	.L.str.840
	.quad	8482                    # 0x2122
	.quad	.L.str.841
	.quad	8651                    # 0x21cb
	.quad	.L.str.842
	.quad	8617                    # 0x21a9
	.quad	.L.str.843
	.quad	8749                    # 0x222d
	.zero	16
	.quad	.L.str.844
	.quad	8627                    # 0x21b3
	.quad	.L.str.845
	.quad	968                     # 0x3c8
	.quad	.L.str.846
	.quad	8618                    # 0x21aa
	.quad	.L.str.847
	.quad	936                     # 0x3a8
	.zero	16
	.quad	.L.str.848
	.quad	10611                   # 0x2973
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.849
	.quad	917                     # 0x395
	.zero	16
	.quad	.L.str.850
	.quad	10612                   # 0x2974
	.quad	.L.str.851
	.quad	9794                    # 0x2642
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.852
	.quad	8593                    # 0x2191
	.quad	.L.str.853
	.quad	352                     # 0x160
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.854
	.quad	933                     # 0x3a5
	.zero	16
	.quad	.L.str.855
	.quad	9829                    # 0x2665
	.zero	16
	.zero	16
	.quad	.L.str.856
	.quad	8770                    # 0x2242
	.quad	.L.str.857
	.quad	931                     # 0x3a3
	.quad	.L.str.858
	.quad	10836                   # 0x2a54
	.quad	.L.str.859
	.quad	8645                    # 0x21c5
	.quad	.L.str.860
	.quad	8812                    # 0x226c
	.quad	.L.str.861
	.quad	968                     # 0x3c8
	.zero	16
	.quad	.L.str.862
	.quad	8711                    # 0x2207
	.quad	.L.str.863
	.quad	8904                    # 0x22c8
	.zero	16
	.zero	16
	.quad	.L.str.864
	.quad	949                     # 0x3b5
	.zero	16
	.quad	.L.str.865
	.quad	10878                   # 0x2a7e
	.quad	.L.str.866
	.quad	8940                    # 0x22ec
	.quad	.L.str.867
	.quad	10863                   # 0x2a6f
	.zero	16
	.zero	16
	.quad	.L.str.868
	.quad	8638                    # 0x21be
	.zero	16
	.quad	.L.str.869
	.quad	353                     # 0x161
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.870
	.quad	8289                    # 0x2061
	.quad	.L.str.871
	.quad	965                     # 0x3c5
	.quad	.L.str.872
	.quad	8771                    # 0x2243
	.zero	16
	.quad	.L.str.873
	.quad	923                     # 0x39b
	.quad	.L.str.874
	.quad	8663                    # 0x21d7
	.quad	.L.str.875
	.quad	963                     # 0x3c3
	.quad	.L.str.876
	.quad	208                     # 0xd0
	.quad	.L.str.877
	.quad	8907                    # 0x22cb
	.quad	.L.str.878
	.quad	8664                    # 0x21d8
	.quad	.L.str.879
	.quad	8764                    # 0x223c
	.quad	.L.str.880
	.quad	8898                    # 0x22c2
	.quad	.L.str.881
	.quad	8736                    # 0x2220
	.quad	.L.str.882
	.quad	10550                   # 0x2936
	.quad	.L.str.883
	.quad	10551                   # 0x2937
	.quad	.L.str.884
	.quad	8768                    # 0x2240
	.zero	16
	.quad	.L.str.885
	.quad	8744                    # 0x2228
	.quad	.L.str.886
	.quad	8826                    # 0x227a
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.887
	.quad	8796                    # 0x225c
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.888
	.quad	8819                    # 0x2273
	.quad	.L.str.889
	.quad	8619                    # 0x21ab
	.quad	.L.str.890
	.quad	10230                   # 0x27f6
	.quad	.L.str.891
	.quad	8956                    # 0x22fc
	.quad	.L.str.892
	.quad	197                     # 0xc5
	.quad	.L.str.893
	.quad	10585                   # 0x2959
	.quad	.L.str.894
	.quad	8772                    # 0x2244
	.quad	.L.str.895
	.quad	955                     # 0x3bb
	.quad	.L.str.896
	.quad	8619                    # 0x21ab
	.quad	.L.str.897
	.quad	8637                    # 0x21bd
	.zero	16
	.zero	16
	.quad	.L.str.898
	.quad	178                     # 0xb2
	.zero	16
	.quad	.L.str.899
	.quad	8620                    # 0x21ac
	.quad	.L.str.900
	.quad	8728                    # 0x2218
	.quad	.L.str.901
	.quad	10989                   # 0x2aed
	.zero	16
	.zero	16
	.quad	.L.str.902
	.quad	8501                    # 0x2135
	.quad	.L.str.903
	.quad	9652                    # 0x25b4
	.zero	16
	.quad	.L.str.904
	.quad	8861                    # 0x229d
	.zero	16
	.zero	16
	.quad	.L.str.905
	.quad	1009                    # 0x3f1
	.zero	16
	.quad	.L.str.906
	.quad	8869                    # 0x22a5
	.quad	.L.str.907
	.quad	168                     # 0xa8
	.quad	.L.str.908
	.quad	8782                    # 0x224e
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.909
	.quad	8917                    # 0x22d5
	.quad	.L.str.910
	.quad	10890                   # 0x2a8a
	.quad	.L.str.911
	.quad	8971                    # 0x230b
	.quad	.L.str.912
	.quad	229                     # 0xe5
	.zero	16
	.quad	.L.str.913
	.quad	10889                   # 0x2a89
	.quad	.L.str.914
	.quad	8776                    # 0x2248
	.quad	.L.str.915
	.quad	10527                   # 0x291f
	.quad	.L.str.916
	.quad	40                      # 0x28
	.quad	.L.str.917
	.quad	8742                    # 0x2226
	.quad	.L.str.918
	.quad	10892                   # 0x2a8c
	.quad	.L.str.919
	.quad	9570                    # 0x2562
	.quad	.L.str.920
	.quad	8789                    # 0x2255
	.quad	.L.str.921
	.quad	10528                   # 0x2920
	.quad	.L.str.922
	.quad	41                      # 0x29
	.quad	.L.str.923
	.quad	8741                    # 0x2225
	.quad	.L.str.924
	.quad	8764                    # 0x223c
	.zero	16
	.quad	.L.str.925
	.quad	10581                   # 0x2955
	.zero	16
	.quad	.L.str.926
	.quad	182                     # 0xb6
	.quad	.L.str.927
	.quad	8776                    # 0x2248
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.928
	.quad	8835                    # 0x2283
	.zero	16
	.quad	.L.str.929
	.quad	8650                    # 0x21ca
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.930
	.quad	915                     # 0x393
	.quad	.L.str.931
	.quad	8533                    # 0x2155
	.quad	.L.str.932
	.quad	1008                    # 0x3f0
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.933
	.quad	8645                    # 0x21c5
	.quad	.L.str.934
	.quad	8756                    # 0x2234
	.quad	.L.str.935
	.quad	8202                    # 0x200a
	.zero	16
	.zero	16
	.quad	.L.str.936
	.quad	8854                    # 0x2296
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.937
	.quad	170                     # 0xaa
	.quad	.L.str.938
	.quad	8768                    # 0x2240
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.939
	.quad	10578                   # 0x2952
	.zero	16
	.zero	16
	.quad	.L.str.940
	.quad	9141                    # 0x23b5
	.zero	16
	.quad	.L.str.941
	.quad	947                     # 0x3b3
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.942
	.quad	168                     # 0xa8
	.quad	.L.str.943
	.quad	8968                    # 0x2308
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.944
	.quad	8926                    # 0x22de
	.quad	.L.str.945
	.quad	8709                    # 0x2205
	.quad	.L.str.946
	.quad	10994                   # 0x2af2
	.quad	.L.str.947
	.quad	8756                    # 0x2234
	.quad	.L.str.948
	.quad	8973                    # 0x230d
	.quad	.L.str.949
	.quad	8969                    # 0x2309
	.quad	.L.str.950
	.quad	8647                    # 0x21c7
	.quad	.L.str.951
	.quad	10652                   # 0x299c
	.zero	16
	.quad	.L.str.952
	.quad	8206                    # 0x200e
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.953
	.quad	8631                    # 0x21b7
	.quad	.L.str.954
	.quad	952                     # 0x3b8
	.quad	.L.str.955
	.quad	8706                    # 0x2202
	.quad	.L.str.956
	.quad	8879                    # 0x22af
	.quad	.L.str.957
	.quad	8793                    # 0x2259
	.zero	16
	.zero	16
	.quad	.L.str.958
	.quad	8975                    # 0x230f
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.959
	.quad	8669                    # 0x21dd
	.quad	.L.str.960
	.quad	10731                   # 0x29eb
	.quad	.L.str.961
	.quad	9834                    # 0x266a
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.962
	.quad	8660                    # 0x21d4
	.quad	.L.str.963
	.quad	8955                    # 0x22fb
	.quad	.L.str.964
	.quad	730                     # 0x2da
	.quad	.L.str.965
	.quad	10623                   # 0x297f
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.966
	.quad	8940                    # 0x22ec
	.quad	.L.str.967
	.quad	8990                    # 0x231e
	.zero	16
	.quad	.L.str.968
	.quad	10620                   # 0x297c
	.quad	.L.str.969
	.quad	8943                    # 0x22ef
	.quad	.L.str.970
	.quad	8945                    # 0x22f1
	.quad	.L.str.971
	.quad	8217                    # 0x2019
	.quad	.L.str.972
	.quad	8741                    # 0x2225
	.zero	16
	.quad	.L.str.973
	.quad	10621                   # 0x297d
	.quad	.L.str.974
	.quad	8988                    # 0x231c
	.zero	16
	.quad	.L.str.975
	.quad	10622                   # 0x297e
	.zero	16
	.quad	.L.str.976
	.quad	8950                    # 0x22f6
	.quad	.L.str.977
	.quad	8500                    # 0x2134
	.zero	16
	.quad	.L.str.978
	.quad	8667                    # 0x21db
	.quad	.L.str.979
	.quad	8195                    # 0x2003
	.quad	.L.str.980
	.quad	935                     # 0x3a7
	.zero	16
	.zero	16
	.quad	.L.str.981
	.quad	8944                    # 0x22f0
	.quad	.L.str.982
	.quad	8226                    # 0x2022
	.zero	16
	.quad	.L.str.983
	.quad	967                     # 0x3c7
	.quad	.L.str.984
	.quad	8854                    # 0x2296
	.quad	.L.str.985
	.quad	10902                   # 0x2a96
	.quad	.L.str.986
	.quad	8888                    # 0x22b8
	.quad	.L.str.987
	.quad	8727                    # 0x2217
	.quad	.L.str.988
	.quad	969                     # 0x3c9
	.quad	.L.str.989
	.quad	8814                    # 0x226e
	.quad	.L.str.990
	.quad	8501                    # 0x2135
	.quad	.L.str.991
	.quad	934                     # 0x3a6
	.quad	.L.str.992
	.quad	952                     # 0x3b8
	.zero	16
	.quad	.L.str.993
	.quad	8947                    # 0x22f3
	.quad	.L.str.994
	.quad	911                     # 0x38f
	.zero	16
	.quad	.L.str.995
	.quad	8713                    # 0x2209
	.quad	.L.str.996
	.quad	8599                    # 0x2197
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.997
	.quad	10980                   # 0x2ae4
	.quad	.L.str.998
	.quad	8603                    # 0x219b
	.zero	16
	.quad	.L.str.999
	.quad	8600                    # 0x2198
	.quad	.L.str.1000
	.quad	8613                    # 0x21a5
	.zero	16
	.quad	.L.str.1001
	.quad	10511                   # 0x290f
	.quad	.L.str.1002
	.quad	46                      # 0x2e
	.quad	.L.str.1003
	.quad	8921                    # 0x22d9
	.quad	.L.str.1004
	.quad	174                     # 0xae
	.quad	.L.str.1005
	.quad	785                     # 0x311
	.zero	16
	.quad	.L.str.1006
	.quad	8464                    # 0x2110
	.quad	.L.str.1007
	.quad	10888                   # 0x2a88
	.zero	16
	.zero	16
	.quad	.L.str.1008
	.quad	967                     # 0x3c7
	.zero	16
	.quad	.L.str.1009
	.quad	10887                   # 0x2a87
	.quad	.L.str.1010
	.quad	981                     # 0x3d5
	.zero	16
	.quad	.L.str.1011
	.quad	9824                    # 0x2660
	.zero	16
	.zero	16
	.quad	.L.str.1012
	.quad	8830                    # 0x227e
	.quad	.L.str.1013
	.quad	195                     # 0xc3
	.zero	16
	.zero	16
	.quad	.L.str.1014
	.quad	8712                    # 0x2208
	.quad	.L.str.1015
	.quad	8644                    # 0x21c4
	.quad	.L.str.1016
	.quad	8655                    # 0x21cf
	.zero	16
	.quad	.L.str.1017
	.quad	8491                    # 0x212b
	.zero	16
	.quad	.L.str.1018
	.quad	8865                    # 0x22a1
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1019
	.quad	209                     # 0xd1
	.quad	.L.str.1020
	.quad	8811                    # 0x226b
	.quad	.L.str.1021
	.quad	10233                   # 0x27f9
	.quad	.L.str.1022
	.quad	10234                   # 0x27fa
	.quad	.L.str.1023
	.quad	213                     # 0xd5
	.zero	16
	.quad	.L.str.1024
	.quad	8822                    # 0x2276
	.quad	.L.str.1025
	.quad	8676                    # 0x21e4
	.quad	.L.str.1026
	.quad	8776                    # 0x2248
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1027
	.quad	8936                    # 0x22e8
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1028
	.quad	8712                    # 0x2208
	.quad	.L.str.1029
	.quad	227                     # 0xe3
	.zero	16
	.quad	.L.str.1030
	.quad	8922                    # 0x22da
	.zero	16
	.quad	.L.str.1031
	.quad	10901                   # 0x2a95
	.quad	.L.str.1032
	.quad	9643                    # 0x25ab
	.quad	.L.str.1033
	.quad	966                     # 0x3c6
	.quad	.L.str.1034
	.quad	949                     # 0x3b5
	.quad	.L.str.1035
	.quad	8741                    # 0x2225
	.quad	.L.str.1036
	.quad	8882                    # 0x22b2
	.zero	16
	.quad	.L.str.1037
	.quad	9824                    # 0x2660
	.quad	.L.str.1038
	.quad	8532                    # 0x2154
	.quad	.L.str.1039
	.quad	241                     # 0xf1
	.quad	.L.str.1040
	.quad	245                     # 0xf5
	.zero	16
	.quad	.L.str.1041
	.quad	185                     # 0xb9
	.quad	.L.str.1042
	.quad	10914                   # 0x2aa2
	.quad	.L.str.1043
	.quad	1077                    # 0x435
	.quad	.L.str.1044
	.quad	8811                    # 0x226b
	.quad	.L.str.1045
	.quad	8970                    # 0x230a
	.quad	.L.str.1046
	.quad	8849                    # 0x2291
	.zero	16
	.quad	.L.str.1047
	.quad	180                     # 0xb4
	.quad	.L.str.1048
	.quad	1071                    # 0x42f
	.zero	16
	.quad	.L.str.1049
	.quad	8971                    # 0x230b
	.zero	16
	.zero	16
	.quad	.L.str.1050
	.quad	63                      # 0x3f
	.zero	16
	.quad	.L.str.1051
	.quad	8652                    # 0x21cc
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1052
	.quad	8805                    # 0x2265
	.quad	.L.str.1053
	.quad	8221                    # 0x201d
	.quad	.L.str.1054
	.quad	8218                    # 0x201a
	.zero	16
	.quad	.L.str.1055
	.quad	1115                    # 0x45b
	.quad	.L.str.1056
	.quad	8804                    # 0x2264
	.quad	.L.str.1057
	.quad	9564                    # 0x255c
	.quad	.L.str.1058
	.quad	10570                   # 0x294a
	.quad	.L.str.1059
	.quad	8217                    # 0x2019
	.quad	.L.str.1060
	.quad	10072                   # 0x2758
	.quad	.L.str.1061
	.quad	9554                    # 0x2552
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1062
	.quad	10936                   # 0x2ab8
	.quad	.L.str.1063
	.quad	10534                   # 0x2926
	.zero	16
	.quad	.L.str.1064
	.quad	8517                    # 0x2145
	.quad	.L.str.1065
	.quad	8288                    # 0x2060
	.quad	.L.str.1066
	.quad	8907                    # 0x22cb
	.quad	.L.str.1067
	.quad	9651                    # 0x25b3
	.quad	.L.str.1068
	.quad	65533                   # 0xfffd
	.zero	16
	.quad	.L.str.1069
	.quad	8704                    # 0x2200
	.quad	.L.str.1070
	.quad	8815                    # 0x226f
	.quad	.L.str.1071
	.quad	8908                    # 0x22cc
	.quad	.L.str.1072
	.quad	1028                    # 0x404
	.zero	16
	.quad	.L.str.1073
	.quad	1030                    # 0x406
	.zero	16
	.quad	.L.str.1074
	.quad	10892                   # 0x2a8c
	.zero	16
	.quad	.L.str.1075
	.quad	65078                   # 0xfe36
	.zero	16
	.quad	.L.str.1076
	.quad	8599                    # 0x2197
	.zero	16
	.zero	16
	.quad	.L.str.1077
	.quad	8869                    # 0x22a5
	.quad	.L.str.1078
	.quad	1014                    # 0x3f6
	.quad	.L.str.1079
	.quad	8600                    # 0x2198
	.quad	.L.str.1080
	.quad	161                     # 0xa1
	.quad	.L.str.1081
	.quad	8592                    # 0x2190
	.quad	.L.str.1082
	.quad	8203                    # 0x200b
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1083
	.quad	42                      # 0x2a
	.zero	16
	.quad	.L.str.1084
	.quad	8666                    # 0x21da
	.quad	.L.str.1085
	.quad	10583                   # 0x2957
	.quad	.L.str.1086
	.quad	8910                    # 0x22ce
	.zero	16
	.quad	.L.str.1087
	.quad	1110                    # 0x456
	.quad	.L.str.1088
	.quad	1108                    # 0x454
	.quad	.L.str.1089
	.quad	10521                   # 0x2919
	.quad	.L.str.1090
	.quad	8946                    # 0x22f2
	.zero	16
	.quad	.L.str.1091
	.quad	8667                    # 0x21db
	.quad	.L.str.1092
	.quad	8738                    # 0x2222
	.zero	16
	.quad	.L.str.1093
	.quad	8291                    # 0x2063
	.quad	.L.str.1094
	.quad	10522                   # 0x291a
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1095
	.quad	10956                   # 0x2acc
	.quad	.L.str.1096
	.quad	8815                    # 0x226f
	.quad	.L.str.1097
	.quad	8500                    # 0x2134
	.quad	.L.str.1098
	.quad	247                     # 0xf7
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1099
	.quad	58                      # 0x3a
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1100
	.quad	8853                    # 0x2295
	.quad	.L.str.1101
	.quad	8539                    # 0x215b
	.quad	.L.str.1102
	.quad	8837                    # 0x2285
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1103
	.quad	10812                   # 0x2a3c
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1104
	.quad	10835                   # 0x2a53
	.zero	16
	.quad	.L.str.1105
	.quad	8915                    # 0x22d3
	.zero	16
	.quad	.L.str.1106
	.quad	8772                    # 0x2244
	.zero	16
	.quad	.L.str.1107
	.quad	10955                   # 0x2acb
	.zero	16
	.quad	.L.str.1108
	.quad	1065                    # 0x429
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1109
	.quad	10913                   # 0x2aa1
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1110
	.quad	8611                    # 0x21a3
	.quad	.L.str.1111
	.quad	919                     # 0x397
	.quad	.L.str.1112
	.quad	8913                    # 0x22d1
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1113
	.quad	8771                    # 0x2243
	.quad	.L.str.1114
	.quad	8636                    # 0x21bc
	.zero	16
	.quad	.L.str.1115
	.quad	9516                    # 0x252c
	.zero	16
	.zero	16
	.quad	.L.str.1116
	.quad	8743                    # 0x2227
	.quad	.L.str.1117
	.quad	184                     # 0xb8
	.quad	.L.str.1118
	.quad	8746                    # 0x222a
	.zero	16
	.quad	.L.str.1119
	.quad	8194                    # 0x2002
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1120
	.quad	8503                    # 0x2137
	.zero	16
	.quad	.L.str.1121
	.quad	981                     # 0x3d5
	.quad	.L.str.1122
	.quad	160                     # 0xa0
	.quad	.L.str.1123
	.quad	8824                    # 0x2278
	.quad	.L.str.1124
	.quad	977                     # 0x3d1
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1125
	.quad	8835                    # 0x2283
	.quad	.L.str.1126
	.quad	91                      # 0x5b
	.zero	16
	.quad	.L.str.1127
	.quad	10003                   # 0x2713
	.quad	.L.str.1128
	.quad	8811                    # 0x226b
	.quad	.L.str.1129
	.quad	9474                    # 0x2502
	.quad	.L.str.1130
	.quad	93                      # 0x5d
	.quad	.L.str.1131
	.quad	8658                    # 0x21d2
	.quad	.L.str.1132
	.quad	8741                    # 0x2225
	.quad	.L.str.1133
	.quad	8810                    # 0x226a
	.zero	16
	.zero	16
	.quad	.L.str.1134
	.quad	8912                    # 0x22d0
	.zero	16
	.quad	.L.str.1135
	.quad	1066                    # 0x42a
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1136
	.quad	254                     # 0xfe
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1137
	.quad	9500                    # 0x251c
	.quad	.L.str.1138
	.quad	8517                    # 0x2145
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1139
	.quad	8878                    # 0x22ae
	.quad	.L.str.1140
	.quad	8735                    # 0x221f
	.quad	.L.str.1141
	.quad	62                      # 0x3e
	.quad	.L.str.1142
	.quad	8885                    # 0x22b5
	.quad	.L.str.1143
	.quad	8834                    # 0x2282
	.quad	.L.str.1144
	.quad	8290                    # 0x2062
	.zero	16
	.quad	.L.str.1145
	.quad	60                      # 0x3c
	.zero	16
	.zero	16
	.quad	.L.str.1146
	.quad	10549                   # 0x2935
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1147
	.quad	1039                    # 0x40f
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1148
	.quad	10537                   # 0x2929
	.quad	.L.str.1149
	.quad	9576                    # 0x2568
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1150
	.quad	10937                   # 0x2ab9
	.zero	16
	.quad	.L.str.1151
	.quad	8786                    # 0x2252
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1152
	.quad	977                     # 0x3d1
	.quad	.L.str.1153
	.quad	989                     # 0x3dd
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1154
	.quad	927                     # 0x39f
	.quad	.L.str.1155
	.quad	10838                   # 0x2a56
	.zero	16
	.quad	.L.str.1156
	.quad	10576                   # 0x2950
	.quad	.L.str.1157
	.quad	160                     # 0xa0
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1158
	.quad	8740                    # 0x2224
	.zero	16
	.quad	.L.str.1159
	.quad	9532                    # 0x253c
	.quad	.L.str.1160
	.quad	8995                    # 0x2323
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1161
	.quad	8782                    # 0x224e
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1162
	.quad	8759                    # 0x2237
	.quad	.L.str.1163
	.quad	8785                    # 0x2251
	.zero	16
	.quad	.L.str.1164
	.quad	8199                    # 0x2007
	.zero	16
	.quad	.L.str.1165
	.quad	8592                    # 0x2190
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1166
	.quad	959                     # 0x3bf
	.quad	.L.str.1167
	.quad	8476                    # 0x211c
	.zero	16
	.quad	.L.str.1168
	.quad	8783                    # 0x224f
	.quad	.L.str.1169
	.quad	12315                   # 0x301b
	.zero	16
	.quad	.L.str.1170
	.quad	8637                    # 0x21bd
	.zero	16
	.quad	.L.str.1171
	.quad	8899                    # 0x22c3
	.quad	.L.str.1172
	.quad	8641                    # 0x21c1
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1173
	.quad	10993                   # 0x2af1
	.zero	16
	.quad	.L.str.1174
	.quad	8450                    # 0x2102
	.quad	.L.str.1175
	.quad	8783                    # 0x224f
	.quad	.L.str.1176
	.quad	8940                    # 0x22ec
	.quad	.L.str.1177
	.quad	8216                    # 0x2018
	.quad	.L.str.1178
	.quad	8600                    # 0x2198
	.quad	.L.str.1179
	.quad	10609                   # 0x2971
	.quad	.L.str.1180
	.quad	8629                    # 0x21b5
	.zero	16
	.quad	.L.str.1181
	.quad	8461                    # 0x210d
	.quad	.L.str.1182
	.quad	8217                    # 0x2019
	.zero	16
	.quad	.L.str.1183
	.quad	8469                    # 0x2115
	.quad	.L.str.1184
	.quad	8646                    # 0x21c6
	.quad	.L.str.1185
	.quad	10569                   # 0x2949
	.quad	.L.str.1186
	.quad	8603                    # 0x219b
	.quad	.L.str.1187
	.quad	8780                    # 0x224c
	.quad	.L.str.1188
	.quad	8635                    # 0x21bb
	.quad	.L.str.1189
	.quad	10512                   # 0x2910
	.quad	.L.str.1190
	.quad	8649                    # 0x21c9
	.quad	.L.str.1191
	.quad	8594                    # 0x2192
	.quad	.L.str.1192
	.quad	8474                    # 0x211a
	.quad	.L.str.1193
	.quad	8477                    # 0x211d
	.quad	.L.str.1194
	.quad	8884                    # 0x22b4
	.quad	.L.str.1195
	.quad	8473                    # 0x2119
	.quad	.L.str.1196
	.quad	10230                   # 0x27f6
	.zero	16
	.quad	.L.str.1197
	.quad	8484                    # 0x2124
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1198
	.quad	8781                    # 0x224d
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1199
	.quad	44                      # 0x2c
	.quad	.L.str.1200
	.quad	1040                    # 0x410
	.quad	.L.str.1201
	.quad	1041                    # 0x411
	.quad	.L.str.1202
	.quad	10510                   # 0x290e
	.quad	.L.str.1203
	.quad	8459                    # 0x210b
	.quad	.L.str.1204
	.quad	1069                    # 0x42d
	.quad	.L.str.1205
	.quad	1060                    # 0x424
	.quad	.L.str.1206
	.quad	1043                    # 0x413
	.quad	.L.str.1207
	.quad	10511                   # 0x290f
	.quad	.L.str.1208
	.quad	1048                    # 0x418
	.quad	.L.str.1209
	.quad	1049                    # 0x419
	.quad	.L.str.1210
	.quad	1050                    # 0x41a
	.quad	.L.str.1211
	.quad	1051                    # 0x41b
	.quad	.L.str.1212
	.quad	230                     # 0xe6
	.quad	.L.str.1213
	.quad	1052                    # 0x41c
	.quad	.L.str.1214
	.quad	1054                    # 0x41e
	.quad	.L.str.1215
	.quad	1055                    # 0x41f
	.quad	.L.str.1216
	.quad	1053                    # 0x41d
	.quad	.L.str.1217
	.quad	1056                    # 0x420
	.quad	.L.str.1218
	.quad	1057                    # 0x421
	.quad	.L.str.1219
	.quad	8593                    # 0x2191
	.quad	.L.str.1220
	.quad	1059                    # 0x423
	.quad	.L.str.1221
	.quad	1042                    # 0x412
	.quad	.L.str.1222
	.quad	1058                    # 0x422
	.quad	.L.str.1223
	.quad	733                     # 0x2dd
	.quad	.L.str.1224
	.quad	913                     # 0x391
	.quad	.L.str.1225
	.quad	1067                    # 0x42b
	.quad	.L.str.1226
	.quad	1047                    # 0x417
	.quad	.L.str.1227
	.quad	8740                    # 0x2224
	.quad	.L.str.1228
	.quad	8840                    # 0x2288
	.quad	.L.str.1229
	.quad	8941                    # 0x22ed
	.quad	.L.str.1230
	.quad	1013                    # 0x3f5
	.quad	.L.str.1231
	.quad	10871                   # 0x2a77
	.quad	.L.str.1232
	.quad	1072                    # 0x430
	.quad	.L.str.1233
	.quad	1073                    # 0x431
	.zero	16
	.quad	.L.str.1234
	.quad	1076                    # 0x434
	.quad	.L.str.1235
	.quad	1101                    # 0x44d
	.quad	.L.str.1236
	.quad	10890                   # 0x2a8a
	.quad	.L.str.1237
	.quad	1092                    # 0x444
	.quad	.L.str.1238
	.quad	1044                    # 0x414
	.quad	.L.str.1239
	.quad	1080                    # 0x438
	.quad	.L.str.1240
	.quad	1075                    # 0x433
	.quad	.L.str.1241
	.quad	10889                   # 0x2a89
	.quad	.L.str.1242
	.quad	8773                    # 0x2245
	.quad	.L.str.1243
	.quad	1081                    # 0x439
	.quad	.L.str.1244
	.quad	1082                    # 0x43a
	.quad	.L.str.1245
	.quad	1083                    # 0x43b
	.quad	.L.str.1246
	.quad	10607                   # 0x296f
	.quad	.L.str.1247
	.quad	1085                    # 0x43d
	.quad	.L.str.1248
	.quad	1086                    # 0x43e
	.quad	.L.str.1249
	.quad	8858                    # 0x229a
	.quad	.L.str.1250
	.quad	1089                    # 0x441
	.quad	.L.str.1251
	.quad	1088                    # 0x440
	.quad	.L.str.1252
	.quad	1087                    # 0x43f
	.quad	.L.str.1253
	.quad	1084                    # 0x43c
	.quad	.L.str.1254
	.quad	1091                    # 0x443
	.quad	.L.str.1255
	.quad	945                     # 0x3b1
	.quad	.L.str.1256
	.quad	8751                    # 0x222f
	.quad	.L.str.1257
	.quad	1079                    # 0x437
	.quad	.L.str.1258
	.quad	8740                    # 0x2224
	.quad	.L.str.1259
	.quad	8486                    # 0x2126
	.quad	.L.str.1260
	.quad	1090                    # 0x442
	.quad	.L.str.1261
	.quad	1099                    # 0x44b
	.quad	.L.str.1262
	.quad	1074                    # 0x432
	.quad	.L.str.1263
	.quad	8847                    # 0x228f
	.quad	.L.str.1264
	.quad	939                     # 0x3ab
	.quad	.L.str.1265
	.quad	10590                   # 0x295e
	.quad	.L.str.1266
	.quad	971                     # 0x3cb
	.quad	.L.str.1267
	.quad	8790                    # 0x2256
	.quad	.L.str.1268
	.quad	10557                   # 0x293d
	.quad	.L.str.1269
	.quad	970                     # 0x3ca
	.quad	.L.str.1270
	.quad	8739                    # 0x2223
	.quad	.L.str.1271
	.quad	938                     # 0x3aa
	.zero	16
	.zero	16
	.quad	.L.str.1272
	.quad	10232                   # 0x27f8
	.zero	16
	.quad	.L.str.1273
	.quad	8850                    # 0x2292
	.zero	16
	.zero	16
	.quad	.L.str.1274
	.quad	9724                    # 0x25fc
	.quad	.L.str.1275
	.quad	339                     # 0x153
	.quad	.L.str.1276
	.quad	8825                    # 0x2279
	.quad	.L.str.1277
	.quad	988                     # 0x3dc
	.zero	16
	.quad	.L.str.1278
	.quad	10613                   # 0x2975
	.zero	16
	.quad	.L.str.1279
	.quad	9005                    # 0x232d
	.quad	.L.str.1280
	.quad	8750                    # 0x222e
	.quad	.L.str.1281
	.quad	8898                    # 0x22c2
	.quad	.L.str.1282
	.quad	8767                    # 0x223f
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1283
	.quad	9649                    # 0x25b1
	.quad	.L.str.1284
	.quad	8666                    # 0x21da
	.zero	16
	.zero	16
	.quad	.L.str.1285
	.quad	8841                    # 0x2289
	.zero	16
	.quad	.L.str.1286
	.quad	10718                   # 0x29de
	.quad	.L.str.1287
	.quad	194                     # 0xc2
	.zero	16
	.zero	16
	.quad	.L.str.1288
	.quad	8634                    # 0x21ba
	.quad	.L.str.1289
	.quad	202                     # 0xca
	.quad	.L.str.1290
	.quad	924                     # 0x39c
	.quad	.L.str.1291
	.quad	925                     # 0x39d
	.quad	.L.str.1292
	.quad	8450                    # 0x2102
	.quad	.L.str.1293
	.quad	206                     # 0xce
	.zero	16
	.quad	.L.str.1294
	.quad	8938                    # 0x22ea
	.quad	.L.str.1295
	.quad	8865                    # 0x22a1
	.zero	16
	.zero	16
	.quad	.L.str.1296
	.quad	212                     # 0xd4
	.quad	.L.str.1297
	.quad	10538                   # 0x292a
	.quad	.L.str.1298
	.quad	8833                    # 0x2281
	.zero	16
	.quad	.L.str.1299
	.quad	928                     # 0x3a0
	.zero	16
	.quad	.L.str.1300
	.quad	219                     # 0xdb
	.quad	.L.str.1301
	.quad	9633                    # 0x25a1
	.quad	.L.str.1302
	.quad	9492                    # 0x2514
	.quad	.L.str.1303
	.quad	926                     # 0x39e
	.quad	.L.str.1304
	.quad	8756                    # 0x2234
	.quad	.L.str.1305
	.quad	10574                   # 0x294e
	.zero	16
	.zero	16
	.quad	.L.str.1306
	.quad	8602                    # 0x219a
	.quad	.L.str.1307
	.quad	8930                    # 0x22e2
	.zero	16
	.zero	16
	.quad	.L.str.1308
	.quad	226                     # 0xe2
	.quad	.L.str.1309
	.quad	8651                    # 0x21cb
	.quad	.L.str.1310
	.quad	937                     # 0x3a9
	.quad	.L.str.1311
	.quad	902                     # 0x386
	.quad	.L.str.1312
	.quad	9574                    # 0x2566
	.quad	.L.str.1313
	.quad	234                     # 0xea
	.quad	.L.str.1314
	.quad	957                     # 0x3bd
	.quad	.L.str.1315
	.quad	8654                    # 0x21ce
	.quad	.L.str.1316
	.quad	8499                    # 0x2133
	.quad	.L.str.1317
	.quad	238                     # 0xee
	.quad	.L.str.1318
	.quad	8827                    # 0x227b
	.quad	.L.str.1319
	.quad	956                     # 0x3bc
	.zero	16
	.quad	.L.str.1320
	.quad	8715                    # 0x220b
	.quad	.L.str.1321
	.quad	244                     # 0xf4
	.quad	.L.str.1322
	.quad	960                     # 0x3c0
	.quad	.L.str.1323
	.quad	180                     # 0xb4
	.quad	.L.str.1324
	.quad	10234                   # 0x27fa
	.quad	.L.str.1325
	.quad	8910                    # 0x22ce
	.quad	.L.str.1326
	.quad	176                     # 0xb0
	.quad	.L.str.1327
	.quad	251                     # 0xfb
	.quad	.L.str.1328
	.quad	165                     # 0xa5
	.zero	16
	.quad	.L.str.1329
	.quad	958                     # 0x3be
	.quad	.L.str.1330
	.quad	9711                    # 0x25ef
	.quad	.L.str.1331
	.quad	1025                    # 0x401
	.quad	.L.str.1332
	.quad	10901                   # 0x2a95
	.quad	.L.str.1333
	.quad	8807                    # 0x2267
	.quad	.L.str.1334
	.quad	8922                    # 0x22da
	.quad	.L.str.1335
	.quad	906                     # 0x38a
	.quad	.L.str.1336
	.quad	8612                    # 0x21a4
	.quad	.L.str.1337
	.quad	1109                    # 0x455
	.quad	.L.str.1338
	.quad	8829                    # 0x227d
	.quad	.L.str.1339
	.quad	10765                   # 0x2a0d
	.quad	.L.str.1340
	.quad	969                     # 0x3c9
	.quad	.L.str.1341
	.quad	190                     # 0xbe
	.quad	.L.str.1342
	.quad	174                     # 0xae
	.quad	.L.str.1343
	.quad	8595                    # 0x2193
	.quad	.L.str.1344
	.quad	34                      # 0x22
	.quad	.L.str.1345
	.quad	8467                    # 0x2113
	.quad	.L.str.1346
	.quad	8635                    # 0x21bb
	.quad	.L.str.1347
	.quad	305                     # 0x131
	.quad	.L.str.1348
	.quad	9661                    # 0x25bd
	.quad	.L.str.1349
	.quad	943                     # 0x3af
	.quad	.L.str.1350
	.quad	8465                    # 0x2111
	.quad	.L.str.1351
	.quad	10599                   # 0x2967
	.quad	.L.str.1352
	.quad	1094                    # 0x446
	.quad	.L.str.1353
	.quad	106                     # 0x6a
	.quad	.L.str.1354
	.quad	8788                    # 0x2254
	.quad	.L.str.1355
	.quad	904                     # 0x388
	.quad	.L.str.1356
	.quad	9619                    # 0x2593
	.quad	.L.str.1357
	.quad	941                     # 0x3ad
	.quad	.L.str.1358
	.quad	10229                   # 0x27f5
	.quad	.L.str.1359
	.quad	8806                    # 0x2266
	.quad	.L.str.1360
	.quad	8740                    # 0x2224
	.quad	.L.str.1361
	.quad	973                     # 0x3cd
	.quad	.L.str.1362
	.quad	9568                    # 0x2560
	.zero	16
	.zero	16
	.quad	.L.str.1363
	.quad	908                     # 0x38c
	.quad	.L.str.1364
	.quad	940                     # 0x3ac
	.quad	.L.str.1365
	.quad	972                     # 0x3cc
	.quad	.L.str.1366
	.quad	10598                   # 0x2966
	.quad	.L.str.1367
	.quad	8520                    # 0x2148
	.zero	16
	.quad	.L.str.1368
	.quad	8981                    # 0x2315
	.zero	16
	.quad	.L.str.1369
	.quad	10513                   # 0x2911
	.quad	.L.str.1370
	.quad	8816                    # 0x2270
	.quad	.L.str.1371
	.quad	8823                    # 0x2277
	.zero	16
	.quad	.L.str.1372
	.quad	10992                   # 0x2af0
	.quad	.L.str.1373
	.quad	10515                   # 0x2913
	.zero	16
	.quad	.L.str.1374
	.quad	59                      # 0x3b
	.quad	.L.str.1375
	.quad	910                     # 0x38e
	.quad	.L.str.1376
	.quad	123                     # 0x7b
	.quad	.L.str.1377
	.quad	8499                    # 0x2133
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1378
	.quad	125                     # 0x7d
	.quad	.L.str.1379
	.quad	8802                    # 0x2262
	.zero	16
	.quad	.L.str.1380
	.quad	1097                    # 0x449
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1381
	.quad	10858                   # 0x2a6a
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1382
	.quad	10991                   # 0x2aef
	.zero	16
	.quad	.L.str.1383
	.quad	8838                    # 0x2286
	.quad	.L.str.1384
	.quad	9792                    # 0x2640
	.zero	16
	.zero	16
	.quad	.L.str.1385
	.quad	8833                    # 0x2281
	.zero	16
	.zero	16
	.quad	.L.str.1386
	.quad	9580                    # 0x256c
	.zero	16
	.zero	16
	.quad	.L.str.1387
	.quad	10571                   # 0x294b
	.quad	.L.str.1388
	.quad	9657                    # 0x25b9
	.quad	.L.str.1389
	.quad	8809                    # 0x2269
	.zero	16
	.quad	.L.str.1390
	.quad	8595                    # 0x2193
	.zero	16
	.quad	.L.str.1391
	.quad	8765                    # 0x223d
	.quad	.L.str.1392
	.quad	8808                    # 0x2268
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1393
	.quad	8721                    # 0x2211
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1394
	.quad	64256                   # 0xfb00
	.zero	16
	.quad	.L.str.1395
	.quad	1098                    # 0x44a
	.quad	.L.str.1396
	.quad	8836                    # 0x2284
	.zero	16
	.quad	.L.str.1397
	.quad	8777                    # 0x2249
	.quad	.L.str.1398
	.quad	8257                    # 0x2041
	.zero	16
	.quad	.L.str.1399
	.quad	10934                   # 0x2ab6
	.quad	.L.str.1400
	.quad	8531                    # 0x2153
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1401
	.quad	35                      # 0x23
	.quad	.L.str.1402
	.quad	8197                    # 0x2005
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1403
	.quad	8721                    # 0x2211
	.zero	16
	.quad	.L.str.1404
	.quad	8752                    # 0x2230
	.zero	16
	.quad	.L.str.1405
	.quad	9656                    # 0x25b8
	.zero	16
	.quad	.L.str.1406
	.quad	8839                    # 0x2287
	.quad	.L.str.1407
	.quad	8764                    # 0x223c
	.zero	16
	.zero	16
	.quad	.L.str.1408
	.quad	8970                    # 0x230a
	.zero	16
	.quad	.L.str.1409
	.quad	8885                    # 0x22b5
	.zero	16
	.quad	.L.str.1410
	.quad	9555                    # 0x2553
	.zero	16
	.quad	.L.str.1411
	.quad	43                      # 0x2b
	.zero	16
	.zero	16
	.quad	.L.str.1412
	.quad	966                     # 0x3c6
	.zero	16
	.quad	.L.str.1413
	.quad	8832                    # 0x2280
	.quad	.L.str.1414
	.quad	8969                    # 0x2309
	.quad	.L.str.1415
	.quad	9742                    # 0x260e
	.zero	16
	.zero	16
	.quad	.L.str.1416
	.quad	8723                    # 0x2213
	.zero	16
	.quad	.L.str.1417
	.quad	8872                    # 0x22a8
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1418
	.quad	8949                    # 0x22f5
	.zero	16
	.zero	16
	.quad	.L.str.1419
	.quad	936                     # 0x3a8
	.zero	16
	.zero	16
	.quad	.L.str.1420
	.quad	8938                    # 0x22ea
	.quad	.L.str.1421
	.quad	8465                    # 0x2111
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1422
	.quad	9472                    # 0x2500
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1423
	.quad	8642                    # 0x21c2
	.quad	.L.str.1424
	.quad	10771                   # 0x2a13
	.quad	.L.str.1425
	.quad	9557                    # 0x2555
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1426
	.quad	10588                   # 0x295c
	.zero	16
	.quad	.L.str.1427
	.quad	1035                    # 0x40b
	.quad	.L.str.1428
	.quad	10236                   # 0x27fc
	.quad	.L.str.1429
	.quad	8966                    # 0x2306
	.quad	.L.str.1430
	.quad	8260                    # 0x2044
	.zero	16
	.quad	.L.str.1431
	.quad	10233                   # 0x27f9
	.quad	.L.str.1432
	.quad	8809                    # 0x2269
	.zero	16
	.quad	.L.str.1433
	.quad	8808                    # 0x2268
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1434
	.quad	8519                    # 0x2147
	.zero	16
	.quad	.L.str.1435
	.quad	10525                   # 0x291d
	.quad	.L.str.1436
	.quad	8742                    # 0x2226
	.zero	16
	.quad	.L.str.1437
	.quad	8911                    # 0x22cf
	.zero	16
	.zero	16
	.quad	.L.str.1438
	.quad	10526                   # 0x291e
	.quad	.L.str.1439
	.quad	10774                   # 0x2a16
	.zero	16
	.quad	.L.str.1440
	.quad	8541                    # 0x215d
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1441
	.quad	8923                    # 0x22db
	.zero	16
	.zero	16
	.quad	.L.str.1442
	.quad	8828                    # 0x227c
	.zero	16
	.quad	.L.str.1443
	.quad	8784                    # 0x2250
	.quad	.L.str.1444
	.quad	191                     # 0xbf
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1445
	.quad	8800                    # 0x2260
	.zero	16
	.quad	.L.str.1446
	.quad	8965                    # 0x2305
	.quad	.L.str.1447
	.quad	989                     # 0x3dd
	.zero	16
	.quad	.L.str.1448
	.quad	9608                    # 0x2588
	.zero	16
	.quad	.L.str.1449
	.quad	175                     # 0xaf
	.zero	16
	.quad	.L.str.1450
	.quad	8719                    # 0x220f
	.quad	.L.str.1451
	.quad	8828                    # 0x227c
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1452
	.quad	8852                    # 0x2294
	.quad	.L.str.1453
	.quad	196                     # 0xc4
	.zero	16
	.quad	.L.str.1454
	.quad	8648                    # 0x21c8
	.zero	16
	.quad	.L.str.1455
	.quad	203                     # 0xcb
	.quad	.L.str.1456
	.quad	9472                    # 0x2500
	.zero	16
	.zero	16
	.quad	.L.str.1457
	.quad	207                     # 0xcf
	.quad	.L.str.1458
	.quad	10499                   # 0x2903
	.quad	.L.str.1459
	.quad	8226                    # 0x2022
	.quad	.L.str.1460
	.quad	8754                    # 0x2232
	.zero	16
	.quad	.L.str.1461
	.quad	8733                    # 0x221d
	.quad	.L.str.1462
	.quad	214                     # 0xd6
	.zero	16
	.quad	.L.str.1463
	.quad	1095                    # 0x447
	.quad	.L.str.1464
	.quad	8461                    # 0x210d
	.quad	.L.str.1465
	.quad	8502                    # 0x2136
	.quad	.L.str.1466
	.quad	175                     # 0xaf
	.quad	.L.str.1467
	.quad	220                     # 0xdc
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1468
	.quad	1093                    # 0x445
	.quad	.L.str.1469
	.quad	8902                    # 0x22c6
	.quad	.L.str.1470
	.quad	8471                    # 0x2117
	.quad	.L.str.1471
	.quad	376                     # 0x178
	.zero	16
	.quad	.L.str.1472
	.quad	164                     # 0xa4
	.zero	16
	.quad	.L.str.1473
	.quad	9633                    # 0x25a1
	.quad	.L.str.1474
	.quad	1096                    # 0x448
	.quad	.L.str.1475
	.quad	228                     # 0xe4
	.zero	16
	.quad	.L.str.1476
	.quad	9562                    # 0x255a
	.quad	.L.str.1477
	.quad	168                     # 0xa8
	.quad	.L.str.1478
	.quad	235                     # 0xeb
	.zero	16
	.quad	.L.str.1479
	.quad	1078                    # 0x436
	.quad	.L.str.1480
	.quad	239                     # 0xef
	.zero	16
	.quad	.L.str.1481
	.quad	8726                    # 0x2216
	.quad	.L.str.1482
	.quad	9633                    # 0x25a1
	.zero	16
	.quad	.L.str.1483
	.quad	8849                    # 0x2291
	.quad	.L.str.1484
	.quad	246                     # 0xf6
	.zero	16
	.quad	.L.str.1485
	.quad	8764                    # 0x223c
	.zero	16
	.zero	16
	.quad	.L.str.1486
	.quad	10935                   # 0x2ab7
	.quad	.L.str.1487
	.quad	252                     # 0xfc
	.quad	.L.str.1488
	.quad	8791                    # 0x2257
	.zero	16
	.quad	.L.str.1489
	.quad	10239                   # 0x27ff
	.quad	.L.str.1490
	.quad	255                     # 0xff
	.quad	.L.str.1491
	.quad	9734                    # 0x2606
	.quad	.L.str.1492
	.quad	8849                    # 0x2291
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1493
	.quad	8916                    # 0x22d4
	.quad	.L.str.1494
	.quad	10956                   # 0x2acc
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1495
	.quad	8659                    # 0x21d3
	.zero	16
	.quad	.L.str.1496
	.quad	8642                    # 0x21c2
	.quad	.L.str.1497
	.quad	8466                    # 0x2112
	.zero	16
	.quad	.L.str.1498
	.quad	10605                   # 0x296d
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1499
	.quad	8643                    # 0x21c3
	.quad	.L.str.1500
	.quad	732                     # 0x2dc
	.quad	.L.str.1501
	.quad	8622                    # 0x21ae
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1502
	.quad	8200                    # 0x2008
	.quad	.L.str.1503
	.quad	8638                    # 0x21be
	.quad	.L.str.1504
	.quad	9575                    # 0x2567
	.quad	.L.str.1505
	.quad	8871                    # 0x22a7
	.quad	.L.str.1506
	.quad	10231                   # 0x27f7
	.quad	.L.str.1507
	.quad	8742                    # 0x2226
	.zero	16
	.quad	.L.str.1508
	.quad	8733                    # 0x221d
	.zero	16
	.quad	.L.str.1509
	.quad	9666                    # 0x25c2
	.zero	16
	.quad	.L.str.1510
	.quad	9662                    # 0x25be
	.zero	16
	.zero	16
	.quad	.L.str.1511
	.quad	10955                   # 0x2acb
	.quad	.L.str.1512
	.quad	8817                    # 0x2271
	.quad	.L.str.1513
	.quad	9656                    # 0x25b8
	.zero	16
	.quad	.L.str.1514
	.quad	9652                    # 0x25b4
	.quad	.L.str.1515
	.quad	9508                    # 0x2524
	.quad	.L.str.1516
	.quad	8630                    # 0x21b6
	.quad	.L.str.1517
	.quad	10885                   # 0x2a85
	.quad	.L.str.1518
	.quad	10934                   # 0x2ab6
	.quad	.L.str.1519
	.quad	977                     # 0x3d1
	.zero	16
	.zero	16
	.quad	.L.str.1520
	.quad	8519                    # 0x2147
	.quad	.L.str.1521
	.quad	10842                   # 0x2a5a
	.zero	16
	.zero	16
	.quad	.L.str.1522
	.quad	8615                    # 0x21a7
	.zero	16
	.zero	16
	.quad	.L.str.1523
	.quad	8637                    # 0x21bd
	.quad	.L.str.1524
	.quad	8640                    # 0x21c0
	.quad	.L.str.1525
	.quad	8614                    # 0x21a6
	.quad	.L.str.1526
	.quad	12298                   # 0x300a
	.quad	.L.str.1527
	.quad	8848                    # 0x2290
	.quad	.L.str.1528
	.quad	8605                    # 0x219d
	.zero	16
	.zero	16
	.quad	.L.str.1529
	.quad	12299                   # 0x300b
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1530
	.quad	962                     # 0x3c2
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1531
	.quad	8851                    # 0x2293
	.zero	16
	.zero	16
	.quad	.L.str.1532
	.quad	8642                    # 0x21c2
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1533
	.quad	9001                    # 0x2329
	.quad	.L.str.1534
	.quad	8728                    # 0x2218
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1535
	.quad	9002                    # 0x232a
	.zero	16
	.quad	.L.str.1536
	.quad	8660                    # 0x21d4
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1537
	.quad	8651                    # 0x21cb
	.zero	16
	.quad	.L.str.1538
	.quad	8658                    # 0x21d2
	.quad	.L.str.1539
	.quad	8463                    # 0x210f
	.zero	16
	.zero	16
	.quad	.L.str.1540
	.quad	935                     # 0x3a7
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1541
	.quad	937                     # 0x3a9
	.quad	.L.str.1542
	.quad	934                     # 0x3a6
	.zero	16
	.quad	.L.str.1543
	.quad	8848                    # 0x2290
	.zero	16
	.quad	.L.str.1544
	.quad	920                     # 0x398
	.zero	16
	.quad	.L.str.1545
	.quad	10770                   # 0x2a12
	.zero	16
	.zero	16
	.quad	.L.str.1546
	.quad	10886                   # 0x2a86
	.quad	.L.str.1547
	.quad	10231                   # 0x27f7
	.zero	16
	.quad	.L.str.1548
	.quad	10716                   # 0x29dc
	.zero	16
	.zero	16
	.quad	.L.str.1549
	.quad	8537                    # 0x2159
	.zero	16
	.quad	.L.str.1550
	.quad	8596                    # 0x2194
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1551
	.quad	8594                    # 0x2192
	.quad	.L.str.1552
	.quad	8787                    # 0x2253
	.zero	16
	.quad	.L.str.1553
	.quad	9642                    # 0x25aa
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1554
	.quad	8412                    # 0x20dc
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1555
	.quad	8730                    # 0x221a
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1556
	.quad	8655                    # 0x21cf
	.zero	16
	.quad	.L.str.1557
	.quad	8477                    # 0x211d
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1558
	.quad	966                     # 0x3c6
	.zero	16
	.quad	.L.str.1559
	.quad	8641                    # 0x21c1
	.quad	.L.str.1560
	.quad	10869                   # 0x2a75
	.quad	.L.str.1561
	.quad	8813                    # 0x226d
	.zero	16
	.zero	16
	.quad	.L.str.1562
	.quad	8831                    # 0x227f
	.zero	16
	.quad	.L.str.1563
	.quad	8827                    # 0x227b
	.quad	.L.str.1564
	.quad	8724                    # 0x2214
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1565
	.quad	8716                    # 0x220c
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1566
	.quad	8620                    # 0x21ac
	.quad	.L.str.1567
	.quad	8921                    # 0x22d9
	.quad	.L.str.1568
	.quad	9723                    # 0x25fb
	.zero	16
	.quad	.L.str.1569
	.quad	8939                    # 0x22eb
	.zero	16
	.zero	16
	.quad	.L.str.1570
	.quad	9416                    # 0x24c8
	.zero	16
	.zero	16
	.quad	.L.str.1571
	.quad	1045                    # 0x415
	.zero	16
	.quad	.L.str.1572
	.quad	10928                   # 0x2ab0
	.quad	.L.str.1573
	.quad	8883                    # 0x22b3
	.quad	.L.str.1574
	.quad	8615                    # 0x21a7
	.zero	16
	.quad	.L.str.1575
	.quad	8536                    # 0x2158
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1576
	.quad	733                     # 0x2dd
	.zero	16
	.quad	.L.str.1577
	.quad	9617                    # 0x2591
	.zero	16
	.quad	.L.str.1578
	.quad	10891                   # 0x2a8b
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1579
	.quad	1102                    # 0x44e
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1580
	.quad	1111                    # 0x457
	.zero	16
	.quad	.L.str.1581
	.quad	10888                   # 0x2a88
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1582
	.quad	10606                   # 0x296e
	.quad	.L.str.1583
	.quad	8846                    # 0x228e
	.quad	.L.str.1584
	.quad	9251                    # 0x2423
	.quad	.L.str.1585
	.quad	10887                   # 0x2a87
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1586
	.quad	8775                    # 0x2247
	.quad	.L.str.1587
	.quad	10950                   # 0x2ac6
	.zero	16
	.quad	.L.str.1588
	.quad	177                     # 0xb1
	.zero	16
	.zero	16
	.quad	.L.str.1589
	.quad	8819                    # 0x2273
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1590
	.quad	8693                    # 0x21f5
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1591
	.quad	12314                   # 0x301a
	.zero	16
	.zero	16
	.quad	.L.str.1592
	.quad	8755                    # 0x2233
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1593
	.quad	9838                    # 0x266e
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1594
	.quad	8648                    # 0x21c8
	.quad	.L.str.1595
	.quad	10756                   # 0x2a04
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1596
	.quad	10582                   # 0x2956
	.zero	16
	.zero	16
	.quad	.L.str.1597
	.quad	10949                   # 0x2ac5
	.zero	16
	.zero	16
	.quad	.L.str.1598
	.quad	8706                    # 0x2202
	.quad	.L.str.1599
	.quad	8991                    # 0x231f
	.quad	.L.str.1600
	.quad	8213                    # 0x2015
	.quad	.L.str.1601
	.quad	65128                   # 0xfe68
	.quad	.L.str.1602
	.quad	1009                    # 0x3f1
	.zero	16
	.zero	16
	.quad	.L.str.1603
	.quad	10552                   # 0x2938
	.zero	16
	.zero	16
	.quad	.L.str.1604
	.quad	9496                    # 0x2518
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1605
	.quad	186                     # 0xba
	.zero	16
	.quad	.L.str.1606
	.quad	8989                    # 0x231d
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1607
	.quad	8760                    # 0x2238
	.quad	.L.str.1608
	.quad	8937                    # 0x22e9
	.quad	.L.str.1609
	.quad	10840                   # 0x2a58
	.quad	.L.str.1610
	.quad	8493                    # 0x212d
	.zero	16
	.zero	16
	.quad	.L.str.1611
	.quad	8908                    # 0x22cc
	.quad	.L.str.1612
	.quad	8460                    # 0x210c
	.quad	.L.str.1613
	.quad	8847                    # 0x228f
	.quad	.L.str.1614
	.quad	10523                   # 0x291b
	.quad	.L.str.1615
	.quad	8652                    # 0x21cc
	.quad	.L.str.1616
	.quad	8465                    # 0x2111
	.quad	.L.str.1617
	.quad	10717                   # 0x29dd
	.zero	16
	.zero	16
	.quad	.L.str.1618
	.quad	10524                   # 0x291c
	.quad	.L.str.1619
	.quad	10938                   # 0x2aba
	.quad	.L.str.1620
	.quad	10602                   # 0x296a
	.quad	.L.str.1621
	.quad	8476                    # 0x211c
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1622
	.quad	8741                    # 0x2225
	.quad	.L.str.1623
	.quad	10604                   # 0x296c
	.zero	16
	.quad	.L.str.1624
	.quad	8488                    # 0x2128
	.quad	.L.str.1625
	.quad	10740                   # 0x29f4
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1626
	.quad	8644                    # 0x21c4
	.quad	.L.str.1627
	.quad	8979                    # 0x2313
	.quad	.L.str.1628
	.quad	8841                    # 0x2289
	.quad	.L.str.1629
	.quad	10532                   # 0x2924
	.zero	16
	.zero	16
	.quad	.L.str.1630
	.quad	9663                    # 0x25bf
	.zero	16
	.quad	.L.str.1631
	.quad	10533                   # 0x2925
	.quad	.L.str.1632
	.quad	8709                    # 0x2205
	.zero	16
	.quad	.L.str.1633
	.quad	8657                    # 0x21d1
	.quad	.L.str.1634
	.quad	10003                   # 0x2713
	.zero	16
	.quad	.L.str.1635
	.quad	10936                   # 0x2ab8
	.zero	16
	.quad	.L.str.1636
	.quad	8914                    # 0x22d2
	.quad	.L.str.1637
	.quad	8883                    # 0x22b3
	.zero	16
	.quad	.L.str.1638
	.quad	10537                   # 0x2929
	.quad	.L.str.1639
	.quad	8638                    # 0x21be
	.zero	16
	.quad	.L.str.1640
	.quad	9571                    # 0x2563
	.quad	.L.str.1641
	.quad	8789                    # 0x2255
	.zero	16
	.zero	16
	.quad	.L.str.1642
	.quad	10501                   # 0x2905
	.zero	16
	.zero	16
	.quad	.L.str.1643
	.quad	91                      # 0x5b
	.quad	.L.str.1644
	.quad	222                     # 0xde
	.quad	.L.str.1645
	.quad	8693                    # 0x21f5
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1646
	.quad	93                      # 0x5d
	.zero	16
	.zero	16
	.quad	.L.str.1647
	.quad	732                     # 0x2dc
	.quad	.L.str.1648
	.quad	38                      # 0x26
	.quad	.L.str.1649
	.quad	8652                    # 0x21cc
	.zero	16
	.quad	.L.str.1650
	.quad	8780                    # 0x224c
	.zero	16
	.zero	16
	.quad	.L.str.1651
	.quad	8707                    # 0x2203
	.quad	.L.str.1652
	.quad	10845                   # 0x2a5d
	.zero	16
	.quad	.L.str.1653
	.quad	8745                    # 0x2229
	.quad	.L.str.1654
	.quad	942                     # 0x3ae
	.quad	.L.str.1655
	.quad	8830                    # 0x227e
	.quad	.L.str.1656
	.quad	8903                    # 0x22c7
	.quad	.L.str.1657
	.quad	8826                    # 0x227a
	.size	entities_htable_elements, 49264

	.type	entities_htable,@object # @entities_htable
	.section	.rodata,"a",@progbits
	.globl	entities_htable
	.p2align	3
entities_htable:
	.quad	entities_htable_elements
	.quad	3079                    # 0xc07
	.quad	1643                    # 0x66b
	.quad	2463                    # 0x99f
	.size	entities_htable, 32

	.type	aliases_htable_elements,@object # @aliases_htable_elements
	.data
	.p2align	4
aliases_htable_elements:
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1658
	.quad	8                       # 0x8
	.quad	.L.str.1659
	.quad	8                       # 0x8
	.zero	16
	.quad	.L.str.1660
	.quad	1                       # 0x1
	.quad	.L.str.1661
	.quad	7                       # 0x7
	.zero	16
	.quad	.L.str.1662
	.quad	0                       # 0x0
	.quad	.L.str.1663
	.quad	0                       # 0x0
	.zero	16
	.quad	.L.str.1664
	.quad	7                       # 0x7
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1665
	.quad	2                       # 0x2
	.quad	.L.str.1666
	.quad	0                       # 0x0
	.quad	.L.str.1667
	.quad	1                       # 0x1
	.quad	.L.str.1668
	.quad	6                       # 0x6
	.zero	16
	.quad	.L.str.1669
	.quad	2                       # 0x2
	.zero	16
	.quad	.L.str.1670
	.quad	6                       # 0x6
	.quad	.L.str.1671
	.quad	0                       # 0x0
	.quad	.L.str.1672
	.quad	3                       # 0x3
	.quad	.L.str.1673
	.quad	2                       # 0x2
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.quad	.L.str.1674
	.quad	3                       # 0x3
	.quad	.L.str.1675
	.quad	1                       # 0x1
	.zero	16
	.quad	.L.str.1676
	.quad	0                       # 0x0
	.quad	.L.str.1677
	.quad	0                       # 0x0
	.quad	.L.str.1678
	.quad	0                       # 0x0
	.quad	.L.str.1679
	.quad	8                       # 0x8
	.quad	.L.str.1680
	.quad	3                       # 0x3
	.quad	.L.str.1681
	.quad	8                       # 0x8
	.zero	16
	.zero	16
	.quad	.L.str.1682
	.quad	0                       # 0x0
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.size	aliases_htable_elements, 848

	.type	aliases_htable,@object  # @aliases_htable
	.section	.rodata,"a",@progbits
	.globl	aliases_htable
	.p2align	3
aliases_htable:
	.quad	aliases_htable_elements
	.quad	53                      # 0x35
	.quad	25                      # 0x19
	.quad	42                      # 0x2a
	.size	aliases_htable, 32

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"&lt;"
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"&gt;"
	.size	.L.str.1, 5

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" "
	.size	.L.str.2, 2

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"&#%d;"
	.size	.L.str.3, 6

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Entity converter: Supplied buffer size:%lu, smaller than minimum required: %d\n"
	.size	.L.str.4, 79

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"ISO-8859-1"
	.size	.L.str.5, 11

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"OTHER"
	.size	.L.str.6, 6

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Setting encoding for %p  to %s, priority: %d\n"
	.size	.L.str.7, 46

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"process_encoding_set: refusing to override encoding - new encoding size differs: %s(%lu) != %s(%lu)\n"
	.size	.L.str.8, 101

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"New encoding for %p:%s\n"
	.size	.L.str.9, 24

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Iconv init problem for encoding:%s, falling back to iso encoding!\n"
	.size	.L.str.10, 67

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"fallback failed... bail out\n"
	.size	.L.str.11, 29

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"iconv error:%s, silently resuming (%ld,%ld,%lu,%lu)\n"
	.size	.L.str.12, 53

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Skipping null character in html stream\n"
	.size	.L.str.13, 40

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Impossible\n"
	.size	.L.str.14, 12

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"varpropto"
	.size	.L.str.15, 10

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"ncong"
	.size	.L.str.16, 6

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"grave"
	.size	.L.str.17, 6

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"toea"
	.size	.L.str.18, 5

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"map"
	.size	.L.str.19, 4

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"nap"
	.size	.L.str.20, 4

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"lap"
	.size	.L.str.21, 4

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"gap"
	.size	.L.str.22, 4

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"twoheadrightarrow"
	.size	.L.str.23, 18

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"Ll"
	.size	.L.str.24, 3

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"amalg"
	.size	.L.str.25, 6

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"geqslant"
	.size	.L.str.26, 9

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"origof"
	.size	.L.str.27, 7

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"Tab"
	.size	.L.str.28, 4

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"leqslant"
	.size	.L.str.29, 9

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"asympeq"
	.size	.L.str.30, 8

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"sext"
	.size	.L.str.31, 5

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"Wedge"
	.size	.L.str.32, 6

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"sqcap"
	.size	.L.str.33, 6

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"loang"
	.size	.L.str.34, 6

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"ngeq"
	.size	.L.str.35, 5

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"NotLessTilde"
	.size	.L.str.36, 13

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"nwArr"
	.size	.L.str.37, 6

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"roang"
	.size	.L.str.38, 6

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"Delta"
	.size	.L.str.39, 6

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"swArr"
	.size	.L.str.40, 6

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"DifferentialD"
	.size	.L.str.41, 14

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"gl"
	.size	.L.str.42, 3

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"loz"
	.size	.L.str.43, 4

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"Prime"
	.size	.L.str.44, 6

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"ShortRightArrow"
	.size	.L.str.45, 16

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"sigmaf"
	.size	.L.str.46, 7

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"ll"
	.size	.L.str.47, 3

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"rx"
	.size	.L.str.48, 3

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"circledS"
	.size	.L.str.49, 9

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"notni"
	.size	.L.str.50, 6

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"wedge"
	.size	.L.str.51, 6

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"rightrightarrows"
	.size	.L.str.52, 17

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"delta"
	.size	.L.str.53, 6

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"ffllig"
	.size	.L.str.54, 7

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"Bernoullis"
	.size	.L.str.55, 11

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"djcy"
	.size	.L.str.56, 5

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"gjcy"
	.size	.L.str.57, 5

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"prime"
	.size	.L.str.58, 6

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"kjcy"
	.size	.L.str.59, 5

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"ljcy"
	.size	.L.str.60, 5

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"lmoustache"
	.size	.L.str.61, 11

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"njcy"
	.size	.L.str.62, 5

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"boxDl"
	.size	.L.str.63, 6

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"clubs"
	.size	.L.str.64, 6

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"Integral"
	.size	.L.str.65, 9

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"rmoustache"
	.size	.L.str.66, 11

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"sol"
	.size	.L.str.67, 4

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"SquareSubset"
	.size	.L.str.68, 13

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"rect"
	.size	.L.str.69, 5

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"sect"
	.size	.L.str.70, 5

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"VeryThinSpace"
	.size	.L.str.71, 14

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"lesssim"
	.size	.L.str.72, 8

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"UnderBrace"
	.size	.L.str.73, 11

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"comp"
	.size	.L.str.74, 5

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"mapstoup"
	.size	.L.str.75, 9

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"circ"
	.size	.L.str.76, 5

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"complement"
	.size	.L.str.77, 11

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"flat"
	.size	.L.str.78, 5

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"LowerLeftArrow"
	.size	.L.str.79, 15

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"cuesc"
	.size	.L.str.80, 6

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"sce"
	.size	.L.str.81, 4

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"euro"
	.size	.L.str.82, 5

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"succcurlyeq"
	.size	.L.str.83, 12

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"LeftCeiling"
	.size	.L.str.84, 12

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"vprop"
	.size	.L.str.85, 6

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"nexists"
	.size	.L.str.86, 8

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"pitchfork"
	.size	.L.str.87, 10

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"darr2"
	.size	.L.str.88, 6

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"Ubrcy"
	.size	.L.str.89, 6

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"sqsupe"
	.size	.L.str.90, 7

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"eparsl"
	.size	.L.str.91, 7

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"larr2"
	.size	.L.str.92, 6

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"LeftUpVectorBar"
	.size	.L.str.93, 16

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"sqsupseteq"
	.size	.L.str.94, 11

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"rarr2"
	.size	.L.str.95, 6

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"uarr2"
	.size	.L.str.96, 6

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"veeeq"
	.size	.L.str.97, 6

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"AElig"
	.size	.L.str.98, 6

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"notnivb"
	.size	.L.str.99, 8

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"plusb"
	.size	.L.str.100, 6

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"DoubleRightTee"
	.size	.L.str.101, 15

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"NotSquareSupersetEqual"
	.size	.L.str.102, 23

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"hookrightarrow"
	.size	.L.str.103, 15

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"ubrcy"
	.size	.L.str.104, 6

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"OElig"
	.size	.L.str.105, 6

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"NotGreaterTilde"
	.size	.L.str.106, 16

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"filig"
	.size	.L.str.107, 6

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"nlt"
	.size	.L.str.108, 4

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"Agr"
	.size	.L.str.109, 4

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"GreaterEqual"
	.size	.L.str.110, 13

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"mldr"
	.size	.L.str.111, 5

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"nldr"
	.size	.L.str.112, 5

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"drcrop"
	.size	.L.str.113, 7

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"Dgr"
	.size	.L.str.114, 4

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"Bgr"
	.size	.L.str.115, 4

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"Igr"
	.size	.L.str.116, 4

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"angmsd"
	.size	.L.str.117, 7

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"trie"
	.size	.L.str.118, 5

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"Kgr"
	.size	.L.str.119, 4

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"Ggr"
	.size	.L.str.120, 4

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"Mgr"
	.size	.L.str.121, 4

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"Egr"
	.size	.L.str.122, 4

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"Ogr"
	.size	.L.str.123, 4

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"Ngr"
	.size	.L.str.124, 4

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"Lgr"
	.size	.L.str.125, 4

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"Pgr"
	.size	.L.str.126, 4

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"Sgr"
	.size	.L.str.127, 4

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"cong"
	.size	.L.str.128, 5

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"doteqdot"
	.size	.L.str.129, 9

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"urcrop"
	.size	.L.str.130, 7

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"Rgr"
	.size	.L.str.131, 4

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"Xgr"
	.size	.L.str.132, 4

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"Tgr"
	.size	.L.str.133, 4

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"Ugr"
	.size	.L.str.134, 4

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"nless"
	.size	.L.str.135, 6

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	"LeftTee"
	.size	.L.str.136, 8

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"OverBar"
	.size	.L.str.137, 8

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"Zgr"
	.size	.L.str.138, 4

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"agr"
	.size	.L.str.139, 4

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"bgr"
	.size	.L.str.140, 4

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"phis"
	.size	.L.str.141, 5

	.type	.L.str.142,@object      # @.str.142
.L.str.142:
	.asciz	"dgr"
	.size	.L.str.142, 4

	.type	.L.str.143,@object      # @.str.143
.L.str.143:
	.asciz	"intlarhk"
	.size	.L.str.143, 9

	.type	.L.str.144,@object      # @.str.144
.L.str.144:
	.asciz	"boxplus"
	.size	.L.str.144, 8

	.type	.L.str.145,@object      # @.str.145
.L.str.145:
	.asciz	"ggr"
	.size	.L.str.145, 4

	.type	.L.str.146,@object      # @.str.146
.L.str.146:
	.asciz	"egr"
	.size	.L.str.146, 4

	.type	.L.str.147,@object      # @.str.147
.L.str.147:
	.asciz	"nrtrie"
	.size	.L.str.147, 7

	.type	.L.str.148,@object      # @.str.148
.L.str.148:
	.asciz	"rdldhar"
	.size	.L.str.148, 8

	.type	.L.str.149,@object      # @.str.149
.L.str.149:
	.asciz	"lrcorner"
	.size	.L.str.149, 9

	.type	.L.str.150,@object      # @.str.150
.L.str.150:
	.asciz	"igr"
	.size	.L.str.150, 4

	.type	.L.str.151,@object      # @.str.151
.L.str.151:
	.asciz	"lgr"
	.size	.L.str.151, 4

	.type	.L.str.152,@object      # @.str.152
.L.str.152:
	.asciz	"lhblk"
	.size	.L.str.152, 6

	.type	.L.str.153,@object      # @.str.153
.L.str.153:
	.asciz	"ngr"
	.size	.L.str.153, 4

	.type	.L.str.154,@object      # @.str.154
.L.str.154:
	.asciz	"mgr"
	.size	.L.str.154, 4

	.type	.L.str.155,@object      # @.str.155
.L.str.155:
	.asciz	"kgr"
	.size	.L.str.155, 4

	.type	.L.str.156,@object      # @.str.156
.L.str.156:
	.asciz	"ogr"
	.size	.L.str.156, 4

	.type	.L.str.157,@object      # @.str.157
.L.str.157:
	.asciz	"urcorner"
	.size	.L.str.157, 9

	.type	.L.str.158,@object      # @.str.158
.L.str.158:
	.asciz	"sgr"
	.size	.L.str.158, 4

	.type	.L.str.159,@object      # @.str.159
.L.str.159:
	.asciz	"boxUL"
	.size	.L.str.159, 6

	.type	.L.str.160,@object      # @.str.160
.L.str.160:
	.asciz	"npolint"
	.size	.L.str.160, 8

	.type	.L.str.161,@object      # @.str.161
.L.str.161:
	.asciz	"uhblk"
	.size	.L.str.161, 6

	.type	.L.str.162,@object      # @.str.162
.L.str.162:
	.asciz	"rgr"
	.size	.L.str.162, 4

	.type	.L.str.163,@object      # @.str.163
.L.str.163:
	.asciz	"curarrm"
	.size	.L.str.163, 8

	.type	.L.str.164,@object      # @.str.164
.L.str.164:
	.asciz	"pgr"
	.size	.L.str.164, 4

	.type	.L.str.165,@object      # @.str.165
.L.str.165:
	.asciz	"ruluhar"
	.size	.L.str.165, 8

	.type	.L.str.166,@object      # @.str.166
.L.str.166:
	.asciz	"lbbrk"
	.size	.L.str.166, 6

	.type	.L.str.167,@object      # @.str.167
.L.str.167:
	.asciz	"zgr"
	.size	.L.str.167, 4

	.type	.L.str.168,@object      # @.str.168
.L.str.168:
	.asciz	"tgr"
	.size	.L.str.168, 4

	.type	.L.str.169,@object      # @.str.169
.L.str.169:
	.asciz	"ugr"
	.size	.L.str.169, 4

	.type	.L.str.170,@object      # @.str.170
.L.str.170:
	.asciz	"rbbrk"
	.size	.L.str.170, 6

	.type	.L.str.171,@object      # @.str.171
.L.str.171:
	.asciz	"profline"
	.size	.L.str.171, 9

	.type	.L.str.172,@object      # @.str.172
.L.str.172:
	.asciz	"rlm"
	.size	.L.str.172, 4

	.type	.L.str.173,@object      # @.str.173
.L.str.173:
	.asciz	"OverBrace"
	.size	.L.str.173, 10

	.type	.L.str.174,@object      # @.str.174
.L.str.174:
	.asciz	"xgr"
	.size	.L.str.174, 4

	.type	.L.str.175,@object      # @.str.175
.L.str.175:
	.asciz	"lsaquo"
	.size	.L.str.175, 7

	.type	.L.str.176,@object      # @.str.176
.L.str.176:
	.asciz	"nwarrow"
	.size	.L.str.176, 8

	.type	.L.str.177,@object      # @.str.177
.L.str.177:
	.asciz	"Equilibrium"
	.size	.L.str.177, 12

	.type	.L.str.178,@object      # @.str.178
.L.str.178:
	.asciz	"rsaquo"
	.size	.L.str.178, 7

	.type	.L.str.179,@object      # @.str.179
.L.str.179:
	.asciz	"swarrow"
	.size	.L.str.179, 8

	.type	.L.str.180,@object      # @.str.180
.L.str.180:
	.asciz	"prec"
	.size	.L.str.180, 5

	.type	.L.str.181,@object      # @.str.181
.L.str.181:
	.asciz	"ltrie"
	.size	.L.str.181, 6

	.type	.L.str.182,@object      # @.str.182
.L.str.182:
	.asciz	"dharl"
	.size	.L.str.182, 6

	.type	.L.str.183,@object      # @.str.183
.L.str.183:
	.asciz	"shortmid"
	.size	.L.str.183, 9

	.type	.L.str.184,@object      # @.str.184
.L.str.184:
	.asciz	"nleftrightarrow"
	.size	.L.str.184, 16

	.type	.L.str.185,@object      # @.str.185
.L.str.185:
	.asciz	"rtrie"
	.size	.L.str.185, 6

	.type	.L.str.186,@object      # @.str.186
.L.str.186:
	.asciz	"Im"
	.size	.L.str.186, 3

	.type	.L.str.187,@object      # @.str.187
.L.str.187:
	.asciz	"frown"
	.size	.L.str.187, 6

	.type	.L.str.188,@object      # @.str.188
.L.str.188:
	.asciz	"pertenk"
	.size	.L.str.188, 8

	.type	.L.str.189,@object      # @.str.189
.L.str.189:
	.asciz	"lessgtr"
	.size	.L.str.189, 8

	.type	.L.str.190,@object      # @.str.190
.L.str.190:
	.asciz	"bigvee"
	.size	.L.str.190, 7

	.type	.L.str.191,@object      # @.str.191
.L.str.191:
	.asciz	"supseteq"
	.size	.L.str.191, 9

	.type	.L.str.192,@object      # @.str.192
.L.str.192:
	.asciz	"uharl"
	.size	.L.str.192, 6

	.type	.L.str.193,@object      # @.str.193
.L.str.193:
	.asciz	"Longrightarrow"
	.size	.L.str.193, 15

	.type	.L.str.194,@object      # @.str.194
.L.str.194:
	.asciz	"lrarr2"
	.size	.L.str.194, 7

	.type	.L.str.195,@object      # @.str.195
.L.str.195:
	.asciz	"wreath"
	.size	.L.str.195, 7

	.type	.L.str.196,@object      # @.str.196
.L.str.196:
	.asciz	"Updownarrow"
	.size	.L.str.196, 12

	.type	.L.str.197,@object      # @.str.197
.L.str.197:
	.asciz	"lessdot"
	.size	.L.str.197, 8

	.type	.L.str.198,@object      # @.str.198
.L.str.198:
	.asciz	"nlArr"
	.size	.L.str.198, 6

	.type	.L.str.199,@object      # @.str.199
.L.str.199:
	.asciz	"SOFTcy"
	.size	.L.str.199, 7

	.type	.L.str.200,@object      # @.str.200
.L.str.200:
	.asciz	"Barwed"
	.size	.L.str.200, 7

	.type	.L.str.201,@object      # @.str.201
.L.str.201:
	.asciz	"upharpoonleft"
	.size	.L.str.201, 14

	.type	.L.str.202,@object      # @.str.202
.L.str.202:
	.asciz	"FilledVerySmallSquare"
	.size	.L.str.202, 22

	.type	.L.str.203,@object      # @.str.203
.L.str.203:
	.asciz	"coprod"
	.size	.L.str.203, 7

	.type	.L.str.204,@object      # @.str.204
.L.str.204:
	.asciz	"pm"
	.size	.L.str.204, 3

	.type	.L.str.205,@object      # @.str.205
.L.str.205:
	.asciz	"nLeftarrow"
	.size	.L.str.205, 11

	.type	.L.str.206,@object      # @.str.206
.L.str.206:
	.asciz	"DScy"
	.size	.L.str.206, 5

	.type	.L.str.207,@object      # @.str.207
.L.str.207:
	.asciz	"xlArr"
	.size	.L.str.207, 6

	.type	.L.str.208,@object      # @.str.208
.L.str.208:
	.asciz	"longrightarrow"
	.size	.L.str.208, 15

	.type	.L.str.209,@object      # @.str.209
.L.str.209:
	.asciz	"subseteq"
	.size	.L.str.209, 9

	.type	.L.str.210,@object      # @.str.210
.L.str.210:
	.asciz	"updownarrow"
	.size	.L.str.210, 12

	.type	.L.str.211,@object      # @.str.211
.L.str.211:
	.asciz	"bsime"
	.size	.L.str.211, 6

	.type	.L.str.212,@object      # @.str.212
.L.str.212:
	.asciz	"supsetneq"
	.size	.L.str.212, 10

	.type	.L.str.213,@object      # @.str.213
.L.str.213:
	.asciz	"frac56"
	.size	.L.str.213, 7

	.type	.L.str.214,@object      # @.str.214
.L.str.214:
	.asciz	"TScy"
	.size	.L.str.214, 5

	.type	.L.str.215,@object      # @.str.215
.L.str.215:
	.asciz	"zeetrf"
	.size	.L.str.215, 7

	.type	.L.str.216,@object      # @.str.216
.L.str.216:
	.asciz	"ogon"
	.size	.L.str.216, 5

	.type	.L.str.217,@object      # @.str.217
.L.str.217:
	.asciz	"barwed"
	.size	.L.str.217, 7

	.type	.L.str.218,@object      # @.str.218
.L.str.218:
	.asciz	"nsime"
	.size	.L.str.218, 6

	.type	.L.str.219,@object      # @.str.219
.L.str.219:
	.asciz	"lEg"
	.size	.L.str.219, 4

	.type	.L.str.220,@object      # @.str.220
.L.str.220:
	.asciz	"lrhar"
	.size	.L.str.220, 6

	.type	.L.str.221,@object      # @.str.221
.L.str.221:
	.asciz	"boxminus"
	.size	.L.str.221, 9

	.type	.L.str.222,@object      # @.str.222
.L.str.222:
	.asciz	"ape"
	.size	.L.str.222, 4

	.type	.L.str.223,@object      # @.str.223
.L.str.223:
	.asciz	"divonx"
	.size	.L.str.223, 7

	.type	.L.str.224,@object      # @.str.224
.L.str.224:
	.asciz	"hksearow"
	.size	.L.str.224, 9

	.type	.L.str.225,@object      # @.str.225
.L.str.225:
	.asciz	"boxhD"
	.size	.L.str.225, 6

	.type	.L.str.226,@object      # @.str.226
.L.str.226:
	.asciz	"NotLessEqual"
	.size	.L.str.226, 13

	.type	.L.str.227,@object      # @.str.227
.L.str.227:
	.asciz	"ang90"
	.size	.L.str.227, 6

	.type	.L.str.228,@object      # @.str.228
.L.str.228:
	.asciz	"sstarf"
	.size	.L.str.228, 7

	.type	.L.str.229,@object      # @.str.229
.L.str.229:
	.asciz	"subsetneq"
	.size	.L.str.229, 10

	.type	.L.str.230,@object      # @.str.230
.L.str.230:
	.asciz	"Darr"
	.size	.L.str.230, 5

	.type	.L.str.231,@object      # @.str.231
.L.str.231:
	.asciz	"curarr"
	.size	.L.str.231, 7

	.type	.L.str.232,@object      # @.str.232
.L.str.232:
	.asciz	"real"
	.size	.L.str.232, 5

	.type	.L.str.233,@object      # @.str.233
.L.str.233:
	.asciz	"Larr"
	.size	.L.str.233, 5

	.type	.L.str.234,@object      # @.str.234
.L.str.234:
	.asciz	"CircleDot"
	.size	.L.str.234, 10

	.type	.L.str.235,@object      # @.str.235
.L.str.235:
	.asciz	"CircleTimes"
	.size	.L.str.235, 12

	.type	.L.str.236,@object      # @.str.236
.L.str.236:
	.asciz	"Rarr"
	.size	.L.str.236, 5

	.type	.L.str.237,@object      # @.str.237
.L.str.237:
	.asciz	"varpi"
	.size	.L.str.237, 6

	.type	.L.str.238,@object      # @.str.238
.L.str.238:
	.asciz	"SuchThat"
	.size	.L.str.238, 9

	.type	.L.str.239,@object      # @.str.239
.L.str.239:
	.asciz	"Uarr"
	.size	.L.str.239, 5

	.type	.L.str.240,@object      # @.str.240
.L.str.240:
	.asciz	"smeparsl"
	.size	.L.str.240, 9

	.type	.L.str.241,@object      # @.str.241
.L.str.241:
	.asciz	"orslope"
	.size	.L.str.241, 8

	.type	.L.str.242,@object      # @.str.242
.L.str.242:
	.asciz	"cuwed"
	.size	.L.str.242, 6

	.type	.L.str.243,@object      # @.str.243
.L.str.243:
	.asciz	"Breve"
	.size	.L.str.243, 6

	.type	.L.str.244,@object      # @.str.244
.L.str.244:
	.asciz	"nwarr"
	.size	.L.str.244, 6

	.type	.L.str.245,@object      # @.str.245
.L.str.245:
	.asciz	"orv"
	.size	.L.str.245, 4

	.type	.L.str.246,@object      # @.str.246
.L.str.246:
	.asciz	"zwj"
	.size	.L.str.246, 4

	.type	.L.str.247,@object      # @.str.247
.L.str.247:
	.asciz	"swarr"
	.size	.L.str.247, 6

	.type	.L.str.248,@object      # @.str.248
.L.str.248:
	.asciz	"darr"
	.size	.L.str.248, 5

	.type	.L.str.249,@object      # @.str.249
.L.str.249:
	.asciz	"gtrdot"
	.size	.L.str.249, 7

	.type	.L.str.250,@object      # @.str.250
.L.str.250:
	.asciz	"precneqq"
	.size	.L.str.250, 9

	.type	.L.str.251,@object      # @.str.251
.L.str.251:
	.asciz	"LessGreater"
	.size	.L.str.251, 12

	.type	.L.str.252,@object      # @.str.252
.L.str.252:
	.asciz	"harr"
	.size	.L.str.252, 5

	.type	.L.str.253,@object      # @.str.253
.L.str.253:
	.asciz	"UpperLeftArrow"
	.size	.L.str.253, 15

	.type	.L.str.254,@object      # @.str.254
.L.str.254:
	.asciz	"Verbar"
	.size	.L.str.254, 7

	.type	.L.str.255,@object      # @.str.255
.L.str.255:
	.asciz	"harrw"
	.size	.L.str.255, 6

	.type	.L.str.256,@object      # @.str.256
.L.str.256:
	.asciz	"TildeTilde"
	.size	.L.str.256, 11

	.type	.L.str.257,@object      # @.str.257
.L.str.257:
	.asciz	"larr"
	.size	.L.str.257, 5

	.type	.L.str.258,@object      # @.str.258
.L.str.258:
	.asciz	"succsim"
	.size	.L.str.258, 8

	.type	.L.str.259,@object      # @.str.259
.L.str.259:
	.asciz	"rarrw"
	.size	.L.str.259, 6

	.type	.L.str.260,@object      # @.str.260
.L.str.260:
	.asciz	"rarr"
	.size	.L.str.260, 5

	.type	.L.str.261,@object      # @.str.261
.L.str.261:
	.asciz	"prnE"
	.size	.L.str.261, 5

	.type	.L.str.262,@object      # @.str.262
.L.str.262:
	.asciz	"Hat"
	.size	.L.str.262, 4

	.type	.L.str.263,@object      # @.str.263
.L.str.263:
	.asciz	"uarr"
	.size	.L.str.263, 5

	.type	.L.str.264,@object      # @.str.264
.L.str.264:
	.asciz	"varr"
	.size	.L.str.264, 5

	.type	.L.str.265,@object      # @.str.265
.L.str.265:
	.asciz	"boxvR"
	.size	.L.str.265, 6

	.type	.L.str.266,@object      # @.str.266
.L.str.266:
	.asciz	"micro"
	.size	.L.str.266, 6

	.type	.L.str.267,@object      # @.str.267
.L.str.267:
	.asciz	"breve"
	.size	.L.str.267, 6

	.type	.L.str.268,@object      # @.str.268
.L.str.268:
	.asciz	"nequiv"
	.size	.L.str.268, 7

	.type	.L.str.269,@object      # @.str.269
.L.str.269:
	.asciz	"verbar"
	.size	.L.str.269, 7

	.type	.L.str.270,@object      # @.str.270
.L.str.270:
	.asciz	"lopar"
	.size	.L.str.270, 6

	.type	.L.str.271,@object      # @.str.271
.L.str.271:
	.asciz	"frac35"
	.size	.L.str.271, 7

	.type	.L.str.272,@object      # @.str.272
.L.str.272:
	.asciz	"RightArrow"
	.size	.L.str.272, 11

	.type	.L.str.273,@object      # @.str.273
.L.str.273:
	.asciz	"copy"
	.size	.L.str.273, 5

	.type	.L.str.274,@object      # @.str.274
.L.str.274:
	.asciz	"ropar"
	.size	.L.str.274, 6

	.type	.L.str.275,@object      # @.str.275
.L.str.275:
	.asciz	"nsubseteq"
	.size	.L.str.275, 10

	.type	.L.str.276,@object      # @.str.276
.L.str.276:
	.asciz	"nvlArr"
	.size	.L.str.276, 7

	.type	.L.str.277,@object      # @.str.277
.L.str.277:
	.asciz	"setminus"
	.size	.L.str.277, 9

	.type	.L.str.278,@object      # @.str.278
.L.str.278:
	.asciz	"boxvH"
	.size	.L.str.278, 6

	.type	.L.str.279,@object      # @.str.279
.L.str.279:
	.asciz	"doteq"
	.size	.L.str.279, 6

	.type	.L.str.280,@object      # @.str.280
.L.str.280:
	.asciz	"vartriangleleft"
	.size	.L.str.280, 16

	.type	.L.str.281,@object      # @.str.281
.L.str.281:
	.asciz	"RightTriangleBar"
	.size	.L.str.281, 17

	.type	.L.str.282,@object      # @.str.282
.L.str.282:
	.asciz	"shy"
	.size	.L.str.282, 4

	.type	.L.str.283,@object      # @.str.283
.L.str.283:
	.asciz	"RightUpVectorBar"
	.size	.L.str.283, 17

	.type	.L.str.284,@object      # @.str.284
.L.str.284:
	.asciz	"Kappa"
	.size	.L.str.284, 6

	.type	.L.str.285,@object      # @.str.285
.L.str.285:
	.asciz	"leftrightarrows"
	.size	.L.str.285, 16

	.type	.L.str.286,@object      # @.str.286
.L.str.286:
	.asciz	"sqsupset"
	.size	.L.str.286, 9

	.type	.L.str.287,@object      # @.str.287
.L.str.287:
	.asciz	"rationals"
	.size	.L.str.287, 10

	.type	.L.str.288,@object      # @.str.288
.L.str.288:
	.asciz	"cent"
	.size	.L.str.288, 5

	.type	.L.str.289,@object      # @.str.289
.L.str.289:
	.asciz	"lobrk"
	.size	.L.str.289, 6

	.type	.L.str.290,@object      # @.str.290
.L.str.290:
	.asciz	"OverBracket"
	.size	.L.str.290, 12

	.type	.L.str.291,@object      # @.str.291
.L.str.291:
	.asciz	"robrk"
	.size	.L.str.291, 6

	.type	.L.str.292,@object      # @.str.292
.L.str.292:
	.asciz	"Fouriertrf"
	.size	.L.str.292, 11

	.type	.L.str.293,@object      # @.str.293
.L.str.293:
	.asciz	"blacktriangleleft"
	.size	.L.str.293, 18

	.type	.L.str.294,@object      # @.str.294
.L.str.294:
	.asciz	"bumpe"
	.size	.L.str.294, 6

	.type	.L.str.295,@object      # @.str.295
.L.str.295:
	.asciz	"kappa"
	.size	.L.str.295, 6

	.type	.L.str.296,@object      # @.str.296
.L.str.296:
	.asciz	"weierp"
	.size	.L.str.296, 7

	.type	.L.str.297,@object      # @.str.297
.L.str.297:
	.asciz	"intcal"
	.size	.L.str.297, 7

	.type	.L.str.298,@object      # @.str.298
.L.str.298:
	.asciz	"rarrc"
	.size	.L.str.298, 6

	.type	.L.str.299,@object      # @.str.299
.L.str.299:
	.asciz	"iiint"
	.size	.L.str.299, 6

	.type	.L.str.300,@object      # @.str.300
.L.str.300:
	.asciz	"boxdr"
	.size	.L.str.300, 6

	.type	.L.str.301,@object      # @.str.301
.L.str.301:
	.asciz	"percnt"
	.size	.L.str.301, 7

	.type	.L.str.302,@object      # @.str.302
.L.str.302:
	.asciz	"top"
	.size	.L.str.302, 4

	.type	.L.str.303,@object      # @.str.303
.L.str.303:
	.asciz	"ffilig"
	.size	.L.str.303, 7

	.type	.L.str.304,@object      # @.str.304
.L.str.304:
	.asciz	"notnivc"
	.size	.L.str.304, 8

	.type	.L.str.305,@object      # @.str.305
.L.str.305:
	.asciz	"diams"
	.size	.L.str.305, 6

	.type	.L.str.306,@object      # @.str.306
.L.str.306:
	.asciz	"maltese"
	.size	.L.str.306, 8

	.type	.L.str.307,@object      # @.str.307
.L.str.307:
	.asciz	"vartriangleright"
	.size	.L.str.307, 17

	.type	.L.str.308,@object      # @.str.308
.L.str.308:
	.asciz	"OverParenthesis"
	.size	.L.str.308, 16

	.type	.L.str.309,@object      # @.str.309
.L.str.309:
	.asciz	"nesear"
	.size	.L.str.309, 7

	.type	.L.str.310,@object      # @.str.310
.L.str.310:
	.asciz	"LessFullEqual"
	.size	.L.str.310, 14

	.type	.L.str.311,@object      # @.str.311
.L.str.311:
	.asciz	"UpDownArrow"
	.size	.L.str.311, 12

	.type	.L.str.312,@object      # @.str.312
.L.str.312:
	.asciz	"measuredangle"
	.size	.L.str.312, 14

	.type	.L.str.313,@object      # @.str.313
.L.str.313:
	.asciz	"Leftarrow"
	.size	.L.str.313, 10

	.type	.L.str.314,@object      # @.str.314
.L.str.314:
	.asciz	"HumpDownHump"
	.size	.L.str.314, 13

	.type	.L.str.315,@object      # @.str.315
.L.str.315:
	.asciz	"frac14"
	.size	.L.str.315, 7

	.type	.L.str.316,@object      # @.str.316
.L.str.316:
	.asciz	"LeftUpVector"
	.size	.L.str.316, 13

	.type	.L.str.317,@object      # @.str.317
.L.str.317:
	.asciz	"bigotimes"
	.size	.L.str.317, 10

	.type	.L.str.318,@object      # @.str.318
.L.str.318:
	.asciz	"questeq"
	.size	.L.str.318, 8

	.type	.L.str.319,@object      # @.str.319
.L.str.319:
	.asciz	"GreaterFullEqual"
	.size	.L.str.319, 17

	.type	.L.str.320,@object      # @.str.320
.L.str.320:
	.asciz	"in"
	.size	.L.str.320, 3

	.type	.L.str.321,@object      # @.str.321
.L.str.321:
	.asciz	"Del"
	.size	.L.str.321, 4

	.type	.L.str.322,@object      # @.str.322
.L.str.322:
	.asciz	"CHcy"
	.size	.L.str.322, 5

	.type	.L.str.323,@object      # @.str.323
.L.str.323:
	.asciz	"supne"
	.size	.L.str.323, 6

	.type	.L.str.324,@object      # @.str.324
.L.str.324:
	.asciz	"leftarrow"
	.size	.L.str.324, 10

	.type	.L.str.325,@object      # @.str.325
.L.str.325:
	.asciz	"ges"
	.size	.L.str.325, 4

	.type	.L.str.326,@object      # @.str.326
.L.str.326:
	.asciz	"bnot"
	.size	.L.str.326, 5

	.type	.L.str.327,@object      # @.str.327
.L.str.327:
	.asciz	"mumap"
	.size	.L.str.327, 6

	.type	.L.str.328,@object      # @.str.328
.L.str.328:
	.asciz	"LeftTriangleEqual"
	.size	.L.str.328, 18

	.type	.L.str.329,@object      # @.str.329
.L.str.329:
	.asciz	"KHcy"
	.size	.L.str.329, 5

	.type	.L.str.330,@object      # @.str.330
.L.str.330:
	.asciz	"les"
	.size	.L.str.330, 4

	.type	.L.str.331,@object      # @.str.331
.L.str.331:
	.asciz	"xutri"
	.size	.L.str.331, 6

	.type	.L.str.332,@object      # @.str.332
.L.str.332:
	.asciz	"caron"
	.size	.L.str.332, 6

	.type	.L.str.333,@object      # @.str.333
.L.str.333:
	.asciz	"EqualTilde"
	.size	.L.str.333, 11

	.type	.L.str.334,@object      # @.str.334
.L.str.334:
	.asciz	"UnderBar"
	.size	.L.str.334, 9

	.type	.L.str.335,@object      # @.str.335
.L.str.335:
	.asciz	"SHcy"
	.size	.L.str.335, 5

	.type	.L.str.336,@object      # @.str.336
.L.str.336:
	.asciz	"ZHcy"
	.size	.L.str.336, 5

	.type	.L.str.337,@object      # @.str.337
.L.str.337:
	.asciz	"gel"
	.size	.L.str.337, 4

	.type	.L.str.338,@object      # @.str.338
.L.str.338:
	.asciz	"Vee"
	.size	.L.str.338, 4

	.type	.L.str.339,@object      # @.str.339
.L.str.339:
	.asciz	"subne"
	.size	.L.str.339, 6

	.type	.L.str.340,@object      # @.str.340
.L.str.340:
	.asciz	"ngtr"
	.size	.L.str.340, 5

	.type	.L.str.341,@object      # @.str.341
.L.str.341:
	.asciz	"isins"
	.size	.L.str.341, 6

	.type	.L.str.342,@object      # @.str.342
.L.str.342:
	.asciz	"angle"
	.size	.L.str.342, 6

	.type	.L.str.343,@object      # @.str.343
.L.str.343:
	.asciz	"ThinSpace"
	.size	.L.str.343, 10

	.type	.L.str.344,@object      # @.str.344
.L.str.344:
	.asciz	"samalg"
	.size	.L.str.344, 7

	.type	.L.str.345,@object      # @.str.345
.L.str.345:
	.asciz	"supseteqq"
	.size	.L.str.345, 10

	.type	.L.str.346,@object      # @.str.346
.L.str.346:
	.asciz	"SucceedsEqual"
	.size	.L.str.346, 14

	.type	.L.str.347,@object      # @.str.347
.L.str.347:
	.asciz	"vee"
	.size	.L.str.347, 4

	.type	.L.str.348,@object      # @.str.348
.L.str.348:
	.asciz	"approxeq"
	.size	.L.str.348, 9

	.type	.L.str.349,@object      # @.str.349
.L.str.349:
	.asciz	"leftarrowtail"
	.size	.L.str.349, 14

	.type	.L.str.350,@object      # @.str.350
.L.str.350:
	.asciz	"dHar"
	.size	.L.str.350, 5

	.type	.L.str.351,@object      # @.str.351
.L.str.351:
	.asciz	"andand"
	.size	.L.str.351, 7

	.type	.L.str.352,@object      # @.str.352
.L.str.352:
	.asciz	"dlarr"
	.size	.L.str.352, 6

	.type	.L.str.353,@object      # @.str.353
.L.str.353:
	.asciz	"circeq"
	.size	.L.str.353, 7

	.type	.L.str.354,@object      # @.str.354
.L.str.354:
	.asciz	"Lsh"
	.size	.L.str.354, 4

	.type	.L.str.355,@object      # @.str.355
.L.str.355:
	.asciz	"laquo"
	.size	.L.str.355, 6

	.type	.L.str.356,@object      # @.str.356
.L.str.356:
	.asciz	"coloneq"
	.size	.L.str.356, 8

	.type	.L.str.357,@object      # @.str.357
.L.str.357:
	.asciz	"llarr"
	.size	.L.str.357, 6

	.type	.L.str.358,@object      # @.str.358
.L.str.358:
	.asciz	"Int"
	.size	.L.str.358, 4

	.type	.L.str.359,@object      # @.str.359
.L.str.359:
	.asciz	"nlarr"
	.size	.L.str.359, 6

	.type	.L.str.360,@object      # @.str.360
.L.str.360:
	.asciz	"Rsh"
	.size	.L.str.360, 4

	.type	.L.str.361,@object      # @.str.361
.L.str.361:
	.asciz	"varepsilon"
	.size	.L.str.361, 11

	.type	.L.str.362,@object      # @.str.362
.L.str.362:
	.asciz	"olarr"
	.size	.L.str.362, 6

	.type	.L.str.363,@object      # @.str.363
.L.str.363:
	.asciz	"raquo"
	.size	.L.str.363, 6

	.type	.L.str.364,@object      # @.str.364
.L.str.364:
	.asciz	"slarr"
	.size	.L.str.364, 6

	.type	.L.str.365,@object      # @.str.365
.L.str.365:
	.asciz	"rlarr"
	.size	.L.str.365, 6

	.type	.L.str.366,@object      # @.str.366
.L.str.366:
	.asciz	"boxuR"
	.size	.L.str.366, 6

	.type	.L.str.367,@object      # @.str.367
.L.str.367:
	.asciz	"minus"
	.size	.L.str.367, 6

	.type	.L.str.368,@object      # @.str.368
.L.str.368:
	.asciz	"xlarr"
	.size	.L.str.368, 6

	.type	.L.str.369,@object      # @.str.369
.L.str.369:
	.asciz	"lHar"
	.size	.L.str.369, 5

	.type	.L.str.370,@object      # @.str.370
.L.str.370:
	.asciz	"rHar"
	.size	.L.str.370, 5

	.type	.L.str.371,@object      # @.str.371
.L.str.371:
	.asciz	"subseteqq"
	.size	.L.str.371, 10

	.type	.L.str.372,@object      # @.str.372
.L.str.372:
	.asciz	"uHar"
	.size	.L.str.372, 5

	.type	.L.str.373,@object      # @.str.373
.L.str.373:
	.asciz	"epsis"
	.size	.L.str.373, 6

	.type	.L.str.374,@object      # @.str.374
.L.str.374:
	.asciz	"ssetmn"
	.size	.L.str.374, 7

	.type	.L.str.375,@object      # @.str.375
.L.str.375:
	.asciz	"equals"
	.size	.L.str.375, 7

	.type	.L.str.376,@object      # @.str.376
.L.str.376:
	.asciz	"NotExists"
	.size	.L.str.376, 10

	.type	.L.str.377,@object      # @.str.377
.L.str.377:
	.asciz	"prnsim"
	.size	.L.str.377, 7

	.type	.L.str.378,@object      # @.str.378
.L.str.378:
	.asciz	"lsh"
	.size	.L.str.378, 4

	.type	.L.str.379,@object      # @.str.379
.L.str.379:
	.asciz	"curlyeqsucc"
	.size	.L.str.379, 12

	.type	.L.str.380,@object      # @.str.380
.L.str.380:
	.asciz	"int"
	.size	.L.str.380, 4

	.type	.L.str.381,@object      # @.str.381
.L.str.381:
	.asciz	"rsh"
	.size	.L.str.381, 4

	.type	.L.str.382,@object      # @.str.382
.L.str.382:
	.asciz	"LessSlantEqual"
	.size	.L.str.382, 15

	.type	.L.str.383,@object      # @.str.383
.L.str.383:
	.asciz	"DiacriticalDot"
	.size	.L.str.383, 15

	.type	.L.str.384,@object      # @.str.384
.L.str.384:
	.asciz	"nvDash"
	.size	.L.str.384, 7

	.type	.L.str.385,@object      # @.str.385
.L.str.385:
	.asciz	"prnap"
	.size	.L.str.385, 6

	.type	.L.str.386,@object      # @.str.386
.L.str.386:
	.asciz	"ZeroWidthSpace"
	.size	.L.str.386, 15

	.type	.L.str.387,@object      # @.str.387
.L.str.387:
	.asciz	"oplus"
	.size	.L.str.387, 6

	.type	.L.str.388,@object      # @.str.388
.L.str.388:
	.asciz	"middot"
	.size	.L.str.388, 7

	.type	.L.str.389,@object      # @.str.389
.L.str.389:
	.asciz	"emptyset"
	.size	.L.str.389, 9

	.type	.L.str.390,@object      # @.str.390
.L.str.390:
	.asciz	"uplus"
	.size	.L.str.390, 6

	.type	.L.str.391,@object      # @.str.391
.L.str.391:
	.asciz	"boxhu"
	.size	.L.str.391, 6

	.type	.L.str.392,@object      # @.str.392
.L.str.392:
	.asciz	"softcy"
	.size	.L.str.392, 7

	.type	.L.str.393,@object      # @.str.393
.L.str.393:
	.asciz	"squarf"
	.size	.L.str.393, 7

	.type	.L.str.394,@object      # @.str.394
.L.str.394:
	.asciz	"TripleDot"
	.size	.L.str.394, 10

	.type	.L.str.395,@object      # @.str.395
.L.str.395:
	.asciz	"DownTee"
	.size	.L.str.395, 8

	.type	.L.str.396,@object      # @.str.396
.L.str.396:
	.asciz	"cir"
	.size	.L.str.396, 4

	.type	.L.str.397,@object      # @.str.397
.L.str.397:
	.asciz	"expectation"
	.size	.L.str.397, 12

	.type	.L.str.398,@object      # @.str.398
.L.str.398:
	.asciz	"cirfnint"
	.size	.L.str.398, 9

	.type	.L.str.399,@object      # @.str.399
.L.str.399:
	.asciz	"sfrown"
	.size	.L.str.399, 7

	.type	.L.str.400,@object      # @.str.400
.L.str.400:
	.asciz	"ntriangleright"
	.size	.L.str.400, 15

	.type	.L.str.401,@object      # @.str.401
.L.str.401:
	.asciz	"nisd"
	.size	.L.str.401, 5

	.type	.L.str.402,@object      # @.str.402
.L.str.402:
	.asciz	"dash"
	.size	.L.str.402, 5

	.type	.L.str.403,@object      # @.str.403
.L.str.403:
	.asciz	"nvHarr"
	.size	.L.str.403, 7

	.type	.L.str.404,@object      # @.str.404
.L.str.404:
	.asciz	"hybull"
	.size	.L.str.404, 7

	.type	.L.str.405,@object      # @.str.405
.L.str.405:
	.asciz	"RightArrowBar"
	.size	.L.str.405, 14

	.type	.L.str.406,@object      # @.str.406
.L.str.406:
	.asciz	"leftrightsquigarrow"
	.size	.L.str.406, 20

	.type	.L.str.407,@object      # @.str.407
.L.str.407:
	.asciz	"minusb"
	.size	.L.str.407, 7

	.type	.L.str.408,@object      # @.str.408
.L.str.408:
	.asciz	"frac38"
	.size	.L.str.408, 7

	.type	.L.str.409,@object      # @.str.409
.L.str.409:
	.asciz	"centerdot"
	.size	.L.str.409, 10

	.type	.L.str.410,@object      # @.str.410
.L.str.410:
	.asciz	"nwnear"
	.size	.L.str.410, 7

	.type	.L.str.411,@object      # @.str.411
.L.str.411:
	.asciz	"larrpl"
	.size	.L.str.411, 7

	.type	.L.str.412,@object      # @.str.412
.L.str.412:
	.asciz	"hookleftarrow"
	.size	.L.str.412, 14

	.type	.L.str.413,@object      # @.str.413
.L.str.413:
	.asciz	"hellip"
	.size	.L.str.413, 7

	.type	.L.str.414,@object      # @.str.414
.L.str.414:
	.asciz	"rarrpl"
	.size	.L.str.414, 7

	.type	.L.str.415,@object      # @.str.415
.L.str.415:
	.asciz	"mid"
	.size	.L.str.415, 4

	.type	.L.str.416,@object      # @.str.416
.L.str.416:
	.asciz	"oast"
	.size	.L.str.416, 5

	.type	.L.str.417,@object      # @.str.417
.L.str.417:
	.asciz	"NotTildeTilde"
	.size	.L.str.417, 14

	.type	.L.str.418,@object      # @.str.418
.L.str.418:
	.asciz	"NotTilde"
	.size	.L.str.418, 9

	.type	.L.str.419,@object      # @.str.419
.L.str.419:
	.asciz	"ohacgr"
	.size	.L.str.419, 7

	.type	.L.str.420,@object      # @.str.420
.L.str.420:
	.asciz	"marker"
	.size	.L.str.420, 7

	.type	.L.str.421,@object      # @.str.421
.L.str.421:
	.asciz	"langle"
	.size	.L.str.421, 7

	.type	.L.str.422,@object      # @.str.422
.L.str.422:
	.asciz	"vellip"
	.size	.L.str.422, 7

	.type	.L.str.423,@object      # @.str.423
.L.str.423:
	.asciz	"NotRightTriangleEqual"
	.size	.L.str.423, 22

	.type	.L.str.424,@object      # @.str.424
.L.str.424:
	.asciz	"rangle"
	.size	.L.str.424, 7

	.type	.L.str.425,@object      # @.str.425
.L.str.425:
	.asciz	"gnsim"
	.size	.L.str.425, 6

	.type	.L.str.426,@object      # @.str.426
.L.str.426:
	.asciz	"malt"
	.size	.L.str.426, 5

	.type	.L.str.427,@object      # @.str.427
.L.str.427:
	.asciz	"times"
	.size	.L.str.427, 6

	.type	.L.str.428,@object      # @.str.428
.L.str.428:
	.asciz	"RightDownTeeVector"
	.size	.L.str.428, 19

	.type	.L.str.429,@object      # @.str.429
.L.str.429:
	.asciz	"lnsim"
	.size	.L.str.429, 6

	.type	.L.str.430,@object      # @.str.430
.L.str.430:
	.asciz	"preceq"
	.size	.L.str.430, 7

	.type	.L.str.431,@object      # @.str.431
.L.str.431:
	.asciz	"equiv"
	.size	.L.str.431, 6

	.type	.L.str.432,@object      # @.str.432
.L.str.432:
	.asciz	"geqq"
	.size	.L.str.432, 5

	.type	.L.str.433,@object      # @.str.433
.L.str.433:
	.asciz	"Iota"
	.size	.L.str.433, 5

	.type	.L.str.434,@object      # @.str.434
.L.str.434:
	.asciz	"leqq"
	.size	.L.str.434, 5

	.type	.L.str.435,@object      # @.str.435
.L.str.435:
	.asciz	"cuepr"
	.size	.L.str.435, 6

	.type	.L.str.436,@object      # @.str.436
.L.str.436:
	.asciz	"boxtimes"
	.size	.L.str.436, 9

	.type	.L.str.437,@object      # @.str.437
.L.str.437:
	.asciz	"numero"
	.size	.L.str.437, 7

	.type	.L.str.438,@object      # @.str.438
.L.str.438:
	.asciz	"setmn"
	.size	.L.str.438, 6

	.type	.L.str.439,@object      # @.str.439
.L.str.439:
	.asciz	"ForAll"
	.size	.L.str.439, 7

	.type	.L.str.440,@object      # @.str.440
.L.str.440:
	.asciz	"excl"
	.size	.L.str.440, 5

	.type	.L.str.441,@object      # @.str.441
.L.str.441:
	.asciz	"bsol"
	.size	.L.str.441, 5

	.type	.L.str.442,@object      # @.str.442
.L.str.442:
	.asciz	"imof"
	.size	.L.str.442, 5

	.type	.L.str.443,@object      # @.str.443
.L.str.443:
	.asciz	"dsol"
	.size	.L.str.443, 5

	.type	.L.str.444,@object      # @.str.444
.L.str.444:
	.asciz	"ic"
	.size	.L.str.444, 3

	.type	.L.str.445,@object      # @.str.445
.L.str.445:
	.asciz	"ReverseElement"
	.size	.L.str.445, 15

	.type	.L.str.446,@object      # @.str.446
.L.str.446:
	.asciz	"Exists"
	.size	.L.str.446, 7

	.type	.L.str.447,@object      # @.str.447
.L.str.447:
	.asciz	"parsl"
	.size	.L.str.447, 6

	.type	.L.str.448,@object      # @.str.448
.L.str.448:
	.asciz	"bprime"
	.size	.L.str.448, 7

	.type	.L.str.449,@object      # @.str.449
.L.str.449:
	.asciz	"Ccedil"
	.size	.L.str.449, 7

	.type	.L.str.450,@object      # @.str.450
.L.str.450:
	.asciz	"sc"
	.size	.L.str.450, 3

	.type	.L.str.451,@object      # @.str.451
.L.str.451:
	.asciz	"propto"
	.size	.L.str.451, 7

	.type	.L.str.452,@object      # @.str.452
.L.str.452:
	.asciz	"osol"
	.size	.L.str.452, 5

	.type	.L.str.453,@object      # @.str.453
.L.str.453:
	.asciz	"RightVectorBar"
	.size	.L.str.453, 15

	.type	.L.str.454,@object      # @.str.454
.L.str.454:
	.asciz	"iota"
	.size	.L.str.454, 5

	.type	.L.str.455,@object      # @.str.455
.L.str.455:
	.asciz	"boxDR"
	.size	.L.str.455, 6

	.type	.L.str.456,@object      # @.str.456
.L.str.456:
	.asciz	"Theta"
	.size	.L.str.456, 6

	.type	.L.str.457,@object      # @.str.457
.L.str.457:
	.asciz	"lharu"
	.size	.L.str.457, 6

	.type	.L.str.458,@object      # @.str.458
.L.str.458:
	.asciz	"Because"
	.size	.L.str.458, 8

	.type	.L.str.459,@object      # @.str.459
.L.str.459:
	.asciz	"qprime"
	.size	.L.str.459, 7

	.type	.L.str.460,@object      # @.str.460
.L.str.460:
	.asciz	"simrarr"
	.size	.L.str.460, 8

	.type	.L.str.461,@object      # @.str.461
.L.str.461:
	.asciz	"tprime"
	.size	.L.str.461, 7

	.type	.L.str.462,@object      # @.str.462
.L.str.462:
	.asciz	"rharu"
	.size	.L.str.462, 6

	.type	.L.str.463,@object      # @.str.463
.L.str.463:
	.asciz	"vprime"
	.size	.L.str.463, 7

	.type	.L.str.464,@object      # @.str.464
.L.str.464:
	.asciz	"YUcy"
	.size	.L.str.464, 5

	.type	.L.str.465,@object      # @.str.465
.L.str.465:
	.asciz	"plusmn"
	.size	.L.str.465, 7

	.type	.L.str.466,@object      # @.str.466
.L.str.466:
	.asciz	"planckh"
	.size	.L.str.466, 8

	.type	.L.str.467,@object      # @.str.467
.L.str.467:
	.asciz	"YIcy"
	.size	.L.str.467, 5

	.type	.L.str.468,@object      # @.str.468
.L.str.468:
	.asciz	"NotRightTriangle"
	.size	.L.str.468, 17

	.type	.L.str.469,@object      # @.str.469
.L.str.469:
	.asciz	"becaus"
	.size	.L.str.469, 7

	.type	.L.str.470,@object      # @.str.470
.L.str.470:
	.asciz	"PrecedesEqual"
	.size	.L.str.470, 14

	.type	.L.str.471,@object      # @.str.471
.L.str.471:
	.asciz	"nsupe"
	.size	.L.str.471, 6

	.type	.L.str.472,@object      # @.str.472
.L.str.472:
	.asciz	"ccedil"
	.size	.L.str.472, 7

	.type	.L.str.473,@object      # @.str.473
.L.str.473:
	.asciz	"yacy"
	.size	.L.str.473, 5

	.type	.L.str.474,@object      # @.str.474
.L.str.474:
	.asciz	"succnsim"
	.size	.L.str.474, 9

	.type	.L.str.475,@object      # @.str.475
.L.str.475:
	.asciz	"theta"
	.size	.L.str.475, 6

	.type	.L.str.476,@object      # @.str.476
.L.str.476:
	.asciz	"because"
	.size	.L.str.476, 8

	.type	.L.str.477,@object      # @.str.477
.L.str.477:
	.asciz	"pre"
	.size	.L.str.477, 4

	.type	.L.str.478,@object      # @.str.478
.L.str.478:
	.asciz	"sharp"
	.size	.L.str.478, 6

	.type	.L.str.479,@object      # @.str.479
.L.str.479:
	.asciz	"timesb"
	.size	.L.str.479, 7

	.type	.L.str.480,@object      # @.str.480
.L.str.480:
	.asciz	"Longleftarrow"
	.size	.L.str.480, 14

	.type	.L.str.481,@object      # @.str.481
.L.str.481:
	.asciz	"Sqrt"
	.size	.L.str.481, 5

	.type	.L.str.482,@object      # @.str.482
.L.str.482:
	.asciz	"Vert"
	.size	.L.str.482, 5

	.type	.L.str.483,@object      # @.str.483
.L.str.483:
	.asciz	"Rho"
	.size	.L.str.483, 4

	.type	.L.str.484,@object      # @.str.484
.L.str.484:
	.asciz	"LeftDownTeeVector"
	.size	.L.str.484, 18

	.type	.L.str.485,@object      # @.str.485
.L.str.485:
	.asciz	"erDot"
	.size	.L.str.485, 6

	.type	.L.str.486,@object      # @.str.486
.L.str.486:
	.asciz	"integers"
	.size	.L.str.486, 9

	.type	.L.str.487,@object      # @.str.487
.L.str.487:
	.asciz	"Dot"
	.size	.L.str.487, 4

	.type	.L.str.488,@object      # @.str.488
.L.str.488:
	.asciz	"eqvparsl"
	.size	.L.str.488, 9

	.type	.L.str.489,@object      # @.str.489
.L.str.489:
	.asciz	"efDot"
	.size	.L.str.489, 6

	.type	.L.str.490,@object      # @.str.490
.L.str.490:
	.asciz	"NewLine"
	.size	.L.str.490, 8

	.type	.L.str.491,@object      # @.str.491
.L.str.491:
	.asciz	"sbquo"
	.size	.L.str.491, 6

	.type	.L.str.492,@object      # @.str.492
.L.str.492:
	.asciz	"Diamond"
	.size	.L.str.492, 8

	.type	.L.str.493,@object      # @.str.493
.L.str.493:
	.asciz	"Not"
	.size	.L.str.493, 4

	.type	.L.str.494,@object      # @.str.494
.L.str.494:
	.asciz	"Eta"
	.size	.L.str.494, 4

	.type	.L.str.495,@object      # @.str.495
.L.str.495:
	.asciz	"longleftarrow"
	.size	.L.str.495, 14

	.type	.L.str.496,@object      # @.str.496
.L.str.496:
	.asciz	"starf"
	.size	.L.str.496, 6

	.type	.L.str.497,@object      # @.str.497
.L.str.497:
	.asciz	"RoundImplies"
	.size	.L.str.497, 13

	.type	.L.str.498,@object      # @.str.498
.L.str.498:
	.asciz	"RightAngleBracket"
	.size	.L.str.498, 18

	.type	.L.str.499,@object      # @.str.499
.L.str.499:
	.asciz	"diamondsuit"
	.size	.L.str.499, 12

	.type	.L.str.500,@object      # @.str.500
.L.str.500:
	.asciz	"vert"
	.size	.L.str.500, 5

	.type	.L.str.501,@object      # @.str.501
.L.str.501:
	.asciz	"eth"
	.size	.L.str.501, 4

	.type	.L.str.502,@object      # @.str.502
.L.str.502:
	.asciz	"NotReverseElement"
	.size	.L.str.502, 18

	.type	.L.str.503,@object      # @.str.503
.L.str.503:
	.asciz	"Longleftrightarrow"
	.size	.L.str.503, 19

	.type	.L.str.504,@object      # @.str.504
.L.str.504:
	.asciz	"ssmile"
	.size	.L.str.504, 7

	.type	.L.str.505,@object      # @.str.505
.L.str.505:
	.asciz	"bot"
	.size	.L.str.505, 4

	.type	.L.str.506,@object      # @.str.506
.L.str.506:
	.asciz	"supe"
	.size	.L.str.506, 5

	.type	.L.str.507,@object      # @.str.507
.L.str.507:
	.asciz	"dot"
	.size	.L.str.507, 4

	.type	.L.str.508,@object      # @.str.508
.L.str.508:
	.asciz	"pointint"
	.size	.L.str.508, 9

	.type	.L.str.509,@object      # @.str.509
.L.str.509:
	.asciz	"intercal"
	.size	.L.str.509, 9

	.type	.L.str.510,@object      # @.str.510
.L.str.510:
	.asciz	"rho"
	.size	.L.str.510, 4

	.type	.L.str.511,@object      # @.str.511
.L.str.511:
	.asciz	"primes"
	.size	.L.str.511, 7

	.type	.L.str.512,@object      # @.str.512
.L.str.512:
	.asciz	"fllig"
	.size	.L.str.512, 6

	.type	.L.str.513,@object      # @.str.513
.L.str.513:
	.asciz	"diamond"
	.size	.L.str.513, 8

	.type	.L.str.514,@object      # @.str.514
.L.str.514:
	.asciz	"not"
	.size	.L.str.514, 4

	.type	.L.str.515,@object      # @.str.515
.L.str.515:
	.asciz	"precapprox"
	.size	.L.str.515, 11

	.type	.L.str.516,@object      # @.str.516
.L.str.516:
	.asciz	"eta"
	.size	.L.str.516, 4

	.type	.L.str.517,@object      # @.str.517
.L.str.517:
	.asciz	"backprime"
	.size	.L.str.517, 10

	.type	.L.str.518,@object      # @.str.518
.L.str.518:
	.asciz	"GreaterEqualLess"
	.size	.L.str.518, 17

	.type	.L.str.519,@object      # @.str.519
.L.str.519:
	.asciz	"RightTeeArrow"
	.size	.L.str.519, 14

	.type	.L.str.520,@object      # @.str.520
.L.str.520:
	.asciz	"boxHd"
	.size	.L.str.520, 6

	.type	.L.str.521,@object      # @.str.521
.L.str.521:
	.asciz	"Gammad"
	.size	.L.str.521, 7

	.type	.L.str.522,@object      # @.str.522
.L.str.522:
	.asciz	"Assign"
	.size	.L.str.522, 7

	.type	.L.str.523,@object      # @.str.523
.L.str.523:
	.asciz	"squf"
	.size	.L.str.523, 5

	.type	.L.str.524,@object      # @.str.524
.L.str.524:
	.asciz	"angzarr"
	.size	.L.str.524, 8

	.type	.L.str.525,@object      # @.str.525
.L.str.525:
	.asciz	"longleftrightarrow"
	.size	.L.str.525, 19

	.type	.L.str.526,@object      # @.str.526
.L.str.526:
	.asciz	"pound"
	.size	.L.str.526, 6

	.type	.L.str.527,@object      # @.str.527
.L.str.527:
	.asciz	"topbot"
	.size	.L.str.527, 7

	.type	.L.str.528,@object      # @.str.528
.L.str.528:
	.asciz	"twoheadleftarrow"
	.size	.L.str.528, 17

	.type	.L.str.529,@object      # @.str.529
.L.str.529:
	.asciz	"Congruent"
	.size	.L.str.529, 10

	.type	.L.str.530,@object      # @.str.530
.L.str.530:
	.asciz	"nshortparallel"
	.size	.L.str.530, 15

	.type	.L.str.531,@object      # @.str.531
.L.str.531:
	.asciz	"LeftArrowRightArrow"
	.size	.L.str.531, 20

	.type	.L.str.532,@object      # @.str.532
.L.str.532:
	.asciz	"sube"
	.size	.L.str.532, 5

	.type	.L.str.533,@object      # @.str.533
.L.str.533:
	.asciz	"hamilt"
	.size	.L.str.533, 7

	.type	.L.str.534,@object      # @.str.534
.L.str.534:
	.asciz	"harrcir"
	.size	.L.str.534, 8

	.type	.L.str.535,@object      # @.str.535
.L.str.535:
	.asciz	"boxV"
	.size	.L.str.535, 5

	.type	.L.str.536,@object      # @.str.536
.L.str.536:
	.asciz	"gammad"
	.size	.L.str.536, 7

	.type	.L.str.537,@object      # @.str.537
.L.str.537:
	.asciz	"OpenCurlyDoubleQuote"
	.size	.L.str.537, 21

	.type	.L.str.538,@object      # @.str.538
.L.str.538:
	.asciz	"target"
	.size	.L.str.538, 7

	.type	.L.str.539,@object      # @.str.539
.L.str.539:
	.asciz	"bigsqcup"
	.size	.L.str.539, 9

	.type	.L.str.540,@object      # @.str.540
.L.str.540:
	.asciz	"boxVr"
	.size	.L.str.540, 6

	.type	.L.str.541,@object      # @.str.541
.L.str.541:
	.asciz	"eegr"
	.size	.L.str.541, 5

	.type	.L.str.542,@object      # @.str.542
.L.str.542:
	.asciz	"awint"
	.size	.L.str.542, 6

	.type	.L.str.543,@object      # @.str.543
.L.str.543:
	.asciz	"EEacgr"
	.size	.L.str.543, 7

	.type	.L.str.544,@object      # @.str.544
.L.str.544:
	.asciz	"cwint"
	.size	.L.str.544, 6

	.type	.L.str.545,@object      # @.str.545
.L.str.545:
	.asciz	"RightVector"
	.size	.L.str.545, 12

	.type	.L.str.546,@object      # @.str.546
.L.str.546:
	.asciz	"Vvdash"
	.size	.L.str.546, 7

	.type	.L.str.547,@object      # @.str.547
.L.str.547:
	.asciz	"Aacute"
	.size	.L.str.547, 7

	.type	.L.str.548,@object      # @.str.548
.L.str.548:
	.asciz	"Eacute"
	.size	.L.str.548, 7

	.type	.L.str.549,@object      # @.str.549
.L.str.549:
	.asciz	"Iacute"
	.size	.L.str.549, 7

	.type	.L.str.550,@object      # @.str.550
.L.str.550:
	.asciz	"boxHU"
	.size	.L.str.550, 6

	.type	.L.str.551,@object      # @.str.551
.L.str.551:
	.asciz	"daleth"
	.size	.L.str.551, 7

	.type	.L.str.552,@object      # @.str.552
.L.str.552:
	.asciz	"olcross"
	.size	.L.str.552, 8

	.type	.L.str.553,@object      # @.str.553
.L.str.553:
	.asciz	"eqcirc"
	.size	.L.str.553, 7

	.type	.L.str.554,@object      # @.str.554
.L.str.554:
	.asciz	"Oacute"
	.size	.L.str.554, 7

	.type	.L.str.555,@object      # @.str.555
.L.str.555:
	.asciz	"bigodot"
	.size	.L.str.555, 8

	.type	.L.str.556,@object      # @.str.556
.L.str.556:
	.asciz	"notinva"
	.size	.L.str.556, 8

	.type	.L.str.557,@object      # @.str.557
.L.str.557:
	.asciz	"LeftRightArrow"
	.size	.L.str.557, 15

	.type	.L.str.558,@object      # @.str.558
.L.str.558:
	.asciz	"lozenge"
	.size	.L.str.558, 8

	.type	.L.str.559,@object      # @.str.559
.L.str.559:
	.asciz	"Uacute"
	.size	.L.str.559, 7

	.type	.L.str.560,@object      # @.str.560
.L.str.560:
	.asciz	"nvdash"
	.size	.L.str.560, 7

	.type	.L.str.561,@object      # @.str.561
.L.str.561:
	.asciz	"Yacute"
	.size	.L.str.561, 7

	.type	.L.str.562,@object      # @.str.562
.L.str.562:
	.asciz	"boxvL"
	.size	.L.str.562, 6

	.type	.L.str.563,@object      # @.str.563
.L.str.563:
	.asciz	"Bscr"
	.size	.L.str.563, 5

	.type	.L.str.564,@object      # @.str.564
.L.str.564:
	.asciz	"Escr"
	.size	.L.str.564, 5

	.type	.L.str.565,@object      # @.str.565
.L.str.565:
	.asciz	"Fscr"
	.size	.L.str.565, 5

	.type	.L.str.566,@object      # @.str.566
.L.str.566:
	.asciz	"Hacek"
	.size	.L.str.566, 6

	.type	.L.str.567,@object      # @.str.567
.L.str.567:
	.asciz	"Hscr"
	.size	.L.str.567, 5

	.type	.L.str.568,@object      # @.str.568
.L.str.568:
	.asciz	"Iscr"
	.size	.L.str.568, 5

	.type	.L.str.569,@object      # @.str.569
.L.str.569:
	.asciz	"boxVh"
	.size	.L.str.569, 6

	.type	.L.str.570,@object      # @.str.570
.L.str.570:
	.asciz	"aacute"
	.size	.L.str.570, 7

	.type	.L.str.571,@object      # @.str.571
.L.str.571:
	.asciz	"Lscr"
	.size	.L.str.571, 5

	.type	.L.str.572,@object      # @.str.572
.L.str.572:
	.asciz	"Mscr"
	.size	.L.str.572, 5

	.type	.L.str.573,@object      # @.str.573
.L.str.573:
	.asciz	"Downarrow"
	.size	.L.str.573, 10

	.type	.L.str.574,@object      # @.str.574
.L.str.574:
	.asciz	"eacute"
	.size	.L.str.574, 7

	.type	.L.str.575,@object      # @.str.575
.L.str.575:
	.asciz	"iacute"
	.size	.L.str.575, 7

	.type	.L.str.576,@object      # @.str.576
.L.str.576:
	.asciz	"VerticalLine"
	.size	.L.str.576, 13

	.type	.L.str.577,@object      # @.str.577
.L.str.577:
	.asciz	"Rscr"
	.size	.L.str.577, 5

	.type	.L.str.578,@object      # @.str.578
.L.str.578:
	.asciz	"ap"
	.size	.L.str.578, 3

	.type	.L.str.579,@object      # @.str.579
.L.str.579:
	.asciz	"NegativeVeryThinSpace"
	.size	.L.str.579, 22

	.type	.L.str.580,@object      # @.str.580
.L.str.580:
	.asciz	"LessTilde"
	.size	.L.str.580, 10

	.type	.L.str.581,@object      # @.str.581
.L.str.581:
	.asciz	"oacute"
	.size	.L.str.581, 7

	.type	.L.str.582,@object      # @.str.582
.L.str.582:
	.asciz	"blk12"
	.size	.L.str.582, 6

	.type	.L.str.583,@object      # @.str.583
.L.str.583:
	.asciz	"frac25"
	.size	.L.str.583, 7

	.type	.L.str.584,@object      # @.str.584
.L.str.584:
	.asciz	"mp"
	.size	.L.str.584, 3

	.type	.L.str.585,@object      # @.str.585
.L.str.585:
	.asciz	"fnof"
	.size	.L.str.585, 5

	.type	.L.str.586,@object      # @.str.586
.L.str.586:
	.asciz	"Conint"
	.size	.L.str.586, 7

	.type	.L.str.587,@object      # @.str.587
.L.str.587:
	.asciz	"uacute"
	.size	.L.str.587, 7

	.type	.L.str.588,@object      # @.str.588
.L.str.588:
	.asciz	"dd"
	.size	.L.str.588, 3

	.type	.L.str.589,@object      # @.str.589
.L.str.589:
	.asciz	"scsim"
	.size	.L.str.589, 6

	.type	.L.str.590,@object      # @.str.590
.L.str.590:
	.asciz	"NotSucceeds"
	.size	.L.str.590, 12

	.type	.L.str.591,@object      # @.str.591
.L.str.591:
	.asciz	"yacute"
	.size	.L.str.591, 7

	.type	.L.str.592,@object      # @.str.592
.L.str.592:
	.asciz	"wp"
	.size	.L.str.592, 3

	.type	.L.str.593,@object      # @.str.593
.L.str.593:
	.asciz	"infin"
	.size	.L.str.593, 6

	.type	.L.str.594,@object      # @.str.594
.L.str.594:
	.asciz	"escr"
	.size	.L.str.594, 5

	.type	.L.str.595,@object      # @.str.595
.L.str.595:
	.asciz	"DJcy"
	.size	.L.str.595, 5

	.type	.L.str.596,@object      # @.str.596
.L.str.596:
	.asciz	"gscr"
	.size	.L.str.596, 5

	.type	.L.str.597,@object      # @.str.597
.L.str.597:
	.asciz	"LowerRightArrow"
	.size	.L.str.597, 16

	.type	.L.str.598,@object      # @.str.598
.L.str.598:
	.asciz	"GJcy"
	.size	.L.str.598, 5

	.type	.L.str.599,@object      # @.str.599
.L.str.599:
	.asciz	"dzcy"
	.size	.L.str.599, 5

	.type	.L.str.600,@object      # @.str.600
.L.str.600:
	.asciz	"brvbar"
	.size	.L.str.600, 7

	.type	.L.str.601,@object      # @.str.601
.L.str.601:
	.asciz	"sqcup"
	.size	.L.str.601, 6

	.type	.L.str.602,@object      # @.str.602
.L.str.602:
	.asciz	"KJcy"
	.size	.L.str.602, 5

	.type	.L.str.603,@object      # @.str.603
.L.str.603:
	.asciz	"LJcy"
	.size	.L.str.603, 5

	.type	.L.str.604,@object      # @.str.604
.L.str.604:
	.asciz	"oscr"
	.size	.L.str.604, 5

	.type	.L.str.605,@object      # @.str.605
.L.str.605:
	.asciz	"NJcy"
	.size	.L.str.605, 5

	.type	.L.str.606,@object      # @.str.606
.L.str.606:
	.asciz	"ddagger"
	.size	.L.str.606, 8

	.type	.L.str.607,@object      # @.str.607
.L.str.607:
	.asciz	"Agrave"
	.size	.L.str.607, 7

	.type	.L.str.608,@object      # @.str.608
.L.str.608:
	.asciz	"downarrow"
	.size	.L.str.608, 10

	.type	.L.str.609,@object      # @.str.609
.L.str.609:
	.asciz	"duhar"
	.size	.L.str.609, 6

	.type	.L.str.610,@object      # @.str.610
.L.str.610:
	.asciz	"Egrave"
	.size	.L.str.610, 7

	.type	.L.str.611,@object      # @.str.611
.L.str.611:
	.asciz	"Oslash"
	.size	.L.str.611, 7

	.type	.L.str.612,@object      # @.str.612
.L.str.612:
	.asciz	"preccurlyeq"
	.size	.L.str.612, 12

	.type	.L.str.613,@object      # @.str.613
.L.str.613:
	.asciz	"Igrave"
	.size	.L.str.613, 7

	.type	.L.str.614,@object      # @.str.614
.L.str.614:
	.asciz	"conint"
	.size	.L.str.614, 7

	.type	.L.str.615,@object      # @.str.615
.L.str.615:
	.asciz	"drbkarow"
	.size	.L.str.615, 9

	.type	.L.str.616,@object      # @.str.616
.L.str.616:
	.asciz	"DownRightTeeVector"
	.size	.L.str.616, 19

	.type	.L.str.617,@object      # @.str.617
.L.str.617:
	.asciz	"odot"
	.size	.L.str.617, 5

	.type	.L.str.618,@object      # @.str.618
.L.str.618:
	.asciz	"Ograve"
	.size	.L.str.618, 7

	.type	.L.str.619,@object      # @.str.619
.L.str.619:
	.asciz	"ldot"
	.size	.L.str.619, 5

	.type	.L.str.620,@object      # @.str.620
.L.str.620:
	.asciz	"LeftAngleBracket"
	.size	.L.str.620, 17

	.type	.L.str.621,@object      # @.str.621
.L.str.621:
	.asciz	"tdot"
	.size	.L.str.621, 5

	.type	.L.str.622,@object      # @.str.622
.L.str.622:
	.asciz	"idiagr"
	.size	.L.str.622, 7

	.type	.L.str.623,@object      # @.str.623
.L.str.623:
	.asciz	"sdot"
	.size	.L.str.623, 5

	.type	.L.str.624,@object      # @.str.624
.L.str.624:
	.asciz	"Ugrave"
	.size	.L.str.624, 7

	.type	.L.str.625,@object      # @.str.625
.L.str.625:
	.asciz	"triangleleft"
	.size	.L.str.625, 13

	.type	.L.str.626,@object      # @.str.626
.L.str.626:
	.asciz	"diam"
	.size	.L.str.626, 5

	.type	.L.str.627,@object      # @.str.627
.L.str.627:
	.asciz	"commat"
	.size	.L.str.627, 7

	.type	.L.str.628,@object      # @.str.628
.L.str.628:
	.asciz	"udiagr"
	.size	.L.str.628, 7

	.type	.L.str.629,@object      # @.str.629
.L.str.629:
	.asciz	"larrb"
	.size	.L.str.629, 6

	.type	.L.str.630,@object      # @.str.630
.L.str.630:
	.asciz	"hslash"
	.size	.L.str.630, 7

	.type	.L.str.631,@object      # @.str.631
.L.str.631:
	.asciz	"rightleftarrows"
	.size	.L.str.631, 16

	.type	.L.str.632,@object      # @.str.632
.L.str.632:
	.asciz	"succnapprox"
	.size	.L.str.632, 12

	.type	.L.str.633,@object      # @.str.633
.L.str.633:
	.asciz	"agrave"
	.size	.L.str.633, 7

	.type	.L.str.634,@object      # @.str.634
.L.str.634:
	.asciz	"egs"
	.size	.L.str.634, 4

	.type	.L.str.635,@object      # @.str.635
.L.str.635:
	.asciz	"egrave"
	.size	.L.str.635, 7

	.type	.L.str.636,@object      # @.str.636
.L.str.636:
	.asciz	"oslash"
	.size	.L.str.636, 7

	.type	.L.str.637,@object      # @.str.637
.L.str.637:
	.asciz	"nge"
	.size	.L.str.637, 4

	.type	.L.str.638,@object      # @.str.638
.L.str.638:
	.asciz	"igrave"
	.size	.L.str.638, 7

	.type	.L.str.639,@object      # @.str.639
.L.str.639:
	.asciz	"hbar"
	.size	.L.str.639, 5

	.type	.L.str.640,@object      # @.str.640
.L.str.640:
	.asciz	"NestedLessLess"
	.size	.L.str.640, 15

	.type	.L.str.641,@object      # @.str.641
.L.str.641:
	.asciz	"nexist"
	.size	.L.str.641, 7

	.type	.L.str.642,@object      # @.str.642
.L.str.642:
	.asciz	"ograve"
	.size	.L.str.642, 7

	.type	.L.str.643,@object      # @.str.643
.L.str.643:
	.asciz	"ldquor"
	.size	.L.str.643, 7

	.type	.L.str.644,@object      # @.str.644
.L.str.644:
	.asciz	"rdquor"
	.size	.L.str.644, 7

	.type	.L.str.645,@object      # @.str.645
.L.str.645:
	.asciz	"ugrave"
	.size	.L.str.645, 7

	.type	.L.str.646,@object      # @.str.646
.L.str.646:
	.asciz	"div"
	.size	.L.str.646, 4

	.type	.L.str.647,@object      # @.str.647
.L.str.647:
	.asciz	"blacktriangledown"
	.size	.L.str.647, 18

	.type	.L.str.648,@object      # @.str.648
.L.str.648:
	.asciz	"UpArrow"
	.size	.L.str.648, 8

	.type	.L.str.649,@object      # @.str.649
.L.str.649:
	.asciz	"niv"
	.size	.L.str.649, 4

	.type	.L.str.650,@object      # @.str.650
.L.str.650:
	.asciz	"llhard"
	.size	.L.str.650, 7

	.type	.L.str.651,@object      # @.str.651
.L.str.651:
	.asciz	"boxdl"
	.size	.L.str.651, 6

	.type	.L.str.652,@object      # @.str.652
.L.str.652:
	.asciz	"piv"
	.size	.L.str.652, 4

	.type	.L.str.653,@object      # @.str.653
.L.str.653:
	.asciz	"NotPrecedes"
	.size	.L.str.653, 12

	.type	.L.str.654,@object      # @.str.654
.L.str.654:
	.asciz	"lbarr"
	.size	.L.str.654, 6

	.type	.L.str.655,@object      # @.str.655
.L.str.655:
	.asciz	"andd"
	.size	.L.str.655, 5

	.type	.L.str.656,@object      # @.str.656
.L.str.656:
	.asciz	"bigwedge"
	.size	.L.str.656, 9

	.type	.L.str.657,@object      # @.str.657
.L.str.657:
	.asciz	"InvisibleTimes"
	.size	.L.str.657, 15

	.type	.L.str.658,@object      # @.str.658
.L.str.658:
	.asciz	"rbarr"
	.size	.L.str.658, 6

	.type	.L.str.659,@object      # @.str.659
.L.str.659:
	.asciz	"ApplyFunction"
	.size	.L.str.659, 14

	.type	.L.str.660,@object      # @.str.660
.L.str.660:
	.asciz	"bottom"
	.size	.L.str.660, 7

	.type	.L.str.661,@object      # @.str.661
.L.str.661:
	.asciz	"awconint"
	.size	.L.str.661, 9

	.type	.L.str.662,@object      # @.str.662
.L.str.662:
	.asciz	"cwconint"
	.size	.L.str.662, 9

	.type	.L.str.663,@object      # @.str.663
.L.str.663:
	.asciz	"dwangle"
	.size	.L.str.663, 8

	.type	.L.str.664,@object      # @.str.664
.L.str.664:
	.asciz	"rarrb"
	.size	.L.str.664, 6

	.type	.L.str.665,@object      # @.str.665
.L.str.665:
	.asciz	"UpArrowBar"
	.size	.L.str.665, 11

	.type	.L.str.666,@object      # @.str.666
.L.str.666:
	.asciz	"realine"
	.size	.L.str.666, 8

	.type	.L.str.667,@object      # @.str.667
.L.str.667:
	.asciz	"uwangle"
	.size	.L.str.667, 8

	.type	.L.str.668,@object      # @.str.668
.L.str.668:
	.asciz	"lowbar"
	.size	.L.str.668, 7

	.type	.L.str.669,@object      # @.str.669
.L.str.669:
	.asciz	"Cayleys"
	.size	.L.str.669, 8

	.type	.L.str.670,@object      # @.str.670
.L.str.670:
	.asciz	"bigstar"
	.size	.L.str.670, 8

	.type	.L.str.671,@object      # @.str.671
.L.str.671:
	.asciz	"isin"
	.size	.L.str.671, 5

	.type	.L.str.672,@object      # @.str.672
.L.str.672:
	.asciz	"backepsilon"
	.size	.L.str.672, 12

	.type	.L.str.673,@object      # @.str.673
.L.str.673:
	.asciz	"OpenCurlyQuote"
	.size	.L.str.673, 15

	.type	.L.str.674,@object      # @.str.674
.L.str.674:
	.asciz	"Cross"
	.size	.L.str.674, 6

	.type	.L.str.675,@object      # @.str.675
.L.str.675:
	.asciz	"lesseqgtr"
	.size	.L.str.675, 10

	.type	.L.str.676,@object      # @.str.676
.L.str.676:
	.asciz	"Upsi"
	.size	.L.str.676, 5

	.type	.L.str.677,@object      # @.str.677
.L.str.677:
	.asciz	"DoubleRightArrow"
	.size	.L.str.677, 17

	.type	.L.str.678,@object      # @.str.678
.L.str.678:
	.asciz	"Laplacetrf"
	.size	.L.str.678, 11

	.type	.L.str.679,@object      # @.str.679
.L.str.679:
	.asciz	"bigcirc"
	.size	.L.str.679, 8

	.type	.L.str.680,@object      # @.str.680
.L.str.680:
	.asciz	"epsi"
	.size	.L.str.680, 5

	.type	.L.str.681,@object      # @.str.681
.L.str.681:
	.asciz	"LeftTeeVector"
	.size	.L.str.681, 14

	.type	.L.str.682,@object      # @.str.682
.L.str.682:
	.asciz	"DoubleUpDownArrow"
	.size	.L.str.682, 18

	.type	.L.str.683,@object      # @.str.683
.L.str.683:
	.asciz	"DiacriticalGrave"
	.size	.L.str.683, 17

	.type	.L.str.684,@object      # @.str.684
.L.str.684:
	.asciz	"triangle"
	.size	.L.str.684, 9

	.type	.L.str.685,@object      # @.str.685
.L.str.685:
	.asciz	"permil"
	.size	.L.str.685, 7

	.type	.L.str.686,@object      # @.str.686
.L.str.686:
	.asciz	"lpargt"
	.size	.L.str.686, 7

	.type	.L.str.687,@object      # @.str.687
.L.str.687:
	.asciz	"UpEquilibrium"
	.size	.L.str.687, 14

	.type	.L.str.688,@object      # @.str.688
.L.str.688:
	.asciz	"backsimeq"
	.size	.L.str.688, 10

	.type	.L.str.689,@object      # @.str.689
.L.str.689:
	.asciz	"Supset"
	.size	.L.str.689, 7

	.type	.L.str.690,@object      # @.str.690
.L.str.690:
	.asciz	"boxUr"
	.size	.L.str.690, 6

	.type	.L.str.691,@object      # @.str.691
.L.str.691:
	.asciz	"sccue"
	.size	.L.str.691, 6

	.type	.L.str.692,@object      # @.str.692
.L.str.692:
	.asciz	"rpargt"
	.size	.L.str.692, 7

	.type	.L.str.693,@object      # @.str.693
.L.str.693:
	.asciz	"cross"
	.size	.L.str.693, 6

	.type	.L.str.694,@object      # @.str.694
.L.str.694:
	.asciz	"upsi"
	.size	.L.str.694, 5

	.type	.L.str.695,@object      # @.str.695
.L.str.695:
	.asciz	"cularr"
	.size	.L.str.695, 7

	.type	.L.str.696,@object      # @.str.696
.L.str.696:
	.asciz	"varkappa"
	.size	.L.str.696, 9

	.type	.L.str.697,@object      # @.str.697
.L.str.697:
	.asciz	"half"
	.size	.L.str.697, 5

	.type	.L.str.698,@object      # @.str.698
.L.str.698:
	.asciz	"sfgr"
	.size	.L.str.698, 5

	.type	.L.str.699,@object      # @.str.699
.L.str.699:
	.asciz	"ImaginaryI"
	.size	.L.str.699, 11

	.type	.L.str.700,@object      # @.str.700
.L.str.700:
	.asciz	"Tau"
	.size	.L.str.700, 4

	.type	.L.str.701,@object      # @.str.701
.L.str.701:
	.asciz	"bernou"
	.size	.L.str.701, 7

	.type	.L.str.702,@object      # @.str.702
.L.str.702:
	.asciz	"DoubleLeftArrow"
	.size	.L.str.702, 16

	.type	.L.str.703,@object      # @.str.703
.L.str.703:
	.asciz	"supset"
	.size	.L.str.703, 7

	.type	.L.str.704,@object      # @.str.704
.L.str.704:
	.asciz	"MediumSpace"
	.size	.L.str.704, 12

	.type	.L.str.705,@object      # @.str.705
.L.str.705:
	.asciz	"Subset"
	.size	.L.str.705, 7

	.type	.L.str.706,@object      # @.str.706
.L.str.706:
	.asciz	"NegativeThickSpace"
	.size	.L.str.706, 19

	.type	.L.str.707,@object      # @.str.707
.L.str.707:
	.asciz	"boxuL"
	.size	.L.str.707, 6

	.type	.L.str.708,@object      # @.str.708
.L.str.708:
	.asciz	"notin"
	.size	.L.str.708, 6

	.type	.L.str.709,@object      # @.str.709
.L.str.709:
	.asciz	"Re"
	.size	.L.str.709, 3

	.type	.L.str.710,@object      # @.str.710
.L.str.710:
	.asciz	"nleq"
	.size	.L.str.710, 5

	.type	.L.str.711,@object      # @.str.711
.L.str.711:
	.asciz	"nLeftrightarrow"
	.size	.L.str.711, 16

	.type	.L.str.712,@object      # @.str.712
.L.str.712:
	.asciz	"Beta"
	.size	.L.str.712, 5

	.type	.L.str.713,@object      # @.str.713
.L.str.713:
	.asciz	"dollar"
	.size	.L.str.713, 7

	.type	.L.str.714,@object      # @.str.714
.L.str.714:
	.asciz	"Product"
	.size	.L.str.714, 8

	.type	.L.str.715,@object      # @.str.715
.L.str.715:
	.asciz	"tau"
	.size	.L.str.715, 4

	.type	.L.str.716,@object      # @.str.716
.L.str.716:
	.asciz	"Poincareplane"
	.size	.L.str.716, 14

	.type	.L.str.717,@object      # @.str.717
.L.str.717:
	.asciz	"ee"
	.size	.L.str.717, 3

	.type	.L.str.718,@object      # @.str.718
.L.str.718:
	.asciz	"CenterDot"
	.size	.L.str.718, 10

	.type	.L.str.719,@object      # @.str.719
.L.str.719:
	.asciz	"Jsercy"
	.size	.L.str.719, 7

	.type	.L.str.720,@object      # @.str.720
.L.str.720:
	.asciz	"ge"
	.size	.L.str.720, 3

	.type	.L.str.721,@object      # @.str.721
.L.str.721:
	.asciz	"Rarrtl"
	.size	.L.str.721, 7

	.type	.L.str.722,@object      # @.str.722
.L.str.722:
	.asciz	"subset"
	.size	.L.str.722, 7

	.type	.L.str.723,@object      # @.str.723
.L.str.723:
	.asciz	"le"
	.size	.L.str.723, 3

	.type	.L.str.724,@object      # @.str.724
.L.str.724:
	.asciz	"ne"
	.size	.L.str.724, 3

	.type	.L.str.725,@object      # @.str.725
.L.str.725:
	.asciz	"mapstoleft"
	.size	.L.str.725, 11

	.type	.L.str.726,@object      # @.str.726
.L.str.726:
	.asciz	"NotSucceedsSlantEqual"
	.size	.L.str.726, 22

	.type	.L.str.727,@object      # @.str.727
.L.str.727:
	.asciz	"upsih"
	.size	.L.str.727, 6

	.type	.L.str.728,@object      # @.str.728
.L.str.728:
	.asciz	"cedil"
	.size	.L.str.728, 6

	.type	.L.str.729,@object      # @.str.729
.L.str.729:
	.asciz	"Zeta"
	.size	.L.str.729, 5

	.type	.L.str.730,@object      # @.str.730
.L.str.730:
	.asciz	"apos"
	.size	.L.str.730, 5

	.type	.L.str.731,@object      # @.str.731
.L.str.731:
	.asciz	"nsube"
	.size	.L.str.731, 6

	.type	.L.str.732,@object      # @.str.732
.L.str.732:
	.asciz	"beta"
	.size	.L.str.732, 5

	.type	.L.str.733,@object      # @.str.733
.L.str.733:
	.asciz	"frac78"
	.size	.L.str.733, 7

	.type	.L.str.734,@object      # @.str.734
.L.str.734:
	.asciz	"nltri"
	.size	.L.str.734, 6

	.type	.L.str.735,@object      # @.str.735
.L.str.735:
	.asciz	"iiiint"
	.size	.L.str.735, 7

	.type	.L.str.736,@object      # @.str.736
.L.str.736:
	.asciz	"veebar"
	.size	.L.str.736, 7

	.type	.L.str.737,@object      # @.str.737
.L.str.737:
	.asciz	"RightTeeVector"
	.size	.L.str.737, 15

	.type	.L.str.738,@object      # @.str.738
.L.str.738:
	.asciz	"Dagger"
	.size	.L.str.738, 7

	.type	.L.str.739,@object      # @.str.739
.L.str.739:
	.asciz	"vltri"
	.size	.L.str.739, 6

	.type	.L.str.740,@object      # @.str.740
.L.str.740:
	.asciz	"larrtl"
	.size	.L.str.740, 7

	.type	.L.str.741,@object      # @.str.741
.L.str.741:
	.asciz	"iocy"
	.size	.L.str.741, 5

	.type	.L.str.742,@object      # @.str.742
.L.str.742:
	.asciz	"dashv"
	.size	.L.str.742, 6

	.type	.L.str.743,@object      # @.str.743
.L.str.743:
	.asciz	"frac12"
	.size	.L.str.743, 7

	.type	.L.str.744,@object      # @.str.744
.L.str.744:
	.asciz	"GreaterLess"
	.size	.L.str.744, 12

	.type	.L.str.745,@object      # @.str.745
.L.str.745:
	.asciz	"jsercy"
	.size	.L.str.745, 7

	.type	.L.str.746,@object      # @.str.746
.L.str.746:
	.asciz	"rarrtl"
	.size	.L.str.746, 7

	.type	.L.str.747,@object      # @.str.747
.L.str.747:
	.asciz	"oline"
	.size	.L.str.747, 6

	.type	.L.str.748,@object      # @.str.748
.L.str.748:
	.asciz	"sup3"
	.size	.L.str.748, 5

	.type	.L.str.749,@object      # @.str.749
.L.str.749:
	.asciz	"emsp13"
	.size	.L.str.749, 7

	.type	.L.str.750,@object      # @.str.750
.L.str.750:
	.asciz	"asymp"
	.size	.L.str.750, 6

	.type	.L.str.751,@object      # @.str.751
.L.str.751:
	.asciz	"zeta"
	.size	.L.str.751, 5

	.type	.L.str.752,@object      # @.str.752
.L.str.752:
	.asciz	"UpperRightArrow"
	.size	.L.str.752, 16

	.type	.L.str.753,@object      # @.str.753
.L.str.753:
	.asciz	"smallsetminus"
	.size	.L.str.753, 14

	.type	.L.str.754,@object      # @.str.754
.L.str.754:
	.asciz	"LeftUpTeeVector"
	.size	.L.str.754, 16

	.type	.L.str.755,@object      # @.str.755
.L.str.755:
	.asciz	"dagger"
	.size	.L.str.755, 7

	.type	.L.str.756,@object      # @.str.756
.L.str.756:
	.asciz	"LeftDownVector"
	.size	.L.str.756, 15

	.type	.L.str.757,@object      # @.str.757
.L.str.757:
	.asciz	"sime"
	.size	.L.str.757, 5

	.type	.L.str.758,@object      # @.str.758
.L.str.758:
	.asciz	"precsim"
	.size	.L.str.758, 8

	.type	.L.str.759,@object      # @.str.759
.L.str.759:
	.asciz	"MinusPlus"
	.size	.L.str.759, 10

	.type	.L.str.760,@object      # @.str.760
.L.str.760:
	.asciz	"LeftVector"
	.size	.L.str.760, 11

	.type	.L.str.761,@object      # @.str.761
.L.str.761:
	.asciz	"profalar"
	.size	.L.str.761, 9

	.type	.L.str.762,@object      # @.str.762
.L.str.762:
	.asciz	"dtri"
	.size	.L.str.762, 5

	.type	.L.str.763,@object      # @.str.763
.L.str.763:
	.asciz	"between"
	.size	.L.str.763, 8

	.type	.L.str.764,@object      # @.str.764
.L.str.764:
	.asciz	"hyphen"
	.size	.L.str.764, 7

	.type	.L.str.765,@object      # @.str.765
.L.str.765:
	.asciz	"NegativeThinSpace"
	.size	.L.str.765, 18

	.type	.L.str.766,@object      # @.str.766
.L.str.766:
	.asciz	"downdownarrows"
	.size	.L.str.766, 15

	.type	.L.str.767,@object      # @.str.767
.L.str.767:
	.asciz	"xmap"
	.size	.L.str.767, 5

	.type	.L.str.768,@object      # @.str.768
.L.str.768:
	.asciz	"ltri"
	.size	.L.str.768, 5

	.type	.L.str.769,@object      # @.str.769
.L.str.769:
	.asciz	"bdquo"
	.size	.L.str.769, 6

	.type	.L.str.770,@object      # @.str.770
.L.str.770:
	.asciz	"notinvb"
	.size	.L.str.770, 8

	.type	.L.str.771,@object      # @.str.771
.L.str.771:
	.asciz	"rtri"
	.size	.L.str.771, 5

	.type	.L.str.772,@object      # @.str.772
.L.str.772:
	.asciz	"hoarr"
	.size	.L.str.772, 6

	.type	.L.str.773,@object      # @.str.773
.L.str.773:
	.asciz	"plusdo"
	.size	.L.str.773, 7

	.type	.L.str.774,@object      # @.str.774
.L.str.774:
	.asciz	"ldquo"
	.size	.L.str.774, 6

	.type	.L.str.775,@object      # @.str.775
.L.str.775:
	.asciz	"bigoplus"
	.size	.L.str.775, 9

	.type	.L.str.776,@object      # @.str.776
.L.str.776:
	.asciz	"loarr"
	.size	.L.str.776, 6

	.type	.L.str.777,@object      # @.str.777
.L.str.777:
	.asciz	"natur"
	.size	.L.str.777, 6

	.type	.L.str.778,@object      # @.str.778
.L.str.778:
	.asciz	"utri"
	.size	.L.str.778, 5

	.type	.L.str.779,@object      # @.str.779
.L.str.779:
	.asciz	"rdquo"
	.size	.L.str.779, 6

	.type	.L.str.780,@object      # @.str.780
.L.str.780:
	.asciz	"roarr"
	.size	.L.str.780, 6

	.type	.L.str.781,@object      # @.str.781
.L.str.781:
	.asciz	"VerticalBar"
	.size	.L.str.781, 12

	.type	.L.str.782,@object      # @.str.782
.L.str.782:
	.asciz	"NotPrecedesSlantEqual"
	.size	.L.str.782, 22

	.type	.L.str.783,@object      # @.str.783
.L.str.783:
	.asciz	"imped"
	.size	.L.str.783, 6

	.type	.L.str.784,@object      # @.str.784
.L.str.784:
	.asciz	"thinsp"
	.size	.L.str.784, 7

	.type	.L.str.785,@object      # @.str.785
.L.str.785:
	.asciz	"dArr"
	.size	.L.str.785, 5

	.type	.L.str.786,@object      # @.str.786
.L.str.786:
	.asciz	"dlcorn"
	.size	.L.str.786, 7

	.type	.L.str.787,@object      # @.str.787
.L.str.787:
	.asciz	"LeftUpDownVector"
	.size	.L.str.787, 17

	.type	.L.str.788,@object      # @.str.788
.L.str.788:
	.asciz	"hArr"
	.size	.L.str.788, 5

	.type	.L.str.789,@object      # @.str.789
.L.str.789:
	.asciz	"lArr"
	.size	.L.str.789, 5

	.type	.L.str.790,@object      # @.str.790
.L.str.790:
	.asciz	"ltimes"
	.size	.L.str.790, 7

	.type	.L.str.791,@object      # @.str.791
.L.str.791:
	.asciz	"rightharpoondown"
	.size	.L.str.791, 17

	.type	.L.str.792,@object      # @.str.792
.L.str.792:
	.asciz	"otimes"
	.size	.L.str.792, 7

	.type	.L.str.793,@object      # @.str.793
.L.str.793:
	.asciz	"RightTee"
	.size	.L.str.793, 9

	.type	.L.str.794,@object      # @.str.794
.L.str.794:
	.asciz	"rArr"
	.size	.L.str.794, 5

	.type	.L.str.795,@object      # @.str.795
.L.str.795:
	.asciz	"rtimes"
	.size	.L.str.795, 7

	.type	.L.str.796,@object      # @.str.796
.L.str.796:
	.asciz	"varsigma"
	.size	.L.str.796, 9

	.type	.L.str.797,@object      # @.str.797
.L.str.797:
	.asciz	"uArr"
	.size	.L.str.797, 5

	.type	.L.str.798,@object      # @.str.798
.L.str.798:
	.asciz	"ulcorn"
	.size	.L.str.798, 7

	.type	.L.str.799,@object      # @.str.799
.L.str.799:
	.asciz	"vArr"
	.size	.L.str.799, 5

	.type	.L.str.800,@object      # @.str.800
.L.str.800:
	.asciz	"szlig"
	.size	.L.str.800, 6

	.type	.L.str.801,@object      # @.str.801
.L.str.801:
	.asciz	"Vdash"
	.size	.L.str.801, 6

	.type	.L.str.802,@object      # @.str.802
.L.str.802:
	.asciz	"nspar"
	.size	.L.str.802, 6

	.type	.L.str.803,@object      # @.str.803
.L.str.803:
	.asciz	"bsim"
	.size	.L.str.803, 5

	.type	.L.str.804,@object      # @.str.804
.L.str.804:
	.asciz	"LeftTriangleBar"
	.size	.L.str.804, 16

	.type	.L.str.805,@object      # @.str.805
.L.str.805:
	.asciz	"gsim"
	.size	.L.str.805, 5

	.type	.L.str.806,@object      # @.str.806
.L.str.806:
	.asciz	"lcub"
	.size	.L.str.806, 5

	.type	.L.str.807,@object      # @.str.807
.L.str.807:
	.asciz	"isinE"
	.size	.L.str.807, 6

	.type	.L.str.808,@object      # @.str.808
.L.str.808:
	.asciz	"lsim"
	.size	.L.str.808, 5

	.type	.L.str.809,@object      # @.str.809
.L.str.809:
	.asciz	"rcub"
	.size	.L.str.809, 5

	.type	.L.str.810,@object      # @.str.810
.L.str.810:
	.asciz	"boxH"
	.size	.L.str.810, 5

	.type	.L.str.811,@object      # @.str.811
.L.str.811:
	.asciz	"nsim"
	.size	.L.str.811, 5

	.type	.L.str.812,@object      # @.str.812
.L.str.812:
	.asciz	"bkarow"
	.size	.L.str.812, 7

	.type	.L.str.813,@object      # @.str.813
.L.str.813:
	.asciz	"mdash"
	.size	.L.str.813, 6

	.type	.L.str.814,@object      # @.str.814
.L.str.814:
	.asciz	"odash"
	.size	.L.str.814, 6

	.type	.L.str.815,@object      # @.str.815
.L.str.815:
	.asciz	"clubsuit"
	.size	.L.str.815, 9

	.type	.L.str.816,@object      # @.str.816
.L.str.816:
	.asciz	"bigcup"
	.size	.L.str.816, 7

	.type	.L.str.817,@object      # @.str.817
.L.str.817:
	.asciz	"nwarhk"
	.size	.L.str.817, 7

	.type	.L.str.818,@object      # @.str.818
.L.str.818:
	.asciz	"boxDL"
	.size	.L.str.818, 6

	.type	.L.str.819,@object      # @.str.819
.L.str.819:
	.asciz	"ndash"
	.size	.L.str.819, 6

	.type	.L.str.820,@object      # @.str.820
.L.str.820:
	.asciz	"vdash"
	.size	.L.str.820, 6

	.type	.L.str.821,@object      # @.str.821
.L.str.821:
	.asciz	"swarhk"
	.size	.L.str.821, 7

	.type	.L.str.822,@object      # @.str.822
.L.str.822:
	.asciz	"blacklozenge"
	.size	.L.str.822, 13

	.type	.L.str.823,@object      # @.str.823
.L.str.823:
	.asciz	"DoubleLeftRightArrow"
	.size	.L.str.823, 21

	.type	.L.str.824,@object      # @.str.824
.L.str.824:
	.asciz	"naturals"
	.size	.L.str.824, 9

	.type	.L.str.825,@object      # @.str.825
.L.str.825:
	.asciz	"incare"
	.size	.L.str.825, 7

	.type	.L.str.826,@object      # @.str.826
.L.str.826:
	.asciz	"RightUpDownVector"
	.size	.L.str.826, 18

	.type	.L.str.827,@object      # @.str.827
.L.str.827:
	.asciz	"zwnj"
	.size	.L.str.827, 5

	.type	.L.str.828,@object      # @.str.828
.L.str.828:
	.asciz	"nprec"
	.size	.L.str.828, 6

	.type	.L.str.829,@object      # @.str.829
.L.str.829:
	.asciz	"circledast"
	.size	.L.str.829, 11

	.type	.L.str.830,@object      # @.str.830
.L.str.830:
	.asciz	"heartsuit"
	.size	.L.str.830, 10

	.type	.L.str.831,@object      # @.str.831
.L.str.831:
	.asciz	"esdot"
	.size	.L.str.831, 6

	.type	.L.str.832,@object      # @.str.832
.L.str.832:
	.asciz	"bigtriangledown"
	.size	.L.str.832, 16

	.type	.L.str.833,@object      # @.str.833
.L.str.833:
	.asciz	"Uparrow"
	.size	.L.str.833, 8

	.type	.L.str.834,@object      # @.str.834
.L.str.834:
	.asciz	"gsdot"
	.size	.L.str.834, 6

	.type	.L.str.835,@object      # @.str.835
.L.str.835:
	.asciz	"Coproduct"
	.size	.L.str.835, 10

	.type	.L.str.836,@object      # @.str.836
.L.str.836:
	.asciz	"circledcirc"
	.size	.L.str.836, 12

	.type	.L.str.837,@object      # @.str.837
.L.str.837:
	.asciz	"oint"
	.size	.L.str.837, 5

	.type	.L.str.838,@object      # @.str.838
.L.str.838:
	.asciz	"qint"
	.size	.L.str.838, 5

	.type	.L.str.839,@object      # @.str.839
.L.str.839:
	.asciz	"ldsh"
	.size	.L.str.839, 5

	.type	.L.str.840,@object      # @.str.840
.L.str.840:
	.asciz	"trade"
	.size	.L.str.840, 6

	.type	.L.str.841,@object      # @.str.841
.L.str.841:
	.asciz	"lrhar2"
	.size	.L.str.841, 7

	.type	.L.str.842,@object      # @.str.842
.L.str.842:
	.asciz	"larrhk"
	.size	.L.str.842, 7

	.type	.L.str.843,@object      # @.str.843
.L.str.843:
	.asciz	"tint"
	.size	.L.str.843, 5

	.type	.L.str.844,@object      # @.str.844
.L.str.844:
	.asciz	"rdsh"
	.size	.L.str.844, 5

	.type	.L.str.845,@object      # @.str.845
.L.str.845:
	.asciz	"psgr"
	.size	.L.str.845, 5

	.type	.L.str.846,@object      # @.str.846
.L.str.846:
	.asciz	"rarrhk"
	.size	.L.str.846, 7

	.type	.L.str.847,@object      # @.str.847
.L.str.847:
	.asciz	"Psi"
	.size	.L.str.847, 4

	.type	.L.str.848,@object      # @.str.848
.L.str.848:
	.asciz	"larrsim"
	.size	.L.str.848, 8

	.type	.L.str.849,@object      # @.str.849
.L.str.849:
	.asciz	"Epsilon"
	.size	.L.str.849, 8

	.type	.L.str.850,@object      # @.str.850
.L.str.850:
	.asciz	"rarrsim"
	.size	.L.str.850, 8

	.type	.L.str.851,@object      # @.str.851
.L.str.851:
	.asciz	"male"
	.size	.L.str.851, 5

	.type	.L.str.852,@object      # @.str.852
.L.str.852:
	.asciz	"uparrow"
	.size	.L.str.852, 8

	.type	.L.str.853,@object      # @.str.853
.L.str.853:
	.asciz	"Scaron"
	.size	.L.str.853, 7

	.type	.L.str.854,@object      # @.str.854
.L.str.854:
	.asciz	"Upsilon"
	.size	.L.str.854, 8

	.type	.L.str.855,@object      # @.str.855
.L.str.855:
	.asciz	"hearts"
	.size	.L.str.855, 7

	.type	.L.str.856,@object      # @.str.856
.L.str.856:
	.asciz	"eqsim"
	.size	.L.str.856, 6

	.type	.L.str.857,@object      # @.str.857
.L.str.857:
	.asciz	"Sigma"
	.size	.L.str.857, 6

	.type	.L.str.858,@object      # @.str.858
.L.str.858:
	.asciz	"Or"
	.size	.L.str.858, 3

	.type	.L.str.859,@object      # @.str.859
.L.str.859:
	.asciz	"UpArrowDownArrow"
	.size	.L.str.859, 17

	.type	.L.str.860,@object      # @.str.860
.L.str.860:
	.asciz	"twixt"
	.size	.L.str.860, 6

	.type	.L.str.861,@object      # @.str.861
.L.str.861:
	.asciz	"psi"
	.size	.L.str.861, 4

	.type	.L.str.862,@object      # @.str.862
.L.str.862:
	.asciz	"nabla"
	.size	.L.str.862, 6

	.type	.L.str.863,@object      # @.str.863
.L.str.863:
	.asciz	"bowtie"
	.size	.L.str.863, 7

	.type	.L.str.864,@object      # @.str.864
.L.str.864:
	.asciz	"epsilon"
	.size	.L.str.864, 8

	.type	.L.str.865,@object      # @.str.865
.L.str.865:
	.asciz	"GreaterSlantEqual"
	.size	.L.str.865, 18

	.type	.L.str.866,@object      # @.str.866
.L.str.866:
	.asciz	"NotLeftTriangleEqual"
	.size	.L.str.866, 21

	.type	.L.str.867,@object      # @.str.867
.L.str.867:
	.asciz	"apacir"
	.size	.L.str.867, 7

	.type	.L.str.868,@object      # @.str.868
.L.str.868:
	.asciz	"upharpoonright"
	.size	.L.str.868, 15

	.type	.L.str.869,@object      # @.str.869
.L.str.869:
	.asciz	"scaron"
	.size	.L.str.869, 7

	.type	.L.str.870,@object      # @.str.870
.L.str.870:
	.asciz	"af"
	.size	.L.str.870, 3

	.type	.L.str.871,@object      # @.str.871
.L.str.871:
	.asciz	"upsilon"
	.size	.L.str.871, 8

	.type	.L.str.872,@object      # @.str.872
.L.str.872:
	.asciz	"TildeEqual"
	.size	.L.str.872, 11

	.type	.L.str.873,@object      # @.str.873
.L.str.873:
	.asciz	"Lambda"
	.size	.L.str.873, 7

	.type	.L.str.874,@object      # @.str.874
.L.str.874:
	.asciz	"neArr"
	.size	.L.str.874, 6

	.type	.L.str.875,@object      # @.str.875
.L.str.875:
	.asciz	"sigma"
	.size	.L.str.875, 6

	.type	.L.str.876,@object      # @.str.876
.L.str.876:
	.asciz	"ETH"
	.size	.L.str.876, 4

	.type	.L.str.877,@object      # @.str.877
.L.str.877:
	.asciz	"leftthreetimes"
	.size	.L.str.877, 15

	.type	.L.str.878,@object      # @.str.878
.L.str.878:
	.asciz	"seArr"
	.size	.L.str.878, 6

	.type	.L.str.879,@object      # @.str.879
.L.str.879:
	.asciz	"thicksim"
	.size	.L.str.879, 9

	.type	.L.str.880,@object      # @.str.880
.L.str.880:
	.asciz	"bigcap"
	.size	.L.str.880, 7

	.type	.L.str.881,@object      # @.str.881
.L.str.881:
	.asciz	"ang"
	.size	.L.str.881, 4

	.type	.L.str.882,@object      # @.str.882
.L.str.882:
	.asciz	"ldca"
	.size	.L.str.882, 5

	.type	.L.str.883,@object      # @.str.883
.L.str.883:
	.asciz	"rdca"
	.size	.L.str.883, 5

	.type	.L.str.884,@object      # @.str.884
.L.str.884:
	.asciz	"wr"
	.size	.L.str.884, 3

	.type	.L.str.885,@object      # @.str.885
.L.str.885:
	.asciz	"or"
	.size	.L.str.885, 3

	.type	.L.str.886,@object      # @.str.886
.L.str.886:
	.asciz	"pr"
	.size	.L.str.886, 3

	.type	.L.str.887,@object      # @.str.887
.L.str.887:
	.asciz	"triangleq"
	.size	.L.str.887, 10

	.type	.L.str.888,@object      # @.str.888
.L.str.888:
	.asciz	"gtrsim"
	.size	.L.str.888, 7

	.type	.L.str.889,@object      # @.str.889
.L.str.889:
	.asciz	"looparrowleft"
	.size	.L.str.889, 14

	.type	.L.str.890,@object      # @.str.890
.L.str.890:
	.asciz	"LongRightArrow"
	.size	.L.str.890, 15

	.type	.L.str.891,@object      # @.str.891
.L.str.891:
	.asciz	"nis"
	.size	.L.str.891, 4

	.type	.L.str.892,@object      # @.str.892
.L.str.892:
	.asciz	"Aring"
	.size	.L.str.892, 6

	.type	.L.str.893,@object      # @.str.893
.L.str.893:
	.asciz	"LeftDownVectorBar"
	.size	.L.str.893, 18

	.type	.L.str.894,@object      # @.str.894
.L.str.894:
	.asciz	"NotTildeEqual"
	.size	.L.str.894, 14

	.type	.L.str.895,@object      # @.str.895
.L.str.895:
	.asciz	"lambda"
	.size	.L.str.895, 7

	.type	.L.str.896,@object      # @.str.896
.L.str.896:
	.asciz	"larrlp"
	.size	.L.str.896, 7

	.type	.L.str.897,@object      # @.str.897
.L.str.897:
	.asciz	"leftharpoondown"
	.size	.L.str.897, 16

	.type	.L.str.898,@object      # @.str.898
.L.str.898:
	.asciz	"sup2"
	.size	.L.str.898, 5

	.type	.L.str.899,@object      # @.str.899
.L.str.899:
	.asciz	"rarrlp"
	.size	.L.str.899, 7

	.type	.L.str.900,@object      # @.str.900
.L.str.900:
	.asciz	"SmallCircle"
	.size	.L.str.900, 12

	.type	.L.str.901,@object      # @.str.901
.L.str.901:
	.asciz	"bNot"
	.size	.L.str.901, 5

	.type	.L.str.902,@object      # @.str.902
.L.str.902:
	.asciz	"aleph"
	.size	.L.str.902, 6

	.type	.L.str.903,@object      # @.str.903
.L.str.903:
	.asciz	"blacktriangle"
	.size	.L.str.903, 14

	.type	.L.str.904,@object      # @.str.904
.L.str.904:
	.asciz	"circleddash"
	.size	.L.str.904, 12

	.type	.L.str.905,@object      # @.str.905
.L.str.905:
	.asciz	"rhov"
	.size	.L.str.905, 5

	.type	.L.str.906,@object      # @.str.906
.L.str.906:
	.asciz	"UpTee"
	.size	.L.str.906, 6

	.type	.L.str.907,@object      # @.str.907
.L.str.907:
	.asciz	"die"
	.size	.L.str.907, 4

	.type	.L.str.908,@object      # @.str.908
.L.str.908:
	.asciz	"bump"
	.size	.L.str.908, 5

	.type	.L.str.909,@object      # @.str.909
.L.str.909:
	.asciz	"epar"
	.size	.L.str.909, 5

	.type	.L.str.910,@object      # @.str.910
.L.str.910:
	.asciz	"gnap"
	.size	.L.str.910, 5

	.type	.L.str.911,@object      # @.str.911
.L.str.911:
	.asciz	"RightFloor"
	.size	.L.str.911, 11

	.type	.L.str.912,@object      # @.str.912
.L.str.912:
	.asciz	"aring"
	.size	.L.str.912, 6

	.type	.L.str.913,@object      # @.str.913
.L.str.913:
	.asciz	"lnap"
	.size	.L.str.913, 5

	.type	.L.str.914,@object      # @.str.914
.L.str.914:
	.asciz	"approx"
	.size	.L.str.914, 7

	.type	.L.str.915,@object      # @.str.915
.L.str.915:
	.asciz	"larrbfs"
	.size	.L.str.915, 8

	.type	.L.str.916,@object      # @.str.916
.L.str.916:
	.asciz	"lpar"
	.size	.L.str.916, 5

	.type	.L.str.917,@object      # @.str.917
.L.str.917:
	.asciz	"npar"
	.size	.L.str.917, 5

	.type	.L.str.918,@object      # @.str.918
.L.str.918:
	.asciz	"gEl"
	.size	.L.str.918, 4

	.type	.L.str.919,@object      # @.str.919
.L.str.919:
	.asciz	"boxVl"
	.size	.L.str.919, 6

	.type	.L.str.920,@object      # @.str.920
.L.str.920:
	.asciz	"eqcolon"
	.size	.L.str.920, 8

	.type	.L.str.921,@object      # @.str.921
.L.str.921:
	.asciz	"rarrbfs"
	.size	.L.str.921, 8

	.type	.L.str.922,@object      # @.str.922
.L.str.922:
	.asciz	"rpar"
	.size	.L.str.922, 5

	.type	.L.str.923,@object      # @.str.923
.L.str.923:
	.asciz	"spar"
	.size	.L.str.923, 5

	.type	.L.str.924,@object      # @.str.924
.L.str.924:
	.asciz	"thksim"
	.size	.L.str.924, 7

	.type	.L.str.925,@object      # @.str.925
.L.str.925:
	.asciz	"RightDownVectorBar"
	.size	.L.str.925, 19

	.type	.L.str.926,@object      # @.str.926
.L.str.926:
	.asciz	"para"
	.size	.L.str.926, 5

	.type	.L.str.927,@object      # @.str.927
.L.str.927:
	.asciz	"thkap"
	.size	.L.str.927, 6

	.type	.L.str.928,@object      # @.str.928
.L.str.928:
	.asciz	"Superset"
	.size	.L.str.928, 9

	.type	.L.str.929,@object      # @.str.929
.L.str.929:
	.asciz	"ddarr"
	.size	.L.str.929, 6

	.type	.L.str.930,@object      # @.str.930
.L.str.930:
	.asciz	"Gamma"
	.size	.L.str.930, 6

	.type	.L.str.931,@object      # @.str.931
.L.str.931:
	.asciz	"frac15"
	.size	.L.str.931, 7

	.type	.L.str.932,@object      # @.str.932
.L.str.932:
	.asciz	"kappav"
	.size	.L.str.932, 7

	.type	.L.str.933,@object      # @.str.933
.L.str.933:
	.asciz	"udarr"
	.size	.L.str.933, 6

	.type	.L.str.934,@object      # @.str.934
.L.str.934:
	.asciz	"Therefore"
	.size	.L.str.934, 10

	.type	.L.str.935,@object      # @.str.935
.L.str.935:
	.asciz	"hairsp"
	.size	.L.str.935, 7

	.type	.L.str.936,@object      # @.str.936
.L.str.936:
	.asciz	"CircleMinus"
	.size	.L.str.936, 12

	.type	.L.str.937,@object      # @.str.937
.L.str.937:
	.asciz	"ordf"
	.size	.L.str.937, 5

	.type	.L.str.938,@object      # @.str.938
.L.str.938:
	.asciz	"VerticalTilde"
	.size	.L.str.938, 14

	.type	.L.str.939,@object      # @.str.939
.L.str.939:
	.asciz	"LeftVectorBar"
	.size	.L.str.939, 14

	.type	.L.str.940,@object      # @.str.940
.L.str.940:
	.asciz	"UnderBracket"
	.size	.L.str.940, 13

	.type	.L.str.941,@object      # @.str.941
.L.str.941:
	.asciz	"gamma"
	.size	.L.str.941, 6

	.type	.L.str.942,@object      # @.str.942
.L.str.942:
	.asciz	"DoubleDot"
	.size	.L.str.942, 10

	.type	.L.str.943,@object      # @.str.943
.L.str.943:
	.asciz	"lceil"
	.size	.L.str.943, 6

	.type	.L.str.944,@object      # @.str.944
.L.str.944:
	.asciz	"curlyeqprec"
	.size	.L.str.944, 12

	.type	.L.str.945,@object      # @.str.945
.L.str.945:
	.asciz	"varnothing"
	.size	.L.str.945, 11

	.type	.L.str.946,@object      # @.str.946
.L.str.946:
	.asciz	"nhpar"
	.size	.L.str.946, 6

	.type	.L.str.947,@object      # @.str.947
.L.str.947:
	.asciz	"therefore"
	.size	.L.str.947, 10

	.type	.L.str.948,@object      # @.str.948
.L.str.948:
	.asciz	"dlcrop"
	.size	.L.str.948, 7

	.type	.L.str.949,@object      # @.str.949
.L.str.949:
	.asciz	"rceil"
	.size	.L.str.949, 6

	.type	.L.str.950,@object      # @.str.950
.L.str.950:
	.asciz	"leftleftarrows"
	.size	.L.str.950, 15

	.type	.L.str.951,@object      # @.str.951
.L.str.951:
	.asciz	"vangrt"
	.size	.L.str.951, 7

	.type	.L.str.952,@object      # @.str.952
.L.str.952:
	.asciz	"lrm"
	.size	.L.str.952, 4

	.type	.L.str.953,@object      # @.str.953
.L.str.953:
	.asciz	"curvearrowright"
	.size	.L.str.953, 16

	.type	.L.str.954,@object      # @.str.954
.L.str.954:
	.asciz	"thetas"
	.size	.L.str.954, 7

	.type	.L.str.955,@object      # @.str.955
.L.str.955:
	.asciz	"PartialD"
	.size	.L.str.955, 9

	.type	.L.str.956,@object      # @.str.956
.L.str.956:
	.asciz	"nVDash"
	.size	.L.str.956, 7

	.type	.L.str.957,@object      # @.str.957
.L.str.957:
	.asciz	"wedgeq"
	.size	.L.str.957, 7

	.type	.L.str.958,@object      # @.str.958
.L.str.958:
	.asciz	"ulcrop"
	.size	.L.str.958, 7

	.type	.L.str.959,@object      # @.str.959
.L.str.959:
	.asciz	"zigrarr"
	.size	.L.str.959, 8

	.type	.L.str.960,@object      # @.str.960
.L.str.960:
	.asciz	"lozf"
	.size	.L.str.960, 5

	.type	.L.str.961,@object      # @.str.961
.L.str.961:
	.asciz	"sung"
	.size	.L.str.961, 5

	.type	.L.str.962,@object      # @.str.962
.L.str.962:
	.asciz	"iff"
	.size	.L.str.962, 4

	.type	.L.str.963,@object      # @.str.963
.L.str.963:
	.asciz	"xnis"
	.size	.L.str.963, 5

	.type	.L.str.964,@object      # @.str.964
.L.str.964:
	.asciz	"ring"
	.size	.L.str.964, 5

	.type	.L.str.965,@object      # @.str.965
.L.str.965:
	.asciz	"dfisht"
	.size	.L.str.965, 7

	.type	.L.str.966,@object      # @.str.966
.L.str.966:
	.asciz	"nltrie"
	.size	.L.str.966, 7

	.type	.L.str.967,@object      # @.str.967
.L.str.967:
	.asciz	"llcorner"
	.size	.L.str.967, 9

	.type	.L.str.968,@object      # @.str.968
.L.str.968:
	.asciz	"lfisht"
	.size	.L.str.968, 7

	.type	.L.str.969,@object      # @.str.969
.L.str.969:
	.asciz	"ctdot"
	.size	.L.str.969, 6

	.type	.L.str.970,@object      # @.str.970
.L.str.970:
	.asciz	"dtdot"
	.size	.L.str.970, 6

	.type	.L.str.971,@object      # @.str.971
.L.str.971:
	.asciz	"CloseCurlyQuote"
	.size	.L.str.971, 16

	.type	.L.str.972,@object      # @.str.972
.L.str.972:
	.asciz	"par"
	.size	.L.str.972, 4

	.type	.L.str.973,@object      # @.str.973
.L.str.973:
	.asciz	"rfisht"
	.size	.L.str.973, 7

	.type	.L.str.974,@object      # @.str.974
.L.str.974:
	.asciz	"ulcorner"
	.size	.L.str.974, 9

	.type	.L.str.975,@object      # @.str.975
.L.str.975:
	.asciz	"ufisht"
	.size	.L.str.975, 7

	.type	.L.str.976,@object      # @.str.976
.L.str.976:
	.asciz	"notinvc"
	.size	.L.str.976, 8

	.type	.L.str.977,@object      # @.str.977
.L.str.977:
	.asciz	"orderof"
	.size	.L.str.977, 8

	.type	.L.str.978,@object      # @.str.978
.L.str.978:
	.asciz	"Rrightarrow"
	.size	.L.str.978, 12

	.type	.L.str.979,@object      # @.str.979
.L.str.979:
	.asciz	"emsp"
	.size	.L.str.979, 5

	.type	.L.str.980,@object      # @.str.980
.L.str.980:
	.asciz	"Chi"
	.size	.L.str.980, 4

	.type	.L.str.981,@object      # @.str.981
.L.str.981:
	.asciz	"utdot"
	.size	.L.str.981, 6

	.type	.L.str.982,@object      # @.str.982
.L.str.982:
	.asciz	"bull"
	.size	.L.str.982, 5

	.type	.L.str.983,@object      # @.str.983
.L.str.983:
	.asciz	"khgr"
	.size	.L.str.983, 5

	.type	.L.str.984,@object      # @.str.984
.L.str.984:
	.asciz	"ominus"
	.size	.L.str.984, 7

	.type	.L.str.985,@object      # @.str.985
.L.str.985:
	.asciz	"eqslantgtr"
	.size	.L.str.985, 11

	.type	.L.str.986,@object      # @.str.986
.L.str.986:
	.asciz	"multimap"
	.size	.L.str.986, 9

	.type	.L.str.987,@object      # @.str.987
.L.str.987:
	.asciz	"lowast"
	.size	.L.str.987, 7

	.type	.L.str.988,@object      # @.str.988
.L.str.988:
	.asciz	"ohgr"
	.size	.L.str.988, 5

	.type	.L.str.989,@object      # @.str.989
.L.str.989:
	.asciz	"NotLess"
	.size	.L.str.989, 8

	.type	.L.str.990,@object      # @.str.990
.L.str.990:
	.asciz	"alefsym"
	.size	.L.str.990, 8

	.type	.L.str.991,@object      # @.str.991
.L.str.991:
	.asciz	"Phi"
	.size	.L.str.991, 4

	.type	.L.str.992,@object      # @.str.992
.L.str.992:
	.asciz	"thgr"
	.size	.L.str.992, 5

	.type	.L.str.993,@object      # @.str.993
.L.str.993:
	.asciz	"isinsv"
	.size	.L.str.993, 7

	.type	.L.str.994,@object      # @.str.994
.L.str.994:
	.asciz	"OHacgr"
	.size	.L.str.994, 7

	.type	.L.str.995,@object      # @.str.995
.L.str.995:
	.asciz	"NotElement"
	.size	.L.str.995, 11

	.type	.L.str.996,@object      # @.str.996
.L.str.996:
	.asciz	"nearrow"
	.size	.L.str.996, 8

	.type	.L.str.997,@object      # @.str.997
.L.str.997:
	.asciz	"DoubleLeftTee"
	.size	.L.str.997, 14

	.type	.L.str.998,@object      # @.str.998
.L.str.998:
	.asciz	"nrightarrow"
	.size	.L.str.998, 12

	.type	.L.str.999,@object      # @.str.999
.L.str.999:
	.asciz	"searrow"
	.size	.L.str.999, 8

	.type	.L.str.1000,@object     # @.str.1000
.L.str.1000:
	.asciz	"UpTeeArrow"
	.size	.L.str.1000, 11

	.type	.L.str.1001,@object     # @.str.1001
.L.str.1001:
	.asciz	"dbkarow"
	.size	.L.str.1001, 8

	.type	.L.str.1002,@object     # @.str.1002
.L.str.1002:
	.asciz	"period"
	.size	.L.str.1002, 7

	.type	.L.str.1003,@object     # @.str.1003
.L.str.1003:
	.asciz	"Gg"
	.size	.L.str.1003, 3

	.type	.L.str.1004,@object     # @.str.1004
.L.str.1004:
	.asciz	"circledR"
	.size	.L.str.1004, 9

	.type	.L.str.1005,@object     # @.str.1005
.L.str.1005:
	.asciz	"DownBreve"
	.size	.L.str.1005, 10

	.type	.L.str.1006,@object     # @.str.1006
.L.str.1006:
	.asciz	"imagline"
	.size	.L.str.1006, 9

	.type	.L.str.1007,@object     # @.str.1007
.L.str.1007:
	.asciz	"gneq"
	.size	.L.str.1007, 5

	.type	.L.str.1008,@object     # @.str.1008
.L.str.1008:
	.asciz	"chi"
	.size	.L.str.1008, 4

	.type	.L.str.1009,@object     # @.str.1009
.L.str.1009:
	.asciz	"lneq"
	.size	.L.str.1009, 5

	.type	.L.str.1010,@object     # @.str.1010
.L.str.1010:
	.asciz	"phi"
	.size	.L.str.1010, 4

	.type	.L.str.1011,@object     # @.str.1011
.L.str.1011:
	.asciz	"spadesuit"
	.size	.L.str.1011, 10

	.type	.L.str.1012,@object     # @.str.1012
.L.str.1012:
	.asciz	"prsim"
	.size	.L.str.1012, 6

	.type	.L.str.1013,@object     # @.str.1013
.L.str.1013:
	.asciz	"Atilde"
	.size	.L.str.1013, 7

	.type	.L.str.1014,@object     # @.str.1014
.L.str.1014:
	.asciz	"isinv"
	.size	.L.str.1014, 6

	.type	.L.str.1015,@object     # @.str.1015
.L.str.1015:
	.asciz	"rlarr2"
	.size	.L.str.1015, 7

	.type	.L.str.1016,@object     # @.str.1016
.L.str.1016:
	.asciz	"nrArr"
	.size	.L.str.1016, 6

	.type	.L.str.1017,@object     # @.str.1017
.L.str.1017:
	.asciz	"angst"
	.size	.L.str.1017, 6

	.type	.L.str.1018,@object     # @.str.1018
.L.str.1018:
	.asciz	"sdotb"
	.size	.L.str.1018, 6

	.type	.L.str.1019,@object     # @.str.1019
.L.str.1019:
	.asciz	"Ntilde"
	.size	.L.str.1019, 7

	.type	.L.str.1020,@object     # @.str.1020
.L.str.1020:
	.asciz	"gg"
	.size	.L.str.1020, 3

	.type	.L.str.1021,@object     # @.str.1021
.L.str.1021:
	.asciz	"xrArr"
	.size	.L.str.1021, 6

	.type	.L.str.1022,@object     # @.str.1022
.L.str.1022:
	.asciz	"DoubleLongLeftRightArrow"
	.size	.L.str.1022, 25

	.type	.L.str.1023,@object     # @.str.1023
.L.str.1023:
	.asciz	"Otilde"
	.size	.L.str.1023, 7

	.type	.L.str.1024,@object     # @.str.1024
.L.str.1024:
	.asciz	"lg"
	.size	.L.str.1024, 3

	.type	.L.str.1025,@object     # @.str.1025
.L.str.1025:
	.asciz	"LeftArrowBar"
	.size	.L.str.1025, 13

	.type	.L.str.1026,@object     # @.str.1026
.L.str.1026:
	.asciz	"thickapprox"
	.size	.L.str.1026, 12

	.type	.L.str.1027,@object     # @.str.1027
.L.str.1027:
	.asciz	"precnsim"
	.size	.L.str.1027, 9

	.type	.L.str.1028,@object     # @.str.1028
.L.str.1028:
	.asciz	"Element"
	.size	.L.str.1028, 8

	.type	.L.str.1029,@object     # @.str.1029
.L.str.1029:
	.asciz	"atilde"
	.size	.L.str.1029, 7

	.type	.L.str.1030,@object     # @.str.1030
.L.str.1030:
	.asciz	"LessEqualGreater"
	.size	.L.str.1030, 17

	.type	.L.str.1031,@object     # @.str.1031
.L.str.1031:
	.asciz	"eqslantless"
	.size	.L.str.1031, 12

	.type	.L.str.1032,@object     # @.str.1032
.L.str.1032:
	.asciz	"EmptyVerySmallSquare"
	.size	.L.str.1032, 21

	.type	.L.str.1033,@object     # @.str.1033
.L.str.1033:
	.asciz	"phgr"
	.size	.L.str.1033, 5

	.type	.L.str.1034,@object     # @.str.1034
.L.str.1034:
	.asciz	"epsiv"
	.size	.L.str.1034, 6

	.type	.L.str.1035,@object     # @.str.1035
.L.str.1035:
	.asciz	"DoubleVerticalBar"
	.size	.L.str.1035, 18

	.type	.L.str.1036,@object     # @.str.1036
.L.str.1036:
	.asciz	"LeftTriangle"
	.size	.L.str.1036, 13

	.type	.L.str.1037,@object     # @.str.1037
.L.str.1037:
	.asciz	"spades"
	.size	.L.str.1037, 7

	.type	.L.str.1038,@object     # @.str.1038
.L.str.1038:
	.asciz	"frac23"
	.size	.L.str.1038, 7

	.type	.L.str.1039,@object     # @.str.1039
.L.str.1039:
	.asciz	"ntilde"
	.size	.L.str.1039, 7

	.type	.L.str.1040,@object     # @.str.1040
.L.str.1040:
	.asciz	"otilde"
	.size	.L.str.1040, 7

	.type	.L.str.1041,@object     # @.str.1041
.L.str.1041:
	.asciz	"sup1"
	.size	.L.str.1041, 5

	.type	.L.str.1042,@object     # @.str.1042
.L.str.1042:
	.asciz	"GreaterGreater"
	.size	.L.str.1042, 15

	.type	.L.str.1043,@object     # @.str.1043
.L.str.1043:
	.asciz	"iecy"
	.size	.L.str.1043, 5

	.type	.L.str.1044,@object     # @.str.1044
.L.str.1044:
	.asciz	"NestedGreaterGreater"
	.size	.L.str.1044, 21

	.type	.L.str.1045,@object     # @.str.1045
.L.str.1045:
	.asciz	"lfloor"
	.size	.L.str.1045, 7

	.type	.L.str.1046,@object     # @.str.1046
.L.str.1046:
	.asciz	"SquareSubsetEqual"
	.size	.L.str.1046, 18

	.type	.L.str.1047,@object     # @.str.1047
.L.str.1047:
	.asciz	"acute"
	.size	.L.str.1047, 6

	.type	.L.str.1048,@object     # @.str.1048
.L.str.1048:
	.asciz	"YAcy"
	.size	.L.str.1048, 5

	.type	.L.str.1049,@object     # @.str.1049
.L.str.1049:
	.asciz	"rfloor"
	.size	.L.str.1049, 7

	.type	.L.str.1050,@object     # @.str.1050
.L.str.1050:
	.asciz	"quest"
	.size	.L.str.1050, 6

	.type	.L.str.1051,@object     # @.str.1051
.L.str.1051:
	.asciz	"rlhar"
	.size	.L.str.1051, 6

	.type	.L.str.1052,@object     # @.str.1052
.L.str.1052:
	.asciz	"geq"
	.size	.L.str.1052, 4

	.type	.L.str.1053,@object     # @.str.1053
.L.str.1053:
	.asciz	"CloseCurlyDoubleQuote"
	.size	.L.str.1053, 22

	.type	.L.str.1054,@object     # @.str.1054
.L.str.1054:
	.asciz	"lsquor"
	.size	.L.str.1054, 7

	.type	.L.str.1055,@object     # @.str.1055
.L.str.1055:
	.asciz	"tshcy"
	.size	.L.str.1055, 6

	.type	.L.str.1056,@object     # @.str.1056
.L.str.1056:
	.asciz	"leq"
	.size	.L.str.1056, 4

	.type	.L.str.1057,@object     # @.str.1057
.L.str.1057:
	.asciz	"boxUl"
	.size	.L.str.1057, 6

	.type	.L.str.1058,@object     # @.str.1058
.L.str.1058:
	.asciz	"lurdshar"
	.size	.L.str.1058, 9

	.type	.L.str.1059,@object     # @.str.1059
.L.str.1059:
	.asciz	"rsquor"
	.size	.L.str.1059, 7

	.type	.L.str.1060,@object     # @.str.1060
.L.str.1060:
	.asciz	"VerticalSeparator"
	.size	.L.str.1060, 18

	.type	.L.str.1061,@object     # @.str.1061
.L.str.1061:
	.asciz	"boxdR"
	.size	.L.str.1061, 6

	.type	.L.str.1062,@object     # @.str.1062
.L.str.1062:
	.asciz	"scap"
	.size	.L.str.1062, 5

	.type	.L.str.1063,@object     # @.str.1063
.L.str.1063:
	.asciz	"hkswarow"
	.size	.L.str.1063, 9

	.type	.L.str.1064,@object     # @.str.1064
.L.str.1064:
	.asciz	"CapitalDifferentialD"
	.size	.L.str.1064, 21

	.type	.L.str.1065,@object     # @.str.1065
.L.str.1065:
	.asciz	"NoBreak"
	.size	.L.str.1065, 8

	.type	.L.str.1066,@object     # @.str.1066
.L.str.1066:
	.asciz	"lthree"
	.size	.L.str.1066, 7

	.type	.L.str.1067,@object     # @.str.1067
.L.str.1067:
	.asciz	"bigtriangleup"
	.size	.L.str.1067, 14

	.type	.L.str.1068,@object     # @.str.1068
.L.str.1068:
	.asciz	"elinters"
	.size	.L.str.1068, 9

	.type	.L.str.1069,@object     # @.str.1069
.L.str.1069:
	.asciz	"forall"
	.size	.L.str.1069, 7

	.type	.L.str.1070,@object     # @.str.1070
.L.str.1070:
	.asciz	"NotGreater"
	.size	.L.str.1070, 11

	.type	.L.str.1071,@object     # @.str.1071
.L.str.1071:
	.asciz	"rthree"
	.size	.L.str.1071, 7

	.type	.L.str.1072,@object     # @.str.1072
.L.str.1072:
	.asciz	"Jukcy"
	.size	.L.str.1072, 6

	.type	.L.str.1073,@object     # @.str.1073
.L.str.1073:
	.asciz	"Iukcy"
	.size	.L.str.1073, 6

	.type	.L.str.1074,@object     # @.str.1074
.L.str.1074:
	.asciz	"gtreqqless"
	.size	.L.str.1074, 11

	.type	.L.str.1075,@object     # @.str.1075
.L.str.1075:
	.asciz	"UnderParenthesis"
	.size	.L.str.1075, 17

	.type	.L.str.1076,@object     # @.str.1076
.L.str.1076:
	.asciz	"nearr"
	.size	.L.str.1076, 6

	.type	.L.str.1077,@object     # @.str.1077
.L.str.1077:
	.asciz	"perp"
	.size	.L.str.1077, 5

	.type	.L.str.1078,@object     # @.str.1078
.L.str.1078:
	.asciz	"bepsi"
	.size	.L.str.1078, 6

	.type	.L.str.1079,@object     # @.str.1079
.L.str.1079:
	.asciz	"searr"
	.size	.L.str.1079, 6

	.type	.L.str.1080,@object     # @.str.1080
.L.str.1080:
	.asciz	"iexcl"
	.size	.L.str.1080, 6

	.type	.L.str.1081,@object     # @.str.1081
.L.str.1081:
	.asciz	"LeftArrow"
	.size	.L.str.1081, 10

	.type	.L.str.1082,@object     # @.str.1082
.L.str.1082:
	.asciz	"NegativeMediumSpace"
	.size	.L.str.1082, 20

	.type	.L.str.1083,@object     # @.str.1083
.L.str.1083:
	.asciz	"ast"
	.size	.L.str.1083, 4

	.type	.L.str.1084,@object     # @.str.1084
.L.str.1084:
	.asciz	"lAarr"
	.size	.L.str.1084, 6

	.type	.L.str.1085,@object     # @.str.1085
.L.str.1085:
	.asciz	"DownRightVectorBar"
	.size	.L.str.1085, 19

	.type	.L.str.1086,@object     # @.str.1086
.L.str.1086:
	.asciz	"cuvee"
	.size	.L.str.1086, 6

	.type	.L.str.1087,@object     # @.str.1087
.L.str.1087:
	.asciz	"iukcy"
	.size	.L.str.1087, 6

	.type	.L.str.1088,@object     # @.str.1088
.L.str.1088:
	.asciz	"jukcy"
	.size	.L.str.1088, 6

	.type	.L.str.1089,@object     # @.str.1089
.L.str.1089:
	.asciz	"latail"
	.size	.L.str.1089, 7

	.type	.L.str.1090,@object     # @.str.1090
.L.str.1090:
	.asciz	"disin"
	.size	.L.str.1090, 6

	.type	.L.str.1091,@object     # @.str.1091
.L.str.1091:
	.asciz	"rAarr"
	.size	.L.str.1091, 6

	.type	.L.str.1092,@object     # @.str.1092
.L.str.1092:
	.asciz	"angsph"
	.size	.L.str.1092, 7

	.type	.L.str.1093,@object     # @.str.1093
.L.str.1093:
	.asciz	"InvisibleComma"
	.size	.L.str.1093, 15

	.type	.L.str.1094,@object     # @.str.1094
.L.str.1094:
	.asciz	"ratail"
	.size	.L.str.1094, 7

	.type	.L.str.1095,@object     # @.str.1095
.L.str.1095:
	.asciz	"supnE"
	.size	.L.str.1095, 6

	.type	.L.str.1096,@object     # @.str.1096
.L.str.1096:
	.asciz	"ngt"
	.size	.L.str.1096, 4

	.type	.L.str.1097,@object     # @.str.1097
.L.str.1097:
	.asciz	"order"
	.size	.L.str.1097, 6

	.type	.L.str.1098,@object     # @.str.1098
.L.str.1098:
	.asciz	"divide"
	.size	.L.str.1098, 7

	.type	.L.str.1099,@object     # @.str.1099
.L.str.1099:
	.asciz	"colon"
	.size	.L.str.1099, 6

	.type	.L.str.1100,@object     # @.str.1100
.L.str.1100:
	.asciz	"CirclePlus"
	.size	.L.str.1100, 11

	.type	.L.str.1101,@object     # @.str.1101
.L.str.1101:
	.asciz	"frac18"
	.size	.L.str.1101, 7

	.type	.L.str.1102,@object     # @.str.1102
.L.str.1102:
	.asciz	"nsup"
	.size	.L.str.1102, 5

	.type	.L.str.1103,@object     # @.str.1103
.L.str.1103:
	.asciz	"intprod"
	.size	.L.str.1103, 8

	.type	.L.str.1104,@object     # @.str.1104
.L.str.1104:
	.asciz	"And"
	.size	.L.str.1104, 4

	.type	.L.str.1105,@object     # @.str.1105
.L.str.1105:
	.asciz	"Cup"
	.size	.L.str.1105, 4

	.type	.L.str.1106,@object     # @.str.1106
.L.str.1106:
	.asciz	"nsimeq"
	.size	.L.str.1106, 7

	.type	.L.str.1107,@object     # @.str.1107
.L.str.1107:
	.asciz	"subnE"
	.size	.L.str.1107, 6

	.type	.L.str.1108,@object     # @.str.1108
.L.str.1108:
	.asciz	"SHCHcy"
	.size	.L.str.1108, 7

	.type	.L.str.1109,@object     # @.str.1109
.L.str.1109:
	.asciz	"LessLess"
	.size	.L.str.1109, 9

	.type	.L.str.1110,@object     # @.str.1110
.L.str.1110:
	.asciz	"rightarrowtail"
	.size	.L.str.1110, 15

	.type	.L.str.1111,@object     # @.str.1111
.L.str.1111:
	.asciz	"EEgr"
	.size	.L.str.1111, 5

	.type	.L.str.1112,@object     # @.str.1112
.L.str.1112:
	.asciz	"Sup"
	.size	.L.str.1112, 4

	.type	.L.str.1113,@object     # @.str.1113
.L.str.1113:
	.asciz	"simeq"
	.size	.L.str.1113, 6

	.type	.L.str.1114,@object     # @.str.1114
.L.str.1114:
	.asciz	"leftharpoonup"
	.size	.L.str.1114, 14

	.type	.L.str.1115,@object     # @.str.1115
.L.str.1115:
	.asciz	"boxhd"
	.size	.L.str.1115, 6

	.type	.L.str.1116,@object     # @.str.1116
.L.str.1116:
	.asciz	"and"
	.size	.L.str.1116, 4

	.type	.L.str.1117,@object     # @.str.1117
.L.str.1117:
	.asciz	"Cedilla"
	.size	.L.str.1117, 8

	.type	.L.str.1118,@object     # @.str.1118
.L.str.1118:
	.asciz	"cup"
	.size	.L.str.1118, 4

	.type	.L.str.1119,@object     # @.str.1119
.L.str.1119:
	.asciz	"ensp"
	.size	.L.str.1119, 5

	.type	.L.str.1120,@object     # @.str.1120
.L.str.1120:
	.asciz	"gimel"
	.size	.L.str.1120, 6

	.type	.L.str.1121,@object     # @.str.1121
.L.str.1121:
	.asciz	"straightphi"
	.size	.L.str.1121, 12

	.type	.L.str.1122,@object     # @.str.1122
.L.str.1122:
	.asciz	"nbsp"
	.size	.L.str.1122, 5

	.type	.L.str.1123,@object     # @.str.1123
.L.str.1123:
	.asciz	"NotLessGreater"
	.size	.L.str.1123, 15

	.type	.L.str.1124,@object     # @.str.1124
.L.str.1124:
	.asciz	"thetav"
	.size	.L.str.1124, 7

	.type	.L.str.1125,@object     # @.str.1125
.L.str.1125:
	.asciz	"sup"
	.size	.L.str.1125, 4

	.type	.L.str.1126,@object     # @.str.1126
.L.str.1126:
	.asciz	"lsqb"
	.size	.L.str.1126, 5

	.type	.L.str.1127,@object     # @.str.1127
.L.str.1127:
	.asciz	"check"
	.size	.L.str.1127, 6

	.type	.L.str.1128,@object     # @.str.1128
.L.str.1128:
	.asciz	"Gt"
	.size	.L.str.1128, 3

	.type	.L.str.1129,@object     # @.str.1129
.L.str.1129:
	.asciz	"boxv"
	.size	.L.str.1129, 5

	.type	.L.str.1130,@object     # @.str.1130
.L.str.1130:
	.asciz	"rsqb"
	.size	.L.str.1130, 5

	.type	.L.str.1131,@object     # @.str.1131
.L.str.1131:
	.asciz	"Implies"
	.size	.L.str.1131, 8

	.type	.L.str.1132,@object     # @.str.1132
.L.str.1132:
	.asciz	"shortparallel"
	.size	.L.str.1132, 14

	.type	.L.str.1133,@object     # @.str.1133
.L.str.1133:
	.asciz	"Lt"
	.size	.L.str.1133, 3

	.type	.L.str.1134,@object     # @.str.1134
.L.str.1134:
	.asciz	"Sub"
	.size	.L.str.1134, 4

	.type	.L.str.1135,@object     # @.str.1135
.L.str.1135:
	.asciz	"HARDcy"
	.size	.L.str.1135, 7

	.type	.L.str.1136,@object     # @.str.1136
.L.str.1136:
	.asciz	"thorn"
	.size	.L.str.1136, 6

	.type	.L.str.1137,@object     # @.str.1137
.L.str.1137:
	.asciz	"boxvr"
	.size	.L.str.1137, 6

	.type	.L.str.1138,@object     # @.str.1138
.L.str.1138:
	.asciz	"DD"
	.size	.L.str.1138, 3

	.type	.L.str.1139,@object     # @.str.1139
.L.str.1139:
	.asciz	"nVdash"
	.size	.L.str.1139, 7

	.type	.L.str.1140,@object     # @.str.1140
.L.str.1140:
	.asciz	"angrt"
	.size	.L.str.1140, 6

	.type	.L.str.1141,@object     # @.str.1141
.L.str.1141:
	.asciz	"gt"
	.size	.L.str.1141, 3

	.type	.L.str.1142,@object     # @.str.1142
.L.str.1142:
	.asciz	"trianglerighteq"
	.size	.L.str.1142, 16

	.type	.L.str.1143,@object     # @.str.1143
.L.str.1143:
	.asciz	"sub"
	.size	.L.str.1143, 4

	.type	.L.str.1144,@object     # @.str.1144
.L.str.1144:
	.asciz	"it"
	.size	.L.str.1144, 3

	.type	.L.str.1145,@object     # @.str.1145
.L.str.1145:
	.asciz	"lt"
	.size	.L.str.1145, 3

	.type	.L.str.1146,@object     # @.str.1146
.L.str.1146:
	.asciz	"cudarrr"
	.size	.L.str.1146, 8

	.type	.L.str.1147,@object     # @.str.1147
.L.str.1147:
	.asciz	"DZcy"
	.size	.L.str.1147, 5

	.type	.L.str.1148,@object     # @.str.1148
.L.str.1148:
	.asciz	"seswar"
	.size	.L.str.1148, 7

	.type	.L.str.1149,@object     # @.str.1149
.L.str.1149:
	.asciz	"boxhU"
	.size	.L.str.1149, 6

	.type	.L.str.1150,@object     # @.str.1150
.L.str.1150:
	.asciz	"precnapprox"
	.size	.L.str.1150, 12

	.type	.L.str.1151,@object     # @.str.1151
.L.str.1151:
	.asciz	"fallingdotseq"
	.size	.L.str.1151, 14

	.type	.L.str.1152,@object     # @.str.1152
.L.str.1152:
	.asciz	"vartheta"
	.size	.L.str.1152, 9

	.type	.L.str.1153,@object     # @.str.1153
.L.str.1153:
	.asciz	"digamma"
	.size	.L.str.1153, 8

	.type	.L.str.1154,@object     # @.str.1154
.L.str.1154:
	.asciz	"Omicron"
	.size	.L.str.1154, 8

	.type	.L.str.1155,@object     # @.str.1155
.L.str.1155:
	.asciz	"oror"
	.size	.L.str.1155, 5

	.type	.L.str.1156,@object     # @.str.1156
.L.str.1156:
	.asciz	"DownLeftRightVector"
	.size	.L.str.1156, 20

	.type	.L.str.1157,@object     # @.str.1157
.L.str.1157:
	.asciz	"NonBreakingSpace"
	.size	.L.str.1157, 17

	.type	.L.str.1158,@object     # @.str.1158
.L.str.1158:
	.asciz	"NotVerticalBar"
	.size	.L.str.1158, 15

	.type	.L.str.1159,@object     # @.str.1159
.L.str.1159:
	.asciz	"boxvh"
	.size	.L.str.1159, 6

	.type	.L.str.1160,@object     # @.str.1160
.L.str.1160:
	.asciz	"smile"
	.size	.L.str.1160, 6

	.type	.L.str.1161,@object     # @.str.1161
.L.str.1161:
	.asciz	"Bumpeq"
	.size	.L.str.1161, 7

	.type	.L.str.1162,@object     # @.str.1162
.L.str.1162:
	.asciz	"Proportion"
	.size	.L.str.1162, 11

	.type	.L.str.1163,@object     # @.str.1163
.L.str.1163:
	.asciz	"eDot"
	.size	.L.str.1163, 5

	.type	.L.str.1164,@object     # @.str.1164
.L.str.1164:
	.asciz	"numsp"
	.size	.L.str.1164, 6

	.type	.L.str.1165,@object     # @.str.1165
.L.str.1165:
	.asciz	"ShortLeftArrow"
	.size	.L.str.1165, 15

	.type	.L.str.1166,@object     # @.str.1166
.L.str.1166:
	.asciz	"omicron"
	.size	.L.str.1166, 8

	.type	.L.str.1167,@object     # @.str.1167
.L.str.1167:
	.asciz	"realpart"
	.size	.L.str.1167, 9

	.type	.L.str.1168,@object     # @.str.1168
.L.str.1168:
	.asciz	"HumpEqual"
	.size	.L.str.1168, 10

	.type	.L.str.1169,@object     # @.str.1169
.L.str.1169:
	.asciz	"RightDoubleBracket"
	.size	.L.str.1169, 19

	.type	.L.str.1170,@object     # @.str.1170
.L.str.1170:
	.asciz	"lhard"
	.size	.L.str.1170, 6

	.type	.L.str.1171,@object     # @.str.1171
.L.str.1171:
	.asciz	"Union"
	.size	.L.str.1171, 6

	.type	.L.str.1172,@object     # @.str.1172
.L.str.1172:
	.asciz	"rhard"
	.size	.L.str.1172, 6

	.type	.L.str.1173,@object     # @.str.1173
.L.str.1173:
	.asciz	"topcir"
	.size	.L.str.1173, 7

	.type	.L.str.1174,@object     # @.str.1174
.L.str.1174:
	.asciz	"Copf"
	.size	.L.str.1174, 5

	.type	.L.str.1175,@object     # @.str.1175
.L.str.1175:
	.asciz	"bumpeq"
	.size	.L.str.1175, 7

	.type	.L.str.1176,@object     # @.str.1176
.L.str.1176:
	.asciz	"ntrianglelefteq"
	.size	.L.str.1176, 16

	.type	.L.str.1177,@object     # @.str.1177
.L.str.1177:
	.asciz	"lsquo"
	.size	.L.str.1177, 6

	.type	.L.str.1178,@object     # @.str.1178
.L.str.1178:
	.asciz	"drarr"
	.size	.L.str.1178, 6

	.type	.L.str.1179,@object     # @.str.1179
.L.str.1179:
	.asciz	"erarr"
	.size	.L.str.1179, 6

	.type	.L.str.1180,@object     # @.str.1180
.L.str.1180:
	.asciz	"crarr"
	.size	.L.str.1180, 6

	.type	.L.str.1181,@object     # @.str.1181
.L.str.1181:
	.asciz	"Hopf"
	.size	.L.str.1181, 5

	.type	.L.str.1182,@object     # @.str.1182
.L.str.1182:
	.asciz	"rsquo"
	.size	.L.str.1182, 6

	.type	.L.str.1183,@object     # @.str.1183
.L.str.1183:
	.asciz	"Nopf"
	.size	.L.str.1183, 5

	.type	.L.str.1184,@object     # @.str.1184
.L.str.1184:
	.asciz	"lrarr"
	.size	.L.str.1184, 6

	.type	.L.str.1185,@object     # @.str.1185
.L.str.1185:
	.asciz	"Uarrocir"
	.size	.L.str.1185, 9

	.type	.L.str.1186,@object     # @.str.1186
.L.str.1186:
	.asciz	"nrarr"
	.size	.L.str.1186, 6

	.type	.L.str.1187,@object     # @.str.1187
.L.str.1187:
	.asciz	"backcong"
	.size	.L.str.1187, 9

	.type	.L.str.1188,@object     # @.str.1188
.L.str.1188:
	.asciz	"orarr"
	.size	.L.str.1188, 6

	.type	.L.str.1189,@object     # @.str.1189
.L.str.1189:
	.asciz	"RBarr"
	.size	.L.str.1189, 6

	.type	.L.str.1190,@object     # @.str.1190
.L.str.1190:
	.asciz	"rrarr"
	.size	.L.str.1190, 6

	.type	.L.str.1191,@object     # @.str.1191
.L.str.1191:
	.asciz	"srarr"
	.size	.L.str.1191, 6

	.type	.L.str.1192,@object     # @.str.1192
.L.str.1192:
	.asciz	"Qopf"
	.size	.L.str.1192, 5

	.type	.L.str.1193,@object     # @.str.1193
.L.str.1193:
	.asciz	"Ropf"
	.size	.L.str.1193, 5

	.type	.L.str.1194,@object     # @.str.1194
.L.str.1194:
	.asciz	"trianglelefteq"
	.size	.L.str.1194, 15

	.type	.L.str.1195,@object     # @.str.1195
.L.str.1195:
	.asciz	"Popf"
	.size	.L.str.1195, 5

	.type	.L.str.1196,@object     # @.str.1196
.L.str.1196:
	.asciz	"xrarr"
	.size	.L.str.1196, 6

	.type	.L.str.1197,@object     # @.str.1197
.L.str.1197:
	.asciz	"Zopf"
	.size	.L.str.1197, 5

	.type	.L.str.1198,@object     # @.str.1198
.L.str.1198:
	.asciz	"CupCap"
	.size	.L.str.1198, 7

	.type	.L.str.1199,@object     # @.str.1199
.L.str.1199:
	.asciz	"comma"
	.size	.L.str.1199, 6

	.type	.L.str.1200,@object     # @.str.1200
.L.str.1200:
	.asciz	"Acy"
	.size	.L.str.1200, 4

	.type	.L.str.1201,@object     # @.str.1201
.L.str.1201:
	.asciz	"Bcy"
	.size	.L.str.1201, 4

	.type	.L.str.1202,@object     # @.str.1202
.L.str.1202:
	.asciz	"lBarr"
	.size	.L.str.1202, 6

	.type	.L.str.1203,@object     # @.str.1203
.L.str.1203:
	.asciz	"HilbertSpace"
	.size	.L.str.1203, 13

	.type	.L.str.1204,@object     # @.str.1204
.L.str.1204:
	.asciz	"Ecy"
	.size	.L.str.1204, 4

	.type	.L.str.1205,@object     # @.str.1205
.L.str.1205:
	.asciz	"Fcy"
	.size	.L.str.1205, 4

	.type	.L.str.1206,@object     # @.str.1206
.L.str.1206:
	.asciz	"Gcy"
	.size	.L.str.1206, 4

	.type	.L.str.1207,@object     # @.str.1207
.L.str.1207:
	.asciz	"rBarr"
	.size	.L.str.1207, 6

	.type	.L.str.1208,@object     # @.str.1208
.L.str.1208:
	.asciz	"Icy"
	.size	.L.str.1208, 4

	.type	.L.str.1209,@object     # @.str.1209
.L.str.1209:
	.asciz	"Jcy"
	.size	.L.str.1209, 4

	.type	.L.str.1210,@object     # @.str.1210
.L.str.1210:
	.asciz	"Kcy"
	.size	.L.str.1210, 4

	.type	.L.str.1211,@object     # @.str.1211
.L.str.1211:
	.asciz	"Lcy"
	.size	.L.str.1211, 4

	.type	.L.str.1212,@object     # @.str.1212
.L.str.1212:
	.asciz	"aelig"
	.size	.L.str.1212, 6

	.type	.L.str.1213,@object     # @.str.1213
.L.str.1213:
	.asciz	"Mcy"
	.size	.L.str.1213, 4

	.type	.L.str.1214,@object     # @.str.1214
.L.str.1214:
	.asciz	"Ocy"
	.size	.L.str.1214, 4

	.type	.L.str.1215,@object     # @.str.1215
.L.str.1215:
	.asciz	"Pcy"
	.size	.L.str.1215, 4

	.type	.L.str.1216,@object     # @.str.1216
.L.str.1216:
	.asciz	"Ncy"
	.size	.L.str.1216, 4

	.type	.L.str.1217,@object     # @.str.1217
.L.str.1217:
	.asciz	"Rcy"
	.size	.L.str.1217, 4

	.type	.L.str.1218,@object     # @.str.1218
.L.str.1218:
	.asciz	"Scy"
	.size	.L.str.1218, 4

	.type	.L.str.1219,@object     # @.str.1219
.L.str.1219:
	.asciz	"ShortUpArrow"
	.size	.L.str.1219, 13

	.type	.L.str.1220,@object     # @.str.1220
.L.str.1220:
	.asciz	"Ucy"
	.size	.L.str.1220, 4

	.type	.L.str.1221,@object     # @.str.1221
.L.str.1221:
	.asciz	"Vcy"
	.size	.L.str.1221, 4

	.type	.L.str.1222,@object     # @.str.1222
.L.str.1222:
	.asciz	"Tcy"
	.size	.L.str.1222, 4

	.type	.L.str.1223,@object     # @.str.1223
.L.str.1223:
	.asciz	"DiacriticalDoubleAcute"
	.size	.L.str.1223, 23

	.type	.L.str.1224,@object     # @.str.1224
.L.str.1224:
	.asciz	"Alpha"
	.size	.L.str.1224, 6

	.type	.L.str.1225,@object     # @.str.1225
.L.str.1225:
	.asciz	"Ycy"
	.size	.L.str.1225, 4

	.type	.L.str.1226,@object     # @.str.1226
.L.str.1226:
	.asciz	"Zcy"
	.size	.L.str.1226, 4

	.type	.L.str.1227,@object     # @.str.1227
.L.str.1227:
	.asciz	"nmid"
	.size	.L.str.1227, 5

	.type	.L.str.1228,@object     # @.str.1228
.L.str.1228:
	.asciz	"NotSubsetEqual"
	.size	.L.str.1228, 15

	.type	.L.str.1229,@object     # @.str.1229
.L.str.1229:
	.asciz	"ntrianglerighteq"
	.size	.L.str.1229, 17

	.type	.L.str.1230,@object     # @.str.1230
.L.str.1230:
	.asciz	"straightepsilon"
	.size	.L.str.1230, 16

	.type	.L.str.1231,@object     # @.str.1231
.L.str.1231:
	.asciz	"ddotseq"
	.size	.L.str.1231, 8

	.type	.L.str.1232,@object     # @.str.1232
.L.str.1232:
	.asciz	"acy"
	.size	.L.str.1232, 4

	.type	.L.str.1233,@object     # @.str.1233
.L.str.1233:
	.asciz	"bcy"
	.size	.L.str.1233, 4

	.type	.L.str.1234,@object     # @.str.1234
.L.str.1234:
	.asciz	"dcy"
	.size	.L.str.1234, 4

	.type	.L.str.1235,@object     # @.str.1235
.L.str.1235:
	.asciz	"ecy"
	.size	.L.str.1235, 4

	.type	.L.str.1236,@object     # @.str.1236
.L.str.1236:
	.asciz	"gnapprox"
	.size	.L.str.1236, 9

	.type	.L.str.1237,@object     # @.str.1237
.L.str.1237:
	.asciz	"fcy"
	.size	.L.str.1237, 4

	.type	.L.str.1238,@object     # @.str.1238
.L.str.1238:
	.asciz	"Dcy"
	.size	.L.str.1238, 4

	.type	.L.str.1239,@object     # @.str.1239
.L.str.1239:
	.asciz	"icy"
	.size	.L.str.1239, 4

	.type	.L.str.1240,@object     # @.str.1240
.L.str.1240:
	.asciz	"gcy"
	.size	.L.str.1240, 4

	.type	.L.str.1241,@object     # @.str.1241
.L.str.1241:
	.asciz	"lnapprox"
	.size	.L.str.1241, 9

	.type	.L.str.1242,@object     # @.str.1242
.L.str.1242:
	.asciz	"TildeFullEqual"
	.size	.L.str.1242, 15

	.type	.L.str.1243,@object     # @.str.1243
.L.str.1243:
	.asciz	"jcy"
	.size	.L.str.1243, 4

	.type	.L.str.1244,@object     # @.str.1244
.L.str.1244:
	.asciz	"kcy"
	.size	.L.str.1244, 4

	.type	.L.str.1245,@object     # @.str.1245
.L.str.1245:
	.asciz	"lcy"
	.size	.L.str.1245, 4

	.type	.L.str.1246,@object     # @.str.1246
.L.str.1246:
	.asciz	"ReverseUpEquilibrium"
	.size	.L.str.1246, 21

	.type	.L.str.1247,@object     # @.str.1247
.L.str.1247:
	.asciz	"ncy"
	.size	.L.str.1247, 4

	.type	.L.str.1248,@object     # @.str.1248
.L.str.1248:
	.asciz	"ocy"
	.size	.L.str.1248, 4

	.type	.L.str.1249,@object     # @.str.1249
.L.str.1249:
	.asciz	"ocir"
	.size	.L.str.1249, 5

	.type	.L.str.1250,@object     # @.str.1250
.L.str.1250:
	.asciz	"scy"
	.size	.L.str.1250, 4

	.type	.L.str.1251,@object     # @.str.1251
.L.str.1251:
	.asciz	"rcy"
	.size	.L.str.1251, 4

	.type	.L.str.1252,@object     # @.str.1252
.L.str.1252:
	.asciz	"pcy"
	.size	.L.str.1252, 4

	.type	.L.str.1253,@object     # @.str.1253
.L.str.1253:
	.asciz	"mcy"
	.size	.L.str.1253, 4

	.type	.L.str.1254,@object     # @.str.1254
.L.str.1254:
	.asciz	"ucy"
	.size	.L.str.1254, 4

	.type	.L.str.1255,@object     # @.str.1255
.L.str.1255:
	.asciz	"alpha"
	.size	.L.str.1255, 6

	.type	.L.str.1256,@object     # @.str.1256
.L.str.1256:
	.asciz	"DoubleContourIntegral"
	.size	.L.str.1256, 22

	.type	.L.str.1257,@object     # @.str.1257
.L.str.1257:
	.asciz	"zcy"
	.size	.L.str.1257, 4

	.type	.L.str.1258,@object     # @.str.1258
.L.str.1258:
	.asciz	"nshortmid"
	.size	.L.str.1258, 10

	.type	.L.str.1259,@object     # @.str.1259
.L.str.1259:
	.asciz	"ohm"
	.size	.L.str.1259, 4

	.type	.L.str.1260,@object     # @.str.1260
.L.str.1260:
	.asciz	"tcy"
	.size	.L.str.1260, 4

	.type	.L.str.1261,@object     # @.str.1261
.L.str.1261:
	.asciz	"ycy"
	.size	.L.str.1261, 4

	.type	.L.str.1262,@object     # @.str.1262
.L.str.1262:
	.asciz	"vcy"
	.size	.L.str.1262, 4

	.type	.L.str.1263,@object     # @.str.1263
.L.str.1263:
	.asciz	"sqsub"
	.size	.L.str.1263, 6

	.type	.L.str.1264,@object     # @.str.1264
.L.str.1264:
	.asciz	"Udigr"
	.size	.L.str.1264, 6

	.type	.L.str.1265,@object     # @.str.1265
.L.str.1265:
	.asciz	"DownLeftTeeVector"
	.size	.L.str.1265, 18

	.type	.L.str.1266,@object     # @.str.1266
.L.str.1266:
	.asciz	"udigr"
	.size	.L.str.1266, 6

	.type	.L.str.1267,@object     # @.str.1267
.L.str.1267:
	.asciz	"ecir"
	.size	.L.str.1267, 5

	.type	.L.str.1268,@object     # @.str.1268
.L.str.1268:
	.asciz	"cularrp"
	.size	.L.str.1268, 8

	.type	.L.str.1269,@object     # @.str.1269
.L.str.1269:
	.asciz	"idigr"
	.size	.L.str.1269, 6

	.type	.L.str.1270,@object     # @.str.1270
.L.str.1270:
	.asciz	"smid"
	.size	.L.str.1270, 5

	.type	.L.str.1271,@object     # @.str.1271
.L.str.1271:
	.asciz	"Idigr"
	.size	.L.str.1271, 6

	.type	.L.str.1272,@object     # @.str.1272
.L.str.1272:
	.asciz	"DoubleLongLeftArrow"
	.size	.L.str.1272, 20

	.type	.L.str.1273,@object     # @.str.1273
.L.str.1273:
	.asciz	"SquareSupersetEqual"
	.size	.L.str.1273, 20

	.type	.L.str.1274,@object     # @.str.1274
.L.str.1274:
	.asciz	"FilledSmallSquare"
	.size	.L.str.1274, 18

	.type	.L.str.1275,@object     # @.str.1275
.L.str.1275:
	.asciz	"oelig"
	.size	.L.str.1275, 6

	.type	.L.str.1276,@object     # @.str.1276
.L.str.1276:
	.asciz	"NotGreaterLess"
	.size	.L.str.1276, 15

	.type	.L.str.1277,@object     # @.str.1277
.L.str.1277:
	.asciz	"b.Gammad"
	.size	.L.str.1277, 9

	.type	.L.str.1278,@object     # @.str.1278
.L.str.1278:
	.asciz	"rarrap"
	.size	.L.str.1278, 7

	.type	.L.str.1279,@object     # @.str.1279
.L.str.1279:
	.asciz	"cylcty"
	.size	.L.str.1279, 7

	.type	.L.str.1280,@object     # @.str.1280
.L.str.1280:
	.asciz	"ContourIntegral"
	.size	.L.str.1280, 16

	.type	.L.str.1281,@object     # @.str.1281
.L.str.1281:
	.asciz	"Intersection"
	.size	.L.str.1281, 13

	.type	.L.str.1282,@object     # @.str.1282
.L.str.1282:
	.asciz	"acd"
	.size	.L.str.1282, 4

	.type	.L.str.1283,@object     # @.str.1283
.L.str.1283:
	.asciz	"fltns"
	.size	.L.str.1283, 6

	.type	.L.str.1284,@object     # @.str.1284
.L.str.1284:
	.asciz	"Lleftarrow"
	.size	.L.str.1284, 11

	.type	.L.str.1285,@object     # @.str.1285
.L.str.1285:
	.asciz	"NotSupersetEqual"
	.size	.L.str.1285, 17

	.type	.L.str.1286,@object     # @.str.1286
.L.str.1286:
	.asciz	"nvinfin"
	.size	.L.str.1286, 8

	.type	.L.str.1287,@object     # @.str.1287
.L.str.1287:
	.asciz	"Acirc"
	.size	.L.str.1287, 6

	.type	.L.str.1288,@object     # @.str.1288
.L.str.1288:
	.asciz	"circlearrowleft"
	.size	.L.str.1288, 16

	.type	.L.str.1289,@object     # @.str.1289
.L.str.1289:
	.asciz	"Ecirc"
	.size	.L.str.1289, 6

	.type	.L.str.1290,@object     # @.str.1290
.L.str.1290:
	.asciz	"Mu"
	.size	.L.str.1290, 3

	.type	.L.str.1291,@object     # @.str.1291
.L.str.1291:
	.asciz	"Nu"
	.size	.L.str.1291, 3

	.type	.L.str.1292,@object     # @.str.1292
.L.str.1292:
	.asciz	"complexes"
	.size	.L.str.1292, 10

	.type	.L.str.1293,@object     # @.str.1293
.L.str.1293:
	.asciz	"Icirc"
	.size	.L.str.1293, 6

	.type	.L.str.1294,@object     # @.str.1294
.L.str.1294:
	.asciz	"NotLeftTriangle"
	.size	.L.str.1294, 16

	.type	.L.str.1295,@object     # @.str.1295
.L.str.1295:
	.asciz	"dotsquare"
	.size	.L.str.1295, 10

	.type	.L.str.1296,@object     # @.str.1296
.L.str.1296:
	.asciz	"Ocirc"
	.size	.L.str.1296, 6

	.type	.L.str.1297,@object     # @.str.1297
.L.str.1297:
	.asciz	"swnwar"
	.size	.L.str.1297, 7

	.type	.L.str.1298,@object     # @.str.1298
.L.str.1298:
	.asciz	"nsucc"
	.size	.L.str.1298, 6

	.type	.L.str.1299,@object     # @.str.1299
.L.str.1299:
	.asciz	"Pi"
	.size	.L.str.1299, 3

	.type	.L.str.1300,@object     # @.str.1300
.L.str.1300:
	.asciz	"Ucirc"
	.size	.L.str.1300, 6

	.type	.L.str.1301,@object     # @.str.1301
.L.str.1301:
	.asciz	"squ"
	.size	.L.str.1301, 4

	.type	.L.str.1302,@object     # @.str.1302
.L.str.1302:
	.asciz	"boxur"
	.size	.L.str.1302, 6

	.type	.L.str.1303,@object     # @.str.1303
.L.str.1303:
	.asciz	"Xi"
	.size	.L.str.1303, 3

	.type	.L.str.1304,@object     # @.str.1304
.L.str.1304:
	.asciz	"there4"
	.size	.L.str.1304, 7

	.type	.L.str.1305,@object     # @.str.1305
.L.str.1305:
	.asciz	"LeftRightVector"
	.size	.L.str.1305, 16

	.type	.L.str.1306,@object     # @.str.1306
.L.str.1306:
	.asciz	"nleftarrow"
	.size	.L.str.1306, 11

	.type	.L.str.1307,@object     # @.str.1307
.L.str.1307:
	.asciz	"NotSquareSubsetEqual"
	.size	.L.str.1307, 21

	.type	.L.str.1308,@object     # @.str.1308
.L.str.1308:
	.asciz	"acirc"
	.size	.L.str.1308, 6

	.type	.L.str.1309,@object     # @.str.1309
.L.str.1309:
	.asciz	"leftrightharpoons"
	.size	.L.str.1309, 18

	.type	.L.str.1310,@object     # @.str.1310
.L.str.1310:
	.asciz	"Omega"
	.size	.L.str.1310, 6

	.type	.L.str.1311,@object     # @.str.1311
.L.str.1311:
	.asciz	"Aacgr"
	.size	.L.str.1311, 6

	.type	.L.str.1312,@object     # @.str.1312
.L.str.1312:
	.asciz	"boxHD"
	.size	.L.str.1312, 6

	.type	.L.str.1313,@object     # @.str.1313
.L.str.1313:
	.asciz	"ecirc"
	.size	.L.str.1313, 6

	.type	.L.str.1314,@object     # @.str.1314
.L.str.1314:
	.asciz	"nu"
	.size	.L.str.1314, 3

	.type	.L.str.1315,@object     # @.str.1315
.L.str.1315:
	.asciz	"nhArr"
	.size	.L.str.1315, 6

	.type	.L.str.1316,@object     # @.str.1316
.L.str.1316:
	.asciz	"Mellintrf"
	.size	.L.str.1316, 10

	.type	.L.str.1317,@object     # @.str.1317
.L.str.1317:
	.asciz	"icirc"
	.size	.L.str.1317, 6

	.type	.L.str.1318,@object     # @.str.1318
.L.str.1318:
	.asciz	"succ"
	.size	.L.str.1318, 5

	.type	.L.str.1319,@object     # @.str.1319
.L.str.1319:
	.asciz	"mu"
	.size	.L.str.1319, 3

	.type	.L.str.1320,@object     # @.str.1320
.L.str.1320:
	.asciz	"ni"
	.size	.L.str.1320, 3

	.type	.L.str.1321,@object     # @.str.1321
.L.str.1321:
	.asciz	"ocirc"
	.size	.L.str.1321, 6

	.type	.L.str.1322,@object     # @.str.1322
.L.str.1322:
	.asciz	"pi"
	.size	.L.str.1322, 3

	.type	.L.str.1323,@object     # @.str.1323
.L.str.1323:
	.asciz	"DiacriticalAcute"
	.size	.L.str.1323, 17

	.type	.L.str.1324,@object     # @.str.1324
.L.str.1324:
	.asciz	"xhArr"
	.size	.L.str.1324, 6

	.type	.L.str.1325,@object     # @.str.1325
.L.str.1325:
	.asciz	"curlyvee"
	.size	.L.str.1325, 9

	.type	.L.str.1326,@object     # @.str.1326
.L.str.1326:
	.asciz	"deg"
	.size	.L.str.1326, 4

	.type	.L.str.1327,@object     # @.str.1327
.L.str.1327:
	.asciz	"ucirc"
	.size	.L.str.1327, 6

	.type	.L.str.1328,@object     # @.str.1328
.L.str.1328:
	.asciz	"yen"
	.size	.L.str.1328, 4

	.type	.L.str.1329,@object     # @.str.1329
.L.str.1329:
	.asciz	"xi"
	.size	.L.str.1329, 3

	.type	.L.str.1330,@object     # @.str.1330
.L.str.1330:
	.asciz	"xcirc"
	.size	.L.str.1330, 6

	.type	.L.str.1331,@object     # @.str.1331
.L.str.1331:
	.asciz	"IOcy"
	.size	.L.str.1331, 5

	.type	.L.str.1332,@object     # @.str.1332
.L.str.1332:
	.asciz	"els"
	.size	.L.str.1332, 4

	.type	.L.str.1333,@object     # @.str.1333
.L.str.1333:
	.asciz	"gE"
	.size	.L.str.1333, 3

	.type	.L.str.1334,@object     # @.str.1334
.L.str.1334:
	.asciz	"leg"
	.size	.L.str.1334, 4

	.type	.L.str.1335,@object     # @.str.1335
.L.str.1335:
	.asciz	"Iacgr"
	.size	.L.str.1335, 6

	.type	.L.str.1336,@object     # @.str.1336
.L.str.1336:
	.asciz	"LeftTeeArrow"
	.size	.L.str.1336, 13

	.type	.L.str.1337,@object     # @.str.1337
.L.str.1337:
	.asciz	"dscy"
	.size	.L.str.1337, 5

	.type	.L.str.1338,@object     # @.str.1338
.L.str.1338:
	.asciz	"SucceedsSlantEqual"
	.size	.L.str.1338, 19

	.type	.L.str.1339,@object     # @.str.1339
.L.str.1339:
	.asciz	"fpartint"
	.size	.L.str.1339, 9

	.type	.L.str.1340,@object     # @.str.1340
.L.str.1340:
	.asciz	"omega"
	.size	.L.str.1340, 6

	.type	.L.str.1341,@object     # @.str.1341
.L.str.1341:
	.asciz	"frac34"
	.size	.L.str.1341, 7

	.type	.L.str.1342,@object     # @.str.1342
.L.str.1342:
	.asciz	"reg"
	.size	.L.str.1342, 4

	.type	.L.str.1343,@object     # @.str.1343
.L.str.1343:
	.asciz	"ShortDownArrow"
	.size	.L.str.1343, 15

	.type	.L.str.1344,@object     # @.str.1344
.L.str.1344:
	.asciz	"quot"
	.size	.L.str.1344, 5

	.type	.L.str.1345,@object     # @.str.1345
.L.str.1345:
	.asciz	"ell"
	.size	.L.str.1345, 4

	.type	.L.str.1346,@object     # @.str.1346
.L.str.1346:
	.asciz	"circlearrowright"
	.size	.L.str.1346, 17

	.type	.L.str.1347,@object     # @.str.1347
.L.str.1347:
	.asciz	"inodot"
	.size	.L.str.1347, 7

	.type	.L.str.1348,@object     # @.str.1348
.L.str.1348:
	.asciz	"xdtri"
	.size	.L.str.1348, 6

	.type	.L.str.1349,@object     # @.str.1349
.L.str.1349:
	.asciz	"iacgr"
	.size	.L.str.1349, 6

	.type	.L.str.1350,@object     # @.str.1350
.L.str.1350:
	.asciz	"imagpart"
	.size	.L.str.1350, 9

	.type	.L.str.1351,@object     # @.str.1351
.L.str.1351:
	.asciz	"ldrdhar"
	.size	.L.str.1351, 8

	.type	.L.str.1352,@object     # @.str.1352
.L.str.1352:
	.asciz	"tscy"
	.size	.L.str.1352, 5

	.type	.L.str.1353,@object     # @.str.1353
.L.str.1353:
	.asciz	"jnodot"
	.size	.L.str.1353, 7

	.type	.L.str.1354,@object     # @.str.1354
.L.str.1354:
	.asciz	"colone"
	.size	.L.str.1354, 7

	.type	.L.str.1355,@object     # @.str.1355
.L.str.1355:
	.asciz	"Eacgr"
	.size	.L.str.1355, 6

	.type	.L.str.1356,@object     # @.str.1356
.L.str.1356:
	.asciz	"blk34"
	.size	.L.str.1356, 6

	.type	.L.str.1357,@object     # @.str.1357
.L.str.1357:
	.asciz	"eacgr"
	.size	.L.str.1357, 6

	.type	.L.str.1358,@object     # @.str.1358
.L.str.1358:
	.asciz	"LongLeftArrow"
	.size	.L.str.1358, 14

	.type	.L.str.1359,@object     # @.str.1359
.L.str.1359:
	.asciz	"lE"
	.size	.L.str.1359, 3

	.type	.L.str.1360,@object     # @.str.1360
.L.str.1360:
	.asciz	"nsmid"
	.size	.L.str.1360, 6

	.type	.L.str.1361,@object     # @.str.1361
.L.str.1361:
	.asciz	"uacgr"
	.size	.L.str.1361, 6

	.type	.L.str.1362,@object     # @.str.1362
.L.str.1362:
	.asciz	"boxVR"
	.size	.L.str.1362, 6

	.type	.L.str.1363,@object     # @.str.1363
.L.str.1363:
	.asciz	"Oacgr"
	.size	.L.str.1363, 6

	.type	.L.str.1364,@object     # @.str.1364
.L.str.1364:
	.asciz	"aacgr"
	.size	.L.str.1364, 6

	.type	.L.str.1365,@object     # @.str.1365
.L.str.1365:
	.asciz	"oacgr"
	.size	.L.str.1365, 6

	.type	.L.str.1366,@object     # @.str.1366
.L.str.1366:
	.asciz	"luruhar"
	.size	.L.str.1366, 8

	.type	.L.str.1367,@object     # @.str.1367
.L.str.1367:
	.asciz	"ii"
	.size	.L.str.1367, 3

	.type	.L.str.1368,@object     # @.str.1368
.L.str.1368:
	.asciz	"telrec"
	.size	.L.str.1368, 7

	.type	.L.str.1369,@object     # @.str.1369
.L.str.1369:
	.asciz	"DDotrahd"
	.size	.L.str.1369, 9

	.type	.L.str.1370,@object     # @.str.1370
.L.str.1370:
	.asciz	"nle"
	.size	.L.str.1370, 4

	.type	.L.str.1371,@object     # @.str.1371
.L.str.1371:
	.asciz	"gtrless"
	.size	.L.str.1371, 8

	.type	.L.str.1372,@object     # @.str.1372
.L.str.1372:
	.asciz	"midcir"
	.size	.L.str.1372, 7

	.type	.L.str.1373,@object     # @.str.1373
.L.str.1373:
	.asciz	"DownArrowBar"
	.size	.L.str.1373, 13

	.type	.L.str.1374,@object     # @.str.1374
.L.str.1374:
	.asciz	"semi"
	.size	.L.str.1374, 5

	.type	.L.str.1375,@object     # @.str.1375
.L.str.1375:
	.asciz	"Uacgr"
	.size	.L.str.1375, 6

	.type	.L.str.1376,@object     # @.str.1376
.L.str.1376:
	.asciz	"lbrace"
	.size	.L.str.1376, 7

	.type	.L.str.1377,@object     # @.str.1377
.L.str.1377:
	.asciz	"phmmat"
	.size	.L.str.1377, 7

	.type	.L.str.1378,@object     # @.str.1378
.L.str.1378:
	.asciz	"rbrace"
	.size	.L.str.1378, 7

	.type	.L.str.1379,@object     # @.str.1379
.L.str.1379:
	.asciz	"NotCongruent"
	.size	.L.str.1379, 13

	.type	.L.str.1380,@object     # @.str.1380
.L.str.1380:
	.asciz	"shchcy"
	.size	.L.str.1380, 7

	.type	.L.str.1381,@object     # @.str.1381
.L.str.1381:
	.asciz	"simdot"
	.size	.L.str.1381, 7

	.type	.L.str.1382,@object     # @.str.1382
.L.str.1382:
	.asciz	"cirmid"
	.size	.L.str.1382, 7

	.type	.L.str.1383,@object     # @.str.1383
.L.str.1383:
	.asciz	"SubsetEqual"
	.size	.L.str.1383, 12

	.type	.L.str.1384,@object     # @.str.1384
.L.str.1384:
	.asciz	"female"
	.size	.L.str.1384, 7

	.type	.L.str.1385,@object     # @.str.1385
.L.str.1385:
	.asciz	"nsc"
	.size	.L.str.1385, 4

	.type	.L.str.1386,@object     # @.str.1386
.L.str.1386:
	.asciz	"boxVH"
	.size	.L.str.1386, 6

	.type	.L.str.1387,@object     # @.str.1387
.L.str.1387:
	.asciz	"ldrushar"
	.size	.L.str.1387, 9

	.type	.L.str.1388,@object     # @.str.1388
.L.str.1388:
	.asciz	"triangleright"
	.size	.L.str.1388, 14

	.type	.L.str.1389,@object     # @.str.1389
.L.str.1389:
	.asciz	"gneqq"
	.size	.L.str.1389, 6

	.type	.L.str.1390,@object     # @.str.1390
.L.str.1390:
	.asciz	"DownArrow"
	.size	.L.str.1390, 10

	.type	.L.str.1391,@object     # @.str.1391
.L.str.1391:
	.asciz	"backsim"
	.size	.L.str.1391, 8

	.type	.L.str.1392,@object     # @.str.1392
.L.str.1392:
	.asciz	"lneqq"
	.size	.L.str.1392, 6

	.type	.L.str.1393,@object     # @.str.1393
.L.str.1393:
	.asciz	"Sum"
	.size	.L.str.1393, 4

	.type	.L.str.1394,@object     # @.str.1394
.L.str.1394:
	.asciz	"fflig"
	.size	.L.str.1394, 6

	.type	.L.str.1395,@object     # @.str.1395
.L.str.1395:
	.asciz	"hardcy"
	.size	.L.str.1395, 7

	.type	.L.str.1396,@object     # @.str.1396
.L.str.1396:
	.asciz	"nsub"
	.size	.L.str.1396, 5

	.type	.L.str.1397,@object     # @.str.1397
.L.str.1397:
	.asciz	"napprox"
	.size	.L.str.1397, 8

	.type	.L.str.1398,@object     # @.str.1398
.L.str.1398:
	.asciz	"caret"
	.size	.L.str.1398, 6

	.type	.L.str.1399,@object     # @.str.1399
.L.str.1399:
	.asciz	"succneqq"
	.size	.L.str.1399, 9

	.type	.L.str.1400,@object     # @.str.1400
.L.str.1400:
	.asciz	"frac13"
	.size	.L.str.1400, 7

	.type	.L.str.1401,@object     # @.str.1401
.L.str.1401:
	.asciz	"num"
	.size	.L.str.1401, 4

	.type	.L.str.1402,@object     # @.str.1402
.L.str.1402:
	.asciz	"emsp14"
	.size	.L.str.1402, 7

	.type	.L.str.1403,@object     # @.str.1403
.L.str.1403:
	.asciz	"sum"
	.size	.L.str.1403, 4

	.type	.L.str.1404,@object     # @.str.1404
.L.str.1404:
	.asciz	"Cconint"
	.size	.L.str.1404, 8

	.type	.L.str.1405,@object     # @.str.1405
.L.str.1405:
	.asciz	"blacktriangleright"
	.size	.L.str.1405, 19

	.type	.L.str.1406,@object     # @.str.1406
.L.str.1406:
	.asciz	"SupersetEqual"
	.size	.L.str.1406, 14

	.type	.L.str.1407,@object     # @.str.1407
.L.str.1407:
	.asciz	"sim"
	.size	.L.str.1407, 4

	.type	.L.str.1408,@object     # @.str.1408
.L.str.1408:
	.asciz	"LeftFloor"
	.size	.L.str.1408, 10

	.type	.L.str.1409,@object     # @.str.1409
.L.str.1409:
	.asciz	"RightTriangleEqual"
	.size	.L.str.1409, 19

	.type	.L.str.1410,@object     # @.str.1410
.L.str.1410:
	.asciz	"boxDr"
	.size	.L.str.1410, 6

	.type	.L.str.1411,@object     # @.str.1411
.L.str.1411:
	.asciz	"plus"
	.size	.L.str.1411, 5

	.type	.L.str.1412,@object     # @.str.1412
.L.str.1412:
	.asciz	"phiv"
	.size	.L.str.1412, 5

	.type	.L.str.1413,@object     # @.str.1413
.L.str.1413:
	.asciz	"npr"
	.size	.L.str.1413, 4

	.type	.L.str.1414,@object     # @.str.1414
.L.str.1414:
	.asciz	"RightCeiling"
	.size	.L.str.1414, 13

	.type	.L.str.1415,@object     # @.str.1415
.L.str.1415:
	.asciz	"phone"
	.size	.L.str.1415, 6

	.type	.L.str.1416,@object     # @.str.1416
.L.str.1416:
	.asciz	"mnplus"
	.size	.L.str.1416, 7

	.type	.L.str.1417,@object     # @.str.1417
.L.str.1417:
	.asciz	"vDash"
	.size	.L.str.1417, 6

	.type	.L.str.1418,@object     # @.str.1418
.L.str.1418:
	.asciz	"isindot"
	.size	.L.str.1418, 8

	.type	.L.str.1419,@object     # @.str.1419
.L.str.1419:
	.asciz	"PSgr"
	.size	.L.str.1419, 5

	.type	.L.str.1420,@object     # @.str.1420
.L.str.1420:
	.asciz	"ntriangleleft"
	.size	.L.str.1420, 14

	.type	.L.str.1421,@object     # @.str.1421
.L.str.1421:
	.asciz	"image"
	.size	.L.str.1421, 6

	.type	.L.str.1422,@object     # @.str.1422
.L.str.1422:
	.asciz	"boxh"
	.size	.L.str.1422, 5

	.type	.L.str.1423,@object     # @.str.1423
.L.str.1423:
	.asciz	"RightDownVector"
	.size	.L.str.1423, 16

	.type	.L.str.1424,@object     # @.str.1424
.L.str.1424:
	.asciz	"scpolint"
	.size	.L.str.1424, 9

	.type	.L.str.1425,@object     # @.str.1425
.L.str.1425:
	.asciz	"boxdL"
	.size	.L.str.1425, 6

	.type	.L.str.1426,@object     # @.str.1426
.L.str.1426:
	.asciz	"RightUpTeeVector"
	.size	.L.str.1426, 17

	.type	.L.str.1427,@object     # @.str.1427
.L.str.1427:
	.asciz	"TSHcy"
	.size	.L.str.1427, 6

	.type	.L.str.1428,@object     # @.str.1428
.L.str.1428:
	.asciz	"longmapsto"
	.size	.L.str.1428, 11

	.type	.L.str.1429,@object     # @.str.1429
.L.str.1429:
	.asciz	"doublebarwedge"
	.size	.L.str.1429, 15

	.type	.L.str.1430,@object     # @.str.1430
.L.str.1430:
	.asciz	"frasl"
	.size	.L.str.1430, 6

	.type	.L.str.1431,@object     # @.str.1431
.L.str.1431:
	.asciz	"DoubleLongRightArrow"
	.size	.L.str.1431, 21

	.type	.L.str.1432,@object     # @.str.1432
.L.str.1432:
	.asciz	"gnE"
	.size	.L.str.1432, 4

	.type	.L.str.1433,@object     # @.str.1433
.L.str.1433:
	.asciz	"lnE"
	.size	.L.str.1433, 4

	.type	.L.str.1434,@object     # @.str.1434
.L.str.1434:
	.asciz	"exponentiale"
	.size	.L.str.1434, 13

	.type	.L.str.1435,@object     # @.str.1435
.L.str.1435:
	.asciz	"larrfs"
	.size	.L.str.1435, 7

	.type	.L.str.1436,@object     # @.str.1436
.L.str.1436:
	.asciz	"NotDoubleVerticalBar"
	.size	.L.str.1436, 21

	.type	.L.str.1437,@object     # @.str.1437
.L.str.1437:
	.asciz	"curlywedge"
	.size	.L.str.1437, 11

	.type	.L.str.1438,@object     # @.str.1438
.L.str.1438:
	.asciz	"rarrfs"
	.size	.L.str.1438, 7

	.type	.L.str.1439,@object     # @.str.1439
.L.str.1439:
	.asciz	"quatint"
	.size	.L.str.1439, 8

	.type	.L.str.1440,@object     # @.str.1440
.L.str.1440:
	.asciz	"frac58"
	.size	.L.str.1440, 7

	.type	.L.str.1441,@object     # @.str.1441
.L.str.1441:
	.asciz	"gtreqless"
	.size	.L.str.1441, 10

	.type	.L.str.1442,@object     # @.str.1442
.L.str.1442:
	.asciz	"PrecedesSlantEqual"
	.size	.L.str.1442, 19

	.type	.L.str.1443,@object     # @.str.1443
.L.str.1443:
	.asciz	"DotEqual"
	.size	.L.str.1443, 9

	.type	.L.str.1444,@object     # @.str.1444
.L.str.1444:
	.asciz	"iquest"
	.size	.L.str.1444, 7

	.type	.L.str.1445,@object     # @.str.1445
.L.str.1445:
	.asciz	"NotEqual"
	.size	.L.str.1445, 9

	.type	.L.str.1446,@object     # @.str.1446
.L.str.1446:
	.asciz	"barwedge"
	.size	.L.str.1446, 9

	.type	.L.str.1447,@object     # @.str.1447
.L.str.1447:
	.asciz	"b.gammad"
	.size	.L.str.1447, 9

	.type	.L.str.1448,@object     # @.str.1448
.L.str.1448:
	.asciz	"block"
	.size	.L.str.1448, 6

	.type	.L.str.1449,@object     # @.str.1449
.L.str.1449:
	.asciz	"strns"
	.size	.L.str.1449, 6

	.type	.L.str.1450,@object     # @.str.1450
.L.str.1450:
	.asciz	"prod"
	.size	.L.str.1450, 5

	.type	.L.str.1451,@object     # @.str.1451
.L.str.1451:
	.asciz	"cupre"
	.size	.L.str.1451, 6

	.type	.L.str.1452,@object     # @.str.1452
.L.str.1452:
	.asciz	"SquareUnion"
	.size	.L.str.1452, 12

	.type	.L.str.1453,@object     # @.str.1453
.L.str.1453:
	.asciz	"Auml"
	.size	.L.str.1453, 5

	.type	.L.str.1454,@object     # @.str.1454
.L.str.1454:
	.asciz	"upuparrows"
	.size	.L.str.1454, 11

	.type	.L.str.1455,@object     # @.str.1455
.L.str.1455:
	.asciz	"Euml"
	.size	.L.str.1455, 5

	.type	.L.str.1456,@object     # @.str.1456
.L.str.1456:
	.asciz	"HorizontalLine"
	.size	.L.str.1456, 15

	.type	.L.str.1457,@object     # @.str.1457
.L.str.1457:
	.asciz	"Iuml"
	.size	.L.str.1457, 5

	.type	.L.str.1458,@object     # @.str.1458
.L.str.1458:
	.asciz	"nvrArr"
	.size	.L.str.1458, 7

	.type	.L.str.1459,@object     # @.str.1459
.L.str.1459:
	.asciz	"bullet"
	.size	.L.str.1459, 7

	.type	.L.str.1460,@object     # @.str.1460
.L.str.1460:
	.asciz	"ClockwiseContourIntegral"
	.size	.L.str.1460, 25

	.type	.L.str.1461,@object     # @.str.1461
.L.str.1461:
	.asciz	"prop"
	.size	.L.str.1461, 5

	.type	.L.str.1462,@object     # @.str.1462
.L.str.1462:
	.asciz	"Ouml"
	.size	.L.str.1462, 5

	.type	.L.str.1463,@object     # @.str.1463
.L.str.1463:
	.asciz	"chcy"
	.size	.L.str.1463, 5

	.type	.L.str.1464,@object     # @.str.1464
.L.str.1464:
	.asciz	"quaternions"
	.size	.L.str.1464, 12

	.type	.L.str.1465,@object     # @.str.1465
.L.str.1465:
	.asciz	"beth"
	.size	.L.str.1465, 5

	.type	.L.str.1466,@object     # @.str.1466
.L.str.1466:
	.asciz	"macr"
	.size	.L.str.1466, 5

	.type	.L.str.1467,@object     # @.str.1467
.L.str.1467:
	.asciz	"Uuml"
	.size	.L.str.1467, 5

	.type	.L.str.1468,@object     # @.str.1468
.L.str.1468:
	.asciz	"khcy"
	.size	.L.str.1468, 5

	.type	.L.str.1469,@object     # @.str.1469
.L.str.1469:
	.asciz	"Star"
	.size	.L.str.1469, 5

	.type	.L.str.1470,@object     # @.str.1470
.L.str.1470:
	.asciz	"copysr"
	.size	.L.str.1470, 7

	.type	.L.str.1471,@object     # @.str.1471
.L.str.1471:
	.asciz	"Yuml"
	.size	.L.str.1471, 5

	.type	.L.str.1472,@object     # @.str.1472
.L.str.1472:
	.asciz	"curren"
	.size	.L.str.1472, 7

	.type	.L.str.1473,@object     # @.str.1473
.L.str.1473:
	.asciz	"Square"
	.size	.L.str.1473, 7

	.type	.L.str.1474,@object     # @.str.1474
.L.str.1474:
	.asciz	"shcy"
	.size	.L.str.1474, 5

	.type	.L.str.1475,@object     # @.str.1475
.L.str.1475:
	.asciz	"auml"
	.size	.L.str.1475, 5

	.type	.L.str.1476,@object     # @.str.1476
.L.str.1476:
	.asciz	"boxUR"
	.size	.L.str.1476, 6

	.type	.L.str.1477,@object     # @.str.1477
.L.str.1477:
	.asciz	"uml"
	.size	.L.str.1477, 4

	.type	.L.str.1478,@object     # @.str.1478
.L.str.1478:
	.asciz	"euml"
	.size	.L.str.1478, 5

	.type	.L.str.1479,@object     # @.str.1479
.L.str.1479:
	.asciz	"zhcy"
	.size	.L.str.1479, 5

	.type	.L.str.1480,@object     # @.str.1480
.L.str.1480:
	.asciz	"iuml"
	.size	.L.str.1480, 5

	.type	.L.str.1481,@object     # @.str.1481
.L.str.1481:
	.asciz	"Backslash"
	.size	.L.str.1481, 10

	.type	.L.str.1482,@object     # @.str.1482
.L.str.1482:
	.asciz	"square"
	.size	.L.str.1482, 7

	.type	.L.str.1483,@object     # @.str.1483
.L.str.1483:
	.asciz	"sqsube"
	.size	.L.str.1483, 7

	.type	.L.str.1484,@object     # @.str.1484
.L.str.1484:
	.asciz	"ouml"
	.size	.L.str.1484, 5

	.type	.L.str.1485,@object     # @.str.1485
.L.str.1485:
	.asciz	"Tilde"
	.size	.L.str.1485, 6

	.type	.L.str.1486,@object     # @.str.1486
.L.str.1486:
	.asciz	"prap"
	.size	.L.str.1486, 5

	.type	.L.str.1487,@object     # @.str.1487
.L.str.1487:
	.asciz	"uuml"
	.size	.L.str.1487, 5

	.type	.L.str.1488,@object     # @.str.1488
.L.str.1488:
	.asciz	"cire"
	.size	.L.str.1488, 5

	.type	.L.str.1489,@object     # @.str.1489
.L.str.1489:
	.asciz	"dzigrarr"
	.size	.L.str.1489, 9

	.type	.L.str.1490,@object     # @.str.1490
.L.str.1490:
	.asciz	"yuml"
	.size	.L.str.1490, 5

	.type	.L.str.1491,@object     # @.str.1491
.L.str.1491:
	.asciz	"star"
	.size	.L.str.1491, 5

	.type	.L.str.1492,@object     # @.str.1492
.L.str.1492:
	.asciz	"sqsubseteq"
	.size	.L.str.1492, 11

	.type	.L.str.1493,@object     # @.str.1493
.L.str.1493:
	.asciz	"fork"
	.size	.L.str.1493, 5

	.type	.L.str.1494,@object     # @.str.1494
.L.str.1494:
	.asciz	"supsetneqq"
	.size	.L.str.1494, 11

	.type	.L.str.1495,@object     # @.str.1495
.L.str.1495:
	.asciz	"DoubleDownArrow"
	.size	.L.str.1495, 16

	.type	.L.str.1496,@object     # @.str.1496
.L.str.1496:
	.asciz	"dharr"
	.size	.L.str.1496, 6

	.type	.L.str.1497,@object     # @.str.1497
.L.str.1497:
	.asciz	"lagran"
	.size	.L.str.1497, 7

	.type	.L.str.1498,@object     # @.str.1498
.L.str.1498:
	.asciz	"lrhard"
	.size	.L.str.1498, 7

	.type	.L.str.1499,@object     # @.str.1499
.L.str.1499:
	.asciz	"downharpoonleft"
	.size	.L.str.1499, 16

	.type	.L.str.1500,@object     # @.str.1500
.L.str.1500:
	.asciz	"tilde"
	.size	.L.str.1500, 6

	.type	.L.str.1501,@object     # @.str.1501
.L.str.1501:
	.asciz	"nharr"
	.size	.L.str.1501, 6

	.type	.L.str.1502,@object     # @.str.1502
.L.str.1502:
	.asciz	"puncsp"
	.size	.L.str.1502, 7

	.type	.L.str.1503,@object     # @.str.1503
.L.str.1503:
	.asciz	"uharr"
	.size	.L.str.1503, 6

	.type	.L.str.1504,@object     # @.str.1504
.L.str.1504:
	.asciz	"boxHu"
	.size	.L.str.1504, 6

	.type	.L.str.1505,@object     # @.str.1505
.L.str.1505:
	.asciz	"models"
	.size	.L.str.1505, 7

	.type	.L.str.1506,@object     # @.str.1506
.L.str.1506:
	.asciz	"xharr"
	.size	.L.str.1506, 6

	.type	.L.str.1507,@object     # @.str.1507
.L.str.1507:
	.asciz	"nparallel"
	.size	.L.str.1507, 10

	.type	.L.str.1508,@object     # @.str.1508
.L.str.1508:
	.asciz	"Proportional"
	.size	.L.str.1508, 13

	.type	.L.str.1509,@object     # @.str.1509
.L.str.1509:
	.asciz	"ltrif"
	.size	.L.str.1509, 6

	.type	.L.str.1510,@object     # @.str.1510
.L.str.1510:
	.asciz	"dtrif"
	.size	.L.str.1510, 6

	.type	.L.str.1511,@object     # @.str.1511
.L.str.1511:
	.asciz	"subsetneqq"
	.size	.L.str.1511, 11

	.type	.L.str.1512,@object     # @.str.1512
.L.str.1512:
	.asciz	"NotGreaterEqual"
	.size	.L.str.1512, 16

	.type	.L.str.1513,@object     # @.str.1513
.L.str.1513:
	.asciz	"rtrif"
	.size	.L.str.1513, 6

	.type	.L.str.1514,@object     # @.str.1514
.L.str.1514:
	.asciz	"utrif"
	.size	.L.str.1514, 6

	.type	.L.str.1515,@object     # @.str.1515
.L.str.1515:
	.asciz	"boxvl"
	.size	.L.str.1515, 6

	.type	.L.str.1516,@object     # @.str.1516
.L.str.1516:
	.asciz	"curvearrowleft"
	.size	.L.str.1516, 15

	.type	.L.str.1517,@object     # @.str.1517
.L.str.1517:
	.asciz	"lessapprox"
	.size	.L.str.1517, 11

	.type	.L.str.1518,@object     # @.str.1518
.L.str.1518:
	.asciz	"scnE"
	.size	.L.str.1518, 5

	.type	.L.str.1519,@object     # @.str.1519
.L.str.1519:
	.asciz	"thetasym"
	.size	.L.str.1519, 9

	.type	.L.str.1520,@object     # @.str.1520
.L.str.1520:
	.asciz	"ExponentialE"
	.size	.L.str.1520, 13

	.type	.L.str.1521,@object     # @.str.1521
.L.str.1521:
	.asciz	"andv"
	.size	.L.str.1521, 5

	.type	.L.str.1522,@object     # @.str.1522
.L.str.1522:
	.asciz	"mapstodown"
	.size	.L.str.1522, 11

	.type	.L.str.1523,@object     # @.str.1523
.L.str.1523:
	.asciz	"DownLeftVector"
	.size	.L.str.1523, 15

	.type	.L.str.1524,@object     # @.str.1524
.L.str.1524:
	.asciz	"rightharpoonup"
	.size	.L.str.1524, 15

	.type	.L.str.1525,@object     # @.str.1525
.L.str.1525:
	.asciz	"mapsto"
	.size	.L.str.1525, 7

	.type	.L.str.1526,@object     # @.str.1526
.L.str.1526:
	.asciz	"Lang"
	.size	.L.str.1526, 5

	.type	.L.str.1527,@object     # @.str.1527
.L.str.1527:
	.asciz	"SquareSuperset"
	.size	.L.str.1527, 15

	.type	.L.str.1528,@object     # @.str.1528
.L.str.1528:
	.asciz	"rightsquigarrow"
	.size	.L.str.1528, 16

	.type	.L.str.1529,@object     # @.str.1529
.L.str.1529:
	.asciz	"Rang"
	.size	.L.str.1529, 5

	.type	.L.str.1530,@object     # @.str.1530
.L.str.1530:
	.asciz	"sigmav"
	.size	.L.str.1530, 7

	.type	.L.str.1531,@object     # @.str.1531
.L.str.1531:
	.asciz	"SquareIntersection"
	.size	.L.str.1531, 19

	.type	.L.str.1532,@object     # @.str.1532
.L.str.1532:
	.asciz	"downharpoonright"
	.size	.L.str.1532, 17

	.type	.L.str.1533,@object     # @.str.1533
.L.str.1533:
	.asciz	"lang"
	.size	.L.str.1533, 5

	.type	.L.str.1534,@object     # @.str.1534
.L.str.1534:
	.asciz	"compfn"
	.size	.L.str.1534, 7

	.type	.L.str.1535,@object     # @.str.1535
.L.str.1535:
	.asciz	"rang"
	.size	.L.str.1535, 5

	.type	.L.str.1536,@object     # @.str.1536
.L.str.1536:
	.asciz	"Leftrightarrow"
	.size	.L.str.1536, 15

	.type	.L.str.1537,@object     # @.str.1537
.L.str.1537:
	.asciz	"ReverseEquilibrium"
	.size	.L.str.1537, 19

	.type	.L.str.1538,@object     # @.str.1538
.L.str.1538:
	.asciz	"Rightarrow"
	.size	.L.str.1538, 11

	.type	.L.str.1539,@object     # @.str.1539
.L.str.1539:
	.asciz	"planck"
	.size	.L.str.1539, 7

	.type	.L.str.1540,@object     # @.str.1540
.L.str.1540:
	.asciz	"KHgr"
	.size	.L.str.1540, 5

	.type	.L.str.1541,@object     # @.str.1541
.L.str.1541:
	.asciz	"OHgr"
	.size	.L.str.1541, 5

	.type	.L.str.1542,@object     # @.str.1542
.L.str.1542:
	.asciz	"PHgr"
	.size	.L.str.1542, 5

	.type	.L.str.1543,@object     # @.str.1543
.L.str.1543:
	.asciz	"sqsup"
	.size	.L.str.1543, 6

	.type	.L.str.1544,@object     # @.str.1544
.L.str.1544:
	.asciz	"THgr"
	.size	.L.str.1544, 5

	.type	.L.str.1545,@object     # @.str.1545
.L.str.1545:
	.asciz	"rppolint"
	.size	.L.str.1545, 9

	.type	.L.str.1546,@object     # @.str.1546
.L.str.1546:
	.asciz	"gtrapprox"
	.size	.L.str.1546, 10

	.type	.L.str.1547,@object     # @.str.1547
.L.str.1547:
	.asciz	"LongLeftRightArrow"
	.size	.L.str.1547, 19

	.type	.L.str.1548,@object     # @.str.1548
.L.str.1548:
	.asciz	"iinfin"
	.size	.L.str.1548, 7

	.type	.L.str.1549,@object     # @.str.1549
.L.str.1549:
	.asciz	"frac16"
	.size	.L.str.1549, 7

	.type	.L.str.1550,@object     # @.str.1550
.L.str.1550:
	.asciz	"leftrightarrow"
	.size	.L.str.1550, 15

	.type	.L.str.1551,@object     # @.str.1551
.L.str.1551:
	.asciz	"rightarrow"
	.size	.L.str.1551, 11

	.type	.L.str.1552,@object     # @.str.1552
.L.str.1552:
	.asciz	"risingdotseq"
	.size	.L.str.1552, 13

	.type	.L.str.1553,@object     # @.str.1553
.L.str.1553:
	.asciz	"blacksquare"
	.size	.L.str.1553, 12

	.type	.L.str.1554,@object     # @.str.1554
.L.str.1554:
	.asciz	"DotDot"
	.size	.L.str.1554, 7

	.type	.L.str.1555,@object     # @.str.1555
.L.str.1555:
	.asciz	"radic"
	.size	.L.str.1555, 6

	.type	.L.str.1556,@object     # @.str.1556
.L.str.1556:
	.asciz	"nRightarrow"
	.size	.L.str.1556, 12

	.type	.L.str.1557,@object     # @.str.1557
.L.str.1557:
	.asciz	"reals"
	.size	.L.str.1557, 6

	.type	.L.str.1558,@object     # @.str.1558
.L.str.1558:
	.asciz	"varphi"
	.size	.L.str.1558, 7

	.type	.L.str.1559,@object     # @.str.1559
.L.str.1559:
	.asciz	"DownRightVector"
	.size	.L.str.1559, 16

	.type	.L.str.1560,@object     # @.str.1560
.L.str.1560:
	.asciz	"Equal"
	.size	.L.str.1560, 6

	.type	.L.str.1561,@object     # @.str.1561
.L.str.1561:
	.asciz	"NotCupCap"
	.size	.L.str.1561, 10

	.type	.L.str.1562,@object     # @.str.1562
.L.str.1562:
	.asciz	"SucceedsTilde"
	.size	.L.str.1562, 14

	.type	.L.str.1563,@object     # @.str.1563
.L.str.1563:
	.asciz	"Succeeds"
	.size	.L.str.1563, 9

	.type	.L.str.1564,@object     # @.str.1564
.L.str.1564:
	.asciz	"dotplus"
	.size	.L.str.1564, 8

	.type	.L.str.1565,@object     # @.str.1565
.L.str.1565:
	.asciz	"notniva"
	.size	.L.str.1565, 8

	.type	.L.str.1566,@object     # @.str.1566
.L.str.1566:
	.asciz	"looparrowright"
	.size	.L.str.1566, 15

	.type	.L.str.1567,@object     # @.str.1567
.L.str.1567:
	.asciz	"ggg"
	.size	.L.str.1567, 4

	.type	.L.str.1568,@object     # @.str.1568
.L.str.1568:
	.asciz	"EmptySmallSquare"
	.size	.L.str.1568, 17

	.type	.L.str.1569,@object     # @.str.1569
.L.str.1569:
	.asciz	"nrtri"
	.size	.L.str.1569, 6

	.type	.L.str.1570,@object     # @.str.1570
.L.str.1570:
	.asciz	"oS"
	.size	.L.str.1570, 3

	.type	.L.str.1571,@object     # @.str.1571
.L.str.1571:
	.asciz	"IEcy"
	.size	.L.str.1571, 5

	.type	.L.str.1572,@object     # @.str.1572
.L.str.1572:
	.asciz	"succeq"
	.size	.L.str.1572, 7

	.type	.L.str.1573,@object     # @.str.1573
.L.str.1573:
	.asciz	"vrtri"
	.size	.L.str.1573, 6

	.type	.L.str.1574,@object     # @.str.1574
.L.str.1574:
	.asciz	"DownTeeArrow"
	.size	.L.str.1574, 13

	.type	.L.str.1575,@object     # @.str.1575
.L.str.1575:
	.asciz	"frac45"
	.size	.L.str.1575, 7

	.type	.L.str.1576,@object     # @.str.1576
.L.str.1576:
	.asciz	"dblac"
	.size	.L.str.1576, 6

	.type	.L.str.1577,@object     # @.str.1577
.L.str.1577:
	.asciz	"blk14"
	.size	.L.str.1577, 6

	.type	.L.str.1578,@object     # @.str.1578
.L.str.1578:
	.asciz	"lesseqqgtr"
	.size	.L.str.1578, 11

	.type	.L.str.1579,@object     # @.str.1579
.L.str.1579:
	.asciz	"yucy"
	.size	.L.str.1579, 5

	.type	.L.str.1580,@object     # @.str.1580
.L.str.1580:
	.asciz	"yicy"
	.size	.L.str.1580, 5

	.type	.L.str.1581,@object     # @.str.1581
.L.str.1581:
	.asciz	"gne"
	.size	.L.str.1581, 4

	.type	.L.str.1582,@object     # @.str.1582
.L.str.1582:
	.asciz	"udhar"
	.size	.L.str.1582, 6

	.type	.L.str.1583,@object     # @.str.1583
.L.str.1583:
	.asciz	"UnionPlus"
	.size	.L.str.1583, 10

	.type	.L.str.1584,@object     # @.str.1584
.L.str.1584:
	.asciz	"blank"
	.size	.L.str.1584, 6

	.type	.L.str.1585,@object     # @.str.1585
.L.str.1585:
	.asciz	"lne"
	.size	.L.str.1585, 4

	.type	.L.str.1586,@object     # @.str.1586
.L.str.1586:
	.asciz	"NotTildeFullEqual"
	.size	.L.str.1586, 18

	.type	.L.str.1587,@object     # @.str.1587
.L.str.1587:
	.asciz	"supE"
	.size	.L.str.1587, 5

	.type	.L.str.1588,@object     # @.str.1588
.L.str.1588:
	.asciz	"PlusMinus"
	.size	.L.str.1588, 10

	.type	.L.str.1589,@object     # @.str.1589
.L.str.1589:
	.asciz	"GreaterTilde"
	.size	.L.str.1589, 13

	.type	.L.str.1590,@object     # @.str.1590
.L.str.1590:
	.asciz	"duarr"
	.size	.L.str.1590, 6

	.type	.L.str.1591,@object     # @.str.1591
.L.str.1591:
	.asciz	"LeftDoubleBracket"
	.size	.L.str.1591, 18

	.type	.L.str.1592,@object     # @.str.1592
.L.str.1592:
	.asciz	"CounterClockwiseContourIntegral"
	.size	.L.str.1592, 32

	.type	.L.str.1593,@object     # @.str.1593
.L.str.1593:
	.asciz	"natural"
	.size	.L.str.1593, 8

	.type	.L.str.1594,@object     # @.str.1594
.L.str.1594:
	.asciz	"uuarr"
	.size	.L.str.1594, 6

	.type	.L.str.1595,@object     # @.str.1595
.L.str.1595:
	.asciz	"biguplus"
	.size	.L.str.1595, 9

	.type	.L.str.1596,@object     # @.str.1596
.L.str.1596:
	.asciz	"DownLeftVectorBar"
	.size	.L.str.1596, 18

	.type	.L.str.1597,@object     # @.str.1597
.L.str.1597:
	.asciz	"subE"
	.size	.L.str.1597, 5

	.type	.L.str.1598,@object     # @.str.1598
.L.str.1598:
	.asciz	"part"
	.size	.L.str.1598, 5

	.type	.L.str.1599,@object     # @.str.1599
.L.str.1599:
	.asciz	"drcorn"
	.size	.L.str.1599, 7

	.type	.L.str.1600,@object     # @.str.1600
.L.str.1600:
	.asciz	"horbar"
	.size	.L.str.1600, 7

	.type	.L.str.1601,@object     # @.str.1601
.L.str.1601:
	.asciz	"sbsol"
	.size	.L.str.1601, 6

	.type	.L.str.1602,@object     # @.str.1602
.L.str.1602:
	.asciz	"varrho"
	.size	.L.str.1602, 7

	.type	.L.str.1603,@object     # @.str.1603
.L.str.1603:
	.asciz	"cudarrl"
	.size	.L.str.1603, 8

	.type	.L.str.1604,@object     # @.str.1604
.L.str.1604:
	.asciz	"boxul"
	.size	.L.str.1604, 6

	.type	.L.str.1605,@object     # @.str.1605
.L.str.1605:
	.asciz	"ordm"
	.size	.L.str.1605, 5

	.type	.L.str.1606,@object     # @.str.1606
.L.str.1606:
	.asciz	"urcorn"
	.size	.L.str.1606, 7

	.type	.L.str.1607,@object     # @.str.1607
.L.str.1607:
	.asciz	"dotminus"
	.size	.L.str.1607, 9

	.type	.L.str.1608,@object     # @.str.1608
.L.str.1608:
	.asciz	"scnsim"
	.size	.L.str.1608, 7

	.type	.L.str.1609,@object     # @.str.1609
.L.str.1609:
	.asciz	"andslope"
	.size	.L.str.1609, 9

	.type	.L.str.1610,@object     # @.str.1610
.L.str.1610:
	.asciz	"Cfr"
	.size	.L.str.1610, 4

	.type	.L.str.1611,@object     # @.str.1611
.L.str.1611:
	.asciz	"rightthreetimes"
	.size	.L.str.1611, 16

	.type	.L.str.1612,@object     # @.str.1612
.L.str.1612:
	.asciz	"Hfr"
	.size	.L.str.1612, 4

	.type	.L.str.1613,@object     # @.str.1613
.L.str.1613:
	.asciz	"sqsubset"
	.size	.L.str.1613, 9

	.type	.L.str.1614,@object     # @.str.1614
.L.str.1614:
	.asciz	"lAtail"
	.size	.L.str.1614, 7

	.type	.L.str.1615,@object     # @.str.1615
.L.str.1615:
	.asciz	"rightleftharpoons"
	.size	.L.str.1615, 18

	.type	.L.str.1616,@object     # @.str.1616
.L.str.1616:
	.asciz	"Ifr"
	.size	.L.str.1616, 4

	.type	.L.str.1617,@object     # @.str.1617
.L.str.1617:
	.asciz	"infintie"
	.size	.L.str.1617, 9

	.type	.L.str.1618,@object     # @.str.1618
.L.str.1618:
	.asciz	"rAtail"
	.size	.L.str.1618, 7

	.type	.L.str.1619,@object     # @.str.1619
.L.str.1619:
	.asciz	"scnap"
	.size	.L.str.1619, 6

	.type	.L.str.1620,@object     # @.str.1620
.L.str.1620:
	.asciz	"lharul"
	.size	.L.str.1620, 7

	.type	.L.str.1621,@object     # @.str.1621
.L.str.1621:
	.asciz	"Rfr"
	.size	.L.str.1621, 4

	.type	.L.str.1622,@object     # @.str.1622
.L.str.1622:
	.asciz	"parallel"
	.size	.L.str.1622, 9

	.type	.L.str.1623,@object     # @.str.1623
.L.str.1623:
	.asciz	"rharul"
	.size	.L.str.1623, 7

	.type	.L.str.1624,@object     # @.str.1624
.L.str.1624:
	.asciz	"Zfr"
	.size	.L.str.1624, 4

	.type	.L.str.1625,@object     # @.str.1625
.L.str.1625:
	.asciz	"RuleDelayed"
	.size	.L.str.1625, 12

	.type	.L.str.1626,@object     # @.str.1626
.L.str.1626:
	.asciz	"RightArrowLeftArrow"
	.size	.L.str.1626, 20

	.type	.L.str.1627,@object     # @.str.1627
.L.str.1627:
	.asciz	"profsurf"
	.size	.L.str.1627, 9

	.type	.L.str.1628,@object     # @.str.1628
.L.str.1628:
	.asciz	"nsupseteq"
	.size	.L.str.1628, 10

	.type	.L.str.1629,@object     # @.str.1629
.L.str.1629:
	.asciz	"nearhk"
	.size	.L.str.1629, 7

	.type	.L.str.1630,@object     # @.str.1630
.L.str.1630:
	.asciz	"triangledown"
	.size	.L.str.1630, 13

	.type	.L.str.1631,@object     # @.str.1631
.L.str.1631:
	.asciz	"searhk"
	.size	.L.str.1631, 7

	.type	.L.str.1632,@object     # @.str.1632
.L.str.1632:
	.asciz	"empty"
	.size	.L.str.1632, 6

	.type	.L.str.1633,@object     # @.str.1633
.L.str.1633:
	.asciz	"DoubleUpArrow"
	.size	.L.str.1633, 14

	.type	.L.str.1634,@object     # @.str.1634
.L.str.1634:
	.asciz	"checkmark"
	.size	.L.str.1634, 10

	.type	.L.str.1635,@object     # @.str.1635
.L.str.1635:
	.asciz	"succapprox"
	.size	.L.str.1635, 11

	.type	.L.str.1636,@object     # @.str.1636
.L.str.1636:
	.asciz	"Cap"
	.size	.L.str.1636, 4

	.type	.L.str.1637,@object     # @.str.1637
.L.str.1637:
	.asciz	"RightTriangle"
	.size	.L.str.1637, 14

	.type	.L.str.1638,@object     # @.str.1638
.L.str.1638:
	.asciz	"tosa"
	.size	.L.str.1638, 5

	.type	.L.str.1639,@object     # @.str.1639
.L.str.1639:
	.asciz	"RightUpVector"
	.size	.L.str.1639, 14

	.type	.L.str.1640,@object     # @.str.1640
.L.str.1640:
	.asciz	"boxVL"
	.size	.L.str.1640, 6

	.type	.L.str.1641,@object     # @.str.1641
.L.str.1641:
	.asciz	"ecolon"
	.size	.L.str.1641, 7

	.type	.L.str.1642,@object     # @.str.1642
.L.str.1642:
	.asciz	"Map"
	.size	.L.str.1642, 4

	.type	.L.str.1643,@object     # @.str.1643
.L.str.1643:
	.asciz	"lbrack"
	.size	.L.str.1643, 7

	.type	.L.str.1644,@object     # @.str.1644
.L.str.1644:
	.asciz	"THORN"
	.size	.L.str.1644, 6

	.type	.L.str.1645,@object     # @.str.1645
.L.str.1645:
	.asciz	"DownArrowUpArrow"
	.size	.L.str.1645, 17

	.type	.L.str.1646,@object     # @.str.1646
.L.str.1646:
	.asciz	"rbrack"
	.size	.L.str.1646, 7

	.type	.L.str.1647,@object     # @.str.1647
.L.str.1647:
	.asciz	"DiacriticalTilde"
	.size	.L.str.1647, 17

	.type	.L.str.1648,@object     # @.str.1648
.L.str.1648:
	.asciz	"amp"
	.size	.L.str.1648, 4

	.type	.L.str.1649,@object     # @.str.1649
.L.str.1649:
	.asciz	"rlhar2"
	.size	.L.str.1649, 7

	.type	.L.str.1650,@object     # @.str.1650
.L.str.1650:
	.asciz	"bcong"
	.size	.L.str.1650, 6

	.type	.L.str.1651,@object     # @.str.1651
.L.str.1651:
	.asciz	"exist"
	.size	.L.str.1651, 6

	.type	.L.str.1652,@object     # @.str.1652
.L.str.1652:
	.asciz	"ord"
	.size	.L.str.1652, 4

	.type	.L.str.1653,@object     # @.str.1653
.L.str.1653:
	.asciz	"cap"
	.size	.L.str.1653, 4

	.type	.L.str.1654,@object     # @.str.1654
.L.str.1654:
	.asciz	"eeacgr"
	.size	.L.str.1654, 7

	.type	.L.str.1655,@object     # @.str.1655
.L.str.1655:
	.asciz	"PrecedesTilde"
	.size	.L.str.1655, 14

	.type	.L.str.1656,@object     # @.str.1656
.L.str.1656:
	.asciz	"divideontimes"
	.size	.L.str.1656, 14

	.type	.L.str.1657,@object     # @.str.1657
.L.str.1657:
	.asciz	"Precedes"
	.size	.L.str.1657, 9

	.type	.L.str.1658,@object     # @.str.1658
.L.str.1658:
	.asciz	"UTF8"
	.size	.L.str.1658, 5

	.type	.L.str.1659,@object     # @.str.1659
.L.str.1659:
	.asciz	"ISO-10646/UTF-8"
	.size	.L.str.1659, 16

	.type	.L.str.1660,@object     # @.str.1660
.L.str.1660:
	.asciz	"UTF-16"
	.size	.L.str.1660, 7

	.type	.L.str.1661,@object     # @.str.1661
.L.str.1661:
	.asciz	"UTF16LE"
	.size	.L.str.1661, 8

	.type	.L.str.1662,@object     # @.str.1662
.L.str.1662:
	.asciz	"UTF-32"
	.size	.L.str.1662, 7

	.type	.L.str.1663,@object     # @.str.1663
.L.str.1663:
	.asciz	"10646-1:1993/UCS4"
	.size	.L.str.1663, 18

	.type	.L.str.1664,@object     # @.str.1664
.L.str.1664:
	.asciz	"UTF-16LE"
	.size	.L.str.1664, 9

	.type	.L.str.1665,@object     # @.str.1665
.L.str.1665:
	.asciz	"UCS-4LE"
	.size	.L.str.1665, 8

	.type	.L.str.1666,@object     # @.str.1666
.L.str.1666:
	.asciz	"UCS-4"
	.size	.L.str.1666, 6

	.type	.L.str.1667,@object     # @.str.1667
.L.str.1667:
	.asciz	"UCS2"
	.size	.L.str.1667, 5

	.type	.L.str.1668,@object     # @.str.1668
.L.str.1668:
	.asciz	"UTF-16BE"
	.size	.L.str.1668, 9

	.type	.L.str.1669,@object     # @.str.1669
.L.str.1669:
	.asciz	"UTF-32LE"
	.size	.L.str.1669, 9

	.type	.L.str.1670,@object     # @.str.1670
.L.str.1670:
	.asciz	"UTF16BE"
	.size	.L.str.1670, 8

	.type	.L.str.1671,@object     # @.str.1671
.L.str.1671:
	.asciz	"UTF32"
	.size	.L.str.1671, 6

	.type	.L.str.1672,@object     # @.str.1672
.L.str.1672:
	.asciz	"UTF-32BE"
	.size	.L.str.1672, 9

	.type	.L.str.1673,@object     # @.str.1673
.L.str.1673:
	.asciz	"UTF32LE"
	.size	.L.str.1673, 8

	.type	.L.str.1674,@object     # @.str.1674
.L.str.1674:
	.asciz	"UCS-4BE"
	.size	.L.str.1674, 8

	.type	.L.str.1675,@object     # @.str.1675
.L.str.1675:
	.asciz	"ISO-10646/UCS2"
	.size	.L.str.1675, 15

	.type	.L.str.1676,@object     # @.str.1676
.L.str.1676:
	.asciz	"10646-1:1993"
	.size	.L.str.1676, 13

	.type	.L.str.1677,@object     # @.str.1677
.L.str.1677:
	.asciz	"ISO-10646/UCS4"
	.size	.L.str.1677, 15

	.type	.L.str.1678,@object     # @.str.1678
.L.str.1678:
	.asciz	"ISO-10646"
	.size	.L.str.1678, 10

	.type	.L.str.1679,@object     # @.str.1679
.L.str.1679:
	.asciz	"UTF-8"
	.size	.L.str.1679, 6

	.type	.L.str.1680,@object     # @.str.1680
.L.str.1680:
	.asciz	"UTF32BE"
	.size	.L.str.1680, 8

	.type	.L.str.1681,@object     # @.str.1681
.L.str.1681:
	.asciz	"ISO-10646/UTF8"
	.size	.L.str.1681, 15

	.type	.L.str.1682,@object     # @.str.1682
.L.str.1682:
	.asciz	"UCS4"
	.size	.L.str.1682, 5

	.type	.L.str.1683,@object     # @.str.1683
.L.str.1683:
	.asciz	"No HTML stream\n"
	.size	.L.str.1683, 16

	.type	.L.str.1684,@object     # @.str.1684
.L.str.1684:
	.asciz	"Error while reading HTML stream\n"
	.size	.L.str.1684, 33

	.type	.L.str.1685,@object     # @.str.1685
.L.str.1685:
	.asciz	"EBCDIC-US"
	.size	.L.str.1685, 10

	.type	.L.str.1686,@object     # @.str.1686
.L.str.1686:
	.asciz	"!Unable to get TLS iconv cache!\n"
	.size	.L.str.1686, 33

	.type	.L.str.1687,@object     # @.str.1687
.L.str.1687:
	.asciz	"iconv not found in cache, for encoding:%s\n"
	.size	.L.str.1687, 43

	.type	.L.str.1688,@object     # @.str.1688
.L.str.1688:
	.asciz	"!Out of mem in iconv-pool\n"
	.size	.L.str.1688, 27

	.type	.L.str.1689,@object     # @.str.1689
.L.str.1689:
	.asciz	"iconv_open(),for:%s -> %p\n"
	.size	.L.str.1689, 27

	.type	iconv_global_inited,@object # @iconv_global_inited
	.local	iconv_global_inited
	.comm	iconv_global_inited,1,4
	.type	global_iconv_cache,@object # @global_iconv_cache
	.local	global_iconv_cache
	.comm	global_iconv_cache,8,8
	.type	.L.str.1690,@object     # @.str.1690
.L.str.1690:
	.asciz	"Initializing iconv pool:%p\n"
	.size	.L.str.1690, 28

	.type	.L.str.1691,@object     # @.str.1691
.L.str.1691:
	.asciz	"Destroying iconv pool:%p\n"
	.size	.L.str.1691, 26

	.type	.L.str.1692,@object     # @.str.1692
.L.str.1692:
	.asciz	"closing iconv:%p\n"
	.size	.L.str.1692, 18

	.type	.L.str.1693,@object     # @.str.1693
.L.str.1693:
	.asciz	"Warning: unicode character out of utf16 range!\n"
	.size	.L.str.1693, 48

	.type	.L.str.1694,@object     # @.str.1694
.L.str.1694:
	.asciz	"invalid UTF8 character encountered\n"
	.size	.L.str.1694, 36

	.type	.L.str.1695,@object     # @.str.1695
.L.str.1695:
	.asciz	"UTF8 character out of UTF16 range encountered"
	.size	.L.str.1695, 46

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.quad	4                       # 0x4
	.quad	2                       # 0x2
	.quad	4                       # 0x4
	.quad	4                       # 0x4
	.quad	4                       # 0x4
	.quad	4                       # 0x4
	.quad	2                       # 0x2
	.quad	2                       # 0x2
	.size	.Lswitch.table, 64


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
