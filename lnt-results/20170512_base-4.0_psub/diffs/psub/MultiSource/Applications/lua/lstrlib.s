	.text
	.file	"lstrlib.bc"
	.globl	luaopen_string
	.p2align	4, 0x90
	.type	luaopen_string,@function
luaopen_string:                         # @luaopen_string
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$.L.str, %esi
	movl	$strlib, %edx
	callq	luaL_register
	movl	$-1, %esi
	movl	$.L.str.1, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-2, %esi
	movl	$.L.str.2, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_createtable
	movl	$.L.str.38, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushlstring
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_setmetatable
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$-2, %esi
	movl	$.L.str.39, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	luaopen_string, .Lfunc_end0-luaopen_string
	.cfi_endproc

	.p2align	4, 0x90
	.type	str_byte,@function
str_byte:                               # @str_byte
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 64
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movq	%rsp, %rdx
	movl	$1, %esi
	callq	luaL_checklstring
	movq	%rax, %r14
	movl	$1, %r15d
	movl	$2, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	callq	luaL_optinteger
	movq	(%rsp), %rcx
	incq	%rcx
	movq	%rax, %rbp
	sarq	$63, %rbp
	andq	%rcx, %rbp
	xorl	%r12d, %r12d
	addq	%rax, %rbp
	movl	$0, %ebx
	cmovnsq	%rbp, %rbx
	movl	$3, %esi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	luaL_optinteger
	movq	(%rsp), %rdx
	leaq	1(%rdx), %rsi
	movq	%rax, %rcx
	sarq	$63, %rcx
	andq	%rsi, %rcx
	addq	%rax, %rcx
	cmovsq	%r12, %rcx
	cmpq	$1, %rbx
	cmovleq	%r15, %rbp
	cmpq	%rdx, %rcx
	cmovaq	%rdx, %rcx
	movq	%rcx, %r15
	subq	%rbp, %r15
	jl	.LBB1_7
# BB#1:
	incq	%r15
	movslq	%r15d, %rax
	addq	%rbp, %rax
	cmpq	%rcx, %rax
	jg	.LBB1_3
# BB#2:
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	luaL_error
.LBB1_3:
	movl	$.L.str.16, %edx
	movq	%r13, %rdi
	movl	%r15d, %esi
	callq	luaL_checkstack
	testl	%r15d, %r15d
	jle	.LBB1_6
# BB#4:                                 # %.lr.ph
	movslq	%r15d, %r12
	leaq	-1(%r14,%rbp), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_5:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp,%rbx), %esi
	movq	%r13, %rdi
	callq	lua_pushinteger
	incq	%rbx
	cmpq	%r12, %rbx
	jl	.LBB1_5
.LBB1_6:
	movl	%r15d, %r12d
.LBB1_7:                                # %.loopexit
	movl	%r12d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	str_byte, .Lfunc_end1-str_byte
	.cfi_endproc

	.p2align	4, 0x90
	.type	str_char,@function
str_char:                               # @str_char
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$8216, %rsp             # imm = 0x2018
.Lcfi21:
	.cfi_def_cfa_offset 8272
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	callq	lua_gettop
	movl	%eax, %r14d
	movq	%rsp, %rsi
	movq	%r12, %rdi
	callq	luaL_buffinit
	testl	%r14d, %r14d
	jle	.LBB2_7
# BB#1:                                 # %.lr.ph
	xorl	%ebp, %ebp
	leaq	8216(%rsp), %r13
	movq	%rsp, %r15
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	incl	%ebp
	movq	%r12, %rdi
	movl	%ebp, %esi
	callq	luaL_checkinteger
	movq	%rax, %rbx
	movzbl	%bl, %eax
	cmpl	%ebx, %eax
	je	.LBB2_4
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	movl	$.L.str.17, %edx
	movq	%r12, %rdi
	movl	%ebp, %esi
	callq	luaL_argerror
.LBB2_4:                                #   in Loop: Header=BB2_2 Depth=1
	movq	(%rsp), %rax
	cmpq	%r13, %rax
	jb	.LBB2_6
# BB#5:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	%r15, %rdi
	callq	luaL_prepbuffer
	movq	(%rsp), %rax
.LBB2_6:                                #   in Loop: Header=BB2_2 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rsp)
	movb	%bl, (%rax)
	cmpl	%ebp, %r14d
	jne	.LBB2_2
.LBB2_7:                                # %._crit_edge
	movq	%rsp, %rdi
	callq	luaL_pushresult
	movl	$1, %eax
	addq	$8216, %rsp             # imm = 0x2018
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	str_char, .Lfunc_end2-str_char
	.cfi_endproc

	.p2align	4, 0x90
	.type	str_dump,@function
str_dump:                               # @str_dump
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 24
	subq	$8216, %rsp             # imm = 0x2018
.Lcfi30:
	.cfi_def_cfa_offset 8240
.Lcfi31:
	.cfi_offset %rbx, -24
.Lcfi32:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	movl	$6, %edx
	callq	luaL_checktype
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movq	%rsp, %r14
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	luaL_buffinit
	movl	$writer, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	lua_dump
	testl	%eax, %eax
	je	.LBB3_2
# BB#1:
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
.LBB3_2:
	movq	%rsp, %rdi
	callq	luaL_pushresult
	movl	$1, %eax
	addq	$8216, %rsp             # imm = 0x2018
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	str_dump, .Lfunc_end3-str_dump
	.cfi_endproc

	.p2align	4, 0x90
	.type	str_find,@function
str_find:                               # @str_find
	.cfi_startproc
# BB#0:
	movl	$1, %esi
	jmp	str_find_aux            # TAILCALL
.Lfunc_end4:
	.size	str_find, .Lfunc_end4-str_find
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4890909195324358656     # double 9.2233720368547758E+18
	.text
	.p2align	4, 0x90
	.type	str_format,@function
str_format:                             # @str_format
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 56
	subq	$8792, %rsp             # imm = 0x2258
.Lcfi39:
	.cfi_def_cfa_offset 8848
.Lcfi40:
	.cfi_offset %rbx, -56
.Lcfi41:
	.cfi_offset %r12, -48
.Lcfi42:
	.cfi_offset %r13, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movl	$1, %ebx
	leaq	56(%rsp), %rdx
	movl	$1, %esi
	callq	luaL_checklstring
	movq	%rax, %r15
	movq	56(%rsp), %r12
	leaq	576(%rsp), %rsi
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%rbp, %rdi
	callq	luaL_buffinit
	testq	%r12, %r12
	jle	.LBB5_11
# BB#1:                                 # %.lr.ph.lr.ph
	addq	%r15, %r12
	leaq	8792(%rsp), %r14
	movl	$1, 12(%rsp)            # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB5_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_15 Depth 2
                                        #     Child Loop BB5_34 Depth 2
	movb	(%r15), %al
	cmpb	$37, %al
	jne	.LBB5_3
# BB#6:                                 #   in Loop: Header=BB5_2 Depth=1
	leaq	1(%r15), %r13
	movb	1(%r15), %bpl
	cmpb	$37, %bpl
	jne	.LBB5_12
# BB#7:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	576(%rsp), %rax
	movb	$37, %cl
	cmpq	%r14, %rax
	jb	.LBB5_9
# BB#8:                                 #   in Loop: Header=BB5_2 Depth=1
	leaq	576(%rsp), %rdi
	callq	luaL_prepbuffer
	movb	(%r13), %cl
	movq	576(%rsp), %rax
.LBB5_9:                                #   in Loop: Header=BB5_2 Depth=1
	addq	$2, %r15
	leaq	1(%rax), %rdx
	movq	%rdx, 576(%rsp)
	movb	%cl, (%rax)
	cmpq	%r12, %r15
	jb	.LBB5_2
	jmp	.LBB5_11
	.p2align	4, 0x90
.LBB5_3:                                #   in Loop: Header=BB5_2 Depth=1
	movq	576(%rsp), %rcx
	cmpq	%r14, %rcx
	jb	.LBB5_5
# BB#4:                                 #   in Loop: Header=BB5_2 Depth=1
	leaq	576(%rsp), %rdi
	callq	luaL_prepbuffer
	movb	(%r15), %al
	movq	576(%rsp), %rcx
.LBB5_5:                                #   in Loop: Header=BB5_2 Depth=1
	incq	%r15
	leaq	1(%rcx), %rdx
	movq	%rdx, 576(%rsp)
	movb	%al, (%rcx)
	cmpq	%r12, %r15
	jb	.LBB5_2
	jmp	.LBB5_11
.LBB5_12:                               #   in Loop: Header=BB5_2 Depth=1
	testb	%bpl, %bpl
	je	.LBB5_13
# BB#14:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	%r13, %rbx
	movabsq	$325494096527361, %rdx  # imm = 0x1280900000001
	.p2align	4, 0x90
.LBB5_15:                               # %.lr.ph.i
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%bpl, %rax
	movl	%eax, %ecx
	movl	$1, %eax
	shlq	%cl, %rax
	cmpq	$63, %rcx
	ja	.LBB5_19
# BB#16:                                # %.lr.ph.i
                                        #   in Loop: Header=BB5_15 Depth=2
	andq	%rdx, %rax
	je	.LBB5_19
# BB#17:                                #   in Loop: Header=BB5_15 Depth=2
	movb	1(%rbx), %bpl
	incq	%rbx
	testb	%bpl, %bpl
	jne	.LBB5_15
# BB#18:                                #   in Loop: Header=BB5_2 Depth=1
	xorl	%ebp, %ebp
	jmp	.LBB5_19
.LBB5_13:                               #   in Loop: Header=BB5_2 Depth=1
	xorl	%ebp, %ebp
	movq	%r13, %rbx
.LBB5_19:                               # %.critedge.i
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	%rbx, %rax
	subq	%r13, %rax
	cmpq	$6, %rax
	jb	.LBB5_21
# BB#20:                                #   in Loop: Header=BB5_2 Depth=1
	movl	$.L.str.30, %esi
	xorl	%eax, %eax
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	luaL_error
	movb	(%rbx), %bpl
.LBB5_21:                               #   in Loop: Header=BB5_2 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movzbl	%bpl, %ecx
	leaq	1(%rbx), %rdx
	testb	$8, 1(%rax,%rcx,2)
	cmoveq	%rbx, %rdx
	movzbl	(%rdx), %ecx
	testb	$8, 1(%rax,%rcx,2)
	leaq	1(%rdx), %r15
	cmoveq	%rdx, %r15
	movb	(%r15), %cl
	cmpb	$46, %cl
	jne	.LBB5_23
# BB#22:                                #   in Loop: Header=BB5_2 Depth=1
	movzbl	1(%r15), %ecx
	leaq	1(%r15), %rdx
	addq	$2, %r15
	testb	$8, 1(%rax,%rcx,2)
	cmoveq	%rdx, %r15
	movzbl	(%r15), %ecx
	leaq	1(%r15), %rdx
	testb	$8, 1(%rax,%rcx,2)
	cmovneq	%rdx, %r15
	movb	(%r15), %cl
.LBB5_23:                               #   in Loop: Header=BB5_2 Depth=1
	movzbl	%cl, %ecx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB5_25
# BB#24:                                #   in Loop: Header=BB5_2 Depth=1
	movl	$.L.str.31, %esi
	xorl	%eax, %eax
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	luaL_error
.LBB5_25:                               # %scanformat.exit
                                        #   in Loop: Header=BB5_2 Depth=1
	movb	$37, 16(%rsp)
	movq	%r15, %rbx
	subq	%r13, %rbx
	leaq	1(%rbx), %rdx
	leaq	17(%rsp), %rdi
	movq	%r13, %rsi
	callq	strncpy
	movb	$0, 18(%rsp,%rbx)
	movsbl	(%r15), %edx
	leal	-69(%rdx), %eax
	cmpl	$51, %eax
	movl	12(%rsp), %ebx          # 4-byte Reload
	ja	.LBB5_60
# BB#26:                                # %scanformat.exit
                                        #   in Loop: Header=BB5_2 Depth=1
	incl	%ebx
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	jmpq	*.LJTI5_0(,%rax,8)
.LBB5_30:                               #   in Loop: Header=BB5_2 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%ebx, %esi
	callq	luaL_checknumber
	movb	$1, %al
	leaq	64(%rsp), %rbp
	movq	%rbp, %rdi
	leaq	16(%rsp), %rsi
	callq	sprintf
	jmp	.LBB5_57
.LBB5_29:                               #   in Loop: Header=BB5_2 Depth=1
	leaq	16(%rsp), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movb	15(%rsp,%rax), %cl
	movw	$108, 15(%rsp,%rax)
	movb	%cl, 16(%rsp,%rax)
	movb	$0, 17(%rsp,%rax)
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%ebx, %esi
	callq	luaL_checknumber
	movapd	%xmm0, %xmm1
	movsd	.LCPI5_0(%rip), %xmm2   # xmm2 = mem[0],zero
	subsd	%xmm2, %xmm1
	cvttsd2si	%xmm1, %rax
	movabsq	$-9223372036854775808, %rcx # imm = 0x8000000000000000
	xorq	%rcx, %rax
	cvttsd2si	%xmm0, %rdx
	ucomisd	%xmm2, %xmm0
	cmovaeq	%rax, %rdx
	jmp	.LBB5_28
.LBB5_27:                               #   in Loop: Header=BB5_2 Depth=1
	leaq	16(%rsp), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movb	15(%rsp,%rax), %cl
	movw	$108, 15(%rsp,%rax)
	movb	%cl, 16(%rsp,%rax)
	movb	$0, 17(%rsp,%rax)
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%ebx, %esi
	callq	luaL_checknumber
	cvttsd2si	%xmm0, %rdx
.LBB5_28:                               #   in Loop: Header=BB5_2 Depth=1
	xorl	%eax, %eax
	leaq	64(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	sprintf
	movq	%rbx, %rbp
.LBB5_57:                               #   in Loop: Header=BB5_2 Depth=1
	movl	$1, %ebx
	movq	%rbp, %rdi
	callq	strlen
	leaq	576(%rsp), %rdi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	luaL_addlstring
	incq	%r15
	cmpq	%r12, %r15
	jb	.LBB5_2
	jmp	.LBB5_11
.LBB5_56:                               #   in Loop: Header=BB5_2 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%ebx, %esi
	callq	luaL_checknumber
	cvttsd2si	%xmm0, %edx
	xorl	%eax, %eax
	leaq	64(%rsp), %rbp
	movq	%rbp, %rdi
	leaq	16(%rsp), %rsi
	callq	sprintf
	jmp	.LBB5_57
.LBB5_31:                               #   in Loop: Header=BB5_2 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%ebx, %esi
	leaq	48(%rsp), %rdx
	callq	luaL_checklstring
	movq	%rax, %rbx
	movq	576(%rsp), %rax
	cmpq	%r14, %rax
	jb	.LBB5_33
# BB#32:                                #   in Loop: Header=BB5_2 Depth=1
	leaq	576(%rsp), %rdi
	callq	luaL_prepbuffer
	movq	576(%rsp), %rax
.LBB5_33:                               #   in Loop: Header=BB5_2 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, 576(%rsp)
	movb	$34, (%rax)
	jmp	.LBB5_34
	.p2align	4, 0x90
.LBB5_41:                               #   in Loop: Header=BB5_34 Depth=2
	movzbl	(%rbx), %ecx
	leaq	1(%rax), %rdx
	movq	%rdx, 576(%rsp)
	movb	%cl, (%rax)
	incq	%rbx
.LBB5_34:                               #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	48(%rsp), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, 48(%rsp)
	testq	%rax, %rax
	je	.LBB5_49
# BB#35:                                # %.lr.ph.i46
                                        #   in Loop: Header=BB5_34 Depth=2
	movzbl	(%rbx), %eax
	movsbl	%al, %ecx
	cmpl	$34, %ecx
	ja	.LBB5_36
# BB#59:                                # %.lr.ph.i46
                                        #   in Loop: Header=BB5_34 Depth=2
	jmpq	*.LJTI5_1(,%rcx,8)
.LBB5_44:                               #   in Loop: Header=BB5_34 Depth=2
	movl	$.L.str.34, %esi
	movl	$4, %edx
	jmp	.LBB5_43
.LBB5_36:                               # %.lr.ph.i46
                                        #   in Loop: Header=BB5_34 Depth=2
	cmpl	$92, %ecx
	jne	.LBB5_45
.LBB5_37:                               #   in Loop: Header=BB5_34 Depth=2
	movq	576(%rsp), %rax
	cmpq	%r14, %rax
	jb	.LBB5_39
# BB#38:                                #   in Loop: Header=BB5_34 Depth=2
	leaq	576(%rsp), %rdi
	callq	luaL_prepbuffer
	movq	576(%rsp), %rax
.LBB5_39:                               #   in Loop: Header=BB5_34 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, 576(%rsp)
	movb	$92, (%rax)
	movq	576(%rsp), %rax
	cmpq	%r14, %rax
	jb	.LBB5_41
# BB#40:                                #   in Loop: Header=BB5_34 Depth=2
	leaq	576(%rsp), %rdi
	callq	luaL_prepbuffer
	movq	576(%rsp), %rax
	jmp	.LBB5_41
.LBB5_45:                               #   in Loop: Header=BB5_34 Depth=2
	movq	576(%rsp), %rcx
	cmpq	%r14, %rcx
	jb	.LBB5_47
# BB#46:                                #   in Loop: Header=BB5_34 Depth=2
	leaq	576(%rsp), %rdi
	callq	luaL_prepbuffer
	movzbl	(%rbx), %eax
	movq	576(%rsp), %rcx
.LBB5_47:                               #   in Loop: Header=BB5_34 Depth=2
	leaq	1(%rcx), %rdx
	movq	%rdx, 576(%rsp)
	movb	%al, (%rcx)
	incq	%rbx
	jmp	.LBB5_34
.LBB5_42:                               #   in Loop: Header=BB5_34 Depth=2
	movl	$.L.str.33, %esi
	movl	$2, %edx
.LBB5_43:                               #   in Loop: Header=BB5_34 Depth=2
	leaq	576(%rsp), %rdi
	callq	luaL_addlstring
	incq	%rbx
	jmp	.LBB5_34
.LBB5_52:                               #   in Loop: Header=BB5_2 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%ebx, %esi
	leaq	48(%rsp), %rdx
	callq	luaL_checklstring
	movq	%rax, %rbx
	movl	$46, %esi
	leaq	16(%rsp), %rdi
	callq	strchr
	testq	%rax, %rax
	jne	.LBB5_54
# BB#53:                                #   in Loop: Header=BB5_2 Depth=1
	cmpq	$99, 48(%rsp)
	jbe	.LBB5_54
# BB#55:                                #   in Loop: Header=BB5_2 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
	callq	lua_pushvalue
	leaq	576(%rsp), %rdi
	callq	luaL_addvalue
	movl	$1, %ebx
	incq	%r15
	cmpq	%r12, %r15
	jb	.LBB5_2
	jmp	.LBB5_11
.LBB5_49:                               # %._crit_edge.i
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	576(%rsp), %rax
	cmpq	%r14, %rax
	jb	.LBB5_51
# BB#50:                                #   in Loop: Header=BB5_2 Depth=1
	leaq	576(%rsp), %rdi
	callq	luaL_prepbuffer
	movq	576(%rsp), %rax
.LBB5_51:                               # %addquoted.exit
                                        #   in Loop: Header=BB5_2 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, 576(%rsp)
	movb	$34, (%rax)
	movl	$1, %ebx
	incq	%r15
	cmpq	%r12, %r15
	jb	.LBB5_2
	jmp	.LBB5_11
.LBB5_54:                               # %.thread
                                        #   in Loop: Header=BB5_2 Depth=1
	xorl	%eax, %eax
	leaq	64(%rsp), %rbp
	movq	%rbp, %rdi
	leaq	16(%rsp), %rsi
	movq	%rbx, %rdx
	callq	sprintf
	jmp	.LBB5_57
.LBB5_11:                               # %.outer._crit_edge
	leaq	576(%rsp), %rdi
	callq	luaL_pushresult
.LBB5_61:
	movl	%ebx, %eax
	addq	$8792, %rsp             # imm = 0x2258
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_60:
	movl	$.L.str.28, %esi
	xorl	%eax, %eax
	movq	(%rsp), %rdi            # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	luaL_error
	movl	%eax, %ebx
	jmp	.LBB5_61
.Lfunc_end5:
	.size	str_format, .Lfunc_end5-str_format
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI5_0:
	.quad	.LBB5_30
	.quad	.LBB5_60
	.quad	.LBB5_30
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_29
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_56
	.quad	.LBB5_27
	.quad	.LBB5_30
	.quad	.LBB5_30
	.quad	.LBB5_30
	.quad	.LBB5_60
	.quad	.LBB5_27
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_29
	.quad	.LBB5_60
	.quad	.LBB5_31
	.quad	.LBB5_60
	.quad	.LBB5_52
	.quad	.LBB5_60
	.quad	.LBB5_29
	.quad	.LBB5_60
	.quad	.LBB5_60
	.quad	.LBB5_29
.LJTI5_1:
	.quad	.LBB5_44
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_37
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_42
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_45
	.quad	.LBB5_37

	.text
	.p2align	4, 0x90
	.type	gfind_nodef,@function
gfind_nodef:                            # @gfind_nodef
	.cfi_startproc
# BB#0:
	movl	$.L.str.35, %esi
	xorl	%eax, %eax
	jmp	luaL_error              # TAILCALL
.Lfunc_end6:
	.size	gfind_nodef, .Lfunc_end6-gfind_nodef
	.cfi_endproc

	.p2align	4, 0x90
	.type	gmatch,@function
gmatch:                                 # @gmatch
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 16
.Lcfi47:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	xorl	%edx, %edx
	callq	luaL_checklstring
	movl	$2, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	luaL_checklstring
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	lua_pushinteger
	movl	$gmatch_aux, %esi
	movl	$3, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end7:
	.size	gmatch, .Lfunc_end7-gmatch
	.cfi_endproc

	.p2align	4, 0x90
	.type	str_gsub,@function
str_gsub:                               # @str_gsub
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 56
	subq	$8824, %rsp             # imm = 0x2278
.Lcfi54:
	.cfi_def_cfa_offset 8880
.Lcfi55:
	.cfi_offset %rbx, -56
.Lcfi56:
	.cfi_offset %r12, -48
.Lcfi57:
	.cfi_offset %r13, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	32(%rsp), %rdx
	movl	$1, %esi
	callq	luaL_checklstring
	movq	%rax, %r12
	movl	$2, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	luaL_checklstring
	movq	%rax, %r14
	movl	$3, %esi
	movq	%rbx, %rdi
	callq	lua_type
	movl	%eax, %ebp
	movq	32(%rsp), %rdx
	incq	%rdx
	movl	$4, %esi
	movq	%rbx, %rdi
	callq	luaL_optinteger
	movq	%rax, %r13
	movb	(%r14), %al
	leaq	1(%r14), %rcx
	movb	%al, 15(%rsp)           # 1-byte Spill
	cmpb	$94, %al
	cmovneq	%r14, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	addl	$-3, %ebp
	cmpl	$4, %ebp
	jb	.LBB8_2
# BB#1:
	movl	$3, %esi
	movl	$.L.str.36, %edx
	movq	%rbx, %rdi
	callq	luaL_argerror
.LBB8_2:
	leaq	608(%rsp), %rsi
	movq	%rbx, %rdi
	callq	luaL_buffinit
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movq	%rbx, 80(%rsp)
	movq	%r12, 64(%rsp)
	movq	32(%rsp), %rax
	addq	%r12, %rax
	movq	%rax, 72(%rsp)
	leaq	64(%rsp), %rdi
	xorl	%r15d, %r15d
	movb	15(%rsp), %bl           # 1-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%r13, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB8_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_10 Depth 2
                                        #     Child Loop BB8_17 Depth 2
	cmpl	%r13d, %r15d
	jge	.LBB8_48
# BB#4:                                 #   in Loop: Header=BB8_3 Depth=1
	movl	$0, 88(%rsp)
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	match
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB8_42
# BB#5:                                 #   in Loop: Header=BB8_3 Depth=1
	movl	%r15d, 28(%rsp)         # 4-byte Spill
	movq	80(%rsp), %r15
	movl	$3, %esi
	movq	%r15, %rdi
	callq	lua_type
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-3(%rax), %ecx
	cmpl	$2, %ecx
	jb	.LBB8_12
# BB#6:                                 #   in Loop: Header=BB8_3 Depth=1
	cmpl	$5, %eax
	je	.LBB8_27
# BB#7:                                 #   in Loop: Header=BB8_3 Depth=1
	cmpl	$6, %eax
	jne	.LBB8_36
# BB#8:                                 #   in Loop: Header=BB8_3 Depth=1
	movl	$3, %esi
	movq	%r15, %rdi
	callq	lua_pushvalue
	movl	88(%rsp), %eax
	testl	%eax, %eax
	movl	%eax, %ebp
	movl	$1, %ecx
	cmovel	%ecx, %ebp
	testq	%r12, %r12
	cmovel	%eax, %ebp
	movq	80(%rsp), %rdi
	movl	$.L.str.21, %edx
	movl	%ebp, %esi
	callq	luaL_checkstack
	testl	%ebp, %ebp
	leaq	64(%rsp), %rbx
	jle	.LBB8_11
# BB#9:                                 # %.lr.ph.i28.i.preheader
                                        #   in Loop: Header=BB8_3 Depth=1
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB8_10:                               # %.lr.ph.i28.i
                                        #   Parent Loop BB8_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%r12, %rdx
	movq	%r14, %rcx
	callq	push_onecapture
	incl	%r13d
	cmpl	%r13d, %ebp
	jne	.LBB8_10
.LBB8_11:                               # %push_captures.exit.i
                                        #   in Loop: Header=BB8_3 Depth=1
	movl	$1, %edx
	movq	%r15, %rdi
	movl	%ebp, %esi
	callq	lua_call
	movq	56(%rsp), %r13          # 8-byte Reload
	movb	15(%rsp), %bl           # 1-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB8_36
	.p2align	4, 0x90
.LBB8_12:                               #   in Loop: Header=BB8_3 Depth=1
	movq	80(%rsp), %rdi
	movl	$3, %esi
	leaq	40(%rsp), %rdx
	callq	lua_tolstring
	movq	%rax, %rbp
	cmpq	$0, 40(%rsp)
	movl	28(%rsp), %r15d         # 4-byte Reload
	je	.LBB8_26
# BB#13:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB8_3 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB8_17
	.p2align	4, 0x90
.LBB8_14:                               #   in Loop: Header=BB8_17 Depth=2
	cmpb	$48, %al
	jne	.LBB8_16
# BB#15:                                #   in Loop: Header=BB8_17 Depth=2
	movq	%r14, %rdx
	subq	%r12, %rdx
	leaq	608(%rsp), %rdi
	movq	%r12, %rsi
	callq	luaL_addlstring
	jmp	.LBB8_25
.LBB8_16:                               #   in Loop: Header=BB8_17 Depth=2
	movsbl	%al, %esi
	addl	$-49, %esi
	leaq	64(%rsp), %rdi
	movq	%r12, %rdx
	movq	%r14, %rcx
	callq	push_onecapture
	leaq	608(%rsp), %rdi
	callq	luaL_addvalue
	jmp	.LBB8_25
	.p2align	4, 0x90
.LBB8_17:                               # %.lr.ph.i.i
                                        #   Parent Loop BB8_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbp,%rbx), %eax
	cmpb	$37, %al
	jne	.LBB8_22
# BB#18:                                #   in Loop: Header=BB8_17 Depth=2
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movzbl	1(%rbp,%rbx), %eax
	incq	%rbx
	testb	$8, 1(%rcx,%rax,2)
	jne	.LBB8_14
# BB#19:                                #   in Loop: Header=BB8_17 Depth=2
	movq	608(%rsp), %rcx
	leaq	8824(%rsp), %rdx
	cmpq	%rdx, %rcx
	jb	.LBB8_21
# BB#20:                                #   in Loop: Header=BB8_17 Depth=2
	leaq	608(%rsp), %rdi
	callq	luaL_prepbuffer
	movb	(%rbp,%rbx), %al
	movq	608(%rsp), %rcx
.LBB8_21:                               #   in Loop: Header=BB8_17 Depth=2
	leaq	1(%rcx), %rdx
	movq	%rdx, 608(%rsp)
	movb	%al, (%rcx)
	jmp	.LBB8_25
	.p2align	4, 0x90
.LBB8_22:                               #   in Loop: Header=BB8_17 Depth=2
	movq	608(%rsp), %rcx
	leaq	8824(%rsp), %rdx
	cmpq	%rdx, %rcx
	jb	.LBB8_24
# BB#23:                                #   in Loop: Header=BB8_17 Depth=2
	leaq	608(%rsp), %rdi
	callq	luaL_prepbuffer
	movzbl	(%rbp,%rbx), %eax
	movq	608(%rsp), %rcx
.LBB8_24:                               #   in Loop: Header=BB8_17 Depth=2
	leaq	1(%rcx), %rdx
	movq	%rdx, 608(%rsp)
	movb	%al, (%rcx)
.LBB8_25:                               #   in Loop: Header=BB8_17 Depth=2
	incq	%rbx
	cmpq	40(%rsp), %rbx
	jb	.LBB8_17
.LBB8_26:                               # %add_s.exit.i
                                        #   in Loop: Header=BB8_3 Depth=1
	movb	15(%rsp), %bl           # 1-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB8_41
.LBB8_27:                               #   in Loop: Header=BB8_3 Depth=1
	cmpl	$0, 88(%rsp)
	jle	.LBB8_31
# BB#28:                                #   in Loop: Header=BB8_3 Depth=1
	movq	104(%rsp), %rbp
	cmpq	$-1, %rbp
	je	.LBB8_32
# BB#29:                                #   in Loop: Header=BB8_3 Depth=1
	movq	80(%rsp), %rdi
	movq	96(%rsp), %rsi
	cmpq	$-2, %rbp
	jne	.LBB8_33
# BB#30:                                #   in Loop: Header=BB8_3 Depth=1
	subq	64(%rsp), %rsi
	incq	%rsi
	callq	lua_pushinteger
	jmp	.LBB8_34
.LBB8_31:                               #   in Loop: Header=BB8_3 Depth=1
	movq	80(%rsp), %rdi
	movq	%r14, %rdx
	subq	%r12, %rdx
	movq	%r12, %rsi
	callq	lua_pushlstring
	jmp	.LBB8_35
.LBB8_32:                               # %.thread.i.i
                                        #   in Loop: Header=BB8_3 Depth=1
	movq	80(%rsp), %rdi
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	callq	luaL_error
	movq	80(%rsp), %rdi
	movq	96(%rsp), %rsi
.LBB8_33:                               #   in Loop: Header=BB8_3 Depth=1
	movq	%rbp, %rdx
	callq	lua_pushlstring
.LBB8_34:                               # %push_onecapture.exit.i
                                        #   in Loop: Header=BB8_3 Depth=1
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB8_35:                               # %push_onecapture.exit.i
                                        #   in Loop: Header=BB8_3 Depth=1
	movl	$3, %esi
	movq	%r15, %rdi
	callq	lua_gettable
.LBB8_36:                               #   in Loop: Header=BB8_3 Depth=1
	movl	$-1, %esi
	movq	%r15, %rdi
	callq	lua_toboolean
	testl	%eax, %eax
	je	.LBB8_39
# BB#37:                                #   in Loop: Header=BB8_3 Depth=1
	movl	$-1, %esi
	movq	%r15, %rdi
	callq	lua_isstring
	testl	%eax, %eax
	jne	.LBB8_40
# BB#38:                                #   in Loop: Header=BB8_3 Depth=1
	movl	$-1, %esi
	movq	%r15, %rdi
	callq	lua_type
	movq	%r15, %rdi
	movl	%eax, %esi
	callq	lua_typename
	movq	%rax, %rcx
	movl	$.L.str.37, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rcx, %rdx
	callq	luaL_error
	jmp	.LBB8_40
.LBB8_39:                               #   in Loop: Header=BB8_3 Depth=1
	movl	$-2, %esi
	movq	%r15, %rdi
	callq	lua_settop
	movq	%r14, %rdx
	subq	%r12, %rdx
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	lua_pushlstring
.LBB8_40:                               #   in Loop: Header=BB8_3 Depth=1
	leaq	608(%rsp), %rdi
	callq	luaL_addvalue
	movl	28(%rsp), %r15d         # 4-byte Reload
.LBB8_41:                               # %add_value.exit
                                        #   in Loop: Header=BB8_3 Depth=1
	incl	%r15d
	cmpq	%r12, %r14
	leaq	64(%rsp), %rdi
	ja	.LBB8_46
.LBB8_42:                               # %add_value.exit.thread
                                        #   in Loop: Header=BB8_3 Depth=1
	cmpq	72(%rsp), %r12
	jae	.LBB8_48
# BB#43:                                #   in Loop: Header=BB8_3 Depth=1
	movq	608(%rsp), %rax
	leaq	8824(%rsp), %rcx
	cmpq	%rcx, %rax
	jb	.LBB8_45
# BB#44:                                #   in Loop: Header=BB8_3 Depth=1
	leaq	608(%rsp), %rdi
	callq	luaL_prepbuffer
	movq	608(%rsp), %rax
.LBB8_45:                               #   in Loop: Header=BB8_3 Depth=1
	movb	(%r12), %cl
	incq	%r12
	leaq	1(%rax), %rdx
	movq	%rdx, 608(%rsp)
	movb	%cl, (%rax)
	movq	%r12, %r14
	leaq	64(%rsp), %rdi
.LBB8_46:                               #   in Loop: Header=BB8_3 Depth=1
	cmpb	$94, %bl
	movq	%r14, %r12
	jne	.LBB8_3
	jmp	.LBB8_49
.LBB8_48:
	movq	%r12, %r14
.LBB8_49:                               # %.thread
	movq	72(%rsp), %rdx
	subq	%r14, %rdx
	leaq	608(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	luaL_addlstring
	movq	%rbx, %rdi
	callq	luaL_pushresult
	movslq	%r15d, %rsi
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	lua_pushinteger
	movl	$2, %eax
	addq	$8824, %rsp             # imm = 0x2278
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	str_gsub, .Lfunc_end8-str_gsub
	.cfi_endproc

	.p2align	4, 0x90
	.type	str_len,@function
str_len:                                # @str_len
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi62:
	.cfi_def_cfa_offset 32
.Lcfi63:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	8(%rsp), %rdx
	movl	$1, %esi
	callq	luaL_checklstring
	movq	8(%rsp), %rsi
	movq	%rbx, %rdi
	callq	lua_pushinteger
	movl	$1, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end9:
	.size	str_len, .Lfunc_end9-str_len
	.cfi_endproc

	.p2align	4, 0x90
	.type	str_lower,@function
str_lower:                              # @str_lower
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 48
	subq	$8224, %rsp             # imm = 0x2020
.Lcfi69:
	.cfi_def_cfa_offset 8272
.Lcfi70:
	.cfi_offset %rbx, -48
.Lcfi71:
	.cfi_offset %r12, -40
.Lcfi72:
	.cfi_offset %r13, -32
.Lcfi73:
	.cfi_offset %r14, -24
.Lcfi74:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	%rsp, %rdx
	movl	$1, %esi
	callq	luaL_checklstring
	movq	%rax, %r12
	leaq	8(%rsp), %rsi
	movq	%r14, %rdi
	callq	luaL_buffinit
	cmpq	$0, (%rsp)
	je	.LBB10_5
# BB#1:                                 # %.lr.ph
	leaq	8224(%rsp), %r15
	xorl	%ebx, %ebx
	leaq	8(%rsp), %r14
	.p2align	4, 0x90
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %r13
	cmpq	%r15, %r13
	jb	.LBB10_4
# BB#3:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	%r14, %rdi
	callq	luaL_prepbuffer
	movq	8(%rsp), %r13
.LBB10_4:                               #   in Loop: Header=BB10_2 Depth=1
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movzbl	(%r12,%rbx), %ecx
	movzbl	(%rax,%rcx,4), %eax
	leaq	1(%r13), %rcx
	movq	%rcx, 8(%rsp)
	movb	%al, (%r13)
	incq	%rbx
	cmpq	(%rsp), %rbx
	jb	.LBB10_2
.LBB10_5:                               # %._crit_edge
	leaq	8(%rsp), %rdi
	callq	luaL_pushresult
	movl	$1, %eax
	addq	$8224, %rsp             # imm = 0x2020
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end10:
	.size	str_lower, .Lfunc_end10-str_lower
	.cfi_endproc

	.p2align	4, 0x90
	.type	str_match,@function
str_match:                              # @str_match
	.cfi_startproc
# BB#0:
	xorl	%esi, %esi
	jmp	str_find_aux            # TAILCALL
.Lfunc_end11:
	.size	str_match, .Lfunc_end11-str_match
	.cfi_endproc

	.p2align	4, 0x90
	.type	str_rep,@function
str_rep:                                # @str_rep
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi75:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi76:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 32
	subq	$8224, %rsp             # imm = 0x2020
.Lcfi78:
	.cfi_def_cfa_offset 8256
.Lcfi79:
	.cfi_offset %rbx, -32
.Lcfi80:
	.cfi_offset %r14, -24
.Lcfi81:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	%rsp, %rdx
	movl	$1, %esi
	callq	luaL_checklstring
	movq	%rax, %r14
	movl	$2, %esi
	movq	%r15, %rdi
	callq	luaL_checkinteger
	movq	%rax, %rbx
	leaq	8(%rsp), %rsi
	movq	%r15, %rdi
	callq	luaL_buffinit
	testl	%ebx, %ebx
	jle	.LBB12_3
# BB#1:                                 # %.lr.ph.preheader
	incl	%ebx
	leaq	8(%rsp), %r15
	.p2align	4, 0x90
.LBB12_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsp), %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	luaL_addlstring
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB12_2
.LBB12_3:                               # %._crit_edge
	leaq	8(%rsp), %rdi
	callq	luaL_pushresult
	movl	$1, %eax
	addq	$8224, %rsp             # imm = 0x2020
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	str_rep, .Lfunc_end12-str_rep
	.cfi_endproc

	.p2align	4, 0x90
	.type	str_reverse,@function
str_reverse:                            # @str_reverse
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 32
	subq	$8224, %rsp             # imm = 0x2020
.Lcfi85:
	.cfi_def_cfa_offset 8256
.Lcfi86:
	.cfi_offset %rbx, -32
.Lcfi87:
	.cfi_offset %r14, -24
.Lcfi88:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	%rsp, %rdx
	movl	$1, %esi
	callq	luaL_checklstring
	movq	%rax, %rbx
	leaq	8(%rsp), %rsi
	movq	%r14, %rdi
	callq	luaL_buffinit
	movq	(%rsp), %rcx
	leaq	-1(%rcx), %rax
	movq	%rax, (%rsp)
	testq	%rcx, %rcx
	je	.LBB13_5
# BB#1:                                 # %.lr.ph
	leaq	8224(%rsp), %r15
	leaq	8(%rsp), %r14
	.p2align	4, 0x90
.LBB13_2:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rcx
	cmpq	%r15, %rcx
	jb	.LBB13_4
# BB#3:                                 #   in Loop: Header=BB13_2 Depth=1
	movq	%r14, %rdi
	callq	luaL_prepbuffer
	movq	(%rsp), %rax
	movq	8(%rsp), %rcx
.LBB13_4:                               #   in Loop: Header=BB13_2 Depth=1
	movzbl	(%rbx,%rax), %eax
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rsp)
	movb	%al, (%rcx)
	movq	(%rsp), %rcx
	leaq	-1(%rcx), %rax
	movq	%rax, (%rsp)
	testq	%rcx, %rcx
	jne	.LBB13_2
.LBB13_5:                               # %._crit_edge
	leaq	8(%rsp), %rdi
	callq	luaL_pushresult
	movl	$1, %eax
	addq	$8224, %rsp             # imm = 0x2020
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end13:
	.size	str_reverse, .Lfunc_end13-str_reverse
	.cfi_endproc

	.p2align	4, 0x90
	.type	str_sub,@function
str_sub:                                # @str_sub
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi92:
	.cfi_def_cfa_offset 48
.Lcfi93:
	.cfi_offset %rbx, -32
.Lcfi94:
	.cfi_offset %r14, -24
.Lcfi95:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	leaq	8(%rsp), %rdx
	movl	$1, %esi
	callq	luaL_checklstring
	movq	%rax, %r14
	movl	$2, %esi
	movq	%r15, %rdi
	callq	luaL_checkinteger
	movq	8(%rsp), %rcx
	incq	%rcx
	movq	%rax, %rbx
	sarq	$63, %rbx
	andq	%rcx, %rbx
	addq	%rax, %rbx
	movl	$3, %esi
	movq	$-1, %rdx
	movq	%r15, %rdi
	callq	luaL_optinteger
	movq	8(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rax, %rsi
	sarq	$63, %rsi
	andq	%rcx, %rsi
	xorl	%ecx, %ecx
	addq	%rax, %rsi
	cmovnsq	%rsi, %rcx
	testq	%rbx, %rbx
	movl	$1, %eax
	cmovgq	%rbx, %rax
	cmpq	%rdx, %rcx
	cmovgq	%rdx, %rcx
	cmpq	%rax, %rcx
	jge	.LBB14_1
# BB#2:
	movl	$.L.str.38, %esi
	xorl	%edx, %edx
	jmp	.LBB14_3
.LBB14_1:
	leaq	-1(%r14,%rax), %rsi
	movl	$1, %edx
	subq	%rax, %rdx
	addq	%rcx, %rdx
.LBB14_3:
	movq	%r15, %rdi
	callq	lua_pushlstring
	movl	$1, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	str_sub, .Lfunc_end14-str_sub
	.cfi_endproc

	.p2align	4, 0x90
	.type	str_upper,@function
str_upper:                              # @str_upper
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi98:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi99:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 48
	subq	$8224, %rsp             # imm = 0x2020
.Lcfi101:
	.cfi_def_cfa_offset 8272
.Lcfi102:
	.cfi_offset %rbx, -48
.Lcfi103:
	.cfi_offset %r12, -40
.Lcfi104:
	.cfi_offset %r13, -32
.Lcfi105:
	.cfi_offset %r14, -24
.Lcfi106:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	%rsp, %rdx
	movl	$1, %esi
	callq	luaL_checklstring
	movq	%rax, %r12
	leaq	8(%rsp), %rsi
	movq	%r14, %rdi
	callq	luaL_buffinit
	cmpq	$0, (%rsp)
	je	.LBB15_5
# BB#1:                                 # %.lr.ph
	leaq	8224(%rsp), %r15
	xorl	%ebx, %ebx
	leaq	8(%rsp), %r14
	.p2align	4, 0x90
.LBB15_2:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %r13
	cmpq	%r15, %r13
	jb	.LBB15_4
# BB#3:                                 #   in Loop: Header=BB15_2 Depth=1
	movq	%r14, %rdi
	callq	luaL_prepbuffer
	movq	8(%rsp), %r13
.LBB15_4:                               #   in Loop: Header=BB15_2 Depth=1
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movzbl	(%r12,%rbx), %ecx
	movzbl	(%rax,%rcx,4), %eax
	leaq	1(%r13), %rcx
	movq	%rcx, 8(%rsp)
	movb	%al, (%r13)
	incq	%rbx
	cmpq	(%rsp), %rbx
	jb	.LBB15_2
.LBB15_5:                               # %._crit_edge
	leaq	8(%rsp), %rdi
	callq	luaL_pushresult
	movl	$1, %eax
	addq	$8224, %rsp             # imm = 0x2020
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end15:
	.size	str_upper, .Lfunc_end15-str_upper
	.cfi_endproc

	.p2align	4, 0x90
	.type	writer,@function
writer:                                 # @writer
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi107:
	.cfi_def_cfa_offset 16
	movq	%rcx, %rdi
	callq	luaL_addlstring
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end16:
	.size	writer, .Lfunc_end16-writer
	.cfi_endproc

	.p2align	4, 0x90
	.type	str_find_aux,@function
str_find_aux:                           # @str_find_aux
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi109:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi111:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi112:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 56
	subq	$584, %rsp              # imm = 0x248
.Lcfi114:
	.cfi_def_cfa_offset 640
.Lcfi115:
	.cfi_offset %rbx, -56
.Lcfi116:
	.cfi_offset %r12, -48
.Lcfi117:
	.cfi_offset %r13, -40
.Lcfi118:
	.cfi_offset %r14, -32
.Lcfi119:
	.cfi_offset %r15, -24
.Lcfi120:
	.cfi_offset %rbp, -16
	movl	%esi, %r12d
	movq	%rdi, %r15
	leaq	16(%rsp), %rdx
	movl	$1, %esi
	callq	luaL_checklstring
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	32(%rsp), %rdx
	movl	$2, %esi
	movq	%r15, %rdi
	callq	luaL_checklstring
	movq	%rax, %r13
	movl	$3, %esi
	movl	$1, %edx
	movq	%r15, %rdi
	callq	luaL_optinteger
	movq	16(%rsp), %rcx
	leaq	1(%rcx), %rdx
	movq	%rax, %rsi
	sarq	$63, %rsi
	andq	%rdx, %rsi
	xorl	%edx, %edx
	addq	%rax, %rsi
	cmovsq	%rdx, %rsi
	leaq	-1(%rsi), %rbx
	cmpq	%rcx, %rbx
	cmovaq	%rcx, %rbx
	testq	%rsi, %rsi
	cmovleq	%rdx, %rbx
	testl	%r12d, %r12d
	je	.LBB17_4
# BB#1:
	movl	$4, %esi
	movq	%r15, %rdi
	callq	lua_toboolean
	testl	%eax, %eax
	je	.LBB17_2
.LBB17_9:
	movq	%r15, 8(%rsp)           # 8-byte Spill
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%rax,%rbx), %rbp
	movq	32(%rsp), %r12
	testq	%r12, %r12
	je	.LBB17_16
# BB#10:
	movq	16(%rsp), %r14
	subq	%rbx, %r14
	cmpq	%r14, %r12
	ja	.LBB17_28
# BB#11:
	decq	%r12
	subq	%r12, %r14
	je	.LBB17_28
# BB#12:                                # %.lr.ph.i81
	movsbl	(%r13), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	incq	%r13
	.p2align	4, 0x90
.LBB17_13:                              # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rdi
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	%r14, %rdx
	callq	memchr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB17_28
# BB#14:                                #   in Loop: Header=BB17_13 Depth=1
	leaq	1(%rbx), %r15
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	callq	memcmp
	testl	%eax, %eax
	je	.LBB17_15
# BB#27:                                #   in Loop: Header=BB17_13 Depth=1
	movq	%r15, %rax
	subq	%rbp, %rax
	subq	%rax, %r14
	movq	%r15, %rbp
	jne	.LBB17_13
	jmp	.LBB17_28
.LBB17_2:
	movl	$.L.str.19, %esi
	movq	%r13, %rdi
	callq	strpbrk
	testq	%rax, %rax
	je	.LBB17_9
# BB#3:                                 # %._crit_edge
	movq	16(%rsp), %rcx
.LBB17_4:
	movb	(%r13), %r14b
	leaq	1(%r13), %rbp
	cmpb	$94, %r14b
	cmovneq	%r13, %rbp
	movq	(%rsp), %rax            # 8-byte Reload
	addq	%rax, %rbx
	movq	%r15, 56(%rsp)
	movq	%rax, 40(%rsp)
	addq	%rax, %rcx
	movq	%rcx, 48(%rsp)
	movl	$0, 64(%rsp)
	leaq	40(%rsp), %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	match
	movq	%rax, %r13
	cmpb	$94, %r14b
	jne	.LBB17_5
# BB#18:                                # %.split.us
	testq	%r13, %r13
	jne	.LBB17_19
	jmp	.LBB17_30
.LBB17_5:                               # %.split.preheader
	testq	%r13, %r13
	jne	.LBB17_19
# BB#6:
	leaq	40(%rsp), %r14
	.p2align	4, 0x90
.LBB17_7:                               # %push_captures.exit
                                        # =>This Inner Loop Header: Depth=1
	cmpq	48(%rsp), %rbx
	jae	.LBB17_30
# BB#8:                                 # %.split
                                        #   in Loop: Header=BB17_7 Depth=1
	incq	%rbx
	movl	$0, 64(%rsp)
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	match
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB17_7
.LBB17_19:                              # %.us-lcssa.us
	testl	%r12d, %r12d
	je	.LBB17_24
# BB#20:
	movq	(%rsp), %rbp            # 8-byte Reload
	subq	%rbp, %rbx
	incq	%rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	lua_pushinteger
	subq	%rbp, %r13
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	lua_pushinteger
	movl	64(%rsp), %r15d
	movq	56(%rsp), %rdi
	movl	$.L.str.21, %edx
	movl	%r15d, %esi
	callq	luaL_checkstack
	testl	%r15d, %r15d
	jle	.LBB17_23
# BB#21:                                # %.lr.ph.i84.preheader
	xorl	%ebx, %ebx
	leaq	40(%rsp), %r14
	.p2align	4, 0x90
.LBB17_22:                              # %.lr.ph.i84
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movl	%ebx, %esi
	callq	push_onecapture
	incl	%ebx
	cmpl	%ebx, %r15d
	jne	.LBB17_22
.LBB17_23:                              # %push_captures.exit85
	addl	$2, %r15d
	jmp	.LBB17_29
.LBB17_16:                              # %lmemfind.exit
	testq	%rbp, %rbp
	jne	.LBB17_17
	jmp	.LBB17_28
.LBB17_30:                              # %.critedge.thread
	movq	%r15, 8(%rsp)           # 8-byte Spill
.LBB17_28:                              # %lmemfind.exit.thread
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	lua_pushnil
	movl	$1, %r15d
	jmp	.LBB17_29
.LBB17_24:
	movl	64(%rsp), %eax
	testl	%eax, %eax
	movl	$1, %r15d
	cmovnel	%eax, %r15d
	testq	%rbx, %rbx
	cmovel	%eax, %r15d
	movq	56(%rsp), %rdi
	movl	$.L.str.21, %edx
	movl	%r15d, %esi
	callq	luaL_checkstack
	testl	%r15d, %r15d
	jle	.LBB17_29
# BB#25:                                # %.lr.ph.i.preheader
	xorl	%ebp, %ebp
	leaq	40(%rsp), %r14
	.p2align	4, 0x90
.LBB17_26:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	movl	%ebp, %esi
	movq	%rbx, %rdx
	movq	%r13, %rcx
	callq	push_onecapture
	incl	%ebp
	cmpl	%ebp, %r15d
	jne	.LBB17_26
	jmp	.LBB17_29
.LBB17_15:
	movq	%rbx, %rbp
.LBB17_17:                              # %.critedge78
	subq	(%rsp), %rbp            # 8-byte Folded Reload
	leaq	1(%rbp), %rsi
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rdi
	callq	lua_pushinteger
	addq	32(%rsp), %rbp
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	lua_pushinteger
	movl	$2, %r15d
.LBB17_29:
	movl	%r15d, %eax
	addq	$584, %rsp              # imm = 0x248
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	str_find_aux, .Lfunc_end17-str_find_aux
	.cfi_endproc

	.p2align	4, 0x90
	.type	match,@function
match:                                  # @match
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi121:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi122:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi123:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi124:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi125:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi127:
	.cfi_def_cfa_offset 96
.Lcfi128:
	.cfi_offset %rbx, -56
.Lcfi129:
	.cfi_offset %r12, -48
.Lcfi130:
	.cfi_offset %r13, -40
.Lcfi131:
	.cfi_offset %r14, -32
.Lcfi132:
	.cfi_offset %r15, -24
.Lcfi133:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	%r15, (%rsp)            # 8-byte Spill
	jmp	.LBB18_2
.LBB18_1:                               #   in Loop: Header=BB18_2 Depth=1
	addq	$2, %r12
	movq	%r12, %rbx
	movq	%rcx, %r14
	.p2align	4, 0x90
.LBB18_2:                               # %min_expand.exit.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_3 Depth 2
                                        #       Child Loop BB18_4 Depth 3
                                        #         Child Loop BB18_10 Depth 4
                                        #         Child Loop BB18_31 Depth 4
                                        #       Child Loop BB18_58 Depth 3
                                        #       Child Loop BB18_70 Depth 3
                                        #       Child Loop BB18_84 Depth 3
                                        #     Child Loop BB18_110 Depth 2
	leaq	1(%r14), %r13
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%r13, 32(%rsp)          # 8-byte Spill
.LBB18_3:                               # %min_expand.exit.outer134
                                        #   Parent Loop BB18_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB18_4 Depth 3
                                        #         Child Loop BB18_10 Depth 4
                                        #         Child Loop BB18_31 Depth 4
                                        #       Child Loop BB18_58 Depth 3
                                        #       Child Loop BB18_70 Depth 3
                                        #       Child Loop BB18_84 Depth 3
	movq	%rbx, %r12
	.p2align	4, 0x90
.LBB18_4:                               # %min_expand.exit
                                        #   Parent Loop BB18_2 Depth=1
                                        #     Parent Loop BB18_3 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB18_10 Depth 4
                                        #         Child Loop BB18_31 Depth 4
	movsbl	(%r12), %eax
	cmpl	$41, %eax
	ja	.LBB18_8
# BB#5:                                 # %min_expand.exit
                                        #   in Loop: Header=BB18_4 Depth=3
	jmpq	*.LJTI18_0(,%rax,8)
.LBB18_6:                               #   in Loop: Header=BB18_4 Depth=3
	cmpb	$0, 1(%r12)
	je	.LBB18_123
# BB#7:                                 #   in Loop: Header=BB18_4 Depth=3
	leaq	1(%r12), %rbp
	jmp	.LBB18_23
	.p2align	4, 0x90
.LBB18_8:                               #   in Loop: Header=BB18_4 Depth=3
	leaq	1(%r12), %rbp
	cmpb	$91, %al
	jne	.LBB18_23
# BB#9:                                 #   in Loop: Header=BB18_4 Depth=3
	leaq	2(%r12), %rbx
	cmpb	$94, 1(%r12)
	cmovneq	%rbp, %rbx
	movb	(%rbx), %al
	.p2align	4, 0x90
.LBB18_10:                              #   Parent Loop BB18_2 Depth=1
                                        #     Parent Loop BB18_3 Depth=2
                                        #       Parent Loop BB18_4 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	testb	%al, %al
	jne	.LBB18_12
# BB#11:                                #   in Loop: Header=BB18_10 Depth=4
	movq	16(%r15), %rdi
	movl	$.L.str.25, %esi
	xorl	%eax, %eax
	callq	luaL_error
	movzbl	(%rbx), %eax
.LBB18_12:                              #   in Loop: Header=BB18_10 Depth=4
	leaq	1(%rbx), %rcx
	cmpb	$37, %al
	jne	.LBB18_14
# BB#13:                                #   in Loop: Header=BB18_10 Depth=4
	cmpb	$0, 1(%rbx)
	leaq	2(%rbx), %rbx
	cmoveq	%rcx, %rbx
	jmp	.LBB18_15
	.p2align	4, 0x90
.LBB18_14:                              #   in Loop: Header=BB18_10 Depth=4
	movq	%rcx, %rbx
.LBB18_15:                              #   in Loop: Header=BB18_10 Depth=4
	movzbl	(%rbx), %eax
	cmpb	$93, %al
	jne	.LBB18_10
# BB#16:                                #   in Loop: Header=BB18_4 Depth=3
	incq	%rbx
	cmpq	8(%r15), %r14
	jb	.LBB18_24
	jmp	.LBB18_28
	.p2align	4, 0x90
.LBB18_23:                              #   in Loop: Header=BB18_4 Depth=3
	movq	%rbp, %rbx
	cmpq	8(%r15), %r14
	jae	.LBB18_28
	jmp	.LBB18_24
	.p2align	4, 0x90
.LBB18_17:                              #   in Loop: Header=BB18_4 Depth=3
	movsbl	1(%r12), %eax
	cmpl	$102, %eax
	je	.LBB18_50
# BB#18:                                #   in Loop: Header=BB18_4 Depth=3
	cmpl	$98, %eax
	je	.LBB18_104
# BB#19:                                #   in Loop: Header=BB18_4 Depth=3
	movzbl	%al, %ebx
	callq	__ctype_b_loc
	movq	(%rax), %rax
	testb	$8, 1(%rax,%rbx,2)
	jne	.LBB18_96
# BB#20:                                # %.thread275
                                        #   in Loop: Header=BB18_4 Depth=3
	leaq	1(%r12), %rbp
	cmpb	$0, (%rbp)
	jne	.LBB18_22
# BB#21:                                #   in Loop: Header=BB18_4 Depth=3
	movq	16(%r15), %rdi
	movl	$.L.str.24, %esi
	xorl	%eax, %eax
	callq	luaL_error
.LBB18_22:                              #   in Loop: Header=BB18_4 Depth=3
	leaq	2(%r12), %rbx
	cmpq	8(%r15), %r14
	jae	.LBB18_28
.LBB18_24:                              #   in Loop: Header=BB18_4 Depth=3
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movzbl	(%r14), %r13d
	movb	(%r12), %al
	movsbl	%al, %ecx
	cmpl	$91, %ecx
	je	.LBB18_29
# BB#25:                                #   in Loop: Header=BB18_4 Depth=3
	cmpl	$46, %ecx
	je	.LBB18_41
# BB#26:                                #   in Loop: Header=BB18_4 Depth=3
	cmpl	$37, %ecx
	jne	.LBB18_42
# BB#27:                                #   in Loop: Header=BB18_4 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax), %esi
	movl	%r13d, %edi
	callq	match_class
	movl	%eax, %ebp
	jmp	.LBB18_45
	.p2align	4, 0x90
.LBB18_28:                              #   in Loop: Header=BB18_4 Depth=3
	xorl	%eax, %eax
	jmp	.LBB18_46
.LBB18_29:                              #   in Loop: Header=BB18_4 Depth=3
	leaq	-1(%rbx), %r15
	xorl	%ebp, %ebp
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpb	$94, (%rax)
	movq	%r12, %r14
	cmoveq	%rax, %r14
	setne	%bpl
	jmp	.LBB18_31
	.p2align	4, 0x90
.LBB18_30:                              #   in Loop: Header=BB18_31 Depth=4
	addq	$2, %r14
	movq	%r14, %rax
.LBB18_31:                              #   Parent Loop BB18_2 Depth=1
                                        #     Parent Loop BB18_3 Depth=2
                                        #       Parent Loop BB18_4 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leaq	1(%r14), %rax
	cmpq	%r15, %rax
	jae	.LBB18_43
# BB#32:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB18_31 Depth=4
	movzbl	(%rax), %ecx
	movzbl	2(%r14), %esi
	cmpb	$37, %cl
	jne	.LBB18_34
# BB#33:                                #   in Loop: Header=BB18_31 Depth=4
	movl	%r13d, %edi
	callq	match_class
	testl	%eax, %eax
	je	.LBB18_30
	jmp	.LBB18_44
	.p2align	4, 0x90
.LBB18_34:                              #   in Loop: Header=BB18_31 Depth=4
	cmpb	$45, %sil
	jne	.LBB18_38
# BB#35:                                #   in Loop: Header=BB18_31 Depth=4
	addq	$3, %r14
	cmpq	%r15, %r14
	jae	.LBB18_38
# BB#36:                                #   in Loop: Header=BB18_31 Depth=4
	cmpb	%r13b, %cl
	jbe	.LBB18_40
# BB#37:                                #   in Loop: Header=BB18_31 Depth=4
	movq	%r14, %rax
	jmp	.LBB18_31
	.p2align	4, 0x90
.LBB18_38:                              #   in Loop: Header=BB18_31 Depth=4
	cmpb	%r13b, %cl
	je	.LBB18_44
.LBB18_39:                              # %.backedge.i.i
                                        #   in Loop: Header=BB18_31 Depth=4
	movq	%rax, %r14
	jmp	.LBB18_31
.LBB18_40:                              #   in Loop: Header=BB18_31 Depth=4
	cmpb	%r13b, (%r14)
	movq	%r14, %rax
	jb	.LBB18_39
	jmp	.LBB18_44
.LBB18_41:                              #   in Loop: Header=BB18_4 Depth=3
	movl	$1, %ebp
	jmp	.LBB18_45
.LBB18_42:                              #   in Loop: Header=BB18_4 Depth=3
	xorl	%ebp, %ebp
	cmpb	%r13b, %al
	sete	%bpl
	jmp	.LBB18_45
.LBB18_43:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB18_4 Depth=3
	xorl	$1, %ebp
.LBB18_44:                              #   in Loop: Header=BB18_4 Depth=3
	movq	(%rsp), %r15            # 8-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB18_45:                              # %singlematch.exit
                                        #   in Loop: Header=BB18_4 Depth=3
	testl	%ebp, %ebp
	setne	%al
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB18_46:                              #   in Loop: Header=BB18_4 Depth=3
	movsbl	(%rbx), %ecx
	addl	$-42, %ecx
	cmpl	$21, %ecx
	ja	.LBB18_103
# BB#47:                                #   in Loop: Header=BB18_4 Depth=3
	jmpq	*.LJTI18_1(,%rcx,8)
.LBB18_48:                              #   in Loop: Header=BB18_4 Depth=3
	incq	%rbx
	testb	%al, %al
	movq	%rbx, %r12
	je	.LBB18_4
# BB#49:                                #   in Loop: Header=BB18_4 Depth=3
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	callq	match
	testq	%rax, %rax
	movq	%rbx, %r12
	je	.LBB18_4
	jmp	.LBB18_115
.LBB18_50:                              #   in Loop: Header=BB18_3 Depth=2
	leaq	2(%r12), %r14
	movb	$91, %al
	cmpb	$91, 2(%r12)
	je	.LBB18_52
# BB#51:                                #   in Loop: Header=BB18_3 Depth=2
	movq	16(%r15), %rdi
	movl	$.L.str.20, %esi
	xorl	%eax, %eax
	callq	luaL_error
	movb	(%r14), %al
.LBB18_52:                              #   in Loop: Header=BB18_3 Depth=2
	leaq	3(%r12), %rbp
	movsbl	%al, %eax
	cmpl	$91, %eax
	je	.LBB18_57
# BB#53:                                #   in Loop: Header=BB18_3 Depth=2
	cmpl	$37, %eax
	movq	%rbp, %rbx
	jne	.LBB18_65
# BB#54:                                #   in Loop: Header=BB18_3 Depth=2
	cmpb	$0, (%rbp)
	jne	.LBB18_56
# BB#55:                                #   in Loop: Header=BB18_3 Depth=2
	movq	16(%r15), %rdi
	movl	$.L.str.24, %esi
	xorl	%eax, %eax
	callq	luaL_error
.LBB18_56:                              #   in Loop: Header=BB18_3 Depth=2
	addq	$4, %r12
	movq	%r12, %rbx
	jmp	.LBB18_65
.LBB18_57:                              #   in Loop: Header=BB18_3 Depth=2
	leaq	4(%r12), %rbx
	cmpb	$94, 3(%r12)
	cmovneq	%rbp, %rbx
	movb	(%rbx), %al
	.p2align	4, 0x90
.LBB18_58:                              #   Parent Loop BB18_2 Depth=1
                                        #     Parent Loop BB18_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testb	%al, %al
	jne	.LBB18_60
# BB#59:                                #   in Loop: Header=BB18_58 Depth=3
	movq	16(%r15), %rdi
	movl	$.L.str.25, %esi
	xorl	%eax, %eax
	callq	luaL_error
	movzbl	(%rbx), %eax
.LBB18_60:                              #   in Loop: Header=BB18_58 Depth=3
	leaq	1(%rbx), %rcx
	cmpb	$37, %al
	jne	.LBB18_62
# BB#61:                                #   in Loop: Header=BB18_58 Depth=3
	cmpb	$0, 1(%rbx)
	leaq	2(%rbx), %rbx
	cmoveq	%rcx, %rbx
	jmp	.LBB18_63
	.p2align	4, 0x90
.LBB18_62:                              #   in Loop: Header=BB18_58 Depth=3
	movq	%rcx, %rbx
.LBB18_63:                              #   in Loop: Header=BB18_58 Depth=3
	movzbl	(%rbx), %eax
	cmpb	$93, %al
	jne	.LBB18_58
# BB#64:                                #   in Loop: Header=BB18_3 Depth=2
	incq	%rbx
.LBB18_65:                              # %classend.exit113
                                        #   in Loop: Header=BB18_3 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpq	(%r15), %rax
	je	.LBB18_67
# BB#66:                                #   in Loop: Header=BB18_3 Depth=2
	movb	-1(%rax), %r12b
	jmp	.LBB18_68
.LBB18_67:                              #   in Loop: Header=BB18_3 Depth=2
	xorl	%r12d, %r12d
.LBB18_68:                              #   in Loop: Header=BB18_3 Depth=2
	leaq	-1(%rbx), %r13
	xorl	%eax, %eax
	cmpb	$94, (%rbp)
	cmoveq	%rbp, %r14
	setne	%al
	movl	%eax, 12(%rsp)          # 4-byte Spill
	leaq	1(%r14), %r15
	cmpq	%r13, %r15
	jae	.LBB18_81
# BB#69:                                # %.lr.ph.i100.preheader
                                        #   in Loop: Header=BB18_3 Depth=2
	movzbl	%r12b, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	%r15, %rax
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB18_70:                              # %.lr.ph.i100
                                        #   Parent Loop BB18_2 Depth=1
                                        #     Parent Loop BB18_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %ecx
	movzbl	2(%rbp), %esi
	cmpb	$37, %cl
	jne	.LBB18_73
# BB#71:                                #   in Loop: Header=BB18_70 Depth=3
	movl	16(%rsp), %edi          # 4-byte Reload
	callq	match_class
	testl	%eax, %eax
	jne	.LBB18_80
# BB#72:                                #   in Loop: Header=BB18_70 Depth=3
	addq	$2, %rbp
	movq	%rbp, %rax
	jmp	.LBB18_78
	.p2align	4, 0x90
.LBB18_73:                              #   in Loop: Header=BB18_70 Depth=3
	cmpb	$45, %sil
	jne	.LBB18_77
# BB#74:                                #   in Loop: Header=BB18_70 Depth=3
	addq	$3, %rbp
	cmpq	%r13, %rbp
	jae	.LBB18_77
# BB#75:                                #   in Loop: Header=BB18_70 Depth=3
	cmpb	%r12b, %cl
	jbe	.LBB18_79
# BB#76:                                #   in Loop: Header=BB18_70 Depth=3
	movq	%rbp, %rax
	jmp	.LBB18_78
.LBB18_77:                              #   in Loop: Header=BB18_70 Depth=3
	cmpb	%r12b, %cl
	je	.LBB18_80
.LBB18_78:                              # %.backedge.i102
                                        #   in Loop: Header=BB18_70 Depth=3
	movq	%rax, %rbp
	leaq	1(%rbp), %rax
	cmpq	%r13, %rax
	jb	.LBB18_70
	jmp	.LBB18_81
.LBB18_79:                              #   in Loop: Header=BB18_70 Depth=3
	cmpb	%r12b, (%rbp)
	movq	%rbp, %rax
	jb	.LBB18_78
.LBB18_80:                              #   in Loop: Header=BB18_3 Depth=2
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %eax
	testl	%eax, %eax
	je	.LBB18_82
	jmp	.LBB18_160
.LBB18_81:                              # %._crit_edge.i103
                                        #   in Loop: Header=BB18_3 Depth=2
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %eax
	xorl	$1, %eax
	testl	%eax, %eax
	jne	.LBB18_160
.LBB18_82:                              #   in Loop: Header=BB18_3 Depth=2
	cmpq	%r13, %r15
	jae	.LBB18_94
# BB#83:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB18_3 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax), %r12d
	.p2align	4, 0x90
.LBB18_84:                              # %.lr.ph.i
                                        #   Parent Loop BB18_2 Depth=1
                                        #     Parent Loop BB18_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%r15), %eax
	movzbl	2(%r14), %esi
	cmpb	$37, %al
	jne	.LBB18_87
# BB#85:                                #   in Loop: Header=BB18_84 Depth=3
	movl	%r12d, %edi
	callq	match_class
	testl	%eax, %eax
	jne	.LBB18_95
# BB#86:                                #   in Loop: Header=BB18_84 Depth=3
	addq	$2, %r14
	movq	%r14, %r15
	jmp	.LBB18_92
	.p2align	4, 0x90
.LBB18_87:                              #   in Loop: Header=BB18_84 Depth=3
	cmpb	$45, %sil
	jne	.LBB18_91
# BB#88:                                #   in Loop: Header=BB18_84 Depth=3
	addq	$3, %r14
	cmpq	%r13, %r14
	jae	.LBB18_91
# BB#89:                                #   in Loop: Header=BB18_84 Depth=3
	cmpb	%r12b, %al
	jbe	.LBB18_93
# BB#90:                                #   in Loop: Header=BB18_84 Depth=3
	movq	%r14, %r15
	jmp	.LBB18_92
.LBB18_91:                              #   in Loop: Header=BB18_84 Depth=3
	cmpb	%r12b, %al
	je	.LBB18_95
.LBB18_92:                              # %.backedge.i
                                        #   in Loop: Header=BB18_84 Depth=3
	movq	%r15, %r14
	leaq	1(%r14), %r15
	cmpq	%r13, %r15
	jb	.LBB18_84
	jmp	.LBB18_94
.LBB18_93:                              #   in Loop: Header=BB18_84 Depth=3
	cmpb	%r12b, (%r14)
	movq	%r14, %r15
	jb	.LBB18_92
	jmp	.LBB18_95
.LBB18_94:                              # %._crit_edge.i
                                        #   in Loop: Header=BB18_3 Depth=2
	xorl	$1, %ebp
.LBB18_95:                              # %.loopexit131
                                        #   in Loop: Header=BB18_3 Depth=2
	testl	%ebp, %ebp
	movq	(%rsp), %r15            # 8-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %r13          # 8-byte Reload
	jne	.LBB18_3
	jmp	.LBB18_160
.LBB18_96:                              #   in Loop: Header=BB18_2 Depth=1
	cmpb	$49, %bl
	jb	.LBB18_99
# BB#97:                                #   in Loop: Header=BB18_2 Depth=1
	addq	$-49, %rbx
	cmpl	24(%r15), %ebx
	jge	.LBB18_99
# BB#98:                                #   in Loop: Header=BB18_2 Depth=1
	movq	%rbx, %rax
	shlq	$4, %rax
	movq	40(%r15,%rax), %rbp
	cmpq	$-1, %rbp
	jne	.LBB18_100
.LBB18_99:                              #   in Loop: Header=BB18_2 Depth=1
	movq	16(%r15), %rdi
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	callq	luaL_error
	movl	%eax, %ebx
	movslq	%ebx, %rax
	shlq	$4, %rax
	movq	40(%r15,%rax), %rbp
.LBB18_100:                             # %check_capture.exit.i
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	8(%r15), %rax
	subq	%r14, %rax
	cmpq	%rbp, %rax
	jb	.LBB18_160
# BB#101:                               #   in Loop: Header=BB18_2 Depth=1
	movslq	%ebx, %rax
	shlq	$4, %rax
	movq	32(%r15,%rax), %rdi
	movq	%r14, %rsi
	movq	%rbp, %rdx
	callq	memcmp
	movq	%r14, %rcx
	addq	%rbp, %rcx
	je	.LBB18_160
# BB#102:                               #   in Loop: Header=BB18_2 Depth=1
	testl	%eax, %eax
	movl	$0, %r14d
	je	.LBB18_1
	jmp	.LBB18_161
.LBB18_103:                             #   in Loop: Header=BB18_2 Depth=1
	testb	%al, %al
	movq	%r13, %r14
	jne	.LBB18_2
	jmp	.LBB18_160
.LBB18_104:                             #   in Loop: Header=BB18_2 Depth=1
	movb	2(%r12), %al
	testb	%al, %al
	je	.LBB18_106
# BB#105:                               #   in Loop: Header=BB18_2 Depth=1
	cmpb	$0, 3(%r12)
	jne	.LBB18_107
.LBB18_106:                             #   in Loop: Header=BB18_2 Depth=1
	movq	16(%r15), %rdi
	movl	$.L.str.23, %esi
	xorl	%eax, %eax
	callq	luaL_error
	movb	2(%r12), %al
.LBB18_107:                             #   in Loop: Header=BB18_2 Depth=1
	cmpb	%al, (%r14)
	jne	.LBB18_160
# BB#108:                               #   in Loop: Header=BB18_2 Depth=1
	movq	8(%r15), %rcx
	cmpq	%rcx, %r13
	jae	.LBB18_160
# BB#109:                               # %.lr.ph.i115.preheader
                                        #   in Loop: Header=BB18_2 Depth=1
	movb	3(%r12), %dl
	movl	$1, %esi
	.p2align	4, 0x90
.LBB18_110:                             # %.lr.ph.i115
                                        #   Parent Loop BB18_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r13), %ebx
	cmpb	%dl, %bl
	jne	.LBB18_112
# BB#111:                               #   in Loop: Header=BB18_110 Depth=2
	decl	%esi
	jne	.LBB18_113
	jmp	.LBB18_114
	.p2align	4, 0x90
.LBB18_112:                             #   in Loop: Header=BB18_110 Depth=2
	xorl	%edi, %edi
	cmpb	%al, %bl
	sete	%dil
	addl	%edi, %esi
.LBB18_113:                             # %.backedge.i116
                                        #   in Loop: Header=BB18_110 Depth=2
	incq	%r13
	cmpq	%rcx, %r13
	jb	.LBB18_110
	jmp	.LBB18_160
.LBB18_114:                             # %matchbalance.exit
                                        #   in Loop: Header=BB18_2 Depth=1
	incq	%r13
	addq	$4, %r12
	movq	%r12, %rbx
	movq	%r13, %r14
	jmp	.LBB18_2
.LBB18_115:
	movq	%rax, %r14
	jmp	.LBB18_161
.LBB18_116:
	cmpb	$41, 1(%r12)
	jne	.LBB18_155
# BB#117:
	addq	$2, %r12
	movslq	24(%r15), %rbx
	cmpq	$32, %rbx
	jl	.LBB18_119
# BB#118:
	movq	16(%r15), %rdi
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	callq	luaL_error
.LBB18_119:
	movq	%rbx, %rax
	shlq	$4, %rax
	movq	%r14, 32(%r15,%rax)
	movq	$-2, 40(%r15,%rax)
	jmp	.LBB18_158
.LBB18_120:
	incq	%r12
	movslq	24(%r15), %rax
	movq	%rax, %rcx
	shlq	$4, %rcx
	leaq	24(%r15,%rcx), %rcx
	.p2align	4, 0x90
.LBB18_121:                             # =>This Inner Loop Header: Depth=1
	testq	%rax, %rax
	jle	.LBB18_152
# BB#122:                               #   in Loop: Header=BB18_121 Depth=1
	decq	%rax
	cmpq	$-1, (%rcx)
	leaq	-16(%rcx), %rcx
	jne	.LBB18_121
	jmp	.LBB18_153
.LBB18_123:
	xorl	%eax, %eax
	cmpq	8(%r15), %r14
	cmovneq	%rax, %r14
	jmp	.LBB18_161
.LBB18_124:
	movq	%r15, %rdi
	movq	%r14, %rsi
	jmp	.LBB18_127
.LBB18_125:
	testb	%al, %al
	je	.LBB18_160
# BB#126:
	movq	%r15, %rdi
	movq	%r13, %rsi
.LBB18_127:
	movq	%r12, %rdx
	movq	%rbx, %rcx
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	max_expand              # TAILCALL
.LBB18_128:                             # %.preheader
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	leaq	1(%rbx), %r13
	decq	%rbx
	movq	%r14, %rbp
	jmp	.LBB18_130
	.p2align	4, 0x90
.LBB18_129:                             # %.thread124
                                        #   in Loop: Header=BB18_130 Depth=1
	incq	%rbp
.LBB18_130:                             # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_137 Depth 2
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	callq	match
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB18_161
# BB#131:                               # %.lr.ph
                                        #   in Loop: Header=BB18_130 Depth=1
	cmpq	8(%r15), %rbp
	jae	.LBB18_160
# BB#132:                               #   in Loop: Header=BB18_130 Depth=1
	movb	(%r12), %al
	movsbl	%al, %ecx
	cmpl	$46, %ecx
	je	.LBB18_129
# BB#133:                               #   in Loop: Header=BB18_130 Depth=1
	movzbl	(%rbp), %edi
	cmpl	$37, %ecx
	je	.LBB18_148
# BB#134:                               #   in Loop: Header=BB18_130 Depth=1
	cmpl	$91, %ecx
	jne	.LBB18_149
# BB#135:                               #   in Loop: Header=BB18_130 Depth=1
	xorl	%ecx, %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpb	$94, (%rax)
	movq	%r12, %r15
	cmoveq	%rax, %r15
	setne	%cl
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	jmp	.LBB18_137
.LBB18_136:                             #   in Loop: Header=BB18_137 Depth=2
	addq	$2, %r15
	movq	%r15, %rax
	movl	%r14d, %edi
.LBB18_137:                             #   Parent Loop BB18_130 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%r15), %rax
	cmpq	%rbx, %rax
	jae	.LBB18_150
# BB#138:                               # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB18_137 Depth=2
	movzbl	(%rax), %ecx
	movzbl	2(%r15), %esi
	cmpb	$37, %cl
	jne	.LBB18_140
# BB#139:                               #   in Loop: Header=BB18_137 Depth=2
	movl	%edi, %r14d
	callq	match_class
	testl	%eax, %eax
	je	.LBB18_136
	jmp	.LBB18_147
.LBB18_140:                             #   in Loop: Header=BB18_137 Depth=2
	cmpb	$45, %sil
	jne	.LBB18_144
# BB#141:                               #   in Loop: Header=BB18_137 Depth=2
	addq	$3, %r15
	cmpq	%rbx, %r15
	jae	.LBB18_144
# BB#142:                               #   in Loop: Header=BB18_137 Depth=2
	cmpb	%dil, %cl
	jbe	.LBB18_146
# BB#143:                               #   in Loop: Header=BB18_137 Depth=2
	movq	%r15, %rax
	jmp	.LBB18_137
.LBB18_144:                             #   in Loop: Header=BB18_137 Depth=2
	cmpb	%dil, %cl
	je	.LBB18_147
.LBB18_145:                             # %.backedge.i.i.i
                                        #   in Loop: Header=BB18_137 Depth=2
	movq	%rax, %r15
	jmp	.LBB18_137
.LBB18_146:                             #   in Loop: Header=BB18_137 Depth=2
	cmpb	%dil, (%r15)
	movq	%r15, %rax
	jb	.LBB18_145
.LBB18_147:                             #   in Loop: Header=BB18_130 Depth=1
	movq	(%rsp), %r15            # 8-byte Reload
	movl	32(%rsp), %eax          # 4-byte Reload
	jmp	.LBB18_151
.LBB18_148:                             #   in Loop: Header=BB18_130 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax), %esi
	callq	match_class
	jmp	.LBB18_151
.LBB18_149:                             #   in Loop: Header=BB18_130 Depth=1
	cmpb	%dil, %al
	movl	$0, %eax
	sete	%al
	jmp	.LBB18_151
.LBB18_150:                             # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB18_130 Depth=1
	movl	32(%rsp), %eax          # 4-byte Reload
	xorl	$1, %eax
	movq	(%rsp), %r15            # 8-byte Reload
.LBB18_151:                             # %.loopexit
                                        #   in Loop: Header=BB18_130 Depth=1
	incq	%rbp
	testl	%eax, %eax
	movl	$0, %r14d
	jne	.LBB18_130
	jmp	.LBB18_161
.LBB18_152:
	movq	16(%r15), %rdi
	movl	$.L.str.22, %esi
	xorl	%eax, %eax
	callq	luaL_error
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
.LBB18_153:                             # %capture_to_close.exit.i
	movslq	%eax, %rbx
	shlq	$4, %rbx
	movq	%r14, %rax
	subq	32(%r15,%rbx), %rax
	movq	%rax, 40(%r15,%rbx)
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	match
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB18_161
# BB#154:
	leaq	40(%r15,%rbx), %rax
	movq	$-1, (%rax)
	jmp	.LBB18_160
.LBB18_155:
	incq	%r12
	movslq	24(%r15), %rbx
	cmpq	$32, %rbx
	jl	.LBB18_157
# BB#156:
	movq	16(%r15), %rdi
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	callq	luaL_error
.LBB18_157:
	movq	%rbx, %rax
	shlq	$4, %rax
	movq	%r14, 32(%r15,%rax)
	movq	$-1, 40(%r15,%rax)
.LBB18_158:
	leal	1(%rbx), %eax
	movl	%eax, 24(%r15)
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	match
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB18_161
# BB#159:
	decl	24(%r15)
.LBB18_160:
	xorl	%r14d, %r14d
.LBB18_161:                             # %start_capture.exit117
	movq	%r14, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	match, .Lfunc_end18-match
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI18_0:
	.quad	.LBB18_161
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_6
	.quad	.LBB18_17
	.quad	.LBB18_8
	.quad	.LBB18_8
	.quad	.LBB18_116
	.quad	.LBB18_120
.LJTI18_1:
	.quad	.LBB18_124
	.quad	.LBB18_125
	.quad	.LBB18_103
	.quad	.LBB18_128
	.quad	.LBB18_103
	.quad	.LBB18_103
	.quad	.LBB18_103
	.quad	.LBB18_103
	.quad	.LBB18_103
	.quad	.LBB18_103
	.quad	.LBB18_103
	.quad	.LBB18_103
	.quad	.LBB18_103
	.quad	.LBB18_103
	.quad	.LBB18_103
	.quad	.LBB18_103
	.quad	.LBB18_103
	.quad	.LBB18_103
	.quad	.LBB18_103
	.quad	.LBB18_103
	.quad	.LBB18_103
	.quad	.LBB18_48

	.text
	.p2align	4, 0x90
	.type	max_expand,@function
max_expand:                             # @max_expand
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi134:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi135:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi136:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi137:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi138:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi139:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi140:
	.cfi_def_cfa_offset 112
.Lcfi141:
	.cfi_offset %rbx, -56
.Lcfi142:
	.cfi_offset %r12, -48
.Lcfi143:
	.cfi_offset %r13, -40
.Lcfi144:
	.cfi_offset %r14, -32
.Lcfi145:
	.cfi_offset %r15, -24
.Lcfi146:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	8(%r15), %rcx
	cmpq	%r14, %rcx
	jbe	.LBB19_1
# BB#2:                                 # %.lr.ph
	movsbl	(%rdx), %esi
	cmpl	$46, %esi
	jne	.LBB19_3
# BB#8:                                 # %.lr.ph.split.us.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB19_9:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%r14,%r12), %rax
	incq	%r12
	cmpq	%rcx, %rax
	jb	.LBB19_9
	jmp	.LBB19_27
.LBB19_1:
	xorl	%r12d, %r12d
	jmp	.LBB19_27
.LBB19_3:                               # %.lr.ph.split.preheader
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	leaq	1(%rdx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	-1(%rbx), %rdx
	xorl	%r12d, %r12d
	movq	%r14, %rax
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	%esi, 28(%rsp)          # 4-byte Spill
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	jmp	.LBB19_4
.LBB19_14:                              #   in Loop: Header=BB19_4 Depth=1
	movq	%rbx, %r15
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB19_26
	.p2align	4, 0x90
.LBB19_4:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_11 Depth 2
	movzbl	(%rax), %ebp
	cmpl	$91, %esi
	je	.LBB19_10
# BB#5:                                 # %.lr.ph.split
                                        #   in Loop: Header=BB19_4 Depth=1
	cmpl	$46, %esi
	je	.LBB19_32
# BB#6:                                 # %.lr.ph.split
                                        #   in Loop: Header=BB19_4 Depth=1
	cmpl	$37, %esi
	jne	.LBB19_25
# BB#7:                                 #   in Loop: Header=BB19_4 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax), %esi
	movl	%ebp, %edi
	callq	match_class
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	%eax, %r13d
	jmp	.LBB19_26
	.p2align	4, 0x90
.LBB19_10:                              #   in Loop: Header=BB19_4 Depth=1
	movq	%r15, %rbx
	xorl	%r13d, %r13d
	movq	40(%rsp), %r15          # 8-byte Reload
	cmpb	$94, 1(%r15)
	cmoveq	32(%rsp), %r15          # 8-byte Folded Reload
	setne	%r13b
	jmp	.LBB19_11
	.p2align	4, 0x90
.LBB19_23:                              # %.backedge.i.i
                                        #   in Loop: Header=BB19_11 Depth=2
	movq	%rax, %r15
.LBB19_11:                              #   Parent Loop BB19_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%r15), %rax
	cmpq	%rdx, %rax
	jae	.LBB19_24
# BB#12:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB19_11 Depth=2
	movzbl	(%rax), %ecx
	movzbl	2(%r15), %esi
	cmpb	$37, %cl
	jne	.LBB19_15
# BB#13:                                #   in Loop: Header=BB19_11 Depth=2
	movl	%ebp, %edi
	callq	match_class
	testl	%eax, %eax
	jne	.LBB19_14
# BB#22:                                #   in Loop: Header=BB19_11 Depth=2
	addq	$2, %r15
	movq	%r15, %rax
	movq	16(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB19_23
	.p2align	4, 0x90
.LBB19_15:                              #   in Loop: Header=BB19_11 Depth=2
	cmpb	$45, %sil
	jne	.LBB19_20
# BB#16:                                #   in Loop: Header=BB19_11 Depth=2
	addq	$3, %r15
	cmpq	%rdx, %r15
	jae	.LBB19_20
# BB#17:                                #   in Loop: Header=BB19_11 Depth=2
	cmpb	%bpl, %cl
	jbe	.LBB19_19
# BB#18:                                #   in Loop: Header=BB19_11 Depth=2
	movq	%r15, %rax
	jmp	.LBB19_11
	.p2align	4, 0x90
.LBB19_20:                              #   in Loop: Header=BB19_11 Depth=2
	cmpb	%bpl, %cl
	jne	.LBB19_23
	jmp	.LBB19_21
.LBB19_19:                              #   in Loop: Header=BB19_11 Depth=2
	cmpb	%bpl, (%r15)
	movq	%r15, %rax
	jb	.LBB19_23
	jmp	.LBB19_21
	.p2align	4, 0x90
.LBB19_25:                              #   in Loop: Header=BB19_4 Depth=1
	xorl	%r13d, %r13d
	cmpb	%bpl, %sil
	sete	%r13b
	jmp	.LBB19_26
.LBB19_24:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB19_4 Depth=1
	xorl	$1, %r13d
.LBB19_21:                              #   in Loop: Header=BB19_4 Depth=1
	movq	%rbx, %r15
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB19_26:                              # %singlematch.exit
                                        #   in Loop: Header=BB19_4 Depth=1
	testl	%r13d, %r13d
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
	je	.LBB19_27
.LBB19_32:                              # %singlematch.exit.thread
                                        #   in Loop: Header=BB19_4 Depth=1
	leaq	1(%r14,%r12), %rax
	incq	%r12
	cmpq	%rcx, %rax
	jb	.LBB19_4
.LBB19_27:                              # %.critedge.preheader
	incq	%rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB19_28:                              # %.critedge
                                        # =>This Inner Loop Header: Depth=1
	testq	%r12, %r12
	js	.LBB19_31
# BB#29:                                #   in Loop: Header=BB19_28 Depth=1
	leaq	(%r14,%r12), %rsi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	match
	xorl	%ecx, %ecx
	testq	%rax, %rax
	sete	%cl
	subq	%rcx, %r12
	testq	%rax, %rax
	je	.LBB19_28
# BB#30:
	movq	%rax, %rbp
.LBB19_31:
	movq	%rbp, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	max_expand, .Lfunc_end19-max_expand
	.cfi_endproc

	.p2align	4, 0x90
	.type	match_class,@function
match_class:                            # @match_class
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi147:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi148:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi149:
	.cfi_def_cfa_offset 32
.Lcfi150:
	.cfi_offset %rbx, -32
.Lcfi151:
	.cfi_offset %r14, -24
.Lcfi152:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movl	%edi, %r14d
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	movl	%ebx, %eax
	ja	.LBB20_2
# BB#1:
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	movl	(%rax,%rcx,4), %eax
.LBB20_2:                               # %tolower.exit
	addl	$-97, %eax
	cmpl	$25, %eax
	ja	.LBB20_13
# BB#3:                                 # %tolower.exit
	jmpq	*.LJTI20_0(,%rax,8)
.LBB20_14:
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movslq	%r14d, %rdx
	movzwl	(%rcx,%rdx,2), %ebp
	andl	$1024, %ebp             # imm = 0x400
	jmp	.LBB20_15
.LBB20_13:
	xorl	%eax, %eax
	cmpl	%r14d, %ebx
	sete	%al
	jmp	.LBB20_16
.LBB20_4:
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movslq	%r14d, %rdx
	movzwl	(%rcx,%rdx,2), %ebp
	andl	$2, %ebp
	jmp	.LBB20_15
.LBB20_5:
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movslq	%r14d, %rdx
	movzwl	(%rcx,%rdx,2), %ebp
	andl	$2048, %ebp             # imm = 0x800
	jmp	.LBB20_15
.LBB20_6:
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movslq	%r14d, %rdx
	movzwl	(%rcx,%rdx,2), %ebp
	andl	$512, %ebp              # imm = 0x200
	jmp	.LBB20_15
.LBB20_7:
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movslq	%r14d, %rdx
	movzwl	(%rcx,%rdx,2), %ebp
	andl	$4, %ebp
	jmp	.LBB20_15
.LBB20_8:
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movslq	%r14d, %rdx
	movzwl	(%rcx,%rdx,2), %ebp
	andl	$8192, %ebp             # imm = 0x2000
	jmp	.LBB20_15
.LBB20_9:
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movslq	%r14d, %rdx
	movzwl	(%rcx,%rdx,2), %ebp
	andl	$256, %ebp              # imm = 0x100
	jmp	.LBB20_15
.LBB20_10:
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movslq	%r14d, %rdx
	movzwl	(%rcx,%rdx,2), %ebp
	andl	$8, %ebp
	jmp	.LBB20_15
.LBB20_11:
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movslq	%r14d, %rdx
	movzwl	(%rcx,%rdx,2), %ebp
	andl	$4096, %ebp             # imm = 0x1000
	jmp	.LBB20_15
.LBB20_12:
	xorl	%ebp, %ebp
	testl	%r14d, %r14d
	sete	%bpl
	callq	__ctype_b_loc
.LBB20_15:
	movq	(%rax), %rcx
	movslq	%ebx, %rdx
	xorl	%eax, %eax
	testl	%ebp, %ebp
	sete	%al
	testb	$2, 1(%rcx,%rdx,2)
	cmovnel	%ebp, %eax
.LBB20_16:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end20:
	.size	match_class, .Lfunc_end20-match_class
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI20_0:
	.quad	.LBB20_14
	.quad	.LBB20_13
	.quad	.LBB20_4
	.quad	.LBB20_5
	.quad	.LBB20_13
	.quad	.LBB20_13
	.quad	.LBB20_13
	.quad	.LBB20_13
	.quad	.LBB20_13
	.quad	.LBB20_13
	.quad	.LBB20_13
	.quad	.LBB20_6
	.quad	.LBB20_13
	.quad	.LBB20_13
	.quad	.LBB20_13
	.quad	.LBB20_7
	.quad	.LBB20_13
	.quad	.LBB20_13
	.quad	.LBB20_8
	.quad	.LBB20_13
	.quad	.LBB20_9
	.quad	.LBB20_13
	.quad	.LBB20_10
	.quad	.LBB20_11
	.quad	.LBB20_13
	.quad	.LBB20_12

	.text
	.p2align	4, 0x90
	.type	push_onecapture,@function
push_onecapture:                        # @push_onecapture
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi153:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi154:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi155:
	.cfi_def_cfa_offset 32
.Lcfi156:
	.cfi_offset %rbx, -32
.Lcfi157:
	.cfi_offset %r14, -24
.Lcfi158:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	cmpl	%esi, 24(%rbx)
	jle	.LBB21_1
# BB#4:
	movslq	%esi, %r15
	shlq	$4, %r15
	movq	40(%rbx,%r15), %r14
	cmpq	$-1, %r14
	je	.LBB21_5
# BB#6:
	movq	16(%rbx), %rdi
	movq	32(%rbx,%r15), %rsi
	cmpq	$-2, %r14
	jne	.LBB21_7
# BB#9:
	subq	(%rbx), %rsi
	incq	%rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	lua_pushinteger         # TAILCALL
.LBB21_1:
	movq	16(%rbx), %rdi
	testl	%esi, %esi
	je	.LBB21_2
# BB#3:
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	luaL_error              # TAILCALL
.LBB21_5:                               # %.thread
	movq	16(%rbx), %rdi
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	callq	luaL_error
	movq	16(%rbx), %rdi
	movq	32(%rbx,%r15), %rsi
.LBB21_7:
	movq	%r14, %rdx
	jmp	.LBB21_8
.LBB21_2:
	subq	%rdx, %rcx
	movq	%rdx, %rsi
	movq	%rcx, %rdx
.LBB21_8:
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	lua_pushlstring         # TAILCALL
.Lfunc_end21:
	.size	push_onecapture, .Lfunc_end21-push_onecapture
	.cfi_endproc

	.p2align	4, 0x90
	.type	gmatch_aux,@function
gmatch_aux:                             # @gmatch_aux
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi159:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi160:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi161:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi162:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi163:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi164:
	.cfi_def_cfa_offset 56
	subq	$568, %rsp              # imm = 0x238
.Lcfi165:
	.cfi_def_cfa_offset 624
.Lcfi166:
	.cfi_offset %rbx, -56
.Lcfi167:
	.cfi_offset %r12, -48
.Lcfi168:
	.cfi_offset %r13, -40
.Lcfi169:
	.cfi_offset %r14, -32
.Lcfi170:
	.cfi_offset %r15, -24
.Lcfi171:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	leaq	16(%rsp), %rdx
	movl	$-10003, %esi           # imm = 0xD8ED
	callq	lua_tolstring
	movq	%rax, %r13
	xorl	%r14d, %r14d
	movl	$-10004, %esi           # imm = 0xD8EC
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	lua_tolstring
	movq	%rax, %rbp
	movq	%r15, 40(%rsp)
	movq	%r13, 24(%rsp)
	movq	16(%rsp), %r12
	leaq	(%r13,%r12), %rax
	movq	%rax, 32(%rsp)
	movl	$-10005, %esi           # imm = 0xD8EB
	movq	%r15, %rdi
	callq	lua_tointeger
	movq	%rax, %rbx
	cmpq	%r12, %rbx
	jg	.LBB22_7
# BB#1:                                 # %.lr.ph
	movq	%r13, 8(%rsp)           # 8-byte Spill
	addq	%r13, %rbx
	leaq	24(%rsp), %r13
	.p2align	4, 0x90
.LBB22_2:                               # =>This Inner Loop Header: Depth=1
	movl	$0, 48(%rsp)
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	match
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.LBB22_3
# BB#6:                                 # %push_captures.exit
                                        #   in Loop: Header=BB22_2 Depth=1
	incq	%rbx
	cmpq	32(%rsp), %rbx
	jbe	.LBB22_2
	jmp	.LBB22_7
.LBB22_3:
	movq	%r12, %rax
	subq	8(%rsp), %rax           # 8-byte Folded Reload
	xorl	%esi, %esi
	cmpq	%r12, %rbx
	sete	%sil
	addq	%rax, %rsi
	movq	%r15, %rdi
	callq	lua_pushinteger
	movl	$-10005, %esi           # imm = 0xD8EB
	movq	%r15, %rdi
	callq	lua_replace
	movl	48(%rsp), %eax
	testl	%eax, %eax
	movl	$1, %r14d
	cmovnel	%eax, %r14d
	testq	%rbx, %rbx
	cmovel	%eax, %r14d
	movq	40(%rsp), %rdi
	movl	$.L.str.21, %edx
	movl	%r14d, %esi
	callq	luaL_checkstack
	testl	%r14d, %r14d
	jle	.LBB22_7
# BB#4:                                 # %.lr.ph.i.preheader
	xorl	%ebp, %ebp
	leaq	24(%rsp), %r15
	.p2align	4, 0x90
.LBB22_5:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movl	%ebp, %esi
	movq	%rbx, %rdx
	movq	%r12, %rcx
	callq	push_onecapture
	incl	%ebp
	cmpl	%ebp, %r14d
	jne	.LBB22_5
.LBB22_7:                               # %push_captures.exit.thread
	movl	%r14d, %eax
	addq	$568, %rsp              # imm = 0x238
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	gmatch_aux, .Lfunc_end22-gmatch_aux
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"string"
	.size	.L.str, 7

	.type	strlib,@object          # @strlib
	.section	.rodata,"a",@progbits
	.p2align	4
strlib:
	.quad	.L.str.3
	.quad	str_byte
	.quad	.L.str.4
	.quad	str_char
	.quad	.L.str.5
	.quad	str_dump
	.quad	.L.str.6
	.quad	str_find
	.quad	.L.str.7
	.quad	str_format
	.quad	.L.str.2
	.quad	gfind_nodef
	.quad	.L.str.1
	.quad	gmatch
	.quad	.L.str.8
	.quad	str_gsub
	.quad	.L.str.9
	.quad	str_len
	.quad	.L.str.10
	.quad	str_lower
	.quad	.L.str.11
	.quad	str_match
	.quad	.L.str.12
	.quad	str_rep
	.quad	.L.str.13
	.quad	str_reverse
	.quad	.L.str.14
	.quad	str_sub
	.quad	.L.str.15
	.quad	str_upper
	.zero	16
	.size	strlib, 256

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"gmatch"
	.size	.L.str.1, 7

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"gfind"
	.size	.L.str.2, 6

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"byte"
	.size	.L.str.3, 5

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"char"
	.size	.L.str.4, 5

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"dump"
	.size	.L.str.5, 5

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"find"
	.size	.L.str.6, 5

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"format"
	.size	.L.str.7, 7

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"gsub"
	.size	.L.str.8, 5

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"len"
	.size	.L.str.9, 4

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"lower"
	.size	.L.str.10, 6

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"match"
	.size	.L.str.11, 6

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"rep"
	.size	.L.str.12, 4

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"reverse"
	.size	.L.str.13, 8

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"sub"
	.size	.L.str.14, 4

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"upper"
	.size	.L.str.15, 6

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"string slice too long"
	.size	.L.str.16, 22

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"invalid value"
	.size	.L.str.17, 14

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"unable to dump given function"
	.size	.L.str.18, 30

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"^$*+?.([%-"
	.size	.L.str.19, 11

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"missing '[' after '%%f' in pattern"
	.size	.L.str.20, 35

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"too many captures"
	.size	.L.str.21, 18

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"invalid pattern capture"
	.size	.L.str.22, 24

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"unbalanced pattern"
	.size	.L.str.23, 19

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"malformed pattern (ends with '%%')"
	.size	.L.str.24, 35

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"malformed pattern (missing ']')"
	.size	.L.str.25, 32

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"invalid capture index"
	.size	.L.str.26, 22

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"unfinished capture"
	.size	.L.str.27, 19

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"invalid option '%%%c' to 'format'"
	.size	.L.str.28, 34

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"invalid format (repeated flags)"
	.size	.L.str.30, 32

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"invalid format (width or precision too long)"
	.size	.L.str.31, 45

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"\\r"
	.size	.L.str.33, 3

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"\\000"
	.size	.L.str.34, 5

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"'string.gfind' was renamed to 'string.gmatch'"
	.size	.L.str.35, 46

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"string/function/table expected"
	.size	.L.str.36, 31

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"invalid replacement value (a %s)"
	.size	.L.str.37, 33

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.zero	1
	.size	.L.str.38, 1

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"__index"
	.size	.L.str.39, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
