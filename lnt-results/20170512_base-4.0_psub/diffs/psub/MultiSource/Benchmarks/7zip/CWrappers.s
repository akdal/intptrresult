	.text
	.file	"CWrappers.bc"
	.globl	_ZN21CCompressProgressWrapC2EP21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN21CCompressProgressWrapC2EP21ICompressProgressInfo,@function
_ZN21CCompressProgressWrapC2EP21ICompressProgressInfo: # @_ZN21CCompressProgressWrapC2EP21ICompressProgressInfo
	.cfi_startproc
# BB#0:
	movq	$_ZL16CompressProgressPvyy, (%rdi)
	movq	%rsi, 8(%rdi)
	movl	$0, 16(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN21CCompressProgressWrapC2EP21ICompressProgressInfo, .Lfunc_end0-_ZN21CCompressProgressWrapC2EP21ICompressProgressInfo
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL16CompressProgressPvyy,@function
_ZL16CompressProgressPvyy:              # @_ZL16CompressProgressPvyy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	%rsi, 8(%rsp)
	movq	%rdx, (%rsp)
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	xorl	%ecx, %ecx
	cmpq	$-1, %rsi
	leaq	8(%rsp), %rsi
	cmoveq	%rcx, %rsi
	cmpq	$-1, %rdx
	movq	%rsp, %rdx
	cmoveq	%rcx, %rdx
	callq	*40(%rax)
	movl	%eax, 16(%rbx)
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZL16CompressProgressPvyy, .Lfunc_end1-_ZL16CompressProgressPvyy
	.cfi_endproc

	.globl	_Z15HRESULT_To_SResii
	.p2align	4, 0x90
	.type	_Z15HRESULT_To_SResii,@function
_Z15HRESULT_To_SResii:                  # @_Z15HRESULT_To_SResii
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	jns	.LBB2_6
# BB#1:
	cmpl	$-2147467260, %edi      # imm = 0x80004004
	je	.LBB2_5
# BB#2:
	cmpl	$-2147024882, %edi      # imm = 0x8007000E
	je	.LBB2_9
# BB#3:
	cmpl	$-2147024809, %edi      # imm = 0x80070057
	jne	.LBB2_7
# BB#4:
	movl	$5, %edi
	movl	%edi, %eax
	retq
.LBB2_6:
	cmpl	$2, %edi
	jb	.LBB2_8
.LBB2_7:
	movl	%esi, %edi
.LBB2_8:
	movl	%edi, %eax
	retq
.LBB2_5:
	movl	$10, %edi
	movl	%edi, %eax
	retq
.LBB2_9:
	movl	$2, %edi
	movl	%edi, %eax
	retq
.Lfunc_end2:
	.size	_Z15HRESULT_To_SResii, .Lfunc_end2-_Z15HRESULT_To_SResii
	.cfi_endproc

	.globl	_ZN16CSeqInStreamWrapC2EP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN16CSeqInStreamWrapC2EP19ISequentialInStream,@function
_ZN16CSeqInStreamWrapC2EP19ISequentialInStream: # @_ZN16CSeqInStreamWrapC2EP19ISequentialInStream
	.cfi_startproc
# BB#0:
	movq	$_ZL6MyReadPvS_Pm, (%rdi)
	movq	%rsi, 8(%rdi)
	retq
.Lfunc_end3:
	.size	_ZN16CSeqInStreamWrapC2EP19ISequentialInStream, .Lfunc_end3-_ZN16CSeqInStreamWrapC2EP19ISequentialInStream
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL6MyReadPvS_Pm,@function
_ZL6MyReadPvS_Pm:                       # @_ZL6MyReadPvS_Pm
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 32
.Lcfi6:
	.cfi_offset %rbx, -24
.Lcfi7:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movq	(%r14), %rax
	testq	$-2147483648, %rax      # imm = 0x80000000
	movl	$-2147483648, %edx      # imm = 0x80000000
	cmovel	%eax, %edx
	movl	%edx, 4(%rsp)
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	leaq	4(%rsp), %rcx
	callq	*40(%rax)
	movl	%eax, 16(%rbx)
	movl	4(%rsp), %ecx
	movq	%rcx, (%r14)
	cmpl	$-2147024810, %eax      # imm = 0x80070056
	jle	.LBB4_1
# BB#4:
	cmpl	$-2147024809, %eax      # imm = 0x80070057
	je	.LBB4_8
# BB#5:
	xorl	%ecx, %ecx
	testl	%eax, %eax
	je	.LBB4_11
# BB#6:
	cmpl	$1, %eax
	jne	.LBB4_7
# BB#10:                                # %_Z15HRESULT_To_SResii.exit.fold.split
	movl	$1, %ecx
	jmp	.LBB4_11
.LBB4_1:
	cmpl	$-2147467260, %eax      # imm = 0x80004004
	je	.LBB4_9
# BB#2:
	cmpl	$-2147024882, %eax      # imm = 0x8007000E
	jne	.LBB4_7
# BB#3:
	movl	$2, %ecx
	jmp	.LBB4_11
.LBB4_8:
	movl	$5, %ecx
	jmp	.LBB4_11
.LBB4_9:
	movl	$10, %ecx
	jmp	.LBB4_11
.LBB4_7:
	movl	$8, %ecx
.LBB4_11:                               # %_Z15HRESULT_To_SResii.exit
	movl	%ecx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	_ZL6MyReadPvS_Pm, .Lfunc_end4-_ZL6MyReadPvS_Pm
	.cfi_endproc

	.globl	_ZN17CSeqOutStreamWrapC2EP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZN17CSeqOutStreamWrapC2EP20ISequentialOutStream,@function
_ZN17CSeqOutStreamWrapC2EP20ISequentialOutStream: # @_ZN17CSeqOutStreamWrapC2EP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	movq	$_ZL7MyWritePvPKvm, (%rdi)
	movq	%rsi, 8(%rdi)
	movl	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	retq
.Lfunc_end5:
	.size	_ZN17CSeqOutStreamWrapC2EP20ISequentialOutStream, .Lfunc_end5-_ZN17CSeqOutStreamWrapC2EP20ISequentialOutStream
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL7MyWritePvPKvm,@function
_ZL7MyWritePvPKvm:                      # @_ZL7MyWritePvPKvm
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -24
.Lcfi12:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_3
# BB#1:
	movq	%r14, %rdx
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	movl	%eax, 16(%rbx)
	testl	%eax, %eax
	je	.LBB6_4
# BB#2:
	xorl	%r14d, %r14d
	jmp	.LBB6_5
.LBB6_3:
	movl	$0, 16(%rbx)
.LBB6_4:
	addq	%r14, 24(%rbx)
.LBB6_5:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	_ZL7MyWritePvPKvm, .Lfunc_end6-_ZL7MyWritePvPKvm
	.cfi_endproc

	.globl	_Z13SResToHRESULTi
	.p2align	4, 0x90
	.type	_Z13SResToHRESULTi,@function
_Z13SResToHRESULTi:                     # @_Z13SResToHRESULTi
	.cfi_startproc
# BB#0:
	cmpl	$10, %edi
	ja	.LBB7_2
# BB#1:                                 # %switch.lookup
	movslq	%edi, %rax
	movl	.Lswitch.table(,%rax,4), %eax
	retq
.LBB7_2:
	movl	$-2147467259, %eax      # imm = 0x80004005
	retq
.Lfunc_end7:
	.size	_Z13SResToHRESULTi, .Lfunc_end7-_Z13SResToHRESULTi
	.cfi_endproc

	.globl	_ZN17CSeekInStreamWrapC2EP9IInStream
	.p2align	4, 0x90
	.type	_ZN17CSeekInStreamWrapC2EP9IInStream,@function
_ZN17CSeekInStreamWrapC2EP9IInStream:   # @_ZN17CSeekInStreamWrapC2EP9IInStream
	.cfi_startproc
# BB#0:
	movq	%rsi, 16(%rdi)
	movq	$_ZL17InStreamWrap_ReadPvS_Pm, (%rdi)
	movq	$_ZL17InStreamWrap_SeekPvPx7ESzSeek, 8(%rdi)
	movl	$0, 24(%rdi)
	retq
.Lfunc_end8:
	.size	_ZN17CSeekInStreamWrapC2EP9IInStream, .Lfunc_end8-_ZN17CSeekInStreamWrapC2EP9IInStream
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL17InStreamWrap_ReadPvS_Pm,@function
_ZL17InStreamWrap_ReadPvS_Pm:           # @_ZL17InStreamWrap_ReadPvS_Pm
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movq	(%r14), %rax
	testq	$-2147483648, %rax      # imm = 0x80000000
	movl	$-2147483648, %edx      # imm = 0x80000000
	cmovel	%eax, %edx
	movl	%edx, 4(%rsp)
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	leaq	4(%rsp), %rcx
	callq	*40(%rax)
	movl	%eax, 24(%rbx)
	movl	4(%rsp), %ecx
	movq	%rcx, (%r14)
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	shll	$3, %ecx
	movl	%ecx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	_ZL17InStreamWrap_ReadPvS_Pm, .Lfunc_end9-_ZL17InStreamWrap_ReadPvS_Pm
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL17InStreamWrap_SeekPvPx7ESzSeek,@function
_ZL17InStreamWrap_SeekPvPx7ESzSeek:     # @_ZL17InStreamWrap_SeekPvPx7ESzSeek
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$5, %eax
	cmpl	$2, %edx
	ja	.LBB10_2
# BB#1:                                 # %switch.lookup
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	(%r14), %rsi
	movq	%rsp, %rcx
	callq	*48(%rax)
	movl	%eax, %ecx
	movl	%ecx, 24(%rbx)
	movq	(%rsp), %rax
	movq	%rax, (%r14)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	setne	%al
	shll	$3, %eax
.LBB10_2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	_ZL17InStreamWrap_SeekPvPx7ESzSeek, .Lfunc_end10-_ZL17InStreamWrap_SeekPvPx7ESzSeek
	.cfi_endproc

	.globl	_ZN14CByteInBufWrap4FreeEv
	.p2align	4, 0x90
	.type	_ZN14CByteInBufWrap4FreeEv,@function
_ZN14CByteInBufWrap4FreeEv:             # @_ZN14CByteInBufWrap4FreeEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 16
.Lcfi24:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	callq	MidFree
	movq	$0, 24(%rbx)
	popq	%rbx
	retq
.Lfunc_end11:
	.size	_ZN14CByteInBufWrap4FreeEv, .Lfunc_end11-_ZN14CByteInBufWrap4FreeEv
	.cfi_endproc

	.globl	_ZN14CByteInBufWrap5AllocEj
	.p2align	4, 0x90
	.type	_ZN14CByteInBufWrap5AllocEj,@function
_ZN14CByteInBufWrap5AllocEj:            # @_ZN14CByteInBufWrap5AllocEj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 32
.Lcfi28:
	.cfi_offset %rbx, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.LBB12_2
# BB#1:
	cmpl	%ebp, 32(%rbx)
	je	.LBB12_3
.LBB12_2:                               # %._crit_edge
	movq	%rax, %rdi
	callq	MidFree
	movq	$0, 24(%rbx)
	movl	%ebp, %edi
	callq	MidAlloc
	movq	%rax, 24(%rbx)
	movq	%rax, 8(%rbx)
	movq	%rax, 16(%rbx)
	movl	%ebp, 32(%rbx)
.LBB12_3:
	testq	%rax, %rax
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN14CByteInBufWrap5AllocEj, .Lfunc_end12-_ZN14CByteInBufWrap5AllocEj
	.cfi_endproc

	.globl	_ZN14CByteInBufWrap20ReadByteFromNewBlockEv
	.p2align	4, 0x90
	.type	_ZN14CByteInBufWrap20ReadByteFromNewBlockEv,@function
_ZN14CByteInBufWrap20ReadByteFromNewBlockEv: # @_ZN14CByteInBufWrap20ReadByteFromNewBlockEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi31:
	.cfi_def_cfa_offset 32
.Lcfi32:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpl	$0, 60(%rbx)
	je	.LBB13_1
.LBB13_3:
	movb	$1, 56(%rbx)
	xorl	%eax, %eax
	jmp	.LBB13_4
.LBB13_1:
	movq	8(%rbx), %rax
	movq	24(%rbx), %rsi
	subq	%rsi, %rax
	addq	%rax, 48(%rbx)
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	movl	32(%rbx), %edx
	leaq	12(%rsp), %rcx
	callq	*40(%rax)
	movl	%eax, 60(%rbx)
	movq	24(%rbx), %rax
	movq	%rax, 8(%rbx)
	movl	12(%rsp), %ecx
	leaq	(%rax,%rcx), %rdx
	testq	%rcx, %rcx
	movq	%rdx, 16(%rbx)
	je	.LBB13_3
# BB#2:
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rbx)
	movb	(%rax), %al
.LBB13_4:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end13:
	.size	_ZN14CByteInBufWrap20ReadByteFromNewBlockEv, .Lfunc_end13-_ZN14CByteInBufWrap20ReadByteFromNewBlockEv
	.cfi_endproc

	.globl	_ZN14CByteInBufWrapC2Ev
	.p2align	4, 0x90
	.type	_ZN14CByteInBufWrapC2Ev,@function
_ZN14CByteInBufWrapC2Ev:                # @_ZN14CByteInBufWrapC2Ev
	.cfi_startproc
# BB#0:
	movq	$0, 24(%rdi)
	movq	$_ZL13Wrap_ReadBytePv, (%rdi)
	retq
.Lfunc_end14:
	.size	_ZN14CByteInBufWrapC2Ev, .Lfunc_end14-_ZN14CByteInBufWrapC2Ev
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL13Wrap_ReadBytePv,@function
_ZL13Wrap_ReadBytePv:                   # @_ZL13Wrap_ReadBytePv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	cmpq	16(%rbx), %rax
	je	.LBB15_2
.LBB15_1:
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rbx)
	movb	(%rax), %al
	jmp	.LBB15_5
.LBB15_2:
	cmpl	$0, 60(%rbx)
	jne	.LBB15_4
# BB#3:
	movq	24(%rbx), %rsi
	subq	%rsi, %rax
	addq	%rax, 48(%rbx)
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	movl	32(%rbx), %edx
	leaq	12(%rsp), %rcx
	callq	*40(%rax)
	movl	%eax, 60(%rbx)
	movq	24(%rbx), %rax
	movq	%rax, 8(%rbx)
	movl	12(%rsp), %ecx
	leaq	(%rax,%rcx), %rdx
	testq	%rcx, %rcx
	movq	%rdx, 16(%rbx)
	jne	.LBB15_1
.LBB15_4:
	movb	$1, 56(%rbx)
	xorl	%eax, %eax
.LBB15_5:                               # %_ZN14CByteInBufWrap20ReadByteFromNewBlockEv.exit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end15:
	.size	_ZL13Wrap_ReadBytePv, .Lfunc_end15-_ZL13Wrap_ReadBytePv
	.cfi_endproc

	.globl	_ZN15CByteOutBufWrap4FreeEv
	.p2align	4, 0x90
	.type	_ZN15CByteOutBufWrap4FreeEv,@function
_ZN15CByteOutBufWrap4FreeEv:            # @_ZN15CByteOutBufWrap4FreeEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 16
.Lcfi37:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	callq	MidFree
	movq	$0, 24(%rbx)
	popq	%rbx
	retq
.Lfunc_end16:
	.size	_ZN15CByteOutBufWrap4FreeEv, .Lfunc_end16-_ZN15CByteOutBufWrap4FreeEv
	.cfi_endproc

	.globl	_ZN15CByteOutBufWrap5AllocEm
	.p2align	4, 0x90
	.type	_ZN15CByteOutBufWrap5AllocEm,@function
_ZN15CByteOutBufWrap5AllocEm:           # @_ZN15CByteOutBufWrap5AllocEm
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi40:
	.cfi_def_cfa_offset 32
.Lcfi41:
	.cfi_offset %rbx, -24
.Lcfi42:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.LBB17_2
# BB#1:
	cmpq	%r14, 32(%rbx)
	je	.LBB17_3
.LBB17_2:                               # %._crit_edge
	movq	%rax, %rdi
	callq	MidFree
	movq	$0, 24(%rbx)
	movq	%r14, %rdi
	callq	MidAlloc
	movq	%rax, 24(%rbx)
	movq	%r14, 32(%rbx)
.LBB17_3:
	testq	%rax, %rax
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end17:
	.size	_ZN15CByteOutBufWrap5AllocEm, .Lfunc_end17-_ZN15CByteOutBufWrap5AllocEm
	.cfi_endproc

	.globl	_ZN15CByteOutBufWrap5FlushEv
	.p2align	4, 0x90
	.type	_ZN15CByteOutBufWrap5FlushEv,@function
_ZN15CByteOutBufWrap5FlushEv:           # @_ZN15CByteOutBufWrap5FlushEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi45:
	.cfi_def_cfa_offset 32
.Lcfi46:
	.cfi_offset %rbx, -24
.Lcfi47:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	56(%rbx), %eax
	testl	%eax, %eax
	jne	.LBB18_4
# BB#1:
	movq	8(%rbx), %r14
	movq	24(%rbx), %rsi
	subq	%rsi, %r14
	movq	40(%rbx), %rdi
	movq	%r14, %rdx
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	movl	%eax, 56(%rbx)
	testl	%eax, %eax
	jne	.LBB18_3
# BB#2:
	addq	%r14, 48(%rbx)
.LBB18_3:
	movq	24(%rbx), %rcx
	movq	%rcx, 8(%rbx)
.LBB18_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end18:
	.size	_ZN15CByteOutBufWrap5FlushEv, .Lfunc_end18-_ZN15CByteOutBufWrap5FlushEv
	.cfi_endproc

	.globl	_ZN15CByteOutBufWrapC2Ev
	.p2align	4, 0x90
	.type	_ZN15CByteOutBufWrapC2Ev,@function
_ZN15CByteOutBufWrapC2Ev:               # @_ZN15CByteOutBufWrapC2Ev
	.cfi_startproc
# BB#0:
	movq	$0, 24(%rdi)
	movq	$_ZL14Wrap_WriteBytePvh, (%rdi)
	retq
.Lfunc_end19:
	.size	_ZN15CByteOutBufWrapC2Ev, .Lfunc_end19-_ZN15CByteOutBufWrapC2Ev
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL14Wrap_WriteBytePvh,@function
_ZL14Wrap_WriteBytePvh:                 # @_ZL14Wrap_WriteBytePvh
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 32
.Lcfi51:
	.cfi_offset %rbx, -24
.Lcfi52:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	8(%r14), %rbx
	movb	%sil, (%rbx)
	incq	%rbx
	movq	%rbx, 8(%r14)
	cmpq	16(%r14), %rbx
	jne	.LBB20_5
# BB#1:
	cmpl	$0, 56(%r14)
	jne	.LBB20_5
# BB#2:
	movq	24(%r14), %rsi
	movq	40(%r14), %rdi
	subq	%rsi, %rbx
	movq	%rbx, %rdx
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	movl	%eax, 56(%r14)
	testl	%eax, %eax
	jne	.LBB20_4
# BB#3:
	addq	%rbx, 48(%r14)
.LBB20_4:
	movq	24(%r14), %rax
	movq	%rax, 8(%r14)
.LBB20_5:                               # %_ZN15CByteOutBufWrap5FlushEv.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end20:
	.size	_ZL14Wrap_WriteBytePvh, .Lfunc_end20-_ZL14Wrap_WriteBytePvh
	.cfi_endproc

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2147942414              # 0x8007000e
	.long	2147500037              # 0x80004005
	.long	2147500037              # 0x80004005
	.long	2147942487              # 0x80070057
	.long	2147500037              # 0x80004005
	.long	2147500037              # 0x80004005
	.long	2147500037              # 0x80004005
	.long	2147500037              # 0x80004005
	.long	2147500036              # 0x80004004
	.size	.Lswitch.table, 44


	.globl	_ZN21CCompressProgressWrapC1EP21ICompressProgressInfo
	.type	_ZN21CCompressProgressWrapC1EP21ICompressProgressInfo,@function
_ZN21CCompressProgressWrapC1EP21ICompressProgressInfo = _ZN21CCompressProgressWrapC2EP21ICompressProgressInfo
	.globl	_ZN16CSeqInStreamWrapC1EP19ISequentialInStream
	.type	_ZN16CSeqInStreamWrapC1EP19ISequentialInStream,@function
_ZN16CSeqInStreamWrapC1EP19ISequentialInStream = _ZN16CSeqInStreamWrapC2EP19ISequentialInStream
	.globl	_ZN17CSeqOutStreamWrapC1EP20ISequentialOutStream
	.type	_ZN17CSeqOutStreamWrapC1EP20ISequentialOutStream,@function
_ZN17CSeqOutStreamWrapC1EP20ISequentialOutStream = _ZN17CSeqOutStreamWrapC2EP20ISequentialOutStream
	.globl	_ZN17CSeekInStreamWrapC1EP9IInStream
	.type	_ZN17CSeekInStreamWrapC1EP9IInStream,@function
_ZN17CSeekInStreamWrapC1EP9IInStream = _ZN17CSeekInStreamWrapC2EP9IInStream
	.globl	_ZN14CByteInBufWrapC1Ev
	.type	_ZN14CByteInBufWrapC1Ev,@function
_ZN14CByteInBufWrapC1Ev = _ZN14CByteInBufWrapC2Ev
	.globl	_ZN15CByteOutBufWrapC1Ev
	.type	_ZN15CByteOutBufWrapC1Ev,@function
_ZN15CByteOutBufWrapC1Ev = _ZN15CByteOutBufWrapC2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
