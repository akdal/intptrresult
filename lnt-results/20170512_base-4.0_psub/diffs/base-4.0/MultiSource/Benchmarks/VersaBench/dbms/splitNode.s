	.text
	.file	"splitNode.bc"
	.globl	splitNode
	.p2align	4, 0x90
	.type	splitNode,@function
splitNode:                              # @splitNode
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r13, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	%rcx, %r12
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	callq	createIndexEntry
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.LBB0_1
# BB#3:
	movq	(%rbx), %rdi
	callq	createIndexNode
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB0_4
# BB#5:
	movq	8(%rbx), %rax
	movq	%rax, 40(%r15)
	movq	$0, 8(%rbx)
	leaq	8(%rbx), %rdx
	movq	%r13, %rcx
	addq	$8, %rcx
	movq	$0, 8(%r13)
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	partitionEntries
	movq	8(%r13), %rdi
	movq	(%r12), %rsi
	addq	$8, %rsi
	callq	keysUnion
	movq	(%r12), %rax
	movq	%r13, (%rax)
	xorl	%eax, %eax
	jmp	.LBB0_6
.LBB0_1:
	movl	$.L.str, %edi
	jmp	.LBB0_2
.LBB0_4:
	movq	(%r12), %rdi
	movq	(%rbx), %rsi
	callq	deleteIndexEntry
	movl	$.L.str.1, %edi
.LBB0_2:
	xorl	%esi, %esi
	callq	errorMessage
	movl	$splitNode.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$1, %eax
.LBB0_6:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	splitNode, .Lfunc_end0-splitNode
	.cfi_endproc

	.type	splitNode.name,@object  # @splitNode.name
	.data
splitNode.name:
	.asciz	"splitNode"
	.size	splitNode.name, 10

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"allocation failure - split entry"
	.size	.L.str, 33

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"allocation failure - sibling node"
	.size	.L.str.1, 34


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
