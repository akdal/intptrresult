	.text
	.file	"methcall.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	$1000000000, %ebx       # imm = 0x3B9ACA00
	cmpl	$2, %edi
	jne	.LBB0_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rbx
.LBB0_2:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r14
	movq	$_ZTV6Toggle+16, (%r14)
	movb	$1, 8(%r14)
	testl	%ebx, %ebx
	jle	.LBB0_3
# BB#4:                                 # %.lr.ph36
	movq	%r14, %rdi
	callq	_ZN6Toggle8activateEv
	cmpl	$1, %ebx
	je	.LBB0_7
# BB#5:                                 # %._crit_edge41.preheader
	leal	-1(%rbx), %ebp
	.p2align	4, 0x90
.LBB0_6:                                # %._crit_edge41
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*16(%rax)
	decl	%ebp
	jne	.LBB0_6
.LBB0_7:                                # %._crit_edge37
	cmpb	$0, 8(%rax)
	movl	$.L.str, %eax
	movl	$.L.str.1, %r15d
	cmovneq	%rax, %r15
	jmp	.LBB0_8
.LBB0_3:
	movl	$.L.str, %r15d
.LBB0_8:
	movq	%r15, %rdi
	callq	strlen
	movl	$_ZSt4cout, %edi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %r15
	testq	%r15, %r15
	je	.LBB0_23
# BB#9:                                 # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit
	cmpb	$0, 56(%r15)
	je	.LBB0_11
# BB#10:
	movb	67(%r15), %al
	jmp	.LBB0_12
.LBB0_11:
	movq	%r15, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%r15), %rax
	movl	$10, %esi
	movq	%r15, %rdi
	callq	*48(%rax)
.LBB0_12:                               # %_ZNKSt5ctypeIcE5widenEc.exit
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r14
	movb	$1, 8(%r14)
	movq	$_ZTV9NthToggle+16, (%r14)
	movq	$3, 12(%r14)
	testl	%ebx, %ebx
	jle	.LBB0_13
# BB#14:                                # %.lr.ph
	movq	%r14, %rdi
	callq	_ZN9NthToggle8activateEv
	cmpl	$1, %ebx
	je	.LBB0_17
# BB#15:                                # %._crit_edge42.preheader
	decl	%ebx
	.p2align	4, 0x90
.LBB0_16:                               # %._crit_edge42
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*16(%rax)
	decl	%ebx
	jne	.LBB0_16
.LBB0_17:                               # %._crit_edge
	cmpb	$0, 8(%rax)
	movl	$.L.str, %eax
	movl	$.L.str.1, %ebx
	cmovneq	%rax, %rbx
	jmp	.LBB0_18
.LBB0_13:
	movl	$.L.str, %ebx
.LBB0_18:
	movq	%rbx, %rdi
	callq	strlen
	movl	$_ZSt4cout, %edi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_23
# BB#19:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit31
	cmpb	$0, 56(%rbx)
	je	.LBB0_21
# BB#20:
	movb	67(%rbx), %al
	jmp	.LBB0_22
.LBB0_21:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB0_22:                               # %_ZNKSt5ctypeIcE5widenEc.exit30
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_23:
	callq	_ZSt16__throw_bad_castv
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.section	.text._ZN6ToggleD2Ev,"axG",@progbits,_ZN6ToggleD2Ev,comdat
	.weak	_ZN6ToggleD2Ev
	.p2align	4, 0x90
	.type	_ZN6ToggleD2Ev,@function
_ZN6ToggleD2Ev:                         # @_ZN6ToggleD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	_ZN6ToggleD2Ev, .Lfunc_end1-_ZN6ToggleD2Ev
	.cfi_endproc

	.section	.text._ZN6ToggleD0Ev,"axG",@progbits,_ZN6ToggleD0Ev,comdat
	.weak	_ZN6ToggleD0Ev
	.p2align	4, 0x90
	.type	_ZN6ToggleD0Ev,@function
_ZN6ToggleD0Ev:                         # @_ZN6ToggleD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end2:
	.size	_ZN6ToggleD0Ev, .Lfunc_end2-_ZN6ToggleD0Ev
	.cfi_endproc

	.section	.text._ZN6Toggle8activateEv,"axG",@progbits,_ZN6Toggle8activateEv,comdat
	.weak	_ZN6Toggle8activateEv
	.p2align	4, 0x90
	.type	_ZN6Toggle8activateEv,@function
_ZN6Toggle8activateEv:                  # @_ZN6Toggle8activateEv
	.cfi_startproc
# BB#0:
	xorb	$1, 8(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end3:
	.size	_ZN6Toggle8activateEv, .Lfunc_end3-_ZN6Toggle8activateEv
	.cfi_endproc

	.section	.text._ZN9NthToggleD0Ev,"axG",@progbits,_ZN9NthToggleD0Ev,comdat
	.weak	_ZN9NthToggleD0Ev
	.p2align	4, 0x90
	.type	_ZN9NthToggleD0Ev,@function
_ZN9NthToggleD0Ev:                      # @_ZN9NthToggleD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end4:
	.size	_ZN9NthToggleD0Ev, .Lfunc_end4-_ZN9NthToggleD0Ev
	.cfi_endproc

	.section	.text._ZN9NthToggle8activateEv,"axG",@progbits,_ZN9NthToggle8activateEv,comdat
	.weak	_ZN9NthToggle8activateEv
	.p2align	4, 0x90
	.type	_ZN9NthToggle8activateEv,@function
_ZN9NthToggle8activateEv:               # @_ZN9NthToggle8activateEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	cmpl	12(%rdi), %eax
	jl	.LBB5_2
# BB#1:
	xorb	$1, 8(%rdi)
	movl	$0, 16(%rdi)
.LBB5_2:
	movq	%rdi, %rax
	retq
.Lfunc_end5:
	.size	_ZN9NthToggle8activateEv, .Lfunc_end5-_ZN9NthToggle8activateEv
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_methcall.ii,@function
_GLOBAL__sub_I_methcall.ii:             # @_GLOBAL__sub_I_methcall.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end6:
	.size	_GLOBAL__sub_I_methcall.ii, .Lfunc_end6-_GLOBAL__sub_I_methcall.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"true"
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"false"
	.size	.L.str.1, 6

	.type	_ZTV6Toggle,@object     # @_ZTV6Toggle
	.section	.rodata._ZTV6Toggle,"aG",@progbits,_ZTV6Toggle,comdat
	.weak	_ZTV6Toggle
	.p2align	3
_ZTV6Toggle:
	.quad	0
	.quad	_ZTI6Toggle
	.quad	_ZN6ToggleD2Ev
	.quad	_ZN6ToggleD0Ev
	.quad	_ZN6Toggle8activateEv
	.size	_ZTV6Toggle, 40

	.type	_ZTS6Toggle,@object     # @_ZTS6Toggle
	.section	.rodata._ZTS6Toggle,"aG",@progbits,_ZTS6Toggle,comdat
	.weak	_ZTS6Toggle
_ZTS6Toggle:
	.asciz	"6Toggle"
	.size	_ZTS6Toggle, 8

	.type	_ZTI6Toggle,@object     # @_ZTI6Toggle
	.section	.rodata._ZTI6Toggle,"aG",@progbits,_ZTI6Toggle,comdat
	.weak	_ZTI6Toggle
	.p2align	3
_ZTI6Toggle:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS6Toggle
	.size	_ZTI6Toggle, 16

	.type	_ZTV9NthToggle,@object  # @_ZTV9NthToggle
	.section	.rodata._ZTV9NthToggle,"aG",@progbits,_ZTV9NthToggle,comdat
	.weak	_ZTV9NthToggle
	.p2align	3
_ZTV9NthToggle:
	.quad	0
	.quad	_ZTI9NthToggle
	.quad	_ZN6ToggleD2Ev
	.quad	_ZN9NthToggleD0Ev
	.quad	_ZN9NthToggle8activateEv
	.size	_ZTV9NthToggle, 40

	.type	_ZTS9NthToggle,@object  # @_ZTS9NthToggle
	.section	.rodata._ZTS9NthToggle,"aG",@progbits,_ZTS9NthToggle,comdat
	.weak	_ZTS9NthToggle
_ZTS9NthToggle:
	.asciz	"9NthToggle"
	.size	_ZTS9NthToggle, 11

	.type	_ZTI9NthToggle,@object  # @_ZTI9NthToggle
	.section	.rodata._ZTI9NthToggle,"aG",@progbits,_ZTI9NthToggle,comdat
	.weak	_ZTI9NthToggle
	.p2align	4
_ZTI9NthToggle:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS9NthToggle
	.quad	_ZTI6Toggle
	.size	_ZTI9NthToggle, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_methcall.ii

	.ident	"clang version 5.0.0 (https://github.com/aqjune/clang-intptr.git 2b963f0b79b3094bfc850c67ca5db086f5651807) (https://github.com/aqjune/llvm-intptr.git 90a84f9e4908c61106c413cb18b15a9bb8978e0d)"
	.section	".note.GNU-stack","",@progbits
