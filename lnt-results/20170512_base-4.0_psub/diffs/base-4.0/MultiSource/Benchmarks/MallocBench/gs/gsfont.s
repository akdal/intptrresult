	.text
	.file	"gsfont.bc"
	.globl	gs_font_dir_alloc
	.p2align	4, 0x90
	.type	gs_font_dir_alloc,@function
gs_font_dir_alloc:                      # @gs_font_dir_alloc
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$100, (%rsp)
	movl	$10, %edx
	movl	$20000, %ecx            # imm = 0x4E20
	movl	$20, %r8d
	movl	$500, %r9d              # imm = 0x1F4
	callq	gs_font_dir_alloc_limits
	popq	%rcx
	retq
.Lfunc_end0:
	.size	gs_font_dir_alloc, .Lfunc_end0-gs_font_dir_alloc
	.cfi_endproc

	.globl	gs_font_dir_alloc_limits
	.p2align	4, 0x90
	.type	gs_font_dir_alloc_limits,@function
gs_font_dir_alloc_limits:               # @gs_font_dir_alloc_limits
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 96
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movl	%r9d, %r12d
	movl	%r8d, %r14d
	movl	%ecx, %ebx
	movl	%edx, 28(%rsp)          # 4-byte Spill
	movq	%rsi, %rbp
	movq	%rdi, %r15
	movl	$1, %edi
	movl	$1144, %esi             # imm = 0x478
	movl	$.L.str, %edx
	callq	*%r15
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB1_13
# BB#1:
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movl	$1, %esi
	movl	$.L.str.1, %edx
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	movl	%ebx, %edi
	callq	*%r15
	movq	%rax, %rbx
	movl	cached_fm_pair_sizeof(%rip), %esi
	movl	$.L.str.2, %edx
	movl	%r14d, 8(%rsp)          # 4-byte Spill
	movl	%r14d, %edi
	callq	*%r15
	movq	%rax, %r14
	movl	cached_char_sizeof(%rip), %esi
	movl	$.L.str.3, %edx
	movl	%r12d, %ebp
	movl	%r12d, %edi
	callq	*%r15
	movq	%rax, %r12
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	testq	%rbx, %rbx
	je	.LBB1_6
# BB#2:
	testq	%r14, %r14
	je	.LBB1_6
# BB#3:
	testq	%r12, %r12
	je	.LBB1_6
# BB#4:
	movl	96(%rsp), %ebx
	movq	%r13, %rdi
	addq	$16, %rdi
	xorl	%esi, %esi
	movl	$1128, %edx             # imm = 0x468
	callq	memset
	movq	%r15, (%r13)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%r13)
	movl	28(%rsp), %eax          # 4-byte Reload
	movl	%eax, 36(%r13)
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, 44(%r13)
	movl	8(%rsp), %eax           # 4-byte Reload
	movl	%eax, 52(%r13)
	movl	%ebp, 60(%r13)
	movl	%ebx, %eax
	movl	$3435973837, %ecx       # imm = 0xCCCCCCCD
	imulq	%rax, %rcx
	shrq	$35, %rcx
	movl	%ecx, 64(%r13)
	movl	%ebx, 68(%r13)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, 1096(%r13)
	movq	%r14, 1112(%r13)
	movq	%r12, 1128(%r13)
	jmp	.LBB1_14
.LBB1_6:
	testq	%r12, %r12
	je	.LBB1_8
# BB#7:
	movl	cached_char_sizeof(%rip), %edx
	movl	$.L.str.3, %ecx
	movq	%r12, %rdi
	movl	%ebp, %esi
	callq	*16(%rsp)               # 8-byte Folded Reload
.LBB1_8:
	testq	%r14, %r14
	movq	16(%rsp), %rbp          # 8-byte Reload
	je	.LBB1_10
# BB#9:
	movl	cached_fm_pair_sizeof(%rip), %edx
	movl	$.L.str.2, %ecx
	movq	%r14, %rdi
	movl	8(%rsp), %esi           # 4-byte Reload
	callq	*%rbp
.LBB1_10:
	movq	32(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB1_12
# BB#11:
	movl	$1, %edx
	movl	$.L.str.1, %ecx
	movl	12(%rsp), %esi          # 4-byte Reload
	callq	*%rbp
.LBB1_12:
	movl	$1, %esi
	movl	$1144, %edx             # imm = 0x478
	movl	$.L.str, %ecx
	movq	%r13, %rdi
	callq	*%rbp
.LBB1_13:
	xorl	%r13d, %r13d
.LBB1_14:
	movq	%r13, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	gs_font_dir_alloc_limits, .Lfunc_end1-gs_font_dir_alloc_limits
	.cfi_endproc

	.globl	gs_scalefont
	.p2align	4, 0x90
	.type	gs_scalefont,@function
gs_scalefont:                           # @gs_scalefont
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 48
	subq	$96, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 144
.Lcfi20:
	.cfi_offset %rbx, -48
.Lcfi21:
	.cfi_offset %r12, -40
.Lcfi22:
	.cfi_offset %r13, -32
.Lcfi23:
	.cfi_offset %r14, -24
.Lcfi24:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	%rsp, %r13
	movaps	%xmm0, %xmm1
	movq	%r13, %rdi
	callq	gs_make_scaling
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	callq	gs_makefont
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	gs_scalefont, .Lfunc_end2-gs_scalefont
	.cfi_endproc

	.globl	gs_makefont
	.p2align	4, 0x90
	.type	gs_makefont,@function
gs_makefont:                            # @gs_makefont
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi31:
	.cfi_def_cfa_offset 160
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%rdx, %rbx
	movq	%rsi, %r13
	movq	%rdi, %r15
	movq	24(%r15), %rbp
	movq	$0, (%r12)
	leaq	8(%rsp), %r14
	movq	%r14, %rdi
	callq	gs_make_identity
	leaq	40(%r13), %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	gs_matrix_multiply
	testl	%eax, %eax
	js	.LBB3_23
# BB#1:
	xorl	%r14d, %r14d
	testq	%rbp, %rbp
	je	.LBB3_12
# BB#2:
	movq	160(%r13), %rax
	cmpq	$-1, %rax
	je	.LBB3_12
# BB#3:                                 # %.preheader
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	40(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	56(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB3_4:                                # =>This Inner Loop Header: Depth=1
	movq	%rbp, %r14
	cmpq	%rax, 160(%r14)
	jne	.LBB3_11
# BB#5:                                 #   in Loop: Header=BB3_4 Depth=1
	movl	136(%r14), %ecx
	cmpl	136(%r13), %ecx
	jne	.LBB3_11
# BB#6:                                 #   in Loop: Header=BB3_4 Depth=1
	movss	40(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm4
	jne	.LBB3_11
	jp	.LBB3_11
# BB#7:                                 #   in Loop: Header=BB3_4 Depth=1
	movss	56(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm4
	jne	.LBB3_11
	jp	.LBB3_11
# BB#8:                                 #   in Loop: Header=BB3_4 Depth=1
	movss	72(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm4
	jne	.LBB3_11
	jp	.LBB3_11
# BB#9:                                 #   in Loop: Header=BB3_4 Depth=1
	movss	88(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm4
	jne	.LBB3_11
	jnp	.LBB3_10
	.p2align	4, 0x90
.LBB3_11:                               #   in Loop: Header=BB3_4 Depth=1
	movq	(%r14), %rbp
	testq	%rbp, %rbp
	jne	.LBB3_4
.LBB3_12:                               # %.loopexit55
	movl	$1, %edi
	movl	$184, %esi
	movl	$.L.str.4, %edx
	callq	*(%r15)
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_13
# BB#14:
	movl	$184, %edx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	memcpy
	movups	88(%rsp), %xmm0
	movups	%xmm0, 120(%rbx)
	movups	72(%rsp), %xmm0
	movups	%xmm0, 104(%rbx)
	movups	8(%rsp), %xmm0
	movups	24(%rsp), %xmm1
	movups	40(%rsp), %xmm2
	movups	56(%rsp), %xmm3
	movups	%xmm3, 88(%rbx)
	movups	%xmm2, 72(%rbx)
	movups	%xmm1, 56(%rbx)
	movups	%xmm0, 40(%rbx)
	movl	32(%r15), %eax
	cmpl	36(%r15), %eax
	jne	.LBB3_19
# BB#15:
	testq	%r14, %r14
	jne	.LBB3_18
# BB#16:
	movq	24(%r15), %rax
	.p2align	4, 0x90
.LBB3_17:                               # =>This Inner Loop Header: Depth=1
	movq	%rax, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	jne	.LBB3_17
.LBB3_18:                               # %.loopexit
	movq	%r14, (%r12)
	movq	8(%r14), %rax
	movq	$0, (%rax)
	jmp	.LBB3_20
.LBB3_13:
	movl	$-25, %eax
	jmp	.LBB3_23
.LBB3_19:
	incl	%eax
	movl	%eax, 32(%r15)
.LBB3_20:
	movq	24(%r15), %rax
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.LBB3_22
# BB#21:
	movq	%rbx, 8(%rax)
.LBB3_22:
	movq	$0, 8(%rbx)
	movq	%rbx, 24(%r15)
	movq	16(%r13), %rax
	movq	%rax, 16(%rbx)
	movq	%r15, 24(%rbx)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rbx, (%rax)
	movl	$1, %eax
.LBB3_23:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_10:
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%r14, (%rax)
	xorl	%eax, %eax
	jmp	.LBB3_23
.Lfunc_end3:
	.size	gs_makefont, .Lfunc_end3-gs_makefont
	.cfi_endproc

	.globl	gs_setfont
	.p2align	4, 0x90
	.type	gs_setfont,@function
gs_setfont:                             # @gs_setfont
	.cfi_startproc
# BB#0:
	movq	%rsi, 328(%rdi)
	movl	$0, 432(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end4:
	.size	gs_setfont, .Lfunc_end4-gs_setfont
	.cfi_endproc

	.globl	gs_currentfont
	.p2align	4, 0x90
	.type	gs_currentfont,@function
gs_currentfont:                         # @gs_currentfont
	.cfi_startproc
# BB#0:
	movq	328(%rdi), %rax
	retq
.Lfunc_end5:
	.size	gs_currentfont, .Lfunc_end5-gs_currentfont
	.cfi_endproc

	.globl	gs_cachestatus
	.p2align	4, 0x90
	.type	gs_cachestatus,@function
gs_cachestatus:                         # @gs_cachestatus
	.cfi_startproc
# BB#0:
	movl	40(%rdi), %eax
	movl	%eax, (%rsi)
	movl	44(%rdi), %eax
	movl	%eax, 4(%rsi)
	movl	48(%rdi), %eax
	movl	%eax, 8(%rsi)
	movl	52(%rdi), %eax
	movl	%eax, 12(%rsi)
	movl	56(%rdi), %eax
	movl	%eax, 16(%rsi)
	movl	60(%rdi), %eax
	movl	%eax, 20(%rsi)
	movl	68(%rdi), %eax
	movl	%eax, 24(%rsi)
	retq
.Lfunc_end6:
	.size	gs_cachestatus, .Lfunc_end6-gs_cachestatus
	.cfi_endproc

	.globl	gs_setcachelimit
	.p2align	4, 0x90
	.type	gs_setcachelimit,@function
gs_setcachelimit:                       # @gs_setcachelimit
	.cfi_startproc
# BB#0:
	movl	%esi, 68(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end7:
	.size	gs_setcachelimit, .Lfunc_end7-gs_setcachelimit
	.cfi_endproc

	.globl	gs_setcachelower
	.p2align	4, 0x90
	.type	gs_setcachelower,@function
gs_setcachelower:                       # @gs_setcachelower
	.cfi_startproc
# BB#0:
	movl	%esi, 64(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end8:
	.size	gs_setcachelower, .Lfunc_end8-gs_setcachelower
	.cfi_endproc

	.globl	gs_setcacheupper
	.p2align	4, 0x90
	.type	gs_setcacheupper,@function
gs_setcacheupper:                       # @gs_setcacheupper
	.cfi_startproc
# BB#0:
	movl	%esi, 68(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end9:
	.size	gs_setcacheupper, .Lfunc_end9-gs_setcacheupper
	.cfi_endproc

	.globl	gs_currentcachelower
	.p2align	4, 0x90
	.type	gs_currentcachelower,@function
gs_currentcachelower:                   # @gs_currentcachelower
	.cfi_startproc
# BB#0:
	movl	64(%rdi), %eax
	retq
.Lfunc_end10:
	.size	gs_currentcachelower, .Lfunc_end10-gs_currentcachelower
	.cfi_endproc

	.globl	gs_currentcacheupper
	.p2align	4, 0x90
	.type	gs_currentcacheupper,@function
gs_currentcacheupper:                   # @gs_currentcacheupper
	.cfi_startproc
# BB#0:
	movl	68(%rdi), %eax
	retq
.Lfunc_end11:
	.size	gs_currentcacheupper, .Lfunc_end11-gs_currentcacheupper
	.cfi_endproc

	.globl	gs_no_build_char_proc
	.p2align	4, 0x90
	.type	gs_no_build_char_proc,@function
gs_no_build_char_proc:                  # @gs_no_build_char_proc
	.cfi_startproc
# BB#0:
	movl	$1, %eax
	retq
.Lfunc_end12:
	.size	gs_no_build_char_proc, .Lfunc_end12-gs_no_build_char_proc
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"font_dir_alloc(dir)"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"font_dir_alloc(bdata)"
	.size	.L.str.1, 22

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"font_dir_alloc(mdata)"
	.size	.L.str.2, 22

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"font_dir_alloc(cdata)"
	.size	.L.str.3, 22

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"gs_makefont"
	.size	.L.str.4, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
