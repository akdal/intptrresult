	.text
	.file	"z48.bc"
	.globl	PDFHasValidTextMatrix
	.p2align	4, 0x90
	.type	PDFHasValidTextMatrix,@function
PDFHasValidTextMatrix:                  # @PDFHasValidTextMatrix
	.cfi_startproc
# BB#0:
	movzbl	g_valid_text_matrix(%rip), %eax
	retq
.Lfunc_end0:
	.size	PDFHasValidTextMatrix, .Lfunc_end0-PDFHasValidTextMatrix
	.cfi_endproc

	.globl	PDFFile_BeginFontEncoding
	.p2align	4, 0x90
	.type	PDFFile_BeginFontEncoding,@function
PDFFile_BeginFontEncoding:              # @PDFFile_BeginFontEncoding
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	PDFObject_New
	movl	%eax, %ebp
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	PDFObject_WriteObj
	movl	$.L.str.1, %edi
	movl	$36, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB1_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$2, %esi
	movl	$.L.str.2, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB1_2:
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	malloc
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	jne	.LBB1_4
# BB#3:
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$3, %esi
	movl	$.L.str.3, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	16(%rbx), %rax
.LBB1_4:
	movq	g_font_encoding_list(%rip), %rcx
	movq	%rcx, (%rbx)
	movl	%ebp, 8(%rbx)
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	strcpy
	movq	%rbx, g_font_encoding_list(%rip)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	PDFFile_BeginFontEncoding, .Lfunc_end1-PDFFile_BeginFontEncoding
	.cfi_endproc

	.globl	PDFFile_EndFontEncoding
	.p2align	4, 0x90
	.type	PDFFile_EndFontEncoding,@function
PDFFile_EndFontEncoding:                # @PDFFile_EndFontEncoding
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movl	$.L.str.4, %edi
	movl	$12, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	jmp	fwrite                  # TAILCALL
.Lfunc_end2:
	.size	PDFFile_EndFontEncoding, .Lfunc_end2-PDFFile_EndFontEncoding
	.cfi_endproc

	.globl	PDFFont_AddFont
	.p2align	4, 0x90
	.type	PDFFont_AddFont,@function
PDFFont_AddFont:                        # @PDFFont_AddFont
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 160
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, %r14
	movq	%rsi, %r15
	xorl	%ebp, %ebp
	testq	%r12, %r12
	movl	$0, %r13d
	je	.LBB3_6
# BB#1:                                 # %.preheader.i
	movq	g_font_encoding_list(%rip), %rbx
	testq	%rbx, %rbx
	movl	$0, %r13d
	je	.LBB3_6
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph.i5
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rsi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_5
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_2
# BB#4:
	xorl	%r13d, %r13d
	jmp	.LBB3_6
.LBB3_5:
	movl	8(%rbx), %r13d
.LBB3_6:                                # %PDFFont_FindFontEncoding.exit
	movq	g_font_list(%rip), %rax
	testq	%rax, %rax
	je	.LBB3_9
# BB#7:                                 # %.lr.ph.i.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_8:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB3_8
.LBB3_9:                                # %._crit_edge.i
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movw	$17967, (%rsp)          # imm = 0x462F
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB3_11
# BB#10:
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$4, %esi
	movl	$.L.str.69, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB3_11:
	leaq	64(%rsp), %r12
	movl	$.L.str.70, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movl	%ebp, %edx
	callq	sprintf
	movq	%rsp, %rbp
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	strcat
	movq	%rbp, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	malloc
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	jne	.LBB3_13
# BB#12:
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$5, %esi
	movl	$.L.str.69, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	8(%rbx), %rax
.LBB3_13:
	movq	%rsp, %rsi
	movq	%rax, %rdi
	callq	strcpy
	movq	%r15, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	malloc
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	jne	.LBB3_15
# BB#14:
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$6, %esi
	movl	$.L.str.69, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	16(%rbx), %rax
.LBB3_15:
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	strcpy
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	malloc
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	jne	.LBB3_17
# BB#16:
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$7, %esi
	movl	$.L.str.69, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	24(%rbx), %rax
.LBB3_17:                               # %PDFFont_NewListEntry.exit
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	strcpy
	movl	%r13d, 32(%rbx)
	movl	$0, 36(%rbx)
	movl	$0, 40(%rbx)
	movq	g_font_list(%rip), %rax
	movq	%rax, (%rbx)
	movq	%rbx, g_font_list(%rip)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	PDFFont_AddFont, .Lfunc_end3-PDFFont_AddFont
	.cfi_endproc

	.globl	PDFPage_SetVars
	.p2align	4, 0x90
	.type	PDFPage_SetVars,@function
PDFPage_SetVars:                        # @PDFPage_SetVars
	.cfi_startproc
# BB#0:
	movl	8(%rsp), %eax
	movl	%edi, g_graphics_vars(%rip)
	movl	%esi, g_graphics_vars+4(%rip)
	movl	%edx, g_graphics_vars+8(%rip)
	movl	%ecx, g_graphics_vars+12(%rip)
	movl	%r8d, g_units+16(%rip)
	movl	%r9d, g_units+20(%rip)
	movl	%eax, g_units+24(%rip)
	retq
.Lfunc_end4:
	.size	PDFPage_SetVars, .Lfunc_end4-PDFPage_SetVars
	.cfi_endproc

	.globl	PDFPage_Write
	.p2align	4, 0x90
	.type	PDFPage_Write,@function
PDFPage_Write:                          # @PDFPage_Write
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 32
	subq	$512, %rsp              # imm = 0x200
.Lcfi22:
	.cfi_def_cfa_offset 544
.Lcfi23:
	.cfi_offset %rbx, -32
.Lcfi24:
	.cfi_offset %r14, -24
.Lcfi25:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	cmpb	$0, (%r15)
	je	.LBB5_20
# BB#1:
	cmpl	$0, g_page_contents_obj_num(%rip)
	jne	.LBB5_6
# BB#2:
	callq	PDFObject_New
	movl	%eax, %ebx
	movq	%r14, %rdi
	movl	%ebx, %esi
	callq	PDFObject_WriteObj
	movl	%ebx, g_page_contents_obj_num(%rip)
	callq	PDFObject_New
	movl	%eax, g_page_length_obj_num(%rip)
	movl	$.L.str.72, %edi
	movl	$11, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	g_page_length_obj_num(%rip), %edx
	movl	$.L.str.169, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	$.L.str.73, %edi
	movl	$11, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movq	%r14, %rdi
	callq	ftell
	movl	%eax, g_page_start_offset(%rip)
	movss	g_page_h_scale_factor(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movss	g_page_v_scale_factor(%rip), %xmm1 # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	movq	%rsp, %rdi
	movl	$.L.str.13, %esi
	movb	$2, %al
	callq	sprintf
	cmpb	$0, (%rsp)
	je	.LBB5_4
# BB#3:
	movq	%rsp, %rdi
	movq	%r14, %rsi
	callq	fputs
.LBB5_4:                                # %PDFPage_WriteStream.exit.i
	movl	g_page_line_width(%rip), %edx
	movq	%rsp, %rdi
	movl	$.L.str.74, %esi
	xorl	%eax, %eax
	callq	sprintf
	cmpb	$0, (%rsp)
	je	.LBB5_6
# BB#5:
	movq	%rsp, %rdi
	movq	%r14, %rsi
	callq	fputs
.LBB5_6:                                # %PDFPage_Begin.exit
	cmpb	$1, g_in_buffering_mode(%rip)
	jne	.LBB5_15
# BB#7:
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbx
	movl	g_buffer_pos(%rip), %eax
	leal	(%rax,%rbx), %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jb	.LBB5_19
# BB#8:
	movb	$0, g_in_buffering_mode(%rip)
	jmp	.LBB5_10
	.p2align	4, 0x90
.LBB5_9:                                # %.lr.ph.i
                                        #   in Loop: Header=BB5_10 Depth=1
	movq	(%rdi), %rax
	movq	%rax, g_qsave_marking_stack(%rip)
	callq	free
.LBB5_10:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	g_qsave_marking_stack(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB5_9
# BB#11:                                # %._crit_edge.i
	cmpb	$0, g_buffer(%rip)
	je	.LBB5_13
# BB#12:
	movl	$g_buffer, %edi
	movq	%r14, %rsi
	callq	fputs
	cmpb	$0, (%r15)
	jne	.LBB5_14
	jmp	.LBB5_20
.LBB5_15:
	cmpb	$1, g_TJ_pending(%rip)
	jne	.LBB5_17
# BB#16:
	movb	$0, g_TJ_pending(%rip)
	movl	$.L.str.5, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
.LBB5_17:
	cmpb	$1, g_ET_pending(%rip)
	jne	.LBB5_13
# BB#18:
	movb	$0, g_ET_pending(%rip)
	movl	$.L.str.6, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movb	$0, g_valid_text_matrix(%rip)
.LBB5_13:                               # %PDFPage_FlushBuffer.exit
	cmpb	$0, (%r15)
	je	.LBB5_20
.LBB5_14:
	movq	%r15, %rdi
	movq	%r14, %rsi
	addq	$512, %rsp              # imm = 0x200
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	fputs                   # TAILCALL
.LBB5_19:
	leaq	g_buffer(%rax), %rdi
	movq	%r15, %rsi
	callq	strcpy
	addl	%ebx, g_buffer_pos(%rip)
.LBB5_20:                               # %PDFPage_WriteStream.exit
	addq	$512, %rsp              # imm = 0x200
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	PDFPage_Write, .Lfunc_end5-PDFPage_Write
	.cfi_endproc

	.globl	PDFPage_Push
	.p2align	4, 0x90
	.type	PDFPage_Push,@function
PDFPage_Push:                           # @PDFPage_Push
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB6_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$14, %esi
	movl	$.L.str.7, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB6_2:
	movl	g_page_h_origin(%rip), %eax
	movl	%eax, 8(%rbx)
	movl	g_page_v_origin(%rip), %eax
	movl	%eax, 12(%rbx)
	movq	g_qsave_stack(%rip), %rax
	movq	%rax, (%rbx)
	movq	%rbx, g_qsave_stack(%rip)
	cmpb	$1, g_in_buffering_mode(%rip)
	jne	.LBB6_6
# BB#3:
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB6_5
# BB#4:
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$15, %esi
	movl	$.L.str.7, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB6_5:
	movq	g_qsave_marking_stack(%rip), %rax
	movq	%rax, (%rbx)
	movl	g_buffer_pos(%rip), %eax
	movl	%eax, 8(%rbx)
	movq	%rbx, g_qsave_marking_stack(%rip)
.LBB6_6:
	movl	$.L.str.8, %esi
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	PDFPage_Write           # TAILCALL
.Lfunc_end6:
	.size	PDFPage_Push, .Lfunc_end6-PDFPage_Push
	.cfi_endproc

	.globl	PDFPage_Pop
	.p2align	4, 0x90
	.type	PDFPage_Pop,@function
PDFPage_Pop:                            # @PDFPage_Pop
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi33:
	.cfi_def_cfa_offset 32
.Lcfi34:
	.cfi_offset %rbx, -24
.Lcfi35:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	g_qsave_stack(%rip), %rdi
	movl	8(%rdi), %eax
	movl	%eax, g_page_h_origin(%rip)
	movl	12(%rdi), %eax
	movl	%eax, g_page_v_origin(%rip)
	movq	(%rdi), %rax
	movq	%rax, g_qsave_stack(%rip)
	callq	free
	movq	g_qsave_marking_stack(%rip), %rbx
	movq	no_fpos(%rip), %r8
	cmpb	$1, g_in_buffering_mode(%rip)
	jne	.LBB7_4
# BB#1:
	testq	%rbx, %rbx
	jne	.LBB7_3
# BB#2:
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
.LBB7_3:                                # %Assert.exit
	movq	(%rbx), %rax
	movq	%rax, g_qsave_marking_stack(%rip)
	movl	8(%rbx), %eax
	movl	%eax, g_buffer_pos(%rip)
	movb	$0, g_buffer(%rax)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.LBB7_4:
	testq	%rbx, %rbx
	je	.LBB7_6
# BB#5:
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
.LBB7_6:                                # %Assert.exit12
	movl	$.L.str.9, %esi
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	PDFPage_Write           # TAILCALL
.Lfunc_end7:
	.size	PDFPage_Pop, .Lfunc_end7-PDFPage_Pop
	.cfi_endproc

	.globl	PDFFont_Set
	.p2align	4, 0x90
	.type	PDFFont_Set,@function
PDFFont_Set:                            # @PDFFont_Set
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 40
	subq	$520, %rsp              # imm = 0x208
.Lcfi40:
	.cfi_def_cfa_offset 560
.Lcfi41:
	.cfi_offset %rbx, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%esi, %r15d
	movq	%rdi, %r14
	movq	g_font_list(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB8_2
	jmp	.LBB8_4
	.p2align	4, 0x90
.LBB8_3:                                #   in Loop: Header=BB8_2 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB8_4
.LBB8_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rsi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB8_3
	jmp	.LBB8_5
.LBB8_4:                                # %.loopexit
	movq	no_fpos(%rip), %r8
	xorl	%ebp, %ebp
	movl	$48, %edi
	movl	$42, %esi
	movl	$.L.str.10, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r9
	callq	Error
.LBB8_5:                                # %PDFFont_FindListEntry_Short.exit
	movq	8(%rbp), %rdx
	movq	%rsp, %rdi
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movl	%r15d, %ecx
	callq	sprintf
	cmpb	$1, g_TJ_pending(%rip)
	jne	.LBB8_7
# BB#6:
	movb	$0, g_TJ_pending(%rip)
	movl	$.L.str.5, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
.LBB8_7:
	movb	g_ET_pending(%rip), %bl
	movb	$0, g_ET_pending(%rip)
	movq	%rsp, %rsi
	movq	%r14, %rdi
	callq	PDFPage_Write
	movb	%bl, g_ET_pending(%rip)
	movl	$1, 44(%rbp)
	movb	$1, g_page_uses_fonts(%rip)
	addq	$520, %rsp              # imm = 0x208
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	PDFFont_Set, .Lfunc_end8-PDFFont_Set
	.cfi_endproc

	.globl	PDFText_OpenXY
	.p2align	4, 0x90
	.type	PDFText_OpenXY,@function
PDFText_OpenXY:                         # @PDFText_OpenXY
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 40
	subq	$520, %rsp              # imm = 0x208
.Lcfi49:
	.cfi_def_cfa_offset 560
.Lcfi50:
	.cfi_offset %rbx, -40
.Lcfi51:
	.cfi_offset %r14, -32
.Lcfi52:
	.cfi_offset %r15, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %r15d
	movq	%rdi, %rbx
	cmpb	$1, g_in_buffering_mode(%rip)
	jne	.LBB9_6
# BB#1:
	movb	$0, g_in_buffering_mode(%rip)
	jmp	.LBB9_3
	.p2align	4, 0x90
.LBB9_2:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB9_3 Depth=1
	movq	(%rdi), %rax
	movq	%rax, g_qsave_marking_stack(%rip)
	callq	free
.LBB9_3:                                # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	g_qsave_marking_stack(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB9_2
# BB#4:                                 # %._crit_edge.i.i
	cmpb	$0, g_buffer(%rip)
	je	.LBB9_6
# BB#5:
	movl	$g_buffer, %edi
	movq	%rbx, %rsi
	callq	fputs
.LBB9_6:                                # %PDFPage_FlushBuffer.exit.i
	movb	$1, g_page_has_text(%rip)
	cmpb	$1, g_TJ_pending(%rip)
	jne	.LBB9_8
# BB#7:
	movb	$0, g_TJ_pending(%rip)
	movl	$.L.str.5, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB9_8:
	cmpb	$1, g_ET_pending(%rip)
	jne	.LBB9_10
# BB#9:
	movb	$0, g_ET_pending(%rip)
	jmp	.LBB9_11
.LBB9_10:
	movl	$.L.str.76, %esi
	movq	%rbx, %rdi
	callq	PDFPage_Write
	movb	$1, g_valid_text_matrix(%rip)
.LBB9_11:                               # %PDFText_OpenBT.exit
	movq	%rsp, %rbp
	movl	$.L.str.77, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movl	%r15d, %edx
	movl	%r14d, %ecx
	callq	sprintf
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	PDFPage_Write
	cmpb	$1, g_TJ_pending(%rip)
	jne	.LBB9_13
# BB#12:
	movb	$0, g_TJ_pending(%rip)
	jmp	.LBB9_14
.LBB9_13:
	movl	$.L.str.79, %esi
	movq	%rbx, %rdi
	callq	PDFPage_Write
.LBB9_14:                               # %PDFText_MoveToXYAndOpen.exit
	addq	$520, %rsp              # imm = 0x208
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	PDFText_OpenXY, .Lfunc_end9-PDFText_OpenXY
	.cfi_endproc

	.globl	PDFText_OpenX
	.p2align	4, 0x90
	.type	PDFText_OpenX,@function
PDFText_OpenX:                          # @PDFText_OpenX
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 32
	subq	$512, %rsp              # imm = 0x200
.Lcfi57:
	.cfi_def_cfa_offset 544
.Lcfi58:
	.cfi_offset %rbx, -32
.Lcfi59:
	.cfi_offset %r14, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	cmpb	$1, g_in_buffering_mode(%rip)
	jne	.LBB10_6
# BB#1:
	movb	$0, g_in_buffering_mode(%rip)
	jmp	.LBB10_3
	.p2align	4, 0x90
.LBB10_2:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	(%rdi), %rax
	movq	%rax, g_qsave_marking_stack(%rip)
	callq	free
.LBB10_3:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	g_qsave_marking_stack(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB10_2
# BB#4:                                 # %._crit_edge.i.i
	cmpb	$0, g_buffer(%rip)
	je	.LBB10_6
# BB#5:
	movl	$g_buffer, %edi
	movq	%rbx, %rsi
	callq	fputs
.LBB10_6:                               # %PDFPage_FlushBuffer.exit.i
	movb	$1, g_page_has_text(%rip)
	cmpb	$1, g_TJ_pending(%rip)
	jne	.LBB10_8
# BB#7:
	movb	$0, g_TJ_pending(%rip)
	movl	$.L.str.5, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB10_8:
	cmpb	$1, g_ET_pending(%rip)
	jne	.LBB10_10
# BB#9:
	movb	$0, g_ET_pending(%rip)
	jmp	.LBB10_11
.LBB10_10:
	movl	$.L.str.76, %esi
	movq	%rbx, %rdi
	callq	PDFPage_Write
	movb	$1, g_valid_text_matrix(%rip)
.LBB10_11:                              # %PDFText_OpenBT.exit
	movq	%rsp, %rbp
	movl	$.L.str.78, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movl	%r14d, %edx
	callq	sprintf
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	PDFPage_Write
	cmpb	$1, g_TJ_pending(%rip)
	jne	.LBB10_13
# BB#12:
	movb	$0, g_TJ_pending(%rip)
	jmp	.LBB10_14
.LBB10_13:
	movl	$.L.str.79, %esi
	movq	%rbx, %rdi
	callq	PDFPage_Write
.LBB10_14:                              # %PDFText_MoveToXAndOpen.exit
	addq	$512, %rsp              # imm = 0x200
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end10:
	.size	PDFText_OpenX, .Lfunc_end10-PDFText_OpenX
	.cfi_endproc

	.globl	PDFText_Open
	.p2align	4, 0x90
	.type	PDFText_Open,@function
PDFText_Open:                           # @PDFText_Open
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 16
.Lcfi62:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpb	$1, g_TJ_pending(%rip)
	jne	.LBB11_4
# BB#1:
	movb	$0, g_TJ_pending(%rip)
	movb	g_ET_pending(%rip), %al
	testb	%al, %al
	jne	.LBB11_3
# BB#2:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
.LBB11_3:                               # %Assert.exit
	movb	$0, g_ET_pending(%rip)
	popq	%rbx
	retq
.LBB11_4:
	cmpb	$1, g_in_buffering_mode(%rip)
	jne	.LBB11_10
# BB#5:
	movb	$0, g_in_buffering_mode(%rip)
	jmp	.LBB11_7
	.p2align	4, 0x90
.LBB11_6:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB11_7 Depth=1
	movq	(%rdi), %rax
	movq	%rax, g_qsave_marking_stack(%rip)
	callq	free
.LBB11_7:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	g_qsave_marking_stack(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB11_6
# BB#8:                                 # %._crit_edge.i.i
	cmpb	$0, g_buffer(%rip)
	je	.LBB11_10
# BB#9:
	movl	$g_buffer, %edi
	movq	%rbx, %rsi
	callq	fputs
.LBB11_10:                              # %PDFPage_FlushBuffer.exit.i
	movb	$1, g_page_has_text(%rip)
	cmpb	$1, g_TJ_pending(%rip)
	jne	.LBB11_12
# BB#11:
	movb	$0, g_TJ_pending(%rip)
	movl	$.L.str.5, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB11_12:
	cmpb	$1, g_ET_pending(%rip)
	jne	.LBB11_14
# BB#13:
	movb	$0, g_ET_pending(%rip)
	cmpb	$1, g_TJ_pending(%rip)
	je	.LBB11_16
	jmp	.LBB11_18
.LBB11_14:
	movl	$.L.str.76, %esi
	movq	%rbx, %rdi
	callq	PDFPage_Write
	movb	$1, g_valid_text_matrix(%rip)
	cmpb	$1, g_TJ_pending(%rip)
	jne	.LBB11_18
.LBB11_16:
	movb	$0, g_TJ_pending(%rip)
	popq	%rbx
	retq
.LBB11_18:
	movl	$.L.str.79, %esi
	movq	%rbx, %rdi
	popq	%rbx
	jmp	PDFPage_Write           # TAILCALL
.Lfunc_end11:
	.size	PDFText_Open, .Lfunc_end11-PDFText_Open
	.cfi_endproc

	.globl	PDFText_Kern
	.p2align	4, 0x90
	.type	PDFText_Kern,@function
PDFText_Kern:                           # @PDFText_Kern
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 24
	subq	$520, %rsp              # imm = 0x208
.Lcfi65:
	.cfi_def_cfa_offset 544
.Lcfi66:
	.cfi_offset %rbx, -24
.Lcfi67:
	.cfi_offset %r14, -16
	movl	%esi, %ecx
	movq	%rdi, %rbx
	negl	%ecx
	movq	%rsp, %r14
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%ecx, %edx
	callq	sprintf
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	PDFPage_Write
	addq	$520, %rsp              # imm = 0x208
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end12:
	.size	PDFText_Kern, .Lfunc_end12-PDFText_Kern
	.cfi_endproc

	.globl	PDFText_Close
	.p2align	4, 0x90
	.type	PDFText_Close,@function
PDFText_Close:                          # @PDFText_Close
	.cfi_startproc
# BB#0:
	cmpl	$0, g_page_contents_obj_num(%rip)
	jne	.LBB13_2
# BB#1:
	pushq	%rax
.Lcfi68:
	.cfi_def_cfa_offset 16
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
	addq	$8, %rsp
.LBB13_2:                               # %Assert.exit
	movb	$1, g_TJ_pending(%rip)
	movb	$1, g_ET_pending(%rip)
	retq
.Lfunc_end13:
	.size	PDFText_Close, .Lfunc_end13-PDFText_Close
	.cfi_endproc

	.globl	PDFPage_Scale
	.p2align	4, 0x90
	.type	PDFPage_Scale,@function
PDFPage_Scale:                          # @PDFPage_Scale
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 24
	subq	$536, %rsp              # imm = 0x218
.Lcfi71:
	.cfi_def_cfa_offset 560
.Lcfi72:
	.cfi_offset %rbx, -24
.Lcfi73:
	.cfi_offset %r14, -16
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movq	%rdi, %rbx
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	leaq	16(%rsp), %r14
	movl	$.L.str.13, %esi
	movb	$2, %al
	movq	%r14, %rdi
	callq	sprintf
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	PDFPage_Write
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	g_page_h_scale_factor(%rip), %xmm0
	movss	%xmm0, g_page_h_scale_factor(%rip)
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	g_page_v_scale_factor(%rip), %xmm0
	movss	%xmm0, g_page_v_scale_factor(%rip)
	addq	$536, %rsp              # imm = 0x218
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end14:
	.size	PDFPage_Scale, .Lfunc_end14-PDFPage_Scale
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI15_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	PDFPage_Rotate
	.p2align	4, 0x90
	.type	PDFPage_Rotate,@function
PDFPage_Rotate:                         # @PDFPage_Rotate
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 24
	subq	$536, %rsp              # imm = 0x218
.Lcfi76:
	.cfi_def_cfa_offset 560
.Lcfi77:
	.cfi_offset %rbx, -24
.Lcfi78:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	cos
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	cvtsd2ss	%xmm0, %xmm2
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm2, %xmm1
	xorps	.LCPI15_0(%rip), %xmm2
	cvtss2sd	%xmm2, %xmm2
	leaq	16(%rsp), %rbx
	movl	$.L.str.14, %esi
	movb	$4, %al
	movq	%rbx, %rdi
	movaps	%xmm0, %xmm3
	callq	sprintf
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	PDFPage_Write
	addq	$536, %rsp              # imm = 0x218
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end15:
	.size	PDFPage_Rotate, .Lfunc_end15-PDFPage_Rotate
	.cfi_endproc

	.globl	PDFPage_Translate
	.p2align	4, 0x90
	.type	PDFPage_Translate,@function
PDFPage_Translate:                      # @PDFPage_Translate
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 24
	subq	$536, %rsp              # imm = 0x218
.Lcfi81:
	.cfi_def_cfa_offset 560
.Lcfi82:
	.cfi_offset %rbx, -24
.Lcfi83:
	.cfi_offset %r14, -16
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movq	%rdi, %rbx
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	leaq	16(%rsp), %r14
	movl	$.L.str.15, %esi
	movb	$2, %al
	movq	%r14, %rdi
	callq	sprintf
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	PDFPage_Write
	xorps	%xmm0, %xmm0
	cvtsi2ssl	g_page_h_origin(%rip), %xmm0
	addss	8(%rsp), %xmm0          # 4-byte Folded Reload
	cvttss2si	%xmm0, %eax
	movl	%eax, g_page_h_origin(%rip)
	xorps	%xmm0, %xmm0
	cvtsi2ssl	g_page_v_origin(%rip), %xmm0
	addss	12(%rsp), %xmm0         # 4-byte Folded Reload
	cvttss2si	%xmm0, %eax
	movl	%eax, g_page_v_origin(%rip)
	addq	$536, %rsp              # imm = 0x218
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end16:
	.size	PDFPage_Translate, .Lfunc_end16-PDFPage_Translate
	.cfi_endproc

	.globl	PDFPage_WriteGraphic
	.p2align	4, 0x90
	.type	PDFPage_WriteGraphic,@function
PDFPage_WriteGraphic:                   # @PDFPage_WriteGraphic
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi87:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi88:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 56
	subq	$536, %rsp              # imm = 0x218
.Lcfi90:
	.cfi_def_cfa_offset 592
.Lcfi91:
	.cfi_offset %rbx, -56
.Lcfi92:
	.cfi_offset %r12, -48
.Lcfi93:
	.cfi_offset %r13, -40
.Lcfi94:
	.cfi_offset %r14, -32
.Lcfi95:
	.cfi_offset %r15, -24
.Lcfi96:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	cmpb	$0, (%rbp)
	je	.LBB17_96
# BB#1:
	cmpl	$0, g_expr_depth(%rip)
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	je	.LBB17_5
# BB#2:
	leaq	4(%rsp), %rsi
	movq	%rsp, %rdx
	movq	%rbp, %rdi
	callq	PDFPage_CollectExpr
	movq	%rax, %rbp
	cmpl	$0, 4(%rsp)
	je	.LBB17_5
# BB#3:
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	leaq	16(%rsp), %rbx
	movl	$.L.str.16, %esi
	movb	$1, %al
	movq	%rbx, %rdi
	callq	sprintf
	movq	%rbx, %rdi
	callq	strlen
	leaq	16(%rsp,%rax), %r13
	cmpl	$0, g_link_depth(%rip)
	jne	.LBB17_6
	jmp	.LBB17_7
.LBB17_5:
	leaq	16(%rsp), %r13
	cmpl	$0, g_link_depth(%rip)
	je	.LBB17_7
.LBB17_6:
	movq	%rbp, %rdi
	callq	PDFPage_CollectLink
	movq	%rax, %rbp
.LBB17_7:                               # %.preheader
	movb	(%rbp), %al
	testb	%al, %al
	je	.LBB17_89
# BB#8:                                 # %.lr.ph
	leaq	528(%rsp), %r14
	jmp	.LBB17_56
.LBB17_9:                               #   in Loop: Header=BB17_56 Depth=1
	xorl	%r15d, %r15d
	jmp	.LBB17_14
.LBB17_10:                              #   in Loop: Header=BB17_56 Depth=1
	movl	$1, %r15d
	jmp	.LBB17_14
.LBB17_11:                              #   in Loop: Header=BB17_56 Depth=1
	movl	$2, %r15d
	jmp	.LBB17_14
.LBB17_12:                              #   in Loop: Header=BB17_56 Depth=1
	movl	$3, %r15d
	jmp	.LBB17_14
.LBB17_13:                              #   in Loop: Header=BB17_56 Depth=1
	movl	$4, %r15d
.LBB17_14:                              #   in Loop: Header=BB17_56 Depth=1
	movq	g_link_keywords(,%r15,8), %rdi
	callq	strlen
	movq	%rax, %rbx
	callq	__ctype_b_loc
	movq	(%rax), %rax
	leaq	1(%rbp,%rbx), %rdi
	.p2align	4, 0x90
.LBB17_15:                              #   Parent Loop BB17_56 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	1(%rdi), %ecx
	incq	%rdi
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB17_15
# BB#16:                                #   in Loop: Header=BB17_56 Depth=1
	movl	$0, g_link_index(%rip)
	incl	g_link_depth(%rip)
	movl	%r15d, g_link_keyword(%rip)
	callq	PDFPage_CollectLink
	movq	%rax, %r12
	jmp	.LBB17_88
.LBB17_17:                              #   in Loop: Header=BB17_56 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB17_21
.LBB17_18:                              #   in Loop: Header=BB17_56 Depth=1
	movl	$1, %ebx
	jmp	.LBB17_21
.LBB17_19:                              #   in Loop: Header=BB17_56 Depth=1
	movl	$2, %ebx
	jmp	.LBB17_21
.LBB17_20:                              #   in Loop: Header=BB17_56 Depth=1
	movl	$3, %ebx
.LBB17_21:                              #   in Loop: Header=BB17_56 Depth=1
	movq	g_doc_info_keywords(,%rbx,8), %rdi
	callq	strlen
	addq	%rax, %r12
	andb	$3, %bl
	cmpb	$1, %bl
	je	.LBB17_28
# BB#22:                                #   in Loop: Header=BB17_56 Depth=1
	cmpb	$2, %bl
	je	.LBB17_32
# BB#23:                                #   in Loop: Header=BB17_56 Depth=1
	cmpb	$3, %bl
	jne	.LBB17_36
# BB#24:                                #   in Loop: Header=BB17_56 Depth=1
	movq	g_doc_keywords(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB17_26
# BB#25:                                #   in Loop: Header=BB17_56 Depth=1
	callq	free
.LBB17_26:                              #   in Loop: Header=BB17_56 Depth=1
	movq	%r12, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	malloc
	movq	%rax, g_doc_keywords(%rip)
	testq	%rax, %rax
	jne	.LBB17_39
# BB#27:                                #   in Loop: Header=BB17_56 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$29, %esi
	movl	$.L.str.125, %edx
	jmp	.LBB17_41
.LBB17_28:                              #   in Loop: Header=BB17_56 Depth=1
	movq	g_doc_title(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB17_30
# BB#29:                                #   in Loop: Header=BB17_56 Depth=1
	callq	free
.LBB17_30:                              #   in Loop: Header=BB17_56 Depth=1
	movq	%r12, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	malloc
	movq	%rax, g_doc_title(%rip)
	testq	%rax, %rax
	jne	.LBB17_39
# BB#31:                                #   in Loop: Header=BB17_56 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$27, %esi
	movl	$.L.str.123, %edx
	jmp	.LBB17_41
.LBB17_32:                              #   in Loop: Header=BB17_56 Depth=1
	movq	g_doc_subject(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB17_34
# BB#33:                                #   in Loop: Header=BB17_56 Depth=1
	callq	free
.LBB17_34:                              #   in Loop: Header=BB17_56 Depth=1
	movq	%r12, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	malloc
	movq	%rax, g_doc_subject(%rip)
	testq	%rax, %rax
	jne	.LBB17_39
# BB#35:                                #   in Loop: Header=BB17_56 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$47, %edi
	movl	$28, %esi
	movl	$.L.str.124, %edx
	jmp	.LBB17_41
.LBB17_36:                              #   in Loop: Header=BB17_56 Depth=1
	movq	g_doc_author(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB17_38
# BB#37:                                #   in Loop: Header=BB17_56 Depth=1
	callq	free
.LBB17_38:                              #   in Loop: Header=BB17_56 Depth=1
	movq	%r12, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	malloc
	movq	%rax, g_doc_author(%rip)
	testq	%rax, %rax
	je	.LBB17_40
.LBB17_39:                              #   in Loop: Header=BB17_56 Depth=1
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	strcpy
	jmp	.LBB17_42
.LBB17_40:                              #   in Loop: Header=BB17_56 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$26, %esi
	movl	$.L.str.122, %edx
.LBB17_41:                              # %PDFPage_ProcessDocInfoKeyword.exit
                                        #   in Loop: Header=BB17_56 Depth=1
	movl	$2, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB17_42:                              # %PDFPage_ProcessDocInfoKeyword.exit
                                        #   in Loop: Header=BB17_56 Depth=1
	movq	%r12, %rdi
	callq	strlen
	addq	%rax, %r12
	jmp	.LBB17_88
.LBB17_43:                              #   in Loop: Header=BB17_56 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB17_50
.LBB17_44:                              #   in Loop: Header=BB17_56 Depth=1
	movl	$1, %ebx
	jmp	.LBB17_50
.LBB17_45:                              #   in Loop: Header=BB17_56 Depth=1
	movl	$2, %ebx
	jmp	.LBB17_50
.LBB17_46:                              #   in Loop: Header=BB17_56 Depth=1
	movl	$3, %ebx
	jmp	.LBB17_50
.LBB17_47:                              #   in Loop: Header=BB17_56 Depth=1
	movl	$4, %ebx
	jmp	.LBB17_50
.LBB17_48:                              #   in Loop: Header=BB17_56 Depth=1
	movl	$5, %ebx
	jmp	.LBB17_50
.LBB17_49:                              #   in Loop: Header=BB17_56 Depth=1
	movl	$6, %ebx
.LBB17_50:                              #   in Loop: Header=BB17_56 Depth=1
	movq	g_arithmetic_keywords(,%rbx,8), %rsi
	movl	$g_expr, %edi
	callq	strcpy
	movq	g_arithmetic_keywords(,%rbx,8), %rdi
	callq	strlen
	addq	%rax, %r12
	callq	__ctype_b_loc
	movq	(%rax), %rax
	.p2align	4, 0x90
.LBB17_51:                              #   Parent Loop BB17_56 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r12), %ecx
	incq	%r12
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB17_51
# BB#52:                                #   in Loop: Header=BB17_56 Depth=1
	cmpb	$40, %cl
	je	.LBB17_54
# BB#53:                                #   in Loop: Header=BB17_56 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$36, %esi
	movl	$.L.str.18, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB17_54:                              #   in Loop: Header=BB17_56 Depth=1
	movl	$g_expr, %edi
	callq	strlen
	movw	$40, g_expr(%rax)
	movl	$g_expr, %edi
	callq	strlen
	movl	%eax, g_expr_index(%rip)
	incl	g_expr_depth(%rip)
	movq	%r12, %rdi
	movq	%rsp, %rsi
	leaq	4(%rsp), %rdx
	callq	PDFPage_CollectExpr
	movq	%rax, %r12
	cmpl	$0, (%rsp)
	je	.LBB17_88
# BB#55:                                #   in Loop: Header=BB17_56 Depth=1
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.16, %esi
	movb	$1, %al
	movq	%r13, %rdi
	callq	sprintf
	jmp	.LBB17_87
	.p2align	4, 0x90
.LBB17_56:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_51 Depth 2
                                        #     Child Loop BB17_15 Depth 2
	cmpq	%r14, %r13
	jb	.LBB17_58
# BB#57:                                #   in Loop: Header=BB17_56 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
	movb	(%rbp), %al
.LBB17_58:                              # %Assert.exit
                                        #   in Loop: Header=BB17_56 Depth=1
	leaq	1(%rbp), %r12
	cmpb	$95, %al
	jne	.LBB17_81
# BB#59:                                #   in Loop: Header=BB17_56 Depth=1
	cmpb	$95, (%r12)
	jne	.LBB17_81
# BB#60:                                # %.lr.ph.i
                                        #   in Loop: Header=BB17_56 Depth=1
	leaq	2(%rbp), %r12
	movq	g_graphic_keywords(%rip), %r15
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB17_82
# BB#61:                                # %.lr.ph.i.193
                                        #   in Loop: Header=BB17_56 Depth=1
	movq	g_graphic_keywords+8(%rip), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB17_83
# BB#62:                                # %.lr.ph.i.294
                                        #   in Loop: Header=BB17_56 Depth=1
	movq	g_graphic_keywords+16(%rip), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB17_84
# BB#63:                                # %.lr.ph.i.395
                                        #   in Loop: Header=BB17_56 Depth=1
	movq	g_graphic_keywords+24(%rip), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB17_85
# BB#64:                                # %.lr.ph.i63.preheader96
                                        #   in Loop: Header=BB17_56 Depth=1
	movq	g_link_keywords(%rip), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB17_9
# BB#65:                                # %.lr.ph.i63.197
                                        #   in Loop: Header=BB17_56 Depth=1
	movq	g_link_keywords+8(%rip), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB17_10
# BB#66:                                # %.lr.ph.i63.298
                                        #   in Loop: Header=BB17_56 Depth=1
	movq	g_link_keywords+16(%rip), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB17_11
# BB#67:                                # %.lr.ph.i63.399
                                        #   in Loop: Header=BB17_56 Depth=1
	movq	g_link_keywords+24(%rip), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB17_12
# BB#68:                                # %.lr.ph.i63.4100
                                        #   in Loop: Header=BB17_56 Depth=1
	movq	g_link_keywords+32(%rip), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB17_13
# BB#69:                                # %.lr.ph.i68.preheader101
                                        #   in Loop: Header=BB17_56 Depth=1
	movq	g_doc_info_keywords(%rip), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB17_17
# BB#70:                                # %.lr.ph.i68.1102
                                        #   in Loop: Header=BB17_56 Depth=1
	movq	g_doc_info_keywords+8(%rip), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB17_18
# BB#71:                                # %.lr.ph.i68.2103
                                        #   in Loop: Header=BB17_56 Depth=1
	movq	g_doc_info_keywords+16(%rip), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB17_19
# BB#72:                                # %.lr.ph.i68.3104
                                        #   in Loop: Header=BB17_56 Depth=1
	movq	g_doc_info_keywords+24(%rip), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB17_20
# BB#73:                                # %.lr.ph.i73.preheader105
                                        #   in Loop: Header=BB17_56 Depth=1
	movq	g_arithmetic_keywords(%rip), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB17_43
# BB#74:                                # %.lr.ph.i73.1106
                                        #   in Loop: Header=BB17_56 Depth=1
	movq	g_arithmetic_keywords+8(%rip), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB17_44
# BB#75:                                # %.lr.ph.i73.2107
                                        #   in Loop: Header=BB17_56 Depth=1
	movq	g_arithmetic_keywords+16(%rip), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB17_45
# BB#76:                                # %.lr.ph.i73.3108
                                        #   in Loop: Header=BB17_56 Depth=1
	movq	g_arithmetic_keywords+24(%rip), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB17_46
# BB#77:                                # %.lr.ph.i73.4109
                                        #   in Loop: Header=BB17_56 Depth=1
	movq	g_arithmetic_keywords+32(%rip), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB17_47
# BB#78:                                # %.lr.ph.i73.5110
                                        #   in Loop: Header=BB17_56 Depth=1
	movq	g_arithmetic_keywords+40(%rip), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB17_48
# BB#79:                                # %.lr.ph.i73.6111
                                        #   in Loop: Header=BB17_56 Depth=1
	movq	g_arithmetic_keywords+48(%rip), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB17_49
# BB#80:                                # %PDFKeyword_Find.exit75112
                                        #   in Loop: Header=BB17_56 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$37, %esi
	movl	$.L.str.20, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	callq	Error
	movw	$24415, (%r13)          # imm = 0x5F5F
	addq	$2, %r13
	jmp	.LBB17_88
	.p2align	4, 0x90
.LBB17_81:                              # %Assert.exit._crit_edge
                                        #   in Loop: Header=BB17_56 Depth=1
	movb	%al, (%r13)
	incq	%r13
	jmp	.LBB17_88
.LBB17_82:                              #   in Loop: Header=BB17_56 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB17_86
.LBB17_83:                              #   in Loop: Header=BB17_56 Depth=1
	movl	$1, %ebx
	jmp	.LBB17_86
.LBB17_84:                              #   in Loop: Header=BB17_56 Depth=1
	movl	$2, %ebx
	jmp	.LBB17_86
.LBB17_85:                              #   in Loop: Header=BB17_56 Depth=1
	movl	$3, %ebx
	.p2align	4, 0x90
.LBB17_86:                              #   in Loop: Header=BB17_56 Depth=1
	movq	g_graphic_keywords(,%rbx,8), %rdi
	callq	strlen
	addq	%rax, %r12
	movl	g_graphics_vars(,%rbx,4), %edx
	movl	$.L.str.17, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	sprintf
.LBB17_87:                              #   in Loop: Header=BB17_56 Depth=1
	movq	%r13, %rdi
	callq	strlen
	addq	%rax, %r13
.LBB17_88:                              #   in Loop: Header=BB17_56 Depth=1
	movb	(%r12), %al
	testb	%al, %al
	movq	%r12, %rbp
	jne	.LBB17_56
.LBB17_89:                              # %._crit_edge
	movb	$0, (%r13)
	cmpb	$1, g_in_buffering_mode(%rip)
	movq	8(%rsp), %rbx           # 8-byte Reload
	jne	.LBB17_95
# BB#90:
	movb	$0, g_in_buffering_mode(%rip)
	jmp	.LBB17_92
	.p2align	4, 0x90
.LBB17_91:                              # %.lr.ph.i76
                                        #   in Loop: Header=BB17_92 Depth=1
	movq	(%rdi), %rax
	movq	%rax, g_qsave_marking_stack(%rip)
	callq	free
.LBB17_92:                              # %.lr.ph.i76
                                        # =>This Inner Loop Header: Depth=1
	movq	g_qsave_marking_stack(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB17_91
# BB#93:                                # %._crit_edge.i
	cmpb	$0, g_buffer(%rip)
	je	.LBB17_95
# BB#94:
	movl	$g_buffer, %edi
	movq	%rbx, %rsi
	callq	fputs
.LBB17_95:                              # %PDFPage_FlushBuffer.exit
	leaq	16(%rsp), %rsi
	movq	%rbx, %rdi
	callq	PDFPage_Write
.LBB17_96:
	addq	$536, %rsp              # imm = 0x218
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	PDFPage_WriteGraphic, .Lfunc_end17-PDFPage_WriteGraphic
	.cfi_endproc

	.p2align	4, 0x90
	.type	PDFPage_CollectExpr,@function
PDFPage_CollectExpr:                    # @PDFPage_CollectExpr
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 32
.Lcfi100:
	.cfi_offset %rbx, -32
.Lcfi101:
	.cfi_offset %r14, -24
.Lcfi102:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$0, (%r14)
	movb	(%rbx), %al
	testb	%al, %al
	je	.LBB18_12
# BB#1:                                 # %.lr.ph.preheader
	incq	%rbx
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB18_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rbx
	movl	g_expr_index(%rip), %ecx
	cmpl	$512, %ecx              # imm = 0x200
	jb	.LBB18_4
# BB#3:                                 #   in Loop: Header=BB18_2 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$34, %esi
	movl	$.L.str.80, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	movzbl	-1(%rbx), %eax
	movl	g_expr_index(%rip), %ecx
.LBB18_4:                               #   in Loop: Header=BB18_2 Depth=1
	leal	1(%rcx), %edx
	movl	%edx, g_expr_index(%rip)
	movslq	%ecx, %rcx
	movb	%al, g_expr(%rcx)
	cmpb	$41, %al
	je	.LBB18_8
# BB#5:                                 #   in Loop: Header=BB18_2 Depth=1
	cmpb	$40, %al
	jne	.LBB18_7
# BB#6:                                 #   in Loop: Header=BB18_2 Depth=1
	incl	g_expr_depth(%rip)
	jmp	.LBB18_7
	.p2align	4, 0x90
.LBB18_8:                               #   in Loop: Header=BB18_2 Depth=1
	movl	g_expr_depth(%rip), %eax
	testl	%eax, %eax
	jne	.LBB18_10
# BB#9:                                 #   in Loop: Header=BB18_2 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
	movl	g_expr_depth(%rip), %eax
.LBB18_10:                              # %Assert.exit
                                        #   in Loop: Header=BB18_2 Depth=1
	decl	%eax
	movl	%eax, g_expr_depth(%rip)
	je	.LBB18_11
.LBB18_7:                               # %.backedge
                                        #   in Loop: Header=BB18_2 Depth=1
	movzbl	(%rbx), %eax
	leaq	1(%rbx), %rcx
	testb	%al, %al
	jne	.LBB18_2
	jmp	.LBB18_12
.LBB18_11:                              # %.critedge
	movslq	g_expr_index(%rip), %rax
	movb	$0, g_expr(%rax)
	movl	$g_expr, %edi
	movq	%r15, %rsi
	callq	PDFPage_EvalExpr
	movl	$1, (%r14)
.LBB18_12:                              # %.loopexit
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end18:
	.size	PDFPage_CollectExpr, .Lfunc_end18-PDFPage_CollectExpr
	.cfi_endproc

	.p2align	4, 0x90
	.type	PDFPage_CollectLink,@function
PDFPage_CollectLink:                    # @PDFPage_CollectLink
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi103:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi104:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi106:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi107:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi109:
	.cfi_def_cfa_offset 80
.Lcfi110:
	.cfi_offset %rbx, -56
.Lcfi111:
	.cfi_offset %r12, -48
.Lcfi112:
	.cfi_offset %r13, -40
.Lcfi113:
	.cfi_offset %r14, -32
.Lcfi114:
	.cfi_offset %r15, -24
.Lcfi115:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movb	(%rbx), %dl
	testb	%dl, %dl
	je	.LBB19_41
# BB#1:                                 # %.lr.ph.preheader
	movl	g_link_index(%rip), %ecx
	.p2align	4, 0x90
.LBB19_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$512, %ecx              # imm = 0x200
	movl	%ecx, %esi
	jb	.LBB19_4
# BB#3:                                 #   in Loop: Header=BB19_2 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$35, %esi
	movl	$.L.str.94, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	movzbl	(%rbx), %edx
	movl	g_link_index(%rip), %esi
.LBB19_4:                               #   in Loop: Header=BB19_2 Depth=1
	leaq	1(%rbx), %rax
	movslq	%esi, %rdi
	leaq	1(%rdi), %rcx
	movl	%ecx, g_link_index(%rip)
	movb	%dl, g_link(%rdi)
	cmpb	$60, %dl
	je	.LBB19_11
# BB#5:                                 #   in Loop: Header=BB19_2 Depth=1
	cmpb	$62, %dl
	jne	.LBB19_14
# BB#6:                                 #   in Loop: Header=BB19_2 Depth=1
	cmpb	$62, (%rax)
	jne	.LBB19_14
# BB#7:                                 #   in Loop: Header=BB19_2 Depth=1
	movl	g_link_depth(%rip), %eax
	testl	%eax, %eax
	jne	.LBB19_9
# BB#8:                                 #   in Loop: Header=BB19_2 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
	movl	g_link_depth(%rip), %eax
.LBB19_9:                               # %Assert.exit
                                        #   in Loop: Header=BB19_2 Depth=1
	decl	%eax
	movl	%eax, g_link_depth(%rip)
	je	.LBB19_15
# BB#10:                                #   in Loop: Header=BB19_2 Depth=1
	movzbl	1(%rbx), %eax
	addq	$2, %rbx
	movslq	g_link_index(%rip), %rdx
	leal	1(%rdx), %ecx
	movl	%ecx, g_link_index(%rip)
	movb	%al, g_link(%rdx)
	jmp	.LBB19_13
	.p2align	4, 0x90
.LBB19_11:                              #   in Loop: Header=BB19_2 Depth=1
	cmpb	$60, (%rax)
	jne	.LBB19_14
# BB#12:                                #   in Loop: Header=BB19_2 Depth=1
	addq	$2, %rbx
	addl	$2, %esi
	movl	%esi, g_link_index(%rip)
	movb	$60, g_link(%rcx)
	incl	g_link_depth(%rip)
	movl	%esi, %ecx
.LBB19_13:                              # %.thread.backedge
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	%rbx, %rax
.LBB19_14:                              # %.thread.backedge
                                        #   in Loop: Header=BB19_2 Depth=1
	movzbl	(%rax), %edx
	testb	%dl, %dl
	movq	%rax, %rbx
	jne	.LBB19_2
	jmp	.LBB19_42
.LBB19_15:
	movslq	g_link_index(%rip), %rax
	leal	-1(%rax), %ecx
	movl	%ecx, g_link_index(%rip)
	movb	$0, g_link-1(%rax)
	movl	g_link_keyword(%rip), %r12d
	movb	g_link(%rip), %r13b
	movb	$1, %bpl
	testb	%r13b, %r13b
	je	.LBB19_23
# BB#16:                                # %.lr.ph.i
	callq	__ctype_b_loc
	movq	(%rax), %rax
	xorl	%r14d, %r14d
	movl	$g_link+1, %r15d
	jmp	.LBB19_18
	.p2align	4, 0x90
.LBB19_17:                              # %.backedge.i.backedge
                                        #   in Loop: Header=BB19_18 Depth=1
	incq	%r15
.LBB19_18:                              # %.backedge.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%r13b, %ecx
	movzwl	(%rax,%rcx,2), %ecx
	movzbl	(%r15), %r13d
	testb	$32, %ch
	je	.LBB19_22
# BB#19:                                # %.backedge.i
                                        #   in Loop: Header=BB19_18 Depth=1
	cmpb	$95, %r13b
	jne	.LBB19_22
# BB#20:                                #   in Loop: Header=BB19_18 Depth=1
	cmpb	$95, 1(%r15)
	je	.LBB19_24
# BB#21:                                # %.critedge83.thread.i
                                        #   in Loop: Header=BB19_18 Depth=1
	incl	%r14d
	movb	$95, %r13b
	jmp	.LBB19_17
	.p2align	4, 0x90
.LBB19_22:                              # %.critedge83.i
                                        #   in Loop: Header=BB19_18 Depth=1
	incl	%r14d
	testb	%r13b, %r13b
	jne	.LBB19_17
	jmp	.LBB19_25
.LBB19_23:
	movl	$g_link, %r15d
	xorl	%r14d, %r14d
	jmp	.LBB19_25
.LBB19_24:                              # %..critedge.i.loopexit_crit_edge
	decq	%r15
	xorl	%ebp, %ebp
.LBB19_25:                              # %.critedge.i
	leaq	1(%r15), %r13
	testb	%bpl, %bpl
	cmoveq	%r13, %r15
	decq	%r15
	.p2align	4, 0x90
.LBB19_26:                              # =>This Inner Loop Header: Depth=1
	cmpb	$0, 1(%r15)
	leaq	1(%r15), %r15
	jne	.LBB19_26
# BB#27:
	xorl	%eax, %eax
	testb	%bpl, %bpl
	cmovneq	%rax, %r13
	testl	%r14d, %r14d
	je	.LBB19_38
# BB#28:
	movl	g_page_h_origin(%rip), %eax
	cvtsi2ssl	%eax, %xmm0
	movss	g_page_h_scale_factor(%rip), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	cvttss2si	%xmm0, %edx
	movl	g_page_v_origin(%rip), %esi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%esi, %xmm0
	movss	g_page_v_scale_factor(%rip), %xmm2 # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	cvttss2si	%xmm0, %ecx
	addl	g_graphics_vars(%rip), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	mulss	%xmm1, %xmm0
	cvttss2si	%xmm0, %r8d
	addl	g_graphics_vars+4(%rip), %esi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%esi, %xmm0
	mulss	%xmm2, %xmm0
	cvttss2si	%xmm0, %r9d
	leal	-1(%r12), %eax
	cmpl	$2, %eax
	jb	.LBB19_43
# BB#29:
	leal	-3(%r12), %eax
	cmpl	$2, %eax
	jae	.LBB19_47
# BB#30:
	movl	%r9d, 8(%rsp)           # 4-byte Spill
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movl	%edx, 20(%rsp)          # 4-byte Spill
	xorl	%r15d, %r15d
	cmpl	$4, %r12d
	sete	%r13b
	movl	$40, %edi
	callq	malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB19_32
# BB#31:
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$16, %esi
	movl	$.L.str.108, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB19_32:
	leal	1(%r14), %edi
	callq	malloc
	movq	%rax, 8(%rbp)
	testq	%rax, %rax
	jne	.LBB19_34
# BB#33:
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$17, %esi
	movl	$.L.str.108, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	8(%rbp), %rax
.LBB19_34:
	movb	%r13b, %r15b
	movl	%r14d, %r14d
	movl	$g_link, %esi
	movq	%rax, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movq	8(%rbp), %rax
	movb	$0, (%rax,%r14)
	cmpl	$0, g_page_contents_obj_num(%rip)
	jne	.LBB19_36
# BB#35:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
.LBB19_36:                              # %Assert.exit.i.i
	movl	g_page_object_num(%rip), %eax
	movl	%eax, 16(%rbp)
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, 20(%rbp)
	movl	16(%rsp), %eax          # 4-byte Reload
	movl	%eax, 24(%rbp)
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, 28(%rbp)
	movl	8(%rsp), %eax           # 4-byte Reload
	movl	%eax, 32(%rbp)
	movl	%r15d, 36(%rbp)
	movq	g_target_annot_list(%rip), %rax
	movq	%rax, (%rbp)
	movq	%rbp, g_target_annot_list(%rip)
	cmpl	$4, %r12d
	jne	.LBB19_40
# BB#37:
	movl	%r15d, g_has_exported_targets(%rip)
	jmp	.LBB19_40
.LBB19_38:
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$22, %esi
	movl	$.L.str.95, %edx
	movl	$2, %ecx
.LBB19_39:
	xorl	%eax, %eax
	callq	Error
.LBB19_40:
	addq	$2, %rbx
.LBB19_41:                              # %.loopexit
	movq	%rbx, %rax
.LBB19_42:                              # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB19_43:
	movl	$0, (%rsp)
	movl	%r12d, %edi
	movl	%r14d, %esi
	callq	PDFSourceAnnot_New
	movq	%rax, %r14
	cmpl	$1, %r12d
	jne	.LBB19_40
# BB#44:
	testb	%bpl, %bpl
	jne	.LBB19_46
# BB#45:
	movq	g_external_file_spec_keyword(%rip), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %r15
	movl	%r15d, %edx
	movq	%rbp, %rdi
	movq	%r13, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB19_58
.LBB19_46:                              # %PDFKeyword_Find.exit89.thread.i
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$24, %esi
	movl	$.L.str.96, %edx
	movl	$1, %ecx
	jmp	.LBB19_39
.LBB19_47:
	testl	%r12d, %r12d
	jne	.LBB19_40
# BB#48:
	xorl	%r12d, %r12d
	testb	%bpl, %bpl
	jne	.LBB19_70
# BB#49:                                # %.lr.ph.i.preheader.i
	movl	%r9d, 8(%rsp)           # 4-byte Spill
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movl	%edx, %r13d
	movq	g_dest_link_options(%rip), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB19_69
# BB#50:                                # %.lr.ph.i.1102.i
	movq	g_dest_link_options+8(%rip), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB19_62
# BB#51:                                # %.lr.ph.i.2103.i
	movq	g_dest_link_options+16(%rip), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB19_63
# BB#52:                                # %.lr.ph.i.3104.i
	movq	g_dest_link_options+24(%rip), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB19_64
# BB#53:                                # %.lr.ph.i.4105.i
	movq	g_dest_link_options+32(%rip), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB19_65
# BB#54:                                # %.lr.ph.i.5106.i
	movq	g_dest_link_options+40(%rip), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB19_66
# BB#55:                                # %.lr.ph.i.6107.i
	movq	g_dest_link_options+48(%rip), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB19_67
# BB#56:                                # %.lr.ph.i.7108.i
	movq	g_dest_link_options+56(%rip), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB19_68
# BB#57:                                # %PDFKeyword_Find.exit.i
	movl	$g_link, %eax
	subl	%eax, %r15d
	movl	%r15d, %r14d
	jmp	.LBB19_69
.LBB19_58:                              # %PDFKeyword_Find.exit89.i
	addq	%r15, %r13
	movq	%r13, %rdi
	callq	strlen
	testl	%eax, %eax
	je	.LBB19_46
# BB#59:
	incl	%eax
	movq	%rax, %rdi
	callq	malloc
	movq	%rax, 24(%r14)
	testq	%rax, %rax
	jne	.LBB19_61
# BB#60:
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$25, %esi
	movl	$.L.str.97, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	24(%r14), %rax
.LBB19_61:
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	strcpy
	jmp	.LBB19_40
.LBB19_62:
	movl	$1, %r12d
	jmp	.LBB19_69
.LBB19_63:
	movl	$2, %r12d
	jmp	.LBB19_69
.LBB19_64:
	movl	$3, %r12d
	jmp	.LBB19_69
.LBB19_65:
	movl	$4, %r12d
	jmp	.LBB19_69
.LBB19_66:
	movl	$5, %r12d
	jmp	.LBB19_69
.LBB19_67:
	movl	$6, %r12d
	jmp	.LBB19_69
.LBB19_68:
	movl	$7, %r12d
.LBB19_69:                              # %PDFKeyword_Find.exit.thread.i
	movl	%r13d, %edx
	movl	16(%rsp), %ecx          # 4-byte Reload
	movl	12(%rsp), %r8d          # 4-byte Reload
	movl	8(%rsp), %r9d           # 4-byte Reload
.LBB19_70:                              # %PDFKeyword_Find.exit.thread.i
	movl	%r12d, (%rsp)
	xorl	%edi, %edi
	movl	%r14d, %esi
	callq	PDFSourceAnnot_New
	jmp	.LBB19_40
.Lfunc_end19:
	.size	PDFPage_CollectLink, .Lfunc_end19-PDFPage_CollectLink
	.cfi_endproc

	.globl	PDFPage_PrintUnderline
	.p2align	4, 0x90
	.type	PDFPage_PrintUnderline,@function
PDFPage_PrintUnderline:                 # @PDFPage_PrintUnderline
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi116:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi117:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi118:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi119:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi120:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi121:
	.cfi_def_cfa_offset 56
	subq	$536, %rsp              # imm = 0x218
.Lcfi122:
	.cfi_def_cfa_offset 592
.Lcfi123:
	.cfi_offset %rbx, -56
.Lcfi124:
	.cfi_offset %r12, -48
.Lcfi125:
	.cfi_offset %r13, -40
.Lcfi126:
	.cfi_offset %r14, -32
.Lcfi127:
	.cfi_offset %r15, -24
.Lcfi128:
	.cfi_offset %rbp, -16
	movl	%r8d, %r14d
	movl	%ecx, %ebp
	movl	%edx, %r15d
	movl	%esi, %r12d
	movq	%rdi, %r13
	cmpb	$1, g_in_buffering_mode(%rip)
	jne	.LBB20_6
# BB#1:
	movb	$0, g_in_buffering_mode(%rip)
	jmp	.LBB20_3
	.p2align	4, 0x90
.LBB20_2:                               # %.lr.ph.i
                                        #   in Loop: Header=BB20_3 Depth=1
	movq	(%rdi), %rax
	movq	%rax, g_qsave_marking_stack(%rip)
	callq	free
.LBB20_3:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	g_qsave_marking_stack(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB20_2
# BB#4:                                 # %._crit_edge.i
	cmpb	$0, g_buffer(%rip)
	je	.LBB20_6
# BB#5:
	movl	$g_buffer, %edi
	movq	%r13, %rsi
	callq	fputs
.LBB20_6:                               # %PDFPage_FlushBuffer.exit
	movl	%ebp, (%rsp)
	leaq	16(%rsp), %rbx
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	movl	%ebp, %r8d
	movq	%rbx, %rdi
	movl	%r14d, %edx
	movl	%r12d, %ecx
	movl	%r15d, %r9d
	callq	sprintf
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	PDFPage_Write
	addq	$536, %rsp              # imm = 0x218
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	PDFPage_PrintUnderline, .Lfunc_end20-PDFPage_PrintUnderline
	.cfi_endproc

	.globl	PDFPage_Init
	.p2align	4, 0x90
	.type	PDFPage_Init,@function
PDFPage_Init:                           # @PDFPage_Init
	.cfi_startproc
# BB#0:
	movb	$0, g_page_uses_fonts(%rip)
	movb	$0, g_page_has_text(%rip)
	movl	$0, g_page_contents_obj_num(%rip)
	movl	$0, g_page_length_obj_num(%rip)
	movl	$0, g_page_start_offset(%rip)
	movss	%xmm0, g_page_v_scale_factor(%rip)
	movss	%xmm0, g_page_h_scale_factor(%rip)
	movl	$0, g_page_v_origin(%rip)
	movl	$0, g_page_h_origin(%rip)
	movl	%esi, g_page_line_width(%rip)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, g_graphics_vars(%rip)
	movl	$0, g_units+16(%rip)
	movl	$0, g_units+20(%rip)
	movl	$0, g_units+24(%rip)
	movb	$0, g_ET_pending(%rip)
	movb	$0, g_TJ_pending(%rip)
	movb	$0, g_valid_text_matrix(%rip)
	movq	g_font_list(%rip), %rax
	testq	%rax, %rax
	je	.LBB21_3
	.p2align	4, 0x90
.LBB21_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, 44(%rax)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB21_1
.LBB21_3:                               # %._crit_edge
	pushq	%rax
.Lcfi129:
	.cfi_def_cfa_offset 16
	movq	$0, g_qsave_stack(%rip)
	movq	$0, g_qsave_marking_stack(%rip)
	movl	$0, g_buffer_pos(%rip)
	movb	$0, g_buffer(%rip)
	movb	$1, g_in_buffering_mode(%rip)
	incl	g_page_count(%rip)
	callq	PDFObject_New
	movl	%eax, g_page_object_num(%rip)
	popq	%rax
	retq
.Lfunc_end21:
	.size	PDFPage_Init, .Lfunc_end21-PDFPage_Init
	.cfi_endproc

	.globl	PDFPage_Cleanup
	.p2align	4, 0x90
	.type	PDFPage_Cleanup,@function
PDFPage_Cleanup:                        # @PDFPage_Cleanup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi130:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi131:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi133:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi134:
	.cfi_def_cfa_offset 48
.Lcfi135:
	.cfi_offset %rbx, -48
.Lcfi136:
	.cfi_offset %r12, -40
.Lcfi137:
	.cfi_offset %r14, -32
.Lcfi138:
	.cfi_offset %r15, -24
.Lcfi139:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpq	$0, g_qsave_stack(%rip)
	je	.LBB22_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
.LBB22_2:                               # %Assert.exit
	cmpl	$0, g_page_contents_obj_num(%rip)
	je	.LBB22_19
# BB#3:
	cmpb	$1, g_in_buffering_mode(%rip)
	jne	.LBB22_8
# BB#4:
	movl	$0, g_buffer_pos(%rip)
	movb	$0, g_buffer(%rip)
	movb	$0, g_in_buffering_mode(%rip)
	movq	g_qsave_marking_stack(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB22_8
	.p2align	4, 0x90
.LBB22_5:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rax
	movq	%rax, g_qsave_marking_stack(%rip)
	callq	free
	movq	g_qsave_marking_stack(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB22_5
# BB#6:                                 # %._crit_edge.i.i
	cmpb	$0, g_buffer(%rip)
	je	.LBB22_8
# BB#7:
	movl	$g_buffer, %edi
	movq	%r14, %rsi
	callq	fputs
.LBB22_8:                               # %PDFPage_FlushBuffer.exit.i
	cmpl	$0, g_page_contents_obj_num(%rip)
	jne	.LBB22_10
# BB#9:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
.LBB22_10:                              # %PDFPage_End.exit
	movq	%r14, %rdi
	callq	ftell
	movq	%rax, %rbx
	subl	g_page_start_offset(%rip), %ebx
	movl	$.L.str.134, %edi
	movl	$17, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	g_page_length_obj_num(%rip), %esi
	movq	%r14, %rdi
	callq	PDFObject_WriteObj
	movl	$.L.str.23, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%ebx, %edx
	callq	fprintf
	movq	g_font_list(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB22_13
	jmp	.LBB22_19
	.p2align	4, 0x90
.LBB22_11:                              # %PDFFont_WriteFontResource.exit
                                        #   in Loop: Header=BB22_13 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB22_19
.LBB22_13:                              # %.lr.ph139
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, 40(%rbx)
	jne	.LBB22_11
# BB#14:                                #   in Loop: Header=BB22_13 Depth=1
	movl	$1, 40(%rbx)
	movl	36(%rbx), %esi
	testl	%esi, %esi
	jne	.LBB22_16
# BB#15:                                #   in Loop: Header=BB22_13 Depth=1
	callq	PDFObject_New
	movl	%eax, %esi
	movl	%esi, 36(%rbx)
.LBB22_16:                              # %PDFFont_WriteObject.exit.i
                                        #   in Loop: Header=BB22_13 Depth=1
	movq	%r14, %rdi
	callq	PDFObject_WriteObj
	movl	$.L.str.137, %edi
	movl	$31, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movq	8(%rbx), %rdx
	movl	$.L.str.138, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	24(%rbx), %rdx
	movl	$.L.str.139, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	cmpl	$0, 32(%rbx)
	je	.LBB22_18
# BB#17:                                #   in Loop: Header=BB22_13 Depth=1
	movl	$.L.str.140, %edi
	movl	$10, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	32(%rbx), %edx
	movl	$.L.str.169, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	$10, %edi
	movq	%r14, %rsi
	callq	fputc
.LBB22_18:                              #   in Loop: Header=BB22_13 Depth=1
	movl	$.L.str.41, %edi
	movl	$10, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	jmp	.LBB22_11
.LBB22_19:                              # %.preheader
	movq	g_source_annot_list(%rip), %rbx
	xorl	%r15d, %r15d
	testq	%rbx, %rbx
	je	.LBB22_23
	.p2align	4, 0x90
.LBB22_21:                              # %.lr.ph134
                                        # =>This Inner Loop Header: Depth=1
	movl	52(%rbx), %eax
	cmpl	g_page_object_num(%rip), %eax
	jne	.LBB22_20
# BB#22:                                #   in Loop: Header=BB22_21 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	PDFSourceAnnot_Write
	movl	$1, %r15d
.LBB22_20:                              #   in Loop: Header=BB22_21 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB22_21
.LBB22_23:                              # %._crit_edge135
	movl	g_page_count(%rip), %ebp
	decl	%ebp
	movl	%ebp, %r12d
	andl	$63, %r12d
	je	.LBB22_26
# BB#24:
	movq	g_cur_page_block(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB22_35
# BB#25:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
.LBB22_26:
	movl	$264, %edi              # imm = 0x108
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB22_28
# BB#27:
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$38, %esi
	movl	$.L.str.24, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB22_28:
	cmpl	$63, %ebp
	ja	.LBB22_31
# BB#29:
	cmpq	$0, g_page_block_list(%rip)
	je	.LBB22_33
# BB#30:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
.LBB22_33:
	movl	$g_page_block_list, %eax
	jmp	.LBB22_34
.LBB22_31:
	movq	g_cur_page_block(%rip), %rax
	testq	%rax, %rax
	jne	.LBB22_34
# BB#32:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
	movq	g_cur_page_block(%rip), %rax
.LBB22_34:                              # %Assert.exit117
	movq	%rbx, (%rax)
	movq	$0, (%rbx)
	movq	%rbx, g_cur_page_block(%rip)
.LBB22_35:                              # %Assert.exit119
	movl	g_page_object_num(%rip), %esi
	movl	%r12d, %eax
	movl	%esi, 8(%rbx,%rax,4)
	movq	%r14, %rdi
	callq	PDFObject_WriteObj
	movl	$.L.str.26, %edi
	movl	$15, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	g_doc_h_bound(%rip), %edx
	movl	g_doc_v_bound(%rip), %ecx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	$.L.str.28, %edi
	movl	$8, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	g_pages_root(%rip), %edx
	movl	$.L.str.169, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	$10, %edi
	movq	%r14, %rsi
	callq	fputc
	cmpl	$0, g_page_contents_obj_num(%rip)
	je	.LBB22_37
# BB#36:
	movl	$.L.str.30, %edi
	movl	$10, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	g_page_contents_obj_num(%rip), %edx
	movl	$.L.str.169, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	$10, %edi
	movq	%r14, %rsi
	callq	fputc
.LBB22_37:
	movb	g_page_uses_fonts(%rip), %al
	testb	%al, %al
	jne	.LBB22_41
# BB#38:
	movb	g_page_has_text(%rip), %al
	testb	$1, %al
	jne	.LBB22_41
# BB#39:
	testb	$1, %al
	je	.LBB22_53
	jmp	.LBB22_50
.LBB22_41:
	movl	$.L.str.31, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	cmpb	$1, g_page_uses_fonts(%rip)
	jne	.LBB22_49
# BB#42:
	movq	g_font_list(%rip), %rbx
	movl	$.L.str.32, %edi
	movl	$8, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	testq	%rbx, %rbx
	je	.LBB22_48
	.p2align	4, 0x90
.LBB22_44:                              # %.lr.ph129
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, 44(%rbx)
	je	.LBB22_43
# BB#45:                                #   in Loop: Header=BB22_44 Depth=1
	movq	8(%rbx), %rdx
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	36(%rbx), %edx
	testl	%edx, %edx
	jne	.LBB22_47
# BB#46:                                #   in Loop: Header=BB22_44 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
	movl	36(%rbx), %edx
.LBB22_47:                              # %PDFFont_WriteObjectRef.exit
                                        #   in Loop: Header=BB22_44 Depth=1
	movl	$.L.str.169, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
.LBB22_43:                              #   in Loop: Header=BB22_44 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB22_44
.LBB22_48:                              # %._crit_edge130
	movl	$.L.str.34, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
.LBB22_49:                              # %thread-pre-split
	movb	g_page_has_text(%rip), %al
	testb	%al, %al
	je	.LBB22_53
.LBB22_50:
	movl	$.L.str.35, %edi
	movl	$15, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	cmpb	$1, g_page_has_text(%rip)
	jne	.LBB22_52
# BB#51:
	movl	$.L.str.36, %edi
	movl	$6, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
.LBB22_52:
	movl	$.L.str.37, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movb	g_page_has_text(%rip), %al
	jmp	.LBB22_54
.LBB22_53:
	xorl	%eax, %eax
.LBB22_54:
	movb	g_page_uses_fonts(%rip), %cl
	testb	%cl, %cl
	jne	.LBB22_56
# BB#55:
	testb	$1, %al
	je	.LBB22_57
.LBB22_56:
	movl	$.L.str.38, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
.LBB22_57:
	testl	%r15d, %r15d
	je	.LBB22_82
# BB#58:
	movq	g_source_annot_list(%rip), %rbx
	movl	$.L.str.39, %edi
	movl	$9, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	testq	%rbx, %rbx
	je	.LBB22_81
# BB#59:                                # %.lr.ph.preheader
	xorl	%r15d, %r15d
.LBB22_60:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_61 Depth 2
                                        #     Child Loop BB22_70 Depth 2
	testq	%r15, %r15
	je	.LBB22_70
	.p2align	4, 0x90
.LBB22_61:                              # %.lr.ph.split
                                        #   Parent Loop BB22_60 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	52(%rbx), %eax
	cmpl	g_page_object_num(%rip), %eax
	jne	.LBB22_79
# BB#62:                                #   in Loop: Header=BB22_61 Depth=2
	movl	$32, %edi
	movq	%r14, %rsi
	callq	fputc
	movl	48(%rbx), %edx
	movl	$.L.str.169, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	cmpl	$0, 64(%rbx)
	movq	(%rbx), %rbp
	je	.LBB22_80
# BB#63:                                #   in Loop: Header=BB22_61 Depth=2
	cmpq	%rbx, g_source_annot_list(%rip)
	jne	.LBB22_65
# BB#64:                                #   in Loop: Header=BB22_61 Depth=2
	movq	%rbp, g_source_annot_list(%rip)
.LBB22_65:                              #   in Loop: Header=BB22_61 Depth=2
	movq	%rbp, (%r15)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_67
# BB#66:                                #   in Loop: Header=BB22_61 Depth=2
	callq	free
.LBB22_67:                              #   in Loop: Header=BB22_61 Depth=2
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_69
# BB#68:                                #   in Loop: Header=BB22_61 Depth=2
	callq	free
.LBB22_69:                              # %PDFSourceAnnot_Dispose.exit
                                        #   in Loop: Header=BB22_61 Depth=2
	movq	%rbx, %rdi
	callq	free
	testq	%rbp, %rbp
	movq	%rbp, %rbx
	jne	.LBB22_61
	jmp	.LBB22_81
	.p2align	4, 0x90
.LBB22_70:                              # %.lr.ph.split.us
                                        #   Parent Loop BB22_60 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	52(%rbx), %eax
	cmpl	g_page_object_num(%rip), %eax
	jne	.LBB22_79
# BB#71:                                #   in Loop: Header=BB22_70 Depth=2
	movl	$32, %edi
	movq	%r14, %rsi
	callq	fputc
	movl	48(%rbx), %edx
	movl	$.L.str.169, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	cmpl	$0, 64(%rbx)
	movq	(%rbx), %rbp
	je	.LBB22_80
# BB#72:                                #   in Loop: Header=BB22_70 Depth=2
	cmpq	%rbx, g_source_annot_list(%rip)
	jne	.LBB22_74
# BB#73:                                #   in Loop: Header=BB22_70 Depth=2
	movq	%rbp, g_source_annot_list(%rip)
.LBB22_74:                              #   in Loop: Header=BB22_70 Depth=2
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_76
# BB#75:                                #   in Loop: Header=BB22_70 Depth=2
	callq	free
.LBB22_76:                              #   in Loop: Header=BB22_70 Depth=2
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_78
# BB#77:                                #   in Loop: Header=BB22_70 Depth=2
	callq	free
.LBB22_78:                              # %PDFSourceAnnot_Dispose.exit.us
                                        #   in Loop: Header=BB22_70 Depth=2
	movq	%rbx, %rdi
	callq	free
	testq	%rbp, %rbp
	movq	%rbp, %rbx
	jne	.LBB22_70
	jmp	.LBB22_81
	.p2align	4, 0x90
.LBB22_79:                              # %.us-lcssa.us
                                        #   in Loop: Header=BB22_60 Depth=1
	movq	(%rbx), %rbp
.LBB22_80:                              # %.outer.backedge
                                        #   in Loop: Header=BB22_60 Depth=1
	testq	%rbp, %rbp
	movq	%rbx, %r15
	movq	%rbp, %rbx
	jne	.LBB22_60
.LBB22_81:                              # %.outer._crit_edge
	movl	$.L.str.37, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
.LBB22_82:
	movl	$.L.str.41, %edi
	movl	$10, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.Lfunc_end22:
	.size	PDFPage_Cleanup, .Lfunc_end22-PDFPage_Cleanup
	.cfi_endproc

	.p2align	4, 0x90
	.type	PDFObject_WriteObj,@function
PDFObject_WriteObj:                     # @PDFObject_WriteObj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi140:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi141:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi142:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi143:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi144:
	.cfi_def_cfa_offset 48
.Lcfi145:
	.cfi_offset %rbx, -48
.Lcfi146:
	.cfi_offset %r12, -40
.Lcfi147:
	.cfi_offset %r14, -32
.Lcfi148:
	.cfi_offset %r15, -24
.Lcfi149:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r14
	leal	-1(%r15), %r12d
	movl	%r12d, %ebp
	shrl	$8, %ebp
	movq	g_obj_offset_list(%rip), %rbx
	movq	no_fpos(%rip), %r8
	testl	%r15d, %r15d
	je	.LBB23_2
# BB#1:
	cmpl	%r15d, g_next_objnum(%rip)
	ja	.LBB23_3
.LBB23_2:
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
.LBB23_3:                               # %Assert.exit.i.preheader
	movzbl	%r12b, %r12d
	negl	%ebp
	testq	%rbx, %rbx
	jne	.LBB23_6
	jmp	.LBB23_5
	.p2align	4, 0x90
.LBB23_7:                               #   in Loop: Header=BB23_6 Depth=1
	movq	(%rbx), %rbx
	incl	%ebp
	testq	%rbx, %rbx
	jne	.LBB23_6
.LBB23_5:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
.LBB23_6:                               # %Assert.exit17.i
                                        # =>This Inner Loop Header: Depth=1
	testl	%ebp, %ebp
	jne	.LBB23_7
# BB#8:                                 # %PDFObject_FindOffsetBlock.exit
	cmpl	$0, 8(%rbx,%r12,4)
	je	.LBB23_10
# BB#9:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
.LBB23_10:                              # %Assert.exit
	movq	%r14, %rdi
	callq	ftell
	movl	%eax, 8(%rbx,%r12,4)
	movl	$.L.str.135, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%r15d, %edx
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fprintf                 # TAILCALL
.Lfunc_end23:
	.size	PDFObject_WriteObj, .Lfunc_end23-PDFObject_WriteObj
	.cfi_endproc

	.p2align	4, 0x90
	.type	PDFSourceAnnot_Write,@function
PDFSourceAnnot_Write:                   # @PDFSourceAnnot_Write
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi150:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi151:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi152:
	.cfi_def_cfa_offset 32
.Lcfi153:
	.cfi_offset %rbx, -32
.Lcfi154:
	.cfi_offset %r14, -24
.Lcfi155:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	jne	.LBB24_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
.LBB24_2:                               # %Assert.exit
	movq	8(%rbx), %r15
	testq	%r15, %r15
	jne	.LBB24_4
# BB#3:                                 # %Assert.exit
	movl	60(%rbx), %eax
	testl	%eax, %eax
	je	.LBB24_24
.LBB24_4:
	movl	48(%rbx), %esi
	movq	%r14, %rdi
	callq	PDFObject_WriteObj
	movl	32(%rbx), %edx
	movl	36(%rbx), %ecx
	movl	40(%rbx), %r8d
	movl	44(%rbx), %r9d
	movl	$.L.str.156, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	60(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB24_22
# BB#5:
	cmpl	$1, %eax
	je	.LBB24_21
# BB#6:
	testl	%eax, %eax
	jne	.LBB24_23
# BB#7:
	movl	$.L.str.157, %edi
	movl	$8, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movq	8(%rbx), %rax
	movl	16(%rax), %edx
	movl	$.L.str.169, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	56(%rbx), %eax
	cmpq	$7, %rax
	ja	.LBB24_19
# BB#8:
	jmpq	*.LJTI24_0(,%rax,8)
.LBB24_9:
	movl	$.L.str.158, %edi
	movl	$20, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
.LBB24_10:
	movl	$.L.str.159, %edi
	movl	$5, %esi
	jmp	.LBB24_11
.LBB24_22:
	movq	16(%rbx), %rdx
	movl	$.L.str.168, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	jmp	.LBB24_23
.LBB24_21:
	movq	16(%rbx), %rdx
	movq	24(%rbx), %rcx
	movl	$.L.str.167, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	jmp	.LBB24_23
.LBB24_12:
	movl	32(%r15), %edx
	movl	$.L.str.160, %esi
	jmp	.LBB24_13
.LBB24_14:
	movl	20(%r15), %edx
	movl	$.L.str.161, %esi
	jmp	.LBB24_13
.LBB24_15:
	movl	20(%r15), %edx
	movl	24(%r15), %ecx
	movl	28(%r15), %r8d
	movl	32(%r15), %r9d
	movl	$.L.str.162, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	jmp	.LBB24_20
.LBB24_16:
	movl	$.L.str.163, %edi
	movl	$6, %esi
.LBB24_11:
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	jmp	.LBB24_20
.LBB24_17:
	movl	32(%r15), %edx
	movl	$.L.str.164, %esi
	jmp	.LBB24_13
.LBB24_18:
	movl	20(%r15), %edx
	movl	$.L.str.165, %esi
.LBB24_13:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
.LBB24_20:
	movl	$.L.str.37, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
.LBB24_23:
	movl	$.L.str.41, %edi
	movl	$10, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$1, 64(%rbx)
.LBB24_24:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB24_19:
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$18, %esi
	movl	$.L.str.166, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB24_20
.Lfunc_end24:
	.size	PDFSourceAnnot_Write, .Lfunc_end24-PDFSourceAnnot_Write
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI24_0:
	.quad	.LBB24_9
	.quad	.LBB24_10
	.quad	.LBB24_12
	.quad	.LBB24_14
	.quad	.LBB24_15
	.quad	.LBB24_16
	.quad	.LBB24_17
	.quad	.LBB24_18

	.text
	.globl	PDFFile_Init
	.p2align	4, 0x90
	.type	PDFFile_Init,@function
PDFFile_Init:                           # @PDFFile_Init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi156:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi157:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi158:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi159:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi160:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi161:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi162:
	.cfi_def_cfa_offset 64
.Lcfi163:
	.cfi_offset %rbx, -56
.Lcfi164:
	.cfi_offset %r12, -48
.Lcfi165:
	.cfi_offset %r13, -40
.Lcfi166:
	.cfi_offset %r14, -32
.Lcfi167:
	.cfi_offset %r15, -24
.Lcfi168:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movl	%r8d, %r15d
	movl	%ecx, %r12d
	movl	%edx, %r13d
	movl	%esi, %ebx
	movq	%rdi, %rbp
	movl	$.L.str.42, %edi
	movl	$9, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	movl	$.L.str.43, %edi
	movl	$6, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	movl	$1, g_next_objnum(%rip)
	movq	$0, g_obj_offset_list(%rip)
	movq	$0, g_cur_obj_offset_block(%rip)
	movq	$0, g_font_list(%rip)
	movq	$0, g_font_encoding_list(%rip)
	movl	$0, g_page_count(%rip)
	movq	$0, g_page_block_list(%rip)
	movq	$0, g_cur_page_block(%rip)
	callq	PDFObject_New
	movl	%eax, g_pages_root(%rip)
	movl	%ebx, g_doc_h_bound(%rip)
	movl	%r13d, g_doc_v_bound(%rip)
	movq	$0, g_doc_author(%rip)
	movq	$0, g_doc_title(%rip)
	movq	$0, g_doc_subject(%rip)
	movq	$0, g_doc_keywords(%rip)
	movq	$0, g_target_annot_list(%rip)
	movl	$0, g_has_exported_targets(%rip)
	movq	$0, g_source_annot_list(%rip)
	movl	%r12d, g_units(%rip)
	movl	%r15d, g_units+4(%rip)
	movl	%r14d, g_units+8(%rip)
	movl	64(%rsp), %eax
	movl	%eax, g_units+12(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	PDFFile_Init, .Lfunc_end25-PDFFile_Init
	.cfi_endproc

	.globl	PDFFile_Cleanup
	.p2align	4, 0x90
	.type	PDFFile_Cleanup,@function
PDFFile_Cleanup:                        # @PDFFile_Cleanup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi169:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi170:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi171:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi172:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi173:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi174:
	.cfi_def_cfa_offset 56
	subq	$536, %rsp              # imm = 0x218
.Lcfi175:
	.cfi_def_cfa_offset 592
.Lcfi176:
	.cfi_offset %rbx, -56
.Lcfi177:
	.cfi_offset %r12, -48
.Lcfi178:
	.cfi_offset %r13, -40
.Lcfi179:
	.cfi_offset %r14, -32
.Lcfi180:
	.cfi_offset %r15, -24
.Lcfi181:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	g_source_annot_list(%rip), %r15
	testq	%r15, %r15
	jne	.LBB26_2
	jmp	.LBB26_10
	.p2align	4, 0x90
.LBB26_8:                               # %PDFTargetAnnot_Find.exit
                                        #   in Loop: Header=BB26_2 Depth=1
	movq	%rbp, 8(%r15)
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	PDFSourceAnnot_Write
.LBB26_9:                               # %PDFTargetAnnot_Find.exit.thread
                                        #   in Loop: Header=BB26_2 Depth=1
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.LBB26_10
.LBB26_2:                               # %.lr.ph127
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_6 Depth 2
	cmpq	$0, 8(%r15)
	je	.LBB26_4
# BB#3:                                 #   in Loop: Header=BB26_2 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
.LBB26_4:                               # %Assert.exit
                                        #   in Loop: Header=BB26_2 Depth=1
	movq	g_target_annot_list(%rip), %rbp
	testq	%rbp, %rbp
	je	.LBB26_9
# BB#5:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB26_2 Depth=1
	movq	16(%r15), %rbx
	.p2align	4, 0x90
.LBB26_6:                               # %.lr.ph.i
                                        #   Parent Loop BB26_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rsi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB26_8
# BB#7:                                 #   in Loop: Header=BB26_6 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB26_6
	jmp	.LBB26_9
.LBB26_10:                              # %._crit_edge128
	movq	g_page_block_list(%rip), %r15
	movl	g_pages_root(%rip), %esi
	movq	%r14, %rdi
	callq	PDFObject_WriteObj
	movl	$.L.str.45, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$.L.str.171, %edi
	movl	$13, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$.L.str.172, %edi
	movl	$8, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	cmpl	$0, g_page_count(%rip)
	je	.LBB26_11
# BB#12:                                # %.lr.ph.i107.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB26_13:                              # %.lr.ph.i107
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %ebx
	andl	$63, %ebx
	movl	8(%r15,%rbx,4), %edx
	movl	$.L.str.169, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	cmpl	$63, %ebx
	jne	.LBB26_15
# BB#14:                                #   in Loop: Header=BB26_13 Depth=1
	movq	(%r15), %r15
.LBB26_15:                              #   in Loop: Header=BB26_13 Depth=1
	movl	$32, %edi
	movq	%r14, %rsi
	callq	fputc
	incl	%ebp
	movl	g_page_count(%rip), %edx
	cmpl	%edx, %ebp
	jb	.LBB26_13
	jmp	.LBB26_16
.LBB26_11:
	xorl	%edx, %edx
.LBB26_16:                              # %PDFFile_WritePagesObject.exit
	movl	$.L.str.173, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	g_doc_h_bound(%rip), %edx
	movl	g_doc_v_bound(%rip), %ecx
	movl	$.L.str.174, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	$.L.str.41, %edi
	movl	$10, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	cmpl	$0, g_has_exported_targets(%rip)
	je	.LBB26_17
# BB#18:
	movq	g_target_annot_list(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB26_20
# BB#19:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
.LBB26_20:                              # %Assert.exit108
	callq	PDFObject_New
	movl	%eax, %r15d
	movq	%r14, %rdi
	movl	%r15d, %esi
	callq	PDFObject_WriteObj
	movl	$.L.str.45, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	testq	%rbx, %rbx
	jne	.LBB26_22
	jmp	.LBB26_25
	.p2align	4, 0x90
.LBB26_24:                              #   in Loop: Header=BB26_22 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB26_25
.LBB26_22:                              # %.lr.ph121
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, 36(%rbx)
	je	.LBB26_24
# BB#23:                                #   in Loop: Header=BB26_22 Depth=1
	movq	8(%rbx), %rdx
	movl	$.L.str.46, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	16(%rbx), %edx
	movl	$.L.str.169, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	$.L.str.47, %edi
	movl	$23, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	jmp	.LBB26_24
.LBB26_25:                              # %._crit_edge122
	movl	$.L.str.41, %edi
	movl	$10, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	jmp	.LBB26_26
.LBB26_17:
                                        # implicit-def: %R15D
.LBB26_26:
	callq	PDFObject_New
	movl	%eax, %r12d
	movq	%r14, %rdi
	movl	%r12d, %esi
	callq	PDFObject_WriteObj
	movl	$.L.str.45, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$.L.str.49, %edi
	movl	$15, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$.L.str.50, %edi
	movl	$7, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	g_pages_root(%rip), %edx
	movl	$.L.str.169, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	$10, %edi
	movq	%r14, %rsi
	callq	fputc
	cmpl	$0, g_has_exported_targets(%rip)
	je	.LBB26_28
# BB#27:
	movl	$.L.str.51, %edi
	movl	$7, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$.L.str.169, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%r15d, %edx
	callq	fprintf
	movl	$10, %edi
	movq	%r14, %rsi
	callq	fputc
.LBB26_28:
	movl	$.L.str.41, %edi
	movl	$10, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	callq	PDFObject_New
	movl	%eax, %r15d
	movq	%r14, %rdi
	movl	%r15d, %esi
	callq	PDFObject_WriteObj
	movl	$.L.str.45, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$.L.str.53, %esi
	movl	$.L.str.54, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	$.L.str.55, %esi
	movl	$.L.str.54, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	xorl	%edi, %edi
	callq	time
	movq	%rax, 16(%rsp)
	leaq	16(%rsp), %rdi
	callq	localtime
	movl	$.L.str.56, %edi
	movl	$31, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movq	g_doc_author(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB26_30
# BB#29:
	movl	$.L.str.57, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
.LBB26_30:
	movq	g_doc_title(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB26_32
# BB#31:
	movl	$.L.str.58, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
.LBB26_32:
	movq	g_doc_subject(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB26_34
# BB#33:
	movl	$.L.str.59, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
.LBB26_34:
	movq	g_doc_keywords(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB26_36
# BB#35:
	movl	$.L.str.60, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
.LBB26_36:
	movl	$.L.str.41, %edi
	movl	$10, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movq	g_obj_offset_list(%rip), %rbx
	movq	%r14, %rdi
	callq	ftell
	movq	%rax, %rbp
	movl	$.L.str.176, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	g_next_objnum(%rip), %edx
	movl	$.L.str.177, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	$.L.str.178, %edi
	movl	$20, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	g_next_objnum(%rip), %eax
	testq	%rbx, %rbx
	movl	%r12d, 4(%rsp)          # 4-byte Spill
	movl	%r15d, (%rsp)           # 4-byte Spill
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	jne	.LBB26_39
# BB#37:
	cmpl	$1, %eax
	je	.LBB26_39
# BB#38:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
	movl	g_next_objnum(%rip), %eax
.LBB26_39:                              # %Assert.exit.preheader.i
	cmpl	$2, %eax
	jb	.LBB26_46
# BB#40:                                # %.lr.ph.i109
	movl	$1, %ebp
	leaq	16(%rsp), %r13
	.p2align	4, 0x90
.LBB26_41:                              # =>This Inner Loop Header: Depth=1
	leal	-1(%rbp), %r15d
	movl	%r15d, %eax
	sarl	$31, %eax
	shrl	$24, %eax
	leal	-1(%rbp,%rax), %eax
	andl	$-256, %eax
	subl	%eax, %r15d
	movslq	%r15d, %r12
	movl	8(%rbx,%r12,4), %edx
	movl	$.L.str.179, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	cmpl	$0, 8(%rbx,%r12,4)
	jne	.LBB26_43
# BB#42:                                #   in Loop: Header=BB26_41 Depth=1
	movups	.L.str.180+29(%rip), %xmm0
	movups	%xmm0, 45(%rsp)
	movups	.L.str.180+16(%rip), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	.L.str.180(%rip), %xmm0
	movaps	%xmm0, 16(%rsp)
	movq	%r13, %rdi
	callq	strlen
	leaq	16(%rsp,%rax), %rdi
	movl	$.L.str.70, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	sprintf
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$39, %esi
	movl	$.L.str.68, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r13, %r9
	callq	Error
.LBB26_43:                              #   in Loop: Header=BB26_41 Depth=1
	cmpl	$255, %r15d
	jne	.LBB26_45
# BB#44:                                #   in Loop: Header=BB26_41 Depth=1
	movq	(%rbx), %rbx
.LBB26_45:                              # %Assert.exit.i
                                        #   in Loop: Header=BB26_41 Depth=1
	incl	%ebp
	cmpl	g_next_objnum(%rip), %ebp
	jb	.LBB26_41
.LBB26_46:                              # %PDFFile_WriteXREF.exit
	movl	$.L.str.61, %edi
	movl	$11, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	g_next_objnum(%rip), %edx
	movl	$.L.str.62, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	$.L.str.63, %edi
	movl	$6, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$.L.str.169, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	4(%rsp), %edx           # 4-byte Reload
	callq	fprintf
	movl	$.L.str.64, %edi
	movl	$7, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$.L.str.169, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	(%rsp), %edx            # 4-byte Reload
	callq	fprintf
	movl	$.L.str.65, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	8(%rsp), %rdx           # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	movl	$.L.str.66, %edi
	movl	$6, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	jmp	.LBB26_47
	.p2align	4, 0x90
.LBB26_65:                              # %.lr.ph119
                                        #   in Loop: Header=BB26_47 Depth=1
	movq	(%rdi), %rax
	movq	%rax, g_obj_offset_list(%rip)
	callq	free
.LBB26_47:                              # %PDFFile_WriteXREF.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	g_obj_offset_list(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB26_65
	jmp	.LBB26_48
	.p2align	4, 0x90
.LBB26_66:                              # %.lr.ph118
                                        #   in Loop: Header=BB26_48 Depth=1
	movq	(%rbx), %rax
	movq	%rax, g_font_encoding_list(%rip)
	movq	16(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
.LBB26_48:                              # %.preheader113
                                        # =>This Inner Loop Header: Depth=1
	movq	g_font_encoding_list(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB26_66
	jmp	.LBB26_49
	.p2align	4, 0x90
.LBB26_50:                              # %.lr.ph117
                                        #   in Loop: Header=BB26_49 Depth=1
	movq	(%rbx), %rax
	movq	%rax, g_font_list(%rip)
	movq	8(%rbx), %rdi
	callq	free
	movq	16(%rbx), %rdi
	callq	free
	movq	24(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
.LBB26_49:                              # %.preheader112
                                        # =>This Inner Loop Header: Depth=1
	movq	g_font_list(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB26_50
	jmp	.LBB26_52
	.p2align	4, 0x90
.LBB26_51:                              # %.lr.ph116
                                        #   in Loop: Header=BB26_52 Depth=1
	movq	(%rdi), %rax
	movq	%rax, g_page_block_list(%rip)
	callq	free
.LBB26_52:                              # %.lr.ph116
                                        # =>This Inner Loop Header: Depth=1
	movq	g_page_block_list(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB26_51
# BB#53:                                # %thread-pre-split
	movq	g_source_annot_list(%rip), %rbp
	testq	%rbp, %rbp
	je	.LBB26_63
# BB#54:
	leaq	16(%rsp), %r14
	.p2align	4, 0x90
.LBB26_55:                              # =>This Inner Loop Header: Depth=1
	cmpq	$0, 8(%rbp)
	jne	.LBB26_57
# BB#56:                                #   in Loop: Header=BB26_55 Depth=1
	movups	.L.str.67+32(%rip), %xmm0
	movaps	%xmm0, 48(%rsp)
	movups	.L.str.67+16(%rip), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	.L.str.67(%rip), %xmm0
	movaps	%xmm0, 16(%rsp)
	movb	$0, 66(%rsp)
	movw	$8292, 64(%rsp)         # imm = 0x2064
	movq	16(%rbp), %rsi
	movq	%r14, %rdi
	callq	strcat
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$40, %esi
	movl	$.L.str.68, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r14, %r9
	callq	Error
.LBB26_57:                              #   in Loop: Header=BB26_55 Depth=1
	movq	(%rbp), %rbx
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB26_59
# BB#58:                                #   in Loop: Header=BB26_55 Depth=1
	callq	free
.LBB26_59:                              #   in Loop: Header=BB26_55 Depth=1
	movq	24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB26_61
# BB#60:                                #   in Loop: Header=BB26_55 Depth=1
	callq	free
.LBB26_61:                              # %PDFSourceAnnot_Dispose.exit
                                        #   in Loop: Header=BB26_55 Depth=1
	movq	%rbp, %rdi
	callq	free
	movq	%rbx, g_source_annot_list(%rip)
	testq	%rbx, %rbx
	movq	%rbx, %rbp
	jne	.LBB26_55
	jmp	.LBB26_63
	.p2align	4, 0x90
.LBB26_62:                              # %.lr.ph
                                        #   in Loop: Header=BB26_63 Depth=1
	movq	(%rbx), %rax
	movq	%rax, g_target_annot_list(%rip)
	movq	8(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
.LBB26_63:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	g_target_annot_list(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB26_62
# BB#64:                                # %._crit_edge
	addq	$536, %rsp              # imm = 0x218
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	PDFFile_Cleanup, .Lfunc_end26-PDFFile_Cleanup
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI27_0:
	.quad	4614256656552045848     # double 3.1415926535897931
.LCPI27_1:
	.quad	4640537203540230144     # double 180
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI27_2:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.p2align	4, 0x90
	.type	PDFPage_EvalExpr,@function
PDFPage_EvalExpr:                       # @PDFPage_EvalExpr
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi182:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi183:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi184:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi185:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi186:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi187:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi188:
	.cfi_def_cfa_offset 80
.Lcfi189:
	.cfi_offset %rbx, -56
.Lcfi190:
	.cfi_offset %r12, -48
.Lcfi191:
	.cfi_offset %r13, -40
.Lcfi192:
	.cfi_offset %r14, -32
.Lcfi193:
	.cfi_offset %r15, -24
.Lcfi194:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	__ctype_b_loc
	movq	%rax, %r15
	movq	(%r15), %r12
	decq	%rbx
	.p2align	4, 0x90
.LBB27_1:                               # =>This Inner Loop Header: Depth=1
	movsbq	1(%rbx), %rax
	incq	%rbx
	testb	$32, 1(%r12,%rax,2)
	jne	.LBB27_1
	jmp	.LBB27_3
	.p2align	4, 0x90
.LBB27_2:                               # %._crit_edge
                                        #   in Loop: Header=BB27_3 Depth=1
	movb	1(%rbx), %al
	incq	%rbx
.LBB27_3:                               # %.preheader76.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$95, %al
	je	.LBB27_2
# BB#4:                                 # %.preheader75.preheader
	leaq	1(%rbx), %rdi
	cmpb	$43, %al
	jne	.LBB27_7
	.p2align	4, 0x90
.LBB27_5:                               # %.preheader75..preheader75_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movb	(%rdi), %al
	incq	%rdi
	cmpb	$43, %al
	je	.LBB27_5
# BB#6:                                 # %.preheader75._crit_edge.loopexit
	leaq	-1(%rdi), %rbx
.LBB27_7:                               # %.preheader75._crit_edge
	cmpb	$46, %al
	je	.LBB27_12
# BB#8:                                 # %.preheader75._crit_edge
	movsbq	%al, %rcx
	movzwl	(%r12,%rcx,2), %ecx
	andl	$2048, %ecx             # imm = 0x800
	testw	%cx, %cx
	jne	.LBB27_12
# BB#9:
	cmpb	$45, %al
	jne	.LBB27_21
# BB#10:
	leaq	20(%rsp), %rsi
	callq	PDFPage_EvalExpr
	movq	%rax, %rbx
	movss	20(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	xorps	.LCPI27_2(%rip), %xmm0
.LBB27_11:                              # %PDFPage_GetFloat.exit
	movss	%xmm0, (%r14)
	jmp	.LBB27_42
.LBB27_12:
	movl	$.L.str.85, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB27_40
# BB#13:                                # %.preheader10.i
	movq	(%r15), %rax
	.p2align	4, 0x90
.LBB27_14:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %ecx
	incq	%rbx
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB27_14
# BB#15:
	cmpb	$43, %cl
	je	.LBB27_18
# BB#16:
	cmpb	$45, %cl
	je	.LBB27_18
# BB#17:                                # %..preheader.i.preheader_crit_edge
	decq	%rbx
.LBB27_18:                              # %.preheader.i.preheader
	decq	%rbx
	.p2align	4, 0x90
.LBB27_19:                              # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	1(%rbx), %edx
	incq	%rbx
	movzwl	(%rax,%rdx,2), %ecx
	andl	$2048, %ecx             # imm = 0x800
	cmpq	$46, %rdx
	je	.LBB27_19
# BB#20:                                # %.preheader.i
                                        #   in Loop: Header=BB27_19 Depth=1
	testw	%cx, %cx
	jne	.LBB27_19
	jmp	.LBB27_42
.LBB27_21:                              # %.lr.ph.i64.preheader
	movq	g_arithmetic_keywords(%rip), %r15
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB27_43
# BB#22:                                # %.lr.ph.i64.1104
	movq	g_arithmetic_keywords+8(%rip), %r15
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB27_44
# BB#23:                                # %.lr.ph.i64.2105
	movq	g_arithmetic_keywords+16(%rip), %r15
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB27_45
# BB#24:                                # %.lr.ph.i64.3106
	movq	g_arithmetic_keywords+24(%rip), %r15
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB27_46
# BB#25:                                # %.lr.ph.i64.4107
	movq	g_arithmetic_keywords+32(%rip), %r15
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB27_47
# BB#26:                                # %.lr.ph.i64.5108
	movq	g_arithmetic_keywords+40(%rip), %r15
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB27_48
# BB#27:                                # %.lr.ph.i64.6109
	movq	g_arithmetic_keywords+48(%rip), %r15
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB27_49
# BB#28:                                # %.lr.ph.i69.preheader110
	movq	g_graphic_keywords(%rip), %r15
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB27_80
# BB#29:                                # %.lr.ph.i69.1100
	movq	g_graphic_keywords+8(%rip), %r15
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB27_81
# BB#30:                                # %.lr.ph.i69.2101
	movq	g_graphic_keywords+16(%rip), %r15
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB27_82
# BB#31:                                # %.lr.ph.i69.3102
	movq	g_graphic_keywords+24(%rip), %r15
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB27_83
# BB#32:                                # %.lr.ph.i.preheader103
	movq	g_unit_keywords(%rip), %r15
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB27_85
# BB#33:                                # %.lr.ph.i.193
	movq	g_unit_keywords+8(%rip), %r15
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB27_86
# BB#34:                                # %.lr.ph.i.294
	movq	g_unit_keywords+16(%rip), %r15
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB27_87
# BB#35:                                # %.lr.ph.i.395
	movq	g_unit_keywords+24(%rip), %r15
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB27_88
# BB#36:                                # %.lr.ph.i.496
	movq	g_unit_keywords+32(%rip), %r15
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB27_89
# BB#37:                                # %.lr.ph.i.597
	movq	g_unit_keywords+40(%rip), %r15
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB27_90
# BB#38:                                # %.lr.ph.i.698
	movq	g_unit_keywords+48(%rip), %r15
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB27_91
# BB#39:                                # %PDFKeyword_Find.exit99
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$33, %esi
	movl	$.L.str.84, %edx
	jmp	.LBB27_41
.LBB27_40:
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$21, %esi
	movl	$.L.str.86, %edx
.LBB27_41:                              # %PDFPage_GetFloat.exit
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB27_42
.LBB27_43:
	xorl	%r13d, %r13d
	jmp	.LBB27_50
.LBB27_44:
	movl	$1, %r13d
	jmp	.LBB27_50
.LBB27_45:
	movl	$2, %r13d
	jmp	.LBB27_50
.LBB27_46:
	movl	$3, %r13d
	jmp	.LBB27_50
.LBB27_47:
	movl	$4, %r13d
	jmp	.LBB27_50
.LBB27_48:
	movl	$5, %r13d
	jmp	.LBB27_50
.LBB27_49:
	movl	$6, %r13d
.LBB27_50:
	movq	g_arithmetic_keywords(,%r13,8), %rdi
	callq	strlen
	addq	%rax, %rbx
	.p2align	4, 0x90
.LBB27_51:                              # =>This Inner Loop Header: Depth=1
	movsbq	(%rbx), %rax
	incq	%rbx
	testb	$32, 1(%r12,%rax,2)
	jne	.LBB27_51
# BB#52:
	cmpb	$40, %al
	je	.LBB27_54
# BB#53:
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$30, %esi
	movl	$.L.str.81, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB27_54:
	leaq	12(%rsp), %rsi
	movq	%rbx, %rdi
	callq	PDFPage_EvalExpr
	movq	%rax, %rbx
	movl	%r13d, %eax
	shlb	$5, %al
	sarb	$5, %al
	movl	$1, %ebp
	js	.LBB27_61
.LBB27_55:                              # %Assert.exit
	cmpb	$44, (%rbx)
	je	.LBB27_57
.LBB27_56:
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$31, %esi
	movl	$.L.str.82, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB27_57:                              # %.preheader.preheader
	leaq	16(%rsp), %r15
	.p2align	4, 0x90
.LBB27_58:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	incq	%rbx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	PDFPage_EvalExpr
	cmpl	$1, %ebp
	je	.LBB27_60
# BB#59:                                #   in Loop: Header=BB27_58 Depth=1
	leaq	1(%rax), %rbx
	cmpb	$44, (%rax)
	cmovneq	%rax, %rbx
	decl	%ebp
	jne	.LBB27_58
	jmp	.LBB27_64
.LBB27_60:
	movq	%rax, %rbx
	cmpb	$41, (%rbx)
	jne	.LBB27_65
	jmp	.LBB27_66
.LBB27_61:
	movl	%r13d, %eax
	andb	$7, %al
	cmpb	$6, %al
	jne	.LBB27_64
# BB#62:
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	callq	floorf
	cvttss2si	%xmm0, %ebp
	testl	%ebp, %ebp
	jne	.LBB27_55
# BB#63:
	movq	no_fpos(%rip), %r8
	xorl	%ebp, %ebp
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
	cmpb	$44, (%rbx)
	jne	.LBB27_56
	jmp	.LBB27_57
.LBB27_64:                              # %.thread
	cmpb	$41, (%rbx)
	je	.LBB27_66
.LBB27_65:
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$32, %esi
	movl	$.L.str.83, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB27_66:
	movl	%r13d, %eax
	andb	$7, %al
	cmpb	$7, %al
	je	.LBB27_79
# BB#67:
	andl	$7, %r13d
	jmpq	*.LJTI27_0(,%r13,8)
.LBB27_68:
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	16(%rsp), %xmm0
	jmp	.LBB27_77
.LBB27_69:
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	16(%rsp), %xmm0
	jmp	.LBB27_77
.LBB27_70:
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	16(%rsp), %xmm0
	jmp	.LBB27_77
.LBB27_71:
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB27_73
	jp	.LBB27_73
# BB#72:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
.LBB27_73:                              # %Assert.exit72
	movss	12(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movss	%xmm1, (%r14)
	incq	%rbx
	jmp	.LBB27_42
.LBB27_74:
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	mulsd	.LCPI27_0(%rip), %xmm0
	divsd	.LCPI27_1(%rip), %xmm0
	callq	sin
	jmp	.LBB27_76
.LBB27_75:
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	mulsd	.LCPI27_0(%rip), %xmm0
	divsd	.LCPI27_1(%rip), %xmm0
	callq	cos
.LBB27_76:
	cvtsd2ss	%xmm0, %xmm0
.LBB27_77:                              # %PDFPage_GetFloat.exit
	movss	%xmm0, (%r14)
	incq	%rbx
	jmp	.LBB27_42
.LBB27_78:
	movl	16(%rsp), %eax
	movl	%eax, (%r14)
.LBB27_79:
	incq	%rbx
.LBB27_42:                              # %PDFPage_GetFloat.exit
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB27_80:
	xorl	%ebp, %ebp
	jmp	.LBB27_84
.LBB27_81:
	movl	$1, %ebp
	jmp	.LBB27_84
.LBB27_82:
	movl	$2, %ebp
	jmp	.LBB27_84
.LBB27_83:
	movl	$3, %ebp
.LBB27_84:
	movq	g_graphic_keywords(,%rbp,8), %rdi
	callq	strlen
	addq	%rax, %rbx
	cvtsi2ssl	g_graphics_vars(,%rbp,4), %xmm0
	jmp	.LBB27_11
.LBB27_85:
	xorl	%ebp, %ebp
	jmp	.LBB27_92
.LBB27_86:
	movl	$1, %ebp
	jmp	.LBB27_92
.LBB27_87:
	movl	$2, %ebp
	jmp	.LBB27_92
.LBB27_88:
	movl	$3, %ebp
	jmp	.LBB27_92
.LBB27_89:
	movl	$4, %ebp
	jmp	.LBB27_92
.LBB27_90:
	movl	$5, %ebp
	jmp	.LBB27_92
.LBB27_91:
	movl	$6, %ebp
.LBB27_92:
	movq	g_unit_keywords(,%rbp,8), %rdi
	callq	strlen
	addq	%rax, %rbx
	cvtsi2ssl	g_units(,%rbp,4), %xmm0
	jmp	.LBB27_11
.Lfunc_end27:
	.size	PDFPage_EvalExpr, .Lfunc_end27-PDFPage_EvalExpr
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI27_0:
	.quad	.LBB27_68
	.quad	.LBB27_69
	.quad	.LBB27_70
	.quad	.LBB27_71
	.quad	.LBB27_74
	.quad	.LBB27_75
	.quad	.LBB27_78

	.text
	.p2align	4, 0x90
	.type	PDFSourceAnnot_New,@function
PDFSourceAnnot_New:                     # @PDFSourceAnnot_New
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi195:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi196:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi197:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi198:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi199:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi200:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi201:
	.cfi_def_cfa_offset 64
.Lcfi202:
	.cfi_offset %rbx, -56
.Lcfi203:
	.cfi_offset %r12, -48
.Lcfi204:
	.cfi_offset %r13, -40
.Lcfi205:
	.cfi_offset %r14, -32
.Lcfi206:
	.cfi_offset %r15, -24
.Lcfi207:
	.cfi_offset %rbp, -16
	movl	%r9d, %r15d
	movl	%r8d, %r12d
	movl	%ecx, %ebp
	movl	%edx, %r14d
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rsi, (%rsp)            # 8-byte Spill
	movl	%edi, %r13d
	movl	$72, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB28_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$19, %esi
	movl	$.L.str.106, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB28_2:
	movl	%r14d, 32(%rbx)
	movl	%ebp, 36(%rbx)
	movl	%r12d, 40(%rbx)
	movl	%r15d, 44(%rbx)
	callq	PDFObject_New
	movl	%eax, 48(%rbx)
	movl	g_page_object_num(%rip), %eax
	movl	%eax, 52(%rbx)
	movl	%r13d, 60(%rbx)
	movl	64(%rsp), %eax
	cmpl	$8, %eax
	movl	%eax, %ebp
	jb	.LBB28_4
# BB#3:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
.LBB28_4:                               # %Assert.exit
	movl	%ebp, 56(%rbx)
	movq	$0, 24(%rbx)
	movl	$0, 64(%rbx)
	testl	%r13d, %r13d
	jne	.LBB28_9
# BB#5:
	movq	g_target_annot_list(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB28_7
	jmp	.LBB28_9
	.p2align	4, 0x90
.LBB28_8:                               #   in Loop: Header=BB28_7 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB28_9
.LBB28_7:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rsi
	movl	$g_link, %edi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB28_8
# BB#13:
	movq	%rbp, 8(%rbx)
	movq	$0, 16(%rbx)
	jmp	.LBB28_12
.LBB28_9:                               # %.loopexit
	movq	$0, 8(%rbx)
	movq	(%rsp), %rbp            # 8-byte Reload
	leal	1(%rbp), %edi
	callq	malloc
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	jne	.LBB28_11
# BB#10:
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$20, %esi
	movl	$.L.str.106, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	16(%rbx), %rax
.LBB28_11:
	movl	%ebp, %ebp
	movl	$g_link, %esi
	movq	%rax, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	movq	16(%rbx), %rax
	movb	$0, (%rax,%rbp)
.LBB28_12:
	movq	g_source_annot_list(%rip), %rax
	movq	%rax, (%rbx)
	movq	%rbx, g_source_annot_list(%rip)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end28:
	.size	PDFSourceAnnot_New, .Lfunc_end28-PDFSourceAnnot_New
	.cfi_endproc

	.p2align	4, 0x90
	.type	PDFObject_New,@function
PDFObject_New:                          # @PDFObject_New
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi208:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi209:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi210:
	.cfi_def_cfa_offset 32
.Lcfi211:
	.cfi_offset %rbx, -32
.Lcfi212:
	.cfi_offset %r14, -24
.Lcfi213:
	.cfi_offset %rbp, -16
	movl	g_next_objnum(%rip), %ebp
	decl	%ebp
	movl	%ebp, %r14d
	andl	$255, %r14d
	je	.LBB29_3
# BB#1:
	movq	g_cur_obj_offset_block(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB29_12
# BB#2:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
.LBB29_3:
	movl	$1032, %edi             # imm = 0x408
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB29_5
# BB#4:
	movq	no_fpos(%rip), %r8
	movl	$48, %edi
	movl	$1, %esi
	movl	$.L.str.133, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB29_5:
	cmpl	$255, %ebp
	ja	.LBB29_8
# BB#6:
	cmpq	$0, g_obj_offset_list(%rip)
	je	.LBB29_10
# BB#7:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
.LBB29_10:
	movl	$g_obj_offset_list, %eax
	jmp	.LBB29_11
.LBB29_8:
	movq	g_cur_obj_offset_block(%rip), %rax
	testq	%rax, %rax
	jne	.LBB29_11
# BB#9:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r8, %r9
	callq	Error
	movq	g_cur_obj_offset_block(%rip), %rax
.LBB29_11:                              # %Assert.exit
	movq	%rbx, (%rax)
	movq	$0, (%rbx)
	movq	%rbx, g_cur_obj_offset_block(%rip)
.LBB29_12:                              # %Assert.exit15
	movl	%r14d, %eax
	movl	$0, 8(%rbx,%rax,4)
	movl	g_next_objnum(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, g_next_objnum(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end29:
	.size	PDFObject_New, .Lfunc_end29-PDFObject_New
	.cfi_endproc

	.type	g_valid_text_matrix,@object # @g_valid_text_matrix
	.local	g_valid_text_matrix
	.comm	g_valid_text_matrix,1,4
	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"<<\n/Type /Encoding\n/Differences [ 0\n"
	.size	.L.str.1, 37

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"PDFFile_BeginFontEncoding: run out of memory"
	.size	.L.str.2, 45

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"PDFFile_BeginFontEncoding: out of memory"
	.size	.L.str.3, 41

	.type	g_font_encoding_list,@object # @g_font_encoding_list
	.local	g_font_encoding_list
	.comm	g_font_encoding_list,8,8
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"]\n>>\nendobj\n"
	.size	.L.str.4, 13

	.type	g_graphics_vars,@object # @g_graphics_vars
	.local	g_graphics_vars
	.comm	g_graphics_vars,16,16
	.type	g_units,@object         # @g_units
	.local	g_units
	.comm	g_units,28,16
	.type	g_in_buffering_mode,@object # @g_in_buffering_mode
	.local	g_in_buffering_mode
	.comm	g_in_buffering_mode,1,4
	.type	g_buffer_pos,@object    # @g_buffer_pos
	.local	g_buffer_pos
	.comm	g_buffer_pos,4,4
	.type	g_buffer,@object        # @g_buffer
	.local	g_buffer
	.comm	g_buffer,1024,16
	.type	g_TJ_pending,@object    # @g_TJ_pending
	.local	g_TJ_pending
	.comm	g_TJ_pending,1,4
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	")]TJ\n"
	.size	.L.str.5, 6

	.type	g_ET_pending,@object    # @g_ET_pending
	.local	g_ET_pending
	.comm	g_ET_pending,1,4
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"ET\n"
	.size	.L.str.6, 4

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"PDFPage_Push: run out of memory"
	.size	.L.str.7, 32

	.type	g_page_h_origin,@object # @g_page_h_origin
	.local	g_page_h_origin
	.comm	g_page_h_origin,4,4
	.type	g_page_v_origin,@object # @g_page_v_origin
	.local	g_page_v_origin
	.comm	g_page_v_origin,4,4
	.type	g_qsave_stack,@object   # @g_qsave_stack
	.local	g_qsave_stack
	.comm	g_qsave_stack,8,8
	.type	g_qsave_marking_stack,@object # @g_qsave_marking_stack
	.local	g_qsave_marking_stack
	.comm	g_qsave_marking_stack,8,8
	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"q\n"
	.size	.L.str.8, 3

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"\nQ\n"
	.size	.L.str.9, 4

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"cannot find font entry for name %s"
	.size	.L.str.10, 35

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%s %u Tf\n"
	.size	.L.str.11, 10

	.type	g_page_uses_fonts,@object # @g_page_uses_fonts
	.local	g_page_uses_fonts
	.comm	g_page_uses_fonts,1,4
	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	")%d("
	.size	.L.str.12, 5

	.type	g_page_contents_obj_num,@object # @g_page_contents_obj_num
	.local	g_page_contents_obj_num
	.comm	g_page_contents_obj_num,4,4
	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"%.2f 0 0 %.2f 0 0 cm\n"
	.size	.L.str.13, 22

	.type	g_page_h_scale_factor,@object # @g_page_h_scale_factor
	.local	g_page_h_scale_factor
	.comm	g_page_h_scale_factor,4,4
	.type	g_page_v_scale_factor,@object # @g_page_v_scale_factor
	.local	g_page_v_scale_factor
	.comm	g_page_v_scale_factor,4,4
	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"%.2f %.2f %.2f %.2f 0 0 cm\n"
	.size	.L.str.14, 28

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"1 0 0 1 %.2f %.2f cm\n"
	.size	.L.str.15, 22

	.type	g_expr_depth,@object    # @g_expr_depth
	.local	g_expr_depth
	.comm	g_expr_depth,4,4
	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"%.2f"
	.size	.L.str.16, 5

	.type	g_link_depth,@object    # @g_link_depth
	.local	g_link_depth
	.comm	g_link_depth,4,4
	.type	g_graphic_keywords,@object # @g_graphic_keywords
	.data
	.p2align	4
g_graphic_keywords:
	.quad	.L.str.109
	.quad	.L.str.110
	.quad	.L.str.111
	.quad	.L.str.112
	.size	g_graphic_keywords, 32

	.type	.L.str.17,@object       # @.str.17
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.17:
	.asciz	"%d"
	.size	.L.str.17, 3

	.type	g_link_keywords,@object # @g_link_keywords
	.data
	.p2align	4
g_link_keywords:
	.quad	.L.str.113
	.quad	.L.str.114
	.quad	.L.str.115
	.quad	.L.str.116
	.quad	.L.str.117
	.size	g_link_keywords, 40

	.type	g_link_index,@object    # @g_link_index
	.local	g_link_index
	.comm	g_link_index,4,4
	.type	g_link_keyword,@object  # @g_link_keyword
	.local	g_link_keyword
	.comm	g_link_keyword,4,4
	.type	g_doc_info_keywords,@object # @g_doc_info_keywords
	.p2align	4
g_doc_info_keywords:
	.quad	.L.str.118
	.quad	.L.str.119
	.quad	.L.str.120
	.quad	.L.str.121
	.size	g_doc_info_keywords, 32

	.type	g_arithmetic_keywords,@object # @g_arithmetic_keywords
	.p2align	4
g_arithmetic_keywords:
	.quad	.L.str.126
	.quad	.L.str.127
	.quad	.L.str.128
	.quad	.L.str.129
	.quad	.L.str.130
	.quad	.L.str.131
	.quad	.L.str.132
	.size	g_arithmetic_keywords, 56

	.type	g_expr,@object          # @g_expr
	.local	g_expr
	.comm	g_expr,512,16
	.type	.L.str.18,@object       # @.str.18
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.18:
	.asciz	"PDFPage_WriteGraphic: '(' expected"
	.size	.L.str.18, 35

	.type	g_expr_index,@object    # @g_expr_index
	.local	g_expr_index
	.comm	g_expr_index,4,4
	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"PDFPage_WriteGraphic: '__' encountered while processing @Graphic"
	.size	.L.str.20, 65

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"q %d w %d %d m %d %d l s Q\n"
	.size	.L.str.21, 28

	.type	g_page_has_text,@object # @g_page_has_text
	.local	g_page_has_text
	.comm	g_page_has_text,1,4
	.type	g_page_length_obj_num,@object # @g_page_length_obj_num
	.local	g_page_length_obj_num
	.comm	g_page_length_obj_num,4,4
	.type	g_page_start_offset,@object # @g_page_start_offset
	.local	g_page_start_offset
	.comm	g_page_start_offset,4,4
	.type	g_page_line_width,@object # @g_page_line_width
	.local	g_page_line_width
	.comm	g_page_line_width,4,4
	.type	g_font_list,@object     # @g_font_list
	.local	g_font_list
	.comm	g_font_list,8,8
	.type	g_page_count,@object    # @g_page_count
	.local	g_page_count
	.comm	g_page_count,4,4
	.type	g_page_object_num,@object # @g_page_object_num
	.local	g_page_object_num
	.comm	g_page_object_num,4,4
	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"%u\nendobj\n"
	.size	.L.str.23, 11

	.type	g_source_annot_list,@object # @g_source_annot_list
	.local	g_source_annot_list
	.comm	g_source_annot_list,8,8
	.type	g_cur_page_block,@object # @g_cur_page_block
	.local	g_cur_page_block
	.comm	g_cur_page_block,8,8
	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"PDFPage_Cleanup: run out of memory"
	.size	.L.str.24, 35

	.type	g_page_block_list,@object # @g_page_block_list
	.local	g_page_block_list
	.comm	g_page_block_list,8,8
	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"<<\n/Type /Page\n"
	.size	.L.str.26, 16

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"/CropBox [ 0 0 %u %u ]\n"
	.size	.L.str.27, 24

	.type	g_doc_h_bound,@object   # @g_doc_h_bound
	.local	g_doc_h_bound
	.comm	g_doc_h_bound,4,4
	.type	g_doc_v_bound,@object   # @g_doc_v_bound
	.local	g_doc_v_bound
	.comm	g_doc_v_bound,4,4
	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"/Parent "
	.size	.L.str.28, 9

	.type	g_pages_root,@object    # @g_pages_root
	.local	g_pages_root
	.comm	g_pages_root,4,4
	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"/Contents "
	.size	.L.str.30, 11

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"/Resources\n<<\n"
	.size	.L.str.31, 15

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"/Font <<"
	.size	.L.str.32, 9

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	" %s "
	.size	.L.str.33, 5

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	" >>\n"
	.size	.L.str.34, 5

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"/ProcSet [ /PDF"
	.size	.L.str.35, 16

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	" /Text"
	.size	.L.str.36, 7

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	" ]\n"
	.size	.L.str.37, 4

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	">>\n"
	.size	.L.str.38, 4

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"/Annots ["
	.size	.L.str.39, 10

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	">>\nendobj\n"
	.size	.L.str.41, 11

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"%PDF-1.2\n"
	.size	.L.str.42, 10

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"%\342\343\317\323\n"
	.size	.L.str.43, 7

	.type	g_next_objnum,@object   # @g_next_objnum
	.local	g_next_objnum
	.comm	g_next_objnum,4,4
	.type	g_obj_offset_list,@object # @g_obj_offset_list
	.local	g_obj_offset_list
	.comm	g_obj_offset_list,8,8
	.type	g_cur_obj_offset_block,@object # @g_cur_obj_offset_block
	.local	g_cur_obj_offset_block
	.comm	g_cur_obj_offset_block,8,8
	.type	g_doc_author,@object    # @g_doc_author
	.local	g_doc_author
	.comm	g_doc_author,8,8
	.type	g_doc_title,@object     # @g_doc_title
	.local	g_doc_title
	.comm	g_doc_title,8,8
	.type	g_doc_subject,@object   # @g_doc_subject
	.local	g_doc_subject
	.comm	g_doc_subject,8,8
	.type	g_doc_keywords,@object  # @g_doc_keywords
	.local	g_doc_keywords
	.comm	g_doc_keywords,8,8
	.type	g_target_annot_list,@object # @g_target_annot_list
	.local	g_target_annot_list
	.comm	g_target_annot_list,8,8
	.type	g_has_exported_targets,@object # @g_has_exported_targets
	.local	g_has_exported_targets
	.comm	g_has_exported_targets,4,4
	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"<<\n"
	.size	.L.str.45, 4

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"/%s [ "
	.size	.L.str.46, 7

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	" /XYZ null null null ]\n"
	.size	.L.str.47, 24

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"/Type /Catalog\n"
	.size	.L.str.49, 16

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"/Pages "
	.size	.L.str.50, 8

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"/Dests "
	.size	.L.str.51, 8

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"/Creator (%s)\n"
	.size	.L.str.53, 15

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"Basser Lout Version 3.24 (October 2000)"
	.size	.L.str.54, 40

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"/Producer (%s)\n"
	.size	.L.str.55, 16

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"/CreationDate (Sometime Today)\n"
	.size	.L.str.56, 32

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"/Author (%s)\n"
	.size	.L.str.57, 14

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"/Title (%s)\n"
	.size	.L.str.58, 13

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"/Subject (%s)\n"
	.size	.L.str.59, 15

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"/Keywords (%s)\n"
	.size	.L.str.60, 16

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"trailer\n<<\n"
	.size	.L.str.61, 12

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"/Size %u\n"
	.size	.L.str.62, 10

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"/Root "
	.size	.L.str.63, 7

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"\n/Info "
	.size	.L.str.64, 8

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	" >>\nstartxref\n%u\n"
	.size	.L.str.65, 18

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"%%EOF\n"
	.size	.L.str.66, 7

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"PDFFile_Cleanup: unresolved link annotation named "
	.size	.L.str.67, 51

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"%s"
	.size	.L.str.68, 3

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"PDFFont_NewListEntry: run out of memory"
	.size	.L.str.69, 40

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"%u"
	.size	.L.str.70, 3

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"<< /Length "
	.size	.L.str.72, 12

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	" >>\nstream\n"
	.size	.L.str.73, 12

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"%u w\n"
	.size	.L.str.74, 6

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"assert failed in %s"
	.size	.L.str.75, 20

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"BT\n"
	.size	.L.str.76, 4

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"1 0 0 1 %d %d Tm\n"
	.size	.L.str.77, 18

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"%d 0 Td\n"
	.size	.L.str.78, 9

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"[("
	.size	.L.str.79, 3

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"PDFPage_CollectExpr: expression too long (max. 512 chars)"
	.size	.L.str.80, 58

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"PDFPage_EvalExpr: '(' expected"
	.size	.L.str.81, 31

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"PDFPage_EvalExpr: ',' expected"
	.size	.L.str.82, 31

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"PDFPage_EvalExpr: ')' expected"
	.size	.L.str.83, 31

	.type	g_unit_keywords,@object # @g_unit_keywords
	.data
	.p2align	4
g_unit_keywords:
	.quad	.L.str.87
	.quad	.L.str.88
	.quad	.L.str.89
	.quad	.L.str.90
	.quad	.L.str.91
	.quad	.L.str.92
	.quad	.L.str.93
	.size	g_unit_keywords, 56

	.type	.L.str.84,@object       # @.str.84
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.84:
	.asciz	"PDFPage_EvalExpr: __add, __sub, __mul, __div, or a unit keyword was expected"
	.size	.L.str.84, 77

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"%f"
	.size	.L.str.85, 3

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"PDFPage_GetFloat: unable to evaluate number for Lout graphic keyword processing"
	.size	.L.str.86, 80

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"in"
	.size	.L.str.87, 3

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"cm"
	.size	.L.str.88, 3

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"pt"
	.size	.L.str.89, 3

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"em"
	.size	.L.str.90, 3

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"loutf"
	.size	.L.str.91, 6

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"loutv"
	.size	.L.str.92, 6

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"louts"
	.size	.L.str.93, 6

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"PDFPage_CollectLink: link too long (max. 512 chars)"
	.size	.L.str.94, 52

	.type	g_link,@object          # @g_link
	.local	g_link
	.comm	g_link,512,16
	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"PDFPage_ProcessLinkKeyword: empty link-name / URI; ignored."
	.size	.L.str.95, 60

	.type	g_dest_link_options,@object # @g_dest_link_options
	.data
	.p2align	4
g_dest_link_options:
	.quad	.L.str.98
	.quad	.L.str.99
	.quad	.L.str.100
	.quad	.L.str.101
	.quad	.L.str.102
	.quad	.L.str.103
	.quad	.L.str.104
	.quad	.L.str.105
	.size	g_dest_link_options, 64

	.type	g_external_file_spec_keyword,@object # @g_external_file_spec_keyword
	.p2align	3
g_external_file_spec_keyword:
	.quad	.L.str.107
	.size	g_external_file_spec_keyword, 8

	.type	.L.str.96,@object       # @.str.96
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.96:
	.asciz	"PDFPage_ProcessLinkKeyword: empty file spec"
	.size	.L.str.96, 44

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"PDFPage_ProcessLinkKeyword: out of memory"
	.size	.L.str.97, 42

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"__FitNoChange"
	.size	.L.str.98, 14

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"__Fit"
	.size	.L.str.99, 6

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"__FitH"
	.size	.L.str.100, 7

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"__FitV"
	.size	.L.str.101, 7

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"__FitR"
	.size	.L.str.102, 7

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"__FitB"
	.size	.L.str.103, 7

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"__FitBH"
	.size	.L.str.104, 8

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"__FitBV"
	.size	.L.str.105, 8

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"PDFSourceAnnot_New: run out of memory"
	.size	.L.str.106, 38

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"__link_to="
	.size	.L.str.107, 11

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"PDFTargetAnnot_New: run out of memory"
	.size	.L.str.108, 38

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"xsize"
	.size	.L.str.109, 6

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"ysize"
	.size	.L.str.110, 6

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"xmark"
	.size	.L.str.111, 6

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"ymark"
	.size	.L.str.112, 6

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"link_source=<<"
	.size	.L.str.113, 15

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"link_external=<<"
	.size	.L.str.114, 17

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"link_URI=<<"
	.size	.L.str.115, 12

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"link_target=<<"
	.size	.L.str.116, 15

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"link_target_for_export=<<"
	.size	.L.str.117, 26

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"author="
	.size	.L.str.118, 8

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"title="
	.size	.L.str.119, 7

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"subject="
	.size	.L.str.120, 9

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"keywords="
	.size	.L.str.121, 10

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"PDFPage_ProcessDocInfoKeyword: no memory for __author="
	.size	.L.str.122, 55

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"PDFPage_ProcessDocInfoKeyword: no memory for __title="
	.size	.L.str.123, 54

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"PDFPage_ProcessDocInfoKeyword: no memory for __subject="
	.size	.L.str.124, 56

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"PDFPage_ProcessDocInfoKeyword: no memory for __keywords="
	.size	.L.str.125, 57

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"add"
	.size	.L.str.126, 4

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"sub"
	.size	.L.str.127, 4

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"mul"
	.size	.L.str.128, 4

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"div"
	.size	.L.str.129, 4

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"sin"
	.size	.L.str.130, 4

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"cos"
	.size	.L.str.131, 4

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"pick"
	.size	.L.str.132, 5

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"PDFObject_New: run out of memory"
	.size	.L.str.133, 33

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"endstream\nendobj\n"
	.size	.L.str.134, 18

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"%u 0 obj\n"
	.size	.L.str.135, 10

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"<<\n/Type /Font\n/Subtype /Type1\n"
	.size	.L.str.137, 32

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"/Name %s\n"
	.size	.L.str.138, 10

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"/BaseFont /%s\n"
	.size	.L.str.139, 15

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"/Encoding "
	.size	.L.str.140, 11

	.type	.L.str.156,@object      # @.str.156
.L.str.156:
	.asciz	"<<\n/Type /Annot\n/Subtype /Link\n/Rect [ %d %d %d %d ]\n/Border [ 0 0 0 ]\n"
	.size	.L.str.156, 72

	.type	.L.str.157,@object      # @.str.157
.L.str.157:
	.asciz	"/Dest [ "
	.size	.L.str.157, 9

	.type	.L.str.158,@object      # @.str.158
.L.str.158:
	.asciz	" /XYZ null null null"
	.size	.L.str.158, 21

	.type	.L.str.159,@object      # @.str.159
.L.str.159:
	.asciz	" /Fit"
	.size	.L.str.159, 6

	.type	.L.str.160,@object      # @.str.160
.L.str.160:
	.asciz	" /FitH %u"
	.size	.L.str.160, 10

	.type	.L.str.161,@object      # @.str.161
.L.str.161:
	.asciz	" /FitV %u"
	.size	.L.str.161, 10

	.type	.L.str.162,@object      # @.str.162
.L.str.162:
	.asciz	" /FitR %u %u %u %u"
	.size	.L.str.162, 19

	.type	.L.str.163,@object      # @.str.163
.L.str.163:
	.asciz	" /FitB"
	.size	.L.str.163, 7

	.type	.L.str.164,@object      # @.str.164
.L.str.164:
	.asciz	" /FitBH %u"
	.size	.L.str.164, 11

	.type	.L.str.165,@object      # @.str.165
.L.str.165:
	.asciz	" /FitBV %u"
	.size	.L.str.165, 11

	.type	.L.str.166,@object      # @.str.166
.L.str.166:
	.asciz	"PDFSourceAnnot_Write: invalid link dest option"
	.size	.L.str.166, 47

	.type	.L.str.167,@object      # @.str.167
.L.str.167:
	.asciz	"/A << /Type /Action /S /GoToR /D (%s) /F\n(%s) >>\n"
	.size	.L.str.167, 50

	.type	.L.str.168,@object      # @.str.168
.L.str.168:
	.asciz	"/A << /Type /Action /S /URI /URI\n(%s) >>\n"
	.size	.L.str.168, 42

	.type	.L.str.169,@object      # @.str.169
.L.str.169:
	.asciz	"%u 0 R"
	.size	.L.str.169, 7

	.type	.L.str.171,@object      # @.str.171
.L.str.171:
	.asciz	"/Type /Pages\n"
	.size	.L.str.171, 14

	.type	.L.str.172,@object      # @.str.172
.L.str.172:
	.asciz	"/Kids [ "
	.size	.L.str.172, 9

	.type	.L.str.173,@object      # @.str.173
.L.str.173:
	.asciz	" ]\n/Count %u\n"
	.size	.L.str.173, 14

	.type	.L.str.174,@object      # @.str.174
.L.str.174:
	.asciz	"/MediaBox [ 0 0 %u %u ]\n"
	.size	.L.str.174, 25

	.type	.L.str.176,@object      # @.str.176
.L.str.176:
	.asciz	"xref\n"
	.size	.L.str.176, 6

	.type	.L.str.177,@object      # @.str.177
.L.str.177:
	.asciz	"0 %u\n"
	.size	.L.str.177, 6

	.type	.L.str.178,@object      # @.str.178
.L.str.178:
	.asciz	"0000000000 65535 f \n"
	.size	.L.str.178, 21

	.type	.L.str.179,@object      # @.str.179
.L.str.179:
	.asciz	"%010u 00000 n \n"
	.size	.L.str.179, 16

	.type	.L.str.180,@object      # @.str.180
.L.str.180:
	.asciz	"PDFFile_WriteXREF: undefined object number: "
	.size	.L.str.180, 45


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
