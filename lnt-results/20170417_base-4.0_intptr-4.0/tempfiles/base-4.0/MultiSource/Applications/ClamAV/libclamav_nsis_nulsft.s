	.text
	.file	"libclamav_nsis_nulsft.bc"
	.globl	cli_scannulsft
	.p2align	4, 0x90
	.type	cli_scannulsft,@function
cli_scannulsft:                         # @cli_scannulsft
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$41480, %rsp            # imm = 0xA208
.Lcfi6:
	.cfi_def_cfa_offset 41536
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r13
	movl	%edi, %ebp
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	32(%r13), %rax
	testq	%rax, %rax
	je	.LBB0_4
# BB#1:
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.LBB0_4
# BB#2:
	movl	44(%r13), %esi
	cmpl	%eax, %esi
	jae	.LBB0_3
.LBB0_4:
	leaq	192(%rsp), %rdi
	xorl	%esi, %esi
	movl	$41288, %edx            # imm = 0xA148
	callq	memset
	movl	%ebp, 192(%rsp)
	movq	%rbx, 200(%rsp)
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %rbx
	movq	%rbx, 208(%rsp)
	movl	$-118, %ebp
	testq	%rbx, %rbx
	je	.LBB0_71
# BB#5:
	movl	$448, %esi              # imm = 0x1C0
	movq	%rbx, %rdi
	callq	mkdir
	testl	%eax, %eax
	je	.LBB0_7
# BB#6:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB0_71
.LBB0_3:
	incl	%esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-100, %ebp
	jmp	.LBB0_71
.LBB0_7:
	cmpb	$0, cli_leavetemps_flag(%rip)
	je	.LBB0_9
# BB#8:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
.LBB0_9:
	incl	44(%r13)
	leaq	40452(%rsp), %r14
	leaq	48(%rsp), %r15
	leaq	16(%rsp), %r12
	leaq	192(%rsp), %rbx
	xorl	%eax, %eax
	testl	%eax, %eax
	jne	.LBB0_45
	jmp	.LBB0_11
.LBB0_21:
	leaq	40452(%rsp), %r14
	leaq	48(%rsp), %r15
	leaq	16(%rsp), %r12
	leaq	192(%rsp), %rbx
	testl	%ebp, %ebp
	jne	.LBB0_47
	.p2align	4, 0x90
.LBB0_51:
	movl	224(%rsp), %esi
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	196(%rsp), %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	lseek
	movl	196(%rsp), %edi
	cmpl	$1, 224(%rsp)
	jne	.LBB0_53
# BB#52:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r13, %rsi
	callq	cli_scandesc
	jmp	.LBB0_54
	.p2align	4, 0x90
.LBB0_53:
	movq	%r13, %rsi
	callq	cli_magic_scandesc
.LBB0_54:
	movl	%eax, %ebp
	movl	196(%rsp), %edi
	callq	close
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB0_56
# BB#55:
	movq	%r14, %rdi
	callq	unlink
	.p2align	4, 0x90
.LBB0_56:
	testl	%ebp, %ebp
	je	.LBB0_72
	jmp	.LBB0_57
.LBB0_33:                               # %.thread64.i.i
	movb	$1, 40449(%rsp)
.LBB0_36:                               # %.loopexit.i.i
	cmpl	$0, 44(%rsp)            # 4-byte Folded Reload
	leaq	40452(%rsp), %r14
	leaq	48(%rsp), %r15
	je	.LBB0_38
# BB#37:                                # %.loopexit.i.i
	cmpl	$1, %r12d
	jle	.LBB0_38
# BB#39:
	movb	$0, 40449(%rsp)
	movl	$.L.str.37, %esi
	jmp	.LBB0_40
	.p2align	4, 0x90
.LBB0_72:                               # %._crit_edge
	movl	224(%rsp), %eax
	testl	%eax, %eax
	je	.LBB0_11
.LBB0_45:
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	nsis_unpack_next
	movl	%eax, %ebp
.LBB0_46:                               # %cli_nsis_unpack.exit
	testl	%ebp, %ebp
	je	.LBB0_51
.LBB0_47:                               # %cli_nsis_unpack.exit
	cmpl	$-101, %ebp
	jne	.LBB0_56
# BB#48:
	testb	$1, 41(%r13)
	jne	.LBB0_49
# BB#50:
	xorl	%ebp, %ebp
	cmpb	$0, 40449(%rsp)
	setne	%bpl
	addl	%ebp, %ebp
	testl	%ebp, %ebp
	je	.LBB0_72
	jmp	.LBB0_57
	.p2align	4, 0x90
.LBB0_11:
	movl	$0, 12(%rsp)
	movl	192(%rsp), %esi
	movl	$1, %edi
	movq	%r15, %rdx
	callq	__fxstat
	movl	$-123, %ebp
	cmpl	$-1, %eax
	je	.LBB0_46
# BB#12:
	movl	192(%rsp), %edi
	movq	200(%rsp), %rsi
	xorl	%edx, %edx
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_46
# BB#13:
	movl	192(%rsp), %edi
	movl	$28, %edx
	movq	%r12, %rsi
	callq	cli_readn
	cmpl	$28, %eax
	jne	.LBB0_46
# BB#14:
	movl	36(%rsp), %edx
	movl	%edx, 220(%rsp)
	movl	40(%rsp), %ecx
	movl	%ecx, 216(%rsp)
	movl	16(%rsp), %esi
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	96(%rsp), %rcx
	subq	200(%rsp), %rcx
	movl	216(%rsp), %eax
	cmpq	%rax, %rcx
	jge	.LBB0_16
# BB#15:
	movl	$.L.str.33, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	96(%rsp), %eax
	subl	200(%rsp), %eax
	movl	%eax, 216(%rsp)
	movl	$1, %ebx
	jmp	.LBB0_18
.LBB0_16:
	xorl	%ebx, %ebx
	cmpq	%rax, %rcx
	je	.LBB0_18
# BB#17:
	xorl	%ebx, %ebx
	movl	$.L.str.34, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	216(%rsp), %eax
.LBB0_18:
	leal	-28(%rax), %ecx
	movl	%ecx, 216(%rsp)
	cmpl	$32, %eax
	jne	.LBB0_19
.LBB0_38:                               # %.loopexit._crit_edge.i.i
	cmpb	$0, 40449(%rsp)
	movl	$.L.str.37, %esi
	movl	$.L.str.36, %eax
	cmovneq	%rax, %rsi
.LBB0_40:
	leaq	16(%rsp), %r12
	leaq	192(%rsp), %rbx
	movl	$.L.str.35, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpb	$0, 40449(%rsp)
	jne	.LBB0_44
# BB#41:
	movzbl	13(%rsp), %ebx
	movzbl	14(%rsp), %r12d
	movzbl	15(%rsp), %r14d
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	movl	%r12d, %edx
	movl	%r14d, %ecx
	callq	cli_dbgmsg
	xorl	%ecx, %ecx
	cmpb	%r12b, %bl
	setb	%cl
	movb	$1, %al
	adcb	$0, %al
	cmpb	%r14b, 13(%rsp,%rcx)
	movb	$3, %cl
	jb	.LBB0_43
# BB#42:
	movl	%eax, %ecx
.LBB0_43:
	movb	%cl, 40448(%rsp)
	leaq	40452(%rsp), %r14
	leaq	48(%rsp), %r15
	leaq	16(%rsp), %r12
	leaq	192(%rsp), %rbx
.LBB0_44:
	movl	192(%rsp), %edi
	movq	200(%rsp), %rsi
	addq	$28, %rsi
	xorl	%edx, %edx
	callq	lseek
	cmpq	$-1, %rax
	jne	.LBB0_45
	jmp	.LBB0_46
.LBB0_19:                               # %.lr.ph.i.i.preheader
	movl	%ebx, 44(%rsp)          # 4-byte Spill
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_20:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	192(%rsp), %edi
	movl	$4, %edx
	leaq	20(%rsp), %rsi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB0_21
# BB#22:                                #   in Loop: Header=BB0_20 Depth=1
	movl	20(%rsp), %r14d
	testl	%r12d, %r12d
	jne	.LBB0_26
# BB#23:                                #   in Loop: Header=BB0_20 Depth=1
	movb	$1, %al
	cmpb	$49, %r14b
	je	.LBB0_25
# BB#24:                                #   in Loop: Header=BB0_20 Depth=1
	movl	%r14d, %eax
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	cmpl	$93, %eax
	setne	%al
	orb	$2, %al
.LBB0_25:                               # %nsis_detcomp.exit.i.i
                                        #   in Loop: Header=BB0_20 Depth=1
	movb	%al, 40448(%rsp)
.LBB0_26:                               #   in Loop: Header=BB0_20 Depth=1
	testl	%r14d, %r14d
	jns	.LBB0_32
# BB#27:                                #   in Loop: Header=BB0_20 Depth=1
	movl	192(%rsp), %edi
	movl	$4, %edx
	leaq	20(%rsp), %rsi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB0_21
# BB#28:                                #   in Loop: Header=BB0_20 Depth=1
	andl	$2147483647, %r14d      # imm = 0x7FFFFFFF
	cmpb	$49, 20(%rsp)
	jne	.LBB0_30
# BB#29:                                #   in Loop: Header=BB0_20 Depth=1
	movl	$1, %eax
	jmp	.LBB0_31
.LBB0_30:                               #   in Loop: Header=BB0_20 Depth=1
	movl	20(%rsp), %ecx
	movl	$2147483647, %eax       # imm = 0x7FFFFFFF
	andl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	$93, %ecx
	setne	%al
	orq	$2, %rax
.LBB0_31:                               # %nsis_detcomp.exit63.i.i
                                        #   in Loop: Header=BB0_20 Depth=1
	incb	12(%rsp,%rax)
	addl	$-4, %r14d
	addl	$4, %ebx
.LBB0_32:                               #   in Loop: Header=BB0_20 Depth=1
	leal	4(%rbx,%r14), %ebx
	cmpl	216(%rsp), %ebx
	ja	.LBB0_33
# BB#34:                                #   in Loop: Header=BB0_20 Depth=1
	movl	192(%rsp), %edi
	movslq	%r14d, %rsi
	movl	$1, %edx
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_21
# BB#35:                                #   in Loop: Header=BB0_20 Depth=1
	incl	%r12d
	movl	216(%rsp), %eax
	addl	$-4, %eax
	cmpl	%eax, %ebx
	jb	.LBB0_20
	jmp	.LBB0_36
.LBB0_57:
	cmpl	$2, %ebp
	jne	.LBB0_59
# BB#58:                                # %.loopexit.loopexit30
	xorl	%ebp, %ebp
	cmpb	$0, 40450(%rsp)
	jne	.LBB0_60
	jmp	.LBB0_65
.LBB0_49:                               # %.thread
	movq	(%r13), %rax
	movq	$.L.str.4, (%rax)
	movl	$1, %ebp
.LBB0_59:                               # %.loopexit
	cmpb	$0, 40450(%rsp)
	je	.LBB0_65
.LBB0_60:
	movb	40448(%rsp), %al
	cmpb	$2, %al
	je	.LBB0_63
# BB#61:
	cmpb	$1, %al
	jne	.LBB0_64
# BB#62:
	leaq	264(%rsp), %rdi
	callq	nsis_BZ2_bzDecompressEnd
	jmp	.LBB0_64
.LBB0_63:
	leaq	344(%rsp), %rdi
	callq	lzmaShutdown
.LBB0_64:
	movb	$0, 40450(%rsp)
.LBB0_65:                               # %nsis_shutdown.exit.i
	cmpb	$0, 40449(%rsp)
	je	.LBB0_68
# BB#66:
	movq	40440(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_68
# BB#67:
	callq	free
.LBB0_68:                               # %cli_nsis_free.exit
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB0_70
# BB#69:
	movq	208(%rsp), %rdi
	callq	cli_rmdirs
.LBB0_70:
	movq	208(%rsp), %rdi
	callq	free
	decl	44(%r13)
.LBB0_71:
	movl	%ebp, %eax
	addq	$41480, %rsp            # imm = 0xA208
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	cli_scannulsft, .Lfunc_end0-cli_scannulsft
	.cfi_endproc

	.p2align	4, 0x90
	.type	nsis_unpack_next,@function
nsis_unpack_next:                       # @nsis_unpack_next
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 48
	subq	$8208, %rsp             # imm = 0x2010
.Lcfi18:
	.cfi_def_cfa_offset 8256
.Lcfi19:
	.cfi_offset %rbx, -48
.Lcfi20:
	.cfi_offset %r12, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpb	$0, 40259(%rbx)
	je	.LBB1_4
# BB#1:
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB1_2:                                # %nsis_shutdown.exit
	movl	$2, %ebp
.LBB1_3:                                # %nsis_shutdown.exit
	movl	%ebp, %eax
	addq	$8208, %rsp             # imm = 0x2010
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_4:
	movq	32(%r14), %rax
	testq	%rax, %rax
	je	.LBB1_7
# BB#5:
	movl	4(%rax), %esi
	testl	%esi, %esi
	je	.LBB1_7
# BB#6:
	cmpl	%esi, 32(%rbx)
	jae	.LBB1_16
.LBB1_7:
	movl	32(%rbx), %r8d
	leaq	40260(%rbx), %rbp
	testl	%r8d, %r8d
	movq	16(%rbx), %rcx
	movl	$1023, %esi             # imm = 0x3FF
	je	.LBB1_9
# BB#8:
	movl	$.L.str.8, %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	snprintf
	jmp	.LBB1_10
.LBB1_9:
	movl	$.L.str.9, %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	snprintf
.LBB1_10:
	incl	32(%rbx)
	movl	$578, %esi              # imm = 0x242
	movl	$384, %edx              # imm = 0x180
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	open
	movl	%eax, 4(%rbx)
	cmpl	$-1, %eax
	je	.LBB1_15
# BB#11:
	cmpb	$0, 40257(%rbx)
	je	.LBB1_17
# BB#12:
	cmpq	$0, 40248(%rbx)
	je	.LBB1_20
# BB#13:                                # %._crit_edge
	movl	40(%rbx), %esi
	cmpl	$4, %esi
	ja	.LBB1_44
.LBB1_14:
	movl	$.L.str.6, %edi
	jmp	.LBB1_27
.LBB1_15:
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_errmsg
	movl	$-123, %ebp
	jmp	.LBB1_3
.LBB1_16:
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-102, %ebp
	jmp	.LBB1_3
.LBB1_17:
	movl	(%rbx), %edi
	leaq	12(%rsp), %rsi
	movl	$4, %edx
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB1_26
# BB#18:
	movl	24(%rbx), %eax
	cmpl	$4, %eax
	jne	.LBB1_28
# BB#19:
	movl	$.L.str.12, %edi
	jmp	.LBB1_27
.LBB1_20:
	movb	40256(%rbx), %al
	cmpb	$3, %al
	je	.LBB1_35
# BB#21:
	cmpb	$2, %al
	je	.LBB1_36
# BB#22:
	cmpb	$1, %al
	jne	.LBB1_41
# BB#23:
	leaq	72(%rbx), %rdi
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	nsis_BZ2_bzDecompressInit
	testl	%eax, %eax
	je	.LBB1_39
# BB#24:                                # %nsis_init.exit144
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB1_25:                               # %nsis_shutdown.exit
	movl	4(%rbx), %edi
	callq	close
	movl	$-106, %ebp
	jmp	.LBB1_3
.LBB1_26:
	movl	$.L.str.11, %edi
.LBB1_27:                               # %nsis_shutdown.exit
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	4(%rbx), %edi
	callq	close
	jmp	.LBB1_2
.LBB1_28:
	movl	12(%rsp), %ebp
	movl	%ebp, %esi
	andl	$2147483647, %esi       # imm = 0x7FFFFFFF
	movl	%esi, 12(%rsp)
	je	.LBB1_37
# BB#29:
	cmpl	$4, %eax
	jb	.LBB1_38
# BB#30:
	leal	-4(%rax), %ecx
	cmpl	%ecx, %esi
	ja	.LBB1_38
# BB#31:
	movl	$-4, %ecx
	subl	%esi, %ecx
	addl	%eax, %ecx
	movl	%ecx, 24(%rbx)
	movq	32(%r14), %rax
	testq	%rax, %rax
	je	.LBB1_68
# BB#32:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB1_68
# BB#33:
	movl	%esi, %eax
	cmpq	%rdx, %rax
	jbe	.LBB1_68
# BB#34:
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	4(%rbx), %edi
	callq	close
	movl	(%rbx), %edi
	movl	12(%rsp), %esi
	movl	$1, %edx
	callq	lseek
	cmpq	$-1, %rax
	movl	$-123, %eax
	movl	$-101, %ebp
	cmovel	%eax, %ebp
	jmp	.LBB1_3
.LBB1_35:
	movl	$8, 360(%rbx)
	movq	$0, 1680(%rbx)
	movl	$0, 1676(%rbx)
	leaq	7448(%rbx), %rax
	movq	%rax, 40232(%rbx)
	movq	%rax, 40224(%rbx)
	leaq	40216(%rbx), %rax
	movq	%rax, 40216(%rbx)
	xorl	%eax, %eax
	jmp	.LBB1_40
.LBB1_36:
	leaq	152(%rbx), %rdi
	callq	lzmaInit
.LBB1_39:
	movb	$1, %al
.LBB1_40:                               # %.sink.split.i142
	movb	%al, 40258(%rbx)
.LBB1_41:
	movl	24(%rbx), %edi
	callq	cli_malloc
	movq	%rax, 40248(%rbx)
	testq	%rax, %rax
	je	.LBB1_56
# BB#42:
	movl	(%rbx), %edi
	movl	24(%rbx), %edx
	movq	%rax, %rsi
	callq	cli_readn
	cltq
	movl	24(%rbx), %esi
	cmpq	%rsi, %rax
	jne	.LBB1_58
# BB#43:
	movq	40248(%rbx), %rax
	movq	%rax, 48(%rbx)
	movl	%esi, 40(%rbx)
	cmpl	$4, %esi
	jbe	.LBB1_14
.LBB1_44:
	leaq	16(%rsp), %r15
	movq	%r15, 64(%rbx)
	movl	$4, 56(%rbx)
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_45:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	nsis_decomp
	testl	%eax, %eax
	jne	.LBB1_49
# BB#46:                                #   in Loop: Header=BB1_45 Depth=1
	movq	64(%rbx), %rax
	subq	%r15, %rax
	cmpq	$4, %rax
	je	.LBB1_52
# BB#47:                                #   in Loop: Header=BB1_45 Depth=1
	incl	%ebp
	cmpl	$21, %ebp
	jb	.LBB1_45
# BB#48:
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB1_49:                               # %.loopexit153
	movl	$.L.str.28, %edi
.LBB1_50:                               # %nsis_shutdown.exit
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB1_51:                               # %nsis_shutdown.exit
	movl	4(%rbx), %edi
	callq	close
	movl	$-124, %ebp
	jmp	.LBB1_3
.LBB1_52:
	movl	16(%rsp), %esi
	movl	%esi, 12(%rsp)
	movq	32(%r14), %rax
	testq	%rax, %rax
	je	.LBB1_60
# BB#53:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB1_60
# BB#54:
	cmpq	%rdx, %rsi
	jbe	.LBB1_60
# BB#55:
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	jmp	.LBB1_51
.LBB1_56:
	movl	$.L.str.25, %edi
.LBB1_57:                               # %nsis_shutdown.exit
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	4(%rbx), %edi
	callq	close
	movl	$-114, %ebp
	jmp	.LBB1_3
.LBB1_58:
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
.LBB1_59:                               # %.thread147
	movl	4(%rbx), %edi
	callq	close
	movl	$-123, %ebp
	jmp	.LBB1_3
.LBB1_60:                               # %.thread146.sink.split.preheader
	movq	%r15, 64(%rbx)
	cmpl	$8192, %esi             # imm = 0x2000
	movl	$8192, %eax             # imm = 0x2000
	cmovbl	%esi, %eax
	movl	%eax, 56(%rbx)
	testl	%esi, %esi
	je	.LBB1_78
# BB#61:                                # %.lr.ph.preheader
	xorl	%r14d, %r14d
.LBB1_62:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	nsis_decomp
	testl	%eax, %eax
	jne	.LBB1_73
# BB#63:                                #   in Loop: Header=BB1_62 Depth=1
	movq	64(%rbx), %rbp
	subq	%r15, %rbp
	testl	%ebp, %ebp
	jne	.LBB1_66
# BB#64:                                #   in Loop: Header=BB1_62 Depth=1
	incl	%r14d
	cmpl	$20, %r14d
	ja	.LBB1_75
# BB#65:                                # %..thread146_crit_edge
                                        #   in Loop: Header=BB1_62 Depth=1
	xorl	%ebp, %ebp
	cmpl	$0, 12(%rsp)
	jne	.LBB1_62
	jmp	.LBB1_3
.LBB1_66:                               #   in Loop: Header=BB1_62 Depth=1
	movl	4(%rbx), %edi
	movq	%r15, %rsi
	movl	%ebp, %edx
	callq	cli_writen
	cltq
	movl	%ebp, %ecx
	cmpq	%rcx, %rax
	jne	.LBB1_59
# BB#67:                                # %.thread146.sink.split
                                        #   in Loop: Header=BB1_62 Depth=1
	movl	12(%rsp), %eax
	subl	%ebp, %eax
	movl	%eax, 12(%rsp)
	movq	%r15, 64(%rbx)
	cmpl	$8192, %eax             # imm = 0x2000
	movl	$8192, %ecx             # imm = 0x2000
	cmovbl	%eax, %ecx
	movl	%ecx, 56(%rbx)
	xorl	%ebp, %ebp
	testl	%eax, %eax
	jne	.LBB1_62
	jmp	.LBB1_3
.LBB1_37:
	xorl	%ebp, %ebp
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB1_3
.LBB1_38:
	movl	$.L.str.14, %edi
	jmp	.LBB1_27
.LBB1_68:
	movl	%esi, %edi
	callq	cli_malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB1_79
# BB#69:
	movl	(%rbx), %edi
	movl	12(%rsp), %edx
	movq	%r15, %rsi
	callq	cli_readn
	cltq
	movl	12(%rsp), %esi
	cmpq	%rsi, %rax
	jne	.LBB1_80
# BB#70:
	cmpl	%esi, %ebp
	jne	.LBB1_81
# BB#71:
	movl	4(%rbx), %edi
	movq	%r15, %rsi
	movl	%ebp, %edx
	callq	cli_writen
	cltq
	movl	12(%rsp), %ecx
	cmpq	%rcx, %rax
	jne	.LBB1_86
.LBB1_72:                               # %nsis_shutdown.exit140
	movq	%r15, %rdi
	callq	free
	jmp	.LBB1_78
.LBB1_73:
	cmpl	$2, %eax
	je	.LBB1_76
# BB#74:
	movl	$.L.str.31, %edi
	jmp	.LBB1_50
.LBB1_75:                               # %.critedge.thread
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB1_76:                               # %.loopexit152
	movl	4(%rbx), %edi
	movl	64(%rbx), %edx
	subl	%r15d, %edx
	movq	%r15, %rsi
	callq	cli_writen
	cltq
	movq	64(%rbx), %rcx
	subq	%r15, %rcx
	cmpq	%rcx, %rax
	jne	.LBB1_59
# BB#77:
	movb	$1, 40259(%rbx)
.LBB1_78:                               # %nsis_shutdown.exit
	xorl	%ebp, %ebp
	jmp	.LBB1_3
.LBB1_79:
	movl	$.L.str.16, %edi
	jmp	.LBB1_57
.LBB1_80:
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	jmp	.LBB1_110
.LBB1_81:
	movb	40256(%rbx), %al
	cmpb	$3, %al
	je	.LBB1_87
# BB#82:
	cmpb	$2, %al
	je	.LBB1_88
# BB#83:
	cmpb	$1, %al
	jne	.LBB1_91
# BB#84:
	leaq	72(%rbx), %rdi
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	nsis_BZ2_bzDecompressInit
	testl	%eax, %eax
	je	.LBB1_89
# BB#85:                                # %nsis_init.exit
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r15, %rdi
	callq	free
	jmp	.LBB1_25
.LBB1_86:
	movl	$.L.str.18, %edi
	jmp	.LBB1_109
.LBB1_87:
	movl	$8, 360(%rbx)
	movq	$0, 1680(%rbx)
	movl	$0, 1676(%rbx)
	leaq	7448(%rbx), %rax
	movq	%rax, 40232(%rbx)
	movq	%rax, 40224(%rbx)
	leaq	40216(%rbx), %rax
	movq	%rax, 40216(%rbx)
	xorl	%eax, %eax
	jmp	.LBB1_90
.LBB1_88:
	leaq	152(%rbx), %rdi
	callq	lzmaInit
.LBB1_89:
	movb	$1, %al
.LBB1_90:                               # %.sink.split.i
	movb	%al, 40258(%rbx)
	movl	12(%rsp), %esi
.LBB1_91:
	movl	%esi, 40(%rbx)
	movq	%r15, 48(%rbx)
	leaq	16(%rsp), %r12
	movq	%r12, 64(%rbx)
	movl	$8192, 56(%rbx)         # imm = 0x2000
	xorl	%ebp, %ebp
.LBB1_92:                               # %.backedge
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	nsis_decomp
	testl	%eax, %eax
	jne	.LBB1_100
# BB#93:                                #   in Loop: Header=BB1_92 Depth=1
	movl	64(%rbx), %edx
	subl	%r12d, %edx
	movl	%edx, 12(%rsp)
	je	.LBB1_98
# BB#94:                                #   in Loop: Header=BB1_92 Depth=1
	movl	4(%rbx), %edi
	movq	%r12, %rsi
	callq	cli_writen
	cltq
	movl	12(%rsp), %esi
	cmpq	%rsi, %rax
	jne	.LBB1_106
# BB#95:                                #   in Loop: Header=BB1_92 Depth=1
	movq	%r12, 64(%rbx)
	movl	$8192, 56(%rbx)         # imm = 0x2000
	movq	32(%r14), %rcx
	testq	%rcx, %rcx
	movl	$0, %ebp
	je	.LBB1_92
# BB#96:                                #   in Loop: Header=BB1_92 Depth=1
	movq	24(%rcx), %rdx
	testq	%rdx, %rdx
	movl	$0, %ebp
	je	.LBB1_92
# BB#97:                                #   in Loop: Header=BB1_92 Depth=1
	cmpq	%rdx, %rax
	movl	$0, %ebp
	jbe	.LBB1_92
	jmp	.LBB1_111
.LBB1_98:                               #   in Loop: Header=BB1_92 Depth=1
	incl	%ebp
	cmpl	$11, %ebp
	jb	.LBB1_92
# BB#99:                                # %.thread
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB1_101
.LBB1_100:                              # %.backedge
	cmpl	$2, %eax
	jne	.LBB1_107
.LBB1_101:                              # %.loopexit
	movl	4(%rbx), %edi
	movl	64(%rbx), %edx
	subl	%r12d, %edx
	movq	%r12, %rsi
	callq	cli_writen
	cltq
	movq	64(%rbx), %rcx
	subq	%r12, %rcx
	cmpq	%rcx, %rax
	jne	.LBB1_108
# BB#102:
	cmpb	$0, 40258(%rbx)
	je	.LBB1_72
# BB#103:
	movb	40256(%rbx), %al
	cmpb	$2, %al
	je	.LBB1_115
# BB#104:
	cmpb	$1, %al
	jne	.LBB1_116
# BB#105:
	leaq	72(%rbx), %rdi
	callq	nsis_BZ2_bzDecompressEnd
	jmp	.LBB1_116
.LBB1_106:
	movl	$.L.str.20, %edi
	jmp	.LBB1_109
.LBB1_107:
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r15, %rdi
	callq	free
	jmp	.LBB1_51
.LBB1_108:
	movl	$.L.str.23, %edi
.LBB1_109:                              # %nsis_shutdown.exit
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB1_110:                              # %nsis_shutdown.exit
	movq	%r15, %rdi
	callq	free
	jmp	.LBB1_59
.LBB1_111:
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movq	%r15, %rdi
	callq	free
	movl	4(%rbx), %edi
	callq	close
	movl	$-101, %ebp
	cmpb	$0, 40258(%rbx)
	je	.LBB1_3
# BB#112:
	movb	40256(%rbx), %al
	cmpb	$2, %al
	je	.LBB1_117
# BB#113:
	cmpb	$1, %al
	jne	.LBB1_118
# BB#114:
	leaq	72(%rbx), %rdi
	callq	nsis_BZ2_bzDecompressEnd
	movb	$0, 40258(%rbx)
	jmp	.LBB1_3
.LBB1_115:
	leaq	152(%rbx), %rdi
	callq	lzmaShutdown
.LBB1_116:
	movb	$0, 40258(%rbx)
	jmp	.LBB1_72
.LBB1_117:
	leaq	152(%rbx), %rdi
	callq	lzmaShutdown
.LBB1_118:
	movb	$0, 40258(%rbx)
	jmp	.LBB1_3
.Lfunc_end1:
	.size	nsis_unpack_next, .Lfunc_end1-nsis_unpack_next
	.cfi_endproc

	.p2align	4, 0x90
	.type	nsis_decomp,@function
nsis_decomp:                            # @nsis_decomp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 80
.Lcfi31:
	.cfi_offset %rbx, -56
.Lcfi32:
	.cfi_offset %r12, -48
.Lcfi33:
	.cfi_offset %r13, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movb	40256(%rbx), %al
	cmpb	$3, %al
	je	.LBB2_4
# BB#1:
	cmpb	$2, %al
	je	.LBB2_5
# BB#2:
	movl	$-124, %ecx
	cmpb	$1, %al
	jne	.LBB2_8
# BB#3:
	leaq	40(%rbx), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	40(%rbx), %eax
	leaq	72(%rbx), %r14
	leaq	80(%rbx), %rbp
	movl	%eax, 80(%rbx)
	leaq	48(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	48(%rbx), %rax
	movq	%rax, 72(%rbx)
	leaq	56(%rbx), %r13
	movl	56(%rbx), %eax
	leaq	104(%rbx), %r15
	movl	%eax, 104(%rbx)
	leaq	64(%rbx), %r12
	movq	64(%rbx), %rax
	movq	%rax, 96(%rbx)
	addq	$96, %rbx
	movq	%r14, %rdi
	callq	nsis_BZ2_bzDecompress
	cmpl	$4, %eax
	jmp	.LBB2_6
.LBB2_4:
	movl	40(%rbx), %eax
	leaq	320(%rbx), %r14
	leaq	328(%rbx), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%eax, 328(%rbx)
	leaq	48(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	48(%rbx), %rax
	movq	%rax, 320(%rbx)
	leaq	56(%rbx), %r13
	movl	56(%rbx), %eax
	leaq	352(%rbx), %r15
	movl	%eax, 352(%rbx)
	leaq	64(%rbx), %r12
	movq	64(%rbx), %rax
	leaq	344(%rbx), %rbp
	movq	%rax, 344(%rbx)
	leaq	40(%rbx), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%r14, %rdi
	callq	nsis_inflate
	cmpl	$1, %eax
	sete	%cl
	movq	%rbp, %rbx
	movq	16(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB2_7
.LBB2_5:
	leaq	40(%rbx), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	40(%rbx), %eax
	leaq	152(%rbx), %rdi
	leaq	184(%rbx), %rbp
	movl	%eax, 184(%rbx)
	leaq	48(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	48(%rbx), %rax
	leaq	176(%rbx), %r14
	movq	%rax, 176(%rbx)
	leaq	56(%rbx), %r13
	movl	56(%rbx), %eax
	leaq	200(%rbx), %r15
	movl	%eax, 200(%rbx)
	leaq	64(%rbx), %r12
	movq	64(%rbx), %rax
	movq	%rax, 192(%rbx)
	addq	$192, %rbx
	callq	lzmaDecode
	cmpl	$1, %eax
.LBB2_6:                                # %.sink.split
	sete	%cl
.LBB2_7:                                # %.sink.split
	testb	%cl, %cl
	movl	$2, %edx
	movl	$-124, %ecx
	cmovnel	%edx, %ecx
	testl	%eax, %eax
	cmovel	%eax, %ecx
	movl	(%rbp), %eax
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	%eax, (%rdx)
	movq	(%r14), %rax
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rax, (%rdx)
	movl	(%r15), %eax
	movl	%eax, (%r13)
	movq	(%rbx), %rax
	movq	%rax, (%r12)
.LBB2_8:
	movl	%ecx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	nsis_decomp, .Lfunc_end2-nsis_decomp
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"in scannulsft()\n"
	.size	.L.str, 17

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Archive recursion limit exceeded (arec == %u).\n"
	.size	.L.str.1, 48

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"NSIS: Can't create temporary directory %s\n"
	.size	.L.str.2, 43

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"NSIS: Extracting files to %s\n"
	.size	.L.str.3, 30

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"NSIS.ExceededFileSize"
	.size	.L.str.4, 22

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"NSIS: Successully extracted file #%u\n"
	.size	.L.str.5, 38

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"NSIS: extraction complete\n"
	.size	.L.str.6, 27

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"NSIS: Files limit reached (max: %u)\n"
	.size	.L.str.7, 37

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"%s/content.%.3u"
	.size	.L.str.8, 16

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%s/headers"
	.size	.L.str.9, 11

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"NSIS: unable to create output file %s - aborting."
	.size	.L.str.10, 50

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"NSIS: reached EOF - extraction complete\n"
	.size	.L.str.11, 41

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"NSIS: reached CRC - extraction complete\n"
	.size	.L.str.12, 41

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"NSIS: empty file found\n"
	.size	.L.str.13, 24

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"NSIS: next file is outside the archive\n"
	.size	.L.str.14, 40

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"NSIS: Skipping file due to size limit (%u, max: %lu)\n"
	.size	.L.str.15, 54

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"NSIS: out of memory at /mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/ClamAV/libclamav_nsis_nulsft.c:236\n"
	.size	.L.str.16, 122

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"NSIS: cannot read %u bytes at /mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/ClamAV/libclamav_nsis_nulsft.c:241\n"
	.size	.L.str.17, 129

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"NSIS: cannot write output file at /mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/ClamAV/libclamav_nsis_nulsft.c:248\n"
	.size	.L.str.18, 133

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"NSIS: decompressor init failed at /mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/ClamAV/libclamav_nsis_nulsft.c:255\n"
	.size	.L.str.19, 133

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"NSIS: cannot write output file at /mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/ClamAV/libclamav_nsis_nulsft.c:270\n"
	.size	.L.str.20, 133

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"NSIS: xs looping, breaking out at /mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/ClamAV/libclamav_nsis_nulsft.c:286\n"
	.size	.L.str.21, 133

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"NSIS: bad stream at /mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/ClamAV/libclamav_nsis_nulsft.c:293\n"
	.size	.L.str.22, 119

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"NSIS: cannot write output file at /mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/ClamAV/libclamav_nsis_nulsft.c:300\n"
	.size	.L.str.23, 133

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"NSIS: decompressor init failed\n"
	.size	.L.str.24, 32

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"NSIS: out of memory\n"
	.size	.L.str.25, 21

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"NSIS: cannot read %u bytes at /mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/ClamAV/libclamav_nsis_nulsft.c:324\n"
	.size	.L.str.26, 129

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"NSIS: xs looping, breaking out at /mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/ClamAV/libclamav_nsis_nulsft.c:344\n"
	.size	.L.str.27, 133

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"NSIS: bad stream at /mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/ClamAV/libclamav_nsis_nulsft.c:351\n"
	.size	.L.str.28, 119

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"NSIS: Breaking out due to filesize limit (%u, max: %lu) in solid archive\n"
	.size	.L.str.29, 74

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"NSIS: xs looping, breaking out at /mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/ClamAV/libclamav_nsis_nulsft.c:378\n"
	.size	.L.str.30, 133

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"NSIS: bad stream at /mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/ClamAV/libclamav_nsis_nulsft.c:391\n"
	.size	.L.str.31, 119

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"NSIS: Header info - Flags=%x, Header size=%x, Archive size=%x\n"
	.size	.L.str.32, 63

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"NSIS: Possibly truncated file\n"
	.size	.L.str.33, 31

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"NSIS: Overlays found\n"
	.size	.L.str.34, 22

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"NSIS: solid compression%s detected\n"
	.size	.L.str.35, 36

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.zero	1
	.size	.L.str.36, 1

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	" not"
	.size	.L.str.37, 5

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"NSIS: bzip2 %u - lzma %u - zlib %u\n"
	.size	.L.str.38, 36


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
