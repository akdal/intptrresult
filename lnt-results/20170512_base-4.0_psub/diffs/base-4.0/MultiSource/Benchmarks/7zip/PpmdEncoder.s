	.text
	.file	"PpmdEncoder.bc"
	.globl	_ZN9NCompress5NPpmd8CEncoderC2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CEncoderC2Ev,@function
_ZN9NCompress5NPpmd8CEncoderC2Ev:       # @_ZN9NCompress5NPpmd8CEncoderC2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$0, 24(%rbx)
	movl	$_ZTVN9NCompress5NPpmd8CEncoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress5NPpmd8CEncoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTVN9NCompress5NPpmd8CEncoderE+160, 16(%rbx)
	movq	$0, 32(%rbx)
	leaq	40(%rbx), %r14
.Ltmp0:
	movq	%r14, %rdi
	callq	_ZN15CByteOutBufWrapC1Ev
.Ltmp1:
# BB#1:
	movl	$16777216, 19320(%rbx)  # imm = 0x1000000
	movb	$6, 19324(%rbx)
	movq	%r14, 128(%rbx)
	addq	$136, %rbx
.Ltmp3:
	movq	%rbx, %rdi
	callq	Ppmd7_Construct
.Ltmp4:
# BB#2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_4:
.Ltmp5:
	movq	%rax, %rbx
.Ltmp6:
	movq	%r14, %rdi
	callq	_ZN15CByteOutBufWrap4FreeEv
.Ltmp7:
# BB#5:                                 # %_ZN15CByteOutBufWrapD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_6:
.Ltmp8:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_3:
.Ltmp2:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_ZN9NCompress5NPpmd8CEncoderC2Ev, .Lfunc_end0-_ZN9NCompress5NPpmd8CEncoderC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	1                       #   On action: 1
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN9NCompress5NPpmd8CEncoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CEncoderD2Ev,@function
_ZN9NCompress5NPpmd8CEncoderD2Ev:       # @_ZN9NCompress5NPpmd8CEncoderD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress5NPpmd8CEncoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress5NPpmd8CEncoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTVN9NCompress5NPpmd8CEncoderE+160, 16(%rbx)
	movq	32(%rbx), %rdi
.Ltmp9:
	callq	MidFree
.Ltmp10:
# BB#1:
	leaq	136(%rbx), %rdi
.Ltmp11:
	movl	$_ZN9NCompress5NPpmdL10g_BigAllocE, %esi
	callq	Ppmd7_Free
.Ltmp12:
# BB#2:
	addq	$40, %rbx
.Ltmp17:
	movq	%rbx, %rdi
	callq	_ZN15CByteOutBufWrap4FreeEv
.Ltmp18:
# BB#3:                                 # %_ZN15CByteOutBufWrapD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB2_5:
.Ltmp19:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_4:
.Ltmp13:
	movq	%rax, %r14
	addq	$40, %rbx
.Ltmp14:
	movq	%rbx, %rdi
	callq	_ZN15CByteOutBufWrap4FreeEv
.Ltmp15:
# BB#6:                                 # %_ZN15CByteOutBufWrapD2Ev.exit5
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_7:
.Ltmp16:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN9NCompress5NPpmd8CEncoderD2Ev, .Lfunc_end2-_ZN9NCompress5NPpmd8CEncoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp12-.Ltmp9          #   Call between .Ltmp9 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin1   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin1   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp14-.Ltmp18         #   Call between .Ltmp18 and .Ltmp14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin1   #     jumps to .Ltmp16
	.byte	1                       #   On action: 1
	.long	.Ltmp15-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end2-.Ltmp15     #   Call between .Ltmp15 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn8_N9NCompress5NPpmd8CEncoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NPpmd8CEncoderD1Ev,@function
_ZThn8_N9NCompress5NPpmd8CEncoderD1Ev:  # @_ZThn8_N9NCompress5NPpmd8CEncoderD1Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %r14, -16
	movl	$_ZTVN9NCompress5NPpmd8CEncoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress5NPpmd8CEncoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rdi)
	movq	$_ZTVN9NCompress5NPpmd8CEncoderE+160, 8(%rdi)
	movq	24(%rdi), %rax
	leaq	-8(%rdi), %rbx
.Ltmp20:
	movq	%rax, %rdi
	callq	MidFree
.Ltmp21:
# BB#1:
	leaq	136(%rbx), %rdi
.Ltmp22:
	movl	$_ZN9NCompress5NPpmdL10g_BigAllocE, %esi
	callq	Ppmd7_Free
.Ltmp23:
# BB#2:
	addq	$40, %rbx
.Ltmp28:
	movq	%rbx, %rdi
	callq	_ZN15CByteOutBufWrap4FreeEv
.Ltmp29:
# BB#3:                                 # %_ZN9NCompress5NPpmd8CEncoderD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB3_5:
.Ltmp30:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_4:
.Ltmp24:
	movq	%rax, %r14
	addq	$40, %rbx
.Ltmp25:
	movq	%rbx, %rdi
	callq	_ZN15CByteOutBufWrap4FreeEv
.Ltmp26:
# BB#6:                                 # %_ZN15CByteOutBufWrapD2Ev.exit5.i
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_7:
.Ltmp27:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZThn8_N9NCompress5NPpmd8CEncoderD1Ev, .Lfunc_end3-_ZThn8_N9NCompress5NPpmd8CEncoderD1Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp20-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp23-.Ltmp20         #   Call between .Ltmp20 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin2   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin2   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp25-.Ltmp29         #   Call between .Ltmp29 and .Ltmp25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin2   #     jumps to .Ltmp27
	.byte	1                       #   On action: 1
	.long	.Ltmp26-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Lfunc_end3-.Ltmp26     #   Call between .Ltmp26 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn16_N9NCompress5NPpmd8CEncoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress5NPpmd8CEncoderD1Ev,@function
_ZThn16_N9NCompress5NPpmd8CEncoderD1Ev: # @_ZThn16_N9NCompress5NPpmd8CEncoderD1Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress5NPpmd8CEncoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress5NPpmd8CEncoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -16(%rbx)
	movq	$_ZTVN9NCompress5NPpmd8CEncoderE+160, (%rbx)
	movq	16(%rbx), %rdi
.Ltmp31:
	callq	MidFree
.Ltmp32:
# BB#1:
	leaq	120(%rbx), %rdi
.Ltmp33:
	movl	$_ZN9NCompress5NPpmdL10g_BigAllocE, %esi
	callq	Ppmd7_Free
.Ltmp34:
# BB#2:
	addq	$24, %rbx
.Ltmp39:
	movq	%rbx, %rdi
	callq	_ZN15CByteOutBufWrap4FreeEv
.Ltmp40:
# BB#3:                                 # %_ZN9NCompress5NPpmd8CEncoderD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB4_5:
.Ltmp41:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB4_4:
.Ltmp35:
	movq	%rax, %r14
	addq	$24, %rbx
.Ltmp36:
	movq	%rbx, %rdi
	callq	_ZN15CByteOutBufWrap4FreeEv
.Ltmp37:
# BB#6:                                 # %_ZN15CByteOutBufWrapD2Ev.exit5.i
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB4_7:
.Ltmp38:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZThn16_N9NCompress5NPpmd8CEncoderD1Ev, .Lfunc_end4-_ZThn16_N9NCompress5NPpmd8CEncoderD1Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp31-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp34-.Ltmp31         #   Call between .Ltmp31 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin3   #     jumps to .Ltmp35
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin3   #     jumps to .Ltmp41
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp36-.Ltmp40         #   Call between .Ltmp40 and .Ltmp36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp37-.Ltmp36         #   Call between .Ltmp36 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin3   #     jumps to .Ltmp38
	.byte	1                       #   On action: 1
	.long	.Ltmp37-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Lfunc_end4-.Ltmp37     #   Call between .Ltmp37 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress5NPpmd8CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CEncoderD0Ev,@function
_ZN9NCompress5NPpmd8CEncoderD0Ev:       # @_ZN9NCompress5NPpmd8CEncoderD0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 32
.Lcfi23:
	.cfi_offset %rbx, -24
.Lcfi24:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress5NPpmd8CEncoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress5NPpmd8CEncoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTVN9NCompress5NPpmd8CEncoderE+160, 16(%rbx)
	movq	32(%rbx), %rdi
.Ltmp42:
	callq	MidFree
.Ltmp43:
# BB#1:
	leaq	136(%rbx), %rdi
.Ltmp44:
	movl	$_ZN9NCompress5NPpmdL10g_BigAllocE, %esi
	callq	Ppmd7_Free
.Ltmp45:
# BB#2:
	leaq	40(%rbx), %rdi
.Ltmp50:
	callq	_ZN15CByteOutBufWrap4FreeEv
.Ltmp51:
# BB#3:                                 # %_ZN9NCompress5NPpmd8CEncoderD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB5_6:
.Ltmp52:
	movq	%rax, %r14
	jmp	.LBB5_7
.LBB5_4:
.Ltmp46:
	movq	%rax, %r14
	leaq	40(%rbx), %rdi
.Ltmp47:
	callq	_ZN15CByteOutBufWrap4FreeEv
.Ltmp48:
.LBB5_7:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_5:
.Ltmp49:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN9NCompress5NPpmd8CEncoderD0Ev, .Lfunc_end5-_ZN9NCompress5NPpmd8CEncoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp42-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp45-.Ltmp42         #   Call between .Ltmp42 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin4   #     jumps to .Ltmp46
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin4   #     jumps to .Ltmp52
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin4   #     jumps to .Ltmp49
	.byte	1                       #   On action: 1
	.long	.Ltmp48-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp48     #   Call between .Ltmp48 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn8_N9NCompress5NPpmd8CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NPpmd8CEncoderD0Ev,@function
_ZThn8_N9NCompress5NPpmd8CEncoderD0Ev:  # @_ZThn8_N9NCompress5NPpmd8CEncoderD0Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress5NPpmd8CEncoderD0Ev # TAILCALL
.Lfunc_end6:
	.size	_ZThn8_N9NCompress5NPpmd8CEncoderD0Ev, .Lfunc_end6-_ZThn8_N9NCompress5NPpmd8CEncoderD0Ev
	.cfi_endproc

	.globl	_ZThn16_N9NCompress5NPpmd8CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress5NPpmd8CEncoderD0Ev,@function
_ZThn16_N9NCompress5NPpmd8CEncoderD0Ev: # @_ZThn16_N9NCompress5NPpmd8CEncoderD0Ev
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress5NPpmd8CEncoderD0Ev # TAILCALL
.Lfunc_end7:
	.size	_ZThn16_N9NCompress5NPpmd8CEncoderD0Ev, .Lfunc_end7-_ZThn16_N9NCompress5NPpmd8CEncoderD0Ev
	.cfi_endproc

	.globl	_ZN9NCompress5NPpmd8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj,@function
_ZN9NCompress5NPpmd8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj: # @_ZN9NCompress5NPpmd8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_startproc
# BB#0:
	testl	%ecx, %ecx
	je	.LBB8_11
# BB#1:                                 # %.lr.ph
	addq	$8, %rdx
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB8_2:                                # =>This Inner Loop Header: Depth=1
	movl	$-2147024809, %eax      # imm = 0x80070057
	movzwl	-8(%rdx), %r9d
	cmpl	$19, %r9d
	jne	.LBB8_12
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	movl	(%rdx), %r9d
	movl	(%rsi,%r8,4), %r10d
	cmpl	$3, %r10d
	je	.LBB8_8
# BB#4:                                 #   in Loop: Header=BB8_2 Depth=1
	cmpl	$2, %r10d
	jne	.LBB8_12
# BB#5:                                 #   in Loop: Header=BB8_2 Depth=1
	leal	-65536(%r9), %r10d
	cmpl	$-65573, %r10d          # imm = 0xFFFEFFDB
	ja	.LBB8_12
# BB#6:                                 #   in Loop: Header=BB8_2 Depth=1
	movl	%r9d, %r10d
	andl	$3, %r10d
	jne	.LBB8_12
# BB#7:                                 #   in Loop: Header=BB8_2 Depth=1
	movl	%r9d, 19320(%rdi)
	jmp	.LBB8_10
	.p2align	4, 0x90
.LBB8_8:                                #   in Loop: Header=BB8_2 Depth=1
	leal	-2(%r9), %r10d
	cmpl	$30, %r10d
	ja	.LBB8_12
# BB#9:                                 #   in Loop: Header=BB8_2 Depth=1
	movb	%r9b, 19324(%rdi)
.LBB8_10:                               #   in Loop: Header=BB8_2 Depth=1
	addq	$16, %rdx
	incq	%r8
	cmpl	%ecx, %r8d
	jb	.LBB8_2
.LBB8_11:
	xorl	%eax, %eax
.LBB8_12:                               # %.thread
	retq
.Lfunc_end8:
	.size	_ZN9NCompress5NPpmd8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj, .Lfunc_end8-_ZN9NCompress5NPpmd8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_endproc

	.globl	_ZThn8_N9NCompress5NPpmd8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NPpmd8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj,@function
_ZThn8_N9NCompress5NPpmd8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj: # @_ZThn8_N9NCompress5NPpmd8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_startproc
# BB#0:
	testl	%ecx, %ecx
	je	.LBB9_11
# BB#1:                                 # %.lr.ph.i
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	movl	%r8d, %r8d
	movq	%r8, %r9
	shlq	$4, %r9
	movl	$-2147024809, %eax      # imm = 0x80070057
	movzwl	(%rdx,%r9), %r10d
	cmpl	$19, %r10d
	jne	.LBB9_12
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	movl	8(%rdx,%r9), %r9d
	movl	(%rsi,%r8,4), %r10d
	cmpl	$3, %r10d
	je	.LBB9_8
# BB#4:                                 #   in Loop: Header=BB9_2 Depth=1
	cmpl	$2, %r10d
	jne	.LBB9_12
# BB#5:                                 #   in Loop: Header=BB9_2 Depth=1
	leal	-65536(%r9), %r10d
	cmpl	$-65573, %r10d          # imm = 0xFFFEFFDB
	ja	.LBB9_12
# BB#6:                                 #   in Loop: Header=BB9_2 Depth=1
	movl	%r9d, %r10d
	andl	$3, %r10d
	jne	.LBB9_12
# BB#7:                                 #   in Loop: Header=BB9_2 Depth=1
	movl	%r9d, 19312(%rdi)
	jmp	.LBB9_10
	.p2align	4, 0x90
.LBB9_8:                                #   in Loop: Header=BB9_2 Depth=1
	leal	-2(%r9), %r10d
	cmpl	$30, %r10d
	ja	.LBB9_12
# BB#9:                                 #   in Loop: Header=BB9_2 Depth=1
	movb	%r9b, 19316(%rdi)
.LBB9_10:                               #   in Loop: Header=BB9_2 Depth=1
	incl	%r8d
	cmpl	%ecx, %r8d
	jb	.LBB9_2
.LBB9_11:
	xorl	%eax, %eax
.LBB9_12:                               # %_ZN9NCompress5NPpmd8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj.exit
	retq
.Lfunc_end9:
	.size	_ZThn8_N9NCompress5NPpmd8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj, .Lfunc_end9-_ZThn8_N9NCompress5NPpmd8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_endproc

	.globl	_ZN9NCompress5NPpmd8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream,@function
_ZN9NCompress5NPpmd8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream: # @_ZN9NCompress5NPpmd8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 16
	movb	19324(%rdi), %al
	movb	%al, 3(%rsp)
	movl	19320(%rdi), %eax
	movl	%eax, 4(%rsp)
	leaq	3(%rsp), %rax
	movl	$5, %edx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	popq	%rcx
	retq
.Lfunc_end10:
	.size	_ZN9NCompress5NPpmd8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream, .Lfunc_end10-_ZN9NCompress5NPpmd8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZThn16_N9NCompress5NPpmd8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress5NPpmd8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream,@function
_ZThn16_N9NCompress5NPpmd8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream: # @_ZThn16_N9NCompress5NPpmd8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 16
	movb	19308(%rdi), %al
	movb	%al, 3(%rsp)
	movl	19304(%rdi), %eax
	movl	%eax, 4(%rsp)
	leaq	3(%rsp), %rax
	movl	$5, %edx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	popq	%rcx
	retq
.Lfunc_end11:
	.size	_ZThn16_N9NCompress5NPpmd8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream, .Lfunc_end11-_ZThn16_N9NCompress5NPpmd8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZN9NCompress5NPpmd8CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo,@function
_ZN9NCompress5NPpmd8CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo: # @_ZN9NCompress5NPpmd8CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 112
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movq	%rdx, %r13
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpq	$0, 32(%rbx)
	jne	.LBB12_3
# BB#1:
	movl	$1048576, %edi          # imm = 0x100000
	callq	MidAlloc
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.LBB12_2
.LBB12_3:
	leaq	40(%rbx), %rbp
	movl	$1048576, %esi          # imm = 0x100000
	movq	%rbp, %rdi
	callq	_ZN15CByteOutBufWrap5AllocEm
	movl	$-2147024882, %r15d     # imm = 0x8007000E
	testb	%al, %al
	je	.LBB12_21
# BB#4:
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	leaq	136(%rbx), %r12
	movl	19320(%rbx), %esi
	movl	$_ZN9NCompress5NPpmdL10g_BigAllocE, %edx
	movq	%r12, %rdi
	callq	Ppmd7_Alloc
	testl	%eax, %eax
	je	.LBB12_21
# BB#5:
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%r13, 80(%rbx)
	movq	64(%rbx), %rax
	movq	%rax, 48(%rbx)
	addq	72(%rbx), %rax
	movq	%rax, 56(%rbx)
	movq	$0, 88(%rbx)
	movl	$0, 96(%rbx)
	leaq	104(%rbx), %r13
	movq	%r13, %rdi
	callq	Ppmd7z_RangeEnc_Init
	movzbl	19324(%rbx), %esi
	movq	%r12, %rdi
	callq	Ppmd7_Init
	movq	$0, 24(%rsp)
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB12_10
# BB#6:
	leaq	12(%rsp), %r14
.LBB12_7:                               # %.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_18 Depth 2
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movq	32(%rbx), %rsi
	movl	$1048576, %edx          # imm = 0x100000
	movq	%r14, %rcx
	callq	*40(%rax)
	movl	%eax, %r15d
	testl	%r15d, %r15d
	jne	.LBB12_21
# BB#8:                                 #   in Loop: Header=BB12_7 Depth=1
	cmpl	$0, 12(%rsp)
	je	.LBB12_16
# BB#9:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB12_7 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB12_18:                              # %.lr.ph
                                        #   Parent Loop BB12_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	32(%rbx), %rax
	movl	%ebp, %ecx
	movzbl	(%rax,%rcx), %edx
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	Ppmd7_EncodeSymbol
	movl	96(%rbx), %r15d
	testl	%r15d, %r15d
	jne	.LBB12_21
# BB#17:                                #   in Loop: Header=BB12_18 Depth=2
	incl	%ebp
	movl	12(%rsp), %eax
	cmpl	%eax, %ebp
	jb	.LBB12_18
# BB#19:                                # %._crit_edge
                                        #   in Loop: Header=BB12_7 Depth=1
	addq	%rax, 24(%rsp)
	movq	48(%rbx), %rax
	addq	88(%rbx), %rax
	subq	64(%rbx), %rax
	movq	%rax, 48(%rsp)
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	leaq	24(%rsp), %rsi
	leaq	48(%rsp), %rdx
	callq	*40(%rax)
	movl	%eax, %r15d
	testl	%r15d, %r15d
	je	.LBB12_7
	jmp	.LBB12_21
.LBB12_2:
	movl	$-2147024882, %r15d     # imm = 0x8007000E
	jmp	.LBB12_21
.LBB12_10:                              # %.split.us.preheader
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movq	32(%rbx), %rsi
	leaq	12(%rsp), %rcx
	movl	$1048576, %edx          # imm = 0x100000
	callq	*40(%rax)
	movl	%eax, %r15d
	testl	%r15d, %r15d
	jne	.LBB12_21
# BB#11:                                # %.lr.ph92.preheader
	leaq	12(%rsp), %r14
.LBB12_12:                              # %.lr.ph92
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_15 Depth 2
	cmpl	$0, 12(%rsp)
	je	.LBB12_16
# BB#13:                                # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB12_12 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB12_15:                              # %.lr.ph.us
                                        #   Parent Loop BB12_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	32(%rbx), %rax
	movl	%ebp, %ecx
	movzbl	(%rax,%rcx), %edx
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	Ppmd7_EncodeSymbol
	movl	96(%rbx), %r15d
	testl	%r15d, %r15d
	jne	.LBB12_21
# BB#14:                                #   in Loop: Header=BB12_15 Depth=2
	incl	%ebp
	movl	12(%rsp), %eax
	cmpl	%eax, %ebp
	jb	.LBB12_15
# BB#20:                                # %.split.us.backedge
                                        #   in Loop: Header=BB12_12 Depth=1
	addq	%rax, 24(%rsp)
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movq	32(%rbx), %rsi
	movl	$1048576, %edx          # imm = 0x100000
	movq	%r14, %rcx
	callq	*40(%rax)
	movl	%eax, %r15d
	testl	%r15d, %r15d
	je	.LBB12_12
	jmp	.LBB12_21
.LBB12_16:                              # %.thread44
	movq	%r13, %rdi
	callq	Ppmd7z_RangeEnc_FlushData
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	_ZN15CByteOutBufWrap5FlushEv
	movl	%eax, %r15d
.LBB12_21:
	movl	%r15d, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN9NCompress5NPpmd8CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo, .Lfunc_end12-_ZN9NCompress5NPpmd8CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_endproc

	.section	.text._ZN9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi40:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB13_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB13_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB13_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB13_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB13_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB13_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB13_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB13_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB13_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB13_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB13_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB13_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB13_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB13_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB13_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB13_16
.LBB13_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_ICompressSetCoderProperties(%rip), %cl
	jne	.LBB13_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+1(%rip), %al
	jne	.LBB13_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+2(%rip), %al
	jne	.LBB13_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+3(%rip), %al
	jne	.LBB13_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+4(%rip), %al
	jne	.LBB13_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+5(%rip), %al
	jne	.LBB13_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+6(%rip), %al
	jne	.LBB13_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+7(%rip), %al
	jne	.LBB13_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+8(%rip), %al
	jne	.LBB13_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+9(%rip), %al
	jne	.LBB13_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+10(%rip), %al
	jne	.LBB13_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+11(%rip), %al
	jne	.LBB13_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+12(%rip), %al
	jne	.LBB13_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+13(%rip), %al
	jne	.LBB13_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+14(%rip), %al
	jne	.LBB13_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit8
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+15(%rip), %al
	jne	.LBB13_33
.LBB13_16:
	leaq	8(%rdi), %rax
	jmp	.LBB13_50
.LBB13_33:                              # %_ZeqRK4GUIDS1_.exit8.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressWriteCoderProperties(%rip), %cl
	jne	.LBB13_51
# BB#34:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+1(%rip), %cl
	jne	.LBB13_51
# BB#35:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+2(%rip), %cl
	jne	.LBB13_51
# BB#36:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+3(%rip), %cl
	jne	.LBB13_51
# BB#37:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+4(%rip), %cl
	jne	.LBB13_51
# BB#38:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+5(%rip), %cl
	jne	.LBB13_51
# BB#39:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+6(%rip), %cl
	jne	.LBB13_51
# BB#40:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+7(%rip), %cl
	jne	.LBB13_51
# BB#41:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+8(%rip), %cl
	jne	.LBB13_51
# BB#42:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+9(%rip), %cl
	jne	.LBB13_51
# BB#43:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+10(%rip), %cl
	jne	.LBB13_51
# BB#44:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+11(%rip), %cl
	jne	.LBB13_51
# BB#45:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+12(%rip), %cl
	jne	.LBB13_51
# BB#46:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+13(%rip), %cl
	jne	.LBB13_51
# BB#47:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+14(%rip), %cl
	jne	.LBB13_51
# BB#48:                                # %_ZeqRK4GUIDS1_.exit10
	movb	15(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+15(%rip), %cl
	jne	.LBB13_51
# BB#49:
	leaq	16(%rdi), %rax
.LBB13_50:                              # %_ZeqRK4GUIDS1_.exit10.thread
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB13_51:                              # %_ZeqRK4GUIDS1_.exit10.thread
	popq	%rcx
	retq
.Lfunc_end13:
	.size	_ZN9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end13-_ZN9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress5NPpmd8CEncoder6AddRefEv,"axG",@progbits,_ZN9NCompress5NPpmd8CEncoder6AddRefEv,comdat
	.weak	_ZN9NCompress5NPpmd8CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CEncoder6AddRefEv,@function
_ZN9NCompress5NPpmd8CEncoder6AddRefEv:  # @_ZN9NCompress5NPpmd8CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	incl	%eax
	movl	%eax, 24(%rdi)
	retq
.Lfunc_end14:
	.size	_ZN9NCompress5NPpmd8CEncoder6AddRefEv, .Lfunc_end14-_ZN9NCompress5NPpmd8CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress5NPpmd8CEncoder7ReleaseEv,"axG",@progbits,_ZN9NCompress5NPpmd8CEncoder7ReleaseEv,comdat
	.weak	_ZN9NCompress5NPpmd8CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CEncoder7ReleaseEv,@function
_ZN9NCompress5NPpmd8CEncoder7ReleaseEv: # @_ZN9NCompress5NPpmd8CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 16
	movl	24(%rdi), %eax
	decl	%eax
	movl	%eax, 24(%rdi)
	jne	.LBB15_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB15_2:
	popq	%rcx
	retq
.Lfunc_end15:
	.size	_ZN9NCompress5NPpmd8CEncoder7ReleaseEv, .Lfunc_end15-_ZN9NCompress5NPpmd8CEncoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end16:
	.size	_ZThn8_N9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end16-_ZThn8_N9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress5NPpmd8CEncoder6AddRefEv,"axG",@progbits,_ZThn8_N9NCompress5NPpmd8CEncoder6AddRefEv,comdat
	.weak	_ZThn8_N9NCompress5NPpmd8CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NPpmd8CEncoder6AddRefEv,@function
_ZThn8_N9NCompress5NPpmd8CEncoder6AddRefEv: # @_ZThn8_N9NCompress5NPpmd8CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end17:
	.size	_ZThn8_N9NCompress5NPpmd8CEncoder6AddRefEv, .Lfunc_end17-_ZThn8_N9NCompress5NPpmd8CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress5NPpmd8CEncoder7ReleaseEv,"axG",@progbits,_ZThn8_N9NCompress5NPpmd8CEncoder7ReleaseEv,comdat
	.weak	_ZThn8_N9NCompress5NPpmd8CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NPpmd8CEncoder7ReleaseEv,@function
_ZThn8_N9NCompress5NPpmd8CEncoder7ReleaseEv: # @_ZThn8_N9NCompress5NPpmd8CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB18_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB18_2:                               # %_ZN9NCompress5NPpmd8CEncoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end18:
	.size	_ZThn8_N9NCompress5NPpmd8CEncoder7ReleaseEv, .Lfunc_end18-_ZThn8_N9NCompress5NPpmd8CEncoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end19:
	.size	_ZThn16_N9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end19-_ZThn16_N9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress5NPpmd8CEncoder6AddRefEv,"axG",@progbits,_ZThn16_N9NCompress5NPpmd8CEncoder6AddRefEv,comdat
	.weak	_ZThn16_N9NCompress5NPpmd8CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress5NPpmd8CEncoder6AddRefEv,@function
_ZThn16_N9NCompress5NPpmd8CEncoder6AddRefEv: # @_ZThn16_N9NCompress5NPpmd8CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end20:
	.size	_ZThn16_N9NCompress5NPpmd8CEncoder6AddRefEv, .Lfunc_end20-_ZThn16_N9NCompress5NPpmd8CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress5NPpmd8CEncoder7ReleaseEv,"axG",@progbits,_ZThn16_N9NCompress5NPpmd8CEncoder7ReleaseEv,comdat
	.weak	_ZThn16_N9NCompress5NPpmd8CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress5NPpmd8CEncoder7ReleaseEv,@function
_ZThn16_N9NCompress5NPpmd8CEncoder7ReleaseEv: # @_ZThn16_N9NCompress5NPpmd8CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB21_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB21_2:                               # %_ZN9NCompress5NPpmd8CEncoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end21:
	.size	_ZThn16_N9NCompress5NPpmd8CEncoder7ReleaseEv, .Lfunc_end21-_ZThn16_N9NCompress5NPpmd8CEncoder7ReleaseEv
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmdL10SzBigAllocEPvm,@function
_ZN9NCompress5NPpmdL10SzBigAllocEPvm:   # @_ZN9NCompress5NPpmdL10SzBigAllocEPvm
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	BigAlloc                # TAILCALL
.Lfunc_end22:
	.size	_ZN9NCompress5NPpmdL10SzBigAllocEPvm, .Lfunc_end22-_ZN9NCompress5NPpmdL10SzBigAllocEPvm
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmdL9SzBigFreeEPvS1_,@function
_ZN9NCompress5NPpmdL9SzBigFreeEPvS1_:   # @_ZN9NCompress5NPpmdL9SzBigFreeEPvS1_
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	BigFree                 # TAILCALL
.Lfunc_end23:
	.size	_ZN9NCompress5NPpmdL9SzBigFreeEPvS1_, .Lfunc_end23-_ZN9NCompress5NPpmdL9SzBigFreeEPvS1_
	.cfi_endproc

	.type	_ZTVN9NCompress5NPpmd8CEncoderE,@object # @_ZTVN9NCompress5NPpmd8CEncoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN9NCompress5NPpmd8CEncoderE
	.p2align	3
_ZTVN9NCompress5NPpmd8CEncoderE:
	.quad	0
	.quad	_ZTIN9NCompress5NPpmd8CEncoderE
	.quad	_ZN9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress5NPpmd8CEncoder6AddRefEv
	.quad	_ZN9NCompress5NPpmd8CEncoder7ReleaseEv
	.quad	_ZN9NCompress5NPpmd8CEncoderD2Ev
	.quad	_ZN9NCompress5NPpmd8CEncoderD0Ev
	.quad	_ZN9NCompress5NPpmd8CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.quad	_ZN9NCompress5NPpmd8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.quad	_ZN9NCompress5NPpmd8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.quad	-8
	.quad	_ZTIN9NCompress5NPpmd8CEncoderE
	.quad	_ZThn8_N9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N9NCompress5NPpmd8CEncoder6AddRefEv
	.quad	_ZThn8_N9NCompress5NPpmd8CEncoder7ReleaseEv
	.quad	_ZThn8_N9NCompress5NPpmd8CEncoderD1Ev
	.quad	_ZThn8_N9NCompress5NPpmd8CEncoderD0Ev
	.quad	_ZThn8_N9NCompress5NPpmd8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.quad	-16
	.quad	_ZTIN9NCompress5NPpmd8CEncoderE
	.quad	_ZThn16_N9NCompress5NPpmd8CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N9NCompress5NPpmd8CEncoder6AddRefEv
	.quad	_ZThn16_N9NCompress5NPpmd8CEncoder7ReleaseEv
	.quad	_ZThn16_N9NCompress5NPpmd8CEncoderD1Ev
	.quad	_ZThn16_N9NCompress5NPpmd8CEncoderD0Ev
	.quad	_ZThn16_N9NCompress5NPpmd8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.size	_ZTVN9NCompress5NPpmd8CEncoderE, 208

	.type	_ZN9NCompress5NPpmdL10g_BigAllocE,@object # @_ZN9NCompress5NPpmdL10g_BigAllocE
	.data
	.p2align	3
_ZN9NCompress5NPpmdL10g_BigAllocE:
	.quad	_ZN9NCompress5NPpmdL10SzBigAllocEPvm
	.quad	_ZN9NCompress5NPpmdL9SzBigFreeEPvS1_
	.size	_ZN9NCompress5NPpmdL10g_BigAllocE, 16

	.type	_ZTSN9NCompress5NPpmd8CEncoderE,@object # @_ZTSN9NCompress5NPpmd8CEncoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTSN9NCompress5NPpmd8CEncoderE
	.p2align	4
_ZTSN9NCompress5NPpmd8CEncoderE:
	.asciz	"N9NCompress5NPpmd8CEncoderE"
	.size	_ZTSN9NCompress5NPpmd8CEncoderE, 28

	.type	_ZTS14ICompressCoder,@object # @_ZTS14ICompressCoder
	.section	.rodata._ZTS14ICompressCoder,"aG",@progbits,_ZTS14ICompressCoder,comdat
	.weak	_ZTS14ICompressCoder
	.p2align	4
_ZTS14ICompressCoder:
	.asciz	"14ICompressCoder"
	.size	_ZTS14ICompressCoder, 17

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI14ICompressCoder,@object # @_ZTI14ICompressCoder
	.section	.rodata._ZTI14ICompressCoder,"aG",@progbits,_ZTI14ICompressCoder,comdat
	.weak	_ZTI14ICompressCoder
	.p2align	4
_ZTI14ICompressCoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ICompressCoder
	.quad	_ZTI8IUnknown
	.size	_ZTI14ICompressCoder, 24

	.type	_ZTS27ICompressSetCoderProperties,@object # @_ZTS27ICompressSetCoderProperties
	.section	.rodata._ZTS27ICompressSetCoderProperties,"aG",@progbits,_ZTS27ICompressSetCoderProperties,comdat
	.weak	_ZTS27ICompressSetCoderProperties
	.p2align	4
_ZTS27ICompressSetCoderProperties:
	.asciz	"27ICompressSetCoderProperties"
	.size	_ZTS27ICompressSetCoderProperties, 30

	.type	_ZTI27ICompressSetCoderProperties,@object # @_ZTI27ICompressSetCoderProperties
	.section	.rodata._ZTI27ICompressSetCoderProperties,"aG",@progbits,_ZTI27ICompressSetCoderProperties,comdat
	.weak	_ZTI27ICompressSetCoderProperties
	.p2align	4
_ZTI27ICompressSetCoderProperties:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS27ICompressSetCoderProperties
	.quad	_ZTI8IUnknown
	.size	_ZTI27ICompressSetCoderProperties, 24

	.type	_ZTS29ICompressWriteCoderProperties,@object # @_ZTS29ICompressWriteCoderProperties
	.section	.rodata._ZTS29ICompressWriteCoderProperties,"aG",@progbits,_ZTS29ICompressWriteCoderProperties,comdat
	.weak	_ZTS29ICompressWriteCoderProperties
	.p2align	4
_ZTS29ICompressWriteCoderProperties:
	.asciz	"29ICompressWriteCoderProperties"
	.size	_ZTS29ICompressWriteCoderProperties, 32

	.type	_ZTI29ICompressWriteCoderProperties,@object # @_ZTI29ICompressWriteCoderProperties
	.section	.rodata._ZTI29ICompressWriteCoderProperties,"aG",@progbits,_ZTI29ICompressWriteCoderProperties,comdat
	.weak	_ZTI29ICompressWriteCoderProperties
	.p2align	4
_ZTI29ICompressWriteCoderProperties:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS29ICompressWriteCoderProperties
	.quad	_ZTI8IUnknown
	.size	_ZTI29ICompressWriteCoderProperties, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN9NCompress5NPpmd8CEncoderE,@object # @_ZTIN9NCompress5NPpmd8CEncoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN9NCompress5NPpmd8CEncoderE
	.p2align	4
_ZTIN9NCompress5NPpmd8CEncoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress5NPpmd8CEncoderE
	.long	1                       # 0x1
	.long	4                       # 0x4
	.quad	_ZTI14ICompressCoder
	.quad	2                       # 0x2
	.quad	_ZTI27ICompressSetCoderProperties
	.quad	2050                    # 0x802
	.quad	_ZTI29ICompressWriteCoderProperties
	.quad	4098                    # 0x1002
	.quad	_ZTI13CMyUnknownImp
	.quad	6146                    # 0x1802
	.size	_ZTIN9NCompress5NPpmd8CEncoderE, 88


	.globl	_ZN9NCompress5NPpmd8CEncoderC1Ev
	.type	_ZN9NCompress5NPpmd8CEncoderC1Ev,@function
_ZN9NCompress5NPpmd8CEncoderC1Ev = _ZN9NCompress5NPpmd8CEncoderC2Ev
	.globl	_ZN9NCompress5NPpmd8CEncoderD1Ev
	.type	_ZN9NCompress5NPpmd8CEncoderD1Ev,@function
_ZN9NCompress5NPpmd8CEncoderD1Ev = _ZN9NCompress5NPpmd8CEncoderD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
