	.text
	.file	"gschar.bc"
	.globl	gs_show_init
	.p2align	4, 0x90
	.type	gs_show_init,@function
gs_show_init:                           # @gs_show_init
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, 16(%rbx)
	movl	$0, 48(%rbx)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	show_setup              # TAILCALL
.Lfunc_end0:
	.size	gs_show_init, .Lfunc_end0-gs_show_init
	.cfi_endproc

	.globl	show_setup
	.p2align	4, 0x90
	.type	show_setup,@function
show_setup:                             # @show_setup
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi9:
	.cfi_def_cfa_offset 64
.Lcfi10:
	.cfi_offset %rbx, -32
.Lcfi11:
	.cfi_offset %r14, -24
.Lcfi12:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	%r14, (%rbx)
	movq	%rdx, 8(%rbx)
	movl	$256, 28(%rbx)          # imm = 0x100
	movl	$0, 40(%rbx)
	movl	$0, 44(%rbx)
	movl	$0, 52(%rbx)
	movl	$0, 312(%rbx)
	movl	$0, 316(%rbx)
	movq	$continue_show, 360(%rbx)
	cmpl	$0, 432(%r14)
	jne	.LBB1_3
# BB#1:
	leaq	336(%r14), %r15
	movq	%r15, %rdi
	callq	gs_make_identity
	movq	328(%r14), %rdi
	addq	$40, %rdi
	leaq	24(%r14), %rsi
	movq	%r15, %rdx
	callq	gs_matrix_multiply
	testl	%eax, %eax
	js	.LBB1_7
# BB#2:
	movl	$1, 432(%r14)
.LBB1_3:
	movq	368(%r14), %rax
	orq	352(%r14), %rax
	addq	%rax, %rax
	je	.LBB1_4
# BB#5:
	movabsq	$9223372036854775807, %rax # imm = 0x7FFFFFFFFFFFFFFF
	movq	384(%r14), %rcx
	orq	336(%r14), %rcx
	xorl	%edx, %edx
	andq	%rax, %rcx
	sete	%dl
	movl	%edx, 56(%rbx)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	jne	.LBB1_7
	jmp	.LBB1_6
.LBB1_4:                                # %.thread
	movl	$1, 56(%rbx)
.LBB1_6:
	movq	264(%r14), %rdi
	movq	%rsp, %rsi
	callq	gx_cpath_box_for_check
	movl	$4095, %eax             # imm = 0xFFF
	movq	(%rsp), %rcx
	addq	%rax, %rcx
	shrq	$12, %rcx
	movl	%ecx, 60(%rbx)
	addq	8(%rsp), %rax
	shrq	$12, %rax
	movl	%eax, 64(%rbx)
	movq	16(%rsp), %rax
	shrq	$12, %rax
	movl	%eax, 68(%rbx)
	movq	24(%rsp), %rax
	shrq	$12, %rax
	movl	%eax, 72(%rbx)
	xorl	%eax, %eax
.LBB1_7:
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	show_setup, .Lfunc_end1-show_setup
	.cfi_endproc

	.globl	gs_show_n_init
	.p2align	4, 0x90
	.type	gs_show_n_init,@function
gs_show_n_init:                         # @gs_show_n_init
	.cfi_startproc
# BB#0:
	movl	%ecx, 16(%rdi)
	movl	$0, 48(%rdi)
	jmp	show_setup              # TAILCALL
.Lfunc_end2:
	.size	gs_show_n_init, .Lfunc_end2-gs_show_n_init
	.cfi_endproc

	.globl	gs_ashow_init
	.p2align	4, 0x90
	.type	gs_ashow_init,@function
gs_ashow_init:                          # @gs_ashow_init
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 48
.Lcfi17:
	.cfi_offset %rbx, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, 16(%rbx)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	show_setup
	movl	$1, 40(%rbx)
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 32(%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 36(%rbx)
	movl	$1, 48(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	gs_ashow_init, .Lfunc_end3-gs_ashow_init
	.cfi_endproc

	.globl	gs_ashow_n_init
	.p2align	4, 0x90
	.type	gs_ashow_n_init,@function
gs_ashow_n_init:                        # @gs_ashow_n_init
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -16
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%rdi, %rbx
	movl	%ecx, 16(%rbx)
	callq	show_setup
	movl	$1, 40(%rbx)
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 32(%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 36(%rbx)
	movl	$1, 48(%rbx)
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end4:
	.size	gs_ashow_n_init, .Lfunc_end4-gs_ashow_n_init
	.cfi_endproc

	.globl	gs_widthshow_init
	.p2align	4, 0x90
	.type	gs_widthshow_init,@function
gs_widthshow_init:                      # @gs_widthshow_init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 64
.Lcfi28:
	.cfi_offset %rbx, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movl	%edx, %r14d
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, 16(%rbp)
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	show_setup
	movl	%r14d, 28(%rbp)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 20(%rbp)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 24(%rbp)
	movl	$1, 48(%rbp)
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	gs_widthshow_init, .Lfunc_end5-gs_widthshow_init
	.cfi_endproc

	.globl	gs_widthshow_n_init
	.p2align	4, 0x90
	.type	gs_widthshow_n_init,@function
gs_widthshow_n_init:                    # @gs_widthshow_n_init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 48
.Lcfi35:
	.cfi_offset %rbx, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	movl	%r8d, 16(%rbx)
	movq	%rcx, %rdx
	callq	show_setup
	movl	%ebp, 28(%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 20(%rbx)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 24(%rbx)
	movl	$1, 48(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end6:
	.size	gs_widthshow_n_init, .Lfunc_end6-gs_widthshow_n_init
	.cfi_endproc

	.globl	gs_awidthshow_init
	.p2align	4, 0x90
	.type	gs_awidthshow_init,@function
gs_awidthshow_init:                     # @gs_awidthshow_init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi41:
	.cfi_def_cfa_offset 80
.Lcfi42:
	.cfi_offset %rbx, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movsd	%xmm3, 24(%rsp)         # 8-byte Spill
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	movl	%edx, %r14d
	movsd	%xmm1, 32(%rsp)         # 8-byte Spill
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, 16(%rbp)
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	show_setup
	movl	$1, 40(%rbp)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 32(%rbp)
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 36(%rbp)
	movl	%r14d, 28(%rbp)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 20(%rbp)
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 24(%rbp)
	movl	$1, 48(%rbp)
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	gs_awidthshow_init, .Lfunc_end7-gs_awidthshow_init
	.cfi_endproc

	.globl	gs_awidthshow_n_init
	.p2align	4, 0x90
	.type	gs_awidthshow_n_init,@function
gs_awidthshow_n_init:                   # @gs_awidthshow_n_init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi48:
	.cfi_def_cfa_offset 64
.Lcfi49:
	.cfi_offset %rbx, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movsd	%xmm3, 24(%rsp)         # 8-byte Spill
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	movl	%edx, %ebp
	movsd	%xmm1, 32(%rsp)         # 8-byte Spill
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movq	%rdi, %rbx
	movl	%r8d, 16(%rbx)
	movq	%rcx, %rdx
	callq	show_setup
	movl	$1, 40(%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 32(%rbx)
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 36(%rbx)
	movl	%ebp, 28(%rbx)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 20(%rbx)
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 24(%rbx)
	movl	$1, 48(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end8:
	.size	gs_awidthshow_n_init, .Lfunc_end8-gs_awidthshow_n_init
	.cfi_endproc

	.globl	gs_kshow_init
	.p2align	4, 0x90
	.type	gs_kshow_init,@function
gs_kshow_init:                          # @gs_kshow_init
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 32
.Lcfi54:
	.cfi_offset %rbx, -32
.Lcfi55:
	.cfi_offset %r14, -24
.Lcfi56:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	%r15, %rdi
	callq	strlen
	movl	%eax, 16(%rbx)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	show_setup
	movabsq	$4294967297, %rcx       # imm = 0x100000001
	movq	%rcx, 44(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	gs_kshow_init, .Lfunc_end9-gs_kshow_init
	.cfi_endproc

	.globl	gs_kshow_n_init
	.p2align	4, 0x90
	.type	gs_kshow_n_init,@function
gs_kshow_n_init:                        # @gs_kshow_n_init
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 16
.Lcfi58:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	%ecx, 16(%rbx)
	callq	show_setup
	movabsq	$4294967297, %rcx       # imm = 0x100000001
	movq	%rcx, 44(%rbx)
	popq	%rbx
	retq
.Lfunc_end10:
	.size	gs_kshow_n_init, .Lfunc_end10-gs_kshow_n_init
	.cfi_endproc

	.globl	gs_stringwidth_init
	.p2align	4, 0x90
	.type	gs_stringwidth_init,@function
gs_stringwidth_init:                    # @gs_stringwidth_init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi62:
	.cfi_def_cfa_offset 48
.Lcfi63:
	.cfi_offset %rbx, -32
.Lcfi64:
	.cfi_offset %r14, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	%rbp, %rdi
	callq	strlen
	movl	%eax, 16(%rbx)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%rbp, %rdx
	callq	show_setup
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB11_4
# BB#1:
	movq	$continue_stringwidth, 360(%rbx)
	movq	%r14, %rdi
	callq	gs_gsave
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB11_4
# BB#2:
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	gx_device_no_output
	movq	256(%r14), %rdi
	movq	%rsp, %rsi
	callq	gx_path_current_point
	testl	%eax, %eax
	jns	.LBB11_4
# BB#3:
	movq	256(%r14), %rdi
	movq	120(%r14), %rsi
	movq	128(%r14), %rdx
	callq	gx_path_add_point
.LBB11_4:                               # %stringwidth_setup.exit
	movl	%ebp, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end11:
	.size	gs_stringwidth_init, .Lfunc_end11-gs_stringwidth_init
	.cfi_endproc

	.globl	stringwidth_setup
	.p2align	4, 0x90
	.type	stringwidth_setup,@function
stringwidth_setup:                      # @stringwidth_setup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi69:
	.cfi_def_cfa_offset 48
.Lcfi70:
	.cfi_offset %rbx, -32
.Lcfi71:
	.cfi_offset %r14, -24
.Lcfi72:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	show_setup
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB12_4
# BB#1:
	movq	$continue_stringwidth, 360(%rbx)
	movq	%r14, %rdi
	callq	gs_gsave
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB12_4
# BB#2:
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	gx_device_no_output
	movq	256(%r14), %rdi
	movq	%rsp, %rsi
	callq	gx_path_current_point
	testl	%eax, %eax
	jns	.LBB12_4
# BB#3:
	movq	256(%r14), %rdi
	movq	120(%r14), %rsi
	movq	128(%r14), %rdx
	callq	gx_path_add_point
.LBB12_4:
	movl	%ebp, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end12:
	.size	stringwidth_setup, .Lfunc_end12-stringwidth_setup
	.cfi_endproc

	.globl	gs_stringwidth_n_init
	.p2align	4, 0x90
	.type	gs_stringwidth_n_init,@function
gs_stringwidth_n_init:                  # @gs_stringwidth_n_init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi76:
	.cfi_def_cfa_offset 48
.Lcfi77:
	.cfi_offset %rbx, -32
.Lcfi78:
	.cfi_offset %r14, -24
.Lcfi79:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	%ecx, 16(%rbx)
	callq	show_setup
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB13_4
# BB#1:
	movq	$continue_stringwidth, 360(%rbx)
	movq	%r14, %rdi
	callq	gs_gsave
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB13_4
# BB#2:
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	gx_device_no_output
	movq	256(%r14), %rdi
	movq	%rsp, %rsi
	callq	gx_path_current_point
	testl	%eax, %eax
	jns	.LBB13_4
# BB#3:
	movq	256(%r14), %rdi
	movq	120(%r14), %rsi
	movq	128(%r14), %rdx
	callq	gx_path_add_point
.LBB13_4:                               # %stringwidth_setup.exit
	movl	%ebp, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end13:
	.size	gs_stringwidth_n_init, .Lfunc_end13-gs_stringwidth_n_init
	.cfi_endproc

	.globl	continue_stringwidth
	.p2align	4, 0x90
	.type	continue_stringwidth,@function
continue_stringwidth:                   # @continue_stringwidth
	.cfi_startproc
# BB#0:
	jmp	stringwidth_proceed     # TAILCALL
.Lfunc_end14:
	.size	continue_stringwidth, .Lfunc_end14-continue_stringwidth
	.cfi_endproc

	.globl	gs_charpath_init
	.p2align	4, 0x90
	.type	gs_charpath_init,@function
gs_charpath_init:                       # @gs_charpath_init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi81:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi84:
	.cfi_def_cfa_offset 48
.Lcfi85:
	.cfi_offset %rbx, -40
.Lcfi86:
	.cfi_offset %r14, -32
.Lcfi87:
	.cfi_offset %r15, -24
.Lcfi88:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, 16(%rbp)
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	show_setup
	cmpl	$1, %r14d
	movl	$1, %ecx
	sbbl	$-1, %ecx
	movl	%ecx, 52(%rbp)
	movl	$0, 56(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	gs_charpath_init, .Lfunc_end15-gs_charpath_init
	.cfi_endproc

	.globl	gs_charpath_n_init
	.p2align	4, 0x90
	.type	gs_charpath_n_init,@function
gs_charpath_n_init:                     # @gs_charpath_n_init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi91:
	.cfi_def_cfa_offset 32
.Lcfi92:
	.cfi_offset %rbx, -24
.Lcfi93:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebp
	movq	%rdi, %rbx
	movl	%ecx, 16(%rbx)
	callq	show_setup
	cmpl	$1, %ebp
	movl	$1, %ecx
	sbbl	$-1, %ecx
	movl	%ecx, 52(%rbx)
	movl	$0, 56(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end16:
	.size	gs_charpath_n_init, .Lfunc_end16-gs_charpath_n_init
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI17_0:
	.quad	1                       # 0x1
	.quad	1                       # 0x1
.LCPI17_1:
	.quad	2048                    # 0x800
	.quad	2048                    # 0x800
.LCPI17_2:
	.quad	-4096                   # 0xfffffffffffff000
	.quad	-4096                   # 0xfffffffffffff000
	.text
	.globl	gs_setcachedevice
	.p2align	4, 0x90
	.type	gs_setcachedevice,@function
gs_setcachedevice:                      # @gs_setcachedevice
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi94:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi95:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi96:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi97:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi98:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi100:
	.cfi_def_cfa_offset 128
.Lcfi101:
	.cfi_offset %rbx, -56
.Lcfi102:
	.cfi_offset %r12, -48
.Lcfi103:
	.cfi_offset %r13, -40
.Lcfi104:
	.cfi_offset %r14, -32
.Lcfi105:
	.cfi_offset %r15, -24
.Lcfi106:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$-21, %r12d
	cmpl	$0, 352(%rbx)
	jne	.LBB17_18
# BB#1:
	movsd	%xmm2, 64(%rsp)         # 8-byte Spill
	movsd	%xmm3, 24(%rsp)         # 8-byte Spill
	movsd	%xmm4, 8(%rsp)          # 8-byte Spill
	movsd	%xmm5, (%rsp)           # 8-byte Spill
	movq	(%rbx), %rdi
	addq	$24, %rdi
	leaq	320(%rbx), %r15
	movq	%r15, %rsi
	callq	gs_distance_transform2fixed
	movl	$2, 352(%rbx)
	movq	(%rbx), %r14
	xorl	%r12d, %r12d
	cmpb	$0, 436(%r14)
	je	.LBB17_2
.LBB17_18:                              # %gs_setcharwidth.exit.thread
	movl	%r12d, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_2:
	movb	$1, 436(%r14)
	cmpl	$0, 56(%rbx)
	je	.LBB17_18
# BB#3:
	movsd	(%rsp), %xmm2           # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movq	24(%rsp), %xmm1         # 8-byte Folded Reload
                                        # xmm1 = mem[0],zero
	ucomisd	%xmm2, %xmm1
	ja	.LBB17_18
# BB#4:
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movq	64(%rsp), %xmm0         # 8-byte Folded Reload
                                        # xmm0 = mem[0],zero
	ucomisd	%xmm3, %xmm0
	ja	.LBB17_18
# BB#5:
	movl	432(%r14), %eax
	testl	%eax, %eax
	je	.LBB17_18
# BB#6:
	movq	328(%r14), %rax
	movq	24(%rax), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	24(%r14), %rbp
	leaq	48(%rsp), %rsi
	movq	%rbp, %rdi
	callq	gs_distance_transform2fixed
	leaq	32(%rsp), %rsi
	movq	%rbp, %rdi
	movq	8(%rsp), %xmm0          # 8-byte Folded Reload
                                        # xmm0 = mem[0],zero
	movq	(%rsp), %xmm1           # 8-byte Folded Reload
                                        # xmm1 = mem[0],zero
	callq	gs_distance_transform2fixed
	movq	32(%rsp), %rax
	movq	40(%rsp), %rcx
	subq	48(%rsp), %rax
	movq	%rax, %r13
	negq	%r13
	cmovlq	%rax, %r13
	subq	56(%rsp), %rcx
	movq	%rcx, %rbp
	negq	%rbp
	cmovlq	%rcx, %rbp
	sarq	$12, %r13
	addq	$2, %r13
	movzwl	%r13w, %edx
	xorl	%r12d, %r12d
	cmpq	%rdx, %r13
	jne	.LBB17_18
# BB#7:
	sarq	$12, %rbp
	addq	$2, %rbp
	movzwl	%bp, %ecx
	cmpq	%rcx, %rbp
	jne	.LBB17_18
# BB#8:
	cmpl	$0, 312(%rbx)
	je	.LBB17_10
# BB#9:                                 # %._crit_edge
	leaq	80(%rbx), %rsi
	jmp	.LBB17_11
.LBB17_10:
	leaq	80(%rbx), %rdi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movl	$mem_mono_device, %esi
	movq	%rdx, (%rsp)            # 8-byte Spill
	movl	$200, %edx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	callq	memcpy
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	%rsi, 280(%rbx)
	movl	$0, 288(%rbx)
	movdqa	.LCPI17_0(%rip), %xmm0  # xmm0 = [1,1]
	movdqu	%xmm0, 296(%rbx)
	movl	$1, 312(%rbx)
.LBB17_11:
	movq	16(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	gx_alloc_char_bits
	movq	%rax, %rdi
	testq	%rdi, %rdi
	je	.LBB17_18
# BB#12:
	movq	48(%rsp), %rax
	movq	56(%rsp), %rcx
	cmpq	32(%rsp), %rax
	leaq	32(%rsp), %rax
	movq	%rax, %rsi
	leaq	48(%rsp), %rdx
	cmovlq	%rdx, %rax
	cmpq	40(%rsp), %rcx
	cmovlq	%rdx, %rsi
	movq	8(%rsi), %xmm0          # xmm0 = mem[0],zero
	movq	(%rax), %xmm1           # xmm1 = mem[0],zero
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	.LCPI17_1(%rip), %xmm0  # xmm0 = [2048,2048]
	psubq	%xmm1, %xmm0
	pand	.LCPI17_2(%rip), %xmm0
	movdqu	%xmm0, 48(%rdi)
	movq	%rdi, (%rsp)            # 8-byte Spill
	movq	304(%r14), %rdi
	movq	312(%r14), %rsi
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	gx_color_render
	movq	312(%r14), %rax
	cmpl	$0, 16(%rax)
	je	.LBB17_14
.LBB17_13:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	gx_unalloc_cached_char
	jmp	.LBB17_18
.LBB17_14:
	movq	%r14, %rdi
	callq	gs_gsave
	movl	%eax, %r12d
	testl	%r12d, %r12d
	js	.LBB17_13
# BB#15:
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	%rdx, 336(%rbx)
	movq	8(%rbx), %rax
	movl	316(%rbx), %ecx
	decl	%ecx
	movzbl	(%rax,%rcx), %eax
	movl	%eax, 8(%rdx)
	movdqu	(%r15), %xmm0
	movdqu	%xmm0, 32(%rdx)
	leaq	280(%rbx), %rax
	movq	%rax, 448(%r14)
	movl	$1, 456(%r14)
	movq	48(%rdx), %rsi
	movq	56(%rdx), %rdx
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	gs_translate_to_fixed
	shlq	$12, %r13
	shlq	$12, %rbp
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r13, %rcx
	movq	%rbp, %r8
	callq	gx_clip_to_rectangle
	testl	%eax, %eax
	js	.LBB17_16
# BB#17:
	movb	$0, 436(%r14)
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	callq	gs_setgray
	movb	$1, 436(%r14)
	movl	$1, 352(%rbx)
	jmp	.LBB17_18
.LBB17_16:
	movl	%eax, %r12d
	jmp	.LBB17_18
.Lfunc_end17:
	.size	gs_setcachedevice, .Lfunc_end17-gs_setcachedevice
	.cfi_endproc

	.globl	gs_setcharwidth
	.p2align	4, 0x90
	.type	gs_setcharwidth,@function
gs_setcharwidth:                        # @gs_setcharwidth
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi107:
	.cfi_def_cfa_offset 16
.Lcfi108:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$-21, %eax
	cmpl	$0, 352(%rbx)
	jne	.LBB18_2
# BB#1:
	movq	(%rbx), %rdi
	addq	$24, %rdi
	leaq	320(%rbx), %rsi
	callq	gs_distance_transform2fixed
	movl	$2, 352(%rbx)
	xorl	%eax, %eax
.LBB18_2:
	popq	%rbx
	retq
.Lfunc_end18:
	.size	gs_setcharwidth, .Lfunc_end18-gs_setcharwidth
	.cfi_endproc

	.globl	gs_show_current_char
	.p2align	4, 0x90
	.type	gs_show_current_char,@function
gs_show_current_char:                   # @gs_show_current_char
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	movl	316(%rdi), %ecx
	decl	%ecx
	movb	(%rax,%rcx), %al
	retq
.Lfunc_end19:
	.size	gs_show_current_char, .Lfunc_end19-gs_show_current_char
	.cfi_endproc

	.globl	gs_show_next
	.p2align	4, 0x90
	.type	gs_show_next,@function
gs_show_next:                           # @gs_show_next
	.cfi_startproc
# BB#0:
	jmpq	*360(%rdi)              # TAILCALL
.Lfunc_end20:
	.size	gs_show_next, .Lfunc_end20-gs_show_next
	.cfi_endproc

	.globl	continue_show_update
	.p2align	4, 0x90
	.type	continue_show_update,@function
continue_show_update:                   # @continue_show_update
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi111:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi112:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi113:
	.cfi_def_cfa_offset 48
.Lcfi114:
	.cfi_offset %rbx, -40
.Lcfi115:
	.cfi_offset %r12, -32
.Lcfi116:
	.cfi_offset %r14, -24
.Lcfi117:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	352(%rbx), %eax
	testl	%eax, %eax
	je	.LBB21_1
# BB#2:
	movq	(%rbx), %r14
	cmpl	$1, %eax
	jne	.LBB21_4
# BB#3:
	movq	336(%rbx), %r15
	movq	328(%r14), %rax
	movq	24(%rax), %r12
	movq	%r14, %rdi
	callq	gx_lookup_fm_pair
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	gx_add_cached_char
	movq	%r14, %rdi
	callq	gs_grestore
	movl	$0, 356(%rbx)
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	gx_copy_cached_char
	testl	%eax, %eax
	js	.LBB21_13
.LBB21_4:
	movq	%r14, %rdi
	callq	gs_grestore
	movq	(%rbx), %r14
	cmpl	$0, 40(%rbx)
	je	.LBB21_6
# BB#5:
	movss	32(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	36(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movb	$2, %al
	movq	%r14, %rdi
	callq	gs_rmoveto
.LBB21_6:
	movq	8(%rbx), %rax
	movl	316(%rbx), %ecx
	decl	%ecx
	movzbl	(%rax,%rcx), %eax
	cmpl	28(%rbx), %eax
	jne	.LBB21_8
# BB#7:
	movss	20(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movb	$2, %al
	movq	%r14, %rdi
	callq	gs_rmoveto
.LBB21_8:
	movq	256(%r14), %rax
	cmpb	$0, 136(%rax)
	je	.LBB21_9
# BB#10:
	movdqu	320(%rbx), %xmm0
	movdqu	120(%rax), %xmm1
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 120(%rax)
	movb	$0, 137(%rax)
	cmpl	$0, 44(%rbx)
	je	.LBB21_14
# BB#11:
	movl	316(%rbx), %eax
	cmpl	16(%rbx), %eax
	jae	.LBB21_14
# BB#12:
	movq	$continue_show, 360(%rbx)
	movl	$2, %eax
	jmp	.LBB21_13
.LBB21_1:
	movl	$-23, %eax
	jmp	.LBB21_13
.LBB21_14:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	show_proceed            # TAILCALL
.LBB21_9:
	movl	$-14, %eax
.LBB21_13:                              # %show_move.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end21:
	.size	continue_show_update, .Lfunc_end21-continue_show_update
	.cfi_endproc

	.globl	show_update
	.p2align	4, 0x90
	.type	show_update,@function
show_update:                            # @show_update
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi118:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi119:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi120:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi121:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi122:
	.cfi_def_cfa_offset 48
.Lcfi123:
	.cfi_offset %rbx, -40
.Lcfi124:
	.cfi_offset %r12, -32
.Lcfi125:
	.cfi_offset %r14, -24
.Lcfi126:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	352(%rbx), %eax
	testl	%eax, %eax
	je	.LBB22_1
# BB#2:
	movq	(%rbx), %r14
	cmpl	$1, %eax
	jne	.LBB22_4
# BB#3:
	movq	336(%rbx), %r15
	movq	328(%r14), %rax
	movq	24(%rax), %r12
	movq	%r14, %rdi
	callq	gx_lookup_fm_pair
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	gx_add_cached_char
	movq	%r14, %rdi
	callq	gs_grestore
	movl	$0, 356(%rbx)
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	gx_copy_cached_char
	testl	%eax, %eax
	js	.LBB22_5
.LBB22_4:
	movq	%r14, %rdi
	callq	gs_grestore
	xorl	%eax, %eax
	jmp	.LBB22_5
.LBB22_1:
	movl	$-23, %eax
.LBB22_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end22:
	.size	show_update, .Lfunc_end22-show_update
	.cfi_endproc

	.globl	show_move
	.p2align	4, 0x90
	.type	show_move,@function
show_move:                              # @show_move
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi127:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi128:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi129:
	.cfi_def_cfa_offset 32
.Lcfi130:
	.cfi_offset %rbx, -24
.Lcfi131:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	(%rbx), %r14
	cmpl	$0, 40(%rbx)
	je	.LBB23_2
# BB#1:
	movss	32(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	36(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movb	$2, %al
	movq	%r14, %rdi
	callq	gs_rmoveto
.LBB23_2:
	movq	8(%rbx), %rax
	movl	316(%rbx), %ecx
	decl	%ecx
	movzbl	(%rax,%rcx), %eax
	cmpl	28(%rbx), %eax
	jne	.LBB23_4
# BB#3:
	movss	20(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movb	$2, %al
	movq	%r14, %rdi
	callq	gs_rmoveto
.LBB23_4:
	movq	256(%r14), %rax
	cmpb	$0, 136(%rax)
	je	.LBB23_5
# BB#6:
	movdqu	320(%rbx), %xmm0
	movdqu	120(%rax), %xmm1
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 120(%rax)
	movb	$0, 137(%rax)
	xorl	%eax, %eax
	cmpl	$0, 44(%rbx)
	je	.LBB23_9
# BB#7:
	movl	316(%rbx), %ecx
	cmpl	16(%rbx), %ecx
	jae	.LBB23_9
# BB#8:
	movq	$continue_show, 360(%rbx)
	movl	$2, %eax
	jmp	.LBB23_9
.LBB23_5:
	movl	$-14, %eax
.LBB23_9:                               # %.thread29
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end23:
	.size	show_move, .Lfunc_end23-show_move
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI24_0:
	.quad	2048                    # 0x800
	.quad	2048                    # 0x800
.LCPI24_1:
	.quad	-4096                   # 0xfffffffffffff000
	.quad	-4096                   # 0xfffffffffffff000
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI24_2:
	.quad	4553139223271571456     # double 2.44140625E-4
	.text
	.globl	show_proceed
	.p2align	4, 0x90
	.type	show_proceed,@function
show_proceed:                           # @show_proceed
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi132:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi133:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi134:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi135:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi136:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi137:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi138:
	.cfi_def_cfa_offset 96
.Lcfi139:
	.cfi_offset %rbx, -56
.Lcfi140:
	.cfi_offset %r12, -48
.Lcfi141:
	.cfi_offset %r13, -40
.Lcfi142:
	.cfi_offset %r14, -32
.Lcfi143:
	.cfi_offset %r15, -24
.Lcfi144:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	(%rbx), %r14
	movq	8(%rbx), %r15
	movl	$0, 356(%rbx)
	leaq	336(%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	320(%rbx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%r15, 16(%rsp)          # 8-byte Spill
.LBB24_1:                               # %.thread134
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_6 Depth 2
	cmpl	$0, 56(%rbx)
	je	.LBB24_19
# BB#2:                                 #   in Loop: Header=BB24_1 Depth=1
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	jne	.LBB24_4
# BB#3:                                 #   in Loop: Header=BB24_1 Depth=1
	movq	%r14, %rdi
	callq	gx_lookup_fm_pair
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB24_4:                               # %.thread116.preheader
                                        #   in Loop: Header=BB24_1 Depth=1
	movl	316(%rbx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 316(%rbx)
	xorl	%r12d, %r12d
	cmpl	16(%rbx), %eax
	je	.LBB24_47
	.p2align	4, 0x90
.LBB24_6:                               # %.lr.ph
                                        #   Parent Loop BB24_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %eax
	movzbl	(%r15,%rax), %r13d
	movzbl	%r13b, %edx
	movq	%r14, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	gx_lookup_cached_char
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB24_21
# BB#7:                                 #   in Loop: Header=BB24_6 Depth=2
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	gx_copy_cached_char
	testl	%eax, %eax
	js	.LBB24_46
# BB#8:                                 #   in Loop: Header=BB24_6 Depth=2
	jne	.LBB24_21
# BB#9:                                 #   in Loop: Header=BB24_6 Depth=2
	cmpl	$0, 48(%rbx)
	je	.LBB24_17
# BB#10:                                #   in Loop: Header=BB24_6 Depth=2
	movdqu	32(%rbp), %xmm0
	movq	32(%rsp), %rax          # 8-byte Reload
	movdqu	%xmm0, (%rax)
	movq	(%rbx), %rbp
	cmpl	$0, 40(%rbx)
	je	.LBB24_12
# BB#11:                                #   in Loop: Header=BB24_6 Depth=2
	movss	32(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	36(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movb	$2, %al
	movq	%rbp, %rdi
	callq	gs_rmoveto
.LBB24_12:                              #   in Loop: Header=BB24_6 Depth=2
	movq	8(%rbx), %rax
	movl	316(%rbx), %ecx
	decl	%ecx
	movzbl	(%rax,%rcx), %eax
	cmpl	28(%rbx), %eax
	jne	.LBB24_14
# BB#13:                                #   in Loop: Header=BB24_6 Depth=2
	movss	20(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movb	$2, %al
	movq	%rbp, %rdi
	callq	gs_rmoveto
.LBB24_14:                              #   in Loop: Header=BB24_6 Depth=2
	movq	256(%rbp), %rax
	cmpb	$0, 136(%rax)
	je	.LBB24_41
# BB#15:                                #   in Loop: Header=BB24_6 Depth=2
	movdqu	320(%rbx), %xmm0
	movdqu	120(%rax), %xmm1
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 120(%rax)
	movb	$0, 137(%rax)
	cmpl	$0, 44(%rbx)
	je	.LBB24_5
# BB#16:                                #   in Loop: Header=BB24_6 Depth=2
	movl	316(%rbx), %eax
	cmpl	16(%rbx), %eax
	jae	.LBB24_5
	jmp	.LBB24_38
	.p2align	4, 0x90
.LBB24_17:                              #   in Loop: Header=BB24_6 Depth=2
	movq	256(%r14), %rax
	cmpb	$0, 136(%rax)
	je	.LBB24_41
# BB#18:                                #   in Loop: Header=BB24_6 Depth=2
	movdqu	32(%rbp), %xmm0
	movdqu	120(%rax), %xmm1
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 120(%rax)
	movb	$0, 137(%rax)
.LBB24_5:                               # %.thread116.backedge
                                        #   in Loop: Header=BB24_6 Depth=2
	movl	316(%rbx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 316(%rbx)
	cmpl	16(%rbx), %eax
	jne	.LBB24_6
	jmp	.LBB24_47
	.p2align	4, 0x90
.LBB24_19:                              #   in Loop: Header=BB24_1 Depth=1
	movl	316(%rbx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 316(%rbx)
	cmpl	16(%rbx), %eax
	je	.LBB24_45
# BB#20:                                #   in Loop: Header=BB24_1 Depth=1
	movb	(%r15,%rax), %r13b
.LBB24_21:                              # %.thread
                                        #   in Loop: Header=BB24_1 Depth=1
	movq	%r14, %rdi
	callq	gs_gsave
	movl	%eax, %r12d
	testl	%r12d, %r12d
	js	.LBB24_47
# BB#22:                                #   in Loop: Header=BB24_1 Depth=1
	movb	52(%rbx), %al
	movb	%al, 437(%r14)
	movq	256(%r14), %rbp
	movl	$-14, %r12d
	cmpb	$0, 136(%rbp)
	je	.LBB24_47
# BB#23:                                #   in Loop: Header=BB24_1 Depth=1
	movq	120(%rbp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	128(%rbp), %r15
	movq	%rbp, %rdi
	callq	gx_path_is_void
	testl	%eax, %eax
	jne	.LBB24_26
# BB#24:                                #   in Loop: Header=BB24_1 Depth=1
	cmpl	$0, 52(%rbx)
	jne	.LBB24_26
# BB#25:                                #   in Loop: Header=BB24_1 Depth=1
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	gs_newpath
	movq	%rbp, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r15, %rdx
	callq	gx_path_add_point
	.p2align	4, 0x90
.LBB24_26:                              #   in Loop: Header=BB24_1 Depth=1
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	gs_setmatrix
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r15, %rdx
	callq	gs_translate_to_fixed
	movdqu	120(%r14), %xmm0
	paddq	.LCPI24_0(%rip), %xmm0
	pand	.LCPI24_1(%rip), %xmm0
	movd	%xmm0, %rax
	cvtsi2sdq	%rax, %xmm1
	movsd	.LCPI24_2(%rip), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movd	%xmm1, 88(%r14)
	movdqu	%xmm0, 120(%r14)
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	mulsd	%xmm2, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movd	%xmm0, 104(%r14)
	movl	$0, 352(%rbx)
	movq	$continue_show_update, 360(%rbx)
	movq	328(%r14), %rdx
	movzbl	%r13b, %ecx
	movq	176(%rdx), %r8
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*168(%rdx)
	testl	%eax, %eax
	js	.LBB24_46
# BB#27:                                #   in Loop: Header=BB24_1 Depth=1
	jne	.LBB24_43
# BB#28:                                #   in Loop: Header=BB24_1 Depth=1
	movq	(%rbx), %rbp
	movl	352(%rbx), %eax
	cmpl	$1, %eax
	je	.LBB24_30
# BB#29:                                #   in Loop: Header=BB24_1 Depth=1
	testl	%eax, %eax
	jne	.LBB24_31
	jmp	.LBB24_44
	.p2align	4, 0x90
.LBB24_30:                              #   in Loop: Header=BB24_1 Depth=1
	movq	336(%rbx), %r15
	movq	328(%rbp), %rax
	movq	24(%rax), %r13
	movq	%rbp, %rdi
	callq	gx_lookup_fm_pair
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	gx_add_cached_char
	movq	%rbp, %rdi
	callq	gs_grestore
	movl	$0, 356(%rbx)
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	gx_copy_cached_char
	testl	%eax, %eax
	js	.LBB24_46
.LBB24_31:                              #   in Loop: Header=BB24_1 Depth=1
	movq	%rbp, %rdi
	callq	gs_grestore
	movl	$0, 356(%rbx)
	movq	(%rbx), %rbp
	cmpl	$0, 40(%rbx)
	je	.LBB24_33
# BB#32:                                #   in Loop: Header=BB24_1 Depth=1
	movss	32(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	36(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movb	$2, %al
	movq	%rbp, %rdi
	callq	gs_rmoveto
.LBB24_33:                              #   in Loop: Header=BB24_1 Depth=1
	movq	8(%rbx), %rax
	movl	316(%rbx), %ecx
	decl	%ecx
	movzbl	(%rax,%rcx), %eax
	cmpl	28(%rbx), %eax
	movq	16(%rsp), %r15          # 8-byte Reload
	jne	.LBB24_35
# BB#34:                                #   in Loop: Header=BB24_1 Depth=1
	movss	20(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movb	$2, %al
	movq	%rbp, %rdi
	callq	gs_rmoveto
.LBB24_35:                              #   in Loop: Header=BB24_1 Depth=1
	movq	256(%rbp), %rax
	cmpb	$0, 136(%rax)
	je	.LBB24_47
# BB#36:                                #   in Loop: Header=BB24_1 Depth=1
	movdqu	320(%rbx), %xmm0
	movdqu	120(%rax), %xmm1
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 120(%rax)
	movb	$0, 137(%rax)
	cmpl	$0, 44(%rbx)
	je	.LBB24_1
# BB#37:                                #   in Loop: Header=BB24_1 Depth=1
	movl	316(%rbx), %eax
	cmpl	16(%rbx), %eax
	jae	.LBB24_1
.LBB24_38:
	movq	$continue_show, 360(%rbx)
	movl	$2, %r12d
	jmp	.LBB24_47
.LBB24_46:
	movl	%eax, %r12d
	jmp	.LBB24_47
.LBB24_41:
	movl	$-14, %r12d
.LBB24_47:                              # %.thread110
	movl	%r12d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB24_43:
	movl	$1, %r12d
	jmp	.LBB24_47
.LBB24_44:
	movl	$-23, %r12d
	jmp	.LBB24_47
.LBB24_45:
	xorl	%r12d, %r12d
	jmp	.LBB24_47
.Lfunc_end24:
	.size	show_proceed, .Lfunc_end24-show_proceed
	.cfi_endproc

	.globl	continue_show
	.p2align	4, 0x90
	.type	continue_show,@function
continue_show:                          # @continue_show
	.cfi_startproc
# BB#0:
	jmp	show_proceed            # TAILCALL
.Lfunc_end25:
	.size	continue_show, .Lfunc_end25-continue_show
	.cfi_endproc

	.globl	continue_stringwidth_update
	.p2align	4, 0x90
	.type	continue_stringwidth_update,@function
continue_stringwidth_update:            # @continue_stringwidth_update
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi145:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi146:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi147:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi148:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi149:
	.cfi_def_cfa_offset 48
.Lcfi150:
	.cfi_offset %rbx, -40
.Lcfi151:
	.cfi_offset %r12, -32
.Lcfi152:
	.cfi_offset %r14, -24
.Lcfi153:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	352(%rbx), %eax
	testl	%eax, %eax
	je	.LBB26_1
# BB#2:
	movq	(%rbx), %r14
	cmpl	$1, %eax
	jne	.LBB26_4
# BB#3:
	movq	328(%r14), %rax
	movq	24(%rax), %r15
	movq	336(%rbx), %r12
	movq	%r14, %rdi
	callq	gx_lookup_fm_pair
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
	callq	gx_add_cached_char
	movq	%r14, %rdi
	callq	gs_grestore
.LBB26_4:
	movq	%r14, %rdi
	callq	gs_grestore
	movq	(%rbx), %rax
	movq	256(%rax), %rax
	cmpb	$0, 136(%rax)
	je	.LBB26_5
# BB#7:
	movdqu	320(%rbx), %xmm0
	movdqu	120(%rax), %xmm1
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 120(%rax)
	movb	$0, 137(%rax)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	stringwidth_proceed     # TAILCALL
.LBB26_1:
	movl	$-23, %eax
	jmp	.LBB26_6
.LBB26_5:
	movl	$-14, %eax
.LBB26_6:                               # %stringwidth_move.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end26:
	.size	continue_stringwidth_update, .Lfunc_end26-continue_stringwidth_update
	.cfi_endproc

	.globl	stringwidth_update
	.p2align	4, 0x90
	.type	stringwidth_update,@function
stringwidth_update:                     # @stringwidth_update
	.cfi_startproc
# BB#0:
	movl	352(%rdi), %eax
	testl	%eax, %eax
	je	.LBB27_1
# BB#2:
	pushq	%r15
.Lcfi154:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi155:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi156:
	.cfi_def_cfa_offset 32
.Lcfi157:
	.cfi_offset %rbx, -32
.Lcfi158:
	.cfi_offset %r14, -24
.Lcfi159:
	.cfi_offset %r15, -16
	movq	(%rdi), %rbx
	cmpl	$1, %eax
	jne	.LBB27_4
# BB#3:
	movq	328(%rbx), %rax
	movq	24(%rax), %r14
	movq	336(%rdi), %r15
	movq	%rbx, %rdi
	callq	gx_lookup_fm_pair
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	gx_add_cached_char
	movq	%rbx, %rdi
	callq	gs_grestore
.LBB27_4:
	movq	%rbx, %rdi
	callq	gs_grestore
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB27_1:
	movl	$-23, %eax
	retq
.Lfunc_end27:
	.size	stringwidth_update, .Lfunc_end27-stringwidth_update
	.cfi_endproc

	.globl	stringwidth_move
	.p2align	4, 0x90
	.type	stringwidth_move,@function
stringwidth_move:                       # @stringwidth_move
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	256(%rax), %rax
	cmpb	$0, 136(%rax)
	je	.LBB28_1
# BB#2:
	movdqu	320(%rdi), %xmm0
	movdqu	120(%rax), %xmm1
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 120(%rax)
	movb	$0, 137(%rax)
	xorl	%eax, %eax
	retq
.LBB28_1:
	movl	$-14, %eax
	retq
.Lfunc_end28:
	.size	stringwidth_move, .Lfunc_end28-stringwidth_move
	.cfi_endproc

	.globl	stringwidth_proceed
	.p2align	4, 0x90
	.type	stringwidth_proceed,@function
stringwidth_proceed:                    # @stringwidth_proceed
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi160:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi161:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi162:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi163:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi164:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi166:
	.cfi_def_cfa_offset 112
.Lcfi167:
	.cfi_offset %rbx, -56
.Lcfi168:
	.cfi_offset %r12, -48
.Lcfi169:
	.cfi_offset %r13, -40
.Lcfi170:
	.cfi_offset %r14, -32
.Lcfi171:
	.cfi_offset %r15, -24
.Lcfi172:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	(%rbx), %r14
	movq	8(%rbx), %r15
	movl	316(%rbx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 316(%rbx)
	cmpl	16(%rbx), %eax
	jne	.LBB29_1
.LBB29_9:                               # %._crit_edge
	leaq	16(%rsp), %rsi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	gs_currentpoint
	movq	%r14, %rdi
	callq	gs_grestore
	leaq	8(%rsp), %rsi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	gs_currentpoint
	cmpl	$-14, %eax
	jne	.LBB29_10
# BB#11:
	movq	$0, 8(%rsp)
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	jmp	.LBB29_12
.LBB29_1:                               # %.lr.ph
	leaq	336(%r14), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leaq	320(%rbx), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	movq	%r15, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB29_2:                               # =>This Inner Loop Header: Depth=1
	movl	%eax, %eax
	movzbl	(%r15,%rax), %r13d
	cmpl	$0, 56(%rbx)
	je	.LBB29_13
# BB#3:                                 #   in Loop: Header=BB29_2 Depth=1
	testq	%r12, %r12
	jne	.LBB29_5
# BB#4:                                 #   in Loop: Header=BB29_2 Depth=1
	movq	%r14, %rdi
	callq	gx_lookup_fm_pair
	movq	%rax, %r12
.LBB29_5:                               #   in Loop: Header=BB29_2 Depth=1
	movq	%r14, %rdi
	movq	%r12, %rsi
	movl	%r13d, %edx
	callq	gx_lookup_cached_char
	testq	%rax, %rax
	je	.LBB29_13
# BB#6:                                 #   in Loop: Header=BB29_2 Depth=1
	movups	32(%rax), %xmm0
	movq	24(%rsp), %rax          # 8-byte Reload
	movups	%xmm0, (%rax)
	movl	$2, 352(%rbx)
	movq	(%rbx), %rax
	movq	256(%rax), %rax
	cmpb	$0, 136(%rax)
	jne	.LBB29_8
	jmp	.LBB29_7
	.p2align	4, 0x90
.LBB29_13:                              # %.thread
                                        #   in Loop: Header=BB29_2 Depth=1
	movq	%r14, %rdi
	callq	gs_gsave
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB29_24
# BB#14:                                #   in Loop: Header=BB29_2 Depth=1
	movq	256(%r14), %rax
	movl	$-14, %ebp
	cmpb	$0, 136(%rax)
	je	.LBB29_24
# BB#15:                                #   in Loop: Header=BB29_2 Depth=1
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movq	120(%rax), %r15
	movq	128(%rax), %r12
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	gs_setmatrix
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	gs_translate_to_fixed
	movl	$0, 352(%rbx)
	movq	$continue_stringwidth_update, 360(%rbx)
	movq	328(%r14), %rdx
	movq	176(%rdx), %r8
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	%r13d, %ecx
	callq	*168(%rdx)
	testl	%eax, %eax
	js	.LBB29_16
# BB#17:                                #   in Loop: Header=BB29_2 Depth=1
	jne	.LBB29_18
# BB#19:                                #   in Loop: Header=BB29_2 Depth=1
	movq	(%rbx), %r13
	movl	352(%rbx), %eax
	cmpl	$1, %eax
	je	.LBB29_22
# BB#20:                                #   in Loop: Header=BB29_2 Depth=1
	testl	%eax, %eax
	jne	.LBB29_23
	jmp	.LBB29_21
	.p2align	4, 0x90
.LBB29_22:                              #   in Loop: Header=BB29_2 Depth=1
	movq	328(%r13), %rax
	movq	24(%rax), %r15
	movq	336(%rbx), %r12
	movq	%r13, %rdi
	callq	gx_lookup_fm_pair
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
	callq	gx_add_cached_char
	movq	%r13, %rdi
	callq	gs_grestore
.LBB29_23:                              #   in Loop: Header=BB29_2 Depth=1
	movq	%r13, %rdi
	callq	gs_grestore
	movq	(%rbx), %rax
	movq	256(%rax), %rax
	cmpb	$0, 136(%rax)
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	je	.LBB29_24
.LBB29_8:                               # %.backedge
                                        #   in Loop: Header=BB29_2 Depth=1
	movdqu	320(%rbx), %xmm0
	movdqu	120(%rax), %xmm1
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 120(%rax)
	movb	$0, 137(%rax)
	movl	316(%rbx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 316(%rbx)
	cmpl	16(%rbx), %eax
	jne	.LBB29_2
	jmp	.LBB29_9
.LBB29_10:                              # %._crit_edge._crit_edge
	movd	8(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movd	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
.LBB29_12:
	movss	16(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm2
	movss	%xmm2, 344(%rbx)
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 348(%rbx)
	xorl	%ebp, %ebp
.LBB29_24:                              # %.thread76
	movl	%ebp, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB29_16:
	movl	%eax, %ebp
	jmp	.LBB29_24
.LBB29_18:
	movl	$1, %ebp
	jmp	.LBB29_24
.LBB29_21:
	movl	$-23, %ebp
	jmp	.LBB29_24
.LBB29_7:
	movl	$-14, %ebp
	jmp	.LBB29_24
.Lfunc_end29:
	.size	stringwidth_proceed, .Lfunc_end29-stringwidth_proceed
	.cfi_endproc

	.globl	gs_kshow_previous_char
	.p2align	4, 0x90
	.type	gs_kshow_previous_char,@function
gs_kshow_previous_char:                 # @gs_kshow_previous_char
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	movl	316(%rdi), %ecx
	decl	%ecx
	movb	(%rax,%rcx), %al
	retq
.Lfunc_end30:
	.size	gs_kshow_previous_char, .Lfunc_end30-gs_kshow_previous_char
	.cfi_endproc

	.globl	gs_kshow_next_char
	.p2align	4, 0x90
	.type	gs_kshow_next_char,@function
gs_kshow_next_char:                     # @gs_kshow_next_char
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	movl	316(%rdi), %ecx
	movb	(%rax,%rcx), %al
	retq
.Lfunc_end31:
	.size	gs_kshow_next_char, .Lfunc_end31-gs_kshow_next_char
	.cfi_endproc

	.globl	gs_show_width
	.p2align	4, 0x90
	.type	gs_show_width,@function
gs_show_width:                          # @gs_show_width
	.cfi_startproc
# BB#0:
	movq	344(%rdi), %rax
	movq	%rax, (%rsi)
	retq
.Lfunc_end32:
	.size	gs_show_width, .Lfunc_end32-gs_show_width
	.cfi_endproc

	.globl	gs_show_in_charpath
	.p2align	4, 0x90
	.type	gs_show_in_charpath,@function
gs_show_in_charpath:                    # @gs_show_in_charpath
	.cfi_startproc
# BB#0:
	movl	52(%rdi), %eax
	retq
.Lfunc_end33:
	.size	gs_show_in_charpath, .Lfunc_end33-gs_show_in_charpath
	.cfi_endproc

	.type	gs_show_enum_sizeof,@object # @gs_show_enum_sizeof
	.data
	.globl	gs_show_enum_sizeof
	.p2align	2
gs_show_enum_sizeof:
	.long	368                     # 0x170
	.size	gs_show_enum_sizeof, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
