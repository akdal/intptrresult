	.text
	.file	"BranchCoder.bc"
	.globl	_ZN16CBranchConverter4InitEv
	.p2align	4, 0x90
	.type	_ZN16CBranchConverter4InitEv,@function
_ZN16CBranchConverter4InitEv:           # @_ZN16CBranchConverter4InitEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$0, 12(%rdi)
	movq	(%rdi), %rax
	callq	*56(%rax)
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end0:
	.size	_ZN16CBranchConverter4InitEv, .Lfunc_end0-_ZN16CBranchConverter4InitEv
	.cfi_endproc

	.globl	_ZN16CBranchConverter6FilterEPhj
	.p2align	4, 0x90
	.type	_ZN16CBranchConverter6FilterEPhj,@function
_ZN16CBranchConverter6FilterEPhj:       # @_ZN16CBranchConverter6FilterEPhj
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 16
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*64(%rax)
	addl	%eax, 12(%rbx)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZN16CBranchConverter6FilterEPhj, .Lfunc_end1-_ZN16CBranchConverter6FilterEPhj
	.cfi_endproc

	.section	.text._ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv,@function
_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv: # @_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB2_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB2_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB2_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB2_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB2_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB2_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB2_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB2_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB2_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB2_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB2_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB2_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB2_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB2_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB2_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB2_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB2_17:                               # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end2:
	.size	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv, .Lfunc_end2-_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN16CBranchConverter6AddRefEv,"axG",@progbits,_ZN16CBranchConverter6AddRefEv,comdat
	.weak	_ZN16CBranchConverter6AddRefEv
	.p2align	4, 0x90
	.type	_ZN16CBranchConverter6AddRefEv,@function
_ZN16CBranchConverter6AddRefEv:         # @_ZN16CBranchConverter6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end3:
	.size	_ZN16CBranchConverter6AddRefEv, .Lfunc_end3-_ZN16CBranchConverter6AddRefEv
	.cfi_endproc

	.section	.text._ZN16CBranchConverter7ReleaseEv,"axG",@progbits,_ZN16CBranchConverter7ReleaseEv,comdat
	.weak	_ZN16CBranchConverter7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN16CBranchConverter7ReleaseEv,@function
_ZN16CBranchConverter7ReleaseEv:        # @_ZN16CBranchConverter7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB4_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB4_2:
	popq	%rcx
	retq
.Lfunc_end4:
	.size	_ZN16CBranchConverter7ReleaseEv, .Lfunc_end4-_ZN16CBranchConverter7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8IUnknownD2Ev,"axG",@progbits,_ZN8IUnknownD2Ev,comdat
	.weak	_ZN8IUnknownD2Ev
	.p2align	4, 0x90
	.type	_ZN8IUnknownD2Ev,@function
_ZN8IUnknownD2Ev:                       # @_ZN8IUnknownD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end5:
	.size	_ZN8IUnknownD2Ev, .Lfunc_end5-_ZN8IUnknownD2Ev
	.cfi_endproc

	.section	.text._ZN16CBranchConverterD0Ev,"axG",@progbits,_ZN16CBranchConverterD0Ev,comdat
	.weak	_ZN16CBranchConverterD0Ev
	.p2align	4, 0x90
	.type	_ZN16CBranchConverterD0Ev,@function
_ZN16CBranchConverterD0Ev:              # @_ZN16CBranchConverterD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end6:
	.size	_ZN16CBranchConverterD0Ev, .Lfunc_end6-_ZN16CBranchConverterD0Ev
	.cfi_endproc

	.section	.text._ZN16CBranchConverter7SubInitEv,"axG",@progbits,_ZN16CBranchConverter7SubInitEv,comdat
	.weak	_ZN16CBranchConverter7SubInitEv
	.p2align	4, 0x90
	.type	_ZN16CBranchConverter7SubInitEv,@function
_ZN16CBranchConverter7SubInitEv:        # @_ZN16CBranchConverter7SubInitEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end7:
	.size	_ZN16CBranchConverter7SubInitEv, .Lfunc_end7-_ZN16CBranchConverter7SubInitEv
	.cfi_endproc

	.type	_ZTV16CBranchConverter,@object # @_ZTV16CBranchConverter
	.section	.rodata,"a",@progbits
	.globl	_ZTV16CBranchConverter
	.p2align	3
_ZTV16CBranchConverter:
	.quad	0
	.quad	_ZTI16CBranchConverter
	.quad	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.quad	_ZN16CBranchConverter6AddRefEv
	.quad	_ZN16CBranchConverter7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN16CBranchConverterD0Ev
	.quad	_ZN16CBranchConverter4InitEv
	.quad	_ZN16CBranchConverter6FilterEPhj
	.quad	_ZN16CBranchConverter7SubInitEv
	.quad	__cxa_pure_virtual
	.size	_ZTV16CBranchConverter, 88

	.type	_ZTS16CBranchConverter,@object # @_ZTS16CBranchConverter
	.globl	_ZTS16CBranchConverter
	.p2align	4
_ZTS16CBranchConverter:
	.asciz	"16CBranchConverter"
	.size	_ZTS16CBranchConverter, 19

	.type	_ZTS15ICompressFilter,@object # @_ZTS15ICompressFilter
	.section	.rodata._ZTS15ICompressFilter,"aG",@progbits,_ZTS15ICompressFilter,comdat
	.weak	_ZTS15ICompressFilter
	.p2align	4
_ZTS15ICompressFilter:
	.asciz	"15ICompressFilter"
	.size	_ZTS15ICompressFilter, 18

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI15ICompressFilter,@object # @_ZTI15ICompressFilter
	.section	.rodata._ZTI15ICompressFilter,"aG",@progbits,_ZTI15ICompressFilter,comdat
	.weak	_ZTI15ICompressFilter
	.p2align	4
_ZTI15ICompressFilter:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15ICompressFilter
	.quad	_ZTI8IUnknown
	.size	_ZTI15ICompressFilter, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI16CBranchConverter,@object # @_ZTI16CBranchConverter
	.section	.rodata,"a",@progbits
	.globl	_ZTI16CBranchConverter
	.p2align	4
_ZTI16CBranchConverter:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS16CBranchConverter
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI15ICompressFilter
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI16CBranchConverter, 56


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
