	.text
	.file	"mltaln9.bc"
	.globl	seqlen
	.p2align	4, 0x90
	.type	seqlen,@function
seqlen:                                 # @seqlen
	.cfi_startproc
# BB#0:
	movb	(%rdi), %cl
	testb	%cl, %cl
	je	.LBB0_1
# BB#2:                                 # %.lr.ph.preheader
	incq	%rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edx, %edx
	cmpb	$45, %cl
	setne	%dl
	addl	%edx, %eax
	movzbl	(%rdi), %ecx
	incq	%rdi
	testb	%cl, %cl
	jne	.LBB0_3
# BB#4:                                 # %._crit_edge
	retq
.LBB0_1:
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	seqlen, .Lfunc_end0-seqlen
	.cfi_endproc

	.globl	intlen
	.p2align	4, 0x90
	.type	intlen,@function
intlen:                                 # @intlen
	.cfi_startproc
# BB#0:
	cmpl	$-1, (%rdi)
	je	.LBB1_1
# BB#2:                                 # %.lr.ph.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$-1, 4(%rdi,%rax,4)
	leaq	1(%rax), %rax
	jne	.LBB1_3
# BB#4:                                 # %._crit_edge
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.LBB1_1:
	xorl	%eax, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end1:
	.size	intlen, .Lfunc_end1-intlen
	.cfi_endproc

	.globl	seqcheck
	.p2align	4, 0x90
	.type	seqcheck,@function
seqcheck:                               # @seqcheck
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.LBB2_8
# BB#1:                                 # %.lr.ph28.preheader
	movq	%r14, %r15
.LBB2_2:                                # %.lr.ph28
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_4 Depth 2
	movq	%r12, %rdi
	callq	strlen
	testl	%eax, %eax
	jle	.LBB2_7
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB2_2 Depth=1
	cltq
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_4:                                #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	(%r12,%rbx), %rcx
	cmpl	$-1, amino_n(,%rcx,4)
	je	.LBB2_5
# BB#6:                                 #   in Loop: Header=BB2_4 Depth=2
	incq	%rbx
	cmpq	%rax, %rbx
	jl	.LBB2_4
.LBB2_7:                                # %._crit_edge
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	8(%r15), %r12
	addq	$8, %r15
	testq	%r12, %r12
	jne	.LBB2_2
.LBB2_8:
	xorl	%eax, %eax
	jmp	.LBB2_9
.LBB2_5:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$75, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$75, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$75, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$5, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	movq	(%r15), %rax
	movsbl	(%rax,%rbx), %edx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	leal	1(%rbx), %edx
	movq	%r15, %rcx
	subq	%r14, %rcx
	shrq	$3, %rcx
	incl	%ecx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$5, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$75, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$75, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$75, %esi
	movl	$1, %edx
	callq	fwrite
	movq	(%r15), %rax
	movb	(%rax,%rbx), %al
.LBB2_9:                                # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	seqcheck, .Lfunc_end2-seqcheck
	.cfi_endproc

	.globl	scmx_calc
	.p2align	4, 0x90
	.type	scmx_calc,@function
scmx_calc:                              # @scmx_calc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 208
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	(%rsi), %rdi
	callq	strlen
	movq	%rax, (%rsp)            # 8-byte Spill
	testl	%eax, %eax
	jle	.LBB3_3
# BB#1:                                 # %.preheader58.preheader
	movq	16(%rbx), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	24(%rbx), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	32(%rbx), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	40(%rbx), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	48(%rbx), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	56(%rbx), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	64(%rbx), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	72(%rbx), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	80(%rbx), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	88(%rbx), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	96(%rbx), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	104(%rbx), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	112(%rbx), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	120(%rbx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	128(%rbx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	136(%rbx), %r9
	movq	144(%rbx), %r10
	movq	152(%rbx), %r11
	movq	160(%rbx), %r13
	movq	168(%rbx), %rdx
	movq	176(%rbx), %rsi
	movq	184(%rbx), %rdi
	movq	192(%rbx), %rcx
	movq	(%rbx), %rbp
	movq	8(%rbx), %r14
	movq	200(%rbx), %r15
	movl	(%rsp), %r12d           # 4-byte Reload
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB3_2:                                # %.preheader58
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, (%rbp,%r8,4)
	movl	$0, (%r14,%r8,4)
	movq	144(%rsp), %rax         # 8-byte Reload
	movl	$0, (%rax,%r8,4)
	movq	136(%rsp), %rax         # 8-byte Reload
	movl	$0, (%rax,%r8,4)
	movq	128(%rsp), %rax         # 8-byte Reload
	movl	$0, (%rax,%r8,4)
	movq	120(%rsp), %rax         # 8-byte Reload
	movl	$0, (%rax,%r8,4)
	movq	112(%rsp), %rax         # 8-byte Reload
	movl	$0, (%rax,%r8,4)
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	$0, (%rax,%r8,4)
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rax,%r8,4)
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rax,%r8,4)
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rax,%r8,4)
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rax,%r8,4)
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rax,%r8,4)
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rax,%r8,4)
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rax,%r8,4)
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rax,%r8,4)
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rax,%r8,4)
	movl	$0, (%r9,%r8,4)
	movl	$0, (%r10,%r8,4)
	movl	$0, (%r11,%r8,4)
	movl	$0, (%r13,%r8,4)
	movl	$0, (%rdx,%r8,4)
	movl	$0, (%rsi,%r8,4)
	movl	$0, (%rdi,%r8,4)
	movl	$0, (%rcx,%r8,4)
	movl	$0, (%r15,%r8,4)
	incq	%r8
	cmpq	%r8, %r12
	jne	.LBB3_2
.LBB3_3:                                # %.preheader57
	movq	8(%rsp), %r12           # 8-byte Reload
	testl	%r12d, %r12d
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	16(%rsp), %r15          # 8-byte Reload
	js	.LBB3_10
# BB#4:                                 # %.lr.ph65.preheader
	leal	1(%r12), %ecx
	testb	$1, %cl
	jne	.LBB3_6
# BB#5:
	xorl	%edx, %edx
	testl	%r12d, %r12d
	jne	.LBB3_8
	jmp	.LBB3_10
.LBB3_6:                                # %.lr.ph65.prol
	movq	(%r15), %rax
	movsbq	(%rax), %rax
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movslq	amino_n(,%rax,4), %rax
	movq	(%rbx,%rax,8), %rax
	addss	(%rax), %xmm0
	movss	%xmm0, (%rax)
	movl	$1, %edx
	testl	%r12d, %r12d
	je	.LBB3_10
.LBB3_8:                                # %.lr.ph65.preheader.new
	subq	%rdx, %rcx
	leaq	8(%r15,%rdx,8), %rax
	leaq	8(%r14,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB3_9:                                # %.lr.ph65
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rax), %rsi
	movsbq	(%rsi), %rsi
	movsd	-8(%rdx), %xmm0         # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movslq	amino_n(,%rsi,4), %rsi
	movq	(%rbx,%rsi,8), %rsi
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	movq	(%rax), %rsi
	movsbq	(%rsi), %rsi
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movslq	amino_n(,%rsi,4), %rsi
	movq	(%rbx,%rsi,8), %rsi
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	addq	$16, %rax
	addq	$16, %rdx
	addq	$-2, %rcx
	jne	.LBB3_9
.LBB3_10:                               # %.preheader56
	movq	(%rsp), %rax            # 8-byte Reload
	leal	-1(%rax), %eax
	cmpl	$2, %eax
	jl	.LBB3_20
# BB#11:                                # %.preheader55.lr.ph
	testl	%r12d, %r12d
	js	.LBB3_27
# BB#12:                                # %.preheader55.preheader
	leal	1(%r12), %r8d
	movl	%eax, %r11d
	movl	%r8d, %esi
	andl	$1, %esi
	leaq	8(%r15), %r9
	leaq	8(%r14), %r10
	movl	$1, %edi
	.p2align	4, 0x90
.LBB3_13:                               # %.preheader55
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_18 Depth 2
	testq	%rsi, %rsi
	jne	.LBB3_15
# BB#14:                                #   in Loop: Header=BB3_13 Depth=1
	xorl	%ecx, %ecx
	testl	%r12d, %r12d
	jne	.LBB3_17
	jmp	.LBB3_19
	.p2align	4, 0x90
.LBB3_15:                               #   in Loop: Header=BB3_13 Depth=1
	movq	(%r15), %rax
	movsbq	(%rax,%rdi), %rax
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movslq	amino_n(,%rax,4), %rax
	movq	(%rbx,%rax,8), %rax
	addss	(%rax,%rdi,4), %xmm0
	movss	%xmm0, (%rax,%rdi,4)
	movl	$1, %ecx
	testl	%r12d, %r12d
	je	.LBB3_19
.LBB3_17:                               # %.preheader55.new
                                        #   in Loop: Header=BB3_13 Depth=1
	movq	%r8, %rax
	subq	%rcx, %rax
	leaq	(%r9,%rcx,8), %rbp
	leaq	(%r10,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB3_18:                               #   Parent Loop BB3_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rbp), %rdx
	movsbq	(%rdx,%rdi), %rdx
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movslq	amino_n(,%rdx,4), %rdx
	movq	(%rbx,%rdx,8), %rdx
	addss	(%rdx,%rdi,4), %xmm0
	movss	%xmm0, (%rdx,%rdi,4)
	movq	(%rbp), %rdx
	movsbq	(%rdx,%rdi), %rdx
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movslq	amino_n(,%rdx,4), %rdx
	movq	(%rbx,%rdx,8), %rdx
	addss	(%rdx,%rdi,4), %xmm0
	movss	%xmm0, (%rdx,%rdi,4)
	addq	$16, %rbp
	addq	$16, %rcx
	addq	$-2, %rax
	jne	.LBB3_18
.LBB3_19:                               # %._crit_edge62
                                        #   in Loop: Header=BB3_13 Depth=1
	incq	%rdi
	cmpq	%r11, %rdi
	jne	.LBB3_13
.LBB3_20:                               # %.preheader
	testl	%r12d, %r12d
	js	.LBB3_27
# BB#21:                                # %.lr.ph
	movslq	(%rsp), %rax            # 4-byte Folded Reload
	leal	1(%r12), %ecx
	testb	$1, %cl
	jne	.LBB3_23
# BB#22:
	xorl	%esi, %esi
	testl	%r12d, %r12d
	jne	.LBB3_25
	jmp	.LBB3_27
.LBB3_23:
	movq	(%r15), %rdx
	movsbq	-1(%rdx,%rax), %rdx
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movslq	amino_n(,%rdx,4), %rdx
	movq	(%rbx,%rdx,8), %rdx
	addss	-4(%rdx,%rax,4), %xmm0
	movss	%xmm0, -4(%rdx,%rax,4)
	movl	$1, %esi
	testl	%r12d, %r12d
	je	.LBB3_27
.LBB3_25:                               # %.lr.ph.new
	subq	%rsi, %rcx
	leaq	8(%r15,%rsi,8), %rdx
	leaq	8(%r14,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB3_26:                               # =>This Inner Loop Header: Depth=1
	movq	-8(%rdx), %rdi
	movsbq	-1(%rdi,%rax), %rdi
	movsd	-8(%rsi), %xmm0         # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movslq	amino_n(,%rdi,4), %rdi
	movq	(%rbx,%rdi,8), %rdi
	addss	-4(%rdi,%rax,4), %xmm0
	movss	%xmm0, -4(%rdi,%rax,4)
	movq	(%rdx), %rdi
	movsbq	-1(%rdi,%rax), %rdi
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movslq	amino_n(,%rdi,4), %rdi
	movq	(%rbx,%rdi,8), %rdi
	addss	-4(%rdi,%rax,4), %xmm0
	movss	%xmm0, -4(%rdi,%rax,4)
	addq	$16, %rdx
	addq	$16, %rsi
	addq	$-2, %rcx
	jne	.LBB3_26
.LBB3_27:                               # %._crit_edge
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	scmx_calc, .Lfunc_end3-scmx_calc
	.cfi_endproc

	.globl	exitall
	.p2align	4, 0x90
	.type	exitall,@function
exitall:                                # @exitall
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rcx
	movq	stderr(%rip), %rdi
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end4:
	.size	exitall, .Lfunc_end4-exitall
	.cfi_endproc

	.globl	display
	.p2align	4, 0x90
	.type	display,@function
display:                                # @display
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 40
	subq	$136, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 176
.Lcfi28:
	.cfi_offset %rbx, -40
.Lcfi29:
	.cfi_offset %r12, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %r15, -16
	movl	%esi, %ebx
	movq	%rdi, %r15
	cmpl	$0, disp(%rip)
	jne	.LBB5_1
.LBB5_4:                                # %.loopexit
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB5_1:
	movq	stderr(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$125, %esi
	movl	$1, %edx
	callq	fwrite
	testl	%ebx, %ebx
	jle	.LBB5_4
# BB#2:                                 # %.lr.ph
	cmpl	$61, %ebx
	movl	$60, %eax
	cmovll	%ebx, %eax
	movslq	%eax, %r12
	movq	%rsp, %r14
	xorl	%ebx, %ebx
.LBB5_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rbx,8), %rsi
	movl	$120, %edx
	movq	%r14, %rdi
	callq	strncpy
	movb	$0, 120(%rsp)
	movq	stderr(%rip), %rdi
	incq	%rbx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	movq	%r14, %rcx
	callq	fprintf
	cmpq	%r12, %rbx
	jl	.LBB5_3
	jmp	.LBB5_4
.Lfunc_end5:
	.size	display, .Lfunc_end5-display
	.cfi_endproc

	.globl	intergroup_score_consweight
	.p2align	4, 0x90
	.type	intergroup_score_consweight,@function
intergroup_score_consweight:            # @intergroup_score_consweight
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movl	%r9d, -60(%rsp)         # 4-byte Spill
	movq	%rcx, -56(%rsp)         # 8-byte Spill
	movq	%rsi, -8(%rsp)          # 8-byte Spill
	movq	%rdi, -24(%rsp)         # 8-byte Spill
	movq	64(%rsp), %rax
	movq	$0, (%rax)
	testl	%r8d, %r8d
	jle	.LBB6_11
# BB#1:                                 # %.preheader.lr.ph
	movl	56(%rsp), %eax
	leal	-2(%rax), %r11d
	movl	-60(%rsp), %ecx         # 4-byte Reload
	movl	%r8d, %esi
	movq	%rsi, -16(%rsp)         # 8-byte Spill
	movq	%rcx, -48(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1, %ecx
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	movq	-56(%rsp), %rcx         # 8-byte Reload
	leaq	8(%rcx), %rcx
	movq	%rcx, -40(%rsp)         # 8-byte Spill
	xorpd	%xmm0, %xmm0
	xorl	%r13d, %r13d
	xorpd	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB6_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_9 Depth 2
                                        #     Child Loop BB6_13 Depth 2
                                        #       Child Loop BB6_14 Depth 3
                                        #         Child Loop BB6_19 Depth 4
                                        #         Child Loop BB6_21 Depth 4
	cmpl	$0, -60(%rsp)           # 4-byte Folded Reload
	jle	.LBB6_10
# BB#3:                                 # %.lr.ph82
                                        #   in Loop: Header=BB6_2 Depth=1
	testl	%eax, %eax
	jle	.LBB6_4
# BB#12:                                # %.lr.ph82.split.us.preheader
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	-24(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%r13,8), %rbx
	leaq	1(%rbx), %rbp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB6_13:                               # %.lr.ph82.split.us
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_14 Depth 3
                                        #         Child Loop BB6_19 Depth 4
                                        #         Child Loop BB6_21 Depth 4
	movsd	(%rdx,%r13,8), %xmm2    # xmm2 = mem[0],zero
	movq	-56(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r15,8), %xmm2
	movq	-8(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%r15,8), %r14
	xorps	%xmm4, %xmm4
	cvtsi2sdl	penalty(%rip), %xmm4
	leaq	1(%r14), %r8
	xorpd	%xmm3, %xmm3
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB6_14:                               #   Parent Loop BB6_2 Depth=1
                                        #     Parent Loop BB6_13 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB6_19 Depth 4
                                        #         Child Loop BB6_21 Depth 4
	movslq	%edi, %r10
	movsbq	(%rbx,%r10), %r12
	cmpq	$45, %r12
	movb	(%r14,%r10), %r9b
	jne	.LBB6_16
# BB#15:                                #   in Loop: Header=BB6_14 Depth=3
	cmpb	$45, %r9b
	je	.LBB6_24
.LBB6_16:                               #   in Loop: Header=BB6_14 Depth=3
	movsbq	%r9b, %rsi
	movq	%r12, %rcx
	shlq	$10, %rcx
	cmpb	$45, %r12b
	movsd	amino_dis_consweight_multi(%rcx,%rsi,8), %xmm5 # xmm5 = mem[0],zero
	addsd	%xmm5, %xmm3
	jne	.LBB6_17
# BB#20:                                #   in Loop: Header=BB6_14 Depth=3
	addsd	%xmm4, %xmm3
	leal	-1(%r10), %edi
	addq	%rbp, %r10
	.p2align	4, 0x90
.LBB6_21:                               #   Parent Loop BB6_2 Depth=1
                                        #     Parent Loop BB6_13 Depth=2
                                        #       Parent Loop BB6_14 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%edi
	cmpb	$45, (%r10)
	leaq	1(%r10), %r10
	je	.LBB6_21
	jmp	.LBB6_22
	.p2align	4, 0x90
.LBB6_17:                               #   in Loop: Header=BB6_14 Depth=3
	cmpb	$45, %r9b
	jne	.LBB6_24
# BB#18:                                #   in Loop: Header=BB6_14 Depth=3
	addsd	%xmm4, %xmm3
	leal	-1(%r10), %edi
	addq	%r8, %r10
	.p2align	4, 0x90
.LBB6_19:                               #   Parent Loop BB6_2 Depth=1
                                        #     Parent Loop BB6_13 Depth=2
                                        #       Parent Loop BB6_14 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%edi
	cmpb	$45, (%r10)
	leaq	1(%r10), %r10
	je	.LBB6_19
.LBB6_22:                               #   in Loop: Header=BB6_14 Depth=3
	addsd	%xmm5, %xmm3
	cmpl	%r11d, %edi
	jg	.LBB6_23
.LBB6_24:                               #   in Loop: Header=BB6_14 Depth=3
	incl	%edi
	cmpl	%eax, %edi
	jl	.LBB6_14
.LBB6_23:                               # %._crit_edge.us
                                        #   in Loop: Header=BB6_13 Depth=2
	mulsd	%xmm3, %xmm2
	addsd	%xmm2, %xmm1
	movq	64(%rsp), %rcx
	movsd	%xmm1, (%rcx)
	incq	%r15
	cmpq	-48(%rsp), %r15         # 8-byte Folded Reload
	jne	.LBB6_13
	jmp	.LBB6_10
	.p2align	4, 0x90
.LBB6_4:                                # %.lr.ph82.split.preheader
                                        #   in Loop: Header=BB6_2 Depth=1
	cmpq	$0, -32(%rsp)           # 8-byte Folded Reload
	jne	.LBB6_6
# BB#5:                                 #   in Loop: Header=BB6_2 Depth=1
	xorl	%ebp, %ebp
	cmpl	$1, -60(%rsp)           # 4-byte Folded Reload
	jne	.LBB6_8
	jmp	.LBB6_10
.LBB6_6:                                # %.lr.ph82.split.prol
                                        #   in Loop: Header=BB6_2 Depth=1
	movsd	(%rdx,%r13,8), %xmm2    # xmm2 = mem[0],zero
	movq	-56(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx), %xmm2
	mulsd	%xmm0, %xmm2
	addsd	%xmm2, %xmm1
	movq	64(%rsp), %rcx
	movsd	%xmm1, (%rcx)
	movl	$1, %ebp
	cmpl	$1, -60(%rsp)           # 4-byte Folded Reload
	je	.LBB6_10
.LBB6_8:                                # %.lr.ph82.split.preheader.new
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	-48(%rsp), %rdi         # 8-byte Reload
	subq	%rbp, %rdi
	movq	-40(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbp,8), %rbp
	.p2align	4, 0x90
.LBB6_9:                                # %.lr.ph82.split
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdx,%r13,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	-8(%rbp), %xmm2
	mulsd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	movq	64(%rsp), %rcx
	movsd	%xmm2, (%rcx)
	movsd	(%rdx,%r13,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	(%rbp), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	movsd	%xmm1, (%rcx)
	addq	$16, %rbp
	addq	$-2, %rdi
	jne	.LBB6_9
	.p2align	4, 0x90
.LBB6_10:                               # %._crit_edge83
                                        #   in Loop: Header=BB6_2 Depth=1
	incq	%r13
	cmpq	-16(%rsp), %r13         # 8-byte Folded Reload
	jne	.LBB6_2
.LBB6_11:                               # %._crit_edge85
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	intergroup_score_consweight, .Lfunc_end6-intergroup_score_consweight
	.cfi_endproc

	.globl	intergroup_score_gapnomi
	.p2align	4, 0x90
	.type	intergroup_score_gapnomi,@function
intergroup_score_gapnomi:               # @intergroup_score_gapnomi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 56
.Lcfi50:
	.cfi_offset %rbx, -56
.Lcfi51:
	.cfi_offset %r12, -48
.Lcfi52:
	.cfi_offset %r13, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movl	%r9d, -44(%rsp)         # 4-byte Spill
	movq	%rsi, -8(%rsp)          # 8-byte Spill
	movq	%rdi, -24(%rsp)         # 8-byte Spill
	movq	64(%rsp), %rax
	movq	$0, (%rax)
	testl	%r8d, %r8d
	jle	.LBB7_11
# BB#1:                                 # %.preheader.lr.ph
	movl	56(%rsp), %eax
	leal	-2(%rax), %r11d
	movl	-44(%rsp), %esi         # 4-byte Reload
	movl	%r8d, %edi
	movq	%rdi, -16(%rsp)         # 8-byte Spill
	movl	%esi, %edi
	andl	$1, %edi
	movq	%rdi, -32(%rsp)         # 8-byte Spill
	leaq	8(%rcx), %rdi
	movq	%rdi, -40(%rsp)         # 8-byte Spill
	xorpd	%xmm0, %xmm0
	xorl	%r13d, %r13d
	xorpd	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB7_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_9 Depth 2
                                        #     Child Loop BB7_13 Depth 2
                                        #       Child Loop BB7_14 Depth 3
                                        #         Child Loop BB7_21 Depth 4
                                        #         Child Loop BB7_18 Depth 4
	cmpl	$0, -44(%rsp)           # 4-byte Folded Reload
	jle	.LBB7_10
# BB#3:                                 # %.lr.ph73
                                        #   in Loop: Header=BB7_2 Depth=1
	testl	%eax, %eax
	jle	.LBB7_4
# BB#12:                                # %.lr.ph73.split.us.preheader
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	-24(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi,%r13,8), %rbx
	leaq	1(%rbx), %rbp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB7_13:                               # %.lr.ph73.split.us
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_14 Depth 3
                                        #         Child Loop BB7_21 Depth 4
                                        #         Child Loop BB7_18 Depth 4
	movsd	(%rdx,%r13,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	(%rcx,%r15,8), %xmm2
	movq	-8(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi,%r15,8), %r14
	xorps	%xmm4, %xmm4
	cvtsi2sdl	penalty(%rip), %xmm4
	leaq	1(%r14), %r8
	xorpd	%xmm3, %xmm3
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB7_14:                               #   Parent Loop BB7_2 Depth=1
                                        #     Parent Loop BB7_13 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB7_21 Depth 4
                                        #         Child Loop BB7_18 Depth 4
	movslq	%edi, %r10
	movb	(%rbx,%r10), %r12b
	movb	(%r14,%r10), %r9b
	cmpb	$45, %r12b
	jne	.LBB7_16
# BB#15:                                #   in Loop: Header=BB7_14 Depth=3
	cmpb	$45, %r9b
	je	.LBB7_24
.LBB7_16:                               #   in Loop: Header=BB7_14 Depth=3
	cmpb	$45, %r12b
	jne	.LBB7_19
# BB#17:                                # %.preheader90.preheader
                                        #   in Loop: Header=BB7_14 Depth=3
	leal	-1(%r10), %edi
	addq	%rbp, %r10
	.p2align	4, 0x90
.LBB7_18:                               # %.preheader90
                                        #   Parent Loop BB7_2 Depth=1
                                        #     Parent Loop BB7_13 Depth=2
                                        #       Parent Loop BB7_14 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%edi
	cmpb	$45, (%r10)
	leaq	1(%r10), %r10
	je	.LBB7_18
	jmp	.LBB7_22
	.p2align	4, 0x90
.LBB7_19:                               #   in Loop: Header=BB7_14 Depth=3
	cmpb	$45, %r9b
	jne	.LBB7_24
# BB#20:                                # %.preheader91.preheader
                                        #   in Loop: Header=BB7_14 Depth=3
	leal	-1(%r10), %edi
	addq	%r8, %r10
	.p2align	4, 0x90
.LBB7_21:                               # %.preheader91
                                        #   Parent Loop BB7_2 Depth=1
                                        #     Parent Loop BB7_13 Depth=2
                                        #       Parent Loop BB7_14 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%edi
	cmpb	$45, (%r10)
	leaq	1(%r10), %r10
	je	.LBB7_21
.LBB7_22:                               #   in Loop: Header=BB7_14 Depth=3
	addsd	%xmm4, %xmm3
	cmpl	%r11d, %edi
	jg	.LBB7_23
.LBB7_24:                               #   in Loop: Header=BB7_14 Depth=3
	incl	%edi
	cmpl	%eax, %edi
	jl	.LBB7_14
.LBB7_23:                               # %._crit_edge.us
                                        #   in Loop: Header=BB7_13 Depth=2
	mulsd	%xmm3, %xmm2
	addsd	%xmm2, %xmm1
	movq	64(%rsp), %rdi
	movsd	%xmm1, (%rdi)
	incq	%r15
	cmpq	%rsi, %r15
	jne	.LBB7_13
	jmp	.LBB7_10
	.p2align	4, 0x90
.LBB7_4:                                # %.lr.ph73.split.preheader
                                        #   in Loop: Header=BB7_2 Depth=1
	cmpq	$0, -32(%rsp)           # 8-byte Folded Reload
	jne	.LBB7_6
# BB#5:                                 #   in Loop: Header=BB7_2 Depth=1
	xorl	%ebp, %ebp
	cmpl	$1, -44(%rsp)           # 4-byte Folded Reload
	jne	.LBB7_8
	jmp	.LBB7_10
.LBB7_6:                                # %.lr.ph73.split.prol
                                        #   in Loop: Header=BB7_2 Depth=1
	movsd	(%rdx,%r13,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	(%rcx), %xmm2
	mulsd	%xmm0, %xmm2
	addsd	%xmm2, %xmm1
	movq	64(%rsp), %rdi
	movsd	%xmm1, (%rdi)
	movl	$1, %ebp
	cmpl	$1, -44(%rsp)           # 4-byte Folded Reload
	je	.LBB7_10
.LBB7_8:                                # %.lr.ph73.split.preheader.new
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	%rsi, %rdi
	subq	%rbp, %rdi
	movq	-40(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rbp,8), %rbp
	.p2align	4, 0x90
.LBB7_9:                                # %.lr.ph73.split
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdx,%r13,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	-8(%rbp), %xmm2
	mulsd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	movq	64(%rsp), %rbx
	movsd	%xmm2, (%rbx)
	movsd	(%rdx,%r13,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	(%rbp), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	movsd	%xmm1, (%rbx)
	addq	$16, %rbp
	addq	$-2, %rdi
	jne	.LBB7_9
	.p2align	4, 0x90
.LBB7_10:                               # %._crit_edge74
                                        #   in Loop: Header=BB7_2 Depth=1
	incq	%r13
	cmpq	-16(%rsp), %r13         # 8-byte Folded Reload
	jne	.LBB7_2
.LBB7_11:                               # %._crit_edge76
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	intergroup_score_gapnomi, .Lfunc_end7-intergroup_score_gapnomi
	.cfi_endproc

	.globl	intergroup_score
	.p2align	4, 0x90
	.type	intergroup_score,@function
intergroup_score:                       # @intergroup_score
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi60:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 56
.Lcfi62:
	.cfi_offset %rbx, -56
.Lcfi63:
	.cfi_offset %r12, -48
.Lcfi64:
	.cfi_offset %r13, -40
.Lcfi65:
	.cfi_offset %r14, -32
.Lcfi66:
	.cfi_offset %r15, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movl	%r9d, -60(%rsp)         # 4-byte Spill
	movq	%rcx, -56(%rsp)         # 8-byte Spill
	movq	%rsi, -8(%rsp)          # 8-byte Spill
	movq	%rdi, -24(%rsp)         # 8-byte Spill
	movq	64(%rsp), %rax
	movq	$0, (%rax)
	testl	%r8d, %r8d
	jle	.LBB8_12
# BB#1:                                 # %.preheader.lr.ph
	movl	56(%rsp), %eax
	leal	-2(%rax), %r11d
	movl	-60(%rsp), %ecx         # 4-byte Reload
	movl	%r8d, %esi
	movq	%rsi, -16(%rsp)         # 8-byte Spill
	movq	%rcx, -48(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1, %ecx
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	movq	-56(%rsp), %rcx         # 8-byte Reload
	leaq	8(%rcx), %rcx
	movq	%rcx, -40(%rsp)         # 8-byte Spill
	xorpd	%xmm0, %xmm0
	xorl	%r13d, %r13d
	xorpd	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB8_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_10 Depth 2
                                        #     Child Loop BB8_14 Depth 2
                                        #       Child Loop BB8_15 Depth 3
                                        #         Child Loop BB8_21 Depth 4
                                        #         Child Loop BB8_26 Depth 4
	cmpl	$0, -60(%rsp)           # 4-byte Folded Reload
	jle	.LBB8_3
# BB#4:                                 # %.lr.ph120
                                        #   in Loop: Header=BB8_2 Depth=1
	testl	%eax, %eax
	jle	.LBB8_5
# BB#13:                                # %.lr.ph120.split.us.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	-24(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%r13,8), %rbx
	leaq	1(%rbx), %rbp
	movapd	%xmm2, %xmm1
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB8_14:                               # %.lr.ph120.split.us
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_15 Depth 3
                                        #         Child Loop BB8_21 Depth 4
                                        #         Child Loop BB8_26 Depth 4
	movsd	(%rdx,%r13,8), %xmm2    # xmm2 = mem[0],zero
	movq	-56(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r15,8), %xmm2
	movq	-8(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%r15,8), %r14
	xorps	%xmm3, %xmm3
	cvtsi2sdl	penalty(%rip), %xmm3
	leaq	1(%r14), %r8
	xorpd	%xmm5, %xmm5
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB8_15:                               #   Parent Loop BB8_2 Depth=1
                                        #     Parent Loop BB8_14 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB8_21 Depth 4
                                        #         Child Loop BB8_26 Depth 4
	movslq	%edi, %r10
	movsbq	(%rbx,%r10), %r9
	cmpq	$45, %r9
	movb	(%r14,%r10), %sil
	jne	.LBB8_17
# BB#16:                                #   in Loop: Header=BB8_15 Depth=3
	cmpb	$45, %sil
	je	.LBB8_27
.LBB8_17:                               #   in Loop: Header=BB8_15 Depth=3
	movsbq	%sil, %r12
	movl	%r9d, %ecx
	shlq	$10, %r9
	cmpb	$45, %cl
	movsd	amino_dis_consweight_multi(%r9,%r12,8), %xmm4 # xmm4 = mem[0],zero
	addsd	%xmm4, %xmm5
	jne	.LBB8_18
# BB#24:                                #   in Loop: Header=BB8_15 Depth=3
	addsd	%xmm3, %xmm5
	addsd	%xmm5, %xmm4
	cmpb	$45, 1(%rbx,%r10)
	jne	.LBB8_22
# BB#25:                                # %.lr.ph100.us.preheader
                                        #   in Loop: Header=BB8_15 Depth=3
	incq	%r10
	movsd	amino_dis_consweight_multi+46080(,%r12,8), %xmm5 # xmm5 = mem[0],zero
	addq	%rbp, %r10
	.p2align	4, 0x90
.LBB8_26:                               # %.lr.ph100.us
                                        #   Parent Loop BB8_2 Depth=1
                                        #     Parent Loop BB8_14 Depth=2
                                        #       Parent Loop BB8_15 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	addsd	%xmm5, %xmm4
	incl	%edi
	cmpb	$45, (%r10)
	leaq	1(%r10), %r10
	je	.LBB8_26
	jmp	.LBB8_22
	.p2align	4, 0x90
.LBB8_18:                               #   in Loop: Header=BB8_15 Depth=3
	cmpb	$45, %sil
	jne	.LBB8_27
# BB#19:                                #   in Loop: Header=BB8_15 Depth=3
	addsd	%xmm3, %xmm5
	addsd	%xmm5, %xmm4
	cmpb	$45, 1(%r14,%r10)
	jne	.LBB8_22
# BB#20:                                # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB8_15 Depth=3
	incq	%r10
	movsd	amino_dis_consweight_multi+360(%r9), %xmm5 # xmm5 = mem[0],zero
	addq	%r8, %r10
	.p2align	4, 0x90
.LBB8_21:                               # %.lr.ph.us
                                        #   Parent Loop BB8_2 Depth=1
                                        #     Parent Loop BB8_14 Depth=2
                                        #       Parent Loop BB8_15 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	addsd	%xmm5, %xmm4
	incl	%edi
	cmpb	$45, (%r10)
	leaq	1(%r10), %r10
	je	.LBB8_21
	.p2align	4, 0x90
.LBB8_22:                               # %._crit_edge101.us
                                        #   in Loop: Header=BB8_15 Depth=3
	cmpl	%r11d, %edi
	movapd	%xmm4, %xmm5
	jg	.LBB8_23
.LBB8_27:                               #   in Loop: Header=BB8_15 Depth=3
	incl	%edi
	cmpl	%eax, %edi
	jl	.LBB8_15
.LBB8_23:                               # %._crit_edge109.us
                                        #   in Loop: Header=BB8_14 Depth=2
	mulsd	%xmm5, %xmm2
	addsd	%xmm2, %xmm1
	movq	64(%rsp), %rcx
	movsd	%xmm1, (%rcx)
	incq	%r15
	cmpq	-48(%rsp), %r15         # 8-byte Folded Reload
	jne	.LBB8_14
	jmp	.LBB8_11
	.p2align	4, 0x90
.LBB8_3:                                #   in Loop: Header=BB8_2 Depth=1
	movapd	%xmm2, %xmm1
	jmp	.LBB8_11
	.p2align	4, 0x90
.LBB8_5:                                # %.lr.ph120.split.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	cmpq	$0, -32(%rsp)           # 8-byte Folded Reload
	jne	.LBB8_7
# BB#6:                                 #   in Loop: Header=BB8_2 Depth=1
                                        # implicit-def: %XMM1
	xorl	%esi, %esi
	cmpl	$1, -60(%rsp)           # 4-byte Folded Reload
	jne	.LBB8_9
	jmp	.LBB8_11
.LBB8_7:                                # %.lr.ph120.split.prol
                                        #   in Loop: Header=BB8_2 Depth=1
	movsd	(%rdx,%r13,8), %xmm1    # xmm1 = mem[0],zero
	movq	-56(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	movq	64(%rsp), %rcx
	movsd	%xmm1, (%rcx)
	movapd	%xmm1, %xmm2
	movl	$1, %esi
	cmpl	$1, -60(%rsp)           # 4-byte Folded Reload
	je	.LBB8_11
.LBB8_9:                                # %.lr.ph120.split.preheader.new
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	-48(%rsp), %rdi         # 8-byte Reload
	subq	%rsi, %rdi
	movq	-40(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi,8), %rbp
	movapd	%xmm2, %xmm1
	.p2align	4, 0x90
.LBB8_10:                               # %.lr.ph120.split
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdx,%r13,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	-8(%rbp), %xmm2
	mulsd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	movq	64(%rsp), %rcx
	movsd	%xmm2, (%rcx)
	movsd	(%rdx,%r13,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	(%rbp), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	movsd	%xmm1, (%rcx)
	addq	$16, %rbp
	addq	$-2, %rdi
	jne	.LBB8_10
	.p2align	4, 0x90
.LBB8_11:                               # %._crit_edge121
                                        #   in Loop: Header=BB8_2 Depth=1
	incq	%r13
	cmpq	-16(%rsp), %r13         # 8-byte Folded Reload
	movapd	%xmm1, %xmm2
	jne	.LBB8_2
.LBB8_12:                               # %._crit_edge125
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	intergroup_score, .Lfunc_end8-intergroup_score
	.cfi_endproc

	.globl	intergroup_score_new
	.p2align	4, 0x90
	.type	intergroup_score_new,@function
intergroup_score_new:                   # @intergroup_score_new
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi71:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 56
.Lcfi74:
	.cfi_offset %rbx, -56
.Lcfi75:
	.cfi_offset %r12, -48
.Lcfi76:
	.cfi_offset %r13, -40
.Lcfi77:
	.cfi_offset %r14, -32
.Lcfi78:
	.cfi_offset %r15, -24
.Lcfi79:
	.cfi_offset %rbp, -16
	movq	%rcx, -64(%rsp)         # 8-byte Spill
	movq	%rsi, -8(%rsp)          # 8-byte Spill
	movq	%rdi, -24(%rsp)         # 8-byte Spill
	movq	64(%rsp), %rax
	movq	$0, (%rax)
	testl	%r8d, %r8d
	jle	.LBB9_12
# BB#1:                                 # %.preheader.lr.ph
	movl	56(%rsp), %eax
	leal	-2(%rax), %r11d
	movl	%r9d, %ecx
	movl	%r8d, %esi
	movq	%rsi, -16(%rsp)         # 8-byte Spill
	movq	%rcx, -48(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1, %ecx
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	movq	-64(%rsp), %rcx         # 8-byte Reload
	leaq	8(%rcx), %rcx
	movq	%rcx, -40(%rsp)         # 8-byte Spill
	xorpd	%xmm0, %xmm0
	xorl	%r13d, %r13d
	xorpd	%xmm1, %xmm1
	movl	%r9d, -52(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB9_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_9 Depth 2
                                        #     Child Loop BB9_14 Depth 2
                                        #       Child Loop BB9_15 Depth 3
                                        #         Child Loop BB9_21 Depth 4
                                        #         Child Loop BB9_26 Depth 4
	testl	%r9d, %r9d
	jle	.LBB9_11
# BB#3:                                 # %.lr.ph98
                                        #   in Loop: Header=BB9_2 Depth=1
	testl	%eax, %eax
	jle	.LBB9_4
# BB#13:                                # %.lr.ph98.split.us.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	-24(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%r13,8), %rbx
	leaq	1(%rbx), %rbp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB9_14:                               # %.lr.ph98.split.us
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_15 Depth 3
                                        #         Child Loop BB9_21 Depth 4
                                        #         Child Loop BB9_26 Depth 4
	movsd	(%rdx,%r13,8), %xmm2    # xmm2 = mem[0],zero
	movq	-64(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r15,8), %xmm2
	movq	-8(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%r15,8), %r14
	xorps	%xmm3, %xmm3
	cvtsi2sdl	penalty(%rip), %xmm3
	leaq	1(%r14), %r8
	xorl	%edi, %edi
	xorpd	%xmm5, %xmm5
	.p2align	4, 0x90
.LBB9_15:                               #   Parent Loop BB9_2 Depth=1
                                        #     Parent Loop BB9_14 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB9_21 Depth 4
                                        #         Child Loop BB9_26 Depth 4
	movslq	%edi, %r10
	movsbq	(%rbx,%r10), %r9
	cmpq	$45, %r9
	movb	(%r14,%r10), %sil
	jne	.LBB9_17
# BB#16:                                #   in Loop: Header=BB9_15 Depth=3
	cmpb	$45, %sil
	je	.LBB9_27
.LBB9_17:                               #   in Loop: Header=BB9_15 Depth=3
	movsbq	%sil, %r12
	movl	%r9d, %ecx
	shlq	$9, %r9
	cmpb	$45, %cl
	cvtsi2sdl	amino_dis(%r9,%r12,4), %xmm4
	addsd	%xmm4, %xmm5
	jne	.LBB9_18
# BB#24:                                #   in Loop: Header=BB9_15 Depth=3
	addsd	%xmm3, %xmm5
	addsd	%xmm5, %xmm4
	cmpb	$45, 1(%rbx,%r10)
	jne	.LBB9_22
# BB#25:                                # %.lr.ph83.us.preheader
                                        #   in Loop: Header=BB9_15 Depth=3
	incq	%r10
	xorps	%xmm5, %xmm5
	cvtsi2sdl	amino_dis+23040(,%r12,4), %xmm5
	addq	%rbp, %r10
	.p2align	4, 0x90
.LBB9_26:                               # %.lr.ph83.us
                                        #   Parent Loop BB9_2 Depth=1
                                        #     Parent Loop BB9_14 Depth=2
                                        #       Parent Loop BB9_15 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	addsd	%xmm5, %xmm4
	incl	%edi
	cmpb	$45, (%r10)
	leaq	1(%r10), %r10
	je	.LBB9_26
	jmp	.LBB9_22
	.p2align	4, 0x90
.LBB9_18:                               #   in Loop: Header=BB9_15 Depth=3
	cmpb	$45, %sil
	jne	.LBB9_27
# BB#19:                                #   in Loop: Header=BB9_15 Depth=3
	addsd	%xmm3, %xmm5
	addsd	%xmm5, %xmm4
	cmpb	$45, 1(%r14,%r10)
	jne	.LBB9_22
# BB#20:                                # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB9_15 Depth=3
	incq	%r10
	xorps	%xmm5, %xmm5
	cvtsi2sdl	amino_dis+180(%r9), %xmm5
	addq	%r8, %r10
	.p2align	4, 0x90
.LBB9_21:                               # %.lr.ph.us
                                        #   Parent Loop BB9_2 Depth=1
                                        #     Parent Loop BB9_14 Depth=2
                                        #       Parent Loop BB9_15 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	addsd	%xmm5, %xmm4
	incl	%edi
	cmpb	$45, (%r10)
	leaq	1(%r10), %r10
	je	.LBB9_21
	.p2align	4, 0x90
.LBB9_22:                               # %._crit_edge84.us
                                        #   in Loop: Header=BB9_15 Depth=3
	cmpl	%r11d, %edi
	movapd	%xmm4, %xmm5
	jg	.LBB9_23
.LBB9_27:                               #   in Loop: Header=BB9_15 Depth=3
	incl	%edi
	cmpl	%eax, %edi
	jl	.LBB9_15
.LBB9_23:                               # %._crit_edge91.us
                                        #   in Loop: Header=BB9_14 Depth=2
	mulsd	%xmm2, %xmm5
	addsd	%xmm5, %xmm1
	movq	64(%rsp), %rcx
	movsd	%xmm1, (%rcx)
	incq	%r15
	cmpq	-48(%rsp), %r15         # 8-byte Folded Reload
	jne	.LBB9_14
	jmp	.LBB9_10
	.p2align	4, 0x90
.LBB9_4:                                # %.lr.ph98.split.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	cmpq	$0, -32(%rsp)           # 8-byte Folded Reload
	jne	.LBB9_6
# BB#5:                                 #   in Loop: Header=BB9_2 Depth=1
                                        # implicit-def: %XMM2
	xorl	%esi, %esi
	cmpl	$1, %r9d
	jne	.LBB9_8
	jmp	.LBB9_10
.LBB9_6:                                # %.lr.ph98.split.prol
                                        #   in Loop: Header=BB9_2 Depth=1
	movsd	(%rdx,%r13,8), %xmm2    # xmm2 = mem[0],zero
	movq	-64(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx), %xmm2
	movapd	%xmm2, %xmm3
	mulsd	%xmm0, %xmm3
	addsd	%xmm3, %xmm1
	movq	64(%rsp), %rcx
	movsd	%xmm1, (%rcx)
	movl	$1, %esi
	cmpl	$1, %r9d
	je	.LBB9_10
.LBB9_8:                                # %.lr.ph98.split.preheader.new
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	-48(%rsp), %rdi         # 8-byte Reload
	subq	%rsi, %rdi
	movq	-40(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi,8), %rbp
	.p2align	4, 0x90
.LBB9_9:                                # %.lr.ph98.split
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdx,%r13,8), %xmm3    # xmm3 = mem[0],zero
	mulsd	-8(%rbp), %xmm3
	mulsd	%xmm0, %xmm3
	addsd	%xmm1, %xmm3
	movq	64(%rsp), %rcx
	movsd	%xmm3, (%rcx)
	movsd	(%rdx,%r13,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	(%rbp), %xmm2
	movapd	%xmm2, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm3, %xmm1
	movsd	%xmm1, (%rcx)
	addq	$16, %rbp
	addq	$-2, %rdi
	jne	.LBB9_9
	.p2align	4, 0x90
.LBB9_10:                               # %._crit_edge99
                                        #   in Loop: Header=BB9_2 Depth=1
	movsd	%xmm2, intergroup_score_new.efficient.0(%rip)
	movl	-52(%rsp), %r9d         # 4-byte Reload
.LBB9_11:                               #   in Loop: Header=BB9_2 Depth=1
	incq	%r13
	cmpq	-16(%rsp), %r13         # 8-byte Folded Reload
	jne	.LBB9_2
.LBB9_12:                               # %._crit_edge101
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	intergroup_score_new, .Lfunc_end9-intergroup_score_new
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI10_0:
	.quad	-4616189618054758400    # double -1
.LCPI10_1:
	.quad	4602678819172646912     # double 0.5
.LCPI10_2:
	.quad	9221120237041090560     # double NaN
	.text
	.globl	score_calc3
	.p2align	4, 0x90
	.type	score_calc3,@function
score_calc3:                            # @score_calc3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi81:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi83:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi84:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi86:
	.cfi_def_cfa_offset 128
.Lcfi87:
	.cfi_offset %rbx, -56
.Lcfi88:
	.cfi_offset %r12, -48
.Lcfi89:
	.cfi_offset %r13, -40
.Lcfi90:
	.cfi_offset %r14, -32
.Lcfi91:
	.cfi_offset %r15, -24
.Lcfi92:
	.cfi_offset %rbp, -16
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	%esi, %ebx
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	(%rdi), %rdi
	callq	strlen
	movq	%rax, %r15
	movl	weight(%rip), %r11d
	testl	%r11d, %r11d
	je	.LBB10_18
# BB#1:
	cmpl	$2, %r11d
	je	.LBB10_6
# BB#2:
	cmpl	$3, %r11d
	jne	.LBB10_17
# BB#3:                                 # %.preheader
	cmpl	$2, %ebx
	jl	.LBB10_4
# BB#5:                                 # %.lr.ph116.preheader
	leal	-1(%rbx), %r14d
	movslq	%ebx, %r12
	movl	%ebx, %r8d
	leaq	7(%r8), %r9
	leaq	-2(%r8), %r10
	movq	%rbx, (%rsp)            # 8-byte Spill
	movl	%ebx, %eax
	addb	$7, %al
	xorpd	%xmm1, %xmm1
	movl	$1, %edi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB10_8:                               # %.lr.ph116
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_12 Depth 2
                                        #     Child Loop BB10_16 Depth 2
	movq	%rbp, %rdx
	leaq	1(%rdx), %rbp
	cmpq	%r12, %rbp
	jge	.LBB10_7
# BB#9:                                 # %.lr.ph112
                                        #   in Loop: Header=BB10_8 Depth=1
	movq	%r10, %rcx
	subq	%rdx, %rcx
	movl	%r9d, %esi
	subl	%edx, %esi
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	(%rbx,%rdx,8), %r13
	testb	$7, %sil
	je	.LBB10_10
# BB#11:                                # %.prol.preheader
                                        #   in Loop: Header=BB10_8 Depth=1
	movl	%eax, %edx
	andb	$7, %dl
	movzbl	%dl, %edx
	negq	%rdx
	movq	%r13, %rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB10_12:                              #   Parent Loop BB10_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rsi,%rdi,8), %xmm1
	decq	%rbx
	addq	$8, %rsi
	cmpq	%rbx, %rdx
	jne	.LBB10_12
# BB#13:                                # %.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB10_8 Depth=1
	movq	%rdi, %rdx
	subq	%rbx, %rdx
	cmpq	$7, %rcx
	jae	.LBB10_15
	jmp	.LBB10_7
.LBB10_10:                              #   in Loop: Header=BB10_8 Depth=1
	movq	%rdi, %rdx
	cmpq	$7, %rcx
	jb	.LBB10_7
.LBB10_15:                              # %.lr.ph112.new
                                        #   in Loop: Header=BB10_8 Depth=1
	movq	%r8, %rcx
	subq	%rdx, %rcx
	leaq	56(%r13,%rdx,8), %rsi
	.p2align	4, 0x90
.LBB10_16:                              #   Parent Loop BB10_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-56(%rsi), %xmm1
	addsd	-48(%rsi), %xmm1
	addsd	-40(%rsi), %xmm1
	addsd	-32(%rsi), %xmm1
	addsd	-24(%rsi), %xmm1
	addsd	-16(%rsi), %xmm1
	addsd	-8(%rsi), %xmm1
	addsd	(%rsi), %xmm1
	addq	$64, %rsi
	addq	$-8, %rcx
	jne	.LBB10_16
.LBB10_7:                               # %.loopexit86
                                        #   in Loop: Header=BB10_8 Depth=1
	incq	%rdi
	addb	$7, %al
	cmpq	%r14, %rbp
	jne	.LBB10_8
	jmp	.LBB10_19
.LBB10_18:
	movq	%rbx, (%rsp)            # 8-byte Spill
	cvtsi2sdl	%ebx, %xmm0
	movsd	.LCPI10_0(%rip), %xmm1  # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	.LCPI10_1(%rip), %xmm1
	jmp	.LBB10_19
.LBB10_6:
	movl	%ebx, %eax
	shrl	$31, %eax
	movq	%rbx, (%rsp)            # 8-byte Spill
	addl	%ebx, %eax
	sarl	%eax
	cvtsi2sdl	%eax, %xmm1
.LBB10_19:                              # %.loopexit87
	xorpd	%xmm0, %xmm0
	movq	(%rsp), %rcx            # 8-byte Reload
	cmpl	$2, %ecx
	jl	.LBB10_38
# BB#20:                                # %.lr.ph106
	leal	-1(%rcx), %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leal	-2(%r15), %r13d
	movslq	%ecx, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%ecx, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorpd	%xmm0, %xmm0
	movl	$1, %r14d
	xorl	%r12d, %r12d
	movsd	%xmm1, 48(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB10_23:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_21 Depth 2
                                        #     Child Loop BB10_25 Depth 2
                                        #       Child Loop BB10_26 Depth 3
                                        #         Child Loop BB10_33 Depth 4
                                        #         Child Loop BB10_30 Depth 4
	leaq	1(%r12), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	cmpq	32(%rsp), %rcx          # 8-byte Folded Reload
	movq	%r14, 64(%rsp)          # 8-byte Spill
	jge	.LBB10_22
# BB#24:                                # %.lr.ph101
                                        #   in Loop: Header=BB10_23 Depth=1
	testl	%r15d, %r15d
	jle	.LBB10_21
	.p2align	4, 0x90
.LBB10_25:                              # %.lr.ph101.split.us
                                        #   Parent Loop BB10_23 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_26 Depth 3
                                        #         Child Loop BB10_33 Depth 4
                                        #         Child Loop BB10_30 Depth 4
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx,%r12,8), %rsi
	movl	$score_calc3.mseq1, %edi
	callq	strcpy
	movq	(%rbx,%r14,8), %rsi
	movl	$score_calc3.mseq2, %edi
	callq	strcpy
	xorl	%eax, %eax
	cmpl	$0, scoremtx(%rip)
	movl	$0, %r9d
	movl	$400, %ecx              # imm = 0x190
	cmovel	%ecx, %r9d
	movslq	penalty(%rip), %r8
	movslq	n_dis+96(%rip), %rdx
	subq	%rdx, %r8
	xorl	%r10d, %r10d
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB10_26:                              #   Parent Loop BB10_23 Depth=1
                                        #     Parent Loop BB10_25 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB10_33 Depth 4
                                        #         Child Loop BB10_30 Depth 4
	movslq	%edi, %rbp
	movsbq	score_calc3.mseq1(%rbp), %rsi
	cmpq	$45, %rsi
	movb	score_calc3.mseq2(%rbp), %bl
	jne	.LBB10_28
# BB#27:                                #   in Loop: Header=BB10_26 Depth=3
	cmpb	$45, %bl
	je	.LBB10_36
.LBB10_28:                              # %thread-pre-split.us
                                        #   in Loop: Header=BB10_26 Depth=3
	movsbq	%bl, %rcx
	movq	%rsi, %rdx
	shlq	$9, %rdx
	movslq	amino_dis(%rdx,%rcx,4), %rcx
	movslq	%r9d, %rdx
	addq	%rcx, %rdx
	addq	%rdx, %rax
	incl	%r10d
	cmpb	$45, %sil
	jne	.LBB10_31
# BB#29:                                # %.preheader147.preheader
                                        #   in Loop: Header=BB10_26 Depth=3
	leal	-1(%rbp), %edi
	leaq	score_calc3.mseq1+1(%rbp), %rdx
	.p2align	4, 0x90
.LBB10_30:                              # %.preheader147
                                        #   Parent Loop BB10_23 Depth=1
                                        #     Parent Loop BB10_25 Depth=2
                                        #       Parent Loop BB10_26 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%edi
	cmpb	$45, (%rdx)
	leaq	1(%rdx), %rdx
	je	.LBB10_30
	jmp	.LBB10_34
	.p2align	4, 0x90
.LBB10_31:                              #   in Loop: Header=BB10_26 Depth=3
	cmpb	$45, %bl
	jne	.LBB10_36
# BB#32:                                # %.preheader148.preheader
                                        #   in Loop: Header=BB10_26 Depth=3
	leal	-1(%rbp), %edi
	leaq	score_calc3.mseq2+1(%rbp), %rdx
	.p2align	4, 0x90
.LBB10_33:                              # %.preheader148
                                        #   Parent Loop BB10_23 Depth=1
                                        #     Parent Loop BB10_25 Depth=2
                                        #       Parent Loop BB10_26 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%edi
	cmpb	$45, (%rdx)
	leaq	1(%rdx), %rdx
	je	.LBB10_33
.LBB10_34:                              #   in Loop: Header=BB10_26 Depth=3
	addq	%r8, %rax
	cmpl	%r13d, %edi
	jg	.LBB10_35
.LBB10_36:                              #   in Loop: Header=BB10_26 Depth=3
	incl	%edi
	cmpl	%r15d, %edi
	jl	.LBB10_26
.LBB10_35:                              # %._crit_edge.us
                                        #   in Loop: Header=BB10_25 Depth=2
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r10d, %xmm1
	divsd	%xmm1, %xmm0
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%r12,8), %rax
	mulsd	(%rax,%r14,8), %xmm0
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	incq	%r14
	cmpq	16(%rsp), %r14          # 8-byte Folded Reload
	jne	.LBB10_25
	jmp	.LBB10_22
	.p2align	4, 0x90
.LBB10_21:                              # %.lr.ph101.split
                                        #   Parent Loop BB10_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx,%r12,8), %rsi
	movl	$score_calc3.mseq1, %edi
	callq	strcpy
	movq	(%rbx,%r14,8), %rsi
	movl	$score_calc3.mseq2, %edi
	callq	strcpy
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%r12,8), %rax
	movsd	(%rax,%r14,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	.LCPI10_2(%rip), %xmm0
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	incq	%r14
	cmpq	%r14, 16(%rsp)          # 8-byte Folded Reload
	jne	.LBB10_21
	.p2align	4, 0x90
.LBB10_22:                              # %.loopexit
                                        #   in Loop: Header=BB10_23 Depth=1
	movq	64(%rsp), %r14          # 8-byte Reload
	incq	%r14
	movq	56(%rsp), %r12          # 8-byte Reload
	cmpq	40(%rsp), %r12          # 8-byte Folded Reload
	movsd	48(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	jne	.LBB10_23
# BB#37:                                # %._crit_edge107.loopexit
	movl	weight(%rip), %r11d
	testl	%r11d, %r11d
	jne	.LBB10_40
	jmp	.LBB10_39
.LBB10_4:
	movl	$3, %r11d
	xorpd	%xmm1, %xmm1
	xorpd	%xmm0, %xmm0
.LBB10_38:                              # %._crit_edge107
	testl	%r11d, %r11d
	jne	.LBB10_40
.LBB10_39:                              # %select.true.sink
	divsd	%xmm1, %xmm0
.LBB10_40:                              # %select.end
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_17:
	movq	stderr(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end10:
	.size	score_calc3, .Lfunc_end10-score_calc3
	.cfi_endproc

	.globl	score_calc5
	.p2align	4, 0x90
	.type	score_calc5,@function
score_calc5:                            # @score_calc5
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi95:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi96:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi97:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi99:
	.cfi_def_cfa_offset 112
.Lcfi100:
	.cfi_offset %rbx, -56
.Lcfi101:
	.cfi_offset %r12, -48
.Lcfi102:
	.cfi_offset %r13, -40
.Lcfi103:
	.cfi_offset %r14, -32
.Lcfi104:
	.cfi_offset %r15, -24
.Lcfi105:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	%esi, %r12d
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	(%rdi), %rdi
	callq	strlen
	testl	%r12d, %r12d
	jle	.LBB11_1
# BB#3:                                 # %.lr.ph212
	movslq	%ebp, %r8
	leal	-2(%rax), %r10d
	testl	%eax, %eax
	movl	%r8d, %r11d
	movl	%r12d, %ebx
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	jle	.LBB11_4
# BB#17:                                # %.lr.ph212.split.us.preheader
	cvtsi2sdl	penalty(%rip), %xmm1
	xorpd	%xmm0, %xmm0
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB11_18:                              # %.lr.ph212.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_20 Depth 2
                                        #       Child Loop BB11_26 Depth 3
                                        #       Child Loop BB11_34 Depth 3
	cmpq	%r11, %r15
	je	.LBB11_29
# BB#19:                                # %.lr.ph202.us
                                        #   in Loop: Header=BB11_18 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%r15,8), %rcx
	movsd	(%rcx,%r8,8), %xmm2     # xmm2 = mem[0],zero
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%r15,8), %rbx
	movq	(%rcx,%r8,8), %rdi
	leaq	1(%rdi), %r9
	leaq	1(%rbx), %r14
	xorl	%edx, %edx
	xorpd	%xmm3, %xmm3
	.p2align	4, 0x90
.LBB11_20:                              #   Parent Loop BB11_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB11_26 Depth 3
                                        #       Child Loop BB11_34 Depth 3
	movslq	%edx, %rsi
	movsbq	(%rbx,%rsi), %rcx
	cmpq	$45, %rcx
	movsbq	(%rdi,%rsi), %rbp
	jne	.LBB11_23
# BB#21:                                #   in Loop: Header=BB11_20 Depth=2
	cmpb	$45, %bpl
	je	.LBB11_22
# BB#32:                                # %.preheader161.us
                                        #   in Loop: Header=BB11_20 Depth=2
	shlq	$9, %rcx
	cvtsi2sdl	amino_dis(%rcx,%rbp,4), %xmm4
	addsd	%xmm4, %xmm3
	addsd	%xmm1, %xmm3
	cmpb	$45, 1(%rbx,%rsi)
	jne	.LBB11_27
# BB#33:                                # %.lr.ph195.us.preheader
                                        #   in Loop: Header=BB11_20 Depth=2
	incq	%rsi
	leaq	(%rdi,%rsi), %rbp
	addq	%r14, %rsi
	.p2align	4, 0x90
.LBB11_34:                              # %.lr.ph195.us
                                        #   Parent Loop BB11_18 Depth=1
                                        #     Parent Loop BB11_20 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsbq	(%rbp), %rcx
	xorps	%xmm4, %xmm4
	cvtsi2sdl	amino_dis+23040(,%rcx,4), %xmm4
	addsd	%xmm4, %xmm3
	incl	%edx
	incq	%rbp
	cmpb	$45, (%rsi)
	leaq	1(%rsi), %rsi
	je	.LBB11_34
	jmp	.LBB11_27
	.p2align	4, 0x90
.LBB11_23:                              #   in Loop: Header=BB11_20 Depth=2
	shlq	$9, %rcx
	cvtsi2sdl	amino_dis(%rcx,%rbp,4), %xmm4
	addsd	%xmm4, %xmm3
	cmpb	$45, %bpl
	jne	.LBB11_22
# BB#24:                                # %.preheader162.us
                                        #   in Loop: Header=BB11_20 Depth=2
	addsd	%xmm1, %xmm3
	cmpb	$45, 1(%rdi,%rsi)
	jne	.LBB11_27
# BB#25:                                # %.lr.ph191.us.preheader
                                        #   in Loop: Header=BB11_20 Depth=2
	incq	%rsi
	leaq	(%rbx,%rsi), %rbp
	addq	%r9, %rsi
	.p2align	4, 0x90
.LBB11_26:                              # %.lr.ph191.us
                                        #   Parent Loop BB11_18 Depth=1
                                        #     Parent Loop BB11_20 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsbq	(%rbp), %rcx
	shlq	$9, %rcx
	xorps	%xmm4, %xmm4
	cvtsi2sdl	amino_dis+180(%rcx), %xmm4
	addsd	%xmm4, %xmm3
	incl	%edx
	incq	%rbp
	cmpb	$45, (%rsi)
	leaq	1(%rsi), %rsi
	je	.LBB11_26
.LBB11_27:                              # %._crit_edge196.us
                                        #   in Loop: Header=BB11_20 Depth=2
	cmpl	%r10d, %edx
	jg	.LBB11_28
.LBB11_22:                              #   in Loop: Header=BB11_20 Depth=2
	incl	%edx
	cmpl	%eax, %edx
	jl	.LBB11_20
.LBB11_28:                              # %._crit_edge203.us
                                        #   in Loop: Header=BB11_18 Depth=1
	mulsd	%xmm3, %xmm2
	addsd	%xmm2, %xmm0
	movq	24(%rsp), %rbx          # 8-byte Reload
.LBB11_29:                              #   in Loop: Header=BB11_18 Depth=1
	incq	%r15
	cmpq	%rbx, %r15
	jne	.LBB11_18
	jmp	.LBB11_30
.LBB11_1:
	xorpd	%xmm0, %xmm0
	jmp	.LBB11_2
.LBB11_4:                               # %.lr.ph212.split.preheader
	testb	$1, %bl
	jne	.LBB11_6
# BB#5:
	xorpd	%xmm0, %xmm0
	xorl	%edx, %edx
	cmpl	$1, %r12d
	jne	.LBB11_11
	jmp	.LBB11_30
.LBB11_6:                               # %.lr.ph212.split.prol
	testl	%ebp, %ebp
	je	.LBB11_7
# BB#8:
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx), %rcx
	xorpd	%xmm1, %xmm1
	movsd	(%rcx,%r8,8), %xmm0     # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	addsd	%xmm1, %xmm0
	jmp	.LBB11_9
.LBB11_7:
	xorpd	%xmm0, %xmm0
.LBB11_9:                               # %.lr.ph212.split.prol.loopexit
	movl	$1, %edx
	cmpl	$1, %r12d
	je	.LBB11_30
.LBB11_11:                              # %.lr.ph212.split.preheader.new
	movq	%r11, %rsi
	subq	%rdx, %rsi
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	8(%rcx,%rdx,8), %rdi
	xorpd	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB11_12:                              # %.lr.ph212.split
                                        # =>This Inner Loop Header: Depth=1
	testq	%rsi, %rsi
	je	.LBB11_14
# BB#13:                                #   in Loop: Header=BB11_12 Depth=1
	movq	-8(%rdi), %rcx
	movsd	(%rcx,%r8,8), %xmm2     # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	addsd	%xmm2, %xmm0
.LBB11_14:                              # %.lr.ph212.split.1266
                                        #   in Loop: Header=BB11_12 Depth=1
	leaq	1(%rdx), %rcx
	cmpq	%r11, %rcx
	je	.LBB11_16
# BB#15:                                #   in Loop: Header=BB11_12 Depth=1
	movq	(%rdi), %rcx
	movsd	(%rcx,%r8,8), %xmm2     # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	addsd	%xmm2, %xmm0
.LBB11_16:                              #   in Loop: Header=BB11_12 Depth=1
	addq	$2, %rdx
	addq	$-2, %rsi
	addq	$16, %rdi
	cmpq	%rbx, %rdx
	jne	.LBB11_12
.LBB11_30:                              # %.preheader160
	cmpl	$2, %r12d
	jl	.LBB11_2
# BB#31:                                # %.lr.ph188
	xorps	%xmm1, %xmm1
	cvtsi2sdl	penalty(%rip), %xmm1
	movslq	%r12d, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	decl	%r12d
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movl	$1, %r13d
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB11_36:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_39 Depth 2
                                        #       Child Loop BB11_43 Depth 3
                                        #         Child Loop BB11_51 Depth 4
                                        #         Child Loop BB11_47 Depth 4
	leaq	1(%r9), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	cmpq	32(%rsp), %rcx          # 8-byte Folded Reload
	jge	.LBB11_35
# BB#37:                                # %.lr.ph184
                                        #   in Loop: Header=BB11_36 Depth=1
	cmpq	%r11, %r9
	je	.LBB11_35
# BB#38:                                # %.lr.ph184.split.preheader
                                        #   in Loop: Header=BB11_36 Depth=1
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB11_39:                              # %.lr.ph184.split
                                        #   Parent Loop BB11_36 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB11_43 Depth 3
                                        #         Child Loop BB11_51 Depth 4
                                        #         Child Loop BB11_47 Depth 4
	cmpq	%r11, %rbp
	je	.LBB11_55
# BB#40:                                #   in Loop: Header=BB11_39 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%r9,8), %rcx
	movsd	(%rcx,%rbp,8), %xmm2    # xmm2 = mem[0],zero
	testl	%eax, %eax
	jle	.LBB11_41
# BB#42:                                # %.lr.ph174.preheader
                                        #   in Loop: Header=BB11_39 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%r9,8), %rsi
	movq	(%rcx,%rbp,8), %rdx
	leaq	1(%rdx), %rbx
	leaq	1(%rsi), %r14
	xorl	%r8d, %r8d
	xorpd	%xmm3, %xmm3
	.p2align	4, 0x90
.LBB11_43:                              # %.lr.ph174
                                        #   Parent Loop BB11_36 Depth=1
                                        #     Parent Loop BB11_39 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB11_51 Depth 4
                                        #         Child Loop BB11_47 Depth 4
	movslq	%r8d, %r15
	movsbq	(%rsi,%r15), %rcx
	cmpq	$45, %rcx
	movsbq	(%rdx,%r15), %rdi
	jne	.LBB11_48
# BB#44:                                #   in Loop: Header=BB11_43 Depth=3
	cmpb	$45, %dil
	je	.LBB11_53
# BB#45:                                # %.preheader
                                        #   in Loop: Header=BB11_43 Depth=3
	shlq	$9, %rcx
	xorps	%xmm4, %xmm4
	cvtsi2sdl	amino_dis(%rcx,%rdi,4), %xmm4
	addsd	%xmm4, %xmm3
	addsd	%xmm1, %xmm3
	cmpb	$45, 1(%rsi,%r15)
	jne	.LBB11_52
# BB#46:                                # %.lr.ph167.preheader
                                        #   in Loop: Header=BB11_43 Depth=3
	incq	%r15
	leaq	(%rdx,%r15), %rdi
	addq	%r14, %r15
	.p2align	4, 0x90
.LBB11_47:                              # %.lr.ph167
                                        #   Parent Loop BB11_36 Depth=1
                                        #     Parent Loop BB11_39 Depth=2
                                        #       Parent Loop BB11_43 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsbq	(%rdi), %rcx
	xorps	%xmm4, %xmm4
	cvtsi2sdl	amino_dis+23040(,%rcx,4), %xmm4
	addsd	%xmm4, %xmm3
	incl	%r8d
	incq	%rdi
	cmpb	$45, (%r15)
	leaq	1(%r15), %r15
	je	.LBB11_47
	jmp	.LBB11_52
	.p2align	4, 0x90
.LBB11_48:                              #   in Loop: Header=BB11_43 Depth=3
	shlq	$9, %rcx
	xorps	%xmm4, %xmm4
	cvtsi2sdl	amino_dis(%rcx,%rdi,4), %xmm4
	addsd	%xmm4, %xmm3
	cmpb	$45, %dil
	jne	.LBB11_53
# BB#49:                                # %.preheader159
                                        #   in Loop: Header=BB11_43 Depth=3
	addsd	%xmm1, %xmm3
	cmpb	$45, 1(%rdx,%r15)
	jne	.LBB11_52
# BB#50:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB11_43 Depth=3
	incq	%r15
	leaq	(%rsi,%r15), %r12
	addq	%rbx, %r15
	.p2align	4, 0x90
.LBB11_51:                              # %.lr.ph
                                        #   Parent Loop BB11_36 Depth=1
                                        #     Parent Loop BB11_39 Depth=2
                                        #       Parent Loop BB11_43 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsbq	(%r12), %rcx
	shlq	$9, %rcx
	xorps	%xmm4, %xmm4
	cvtsi2sdl	amino_dis+180(%rcx), %xmm4
	addsd	%xmm4, %xmm3
	incl	%r8d
	incq	%r12
	cmpb	$45, (%r15)
	leaq	1(%r15), %r15
	je	.LBB11_51
.LBB11_52:                              # %._crit_edge
                                        #   in Loop: Header=BB11_43 Depth=3
	cmpl	%r10d, %r8d
	jg	.LBB11_54
.LBB11_53:                              #   in Loop: Header=BB11_43 Depth=3
	incl	%r8d
	cmpl	%eax, %r8d
	jl	.LBB11_43
	jmp	.LBB11_54
.LBB11_41:                              #   in Loop: Header=BB11_39 Depth=2
	xorpd	%xmm3, %xmm3
.LBB11_54:                              # %._crit_edge175
                                        #   in Loop: Header=BB11_39 Depth=2
	mulsd	%xmm3, %xmm2
	addsd	%xmm2, %xmm0
	movq	24(%rsp), %rbx          # 8-byte Reload
.LBB11_55:                              #   in Loop: Header=BB11_39 Depth=2
	incq	%rbp
	cmpq	%rbx, %rbp
	jne	.LBB11_39
.LBB11_35:                              # %.loopexit
                                        #   in Loop: Header=BB11_36 Depth=1
	incq	%r13
	movq	48(%rsp), %r9           # 8-byte Reload
	cmpq	40(%rsp), %r9           # 8-byte Folded Reload
	jne	.LBB11_36
.LBB11_2:                               # %._crit_edge189
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	score_calc5, .Lfunc_end11-score_calc5
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI12_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	score_calc4
	.p2align	4, 0x90
	.type	score_calc4,@function
score_calc4:                            # @score_calc4
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi106:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi107:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi108:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi109:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi110:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi112:
	.cfi_def_cfa_offset 160
.Lcfi113:
	.cfi_offset %rbx, -56
.Lcfi114:
	.cfi_offset %r12, -48
.Lcfi115:
	.cfi_offset %r13, -40
.Lcfi116:
	.cfi_offset %r14, -32
.Lcfi117:
	.cfi_offset %r15, -24
.Lcfi118:
	.cfi_offset %rbp, -16
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movl	%esi, %r14d
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	(%rdi), %rdi
	callq	strlen
	xorpd	%xmm1, %xmm1
	cmpl	$2, %r14d
	xorpd	%xmm0, %xmm0
	jl	.LBB12_38
# BB#1:                                 # %.lr.ph101
	leal	-1(%r14), %ecx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	mix(%rip), %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	xorl	%esi, %esi
	cmpl	$0, scoremtx(%rip)
	leal	-2(%rax), %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	$400, %edx              # imm = 0x190
	cmovnel	%esi, %edx
	movslq	penalty(%rip), %rcx
	movslq	n_dis+2496(%rip), %rdi
	subq	%rdi, %rcx
	movslq	%r14d, %rdi
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movl	%r14d, %edi
	leaq	3(%rdi), %rbp
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	leaq	-2(%rdi), %rdi
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	addb	$3, %r14b
	xorpd	%xmm0, %xmm0
	movl	$1, %r13d
	movsd	.LCPI12_0(%rip), %xmm2  # xmm2 = mem[0],zero
	movslq	%edx, %rbp
	xorpd	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB12_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_8 Depth 2
                                        #     Child Loop BB12_14 Depth 2
                                        #     Child Loop BB12_24 Depth 2
                                        #       Child Loop BB12_27 Depth 3
                                        #         Child Loop BB12_34 Depth 4
                                        #         Child Loop BB12_31 Depth 4
	movq	%rsi, %rdx
	leaq	1(%rdx), %r8
	cmpq	64(%rsp), %r8           # 8-byte Folded Reload
	movq	%r14, 96(%rsp)          # 8-byte Spill
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movq	%r13, 80(%rsp)          # 8-byte Spill
	jge	.LBB12_2
# BB#4:                                 # %.lr.ph94
                                        #   in Loop: Header=BB12_3 Depth=1
	testl	%eax, %eax
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	jle	.LBB12_5
# BB#23:                                # %.lr.ph94.split.us.preheader
                                        #   in Loop: Header=BB12_3 Depth=1
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	(%rsi,%rdx,8), %r12
	leaq	1(%r12), %r14
	.p2align	4, 0x90
.LBB12_24:                              # %.lr.ph94.split.us
                                        #   Parent Loop BB12_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_27 Depth 3
                                        #         Child Loop BB12_34 Depth 4
                                        #         Child Loop BB12_31 Depth 4
	cmpl	$1, 8(%rsp)             # 4-byte Folded Reload
	movapd	%xmm2, %xmm3
	je	.LBB12_26
# BB#25:                                # %.lr.ph94.split.us
                                        #   in Loop: Header=BB12_24 Depth=2
	movq	16(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%r13,8), %xmm3    # xmm3 = mem[0],zero
.LBB12_26:                              # %.lr.ph94.split.us
                                        #   in Loop: Header=BB12_24 Depth=2
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx,%r13,8), %r10
	leaq	1(%r10), %r15
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB12_27:                              #   Parent Loop BB12_3 Depth=1
                                        #     Parent Loop BB12_24 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB12_34 Depth 4
                                        #         Child Loop BB12_31 Depth 4
	movslq	%r11d, %rdi
	movsbq	(%r12,%rdi), %rsi
	cmpq	$45, %rsi
	movb	(%r10,%rdi), %dl
	jne	.LBB12_29
# BB#28:                                #   in Loop: Header=BB12_27 Depth=3
	cmpb	$45, %dl
	je	.LBB12_37
.LBB12_29:                              # %thread-pre-split.us
                                        #   in Loop: Header=BB12_27 Depth=3
	movsbq	%dl, %r8
	movq	%rsi, %rbx
	shlq	$9, %rbx
	movslq	amino_dis(%rbx,%r8,4), %rbx
	addq	%rbp, %rbx
	addq	%rbx, %r9
	addsd	%xmm3, %xmm1
	cmpb	$45, %sil
	jne	.LBB12_32
# BB#30:                                # %.preheader.preheader
                                        #   in Loop: Header=BB12_27 Depth=3
	leal	-1(%rdi), %r11d
	addq	%r14, %rdi
	.p2align	4, 0x90
.LBB12_31:                              # %.preheader
                                        #   Parent Loop BB12_3 Depth=1
                                        #     Parent Loop BB12_24 Depth=2
                                        #       Parent Loop BB12_27 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%r11d
	cmpb	$45, (%rdi)
	leaq	1(%rdi), %rdi
	je	.LBB12_31
	jmp	.LBB12_35
	.p2align	4, 0x90
.LBB12_32:                              #   in Loop: Header=BB12_27 Depth=3
	cmpb	$45, %dl
	jne	.LBB12_37
# BB#33:                                # %.preheader123.preheader
                                        #   in Loop: Header=BB12_27 Depth=3
	leal	-1(%rdi), %r11d
	addq	%r15, %rdi
	.p2align	4, 0x90
.LBB12_34:                              # %.preheader123
                                        #   Parent Loop BB12_3 Depth=1
                                        #     Parent Loop BB12_24 Depth=2
                                        #       Parent Loop BB12_27 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%r11d
	cmpb	$45, (%rdi)
	leaq	1(%rdi), %rdi
	je	.LBB12_34
.LBB12_35:                              #   in Loop: Header=BB12_27 Depth=3
	addq	%rcx, %r9
	cmpl	12(%rsp), %r11d         # 4-byte Folded Reload
	jg	.LBB12_36
.LBB12_37:                              #   in Loop: Header=BB12_27 Depth=3
	incl	%r11d
	cmpl	%eax, %r11d
	jl	.LBB12_27
.LBB12_36:                              # %._crit_edge.us
                                        #   in Loop: Header=BB12_24 Depth=2
	cvtsi2sdq	%r9, %xmm4
	mulsd	%xmm4, %xmm3
	addsd	%xmm3, %xmm0
	incq	%r13
	cmpq	24(%rsp), %r13          # 8-byte Folded Reload
	jne	.LBB12_24
	jmp	.LBB12_2
.LBB12_5:                               # %.lr.ph94.split.preheader
                                        #   in Loop: Header=BB12_3 Depth=1
	movq	48(%rsp), %rdi          # 8-byte Reload
	subq	%rdx, %rdi
	movq	40(%rsp), %r9           # 8-byte Reload
	subq	%rdx, %r9
	testb	$3, %dil
	je	.LBB12_6
# BB#7:                                 # %.lr.ph94.split.prol.preheader
                                        #   in Loop: Header=BB12_3 Depth=1
	movl	%r14d, %edx
	andb	$3, %dl
	movzbl	%dl, %ebx
	negq	%rbx
	movq	16(%rsp), %rdx          # 8-byte Reload
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB12_8:                               # %.lr.ph94.split.prol
                                        #   Parent Loop BB12_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$1, 8(%rsp)             # 4-byte Folded Reload
	xorpd	%xmm3, %xmm3
	je	.LBB12_10
# BB#9:                                 # %.lr.ph94.split.prol
                                        #   in Loop: Header=BB12_8 Depth=2
	mulsd	(%rdx,%r13,8), %xmm3
.LBB12_10:                              # %.lr.ph94.split.prol
                                        #   in Loop: Header=BB12_8 Depth=2
	addsd	%xmm3, %xmm0
	decq	%rdi
	addq	$8, %rdx
	cmpq	%rdi, %rbx
	jne	.LBB12_8
# BB#11:                                # %.lr.ph94.split.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB12_3 Depth=1
	movq	%r13, %rdx
	subq	%rdi, %rdx
	cmpq	$3, %r9
	jae	.LBB12_13
	jmp	.LBB12_2
.LBB12_6:                               #   in Loop: Header=BB12_3 Depth=1
	movq	%r13, %rdx
	cmpq	$3, %r9
	jb	.LBB12_2
.LBB12_13:                              # %.lr.ph94.split.preheader.new
                                        #   in Loop: Header=BB12_3 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	subq	%rdx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	24(%rsi,%rdx,8), %rbx
	.p2align	4, 0x90
.LBB12_14:                              # %.lr.ph94.split
                                        #   Parent Loop BB12_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$1, 8(%rsp)             # 4-byte Folded Reload
	xorpd	%xmm3, %xmm3
	xorpd	%xmm4, %xmm4
	je	.LBB12_16
# BB#15:                                # %.lr.ph94.split
                                        #   in Loop: Header=BB12_14 Depth=2
	movsd	-24(%rbx), %xmm4        # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
.LBB12_16:                              # %.lr.ph94.split
                                        #   in Loop: Header=BB12_14 Depth=2
	addsd	%xmm4, %xmm0
	xorpd	%xmm4, %xmm4
	je	.LBB12_18
# BB#17:                                # %.lr.ph94.split
                                        #   in Loop: Header=BB12_14 Depth=2
	movsd	-16(%rbx), %xmm4        # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
.LBB12_18:                              # %.lr.ph94.split
                                        #   in Loop: Header=BB12_14 Depth=2
	addsd	%xmm4, %xmm0
	xorpd	%xmm4, %xmm4
	je	.LBB12_20
# BB#19:                                # %.lr.ph94.split
                                        #   in Loop: Header=BB12_14 Depth=2
	movsd	-8(%rbx), %xmm4         # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
.LBB12_20:                              # %.lr.ph94.split
                                        #   in Loop: Header=BB12_14 Depth=2
	addsd	%xmm4, %xmm0
	je	.LBB12_22
# BB#21:                                # %.lr.ph94.split
                                        #   in Loop: Header=BB12_14 Depth=2
	mulsd	(%rbx), %xmm3
.LBB12_22:                              # %.lr.ph94.split
                                        #   in Loop: Header=BB12_14 Depth=2
	addsd	%xmm3, %xmm0
	addq	$32, %rbx
	addq	$-4, %rdi
	jne	.LBB12_14
	.p2align	4, 0x90
.LBB12_2:                               # %.loopexit
                                        #   in Loop: Header=BB12_3 Depth=1
	movq	80(%rsp), %r13          # 8-byte Reload
	incq	%r13
	movq	96(%rsp), %r14          # 8-byte Reload
	addb	$3, %r14b
	movq	88(%rsp), %rsi          # 8-byte Reload
	cmpq	72(%rsp), %rsi          # 8-byte Folded Reload
	jne	.LBB12_3
.LBB12_38:                              # %._crit_edge102
	divsd	%xmm1, %xmm0
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	score_calc4, .Lfunc_end12-score_calc4
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI13_0:
	.long	1176255488              # float 9999
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI13_1:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	upg2
	.p2align	4, 0x90
	.type	upg2,@function
upg2:                                   # @upg2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi119:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi120:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi121:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi122:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi123:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 56
	subq	$400120, %rsp           # imm = 0x61AF8
.Lcfi125:
	.cfi_def_cfa_offset 400176
.Lcfi126:
	.cfi_offset %rbx, -56
.Lcfi127:
	.cfi_offset %r12, -48
.Lcfi128:
	.cfi_offset %r13, -40
.Lcfi129:
	.cfi_offset %r14, -32
.Lcfi130:
	.cfi_offset %r15, -24
.Lcfi131:
	.cfi_offset %rbp, -16
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	%rdx, %r12
	movq	%rsi, %r13
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	upg2.pair(%rip), %rbx
	testq	%rbx, %rbx
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	jne	.LBB13_2
# BB#1:
	movl	njob(%rip), %edi
	movl	%edi, %esi
	callq	AllocateCharMtx
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rbx
	movq	%rbx, upg2.pair(%rip)
.LBB13_2:                               # %.preheader165
	testl	%edi, %edi
	jle	.LBB13_69
# BB#3:                                 # %.preheader163.lr.ph
	leal	-1(%rdi), %r14d
	leaq	8(,%r14,8), %rdx
	leaq	112(%rsp), %rdi
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	callq	memset
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%edi, %r15d
	leaq	-1(%r15), %rcx
	movl	%r15d, %edx
	andl	$7, %edx
	.p2align	4, 0x90
.LBB13_4:                               # %.preheader163.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_5 Depth 2
                                        #     Child Loop BB13_7 Depth 2
	xorl	%esi, %esi
	testq	%rdx, %rdx
	je	.LBB13_6
	.p2align	4, 0x90
.LBB13_5:                               #   Parent Loop BB13_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rbp,8), %rax
	movb	$0, (%rax,%rsi)
	incq	%rsi
	cmpq	%rsi, %rdx
	jne	.LBB13_5
.LBB13_6:                               # %.prol.loopexit278
                                        #   in Loop: Header=BB13_4 Depth=1
	cmpq	$7, %rcx
	jb	.LBB13_8
	.p2align	4, 0x90
.LBB13_7:                               #   Parent Loop BB13_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rbp,8), %rax
	movb	$0, (%rax,%rsi)
	movq	(%rbx,%rbp,8), %rax
	movb	$0, 1(%rax,%rsi)
	movq	(%rbx,%rbp,8), %rax
	movb	$0, 2(%rax,%rsi)
	movq	(%rbx,%rbp,8), %rax
	movb	$0, 3(%rax,%rsi)
	movq	(%rbx,%rbp,8), %rax
	movb	$0, 4(%rax,%rsi)
	movq	(%rbx,%rbp,8), %rax
	movb	$0, 5(%rax,%rsi)
	movq	(%rbx,%rbp,8), %rax
	movb	$0, 6(%rax,%rsi)
	movq	(%rbx,%rbp,8), %rax
	movb	$0, 7(%rax,%rsi)
	addq	$8, %rsi
	cmpq	%rsi, %r15
	jne	.LBB13_7
.LBB13_8:                               # %._crit_edge204.us
                                        #   in Loop: Header=BB13_4 Depth=1
	incq	%rbp
	cmpq	%rsi, %rbp
	jne	.LBB13_4
# BB#9:                                 # %.preheader162
	testl	%edi, %edi
	jle	.LBB13_69
# BB#10:                                # %.lr.ph201.preheader
	leaq	-1(%r15), %r11
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	andq	$7, %rdx
	je	.LBB13_12
	.p2align	4, 0x90
.LBB13_11:                              # %.lr.ph201.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rcx,8), %rax
	movb	$1, (%rax,%rcx)
	incq	%rcx
	cmpq	%rcx, %rdx
	jne	.LBB13_11
.LBB13_12:                              # %.lr.ph201.prol.loopexit
	cmpq	$7, %r11
	jb	.LBB13_14
	.p2align	4, 0x90
.LBB13_13:                              # %.lr.ph201
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rcx,8), %rax
	movb	$1, (%rax,%rcx)
	movq	8(%rbx,%rcx,8), %rax
	movb	$1, 1(%rax,%rcx)
	movq	16(%rbx,%rcx,8), %rax
	movb	$1, 2(%rax,%rcx)
	movq	24(%rbx,%rcx,8), %rax
	movb	$1, 3(%rax,%rcx)
	movq	32(%rbx,%rcx,8), %rax
	movb	$1, 4(%rax,%rcx)
	movq	40(%rbx,%rcx,8), %rax
	movb	$1, 5(%rax,%rcx)
	movq	48(%rbx,%rcx,8), %rax
	movb	$1, 6(%rax,%rcx)
	movq	56(%rbx,%rcx,8), %rax
	movb	$1, 7(%rax,%rcx)
	addq	$8, %rcx
	cmpq	%rcx, %r15
	jne	.LBB13_13
.LBB13_14:                              # %.preheader161
	cmpl	$2, %edi
	jl	.LBB13_69
# BB#15:                                # %.preheader160.lr.ph
	movslq	%edi, %r8
	movl	%r14d, %r9d
	leaq	-2(%r15), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	%r15d, %eax
	andl	$1, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%r15d, %eax
	andl	$7, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%r11d, %eax
	andl	$1, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movss	.LCPI13_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movsd	.LCPI13_1(%rip), %xmm1  # xmm1 = mem[0],zero
	movq	%r8, 80(%rsp)           # 8-byte Spill
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movq	%r12, 96(%rsp)          # 8-byte Spill
	movq	%r13, 88(%rsp)          # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB13_16:                              # %.lr.ph176.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_17 Depth 2
                                        #       Child Loop BB13_23 Depth 3
                                        #     Child Loop BB13_36 Depth 2
                                        #     Child Loop BB13_49 Depth 2
                                        #     Child Loop BB13_58 Depth 2
                                        #     Child Loop BB13_60 Depth 2
                                        #     Child Loop BB13_62 Depth 2
                                        #     Child Loop BB13_64 Depth 2
	movl	$-1, %r10d
	xorl	%edx, %edx
	movl	$1, %ecx
	movl	$-1, %r14d
	movaps	%xmm0, %xmm2
	.p2align	4, 0x90
.LBB13_17:                              # %.lr.ph176
                                        #   Parent Loop BB13_16 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB13_23 Depth 3
	movq	%rdx, %rdi
	leaq	1(%rdi), %rdx
	cmpq	%r8, %rdx
	jge	.LBB13_28
# BB#18:                                # %.lr.ph
                                        #   in Loop: Header=BB13_17 Depth=2
	movl	%r11d, %eax
	subl	%edi, %eax
	movq	(%r13,%rdi,8), %rsi
	testb	$1, %al
	jne	.LBB13_20
# BB#19:                                #   in Loop: Header=BB13_17 Depth=2
	movq	%rcx, %rbp
	cmpq	%rdi, 48(%rsp)          # 8-byte Folded Reload
	jne	.LBB13_23
	jmp	.LBB13_28
	.p2align	4, 0x90
.LBB13_20:                              #   in Loop: Header=BB13_17 Depth=2
	movsd	(%rsi,%rcx,8), %xmm3    # xmm3 = mem[0],zero
	cvtss2sd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	jbe	.LBB13_22
# BB#21:                                #   in Loop: Header=BB13_17 Depth=2
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
.LBB13_22:                              #   in Loop: Header=BB13_17 Depth=2
	cmoval	%edi, %r14d
	cmoval	%ecx, %r10d
	leaq	1(%rcx), %rbp
	cmpq	%rdi, 48(%rsp)          # 8-byte Folded Reload
	je	.LBB13_28
	.p2align	4, 0x90
.LBB13_23:                              #   Parent Loop BB13_16 Depth=1
                                        #     Parent Loop BB13_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rsi,%rbp,8), %xmm3    # xmm3 = mem[0],zero
	xorps	%xmm4, %xmm4
	cvtss2sd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	jbe	.LBB13_25
# BB#24:                                #   in Loop: Header=BB13_23 Depth=3
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
.LBB13_25:                              #   in Loop: Header=BB13_23 Depth=3
	cmoval	%ebp, %r10d
	movsd	8(%rsi,%rbp,8), %xmm3   # xmm3 = mem[0],zero
	xorps	%xmm4, %xmm4
	cvtss2sd	%xmm2, %xmm4
	cmoval	%edi, %r14d
	leal	1(%rbp), %eax
	ucomisd	%xmm3, %xmm4
	jbe	.LBB13_27
# BB#26:                                #   in Loop: Header=BB13_23 Depth=3
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
.LBB13_27:                              #   in Loop: Header=BB13_23 Depth=3
	cmoval	%edi, %r14d
	cmoval	%eax, %r10d
	addq	$2, %rbp
	cmpq	%r15, %rbp
	jne	.LBB13_23
.LBB13_28:                              # %.loopexit
                                        #   in Loop: Header=BB13_17 Depth=2
	incq	%rcx
	cmpq	%r9, %rdx
	jne	.LBB13_17
# BB#29:                                # %.lr.ph182
                                        #   in Loop: Header=BB13_16 Depth=1
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	movslq	%r14d, %r8
	movq	(%rbx,%r8,8), %rdi
	jne	.LBB13_31
# BB#30:                                #   in Loop: Header=BB13_16 Depth=1
	xorl	%edx, %edx
                                        # implicit-def: %ECX
	jmp	.LBB13_34
	.p2align	4, 0x90
.LBB13_31:                              #   in Loop: Header=BB13_16 Depth=1
	cmpb	$0, (%rdi)
	jle	.LBB13_33
# BB#32:                                #   in Loop: Header=BB13_16 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%r12,%rax,8), %rax
	movq	(%rax), %rax
	movl	$0, (%rax)
	movl	$1, %ecx
	movl	$1, %edx
	movl	$1, %r9d
	testq	%r11, %r11
	jne	.LBB13_35
	jmp	.LBB13_41
.LBB13_33:                              #   in Loop: Header=BB13_16 Depth=1
	xorl	%ecx, %ecx
	movl	$1, %edx
.LBB13_34:                              #   in Loop: Header=BB13_16 Depth=1
	xorl	%r9d, %r9d
	testq	%r11, %r11
	je	.LBB13_41
.LBB13_35:                              # %.lr.ph182.new
                                        #   in Loop: Header=BB13_16 Depth=1
	movq	%r15, %r11
	subq	%rdx, %r11
	leaq	1(%rdi,%rdx), %rdi
	xorl	%ebp, %ebp
	movl	%r9d, %ecx
	.p2align	4, 0x90
.LBB13_36:                              #   Parent Loop BB13_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, -1(%rdi,%rbp)
	jle	.LBB13_38
# BB#37:                                #   in Loop: Header=BB13_36 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%r12,%rax,8), %rax
	movq	(%rax), %rax
	movslq	%ecx, %rcx
	leal	(%rdx,%rbp), %esi
	movl	%esi, (%rax,%rcx,4)
	incl	%ecx
.LBB13_38:                              #   in Loop: Header=BB13_36 Depth=2
	cmpb	$0, (%rdi,%rbp)
	jle	.LBB13_40
# BB#39:                                #   in Loop: Header=BB13_36 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%r12,%rax,8), %rax
	movq	(%rax), %rax
	movslq	%ecx, %rcx
	leal	1(%rdx,%rbp), %esi
	movl	%esi, (%rax,%rcx,4)
	incl	%ecx
.LBB13_40:                              #   in Loop: Header=BB13_36 Depth=2
	addq	$2, %rbp
	cmpq	%rbp, %r11
	jne	.LBB13_36
.LBB13_41:                              # %.lr.ph187
                                        #   in Loop: Header=BB13_16 Depth=1
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%r12,%rax,8), %r13
	movq	(%r13), %rax
	movslq	%ecx, %rcx
	movl	$-1, (%rax,%rcx,4)
	movslq	%r10d, %r9
	movq	(%rbx,%r9,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	jne	.LBB13_43
# BB#42:                                #   in Loop: Header=BB13_16 Depth=1
	xorl	%edi, %edi
                                        # implicit-def: %ECX
	jmp	.LBB13_46
	.p2align	4, 0x90
.LBB13_43:                              #   in Loop: Header=BB13_16 Depth=1
	cmpb	$0, (%rax)
	jle	.LBB13_45
# BB#44:                                #   in Loop: Header=BB13_16 Depth=1
	movq	8(%r13), %rax
	movl	$0, (%rax)
	movl	$1, %ecx
	movl	$1, %edi
	movl	$1, %esi
	jmp	.LBB13_47
.LBB13_45:                              #   in Loop: Header=BB13_16 Depth=1
	xorl	%ecx, %ecx
	movl	$1, %edi
.LBB13_46:                              # %.prol.loopexit265
                                        #   in Loop: Header=BB13_16 Depth=1
	xorl	%esi, %esi
.LBB13_47:                              # %.prol.loopexit265
                                        #   in Loop: Header=BB13_16 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB13_54
# BB#48:                                # %.lr.ph187.new
                                        #   in Loop: Header=BB13_16 Depth=1
	movq	%r15, %rbp
	subq	%rdi, %rbp
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	1(%rax,%rdi), %rdx
	xorl	%r11d, %r11d
	movl	%esi, %ecx
	.p2align	4, 0x90
.LBB13_49:                              #   Parent Loop BB13_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, -1(%rdx,%r11)
	jle	.LBB13_51
# BB#50:                                #   in Loop: Header=BB13_49 Depth=2
	movq	8(%r13), %rax
	movslq	%ecx, %rcx
	leal	(%rdi,%r11), %r12d
	movl	%r12d, (%rax,%rcx,4)
	incl	%ecx
.LBB13_51:                              #   in Loop: Header=BB13_49 Depth=2
	cmpb	$0, (%rdx,%r11)
	jle	.LBB13_53
# BB#52:                                #   in Loop: Header=BB13_49 Depth=2
	movq	8(%r13), %rax
	movslq	%ecx, %rcx
	leal	1(%rdi,%r11), %esi
	movl	%esi, (%rax,%rcx,4)
	incl	%ecx
.LBB13_53:                              #   in Loop: Header=BB13_49 Depth=2
	addq	$2, %r11
	cmpq	%r11, %rbp
	jne	.LBB13_49
.LBB13_54:                              # %.lr.ph192
                                        #   in Loop: Header=BB13_16 Depth=1
	movq	8(%r13), %rax
	movslq	%ecx, %rcx
	movsd	112(%rsp,%r9,8), %xmm3  # xmm3 = mem[0],zero
	movl	$-1, (%rax,%rcx,4)
	cvtss2sd	%xmm2, %xmm2
	mulsd	%xmm1, %xmm2
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movapd	%xmm2, %xmm4
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	movsd	112(%rsp,%r8,8), %xmm5  # xmm5 = mem[0],zero
	unpcklpd	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0]
	subpd	%xmm5, %xmm4
	movupd	%xmm4, (%rax)
	movsd	%xmm2, 112(%rsp,%r8,8)
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpb	$0, (%rax)
	setg	%al
	movq	(%rbx,%r8,8), %rcx
	addb	%al, (%rcx)
	movq	24(%rsp), %r11          # 8-byte Reload
	cmpl	$1, %r11d
	movq	96(%rsp), %r12          # 8-byte Reload
	movq	88(%rsp), %r13          # 8-byte Reload
	je	.LBB13_59
# BB#55:                                # %._crit_edge253.preheader
                                        #   in Loop: Header=BB13_16 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	jne	.LBB13_57
# BB#56:                                #   in Loop: Header=BB13_16 Depth=1
	movl	$1, %ecx
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	jne	.LBB13_58
	jmp	.LBB13_59
	.p2align	4, 0x90
.LBB13_57:                              # %._crit_edge253.prol
                                        #   in Loop: Header=BB13_16 Depth=1
	movq	(%rbx,%r9,8), %rax
	cmpb	$0, 1(%rax)
	setg	%al
	movq	(%rbx,%r8,8), %rcx
	addb	%al, 1(%rcx)
	movl	$2, %ecx
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	je	.LBB13_59
	.p2align	4, 0x90
.LBB13_58:                              # %._crit_edge253
                                        #   Parent Loop BB13_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%r9,8), %rax
	cmpb	$0, (%rax,%rcx)
	setg	%al
	movq	(%rbx,%r8,8), %rdx
	addb	%al, (%rdx,%rcx)
	movq	(%rbx,%r9,8), %rax
	cmpb	$0, 1(%rax,%rcx)
	setg	%al
	movq	(%rbx,%r8,8), %rdx
	addb	%al, 1(%rdx,%rcx)
	addq	$2, %rcx
	cmpq	%rcx, %r15
	jne	.LBB13_58
.LBB13_59:                              # %.lr.ph194
                                        #   in Loop: Header=BB13_16 Depth=1
	movq	64(%rsp), %rdx          # 8-byte Reload
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	je	.LBB13_61
	.p2align	4, 0x90
.LBB13_60:                              #   Parent Loop BB13_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%r9,8), %rax
	movb	$0, (%rax,%rcx)
	incq	%rcx
	cmpq	%rcx, %rdx
	jne	.LBB13_60
.LBB13_61:                              # %.prol.loopexit271
                                        #   in Loop: Header=BB13_16 Depth=1
	cmpq	$7, 16(%rsp)            # 8-byte Folded Reload
	jb	.LBB13_63
	.p2align	4, 0x90
.LBB13_62:                              #   Parent Loop BB13_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%r9,8), %rax
	movb	$0, (%rax,%rcx)
	movq	(%rbx,%r9,8), %rax
	movb	$0, 1(%rax,%rcx)
	movq	(%rbx,%r9,8), %rax
	movb	$0, 2(%rax,%rcx)
	movq	(%rbx,%r9,8), %rax
	movb	$0, 3(%rax,%rcx)
	movq	(%rbx,%r9,8), %rax
	movb	$0, 4(%rax,%rcx)
	movq	(%rbx,%r9,8), %rax
	movb	$0, 5(%rax,%rcx)
	movq	(%rbx,%r9,8), %rax
	movb	$0, 6(%rax,%rcx)
	movq	(%rbx,%r9,8), %rax
	movb	$0, 7(%rax,%rcx)
	addq	$8, %rcx
	cmpq	%rcx, %r15
	jne	.LBB13_62
.LBB13_63:                              # %.lr.ph196
                                        #   in Loop: Header=BB13_16 Depth=1
	movq	(%r13,%r8,8), %rcx
	xorl	%edx, %edx
	movabsq	$4666722622711529472, %r8 # imm = 0x40C3878000000000
	.p2align	4, 0x90
.LBB13_64:                              #   Parent Loop BB13_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%edx, %r14d
	je	.LBB13_67
# BB#65:                                #   in Loop: Header=BB13_64 Depth=2
	cmpl	%edx, %r10d
	je	.LBB13_67
# BB#66:                                #   in Loop: Header=BB13_64 Depth=2
	cmpl	%r14d, %edx
	movl	%r14d, %eax
	cmovlel	%edx, %eax
	cltq
	movq	(%r13,%rax,8), %rax
	movl	%r14d, %esi
	cmovgel	%edx, %esi
	movslq	%esi, %rsi
	movsd	(%rax,%rsi,8), %xmm2    # xmm2 = mem[0],zero
	cmpl	%r10d, %edx
	movl	%r10d, %edi
	cmovlel	%edx, %edi
	movslq	%edi, %rdi
	movq	(%r13,%rdi,8), %rdi
	movl	%r10d, %ebp
	cmovgel	%edx, %ebp
	movslq	%ebp, %rbp
	addsd	(%rdi,%rbp,8), %xmm2
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, (%rax,%rsi,8)
	movq	%r8, (%rdi,%rbp,8)
.LBB13_67:                              #   in Loop: Header=BB13_64 Depth=2
	movq	%r8, (%rcx,%r9,8)
	incl	%edx
	cmpl	%edx, %r11d
	jne	.LBB13_64
# BB#68:                                # %._crit_edge197
                                        #   in Loop: Header=BB13_16 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	72(%rsp), %r9           # 8-byte Reload
	movq	%rcx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpq	%r9, %rcx
	movq	16(%rsp), %r11          # 8-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	jne	.LBB13_16
.LBB13_69:                              # %._crit_edge199
	addq	$400120, %rsp           # imm = 0x61AF8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	upg2, .Lfunc_end13-upg2
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI14_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI14_1:
	.long	3212836864              # float -1
.LCPI14_2:
	.long	0                       # float 0
	.text
	.globl	loadtree
	.p2align	4, 0x90
	.type	loadtree,@function
loadtree:                               # @loadtree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi132:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi133:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi134:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi135:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi136:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi137:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi138:
	.cfi_def_cfa_offset 144
.Lcfi139:
	.cfi_offset %rbx, -56
.Lcfi140:
	.cfi_offset %r12, -48
.Lcfi141:
	.cfi_offset %r13, -40
.Lcfi142:
	.cfi_offset %r14, -32
.Lcfi143:
	.cfi_offset %r15, -24
.Lcfi144:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbp
	movq	%rsi, 72(%rsp)          # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movl	$.L.str.8, %edi
	movl	$.L.str.9, %esi
	callq	fopen
	movq	%rax, 40(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB14_79
# BB#1:
	xorl	%ebx, %ebx
	cmpq	$0, loadtree.hist(%rip)
	movl	$0, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jne	.LBB14_3
# BB#2:
	movl	njob(%rip), %edi
	callq	AllocateIntVec
	movq	%rax, loadtree.hist(%rip)
	movslq	njob(%rip), %rbx
	movq	%rbx, %rax
	shlq	$3, %rax
	leaq	(%rax,%rax,2), %rdi
	callq	malloc
	movq	%rax, loadtree.ac(%rip)
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, loadtree.nmemar(%rip)
	movl	njob(%rip), %edi
	callq	AllocateFloatVec
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	njob(%rip), %edi
	callq	AllocateIntVec
	movq	%rax, %rbx
	imull	$50, njob(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, loadtree.treetmp(%rip)
	movl	$30, %edi
	callq	AllocateCharVec
	movq	%rax, loadtree.nametmp(%rip)
	movl	njob(%rip), %edi
	imull	$50, %edi, %esi
	callq	AllocateCharMtx
	movq	%rax, loadtree.tree(%rip)
.LBB14_3:
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rbx          # 8-byte Reload
	testl	%ebx, %ebx
	jle	.LBB14_71
# BB#4:                                 # %.preheader247.preheader
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	callq	__ctype_b_loc
	movq	%rax, %r15
	movl	%ebx, %r13d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB14_5:                               # %.preheader247
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_6 Depth 2
	movq	loadtree.nametmp(%rip), %rcx
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 14(%rcx)
	movdqu	%xmm0, (%rcx)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB14_6:                               #   Parent Loop BB14_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15), %rdx
	movq	(%r14,%rbp,8), %rsi
	movsbq	(%rsi,%rax), %rsi
	movzwl	(%rdx,%rsi,2), %edx
	andl	$8, %edx
	testw	%dx, %dx
	movb	$95, %dl
	movb	$95, %bl
	je	.LBB14_8
# BB#7:                                 #   in Loop: Header=BB14_6 Depth=2
	movl	%esi, %ebx
.LBB14_8:                               #   in Loop: Header=BB14_6 Depth=2
	movb	%bl, (%rcx,%rax)
	movq	(%r15), %rdi
	movq	(%r14,%rbp,8), %rsi
	movsbq	1(%rsi,%rax), %rsi
	movzwl	(%rdi,%rsi,2), %edi
	andl	$8, %edi
	testw	%di, %di
	je	.LBB14_10
# BB#9:                                 #   in Loop: Header=BB14_6 Depth=2
	movl	%esi, %edx
.LBB14_10:                              #   in Loop: Header=BB14_6 Depth=2
	movb	%dl, 1(%rcx,%rax)
	addq	$2, %rax
	cmpq	$30, %rax
	jne	.LBB14_6
# BB#11:                                #   in Loop: Header=BB14_5 Depth=1
	movb	$0, 30(%rcx)
	movq	loadtree.tree(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	incq	%rbp
	incq	%rcx
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	sprintf
	cmpq	%r13, %rbp
	jne	.LBB14_5
# BB#12:                                # %.preheader245
	movq	loadtree.ac(%rip), %rax
	movq	16(%rsp), %r14          # 8-byte Reload
	testl	%r14d, %r14d
	jle	.LBB14_72
# BB#13:                                # %.lr.ph269.preheader
	testb	$1, %r13b
	jne	.LBB14_15
# BB#14:
	xorl	%ecx, %ecx
	jmp	.LBB14_16
.LBB14_15:                              # %.lr.ph269.prol
	leaq	24(%rax), %rcx
	movq	%rcx, (%rax)
	movq	loadtree.ac(%rip), %rax
	leaq	-24(%rax), %rcx
	movq	%rcx, 8(%rax)
	movl	$0, 16(%rax)
	movl	$1, %ecx
.LBB14_16:                              # %.lr.ph269.prol.loopexit
	movq	24(%rsp), %rbx          # 8-byte Reload
	cmpl	$1, %r14d
	je	.LBB14_19
# BB#17:                                # %.lr.ph269.preheader.new
	leaq	(,%rcx,8), %rdx
	leaq	(%rdx,%rdx,2), %rdx
	.p2align	4, 0x90
.LBB14_18:                              # %.lr.ph269
                                        # =>This Inner Loop Header: Depth=1
	leaq	24(%rax,%rdx), %rsi
	movq	%rsi, (%rax,%rdx)
	movq	loadtree.ac(%rip), %rax
	leaq	-24(%rax,%rdx), %rsi
	movq	%rsi, 8(%rax,%rdx)
	movl	%ecx, 16(%rax,%rdx)
	leal	1(%rcx), %esi
	leaq	48(%rax,%rdx), %rdi
	movq	%rdi, 24(%rax,%rdx)
	movq	loadtree.ac(%rip), %rax
	leaq	(%rax,%rdx), %rdi
	movq	%rdi, 32(%rax,%rdx)
	movl	%esi, 40(%rax,%rdx)
	addq	$2, %rcx
	addq	$48, %rdx
	cmpq	%r13, %rcx
	jne	.LBB14_18
.LBB14_19:                              # %._crit_edge270
	leal	-1(%r14), %ecx
	movslq	%ecx, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rcx,2), %rcx
	movq	$0, (%rax,%rcx,8)
	testl	%r14d, %r14d
	jle	.LBB14_73
# BB#20:                                # %.lr.ph266
	movq	loadtree.hist(%rip), %rax
	movq	loadtree.nmemar(%rip), %rcx
	cmpl	$8, %r14d
	jae	.LBB14_22
# BB#21:
	xorl	%edx, %edx
	jmp	.LBB14_30
.LBB14_22:                              # %min.iters.checked
	movl	%r14d, %esi
	andl	$7, %esi
	movq	%r13, %rdx
	subq	%rsi, %rdx
	je	.LBB14_26
# BB#23:                                # %vector.memcheck
	leaq	(%rcx,%r13,4), %rdi
	cmpq	%rdi, %rax
	jae	.LBB14_27
# BB#24:                                # %vector.memcheck
	leaq	(%rax,%r13,4), %rdi
	cmpq	%rdi, %rcx
	jae	.LBB14_27
.LBB14_26:
	xorl	%edx, %edx
	jmp	.LBB14_30
.LBB14_27:                              # %vector.body.preheader
	movq	%rbx, %r8
	leaq	16(%rax), %rdi
	leaq	16(%rcx), %rbp
	pcmpeqd	%xmm0, %xmm0
	movaps	.LCPI14_0(%rip), %xmm1  # xmm1 = [1,1,1,1]
	movq	%rdx, %rbx
	.p2align	4, 0x90
.LBB14_28:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm0, (%rdi)
	movups	%xmm1, -16(%rbp)
	movups	%xmm1, (%rbp)
	addq	$32, %rdi
	addq	$32, %rbp
	addq	$-8, %rbx
	jne	.LBB14_28
# BB#29:                                # %middle.block
	testl	%esi, %esi
	movq	%r8, %rbx
	je	.LBB14_36
.LBB14_30:                              # %scalar.ph.preheader
	movl	%r13d, %edi
	subl	%edx, %edi
	leaq	-1(%r13), %rsi
	subq	%rdx, %rsi
	andq	$7, %rdi
	je	.LBB14_33
# BB#31:                                # %scalar.ph.prol.preheader
	negq	%rdi
	.p2align	4, 0x90
.LBB14_32:                              # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$-1, (%rax,%rdx,4)
	movl	$1, (%rcx,%rdx,4)
	incq	%rdx
	incq	%rdi
	jne	.LBB14_32
.LBB14_33:                              # %scalar.ph.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB14_36
# BB#34:                                # %scalar.ph.preheader.new
	subq	%rdx, %r13
	leaq	28(%rcx,%rdx,4), %rcx
	leaq	28(%rax,%rdx,4), %rax
	.p2align	4, 0x90
.LBB14_35:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$-1, -28(%rax)
	movl	$1, -28(%rcx)
	movl	$-1, -24(%rax)
	movl	$1, -24(%rcx)
	movl	$-1, -20(%rax)
	movl	$1, -20(%rcx)
	movl	$-1, -16(%rax)
	movl	$1, -16(%rcx)
	movl	$-1, -12(%rax)
	movl	$1, -12(%rcx)
	movl	$-1, -8(%rax)
	movl	$1, -8(%rcx)
	movl	$-1, -4(%rax)
	movl	$1, -4(%rcx)
	movl	$-1, (%rax)
	movl	$1, (%rcx)
	addq	$32, %rcx
	addq	$32, %rax
	addq	$-8, %r13
	jne	.LBB14_35
.LBB14_36:                              # %._crit_edge267
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	cmpl	$2, %r14d
	jl	.LBB14_74
# BB#37:                                # %.lr.ph263
	xorl	%r14d, %r14d
	jmp	.LBB14_39
.LBB14_38:                              #   in Loop: Header=BB14_39 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
	movl	%r14d, %edx
	movq	16(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	jmp	.LBB14_40
	.p2align	4, 0x90
.LBB14_39:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_50 Depth 2
                                        #     Child Loop BB14_55 Depth 2
                                        #     Child Loop BB14_60 Depth 2
                                        #     Child Loop BB14_65 Depth 2
                                        #     Child Loop BB14_67 Depth 2
	movslq	%r14d, %rax
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$34, %rcx
	addl	%edx, %ecx
	addl	%ecx, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	je	.LBB14_38
.LBB14_40:                              #   in Loop: Header=BB14_39 Depth=1
	movq	(%rbx,%r14,8), %rbp
	movl	$-1082130432, 4(%rbp)   # imm = 0xBF800000
	movl	$-1082130432, (%rbp)    # imm = 0xBF800000
	movl	$loadtreeoneline.gett, %edi
	movl	$999, %esi              # imm = 0x3E7
	movq	40(%rsp), %rdx          # 8-byte Reload
	callq	fgets
	movl	$loadtreeoneline.gett, %edi
	movl	$.L.str.38, %esi
	leaq	4(%rbp), %r9
	xorl	%eax, %eax
	leaq	8(%rsp), %rdx
	movq	%rbp, %r8
	leaq	12(%rsp), %rcx
	callq	sscanf
	movslq	8(%rsp), %r13
	leal	-1(%r13), %r15d
	movl	%r15d, 8(%rsp)
	movslq	12(%rsp), %r12
	leaq	-1(%r12), %rcx
	movl	%ecx, 12(%rsp)
	cmpl	%r12d, %r13d
	jge	.LBB14_76
# BB#41:                                # %loadtreeoneline.exit
                                        #   in Loop: Header=BB14_39 Depth=1
	movq	(%rbx,%r14,8), %rax
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	.LCPI14_1(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	jne	.LBB14_42
	jnp	.LBB14_75
.LBB14_42:                              #   in Loop: Header=BB14_39 Depth=1
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm1
	jne	.LBB14_43
	jnp	.LBB14_75
.LBB14_43:                              #   in Loop: Header=BB14_39 Depth=1
	xorps	%xmm2, %xmm2
	ucomiss	%xmm0, %xmm2
	jbe	.LBB14_45
# BB#44:                                #   in Loop: Header=BB14_39 Depth=1
	movl	$0, (%rax)
.LBB14_45:                              #   in Loop: Header=BB14_39 Depth=1
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	pxor	%xmm0, %xmm0
	ucomiss	%xmm1, %xmm0
	jbe	.LBB14_47
# BB#46:                                #   in Loop: Header=BB14_39 Depth=1
	movl	$0, 4(%rax)
.LBB14_47:                              #   in Loop: Header=BB14_39 Depth=1
	movq	loadtree.hist(%rip), %rax
	movslq	-4(%rax,%r13,4), %rbx
	movq	loadtree.nmemar(%rip), %rax
	movslq	-4(%rax,%r13,4), %rcx
	movq	72(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp,%r14,8), %rax
	movq	(%rax), %rdi
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	leaq	4(,%rcx,4), %rsi
	callq	realloc
	cmpq	$-1, %rbx
	movq	(%rbp,%r14,8), %rcx
	movq	%rax, (%rcx)
	je	.LBB14_51
# BB#48:                                #   in Loop: Header=BB14_39 Depth=1
	movq	(%rbp,%rbx,8), %rcx
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	movl	(%rdx), %esi
	cmpl	(%rcx), %esi
	movq	%rdx, %rsi
	cmovgq	%rcx, %rsi
	cmovgq	%rdx, %rcx
	movl	(%rsi), %edi
	cmpl	$-1, %edi
	movq	%rbp, %rbx
	je	.LBB14_52
# BB#49:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB14_39 Depth=1
	addq	$4, %rsi
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB14_50:                              # %.lr.ph
                                        #   Parent Loop BB14_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, (%rdx)
	addq	$4, %rdx
	movl	(%rsi), %edi
	addq	$4, %rsi
	cmpl	$-1, %edi
	jne	.LBB14_50
	jmp	.LBB14_53
	.p2align	4, 0x90
.LBB14_51:                              #   in Loop: Header=BB14_39 Depth=1
	movq	%rax, %rdx
	addq	$4, %rdx
	movl	%r15d, (%rax)
	movq	%rbp, %rbx
	jmp	.LBB14_56
	.p2align	4, 0x90
.LBB14_52:                              #   in Loop: Header=BB14_39 Depth=1
	movq	%rax, %rdx
.LBB14_53:                              # %.preheader243
                                        #   in Loop: Header=BB14_39 Depth=1
	movl	(%rcx), %eax
	cmpl	$-1, %eax
	je	.LBB14_56
# BB#54:                                # %.lr.ph252.preheader
                                        #   in Loop: Header=BB14_39 Depth=1
	addq	$4, %rcx
	.p2align	4, 0x90
.LBB14_55:                              # %.lr.ph252
                                        #   Parent Loop BB14_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rdx)
	addq	$4, %rdx
	movl	(%rcx), %eax
	addq	$4, %rcx
	cmpl	$-1, %eax
	jne	.LBB14_55
.LBB14_56:                              # %.loopexit244
                                        #   in Loop: Header=BB14_39 Depth=1
	movl	$-1, (%rdx)
	movq	loadtree.nmemar(%rip), %rax
	movslq	-4(%rax,%r12,4), %r15
	movq	loadtree.hist(%rip), %rax
	movslq	-4(%rax,%r12,4), %rbp
	movq	(%rbx,%r14,8), %rax
	movq	8(%rax), %rdi
	leaq	4(,%r15,4), %rsi
	callq	realloc
	movq	(%rbx,%r14,8), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	je	.LBB14_77
# BB#57:                                #   in Loop: Header=BB14_39 Depth=1
	cmpl	$-1, %ebp
	je	.LBB14_61
# BB#58:                                #   in Loop: Header=BB14_39 Depth=1
	movq	(%rbx,%rbp,8), %rcx
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	movl	(%rdx), %esi
	cmpl	(%rcx), %esi
	movq	%rdx, %rsi
	cmovgq	%rcx, %rsi
	cmovgq	%rdx, %rcx
	movl	(%rsi), %edi
	cmpl	$-1, %edi
	je	.LBB14_62
# BB#59:                                # %.lr.ph256.preheader
                                        #   in Loop: Header=BB14_39 Depth=1
	addq	$4, %rsi
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB14_60:                              # %.lr.ph256
                                        #   Parent Loop BB14_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, (%rdx)
	addq	$4, %rdx
	movl	(%rsi), %edi
	addq	$4, %rsi
	cmpl	$-1, %edi
	jne	.LBB14_60
	jmp	.LBB14_63
	.p2align	4, 0x90
.LBB14_61:                              #   in Loop: Header=BB14_39 Depth=1
	movq	%rax, %rdx
	addq	$4, %rdx
	movq	48(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, (%rax)
	jmp	.LBB14_66
	.p2align	4, 0x90
.LBB14_62:                              #   in Loop: Header=BB14_39 Depth=1
	movq	%rax, %rdx
.LBB14_63:                              # %.preheader
                                        #   in Loop: Header=BB14_39 Depth=1
	movl	(%rcx), %eax
	cmpl	$-1, %eax
	movq	48(%rsp), %rbp          # 8-byte Reload
	je	.LBB14_66
# BB#64:                                # %.lr.ph260.preheader
                                        #   in Loop: Header=BB14_39 Depth=1
	addq	$4, %rcx
	.p2align	4, 0x90
.LBB14_65:                              # %.lr.ph260
                                        #   Parent Loop BB14_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rdx)
	addq	$4, %rdx
	movl	(%rcx), %eax
	addq	$4, %rcx
	cmpl	$-1, %eax
	jne	.LBB14_65
.LBB14_66:                              # %.loopexit
                                        #   in Loop: Header=BB14_39 Depth=1
	movl	$-1, (%rdx)
	movq	loadtree.hist(%rip), %rax
	movl	%r14d, -4(%rax,%r13,4)
	addl	80(%rsp), %r15d         # 4-byte Folded Reload
	movq	loadtree.nmemar(%rip), %rax
	movl	%r15d, -4(%rax,%r13,4)
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	$1148844442, -4(%rax,%r13,4) # imm = 0x4479F99A
	movl	$loadtree.ac, %eax
	.p2align	4, 0x90
.LBB14_67:                              #   Parent Loop BB14_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB14_67
# BB#68:                                #   in Loop: Header=BB14_39 Depth=1
	movq	loadtree.treetmp(%rip), %rdi
	movq	loadtree.tree(%rip), %rax
	movq	-8(%rax,%r13,8), %rdx
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx,%r14,8), %rcx
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movq	-8(%rax,%r12,8), %rcx
	cvtss2sd	%xmm1, %xmm1
	movl	$.L.str.16, %esi
	movb	$2, %al
	callq	sprintf
	movq	loadtree.tree(%rip), %rax
	movq	-8(%rax,%r13,8), %rdi
	movq	loadtree.treetmp(%rip), %rsi
	callq	strcpy
	movq	loadtree.ac(%rip), %rcx
	leaq	(%rbp,%rbp,2), %rdx
	movq	8(%rcx,%rdx,8), %rax
	movq	(%rcx,%rdx,8), %rcx
	movq	%rcx, (%rax)
	testq	%rcx, %rcx
	je	.LBB14_70
# BB#69:                                #   in Loop: Header=BB14_39 Depth=1
	movq	%rax, 8(%rcx)
.LBB14_70:                              #   in Loop: Header=BB14_39 Depth=1
	incq	%r14
	cmpq	64(%rsp), %r14          # 8-byte Folded Reload
	jl	.LBB14_39
	jmp	.LBB14_74
.LBB14_71:                              # %.preheader245.thread
	movq	loadtree.ac(%rip), %rax
	movq	%rbx, %r14
.LBB14_72:                              # %._crit_edge270.thread
	decl	%r14d
	movslq	%r14d, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movq	$0, (%rax,%rcx,8)
.LBB14_73:                              # %._crit_edge267.thread
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
.LBB14_74:                              # %._crit_edge
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	movl	$.L.str.17, %edi
	movl	$.L.str.18, %esi
	callq	fopen
	movq	%rax, %rbx
	movq	loadtree.treetmp(%rip), %rdx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.19, %edi
	movl	$13, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	%rbx, %rdi
	callq	fclose
	movq	loadtree.tree(%rip), %rdi
	callq	FreeCharMtx
	movq	loadtree.treetmp(%rip), %rdi
	callq	free
	movq	loadtree.nametmp(%rip), %rdi
	callq	free
	movq	loadtree.hist(%rip), %rdi
	callq	free
	movq	$0, loadtree.hist(%rip)
	movq	loadtree.ac(%rip), %rdi
	callq	free
	movq	$0, loadtree.ac(%rip)
	movq	loadtree.nmemar(%rip), %rdi
	callq	free
	movq	$0, loadtree.nmemar(%rip)
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_75:
	movq	stderr(%rip), %rcx
	movl	$.L.str.14, %edi
	movl	$37, %esi
	jmp	.LBB14_78
.LBB14_76:
	movq	stderr(%rip), %rcx
	movl	$.L.str.39, %edi
	movl	$21, %esi
	jmp	.LBB14_78
.LBB14_77:
	movq	stderr(%rip), %rcx
	movl	$.L.str.15, %edi
	movl	$24, %esi
.LBB14_78:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB14_79:
	movq	stderr(%rip), %rcx
	movl	$.L.str.10, %edi
	movl	$23, %esi
	jmp	.LBB14_78
.Lfunc_end14:
	.size	loadtree, .Lfunc_end14-loadtree
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI15_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI15_1:
	.long	3212836864              # float -1
.LCPI15_2:
	.long	1056964608              # float 0.5
.LCPI15_3:
	.long	0                       # float 0
	.text
	.globl	loadtop
	.p2align	4, 0x90
	.type	loadtop,@function
loadtop:                                # @loadtop
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi145:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi146:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi147:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi148:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi149:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi150:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi151:
	.cfi_def_cfa_offset 176
.Lcfi152:
	.cfi_offset %rbx, -56
.Lcfi153:
	.cfi_offset %r12, -48
.Lcfi154:
	.cfi_offset %r13, -40
.Lcfi155:
	.cfi_offset %r14, -32
.Lcfi156:
	.cfi_offset %r15, -24
.Lcfi157:
	.cfi_offset %rbp, -16
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	%rdx, %rbx
	movq	%rsi, %r12
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	$1063675494, sueff1(%rip) # imm = 0x3F666666
	movl	$1028443341, sueff05(%rip) # imm = 0x3D4CCCCD
	movl	treemethod(%rip), %edx
	cmpl	$69, %edx
	je	.LBB15_4
# BB#1:
	cmpl	$88, %edx
	je	.LBB15_5
# BB#2:
	cmpl	$113, %edx
	jne	.LBB15_85
# BB#3:                                 # %.fold.split244
	movl	$cluster_minimum_float, %eax
	jmp	.LBB15_6
.LBB15_4:                               # %.fold.split
	movl	$cluster_average_float, %eax
	jmp	.LBB15_6
.LBB15_5:
	movl	$cluster_mix_float, %eax
.LBB15_6:
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	$.L.str.8, %edi
	movl	$.L.str.9, %esi
	callq	fopen
	movq	%rax, 88(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB15_86
# BB#7:
	cmpq	$0, loadtop.hist(%rip)
	movq	8(%rsp), %rbp           # 8-byte Reload
	jne	.LBB15_9
# BB#8:
	imull	$50, njob(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, loadtop.treetmp(%rip)
	movl	njob(%rip), %edi
	imull	$50, %edi, %esi
	callq	AllocateCharMtx
	movq	%rax, loadtop.tree(%rip)
	movl	njob(%rip), %edi
	callq	AllocateIntVec
	movq	%rax, loadtop.hist(%rip)
	movl	njob(%rip), %edi
	callq	AllocateFloatVec
	movq	%rax, loadtop.tmptmplen(%rip)
	movslq	njob(%rip), %rbp
	movq	%rbp, %rax
	shlq	$3, %rax
	leaq	(%rax,%rax,2), %rdi
	callq	malloc
	movq	%rax, loadtop.ac(%rip)
	movl	%ebp, %edi
	callq	AllocateIntVec
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rax, loadtop.nmemar(%rip)
.LBB15_9:                               # %.preheader256
	testl	%ebp, %ebp
	jle	.LBB15_77
# BB#10:                                # %.lr.ph289.preheader
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movl	%ebp, %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB15_11:                              # %.lr.ph289
                                        # =>This Inner Loop Header: Depth=1
	movq	loadtop.tree(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	incq	%rbp
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	sprintf
	cmpq	%rbp, %r14
	jne	.LBB15_11
# BB#12:                                # %.preheader255
	movq	loadtop.ac(%rip), %rax
	movq	8(%rsp), %rbp           # 8-byte Reload
	testl	%ebp, %ebp
	jle	.LBB15_78
# BB#13:                                # %.lr.ph285.preheader
	testb	$1, %r14b
	jne	.LBB15_15
# BB#14:
	xorl	%ecx, %ecx
	cmpl	$1, %r14d
	jne	.LBB15_16
	jmp	.LBB15_18
.LBB15_15:                              # %.lr.ph285.prol
	leaq	24(%rax), %rcx
	movq	%rcx, (%rax)
	movq	loadtop.ac(%rip), %rax
	leaq	-24(%rax), %rcx
	movq	%rcx, 8(%rax)
	movl	$0, 16(%rax)
	movl	$1, %ecx
	cmpl	$1, %r14d
	je	.LBB15_18
.LBB15_16:                              # %.lr.ph285.preheader.new
	leaq	(,%rcx,8), %rdx
	leaq	(%rdx,%rdx,2), %rdx
	.p2align	4, 0x90
.LBB15_17:                              # %.lr.ph285
                                        # =>This Inner Loop Header: Depth=1
	leaq	24(%rax,%rdx), %rsi
	movq	%rsi, (%rax,%rdx)
	movq	loadtop.ac(%rip), %rax
	leaq	-24(%rax,%rdx), %rsi
	movq	%rsi, 8(%rax,%rdx)
	movl	%ecx, 16(%rax,%rdx)
	leal	1(%rcx), %esi
	leaq	48(%rax,%rdx), %rdi
	movq	%rdi, 24(%rax,%rdx)
	movq	loadtop.ac(%rip), %rax
	leaq	(%rax,%rdx), %rdi
	movq	%rdi, 32(%rax,%rdx)
	movl	%esi, 40(%rax,%rdx)
	addq	$2, %rcx
	addq	$48, %rdx
	cmpq	%r14, %rcx
	jne	.LBB15_17
.LBB15_18:                              # %._crit_edge286
	leal	-1(%rbp), %ecx
	testl	%ebp, %ebp
	movslq	%ecx, %rdx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	leaq	(%rdx,%rdx,2), %rdx
	movq	$0, (%rax,%rdx,8)
	jle	.LBB15_79
# BB#19:                                # %.lr.ph279
	movq	loadtop.tmptmplen(%rip), %rdi
	movl	%ecx, %eax
	leaq	4(,%rax,4), %rdx
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	callq	memset
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	loadtop.hist(%rip), %rax
	movq	loadtop.nmemar(%rip), %rcx
	cmpl	$8, %edx
	jb	.LBB15_28
# BB#20:                                # %min.iters.checked
	movl	%edx, %r8d
	andl	$7, %r8d
	movq	%r14, %rbp
	subq	%r8, %rbp
	je	.LBB15_24
# BB#21:                                # %vector.memcheck
	leaq	(%rcx,%r14,4), %rdx
	cmpq	%rdx, %rax
	jae	.LBB15_25
# BB#22:                                # %vector.memcheck
	leaq	(%rax,%r14,4), %rdx
	cmpq	%rdx, %rcx
	jae	.LBB15_25
.LBB15_24:
	xorl	%ebp, %ebp
	jmp	.LBB15_28
.LBB15_25:                              # %vector.body.preheader
	leaq	16(%rax), %rsi
	leaq	16(%rcx), %rdi
	pcmpeqd	%xmm0, %xmm0
	movaps	.LCPI15_0(%rip), %xmm1  # xmm1 = [1,1,1,1]
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB15_26:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm0, (%rsi)
	movups	%xmm1, -16(%rdi)
	movups	%xmm1, (%rdi)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB15_26
# BB#27:                                # %middle.block
	testl	%r8d, %r8d
	je	.LBB15_34
.LBB15_28:                              # %scalar.ph.preheader
	movl	%r14d, %esi
	subl	%ebp, %esi
	leaq	-1(%r14), %rdx
	subq	%rbp, %rdx
	andq	$7, %rsi
	je	.LBB15_31
# BB#29:                                # %scalar.ph.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB15_30:                              # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$-1, (%rax,%rbp,4)
	movl	$1, (%rcx,%rbp,4)
	incq	%rbp
	incq	%rsi
	jne	.LBB15_30
.LBB15_31:                              # %scalar.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB15_34
# BB#32:                                # %scalar.ph.preheader.new
	subq	%rbp, %r14
	leaq	28(%rcx,%rbp,4), %rcx
	leaq	28(%rax,%rbp,4), %rax
	.p2align	4, 0x90
.LBB15_33:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$-1, -28(%rax)
	movl	$1, -28(%rcx)
	movl	$-1, -24(%rax)
	movl	$1, -24(%rcx)
	movl	$-1, -20(%rax)
	movl	$1, -20(%rcx)
	movl	$-1, -16(%rax)
	movl	$1, -16(%rcx)
	movl	$-1, -12(%rax)
	movl	$1, -12(%rcx)
	movl	$-1, -8(%rax)
	movl	$1, -8(%rcx)
	movl	$-1, -4(%rax)
	movl	$1, -4(%rcx)
	movl	$-1, (%rax)
	movl	$1, (%rcx)
	addq	$32, %rcx
	addq	$32, %rax
	addq	$-8, %r14
	jne	.LBB15_33
.LBB15_34:                              # %._crit_edge280
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	cmpl	$2, 8(%rsp)             # 4-byte Folded Reload
	jl	.LBB15_80
# BB#35:                                # %.lr.ph276
	xorl	%ebx, %ebx
	movq	%r12, 80(%rsp)          # 8-byte Spill
	jmp	.LBB15_37
.LBB15_36:                              #   in Loop: Header=BB15_37 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	movq	8(%rsp), %rcx           # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	jmp	.LBB15_38
	.p2align	4, 0x90
.LBB15_37:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_44 Depth 2
                                        #     Child Loop BB15_49 Depth 2
                                        #     Child Loop BB15_54 Depth 2
                                        #     Child Loop BB15_59 Depth 2
                                        #     Child Loop BB15_66 Depth 2
	movslq	%ebx, %rax
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$34, %rcx
	addl	%edx, %ecx
	addl	%ecx, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	je	.LBB15_36
.LBB15_38:                              #   in Loop: Header=BB15_37 Depth=1
	movabsq	$-4647714812233515008, %rax # imm = 0xBF800000BF800000
	movq	%rax, 48(%rsp)
	movl	$loadtreeoneline.gett, %edi
	movl	$999, %esi              # imm = 0x3E7
	movq	88(%rsp), %rdx          # 8-byte Reload
	callq	fgets
	movl	$loadtreeoneline.gett, %edi
	movl	$.L.str.38, %esi
	xorl	%eax, %eax
	leaq	24(%rsp), %rdx
	leaq	28(%rsp), %rcx
	leaq	48(%rsp), %r8
	leaq	52(%rsp), %r9
	callq	sscanf
	movslq	24(%rsp), %rdx
	leal	-1(%rdx), %r14d
	movl	%r14d, 24(%rsp)
	movslq	28(%rsp), %rcx
	leaq	-1(%rcx), %r15
	movl	%r15d, 28(%rsp)
	movq	%rcx, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpl	%ecx, %edx
	jge	.LBB15_81
# BB#39:                                # %loadtreeoneline.exit
                                        #   in Loop: Header=BB15_37 Depth=1
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movd	48(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	.LCPI15_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jne	.LBB15_82
	jp	.LBB15_82
# BB#40:                                # %loadtreeoneline.exit
                                        #   in Loop: Header=BB15_37 Depth=1
	movd	52(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jne	.LBB15_82
	jp	.LBB15_82
# BB#41:                                #   in Loop: Header=BB15_37 Depth=1
	movq	-8(%r12,%rdx,8), %rax
	movl	%r15d, %ecx
	subl	%r14d, %ecx
	movslq	%ecx, %rcx
	movd	(%rax,%rcx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movd	%xmm0, 76(%rsp)         # 4-byte Folded Spill
	movq	loadtop.hist(%rip), %rax
	movslq	-4(%rax,%rdx,4), %r13
	movq	loadtop.nmemar(%rip), %rax
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movslq	-4(%rax,%rdx,4), %r12
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	(%rbx,%rbp,8), %rax
	movq	(%rax), %rdi
	leaq	4(,%r12,4), %rsi
	callq	realloc
	cmpq	$-1, %r13
	movq	(%rbx,%rbp,8), %rcx
	movq	%rax, (%rcx)
	movq	%r12, 112(%rsp)         # 8-byte Spill
	je	.LBB15_45
# BB#42:                                #   in Loop: Header=BB15_37 Depth=1
	movq	(%rbx,%r13,8), %rcx
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	movl	(%rdx), %esi
	cmpl	(%rcx), %esi
	movq	%rdx, %rsi
	cmovgq	%rcx, %rsi
	cmovgq	%rdx, %rcx
	movl	(%rsi), %edi
	cmpl	$-1, %edi
	je	.LBB15_46
# BB#43:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB15_37 Depth=1
	addq	$4, %rsi
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB15_44:                              # %.lr.ph
                                        #   Parent Loop BB15_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, (%rdx)
	addq	$4, %rdx
	movl	(%rsi), %edi
	addq	$4, %rsi
	cmpl	$-1, %edi
	jne	.LBB15_44
	jmp	.LBB15_47
	.p2align	4, 0x90
.LBB15_45:                              #   in Loop: Header=BB15_37 Depth=1
	movq	%rax, %rdx
	addq	$4, %rdx
	movl	%r14d, (%rax)
	jmp	.LBB15_50
	.p2align	4, 0x90
.LBB15_46:                              #   in Loop: Header=BB15_37 Depth=1
	movq	%rax, %rdx
.LBB15_47:                              # %.preheader252
                                        #   in Loop: Header=BB15_37 Depth=1
	movl	(%rcx), %eax
	cmpl	$-1, %eax
	je	.LBB15_50
# BB#48:                                # %.lr.ph261.preheader
                                        #   in Loop: Header=BB15_37 Depth=1
	addq	$4, %rcx
	.p2align	4, 0x90
.LBB15_49:                              # %.lr.ph261
                                        #   Parent Loop BB15_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rdx)
	addq	$4, %rdx
	movl	(%rcx), %eax
	addq	$4, %rcx
	cmpl	$-1, %eax
	jne	.LBB15_49
.LBB15_50:                              # %.loopexit253
                                        #   in Loop: Header=BB15_37 Depth=1
	movl	$-1, (%rdx)
	movq	loadtop.hist(%rip), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movslq	-4(%rax,%rcx,4), %rbp
	movq	loadtop.nmemar(%rip), %rax
	movslq	-4(%rax,%rcx,4), %r13
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	(%rbx,%r12,8), %rax
	movq	8(%rax), %rdi
	leaq	4(,%r13,4), %rsi
	callq	realloc
	movq	(%rbx,%r12,8), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	je	.LBB15_83
# BB#51:                                #   in Loop: Header=BB15_37 Depth=1
	cmpl	$-1, %ebp
	movss	76(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	je	.LBB15_55
# BB#52:                                #   in Loop: Header=BB15_37 Depth=1
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rbp,8), %rcx
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	movl	(%rdx), %esi
	cmpl	(%rcx), %esi
	movq	%rdx, %rsi
	cmovgq	%rcx, %rsi
	cmovgq	%rdx, %rcx
	movl	(%rsi), %edi
	cmpl	$-1, %edi
	movq	80(%rsp), %r12          # 8-byte Reload
	je	.LBB15_56
# BB#53:                                # %.lr.ph265.preheader
                                        #   in Loop: Header=BB15_37 Depth=1
	addq	$4, %rsi
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB15_54:                              # %.lr.ph265
                                        #   Parent Loop BB15_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, (%rdx)
	addq	$4, %rdx
	movl	(%rsi), %edi
	addq	$4, %rsi
	cmpl	$-1, %edi
	jne	.LBB15_54
	jmp	.LBB15_57
	.p2align	4, 0x90
.LBB15_55:                              #   in Loop: Header=BB15_37 Depth=1
	movq	%rax, %rdx
	addq	$4, %rdx
	movl	%r15d, (%rax)
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	80(%rsp), %r12          # 8-byte Reload
	jmp	.LBB15_60
	.p2align	4, 0x90
.LBB15_56:                              #   in Loop: Header=BB15_37 Depth=1
	movq	%rax, %rdx
.LBB15_57:                              # %.preheader
                                        #   in Loop: Header=BB15_37 Depth=1
	movl	(%rcx), %eax
	cmpl	$-1, %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
	je	.LBB15_60
# BB#58:                                # %.lr.ph269.preheader
                                        #   in Loop: Header=BB15_37 Depth=1
	addq	$4, %rcx
	movq	32(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB15_59:                              # %.lr.ph269
                                        #   Parent Loop BB15_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rdx)
	addq	$4, %rdx
	movl	(%rcx), %eax
	addq	$4, %rcx
	cmpl	$-1, %eax
	jne	.LBB15_59
	jmp	.LBB15_61
	.p2align	4, 0x90
.LBB15_60:                              #   in Loop: Header=BB15_37 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
.LBB15_61:                              # %.loopexit
                                        #   in Loop: Header=BB15_37 Depth=1
	movl	$-1, (%rdx)
	mulss	.LCPI15_2(%rip), %xmm3
	movq	loadtop.tmptmplen(%rip), %rcx
	movaps	%xmm3, %xmm1
	subss	-4(%rcx,%rdi,4), %xmm1
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rsi,8), %rax
	movss	%xmm1, (%rax)
	movaps	%xmm3, %xmm0
	movq	40(%rsp), %rdx          # 8-byte Reload
	subss	-4(%rcx,%rdx,4), %xmm0
	movss	%xmm0, 4(%rax)
	xorps	%xmm2, %xmm2
	ucomiss	%xmm1, %xmm2
	jbe	.LBB15_63
# BB#62:                                #   in Loop: Header=BB15_37 Depth=1
	movl	$0, (%rax)
.LBB15_63:                              #   in Loop: Header=BB15_37 Depth=1
	xorps	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jbe	.LBB15_65
# BB#64:                                #   in Loop: Header=BB15_37 Depth=1
	movl	$0, 4(%rax)
.LBB15_65:                              #   in Loop: Header=BB15_37 Depth=1
	movss	%xmm3, -4(%rcx,%rdi,4)
	movq	loadtop.hist(%rip), %rcx
	movl	%esi, -4(%rcx,%rdi,4)
	addl	112(%rsp), %r13d        # 4-byte Folded Reload
	movq	loadtop.nmemar(%rip), %rcx
	movl	%r13d, -4(%rcx,%rdi,4)
	movq	loadtop.ac(%rip), %rbp
	testq	%rbp, %rbp
	je	.LBB15_74
	.p2align	4, 0x90
.LBB15_66:                              # %.lr.ph273
                                        #   Parent Loop BB15_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	16(%rbp), %eax
	cmpl	%r14d, %eax
	je	.LBB15_72
# BB#67:                                # %.lr.ph273
                                        #   in Loop: Header=BB15_66 Depth=2
	cmpl	%r15d, %eax
	je	.LBB15_72
# BB#68:                                #   in Loop: Header=BB15_66 Depth=2
	cmpl	%r14d, %eax
	jge	.LBB15_70
# BB#69:                                #   in Loop: Header=BB15_66 Depth=2
	movl	%r15d, %ecx
	movl	%eax, %edx
	movl	%r14d, %esi
	jmp	.LBB15_71
	.p2align	4, 0x90
.LBB15_70:                              #   in Loop: Header=BB15_66 Depth=2
	cmpl	%r15d, %eax
	movl	%eax, %ecx
	cmovll	%r15d, %ecx
	movl	%r15d, %edx
	cmovlel	%eax, %edx
	movl	%eax, %esi
	movl	%r14d, %eax
.LBB15_71:                              #   in Loop: Header=BB15_66 Depth=2
	movslq	%eax, %rbx
	movq	(%r12,%rbx,8), %rax
	subl	%ebx, %esi
	movslq	%esi, %r13
	movss	(%rax,%r13,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movslq	%edx, %rax
	movq	(%r12,%rax,8), %rdx
	subl	%eax, %ecx
	movslq	%ecx, %rax
	movss	(%rdx,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	callq	*64(%rsp)               # 8-byte Folded Reload
	movq	(%r12,%rbx,8), %rax
	movss	%xmm0, (%rax,%r13,4)
.LBB15_72:                              #   in Loop: Header=BB15_66 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB15_66
# BB#73:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB15_37 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
.LBB15_74:                              # %._crit_edge
                                        #   in Loop: Header=BB15_37 Depth=1
	movq	loadtop.treetmp(%rip), %rdi
	movq	loadtop.tree(%rip), %rcx
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	-8(%rcx,%rbx,8), %rdx
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	-8(%rcx,%rax,8), %rcx
	cvtss2sd	%xmm1, %xmm1
	movl	$.L.str.16, %esi
	movb	$2, %al
	callq	sprintf
	movq	loadtop.tree(%rip), %rax
	movq	-8(%rax,%rbx,8), %rdi
	movq	loadtop.treetmp(%rip), %rsi
	callq	strcpy
	movq	loadtop.ac(%rip), %rcx
	leaq	(%r15,%r15,2), %rdx
	movq	8(%rcx,%rdx,8), %rax
	movq	(%rcx,%rdx,8), %rcx
	movq	%rcx, (%rax)
	testq	%rcx, %rcx
	je	.LBB15_76
# BB#75:                                #   in Loop: Header=BB15_37 Depth=1
	movq	%rax, 8(%rcx)
.LBB15_76:                              #   in Loop: Header=BB15_37 Depth=1
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	-8(%r12,%rbx,8), %rdi
	callq	free
	movq	$0, -8(%r12,%rbx,8)
	movq	16(%rsp), %rbx          # 8-byte Reload
	incq	%rbx
	cmpq	104(%rsp), %rbx         # 8-byte Folded Reload
	jl	.LBB15_37
	jmp	.LBB15_80
.LBB15_77:                              # %.preheader255.thread
	movq	loadtop.ac(%rip), %rax
.LBB15_78:                              # %._crit_edge286.thread
	decl	%ebp
	movslq	%ebp, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movq	$0, (%rax,%rcx,8)
.LBB15_79:                              # %._crit_edge280.thread
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
.LBB15_80:                              # %._crit_edge277
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	movl	$.L.str.17, %edi
	movl	$.L.str.18, %esi
	callq	fopen
	movq	%rax, %rbx
	movq	loadtop.treetmp(%rip), %rdx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.23, %edi
	movl	$11, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	%rbx, %rdi
	callq	fclose
	movq	loadtop.tmptmplen(%rip), %rdi
	callq	free
	movq	$0, loadtop.tmptmplen(%rip)
	movq	loadtop.hist(%rip), %rdi
	callq	free
	movq	$0, loadtop.hist(%rip)
	movq	loadtop.ac(%rip), %rdi
	callq	free
	movq	$0, loadtop.ac(%rip)
	movq	loadtop.nmemar(%rip), %rdi
	callq	free
	movq	$0, loadtop.nmemar(%rip)
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_81:
	movq	stderr(%rip), %rcx
	movl	$.L.str.39, %edi
	movl	$21, %esi
	jmp	.LBB15_84
.LBB15_82:
	movq	stderr(%rip), %rcx
	movl	$.L.str.22, %edi
	movl	$44, %esi
	jmp	.LBB15_84
.LBB15_83:
	movq	stderr(%rip), %rcx
	movl	$.L.str.15, %edi
	movl	$24, %esi
.LBB15_84:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB15_85:
	movq	stderr(%rip), %rdi
	movl	$.L.str.20, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB15_86:
	movq	stderr(%rip), %rcx
	movl	$.L.str.10, %edi
	movl	$23, %esi
	jmp	.LBB15_84
.Lfunc_end15:
	.size	loadtop, .Lfunc_end15-loadtop
	.cfi_endproc

	.p2align	4, 0x90
	.type	cluster_mix_float,@function
cluster_mix_float:                      # @cluster_mix_float
	.cfi_startproc
# BB#0:
	movaps	%xmm0, %xmm2
	minss	%xmm1, %xmm2
	mulss	sueff1(%rip), %xmm2
	addss	%xmm1, %xmm0
	mulss	sueff05(%rip), %xmm0
	addss	%xmm2, %xmm0
	retq
.Lfunc_end16:
	.size	cluster_mix_float, .Lfunc_end16-cluster_mix_float
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI17_0:
	.long	1056964608              # float 0.5
	.text
	.p2align	4, 0x90
	.type	cluster_average_float,@function
cluster_average_float:                  # @cluster_average_float
	.cfi_startproc
# BB#0:
	addss	%xmm1, %xmm0
	mulss	.LCPI17_0(%rip), %xmm0
	retq
.Lfunc_end17:
	.size	cluster_average_float, .Lfunc_end17-cluster_average_float
	.cfi_endproc

	.p2align	4, 0x90
	.type	cluster_minimum_float,@function
cluster_minimum_float:                  # @cluster_minimum_float
	.cfi_startproc
# BB#0:
	minss	%xmm1, %xmm0
	retq
.Lfunc_end18:
	.size	cluster_minimum_float, .Lfunc_end18-cluster_minimum_float
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI19_0:
	.long	1148844442              # float 999.900024
.LCPI19_2:
	.long	1056964608              # float 0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI19_1:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout
	.p2align	4, 0x90
	.type	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout,@function
fixed_musclesupg_float_realloc_nobk_halfmtx_treeout: # @fixed_musclesupg_float_realloc_nobk_halfmtx_treeout
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi158:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi159:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi160:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi161:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi162:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi163:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi164:
	.cfi_def_cfa_offset 160
.Lcfi165:
	.cfi_offset %rbx, -56
.Lcfi166:
	.cfi_offset %r12, -48
.Lcfi167:
	.cfi_offset %r13, -40
.Lcfi168:
	.cfi_offset %r14, -32
.Lcfi169:
	.cfi_offset %r15, -24
.Lcfi170:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rcx, %r12
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movl	%edi, %ebp
	movl	$1063675494, sueff1(%rip) # imm = 0x3F666666
	movl	$1028443341, sueff05(%rip) # imm = 0x3D4CCCCD
	movl	treemethod(%rip), %edx
	cmpl	$69, %edx
	je	.LBB19_4
# BB#1:
	cmpl	$88, %edx
	je	.LBB19_5
# BB#2:
	cmpl	$113, %edx
	jne	.LBB19_120
# BB#3:                                 # %.fold.split334
	movl	$cluster_minimum_float, %eax
	jmp	.LBB19_6
.LBB19_4:                               # %.fold.split
	movl	$cluster_average_float, %eax
	jmp	.LBB19_6
.LBB19_5:
	movl	$cluster_mix_float, %eax
.LBB19_6:
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	cmpq	$0, fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.hist(%rip)
	movl	$0, %r13d
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	jne	.LBB19_8
# BB#7:
	movl	njob(%rip), %edi
	callq	AllocateIntVec
	movq	%rax, fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.hist(%rip)
	movl	njob(%rip), %edi
	callq	AllocateFloatVec
	movq	%rax, fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.tmptmplen(%rip)
	movslq	njob(%rip), %rbx
	movq	%rbx, %rax
	shlq	$3, %rax
	leaq	(%rax,%rax,2), %rdi
	callq	malloc
	movq	%rax, fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.ac(%rip)
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.nmemar(%rip)
	movl	njob(%rip), %edi
	callq	AllocateFloatVec
	movq	%rax, %r13
	movl	njob(%rip), %edi
	callq	AllocateIntVec
	movq	%rax, %r14
	imull	$50, njob(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.treetmp(%rip)
	movl	$30, %edi
	callq	AllocateCharVec
	movq	%rax, fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.nametmp(%rip)
	movl	njob(%rip), %edi
	imull	$50, %edi, %esi
	callq	AllocateCharMtx
	movq	%rax, fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.tree(%rip)
.LBB19_8:
	testl	%ebp, %ebp
	jle	.LBB19_115
# BB#9:                                 # %.preheader355.preheader
	movq	%r12, 56(%rsp)          # 8-byte Spill
	callq	__ctype_b_loc
	movq	%rax, %r12
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movl	%ebp, %r10d
	xorl	%ebp, %ebp
	movq	%r10, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB19_10:                              # %.preheader355
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_11 Depth 2
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.nametmp(%rip), %rcx
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 14(%rcx)
	movdqu	%xmm0, (%rcx)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB19_11:                              #   Parent Loop BB19_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12), %rdx
	movq	(%r15,%rbp,8), %rsi
	movsbq	(%rsi,%rax), %rsi
	movzwl	(%rdx,%rsi,2), %edx
	andl	$8, %edx
	testw	%dx, %dx
	movb	$95, %dl
	movb	$95, %bl
	je	.LBB19_13
# BB#12:                                #   in Loop: Header=BB19_11 Depth=2
	movl	%esi, %ebx
.LBB19_13:                              #   in Loop: Header=BB19_11 Depth=2
	movb	%bl, (%rcx,%rax)
	movq	(%r12), %rdi
	movq	(%r15,%rbp,8), %rsi
	movsbq	1(%rsi,%rax), %rsi
	movzwl	(%rdi,%rsi,2), %edi
	andl	$8, %edi
	testw	%di, %di
	je	.LBB19_15
# BB#14:                                #   in Loop: Header=BB19_11 Depth=2
	movl	%esi, %edx
.LBB19_15:                              #   in Loop: Header=BB19_11 Depth=2
	movb	%dl, 1(%rcx,%rax)
	addq	$2, %rax
	cmpq	$30, %rax
	jne	.LBB19_11
# BB#16:                                #   in Loop: Header=BB19_10 Depth=1
	movb	$0, 30(%rcx)
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.tree(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	incq	%rbp
	incq	%rcx
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	sprintf
	movq	8(%rsp), %r10           # 8-byte Reload
	cmpq	%r10, %rbp
	jne	.LBB19_10
# BB#17:                                # %.preheader353
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.ac(%rip), %rcx
	movq	32(%rsp), %rbp          # 8-byte Reload
	testl	%ebp, %ebp
	jle	.LBB19_116
# BB#18:                                # %.lr.ph407.preheader
	testb	$1, %r10b
	jne	.LBB19_20
# BB#19:
	xorl	%eax, %eax
	jmp	.LBB19_21
.LBB19_20:                              # %.lr.ph407.prol
	leaq	24(%rcx), %rax
	movq	%rax, (%rcx)
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.ac(%rip), %rcx
	leaq	-24(%rcx), %rax
	movq	%rax, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$1, %eax
.LBB19_21:                              # %.lr.ph407.prol.loopexit
	movq	24(%rsp), %r9           # 8-byte Reload
	cmpl	$1, %ebp
	je	.LBB19_24
# BB#22:                                # %.lr.ph407.preheader.new
	leaq	(,%rax,8), %rdx
	leaq	(%rdx,%rdx,2), %rdx
	.p2align	4, 0x90
.LBB19_23:                              # %.lr.ph407
                                        # =>This Inner Loop Header: Depth=1
	leaq	24(%rcx,%rdx), %rsi
	movq	%rsi, (%rcx,%rdx)
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.ac(%rip), %rcx
	leaq	-24(%rcx,%rdx), %rsi
	movq	%rsi, 8(%rcx,%rdx)
	movl	%eax, 16(%rcx,%rdx)
	leal	1(%rax), %esi
	leaq	48(%rcx,%rdx), %rdi
	movq	%rdi, 24(%rcx,%rdx)
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.ac(%rip), %rcx
	leaq	(%rcx,%rdx), %rdi
	movq	%rdi, 32(%rcx,%rdx)
	movl	%esi, 40(%rcx,%rdx)
	addq	$2, %rax
	addq	$48, %rdx
	cmpq	%r10, %rax
	jne	.LBB19_23
.LBB19_24:                              # %._crit_edge408
	leal	-1(%rbp), %r8d
	movslq	%r8d, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	(%rax,%rax,2), %rdx
	movq	$0, (%rcx,%rdx,8)
	testl	%ebp, %ebp
	jle	.LBB19_117
# BB#25:                                # %.lr.ph405
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.ac(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB19_121
# BB#26:                                # %.lr.ph405.split.preheader
	xorl	%edx, %edx
	movd	.LCPI19_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB19_27:                              # %.lr.ph405.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_29 Depth 2
                                        #     Child Loop BB19_33 Depth 2
	movl	$1148844442, (%r13,%rdx,4) # imm = 0x4479F99A
	movl	$-1, (%r14,%rdx,4)
	leaq	(%rdx,%rdx,2), %rsi
	movq	(%rcx,%rsi,8), %rsi
	testq	%rsi, %rsi
	movdqa	%xmm0, %xmm1
	je	.LBB19_32
# BB#28:                                # %.lr.ph5.i
                                        #   in Loop: Header=BB19_27 Depth=1
	movq	(%r9,%rdx,8), %rdi
	movdqa	%xmm0, %xmm1
	.p2align	4, 0x90
.LBB19_29:                              #   Parent Loop BB19_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	16(%rsi), %ebx
	movl	%ebx, %ebp
	subl	%edx, %ebp
	movslq	%ebp, %rbp
	movd	(%rdi,%rbp,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm1
	jbe	.LBB19_31
# BB#30:                                #   in Loop: Header=BB19_29 Depth=2
	movd	%xmm2, (%r13,%rdx,4)
	movl	%ebx, (%r14,%rdx,4)
	movdqa	%xmm2, %xmm1
.LBB19_31:                              #   in Loop: Header=BB19_29 Depth=2
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB19_29
.LBB19_32:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB19_27 Depth=1
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB19_33:                              # %.lr.ph.i
                                        #   Parent Loop BB19_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	16(%rsi), %edi
	cmpq	%rdi, %rdx
	je	.LBB19_37
# BB#34:                                #   in Loop: Header=BB19_33 Depth=2
	movslq	%edi, %rbp
	movq	(%r9,%rbp,8), %rbx
	movl	%edx, %eax
	subl	%ebp, %eax
	cltq
	movd	(%rbx,%rax,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm1
	jbe	.LBB19_36
# BB#35:                                #   in Loop: Header=BB19_33 Depth=2
	movd	%xmm2, (%r13,%rdx,4)
	movl	%edi, (%r14,%rdx,4)
	movdqa	%xmm2, %xmm1
.LBB19_36:                              #   in Loop: Header=BB19_33 Depth=2
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB19_33
.LBB19_37:                              # %setnearest.exit
                                        #   in Loop: Header=BB19_27 Depth=1
	incq	%rdx
	cmpq	%r10, %rdx
	jne	.LBB19_27
# BB#38:                                # %.preheader351
	movq	32(%rsp), %rbp          # 8-byte Reload
	testl	%ebp, %ebp
	jle	.LBB19_117
# BB#39:                                # %.lr.ph397
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.tmptmplen(%rip), %rdi
	movl	%r8d, %eax
	leaq	4(,%rax,4), %rdx
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	callq	memset
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.hist(%rip), %rax
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.nmemar(%rip), %rcx
	cmpl	$8, %ebp
	jae	.LBB19_41
# BB#40:
	movq	8(%rsp), %rdx           # 8-byte Reload
	jmp	.LBB19_49
.LBB19_41:                              # %min.iters.checked
	movl	%ebp, %r8d
	andl	$7, %r8d
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rdx, %rbx
	subq	%r8, %rbx
	je	.LBB19_45
# BB#42:                                # %vector.memcheck
	leaq	(%rcx,%rdx,4), %rsi
	cmpq	%rsi, %rax
	jae	.LBB19_46
# BB#43:                                # %vector.memcheck
	leaq	(%rax,%rdx,4), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB19_46
.LBB19_45:
	xorl	%ebx, %ebx
	jmp	.LBB19_49
.LBB19_46:                              # %vector.body.preheader
	leaq	16(%rax), %rsi
	leaq	16(%rcx), %rdi
	pcmpeqd	%xmm0, %xmm0
	movdqa	.LCPI19_1(%rip), %xmm1  # xmm1 = [1,1,1,1]
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB19_47:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm0, (%rsi)
	movdqu	%xmm1, -16(%rdi)
	movdqu	%xmm1, (%rdi)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-8, %rbp
	jne	.LBB19_47
# BB#48:                                # %middle.block
	testl	%r8d, %r8d
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB19_55
.LBB19_49:                              # %scalar.ph.preheader
	movl	%edx, %esi
	subl	%ebx, %esi
	leaq	-1(%rdx), %rdi
	subq	%rbx, %rdi
	andq	$7, %rsi
	je	.LBB19_52
# BB#50:                                # %scalar.ph.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB19_51:                              # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$-1, (%rax,%rbx,4)
	movl	$1, (%rcx,%rbx,4)
	incq	%rbx
	incq	%rsi
	jne	.LBB19_51
.LBB19_52:                              # %scalar.ph.prol.loopexit
	cmpq	$7, %rdi
	jb	.LBB19_55
# BB#53:                                # %scalar.ph.preheader.new
	subq	%rbx, %rdx
	leaq	28(%rcx,%rbx,4), %rcx
	leaq	28(%rax,%rbx,4), %rax
	.p2align	4, 0x90
.LBB19_54:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$-1, -28(%rax)
	movl	$1, -28(%rcx)
	movl	$-1, -24(%rax)
	movl	$1, -24(%rcx)
	movl	$-1, -20(%rax)
	movl	$1, -20(%rcx)
	movl	$-1, -16(%rax)
	movl	$1, -16(%rcx)
	movl	$-1, -12(%rax)
	movl	$1, -12(%rcx)
	movl	$-1, -8(%rax)
	movl	$1, -8(%rcx)
	movl	$-1, -4(%rax)
	movl	$1, -4(%rcx)
	movl	$-1, (%rax)
	movl	$1, (%rcx)
	addq	$32, %rcx
	addq	$32, %rax
	addq	$-8, %rdx
	jne	.LBB19_54
.LBB19_55:                              # %._crit_edge398
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	cmpl	$2, %ebp
	jl	.LBB19_118
# BB#56:                                # %.lr.ph394.preheader
	movl	$-1, %r9d
	movss	.LCPI19_0(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	xorl	%ebp, %ebp
	movq	%r14, 96(%rsp)          # 8-byte Spill
	movq	%r13, 88(%rsp)          # 8-byte Spill
	jmp	.LBB19_58
.LBB19_57:                              #   in Loop: Header=BB19_58 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	movq	32(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r9d, %ebx
	callq	fprintf
	movss	.LCPI19_0(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	movl	%ebx, %r9d
	jmp	.LBB19_59
	.p2align	4, 0x90
.LBB19_58:                              # %.lr.ph394
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_61 Depth 2
                                        #     Child Loop BB19_66 Depth 2
                                        #     Child Loop BB19_71 Depth 2
                                        #     Child Loop BB19_77 Depth 2
                                        #     Child Loop BB19_82 Depth 2
                                        #     Child Loop BB19_86 Depth 2
                                        #     Child Loop BB19_102 Depth 2
                                        #       Child Loop BB19_105 Depth 3
                                        #       Child Loop BB19_109 Depth 3
	movslq	%ebp, %rax
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$34, %rcx
	addl	%edx, %ecx
	addl	%ecx, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	je	.LBB19_57
.LBB19_59:                              # %.preheader349
                                        #   in Loop: Header=BB19_58 Depth=1
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.ac(%rip), %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB19_62
# BB#60:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB19_58 Depth=1
	movaps	%xmm2, %xmm0
	.p2align	4, 0x90
.LBB19_61:                              # %.lr.ph
                                        #   Parent Loop BB19_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	16(%rax), %rax
	movss	(%r13,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	cmoval	%eax, %r9d
	minss	%xmm0, %xmm1
	movq	%rcx, %rax
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	movaps	%xmm1, %xmm0
	jne	.LBB19_61
	jmp	.LBB19_63
	.p2align	4, 0x90
.LBB19_62:                              #   in Loop: Header=BB19_58 Depth=1
	movaps	%xmm2, %xmm1
.LBB19_63:                              # %._crit_edge
                                        #   in Loop: Header=BB19_58 Depth=1
	movss	%xmm1, 40(%rsp)         # 4-byte Spill
	movslq	%r9d, %rax
	movl	(%r14,%rax,4), %ecx
	cmpl	%eax, %ecx
	movl	%eax, %ebp
	cmovlel	%ecx, %ebp
	cmovll	%eax, %ecx
	movl	%ecx, (%rsp)            # 4-byte Spill
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.hist(%rip), %rax
	movslq	%ebp, %rcx
	movslq	(%rax,%rcx,4), %rbx
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.nmemar(%rip), %rax
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movslq	(%rax,%rcx,4), %rcx
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	64(%rsp), %r12          # 8-byte Reload
	movq	(%r12,%r15,8), %rax
	movq	(%rax), %rdi
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	leaq	4(,%rcx,4), %rsi
	callq	realloc
	movq	%r12, %r8
	cmpq	$-1, %rbx
	movq	(%r8,%r15,8), %rcx
	movq	%rax, (%rcx)
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	je	.LBB19_67
# BB#64:                                #   in Loop: Header=BB19_58 Depth=1
	movq	(%r8,%rbx,8), %rcx
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	movl	(%rdx), %esi
	cmpl	(%rcx), %esi
	movq	%rdx, %rsi
	cmovgq	%rcx, %rsi
	cmovgq	%rdx, %rcx
	movl	(%rsi), %edi
	cmpl	$-1, %edi
	je	.LBB19_68
# BB#65:                                # %.lr.ph366.preheader
                                        #   in Loop: Header=BB19_58 Depth=1
	addq	$4, %rsi
	movq	%rax, %rdx
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB19_66:                              # %.lr.ph366
                                        #   Parent Loop BB19_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, (%rdx)
	addq	$4, %rdx
	movl	(%rsi), %edi
	addq	$4, %rsi
	cmpl	$-1, %edi
	jne	.LBB19_66
	jmp	.LBB19_69
	.p2align	4, 0x90
.LBB19_67:                              #   in Loop: Header=BB19_58 Depth=1
	movq	%rax, %rdx
	addq	$4, %rdx
	movl	%ebp, (%rax)
	movq	16(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB19_72
	.p2align	4, 0x90
.LBB19_68:                              #   in Loop: Header=BB19_58 Depth=1
	movq	%rax, %rdx
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB19_69:                              # %.preheader347
                                        #   in Loop: Header=BB19_58 Depth=1
	movl	(%rcx), %eax
	cmpl	$-1, %eax
	je	.LBB19_72
# BB#70:                                # %.lr.ph370.preheader
                                        #   in Loop: Header=BB19_58 Depth=1
	addq	$4, %rcx
	movl	(%rsp), %esi            # 4-byte Reload
	.p2align	4, 0x90
.LBB19_71:                              # %.lr.ph370
                                        #   Parent Loop BB19_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rdx)
	addq	$4, %rdx
	movl	(%rcx), %eax
	addq	$4, %rcx
	cmpl	$-1, %eax
	jne	.LBB19_71
	jmp	.LBB19_73
	.p2align	4, 0x90
.LBB19_72:                              #   in Loop: Header=BB19_58 Depth=1
	movl	(%rsp), %esi            # 4-byte Reload
.LBB19_73:                              # %.loopexit348
                                        #   in Loop: Header=BB19_58 Depth=1
	movl	$-1, (%rdx)
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.hist(%rip), %rax
	movslq	%esi, %r15
	movslq	(%rax,%r15,4), %rbx
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.nmemar(%rip), %rax
	movslq	(%rax,%r15,4), %r12
	movq	(%r8,%rbp,8), %rax
	movq	8(%rax), %rdi
	leaq	4(,%r12,4), %rsi
	callq	realloc
	movq	64(%rsp), %r9           # 8-byte Reload
	movq	(%r9,%rbp,8), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	je	.LBB19_119
# BB#74:                                #   in Loop: Header=BB19_58 Depth=1
	cmpl	$-1, %ebx
	movq	24(%rsp), %r8           # 8-byte Reload
	movss	40(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	je	.LBB19_78
# BB#75:                                #   in Loop: Header=BB19_58 Depth=1
	movq	(%r9,%rbx,8), %rcx
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	movl	(%rdx), %esi
	cmpl	(%rcx), %esi
	movq	%rdx, %rsi
	cmovgq	%rcx, %rsi
	cmovgq	%rdx, %rcx
	movl	(%rsi), %edi
	cmpl	$-1, %edi
	movl	4(%rsp), %ebp           # 4-byte Reload
	je	.LBB19_79
# BB#76:                                # %.lr.ph375.preheader
                                        #   in Loop: Header=BB19_58 Depth=1
	addq	$4, %rsi
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB19_77:                              # %.lr.ph375
                                        #   Parent Loop BB19_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, (%rdx)
	addq	$4, %rdx
	movl	(%rsi), %edi
	addq	$4, %rsi
	cmpl	$-1, %edi
	jne	.LBB19_77
	jmp	.LBB19_80
	.p2align	4, 0x90
.LBB19_78:                              #   in Loop: Header=BB19_58 Depth=1
	movq	%rax, %rdx
	addq	$4, %rdx
	movl	(%rsp), %edi            # 4-byte Reload
	movl	%edi, (%rax)
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB19_84
	.p2align	4, 0x90
.LBB19_79:                              #   in Loop: Header=BB19_58 Depth=1
	movq	%rax, %rdx
.LBB19_80:                              # %.preheader
                                        #   in Loop: Header=BB19_58 Depth=1
	movl	(%rcx), %eax
	cmpl	$-1, %eax
	je	.LBB19_83
# BB#81:                                # %.lr.ph379.preheader
                                        #   in Loop: Header=BB19_58 Depth=1
	addq	$4, %rcx
	movl	(%rsp), %edi            # 4-byte Reload
	.p2align	4, 0x90
.LBB19_82:                              # %.lr.ph379
                                        #   Parent Loop BB19_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rdx)
	addq	$4, %rdx
	movl	(%rcx), %eax
	addq	$4, %rcx
	cmpl	$-1, %eax
	jne	.LBB19_82
	jmp	.LBB19_84
	.p2align	4, 0x90
.LBB19_83:                              #   in Loop: Header=BB19_58 Depth=1
	movl	(%rsp), %edi            # 4-byte Reload
.LBB19_84:                              # %.loopexit
                                        #   in Loop: Header=BB19_58 Depth=1
	movl	$-1, (%rdx)
	mulss	.LCPI19_2(%rip), %xmm1
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.tmptmplen(%rip), %rcx
	movaps	%xmm1, %xmm0
	movq	8(%rsp), %rsi           # 8-byte Reload
	subss	(%rcx,%rsi,4), %xmm0
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movss	%xmm0, (%rax)
	movaps	%xmm1, %xmm0
	subss	(%rcx,%r15,4), %xmm0
	movss	%xmm0, 4(%rax)
	movss	%xmm1, (%rcx,%rsi,4)
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.hist(%rip), %rcx
	movl	%edx, (%rcx,%rsi,4)
	addl	80(%rsp), %r12d         # 4-byte Folded Reload
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.nmemar(%rip), %rcx
	movl	%r12d, (%rcx,%rsi,4)
	movl	$1148844442, (%r13,%rsi,4) # imm = 0x4479F99A
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.ac(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB19_98
# BB#85:                                # %.lr.ph384
                                        #   in Loop: Header=BB19_58 Depth=1
	movq	%r15, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB19_86:                              #   Parent Loop BB19_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	16(%rbx), %r15d
	cmpl	%ebp, %r15d
	je	.LBB19_96
# BB#87:                                #   in Loop: Header=BB19_86 Depth=2
	cmpl	%edi, %r15d
	je	.LBB19_96
# BB#88:                                #   in Loop: Header=BB19_86 Depth=2
	movslq	%r15d, %r12
	cmpl	%ebp, %r15d
	movl	%edi, %eax
	movl	%r15d, %ecx
	movl	%ebp, %edx
	movl	%r15d, %esi
	jl	.LBB19_90
# BB#89:                                #   in Loop: Header=BB19_86 Depth=2
	cmpl	%edi, %r15d
	movl	%r15d, %eax
	cmovll	%edi, %eax
	movl	%edi, %ecx
	cmovlel	%r15d, %ecx
	movl	%r15d, %edx
	movl	%ebp, %esi
.LBB19_90:                              #   in Loop: Header=BB19_86 Depth=2
	movslq	%esi, %r14
	movq	(%r8,%r14,8), %rsi
	subl	%r14d, %edx
	movslq	%edx, %r13
	movss	(%rsi,%r13,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movslq	%ecx, %rcx
	movq	(%r8,%rcx,8), %rdx
	subl	%ecx, %eax
	cltq
	movss	(%rdx,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	callq	*48(%rsp)               # 8-byte Folded Reload
	movl	4(%rsp), %ebp           # 4-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	(%r8,%r14,8), %rax
	movss	%xmm0, (%rax,%r13,4)
	movq	88(%rsp), %r13          # 8-byte Reload
	movss	(%r13,%r12,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	movq	96(%rsp), %r14          # 8-byte Reload
	jbe	.LBB19_92
# BB#91:                                #   in Loop: Header=BB19_86 Depth=2
	movss	%xmm0, (%r13,%r12,4)
	movl	%ebp, (%r14,%r12,4)
.LBB19_92:                              #   in Loop: Header=BB19_86 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movss	(%r13,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	movl	(%rsp), %edi            # 4-byte Reload
	jbe	.LBB19_94
# BB#93:                                #   in Loop: Header=BB19_86 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movss	%xmm0, (%r13,%rax,4)
	movl	%r15d, (%r14,%rax,4)
.LBB19_94:                              #   in Loop: Header=BB19_86 Depth=2
	cmpl	%edi, (%r14,%r12,4)
	jne	.LBB19_96
# BB#95:                                #   in Loop: Header=BB19_86 Depth=2
	movl	%ebp, (%r14,%r12,4)
.LBB19_96:                              #   in Loop: Header=BB19_86 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB19_86
# BB#97:                                # %._crit_edge385.loopexit
                                        #   in Loop: Header=BB19_58 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movq	40(%rsp), %r15          # 8-byte Reload
.LBB19_98:                              # %._crit_edge385
                                        #   in Loop: Header=BB19_58 Depth=1
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.treetmp(%rip), %rdi
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.tree(%rip), %rcx
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	(%rcx,%rbx,8), %rdx
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movq	(%rcx,%r15,8), %rcx
	cvtss2sd	%xmm1, %xmm1
	movl	$.L.str.16, %esi
	movb	$2, %al
	callq	sprintf
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.tree(%rip), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.treetmp(%rip), %rsi
	callq	strcpy
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.ac(%rip), %rcx
	leaq	(%r15,%r15,2), %rdx
	movq	8(%rcx,%rdx,8), %rax
	movq	(%rcx,%rdx,8), %rcx
	movq	%rcx, (%rax)
	testq	%rcx, %rcx
	je	.LBB19_100
# BB#99:                                #   in Loop: Header=BB19_58 Depth=1
	movq	%rax, 8(%rcx)
.LBB19_100:                             #   in Loop: Header=BB19_58 Depth=1
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx,%r15,8), %rdi
	callq	free
	movq	%rbx, %rbp
	movq	$0, (%rbp,%r15,8)
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.ac(%rip), %r8
	testq	%r8, %r8
	movl	4(%rsp), %r9d           # 4-byte Reload
	movd	.LCPI19_0(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	je	.LBB19_114
# BB#101:                               # %.lr.ph389.split.preheader
                                        #   in Loop: Header=BB19_58 Depth=1
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB19_102:                             # %.lr.ph389.split
                                        #   Parent Loop BB19_58 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB19_105 Depth 3
                                        #       Child Loop BB19_109 Depth 3
	movslq	16(%rcx), %rdx
	cmpl	%r9d, (%r14,%rdx,4)
	jne	.LBB19_113
# BB#103:                               #   in Loop: Header=BB19_102 Depth=2
	movl	$1148844442, (%r13,%rdx,4) # imm = 0x4479F99A
	movl	$-1, (%r14,%rdx,4)
	leaq	(%rdx,%rdx,2), %rsi
	movq	(%r8,%rsi,8), %rsi
	testq	%rsi, %rsi
	movdqa	%xmm2, %xmm0
	je	.LBB19_108
# BB#104:                               # %.lr.ph5.i340
                                        #   in Loop: Header=BB19_102 Depth=2
	movq	(%rbp,%rdx,8), %rdi
	movdqa	%xmm2, %xmm0
	.p2align	4, 0x90
.LBB19_105:                             #   Parent Loop BB19_58 Depth=1
                                        #     Parent Loop BB19_102 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	16(%rsi), %eax
	movl	%eax, %ebx
	subl	%edx, %ebx
	movslq	%ebx, %rbx
	movd	(%rdi,%rbx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jbe	.LBB19_107
# BB#106:                               #   in Loop: Header=BB19_105 Depth=3
	movd	%xmm1, (%r13,%rdx,4)
	movl	%eax, (%r14,%rdx,4)
	movdqa	%xmm1, %xmm0
.LBB19_107:                             #   in Loop: Header=BB19_105 Depth=3
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB19_105
.LBB19_108:                             # %.lr.ph.i345.preheader
                                        #   in Loop: Header=BB19_102 Depth=2
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB19_109:                             # %.lr.ph.i345
                                        #   Parent Loop BB19_58 Depth=1
                                        #     Parent Loop BB19_102 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	16(%rsi), %edi
	cmpl	%edi, %edx
	je	.LBB19_113
# BB#110:                               #   in Loop: Header=BB19_109 Depth=3
	movslq	%edi, %rax
	movq	(%rbp,%rax,8), %rax
	movl	%edx, %ebx
	subl	%edi, %ebx
	movslq	%ebx, %rbx
	movd	(%rax,%rbx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jbe	.LBB19_112
# BB#111:                               #   in Loop: Header=BB19_109 Depth=3
	movd	%xmm1, (%r13,%rdx,4)
	movl	%edi, (%r14,%rdx,4)
	movdqa	%xmm1, %xmm0
.LBB19_112:                             #   in Loop: Header=BB19_109 Depth=3
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB19_109
.LBB19_113:                             # %setnearest.exit346
                                        #   in Loop: Header=BB19_102 Depth=2
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB19_102
.LBB19_114:                             # %._crit_edge390
                                        #   in Loop: Header=BB19_58 Depth=1
	movq	16(%rsp), %rbp          # 8-byte Reload
	incq	%rbp
	cmpq	72(%rsp), %rbp          # 8-byte Folded Reload
	jl	.LBB19_58
	jmp	.LBB19_118
.LBB19_115:                             # %.preheader353.thread
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.ac(%rip), %rcx
.LBB19_116:                             # %._crit_edge408.thread
	decl	%ebp
	movslq	%ebp, %rax
	leaq	(%rax,%rax,2), %rax
	movq	$0, (%rcx,%rax,8)
.LBB19_117:                             # %._crit_edge398.thread
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
.LBB19_118:                             # %._crit_edge395
	movl	$.L.str.17, %edi
	movl	$.L.str.18, %esi
	callq	fopen
	movq	%rax, %rbx
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.treetmp(%rip), %rdx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	%rbx, %rdi
	callq	fclose
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.tree(%rip), %rdi
	callq	FreeCharMtx
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.treetmp(%rip), %rdi
	callq	free
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.nametmp(%rip), %rdi
	callq	free
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.tmptmplen(%rip), %rdi
	callq	free
	movq	$0, fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.tmptmplen(%rip)
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.hist(%rip), %rdi
	callq	free
	movq	$0, fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.hist(%rip)
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.ac(%rip), %rdi
	callq	free
	movq	$0, fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.ac(%rip)
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.nmemar(%rip), %rdi
	callq	free
	movq	$0, fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.nmemar(%rip)
	movq	%r13, %rdi
	callq	free
	movq	%r14, %rdi
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB19_119:
	movq	stderr(%rip), %rcx
	movl	$.L.str.15, %edi
	movl	$24, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB19_120:
	movq	stderr(%rip), %rdi
	movl	$.L.str.20, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB19_121:                             # %.lr.ph405.split.us
	movl	$1148844442, (%r13)     # imm = 0x4479F99A
	movl	$-1, (%r14)
	ud2
.Lfunc_end19:
	.size	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout, .Lfunc_end19-fixed_musclesupg_float_realloc_nobk_halfmtx_treeout
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI20_0:
	.long	1148844442              # float 999.900024
.LCPI20_2:
	.long	1056964608              # float 0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI20_1:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	fixed_musclesupg_float_realloc_nobk_halfmtx
	.p2align	4, 0x90
	.type	fixed_musclesupg_float_realloc_nobk_halfmtx,@function
fixed_musclesupg_float_realloc_nobk_halfmtx: # @fixed_musclesupg_float_realloc_nobk_halfmtx
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi171:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi172:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi173:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi174:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi175:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi176:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi177:
	.cfi_def_cfa_offset 160
.Lcfi178:
	.cfi_offset %rbx, -56
.Lcfi179:
	.cfi_offset %r12, -48
.Lcfi180:
	.cfi_offset %r13, -40
.Lcfi181:
	.cfi_offset %r14, -32
.Lcfi182:
	.cfi_offset %r15, -24
.Lcfi183:
	.cfi_offset %rbp, -16
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	%rdx, %r11
	movq	%rsi, %r15
	movl	%edi, %r12d
	movl	$1063675494, sueff1(%rip) # imm = 0x3F666666
	movl	$1028443341, sueff05(%rip) # imm = 0x3D4CCCCD
	movl	treemethod(%rip), %edx
	cmpl	$69, %edx
	je	.LBB20_4
# BB#1:
	cmpl	$88, %edx
	je	.LBB20_5
# BB#2:
	cmpl	$113, %edx
	jne	.LBB20_104
# BB#3:                                 # %.fold.split298
	movl	$cluster_minimum_float, %eax
	jmp	.LBB20_6
.LBB20_4:                               # %.fold.split
	movl	$cluster_average_float, %eax
	jmp	.LBB20_6
.LBB20_5:
	movl	$cluster_mix_float, %eax
.LBB20_6:
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	cmpq	$0, fixed_musclesupg_float_realloc_nobk_halfmtx.hist(%rip)
	movl	$0, %r10d
	jne	.LBB20_8
# BB#7:
	movl	njob(%rip), %edi
	movq	%r11, %rbp
	callq	AllocateIntVec
	movq	%rax, fixed_musclesupg_float_realloc_nobk_halfmtx.hist(%rip)
	movl	njob(%rip), %edi
	callq	AllocateFloatVec
	movq	%rax, fixed_musclesupg_float_realloc_nobk_halfmtx.tmptmplen(%rip)
	movslq	njob(%rip), %rbx
	movq	%rbx, %rax
	shlq	$3, %rax
	leaq	(%rax,%rax,2), %rdi
	callq	malloc
	movq	%rax, fixed_musclesupg_float_realloc_nobk_halfmtx.ac(%rip)
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, fixed_musclesupg_float_realloc_nobk_halfmtx.nmemar(%rip)
	movl	njob(%rip), %edi
	callq	AllocateFloatVec
	movq	%rax, %rbx
	movl	njob(%rip), %edi
	callq	AllocateIntVec
	movq	%rbp, %r11
	movq	%rbx, %r10
.LBB20_8:
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.ac(%rip), %rcx
	testl	%r12d, %r12d
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%r10, (%rsp)            # 8-byte Spill
	jle	.LBB20_100
# BB#9:                                 # %.lr.ph368.preheader
	movl	%r12d, %r14d
	testb	$1, %r14b
	jne	.LBB20_11
# BB#10:
	xorl	%ebx, %ebx
	cmpl	$1, %r12d
	jne	.LBB20_12
	jmp	.LBB20_14
.LBB20_11:                              # %.lr.ph368.prol
	leaq	24(%rcx), %rdx
	movq	%rdx, (%rcx)
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.ac(%rip), %rcx
	leaq	-24(%rcx), %rdx
	movq	%rdx, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$1, %ebx
	cmpl	$1, %r12d
	je	.LBB20_14
.LBB20_12:                              # %.lr.ph368.preheader.new
	leaq	(,%rbx,8), %rdx
	leaq	(%rdx,%rdx,2), %rdx
	.p2align	4, 0x90
.LBB20_13:                              # %.lr.ph368
                                        # =>This Inner Loop Header: Depth=1
	leaq	24(%rcx,%rdx), %rsi
	movq	%rsi, (%rcx,%rdx)
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.ac(%rip), %rcx
	leaq	-24(%rcx,%rdx), %rsi
	movq	%rsi, 8(%rcx,%rdx)
	movl	%ebx, 16(%rcx,%rdx)
	leal	1(%rbx), %esi
	leaq	48(%rcx,%rdx), %rdi
	movq	%rdi, 24(%rcx,%rdx)
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.ac(%rip), %rcx
	leaq	(%rcx,%rdx), %rdi
	movq	%rdi, 32(%rcx,%rdx)
	movl	%esi, 40(%rcx,%rdx)
	addq	$2, %rbx
	addq	$48, %rdx
	cmpq	%r14, %rbx
	jne	.LBB20_13
.LBB20_14:                              # %._crit_edge369
	leal	-1(%r12), %r8d
	movslq	%r8d, %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	leaq	(%rdx,%rdx,2), %rdx
	movq	$0, (%rcx,%rdx,8)
	testl	%r12d, %r12d
	jle	.LBB20_101
# BB#15:                                # %.lr.ph365
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.ac(%rip), %r9
	testq	%r9, %r9
	je	.LBB20_105
# BB#16:                                # %.lr.ph365.split.preheader
	xorl	%edx, %edx
	movd	.LCPI20_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB20_17:                              # %.lr.ph365.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_19 Depth 2
                                        #     Child Loop BB20_23 Depth 2
	movl	$1148844442, (%r10,%rdx,4) # imm = 0x4479F99A
	movl	$-1, (%rax,%rdx,4)
	leaq	(%rdx,%rdx,2), %rcx
	movq	(%r9,%rcx,8), %rsi
	testq	%rsi, %rsi
	movdqa	%xmm0, %xmm1
	je	.LBB20_22
# BB#18:                                # %.lr.ph5.i
                                        #   in Loop: Header=BB20_17 Depth=1
	movq	(%r15,%rdx,8), %rdi
	movdqa	%xmm0, %xmm1
	.p2align	4, 0x90
.LBB20_19:                              #   Parent Loop BB20_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	16(%rsi), %ebx
	movl	%ebx, %ecx
	subl	%edx, %ecx
	movslq	%ecx, %rcx
	movd	(%rdi,%rcx,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm1
	jbe	.LBB20_21
# BB#20:                                #   in Loop: Header=BB20_19 Depth=2
	movd	%xmm2, (%r10,%rdx,4)
	movl	%ebx, (%rax,%rdx,4)
	movdqa	%xmm2, %xmm1
.LBB20_21:                              #   in Loop: Header=BB20_19 Depth=2
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB20_19
.LBB20_22:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB20_17 Depth=1
	movq	%r9, %rsi
	.p2align	4, 0x90
.LBB20_23:                              # %.lr.ph.i
                                        #   Parent Loop BB20_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	16(%rsi), %edi
	cmpq	%rdi, %rdx
	je	.LBB20_27
# BB#24:                                #   in Loop: Header=BB20_23 Depth=2
	movslq	%edi, %rbp
	movq	(%r15,%rbp,8), %rcx
	movl	%edx, %ebx
	subl	%ebp, %ebx
	movslq	%ebx, %rbp
	movd	(%rcx,%rbp,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm1
	jbe	.LBB20_26
# BB#25:                                #   in Loop: Header=BB20_23 Depth=2
	movd	%xmm2, (%r10,%rdx,4)
	movl	%edi, (%rax,%rdx,4)
	movdqa	%xmm2, %xmm1
.LBB20_26:                              #   in Loop: Header=BB20_23 Depth=2
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB20_23
.LBB20_27:                              # %setnearest.exit
                                        #   in Loop: Header=BB20_17 Depth=1
	incq	%rdx
	cmpq	%r14, %rdx
	jne	.LBB20_17
# BB#28:                                # %.preheader314
	testl	%r12d, %r12d
	jle	.LBB20_101
# BB#29:                                # %.lr.ph357
	movq	%r11, %rbx
	movq	%r15, 56(%rsp)          # 8-byte Spill
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.tmptmplen(%rip), %rdi
	movl	%r8d, %eax
	leaq	4(,%rax,4), %rdx
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	callq	memset
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.hist(%rip), %rax
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.nmemar(%rip), %rcx
	cmpl	$8, %r12d
	jb	.LBB20_38
# BB#30:                                # %min.iters.checked
	movl	%r12d, %r8d
	andl	$7, %r8d
	movq	%r14, %r15
	subq	%r8, %r15
	je	.LBB20_34
# BB#31:                                # %vector.memcheck
	leaq	(%rcx,%r14,4), %rdx
	cmpq	%rdx, %rax
	jae	.LBB20_35
# BB#32:                                # %vector.memcheck
	leaq	(%rax,%r14,4), %rdx
	cmpq	%rdx, %rcx
	jae	.LBB20_35
.LBB20_34:
	xorl	%r15d, %r15d
	jmp	.LBB20_38
.LBB20_35:                              # %vector.body.preheader
	leaq	16(%rax), %rsi
	leaq	16(%rcx), %rdi
	pcmpeqd	%xmm0, %xmm0
	movdqa	.LCPI20_1(%rip), %xmm1  # xmm1 = [1,1,1,1]
	movq	%r15, %rdx
	.p2align	4, 0x90
.LBB20_36:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm0, (%rsi)
	movdqu	%xmm1, -16(%rdi)
	movdqu	%xmm1, (%rdi)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB20_36
# BB#37:                                # %middle.block
	testl	%r8d, %r8d
	je	.LBB20_44
.LBB20_38:                              # %scalar.ph.preheader
	movl	%r14d, %esi
	subl	%r15d, %esi
	leaq	-1(%r14), %rdx
	subq	%r15, %rdx
	andq	$7, %rsi
	je	.LBB20_41
# BB#39:                                # %scalar.ph.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB20_40:                              # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$-1, (%rax,%r15,4)
	movl	$1, (%rcx,%r15,4)
	incq	%r15
	incq	%rsi
	jne	.LBB20_40
.LBB20_41:                              # %scalar.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB20_44
# BB#42:                                # %scalar.ph.preheader.new
	subq	%r15, %r14
	leaq	28(%rcx,%r15,4), %rcx
	leaq	28(%rax,%r15,4), %rax
	.p2align	4, 0x90
.LBB20_43:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$-1, -28(%rax)
	movl	$1, -28(%rcx)
	movl	$-1, -24(%rax)
	movl	$1, -24(%rcx)
	movl	$-1, -20(%rax)
	movl	$1, -20(%rcx)
	movl	$-1, -16(%rax)
	movl	$1, -16(%rcx)
	movl	$-1, -12(%rax)
	movl	$1, -12(%rcx)
	movl	$-1, -8(%rax)
	movl	$1, -8(%rcx)
	movl	$-1, -4(%rax)
	movl	$1, -4(%rcx)
	movl	$-1, (%rax)
	movl	$1, (%rcx)
	addq	$32, %rcx
	addq	$32, %rax
	addq	$-8, %r14
	jne	.LBB20_43
.LBB20_44:                              # %._crit_edge358
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	%r12, 64(%rsp)          # 8-byte Spill
	cmpl	$2, %r12d
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	(%rsp), %r10            # 8-byte Reload
	movq	%rbx, %r11
	jl	.LBB20_102
# BB#45:                                # %.lr.ph354.preheader
	movl	$-1, %r12d
	movss	.LCPI20_0(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	xorl	%r15d, %r15d
	movq	%r11, 72(%rsp)          # 8-byte Spill
	jmp	.LBB20_47
.LBB20_46:                              #   in Loop: Header=BB20_47 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	movq	64(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movq	%r13, %r11
	movss	.LCPI20_0(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	movq	(%rsp), %r10            # 8-byte Reload
	movq	8(%rsp), %r9            # 8-byte Reload
	jmp	.LBB20_48
	.p2align	4, 0x90
.LBB20_47:                              # %.lr.ph354
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_50 Depth 2
                                        #     Child Loop BB20_55 Depth 2
                                        #     Child Loop BB20_60 Depth 2
                                        #     Child Loop BB20_65 Depth 2
                                        #     Child Loop BB20_70 Depth 2
                                        #     Child Loop BB20_72 Depth 2
                                        #     Child Loop BB20_87 Depth 2
                                        #       Child Loop BB20_90 Depth 3
                                        #       Child Loop BB20_94 Depth 3
	movslq	%r15d, %rax
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$34, %rcx
	addl	%edx, %ecx
	addl	%ecx, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	movq	%r11, %r13
	je	.LBB20_46
.LBB20_48:                              # %.preheader312
                                        #   in Loop: Header=BB20_47 Depth=1
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.ac(%rip), %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB20_51
# BB#49:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB20_47 Depth=1
	movaps	%xmm2, %xmm0
	.p2align	4, 0x90
.LBB20_50:                              # %.lr.ph
                                        #   Parent Loop BB20_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	16(%rax), %rax
	movss	(%r10,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	cmoval	%eax, %r12d
	minss	%xmm0, %xmm1
	movq	%rcx, %rax
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	movaps	%xmm1, %xmm0
	jne	.LBB20_50
	jmp	.LBB20_52
	.p2align	4, 0x90
.LBB20_51:                              #   in Loop: Header=BB20_47 Depth=1
	movaps	%xmm2, %xmm1
.LBB20_52:                              # %._crit_edge
                                        #   in Loop: Header=BB20_47 Depth=1
	movss	%xmm1, 16(%rsp)         # 4-byte Spill
	movslq	%r12d, %rax
	movl	(%r9,%rax,4), %ebx
	cmpl	%eax, %ebx
	movl	%eax, %r12d
	cmovlel	%ebx, %r12d
	cmovll	%eax, %ebx
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.hist(%rip), %rax
	movslq	%r12d, %rcx
	movslq	(%rax,%rcx,4), %r14
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.nmemar(%rip), %rax
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movslq	(%rax,%rcx,4), %rcx
	movq	(%r11,%r15,8), %rax
	movq	(%rax), %rdi
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leaq	4(,%rcx,4), %rsi
	callq	realloc
	movq	%r13, %rbp
	cmpq	$-1, %r14
	movq	(%rbp,%r15,8), %rcx
	movq	%rax, (%rcx)
	je	.LBB20_56
# BB#53:                                #   in Loop: Header=BB20_47 Depth=1
	movq	(%rbp,%r14,8), %rcx
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	movl	(%rdx), %esi
	cmpl	(%rcx), %esi
	movq	%rdx, %rsi
	cmovgq	%rcx, %rsi
	cmovgq	%rdx, %rcx
	movl	(%rsi), %edi
	cmpl	$-1, %edi
	je	.LBB20_57
# BB#54:                                # %.lr.ph326.preheader
                                        #   in Loop: Header=BB20_47 Depth=1
	addq	$4, %rsi
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB20_55:                              # %.lr.ph326
                                        #   Parent Loop BB20_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, (%rdx)
	addq	$4, %rdx
	movl	(%rsi), %edi
	addq	$4, %rsi
	cmpl	$-1, %edi
	jne	.LBB20_55
	jmp	.LBB20_58
	.p2align	4, 0x90
.LBB20_56:                              #   in Loop: Header=BB20_47 Depth=1
	movq	%rax, %rdx
	addq	$4, %rdx
	movl	%r12d, (%rax)
	jmp	.LBB20_61
	.p2align	4, 0x90
.LBB20_57:                              #   in Loop: Header=BB20_47 Depth=1
	movq	%rax, %rdx
.LBB20_58:                              # %.preheader310
                                        #   in Loop: Header=BB20_47 Depth=1
	movl	(%rcx), %eax
	cmpl	$-1, %eax
	je	.LBB20_61
# BB#59:                                # %.lr.ph330.preheader
                                        #   in Loop: Header=BB20_47 Depth=1
	addq	$4, %rcx
	.p2align	4, 0x90
.LBB20_60:                              # %.lr.ph330
                                        #   Parent Loop BB20_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rdx)
	addq	$4, %rdx
	movl	(%rcx), %eax
	addq	$4, %rcx
	cmpl	$-1, %eax
	jne	.LBB20_60
.LBB20_61:                              # %.loopexit311
                                        #   in Loop: Header=BB20_47 Depth=1
	movl	$-1, (%rdx)
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.hist(%rip), %rax
	movslq	%ebx, %rcx
	movslq	(%rax,%rcx,4), %r14
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.nmemar(%rip), %rax
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movslq	(%rax,%rcx,4), %rcx
	movq	(%rbp,%r15,8), %rax
	movq	8(%rax), %rdi
	movq	%rcx, %rbp
	leaq	4(,%rcx,4), %rsi
	callq	realloc
	movq	(%r13,%r15,8), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	je	.LBB20_103
# BB#62:                                #   in Loop: Header=BB20_47 Depth=1
	cmpl	$-1, %r14d
	movq	(%rsp), %r8             # 8-byte Reload
	movss	16(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	je	.LBB20_66
# BB#63:                                #   in Loop: Header=BB20_47 Depth=1
	movq	(%r13,%r14,8), %rcx
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	movl	(%rdx), %esi
	cmpl	(%rcx), %esi
	movq	%rdx, %rsi
	cmovgq	%rcx, %rsi
	cmovgq	%rdx, %rcx
	movl	(%rsi), %edi
	cmpl	$-1, %edi
	je	.LBB20_67
# BB#64:                                # %.lr.ph335.preheader
                                        #   in Loop: Header=BB20_47 Depth=1
	addq	$4, %rsi
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB20_65:                              # %.lr.ph335
                                        #   Parent Loop BB20_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, (%rdx)
	addq	$4, %rdx
	movl	(%rsi), %edi
	addq	$4, %rsi
	cmpl	$-1, %edi
	jne	.LBB20_65
	jmp	.LBB20_68
	.p2align	4, 0x90
.LBB20_66:                              #   in Loop: Header=BB20_47 Depth=1
	movq	%rax, %rdx
	addq	$4, %rdx
	movl	%ebx, (%rax)
	jmp	.LBB20_71
	.p2align	4, 0x90
.LBB20_67:                              #   in Loop: Header=BB20_47 Depth=1
	movq	%rax, %rdx
.LBB20_68:                              # %.preheader
                                        #   in Loop: Header=BB20_47 Depth=1
	movl	(%rcx), %eax
	cmpl	$-1, %eax
	je	.LBB20_71
# BB#69:                                # %.lr.ph339.preheader
                                        #   in Loop: Header=BB20_47 Depth=1
	addq	$4, %rcx
	.p2align	4, 0x90
.LBB20_70:                              # %.lr.ph339
                                        #   Parent Loop BB20_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rdx)
	addq	$4, %rdx
	movl	(%rcx), %eax
	addq	$4, %rcx
	cmpl	$-1, %eax
	jne	.LBB20_70
.LBB20_71:                              # %.loopexit
                                        #   in Loop: Header=BB20_47 Depth=1
	movl	$-1, (%rdx)
	mulss	.LCPI20_2(%rip), %xmm1
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.tmptmplen(%rip), %rax
	movaps	%xmm1, %xmm0
	movq	40(%rsp), %rdx          # 8-byte Reload
	subss	(%rax,%rdx,4), %xmm0
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%r15,8), %rcx
	movss	%xmm0, (%rcx)
	movaps	%xmm1, %xmm0
	movq	24(%rsp), %rsi          # 8-byte Reload
	subss	(%rax,%rsi,4), %xmm0
	movss	%xmm0, 4(%rcx)
	movss	%xmm1, (%rax,%rdx,4)
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.hist(%rip), %rax
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movl	%r15d, (%rax,%rdx,4)
	addl	96(%rsp), %ebp          # 4-byte Folded Reload
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.nmemar(%rip), %rax
	movl	%ebp, (%rax,%rdx,4)
	movl	$1148844442, (%r8,%rdx,4) # imm = 0x4479F99A
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.ac(%rip), %r14
	movl	%r12d, 52(%rsp)         # 4-byte Spill
	movl	%ebx, 48(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB20_72:                              #   Parent Loop BB20_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	16(%r14), %r15d
	cmpl	%r12d, %r15d
	je	.LBB20_82
# BB#73:                                #   in Loop: Header=BB20_72 Depth=2
	cmpl	%ebx, %r15d
	je	.LBB20_82
# BB#74:                                #   in Loop: Header=BB20_72 Depth=2
	movslq	%r15d, %r13
	cmpl	%r12d, %r15d
	movl	%ebx, %eax
	movl	%r15d, %ecx
	movl	%r12d, %edx
	movl	%r15d, %esi
	jl	.LBB20_76
# BB#75:                                #   in Loop: Header=BB20_72 Depth=2
	cmpl	%ebx, %r15d
	movl	%r15d, %eax
	cmovll	%ebx, %eax
	movl	%ebx, %ecx
	cmovlel	%r15d, %ecx
	movl	%r15d, %edx
	movl	%r12d, %esi
.LBB20_76:                              #   in Loop: Header=BB20_72 Depth=2
	movslq	%esi, %rbx
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	(%r12,%rbx,8), %rsi
	subl	%ebx, %edx
	movslq	%edx, %rbp
	movss	(%rsi,%rbp,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movslq	%ecx, %rcx
	movq	(%r12,%rcx,8), %rdx
	subl	%ecx, %eax
	cltq
	movss	(%rdx,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	callq	*32(%rsp)               # 8-byte Folded Reload
	movq	(%r12,%rbx,8), %rax
	movl	52(%rsp), %r12d         # 4-byte Reload
	movss	%xmm0, (%rax,%rbp,4)
	movq	(%rsp), %rcx            # 8-byte Reload
	movss	(%rcx,%r13,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	movq	8(%rsp), %rax           # 8-byte Reload
	jbe	.LBB20_78
# BB#77:                                #   in Loop: Header=BB20_72 Depth=2
	movss	%xmm0, (%rcx,%r13,4)
	movl	%r12d, (%rax,%r13,4)
.LBB20_78:                              #   in Loop: Header=BB20_72 Depth=2
	movq	40(%rsp), %rdx          # 8-byte Reload
	movss	(%rcx,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	movl	48(%rsp), %ebx          # 4-byte Reload
	jbe	.LBB20_80
# BB#79:                                #   in Loop: Header=BB20_72 Depth=2
	movq	40(%rsp), %rdx          # 8-byte Reload
	movss	%xmm0, (%rcx,%rdx,4)
	movl	%r15d, (%rax,%rdx,4)
.LBB20_80:                              #   in Loop: Header=BB20_72 Depth=2
	cmpl	%ebx, (%rax,%r13,4)
	jne	.LBB20_82
# BB#81:                                #   in Loop: Header=BB20_72 Depth=2
	movl	%r12d, (%rax,%r13,4)
.LBB20_82:                              #   in Loop: Header=BB20_72 Depth=2
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB20_72
# BB#83:                                # %._crit_edge345.loopexit
                                        #   in Loop: Header=BB20_47 Depth=1
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.ac(%rip), %rcx
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rax,2), %rdx
	movq	8(%rcx,%rdx,8), %rax
	movq	(%rcx,%rdx,8), %rcx
	movq	%rcx, (%rax)
	testq	%rcx, %rcx
	je	.LBB20_85
# BB#84:                                #   in Loop: Header=BB20_47 Depth=1
	movq	%rax, 8(%rcx)
.LBB20_85:                              #   in Loop: Header=BB20_47 Depth=1
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	(%rbx,%rbp,8), %rdi
	callq	free
	movq	$0, (%rbx,%rbp,8)
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.ac(%rip), %r8
	testq	%r8, %r8
	movq	72(%rsp), %r11          # 8-byte Reload
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	(%rsp), %r10            # 8-byte Reload
	movss	.LCPI20_0(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	movq	16(%rsp), %r15          # 8-byte Reload
	je	.LBB20_99
# BB#86:                                # %.lr.ph349.split.preheader
                                        #   in Loop: Header=BB20_47 Depth=1
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB20_87:                              # %.lr.ph349.split
                                        #   Parent Loop BB20_47 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB20_90 Depth 3
                                        #       Child Loop BB20_94 Depth 3
	movslq	16(%rcx), %rdx
	cmpl	%r12d, (%r9,%rdx,4)
	jne	.LBB20_98
# BB#88:                                #   in Loop: Header=BB20_87 Depth=2
	movl	$1148844442, (%r10,%rdx,4) # imm = 0x4479F99A
	movl	$-1, (%r9,%rdx,4)
	leaq	(%rdx,%rdx,2), %rax
	movq	(%r8,%rax,8), %rsi
	testq	%rsi, %rsi
	movaps	%xmm2, %xmm0
	je	.LBB20_93
# BB#89:                                # %.lr.ph5.i303
                                        #   in Loop: Header=BB20_87 Depth=2
	movq	(%rbx,%rdx,8), %rdi
	movaps	%xmm2, %xmm0
	.p2align	4, 0x90
.LBB20_90:                              #   Parent Loop BB20_47 Depth=1
                                        #     Parent Loop BB20_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	16(%rsi), %ebp
	movl	%ebp, %eax
	subl	%edx, %eax
	cltq
	movss	(%rdi,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jbe	.LBB20_92
# BB#91:                                #   in Loop: Header=BB20_90 Depth=3
	movss	%xmm1, (%r10,%rdx,4)
	movl	%ebp, (%r9,%rdx,4)
	movaps	%xmm1, %xmm0
.LBB20_92:                              #   in Loop: Header=BB20_90 Depth=3
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB20_90
.LBB20_93:                              # %.lr.ph.i308.preheader
                                        #   in Loop: Header=BB20_87 Depth=2
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB20_94:                              # %.lr.ph.i308
                                        #   Parent Loop BB20_47 Depth=1
                                        #     Parent Loop BB20_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	16(%rsi), %edi
	cmpl	%edi, %edx
	je	.LBB20_98
# BB#95:                                #   in Loop: Header=BB20_94 Depth=3
	movslq	%edi, %rax
	movq	(%rbx,%rax,8), %rax
	movl	%edx, %ebp
	subl	%edi, %ebp
	movslq	%ebp, %rbp
	movss	(%rax,%rbp,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jbe	.LBB20_97
# BB#96:                                #   in Loop: Header=BB20_94 Depth=3
	movss	%xmm1, (%r10,%rdx,4)
	movl	%edi, (%r9,%rdx,4)
	movaps	%xmm1, %xmm0
.LBB20_97:                              #   in Loop: Header=BB20_94 Depth=3
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB20_94
.LBB20_98:                              # %setnearest.exit309
                                        #   in Loop: Header=BB20_87 Depth=2
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB20_87
.LBB20_99:                              # %._crit_edge350
                                        #   in Loop: Header=BB20_47 Depth=1
	incq	%r15
	cmpq	80(%rsp), %r15          # 8-byte Folded Reload
	jl	.LBB20_47
	jmp	.LBB20_102
.LBB20_100:                             # %._crit_edge369.thread
	decl	%r12d
	movslq	%r12d, %rax
	leaq	(%rax,%rax,2), %rax
	movq	$0, (%rcx,%rax,8)
.LBB20_101:                             # %._crit_edge358.thread
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	(%rsp), %r10            # 8-byte Reload
	movq	8(%rsp), %r9            # 8-byte Reload
.LBB20_102:                             # %._crit_edge355
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.tmptmplen(%rip), %rdi
	movq	%r9, %rbx
	movq	%r10, %rbp
	callq	free
	movq	$0, fixed_musclesupg_float_realloc_nobk_halfmtx.tmptmplen(%rip)
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.hist(%rip), %rdi
	callq	free
	movq	$0, fixed_musclesupg_float_realloc_nobk_halfmtx.hist(%rip)
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.ac(%rip), %rdi
	callq	free
	movq	$0, fixed_musclesupg_float_realloc_nobk_halfmtx.ac(%rip)
	movq	fixed_musclesupg_float_realloc_nobk_halfmtx.nmemar(%rip), %rdi
	callq	free
	movq	$0, fixed_musclesupg_float_realloc_nobk_halfmtx.nmemar(%rip)
	movq	%rbp, %rdi
	callq	free
	movq	%rbx, %rdi
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB20_103:
	movq	stderr(%rip), %rcx
	movl	$.L.str.15, %edi
	movl	$24, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB20_104:
	movq	stderr(%rip), %rdi
	movl	$.L.str.20, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB20_105:                             # %.lr.ph365.split.us
	movl	$1148844442, (%r10)     # imm = 0x4479F99A
	movl	$-1, (%rax)
	ud2
.Lfunc_end20:
	.size	fixed_musclesupg_float_realloc_nobk_halfmtx, .Lfunc_end20-fixed_musclesupg_float_realloc_nobk_halfmtx
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI21_0:
	.long	3212836864              # float -1
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI21_1:
	.quad	4602678819172646912     # double 0.5
.LCPI21_2:
	.quad	4606281698874543309     # double 0.90000000000000002
.LCPI21_3:
	.quad	4591870180066957722     # double 0.10000000000000001
.LCPI21_4:
	.quad	0                       # double 0
	.text
	.globl	veryfastsupg_double_loadtop
	.p2align	4, 0x90
	.type	veryfastsupg_double_loadtop,@function
veryfastsupg_double_loadtop:            # @veryfastsupg_double_loadtop
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi184:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi185:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi186:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi187:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi188:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi189:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi190:
	.cfi_def_cfa_offset 112
.Lcfi191:
	.cfi_offset %rbx, -56
.Lcfi192:
	.cfi_offset %r12, -48
.Lcfi193:
	.cfi_offset %r13, -40
.Lcfi194:
	.cfi_offset %r14, -32
.Lcfi195:
	.cfi_offset %r15, -24
.Lcfi196:
	.cfi_offset %rbp, -16
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rdx, %r15
	movq	%rsi, %r12
	movl	%edi, %r13d
	movl	$.L.str.8, %edi
	movl	$.L.str.9, %esi
	callq	fopen
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB21_1
# BB#3:
	cmpq	$0, veryfastsupg_double_loadtop.hist(%rip)
	jne	.LBB21_5
# BB#4:
	imull	$50, njob(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, veryfastsupg_double_loadtop.treetmp(%rip)
	movl	njob(%rip), %edi
	imull	$50, %edi, %esi
	callq	AllocateCharMtx
	movq	%rax, veryfastsupg_double_loadtop.tree(%rip)
	movl	njob(%rip), %edi
	callq	AllocateIntVec
	movq	%rax, veryfastsupg_double_loadtop.hist(%rip)
	movslq	njob(%rip), %rbp
	shlq	$3, %rbp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, veryfastsupg_double_loadtop.tmptmplen(%rip)
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, veryfastsupg_double_loadtop.ac(%rip)
.LBB21_5:                               # %.preheader226
	testl	%r13d, %r13d
	jle	.LBB21_9
# BB#6:                                 # %.lr.ph254.preheader
	movl	%r13d, %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB21_7:                               # %.lr.ph254
                                        # =>This Inner Loop Header: Depth=1
	movq	veryfastsupg_double_loadtop.tree(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	incq	%rbp
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	sprintf
	cmpq	%rbp, %r14
	jne	.LBB21_7
# BB#8:                                 # %.preheader225
	testl	%r13d, %r13d
	jle	.LBB21_9
# BB#10:                                # %.lr.ph251
	movq	veryfastsupg_double_loadtop.ac(%rip), %rax
	leaq	-1(%r14), %rdx
	movq	%r14, %rsi
	andq	$3, %rsi
	je	.LBB21_11
# BB#12:                                # %.prol.preheader
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB21_13:                              # =>This Inner Loop Header: Depth=1
	leaq	1(%rdi), %rcx
	movl	%ecx, (%rax,%rdi,8)
	leal	-1(%rdi), %ebp
	movl	%ebp, 4(%rax,%rdi,8)
	cmpq	%rcx, %rsi
	movq	%rcx, %rdi
	jne	.LBB21_13
	jmp	.LBB21_14
.LBB21_11:
	xorl	%ecx, %ecx
.LBB21_14:                              # %.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB21_17
# BB#15:                                # %.lr.ph251.new
	movq	%rcx, %r8
	subq	%r14, %r8
	leaq	28(%rax,%rcx,8), %rsi
	movl	$4294967295, %r9d       # imm = 0xFFFFFFFF
	addq	%rcx, %r9
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB21_16:                              # =>This Inner Loop Header: Depth=1
	leal	(%rcx,%rbp), %ebx
	leal	1(%rcx,%rbp), %edx
	movl	%edx, -28(%rsi,%rbp,8)
	leal	(%r9,%rbp), %edi
	movl	%edi, -24(%rsi,%rbp,8)
	leal	2(%rcx,%rbp), %edi
	movl	%edi, -20(%rsi,%rbp,8)
	movl	%ebx, -16(%rsi,%rbp,8)
	leal	3(%rcx,%rbp), %ebx
	movl	%ebx, -12(%rsi,%rbp,8)
	movl	%edx, -8(%rsi,%rbp,8)
	leal	4(%rcx,%rbp), %edx
	movl	%edx, -4(%rsi,%rbp,8)
	movl	%edi, (%rsi,%rbp,8)
	addq	$4, %rbp
	movq	%r8, %rdx
	addq	%rbp, %rdx
	jne	.LBB21_16
.LBB21_17:                              # %._crit_edge252
	movslq	%r13d, %rcx
	testl	%r13d, %r13d
	movl	$-1, -8(%rax,%rcx,8)
	jle	.LBB21_18
# BB#19:                                # %._crit_edge246
	movl	%r13d, 28(%rsp)         # 4-byte Spill
	decq	%rcx
	movq	veryfastsupg_double_loadtop.tmptmplen(%rip), %rdi
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	%ecx, %ebx
	leaq	8(,%rbx,8), %rdx
	xorl	%esi, %esi
	callq	memset
	movq	veryfastsupg_double_loadtop.hist(%rip), %rdi
	leaq	4(,%rbx,4), %rdx
	movl	$255, %esi
	callq	memset
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	cmpl	$1, %r14d
	je	.LBB21_58
# BB#20:                                # %.lr.ph243
	xorl	%r13d, %r13d
	jmp	.LBB21_21
.LBB21_22:                              #   in Loop: Header=BB21_21 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.24, %esi
	xorl	%eax, %eax
	movl	%r13d, %edx
	movl	28(%rsp), %ecx          # 4-byte Reload
	callq	fprintf
	jmp	.LBB21_23
	.p2align	4, 0x90
.LBB21_21:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_32 Depth 2
                                        #     Child Loop BB21_35 Depth 2
                                        #     Child Loop BB21_40 Depth 2
                                        #     Child Loop BB21_43 Depth 2
                                        #     Child Loop BB21_49 Depth 2
	movslq	%r13d, %rax
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$34, %rcx
	addl	%edx, %ecx
	addl	%ecx, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	je	.LBB21_22
.LBB21_23:                              #   in Loop: Header=BB21_21 Depth=1
	movabsq	$-4647714812233515008, %rax # imm = 0xBF800000BF800000
	movq	%rax, 20(%rsp)
	movl	$loadtreeoneline.gett, %edi
	movl	$999, %esi              # imm = 0x3E7
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	fgets
	movl	$loadtreeoneline.gett, %edi
	movl	$.L.str.38, %esi
	xorl	%eax, %eax
	leaq	12(%rsp), %rdx
	leaq	16(%rsp), %rcx
	leaq	20(%rsp), %r8
	leaq	24(%rsp), %r9
	callq	sscanf
	movslq	12(%rsp), %r14
	leal	-1(%r14), %ebx
	movl	%ebx, 12(%rsp)
	movslq	16(%rsp), %r10
	leal	-1(%r10), %r11d
	movl	%r11d, 16(%rsp)
	cmpl	%r10d, %r14d
	jge	.LBB21_24
# BB#25:                                # %loadtreeoneline.exit
                                        #   in Loop: Header=BB21_21 Depth=1
	movss	20(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	.LCPI21_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jne	.LBB21_27
	jp	.LBB21_27
# BB#26:                                # %loadtreeoneline.exit
                                        #   in Loop: Header=BB21_21 Depth=1
	movss	24(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jne	.LBB21_27
	jp	.LBB21_27
# BB#28:                                #   in Loop: Header=BB21_21 Depth=1
	movq	-8(%r12,%r14,8), %rax
	movsd	-8(%rax,%r10,8), %xmm0  # xmm0 = mem[0],zero
	movq	(%r15,%r13,8), %r9
	movq	(%r9), %rbp
	movq	veryfastsupg_double_loadtop.hist(%rip), %r8
	movslq	-4(%r8,%r14,4), %rax
	cmpq	$-1, %rax
	movsd	.LCPI21_1(%rip), %xmm3  # xmm3 = mem[0],zero
	movsd	.LCPI21_3(%rip), %xmm5  # xmm5 = mem[0],zero
	je	.LBB21_29
# BB#30:                                #   in Loop: Header=BB21_21 Depth=1
	movq	(%r15,%rax,8), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movl	(%rcx), %eax
	cmpl	(%rsi), %eax
	movq	%rcx, %rdi
	cmovgq	%rsi, %rdi
	cmovgq	%rcx, %rsi
	movl	(%rdi), %eax
	cmpl	$-1, %eax
	je	.LBB21_33
# BB#31:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB21_21 Depth=1
	addq	$4, %rdi
	.p2align	4, 0x90
.LBB21_32:                              # %.lr.ph
                                        #   Parent Loop BB21_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rbp)
	addq	$4, %rbp
	movl	(%rdi), %eax
	addq	$4, %rdi
	cmpl	$-1, %eax
	jne	.LBB21_32
.LBB21_33:                              # %.preheader222
                                        #   in Loop: Header=BB21_21 Depth=1
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.LBB21_36
# BB#34:                                # %.lr.ph231.preheader
                                        #   in Loop: Header=BB21_21 Depth=1
	addq	$4, %rsi
	.p2align	4, 0x90
.LBB21_35:                              # %.lr.ph231
                                        #   Parent Loop BB21_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rbp)
	addq	$4, %rbp
	movl	(%rsi), %eax
	addq	$4, %rsi
	cmpl	$-1, %eax
	jne	.LBB21_35
	jmp	.LBB21_36
	.p2align	4, 0x90
.LBB21_29:                              #   in Loop: Header=BB21_21 Depth=1
	movl	%ebx, (%rbp)
	addq	$4, %rbp
.LBB21_36:                              # %.loopexit223
                                        #   in Loop: Header=BB21_21 Depth=1
	movl	$-1, (%rbp)
	movq	8(%r9), %rdi
	movslq	-4(%r8,%r10,4), %rax
	cmpq	$-1, %rax
	je	.LBB21_37
# BB#38:                                #   in Loop: Header=BB21_21 Depth=1
	movq	(%r15,%rax,8), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movl	(%rcx), %eax
	cmpl	(%rsi), %eax
	movq	%rcx, %rbp
	cmovgq	%rsi, %rbp
	cmovgq	%rcx, %rsi
	movl	(%rbp), %eax
	cmpl	$-1, %eax
	je	.LBB21_41
# BB#39:                                # %.lr.ph235.preheader
                                        #   in Loop: Header=BB21_21 Depth=1
	addq	$4, %rbp
	.p2align	4, 0x90
.LBB21_40:                              # %.lr.ph235
                                        #   Parent Loop BB21_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rdi)
	addq	$4, %rdi
	movl	(%rbp), %eax
	addq	$4, %rbp
	cmpl	$-1, %eax
	jne	.LBB21_40
.LBB21_41:                              # %.preheader
                                        #   in Loop: Header=BB21_21 Depth=1
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.LBB21_44
# BB#42:                                # %.lr.ph239.preheader
                                        #   in Loop: Header=BB21_21 Depth=1
	addq	$4, %rsi
	.p2align	4, 0x90
.LBB21_43:                              # %.lr.ph239
                                        #   Parent Loop BB21_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rdi)
	addq	$4, %rdi
	movl	(%rsi), %eax
	addq	$4, %rsi
	cmpl	$-1, %eax
	jne	.LBB21_43
	jmp	.LBB21_44
	.p2align	4, 0x90
.LBB21_37:                              #   in Loop: Header=BB21_21 Depth=1
	movl	%r11d, (%rdi)
	addq	$4, %rdi
.LBB21_44:                              # %.loopexit
                                        #   in Loop: Header=BB21_21 Depth=1
	movl	$-1, (%rdi)
	mulsd	%xmm3, %xmm0
	movq	veryfastsupg_double_loadtop.tmptmplen(%rip), %rax
	movapd	%xmm0, %xmm2
	subsd	-8(%rax,%r14,8), %xmm2
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%r13,8), %r9
	movsd	%xmm2, (%r9)
	movapd	%xmm0, %xmm1
	subsd	-8(%rax,%r10,8), %xmm1
	movsd	%xmm1, 8(%r9)
	xorps	%xmm4, %xmm4
	ucomisd	%xmm2, %xmm4
	jbe	.LBB21_46
# BB#45:                                #   in Loop: Header=BB21_21 Depth=1
	movq	$0, (%r9)
.LBB21_46:                              #   in Loop: Header=BB21_21 Depth=1
	xorpd	%xmm2, %xmm2
	ucomisd	%xmm1, %xmm2
	movsd	.LCPI21_2(%rip), %xmm4  # xmm4 = mem[0],zero
	jbe	.LBB21_48
# BB#47:                                #   in Loop: Header=BB21_21 Depth=1
	movq	$0, 8(%r9)
.LBB21_48:                              #   in Loop: Header=BB21_21 Depth=1
	movsd	%xmm0, -8(%rax,%r14,8)
	movl	%r13d, -4(%r8,%r14,4)
	xorl	%edi, %edi
	movq	veryfastsupg_double_loadtop.ac(%rip), %rsi
	.p2align	4, 0x90
.LBB21_49:                              #   Parent Loop BB21_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ebx, %edi
	je	.LBB21_54
# BB#50:                                #   in Loop: Header=BB21_49 Depth=2
	cmpl	%r11d, %edi
	je	.LBB21_54
# BB#51:                                #   in Loop: Header=BB21_49 Depth=2
	cmpl	%ebx, %edi
	movl	%edi, %eax
	movl	%r11d, %edx
	movl	%ebx, %ecx
	movl	%edi, %ebp
	jl	.LBB21_53
# BB#52:                                #   in Loop: Header=BB21_49 Depth=2
	cmpl	%r11d, %edi
	movl	%r11d, %eax
	cmovlel	%edi, %eax
	movl	%edi, %edx
	cmovll	%r11d, %edx
	movl	%edi, %ecx
	movl	%ebx, %ebp
.LBB21_53:                              #   in Loop: Header=BB21_49 Depth=2
	movslq	%ebp, %rbp
	movq	(%r12,%rbp,8), %rbp
	movslq	%ecx, %rcx
	movsd	(%rbp,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	cltq
	movq	(%r12,%rax,8), %rax
	movslq	%edx, %rdx
	movsd	(%rax,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	minsd	%xmm1, %xmm2
	mulsd	%xmm4, %xmm2
	addsd	%xmm1, %xmm0
	mulsd	%xmm3, %xmm0
	mulsd	%xmm5, %xmm0
	addsd	%xmm2, %xmm0
	movsd	%xmm0, (%rbp,%rcx,8)
.LBB21_54:                              #   in Loop: Header=BB21_49 Depth=2
	movslq	%edi, %rax
	movl	(%rsi,%rax,8), %edi
	cmpl	$-1, %edi
	jne	.LBB21_49
# BB#55:                                #   in Loop: Header=BB21_21 Depth=1
	movslq	-4(%rsi,%r10,8), %rax
	movslq	-8(%rsi,%r10,8), %rcx
	cmpq	$-1, %rcx
	movl	%ecx, (%rsi,%rax,8)
	je	.LBB21_57
# BB#56:                                #   in Loop: Header=BB21_21 Depth=1
	movl	%eax, 4(%rsi,%rcx,8)
.LBB21_57:                              #   in Loop: Header=BB21_21 Depth=1
	movq	veryfastsupg_double_loadtop.treetmp(%rip), %rdi
	movq	veryfastsupg_double_loadtop.tree(%rip), %rax
	movq	-8(%rax,%r14,8), %rdx
	movq	-8(%rax,%r10,8), %rcx
	movsd	(%r9), %xmm0            # xmm0 = mem[0],zero
	movsd	8(%r9), %xmm1           # xmm1 = mem[0],zero
	movl	$.L.str.16, %esi
	movb	$2, %al
	callq	sprintf
	movq	veryfastsupg_double_loadtop.tree(%rip), %rax
	movq	-8(%rax,%r14,8), %rdi
	movq	veryfastsupg_double_loadtop.treetmp(%rip), %rsi
	callq	strcpy
	incq	%r13
	cmpq	40(%rsp), %r13          # 8-byte Folded Reload
	jl	.LBB21_21
	jmp	.LBB21_58
.LBB21_9:                               # %._crit_edge252.thread
	movq	veryfastsupg_double_loadtop.ac(%rip), %rax
	movslq	%r13d, %rcx
	movl	$-1, -8(%rax,%rcx,8)
.LBB21_18:                              # %._crit_edge246.thread
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
.LBB21_58:                              # %._crit_edge
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	movl	$.L.str.17, %edi
	movl	$.L.str.18, %esi
	callq	fopen
	movq	%rax, %rbx
	movq	veryfastsupg_double_loadtop.treetmp(%rip), %rdx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	%rbx, %rdi
	callq	fclose
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	veryfastsupg_double_loadtop.tmptmplen(%rip), %rdi
	callq	free
	movq	$0, veryfastsupg_double_loadtop.tmptmplen(%rip)
	movq	veryfastsupg_double_loadtop.hist(%rip), %rdi
	callq	free
	movq	$0, veryfastsupg_double_loadtop.hist(%rip)
	movq	veryfastsupg_double_loadtop.ac(%rip), %rdi
	callq	free
	movq	$0, veryfastsupg_double_loadtop.ac(%rip)
	movq	veryfastsupg_double_loadtop.tree(%rip), %rdi
	callq	FreeCharMtx
	movq	veryfastsupg_double_loadtop.treetmp(%rip), %rdi
	callq	free
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB21_24:
	movq	stderr(%rip), %rcx
	movl	$.L.str.39, %edi
	movl	$21, %esi
	jmp	.LBB21_2
.LBB21_27:
	movq	stderr(%rip), %rcx
	movl	$.L.str.25, %edi
	movl	$34, %esi
.LBB21_2:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB21_1:
	movq	stderr(%rip), %rcx
	movl	$.L.str.10, %edi
	movl	$23, %esi
	jmp	.LBB21_2
.Lfunc_end21:
	.size	veryfastsupg_double_loadtop, .Lfunc_end21-veryfastsupg_double_loadtop
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI22_0:
	.long	3212836864              # float -1
.LCPI22_4:
	.long	0                       # float 0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI22_1:
	.quad	4602678819172646912     # double 0.5
.LCPI22_2:
	.quad	4606281698874543309     # double 0.90000000000000002
.LCPI22_3:
	.quad	4591870180066957722     # double 0.10000000000000001
	.text
	.globl	veryfastsupg_double_loadtree
	.p2align	4, 0x90
	.type	veryfastsupg_double_loadtree,@function
veryfastsupg_double_loadtree:           # @veryfastsupg_double_loadtree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi197:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi198:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi199:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi200:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi201:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi202:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi203:
	.cfi_def_cfa_offset 112
.Lcfi204:
	.cfi_offset %rbx, -56
.Lcfi205:
	.cfi_offset %r12, -48
.Lcfi206:
	.cfi_offset %r13, -40
.Lcfi207:
	.cfi_offset %r14, -32
.Lcfi208:
	.cfi_offset %r15, -24
.Lcfi209:
	.cfi_offset %rbp, -16
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rdx, %r15
	movq	%rsi, %r12
	movl	%edi, %r13d
	movl	$.L.str.8, %edi
	movl	$.L.str.9, %esi
	callq	fopen
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB22_1
# BB#3:
	cmpq	$0, veryfastsupg_double_loadtree.hist(%rip)
	jne	.LBB22_5
# BB#4:
	imull	$50, njob(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, veryfastsupg_double_loadtree.treetmp(%rip)
	movl	njob(%rip), %edi
	imull	$50, %edi, %esi
	callq	AllocateCharMtx
	movq	%rax, veryfastsupg_double_loadtree.tree(%rip)
	movl	njob(%rip), %edi
	callq	AllocateIntVec
	movq	%rax, veryfastsupg_double_loadtree.hist(%rip)
	movslq	njob(%rip), %rbp
	shlq	$3, %rbp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, veryfastsupg_double_loadtree.tmptmplen(%rip)
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, veryfastsupg_double_loadtree.ac(%rip)
.LBB22_5:                               # %.preheader214
	testl	%r13d, %r13d
	jle	.LBB22_9
# BB#6:                                 # %.lr.ph242.preheader
	movl	%r13d, %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB22_7:                               # %.lr.ph242
                                        # =>This Inner Loop Header: Depth=1
	movq	veryfastsupg_double_loadtree.tree(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	incq	%rbp
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	sprintf
	cmpq	%rbp, %r14
	jne	.LBB22_7
# BB#8:                                 # %.preheader213
	testl	%r13d, %r13d
	jle	.LBB22_9
# BB#10:                                # %.lr.ph239
	movq	veryfastsupg_double_loadtree.ac(%rip), %rax
	leaq	-1(%r14), %rdx
	movq	%r14, %rsi
	andq	$3, %rsi
	je	.LBB22_11
# BB#12:                                # %.prol.preheader
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB22_13:                              # =>This Inner Loop Header: Depth=1
	leaq	1(%rdi), %rcx
	movl	%ecx, (%rax,%rdi,8)
	leal	-1(%rdi), %ebp
	movl	%ebp, 4(%rax,%rdi,8)
	cmpq	%rcx, %rsi
	movq	%rcx, %rdi
	jne	.LBB22_13
	jmp	.LBB22_14
.LBB22_11:
	xorl	%ecx, %ecx
.LBB22_14:                              # %.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB22_17
# BB#15:                                # %.lr.ph239.new
	movq	%rcx, %r8
	subq	%r14, %r8
	leaq	28(%rax,%rcx,8), %rsi
	movl	$4294967295, %r9d       # imm = 0xFFFFFFFF
	addq	%rcx, %r9
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB22_16:                              # =>This Inner Loop Header: Depth=1
	leal	(%rcx,%rbp), %ebx
	leal	1(%rcx,%rbp), %edx
	movl	%edx, -28(%rsi,%rbp,8)
	leal	(%r9,%rbp), %edi
	movl	%edi, -24(%rsi,%rbp,8)
	leal	2(%rcx,%rbp), %edi
	movl	%edi, -20(%rsi,%rbp,8)
	movl	%ebx, -16(%rsi,%rbp,8)
	leal	3(%rcx,%rbp), %ebx
	movl	%ebx, -12(%rsi,%rbp,8)
	movl	%edx, -8(%rsi,%rbp,8)
	leal	4(%rcx,%rbp), %edx
	movl	%edx, -4(%rsi,%rbp,8)
	movl	%edi, (%rsi,%rbp,8)
	addq	$4, %rbp
	movq	%r8, %rdx
	addq	%rbp, %rdx
	jne	.LBB22_16
.LBB22_17:                              # %._crit_edge240
	movslq	%r13d, %rcx
	testl	%r13d, %r13d
	movl	$-1, -8(%rax,%rcx,8)
	jle	.LBB22_18
# BB#19:                                # %._crit_edge234
	movl	%r13d, 28(%rsp)         # 4-byte Spill
	decq	%rcx
	movq	veryfastsupg_double_loadtree.tmptmplen(%rip), %rdi
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	%ecx, %ebx
	leaq	8(,%rbx,8), %rdx
	xorl	%esi, %esi
	callq	memset
	movq	veryfastsupg_double_loadtree.hist(%rip), %rdi
	leaq	4(,%rbx,4), %rdx
	movl	$255, %esi
	callq	memset
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	cmpl	$1, %r14d
	je	.LBB22_58
# BB#20:                                # %.lr.ph231
	xorl	%r13d, %r13d
	jmp	.LBB22_21
.LBB22_22:                              #   in Loop: Header=BB22_21 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.24, %esi
	xorl	%eax, %eax
	movl	%r13d, %edx
	movl	28(%rsp), %ecx          # 4-byte Reload
	callq	fprintf
	jmp	.LBB22_23
	.p2align	4, 0x90
.LBB22_21:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_36 Depth 2
                                        #     Child Loop BB22_39 Depth 2
                                        #     Child Loop BB22_44 Depth 2
                                        #     Child Loop BB22_47 Depth 2
                                        #     Child Loop BB22_49 Depth 2
	movslq	%r13d, %rax
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$34, %rcx
	addl	%edx, %ecx
	addl	%ecx, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	je	.LBB22_22
.LBB22_23:                              #   in Loop: Header=BB22_21 Depth=1
	movabsq	$-4647714812233515008, %rax # imm = 0xBF800000BF800000
	movq	%rax, 12(%rsp)
	movl	$loadtreeoneline.gett, %edi
	movl	$999, %esi              # imm = 0x3E7
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	fgets
	movl	$loadtreeoneline.gett, %edi
	movl	$.L.str.38, %esi
	xorl	%eax, %eax
	leaq	20(%rsp), %rdx
	leaq	24(%rsp), %rcx
	leaq	12(%rsp), %r8
	leaq	16(%rsp), %r9
	callq	sscanf
	movslq	20(%rsp), %r14
	leal	-1(%r14), %ebx
	movl	%ebx, 20(%rsp)
	movslq	24(%rsp), %r10
	leal	-1(%r10), %r11d
	movl	%r11d, 24(%rsp)
	cmpl	%r10d, %r14d
	jge	.LBB22_24
# BB#25:                                # %loadtreeoneline.exit
                                        #   in Loop: Header=BB22_21 Depth=1
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	.LCPI22_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jne	.LBB22_26
	jnp	.LBB22_27
.LBB22_26:                              # %loadtreeoneline.exit
                                        #   in Loop: Header=BB22_21 Depth=1
	movss	16(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jne	.LBB22_28
	jnp	.LBB22_27
.LBB22_28:                              #   in Loop: Header=BB22_21 Depth=1
	movq	-8(%r12,%r14,8), %rax
	movsd	-8(%rax,%r10,8), %xmm1  # xmm1 = mem[0],zero
	xorps	%xmm3, %xmm3
	ucomiss	%xmm0, %xmm3
	movsd	.LCPI22_2(%rip), %xmm4  # xmm4 = mem[0],zero
	movsd	.LCPI22_3(%rip), %xmm5  # xmm5 = mem[0],zero
	jbe	.LBB22_30
# BB#29:                                #   in Loop: Header=BB22_21 Depth=1
	movl	$0, 12(%rsp)
	xorps	%xmm0, %xmm0
.LBB22_30:                              #   in Loop: Header=BB22_21 Depth=1
	xorps	%xmm3, %xmm3
	ucomiss	%xmm2, %xmm3
	jbe	.LBB22_32
# BB#31:                                #   in Loop: Header=BB22_21 Depth=1
	movl	$0, 16(%rsp)
.LBB22_32:                              #   in Loop: Header=BB22_21 Depth=1
	movq	(%r15,%r13,8), %r9
	movq	(%r9), %rbp
	movq	veryfastsupg_double_loadtree.hist(%rip), %r8
	movslq	-4(%r8,%r14,4), %rax
	cmpq	$-1, %rax
	movsd	.LCPI22_1(%rip), %xmm3  # xmm3 = mem[0],zero
	je	.LBB22_33
# BB#34:                                #   in Loop: Header=BB22_21 Depth=1
	movq	(%r15,%rax,8), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movl	(%rcx), %eax
	cmpl	(%rsi), %eax
	movq	%rcx, %rdi
	cmovgq	%rsi, %rdi
	cmovgq	%rcx, %rsi
	movl	(%rdi), %eax
	cmpl	$-1, %eax
	je	.LBB22_37
# BB#35:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB22_21 Depth=1
	addq	$4, %rdi
	.p2align	4, 0x90
.LBB22_36:                              # %.lr.ph
                                        #   Parent Loop BB22_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rbp)
	addq	$4, %rbp
	movl	(%rdi), %eax
	addq	$4, %rdi
	cmpl	$-1, %eax
	jne	.LBB22_36
.LBB22_37:                              # %.preheader210
                                        #   in Loop: Header=BB22_21 Depth=1
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.LBB22_40
# BB#38:                                # %.lr.ph219.preheader
                                        #   in Loop: Header=BB22_21 Depth=1
	addq	$4, %rsi
	.p2align	4, 0x90
.LBB22_39:                              # %.lr.ph219
                                        #   Parent Loop BB22_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rbp)
	addq	$4, %rbp
	movl	(%rsi), %eax
	addq	$4, %rsi
	cmpl	$-1, %eax
	jne	.LBB22_39
	jmp	.LBB22_40
	.p2align	4, 0x90
.LBB22_33:                              #   in Loop: Header=BB22_21 Depth=1
	movl	%ebx, (%rbp)
	addq	$4, %rbp
.LBB22_40:                              # %.loopexit211
                                        #   in Loop: Header=BB22_21 Depth=1
	movl	$-1, (%rbp)
	movq	8(%r9), %rdi
	movslq	-4(%r8,%r10,4), %rax
	cmpq	$-1, %rax
	je	.LBB22_41
# BB#42:                                #   in Loop: Header=BB22_21 Depth=1
	movq	(%r15,%rax,8), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movl	(%rcx), %eax
	cmpl	(%rsi), %eax
	movq	%rcx, %rbp
	cmovgq	%rsi, %rbp
	cmovgq	%rcx, %rsi
	movl	(%rbp), %eax
	cmpl	$-1, %eax
	je	.LBB22_45
# BB#43:                                # %.lr.ph223.preheader
                                        #   in Loop: Header=BB22_21 Depth=1
	addq	$4, %rbp
	.p2align	4, 0x90
.LBB22_44:                              # %.lr.ph223
                                        #   Parent Loop BB22_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rdi)
	addq	$4, %rdi
	movl	(%rbp), %eax
	addq	$4, %rbp
	cmpl	$-1, %eax
	jne	.LBB22_44
.LBB22_45:                              # %.preheader
                                        #   in Loop: Header=BB22_21 Depth=1
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.LBB22_48
# BB#46:                                # %.lr.ph227.preheader
                                        #   in Loop: Header=BB22_21 Depth=1
	addq	$4, %rsi
	.p2align	4, 0x90
.LBB22_47:                              # %.lr.ph227
                                        #   Parent Loop BB22_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rdi)
	addq	$4, %rdi
	movl	(%rsi), %eax
	addq	$4, %rsi
	cmpl	$-1, %eax
	jne	.LBB22_47
	jmp	.LBB22_48
	.p2align	4, 0x90
.LBB22_41:                              #   in Loop: Header=BB22_21 Depth=1
	movl	%r11d, (%rdi)
	addq	$4, %rdi
.LBB22_48:                              # %.loopexit
                                        #   in Loop: Header=BB22_21 Depth=1
	movl	$-1, (%rdi)
	mulsd	%xmm3, %xmm1
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r13,8), %r9
	movss	16(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	cvtps2pd	%xmm0, %xmm0
	movupd	%xmm0, (%r9)
	movq	veryfastsupg_double_loadtree.tmptmplen(%rip), %rax
	movsd	%xmm1, -8(%rax,%r14,8)
	movl	%r13d, -4(%r8,%r14,4)
	xorl	%edi, %edi
	movq	veryfastsupg_double_loadtree.ac(%rip), %rsi
	.p2align	4, 0x90
.LBB22_49:                              #   Parent Loop BB22_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ebx, %edi
	je	.LBB22_54
# BB#50:                                #   in Loop: Header=BB22_49 Depth=2
	cmpl	%r11d, %edi
	je	.LBB22_54
# BB#51:                                #   in Loop: Header=BB22_49 Depth=2
	cmpl	%ebx, %edi
	movl	%edi, %eax
	movl	%r11d, %edx
	movl	%ebx, %ecx
	movl	%edi, %ebp
	jl	.LBB22_53
# BB#52:                                #   in Loop: Header=BB22_49 Depth=2
	cmpl	%r11d, %edi
	movl	%r11d, %eax
	cmovlel	%edi, %eax
	movl	%edi, %edx
	cmovll	%r11d, %edx
	movl	%edi, %ecx
	movl	%ebx, %ebp
.LBB22_53:                              #   in Loop: Header=BB22_49 Depth=2
	movslq	%ebp, %rbp
	movq	(%r12,%rbp,8), %rbp
	movslq	%ecx, %rcx
	movsd	(%rbp,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	cltq
	movq	(%r12,%rax,8), %rax
	movslq	%edx, %rdx
	movsd	(%rax,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	minsd	%xmm1, %xmm2
	mulsd	%xmm4, %xmm2
	addsd	%xmm1, %xmm0
	mulsd	%xmm3, %xmm0
	mulsd	%xmm5, %xmm0
	addsd	%xmm2, %xmm0
	movsd	%xmm0, (%rbp,%rcx,8)
.LBB22_54:                              #   in Loop: Header=BB22_49 Depth=2
	movslq	%edi, %rax
	movl	(%rsi,%rax,8), %edi
	cmpl	$-1, %edi
	jne	.LBB22_49
# BB#55:                                #   in Loop: Header=BB22_21 Depth=1
	movslq	-4(%rsi,%r10,8), %rax
	movslq	-8(%rsi,%r10,8), %rcx
	cmpq	$-1, %rcx
	movl	%ecx, (%rsi,%rax,8)
	je	.LBB22_57
# BB#56:                                #   in Loop: Header=BB22_21 Depth=1
	movl	%eax, 4(%rsi,%rcx,8)
.LBB22_57:                              #   in Loop: Header=BB22_21 Depth=1
	movq	veryfastsupg_double_loadtree.treetmp(%rip), %rdi
	movq	veryfastsupg_double_loadtree.tree(%rip), %rax
	movq	-8(%rax,%r14,8), %rdx
	movq	-8(%rax,%r10,8), %rcx
	movsd	(%r9), %xmm0            # xmm0 = mem[0],zero
	movsd	8(%r9), %xmm1           # xmm1 = mem[0],zero
	movl	$.L.str.16, %esi
	movb	$2, %al
	callq	sprintf
	movq	veryfastsupg_double_loadtree.tree(%rip), %rax
	movq	-8(%rax,%r14,8), %rdi
	movq	veryfastsupg_double_loadtree.treetmp(%rip), %rsi
	callq	strcpy
	incq	%r13
	cmpq	40(%rsp), %r13          # 8-byte Folded Reload
	jl	.LBB22_21
	jmp	.LBB22_58
.LBB22_9:                               # %._crit_edge240.thread
	movq	veryfastsupg_double_loadtree.ac(%rip), %rax
	movslq	%r13d, %rcx
	movl	$-1, -8(%rax,%rcx,8)
.LBB22_18:                              # %._crit_edge234.thread
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
.LBB22_58:                              # %._crit_edge
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	movl	$.L.str.17, %edi
	movl	$.L.str.18, %esi
	callq	fopen
	movq	%rax, %rbx
	movq	veryfastsupg_double_loadtree.treetmp(%rip), %rdx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	%rbx, %rdi
	callq	fclose
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	veryfastsupg_double_loadtree.tmptmplen(%rip), %rdi
	callq	free
	movq	$0, veryfastsupg_double_loadtree.tmptmplen(%rip)
	movq	veryfastsupg_double_loadtree.hist(%rip), %rdi
	callq	free
	movq	$0, veryfastsupg_double_loadtree.hist(%rip)
	movq	veryfastsupg_double_loadtree.ac(%rip), %rdi
	callq	free
	movq	$0, veryfastsupg_double_loadtree.ac(%rip)
	movq	veryfastsupg_double_loadtree.tree(%rip), %rdi
	callq	FreeCharMtx
	movq	veryfastsupg_double_loadtree.treetmp(%rip), %rdi
	callq	free
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB22_24:
	movq	stderr(%rip), %rcx
	movl	$.L.str.39, %edi
	movl	$21, %esi
	jmp	.LBB22_2
.LBB22_27:
	movq	stderr(%rip), %rcx
	movl	$.L.str.26, %edi
	movl	$39, %esi
.LBB22_2:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB22_1:
	movq	stderr(%rip), %rcx
	movl	$.L.str.10, %edi
	movl	$23, %esi
	jmp	.LBB22_2
.Lfunc_end22:
	.size	veryfastsupg_double_loadtree, .Lfunc_end22-veryfastsupg_double_loadtree
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI23_0:
	.quad	4681608354012227174     # double 99999.899999999994
.LCPI23_1:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	veryfastsupg_double_outtree
	.p2align	4, 0x90
	.type	veryfastsupg_double_outtree,@function
veryfastsupg_double_outtree:            # @veryfastsupg_double_outtree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi210:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi211:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi212:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi213:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi214:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi215:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi216:
	.cfi_def_cfa_offset 128
.Lcfi217:
	.cfi_offset %rbx, -56
.Lcfi218:
	.cfi_offset %r12, -48
.Lcfi219:
	.cfi_offset %r13, -40
.Lcfi220:
	.cfi_offset %r14, -32
.Lcfi221:
	.cfi_offset %r15, -24
.Lcfi222:
	.cfi_offset %rbp, -16
	movq	%r8, %r13
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rdx, %rbp
	movq	%rsi, %r14
	movl	%edi, %ebx
	movabsq	$4606281698874543309, %rax # imm = 0x3FECCCCCCCCCCCCD
	movq	%rax, sueff1_double(%rip)
	movabsq	$4587366580439587226, %rax # imm = 0x3FA999999999999A
	movq	%rax, sueff05_double(%rip)
	movl	treemethod(%rip), %edx
	cmpl	$69, %edx
	je	.LBB23_5
# BB#1:
	cmpl	$88, %edx
	je	.LBB23_2
# BB#3:
	cmpl	$113, %edx
	jne	.LBB23_4
# BB#6:                                 # %.fold.split250
	movl	$cluster_minimum_double, %eax
	jmp	.LBB23_7
.LBB23_5:                               # %.fold.split
	movl	$cluster_average_double, %eax
	jmp	.LBB23_7
.LBB23_2:
	movl	$cluster_mix_double, %eax
.LBB23_7:
	movq	%rax, 32(%rsp)          # 8-byte Spill
	cmpq	$0, veryfastsupg_double_outtree.hist(%rip)
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	jne	.LBB23_9
# BB#8:
	imull	$50, njob(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, veryfastsupg_double_outtree.treetmp(%rip)
	movl	njob(%rip), %edi
	imull	$50, %edi, %esi
	callq	AllocateCharMtx
	movq	%rax, veryfastsupg_double_outtree.tree(%rip)
	movl	njob(%rip), %edi
	callq	AllocateIntVec
	movq	%rax, veryfastsupg_double_outtree.hist(%rip)
	movslq	njob(%rip), %rbx
	shlq	$3, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, veryfastsupg_double_outtree.tmptmplen(%rip)
	movq	%rbx, %rdi
	movq	8(%rsp), %rbx           # 8-byte Reload
	callq	malloc
	movq	%rax, veryfastsupg_double_outtree.ac(%rip)
	movl	$30, %edi
	callq	AllocateCharVec
	movq	%rax, veryfastsupg_double_outtree.nametmp(%rip)
.LBB23_9:                               # %.preheader263
	testl	%ebx, %ebx
	jle	.LBB23_19
# BB#10:                                # %.preheader262.preheader
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	callq	__ctype_b_loc
	movq	%rax, %rbp
	movl	%ebx, %r15d
	incq	%r13
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB23_11:                              # %.preheader262
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_12 Depth 2
	movq	veryfastsupg_double_outtree.nametmp(%rip), %rcx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 14(%rcx)
	movups	%xmm0, (%rcx)
	movl	$1, %eax
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB23_12:                              #   Parent Loop BB23_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rdi
	movsbq	-1(%rdx), %rsi
	movzwl	(%rdi,%rsi,2), %edi
	andl	$8, %edi
	testw	%di, %di
	movb	$95, %dil
	movb	$95, %bl
	je	.LBB23_14
# BB#13:                                #   in Loop: Header=BB23_12 Depth=2
	movl	%esi, %ebx
.LBB23_14:                              #   in Loop: Header=BB23_12 Depth=2
	movb	%bl, -1(%rcx,%rax)
	movq	(%rbp), %rbx
	movsbq	(%rdx), %rsi
	movzwl	(%rbx,%rsi,2), %ebx
	andl	$8, %ebx
	testw	%bx, %bx
	je	.LBB23_16
# BB#15:                                #   in Loop: Header=BB23_12 Depth=2
	movl	%esi, %edi
.LBB23_16:                              #   in Loop: Header=BB23_12 Depth=2
	movb	%dil, (%rcx,%rax)
	addq	$2, %rdx
	addq	$2, %rax
	cmpq	$31, %rax
	jne	.LBB23_12
# BB#17:                                #   in Loop: Header=BB23_11 Depth=1
	movb	$0, 30(%rcx)
	movq	veryfastsupg_double_outtree.tree(%rip), %rax
	movq	(%rax,%r12,8), %rdi
	incq	%r12
	incq	%rcx
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movl	%r12d, %edx
	callq	sprintf
	addq	$256, %r13              # imm = 0x100
	cmpq	%r15, %r12
	jne	.LBB23_11
# BB#18:                                # %.preheader260
	movq	8(%rsp), %rbx           # 8-byte Reload
	testl	%ebx, %ebx
	movq	40(%rsp), %r12          # 8-byte Reload
	jle	.LBB23_19
# BB#20:                                # %.lr.ph310
	movq	veryfastsupg_double_outtree.ac(%rip), %rax
	leaq	-1(%r15), %rdx
	movq	%r15, %rsi
	andq	$3, %rsi
	je	.LBB23_21
# BB#22:                                # %.prol.preheader
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB23_23:                              # =>This Inner Loop Header: Depth=1
	leaq	1(%rdi), %rcx
	movl	%ecx, (%rax,%rdi,8)
	leal	-1(%rdi), %ebp
	movl	%ebp, 4(%rax,%rdi,8)
	cmpq	%rcx, %rsi
	movq	%rcx, %rdi
	jne	.LBB23_23
	jmp	.LBB23_24
.LBB23_21:
	xorl	%ecx, %ecx
.LBB23_24:                              # %.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB23_27
# BB#25:                                # %.lr.ph310.new
	movq	%rcx, %r8
	subq	%r15, %r8
	leaq	28(%rax,%rcx,8), %rsi
	movl	$4294967295, %r9d       # imm = 0xFFFFFFFF
	addq	%rcx, %r9
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB23_26:                              # =>This Inner Loop Header: Depth=1
	leal	(%rcx,%rbp), %ebx
	leal	1(%rcx,%rbp), %edx
	movl	%edx, -28(%rsi,%rbp,8)
	leal	(%r9,%rbp), %edi
	movl	%edi, -24(%rsi,%rbp,8)
	leal	2(%rcx,%rbp), %edi
	movl	%edi, -20(%rsi,%rbp,8)
	movl	%ebx, -16(%rsi,%rbp,8)
	leal	3(%rcx,%rbp), %ebx
	movl	%ebx, -12(%rsi,%rbp,8)
	movl	%edx, -8(%rsi,%rbp,8)
	leal	4(%rcx,%rbp), %edx
	movl	%edx, -4(%rsi,%rbp,8)
	movl	%edi, (%rsi,%rbp,8)
	addq	$4, %rbp
	movq	%r8, %rdx
	addq	%rbp, %rdx
	jne	.LBB23_26
.LBB23_27:                              # %._crit_edge311
	movq	8(%rsp), %rbx           # 8-byte Reload
	testl	%ebx, %ebx
	movslq	%ebx, %rcx
	movl	$-1, -8(%rax,%rcx,8)
	jle	.LBB23_28
# BB#29:                                # %._crit_edge305
	leal	-1(%rbx), %eax
	movq	veryfastsupg_double_outtree.tmptmplen(%rip), %rdi
	movl	%eax, %ebp
	leaq	8(,%rbp,8), %rdx
	xorl	%esi, %esi
	callq	memset
	movq	veryfastsupg_double_outtree.hist(%rip), %rdi
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	leaq	4(,%rbp,4), %rdx
	movl	$255, %esi
	callq	memset
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	cmpl	$1, %ebx
	je	.LBB23_65
# BB#30:                                # %.lr.ph301.preheader
	movl	$-1, %ebp
	xorl	%eax, %eax
	movl	$-1, %ebx
	jmp	.LBB23_31
.LBB23_32:                              #   in Loop: Header=BB23_31 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.24, %esi
	xorl	%eax, %eax
	movq	16(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	8(%rsp), %rcx           # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	jmp	.LBB23_33
	.p2align	4, 0x90
.LBB23_31:                              # %.lr.ph301
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_36 Depth 2
                                        #       Child Loop BB23_37 Depth 3
                                        #     Child Loop BB23_43 Depth 2
                                        #     Child Loop BB23_46 Depth 2
                                        #     Child Loop BB23_51 Depth 2
                                        #     Child Loop BB23_54 Depth 2
                                        #     Child Loop BB23_56 Depth 2
	movq	%rax, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	cltq
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$34, %rcx
	addl	%edx, %ecx
	addl	%ecx, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	je	.LBB23_32
.LBB23_33:                              # %.preheader258
                                        #   in Loop: Header=BB23_31 Depth=1
	movq	veryfastsupg_double_outtree.ac(%rip), %rax
	movl	(%rax), %esi
	cmpl	$-1, %esi
	je	.LBB23_34
# BB#35:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB23_31 Depth=1
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	movsd	.LCPI23_0(%rip), %xmm0  # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB23_36:                              # %.lr.ph
                                        #   Parent Loop BB23_31 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB23_37 Depth 3
	movl	%esi, %edx
	movq	(%r14,%rdi,8), %rsi
	movl	%edx, %edi
	.p2align	4, 0x90
.LBB23_37:                              #   Parent Loop BB23_31 Depth=1
                                        #     Parent Loop BB23_36 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movapd	%xmm0, %xmm1
	movslq	%edi, %rdi
	movsd	(%rsi,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	minsd	%xmm1, %xmm0
	cmoval	%ecx, %ebx
	cmoval	%edi, %ebp
	movl	(%rax,%rdi,8), %edi
	cmpl	$-1, %edi
	jne	.LBB23_37
# BB#38:                                # %._crit_edge
                                        #   in Loop: Header=BB23_36 Depth=2
	movslq	%edx, %rdi
	movl	(%rax,%rdi,8), %esi
	cmpl	$-1, %esi
	movl	%edx, %ecx
	jne	.LBB23_36
	jmp	.LBB23_39
	.p2align	4, 0x90
.LBB23_34:                              #   in Loop: Header=BB23_31 Depth=1
	movsd	.LCPI23_0(%rip), %xmm0  # xmm0 = mem[0],zero
.LBB23_39:                              # %._crit_edge274
                                        #   in Loop: Header=BB23_31 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%r12,%rax,8), %r9
	movq	(%r9), %rdx
	movq	veryfastsupg_double_outtree.hist(%rip), %r8
	movslq	%ebx, %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movslq	(%r8,%rax,4), %rax
	cmpq	$-1, %rax
	je	.LBB23_40
# BB#41:                                #   in Loop: Header=BB23_31 Depth=1
	movq	(%r12,%rax,8), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movl	(%rcx), %eax
	cmpl	(%rsi), %eax
	movq	%rcx, %rdi
	cmovgq	%rsi, %rdi
	cmovgq	%rcx, %rsi
	movl	(%rdi), %eax
	cmpl	$-1, %eax
	je	.LBB23_44
# BB#42:                                # %.lr.ph281.preheader
                                        #   in Loop: Header=BB23_31 Depth=1
	addq	$4, %rdi
	.p2align	4, 0x90
.LBB23_43:                              # %.lr.ph281
                                        #   Parent Loop BB23_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rdx)
	addq	$4, %rdx
	movl	(%rdi), %eax
	addq	$4, %rdi
	cmpl	$-1, %eax
	jne	.LBB23_43
.LBB23_44:                              # %.preheader256
                                        #   in Loop: Header=BB23_31 Depth=1
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.LBB23_47
# BB#45:                                # %.lr.ph285.preheader
                                        #   in Loop: Header=BB23_31 Depth=1
	addq	$4, %rsi
	.p2align	4, 0x90
.LBB23_46:                              # %.lr.ph285
                                        #   Parent Loop BB23_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rdx)
	addq	$4, %rdx
	movl	(%rsi), %eax
	addq	$4, %rsi
	cmpl	$-1, %eax
	jne	.LBB23_46
	jmp	.LBB23_47
	.p2align	4, 0x90
.LBB23_40:                              #   in Loop: Header=BB23_31 Depth=1
	movl	%ebx, (%rdx)
	addq	$4, %rdx
.LBB23_47:                              # %.loopexit257
                                        #   in Loop: Header=BB23_31 Depth=1
	movl	$-1, (%rdx)
	movq	8(%r9), %rcx
	movslq	%ebp, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movslq	(%r8,%rax,4), %rax
	cmpq	$-1, %rax
	je	.LBB23_48
# BB#49:                                #   in Loop: Header=BB23_31 Depth=1
	movq	(%r12,%rax,8), %rax
	movq	(%rax), %rdi
	movq	8(%rax), %rdx
	movl	(%rdi), %eax
	cmpl	(%rdx), %eax
	movq	%rdi, %rsi
	cmovgq	%rdx, %rsi
	cmovgq	%rdi, %rdx
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.LBB23_52
# BB#50:                                # %.lr.ph290.preheader
                                        #   in Loop: Header=BB23_31 Depth=1
	addq	$4, %rsi
	.p2align	4, 0x90
.LBB23_51:                              # %.lr.ph290
                                        #   Parent Loop BB23_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rcx)
	addq	$4, %rcx
	movl	(%rsi), %eax
	addq	$4, %rsi
	cmpl	$-1, %eax
	jne	.LBB23_51
.LBB23_52:                              # %.preheader255
                                        #   in Loop: Header=BB23_31 Depth=1
	movl	(%rdx), %eax
	cmpl	$-1, %eax
	je	.LBB23_55
# BB#53:                                # %.lr.ph294.preheader
                                        #   in Loop: Header=BB23_31 Depth=1
	addq	$4, %rdx
	.p2align	4, 0x90
.LBB23_54:                              # %.lr.ph294
                                        #   Parent Loop BB23_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rcx)
	addq	$4, %rcx
	movl	(%rdx), %eax
	addq	$4, %rdx
	cmpl	$-1, %eax
	jne	.LBB23_54
	jmp	.LBB23_55
	.p2align	4, 0x90
.LBB23_48:                              #   in Loop: Header=BB23_31 Depth=1
	movl	%ebp, (%rcx)
	addq	$4, %rcx
.LBB23_55:                              # %.loopexit
                                        #   in Loop: Header=BB23_31 Depth=1
	movl	$-1, (%rcx)
	mulsd	.LCPI23_1(%rip), %xmm0
	movq	veryfastsupg_double_outtree.tmptmplen(%rip), %rax
	movapd	%xmm0, %xmm1
	movq	56(%rsp), %rsi          # 8-byte Reload
	subsd	(%rax,%rsi,8), %xmm1
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	(%rcx,%rdx,8), %rcx
	movsd	%xmm1, (%rcx)
	movapd	%xmm0, %xmm1
	movq	24(%rsp), %rdi          # 8-byte Reload
	subsd	(%rax,%rdi,8), %xmm1
	movsd	%xmm1, 8(%rcx)
	movsd	%xmm0, (%rax,%rsi,8)
	movl	%edx, (%r8,%rsi,4)
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB23_56:                              #   Parent Loop BB23_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ebx, %r12d
	je	.LBB23_61
# BB#57:                                #   in Loop: Header=BB23_56 Depth=2
	cmpl	%ebp, %r12d
	je	.LBB23_61
# BB#58:                                #   in Loop: Header=BB23_56 Depth=2
	cmpl	%ebx, %r12d
	movl	%ebp, %eax
	movl	%r12d, %ecx
	movl	%ebx, %edx
	movl	%r12d, %esi
	jl	.LBB23_60
# BB#59:                                #   in Loop: Header=BB23_56 Depth=2
	cmpl	%ebp, %r12d
	movl	%r12d, %eax
	cmovll	%ebp, %eax
	movl	%ebp, %ecx
	cmovlel	%r12d, %ecx
	movl	%r12d, %edx
	movl	%ebx, %esi
.LBB23_60:                              #   in Loop: Header=BB23_56 Depth=2
	movslq	%esi, %r13
	movq	(%r14,%r13,8), %rsi
	movslq	%edx, %r15
	movsd	(%rsi,%r15,8), %xmm0    # xmm0 = mem[0],zero
	movslq	%ecx, %rcx
	movq	(%r14,%rcx,8), %rcx
	cltq
	movsd	(%rcx,%rax,8), %xmm1    # xmm1 = mem[0],zero
	callq	*32(%rsp)               # 8-byte Folded Reload
	movq	(%r14,%r13,8), %rax
	movsd	%xmm0, (%rax,%r15,8)
.LBB23_61:                              #   in Loop: Header=BB23_56 Depth=2
	movq	veryfastsupg_double_outtree.ac(%rip), %rax
	movslq	%r12d, %rcx
	movl	(%rax,%rcx,8), %r12d
	cmpl	$-1, %r12d
	jne	.LBB23_56
# BB#62:                                #   in Loop: Header=BB23_31 Depth=1
	movq	24(%rsp), %rdx          # 8-byte Reload
	movslq	4(%rax,%rdx,8), %rcx
	movslq	(%rax,%rdx,8), %rdx
	cmpq	$-1, %rdx
	movl	%edx, (%rax,%rcx,8)
	je	.LBB23_64
# BB#63:                                #   in Loop: Header=BB23_31 Depth=1
	movl	%ecx, 4(%rax,%rdx,8)
.LBB23_64:                              #   in Loop: Header=BB23_31 Depth=1
	movq	veryfastsupg_double_outtree.treetmp(%rip), %rdi
	movq	veryfastsupg_double_outtree.tree(%rip), %rax
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	(%rax,%r12,8), %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	(%rcx,%r15,8), %rsi
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rcx
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rsi), %xmm1          # xmm1 = mem[0],zero
	movl	$.L.str.16, %esi
	movb	$2, %al
	callq	sprintf
	movq	veryfastsupg_double_outtree.tree(%rip), %rax
	movq	(%rax,%r12,8), %rdi
	movq	veryfastsupg_double_outtree.treetmp(%rip), %rsi
	callq	strcpy
	movq	%r15, %rax
	incq	%rax
	cmpq	64(%rsp), %rax          # 8-byte Folded Reload
	movq	40(%rsp), %r12          # 8-byte Reload
	jne	.LBB23_31
	jmp	.LBB23_65
.LBB23_19:                              # %._crit_edge311.thread
	movq	veryfastsupg_double_outtree.ac(%rip), %rax
	movslq	%ebx, %rcx
	movl	$-1, -8(%rax,%rcx,8)
.LBB23_28:                              # %._crit_edge305.thread
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
.LBB23_65:                              # %._crit_edge302
	movl	$.L.str.17, %edi
	movl	$.L.str.18, %esi
	callq	fopen
	movq	%rax, %rbx
	movq	veryfastsupg_double_outtree.treetmp(%rip), %rdx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	%rbx, %rdi
	callq	fclose
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	veryfastsupg_double_outtree.tmptmplen(%rip), %rdi
	callq	free
	movq	$0, veryfastsupg_double_outtree.tmptmplen(%rip)
	movq	veryfastsupg_double_outtree.hist(%rip), %rdi
	callq	free
	movq	$0, veryfastsupg_double_outtree.hist(%rip)
	movq	veryfastsupg_double_outtree.ac(%rip), %rdi
	callq	free
	movq	$0, veryfastsupg_double_outtree.ac(%rip)
	movq	veryfastsupg_double_outtree.tree(%rip), %rdi
	callq	FreeCharMtx
	movq	veryfastsupg_double_outtree.treetmp(%rip), %rdi
	callq	free
	movq	veryfastsupg_double_outtree.nametmp(%rip), %rdi
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB23_4:
	movq	stderr(%rip), %rdi
	movl	$.L.str.20, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end23:
	.size	veryfastsupg_double_outtree, .Lfunc_end23-veryfastsupg_double_outtree
	.cfi_endproc

	.p2align	4, 0x90
	.type	cluster_mix_double,@function
cluster_mix_double:                     # @cluster_mix_double
	.cfi_startproc
# BB#0:
	movapd	%xmm0, %xmm2
	minsd	%xmm1, %xmm2
	mulsd	sueff1_double(%rip), %xmm2
	addsd	%xmm1, %xmm0
	mulsd	sueff05_double(%rip), %xmm0
	addsd	%xmm2, %xmm0
	retq
.Lfunc_end24:
	.size	cluster_mix_double, .Lfunc_end24-cluster_mix_double
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI25_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.p2align	4, 0x90
	.type	cluster_average_double,@function
cluster_average_double:                 # @cluster_average_double
	.cfi_startproc
# BB#0:
	addsd	%xmm1, %xmm0
	mulsd	.LCPI25_0(%rip), %xmm0
	retq
.Lfunc_end25:
	.size	cluster_average_double, .Lfunc_end25-cluster_average_double
	.cfi_endproc

	.p2align	4, 0x90
	.type	cluster_minimum_double,@function
cluster_minimum_double:                 # @cluster_minimum_double
	.cfi_startproc
# BB#0:
	minsd	%xmm1, %xmm0
	retq
.Lfunc_end26:
	.size	cluster_minimum_double, .Lfunc_end26-cluster_minimum_double
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI27_0:
	.quad	4696837146684686336     # double 1.0E+6
	.quad	4696837146684686336     # double 1.0E+6
.LCPI27_1:
	.quad	4602678819172646912     # double 0.5
	.quad	4602678819172646912     # double 0.5
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI27_2:
	.quad	4696837146684686336     # double 1.0E+6
.LCPI27_3:
	.quad	4602678819172646912     # double 0.5
.LCPI27_4:
	.quad	4606281698874543309     # double 0.90000000000000002
.LCPI27_5:
	.quad	4591870180066957722     # double 0.10000000000000001
	.text
	.globl	veryfastsupg
	.p2align	4, 0x90
	.type	veryfastsupg,@function
veryfastsupg:                           # @veryfastsupg
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi223:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi224:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi225:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi226:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi227:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi228:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi229:
	.cfi_def_cfa_offset 80
.Lcfi230:
	.cfi_offset %rbx, -56
.Lcfi231:
	.cfi_offset %r12, -48
.Lcfi232:
	.cfi_offset %r13, -40
.Lcfi233:
	.cfi_offset %r14, -32
.Lcfi234:
	.cfi_offset %r15, -24
.Lcfi235:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movl	%edi, %r12d
	cmpq	$0, veryfastsupg.eff(%rip)
	jne	.LBB27_2
# BB#1:
	movl	njob(%rip), %edi
	movl	%edi, %esi
	movq	%rcx, %rbx
	callq	AllocateIntMtx
	movq	%rax, veryfastsupg.eff(%rip)
	movl	njob(%rip), %edi
	callq	AllocateIntVec
	movq	%rax, veryfastsupg.hist(%rip)
	movslq	njob(%rip), %r13
	shlq	$3, %r13
	movq	%r13, %rdi
	callq	malloc
	movq	%rax, veryfastsupg.tmptmplen(%rip)
	movq	%r13, %rdi
	callq	malloc
	movq	%rbx, %rcx
	movq	%rax, veryfastsupg.ac(%rip)
.LBB27_2:                               # %.preheader233
	testl	%r12d, %r12d
	jle	.LBB27_14
# BB#3:                                 # %.preheader232.lr.ph
	movq	veryfastsupg.eff(%rip), %r9
	movl	%r12d, %eax
	movl	%r12d, %r8d
	andl	$3, %r8d
	movq	%rax, %r10
	subq	%r8, %r10
	xorl	%r11d, %r11d
	movapd	.LCPI27_0(%rip), %xmm0  # xmm0 = [1.000000e+06,1.000000e+06]
	movapd	.LCPI27_1(%rip), %xmm1  # xmm1 = [5.000000e-01,5.000000e-01]
	.p2align	4, 0x90
.LBB27_4:                               # %.preheader232.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_9 Depth 2
                                        #     Child Loop BB27_11 Depth 2
	cmpl	$4, %r12d
	movq	(%r14,%r11,8), %rbp
	movq	(%r9,%r11,8), %rdx
	jae	.LBB27_6
# BB#5:                                 #   in Loop: Header=BB27_4 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB27_11
	.p2align	4, 0x90
.LBB27_6:                               # %min.iters.checked
                                        #   in Loop: Header=BB27_4 Depth=1
	testq	%r10, %r10
	je	.LBB27_7
# BB#8:                                 # %vector.body.preheader
                                        #   in Loop: Header=BB27_4 Depth=1
	leaq	16(%rbp), %rbx
	leaq	8(%rdx), %rdi
	movq	%r10, %rsi
	.p2align	4, 0x90
.LBB27_9:                               # %vector.body
                                        #   Parent Loop BB27_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-16(%rbx), %xmm2
	movupd	(%rbx), %xmm3
	mulpd	%xmm0, %xmm2
	mulpd	%xmm0, %xmm3
	addpd	%xmm1, %xmm2
	addpd	%xmm1, %xmm3
	cvttpd2dq	%xmm2, %xmm2
	cvttpd2dq	%xmm3, %xmm3
	movlpd	%xmm2, -8(%rdi)
	movlpd	%xmm3, (%rdi)
	addq	$32, %rbx
	addq	$16, %rdi
	addq	$-4, %rsi
	jne	.LBB27_9
# BB#10:                                # %middle.block
                                        #   in Loop: Header=BB27_4 Depth=1
	testl	%r8d, %r8d
	movq	%r10, %rbx
	jne	.LBB27_11
	jmp	.LBB27_12
.LBB27_7:                               #   in Loop: Header=BB27_4 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB27_11:                              # %scalar.ph
                                        #   Parent Loop BB27_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbp,%rbx,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	.LCPI27_2(%rip), %xmm2
	addsd	.LCPI27_3(%rip), %xmm2
	cvttsd2si	%xmm2, %esi
	movl	%esi, (%rdx,%rbx,4)
	incq	%rbx
	cmpq	%rbx, %rax
	jne	.LBB27_11
.LBB27_12:                              # %._crit_edge284.us
                                        #   in Loop: Header=BB27_4 Depth=1
	incq	%r11
	cmpq	%rbx, %r11
	jne	.LBB27_4
# BB#13:                                # %.preheader231
	testl	%r12d, %r12d
	jle	.LBB27_14
# BB#15:                                # %.lr.ph280
	movq	veryfastsupg.ac(%rip), %r10
	leaq	-1(%rax), %rsi
	movq	%rax, %rdi
	andq	$3, %rdi
	je	.LBB27_16
# BB#17:                                # %.prol.preheader
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB27_18:                              # =>This Inner Loop Header: Depth=1
	leaq	1(%rdx), %rbx
	movl	%ebx, (%r10,%rdx,8)
	leal	-1(%rdx), %ebp
	movl	%ebp, 4(%r10,%rdx,8)
	cmpq	%rbx, %rdi
	movq	%rbx, %rdx
	jne	.LBB27_18
	jmp	.LBB27_19
.LBB27_16:
	xorl	%ebx, %ebx
.LBB27_19:                              # %.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB27_22
# BB#20:                                # %.lr.ph280.new
	movq	%rbx, %r8
	subq	%rax, %r8
	leaq	28(%r10,%rbx,8), %rax
	movl	$4294967295, %r9d       # imm = 0xFFFFFFFF
	addq	%rbx, %r9
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB27_21:                              # =>This Inner Loop Header: Depth=1
	leal	(%rbx,%rbp), %edx
	leal	1(%rbx,%rbp), %esi
	movl	%esi, -28(%rax,%rbp,8)
	leal	(%r9,%rbp), %edi
	movl	%edi, -24(%rax,%rbp,8)
	leal	2(%rbx,%rbp), %edi
	movl	%edi, -20(%rax,%rbp,8)
	movl	%edx, -16(%rax,%rbp,8)
	leal	3(%rbx,%rbp), %edx
	movl	%edx, -12(%rax,%rbp,8)
	movl	%esi, -8(%rax,%rbp,8)
	leal	4(%rbx,%rbp), %edx
	movl	%edx, -4(%rax,%rbp,8)
	movl	%edi, (%rax,%rbp,8)
	addq	$4, %rbp
	movq	%r8, %rdx
	addq	%rbp, %rdx
	jne	.LBB27_21
.LBB27_22:                              # %._crit_edge281
	testl	%r12d, %r12d
	movslq	%r12d, %rax
	movl	$-1, -8(%r10,%rax,8)
	jle	.LBB27_23
# BB#25:                                # %._crit_edge275
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	leal	-1(%r12), %eax
	movq	veryfastsupg.tmptmplen(%rip), %rdi
	movl	%eax, %ebx
	leaq	8(,%rbx,8), %rdx
	xorl	%esi, %esi
	callq	memset
	movq	veryfastsupg.hist(%rip), %rdi
	leaq	4(,%rbx,4), %rdx
	movl	$255, %esi
	callq	memset
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	cmpl	$1, %r12d
	je	.LBB27_24
# BB#26:                                # %.lr.ph271.preheader
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movl	$-1, %ebp
	movsd	.LCPI27_4(%rip), %xmm4  # xmm4 = mem[0],zero
	movsd	.LCPI27_5(%rip), %xmm5  # xmm5 = mem[0],zero
	xorl	%r13d, %r13d
	movl	$-1, %r14d
	movq	8(%rsp), %r11           # 8-byte Reload
	movsd	.LCPI27_2(%rip), %xmm2  # xmm2 = mem[0],zero
	movsd	.LCPI27_3(%rip), %xmm3  # xmm3 = mem[0],zero
	jmp	.LBB27_27
.LBB27_28:                              #   in Loop: Header=BB27_27 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.24, %esi
	xorl	%eax, %eax
	movl	%r13d, %edx
	movl	%r12d, %ecx
	callq	fprintf
	movsd	.LCPI27_5(%rip), %xmm5  # xmm5 = mem[0],zero
	movsd	.LCPI27_4(%rip), %xmm4  # xmm4 = mem[0],zero
	movsd	.LCPI27_3(%rip), %xmm3  # xmm3 = mem[0],zero
	movsd	.LCPI27_2(%rip), %xmm2  # xmm2 = mem[0],zero
	movq	8(%rsp), %r11           # 8-byte Reload
	jmp	.LBB27_29
	.p2align	4, 0x90
.LBB27_27:                              # %.lr.ph271
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_31 Depth 2
                                        #       Child Loop BB27_32 Depth 3
                                        #     Child Loop BB27_38 Depth 2
                                        #     Child Loop BB27_41 Depth 2
                                        #     Child Loop BB27_46 Depth 2
                                        #     Child Loop BB27_49 Depth 2
                                        #     Child Loop BB27_51 Depth 2
	movslq	%r13d, %rax
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$34, %rcx
	addl	%edx, %ecx
	addl	%ecx, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	je	.LBB27_28
.LBB27_29:                              # %.preheader229
                                        #   in Loop: Header=BB27_27 Depth=1
	movq	veryfastsupg.ac(%rip), %rcx
	movl	(%rcx), %edi
	movl	$4000000, %eax          # imm = 0x3D0900
	cmpl	$-1, %edi
	je	.LBB27_34
# BB#30:                                # %.preheader.lr.ph
                                        #   in Loop: Header=BB27_27 Depth=1
	xorl	%edx, %edx
	movq	veryfastsupg.eff(%rip), %r8
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB27_31:                              # %.lr.ph
                                        #   Parent Loop BB27_27 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB27_32 Depth 3
	movl	%edi, %r9d
	movq	(%r8,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB27_32:                              #   Parent Loop BB27_27 Depth=1
                                        #     Parent Loop BB27_31 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%edi, %rdi
	movl	(%rdx,%rdi,4), %ebx
	cmpl	%eax, %ebx
	cmovlel	%ebx, %eax
	cmovll	%esi, %r14d
	cmovll	%edi, %ebp
	movl	(%rcx,%rdi,8), %edi
	cmpl	$-1, %edi
	jne	.LBB27_32
# BB#33:                                # %._crit_edge
                                        #   in Loop: Header=BB27_31 Depth=2
	movslq	%r9d, %rdx
	movl	(%rcx,%rdx,8), %edi
	cmpl	$-1, %edi
	movl	%r9d, %esi
	jne	.LBB27_31
.LBB27_34:                              # %._crit_edge244
                                        #   in Loop: Header=BB27_27 Depth=1
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm3, %xmm0
	movq	(%r15,%r13,8), %rdx
	movq	(%rdx), %rsi
	movq	veryfastsupg.hist(%rip), %r8
	movslq	%r14d, %r10
	movslq	(%r8,%r10,4), %rax
	cmpq	$-1, %rax
	je	.LBB27_35
# BB#36:                                #   in Loop: Header=BB27_27 Depth=1
	movq	(%r15,%rax,8), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdi
	movl	(%rcx), %eax
	cmpl	(%rdi), %eax
	movq	%rcx, %rbx
	cmovgq	%rdi, %rbx
	cmovgq	%rcx, %rdi
	movl	(%rbx), %eax
	cmpl	$-1, %eax
	je	.LBB27_39
# BB#37:                                # %.lr.ph251.preheader
                                        #   in Loop: Header=BB27_27 Depth=1
	addq	$4, %rbx
	.p2align	4, 0x90
.LBB27_38:                              # %.lr.ph251
                                        #   Parent Loop BB27_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rsi)
	addq	$4, %rsi
	movl	(%rbx), %eax
	addq	$4, %rbx
	cmpl	$-1, %eax
	jne	.LBB27_38
.LBB27_39:                              # %.preheader227
                                        #   in Loop: Header=BB27_27 Depth=1
	movl	(%rdi), %eax
	cmpl	$-1, %eax
	je	.LBB27_42
# BB#40:                                # %.lr.ph255.preheader
                                        #   in Loop: Header=BB27_27 Depth=1
	addq	$4, %rdi
	.p2align	4, 0x90
.LBB27_41:                              # %.lr.ph255
                                        #   Parent Loop BB27_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rsi)
	addq	$4, %rsi
	movl	(%rdi), %eax
	addq	$4, %rdi
	cmpl	$-1, %eax
	jne	.LBB27_41
	jmp	.LBB27_42
	.p2align	4, 0x90
.LBB27_35:                              #   in Loop: Header=BB27_27 Depth=1
	movl	%r14d, (%rsi)
	addq	$4, %rsi
.LBB27_42:                              # %.loopexit228
                                        #   in Loop: Header=BB27_27 Depth=1
	divsd	%xmm2, %xmm0
	movl	$-1, (%rsi)
	movq	8(%rdx), %rsi
	movslq	%ebp, %r9
	movslq	(%r8,%r9,4), %rax
	cmpq	$-1, %rax
	je	.LBB27_43
# BB#44:                                #   in Loop: Header=BB27_27 Depth=1
	movq	(%r15,%rax,8), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdi
	movl	(%rcx), %eax
	cmpl	(%rdi), %eax
	movq	%rcx, %rbx
	cmovgq	%rdi, %rbx
	cmovgq	%rcx, %rdi
	movl	(%rbx), %eax
	cmpl	$-1, %eax
	je	.LBB27_47
# BB#45:                                # %.lr.ph260.preheader
                                        #   in Loop: Header=BB27_27 Depth=1
	addq	$4, %rbx
	.p2align	4, 0x90
.LBB27_46:                              # %.lr.ph260
                                        #   Parent Loop BB27_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rsi)
	addq	$4, %rsi
	movl	(%rbx), %eax
	addq	$4, %rbx
	cmpl	$-1, %eax
	jne	.LBB27_46
.LBB27_47:                              # %.preheader226
                                        #   in Loop: Header=BB27_27 Depth=1
	movl	(%rdi), %eax
	cmpl	$-1, %eax
	je	.LBB27_50
# BB#48:                                # %.lr.ph264.preheader
                                        #   in Loop: Header=BB27_27 Depth=1
	addq	$4, %rdi
	.p2align	4, 0x90
.LBB27_49:                              # %.lr.ph264
                                        #   Parent Loop BB27_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rsi)
	addq	$4, %rsi
	movl	(%rdi), %eax
	addq	$4, %rdi
	cmpl	$-1, %eax
	jne	.LBB27_49
	jmp	.LBB27_50
	.p2align	4, 0x90
.LBB27_43:                              #   in Loop: Header=BB27_27 Depth=1
	movl	%ebp, (%rsi)
	addq	$4, %rsi
.LBB27_50:                              # %.loopexit
                                        #   in Loop: Header=BB27_27 Depth=1
	movl	$-1, (%rsi)
	movq	veryfastsupg.tmptmplen(%rip), %rax
	movapd	%xmm0, %xmm1
	subsd	(%rax,%r10,8), %xmm1
	movq	(%r11,%r13,8), %rcx
	movsd	%xmm1, (%rcx)
	movapd	%xmm0, %xmm1
	subsd	(%rax,%r9,8), %xmm1
	movsd	%xmm1, 8(%rcx)
	movsd	%xmm0, (%rax,%r10,8)
	movl	%r13d, (%r8,%r10,4)
	movq	veryfastsupg.ac(%rip), %r8
	xorl	%ecx, %ecx
	movq	veryfastsupg.eff(%rip), %rdi
	.p2align	4, 0x90
.LBB27_51:                              #   Parent Loop BB27_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%r14d, %ecx
	je	.LBB27_56
# BB#52:                                #   in Loop: Header=BB27_51 Depth=2
	cmpl	%ebp, %ecx
	je	.LBB27_56
# BB#53:                                #   in Loop: Header=BB27_51 Depth=2
	cmpl	%r14d, %ecx
	movl	%ebp, %esi
	movl	%ecx, %ebx
	movl	%r14d, %edx
	movl	%ecx, %eax
	jl	.LBB27_55
# BB#54:                                #   in Loop: Header=BB27_51 Depth=2
	cmpl	%ebp, %ecx
	movl	%ecx, %esi
	cmovll	%ebp, %esi
	movl	%ebp, %ebx
	cmovlel	%ecx, %ebx
	movl	%ecx, %edx
	movl	%r14d, %eax
.LBB27_55:                              #   in Loop: Header=BB27_51 Depth=2
	cltq
	movq	(%rdi,%rax,8), %r10
	movslq	%edx, %rdx
	movl	(%r10,%rdx,4), %eax
	movslq	%ebx, %rbx
	movq	(%rdi,%rbx,8), %rbx
	movslq	%esi, %rsi
	movl	(%rbx,%rsi,4), %ebx
	cmpl	%ebx, %eax
	movl	%ebx, %esi
	cmovlel	%eax, %esi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm0
	mulsd	%xmm4, %xmm0
	addl	%eax, %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm3, %xmm1
	mulsd	%xmm5, %xmm1
	addsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %eax
	movl	%eax, (%r10,%rdx,4)
.LBB27_56:                              #   in Loop: Header=BB27_51 Depth=2
	movslq	%ecx, %rax
	movl	(%r8,%rax,8), %ecx
	cmpl	$-1, %ecx
	jne	.LBB27_51
# BB#57:                                #   in Loop: Header=BB27_27 Depth=1
	movslq	4(%r8,%r9,8), %rcx
	movslq	(%r8,%r9,8), %rax
	cmpq	$-1, %rax
	movl	%eax, (%r8,%rcx,8)
	je	.LBB27_59
# BB#58:                                #   in Loop: Header=BB27_27 Depth=1
	movl	%ecx, 4(%r8,%rax,8)
.LBB27_59:                              #   in Loop: Header=BB27_27 Depth=1
	incq	%r13
	cmpq	16(%rsp), %r13          # 8-byte Folded Reload
	jne	.LBB27_27
	jmp	.LBB27_60
.LBB27_14:                              # %._crit_edge281.thread
	movq	veryfastsupg.ac(%rip), %rax
	movslq	%r12d, %rcx
	movl	$-1, -8(%rax,%rcx,8)
.LBB27_23:                              # %._crit_edge275.thread
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
.LBB27_24:                              # %._crit_edge275.._crit_edge272_crit_edge
	movq	veryfastsupg.eff(%rip), %rdi
.LBB27_60:                              # %._crit_edge272
	callq	FreeIntMtx
	movq	$0, veryfastsupg.eff(%rip)
	movq	veryfastsupg.tmptmplen(%rip), %rdi
	callq	free
	movq	$0, veryfastsupg.tmptmplen(%rip)
	movq	veryfastsupg.hist(%rip), %rdi
	callq	free
	movq	$0, veryfastsupg.hist(%rip)
	movq	veryfastsupg.ac(%rip), %rdi
	callq	free
	movq	$0, veryfastsupg.ac(%rip)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end27:
	.size	veryfastsupg, .Lfunc_end27-veryfastsupg
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI28_0:
	.quad	4602678819172646912     # double 0.5
.LCPI28_1:
	.quad	4606281698874543309     # double 0.90000000000000002
.LCPI28_2:
	.quad	4591870180066957722     # double 0.10000000000000001
	.text
	.globl	veryfastsupg_int
	.p2align	4, 0x90
	.type	veryfastsupg_int,@function
veryfastsupg_int:                       # @veryfastsupg_int
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi236:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi237:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi238:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi239:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi240:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi241:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi242:
	.cfi_def_cfa_offset 80
.Lcfi243:
	.cfi_offset %rbx, -56
.Lcfi244:
	.cfi_offset %r12, -48
.Lcfi245:
	.cfi_offset %r13, -40
.Lcfi246:
	.cfi_offset %r14, -32
.Lcfi247:
	.cfi_offset %r15, -24
.Lcfi248:
	.cfi_offset %rbp, -16
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, %r15
	movq	%rsi, %r14
	movl	%edi, %r12d
	cmpq	$0, veryfastsupg_int.eff(%rip)
	jne	.LBB28_2
# BB#1:
	movl	njob(%rip), %edi
	movl	%edi, %esi
	callq	AllocateIntMtx
	movq	%rax, veryfastsupg_int.eff(%rip)
	movl	njob(%rip), %edi
	callq	AllocateIntVec
	movq	%rax, veryfastsupg_int.hist(%rip)
	movl	njob(%rip), %edi
	callq	AllocateIntVec
	movq	%rax, veryfastsupg_int.tmptmplen(%rip)
	movslq	njob(%rip), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, veryfastsupg_int.ac(%rip)
.LBB28_2:                               # %.preheader231
	testl	%r12d, %r12d
	jle	.LBB28_64
# BB#3:                                 # %.preheader230.lr.ph
	movq	veryfastsupg_int.eff(%rip), %r11
	movl	%r12d, %ecx
	leaq	-1(%rcx), %r10
	movl	%r12d, %r8d
	andl	$7, %r8d
	movq	%rcx, %r9
	subq	%r8, %r9
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB28_4:                               # %.preheader230.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB28_12 Depth 2
                                        #     Child Loop BB28_16 Depth 2
                                        #     Child Loop BB28_18 Depth 2
	cmpl	$8, %r12d
	movq	(%r14,%r13,8), %rsi
	movq	(%r11,%r13,8), %rbx
	jae	.LBB28_6
# BB#5:                                 #   in Loop: Header=BB28_4 Depth=1
	xorl	%edi, %edi
	jmp	.LBB28_14
	.p2align	4, 0x90
.LBB28_6:                               # %min.iters.checked
                                        #   in Loop: Header=BB28_4 Depth=1
	testq	%r9, %r9
	je	.LBB28_10
# BB#7:                                 # %vector.memcheck
                                        #   in Loop: Header=BB28_4 Depth=1
	leaq	(%rsi,%rcx,4), %rax
	cmpq	%rax, %rbx
	jae	.LBB28_11
# BB#8:                                 # %vector.memcheck
                                        #   in Loop: Header=BB28_4 Depth=1
	leaq	(%rbx,%rcx,4), %rax
	cmpq	%rax, %rsi
	jae	.LBB28_11
.LBB28_10:                              #   in Loop: Header=BB28_4 Depth=1
	xorl	%edi, %edi
	jmp	.LBB28_14
.LBB28_11:                              # %vector.body.preheader
                                        #   in Loop: Header=BB28_4 Depth=1
	leaq	16(%rsi), %rax
	leaq	16(%rbx), %rdx
	movq	%r9, %rdi
	.p2align	4, 0x90
.LBB28_12:                              # %vector.body
                                        #   Parent Loop BB28_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-16(%rax), %xmm0
	movupd	(%rax), %xmm1
	movupd	%xmm0, -16(%rdx)
	movupd	%xmm1, (%rdx)
	addq	$32, %rax
	addq	$32, %rdx
	addq	$-8, %rdi
	jne	.LBB28_12
# BB#13:                                # %middle.block
                                        #   in Loop: Header=BB28_4 Depth=1
	testl	%r8d, %r8d
	movq	%r9, %rdi
	je	.LBB28_19
	.p2align	4, 0x90
.LBB28_14:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB28_4 Depth=1
	movl	%ecx, %eax
	subl	%edi, %eax
	movq	%r10, %rdx
	subq	%rdi, %rdx
	andq	$7, %rax
	je	.LBB28_17
# BB#15:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB28_4 Depth=1
	negq	%rax
	.p2align	4, 0x90
.LBB28_16:                              # %scalar.ph.prol
                                        #   Parent Loop BB28_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi,%rdi,4), %ebp
	movl	%ebp, (%rbx,%rdi,4)
	incq	%rdi
	incq	%rax
	jne	.LBB28_16
.LBB28_17:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB28_4 Depth=1
	cmpq	$7, %rdx
	jb	.LBB28_19
	.p2align	4, 0x90
.LBB28_18:                              # %scalar.ph
                                        #   Parent Loop BB28_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi,%rdi,4), %eax
	movl	%eax, (%rbx,%rdi,4)
	movl	4(%rsi,%rdi,4), %eax
	movl	%eax, 4(%rbx,%rdi,4)
	movl	8(%rsi,%rdi,4), %eax
	movl	%eax, 8(%rbx,%rdi,4)
	movl	12(%rsi,%rdi,4), %eax
	movl	%eax, 12(%rbx,%rdi,4)
	movl	16(%rsi,%rdi,4), %eax
	movl	%eax, 16(%rbx,%rdi,4)
	movl	20(%rsi,%rdi,4), %eax
	movl	%eax, 20(%rbx,%rdi,4)
	movl	24(%rsi,%rdi,4), %eax
	movl	%eax, 24(%rbx,%rdi,4)
	movl	28(%rsi,%rdi,4), %eax
	movl	%eax, 28(%rbx,%rdi,4)
	addq	$8, %rdi
	cmpq	%rdi, %rcx
	jne	.LBB28_18
.LBB28_19:                              # %._crit_edge282.us
                                        #   in Loop: Header=BB28_4 Depth=1
	incq	%r13
	cmpq	%rdi, %r13
	jne	.LBB28_4
# BB#20:                                # %.preheader229
	testl	%r12d, %r12d
	jle	.LBB28_64
# BB#21:                                # %.lr.ph278
	movq	veryfastsupg_int.ac(%rip), %rbx
	leaq	-1(%rcx), %rsi
	movq	%rcx, %rax
	andq	$3, %rax
	je	.LBB28_24
# BB#22:                                # %.prol.preheader
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB28_23:                              # =>This Inner Loop Header: Depth=1
	leaq	1(%rdi), %rdx
	movl	%edx, (%rbx,%rdi,8)
	leal	-1(%rdi), %ebp
	movl	%ebp, 4(%rbx,%rdi,8)
	cmpq	%rdx, %rax
	movq	%rdx, %rdi
	jne	.LBB28_23
	jmp	.LBB28_25
.LBB28_24:
	xorl	%edx, %edx
.LBB28_25:                              # %.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB28_28
# BB#26:                                # %.lr.ph278.new
	movq	%rdx, %r8
	subq	%rcx, %r8
	leaq	28(%rbx,%rdx,8), %rax
	movl	$4294967295, %r9d       # imm = 0xFFFFFFFF
	addq	%rdx, %r9
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB28_27:                              # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rbp), %ecx
	leal	1(%rdx,%rbp), %esi
	movl	%esi, -28(%rax,%rbp,8)
	leal	(%r9,%rbp), %edi
	movl	%edi, -24(%rax,%rbp,8)
	leal	2(%rdx,%rbp), %edi
	movl	%edi, -20(%rax,%rbp,8)
	movl	%ecx, -16(%rax,%rbp,8)
	leal	3(%rdx,%rbp), %ecx
	movl	%ecx, -12(%rax,%rbp,8)
	movl	%esi, -8(%rax,%rbp,8)
	leal	4(%rdx,%rbp), %ecx
	movl	%ecx, -4(%rax,%rbp,8)
	movl	%edi, (%rax,%rbp,8)
	addq	$4, %rbp
	movq	%r8, %rcx
	addq	%rbp, %rcx
	jne	.LBB28_27
.LBB28_28:                              # %._crit_edge279
	testl	%r12d, %r12d
	movslq	%r12d, %rax
	movl	$-1, -8(%rbx,%rax,8)
	jle	.LBB28_65
# BB#29:                                # %._crit_edge273
	leal	-1(%r12), %eax
	movq	veryfastsupg_int.tmptmplen(%rip), %rdi
	movl	%eax, %ebp
	leaq	4(,%rbp,4), %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	veryfastsupg_int.hist(%rip), %rdi
	movl	$255, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	cmpl	$1, %r12d
	je	.LBB28_66
# BB#30:                                # %.lr.ph269.preheader
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movl	$-1, %ebp
	movsd	.LCPI28_0(%rip), %xmm2  # xmm2 = mem[0],zero
	movsd	.LCPI28_1(%rip), %xmm3  # xmm3 = mem[0],zero
	movsd	.LCPI28_2(%rip), %xmm4  # xmm4 = mem[0],zero
	xorl	%r13d, %r13d
	movl	$-1, %r14d
	movq	8(%rsp), %r11           # 8-byte Reload
	jmp	.LBB28_32
.LBB28_31:                              #   in Loop: Header=BB28_32 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.24, %esi
	xorl	%eax, %eax
	movl	%r13d, %edx
	movl	%r12d, %ecx
	callq	fprintf
	movsd	.LCPI28_2(%rip), %xmm4  # xmm4 = mem[0],zero
	movsd	.LCPI28_1(%rip), %xmm3  # xmm3 = mem[0],zero
	movsd	.LCPI28_0(%rip), %xmm2  # xmm2 = mem[0],zero
	movq	8(%rsp), %r11           # 8-byte Reload
	jmp	.LBB28_33
	.p2align	4, 0x90
.LBB28_32:                              # %.lr.ph269
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB28_35 Depth 2
                                        #       Child Loop BB28_36 Depth 3
                                        #     Child Loop BB28_41 Depth 2
                                        #     Child Loop BB28_44 Depth 2
                                        #     Child Loop BB28_49 Depth 2
                                        #     Child Loop BB28_52 Depth 2
                                        #     Child Loop BB28_55 Depth 2
	movslq	%r13d, %rax
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$34, %rcx
	addl	%edx, %ecx
	addl	%ecx, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	je	.LBB28_31
.LBB28_33:                              # %.preheader227
                                        #   in Loop: Header=BB28_32 Depth=1
	movq	veryfastsupg_int.ac(%rip), %rcx
	movl	(%rcx), %edi
	movl	$4000000, %eax          # imm = 0x3D0900
	cmpl	$-1, %edi
	je	.LBB28_38
# BB#34:                                # %.preheader.lr.ph
                                        #   in Loop: Header=BB28_32 Depth=1
	xorl	%edx, %edx
	movq	veryfastsupg_int.eff(%rip), %r8
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB28_35:                              # %.lr.ph
                                        #   Parent Loop BB28_32 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB28_36 Depth 3
	movl	%edi, %r9d
	movq	(%r8,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB28_36:                              #   Parent Loop BB28_32 Depth=1
                                        #     Parent Loop BB28_35 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%edi, %rdi
	movl	(%rdx,%rdi,4), %ebx
	cmpl	%eax, %ebx
	cmovlel	%ebx, %eax
	cmovll	%esi, %r14d
	cmovll	%edi, %ebp
	movl	(%rcx,%rdi,8), %edi
	cmpl	$-1, %edi
	jne	.LBB28_36
# BB#37:                                # %._crit_edge
                                        #   in Loop: Header=BB28_35 Depth=2
	movslq	%r9d, %rdx
	movl	(%rcx,%rdx,8), %edi
	cmpl	$-1, %edi
	movl	%r9d, %esi
	jne	.LBB28_35
.LBB28_38:                              # %._crit_edge242
                                        #   in Loop: Header=BB28_32 Depth=1
	movq	(%r15,%r13,8), %rsi
	movq	(%rsi), %rdi
	movq	veryfastsupg_int.hist(%rip), %r8
	movslq	%r14d, %r9
	movslq	(%r8,%r9,4), %rcx
	cmpq	$-1, %rcx
	je	.LBB28_45
# BB#39:                                #   in Loop: Header=BB28_32 Depth=1
	movq	(%r15,%rcx,8), %rcx
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	movl	(%rdx), %ebx
	cmpl	(%rcx), %ebx
	movq	%rdx, %rbx
	cmovgq	%rcx, %rbx
	cmovgq	%rdx, %rcx
	movl	(%rbx), %edx
	cmpl	$-1, %edx
	je	.LBB28_42
# BB#40:                                # %.lr.ph249.preheader
                                        #   in Loop: Header=BB28_32 Depth=1
	addq	$4, %rbx
	.p2align	4, 0x90
.LBB28_41:                              # %.lr.ph249
                                        #   Parent Loop BB28_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, (%rdi)
	addq	$4, %rdi
	movl	(%rbx), %edx
	addq	$4, %rbx
	cmpl	$-1, %edx
	jne	.LBB28_41
.LBB28_42:                              # %.preheader225
                                        #   in Loop: Header=BB28_32 Depth=1
	movl	(%rcx), %edx
	cmpl	$-1, %edx
	je	.LBB28_46
# BB#43:                                # %.lr.ph253.preheader
                                        #   in Loop: Header=BB28_32 Depth=1
	addq	$4, %rcx
	.p2align	4, 0x90
.LBB28_44:                              # %.lr.ph253
                                        #   Parent Loop BB28_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, (%rdi)
	addq	$4, %rdi
	movl	(%rcx), %edx
	addq	$4, %rcx
	cmpl	$-1, %edx
	jne	.LBB28_44
	jmp	.LBB28_46
	.p2align	4, 0x90
.LBB28_45:                              #   in Loop: Header=BB28_32 Depth=1
	movl	%r14d, (%rdi)
	addq	$4, %rdi
.LBB28_46:                              # %.loopexit226
                                        #   in Loop: Header=BB28_32 Depth=1
	movl	$-1, (%rdi)
	movq	8(%rsi), %rdi
	movslq	%ebp, %r10
	movslq	(%r8,%r10,4), %rcx
	cmpq	$-1, %rcx
	je	.LBB28_53
# BB#47:                                #   in Loop: Header=BB28_32 Depth=1
	movq	(%r15,%rcx,8), %rcx
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	movl	(%rdx), %esi
	cmpl	(%rcx), %esi
	movq	%rdx, %rbx
	cmovgq	%rcx, %rbx
	cmovgq	%rdx, %rcx
	movl	(%rbx), %edx
	cmpl	$-1, %edx
	je	.LBB28_50
# BB#48:                                # %.lr.ph258.preheader
                                        #   in Loop: Header=BB28_32 Depth=1
	addq	$4, %rbx
	.p2align	4, 0x90
.LBB28_49:                              # %.lr.ph258
                                        #   Parent Loop BB28_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, (%rdi)
	addq	$4, %rdi
	movl	(%rbx), %edx
	addq	$4, %rbx
	cmpl	$-1, %edx
	jne	.LBB28_49
.LBB28_50:                              # %.preheader224
                                        #   in Loop: Header=BB28_32 Depth=1
	movl	(%rcx), %edx
	cmpl	$-1, %edx
	je	.LBB28_54
# BB#51:                                # %.lr.ph262.preheader
                                        #   in Loop: Header=BB28_32 Depth=1
	addq	$4, %rcx
	.p2align	4, 0x90
.LBB28_52:                              # %.lr.ph262
                                        #   Parent Loop BB28_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, (%rdi)
	addq	$4, %rdi
	movl	(%rcx), %edx
	addq	$4, %rcx
	cmpl	$-1, %edx
	jne	.LBB28_52
	jmp	.LBB28_54
	.p2align	4, 0x90
.LBB28_53:                              #   in Loop: Header=BB28_32 Depth=1
	movl	%ebp, (%rdi)
	addq	$4, %rdi
.LBB28_54:                              # %.loopexit
                                        #   in Loop: Header=BB28_32 Depth=1
	movl	$-1, (%rdi)
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm2, %xmm0
	cvttsd2si	%xmm0, %eax
	movq	veryfastsupg_int.tmptmplen(%rip), %rcx
	movl	%eax, %edx
	subl	(%rcx,%r9,4), %edx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	movq	(%r11,%r13,8), %rdx
	movsd	%xmm0, (%rdx)
	movl	%eax, %esi
	subl	(%rcx,%r10,4), %esi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm0
	movsd	%xmm0, 8(%rdx)
	movl	%eax, (%rcx,%r9,4)
	movl	%r13d, (%r8,%r9,4)
	movq	veryfastsupg_int.ac(%rip), %r8
	xorl	%ecx, %ecx
	movq	veryfastsupg_int.eff(%rip), %rdi
	.p2align	4, 0x90
.LBB28_55:                              #   Parent Loop BB28_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%r14d, %ecx
	je	.LBB28_60
# BB#56:                                #   in Loop: Header=BB28_55 Depth=2
	cmpl	%ebp, %ecx
	je	.LBB28_60
# BB#57:                                #   in Loop: Header=BB28_55 Depth=2
	cmpl	%r14d, %ecx
	movl	%ecx, %edx
	movl	%ebp, %ebx
	movl	%r14d, %esi
	movl	%ecx, %eax
	jl	.LBB28_59
# BB#58:                                #   in Loop: Header=BB28_55 Depth=2
	cmpl	%ebp, %ecx
	movl	%ebp, %edx
	cmovlel	%ecx, %edx
	movl	%ecx, %ebx
	cmovll	%ebp, %ebx
	movl	%ecx, %esi
	movl	%r14d, %eax
.LBB28_59:                              #   in Loop: Header=BB28_55 Depth=2
	cltq
	movq	(%rdi,%rax,8), %r9
	movslq	%esi, %rsi
	movl	(%r9,%rsi,4), %eax
	movslq	%edx, %rdx
	movq	(%rdi,%rdx,8), %rdx
	movslq	%ebx, %rbx
	movl	(%rdx,%rbx,4), %ebx
	cmpl	%ebx, %eax
	movl	%ebx, %edx
	cmovlel	%eax, %edx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm0
	cvtss2sd	%xmm0, %xmm0
	mulsd	%xmm3, %xmm0
	addl	%eax, %ebx
	cvtsi2ssl	%ebx, %xmm1
	cvtss2sd	%xmm1, %xmm1
	mulsd	%xmm2, %xmm1
	mulsd	%xmm4, %xmm1
	addsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %eax
	movl	%eax, (%r9,%rsi,4)
.LBB28_60:                              #   in Loop: Header=BB28_55 Depth=2
	movslq	%ecx, %rax
	movl	(%r8,%rax,8), %ecx
	cmpl	$-1, %ecx
	jne	.LBB28_55
# BB#61:                                #   in Loop: Header=BB28_32 Depth=1
	movslq	4(%r8,%r10,8), %rcx
	movslq	(%r8,%r10,8), %rax
	cmpq	$-1, %rax
	movl	%eax, (%r8,%rcx,8)
	je	.LBB28_63
# BB#62:                                #   in Loop: Header=BB28_32 Depth=1
	movl	%ecx, 4(%r8,%rax,8)
.LBB28_63:                              #   in Loop: Header=BB28_32 Depth=1
	incq	%r13
	cmpq	16(%rsp), %r13          # 8-byte Folded Reload
	jne	.LBB28_32
	jmp	.LBB28_67
.LBB28_64:                              # %._crit_edge279.thread
	movq	veryfastsupg_int.ac(%rip), %rax
	movslq	%r12d, %rcx
	movl	$-1, -8(%rax,%rcx,8)
.LBB28_65:                              # %._crit_edge273.thread
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
.LBB28_66:                              # %._crit_edge273.._crit_edge270_crit_edge
	movq	veryfastsupg_int.eff(%rip), %rdi
.LBB28_67:                              # %._crit_edge270
	callq	FreeIntMtx
	movq	$0, veryfastsupg_int.eff(%rip)
	movq	veryfastsupg_int.tmptmplen(%rip), %rdi
	callq	free
	movq	$0, veryfastsupg_int.tmptmplen(%rip)
	movq	veryfastsupg_int.hist(%rip), %rdi
	callq	free
	movq	$0, veryfastsupg_int.hist(%rip)
	movq	veryfastsupg_int.ac(%rip), %rdi
	callq	free
	movq	$0, veryfastsupg_int.ac(%rip)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end28:
	.size	veryfastsupg_int, .Lfunc_end28-veryfastsupg_int
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI29_0:
	.long	1176255488              # float 9999
.LCPI29_1:
	.long	1056964608              # float 0.5
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI29_2:
	.quad	4606281698874543309     # double 0.90000000000000002
.LCPI29_3:
	.quad	4602678819172646912     # double 0.5
.LCPI29_4:
	.quad	4591870180066957722     # double 0.10000000000000001
	.text
	.globl	fastsupg
	.p2align	4, 0x90
	.type	fastsupg,@function
fastsupg:                               # @fastsupg
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi249:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi250:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi251:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi252:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi253:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi254:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi255:
	.cfi_def_cfa_offset 128
.Lcfi256:
	.cfi_offset %rbx, -56
.Lcfi257:
	.cfi_offset %r12, -48
.Lcfi258:
	.cfi_offset %r13, -40
.Lcfi259:
	.cfi_offset %r14, -32
.Lcfi260:
	.cfi_offset %r15, -24
.Lcfi261:
	.cfi_offset %rbp, -16
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %rbx
	movl	%edi, %r14d
	cmpq	$0, fastsupg.eff(%rip)
	jne	.LBB29_2
# BB#1:
	movl	njob(%rip), %edi
	movl	%edi, %esi
	callq	AllocateFloatMtx
	movq	%rax, fastsupg.eff(%rip)
	movl	njob(%rip), %edi
	movl	%edi, %esi
	callq	AllocateCharMtx
	movq	%rax, fastsupg.pair(%rip)
	movl	njob(%rip), %edi
	callq	AllocateFloatVec
	movq	%rax, fastsupg.tmplen(%rip)
	movslq	njob(%rip), %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, fastsupg.ac(%rip)
.LBB29_2:                               # %.preheader192
	testl	%r14d, %r14d
	jle	.LBB29_14
# BB#3:                                 # %.preheader191.lr.ph
	movq	fastsupg.eff(%rip), %r9
	movl	%r14d, %r13d
	movl	%r14d, %r8d
	andl	$3, %r8d
	movq	%r13, %r10
	subq	%r8, %r10
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB29_4:                               # %.preheader191.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB29_9 Depth 2
                                        #     Child Loop BB29_11 Depth 2
	cmpl	$4, %r14d
	movq	(%rbx,%rsi,8), %rdi
	movq	(%r9,%rsi,8), %rbp
	jae	.LBB29_6
# BB#5:                                 #   in Loop: Header=BB29_4 Depth=1
	xorl	%edx, %edx
	jmp	.LBB29_11
	.p2align	4, 0x90
.LBB29_6:                               # %min.iters.checked
                                        #   in Loop: Header=BB29_4 Depth=1
	testq	%r10, %r10
	je	.LBB29_7
# BB#8:                                 # %vector.body.preheader
                                        #   in Loop: Header=BB29_4 Depth=1
	leaq	16(%rdi), %rdx
	leaq	8(%rbp), %rax
	movq	%r10, %rcx
	.p2align	4, 0x90
.LBB29_9:                               # %vector.body
                                        #   Parent Loop BB29_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-16(%rdx), %xmm0
	movupd	(%rdx), %xmm1
	cvtpd2ps	%xmm0, %xmm0
	cvtpd2ps	%xmm1, %xmm1
	movlpd	%xmm0, -8(%rax)
	movlpd	%xmm1, (%rax)
	addq	$32, %rdx
	addq	$16, %rax
	addq	$-4, %rcx
	jne	.LBB29_9
# BB#10:                                # %middle.block
                                        #   in Loop: Header=BB29_4 Depth=1
	testl	%r8d, %r8d
	movq	%r10, %rdx
	jne	.LBB29_11
	jmp	.LBB29_12
.LBB29_7:                               #   in Loop: Header=BB29_4 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB29_11:                              # %scalar.ph
                                        #   Parent Loop BB29_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rbp,%rdx,4)
	incq	%rdx
	cmpq	%rdx, %r13
	jne	.LBB29_11
.LBB29_12:                              # %._crit_edge246.us
                                        #   in Loop: Header=BB29_4 Depth=1
	incq	%rsi
	cmpq	%rdx, %rsi
	jne	.LBB29_4
# BB#13:                                # %.preheader190
	testl	%r14d, %r14d
	jle	.LBB29_14
# BB#16:                                # %.lr.ph242
	movq	fastsupg.ac(%rip), %rax
	leaq	-1(%r13), %rdx
	movq	%r13, %rsi
	andq	$3, %rsi
	je	.LBB29_17
# BB#18:                                # %.prol.preheader324
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB29_19:                              # =>This Inner Loop Header: Depth=1
	leaq	1(%rdi), %rcx
	movl	%ecx, (%rax,%rdi,8)
	leal	-1(%rdi), %ebp
	movl	%ebp, 4(%rax,%rdi,8)
	cmpq	%rcx, %rsi
	movq	%rcx, %rdi
	jne	.LBB29_19
	jmp	.LBB29_20
.LBB29_17:
	xorl	%ecx, %ecx
.LBB29_20:                              # %.prol.loopexit325
	cmpq	$3, %rdx
	jb	.LBB29_23
# BB#21:                                # %.lr.ph242.new
	movq	%rcx, %r8
	subq	%r13, %r8
	leaq	28(%rax,%rcx,8), %rsi
	movl	$4294967295, %r9d       # imm = 0xFFFFFFFF
	addq	%rcx, %r9
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB29_22:                              # =>This Inner Loop Header: Depth=1
	leal	(%rcx,%rbp), %ebx
	leal	1(%rcx,%rbp), %edx
	movl	%edx, -28(%rsi,%rbp,8)
	leal	(%r9,%rbp), %edi
	movl	%edi, -24(%rsi,%rbp,8)
	leal	2(%rcx,%rbp), %edi
	movl	%edi, -20(%rsi,%rbp,8)
	movl	%ebx, -16(%rsi,%rbp,8)
	leal	3(%rcx,%rbp), %ebx
	movl	%ebx, -12(%rsi,%rbp,8)
	movl	%edx, -8(%rsi,%rbp,8)
	leal	4(%rcx,%rbp), %edx
	movl	%edx, -4(%rsi,%rbp,8)
	movl	%edi, (%rsi,%rbp,8)
	addq	$4, %rbp
	movq	%r8, %rdx
	addq	%rbp, %rdx
	jne	.LBB29_22
.LBB29_23:                              # %._crit_edge243
	movslq	%r14d, %rcx
	movl	$-1, -8(%rax,%rcx,8)
	testl	%r14d, %r14d
	jle	.LBB29_15
# BB#24:                                # %.preheader188.lr.ph
	leal	-1(%r14), %eax
	movq	fastsupg.tmplen(%rip), %rdi
	movl	%eax, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	4(,%rax,4), %rdx
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	callq	memset
	movq	fastsupg.pair(%rip), %rax
	leaq	-1(%r13), %rcx
	movl	%r13d, %edx
	andl	$7, %edx
	.p2align	4, 0x90
.LBB29_25:                              # %.preheader188.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB29_26 Depth 2
                                        #     Child Loop BB29_28 Depth 2
	xorl	%esi, %esi
	testq	%rdx, %rdx
	je	.LBB29_27
	.p2align	4, 0x90
.LBB29_26:                              #   Parent Loop BB29_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rbp,8), %rdi
	movb	$0, (%rdi,%rsi)
	incq	%rsi
	cmpq	%rsi, %rdx
	jne	.LBB29_26
.LBB29_27:                              # %.prol.loopexit320
                                        #   in Loop: Header=BB29_25 Depth=1
	cmpq	$7, %rcx
	jb	.LBB29_29
	.p2align	4, 0x90
.LBB29_28:                              #   Parent Loop BB29_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rbp,8), %rdi
	movb	$0, (%rdi,%rsi)
	movq	(%rax,%rbp,8), %rdi
	movb	$0, 1(%rdi,%rsi)
	movq	(%rax,%rbp,8), %rdi
	movb	$0, 2(%rdi,%rsi)
	movq	(%rax,%rbp,8), %rdi
	movb	$0, 3(%rdi,%rsi)
	movq	(%rax,%rbp,8), %rdi
	movb	$0, 4(%rdi,%rsi)
	movq	(%rax,%rbp,8), %rdi
	movb	$0, 5(%rdi,%rsi)
	movq	(%rax,%rbp,8), %rdi
	movb	$0, 6(%rdi,%rsi)
	movq	(%rax,%rbp,8), %rdi
	movb	$0, 7(%rdi,%rsi)
	addq	$8, %rsi
	cmpq	%rsi, %r13
	jne	.LBB29_28
.LBB29_29:                              # %._crit_edge236.us
                                        #   in Loop: Header=BB29_25 Depth=1
	incq	%rbp
	cmpq	%r13, %rbp
	jne	.LBB29_25
# BB#30:                                # %.preheader187
	testl	%r14d, %r14d
	jle	.LBB29_15
# BB#31:                                # %.lr.ph232
	movq	fastsupg.pair(%rip), %rax
	leaq	-1(%r13), %rdx
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	andq	$7, %rsi
	je	.LBB29_33
	.p2align	4, 0x90
.LBB29_32:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rcx,8), %rdi
	movb	$1, (%rdi,%rcx)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB29_32
.LBB29_33:                              # %.prol.loopexit315
	cmpq	$7, %rdx
	jb	.LBB29_35
	.p2align	4, 0x90
.LBB29_34:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rcx,8), %rdx
	movb	$1, (%rdx,%rcx)
	movq	8(%rax,%rcx,8), %rdx
	movb	$1, 1(%rdx,%rcx)
	movq	16(%rax,%rcx,8), %rdx
	movb	$1, 2(%rdx,%rcx)
	movq	24(%rax,%rcx,8), %rdx
	movb	$1, 3(%rdx,%rcx)
	movq	32(%rax,%rcx,8), %rdx
	movb	$1, 4(%rdx,%rcx)
	movq	40(%rax,%rcx,8), %rdx
	movb	$1, 5(%rdx,%rcx)
	movq	48(%rax,%rcx,8), %rdx
	movb	$1, 6(%rdx,%rcx)
	movq	56(%rax,%rcx,8), %rdx
	movb	$1, 7(%rdx,%rcx)
	addq	$8, %rcx
	cmpq	%rcx, %r13
	jne	.LBB29_34
.LBB29_35:                              # %._crit_edge233
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	%r14, 40(%rsp)          # 8-byte Spill
	cmpl	$2, %r14d
	movq	24(%rsp), %r10          # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	jl	.LBB29_96
# BB#36:                                # %.lr.ph229.preheader
	leaq	-1(%r13), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%r13d, %r15d
	andl	$3, %r15d
	movl	%r13d, %eax
	andl	$1, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%r13d, %eax
	andl	$7, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%r13, %rax
	negq	%rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$-1, %r12d
	movss	.LCPI29_0(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	movss	.LCPI29_1(%rip), %xmm5  # xmm5 = mem[0],zero,zero,zero
	movsd	.LCPI29_2(%rip), %xmm6  # xmm6 = mem[0],zero
	movsd	.LCPI29_3(%rip), %xmm7  # xmm7 = mem[0],zero
	movsd	.LCPI29_4(%rip), %xmm8  # xmm8 = mem[0],zero
	xorl	%edx, %edx
	movl	$-1, %r14d
	jmp	.LBB29_37
.LBB29_38:                              #   in Loop: Header=BB29_37 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.24, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	40(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movq	(%rsp), %rdx            # 8-byte Reload
	movsd	.LCPI29_4(%rip), %xmm8  # xmm8 = mem[0],zero
	movsd	.LCPI29_3(%rip), %xmm7  # xmm7 = mem[0],zero
	movsd	.LCPI29_2(%rip), %xmm6  # xmm6 = mem[0],zero
	movss	.LCPI29_1(%rip), %xmm5  # xmm5 = mem[0],zero,zero,zero
	movss	.LCPI29_0(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	movq	16(%rsp), %r11          # 8-byte Reload
	movq	24(%rsp), %r10          # 8-byte Reload
	jmp	.LBB29_39
	.p2align	4, 0x90
.LBB29_37:                              # %.lr.ph229
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB29_42 Depth 2
                                        #       Child Loop BB29_43 Depth 3
                                        #     Child Loop BB29_48 Depth 2
                                        #     Child Loop BB29_53 Depth 2
                                        #     Child Loop BB29_65 Depth 2
                                        #     Child Loop BB29_70 Depth 2
                                        #     Child Loop BB29_97 Depth 2
                                        #     Child Loop BB29_86 Depth 2
                                        #     Child Loop BB29_98 Depth 2
                                        #     Child Loop BB29_89 Depth 2
	movslq	%edx, %rax
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rsi
	shrq	$63, %rsi
	sarq	$34, %rcx
	addl	%esi, %ecx
	addl	%ecx, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	movq	%rdx, (%rsp)            # 8-byte Spill
	je	.LBB29_38
.LBB29_39:                              # %.preheader186
                                        #   in Loop: Header=BB29_37 Depth=1
	movq	fastsupg.ac(%rip), %rax
	movl	(%rax), %edi
	cmpl	$-1, %edi
	je	.LBB29_40
# BB#41:                                # %.preheader.lr.ph
                                        #   in Loop: Header=BB29_37 Depth=1
	xorl	%ebp, %ebp
	movq	fastsupg.eff(%rip), %r8
	movaps	%xmm4, %xmm0
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB29_42:                              # %.lr.ph
                                        #   Parent Loop BB29_37 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB29_43 Depth 3
	movl	%edi, %ecx
	movq	(%r8,%rbp,8), %rdi
	movl	%ecx, %esi
	.p2align	4, 0x90
.LBB29_43:                              #   Parent Loop BB29_37 Depth=1
                                        #     Parent Loop BB29_42 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movaps	%xmm0, %xmm1
	movslq	%esi, %rsi
	movss	(%rdi,%rsi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	minss	%xmm1, %xmm0
	cmoval	%ebx, %r14d
	cmoval	%esi, %r12d
	movl	(%rax,%rsi,8), %esi
	cmpl	$-1, %esi
	jne	.LBB29_43
# BB#44:                                # %._crit_edge
                                        #   in Loop: Header=BB29_42 Depth=2
	movslq	%ecx, %rbp
	movl	(%rax,%rbp,8), %edi
	cmpl	$-1, %edi
	movl	%ecx, %ebx
	jne	.LBB29_42
	jmp	.LBB29_45
	.p2align	4, 0x90
.LBB29_40:                              #   in Loop: Header=BB29_37 Depth=1
	movaps	%xmm4, %xmm0
.LBB29_45:                              # %.lr.ph210
                                        #   in Loop: Header=BB29_37 Depth=1
	testq	%r15, %r15
	movq	(%r11,%rdx,8), %r8
	movq	(%r8), %rdx
	movq	fastsupg.pair(%rip), %rax
	movslq	%r14d, %r9
	movq	(%rax,%r9,8), %rax
	je	.LBB29_46
# BB#47:                                # %.prol.preheader
                                        #   in Loop: Header=BB29_37 Depth=1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB29_48:                              #   Parent Loop BB29_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, (%rax,%rsi)
	jle	.LBB29_50
# BB#49:                                #   in Loop: Header=BB29_48 Depth=2
	movl	%esi, (%rdx)
	addq	$4, %rdx
.LBB29_50:                              #   in Loop: Header=BB29_48 Depth=2
	incq	%rsi
	cmpq	%rsi, %r15
	jne	.LBB29_48
	jmp	.LBB29_51
	.p2align	4, 0x90
.LBB29_46:                              #   in Loop: Header=BB29_37 Depth=1
	xorl	%esi, %esi
.LBB29_51:                              # %.prol.loopexit
                                        #   in Loop: Header=BB29_37 Depth=1
	cmpq	$3, 8(%rsp)             # 8-byte Folded Reload
	jb	.LBB29_62
# BB#52:                                # %.lr.ph210.new
                                        #   in Loop: Header=BB29_37 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rsi), %rdi
	leaq	3(%rax,%rsi), %rcx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB29_53:                              #   Parent Loop BB29_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, -3(%rcx,%rbp)
	jle	.LBB29_55
# BB#54:                                #   in Loop: Header=BB29_53 Depth=2
	leal	(%rsi,%rbp), %eax
	movl	%eax, (%rdx)
	addq	$4, %rdx
.LBB29_55:                              #   in Loop: Header=BB29_53 Depth=2
	cmpb	$0, -2(%rcx,%rbp)
	jle	.LBB29_57
# BB#56:                                #   in Loop: Header=BB29_53 Depth=2
	leal	1(%rsi,%rbp), %eax
	movl	%eax, (%rdx)
	addq	$4, %rdx
.LBB29_57:                              #   in Loop: Header=BB29_53 Depth=2
	cmpb	$0, -1(%rcx,%rbp)
	jle	.LBB29_59
# BB#58:                                #   in Loop: Header=BB29_53 Depth=2
	leal	2(%rsi,%rbp), %eax
	movl	%eax, (%rdx)
	addq	$4, %rdx
.LBB29_59:                              #   in Loop: Header=BB29_53 Depth=2
	cmpb	$0, (%rcx,%rbp)
	jle	.LBB29_61
# BB#60:                                #   in Loop: Header=BB29_53 Depth=2
	leal	3(%rsi,%rbp), %eax
	movl	%eax, (%rdx)
	addq	$4, %rdx
.LBB29_61:                              #   in Loop: Header=BB29_53 Depth=2
	addq	$4, %rbp
	movq	%rdi, %rax
	addq	%rbp, %rax
	jne	.LBB29_53
.LBB29_62:                              # %.lr.ph216
                                        #   in Loop: Header=BB29_37 Depth=1
	testq	%r15, %r15
	movl	$-1, (%rdx)
	movq	8(%r8), %rdx
	movq	fastsupg.pair(%rip), %rax
	movslq	%r12d, %rcx
	movq	(%rax,%rcx,8), %rax
	je	.LBB29_63
# BB#64:                                # %.prol.preheader300
                                        #   in Loop: Header=BB29_37 Depth=1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB29_65:                              #   Parent Loop BB29_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, (%rax,%rsi)
	jle	.LBB29_67
# BB#66:                                #   in Loop: Header=BB29_65 Depth=2
	movl	%esi, (%rdx)
	addq	$4, %rdx
.LBB29_67:                              #   in Loop: Header=BB29_65 Depth=2
	incq	%rsi
	cmpq	%rsi, %r15
	jne	.LBB29_65
	jmp	.LBB29_68
	.p2align	4, 0x90
.LBB29_63:                              #   in Loop: Header=BB29_37 Depth=1
	xorl	%esi, %esi
.LBB29_68:                              # %.prol.loopexit301
                                        #   in Loop: Header=BB29_37 Depth=1
	cmpq	$3, 8(%rsp)             # 8-byte Folded Reload
	jb	.LBB29_79
# BB#69:                                # %.lr.ph216.new
                                        #   in Loop: Header=BB29_37 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rsi), %r8
	leaq	3(%rax,%rsi), %rbp
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB29_70:                              #   Parent Loop BB29_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, -3(%rbp,%rax)
	jle	.LBB29_72
# BB#71:                                #   in Loop: Header=BB29_70 Depth=2
	leal	(%rsi,%rax), %ebx
	movl	%ebx, (%rdx)
	addq	$4, %rdx
.LBB29_72:                              #   in Loop: Header=BB29_70 Depth=2
	cmpb	$0, -2(%rbp,%rax)
	jle	.LBB29_74
# BB#73:                                #   in Loop: Header=BB29_70 Depth=2
	leal	1(%rsi,%rax), %ebx
	movl	%ebx, (%rdx)
	addq	$4, %rdx
.LBB29_74:                              #   in Loop: Header=BB29_70 Depth=2
	cmpb	$0, -1(%rbp,%rax)
	jle	.LBB29_76
# BB#75:                                #   in Loop: Header=BB29_70 Depth=2
	leal	2(%rsi,%rax), %ebx
	movl	%ebx, (%rdx)
	addq	$4, %rdx
.LBB29_76:                              #   in Loop: Header=BB29_70 Depth=2
	cmpb	$0, (%rbp,%rax)
	jle	.LBB29_78
# BB#77:                                #   in Loop: Header=BB29_70 Depth=2
	leal	3(%rsi,%rax), %ebx
	movl	%ebx, (%rdx)
	addq	$4, %rdx
.LBB29_78:                              #   in Loop: Header=BB29_70 Depth=2
	addq	$4, %rax
	movq	%r8, %rdi
	addq	%rax, %rdi
	jne	.LBB29_70
.LBB29_79:                              # %.lr.ph221
                                        #   in Loop: Header=BB29_37 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	movl	$-1, (%rdx)
	mulss	%xmm5, %xmm0
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm0, %xmm1
	movq	fastsupg.tmplen(%rip), %rax
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	(%r10,%rdx,8), %rdx
	movss	(%rax,%rcx,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movss	(%rax,%r9,4), %xmm3     # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	cvtps2pd	%xmm3, %xmm2
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	subpd	%xmm2, %xmm1
	movupd	%xmm1, (%rdx)
	movss	%xmm0, (%rax,%r9,4)
	movq	fastsupg.pair(%rip), %rax
	jne	.LBB29_81
# BB#80:                                #   in Loop: Header=BB29_37 Depth=1
	xorl	%edx, %edx
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	jne	.LBB29_97
	jmp	.LBB29_83
	.p2align	4, 0x90
.LBB29_81:                              #   in Loop: Header=BB29_37 Depth=1
	movq	(%rax,%rcx,8), %rdx
	cmpb	$0, (%rdx)
	setg	%dl
	movq	(%rax,%r9,8), %rsi
	addb	%dl, (%rsi)
	movl	$1, %edx
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB29_83
	.p2align	4, 0x90
.LBB29_97:                              #   Parent Loop BB29_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rcx,8), %rsi
	cmpb	$0, (%rsi,%rdx)
	setg	%bl
	movq	(%rax,%r9,8), %rsi
	addb	%bl, (%rsi,%rdx)
	movq	(%rax,%rcx,8), %rsi
	cmpb	$0, 1(%rsi,%rdx)
	setg	%bl
	movq	(%rax,%r9,8), %rsi
	addb	%bl, 1(%rsi,%rdx)
	addq	$2, %rdx
	cmpq	%rdx, %r13
	jne	.LBB29_97
.LBB29_83:                              # %.lr.ph223
                                        #   in Loop: Header=BB29_37 Depth=1
	movq	48(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	movq	fastsupg.pair(%rip), %rax
	je	.LBB29_84
# BB#85:                                # %.prol.preheader309
                                        #   in Loop: Header=BB29_37 Depth=1
	xorl	%ebp, %ebp
	movq	(%rsp), %rdx            # 8-byte Reload
	.p2align	4, 0x90
.LBB29_86:                              #   Parent Loop BB29_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rcx,8), %rsi
	movb	$0, (%rsi,%rbp)
	incq	%rbp
	cmpq	%rbp, %rdi
	jne	.LBB29_86
	jmp	.LBB29_87
	.p2align	4, 0x90
.LBB29_84:                              #   in Loop: Header=BB29_37 Depth=1
	xorl	%ebp, %ebp
	movq	(%rsp), %rdx            # 8-byte Reload
.LBB29_87:                              # %.prol.loopexit310
                                        #   in Loop: Header=BB29_37 Depth=1
	cmpq	$7, 8(%rsp)             # 8-byte Folded Reload
	jb	.LBB29_88
	.p2align	4, 0x90
.LBB29_98:                              #   Parent Loop BB29_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rcx,8), %rsi
	movb	$0, (%rsi,%rbp)
	movq	(%rax,%rcx,8), %rsi
	movb	$0, 1(%rsi,%rbp)
	movq	(%rax,%rcx,8), %rsi
	movb	$0, 2(%rsi,%rbp)
	movq	(%rax,%rcx,8), %rsi
	movb	$0, 3(%rsi,%rbp)
	movq	(%rax,%rcx,8), %rsi
	movb	$0, 4(%rsi,%rbp)
	movq	(%rax,%rcx,8), %rsi
	movb	$0, 5(%rsi,%rbp)
	movq	(%rax,%rcx,8), %rsi
	movb	$0, 6(%rsi,%rbp)
	movq	(%rax,%rcx,8), %rsi
	movb	$0, 7(%rsi,%rbp)
	addq	$8, %rbp
	cmpq	%rbp, %r13
	jne	.LBB29_98
.LBB29_88:                              # %.preheader184
                                        #   in Loop: Header=BB29_37 Depth=1
	movq	fastsupg.ac(%rip), %r9
	xorl	%esi, %esi
	movq	fastsupg.eff(%rip), %r8
	.p2align	4, 0x90
.LBB29_89:                              #   Parent Loop BB29_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%r14d, %esi
	je	.LBB29_94
# BB#90:                                #   in Loop: Header=BB29_89 Depth=2
	cmpl	%r12d, %esi
	je	.LBB29_94
# BB#91:                                #   in Loop: Header=BB29_89 Depth=2
	cmpl	%r14d, %esi
	movl	%esi, %ebx
	movl	%r14d, %eax
	movl	%esi, %edi
	movl	%r12d, %ebp
	jl	.LBB29_93
# BB#92:                                #   in Loop: Header=BB29_89 Depth=2
	cmpl	%r12d, %esi
	movl	%r12d, %edi
	cmovlel	%esi, %edi
	movl	%esi, %ebp
	cmovll	%r12d, %ebp
	movl	%r14d, %ebx
	movl	%esi, %eax
.LBB29_93:                              #   in Loop: Header=BB29_89 Depth=2
	movslq	%ebx, %rbx
	movq	(%r8,%rbx,8), %rbx
	cltq
	movss	(%rbx,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movslq	%edi, %rdi
	movq	(%r8,%rdi,8), %rdi
	movslq	%ebp, %rbp
	movss	(%rdi,%rbp,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	minss	%xmm1, %xmm2
	cvtss2sd	%xmm2, %xmm2
	mulsd	%xmm6, %xmm2
	addss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	mulsd	%xmm7, %xmm0
	mulsd	%xmm8, %xmm0
	addsd	%xmm2, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rbx,%rax,4)
.LBB29_94:                              #   in Loop: Header=BB29_89 Depth=2
	movslq	%esi, %rax
	movl	(%r9,%rax,8), %esi
	cmpl	$-1, %esi
	jne	.LBB29_89
# BB#95:                                #   in Loop: Header=BB29_37 Depth=1
	movl	(%r9,%rcx,8), %eax
	movslq	4(%r9,%rcx,8), %rsi
	movl	%eax, (%r9,%rsi,8)
	movslq	(%r9,%rcx,8), %rcx
	movl	%esi, 4(%r9,%rcx,8)
	incq	%rdx
	cmpq	64(%rsp), %rdx          # 8-byte Folded Reload
	jne	.LBB29_37
	jmp	.LBB29_96
.LBB29_14:                              # %._crit_edge243.thread
	movq	fastsupg.ac(%rip), %rax
	movslq	%r14d, %rcx
	movl	$-1, -8(%rax,%rcx,8)
.LBB29_15:                              # %._crit_edge233.thread
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
.LBB29_96:                              # %._crit_edge230
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fputc                   # TAILCALL
.Lfunc_end29:
	.size	fastsupg, .Lfunc_end29-fastsupg
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI30_0:
	.long	1176255488              # float 9999
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI30_1:
	.quad	4602678819172646912     # double 0.5
.LCPI30_2:
	.quad	4606281698874543309     # double 0.90000000000000002
.LCPI30_3:
	.quad	4591870180066957722     # double 0.10000000000000001
	.text
	.globl	supg
	.p2align	4, 0x90
	.type	supg,@function
supg:                                   # @supg
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi262:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi263:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi264:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi265:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi266:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi267:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi268:
	.cfi_def_cfa_offset 176
.Lcfi269:
	.cfi_offset %rbx, -56
.Lcfi270:
	.cfi_offset %r12, -48
.Lcfi271:
	.cfi_offset %r13, -40
.Lcfi272:
	.cfi_offset %r14, -32
.Lcfi273:
	.cfi_offset %r15, -24
.Lcfi274:
	.cfi_offset %rbp, -16
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movq	%rsi, %r12
	movl	%edi, %r13d
	cmpq	$0, supg.eff(%rip)
	jne	.LBB30_2
# BB#1:
	movl	njob(%rip), %edi
	movl	%edi, %esi
	callq	AllocateFloatMtx
	movq	%rax, supg.eff(%rip)
	movl	njob(%rip), %edi
	movl	%edi, %esi
	callq	AllocateCharMtx
	movq	%rax, supg.pair(%rip)
	movl	njob(%rip), %edi
	callq	AllocateFloatVec
	movq	%rax, supg.tmplen(%rip)
.LBB30_2:                               # %.preheader184
	testl	%r13d, %r13d
	jle	.LBB30_87
# BB#3:                                 # %.preheader183.lr.ph
	movq	supg.eff(%rip), %r9
	movl	%r13d, %eax
	movl	%r13d, %r8d
	andl	$3, %r8d
	movq	%rax, %r10
	subq	%r8, %r10
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB30_4:                               # %.preheader183.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_9 Depth 2
                                        #     Child Loop BB30_11 Depth 2
	cmpl	$4, %r13d
	movq	(%r12,%rsi,8), %rdi
	movq	(%r9,%rsi,8), %rbp
	jae	.LBB30_6
# BB#5:                                 #   in Loop: Header=BB30_4 Depth=1
	xorl	%edx, %edx
	jmp	.LBB30_11
	.p2align	4, 0x90
.LBB30_6:                               # %min.iters.checked
                                        #   in Loop: Header=BB30_4 Depth=1
	testq	%r10, %r10
	je	.LBB30_7
# BB#8:                                 # %vector.body.preheader
                                        #   in Loop: Header=BB30_4 Depth=1
	leaq	16(%rdi), %rdx
	leaq	8(%rbp), %rcx
	movq	%r10, %rbx
	.p2align	4, 0x90
.LBB30_9:                               # %vector.body
                                        #   Parent Loop BB30_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-16(%rdx), %xmm0
	movupd	(%rdx), %xmm1
	cvtpd2ps	%xmm0, %xmm0
	cvtpd2ps	%xmm1, %xmm1
	movlpd	%xmm0, -8(%rcx)
	movlpd	%xmm1, (%rcx)
	addq	$32, %rdx
	addq	$16, %rcx
	addq	$-4, %rbx
	jne	.LBB30_9
# BB#10:                                # %middle.block
                                        #   in Loop: Header=BB30_4 Depth=1
	testl	%r8d, %r8d
	movq	%r10, %rdx
	jne	.LBB30_11
	jmp	.LBB30_12
.LBB30_7:                               #   in Loop: Header=BB30_4 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB30_11:                              # %scalar.ph
                                        #   Parent Loop BB30_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rbp,%rdx,4)
	incq	%rdx
	cmpq	%rdx, %rax
	jne	.LBB30_11
.LBB30_12:                              # %._crit_edge235.us
                                        #   in Loop: Header=BB30_4 Depth=1
	incq	%rsi
	cmpq	%rdx, %rsi
	jne	.LBB30_4
# BB#13:                                # %.preheader182
	testl	%r13d, %r13d
	jle	.LBB30_87
# BB#14:                                # %.preheader180.lr.ph
	movq	supg.tmplen(%rip), %rdi
	leal	-1(%r13), %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leaq	4(,%rcx,4), %rdx
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movq	%rax, 48(%rsp)          # 8-byte Spill
	callq	memset
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	supg.pair(%rip), %rax
	leaq	-1(%rbp), %rcx
	movl	%ebp, %edx
	andl	$7, %edx
	.p2align	4, 0x90
.LBB30_15:                              # %.preheader180.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_16 Depth 2
                                        #     Child Loop BB30_18 Depth 2
	xorl	%esi, %esi
	testq	%rdx, %rdx
	je	.LBB30_17
	.p2align	4, 0x90
.LBB30_16:                              #   Parent Loop BB30_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rbx,8), %rdi
	movb	$0, (%rdi,%rsi)
	incq	%rsi
	cmpq	%rsi, %rdx
	jne	.LBB30_16
.LBB30_17:                              # %.prol.loopexit314
                                        #   in Loop: Header=BB30_15 Depth=1
	cmpq	$7, %rcx
	jb	.LBB30_19
	.p2align	4, 0x90
.LBB30_18:                              #   Parent Loop BB30_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rbx,8), %rdi
	movb	$0, (%rdi,%rsi)
	movq	(%rax,%rbx,8), %rdi
	movb	$0, 1(%rdi,%rsi)
	movq	(%rax,%rbx,8), %rdi
	movb	$0, 2(%rdi,%rsi)
	movq	(%rax,%rbx,8), %rdi
	movb	$0, 3(%rdi,%rsi)
	movq	(%rax,%rbx,8), %rdi
	movb	$0, 4(%rdi,%rsi)
	movq	(%rax,%rbx,8), %rdi
	movb	$0, 5(%rdi,%rsi)
	movq	(%rax,%rbx,8), %rdi
	movb	$0, 6(%rdi,%rsi)
	movq	(%rax,%rbx,8), %rdi
	movb	$0, 7(%rdi,%rsi)
	addq	$8, %rsi
	cmpq	%rsi, %rbp
	jne	.LBB30_18
.LBB30_19:                              # %._crit_edge229.us
                                        #   in Loop: Header=BB30_15 Depth=1
	incq	%rbx
	cmpq	%rbp, %rbx
	jne	.LBB30_15
# BB#20:                                # %.preheader179
	testl	%r13d, %r13d
	jle	.LBB30_87
# BB#21:                                # %.lr.ph226
	movq	supg.pair(%rip), %rax
	leaq	-1(%rbp), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %rdx
	xorl	%ecx, %ecx
	andq	$7, %rdx
	je	.LBB30_23
	.p2align	4, 0x90
.LBB30_22:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rcx,8), %rsi
	movb	$1, (%rsi,%rcx)
	incq	%rcx
	cmpq	%rcx, %rdx
	jne	.LBB30_22
.LBB30_23:                              # %.prol.loopexit309
	cmpq	$7, 8(%rsp)             # 8-byte Folded Reload
	jb	.LBB30_24
	.p2align	4, 0x90
.LBB30_88:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rcx,8), %rdx
	movb	$1, (%rdx,%rcx)
	movq	8(%rax,%rcx,8), %rdx
	movb	$1, 1(%rdx,%rcx)
	movq	16(%rax,%rcx,8), %rdx
	movb	$1, 2(%rdx,%rcx)
	movq	24(%rax,%rcx,8), %rdx
	movb	$1, 3(%rdx,%rcx)
	movq	32(%rax,%rcx,8), %rdx
	movb	$1, 4(%rdx,%rcx)
	movq	40(%rax,%rcx,8), %rdx
	movb	$1, 5(%rdx,%rcx)
	movq	48(%rax,%rcx,8), %rdx
	movb	$1, 6(%rdx,%rcx)
	movq	56(%rax,%rcx,8), %rdx
	movb	$1, 7(%rdx,%rcx)
	addq	$8, %rcx
	cmpq	%rcx, %rbp
	jne	.LBB30_88
.LBB30_24:                              # %.preheader178
	cmpl	$2, %r13d
	jl	.LBB30_87
# BB#25:                                # %.lr.ph223
	movq	supg.eff(%rip), %r11
	movq	supg.tmplen(%rip), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	supg.pair(%rip), %r14
	movslq	%r13d, %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	40(%rsp), %eax          # 4-byte Reload
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leal	-2(%r13), %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	%ebp, %eax
	andl	$3, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	%ebp, %ebx
	andl	$7, %ebx
	movq	8(%rsp), %rax           # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	negq	%rbp
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movss	.LCPI30_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movsd	.LCPI30_1(%rip), %xmm1  # xmm1 = mem[0],zero
	movsd	.LCPI30_2(%rip), %xmm2  # xmm2 = mem[0],zero
	movsd	.LCPI30_3(%rip), %xmm3  # xmm3 = mem[0],zero
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB30_26:                              # %.lr.ph198.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_28 Depth 2
                                        #       Child Loop BB30_32 Depth 3
                                        #       Child Loop BB30_36 Depth 3
                                        #     Child Loop BB30_40 Depth 2
                                        #     Child Loop BB30_45 Depth 2
                                        #     Child Loop BB30_57 Depth 2
                                        #     Child Loop BB30_62 Depth 2
                                        #     Child Loop BB30_89 Depth 2
                                        #     Child Loop BB30_77 Depth 2
                                        #     Child Loop BB30_90 Depth 2
                                        #     Child Loop BB30_80 Depth 2
	movl	$-1, %r12d
	movq	40(%rsp), %rbp          # 8-byte Reload
                                        # kill: %BPL<def> %BPL<kill> %RBP<kill> %RBP<def>
	xorl	%r10d, %r10d
	movl	$-1, %r15d
	movaps	%xmm0, %xmm4
	movq	%r11, %r9
	.p2align	4, 0x90
.LBB30_28:                              # %.lr.ph198
                                        #   Parent Loop BB30_26 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB30_32 Depth 3
                                        #       Child Loop BB30_36 Depth 3
	movq	%r10, %rdi
	andb	$3, %bpl
	movzbl	%bpl, %ebp
	leaq	1(%rdi), %r10
	cmpq	112(%rsp), %r10         # 8-byte Folded Reload
	jge	.LBB30_27
# BB#29:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB30_28 Depth=2
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	%eax, %r8d
	subl	%edi, %r8d
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, %edx
	subl	%edi, %edx
	leaq	(,%rdi,4), %rax
	addq	(%r9), %rax
	testb	$3, %dl
	je	.LBB30_30
# BB#31:                                # %.lr.ph.prol.preheader
                                        #   in Loop: Header=BB30_28 Depth=2
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB30_32:                              # %.lr.ph.prol
                                        #   Parent Loop BB30_26 Depth=1
                                        #     Parent Loop BB30_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movaps	%xmm4, %xmm5
	leal	1(%rdi,%rdx), %ebx
	movss	4(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	addq	$4, %rax
	ucomiss	%xmm4, %xmm5
	minss	%xmm5, %xmm4
	cmoval	%edi, %r15d
	cmoval	%ebx, %r12d
	incq	%rdx
	cmpl	%edx, %ebp
	jne	.LBB30_32
# BB#33:                                # %.lr.ph.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB30_28 Depth=2
	leal	1(%rdi,%rdx), %edx
	movl	%r12d, %ebx
	cmpl	$3, %r8d
	jae	.LBB30_35
	jmp	.LBB30_27
.LBB30_30:                              #   in Loop: Header=BB30_28 Depth=2
	movl	%r12d, %ebx
	movl	%r10d, %edx
	cmpl	$3, %r8d
	jb	.LBB30_27
.LBB30_35:                              # %.lr.ph.preheader.new
                                        #   in Loop: Header=BB30_28 Depth=2
	addq	$16, %rax
	.p2align	4, 0x90
.LBB30_36:                              # %.lr.ph
                                        #   Parent Loop BB30_26 Depth=1
                                        #     Parent Loop BB30_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	-12(%rax), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	-8(%rax), %xmm6         # xmm6 = mem[0],zero,zero,zero
	leal	1(%rdx), %esi
	leal	2(%rdx), %ecx
	ucomiss	%xmm5, %xmm4
	minss	%xmm4, %xmm5
	cmoval	%edx, %ebx
	cmoval	%edi, %r15d
	ucomiss	%xmm6, %xmm5
	minss	%xmm5, %xmm6
	cmoval	%esi, %ebx
	movss	-4(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	cmoval	%edi, %r15d
	ucomiss	%xmm5, %xmm6
	minss	%xmm6, %xmm5
	cmoval	%ecx, %ebx
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cmoval	%edi, %r15d
	leal	3(%rdx), %r12d
	ucomiss	%xmm4, %xmm5
	minss	%xmm5, %xmm4
	cmoval	%edi, %r15d
	cmovbel	%ebx, %r12d
	addq	$16, %rax
	addl	$4, %edx
	cmpl	%r13d, %edx
	movl	%r12d, %ebx
	jne	.LBB30_36
.LBB30_27:                              # %.loopexit
                                        #   in Loop: Header=BB30_28 Depth=2
	addq	$8, %r9
	addb	$3, %bpl
	cmpq	24(%rsp), %r10          # 8-byte Folded Reload
	jne	.LBB30_28
# BB#37:                                # %.lr.ph205
                                        #   in Loop: Header=BB30_26 Depth=1
	movq	72(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %r8
	movq	(%r8), %rcx
	movslq	%r15d, %r9
	movq	(%r14,%r9,8), %rdi
	je	.LBB30_38
# BB#39:                                # %.prol.preheader
                                        #   in Loop: Header=BB30_26 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB30_40:                              #   Parent Loop BB30_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, (%rdi,%rdx)
	jle	.LBB30_42
# BB#41:                                #   in Loop: Header=BB30_40 Depth=2
	movl	%edx, (%rcx)
	addq	$4, %rcx
.LBB30_42:                              #   in Loop: Header=BB30_40 Depth=2
	incq	%rdx
	cmpq	%rdx, %rbp
	jne	.LBB30_40
	jmp	.LBB30_43
	.p2align	4, 0x90
.LBB30_38:                              #   in Loop: Header=BB30_26 Depth=1
	xorl	%edx, %edx
.LBB30_43:                              # %.prol.loopexit
                                        #   in Loop: Header=BB30_26 Depth=1
	cmpq	$3, 8(%rsp)             # 8-byte Folded Reload
	jb	.LBB30_54
# BB#44:                                # %.lr.ph205.new
                                        #   in Loop: Header=BB30_26 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx), %rsi
	leaq	3(%rdi,%rdx), %rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB30_45:                              #   Parent Loop BB30_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, -3(%rdi,%rbx)
	jle	.LBB30_47
# BB#46:                                #   in Loop: Header=BB30_45 Depth=2
	leal	(%rdx,%rbx), %eax
	movl	%eax, (%rcx)
	addq	$4, %rcx
.LBB30_47:                              #   in Loop: Header=BB30_45 Depth=2
	cmpb	$0, -2(%rdi,%rbx)
	jle	.LBB30_49
# BB#48:                                #   in Loop: Header=BB30_45 Depth=2
	leal	1(%rdx,%rbx), %eax
	movl	%eax, (%rcx)
	addq	$4, %rcx
.LBB30_49:                              #   in Loop: Header=BB30_45 Depth=2
	cmpb	$0, -1(%rdi,%rbx)
	jle	.LBB30_51
# BB#50:                                #   in Loop: Header=BB30_45 Depth=2
	leal	2(%rdx,%rbx), %eax
	movl	%eax, (%rcx)
	addq	$4, %rcx
.LBB30_51:                              #   in Loop: Header=BB30_45 Depth=2
	cmpb	$0, (%rdi,%rbx)
	jle	.LBB30_53
# BB#52:                                #   in Loop: Header=BB30_45 Depth=2
	leal	3(%rdx,%rbx), %eax
	movl	%eax, (%rcx)
	addq	$4, %rcx
.LBB30_53:                              #   in Loop: Header=BB30_45 Depth=2
	addq	$4, %rbx
	movq	%rsi, %rax
	addq	%rbx, %rax
	jne	.LBB30_45
.LBB30_54:                              # %.lr.ph211
                                        #   in Loop: Header=BB30_26 Depth=1
	testq	%rbp, %rbp
	movl	$-1, (%rcx)
	movq	8(%r8), %rcx
	movslq	%r12d, %r8
	movq	(%r14,%r8,8), %r10
	je	.LBB30_55
# BB#56:                                # %.prol.preheader296
                                        #   in Loop: Header=BB30_26 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB30_57:                              #   Parent Loop BB30_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, (%r10,%rdx)
	jle	.LBB30_59
# BB#58:                                #   in Loop: Header=BB30_57 Depth=2
	movl	%edx, (%rcx)
	addq	$4, %rcx
.LBB30_59:                              #   in Loop: Header=BB30_57 Depth=2
	incq	%rdx
	cmpq	%rdx, %rbp
	jne	.LBB30_57
	jmp	.LBB30_60
	.p2align	4, 0x90
.LBB30_55:                              #   in Loop: Header=BB30_26 Depth=1
	xorl	%edx, %edx
.LBB30_60:                              # %.prol.loopexit297
                                        #   in Loop: Header=BB30_26 Depth=1
	cmpq	$3, 8(%rsp)             # 8-byte Folded Reload
	jb	.LBB30_71
# BB#61:                                # %.lr.ph211.new
                                        #   in Loop: Header=BB30_26 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx), %rax
	leaq	3(%r10,%rdx), %rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB30_62:                              #   Parent Loop BB30_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, -3(%rdi,%rbx)
	jle	.LBB30_64
# BB#63:                                #   in Loop: Header=BB30_62 Depth=2
	leal	(%rdx,%rbx), %ebp
	movl	%ebp, (%rcx)
	addq	$4, %rcx
.LBB30_64:                              #   in Loop: Header=BB30_62 Depth=2
	cmpb	$0, -2(%rdi,%rbx)
	jle	.LBB30_66
# BB#65:                                #   in Loop: Header=BB30_62 Depth=2
	leal	1(%rdx,%rbx), %ebp
	movl	%ebp, (%rcx)
	addq	$4, %rcx
.LBB30_66:                              #   in Loop: Header=BB30_62 Depth=2
	cmpb	$0, -1(%rdi,%rbx)
	jle	.LBB30_68
# BB#67:                                #   in Loop: Header=BB30_62 Depth=2
	leal	2(%rdx,%rbx), %ebp
	movl	%ebp, (%rcx)
	addq	$4, %rcx
.LBB30_68:                              #   in Loop: Header=BB30_62 Depth=2
	cmpb	$0, (%rdi,%rbx)
	jle	.LBB30_70
# BB#69:                                #   in Loop: Header=BB30_62 Depth=2
	leal	3(%rdx,%rbx), %ebp
	movl	%ebp, (%rcx)
	addq	$4, %rcx
.LBB30_70:                              #   in Loop: Header=BB30_62 Depth=2
	addq	$4, %rbx
	movq	%rax, %rsi
	addq	%rbx, %rsi
	jne	.LBB30_62
.LBB30_71:                              # %.lr.ph216
                                        #   in Loop: Header=BB30_26 Depth=1
	movq	80(%rsp), %rdx          # 8-byte Reload
	movss	(%rdx,%r8,4), %xmm5     # xmm5 = mem[0],zero,zero,zero
	movl	$-1, (%rcx)
	cvtss2sd	%xmm4, %xmm4
	mulsd	%xmm1, %xmm4
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rcx
	movss	(%rdx,%r9,4), %xmm6     # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	cvtps2pd	%xmm6, %xmm5
	xorps	%xmm6, %xmm6
	cvtsd2ss	%xmm4, %xmm6
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	subpd	%xmm5, %xmm4
	movupd	%xmm4, (%rcx)
	movss	%xmm6, (%rdx,%r9,4)
	cmpb	$0, (%r10)
	setg	%al
	movq	(%r14,%r9,8), %rcx
	addb	%al, (%rcx)
	cmpl	$1, %r13d
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %rbx          # 8-byte Reload
	je	.LBB30_76
# BB#72:                                # %._crit_edge285.preheader
                                        #   in Loop: Header=BB30_26 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	jne	.LBB30_74
# BB#73:                                #   in Loop: Header=BB30_26 Depth=1
	movl	$1, %eax
	cmpl	$2, %r13d
	jne	.LBB30_89
	jmp	.LBB30_76
	.p2align	4, 0x90
.LBB30_74:                              # %._crit_edge285.prol
                                        #   in Loop: Header=BB30_26 Depth=1
	movq	(%r14,%r8,8), %rax
	cmpb	$0, 1(%rax)
	setg	%al
	movq	(%r14,%r9,8), %rcx
	addb	%al, 1(%rcx)
	movl	$2, %eax
	cmpl	$2, %r13d
	je	.LBB30_76
	.p2align	4, 0x90
.LBB30_89:                              # %._crit_edge285
                                        #   Parent Loop BB30_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%r8,8), %rcx
	cmpb	$0, (%rcx,%rax)
	setg	%cl
	movq	(%r14,%r9,8), %rdx
	addb	%cl, (%rdx,%rax)
	movq	(%r14,%r8,8), %rcx
	cmpb	$0, 1(%rcx,%rax)
	setg	%cl
	movq	(%r14,%r9,8), %rdx
	addb	%cl, 1(%rdx,%rax)
	addq	$2, %rax
	cmpq	%rax, %rsi
	jne	.LBB30_89
.LBB30_76:                              # %.lr.ph218
                                        #   in Loop: Header=BB30_26 Depth=1
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB30_78
	.p2align	4, 0x90
.LBB30_77:                              #   Parent Loop BB30_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%r8,8), %rcx
	movb	$0, (%rcx,%rax)
	incq	%rax
	cmpq	%rax, %rbx
	jne	.LBB30_77
.LBB30_78:                              # %.prol.loopexit304
                                        #   in Loop: Header=BB30_26 Depth=1
	cmpq	$7, 8(%rsp)             # 8-byte Folded Reload
	jb	.LBB30_79
	.p2align	4, 0x90
.LBB30_90:                              #   Parent Loop BB30_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%r8,8), %rcx
	movb	$0, (%rcx,%rax)
	movq	(%r14,%r8,8), %rcx
	movb	$0, 1(%rcx,%rax)
	movq	(%r14,%r8,8), %rcx
	movb	$0, 2(%rcx,%rax)
	movq	(%r14,%r8,8), %rcx
	movb	$0, 3(%rcx,%rax)
	movq	(%r14,%r8,8), %rcx
	movb	$0, 4(%rcx,%rax)
	movq	(%r14,%r8,8), %rcx
	movb	$0, 5(%rcx,%rax)
	movq	(%r14,%r8,8), %rcx
	movb	$0, 6(%rcx,%rax)
	movq	(%r14,%r8,8), %rcx
	movb	$0, 7(%rcx,%rax)
	addq	$8, %rax
	cmpq	%rax, %rsi
	jne	.LBB30_90
.LBB30_79:                              # %.lr.ph220
                                        #   in Loop: Header=BB30_26 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB30_80:                              #   Parent Loop BB30_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%eax, %r15d
	je	.LBB30_85
# BB#81:                                #   in Loop: Header=BB30_80 Depth=2
	cmpl	%eax, %r12d
	je	.LBB30_85
# BB#82:                                #   in Loop: Header=BB30_80 Depth=2
	cmpl	%r15d, %eax
	movl	%eax, %esi
	movl	%r15d, %edi
	movl	%eax, %ecx
	movl	%r12d, %edx
	jl	.LBB30_84
# BB#83:                                #   in Loop: Header=BB30_80 Depth=2
	cmpl	%r12d, %eax
	movl	%r12d, %ecx
	cmovlel	%eax, %ecx
	movl	%eax, %edx
	cmovll	%r12d, %edx
	movl	%r15d, %esi
	movl	%eax, %edi
.LBB30_84:                              #   in Loop: Header=BB30_80 Depth=2
	movslq	%esi, %rsi
	movq	(%r11,%rsi,8), %rsi
	movslq	%edi, %rdi
	movss	(%rsi,%rdi,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	movslq	%ecx, %rcx
	movq	(%r11,%rcx,8), %rcx
	movslq	%edx, %rdx
	movss	(%rcx,%rdx,4), %xmm5    # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm6
	minss	%xmm5, %xmm6
	cvtss2sd	%xmm6, %xmm6
	mulsd	%xmm2, %xmm6
	addss	%xmm5, %xmm4
	cvtss2sd	%xmm4, %xmm4
	mulsd	%xmm1, %xmm4
	mulsd	%xmm3, %xmm4
	addsd	%xmm6, %xmm4
	cvtsd2ss	%xmm4, %xmm4
	movss	%xmm4, (%rsi,%rdi,4)
	movl	$1176255488, (%rcx,%rdx,4) # imm = 0x461C3C00
	movq	(%r11,%r9,8), %rcx
	movl	$1176255488, (%rcx,%r8,4) # imm = 0x461C3C00
.LBB30_85:                              #   in Loop: Header=BB30_80 Depth=2
	incl	%eax
	cmpl	%eax, %r13d
	jne	.LBB30_80
# BB#86:                                # %._crit_edge221
                                        #   in Loop: Header=BB30_26 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpq	24(%rsp), %rcx          # 8-byte Folded Reload
	jne	.LBB30_26
.LBB30_87:                              # %._crit_edge224
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end30:
	.size	supg, .Lfunc_end30-supg
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI31_0:
	.long	1176255488              # float 9999
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI31_1:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	spg
	.p2align	4, 0x90
	.type	spg,@function
spg:                                    # @spg
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi275:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi276:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi277:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi278:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi279:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi280:
	.cfi_def_cfa_offset 56
	subq	$400120, %rsp           # imm = 0x61AF8
.Lcfi281:
	.cfi_def_cfa_offset 400176
.Lcfi282:
	.cfi_offset %rbx, -56
.Lcfi283:
	.cfi_offset %r12, -48
.Lcfi284:
	.cfi_offset %r13, -40
.Lcfi285:
	.cfi_offset %r14, -32
.Lcfi286:
	.cfi_offset %r15, -24
.Lcfi287:
	.cfi_offset %rbp, -16
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %r15
	movl	%edi, %ebx
	movl	njob(%rip), %edi
	movl	%edi, %esi
	callq	AllocateDoubleMtx
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	njob(%rip), %edi
	movl	%edi, %esi
	callq	AllocateCharMtx
	movq	%rax, %r14
	movq	%rbx, (%rsp)            # 8-byte Spill
	testl	%ebx, %ebx
	jle	.LBB31_87
# BB#1:                                 # %.preheader212.us.preheader
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, %ebx
	leaq	-1(%rbx), %r10
	movl	%eax, %r8d
	andl	$3, %r8d
	movq	%rbx, %r9
	subq	%r8, %r9
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB31_2:                               # %.preheader212.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_10 Depth 2
                                        #     Child Loop BB31_14 Depth 2
                                        #     Child Loop BB31_16 Depth 2
	cmpl	$4, (%rsp)              # 4-byte Folded Reload
	movq	(%r15,%r11,8), %rdi
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r11,8), %rcx
	jae	.LBB31_4
# BB#3:                                 #   in Loop: Header=BB31_2 Depth=1
	xorl	%edx, %edx
	jmp	.LBB31_12
	.p2align	4, 0x90
.LBB31_4:                               # %min.iters.checked
                                        #   in Loop: Header=BB31_2 Depth=1
	testq	%r9, %r9
	je	.LBB31_8
# BB#5:                                 # %vector.memcheck
                                        #   in Loop: Header=BB31_2 Depth=1
	leaq	(%rdi,%rbx,8), %rax
	cmpq	%rax, %rcx
	jae	.LBB31_9
# BB#6:                                 # %vector.memcheck
                                        #   in Loop: Header=BB31_2 Depth=1
	leaq	(%rcx,%rbx,8), %rax
	cmpq	%rax, %rdi
	jae	.LBB31_9
.LBB31_8:                               #   in Loop: Header=BB31_2 Depth=1
	xorl	%edx, %edx
	jmp	.LBB31_12
.LBB31_9:                               # %vector.body.preheader
                                        #   in Loop: Header=BB31_2 Depth=1
	leaq	16(%rdi), %rax
	leaq	16(%rcx), %rdx
	movq	%r9, %rbp
	.p2align	4, 0x90
.LBB31_10:                              # %vector.body
                                        #   Parent Loop BB31_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rax), %xmm0
	movupd	(%rax), %xmm1
	movups	%xmm0, -16(%rdx)
	movupd	%xmm1, (%rdx)
	addq	$32, %rax
	addq	$32, %rdx
	addq	$-4, %rbp
	jne	.LBB31_10
# BB#11:                                # %middle.block
                                        #   in Loop: Header=BB31_2 Depth=1
	testl	%r8d, %r8d
	movq	%r9, %rdx
	je	.LBB31_17
	.p2align	4, 0x90
.LBB31_12:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB31_2 Depth=1
	movl	%ebx, %ebp
	subl	%edx, %ebp
	movq	%r10, %rax
	subq	%rdx, %rax
	andq	$7, %rbp
	je	.LBB31_15
# BB#13:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB31_2 Depth=1
	negq	%rbp
	.p2align	4, 0x90
.LBB31_14:                              # %scalar.ph.prol
                                        #   Parent Loop BB31_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi,%rdx,8), %rsi
	movq	%rsi, (%rcx,%rdx,8)
	incq	%rdx
	incq	%rbp
	jne	.LBB31_14
.LBB31_15:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB31_2 Depth=1
	cmpq	$7, %rax
	jb	.LBB31_17
	.p2align	4, 0x90
.LBB31_16:                              # %scalar.ph
                                        #   Parent Loop BB31_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi,%rdx,8), %rax
	movq	%rax, (%rcx,%rdx,8)
	movq	8(%rdi,%rdx,8), %rax
	movq	%rax, 8(%rcx,%rdx,8)
	movq	16(%rdi,%rdx,8), %rax
	movq	%rax, 16(%rcx,%rdx,8)
	movq	24(%rdi,%rdx,8), %rax
	movq	%rax, 24(%rcx,%rdx,8)
	movq	32(%rdi,%rdx,8), %rax
	movq	%rax, 32(%rcx,%rdx,8)
	movq	40(%rdi,%rdx,8), %rax
	movq	%rax, 40(%rcx,%rdx,8)
	movq	48(%rdi,%rdx,8), %rax
	movq	%rax, 48(%rcx,%rdx,8)
	movq	56(%rdi,%rdx,8), %rax
	movq	%rax, 56(%rcx,%rdx,8)
	addq	$8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB31_16
.LBB31_17:                              # %._crit_edge257.us
                                        #   in Loop: Header=BB31_2 Depth=1
	incq	%r11
	cmpq	%rdx, %r11
	jne	.LBB31_2
# BB#18:                                # %.preheader211
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB31_87
# BB#19:                                # %.preheader209.us.preheader
	movq	(%rsp), %rbx            # 8-byte Reload
	leal	-1(%rbx), %r12d
	leaq	8(,%r12,8), %rdx
	leaq	112(%rsp), %rdi
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	callq	memset
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	-1(%rcx), %rax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$7, %ecx
	movq	8(%rsp), %rdi           # 8-byte Reload
	.p2align	4, 0x90
.LBB31_20:                              # %.preheader209.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_21 Depth 2
                                        #     Child Loop BB31_23 Depth 2
	xorl	%edx, %edx
	testq	%rcx, %rcx
	je	.LBB31_22
	.p2align	4, 0x90
.LBB31_21:                              #   Parent Loop BB31_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%rbp,8), %rsi
	movb	$0, (%rsi,%rdx)
	incq	%rdx
	cmpq	%rdx, %rcx
	jne	.LBB31_21
.LBB31_22:                              # %.prol.loopexit343
                                        #   in Loop: Header=BB31_20 Depth=1
	cmpq	$7, %rax
	jb	.LBB31_24
	.p2align	4, 0x90
.LBB31_23:                              #   Parent Loop BB31_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%rbp,8), %rsi
	movb	$0, (%rsi,%rdx)
	movq	(%r14,%rbp,8), %rsi
	movb	$0, 1(%rsi,%rdx)
	movq	(%r14,%rbp,8), %rsi
	movb	$0, 2(%rsi,%rdx)
	movq	(%r14,%rbp,8), %rsi
	movb	$0, 3(%rsi,%rdx)
	movq	(%r14,%rbp,8), %rsi
	movb	$0, 4(%rsi,%rdx)
	movq	(%r14,%rbp,8), %rsi
	movb	$0, 5(%rsi,%rdx)
	movq	(%r14,%rbp,8), %rsi
	movb	$0, 6(%rsi,%rdx)
	movq	(%r14,%rbp,8), %rsi
	movb	$0, 7(%rsi,%rdx)
	addq	$8, %rdx
	cmpq	%rdx, %rdi
	jne	.LBB31_23
.LBB31_24:                              # %._crit_edge251.us
                                        #   in Loop: Header=BB31_20 Depth=1
	incq	%rbp
	cmpq	%rdi, %rbp
	jne	.LBB31_20
# BB#25:                                # %.preheader208
	testl	%ebx, %ebx
	jle	.LBB31_87
# BB#26:                                # %.lr.ph248.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	-1(%rax), %r11
	movq	%rax, %rcx
	xorl	%eax, %eax
	andq	$7, %rcx
	je	.LBB31_29
# BB#27:                                # %.lr.ph248.prol.preheader
	movq	(%rsp), %rsi            # 8-byte Reload
	.p2align	4, 0x90
.LBB31_28:                              # %.lr.ph248.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rax,8), %rdx
	movb	$1, (%rdx,%rax)
	incq	%rax
	cmpq	%rax, %rcx
	jne	.LBB31_28
	jmp	.LBB31_30
.LBB31_29:
	movq	(%rsp), %rsi            # 8-byte Reload
.LBB31_30:                              # %.lr.ph248.prol.loopexit
	cmpq	$7, %r11
	movq	8(%rsp), %rdx           # 8-byte Reload
	jb	.LBB31_32
	.p2align	4, 0x90
.LBB31_31:                              # %.lr.ph248
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rax,8), %rcx
	movb	$1, (%rcx,%rax)
	movq	8(%r14,%rax,8), %rcx
	movb	$1, 1(%rcx,%rax)
	movq	16(%r14,%rax,8), %rcx
	movb	$1, 2(%rcx,%rax)
	movq	24(%r14,%rax,8), %rcx
	movb	$1, 3(%rcx,%rax)
	movq	32(%r14,%rax,8), %rcx
	movb	$1, 4(%rcx,%rax)
	movq	40(%r14,%rax,8), %rcx
	movb	$1, 5(%rcx,%rax)
	movq	48(%r14,%rax,8), %rcx
	movb	$1, 6(%rcx,%rax)
	movq	56(%r14,%rax,8), %rcx
	movb	$1, 7(%rcx,%rax)
	addq	$8, %rax
	cmpq	%rax, %rdx
	jne	.LBB31_31
.LBB31_32:                              # %.preheader207
	cmpl	$2, %esi
	jl	.LBB31_87
# BB#33:                                # %.preheader206.lr.ph
	movslq	(%rsp), %r15            # 4-byte Folded Reload
	movl	%r12d, %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	-2(%rcx), %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movl	%ecx, %edx
	andl	$1, %edx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movl	%ecx, %edi
	andl	$7, %edi
	movl	%r11d, %ecx
	andl	$1, %ecx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movss	.LCPI31_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movsd	.LCPI31_1(%rip), %xmm1  # xmm1 = mem[0],zero
	movq	%r15, 96(%rsp)          # 8-byte Spill
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%r11, 24(%rsp)          # 8-byte Spill
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB31_34:                              # %.lr.ph223.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_35 Depth 2
                                        #       Child Loop BB31_41 Depth 3
                                        #     Child Loop BB31_54 Depth 2
                                        #     Child Loop BB31_67 Depth 2
                                        #     Child Loop BB31_76 Depth 2
                                        #     Child Loop BB31_78 Depth 2
                                        #     Child Loop BB31_80 Depth 2
                                        #     Child Loop BB31_82 Depth 2
	movl	$-1, %r13d
	xorl	%r9d, %r9d
	movl	$1, %edi
	movl	$-1, %r8d
	movaps	%xmm0, %xmm2
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB31_35:                              # %.lr.ph223
                                        #   Parent Loop BB31_34 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB31_41 Depth 3
	movq	%r9, %rbp
	leaq	1(%rbp), %r9
	cmpq	%r15, %r9
	jge	.LBB31_46
# BB#36:                                # %.lr.ph
                                        #   in Loop: Header=BB31_35 Depth=2
	movl	%r11d, %ecx
	subl	%ebp, %ecx
	movq	(%r10,%rbp,8), %rdx
	testb	$1, %cl
	jne	.LBB31_38
# BB#37:                                #   in Loop: Header=BB31_35 Depth=2
	movq	%rdi, %rsi
	cmpq	%rbp, 64(%rsp)          # 8-byte Folded Reload
	jne	.LBB31_41
	jmp	.LBB31_46
	.p2align	4, 0x90
.LBB31_38:                              #   in Loop: Header=BB31_35 Depth=2
	movsd	(%rdx,%rdi,8), %xmm3    # xmm3 = mem[0],zero
	cvtss2sd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	jbe	.LBB31_40
# BB#39:                                #   in Loop: Header=BB31_35 Depth=2
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
.LBB31_40:                              #   in Loop: Header=BB31_35 Depth=2
	cmoval	%ebp, %r8d
	cmoval	%edi, %r13d
	leaq	1(%rdi), %rsi
	cmpq	%rbp, 64(%rsp)          # 8-byte Folded Reload
	je	.LBB31_46
	.p2align	4, 0x90
.LBB31_41:                              #   Parent Loop BB31_34 Depth=1
                                        #     Parent Loop BB31_35 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rdx,%rsi,8), %xmm3    # xmm3 = mem[0],zero
	xorps	%xmm4, %xmm4
	cvtss2sd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	jbe	.LBB31_43
# BB#42:                                #   in Loop: Header=BB31_41 Depth=3
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
.LBB31_43:                              #   in Loop: Header=BB31_41 Depth=3
	cmoval	%esi, %r13d
	movsd	8(%rdx,%rsi,8), %xmm3   # xmm3 = mem[0],zero
	xorps	%xmm4, %xmm4
	cvtss2sd	%xmm2, %xmm4
	cmoval	%ebp, %r8d
	leal	1(%rsi), %ecx
	ucomisd	%xmm3, %xmm4
	jbe	.LBB31_45
# BB#44:                                #   in Loop: Header=BB31_41 Depth=3
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
.LBB31_45:                              #   in Loop: Header=BB31_41 Depth=3
	cmoval	%ebp, %r8d
	cmoval	%ecx, %r13d
	addq	$2, %rsi
	cmpq	%rbx, %rsi
	jne	.LBB31_41
.LBB31_46:                              # %.loopexit
                                        #   in Loop: Header=BB31_35 Depth=2
	incq	%rdi
	cmpq	%rax, %r9
	jne	.LBB31_35
# BB#47:                                # %.lr.ph229
                                        #   in Loop: Header=BB31_34 Depth=1
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	movslq	%r8d, %r9
	movq	(%r14,%r9,8), %rdi
	jne	.LBB31_49
# BB#48:                                #   in Loop: Header=BB31_34 Depth=1
	xorl	%r10d, %r10d
                                        # implicit-def: %ECX
	jmp	.LBB31_52
	.p2align	4, 0x90
.LBB31_49:                              #   in Loop: Header=BB31_34 Depth=1
	cmpb	$0, (%rdi)
	jle	.LBB31_51
# BB#50:                                #   in Loop: Header=BB31_34 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rcx
	movq	(%rcx), %rcx
	movl	$0, (%rcx)
	movl	$1, %ecx
	movl	$1, %r10d
	movl	$1, %edx
	testq	%r11, %r11
	jne	.LBB31_53
	jmp	.LBB31_59
.LBB31_51:                              #   in Loop: Header=BB31_34 Depth=1
	xorl	%ecx, %ecx
	movl	$1, %r10d
.LBB31_52:                              #   in Loop: Header=BB31_34 Depth=1
	xorl	%edx, %edx
	testq	%r11, %r11
	je	.LBB31_59
.LBB31_53:                              # %.lr.ph229.new
                                        #   in Loop: Header=BB31_34 Depth=1
	movq	%rbx, %r15
	subq	%r10, %r15
	leaq	1(%rdi,%r10), %rdi
	xorl	%ebp, %ebp
	movl	%edx, %ecx
	.p2align	4, 0x90
.LBB31_54:                              #   Parent Loop BB31_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, -1(%rdi,%rbp)
	jle	.LBB31_56
# BB#55:                                #   in Loop: Header=BB31_54 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	(%rax,%rdx,8), %rsi
	movq	(%rsi), %rsi
	movslq	%ecx, %rcx
	leal	(%r10,%rbp), %edx
	movl	%edx, (%rsi,%rcx,4)
	incl	%ecx
.LBB31_56:                              #   in Loop: Header=BB31_54 Depth=2
	cmpb	$0, (%rdi,%rbp)
	jle	.LBB31_58
# BB#57:                                #   in Loop: Header=BB31_54 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	(%rax), %rax
	movslq	%ecx, %rcx
	leal	1(%r10,%rbp), %edx
	movl	%edx, (%rax,%rcx,4)
	incl	%ecx
.LBB31_58:                              #   in Loop: Header=BB31_54 Depth=2
	addq	$2, %rbp
	cmpq	%rbp, %r15
	jne	.LBB31_54
.LBB31_59:                              # %.lr.ph234
                                        #   in Loop: Header=BB31_34 Depth=1
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	(%rax,%rdx,8), %rdx
	movq	(%rdx), %rsi
	movslq	%ecx, %rcx
	movl	$-1, (%rsi,%rcx,4)
	movslq	%r13d, %r10
	movq	(%r14,%r10,8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jne	.LBB31_61
# BB#60:                                #   in Loop: Header=BB31_34 Depth=1
	xorl	%edi, %edi
                                        # implicit-def: %ESI
	jmp	.LBB31_64
	.p2align	4, 0x90
.LBB31_61:                              #   in Loop: Header=BB31_34 Depth=1
	cmpb	$0, (%rax)
	jle	.LBB31_63
# BB#62:                                #   in Loop: Header=BB31_34 Depth=1
	movq	8(%rdx), %rcx
	movl	$0, (%rcx)
	movl	$1, %esi
	movl	$1, %edi
	movl	$1, %r11d
	jmp	.LBB31_65
.LBB31_63:                              #   in Loop: Header=BB31_34 Depth=1
	xorl	%esi, %esi
	movl	$1, %edi
.LBB31_64:                              # %.prol.loopexit330
                                        #   in Loop: Header=BB31_34 Depth=1
	xorl	%r11d, %r11d
.LBB31_65:                              # %.prol.loopexit330
                                        #   in Loop: Header=BB31_34 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB31_72
# BB#66:                                # %.lr.ph234.new
                                        #   in Loop: Header=BB31_34 Depth=1
	movq	%rbx, %rbp
	subq	%rdi, %rbp
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	1(%rax,%rdi), %rcx
	xorl	%r15d, %r15d
	movl	%r11d, %esi
	.p2align	4, 0x90
.LBB31_67:                              #   Parent Loop BB31_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, -1(%rcx,%r15)
	jle	.LBB31_69
# BB#68:                                #   in Loop: Header=BB31_67 Depth=2
	movq	8(%rdx), %r11
	movslq	%esi, %rsi
	leal	(%rdi,%r15), %r12d
	movl	%r12d, (%r11,%rsi,4)
	incl	%esi
.LBB31_69:                              #   in Loop: Header=BB31_67 Depth=2
	cmpb	$0, (%rcx,%r15)
	jle	.LBB31_71
# BB#70:                                #   in Loop: Header=BB31_67 Depth=2
	movq	8(%rdx), %rax
	movslq	%esi, %rsi
	leal	1(%rdi,%r15), %ebx
	movl	%ebx, (%rax,%rsi,4)
	incl	%esi
.LBB31_71:                              #   in Loop: Header=BB31_67 Depth=2
	movq	8(%rsp), %rbx           # 8-byte Reload
	addq	$2, %r15
	cmpq	%r15, %rbp
	jne	.LBB31_67
.LBB31_72:                              # %.lr.ph239
                                        #   in Loop: Header=BB31_34 Depth=1
	movq	8(%rdx), %rcx
	movslq	%esi, %rdx
	movsd	112(%rsp,%r10,8), %xmm3 # xmm3 = mem[0],zero
	movl	$-1, (%rcx,%rdx,4)
	cvtss2sd	%xmm2, %xmm2
	mulsd	%xmm1, %xmm2
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rcx
	movapd	%xmm2, %xmm4
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	movsd	112(%rsp,%r9,8), %xmm5  # xmm5 = mem[0],zero
	unpcklpd	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0]
	subpd	%xmm5, %xmm4
	movupd	%xmm4, (%rcx)
	movsd	%xmm2, 112(%rsp,%r9,8)
	movq	56(%rsp), %rax          # 8-byte Reload
	cmpb	$0, (%rax)
	setg	%cl
	movq	(%r14,%r9,8), %rdx
	addb	%cl, (%rdx)
	movq	(%rsp), %r11            # 8-byte Reload
	cmpl	$1, %r11d
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	80(%rsp), %rdi          # 8-byte Reload
	je	.LBB31_77
# BB#73:                                # %._crit_edge312.preheader
                                        #   in Loop: Header=BB31_34 Depth=1
	cmpq	$0, 72(%rsp)            # 8-byte Folded Reload
	jne	.LBB31_75
# BB#74:                                #   in Loop: Header=BB31_34 Depth=1
	movl	$1, %ecx
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	jne	.LBB31_76
	jmp	.LBB31_77
	.p2align	4, 0x90
.LBB31_75:                              # %._crit_edge312.prol
                                        #   in Loop: Header=BB31_34 Depth=1
	movq	(%r14,%r10,8), %rcx
	cmpb	$0, 1(%rcx)
	setg	%cl
	movq	(%r14,%r9,8), %rdx
	addb	%cl, 1(%rdx)
	movl	$2, %ecx
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	je	.LBB31_77
	.p2align	4, 0x90
.LBB31_76:                              # %._crit_edge312
                                        #   Parent Loop BB31_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%r10,8), %rdx
	cmpb	$0, (%rdx,%rcx)
	setg	%dl
	movq	(%r14,%r9,8), %rsi
	addb	%dl, (%rsi,%rcx)
	movq	(%r14,%r10,8), %rdx
	cmpb	$0, 1(%rdx,%rcx)
	setg	%dl
	movq	(%r14,%r9,8), %rsi
	addb	%dl, 1(%rsi,%rcx)
	addq	$2, %rcx
	cmpq	%rcx, %rbx
	jne	.LBB31_76
.LBB31_77:                              # %.lr.ph241
                                        #   in Loop: Header=BB31_34 Depth=1
	xorl	%ecx, %ecx
	testq	%rdi, %rdi
	je	.LBB31_79
	.p2align	4, 0x90
.LBB31_78:                              #   Parent Loop BB31_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%r10,8), %rdx
	movb	$0, (%rdx,%rcx)
	incq	%rcx
	cmpq	%rcx, %rdi
	jne	.LBB31_78
.LBB31_79:                              # %.prol.loopexit336
                                        #   in Loop: Header=BB31_34 Depth=1
	cmpq	$7, 24(%rsp)            # 8-byte Folded Reload
	jb	.LBB31_81
	.p2align	4, 0x90
.LBB31_80:                              #   Parent Loop BB31_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%r10,8), %rdx
	movb	$0, (%rdx,%rcx)
	movq	(%r14,%r10,8), %rdx
	movb	$0, 1(%rdx,%rcx)
	movq	(%r14,%r10,8), %rdx
	movb	$0, 2(%rdx,%rcx)
	movq	(%r14,%r10,8), %rdx
	movb	$0, 3(%rdx,%rcx)
	movq	(%r14,%r10,8), %rdx
	movb	$0, 4(%rdx,%rcx)
	movq	(%r14,%r10,8), %rdx
	movb	$0, 5(%rdx,%rcx)
	movq	(%r14,%r10,8), %rdx
	movb	$0, 6(%rdx,%rcx)
	movq	(%r14,%r10,8), %rdx
	movb	$0, 7(%rdx,%rcx)
	addq	$8, %rcx
	cmpq	%rcx, %rbx
	jne	.LBB31_80
.LBB31_81:                              # %.lr.ph243
                                        #   in Loop: Header=BB31_34 Depth=1
	movq	(%r15,%r9,8), %r9
	xorl	%edx, %edx
	movabsq	$4666722622711529472, %rax # imm = 0x40C3878000000000
	.p2align	4, 0x90
.LBB31_82:                              #   Parent Loop BB31_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%edx, %r8d
	je	.LBB31_85
# BB#83:                                #   in Loop: Header=BB31_82 Depth=2
	cmpl	%edx, %r13d
	je	.LBB31_85
# BB#84:                                #   in Loop: Header=BB31_82 Depth=2
	cmpl	%r8d, %edx
	movl	%r8d, %esi
	cmovlel	%edx, %esi
	movslq	%esi, %rsi
	movq	(%r15,%rsi,8), %rsi
	movl	%r8d, %edi
	cmovgel	%edx, %edi
	movslq	%edi, %rdi
	cmpl	%r13d, %edx
	movl	%r13d, %eax
	cmovlel	%edx, %eax
	cltq
	movq	(%r15,%rax,8), %rbx
	movl	%r13d, %ebp
	cmovgel	%edx, %ebp
	movslq	%ebp, %rbp
	movsd	(%rbx,%rbp,8), %xmm2    # xmm2 = mem[0],zero
	ucomisd	(%rsi,%rdi,8), %xmm2
	movl	%ebp, %ecx
	cmoval	%edi, %ecx
	movq	%rbx, %rax
	cmovaq	%rsi, %rax
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	%rax, (%rsi,%rdi,8)
	movabsq	$4666722622711529472, %rax # imm = 0x40C3878000000000
	movq	%rax, (%rbx,%rbp,8)
	movabsq	$4666722622711529472, %rax # imm = 0x40C3878000000000
.LBB31_85:                              #   in Loop: Header=BB31_82 Depth=2
	movq	%rax, (%r9,%r10,8)
	incl	%edx
	cmpl	%edx, %r11d
	jne	.LBB31_82
# BB#86:                                # %._crit_edge244
                                        #   in Loop: Header=BB31_34 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rdx
	incq	%rdx
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	%rdx, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	cmpq	%rax, %rdx
	movq	24(%rsp), %r11          # 8-byte Reload
	movq	96(%rsp), %r15          # 8-byte Reload
	jne	.LBB31_34
.LBB31_87:                              # %._crit_edge246
	addq	$400120, %rsp           # imm = 0x61AF8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end31:
	.size	spg, .Lfunc_end31-spg
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI32_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	ipower
	.p2align	4, 0x90
	.type	ipower,@function
ipower:                                 # @ipower
	.cfi_startproc
# BB#0:
	movapd	%xmm0, %xmm1
	movsd	.LCPI32_0(%rip), %xmm0  # xmm0 = mem[0],zero
	testl	%edi, %edi
	je	.LBB32_4
	.p2align	4, 0x90
.LBB32_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testb	$1, %dil
	je	.LBB32_3
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB32_1 Depth=1
	mulsd	%xmm1, %xmm0
.LBB32_3:                               # %.lr.ph
                                        #   in Loop: Header=BB32_1 Depth=1
	mulsd	%xmm1, %xmm1
	sarl	%edi
	jne	.LBB32_1
.LBB32_4:                               # %._crit_edge
	retq
.Lfunc_end32:
	.size	ipower, .Lfunc_end32-ipower
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI33_0:
	.quad	4607182418800017408     # double 1
.LCPI33_1:
	.quad	-4616189618054758400    # double -1
	.text
	.globl	countnode
	.p2align	4, 0x90
	.type	countnode,@function
countnode:                              # @countnode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi288:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi289:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi290:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi291:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi292:
	.cfi_def_cfa_offset 48
.Lcfi293:
	.cfi_offset %rbx, -48
.Lcfi294:
	.cfi_offset %r12, -40
.Lcfi295:
	.cfi_offset %r14, -32
.Lcfi296:
	.cfi_offset %r15, -24
.Lcfi297:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movl	%edi, %r15d
	cmpl	$1, %r15d
	jle	.LBB33_27
# BB#1:                                 # %.preheader90
	leal	-2(%r15), %r12d
	leal	-1(%r15), %eax
	leaq	8(,%rax,8), %rdx
	movl	$countnode.rootnode, %edi
	xorl	%esi, %esi
	callq	memset
	cmpl	$2, %r15d
	je	.LBB33_13
# BB#2:                                 # %.preheader89.preheader
	movl	%r12d, %r8d
	xorl	%r9d, %r9d
	movsd	.LCPI33_0(%rip), %xmm0  # xmm0 = mem[0],zero
	movsd	.LCPI33_1(%rip), %xmm1  # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB33_3:                               # %.preheader89
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_5 Depth 2
                                        #     Child Loop BB33_8 Depth 2
                                        #     Child Loop BB33_12 Depth 2
                                        #     Child Loop BB33_23 Depth 2
                                        #       Child Loop BB33_24 Depth 3
	movq	(%r14,%r9,8), %rax
	movq	(%rax), %rdx
	movl	(%rdx), %esi
	testl	%esi, %esi
	js	.LBB33_6
# BB#4:                                 # %.lr.ph96.preheader
                                        #   in Loop: Header=BB33_3 Depth=1
	leaq	4(%rdx), %rcx
	movl	%esi, %edi
	.p2align	4, 0x90
.LBB33_5:                               # %.lr.ph96
                                        #   Parent Loop BB33_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edi, %rdi
	movsd	countnode.rootnode(,%rdi,8), %xmm2 # xmm2 = mem[0],zero
	addsd	%xmm0, %xmm2
	movsd	%xmm2, countnode.rootnode(,%rdi,8)
	movl	(%rcx), %edi
	addq	$4, %rcx
	testl	%edi, %edi
	jns	.LBB33_5
.LBB33_6:                               # %.preheader88
                                        #   in Loop: Header=BB33_3 Depth=1
	movq	8(%rax), %r11
	movl	(%r11), %r10d
	testl	%r10d, %r10d
	js	.LBB33_9
# BB#7:                                 # %.lr.ph98.preheader
                                        #   in Loop: Header=BB33_3 Depth=1
	leaq	4(%r11), %rax
	movl	%r10d, %edi
	.p2align	4, 0x90
.LBB33_8:                               # %.lr.ph98
                                        #   Parent Loop BB33_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edi, %rcx
	movsd	countnode.rootnode(,%rcx,8), %xmm2 # xmm2 = mem[0],zero
	addsd	%xmm0, %xmm2
	movsd	%xmm2, countnode.rootnode(,%rcx,8)
	movl	(%rax), %edi
	addq	$4, %rax
	testl	%edi, %edi
	jns	.LBB33_8
.LBB33_9:                               # %.preheader87
                                        #   in Loop: Header=BB33_3 Depth=1
	testl	%esi, %esi
	js	.LBB33_26
# BB#10:                                # %.preheader86.lr.ph
                                        #   in Loop: Header=BB33_3 Depth=1
	testl	%r10d, %r10d
	js	.LBB33_11
# BB#22:                                # %.preheader86.us.preheader
                                        #   in Loop: Header=BB33_3 Depth=1
	addq	$4, %r11
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB33_23:                              # %.preheader86.us
                                        #   Parent Loop BB33_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB33_24 Depth 3
	movslq	%esi, %rdi
	movsd	countnode.rootnode(,%rdi,8), %xmm2 # xmm2 = mem[0],zero
	movq	%r11, %rdi
	movl	%r10d, %ebp
	.p2align	4, 0x90
.LBB33_24:                              #   Parent Loop BB33_3 Depth=1
                                        #     Parent Loop BB33_23 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%ebp, %rbp
	movsd	countnode.rootnode(,%rbp,8), %xmm3 # xmm3 = mem[0],zero
	addsd	%xmm2, %xmm3
	addsd	%xmm1, %xmm3
	cmpl	%ebp, %esi
	movl	%ebp, %ecx
	cmovlel	%esi, %ecx
	movslq	%ecx, %rcx
	movq	(%rbx,%rcx,8), %rcx
	cmovgel	%esi, %ebp
	movslq	%ebp, %rbp
	movsd	%xmm3, (%rcx,%rbp,8)
	movl	(%rdi), %ebp
	addq	$4, %rdi
	testl	%ebp, %ebp
	jns	.LBB33_24
# BB#25:                                # %._crit_edge101.us
                                        #   in Loop: Header=BB33_23 Depth=2
	movl	4(%rdx,%rax,4), %esi
	incq	%rax
	testl	%esi, %esi
	jns	.LBB33_23
	jmp	.LBB33_26
	.p2align	4, 0x90
.LBB33_11:                              # %.preheader86.preheader
                                        #   in Loop: Header=BB33_3 Depth=1
	addq	$4, %rdx
	.p2align	4, 0x90
.LBB33_12:                              # %.preheader86
                                        #   Parent Loop BB33_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, (%rdx)
	leaq	4(%rdx), %rdx
	jns	.LBB33_12
.LBB33_26:                              # %._crit_edge103
                                        #   in Loop: Header=BB33_3 Depth=1
	incq	%r9
	cmpq	%r8, %r9
	jne	.LBB33_3
.LBB33_13:                              # %.preheader85
	movslq	%r12d, %rax
	movq	(%r14,%rax,8), %rdx
	movq	(%rdx), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	js	.LBB33_17
# BB#14:                                # %.preheader.lr.ph
	movq	8(%rdx), %r9
	movl	(%r9), %r8d
	testl	%r8d, %r8d
	js	.LBB33_15
# BB#18:                                # %.preheader.us.preheader
	addq	$4, %r9
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB33_19:                              # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_20 Depth 2
	movslq	%ecx, %rdx
	movsd	countnode.rootnode(,%rdx,8), %xmm0 # xmm0 = mem[0],zero
	movq	%r9, %rdx
	movl	%r8d, %esi
	.p2align	4, 0x90
.LBB33_20:                              #   Parent Loop BB33_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%esi, %rsi
	movsd	countnode.rootnode(,%rsi,8), %xmm1 # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	cmpl	%esi, %ecx
	movl	%esi, %ebp
	cmovlel	%ecx, %ebp
	movslq	%ebp, %rbp
	movq	(%rbx,%rbp,8), %rbp
	cmovgel	%ecx, %esi
	movslq	%esi, %rsi
	movsd	%xmm1, (%rbp,%rsi,8)
	movl	(%rdx), %esi
	addq	$4, %rdx
	testl	%esi, %esi
	jns	.LBB33_20
# BB#21:                                # %._crit_edge.us
                                        #   in Loop: Header=BB33_19 Depth=1
	movl	4(%rax,%rdi,4), %ecx
	incq	%rdi
	testl	%ecx, %ecx
	jns	.LBB33_19
	jmp	.LBB33_17
.LBB33_15:                              # %.preheader.preheader
	addq	$4, %rax
	.p2align	4, 0x90
.LBB33_16:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jns	.LBB33_16
.LBB33_17:                              # %._crit_edge94
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB33_27:
	movq	stderr(%rip), %rdi
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end33:
	.size	countnode, .Lfunc_end33-countnode
	.cfi_endproc

	.globl	countnode_int
	.p2align	4, 0x90
	.type	countnode_int,@function
countnode_int:                          # @countnode_int
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi298:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi299:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi300:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi301:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi302:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi303:
	.cfi_def_cfa_offset 56
	subq	$200040, %rsp           # imm = 0x30D68
.Lcfi304:
	.cfi_def_cfa_offset 200096
.Lcfi305:
	.cfi_offset %rbx, -56
.Lcfi306:
	.cfi_offset %r12, -48
.Lcfi307:
	.cfi_offset %r13, -40
.Lcfi308:
	.cfi_offset %r14, -32
.Lcfi309:
	.cfi_offset %r15, -24
.Lcfi310:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movl	%edi, %r14d
	testl	%r14d, %r14d
	jle	.LBB34_1
# BB#10:                                # %.preheader103
	leal	-1(%r14), %eax
	leaq	4(,%rax,4), %rdx
	leaq	32(%rsp), %rdi
	xorl	%esi, %esi
	callq	memset
	leal	-2(%r14), %eax
	cmpl	$3, %r14d
	jl	.LBB34_2
# BB#11:                                # %.preheader102.preheader
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	%eax, %r9d
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB34_12:                              # %.preheader102
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB34_14 Depth 2
                                        #     Child Loop BB34_17 Depth 2
                                        #     Child Loop BB34_22 Depth 2
                                        #     Child Loop BB34_23 Depth 2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%r10,8), %rax
	movq	(%rax), %r12
	movl	(%r12), %edi
	testl	%edi, %edi
	js	.LBB34_15
# BB#13:                                # %.lr.ph113.preheader
                                        #   in Loop: Header=BB34_12 Depth=1
	leaq	4(%r12), %rcx
	movl	%edi, %edx
	.p2align	4, 0x90
.LBB34_14:                              # %.lr.ph113
                                        #   Parent Loop BB34_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rdx
	incl	32(%rsp,%rdx,4)
	movl	(%rcx), %edx
	addq	$4, %rcx
	testl	%edx, %edx
	jns	.LBB34_14
.LBB34_15:                              # %.preheader101
                                        #   in Loop: Header=BB34_12 Depth=1
	movq	8(%rax), %r13
	movl	(%r13), %ecx
	testl	%ecx, %ecx
	js	.LBB34_18
# BB#16:                                # %.lr.ph115.preheader
                                        #   in Loop: Header=BB34_12 Depth=1
	leaq	4(%r13), %rdx
	movl	%ecx, %esi
	.p2align	4, 0x90
.LBB34_17:                              # %.lr.ph115
                                        #   Parent Loop BB34_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%esi, %rsi
	incl	32(%rsp,%rsi,4)
	movl	(%rdx), %esi
	addq	$4, %rdx
	testl	%esi, %esi
	jns	.LBB34_17
.LBB34_18:                              # %.preheader100
                                        #   in Loop: Header=BB34_12 Depth=1
	testl	%edi, %edi
	js	.LBB34_25
# BB#19:                                # %.preheader99.preheader
                                        #   in Loop: Header=BB34_12 Depth=1
	leaq	4(%r13), %r11
	xorl	%r15d, %r15d
	testl	%ecx, %ecx
	jns	.LBB34_21
	jmp	.LBB34_23
	.p2align	4, 0x90
.LBB34_24:                              # %._crit_edge118..preheader99_crit_edge
                                        #   in Loop: Header=BB34_23 Depth=2
	incq	%r15
	movl	(%r13), %ecx
	testl	%ecx, %ecx
	js	.LBB34_23
.LBB34_21:                              # %.lr.ph117
                                        #   in Loop: Header=BB34_12 Depth=1
	movslq	%edi, %rsi
	movl	32(%rsp,%rsi,4), %ebp
	decl	%ebp
	movq	%r11, %rsi
	.p2align	4, 0x90
.LBB34_22:                              #   Parent Loop BB34_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ecx, %rcx
	movl	32(%rsp,%rcx,4), %eax
	addl	%ebp, %eax
	cmpl	%ecx, %edi
	movl	%ecx, %r8d
	cmovlel	%edi, %r8d
	movslq	%r8d, %rdx
	movq	(%rbx,%rdx,8), %rdx
	cmovgel	%edi, %ecx
	movslq	%ecx, %rcx
	movl	%eax, (%rdx,%rcx,4)
	movl	(%rsi), %ecx
	addq	$4, %rsi
	testl	%ecx, %ecx
	jns	.LBB34_22
.LBB34_23:                              # %._crit_edge118
                                        #   Parent Loop BB34_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%r12,%r15,4), %edi
	testl	%edi, %edi
	jns	.LBB34_24
.LBB34_25:                              # %._crit_edge120
                                        #   in Loop: Header=BB34_12 Depth=1
	incq	%r10
	cmpq	%r9, %r10
	jne	.LBB34_12
# BB#26:
	movl	16(%rsp), %eax          # 4-byte Reload
	jmp	.LBB34_2
.LBB34_1:                               # %.preheader103.thread
	leal	-2(%r14), %eax
.LBB34_2:                               # %.preheader98
	cltq
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movq	(%rax), %r9
	movl	(%r9), %ecx
	testl	%ecx, %ecx
	js	.LBB34_8
# BB#3:                                 # %.preheader97.lr.ph
	movq	8(%rax), %r10
	leaq	4(%r10), %r8
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB34_4:                               # %.preheader97
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB34_6 Depth 2
	movl	(%r10), %eax
	testl	%eax, %eax
	js	.LBB34_7
# BB#5:                                 # %.lr.ph109
                                        #   in Loop: Header=BB34_4 Depth=1
	movslq	%ecx, %rsi
	movl	32(%rsp,%rsi,4), %ebp
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB34_6:                               #   Parent Loop BB34_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cltq
	movl	32(%rsp,%rax,4), %edx
	addl	%ebp, %edx
	cmpl	%eax, %ecx
	movl	%eax, %edi
	cmovlel	%ecx, %edi
	movslq	%edi, %rdi
	movq	(%rbx,%rdi,8), %rdi
	cmovgel	%ecx, %eax
	cltq
	movl	%edx, (%rdi,%rax,4)
	movl	(%rsi), %eax
	addq	$4, %rsi
	testl	%eax, %eax
	jns	.LBB34_6
.LBB34_7:                               # %._crit_edge110
                                        #   in Loop: Header=BB34_4 Depth=1
	movl	4(%r9,%r11,4), %ecx
	incq	%r11
	testl	%ecx, %ecx
	jns	.LBB34_4
.LBB34_8:                               # %.preheader
	cmpl	$2, %r14d
	jl	.LBB34_35
# BB#9:                                 # %.lr.ph107.preheader
	leal	-1(%r14), %r15d
	movslq	%r14d, %r12
	movl	%r14d, %eax
	leaq	3(%rax), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	-2(%rax), %r8
	addb	$3, %r14b
	leaq	24(%rbx), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$1, %r10d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB34_28:                              # %.lr.ph107
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB34_31 Depth 2
                                        #     Child Loop BB34_34 Depth 2
	movq	%r13, %rsi
	leaq	1(%rsi), %r13
	cmpq	%r12, %r13
	jge	.LBB34_27
# BB#29:                                # %.lr.ph
                                        #   in Loop: Header=BB34_28 Depth=1
	movq	%r8, %r11
	subq	%rsi, %r11
	movq	8(%rsp), %rdi           # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	subl	%esi, %edi
	movq	(%rbx,%rsi,8), %rax
	testb	$3, %dil
	movq	%r10, %rbp
	je	.LBB34_32
# BB#30:                                # %.prol.preheader
                                        #   in Loop: Header=BB34_28 Depth=1
	movl	%r14d, %ecx
	andb	$3, %cl
	movzbl	%cl, %edi
	negq	%rdi
	movq	%r10, %rbp
	.p2align	4, 0x90
.LBB34_31:                              #   Parent Loop BB34_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax,%rbp,4), %ecx
	movq	(%rbx,%rbp,8), %r9
	movl	%ecx, (%r9,%rsi,4)
	incq	%rbp
	incq	%rdi
	jne	.LBB34_31
.LBB34_32:                              # %.prol.loopexit
                                        #   in Loop: Header=BB34_28 Depth=1
	cmpq	$3, %r11
	jb	.LBB34_27
# BB#33:                                # %.lr.ph.new
                                        #   in Loop: Header=BB34_28 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	subq	%rbp, %rcx
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rbp,8), %rdi
	leaq	12(%rax,%rbp,4), %rax
	.p2align	4, 0x90
.LBB34_34:                              #   Parent Loop BB34_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-12(%rax), %ebp
	movq	-24(%rdi), %rdx
	movl	%ebp, (%rdx,%rsi,4)
	movl	-8(%rax), %edx
	movq	-16(%rdi), %rbp
	movl	%edx, (%rbp,%rsi,4)
	movl	-4(%rax), %edx
	movq	-8(%rdi), %rbp
	movl	%edx, (%rbp,%rsi,4)
	movl	(%rax), %edx
	movq	(%rdi), %rbp
	movl	%edx, (%rbp,%rsi,4)
	addq	$32, %rdi
	addq	$16, %rax
	addq	$-4, %rcx
	jne	.LBB34_34
.LBB34_27:                              # %.loopexit
                                        #   in Loop: Header=BB34_28 Depth=1
	incq	%r10
	addb	$3, %r14b
	cmpq	%r15, %r13
	jne	.LBB34_28
.LBB34_35:                              # %._crit_edge
	addq	$200040, %rsp           # imm = 0x30D68
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end34:
	.size	countnode_int, .Lfunc_end34-countnode_int
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI35_0:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
.LCPI35_2:
	.quad	4562254508917369340     # double 0.001
	.quad	4562254508917369340     # double 0.001
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI35_1:
	.quad	4602678819172646912     # double 0.5
.LCPI35_3:
	.quad	4562254508917369340     # double 0.001
	.text
	.globl	counteff_simple_float
	.p2align	4, 0x90
	.type	counteff_simple_float,@function
counteff_simple_float:                  # @counteff_simple_float
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi311:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi312:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi313:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi314:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi315:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi316:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi317:
	.cfi_def_cfa_offset 64
.Lcfi318:
	.cfi_offset %rbx, -56
.Lcfi319:
	.cfi_offset %r12, -48
.Lcfi320:
	.cfi_offset %r13, -40
.Lcfi321:
	.cfi_offset %r14, -32
.Lcfi322:
	.cfi_offset %r15, -24
.Lcfi323:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r12
	movl	%edi, %r15d
	testl	%r15d, %r15d
	jle	.LBB35_47
# BB#1:                                 # %.lr.ph75.preheader
	movq	%rcx, %rbx
	leal	-1(%r15), %r13d
	leaq	8(,%r13,8), %rdx
	xorl	%ebp, %ebp
	movl	$counteff_simple_float.rootnode, %edi
	xorl	%esi, %esi
	callq	memset
	movl	%r15d, %eax
	cmpl	$3, %r15d
	jbe	.LBB35_2
# BB#5:                                 # %min.iters.checked
	movl	%r15d, %ecx
	andl	$3, %ecx
	movq	%rax, %rbp
	subq	%rcx, %rbp
	je	.LBB35_6
# BB#7:                                 # %vector.body.preheader
	xorl	%edx, %edx
	movapd	.LCPI35_0(%rip), %xmm0  # xmm0 = [1.000000e+00,1.000000e+00]
	movq	%rbx, %r8
	.p2align	4, 0x90
.LBB35_8:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, counteff_simple_float.eff(,%rdx,8)
	movapd	%xmm0, counteff_simple_float.eff+16(,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rbp
	jne	.LBB35_8
# BB#9:                                 # %middle.block
	testl	%ecx, %ecx
	jne	.LBB35_3
	jmp	.LBB35_10
.LBB35_6:
	xorl	%ebp, %ebp
.LBB35_2:
	movq	%rbx, %r8
.LBB35_3:                               # %.lr.ph75.preheader140
	movabsq	$4607182418800017408, %rcx # imm = 0x3FF0000000000000
	.p2align	4, 0x90
.LBB35_4:                               # %.lr.ph75
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, counteff_simple_float.eff(,%rbp,8)
	incq	%rbp
	cmpq	%rbp, %rax
	jne	.LBB35_4
.LBB35_10:                              # %.preheader57
	cmpl	$2, %r15d
	jl	.LBB35_19
# BB#11:                                # %.preheader56.preheader
	movl	%r13d, %ecx
	xorl	%edx, %edx
	movsd	.LCPI35_1(%rip), %xmm0  # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB35_12:                              # %.preheader56
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB35_14 Depth 2
                                        #     Child Loop BB35_17 Depth 2
	movq	(%r12,%rdx,8), %rsi
	movq	(%rsi), %rdi
	movl	(%rdi), %ebp
	testl	%ebp, %ebp
	js	.LBB35_15
# BB#13:                                # %.lr.ph66
                                        #   in Loop: Header=BB35_12 Depth=1
	movq	(%r14,%rdx,8), %rbx
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addq	$4, %rdi
	.p2align	4, 0x90
.LBB35_14:                              #   Parent Loop BB35_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebp, %rbp
	movsd	counteff_simple_float.eff(,%rbp,8), %xmm2 # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm3
	mulsd	%xmm2, %xmm3
	addsd	counteff_simple_float.rootnode(,%rbp,8), %xmm3
	movsd	%xmm3, counteff_simple_float.rootnode(,%rbp,8)
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, counteff_simple_float.eff(,%rbp,8)
	movl	(%rdi), %ebp
	addq	$4, %rdi
	testl	%ebp, %ebp
	jns	.LBB35_14
.LBB35_15:                              # %.preheader55
                                        #   in Loop: Header=BB35_12 Depth=1
	movq	8(%rsi), %rsi
	movl	(%rsi), %edi
	testl	%edi, %edi
	js	.LBB35_18
# BB#16:                                # %.lr.ph70
                                        #   in Loop: Header=BB35_12 Depth=1
	movq	(%r14,%rdx,8), %rbp
	movss	4(%rbp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addq	$4, %rsi
	.p2align	4, 0x90
.LBB35_17:                              #   Parent Loop BB35_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edi, %rdi
	movsd	counteff_simple_float.eff(,%rdi,8), %xmm2 # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm3
	mulsd	%xmm2, %xmm3
	addsd	counteff_simple_float.rootnode(,%rdi,8), %xmm3
	movsd	%xmm3, counteff_simple_float.rootnode(,%rdi,8)
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, counteff_simple_float.eff(,%rdi,8)
	movl	(%rsi), %edi
	addq	$4, %rsi
	testl	%edi, %edi
	jns	.LBB35_17
.LBB35_18:                              # %._crit_edge71
                                        #   in Loop: Header=BB35_12 Depth=1
	incq	%rdx
	cmpq	%rcx, %rdx
	jne	.LBB35_12
.LBB35_19:                              # %.preheader54
	testl	%r15d, %r15d
	jle	.LBB35_47
# BB#20:                                # %.lr.ph64.preheader
	cmpl	$3, %r15d
	jbe	.LBB35_21
# BB#24:                                # %min.iters.checked101
	movl	%r15d, %edx
	andl	$3, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB35_21
# BB#25:                                # %vector.body97.preheader
	xorl	%esi, %esi
	movapd	.LCPI35_2(%rip), %xmm0  # xmm0 = [1.000000e-03,1.000000e-03]
	.p2align	4, 0x90
.LBB35_26:                              # %vector.body97
                                        # =>This Inner Loop Header: Depth=1
	movapd	counteff_simple_float.rootnode(,%rsi,8), %xmm1
	addpd	%xmm0, %xmm1
	movapd	counteff_simple_float.rootnode+16(,%rsi,8), %xmm2
	addpd	%xmm0, %xmm2
	movapd	%xmm1, counteff_simple_float.rootnode(,%rsi,8)
	movapd	%xmm2, counteff_simple_float.rootnode+16(,%rsi,8)
	addq	$4, %rsi
	cmpq	%rsi, %rcx
	jne	.LBB35_26
# BB#27:                                # %middle.block98
	testl	%edx, %edx
	jne	.LBB35_22
	jmp	.LBB35_28
.LBB35_21:
	xorl	%ecx, %ecx
.LBB35_22:                              # %.lr.ph64.preheader139
	movsd	.LCPI35_3(%rip), %xmm0  # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB35_23:                              # %.lr.ph64
                                        # =>This Inner Loop Header: Depth=1
	movsd	counteff_simple_float.rootnode(,%rcx,8), %xmm1 # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, counteff_simple_float.rootnode(,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rax
	jne	.LBB35_23
.LBB35_28:                              # %.preheader53
	testl	%r15d, %r15d
	jle	.LBB35_47
# BB#29:                                # %.lr.ph62.preheader
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	andq	$7, %rsi
	je	.LBB35_30
# BB#31:                                # %.lr.ph62.prol.preheader
	xorpd	%xmm0, %xmm0
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB35_32:                              # %.lr.ph62.prol
                                        # =>This Inner Loop Header: Depth=1
	addsd	counteff_simple_float.rootnode(,%rcx,8), %xmm0
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB35_32
	jmp	.LBB35_33
.LBB35_30:
	xorl	%ecx, %ecx
	xorpd	%xmm0, %xmm0
.LBB35_33:                              # %.lr.ph62.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB35_34
	.p2align	4, 0x90
.LBB35_48:                              # %.lr.ph62
                                        # =>This Inner Loop Header: Depth=1
	addsd	counteff_simple_float.rootnode(,%rcx,8), %xmm0
	addsd	counteff_simple_float.rootnode+8(,%rcx,8), %xmm0
	addsd	counteff_simple_float.rootnode+16(,%rcx,8), %xmm0
	addsd	counteff_simple_float.rootnode+24(,%rcx,8), %xmm0
	addsd	counteff_simple_float.rootnode+32(,%rcx,8), %xmm0
	addsd	counteff_simple_float.rootnode+40(,%rcx,8), %xmm0
	addsd	counteff_simple_float.rootnode+48(,%rcx,8), %xmm0
	addsd	counteff_simple_float.rootnode+56(,%rcx,8), %xmm0
	addq	$8, %rcx
	cmpq	%rcx, %rax
	jne	.LBB35_48
.LBB35_34:                              # %.preheader
	testl	%r15d, %r15d
	jle	.LBB35_47
# BB#35:                                # %.lr.ph.preheader
	cmpl	$1, %r15d
	je	.LBB35_36
# BB#41:                                # %min.iters.checked119
	andl	$1, %r15d
	movq	%rax, %rcx
	subq	%r15, %rcx
	je	.LBB35_36
# BB#42:                                # %vector.memcheck
	leaq	counteff_simple_float.rootnode(,%rax,8), %rdx
	cmpq	%r8, %rdx
	jbe	.LBB35_44
# BB#43:                                # %vector.memcheck
	leaq	(%r8,%rax,8), %rdx
	movl	$counteff_simple_float.rootnode, %esi
	cmpq	%rsi, %rdx
	jbe	.LBB35_44
.LBB35_36:
	xorl	%ecx, %ecx
.LBB35_37:                              # %.lr.ph.preheader138
	movl	%eax, %edx
	subl	%ecx, %edx
	leaq	-1(%rax), %rsi
	testb	$1, %dl
	movq	%rcx, %rdx
	je	.LBB35_39
# BB#38:                                # %.lr.ph.prol
	movsd	counteff_simple_float.rootnode(,%rcx,8), %xmm1 # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, (%r8,%rcx,8)
	leaq	1(%rcx), %rdx
.LBB35_39:                              # %.lr.ph.prol.loopexit
	cmpq	%rcx, %rsi
	je	.LBB35_47
	.p2align	4, 0x90
.LBB35_40:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	counteff_simple_float.rootnode(,%rdx,8), %xmm1 # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, (%r8,%rdx,8)
	movsd	counteff_simple_float.rootnode+8(,%rdx,8), %xmm1 # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%r8,%rdx,8)
	addq	$2, %rdx
	cmpq	%rdx, %rax
	jne	.LBB35_40
.LBB35_47:                              # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB35_44:                              # %vector.ph127
	movapd	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB35_45:                              # %vector.body115
                                        # =>This Inner Loop Header: Depth=1
	movapd	counteff_simple_float.rootnode(,%rdx,8), %xmm2
	divpd	%xmm1, %xmm2
	movupd	%xmm2, (%r8,%rdx,8)
	addq	$2, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB35_45
# BB#46:                                # %middle.block116
	testl	%r15d, %r15d
	jne	.LBB35_37
	jmp	.LBB35_47
.Lfunc_end35:
	.size	counteff_simple_float, .Lfunc_end35-counteff_simple_float
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI36_0:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
.LCPI36_2:
	.quad	4562254508917369340     # double 0.001
	.quad	4562254508917369340     # double 0.001
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI36_1:
	.quad	4602678819172646912     # double 0.5
.LCPI36_3:
	.quad	4562254508917369340     # double 0.001
	.text
	.globl	counteff_simple
	.p2align	4, 0x90
	.type	counteff_simple,@function
counteff_simple:                        # @counteff_simple
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi324:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi325:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi326:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi327:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi328:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi329:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi330:
	.cfi_def_cfa_offset 64
.Lcfi331:
	.cfi_offset %rbx, -56
.Lcfi332:
	.cfi_offset %r12, -48
.Lcfi333:
	.cfi_offset %r13, -40
.Lcfi334:
	.cfi_offset %r14, -32
.Lcfi335:
	.cfi_offset %r15, -24
.Lcfi336:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r12
	movl	%edi, %r15d
	testl	%r15d, %r15d
	jle	.LBB36_47
# BB#1:                                 # %.lr.ph75.preheader
	movq	%rcx, %rbx
	leal	-1(%r15), %r13d
	leaq	8(,%r13,8), %rdx
	xorl	%ebp, %ebp
	movl	$counteff_simple.rootnode, %edi
	xorl	%esi, %esi
	callq	memset
	movl	%r15d, %eax
	cmpl	$3, %r15d
	jbe	.LBB36_2
# BB#5:                                 # %min.iters.checked
	movl	%r15d, %ecx
	andl	$3, %ecx
	movq	%rax, %rbp
	subq	%rcx, %rbp
	je	.LBB36_6
# BB#7:                                 # %vector.body.preheader
	xorl	%edx, %edx
	movapd	.LCPI36_0(%rip), %xmm0  # xmm0 = [1.000000e+00,1.000000e+00]
	movq	%rbx, %r8
	.p2align	4, 0x90
.LBB36_8:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, counteff_simple.eff(,%rdx,8)
	movapd	%xmm0, counteff_simple.eff+16(,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rbp
	jne	.LBB36_8
# BB#9:                                 # %middle.block
	testl	%ecx, %ecx
	jne	.LBB36_3
	jmp	.LBB36_10
.LBB36_6:
	xorl	%ebp, %ebp
.LBB36_2:
	movq	%rbx, %r8
.LBB36_3:                               # %.lr.ph75.preheader140
	movabsq	$4607182418800017408, %rcx # imm = 0x3FF0000000000000
	.p2align	4, 0x90
.LBB36_4:                               # %.lr.ph75
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, counteff_simple.eff(,%rbp,8)
	incq	%rbp
	cmpq	%rbp, %rax
	jne	.LBB36_4
.LBB36_10:                              # %.preheader57
	cmpl	$2, %r15d
	jl	.LBB36_19
# BB#11:                                # %.preheader56.preheader
	movl	%r13d, %ecx
	xorl	%edx, %edx
	movsd	.LCPI36_1(%rip), %xmm0  # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB36_12:                              # %.preheader56
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB36_14 Depth 2
                                        #     Child Loop BB36_17 Depth 2
	movq	(%r12,%rdx,8), %rsi
	movq	(%rsi), %rdi
	movl	(%rdi), %ebp
	testl	%ebp, %ebp
	js	.LBB36_15
# BB#13:                                # %.lr.ph66
                                        #   in Loop: Header=BB36_12 Depth=1
	movq	(%r14,%rdx,8), %rbx
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	addq	$4, %rdi
	.p2align	4, 0x90
.LBB36_14:                              #   Parent Loop BB36_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebp, %rbp
	movsd	counteff_simple.eff(,%rbp,8), %xmm2 # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm3
	mulsd	%xmm2, %xmm3
	addsd	counteff_simple.rootnode(,%rbp,8), %xmm3
	movsd	%xmm3, counteff_simple.rootnode(,%rbp,8)
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, counteff_simple.eff(,%rbp,8)
	movl	(%rdi), %ebp
	addq	$4, %rdi
	testl	%ebp, %ebp
	jns	.LBB36_14
.LBB36_15:                              # %.preheader55
                                        #   in Loop: Header=BB36_12 Depth=1
	movq	8(%rsi), %rsi
	movl	(%rsi), %edi
	testl	%edi, %edi
	js	.LBB36_18
# BB#16:                                # %.lr.ph70
                                        #   in Loop: Header=BB36_12 Depth=1
	movq	(%r14,%rdx,8), %rbp
	movsd	8(%rbp), %xmm1          # xmm1 = mem[0],zero
	addq	$4, %rsi
	.p2align	4, 0x90
.LBB36_17:                              #   Parent Loop BB36_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edi, %rdi
	movsd	counteff_simple.eff(,%rdi,8), %xmm2 # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm3
	mulsd	%xmm2, %xmm3
	addsd	counteff_simple.rootnode(,%rdi,8), %xmm3
	movsd	%xmm3, counteff_simple.rootnode(,%rdi,8)
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, counteff_simple.eff(,%rdi,8)
	movl	(%rsi), %edi
	addq	$4, %rsi
	testl	%edi, %edi
	jns	.LBB36_17
.LBB36_18:                              # %._crit_edge71
                                        #   in Loop: Header=BB36_12 Depth=1
	incq	%rdx
	cmpq	%rcx, %rdx
	jne	.LBB36_12
.LBB36_19:                              # %.preheader54
	testl	%r15d, %r15d
	jle	.LBB36_47
# BB#20:                                # %.lr.ph64.preheader
	cmpl	$3, %r15d
	jbe	.LBB36_21
# BB#24:                                # %min.iters.checked101
	movl	%r15d, %edx
	andl	$3, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB36_21
# BB#25:                                # %vector.body97.preheader
	xorl	%esi, %esi
	movapd	.LCPI36_2(%rip), %xmm0  # xmm0 = [1.000000e-03,1.000000e-03]
	.p2align	4, 0x90
.LBB36_26:                              # %vector.body97
                                        # =>This Inner Loop Header: Depth=1
	movapd	counteff_simple.rootnode(,%rsi,8), %xmm1
	addpd	%xmm0, %xmm1
	movapd	counteff_simple.rootnode+16(,%rsi,8), %xmm2
	addpd	%xmm0, %xmm2
	movapd	%xmm1, counteff_simple.rootnode(,%rsi,8)
	movapd	%xmm2, counteff_simple.rootnode+16(,%rsi,8)
	addq	$4, %rsi
	cmpq	%rsi, %rcx
	jne	.LBB36_26
# BB#27:                                # %middle.block98
	testl	%edx, %edx
	jne	.LBB36_22
	jmp	.LBB36_28
.LBB36_21:
	xorl	%ecx, %ecx
.LBB36_22:                              # %.lr.ph64.preheader139
	movsd	.LCPI36_3(%rip), %xmm0  # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB36_23:                              # %.lr.ph64
                                        # =>This Inner Loop Header: Depth=1
	movsd	counteff_simple.rootnode(,%rcx,8), %xmm1 # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, counteff_simple.rootnode(,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rax
	jne	.LBB36_23
.LBB36_28:                              # %.preheader53
	testl	%r15d, %r15d
	jle	.LBB36_47
# BB#29:                                # %.lr.ph62.preheader
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	andq	$7, %rsi
	je	.LBB36_30
# BB#31:                                # %.lr.ph62.prol.preheader
	xorpd	%xmm0, %xmm0
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB36_32:                              # %.lr.ph62.prol
                                        # =>This Inner Loop Header: Depth=1
	addsd	counteff_simple.rootnode(,%rcx,8), %xmm0
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB36_32
	jmp	.LBB36_33
.LBB36_30:
	xorl	%ecx, %ecx
	xorpd	%xmm0, %xmm0
.LBB36_33:                              # %.lr.ph62.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB36_34
	.p2align	4, 0x90
.LBB36_48:                              # %.lr.ph62
                                        # =>This Inner Loop Header: Depth=1
	addsd	counteff_simple.rootnode(,%rcx,8), %xmm0
	addsd	counteff_simple.rootnode+8(,%rcx,8), %xmm0
	addsd	counteff_simple.rootnode+16(,%rcx,8), %xmm0
	addsd	counteff_simple.rootnode+24(,%rcx,8), %xmm0
	addsd	counteff_simple.rootnode+32(,%rcx,8), %xmm0
	addsd	counteff_simple.rootnode+40(,%rcx,8), %xmm0
	addsd	counteff_simple.rootnode+48(,%rcx,8), %xmm0
	addsd	counteff_simple.rootnode+56(,%rcx,8), %xmm0
	addq	$8, %rcx
	cmpq	%rcx, %rax
	jne	.LBB36_48
.LBB36_34:                              # %.preheader
	testl	%r15d, %r15d
	jle	.LBB36_47
# BB#35:                                # %.lr.ph.preheader
	cmpl	$1, %r15d
	je	.LBB36_36
# BB#41:                                # %min.iters.checked119
	andl	$1, %r15d
	movq	%rax, %rcx
	subq	%r15, %rcx
	je	.LBB36_36
# BB#42:                                # %vector.memcheck
	leaq	counteff_simple.rootnode(,%rax,8), %rdx
	cmpq	%r8, %rdx
	jbe	.LBB36_44
# BB#43:                                # %vector.memcheck
	leaq	(%r8,%rax,8), %rdx
	movl	$counteff_simple.rootnode, %esi
	cmpq	%rsi, %rdx
	jbe	.LBB36_44
.LBB36_36:
	xorl	%ecx, %ecx
.LBB36_37:                              # %.lr.ph.preheader138
	movl	%eax, %edx
	subl	%ecx, %edx
	leaq	-1(%rax), %rsi
	testb	$1, %dl
	movq	%rcx, %rdx
	je	.LBB36_39
# BB#38:                                # %.lr.ph.prol
	movsd	counteff_simple.rootnode(,%rcx,8), %xmm1 # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, (%r8,%rcx,8)
	leaq	1(%rcx), %rdx
.LBB36_39:                              # %.lr.ph.prol.loopexit
	cmpq	%rcx, %rsi
	je	.LBB36_47
	.p2align	4, 0x90
.LBB36_40:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	counteff_simple.rootnode(,%rdx,8), %xmm1 # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, (%r8,%rdx,8)
	movsd	counteff_simple.rootnode+8(,%rdx,8), %xmm1 # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%r8,%rdx,8)
	addq	$2, %rdx
	cmpq	%rdx, %rax
	jne	.LBB36_40
.LBB36_47:                              # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB36_44:                              # %vector.ph127
	movapd	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB36_45:                              # %vector.body115
                                        # =>This Inner Loop Header: Depth=1
	movapd	counteff_simple.rootnode(,%rdx,8), %xmm2
	divpd	%xmm1, %xmm2
	movupd	%xmm2, (%r8,%rdx,8)
	addq	$2, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB36_45
# BB#46:                                # %middle.block116
	testl	%r15d, %r15d
	jne	.LBB36_37
	jmp	.LBB36_47
.Lfunc_end36:
	.size	counteff_simple, .Lfunc_end36-counteff_simple
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI37_0:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
.LCPI37_2:
	.quad	4562254508917369340     # double 0.001
	.quad	4562254508917369340     # double 0.001
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI37_1:
	.quad	4602678819172646912     # double 0.5
.LCPI37_3:
	.quad	4562254508917369340     # double 0.001
.LCPI37_4:
	.quad	4607182418800017408     # double 1
.LCPI37_5:
	.quad	-4616189618054758400    # double -1
	.text
	.globl	counteff
	.p2align	4, 0x90
	.type	counteff,@function
counteff:                               # @counteff
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi337:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi338:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi339:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi340:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi341:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi342:
	.cfi_def_cfa_offset 56
	subq	$800040, %rsp           # imm = 0xC3528
.Lcfi343:
	.cfi_def_cfa_offset 800096
.Lcfi344:
	.cfi_offset %rbx, -56
.Lcfi345:
	.cfi_offset %r12, -48
.Lcfi346:
	.cfi_offset %r13, -40
.Lcfi347:
	.cfi_offset %r14, -32
.Lcfi348:
	.cfi_offset %r15, -24
.Lcfi349:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r12
	movq	%rsi, %r15
	movl	%edi, %r14d
	cmpl	$0, mix(%rip)
	je	.LBB37_12
# BB#1:
	movl	weight(%rip), %eax
	cmpl	$3, %eax
	je	.LBB37_10
# BB#2:
	cmpl	$2, %eax
	jne	.LBB37_11
# BB#3:                                 # %thread-pre-split.thread300
	movl	$3, weight(%rip)
	testl	%r14d, %r14d
	jg	.LBB37_5
	jmp	.LBB37_98
.LBB37_10:                              # %.thread
	movl	$2, weight(%rip)
	testl	%r14d, %r14d
	jg	.LBB37_33
	jmp	.LBB37_15
.LBB37_11:
	movl	$.L.str.28, %edi
	callq	ErrorExit
.LBB37_12:
	movl	weight(%rip), %eax
	cmpl	$3, %eax
	je	.LBB37_4
# BB#13:
	cmpl	$2, %eax
	jne	.LBB37_98
# BB#14:                                # %.preheader184
	testl	%r14d, %r14d
	jle	.LBB37_15
.LBB37_33:                              # %.preheader183
	leal	-1(%r14), %eax
	leaq	8(,%rax,8), %rdx
	leaq	32(%rsp), %rdi
	xorl	%esi, %esi
	callq	memset
	leal	-2(%r14), %r8d
	cmpl	$3, %r14d
	jl	.LBB37_16
# BB#34:                                # %.preheader182.preheader
	movl	%r8d, %r9d
	xorl	%r10d, %r10d
	movsd	.LCPI37_4(%rip), %xmm0  # xmm0 = mem[0],zero
	movsd	.LCPI37_5(%rip), %xmm1  # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB37_35:                              # %.preheader182
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB37_37 Depth 2
                                        #     Child Loop BB37_40 Depth 2
                                        #     Child Loop BB37_44 Depth 2
                                        #     Child Loop BB37_50 Depth 2
                                        #       Child Loop BB37_51 Depth 3
	movq	(%r15,%r10,8), %rax
	movq	(%rax), %rsi
	movl	(%rsi), %edi
	testl	%edi, %edi
	js	.LBB37_38
# BB#36:                                # %.lr.ph219.preheader
                                        #   in Loop: Header=BB37_35 Depth=1
	leaq	4(%rsi), %rcx
	movl	%edi, %edx
	.p2align	4, 0x90
.LBB37_37:                              # %.lr.ph219
                                        #   Parent Loop BB37_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rdx
	movsd	32(%rsp,%rdx,8), %xmm2  # xmm2 = mem[0],zero
	addsd	%xmm0, %xmm2
	movsd	%xmm2, 32(%rsp,%rdx,8)
	movl	(%rcx), %edx
	addq	$4, %rcx
	testl	%edx, %edx
	jns	.LBB37_37
.LBB37_38:                              # %.preheader181
                                        #   in Loop: Header=BB37_35 Depth=1
	movq	8(%rax), %r12
	movl	(%r12), %r11d
	testl	%r11d, %r11d
	js	.LBB37_41
# BB#39:                                # %.lr.ph222.preheader
                                        #   in Loop: Header=BB37_35 Depth=1
	leaq	4(%r12), %rax
	movl	%r11d, %ecx
	.p2align	4, 0x90
.LBB37_40:                              # %.lr.ph222
                                        #   Parent Loop BB37_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ecx, %rcx
	movsd	32(%rsp,%rcx,8), %xmm2  # xmm2 = mem[0],zero
	addsd	%xmm0, %xmm2
	movsd	%xmm2, 32(%rsp,%rcx,8)
	movl	(%rax), %ecx
	addq	$4, %rax
	testl	%ecx, %ecx
	jns	.LBB37_40
.LBB37_41:                              # %.preheader180
                                        #   in Loop: Header=BB37_35 Depth=1
	testl	%edi, %edi
	js	.LBB37_53
# BB#42:                                # %.preheader179.lr.ph
                                        #   in Loop: Header=BB37_35 Depth=1
	testl	%r11d, %r11d
	js	.LBB37_43
# BB#49:                                # %.preheader179.us.preheader
                                        #   in Loop: Header=BB37_35 Depth=1
	addq	$4, %r12
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB37_50:                              # %.preheader179.us
                                        #   Parent Loop BB37_35 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB37_51 Depth 3
	movslq	%edi, %rax
	movsd	32(%rsp,%rax,8), %xmm2  # xmm2 = mem[0],zero
	movq	%r12, %rax
	movl	%r11d, %ecx
	.p2align	4, 0x90
.LBB37_51:                              #   Parent Loop BB37_35 Depth=1
                                        #     Parent Loop BB37_50 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%ecx, %rcx
	movsd	32(%rsp,%rcx,8), %xmm3  # xmm3 = mem[0],zero
	addsd	%xmm2, %xmm3
	addsd	%xmm1, %xmm3
	cmpl	%ecx, %edi
	movl	%ecx, %edx
	cmovlel	%edi, %edx
	movslq	%edx, %rdx
	movq	(%rbx,%rdx,8), %rdx
	cmovgel	%edi, %ecx
	movslq	%ecx, %rcx
	movsd	%xmm3, (%rdx,%rcx,8)
	movl	(%rax), %ecx
	addq	$4, %rax
	testl	%ecx, %ecx
	jns	.LBB37_51
# BB#52:                                # %._crit_edge225.us
                                        #   in Loop: Header=BB37_50 Depth=2
	movl	4(%rsi,%rbp,4), %edi
	incq	%rbp
	testl	%edi, %edi
	jns	.LBB37_50
	jmp	.LBB37_53
	.p2align	4, 0x90
.LBB37_43:                              # %.preheader179.preheader
                                        #   in Loop: Header=BB37_35 Depth=1
	addq	$4, %rsi
	.p2align	4, 0x90
.LBB37_44:                              # %.preheader179
                                        #   Parent Loop BB37_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, (%rsi)
	leaq	4(%rsi), %rsi
	jns	.LBB37_44
.LBB37_53:                              # %._crit_edge227
                                        #   in Loop: Header=BB37_35 Depth=1
	incq	%r10
	cmpq	%r9, %r10
	jne	.LBB37_35
	jmp	.LBB37_16
.LBB37_4:                               # %.preheader172
	testl	%r14d, %r14d
	jle	.LBB37_98
.LBB37_5:                               # %.lr.ph202.preheader
	leal	-1(%r14), %r13d
	leaq	8(,%r13,8), %rdx
	leaq	32(%rsp), %rdi
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	callq	memset
	movl	%r14d, %eax
	cmpl	$3, %r14d
	jbe	.LBB37_8
# BB#6:                                 # %min.iters.checked
	movl	%r14d, %ecx
	andl	$3, %ecx
	movq	%rax, %rbp
	subq	%rcx, %rbp
	je	.LBB37_7
# BB#62:                                # %vector.body.preheader
	leaq	400048(%rsp), %rdx
	movapd	.LCPI37_0(%rip), %xmm0  # xmm0 = [1.000000e+00,1.000000e+00]
	movq	%rbp, %rsi
	.p2align	4, 0x90
.LBB37_63:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, -16(%rdx)
	movapd	%xmm0, (%rdx)
	addq	$32, %rdx
	addq	$-4, %rsi
	jne	.LBB37_63
# BB#64:                                # %middle.block
	testl	%ecx, %ecx
	jne	.LBB37_8
	jmp	.LBB37_65
.LBB37_15:                              # %.preheader183.thread
	leal	-2(%r14), %r8d
.LBB37_16:                              # %.preheader178
	movslq	%r8d, %rax
	movq	(%r15,%rax,8), %rdx
	movq	(%rdx), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	js	.LBB37_20
# BB#17:                                # %.preheader177.lr.ph
	movq	8(%rdx), %r9
	movl	(%r9), %r8d
	testl	%r8d, %r8d
	js	.LBB37_18
# BB#45:                                # %.preheader177.us.preheader
	addq	$4, %r9
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB37_46:                              # %.preheader177.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB37_47 Depth 2
	movslq	%ecx, %rdx
	movsd	32(%rsp,%rdx,8), %xmm0  # xmm0 = mem[0],zero
	movq	%r9, %rdx
	movl	%r8d, %esi
	.p2align	4, 0x90
.LBB37_47:                              #   Parent Loop BB37_46 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%esi, %rsi
	movsd	32(%rsp,%rsi,8), %xmm1  # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	cmpl	%esi, %ecx
	movl	%esi, %ebp
	cmovlel	%ecx, %ebp
	movslq	%ebp, %rbp
	movq	(%rbx,%rbp,8), %rbp
	cmovgel	%ecx, %esi
	movslq	%esi, %rsi
	movsd	%xmm1, (%rbp,%rsi,8)
	movl	(%rdx), %esi
	addq	$4, %rdx
	testl	%esi, %esi
	jns	.LBB37_47
# BB#48:                                # %._crit_edge216.us
                                        #   in Loop: Header=BB37_46 Depth=1
	movl	4(%rax,%rdi,4), %ecx
	incq	%rdi
	testl	%ecx, %ecx
	jns	.LBB37_46
	jmp	.LBB37_20
.LBB37_18:                              # %.preheader177.preheader
	addq	$4, %rax
	.p2align	4, 0x90
.LBB37_19:                              # %.preheader177
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jns	.LBB37_19
.LBB37_20:                              # %.preheader176
	cmpl	$2, %r14d
	jl	.LBB37_98
# BB#21:                                # %.lr.ph212
	leal	-1(%r14), %eax
	movss	geta2(%rip), %xmm0      # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movslq	%r14d, %r11
	movl	%r14d, %r10d
	movl	%eax, %r15d
	movl	$1, %edx
	xorl	%ecx, %ecx
	movsd	.LCPI37_4(%rip), %xmm1  # xmm1 = mem[0],zero
	movsd	.LCPI37_1(%rip), %xmm2  # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB37_22:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB37_24 Depth 2
                                        #       Child Loop BB37_26 Depth 3
	movq	%rcx, %rax
	leaq	1(%rax), %rcx
	cmpq	%r11, %rcx
	jge	.LBB37_30
# BB#23:                                # %.lr.ph210
                                        #   in Loop: Header=BB37_22 Depth=1
	movq	(%rbx,%rax,8), %rdi
	movq	%rdx, %rax
	.p2align	4, 0x90
.LBB37_24:                              #   Parent Loop BB37_22 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB37_26 Depth 3
	cvttsd2si	(%rdi,%rax,8), %ebp
	testl	%ebp, %ebp
	movapd	%xmm1, %xmm3
	je	.LBB37_29
# BB#25:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB37_24 Depth=2
	movapd	%xmm1, %xmm3
	movapd	%xmm2, %xmm4
	.p2align	4, 0x90
.LBB37_26:                              # %.lr.ph.i
                                        #   Parent Loop BB37_22 Depth=1
                                        #     Parent Loop BB37_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testb	$1, %bpl
	je	.LBB37_28
# BB#27:                                # %.lr.ph.i
                                        #   in Loop: Header=BB37_26 Depth=3
	mulsd	%xmm4, %xmm3
.LBB37_28:                              # %.lr.ph.i
                                        #   in Loop: Header=BB37_26 Depth=3
	mulsd	%xmm4, %xmm4
	sarl	%ebp
	jne	.LBB37_26
.LBB37_29:                              # %ipower.exit
                                        #   in Loop: Header=BB37_24 Depth=2
	addsd	%xmm0, %xmm3
	movsd	%xmm3, (%rdi,%rax,8)
	incq	%rax
	cmpq	%r10, %rax
	jne	.LBB37_24
.LBB37_30:                              # %.loopexit175
                                        #   in Loop: Header=BB37_22 Depth=1
	incq	%rdx
	cmpq	%r15, %rcx
	jne	.LBB37_22
# BB#31:                                # %.preheader174
	cmpl	$2, %r14d
	jl	.LBB37_98
# BB#32:                                # %.lr.ph207.preheader
	leaq	3(%r10), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	-2(%r10), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	addb	$3, %r14b
	leaq	24(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$1, %r13d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB37_55:                              # %.lr.ph207
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB37_58 Depth 2
                                        #     Child Loop BB37_61 Depth 2
	movq	%r12, %rdi
	leaq	1(%rdi), %r12
	cmpq	%r11, %r12
	jge	.LBB37_54
# BB#56:                                # %.lr.ph205
                                        #   in Loop: Header=BB37_55 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	subq	%rdi, %rcx
	movq	24(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	subl	%edi, %eax
	movq	(%rbx,%rdi,8), %rdx
	testb	$3, %al
	movq	%r13, %rbp
	je	.LBB37_59
# BB#57:                                # %.prol.preheader
                                        #   in Loop: Header=BB37_55 Depth=1
	movl	%r14d, %eax
	andb	$3, %al
	movzbl	%al, %eax
	negq	%rax
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB37_58:                              #   Parent Loop BB37_55 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rbp,8), %r8
	movq	(%rbx,%rbp,8), %r9
	movq	%r8, (%r9,%rdi,8)
	incq	%rbp
	incq	%rax
	jne	.LBB37_58
.LBB37_59:                              # %.prol.loopexit
                                        #   in Loop: Header=BB37_55 Depth=1
	cmpq	$3, %rcx
	jb	.LBB37_54
# BB#60:                                # %.lr.ph205.new
                                        #   in Loop: Header=BB37_55 Depth=1
	movq	%r10, %rcx
	subq	%rbp, %rcx
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%rax,%rbp,8), %rax
	leaq	24(%rdx,%rbp,8), %rdx
	.p2align	4, 0x90
.LBB37_61:                              #   Parent Loop BB37_55 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rdx), %rbp
	movq	-24(%rax), %rsi
	movq	%rbp, (%rsi,%rdi,8)
	movq	-16(%rdx), %rsi
	movq	-16(%rax), %rbp
	movq	%rsi, (%rbp,%rdi,8)
	movq	-8(%rdx), %rsi
	movq	-8(%rax), %rbp
	movq	%rsi, (%rbp,%rdi,8)
	movq	(%rdx), %rsi
	movq	(%rax), %rbp
	movq	%rsi, (%rbp,%rdi,8)
	addq	$32, %rax
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB37_61
.LBB37_54:                              # %.loopexit173
                                        #   in Loop: Header=BB37_55 Depth=1
	incq	%r13
	addb	$3, %r14b
	cmpq	%r15, %r12
	jne	.LBB37_55
	jmp	.LBB37_98
.LBB37_7:
	xorl	%ebp, %ebp
.LBB37_8:                               # %.lr.ph202.preheader330
	leaq	400032(%rsp,%rbp,8), %rcx
	movq	%rax, %rdx
	subq	%rbp, %rdx
	movabsq	$4607182418800017408, %rsi # imm = 0x3FF0000000000000
	.p2align	4, 0x90
.LBB37_9:                               # %.lr.ph202
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, (%rcx)
	addq	$8, %rcx
	decq	%rdx
	jne	.LBB37_9
.LBB37_65:                              # %.preheader171
	cmpl	$2, %r14d
	jl	.LBB37_74
# BB#66:                                # %.preheader170.preheader
	movl	%r13d, %r8d
	xorl	%edx, %edx
	movsd	.LCPI37_1(%rip), %xmm0  # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB37_67:                              # %.preheader170
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB37_69 Depth 2
                                        #     Child Loop BB37_72 Depth 2
	movq	(%r15,%rdx,8), %rsi
	movq	(%rsi), %rdi
	movl	(%rdi), %ebp
	testl	%ebp, %ebp
	js	.LBB37_70
# BB#68:                                # %.lr.ph195
                                        #   in Loop: Header=BB37_67 Depth=1
	movq	(%r12,%rdx,8), %rcx
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	addq	$4, %rdi
	.p2align	4, 0x90
.LBB37_69:                              #   Parent Loop BB37_67 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebp, %rcx
	movsd	400032(%rsp,%rcx,8), %xmm2 # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm3
	mulsd	%xmm2, %xmm3
	addsd	32(%rsp,%rcx,8), %xmm3
	movsd	%xmm3, 32(%rsp,%rcx,8)
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, 400032(%rsp,%rcx,8)
	movl	(%rdi), %ebp
	addq	$4, %rdi
	testl	%ebp, %ebp
	jns	.LBB37_69
.LBB37_70:                              # %.preheader169
                                        #   in Loop: Header=BB37_67 Depth=1
	movq	8(%rsi), %rsi
	movl	(%rsi), %edi
	testl	%edi, %edi
	js	.LBB37_73
# BB#71:                                # %.lr.ph198
                                        #   in Loop: Header=BB37_67 Depth=1
	movq	(%r12,%rdx,8), %rcx
	movsd	8(%rcx), %xmm1          # xmm1 = mem[0],zero
	addq	$4, %rsi
	.p2align	4, 0x90
.LBB37_72:                              #   Parent Loop BB37_67 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edi, %rcx
	movsd	400032(%rsp,%rcx,8), %xmm2 # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm3
	mulsd	%xmm2, %xmm3
	addsd	32(%rsp,%rcx,8), %xmm3
	movsd	%xmm3, 32(%rsp,%rcx,8)
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, 400032(%rsp,%rcx,8)
	movl	(%rsi), %edi
	addq	$4, %rsi
	testl	%edi, %edi
	jns	.LBB37_72
.LBB37_73:                              # %._crit_edge199
                                        #   in Loop: Header=BB37_67 Depth=1
	incq	%rdx
	cmpq	%r8, %rdx
	jne	.LBB37_67
.LBB37_74:                              # %.preheader168
	testl	%r14d, %r14d
	jle	.LBB37_98
# BB#75:                                # %.lr.ph193.preheader
	cmpl	$3, %r14d
	jbe	.LBB37_76
# BB#79:                                # %min.iters.checked312
	movl	%r14d, %edx
	andl	$3, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB37_76
# BB#80:                                # %vector.body308.preheader
	leaq	48(%rsp), %rsi
	movapd	.LCPI37_2(%rip), %xmm0  # xmm0 = [1.000000e-03,1.000000e-03]
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB37_81:                              # %vector.body308
                                        # =>This Inner Loop Header: Depth=1
	movapd	-16(%rsi), %xmm1
	addpd	%xmm0, %xmm1
	movapd	(%rsi), %xmm2
	addpd	%xmm0, %xmm2
	movapd	%xmm1, -16(%rsi)
	movapd	%xmm2, (%rsi)
	addq	$32, %rsi
	addq	$-4, %rdi
	jne	.LBB37_81
# BB#82:                                # %middle.block309
	testl	%edx, %edx
	jne	.LBB37_77
	jmp	.LBB37_83
.LBB37_76:
	xorl	%ecx, %ecx
.LBB37_77:                              # %.lr.ph193.preheader329
	leaq	32(%rsp,%rcx,8), %rdx
	movq	%rax, %rsi
	subq	%rcx, %rsi
	movsd	.LCPI37_3(%rip), %xmm0  # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB37_78:                              # %.lr.ph193
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdx)
	addq	$8, %rdx
	decq	%rsi
	jne	.LBB37_78
.LBB37_83:                              # %.preheader167
	testl	%r14d, %r14d
	jle	.LBB37_98
# BB#84:                                # %.preheader.us.preheader
	movl	%eax, %r8d
	andl	$1, %r8d
	movsd	32(%rsp), %xmm0         # xmm0 = mem[0],zero
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB37_85:                              # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB37_92 Depth 2
	testq	%r8, %r8
	movsd	32(%rsp,%rcx,8), %xmm1  # xmm1 = mem[0],zero
	movq	(%rbx,%rcx,8), %rsi
	jne	.LBB37_87
# BB#86:                                #   in Loop: Header=BB37_85 Depth=1
	xorl	%edi, %edi
	cmpl	$1, %r14d
	jne	.LBB37_91
	jmp	.LBB37_97
	.p2align	4, 0x90
.LBB37_87:                              #   in Loop: Header=BB37_85 Depth=1
	testq	%rcx, %rcx
	movq	%rcx, %rdx
	movapd	%xmm1, %xmm2
	je	.LBB37_89
# BB#88:                                #   in Loop: Header=BB37_85 Depth=1
	movapd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	xorl	%edx, %edx
.LBB37_89:                              #   in Loop: Header=BB37_85 Depth=1
	movslq	%edx, %rdx
	movsd	%xmm2, (%rsi,%rdx,8)
	movl	$1, %edi
	cmpl	$1, %r14d
	je	.LBB37_97
.LBB37_91:                              # %.preheader.us.new
                                        #   in Loop: Header=BB37_85 Depth=1
	leaq	-1(%rcx), %rbp
	.p2align	4, 0x90
.LBB37_92:                              #   Parent Loop BB37_85 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rdi, %rcx
	movq	%rcx, %rdx
	movapd	%xmm1, %xmm2
	je	.LBB37_94
# BB#93:                                #   in Loop: Header=BB37_92 Depth=2
	movsd	32(%rsp,%rdi,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movq	%rdi, %rdx
.LBB37_94:                              #   in Loop: Header=BB37_92 Depth=2
	movslq	%edx, %rdx
	movsd	%xmm2, (%rsi,%rdx,8)
	cmpq	%rdi, %rbp
	movq	%rcx, %rdx
	movapd	%xmm1, %xmm2
	je	.LBB37_96
# BB#95:                                #   in Loop: Header=BB37_92 Depth=2
	leaq	1(%rdi), %rdx
	movsd	40(%rsp,%rdi,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
.LBB37_96:                              #   in Loop: Header=BB37_92 Depth=2
	movslq	%edx, %rdx
	movsd	%xmm2, (%rsi,%rdx,8)
	addq	$2, %rdi
	cmpq	%rdi, %rax
	jne	.LBB37_92
.LBB37_97:                              # %._crit_edge.us
                                        #   in Loop: Header=BB37_85 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jne	.LBB37_85
.LBB37_98:                              # %.loopexit
	addq	$800040, %rsp           # imm = 0xC3528
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end37:
	.size	counteff, .Lfunc_end37-counteff
	.cfi_endproc

	.globl	score_calcp
	.p2align	4, 0x90
	.type	score_calcp,@function
score_calcp:                            # @score_calcp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi350:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi351:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi352:
	.cfi_def_cfa_offset 32
.Lcfi353:
	.cfi_offset %rbx, -32
.Lcfi354:
	.cfi_offset %r14, -24
.Lcfi355:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	testl	%edx, %edx
	jle	.LBB38_1
# BB#2:                                 # %.lr.ph64
	leal	-2(%rdx), %r10d
	cvtsi2ssl	penalty(%rip), %xmm1
	leaq	1(%rsi), %r8
	leaq	1(%rdi), %r9
	xorps	%xmm0, %xmm0
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB38_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB38_12 Depth 2
                                        #     Child Loop BB38_8 Depth 2
	movslq	%ecx, %rax
	movsbq	(%rdi,%rax), %r11
	cmpq	$45, %r11
	movb	(%rsi,%rax), %bl
	jne	.LBB38_5
# BB#4:                                 #   in Loop: Header=BB38_3 Depth=1
	cmpb	$45, %bl
	je	.LBB38_14
.LBB38_5:                               #   in Loop: Header=BB38_3 Depth=1
	movsbq	%bl, %r14
	movl	%r11d, %ebp
	shlq	$9, %r11
	cvtsi2ssl	amino_dis(%r11,%r14,4), %xmm2
	addss	%xmm2, %xmm0
	cmpb	$45, %bpl
	jne	.LBB38_9
# BB#6:                                 #   in Loop: Header=BB38_3 Depth=1
	addss	%xmm1, %xmm0
	addss	%xmm0, %xmm2
	cmpb	$45, 1(%rdi,%rax)
	jne	.LBB38_13
# BB#7:                                 # %.lr.ph57.preheader
                                        #   in Loop: Header=BB38_3 Depth=1
	incq	%rax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis+23040(,%r14,4), %xmm0
	addq	%r9, %rax
	.p2align	4, 0x90
.LBB38_8:                               # %.lr.ph57
                                        #   Parent Loop BB38_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addss	%xmm0, %xmm2
	incl	%ecx
	cmpb	$45, (%rax)
	leaq	1(%rax), %rax
	je	.LBB38_8
	jmp	.LBB38_13
	.p2align	4, 0x90
.LBB38_9:                               #   in Loop: Header=BB38_3 Depth=1
	cmpb	$45, %bl
	jne	.LBB38_14
# BB#10:                                #   in Loop: Header=BB38_3 Depth=1
	addss	%xmm1, %xmm0
	addss	%xmm0, %xmm2
	cmpb	$45, 1(%rsi,%rax)
	jne	.LBB38_13
# BB#11:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB38_3 Depth=1
	incq	%rax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis+180(%r11), %xmm0
	addq	%r8, %rax
	.p2align	4, 0x90
.LBB38_12:                              # %.lr.ph
                                        #   Parent Loop BB38_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addss	%xmm0, %xmm2
	incl	%ecx
	cmpb	$45, (%rax)
	leaq	1(%rax), %rax
	je	.LBB38_12
	.p2align	4, 0x90
.LBB38_13:                              # %._crit_edge
                                        #   in Loop: Header=BB38_3 Depth=1
	cmpl	%r10d, %ecx
	movaps	%xmm2, %xmm0
	jg	.LBB38_15
.LBB38_14:                              #   in Loop: Header=BB38_3 Depth=1
	incl	%ecx
	cmpl	%edx, %ecx
	jl	.LBB38_3
	jmp	.LBB38_15
.LBB38_1:
	xorps	%xmm0, %xmm0
.LBB38_15:                              # %._crit_edge65
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end38:
	.size	score_calcp, .Lfunc_end38-score_calcp
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI39_0:
	.long	1065353216              # float 1
	.text
	.globl	score_calc1
	.p2align	4, 0x90
	.type	score_calc1,@function
score_calc1:                            # @score_calc1
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi356:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi357:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi358:
	.cfi_def_cfa_offset 32
.Lcfi359:
	.cfi_offset %rbx, -24
.Lcfi360:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	strlen
	movss	.LCPI39_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	testl	%eax, %eax
	jle	.LBB39_8
# BB#1:                                 # %.lr.ph.preheader
	movl	%eax, %ecx
	xorps	%xmm1, %xmm1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB39_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbq	(%rbx), %rdx
	cmpq	$45, %rdx
	je	.LBB39_5
# BB#3:                                 #   in Loop: Header=BB39_2 Depth=1
	movsbq	(%r14), %rsi
	cmpq	$45, %rsi
	je	.LBB39_5
# BB#4:                                 #   in Loop: Header=BB39_2 Depth=1
	shlq	$9, %rdx
	cvtsi2ssl	amino_dis(%rdx,%rsi,4), %xmm2
	addss	%xmm2, %xmm1
	incl	%eax
.LBB39_5:                               #   in Loop: Header=BB39_2 Depth=1
	incq	%rbx
	incq	%r14
	decq	%rcx
	jne	.LBB39_2
# BB#6:                                 # %._crit_edge
	testl	%eax, %eax
	je	.LBB39_8
# BB#7:
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
.LBB39_8:                               # %._crit_edge.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end39:
	.size	score_calc1, .Lfunc_end39-score_calc1
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI40_0:
	.long	1065353216              # float 1
	.text
	.globl	substitution_nid
	.p2align	4, 0x90
	.type	substitution_nid,@function
substitution_nid:                       # @substitution_nid
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi361:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi362:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi363:
	.cfi_def_cfa_offset 32
.Lcfi364:
	.cfi_offset %rbx, -24
.Lcfi365:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	strlen
	testl	%eax, %eax
	jle	.LBB40_1
# BB#2:                                 # %.lr.ph.preheader
	movl	%eax, %esi
	testb	$1, %al
	jne	.LBB40_4
# BB#3:
	xorps	%xmm0, %xmm0
	xorl	%edx, %edx
	cmpq	$1, %rsi
	jne	.LBB40_11
	jmp	.LBB40_23
.LBB40_1:
	xorps	%xmm0, %xmm0
	jmp	.LBB40_23
.LBB40_4:                               # %.lr.ph.prol
	movb	(%rbx), %al
	xorps	%xmm0, %xmm0
	cmpb	$45, %al
	je	.LBB40_9
# BB#5:
	movb	(%r14), %dl
	cmpb	$45, %dl
	je	.LBB40_9
# BB#6:
	cmpb	%dl, %al
	je	.LBB40_7
# BB#8:
	xorps	%xmm0, %xmm0
	jmp	.LBB40_9
.LBB40_7:
	movss	.LCPI40_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
.LBB40_9:
	movl	$1, %edx
	cmpq	$1, %rsi
	je	.LBB40_23
.LBB40_11:                              # %.lr.ph.preheader.new
	subq	%rdx, %rsi
	leaq	1(%rbx,%rdx), %rax
	leaq	1(%r14,%rdx), %rdx
	movss	.LCPI40_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB40_12:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rax), %ebx
	cmpb	$45, %bl
	je	.LBB40_17
# BB#13:                                #   in Loop: Header=BB40_12 Depth=1
	movzbl	-1(%rdx), %ecx
	cmpb	$45, %cl
	je	.LBB40_17
# BB#14:                                #   in Loop: Header=BB40_12 Depth=1
	cmpb	%cl, %bl
	movaps	%xmm1, %xmm2
	je	.LBB40_16
# BB#15:                                #   in Loop: Header=BB40_12 Depth=1
	xorps	%xmm2, %xmm2
.LBB40_16:                              #   in Loop: Header=BB40_12 Depth=1
	addss	%xmm2, %xmm0
.LBB40_17:                              # %.lr.ph.121
                                        #   in Loop: Header=BB40_12 Depth=1
	movzbl	(%rax), %ebx
	cmpb	$45, %bl
	je	.LBB40_22
# BB#18:                                #   in Loop: Header=BB40_12 Depth=1
	movzbl	(%rdx), %ecx
	cmpb	$45, %cl
	je	.LBB40_22
# BB#19:                                #   in Loop: Header=BB40_12 Depth=1
	cmpb	%cl, %bl
	movaps	%xmm1, %xmm2
	je	.LBB40_21
# BB#20:                                #   in Loop: Header=BB40_12 Depth=1
	xorps	%xmm2, %xmm2
.LBB40_21:                              #   in Loop: Header=BB40_12 Depth=1
	addss	%xmm2, %xmm0
.LBB40_22:                              #   in Loop: Header=BB40_12 Depth=1
	addq	$2, %rax
	addq	$2, %rdx
	addq	$-2, %rsi
	jne	.LBB40_12
.LBB40_23:                              # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end40:
	.size	substitution_nid, .Lfunc_end40-substitution_nid
	.cfi_endproc

	.globl	substitution_score
	.p2align	4, 0x90
	.type	substitution_score,@function
substitution_score:                     # @substitution_score
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi366:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi367:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi368:
	.cfi_def_cfa_offset 32
.Lcfi369:
	.cfi_offset %rbx, -24
.Lcfi370:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	strlen
	testl	%eax, %eax
	jle	.LBB41_1
# BB#2:                                 # %.lr.ph.preheader
	movl	%eax, %eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB41_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbq	(%rbx), %rcx
	cmpq	$45, %rcx
	je	.LBB41_6
# BB#4:                                 #   in Loop: Header=BB41_3 Depth=1
	movsbq	(%r14), %rdx
	cmpq	$45, %rdx
	je	.LBB41_6
# BB#5:                                 #   in Loop: Header=BB41_3 Depth=1
	shlq	$9, %rcx
	cvtsi2ssl	amino_dis(%rcx,%rdx,4), %xmm1
	addss	%xmm1, %xmm0
.LBB41_6:                               #   in Loop: Header=BB41_3 Depth=1
	incq	%rbx
	incq	%r14
	decq	%rax
	jne	.LBB41_3
	jmp	.LBB41_7
.LBB41_1:
	xorps	%xmm0, %xmm0
.LBB41_7:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end41:
	.size	substitution_score, .Lfunc_end41-substitution_score
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI42_0:
	.long	1077936128              # float 3
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI42_1:
	.quad	4606732058837280358     # double 0.94999999999999996
.LCPI42_2:
	.quad	4607182418800017408     # double 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI42_3:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	substitution_hosei
	.p2align	4, 0x90
	.type	substitution_hosei,@function
substitution_hosei:                     # @substitution_hosei
	.cfi_startproc
# BB#0:
	movb	(%rdi), %dl
	movss	.LCPI42_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	testb	%dl, %dl
	je	.LBB42_10
# BB#1:                                 # %.lr.ph.preheader
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
.LBB42_2:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB42_3 Depth 2
	incq	%rdi
	incq	%rsi
	.p2align	4, 0x90
.LBB42_3:                               #   Parent Loop BB42_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$45, %dl
	je	.LBB42_4
# BB#6:                                 #   in Loop: Header=BB42_3 Depth=2
	movzbl	-1(%rsi), %eax
	cmpb	$45, %al
	jne	.LBB42_7
.LBB42_4:                               # %.backedge
                                        #   in Loop: Header=BB42_3 Depth=2
	movzbl	(%rdi), %edx
	incq	%rdi
	incq	%rsi
	testb	%dl, %dl
	jne	.LBB42_3
	jmp	.LBB42_5
	.p2align	4, 0x90
.LBB42_7:                               # %.outer
                                        #   in Loop: Header=BB42_2 Depth=1
	xorl	%ecx, %ecx
	cmpb	%al, %dl
	setne	%cl
	addl	%ecx, %r8d
	incl	%r9d
	movb	(%rdi), %dl
	testb	%dl, %dl
	jne	.LBB42_2
	jmp	.LBB42_8
.LBB42_5:                               # %.outer._crit_edge
	testl	%r9d, %r9d
	je	.LBB42_10
.LBB42_8:                               # %.outer._crit_edge.thread47
	cvtsi2ssl	%r8d, %xmm1
	cvtsi2ssl	%r9d, %xmm2
	divss	%xmm2, %xmm1
	cvtss2sd	%xmm1, %xmm1
	movsd	.LCPI42_1(%rip), %xmm2  # xmm2 = mem[0],zero
	ucomisd	%xmm1, %xmm2
	jbe	.LBB42_10
# BB#9:
	pushq	%rax
.Lcfi371:
	.cfi_def_cfa_offset 16
	movsd	.LCPI42_2(%rip), %xmm0  # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
	callq	log
	cvtsd2ss	%xmm0, %xmm0
	xorps	.LCPI42_3(%rip), %xmm0
	addq	$8, %rsp
.LBB42_10:                              # %.thread
	retq
.Lfunc_end42:
	.size	substitution_hosei, .Lfunc_end42-substitution_hosei
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI43_0:
	.long	1065353216              # float 1
	.text
	.globl	substitution
	.p2align	4, 0x90
	.type	substitution,@function
substitution:                           # @substitution
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi372:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi373:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi374:
	.cfi_def_cfa_offset 32
.Lcfi375:
	.cfi_offset %rbx, -24
.Lcfi376:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	strlen
	movss	.LCPI43_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	testl	%eax, %eax
	jle	.LBB43_25
# BB#1:                                 # %.lr.ph.preheader
	movl	%eax, %ecx
	testb	$1, %al
	jne	.LBB43_3
# BB#2:
	xorps	%xmm1, %xmm1
	xorl	%esi, %esi
	xorl	%edi, %edi
	cmpq	$1, %rcx
	jne	.LBB43_11
	jmp	.LBB43_23
.LBB43_3:                               # %.lr.ph.prol
	movb	(%rbx), %dl
	xorl	%edi, %edi
	xorps	%xmm1, %xmm1
	cmpb	$45, %dl
	je	.LBB43_9
# BB#4:
	movb	(%r14), %sil
	cmpb	$45, %sil
	je	.LBB43_9
# BB#5:
	cmpb	%sil, %dl
	jne	.LBB43_6
# BB#7:
	xorps	%xmm1, %xmm1
	jmp	.LBB43_8
.LBB43_6:
	movss	.LCPI43_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
.LBB43_8:
	movl	$1, %edi
.LBB43_9:                               # %.lr.ph.prol.loopexit
	movl	$1, %esi
	cmpq	$1, %rcx
	je	.LBB43_23
.LBB43_11:                              # %.lr.ph.preheader.new
	subq	%rsi, %rcx
	leaq	1(%rbx,%rsi), %rdx
	leaq	1(%r14,%rsi), %rsi
	movss	.LCPI43_0(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB43_12:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rdx), %ebx
	cmpb	$45, %bl
	je	.LBB43_17
# BB#13:                                #   in Loop: Header=BB43_12 Depth=1
	movzbl	-1(%rsi), %eax
	cmpb	$45, %al
	je	.LBB43_17
# BB#14:                                #   in Loop: Header=BB43_12 Depth=1
	cmpb	%al, %bl
	movaps	%xmm2, %xmm3
	jne	.LBB43_16
# BB#15:                                #   in Loop: Header=BB43_12 Depth=1
	xorps	%xmm3, %xmm3
.LBB43_16:                              #   in Loop: Header=BB43_12 Depth=1
	addss	%xmm3, %xmm1
	incl	%edi
.LBB43_17:                              # %.lr.ph.131
                                        #   in Loop: Header=BB43_12 Depth=1
	movzbl	(%rdx), %ebx
	cmpb	$45, %bl
	je	.LBB43_22
# BB#18:                                #   in Loop: Header=BB43_12 Depth=1
	movzbl	(%rsi), %eax
	cmpb	$45, %al
	je	.LBB43_22
# BB#19:                                #   in Loop: Header=BB43_12 Depth=1
	cmpb	%al, %bl
	movaps	%xmm2, %xmm3
	jne	.LBB43_21
# BB#20:                                #   in Loop: Header=BB43_12 Depth=1
	xorps	%xmm3, %xmm3
.LBB43_21:                              #   in Loop: Header=BB43_12 Depth=1
	addss	%xmm3, %xmm1
	incl	%edi
.LBB43_22:                              #   in Loop: Header=BB43_12 Depth=1
	addq	$2, %rdx
	addq	$2, %rsi
	addq	$-2, %rcx
	jne	.LBB43_12
.LBB43_23:                              # %._crit_edge
	testl	%edi, %edi
	je	.LBB43_25
# BB#24:
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edi, %xmm0
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
.LBB43_25:                              # %._crit_edge.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end43:
	.size	substitution, .Lfunc_end43-substitution
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI44_0:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
.LCPI44_4:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI44_1:
	.long	1077936128              # float 3
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI44_2:
	.quad	4606732058837280358     # double 0.94999999999999996
.LCPI44_3:
	.quad	4607182418800017408     # double 1
	.text
	.globl	treeconstruction
	.p2align	4, 0x90
	.type	treeconstruction,@function
treeconstruction:                       # @treeconstruction
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi377:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi378:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi379:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi380:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi381:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi382:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi383:
	.cfi_def_cfa_offset 112
.Lcfi384:
	.cfi_offset %rbx, -56
.Lcfi385:
	.cfi_offset %r12, -48
.Lcfi386:
	.cfi_offset %r13, -40
.Lcfi387:
	.cfi_offset %r14, -32
.Lcfi388:
	.cfi_offset %r15, -24
.Lcfi389:
	.cfi_offset %rbp, -16
	movq	%r8, %r13
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r14
	cmpl	$1, weight(%rip)
	jle	.LBB44_1
# BB#13:
	cmpl	$0, utree(%rip)
	jne	.LBB44_12
# BB#14:                                # %.preheader
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, (%rsp)            # 8-byte Spill
	cmpl	$2, %esi
	jl	.LBB44_30
# BB#15:                                # %.lr.ph50.preheader
	movq	(%rsp), %rax            # 8-byte Reload
	leal	-1(%rax), %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movslq	%eax, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	%eax, %r12d
	movl	$1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r15d, %r15d
	movss	.LCPI44_1(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	movsd	.LCPI44_2(%rip), %xmm3  # xmm3 = mem[0],zero
	.p2align	4, 0x90
.LBB44_17:                              # %.lr.ph50
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB44_19 Depth 2
                                        #       Child Loop BB44_21 Depth 3
                                        #         Child Loop BB44_22 Depth 4
	leaq	1(%r15), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	jge	.LBB44_16
# BB#18:                                # %.lr.ph
                                        #   in Loop: Header=BB44_17 Depth=1
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB44_19:                              #   Parent Loop BB44_17 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB44_21 Depth 3
                                        #         Child Loop BB44_22 Depth 4
	movq	(%r14,%r15,8), %rcx
	movb	(%rcx), %bl
	testb	%bl, %bl
	movaps	%xmm2, %xmm0
	je	.LBB44_29
# BB#20:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB44_19 Depth=2
	movq	(%r14,%rbp,8), %rsi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
.LBB44_21:                              # %.lr.ph.i
                                        #   Parent Loop BB44_17 Depth=1
                                        #     Parent Loop BB44_19 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB44_22 Depth 4
	incq	%rcx
	incq	%rsi
	.p2align	4, 0x90
.LBB44_22:                              #   Parent Loop BB44_17 Depth=1
                                        #     Parent Loop BB44_19 Depth=2
                                        #       Parent Loop BB44_21 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpb	$45, %bl
	je	.LBB44_23
# BB#25:                                #   in Loop: Header=BB44_22 Depth=4
	movzbl	-1(%rsi), %eax
	cmpb	$45, %al
	jne	.LBB44_26
.LBB44_23:                              # %.backedge.i
                                        #   in Loop: Header=BB44_22 Depth=4
	movzbl	(%rcx), %ebx
	incq	%rcx
	incq	%rsi
	testb	%bl, %bl
	jne	.LBB44_22
	jmp	.LBB44_24
	.p2align	4, 0x90
.LBB44_26:                              # %.outer.i
                                        #   in Loop: Header=BB44_21 Depth=3
	xorl	%edi, %edi
	cmpb	%al, %bl
	setne	%dil
	addl	%edi, %r8d
	incl	%edx
	movb	(%rcx), %bl
	testb	%bl, %bl
	jne	.LBB44_21
	jmp	.LBB44_27
	.p2align	4, 0x90
.LBB44_24:                              # %.outer._crit_edge.i
                                        #   in Loop: Header=BB44_19 Depth=2
	testl	%edx, %edx
	movaps	%xmm2, %xmm0
	je	.LBB44_29
.LBB44_27:                              # %.outer._crit_edge.thread47.i
                                        #   in Loop: Header=BB44_19 Depth=2
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r8d, %xmm0
	cvtsi2ssl	%edx, %xmm1
	divss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm3
	movaps	%xmm2, %xmm0
	jbe	.LBB44_29
# BB#28:                                #   in Loop: Header=BB44_19 Depth=2
	movsd	.LCPI44_3(%rip), %xmm0  # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
	callq	log
	movsd	.LCPI44_2(%rip), %xmm3  # xmm3 = mem[0],zero
	movss	.LCPI44_1(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	cvtsd2ss	%xmm0, %xmm0
	xorps	.LCPI44_4(%rip), %xmm0
	.p2align	4, 0x90
.LBB44_29:                              # %substitution_hosei.exit
                                        #   in Loop: Header=BB44_19 Depth=2
	cvtss2sd	%xmm0, %xmm0
	movq	(%r13,%r15,8), %rax
	movsd	%xmm0, (%rax,%rbp,8)
	incq	%rbp
	cmpq	%r12, %rbp
	jne	.LBB44_19
.LBB44_16:                              # %.loopexit
                                        #   in Loop: Header=BB44_17 Depth=1
	incq	8(%rsp)                 # 8-byte Folded Spill
	movq	48(%rsp), %r15          # 8-byte Reload
	cmpq	40(%rsp), %r15          # 8-byte Folded Reload
	jne	.LBB44_17
.LBB44_30:                              # %._crit_edge
	movq	(%rsp), %r14            # 8-byte Reload
	movl	%r14d, %edi
	movq	%r13, %rsi
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rcx
	callq	spg
	movl	%r14d, %edi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	movq	%r13, %rcx
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	counteff                # TAILCALL
.LBB44_1:                               # %.preheader36
	testl	%esi, %esi
	jle	.LBB44_12
# BB#2:                                 # %.preheader35.us.preheader
	movl	%esi, %eax
	movl	%esi, %r8d
	andl	$3, %r8d
	movq	%rax, %r9
	subq	%r8, %r9
	xorl	%ecx, %ecx
	movabsq	$4607182418800017408, %rdi # imm = 0x3FF0000000000000
	movaps	.LCPI44_0(%rip), %xmm0  # xmm0 = [1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB44_3:                               # %.preheader35.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB44_8 Depth 2
                                        #     Child Loop BB44_10 Depth 2
	cmpl	$4, %esi
	movq	(%r13,%rcx,8), %rbp
	jae	.LBB44_5
# BB#4:                                 #   in Loop: Header=BB44_3 Depth=1
	xorl	%edx, %edx
	jmp	.LBB44_10
	.p2align	4, 0x90
.LBB44_5:                               # %min.iters.checked
                                        #   in Loop: Header=BB44_3 Depth=1
	testq	%r9, %r9
	je	.LBB44_6
# BB#7:                                 # %vector.body.preheader
                                        #   in Loop: Header=BB44_3 Depth=1
	leaq	16(%rbp), %rdx
	movq	%r9, %rbx
	.p2align	4, 0x90
.LBB44_8:                               # %vector.body
                                        #   Parent Loop BB44_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm0, -16(%rdx)
	movups	%xmm0, (%rdx)
	addq	$32, %rdx
	addq	$-4, %rbx
	jne	.LBB44_8
# BB#9:                                 # %middle.block
                                        #   in Loop: Header=BB44_3 Depth=1
	testl	%r8d, %r8d
	movq	%r9, %rdx
	jne	.LBB44_10
	jmp	.LBB44_11
.LBB44_6:                               #   in Loop: Header=BB44_3 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB44_10:                              # %scalar.ph
                                        #   Parent Loop BB44_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdi, (%rbp,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rax
	jne	.LBB44_10
.LBB44_11:                              # %._crit_edge53.us
                                        #   in Loop: Header=BB44_3 Depth=1
	incq	%rcx
	cmpq	%rdx, %rcx
	jne	.LBB44_3
.LBB44_12:                              # %.loopexit37
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end44:
	.size	treeconstruction, .Lfunc_end44-treeconstruction
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI45_0:
	.quad	4607182418800017408     # double 1
.LCPI45_1:
	.quad	4645744490609377280     # double 400
	.text
	.globl	bscore_calc
	.p2align	4, 0x90
	.type	bscore_calc,@function
bscore_calc:                            # @bscore_calc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi390:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi391:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi392:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi393:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi394:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi395:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi396:
	.cfi_def_cfa_offset 176
.Lcfi397:
	.cfi_offset %rbx, -56
.Lcfi398:
	.cfi_offset %r12, -48
.Lcfi399:
	.cfi_offset %r13, -40
.Lcfi400:
	.cfi_offset %r14, -32
.Lcfi401:
	.cfi_offset %r15, -24
.Lcfi402:
	.cfi_offset %rbp, -16
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	%esi, %r15d
	movq	%rdi, (%rsp)            # 8-byte Spill
	movq	(%rdi), %rdi
	callq	strlen
	cmpl	$2, %r15d
	jl	.LBB45_1
# BB#4:                                 # %.lr.ph110
	leal	-1(%r15), %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movslq	penalty(%rip), %rsi
	movslq	%r15d, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%eax, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%r15d, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movl	$1, %edi
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB45_6:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB45_9 Depth 2
                                        #       Child Loop BB45_10 Depth 3
	leaq	1(%rax), %rcx
	cmpq	24(%rsp), %rcx          # 8-byte Folded Reload
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	jge	.LBB45_5
# BB#7:                                 # %.lr.ph103
                                        #   in Loop: Header=BB45_6 Depth=1
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB45_5
# BB#8:                                 # %.lr.ph103.split.us.preheader
                                        #   in Loop: Header=BB45_6 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rax,8), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB45_9:                               # %.lr.ph103.split.us
                                        #   Parent Loop BB45_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB45_10 Depth 3
	movq	80(%rsp), %rax          # 8-byte Reload
	movsd	(%rax,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %r14
	xorl	%ebp, %ebp
	movq	64(%rsp), %r13          # 8-byte Reload
	movq	72(%rsp), %rdi          # 8-byte Reload
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB45_10:                              #   Parent Loop BB45_6 Depth=1
                                        #     Parent Loop BB45_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movl	%r10d, %r8d
	xorl	$-2, %r8d
	incl	%r8d
	movsbq	(%rdi), %rdx
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	cmpq	$45, %rdx
	sete	%al
	setne	%r12b
	movl	%eax, %r11d
	andl	%r8d, %r11d
	movl	%ebp, %r15d
	xorl	$1, %r15d
	negl	%r11d
	movl	%r15d, %ebx
	andl	%r11d, %ebx
	movsbq	(%r14), %rsi
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	cmpq	$45, %rsi
	sete	%r9b
	setne	%cl
	shlq	$9, %rdx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	amino_dis(%rdx,%rsi,4), %xmm1
	movq	96(%rsp), %rsi          # 8-byte Reload
	negl	%ebx
	andl	%ecx, %ebx
	andl	%r12d, %r8d
	negl	%r8d
	andl	%r15d, %r8d
	negl	%r8d
	andl	%r9d, %r8d
	addl	%ebx, %r8d
	andl	%ebp, %r11d
	negl	%r11d
	andl	%ecx, %r11d
	addl	%r8d, %r11d
	negl	%r10d
	movl	%r12d, %edx
	andl	%r10d, %edx
	negl	%edx
	andl	%edx, %r15d
	negl	%r15d
	andl	%r9d, %r15d
	addl	%r11d, %r15d
	andl	%ebp, %edx
	negl	%edx
	andl	%r9d, %edx
	addl	%r15d, %edx
	andl	%eax, %r10d
	negl	%r10d
	andl	%ebp, %r10d
	negl	%r10d
	andl	%ecx, %r10d
	addl	%edx, %r10d
	movslq	%r10d, %rdx
	imulq	%rsi, %rdx
	cvtsi2sdq	%rdx, %xmm2
	mulsd	%xmm0, %xmm2
	movq	104(%rsp), %rdx         # 8-byte Reload
	cvtsi2sdq	%rdx, %xmm3
	addsd	%xmm2, %xmm3
	cvttsd2si	%xmm3, %rdx
	xorps	%xmm2, %xmm2
	cvtsi2sdq	%rdx, %xmm2
	movq	112(%rsp), %rdx         # 8-byte Reload
	andb	%cl, %r12b
	mulsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm2
	movzbl	%r12b, %ecx
	addl	%ecx, %edx
	cvttsd2si	%xmm2, %rbx
	incq	%rdi
	incq	%r14
	decq	%r13
	movl	%r9d, %ebp
	movl	%eax, %r10d
	jne	.LBB45_10
# BB#11:                                # %._crit_edge.us
                                        #   in Loop: Header=BB45_9 Depth=2
	movq	88(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	cmpq	56(%rsp), %rcx          # 8-byte Folded Reload
	jne	.LBB45_9
.LBB45_5:                               # %.loopexit
                                        #   in Loop: Header=BB45_6 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	incq	%rdi
	movq	48(%rsp), %rax          # 8-byte Reload
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	jne	.LBB45_6
	jmp	.LBB45_2
.LBB45_1:
	xorl	%edx, %edx
	xorl	%ebx, %ebx
.LBB45_2:                               # %._crit_edge111
	cvtsi2ssq	%rbx, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%edx, %xmm1
	divss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	cmpl	$0, scoremtx(%rip)
	je	.LBB45_3
# BB#12:                                # %._crit_edge111
	xorps	%xmm1, %xmm1
	jmp	.LBB45_13
.LBB45_3:
	movsd	.LCPI45_0(%rip), %xmm1  # xmm1 = mem[0],zero
.LBB45_13:                              # %._crit_edge111
	mulsd	.LCPI45_1(%rip), %xmm1
	addsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end45:
	.size	bscore_calc, .Lfunc_end45-bscore_calc
	.cfi_endproc

	.globl	AllocateTmpSeqs
	.p2align	4, 0x90
	.type	AllocateTmpSeqs,@function
AllocateTmpSeqs:                        # @AllocateTmpSeqs
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi403:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi404:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi405:
	.cfi_def_cfa_offset 32
.Lcfi406:
	.cfi_offset %rbx, -32
.Lcfi407:
	.cfi_offset %r14, -24
.Lcfi408:
	.cfi_offset %r15, -16
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	njob(%rip), %edi
	incl	%ebx
	movl	%ebx, %esi
	callq	AllocateCharMtx
	movq	%rax, (%r15)
	movl	%ebx, %edi
	callq	AllocateCharVec
	movq	%rax, (%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end46:
	.size	AllocateTmpSeqs, .Lfunc_end46-AllocateTmpSeqs
	.cfi_endproc

	.globl	FreeTmpSeqs
	.p2align	4, 0x90
	.type	FreeTmpSeqs,@function
FreeTmpSeqs:                            # @FreeTmpSeqs
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi409:
	.cfi_def_cfa_offset 16
.Lcfi410:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	callq	FreeCharMtx
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end47:
	.size	FreeTmpSeqs, .Lfunc_end47-FreeTmpSeqs
	.cfi_endproc

	.globl	gappick0
	.p2align	4, 0x90
	.type	gappick0,@function
gappick0:                               # @gappick0
	.cfi_startproc
# BB#0:
	jmp	.LBB48_1
	.p2align	4, 0x90
.LBB48_4:                               #   in Loop: Header=BB48_1 Depth=1
	incq	%rsi
.LBB48_1:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi), %eax
	cmpb	$45, %al
	je	.LBB48_4
# BB#2:                                 #   in Loop: Header=BB48_1 Depth=1
	testb	%al, %al
	je	.LBB48_5
# BB#3:                                 #   in Loop: Header=BB48_1 Depth=1
	movb	%al, (%rdi)
	incq	%rdi
	jmp	.LBB48_4
.LBB48_5:
	movb	$0, (%rdi)
	retq
.Lfunc_end48:
	.size	gappick0, .Lfunc_end48-gappick0
	.cfi_endproc

	.globl	gappick
	.p2align	4, 0x90
	.type	gappick,@function
gappick:                                # @gappick
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi411:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi412:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi413:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi414:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi415:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi416:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi417:
	.cfi_def_cfa_offset 128
.Lcfi418:
	.cfi_offset %rbx, -56
.Lcfi419:
	.cfi_offset %r12, -48
.Lcfi420:
	.cfi_offset %r13, -40
.Lcfi421:
	.cfi_offset %r14, -32
.Lcfi422:
	.cfi_offset %r15, -24
.Lcfi423:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
	movq	%r8, %rbp
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, %r15
	movl	%esi, %r12d
	movl	%edi, %r14d
	movq	(%r15), %rdi
	callq	strlen
	testl	%eax, %eax
	jle	.LBB49_1
# BB#2:                                 # %.preheader65.lr.ph
	testl	%r14d, %r14d
	jle	.LBB49_52
# BB#3:                                 # %.preheader65.us.preheader
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movl	%r12d, %r13d
	movl	%r14d, %edx
	movl	%eax, %r11d
	movl	%edx, %r8d
	andl	$1, %r8d
	leaq	8(%r15), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%r10d, %r10d
	xorl	%edi, %edi
	xorl	%ebx, %ebx
	movl	%r12d, 20(%rsp)         # 4-byte Spill
	movq	%r14, 64(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	%r11, 56(%rsp)          # 8-byte Spill
	movq	%r8, 48(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB49_4:                               # %.preheader65.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB49_31 Depth 2
                                        #     Child Loop BB49_26 Depth 2
	testq	%r8, %r8
	jne	.LBB49_6
# BB#5:                                 #   in Loop: Header=BB49_4 Depth=1
	movl	$1, %ebp
	xorl	%ecx, %ecx
	cmpl	$1, %r14d
	jne	.LBB49_31
	jmp	.LBB49_11
	.p2align	4, 0x90
.LBB49_6:                               #   in Loop: Header=BB49_4 Depth=1
	testl	%r12d, %r12d
	je	.LBB49_7
# BB#8:                                 #   in Loop: Header=BB49_4 Depth=1
	movq	(%r15), %rcx
	xorl	%ebp, %ebp
	cmpb	$45, (%rcx,%rdi)
	sete	%bpl
	jmp	.LBB49_9
.LBB49_7:                               #   in Loop: Header=BB49_4 Depth=1
	movl	$1, %ebp
.LBB49_9:                               # %.prol.loopexit121
                                        #   in Loop: Header=BB49_4 Depth=1
	movl	$1, %ecx
	cmpl	$1, %r14d
	jne	.LBB49_31
.LBB49_11:                              #   in Loop: Header=BB49_4 Depth=1
	movl	$1, %esi
	testl	%ebp, %ebp
	jne	.LBB49_16
	jmp	.LBB49_13
	.p2align	4, 0x90
.LBB49_31:                              #   Parent Loop BB49_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rsi
	cmpq	%rsi, %r13
	je	.LBB49_33
# BB#32:                                #   in Loop: Header=BB49_31 Depth=2
	movq	(%r15,%rsi,8), %rax
	cmpb	$45, (%rax,%rdi)
	cmovnel	%r10d, %ebp
.LBB49_33:                              #   in Loop: Header=BB49_31 Depth=2
	leaq	1(%rsi), %rcx
	cmpq	%r13, %rcx
	je	.LBB49_35
# BB#34:                                #   in Loop: Header=BB49_31 Depth=2
	movq	8(%r15,%rsi,8), %rax
	cmpb	$45, (%rax,%rdi)
	cmovnel	%r10d, %ebp
.LBB49_35:                              #   in Loop: Header=BB49_31 Depth=2
	incq	%rcx
	cmpq	%rdx, %rcx
	jne	.LBB49_31
# BB#36:                                #   in Loop: Header=BB49_4 Depth=1
	addq	$2, %rsi
	testl	%ebp, %ebp
	jne	.LBB49_16
.LBB49_13:                              # %.lr.ph76.us
                                        #   in Loop: Header=BB49_4 Depth=1
	movl	%ebx, %r11d
	movslq	%ebx, %r8
	xorl	%r14d, %r14d
	testb	$1, %sil
	jne	.LBB49_37
# BB#14:                                #   in Loop: Header=BB49_4 Depth=1
	xorl	%ebp, %ebp
	cmpq	$1, %rsi
	jne	.LBB49_41
	jmp	.LBB49_15
.LBB49_37:                              #   in Loop: Header=BB49_4 Depth=1
	testl	%r12d, %r12d
	je	.LBB49_39
# BB#38:                                #   in Loop: Header=BB49_4 Depth=1
	movq	(%r15), %rax
	movb	(%rax,%rdi), %al
	movq	(%r9), %rcx
	movb	%al, (%rcx,%r8)
	movl	$1, %r14d
.LBB49_39:                              # %.prol.loopexit125
                                        #   in Loop: Header=BB49_4 Depth=1
	movl	$1, %ebp
	cmpq	$1, %rsi
	je	.LBB49_15
.LBB49_41:                              # %.lr.ph76.us.new
                                        #   in Loop: Header=BB49_4 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbp,8), %r12
	movq	%r13, %rcx
	subq	%rbp, %rcx
	.p2align	4, 0x90
.LBB49_26:                              #   Parent Loop BB49_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rcx, %rcx
	je	.LBB49_28
# BB#27:                                #   in Loop: Header=BB49_26 Depth=2
	movq	-8(%r12), %r9
	movzbl	(%r9,%rdi), %eax
	movq	8(%rsp), %r9            # 8-byte Reload
	movslq	%r14d, %r14
	movq	(%r9,%r14,8), %rbx
	movb	%al, (%rbx,%r8)
	incl	%r14d
.LBB49_28:                              #   in Loop: Header=BB49_26 Depth=2
	leaq	1(%rbp), %rax
	cmpq	%r13, %rax
	je	.LBB49_30
# BB#29:                                #   in Loop: Header=BB49_26 Depth=2
	movq	(%r12), %rax
	movzbl	(%rax,%rdi), %eax
	movslq	%r14d, %r14
	movq	(%r9,%r14,8), %rbx
	movb	%al, (%rbx,%r8)
	incl	%r14d
.LBB49_30:                              #   in Loop: Header=BB49_26 Depth=2
	addq	$2, %rbp
	addq	$16, %r12
	addq	$-2, %rcx
	cmpq	%rsi, %rbp
	jne	.LBB49_26
.LBB49_15:                              # %._crit_edge77.us
                                        #   in Loop: Header=BB49_4 Depth=1
	movl	%r11d, %ebx
	incl	%ebx
	movl	20(%rsp), %r12d         # 4-byte Reload
	movq	64(%rsp), %r14          # 8-byte Reload
	movq	56(%rsp), %r11          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
.LBB49_16:                              #   in Loop: Header=BB49_4 Depth=1
	incq	%rdi
	cmpq	%r11, %rdi
	jne	.LBB49_4
	jmp	.LBB49_17
.LBB49_1:
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	movq	8(%rsp), %r9            # 8-byte Reload
.LBB49_17:                              # %.preheader63
	cmpl	$2, %r14d
	jl	.LBB49_23
# BB#18:                                # %.lr.ph69
	leal	-1(%r14), %eax
	movslq	%ebx, %rcx
	leaq	-1(%rax), %rdx
	movq	%rax, %rdi
	xorl	%esi, %esi
	andq	$7, %rdi
	je	.LBB49_20
	.p2align	4, 0x90
.LBB49_19:                              # =>This Inner Loop Header: Depth=1
	movq	(%r9,%rsi,8), %rbp
	movb	$0, (%rbp,%rcx)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB49_19
.LBB49_20:                              # %.prol.loopexit117
	cmpq	$7, %rdx
	jb	.LBB49_23
# BB#21:                                # %.lr.ph69.new
	subq	%rsi, %rax
	leaq	56(%r9,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB49_22:                              # =>This Inner Loop Header: Depth=1
	movq	-56(%rdx), %rsi
	movb	$0, (%rsi,%rcx)
	movq	-48(%rdx), %rsi
	movb	$0, (%rsi,%rcx)
	movq	-40(%rdx), %rsi
	movb	$0, (%rsi,%rcx)
	movq	-32(%rdx), %rsi
	movb	$0, (%rsi,%rcx)
	movq	-24(%rdx), %rsi
	movb	$0, (%rsi,%rcx)
	movq	-16(%rdx), %rsi
	movb	$0, (%rsi,%rcx)
	movq	-8(%rdx), %rsi
	movb	$0, (%rsi,%rcx)
	movq	(%rdx), %rsi
	movb	$0, (%rsi,%rcx)
	addq	$64, %rdx
	addq	$-8, %rax
	jne	.LBB49_22
.LBB49_23:                              # %.preheader
	testl	%r14d, %r14d
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	jle	.LBB49_52
# BB#24:                                # %.lr.ph
	movslq	%r12d, %r8
	movl	%r14d, %ecx
	xorl	%edx, %edx
	testb	$1, %cl
	jne	.LBB49_42
# BB#25:
	xorl	%ebp, %ebp
	cmpl	$1, %r14d
	jne	.LBB49_46
	jmp	.LBB49_52
.LBB49_42:
	testl	%r12d, %r12d
	je	.LBB49_44
# BB#43:
	movq	(%rax,%r8,8), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, (%r9)
	movl	$1, %edx
.LBB49_44:                              # %.prol.loopexit
	movl	$1, %ebp
	cmpl	$1, %r14d
	je	.LBB49_52
.LBB49_46:
	movl	%r12d, %edi
	.p2align	4, 0x90
.LBB49_47:                              # =>This Inner Loop Header: Depth=1
	cmpq	%rbp, %rdi
	je	.LBB49_49
# BB#48:                                #   in Loop: Header=BB49_47 Depth=1
	movq	(%rax,%r8,8), %rsi
	movq	(%rsi,%rbp,8), %rsi
	movslq	%edx, %rdx
	movq	%rsi, (%r9,%rdx,8)
	incl	%edx
.LBB49_49:                              #   in Loop: Header=BB49_47 Depth=1
	leaq	1(%rbp), %rsi
	cmpq	%rdi, %rsi
	je	.LBB49_51
# BB#50:                                #   in Loop: Header=BB49_47 Depth=1
	movq	(%rax,%r8,8), %rbx
	movq	8(%rbx,%rbp,8), %rbp
	movslq	%edx, %rdx
	movq	%rbp, (%r9,%rdx,8)
	incl	%edx
.LBB49_51:                              #   in Loop: Header=BB49_47 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	movq	%rsi, %rbp
	jne	.LBB49_47
.LBB49_52:                              # %._crit_edge
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end49:
	.size	gappick, .Lfunc_end49-gappick
	.cfi_endproc

	.globl	commongappick_record
	.p2align	4, 0x90
	.type	commongappick_record,@function
commongappick_record:                   # @commongappick_record
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi424:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi425:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi426:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi427:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi428:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi429:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi430:
	.cfi_def_cfa_offset 64
.Lcfi431:
	.cfi_offset %rbx, -56
.Lcfi432:
	.cfi_offset %r12, -48
.Lcfi433:
	.cfi_offset %r13, -40
.Lcfi434:
	.cfi_offset %r14, -32
.Lcfi435:
	.cfi_offset %r15, -24
.Lcfi436:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r13
	movl	%edi, %r15d
	movq	(%r13), %rdi
	callq	strlen
	testl	%eax, %eax
	js	.LBB50_16
# BB#1:                                 # %.preheader34.lr.ph
	testl	%r15d, %r15d
	jle	.LBB50_2
# BB#17:                                # %.preheader34.us.preheader
	movslq	%r15d, %r12
	incl	%eax
	movl	%r15d, %r9d
	leaq	-1(%r9), %r10
	movl	%r9d, %r11d
	andl	$3, %r11d
	leaq	24(%r13), %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB50_18:                              # %.preheader34.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB50_19 Depth 2
                                        #     Child Loop BB50_28 Depth 2
                                        #     Child Loop BB50_23 Depth 2
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB50_19:                              #   Parent Loop BB50_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13,%rdi,8), %rbp
	cmpb	$45, (%rbp,%rdx)
	jne	.LBB50_20
# BB#26:                                #   in Loop: Header=BB50_19 Depth=2
	incq	%rdi
	cmpq	%r12, %rdi
	jl	.LBB50_19
.LBB50_20:                              # %._crit_edge.us
                                        #   in Loop: Header=BB50_18 Depth=1
	cmpl	%r15d, %edi
	je	.LBB50_25
# BB#21:                                # %.lr.ph39.us
                                        #   in Loop: Header=BB50_18 Depth=1
	testq	%r11, %r11
	movslq	%esi, %rsi
	je	.LBB50_22
# BB#27:                                # %.prol.preheader
                                        #   in Loop: Header=BB50_18 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB50_28:                              #   Parent Loop BB50_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13,%rdi,8), %rcx
	movzbl	(%rcx,%rdx), %ebx
	movb	%bl, (%rcx,%rsi)
	incq	%rdi
	cmpq	%rdi, %r11
	jne	.LBB50_28
	jmp	.LBB50_29
.LBB50_22:                              #   in Loop: Header=BB50_18 Depth=1
	xorl	%edi, %edi
.LBB50_29:                              # %.prol.loopexit
                                        #   in Loop: Header=BB50_18 Depth=1
	cmpq	$3, %r10
	jb	.LBB50_24
# BB#30:                                # %.lr.ph39.us.new
                                        #   in Loop: Header=BB50_18 Depth=1
	movq	%r9, %rbp
	subq	%rdi, %rbp
	leaq	(%r8,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB50_23:                              #   Parent Loop BB50_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rdi), %rcx
	movzbl	(%rcx,%rdx), %ebx
	movb	%bl, (%rcx,%rsi)
	movq	-16(%rdi), %rcx
	movzbl	(%rcx,%rdx), %ebx
	movb	%bl, (%rcx,%rsi)
	movq	-8(%rdi), %rcx
	movzbl	(%rcx,%rdx), %ebx
	movb	%bl, (%rcx,%rsi)
	movq	(%rdi), %rcx
	movzbl	(%rcx,%rdx), %ebx
	movb	%bl, (%rcx,%rsi)
	addq	$32, %rdi
	addq	$-4, %rbp
	jne	.LBB50_23
.LBB50_24:                              # %._crit_edge40.us
                                        #   in Loop: Header=BB50_18 Depth=1
	movl	%edx, (%r14,%rsi,4)
	incl	%esi
.LBB50_25:                              #   in Loop: Header=BB50_18 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jne	.LBB50_18
	jmp	.LBB50_16
.LBB50_2:                               # %.preheader34.preheader
	leal	1(%rax), %ecx
	movl	%ecx, %edi
	xorl	%edx, %edx
	xorl	%esi, %esi
	andl	$3, %edi
	je	.LBB50_6
	.p2align	4, 0x90
.LBB50_3:                               # %.preheader34.prol
                                        # =>This Inner Loop Header: Depth=1
	testl	%r15d, %r15d
	je	.LBB50_5
# BB#4:                                 # %.preheader.prol
                                        #   in Loop: Header=BB50_3 Depth=1
	movslq	%esi, %rsi
	movl	%edx, (%r14,%rsi,4)
	incl	%esi
.LBB50_5:                               #   in Loop: Header=BB50_3 Depth=1
	incl	%edx
	cmpl	%edx, %edi
	jne	.LBB50_3
.LBB50_6:                               # %.preheader34.prol.loopexit
	cmpl	$3, %eax
	jb	.LBB50_16
	.p2align	4, 0x90
.LBB50_7:                               # %.preheader34
                                        # =>This Inner Loop Header: Depth=1
	testl	%r15d, %r15d
	je	.LBB50_9
# BB#8:                                 # %.preheader
                                        #   in Loop: Header=BB50_7 Depth=1
	movslq	%esi, %rsi
	movl	%edx, (%r14,%rsi,4)
	incl	%esi
.LBB50_9:                               # %.preheader34.173
                                        #   in Loop: Header=BB50_7 Depth=1
	testl	%r15d, %r15d
	je	.LBB50_11
# BB#10:                                # %.preheader.1
                                        #   in Loop: Header=BB50_7 Depth=1
	leal	1(%rdx), %eax
	movslq	%esi, %rsi
	movl	%eax, (%r14,%rsi,4)
	incl	%esi
.LBB50_11:                              # %.preheader34.274
                                        #   in Loop: Header=BB50_7 Depth=1
	testl	%r15d, %r15d
	je	.LBB50_13
# BB#12:                                # %.preheader.2
                                        #   in Loop: Header=BB50_7 Depth=1
	leal	2(%rdx), %eax
	movslq	%esi, %rsi
	movl	%eax, (%r14,%rsi,4)
	incl	%esi
.LBB50_13:                              # %.preheader34.375
                                        #   in Loop: Header=BB50_7 Depth=1
	testl	%r15d, %r15d
	je	.LBB50_15
# BB#14:                                # %.preheader.3
                                        #   in Loop: Header=BB50_7 Depth=1
	leal	3(%rdx), %eax
	movslq	%esi, %rsi
	movl	%eax, (%r14,%rsi,4)
	incl	%esi
.LBB50_15:                              #   in Loop: Header=BB50_7 Depth=1
	addl	$4, %edx
	cmpl	%edx, %ecx
	jne	.LBB50_7
.LBB50_16:                              # %._crit_edge43
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end50:
	.size	commongappick_record, .Lfunc_end50-commongappick_record
	.cfi_endproc

	.globl	commongappick
	.p2align	4, 0x90
	.type	commongappick,@function
commongappick:                          # @commongappick
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi437:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi438:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi439:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi440:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi441:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi442:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi443:
	.cfi_def_cfa_offset 64
.Lcfi444:
	.cfi_offset %rbx, -56
.Lcfi445:
	.cfi_offset %r12, -48
.Lcfi446:
	.cfi_offset %r13, -40
.Lcfi447:
	.cfi_offset %r14, -32
.Lcfi448:
	.cfi_offset %r15, -24
.Lcfi449:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movl	%edi, %r14d
	movq	(%r13), %rdi
	callq	strlen
	testl	%eax, %eax
	js	.LBB51_11
# BB#1:
	testl	%r14d, %r14d
	jle	.LBB51_11
# BB#2:                                 # %.preheader31.us.preheader
	movslq	%r14d, %r12
	incl	%eax
	movl	%r14d, %r9d
	leaq	-1(%r9), %r10
	movl	%r9d, %r15d
	andl	$3, %r15d
	leaq	24(%r13), %r8
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB51_3:                               # %.preheader31.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB51_4 Depth 2
                                        #     Child Loop BB51_14 Depth 2
                                        #     Child Loop BB51_8 Depth 2
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB51_4:                               #   Parent Loop BB51_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13,%rsi,8), %rdi
	cmpb	$45, (%rdi,%rdx)
	jne	.LBB51_5
# BB#12:                                #   in Loop: Header=BB51_4 Depth=2
	incq	%rsi
	cmpq	%r12, %rsi
	jl	.LBB51_4
.LBB51_5:                               # %._crit_edge.us
                                        #   in Loop: Header=BB51_3 Depth=1
	cmpl	%r14d, %esi
	je	.LBB51_10
# BB#6:                                 # %.lr.ph36.us
                                        #   in Loop: Header=BB51_3 Depth=1
	testq	%r15, %r15
	movslq	%r11d, %rbp
	je	.LBB51_7
# BB#13:                                # %.prol.preheader
                                        #   in Loop: Header=BB51_3 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB51_14:                              #   Parent Loop BB51_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13,%rdi,8), %rcx
	movzbl	(%rcx,%rdx), %ebx
	movb	%bl, (%rcx,%rbp)
	incq	%rdi
	cmpq	%rdi, %r15
	jne	.LBB51_14
	jmp	.LBB51_15
.LBB51_7:                               #   in Loop: Header=BB51_3 Depth=1
	xorl	%edi, %edi
.LBB51_15:                              # %.prol.loopexit
                                        #   in Loop: Header=BB51_3 Depth=1
	cmpq	$3, %r10
	jb	.LBB51_9
# BB#16:                                # %.lr.ph36.us.new
                                        #   in Loop: Header=BB51_3 Depth=1
	movq	%r9, %rsi
	subq	%rdi, %rsi
	leaq	(%r8,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB51_8:                               #   Parent Loop BB51_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rdi), %rcx
	movzbl	(%rcx,%rdx), %ebx
	movb	%bl, (%rcx,%rbp)
	movq	-16(%rdi), %rcx
	movzbl	(%rcx,%rdx), %ebx
	movb	%bl, (%rcx,%rbp)
	movq	-8(%rdi), %rcx
	movzbl	(%rcx,%rdx), %ebx
	movb	%bl, (%rcx,%rbp)
	movq	(%rdi), %rcx
	movzbl	(%rcx,%rdx), %ebx
	movb	%bl, (%rcx,%rbp)
	addq	$32, %rdi
	addq	$-4, %rsi
	jne	.LBB51_8
.LBB51_9:                               # %._crit_edge37.us
                                        #   in Loop: Header=BB51_3 Depth=1
	incl	%r11d
.LBB51_10:                              #   in Loop: Header=BB51_3 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jne	.LBB51_3
.LBB51_11:                              # %._crit_edge40
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end51:
	.size	commongappick, .Lfunc_end51-commongappick
	.cfi_endproc

	.globl	score_calc0
	.p2align	4, 0x90
	.type	score_calc0,@function
score_calc0:                            # @score_calc0
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi450:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi451:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi452:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi453:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi454:
	.cfi_def_cfa_offset 48
.Lcfi455:
	.cfi_offset %rbx, -40
.Lcfi456:
	.cfi_offset %r14, -32
.Lcfi457:
	.cfi_offset %r15, -24
.Lcfi458:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %r15
	movl	%esi, %ebp
	movq	%rdi, %rbx
	cmpl	$3, scmtd(%rip)
	jne	.LBB52_2
# BB#1:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	callq	score_calc3
.LBB52_2:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	movl	%r14d, %ecx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	score_calc5             # TAILCALL
.Lfunc_end52:
	.size	score_calc0, .Lfunc_end52-score_calc0
	.cfi_endproc

	.globl	strins
	.p2align	4, 0x90
	.type	strins,@function
strins:                                 # @strins
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi459:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi460:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi461:
	.cfi_def_cfa_offset 32
.Lcfi462:
	.cfi_offset %rbx, -32
.Lcfi463:
	.cfi_offset %r14, -24
.Lcfi464:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	strlen
	movq	%rax, %r15
	movq	%r14, %rdi
	callq	strlen
	addl	%r15d, %eax
	movslq	%eax, %rdx
	leaq	(%r14,%rdx), %rax
	movslq	%r15d, %rcx
	cmpq	%rcx, %rdx
	jl	.LBB53_3
# BB#1:                                 # %.lr.ph30
	leaq	(%r14,%rcx), %rsi
	negq	%rcx
	.p2align	4, 0x90
.LBB53_2:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx,%rax), %edx
	movb	%dl, (%rax)
	decq	%rax
	cmpq	%rsi, %rax
	jae	.LBB53_2
.LBB53_3:                               # %.preheader
	cmpq	%r14, %rax
	jb	.LBB53_6
# BB#4:
	shlq	$32, %r15
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	addq	%r15, %rcx
	sarq	$32, %rcx
	addq	%rcx, %rbx
	.p2align	4, 0x90
.LBB53_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %ecx
	decq	%rbx
	movb	%cl, (%rax)
	decq	%rax
	cmpq	%r14, %rax
	jae	.LBB53_5
.LBB53_6:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end53:
	.size	strins, .Lfunc_end53-strins
	.cfi_endproc

	.globl	isaligned
	.p2align	4, 0x90
	.type	isaligned,@function
isaligned:                              # @isaligned
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi465:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi466:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi467:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi468:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi469:
	.cfi_def_cfa_offset 48
.Lcfi470:
	.cfi_offset %rbx, -48
.Lcfi471:
	.cfi_offset %r12, -40
.Lcfi472:
	.cfi_offset %r14, -32
.Lcfi473:
	.cfi_offset %r15, -24
.Lcfi474:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movl	%edi, %r15d
	movl	$1, %r14d
	cmpl	$2, %r15d
	jl	.LBB54_5
# BB#1:                                 # %.lr.ph
	movq	(%r12), %rdi
	callq	strlen
	movslq	%eax, %rbp
	movslq	%r15d, %r15
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB54_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbx,8), %rdi
	callq	strlen
	cmpq	%rbp, %rax
	jne	.LBB54_4
# BB#2:                                 #   in Loop: Header=BB54_3 Depth=1
	incq	%rbx
	cmpq	%r15, %rbx
	jl	.LBB54_3
	jmp	.LBB54_5
.LBB54_4:
	xorl	%r14d, %r14d
.LBB54_5:                               # %._crit_edge
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end54:
	.size	isaligned, .Lfunc_end54-isaligned
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI55_0:
	.quad	9221120237041090560     # double NaN
.LCPI55_1:
	.quad	-4616189618054758400    # double -1
.LCPI55_2:
	.quad	4602678819172646912     # double 0.5
.LCPI55_3:
	.quad	4645744490609377280     # double 400
	.text
	.globl	score_calc_for_score
	.p2align	4, 0x90
	.type	score_calc_for_score,@function
score_calc_for_score:                   # @score_calc_for_score
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi475:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi476:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi477:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi478:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi479:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi480:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi481:
	.cfi_def_cfa_offset 128
.Lcfi482:
	.cfi_offset %rbx, -56
.Lcfi483:
	.cfi_offset %r12, -48
.Lcfi484:
	.cfi_offset %r13, -40
.Lcfi485:
	.cfi_offset %r14, -32
.Lcfi486:
	.cfi_offset %r15, -24
.Lcfi487:
	.cfi_offset %rbp, -16
	movl	%edi, %ebx
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	(%rsi), %rdi
	callq	strlen
	xorpd	%xmm0, %xmm0
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	cmpl	$2, %ebx
	jl	.LBB55_26
# BB#1:                                 # %.lr.ph94
	movq	24(%rsp), %rsi          # 8-byte Reload
	leal	-1(%rsi), %ebx
	leal	-2(%rax), %edx
	movl	penalty(%rip), %ecx
	subl	n_dis+96(%rip), %ecx
	cvtsi2sdl	%ecx, %xmm4
	movslq	%esi, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	%esi, %ebp
	leal	-2(%rsi), %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	xorpd	%xmm0, %xmm0
	movl	$1, %edi
	xorl	%ecx, %ecx
	movsd	.LCPI55_0(%rip), %xmm1  # xmm1 = mem[0],zero
	movq	%rbx, 32(%rsp)          # 8-byte Spill
                                        # kill: %BL<def> %BL<kill> %RBX<kill>
	jmp	.LBB55_3
.LBB55_13:                              # %.lr.ph89.split.preheader
                                        #   in Loop: Header=BB55_3 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %ebp
	subl	%esi, %ebp
	movq	40(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%esi, %ecx
	testb	$7, %bpl
	je	.LBB55_14
# BB#15:                                # %.lr.ph89.split.prol.preheader
                                        #   in Loop: Header=BB55_3 Depth=1
	andb	$7, %bl
	movzbl	%bl, %ebp
	negl	%ebp
	movl	%r9d, %esi
	movq	%r8, %rdi
	.p2align	4, 0x90
.LBB55_16:                              # %.lr.ph89.split.prol
                                        #   Parent Loop BB55_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	%xmm1, %xmm0
	incl	%esi
	incl	%ebp
	jne	.LBB55_16
	jmp	.LBB55_17
.LBB55_14:                              #   in Loop: Header=BB55_3 Depth=1
	movl	%r9d, %esi
	movq	%r8, %rdi
.LBB55_17:                              # %.lr.ph89.split.prol.loopexit
                                        #   in Loop: Header=BB55_3 Depth=1
	cmpl	$7, %ecx
	jb	.LBB55_2
# BB#18:                                # %.lr.ph89.split.preheader.new
                                        #   in Loop: Header=BB55_3 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%esi, %ecx
	.p2align	4, 0x90
.LBB55_19:                              # %.lr.ph89.split
                                        #   Parent Loop BB55_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	%xmm1, %xmm0
	addsd	%xmm1, %xmm0
	addsd	%xmm1, %xmm0
	addsd	%xmm1, %xmm0
	addsd	%xmm1, %xmm0
	addsd	%xmm1, %xmm0
	addsd	%xmm1, %xmm0
	addsd	%xmm1, %xmm0
	addl	$-8, %ecx
	jne	.LBB55_19
	jmp	.LBB55_2
	.p2align	4, 0x90
.LBB55_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB55_16 Depth 2
                                        #     Child Loop BB55_19 Depth 2
                                        #     Child Loop BB55_7 Depth 2
                                        #       Child Loop BB55_8 Depth 3
                                        #         Child Loop BB55_22 Depth 4
                                        #         Child Loop BB55_12 Depth 4
	movq	%rbp, %r8
	movq	%rcx, %rsi
	leaq	1(%rsi), %r9
	cmpq	48(%rsp), %r9           # 8-byte Folded Reload
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	%r9, 56(%rsp)           # 8-byte Spill
	movb	%bl, 15(%rsp)           # 1-byte Spill
	jge	.LBB55_4
# BB#5:                                 # %.lr.ph89
                                        #   in Loop: Header=BB55_3 Depth=1
	testl	%eax, %eax
	jle	.LBB55_13
# BB#6:                                 # %.lr.ph89.split.us.preheader
                                        #   in Loop: Header=BB55_3 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rsi,8), %rcx
	leaq	1(%rcx), %rbp
	movq	%rdi, %rsi
	movq	%r8, %rdi
	.p2align	4, 0x90
.LBB55_7:                               # %.lr.ph89.split.us
                                        #   Parent Loop BB55_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB55_8 Depth 3
                                        #         Child Loop BB55_22 Depth 4
                                        #         Child Loop BB55_12 Depth 4
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx,%rsi,8), %rbx
	leaq	1(%rbx), %r8
	xorl	%r9d, %r9d
	xorpd	%xmm2, %xmm2
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB55_8:                               #   Parent Loop BB55_3 Depth=1
                                        #     Parent Loop BB55_7 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB55_22 Depth 4
                                        #         Child Loop BB55_12 Depth 4
	movslq	%r9d, %r10
	movsbq	(%rcx,%r10), %r13
	cmpq	$45, %r13
	movb	(%rbx,%r10), %r11b
	jne	.LBB55_10
# BB#9:                                 #   in Loop: Header=BB55_8 Depth=3
	cmpb	$45, %r11b
	je	.LBB55_25
.LBB55_10:                              # %thread-pre-split.us
                                        #   in Loop: Header=BB55_8 Depth=3
	movsbq	%r11b, %r12
	movq	%r13, %r15
	shlq	$9, %r15
	cvtsi2sdl	amino_dis(%r15,%r12,4), %xmm3
	addsd	%xmm3, %xmm2
	incl	%r14d
	cmpb	$45, %r13b
	jne	.LBB55_20
# BB#11:                                # %.preheader.preheader
                                        #   in Loop: Header=BB55_8 Depth=3
	leal	-1(%r10), %r9d
	addq	%rbp, %r10
	.p2align	4, 0x90
.LBB55_12:                              # %.preheader
                                        #   Parent Loop BB55_3 Depth=1
                                        #     Parent Loop BB55_7 Depth=2
                                        #       Parent Loop BB55_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%r9d
	cmpb	$45, (%r10)
	leaq	1(%r10), %r10
	je	.LBB55_12
	jmp	.LBB55_23
	.p2align	4, 0x90
.LBB55_20:                              #   in Loop: Header=BB55_8 Depth=3
	cmpb	$45, %r11b
	jne	.LBB55_25
# BB#21:                                # %.preheader110.preheader
                                        #   in Loop: Header=BB55_8 Depth=3
	leal	-1(%r10), %r9d
	addq	%r8, %r10
	.p2align	4, 0x90
.LBB55_22:                              # %.preheader110
                                        #   Parent Loop BB55_3 Depth=1
                                        #     Parent Loop BB55_7 Depth=2
                                        #       Parent Loop BB55_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%r9d
	cmpb	$45, (%r10)
	leaq	1(%r10), %r10
	je	.LBB55_22
.LBB55_23:                              #   in Loop: Header=BB55_8 Depth=3
	addsd	%xmm4, %xmm2
	cmpl	%edx, %r9d
	jg	.LBB55_24
.LBB55_25:                              #   in Loop: Header=BB55_8 Depth=3
	incl	%r9d
	cmpl	%eax, %r9d
	jl	.LBB55_8
.LBB55_24:                              # %._crit_edge.us
                                        #   in Loop: Header=BB55_7 Depth=2
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%r14d, %xmm3
	divsd	%xmm3, %xmm2
	addsd	%xmm2, %xmm0
	incq	%rsi
	cmpq	%rdi, %rsi
	jne	.LBB55_7
	jmp	.LBB55_2
	.p2align	4, 0x90
.LBB55_4:                               #   in Loop: Header=BB55_3 Depth=1
	movq	%r8, %rdi
.LBB55_2:                               # %.loopexit
                                        #   in Loop: Header=BB55_3 Depth=1
	movq	%rdi, %rbp
	movq	64(%rsp), %rdi          # 8-byte Reload
	incq	%rdi
	movb	15(%rsp), %bl           # 1-byte Reload
	addb	$7, %bl
	movq	56(%rsp), %rcx          # 8-byte Reload
	cmpq	32(%rsp), %rcx          # 8-byte Folded Reload
	jne	.LBB55_3
.LBB55_26:                              # %._crit_edge95
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movq	stderr(%rip), %rdi
	movl	$.L.str.29, %esi
	movb	$1, %al
	callq	fprintf
	movq	24(%rsp), %rax          # 8-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI55_1(%rip), %xmm1  # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	.LCPI55_2(%rip), %xmm1
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	addsd	.LCPI55_3(%rip), %xmm0
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end55:
	.size	score_calc_for_score, .Lfunc_end55-score_calc_for_score
	.cfi_endproc

	.globl	floatncpy
	.p2align	4, 0x90
	.type	floatncpy,@function
floatncpy:                              # @floatncpy
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	testl	%edx, %edx
	je	.LBB56_19
# BB#1:                                 # %.lr.ph.preheader
	leal	-1(%rdx), %eax
	leaq	1(%rax), %r10
	cmpq	$8, %r10
	jb	.LBB56_14
# BB#2:                                 # %min.iters.checked
	movabsq	$8589934584, %r9        # imm = 0x1FFFFFFF8
	andq	%r10, %r9
	je	.LBB56_14
# BB#3:                                 # %vector.memcheck
	leaq	4(%rsi,%rax,4), %rcx
	cmpq	%rdi, %rcx
	jbe	.LBB56_5
# BB#4:                                 # %vector.memcheck
	leaq	4(%rdi,%rax,4), %rax
	cmpq	%rsi, %rax
	ja	.LBB56_14
.LBB56_5:                               # %vector.body.preheader
	leaq	-8(%r9), %r8
	movl	%r8d, %ecx
	shrl	$3, %ecx
	incl	%ecx
	andq	$3, %rcx
	je	.LBB56_6
# BB#7:                                 # %vector.body.prol.preheader
	negq	%rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB56_8:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rsi,%rax,4), %xmm0
	movups	16(%rsi,%rax,4), %xmm1
	movups	%xmm0, (%rdi,%rax,4)
	movups	%xmm1, 16(%rdi,%rax,4)
	addq	$8, %rax
	incq	%rcx
	jne	.LBB56_8
	jmp	.LBB56_9
.LBB56_6:
	xorl	%eax, %eax
.LBB56_9:                               # %vector.body.prol.loopexit
	cmpq	$24, %r8
	jb	.LBB56_12
# BB#10:                                # %vector.body.preheader.new
	movq	%r9, %r8
	subq	%rax, %r8
	leaq	112(%rdi,%rax,4), %rcx
	leaq	112(%rsi,%rax,4), %rax
	.p2align	4, 0x90
.LBB56_11:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rax), %xmm0
	movups	-96(%rax), %xmm1
	movups	%xmm0, -112(%rcx)
	movups	%xmm1, -96(%rcx)
	movups	-80(%rax), %xmm0
	movups	-64(%rax), %xmm1
	movups	%xmm0, -80(%rcx)
	movups	%xmm1, -64(%rcx)
	movups	-48(%rax), %xmm0
	movups	-32(%rax), %xmm1
	movups	%xmm0, -48(%rcx)
	movups	%xmm1, -32(%rcx)
	movups	-16(%rax), %xmm0
	movups	(%rax), %xmm1
	movups	%xmm0, -16(%rcx)
	movups	%xmm1, (%rcx)
	subq	$-128, %rcx
	subq	$-128, %rax
	addq	$-32, %r8
	jne	.LBB56_11
.LBB56_12:                              # %middle.block
	cmpq	%r9, %r10
	je	.LBB56_19
# BB#13:
	subl	%r9d, %edx
	leaq	(%rsi,%r9,4), %rsi
	leaq	(%rdi,%r9,4), %rdi
.LBB56_14:                              # %.lr.ph.preheader19
	leal	-1(%rdx), %r8d
	movl	%edx, %ecx
	andl	$7, %ecx
	je	.LBB56_17
# BB#15:                                # %.lr.ph.prol.preheader
	negl	%ecx
	.p2align	4, 0x90
.LBB56_16:                              # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	decl	%edx
	movl	(%rsi), %eax
	addq	$4, %rsi
	movl	%eax, (%rdi)
	addq	$4, %rdi
	incl	%ecx
	jne	.LBB56_16
.LBB56_17:                              # %.lr.ph.prol.loopexit
	cmpl	$7, %r8d
	jb	.LBB56_19
	.p2align	4, 0x90
.LBB56_18:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %eax
	movl	%eax, (%rdi)
	movl	4(%rsi), %eax
	movl	%eax, 4(%rdi)
	movl	8(%rsi), %eax
	movl	%eax, 8(%rdi)
	movl	12(%rsi), %eax
	movl	%eax, 12(%rdi)
	movl	16(%rsi), %eax
	movl	%eax, 16(%rdi)
	movl	20(%rsi), %eax
	movl	%eax, 20(%rdi)
	movl	24(%rsi), %eax
	movl	%eax, 24(%rdi)
	addl	$-8, %edx
	movl	28(%rsi), %eax
	leaq	32(%rsi), %rsi
	movl	%eax, 28(%rdi)
	leaq	32(%rdi), %rdi
	jne	.LBB56_18
.LBB56_19:                              # %._crit_edge
	retq
.Lfunc_end56:
	.size	floatncpy, .Lfunc_end56-floatncpy
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI57_0:
	.quad	4602678819172646912     # double 0.5
.LCPI57_1:
	.quad	4607182418800017408     # double 1
.LCPI57_2:
	.quad	4645744490609377280     # double 400
	.text
	.globl	score_calc_a
	.p2align	4, 0x90
	.type	score_calc_a,@function
score_calc_a:                           # @score_calc_a
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi488:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi489:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi490:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi491:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi492:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi493:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi494:
	.cfi_def_cfa_offset 176
.Lcfi495:
	.cfi_offset %rbx, -56
.Lcfi496:
	.cfi_offset %r12, -48
.Lcfi497:
	.cfi_offset %r13, -40
.Lcfi498:
	.cfi_offset %r14, -32
.Lcfi499:
	.cfi_offset %r15, -24
.Lcfi500:
	.cfi_offset %rbp, -16
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	%esi, %r15d
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	(%rdi), %rdi
	callq	strlen
	cmpl	$2, %r15d
	jl	.LBB57_1
# BB#4:                                 # %.lr.ph125
	leal	-1(%r15), %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	cvtsi2sdl	penalty(%rip), %xmm0
	movslq	%r15d, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	%r15d, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	xorps	%xmm1, %xmm1
	movl	$1, %edx
	movsd	.LCPI57_0(%rip), %xmm2  # xmm2 = mem[0],zero
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB57_6:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB57_9 Depth 2
                                        #       Child Loop BB57_10 Depth 3
	leaq	1(%rax), %rcx
	cmpq	32(%rsp), %rcx          # 8-byte Folded Reload
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	jge	.LBB57_5
# BB#7:                                 # %.lr.ph118
                                        #   in Loop: Header=BB57_6 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB57_5
# BB#8:                                 # %.lr.ph118.split.us.preheader
                                        #   in Loop: Header=BB57_6 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB57_9:                               # %.lr.ph118.split.us
                                        #   Parent Loop BB57_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB57_10 Depth 3
	movq	88(%rsp), %rax          # 8-byte Reload
	movsd	(%rax,%rcx,8), %xmm3    # xmm3 = mem[0],zero
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %rbp
	cvtsd2ss	%xmm3, %xmm4
	xorl	%r8d, %r8d
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	movq	80(%rsp), %r15          # 8-byte Reload
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB57_10:                              #   Parent Loop BB57_6 Depth=1
                                        #     Parent Loop BB57_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	movl	%eax, %r11d
	xorl	$-2, %r11d
	movsbq	(%r15), %rdx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	incl	%r11d
	cmpq	$45, %rdx
	sete	%r9b
	movl	%r9d, 4(%rsp)           # 4-byte Spill
	setne	%cl
	movl	%r8d, %edi
	andl	%r11d, %r9d
	xorl	$1, %edi
	negl	%r9d
	movl	%edi, %r13d
	movsbq	(%rbp), %rbx
	andl	%r9d, %r13d
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
	cmpq	$45, %rbx
	sete	%r12b
	setne	%bpl
	negl	%r13d
	negl	%eax
	movl	%ecx, %r10d
	andl	%ebp, %r13d
	andl	%eax, %r10d
	xorps	%xmm5, %xmm5
	cvtss2sd	%xmm1, %xmm5
	shlq	$9, %rdx
	negl	%r10d
	xorps	%xmm1, %xmm1
	cvtsi2ssl	amino_dis(%rdx,%rbx,4), %xmm1
	movl	%edi, %edx
	andl	%r10d, %edx
	negl	%edx
	movl	%ebp, %r14d
	andl	%edx, %r14d
	addl	%r13d, %r14d
	andl	%ecx, %r11d
	negl	%r11d
	movl	%esi, %ebx
	movl	%edi, %esi
	andl	%r11d, %esi
	negl	%esi
	andl	%r12d, %esi
	addl	%r14d, %esi
	andl	%r8d, %r11d
	negl	%r11d
	andl	%ebp, %r11d
	addl	%esi, %r11d
	movl	%ebx, %esi
	andl	%r8d, %r9d
	andl	%r8d, %r10d
	movl	4(%rsp), %ebx           # 4-byte Reload
	andl	%ebx, %eax
	negl	%eax
	andl	%eax, %r8d
	negl	%r8d
	andl	%ebp, %r8d
	andb	%bpl, %cl
	negl	%r9d
	andl	%r9d, %ebp
	addl	%r11d, %ebp
	andl	%r12d, %edx
	addl	%ebp, %edx
	negl	%r10d
	andl	%r12d, %r10d
	addl	%edx, %r10d
	addl	%r10d, %r8d
	movq	112(%rsp), %rbp         # 8-byte Reload
	andl	%r12d, %r9d
	addl	%r8d, %r9d
	andl	%edi, %eax
	negl	%eax
	andl	%r12d, %eax
	addl	%r9d, %eax
	cvtsi2ssl	%eax, %xmm6
	movzbl	%cl, %eax
	movq	104(%rsp), %rcx         # 8-byte Reload
	addl	%eax, %esi
	cvtss2sd	%xmm6, %xmm6
	mulsd	%xmm2, %xmm6
	mulsd	%xmm0, %xmm6
	mulsd	%xmm3, %xmm6
	addsd	%xmm6, %xmm5
	mulss	%xmm4, %xmm1
	cvtsd2ss	%xmm5, %xmm5
	incq	%r15
	incq	%rbp
	decq	%rcx
	movl	%r12d, %r8d
	movl	%ebx, %eax
	addss	%xmm5, %xmm1
	jne	.LBB57_10
# BB#11:                                # %._crit_edge.us
                                        #   in Loop: Header=BB57_9 Depth=2
	movq	96(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	cmpq	64(%rsp), %rcx          # 8-byte Folded Reload
	jne	.LBB57_9
.LBB57_5:                               # %.loopexit
                                        #   in Loop: Header=BB57_6 Depth=1
	movq	48(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	movq	56(%rsp), %rax          # 8-byte Reload
	cmpq	40(%rsp), %rax          # 8-byte Folded Reload
	jne	.LBB57_6
	jmp	.LBB57_2
.LBB57_1:
	xorps	%xmm1, %xmm1
	xorl	%esi, %esi
.LBB57_2:                               # %._crit_edge126
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%esi, %xmm0
	divss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	cmpl	$0, scoremtx(%rip)
	je	.LBB57_3
# BB#12:                                # %._crit_edge126
	xorps	%xmm1, %xmm1
	jmp	.LBB57_13
.LBB57_3:
	movsd	.LCPI57_1(%rip), %xmm1  # xmm1 = mem[0],zero
.LBB57_13:                              # %._crit_edge126
	mulsd	.LCPI57_2(%rip), %xmm1
	addsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end57:
	.size	score_calc_a, .Lfunc_end57-score_calc_a
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI58_0:
	.quad	4602678819172646912     # double 0.5
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI58_1:
	.long	1137180672              # float 400
	.text
	.globl	score_calc_s
	.p2align	4, 0x90
	.type	score_calc_s,@function
score_calc_s:                           # @score_calc_s
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi501:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi502:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi503:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi504:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi505:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi506:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi507:
	.cfi_def_cfa_offset 160
.Lcfi508:
	.cfi_offset %rbx, -56
.Lcfi509:
	.cfi_offset %r12, -48
.Lcfi510:
	.cfi_offset %r13, -40
.Lcfi511:
	.cfi_offset %r14, -32
.Lcfi512:
	.cfi_offset %r15, -24
.Lcfi513:
	.cfi_offset %rbp, -16
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	%esi, %r15d
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	(%rdi), %rdi
	callq	strlen
	cmpl	$2, %r15d
	jl	.LBB58_1
# BB#3:                                 # %.lr.ph109
	leal	-1(%r15), %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	cvtsi2sdl	penalty(%rip), %xmm1
	movslq	%r15d, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	%r15d, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	xorps	%xmm0, %xmm0
	movl	$1, %esi
	movsd	.LCPI58_0(%rip), %xmm2  # xmm2 = mem[0],zero
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB58_5:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB58_8 Depth 2
                                        #       Child Loop BB58_9 Depth 3
	leaq	1(%rax), %rcx
	cmpq	32(%rsp), %rcx          # 8-byte Folded Reload
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	jge	.LBB58_4
# BB#6:                                 # %.lr.ph102
                                        #   in Loop: Header=BB58_5 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB58_4
# BB#7:                                 # %.lr.ph102.split.us.preheader
                                        #   in Loop: Header=BB58_5 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB58_8:                               # %.lr.ph102.split.us
                                        #   Parent Loop BB58_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB58_9 Depth 3
	movq	88(%rsp), %rax          # 8-byte Reload
	movsd	(%rax,%rcx,8), %xmm3    # xmm3 = mem[0],zero
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %rsi
	cvtsd2ss	%xmm3, %xmm4
	xorl	%r13d, %r13d
	movq	72(%rsp), %rbp          # 8-byte Reload
	movq	80(%rsp), %rbx          # 8-byte Reload
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB58_9:                               #   Parent Loop BB58_5 Depth=1
                                        #     Parent Loop BB58_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movl	%r8d, %r10d
	xorl	$-2, %r10d
	incl	%r10d
	movsbq	(%rbx), %rcx
	xorl	%r15d, %r15d
	xorl	%r11d, %r11d
	cmpq	$45, %rcx
	sete	%r15b
	setne	%r11b
	movl	%r15d, %eax
	andl	%r10d, %eax
	movl	%r13d, %r12d
	xorl	$1, %r12d
	negl	%eax
	movl	%r12d, %r14d
	andl	%eax, %r14d
	movsbq	(%rsi), %rdx
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	cmpq	$45, %rdx
	sete	%r9b
	setne	%dil
	xorps	%xmm5, %xmm5
	cvtss2sd	%xmm0, %xmm5
	shlq	$9, %rcx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rcx,%rdx,4), %xmm0
	movl	4(%rsp), %edx           # 4-byte Reload
	negl	%r8d
	andl	%r11d, %r8d
	negl	%r8d
	andl	%r12d, %r8d
	andl	%r11d, %r10d
	negl	%r10d
	andl	%r10d, %r12d
	andl	%r13d, %r10d
	andl	%r13d, %eax
	negl	%r14d
	andl	%edi, %r14d
	negl	%r8d
	negl	%r10d
	andl	%edi, %r10d
	negl	%eax
	andl	%edi, %eax
	andb	%dil, %r11b
	andl	%r8d, %edi
	addl	%r14d, %edi
	negl	%r12d
	andl	%r9d, %r12d
	addl	%edi, %r12d
	addl	%r12d, %r10d
	addl	%r10d, %eax
	andl	%r9d, %r8d
	addl	%eax, %r8d
	cvtsi2ssl	%r8d, %xmm6
	movzbl	%r11b, %eax
	addl	%eax, %edx
	cvtss2sd	%xmm6, %xmm6
	mulsd	%xmm2, %xmm6
	mulsd	%xmm1, %xmm6
	mulsd	%xmm3, %xmm6
	addsd	%xmm6, %xmm5
	cvtsd2ss	%xmm5, %xmm5
	mulss	%xmm4, %xmm0
	addss	%xmm5, %xmm0
	incq	%rbx
	incq	%rsi
	decq	%rbp
	movl	%r9d, %r13d
	movl	%r15d, %r8d
	jne	.LBB58_9
# BB#10:                                # %._crit_edge.us
                                        #   in Loop: Header=BB58_8 Depth=2
	movq	96(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	cmpq	64(%rsp), %rcx          # 8-byte Folded Reload
	jne	.LBB58_8
.LBB58_4:                               # %.loopexit
                                        #   in Loop: Header=BB58_5 Depth=1
	movq	48(%rsp), %rsi          # 8-byte Reload
	incq	%rsi
	movq	56(%rsp), %rax          # 8-byte Reload
	cmpq	40(%rsp), %rax          # 8-byte Folded Reload
	jne	.LBB58_5
	jmp	.LBB58_2
.LBB58_1:
	xorps	%xmm0, %xmm0
	xorl	%edx, %edx
.LBB58_2:                               # %._crit_edge110
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%edx, %xmm1
	divss	%xmm1, %xmm0
	addss	.LCPI58_1(%rip), %xmm0
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end58:
	.size	score_calc_s, .Lfunc_end58-score_calc_s
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI59_0:
	.quad	4602678819172646912     # double 0.5
.LCPI59_1:
	.quad	4645744490609377280     # double 400
	.text
	.globl	score_calc_for_score_s
	.p2align	4, 0x90
	.type	score_calc_for_score_s,@function
score_calc_for_score_s:                 # @score_calc_for_score_s
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi514:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi515:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi516:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi517:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi518:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi519:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi520:
	.cfi_def_cfa_offset 144
.Lcfi521:
	.cfi_offset %rbx, -56
.Lcfi522:
	.cfi_offset %r12, -48
.Lcfi523:
	.cfi_offset %r13, -40
.Lcfi524:
	.cfi_offset %r14, -32
.Lcfi525:
	.cfi_offset %r15, -24
.Lcfi526:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	(%rsi), %rdi
	callq	strlen
	cmpl	$2, %r14d
	jl	.LBB59_1
# BB#3:                                 # %.lr.ph100
	leal	-1(%r14), %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	cvtsi2sdl	penalty(%rip), %xmm0
	movslq	%r14d, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%eax, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%r14d, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	xorps	%xmm1, %xmm1
	movl	$1, %edx
	movsd	.LCPI59_0(%rip), %xmm2  # xmm2 = mem[0],zero
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB59_5:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB59_8 Depth 2
                                        #       Child Loop BB59_9 Depth 3
	leaq	1(%rax), %rcx
	cmpq	24(%rsp), %rcx          # 8-byte Folded Reload
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	jge	.LBB59_4
# BB#6:                                 #   in Loop: Header=BB59_5 Depth=1
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB59_4
# BB#7:                                 # %.preheader.us.preheader
                                        #   in Loop: Header=BB59_5 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB59_8:                               # %.preheader.us
                                        #   Parent Loop BB59_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB59_9 Depth 3
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %rdi
	xorl	%r14d, %r14d
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB59_9:                               #   Parent Loop BB59_5 Depth=1
                                        #     Parent Loop BB59_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	movl	%ebp, %r9d
	xorl	$-2, %r9d
	incl	%r9d
	movsbq	(%rdx), %r12
	xorl	%r15d, %r15d
	xorl	%r10d, %r10d
	cmpq	$45, %r12
	sete	%r15b
	setne	%r10b
	movl	%r15d, %r8d
	andl	%r9d, %r8d
	movl	%r14d, %r11d
	xorl	$1, %r11d
	negl	%r8d
	movl	%r11d, %r13d
	andl	%r8d, %r13d
	movsbq	(%rdi), %rcx
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	cmpq	$45, %rcx
	sete	%al
	setne	%bl
	xorps	%xmm3, %xmm3
	cvtss2sd	%xmm1, %xmm3
	shlq	$9, %r12
	xorps	%xmm1, %xmm1
	cvtsi2ssl	amino_dis(%r12,%rcx,4), %xmm1
	negl	%ebp
	andl	%r10d, %ebp
	negl	%ebp
	andl	%r11d, %ebp
	andl	%r10d, %r9d
	negl	%r9d
	andl	%r9d, %r11d
	andl	%r14d, %r9d
	andl	%r14d, %r8d
	negl	%r13d
	andl	%ebx, %r13d
	negl	%ebp
	negl	%r9d
	andl	%ebx, %r9d
	negl	%r8d
	andl	%ebx, %r8d
	andb	%bl, %r10b
	andl	%ebp, %ebx
	addl	%r13d, %ebx
	negl	%r11d
	andl	%eax, %r11d
	addl	%ebx, %r11d
	addl	%r11d, %r9d
	addl	%r9d, %r8d
	andl	%eax, %ebp
	addl	%r8d, %ebp
	movl	4(%rsp), %ebx           # 4-byte Reload
	cvtsi2ssl	%ebp, %xmm4
	movzbl	%r10b, %ecx
	addl	%ecx, %ebx
	cvtss2sd	%xmm4, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	addsd	%xmm4, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	addss	%xmm3, %xmm1
	incq	%rdx
	incq	%rdi
	decq	%rsi
	movl	%eax, %r14d
	movl	%r15d, %ebp
	jne	.LBB59_9
# BB#10:                                # %._crit_edge.us
                                        #   in Loop: Header=BB59_8 Depth=2
	movq	80(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	cmpq	56(%rsp), %rcx          # 8-byte Folded Reload
	jne	.LBB59_8
.LBB59_4:                               # %.loopexit
                                        #   in Loop: Header=BB59_5 Depth=1
	movq	40(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	movq	48(%rsp), %rax          # 8-byte Reload
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	jne	.LBB59_5
	jmp	.LBB59_2
.LBB59_1:
	xorps	%xmm1, %xmm1
	xorl	%ebx, %ebx
.LBB59_2:                               # %._crit_edge101
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	divsd	%xmm1, %xmm0
	addsd	.LCPI59_1(%rip), %xmm0
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end59:
	.size	score_calc_for_score_s, .Lfunc_end59-score_calc_for_score_s
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI60_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	SSPscore___
	.p2align	4, 0x90
	.type	SSPscore___,@function
SSPscore___:                            # @SSPscore___
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi527:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi528:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi529:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi530:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi531:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi532:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi533:
	.cfi_def_cfa_offset 112
.Lcfi534:
	.cfi_offset %rbx, -56
.Lcfi535:
	.cfi_offset %r12, -48
.Lcfi536:
	.cfi_offset %r13, -40
.Lcfi537:
	.cfi_offset %r14, -32
.Lcfi538:
	.cfi_offset %r15, -24
.Lcfi539:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%edi, %ebp
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	(%rsi), %rdi
	callq	strlen
	xorps	%xmm1, %xmm1
	testl	%ebp, %ebp
	jle	.LBB60_7
# BB#1:                                 # %.lr.ph91
	testl	%eax, %eax
	jle	.LBB60_7
# BB#2:                                 # %.lr.ph91.split.us.preheader
	movslq	%r14d, %rdx
	cvtsi2sdl	penalty(%rip), %xmm0
	movq	%rdx, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%edx, %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	%ebp, %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorps	%xmm1, %xmm1
	xorl	%edx, %edx
	movsd	.LCPI60_0(%rip), %xmm2  # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB60_3:                               # %.lr.ph91.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB60_5 Depth 2
	cmpq	40(%rsp), %rdx          # 8-byte Folded Reload
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	je	.LBB60_6
# BB#4:                                 # %.lr.ph.us
                                        #   in Loop: Header=BB60_3 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rbp
	movq	(%rax,%rdx,8), %rax
	xorl	%r8d, %r8d
	movq	8(%rsp), %r13           # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB60_5:                               #   Parent Loop BB60_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, %edi
	xorl	$-2, %edi
	incl	%edi
	movsbq	(%rbp), %r9
	xorl	%r12d, %r12d
	xorl	%r10d, %r10d
	cmpq	$45, %r9
	sete	%r12b
	setne	%r10b
	movl	%r12d, %ecx
	andl	%edi, %ecx
	movl	%r8d, %ebx
	xorl	$1, %ebx
	negl	%ecx
	movl	%ebx, %r11d
	andl	%ecx, %r11d
	movsbq	(%rax), %r14
	xorl	%esi, %esi
	xorl	%r15d, %r15d
	cmpq	$45, %r14
	sete	%sil
	setne	%r15b
	xorps	%xmm3, %xmm3
	cvtss2sd	%xmm1, %xmm3
	shlq	$9, %r9
	xorps	%xmm1, %xmm1
	cvtsi2ssl	amino_dis(%r9,%r14,4), %xmm1
	negl	%edx
	andl	%r10d, %edx
	andl	%r10d, %edi
	negl	%edx
	andl	%ebx, %edx
	negl	%edi
	andl	%edi, %ebx
	andl	%r8d, %edi
	andl	%r8d, %ecx
	negl	%r11d
	andl	%r15d, %r11d
	negl	%edx
	negl	%edi
	andl	%r15d, %edi
	negl	%ecx
	andl	%r15d, %ecx
	andl	%edx, %r15d
	addl	%r11d, %r15d
	negl	%ebx
	andl	%esi, %ebx
	addl	%r15d, %ebx
	addl	%ebx, %edi
	leal	(%rdi,%rcx,2), %ecx
	andl	%esi, %edx
	leal	(%rcx,%rdx,2), %ecx
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%ecx, %xmm4
	cvtss2sd	%xmm4, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	addsd	%xmm4, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	addss	%xmm3, %xmm1
	incq	%rbp
	incq	%rax
	decq	%r13
	movl	%esi, %r8d
	movl	%r12d, %edx
	jne	.LBB60_5
.LBB60_6:                               # %..loopexit_crit_edge.us
                                        #   in Loop: Header=BB60_3 Depth=1
	movq	48(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	cmpq	32(%rsp), %rdx          # 8-byte Folded Reload
	jne	.LBB60_3
.LBB60_7:                               # %._crit_edge
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end60:
	.size	SSPscore___, .Lfunc_end60-SSPscore___
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI61_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	SSPscore
	.p2align	4, 0x90
	.type	SSPscore,@function
SSPscore:                               # @SSPscore
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi540:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi541:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi542:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi543:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi544:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi545:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi546:
	.cfi_def_cfa_offset 144
.Lcfi547:
	.cfi_offset %rbx, -56
.Lcfi548:
	.cfi_offset %r12, -48
.Lcfi549:
	.cfi_offset %r13, -40
.Lcfi550:
	.cfi_offset %r14, -32
.Lcfi551:
	.cfi_offset %r15, -24
.Lcfi552:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	(%rsi), %rdi
	callq	strlen
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorps	%xmm1, %xmm1
	cmpl	$2, %r14d
	jl	.LBB61_9
# BB#1:                                 # %.lr.ph99
	leal	-1(%r14), %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	cvtsi2sdl	penalty(%rip), %xmm0
	movslq	%r14d, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	8(%rsp), %eax           # 4-byte Reload
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%r14d, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorps	%xmm1, %xmm1
	movl	$1, %edx
	xorl	%eax, %eax
	movsd	.LCPI61_0(%rip), %xmm2  # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB61_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB61_6 Depth 2
                                        #       Child Loop BB61_7 Depth 3
	leaq	1(%rax), %rcx
	cmpq	24(%rsp), %rcx          # 8-byte Folded Reload
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	jge	.LBB61_2
# BB#4:                                 #   in Loop: Header=BB61_3 Depth=1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB61_2
# BB#5:                                 # %.preheader.us.preheader
                                        #   in Loop: Header=BB61_3 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB61_6:                               # %.preheader.us
                                        #   Parent Loop BB61_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB61_7 Depth 3
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %rbx
	xorl	%r11d, %r11d
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB61_7:                               #   Parent Loop BB61_3 Depth=1
                                        #     Parent Loop BB61_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ebp, %eax
	xorl	$-2, %eax
	incl	%eax
	movsbq	(%rdx), %r15
	xorl	%r10d, %r10d
	xorl	%r14d, %r14d
	cmpq	$45, %r15
	sete	%r10b
	setne	%r14b
	movl	%r10d, %r8d
	andl	%eax, %r8d
	movl	%r11d, %r9d
	xorl	$1, %r9d
	negl	%r8d
	movl	%r9d, %r12d
	andl	%r8d, %r12d
	movsbq	(%rbx), %rcx
	xorl	%edi, %edi
	xorl	%r13d, %r13d
	cmpq	$45, %rcx
	sete	%dil
	setne	%r13b
	xorps	%xmm3, %xmm3
	cvtss2sd	%xmm1, %xmm3
	shlq	$9, %r15
	xorps	%xmm1, %xmm1
	cvtsi2ssl	amino_dis(%r15,%rcx,4), %xmm1
	negl	%ebp
	andl	%r14d, %ebp
	andl	%r14d, %eax
	negl	%ebp
	andl	%r9d, %ebp
	negl	%eax
	andl	%eax, %r9d
	andl	%r11d, %eax
	andl	%r11d, %r8d
	negl	%r12d
	andl	%r13d, %r12d
	negl	%ebp
	negl	%eax
	andl	%r13d, %eax
	negl	%r8d
	andl	%r13d, %r8d
	andl	%ebp, %r13d
	addl	%r12d, %r13d
	negl	%r9d
	andl	%edi, %r9d
	addl	%r13d, %r9d
	addl	%r9d, %eax
	addl	%eax, %r8d
	andl	%edi, %ebp
	addl	%r8d, %ebp
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%ebp, %xmm4
	cvtss2sd	%xmm4, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	addsd	%xmm4, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	addss	%xmm3, %xmm1
	incq	%rdx
	incq	%rbx
	decq	%rsi
	movl	%edi, %r11d
	movl	%r10d, %ebp
	jne	.LBB61_7
# BB#8:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB61_6 Depth=2
	movq	80(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	cmpq	56(%rsp), %rcx          # 8-byte Folded Reload
	jne	.LBB61_6
.LBB61_2:                               # %.loopexit
                                        #   in Loop: Header=BB61_3 Depth=1
	movq	48(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	jne	.LBB61_3
.LBB61_9:                               # %._crit_edge100
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end61:
	.size	SSPscore, .Lfunc_end61-SSPscore
	.cfi_endproc

	.globl	DSPscore
	.p2align	4, 0x90
	.type	DSPscore,@function
DSPscore:                               # @DSPscore
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi553:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi554:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi555:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi556:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi557:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi558:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi559:
	.cfi_def_cfa_offset 112
.Lcfi560:
	.cfi_offset %rbx, -56
.Lcfi561:
	.cfi_offset %r12, -48
.Lcfi562:
	.cfi_offset %r13, -40
.Lcfi563:
	.cfi_offset %r14, -32
.Lcfi564:
	.cfi_offset %r15, -24
.Lcfi565:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movl	%edi, %ebx
	movq	(%r13), %rdi
	callq	strlen
	xorpd	%xmm0, %xmm0
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	cmpl	$2, %ebx
	jl	.LBB62_26
# BB#1:                                 # %.lr.ph106
	movq	16(%rsp), %rcx          # 8-byte Reload
	leal	-1(%rcx), %esi
	leal	-2(%rax), %edx
	cvtsi2sdl	penalty(%rip), %xmm1
	movslq	%ecx, %rdi
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movl	%ecx, %r11d
	leal	-2(%rcx), %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	xorpd	%xmm2, %xmm2
	movl	$1, %r12d
	xorl	%ecx, %ecx
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movl	%esi, %ebx
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB62_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB62_13 Depth 2
                                        #     Child Loop BB62_16 Depth 2
                                        #     Child Loop BB62_6 Depth 2
                                        #       Child Loop BB62_7 Depth 3
                                        #         Child Loop BB62_20 Depth 4
                                        #         Child Loop BB62_25 Depth 4
	movq	%rcx, %rsi
	leaq	1(%rsi), %rbp
	cmpq	40(%rsp), %rbp          # 8-byte Folded Reload
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movb	%bl, 15(%rsp)           # 1-byte Spill
	jge	.LBB62_2
# BB#4:                                 # %.lr.ph101
                                        #   in Loop: Header=BB62_3 Depth=1
	testl	%eax, %eax
	jle	.LBB62_10
# BB#5:                                 # %.lr.ph101.split.us.preheader
                                        #   in Loop: Header=BB62_3 Depth=1
	movq	(%r13,%rsi,8), %rcx
	leaq	1(%rcx), %r15
	movq	%r12, %rsi
	.p2align	4, 0x90
.LBB62_6:                               # %.lr.ph101.split.us
                                        #   Parent Loop BB62_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB62_7 Depth 3
                                        #         Child Loop BB62_20 Depth 4
                                        #         Child Loop BB62_25 Depth 4
	movq	(%r13,%rsi,8), %rbx
	leaq	1(%rbx), %r8
	xorl	%r14d, %r14d
	xorpd	%xmm3, %xmm3
	.p2align	4, 0x90
.LBB62_7:                               #   Parent Loop BB62_3 Depth=1
                                        #     Parent Loop BB62_6 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB62_20 Depth 4
                                        #         Child Loop BB62_25 Depth 4
	movslq	%r14d, %r9
	movsbq	(%rcx,%r9), %rbp
	cmpq	$45, %rbp
	movsbq	(%rbx,%r9), %rdi
	jne	.LBB62_17
# BB#8:                                 #   in Loop: Header=BB62_7 Depth=3
	cmpb	$45, %dil
	je	.LBB62_9
# BB#23:                                # %.preheader.us
                                        #   in Loop: Header=BB62_7 Depth=3
	shlq	$9, %rbp
	cvtsi2sdl	amino_dis(%rbp,%rdi,4), %xmm4
	addsd	%xmm4, %xmm3
	addsd	%xmm1, %xmm3
	cmpb	$45, 1(%rcx,%r9)
	jne	.LBB62_21
# BB#24:                                # %.lr.ph84.us.preheader
                                        #   in Loop: Header=BB62_7 Depth=3
	incq	%r9
	leaq	(%rbx,%r9), %rdi
	addq	%r15, %r9
	.p2align	4, 0x90
.LBB62_25:                              # %.lr.ph84.us
                                        #   Parent Loop BB62_3 Depth=1
                                        #     Parent Loop BB62_6 Depth=2
                                        #       Parent Loop BB62_7 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsbq	(%rdi), %rbp
	xorps	%xmm4, %xmm4
	cvtsi2sdl	amino_dis+23040(,%rbp,4), %xmm4
	addsd	%xmm4, %xmm3
	incl	%r14d
	incq	%rdi
	cmpb	$45, (%r9)
	leaq	1(%r9), %r9
	je	.LBB62_25
	jmp	.LBB62_21
	.p2align	4, 0x90
.LBB62_17:                              #   in Loop: Header=BB62_7 Depth=3
	shlq	$9, %rbp
	cvtsi2sdl	amino_dis(%rbp,%rdi,4), %xmm4
	addsd	%xmm4, %xmm3
	cmpb	$45, %dil
	jne	.LBB62_9
# BB#18:                                # %.preheader81.us
                                        #   in Loop: Header=BB62_7 Depth=3
	addsd	%xmm1, %xmm3
	cmpb	$45, 1(%rbx,%r9)
	jne	.LBB62_21
# BB#19:                                # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB62_7 Depth=3
	incq	%r9
	leaq	(%rcx,%r9), %r10
	addq	%r8, %r9
	.p2align	4, 0x90
.LBB62_20:                              # %.lr.ph.us
                                        #   Parent Loop BB62_3 Depth=1
                                        #     Parent Loop BB62_6 Depth=2
                                        #       Parent Loop BB62_7 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsbq	(%r10), %rdi
	shlq	$9, %rdi
	xorps	%xmm4, %xmm4
	cvtsi2sdl	amino_dis+180(%rdi), %xmm4
	addsd	%xmm4, %xmm3
	incl	%r14d
	incq	%r10
	cmpb	$45, (%r9)
	leaq	1(%r9), %r9
	je	.LBB62_20
.LBB62_21:                              # %._crit_edge85.us
                                        #   in Loop: Header=BB62_7 Depth=3
	cmpl	%edx, %r14d
	jg	.LBB62_22
.LBB62_9:                               #   in Loop: Header=BB62_7 Depth=3
	incl	%r14d
	cmpl	%eax, %r14d
	jl	.LBB62_7
.LBB62_22:                              # %._crit_edge92.us
                                        #   in Loop: Header=BB62_6 Depth=2
	addsd	%xmm3, %xmm0
	incq	%rsi
	cmpq	%r11, %rsi
	jne	.LBB62_6
	jmp	.LBB62_2
.LBB62_10:                              # %.lr.ph101.split.preheader
                                        #   in Loop: Header=BB62_3 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %edi
	subl	%esi, %edi
	movq	32(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%esi, %ecx
	testb	$7, %dil
	je	.LBB62_11
# BB#12:                                # %.lr.ph101.split.prol.preheader
                                        #   in Loop: Header=BB62_3 Depth=1
	andb	$7, %bl
	movzbl	%bl, %edi
	negl	%edi
	movl	%ebp, %esi
	.p2align	4, 0x90
.LBB62_13:                              # %.lr.ph101.split.prol
                                        #   Parent Loop BB62_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	%xmm2, %xmm0
	incl	%esi
	incl	%edi
	jne	.LBB62_13
	jmp	.LBB62_14
.LBB62_11:                              #   in Loop: Header=BB62_3 Depth=1
	movl	%ebp, %esi
.LBB62_14:                              # %.lr.ph101.split.prol.loopexit
                                        #   in Loop: Header=BB62_3 Depth=1
	cmpl	$7, %ecx
	jb	.LBB62_2
# BB#15:                                # %.lr.ph101.split.preheader.new
                                        #   in Loop: Header=BB62_3 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%esi, %ecx
	.p2align	4, 0x90
.LBB62_16:                              # %.lr.ph101.split
                                        #   Parent Loop BB62_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	%xmm2, %xmm0
	addl	$-8, %ecx
	jne	.LBB62_16
	.p2align	4, 0x90
.LBB62_2:                               # %.loopexit
                                        #   in Loop: Header=BB62_3 Depth=1
	incq	%r12
	movb	15(%rsp), %bl           # 1-byte Reload
	addb	$7, %bl
	movq	48(%rsp), %rcx          # 8-byte Reload
	cmpq	24(%rsp), %rcx          # 8-byte Folded Reload
	jne	.LBB62_3
.LBB62_26:                              # %._crit_edge107
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end62:
	.size	DSPscore, .Lfunc_end62-DSPscore
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI63_0:
	.quad	4636737291354636288     # double 100
.LCPI63_1:
	.quad	4648488871632306176     # double 600
.LCPI63_2:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	searchAnchors
	.p2align	4, 0x90
	.type	searchAnchors,@function
searchAnchors:                          # @searchAnchors
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi566:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi567:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi568:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi569:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi570:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi571:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi572:
	.cfi_def_cfa_offset 96
.Lcfi573:
	.cfi_offset %rbx, -56
.Lcfi574:
	.cfi_offset %r12, -48
.Lcfi575:
	.cfi_offset %r13, -40
.Lcfi576:
	.cfi_offset %r14, -32
.Lcfi577:
	.cfi_offset %r15, -24
.Lcfi578:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r13
	movl	%edi, %r12d
	movq	(%r13), %rdi
	callq	strlen
	movq	%rax, %rbx
	movl	searchAnchors.alloclen(%rip), %eax
	cmpl	%ebx, %eax
	jge	.LBB63_5
# BB#1:
	testl	%eax, %eax
	je	.LBB63_3
# BB#2:
	movq	searchAnchors.stra(%rip), %rdi
	callq	FreeDoubleVec
	jmp	.LBB63_4
.LBB63_3:
	cvtsi2sdl	divThreshold(%rip), %xmm0
	divsd	.LCPI63_0(%rip), %xmm0
	mulsd	.LCPI63_1(%rip), %xmm0
	cvtsi2sdl	divWinSize(%rip), %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, searchAnchors.threshold(%rip)
.LBB63_4:
	movl	%ebx, %edi
	callq	AllocateDoubleVec
	movq	%rax, searchAnchors.stra(%rip)
	movl	%ebx, searchAnchors.alloclen(%rip)
.LBB63_5:                               # %.preheader113
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	testl	%ebx, %ebx
	jle	.LBB63_22
# BB#6:                                 # %.lr.ph139
	movq	searchAnchors.stra(%rip), %rax
	leal	-1(%r12), %ecx
	cmpl	$1, %r12d
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r12d, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	mulsd	%xmm1, %xmm0
	mulsd	.LCPI63_2(%rip), %xmm0
	jle	.LBB63_7
# BB#15:                                # %.lr.ph139.split.us.preheader
	movslq	%r12d, %r11
	movl	%r12d, %edx
	movl	%ecx, %r12d
	movl	8(%rsp), %ecx           # 4-byte Reload
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	leaq	8(%r13), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	decq	%rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB63_16:                              # %.lr.ph139.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB63_17 Depth 2
                                        #       Child Loop BB63_19 Depth 3
	movq	$0, (%rax,%rdi,8)
	xorpd	%xmm1, %xmm1
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB63_17:                              #   Parent Loop BB63_16 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB63_19 Depth 3
	leaq	1(%rcx), %r9
	cmpq	%r11, %r9
	jge	.LBB63_20
# BB#18:                                # %.lr.ph132.us
                                        #   in Loop: Header=BB63_17 Depth=2
	movq	(%r13,%rcx,8), %rcx
	movq	%r10, %rdx
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB63_19:                              #   Parent Loop BB63_16 Depth=1
                                        #     Parent Loop BB63_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsbq	(%rcx,%rdi), %r8
	movslq	amino_n(,%r8,4), %rbx
	movq	(%rbp), %rsi
	movsbq	(%rsi,%rdi), %rsi
	movslq	amino_n(,%rsi,4), %rsi
	imulq	$104, %rbx, %rbx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	n_dis(%rbx,%rsi,4), %xmm2
	addsd	%xmm2, %xmm1
	movsd	%xmm1, (%rax,%rdi,8)
	addq	$8, %rbp
	decq	%rdx
	jne	.LBB63_19
.LBB63_20:                              # %.loopexit.us
                                        #   in Loop: Header=BB63_17 Depth=2
	addq	$8, %r14
	decq	%r10
	cmpq	%r12, %r9
	movq	%r9, %rcx
	jne	.LBB63_17
# BB#21:                                # %._crit_edge136.us
                                        #   in Loop: Header=BB63_16 Depth=1
	divsd	%xmm0, %xmm1
	movsd	%xmm1, (%rax,%rdi,8)
	incq	%rdi
	cmpq	32(%rsp), %rdi          # 8-byte Folded Reload
	jne	.LBB63_16
	jmp	.LBB63_22
.LBB63_7:                               # %.lr.ph139.split.preheader
	xorpd	%xmm1, %xmm1
	divsd	%xmm0, %xmm1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	%edx, %ecx
	cmpq	$3, %rcx
	jbe	.LBB63_8
# BB#11:                                # %min.iters.checked
	movl	%edx, %esi
	andl	$3, %esi
	movq	%rcx, %rbp
	subq	%rsi, %rbp
	je	.LBB63_8
# BB#12:                                # %vector.ph
	movapd	%xmm1, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	leaq	16(%rax), %rdi
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB63_13:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -16(%rdi)
	movups	%xmm0, (%rdi)
	addq	$32, %rdi
	addq	$-4, %rdx
	jne	.LBB63_13
# BB#14:                                # %middle.block
	testq	%rsi, %rsi
	jne	.LBB63_9
	jmp	.LBB63_22
.LBB63_8:
	xorl	%ebp, %ebp
.LBB63_9:                               # %.lr.ph139.split.preheader172
	leaq	(%rax,%rbp,8), %rax
	subq	%rbp, %rcx
	.p2align	4, 0x90
.LBB63_10:                              # %.lr.ph139.split
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm1, (%rax)
	addq	$8, %rax
	decq	%rcx
	jne	.LBB63_10
.LBB63_22:                              # %._crit_edge140
	movl	$0, 24(%r15)
	movl	$0, 76(%r15)
	movslq	divWinSize(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB63_23
# BB#24:                                # %.lr.ph128
	movq	searchAnchors.stra(%rip), %rax
	leaq	-1(%rcx), %rsi
	movq	%rcx, %rdi
	andq	$7, %rdi
	je	.LBB63_25
# BB#26:                                # %.prol.preheader
	xorpd	%xmm1, %xmm1
	xorl	%edx, %edx
	movq	8(%rsp), %r12           # 8-byte Reload
	.p2align	4, 0x90
.LBB63_27:                              # =>This Inner Loop Header: Depth=1
	addsd	(%rax,%rdx,8), %xmm1
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB63_27
	jmp	.LBB63_28
.LBB63_23:
	xorpd	%xmm1, %xmm1
	movq	8(%rsp), %r12           # 8-byte Reload
	jmp	.LBB63_29
.LBB63_25:
	xorl	%edx, %edx
	xorpd	%xmm1, %xmm1
	movq	8(%rsp), %r12           # 8-byte Reload
.LBB63_28:                              # %.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB63_29
	.p2align	4, 0x90
.LBB63_44:                              # =>This Inner Loop Header: Depth=1
	addsd	(%rax,%rdx,8), %xmm1
	addsd	8(%rax,%rdx,8), %xmm1
	addsd	16(%rax,%rdx,8), %xmm1
	addsd	24(%rax,%rdx,8), %xmm1
	addsd	32(%rax,%rdx,8), %xmm1
	addsd	40(%rax,%rdx,8), %xmm1
	addsd	48(%rax,%rdx,8), %xmm1
	addsd	56(%rax,%rdx,8), %xmm1
	addq	$8, %rdx
	cmpq	%rcx, %rdx
	jl	.LBB63_44
.LBB63_29:                              # %.preheader
	movl	%r12d, %edx
	subl	%ecx, %edx
	xorl	%eax, %eax
	cmpl	$2, %edx
	jl	.LBB63_43
# BB#30:                                # %.lr.ph.preheader
	xorl	%esi, %esi
	xorpd	%xmm2, %xmm2
	xorl	%edi, %edi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB63_31:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rbx
	movq	searchAnchors.stra(%rip), %rdx
	subsd	(%rdx,%rbx,8), %xmm1
	addl	%ebx, %ecx
	movslq	%ecx, %rcx
	addsd	(%rdx,%rcx,8), %xmm1
	movsd	searchAnchors.threshold(%rip), %xmm0 # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB63_35
# BB#32:                                #   in Loop: Header=BB63_31 Depth=1
	testl	%r14d, %r14d
	jne	.LBB63_34
# BB#33:                                #   in Loop: Header=BB63_31 Depth=1
	leal	1(%rbx), %ecx
	movl	%ecx, (%r15)
	xorpd	%xmm2, %xmm2
	xorl	%edi, %edi
	movl	$1, %r14d
.LBB63_34:                              #   in Loop: Header=BB63_31 Depth=1
	incl	%edi
	addsd	%xmm1, %xmm2
.LBB63_35:                              #   in Loop: Header=BB63_31 Depth=1
	ucomisd	%xmm1, %xmm0
	setae	%cl
	cmpl	$150, %edi
	setg	%dl
	testl	%r14d, %r14d
	je	.LBB63_40
# BB#36:                                #   in Loop: Header=BB63_31 Depth=1
	orb	%cl, %dl
	je	.LBB63_40
# BB#37:                                #   in Loop: Header=BB63_31 Depth=1
	xorl	%ecx, %ecx
	cmpl	$150, %edi
	setg	%cl
	leal	1(%rbx), %edx
	movl	%edx, 4(%r15)
	movl	(%r15), %edx
	addl	divWinSize(%rip), %edx
	leal	(%rbx,%rdx), %esi
	leal	1(%rbx,%rdx), %edx
	shrl	$31, %edx
	leal	1(%rdx,%rsi), %edx
	sarl	%edx
	movl	%edx, 8(%r15)
	movsd	%xmm2, 16(%r15)
	movl	%ecx, 24(%r15)
	movl	%ecx, 76(%r15)
	addq	$48, %r15
	leal	1(%rax), %ebp
	xorpd	%xmm2, %xmm2
	xorl	%r14d, %r14d
	cmpl	$99997, %eax            # imm = 0x1869D
	jl	.LBB63_39
# BB#38:                                #   in Loop: Header=BB63_31 Depth=1
	movl	$.L.str.30, %edi
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	callq	ErrorExit
	xorpd	%xmm2, %xmm2
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
.LBB63_39:                              #   in Loop: Header=BB63_31 Depth=1
	movl	%ebp, %eax
	xorl	%edi, %edi
.LBB63_40:                              #   in Loop: Header=BB63_31 Depth=1
	movl	divWinSize(%rip), %ecx
	movl	%r12d, %edx
	subl	%ecx, %edx
	movslq	%edx, %rdx
	leaq	1(%rbx), %rsi
	addq	$2, %rbx
	cmpq	%rdx, %rbx
	jl	.LBB63_31
# BB#41:                                # %._crit_edge
	testl	%r14d, %r14d
	je	.LBB63_43
# BB#42:
	leaq	1(%rsi), %rdx
	movl	%edx, 4(%r15)
	addl	(%r15), %ecx
	leal	(%rcx,%rsi), %edx
	leal	1(%rsi,%rcx), %ecx
	shrl	$31, %ecx
	leal	1(%rcx,%rdx), %ecx
	sarl	%ecx
	movl	%ecx, 8(%r15)
	movsd	%xmm2, 16(%r15)
	incl	%eax
.LBB63_43:                              # %._crit_edge.thread
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end63:
	.size	searchAnchors, .Lfunc_end63-searchAnchors
	.cfi_endproc

	.globl	dontcalcimportance
	.p2align	4, 0x90
	.type	dontcalcimportance,@function
dontcalcimportance:                     # @dontcalcimportance
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi579:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi580:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi581:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi582:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi583:
	.cfi_def_cfa_offset 48
.Lcfi584:
	.cfi_offset %rbx, -48
.Lcfi585:
	.cfi_offset %r12, -40
.Lcfi586:
	.cfi_offset %r14, -32
.Lcfi587:
	.cfi_offset %r15, -24
.Lcfi588:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r12
	movl	%edi, %r14d
	movq	dontcalcimportance.nogaplen(%rip), %rax
	testq	%rax, %rax
	jne	.LBB64_2
# BB#1:
	movl	%r14d, %edi
	callq	AllocateIntVec
	movq	%rax, dontcalcimportance.nogaplen(%rip)
.LBB64_2:                               # %.preheader30
	testl	%r14d, %r14d
	jle	.LBB64_16
# BB#3:                                 # %.lr.ph38.preheader
	movl	%r14d, %ecx
	xorl	%r8d, %r8d
	testb	$1, %cl
	je	.LBB64_8
# BB#4:                                 # %.lr.ph38.prol
	movq	(%r12), %rsi
	movb	(%rsi), %dl
	testb	%dl, %dl
	je	.LBB64_7
# BB#5:                                 # %.lr.ph.i.preheader.prol
	incq	%rsi
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB64_6:                               # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edi, %edi
	cmpb	$45, %dl
	setne	%dil
	addl	%edi, %r8d
	movzbl	(%rsi), %edx
	incq	%rsi
	testb	%dl, %dl
	jne	.LBB64_6
.LBB64_7:                               # %seqlen.exit.prol
	movl	%r8d, (%rax)
	movl	$1, %r8d
.LBB64_8:                               # %.lr.ph38.prol.loopexit
	cmpl	$1, %r14d
	je	.LBB64_9
	.p2align	4, 0x90
.LBB64_17:                              # %.lr.ph38
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB64_19 Depth 2
                                        #     Child Loop BB64_22 Depth 2
	movq	(%r12,%r8,8), %rdi
	movb	(%rdi), %dl
	xorl	%esi, %esi
	testb	%dl, %dl
	movl	$0, %ebx
	je	.LBB64_20
# BB#18:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB64_17 Depth=1
	incq	%rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB64_19:                              # %.lr.ph.i
                                        #   Parent Loop BB64_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%ebp, %ebp
	cmpb	$45, %dl
	setne	%bpl
	addl	%ebp, %ebx
	movzbl	(%rdi), %edx
	incq	%rdi
	testb	%dl, %dl
	jne	.LBB64_19
.LBB64_20:                              # %seqlen.exit
                                        #   in Loop: Header=BB64_17 Depth=1
	movl	%ebx, (%rax,%r8,4)
	movq	8(%r12,%r8,8), %rdi
	movb	(%rdi), %dl
	testb	%dl, %dl
	je	.LBB64_23
# BB#21:                                # %.lr.ph.i.preheader.1
                                        #   in Loop: Header=BB64_17 Depth=1
	incq	%rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB64_22:                              # %.lr.ph.i.1
                                        #   Parent Loop BB64_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%ebp, %ebp
	cmpb	$45, %dl
	setne	%bpl
	addl	%ebp, %esi
	movzbl	(%rdi), %edx
	incq	%rdi
	testb	%dl, %dl
	jne	.LBB64_22
.LBB64_23:                              # %seqlen.exit.1
                                        #   in Loop: Header=BB64_17 Depth=1
	movl	%esi, 4(%rax,%r8,4)
	addq	$2, %r8
	cmpq	%rcx, %r8
	jne	.LBB64_17
.LBB64_9:                               # %.preheader29
	testl	%r14d, %r14d
	jle	.LBB64_16
# BB#10:                                # %.preheader.us.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB64_11:                              # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB64_12 Depth 2
                                        #       Child Loop BB64_13 Depth 3
	movq	(%r15,%rax,8), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB64_12:                              #   Parent Loop BB64_11 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB64_13 Depth 3
	leaq	(%rsi,%rsi,4), %rdi
	shlq	$4, %rdi
	addq	%rdx, %rdi
	je	.LBB64_14
	.p2align	4, 0x90
.LBB64_13:                              # %.lr.ph.us
                                        #   Parent Loop BB64_11 Depth=1
                                        #     Parent Loop BB64_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	40(%rdi), %xmm0         # xmm0 = mem[0],zero
	xorps	%xmm1, %xmm1
	cvtsi2sdl	48(%rdi), %xmm1
	divsd	%xmm1, %xmm0
	movsd	%xmm0, 56(%rdi)
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 64(%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB64_13
.LBB64_14:                              # %._crit_edge.us
                                        #   in Loop: Header=BB64_12 Depth=2
	incq	%rsi
	cmpq	%rcx, %rsi
	jne	.LBB64_12
# BB#15:                                # %._crit_edge34.us
                                        #   in Loop: Header=BB64_11 Depth=1
	incq	%rax
	cmpq	%rcx, %rax
	jne	.LBB64_11
.LBB64_16:                              # %._crit_edge36
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end64:
	.size	dontcalcimportance, .Lfunc_end64-dontcalcimportance
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI65_0:
	.quad	-4616189618054758400    # double -1
.LCPI65_1:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	calcimportance
	.p2align	4, 0x90
	.type	calcimportance,@function
calcimportance:                         # @calcimportance
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi589:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi590:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi591:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi592:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi593:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi594:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi595:
	.cfi_def_cfa_offset 112
.Lcfi596:
	.cfi_offset %rbx, -56
.Lcfi597:
	.cfi_offset %r12, -48
.Lcfi598:
	.cfi_offset %r13, -40
.Lcfi599:
	.cfi_offset %r14, -32
.Lcfi600:
	.cfi_offset %r15, -24
.Lcfi601:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %r14
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movl	%edi, %r12d
	cmpq	$0, calcimportance.importance(%rip)
	jne	.LBB65_2
# BB#1:
	movl	nlenmax(%rip), %edi
	callq	AllocateDoubleVec
	movq	%rax, calcimportance.importance(%rip)
	movl	%r12d, %edi
	callq	AllocateIntVec
	movq	%rax, calcimportance.nogaplen(%rip)
.LBB65_2:                               # %.preheader132
	testl	%r12d, %r12d
	jle	.LBB65_69
# BB#3:                                 # %.lr.ph171
	movq	calcimportance.nogaplen(%rip), %rax
	movl	%r12d, %r15d
	xorl	%ecx, %ecx
	testb	$1, %r15b
	je	.LBB65_8
# BB#4:
	movq	(%r14), %rdx
	movb	(%rdx), %bl
	testb	%bl, %bl
	je	.LBB65_7
# BB#5:                                 # %.lr.ph.i.preheader.prol
	incq	%rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB65_6:                               # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	cmpb	$45, %bl
	setne	%sil
	addl	%esi, %ecx
	movzbl	(%rdx), %ebx
	incq	%rdx
	testb	%bl, %bl
	jne	.LBB65_6
.LBB65_7:                               # %seqlen.exit.prol
	movl	%ecx, (%rax)
	movl	$1, %ecx
.LBB65_8:                               # %.prol.loopexit
	cmpl	$1, %r12d
	je	.LBB65_9
	.p2align	4, 0x90
.LBB65_25:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB65_27 Depth 2
                                        #     Child Loop BB65_30 Depth 2
	movq	(%r14,%rcx,8), %rsi
	movb	(%rsi), %bl
	xorl	%edx, %edx
	testb	%bl, %bl
	movl	$0, %edi
	je	.LBB65_28
# BB#26:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB65_25 Depth=1
	incq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB65_27:                              # %.lr.ph.i
                                        #   Parent Loop BB65_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%ebp, %ebp
	cmpb	$45, %bl
	setne	%bpl
	addl	%ebp, %edi
	movzbl	(%rsi), %ebx
	incq	%rsi
	testb	%bl, %bl
	jne	.LBB65_27
.LBB65_28:                              # %seqlen.exit
                                        #   in Loop: Header=BB65_25 Depth=1
	movl	%edi, (%rax,%rcx,4)
	movq	8(%r14,%rcx,8), %rsi
	movb	(%rsi), %bl
	testb	%bl, %bl
	je	.LBB65_31
# BB#29:                                # %.lr.ph.i.preheader.1
                                        #   in Loop: Header=BB65_25 Depth=1
	incq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB65_30:                              # %.lr.ph.i.1
                                        #   Parent Loop BB65_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%edi, %edi
	cmpb	$45, %bl
	setne	%dil
	addl	%edi, %edx
	movzbl	(%rsi), %ebx
	incq	%rsi
	testb	%bl, %bl
	jne	.LBB65_30
.LBB65_31:                              # %seqlen.exit.1
                                        #   in Loop: Header=BB65_25 Depth=1
	movl	%edx, 4(%rax,%rcx,4)
	addq	$2, %rcx
	cmpq	%r15, %rcx
	jne	.LBB65_25
.LBB65_9:                               # %.preheader131
	testl	%r12d, %r12d
	jle	.LBB65_69
# BB#10:                                # %.preheader130.lr.ph
	movl	%r12d, 12(%rsp)         # 4-byte Spill
	movl	nlenmax(%rip), %eax
	movq	calcimportance.importance(%rip), %rbp
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leal	-1(%rax), %eax
	leaq	8(,%rax,8), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8(%rbp), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	48(%rbp), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	movsd	.LCPI65_0(%rip), %xmm3  # xmm3 = mem[0],zero
	.p2align	4, 0x90
.LBB65_11:                              # %.preheader130
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB65_14 Depth 2
                                        #       Child Loop BB65_17 Depth 3
                                        #         Child Loop BB65_37 Depth 4
                                        #         Child Loop BB65_41 Depth 4
                                        #     Child Loop BB65_45 Depth 2
                                        #       Child Loop BB65_48 Depth 3
                                        #         Child Loop BB65_52 Depth 4
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB65_13
# BB#12:                                # %.lr.ph145.preheader
                                        #   in Loop: Header=BB65_11 Depth=1
	xorl	%esi, %esi
	movq	%rbp, %rdi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	memset
	movsd	.LCPI65_0(%rip), %xmm3  # xmm3 = mem[0],zero
.LBB65_13:                              # %.lr.ph153
                                        #   in Loop: Header=BB65_11 Depth=1
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB65_14:                              #   Parent Loop BB65_11 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB65_17 Depth 3
                                        #         Child Loop BB65_37 Depth 4
                                        #         Child Loop BB65_41 Depth 4
	cmpq	%r9, %r14
	je	.LBB65_43
# BB#15:                                #   in Loop: Header=BB65_14 Depth=2
	leaq	(%r9,%r9,4), %rcx
	shlq	$4, %rcx
	addq	(%r13,%r14,8), %rcx
	je	.LBB65_43
# BB#16:                                # %.lr.ph151
                                        #   in Loop: Header=BB65_14 Depth=2
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r9,8), %rdx
	.p2align	4, 0x90
.LBB65_17:                              #   Parent Loop BB65_11 Depth=1
                                        #     Parent Loop BB65_14 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB65_37 Depth 4
                                        #         Child Loop BB65_41 Depth 4
	movsd	40(%rcx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	%xmm3, %xmm0
	jne	.LBB65_18
	jnp	.LBB65_42
.LBB65_18:                              #   in Loop: Header=BB65_17 Depth=3
	movslq	24(%rcx), %rsi
	movslq	28(%rcx), %rdi
	cmpl	%edi, %esi
	jg	.LBB65_42
# BB#19:                                # %.lr.ph148
                                        #   in Loop: Header=BB65_17 Depth=3
	cmpq	%rdi, %rsi
	movq	%rdi, %r10
	cmovgeq	%rsi, %r10
	incq	%r10
	subq	%rsi, %r10
	cmpq	$4, %r10
	jb	.LBB65_40
# BB#20:                                # %min.iters.checked
                                        #   in Loop: Header=BB65_17 Depth=3
	movq	%r10, %r8
	andq	$-4, %r8
	je	.LBB65_40
# BB#21:                                # %vector.memcheck
                                        #   in Loop: Header=BB65_17 Depth=3
	leaq	(%rbp,%rsi,8), %rbx
	cmpq	%rdi, %rsi
	movq	%rdi, %rax
	cmovgeq	%rsi, %rax
	cmpq	%rdx, %rbx
	jae	.LBB65_23
# BB#22:                                # %vector.memcheck
                                        #   in Loop: Header=BB65_17 Depth=3
	movq	48(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rax,8), %rax
	cmpq	%rax, %rdx
	jb	.LBB65_40
.LBB65_23:                              # %vector.body.preheader
                                        #   in Loop: Header=BB65_17 Depth=3
	leaq	-4(%r8), %rax
	movq	%rax, %r11
	shrq	$2, %r11
	btl	$2, %eax
	jb	.LBB65_24
# BB#34:                                # %vector.body.prol
                                        #   in Loop: Header=BB65_17 Depth=3
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movupd	(%rbp,%rsi,8), %xmm1
	movupd	16(%rbp,%rsi,8), %xmm2
	addpd	%xmm0, %xmm1
	addpd	%xmm0, %xmm2
	movupd	%xmm1, (%rbp,%rsi,8)
	movupd	%xmm2, 16(%rbp,%rsi,8)
	movl	$4, %r12d
	testq	%r11, %r11
	jne	.LBB65_36
	jmp	.LBB65_38
.LBB65_24:                              #   in Loop: Header=BB65_17 Depth=3
	xorl	%r12d, %r12d
	testq	%r11, %r11
	je	.LBB65_38
.LBB65_36:                              # %vector.body.preheader.new
                                        #   in Loop: Header=BB65_17 Depth=3
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movq	%r8, %rax
	subq	%r12, %rax
	addq	%rsi, %r12
	movq	32(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%r12,8), %rbx
	.p2align	4, 0x90
.LBB65_37:                              # %vector.body
                                        #   Parent Loop BB65_11 Depth=1
                                        #     Parent Loop BB65_14 Depth=2
                                        #       Parent Loop BB65_17 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movupd	-48(%rbx), %xmm1
	movupd	-32(%rbx), %xmm2
	addpd	%xmm0, %xmm1
	addpd	%xmm0, %xmm2
	movupd	%xmm1, -48(%rbx)
	movupd	%xmm2, -32(%rbx)
	movupd	-16(%rbx), %xmm1
	movupd	(%rbx), %xmm2
	addpd	%xmm0, %xmm1
	addpd	%xmm0, %xmm2
	movupd	%xmm1, -16(%rbx)
	movupd	%xmm2, (%rbx)
	addq	$64, %rbx
	addq	$-8, %rax
	jne	.LBB65_37
.LBB65_38:                              # %middle.block
                                        #   in Loop: Header=BB65_17 Depth=3
	cmpq	%r8, %r10
	je	.LBB65_42
# BB#39:                                #   in Loop: Header=BB65_17 Depth=3
	addq	%r8, %rsi
.LBB65_40:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB65_17 Depth=3
	decq	%rsi
	.p2align	4, 0x90
.LBB65_41:                              # %scalar.ph
                                        #   Parent Loop BB65_11 Depth=1
                                        #     Parent Loop BB65_14 Depth=2
                                        #       Parent Loop BB65_17 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	addsd	8(%rbp,%rsi,8), %xmm0
	movsd	%xmm0, 8(%rbp,%rsi,8)
	incq	%rsi
	cmpq	%rdi, %rsi
	jl	.LBB65_41
	.p2align	4, 0x90
.LBB65_42:                              # %.loopexit125
                                        #   in Loop: Header=BB65_17 Depth=3
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB65_17
.LBB65_43:                              # %.loopexit127
                                        #   in Loop: Header=BB65_14 Depth=2
	incq	%r9
	cmpq	%r15, %r9
	jne	.LBB65_14
# BB#44:                                # %.lr.ph167
                                        #   in Loop: Header=BB65_11 Depth=1
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB65_45:                              #   Parent Loop BB65_11 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB65_48 Depth 3
                                        #         Child Loop BB65_52 Depth 4
	cmpq	%r8, %r14
	je	.LBB65_55
# BB#46:                                #   in Loop: Header=BB65_45 Depth=2
	movq	(%r13,%r14,8), %rcx
	leaq	(%r8,%r8,4), %rdx
	shlq	$4, %rdx
	movsd	40(%rcx,%rdx), %xmm0    # xmm0 = mem[0],zero
	ucomisd	%xmm3, %xmm0
	jne	.LBB65_47
	jnp	.LBB65_55
.LBB65_47:                              #   in Loop: Header=BB65_45 Depth=2
	addq	%rdx, %rcx
	je	.LBB65_55
	.p2align	4, 0x90
.LBB65_48:                              # %.lr.ph165
                                        #   Parent Loop BB65_11 Depth=1
                                        #     Parent Loop BB65_45 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB65_52 Depth 4
	movsd	40(%rcx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	%xmm3, %xmm0
	jne	.LBB65_49
	jnp	.LBB65_54
.LBB65_49:                              #   in Loop: Header=BB65_48 Depth=3
	movslq	24(%rcx), %rdx
	movslq	28(%rcx), %rdi
	cmpl	%edi, %edx
	jle	.LBB65_51
# BB#50:                                #   in Loop: Header=BB65_48 Depth=3
	xorpd	%xmm1, %xmm1
	xorl	%esi, %esi
	jmp	.LBB65_53
.LBB65_51:                              # %.lr.ph158
                                        #   in Loop: Header=BB65_48 Depth=3
	leaq	(%rbp,%rdx,8), %rbx
	decq	%rdx
	xorpd	%xmm1, %xmm1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB65_52:                              #   Parent Loop BB65_11 Depth=1
                                        #     Parent Loop BB65_45 Depth=2
                                        #       Parent Loop BB65_48 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	addsd	(%rbx,%rsi,8), %xmm1
	leaq	1(%rdx,%rsi), %rax
	incq	%rsi
	cmpq	%rdi, %rax
	jl	.LBB65_52
.LBB65_53:                              # %._crit_edge159
                                        #   in Loop: Header=BB65_48 Depth=3
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%esi, %xmm2
	divsd	%xmm2, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 56(%rcx)
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 64(%rcx)
.LBB65_54:                              #   in Loop: Header=BB65_48 Depth=3
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB65_48
	.p2align	4, 0x90
.LBB65_55:                              # %.loopexit126
                                        #   in Loop: Header=BB65_45 Depth=2
	incq	%r8
	cmpq	%r15, %r8
	jne	.LBB65_45
# BB#56:                                # %._crit_edge168
                                        #   in Loop: Header=BB65_11 Depth=1
	incq	%r14
	cmpq	%r15, %r14
	jne	.LBB65_11
# BB#32:                                # %.preheader
	movl	12(%rsp), %eax          # 4-byte Reload
	cmpl	$2, %eax
	jl	.LBB65_69
# BB#33:                                # %.lr.ph142.preheader
	movslq	%eax, %r11
	decl	%eax
	movslq	%eax, %r8
	movl	$1, %r10d
	xorl	%r9d, %r9d
	movsd	.LCPI65_1(%rip), %xmm0  # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB65_58:                              # %.lr.ph142
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB65_60 Depth 2
                                        #       Child Loop BB65_61 Depth 3
	movq	%r9, %rdx
	leaq	1(%rdx), %r9
	cmpq	%r11, %r9
	jge	.LBB65_57
# BB#59:                                # %.lr.ph140
                                        #   in Loop: Header=BB65_58 Depth=1
	movq	(%r13,%rdx,8), %rdi
	movq	%r10, %rcx
	.p2align	4, 0x90
.LBB65_60:                              #   Parent Loop BB65_58 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB65_61 Depth 3
	leaq	(%rcx,%rcx,4), %rsi
	shlq	$4, %rsi
	leaq	(%rdx,%rdx,4), %rbp
	shlq	$4, %rbp
	addq	(%r13,%rcx,8), %rbp
	setne	%al
	addq	%rdi, %rsi
	je	.LBB65_66
	.p2align	4, 0x90
.LBB65_61:                              #   Parent Loop BB65_58 Depth=1
                                        #     Parent Loop BB65_60 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rbp, %rbp
	je	.LBB65_66
# BB#62:                                # %.lr.ph
                                        #   in Loop: Header=BB65_61 Depth=3
	movsd	40(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm3, %xmm1
	jne	.LBB65_63
	jnp	.LBB65_65
.LBB65_63:                              #   in Loop: Header=BB65_61 Depth=3
	movsd	40(%rbp), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm3, %xmm1
	jne	.LBB65_64
	jnp	.LBB65_65
.LBB65_64:                              #   in Loop: Header=BB65_61 Depth=3
	movsd	56(%rsi), %xmm1         # xmm1 = mem[0],zero
	addsd	56(%rbp), %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 56(%rbp)
	movsd	%xmm1, 56(%rsi)
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, 64(%rbp)
	movss	%xmm1, 64(%rsi)
	.p2align	4, 0x90
.LBB65_65:                              #   in Loop: Header=BB65_61 Depth=3
	movq	8(%rsi), %rsi
	movq	8(%rbp), %rbp
	testq	%rbp, %rbp
	setne	%al
	testq	%rsi, %rsi
	jne	.LBB65_61
.LBB65_66:                              # %._crit_edge
                                        #   in Loop: Header=BB65_60 Depth=2
	testq	%rsi, %rsi
	sete	%bl
	testb	%al, %bl
	jne	.LBB65_70
# BB#67:                                # %._crit_edge
                                        #   in Loop: Header=BB65_60 Depth=2
	orb	%al, %bl
	xorb	$1, %bl
	jne	.LBB65_70
# BB#68:                                #   in Loop: Header=BB65_60 Depth=2
	incq	%rcx
	cmpq	%r11, %rcx
	jl	.LBB65_60
.LBB65_57:                              # %.loopexit
                                        #   in Loop: Header=BB65_58 Depth=1
	incq	%r10
	cmpq	%r8, %r9
	jl	.LBB65_58
.LBB65_69:                              # %._crit_edge143
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB65_70:
	movq	stderr(%rip), %rdi
	movl	$.L.str.31, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end65:
	.size	calcimportance, .Lfunc_end65-calcimportance
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI66_0:
	.quad	-4616189618054758400    # double -1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI66_1:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI66_2:
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.text
	.globl	extendlocalhom2
	.p2align	4, 0x90
	.type	extendlocalhom2,@function
extendlocalhom2:                        # @extendlocalhom2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi602:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi603:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi604:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi605:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi606:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi607:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi608:
	.cfi_def_cfa_offset 192
.Lcfi609:
	.cfi_offset %rbx, -56
.Lcfi610:
	.cfi_offset %r12, -48
.Lcfi611:
	.cfi_offset %r13, -40
.Lcfi612:
	.cfi_offset %r14, -32
.Lcfi613:
	.cfi_offset %r15, -24
.Lcfi614:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movl	%edi, %r15d
	cmpq	$0, extendlocalhom2.ini(%rip)
	jne	.LBB66_2
# BB#1:
	movl	nlenmax(%rip), %edi
	incl	%edi
	callq	AllocateIntVec
	movq	%rax, extendlocalhom2.ini(%rip)
	movl	nlenmax(%rip), %edi
	incl	%edi
	callq	AllocateIntVec
	movq	%rax, extendlocalhom2.inj(%rip)
.LBB66_2:                               # %.preheader213
	cmpl	$2, %r15d
	jl	.LBB66_86
# BB#3:                                 # %.lr.ph276
	leal	-1(%r15), %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movslq	%r15d, %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	%r15d, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	movl	$1, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%r11d, %r11d
	xorl	%r10d, %r10d
	movq	%r12, 104(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB66_5:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB66_7 Depth 2
                                        #       Child Loop BB66_8 Depth 3
                                        #         Child Loop BB66_16 Depth 4
                                        #           Child Loop BB66_29 Depth 5
                                        #           Child Loop BB66_32 Depth 5
                                        #         Child Loop BB66_35 Depth 4
                                        #           Child Loop BB66_48 Depth 5
                                        #           Child Loop BB66_51 Depth 5
                                        #         Child Loop BB66_62 Depth 4
                                        #         Child Loop BB66_70 Depth 4
	movq	%rcx, %r14
	leaq	1(%r14), %rcx
	cmpq	88(%rsp), %rcx          # 8-byte Folded Reload
	jge	.LBB66_4
# BB#6:                                 # %.preheader.lr.ph
                                        #   in Loop: Header=BB66_5 Depth=1
	movq	72(%rsp), %rbp          # 8-byte Reload
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	%r14, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB66_7:                               # %.lr.ph263
                                        #   Parent Loop BB66_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB66_8 Depth 3
                                        #         Child Loop BB66_16 Depth 4
                                        #           Child Loop BB66_29 Depth 5
                                        #           Child Loop BB66_32 Depth 5
                                        #         Child Loop BB66_35 Depth 4
                                        #           Child Loop BB66_48 Depth 5
                                        #           Child Loop BB66_51 Depth 5
                                        #         Child Loop BB66_62 Depth 4
                                        #         Child Loop BB66_70 Depth 4
	xorl	%r9d, %r9d
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB66_8:                               #   Parent Loop BB66_5 Depth=1
                                        #     Parent Loop BB66_7 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB66_16 Depth 4
                                        #           Child Loop BB66_29 Depth 5
                                        #           Child Loop BB66_32 Depth 5
                                        #         Child Loop BB66_35 Depth 4
                                        #           Child Loop BB66_48 Depth 5
                                        #           Child Loop BB66_51 Depth 5
                                        #         Child Loop BB66_62 Depth 4
                                        #         Child Loop BB66_70 Depth 4
	cmpq	%r14, %r9
	je	.LBB66_84
# BB#9:                                 #   in Loop: Header=BB66_8 Depth=3
	cmpq	%rbp, %r9
	je	.LBB66_84
# BB#10:                                #   in Loop: Header=BB66_8 Depth=3
	cmpq	%r9, %r14
	movq	%r9, %rax
	cmovleq	%r14, %rax
	cltq
	movq	(%r12,%rax,8), %rax
	movq	%r9, %rcx
	cmovgeq	%r14, %rcx
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	movq	(%r12,%r14,8), %rax
	movsd	(%rax,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	thrinter(%rip), %xmm0
	ucomisd	%xmm0, %xmm1
	ja	.LBB66_84
# BB#11:                                #   in Loop: Header=BB66_8 Depth=3
	cmpq	%r9, %rbp
	movq	%r9, %rax
	cmovleq	%rbp, %rax
	cltq
	movq	(%r12,%rax,8), %rax
	movq	%r9, %rcx
	cmovgeq	%rbp, %rcx
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB66_84
# BB#12:                                #   in Loop: Header=BB66_8 Depth=3
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movl	%r10d, 12(%rsp)         # 4-byte Spill
	movl	%r11d, %r13d
	movl	nlenmax(%rip), %eax
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	cmpq	%rcx, %rax
	je	.LBB66_15
# BB#13:                                # %._crit_edge
                                        #   in Loop: Header=BB66_8 Depth=3
	movq	extendlocalhom2.ini(%rip), %rdi
	leaq	4(,%rax,4), %rdx
	movl	$255, %esi
	callq	memset
	movl	nlenmax(%rip), %eax
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	cmpq	%rcx, %rax
	je	.LBB66_15
# BB#14:                                # %.lr.ph219.preheader
                                        #   in Loop: Header=BB66_8 Depth=3
	movq	extendlocalhom2.inj(%rip), %rdi
	leaq	4(,%rax,4), %rdx
	movl	$255, %esi
	callq	memset
.LBB66_15:                              # %._crit_edge220
                                        #   in Loop: Header=BB66_8 Depth=3
	movq	16(%rsp), %r9           # 8-byte Reload
	leaq	(%r9,%r9,4), %r15
	shlq	$4, %r15
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r14,8), %rbp
	addq	%r15, %rbp
	movq	.LCPI66_0(%rip), %xmm2  # xmm2 = mem[0],zero
	movdqa	.LCPI66_1(%rip), %xmm3  # xmm3 = [0,1,2,3]
	movdqa	.LCPI66_2(%rip), %xmm4  # xmm4 = [4,5,6,7]
	je	.LBB66_34
	.p2align	4, 0x90
.LBB66_16:                              # %.lr.ph229
                                        #   Parent Loop BB66_5 Depth=1
                                        #     Parent Loop BB66_7 Depth=2
                                        #       Parent Loop BB66_8 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB66_29 Depth 5
                                        #           Child Loop BB66_32 Depth 5
	movq	40(%rbp), %xmm0         # xmm0 = mem[0],zero
	ucomisd	%xmm2, %xmm0
	jne	.LBB66_18
	jnp	.LBB66_17
.LBB66_18:                              #   in Loop: Header=BB66_16 Depth=4
	cmpl	$0, 52(%rbp)
	jns	.LBB66_34
.LBB66_19:                              #   in Loop: Header=BB66_16 Depth=4
	movl	24(%rbp), %ecx
	movl	28(%rbp), %r11d
	movl	$1, %eax
	subl	%ecx, %eax
	addl	%r11d, %eax
	je	.LBB66_33
# BB#20:                                # %.lr.ph225.preheader
                                        #   in Loop: Header=BB66_16 Depth=4
	movq	extendlocalhom2.ini(%rip), %r9
	movslq	32(%rbp), %r10
	leaq	(%r9,%r10,4), %rdx
	subl	%ecx, %r11d
	incq	%r11
	cmpq	$8, %r11
	jae	.LBB66_22
# BB#21:                                #   in Loop: Header=BB66_16 Depth=4
	movq	16(%rsp), %r9           # 8-byte Reload
	jmp	.LBB66_32
	.p2align	4, 0x90
.LBB66_22:                              # %min.iters.checked318
                                        #   in Loop: Header=BB66_16 Depth=4
	movq	%r11, %r8
	movabsq	$8589934584, %rsi       # imm = 0x1FFFFFFF8
	andq	%rsi, %r8
	movq	%r11, %r14
	andq	%rsi, %r14
	je	.LBB66_23
# BB#24:                                # %vector.body314.preheader
                                        #   in Loop: Header=BB66_16 Depth=4
	leaq	-8(%r14), %rbx
	movq	%rbx, %rdi
	shrq	$3, %rdi
	btl	$3, %ebx
	jb	.LBB66_25
# BB#26:                                # %vector.body314.prol
                                        #   in Loop: Header=BB66_16 Depth=4
	movd	%ecx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	%xmm0, %xmm1
	paddd	%xmm3, %xmm1
	paddd	%xmm4, %xmm0
	movdqu	%xmm1, (%rdx)
	movdqu	%xmm0, 16(%rdx)
	movl	$8, %ebx
	testq	%rdi, %rdi
	jne	.LBB66_28
	jmp	.LBB66_30
.LBB66_23:                              #   in Loop: Header=BB66_16 Depth=4
	movq	16(%rsp), %r9           # 8-byte Reload
	jmp	.LBB66_32
.LBB66_25:                              #   in Loop: Header=BB66_16 Depth=4
	xorl	%ebx, %ebx
	testq	%rdi, %rdi
	je	.LBB66_30
.LBB66_28:                              # %vector.body314.preheader.new
                                        #   in Loop: Header=BB66_16 Depth=4
	leaq	48(%r9,%r10,4), %rdi
	.p2align	4, 0x90
.LBB66_29:                              # %vector.body314
                                        #   Parent Loop BB66_5 Depth=1
                                        #     Parent Loop BB66_7 Depth=2
                                        #       Parent Loop BB66_8 Depth=3
                                        #         Parent Loop BB66_16 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	leal	(%rcx,%rbx), %esi
	movd	%esi, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	%xmm0, %xmm1
	paddd	%xmm3, %xmm1
	paddd	%xmm4, %xmm0
	movdqu	%xmm1, -48(%rdi,%rbx,4)
	movdqu	%xmm0, -32(%rdi,%rbx,4)
	leal	8(%rcx,%rbx), %esi
	movd	%esi, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	%xmm0, %xmm1
	paddd	%xmm3, %xmm1
	paddd	%xmm4, %xmm0
	movdqu	%xmm1, -16(%rdi,%rbx,4)
	movdqu	%xmm0, (%rdi,%rbx,4)
	addq	$16, %rbx
	cmpq	%rbx, %r14
	jne	.LBB66_29
.LBB66_30:                              # %middle.block315
                                        #   in Loop: Header=BB66_16 Depth=4
	cmpq	%r14, %r11
	movq	16(%rsp), %r9           # 8-byte Reload
	je	.LBB66_33
# BB#31:                                #   in Loop: Header=BB66_16 Depth=4
	subl	%r8d, %eax
	addl	%r8d, %ecx
	leaq	(%rdx,%r14,4), %rdx
	.p2align	4, 0x90
.LBB66_32:                              # %.lr.ph225
                                        #   Parent Loop BB66_5 Depth=1
                                        #     Parent Loop BB66_7 Depth=2
                                        #       Parent Loop BB66_8 Depth=3
                                        #         Parent Loop BB66_16 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movl	%ecx, (%rdx)
	leal	1(%rcx), %ecx
	decl	%eax
	leaq	4(%rdx), %rdx
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	jne	.LBB66_32
.LBB66_33:                              # %._crit_edge226
                                        #   in Loop: Header=BB66_16 Depth=4
	movq	8(%rbp), %rbp
	testq	%rbp, %rbp
	movq	64(%rsp), %r14          # 8-byte Reload
	jne	.LBB66_16
	jmp	.LBB66_34
.LBB66_17:                              #   in Loop: Header=BB66_16 Depth=4
	movq	stderr(%rip), %rdi
	movl	$.L.str.32, %esi
	movb	$1, %al
	movdqa	%xmm2, %xmm0
	callq	fprintf
	movq	16(%rsp), %r9           # 8-byte Reload
	movdqa	.LCPI66_2(%rip), %xmm4  # xmm4 = [4,5,6,7]
	movdqa	.LCPI66_1(%rip), %xmm3  # xmm3 = [0,1,2,3]
	movq	.LCPI66_0(%rip), %xmm2  # xmm2 = mem[0],zero
	cmpl	$0, 52(%rbp)
	js	.LBB66_19
.LBB66_34:                              # %._crit_edge230
                                        #   in Loop: Header=BB66_8 Depth=3
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	addq	(%rax,%rbp,8), %r15
	movl	%r13d, %r11d
	movl	12(%rsp), %r10d         # 4-byte Reload
	je	.LBB66_53
	.p2align	4, 0x90
.LBB66_35:                              # %.lr.ph240
                                        #   Parent Loop BB66_5 Depth=1
                                        #     Parent Loop BB66_7 Depth=2
                                        #       Parent Loop BB66_8 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB66_48 Depth 5
                                        #           Child Loop BB66_51 Depth 5
	movq	40(%r15), %xmm0         # xmm0 = mem[0],zero
	ucomisd	%xmm2, %xmm0
	jne	.LBB66_37
	jnp	.LBB66_36
.LBB66_37:                              #   in Loop: Header=BB66_35 Depth=4
	cmpl	$0, 52(%r15)
	jns	.LBB66_53
.LBB66_38:                              #   in Loop: Header=BB66_35 Depth=4
	movl	%r11d, %ebx
	movl	24(%r15), %ecx
	movl	28(%r15), %r11d
	movl	$1, %eax
	subl	%ecx, %eax
	addl	%r11d, %eax
	je	.LBB66_52
# BB#39:                                # %.lr.ph236.preheader
                                        #   in Loop: Header=BB66_35 Depth=4
	movq	extendlocalhom2.inj(%rip), %r9
	movslq	32(%r15), %rbp
	leaq	(%r9,%rbp,4), %rdx
	subl	%ecx, %r11d
	incq	%r11
	cmpq	$8, %r11
	jb	.LBB66_40
# BB#41:                                # %min.iters.checked
                                        #   in Loop: Header=BB66_35 Depth=4
	movq	%r11, %r8
	movabsq	$8589934584, %rsi       # imm = 0x1FFFFFFF8
	movq	%rsi, %rdi
	andq	%rdi, %r8
	movq	%r11, %rsi
	andq	%rdi, %rsi
	je	.LBB66_42
# BB#43:                                # %vector.body.preheader
                                        #   in Loop: Header=BB66_35 Depth=4
	leaq	-8(%rsi), %rbx
	movq	%rbx, %r10
	shrq	$3, %r10
	btl	$3, %ebx
	jb	.LBB66_44
# BB#45:                                # %vector.body.prol
                                        #   in Loop: Header=BB66_35 Depth=4
	movd	%ecx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	%xmm0, %xmm1
	paddd	%xmm3, %xmm1
	paddd	%xmm4, %xmm0
	movdqu	%xmm1, (%rdx)
	movdqu	%xmm0, 16(%rdx)
	movl	$8, %ebx
	testq	%r10, %r10
	jne	.LBB66_47
	jmp	.LBB66_49
.LBB66_42:                              #   in Loop: Header=BB66_35 Depth=4
	movl	12(%rsp), %r10d         # 4-byte Reload
.LBB66_40:                              #   in Loop: Header=BB66_35 Depth=4
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	.p2align	4, 0x90
.LBB66_51:                              # %.lr.ph236
                                        #   Parent Loop BB66_5 Depth=1
                                        #     Parent Loop BB66_7 Depth=2
                                        #       Parent Loop BB66_8 Depth=3
                                        #         Parent Loop BB66_35 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movl	%ecx, (%rdx)
	leal	1(%rcx), %ecx
	decl	%eax
	leaq	4(%rdx), %rdx
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	jne	.LBB66_51
.LBB66_52:                              # %._crit_edge237
                                        #   in Loop: Header=BB66_35 Depth=4
	movq	8(%r15), %r15
	testq	%r15, %r15
	movl	%ebx, %r11d
	jne	.LBB66_35
	jmp	.LBB66_53
.LBB66_44:                              #   in Loop: Header=BB66_35 Depth=4
	xorl	%ebx, %ebx
	testq	%r10, %r10
	je	.LBB66_49
.LBB66_47:                              # %vector.body.preheader.new
                                        #   in Loop: Header=BB66_35 Depth=4
	leaq	48(%r9,%rbp,4), %rbp
	.p2align	4, 0x90
.LBB66_48:                              # %vector.body
                                        #   Parent Loop BB66_5 Depth=1
                                        #     Parent Loop BB66_7 Depth=2
                                        #       Parent Loop BB66_8 Depth=3
                                        #         Parent Loop BB66_35 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	leal	(%rcx,%rbx), %edi
	movd	%edi, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	%xmm0, %xmm1
	paddd	%xmm3, %xmm1
	paddd	%xmm4, %xmm0
	movdqu	%xmm1, -48(%rbp,%rbx,4)
	movdqu	%xmm0, -32(%rbp,%rbx,4)
	leal	8(%rcx,%rbx), %edi
	movd	%edi, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	%xmm0, %xmm1
	paddd	%xmm3, %xmm1
	paddd	%xmm4, %xmm0
	movdqu	%xmm1, -16(%rbp,%rbx,4)
	movdqu	%xmm0, (%rbp,%rbx,4)
	addq	$16, %rbx
	cmpq	%rbx, %rsi
	jne	.LBB66_48
.LBB66_49:                              # %middle.block
                                        #   in Loop: Header=BB66_35 Depth=4
	cmpq	%rsi, %r11
	movl	%r13d, %ebx
	movl	12(%rsp), %r10d         # 4-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	je	.LBB66_52
# BB#50:                                #   in Loop: Header=BB66_35 Depth=4
	subl	%r8d, %eax
	addl	%r8d, %ecx
	leaq	(%rdx,%rsi,4), %rdx
	jmp	.LBB66_51
.LBB66_36:                              #   in Loop: Header=BB66_35 Depth=4
	movq	stderr(%rip), %rdi
	movl	$.L.str.32, %esi
	movb	$1, %al
	movdqa	%xmm2, %xmm0
	callq	fprintf
	movq	16(%rsp), %r9           # 8-byte Reload
	movl	12(%rsp), %r10d         # 4-byte Reload
	movl	%r13d, %r11d
	movdqa	.LCPI66_2(%rip), %xmm4  # xmm4 = [4,5,6,7]
	movdqa	.LCPI66_1(%rip), %xmm3  # xmm3 = [0,1,2,3]
	movq	.LCPI66_0(%rip), %xmm2  # xmm2 = mem[0],zero
	cmpl	$0, 52(%r15)
	js	.LBB66_38
.LBB66_53:                              # %._crit_edge241
                                        #   in Loop: Header=BB66_8 Depth=3
	movl	nlenmax(%rip), %eax
	testl	%eax, %eax
	js	.LBB66_84
# BB#54:                                # %.lr.ph246
                                        #   in Loop: Header=BB66_8 Depth=3
	movq	extendlocalhom2.ini(%rip), %rdx
	movq	extendlocalhom2.inj(%rip), %rsi
	leal	1(%rax), %ebx
	testb	$1, %bl
	jne	.LBB66_56
# BB#55:                                #   in Loop: Header=BB66_8 Depth=3
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	testl	%eax, %eax
	jne	.LBB66_61
	jmp	.LBB66_67
.LBB66_56:                              #   in Loop: Header=BB66_8 Depth=3
	cmpl	$-1, (%rdx)
	je	.LBB66_57
# BB#58:                                #   in Loop: Header=BB66_8 Depth=3
	xorl	%r8d, %r8d
	cmpl	$-1, (%rsi)
	setne	%r8b
	jmp	.LBB66_59
.LBB66_57:                              #   in Loop: Header=BB66_8 Depth=3
	xorl	%r8d, %r8d
.LBB66_59:                              # %.prol.loopexit
                                        #   in Loop: Header=BB66_8 Depth=3
	movl	$1, %edi
	testl	%eax, %eax
	je	.LBB66_67
.LBB66_61:                              # %.lr.ph246.new
                                        #   in Loop: Header=BB66_8 Depth=3
	movq	%rbx, %rcx
	subq	%rdi, %rcx
	leaq	4(%rdx,%rdi,4), %rdx
	leaq	4(%rsi,%rdi,4), %rsi
	.p2align	4, 0x90
.LBB66_62:                              #   Parent Loop BB66_5 Depth=1
                                        #     Parent Loop BB66_7 Depth=2
                                        #       Parent Loop BB66_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpl	$-1, -4(%rdx)
	je	.LBB66_64
# BB#63:                                #   in Loop: Header=BB66_62 Depth=4
	xorl	%edi, %edi
	cmpl	$-1, -4(%rsi)
	setne	%dil
	addl	%edi, %r8d
.LBB66_64:                              #   in Loop: Header=BB66_62 Depth=4
	cmpl	$-1, (%rdx)
	je	.LBB66_66
# BB#65:                                #   in Loop: Header=BB66_62 Depth=4
	xorl	%edi, %edi
	cmpl	$-1, (%rsi)
	setne	%dil
	addl	%edi, %r8d
.LBB66_66:                              #   in Loop: Header=BB66_62 Depth=4
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB66_62
.LBB66_67:                              # %._crit_edge247
                                        #   in Loop: Header=BB66_8 Depth=3
	testl	%eax, %eax
	js	.LBB66_68
# BB#69:                                # %.lr.ph255
                                        #   in Loop: Header=BB66_8 Depth=3
	movq	extendlocalhom2.ini(%rip), %r13
	movq	extendlocalhom2.inj(%rip), %r15
	addq	$-4, %r13
	addq	$-4, %r15
	xorl	%eax, %eax
	movl	12(%rsp), %r10d         # 4-byte Reload
	movl	%r8d, 44(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB66_70:                              #   Parent Loop BB66_5 Depth=1
                                        #     Parent Loop BB66_7 Depth=2
                                        #       Parent Loop BB66_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	testl	%eax, %eax
	je	.LBB66_80
# BB#71:                                #   in Loop: Header=BB66_70 Depth=4
	movl	4(%r13), %edx
	cmpl	$-1, %edx
	je	.LBB66_75
# BB#72:                                #   in Loop: Header=BB66_70 Depth=4
	movl	4(%r15), %ecx
	cmpl	$-1, %ecx
	je	.LBB66_75
# BB#73:                                #   in Loop: Header=BB66_70 Depth=4
	decl	%edx
	cmpl	%edx, (%r13)
	jne	.LBB66_75
# BB#74:                                #   in Loop: Header=BB66_70 Depth=4
	decl	%ecx
	cmpl	%ecx, (%r15)
	je	.LBB66_83
	.p2align	4, 0x90
.LBB66_75:                              #   in Loop: Header=BB66_70 Depth=4
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx,%r14,8), %rax
	leaq	(%rbp,%rbp,4), %rcx
	shlq	$4, %rcx
	leaq	(%rax,%rcx), %r14
	leaq	16(%rax,%rcx), %rsi
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movq	16(%rax,%rcx), %rbx
	movl	(%r13), %esi
	movq	%rbp, %rdi
	movl	(%r15), %ebp
	movq	%r9, %rcx
	shlq	$4, %rcx
	leaq	(%rcx,%rcx,4), %r12
	movq	(%rdx,%rdi,8), %rcx
	movsd	40(%rcx,%r12), %xmm0    # xmm0 = mem[0],zero
	ucomisd	40(%rax,%r12), %xmm0
	cmovaq	%rax, %rcx
	movq	40(%rcx,%r12), %rdx
	cmpq	%r14, %rbx
	movq	%r14, %rax
	movl	%r10d, 12(%rsp)         # 4-byte Spill
	je	.LBB66_77
# BB#76:                                #   in Loop: Header=BB66_70 Depth=4
	movl	$1, %edi
	movl	%esi, 28(%rsp)          # 4-byte Spill
	movl	$80, %esi
	movl	%r11d, 24(%rsp)         # 4-byte Spill
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	callq	calloc
	movq	120(%rsp), %rdx         # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	44(%rsp), %r8d          # 4-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	movl	12(%rsp), %r10d         # 4-byte Reload
	movl	24(%rsp), %r11d         # 4-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rax, 8(%rbx)
.LBB66_77:                              # %addlocalhom2_e.exit
                                        #   in Loop: Header=BB66_70 Depth=4
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	incl	(%r14)
	movl	%r10d, 24(%rax)
	movl	%r11d, 32(%rax)
	movl	%esi, 28(%rax)
	movl	%ebp, 36(%rax)
	movq	%rdx, 40(%rax)
	movl	%r9d, 52(%rax)
	movl	%r8d, 48(%rax)
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx,%rdi,8), %rax
	movq	64(%rsp), %r14          # 8-byte Reload
	leaq	(%r14,%r14,4), %rcx
	shlq	$4, %rcx
	leaq	(%rax,%rcx), %rbp
	leaq	16(%rax,%rcx), %rdi
	movq	16(%rax,%rcx), %rbx
	movl	(%r15), %esi
	movq	(%rdx,%r14,8), %rcx
	movsd	40(%rax,%r12), %xmm0    # xmm0 = mem[0],zero
	ucomisd	40(%rcx,%r12), %xmm0
	cmovaq	%rcx, %rax
	movl	(%r13), %edx
	movq	40(%rax,%r12), %rcx
	cmpq	%rbp, %rbx
	movq	%rbp, %rax
	je	.LBB66_79
# BB#78:                                #   in Loop: Header=BB66_70 Depth=4
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movl	$1, %edi
	movl	%esi, 28(%rsp)          # 4-byte Spill
	movl	$80, %esi
	movl	%r11d, %r12d
	movq	%rcx, %r14
	movl	%edx, 24(%rsp)          # 4-byte Spill
	callq	calloc
	movl	24(%rsp), %edx          # 4-byte Reload
	movq	%r14, %rcx
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	44(%rsp), %r8d          # 4-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	movl	12(%rsp), %r10d         # 4-byte Reload
	movq	64(%rsp), %r14          # 8-byte Reload
	movl	%r12d, %r11d
	movq	%rax, 8(%rbx)
.LBB66_79:                              # %addlocalhom2_e.exit208
                                        #   in Loop: Header=BB66_70 Depth=4
	movq	%rax, (%rdi)
	incl	(%rbp)
	movl	%r11d, 24(%rax)
	movl	%r10d, 32(%rax)
	movl	%esi, 28(%rax)
	movl	%edx, 36(%rax)
	movq	%rcx, 40(%rax)
	movl	%r9d, 52(%rax)
	movl	%r8d, 48(%rax)
	movq	104(%rsp), %r12         # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	128(%rsp), %rbx         # 8-byte Reload
.LBB66_80:                              # %._crit_edge297
                                        #   in Loop: Header=BB66_70 Depth=4
	movl	4(%r13), %ecx
	cmpl	$-1, %ecx
	je	.LBB66_81
# BB#82:                                #   in Loop: Header=BB66_70 Depth=4
	movl	4(%r15), %edx
	xorl	%eax, %eax
	cmpl	$-1, %edx
	setne	%al
	cmovnel	%ecx, %r10d
	cmovnel	%edx, %r11d
	jmp	.LBB66_83
	.p2align	4, 0x90
.LBB66_81:                              #   in Loop: Header=BB66_70 Depth=4
	xorl	%eax, %eax
.LBB66_83:                              #   in Loop: Header=BB66_70 Depth=4
	addq	$4, %r13
	addq	$4, %r15
	decq	%rbx
	jne	.LBB66_70
	jmp	.LBB66_84
.LBB66_68:                              #   in Loop: Header=BB66_8 Depth=3
	movl	12(%rsp), %r10d         # 4-byte Reload
	.p2align	4, 0x90
.LBB66_84:                              # %.loopexit
                                        #   in Loop: Header=BB66_8 Depth=3
	incq	%r9
	cmpq	80(%rsp), %r9           # 8-byte Folded Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	jne	.LBB66_8
# BB#85:                                # %._crit_edge264
                                        #   in Loop: Header=BB66_7 Depth=2
	incq	%rbp
	cmpq	80(%rsp), %rbp          # 8-byte Folded Reload
	jne	.LBB66_7
.LBB66_4:                               # %.loopexit212
                                        #   in Loop: Header=BB66_5 Depth=1
	incq	72(%rsp)                # 8-byte Folded Spill
	cmpq	96(%rsp), %rcx          # 8-byte Folded Reload
	jne	.LBB66_5
.LBB66_86:                              # %._crit_edge277
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end66:
	.size	extendlocalhom2, .Lfunc_end66-extendlocalhom2
	.cfi_endproc

	.globl	makelocal
	.p2align	4, 0x90
	.type	makelocal,@function
makelocal:                              # @makelocal
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi615:
	.cfi_def_cfa_offset 16
.Lcfi616:
	.cfi_offset %rbx, -16
	xorps	%xmm0, %xmm0
	xorl	%r9d, %r9d
	cvtsi2sdl	penalty(%rip), %xmm1
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	movq	%rdi, %r11
	movq	%rsi, %r10
	xorpd	%xmm4, %xmm4
	jmp	.LBB67_1
.LBB67_11:                              #   in Loop: Header=BB67_1 Depth=1
	movl	%r11d, %ecx
	subl	%edi, %ecx
	cmpl	%eax, %r9d
	cmovel	%ecx, %r8d
	movl	%ecx, %r9d
	.p2align	4, 0x90
.LBB67_1:                               # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB67_2 Depth 2
                                        #       Child Loop BB67_7 Depth 3
	xorpd	%xmm2, %xmm2
	movapd	%xmm4, %xmm3
.LBB67_2:                               # %.outer56
                                        #   Parent Loop BB67_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB67_7 Depth 3
	movb	(%r11), %cl
	cmpb	$45, %cl
	jne	.LBB67_5
	jmp	.LBB67_7
	.p2align	4, 0x90
.LBB67_3:                               # %.loopexit.loopexit
                                        #   in Loop: Header=BB67_7 Depth=3
	addsd	%xmm1, %xmm2
	cmpb	$45, %cl
	je	.LBB67_7
.LBB67_5:                               # %.loopexit
                                        #   in Loop: Header=BB67_2 Depth=2
	testb	%cl, %cl
	je	.LBB67_12
# BB#6:                                 #   in Loop: Header=BB67_2 Depth=2
	movsbq	(%r10), %rbx
	cmpq	$45, %rbx
	je	.LBB67_7
	jmp	.LBB67_10
	.p2align	4, 0x90
.LBB67_9:                               # %.critedge
                                        #   in Loop: Header=BB67_7 Depth=3
	incq	%r11
	incq	%r10
.LBB67_7:                               # %.preheader
                                        #   Parent Loop BB67_1 Depth=1
                                        #     Parent Loop BB67_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%r11), %ecx
	cmpb	$45, %cl
	je	.LBB67_9
# BB#8:                                 #   in Loop: Header=BB67_7 Depth=3
	cmpb	$45, (%r10)
	je	.LBB67_9
	jmp	.LBB67_3
	.p2align	4, 0x90
.LBB67_10:                              #   in Loop: Header=BB67_2 Depth=2
	incq	%r11
	movsbq	%cl, %rcx
	incq	%r10
	shlq	$9, %rcx
	movl	amino_dis(%rcx,%rbx,4), %ecx
	subl	%edx, %ecx
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%ecx, %xmm4
	addsd	%xmm4, %xmm2
	ucomisd	%xmm3, %xmm2
	cmoval	%r9d, %eax
	movapd	%xmm2, %xmm4
	maxsd	%xmm3, %xmm4
	ucomisd	%xmm2, %xmm0
	movapd	%xmm4, %xmm3
	jbe	.LBB67_2
	jmp	.LBB67_11
.LBB67_12:
	movl	%edi, %ecx
	notl	%ecx
	addl	%ecx, %r11d
	cmpl	%eax, %r9d
	cmovel	%r11d, %r8d
	movslq	%r8d, %rcx
	movb	$0, 1(%rdi,%rcx)
	movb	$0, 1(%rsi,%rcx)
	popq	%rbx
	retq
.Lfunc_end67:
	.size	makelocal, .Lfunc_end67-makelocal
	.cfi_endproc

	.globl	resetlocalhom
	.p2align	4, 0x90
	.type	resetlocalhom,@function
resetlocalhom:                          # @resetlocalhom
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi617:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi618:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi619:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi620:
	.cfi_def_cfa_offset 40
.Lcfi621:
	.cfi_offset %rbx, -40
.Lcfi622:
	.cfi_offset %r12, -32
.Lcfi623:
	.cfi_offset %r14, -24
.Lcfi624:
	.cfi_offset %r15, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	cmpl	$2, %edi
	jl	.LBB68_14
# BB#1:                                 # %.lr.ph25.preheader
	leal	-1(%rdi), %r10d
	movslq	%edi, %r11
	movl	%edi, %r12d
	leaq	1(%r12), %r9
	leaq	-2(%r12), %r8
	movl	$1, %r15d
	xorl	%r14d, %r14d
	movabsq	$4607182418800017408, %rcx # imm = 0x3FF0000000000000
	.p2align	4, 0x90
.LBB68_3:                               # %.lr.ph25
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB68_6 Depth 2
                                        #     Child Loop BB68_9 Depth 2
                                        #       Child Loop BB68_10 Depth 3
                                        #       Child Loop BB68_12 Depth 3
	movq	%r14, %rax
	leaq	1(%rax), %r14
	cmpq	%r11, %r14
	jge	.LBB68_2
# BB#4:                                 # %.lr.ph22
                                        #   in Loop: Header=BB68_3 Depth=1
	movl	%r9d, %edi
	subl	%eax, %edi
	movq	(%rsi,%rax,8), %rdx
	testb	$1, %dil
	movq	%r15, %rdi
	je	.LBB68_8
# BB#5:                                 # %.lr.ph.preheader.prol
                                        #   in Loop: Header=BB68_3 Depth=1
	leaq	(%r15,%r15,4), %rdi
	shlq	$4, %rdi
	addq	%rdx, %rdi
	.p2align	4, 0x90
.LBB68_6:                               # %.lr.ph.prol
                                        #   Parent Loop BB68_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, 40(%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB68_6
# BB#7:                                 # %._crit_edge.prol
                                        #   in Loop: Header=BB68_3 Depth=1
	leaq	1(%r15), %rdi
.LBB68_8:                               # %.prol.loopexit
                                        #   in Loop: Header=BB68_3 Depth=1
	cmpq	%rax, %r8
	je	.LBB68_2
	.p2align	4, 0x90
.LBB68_9:                               #   Parent Loop BB68_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB68_10 Depth 3
                                        #       Child Loop BB68_12 Depth 3
	leaq	(%rdi,%rdi,4), %rax
	shlq	$4, %rax
	movq	%rdx, %rbx
	addq	%rax, %rbx
	je	.LBB68_11
	.p2align	4, 0x90
.LBB68_10:                              # %.lr.ph
                                        #   Parent Loop BB68_3 Depth=1
                                        #     Parent Loop BB68_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rcx, 40(%rbx)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB68_10
.LBB68_11:                              # %._crit_edge
                                        #   in Loop: Header=BB68_9 Depth=2
	addq	%rdx, %rax
	addq	$80, %rax
	je	.LBB68_13
	.p2align	4, 0x90
.LBB68_12:                              # %.lr.ph.1
                                        #   Parent Loop BB68_3 Depth=1
                                        #     Parent Loop BB68_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rcx, 40(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.LBB68_12
.LBB68_13:                              # %._crit_edge.1
                                        #   in Loop: Header=BB68_9 Depth=2
	addq	$2, %rdi
	cmpq	%r12, %rdi
	jne	.LBB68_9
.LBB68_2:                               # %.loopexit
                                        #   in Loop: Header=BB68_3 Depth=1
	incq	%r15
	cmpq	%r10, %r14
	jne	.LBB68_3
.LBB68_14:                              # %._crit_edge26
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end68:
	.size	resetlocalhom, .Lfunc_end68-resetlocalhom
	.cfi_endproc

	.globl	gapireru
	.p2align	4, 0x90
	.type	gapireru,@function
gapireru:                               # @gapireru
	.cfi_startproc
# BB#0:
	jmp	.LBB69_1
	.p2align	4, 0x90
.LBB69_5:                               #   in Loop: Header=BB69_1 Depth=1
	movb	$45, -1(%rdi)
.LBB69_1:                               # %.outer
                                        # =>This Inner Loop Header: Depth=1
	incq	%rdx
	incq	%rdi
	movzbl	-1(%rdx), %eax
	cmpb	$45, %al
	je	.LBB69_5
# BB#2:                                 #   in Loop: Header=BB69_1 Depth=1
	testb	%al, %al
	je	.LBB69_4
# BB#3:                                 #   in Loop: Header=BB69_1 Depth=1
	movzbl	(%rsi), %eax
	incq	%rsi
	movb	%al, -1(%rdi)
	jmp	.LBB69_1
.LBB69_4:
	movb	$0, -1(%rdi)
	retq
.Lfunc_end69:
	.size	gapireru, .Lfunc_end69-gapireru
	.cfi_endproc

	.globl	getkyokaigap
	.p2align	4, 0x90
	.type	getkyokaigap,@function
getkyokaigap:                           # @getkyokaigap
	.cfi_startproc
# BB#0:
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	testl	%ecx, %ecx
	je	.LBB70_6
# BB#1:                                 # %.lr.ph
	movslq	%edx, %r9
	leal	-1(%rcx), %r8d
	movl	%ecx, %edx
	andl	$3, %edx
	je	.LBB70_4
# BB#2:                                 # %.prol.preheader
	negl	%edx
	.p2align	4, 0x90
.LBB70_3:                               # =>This Inner Loop Header: Depth=1
	decl	%ecx
	movq	(%rsi), %rax
	addq	$8, %rsi
	movzbl	(%rax,%r9), %eax
	movb	%al, (%rdi)
	incq	%rdi
	incl	%edx
	jne	.LBB70_3
.LBB70_4:                               # %.prol.loopexit
	cmpl	$3, %r8d
	jb	.LBB70_6
	.p2align	4, 0x90
.LBB70_5:                               # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rax
	movzbl	(%rax,%r9), %eax
	movb	%al, (%rdi)
	movq	8(%rsi), %rax
	movzbl	(%rax,%r9), %eax
	movb	%al, 1(%rdi)
	movq	16(%rsi), %rax
	movzbl	(%rax,%r9), %eax
	movb	%al, 2(%rdi)
	addl	$-4, %ecx
	movq	24(%rsi), %rax
	leaq	32(%rsi), %rsi
	movzbl	(%rax,%r9), %eax
	movb	%al, 3(%rdi)
	leaq	4(%rdi), %rdi
	jne	.LBB70_5
.LBB70_6:                               # %._crit_edge
	retq
.Lfunc_end70:
	.size	getkyokaigap, .Lfunc_end70-getkyokaigap
	.cfi_endproc

	.globl	new_OpeningGapCount
	.p2align	4, 0x90
	.type	new_OpeningGapCount,@function
new_OpeningGapCount:                    # @new_OpeningGapCount
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi625:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi626:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi627:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi628:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi629:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi630:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi631:
	.cfi_def_cfa_offset 64
.Lcfi632:
	.cfi_offset %rbx, -56
.Lcfi633:
	.cfi_offset %r12, -48
.Lcfi634:
	.cfi_offset %r13, -40
.Lcfi635:
	.cfi_offset %r14, -32
.Lcfi636:
	.cfi_offset %r15, -24
.Lcfi637:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movl	%r8d, %ebx
	movq	%rcx, %r15
	movq	%rdx, %r12
	movl	%esi, %ebp
	movq	%rdi, %r13
	testl	%ebx, %ebx
	je	.LBB71_2
# BB#1:                                 # %.lr.ph49.preheader
	leal	-1(%rbx), %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	memset
.LBB71_2:                               # %.preheader
	testl	%ebp, %ebp
	jle	.LBB71_18
# BB#3:                                 # %.preheader
	testl	%ebx, %ebx
	je	.LBB71_18
# BB#4:                                 # %.lr.ph44.split.preheader
	movl	%ebp, %r10d
	movl	%ebx, %r11d
	andl	$1, %r11d
	leal	-1(%rbx), %r8d
	leaq	4(%r13), %r9
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB71_5:                               # %.lr.ph44.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB71_12 Depth 2
	xorl	%ecx, %ecx
	cmpb	$45, (%r14,%rdi)
	sete	%cl
	testl	%r11d, %r11d
	movsd	(%r15,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	(%r12,%rdi,8), %rbp
	jne	.LBB71_7
# BB#6:                                 #   in Loop: Header=BB71_5 Depth=1
	movl	%ebx, %esi
	movq	%r13, %rax
	movl	%ecx, %edx
	cmpl	$1, %ebx
	jne	.LBB71_11
	jmp	.LBB71_17
	.p2align	4, 0x90
.LBB71_7:                               #   in Loop: Header=BB71_5 Depth=1
	xorl	%edx, %edx
	cmpb	$45, (%rbp)
	leaq	1(%rbp), %rbp
	sete	%dl
	orl	$-2, %ecx
	incl	%ecx
	testl	%ecx, %edx
	je	.LBB71_9
# BB#8:                                 #   in Loop: Header=BB71_5 Depth=1
	movss	(%r13), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r13)
.LBB71_9:                               # %.prol.loopexit
                                        #   in Loop: Header=BB71_5 Depth=1
	movl	%r8d, %esi
	movq	%r9, %rax
	cmpl	$1, %ebx
	je	.LBB71_17
.LBB71_11:                              # %.lr.ph44.split.new
                                        #   in Loop: Header=BB71_5 Depth=1
	addq	$4, %rax
	.p2align	4, 0x90
.LBB71_12:                              #   Parent Loop BB71_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%ecx, %ecx
	cmpb	$45, (%rbp)
	sete	%cl
	xorl	$-2, %edx
	incl	%edx
	testl	%edx, %ecx
	je	.LBB71_14
# BB#13:                                #   in Loop: Header=BB71_12 Depth=2
	movss	-4(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%rax)
.LBB71_14:                              #   in Loop: Header=BB71_12 Depth=2
	addl	$-2, %esi
	xorl	%edx, %edx
	cmpb	$45, 1(%rbp)
	sete	%dl
	orl	$-2, %ecx
	incl	%ecx
	testl	%ecx, %edx
	je	.LBB71_16
# BB#15:                                #   in Loop: Header=BB71_12 Depth=2
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rax)
.LBB71_16:                              #   in Loop: Header=BB71_12 Depth=2
	addq	$8, %rax
	addq	$2, %rbp
	testl	%esi, %esi
	jne	.LBB71_12
.LBB71_17:                              # %._crit_edge
                                        #   in Loop: Header=BB71_5 Depth=1
	incq	%rdi
	cmpq	%r10, %rdi
	jne	.LBB71_5
.LBB71_18:                              # %._crit_edge45
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end71:
	.size	new_OpeningGapCount, .Lfunc_end71-new_OpeningGapCount
	.cfi_endproc

	.globl	new_OpeningGapCount_zure
	.p2align	4, 0x90
	.type	new_OpeningGapCount_zure,@function
new_OpeningGapCount_zure:               # @new_OpeningGapCount_zure
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi638:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi639:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi640:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi641:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi642:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi643:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi644:
	.cfi_def_cfa_offset 64
.Lcfi645:
	.cfi_offset %rbx, -56
.Lcfi646:
	.cfi_offset %r12, -48
.Lcfi647:
	.cfi_offset %r13, -40
.Lcfi648:
	.cfi_offset %r14, -32
.Lcfi649:
	.cfi_offset %r15, -24
.Lcfi650:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%rcx, %r15
	movq	%rdx, %r13
	movl	%esi, %ebp
	movq	%rdi, %r12
	movq	%r8, (%rsp)             # 8-byte Spill
	cmpl	$-2, %r8d
	je	.LBB72_2
# BB#1:                                 # %.lr.ph56.preheader
	movq	(%rsp), %rax            # 8-byte Reload
	leal	1(%rax), %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	memset
.LBB72_2:                               # %.preheader
	testl	%ebp, %ebp
	jle	.LBB72_34
# BB#3:                                 # %.lr.ph51
	movq	64(%rsp), %r8
	movq	(%rsp), %rax            # 8-byte Reload
	testl	%eax, %eax
	je	.LBB72_4
# BB#18:                                # %.lr.ph51.split.preheader
	leal	-1(%rax), %ecx
	movl	%ebp, %r10d
	movl	%eax, %r11d
	andl	$1, %r11d
	leaq	4(%r12), %r9
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB72_19:                              # %.lr.ph51.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB72_26 Depth 2
	xorl	%esi, %esi
	cmpb	$45, (%r14,%rbp)
	sete	%sil
	testl	%r11d, %r11d
	movsd	(%r15,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	(%r13,%rbp,8), %rax
	jne	.LBB72_21
# BB#20:                                #   in Loop: Header=BB72_19 Depth=1
	movq	(%rsp), %rdx            # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	%r12, %rbx
	movl	%esi, %edi
	testl	%ecx, %ecx
	jne	.LBB72_25
	jmp	.LBB72_31
	.p2align	4, 0x90
.LBB72_21:                              #   in Loop: Header=BB72_19 Depth=1
	xorl	%edi, %edi
	cmpb	$45, (%rax)
	leaq	1(%rax), %rax
	sete	%dil
	orl	$-2, %esi
	incl	%esi
	testl	%esi, %edi
	je	.LBB72_23
# BB#22:                                #   in Loop: Header=BB72_19 Depth=1
	movss	(%r12), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r12)
.LBB72_23:                              # %.prol.loopexit
                                        #   in Loop: Header=BB72_19 Depth=1
	movl	%ecx, %edx
	movq	%r9, %rbx
	testl	%ecx, %ecx
	je	.LBB72_31
.LBB72_25:                              # %.lr.ph51.split.new
                                        #   in Loop: Header=BB72_19 Depth=1
	addq	$4, %rbx
	.p2align	4, 0x90
.LBB72_26:                              #   Parent Loop BB72_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%esi, %esi
	cmpb	$45, (%rax)
	sete	%sil
	xorl	$-2, %edi
	incl	%edi
	testl	%edi, %esi
	je	.LBB72_28
# BB#27:                                #   in Loop: Header=BB72_26 Depth=2
	movss	-4(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%rbx)
.LBB72_28:                              #   in Loop: Header=BB72_26 Depth=2
	addl	$-2, %edx
	xorl	%edi, %edi
	cmpb	$45, 1(%rax)
	sete	%dil
	orl	$-2, %esi
	incl	%esi
	testl	%esi, %edi
	je	.LBB72_30
# BB#29:                                #   in Loop: Header=BB72_26 Depth=2
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rbx)
.LBB72_30:                              #   in Loop: Header=BB72_26 Depth=2
	addq	$8, %rbx
	addq	$2, %rax
	testl	%edx, %edx
	jne	.LBB72_26
.LBB72_31:                              # %._crit_edge
                                        #   in Loop: Header=BB72_19 Depth=1
	xorl	%eax, %eax
	cmpb	$45, (%r8,%rbp)
	sete	%al
	xorl	$-2, %edi
	incl	%edi
	testl	%edi, %eax
	je	.LBB72_33
# BB#32:                                #   in Loop: Header=BB72_19 Depth=1
	addss	4(%r12,%rcx,4), %xmm0
	movss	%xmm0, 4(%r12,%rcx,4)
.LBB72_33:                              #   in Loop: Header=BB72_19 Depth=1
	incq	%rbp
	cmpq	%r10, %rbp
	jne	.LBB72_19
	jmp	.LBB72_34
.LBB72_4:                               # %.lr.ph51.split.us.preheader
	movl	%ebp, %eax
	testb	$1, %al
	jne	.LBB72_6
# BB#5:
	xorl	%esi, %esi
	cmpl	$1, %ebp
	jne	.LBB72_10
	jmp	.LBB72_34
.LBB72_6:                               # %.lr.ph51.split.us.prol
	movl	$1, %esi
	cmpb	$45, (%r14)
	je	.LBB72_9
# BB#7:                                 # %.lr.ph51.split.us.prol
	cmpb	$45, (%r8)
	jne	.LBB72_9
# BB#8:
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	addss	(%r12), %xmm0
	movss	%xmm0, (%r12)
	movl	$1, %esi
.LBB72_9:                               # %.lr.ph51.split.us.prol.loopexit
	cmpl	$1, %ebp
	je	.LBB72_34
.LBB72_10:                              # %.lr.ph51.split.us.preheader.new
	subq	%rsi, %rax
	leaq	8(%r15,%rsi,8), %rcx
	leaq	1(%r14,%rsi), %rdx
	leaq	1(%r8,%rsi), %rsi
	.p2align	4, 0x90
.LBB72_11:                              # %.lr.ph51.split.us
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$45, -1(%rdx)
	je	.LBB72_14
# BB#12:                                # %.lr.ph51.split.us
                                        #   in Loop: Header=BB72_11 Depth=1
	cmpb	$45, -1(%rsi)
	jne	.LBB72_14
# BB#13:                                #   in Loop: Header=BB72_11 Depth=1
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	addss	(%r12), %xmm0
	movss	%xmm0, (%r12)
.LBB72_14:                              # %.lr.ph51.split.us.166
                                        #   in Loop: Header=BB72_11 Depth=1
	cmpb	$45, (%rdx)
	je	.LBB72_17
# BB#15:                                # %.lr.ph51.split.us.166
                                        #   in Loop: Header=BB72_11 Depth=1
	cmpb	$45, (%rsi)
	jne	.LBB72_17
# BB#16:                                #   in Loop: Header=BB72_11 Depth=1
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	addss	(%r12), %xmm0
	movss	%xmm0, (%r12)
.LBB72_17:                              #   in Loop: Header=BB72_11 Depth=1
	addq	$16, %rcx
	addq	$2, %rdx
	addq	$2, %rsi
	addq	$-2, %rax
	jne	.LBB72_11
.LBB72_34:                              # %._crit_edge52
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end72:
	.size	new_OpeningGapCount_zure, .Lfunc_end72-new_OpeningGapCount_zure
	.cfi_endproc

	.globl	new_FinalGapCount_zure
	.p2align	4, 0x90
	.type	new_FinalGapCount_zure,@function
new_FinalGapCount_zure:                 # @new_FinalGapCount_zure
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi651:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi652:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi653:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi654:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi655:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi656:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi657:
	.cfi_def_cfa_offset 80
.Lcfi658:
	.cfi_offset %rbx, -56
.Lcfi659:
	.cfi_offset %r12, -48
.Lcfi660:
	.cfi_offset %r13, -40
.Lcfi661:
	.cfi_offset %r14, -32
.Lcfi662:
	.cfi_offset %r15, -24
.Lcfi663:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%rcx, %r15
	movq	%rdx, %r13
	movl	%esi, %ebp
	movq	%rdi, %r12
	movq	%r8, 16(%rsp)           # 8-byte Spill
	cmpl	$-2, %r8d
	je	.LBB73_2
# BB#1:                                 # %.lr.ph56.preheader
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	1(%rax), %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	memset
.LBB73_2:                               # %.preheader
	testl	%ebp, %ebp
	jle	.LBB73_37
# BB#3:                                 # %.lr.ph51
	movq	16(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	je	.LBB73_4
# BB#18:                                # %.lr.ph51.split.preheader
	leal	-1(%rax), %r9d
	movl	%ebp, %r10d
	movl	%eax, %r11d
	andl	$1, %r11d
	leaq	4(%r12), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB73_19:                              # %.lr.ph51.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB73_27 Depth 2
	movb	(%r14,%rbp), %cl
	xorl	%edi, %edi
	cmpb	$45, %cl
	sete	%dl
	testl	%r11d, %r11d
	movsd	(%r15,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	(%r13,%rbp,8), %rax
	jne	.LBB73_21
# BB#20:                                #   in Loop: Header=BB73_19 Depth=1
	movb	%dl, %dil
                                        # implicit-def: %CL
	movq	16(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	%r12, %rbx
	testl	%r9d, %r9d
	jne	.LBB73_26
	jmp	.LBB73_33
	.p2align	4, 0x90
.LBB73_21:                              #   in Loop: Header=BB73_19 Depth=1
	cmpb	$45, %cl
	setne	%dl
	xorl	%edi, %edi
	cmpb	$45, (%rax)
	leaq	1(%rax), %rax
	sete	%cl
	movb	%cl, %dil
	je	.LBB73_24
# BB#22:                                #   in Loop: Header=BB73_19 Depth=1
	testb	%dl, %dl
	jne	.LBB73_24
# BB#23:                                #   in Loop: Header=BB73_19 Depth=1
	movss	(%r12), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r12)
.LBB73_24:                              # %.prol.loopexit
                                        #   in Loop: Header=BB73_19 Depth=1
	movl	%r9d, %edx
	movq	8(%rsp), %rbx           # 8-byte Reload
	testl	%r9d, %r9d
	je	.LBB73_33
.LBB73_26:                              # %.lr.ph51.split.new
                                        #   in Loop: Header=BB73_19 Depth=1
	addq	$4, %rbx
	.p2align	4, 0x90
.LBB73_27:                              #   Parent Loop BB73_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ecx
	xorl	%esi, %esi
	cmpb	$45, %cl
	setne	%sil
	negl	%edi
	testl	%edi, %esi
	je	.LBB73_29
# BB#28:                                #   in Loop: Header=BB73_27 Depth=2
	movss	-4(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%rbx)
.LBB73_29:                              #   in Loop: Header=BB73_27 Depth=2
	addl	$-2, %edx
	cmpb	$45, %cl
	setne	%r8b
	xorl	%edi, %edi
	cmpb	$45, 1(%rax)
	sete	%cl
	je	.LBB73_32
# BB#30:                                #   in Loop: Header=BB73_27 Depth=2
	testb	%r8b, %r8b
	jne	.LBB73_32
# BB#31:                                #   in Loop: Header=BB73_27 Depth=2
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rbx)
.LBB73_32:                              #   in Loop: Header=BB73_27 Depth=2
	movb	%cl, %dil
	addq	$8, %rbx
	addq	$2, %rax
	testl	%edx, %edx
	jne	.LBB73_27
.LBB73_33:                              # %._crit_edge
                                        #   in Loop: Header=BB73_19 Depth=1
	testb	$1, %cl
	je	.LBB73_36
# BB#34:                                # %._crit_edge
                                        #   in Loop: Header=BB73_19 Depth=1
	movq	80(%rsp), %rax
	cmpb	$45, (%rax,%rbp)
	je	.LBB73_36
# BB#35:                                #   in Loop: Header=BB73_19 Depth=1
	addss	4(%r12,%r9,4), %xmm0
	movss	%xmm0, 4(%r12,%r9,4)
.LBB73_36:                              #   in Loop: Header=BB73_19 Depth=1
	incq	%rbp
	cmpq	%r10, %rbp
	jne	.LBB73_19
	jmp	.LBB73_37
.LBB73_4:                               # %.lr.ph51.split.us.preheader
	movl	%ebp, %eax
	testb	$1, %al
	jne	.LBB73_6
# BB#5:
	xorl	%esi, %esi
	cmpl	$1, %ebp
	jne	.LBB73_10
	jmp	.LBB73_37
.LBB73_6:                               # %.lr.ph51.split.us.prol
	movl	$1, %esi
	cmpb	$45, (%r14)
	jne	.LBB73_9
# BB#7:                                 # %.lr.ph51.split.us.prol
	movq	80(%rsp), %rcx
	cmpb	$45, (%rcx)
	je	.LBB73_9
# BB#8:
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	addss	(%r12), %xmm0
	movss	%xmm0, (%r12)
	movl	$1, %esi
.LBB73_9:                               # %.lr.ph51.split.us.prol.loopexit
	cmpl	$1, %ebp
	je	.LBB73_37
.LBB73_10:                              # %.lr.ph51.split.us.preheader.new
	subq	%rsi, %rax
	leaq	8(%r15,%rsi,8), %rcx
	leaq	1(%r14,%rsi), %rdx
	movq	80(%rsp), %rdi
	leaq	1(%rdi,%rsi), %rsi
	.p2align	4, 0x90
.LBB73_11:                              # %.lr.ph51.split.us
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$45, -1(%rdx)
	jne	.LBB73_14
# BB#12:                                # %.lr.ph51.split.us
                                        #   in Loop: Header=BB73_11 Depth=1
	cmpb	$45, -1(%rsi)
	je	.LBB73_14
# BB#13:                                #   in Loop: Header=BB73_11 Depth=1
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	addss	(%r12), %xmm0
	movss	%xmm0, (%r12)
.LBB73_14:                              # %.lr.ph51.split.us.167
                                        #   in Loop: Header=BB73_11 Depth=1
	cmpb	$45, (%rdx)
	jne	.LBB73_17
# BB#15:                                # %.lr.ph51.split.us.167
                                        #   in Loop: Header=BB73_11 Depth=1
	cmpb	$45, (%rsi)
	je	.LBB73_17
# BB#16:                                #   in Loop: Header=BB73_11 Depth=1
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	addss	(%r12), %xmm0
	movss	%xmm0, (%r12)
.LBB73_17:                              #   in Loop: Header=BB73_11 Depth=1
	addq	$16, %rcx
	addq	$2, %rdx
	addq	$2, %rsi
	addq	$-2, %rax
	jne	.LBB73_11
.LBB73_37:                              # %._crit_edge52
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end73:
	.size	new_FinalGapCount_zure, .Lfunc_end73-new_FinalGapCount_zure
	.cfi_endproc

	.globl	new_FinalGapCount
	.p2align	4, 0x90
	.type	new_FinalGapCount,@function
new_FinalGapCount:                      # @new_FinalGapCount
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi664:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi665:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi666:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi667:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi668:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi669:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi670:
	.cfi_def_cfa_offset 80
.Lcfi671:
	.cfi_offset %rbx, -56
.Lcfi672:
	.cfi_offset %r12, -48
.Lcfi673:
	.cfi_offset %r13, -40
.Lcfi674:
	.cfi_offset %r14, -32
.Lcfi675:
	.cfi_offset %r15, -24
.Lcfi676:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movl	%r8d, %ebx
	movq	%rcx, %r15
	movq	%rdx, %r13
	movl	%esi, %ebp
	movq	%rdi, %r12
	testl	%ebx, %ebx
	je	.LBB74_2
# BB#1:                                 # %.lr.ph55.preheader
	leal	-1(%rbx), %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	memset
.LBB74_2:                               # %.preheader
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	testl	%ebp, %ebp
	jle	.LBB74_28
# BB#3:                                 # %.lr.ph50
	movq	16(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	je	.LBB74_4
# BB#9:                                 # %.lr.ph50.split.preheader
	leal	-1(%rax), %r11d
	movl	%ebp, %r9d
	movl	%eax, %r10d
	andl	$1, %r10d
	leaq	4(%r12), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB74_10:                              # %.lr.ph50.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB74_18 Depth 2
	movq	(%r13,%rdi,8), %rbp
	movb	(%rbp), %al
	xorl	%esi, %esi
	cmpb	$45, %al
	sete	%cl
	testl	%r10d, %r10d
	movsd	(%r15,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	jne	.LBB74_12
# BB#11:                                #   in Loop: Header=BB74_10 Depth=1
	movb	%cl, %sil
                                        # implicit-def: %AL
	movq	16(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	%r12, %rdx
	testl	%r11d, %r11d
	jne	.LBB74_17
	jmp	.LBB74_24
	.p2align	4, 0x90
.LBB74_12:                              #   in Loop: Header=BB74_10 Depth=1
	cmpb	$45, %al
	setne	%cl
	xorl	%esi, %esi
	cmpb	$45, 1(%rbp)
	leaq	1(%rbp), %rbp
	sete	%al
	movb	%al, %sil
	je	.LBB74_15
# BB#13:                                #   in Loop: Header=BB74_10 Depth=1
	testb	%cl, %cl
	jne	.LBB74_15
# BB#14:                                #   in Loop: Header=BB74_10 Depth=1
	movss	(%r12), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r12)
.LBB74_15:                              # %.prol.loopexit
                                        #   in Loop: Header=BB74_10 Depth=1
	movl	%r11d, %ecx
	movq	8(%rsp), %rdx           # 8-byte Reload
	testl	%r11d, %r11d
	je	.LBB74_24
.LBB74_17:                              # %.lr.ph50.split.new
                                        #   in Loop: Header=BB74_10 Depth=1
	addq	$2, %rbp
	addq	$4, %rdx
	.p2align	4, 0x90
.LBB74_18:                              #   Parent Loop BB74_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rbp), %eax
	xorl	%ebx, %ebx
	cmpb	$45, %al
	setne	%bl
	negl	%esi
	testl	%esi, %ebx
	je	.LBB74_20
# BB#19:                                #   in Loop: Header=BB74_18 Depth=2
	movss	-4(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%rdx)
.LBB74_20:                              #   in Loop: Header=BB74_18 Depth=2
	addl	$-2, %ecx
	cmpb	$45, %al
	setne	%r8b
	xorl	%esi, %esi
	cmpb	$45, (%rbp)
	sete	%al
	je	.LBB74_23
# BB#21:                                #   in Loop: Header=BB74_18 Depth=2
	testb	%r8b, %r8b
	jne	.LBB74_23
# BB#22:                                #   in Loop: Header=BB74_18 Depth=2
	movss	(%rdx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rdx)
.LBB74_23:                              #   in Loop: Header=BB74_18 Depth=2
	movb	%al, %sil
	addq	$2, %rbp
	addq	$8, %rdx
	testl	%ecx, %ecx
	jne	.LBB74_18
.LBB74_24:                              # %._crit_edge
                                        #   in Loop: Header=BB74_10 Depth=1
	testb	$1, %al
	je	.LBB74_27
# BB#25:                                # %._crit_edge
                                        #   in Loop: Header=BB74_10 Depth=1
	cmpb	$45, (%r14,%rdi)
	je	.LBB74_27
# BB#26:                                #   in Loop: Header=BB74_10 Depth=1
	addss	4(%r12,%r11,4), %xmm0
	movss	%xmm0, 4(%r12,%r11,4)
.LBB74_27:                              #   in Loop: Header=BB74_10 Depth=1
	incq	%rdi
	cmpq	%r9, %rdi
	jne	.LBB74_10
	jmp	.LBB74_28
.LBB74_4:                               # %.lr.ph50.split.us.preheader
	movl	%ebp, %eax
	.p2align	4, 0x90
.LBB74_5:                               # %.lr.ph50.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rcx
	cmpb	$45, (%rcx)
	jne	.LBB74_8
# BB#6:                                 # %.lr.ph50.split.us
                                        #   in Loop: Header=BB74_5 Depth=1
	cmpb	$45, (%r14)
	je	.LBB74_8
# BB#7:                                 #   in Loop: Header=BB74_5 Depth=1
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	addss	(%r12), %xmm0
	movss	%xmm0, (%r12)
.LBB74_8:                               #   in Loop: Header=BB74_5 Depth=1
	addq	$8, %r15
	addq	$8, %r13
	incq	%r14
	decq	%rax
	jne	.LBB74_5
.LBB74_28:                              # %._crit_edge51
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end74:
	.size	new_FinalGapCount, .Lfunc_end74-new_FinalGapCount
	.cfi_endproc

	.globl	st_OpeningGapCount
	.p2align	4, 0x90
	.type	st_OpeningGapCount,@function
st_OpeningGapCount:                     # @st_OpeningGapCount
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi677:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi678:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi679:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi680:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi681:
	.cfi_def_cfa_offset 48
.Lcfi682:
	.cfi_offset %rbx, -48
.Lcfi683:
	.cfi_offset %r12, -40
.Lcfi684:
	.cfi_offset %r14, -32
.Lcfi685:
	.cfi_offset %r15, -24
.Lcfi686:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movq	%rcx, %r14
	movq	%rdx, %r12
	movl	%esi, %ebp
	movq	%rdi, %r15
	testl	%ebx, %ebx
	je	.LBB75_2
# BB#1:                                 # %.lr.ph49.preheader
	leal	-1(%rbx), %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	memset
.LBB75_2:                               # %.preheader
	testl	%ebp, %ebp
	jle	.LBB75_18
# BB#3:                                 # %.preheader
	testl	%ebx, %ebx
	je	.LBB75_18
# BB#4:                                 # %.lr.ph44.split.preheader
	movl	%ebp, %r10d
	movl	%ebx, %r11d
	andl	$1, %r11d
	leal	-1(%rbx), %r8d
	leaq	4(%r15), %r9
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB75_5:                               # %.lr.ph44.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB75_12 Depth 2
	testl	%r11d, %r11d
	movsd	(%r14,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	(%r12,%rdi,8), %rbp
	jne	.LBB75_7
# BB#6:                                 #   in Loop: Header=BB75_5 Depth=1
	movl	%ebx, %edx
	movq	%r15, %rax
	xorl	%esi, %esi
	cmpl	$1, %ebx
	jne	.LBB75_11
	jmp	.LBB75_17
	.p2align	4, 0x90
.LBB75_7:                               #   in Loop: Header=BB75_5 Depth=1
	xorl	%esi, %esi
	cmpb	$45, (%rbp)
	leaq	1(%rbp), %rbp
	sete	%sil
	jne	.LBB75_9
# BB#8:                                 #   in Loop: Header=BB75_5 Depth=1
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r15)
.LBB75_9:                               # %.prol.loopexit
                                        #   in Loop: Header=BB75_5 Depth=1
	movl	%r8d, %edx
	movq	%r9, %rax
	cmpl	$1, %ebx
	je	.LBB75_17
.LBB75_11:                              # %.lr.ph44.split.new
                                        #   in Loop: Header=BB75_5 Depth=1
	addq	$4, %rax
	.p2align	4, 0x90
.LBB75_12:                              #   Parent Loop BB75_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%ecx, %ecx
	cmpb	$45, (%rbp)
	sete	%cl
	xorl	$-2, %esi
	incl	%esi
	testl	%esi, %ecx
	je	.LBB75_14
# BB#13:                                #   in Loop: Header=BB75_12 Depth=2
	movss	-4(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%rax)
.LBB75_14:                              #   in Loop: Header=BB75_12 Depth=2
	addl	$-2, %edx
	xorl	%esi, %esi
	cmpb	$45, 1(%rbp)
	sete	%sil
	orl	$-2, %ecx
	incl	%ecx
	testl	%ecx, %esi
	je	.LBB75_16
# BB#15:                                #   in Loop: Header=BB75_12 Depth=2
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rax)
.LBB75_16:                              #   in Loop: Header=BB75_12 Depth=2
	addq	$8, %rax
	addq	$2, %rbp
	testl	%edx, %edx
	jne	.LBB75_12
.LBB75_17:                              # %._crit_edge
                                        #   in Loop: Header=BB75_5 Depth=1
	incq	%rdi
	cmpq	%r10, %rdi
	jne	.LBB75_5
.LBB75_18:                              # %._crit_edge45
	movslq	%ebx, %rax
	movl	$0, (%r15,%rax,4)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end75:
	.size	st_OpeningGapCount, .Lfunc_end75-st_OpeningGapCount
	.cfi_endproc

	.globl	st_FinalGapCount_zure
	.p2align	4, 0x90
	.type	st_FinalGapCount_zure,@function
st_FinalGapCount_zure:                  # @st_FinalGapCount_zure
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi687:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi688:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi689:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi690:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi691:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi692:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi693:
	.cfi_def_cfa_offset 80
.Lcfi694:
	.cfi_offset %rbx, -56
.Lcfi695:
	.cfi_offset %r12, -48
.Lcfi696:
	.cfi_offset %r13, -40
.Lcfi697:
	.cfi_offset %r14, -32
.Lcfi698:
	.cfi_offset %r15, -24
.Lcfi699:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movq	%rcx, %r14
	movq	%rdx, %r12
	movl	%esi, %ebp
	movq	%rdi, %r13
	cmpl	$-1, %ebx
	je	.LBB76_2
# BB#1:                                 # %.lr.ph54.preheader
	movl	%ebx, %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	memset
.LBB76_2:                               # %.preheader
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	testl	%ebp, %ebp
	jle	.LBB76_33
# BB#3:                                 # %.lr.ph49
	leaq	4(%r13), %r11
	movq	16(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	je	.LBB76_4
# BB#16:                                # %.lr.ph49.split.preheader
	leal	-1(%rax), %r15d
	movl	%ebp, %r9d
	movl	%eax, %r10d
	andl	$1, %r10d
	leaq	8(%r13), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB76_17:                              # %.lr.ph49.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB76_25 Depth 2
	movq	(%r12,%rbp,8), %rdi
	movb	(%rdi), %al
	xorl	%edx, %edx
	cmpb	$45, %al
	sete	%cl
	testl	%r10d, %r10d
	movsd	(%r14,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	jne	.LBB76_19
# BB#18:                                #   in Loop: Header=BB76_17 Depth=1
	movb	%cl, %dl
                                        # implicit-def: %CL
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ebx
	movq	%r11, %rax
	testl	%r15d, %r15d
	jne	.LBB76_24
	jmp	.LBB76_30
	.p2align	4, 0x90
.LBB76_19:                              #   in Loop: Header=BB76_17 Depth=1
	cmpb	$45, %al
	setne	%al
	xorl	%edx, %edx
	cmpb	$45, 1(%rdi)
	leaq	1(%rdi), %rdi
	sete	%cl
	movb	%cl, %dl
	je	.LBB76_22
# BB#20:                                #   in Loop: Header=BB76_17 Depth=1
	testb	%al, %al
	jne	.LBB76_22
# BB#21:                                #   in Loop: Header=BB76_17 Depth=1
	movss	(%r11), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r11)
.LBB76_22:                              # %.prol.loopexit
                                        #   in Loop: Header=BB76_17 Depth=1
	movl	%r15d, %ebx
	movq	8(%rsp), %rax           # 8-byte Reload
	testl	%r15d, %r15d
	je	.LBB76_30
.LBB76_24:                              # %.lr.ph49.split.new
                                        #   in Loop: Header=BB76_17 Depth=1
	addq	$4, %rax
	addq	$2, %rdi
	.p2align	4, 0x90
.LBB76_25:                              #   Parent Loop BB76_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rdi), %ecx
	xorl	%esi, %esi
	cmpb	$45, %cl
	setne	%sil
	negl	%edx
	testl	%edx, %esi
	je	.LBB76_26
# BB#34:                                #   in Loop: Header=BB76_25 Depth=2
	movss	-4(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%rax)
.LBB76_26:                              # %.backedge
                                        #   in Loop: Header=BB76_25 Depth=2
	addl	$-2, %ebx
	cmpb	$45, %cl
	setne	%r8b
	xorl	%edx, %edx
	cmpb	$45, (%rdi)
	sete	%cl
	je	.LBB76_29
# BB#27:                                # %.backedge
                                        #   in Loop: Header=BB76_25 Depth=2
	testb	%r8b, %r8b
	jne	.LBB76_29
# BB#28:                                #   in Loop: Header=BB76_25 Depth=2
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rax)
.LBB76_29:                              # %.backedge.1
                                        #   in Loop: Header=BB76_25 Depth=2
	movb	%cl, %dl
	addq	$8, %rax
	addq	$2, %rdi
	testl	%ebx, %ebx
	jne	.LBB76_25
.LBB76_30:                              # %._crit_edge
                                        #   in Loop: Header=BB76_17 Depth=1
	testb	$1, %cl
	je	.LBB76_32
# BB#31:                                #   in Loop: Header=BB76_17 Depth=1
	addss	8(%r13,%r15,4), %xmm0
	movss	%xmm0, 8(%r13,%r15,4)
.LBB76_32:                              #   in Loop: Header=BB76_17 Depth=1
	incq	%rbp
	cmpq	%r9, %rbp
	jne	.LBB76_17
	jmp	.LBB76_33
.LBB76_4:                               # %.lr.ph49.split.us.preheader
	movl	%ebp, %eax
	testb	$1, %al
	jne	.LBB76_6
# BB#5:
	xorl	%edx, %edx
	cmpl	$1, %ebp
	jne	.LBB76_10
	jmp	.LBB76_33
.LBB76_6:                               # %.lr.ph49.split.us.prol
	movq	(%r12), %rcx
	cmpb	$45, (%rcx)
	jne	.LBB76_8
# BB#7:
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	addss	(%r11), %xmm0
	movss	%xmm0, (%r11)
.LBB76_8:                               # %.lr.ph49.split.us.prol.loopexit
	movl	$1, %edx
	cmpl	$1, %ebp
	je	.LBB76_33
.LBB76_10:                              # %.lr.ph49.split.us.preheader.new
	subq	%rdx, %rax
	leaq	8(%r14,%rdx,8), %rcx
	leaq	8(%r12,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB76_11:                              # %.lr.ph49.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rdx), %rsi
	cmpb	$45, (%rsi)
	jne	.LBB76_13
# BB#12:                                #   in Loop: Header=BB76_11 Depth=1
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	addss	(%r11), %xmm0
	movss	%xmm0, (%r11)
.LBB76_13:                              # %.lr.ph49.split.us.163
                                        #   in Loop: Header=BB76_11 Depth=1
	movq	(%rdx), %rsi
	cmpb	$45, (%rsi)
	jne	.LBB76_15
# BB#14:                                #   in Loop: Header=BB76_11 Depth=1
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	addss	(%r11), %xmm0
	movss	%xmm0, (%r11)
.LBB76_15:                              #   in Loop: Header=BB76_11 Depth=1
	addq	$16, %rcx
	addq	$16, %rdx
	addq	$-2, %rax
	jne	.LBB76_11
.LBB76_33:                              # %._crit_edge50
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end76:
	.size	st_FinalGapCount_zure, .Lfunc_end76-st_FinalGapCount_zure
	.cfi_endproc

	.globl	st_FinalGapCount
	.p2align	4, 0x90
	.type	st_FinalGapCount,@function
st_FinalGapCount:                       # @st_FinalGapCount
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi700:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi701:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi702:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi703:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi704:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi705:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi706:
	.cfi_def_cfa_offset 64
.Lcfi707:
	.cfi_offset %rbx, -56
.Lcfi708:
	.cfi_offset %r12, -48
.Lcfi709:
	.cfi_offset %r13, -40
.Lcfi710:
	.cfi_offset %r14, -32
.Lcfi711:
	.cfi_offset %r15, -24
.Lcfi712:
	.cfi_offset %rbp, -16
	movl	%r8d, %r12d
	movq	%rcx, %r15
	movq	%rdx, %r14
	movl	%esi, %ebp
	movq	%rdi, %r13
	testl	%r12d, %r12d
	je	.LBB77_2
# BB#1:                                 # %.lr.ph53.preheader
	leal	-1(%r12), %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	memset
.LBB77_2:                               # %.preheader
	testl	%ebp, %ebp
	jle	.LBB77_34
# BB#3:                                 # %.lr.ph48
	testl	%r12d, %r12d
	je	.LBB77_4
# BB#16:                                # %.lr.ph48.split.preheader
	leal	-1(%r12), %r11d
	movl	%ebp, %r9d
	movl	%r12d, %r10d
	andl	$1, %r10d
	leaq	4(%r13), %r8
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB77_17:                              # %.lr.ph48.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB77_25 Depth 2
	movq	(%r14,%rdi,8), %rbp
	movb	(%rbp), %cl
	xorl	%esi, %esi
	cmpb	$45, %cl
	sete	%dl
	testl	%r10d, %r10d
	movsd	(%r15,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	jne	.LBB77_19
# BB#18:                                #   in Loop: Header=BB77_17 Depth=1
	movb	%dl, %sil
                                        # implicit-def: %DL
	movl	%r12d, %ecx
	movq	%r13, %rbx
	testl	%r11d, %r11d
	jne	.LBB77_24
	jmp	.LBB77_31
	.p2align	4, 0x90
.LBB77_19:                              #   in Loop: Header=BB77_17 Depth=1
	cmpb	$45, %cl
	setne	%cl
	xorl	%esi, %esi
	cmpb	$45, 1(%rbp)
	leaq	1(%rbp), %rbp
	sete	%dl
	movb	%dl, %sil
	je	.LBB77_22
# BB#20:                                #   in Loop: Header=BB77_17 Depth=1
	testb	%cl, %cl
	jne	.LBB77_22
# BB#21:                                #   in Loop: Header=BB77_17 Depth=1
	movss	(%r13), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r13)
.LBB77_22:                              # %.prol.loopexit
                                        #   in Loop: Header=BB77_17 Depth=1
	movl	%r11d, %ecx
	movq	%r8, %rbx
	testl	%r11d, %r11d
	je	.LBB77_31
.LBB77_24:                              # %.lr.ph48.split.new
                                        #   in Loop: Header=BB77_17 Depth=1
	addq	$2, %rbp
	addq	$4, %rbx
	.p2align	4, 0x90
.LBB77_25:                              #   Parent Loop BB77_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rbp), %edx
	xorl	%eax, %eax
	cmpb	$45, %dl
	setne	%al
	negl	%esi
	testl	%esi, %eax
	je	.LBB77_27
# BB#26:                                #   in Loop: Header=BB77_25 Depth=2
	movss	-4(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%rbx)
.LBB77_27:                              #   in Loop: Header=BB77_25 Depth=2
	addl	$-2, %ecx
	cmpb	$45, %dl
	setne	%al
	xorl	%esi, %esi
	cmpb	$45, (%rbp)
	sete	%dl
	je	.LBB77_30
# BB#28:                                #   in Loop: Header=BB77_25 Depth=2
	testb	%al, %al
	jne	.LBB77_30
# BB#29:                                #   in Loop: Header=BB77_25 Depth=2
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rbx)
.LBB77_30:                              #   in Loop: Header=BB77_25 Depth=2
	movb	%dl, %sil
	addq	$2, %rbp
	addq	$8, %rbx
	testl	%ecx, %ecx
	jne	.LBB77_25
.LBB77_31:                              # %._crit_edge
                                        #   in Loop: Header=BB77_17 Depth=1
	testb	$1, %dl
	je	.LBB77_33
# BB#32:                                #   in Loop: Header=BB77_17 Depth=1
	addss	4(%r13,%r11,4), %xmm0
	movss	%xmm0, 4(%r13,%r11,4)
.LBB77_33:                              #   in Loop: Header=BB77_17 Depth=1
	incq	%rdi
	cmpq	%r9, %rdi
	jne	.LBB77_17
	jmp	.LBB77_34
.LBB77_4:                               # %.lr.ph48.split.us.preheader
	movl	%ebp, %eax
	testb	$1, %al
	jne	.LBB77_6
# BB#5:
	xorl	%edx, %edx
	cmpl	$1, %ebp
	jne	.LBB77_10
	jmp	.LBB77_34
.LBB77_6:                               # %.lr.ph48.split.us.prol
	movq	(%r14), %rcx
	cmpb	$45, (%rcx)
	jne	.LBB77_8
# BB#7:
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	addss	(%r13), %xmm0
	movss	%xmm0, (%r13)
.LBB77_8:                               # %.lr.ph48.split.us.prol.loopexit
	movl	$1, %edx
	cmpl	$1, %ebp
	je	.LBB77_34
.LBB77_10:                              # %.lr.ph48.split.us.preheader.new
	subq	%rdx, %rax
	leaq	8(%r15,%rdx,8), %rcx
	leaq	8(%r14,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB77_11:                              # %.lr.ph48.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rdx), %rsi
	cmpb	$45, (%rsi)
	jne	.LBB77_13
# BB#12:                                #   in Loop: Header=BB77_11 Depth=1
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	addss	(%r13), %xmm0
	movss	%xmm0, (%r13)
.LBB77_13:                              # %.lr.ph48.split.us.163
                                        #   in Loop: Header=BB77_11 Depth=1
	movq	(%rdx), %rsi
	cmpb	$45, (%rsi)
	jne	.LBB77_15
# BB#14:                                #   in Loop: Header=BB77_11 Depth=1
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	addss	(%r13), %xmm0
	movss	%xmm0, (%r13)
.LBB77_15:                              #   in Loop: Header=BB77_11 Depth=1
	addq	$16, %rcx
	addq	$16, %rdx
	addq	$-2, %rax
	jne	.LBB77_11
.LBB77_34:                              # %._crit_edge49
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end77:
	.size	st_FinalGapCount, .Lfunc_end77-st_FinalGapCount
	.cfi_endproc

	.globl	getGapPattern
	.p2align	4, 0x90
	.type	getGapPattern,@function
getGapPattern:                          # @getGapPattern
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi713:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi714:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi715:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi716:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi717:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi718:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi719:
	.cfi_def_cfa_offset 64
.Lcfi720:
	.cfi_offset %rbx, -56
.Lcfi721:
	.cfi_offset %r12, -48
.Lcfi722:
	.cfi_offset %r13, -40
.Lcfi723:
	.cfi_offset %r14, -32
.Lcfi724:
	.cfi_offset %r15, -24
.Lcfi725:
	.cfi_offset %rbp, -16
	movl	%r8d, %r13d
	movq	%rcx, %r15
	movq	%rdx, %r12
	movl	%esi, %ebp
	movq	%rdi, %r14
	cmpl	$-1, %r13d
	je	.LBB78_2
# BB#1:                                 # %.lr.ph59.preheader
	movl	%r13d, %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	memset
.LBB78_2:                               # %.preheader46
	testl	%ebp, %ebp
	jle	.LBB78_7
# BB#3:                                 # %.lr.ph55
	cmpl	$-1, %r13d
	je	.LBB78_10
# BB#4:                                 # %.lr.ph55.split.preheader
	movl	%ebp, %r10d
	movl	%r13d, %r11d
	andl	$1, %r11d
	leaq	4(%r14), %r8
	leal	-1(%r13), %r9d
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB78_5:                               # %.lr.ph55.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB78_17 Depth 2
	movsd	(%r15,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	(%r12,%rdi,8), %rbp
	movb	(%rbp), %al
	xorl	%edx, %edx
	cmpb	$45, %al
	sete	%cl
	testl	%r11d, %r11d
	jne	.LBB78_6
# BB#11:                                #   in Loop: Header=BB78_5 Depth=1
	cmpb	$45, %al
	setne	%al
	xorl	%edx, %edx
	cmpb	$45, 1(%rbp)
	leaq	1(%rbp), %rbp
	sete	%dl
	je	.LBB78_14
# BB#12:                                #   in Loop: Header=BB78_5 Depth=1
	testb	%al, %al
	jne	.LBB78_14
# BB#13:                                #   in Loop: Header=BB78_5 Depth=1
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r14)
.LBB78_14:                              # %.prol.loopexit
                                        #   in Loop: Header=BB78_5 Depth=1
	movl	%r9d, %esi
	movq	%r8, %rax
	testl	%r13d, %r13d
	jne	.LBB78_16
	jmp	.LBB78_23
	.p2align	4, 0x90
.LBB78_6:                               #   in Loop: Header=BB78_5 Depth=1
	movb	%cl, %dl
	movl	%r13d, %esi
	movq	%r14, %rax
	testl	%r13d, %r13d
	je	.LBB78_23
.LBB78_16:                              # %.lr.ph55.split.new
                                        #   in Loop: Header=BB78_5 Depth=1
	notl	%esi
	addq	$2, %rbp
	addq	$4, %rax
	.p2align	4, 0x90
.LBB78_17:                              #   Parent Loop BB78_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rbp), %ecx
	xorl	%ebx, %ebx
	cmpb	$45, %cl
	setne	%bl
	negl	%edx
	testl	%edx, %ebx
	je	.LBB78_19
# BB#18:                                #   in Loop: Header=BB78_17 Depth=2
	movss	-4(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%rax)
.LBB78_19:                              #   in Loop: Header=BB78_17 Depth=2
	cmpb	$45, %cl
	setne	%bl
	xorl	%edx, %edx
	cmpb	$45, (%rbp)
	sete	%cl
	je	.LBB78_22
# BB#20:                                #   in Loop: Header=BB78_17 Depth=2
	testb	%bl, %bl
	jne	.LBB78_22
# BB#21:                                #   in Loop: Header=BB78_17 Depth=2
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rax)
.LBB78_22:                              #   in Loop: Header=BB78_17 Depth=2
	movb	%cl, %dl
	addq	$2, %rbp
	addq	$8, %rax
	addl	$2, %esi
	jne	.LBB78_17
.LBB78_23:                              # %._crit_edge53
                                        #   in Loop: Header=BB78_5 Depth=1
	incq	%rdi
	cmpq	%r10, %rdi
	jne	.LBB78_5
.LBB78_7:                               # %.preheader
	testl	%r13d, %r13d
	jg	.LBB78_8
.LBB78_10:                              # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB78_8:                               # %.lr.ph.preheader
	movl	%r13d, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB78_9:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	stderr(%rip), %rdi
	movss	(%r14,%rbx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.33, %esi
	movb	$1, %al
	movl	%ebx, %edx
	callq	fprintf
	incq	%rbx
	cmpq	%rbx, %rbp
	jne	.LBB78_9
	jmp	.LBB78_10
.Lfunc_end78:
	.size	getGapPattern, .Lfunc_end78-getGapPattern
	.cfi_endproc

	.globl	getdigapfreq_st
	.p2align	4, 0x90
	.type	getdigapfreq_st,@function
getdigapfreq_st:                        # @getdigapfreq_st
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi726:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi727:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi728:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi729:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi730:
	.cfi_def_cfa_offset 48
.Lcfi731:
	.cfi_offset %rbx, -48
.Lcfi732:
	.cfi_offset %r12, -40
.Lcfi733:
	.cfi_offset %r14, -32
.Lcfi734:
	.cfi_offset %r15, -24
.Lcfi735:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movq	%rcx, %r14
	movq	%rdx, %r15
	movl	%esi, %ebp
	movq	%rdi, %r12
	testl	%ebx, %ebx
	js	.LBB79_2
# BB#1:                                 # %.lr.ph33.preheader
	movl	%ebx, %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	memset
.LBB79_2:                               # %.preheader
	testl	%ebp, %ebp
	jle	.LBB79_21
# BB#3:                                 # %.preheader
	cmpl	$2, %ebx
	jl	.LBB79_21
# BB#4:                                 # %.lr.ph29.split.us.preheader
	movl	%ebx, %r9d
	movl	%ebp, %ecx
	movl	%r9d, %edx
	andl	$1, %edx
	leaq	4(%r12), %r8
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB79_5:                               # %.lr.ph29.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB79_13 Depth 2
	movsd	(%r14,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	(%r15,%rdi,8), %rbp
	testq	%rdx, %rdx
	jne	.LBB79_6
# BB#7:                                 #   in Loop: Header=BB79_5 Depth=1
	cmpb	$45, 1(%rbp)
	jne	.LBB79_10
# BB#8:                                 #   in Loop: Header=BB79_5 Depth=1
	cmpb	$45, (%rbp)
	jne	.LBB79_10
# BB#9:                                 #   in Loop: Header=BB79_5 Depth=1
	movss	4(%r12), %xmm1          # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 4(%r12)
.LBB79_10:                              # %.prol.loopexit
                                        #   in Loop: Header=BB79_5 Depth=1
	movl	$2, %esi
	cmpl	$2, %ebx
	jne	.LBB79_12
	jmp	.LBB79_20
	.p2align	4, 0x90
.LBB79_6:                               #   in Loop: Header=BB79_5 Depth=1
	movl	$1, %esi
	cmpl	$2, %ebx
	je	.LBB79_20
.LBB79_12:                              # %.lr.ph29.split.us.new
                                        #   in Loop: Header=BB79_5 Depth=1
	movq	%r9, %rax
	subq	%rsi, %rax
	addq	%rsi, %rbp
	leaq	(%r8,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB79_13:                              #   Parent Loop BB79_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$45, (%rbp)
	jne	.LBB79_16
# BB#14:                                #   in Loop: Header=BB79_13 Depth=2
	cmpb	$45, -1(%rbp)
	jne	.LBB79_16
# BB#15:                                #   in Loop: Header=BB79_13 Depth=2
	movss	-4(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%rsi)
.LBB79_16:                              #   in Loop: Header=BB79_13 Depth=2
	cmpb	$45, 1(%rbp)
	jne	.LBB79_19
# BB#17:                                #   in Loop: Header=BB79_13 Depth=2
	cmpb	$45, (%rbp)
	jne	.LBB79_19
# BB#18:                                #   in Loop: Header=BB79_13 Depth=2
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rsi)
.LBB79_19:                              #   in Loop: Header=BB79_13 Depth=2
	addq	$2, %rbp
	addq	$8, %rsi
	addq	$-2, %rax
	jne	.LBB79_13
.LBB79_20:                              # %._crit_edge.us
                                        #   in Loop: Header=BB79_5 Depth=1
	incq	%rdi
	cmpq	%rcx, %rdi
	jne	.LBB79_5
.LBB79_21:                              # %._crit_edge30
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end79:
	.size	getdigapfreq_st, .Lfunc_end79-getdigapfreq_st
	.cfi_endproc

	.globl	getdiaminofreq_x
	.p2align	4, 0x90
	.type	getdiaminofreq_x,@function
getdiaminofreq_x:                       # @getdiaminofreq_x
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi736:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi737:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi738:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi739:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi740:
	.cfi_def_cfa_offset 48
.Lcfi741:
	.cfi_offset %rbx, -48
.Lcfi742:
	.cfi_offset %r12, -40
.Lcfi743:
	.cfi_offset %r14, -32
.Lcfi744:
	.cfi_offset %r15, -24
.Lcfi745:
	.cfi_offset %rbp, -16
	movl	%r8d, %r12d
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movl	%esi, %ebp
	movq	%rdi, %r15
	cmpl	$-1, %r12d
	jl	.LBB80_2
# BB#1:                                 # %.lr.ph42.preheader
	leal	1(%r12), %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	memset
.LBB80_2:                               # %.preheader35
	testl	%ebp, %ebp
	jle	.LBB80_30
# BB#3:                                 # %.lr.ph38
	cmpl	$1, %r12d
	movslq	%r12d, %rax
	jle	.LBB80_4
# BB#10:                                # %.lr.ph38.split.us.preheader
	movl	%r12d, %r9d
	movl	%ebp, %r10d
	movl	%r9d, %r11d
	andl	$1, %r11d
	leaq	4(%r15), %r8
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB80_11:                              # %.lr.ph38.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB80_20 Depth 2
	movsd	(%r14,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	(%rbx,%rbp,8), %rdi
	cmpb	$45, (%rdi)
	je	.LBB80_12
# BB#31:                                #   in Loop: Header=BB80_11 Depth=1
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r15)
.LBB80_12:                              # %.preheader.us.preheader
                                        #   in Loop: Header=BB80_11 Depth=1
	testq	%r11, %r11
	jne	.LBB80_13
# BB#14:                                # %.preheader.us.prol
                                        #   in Loop: Header=BB80_11 Depth=1
	cmpb	$45, 1(%rdi)
	je	.LBB80_17
# BB#15:                                #   in Loop: Header=BB80_11 Depth=1
	cmpb	$45, (%rdi)
	je	.LBB80_17
# BB#16:                                #   in Loop: Header=BB80_11 Depth=1
	movss	4(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 4(%r15)
.LBB80_17:                              # %.preheader.us.prol.loopexit
                                        #   in Loop: Header=BB80_11 Depth=1
	movl	$2, %esi
	cmpl	$2, %r12d
	jne	.LBB80_19
	jmp	.LBB80_27
	.p2align	4, 0x90
.LBB80_13:                              #   in Loop: Header=BB80_11 Depth=1
	movl	$1, %esi
	cmpl	$2, %r12d
	je	.LBB80_27
.LBB80_19:                              # %.preheader.us.preheader.new
                                        #   in Loop: Header=BB80_11 Depth=1
	movq	%r9, %rcx
	subq	%rsi, %rcx
	leaq	(%rdi,%rsi), %rdx
	leaq	(%r8,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB80_20:                              # %.preheader.us
                                        #   Parent Loop BB80_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$45, (%rdx)
	je	.LBB80_23
# BB#21:                                #   in Loop: Header=BB80_20 Depth=2
	cmpb	$45, -1(%rdx)
	je	.LBB80_23
# BB#22:                                #   in Loop: Header=BB80_20 Depth=2
	movss	-4(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%rsi)
.LBB80_23:                              # %.preheader.us.159
                                        #   in Loop: Header=BB80_20 Depth=2
	cmpb	$45, 1(%rdx)
	je	.LBB80_26
# BB#24:                                #   in Loop: Header=BB80_20 Depth=2
	cmpb	$45, (%rdx)
	je	.LBB80_26
# BB#25:                                #   in Loop: Header=BB80_20 Depth=2
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rsi)
.LBB80_26:                              #   in Loop: Header=BB80_20 Depth=2
	addq	$2, %rdx
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB80_20
.LBB80_27:                              # %._crit_edge.us
                                        #   in Loop: Header=BB80_11 Depth=1
	cmpb	$45, -1(%rdi,%rax)
	je	.LBB80_29
# BB#28:                                #   in Loop: Header=BB80_11 Depth=1
	addss	(%r15,%rax,4), %xmm0
	movss	%xmm0, (%r15,%rax,4)
.LBB80_29:                              #   in Loop: Header=BB80_11 Depth=1
	incq	%rbp
	cmpq	%r10, %rbp
	jne	.LBB80_11
	jmp	.LBB80_30
.LBB80_4:                               # %.lr.ph38.split.preheader
	movl	%ebp, %ecx
	.p2align	4, 0x90
.LBB80_5:                               # %.lr.ph38.split
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	(%rbx), %rdx
	cmpb	$45, (%rdx)
	je	.LBB80_7
# BB#6:                                 #   in Loop: Header=BB80_5 Depth=1
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r15)
.LBB80_7:                               # %.preheader
                                        #   in Loop: Header=BB80_5 Depth=1
	cmpb	$45, -1(%rdx,%rax)
	je	.LBB80_9
# BB#8:                                 #   in Loop: Header=BB80_5 Depth=1
	addss	(%r15,%rax,4), %xmm0
	movss	%xmm0, (%r15,%rax,4)
.LBB80_9:                               #   in Loop: Header=BB80_5 Depth=1
	addq	$8, %r14
	addq	$8, %rbx
	decq	%rcx
	jne	.LBB80_5
.LBB80_30:                              # %._crit_edge39
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end80:
	.size	getdiaminofreq_x, .Lfunc_end80-getdiaminofreq_x
	.cfi_endproc

	.globl	getdiaminofreq_st
	.p2align	4, 0x90
	.type	getdiaminofreq_st,@function
getdiaminofreq_st:                      # @getdiaminofreq_st
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi746:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi747:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi748:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi749:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi750:
	.cfi_def_cfa_offset 48
.Lcfi751:
	.cfi_offset %rbx, -48
.Lcfi752:
	.cfi_offset %r12, -40
.Lcfi753:
	.cfi_offset %r14, -32
.Lcfi754:
	.cfi_offset %r15, -24
.Lcfi755:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebp
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movl	%esi, %r12d
	movq	%rdi, %r15
	testl	%ebp, %ebp
	js	.LBB81_2
# BB#1:                                 # %.lr.ph41.preheader
	movl	%ebp, %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	memset
.LBB81_2:                               # %.preheader34
	testl	%r12d, %r12d
	jle	.LBB81_8
# BB#3:                                 # %.lr.ph37
	cmpl	$1, %ebp
	movslq	%ebp, %rax
	jle	.LBB81_4
# BB#9:                                 # %.lr.ph37.split.us.preheader
	movl	%ebp, %r9d
	movl	%r12d, %r10d
	movl	%r9d, %r11d
	andl	$1, %r11d
	leaq	4(%r15), %r8
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB81_10:                              # %.lr.ph37.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB81_20 Depth 2
	movsd	(%r14,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	(%rbx,%rdi,8), %rcx
	cmpb	$45, (%rcx)
	je	.LBB81_12
# BB#11:                                #   in Loop: Header=BB81_10 Depth=1
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r15)
.LBB81_12:                              # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB81_10 Depth=1
	testq	%r11, %r11
	jne	.LBB81_13
# BB#14:                                # %.lr.ph.us.prol
                                        #   in Loop: Header=BB81_10 Depth=1
	cmpb	$45, 1(%rcx)
	je	.LBB81_17
# BB#15:                                #   in Loop: Header=BB81_10 Depth=1
	cmpb	$45, (%rcx)
	je	.LBB81_17
# BB#16:                                #   in Loop: Header=BB81_10 Depth=1
	movss	4(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 4(%r15)
.LBB81_17:                              # %.lr.ph.us.prol.loopexit
                                        #   in Loop: Header=BB81_10 Depth=1
	movl	$2, %esi
	cmpl	$2, %ebp
	jne	.LBB81_19
	jmp	.LBB81_27
	.p2align	4, 0x90
.LBB81_13:                              #   in Loop: Header=BB81_10 Depth=1
	movl	$1, %esi
	cmpl	$2, %ebp
	je	.LBB81_27
.LBB81_19:                              # %.lr.ph.us.preheader.new
                                        #   in Loop: Header=BB81_10 Depth=1
	movq	%r9, %rdx
	subq	%rsi, %rdx
	addq	%rsi, %rcx
	leaq	(%r8,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB81_20:                              # %.lr.ph.us
                                        #   Parent Loop BB81_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$45, (%rcx)
	je	.LBB81_23
# BB#21:                                #   in Loop: Header=BB81_20 Depth=2
	cmpb	$45, -1(%rcx)
	je	.LBB81_23
# BB#22:                                #   in Loop: Header=BB81_20 Depth=2
	movss	-4(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%rsi)
.LBB81_23:                              # %.lr.ph.us.158
                                        #   in Loop: Header=BB81_20 Depth=2
	cmpb	$45, 1(%rcx)
	je	.LBB81_26
# BB#24:                                #   in Loop: Header=BB81_20 Depth=2
	cmpb	$45, (%rcx)
	je	.LBB81_26
# BB#25:                                #   in Loop: Header=BB81_20 Depth=2
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rsi)
.LBB81_26:                              #   in Loop: Header=BB81_20 Depth=2
	addq	$2, %rcx
	addq	$8, %rsi
	addq	$-2, %rdx
	jne	.LBB81_20
.LBB81_27:                              # %._crit_edge.us
                                        #   in Loop: Header=BB81_10 Depth=1
	addss	(%r15,%rax,4), %xmm0
	movss	%xmm0, (%r15,%rax,4)
	incq	%rdi
	cmpq	%r10, %rdi
	jne	.LBB81_10
	jmp	.LBB81_8
.LBB81_4:                               # %.lr.ph37.split.preheader
	movl	%r12d, %ecx
	.p2align	4, 0x90
.LBB81_5:                               # %.lr.ph37.split
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	(%rbx), %rdx
	cmpb	$45, (%rdx)
	je	.LBB81_7
# BB#6:                                 #   in Loop: Header=BB81_5 Depth=1
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r15)
.LBB81_7:                               # %.preheader
                                        #   in Loop: Header=BB81_5 Depth=1
	addss	(%r15,%rax,4), %xmm0
	movss	%xmm0, (%r15,%rax,4)
	addq	$8, %r14
	addq	$8, %rbx
	decq	%rcx
	jne	.LBB81_5
.LBB81_8:                               # %._crit_edge38
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end81:
	.size	getdiaminofreq_st, .Lfunc_end81-getdiaminofreq_st
	.cfi_endproc

	.globl	getdigapfreq_part
	.p2align	4, 0x90
	.type	getdigapfreq_part,@function
getdigapfreq_part:                      # @getdigapfreq_part
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi756:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi757:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi758:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi759:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi760:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi761:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi762:
	.cfi_def_cfa_offset 96
.Lcfi763:
	.cfi_offset %rbx, -56
.Lcfi764:
	.cfi_offset %r12, -48
.Lcfi765:
	.cfi_offset %r13, -40
.Lcfi766:
	.cfi_offset %r14, -32
.Lcfi767:
	.cfi_offset %r15, -24
.Lcfi768:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%rcx, %r15
	movq	%rdx, %r14
	movl	%esi, %ebp
	movq	%rdi, %r12
	movq	%r8, 8(%rsp)            # 8-byte Spill
	cmpl	$-1, %r8d
	jl	.LBB82_2
# BB#1:                                 # %.lr.ph48.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	1(%rax), %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	memset
.LBB82_2:                               # %.preheader41
	testl	%ebp, %ebp
	jle	.LBB82_35
# BB#3:                                 # %.lr.ph44
	movq	96(%rsp), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	$1, %ecx
	movslq	%ecx, %r11
	jle	.LBB82_4
# BB#12:                                # %.lr.ph44.split.us.preheader
	movl	8(%rsp), %ecx           # 4-byte Reload
	movl	%ebp, %edx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	%ecx, %r13d
	andl	$1, %r13d
	leaq	4(%r12), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB82_13:                              # %.lr.ph44.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB82_24 Depth 2
	movsd	(%r15,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	(%r14,%rbp,8), %r9
	cmpb	$45, (%r9)
	jne	.LBB82_16
# BB#14:                                #   in Loop: Header=BB82_13 Depth=1
	cmpb	$45, (%rbx,%rbp)
	jne	.LBB82_16
# BB#15:                                #   in Loop: Header=BB82_13 Depth=1
	movss	(%r12), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r12)
.LBB82_16:                              # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB82_13 Depth=1
	testq	%r13, %r13
	jne	.LBB82_17
# BB#18:                                # %.lr.ph.us.prol
                                        #   in Loop: Header=BB82_13 Depth=1
	cmpb	$45, 1(%r9)
	jne	.LBB82_21
# BB#19:                                #   in Loop: Header=BB82_13 Depth=1
	cmpb	$45, (%r9)
	jne	.LBB82_21
# BB#20:                                #   in Loop: Header=BB82_13 Depth=1
	movss	4(%r12), %xmm1          # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 4(%r12)
.LBB82_21:                              # %.lr.ph.us.prol.loopexit
                                        #   in Loop: Header=BB82_13 Depth=1
	movl	$2, %ecx
	cmpl	$2, 8(%rsp)             # 4-byte Folded Reload
	jne	.LBB82_23
	jmp	.LBB82_31
	.p2align	4, 0x90
.LBB82_17:                              #   in Loop: Header=BB82_13 Depth=1
	movl	$1, %ecx
	cmpl	$2, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB82_31
.LBB82_23:                              # %.lr.ph.us.preheader.new
                                        #   in Loop: Header=BB82_13 Depth=1
	movq	24(%rsp), %rsi          # 8-byte Reload
	subq	%rcx, %rsi
	leaq	(%r9,%rcx), %rdi
	movq	%rbx, %r10
	movq	%r12, %rbx
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,4), %r8
	movq	%r10, %rbx
	.p2align	4, 0x90
.LBB82_24:                              # %.lr.ph.us
                                        #   Parent Loop BB82_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$45, (%rdi)
	jne	.LBB82_27
# BB#25:                                #   in Loop: Header=BB82_24 Depth=2
	cmpb	$45, -1(%rdi)
	jne	.LBB82_27
# BB#26:                                #   in Loop: Header=BB82_24 Depth=2
	movss	-4(%r8), %xmm1          # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%r8)
.LBB82_27:                              # %.lr.ph.us.165
                                        #   in Loop: Header=BB82_24 Depth=2
	cmpb	$45, 1(%rdi)
	jne	.LBB82_30
# BB#28:                                #   in Loop: Header=BB82_24 Depth=2
	cmpb	$45, (%rdi)
	jne	.LBB82_30
# BB#29:                                #   in Loop: Header=BB82_24 Depth=2
	movss	(%r8), %xmm1            # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r8)
.LBB82_30:                              #   in Loop: Header=BB82_24 Depth=2
	addq	$2, %rdi
	addq	$8, %r8
	addq	$-2, %rsi
	jne	.LBB82_24
.LBB82_31:                              # %._crit_edge.us
                                        #   in Loop: Header=BB82_13 Depth=1
	cmpb	$45, (%rax,%rbp)
	jne	.LBB82_34
# BB#32:                                #   in Loop: Header=BB82_13 Depth=1
	cmpb	$45, -1(%r9,%r11)
	jne	.LBB82_34
# BB#33:                                #   in Loop: Header=BB82_13 Depth=1
	addss	(%r12,%r11,4), %xmm0
	movss	%xmm0, (%r12,%r11,4)
.LBB82_34:                              #   in Loop: Header=BB82_13 Depth=1
	incq	%rbp
	cmpq	32(%rsp), %rbp          # 8-byte Folded Reload
	jne	.LBB82_13
	jmp	.LBB82_35
.LBB82_4:                               # %.lr.ph44.split.preheader
	movl	%ebp, %edx
	.p2align	4, 0x90
.LBB82_5:                               # %.lr.ph44.split
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	(%r14), %rsi
	cmpb	$45, (%rsi)
	jne	.LBB82_8
# BB#6:                                 #   in Loop: Header=BB82_5 Depth=1
	cmpb	$45, (%rbx)
	jne	.LBB82_8
# BB#7:                                 #   in Loop: Header=BB82_5 Depth=1
	movss	(%r12), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r12)
.LBB82_8:                               # %.preheader
                                        #   in Loop: Header=BB82_5 Depth=1
	cmpb	$45, (%rax)
	jne	.LBB82_11
# BB#9:                                 #   in Loop: Header=BB82_5 Depth=1
	cmpb	$45, -1(%rsi,%r11)
	jne	.LBB82_11
# BB#10:                                #   in Loop: Header=BB82_5 Depth=1
	addss	(%r12,%r11,4), %xmm0
	movss	%xmm0, (%r12,%r11,4)
.LBB82_11:                              #   in Loop: Header=BB82_5 Depth=1
	addq	$8, %r15
	addq	$8, %r14
	incq	%rbx
	incq	%rax
	decq	%rdx
	jne	.LBB82_5
.LBB82_35:                              # %._crit_edge45
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end82:
	.size	getdigapfreq_part, .Lfunc_end82-getdigapfreq_part
	.cfi_endproc

	.globl	getdiaminofreq_part
	.p2align	4, 0x90
	.type	getdiaminofreq_part,@function
getdiaminofreq_part:                    # @getdiaminofreq_part
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi769:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi770:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi771:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi772:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi773:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi774:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi775:
	.cfi_def_cfa_offset 96
.Lcfi776:
	.cfi_offset %rbx, -56
.Lcfi777:
	.cfi_offset %r12, -48
.Lcfi778:
	.cfi_offset %r13, -40
.Lcfi779:
	.cfi_offset %r14, -32
.Lcfi780:
	.cfi_offset %r15, -24
.Lcfi781:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%rcx, %r15
	movq	%rdx, %r14
	movl	%esi, %ebp
	movq	%rdi, %r12
	movq	%r8, 8(%rsp)            # 8-byte Spill
	cmpl	$-1, %r8d
	jl	.LBB83_2
# BB#1:                                 # %.lr.ph48.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	1(%rax), %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	memset
.LBB83_2:                               # %.preheader41
	testl	%ebp, %ebp
	jle	.LBB83_35
# BB#3:                                 # %.lr.ph44
	movq	96(%rsp), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	$1, %ecx
	movslq	%ecx, %r11
	jle	.LBB83_4
# BB#12:                                # %.lr.ph44.split.us.preheader
	movl	8(%rsp), %ecx           # 4-byte Reload
	movl	%ebp, %edx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	%ecx, %r13d
	andl	$1, %r13d
	leaq	4(%r12), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB83_13:                              # %.lr.ph44.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB83_24 Depth 2
	movsd	(%r15,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	(%r14,%rbp,8), %r9
	cmpb	$45, (%r9)
	je	.LBB83_16
# BB#14:                                #   in Loop: Header=BB83_13 Depth=1
	cmpb	$45, (%rbx,%rbp)
	je	.LBB83_16
# BB#15:                                #   in Loop: Header=BB83_13 Depth=1
	movss	(%r12), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r12)
.LBB83_16:                              # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB83_13 Depth=1
	testq	%r13, %r13
	jne	.LBB83_17
# BB#18:                                # %.lr.ph.us.prol
                                        #   in Loop: Header=BB83_13 Depth=1
	cmpb	$45, 1(%r9)
	je	.LBB83_21
# BB#19:                                #   in Loop: Header=BB83_13 Depth=1
	cmpb	$45, (%r9)
	je	.LBB83_21
# BB#20:                                #   in Loop: Header=BB83_13 Depth=1
	movss	4(%r12), %xmm1          # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 4(%r12)
.LBB83_21:                              # %.lr.ph.us.prol.loopexit
                                        #   in Loop: Header=BB83_13 Depth=1
	movl	$2, %ecx
	cmpl	$2, 8(%rsp)             # 4-byte Folded Reload
	jne	.LBB83_23
	jmp	.LBB83_31
	.p2align	4, 0x90
.LBB83_17:                              #   in Loop: Header=BB83_13 Depth=1
	movl	$1, %ecx
	cmpl	$2, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB83_31
.LBB83_23:                              # %.lr.ph.us.preheader.new
                                        #   in Loop: Header=BB83_13 Depth=1
	movq	24(%rsp), %rsi          # 8-byte Reload
	subq	%rcx, %rsi
	leaq	(%r9,%rcx), %rdi
	movq	%rbx, %r10
	movq	%r12, %rbx
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,4), %r8
	movq	%r10, %rbx
	.p2align	4, 0x90
.LBB83_24:                              # %.lr.ph.us
                                        #   Parent Loop BB83_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$45, (%rdi)
	je	.LBB83_27
# BB#25:                                #   in Loop: Header=BB83_24 Depth=2
	cmpb	$45, -1(%rdi)
	je	.LBB83_27
# BB#26:                                #   in Loop: Header=BB83_24 Depth=2
	movss	-4(%r8), %xmm1          # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%r8)
.LBB83_27:                              # %.lr.ph.us.165
                                        #   in Loop: Header=BB83_24 Depth=2
	cmpb	$45, 1(%rdi)
	je	.LBB83_30
# BB#28:                                #   in Loop: Header=BB83_24 Depth=2
	cmpb	$45, (%rdi)
	je	.LBB83_30
# BB#29:                                #   in Loop: Header=BB83_24 Depth=2
	movss	(%r8), %xmm1            # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r8)
.LBB83_30:                              #   in Loop: Header=BB83_24 Depth=2
	addq	$2, %rdi
	addq	$8, %r8
	addq	$-2, %rsi
	jne	.LBB83_24
.LBB83_31:                              # %._crit_edge.us
                                        #   in Loop: Header=BB83_13 Depth=1
	cmpb	$45, (%rax,%rbp)
	je	.LBB83_34
# BB#32:                                #   in Loop: Header=BB83_13 Depth=1
	cmpb	$45, -1(%r9,%r11)
	je	.LBB83_34
# BB#33:                                #   in Loop: Header=BB83_13 Depth=1
	addss	(%r12,%r11,4), %xmm0
	movss	%xmm0, (%r12,%r11,4)
.LBB83_34:                              #   in Loop: Header=BB83_13 Depth=1
	incq	%rbp
	cmpq	32(%rsp), %rbp          # 8-byte Folded Reload
	jne	.LBB83_13
	jmp	.LBB83_35
.LBB83_4:                               # %.lr.ph44.split.preheader
	movl	%ebp, %edx
	.p2align	4, 0x90
.LBB83_5:                               # %.lr.ph44.split
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	(%r14), %rsi
	cmpb	$45, (%rsi)
	je	.LBB83_8
# BB#6:                                 #   in Loop: Header=BB83_5 Depth=1
	cmpb	$45, (%rbx)
	je	.LBB83_8
# BB#7:                                 #   in Loop: Header=BB83_5 Depth=1
	movss	(%r12), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r12)
.LBB83_8:                               # %.preheader
                                        #   in Loop: Header=BB83_5 Depth=1
	cmpb	$45, (%rax)
	je	.LBB83_11
# BB#9:                                 #   in Loop: Header=BB83_5 Depth=1
	cmpb	$45, -1(%rsi,%r11)
	je	.LBB83_11
# BB#10:                                #   in Loop: Header=BB83_5 Depth=1
	addss	(%r12,%r11,4), %xmm0
	movss	%xmm0, (%r12,%r11,4)
.LBB83_11:                              #   in Loop: Header=BB83_5 Depth=1
	addq	$8, %r15
	addq	$8, %r14
	incq	%rbx
	incq	%rax
	decq	%rdx
	jne	.LBB83_5
.LBB83_35:                              # %._crit_edge45
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end83:
	.size	getdiaminofreq_part, .Lfunc_end83-getdiaminofreq_part
	.cfi_endproc

	.globl	getgapfreq_zure_part
	.p2align	4, 0x90
	.type	getgapfreq_zure_part,@function
getgapfreq_zure_part:                   # @getgapfreq_zure_part
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi782:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi783:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi784:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi785:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi786:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi787:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi788:
	.cfi_def_cfa_offset 64
.Lcfi789:
	.cfi_offset %rbx, -56
.Lcfi790:
	.cfi_offset %r12, -48
.Lcfi791:
	.cfi_offset %r13, -40
.Lcfi792:
	.cfi_offset %r14, -32
.Lcfi793:
	.cfi_offset %r15, -24
.Lcfi794:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movl	%r8d, %r12d
	movq	%rcx, %r15
	movq	%rdx, %r13
	movl	%esi, %ebp
	movq	%rdi, %rbx
	cmpl	$-1, %r12d
	jl	.LBB84_2
# BB#1:                                 # %.lr.ph35.preheader
	leal	1(%r12), %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	memset
.LBB84_2:                               # %.preheader28
	testl	%ebp, %ebp
	jle	.LBB84_16
# BB#3:                                 # %.lr.ph31
	testl	%r12d, %r12d
	jle	.LBB84_4
# BB#17:                                # %.lr.ph31.split.us.preheader
	movl	%r12d, %r9d
	movl	%ebp, %r10d
	movl	%r9d, %edx
	andl	$1, %edx
	leaq	8(%rbx), %r8
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB84_18:                              # %.lr.ph31.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB84_22 Depth 2
	movsd	(%r15,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	cmpb	$45, (%r14,%rdi)
	jne	.LBB84_20
# BB#19:                                #   in Loop: Header=BB84_18 Depth=1
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rbx)
.LBB84_20:                              # %.lr.ph.us
                                        #   in Loop: Header=BB84_18 Depth=1
	testq	%rdx, %rdx
	movq	(%r13,%rdi,8), %rbp
	jne	.LBB84_28
# BB#21:                                #   in Loop: Header=BB84_18 Depth=1
	xorl	%ecx, %ecx
	cmpl	$1, %r12d
	jne	.LBB84_32
	jmp	.LBB84_27
	.p2align	4, 0x90
.LBB84_28:                              #   in Loop: Header=BB84_18 Depth=1
	cmpb	$45, (%rbp)
	jne	.LBB84_30
# BB#29:                                #   in Loop: Header=BB84_18 Depth=1
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 4(%rbx)
.LBB84_30:                              # %.prol.loopexit
                                        #   in Loop: Header=BB84_18 Depth=1
	movl	$1, %ecx
	cmpl	$1, %r12d
	je	.LBB84_27
.LBB84_32:                              # %.lr.ph.us.new
                                        #   in Loop: Header=BB84_18 Depth=1
	movq	%r9, %rax
	subq	%rcx, %rax
	leaq	(%r8,%rcx,4), %rsi
	leaq	1(%rbp,%rcx), %rbp
	.p2align	4, 0x90
.LBB84_22:                              #   Parent Loop BB84_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$45, -1(%rbp)
	jne	.LBB84_24
# BB#23:                                #   in Loop: Header=BB84_22 Depth=2
	movss	-4(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%rsi)
.LBB84_24:                              # %._crit_edge
                                        #   in Loop: Header=BB84_22 Depth=2
	cmpb	$45, (%rbp)
	jne	.LBB84_26
# BB#25:                                #   in Loop: Header=BB84_22 Depth=2
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rsi)
.LBB84_26:                              # %._crit_edge.1
                                        #   in Loop: Header=BB84_22 Depth=2
	addq	$8, %rsi
	addq	$2, %rbp
	addq	$-2, %rax
	jne	.LBB84_22
.LBB84_27:                              # %._crit_edge.us
                                        #   in Loop: Header=BB84_18 Depth=1
	incq	%rdi
	cmpq	%r10, %rdi
	jne	.LBB84_18
	jmp	.LBB84_16
.LBB84_4:                               # %.lr.ph31.split.preheader
	movl	%ebp, %eax
	testb	$1, %al
	jne	.LBB84_6
# BB#5:
	xorl	%edx, %edx
	cmpl	$1, %ebp
	jne	.LBB84_10
	jmp	.LBB84_16
.LBB84_6:                               # %.lr.ph31.split.prol
	cmpb	$45, (%r14)
	jne	.LBB84_8
# BB#7:
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	addss	(%rbx), %xmm0
	movss	%xmm0, (%rbx)
.LBB84_8:                               # %.lr.ph31.split.prol.loopexit
	movl	$1, %edx
	cmpl	$1, %ebp
	je	.LBB84_16
.LBB84_10:                              # %.lr.ph31.split.preheader.new
	subq	%rdx, %rax
	leaq	8(%r15,%rdx,8), %rcx
	leaq	1(%r14,%rdx), %rdx
	.p2align	4, 0x90
.LBB84_11:                              # %.lr.ph31.split
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$45, -1(%rdx)
	jne	.LBB84_13
# BB#12:                                #   in Loop: Header=BB84_11 Depth=1
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	addss	(%rbx), %xmm0
	movss	%xmm0, (%rbx)
.LBB84_13:                              # %.preheader
                                        #   in Loop: Header=BB84_11 Depth=1
	cmpb	$45, (%rdx)
	jne	.LBB84_15
# BB#14:                                #   in Loop: Header=BB84_11 Depth=1
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	addss	(%rbx), %xmm0
	movss	%xmm0, (%rbx)
.LBB84_15:                              # %.preheader.1
                                        #   in Loop: Header=BB84_11 Depth=1
	addq	$16, %rcx
	addq	$2, %rdx
	addq	$-2, %rax
	jne	.LBB84_11
.LBB84_16:                              # %._crit_edge32
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end84:
	.size	getgapfreq_zure_part, .Lfunc_end84-getgapfreq_zure_part
	.cfi_endproc

	.globl	getgapfreq_zure
	.p2align	4, 0x90
	.type	getgapfreq_zure,@function
getgapfreq_zure:                        # @getgapfreq_zure
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi795:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi796:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi797:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi798:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi799:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi800:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi801:
	.cfi_def_cfa_offset 64
.Lcfi802:
	.cfi_offset %rbx, -56
.Lcfi803:
	.cfi_offset %r12, -48
.Lcfi804:
	.cfi_offset %r13, -40
.Lcfi805:
	.cfi_offset %r14, -32
.Lcfi806:
	.cfi_offset %r15, -24
.Lcfi807:
	.cfi_offset %rbp, -16
	movl	%r8d, %r13d
	movq	%rcx, %r14
	movq	%rdx, %r12
	movl	%esi, %ebp
	movq	%rdi, %r15
	testl	%r13d, %r13d
	js	.LBB85_2
# BB#1:                                 # %.lr.ph32.preheader
	movl	%r13d, %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	memset
.LBB85_2:                               # %.preheader
	leal	1(%r13), %r8d
	testl	%ebp, %ebp
	jle	.LBB85_18
# BB#3:                                 # %.preheader
	testl	%r13d, %r13d
	jle	.LBB85_18
# BB#4:                                 # %.lr.ph28.split.us.preheader
	movl	%r13d, %r9d
	movl	%ebp, %edx
	movl	%r9d, %esi
	andl	$1, %esi
	leaq	8(%r15), %r10
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB85_5:                               # %.lr.ph28.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB85_12 Depth 2
	testq	%rsi, %rsi
	movsd	(%r14,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	(%r12,%rbp,8), %rdi
	jne	.LBB85_7
# BB#6:                                 #   in Loop: Header=BB85_5 Depth=1
	xorl	%ebx, %ebx
	cmpl	$1, %r13d
	jne	.LBB85_11
	jmp	.LBB85_17
	.p2align	4, 0x90
.LBB85_7:                               #   in Loop: Header=BB85_5 Depth=1
	cmpb	$45, (%rdi)
	jne	.LBB85_9
# BB#8:                                 #   in Loop: Header=BB85_5 Depth=1
	movss	4(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 4(%r15)
.LBB85_9:                               # %.prol.loopexit
                                        #   in Loop: Header=BB85_5 Depth=1
	movl	$1, %ebx
	cmpl	$1, %r13d
	je	.LBB85_17
.LBB85_11:                              # %.lr.ph28.split.us.new
                                        #   in Loop: Header=BB85_5 Depth=1
	movq	%r9, %rcx
	subq	%rbx, %rcx
	leaq	(%r10,%rbx,4), %rax
	leaq	1(%rdi,%rbx), %rdi
	.p2align	4, 0x90
.LBB85_12:                              #   Parent Loop BB85_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$45, -1(%rdi)
	jne	.LBB85_14
# BB#13:                                #   in Loop: Header=BB85_12 Depth=2
	movss	-4(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%rax)
.LBB85_14:                              # %._crit_edge
                                        #   in Loop: Header=BB85_12 Depth=2
	cmpb	$45, (%rdi)
	jne	.LBB85_16
# BB#15:                                #   in Loop: Header=BB85_12 Depth=2
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rax)
.LBB85_16:                              # %._crit_edge.1
                                        #   in Loop: Header=BB85_12 Depth=2
	addq	$8, %rax
	addq	$2, %rdi
	addq	$-2, %rcx
	jne	.LBB85_12
.LBB85_17:                              # %._crit_edge.us
                                        #   in Loop: Header=BB85_5 Depth=1
	incq	%rbp
	cmpq	%rdx, %rbp
	jne	.LBB85_5
.LBB85_18:                              # %._crit_edge29
	movslq	%r8d, %rax
	movl	$0, (%r15,%rax,4)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end85:
	.size	getgapfreq_zure, .Lfunc_end85-getgapfreq_zure
	.cfi_endproc

	.globl	getgapfreq
	.p2align	4, 0x90
	.type	getgapfreq,@function
getgapfreq:                             # @getgapfreq
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi808:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi809:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi810:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi811:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi812:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi813:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi814:
	.cfi_def_cfa_offset 64
.Lcfi815:
	.cfi_offset %rbx, -56
.Lcfi816:
	.cfi_offset %r12, -48
.Lcfi817:
	.cfi_offset %r13, -40
.Lcfi818:
	.cfi_offset %r14, -32
.Lcfi819:
	.cfi_offset %r15, -24
.Lcfi820:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movq	%rcx, %r14
	movq	%rdx, %r12
	movl	%esi, %r13d
	movq	%rdi, %r15
	testl	%ebx, %ebx
	js	.LBB86_2
# BB#1:                                 # %.lr.ph32.preheader
	movl	%ebx, %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	memset
.LBB86_2:                               # %.preheader
	testl	%r13d, %r13d
	jle	.LBB86_18
# BB#3:                                 # %.preheader
	testl	%ebx, %ebx
	jle	.LBB86_18
# BB#4:                                 # %.lr.ph28.split.us.preheader
	movl	%ebx, %r8d
	movl	%r13d, %r10d
	movl	%r8d, %edx
	andl	$1, %edx
	leaq	4(%r15), %r9
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB86_5:                               # %.lr.ph28.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB86_12 Depth 2
	testq	%rdx, %rdx
	movsd	(%r14,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	(%r12,%rdi,8), %rsi
	jne	.LBB86_7
# BB#6:                                 #   in Loop: Header=BB86_5 Depth=1
	xorl	%ecx, %ecx
	cmpl	$1, %ebx
	jne	.LBB86_11
	jmp	.LBB86_17
	.p2align	4, 0x90
.LBB86_7:                               #   in Loop: Header=BB86_5 Depth=1
	cmpb	$45, (%rsi)
	jne	.LBB86_9
# BB#8:                                 #   in Loop: Header=BB86_5 Depth=1
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r15)
.LBB86_9:                               # %.prol.loopexit
                                        #   in Loop: Header=BB86_5 Depth=1
	movl	$1, %ecx
	cmpl	$1, %ebx
	je	.LBB86_17
.LBB86_11:                              # %.lr.ph28.split.us.new
                                        #   in Loop: Header=BB86_5 Depth=1
	movq	%r8, %rax
	subq	%rcx, %rax
	leaq	1(%rsi,%rcx), %rbp
	leaq	(%r9,%rcx,4), %rsi
	.p2align	4, 0x90
.LBB86_12:                              #   Parent Loop BB86_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$45, -1(%rbp)
	jne	.LBB86_14
# BB#13:                                #   in Loop: Header=BB86_12 Depth=2
	movss	-4(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%rsi)
.LBB86_14:                              #   in Loop: Header=BB86_12 Depth=2
	cmpb	$45, (%rbp)
	jne	.LBB86_16
# BB#15:                                #   in Loop: Header=BB86_12 Depth=2
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rsi)
.LBB86_16:                              #   in Loop: Header=BB86_12 Depth=2
	addq	$2, %rbp
	addq	$8, %rsi
	addq	$-2, %rax
	jne	.LBB86_12
.LBB86_17:                              # %._crit_edge.us
                                        #   in Loop: Header=BB86_5 Depth=1
	incq	%rdi
	cmpq	%r10, %rdi
	jne	.LBB86_5
.LBB86_18:                              # %._crit_edge29
	movslq	%ebx, %rax
	movl	$0, (%r15,%rax,4)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end86:
	.size	getgapfreq, .Lfunc_end86-getgapfreq
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI87_0:
	.long	1065353216              # float 1
	.text
	.globl	st_getGapPattern
	.p2align	4, 0x90
	.type	st_getGapPattern,@function
st_getGapPattern:                       # @st_getGapPattern
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi821:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi822:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi823:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi824:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi825:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi826:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi827:
	.cfi_def_cfa_offset 112
.Lcfi828:
	.cfi_offset %rbx, -56
.Lcfi829:
	.cfi_offset %r12, -48
.Lcfi830:
	.cfi_offset %r13, -40
.Lcfi831:
	.cfi_offset %r14, -32
.Lcfi832:
	.cfi_offset %r15, -24
.Lcfi833:
	.cfi_offset %rbp, -16
	movl	%r8d, %r15d
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movl	%esi, %r13d
	movq	%rdi, (%rsp)            # 8-byte Spill
	cmpl	$-1, %r15d
	je	.LBB87_5
# BB#1:                                 # %.lr.ph157.preheader
	movl	%r15d, %ebx
	notl	%ebx
	movq	(%rsp), %rbp            # 8-byte Reload
	.p2align	4, 0x90
.LBB87_2:                               # %.lr.ph157
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB87_4
# BB#3:                                 #   in Loop: Header=BB87_2 Depth=1
	callq	free
.LBB87_4:                               #   in Loop: Header=BB87_2 Depth=1
	movq	$0, (%rbp)
	addq	$8, %rbp
	incl	%ebx
	jne	.LBB87_2
.LBB87_5:                               # %.preheader131
	testl	%r13d, %r13d
	jle	.LBB87_13
# BB#6:                                 # %.lr.ph154
	testl	%r15d, %r15d
	js	.LBB87_7
# BB#18:                                # %.lr.ph154.split.preheader
	movslq	%r13d, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB87_19:                              # %.lr.ph154.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB87_20 Depth 2
                                        #       Child Loop BB87_28 Depth 3
	movq	32(%rsp), %rax          # 8-byte Reload
	movsd	(%rax,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movq	(%rsp), %r14            # 8-byte Reload
	movq	$0, (%r14)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	(%rcx,%rdx,8), %rdx
	xorl	%ebp, %ebp
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	jmp	.LBB87_20
.LBB87_33:                              # %.loopexit130.loopexit
                                        #   in Loop: Header=BB87_20 Depth=2
	decl	%ebx
	jmp	.LBB87_34
	.p2align	4, 0x90
.LBB87_20:                              #   Parent Loop BB87_19 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB87_28 Depth 3
	xorl	%esi, %esi
	cmpl	%r15d, %r13d
	je	.LBB87_23
# BB#21:                                #   in Loop: Header=BB87_20 Depth=2
	xorl	%esi, %esi
	cmpb	$45, (%rdx)
	leaq	1(%rdx), %rdx
	sete	%sil
	jne	.LBB87_23
# BB#22:                                #   in Loop: Header=BB87_20 Depth=2
	incl	%ebp
	jmp	.LBB87_35
	.p2align	4, 0x90
.LBB87_23:                              # %.thread
                                        #   in Loop: Header=BB87_20 Depth=2
	testl	%eax, %eax
	je	.LBB87_35
# BB#24:                                # %.thread
                                        #   in Loop: Header=BB87_20 Depth=2
	testl	%ebp, %ebp
	je	.LBB87_35
# BB#25:                                #   in Loop: Header=BB87_20 Depth=2
	movq	(%r14), %rax
	movl	$1, %ebx
	testq	%rax, %rax
	je	.LBB87_31
# BB#26:                                # %.preheader129
                                        #   in Loop: Header=BB87_20 Depth=2
	movl	8(%rax), %ecx
	cmpl	$-1, %ecx
	je	.LBB87_31
# BB#27:                                # %.lr.ph141.preheader
                                        #   in Loop: Header=BB87_20 Depth=2
	movl	$2, %ebx
	.p2align	4, 0x90
.LBB87_28:                              # %.lr.ph141
                                        #   Parent Loop BB87_19 Depth=1
                                        #     Parent Loop BB87_20 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%ebp, %ecx
	je	.LBB87_33
# BB#29:                                #   in Loop: Header=BB87_28 Depth=3
	movl	(%rax,%rbx,8), %ecx
	incq	%rbx
	cmpl	$-1, %ecx
	jne	.LBB87_28
# BB#30:                                # %.loopexit.loopexit
                                        #   in Loop: Header=BB87_20 Depth=2
	decl	%ebx
.LBB87_31:                              # %.loopexit
                                        #   in Loop: Header=BB87_20 Depth=2
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movslq	%ebx, %r12
	leaq	24(,%r12,8), %rsi
	movq	%rax, %rdi
	callq	realloc
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.LBB87_43
# BB#32:                                #   in Loop: Header=BB87_20 Depth=2
	movl	$0, 4(%rax,%r12,8)
	movl	%ebp, (%rax,%r12,8)
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	movq	%rcx, 8(%rax,%r12,8)
	movq	48(%rsp), %rdx          # 8-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
.LBB87_34:                              # %.loopexit130
                                        #   in Loop: Header=BB87_20 Depth=2
	movslq	%ebx, %rcx
	movss	4(%rax,%rcx,8), %xmm0   # xmm0 = mem[0],zero,zero,zero
	addss	8(%rsp), %xmm0          # 4-byte Folded Reload
	movss	%xmm0, 4(%rax,%rcx,8)
	xorl	%ebp, %ebp
.LBB87_35:                              #   in Loop: Header=BB87_20 Depth=2
	addq	$8, %r14
	cmpl	%r15d, %r13d
	leal	1(%r13), %eax
	movl	%eax, %r13d
	movl	%esi, %eax
	jl	.LBB87_20
# BB#36:                                # %._crit_edge152
                                        #   in Loop: Header=BB87_19 Depth=1
	movq	40(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	cmpq	16(%rsp), %rdx          # 8-byte Folded Reload
	jl	.LBB87_19
	jmp	.LBB87_13
.LBB87_7:                               # %.lr.ph154.split.us.preheader
	leal	-1(%r13), %eax
	movl	%r13d, %edx
	xorl	%ecx, %ecx
	andl	$7, %edx
	je	.LBB87_9
	.p2align	4, 0x90
.LBB87_8:                               # %.lr.ph154.split.us.prol
                                        # =>This Inner Loop Header: Depth=1
	incl	%ecx
	cmpl	%ecx, %edx
	jne	.LBB87_8
.LBB87_9:                               # %.lr.ph154.split.us.prol.loopexit
	cmpl	$7, %eax
	jb	.LBB87_12
# BB#10:                                # %.lr.ph154.split.us.preheader.new
	subl	%ecx, %r13d
	.p2align	4, 0x90
.LBB87_11:                              # %.lr.ph154.split.us
                                        # =>This Inner Loop Header: Depth=1
	addl	$-8, %r13d
	jne	.LBB87_11
.LBB87_12:                              # %.preheader.loopexit
	movq	(%rsp), %rax            # 8-byte Reload
	movq	$0, (%rax)
.LBB87_13:                              # %.preheader
	testl	%r15d, %r15d
	movq	(%rsp), %rbx            # 8-byte Reload
	js	.LBB87_42
# BB#14:                                # %.lr.ph138.preheader
	incl	%r15d
	xorl	%ebp, %ebp
	movabsq	$4294967296, %r14       # imm = 0x100000000
	.p2align	4, 0x90
.LBB87_15:                              # %.lr.ph138
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB87_38 Depth 2
	movq	(%rbx,%rbp,8), %rcx
	testq	%rcx, %rcx
	je	.LBB87_40
# BB#16:                                #   in Loop: Header=BB87_15 Depth=1
	movq	$0, (%rcx)
	cmpl	$-1, 8(%rcx)
	je	.LBB87_17
# BB#37:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB87_15 Depth=1
	xorps	%xmm0, %xmm0
	movl	$1, %eax
	.p2align	4, 0x90
.LBB87_38:                              # %.lr.ph
                                        #   Parent Loop BB87_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addss	4(%rcx,%rax,8), %xmm0
	movss	%xmm0, 4(%rcx)
	cmpl	$-1, 8(%rcx,%rax,8)
	leaq	1(%rax), %rax
	jne	.LBB87_38
	jmp	.LBB87_39
	.p2align	4, 0x90
.LBB87_40:                              #   in Loop: Header=BB87_15 Depth=1
	movl	$3, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%rbx,%rbp,8)
	movl	$1065353216, 12(%rax)   # imm = 0x3F800000
	addq	$16, %rax
	jmp	.LBB87_41
	.p2align	4, 0x90
.LBB87_17:                              #   in Loop: Header=BB87_15 Depth=1
	xorps	%xmm0, %xmm0
	movl	$1, %eax
.LBB87_39:                              # %._crit_edge
                                        #   in Loop: Header=BB87_15 Depth=1
	movss	.LCPI87_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 4(%rcx,%rax,8)
	movl	$0, (%rcx,%rax,8)
	shlq	$32, %rax
	addq	%r14, %rax
	sarq	$29, %rax
	addq	%rcx, %rax
.LBB87_41:                              #   in Loop: Header=BB87_15 Depth=1
	movl	$-1, (%rax)
	incq	%rbp
	cmpq	%r15, %rbp
	jne	.LBB87_15
.LBB87_42:                              # %._crit_edge139
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB87_43:                              # %.us-lcssa
	movq	stderr(%rip), %rcx
	movl	$.L.str.34, %edi
	movl	$29, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.35, %edi
	movl	$53, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end87:
	.size	st_getGapPattern, .Lfunc_end87-st_getGapPattern
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI88_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	naiveRpairscore
	.p2align	4, 0x90
	.type	naiveRpairscore,@function
naiveRpairscore:                        # @naiveRpairscore
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi834:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi835:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi836:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi837:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi838:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi839:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi840:
	.cfi_def_cfa_offset 128
.Lcfi841:
	.cfi_offset %rbx, -56
.Lcfi842:
	.cfi_offset %r12, -48
.Lcfi843:
	.cfi_offset %r13, -40
.Lcfi844:
	.cfi_offset %r14, -32
.Lcfi845:
	.cfi_offset %r15, -24
.Lcfi846:
	.cfi_offset %rbp, -16
	movq	%r9, 8(%rsp)            # 8-byte Spill
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	testl	%edi, %edi
	jle	.LBB88_1
# BB#2:                                 # %.preheader.lr.ph
	movl	128(%rsp), %eax
	leal	(%rax,%rax), %r12d
	movl	%esi, %ecx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	%edi, %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	xorps	%xmm7, %xmm7
	xorl	%ebx, %ebx
	movsd	.LCPI88_0(%rip), %xmm0  # xmm0 = mem[0],zero
	xorpd	%xmm1, %xmm1
	xorl	%edx, %edx
	movl	%esi, 20(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB88_3:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB88_5 Depth 2
                                        #       Child Loop BB88_9 Depth 3
	testl	%esi, %esi
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	jle	.LBB88_23
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB88_3 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%rdx,8), %xmm2    # xmm2 = mem[0],zero
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rdx,8), %r11
	movb	(%r11), %r9b
	movsbq	%r9b, %rsi
	incq	%r11
	movl	%r9d, %edx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB88_5:                               #   Parent Loop BB88_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB88_9 Depth 3
	movq	8(%rsp), %rcx           # 8-byte Reload
	movsd	(%rcx,%rdi,8), %xmm3    # xmm3 = mem[0],zero
	mulsd	%xmm2, %xmm3
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rdi,8), %r14
	movb	(%r14), %r15b
	movsbq	%r15b, %rbp
	xorl	%ecx, %ecx
	cmpb	$45, %r9b
	jne	.LBB88_7
# BB#6:                                 #   in Loop: Header=BB88_5 Depth=2
	cmpb	$45, %r15b
	movl	%eax, %ecx
	cmovel	%ebx, %ecx
	movb	$45, %r8b
	cmpb	$45, %dl
	je	.LBB88_8
.LBB88_7:                               # %.thread
                                        #   in Loop: Header=BB88_5 Depth=2
	cmpb	$45, %r15b
	cmovel	%eax, %ecx
	movl	%edx, %r8d
.LBB88_8:                               #   in Loop: Header=BB88_5 Depth=2
	movq	%rsi, %rdx
	shlq	$9, %rdx
	cvtsi2ssl	amino_dis(%rdx,%rbp,4), %xmm4
	cvtss2sd	%xmm4, %xmm5
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%ecx, %xmm4
	mulsd	%xmm0, %xmm4
	addsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	incq	%r14
	movq	%r11, %r13
	movb	%r9b, %dl
	jmp	.LBB88_9
	.p2align	4, 0x90
.LBB88_21:                              # %.thread120.thread
                                        #   in Loop: Header=BB88_9 Depth=3
	movsbq	(%r13), %rdx
	movsbq	(%r14), %r15
	movq	%rdx, %rcx
	shlq	$9, %rcx
	xorps	%xmm5, %xmm5
	cvtsi2sdl	amino_dis(%rcx,%r15,4), %xmm5
	cvtsi2sdl	%r10d, %xmm6
	mulsd	%xmm0, %xmm6
	addsd	%xmm5, %xmm6
	cvtss2sd	%xmm4, %xmm4
	addsd	%xmm6, %xmm4
	incq	%r13
	incq	%r14
.LBB88_9:                               #   Parent Loop BB88_3 Depth=1
                                        #     Parent Loop BB88_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cvtsd2ss	%xmm4, %xmm4
	cmpb	$45, %dl
	je	.LBB88_14
# BB#10:                                #   in Loop: Header=BB88_9 Depth=3
	testb	%dl, %dl
	je	.LBB88_22
# BB#11:                                #   in Loop: Header=BB88_9 Depth=3
	cmpb	$45, %r15b
	movzbl	(%r13), %edx
	movzbl	(%r14), %ecx
	jne	.LBB88_12
# BB#18:                                # %.thread120
                                        #   in Loop: Header=BB88_9 Depth=3
	cmpb	$45, %dl
	jne	.LBB88_13
# BB#19:                                # %.thread126
                                        #   in Loop: Header=BB88_9 Depth=3
	cmpb	$45, %cl
	movl	%r12d, %r10d
	cmovel	%ebx, %r10d
	cmovel	%eax, %r10d
	jmp	.LBB88_21
	.p2align	4, 0x90
.LBB88_14:                              # %.thread114
                                        #   in Loop: Header=BB88_9 Depth=3
	movzbl	(%r13), %edx
	movzbl	(%r14), %ecx
	cmpb	$45, %r15b
	jne	.LBB88_15
.LBB88_12:                              #   in Loop: Header=BB88_9 Depth=3
	cmpb	$45, %dl
	jne	.LBB88_16
.LBB88_13:                              #   in Loop: Header=BB88_9 Depth=3
	cmpb	$45, %cl
	movl	%eax, %r10d
	cmovel	%ebx, %r10d
	jmp	.LBB88_21
	.p2align	4, 0x90
.LBB88_15:                              # %.thread131
                                        #   in Loop: Header=BB88_9 Depth=3
	cmpb	$45, %dl
	jne	.LBB88_20
.LBB88_16:                              # %.thread136
                                        #   in Loop: Header=BB88_9 Depth=3
	cmpb	$45, %cl
	movl	$0, %r10d
	cmovel	%eax, %r10d
	jmp	.LBB88_21
.LBB88_20:                              # %.thread134
                                        #   in Loop: Header=BB88_9 Depth=3
	cmpb	$45, %cl
	movl	%eax, %r10d
	cmovel	%r12d, %r10d
	jmp	.LBB88_21
	.p2align	4, 0x90
.LBB88_22:                              #   in Loop: Header=BB88_5 Depth=2
	cvtss2sd	%xmm4, %xmm4
	mulsd	%xmm4, %xmm3
	xorps	%xmm4, %xmm4
	cvtss2sd	%xmm7, %xmm4
	addsd	%xmm3, %xmm4
	xorps	%xmm7, %xmm7
	cvtsd2ss	%xmm4, %xmm7
	incq	%rdi
	cmpq	56(%rsp), %rdi          # 8-byte Folded Reload
	movl	%r8d, %edx
	jne	.LBB88_5
.LBB88_23:                              # %._crit_edge
                                        #   in Loop: Header=BB88_3 Depth=1
	movq	48(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	cmpq	40(%rsp), %rdx          # 8-byte Folded Reload
	movl	20(%rsp), %esi          # 4-byte Reload
	jne	.LBB88_3
	jmp	.LBB88_24
.LBB88_1:
	xorps	%xmm7, %xmm7
.LBB88_24:                              # %._crit_edge142
	movss	%xmm7, 8(%rsp)          # 4-byte Spill
	movq	stderr(%rip), %rdi
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm7, %xmm0
	movl	$.L.str.36, %esi
	movb	$1, %al
	callq	fprintf
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end88:
	.size	naiveRpairscore, .Lfunc_end88-naiveRpairscore
	.cfi_endproc

	.globl	naiveQpairscore
	.p2align	4, 0x90
	.type	naiveQpairscore,@function
naiveQpairscore:                        # @naiveQpairscore
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	retq
.Lfunc_end89:
	.size	naiveQpairscore, .Lfunc_end89-naiveQpairscore
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI90_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	naiveHpairscore
	.p2align	4, 0x90
	.type	naiveHpairscore,@function
naiveHpairscore:                        # @naiveHpairscore
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi847:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi848:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi849:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi850:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi851:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi852:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi853:
	.cfi_def_cfa_offset 128
.Lcfi854:
	.cfi_offset %rbx, -56
.Lcfi855:
	.cfi_offset %r12, -48
.Lcfi856:
	.cfi_offset %r13, -40
.Lcfi857:
	.cfi_offset %r14, -32
.Lcfi858:
	.cfi_offset %r15, -24
.Lcfi859:
	.cfi_offset %rbp, -16
	movq	%rcx, %r10
	movq	%rdx, %r11
	testl	%edi, %edi
	jle	.LBB90_1
# BB#2:                                 # %.preheader.lr.ph
	movl	128(%rsp), %r14d
	movl	%esi, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	cvtsi2sdl	%r14d, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movl	%edi, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorps	%xmm3, %xmm3
	xorl	%r12d, %r12d
	movsd	.LCPI90_0(%rip), %xmm4  # xmm4 = mem[0],zero
	xorpd	%xmm5, %xmm5
	xorl	%ebp, %ebp
	movl	%esi, %edi
	movq	%r9, 40(%rsp)           # 8-byte Spill
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movq	%r10, 24(%rsp)          # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	movl	%edi, 4(%rsp)           # 4-byte Spill
	.p2align	4, 0x90
.LBB90_3:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB90_5 Depth 2
                                        #       Child Loop BB90_12 Depth 3
	testl	%edi, %edi
	jle	.LBB90_25
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB90_3 Depth=1
	xorl	%r13d, %r13d
	jmp	.LBB90_5
.LBB90_10:                              #   in Loop: Header=BB90_5 Depth=2
	movq	stderr(%rip), %rdi
	movapd	%xmm6, %xmm0
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	mulsd	%xmm4, %xmm0
	movl	$.L.str.37, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movb	$1, %al
	movl	%ebp, %edx
	movl	%r13d, %ecx
	movss	%xmm3, (%rsp)           # 4-byte Spill
	movsd	%xmm6, 56(%rsp)         # 8-byte Spill
	callq	fprintf
	movsd	56(%rsp), %xmm6         # 8-byte Reload
                                        # xmm6 = mem[0],zero
	xorpd	%xmm5, %xmm5
	movsd	.LCPI90_0(%rip), %xmm4  # xmm4 = mem[0],zero
	movl	4(%rsp), %edi           # 4-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	movq	24(%rsp), %r10          # 8-byte Reload
	movss	(%rsp), %xmm3           # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	40(%rsp), %r9           # 8-byte Reload
	movb	(%rbx), %al
	movb	(%r15), %cl
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	jmp	.LBB90_11
	.p2align	4, 0x90
.LBB90_5:                               #   Parent Loop BB90_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB90_12 Depth 3
	movsd	(%r8,%rbp,8), %xmm6     # xmm6 = mem[0],zero
	mulsd	(%r9,%r13,8), %xmm6
	movq	(%r11,%rbp,8), %rbx
	movq	(%r10,%r13,8), %r15
	movb	(%rbx), %al
	movb	(%r15), %cl
	cmpb	$45, %al
	jne	.LBB90_8
# BB#6:                                 #   in Loop: Header=BB90_5 Depth=2
	cmpb	$45, %cl
	movl	%ecx, %edx
	jne	.LBB90_9
# BB#7:                                 #   in Loop: Header=BB90_5 Depth=2
	movb	$45, %cl
	xorpd	%xmm0, %xmm0
	jmp	.LBB90_11
	.p2align	4, 0x90
.LBB90_8:                               # %.thread
                                        #   in Loop: Header=BB90_5 Depth=2
	xorpd	%xmm0, %xmm0
	movb	$45, %dl
	cmpb	$45, %cl
	jne	.LBB90_11
.LBB90_9:                               # %select.unfold132
                                        #   in Loop: Header=BB90_5 Depth=2
	xorpd	%xmm0, %xmm0
	testl	%r14d, %r14d
	movb	%dl, %cl
	jne	.LBB90_10
.LBB90_11:                              # %._crit_edge130
                                        #   in Loop: Header=BB90_5 Depth=2
	movsbq	%al, %rdx
	movsbq	%cl, %rsi
	shlq	$9, %rdx
	cvtsi2ssl	amino_dis(%rdx,%rsi,4), %xmm1
	cvtss2sd	%xmm1, %xmm1
	mulsd	%xmm4, %xmm0
	addsd	%xmm1, %xmm0
	addsd	%xmm5, %xmm0
	jmp	.LBB90_12
	.p2align	4, 0x90
.LBB90_23:                              # %.thread111
                                        #   in Loop: Header=BB90_12 Depth=3
	movsbq	(%rbx), %rax
	movsbq	(%r15), %rcx
	movq	%rax, %rsi
	shlq	$9, %rsi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	amino_dis(%rsi,%rcx,4), %xmm1
	cvtsi2sdl	%edx, %xmm2
	mulsd	%xmm4, %xmm2
	addsd	%xmm1, %xmm2
	cvtss2sd	%xmm0, %xmm0
	addsd	%xmm2, %xmm0
.LBB90_12:                              #   Parent Loop BB90_3 Depth=1
                                        #     Parent Loop BB90_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incq	%rbx
	incq	%r15
	cvtsd2ss	%xmm0, %xmm0
	cmpb	$45, %al
	je	.LBB90_19
# BB#13:                                #   in Loop: Header=BB90_12 Depth=3
	testb	%al, %al
	je	.LBB90_24
# BB#14:                                #   in Loop: Header=BB90_12 Depth=3
	cmpb	$45, %cl
	movzbl	(%rbx), %eax
	jne	.LBB90_15
# BB#18:                                #   in Loop: Header=BB90_12 Depth=3
	xorl	%edx, %edx
	cmpb	$45, %al
	je	.LBB90_23
	jmp	.LBB90_21
	.p2align	4, 0x90
.LBB90_19:                              # %.thread113
                                        #   in Loop: Header=BB90_12 Depth=3
	xorl	%edx, %edx
	cmpb	$45, %cl
	je	.LBB90_23
# BB#20:                                #   in Loop: Header=BB90_12 Depth=3
	cmpb	$45, (%rbx)
	je	.LBB90_23
.LBB90_21:                              #   in Loop: Header=BB90_12 Depth=3
	cmpb	$45, (%r15)
	jmp	.LBB90_22
	.p2align	4, 0x90
.LBB90_15:                              #   in Loop: Header=BB90_12 Depth=3
	movzbl	(%r15), %ecx
	cmpb	$45, %al
	jne	.LBB90_17
# BB#16:                                #   in Loop: Header=BB90_12 Depth=3
	cmpb	$45, %cl
.LBB90_22:                              # %.thread111
                                        #   in Loop: Header=BB90_12 Depth=3
	movl	%r14d, %edx
	cmovel	%r12d, %edx
	jmp	.LBB90_23
.LBB90_17:                              # %.thread108
                                        #   in Loop: Header=BB90_12 Depth=3
	cmpb	$45, %cl
	movl	$0, %edx
	cmovel	%r14d, %edx
	jmp	.LBB90_23
	.p2align	4, 0x90
.LBB90_24:                              #   in Loop: Header=BB90_5 Depth=2
	cvtss2sd	%xmm0, %xmm0
	mulsd	%xmm0, %xmm6
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm3, %xmm0
	addsd	%xmm6, %xmm0
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm0, %xmm3
	incq	%r13
	cmpq	64(%rsp), %r13          # 8-byte Folded Reload
	jne	.LBB90_5
.LBB90_25:                              # %._crit_edge
                                        #   in Loop: Header=BB90_3 Depth=1
	incq	%rbp
	cmpq	48(%rsp), %rbp          # 8-byte Folded Reload
	jne	.LBB90_3
	jmp	.LBB90_26
.LBB90_1:
	xorps	%xmm3, %xmm3
.LBB90_26:                              # %._crit_edge119
	movss	%xmm3, (%rsp)           # 4-byte Spill
	movq	stderr(%rip), %rdi
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm3, %xmm0
	movl	$.L.str.36, %esi
	movb	$1, %al
	callq	fprintf
	movss	(%rsp), %xmm0           # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end90:
	.size	naiveHpairscore, .Lfunc_end90-naiveHpairscore
	.cfi_endproc

	.globl	naivepairscore11
	.p2align	4, 0x90
	.type	naivepairscore11,@function
naivepairscore11:                       # @naivepairscore11
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi860:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi861:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi862:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi863:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi864:
	.cfi_def_cfa_offset 48
.Lcfi865:
	.cfi_offset %rbx, -48
.Lcfi866:
	.cfi_offset %r12, -40
.Lcfi867:
	.cfi_offset %r14, -32
.Lcfi868:
	.cfi_offset %r15, -24
.Lcfi869:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	callq	strlen
	shlq	$32, %rax
	movabsq	$4294967296, %r15       # imm = 0x100000000
	addq	%rax, %r15
	sarq	$32, %r15
	movl	$1, %esi
	movq	%r15, %rdi
	callq	calloc
	movq	%rax, %r14
	movl	$1, %esi
	movq	%r15, %rdi
	callq	calloc
	movq	%rax, %r15
	movq	%r14, %rax
	movq	%r15, %rcx
	jmp	.LBB91_1
.LBB91_11:                              #   in Loop: Header=BB91_1 Depth=1
	movb	%dl, (%rax)
	incq	%rax
	movzbl	-1(%rbx), %edx
	movb	%dl, (%rcx)
	incq	%rcx
	.p2align	4, 0x90
.LBB91_1:                               # %.outer.i
                                        # =>This Inner Loop Header: Depth=1
	incq	%rbp
	incq	%rbx
	movzbl	-1(%rbp), %edx
	cmpb	$45, %dl
	jne	.LBB91_2
# BB#9:                                 #   in Loop: Header=BB91_1 Depth=1
	cmpb	$45, -1(%rbx)
	je	.LBB91_1
# BB#10:                                #   in Loop: Header=BB91_1 Depth=1
	movb	$45, %dl
	jmp	.LBB91_11
.LBB91_2:                               #   in Loop: Header=BB91_1 Depth=1
	testb	%dl, %dl
	jne	.LBB91_11
# BB#3:                                 # %commongappickpair.exit
	movb	$0, (%rax)
	movb	$0, (%rcx)
	xorl	%ebx, %ebx
	movq	%r14, %rax
	movq	%r15, %rcx
	jmp	.LBB91_4
	.p2align	4, 0x90
.LBB91_14:                              #   in Loop: Header=BB91_4 Depth=1
	incq	%rax
	incq	%rcx
	shlq	$9, %rdx
	addl	amino_dis(%rdx,%rsi,4), %ebx
.LBB91_4:                               # %.backedge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB91_8 Depth 2
                                        #     Child Loop BB91_13 Depth 2
	movsbq	(%rax), %rdx
	cmpq	$45, %rdx
	je	.LBB91_12
# BB#5:                                 # %.backedge
                                        #   in Loop: Header=BB91_4 Depth=1
	testb	%dl, %dl
	je	.LBB91_15
# BB#6:                                 #   in Loop: Header=BB91_4 Depth=1
	movsbq	(%rcx), %rsi
	cmpq	$45, %rsi
	jne	.LBB91_14
# BB#7:                                 # %.lr.ph51.preheader
                                        #   in Loop: Header=BB91_4 Depth=1
	addl	%r12d, %ebx
	.p2align	4, 0x90
.LBB91_8:                               # %.lr.ph51
                                        #   Parent Loop BB91_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rax
	cmpb	$45, 1(%rcx)
	leaq	1(%rcx), %rcx
	je	.LBB91_8
	jmp	.LBB91_4
	.p2align	4, 0x90
.LBB91_12:                              # %.lr.ph.preheader
                                        #   in Loop: Header=BB91_4 Depth=1
	addl	%r12d, %ebx
	.p2align	4, 0x90
.LBB91_13:                              # %.lr.ph
                                        #   Parent Loop BB91_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rcx
	cmpb	$45, 1(%rax)
	leaq	1(%rax), %rax
	je	.LBB91_13
	jmp	.LBB91_4
.LBB91_15:
	movq	%r14, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end91:
	.size	naivepairscore11, .Lfunc_end91-naivepairscore11
	.cfi_endproc

	.globl	naivepairscore
	.p2align	4, 0x90
	.type	naivepairscore,@function
naivepairscore:                         # @naivepairscore
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi870:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi871:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi872:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi873:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi874:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi875:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi876:
	.cfi_def_cfa_offset 80
.Lcfi877:
	.cfi_offset %rbx, -56
.Lcfi878:
	.cfi_offset %r12, -48
.Lcfi879:
	.cfi_offset %r13, -40
.Lcfi880:
	.cfi_offset %r14, -32
.Lcfi881:
	.cfi_offset %r15, -24
.Lcfi882:
	.cfi_offset %rbp, -16
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movq	%r8, (%rsp)             # 8-byte Spill
	movq	%rcx, %r12
	movq	%rdx, %r13
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movl	%edi, %ebx
	movq	(%r13), %rdi
	callq	strlen
	shlq	$32, %rax
	movabsq	$4294967296, %rbp       # imm = 0x100000000
	addq	%rax, %rbp
	sarq	$32, %rbp
	movl	$1, %esi
	movq	%rbp, %rdi
	callq	calloc
	movq	%rax, %r14
	movl	$1, %esi
	movq	%rbp, %rdi
	callq	calloc
	movq	%rax, %r15
	xorps	%xmm2, %xmm2
	testl	%ebx, %ebx
	jle	.LBB92_12
# BB#1:
	movl	12(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	movq	16(%rsp), %r11          # 8-byte Reload
	jle	.LBB92_12
# BB#2:                                 # %.preheader.us.preheader
	movl	80(%rsp), %esi
	movl	%eax, %r9d
	movl	%ebx, %r8d
	xorps	%xmm2, %xmm2
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB92_3:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB92_4 Depth 2
                                        #       Child Loop BB92_5 Depth 3
                                        #       Child Loop BB92_8 Depth 3
                                        #         Child Loop BB92_21 Depth 4
                                        #         Child Loop BB92_17 Depth 4
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB92_4:                               #   Parent Loop BB92_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB92_5 Depth 3
                                        #       Child Loop BB92_8 Depth 3
                                        #         Child Loop BB92_21 Depth 4
                                        #         Child Loop BB92_17 Depth 4
	movq	(%rsp), %rax            # 8-byte Reload
	movsd	(%rax,%r10,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%r11,%rdi,8), %xmm0
	movq	(%r13,%r10,8), %rdx
	movq	(%r12,%rdi,8), %rcx
	movq	%r14, %rbp
	movq	%r15, %rbx
	jmp	.LBB92_5
.LBB92_15:                              #   in Loop: Header=BB92_5 Depth=3
	movb	%al, (%rbp)
	incq	%rbp
	movzbl	-1(%rcx), %eax
	movb	%al, (%rbx)
	incq	%rbx
	.p2align	4, 0x90
.LBB92_5:                               # %.outer.i.us
                                        #   Parent Loop BB92_3 Depth=1
                                        #     Parent Loop BB92_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incq	%rdx
	incq	%rcx
	movzbl	-1(%rdx), %eax
	cmpb	$45, %al
	jne	.LBB92_6
# BB#13:                                #   in Loop: Header=BB92_5 Depth=3
	cmpb	$45, -1(%rcx)
	je	.LBB92_5
# BB#14:                                #   in Loop: Header=BB92_5 Depth=3
	movb	$45, %al
	jmp	.LBB92_15
.LBB92_6:                               #   in Loop: Header=BB92_5 Depth=3
	testb	%al, %al
	jne	.LBB92_15
# BB#7:                                 # %commongappickpair.exit.us
                                        #   in Loop: Header=BB92_4 Depth=2
	movb	$0, (%rbp)
	movb	$0, (%rbx)
	xorl	%edx, %edx
	movq	%r14, %rbx
	movq	%r15, %rbp
	jmp	.LBB92_8
	.p2align	4, 0x90
.LBB92_19:                              #   in Loop: Header=BB92_8 Depth=3
	incq	%rbx
	incq	%rbp
	shlq	$9, %rcx
	addl	amino_dis(%rcx,%rax,4), %edx
.LBB92_8:                               # %.backedge.us
                                        #   Parent Loop BB92_3 Depth=1
                                        #     Parent Loop BB92_4 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB92_21 Depth 4
                                        #         Child Loop BB92_17 Depth 4
	movsbq	(%rbx), %rcx
	cmpq	$45, %rcx
	je	.LBB92_16
# BB#9:                                 # %.backedge.us
                                        #   in Loop: Header=BB92_8 Depth=3
	testb	%cl, %cl
	je	.LBB92_10
# BB#18:                                #   in Loop: Header=BB92_8 Depth=3
	movsbq	(%rbp), %rax
	cmpq	$45, %rax
	jne	.LBB92_19
# BB#20:                                # %.lr.ph79.us.preheader
                                        #   in Loop: Header=BB92_8 Depth=3
	addl	%esi, %edx
	.p2align	4, 0x90
.LBB92_21:                              # %.lr.ph79.us
                                        #   Parent Loop BB92_3 Depth=1
                                        #     Parent Loop BB92_4 Depth=2
                                        #       Parent Loop BB92_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incq	%rbx
	cmpb	$45, 1(%rbp)
	leaq	1(%rbp), %rbp
	je	.LBB92_21
	jmp	.LBB92_8
	.p2align	4, 0x90
.LBB92_16:                              # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB92_8 Depth=3
	addl	%esi, %edx
	.p2align	4, 0x90
.LBB92_17:                              # %.lr.ph.us
                                        #   Parent Loop BB92_3 Depth=1
                                        #     Parent Loop BB92_4 Depth=2
                                        #       Parent Loop BB92_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incq	%rbp
	cmpb	$45, 1(%rbx)
	leaq	1(%rbx), %rbx
	je	.LBB92_17
	jmp	.LBB92_8
	.p2align	4, 0x90
.LBB92_10:                              #   in Loop: Header=BB92_4 Depth=2
	cvtsd2ss	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm1
	mulss	%xmm0, %xmm1
	addss	%xmm1, %xmm2
	incq	%rdi
	cmpq	%r9, %rdi
	jne	.LBB92_4
# BB#11:                                # %._crit_edge.us
                                        #   in Loop: Header=BB92_3 Depth=1
	incq	%r10
	cmpq	%r8, %r10
	jne	.LBB92_3
.LBB92_12:                              # %._crit_edge88
	movss	%xmm2, (%rsp)           # 4-byte Spill
	movq	%r14, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	stderr(%rip), %rdi
	movss	(%rsp), %xmm0           # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.36, %esi
	movb	$1, %al
	callq	fprintf
	movss	(%rsp), %xmm0           # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end92:
	.size	naivepairscore, .Lfunc_end92-naivepairscore
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"========================================================================= \n"
	.size	.L.str, 76

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"=== \n"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"=== Alphabet '%c' is unknown.\n"
	.size	.L.str.2, 31

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"=== Please check site %d in sequence %d.\n"
	.size	.L.str.3, 42

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%s\n"
	.size	.L.str.4, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"    ....,....+....,....+....,....+....,....+....,....+....,....+....,....+....,....+....,....+....,....+....,....+....,....+\n"
	.size	.L.str.5, 126

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%3d %s\n"
	.size	.L.str.6, 8

	.type	score_calc3.mseq1,@object # @score_calc3.mseq1
	.local	score_calc3.mseq1
	.comm	score_calc3.mseq1,10000000,16
	.type	score_calc3.mseq2,@object # @score_calc3.mseq2
	.local	score_calc3.mseq2
	.comm	score_calc3.mseq2,10000000,16
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"error\n"
	.size	.L.str.7, 7

	.type	upg2.pair,@object       # @upg2.pair
	.local	upg2.pair
	.comm	upg2.pair,8,8
	.type	loadtree.hist,@object   # @loadtree.hist
	.local	loadtree.hist
	.comm	loadtree.hist,8,8
	.type	loadtree.ac,@object     # @loadtree.ac
	.local	loadtree.ac
	.comm	loadtree.ac,8,8
	.type	loadtree.nmemar,@object # @loadtree.nmemar
	.local	loadtree.nmemar
	.comm	loadtree.nmemar,8,8
	.type	loadtree.tree,@object   # @loadtree.tree
	.local	loadtree.tree
	.comm	loadtree.tree,8,8
	.type	loadtree.treetmp,@object # @loadtree.treetmp
	.local	loadtree.treetmp
	.comm	loadtree.treetmp,8,8
	.type	loadtree.nametmp,@object # @loadtree.nametmp
	.local	loadtree.nametmp
	.comm	loadtree.nametmp,8,8
	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"_guidetree"
	.size	.L.str.8, 11

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"r"
	.size	.L.str.9, 2

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"cannot open _guidetree\n"
	.size	.L.str.10, 24

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%d_%.20s"
	.size	.L.str.11, 9

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"\r% 5d / %d"
	.size	.L.str.13, 11

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"\n\nERROR: Branch length is not given.\n"
	.size	.L.str.14, 38

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Cannot reallocate topol\n"
	.size	.L.str.15, 25

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"(%s:%7.5f,%s:%7.5f)"
	.size	.L.str.16, 20

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"infile.tree"
	.size	.L.str.17, 12

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"w"
	.size	.L.str.18, 2

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"#by loadtree\n"
	.size	.L.str.19, 14

	.type	loadtop.ac,@object      # @loadtop.ac
	.local	loadtop.ac
	.comm	loadtop.ac,8,8
	.type	loadtop.tmptmplen,@object # @loadtop.tmptmplen
	.local	loadtop.tmptmplen
	.comm	loadtop.tmptmplen,8,8
	.type	loadtop.hist,@object    # @loadtop.hist
	.local	loadtop.hist
	.comm	loadtop.hist,8,8
	.type	loadtop.nmemar,@object  # @loadtop.nmemar
	.local	loadtop.nmemar
	.comm	loadtop.nmemar,8,8
	.type	loadtop.tree,@object    # @loadtop.tree
	.local	loadtop.tree
	.comm	loadtop.tree,8,8
	.type	loadtop.treetmp,@object # @loadtop.treetmp
	.local	loadtop.treetmp
	.comm	loadtop.treetmp,8,8
	.type	sueff1,@object          # @sueff1
	.local	sueff1
	.comm	sueff1,4,4
	.type	sueff05,@object         # @sueff05
	.local	sueff05
	.comm	sueff05,4,4
	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Unknown treemethod, %c\n"
	.size	.L.str.20, 24

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"%d"
	.size	.L.str.21, 3

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"\n\nERROR: Branch length should not be given.\n"
	.size	.L.str.22, 45

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"by loadtop\n"
	.size	.L.str.23, 12

	.type	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.tmptmplen,@object # @fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.tmptmplen
	.local	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.tmptmplen
	.comm	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.tmptmplen,8,8
	.type	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.hist,@object # @fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.hist
	.local	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.hist
	.comm	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.hist,8,8
	.type	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.ac,@object # @fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.ac
	.local	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.ac
	.comm	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.ac,8,8
	.type	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.nmemar,@object # @fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.nmemar
	.local	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.nmemar
	.comm	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.nmemar,8,8
	.type	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.tree,@object # @fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.tree
	.local	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.tree
	.comm	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.tree,8,8
	.type	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.treetmp,@object # @fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.treetmp
	.local	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.treetmp
	.comm	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.treetmp,8,8
	.type	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.nametmp,@object # @fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.nametmp
	.local	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.nametmp
	.comm	fixed_musclesupg_float_realloc_nobk_halfmtx_treeout.nametmp,8,8
	.type	fixed_musclesupg_float_realloc_nobk_halfmtx.tmptmplen,@object # @fixed_musclesupg_float_realloc_nobk_halfmtx.tmptmplen
	.local	fixed_musclesupg_float_realloc_nobk_halfmtx.tmptmplen
	.comm	fixed_musclesupg_float_realloc_nobk_halfmtx.tmptmplen,8,8
	.type	fixed_musclesupg_float_realloc_nobk_halfmtx.hist,@object # @fixed_musclesupg_float_realloc_nobk_halfmtx.hist
	.local	fixed_musclesupg_float_realloc_nobk_halfmtx.hist
	.comm	fixed_musclesupg_float_realloc_nobk_halfmtx.hist,8,8
	.type	fixed_musclesupg_float_realloc_nobk_halfmtx.ac,@object # @fixed_musclesupg_float_realloc_nobk_halfmtx.ac
	.local	fixed_musclesupg_float_realloc_nobk_halfmtx.ac
	.comm	fixed_musclesupg_float_realloc_nobk_halfmtx.ac,8,8
	.type	fixed_musclesupg_float_realloc_nobk_halfmtx.nmemar,@object # @fixed_musclesupg_float_realloc_nobk_halfmtx.nmemar
	.local	fixed_musclesupg_float_realloc_nobk_halfmtx.nmemar
	.comm	fixed_musclesupg_float_realloc_nobk_halfmtx.nmemar,8,8
	.type	veryfastsupg_double_loadtop.tmptmplen,@object # @veryfastsupg_double_loadtop.tmptmplen
	.local	veryfastsupg_double_loadtop.tmptmplen
	.comm	veryfastsupg_double_loadtop.tmptmplen,8,8
	.type	veryfastsupg_double_loadtop.hist,@object # @veryfastsupg_double_loadtop.hist
	.local	veryfastsupg_double_loadtop.hist
	.comm	veryfastsupg_double_loadtop.hist,8,8
	.type	veryfastsupg_double_loadtop.ac,@object # @veryfastsupg_double_loadtop.ac
	.local	veryfastsupg_double_loadtop.ac
	.comm	veryfastsupg_double_loadtop.ac,8,8
	.type	veryfastsupg_double_loadtop.tree,@object # @veryfastsupg_double_loadtop.tree
	.local	veryfastsupg_double_loadtop.tree
	.comm	veryfastsupg_double_loadtop.tree,8,8
	.type	veryfastsupg_double_loadtop.treetmp,@object # @veryfastsupg_double_loadtop.treetmp
	.local	veryfastsupg_double_loadtop.treetmp
	.comm	veryfastsupg_double_loadtop.treetmp,8,8
	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"%d / %d\r"
	.size	.L.str.24, 9

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"\n\nBranch length should not given.\n"
	.size	.L.str.25, 35

	.type	veryfastsupg_double_loadtree.tmptmplen,@object # @veryfastsupg_double_loadtree.tmptmplen
	.local	veryfastsupg_double_loadtree.tmptmplen
	.comm	veryfastsupg_double_loadtree.tmptmplen,8,8
	.type	veryfastsupg_double_loadtree.hist,@object # @veryfastsupg_double_loadtree.hist
	.local	veryfastsupg_double_loadtree.hist
	.comm	veryfastsupg_double_loadtree.hist,8,8
	.type	veryfastsupg_double_loadtree.ac,@object # @veryfastsupg_double_loadtree.ac
	.local	veryfastsupg_double_loadtree.ac
	.comm	veryfastsupg_double_loadtree.ac,8,8
	.type	veryfastsupg_double_loadtree.tree,@object # @veryfastsupg_double_loadtree.tree
	.local	veryfastsupg_double_loadtree.tree
	.comm	veryfastsupg_double_loadtree.tree,8,8
	.type	veryfastsupg_double_loadtree.treetmp,@object # @veryfastsupg_double_loadtree.treetmp
	.local	veryfastsupg_double_loadtree.treetmp
	.comm	veryfastsupg_double_loadtree.treetmp,8,8
	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"\n\nWARNING: Branch length is not given.\n"
	.size	.L.str.26, 40

	.type	veryfastsupg_double_outtree.tmptmplen,@object # @veryfastsupg_double_outtree.tmptmplen
	.local	veryfastsupg_double_outtree.tmptmplen
	.comm	veryfastsupg_double_outtree.tmptmplen,8,8
	.type	veryfastsupg_double_outtree.hist,@object # @veryfastsupg_double_outtree.hist
	.local	veryfastsupg_double_outtree.hist
	.comm	veryfastsupg_double_outtree.hist,8,8
	.type	veryfastsupg_double_outtree.ac,@object # @veryfastsupg_double_outtree.ac
	.local	veryfastsupg_double_outtree.ac
	.comm	veryfastsupg_double_outtree.ac,8,8
	.type	veryfastsupg_double_outtree.tree,@object # @veryfastsupg_double_outtree.tree
	.local	veryfastsupg_double_outtree.tree
	.comm	veryfastsupg_double_outtree.tree,8,8
	.type	veryfastsupg_double_outtree.treetmp,@object # @veryfastsupg_double_outtree.treetmp
	.local	veryfastsupg_double_outtree.treetmp
	.comm	veryfastsupg_double_outtree.treetmp,8,8
	.type	veryfastsupg_double_outtree.nametmp,@object # @veryfastsupg_double_outtree.nametmp
	.local	veryfastsupg_double_outtree.nametmp
	.comm	veryfastsupg_double_outtree.nametmp,8,8
	.type	sueff1_double,@object   # @sueff1_double
	.local	sueff1_double
	.comm	sueff1_double,8,8
	.type	sueff05_double,@object  # @sueff05_double
	.local	sueff05_double
	.comm	sueff05_double,8,8
	.type	veryfastsupg.tmptmplen,@object # @veryfastsupg.tmptmplen
	.local	veryfastsupg.tmptmplen
	.comm	veryfastsupg.tmptmplen,8,8
	.type	veryfastsupg.eff,@object # @veryfastsupg.eff
	.local	veryfastsupg.eff
	.comm	veryfastsupg.eff,8,8
	.type	veryfastsupg.hist,@object # @veryfastsupg.hist
	.local	veryfastsupg.hist
	.comm	veryfastsupg.hist,8,8
	.type	veryfastsupg.ac,@object # @veryfastsupg.ac
	.local	veryfastsupg.ac
	.comm	veryfastsupg.ac,8,8
	.type	veryfastsupg_int.tmptmplen,@object # @veryfastsupg_int.tmptmplen
	.local	veryfastsupg_int.tmptmplen
	.comm	veryfastsupg_int.tmptmplen,8,8
	.type	veryfastsupg_int.eff,@object # @veryfastsupg_int.eff
	.local	veryfastsupg_int.eff
	.comm	veryfastsupg_int.eff,8,8
	.type	veryfastsupg_int.hist,@object # @veryfastsupg_int.hist
	.local	veryfastsupg_int.hist
	.comm	veryfastsupg_int.hist,8,8
	.type	veryfastsupg_int.ac,@object # @veryfastsupg_int.ac
	.local	veryfastsupg_int.ac
	.comm	veryfastsupg_int.ac,8,8
	.type	fastsupg.tmplen,@object # @fastsupg.tmplen
	.local	fastsupg.tmplen
	.comm	fastsupg.tmplen,8,8
	.type	fastsupg.eff,@object    # @fastsupg.eff
	.local	fastsupg.eff
	.comm	fastsupg.eff,8,8
	.type	fastsupg.pair,@object   # @fastsupg.pair
	.local	fastsupg.pair
	.comm	fastsupg.pair,8,8
	.type	fastsupg.ac,@object     # @fastsupg.ac
	.local	fastsupg.ac
	.comm	fastsupg.ac,8,8
	.type	supg.tmplen,@object     # @supg.tmplen
	.local	supg.tmplen
	.comm	supg.tmplen,8,8
	.type	supg.eff,@object        # @supg.eff
	.local	supg.eff
	.comm	supg.eff,8,8
	.type	supg.pair,@object       # @supg.pair
	.local	supg.pair
	.comm	supg.pair,8,8
	.type	countnode.rootnode,@object # @countnode.rootnode
	.local	countnode.rootnode
	.comm	countnode.rootnode,400000,16
	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"Too few sequence for countnode: nseq = %d\n"
	.size	.L.str.27, 43

	.type	counteff_simple_float.rootnode,@object # @counteff_simple_float.rootnode
	.local	counteff_simple_float.rootnode
	.comm	counteff_simple_float.rootnode,400000,16
	.type	counteff_simple_float.eff,@object # @counteff_simple_float.eff
	.local	counteff_simple_float.eff
	.comm	counteff_simple_float.eff,400000,16
	.type	counteff_simple.rootnode,@object # @counteff_simple.rootnode
	.local	counteff_simple.rootnode
	.comm	counteff_simple.rootnode,400000,16
	.type	counteff_simple.eff,@object # @counteff_simple.eff
	.local	counteff_simple.eff
	.comm	counteff_simple.eff,400000,16
	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"mix error"
	.size	.L.str.28, 10

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"raw score = %f\n"
	.size	.L.str.29, 16

	.type	searchAnchors.stra,@object # @searchAnchors.stra
	.local	searchAnchors.stra
	.comm	searchAnchors.stra,8,8
	.type	searchAnchors.alloclen,@object # @searchAnchors.alloclen
	.local	searchAnchors.alloclen
	.comm	searchAnchors.alloclen,4,4
	.type	searchAnchors.threshold,@object # @searchAnchors.threshold
	.local	searchAnchors.threshold
	.comm	searchAnchors.threshold,8,8
	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"TOO MANY SEGMENTS!"
	.size	.L.str.30, 19

	.type	dontcalcimportance.nogaplen,@object # @dontcalcimportance.nogaplen
	.local	dontcalcimportance.nogaplen
	.comm	dontcalcimportance.nogaplen,8,8
	.type	calcimportance.importance,@object # @calcimportance.importance
	.local	calcimportance.importance
	.comm	calcimportance.importance,8,8
	.type	calcimportance.nogaplen,@object # @calcimportance.nogaplen
	.local	calcimportance.nogaplen
	.comm	calcimportance.nogaplen,8,8
	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"ERROR: i=%d, j=%d\n"
	.size	.L.str.31, 19

	.type	extendlocalhom2.ini,@object # @extendlocalhom2.ini
	.local	extendlocalhom2.ini
	.comm	extendlocalhom2.ini,8,8
	.type	extendlocalhom2.inj,@object # @extendlocalhom2.inj
	.local	extendlocalhom2.inj
	.comm	extendlocalhom2.inj,8,8
	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"opt kainaide tbfast.c = %f\n"
	.size	.L.str.32, 28

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"%d, %f\n"
	.size	.L.str.33, 8

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"Cannot allocate gappattern!'n"
	.size	.L.str.34, 30

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"Use an approximate method, with the --mafft5 option.\n"
	.size	.L.str.35, 54

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"val = %f\n"
	.size	.L.str.36, 10

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"Penal!, %f, %d-%d, pos1,pos2=%d,%d\n"
	.size	.L.str.37, 36

	.type	loadtreeoneline.gett,@object # @loadtreeoneline.gett
	.local	loadtreeoneline.gett
	.comm	loadtreeoneline.gett,1000,16
	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"%d %d %a %a"
	.size	.L.str.38, 12

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"Incorrect guide tree\n"
	.size	.L.str.39, 22

	.type	intergroup_score_new.efficient.0,@object # @intergroup_score_new.efficient.0
	.local	intergroup_score_new.efficient.0
	.comm	intergroup_score_new.efficient.0,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
