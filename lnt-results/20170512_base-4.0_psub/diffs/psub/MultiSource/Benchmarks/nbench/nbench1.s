	.text
	.file	"nbench1.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4607182418800017408     # double 1
.LCPI0_1:
	.quad	4645304685958266880     # double 375
	.text
	.globl	DoNumSort
	.p2align	4, 0x90
	.type	DoNumSort,@function
DoNumSort:                              # @DoNumSort
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 48
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	cmpl	$0, global_numsortstruct(%rip)
	je	.LBB0_1
# BB#7:
	movzwl	global_numsortstruct+24(%rip), %edi
	imulq	global_numsortstruct+32(%rip), %rdi
	shlq	$3, %rdi
	leaq	4(%rsp), %rsi
	callq	AllocateMemory
	movq	%rax, %r15
	movl	4(%rsp), %esi
	testl	%esi, %esi
	je	.LBB0_9
# BB#8:
	movl	$.L.str.50, %edi
	callq	ReportError
	leaq	4(%rsp), %rsi
	movq	%r15, %rdi
	callq	FreeMemory
	xorl	%eax, %eax
	callq	ErrorExit
	jmp	.LBB0_9
.LBB0_1:
	movw	$1, global_numsortstruct+24(%rip)
	movw	$1, %ax
	leaq	4(%rsp), %r14
	jmp	.LBB0_2
	.p2align	4, 0x90
.LBB0_6:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.Lstr, %edi
	callq	puts
	xorl	%eax, %eax
	callq	ErrorExit
	movw	global_numsortstruct+24(%rip), %ax
.LBB0_2:                                # %.backedge
                                        # =>This Inner Loop Header: Depth=1
	movzwl	%ax, %edi
	imulq	global_numsortstruct+32(%rip), %rdi
	shlq	$3, %rdi
	movq	%r14, %rsi
	callq	AllocateMemory
	movq	%rax, %r15
	movl	4(%rsp), %esi
	testl	%esi, %esi
	je	.LBB0_4
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.50, %edi
	callq	ReportError
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	FreeMemory
	xorl	%eax, %eax
	callq	ErrorExit
.LBB0_4:                                #   in Loop: Header=BB0_2 Depth=1
	movq	global_numsortstruct+32(%rip), %rsi
	movzwl	global_numsortstruct+24(%rip), %edx
	movq	%r15, %rdi
	callq	DoNumSortIteration
	cmpq	global_min_ticks(%rip), %rax
	ja	.LBB0_9
# BB#5:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	FreeMemory
	movzwl	global_numsortstruct+24(%rip), %ecx
	leal	1(%rcx), %eax
	movw	%ax, global_numsortstruct+24(%rip)
	cmpl	$10001, %ecx            # imm = 0x2711
	jb	.LBB0_2
	jmp	.LBB0_6
.LBB0_9:                                # %.loopexit
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_10:                               # =>This Inner Loop Header: Depth=1
	movq	global_numsortstruct+32(%rip), %rsi
	movzwl	global_numsortstruct+24(%rip), %edx
	movq	%r15, %rdi
	callq	DoNumSortIteration
	addq	%rax, %rbx
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	.LCPI0_0(%rip), %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB0_10
# BB#11:
	leaq	4(%rsp), %rsi
	movq	%r15, %rdi
	callq	FreeMemory
	movzwl	global_numsortstruct+24(%rip), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	TicksToFracSecs
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, global_numsortstruct+16(%rip)
	cmpl	$0, global_numsortstruct(%rip)
	jne	.LBB0_13
# BB#12:
	movl	$1, global_numsortstruct(%rip)
.LBB0_13:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	DoNumSort, .Lfunc_end0-DoNumSort
	.cfi_endproc

	.p2align	4, 0x90
	.type	DoNumSortIteration,@function
DoNumSortIteration:                     # @DoNumSortIteration
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 128
.Lcfi14:
	.cfi_offset %rbx, -56
.Lcfi15:
	.cfi_offset %r12, -48
.Lcfi16:
	.cfi_offset %r13, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %r14
	movl	$13, %edi
	callq	randnum
	testq	%r12, %r12
	je	.LBB1_26
# BB#1:                                 # %.lr.ph25.i.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph25.i
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edi, %edi
	callq	randnum
	cltq
	movq	%rax, (%r14,%rbx,8)
	incq	%rbx
	cmpq	%rbx, %r12
	jne	.LBB1_2
# BB#3:                                 # %.preheader.i
	movl	12(%rsp), %eax          # 4-byte Reload
	decl	%eax
	je	.LBB1_28
# BB#4:                                 # %.lr.ph22.split.us.i.preheader
	leaq	(%r12,%r12), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	leaq	(%r14,%r12,8), %r8
	leaq	-4(%r12), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	%ecx, %edx
	shrl	$2, %edx
	incl	%edx
	leaq	-1(%r12), %r9
	movq	%r12, %r15
	andq	$-4, %r15
	andl	$3, %edx
	leaq	(,%r12,8), %rcx
	leaq	16(,%r12,8), %rsi
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	negq	%rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	leaq	64(%r14), %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	movq	%r8, %r13
	movq	%r14, %r10
	movq	%r8, 56(%rsp)           # 8-byte Spill
	jmp	.LBB1_5
.LBB1_12:                               # %vector.body.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	je	.LBB1_15
# BB#13:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	32(%rsp), %rsi          # 8-byte Reload
	leaq	(%r10,%rsi), %rsi
	movq	24(%rsp), %rbx          # 8-byte Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_14:                               # %vector.body.prol
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r14,%rbp,8), %xmm0
	movups	16(%r14,%rbp,8), %xmm1
	movups	%xmm0, -16(%rsi,%rbp,8)
	movups	%xmm1, (%rsi,%rbp,8)
	addq	$4, %rbp
	incq	%rbx
	jne	.LBB1_14
	jmp	.LBB1_16
.LBB1_15:                               #   in Loop: Header=BB1_5 Depth=1
	xorl	%ebp, %ebp
.LBB1_16:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB1_5 Depth=1
	cmpq	$12, 48(%rsp)           # 8-byte Folded Reload
	jb	.LBB1_19
# BB#17:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	%r15, %rsi
	subq	%rbp, %rsi
	leaq	(%r13,%rbp,8), %r11
	movq	16(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rbp,8), %r8
	.p2align	4, 0x90
.LBB1_18:                               # %vector.body
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-64(%r8), %xmm0
	movups	-48(%r8), %xmm1
	movups	%xmm0, (%r11)
	movups	%xmm1, 16(%r11)
	movups	-32(%r8), %xmm0
	movups	-16(%r8), %xmm1
	movups	%xmm0, 32(%r11)
	movups	%xmm1, 48(%r11)
	movups	(%r8), %xmm0
	movups	16(%r8), %xmm1
	movups	%xmm0, 64(%r11)
	movups	%xmm1, 80(%r11)
	movups	32(%r8), %xmm0
	movups	48(%r8), %xmm1
	movups	%xmm0, 96(%r11)
	movups	%xmm1, 112(%r11)
	subq	$-128, %r11
	subq	$-128, %r8
	addq	$-16, %rsi
	jne	.LBB1_18
.LBB1_19:                               # %middle.block
                                        #   in Loop: Header=BB1_5 Depth=1
	cmpq	%r12, %r15
	movq	%r15, %rsi
	movq	56(%rsp), %r8           # 8-byte Reload
	je	.LBB1_25
	jmp	.LBB1_20
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph22.split.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_14 Depth 2
                                        #     Child Loop BB1_18 Depth 2
                                        #     Child Loop BB1_22 Depth 2
                                        #     Child Loop BB1_24 Depth 2
	cmpq	$4, %r12
	jae	.LBB1_7
# BB#6:                                 #   in Loop: Header=BB1_5 Depth=1
	xorl	%esi, %esi
	jmp	.LBB1_20
	.p2align	4, 0x90
.LBB1_7:                                # %min.iters.checked
                                        #   in Loop: Header=BB1_5 Depth=1
	testq	%r15, %r15
	je	.LBB1_11
# BB#8:                                 # %vector.memcheck
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	%rdx, %rsi
	imulq	%r12, %rsi
	leaq	(%rsi,%r12), %rbp
	leaq	(%r14,%rbp,8), %rbp
	cmpq	%r8, %rbp
	jae	.LBB1_12
# BB#9:                                 # %vector.memcheck
                                        #   in Loop: Header=BB1_5 Depth=1
	addq	64(%rsp), %rsi          # 8-byte Folded Reload
	leaq	(%r14,%rsi,8), %rsi
	cmpq	%r14, %rsi
	jbe	.LBB1_12
.LBB1_11:                               #   in Loop: Header=BB1_5 Depth=1
	xorl	%esi, %esi
.LBB1_20:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	movl	%r12d, %ebx
	subl	%esi, %ebx
	movq	%r9, %rbp
	subq	%rsi, %rbp
	andq	$7, %rbx
	je	.LBB1_23
# BB#21:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	negq	%rbx
	.p2align	4, 0x90
.LBB1_22:                               # %scalar.ph.prol
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%rsi,8), %rdi
	movq	%rdi, (%r13,%rsi,8)
	incq	%rsi
	incq	%rbx
	jne	.LBB1_22
.LBB1_23:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB1_5 Depth=1
	cmpq	$7, %rbp
	jb	.LBB1_25
	.p2align	4, 0x90
.LBB1_24:                               # %scalar.ph
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%rsi,8), %rdi
	movq	%rdi, (%r13,%rsi,8)
	movq	8(%r14,%rsi,8), %rdi
	movq	%rdi, 8(%r13,%rsi,8)
	movq	16(%r14,%rsi,8), %rdi
	movq	%rdi, 16(%r13,%rsi,8)
	movq	24(%r14,%rsi,8), %rdi
	movq	%rdi, 24(%r13,%rsi,8)
	movq	32(%r14,%rsi,8), %rdi
	movq	%rdi, 32(%r13,%rsi,8)
	movq	40(%r14,%rsi,8), %rdi
	movq	%rdi, 40(%r13,%rsi,8)
	movq	48(%r14,%rsi,8), %rdi
	movq	%rdi, 48(%r13,%rsi,8)
	movq	56(%r14,%rsi,8), %rdi
	movq	%rdi, 56(%r13,%rsi,8)
	addq	$8, %rsi
	cmpq	%rsi, %r12
	jne	.LBB1_24
.LBB1_25:                               # %..loopexit_crit_edge.us.i
                                        #   in Loop: Header=BB1_5 Depth=1
	incq	%rdx
	addq	%rcx, %r10
	addq	%rcx, %r13
	decl	%eax
	jne	.LBB1_5
.LBB1_26:                               # %LoadNumArrayWithRand.exit
	xorl	%eax, %eax
	callq	StartStopwatch
	movl	12(%rsp), %ecx          # 4-byte Reload
	testl	%ecx, %ecx
	je	.LBB1_60
# BB#27:
	movl	%ecx, %r8d
	jmp	.LBB1_29
.LBB1_28:                               # %LoadNumArrayWithRand.exit.thread
	xorl	%eax, %eax
	callq	StartStopwatch
	movl	$1, %r8d
.LBB1_29:                               # %.lr.ph
	leaq	-1(%r12), %r11
	movq	%r11, %r9
	shrq	%r9
	je	.LBB1_49
# BB#30:                                # %.lr.ph.split.preheader
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB1_31:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_32 Depth 2
                                        #       Child Loop BB1_34 Depth 3
                                        #     Child Loop BB1_41 Depth 2
                                        #       Child Loop BB1_42 Depth 3
	movq	%r10, %rcx
	imulq	%r12, %rcx
	leaq	(%r14,%rcx,8), %rbp
	movq	%r9, %r15
	.p2align	4, 0x90
.LBB1_32:                               # %.lr.ph11.i
                                        #   Parent Loop BB1_31 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_34 Depth 3
	leaq	(%r15,%r15), %rsi
	cmpq	%r11, %rsi
	ja	.LBB1_39
# BB#33:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB1_32 Depth=2
	movq	%r15, %rdi
	.p2align	4, 0x90
.LBB1_34:                               # %.lr.ph.i.i
                                        #   Parent Loop BB1_31 Depth=1
                                        #     Parent Loop BB1_32 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%r11, %rsi
	jae	.LBB1_36
# BB#35:                                #   in Loop: Header=BB1_34 Depth=3
	movq	(%rbp,%rsi,8), %rcx
	movq	%rsi, %rdx
	orq	$1, %rdx
	cmpq	(%rbp,%rdx,8), %rcx
	cmovlq	%rdx, %rsi
.LBB1_36:                               #   in Loop: Header=BB1_34 Depth=3
	movq	(%rbp,%rdi,8), %rbx
	movq	(%rbp,%rsi,8), %rdx
	cmpq	%rdx, %rbx
	movq	%r12, %rcx
	jge	.LBB1_38
# BB#37:                                #   in Loop: Header=BB1_34 Depth=3
	movq	%rbx, (%rbp,%rsi,8)
	movq	%rdx, (%rbp,%rdi,8)
	movq	%rsi, %rcx
.LBB1_38:                               # %.backedge.i.i
                                        #   in Loop: Header=BB1_34 Depth=3
	leaq	(%rcx,%rcx), %rsi
	cmpq	%r11, %rsi
	movq	%rcx, %rdi
	jbe	.LBB1_34
.LBB1_39:                               # %NumSift.exit.i
                                        #   in Loop: Header=BB1_32 Depth=2
	decq	%r15
	jne	.LBB1_32
# BB#40:                                # %.preheader.i14
                                        #   in Loop: Header=BB1_31 Depth=1
	testq	%r11, %r11
	movq	%r11, %r13
	je	.LBB1_48
	.p2align	4, 0x90
.LBB1_41:                               # %.lr.ph.i
                                        #   Parent Loop BB1_31 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_42 Depth 3
	leaq	1(%r13), %r15
	xorl	%edi, %edi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_42:                               #   Parent Loop BB1_31 Depth=1
                                        #     Parent Loop BB1_41 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%r13, %rdi
	jae	.LBB1_44
# BB#43:                                #   in Loop: Header=BB1_42 Depth=3
	movq	(%rbp,%rdi,8), %rcx
	movq	%rdi, %rdx
	orq	$1, %rdx
	cmpq	(%rbp,%rdx,8), %rcx
	cmovlq	%rdx, %rdi
.LBB1_44:                               #   in Loop: Header=BB1_42 Depth=3
	movq	(%rbp,%rbx,8), %rsi
	movq	(%rbp,%rdi,8), %rdx
	cmpq	%rdx, %rsi
	movq	%r15, %rcx
	jge	.LBB1_46
# BB#45:                                #   in Loop: Header=BB1_42 Depth=3
	movq	%rsi, (%rbp,%rdi,8)
	movq	%rdx, (%rbp,%rbx,8)
	movq	%rdi, %rcx
.LBB1_46:                               # %.backedge.i6.i
                                        #   in Loop: Header=BB1_42 Depth=3
	leaq	(%rcx,%rcx), %rdi
	cmpq	%r13, %rdi
	movq	%rcx, %rbx
	jbe	.LBB1_42
# BB#47:                                # %NumSift.exit7.i
                                        #   in Loop: Header=BB1_41 Depth=2
	movq	(%rbp), %rcx
	movq	(%rbp,%r13,8), %rdx
	movq	%rdx, (%rbp)
	movq	%rcx, (%rbp,%r13,8)
	decq	%r13
	jne	.LBB1_41
.LBB1_48:                               # %NumHeapSort.exit
                                        #   in Loop: Header=BB1_31 Depth=1
	incq	%r10
	cmpq	%r8, %r10
	jne	.LBB1_31
	jmp	.LBB1_60
.LBB1_49:                               # %.lr.ph.split.us.preheader
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB1_50:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_52 Depth 2
                                        #       Child Loop BB1_53 Depth 3
	testq	%r11, %r11
	je	.LBB1_59
# BB#51:                                # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB1_50 Depth=1
	movq	%r9, %rcx
	imulq	%r12, %rcx
	leaq	(%r14,%rcx,8), %rdx
	movq	%r11, %r15
	.p2align	4, 0x90
.LBB1_52:                               # %.lr.ph.i.us
                                        #   Parent Loop BB1_50 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_53 Depth 3
	leaq	1(%r15), %r10
	xorl	%esi, %esi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_53:                               #   Parent Loop BB1_50 Depth=1
                                        #     Parent Loop BB1_52 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%r15, %rsi
	jae	.LBB1_55
# BB#54:                                #   in Loop: Header=BB1_53 Depth=3
	movq	(%rdx,%rsi,8), %rcx
	movq	%rsi, %rdi
	orq	$1, %rdi
	cmpq	(%rdx,%rdi,8), %rcx
	cmovlq	%rdi, %rsi
.LBB1_55:                               #   in Loop: Header=BB1_53 Depth=3
	movq	(%rdx,%rbx,8), %rcx
	movq	(%rdx,%rsi,8), %rdi
	cmpq	%rdi, %rcx
	movq	%r10, %rbp
	jge	.LBB1_57
# BB#56:                                #   in Loop: Header=BB1_53 Depth=3
	movq	%rcx, (%rdx,%rsi,8)
	movq	%rdi, (%rdx,%rbx,8)
	movq	%rsi, %rbp
.LBB1_57:                               # %.backedge.i6.i.us
                                        #   in Loop: Header=BB1_53 Depth=3
	leaq	(%rbp,%rbp), %rsi
	cmpq	%r15, %rsi
	movq	%rbp, %rbx
	jbe	.LBB1_53
# BB#58:                                # %NumSift.exit7.i.us
                                        #   in Loop: Header=BB1_52 Depth=2
	movq	(%rdx), %rcx
	movq	(%rdx,%r15,8), %rsi
	movq	%rsi, (%rdx)
	movq	%rcx, (%rdx,%r15,8)
	decq	%r15
	jne	.LBB1_52
.LBB1_59:                               # %NumHeapSort.exit.us
                                        #   in Loop: Header=BB1_50 Depth=1
	incq	%r9
	cmpq	%r8, %r9
	jne	.LBB1_50
.LBB1_60:                               # %._crit_edge
	movq	%rax, %rdi
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	StopStopwatch           # TAILCALL
.Lfunc_end1:
	.size	DoNumSortIteration, .Lfunc_end1-DoNumSortIteration
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4638496509959077888     # double 125
	.text
	.globl	DoStringSort
	.p2align	4, 0x90
	.type	DoStringSort,@function
DoStringSort:                           # @DoStringSort
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 48
.Lcfi24:
	.cfi_offset %rbx, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %r15, -16
	cmpl	$0, global_strsortstruct(%rip)
	je	.LBB2_1
# BB#6:
	movq	global_strsortstruct+32(%rip), %rax
	addq	$100, %rax
	movzwl	global_strsortstruct+24(%rip), %edi
	imulq	%rax, %rdi
	leaq	4(%rsp), %rsi
	callq	AllocateMemory
	movq	%rax, %r15
	movl	4(%rsp), %esi
	testl	%esi, %esi
	je	.LBB2_8
# BB#7:
	movl	$.L.str.52, %edi
	callq	ReportError
	xorl	%eax, %eax
	callq	ErrorExit
	jmp	.LBB2_8
.LBB2_1:                                # %.preheader.preheader
	movw	$1, %ax
	leaq	4(%rsp), %r14
	jmp	.LBB2_2
	.p2align	4, 0x90
.LBB2_5:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	FreeMemory
	movzwl	global_strsortstruct+24(%rip), %eax
	incl	%eax
.LBB2_2:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movw	%ax, global_strsortstruct+24(%rip)
	movq	global_strsortstruct+32(%rip), %rcx
	addq	$100, %rcx
	movzwl	%ax, %edi
	imulq	%rcx, %rdi
	movq	%r14, %rsi
	callq	AllocateMemory
	movq	%rax, %r15
	movl	4(%rsp), %esi
	testl	%esi, %esi
	je	.LBB2_4
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	movl	$.L.str.52, %edi
	callq	ReportError
	xorl	%eax, %eax
	callq	ErrorExit
.LBB2_4:                                #   in Loop: Header=BB2_2 Depth=1
	movzwl	global_strsortstruct+24(%rip), %esi
	movq	global_strsortstruct+32(%rip), %rdx
	movq	%r15, %rdi
	callq	DoStringSortIteration
	cmpq	global_min_ticks(%rip), %rax
	jbe	.LBB2_5
.LBB2_8:                                # %.loopexit
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movw	global_strsortstruct+24(%rip), %ax
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_9:                                # =>This Inner Loop Header: Depth=1
	movzwl	%ax, %esi
	movq	global_strsortstruct+32(%rip), %rdx
	movq	%r15, %rdi
	callq	DoStringSortIteration
	addq	%rax, %rbx
	movzwl	global_strsortstruct+24(%rip), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movsd	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	ja	.LBB2_9
# BB#10:
	leaq	4(%rsp), %rsi
	movq	%r15, %rdi
	callq	FreeMemory
	movq	%rbx, %rdi
	callq	TicksToFracSecs
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, global_strsortstruct+16(%rip)
	cmpl	$0, global_strsortstruct(%rip)
	jne	.LBB2_12
# BB#11:
	movl	$1, global_strsortstruct(%rip)
.LBB2_12:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	DoStringSort, .Lfunc_end2-DoStringSort
	.cfi_endproc

	.p2align	4, 0x90
	.type	DoStringSortIteration,@function
DoStringSortIteration:                  # @DoStringSortIteration
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi33:
	.cfi_def_cfa_offset 336
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movq	%rdi, %r12
	movl	$13, %edi
	callq	randnum
	leaq	255(%r14), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	1(%r12), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	movl	$1, %eax
	xorl	%ebp, %ebp
	movq	%r12, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_4 Depth 2
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rax, %r15
	movl	$76, %edi
	callq	abs_randwc
	incl	%eax
	movzbl	%al, %ecx
	leaq	1(%rcx,%rbp), %r13
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %ebx
	subl	%ebp, %ebx
	cmpq	%r14, %r13
	cmovbl	%eax, %ebx
	movb	%bl, (%r12,%rbp)
	testb	%bl, %bl
	je	.LBB3_2
# BB#3:                                 # %.lr.ph104.preheader.i
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movq	%r14, %r15
	movl	%ebx, %r12d
	decb	%r12b
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbp), %r14
	.p2align	4, 0x90
.LBB3_4:                                # %.lr.ph104.i
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$254, %edi
	callq	abs_randwc
	movb	%al, (%r14)
	incq	%r14
	decb	%bl
	jne	.LBB3_4
# BB#5:                                 # %._crit_edge105.loopexit.i
                                        #   in Loop: Header=BB3_1 Depth=1
	movzbl	%r12b, %eax
	leaq	2(%rbp,%rax), %rbp
	movq	%r15, %r14
	movq	64(%rsp), %r12          # 8-byte Reload
	movq	48(%rsp), %r15          # 8-byte Reload
	jmp	.LBB3_6
	.p2align	4, 0x90
.LBB3_2:                                #   in Loop: Header=BB3_1 Depth=1
	incq	%rbp
.LBB3_6:                                # %._crit_edge105.i
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	1(%rcx), %rdx
	leaq	1(%r15), %rax
	cmpq	%r14, %r13
	jb	.LBB3_1
# BB#7:                                 # %.preheader83.i
	cmpl	$2, 12(%rsp)            # 4-byte Folded Reload
	movq	%rcx, %r13
	jb	.LBB3_19
# BB#8:                                 # %.lr.ph98.i
	testq	%r14, %r14
	je	.LBB3_19
# BB#9:                                 # %.lr.ph98.split.us.i.preheader
	leaq	100(%r14), %rax
	movl	$1, %ebp
	testb	$1, 12(%rsp)            # 1-byte Folded Reload
	movq	%r12, %rcx
	jne	.LBB3_13
# BB#10:                                # %.lr.ph98.split.us.i.prol
	leaq	(%r12,%rax), %rcx
	movl	$1, %edi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_11:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%r12,%rsi), %ebx
	movb	%bl, (%rcx,%rsi)
	movl	%edi, %esi
	incl	%edi
	cmpq	%r14, %rsi
	jb	.LBB3_11
# BB#12:
	movl	$2, %ebp
.LBB3_13:                               # %.lr.ph98.split.us.i.prol.loopexit
	cmpl	$2, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB3_19
	.p2align	4, 0x90
.LBB3_14:                               # %.lr.ph98.split.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_15 Depth 2
                                        #     Child Loop BB3_17 Depth 2
	addq	%rax, %rcx
	movl	$1, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_15:                               #   Parent Loop BB3_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r12,%rdi), %ebx
	movb	%bl, (%rcx,%rdi)
	movl	%esi, %edi
	incl	%esi
	cmpq	%r14, %rdi
	jb	.LBB3_15
# BB#16:                                # %._crit_edge95.us.i
                                        #   in Loop: Header=BB3_14 Depth=1
	addq	%rax, %rcx
	movl	$1, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_17:                               #   Parent Loop BB3_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r12,%rdi), %ebx
	movb	%bl, (%rcx,%rdi)
	movl	%esi, %edi
	incl	%esi
	cmpq	%r14, %rdi
	jb	.LBB3_17
# BB#18:                                # %._crit_edge95.us.i.1
                                        #   in Loop: Header=BB3_14 Depth=1
	addl	$2, %ebp
	cmpl	12(%rsp), %ebp          # 4-byte Folded Reload
	jne	.LBB3_14
.LBB3_19:                               # %._crit_edge99.i
	movl	12(%rsp), %edi          # 4-byte Reload
	movq	%rdx, %rbx
	imulq	%rdx, %rdi
	shlq	$3, %rdi
	leaq	192(%rsp), %rsi
	callq	AllocateMemory
	movl	192(%rsp), %esi
	testl	%esi, %esi
	movq	%rax, 24(%rsp)          # 8-byte Spill
	je	.LBB3_21
# BB#20:
	movl	$.L.str.63, %edi
	callq	ReportError
	leaq	192(%rsp), %rsi
	movq	%r12, %rdi
	callq	FreeMemory
	xorl	%eax, %eax
	callq	ErrorExit
	movq	24(%rsp), %rax          # 8-byte Reload
.LBB3_21:                               # %.preheader82.i
	movq	%rbx, %rbp
	testq	%rbp, %rbp
	je	.LBB3_22
# BB#26:                                # %.lr.ph91.i.preheader
	testb	$3, %bpl
	je	.LBB3_27
# BB#28:                                # %.lr.ph91.i.prol.preheader
	movl	%ebp, %edx
	andl	$3, %edx
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_29:                               # %.lr.ph91.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, (%rax,%rbx,8)
	movzbl	(%r12,%rcx), %esi
	leaq	1(%rcx,%rsi), %rcx
	incq	%rbx
	cmpq	%rbx, %rdx
	jne	.LBB3_29
	jmp	.LBB3_30
.LBB3_22:
	xorl	%ebp, %ebp
	cmpl	$2, 12(%rsp)            # 4-byte Folded Reload
	jae	.LBB3_24
	jmp	.LBB3_46
.LBB3_27:
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
.LBB3_30:                               # %.lr.ph91.i.prol.loopexit
	cmpq	$3, %r13
	jb	.LBB3_23
# BB#31:                                # %.lr.ph91.i.preheader.new
	leaq	1(%r13), %rdx
	.p2align	4, 0x90
.LBB3_32:                               # %.lr.ph91.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, (%rax,%rbx,8)
	movzbl	(%r12,%rcx), %esi
	leaq	(%rcx,%rsi), %rdi
	leaq	1(%rcx,%rsi), %rcx
	movq	%rcx, 8(%rax,%rbx,8)
	movzbl	1(%r12,%rdi), %ecx
	leaq	1(%rcx,%rdi), %rsi
	leaq	2(%rcx,%rdi), %rcx
	movq	%rcx, 16(%rax,%rbx,8)
	movzbl	1(%r12,%rsi), %ecx
	leaq	1(%rcx,%rsi), %rdi
	leaq	2(%rcx,%rsi), %rcx
	movq	%rcx, 24(%rax,%rbx,8)
	movzbl	1(%r12,%rdi), %ecx
	leaq	2(%rcx,%rdi), %rcx
	addq	$4, %rbx
	cmpq	%rbx, %rdx
	jne	.LBB3_32
.LBB3_23:                               # %.preheader.i
	cmpl	$2, 12(%rsp)            # 4-byte Folded Reload
	jb	.LBB3_46
.LBB3_24:                               # %.lr.ph87.i.preheader
	testb	$1, 12(%rsp)            # 1-byte Folded Reload
	jne	.LBB3_25
# BB#33:                                # %.lr.ph87.i.prol
	movq	24(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rbp,8), %rcx
	movl	$2, %eax
	testq	%rbp, %rbp
	je	.LBB3_34
# BB#35:                                # %.lr.ph.i.preheader.prol
	movl	$1, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_36:                               # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rsi,8), %rdi
	movq	%rdi, (%rcx,%rsi,8)
	movl	%edx, %esi
	incl	%edx
	cmpq	%rbp, %rsi
	jb	.LBB3_36
# BB#37:
	movq	%rbp, %rdx
	cmpl	$2, 12(%rsp)            # 4-byte Folded Reload
	jne	.LBB3_39
	jmp	.LBB3_46
.LBB3_25:
	movl	$1, %eax
	movq	%rbp, %rdx
	movq	24(%rsp), %rcx          # 8-byte Reload
	cmpl	$2, 12(%rsp)            # 4-byte Folded Reload
	jne	.LBB3_39
	jmp	.LBB3_46
.LBB3_34:
	xorl	%edx, %edx
	cmpl	$2, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB3_46
	.p2align	4, 0x90
.LBB3_39:                               # %.lr.ph87.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_42 Depth 2
                                        #     Child Loop BB3_82 Depth 2
	leaq	(%rcx,%rdx,8), %rcx
	testq	%rdx, %rdx
	je	.LBB3_40
# BB#41:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB3_39 Depth=1
	movl	$1, %edx
	xorl	%esi, %esi
	movq	24(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_42:                               # %.lr.ph.i
                                        #   Parent Loop BB3_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rsi,8), %rdi
	movq	%rdi, (%rcx,%rsi,8)
	movl	%edx, %esi
	incl	%edx
	cmpq	%rbp, %rsi
	jb	.LBB3_42
# BB#43:                                #   in Loop: Header=BB3_39 Depth=1
	movq	%rbp, %rdx
	jmp	.LBB3_44
	.p2align	4, 0x90
.LBB3_40:                               #   in Loop: Header=BB3_39 Depth=1
	xorl	%edx, %edx
	movq	24(%rsp), %rbx          # 8-byte Reload
.LBB3_44:                               # %._crit_edge.i
                                        #   in Loop: Header=BB3_39 Depth=1
	leaq	(%rcx,%rdx,8), %rcx
	testq	%rdx, %rdx
	je	.LBB3_45
# BB#81:                                # %.lr.ph.i.preheader.1
                                        #   in Loop: Header=BB3_39 Depth=1
	movl	$1, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_82:                               # %.lr.ph.i.1
                                        #   Parent Loop BB3_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rsi,8), %rdi
	movq	%rdi, (%rcx,%rsi,8)
	movl	%edx, %esi
	incl	%edx
	cmpq	%rbp, %rsi
	jb	.LBB3_82
# BB#83:                                #   in Loop: Header=BB3_39 Depth=1
	movq	%rbp, %rdx
	jmp	.LBB3_84
	.p2align	4, 0x90
.LBB3_45:                               #   in Loop: Header=BB3_39 Depth=1
	xorl	%edx, %edx
.LBB3_84:                               # %._crit_edge.i.1
                                        #   in Loop: Header=BB3_39 Depth=1
	addl	$2, %eax
	cmpl	12(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB3_39
.LBB3_46:                               # %LoadStringArray.exit
	movq	%r15, 48(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	callq	StartStopwatch
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%rax, 120(%rsp)         # 8-byte Spill
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB3_80
# BB#47:                                # %.lr.ph
	movq	%r13, %rax
	shrq	%rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	addq	$100, %r14
	leaq	-1(%rdx), %rax
	movq	%rax, %rcx
	subq	%r13, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	leaq	-5(%rdx), %rcx
	shrq	$2, %rcx
	movq	%rax, 96(%rsp)          # 8-byte Spill
	andq	$-4, %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	incq	%rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	%rcx, 168(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1, %ecx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	leaq	(,%rdx,8), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rcx
	addq	$16, %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	%r14, 144(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB3_48:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_85 Depth 2
                                        #     Child Loop BB3_51 Depth 2
                                        #       Child Loop BB3_60 Depth 3
                                        #       Child Loop BB3_62 Depth 3
                                        #       Child Loop BB3_73 Depth 3
                                        #       Child Loop BB3_76 Depth 3
	movq	136(%rsp), %rbp         # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB3_49
	.p2align	4, 0x90
.LBB3_85:                               # %.lr.ph16.i
                                        #   Parent Loop BB3_48 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rcx
	movq	%r13, %r8
	callq	strsift
	movq	16(%rsp), %rdx          # 8-byte Reload
	decq	%rbp
	jne	.LBB3_85
.LBB3_49:                               # %.preheader.i26
                                        #   in Loop: Header=BB3_48 Depth=1
	testq	%r13, %r13
	leaq	192(%rsp), %r13
	je	.LBB3_79
# BB#50:                                # %.lr.ph.i27
                                        #   in Loop: Header=BB3_48 Depth=1
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, 104(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	%r12, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_51:                               #   Parent Loop BB3_48 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_60 Depth 3
                                        #       Child Loop BB3_62 Depth 3
                                        #       Child Loop BB3_73 Depth 3
                                        #       Child Loop BB3_76 Depth 3
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rbx, %r8
	callq	strsift
	movzbl	(%r12), %edx
	incq	%rdx
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	MoveMemory
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	(%r15,%rbx,8), %rax
	movzbl	(%r12,%rax), %r13d
	movq	(%r15), %rax
	movq	8(%r15), %rsi
	leaq	(%r12,%rax), %rcx
	movzbl	(%r12,%rax), %ebx
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	-8(%r15,%rax,8), %rax
	movzbl	(%r12,%rax), %edx
	leaq	1(%rax,%rdx), %rdx
	subq	%rsi, %rdx
	leaq	1(%r13,%rcx), %rdi
	addq	%r12, %rsi
	callq	MoveMemory
	movq	16(%rsp), %rsi          # 8-byte Reload
	cmpq	$2, %rsi
	jb	.LBB3_63
# BB#52:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB3_51 Depth=2
	movl	%r13d, %ecx
	subl	%ebx, %ecx
	movl	%ecx, %eax
	negl	%eax
	cmovll	%ecx, %eax
	movzbl	%al, %edx
	movq	%rdx, %rax
	negq	%rax
	testl	%ecx, %ecx
	cmovnsq	%rdx, %rax
	cmpq	$4, 96(%rsp)            # 8-byte Folded Reload
	jae	.LBB3_54
# BB#53:                                #   in Loop: Header=BB3_51 Depth=2
	movl	$1, %ecx
	jmp	.LBB3_62
	.p2align	4, 0x90
.LBB3_54:                               # %min.iters.checked53
                                        #   in Loop: Header=BB3_51 Depth=2
	movq	176(%rsp), %rdx         # 8-byte Reload
	testq	%rdx, %rdx
	je	.LBB3_55
# BB#56:                                # %vector.ph57
                                        #   in Loop: Header=BB3_51 Depth=2
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	cmpq	$0, 152(%rsp)           # 8-byte Folded Reload
	jne	.LBB3_57
# BB#58:                                # %vector.body49.prol
                                        #   in Loop: Header=BB3_51 Depth=2
	movdqu	8(%r15), %xmm1
	movdqu	24(%r15), %xmm2
	paddq	%xmm0, %xmm1
	paddq	%xmm0, %xmm2
	movdqu	%xmm1, 8(%r15)
	movdqu	%xmm2, 24(%r15)
	movl	$4, %ecx
	cmpq	$0, 168(%rsp)           # 8-byte Folded Reload
	jne	.LBB3_60
	jmp	.LBB3_61
.LBB3_55:                               #   in Loop: Header=BB3_51 Depth=2
	movl	$1, %ecx
	jmp	.LBB3_62
.LBB3_57:                               #   in Loop: Header=BB3_51 Depth=2
	xorl	%ecx, %ecx
	cmpq	$0, 168(%rsp)           # 8-byte Folded Reload
	je	.LBB3_61
	.p2align	4, 0x90
.LBB3_60:                               # %vector.body49
                                        #   Parent Loop BB3_48 Depth=1
                                        #     Parent Loop BB3_51 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	8(%r15,%rcx,8), %xmm1
	movdqu	24(%r15,%rcx,8), %xmm2
	paddq	%xmm0, %xmm1
	paddq	%xmm0, %xmm2
	movdqu	%xmm1, 8(%r15,%rcx,8)
	movdqu	%xmm2, 24(%r15,%rcx,8)
	movdqu	40(%r15,%rcx,8), %xmm1
	movdqu	56(%r15,%rcx,8), %xmm2
	paddq	%xmm0, %xmm1
	paddq	%xmm0, %xmm2
	movdqu	%xmm1, 40(%r15,%rcx,8)
	movdqu	%xmm2, 56(%r15,%rcx,8)
	addq	$8, %rcx
	cmpq	%rcx, %rdx
	jne	.LBB3_60
.LBB3_61:                               # %middle.block50
                                        #   in Loop: Header=BB3_51 Depth=2
	cmpq	%rdx, 96(%rsp)          # 8-byte Folded Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	je	.LBB3_63
	.p2align	4, 0x90
.LBB3_62:                               # %scalar.ph51
                                        #   Parent Loop BB3_48 Depth=1
                                        #     Parent Loop BB3_51 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addq	%rax, (%r15,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB3_62
.LBB3_63:                               # %stradjust.exit.i
                                        #   in Loop: Header=BB3_51 Depth=2
	movq	%r14, 112(%rsp)         # 8-byte Spill
	movq	(%r15), %rax
	movb	%r13b, (%r12,%rax)
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	(%r15,%r14,8), %rsi
	addq	%r12, %rsi
	leaq	1(%r13), %rdx
	movq	%r12, %rdi
	callq	MoveMemory
	movzbl	192(%rsp), %ebp
	movq	(%r15,%r14,8), %rcx
	leaq	(%r12,%rcx), %rax
	movzbl	(%r12,%rcx), %ecx
	movl	%ebp, %ebx
	subl	%ecx, %ebx
	movq	%r12, %rcx
	movl	%ebx, %r12d
	negl	%r12d
	cmovll	%ebx, %r12d
	cmpq	%r14, 40(%rsp)          # 8-byte Folded Reload
	jne	.LBB3_65
# BB#64:                                #   in Loop: Header=BB3_51 Depth=2
	movq	%rcx, %r12
	movq	%r14, %rbx
	movq	112(%rsp), %r14         # 8-byte Reload
	leaq	192(%rsp), %r13
	jmp	.LBB3_78
	.p2align	4, 0x90
.LBB3_65:                               #   in Loop: Header=BB3_51 Depth=2
	movq	16(%rsp), %r13          # 8-byte Reload
	movq	-8(%r15,%r13,8), %rdi
	movzbl	(%rcx,%rdi), %edx
	movq	8(%r15,%r14,8), %rsi
	leaq	1(%rdi,%rdx), %rdx
	subq	%rsi, %rdx
	movq	%rbp, 184(%rsp)         # 8-byte Spill
	leaq	1(%rax,%rbp), %rdi
	addq	%rcx, %rsi
	callq	MoveMemory
	movq	%r13, %r11
	leaq	192(%rsp), %r13
	leaq	1(%r14), %rax
	cmpq	%r11, %rax
	movq	112(%rsp), %r14         # 8-byte Reload
	jae	.LBB3_77
# BB#66:                                # %.lr.ph.i4.i
                                        #   in Loop: Header=BB3_51 Depth=2
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	leaq	(%rcx,%rdx), %r10
	movzbl	%r12b, %esi
	movq	%rsi, %rcx
	negq	%rcx
	testl	%ebx, %ebx
	cmovnsq	%rsi, %rcx
	cmpq	$4, %r10
	jb	.LBB3_76
# BB#67:                                # %min.iters.checked
                                        #   in Loop: Header=BB3_51 Depth=2
	movq	%r10, %r8
	andq	$-4, %r8
	movq	%r10, %r9
	andq	$-4, %r9
	je	.LBB3_76
# BB#68:                                # %vector.ph
                                        #   in Loop: Header=BB3_51 Depth=2
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	72(%rsp), %rsi          # 8-byte Reload
	leaq	-4(%rdx,%rsi), %rdi
	shrq	$2, %rdi
	movd	%rcx, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	testb	$1, %dil
	jne	.LBB3_69
# BB#70:                                # %vector.body.prol
                                        #   in Loop: Header=BB3_51 Depth=2
	movq	32(%rsp), %rdx          # 8-byte Reload
	movdqu	8(%r15,%rdx,8), %xmm1
	movdqu	24(%r15,%rdx,8), %xmm2
	paddq	%xmm0, %xmm1
	paddq	%xmm0, %xmm2
	movdqu	%xmm1, 8(%r15,%rdx,8)
	movdqu	%xmm2, 24(%r15,%rdx,8)
	movl	$4, %esi
	testq	%rdi, %rdi
	jne	.LBB3_72
	jmp	.LBB3_74
.LBB3_69:                               #   in Loop: Header=BB3_51 Depth=2
	xorl	%esi, %esi
	testq	%rdi, %rdi
	je	.LBB3_74
.LBB3_72:                               # %vector.ph.new
                                        #   in Loop: Header=BB3_51 Depth=2
	movq	104(%rsp), %rdi         # 8-byte Reload
	andq	$-4, %rdi
	subq	%rsi, %rdi
	shlq	$3, %rsi
	movq	80(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_73:                               # %vector.body
                                        #   Parent Loop BB3_48 Depth=1
                                        #     Parent Loop BB3_51 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	(%rbx,%rsi), %rdx
	movdqu	-16(%rdx,%r14,8), %xmm1
	movdqu	(%rdx,%r14,8), %xmm2
	paddq	%xmm0, %xmm1
	paddq	%xmm0, %xmm2
	movdqu	%xmm1, -16(%rdx,%r14,8)
	movdqu	%xmm2, (%rdx,%r14,8)
	movdqu	16(%rdx,%r14,8), %xmm1
	movdqu	32(%rdx,%r14,8), %xmm2
	paddq	%xmm0, %xmm1
	paddq	%xmm0, %xmm2
	movdqu	%xmm1, 16(%rdx,%r14,8)
	movdqu	%xmm2, 32(%rdx,%r14,8)
	addq	$64, %rbx
	addq	$-8, %rdi
	jne	.LBB3_73
.LBB3_74:                               # %middle.block
                                        #   in Loop: Header=BB3_51 Depth=2
	cmpq	%r9, %r10
	je	.LBB3_77
# BB#75:                                #   in Loop: Header=BB3_51 Depth=2
	addq	%r8, %rax
	.p2align	4, 0x90
.LBB3_76:                               # %scalar.ph
                                        #   Parent Loop BB3_48 Depth=1
                                        #     Parent Loop BB3_51 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addq	%rcx, (%r15,%rax,8)
	incq	%rax
	cmpq	%rax, %r11
	jne	.LBB3_76
.LBB3_77:                               # %._crit_edge.i9.i
                                        #   in Loop: Header=BB3_51 Depth=2
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	(%r15,%rbx,8), %rax
	movq	64(%rsp), %r12          # 8-byte Reload
	addq	%r12, %rax
	movq	184(%rsp), %rbp         # 8-byte Reload
.LBB3_78:                               # %stradjust.exit11.i
                                        #   in Loop: Header=BB3_51 Depth=2
	movb	%bpl, (%rax)
	movq	(%r15,%rbx,8), %rdi
	addq	%r12, %rdi
	incq	%rbp
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	MoveMemory
	incq	72(%rsp)                # 8-byte Folded Spill
	incq	104(%rsp)               # 8-byte Folded Spill
	decq	%r14
	decq	%rbx
	movq	16(%rsp), %rdx          # 8-byte Reload
	jne	.LBB3_51
.LBB3_79:                               # %StrHeapSort.exit
                                        #   in Loop: Header=BB3_48 Depth=1
	movq	128(%rsp), %rcx         # 8-byte Reload
	addq	%rcx, %r15
	movq	144(%rsp), %r14         # 8-byte Reload
	addq	%r14, %r12
	movq	88(%rsp), %rax          # 8-byte Reload
	incl	%eax
	addq	%rcx, 80(%rsp)          # 8-byte Folded Spill
	movq	%rax, 88(%rsp)          # 8-byte Spill
	cmpl	12(%rsp), %eax          # 4-byte Folded Reload
	movq	40(%rsp), %r13          # 8-byte Reload
	jne	.LBB3_48
.LBB3_80:                               # %._crit_edge
	movq	120(%rsp), %rdi         # 8-byte Reload
	callq	StopStopwatch
	movq	%rax, %rbx
	leaq	192(%rsp), %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	FreeMemory
	movq	%rbx, %rax
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	DoStringSortIteration, .Lfunc_end3-DoStringSortIteration
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI4_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_2:
	.quad	4698119520840056832     # double 1.25E+6
	.text
	.globl	DoBitops
	.p2align	4, 0x90
	.type	DoBitops,@function
DoBitops:                               # @DoBitops
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 80
.Lcfi46:
	.cfi_offset %rbx, -48
.Lcfi47:
	.cfi_offset %r12, -40
.Lcfi48:
	.cfi_offset %r13, -32
.Lcfi49:
	.cfi_offset %r14, -24
.Lcfi50:
	.cfi_offset %r15, -16
	movq	global_bitopstruct+32(%rip), %rdi
	shlq	$3, %rdi
	movl	global_bitopstruct(%rip), %ebx
	leaq	12(%rsp), %rsi
	callq	AllocateMemory
	movq	%rax, %r14
	cmpl	$0, %ebx
	movl	12(%rsp), %esi
	je	.LBB4_1
# BB#8:
	testl	%esi, %esi
	je	.LBB4_10
# BB#9:
	movl	$.L.str.53, %edi
	callq	ReportError
	xorl	%eax, %eax
	callq	ErrorExit
.LBB4_10:
	movq	global_bitopstruct+24(%rip), %rdi
	shlq	$4, %rdi
	leaq	12(%rsp), %rsi
	callq	AllocateMemory
	movq	%rax, %r13
	movl	12(%rsp), %esi
	testl	%esi, %esi
	je	.LBB4_12
# BB#11:
	movl	$.L.str.53, %edi
	callq	ReportError
	leaq	12(%rsp), %rsi
	movq	%r14, %rdi
	callq	FreeMemory
	xorl	%eax, %eax
	callq	ErrorExit
	jmp	.LBB4_12
.LBB4_1:
	testl	%esi, %esi
	je	.LBB4_3
# BB#2:
	movl	$.L.str.53, %edi
	callq	ReportError
	xorl	%eax, %eax
	callq	ErrorExit
.LBB4_3:                                # %.preheader.preheader
	movl	$30, %edi
	leaq	12(%rsp), %r15
	leaq	24(%rsp), %r12
	jmp	.LBB4_4
	.p2align	4, 0x90
.LBB4_7:                                #   in Loop: Header=BB4_4 Depth=1
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	FreeMemory
	movq	global_bitopstruct+24(%rip), %rdi
	addq	$100, %rdi
.LBB4_4:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdi, global_bitopstruct+24(%rip)
	shlq	$4, %rdi
	movq	%r15, %rsi
	callq	AllocateMemory
	movq	%rax, %r13
	movl	12(%rsp), %esi
	testl	%esi, %esi
	je	.LBB4_6
# BB#5:                                 #   in Loop: Header=BB4_4 Depth=1
	movl	$.L.str.53, %edi
	callq	ReportError
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	FreeMemory
	xorl	%eax, %eax
	callq	ErrorExit
.LBB4_6:                                #   in Loop: Header=BB4_4 Depth=1
	movq	global_bitopstruct+24(%rip), %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r12, %rcx
	callq	DoBitfieldIteration
	cltq
	cmpq	global_min_ticks(%rip), %rax
	jbe	.LBB4_7
.LBB4_12:                               # %.loopexit
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	xorl	%ebx, %ebx
	leaq	24(%rsp), %r15
	.p2align	4, 0x90
.LBB4_13:                               # =>This Inner Loop Header: Depth=1
	movq	global_bitopstruct+24(%rip), %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r15, %rcx
	callq	DoBitfieldIteration
	addq	%rax, %rbx
	movq	24(%rsp), %xmm0         # xmm0 = mem[0],zero
	punpckldq	.LCPI4_0(%rip), %xmm0 # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	subpd	.LCPI4_1(%rip), %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm1
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	.LCPI4_2(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB4_13
# BB#14:
	leaq	12(%rsp), %r15
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	FreeMemory
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	FreeMemory
	movq	%rbx, %rdi
	callq	TicksToFracSecs
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, global_bitopstruct+16(%rip)
	cmpl	$0, global_bitopstruct(%rip)
	jne	.LBB4_16
# BB#15:
	movl	$1, global_bitopstruct(%rip)
.LBB4_16:
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	DoBitops, .Lfunc_end4-DoBitops
	.cfi_endproc

	.p2align	4, 0x90
	.type	DoBitfieldIteration,@function
DoBitfieldIteration:                    # @DoBitfieldIteration
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi57:
	.cfi_def_cfa_offset 64
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r13
	movq	$0, (%r12)
	movl	$13, %edi
	callq	randnum
	cmpq	$0, global_bitopstruct+32(%rip)
	je	.LBB5_3
# BB#1:                                 # %.lr.ph59.preheader
	xorl	%eax, %eax
	movabsq	$6148914691236517205, %rcx # imm = 0x5555555555555555
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph59
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, (%r13,%rax,8)
	incq	%rax
	cmpq	global_bitopstruct+32(%rip), %rax
	jb	.LBB5_2
.LBB5_3:                                # %._crit_edge60
	movl	$13, %edi
	callq	randnum
	testq	%r15, %r15
	jle	.LBB5_31
# BB#4:                                 # %.lr.ph55.preheader
	leaq	8(%r14), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_5:                                # %.lr.ph55
                                        # =>This Inner Loop Header: Depth=1
	movl	$262140, %edi           # imm = 0x3FFFC
	callq	abs_randwc
	movl	%eax, %ecx
	movq	%rcx, -8(%rbx)
	movl	$262140, %edi           # imm = 0x3FFFC
	subl	%eax, %edi
	callq	abs_randwc
	movl	%eax, %eax
	movq	%rax, (%rbx)
	addq	%rax, (%r12)
	incq	%rbp
	addq	$16, %rbx
	cmpq	%rbp, %r15
	jne	.LBB5_5
# BB#6:                                 # %._crit_edge56
	xorl	%eax, %eax
	callq	StartStopwatch
	movq	%rax, %rdi
	testq	%r15, %r15
	jle	.LBB5_30
# BB#7:                                 # %.lr.ph.preheader
	xorl	%esi, %esi
	movabsq	$6148914691236517206, %r8 # imm = 0x5555555555555556
	.p2align	4, 0x90
.LBB5_8:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_16 Depth 2
                                        #     Child Loop BB5_22 Depth 2
                                        #     Child Loop BB5_28 Depth 2
	movq	%rsi, %rax
	imulq	%r8
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	leaq	(%rax,%rax,2), %rcx
	movq	%rsi, %rax
	subq	%rcx, %rax
	cmpq	$2, %rax
	je	.LBB5_23
# BB#9:                                 # %.lr.ph
                                        #   in Loop: Header=BB5_8 Depth=1
	cmpq	$1, %rax
	je	.LBB5_17
# BB#10:                                # %.lr.ph
                                        #   in Loop: Header=BB5_8 Depth=1
	testq	%rax, %rax
	jne	.LBB5_29
# BB#11:                                #   in Loop: Header=BB5_8 Depth=1
	leaq	(%r14,%rsi,8), %rax
	movq	8(%rax,%rsi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB5_29
# BB#12:                                # %.lr.ph.split.i.preheader
                                        #   in Loop: Header=BB5_8 Depth=1
	movq	(%rax,%rsi,8), %rbx
	testb	$1, %bpl
	jne	.LBB5_14
# BB#13:                                #   in Loop: Header=BB5_8 Depth=1
	movq	%rbp, %rdx
	cmpq	$1, %rbp
	jne	.LBB5_16
	jmp	.LBB5_29
	.p2align	4, 0x90
.LBB5_23:                               #   in Loop: Header=BB5_8 Depth=1
	leaq	(%r14,%rsi,8), %rax
	movq	8(%rax,%rsi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB5_29
# BB#24:                                # %.lr.ph.i46.preheader
                                        #   in Loop: Header=BB5_8 Depth=1
	movq	(%rax,%rsi,8), %rbx
	testb	$1, %bpl
	jne	.LBB5_26
# BB#25:                                #   in Loop: Header=BB5_8 Depth=1
	movq	%rbp, %rdx
	cmpq	$1, %rbp
	jne	.LBB5_28
	jmp	.LBB5_29
	.p2align	4, 0x90
.LBB5_17:                               #   in Loop: Header=BB5_8 Depth=1
	leaq	(%r14,%rsi,8), %rax
	movq	8(%rax,%rsi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB5_29
# BB#18:                                # %.lr.ph.split.us.i.preheader
                                        #   in Loop: Header=BB5_8 Depth=1
	movq	(%rax,%rsi,8), %rbx
	testb	$1, %bpl
	jne	.LBB5_20
# BB#19:                                #   in Loop: Header=BB5_8 Depth=1
	movq	%rbp, %rdx
	cmpq	$1, %rbp
	jne	.LBB5_22
	jmp	.LBB5_29
.LBB5_14:                               # %.lr.ph.split.i.prol
                                        #   in Loop: Header=BB5_8 Depth=1
	leaq	-1(%rbp), %rdx
	movq	%rbx, %r9
	shrq	$6, %r9
	movl	$1, %eax
	movl	%ebx, %ecx
	shlq	%cl, %rax
	orq	%rax, (%r13,%r9,8)
	incq	%rbx
	cmpq	$1, %rbp
	je	.LBB5_29
	.p2align	4, 0x90
.LBB5_16:                               # %.lr.ph.split.i
                                        #   Parent Loop BB5_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rax
	shrq	$6, %rax
	movl	$1, %ebp
	movl	%ebx, %ecx
	shlq	%cl, %rbp
	orq	%rbp, (%r13,%rax,8)
	leaq	1(%rbx), %rcx
	movq	%rcx, %rax
	shrq	$6, %rax
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rbp
	orq	%rbp, (%r13,%rax,8)
	addq	$2, %rbx
	addq	$-2, %rdx
	jne	.LBB5_16
	jmp	.LBB5_29
.LBB5_26:                               # %.lr.ph.i46.prol
                                        #   in Loop: Header=BB5_8 Depth=1
	leaq	-1(%rbp), %rdx
	movq	%rbx, %r9
	shrq	$6, %r9
	movl	$1, %eax
	movl	%ebx, %ecx
	shlq	%cl, %rax
	xorq	%rax, (%r13,%r9,8)
	incq	%rbx
	cmpq	$1, %rbp
	je	.LBB5_29
	.p2align	4, 0x90
.LBB5_28:                               # %.lr.ph.i46
                                        #   Parent Loop BB5_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rax
	shrq	$6, %rax
	movl	$1, %ebp
	movl	%ebx, %ecx
	shlq	%cl, %rbp
	xorq	%rbp, (%r13,%rax,8)
	leaq	1(%rbx), %rcx
	movq	%rcx, %rax
	shrq	$6, %rax
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rbp
	xorq	%rbp, (%r13,%rax,8)
	addq	$2, %rbx
	addq	$-2, %rdx
	jne	.LBB5_28
	jmp	.LBB5_29
.LBB5_20:                               # %.lr.ph.split.us.i.prol
                                        #   in Loop: Header=BB5_8 Depth=1
	leaq	-1(%rbp), %rdx
	movq	%rbx, %r9
	shrq	$6, %r9
	movq	$-2, %rax
	movl	%ebx, %ecx
	rolq	%cl, %rax
	andq	%rax, (%r13,%r9,8)
	incq	%rbx
	cmpq	$1, %rbp
	je	.LBB5_29
	.p2align	4, 0x90
.LBB5_22:                               # %.lr.ph.split.us.i
                                        #   Parent Loop BB5_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rax
	shrq	$6, %rax
	movq	$-2, %rbp
	movl	%ebx, %ecx
	rolq	%cl, %rbp
	andq	%rbp, (%r13,%rax,8)
	leaq	1(%rbx), %rcx
	movq	%rcx, %rax
	shrq	$6, %rax
	movq	$-2, %rbp
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	rolq	%cl, %rbp
	andq	%rbp, (%r13,%rax,8)
	addq	$2, %rbx
	addq	$-2, %rdx
	jne	.LBB5_22
	.p2align	4, 0x90
.LBB5_29:                               # %ToggleBitRun.exit
                                        #   in Loop: Header=BB5_8 Depth=1
	incq	%rsi
	cmpq	%r15, %rsi
	jne	.LBB5_8
	jmp	.LBB5_30
.LBB5_31:                               # %._crit_edge56.thread
	xorl	%eax, %eax
	callq	StartStopwatch
	movq	%rax, %rdi
.LBB5_30:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	StopStopwatch           # TAILCALL
.Lfunc_end5:
	.size	DoBitfieldIteration, .Lfunc_end5-DoBitfieldIteration
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4607182418800017408     # double 1
.LCPI6_1:
	.quad	4634978072750194688     # double 75
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_2:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI6_3:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.text
	.globl	DoEmFloat
	.p2align	4, 0x90
	.type	DoEmFloat,@function
DoEmFloat:                              # @DoEmFloat
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi69:
	.cfi_def_cfa_offset 64
.Lcfi70:
	.cfi_offset %rbx, -48
.Lcfi71:
	.cfi_offset %r12, -40
.Lcfi72:
	.cfi_offset %r13, -32
.Lcfi73:
	.cfi_offset %r14, -24
.Lcfi74:
	.cfi_offset %r15, -16
	movq	global_emfloatstruct+16(%rip), %rax
	shlq	$2, %rax
	leaq	(%rax,%rax,2), %rdi
	leaq	4(%rsp), %rsi
	callq	AllocateMemory
	movq	%rax, %r14
	movl	4(%rsp), %esi
	testl	%esi, %esi
	je	.LBB6_2
# BB#1:
	movl	$.L.str.54, %edi
	callq	ReportError
	xorl	%eax, %eax
	callq	ErrorExit
.LBB6_2:
	movq	global_emfloatstruct+16(%rip), %rax
	shlq	$2, %rax
	leaq	(%rax,%rax,2), %rdi
	leaq	4(%rsp), %rsi
	callq	AllocateMemory
	movq	%rax, %r15
	movl	4(%rsp), %esi
	testl	%esi, %esi
	je	.LBB6_4
# BB#3:
	movl	$.L.str.54, %edi
	callq	ReportError
	leaq	4(%rsp), %rsi
	movq	%r14, %rdi
	callq	FreeMemory
	xorl	%eax, %eax
	callq	ErrorExit
.LBB6_4:
	movq	global_emfloatstruct+16(%rip), %rax
	shlq	$2, %rax
	leaq	(%rax,%rax,2), %rdi
	leaq	4(%rsp), %rsi
	callq	AllocateMemory
	movq	%rax, %r12
	movl	4(%rsp), %esi
	testl	%esi, %esi
	je	.LBB6_6
# BB#5:
	movl	$.L.str.54, %edi
	callq	ReportError
	leaq	4(%rsp), %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	xorl	%eax, %eax
	callq	ErrorExit
.LBB6_6:
	movq	global_emfloatstruct+16(%rip), %rcx
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	SetupCPUEmFloatArrays
	cmpl	$0, global_emfloatstruct(%rip)
	jne	.LBB6_11
# BB#7:
	movq	$0, global_emfloatstruct+24(%rip)
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB6_8:                                # =>This Inner Loop Header: Depth=1
	movq	global_emfloatstruct+16(%rip), %rcx
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%rbx, %r8
	callq	DoEmFloatIteration
	cmpq	global_min_ticks(%rip), %rax
	ja	.LBB6_9
# BB#10:                                #   in Loop: Header=BB6_8 Depth=1
	addq	%rbx, %rbx
	cmpq	$500000, %rbx           # imm = 0x7A120
	jb	.LBB6_8
.LBB6_11:                               # %thread-pre-split
	movq	global_emfloatstruct+24(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB6_14
	jmp	.LBB6_13
.LBB6_9:
	movq	%rbx, global_emfloatstruct+24(%rip)
	testq	%rbx, %rbx
	jne	.LBB6_14
.LBB6_13:
	movl	$.Lstr.1, %edi
	callq	puts
	leaq	4(%rsp), %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	xorl	%eax, %eax
	callq	ErrorExit
.LBB6_14:                               # %.preheader.preheader
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_15:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	global_emfloatstruct+16(%rip), %rcx
	movq	global_emfloatstruct+24(%rip), %r8
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	DoEmFloatIteration
	addq	%rax, %rbx
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	.LCPI6_0(%rip), %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	.LCPI6_1(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB6_15
# BB#16:
	leaq	4(%rsp), %r13
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	FreeMemory
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	FreeMemory
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	FreeMemory
	movq	global_emfloatstruct+24(%rip), %xmm0 # xmm0 = mem[0],zero
	punpckldq	.LCPI6_2(%rip), %xmm0 # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	subpd	.LCPI6_3(%rip), %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm1
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	TicksToFracSecs
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, global_emfloatstruct+32(%rip)
	cmpl	$0, global_emfloatstruct(%rip)
	jne	.LBB6_18
# BB#17:
	movl	$1, global_emfloatstruct(%rip)
.LBB6_18:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	DoEmFloat, .Lfunc_end6-DoEmFloat
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI7_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI7_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI7_2:
	.quad	-4616189618054758400    # double -1
.LCPI7_3:
	.quad	4666723172467343360     # double 1.0E+4
	.text
	.globl	DoFourier
	.p2align	4, 0x90
	.type	DoFourier,@function
DoFourier:                              # @DoFourier
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi75:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi76:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi77:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi79:
	.cfi_def_cfa_offset 64
.Lcfi80:
	.cfi_offset %rbx, -40
.Lcfi81:
	.cfi_offset %r12, -32
.Lcfi82:
	.cfi_offset %r14, -24
.Lcfi83:
	.cfi_offset %r15, -16
	cmpl	$0, global_fourierstruct(%rip)
	je	.LBB7_1
# BB#8:
	movq	global_fourierstruct+16(%rip), %rdi
	shlq	$3, %rdi
	leaq	12(%rsp), %rsi
	callq	AllocateMemory
	movq	%rax, %r14
	movl	12(%rsp), %esi
	testl	%esi, %esi
	je	.LBB7_10
# BB#9:
	movl	$.L.str.56, %edi
	callq	ReportError
	xorl	%eax, %eax
	callq	ErrorExit
.LBB7_10:
	movq	global_fourierstruct+16(%rip), %rdi
	shlq	$3, %rdi
	leaq	12(%rsp), %rsi
	callq	AllocateMemory
	movq	%rax, %r12
	movl	12(%rsp), %esi
	testl	%esi, %esi
	je	.LBB7_12
# BB#11:
	movl	$.L.str.56, %edi
	callq	ReportError
	leaq	12(%rsp), %rsi
	movq	%r14, %rdi
	callq	FreeMemory
	xorl	%eax, %eax
	callq	ErrorExit
	jmp	.LBB7_12
.LBB7_1:                                # %.preheader.preheader
	movl	$100, %edi
	leaq	12(%rsp), %r15
	jmp	.LBB7_2
	.p2align	4, 0x90
.LBB7_7:                                #   in Loop: Header=BB7_2 Depth=1
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	FreeMemory
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	FreeMemory
	movq	global_fourierstruct+16(%rip), %rdi
	addq	$50, %rdi
.LBB7_2:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdi, global_fourierstruct+16(%rip)
	shlq	$3, %rdi
	movq	%r15, %rsi
	callq	AllocateMemory
	movq	%rax, %r14
	movl	12(%rsp), %esi
	testl	%esi, %esi
	je	.LBB7_4
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	movl	$.L.str.56, %edi
	callq	ReportError
	xorl	%eax, %eax
	callq	ErrorExit
.LBB7_4:                                #   in Loop: Header=BB7_2 Depth=1
	movq	global_fourierstruct+16(%rip), %rdi
	shlq	$3, %rdi
	movq	%r15, %rsi
	callq	AllocateMemory
	movq	%rax, %r12
	movl	12(%rsp), %esi
	testl	%esi, %esi
	je	.LBB7_6
# BB#5:                                 #   in Loop: Header=BB7_2 Depth=1
	movl	$.L.str.56, %edi
	callq	ReportError
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	FreeMemory
	xorl	%eax, %eax
	callq	ErrorExit
.LBB7_6:                                #   in Loop: Header=BB7_2 Depth=1
	movq	global_fourierstruct+16(%rip), %rdx
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	DoFPUTransIteration
	cmpq	global_min_ticks(%rip), %rax
	jbe	.LBB7_7
.LBB7_12:                               # %.loopexit
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movq	global_fourierstruct+16(%rip), %rdx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_13:                               # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	DoFPUTransIteration
	addq	%rax, %rbx
	movq	global_fourierstruct+16(%rip), %rdx
	movd	%rdx, %xmm0
	punpckldq	.LCPI7_0(%rip), %xmm0 # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	subpd	.LCPI7_1(%rip), %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm1
	addsd	%xmm1, %xmm1
	addsd	.LCPI7_2(%rip), %xmm1
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	.LCPI7_3(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB7_13
# BB#14:
	leaq	12(%rsp), %r15
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	FreeMemory
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	FreeMemory
	movq	%rbx, %rdi
	callq	TicksToFracSecs
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, global_fourierstruct+24(%rip)
	cmpl	$0, global_fourierstruct(%rip)
	jne	.LBB7_16
# BB#15:
	movl	$1, global_fourierstruct(%rip)
.LBB7_16:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	DoFourier, .Lfunc_end7-DoFourier
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI8_0:
	.quad	4602678819172646912     # double 0.5
.LCPI8_1:
	.quad	4576918229304087675     # double 0.01
.LCPI8_2:
	.quad	4607182418800017408     # double 1
.LCPI8_3:
	.quad	4616752568008179712     # double 4.5
.LCPI8_6:
	.quad	4614256656552045848     # double 3.1415926535897931
.LCPI8_7:
	.quad	4621256167635550208     # double 9
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_4:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI8_5:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.text
	.p2align	4, 0x90
	.type	DoFPUTransIteration,@function
DoFPUTransIteration:                    # @DoFPUTransIteration
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi87:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi88:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi90:
	.cfi_def_cfa_offset 144
.Lcfi91:
	.cfi_offset %rbx, -56
.Lcfi92:
	.cfi_offset %r12, -48
.Lcfi93:
	.cfi_offset %r13, -40
.Lcfi94:
	.cfi_offset %r14, -32
.Lcfi95:
	.cfi_offset %r15, -24
.Lcfi96:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r13
	xorl	%eax, %eax
	callq	StartStopwatch
	movq	%rax, %r12
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	.LCPI8_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movl	$-198, %ebx
	movsd	.LCPI8_1(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB8_1:                                # %thefunction.exit.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm1, 32(%rsp)         # 8-byte Spill
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	addsd	%xmm0, %xmm2
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	movapd	%xmm2, %xmm0
	movsd	.LCPI8_2(%rip), %xmm1   # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm0
	movapd	%xmm2, %xmm1
	callq	pow
	addsd	32(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	.LCPI8_1(%rip), %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movapd	%xmm1, %xmm0
	addsd	.LCPI8_2(%rip), %xmm0
	callq	pow
	addsd	32(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	.LCPI8_1(%rip), %xmm1
	movapd	%xmm1, %xmm0
	addsd	.LCPI8_2(%rip), %xmm0
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	callq	pow
	movapd	%xmm0, %xmm1
	movsd	.LCPI8_1(%rip), %xmm0   # xmm0 = mem[0],zero
	addsd	32(%rsp), %xmm1         # 8-byte Folded Reload
	addl	$3, %ebx
	jne	.LBB8_1
# BB#2:                                 # %TrapezoidIntegrate.exit
	addsd	.LCPI8_3(%rip), %xmm1
	mulsd	.LCPI8_1(%rip), %xmm1
	mulsd	.LCPI8_0(%rip), %xmm1
	movsd	%xmm1, (%r13)
	cmpq	$2, %r15
	jb	.LBB8_9
# BB#3:                                 # %.lr.ph.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB8_4:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_5 Depth 2
                                        #     Child Loop BB8_7 Depth 2
	movd	%rbx, %xmm0
	punpckldq	.LCPI8_4(%rip), %xmm0 # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	subpd	.LCPI8_5(%rip), %xmm0
	pshufd	$78, %xmm0, %xmm2       # xmm2 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm2
	mulsd	.LCPI8_6(%rip), %xmm2
	xorpd	%xmm3, %xmm3
	movapd	%xmm2, 32(%rsp)         # 16-byte Spill
	movapd	%xmm2, %xmm0
	xorpd	%xmm2, %xmm2
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	mulsd	%xmm3, %xmm0
	movapd	%xmm0, 64(%rsp)         # 16-byte Spill
	callq	cos
	mulsd	.LCPI8_0(%rip), %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movl	$-198, %ebp
	.p2align	4, 0x90
.LBB8_5:                                # %thefunction.exit.split.split.us.i
                                        #   Parent Loop BB8_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	.LCPI8_1(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	addsd	%xmm0, %xmm2
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	movapd	%xmm2, %xmm0
	movsd	.LCPI8_2(%rip), %xmm1   # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm0
	movapd	%xmm2, %xmm1
	callq	pow
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movapd	32(%rsp), %xmm0         # 16-byte Reload
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	callq	cos
	mulsd	24(%rsp), %xmm0         # 8-byte Folded Reload
	addsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	.LCPI8_1(%rip), %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movapd	%xmm1, %xmm0
	addsd	.LCPI8_2(%rip), %xmm0
	callq	pow
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movapd	32(%rsp), %xmm0         # 16-byte Reload
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	callq	cos
	mulsd	24(%rsp), %xmm0         # 8-byte Folded Reload
	addsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	.LCPI8_1(%rip), %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movapd	%xmm1, %xmm0
	addsd	.LCPI8_2(%rip), %xmm0
	callq	pow
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movapd	32(%rsp), %xmm0         # 16-byte Reload
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	callq	cos
	mulsd	24(%rsp), %xmm0         # 8-byte Folded Reload
	addsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	addl	$3, %ebp
	jne	.LBB8_5
# BB#6:                                 # %TrapezoidIntegrate.exit17
                                        #   in Loop: Header=BB8_4 Depth=1
	movapd	32(%rsp), %xmm0         # 16-byte Reload
	addsd	%xmm0, %xmm0
	movapd	%xmm0, 48(%rsp)         # 16-byte Spill
	callq	cos
	mulsd	.LCPI8_7(%rip), %xmm0
	movsd	.LCPI8_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	mulsd	.LCPI8_1(%rip), %xmm1
	movsd	%xmm1, (%r13,%rbx,8)
	movapd	64(%rsp), %xmm0         # 16-byte Reload
	callq	sin
	mulsd	.LCPI8_0(%rip), %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movl	$-198, %ebp
	.p2align	4, 0x90
.LBB8_7:                                # %thefunction.exit.split.split.split.us.i
                                        #   Parent Loop BB8_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	.LCPI8_1(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	addsd	%xmm0, %xmm2
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	movapd	%xmm2, %xmm0
	movsd	.LCPI8_2(%rip), %xmm1   # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm0
	movapd	%xmm2, %xmm1
	callq	pow
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movapd	32(%rsp), %xmm0         # 16-byte Reload
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	callq	sin
	mulsd	24(%rsp), %xmm0         # 8-byte Folded Reload
	addsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	.LCPI8_1(%rip), %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movapd	%xmm1, %xmm0
	addsd	.LCPI8_2(%rip), %xmm0
	callq	pow
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movapd	32(%rsp), %xmm0         # 16-byte Reload
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	callq	sin
	mulsd	24(%rsp), %xmm0         # 8-byte Folded Reload
	addsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	.LCPI8_1(%rip), %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movapd	%xmm1, %xmm0
	addsd	.LCPI8_2(%rip), %xmm0
	callq	pow
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movapd	32(%rsp), %xmm0         # 16-byte Reload
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	callq	sin
	mulsd	24(%rsp), %xmm0         # 8-byte Folded Reload
	addsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	addl	$3, %ebp
	jne	.LBB8_7
# BB#8:                                 # %TrapezoidIntegrate.exit18
                                        #   in Loop: Header=BB8_4 Depth=1
	movapd	48(%rsp), %xmm0         # 16-byte Reload
	callq	sin
	mulsd	.LCPI8_7(%rip), %xmm0
	movsd	.LCPI8_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	16(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	addsd	%xmm0, %xmm2
	movsd	.LCPI8_1(%rip), %xmm0   # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, (%r14,%rbx,8)
	incq	%rbx
	cmpq	%r15, %rbx
	jne	.LBB8_4
.LBB8_9:                                # %._crit_edge
	movq	%r12, %rdi
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	StopStopwatch           # TAILCALL
.Lfunc_end8:
	.size	DoFPUTransIteration, .Lfunc_end8-DoFPUTransIteration
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI9_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI9_2:
	.quad	4620693217682128896     # double 8
	.text
	.globl	DoAssign
	.p2align	4, 0x90
	.type	DoAssign,@function
DoAssign:                               # @DoAssign
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi100:
	.cfi_def_cfa_offset 64
.Lcfi101:
	.cfi_offset %rbx, -32
.Lcfi102:
	.cfi_offset %r14, -24
.Lcfi103:
	.cfi_offset %r15, -16
	cmpl	$0, global_assignstruct(%rip)
	je	.LBB9_1
# BB#6:
	imulq	$81608, global_assignstruct+16(%rip), %rdi # imm = 0x13EC8
	leaq	12(%rsp), %rsi
	callq	AllocateMemory
	movq	%rax, %rbx
	movl	12(%rsp), %esi
	testl	%esi, %esi
	je	.LBB9_8
# BB#7:
	movl	$.L.str.57, %edi
	callq	ReportError
	leaq	12(%rsp), %rsi
	movq	%rbx, %rdi
	callq	FreeMemory
	xorl	%eax, %eax
	callq	ErrorExit
	jmp	.LBB9_8
.LBB9_1:                                # %.preheader.preheader
	movl	$1, %eax
	leaq	12(%rsp), %r14
	jmp	.LBB9_2
	.p2align	4, 0x90
.LBB9_5:                                #   in Loop: Header=BB9_2 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	FreeMemory
	movq	global_assignstruct+16(%rip), %rax
	incq	%rax
.LBB9_2:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, global_assignstruct+16(%rip)
	imulq	$81608, %rax, %rdi      # imm = 0x13EC8
	movq	%r14, %rsi
	callq	AllocateMemory
	movq	%rax, %rbx
	movl	12(%rsp), %esi
	testl	%esi, %esi
	je	.LBB9_4
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	movl	$.L.str.57, %edi
	callq	ReportError
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	FreeMemory
	xorl	%eax, %eax
	callq	ErrorExit
.LBB9_4:                                #   in Loop: Header=BB9_2 Depth=1
	movq	global_assignstruct+16(%rip), %rsi
	movq	%rbx, %rdi
	callq	DoAssignIteration
	cmpq	global_min_ticks(%rip), %rax
	jbe	.LBB9_5
.LBB9_8:                                # %.loopexit
	movq	global_assignstruct+16(%rip), %rsi
	movq	%rbx, %rdi
	callq	DoAssignIteration
	movq	%rax, %r14
	movq	global_assignstruct+16(%rip), %rsi
	movq	%rbx, %rdi
	callq	DoAssignIteration
	movq	%rax, %r15
	addq	%r14, %r15
	movq	global_assignstruct+16(%rip), %rsi
	movq	%rbx, %rdi
	callq	DoAssignIteration
	movq	%rax, %r14
	addq	%r15, %r14
	movq	global_assignstruct+16(%rip), %rsi
	movq	%rbx, %rdi
	callq	DoAssignIteration
	movq	%rax, %r15
	addq	%r14, %r15
	movq	global_assignstruct+16(%rip), %rsi
	movq	%rbx, %rdi
	callq	DoAssignIteration
	movq	%rax, %r14
	addq	%r15, %r14
	movq	global_assignstruct+16(%rip), %rsi
	movq	%rbx, %rdi
	callq	DoAssignIteration
	movq	%rax, %r15
	addq	%r14, %r15
	movq	global_assignstruct+16(%rip), %rsi
	movq	%rbx, %rdi
	callq	DoAssignIteration
	movq	%rax, %r14
	addq	%r15, %r14
	movq	global_assignstruct+16(%rip), %rsi
	movq	%rbx, %rdi
	callq	DoAssignIteration
	leaq	(%rax,%r14), %r14
	leaq	12(%rsp), %rsi
	movq	%rbx, %rdi
	callq	FreeMemory
	movq	global_assignstruct+16(%rip), %xmm0 # xmm0 = mem[0],zero
	punpckldq	.LCPI9_0(%rip), %xmm0 # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	subpd	.LCPI9_1(%rip), %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm1
	mulsd	.LCPI9_2(%rip), %xmm1
	movapd	%xmm1, 16(%rsp)         # 16-byte Spill
	movq	%r14, %rdi
	callq	TicksToFracSecs
	movapd	16(%rsp), %xmm1         # 16-byte Reload
	divsd	%xmm0, %xmm1
	movsd	%xmm1, global_assignstruct+24(%rip)
	cmpl	$0, global_assignstruct(%rip)
	jne	.LBB9_10
# BB#9:
	movl	$1, global_assignstruct(%rip)
.LBB9_10:
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	DoAssign, .Lfunc_end9-DoAssign
	.cfi_endproc

	.p2align	4, 0x90
	.type	DoAssignIteration,@function
DoAssignIteration:                      # @DoAssignIteration
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi106:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi107:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi108:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi109:
	.cfi_def_cfa_offset 56
	subq	$20872, %rsp            # imm = 0x5188
.Lcfi110:
	.cfi_def_cfa_offset 20928
.Lcfi111:
	.cfi_offset %rbx, -56
.Lcfi112:
	.cfi_offset %r12, -48
.Lcfi113:
	.cfi_offset %r13, -40
.Lcfi114:
	.cfi_offset %r14, -32
.Lcfi115:
	.cfi_offset %r15, -24
.Lcfi116:
	.cfi_offset %rbp, -16
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r12
	movl	$13, %edi
	callq	randnum
	xorl	%r14d, %r14d
	movq	%r12, %r15
	.p2align	4, 0x90
.LBB10_1:                               # %.preheader.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_2 Depth 2
	movq	%r15, %rbp
	movl	$101, %ebx
	.p2align	4, 0x90
.LBB10_2:                               #   Parent Loop BB10_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$5000000, %edi          # imm = 0x4C4B40
	callq	abs_randwc
	movl	%eax, %eax
	movq	%rax, (%rbp)
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB10_2
# BB#3:                                 #   in Loop: Header=BB10_1 Depth=1
	incq	%r14
	addq	$808, %r15              # imm = 0x328
	cmpq	$101, %r14
	jne	.LBB10_1
# BB#4:                                 # %LoadAssign.exit.i
	cmpq	$2, 8(%rsp)             # 8-byte Folded Reload
	jb	.LBB10_20
# BB#5:                                 # %.lr.ph.i.preheader
	leaq	81672(%r12), %r10
	leaq	64(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	81608(%r12), %rdx
	movl	$1, %eax
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB10_6:                               # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_7 Depth 2
                                        #       Child Loop BB10_11 Depth 3
                                        #       Child Loop BB10_14 Depth 3
                                        #       Child Loop BB10_17 Depth 3
	movq	%rax, 24(%rsp)          # 8-byte Spill
	imulq	$10201, %r14, %r15      # imm = 0x27D9
	leaq	10201(%r15), %r13
	addq	$10302, %r15            # imm = 0x283E
	movq	%r12, %rax
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	%r10, 40(%rsp)          # 8-byte Spill
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB10_7:                               # %.preheader.i11.i
                                        #   Parent Loop BB10_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_11 Depth 3
                                        #       Child Loop BB10_14 Depth 3
                                        #       Child Loop BB10_17 Depth 3
	imulq	$101, %r11, %rcx
	leaq	(%r13,%rcx), %rsi
	leaq	(%r12,%rsi,8), %rsi
	leaq	808(%r12,%rcx,8), %rdi
	cmpq	%rdi, %rsi
	jae	.LBB10_10
# BB#8:                                 # %.preheader.i11.i
                                        #   in Loop: Header=BB10_7 Depth=2
	leaq	(%r15,%rcx), %rsi
	leaq	(%r12,%rsi,8), %rsi
	leaq	(%r12,%rcx,8), %rcx
	cmpq	%rsi, %rcx
	jae	.LBB10_10
# BB#9:                                 #   in Loop: Header=BB10_7 Depth=2
	xorl	%ebx, %ebx
	jmp	.LBB10_13
	.p2align	4, 0x90
.LBB10_10:                              # %vector.body.preheader
                                        #   in Loop: Header=BB10_7 Depth=2
	movq	%r9, %rbx
	movq	%r10, %rdi
	movl	$100, %r8d
	.p2align	4, 0x90
.LBB10_11:                              # %vector.body
                                        #   Parent Loop BB10_6 Depth=1
                                        #     Parent Loop BB10_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-64(%rbx), %xmm0
	movups	%xmm0, -64(%rdi)
	movups	-48(%rbx), %xmm0
	movups	%xmm0, -48(%rdi)
	movups	-32(%rbx), %xmm0
	movups	%xmm0, -32(%rdi)
	movups	-16(%rbx), %xmm0
	movups	%xmm0, -16(%rdi)
	movdqu	(%rbx), %xmm0
	movdqu	%xmm0, (%rdi)
	addq	$80, %rdi
	addq	$80, %rbx
	addq	$-10, %r8
	jne	.LBB10_11
# BB#12:                                #   in Loop: Header=BB10_7 Depth=2
	movl	$100, %ebx
.LBB10_13:                              # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB10_7 Depth=2
	movl	$100, %r8d
	subq	%rbx, %r8
	leaq	(%rdx,%rbx,8), %rcx
	leaq	(%rax,%rbx,8), %rbp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB10_14:                              # %scalar.ph.prol
                                        #   Parent Loop BB10_6 Depth=1
                                        #     Parent Loop BB10_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbp,%rsi,8), %rdi
	movq	%rdi, (%rcx,%rsi,8)
	incq	%rsi
	cmpq	$1, %rsi
	jne	.LBB10_14
# BB#15:                                # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB10_7 Depth=2
	cmpq	$3, %r8
	jb	.LBB10_18
# BB#16:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB10_7 Depth=2
	addq	%rsi, %rbx
	addq	$3, %rbx
	.p2align	4, 0x90
.LBB10_17:                              # %scalar.ph
                                        #   Parent Loop BB10_6 Depth=1
                                        #     Parent Loop BB10_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rax,%rbx,8), %rcx
	movq	%rcx, -24(%rdx,%rbx,8)
	movq	-16(%rax,%rbx,8), %rcx
	movq	%rcx, -16(%rdx,%rbx,8)
	movq	-8(%rax,%rbx,8), %rcx
	movq	%rcx, -8(%rdx,%rbx,8)
	movq	(%rax,%rbx,8), %rcx
	movq	%rcx, (%rdx,%rbx,8)
	addq	$4, %rbx
	cmpq	$104, %rbx
	jne	.LBB10_17
.LBB10_18:                              # %.loopexit44
                                        #   in Loop: Header=BB10_7 Depth=2
	incq	%r11
	addq	$808, %r10              # imm = 0x328
	addq	$808, %r9               # imm = 0x328
	addq	$808, %rdx              # imm = 0x328
	addq	$808, %rax              # imm = 0x328
	cmpq	$101, %r11
	jne	.LBB10_7
# BB#19:                                # %CopyToAssign.exit.i
                                        #   in Loop: Header=BB10_6 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	incq	%rax
	incq	%r14
	movq	40(%rsp), %r10          # 8-byte Reload
	addq	$81608, %r10            # imm = 0x13EC8
	movq	32(%rsp), %rdx          # 8-byte Reload
	addq	$81608, %rdx            # imm = 0x13EC8
	cmpq	8(%rsp), %rax           # 8-byte Folded Reload
	jne	.LBB10_6
.LBB10_20:                              # %LoadAssignArrayWithRand.exit
	xorl	%eax, %eax
	callq	StartStopwatch
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB10_132
# BB#21:                                # %.lr.ph
	leaq	24(%r12), %rax
	leaq	64(%r12), %rcx
	leaq	8(%r12), %r15
	leaq	466(%rsp), %r14
	xorl	%edx, %edx
	movabsq	$9223372036854775807, %r9 # imm = 0x7FFFFFFFFFFFFFFF
	leaq	464(%rsp), %r10
	.p2align	4, 0x90
.LBB10_22:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_23 Depth 2
                                        #       Child Loop BB10_24 Depth 3
                                        #       Child Loop BB10_26 Depth 3
                                        #     Child Loop BB10_29 Depth 2
                                        #       Child Loop BB10_30 Depth 3
                                        #       Child Loop BB10_33 Depth 3
                                        #     Child Loop BB10_35 Depth 2
                                        #       Child Loop BB10_36 Depth 3
                                        #         Child Loop BB10_37 Depth 4
                                        #           Child Loop BB10_41 Depth 5
                                        #           Child Loop BB10_47 Depth 5
                                        #         Child Loop BB10_51 Depth 4
                                        #           Child Loop BB10_55 Depth 5
                                        #           Child Loop BB10_61 Depth 5
                                        #       Child Loop BB10_67 Depth 3
                                        #         Child Loop BB10_70 Depth 4
                                        #         Child Loop BB10_79 Depth 4
                                        #         Child Loop BB10_84 Depth 4
                                        #       Child Loop BB10_89 Depth 3
                                        #         Child Loop BB10_90 Depth 4
                                        #       Child Loop BB10_94 Depth 3
                                        #         Child Loop BB10_95 Depth 4
                                        #           Child Loop BB10_99 Depth 5
                                        #         Child Loop BB10_102 Depth 4
                                        #           Child Loop BB10_104 Depth 5
                                        #       Child Loop BB10_111 Depth 3
                                        #         Child Loop BB10_113 Depth 4
                                        #       Child Loop BB10_118 Depth 3
                                        #         Child Loop BB10_120 Depth 4
                                        #       Child Loop BB10_125 Depth 3
                                        #         Child Loop BB10_127 Depth 4
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rcx, %r8
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rax, %rbp
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB10_23:                              # %.preheader48.i.i
                                        #   Parent Loop BB10_22 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_24 Depth 3
                                        #       Child Loop BB10_26 Depth 3
	movl	$100, %edi
	movq	%rbp, %rcx
	movq	%r9, %rsi
	jmp	.LBB10_24
	.p2align	4, 0x90
.LBB10_133:                             #   in Loop: Header=BB10_24 Depth=3
	movq	-16(%rcx), %rbx
	movq	-8(%rcx), %rax
	cmpq	%rsi, %rbx
	cmovleq	%rbx, %rsi
	cmpq	%rsi, %rax
	cmovleq	%rax, %rsi
	movq	(%rcx), %rax
	cmpq	%rsi, %rax
	cmovleq	%rax, %rsi
	addq	$32, %rcx
	addq	$-4, %rdi
.LBB10_24:                              #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_23 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rcx), %rbx
	cmpq	%rsi, %rbx
	cmovleq	%rbx, %rsi
	testq	%rdi, %rdi
	jne	.LBB10_133
# BB#25:                                # %vector.ph33
                                        #   in Loop: Header=BB10_23 Depth=2
	movd	%rsi, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movq	%r8, %rcx
	movl	$100, %edi
	.p2align	4, 0x90
.LBB10_26:                              # %vector.body29
                                        #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_23 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	-64(%rcx), %xmm1
	psubq	%xmm0, %xmm1
	movdqu	%xmm1, -64(%rcx)
	movdqu	-48(%rcx), %xmm1
	psubq	%xmm0, %xmm1
	movdqu	%xmm1, -48(%rcx)
	movdqu	-32(%rcx), %xmm1
	psubq	%xmm0, %xmm1
	movdqu	%xmm1, -32(%rcx)
	movdqu	-16(%rcx), %xmm1
	psubq	%xmm0, %xmm1
	movdqu	%xmm1, -16(%rcx)
	movdqu	(%rcx), %xmm1
	psubq	%xmm0, %xmm1
	movdqu	%xmm1, (%rcx)
	addq	$80, %rcx
	addq	$-10, %rdi
	jne	.LBB10_26
# BB#27:                                # %.preheader47.i.i
                                        #   in Loop: Header=BB10_23 Depth=2
	imulq	$808, %rdx, %rcx        # imm = 0x328
	subq	%rsi, 800(%r12,%rcx)
	incq	%rdx
	addq	$808, %rbp              # imm = 0x328
	addq	$808, %r8               # imm = 0x328
	cmpq	$101, %rdx
	jne	.LBB10_23
# BB#28:                                # %.preheader45.i.i.preheader
                                        #   in Loop: Header=BB10_22 Depth=1
	movq	%r12, %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB10_29:                              # %.preheader45.i.i
                                        #   Parent Loop BB10_22 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_30 Depth 3
                                        #       Child Loop BB10_33 Depth 3
	movl	$100, %esi
	movq	%rax, %rdi
	movq	%r9, %rdx
	jmp	.LBB10_30
	.p2align	4, 0x90
.LBB10_134:                             #   in Loop: Header=BB10_30 Depth=3
	movq	808(%rdi), %rbp
	cmpq	%rdx, %rbp
	cmovleq	%rbp, %rdx
	movq	1616(%rdi), %rbp
	cmpq	%rdx, %rbp
	cmovleq	%rbp, %rdx
	movq	2424(%rdi), %rbp
	cmpq	%rdx, %rbp
	cmovleq	%rbp, %rdx
	addq	$3232, %rdi             # imm = 0xCA0
	addq	$-4, %rsi
.LBB10_30:                              #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_29 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rdi), %rbp
	cmpq	%rdx, %rbp
	cmovleq	%rbp, %rdx
	testq	%rsi, %rsi
	jne	.LBB10_134
# BB#31:                                #   in Loop: Header=BB10_29 Depth=2
	testq	%rdx, %rdx
	je	.LBB10_34
# BB#32:                                # %.preheader.i.i14.preheader
                                        #   in Loop: Header=BB10_29 Depth=2
	movl	$2424, %esi             # imm = 0x978
	jmp	.LBB10_33
	.p2align	4, 0x90
.LBB10_135:                             # %.preheader.i.i14.1
                                        #   in Loop: Header=BB10_33 Depth=3
	subq	%rdx, -1616(%rax,%rsi)
	subq	%rdx, -808(%rax,%rsi)
	subq	%rdx, (%rax,%rsi)
	addq	$3232, %rsi             # imm = 0xCA0
.LBB10_33:                              # %.preheader.i.i14
                                        #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_29 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	subq	%rdx, -2424(%rax,%rsi)
	cmpq	$83224, %rsi            # imm = 0x14518
	jne	.LBB10_135
.LBB10_34:                              # %.loopexit.i.i
                                        #   in Loop: Header=BB10_29 Depth=2
	incq	%rcx
	addq	$8, %rax
	cmpq	$101, %rcx
	jne	.LBB10_29
	jmp	.LBB10_35
	.p2align	4, 0x90
.LBB10_88:                              #   in Loop: Header=BB10_35 Depth=2
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 442(%rsp)
	movdqa	%xmm0, 432(%rsp)
	movdqa	%xmm0, 416(%rsp)
	movdqa	%xmm0, 400(%rsp)
	movdqa	%xmm0, 384(%rsp)
	movdqa	%xmm0, 368(%rsp)
	movdqa	%xmm0, 352(%rsp)
	movdqa	%xmm0, 336(%rsp)
	movdqa	%xmm0, 320(%rsp)
	movdqa	%xmm0, 304(%rsp)
	movdqa	%xmm0, 288(%rsp)
	movdqa	%xmm0, 272(%rsp)
	movdqa	%xmm0, 256(%rsp)
	movdqu	%xmm0, 234(%rsp)
	movdqa	%xmm0, 224(%rsp)
	movdqa	%xmm0, 208(%rsp)
	movdqa	%xmm0, 192(%rsp)
	movdqa	%xmm0, 176(%rsp)
	movdqa	%xmm0, 160(%rsp)
	movdqa	%xmm0, 144(%rsp)
	movdqa	%xmm0, 128(%rsp)
	movdqa	%xmm0, 112(%rsp)
	movdqa	%xmm0, 96(%rsp)
	movdqa	%xmm0, 80(%rsp)
	movdqa	%xmm0, 64(%rsp)
	movdqa	%xmm0, 48(%rsp)
	leaq	470(%rsp), %rax
	xorl	%ecx, %ecx
	movabsq	$9223372036854775807, %rbx # imm = 0x7FFFFFFFFFFFFFFF
	.p2align	4, 0x90
.LBB10_89:                              # %.preheader106.i.i
                                        #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB10_90 Depth 4
	movl	$1, %edx
	movq	%rax, %rsi
	jmp	.LBB10_90
	.p2align	4, 0x90
.LBB10_163:                             #   in Loop: Header=BB10_90 Depth=4
	addq	$8, %rsi
	addq	$4, %rdx
.LBB10_90:                              #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        #       Parent Loop BB10_89 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzwl	-6(%rsi), %edi
	cmpl	$1, %edi
	je	.LBB10_93
# BB#91:                                #   in Loop: Header=BB10_90 Depth=4
	cmpq	$101, %rdx
	jge	.LBB10_92
# BB#160:                               #   in Loop: Header=BB10_90 Depth=4
	movzwl	-4(%rsi), %edi
	cmpl	$1, %edi
	je	.LBB10_93
# BB#161:                               #   in Loop: Header=BB10_90 Depth=4
	movzwl	-2(%rsi), %edi
	cmpl	$1, %edi
	je	.LBB10_93
# BB#162:                               #   in Loop: Header=BB10_90 Depth=4
	movzwl	(%rsi), %edi
	cmpl	$1, %edi
	jne	.LBB10_163
	jmp	.LBB10_93
	.p2align	4, 0x90
.LBB10_92:                              # %.critedge.i.i
                                        #   in Loop: Header=BB10_89 Depth=3
	movw	$1, 256(%rsp,%rcx,2)
.LBB10_93:                              # %.loopexit107.i.i
                                        #   in Loop: Header=BB10_89 Depth=3
	incq	%rcx
	addq	$202, %rax
	cmpq	$101, %rcx
	jne	.LBB10_89
	.p2align	4, 0x90
.LBB10_94:                              # %.preheader105.i.i
                                        #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB10_95 Depth 4
                                        #           Child Loop BB10_99 Depth 5
                                        #         Child Loop BB10_102 Depth 4
                                        #           Child Loop BB10_104 Depth 5
	movq	%r15, %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB10_95:                              #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        #       Parent Loop BB10_94 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB10_99 Depth 5
	movzwl	256(%rsp,%rcx,2), %edx
	cmpl	$1, %edx
	jne	.LBB10_100
# BB#96:                                # %.preheader102.i.i.preheader
                                        #   in Loop: Header=BB10_95 Depth=4
	movq	%rax, %rdx
	movl	$1, %esi
	cmpq	$0, -8(%rdx)
	jne	.LBB10_99
	jmp	.LBB10_98
	.p2align	4, 0x90
.LBB10_166:                             #   in Loop: Header=BB10_99 Depth=5
	addq	$2, %rsi
	addq	$16, %rdx
	cmpq	$0, -8(%rdx)
	jne	.LBB10_99
.LBB10_98:                              #   in Loop: Header=BB10_95 Depth=4
	movw	$1, 46(%rsp,%rsi,2)
.LBB10_99:                              #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        #       Parent Loop BB10_94 Depth=3
                                        #         Parent Loop BB10_95 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cmpq	$101, %rsi
	je	.LBB10_100
# BB#164:                               # %.preheader102.i.i.1
                                        #   in Loop: Header=BB10_99 Depth=5
	cmpq	$0, (%rdx)
	jne	.LBB10_166
# BB#165:                               #   in Loop: Header=BB10_99 Depth=5
	movw	$1, 48(%rsp,%rsi,2)
	jmp	.LBB10_166
	.p2align	4, 0x90
.LBB10_100:                             # %.loopexit103.i.i
                                        #   in Loop: Header=BB10_95 Depth=4
	incq	%rcx
	addq	$808, %rax              # imm = 0x328
	cmpq	$101, %rcx
	jne	.LBB10_95
# BB#101:                               # %.preheader104.i.i.preheader
                                        #   in Loop: Header=BB10_94 Depth=3
	xorl	%eax, %eax
	movq	%r10, %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB10_102:                             # %.preheader104.i.i
                                        #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        #       Parent Loop BB10_94 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB10_104 Depth 5
	movzwl	48(%rsp,%rax,2), %esi
	cmpl	$1, %esi
	jne	.LBB10_108
# BB#103:                               # %.preheader100.i.i.preheader
                                        #   in Loop: Header=BB10_102 Depth=4
	movq	%rcx, %rsi
	movl	$1, %edi
	jmp	.LBB10_104
	.p2align	4, 0x90
.LBB10_170:                             #   in Loop: Header=BB10_104 Depth=5
	addq	$2, %rdi
	addq	$404, %rsi              # imm = 0x194
.LBB10_104:                             # %.preheader100.i.i
                                        #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        #       Parent Loop BB10_94 Depth=3
                                        #         Parent Loop BB10_102 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	(%rsi), %ebp
	cmpl	$1, %ebp
	jne	.LBB10_107
# BB#105:                               #   in Loop: Header=BB10_104 Depth=5
	movzwl	254(%rsp,%rdi,2), %ebp
	cmpl	$1, %ebp
	je	.LBB10_107
# BB#106:                               #   in Loop: Header=BB10_104 Depth=5
	movw	$1, 254(%rsp,%rdi,2)
	incl	%edx
.LBB10_107:                             #   in Loop: Header=BB10_104 Depth=5
	cmpq	$101, %rdi
	je	.LBB10_108
# BB#167:                               # %.preheader100.i.i.1
                                        #   in Loop: Header=BB10_104 Depth=5
	movzwl	202(%rsi), %ebp
	cmpl	$1, %ebp
	jne	.LBB10_170
# BB#168:                               #   in Loop: Header=BB10_104 Depth=5
	movzwl	256(%rsp,%rdi,2), %ebp
	cmpl	$1, %ebp
	je	.LBB10_170
# BB#169:                               #   in Loop: Header=BB10_104 Depth=5
	movw	$1, 256(%rsp,%rdi,2)
	incl	%edx
	jmp	.LBB10_170
	.p2align	4, 0x90
.LBB10_108:                             # %.loopexit101.i.i
                                        #   in Loop: Header=BB10_102 Depth=4
	incq	%rax
	addq	$2, %rcx
	cmpq	$101, %rax
	jne	.LBB10_102
# BB#109:                               #   in Loop: Header=BB10_94 Depth=3
	testw	%dx, %dx
	jne	.LBB10_94
# BB#110:                               # %.preheader99.i.i.preheader
                                        #   in Loop: Header=BB10_35 Depth=2
	movq	%r15, %rcx
	xorl	%edx, %edx
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB10_111:                             # %.preheader99.i.i
                                        #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB10_113 Depth 4
	cmpw	$0, 256(%rsp,%rdx,2)
	je	.LBB10_116
# BB#112:                               # %.preheader97.i.i.preheader
                                        #   in Loop: Header=BB10_111 Depth=3
	movq	%rcx, %rsi
	movl	$1, %edi
	jmp	.LBB10_113
	.p2align	4, 0x90
.LBB10_173:                             #   in Loop: Header=BB10_113 Depth=4
	addq	$2, %rdi
	addq	$16, %rsi
.LBB10_113:                             # %.preheader97.i.i
                                        #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        #       Parent Loop BB10_111 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzwl	46(%rsp,%rdi,2), %ebp
	cmpl	$1, %ebp
	je	.LBB10_115
# BB#114:                               #   in Loop: Header=BB10_113 Depth=4
	movq	-8(%rsi), %rbp
	cmpq	%rax, %rbp
	cmovleq	%rbp, %rax
.LBB10_115:                             #   in Loop: Header=BB10_113 Depth=4
	cmpq	$101, %rdi
	je	.LBB10_116
# BB#171:                               # %.preheader97.i.i.1
                                        #   in Loop: Header=BB10_113 Depth=4
	movzwl	48(%rsp,%rdi,2), %ebp
	cmpl	$1, %ebp
	je	.LBB10_173
# BB#172:                               #   in Loop: Header=BB10_113 Depth=4
	movq	(%rsi), %rbp
	cmpq	%rax, %rbp
	cmovleq	%rbp, %rax
	jmp	.LBB10_173
	.p2align	4, 0x90
.LBB10_116:                             # %.loopexit98.i.i
                                        #   in Loop: Header=BB10_111 Depth=3
	incq	%rdx
	addq	$808, %rcx              # imm = 0x328
	cmpq	$101, %rdx
	jne	.LBB10_111
# BB#117:                               # %.preheader96.i.i.preheader
                                        #   in Loop: Header=BB10_35 Depth=2
	movq	%r15, %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB10_118:                             # %.preheader96.i.i
                                        #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB10_120 Depth 4
	cmpw	$0, 256(%rsp,%rdx,2)
	je	.LBB10_123
# BB#119:                               # %.preheader94.i.i.preheader
                                        #   in Loop: Header=BB10_118 Depth=3
	movq	%rcx, %rsi
	movl	$1, %edi
	jmp	.LBB10_120
	.p2align	4, 0x90
.LBB10_176:                             #   in Loop: Header=BB10_120 Depth=4
	addq	$2, %rdi
	addq	$16, %rsi
.LBB10_120:                             # %.preheader94.i.i
                                        #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        #       Parent Loop BB10_118 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzwl	46(%rsp,%rdi,2), %ebp
	cmpl	$1, %ebp
	je	.LBB10_122
# BB#121:                               #   in Loop: Header=BB10_120 Depth=4
	subq	%rax, -8(%rsi)
.LBB10_122:                             #   in Loop: Header=BB10_120 Depth=4
	cmpq	$101, %rdi
	je	.LBB10_123
# BB#174:                               # %.preheader94.i.i.1
                                        #   in Loop: Header=BB10_120 Depth=4
	movzwl	48(%rsp,%rdi,2), %ebp
	cmpl	$1, %ebp
	je	.LBB10_176
# BB#175:                               #   in Loop: Header=BB10_120 Depth=4
	subq	%rax, (%rsi)
	jmp	.LBB10_176
	.p2align	4, 0x90
.LBB10_123:                             # %.loopexit95.i.i
                                        #   in Loop: Header=BB10_118 Depth=3
	incq	%rdx
	addq	$808, %rcx              # imm = 0x328
	cmpq	$101, %rdx
	jne	.LBB10_118
# BB#124:                               # %.preheader93.i.i.preheader
                                        #   in Loop: Header=BB10_35 Depth=2
	movq	%r15, %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB10_125:                             # %.preheader93.i.i
                                        #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB10_127 Depth 4
	cmpw	$0, 256(%rsp,%rdx,2)
	jne	.LBB10_130
# BB#126:                               # %.preheader.i4.i.preheader
                                        #   in Loop: Header=BB10_125 Depth=3
	movq	%rcx, %rsi
	movl	$1, %edi
	jmp	.LBB10_127
	.p2align	4, 0x90
.LBB10_179:                             #   in Loop: Header=BB10_127 Depth=4
	addq	$2, %rdi
	addq	$16, %rsi
.LBB10_127:                             # %.preheader.i4.i
                                        #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        #       Parent Loop BB10_125 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzwl	46(%rsp,%rdi,2), %ebp
	cmpl	$1, %ebp
	jne	.LBB10_129
# BB#128:                               #   in Loop: Header=BB10_127 Depth=4
	addq	%rax, -8(%rsi)
.LBB10_129:                             #   in Loop: Header=BB10_127 Depth=4
	cmpq	$101, %rdi
	je	.LBB10_130
# BB#177:                               # %.preheader.i4.i.1
                                        #   in Loop: Header=BB10_127 Depth=4
	movzwl	48(%rsp,%rdi,2), %ebp
	cmpl	$1, %ebp
	jne	.LBB10_179
# BB#178:                               #   in Loop: Header=BB10_127 Depth=4
	addq	%rax, (%rsi)
	jmp	.LBB10_179
	.p2align	4, 0x90
.LBB10_130:                             # %.loopexit.i7.i
                                        #   in Loop: Header=BB10_125 Depth=3
	incq	%rdx
	addq	$808, %rcx              # imm = 0x328
	cmpq	$101, %rdx
	jne	.LBB10_125
.LBB10_35:                              # %calc_minimum_costs.exit.i
                                        #   Parent Loop BB10_22 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_36 Depth 3
                                        #         Child Loop BB10_37 Depth 4
                                        #           Child Loop BB10_41 Depth 5
                                        #           Child Loop BB10_47 Depth 5
                                        #         Child Loop BB10_51 Depth 4
                                        #           Child Loop BB10_55 Depth 5
                                        #           Child Loop BB10_61 Depth 5
                                        #       Child Loop BB10_67 Depth 3
                                        #         Child Loop BB10_70 Depth 4
                                        #         Child Loop BB10_79 Depth 4
                                        #         Child Loop BB10_84 Depth 4
                                        #       Child Loop BB10_89 Depth 3
                                        #         Child Loop BB10_90 Depth 4
                                        #       Child Loop BB10_94 Depth 3
                                        #         Child Loop BB10_95 Depth 4
                                        #           Child Loop BB10_99 Depth 5
                                        #         Child Loop BB10_102 Depth 4
                                        #           Child Loop BB10_104 Depth 5
                                        #       Child Loop BB10_111 Depth 3
                                        #         Child Loop BB10_113 Depth 4
                                        #       Child Loop BB10_118 Depth 3
                                        #         Child Loop BB10_120 Depth 4
                                        #       Child Loop BB10_125 Depth 3
                                        #         Child Loop BB10_127 Depth 4
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	movl	$20402, %edx            # imm = 0x4FB2
	movq	%r10, %rdi
	movq	%r10, %rbx
	callq	memset
	movq	%rbx, %r10
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB10_36:                              # %.preheader152.i.i
                                        #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB10_37 Depth 4
                                        #           Child Loop BB10_41 Depth 5
                                        #           Child Loop BB10_47 Depth 5
                                        #         Child Loop BB10_51 Depth 4
                                        #           Child Loop BB10_55 Depth 5
                                        #           Child Loop BB10_61 Depth 5
	xorl	%edx, %edx
	movq	%r14, %r9
	movq	%r15, %rbx
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB10_37:                              # %.preheader150.i.i
                                        #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        #       Parent Loop BB10_36 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB10_41 Depth 5
                                        #           Child Loop BB10_47 Depth 5
	xorl	%ebp, %ebp
	movq	%r9, %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	cmpq	$0, -8(%rdi)
	jne	.LBB10_41
	jmp	.LBB10_39
	.p2align	4, 0x90
.LBB10_139:                             #   in Loop: Header=BB10_41 Depth=5
	addq	$2, %rbp
	addq	$16, %rdi
	addq	$4, %rsi
	cmpq	$0, -8(%rdi)
	jne	.LBB10_41
.LBB10_39:                              #   in Loop: Header=BB10_37 Depth=4
	cmpw	$0, -2(%rsi)
	jne	.LBB10_41
# BB#40:                                #   in Loop: Header=BB10_37 Depth=4
	incl	%eax
	movl	%ebp, %ecx
	.p2align	4, 0x90
.LBB10_41:                              #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        #       Parent Loop BB10_36 Depth=3
                                        #         Parent Loop BB10_37 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cmpq	$100, %rbp
	je	.LBB10_42
# BB#136:                               #   in Loop: Header=BB10_41 Depth=5
	cmpq	$0, (%rdi)
	jne	.LBB10_139
# BB#137:                               #   in Loop: Header=BB10_41 Depth=5
	cmpw	$0, (%rsi)
	jne	.LBB10_139
# BB#138:                               #   in Loop: Header=BB10_41 Depth=5
	incl	%eax
	leal	1(%rbp), %ecx
	jmp	.LBB10_139
	.p2align	4, 0x90
.LBB10_42:                              #   in Loop: Header=BB10_37 Depth=4
	movzwl	%ax, %eax
	cmpl	$1, %eax
	jne	.LBB10_49
# BB#43:                                #   in Loop: Header=BB10_37 Depth=4
	movslq	%ecx, %rax
	imulq	$202, %rdx, %rsi
	leaq	464(%rsp,%rsi), %rsi
	movw	$1, (%rsi,%rax,2)
	addq	%rax, %rax
	xorl	%esi, %esi
	cmpq	%rsi, %rdx
	je	.LBB10_47
	jmp	.LBB10_45
	.p2align	4, 0x90
.LBB10_143:                             #   in Loop: Header=BB10_47 Depth=5
	addq	$404, %rax              # imm = 0x194
	incq	%rsi
	cmpq	%rsi, %rdx
	je	.LBB10_47
.LBB10_45:                              #   in Loop: Header=BB10_37 Depth=4
	cmpq	$0, (%r12,%rax,4)
	jne	.LBB10_47
# BB#46:                                #   in Loop: Header=BB10_37 Depth=4
	movw	$2, 464(%rsp,%rax)
	.p2align	4, 0x90
.LBB10_47:                              #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        #       Parent Loop BB10_36 Depth=3
                                        #         Parent Loop BB10_37 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	incq	%rsi
	cmpq	$101, %rsi
	je	.LBB10_48
# BB#140:                               #   in Loop: Header=BB10_47 Depth=5
	cmpq	%rdx, %rsi
	je	.LBB10_143
# BB#141:                               #   in Loop: Header=BB10_47 Depth=5
	cmpq	$0, 808(%r12,%rax,4)
	jne	.LBB10_143
# BB#142:                               #   in Loop: Header=BB10_47 Depth=5
	movw	$2, 666(%rsp,%rax)
	jmp	.LBB10_143
	.p2align	4, 0x90
.LBB10_48:                              # %.loopexit149.loopexit.i.i
                                        #   in Loop: Header=BB10_37 Depth=4
	incl	%r8d
	incl	%r13d
.LBB10_49:                              # %.loopexit149.i.i
                                        #   in Loop: Header=BB10_37 Depth=4
	incq	%rdx
	addq	$808, %rbx              # imm = 0x328
	addq	$202, %r9
	cmpq	$101, %rdx
	jne	.LBB10_37
# BB#50:                                # %.preheader148.i.i.preheader
                                        #   in Loop: Header=BB10_36 Depth=3
	movq	%r10, %r9
	movq	%r12, %rbx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB10_51:                              # %.preheader148.i.i
                                        #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        #       Parent Loop BB10_36 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB10_55 Depth 5
                                        #           Child Loop BB10_61 Depth 5
	xorl	%ebp, %ebp
	movq	%r9, %rax
	movq	%rbx, %rsi
	xorl	%edx, %edx
	cmpq	$0, (%rsi)
	jne	.LBB10_55
	jmp	.LBB10_53
	.p2align	4, 0x90
.LBB10_147:                             #   in Loop: Header=BB10_55 Depth=5
	addq	$2, %rbp
	addq	$1616, %rsi             # imm = 0x650
	addq	$404, %rax              # imm = 0x194
	cmpq	$0, (%rsi)
	jne	.LBB10_55
.LBB10_53:                              #   in Loop: Header=BB10_51 Depth=4
	cmpw	$0, (%rax)
	jne	.LBB10_55
# BB#54:                                #   in Loop: Header=BB10_51 Depth=4
	incl	%edx
	movl	%ebp, %ecx
	.p2align	4, 0x90
.LBB10_55:                              #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        #       Parent Loop BB10_36 Depth=3
                                        #         Parent Loop BB10_51 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cmpq	$100, %rbp
	je	.LBB10_56
# BB#144:                               #   in Loop: Header=BB10_55 Depth=5
	cmpq	$0, 808(%rsi)
	jne	.LBB10_147
# BB#145:                               #   in Loop: Header=BB10_55 Depth=5
	cmpw	$0, 202(%rax)
	jne	.LBB10_147
# BB#146:                               #   in Loop: Header=BB10_55 Depth=5
	incl	%edx
	leal	1(%rbp), %ecx
	jmp	.LBB10_147
	.p2align	4, 0x90
.LBB10_56:                              #   in Loop: Header=BB10_51 Depth=4
	movzwl	%dx, %eax
	cmpl	$1, %eax
	jne	.LBB10_63
# BB#57:                                #   in Loop: Header=BB10_51 Depth=4
	movslq	%ecx, %rax
	imulq	$202, %rax, %rdx
	leaq	464(%rsp,%rdx), %rdx
	movw	$1, (%rdx,%rdi,2)
	imulq	$101, %rax, %rdx
	leaq	(%r12,%rdx,8), %rax
	leaq	(%r14,%rdx,2), %rdx
	xorl	%ebp, %ebp
	cmpq	%rbp, %rdi
	je	.LBB10_61
	jmp	.LBB10_59
	.p2align	4, 0x90
.LBB10_151:                             #   in Loop: Header=BB10_61 Depth=5
	incq	%rsi
	movq	%rsi, %rbp
	cmpq	%rbp, %rdi
	je	.LBB10_61
.LBB10_59:                              #   in Loop: Header=BB10_51 Depth=4
	cmpq	$0, (%rax,%rbp,8)
	jne	.LBB10_61
# BB#60:                                #   in Loop: Header=BB10_51 Depth=4
	movw	$2, -2(%rdx,%rbp,2)
	.p2align	4, 0x90
.LBB10_61:                              #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        #       Parent Loop BB10_36 Depth=3
                                        #         Parent Loop BB10_51 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	leaq	1(%rbp), %rsi
	cmpq	$101, %rsi
	je	.LBB10_62
# BB#148:                               #   in Loop: Header=BB10_61 Depth=5
	cmpq	%rdi, %rsi
	je	.LBB10_151
# BB#149:                               #   in Loop: Header=BB10_61 Depth=5
	cmpq	$0, 8(%rax,%rbp,8)
	jne	.LBB10_151
# BB#150:                               #   in Loop: Header=BB10_61 Depth=5
	movw	$2, (%rdx,%rbp,2)
	jmp	.LBB10_151
	.p2align	4, 0x90
.LBB10_62:                              # %.loopexit147.loopexit.i.i
                                        #   in Loop: Header=BB10_51 Depth=4
	incl	%r8d
	incl	%r13d
.LBB10_63:                              # %.loopexit147.i.i
                                        #   in Loop: Header=BB10_51 Depth=4
	incq	%rdi
	addq	$8, %rbx
	addq	$2, %r9
	cmpq	$101, %rdi
	jne	.LBB10_51
# BB#64:                                #   in Loop: Header=BB10_36 Depth=3
	testw	%r8w, %r8w
	jne	.LBB10_36
# BB#65:                                #   in Loop: Header=BB10_35 Depth=2
	movzwl	%r13w, %eax
	cmpl	$101, %eax
	je	.LBB10_131
# BB#66:                                # %.preheader144.i.i.preheader
                                        #   in Loop: Header=BB10_35 Depth=2
	movq	%r14, %rax
	movq	%r15, %rbp
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB10_67:                              # %.preheader144.i.i
                                        #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB10_70 Depth 4
                                        #         Child Loop BB10_79 Depth 4
                                        #         Child Loop BB10_84 Depth 4
	movq	%rax, %rdi
	movq	%rbp, %rcx
	movl	$1, %esi
	cmpq	$0, -8(%rcx)
	jne	.LBB10_70
	jmp	.LBB10_69
	.p2align	4, 0x90
.LBB10_73:                              #   in Loop: Header=BB10_70 Depth=4
	addq	$2, %rsi
	addq	$16, %rcx
	addq	$4, %rdi
	cmpq	$0, -8(%rcx)
	jne	.LBB10_70
.LBB10_69:                              #   in Loop: Header=BB10_67 Depth=3
	cmpw	$0, -2(%rdi)
	je	.LBB10_74
.LBB10_70:                              #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        #       Parent Loop BB10_67 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	$101, %rsi
	jae	.LBB10_86
# BB#71:                                #   in Loop: Header=BB10_70 Depth=4
	cmpq	$0, (%rcx)
	jne	.LBB10_73
# BB#72:                                #   in Loop: Header=BB10_70 Depth=4
	cmpw	$0, (%rdi)
	jne	.LBB10_73
	jmp	.LBB10_75
	.p2align	4, 0x90
.LBB10_74:                              # %split
                                        #   in Loop: Header=BB10_67 Depth=3
	decq	%rsi
.LBB10_75:                              #   in Loop: Header=BB10_67 Depth=3
	movzwl	%si, %esi
	imulq	$202, %rdx, %rcx
	leaq	464(%rsp,%rcx), %rcx
	movw	$1, (%rcx,%rsi,2)
	xorl	%ecx, %ecx
	cmpq	%rcx, %rsi
	je	.LBB10_79
	jmp	.LBB10_77
	.p2align	4, 0x90
.LBB10_155:                             #   in Loop: Header=BB10_79 Depth=4
	incq	%rdi
	movq	%rdi, %rcx
	cmpq	%rcx, %rsi
	je	.LBB10_79
.LBB10_77:                              #   in Loop: Header=BB10_67 Depth=3
	cmpq	$0, -8(%rbp,%rcx,8)
	jne	.LBB10_79
# BB#78:                                #   in Loop: Header=BB10_67 Depth=3
	movw	$2, -2(%rax,%rcx,2)
	.p2align	4, 0x90
.LBB10_79:                              #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        #       Parent Loop BB10_67 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leaq	1(%rcx), %rdi
	cmpq	$101, %rdi
	je	.LBB10_80
# BB#152:                               #   in Loop: Header=BB10_79 Depth=4
	cmpq	%rsi, %rdi
	je	.LBB10_155
# BB#153:                               #   in Loop: Header=BB10_79 Depth=4
	cmpq	$0, (%rbp,%rcx,8)
	jne	.LBB10_155
# BB#154:                               #   in Loop: Header=BB10_79 Depth=4
	movw	$2, (%rax,%rcx,2)
	jmp	.LBB10_155
	.p2align	4, 0x90
.LBB10_80:                              # %.preheader.i12.i.preheader
                                        #   in Loop: Header=BB10_67 Depth=3
	addq	%rsi, %rsi
	xorl	%edi, %edi
	cmpq	%rdi, %rdx
	je	.LBB10_84
	jmp	.LBB10_82
	.p2align	4, 0x90
.LBB10_159:                             #   in Loop: Header=BB10_84 Depth=4
	addq	$404, %rsi              # imm = 0x194
	incq	%rdi
	cmpq	%rdi, %rdx
	je	.LBB10_84
.LBB10_82:                              #   in Loop: Header=BB10_67 Depth=3
	cmpq	$0, (%r12,%rsi,4)
	jne	.LBB10_84
# BB#83:                                #   in Loop: Header=BB10_67 Depth=3
	movw	$2, 464(%rsp,%rsi)
	.p2align	4, 0x90
.LBB10_84:                              #   Parent Loop BB10_22 Depth=1
                                        #     Parent Loop BB10_35 Depth=2
                                        #       Parent Loop BB10_67 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incq	%rdi
	cmpq	$101, %rdi
	je	.LBB10_85
# BB#156:                               # %.preheader.i12.i.1
                                        #   in Loop: Header=BB10_84 Depth=4
	cmpq	%rdx, %rdi
	je	.LBB10_159
# BB#157:                               #   in Loop: Header=BB10_84 Depth=4
	cmpq	$0, 808(%r12,%rsi,4)
	jne	.LBB10_159
# BB#158:                               #   in Loop: Header=BB10_84 Depth=4
	movw	$2, 666(%rsp,%rsi)
	jmp	.LBB10_159
	.p2align	4, 0x90
.LBB10_85:                              # %.thread.loopexit.i.i
                                        #   in Loop: Header=BB10_67 Depth=3
	incl	%r13d
.LBB10_86:                              # %.thread.i.i
                                        #   in Loop: Header=BB10_67 Depth=3
	incq	%rdx
	addq	$808, %rbp              # imm = 0x328
	addq	$202, %rax
	cmpq	$101, %rdx
	jne	.LBB10_67
# BB#87:                                # %first_assignments.exit.i
                                        #   in Loop: Header=BB10_35 Depth=2
	movzwl	%r13w, %eax
	cmpl	$101, %eax
	jne	.LBB10_88
.LBB10_131:                             # %Assignment.exit
                                        #   in Loop: Header=BB10_22 Depth=1
	addq	$81608, %r12            # imm = 0x13EC8
	movq	24(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	movq	40(%rsp), %rax          # 8-byte Reload
	addq	$81608, %rax            # imm = 0x13EC8
	movq	32(%rsp), %rcx          # 8-byte Reload
	addq	$81608, %rcx            # imm = 0x13EC8
	addq	$81608, %r15            # imm = 0x13EC8
	cmpq	8(%rsp), %rdx           # 8-byte Folded Reload
	movabsq	$9223372036854775807, %r9 # imm = 0x7FFFFFFFFFFFFFFF
	jne	.LBB10_22
.LBB10_132:                             # %._crit_edge
	movq	16(%rsp), %rdi          # 8-byte Reload
	addq	$20872, %rsp            # imm = 0x5188
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	StopStopwatch           # TAILCALL
.Lfunc_end10:
	.size	DoAssignIteration, .Lfunc_end10-DoAssignIteration
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI11_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI11_2:
	.quad	4655961152654671872     # double 1875
	.text
	.globl	DoIDEA
	.p2align	4, 0x90
	.type	DoIDEA,@function
DoIDEA:                                 # @DoIDEA
	.cfi_startproc
# BB#0:                                 # %.preheader78.preheader125
	pushq	%rbp
.Lcfi117:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi118:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi119:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi120:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi121:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi122:
	.cfi_def_cfa_offset 56
	subq	$376, %rsp              # imm = 0x178
.Lcfi123:
	.cfi_def_cfa_offset 432
.Lcfi124:
	.cfi_offset %rbx, -56
.Lcfi125:
	.cfi_offset %r12, -48
.Lcfi126:
	.cfi_offset %r13, -40
.Lcfi127:
	.cfi_offset %r14, -32
.Lcfi128:
	.cfi_offset %r15, -24
.Lcfi129:
	.cfi_offset %rbp, -16
	movl	$3, %edi
	callq	randnum
	movl	$60000, %edi            # imm = 0xEA60
	callq	abs_randwc
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	$60000, %edi            # imm = 0xEA60
	callq	abs_randwc
	movl	%eax, %r15d
	movl	$60000, %edi            # imm = 0xEA60
	callq	abs_randwc
	movl	%eax, %r12d
	movl	$60000, %edi            # imm = 0xEA60
	callq	abs_randwc
	movl	%eax, %r13d
	movl	$60000, %edi            # imm = 0xEA60
	callq	abs_randwc
	movl	%eax, %ebx
	movl	$60000, %edi            # imm = 0xEA60
	callq	abs_randwc
	movl	%eax, %ebp
	movl	$60000, %edi            # imm = 0xEA60
	callq	abs_randwc
	movl	%eax, %r14d
	movl	$60000, %edi            # imm = 0xEA60
	callq	abs_randwc
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 240(%rsp)
	movaps	%xmm0, 224(%rsp)
	movaps	%xmm0, 208(%rsp)
	movaps	%xmm0, 192(%rsp)
	movaps	%xmm0, 176(%rsp)
	movq	$0, 256(%rsp)
	leaq	160(%rsp), %rcx
	movl	16(%rsp), %edx          # 4-byte Reload
	movw	%dx, 160(%rsp)
	movw	%r15w, 162(%rsp)
	movw	%r12w, 164(%rsp)
	movw	%r13w, 166(%rsp)
	leaq	168(%rsp), %r8
	movw	%bx, 168(%rsp)
	movw	%bp, 170(%rsp)
	movw	%r14w, 172(%rsp)
	movw	%ax, 174(%rsp)
	xorl	%edi, %edi
	movl	$44, %r10d
	.p2align	4, 0x90
.LBB11_1:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rdi), %ebp
	movl	%ebp, %esi
	andl	$7, %esi
	movzwl	(%rcx,%rsi,2), %r9d
	leal	2(%rdi), %eax
	andl	$7, %eax
	movzwl	(%rcx,%rax,2), %ebx
	shrdw	$7, %r9w, %bx
	movl	%edi, %edx
	orl	$8, %edx
	movw	%bx, (%rcx,%rdx,2)
	movl	%ebp, %edx
	andl	$8, %edx
	leaq	(%rcx,%rdx,2), %rcx
	incl	%esi
	movzwl	(%rcx,%rax,2), %edx
	addl	$3, %edi
	andl	$7, %edi
	movzwl	(%rcx,%rdi,2), %edi
	shrdw	$7, %dx, %di
	orl	$8, %ebp
	movw	%di, (%rcx,%rbp,2)
	andl	$8, %esi
	leaq	(%rcx,%rsi,2), %rcx
	addl	$-2, %r10d
	movl	%eax, %edi
	jne	.LBB11_1
# BB#2:                                 # %en_key_idea.exit
	movzwl	160(%rsp), %r11d
	cmpl	$2, %r11d
	jb	.LBB11_9
# BB#3:
	movl	$65537, %eax            # imm = 0x10001
	xorl	%edx, %edx
	divl	%r11d
	movl	%eax, %esi
	cmpl	$1, %edx
	jne	.LBB11_4
# BB#69:
	movl	$1, %r11d
	subl	%esi, %r11d
	jmp	.LBB11_9
.LBB11_4:                               # %.preheader.i.i.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB11_5:                               # %.preheader.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%esi, %r9d
	movzwl	%r11w, %eax
	movzwl	%dx, %ebp
	xorl	%edx, %edx
	divl	%ebp
	movl	%eax, %edi
	movzwl	%r9w, %esi
	imull	%esi, %edi
	movzwl	%bx, %ecx
	addl	%edi, %ecx
	movl	%edx, %r11d
	cmpl	$1, %r11d
	je	.LBB11_6
# BB#7:                                 #   in Loop: Header=BB11_5 Depth=1
	xorl	%edx, %edx
	movl	%ebp, %eax
	divl	%r11d
	addl	%ebx, %edi
	movzwl	%di, %edi
	imull	%eax, %edi
	addl	%edi, %esi
	cmpl	$1, %edx
	movl	%ecx, %ebx
	jne	.LBB11_5
# BB#8:
	movl	$1, %r11d
	subl	%r9d, %r11d
	subl	%edi, %r11d
	jmp	.LBB11_9
.LBB11_6:
	movw	%cx, %r11w
.LBB11_9:                               # %inv.exit.i
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	subw	162(%rsp), %r10w
	subw	164(%rsp), %r9w
	movzwl	166(%rsp), %esi
	cmpl	$2, %esi
	jb	.LBB11_16
# BB#10:
	movl	$65537, %eax            # imm = 0x10001
	xorl	%edx, %edx
	divl	%esi
	movl	%eax, %edi
	cmpl	$1, %edx
	jne	.LBB11_11
# BB#70:
	movl	$1, %esi
	subl	%edi, %esi
	jmp	.LBB11_16
.LBB11_11:                              # %.preheader.i74.i.preheader
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB11_12:                              # %.preheader.i74.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%edi, %r14d
	movzwl	%si, %eax
	movzwl	%dx, %ebx
	xorl	%edx, %edx
	divl	%ebx
	movl	%eax, %ebp
	movzwl	%r14w, %edi
	imull	%edi, %ebp
	movzwl	%r15w, %ecx
	addl	%ebp, %ecx
	movl	%edx, %esi
	cmpl	$1, %esi
	je	.LBB11_13
# BB#14:                                #   in Loop: Header=BB11_12 Depth=1
	xorl	%edx, %edx
	movl	%ebx, %eax
	divl	%esi
	addl	%r15d, %ebp
	movzwl	%bp, %ebx
	imull	%eax, %ebx
	addl	%ebx, %edi
	cmpl	$1, %edx
	movl	%ecx, %r15d
	jne	.LBB11_12
# BB#15:
	movl	$1, %esi
	subl	%r14d, %esi
	subl	%ebx, %esi
	jmp	.LBB11_16
.LBB11_13:
	movw	%cx, %si
.LBB11_16:                              # %inv.exit78.i
	movw	%si, 134(%rsp)
	movw	%r9w, 132(%rsp)
	movw	%r10w, 130(%rsp)
	leaq	128(%rsp), %r9
	movw	%r11w, 128(%rsp)
	movl	$1, %r10d
	jmp	.LBB11_17
.LBB11_30:                              #   in Loop: Header=BB11_17 Depth=1
	movl	$1, %esi
	subl	%r12d, %esi
	subl	%ebx, %esi
	jmp	.LBB11_31
.LBB11_28:                              #   in Loop: Header=BB11_17 Depth=1
	movw	%cx, %si
	.p2align	4, 0x90
.LBB11_31:                              # %inv.exit60.i
                                        #   in Loop: Header=BB11_17 Depth=1
	movw	%si, -6(%r9)
	cmpl	$7, %r10d
	jg	.LBB11_33
# BB#32:                                #   in Loop: Header=BB11_17 Depth=1
	addq	$12, %r8
	movw	%r11w, -8(%r9)
	movw	%r14w, -10(%r9)
	movw	%r15w, -12(%r9)
	addq	$-12, %r9
	incl	%r10d
.LBB11_17:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_20 Depth 2
                                        #     Child Loop BB11_27 Depth 2
	movl	(%r8), %eax
	movl	%eax, -4(%r9)
	movzwl	4(%r8), %r15d
	cmpl	$2, %r15d
	jb	.LBB11_24
# BB#18:                                #   in Loop: Header=BB11_17 Depth=1
	movl	$65537, %eax            # imm = 0x10001
	xorl	%edx, %edx
	divl	%r15d
	movl	%eax, %esi
	cmpl	$1, %edx
	jne	.LBB11_19
# BB#71:                                #   in Loop: Header=BB11_17 Depth=1
	movl	$1, %r15d
	subl	%esi, %r15d
	jmp	.LBB11_24
	.p2align	4, 0x90
.LBB11_19:                              # %.preheader.i65.i.preheader
                                        #   in Loop: Header=BB11_17 Depth=1
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB11_20:                              # %.preheader.i65.i
                                        #   Parent Loop BB11_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %r11d
	movzwl	%r15w, %eax
	movzwl	%dx, %ebp
	xorl	%edx, %edx
	divl	%ebp
	movl	%eax, %edi
	movzwl	%r11w, %esi
	imull	%esi, %edi
	movzwl	%bx, %ecx
	addl	%edi, %ecx
	movl	%edx, %r15d
	cmpl	$1, %r15d
	je	.LBB11_21
# BB#22:                                #   in Loop: Header=BB11_20 Depth=2
	xorl	%edx, %edx
	movl	%ebp, %eax
	divl	%r15d
	addl	%ebx, %edi
	movzwl	%di, %edi
	imull	%eax, %edi
	addl	%edi, %esi
	cmpl	$1, %edx
	movl	%ecx, %ebx
	jne	.LBB11_20
# BB#23:                                #   in Loop: Header=BB11_17 Depth=1
	movl	$1, %r15d
	subl	%r11d, %r15d
	subl	%edi, %r15d
	jmp	.LBB11_24
.LBB11_21:                              #   in Loop: Header=BB11_17 Depth=1
	movw	%cx, %r15w
	.p2align	4, 0x90
.LBB11_24:                              # %inv.exit69.i
                                        #   in Loop: Header=BB11_17 Depth=1
	xorl	%r11d, %r11d
	subw	6(%r8), %r11w
	xorl	%r14d, %r14d
	subw	8(%r8), %r14w
	movzwl	10(%r8), %esi
	cmpl	$2, %esi
	jb	.LBB11_31
# BB#25:                                #   in Loop: Header=BB11_17 Depth=1
	movl	$65537, %eax            # imm = 0x10001
	xorl	%edx, %edx
	divl	%esi
	movl	%eax, %edi
	cmpl	$1, %edx
	jne	.LBB11_26
# BB#72:                                #   in Loop: Header=BB11_17 Depth=1
	movl	$1, %esi
	subl	%edi, %esi
	jmp	.LBB11_31
	.p2align	4, 0x90
.LBB11_26:                              # %.preheader.i56.i.preheader
                                        #   in Loop: Header=BB11_17 Depth=1
	movl	$1, %r13d
	.p2align	4, 0x90
.LBB11_27:                              # %.preheader.i56.i
                                        #   Parent Loop BB11_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, %r12d
	movzwl	%si, %eax
	movzwl	%dx, %ebx
	xorl	%edx, %edx
	divl	%ebx
	movl	%eax, %ebp
	movzwl	%r12w, %edi
	imull	%edi, %ebp
	movzwl	%r13w, %ecx
	addl	%ebp, %ecx
	movl	%edx, %esi
	cmpl	$1, %esi
	je	.LBB11_28
# BB#29:                                #   in Loop: Header=BB11_27 Depth=2
	xorl	%edx, %edx
	movl	%ebx, %eax
	divl	%esi
	addl	%r13d, %ebp
	movzwl	%bp, %ebx
	imull	%eax, %ebx
	addl	%ebx, %edi
	cmpl	$1, %edx
	movl	%ecx, %r13d
	jne	.LBB11_27
	jmp	.LBB11_30
.LBB11_33:                              # %scalar.ph
	movw	%r14w, -8(%r9)
	movw	%r11w, -10(%r9)
	movw	%r15w, -12(%r9)
	movapd	32(%rsp), %xmm0
	movapd	%xmm0, 272(%rsp)
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 32(%rsp)
	movaps	48(%rsp), %xmm1
	movaps	%xmm1, 288(%rsp)
	movapd	%xmm0, 48(%rsp)
	movaps	64(%rsp), %xmm1
	movaps	%xmm1, 304(%rsp)
	movapd	%xmm0, 64(%rsp)
	movaps	80(%rsp), %xmm1
	movaps	%xmm1, 320(%rsp)
	movapd	%xmm0, 80(%rsp)
	movaps	96(%rsp), %xmm1
	movaps	%xmm1, 336(%rsp)
	movapd	%xmm0, 96(%rsp)
	movaps	112(%rsp), %xmm1
	movaps	%xmm1, 352(%rsp)
	movapd	%xmm0, 112(%rsp)
	movzwl	128(%rsp), %eax
	movw	%ax, 368(%rsp)
	movw	$0, 128(%rsp)
	movzwl	130(%rsp), %eax
	movw	%ax, 370(%rsp)
	movw	$0, 130(%rsp)
	movzwl	132(%rsp), %eax
	movw	%ax, 372(%rsp)
	movw	$0, 132(%rsp)
	movzwl	134(%rsp), %eax
	movw	%ax, 374(%rsp)
	movw	$0, 134(%rsp)
	movq	global_ideastruct+16(%rip), %rdi
	leaq	32(%rsp), %rsi
	callq	AllocateMemory
	movq	%rax, %r14
	movl	32(%rsp), %esi
	testl	%esi, %esi
	je	.LBB11_35
# BB#34:
	movl	$.L.str.58, %edi
	callq	ReportError
	xorl	%eax, %eax
	callq	ErrorExit
.LBB11_35:
	movq	global_ideastruct+16(%rip), %rdi
	leaq	32(%rsp), %rsi
	callq	AllocateMemory
	movq	%rax, %r15
	movl	32(%rsp), %esi
	testl	%esi, %esi
	je	.LBB11_37
# BB#36:
	movl	$.L.str.58, %edi
	callq	ReportError
	leaq	32(%rsp), %rsi
	movq	%r14, %rdi
	callq	FreeMemory
	xorl	%eax, %eax
	callq	ErrorExit
.LBB11_37:
	movq	global_ideastruct+16(%rip), %rdi
	leaq	32(%rsp), %rsi
	callq	AllocateMemory
	movl	32(%rsp), %esi
	testl	%esi, %esi
	movq	%rax, %rbp
	movq	%rax, 24(%rsp)          # 8-byte Spill
	je	.LBB11_39
# BB#38:
	movl	$.L.str.58, %edi
	callq	ReportError
	leaq	32(%rsp), %rbp
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	FreeMemory
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	24(%rsp), %rbp          # 8-byte Reload
	callq	FreeMemory
	xorl	%eax, %eax
	callq	ErrorExit
.LBB11_39:                              # %.preheader77
	cmpq	$0, global_ideastruct+16(%rip)
	je	.LBB11_40
# BB#41:                                # %.lr.ph.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB11_42:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$255, %edi
	callq	abs_randwc
	movb	%al, (%r14,%r12)
	incq	%r12
	movq	global_ideastruct+16(%rip), %r13
	cmpq	%r13, %r12
	jb	.LBB11_42
	jmp	.LBB11_43
.LBB11_40:
	xorl	%r13d, %r13d
.LBB11_43:                              # %._crit_edge
	cmpl	$0, global_ideastruct(%rip)
	jne	.LBB11_56
# BB#44:                                # %.preheader76
	movq	$100, global_ideastruct+24(%rip)
	movl	$100, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	160(%rsp), %rbx
	jmp	.LBB11_45
	.p2align	4, 0x90
.LBB11_53:                              # %DoIDEAIteration.exit72
                                        #   in Loop: Header=BB11_45 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	StopStopwatch
	cmpq	global_min_ticks(%rip), %rax
	ja	.LBB11_56
# BB#54:                                #   in Loop: Header=BB11_45 Depth=1
	movq	global_ideastruct+24(%rip), %rcx
	addq	$10, %rcx
	movq	%rcx, global_ideastruct+24(%rip)
	movq	%rcx, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	cmpq	$499999, %rcx           # imm = 0x7A11F
	ja	.LBB11_56
# BB#55:                                # %._crit_edge126
                                        #   in Loop: Header=BB11_45 Depth=1
	movq	global_ideastruct+16(%rip), %r13
.LBB11_45:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_48 Depth 2
                                        #       Child Loop BB11_49 Depth 3
                                        #       Child Loop BB11_51 Depth 3
	xorl	%eax, %eax
	callq	StartStopwatch
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	je	.LBB11_53
# BB#46:                                #   in Loop: Header=BB11_45 Depth=1
	testq	%r13, %r13
	je	.LBB11_53
# BB#47:                                # %.preheader25.us.i66.preheader
                                        #   in Loop: Header=BB11_45 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_48:                              # %.preheader25.us.i66
                                        #   Parent Loop BB11_45 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB11_49 Depth 3
                                        #       Child Loop BB11_51 Depth 3
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_49:                              #   Parent Loop BB11_45 Depth=1
                                        #     Parent Loop BB11_48 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	(%r14,%rbp), %rdi
	leaq	(%r15,%rbp), %rsi
	movq	%rbx, %rdx
	callq	cipher_idea
	addq	$8, %rbp
	cmpq	%r13, %rbp
	jb	.LBB11_49
# BB#50:                                # %.lr.ph28.us.i70.preheader
                                        #   in Loop: Header=BB11_48 Depth=2
	xorl	%r12d, %r12d
	movq	24(%rsp), %rbp          # 8-byte Reload
	leaq	272(%rsp), %rbx
	.p2align	4, 0x90
.LBB11_51:                              # %.lr.ph28.us.i70
                                        #   Parent Loop BB11_45 Depth=1
                                        #     Parent Loop BB11_48 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	(%r15,%r12), %rdi
	leaq	(%rbp,%r12), %rsi
	movq	%rbx, %rdx
	callq	cipher_idea
	addq	$8, %r12
	cmpq	%r13, %r12
	jb	.LBB11_51
# BB#52:                                # %._crit_edge.us.i68
                                        #   in Loop: Header=BB11_48 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	cmpq	(%rsp), %rcx            # 8-byte Folded Reload
	leaq	160(%rsp), %rbx
	jne	.LBB11_48
	jmp	.LBB11_53
.LBB11_56:                              # %.preheader.preheader
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movq	global_ideastruct+24(%rip), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	160(%rsp), %r12
	leaq	272(%rsp), %r13
	xorl	%ebx, %ebx
	movq	%r14, 152(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB11_57:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_60 Depth 2
                                        #       Child Loop BB11_61 Depth 3
                                        #       Child Loop BB11_63 Depth 3
	movq	%rbx, 144(%rsp)         # 8-byte Spill
	movq	global_ideastruct+16(%rip), %rbp
	xorl	%eax, %eax
	callq	StartStopwatch
	movq	%rax, 136(%rsp)         # 8-byte Spill
	testq	%rbp, %rbp
	je	.LBB11_65
# BB#58:                                # %.preheader
                                        #   in Loop: Header=BB11_57 Depth=1
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	je	.LBB11_65
# BB#59:                                # %.preheader25.us.i.preheader
                                        #   in Loop: Header=BB11_57 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB11_60:                              # %.preheader25.us.i
                                        #   Parent Loop BB11_57 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB11_61 Depth 3
                                        #       Child Loop BB11_63 Depth 3
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB11_61:                              #   Parent Loop BB11_57 Depth=1
                                        #     Parent Loop BB11_60 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	(%r14,%rbx), %rdi
	leaq	(%r15,%rbx), %rsi
	movq	%r12, %rdx
	callq	cipher_idea
	addq	$8, %rbx
	cmpq	%rbp, %rbx
	jb	.LBB11_61
# BB#62:                                # %.lr.ph28.us.i.preheader
                                        #   in Loop: Header=BB11_60 Depth=2
	xorl	%r14d, %r14d
	movq	24(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB11_63:                              # %.lr.ph28.us.i
                                        #   Parent Loop BB11_57 Depth=1
                                        #     Parent Loop BB11_60 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	(%r15,%r14), %rdi
	leaq	(%rbx,%r14), %rsi
	movq	%r13, %rdx
	callq	cipher_idea
	addq	$8, %r14
	cmpq	%rbp, %r14
	jb	.LBB11_63
# BB#64:                                # %._crit_edge.us.i
                                        #   in Loop: Header=BB11_60 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	incq	%rax
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	movq	152(%rsp), %r14         # 8-byte Reload
	jne	.LBB11_60
.LBB11_65:                              # %DoIDEAIteration.exit
                                        #   in Loop: Header=BB11_57 Depth=1
	movq	136(%rsp), %rdi         # 8-byte Reload
	callq	StopStopwatch
	movq	%rax, %rbx
	addq	144(%rsp), %rbx         # 8-byte Folded Reload
	movq	global_ideastruct+24(%rip), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movd	%rax, %xmm0
	punpckldq	.LCPI11_0(%rip), %xmm0 # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	subpd	.LCPI11_1(%rip), %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm1
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	.LCPI11_2(%rip), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB11_57
# BB#66:
	leaq	32(%rsp), %r12
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	FreeMemory
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	FreeMemory
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rsi
	callq	FreeMemory
	movq	%rbx, %rdi
	callq	TicksToFracSecs
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, global_ideastruct+32(%rip)
	cmpl	$0, global_ideastruct(%rip)
	jne	.LBB11_68
# BB#67:
	movl	$1, global_ideastruct(%rip)
.LBB11_68:
	addq	$376, %rsp              # imm = 0x178
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	DoIDEA, .Lfunc_end11-DoIDEA
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI12_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI12_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI12_2:
	.quad	4647503709213818880     # double 500
	.text
	.globl	DoHuffman
	.p2align	4, 0x90
	.type	DoHuffman,@function
DoHuffman:                              # @DoHuffman
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi130:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi131:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi133:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi134:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi136:
	.cfi_def_cfa_offset 144
.Lcfi137:
	.cfi_offset %rbx, -56
.Lcfi138:
	.cfi_offset %r12, -48
.Lcfi139:
	.cfi_offset %r13, -40
.Lcfi140:
	.cfi_offset %r14, -32
.Lcfi141:
	.cfi_offset %r15, -24
.Lcfi142:
	.cfi_offset %rbp, -16
	movq	global_huffstruct+16(%rip), %rdi
	leaq	4(%rsp), %rsi
	callq	AllocateMemory
	movq	%rax, %rbp
	movl	4(%rsp), %esi
	testl	%esi, %esi
	je	.LBB12_2
# BB#1:
	movl	$.L.str.59, %edi
	callq	ReportError
	xorl	%eax, %eax
	callq	ErrorExit
.LBB12_2:
	movq	global_huffstruct+16(%rip), %rdi
	leaq	4(%rsp), %rsi
	callq	AllocateMemory
	movq	%rax, %r14
	movl	4(%rsp), %esi
	testl	%esi, %esi
	je	.LBB12_4
# BB#3:
	movl	$.L.str.59, %edi
	callq	ReportError
	leaq	4(%rsp), %rsi
	movq	%rbp, %rdi
	callq	FreeMemory
	xorl	%eax, %eax
	callq	ErrorExit
.LBB12_4:
	movq	global_huffstruct+16(%rip), %rdi
	leaq	4(%rsp), %rsi
	callq	AllocateMemory
	movq	%rax, %r15
	movl	4(%rsp), %esi
	testl	%esi, %esi
	je	.LBB12_6
# BB#5:
	movl	$.L.str.59, %edi
	callq	ReportError
	leaq	4(%rsp), %rbx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	xorl	%eax, %eax
	callq	ErrorExit
.LBB12_6:
	leaq	4(%rsp), %rsi
	movl	$10240, %edi            # imm = 0x2800
	callq	AllocateMemory
	movq	%rax, hufftree(%rip)
	movl	4(%rsp), %esi
	testl	%esi, %esi
	je	.LBB12_8
# BB#7:
	movl	$.L.str.59, %edi
	callq	ReportError
	leaq	4(%rsp), %rbx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	xorl	%eax, %eax
	callq	ErrorExit
.LBB12_8:
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movl	$13, %edi
	callq	randnum
	movq	global_huffstruct+16(%rip), %rbx
	decq	%rbx
	xorl	%r15d, %r15d
	leaq	48(%rsp), %r13
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB12_9:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_11 Depth 2
	movl	$494, %edi              # imm = 0x1EE
	callq	abs_randwc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	addl	$6, %eax
	leaq	(%rax,%r15), %rcx
	movq	%rbx, %r14
	subq	%r15, %r14
	cmpq	%rbx, %rcx
	cmovbeq	%rax, %r14
	cmpq	$2, %r14
	jb	.LBB12_13
# BB#10:                                #   in Loop: Header=BB12_9 Depth=1
	xorl	%r12d, %r12d
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB12_11:                              #   Parent Loop BB12_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$50, %edi
	callq	abs_randwc
	movl	%eax, %eax
	movq	wordcatarray(,%rax,8), %rbp
	movq	%rbp, %rdi
	callq	strlen
	leaq	1(%rax), %rdx
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	MoveMemory
	movq	%r13, %rdi
	callq	strlen
	movb	$32, 48(%rsp,%rax)
	leaq	1(%rax,%r12), %rcx
	incq	%rax
	movq	%r14, %rbp
	subq	%r12, %rbp
	cmpq	%r14, %rcx
	cmovleq	%rax, %rbp
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	MoveMemory
	addq	%rbp, %r12
	addq	%rbp, %rbx
	cmpq	%r14, %r12
	jl	.LBB12_11
# BB#12:                                # %create_text_line.exit.i
                                        #   in Loop: Header=BB12_9 Depth=1
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB12_13:                              #   in Loop: Header=BB12_9 Depth=1
	movb	$10, -1(%rbp,%r14)
	addq	%r14, %rbp
	addq	%r14, %r15
	cmpq	%rbx, %r15
	jb	.LBB12_9
# BB#14:                                # %create_text_block.exit
	movq	global_huffstruct+16(%rip), %rax
	movq	32(%rsp), %rbp          # 8-byte Reload
	movb	$0, -1(%rbp,%rax)
	cmpl	$0, global_huffstruct(%rip)
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	jne	.LBB12_18
# BB#15:                                # %.preheader57
	movq	$100, global_huffstruct+24(%rip)
	movl	$100, %r8d
	.p2align	4, 0x90
.LBB12_16:                              # =>This Inner Loop Header: Depth=1
	movq	global_huffstruct+16(%rip), %rcx
	movq	hufftree(%rip), %r9
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	DoHuffIteration
	cmpq	global_min_ticks(%rip), %rax
	ja	.LBB12_18
# BB#17:                                #   in Loop: Header=BB12_16 Depth=1
	movq	global_huffstruct+24(%rip), %r8
	addq	$10, %r8
	movq	%r8, global_huffstruct+24(%rip)
	cmpq	$500000, %r8            # imm = 0x7A120
	jb	.LBB12_16
.LBB12_18:                              # %.preheader.preheader
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movq	global_huffstruct+24(%rip), %r8
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_19:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	global_huffstruct+16(%rip), %rcx
	movq	hufftree(%rip), %r9
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	DoHuffIteration
	addq	%rax, %rbx
	movq	global_huffstruct+24(%rip), %r8
	movd	%r8, %xmm0
	punpckldq	.LCPI12_0(%rip), %xmm0 # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	subpd	.LCPI12_1(%rip), %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm1
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	.LCPI12_2(%rip), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB12_19
# BB#20:
	leaq	4(%rsp), %r14
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	FreeMemory
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	FreeMemory
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	FreeMemory
	movq	hufftree(%rip), %rdi
	movq	%r14, %rsi
	callq	FreeMemory
	movq	%rbx, %rdi
	callq	TicksToFracSecs
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, global_huffstruct+32(%rip)
	cmpl	$0, global_huffstruct(%rip)
	jne	.LBB12_22
# BB#21:
	movl	$1, global_huffstruct(%rip)
.LBB12_22:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	DoHuffman, .Lfunc_end12-DoHuffman
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI13_0:
	.long	1065353216              # float 1
.LCPI13_1:
	.long	1073741824              # float 2
	.text
	.p2align	4, 0x90
	.type	DoHuffIteration,@function
DoHuffIteration:                        # @DoHuffIteration
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi143:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi144:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi145:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi146:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi147:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi148:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi149:
	.cfi_def_cfa_offset 208
.Lcfi150:
	.cfi_offset %rbx, -56
.Lcfi151:
	.cfi_offset %r12, -48
.Lcfi152:
	.cfi_offset %r13, -40
.Lcfi153:
	.cfi_offset %r14, -32
.Lcfi154:
	.cfi_offset %r15, -24
.Lcfi155:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
	movq	%r8, %rbp
	movq	%rcx, %r14
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, (%rsp)            # 8-byte Spill
	xorl	%eax, %eax
	callq	StartStopwatch
	movq	%rbp, %r11
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testq	%r11, %r11
	je	.LBB13_89
# BB#1:                                 # %.preheader165.lr.ph
	movl	%r14d, %eax
	andl	$1, %eax
	testq	%r14, %r14
	movq	%rax, 48(%rsp)          # 8-byte Spill
	js	.LBB13_2
# BB#3:                                 # %.preheader165.lr.ph
	cvtsi2ssq	%r14, %xmm4
	jmp	.LBB13_4
.LBB13_2:
	movq	%rax, %rcx
	movq	%r14, %rax
	shrq	%rax
	orq	%rcx, %rax
	cvtsi2ssq	%rax, %xmm4
	addss	%xmm4, %xmm4
.LBB13_4:                               # %.preheader165.lr.ph
	leaq	5120(%rbx), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	64(%rbx), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	1(%rax), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	28(%rbx), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorps	%xmm5, %xmm5
	xorps	%xmm6, %xmm6
	movss	.LCPI13_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movq	%r14, 72(%rsp)          # 8-byte Spill
	movss	%xmm4, 12(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB13_6:                               # %.preheader165
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_7 Depth 2
                                        #     Child Loop BB13_14 Depth 2
                                        #     Child Loop BB13_16 Depth 2
                                        #     Child Loop BB13_26 Depth 2
                                        #     Child Loop BB13_32 Depth 2
                                        #       Child Loop BB13_41 Depth 3
                                        #       Child Loop BB13_61 Depth 3
                                        #     Child Loop BB13_73 Depth 2
                                        #       Child Loop BB13_75 Depth 3
                                        #       Child Loop BB13_77 Depth 3
                                        #     Child Loop BB13_84 Depth 2
                                        #       Child Loop BB13_87 Depth 3
	movq	56(%rsp), %rax          # 8-byte Reload
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB13_7:                               #   Parent Loop BB13_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$0, -60(%rax)
	movb	%cl, -64(%rax)
	movl	$0, -40(%rax)
	leal	1(%rcx), %edx
	movb	%dl, -44(%rax)
	movl	$0, -20(%rax)
	leal	2(%rcx), %edx
	movb	%dl, -24(%rax)
	movl	$0, (%rax)
	leal	3(%rcx), %edx
	movb	%dl, -4(%rax)
	addq	$4, %rcx
	addq	$80, %rax
	cmpq	$256, %rcx              # imm = 0x100
	jne	.LBB13_7
# BB#8:                                 # %.preheader164
                                        #   in Loop: Header=BB13_6 Depth=1
	testq	%r14, %r14
	je	.LBB13_15
# BB#9:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB13_6 Depth=1
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	jne	.LBB13_11
# BB#10:                                #   in Loop: Header=BB13_6 Depth=1
	xorl	%eax, %eax
	cmpq	$1, %r14
	jne	.LBB13_13
	jmp	.LBB13_15
	.p2align	4, 0x90
.LBB13_11:                              # %.lr.ph.prol
                                        #   in Loop: Header=BB13_6 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movsbq	(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	movss	4(%rbx,%rax,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	movss	%xmm0, 4(%rbx,%rax,4)
	movl	$1, %eax
	cmpq	$1, %r14
	je	.LBB13_15
.LBB13_13:                              # %.lr.ph.preheader.new
                                        #   in Loop: Header=BB13_6 Depth=1
	movq	%r14, %rcx
	subq	%rax, %rcx
	addq	40(%rsp), %rax          # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB13_14:                              # %.lr.ph
                                        #   Parent Loop BB13_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	-1(%rax), %rdx
	leaq	(%rdx,%rdx,4), %rdx
	movss	4(%rbx,%rdx,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	movss	%xmm0, 4(%rbx,%rdx,4)
	movsbq	(%rax), %rdx
	leaq	(%rdx,%rdx,4), %rdx
	movss	4(%rbx,%rdx,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	movss	%xmm0, 4(%rbx,%rdx,4)
	addq	$2, %rax
	addq	$-2, %rcx
	jne	.LBB13_14
.LBB13_15:                              # %vector.body.preheader
                                        #   in Loop: Header=BB13_6 Depth=1
	decq	%r11
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB13_16:                              # %vector.body
                                        #   Parent Loop BB13_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	4(%rbx,%rax), %xmm1
	movups	20(%rbx,%rax), %xmm0
	movaps	%xmm0, %xmm3
	shufps	$1, %xmm1, %xmm3        # xmm3 = xmm3[1,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm3      # xmm3 = xmm3[2,0],xmm1[2,3]
	cmpneqps	%xmm5, %xmm3
	shufps	$212, %xmm3, %xmm3      # xmm3 = xmm3[0,1,1,3]
	psllq	$32, %xmm3
	pshufd	$237, %xmm3, %xmm2      # xmm2 = xmm3[1,3,2,3]
	psrad	$31, %xmm3
	pshufd	$237, %xmm3, %xmm3      # xmm3 = xmm3[1,3,2,3]
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	movdqa	%xmm2, 96(%rsp)
	testb	$1, 96(%rsp)
	je	.LBB13_18
# BB#17:                                # %pred.store.if
                                        #   in Loop: Header=BB13_16 Depth=2
	divss	%xmm4, %xmm1
	movss	%xmm1, 4(%rbx,%rax)
.LBB13_18:                              # %pred.store.continue
                                        #   in Loop: Header=BB13_16 Depth=2
	movdqa	%xmm2, 80(%rsp)
	testb	$1, 88(%rsp)
	je	.LBB13_20
# BB#19:                                # %pred.store.if240
                                        #   in Loop: Header=BB13_16 Depth=2
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	divss	%xmm4, %xmm0
	movss	%xmm0, 24(%rbx,%rax)
.LBB13_20:                              # %pred.store.continue241
                                        #   in Loop: Header=BB13_16 Depth=2
	addq	$40, %rax
	cmpq	$5080, %rax             # imm = 0x13D8
	jne	.LBB13_16
# BB#21:                                # %.preheader163
                                        #   in Loop: Header=BB13_6 Depth=1
	movss	5084(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm6, %xmm0
	jne	.LBB13_22
	jnp	.LBB13_23
.LBB13_22:                              #   in Loop: Header=BB13_6 Depth=1
	divss	%xmm4, %xmm0
	movss	%xmm0, 5084(%rbx)
.LBB13_23:                              # %.preheader163.1246
                                        #   in Loop: Header=BB13_6 Depth=1
	movq	%r11, 16(%rsp)          # 8-byte Spill
	movss	5104(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm6, %xmm0
	jne	.LBB13_24
	jnp	.LBB13_25
.LBB13_24:                              #   in Loop: Header=BB13_6 Depth=1
	divss	%xmm4, %xmm0
	movss	%xmm0, 5104(%rbx)
.LBB13_25:                              # %.loopexit242247
                                        #   in Loop: Header=BB13_6 Depth=1
	xorl	%esi, %esi
	movl	$5120, %edx             # imm = 0x1400
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	memset
	movl	$36, %eax
	xorps	%xmm6, %xmm6
	movss	.LCPI13_1(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB13_26:                              #   Parent Loop BB13_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	-32(%rbx,%rax), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movl	$32000, %ecx            # imm = 0x7D00
	ucomiss	%xmm6, %xmm0
	movl	$32000, %edx            # imm = 0x7D00
	jne	.LBB13_27
	jnp	.LBB13_28
.LBB13_27:                              #   in Loop: Header=BB13_26 Depth=2
	movq	$-1, -24(%rbx,%rax)
	movl	$-1, %edx
.LBB13_28:                              #   in Loop: Header=BB13_26 Depth=2
	movl	%edx, -28(%rbx,%rax)
	movss	-12(%rbx,%rax), %xmm0   # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm6, %xmm0
	jne	.LBB13_29
	jnp	.LBB13_30
.LBB13_29:                              #   in Loop: Header=BB13_26 Depth=2
	movq	$-1, -4(%rbx,%rax)
	movl	$-1, %ecx
.LBB13_30:                              #   in Loop: Header=BB13_26 Depth=2
	movl	%ecx, -8(%rbx,%rax)
	addq	$40, %rax
	cmpq	$10276, %rax            # imm = 0x2824
	jne	.LBB13_26
# BB#31:                                # %.lr.ph173.preheader.preheader
                                        #   in Loop: Header=BB13_6 Depth=1
	xorl	%r9d, %r9d
	movl	$255, %r8d
	movl	$256, %edx              # imm = 0x100
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	xorps	%xmm5, %xmm5
	jmp	.LBB13_32
	.p2align	4, 0x90
.LBB13_70:                              # %._crit_edge179
                                        #   in Loop: Header=BB13_32 Depth=2
	cmpl	$-1, %r11d
	movq	72(%rsp), %r14          # 8-byte Reload
	je	.LBB13_49
# BB#71:                                #   in Loop: Header=BB13_32 Depth=2
	incq	%r8
	movslq	%r10d, %rax
	leaq	(%rax,%rax,4), %rax
	movl	%r8d, 8(%rbx,%rax,4)
	movslq	%r11d, %rax
	leaq	(%rax,%rax,4), %rax
	movl	%r8d, 8(%rbx,%rax,4)
	addss	%xmm1, %xmm0
	leaq	(%r8,%r8,4), %rax
	movss	%xmm0, 4(%rbx,%rax,4)
	movl	%r10d, 12(%rbx,%rax,4)
	movl	%r11d, 16(%rbx,%rax,4)
	movl	$-2, 8(%rbx,%rax,4)
	incq	%rdx
	incq	%r9
.LBB13_32:                              # %.lr.ph173.preheader
                                        #   Parent Loop BB13_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB13_41 Depth 3
                                        #       Child Loop BB13_61 Depth 3
	movl	$-1, %r10d
	testb	$1, %r9b
	jne	.LBB13_34
# BB#33:                                #   in Loop: Header=BB13_32 Depth=2
	xorl	%edi, %edi
	movaps	%xmm3, %xmm0
	cmpq	$-255, %r9
	jne	.LBB13_40
	jmp	.LBB13_48
	.p2align	4, 0x90
.LBB13_34:                              # %.lr.ph173.prol
                                        #   in Loop: Header=BB13_32 Depth=2
	cmpl	$0, 8(%rbx)
	jns	.LBB13_35
# BB#36:                                #   in Loop: Header=BB13_32 Depth=2
	movss	4(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm3
	jbe	.LBB13_35
# BB#37:                                #   in Loop: Header=BB13_32 Depth=2
	xorl	%r10d, %r10d
	jmp	.LBB13_38
	.p2align	4, 0x90
.LBB13_35:                              #   in Loop: Header=BB13_32 Depth=2
	movaps	%xmm3, %xmm0
.LBB13_38:                              # %.lr.ph173.prol.loopexit
                                        #   in Loop: Header=BB13_32 Depth=2
	movl	$1, %edi
	cmpq	$-255, %r9
	je	.LBB13_48
.LBB13_40:                              # %.lr.ph173.preheader.new
                                        #   in Loop: Header=BB13_32 Depth=2
	leaq	(%rdi,%rdi,4), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB13_41:                              # %.lr.ph173
                                        #   Parent Loop BB13_6 Depth=1
                                        #     Parent Loop BB13_32 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	$0, -20(%rax)
	jns	.LBB13_44
# BB#42:                                #   in Loop: Header=BB13_41 Depth=3
	movss	-24(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jbe	.LBB13_44
# BB#43:                                #   in Loop: Header=BB13_41 Depth=3
	leal	(%rdi,%rcx), %r10d
	movaps	%xmm1, %xmm0
	.p2align	4, 0x90
.LBB13_44:                              # %.lr.ph173.1250
                                        #   in Loop: Header=BB13_41 Depth=3
	cmpl	$0, (%rax)
	jns	.LBB13_47
# BB#45:                                #   in Loop: Header=BB13_41 Depth=3
	movss	-4(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jbe	.LBB13_47
# BB#46:                                #   in Loop: Header=BB13_41 Depth=3
	leal	1(%rdi,%rcx), %r10d
	movaps	%xmm1, %xmm0
	.p2align	4, 0x90
.LBB13_47:                              #   in Loop: Header=BB13_41 Depth=3
	leaq	2(%rdi,%rcx), %rsi
	addq	$2, %rcx
	addq	$40, %rax
	cmpq	%rdx, %rsi
	jne	.LBB13_41
.LBB13_48:                              # %._crit_edge
                                        #   in Loop: Header=BB13_32 Depth=2
	cmpl	$-1, %r10d
	je	.LBB13_49
# BB#51:                                # %.lr.ph178.preheader
                                        #   in Loop: Header=BB13_32 Depth=2
	testb	$1, %r9b
	jne	.LBB13_53
# BB#52:                                #   in Loop: Header=BB13_32 Depth=2
	movl	$-1, %r11d
	xorl	%r14d, %r14d
	movaps	%xmm3, %xmm1
	cmpq	$-255, %r9
	je	.LBB13_70
	jmp	.LBB13_60
	.p2align	4, 0x90
.LBB13_53:                              # %.lr.ph178.prol
                                        #   in Loop: Header=BB13_32 Depth=2
	testl	%r10d, %r10d
	movl	$-1, %r11d
	je	.LBB13_56
# BB#54:                                # %.lr.ph178.prol
                                        #   in Loop: Header=BB13_32 Depth=2
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jns	.LBB13_56
# BB#55:                                #   in Loop: Header=BB13_32 Depth=2
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm3
	jbe	.LBB13_56
# BB#57:                                #   in Loop: Header=BB13_32 Depth=2
	xorl	%r11d, %r11d
	jmp	.LBB13_58
	.p2align	4, 0x90
.LBB13_56:                              #   in Loop: Header=BB13_32 Depth=2
	movaps	%xmm3, %xmm1
.LBB13_58:                              # %.lr.ph178.prol.loopexit
                                        #   in Loop: Header=BB13_32 Depth=2
	movl	$1, %r14d
	cmpq	$-255, %r9
	je	.LBB13_70
.LBB13_60:                              # %.lr.ph178.preheader.new
                                        #   in Loop: Header=BB13_32 Depth=2
	movl	%r10d, %eax
	leaq	(%r14,%r14,4), %rcx
	movq	24(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rcx,4), %rcx
	movq	%r14, %r15
	subq	%rax, %r15
	xorl	%edi, %edi
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB13_61:                              # %.lr.ph178
                                        #   Parent Loop BB13_6 Depth=1
                                        #     Parent Loop BB13_32 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r15, %rsi
	addq	%rdi, %rsi
	je	.LBB13_65
# BB#62:                                # %.lr.ph178
                                        #   in Loop: Header=BB13_61 Depth=3
	movl	-20(%rcx), %esi
	testl	%esi, %esi
	jns	.LBB13_65
# BB#63:                                #   in Loop: Header=BB13_61 Depth=3
	movss	-24(%rcx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm1
	jbe	.LBB13_65
# BB#64:                                #   in Loop: Header=BB13_61 Depth=3
	leal	(%r14,%rdi), %r11d
	movaps	%xmm2, %xmm1
.LBB13_65:                              # %.lr.ph178.1253
                                        #   in Loop: Header=BB13_61 Depth=3
	leaq	1(%rbp), %rsi
	cmpq	%rax, %rsi
	je	.LBB13_69
# BB#66:                                # %.lr.ph178.1253
                                        #   in Loop: Header=BB13_61 Depth=3
	movl	(%rcx), %esi
	testl	%esi, %esi
	jns	.LBB13_69
# BB#67:                                #   in Loop: Header=BB13_61 Depth=3
	movss	-4(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm1
	jbe	.LBB13_69
# BB#68:                                #   in Loop: Header=BB13_61 Depth=3
	leal	1(%r14,%rdi), %r11d
	movaps	%xmm2, %xmm1
.LBB13_69:                              #   in Loop: Header=BB13_61 Depth=3
	addq	$2, %rbp
	addq	$40, %rcx
	addq	$2, %rdi
	cmpq	%rdx, %rbp
	jne	.LBB13_61
	jmp	.LBB13_70
	.p2align	4, 0x90
.LBB13_49:                              # %.preheader161
                                        #   in Loop: Header=BB13_6 Depth=1
	testq	%r14, %r14
	je	.LBB13_50
# BB#72:                                # %.lr.ph193.preheader
                                        #   in Loop: Header=BB13_6 Depth=1
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
	movq	16(%rsp), %r11          # 8-byte Reload
	movss	.LCPI13_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB13_73:                              # %.lr.ph193
                                        #   Parent Loop BB13_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB13_75 Depth 3
                                        #       Child Loop BB13_77 Depth 3
	movq	(%rsp), %rax            # 8-byte Reload
	movsbq	(%rax,%r10), %rcx
	leaq	(%rcx,%rcx,4), %rax
	movl	8(%rbx,%rax,4), %edx
	cmpl	$-2, %edx
	je	.LBB13_82
# BB#74:                                # %.lr.ph185.preheader
                                        #   in Loop: Header=BB13_73 Depth=2
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB13_75:                              # %.lr.ph185
                                        #   Parent Loop BB13_6 Depth=1
                                        #     Parent Loop BB13_73 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%edx, %rsi
	leaq	(%rsi,%rsi,4), %rsi
	cmpl	%ecx, 12(%rbx,%rsi,4)
	setne	%cl
	orb	$48, %cl
	movb	%cl, 112(%rsp,%rax)
	incq	%rax
	movl	%edx, %ecx
	movl	8(%rbx,%rsi,4), %edx
	cmpl	$-2, %edx
	jne	.LBB13_75
# BB#76:                                # %.lr.ph188.preheader
                                        #   in Loop: Header=BB13_73 Depth=2
	movl	%r9d, %edi
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB13_77:                              # %.lr.ph188
                                        #   Parent Loop BB13_6 Depth=1
                                        #     Parent Loop BB13_73 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%edi, %esi
	shrl	$3, %esi
	movl	%edi, %ecx
	andb	$7, %cl
	movl	$1, %edx
	shll	%cl, %edx
	cmpb	$49, 111(%rsp,%rbp)
	leaq	-1(%rbp), %rbp
	jne	.LBB13_79
# BB#78:                                #   in Loop: Header=BB13_77 Depth=3
	movl	%esi, %esi
	leaq	(%r13,%rsi), %rcx
	movzbl	(%r13,%rsi), %esi
	orl	%edx, %esi
	jmp	.LBB13_80
	.p2align	4, 0x90
.LBB13_79:                              #   in Loop: Header=BB13_77 Depth=3
	notl	%edx
	movl	%esi, %esi
	leaq	(%r13,%rsi), %rcx
	movzbl	(%r13,%rsi), %esi
	andl	%edx, %esi
.LBB13_80:                              # %SetCompBit.exit
                                        #   in Loop: Header=BB13_77 Depth=3
	movb	%sil, (%rcx)
	incl	%edi
	testq	%rbp, %rbp
	jne	.LBB13_77
# BB#81:                                # %._crit_edge189.loopexit
                                        #   in Loop: Header=BB13_73 Depth=2
	addq	%rax, %r9
.LBB13_82:                              # %._crit_edge189
                                        #   in Loop: Header=BB13_73 Depth=2
	incq	%r10
	cmpq	%r14, %r10
	jne	.LBB13_73
	jmp	.LBB13_83
	.p2align	4, 0x90
.LBB13_50:                              #   in Loop: Header=BB13_6 Depth=1
	xorl	%r9d, %r9d
	movq	16(%rsp), %r11          # 8-byte Reload
	movss	.LCPI13_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
.LBB13_83:                              # %.preheader160
                                        #   in Loop: Header=BB13_6 Depth=1
	movslq	%r8d, %r8
	leaq	(%r8,%r8,4), %rax
	leaq	12(%rbx,%rax,4), %r10
	xorl	%ebp, %ebp
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB13_84:                              #   Parent Loop BB13_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB13_87 Depth 3
	cmpl	$-1, (%r10)
	je	.LBB13_85
# BB#86:                                # %.lr.ph197.preheader
                                        #   in Loop: Header=BB13_84 Depth=2
	movq	%r10, %rcx
	movq	%r8, %rax
	.p2align	4, 0x90
.LBB13_87:                              # %.lr.ph197
                                        #   Parent Loop BB13_6 Depth=1
                                        #     Parent Loop BB13_84 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ebp, %esi
	shrl	$3, %esi
	movl	%ebp, %edx
	andb	$7, %dl
	movzbl	(%r13,%rsi), %esi
	movzbl	%dl, %edx
	btl	%edx, %esi
	leaq	(%rax,%rax,4), %rax
	leaq	16(%rbx,%rax,4), %rax
	cmovaeq	%rcx, %rax
	incq	%rbp
	movslq	(%rax), %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	12(%rbx,%rdx,4), %rcx
	cmpl	$-1, 12(%rbx,%rdx,4)
	jne	.LBB13_87
	jmp	.LBB13_88
	.p2align	4, 0x90
.LBB13_85:                              #   in Loop: Header=BB13_84 Depth=2
	movq	%r8, %rax
.LBB13_88:                              # %._crit_edge198
                                        #   in Loop: Header=BB13_84 Depth=2
	leaq	(%rax,%rax,4), %rax
	movb	(%rbx,%rax,4), %al
	movb	%al, (%r12,%rdi)
	incq	%rdi
	cmpq	%r9, %rbp
	jl	.LBB13_84
# BB#5:                                 # %.loopexit
                                        #   in Loop: Header=BB13_6 Depth=1
	testq	%r11, %r11
	jne	.LBB13_6
.LBB13_89:                              # %._crit_edge201
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	StopStopwatch
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	DoHuffIteration, .Lfunc_end13-DoHuffIteration
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI14_0:
	.quad	4606281698874543309     # double 0.90000000000000002
.LCPI14_1:
	.quad	4591870180066957722     # double 0.10000000000000001
.LCPI14_4:
	.quad	4622945017495814144     # double 12
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI14_2:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI14_3:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.text
	.globl	DoNNET
	.p2align	4, 0x90
	.type	DoNNET,@function
DoNNET:                                 # @DoNNET
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi156:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi157:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi158:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi159:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi160:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi161:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi162:
	.cfi_def_cfa_offset 112
.Lcfi163:
	.cfi_offset %rbx, -56
.Lcfi164:
	.cfi_offset %r12, -48
.Lcfi165:
	.cfi_offset %r13, -40
.Lcfi166:
	.cfi_offset %r14, -32
.Lcfi167:
	.cfi_offset %r15, -24
.Lcfi168:
	.cfi_offset %rbp, -16
	movl	$3, %edi
	callq	randnum
	movq	inpath(%rip), %rdi
	movl	$.L.str.64, %esi
	callq	fopen
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB14_1
# BB#3:
	leaq	48(%rsp), %rdx
	leaq	32(%rsp), %rcx
	leaq	52(%rsp), %r8
	movl	$.L.str.66, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fscanf
	movl	%eax, %ecx
	cmpl	$3, %ecx
	jne	.LBB14_4
# BB#6:
	movl	$.L.str.68, %esi
	movl	$numpats, %edx
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fscanf
	movl	%eax, %ecx
	cmpl	$1, %ecx
	jne	.LBB14_7
# BB#8:
	movl	numpats(%rip), %eax
	cmpl	$11, %eax
	jl	.LBB14_10
# BB#9:                                 # %.preheader80.thread.i
	movl	$10, numpats(%rip)
	jmp	.LBB14_11
.LBB14_1:
	xorl	%ebx, %ebx
	movl	$.L.str.65, %edi
	jmp	.LBB14_2
.LBB14_4:
	xorl	%ebx, %ebx
	movl	$.L.str.67, %edi
	jmp	.LBB14_5
.LBB14_7:
	xorl	%ebx, %ebx
	movl	$.L.str.69, %edi
.LBB14_5:                               # %read_data_file.exit
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	testb	%bl, %bl
	je	.LBB14_25
	jmp	.LBB14_26
.LBB14_10:                              # %.preheader80.i
	testl	%eax, %eax
	jle	.LBB14_23
.LBB14_11:                              # %.preheader79.i.preheader
	movl	$in_pats+8, %r14d
	leaq	28(%rsp), %r12
	leaq	24(%rsp), %r13
	xorl	%ebp, %ebp
.LBB14_12:                              # %.preheader79.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_14 Depth 2
                                        #     Child Loop BB14_18 Depth 2
	movq	%r14, 16(%rsp)          # 8-byte Spill
	cmpl	$0, 32(%rsp)
	jle	.LBB14_17
# BB#13:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB14_12 Depth=1
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB14_14:                              # %.lr.ph.i
                                        #   Parent Loop BB14_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	subq	$8, %rsp
.Lcfi169:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.70, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	leaq	20(%rsp), %rdx
	leaq	16(%rsp), %rcx
	movq	%r12, %r8
	movq	%r13, %r9
	leaq	12(%rsp), %rbx
	pushq	%rbx
.Lcfi170:
	.cfi_adjust_cfa_offset 8
	callq	fscanf
	addq	$16, %rsp
.Lcfi171:
	.cfi_adjust_cfa_offset -16
	cmpl	$5, %eax
	jne	.LBB14_15
# BB#16:                                #   in Loop: Header=BB14_14 Depth=2
	movslq	48(%rsp), %rax
	movslq	%r14d, %r14
	imulq	%r14, %rax
	cvtsi2sdl	12(%rsp), %xmm0
	imulq	$280, %rbp, %rcx        # imm = 0x118
	movsd	%xmm0, in_pats(%rcx,%rax,8)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	8(%rsp), %xmm0
	cltq
	movsd	%xmm0, in_pats+8(%rcx,%rax,8)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	28(%rsp), %xmm0
	movsd	%xmm0, in_pats+16(%rcx,%rax,8)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	24(%rsp), %xmm0
	movsd	%xmm0, in_pats+24(%rcx,%rax,8)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	4(%rsp), %xmm0
	movsd	%xmm0, in_pats+32(%rcx,%rax,8)
	incl	%r14d
	cmpl	32(%rsp), %r14d
	jl	.LBB14_14
.LBB14_17:                              # %.preheader.i.preheader
                                        #   in Loop: Header=BB14_12 Depth=1
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rax
	movl	$34, %ecx
	movsd	.LCPI14_0(%rip), %xmm2  # xmm2 = mem[0],zero
	movsd	.LCPI14_1(%rip), %xmm3  # xmm3 = mem[0],zero
	jmp	.LBB14_18
	.p2align	4, 0x90
.LBB14_38:                              #   in Loop: Header=BB14_18 Depth=2
	addq	$-2, %rcx
	addq	$16, %rax
.LBB14_18:                              # %.preheader.i
                                        #   Parent Loop BB14_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rax), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm2, %xmm1
	movapd	%xmm2, %xmm0
	jae	.LBB14_20
# BB#19:                                #   in Loop: Header=BB14_18 Depth=2
	ucomisd	%xmm1, %xmm3
	movapd	%xmm3, %xmm0
	jb	.LBB14_21
.LBB14_20:                              # %.sink.split.i
                                        #   in Loop: Header=BB14_18 Depth=2
	movsd	%xmm0, -8(%rax)
.LBB14_21:                              #   in Loop: Header=BB14_18 Depth=2
	testq	%rcx, %rcx
	je	.LBB14_22
# BB#35:                                # %.preheader.i.1
                                        #   in Loop: Header=BB14_18 Depth=2
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm2, %xmm1
	movapd	%xmm2, %xmm0
	jae	.LBB14_37
# BB#36:                                #   in Loop: Header=BB14_18 Depth=2
	ucomisd	%xmm1, %xmm3
	movapd	%xmm3, %xmm0
	jb	.LBB14_38
.LBB14_37:                              # %.sink.split.i.1
                                        #   in Loop: Header=BB14_18 Depth=2
	movsd	%xmm0, (%rax)
	jmp	.LBB14_38
	.p2align	4, 0x90
.LBB14_22:                              #   in Loop: Header=BB14_12 Depth=1
	movl	$.L.str.72, %esi
	movl	$0, %eax
	movq	%r15, %rdi
	leaq	12(%rsp), %rdx
	leaq	8(%rsp), %rcx
	movq	%r12, %r8
	movq	%r13, %r9
	leaq	36(%rsp), %rbx
	pushq	%rbx
.Lcfi172:
	.cfi_adjust_cfa_offset 8
	leaq	48(%rsp), %rbx
	pushq	%rbx
.Lcfi173:
	.cfi_adjust_cfa_offset 8
	leaq	60(%rsp), %rbx
	pushq	%rbx
.Lcfi174:
	.cfi_adjust_cfa_offset 8
	leaq	28(%rsp), %rbx
	pushq	%rbx
.Lcfi175:
	.cfi_adjust_cfa_offset 8
	callq	fscanf
	addq	$32, %rsp
.Lcfi176:
	.cfi_adjust_cfa_offset -32
	xorps	%xmm0, %xmm0
	cvtsi2sdl	12(%rsp), %xmm0
	movq	%rbp, %rax
	shlq	$6, %rax
	movsd	%xmm0, out_pats(%rax)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	8(%rsp), %xmm0
	movsd	%xmm0, out_pats+8(%rax)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	28(%rsp), %xmm0
	movsd	%xmm0, out_pats+16(%rax)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	24(%rsp), %xmm0
	movsd	%xmm0, out_pats+24(%rax)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	4(%rsp), %xmm0
	movsd	%xmm0, out_pats+32(%rax)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	44(%rsp), %xmm0
	movsd	%xmm0, out_pats+40(%rax)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	40(%rsp), %xmm0
	movsd	%xmm0, out_pats+48(%rax)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	36(%rsp), %xmm0
	movsd	%xmm0, out_pats+56(%rax)
	incq	%rbp
	movslq	numpats(%rip), %rax
	addq	$280, %r14              # imm = 0x118
	cmpq	%rax, %rbp
	jl	.LBB14_12
.LBB14_23:                              # %._crit_edge.i
	movq	%r15, %rdi
	callq	fclose
	movb	$1, %bl
	testb	%bl, %bl
	je	.LBB14_25
	jmp	.LBB14_26
.LBB14_15:
	xorl	%ebx, %ebx
	movl	$.L.str.71, %edi
.LBB14_2:                               # %read_data_file.exit
	xorl	%eax, %eax
	callq	printf
	testb	%bl, %bl
	jne	.LBB14_26
.LBB14_25:
	xorl	%eax, %eax
	callq	ErrorExit
.LBB14_26:
	cmpl	$0, global_nnetstruct(%rip)
	jne	.LBB14_30
# BB#27:                                # %.preheader22
	movq	$1, global_nnetstruct+16(%rip)
	.p2align	4, 0x90
.LBB14_28:                              # =>This Inner Loop Header: Depth=1
	movl	$3, %edi
	callq	randnum
	movq	global_nnetstruct+16(%rip), %rdi
	callq	DoNNetIteration
	cmpq	global_min_ticks(%rip), %rax
	ja	.LBB14_30
# BB#29:                                #   in Loop: Header=BB14_28 Depth=1
	movq	global_nnetstruct+16(%rip), %rax
	incq	%rax
	movq	%rax, global_nnetstruct+16(%rip)
	cmpq	$500000, %rax           # imm = 0x7A120
	jb	.LBB14_28
.LBB14_30:                              # %.preheader.preheader
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB14_31:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	$3, %edi
	callq	randnum
	movq	global_nnetstruct+16(%rip), %rdi
	callq	DoNNetIteration
	addq	%rax, %rbx
	movq	global_nnetstruct+16(%rip), %xmm0 # xmm0 = mem[0],zero
	punpckldq	.LCPI14_2(%rip), %xmm0 # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	subpd	.LCPI14_3(%rip), %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm1
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	.LCPI14_4(%rip), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB14_31
# BB#32:
	movq	%rbx, %rdi
	callq	TicksToFracSecs
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, global_nnetstruct+24(%rip)
	cmpl	$0, global_nnetstruct(%rip)
	jne	.LBB14_34
# BB#33:
	movl	$1, global_nnetstruct(%rip)
.LBB14_34:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	DoNNET, .Lfunc_end14-DoNNET
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI15_0:
	.quad	4681608360884174848     # double 1.0E+5
.LCPI15_1:
	.quad	-4620693217682128896    # double -0.5
.LCPI15_2:
	.quad	4602678819172646912     # double 0.5
.LCPI15_3:
	.quad	4666723172467343360     # double 1.0E+4
.LCPI15_5:
	.quad	4607182418800017408     # double 1
.LCPI15_6:
	.quad	-9223372036854775808    # double -0
.LCPI15_7:
	.quad	4593671619917905920     # double 0.125
.LCPI15_8:
	.quad	4591149604126578442     # double 0.089999999999999996
.LCPI15_10:
	.quad	4591870180066957722     # double 0.10000000000000001
.LCPI15_11:
	.quad	4625196817309499392     # double 16
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI15_4:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
.LCPI15_9:
	.quad	4591149604126578442     # double 0.089999999999999996
	.quad	4591149604126578442     # double 0.089999999999999996
	.text
	.p2align	4, 0x90
	.type	DoNNetIteration,@function
DoNNetIteration:                        # @DoNNetIteration
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi177:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi178:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi179:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi180:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi181:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi182:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi183:
	.cfi_def_cfa_offset 80
.Lcfi184:
	.cfi_offset %rbx, -56
.Lcfi185:
	.cfi_offset %r12, -48
.Lcfi186:
	.cfi_offset %r13, -40
.Lcfi187:
	.cfi_offset %r14, -32
.Lcfi188:
	.cfi_offset %r15, -24
.Lcfi189:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	xorl	%eax, %eax
	callq	StartStopwatch
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	testq	%rbx, %rbx
	je	.LBB15_1
	.p2align	4, 0x90
.LBB15_3:                               # %.preheader25
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_4 Depth 2
                                        #     Child Loop BB15_6 Depth 2
                                        #     Child Loop BB15_8 Depth 2
                                        #     Child Loop BB15_10 Depth 2
                                        #     Child Loop BB15_12 Depth 2
                                        #     Child Loop BB15_14 Depth 2
                                        #     Child Loop BB15_16 Depth 2
                                        #     Child Loop BB15_18 Depth 2
                                        #     Child Loop BB15_20 Depth 2
                                        #     Child Loop BB15_22 Depth 2
                                        #       Child Loop BB15_24 Depth 3
                                        #         Child Loop BB15_25 Depth 4
                                        #           Child Loop BB15_26 Depth 5
                                        #         Child Loop BB15_29 Depth 4
                                        #         Child Loop BB15_72 Depth 4
                                        #         Child Loop BB15_74 Depth 4
                                        #         Child Loop BB15_76 Depth 4
                                        #           Child Loop BB15_77 Depth 5
                                        #       Child Loop BB15_85 Depth 3
                                        #       Child Loop BB15_87 Depth 3
                                        #       Child Loop BB15_90 Depth 3
                                        #       Child Loop BB15_92 Depth 3
                                        #       Child Loop BB15_94 Depth 3
                                        #       Child Loop BB15_96 Depth 3
	movq	$-280, %rbx             # imm = 0xFEE8
	.p2align	4, 0x90
.LBB15_4:                               #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$100000, %edi           # imm = 0x186A0
	callq	abs_randwc
	movsd	.LCPI15_0(%rip), %xmm0  # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm2
	movsd	.LCPI15_1(%rip), %xmm0  # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	movl	%eax, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	movsd	.LCPI15_2(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, mid_wts+280(%rbx)
	addq	$8, %rbx
	jne	.LBB15_4
# BB#5:                                 # %.preheader23.134.i.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	movq	$-280, %rbx             # imm = 0xFEE8
	.p2align	4, 0x90
.LBB15_6:                               # %.preheader23.134.i
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$100000, %edi           # imm = 0x186A0
	callq	abs_randwc
	movl	%eax, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI15_0(%rip), %xmm0
	addsd	.LCPI15_1(%rip), %xmm0
	mulsd	.LCPI15_2(%rip), %xmm0
	movsd	%xmm0, mid_wts+560(%rbx)
	addq	$8, %rbx
	jne	.LBB15_6
# BB#7:                                 # %.preheader23.235.i.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	movq	$-280, %rbx             # imm = 0xFEE8
	.p2align	4, 0x90
.LBB15_8:                               # %.preheader23.235.i
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$100000, %edi           # imm = 0x186A0
	callq	abs_randwc
	movl	%eax, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI15_0(%rip), %xmm0
	addsd	.LCPI15_1(%rip), %xmm0
	mulsd	.LCPI15_2(%rip), %xmm0
	movsd	%xmm0, mid_wts+840(%rbx)
	addq	$8, %rbx
	jne	.LBB15_8
# BB#9:                                 # %.preheader23.336.i.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	movq	$-280, %rbx             # imm = 0xFEE8
	.p2align	4, 0x90
.LBB15_10:                              # %.preheader23.336.i
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$100000, %edi           # imm = 0x186A0
	callq	abs_randwc
	movl	%eax, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI15_0(%rip), %xmm0
	addsd	.LCPI15_1(%rip), %xmm0
	mulsd	.LCPI15_2(%rip), %xmm0
	movsd	%xmm0, mid_wts+1120(%rbx)
	addq	$8, %rbx
	jne	.LBB15_10
# BB#11:                                # %.preheader23.437.i.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	movq	$-280, %rbx             # imm = 0xFEE8
	.p2align	4, 0x90
.LBB15_12:                              # %.preheader23.437.i
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$100000, %edi           # imm = 0x186A0
	callq	abs_randwc
	movl	%eax, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI15_0(%rip), %xmm0
	addsd	.LCPI15_1(%rip), %xmm0
	mulsd	.LCPI15_2(%rip), %xmm0
	movsd	%xmm0, mid_wts+1400(%rbx)
	addq	$8, %rbx
	jne	.LBB15_12
# BB#13:                                # %.preheader23.538.i.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	movq	$-280, %rbx             # imm = 0xFEE8
	.p2align	4, 0x90
.LBB15_14:                              # %.preheader23.538.i
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$100000, %edi           # imm = 0x186A0
	callq	abs_randwc
	movsd	.LCPI15_2(%rip), %xmm1  # xmm1 = mem[0],zero
	movl	%eax, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI15_0(%rip), %xmm0
	addsd	.LCPI15_1(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, mid_wts+1680(%rbx)
	addq	$8, %rbx
	jne	.LBB15_14
# BB#15:                                # %.preheader23.639.i.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	movq	$-280, %rbx             # imm = 0xFEE8
	.p2align	4, 0x90
.LBB15_16:                              # %.preheader23.639.i
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$100000, %edi           # imm = 0x186A0
	callq	abs_randwc
	movsd	.LCPI15_2(%rip), %xmm0  # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	movl	%eax, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI15_0(%rip), %xmm0
	addsd	.LCPI15_1(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, mid_wts+1960(%rbx)
	addq	$8, %rbx
	jne	.LBB15_16
# BB#17:                                # %.preheader23.740.i.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	movq	$-280, %rbx             # imm = 0xFEE8
	.p2align	4, 0x90
.LBB15_18:                              # %.preheader23.740.i
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$100000, %edi           # imm = 0x186A0
	callq	abs_randwc
	movsd	.LCPI15_1(%rip), %xmm1  # xmm1 = mem[0],zero
	movl	%eax, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI15_0(%rip), %xmm0
	addsd	%xmm1, %xmm0
	mulsd	.LCPI15_2(%rip), %xmm0
	movsd	%xmm0, mid_wts+2240(%rbx)
	addq	$8, %rbx
	jne	.LBB15_18
# BB#19:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	movq	$-512, %rbx             # imm = 0xFE00
	.p2align	4, 0x90
.LBB15_20:                              # %.preheader.i
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$100000, %edi           # imm = 0x186A0
	callq	abs_randwc
	movl	%eax, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	movsd	.LCPI15_3(%rip), %xmm1  # xmm1 = mem[0],zero
	divsd	%xmm1, %xmm0
	movsd	.LCPI15_1(%rip), %xmm1  # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm0
	mulsd	.LCPI15_2(%rip), %xmm0
	movsd	%xmm0, out_wts+512(%rbx)
	movl	$100000, %edi           # imm = 0x186A0
	callq	abs_randwc
	movl	%eax, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI15_3(%rip), %xmm0
	addsd	.LCPI15_1(%rip), %xmm0
	mulsd	.LCPI15_2(%rip), %xmm0
	movsd	%xmm0, out_wts+520(%rbx)
	movl	$100000, %edi           # imm = 0x186A0
	callq	abs_randwc
	movl	%eax, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI15_3(%rip), %xmm0
	addsd	.LCPI15_1(%rip), %xmm0
	mulsd	.LCPI15_2(%rip), %xmm0
	movsd	%xmm0, out_wts+528(%rbx)
	movl	$100000, %edi           # imm = 0x186A0
	callq	abs_randwc
	movl	%eax, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI15_3(%rip), %xmm0
	addsd	.LCPI15_1(%rip), %xmm0
	mulsd	.LCPI15_2(%rip), %xmm0
	movsd	%xmm0, out_wts+536(%rbx)
	movl	$100000, %edi           # imm = 0x186A0
	callq	abs_randwc
	movl	%eax, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI15_3(%rip), %xmm0
	addsd	.LCPI15_1(%rip), %xmm0
	mulsd	.LCPI15_2(%rip), %xmm0
	movsd	%xmm0, out_wts+544(%rbx)
	movl	$100000, %edi           # imm = 0x186A0
	callq	abs_randwc
	movl	%eax, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI15_3(%rip), %xmm0
	addsd	.LCPI15_1(%rip), %xmm0
	mulsd	.LCPI15_2(%rip), %xmm0
	movsd	%xmm0, out_wts+552(%rbx)
	movl	$100000, %edi           # imm = 0x186A0
	callq	abs_randwc
	movl	%eax, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI15_3(%rip), %xmm0
	addsd	.LCPI15_1(%rip), %xmm0
	mulsd	.LCPI15_2(%rip), %xmm0
	movsd	%xmm0, out_wts+560(%rbx)
	movl	$100000, %edi           # imm = 0x186A0
	callq	abs_randwc
	movl	%eax, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI15_3(%rip), %xmm0
	addsd	.LCPI15_1(%rip), %xmm0
	mulsd	.LCPI15_2(%rip), %xmm0
	movsd	%xmm0, out_wts+568(%rbx)
	addq	$64, %rbx
	jne	.LBB15_20
# BB#21:                                # %randomize_wts.exit
                                        #   in Loop: Header=BB15_3 Depth=1
	movl	$mid_wt_change, %edi
	xorl	%esi, %esi
	movl	$2240, %edx             # imm = 0x8C0
	callq	memset
	movl	$mid_wt_cum_change, %edi
	xorl	%esi, %esi
	movl	$2240, %edx             # imm = 0x8C0
	callq	memset
	movl	$out_wt_change, %edi
	xorl	%esi, %esi
	movl	$512, %edx              # imm = 0x200
	callq	memset
	movl	$out_wt_cum_change, %edi
	xorl	%esi, %esi
	movl	$512, %edx              # imm = 0x200
	callq	memset
	movl	$1, iteration_count(%rip)
	movl	$0, learned(%rip)
	movl	$0, numpasses(%rip)
	movl	numpats(%rip), %eax
	movapd	.LCPI15_4(%rip), %xmm14 # xmm14 = [-0.000000e+00,-0.000000e+00]
	jmp	.LBB15_22
.LBB15_83:                              #   in Loop: Header=BB15_22 Depth=2
	xorpd	%xmm1, %xmm1
	movsd	.LCPI15_11(%rip), %xmm3 # xmm3 = mem[0],zero
	movl	$1, %edx
.LBB15_86:                              # %.prol.loopexit
                                        #   in Loop: Header=BB15_22 Depth=2
	cmpq	$3, %r8
	jb	.LBB15_88
	.p2align	4, 0x90
.LBB15_87:                              #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	tot_out_error(,%rdi,8), %xmm2 # xmm2 = mem[0],zero
	maxsd	%xmm1, %xmm2
	addsd	avg_out_error(,%rdi,8), %xmm0
	movsd	tot_out_error+8(,%rdi,8), %xmm1 # xmm1 = mem[0],zero
	maxsd	%xmm2, %xmm1
	addsd	avg_out_error+8(,%rdi,8), %xmm0
	movsd	tot_out_error+16(,%rdi,8), %xmm2 # xmm2 = mem[0],zero
	maxsd	%xmm1, %xmm2
	addsd	avg_out_error+16(,%rdi,8), %xmm0
	movsd	tot_out_error+24(,%rdi,8), %xmm1 # xmm1 = mem[0],zero
	maxsd	%xmm2, %xmm1
	addsd	avg_out_error+24(,%rdi,8), %xmm0
	addq	$4, %rdi
	cmpq	%rdi, %rcx
	jne	.LBB15_87
.LBB15_88:                              # %.lr.ph.i
                                        #   in Loop: Header=BB15_22 Depth=2
	movsd	%xmm1, worst_error(%rip)
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	divsd	%xmm2, %xmm0
	movsd	%xmm0, average_error(%rip)
	ucomisd	.LCPI15_10(%rip), %xmm1
	jae	.LBB15_89
# BB#93:                                # %.lr.ph.split.us.i.preheader
                                        #   in Loop: Header=BB15_22 Depth=2
	xorl	%ebx, %ebx
	xorl	%edi, %edi
	testb	$3, %cl
	je	.LBB15_95
	.p2align	4, 0x90
.LBB15_94:                              # %.lr.ph.split.us.i.prol
                                        #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	tot_out_error(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	ucomisd	%xmm3, %xmm0
	cmovael	%edx, %edi
	incq	%rbx
	cmpq	%rbx, %rsi
	jne	.LBB15_94
.LBB15_95:                              # %.lr.ph.split.us.i.prol.loopexit
                                        #   in Loop: Header=BB15_22 Depth=2
	movl	$1, %ebp
	cmpq	$3, %r8
	jb	.LBB15_97
	.p2align	4, 0x90
.LBB15_96:                              # %.lr.ph.split.us.i
                                        #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	tot_out_error(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	ucomisd	%xmm3, %xmm0
	movsd	tot_out_error+8(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	movl	$1, %ebp
	cmovael	%ebp, %edi
	ucomisd	%xmm3, %xmm0
	movsd	tot_out_error+16(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	cmovael	%ebp, %edi
	ucomisd	%xmm3, %xmm0
	movsd	tot_out_error+24(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	cmovael	%ebp, %edi
	ucomisd	%xmm3, %xmm0
	cmovael	%ebp, %edi
	addq	$4, %rbx
	cmpq	%rbx, %rcx
	jne	.LBB15_96
	jmp	.LBB15_97
.LBB15_89:                              # %.lr.ph.split.i.preheader
                                        #   in Loop: Header=BB15_22 Depth=2
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	xorl	%edi, %edi
	testb	$3, %cl
	je	.LBB15_91
	.p2align	4, 0x90
.LBB15_90:                              # %.lr.ph.split.i.prol
                                        #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	tot_out_error(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	ucomisd	%xmm3, %xmm0
	cmovael	%edx, %edi
	incq	%rbx
	cmpq	%rbx, %rsi
	jne	.LBB15_90
.LBB15_91:                              # %.lr.ph.split.i.prol.loopexit
                                        #   in Loop: Header=BB15_22 Depth=2
	cmpq	$3, %r8
	jb	.LBB15_97
	.p2align	4, 0x90
.LBB15_92:                              # %.lr.ph.split.i
                                        #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	tot_out_error(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	ucomisd	%xmm3, %xmm0
	movsd	tot_out_error+8(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	cmovael	%edx, %edi
	ucomisd	%xmm3, %xmm0
	movsd	tot_out_error+16(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	cmovael	%edx, %edi
	ucomisd	%xmm3, %xmm0
	movsd	tot_out_error+24(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	cmovael	%edx, %edi
	ucomisd	%xmm3, %xmm0
	cmovael	%edx, %edi
	addq	$4, %rbx
	cmpq	%rbx, %rcx
	jne	.LBB15_92
	jmp	.LBB15_97
	.p2align	4, 0x90
.LBB15_22:                              # %.preheader
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB15_24 Depth 3
                                        #         Child Loop BB15_25 Depth 4
                                        #           Child Loop BB15_26 Depth 5
                                        #         Child Loop BB15_29 Depth 4
                                        #         Child Loop BB15_72 Depth 4
                                        #         Child Loop BB15_74 Depth 4
                                        #         Child Loop BB15_76 Depth 4
                                        #           Child Loop BB15_77 Depth 5
                                        #       Child Loop BB15_85 Depth 3
                                        #       Child Loop BB15_87 Depth 3
                                        #       Child Loop BB15_90 Depth 3
                                        #       Child Loop BB15_92 Depth 3
                                        #       Child Loop BB15_94 Depth 3
                                        #       Child Loop BB15_96 Depth 3
	testl	%eax, %eax
	jle	.LBB15_99
# BB#23:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB15_22 Depth=2
	movl	$in_pats, %r13d
	movl	$in_pats+8, %ebp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB15_24:                              # %.preheader.i13
                                        #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_22 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB15_25 Depth 4
                                        #           Child Loop BB15_26 Depth 5
                                        #         Child Loop BB15_29 Depth 4
                                        #         Child Loop BB15_72 Depth 4
                                        #         Child Loop BB15_74 Depth 4
                                        #         Child Loop BB15_76 Depth 4
                                        #           Child Loop BB15_77 Depth 5
	movq	$0, worst_error(%rip)
	movaps	mid_wt_cum_change(%rip), %xmm0
	movaps	%xmm0, mid_wt_change(%rip)
	xorpd	%xmm1, %xmm1
	movapd	%xmm1, mid_wt_cum_change(%rip)
	movaps	mid_wt_cum_change+16(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+16(%rip)
	movapd	%xmm1, mid_wt_cum_change+16(%rip)
	movaps	mid_wt_cum_change+32(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+32(%rip)
	movapd	%xmm1, mid_wt_cum_change+32(%rip)
	movaps	mid_wt_cum_change+48(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+48(%rip)
	movapd	%xmm1, mid_wt_cum_change+48(%rip)
	movaps	mid_wt_cum_change+64(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+64(%rip)
	movapd	%xmm1, mid_wt_cum_change+64(%rip)
	movaps	mid_wt_cum_change+80(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+80(%rip)
	movapd	%xmm1, mid_wt_cum_change+80(%rip)
	movaps	mid_wt_cum_change+96(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+96(%rip)
	movapd	%xmm1, mid_wt_cum_change+96(%rip)
	movaps	mid_wt_cum_change+112(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+112(%rip)
	movapd	%xmm1, mid_wt_cum_change+112(%rip)
	movaps	mid_wt_cum_change+128(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+128(%rip)
	movapd	%xmm1, mid_wt_cum_change+128(%rip)
	movaps	mid_wt_cum_change+144(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+144(%rip)
	movapd	%xmm1, mid_wt_cum_change+144(%rip)
	movaps	mid_wt_cum_change+160(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+160(%rip)
	movapd	%xmm1, mid_wt_cum_change+160(%rip)
	movaps	mid_wt_cum_change+176(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+176(%rip)
	movapd	%xmm1, mid_wt_cum_change+176(%rip)
	movaps	mid_wt_cum_change+192(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+192(%rip)
	movapd	%xmm1, mid_wt_cum_change+192(%rip)
	movaps	mid_wt_cum_change+208(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+208(%rip)
	movapd	%xmm1, mid_wt_cum_change+208(%rip)
	movaps	mid_wt_cum_change+224(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+224(%rip)
	movapd	%xmm1, mid_wt_cum_change+224(%rip)
	movaps	mid_wt_cum_change+240(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+240(%rip)
	movapd	%xmm1, mid_wt_cum_change+240(%rip)
	movaps	mid_wt_cum_change+256(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+256(%rip)
	movapd	%xmm1, mid_wt_cum_change+256(%rip)
	movaps	mid_wt_cum_change+272(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+272(%rip)
	movapd	%xmm1, mid_wt_cum_change+272(%rip)
	movaps	mid_wt_cum_change+288(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+288(%rip)
	movapd	%xmm1, mid_wt_cum_change+288(%rip)
	movaps	mid_wt_cum_change+304(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+304(%rip)
	movapd	%xmm1, mid_wt_cum_change+304(%rip)
	movaps	mid_wt_cum_change+320(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+320(%rip)
	movapd	%xmm1, mid_wt_cum_change+320(%rip)
	movaps	mid_wt_cum_change+336(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+336(%rip)
	movapd	%xmm1, mid_wt_cum_change+336(%rip)
	movaps	mid_wt_cum_change+352(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+352(%rip)
	movapd	%xmm1, mid_wt_cum_change+352(%rip)
	movaps	mid_wt_cum_change+368(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+368(%rip)
	movapd	%xmm1, mid_wt_cum_change+368(%rip)
	movaps	mid_wt_cum_change+384(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+384(%rip)
	movapd	%xmm1, mid_wt_cum_change+384(%rip)
	movaps	mid_wt_cum_change+400(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+400(%rip)
	movapd	%xmm1, mid_wt_cum_change+400(%rip)
	movaps	mid_wt_cum_change+416(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+416(%rip)
	movapd	%xmm1, mid_wt_cum_change+416(%rip)
	movaps	mid_wt_cum_change+432(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+432(%rip)
	movapd	%xmm1, mid_wt_cum_change+432(%rip)
	movaps	mid_wt_cum_change+448(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+448(%rip)
	movapd	%xmm1, mid_wt_cum_change+448(%rip)
	movaps	mid_wt_cum_change+464(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+464(%rip)
	movapd	%xmm1, mid_wt_cum_change+464(%rip)
	movaps	mid_wt_cum_change+480(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+480(%rip)
	movapd	%xmm1, mid_wt_cum_change+480(%rip)
	movaps	mid_wt_cum_change+496(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+496(%rip)
	movapd	%xmm1, mid_wt_cum_change+496(%rip)
	movaps	mid_wt_cum_change+512(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+512(%rip)
	movapd	%xmm1, mid_wt_cum_change+512(%rip)
	movaps	mid_wt_cum_change+528(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+528(%rip)
	movapd	%xmm1, mid_wt_cum_change+528(%rip)
	movaps	mid_wt_cum_change+544(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+544(%rip)
	movapd	%xmm1, mid_wt_cum_change+544(%rip)
	movaps	mid_wt_cum_change+560(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+560(%rip)
	movapd	%xmm1, mid_wt_cum_change+560(%rip)
	movaps	mid_wt_cum_change+576(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+576(%rip)
	movapd	%xmm1, mid_wt_cum_change+576(%rip)
	movaps	mid_wt_cum_change+592(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+592(%rip)
	movapd	%xmm1, mid_wt_cum_change+592(%rip)
	movaps	mid_wt_cum_change+608(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+608(%rip)
	movapd	%xmm1, mid_wt_cum_change+608(%rip)
	movaps	mid_wt_cum_change+624(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+624(%rip)
	movapd	%xmm1, mid_wt_cum_change+624(%rip)
	movaps	mid_wt_cum_change+640(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+640(%rip)
	movapd	%xmm1, mid_wt_cum_change+640(%rip)
	movaps	mid_wt_cum_change+656(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+656(%rip)
	movapd	%xmm1, mid_wt_cum_change+656(%rip)
	movaps	mid_wt_cum_change+672(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+672(%rip)
	movapd	%xmm1, mid_wt_cum_change+672(%rip)
	movaps	mid_wt_cum_change+688(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+688(%rip)
	movapd	%xmm1, mid_wt_cum_change+688(%rip)
	movaps	mid_wt_cum_change+704(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+704(%rip)
	movapd	%xmm1, mid_wt_cum_change+704(%rip)
	movaps	mid_wt_cum_change+720(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+720(%rip)
	movapd	%xmm1, mid_wt_cum_change+720(%rip)
	movaps	mid_wt_cum_change+736(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+736(%rip)
	movapd	%xmm1, mid_wt_cum_change+736(%rip)
	movaps	mid_wt_cum_change+752(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+752(%rip)
	movapd	%xmm1, mid_wt_cum_change+752(%rip)
	movaps	mid_wt_cum_change+768(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+768(%rip)
	movapd	%xmm1, mid_wt_cum_change+768(%rip)
	movaps	mid_wt_cum_change+784(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+784(%rip)
	movapd	%xmm1, mid_wt_cum_change+784(%rip)
	movaps	mid_wt_cum_change+800(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+800(%rip)
	movapd	%xmm1, mid_wt_cum_change+800(%rip)
	movaps	mid_wt_cum_change+816(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+816(%rip)
	movapd	%xmm1, mid_wt_cum_change+816(%rip)
	movaps	mid_wt_cum_change+832(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+832(%rip)
	movapd	%xmm1, mid_wt_cum_change+832(%rip)
	movaps	mid_wt_cum_change+848(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+848(%rip)
	movapd	%xmm1, mid_wt_cum_change+848(%rip)
	movaps	mid_wt_cum_change+864(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+864(%rip)
	movapd	%xmm1, mid_wt_cum_change+864(%rip)
	movaps	mid_wt_cum_change+880(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+880(%rip)
	movapd	%xmm1, mid_wt_cum_change+880(%rip)
	movaps	mid_wt_cum_change+896(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+896(%rip)
	movapd	%xmm1, mid_wt_cum_change+896(%rip)
	movaps	mid_wt_cum_change+912(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+912(%rip)
	movapd	%xmm1, mid_wt_cum_change+912(%rip)
	movaps	mid_wt_cum_change+928(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+928(%rip)
	movapd	%xmm1, mid_wt_cum_change+928(%rip)
	movaps	mid_wt_cum_change+944(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+944(%rip)
	movapd	%xmm1, mid_wt_cum_change+944(%rip)
	movaps	mid_wt_cum_change+960(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+960(%rip)
	movapd	%xmm1, mid_wt_cum_change+960(%rip)
	movaps	mid_wt_cum_change+976(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+976(%rip)
	movapd	%xmm1, mid_wt_cum_change+976(%rip)
	movaps	mid_wt_cum_change+992(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+992(%rip)
	movapd	%xmm1, mid_wt_cum_change+992(%rip)
	movaps	mid_wt_cum_change+1008(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1008(%rip)
	movapd	%xmm1, mid_wt_cum_change+1008(%rip)
	movaps	mid_wt_cum_change+1024(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1024(%rip)
	movapd	%xmm1, mid_wt_cum_change+1024(%rip)
	movaps	mid_wt_cum_change+1040(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1040(%rip)
	movapd	%xmm1, mid_wt_cum_change+1040(%rip)
	movaps	mid_wt_cum_change+1056(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1056(%rip)
	movapd	%xmm1, mid_wt_cum_change+1056(%rip)
	movaps	mid_wt_cum_change+1072(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1072(%rip)
	movapd	%xmm1, mid_wt_cum_change+1072(%rip)
	movaps	mid_wt_cum_change+1088(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1088(%rip)
	movapd	%xmm1, mid_wt_cum_change+1088(%rip)
	movaps	mid_wt_cum_change+1104(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1104(%rip)
	movapd	%xmm1, mid_wt_cum_change+1104(%rip)
	movaps	mid_wt_cum_change+1120(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1120(%rip)
	movapd	%xmm1, mid_wt_cum_change+1120(%rip)
	movaps	mid_wt_cum_change+1136(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1136(%rip)
	movapd	%xmm1, mid_wt_cum_change+1136(%rip)
	movaps	mid_wt_cum_change+1152(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1152(%rip)
	movapd	%xmm1, mid_wt_cum_change+1152(%rip)
	movaps	mid_wt_cum_change+1168(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1168(%rip)
	movapd	%xmm1, mid_wt_cum_change+1168(%rip)
	movaps	mid_wt_cum_change+1184(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1184(%rip)
	movapd	%xmm1, mid_wt_cum_change+1184(%rip)
	movaps	mid_wt_cum_change+1200(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1200(%rip)
	movapd	%xmm1, mid_wt_cum_change+1200(%rip)
	movaps	mid_wt_cum_change+1216(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1216(%rip)
	movapd	%xmm1, mid_wt_cum_change+1216(%rip)
	movaps	mid_wt_cum_change+1232(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1232(%rip)
	movapd	%xmm1, mid_wt_cum_change+1232(%rip)
	movaps	mid_wt_cum_change+1248(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1248(%rip)
	movapd	%xmm1, mid_wt_cum_change+1248(%rip)
	movaps	mid_wt_cum_change+1264(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1264(%rip)
	movapd	%xmm1, mid_wt_cum_change+1264(%rip)
	movaps	mid_wt_cum_change+1280(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1280(%rip)
	movapd	%xmm1, mid_wt_cum_change+1280(%rip)
	movaps	mid_wt_cum_change+1296(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1296(%rip)
	movapd	%xmm1, mid_wt_cum_change+1296(%rip)
	movaps	mid_wt_cum_change+1312(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1312(%rip)
	movapd	%xmm1, mid_wt_cum_change+1312(%rip)
	movaps	mid_wt_cum_change+1328(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1328(%rip)
	movapd	%xmm1, mid_wt_cum_change+1328(%rip)
	movaps	mid_wt_cum_change+1344(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1344(%rip)
	movapd	%xmm1, mid_wt_cum_change+1344(%rip)
	movaps	mid_wt_cum_change+1360(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1360(%rip)
	movapd	%xmm1, mid_wt_cum_change+1360(%rip)
	movaps	mid_wt_cum_change+1376(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1376(%rip)
	movapd	%xmm1, mid_wt_cum_change+1376(%rip)
	movaps	mid_wt_cum_change+1392(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1392(%rip)
	movapd	%xmm1, mid_wt_cum_change+1392(%rip)
	movaps	mid_wt_cum_change+1408(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1408(%rip)
	movapd	%xmm1, mid_wt_cum_change+1408(%rip)
	movaps	mid_wt_cum_change+1424(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1424(%rip)
	movapd	%xmm1, mid_wt_cum_change+1424(%rip)
	movaps	mid_wt_cum_change+1440(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1440(%rip)
	movapd	%xmm1, mid_wt_cum_change+1440(%rip)
	movaps	mid_wt_cum_change+1456(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1456(%rip)
	movapd	%xmm1, mid_wt_cum_change+1456(%rip)
	movaps	mid_wt_cum_change+1472(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1472(%rip)
	movapd	%xmm1, mid_wt_cum_change+1472(%rip)
	movaps	mid_wt_cum_change+1488(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1488(%rip)
	movapd	%xmm1, mid_wt_cum_change+1488(%rip)
	movaps	mid_wt_cum_change+1504(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1504(%rip)
	movapd	%xmm1, mid_wt_cum_change+1504(%rip)
	movaps	mid_wt_cum_change+1520(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1520(%rip)
	movapd	%xmm1, mid_wt_cum_change+1520(%rip)
	movaps	mid_wt_cum_change+1536(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1536(%rip)
	movapd	%xmm1, mid_wt_cum_change+1536(%rip)
	movaps	mid_wt_cum_change+1552(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1552(%rip)
	movapd	%xmm1, mid_wt_cum_change+1552(%rip)
	movaps	mid_wt_cum_change+1568(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1568(%rip)
	movapd	%xmm1, mid_wt_cum_change+1568(%rip)
	movaps	mid_wt_cum_change+1584(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1584(%rip)
	movapd	%xmm1, mid_wt_cum_change+1584(%rip)
	movaps	mid_wt_cum_change+1600(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1600(%rip)
	movapd	%xmm1, mid_wt_cum_change+1600(%rip)
	movaps	mid_wt_cum_change+1616(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1616(%rip)
	movapd	%xmm1, mid_wt_cum_change+1616(%rip)
	movaps	mid_wt_cum_change+1632(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1632(%rip)
	movapd	%xmm1, mid_wt_cum_change+1632(%rip)
	movaps	mid_wt_cum_change+1648(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1648(%rip)
	movapd	%xmm1, mid_wt_cum_change+1648(%rip)
	movaps	mid_wt_cum_change+1664(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1664(%rip)
	movapd	%xmm1, mid_wt_cum_change+1664(%rip)
	movaps	mid_wt_cum_change+1680(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1680(%rip)
	movapd	%xmm1, mid_wt_cum_change+1680(%rip)
	movaps	mid_wt_cum_change+1696(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1696(%rip)
	movapd	%xmm1, mid_wt_cum_change+1696(%rip)
	movaps	mid_wt_cum_change+1712(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1712(%rip)
	movapd	%xmm1, mid_wt_cum_change+1712(%rip)
	movaps	mid_wt_cum_change+1728(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1728(%rip)
	movapd	%xmm1, mid_wt_cum_change+1728(%rip)
	movaps	mid_wt_cum_change+1744(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1744(%rip)
	movapd	%xmm1, mid_wt_cum_change+1744(%rip)
	movaps	mid_wt_cum_change+1760(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1760(%rip)
	movapd	%xmm1, mid_wt_cum_change+1760(%rip)
	movaps	mid_wt_cum_change+1776(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1776(%rip)
	movapd	%xmm1, mid_wt_cum_change+1776(%rip)
	movaps	mid_wt_cum_change+1792(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1792(%rip)
	movapd	%xmm1, mid_wt_cum_change+1792(%rip)
	movaps	mid_wt_cum_change+1808(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1808(%rip)
	movapd	%xmm1, mid_wt_cum_change+1808(%rip)
	movaps	mid_wt_cum_change+1824(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1824(%rip)
	movapd	%xmm1, mid_wt_cum_change+1824(%rip)
	movaps	mid_wt_cum_change+1840(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1840(%rip)
	movapd	%xmm1, mid_wt_cum_change+1840(%rip)
	movaps	mid_wt_cum_change+1856(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1856(%rip)
	movapd	%xmm1, mid_wt_cum_change+1856(%rip)
	movaps	mid_wt_cum_change+1872(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1872(%rip)
	movapd	%xmm1, mid_wt_cum_change+1872(%rip)
	movaps	mid_wt_cum_change+1888(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1888(%rip)
	movapd	%xmm1, mid_wt_cum_change+1888(%rip)
	movaps	mid_wt_cum_change+1904(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1904(%rip)
	movapd	%xmm1, mid_wt_cum_change+1904(%rip)
	movaps	mid_wt_cum_change+1920(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1920(%rip)
	movapd	%xmm1, mid_wt_cum_change+1920(%rip)
	movaps	mid_wt_cum_change+1936(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1936(%rip)
	movapd	%xmm1, mid_wt_cum_change+1936(%rip)
	movaps	mid_wt_cum_change+1952(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1952(%rip)
	movapd	%xmm1, mid_wt_cum_change+1952(%rip)
	movaps	mid_wt_cum_change+1968(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1968(%rip)
	movapd	%xmm1, mid_wt_cum_change+1968(%rip)
	movaps	mid_wt_cum_change+1984(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+1984(%rip)
	movapd	%xmm1, mid_wt_cum_change+1984(%rip)
	movaps	mid_wt_cum_change+2000(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+2000(%rip)
	movapd	%xmm1, mid_wt_cum_change+2000(%rip)
	movaps	mid_wt_cum_change+2016(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+2016(%rip)
	movapd	%xmm1, mid_wt_cum_change+2016(%rip)
	movaps	mid_wt_cum_change+2032(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+2032(%rip)
	movapd	%xmm1, mid_wt_cum_change+2032(%rip)
	movaps	mid_wt_cum_change+2048(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+2048(%rip)
	movapd	%xmm1, mid_wt_cum_change+2048(%rip)
	movaps	mid_wt_cum_change+2064(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+2064(%rip)
	movapd	%xmm1, mid_wt_cum_change+2064(%rip)
	movaps	mid_wt_cum_change+2080(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+2080(%rip)
	movapd	%xmm1, mid_wt_cum_change+2080(%rip)
	movaps	mid_wt_cum_change+2096(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+2096(%rip)
	movapd	%xmm1, mid_wt_cum_change+2096(%rip)
	movaps	mid_wt_cum_change+2112(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+2112(%rip)
	movapd	%xmm1, mid_wt_cum_change+2112(%rip)
	movaps	mid_wt_cum_change+2128(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+2128(%rip)
	movapd	%xmm1, mid_wt_cum_change+2128(%rip)
	movaps	mid_wt_cum_change+2144(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+2144(%rip)
	movapd	%xmm1, mid_wt_cum_change+2144(%rip)
	movaps	mid_wt_cum_change+2160(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+2160(%rip)
	movapd	%xmm1, mid_wt_cum_change+2160(%rip)
	movaps	mid_wt_cum_change+2176(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+2176(%rip)
	movapd	%xmm1, mid_wt_cum_change+2176(%rip)
	movaps	mid_wt_cum_change+2192(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+2192(%rip)
	movapd	%xmm1, mid_wt_cum_change+2192(%rip)
	movaps	mid_wt_cum_change+2208(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+2208(%rip)
	movapd	%xmm1, mid_wt_cum_change+2208(%rip)
	movaps	mid_wt_cum_change+2224(%rip), %xmm0
	movaps	%xmm0, mid_wt_change+2224(%rip)
	movapd	%xmm1, mid_wt_cum_change+2224(%rip)
	movaps	out_wt_cum_change(%rip), %xmm0
	movaps	%xmm0, out_wt_change(%rip)
	movapd	%xmm1, out_wt_cum_change(%rip)
	movaps	out_wt_cum_change+16(%rip), %xmm0
	movaps	%xmm0, out_wt_change+16(%rip)
	movapd	%xmm1, out_wt_cum_change+16(%rip)
	movaps	out_wt_cum_change+32(%rip), %xmm0
	movaps	%xmm0, out_wt_change+32(%rip)
	movapd	%xmm1, out_wt_cum_change+32(%rip)
	movaps	out_wt_cum_change+48(%rip), %xmm0
	movaps	%xmm0, out_wt_change+48(%rip)
	movapd	%xmm1, out_wt_cum_change+48(%rip)
	movaps	out_wt_cum_change+64(%rip), %xmm0
	movaps	%xmm0, out_wt_change+64(%rip)
	movapd	%xmm1, out_wt_cum_change+64(%rip)
	movaps	out_wt_cum_change+80(%rip), %xmm0
	movaps	%xmm0, out_wt_change+80(%rip)
	movapd	%xmm1, out_wt_cum_change+80(%rip)
	movaps	out_wt_cum_change+96(%rip), %xmm0
	movaps	%xmm0, out_wt_change+96(%rip)
	movapd	%xmm1, out_wt_cum_change+96(%rip)
	movaps	out_wt_cum_change+112(%rip), %xmm0
	movaps	%xmm0, out_wt_change+112(%rip)
	movapd	%xmm1, out_wt_cum_change+112(%rip)
	movaps	out_wt_cum_change+128(%rip), %xmm0
	movaps	%xmm0, out_wt_change+128(%rip)
	movapd	%xmm1, out_wt_cum_change+128(%rip)
	movaps	out_wt_cum_change+144(%rip), %xmm0
	movaps	%xmm0, out_wt_change+144(%rip)
	movapd	%xmm1, out_wt_cum_change+144(%rip)
	movaps	out_wt_cum_change+160(%rip), %xmm0
	movaps	%xmm0, out_wt_change+160(%rip)
	movapd	%xmm1, out_wt_cum_change+160(%rip)
	movaps	out_wt_cum_change+176(%rip), %xmm0
	movaps	%xmm0, out_wt_change+176(%rip)
	movapd	%xmm1, out_wt_cum_change+176(%rip)
	movaps	out_wt_cum_change+192(%rip), %xmm0
	movaps	%xmm0, out_wt_change+192(%rip)
	movapd	%xmm1, out_wt_cum_change+192(%rip)
	movaps	out_wt_cum_change+208(%rip), %xmm0
	movaps	%xmm0, out_wt_change+208(%rip)
	movapd	%xmm1, out_wt_cum_change+208(%rip)
	movaps	out_wt_cum_change+224(%rip), %xmm0
	movaps	%xmm0, out_wt_change+224(%rip)
	movapd	%xmm1, out_wt_cum_change+224(%rip)
	movaps	out_wt_cum_change+240(%rip), %xmm0
	movaps	%xmm0, out_wt_change+240(%rip)
	movapd	%xmm1, out_wt_cum_change+240(%rip)
	movaps	out_wt_cum_change+256(%rip), %xmm0
	movaps	%xmm0, out_wt_change+256(%rip)
	movapd	%xmm1, out_wt_cum_change+256(%rip)
	movaps	out_wt_cum_change+272(%rip), %xmm0
	movaps	%xmm0, out_wt_change+272(%rip)
	movapd	%xmm1, out_wt_cum_change+272(%rip)
	movaps	out_wt_cum_change+288(%rip), %xmm0
	movaps	%xmm0, out_wt_change+288(%rip)
	movapd	%xmm1, out_wt_cum_change+288(%rip)
	movaps	out_wt_cum_change+304(%rip), %xmm0
	movaps	%xmm0, out_wt_change+304(%rip)
	movapd	%xmm1, out_wt_cum_change+304(%rip)
	movaps	out_wt_cum_change+320(%rip), %xmm0
	movaps	%xmm0, out_wt_change+320(%rip)
	movapd	%xmm1, out_wt_cum_change+320(%rip)
	movaps	out_wt_cum_change+336(%rip), %xmm0
	movaps	%xmm0, out_wt_change+336(%rip)
	movapd	%xmm1, out_wt_cum_change+336(%rip)
	movaps	out_wt_cum_change+352(%rip), %xmm0
	movaps	%xmm0, out_wt_change+352(%rip)
	movapd	%xmm1, out_wt_cum_change+352(%rip)
	movaps	out_wt_cum_change+368(%rip), %xmm0
	movaps	%xmm0, out_wt_change+368(%rip)
	movapd	%xmm1, out_wt_cum_change+368(%rip)
	movaps	out_wt_cum_change+384(%rip), %xmm0
	movaps	%xmm0, out_wt_change+384(%rip)
	movapd	%xmm1, out_wt_cum_change+384(%rip)
	movaps	out_wt_cum_change+400(%rip), %xmm0
	movaps	%xmm0, out_wt_change+400(%rip)
	movapd	%xmm1, out_wt_cum_change+400(%rip)
	movaps	out_wt_cum_change+416(%rip), %xmm0
	movaps	%xmm0, out_wt_change+416(%rip)
	movapd	%xmm1, out_wt_cum_change+416(%rip)
	movaps	out_wt_cum_change+432(%rip), %xmm0
	movaps	%xmm0, out_wt_change+432(%rip)
	movapd	%xmm1, out_wt_cum_change+432(%rip)
	movaps	out_wt_cum_change+448(%rip), %xmm0
	movaps	%xmm0, out_wt_change+448(%rip)
	movapd	%xmm1, out_wt_cum_change+448(%rip)
	movaps	out_wt_cum_change+464(%rip), %xmm0
	movaps	%xmm0, out_wt_change+464(%rip)
	movapd	%xmm1, out_wt_cum_change+464(%rip)
	movaps	out_wt_cum_change+480(%rip), %xmm0
	movaps	%xmm0, out_wt_change+480(%rip)
	movapd	%xmm1, out_wt_cum_change+480(%rip)
	movapd	out_wt_cum_change+496(%rip), %xmm0
	movapd	%xmm0, out_wt_change+496(%rip)
	movapd	%xmm1, out_wt_cum_change+496(%rip)
	movl	$mid_wts+8, %r15d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB15_25:                              # %.preheader.i.i
                                        #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_22 Depth=2
                                        #       Parent Loop BB15_24 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB15_26 Depth 5
	xorpd	%xmm1, %xmm1
	movq	%rbp, %rax
	movq	%r15, %rcx
	movl	$34, %edx
	jmp	.LBB15_26
	.p2align	4, 0x90
.LBB15_98:                              #   in Loop: Header=BB15_26 Depth=5
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	mulsd	(%rax), %xmm1
	addsd	%xmm1, %xmm0
	addq	$-2, %rdx
	addq	$16, %rcx
	addq	$16, %rax
	movapd	%xmm0, %xmm1
.LBB15_26:                              #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_22 Depth=2
                                        #       Parent Loop BB15_24 Depth=3
                                        #         Parent Loop BB15_25 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	mulsd	-8(%rax), %xmm0
	addsd	%xmm1, %xmm0
	testq	%rdx, %rdx
	jne	.LBB15_98
# BB#27:                                #   in Loop: Header=BB15_25 Depth=4
	xorpd	%xmm14, %xmm0
	callq	exp
	xorpd	%xmm7, %xmm7
	movsd	.LCPI15_5(%rip), %xmm1  # xmm1 = mem[0],zero
	movapd	.LCPI15_4(%rip), %xmm14 # xmm14 = [-0.000000e+00,-0.000000e+00]
	addsd	%xmm1, %xmm0
	divsd	%xmm0, %xmm1
	movsd	%xmm1, mid_out(,%r12,8)
	incq	%r12
	addq	$280, %r15              # imm = 0x118
	cmpq	$8, %r12
	jne	.LBB15_25
# BB#28:                                # %.preheader.i4.i.preheader
                                        #   in Loop: Header=BB15_24 Depth=3
	movq	$-64, %rbx
	.p2align	4, 0x90
.LBB15_29:                              # %.preheader.i4.i
                                        #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_22 Depth=2
                                        #       Parent Loop BB15_24 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	out_wts+512(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	mulsd	mid_out(%rip), %xmm0
	addsd	%xmm7, %xmm0
	movsd	out_wts+520(,%rbx,8), %xmm1 # xmm1 = mem[0],zero
	mulsd	mid_out+8(%rip), %xmm1
	addsd	%xmm0, %xmm1
	movsd	out_wts+528(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	mulsd	mid_out+16(%rip), %xmm0
	addsd	%xmm1, %xmm0
	movsd	out_wts+536(,%rbx,8), %xmm1 # xmm1 = mem[0],zero
	mulsd	mid_out+24(%rip), %xmm1
	addsd	%xmm0, %xmm1
	movsd	out_wts+544(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	mulsd	mid_out+32(%rip), %xmm0
	addsd	%xmm1, %xmm0
	movsd	out_wts+552(,%rbx,8), %xmm1 # xmm1 = mem[0],zero
	mulsd	mid_out+40(%rip), %xmm1
	addsd	%xmm0, %xmm1
	movsd	out_wts+560(,%rbx,8), %xmm2 # xmm2 = mem[0],zero
	mulsd	mid_out+48(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movsd	out_wts+568(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	mulsd	mid_out+56(%rip), %xmm0
	addsd	%xmm2, %xmm0
	xorpd	%xmm14, %xmm0
	callq	exp
	xorpd	%xmm7, %xmm7
	movsd	.LCPI15_5(%rip), %xmm6  # xmm6 = mem[0],zero
	movapd	.LCPI15_4(%rip), %xmm14 # xmm14 = [-0.000000e+00,-0.000000e+00]
	addsd	%xmm6, %xmm0
	movapd	%xmm6, %xmm1
	divsd	%xmm0, %xmm1
	movsd	%xmm1, out_out+64(%rbx)
	addq	$8, %rbx
	jne	.LBB15_29
# BB#30:                                # %do_forward_pass.exit
                                        #   in Loop: Header=BB15_24 Depth=3
	movq	%r14, %rax
	shlq	$6, %rax
	movsd	out_pats(%rax), %xmm0   # xmm0 = mem[0],zero
	subsd	out_out(%rip), %xmm0
	movsd	%xmm0, out_error(%rip)
	xorpd	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm7
	jbe	.LBB15_33
# BB#31:                                #   in Loop: Header=BB15_24 Depth=3
	xorpd	%xmm9, %xmm9
	subsd	%xmm0, %xmm9
	movsd	.LCPI15_6(%rip), %xmm2  # xmm2 = mem[0],zero
	ucomisd	%xmm0, %xmm2
	jbe	.LBB15_35
# BB#32:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm0, %xmm1
	xorpd	%xmm14, %xmm1
	jmp	.LBB15_35
	.p2align	4, 0x90
.LBB15_33:                              #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm0, %xmm9
	addsd	%xmm1, %xmm9
	ucomisd	%xmm1, %xmm0
	jbe	.LBB15_35
# BB#34:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm0, %xmm1
.LBB15_35:                              #   in Loop: Header=BB15_24 Depth=3
	movsd	out_pats+8(%rax), %xmm11 # xmm11 = mem[0],zero
	subsd	out_out+8(%rip), %xmm11
	movsd	%xmm11, out_error+8(%rip)
	ucomisd	%xmm11, %xmm7
	jbe	.LBB15_36
# BB#38:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm11, %xmm2
	xorpd	%xmm14, %xmm2
	subsd	%xmm11, %xmm9
	ucomisd	%xmm1, %xmm2
	jbe	.LBB15_40
# BB#39:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm2, %xmm1
	jmp	.LBB15_40
	.p2align	4, 0x90
.LBB15_36:                              #   in Loop: Header=BB15_24 Depth=3
	addsd	%xmm11, %xmm9
	ucomisd	%xmm1, %xmm11
	jbe	.LBB15_40
# BB#37:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm11, %xmm1
.LBB15_40:                              #   in Loop: Header=BB15_24 Depth=3
	movsd	out_pats+16(%rax), %xmm12 # xmm12 = mem[0],zero
	subsd	out_out+16(%rip), %xmm12
	movsd	%xmm12, out_error+16(%rip)
	ucomisd	%xmm12, %xmm7
	jbe	.LBB15_41
# BB#43:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm12, %xmm2
	xorpd	%xmm14, %xmm2
	subsd	%xmm12, %xmm9
	ucomisd	%xmm1, %xmm2
	jbe	.LBB15_45
# BB#44:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm2, %xmm1
	jmp	.LBB15_45
	.p2align	4, 0x90
.LBB15_41:                              #   in Loop: Header=BB15_24 Depth=3
	addsd	%xmm12, %xmm9
	ucomisd	%xmm1, %xmm12
	jbe	.LBB15_45
# BB#42:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm12, %xmm1
.LBB15_45:                              #   in Loop: Header=BB15_24 Depth=3
	movsd	out_pats+24(%rax), %xmm13 # xmm13 = mem[0],zero
	subsd	out_out+24(%rip), %xmm13
	movsd	%xmm13, out_error+24(%rip)
	ucomisd	%xmm13, %xmm7
	jbe	.LBB15_46
# BB#48:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm13, %xmm2
	xorpd	%xmm14, %xmm2
	subsd	%xmm13, %xmm9
	ucomisd	%xmm1, %xmm2
	jbe	.LBB15_50
# BB#49:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm2, %xmm1
	jmp	.LBB15_50
	.p2align	4, 0x90
.LBB15_46:                              #   in Loop: Header=BB15_24 Depth=3
	addsd	%xmm13, %xmm9
	ucomisd	%xmm1, %xmm13
	jbe	.LBB15_50
# BB#47:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm13, %xmm1
.LBB15_50:                              #   in Loop: Header=BB15_24 Depth=3
	movsd	out_pats+32(%rax), %xmm15 # xmm15 = mem[0],zero
	subsd	out_out+32(%rip), %xmm15
	movsd	%xmm15, out_error+32(%rip)
	ucomisd	%xmm15, %xmm7
	jbe	.LBB15_51
# BB#53:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm15, %xmm2
	xorpd	%xmm14, %xmm2
	subsd	%xmm15, %xmm9
	ucomisd	%xmm1, %xmm2
	jbe	.LBB15_55
# BB#54:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm2, %xmm1
	jmp	.LBB15_55
	.p2align	4, 0x90
.LBB15_51:                              #   in Loop: Header=BB15_24 Depth=3
	addsd	%xmm15, %xmm9
	ucomisd	%xmm1, %xmm15
	jbe	.LBB15_55
# BB#52:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm15, %xmm1
.LBB15_55:                              #   in Loop: Header=BB15_24 Depth=3
	movsd	out_pats+40(%rax), %xmm8 # xmm8 = mem[0],zero
	subsd	out_out+40(%rip), %xmm8
	movsd	%xmm8, out_error+40(%rip)
	ucomisd	%xmm8, %xmm7
	jbe	.LBB15_56
# BB#58:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm8, %xmm2
	xorpd	%xmm14, %xmm2
	subsd	%xmm8, %xmm9
	ucomisd	%xmm1, %xmm2
	jbe	.LBB15_60
# BB#59:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm2, %xmm1
	jmp	.LBB15_60
	.p2align	4, 0x90
.LBB15_56:                              #   in Loop: Header=BB15_24 Depth=3
	addsd	%xmm8, %xmm9
	ucomisd	%xmm1, %xmm8
	jbe	.LBB15_60
# BB#57:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm8, %xmm1
.LBB15_60:                              #   in Loop: Header=BB15_24 Depth=3
	movsd	out_pats+48(%rax), %xmm4 # xmm4 = mem[0],zero
	subsd	out_out+48(%rip), %xmm4
	movsd	%xmm4, out_error+48(%rip)
	ucomisd	%xmm4, %xmm7
	jbe	.LBB15_61
# BB#63:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm4, %xmm2
	xorpd	%xmm14, %xmm2
	subsd	%xmm4, %xmm9
	ucomisd	%xmm1, %xmm2
	jbe	.LBB15_65
# BB#64:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm2, %xmm1
	jmp	.LBB15_65
	.p2align	4, 0x90
.LBB15_61:                              #   in Loop: Header=BB15_24 Depth=3
	addsd	%xmm4, %xmm9
	ucomisd	%xmm1, %xmm4
	jbe	.LBB15_65
# BB#62:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm4, %xmm1
.LBB15_65:                              #   in Loop: Header=BB15_24 Depth=3
	movsd	out_pats+56(%rax), %xmm2 # xmm2 = mem[0],zero
	subsd	out_out+56(%rip), %xmm2
	movsd	%xmm2, out_error+56(%rip)
	ucomisd	%xmm2, %xmm7
	jbe	.LBB15_66
# BB#68:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm2, %xmm10
	xorpd	%xmm14, %xmm10
	subsd	%xmm2, %xmm9
	ucomisd	%xmm1, %xmm10
	jbe	.LBB15_70
# BB#69:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm10, %xmm1
.LBB15_70:                              # %do_out_error.exit.i
                                        #   in Loop: Header=BB15_24 Depth=3
	xorpd	%xmm7, %xmm7
	jmp	.LBB15_71
	.p2align	4, 0x90
.LBB15_66:                              #   in Loop: Header=BB15_24 Depth=3
	addsd	%xmm2, %xmm9
	ucomisd	%xmm1, %xmm2
	jbe	.LBB15_71
# BB#67:                                #   in Loop: Header=BB15_24 Depth=3
	movapd	%xmm2, %xmm1
.LBB15_71:                              # %do_out_error.exit.i
                                        #   in Loop: Header=BB15_24 Depth=3
	mulsd	.LCPI15_7(%rip), %xmm9
	movsd	%xmm9, avg_out_error(,%r14,8)
	movsd	%xmm1, tot_out_error(,%r14,8)
	movq	$-64, %rax
	.p2align	4, 0x90
.LBB15_72:                              # %.preheader.i10.i
                                        #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_22 Depth=2
                                        #       Parent Loop BB15_24 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	out_wts+64(%rax), %xmm1 # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	%xmm7, %xmm1
	movsd	out_wts+128(%rax), %xmm3 # xmm3 = mem[0],zero
	mulsd	%xmm11, %xmm3
	addsd	%xmm1, %xmm3
	movsd	out_wts+192(%rax), %xmm1 # xmm1 = mem[0],zero
	mulsd	%xmm12, %xmm1
	addsd	%xmm3, %xmm1
	movsd	out_wts+256(%rax), %xmm3 # xmm3 = mem[0],zero
	mulsd	%xmm13, %xmm3
	addsd	%xmm1, %xmm3
	movsd	out_wts+320(%rax), %xmm1 # xmm1 = mem[0],zero
	mulsd	%xmm15, %xmm1
	addsd	%xmm3, %xmm1
	movsd	out_wts+384(%rax), %xmm3 # xmm3 = mem[0],zero
	mulsd	%xmm8, %xmm3
	addsd	%xmm1, %xmm3
	movsd	out_wts+448(%rax), %xmm1 # xmm1 = mem[0],zero
	mulsd	%xmm4, %xmm1
	addsd	%xmm3, %xmm1
	movsd	out_wts+512(%rax), %xmm3 # xmm3 = mem[0],zero
	mulsd	%xmm2, %xmm3
	addsd	%xmm1, %xmm3
	movsd	mid_out+64(%rax), %xmm1 # xmm1 = mem[0],zero
	movapd	%xmm6, %xmm5
	subsd	%xmm1, %xmm5
	mulsd	%xmm1, %xmm5
	mulsd	%xmm3, %xmm5
	movsd	%xmm5, mid_error+64(%rax)
	addq	$8, %rax
	jne	.LBB15_72
# BB#73:                                # %do_mid_error.exit.i
                                        #   in Loop: Header=BB15_24 Depth=3
	movapd	mid_out+16(%rip), %xmm8
	movapd	mid_out+32(%rip), %xmm2
	movapd	mid_out(%rip), %xmm3
	movq	$-56, %rax
	movapd	mid_out+48(%rip), %xmm4
	movsd	.LCPI15_8(%rip), %xmm7  # xmm7 = mem[0],zero
	jmp	.LBB15_74
	.p2align	4, 0x90
.LBB15_100:                             # %.preheader.i5..preheader.i5_crit_edge.i
                                        #   in Loop: Header=BB15_74 Depth=4
	movsd	out_error+64(%rax), %xmm0 # xmm0 = mem[0],zero
	addq	$8, %rax
.LBB15_74:                              # %.preheader.i5.i
                                        #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_22 Depth=2
                                        #       Parent Loop BB15_24 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	mulsd	%xmm7, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movapd	%xmm3, %xmm5
	mulpd	%xmm0, %xmm5
	movapd	out_wt_change+448(,%rax,8), %xmm6
	movapd	.LCPI15_9(%rip), %xmm1  # xmm1 = [9.000000e-02,9.000000e-02]
	mulpd	%xmm1, %xmm6
	addpd	%xmm5, %xmm6
	movapd	out_wts+448(,%rax,8), %xmm5
	addpd	%xmm6, %xmm5
	movapd	%xmm5, out_wts+448(,%rax,8)
	addpd	out_wt_cum_change+448(,%rax,8), %xmm6
	movapd	%xmm6, out_wt_cum_change+448(,%rax,8)
	movapd	%xmm8, %xmm5
	mulpd	%xmm0, %xmm5
	movapd	out_wt_change+464(,%rax,8), %xmm6
	mulpd	%xmm1, %xmm6
	addpd	%xmm5, %xmm6
	movapd	out_wts+464(,%rax,8), %xmm5
	addpd	%xmm6, %xmm5
	movapd	%xmm5, out_wts+464(,%rax,8)
	addpd	out_wt_cum_change+464(,%rax,8), %xmm6
	movapd	%xmm6, out_wt_cum_change+464(,%rax,8)
	movapd	%xmm2, %xmm5
	mulpd	%xmm0, %xmm5
	movapd	out_wt_change+480(,%rax,8), %xmm6
	mulpd	%xmm1, %xmm6
	addpd	%xmm5, %xmm6
	movapd	out_wts+480(,%rax,8), %xmm5
	addpd	%xmm6, %xmm5
	movapd	%xmm5, out_wts+480(,%rax,8)
	addpd	out_wt_cum_change+480(,%rax,8), %xmm6
	movapd	%xmm6, out_wt_cum_change+480(,%rax,8)
	mulpd	%xmm4, %xmm0
	movapd	out_wt_change+496(,%rax,8), %xmm5
	mulpd	%xmm1, %xmm5
	addpd	%xmm0, %xmm5
	movapd	out_wts+496(,%rax,8), %xmm0
	addpd	%xmm5, %xmm0
	movapd	%xmm0, out_wts+496(,%rax,8)
	addpd	out_wt_cum_change+496(,%rax,8), %xmm5
	movapd	%xmm5, out_wt_cum_change+496(,%rax,8)
	testq	%rax, %rax
	jne	.LBB15_100
# BB#75:                                # %.preheader.i.i14.preheader
                                        #   in Loop: Header=BB15_24 Depth=3
	imulq	$280, %r14, %rax        # imm = 0x118
	movsd	in_pats+272(%rax), %xmm0 # xmm0 = mem[0],zero
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	movapd	.LCPI15_9(%rip), %xmm5  # xmm5 = [9.000000e-02,9.000000e-02]
	.p2align	4, 0x90
.LBB15_76:                              # %.preheader.i.i14
                                        #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_22 Depth=2
                                        #       Parent Loop BB15_24 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB15_77 Depth 5
	movsd	mid_error(,%rcx,8), %xmm1 # xmm1 = mem[0],zero
	mulsd	%xmm7, %xmm1
	movapd	%xmm1, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	movl	$34, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	.p2align	4, 0x90
.LBB15_77:                              # %vector.body
                                        #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_22 Depth=2
                                        #       Parent Loop BB15_24 Depth=3
                                        #         Parent Loop BB15_76 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movupd	(%rdi), %xmm3
	mulpd	%xmm2, %xmm3
	movupd	mid_wt_change(%rsi), %xmm4
	mulpd	%xmm5, %xmm4
	addpd	%xmm3, %xmm4
	movupd	mid_wts(%rsi), %xmm3
	addpd	%xmm4, %xmm3
	movupd	%xmm3, mid_wts(%rsi)
	movupd	mid_wt_cum_change(%rsi), %xmm3
	addpd	%xmm4, %xmm3
	movupd	%xmm3, mid_wt_cum_change(%rsi)
	addq	$16, %rdi
	addq	$16, %rsi
	addq	$-2, %rdx
	jne	.LBB15_77
# BB#78:                                # %scalar.ph
                                        #   in Loop: Header=BB15_76 Depth=4
	mulsd	%xmm0, %xmm1
	imulq	$280, %rcx, %rdx        # imm = 0x118
	movsd	mid_wt_change+272(%rdx), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm7, %xmm2
	addsd	%xmm1, %xmm2
	movsd	mid_wts+272(%rdx), %xmm1 # xmm1 = mem[0],zero
	addsd	%xmm2, %xmm1
	movsd	%xmm1, mid_wts+272(%rdx)
	addsd	mid_wt_cum_change+272(%rdx), %xmm2
	movsd	%xmm2, mid_wt_cum_change+272(%rdx)
	incq	%rcx
	addq	$280, %rax              # imm = 0x118
	cmpq	$8, %rcx
	jne	.LBB15_76
# BB#79:                                # %do_back_pass.exit
                                        #   in Loop: Header=BB15_24 Depth=3
	incl	iteration_count(%rip)
	incq	%r14
	movl	numpats(%rip), %eax
	movslq	%eax, %rcx
	addq	$280, %rbp              # imm = 0x118
	addq	$280, %r13              # imm = 0x118
	cmpq	%rcx, %r14
	jl	.LBB15_24
# BB#80:                                # %._crit_edge
                                        #   in Loop: Header=BB15_22 Depth=2
	incl	numpasses(%rip)
	testl	%eax, %eax
	jle	.LBB15_81
# BB#82:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB15_22 Depth=2
	leaq	-1(%rcx), %r8
	movq	%rcx, %rsi
	xorpd	%xmm0, %xmm0
	xorl	%edi, %edi
	andq	$3, %rsi
	je	.LBB15_83
# BB#84:                                # %.prol.preheader
                                        #   in Loop: Header=BB15_22 Depth=2
	xorpd	%xmm2, %xmm2
	movsd	.LCPI15_11(%rip), %xmm3 # xmm3 = mem[0],zero
	movl	$1, %edx
	.p2align	4, 0x90
.LBB15_85:                              #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	tot_out_error(,%rdi,8), %xmm1 # xmm1 = mem[0],zero
	maxsd	%xmm2, %xmm1
	addsd	avg_out_error(,%rdi,8), %xmm0
	incq	%rdi
	cmpq	%rdi, %rsi
	movapd	%xmm1, %xmm2
	jne	.LBB15_85
	jmp	.LBB15_86
	.p2align	4, 0x90
.LBB15_99:                              # %._crit_edge.thread
                                        #   in Loop: Header=BB15_22 Depth=2
	incl	numpasses(%rip)
.LBB15_81:                              # %worst_pass_error.exit.thread.i
                                        #   in Loop: Header=BB15_22 Depth=2
	movq	$0, worst_error(%rip)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	xorpd	%xmm1, %xmm1
	divsd	%xmm0, %xmm1
	movsd	%xmm1, average_error(%rip)
	xorl	%edi, %edi
	movl	$1, %ebp
.LBB15_97:                              # %check_out_error.exit
                                        #   in Loop: Header=BB15_22 Depth=2
	cmpl	$1, %edi
	movl	$-1, %ecx
	cmovel	%ecx, %ebp
	movl	%ebp, learned(%rip)
	testl	%ebp, %ebp
	je	.LBB15_22
# BB#2:                                 # %.loopexit
                                        #   in Loop: Header=BB15_3 Depth=1
	decq	16(%rsp)                # 8-byte Folded Spill
	jne	.LBB15_3
.LBB15_1:                               # %._crit_edge28
	movq	8(%rsp), %rdi           # 8-byte Reload
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	StopStopwatch           # TAILCALL
.Lfunc_end15:
	.size	DoNNetIteration, .Lfunc_end15-DoNNetIteration
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI16_0:
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
.LCPI16_1:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI16_2:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI16_3:
	.quad	4647503709213818880     # double 500
	.text
	.globl	DoLU
	.p2align	4, 0x90
	.type	DoLU,@function
DoLU:                                   # @DoLU
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi190:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi191:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi192:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi193:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi194:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi195:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi196:
	.cfi_def_cfa_offset 80
.Lcfi197:
	.cfi_offset %rbx, -56
.Lcfi198:
	.cfi_offset %r12, -48
.Lcfi199:
	.cfi_offset %r13, -40
.Lcfi200:
	.cfi_offset %r14, -32
.Lcfi201:
	.cfi_offset %r15, -24
.Lcfi202:
	.cfi_offset %rbp, -16
	leaq	4(%rsp), %rbx
	movl	$81608, %edi            # imm = 0x13EC8
	movq	%rbx, %rsi
	callq	AllocateMemory
	movq	%rax, %rbp
	movl	$808, %edi              # imm = 0x328
	movq	%rbx, %rsi
	callq	AllocateMemory
	movq	%rax, %r13
	movl	$808, %edi              # imm = 0x328
	movq	%rbx, %rsi
	callq	AllocateMemory
	movq	%rax, LUtempvv(%rip)
	movl	$13, %edi
	callq	randnum
	xorl	%r15d, %r15d
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %r14
	.p2align	4, 0x90
.LBB16_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_2 Depth 2
	movl	$100, %edi
	callq	abs_randwc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	incl	%eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	movsd	%xmm0, (%r13,%r15,8)
	movq	%r14, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_2:                               #   Parent Loop BB16_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorpd	%xmm0, %xmm0
	cmpq	%rbx, %r15
	jne	.LBB16_4
# BB#3:                                 #   in Loop: Header=BB16_2 Depth=2
	movl	$1000, %edi             # imm = 0x3E8
	callq	abs_randwc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	incl	%eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.LBB16_4:                               #   in Loop: Header=BB16_2 Depth=2
	movsd	%xmm0, (%rbp)
	incq	%rbx
	addq	$8, %rbp
	cmpq	$101, %rbx
	jne	.LBB16_2
# BB#5:                                 #   in Loop: Header=BB16_1 Depth=1
	incq	%r15
	addq	$808, %r14              # imm = 0x328
	cmpq	$101, %r15
	jne	.LBB16_1
# BB#6:                                 # %.preheader.i.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	808(%rax), %r15
	leaq	16(%rax), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB16_7:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_12 Depth 2
                                        #     Child Loop BB16_16 Depth 2
	movl	$101, %edi
	callq	abs_randwc
	movl	%eax, %ebx
	movl	$101, %edi
	callq	abs_randwc
	cmpl	%eax, %ebx
	je	.LBB16_18
# BB#8:                                 # %min.iters.checked
                                        #   in Loop: Header=BB16_7 Depth=1
	movl	%ebx, %ecx
	movl	%eax, %r8d
	cmpl	%r8d, %ecx
	sbbq	%rax, %rax
	andl	$8, %eax
	movsd	.LCPI16_0(%rax), %xmm0  # xmm0 = mem[0],zero
	imulq	$808, %rcx, %rdx        # imm = 0x328
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%rax,%rdx), %rdi
	imulq	$808, %r8, %rsi         # imm = 0x328
	leaq	(%rax,%rsi), %rbx
	leaq	(%r15,%rsi), %rax
	cmpq	%rax, %rdi
	jae	.LBB16_11
# BB#9:                                 # %min.iters.checked
                                        #   in Loop: Header=BB16_7 Depth=1
	leaq	(%r15,%rdx), %rax
	cmpq	%rax, %rbx
	jae	.LBB16_11
# BB#10:                                #   in Loop: Header=BB16_7 Depth=1
	xorl	%eax, %eax
	jmp	.LBB16_14
	.p2align	4, 0x90
.LBB16_11:                              # %vector.ph
                                        #   in Loop: Header=BB16_7 Depth=1
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$2, %eax
	.p2align	4, 0x90
.LBB16_12:                              # %vector.body
                                        #   Parent Loop BB16_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-16(%rbx,%rax,8), %xmm2
	mulpd	%xmm1, %xmm2
	movupd	-16(%rdi,%rax,8), %xmm3
	addpd	%xmm2, %xmm3
	movupd	%xmm3, -16(%rdi,%rax,8)
	movupd	(%rbx,%rax,8), %xmm2
	mulpd	%xmm1, %xmm2
	movupd	(%rdi,%rax,8), %xmm3
	addpd	%xmm2, %xmm3
	movupd	%xmm3, (%rdi,%rax,8)
	addq	$4, %rax
	cmpq	$102, %rax
	jne	.LBB16_12
# BB#13:                                #   in Loop: Header=BB16_7 Depth=1
	movl	$100, %eax
.LBB16_14:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB16_7 Depth=1
	movsd	(%rbx,%rax,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	(%rdi,%rax,8), %xmm1
	movsd	%xmm1, (%rdi,%rax,8)
	cmpq	$100, %rax
	je	.LBB16_17
# BB#15:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB16_7 Depth=1
	addq	$-100, %rax
	addq	%r14, %rdx
	addq	%r14, %rsi
	.p2align	4, 0x90
.LBB16_16:                              # %scalar.ph
                                        #   Parent Loop BB16_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	792(%rsi,%rax,8), %xmm1 # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	792(%rdx,%rax,8), %xmm1
	movsd	%xmm1, 792(%rdx,%rax,8)
	movsd	800(%rsi,%rax,8), %xmm1 # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	800(%rdx,%rax,8), %xmm1
	movsd	%xmm1, 800(%rdx,%rax,8)
	addq	$2, %rax
	jne	.LBB16_16
.LBB16_17:                              # %.loopexit
                                        #   in Loop: Header=BB16_7 Depth=1
	mulsd	(%r13,%r8,8), %xmm0
	addsd	(%r13,%rcx,8), %xmm0
	movsd	%xmm0, (%r13,%rcx,8)
.LBB16_18:                              #   in Loop: Header=BB16_7 Depth=1
	incq	%rbp
	cmpq	$808, %rbp              # imm = 0x328
	jne	.LBB16_7
# BB#19:                                # %build_problem.exit
	cmpl	$0, global_lustruct(%rip)
	je	.LBB16_20
# BB#36:
	imulq	$81608, global_lustruct+16(%rip), %rdi # imm = 0x13EC8
	leaq	4(%rsp), %rsi
	callq	AllocateMemory
	movq	%rax, %r15
	movl	4(%rsp), %esi
	testl	%esi, %esi
	movq	8(%rsp), %r14           # 8-byte Reload
	je	.LBB16_38
# BB#37:
	movl	$.L.str.61, %edi
	callq	ReportError
	movq	%rsp, %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	movq	LUtempvv(%rip), %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	xorl	%eax, %eax
	callq	ErrorExit
.LBB16_38:
	imulq	$808, global_lustruct+16(%rip), %rdi # imm = 0x328
	leaq	4(%rsp), %rsi
	callq	AllocateMemory
	movq	%rax, %rbp
	movl	4(%rsp), %esi
	testl	%esi, %esi
	je	.LBB16_43
# BB#39:
	movl	$.L.str.61, %edi
	callq	ReportError
	movq	%rsp, %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	movq	LUtempvv(%rip), %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	testq	%r15, %r15
	je	.LBB16_42
# BB#40:
	movq	%rsp, %rsi
	movq	%r15, %rdi
.LBB16_41:                              # %LUFreeMem.exit82
	callq	FreeMemory
.LBB16_42:                              # %LUFreeMem.exit82
	xorl	%eax, %eax
	callq	ErrorExit
	jmp	.LBB16_43
.LBB16_20:
	movq	$0, global_lustruct+16(%rip)
	movl	$1, %ebx
	movl	$163216, %r12d          # imm = 0x27D90
	movl	$1616, %r14d            # imm = 0x650
	leaq	4(%rsp), %r15
	.p2align	4, 0x90
.LBB16_21:                              # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	AllocateMemory
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	4(%rsp), %esi
	testl	%esi, %esi
	movq	%r15, %rbp
	movq	%rsp, %r15
	je	.LBB16_23
# BB#22:                                #   in Loop: Header=BB16_21 Depth=1
	movl	$.L.str.61, %edi
	callq	ReportError
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r15, %rsi
	callq	FreeMemory
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	FreeMemory
	movq	LUtempvv(%rip), %rdi
	movq	%r15, %rsi
	callq	FreeMemory
	xorl	%eax, %eax
	callq	ErrorExit
.LBB16_23:                              #   in Loop: Header=BB16_21 Depth=1
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	AllocateMemory
	movq	%rax, %rbp
	movl	4(%rsp), %esi
	testl	%esi, %esi
	je	.LBB16_27
# BB#24:                                #   in Loop: Header=BB16_21 Depth=1
	movl	$.L.str.61, %edi
	callq	ReportError
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r15, %rsi
	callq	FreeMemory
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	FreeMemory
	movq	LUtempvv(%rip), %rdi
	movq	%r15, %rsi
	callq	FreeMemory
	movq	16(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB16_26
# BB#25:                                #   in Loop: Header=BB16_21 Depth=1
	movq	%r15, %rsi
	callq	FreeMemory
.LBB16_26:                              # %LUFreeMem.exit
                                        #   in Loop: Header=BB16_21 Depth=1
	xorl	%eax, %eax
	callq	ErrorExit
.LBB16_27:                              #   in Loop: Header=BB16_21 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r13, %rsi
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdx
	movq	%rbp, %rcx
	movq	%rbx, %r8
	callq	DoLUIteration
	cmpq	global_min_ticks(%rip), %rax
	ja	.LBB16_28
# BB#29:                                #   in Loop: Header=BB16_21 Depth=1
	incq	%rbx
	movq	%r15, %rdi
	leaq	4(%rsp), %r15
	movq	%r15, %rsi
	callq	FreeMemory
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	FreeMemory
	addq	$81608, %r12            # imm = 0x13EC8
	addq	$808, %r14              # imm = 0x328
	cmpq	$10001, %rbx            # imm = 0x2711
	jl	.LBB16_21
# BB#30:
	cmpq	$0, global_lustruct+16(%rip)
	je	.LBB16_32
# BB#31:
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	16(%rsp), %r15          # 8-byte Reload
	jmp	.LBB16_43
.LBB16_28:                              # %.thread
	movq	%rbx, global_lustruct+16(%rip)
	movq	8(%rsp), %r14           # 8-byte Reload
.LBB16_43:
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movq	global_lustruct+16(%rip), %r8
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_44:                              # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%rbp, %rcx
	callq	DoLUIteration
	addq	%rax, %rbx
	movq	global_lustruct+16(%rip), %r8
	movd	%r8, %xmm0
	punpckldq	.LCPI16_1(%rip), %xmm0 # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	subpd	.LCPI16_2(%rip), %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm1
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	.LCPI16_3(%rip), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB16_44
# BB#45:
	movq	%rbx, %rdi
	callq	TicksToFracSecs
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, global_lustruct+24(%rip)
	cmpl	$0, global_lustruct(%rip)
	jne	.LBB16_47
# BB#46:
	movl	$1, global_lustruct(%rip)
.LBB16_47:
	movq	%rsp, %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	movq	LUtempvv(%rip), %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	testq	%r15, %r15
	je	.LBB16_49
# BB#48:
	movq	%rsp, %rsi
	movq	%r15, %rdi
	callq	FreeMemory
.LBB16_49:
	testq	%rbp, %rbp
	je	.LBB16_51
# BB#50:
	movq	%rsp, %rsi
	movq	%rbp, %rdi
	callq	FreeMemory
.LBB16_51:                              # %LUFreeMem.exit83
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB16_32:
	movl	$.Lstr.2, %edi
	callq	puts
	movq	%rsp, %rbx
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	movq	LUtempvv(%rip), %rdi
	movq	%rbx, %rsi
	callq	FreeMemory
	movq	16(%rsp), %r15          # 8-byte Reload
	testq	%r15, %r15
	je	.LBB16_34
# BB#33:
	movq	%rsp, %rsi
	movq	%r15, %rdi
	callq	FreeMemory
.LBB16_34:
	testq	%rbp, %rbp
	je	.LBB16_42
# BB#35:
	movq	%rsp, %rsi
	movq	%rbp, %rdi
	jmp	.LBB16_41
.Lfunc_end16:
	.size	DoLU, .Lfunc_end16-DoLU
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI17_0:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI17_1:
	.quad	4607182418800017408     # double 1
.LCPI17_2:
	.quad	4307583784117748259     # double 9.9999999999999995E-21
	.text
	.p2align	4, 0x90
	.type	DoLUIteration,@function
DoLUIteration:                          # @DoLUIteration
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi203:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi204:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi205:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi206:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi207:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi208:
	.cfi_def_cfa_offset 56
	subq	$600, %rsp              # imm = 0x258
.Lcfi209:
	.cfi_def_cfa_offset 656
.Lcfi210:
	.cfi_offset %rbx, -56
.Lcfi211:
	.cfi_offset %r12, -48
.Lcfi212:
	.cfi_offset %r13, -40
.Lcfi213:
	.cfi_offset %r14, -32
.Lcfi214:
	.cfi_offset %r15, -24
.Lcfi215:
	.cfi_offset %rbp, -16
	movq	%r8, 56(%rsp)           # 8-byte Spill
	testq	%r8, %r8
	je	.LBB17_114
# BB#1:                                 # %.lr.ph50.preheader
	leaq	808(%rsi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	81608(%rdi), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	144(%rdx), %r8
	leaq	64(%rcx), %r14
	movq	%rcx, %r10
	xorl	%r12d, %r12d
	movq	%r10, 48(%rsp)          # 8-byte Spill
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB17_2:                               # %.lr.ph50
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_6 Depth 2
                                        #     Child Loop BB17_9 Depth 2
                                        #     Child Loop BB17_12 Depth 2
                                        #     Child Loop BB17_17 Depth 2
                                        #     Child Loop BB17_20 Depth 2
                                        #     Child Loop BB17_23 Depth 2
	imulq	$81608, %r12, %rdx      # imm = 0x13EC8
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx), %rbx
	imulq	$808, %r12, %rbp        # imm = 0x328
	cmpq	40(%rsp), %rbx          # 8-byte Folded Reload
	jae	.LBB17_5
# BB#3:                                 # %.lr.ph50
                                        #   in Loop: Header=BB17_2 Depth=1
	leaq	81608(%rax,%rdx), %rdx
	cmpq	%rdi, %rdx
	jbe	.LBB17_5
# BB#4:                                 #   in Loop: Header=BB17_2 Depth=1
	movq	%r8, %r11
	xorl	%edx, %edx
	jmp	.LBB17_8
	.p2align	4, 0x90
.LBB17_5:                               # %vector.body83.preheader
                                        #   in Loop: Header=BB17_2 Depth=1
	movq	%r8, %rdx
	movl	$18, %ebx
	.p2align	4, 0x90
.LBB17_6:                               # %vector.body83
                                        #   Parent Loop BB17_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-144(%rdi,%rbx,8), %xmm0
	movups	-128(%rdi,%rbx,8), %xmm1
	movups	%xmm0, -144(%rdx)
	movups	%xmm1, -128(%rdx)
	movups	-112(%rdi,%rbx,8), %xmm0
	movups	-96(%rdi,%rbx,8), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rdi,%rbx,8), %xmm0
	movups	-64(%rdi,%rbx,8), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rdi,%rbx,8), %xmm0
	movups	-32(%rdi,%rbx,8), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movupd	-16(%rdi,%rbx,8), %xmm0
	movups	(%rdi,%rbx,8), %xmm1
	movupd	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	addq	$20, %rbx
	addq	$160, %rdx
	cmpq	$10218, %rbx            # imm = 0x27EA
	jne	.LBB17_6
# BB#7:                                 #   in Loop: Header=BB17_2 Depth=1
	movq	%r8, %r11
	movl	$10200, %edx            # imm = 0x27D8
.LBB17_8:                               # %scalar.ph85.prol.preheader
                                        #   in Loop: Header=BB17_2 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbp), %r15
	leaq	808(%rax,%rbp), %rax
	movl	$10200, %r13d           # imm = 0x27D8
	subq	%rdx, %r13
	leaq	(%rcx,%rdx,8), %rbx
	leaq	(%rdi,%rdx,8), %r8
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB17_9:                               # %scalar.ph85.prol
                                        #   Parent Loop BB17_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r8,%rbp,8), %r9
	movq	%r9, (%rbx,%rbp,8)
	incq	%rbp
	cmpq	$1, %rbp
	jne	.LBB17_9
# BB#10:                                # %scalar.ph85.prol.loopexit
                                        #   in Loop: Header=BB17_2 Depth=1
	cmpq	$7, %r13
	jb	.LBB17_13
# BB#11:                                # %scalar.ph85.preheader.new
                                        #   in Loop: Header=BB17_2 Depth=1
	addq	%rbp, %rdx
	addq	$7, %rdx
	.p2align	4, 0x90
.LBB17_12:                              # %scalar.ph85
                                        #   Parent Loop BB17_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-56(%rdi,%rdx,8), %rbp
	movq	%rbp, -56(%rcx,%rdx,8)
	movq	-48(%rdi,%rdx,8), %rbp
	movq	%rbp, -48(%rcx,%rdx,8)
	movq	-40(%rdi,%rdx,8), %rbp
	movq	%rbp, -40(%rcx,%rdx,8)
	movq	-32(%rdi,%rdx,8), %rbp
	movq	%rbp, -32(%rcx,%rdx,8)
	movq	-24(%rdi,%rdx,8), %rbp
	movq	%rbp, -24(%rcx,%rdx,8)
	movq	-16(%rdi,%rdx,8), %rbp
	movq	%rbp, -16(%rcx,%rdx,8)
	movq	-8(%rdi,%rdx,8), %rbp
	movq	%rbp, -8(%rcx,%rdx,8)
	movq	(%rdi,%rdx,8), %rbp
	movq	%rbp, (%rcx,%rdx,8)
	addq	$8, %rdx
	cmpq	$10208, %rdx            # imm = 0x27E0
	jne	.LBB17_12
.LBB17_13:                              # %.preheader.preheader
                                        #   in Loop: Header=BB17_2 Depth=1
	cmpq	8(%rsp), %r15           # 8-byte Folded Reload
	movq	%r11, %r8
	jae	.LBB17_16
# BB#14:                                # %.preheader.preheader
                                        #   in Loop: Header=BB17_2 Depth=1
	cmpq	%rsi, %rax
	jbe	.LBB17_16
# BB#15:                                #   in Loop: Header=BB17_2 Depth=1
	xorl	%edx, %edx
	jmp	.LBB17_19
	.p2align	4, 0x90
.LBB17_16:                              # %vector.body.preheader
                                        #   in Loop: Header=BB17_2 Depth=1
	movq	%r14, %rdx
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB17_17:                              # %vector.body
                                        #   Parent Loop BB17_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-64(%rsi,%rbp,8), %xmm0
	movups	%xmm0, -64(%rdx)
	movups	-48(%rsi,%rbp,8), %xmm0
	movups	%xmm0, -48(%rdx)
	movups	-32(%rsi,%rbp,8), %xmm0
	movups	%xmm0, -32(%rdx)
	movups	-16(%rsi,%rbp,8), %xmm0
	movups	%xmm0, -16(%rdx)
	movupd	(%rsi,%rbp,8), %xmm0
	movupd	%xmm0, (%rdx)
	addq	$10, %rbp
	addq	$80, %rdx
	cmpq	$108, %rbp
	jne	.LBB17_17
# BB#18:                                #   in Loop: Header=BB17_2 Depth=1
	movl	$100, %edx
.LBB17_19:                              # %.preheader.prol.preheader
                                        #   in Loop: Header=BB17_2 Depth=1
	movl	$100, %r11d
	subq	%rdx, %r11
	movl	$101, %ebx
	subl	%edx, %ebx
	andl	$5, %ebx
	leaq	(%r10,%rdx,8), %rbp
	negq	%rbx
	.p2align	4, 0x90
.LBB17_20:                              # %.preheader.prol
                                        #   Parent Loop BB17_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi,%rdx,8), %rax
	movq	%rax, (%rbp)
	incq	%rdx
	addq	$8, %rbp
	incq	%rbx
	jne	.LBB17_20
# BB#21:                                # %.preheader.prol.loopexit
                                        #   in Loop: Header=BB17_2 Depth=1
	cmpq	$7, %r11
	jb	.LBB17_24
# BB#22:                                # %.preheader.preheader144.new
                                        #   in Loop: Header=BB17_2 Depth=1
	addq	$7, %rdx
	.p2align	4, 0x90
.LBB17_23:                              # %.preheader
                                        #   Parent Loop BB17_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-56(%rsi,%rdx,8), %rax
	movq	%rax, -56(%r10,%rdx,8)
	movq	-48(%rsi,%rdx,8), %rax
	movq	%rax, -48(%r10,%rdx,8)
	movq	-40(%rsi,%rdx,8), %rax
	movq	%rax, -40(%r10,%rdx,8)
	movq	-32(%rsi,%rdx,8), %rax
	movq	%rax, -32(%r10,%rdx,8)
	movq	-24(%rsi,%rdx,8), %rax
	movq	%rax, -24(%r10,%rdx,8)
	movq	-16(%rsi,%rdx,8), %rax
	movq	%rax, -16(%r10,%rdx,8)
	movq	-8(%rsi,%rdx,8), %rax
	movq	%rax, -8(%r10,%rdx,8)
	movq	(%rsi,%rdx,8), %rax
	movq	%rax, (%r10,%rdx,8)
	addq	$8, %rdx
	cmpq	$108, %rdx
	jne	.LBB17_23
.LBB17_24:                              # %.loopexit140
                                        #   in Loop: Header=BB17_2 Depth=1
	incq	%r12
	addq	$81608, %r8             # imm = 0x13EC8
	addq	$81608, %rcx            # imm = 0x13EC8
	addq	$808, %r14              # imm = 0x328
	addq	$808, %r10              # imm = 0x328
	cmpq	56(%rsp), %r12          # 8-byte Folded Reload
	jne	.LBB17_2
# BB#25:                                # %._crit_edge51
	xorl	%eax, %eax
	callq	StartStopwatch
	movq	%rax, %rdi
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB17_112
# BB#26:                                # %.lr.ph
	movq	%rdi, 136(%rsp)         # 8-byte Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	24(%rax), %rcx
	leaq	8(%rax), %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	leaq	16(%rax), %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	leaq	808(%rax), %rdx
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movq	48(%rsp), %r12          # 8-byte Reload
	leaq	808(%r12), %rdx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	leaq	81608(%rax), %rdx
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	leaq	80824(%rax), %rdx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	movapd	.LCPI17_0(%rip), %xmm0  # xmm0 = [nan,nan]
	xorps	%xmm1, %xmm1
	movsd	.LCPI17_1(%rip), %xmm2  # xmm2 = mem[0],zero
	movsd	.LCPI17_2(%rip), %xmm8  # xmm8 = mem[0],zero
	movapd	.LCPI17_0(%rip), %xmm4  # xmm4 = [nan,nan]
	movq	%r12, %r15
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB17_27:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_28 Depth 2
                                        #       Child Loop BB17_29 Depth 3
                                        #     Child Loop BB17_33 Depth 2
                                        #       Child Loop BB17_39 Depth 3
                                        #         Child Loop BB17_65 Depth 4
                                        #       Child Loop BB17_49 Depth 3
                                        #       Child Loop BB17_55 Depth 3
                                        #         Child Loop BB17_60 Depth 4
                                        #       Child Loop BB17_35 Depth 3
                                        #       Child Loop BB17_68 Depth 3
                                        #       Child Loop BB17_72 Depth 3
                                        #       Child Loop BB17_80 Depth 3
                                        #       Child Loop BB17_84 Depth 3
                                        #     Child Loop BB17_87 Depth 2
                                        #       Child Loop BB17_91 Depth 3
                                        #       Child Loop BB17_93 Depth 3
                                        #     Child Loop BB17_99 Depth 2
                                        #       Child Loop BB17_104 Depth 3
                                        #       Child Loop BB17_108 Depth 3
	imulq	$10201, %rdx, %rsi      # imm = 0x27D9
	leaq	(%rax,%rsi,8), %r14
	leaq	101(%rsi), %rdi
	movq	%rdi, 176(%rsp)         # 8-byte Spill
	movq	%rsi, 168(%rsp)         # 8-byte Spill
	leaq	808(%rax,%rsi,8), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	imulq	$808, %rdx, %rbp        # imm = 0x328
	addq	%r12, %rbp
	movq	LUtempvv(%rip), %rax
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB17_28:                              # %.preheader9.i.i
                                        #   Parent Loop BB17_27 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_29 Depth 3
	xorpd	%xmm5, %xmm5
	movl	$100, %esi
	movq	%rcx, %rdi
	jmp	.LBB17_29
	.p2align	4, 0x90
.LBB17_113:                             #   in Loop: Header=BB17_29 Depth=3
	movsd	-16(%rdi), %xmm3        # xmm3 = mem[0],zero
	movsd	-8(%rdi), %xmm7         # xmm7 = mem[0],zero
	andpd	%xmm4, %xmm3
	maxsd	%xmm6, %xmm3
	andpd	%xmm4, %xmm7
	maxsd	%xmm3, %xmm7
	movsd	(%rdi), %xmm5           # xmm5 = mem[0],zero
	andpd	%xmm4, %xmm5
	maxsd	%xmm7, %xmm5
	addq	$32, %rdi
	addq	$-4, %rsi
.LBB17_29:                              #   Parent Loop BB17_27 Depth=1
                                        #     Parent Loop BB17_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-24(%rdi), %xmm6        # xmm6 = mem[0],zero
	andpd	%xmm0, %xmm6
	maxsd	%xmm5, %xmm6
	testq	%rsi, %rsi
	jne	.LBB17_113
# BB#30:                                #   in Loop: Header=BB17_28 Depth=2
	ucomisd	%xmm1, %xmm6
	jne	.LBB17_31
	jnp	.LBB17_110
.LBB17_31:                              #   in Loop: Header=BB17_28 Depth=2
	movapd	%xmm2, %xmm5
	divsd	%xmm6, %xmm5
	movsd	%xmm5, (%rax,%rdx,8)
	incq	%rdx
	addq	$808, %rcx              # imm = 0x328
	cmpq	$101, %rdx
	jl	.LBB17_28
# BB#32:                                # %.preheader7.i.i
                                        #   in Loop: Header=BB17_27 Depth=1
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	LUtempvv(%rip), %r10
	movq	112(%rsp), %rbx         # 8-byte Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movl	$0, %edx
	movq	16(%rsp), %r9           # 8-byte Reload
	xorl	%edi, %edi
	movl	$1, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%r11d, %r11d
	movq	%rbp, 184(%rsp)         # 8-byte Spill
	jmp	.LBB17_33
	.p2align	4, 0x90
.LBB17_115:                             # %.backedge.i.backedge.i
                                        #   in Loop: Header=BB17_33 Depth=2
	incq	72(%rsp)                # 8-byte Folded Spill
	addq	$8, %r9
	movl	84(%rsp), %edx          # 4-byte Reload
	incl	%edx
	addq	$808, 120(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x328
	addq	$808, 128(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x328
	addq	$816, %rbx              # imm = 0x330
	addb	$3, %r12b
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movq	%rcx, %rdi
.LBB17_33:                              # %.backedge.i.i
                                        #   Parent Loop BB17_27 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_39 Depth 3
                                        #         Child Loop BB17_65 Depth 4
                                        #       Child Loop BB17_49 Depth 3
                                        #       Child Loop BB17_55 Depth 3
                                        #         Child Loop BB17_60 Depth 4
                                        #       Child Loop BB17_35 Depth 3
                                        #       Child Loop BB17_68 Depth 3
                                        #       Child Loop BB17_72 Depth 3
                                        #       Child Loop BB17_80 Depth 3
                                        #       Child Loop BB17_84 Depth 3
	movslq	%edx, %rax
	imulq	$808, %rax, %r12        # imm = 0x328
	addq	32(%rsp), %r12          # 8-byte Folded Reload
	movslq	%edi, %r13
	movl	$101, %r8d
	subq	%r13, %r8
	testq	%rdi, %rdi
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movl	%edx, 84(%rsp)          # 4-byte Spill
	je	.LBB17_34
# BB#37:                                # %.preheader6.i.i
                                        #   in Loop: Header=BB17_33 Depth=2
	jle	.LBB17_42
# BB#38:                                # %.lr.ph14.i.i.preheader
                                        #   in Loop: Header=BB17_33 Depth=2
	movq	32(%rsp), %rsi          # 8-byte Reload
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB17_39:                              # %.lr.ph14.i.i
                                        #   Parent Loop BB17_27 Depth=1
                                        #     Parent Loop BB17_33 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB17_65 Depth 4
	imulq	$808, %rcx, %rdx        # imm = 0x328
	leaq	(%r14,%rdx), %rbx
	leaq	(%rbx,%rdi,8), %rax
	movsd	(%rbx,%rdi,8), %xmm5    # xmm5 = mem[0],zero
	testq	%rcx, %rcx
	jle	.LBB17_66
# BB#40:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB17_39 Depth=3
	testb	$1, %cl
	jne	.LBB17_62
# BB#41:                                #   in Loop: Header=BB17_39 Depth=3
	xorl	%ebp, %ebp
	cmpq	$1, %rcx
	jne	.LBB17_64
	jmp	.LBB17_66
	.p2align	4, 0x90
.LBB17_62:                              # %.lr.ph.i.i.prol
                                        #   in Loop: Header=BB17_39 Depth=3
	movsd	(%r14,%rdx), %xmm3      # xmm3 = mem[0],zero
	mulsd	(%r14,%rdi,8), %xmm3
	subsd	%xmm3, %xmm5
	movl	$1, %ebp
	cmpq	$1, %rcx
	je	.LBB17_66
.LBB17_64:                              # %.lr.ph.i.i.preheader.new
                                        #   in Loop: Header=BB17_39 Depth=3
	imulq	$808, %rbp, %rdx        # imm = 0x328
	addq	%r9, %rdx
	leaq	(%rsi,%rbp,8), %rbx
	.p2align	4, 0x90
.LBB17_65:                              # %.lr.ph.i.i
                                        #   Parent Loop BB17_27 Depth=1
                                        #     Parent Loop BB17_33 Depth=2
                                        #       Parent Loop BB17_39 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	-8(%rbx), %xmm3         # xmm3 = mem[0],zero
	movsd	(%rbx), %xmm6           # xmm6 = mem[0],zero
	mulsd	(%rdx), %xmm3
	subsd	%xmm3, %xmm5
	mulsd	808(%rdx), %xmm6
	subsd	%xmm6, %xmm5
	addq	$2, %rbp
	addq	$1616, %rdx             # imm = 0x650
	addq	$16, %rbx
	cmpq	%rbp, %rcx
	jne	.LBB17_65
.LBB17_66:                              # %.loopexit2.i.i
                                        #   in Loop: Header=BB17_39 Depth=3
	movsd	%xmm5, (%rax)
	incq	%rcx
	addq	$808, %rsi              # imm = 0x328
	cmpq	%rdi, %rcx
	jne	.LBB17_39
.LBB17_42:                              # %.preheader5.i.i
                                        #   in Loop: Header=BB17_33 Depth=2
	cmpl	$100, %edi
	jg	.LBB17_43
# BB#44:                                # %.lr.ph22.split.us.i.i
                                        #   in Loop: Header=BB17_33 Depth=2
	movq	%r13, %rcx
	testq	%rdi, %rdi
	movslq	%edi, %r13
	jle	.LBB17_45
# BB#54:                                # %.lr.ph22.split.us.split.us.i.i.preheader
                                        #   in Loop: Header=BB17_33 Depth=2
	movl	%edi, %ecx
	andl	$1, %ecx
	xorpd	%xmm5, %xmm5
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB17_55:                              # %.lr.ph22.split.us.split.us.i.i
                                        #   Parent Loop BB17_27 Depth=1
                                        #     Parent Loop BB17_33 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB17_60 Depth 4
	imulq	$808, %r13, %rax        # imm = 0x328
	leaq	(%r14,%rax), %rdx
	testq	%rcx, %rcx
	movsd	(%rdx,%rdi,8), %xmm6    # xmm6 = mem[0],zero
	jne	.LBB17_57
# BB#56:                                #   in Loop: Header=BB17_55 Depth=3
	xorl	%eax, %eax
	jmp	.LBB17_58
	.p2align	4, 0x90
.LBB17_57:                              #   in Loop: Header=BB17_55 Depth=3
	movsd	(%r14,%rax), %xmm7      # xmm7 = mem[0],zero
	mulsd	(%r14,%rdi,8), %xmm7
	subsd	%xmm7, %xmm6
	movl	$1, %eax
.LBB17_58:                              # %.prol.loopexit
                                        #   in Loop: Header=BB17_55 Depth=3
	leaq	(%rdx,%rdi,8), %rsi
	cmpq	$1, %rdi
	je	.LBB17_61
# BB#59:                                # %.lr.ph22.split.us.split.us.i.i.new
                                        #   in Loop: Header=BB17_55 Depth=3
	imulq	$808, %rax, %rbp        # imm = 0x328
	addq	%r9, %rbp
	leaq	(%r12,%rax,8), %rdx
	.p2align	4, 0x90
.LBB17_60:                              #   Parent Loop BB17_27 Depth=1
                                        #     Parent Loop BB17_33 Depth=2
                                        #       Parent Loop BB17_55 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	-8(%rdx), %xmm3         # xmm3 = mem[0],zero
	movsd	(%rdx), %xmm7           # xmm7 = mem[0],zero
	mulsd	(%rbp), %xmm3
	subsd	%xmm3, %xmm6
	mulsd	808(%rbp), %xmm7
	subsd	%xmm7, %xmm6
	addq	$2, %rax
	addq	$1616, %rbp             # imm = 0x650
	addq	$16, %rdx
	cmpq	%rax, %rdi
	jne	.LBB17_60
.LBB17_61:                              # %..loopexit_crit_edge.us.us.i.i
                                        #   in Loop: Header=BB17_55 Depth=3
	movsd	%xmm6, (%rsi)
	andpd	%xmm0, %xmm6
	mulsd	(%r10,%r13,8), %xmm6
	ucomisd	%xmm5, %xmm6
	movapd	%xmm5, %xmm7
	cmpnlesd	%xmm6, %xmm7
	movapd	%xmm7, %xmm3
	andnpd	%xmm6, %xmm3
	andpd	%xmm5, %xmm7
	orpd	%xmm3, %xmm7
	cmovael	%r13d, %r11d
	incq	%r13
	addq	$808, %r12              # imm = 0x328
	cmpq	$101, %r13
	movapd	%xmm7, %xmm5
	jne	.LBB17_55
	jmp	.LBB17_50
	.p2align	4, 0x90
.LBB17_34:                              # %.lr.ph22.split.i.i.preheader
                                        #   in Loop: Header=BB17_33 Depth=2
	xorpd	%xmm5, %xmm5
	movq	16(%rsp), %rax          # 8-byte Reload
	xorl	%ecx, %ecx
	jmp	.LBB17_35
	.p2align	4, 0x90
.LBB17_36:                              # %.lr.ph22.split.i.i.1
                                        #   in Loop: Header=BB17_35 Depth=3
	movapd	%xmm5, %xmm3
	cmpnlesd	%xmm6, %xmm3
	movapd	%xmm3, %xmm7
	andnpd	%xmm6, %xmm7
	andpd	%xmm5, %xmm3
	orpd	%xmm7, %xmm3
	movsd	808(%rax), %xmm6        # xmm6 = mem[0],zero
	andpd	%xmm0, %xmm6
	mulsd	8(%r10,%rcx,8), %xmm6
	ucomisd	%xmm3, %xmm6
	movapd	%xmm3, %xmm5
	cmpnlesd	%xmm6, %xmm5
	movapd	%xmm5, %xmm7
	andnpd	%xmm6, %xmm7
	andpd	%xmm3, %xmm5
	orpd	%xmm7, %xmm5
	cmovael	%edx, %r11d
	addq	$1616, %rax             # imm = 0x650
	incq	%rdx
	movq	%rdx, %rcx
.LBB17_35:                              # %.lr.ph22.split.i.i
                                        #   Parent Loop BB17_27 Depth=1
                                        #     Parent Loop BB17_33 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rax), %xmm6           # xmm6 = mem[0],zero
	andpd	%xmm0, %xmm6
	mulsd	(%r10,%rcx,8), %xmm6
	ucomisd	%xmm5, %xmm6
	cmovael	%ecx, %r11d
	leaq	1(%rcx), %rdx
	cmpq	$101, %rdx
	jne	.LBB17_36
	jmp	.LBB17_50
	.p2align	4, 0x90
.LBB17_43:                              #   in Loop: Header=BB17_33 Depth=2
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB17_50:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB17_33 Depth=2
	movl	%r11d, %eax
	cmpq	%rax, %rdi
	movq	184(%rsp), %r13         # 8-byte Reload
	movq	40(%rsp), %r12          # 8-byte Reload
	je	.LBB17_74
# BB#51:                                # %.preheader4.i.i
                                        #   in Loop: Header=BB17_33 Depth=2
	imulq	$101, %rdi, %rsi
	movq	176(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rbp
	movslq	%r11d, %r8
	imulq	$808, %r8, %rdx         # imm = 0x328
	leaq	(%r14,%rdx), %rax
	cmpq	%rbp, %rax
	jae	.LBB17_67
# BB#52:                                # %.preheader4.i.i
                                        #   in Loop: Header=BB17_33 Depth=2
	addq	168(%rsp), %rsi         # 8-byte Folded Reload
	leaq	(%rcx,%rsi,8), %rsi
	movq	160(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx), %rbp
	cmpq	%rbp, %rsi
	jae	.LBB17_67
# BB#53:                                #   in Loop: Header=BB17_33 Depth=2
	xorl	%esi, %esi
	jmp	.LBB17_70
	.p2align	4, 0x90
.LBB17_67:                              # %vector.body110.preheader
                                        #   in Loop: Header=BB17_33 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx), %rsi
	movq	120(%rsp), %rbp         # 8-byte Reload
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB17_68:                              # %vector.body110
                                        #   Parent Loop BB17_27 Depth=1
                                        #     Parent Loop BB17_33 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	(%rsi,%rbx,8), %xmm3
	movups	-16(%rbp), %xmm5
	movups	%xmm5, (%rsi,%rbx,8)
	movups	%xmm3, -16(%rbp)
	movupd	16(%rsi,%rbx,8), %xmm3
	movupd	(%rbp), %xmm5
	movupd	%xmm5, 16(%rsi,%rbx,8)
	movupd	%xmm3, (%rbp)
	addq	$4, %rbx
	addq	$32, %rbp
	cmpq	$100, %rbx
	jne	.LBB17_68
# BB#69:                                #   in Loop: Header=BB17_33 Depth=2
	movl	$100, %esi
.LBB17_70:                              # %scalar.ph112.prol.loopexit
                                        #   in Loop: Header=BB17_33 Depth=2
	movq	(%rax,%rsi,8), %rbp
	imulq	$808, %rdi, %rbx        # imm = 0x328
	addq	%r14, %rbx
	movq	(%rbx,%rsi,8), %rcx
	movq	%rcx, (%rax,%rsi,8)
	movq	%rbp, (%rbx,%rsi,8)
	cmpq	$100, %rsi
	je	.LBB17_73
# BB#71:                                # %scalar.ph112.preheader.new
                                        #   in Loop: Header=BB17_33 Depth=2
	movq	128(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	addq	64(%rsp), %rdx          # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB17_72:                              # %scalar.ph112
                                        #   Parent Loop BB17_27 Depth=1
                                        #     Parent Loop BB17_33 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-8(%rdx,%rsi,8), %rcx
	movq	(%rax), %rbx
	movq	%rbx, -8(%rdx,%rsi,8)
	movq	%rcx, (%rax)
	movq	(%rdx,%rsi,8), %rcx
	movq	8(%rax), %rbx
	movq	%rbx, (%rdx,%rsi,8)
	movq	%rcx, 8(%rax)
	addq	$2, %rsi
	addq	$16, %rax
	cmpq	$100, %rsi
	jne	.LBB17_72
.LBB17_73:                              # %.loopexit
                                        #   in Loop: Header=BB17_33 Depth=2
	movq	(%r10,%r8,8), %rax
	movq	(%r10,%rdi,8), %rcx
	movq	%rcx, (%r10,%r8,8)
	movq	%rax, (%r10,%rdi,8)
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB17_74:                              #   in Loop: Header=BB17_33 Depth=2
	movl	%r11d, 192(%rsp,%rdi,4)
	imulq	$808, %rdi, %rax        # imm = 0x328
	addq	%r14, %rax
	movsd	(%rax,%rdi,8), %xmm6    # xmm6 = mem[0],zero
	ucomisd	%xmm1, %xmm6
	jne	.LBB17_76
	jp	.LBB17_76
# BB#75:                                #   in Loop: Header=BB17_33 Depth=2
	leaq	(%rax,%rdi,8), %rax
	movabsq	$4307583784117748259, %rcx # imm = 0x3BC79CA10C924223
	movq	%rcx, (%rax)
	movapd	%xmm8, %xmm6
.LBB17_76:                              #   in Loop: Header=BB17_33 Depth=2
	cmpq	$100, %rdi
	je	.LBB17_86
# BB#77:                                #   in Loop: Header=BB17_33 Depth=2
	leaq	1(%rdi), %rcx
	cmpq	$100, %rcx
	jg	.LBB17_115
# BB#78:                                # %.lr.ph28.i.i.preheader
                                        #   in Loop: Header=BB17_33 Depth=2
	movl	$99, %eax
	subq	%rdi, %rax
	movapd	%xmm2, %xmm5
	divsd	%xmm6, %xmm5
	negl	%edi
	testb	$3, %dil
	movq	72(%rsp), %rdx          # 8-byte Reload
	je	.LBB17_82
# BB#79:                                # %.lr.ph28.i.i.prol.preheader
                                        #   in Loop: Header=BB17_33 Depth=2
	movl	%r12d, %edx
	andb	$3, %dl
	movzbl	%dl, %edx
	negq	%rdx
	movq	%rbx, %rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB17_80:                              # %.lr.ph28.i.i.prol
                                        #   Parent Loop BB17_27 Depth=1
                                        #     Parent Loop BB17_33 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rdi), %xmm3           # xmm3 = mem[0],zero
	mulsd	%xmm5, %xmm3
	movsd	%xmm3, (%rdi)
	decq	%rsi
	addq	$808, %rdi              # imm = 0x328
	cmpq	%rsi, %rdx
	jne	.LBB17_80
# BB#81:                                # %.lr.ph28.i.i.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB17_33 Depth=2
	movq	72(%rsp), %rdx          # 8-byte Reload
	subq	%rsi, %rdx
.LBB17_82:                              # %.lr.ph28.i.i.prol.loopexit
                                        #   in Loop: Header=BB17_33 Depth=2
	cmpq	$3, %rax
	jb	.LBB17_85
# BB#83:                                # %.lr.ph28.i.i.preheader.new
                                        #   in Loop: Header=BB17_33 Depth=2
	movl	$101, %eax
	subq	%rdx, %rax
	imulq	$808, %rdx, %rdx        # imm = 0x328
	.p2align	4, 0x90
.LBB17_84:                              # %.lr.ph28.i.i
                                        #   Parent Loop BB17_27 Depth=1
                                        #     Parent Loop BB17_33 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%r9,%rdx), %xmm3       # xmm3 = mem[0],zero
	mulsd	%xmm5, %xmm3
	movsd	%xmm3, (%r9,%rdx)
	movsd	808(%r9,%rdx), %xmm3    # xmm3 = mem[0],zero
	mulsd	%xmm5, %xmm3
	movsd	%xmm3, 808(%r9,%rdx)
	movsd	1616(%r9,%rdx), %xmm3   # xmm3 = mem[0],zero
	mulsd	%xmm5, %xmm3
	movsd	%xmm3, 1616(%r9,%rdx)
	movsd	2424(%r9,%rdx), %xmm3   # xmm3 = mem[0],zero
	mulsd	%xmm5, %xmm3
	movsd	%xmm3, 2424(%r9,%rdx)
	addq	$3232, %rdx             # imm = 0xCA0
	addq	$-4, %rax
	jne	.LBB17_84
.LBB17_85:                              # %.loopexit3.i.i
                                        #   in Loop: Header=BB17_33 Depth=2
	cmpq	$101, %rcx
	jne	.LBB17_115
	jmp	.LBB17_86
.LBB17_45:                              # %.lr.ph22.split.us.split.i.i.preheader
                                        #   in Loop: Header=BB17_33 Depth=2
	xorpd	%xmm5, %xmm5
	testb	$1, %r8b
	je	.LBB17_47
# BB#46:                                # %.lr.ph22.split.us.split.i.i.prol
                                        #   in Loop: Header=BB17_33 Depth=2
	imulq	$808, %r13, %rax        # imm = 0x328
	addq	%r14, %rax
	movsd	(%rax,%rdi,8), %xmm6    # xmm6 = mem[0],zero
	andpd	%xmm0, %xmm6
	mulsd	(%r10,%r13,8), %xmm6
	ucomisd	%xmm1, %xmm6
	xorpd	%xmm5, %xmm5
	cmpnlesd	%xmm6, %xmm5
	andnpd	%xmm6, %xmm5
	cmovael	%r13d, %r11d
	incq	%r13
.LBB17_47:                              # %.lr.ph22.split.us.split.i.i.prol.loopexit
                                        #   in Loop: Header=BB17_33 Depth=2
	movq	8(%rsp), %rbx           # 8-byte Reload
	cmpq	$100, %rcx
	je	.LBB17_50
# BB#48:                                # %.lr.ph22.split.us.split.i.i.preheader.new
                                        #   in Loop: Header=BB17_33 Depth=2
	imulq	$101, %r13, %rax
	.p2align	4, 0x90
.LBB17_49:                              # %.lr.ph22.split.us.split.i.i
                                        #   Parent Loop BB17_27 Depth=1
                                        #     Parent Loop BB17_33 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%r9,%rax,8), %xmm3     # xmm3 = mem[0],zero
	andpd	%xmm4, %xmm3
	mulsd	(%r10,%r13,8), %xmm3
	ucomisd	%xmm5, %xmm3
	movapd	%xmm5, %xmm6
	cmpnlesd	%xmm3, %xmm6
	movapd	%xmm6, %xmm7
	andnpd	%xmm3, %xmm7
	andpd	%xmm5, %xmm6
	orpd	%xmm7, %xmm6
	cmovael	%r13d, %r11d
	movsd	808(%r9,%rax,8), %xmm3  # xmm3 = mem[0],zero
	andpd	%xmm4, %xmm3
	mulsd	8(%r10,%r13,8), %xmm3
	leal	1(%r13), %ecx
	ucomisd	%xmm6, %xmm3
	movapd	%xmm6, %xmm5
	cmpnlesd	%xmm3, %xmm5
	movapd	%xmm5, %xmm7
	andpd	%xmm6, %xmm7
	andnpd	%xmm3, %xmm5
	orpd	%xmm7, %xmm5
	cmovael	%ecx, %r11d
	addq	$202, %rax
	addq	$2, %r13
	cmpq	$101, %r13
	jne	.LBB17_49
	jmp	.LBB17_50
	.p2align	4, 0x90
.LBB17_86:                              # %.preheader.i.preheader
                                        #   in Loop: Header=BB17_27 Depth=1
	movl	$-1, %esi
	movq	16(%rsp), %rcx          # 8-byte Reload
	xorl	%edx, %edx
	movq	48(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB17_87:                              # %.preheader.i
                                        #   Parent Loop BB17_27 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_91 Depth 3
                                        #       Child Loop BB17_93 Depth 3
	movslq	192(%rsp,%rdx,4), %rax
	movsd	(%r13,%rax,8), %xmm5    # xmm5 = mem[0],zero
	movq	(%r13,%rdx,8), %rdi
	movq	%rdi, (%r13,%rax,8)
	cmpl	$-1, %esi
	je	.LBB17_94
# BB#88:                                # %.preheader2.i.i
                                        #   in Loop: Header=BB17_87 Depth=2
	movslq	%esi, %rdi
	cmpq	%rdi, %rdx
	jle	.LBB17_97
# BB#89:                                # %.lr.ph10.i.i.preheader
                                        #   in Loop: Header=BB17_87 Depth=2
	leaq	-1(%rdx), %r8
	movl	%edx, %eax
	subl	%esi, %eax
	subq	%rdi, %r8
	testb	$3, %al
	je	.LBB17_92
# BB#90:                                # %.lr.ph10.i.i.prol.preheader
                                        #   in Loop: Header=BB17_87 Depth=2
	leaq	(%r15,%rdi,8), %rbp
	leaq	(%rcx,%rdi,8), %rbx
	movl	%edx, %eax
	subb	%sil, %al
	movzbl	%al, %eax
	andl	$3, %eax
	negq	%rax
	.p2align	4, 0x90
.LBB17_91:                              # %.lr.ph10.i.i.prol
                                        #   Parent Loop BB17_27 Depth=1
                                        #     Parent Loop BB17_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rbx), %xmm3           # xmm3 = mem[0],zero
	mulsd	(%rbp), %xmm3
	subsd	%xmm3, %xmm5
	incq	%rdi
	addq	$8, %rbp
	addq	$8, %rbx
	incq	%rax
	jne	.LBB17_91
.LBB17_92:                              # %.lr.ph10.i.i.prol.loopexit
                                        #   in Loop: Header=BB17_87 Depth=2
	cmpq	$3, %r8
	jb	.LBB17_97
	.p2align	4, 0x90
.LBB17_93:                              # %.lr.ph10.i.i
                                        #   Parent Loop BB17_27 Depth=1
                                        #     Parent Loop BB17_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rcx,%rdi,8), %xmm3    # xmm3 = mem[0],zero
	mulsd	(%r15,%rdi,8), %xmm3
	subsd	%xmm3, %xmm5
	movsd	8(%rcx,%rdi,8), %xmm3   # xmm3 = mem[0],zero
	mulsd	8(%r15,%rdi,8), %xmm3
	subsd	%xmm3, %xmm5
	movsd	16(%rcx,%rdi,8), %xmm3  # xmm3 = mem[0],zero
	mulsd	16(%r15,%rdi,8), %xmm3
	subsd	%xmm3, %xmm5
	movsd	24(%rcx,%rdi,8), %xmm3  # xmm3 = mem[0],zero
	mulsd	24(%r15,%rdi,8), %xmm3
	subsd	%xmm3, %xmm5
	addq	$4, %rdi
	cmpq	%rdi, %rdx
	jne	.LBB17_93
	jmp	.LBB17_97
	.p2align	4, 0x90
.LBB17_94:                              #   in Loop: Header=BB17_87 Depth=2
	ucomisd	%xmm1, %xmm5
	jne	.LBB17_96
	jnp	.LBB17_95
.LBB17_96:                              #   in Loop: Header=BB17_87 Depth=2
	movl	%edx, %esi
	jmp	.LBB17_97
.LBB17_95:                              #   in Loop: Header=BB17_87 Depth=2
	movl	$-1, %esi
	.p2align	4, 0x90
.LBB17_97:                              # %.loopexit3.i1.i
                                        #   in Loop: Header=BB17_87 Depth=2
	movsd	%xmm5, (%r13,%rdx,8)
	incq	%rdx
	addq	$808, %rcx              # imm = 0x328
	cmpq	$101, %rdx
	jne	.LBB17_87
# BB#98:                                # %.preheader1.i.i.preheader
                                        #   in Loop: Header=BB17_27 Depth=1
	movq	88(%rsp), %r9           # 8-byte Reload
	movq	96(%rsp), %r10          # 8-byte Reload
	movq	104(%rsp), %r11         # 8-byte Reload
	xorl	%edi, %edi
	movl	$100, %ebp
	movl	$101, %ebx
	.p2align	4, 0x90
.LBB17_99:                              # %.preheader1.i.i
                                        #   Parent Loop BB17_27 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_104 Depth 3
                                        #       Child Loop BB17_108 Depth 3
	movsd	(%r13,%rbp,8), %xmm5    # xmm5 = mem[0],zero
	cmpq	$100, %rbp
	je	.LBB17_109
# BB#100:                               # %.preheader1.i.i
                                        #   in Loop: Header=BB17_99 Depth=2
	leaq	1(%rbp), %rax
	cmpq	$100, %rax
	jg	.LBB17_109
# BB#101:                               # %.lr.ph.i4.i.preheader
                                        #   in Loop: Header=BB17_99 Depth=2
	leaq	-1(%rdi), %r8
	testb	$3, %dil
	je	.LBB17_102
# BB#103:                               # %.lr.ph.i4.i.prol.preheader
                                        #   in Loop: Header=BB17_99 Depth=2
	movl	%edi, %eax
	andl	$3, %eax
	negq	%rax
	movq	%r10, %rdx
	movq	%r11, %rsi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB17_104:                             # %.lr.ph.i4.i.prol
                                        #   Parent Loop BB17_27 Depth=1
                                        #     Parent Loop BB17_99 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rdx), %xmm3           # xmm3 = mem[0],zero
	mulsd	(%rsi), %xmm3
	subsd	%xmm3, %xmm5
	decq	%rcx
	addq	$8, %rsi
	addq	$8, %rdx
	cmpq	%rcx, %rax
	jne	.LBB17_104
# BB#105:                               # %.lr.ph.i4.i.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB17_99 Depth=2
	movq	%rbx, %rax
	subq	%rcx, %rax
	cmpq	$3, %r8
	jae	.LBB17_107
	jmp	.LBB17_109
.LBB17_102:                             #   in Loop: Header=BB17_99 Depth=2
	movq	%rbx, %rax
	cmpq	$3, %r8
	jb	.LBB17_109
.LBB17_107:                             # %.lr.ph.i4.i.preheader.new
                                        #   in Loop: Header=BB17_99 Depth=2
	leaq	(%r9,%rax,8), %rcx
	addq	$3, %rax
	.p2align	4, 0x90
.LBB17_108:                             # %.lr.ph.i4.i
                                        #   Parent Loop BB17_27 Depth=1
                                        #     Parent Loop BB17_99 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-24(%rcx), %xmm3        # xmm3 = mem[0],zero
	movsd	-16(%rcx), %xmm6        # xmm6 = mem[0],zero
	mulsd	-24(%r15,%rax,8), %xmm3
	subsd	%xmm3, %xmm5
	mulsd	-16(%r15,%rax,8), %xmm6
	subsd	%xmm6, %xmm5
	movsd	-8(%rcx), %xmm3         # xmm3 = mem[0],zero
	mulsd	-8(%r15,%rax,8), %xmm3
	subsd	%xmm3, %xmm5
	movsd	(%rcx), %xmm3           # xmm3 = mem[0],zero
	mulsd	(%r15,%rax,8), %xmm3
	subsd	%xmm3, %xmm5
	addq	$4, %rax
	addq	$32, %rcx
	cmpq	$104, %rax
	jne	.LBB17_108
.LBB17_109:                             # %.loopexit.i.i
                                        #   in Loop: Header=BB17_99 Depth=2
	imulq	$808, %rbp, %rax        # imm = 0x328
	addq	%r14, %rax
	divsd	(%rax,%rbp,8), %xmm5
	movsd	%xmm5, (%r13,%rbp,8)
	decq	%rbx
	incq	%rdi
	addq	$-8, %r11
	addq	$-816, %r10             # imm = 0xFCD0
	addq	$-808, %r9              # imm = 0xFCD8
	testq	%rbp, %rbp
	leaq	-1(%rbp), %rbp
	jg	.LBB17_99
.LBB17_110:                             # %lusolve.exit
                                        #   in Loop: Header=BB17_27 Depth=1
	movq	144(%rsp), %rdx         # 8-byte Reload
	incq	%rdx
	movq	152(%rsp), %rcx         # 8-byte Reload
	addq	$81608, %rcx            # imm = 0x13EC8
	addq	$81608, 16(%rsp)        # 8-byte Folded Spill
                                        # imm = 0x13EC8
	addq	$81608, 32(%rsp)        # 8-byte Folded Spill
                                        # imm = 0x13EC8
	addq	$81608, 64(%rsp)        # 8-byte Folded Spill
                                        # imm = 0x13EC8
	addq	$81608, 112(%rsp)       # 8-byte Folded Spill
                                        # imm = 0x13EC8
	addq	$808, %r15              # imm = 0x328
	addq	$808, 104(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x328
	addq	$81608, 96(%rsp)        # 8-byte Folded Spill
                                        # imm = 0x13EC8
	addq	$81608, 88(%rsp)        # 8-byte Folded Spill
                                        # imm = 0x13EC8
	cmpq	56(%rsp), %rdx          # 8-byte Folded Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	jne	.LBB17_27
# BB#111:
	movq	136(%rsp), %rdi         # 8-byte Reload
	jmp	.LBB17_112
.LBB17_114:                             # %._crit_edge51.thread
	xorl	%eax, %eax
	callq	StartStopwatch
	movq	%rax, %rdi
.LBB17_112:                             # %._crit_edge
	addq	$600, %rsp              # imm = 0x258
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	StopStopwatch           # TAILCALL
.Lfunc_end17:
	.size	DoLUIteration, .Lfunc_end17-DoLUIteration
	.cfi_endproc

	.p2align	4, 0x90
	.type	strsift,@function
strsift:                                # @strsift
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi216:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi217:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi218:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi219:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi220:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi221:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi222:
	.cfi_def_cfa_offset 224
.Lcfi223:
	.cfi_offset %rbx, -56
.Lcfi224:
	.cfi_offset %r12, -48
.Lcfi225:
	.cfi_offset %r13, -40
.Lcfi226:
	.cfi_offset %r14, -32
.Lcfi227:
	.cfi_offset %r15, -24
.Lcfi228:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbp
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, %r12
	leaq	(%rbp,%rbp), %r14
	cmpq	%r8, %r14
	ja	.LBB18_45
# BB#1:                                 # %.lr.ph
	movq	8(%rsp), %rax           # 8-byte Reload
	decq	%rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	1(%r8), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	56(%r12), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%r8, 72(%rsp)           # 8-byte Spill
	movq	%r12, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB18_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_22 Depth 2
                                        #     Child Loop BB18_26 Depth 2
                                        #     Child Loop BB18_37 Depth 2
                                        #     Child Loop BB18_41 Depth 2
	cmpq	%r8, %r14
	jae	.LBB18_8
# BB#3:                                 #   in Loop: Header=BB18_2 Depth=1
	movq	%r14, %rbx
	orq	$1, %rbx
	movq	(%r12,%r14,8), %rax
	leaq	(%r15,%rax), %rdi
	movb	(%r15,%rax), %al
	movq	(%r12,%rbx,8), %rcx
	leaq	(%r15,%rcx), %rsi
	movb	(%r15,%rcx), %cl
	cmpb	%cl, %al
	seta	%r13b
	ja	.LBB18_5
# BB#4:                                 #   in Loop: Header=BB18_2 Depth=1
	movl	%eax, %ecx
.LBB18_5:                               #   in Loop: Header=BB18_2 Depth=1
	movzbl	%cl, %edx
	callq	strncmp
	testl	%eax, %eax
	sets	%al
	je	.LBB18_7
# BB#6:                                 #   in Loop: Header=BB18_2 Depth=1
	movl	%eax, %r13d
.LBB18_7:                               #   in Loop: Header=BB18_2 Depth=1
	testb	%r13b, %r13b
	cmovneq	%rbx, %r14
.LBB18_8:                               #   in Loop: Header=BB18_2 Depth=1
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	(%r12,%rbp,8), %rax
	leaq	(%r15,%rax), %rdi
	movb	(%r15,%rax), %al
	movq	(%r12,%r14,8), %rcx
	leaq	(%r15,%rcx), %rbp
	movzbl	(%r15,%rcx), %ebx
	cmpb	%bl, %al
	seta	%r13b
	movl	%ebx, %ecx
	ja	.LBB18_10
# BB#9:                                 #   in Loop: Header=BB18_2 Depth=1
	movl	%eax, %ecx
.LBB18_10:                              #   in Loop: Header=BB18_2 Depth=1
	movzbl	%cl, %edx
	movq	%rbp, %rsi
	callq	strncmp
	testl	%eax, %eax
	sets	%al
	je	.LBB18_12
# BB#11:                                #   in Loop: Header=BB18_2 Depth=1
	movl	%eax, %r13d
.LBB18_12:                              #   in Loop: Header=BB18_2 Depth=1
	cmpb	$1, %r13b
	movq	64(%rsp), %rax          # 8-byte Reload
	jne	.LBB18_44
# BB#13:                                #   in Loop: Header=BB18_2 Depth=1
	incq	%rbx
	leaq	80(%rsp), %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	MoveMemory
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%r12,%rax,8), %rax
	movzbl	(%r15,%rax), %r13d
	movq	(%r12,%r14,8), %rcx
	leaq	(%r15,%rcx), %rax
	movzbl	(%r15,%rcx), %ecx
	movl	%r13d, %ebx
	subl	%ecx, %ebx
	movl	%ebx, %ebp
	negl	%ebp
	cmovll	%ebx, %ebp
	movq	48(%rsp), %rcx          # 8-byte Reload
	subq	%r14, %rcx
	je	.LBB18_28
# BB#14:                                #   in Loop: Header=BB18_2 Depth=1
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	-8(%rcx,%r12,8), %rcx
	movzbl	(%r15,%rcx), %edx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsi,%r14,8), %rsi
	leaq	1(%rcx,%rdx), %rdx
	subq	%rsi, %rdx
	leaq	1(%rax,%r13), %rdi
	addq	%r15, %rsi
	callq	MoveMemory
	leaq	1(%r14), %rcx
	cmpq	%r12, %rcx
	movq	16(%rsp), %r12          # 8-byte Reload
	jae	.LBB18_27
# BB#15:                                # %.lr.ph.i
                                        #   in Loop: Header=BB18_2 Depth=1
	movzbl	%bpl, %edx
	movq	%rdx, %rax
	negq	%rax
	testl	%ebx, %ebx
	cmovnsq	%rdx, %rax
	movq	32(%rsp), %rdx          # 8-byte Reload
	cmpq	$4, %rdx
	jb	.LBB18_25
# BB#16:                                # %min.iters.checked79
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	%rdx, %r8
	andq	$-4, %r8
	movq	%rdx, %rsi
	andq	$-4, %rsi
	je	.LBB18_25
# BB#17:                                # %vector.ph83
                                        #   in Loop: Header=BB18_2 Depth=1
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%rsi), %rdi
	movq	%rdi, %rbx
	shrq	$2, %rbx
	btl	$2, %edi
	jb	.LBB18_18
# BB#19:                                # %vector.body75.prol
                                        #   in Loop: Header=BB18_2 Depth=1
	movdqu	8(%r12,%r14,8), %xmm1
	movdqu	24(%r12,%r14,8), %xmm2
	paddq	%xmm0, %xmm1
	paddq	%xmm0, %xmm2
	movdqu	%xmm1, 8(%r12,%r14,8)
	movdqu	%xmm2, 24(%r12,%r14,8)
	movl	$4, %ebp
	testq	%rbx, %rbx
	jne	.LBB18_21
	jmp	.LBB18_23
.LBB18_18:                              #   in Loop: Header=BB18_2 Depth=1
	xorl	%ebp, %ebp
	testq	%rbx, %rbx
	je	.LBB18_23
.LBB18_21:                              # %vector.ph83.new
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	%rsi, %rdi
	subq	%rbp, %rdi
	addq	%r14, %rbp
	movq	40(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rbp,8), %rbp
	.p2align	4, 0x90
.LBB18_22:                              # %vector.body75
                                        #   Parent Loop BB18_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-48(%rbp), %xmm1
	movdqu	-32(%rbp), %xmm2
	paddq	%xmm0, %xmm1
	paddq	%xmm0, %xmm2
	movdqu	%xmm1, -48(%rbp)
	movdqu	%xmm2, -32(%rbp)
	movdqu	-16(%rbp), %xmm1
	movdqu	(%rbp), %xmm2
	paddq	%xmm0, %xmm1
	paddq	%xmm0, %xmm2
	movdqu	%xmm1, -16(%rbp)
	movdqu	%xmm2, (%rbp)
	addq	$64, %rbp
	addq	$-8, %rdi
	jne	.LBB18_22
.LBB18_23:                              # %middle.block76
                                        #   in Loop: Header=BB18_2 Depth=1
	cmpq	%rsi, %rdx
	je	.LBB18_27
# BB#24:                                #   in Loop: Header=BB18_2 Depth=1
	addq	%r8, %rcx
.LBB18_25:                              # %scalar.ph77.preheader
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	subq	%rcx, %rdx
	leaq	(%r12,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB18_26:                              # %scalar.ph77
                                        #   Parent Loop BB18_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%rax, (%rcx)
	addq	$8, %rcx
	decq	%rdx
	jne	.LBB18_26
.LBB18_27:                              # %._crit_edge.i
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	(%r12,%r14,8), %rax
	addq	%r15, %rax
.LBB18_28:                              # %stradjust.exit
                                        #   in Loop: Header=BB18_2 Depth=1
	movb	%r13b, (%rax)
	movq	(%r12,%r14,8), %rdi
	addq	%r15, %rdi
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	(%r12,%rbp,8), %rsi
	addq	%r15, %rsi
	incq	%r13
	movq	%r13, %rdx
	callq	MoveMemory
	movzbl	80(%rsp), %eax
	movq	(%r12,%rbp,8), %rcx
	leaq	(%r15,%rcx), %rdi
	movzbl	(%r15,%rcx), %ecx
	movl	%eax, %r13d
	subl	%ecx, %r13d
	movl	%r13d, %ebx
	negl	%ebx
	cmovll	%r13d, %ebx
	movq	48(%rsp), %rcx          # 8-byte Reload
	subq	%rbp, %rcx
	je	.LBB18_43
# BB#29:                                #   in Loop: Header=BB18_2 Depth=1
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	-8(%r12,%rcx,8), %rcx
	movzbl	(%r15,%rcx), %edx
	movq	8(%r12,%rbp,8), %rsi
	leaq	1(%rcx,%rdx), %rdx
	subq	%rsi, %rdx
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	1(%rdi,%rax), %rdi
	addq	%r15, %rsi
	callq	MoveMemory
	leaq	1(%rbp), %rcx
	cmpq	8(%rsp), %rcx           # 8-byte Folded Reload
	jae	.LBB18_42
# BB#30:                                # %.lr.ph.i59
                                        #   in Loop: Header=BB18_2 Depth=1
	movzbl	%bl, %edx
	movq	%rdx, %rax
	negq	%rax
	testl	%r13d, %r13d
	cmovnsq	%rdx, %rax
	movq	56(%rsp), %rsi          # 8-byte Reload
	cmpq	$4, %rsi
	jb	.LBB18_40
# BB#31:                                # %min.iters.checked
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	%rsi, %r8
	andq	$-4, %r8
	movq	%rsi, %r9
	andq	$-4, %r9
	je	.LBB18_40
# BB#32:                                # %vector.ph
                                        #   in Loop: Header=BB18_2 Depth=1
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%r9), %rdi
	movq	%rdi, %rdx
	shrq	$2, %rdx
	btl	$2, %edi
	jb	.LBB18_33
# BB#34:                                # %vector.body.prol
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	movdqu	8(%rdi,%rbp,8), %xmm1
	movdqu	24(%rdi,%rbp,8), %xmm2
	paddq	%xmm0, %xmm1
	paddq	%xmm0, %xmm2
	movdqu	%xmm1, 8(%rdi,%rbp,8)
	movdqu	%xmm2, 24(%rdi,%rbp,8)
	movl	$4, %ebx
	testq	%rdx, %rdx
	jne	.LBB18_36
	jmp	.LBB18_38
.LBB18_33:                              #   in Loop: Header=BB18_2 Depth=1
	xorl	%ebx, %ebx
	movq	24(%rsp), %rbp          # 8-byte Reload
	testq	%rdx, %rdx
	je	.LBB18_38
.LBB18_36:                              # %vector.ph.new
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	%r9, %rdi
	subq	%rbx, %rdi
	addq	%rbp, %rbx
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB18_37:                              # %vector.body
                                        #   Parent Loop BB18_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-48(%rbx), %xmm1
	movdqu	-32(%rbx), %xmm2
	paddq	%xmm0, %xmm1
	paddq	%xmm0, %xmm2
	movdqu	%xmm1, -48(%rbx)
	movdqu	%xmm2, -32(%rbx)
	movdqu	-16(%rbx), %xmm1
	movdqu	(%rbx), %xmm2
	paddq	%xmm0, %xmm1
	paddq	%xmm0, %xmm2
	movdqu	%xmm1, -16(%rbx)
	movdqu	%xmm2, (%rbx)
	addq	$64, %rbx
	addq	$-8, %rdi
	jne	.LBB18_37
.LBB18_38:                              # %middle.block
                                        #   in Loop: Header=BB18_2 Depth=1
	cmpq	%r9, %rsi
	je	.LBB18_42
# BB#39:                                #   in Loop: Header=BB18_2 Depth=1
	addq	%r8, %rcx
.LBB18_40:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	subq	%rcx, %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB18_41:                              # %scalar.ph
                                        #   Parent Loop BB18_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%rax, (%rcx)
	addq	$8, %rcx
	decq	%rdx
	jne	.LBB18_41
.LBB18_42:                              # %._crit_edge.i64
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	(%r12,%rbp,8), %rdi
	addq	%r15, %rdi
	movq	32(%rsp), %rax          # 8-byte Reload
.LBB18_43:                              # %stradjust.exit66
                                        #   in Loop: Header=BB18_2 Depth=1
	movb	%al, (%rdi)
	movq	(%r12,%rbp,8), %rdi
	addq	%r15, %rdi
	incq	%rax
	leaq	80(%rsp), %rsi
	movq	%rax, %rdx
	callq	MoveMemory
	movq	%r14, %rax
.LBB18_44:                              # %.backedge
                                        #   in Loop: Header=BB18_2 Depth=1
	leaq	(%rax,%rax), %r14
	movq	72(%rsp), %r8           # 8-byte Reload
	cmpq	%r8, %r14
	movq	%rax, %rbp
	jbe	.LBB18_2
.LBB18_45:                              # %._crit_edge
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	strsift, .Lfunc_end18-strsift
	.cfi_endproc

	.p2align	4, 0x90
	.type	cipher_idea,@function
cipher_idea:                            # @cipher_idea
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi229:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi230:
	.cfi_def_cfa_offset 24
.Lcfi231:
	.cfi_offset %rbx, -24
.Lcfi232:
	.cfi_offset %rbp, -16
	movw	(%rdi), %r10w
	movw	2(%rdi), %r11w
	movzwl	4(%rdi), %eax
	movw	6(%rdi), %r8w
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB19_1:                               # =>This Inner Loop Header: Depth=1
	movw	%ax, %r9w
	movzwl	(%rdx,%rdi), %eax
	testw	%r10w, %r10w
	je	.LBB19_4
# BB#2:                                 #   in Loop: Header=BB19_1 Depth=1
	testw	%ax, %ax
	je	.LBB19_5
# BB#3:                                 #   in Loop: Header=BB19_1 Depth=1
	movzwl	%r10w, %ecx
	imull	%ecx, %eax
	movl	%eax, %ecx
	shrl	$16, %ecx
	movzwl	%ax, %ebx
	subl	%ecx, %eax
	cmpl	%ecx, %ebx
	adcl	$0, %eax
	jmp	.LBB19_6
	.p2align	4, 0x90
.LBB19_4:                               #   in Loop: Header=BB19_1 Depth=1
	movl	$1, %r10d
	subl	%eax, %r10d
	jmp	.LBB19_7
	.p2align	4, 0x90
.LBB19_5:                               #   in Loop: Header=BB19_1 Depth=1
	movl	$1, %eax
	subl	%r10d, %eax
.LBB19_6:                               # %mul.exit
                                        #   in Loop: Header=BB19_1 Depth=1
	movw	%ax, %r10w
.LBB19_7:                               # %mul.exit
                                        #   in Loop: Header=BB19_1 Depth=1
	addw	2(%rdx,%rdi), %r11w
	addw	4(%rdx,%rdi), %r9w
	movzwl	6(%rdx,%rdi), %eax
	testw	%r8w, %r8w
	je	.LBB19_10
# BB#8:                                 #   in Loop: Header=BB19_1 Depth=1
	testw	%ax, %ax
	je	.LBB19_11
# BB#9:                                 #   in Loop: Header=BB19_1 Depth=1
	movzwl	%r8w, %ecx
	imull	%ecx, %eax
	movl	%eax, %ecx
	shrl	$16, %ecx
	movzwl	%ax, %ebx
	subl	%ecx, %eax
	cmpl	%ecx, %ebx
	adcl	$0, %eax
	jmp	.LBB19_12
	.p2align	4, 0x90
.LBB19_10:                              #   in Loop: Header=BB19_1 Depth=1
	movl	$1, %r8d
	subl	%eax, %r8d
	jmp	.LBB19_13
	.p2align	4, 0x90
.LBB19_11:                              #   in Loop: Header=BB19_1 Depth=1
	movl	$1, %eax
	subl	%r8d, %eax
.LBB19_12:                              # %mul.exit73
                                        #   in Loop: Header=BB19_1 Depth=1
	movw	%ax, %r8w
.LBB19_13:                              # %mul.exit73
                                        #   in Loop: Header=BB19_1 Depth=1
	movw	%r9w, %cx
	xorw	%r10w, %cx
	movzwl	8(%rdx,%rdi), %eax
	je	.LBB19_16
# BB#14:                                #   in Loop: Header=BB19_1 Depth=1
	testw	%ax, %ax
	je	.LBB19_17
# BB#15:                                #   in Loop: Header=BB19_1 Depth=1
	movzwl	%cx, %ecx
	imull	%ecx, %eax
	movl	%eax, %ecx
	shrl	$16, %ecx
	movzwl	%ax, %ebx
	subl	%ecx, %eax
	cmpl	%ecx, %ebx
	adcl	$0, %eax
	jmp	.LBB19_18
	.p2align	4, 0x90
.LBB19_16:                              #   in Loop: Header=BB19_1 Depth=1
	movl	$1, %ecx
	subl	%eax, %ecx
	movw	%cx, %ax
	jmp	.LBB19_18
	.p2align	4, 0x90
.LBB19_17:                              #   in Loop: Header=BB19_1 Depth=1
	movl	$1, %eax
	subl	%ecx, %eax
.LBB19_18:                              # %mul.exit71
                                        #   in Loop: Header=BB19_1 Depth=1
	movl	%r8d, %ebx
	xorl	%r11d, %ebx
	addw	%ax, %bx
	movzwl	10(%rdx,%rdi), %ecx
	je	.LBB19_21
# BB#19:                                #   in Loop: Header=BB19_1 Depth=1
	testw	%cx, %cx
	je	.LBB19_22
# BB#20:                                #   in Loop: Header=BB19_1 Depth=1
	movzwl	%bx, %ebx
	imull	%ebx, %ecx
	movl	%ecx, %ebx
	shrl	$16, %ebx
	movzwl	%cx, %ebp
	subl	%ebx, %ecx
	cmpl	%ebx, %ebp
	adcl	$0, %ecx
	jmp	.LBB19_23
	.p2align	4, 0x90
.LBB19_21:                              #   in Loop: Header=BB19_1 Depth=1
	movl	$1, %ebx
	subl	%ecx, %ebx
	movw	%bx, %cx
	jmp	.LBB19_23
	.p2align	4, 0x90
.LBB19_22:                              #   in Loop: Header=BB19_1 Depth=1
	movl	$1, %ecx
	subl	%ebx, %ecx
.LBB19_23:                              # %mul.exit69
                                        #   in Loop: Header=BB19_1 Depth=1
	addl	%ecx, %eax
	xorl	%ecx, %r10d
	xorl	%eax, %r8d
	xorl	%eax, %r11d
	movl	%r11d, %eax
	xorl	%ecx, %r9d
	movw	%r9w, %r11w
	addq	$12, %rdi
	cmpl	$96, %edi
	jne	.LBB19_1
# BB#24:
	movzwl	96(%rdx), %ecx
	testw	%r10w, %r10w
	je	.LBB19_27
# BB#25:
	testw	%cx, %cx
	je	.LBB19_28
# BB#26:
	movzwl	%r10w, %edi
	imull	%edi, %ecx
	movl	%ecx, %edi
	shrl	$16, %edi
	movzwl	%cx, %ebp
	subl	%edi, %ecx
	cmpl	%edi, %ebp
	adcl	$0, %ecx
	jmp	.LBB19_29
.LBB19_27:
	movl	$1, %edi
	subl	%ecx, %edi
	movw	%di, %cx
	jmp	.LBB19_29
.LBB19_28:
	movl	$1, %ecx
	subl	%r10d, %ecx
.LBB19_29:                              # %mul.exit67
	movw	%cx, (%rsi)
	addw	98(%rdx), %ax
	movw	%ax, 2(%rsi)
	addw	100(%rdx), %r11w
	movw	%r11w, 4(%rsi)
	movzwl	102(%rdx), %eax
	testw	%r8w, %r8w
	je	.LBB19_32
# BB#30:
	testw	%ax, %ax
	je	.LBB19_33
# BB#31:
	movzwl	%r8w, %ecx
	imull	%ecx, %eax
	movl	%eax, %ecx
	shrl	$16, %ecx
	movzwl	%ax, %edx
	subl	%ecx, %eax
	cmpl	%ecx, %edx
	adcl	$0, %eax
	jmp	.LBB19_34
.LBB19_32:
	movl	$1, %ecx
	subl	%eax, %ecx
	movw	%cx, %ax
	jmp	.LBB19_34
.LBB19_33:
	movl	$1, %eax
	subl	%r8d, %eax
.LBB19_34:                              # %mul.exit65
	movw	%ax, 6(%rsi)
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end19:
	.size	cipher_idea, .Lfunc_end19-cipher_idea
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"NNET.DAT"
	.size	.L.str, 9

	.type	inpath,@object          # @inpath
	.data
	.globl	inpath
	.p2align	3
inpath:
	.quad	.L.str
	.size	inpath, 8

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"Hello"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"He"
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Him"
	.size	.L.str.3, 4

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"the"
	.size	.L.str.4, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"this"
	.size	.L.str.5, 5

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"that"
	.size	.L.str.6, 5

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"though"
	.size	.L.str.7, 7

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"rough"
	.size	.L.str.8, 6

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"cough"
	.size	.L.str.9, 6

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"obviously"
	.size	.L.str.10, 10

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"But"
	.size	.L.str.11, 4

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"but"
	.size	.L.str.12, 4

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"bye"
	.size	.L.str.13, 4

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"begin"
	.size	.L.str.14, 6

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"beginning"
	.size	.L.str.15, 10

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"beginnings"
	.size	.L.str.16, 11

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"of"
	.size	.L.str.17, 3

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"our"
	.size	.L.str.18, 4

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"ourselves"
	.size	.L.str.19, 10

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"yourselves"
	.size	.L.str.20, 11

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"to"
	.size	.L.str.21, 3

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"together"
	.size	.L.str.22, 9

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"togetherness"
	.size	.L.str.23, 13

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"from"
	.size	.L.str.24, 5

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"either"
	.size	.L.str.25, 7

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"I"
	.size	.L.str.26, 2

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"A"
	.size	.L.str.27, 2

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"return"
	.size	.L.str.28, 7

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"However"
	.size	.L.str.29, 8

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"example"
	.size	.L.str.30, 8

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"yet"
	.size	.L.str.31, 4

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"quickly"
	.size	.L.str.32, 8

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"all"
	.size	.L.str.33, 4

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"if"
	.size	.L.str.34, 3

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"were"
	.size	.L.str.35, 5

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"includes"
	.size	.L.str.36, 9

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"always"
	.size	.L.str.37, 7

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"never"
	.size	.L.str.38, 6

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"not"
	.size	.L.str.39, 4

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"small"
	.size	.L.str.40, 6

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"returns"
	.size	.L.str.41, 8

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"set"
	.size	.L.str.42, 4

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"basic"
	.size	.L.str.43, 6

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"Entered"
	.size	.L.str.44, 8

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"with"
	.size	.L.str.45, 5

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"used"
	.size	.L.str.46, 5

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"shown"
	.size	.L.str.47, 6

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"you"
	.size	.L.str.48, 4

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"know"
	.size	.L.str.49, 5

	.type	wordcatarray,@object    # @wordcatarray
	.data
	.globl	wordcatarray
	.p2align	4
wordcatarray:
	.quad	.L.str.1
	.quad	.L.str.2
	.quad	.L.str.3
	.quad	.L.str.4
	.quad	.L.str.5
	.quad	.L.str.6
	.quad	.L.str.7
	.quad	.L.str.8
	.quad	.L.str.9
	.quad	.L.str.10
	.quad	.L.str.11
	.quad	.L.str.12
	.quad	.L.str.13
	.quad	.L.str.14
	.quad	.L.str.15
	.quad	.L.str.16
	.quad	.L.str.17
	.quad	.L.str.18
	.quad	.L.str.19
	.quad	.L.str.20
	.quad	.L.str.21
	.quad	.L.str.22
	.quad	.L.str.23
	.quad	.L.str.24
	.quad	.L.str.25
	.quad	.L.str.26
	.quad	.L.str.27
	.quad	.L.str.28
	.quad	.L.str.29
	.quad	.L.str.6
	.quad	.L.str.30
	.quad	.L.str.31
	.quad	.L.str.32
	.quad	.L.str.33
	.quad	.L.str.34
	.quad	.L.str.35
	.quad	.L.str.36
	.quad	.L.str.37
	.quad	.L.str.38
	.quad	.L.str.39
	.quad	.L.str.40
	.quad	.L.str.41
	.quad	.L.str.42
	.quad	.L.str.43
	.quad	.L.str.44
	.quad	.L.str.45
	.quad	.L.str.46
	.quad	.L.str.47
	.quad	.L.str.48
	.quad	.L.str.49
	.size	wordcatarray, 400

	.type	.L.str.50,@object       # @.str.50
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.50:
	.asciz	"CPU:Numeric Sort"
	.size	.L.str.50, 17

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"CPU:String Sort"
	.size	.L.str.52, 16

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"CPU:Bitfields"
	.size	.L.str.53, 14

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"CPU:Floating Emulation"
	.size	.L.str.54, 23

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"FPU:Transcendental"
	.size	.L.str.56, 19

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"CPU:Assignment"
	.size	.L.str.57, 15

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"CPU:IDEA"
	.size	.L.str.58, 9

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"CPU:Huffman"
	.size	.L.str.59, 12

	.type	hufftree,@object        # @hufftree
	.local	hufftree
	.comm	hufftree,8,8
	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"FPU:LU"
	.size	.L.str.61, 7

	.type	LUtempvv,@object        # @LUtempvv
	.comm	LUtempvv,8,8
	.type	mid_wts,@object         # @mid_wts
	.comm	mid_wts,2240,16
	.type	out_wts,@object         # @out_wts
	.comm	out_wts,512,16
	.type	mid_out,@object         # @mid_out
	.comm	mid_out,64,16
	.type	out_out,@object         # @out_out
	.comm	out_out,64,16
	.type	mid_error,@object       # @mid_error
	.comm	mid_error,64,16
	.type	out_error,@object       # @out_error
	.comm	out_error,64,16
	.type	mid_wt_change,@object   # @mid_wt_change
	.comm	mid_wt_change,2240,16
	.type	out_wt_change,@object   # @out_wt_change
	.comm	out_wt_change,512,16
	.type	in_pats,@object         # @in_pats
	.comm	in_pats,2800,16
	.type	out_pats,@object        # @out_pats
	.comm	out_pats,640,16
	.type	tot_out_error,@object   # @tot_out_error
	.comm	tot_out_error,80,16
	.type	out_wt_cum_change,@object # @out_wt_cum_change
	.comm	out_wt_cum_change,512,16
	.type	mid_wt_cum_change,@object # @mid_wt_cum_change
	.comm	mid_wt_cum_change,2240,16
	.type	worst_error,@object     # @worst_error
	.comm	worst_error,8,8
	.type	average_error,@object   # @average_error
	.comm	average_error,8,8
	.type	avg_out_error,@object   # @avg_out_error
	.comm	avg_out_error,80,16
	.type	iteration_count,@object # @iteration_count
	.comm	iteration_count,4,4
	.type	numpats,@object         # @numpats
	.comm	numpats,4,4
	.type	numpasses,@object       # @numpasses
	.comm	numpasses,4,4
	.type	learned,@object         # @learned
	.comm	learned,4,4
	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"CPU:Stringsort"
	.size	.L.str.63, 15

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"r"
	.size	.L.str.64, 2

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"\n CPU:NNET--error in opening file!"
	.size	.L.str.65, 35

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"%d  %d  %d"
	.size	.L.str.66, 11

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"\n CPU:NNET -- Should read 3 items in line one; did read %d"
	.size	.L.str.67, 59

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"%d"
	.size	.L.str.68, 3

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"\n CPU:NNET -- Should read 1 item in line 2; did read %d"
	.size	.L.str.69, 56

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"%d  %d  %d  %d  %d"
	.size	.L.str.70, 19

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"\n CPU:NNET -- failure in reading input!"
	.size	.L.str.71, 40

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"%d  %d  %d  %d  %d  %d  %d  %d"
	.size	.L.str.72, 31

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"CPU:NSORT -- NUMNUMARRAYS hit."
	.size	.Lstr, 31

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"CPU:EMFPU -- CMPUEMFLOATLOOPMAX limit hit"
	.size	.Lstr.1, 42

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"FPU:LU -- Array limit reached"
	.size	.Lstr.2, 30


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
