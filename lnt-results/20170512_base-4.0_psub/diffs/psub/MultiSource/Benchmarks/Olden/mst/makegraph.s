	.text
	.file	"makegraph.bc"
	.globl	MakeGraph
	.p2align	4, 0x90
	.type	MakeGraph,@function
MakeGraph:                              # @MakeGraph
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movl	%edi, %ebp
	movl	$8, %edi
	callq	malloc
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	$0, (%rax)
	movl	%ebp, %eax
	cltd
	idivl	%r15d
	movl	%eax, %r14d
	movl	$.Lstr, %edi
	callq	puts
	movl	%r15d, %ebx
	decl	%ebx
	js	.LBB0_1
# BB#2:                                 # %.lr.ph72
	testl	%r14d, %r14d
	movslq	%ebx, %rdx
	movl	%ebp, 24(%rsp)          # 4-byte Spill
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	jle	.LBB0_3
# BB#4:                                 # %.lr.ph72.split.us.preheader
	movl	%ebx, 40(%rsp)          # 4-byte Spill
	movl	%r15d, 44(%rsp)         # 4-byte Spill
	movslq	%r14d, %rax
	shlq	$3, %rax
	leaq	(%rax,%rax,2), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%ebp, %ebx
	sarl	$31, %ebx
	shrl	$30, %ebx
	addl	%ebp, %ebx
	sarl	$2, %ebx
	movl	%r14d, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph72.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_6 Depth 2
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	malloc
	movq	%rax, %r12
	movq	48(%rsp), %rbp          # 8-byte Reload
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_6:                                #   Parent Loop BB0_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decq	%rbp
	movslq	%ebp, %rax
	leaq	(%rax,%rax,2), %r14
	leaq	(%r12,%r14,8), %r13
	movl	%ebx, HashRange(%rip)
	movl	$9999999, (%r12,%r14,8) # imm = 0x98967F
	movl	$hashfunc, %esi
	movl	%ebx, %edi
	callq	MakeHash
	movq	%rax, 16(%r12,%r14,8)
	movq	%r15, 8(%r12,%r14,8)
	testq	%rbp, %rbp
	movq	%r13, %r15
	jne	.LBB0_6
# BB#7:                                 # %._crit_edge69.us
                                        #   in Loop: Header=BB0_5 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%r12, (%rax,%rdx,8)
	testq	%rdx, %rdx
	leaq	-1(%rdx), %rdx
	jg	.LBB0_5
# BB#8:                                 # %._crit_edge73
	movl	$.Lstr.1, %edi
	callq	puts
	movl	40(%rsp), %ebx          # 4-byte Reload
	testl	%ebx, %ebx
	movl	24(%rsp), %ebp          # 4-byte Reload
	movl	44(%rsp), %r15d         # 4-byte Reload
	jns	.LBB0_9
	jmp	.LBB0_31
.LBB0_1:                                # %._crit_edge73.thread90
	movl	$.Lstr.1, %edi
	callq	puts
	jmp	.LBB0_31
.LBB0_3:                                # %._crit_edge73.thread
	shlq	$3, %rdx
	movl	%r15d, %eax
	negl	%eax
	cmpl	$-2, %eax
	movl	$-1, %ecx
	cmovgl	%eax, %ecx
	addl	%r15d, %ecx
	leaq	(,%rcx,8), %rax
	subq	%rax, %rdx
	movq	16(%rsp), %rdi          # 8-byte Reload
	addq	%rdx, %rdi
	leaq	8(,%rcx,8), %rdx
	xorl	%esi, %esi
	callq	memset
	movl	$.Lstr.1, %edi
	callq	puts
.LBB0_9:                                # %.lr.ph
	movl	12(%rsp), %r13d         # 4-byte Reload
	imull	%r15d, %r13d
	testl	%r15d, %r15d
	jle	.LBB0_10
# BB#19:                                # %.lr.ph.split.us.preheader
	movl	%ebx, %eax
	leaq	8(,%rax,8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movslq	%r15d, %rbx
	.p2align	4, 0x90
.LBB0_20:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_32 Depth 2
                                        #     Child Loop BB0_23 Depth 2
                                        #       Child Loop BB0_24 Depth 3
	leaq	64(%rsp), %rdi
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movq	-8(%r14,%rbx,8), %r15
	decq	%rbx
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	testq	%r15, %r15
	je	.LBB0_28
# BB#21:                                # %.preheader.lr.ph.i.us
                                        #   in Loop: Header=BB0_20 Depth=1
	testl	%r13d, %r13d
	jle	.LBB0_32
# BB#22:                                # %.preheader.us.i.us.preheader
                                        #   in Loop: Header=BB0_20 Depth=1
	movq	64(%rsp), %r12
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r14d
	imull	12(%rsp), %r14d         # 4-byte Folded Reload
	.p2align	4, 0x90
.LBB0_23:                               # %.preheader.us.i.us
                                        #   Parent Loop BB0_20 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_24 Depth 3
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_24:                               #   Parent Loop BB0_20 Depth=1
                                        #     Parent Loop BB0_23 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%ebx, %r14d
	je	.LBB0_26
# BB#25:                                #   in Loop: Header=BB0_24 Depth=3
	cmpl	%r14d, %ebx
	movl	%r14d, %eax
	cmovlel	%ebx, %eax
	movl	%ebx, %ecx
	cmovll	%r14d, %ecx
	imull	%ebp, %eax
	addl	%ecx, %eax
	cltq
	imulq	$1759218605, %rax, %rcx # imm = 0x68DB8BAD
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$44, %rcx
	addl	%edx, %ecx
	imull	$10000, %ecx, %edx      # imm = 0x2710
	subl	%edx, %eax
	imull	$3141, %eax, %edx       # imm = 0xC45
	imull	$5821, %ecx, %ecx       # imm = 0x16BD
	addl	%edx, %ecx
	movslq	%ecx, %rcx
	imulq	$1759218605, %rcx, %rdx # imm = 0x68DB8BAD
	movq	%rdx, %rsi
	shrq	$63, %rsi
	sarq	$44, %rdx
	addl	%esi, %edx
	imull	$10000, %edx, %edx      # imm = 0x2710
	subl	%edx, %ecx
	imull	$10000, %ecx, %ecx      # imm = 0x2710
	imull	$5821, %eax, %eax       # imm = 0x16BD
	leal	(%rax,%rcx), %edx
	leal	1(%rax,%rcx), %ecx
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$21, %eax
	leal	1(%rax,%rdx), %eax
	andl	$-2048, %eax            # imm = 0xF800
	subl	%eax, %ecx
	incl	%ecx
	movl	%ebx, %eax
	cltd
	idivl	12(%rsp)                # 4-byte Folded Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	leal	(%rdx,%rdx,2), %eax
	leal	(%r12,%rax,8), %esi
	movq	16(%r15), %rdx
	movslq	%ecx, %rdi
	callq	HashInsert
.LBB0_26:                               #   in Loop: Header=BB0_24 Depth=3
	incl	%ebx
	cmpl	%ebx, %r13d
	jne	.LBB0_24
# BB#27:                                # %._crit_edge.us.i.us
                                        #   in Loop: Header=BB0_23 Depth=2
	incl	%r14d
	movq	8(%r15), %r15
	testq	%r15, %r15
	jne	.LBB0_23
	jmp	.LBB0_28
	.p2align	4, 0x90
.LBB0_32:                               # %.preheader.i.us
                                        #   Parent Loop BB0_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r15), %r15
	testq	%r15, %r15
	jne	.LBB0_32
.LBB0_28:                               # %AddEdges.exit.us
                                        #   in Loop: Header=BB0_20 Depth=1
	movq	24(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	jg	.LBB0_20
	jmp	.LBB0_31
.LBB0_10:                               # %.lr.ph.split.preheader
	movslq	%ebx, %r15
	.p2align	4, 0x90
.LBB0_11:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_29 Depth 2
                                        #     Child Loop BB0_14 Depth 2
                                        #       Child Loop BB0_15 Depth 3
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r15,8), %r14
	testq	%r14, %r14
	je	.LBB0_30
# BB#12:                                # %.preheader.lr.ph.i
                                        #   in Loop: Header=BB0_11 Depth=1
	testl	%r13d, %r13d
	jle	.LBB0_29
# BB#13:                                # %.preheader.us.i.preheader
                                        #   in Loop: Header=BB0_11 Depth=1
	movl	%r15d, %ebp
	imull	12(%rsp), %ebp          # 4-byte Folded Reload
	.p2align	4, 0x90
.LBB0_14:                               # %.preheader.us.i
                                        #   Parent Loop BB0_11 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_15 Depth 3
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_15:                               #   Parent Loop BB0_11 Depth=1
                                        #     Parent Loop BB0_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%ebx, %ebp
	je	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_15 Depth=3
	cmpl	%ebp, %ebx
	movl	%ebp, %eax
	cmovlel	%ebx, %eax
	movl	%ebx, %ecx
	cmovll	%ebp, %ecx
	imull	24(%rsp), %eax          # 4-byte Folded Reload
	addl	%ecx, %eax
	cltq
	imulq	$1759218605, %rax, %rcx # imm = 0x68DB8BAD
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$44, %rcx
	addl	%edx, %ecx
	imull	$10000, %ecx, %edx      # imm = 0x2710
	subl	%edx, %eax
	imull	$3141, %eax, %edx       # imm = 0xC45
	imull	$5821, %ecx, %ecx       # imm = 0x16BD
	addl	%edx, %ecx
	movslq	%ecx, %rcx
	imulq	$1759218605, %rcx, %rdx # imm = 0x68DB8BAD
	movq	%rdx, %rsi
	shrq	$63, %rsi
	sarq	$44, %rdx
	addl	%esi, %edx
	imull	$10000, %edx, %edx      # imm = 0x2710
	subl	%edx, %ecx
	imull	$10000, %ecx, %ecx      # imm = 0x2710
	imull	$5821, %eax, %eax       # imm = 0x16BD
	leal	(%rax,%rcx), %edx
	leal	1(%rax,%rcx), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$21, %ecx
	leal	1(%rcx,%rdx), %ecx
	andl	$-2048, %ecx            # imm = 0xF800
	subl	%ecx, %eax
	incl	%eax
	movq	16(%r14), %rdx
	movslq	%eax, %rdi
	callq	HashInsert
.LBB0_17:                               #   in Loop: Header=BB0_15 Depth=3
	incl	%ebx
	cmpl	%ebx, %r13d
	jne	.LBB0_15
# BB#18:                                # %._crit_edge.us.i
                                        #   in Loop: Header=BB0_14 Depth=2
	incl	%ebp
	movq	8(%r14), %r14
	testq	%r14, %r14
	jne	.LBB0_14
	jmp	.LBB0_30
	.p2align	4, 0x90
.LBB0_29:                               # %.preheader.i
                                        #   Parent Loop BB0_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r14), %r14
	testq	%r14, %r14
	jne	.LBB0_29
.LBB0_30:                               # %AddEdges.exit
                                        #   in Loop: Header=BB0_11 Depth=1
	testq	%r15, %r15
	leaq	-1(%r15), %r15
	jg	.LBB0_11
.LBB0_31:                               # %._crit_edge
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$.Lstr.3, %edi
	callq	puts
	movq	16(%rsp), %rax          # 8-byte Reload
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	MakeGraph, .Lfunc_end0-MakeGraph
	.cfi_endproc

	.p2align	4, 0x90
	.type	hashfunc,@function
hashfunc:                               # @hashfunc
	.cfi_startproc
# BB#0:
	shrl	$3, %edi
	xorl	%edx, %edx
	movl	%edi, %eax
	divl	HashRange(%rip)
	movl	%edx, %eax
	retq
.Lfunc_end1:
	.size	hashfunc, .Lfunc_end1-hashfunc
	.cfi_endproc

	.type	HashRange,@object       # @HashRange
	.local	HashRange
	.comm	HashRange,4,4
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr:
	.asciz	"Make phase 2"
	.size	.Lstr, 13

	.type	.Lstr.1,@object         # @str.1
.Lstr.1:
	.asciz	"Make phase 3"
	.size	.Lstr.1, 13

	.type	.Lstr.2,@object         # @str.2
.Lstr.2:
	.asciz	"Make phase 4"
	.size	.Lstr.2, 13

	.type	.Lstr.3,@object         # @str.3
.Lstr.3:
	.asciz	"Make returning"
	.size	.Lstr.3, 15


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
