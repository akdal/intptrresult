	.text
	.file	"loopFilter.bc"
	.globl	DeblockFrame
	.p2align	4, 0x90
	.type	DeblockFrame,@function
DeblockFrame:                           # @DeblockFrame
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	15348(%rbx), %eax
	testq	%rax, %rax
	je	.LBB0_16
# BB#1:                                 # %.lr.ph17
	movq	14224(%rbx), %rdx
	testb	$1, %al
	jne	.LBB0_3
# BB#2:
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	jne	.LBB0_7
	jmp	.LBB0_13
.LBB0_3:
	cmpl	$14, 72(%rdx)
	jne	.LBB0_5
# BB#4:
	movl	$0, 8(%rdx)
.LBB0_5:                                # %.prol.loopexit
	movl	$1, %ecx
	cmpl	$1, %eax
	je	.LBB0_13
.LBB0_7:                                # %.lr.ph17.new
	imulq	$536, %rcx, %rsi        # imm = 0x218
	leaq	608(%rdx,%rsi), %rdx
	.p2align	4, 0x90
.LBB0_8:                                # =>This Inner Loop Header: Depth=1
	cmpl	$14, -536(%rdx)
	jne	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_8 Depth=1
	movl	$0, -600(%rdx)
.LBB0_10:                               #   in Loop: Header=BB0_8 Depth=1
	cmpl	$14, (%rdx)
	jne	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_8 Depth=1
	movl	$0, -64(%rdx)
.LBB0_12:                               #   in Loop: Header=BB0_8 Depth=1
	addq	$2, %rcx
	addq	$1072, %rdx             # imm = 0x430
	cmpq	%rax, %rcx
	jb	.LBB0_8
.LBB0_13:                               # %.preheader
	testl	%eax, %eax
	je	.LBB0_16
# BB#14:                                # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_15:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	movl	%ebp, %ecx
	callq	DeblockMb
	incl	%ebp
	cmpl	15348(%rbx), %ebp
	jb	.LBB0_15
.LBB0_16:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	DeblockFrame, .Lfunc_end0-DeblockFrame
	.cfi_endproc

	.globl	DeblockMb
	.p2align	4, 0x90
	.type	DeblockMb,@function
DeblockMb:                              # @DeblockMb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 160
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%ecx, %r13d
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movq	%rdi, %r15
	movaps	.LDeblockMb.filterNon8x8LumaEdgesFlag(%rip), %xmm0
	movaps	%xmm0, 80(%rsp)
	movl	$1, 15420(%r15)
	leaq	60(%rsp), %rsi
	leaq	56(%rsp), %rdx
	xorl	%ecx, %ecx
	movl	%r13d, %edi
	callq	get_mb_pos
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	cmpl	$0, 60(%rsp)
	setne	%sil
	movl	56(%rsp), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	setne	%al
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	14224(%r15), %r14
	movslq	%r13d, %rcx
	imulq	$536, %rcx, %rbp        # imm = 0x218
	xorl	%ecx, %ecx
	cmpl	$0, 472(%r14,%rbp)
	sete	%cl
	movl	%ecx, 92(%rsp)
	movl	%ecx, 84(%rsp)
	movl	15268(%r15), %ecx
	testl	%ecx, %ecx
	je	.LBB1_3
# BB#1:
	cmpl	$16, %edx
	jne	.LBB1_3
# BB#2:
	testl	%edx, %edx
	setne	%dl
	cmpl	$0, 424(%r14,%rbp)
	sete	%al
	andb	%dl, %al
	movzbl	%al, %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
.LBB1_3:
	cmpl	$0, 24(%r15)
	je	.LBB1_4
.LBB1_6:                                # %.thread
	movl	$2, 12(%rsp)            # 4-byte Folded Spill
.LBB1_7:                                # %.thread151
	movl	516(%r14,%rbp), %edx
	cmpl	$1, %edx
	je	.LBB1_40
# BB#8:                                 # %.thread151
	cmpl	$2, %edx
	jne	.LBB1_9
# BB#10:
	movl	452(%r14,%rbp), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	testl	%ecx, %ecx
	je	.LBB1_13
# BB#11:
	testb	$1, %r13b
	je	.LBB1_13
# BB#12:
	movl	424(%r14,%rbp), %eax
	movl	$1, 8(%rsp)             # 4-byte Folded Spill
	testl	%eax, %eax
	je	.LBB1_14
.LBB1_13:
	movl	456(%r14,%rbp), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	jmp	.LBB1_14
.LBB1_4:
	movl	$4, 12(%rsp)            # 4-byte Folded Spill
	testl	%ecx, %ecx
	je	.LBB1_7
# BB#5:
	cmpl	$0, 424(%r14,%rbp)
	jne	.LBB1_6
	jmp	.LBB1_7
.LBB1_9:
	movl	4(%rsp), %eax           # 4-byte Reload
	movb	%sil, %al
	movl	%eax, 4(%rsp)           # 4-byte Spill
.LBB1_14:
	movl	%r13d, 12(%r15)
	callq	CheckAvailabilityOfNeighbors
	leaq	520(%r14,%rbp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_15:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_19 Depth 2
                                        #       Child Loop BB1_22 Depth 3
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB1_17
# BB#16:                                #   in Loop: Header=BB1_15 Depth=1
	movb	$1, %r14b
	testq	%rbp, %rbp
	jne	.LBB1_18
.LBB1_17:                               #   in Loop: Header=BB1_15 Depth=1
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	setne	%al
	testq	%rbp, %rbp
	sete	%r14b
	andb	%al, %r14b
.LBB1_18:                               #   in Loop: Header=BB1_15 Depth=1
	xorl	%r12d, %r12d
	movl	12(%rsp), %ebx          # 4-byte Reload
	movb	%r14b, 3(%rsp)          # 1-byte Spill
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_19:                               #   Parent Loop BB1_15 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_22 Depth 3
	testq	%r12, %r12
	setne	%al
	orb	%r14b, %al
	cmpb	$1, %al
	jne	.LBB1_38
# BB#20:                                #   in Loop: Header=BB1_19 Depth=2
	movslq	15536(%r15), %rax
	movq	%rbp, %rcx
	shlq	$4, %rcx
	leaq	(%rcx,%r12,4), %rcx
	movsbl	chroma_edge(%rax,%rcx), %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	32(%rsp), %rdi
	movq	%r15, %rsi
	movl	%r13d, %edx
	movl	%ebp, %ecx
	movl	%r12d, %r8d
	movl	%ebx, %r9d
	callq	GetStrength
	cmpb	$0, 32(%rsp)
	jne	.LBB1_25
# BB#21:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_19 Depth=2
	movl	$1, %eax
	.p2align	4, 0x90
.LBB1_22:                               # %.lr.ph
                                        #   Parent Loop BB1_15 Depth=1
                                        #     Parent Loop BB1_19 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	32(%rsp,%rax), %ecx
	cmpq	$14, %rax
	jg	.LBB1_24
# BB#23:                                # %.lr.ph
                                        #   in Loop: Header=BB1_22 Depth=3
	incq	%rax
	testb	%cl, %cl
	je	.LBB1_22
.LBB1_24:                               # %._crit_edge
                                        #   in Loop: Header=BB1_19 Depth=2
	testb	%cl, %cl
	je	.LBB1_30
.LBB1_25:                               # %._crit_edge.thread
                                        #   in Loop: Header=BB1_19 Depth=2
	cmpl	$0, 80(%rsp,%r12,4)
	je	.LBB1_27
# BB#26:                                #   in Loop: Header=BB1_19 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r8d
	movl	4(%rax), %r9d
	movl	52(%r15), %eax
	subq	$8, %rsp
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	movq	72(%rsp), %rdi          # 8-byte Reload
	leaq	40(%rsp), %rsi
	movq	%r15, %rdx
	movl	%r13d, %ecx
	pushq	$0
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	callq	EdgeLoop
	addq	$48, %rsp
.Lcfi28:
	.cfi_adjust_cfa_offset -48
.LBB1_27:                               #   in Loop: Header=BB1_19 Depth=2
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB1_30
# BB#28:                                #   in Loop: Header=BB1_19 Depth=2
	cmpb	$0, 48(%rsp)            # 1-byte Folded Reload
	js	.LBB1_30
# BB#29:                                #   in Loop: Header=BB1_19 Depth=2
	movl	%r13d, %r14d
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	(%r13), %rdi
	movq	%r15, %rbx
	movq	16(%rsp), %r15          # 8-byte Reload
	movl	(%r15), %r8d
	movl	4(%r15), %r9d
	movl	64(%rbx), %eax
	subq	$8, %rsp
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	leaq	40(%rsp), %rcx
	movq	%rcx, %rsi
	movq	%rbx, %rdx
	movl	%r14d, %ecx
	pushq	$0
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rbp
	pushq	%rbp
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	pushq	112(%rsp)               # 8-byte Folded Reload
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	callq	EdgeLoop
	addq	$48, %rsp
.Lcfi35:
	.cfi_adjust_cfa_offset -48
	movq	8(%r13), %rdi
	movl	%r14d, %r13d
	movb	3(%rsp), %r14b          # 1-byte Reload
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	(%r15), %r8d
	movl	4(%r15), %r9d
	movq	%rbx, %r15
	movl	%eax, %ebx
	movl	64(%r15), %eax
	subq	$8, %rsp
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	leaq	40(%rsp), %rsi
	movq	%r15, %rdx
	movl	%r13d, %ecx
	pushq	$1
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	movq	112(%rsp), %rbp         # 8-byte Reload
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	callq	EdgeLoop
	addq	$48, %rsp
.Lcfi42:
	.cfi_adjust_cfa_offset -48
.LBB1_30:                               #   in Loop: Header=BB1_19 Depth=2
	testq	%rbp, %rbp
	je	.LBB1_38
# BB#31:                                #   in Loop: Header=BB1_19 Depth=2
	testq	%r12, %r12
	jne	.LBB1_38
# BB#32:                                #   in Loop: Header=BB1_19 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpl	$0, -96(%rax)
	jne	.LBB1_38
# BB#33:                                #   in Loop: Header=BB1_19 Depth=2
	movb	mixedModeEdgeFlag(%rip), %al
	testb	%al, %al
	je	.LBB1_38
# BB#34:                                #   in Loop: Header=BB1_19 Depth=2
	movl	$2, 15420(%r15)
	movl	$4, %r8d
	leaq	32(%rsp), %r14
	movq	%r14, %rdi
	movq	%r15, %rsi
	movl	%r13d, %edx
	movl	%ebp, %ecx
	movl	%ebx, %r9d
	callq	GetStrength
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r8d
	movl	4(%rax), %r9d
	movl	52(%r15), %eax
	subq	$8, %rsp
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rsi
	movq	%r15, %rdx
	movl	%r13d, %ecx
	pushq	$0
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	pushq	$4
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	callq	EdgeLoop
	addq	$48, %rsp
.Lcfi49:
	.cfi_adjust_cfa_offset -48
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB1_37
# BB#35:                                #   in Loop: Header=BB1_19 Depth=2
	cmpb	$0, 48(%rsp)            # 1-byte Folded Reload
	js	.LBB1_37
# BB#36:                                #   in Loop: Header=BB1_19 Depth=2
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	(%r14), %rdi
	movq	16(%rsp), %rbx          # 8-byte Reload
	movl	(%rbx), %r8d
	movl	4(%rbx), %r9d
	movl	64(%r15), %eax
	subq	$8, %rsp
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	leaq	40(%rsp), %rcx
	movq	%rcx, %rsi
	movq	%r15, %rdx
	movl	%r13d, %ecx
	pushq	$0
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	pushq	$4
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	callq	EdgeLoop
	addq	$48, %rsp
.Lcfi56:
	.cfi_adjust_cfa_offset -48
	movq	8(%r14), %rdi
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	(%rbx), %r8d
	movl	4(%rbx), %r9d
	movl	%eax, %ebx
	movl	64(%r15), %eax
	subq	$8, %rsp
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	leaq	40(%rsp), %rsi
	movq	%r15, %rdx
	movl	%r13d, %ecx
	pushq	$1
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi60:
	.cfi_adjust_cfa_offset 8
	pushq	$4
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	callq	EdgeLoop
	addq	$48, %rsp
.Lcfi63:
	.cfi_adjust_cfa_offset -48
.LBB1_37:                               #   in Loop: Header=BB1_19 Depth=2
	movl	$1, 15420(%r15)
	movb	3(%rsp), %r14b          # 1-byte Reload
	.p2align	4, 0x90
.LBB1_38:                               #   in Loop: Header=BB1_19 Depth=2
	incq	%r12
	cmpq	$4, %r12
	jne	.LBB1_19
# BB#39:                                #   in Loop: Header=BB1_15 Depth=1
	incq	%rbp
	cmpq	$2, %rbp
	jne	.LBB1_15
.LBB1_40:                               # %.loopexit
	movl	$0, 15420(%r15)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	DeblockMb, .Lfunc_end1-DeblockMb
	.cfi_endproc

	.globl	GetStrength
	.p2align	4, 0x90
	.type	GetStrength,@function
GetStrength:                            # @GetStrength
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi68:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi70:
	.cfi_def_cfa_offset 208
.Lcfi71:
	.cfi_offset %rbx, -56
.Lcfi72:
	.cfi_offset %r12, -48
.Lcfi73:
	.cfi_offset %r13, -40
.Lcfi74:
	.cfi_offset %r14, -32
.Lcfi75:
	.cfi_offset %r15, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movl	%r9d, 8(%rsp)           # 4-byte Spill
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rsi, %r15
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	enc_picture(%rip), %rax
	movq	6512(%rax), %rsi
	movq	(%rsi), %rdi
	movq	%rdi, 104(%rsp)         # 8-byte Spill
	movq	8(%rsi), %rsi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	6488(%rax), %rsi
	movq	6496(%rax), %rax
	movq	(%rsi), %rdi
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	movq	8(%rsi), %rsi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	14224(%r15), %rsi
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movslq	%edx, %rdx
	leal	(,%r8,4), %edi
	movq	%r8, 96(%rsp)           # 8-byte Spill
	cmpl	$4, %r8d
	movl	$1, %ebp
	movl	%edi, 52(%rsp)          # 4-byte Spill
	cmovll	%edi, %ebp
	movl	%ebp, 48(%rsp)          # 4-byte Spill
	imulq	$536, %rdx, %rdx        # imm = 0x218
	leaq	424(%rsi,%rdx), %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	(%rax), %rdx
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	sete	7(%rsp)                 # 1-byte Folded Spill
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	leal	-1(%rcx), %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	xorl	%r14d, %r14d
	jmp	.LBB2_1
.LBB2_15:                               #   in Loop: Header=BB2_1 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	-352(%rax), %edi
	cmpl	$14, %edi
	ja	.LBB2_17
# BB#16:                                #   in Loop: Header=BB2_1 Depth=1
	movl	$26112, %eax            # imm = 0x6600
	btl	%edi, %eax
	jb	.LBB2_43
.LBB2_17:                               #   in Loop: Header=BB2_1 Depth=1
	andl	$-4, %r13d
	sarl	$2, %r12d
	addl	%r13d, %r12d
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	-56(%rax), %rax
	movb	$2, %bl
	btq	%r12, %rax
	jb	.LBB2_42
# BB#18:                                #   in Loop: Header=BB2_1 Depth=1
	andl	$-4, %esi
	sarl	$2, %r8d
	addl	%esi, %r8d
	movq	368(%rcx,%rdx), %rax
	btq	%r8, %rax
	jb	.LBB2_42
# BB#19:                                #   in Loop: Header=BB2_1 Depth=1
	movb	$1, %bl
	cmpb	$0, mixedModeEdgeFlag(%rip)
	je	.LBB2_20
.LBB2_42:                               #   in Loop: Header=BB2_1 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movb	%bl, (%rax,%r14)
	jmp	.LBB2_43
.LBB2_20:                               #   in Loop: Header=BB2_1 Depth=1
	movl	12(%rsp), %edi          # 4-byte Reload
	leaq	60(%rsp), %rsi
	leaq	56(%rsp), %rdx
	callq	*get_mb_block_pos(%rip)
	movl	56(%rsp), %eax
	movl	%r12d, %ecx
	sarl	$2, %ecx
	leal	(%rcx,%rax,4), %edx
	movl	60(%rsp), %eax
	andl	$3, %r12d
	leal	(%r12,%rax,4), %esi
	movl	144(%rsp), %eax
	movl	148(%rsp), %ecx
	sarl	$2, %ecx
	sarl	$2, %eax
	movslq	%edx, %r13
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx,%r13,8), %rdx
	movslq	%esi, %r11
	cmpb	$0, (%rdx,%r11)
	movabsq	$-9223372036854775808, %rbx # imm = 0x8000000000000000
	movq	%rbx, %r8
	movq	%r8, %rdi
	js	.LBB2_22
# BB#21:                                #   in Loop: Header=BB2_1 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx,%r13,8), %rdx
	movq	(%rdx,%r11,8), %rdi
.LBB2_22:                               #   in Loop: Header=BB2_1 Depth=1
	movslq	%ecx, %rsi
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rsi,8), %rcx
	movslq	%eax, %rdx
	cmpb	$0, (%rcx,%rdx)
	movq	%r8, %rax
	js	.LBB2_24
# BB#23:                                #   in Loop: Header=BB2_1 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rsi,8), %rax
	movq	(%rax,%rdx,8), %rax
.LBB2_24:                               #   in Loop: Header=BB2_1 Depth=1
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%r13,8), %rcx
	cmpb	$0, (%rcx,%r11)
	movq	%r8, %rcx
	movb	$1, %bl
	js	.LBB2_26
# BB#25:                                #   in Loop: Header=BB2_1 Depth=1
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%r13,8), %rcx
	movq	(%rcx,%r11,8), %rcx
.LBB2_26:                               #   in Loop: Header=BB2_1 Depth=1
	movq	80(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp,%rsi,8), %rbp
	cmpb	$0, (%rbp,%rdx)
	movq	%r8, %rbp
	js	.LBB2_28
# BB#27:                                #   in Loop: Header=BB2_1 Depth=1
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp,%rsi,8), %rbp
	movq	(%rbp,%rdx,8), %rbp
.LBB2_28:                               #   in Loop: Header=BB2_1 Depth=1
	cmpq	%rax, %rdi
	jne	.LBB2_30
# BB#29:                                #   in Loop: Header=BB2_1 Depth=1
	cmpq	%rbp, %rcx
	je	.LBB2_32
.LBB2_30:                               #   in Loop: Header=BB2_1 Depth=1
	cmpq	%rax, %rcx
	jne	.LBB2_42
# BB#31:                                #   in Loop: Header=BB2_1 Depth=1
	cmpq	%rbp, %rdi
	jne	.LBB2_42
.LBB2_32:                               #   in Loop: Header=BB2_1 Depth=1
	movq	16(%rsp), %rbp          # 8-byte Reload
	movb	$0, (%rbp,%r14)
	cmpq	%rcx, %rdi
	movq	104(%rsp), %rbp         # 8-byte Reload
	movq	(%rbp,%r13,8), %rcx
	movq	(%rcx,%r11,8), %r12
	movswl	(%r12), %r10d
	movq	(%rbp,%rsi,8), %rcx
	movq	(%rcx,%rdx,8), %r9
	jne	.LBB2_33
# BB#36:                                #   in Loop: Header=BB2_1 Depth=1
	movswl	(%r9), %ecx
	movl	%r10d, %eax
	movl	%ecx, 44(%rsp)          # 4-byte Spill
	subl	%ecx, %eax
	movl	%eax, %r8d
	negl	%r8d
	cmovll	%eax, %r8d
	movswl	2(%r12), %edi
	movswl	2(%r9), %r9d
	movl	%edi, %eax
	subl	%r9d, %eax
	movl	%eax, %ebx
	negl	%ebx
	cmovll	%eax, %ebx
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp,%r13,8), %rax
	movq	(%rax,%r11,8), %rcx
	movswl	(%rcx), %eax
	movq	(%rbp,%rsi,8), %rsi
	movq	(%rsi,%rdx,8), %rdx
	movswl	(%rdx), %r11d
	movl	%eax, %esi
	subl	%r11d, %esi
	movl	%esi, %ebp
	negl	%ebp
	cmovll	%esi, %ebp
	movswl	2(%rcx), %esi
	movswl	2(%rdx), %r12d
	movl	%esi, %ecx
	subl	%r12d, %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$3, %r8d
	jg	.LBB2_41
# BB#37:                                #   in Loop: Header=BB2_1 Depth=1
	cmpl	8(%rsp), %ebx           # 4-byte Folded Reload
	jge	.LBB2_41
# BB#38:                                #   in Loop: Header=BB2_1 Depth=1
	cmpl	$3, %ebp
	jg	.LBB2_41
# BB#39:                                #   in Loop: Header=BB2_1 Depth=1
	cmpl	8(%rsp), %edx           # 4-byte Folded Reload
	jge	.LBB2_41
# BB#40:                                #   in Loop: Header=BB2_1 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB2_42
.LBB2_33:                               #   in Loop: Header=BB2_1 Depth=1
	cmpq	%rax, %rdi
	jne	.LBB2_35
# BB#34:                                #   in Loop: Header=BB2_1 Depth=1
	movswl	(%r9), %eax
	subl	%eax, %r10d
	movl	%r10d, %eax
	negl	%eax
	cmovll	%r10d, %eax
	cmpl	$3, %eax
	setg	%al
	movswl	2(%r12), %ecx
	movswl	2(%r9), %edi
	subl	%edi, %ecx
	movl	%ecx, %edi
	negl	%edi
	cmovll	%ecx, %edi
	movl	8(%rsp), %ebp           # 4-byte Reload
	cmpl	%ebp, %edi
	setge	%cl
	orb	%al, %cl
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx,%r13,8), %rax
	movq	(%rax,%r11,8), %rax
	movswl	(%rax), %edi
	movq	(%rbx,%rsi,8), %rsi
	movq	(%rsi,%rdx,8), %rdx
	movswl	(%rdx), %esi
	subl	%esi, %edi
	movl	%edi, %esi
	negl	%esi
	cmovll	%edi, %esi
	cmpl	$3, %esi
	setg	%sil
	movswl	2(%rax), %eax
	movswl	2(%rdx), %edx
	subl	%edx, %eax
	movl	%eax, %edx
	negl	%edx
	cmovll	%eax, %edx
	cmpl	%ebp, %edx
	setge	%bl
	orb	%sil, %bl
	orb	%cl, %bl
	jmp	.LBB2_42
.LBB2_41:                               #   in Loop: Header=BB2_1 Depth=1
	subl	%r11d, %r10d
	movl	%r10d, %ecx
	negl	%ecx
	cmovll	%r10d, %ecx
	cmpl	$3, %ecx
	setg	%cl
	subl	%r12d, %edi
	movl	%edi, %edx
	negl	%edx
	cmovll	%edi, %edx
	movl	8(%rsp), %edi           # 4-byte Reload
	cmpl	%edi, %edx
	setge	%dl
	orb	%cl, %dl
	subl	44(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	$3, %ecx
	setg	%al
	subl	%r9d, %esi
	movl	%esi, %ecx
	negl	%ecx
	cmovll	%esi, %ecx
	cmpl	%edi, %ecx
	setge	%bl
	orb	%al, %bl
	orb	%dl, %bl
	jmp	.LBB2_42
.LBB2_35:                               #   in Loop: Header=BB2_1 Depth=1
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp,%rsi,8), %rax
	movq	(%rax,%rdx,8), %rax
	movswl	(%rax), %ecx
	subl	%ecx, %r10d
	movl	%r10d, %ecx
	negl	%ecx
	cmovll	%r10d, %ecx
	cmpl	$3, %ecx
	setg	%cl
	movswl	2(%r12), %edx
	movswl	2(%rax), %eax
	subl	%eax, %edx
	movl	%edx, %eax
	negl	%eax
	cmovll	%edx, %eax
	movl	8(%rsp), %edi           # 4-byte Reload
	cmpl	%edi, %eax
	setge	%al
	orb	%cl, %al
	movq	(%rbp,%r13,8), %rcx
	movq	(%rcx,%r11,8), %rcx
	movswl	(%rcx), %edx
	movswl	(%r9), %esi
	subl	%esi, %edx
	movl	%edx, %esi
	negl	%esi
	cmovll	%edx, %esi
	cmpl	$3, %esi
	setg	%dl
	movswl	2(%rcx), %ecx
	movswl	2(%r9), %esi
	subl	%esi, %ecx
	movl	%ecx, %esi
	negl	%esi
	cmovll	%ecx, %esi
	cmpl	%edi, %esi
	setge	%bl
	orb	%dl, %bl
	orb	%al, %bl
	jmp	.LBB2_42
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movq	120(%rsp), %rax         # 8-byte Reload
	testl	%eax, %eax
	movl	52(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %r12d
	cmovnel	%r14d, %r12d
	movl	%r14d, %r13d
	cmovnel	48(%rsp), %r13d         # 4-byte Folded Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%r12), %esi
	movl	%r13d, %edx
	subl	%eax, %edx
	xorl	%ecx, %ecx
	movl	12(%rsp), %edi          # 4-byte Reload
	leaq	128(%rsp), %r8
	callq	*getNeighbour(%rip)
	movl	136(%rsp), %r8d
	movq	14224(%r15), %rcx
	movslq	132(%rsp), %rdx
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %edi
	imulq	$536, %rdx, %rdx        # imm = 0x218
	movl	424(%rcx,%rdx), %ebp
	cmpl	%ebp, %edi
	setne	mixedModeEdgeFlag(%rip)
	movl	140(%rsp), %esi
	movl	20(%r15), %ebx
	addl	$-3, %ebx
	cmpl	$1, %ebx
	ja	.LBB2_8
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
	movb	$3, %al
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	jne	.LBB2_7
# BB#3:                                 #   in Loop: Header=BB2_1 Depth=1
	cmpl	$0, 15268(%r15)
	je	.LBB2_4
# BB#5:                                 # %.thread
                                        #   in Loop: Header=BB2_1 Depth=1
	orl	%edi, %ebp
	sete	%cl
	orb	7(%rsp), %cl            # 1-byte Folded Reload
	cmpb	$1, %cl
	je	.LBB2_6
	jmp	.LBB2_7
	.p2align	4, 0x90
.LBB2_8:                                #   in Loop: Header=BB2_1 Depth=1
	movb	$3, %bl
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	jne	.LBB2_13
# BB#9:                                 #   in Loop: Header=BB2_1 Depth=1
	cmpl	$0, 15268(%r15)
	je	.LBB2_10
# BB#11:                                # %.thread285
                                        #   in Loop: Header=BB2_1 Depth=1
	orl	%edi, %ebp
	sete	%al
	orb	7(%rsp), %al            # 1-byte Folded Reload
	cmpb	$1, %al
	je	.LBB2_12
	jmp	.LBB2_13
.LBB2_4:                                #   in Loop: Header=BB2_1 Depth=1
	cmpl	$0, 24(%r15)
	sete	%cl
	orb	7(%rsp), %cl            # 1-byte Folded Reload
	je	.LBB2_7
.LBB2_6:                                # %.thread281
                                        #   in Loop: Header=BB2_1 Depth=1
	movb	$4, %al
	.p2align	4, 0x90
.LBB2_7:                                # %.thread283
                                        #   in Loop: Header=BB2_1 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movb	%al, (%rcx,%r14)
	jmp	.LBB2_43
.LBB2_10:                               #   in Loop: Header=BB2_1 Depth=1
	cmpl	$0, 24(%r15)
	sete	%al
	orb	7(%rsp), %al            # 1-byte Folded Reload
	je	.LBB2_13
.LBB2_12:                               # %.thread289
                                        #   in Loop: Header=BB2_1 Depth=1
	movb	$4, %bl
	.p2align	4, 0x90
.LBB2_13:                               # %.thread291
                                        #   in Loop: Header=BB2_1 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movb	%bl, (%rax,%r14)
	movl	72(%rcx,%rdx), %edi
	cmpl	$14, %edi
	ja	.LBB2_15
# BB#14:                                # %.thread291
                                        #   in Loop: Header=BB2_1 Depth=1
	movl	$26112, %eax            # imm = 0x6600
	btl	%edi, %eax
	jae	.LBB2_15
.LBB2_43:                               #   in Loop: Header=BB2_1 Depth=1
	incq	%r14
	cmpq	$16, %r14
	jne	.LBB2_1
# BB#44:
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	GetStrength, .Lfunc_end2-GetStrength
	.cfi_endproc

	.globl	EdgeLoop
	.p2align	4, 0x90
	.type	EdgeLoop,@function
EdgeLoop:                               # @EdgeLoop
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi80:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi81:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 56
	subq	$248, %rsp
.Lcfi83:
	.cfi_def_cfa_offset 304
.Lcfi84:
	.cfi_offset %rbx, -56
.Lcfi85:
	.cfi_offset %r12, -48
.Lcfi86:
	.cfi_offset %r13, -40
.Lcfi87:
	.cfi_offset %r14, -32
.Lcfi88:
	.cfi_offset %r15, -24
.Lcfi89:
	.cfi_offset %rbp, -16
	movl	%r9d, 96(%rsp)          # 4-byte Spill
	movl	%r8d, 92(%rsp)          # 4-byte Spill
	movq	%rsi, 184(%rsp)         # 8-byte Spill
	movq	%rdi, 176(%rsp)         # 8-byte Spill
	movl	328(%rsp), %esi
	leaq	15448(%rdx), %rax
	leaq	15444(%rdx), %rdi
	testl	%esi, %esi
	cmovneq	%rax, %rdi
	movl	(%rdi), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	testl	%esi, %esi
	movl	304(%rsp), %ebp
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	je	.LBB3_1
# BB#2:
	movslq	%ebp, %rax
	movslq	15536(%rdx), %rdx
	shlq	$4, %rax
	movl	EdgeLoop.pelnum_cr(%rax,%rdx,4), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	jg	.LBB3_3
	jmp	.LBB3_50
.LBB3_1:
	movl	$16, 24(%rsp)           # 4-byte Folded Spill
.LBB3_3:                                # %.lr.ph
	addl	$-8, 28(%rsp)           # 4-byte Folded Spill
	movl	312(%rsp), %eax
	cmpl	$0, 328(%rsp)
	setne	%dl
	leal	(,%rax,4), %esi
	cmpl	$4, %eax
	movl	$1, %eax
	movl	$1, %edi
	movl	%esi, 108(%rsp)         # 4-byte Spill
	cmovll	%esi, %edi
	movl	%edi, 104(%rsp)         # 4-byte Spill
	leal	-1(%rbp), %esi
	movl	%esi, 100(%rsp)         # 4-byte Spill
	cmpl	$8, 24(%rsp)            # 4-byte Folded Reload
	sete	%bl
	andb	%dl, %bl
	movb	%bl, 7(%rsp)            # 1-byte Spill
	testl	%ebp, %ebp
	movslq	%ecx, %rcx
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	movslq	336(%rsp), %rcx
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	cmovnel	320(%rsp), %eax
	movl	%eax, 88(%rsp)          # 4-byte Spill
	movl	$0, 52(%rsp)            # 4-byte Folded Spill
	xorl	%r15d, %r15d
	movl	$0, (%rsp)              # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB3_4
.LBB3_20:                               #   in Loop: Header=BB3_4 Depth=1
	movq	%rdi, 144(%rsp)         # 8-byte Spill
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r13, 56(%rsp)          # 8-byte Spill
	addl	%r9d, %r9d
	movslq	%r9d, %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movzwl	(%rbp,%rax,2), %r13d
	movq	40(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%rdx), %ecx
	movslq	%ecx, %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	160(%rsp), %r8          # 8-byte Reload
	movzwl	(%r8,%rax,2), %esi
	imull	$-3, 8(%rsp), %ecx      # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movzwl	(%rbp,%rcx,2), %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	leal	(%rdx,%rdx,2), %eax
	cltq
	movzwl	(%r8,%rax,2), %ecx
	cmpl	$0, 328(%rsp)
	movl	(%rsp), %ebp            # 4-byte Reload
	movq	152(%rsp), %rdi         # 8-byte Reload
	jne	.LBB3_22
# BB#21:                                #   in Loop: Header=BB3_4 Depth=1
	movl	%edi, %eax
	subl	%esi, %eax
	movq	%rcx, %r9
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	xorl	%eax, %eax
	cmpl	%ebx, %ecx
	setl	%al
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%r13d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	xorl	%ebp, %ebp
	cmpl	%ebx, %ecx
	movq	%r9, %rcx
	setl	%bpl
.LBB3_22:                               #   in Loop: Header=BB3_4 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	leal	(%rdi,%rax), %r9d
	cmpb	$4, %r14b
	jne	.LBB3_41
# BB#23:                                #   in Loop: Header=BB3_4 Depth=1
	cmpl	$0, 328(%rsp)
	je	.LBB3_25
# BB#24:                                #   in Loop: Header=BB3_4 Depth=1
	movl	%ebp, (%rsp)            # 4-byte Spill
	movq	64(%rsp), %rdx          # 8-byte Reload
	addl	%edx, %edi
	movq	56(%rsp), %rcx          # 8-byte Reload
	leal	2(%rdi,%rcx,2), %eax
	shrl	$2, %eax
	movw	%ax, (%r8)
	movq	32(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%rdx,2), %eax
	leal	2(%rcx,%rax), %r13d
	shrl	$2, %r13d
	movl	304(%rsp), %ebp
	jmp	.LBB3_40
.LBB3_41:                               #   in Loop: Header=BB3_4 Depth=1
	movq	%rdi, %r12
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	144(%rsp), %r14         # 8-byte Reload
	leal	1(%r14), %ecx
	movq	16(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%r14), %edx
	addl	%ebp, %edx
	movl	328(%rsp), %esi
	movl	%ebp, %ebx
	movl	%esi, %ebp
	testl	%ebp, %ebp
	cmovnel	%ecx, %edx
	movl	%edx, %esi
	negl	%esi
	movq	64(%rsp), %rdi          # 8-byte Reload
	leal	4(%rdi), %ecx
	movq	56(%rsp), %rax          # 8-byte Reload
	subl	%eax, %ecx
	leal	(%rcx,%r11,4), %ecx
	sarl	$3, %ecx
	cmpl	%esi, %ecx
	cmovll	%esi, %ecx
	cmpl	%edx, %ecx
	cmovgl	%edx, %ecx
	testl	%ebp, %ebp
	movl	%ebx, (%rsp)            # 4-byte Spill
	je	.LBB3_42
# BB#47:                                #   in Loop: Header=BB3_4 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	15524(%rax), %eax
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %rsi
	addl	%ecx, %esi
	movl	$0, %edx
	cmovsl	%edx, %esi
	cmpl	%eax, %esi
	cmovgew	%ax, %si
	movq	8(%rsp), %rdi           # 8-byte Reload
	movw	%si, (%rdi)
	subl	%ecx, %r12d
	cmovsl	%edx, %r12d
	cmpl	%eax, %r12d
	cmovgew	%ax, %r12w
	movw	%r12w, (%r8)
	jmp	.LBB3_48
.LBB3_25:                               #   in Loop: Header=BB3_4 Depth=1
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rsi, %r14
	sarl	$2, %r10d
	addl	$2, %r10d
	xorl	%ecx, %ecx
	cmpl	%r10d, %r12d
	setl	%cl
	andl	%ecx, %ebp
	movl	%ebp, (%rsp)            # 4-byte Spill
	movq	16(%rsp), %rdx          # 8-byte Reload
	andl	%ecx, %edx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	304(%rsp), %ebp
	je	.LBB3_27
# BB#26:                                #   in Loop: Header=BB3_4 Depth=1
	movq	56(%rsp), %rsi          # 8-byte Reload
	leal	(%rsi,%r9), %edi
	addl	%edi, %edi
	movl	$3, %ecx
	movl	$4, %r12d
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ebx
	movl	%r14d, %edx
	jmp	.LBB3_28
.LBB3_42:                               #   in Loop: Header=BB3_4 Depth=1
	movq	80(%rsp), %rdx          # 8-byte Reload
	movl	15520(%rdx), %edx
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%rsi, %rbp
	addl	%ecx, %ebp
	movl	$0, %esi
	cmovsl	%esi, %ebp
	cmpl	%edx, %ebp
	cmovgew	%dx, %bp
	movq	8(%rsp), %r11           # 8-byte Reload
	movw	%bp, (%r11)
	subl	%ecx, %r12d
	cmovsl	%esi, %r12d
	cmpl	%edx, %r12d
	cmovgew	%dx, %r12w
	movw	%r12w, (%r8)
	testl	%ebx, %ebx
	movl	304(%rsp), %ebp
	je	.LBB3_44
# BB#43:                                #   in Loop: Header=BB3_4 Depth=1
	movl	%r14d, %ecx
	negl	%ecx
	leal	1(%r9), %edx
	shrl	%edx
	addl	%edi, %edi
	subl	%edi, %edx
	addl	%r13d, %edx
	sarl	%edx
	cmpl	%ecx, %edx
	cmovll	%ecx, %edx
	cmpl	%r14d, %edx
	cmovgl	%r14d, %edx
	movq	112(%rsp), %rsi         # 8-byte Reload
	movzwl	(%r11,%rsi,2), %ecx
	addl	%edx, %ecx
	movw	%cx, (%r11,%rsi,2)
.LBB3_44:                               #   in Loop: Header=BB3_4 Depth=1
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB3_45
# BB#46:                                #   in Loop: Header=BB3_4 Depth=1
	movl	%r14d, %ecx
	negl	%ecx
	incl	%r9d
	shrl	%r9d
	addl	%eax, %eax
	subl	%eax, %r9d
	addl	40(%rsp), %r9d          # 4-byte Folded Reload
	sarl	%r9d
	cmpl	%ecx, %r9d
	cmovll	%ecx, %r9d
	cmpl	%r14d, %r9d
	cmovgl	%r14d, %r9d
	movq	72(%rsp), %rax          # 8-byte Reload
	movzwl	(%r8,%rax,2), %ecx
	addl	%r9d, %ecx
	movw	%cx, (%r8,%rax,2)
	jmp	.LBB3_49
.LBB3_27:                               #   in Loop: Header=BB3_4 Depth=1
	movq	56(%rsp), %rsi          # 8-byte Reload
	leal	(%rsi,%rsi), %ebx
	movl	$2, %r12d
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	%eax, %edx
	movl	$2, %ecx
.LBB3_28:                               #   in Loop: Header=BB3_4 Depth=1
	movq	72(%rsp), %r10          # 8-byte Reload
	addl	%ebx, %edi
	addl	%edx, %edi
	addl	%r12d, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edi
	movw	%di, (%r8)
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	je	.LBB3_30
# BB#29:                                #   in Loop: Header=BB3_4 Depth=1
	leal	(%rax,%r9), %edx
	addl	%edx, %edx
	movl	$3, %ecx
	movl	$4, %r12d
	movl	%esi, %edi
	movl	%r13d, %ebx
	jmp	.LBB3_31
.LBB3_45:                               #   in Loop: Header=BB3_4 Depth=1
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB3_49
.LBB3_30:                               #   in Loop: Header=BB3_4 Depth=1
	leal	(%rax,%rax), %edi
	movl	$2, %r12d
	movl	%esi, %ebx
	movl	$2, %ecx
	movq	32(%rsp), %rdx          # 8-byte Reload
.LBB3_31:                               #   in Loop: Header=BB3_4 Depth=1
	addl	%edi, %edx
	addl	%ebx, %edx
	addl	%r12d, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movw	%dx, (%rcx)
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	movl	%esi, %ecx
	je	.LBB3_33
# BB#32:                                #   in Loop: Header=BB3_4 Depth=1
	leal	(%r9,%rsi), %ecx
	leal	2(%r14,%rcx), %ecx
	shrl	$2, %ecx
.LBB3_33:                               #   in Loop: Header=BB3_4 Depth=1
	movw	%cx, (%r8,%r10,2)
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	movl	%eax, %ecx
	movq	112(%rsp), %rdx         # 8-byte Reload
	je	.LBB3_35
# BB#34:                                #   in Loop: Header=BB3_4 Depth=1
	leal	(%r9,%rax), %ecx
	leal	2(%r13,%rcx), %ecx
	shrl	$2, %ecx
.LBB3_35:                               #   in Loop: Header=BB3_4 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movw	%cx, (%rdi,%rdx,2)
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB3_37
# BB#36:                                #   in Loop: Header=BB3_4 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	addl	%r14d, %ecx
	addl	%r9d, %esi
	addl	%r14d, %esi
	leal	4(%rsi,%rcx,2), %r14d
	shrl	$3, %r14d
.LBB3_37:                               #   in Loop: Header=BB3_4 Depth=1
	movq	136(%rsp), %rcx         # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	(%rdx,%rcx,2), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	128(%rsp), %rcx         # 8-byte Reload
	movw	%r14w, (%r8,%rcx,2)
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	je	.LBB3_38
# BB#39:                                #   in Loop: Header=BB3_4 Depth=1
	movq	120(%rsp), %rcx         # 8-byte Reload
	addl	%r13d, %ecx
	addl	%r9d, %eax
	addl	%r13d, %eax
	leal	4(%rax,%rcx,2), %r13d
	shrl	$3, %r13d
	jmp	.LBB3_40
.LBB3_38:                               #   in Loop: Header=BB3_4 Depth=1
	movl	$0, (%rsp)              # 4-byte Folded Spill
.LBB3_40:                               #   in Loop: Header=BB3_4 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movw	%r13w, (%rax)
	jmp	.LBB3_49
	.p2align	4, 0x90
.LBB3_4:                                # =>This Inner Loop Header: Depth=1
	testl	%ebp, %ebp
	movl	108(%rsp), %r12d        # 4-byte Reload
	cmovnel	%r15d, %r12d
	movl	%r15d, %r13d
	cmovnel	104(%rsp), %r13d        # 4-byte Folded Reload
	movq	192(%rsp), %r14         # 8-byte Reload
	movl	%r14d, %edi
	movl	%r12d, %esi
	movl	%r13d, %edx
	movl	328(%rsp), %ebx
	movl	%ebx, %ecx
	leaq	224(%rsp), %r8
	callq	*getNeighbour(%rip)
	addl	100(%rsp), %r12d        # 4-byte Folded Reload
	subl	%ebp, %r13d
	movl	%r14d, %edi
	movl	%r12d, %esi
	movl	%r13d, %edx
	movl	%ebx, %ecx
	leaq	200(%rsp), %r8
	callq	*getNeighbour(%rip)
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	14224(%rax), %rbx
	movslq	204(%rsp), %r9
	imulq	$536, %r14, %rdi        # imm = 0x218
	movl	424(%rbx,%rdi), %ecx
	movb	$1, %al
	testl	%ecx, %ecx
	jne	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_4 Depth=1
	imulq	$536, %r9, %rax         # imm = 0x218
	cmpl	$0, 424(%rbx,%rax)
	setne	%al
.LBB3_6:                                #   in Loop: Header=BB3_4 Depth=1
	movb	%al, fieldModeFilteringFlag(%rip)
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movl	%r15d, %esi
	je	.LBB3_10
# BB#7:                                 #   in Loop: Header=BB3_4 Depth=1
	testl	%ecx, %ecx
	je	.LBB3_9
# BB#8:                                 #   in Loop: Header=BB3_4 Depth=1
	imulq	$536, %r9, %rsi         # imm = 0x218
	cmpl	$0, 424(%rbx,%rsi)
	movl	52(%rsp), %esi          # 4-byte Reload
	je	.LBB3_10
.LBB3_9:                                #   in Loop: Header=BB3_4 Depth=1
	movl	%r15d, %esi
	andl	$2147483646, %esi       # imm = 0x7FFFFFFE
	movl	%r15d, %ebp
	andl	$1, %ebp
	leal	(%rbp,%rsi,2), %esi
	movl	304(%rsp), %ebp
.LBB3_10:                               #   in Loop: Header=BB3_4 Depth=1
	cmpl	$0, 200(%rsp)
	je	.LBB3_11
.LBB3_12:                               #   in Loop: Header=BB3_4 Depth=1
	testl	%ebp, %ebp
	setne	%dl
	andb	%al, %dl
	cmpb	$1, %dl
	movl	88(%rsp), %edx          # 4-byte Reload
	movl	%edx, %r13d
	movl	%edx, %r8d
	jne	.LBB3_14
# BB#13:                                #   in Loop: Header=BB3_4 Depth=1
	testl	%ecx, %ecx
	sete	%cl
	movl	320(%rsp), %eax
	movl	%eax, %edx
	movl	%edx, %r13d
	shll	%cl, %r13d
	imulq	$536, %r9, %rcx         # imm = 0x218
	cmpl	$0, 424(%rbx,%rcx)
	sete	%cl
	movl	%edx, %r8d
	shll	%cl, %r8d
.LBB3_14:                               # %.thread
                                        #   in Loop: Header=BB3_4 Depth=1
	movslq	244(%rsp), %rcx
	movslq	240(%rsp), %rax
	addq	%rax, %rax
	movq	176(%rsp), %rdx         # 8-byte Reload
	addq	(%rdx,%rcx,8), %rax
	movslq	220(%rsp), %rcx
	movslq	216(%rsp), %rbp
	addq	%rbp, %rbp
	addq	(%rdx,%rcx,8), %rbp
	imulq	$536, %r9, %rcx         # imm = 0x218
	cmpl	$0, 328(%rsp)
	je	.LBB3_16
# BB#15:                                #   in Loop: Header=BB3_4 Depth=1
	addq	%rbx, %rcx
	addq	%rbx, %rdi
	movq	168(%rsp), %rdx         # 8-byte Reload
	movl	12(%rdi,%rdx,4), %edi
	addl	12(%rcx,%rdx,4), %edi
	jmp	.LBB3_17
	.p2align	4, 0x90
.LBB3_11:                               #   in Loop: Header=BB3_4 Depth=1
	cmpl	$0, 516(%rbx,%rdi)
	jne	.LBB3_49
	jmp	.LBB3_12
	.p2align	4, 0x90
.LBB3_16:                               #   in Loop: Header=BB3_4 Depth=1
	movl	8(%rbx,%rdi), %edi
	addl	8(%rbx,%rcx), %edi
.LBB3_17:                               #   in Loop: Header=BB3_4 Depth=1
	incl	%edi
	sarl	%edi
	movl	%edi, %edx
	addl	92(%rsp), %edx          # 4-byte Folded Reload
	movl	$0, %ecx
	cmovsl	%ecx, %edx
	cmpl	$52, %edx
	movl	$51, %ebx
	cmovgel	%ebx, %edx
	addl	96(%rsp), %edi          # 4-byte Folded Reload
	cmovsl	%ecx, %edi
	cmpl	$52, %edi
	cmovgel	%ebx, %edi
	movzbl	ALPHA_TABLE(%rdx), %r10d
	movl	28(%rsp), %ecx          # 4-byte Reload
	shll	%cl, %r10d
	movzbl	BETA_TABLE(%rdi), %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	movslq	%esi, %rcx
	movq	184(%rsp), %rsi         # 8-byte Reload
	movzbl	(%rsi,%rcx), %r14d
	testq	%r14, %r14
	je	.LBB3_48
# BB#18:                                #   in Loop: Header=BB3_4 Depth=1
	movzwl	(%rbp), %esi
	movzwl	(%rax), %ecx
	movl	%ecx, %r11d
	subl	%esi, %r11d
	movl	%r11d, %r12d
	negl	%r12d
	cmovll	%r11d, %r12d
	cmpl	%r10d, %r12d
	jge	.LBB3_48
# BB#19:                                #   in Loop: Header=BB3_4 Depth=1
	movl	%r8d, %r9d
	negl	%r9d
	movslq	%r9d, %rdi
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	movl	%r8d, 8(%rsp)           # 4-byte Spill
	movzwl	(%rbp,%rdi,2), %r8d
	movslq	%r13d, %rdi
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	%rcx, %r13
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	movzwl	(%rax,%rdi,2), %eax
	leaq	(%rdx,%rdx,4), %rcx
	movzbl	CLIP_TAB(%r14,%rcx), %edi
	movl	28(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	movq	%r13, 152(%rsp)         # 8-byte Spill
	movl	%r13d, %edx
	movq	%rax, %r13
	subl	%r13d, %edx
	movl	%edx, %ecx
	negl	%ecx
	cmovll	%edx, %ecx
	subl	%ebx, %ecx
	movq	%rsi, %rax
	movq	%r8, 64(%rsp)           # 8-byte Spill
	subl	%r8d, %esi
	movl	%esi, %edx
	negl	%edx
	cmovll	%esi, %edx
	subl	%ebx, %edx
	testl	%edx, %ecx
	js	.LBB3_20
.LBB3_48:                               #   in Loop: Header=BB3_4 Depth=1
	movl	304(%rsp), %ebp
.LBB3_49:                               #   in Loop: Header=BB3_4 Depth=1
	incl	%r15d
	addl	$2, 52(%rsp)            # 4-byte Folded Spill
	cmpl	%r15d, 24(%rsp)         # 4-byte Folded Reload
	jne	.LBB3_4
.LBB3_50:                               # %._crit_edge
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	EdgeLoop, .Lfunc_end3-EdgeLoop
	.cfi_endproc

	.type	.LDeblockMb.filterNon8x8LumaEdgesFlag,@object # @DeblockMb.filterNon8x8LumaEdgesFlag
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LDeblockMb.filterNon8x8LumaEdgesFlag:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.size	.LDeblockMb.filterNon8x8LumaEdgesFlag, 16

	.type	chroma_edge,@object     # @chroma_edge
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
chroma_edge:
	.asciz	"\377\000\000"
	.ascii	"\377\377\377\001"
	.ascii	"\377\001\001\002"
	.ascii	"\377\377\377\003"
	.asciz	"\377\000\000"
	.ascii	"\377\377\001\001"
	.ascii	"\377\001\002\002"
	.ascii	"\377\377\003\003"
	.size	chroma_edge, 32

	.type	mixedModeEdgeFlag,@object # @mixedModeEdgeFlag
	.comm	mixedModeEdgeFlag,1,1
	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	EdgeLoop.pelnum_cr,@object # @EdgeLoop.pelnum_cr
	.p2align	4
EdgeLoop.pelnum_cr:
	.long	0                       # 0x0
	.long	8                       # 0x8
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	0                       # 0x0
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	16                      # 0x10
	.size	EdgeLoop.pelnum_cr, 32

	.type	fieldModeFilteringFlag,@object # @fieldModeFilteringFlag
	.comm	fieldModeFilteringFlag,1,1
	.type	ALPHA_TABLE,@object     # @ALPHA_TABLE
	.section	.rodata,"a",@progbits
	.p2align	4
ALPHA_TABLE:
	.ascii	"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\004\004\005\006\007\b\t\n\f\r\017\021\024\026\031\034 $(-28?GPZeq\177\220\242\266\313\342\377\377"
	.size	ALPHA_TABLE, 52

	.type	BETA_TABLE,@object      # @BETA_TABLE
	.p2align	4
BETA_TABLE:
	.ascii	"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\002\002\002\003\003\003\003\004\004\004\006\006\007\007\b\b\t\t\n\n\013\013\f\f\r\r\016\016\017\017\020\020\021\021\022\022"
	.size	BETA_TABLE, 52

	.type	CLIP_TAB,@object        # @CLIP_TAB
	.p2align	4
CLIP_TAB:
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.ascii	"\000\000\000\001\001"
	.ascii	"\000\000\000\001\001"
	.ascii	"\000\000\000\001\001"
	.ascii	"\000\000\000\001\001"
	.ascii	"\000\000\001\001\001"
	.ascii	"\000\000\001\001\001"
	.ascii	"\000\001\001\001\001"
	.ascii	"\000\001\001\001\001"
	.ascii	"\000\001\001\001\001"
	.ascii	"\000\001\001\001\001"
	.ascii	"\000\001\001\002\002"
	.ascii	"\000\001\001\002\002"
	.ascii	"\000\001\001\002\002"
	.ascii	"\000\001\001\002\002"
	.ascii	"\000\001\002\003\003"
	.ascii	"\000\001\002\003\003"
	.ascii	"\000\002\002\003\003"
	.ascii	"\000\002\002\004\004"
	.ascii	"\000\002\003\004\004"
	.ascii	"\000\002\003\004\004"
	.ascii	"\000\003\003\005\005"
	.ascii	"\000\003\004\006\006"
	.ascii	"\000\003\004\006\006"
	.ascii	"\000\004\005\007\007"
	.ascii	"\000\004\005\b\b"
	.ascii	"\000\004\006\t\t"
	.ascii	"\000\005\007\n\n"
	.ascii	"\000\006\b\013\013"
	.ascii	"\000\006\b\r\r"
	.ascii	"\000\007\n\016\016"
	.ascii	"\000\b\013\020\020"
	.ascii	"\000\t\f\022\022"
	.ascii	"\000\n\r\024\024"
	.ascii	"\000\013\017\027\027"
	.ascii	"\000\r\021\031\031"
	.size	CLIP_TAB, 260

	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
