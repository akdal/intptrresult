	.text
	.file	"arithmetic.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	2                       # 0x2
	.quad	3                       # 0x3
.LCPI0_1:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI0_2:
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
.LCPI0_3:
	.quad	1                       # 0x1
	.quad	1                       # 0x1
.LCPI0_4:
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
.LCPI0_5:
	.long	255                     # 0xff
	.long	255                     # 0xff
	.long	255                     # 0xff
	.long	255                     # 0xff
.LCPI0_6:
	.quad	8                       # 0x8
	.quad	8                       # 0x8
.LCPI0_7:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI0_8:
	.quad	257                     # 0x101
	.quad	257                     # 0x101
.LCPI0_9:
	.quad	253                     # 0xfd
	.quad	253                     # 0xfd
.LCPI0_10:
	.quad	249                     # 0xf9
	.quad	249                     # 0xf9
.LCPI0_11:
	.quad	245                     # 0xf5
	.quad	245                     # 0xf5
.LCPI0_12:
	.quad	16                      # 0x10
	.quad	16                      # 0x10
	.text
	.globl	do_ari
	.p2align	4, 0x90
	.type	do_ari,@function
do_ari:                                 # @do_ari
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	%edi, %r15d
	movl	$0, rle_pos(%rip)
	movl	$0, ari_pos(%rip)
	movl	$1, %eax
	movd	%rax, %xmm5
	pslldq	$8, %xmm5               # xmm5 = zero,zero,zero,zero,zero,zero,zero,zero,xmm5[0,1,2,3,4,5,6,7]
	movdqa	.LCPI0_0(%rip), %xmm4   # xmm4 = [2,3]
	xorl	%eax, %eax
	movdqa	.LCPI0_1(%rip), %xmm8   # xmm8 = [0,1,2,3]
	movdqa	.LCPI0_2(%rip), %xmm9   # xmm9 = [4,5,6,7]
	movdqa	.LCPI0_3(%rip), %xmm11  # xmm11 = [1,1]
	movdqa	.LCPI0_4(%rip), %xmm10  # xmm10 = [5,5,5,5]
	movdqa	.LCPI0_5(%rip), %xmm6   # xmm6 = [255,255,255,255]
	movdqa	.LCPI0_6(%rip), %xmm7   # xmm7 = [8,8]
	.p2align	4, 0x90
.LBB0_1:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	%eax, %xmm2
	pshufd	$0, %xmm2, %xmm3        # xmm3 = xmm2[0,0,0,0]
	movdqa	%xmm3, %xmm2
	paddd	%xmm8, %xmm2
	paddd	%xmm9, %xmm3
	movdqa	%xmm5, %xmm0
	paddq	%xmm11, %xmm0
	movdqa	%xmm4, %xmm1
	paddq	%xmm11, %xmm1
	movd	%xmm0, %rcx
	shufps	$136, %xmm1, %xmm0      # xmm0 = xmm0[0,2],xmm1[0,2]
	movd	%ecx, %xmm1
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movdqa	%xmm5, %xmm1
	shufps	$136, %xmm4, %xmm1      # xmm1 = xmm1[0,2],xmm4[0,2]
	paddd	%xmm10, %xmm1
	movaps	%xmm0, char_to_index(,%rax,4)
	movdqa	%xmm1, char_to_index+16(,%rax,4)
	pand	%xmm6, %xmm2
	packuswb	%xmm2, %xmm2
	packuswb	%xmm2, %xmm2
	movd	%xmm2, index_to_char(%rcx)
	pand	%xmm6, %xmm3
	packuswb	%xmm3, %xmm3
	packuswb	%xmm3, %xmm3
	movd	%xmm3, index_to_char+4(%rcx)
	addq	$8, %rax
	paddq	%xmm7, %xmm5
	paddq	%xmm7, %xmm4
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB0_1
# BB#2:                                 # %vector.body26.preheader
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqa	.LCPI0_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-1024, %rax            # imm = 0xFC00
	movaps	.LCPI0_7(%rip), %xmm2   # xmm2 = [1,1,1,1]
	movdqa	.LCPI0_8(%rip), %xmm8   # xmm8 = [257,257]
	movdqa	.LCPI0_9(%rip), %xmm9   # xmm9 = [253,253]
	movdqa	.LCPI0_10(%rip), %xmm10 # xmm10 = [249,249]
	movdqa	.LCPI0_11(%rip), %xmm11 # xmm11 = [245,245]
	movdqa	.LCPI0_12(%rip), %xmm7  # xmm7 = [16,16]
	.p2align	4, 0x90
.LBB0_3:                                # %vector.body26
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm2, freq+1024(%rax)
	movaps	%xmm2, freq+1040(%rax)
	movdqa	%xmm8, %xmm3
	psubq	%xmm0, %xmm3
	movdqa	%xmm8, %xmm4
	psubq	%xmm1, %xmm4
	movdqa	%xmm9, %xmm5
	psubq	%xmm0, %xmm5
	movdqa	%xmm9, %xmm6
	psubq	%xmm1, %xmm6
	shufps	$136, %xmm4, %xmm3      # xmm3 = xmm3[0,2],xmm4[0,2]
	shufps	$136, %xmm6, %xmm5      # xmm5 = xmm5[0,2],xmm6[0,2]
	movaps	%xmm3, cum_freq+1024(%rax)
	movaps	%xmm5, cum_freq+1040(%rax)
	movaps	%xmm2, freq+1056(%rax)
	movaps	%xmm2, freq+1072(%rax)
	movdqa	%xmm10, %xmm3
	psubq	%xmm0, %xmm3
	movdqa	%xmm10, %xmm4
	psubq	%xmm1, %xmm4
	movdqa	%xmm11, %xmm5
	psubq	%xmm0, %xmm5
	movdqa	%xmm11, %xmm6
	psubq	%xmm1, %xmm6
	shufps	$136, %xmm4, %xmm3      # xmm3 = xmm3[0,2],xmm4[0,2]
	shufps	$136, %xmm6, %xmm5      # xmm5 = xmm5[0,2],xmm6[0,2]
	movaps	%xmm3, cum_freq+1056(%rax)
	movaps	%xmm5, cum_freq+1072(%rax)
	paddq	%xmm7, %xmm0
	paddq	%xmm7, %xmm1
	addq	$64, %rax
	jne	.LBB0_3
# BB#4:                                 # %.preheader.i
	movl	$1, freq+1024(%rip)
	movl	$1, cum_freq+1024(%rip)
	movl	$1, freq+1028(%rip)
	movl	$0, cum_freq+1028(%rip)
	movl	$0, freq(%rip)
	movl	$0, buffer(%rip)
	movl	$8, bits_to_go(%rip)
	movq	$0, low(%rip)
	movq	$65535, high(%rip)      # imm = 0xFFFF
	movq	$0, bits_to_follow(%rip)
	movq	rle(%rip), %rax
	movl	$1, rle_pos(%rip)
	testl	%r15d, %r15d
	je	.LBB0_16
# BB#5:                                 # %.critedge.preheader
	xorl	%ecx, %ecx
	movabsq	$4294967296, %r14       # imm = 0x100000000
	movabsq	$-4294967296, %rbp      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB0_7:                                # %.critedge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_9 Depth 2
                                        #     Child Loop BB0_11 Depth 2
                                        #     Child Loop BB0_15 Depth 2
	movl	%ecx, %ecx
	movzbl	(%rax,%rcx), %eax
	movl	char_to_index(,%rax,4), %r12d
	movslq	%r12d, %rbx
	movl	%ebx, %edi
	callq	encode_symbol
	cmpl	$16383, cum_freq(%rip)  # imm = 0x3FFF
	jne	.LBB0_10
# BB#8:                                 # %.preheader39.i.preheader
                                        #   in Loop: Header=BB0_7 Depth=1
	xorl	%ecx, %ecx
	movl	$259, %eax              # imm = 0x103
	.p2align	4, 0x90
.LBB0_9:                                # %.preheader39.i
                                        #   Parent Loop BB0_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	freq-8(,%rax,4), %edx
	leal	1(%rdx), %esi
	shrl	$31, %esi
	leal	1(%rdx,%rsi), %edx
	sarl	%edx
	movl	%edx, freq-8(,%rax,4)
	movl	%ecx, cum_freq-8(,%rax,4)
	addl	%ecx, %edx
	movl	freq-12(,%rax,4), %ecx
	leal	1(%rcx), %esi
	shrl	$31, %esi
	leal	1(%rcx,%rsi), %ecx
	sarl	%ecx
	movl	%ecx, freq-12(,%rax,4)
	movl	%edx, cum_freq-12(,%rax,4)
	addl	%edx, %ecx
	addq	$-2, %rax
	cmpq	$1, %rax
	jg	.LBB0_9
.LBB0_10:                               # %.preheader38.preheader.i
                                        #   in Loop: Header=BB0_7 Depth=1
	movl	freq(,%rbx,4), %edx
	movq	%rbx, %rsi
	shlq	$32, %rsi
	addq	%r14, %rsi
	leal	1(%rbx), %eax
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB0_11:                               # %.preheader38.i
                                        #   Parent Loop BB0_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdi, %rcx
	leaq	-1(%rcx), %rdi
	addq	%rbp, %rsi
	decl	%eax
	cmpl	freq-4(,%rcx,4), %edx
	je	.LBB0_11
# BB#12:                                #   in Loop: Header=BB0_7 Depth=1
	cmpl	%r12d, %eax
	jge	.LBB0_14
# BB#13:                                #   in Loop: Header=BB0_7 Depth=1
	movzbl	index_to_char+1(%rdi), %edx
	movzbl	index_to_char(%rbx), %esi
	movb	%sil, index_to_char+1(%rdi)
	movb	%dl, index_to_char(%rbx)
	movl	%r12d, char_to_index(,%rdx,4)
	movl	%eax, char_to_index(,%rsi,4)
.LBB0_14:                               # %.preheader.preheader.i
                                        #   in Loop: Header=BB0_7 Depth=1
	movslq	%ecx, %rax
	incq	%rax
	movl	$freq, %ecx
	.p2align	4, 0x90
.LBB0_15:                               # %.preheader.i10
                                        #   Parent Loop BB0_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	-4(%rcx,%rax,4)
	decq	%rax
	movl	$cum_freq, %ecx
	jg	.LBB0_15
# BB#6:                                 # %update_model.exit.loopexit
                                        #   in Loop: Header=BB0_7 Depth=1
	movq	rle(%rip), %rax
	movl	rle_pos(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, rle_pos(%rip)
	cmpl	%r15d, %edx
	jbe	.LBB0_7
.LBB0_16:                               # %update_model.exit._crit_edge
	movl	$257, %edi              # imm = 0x101
	callq	encode_symbol
	movq	bits_to_follow(%rip), %rdx
	incq	%rdx
	movq	%rdx, bits_to_follow(%rip)
	movl	buffer(%rip), %eax
	sarl	%eax
	cmpq	$16383, low(%rip)       # imm = 0x3FFF
	jg	.LBB0_23
# BB#17:
	movl	%eax, buffer(%rip)
	movl	bits_to_go(%rip), %ecx
	decl	%ecx
	movl	%ecx, bits_to_go(%rip)
	jne	.LBB0_19
# BB#18:
	movq	ari(%rip), %rcx
	movl	ari_pos(%rip), %edx
	leal	1(%rdx), %esi
	movl	%esi, ari_pos(%rip)
	movb	%al, (%rcx,%rdx)
	movl	$8, bits_to_go(%rip)
	movl	$8, %ecx
	movq	bits_to_follow(%rip), %rdx
.LBB0_19:                               # %output_bit.exitthread-pre-split.i.i
	testq	%rdx, %rdx
	jle	.LBB0_30
	.p2align	4, 0x90
.LBB0_20:                               # %.lr.ph.split.us.i.i
                                        # =>This Inner Loop Header: Depth=1
	sarl	%eax
	orl	$128, %eax
	decl	%ecx
	jne	.LBB0_22
# BB#21:                                #   in Loop: Header=BB0_20 Depth=1
	movq	ari(%rip), %rcx
	movl	ari_pos(%rip), %edx
	leal	1(%rdx), %esi
	movl	%esi, ari_pos(%rip)
	movb	%al, (%rcx,%rdx)
	movl	$8, %ecx
	movq	bits_to_follow(%rip), %rdx
.LBB0_22:                               # %output_bit.exit3.us.i.i
                                        #   in Loop: Header=BB0_20 Depth=1
	cmpq	$1, %rdx
	leaq	-1(%rdx), %rdx
	movq	%rdx, bits_to_follow(%rip)
	jg	.LBB0_20
	jmp	.LBB0_29
.LBB0_23:
	orl	$128, %eax
	movl	%eax, buffer(%rip)
	movl	bits_to_go(%rip), %ecx
	decl	%ecx
	movl	%ecx, bits_to_go(%rip)
	jne	.LBB0_25
# BB#24:
	movq	ari(%rip), %rcx
	movl	ari_pos(%rip), %edx
	leal	1(%rdx), %esi
	movl	%esi, ari_pos(%rip)
	movb	%al, (%rcx,%rdx)
	movl	$8, bits_to_go(%rip)
	movl	$8, %ecx
	movq	bits_to_follow(%rip), %rdx
.LBB0_25:                               # %output_bit.exitthread-pre-split.i3.i
	testq	%rdx, %rdx
	jle	.LBB0_30
	.p2align	4, 0x90
.LBB0_26:                               # %.lr.ph.split.i.i
                                        # =>This Inner Loop Header: Depth=1
	sarl	%eax
	decl	%ecx
	jne	.LBB0_28
# BB#27:                                #   in Loop: Header=BB0_26 Depth=1
	movq	ari(%rip), %rcx
	movl	ari_pos(%rip), %edx
	leal	1(%rdx), %esi
	movl	%esi, ari_pos(%rip)
	movb	%al, (%rcx,%rdx)
	movl	$8, %ecx
	movq	bits_to_follow(%rip), %rdx
.LBB0_28:                               # %output_bit.exit3.i.i
                                        #   in Loop: Header=BB0_26 Depth=1
	cmpq	$1, %rdx
	leaq	-1(%rdx), %rdx
	movq	%rdx, bits_to_follow(%rip)
	jg	.LBB0_26
.LBB0_29:                               # %output_bit.exit._crit_edge.i5.i
	movl	%eax, buffer(%rip)
	movl	%ecx, bits_to_go(%rip)
.LBB0_30:                               # %done_encoding.exit
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %eax
	movq	ari(%rip), %rcx
	movl	ari_pos(%rip), %edx
	leal	1(%rdx), %esi
	movl	%esi, ari_pos(%rip)
	movb	%al, (%rcx,%rdx)
	movl	ari_pos(%rip), %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	do_ari, .Lfunc_end0-do_ari
	.cfi_endproc

	.p2align	4, 0x90
	.type	encode_symbol,@function
encode_symbol:                          # @encode_symbol
	.cfi_startproc
# BB#0:
	movq	high(%rip), %rsi
	movl	$low, %ecx
	movq	low(%rip), %r8
	subq	%r8, %rsi
	incq	%rsi
	movslq	%edi, %rdi
	movslq	cum_freq-4(,%rdi,4), %rax
	imulq	%rsi, %rax
	movslq	cum_freq(%rip), %r9
	cqto
	idivq	%r9
	leaq	-1(%r8,%rax), %rax
	movq	%rax, high(%rip)
	movslq	cum_freq(,%rdi,4), %rax
	imulq	%rsi, %rax
	cqto
	idivq	%r9
	addq	%r8, %rax
	jmp	.LBB1_1
	.p2align	4, 0x90
.LBB1_8:                                # %output_bit.exit._crit_edge.i
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	%eax, buffer(%rip)
	movl	%ecx, bits_to_go(%rip)
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_9:                                #   in Loop: Header=BB1_1 Depth=1
	movq	low(%rip), %rcx
	cmpq	$32768, %rcx            # imm = 0x8000
	jl	.LBB1_18
# BB#10:                                #   in Loop: Header=BB1_1 Depth=1
	movl	buffer(%rip), %eax
	sarl	%eax
	orl	$128, %eax
	movl	%eax, buffer(%rip)
	movl	bits_to_go(%rip), %ecx
	decl	%ecx
	movl	%ecx, bits_to_go(%rip)
	jne	.LBB1_12
# BB#11:                                #   in Loop: Header=BB1_1 Depth=1
	movq	ari(%rip), %rcx
	movl	ari_pos(%rip), %edx
	leal	1(%rdx), %esi
	movl	%esi, ari_pos(%rip)
	movb	%al, (%rcx,%rdx)
	movl	$8, bits_to_go(%rip)
	movl	$8, %ecx
.LBB1_12:                               # %output_bit.exitthread-pre-split.i3
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	bits_to_follow(%rip), %rdx
	testq	%rdx, %rdx
	jle	.LBB1_17
	.p2align	4, 0x90
.LBB1_13:                               # %.lr.ph.split.i
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	sarl	%eax
	decl	%ecx
	jne	.LBB1_15
# BB#14:                                #   in Loop: Header=BB1_13 Depth=2
	movq	ari(%rip), %rcx
	movl	ari_pos(%rip), %edx
	leal	1(%rdx), %esi
	movl	%esi, ari_pos(%rip)
	movb	%al, (%rcx,%rdx)
	movl	$8, %ecx
	movq	bits_to_follow(%rip), %rdx
.LBB1_15:                               # %output_bit.exit3.i
                                        #   in Loop: Header=BB1_13 Depth=2
	cmpq	$1, %rdx
	leaq	-1(%rdx), %rdx
	movq	%rdx, bits_to_follow(%rip)
	jg	.LBB1_13
# BB#16:                                # %output_bit.exit._crit_edge.i5
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	%eax, buffer(%rip)
	movl	%ecx, bits_to_go(%rip)
.LBB1_17:                               # %bit_plus_follow.exit6
                                        #   in Loop: Header=BB1_1 Depth=1
	addq	$-32768, low(%rip)      # imm = 0x8000
	addq	$-32768, high(%rip)     # imm = 0x8000
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_18:                               #   in Loop: Header=BB1_1 Depth=1
	cmpq	$49151, %rax            # imm = 0xBFFF
	jg	.LBB1_22
# BB#19:                                #   in Loop: Header=BB1_1 Depth=1
	cmpq	$16384, %rcx            # imm = 0x4000
	jl	.LBB1_22
# BB#20:                                #   in Loop: Header=BB1_1 Depth=1
	incq	bits_to_follow(%rip)
	addq	$-16384, %rcx           # imm = 0xC000
	movq	%rcx, low(%rip)
	addq	$-16384, %rax           # imm = 0xC000
	movq	%rax, high(%rip)
.LBB1_21:                               # %bit_plus_follow.exit
                                        #   in Loop: Header=BB1_1 Depth=1
	shlq	low(%rip)
	movq	high(%rip), %rax
	leaq	1(%rax,%rax), %rax
	movl	$high, %ecx
.LBB1_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_13 Depth 2
                                        #     Child Loop BB1_5 Depth 2
	movq	%rax, (%rcx)
	movq	high(%rip), %rax
	cmpq	$32767, %rax            # imm = 0x7FFF
	jg	.LBB1_9
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	movl	buffer(%rip), %eax
	sarl	%eax
	movl	%eax, buffer(%rip)
	movl	bits_to_go(%rip), %ecx
	decl	%ecx
	movl	%ecx, bits_to_go(%rip)
	jne	.LBB1_4
# BB#3:                                 #   in Loop: Header=BB1_1 Depth=1
	movq	ari(%rip), %rcx
	movl	ari_pos(%rip), %edx
	leal	1(%rdx), %esi
	movl	%esi, ari_pos(%rip)
	movb	%al, (%rcx,%rdx)
	movl	$8, bits_to_go(%rip)
	movl	$8, %ecx
.LBB1_4:                                # %output_bit.exitthread-pre-split.i
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	bits_to_follow(%rip), %rdx
	testq	%rdx, %rdx
	jle	.LBB1_21
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph.split.us.i
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	sarl	%eax
	orl	$128, %eax
	decl	%ecx
	jne	.LBB1_7
# BB#6:                                 #   in Loop: Header=BB1_5 Depth=2
	movq	ari(%rip), %rcx
	movl	ari_pos(%rip), %edx
	leal	1(%rdx), %esi
	movl	%esi, ari_pos(%rip)
	movb	%al, (%rcx,%rdx)
	movl	$8, %ecx
	movq	bits_to_follow(%rip), %rdx
.LBB1_7:                                # %output_bit.exit3.us.i
                                        #   in Loop: Header=BB1_5 Depth=2
	cmpq	$1, %rdx
	leaq	-1(%rdx), %rdx
	movq	%rdx, bits_to_follow(%rip)
	jg	.LBB1_5
	jmp	.LBB1_8
.LBB1_22:
	retq
.Lfunc_end1:
	.size	encode_symbol, .Lfunc_end1-encode_symbol
	.cfi_endproc

	.type	rle_pos,@object         # @rle_pos
	.comm	rle_pos,4,4
	.type	ari_pos,@object         # @ari_pos
	.comm	ari_pos,4,4
	.type	char_to_index,@object   # @char_to_index
	.local	char_to_index
	.comm	char_to_index,1024,16
	.type	cum_freq,@object        # @cum_freq
	.local	cum_freq
	.comm	cum_freq,1032,16
	.type	low,@object             # @low
	.comm	low,8,8
	.type	high,@object            # @high
	.comm	high,8,8
	.type	bits_to_follow,@object  # @bits_to_follow
	.comm	bits_to_follow,8,8
	.type	buffer,@object          # @buffer
	.local	buffer
	.comm	buffer,4,4
	.type	bits_to_go,@object      # @bits_to_go
	.local	bits_to_go
	.comm	bits_to_go,4,4
	.type	index_to_char,@object   # @index_to_char
	.local	index_to_char
	.comm	index_to_char,258,16
	.type	freq,@object            # @freq
	.local	freq
	.comm	freq,1032,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
