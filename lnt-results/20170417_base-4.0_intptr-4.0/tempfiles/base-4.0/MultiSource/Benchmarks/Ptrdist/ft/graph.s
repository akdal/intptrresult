	.text
	.file	"graph.bc"
	.globl	GenGraph
	.p2align	4, 0x90
	.type	GenGraph,@function
GenGraph:                               # @GenGraph
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movl	%edi, %ebx
	callq	GenTree
	movq	%rax, %r15
	subl	%ebx, %r14d
	js	.LBB0_19
# BB#1:                                 # %Duplicate.exit.preheader.lr.ph.i
	movslq	%ebx, %r12
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_2:                                # %Duplicate.exit.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
                                        #     Child Loop BB0_7 Depth 2
                                        #     Child Loop BB0_10 Depth 2
                                        #     Child Loop BB0_13 Depth 2
                                        #     Child Loop BB0_17 Depth 2
	callq	random
	cqto
	idivq	%r12
	testl	%edx, %edx
	movq	%r15, %rbp
	jle	.LBB0_8
# BB#3:                                 # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	leal	-1(%rdx), %eax
	movl	%edx, %esi
	xorl	%ecx, %ecx
	movq	%r15, %rbp
	andl	$7, %esi
	je	.LBB0_5
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph.i.i.prol
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	incl	%ecx
	cmpl	%ecx, %esi
	jne	.LBB0_4
.LBB0_5:                                # %.lr.ph.i.i.prol.loopexit
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpl	$7, %eax
	jb	.LBB0_8
# BB#6:                                 # %.lr.ph.i.i.preheader.new
                                        #   in Loop: Header=BB0_2 Depth=1
	subl	%ecx, %edx
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph.i.i
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rbp
	addl	$-8, %edx
	jne	.LBB0_7
.LBB0_8:                                # %PickVertex.exit.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rbp), %rbx
	callq	random
	cqto
	idivq	%r12
	decl	%edx
	testl	%edx, %edx
	jle	.LBB0_14
# BB#9:                                 # %.lr.ph.i19.i.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	leal	-1(%rdx), %eax
	movl	%edx, %esi
	xorl	%ecx, %ecx
	andl	$7, %esi
	je	.LBB0_11
	.p2align	4, 0x90
.LBB0_10:                               # %.lr.ph.i19.i.prol
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	incl	%ecx
	cmpl	%ecx, %esi
	jne	.LBB0_10
.LBB0_11:                               # %.lr.ph.i19.i.prol.loopexit
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpl	$7, %eax
	jb	.LBB0_14
# BB#12:                                # %.lr.ph.i19.i.preheader.new
                                        #   in Loop: Header=BB0_2 Depth=1
	subl	%ecx, %edx
	.p2align	4, 0x90
.LBB0_13:                               # %.lr.ph.i19.i
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rbx
	addl	$-8, %edx
	jne	.LBB0_13
.LBB0_14:                               # %PickVertex.exit21.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	8(%rbp), %rax
	testq	%rax, %rax
	jne	.LBB0_17
	jmp	.LBB0_18
	.p2align	4, 0x90
.LBB0_15:                               #   in Loop: Header=BB0_17 Depth=2
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.LBB0_18
.LBB0_17:                               # %.lr.ph.i22.i
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbx, 16(%rax)
	jne	.LBB0_15
	jmp	.LBB0_2
	.p2align	4, 0x90
.LBB0_18:                               # %.loopexit.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	Connect
	cmpl	%r14d, %r13d
	leal	1(%r13), %eax
	movl	%eax, %r13d
	jne	.LBB0_2
.LBB0_19:                               # %AddEdges.exit
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	GenGraph, .Lfunc_end0-GenGraph
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.zero	16
	.text
	.globl	GenTree
	.p2align	4, 0x90
	.type	GenTree,@function
GenTree:                                # @GenTree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%edi, %ebx
	movl	$40, %edi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB1_15
# BB#1:                                 # %NewVertex.exit
	movl	id(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, id(%rip)
	movl	%eax, (%r14)
	movq	$0, 8(%r14)
	movq	%r14, 16(%r14)
	cmpl	$2, %ebx
	jl	.LBB1_14
# BB#2:                                 # %.lr.ph
	movslq	%ebx, %r13
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB1_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_8 Depth 2
                                        #     Child Loop BB1_11 Depth 2
	movl	$40, %edi
	callq	malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB1_15
# BB#4:                                 # %NewVertex.exit37
                                        #   in Loop: Header=BB1_3 Depth=1
	movl	id(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, id(%rip)
	movl	%eax, (%r15)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r15)
	movl	$32, %edi
	callq	malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB1_15
# BB#5:                                 # %NewEdge.exit
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	%r15, %rax
	addq	$8, %rax
	movl	$0, (%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r12)
	movq	%r12, (%rax)
	callq	random
	cqto
	idivq	%rbp
	testl	%edx, %edx
	jle	.LBB1_6
# BB#7:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB1_3 Depth=1
	leal	-1(%rdx), %ecx
	movl	%edx, %edi
	xorl	%esi, %esi
	movq	%r14, %rax
	andl	$7, %edi
	je	.LBB1_9
	.p2align	4, 0x90
.LBB1_8:                                # %.lr.ph.i.prol
                                        #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rax), %rax
	incl	%esi
	cmpl	%esi, %edi
	jne	.LBB1_8
.LBB1_9:                                # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB1_3 Depth=1
	cmpl	$7, %ecx
	jb	.LBB1_12
# BB#10:                                # %.lr.ph.i.preheader.new
                                        #   in Loop: Header=BB1_3 Depth=1
	subl	%esi, %edx
	.p2align	4, 0x90
.LBB1_11:                               # %.lr.ph.i
                                        #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	addl	$-8, %edx
	jne	.LBB1_11
	jmp	.LBB1_12
	.p2align	4, 0x90
.LBB1_6:                                #   in Loop: Header=BB1_3 Depth=1
	movq	%r14, %rax
.LBB1_12:                               # %PickVertex.exit
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	%rax, 16(%r12)
	callq	random
	movq	%rax, %rcx
	leaq	1(%rcx), %rbx
	movq	%rbx, %rax
	movabsq	$-6640827866535438581, %rdx # imm = 0xA3D70A3D70A3D70B
	imulq	%rdx
	leaq	1(%rdx,%rcx), %rax
	movq	%rax, %rcx
	shrq	$63, %rcx
	shrq	$6, %rax
	addl	%ecx, %eax
	imull	$100, %eax, %eax
	subl	%eax, %ebx
	movl	%ebx, (%r12)
	movq	%r15, 8(%r12)
	movq	16(%r14), %rax
	movq	%rax, 16(%r15)
	movq	%r15, 16(%r14)
	movl	$32, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB1_15
# BB#13:                                # %NewEdge.exit38
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	$0, 16(%rax)
	movl	%ebx, (%rax)
	movq	8(%r15), %rcx
	movq	16(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	%r15, 16(%rax)
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rdx
	movq	%rdx, 24(%rax)
	movq	%rax, 8(%rcx)
	incq	%rbp
	cmpq	%r13, %rbp
	jl	.LBB1_3
.LBB1_14:                               # %._crit_edge
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_15:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	GenTree, .Lfunc_end1-GenTree
	.cfi_endproc

	.globl	AddEdges
	.p2align	4, 0x90
	.type	AddEdges,@function
AddEdges:                               # @AddEdges
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 64
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rdi, %r15
	testl	%r14d, %r14d
	jle	.LBB2_19
# BB#1:                                 # %Duplicate.exit.preheader.lr.ph
	movslq	%esi, %r12
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_2:                                # %Duplicate.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_4 Depth 2
                                        #     Child Loop BB2_7 Depth 2
                                        #     Child Loop BB2_10 Depth 2
                                        #     Child Loop BB2_13 Depth 2
                                        #     Child Loop BB2_17 Depth 2
	callq	random
	cqto
	idivq	%r12
	testl	%edx, %edx
	movq	%r15, %rbp
	jle	.LBB2_8
# BB#3:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	leal	-1(%rdx), %eax
	movl	%edx, %esi
	xorl	%ecx, %ecx
	movq	%r15, %rbp
	andl	$7, %esi
	je	.LBB2_5
	.p2align	4, 0x90
.LBB2_4:                                # %.lr.ph.i.prol
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	incl	%ecx
	cmpl	%ecx, %esi
	jne	.LBB2_4
.LBB2_5:                                # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	cmpl	$7, %eax
	jb	.LBB2_8
# BB#6:                                 # %.lr.ph.i.preheader.new
                                        #   in Loop: Header=BB2_2 Depth=1
	subl	%ecx, %edx
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph.i
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rbp
	addl	$-8, %edx
	jne	.LBB2_7
.LBB2_8:                                # %PickVertex.exit
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	16(%rbp), %rbx
	callq	random
	cqto
	idivq	%r12
	decl	%edx
	testl	%edx, %edx
	jle	.LBB2_14
# BB#9:                                 # %.lr.ph.i19.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	leal	-1(%rdx), %eax
	movl	%edx, %esi
	xorl	%ecx, %ecx
	andl	$7, %esi
	je	.LBB2_11
	.p2align	4, 0x90
.LBB2_10:                               # %.lr.ph.i19.prol
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	incl	%ecx
	cmpl	%ecx, %esi
	jne	.LBB2_10
.LBB2_11:                               # %.lr.ph.i19.prol.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	cmpl	$7, %eax
	jb	.LBB2_14
# BB#12:                                # %.lr.ph.i19.preheader.new
                                        #   in Loop: Header=BB2_2 Depth=1
	subl	%ecx, %edx
	.p2align	4, 0x90
.LBB2_13:                               # %.lr.ph.i19
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rbx
	addl	$-8, %edx
	jne	.LBB2_13
.LBB2_14:                               # %PickVertex.exit21
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	8(%rbp), %rax
	testq	%rax, %rax
	jne	.LBB2_17
	jmp	.LBB2_18
	.p2align	4, 0x90
.LBB2_15:                               #   in Loop: Header=BB2_17 Depth=2
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.LBB2_18
.LBB2_17:                               # %.lr.ph.i22
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbx, 16(%rax)
	jne	.LBB2_15
	jmp	.LBB2_2
	.p2align	4, 0x90
.LBB2_18:                               # %.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	Connect
	incl	%r13d
	cmpl	%r14d, %r13d
	jne	.LBB2_2
.LBB2_19:                               # %._crit_edge
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	AddEdges, .Lfunc_end2-AddEdges
	.cfi_endproc

	.globl	PickVertex
	.p2align	4, 0x90
	.type	PickVertex,@function
PickVertex:                             # @PickVertex
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	testl	%esi, %esi
	jle	.LBB3_6
# BB#1:                                 # %.lr.ph.preheader
	leal	-1(%rsi), %eax
	movl	%esi, %edx
	xorl	%ecx, %ecx
	andl	$7, %edx
	je	.LBB3_3
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rdi), %rdi
	incl	%ecx
	cmpl	%ecx, %edx
	jne	.LBB3_2
.LBB3_3:                                # %.lr.ph.prol.loopexit
	cmpl	$7, %eax
	jb	.LBB3_6
# BB#4:                                 # %.lr.ph.preheader.new
	subl	%ecx, %esi
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rdi), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rdi
	addl	$-8, %esi
	jne	.LBB3_5
.LBB3_6:                                # %._crit_edge
	movq	%rdi, %rax
	retq
.Lfunc_end3:
	.size	PickVertex, .Lfunc_end3-PickVertex
	.cfi_endproc

	.globl	Duplicate
	.p2align	4, 0x90
	.type	Duplicate,@function
Duplicate:                              # @Duplicate
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	jne	.LBB4_3
	jmp	.LBB4_5
	.p2align	4, 0x90
.LBB4_1:                                #   in Loop: Header=BB4_3 Depth=1
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB4_5
.LBB4_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rsi, 16(%rcx)
	jne	.LBB4_1
# BB#4:
	movl	$1, %eax
.LBB4_5:                                # %._crit_edge
	retq
.Lfunc_end4:
	.size	Duplicate, .Lfunc_end4-Duplicate
	.cfi_endproc

	.globl	Connect
	.p2align	4, 0x90
	.type	Connect,@function
Connect:                                # @Connect
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 48
.Lcfi44:
	.cfi_offset %rbx, -48
.Lcfi45:
	.cfi_offset %r12, -40
.Lcfi46:
	.cfi_offset %r13, -32
.Lcfi47:
	.cfi_offset %r14, -24
.Lcfi48:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	callq	random
	movq	%rax, %r12
	leaq	1(%r12), %rbx
	movabsq	$-6640827866535438581, %rcx # imm = 0xA3D70A3D70A3D70B
	movq	%rbx, %rax
	imulq	%rcx
	movq	%rdx, %r13
	movl	$32, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB5_3
# BB#1:                                 # %NewEdge.exit
	leaq	1(%r13,%r12), %rcx
	movq	%rcx, %rdx
	shrq	$63, %rdx
	shrq	$6, %rcx
	addl	%edx, %ecx
	imull	$100, %ecx, %ecx
	subl	%ecx, %ebx
	movl	%ebx, (%rax)
	movq	%r15, 8(%rax)
	movq	%r14, 16(%rax)
	movq	8(%r15), %rcx
	movq	%rcx, 24(%rax)
	movq	%rax, 8(%r15)
	movl	$32, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %NewEdge.exit22
	movl	%ebx, (%rax)
	movq	%r14, 8(%rax)
	movq	%r15, 16(%rax)
	movq	8(%r14), %rcx
	movq	%rcx, 24(%rax)
	movq	%rax, 8(%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	Connect, .Lfunc_end5-Connect
	.cfi_endproc

	.globl	NewVertex
	.p2align	4, 0x90
	.type	NewVertex,@function
NewVertex:                              # @NewVertex
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi49:
	.cfi_def_cfa_offset 16
	movl	$40, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB6_2
# BB#1:
	movl	id(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, id(%rip)
	movl	%ecx, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rax)
	popq	%rcx
	retq
.LBB6_2:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	NewVertex, .Lfunc_end6-NewVertex
	.cfi_endproc

	.globl	NewEdge
	.p2align	4, 0x90
	.type	NewEdge,@function
NewEdge:                                # @NewEdge
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 16
	movl	$32, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB7_2
# BB#1:
	movl	$0, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	popq	%rcx
	retq
.LBB7_2:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end7:
	.size	NewEdge, .Lfunc_end7-NewEdge
	.cfi_endproc

	.globl	PrintGraph
	.p2align	4, 0x90
	.type	PrintGraph,@function
PrintGraph:                             # @PrintGraph
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 32
.Lcfi54:
	.cfi_offset %rbx, -32
.Lcfi55:
	.cfi_offset %r14, -24
.Lcfi56:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	%r14, %r15
	.p2align	4, 0x90
.LBB8_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_2 Depth 2
	movl	(%r15), %esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movq	8(%r15), %rbx
	testq	%rbx, %rbx
	je	.LBB8_4
	.p2align	4, 0x90
.LBB8_2:                                # %.lr.ph.i
                                        #   Parent Loop BB8_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rax
	movl	(%rax), %esi
	movl	(%rbx), %edx
	movq	8(%rbx), %rax
	movl	(%rax), %ecx
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	printf
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_2
.LBB8_4:                                # %PrintNeighbors.exit
                                        #   in Loop: Header=BB8_1 Depth=1
	movl	$10, %edi
	callq	putchar
	movq	16(%r15), %r15
	cmpq	%r14, %r15
	jne	.LBB8_1
# BB#5:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	PrintGraph, .Lfunc_end8-PrintGraph
	.cfi_endproc

	.globl	PrintNeighbors
	.p2align	4, 0x90
	.type	PrintNeighbors,@function
PrintNeighbors:                         # @PrintNeighbors
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 16
.Lcfi58:
	.cfi_offset %rbx, -16
	movq	8(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB9_3
	.p2align	4, 0x90
.LBB9_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rax
	movl	(%rax), %esi
	movl	(%rbx), %edx
	movq	8(%rbx), %rax
	movl	(%rax), %ecx
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	printf
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB9_1
.LBB9_3:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end9:
	.size	PrintNeighbors, .Lfunc_end9-PrintNeighbors
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Could not malloc\n"
	.size	.L.str, 18

	.type	id,@object              # @id
	.data
	.p2align	2
id:
	.long	1                       # 0x1
	.size	id, 4

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"Vertex %d is connected to:"
	.size	.L.str.1, 27

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	" %d(%d)[%d]"
	.size	.L.str.3, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
