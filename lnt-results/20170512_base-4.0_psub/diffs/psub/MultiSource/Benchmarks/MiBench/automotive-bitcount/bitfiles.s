	.text
	.file	"bitfiles.bc"
	.globl	bfopen
	.p2align	4, 0x90
	.type	bfopen,@function
bfopen:                                 # @bfopen
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_5
# BB#1:
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	fopen
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.LBB0_4
# BB#2:
	movb	$0, 9(%rbx)
	movb	$0, 11(%rbx)
	jmp	.LBB0_6
.LBB0_4:
	movq	%rbx, %rdi
	callq	free
.LBB0_5:
	xorl	%ebx, %ebx
.LBB0_6:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	bfopen, .Lfunc_end0-bfopen
	.cfi_endproc

	.globl	bfread
	.p2align	4, 0x90
	.type	bfread,@function
bfread:                                 # @bfread
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movb	9(%rbx), %cl
	testb	%cl, %cl
	je	.LBB1_2
# BB#1:                                 # %._crit_edge
	movb	8(%rbx), %al
	decb	%cl
	jmp	.LBB1_3
.LBB1_2:
	movq	(%rbx), %rdi
	callq	fgetc
	movb	%al, 8(%rbx)
	movb	$8, 9(%rbx)
	movb	$7, %cl
.LBB1_3:
	movb	%cl, 9(%rbx)
	movsbl	%al, %eax
	movzbl	%cl, %ecx
	btl	%ecx, %eax
	sbbl	%eax, %eax
	andl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	bfread, .Lfunc_end1-bfread
	.cfi_endproc

	.globl	bfwrite
	.p2align	4, 0x90
	.type	bfwrite,@function
bfwrite:                                # @bfwrite
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movb	11(%rbx), %al
	cmpb	$8, %al
	jne	.LBB2_1
# BB#2:
	movsbl	10(%rbx), %edi
	movq	(%rbx), %rsi
	callq	fputc
	movb	$0, 11(%rbx)
	movb	$1, %al
	jmp	.LBB2_3
.LBB2_1:                                # %._crit_edge
	incb	%al
.LBB2_3:
	movb	%al, 11(%rbx)
	movb	10(%rbx), %al
	addb	%al, %al
	andl	$1, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movb	%al, 10(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end2:
	.size	bfwrite, .Lfunc_end2-bfwrite
	.cfi_endproc

	.globl	bfclose
	.p2align	4, 0x90
	.type	bfclose,@function
bfclose:                                # @bfclose
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	callq	fclose
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end3:
	.size	bfclose, .Lfunc_end3-bfclose
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
