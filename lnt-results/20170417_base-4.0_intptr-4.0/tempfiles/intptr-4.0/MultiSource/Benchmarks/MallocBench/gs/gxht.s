	.text
	.file	"gxht.bc"
	.globl	gx_ht_construct_order
	.p2align	4, 0x90
	.type	gx_ht_construct_order,@function
gx_ht_construct_order:                  # @gx_ht_construct_order
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	%esi, %r9d
	negl	%r9d
	andl	$31, %r9d
	leal	(%r9,%rsi), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%eax, %ecx
	sarl	$3, %ecx
	imull	%edx, %ecx
	movl	$-13, %r8d
	cmpl	$1000, %ecx             # imm = 0x3E8
	jg	.LBB0_8
# BB#1:
	imull	%esi, %edx
	movq	$0, cache+16(%rip)
	movq	$cache_bits, cache(%rip)
	movl	$1000, cache+8(%rip)    # imm = 0x3E8
	testl	%edx, %edx
	je	.LBB0_2
# BB#3:                                 # %.lr.ph
	cmpl	$9, %esi
	jge	.LBB0_4
# BB#6:                                 # %.lr.ph.split.us.preheader
	movslq	%esi, %rax
	movq	multi_bits(,%rax,8), %r10
	movl	%edx, %ecx
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rdi), %r11d
	xorl	%edx, %edx
	movl	%r11d, %eax
	idivl	%esi
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	imull	%r9d, %eax
	addl	%r11d, %eax
	movl	%eax, %edx
	shrl	$3, %edx
	andl	$65534, %edx            # imm = 0xFFFE
	movw	%dx, (%rdi)
	andl	$15, %eax
	movzwl	(%r10,%rax,2), %eax
	movw	%ax, 2(%rdi)
	addq	$4, %rdi
	decq	%rcx
	jne	.LBB0_7
	jmp	.LBB0_8
.LBB0_2:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	retq
.LBB0_4:                                # %.lr.ph.split.preheader
	movl	%edx, %ecx
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rdi), %r10d
	xorl	%edx, %edx
	movl	%r10d, %eax
	idivl	%esi
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	imull	%r9d, %eax
	addl	%r10d, %eax
	movl	%eax, %edx
	shrl	$3, %edx
	andl	$65534, %edx            # imm = 0xFFFE
	movw	%dx, (%rdi)
	andl	$15, %eax
	movzwl	single_bits8(%rax,%rax), %eax
	movw	%ax, 2(%rdi)
	addq	$4, %rdi
	decq	%rcx
	jne	.LBB0_5
.LBB0_8:                                # %.loopexit
	movl	%r8d, %eax
	retq
.Lfunc_end0:
	.size	gx_ht_construct_order, .Lfunc_end0-gx_ht_construct_order
	.cfi_endproc

	.globl	gx_color_load
	.p2align	4, 0x90
	.type	gx_color_load,@function
gx_color_load:                          # @gx_color_load
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	16(%r14), %r15d
	testl	%r15d, %r15d
	je	.LBB1_22
# BB#1:
	movq	288(%rsi), %r8
	movq	16(%r8), %r12
	cmpq	%r12, cache+16(%rip)
	je	.LBB1_2
# BB#3:
	movslq	8(%r8), %rax
	movl	12(%r8), %edi
	movl	%edi, %r9d
	imull	%eax, %r9d
	cmpq	$8, %rax
	movl	%eax, %r11d
	jg	.LBB1_5
# BB#4:
	movl	init_ht.up_to_16(,%rax,4), %r11d
.LBB1_5:
	leal	31(%rax), %ebx
	sarl	$5, %ebx
	shll	$2, %ebx
	movl	%ebx, %ecx
	imull	%edi, %ecx
	movl	$1000, %eax             # imm = 0x3E8
	xorl	%edx, %edx
	divl	%ecx
	cmpl	%r9d, %eax
	cmovgl	%r9d, %eax
	cmpl	$26, %eax
	movl	$25, %r10d
	cmovll	%eax, %r10d
	testl	%eax, %eax
	jle	.LBB1_8
# BB#6:                                 # %.lr.ph.i
	movq	cache(%rip), %rax
	movl	%ecx, %edx
	movslq	%r10d, %rcx
	xorl	%esi, %esi
	movl	$cache+56, %ebp
	.p2align	4, 0x90
.LBB1_7:                                # =>This Inner Loop Header: Depth=1
	movl	$-1, -24(%rbp)
	movq	%rax, -16(%rbp)
	movl	%ebx, -8(%rbp)
	movl	%r11d, -4(%rbp)
	movl	%edi, (%rbp)
	addq	%rdx, %rax
	incq	%rsi
	addq	$32, %rbp
	cmpq	%rcx, %rsi
	jl	.LBB1_7
.LBB1_8:                                # %init_ht.exit
	movq	%r12, cache+16(%rip)
	movl	%r10d, cache+24(%rip)
	leal	-1(%r9,%r10), %eax
	cltd
	idivl	%r10d
	movl	%eax, %ecx
	movl	%ecx, cache+28(%rip)
	jmp	.LBB1_9
.LBB1_2:                                # %._crit_edge
	movl	cache+28(%rip), %ecx
.LBB1_9:
	movl	%r15d, %eax
	cltd
	idivl	%ecx
	movslq	%eax, %r13
	shlq	$5, %r13
	movl	cache+32(%r13), %ebp
	cmpl	%r15d, %ebp
	je	.LBB1_21
# BB#10:
	movq	cache+40(%r13), %rbx
	testl	%ebp, %ebp
	jns	.LBB1_15
# BB#11:
	movl	cache+56(%r13), %edx
	imull	cache+48(%r13), %edx
	movl	24(%r8), %ebp
	movl	%ebp, %eax
	sarl	%eax
	cmpl	%r15d, %eax
	jle	.LBB1_12
# BB#13:
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	jmp	.LBB1_14
.LBB1_12:
	movl	$255, %esi
.LBB1_14:                               # %.preheader.i
	movq	%rbx, %rdi
	callq	memset
.LBB1_15:                               # %.preheader.i
	cmpl	%r15d, %ebp
	movl	%r15d, %eax
	cmovlel	%ebp, %eax
	cmovll	%r15d, %ebp
	movslq	%eax, %rdx
	leaq	(%r12,%rdx,4), %rax
	movslq	%ebp, %rcx
	leaq	(%r12,%rcx,4), %rcx
	leaq	4(%r12,%rdx,4), %rsi
	shlq	$2, %rdx
	cmpq	%rcx, %rsi
	cmovbeq	%rcx, %rsi
	subq	%rdx, %rsi
	notq	%r12
	addq	%rsi, %r12
	movl	%r12d, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB1_18
# BB#16:                                # %.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB1_17:                               # =>This Inner Loop Header: Depth=1
	movzwl	2(%rax), %esi
	movzwl	(%rax), %edi
	xorw	%si, (%rbx,%rdi)
	addq	$4, %rax
	incq	%rdx
	jne	.LBB1_17
.LBB1_18:                               # %.prol.loopexit
	leaq	cache+32(%r13), %rdx
	cmpq	$12, %r12
	jb	.LBB1_20
	.p2align	4, 0x90
.LBB1_19:                               # =>This Inner Loop Header: Depth=1
	movzwl	2(%rax), %esi
	movzwl	(%rax), %edi
	xorw	%si, (%rbx,%rdi)
	movzwl	6(%rax), %esi
	movzwl	4(%rax), %edi
	xorw	%si, (%rbx,%rdi)
	movzwl	10(%rax), %esi
	movzwl	8(%rax), %edi
	xorw	%si, (%rbx,%rdi)
	movzwl	14(%rax), %esi
	movzwl	12(%rax), %edi
	xorw	%si, (%rbx,%rdi)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jb	.LBB1_19
.LBB1_20:                               # %render_ht.exit
	movl	%r15d, (%rdx)
.LBB1_21:
	leaq	cache+40(%r13), %rax
	movq	%rax, 24(%r14)
.LBB1_22:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	gx_color_load, .Lfunc_end1-gx_color_load
	.cfi_endproc

	.globl	init_ht
	.p2align	4, 0x90
	.type	init_ht,@function
init_ht:                                # @init_ht
	.cfi_startproc
# BB#0:
	movslq	8(%rsi), %rax
	movl	12(%rsi), %r10d
	movl	%r10d, %r8d
	imull	%eax, %r8d
	cmpq	$8, %rax
	movl	%eax, %r9d
	jg	.LBB2_2
# BB#1:
	movl	init_ht.up_to_16(,%rax,4), %r9d
.LBB2_2:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	leal	31(%rax), %r14d
	sarl	$5, %r14d
	shll	$2, %r14d
	movl	%r14d, %ecx
	imull	%r10d, %ecx
	movl	$1000, %eax             # imm = 0x3E8
	xorl	%edx, %edx
	divl	%ecx
	cmpl	%r8d, %eax
	cmovgl	%r8d, %eax
	cmpl	$26, %eax
	movl	$25, %r11d
	cmovll	%eax, %r11d
	testl	%eax, %eax
	jle	.LBB2_5
# BB#3:                                 # %.lr.ph
	movq	(%rdi), %rax
	movl	%ecx, %edx
	movslq	%r11d, %rbx
	leaq	56(%rdi), %rcx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_4:                                # =>This Inner Loop Header: Depth=1
	movl	$-1, -24(%rcx)
	movq	%rax, -16(%rcx)
	movl	%r14d, -8(%rcx)
	movl	%r9d, -4(%rcx)
	movl	%r10d, (%rcx)
	addq	%rdx, %rax
	incq	%rbp
	addq	$32, %rcx
	cmpq	%rbx, %rbp
	jl	.LBB2_4
.LBB2_5:                                # %._crit_edge
	movq	16(%rsi), %rax
	movq	%rax, 16(%rdi)
	movl	%r11d, 24(%rdi)
	leal	-1(%r8,%r11), %eax
	cltd
	idivl	%r11d
	movl	%eax, 28(%rdi)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	init_ht, .Lfunc_end2-init_ht
	.cfi_endproc

	.globl	render_ht
	.p2align	4, 0x90
	.type	render_ht,@function
render_ht:                              # @render_ht
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 48
.Lcfi24:
	.cfi_offset %rbx, -48
.Lcfi25:
	.cfi_offset %r12, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rdx, %rax
	movl	%esi, %r15d
	movq	%rdi, %r14
	movq	16(%rax), %r12
	movq	8(%r14), %rbx
	movl	(%r14), %ebp
	testl	%ebp, %ebp
	jns	.LBB3_5
# BB#1:
	movl	24(%r14), %edx
	imull	16(%r14), %edx
	movl	24(%rax), %ebp
	movl	%ebp, %eax
	sarl	%eax
	cmpl	%r15d, %eax
	jle	.LBB3_2
# BB#3:
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	jmp	.LBB3_4
.LBB3_2:
	movl	$255, %esi
.LBB3_4:                                # %.preheader
	movq	%rbx, %rdi
	callq	memset
.LBB3_5:                                # %.preheader
	cmpl	%r15d, %ebp
	movl	%r15d, %eax
	cmovlel	%ebp, %eax
	cmovll	%r15d, %ebp
	movslq	%eax, %rdx
	leaq	(%r12,%rdx,4), %rax
	movslq	%ebp, %rcx
	leaq	(%r12,%rcx,4), %rcx
	leaq	4(%r12,%rdx,4), %rsi
	shlq	$2, %rdx
	cmpq	%rcx, %rsi
	cmovbeq	%rcx, %rsi
	subq	%rdx, %rsi
	notq	%r12
	addq	%rsi, %r12
	movl	%r12d, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB3_8
# BB#6:                                 # %.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB3_7:                                # =>This Inner Loop Header: Depth=1
	movzwl	2(%rax), %esi
	movzwl	(%rax), %edi
	xorw	%si, (%rbx,%rdi)
	addq	$4, %rax
	incq	%rdx
	jne	.LBB3_7
.LBB3_8:                                # %.prol.loopexit
	cmpq	$12, %r12
	jb	.LBB3_10
	.p2align	4, 0x90
.LBB3_9:                                # =>This Inner Loop Header: Depth=1
	movzwl	2(%rax), %edx
	movzwl	(%rax), %esi
	xorw	%dx, (%rbx,%rsi)
	movzwl	6(%rax), %edx
	movzwl	4(%rax), %esi
	xorw	%dx, (%rbx,%rsi)
	movzwl	10(%rax), %edx
	movzwl	8(%rax), %esi
	xorw	%dx, (%rbx,%rsi)
	movzwl	14(%rax), %edx
	movzwl	12(%rax), %esi
	xorw	%dx, (%rbx,%rsi)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jb	.LBB3_9
.LBB3_10:
	movl	%r15d, (%r14)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	render_ht, .Lfunc_end3-render_ht
	.cfi_endproc

	.type	single_bits8,@object    # @single_bits8
	.data
	.globl	single_bits8
	.p2align	4
single_bits8:
	.ascii	"\200\000@\000 \000\020\000\b\000\004\000\002\000\001\000\000\200\000@\000 \000\020\000\b\000\004\000\002\000\001"
	.size	single_bits8, 32

	.type	mb1,@object             # @mb1
	.globl	mb1
mb1:
	.zero	2,255
	.size	mb1, 2

	.type	mb2,@object             # @mb2
	.globl	mb2
mb2:
	.ascii	"\252\252UU"
	.size	mb2, 4

	.type	mb3,@object             # @mb3
	.globl	mb3
mb3:
	.ascii	"\222II$$\222"
	.size	mb3, 6

	.type	mb4,@object             # @mb4
	.globl	mb4
mb4:
	.ascii	"\210\210DD\"\"\021\021"
	.size	mb4, 8

	.type	mb5,@object             # @mb5
	.globl	mb5
mb5:
	.ascii	"\204!B\020!\b\020\204\bB"
	.size	mb5, 10

	.type	mb6,@object             # @mb6
	.globl	mb6
mb6:
	.ascii	"\202\bA\004 \202\020A\b \004\020"
	.size	mb6, 12

	.type	mb7,@object             # @mb7
	.globl	mb7
mb7:
	.ascii	"\201\002@\201 @\020 \b\020\004\b\002\004"
	.size	mb7, 14

	.type	mb8,@object             # @mb8
	.globl	mb8
	.p2align	4
mb8:
	.ascii	"\200\200@@  \020\020\b\b\004\004\002\002\001\001"
	.size	mb8, 16

	.type	multi_bits,@object      # @multi_bits
	.globl	multi_bits
	.p2align	4
multi_bits:
	.quad	0
	.quad	mb1
	.quad	mb2
	.quad	mb3
	.quad	mb4
	.quad	mb5
	.quad	mb6
	.quad	mb7
	.quad	mb8
	.size	multi_bits, 72

	.type	cache,@object           # @cache
	.comm	cache,832,8
	.type	cache_bits,@object      # @cache_bits
	.comm	cache_bits,1000,16
	.type	init_ht.up_to_16,@object # @init_ht.up_to_16
	.section	.rodata,"a",@progbits
	.p2align	4
init_ht.up_to_16:
	.long	0                       # 0x0
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	15                      # 0xf
	.long	12                      # 0xc
	.long	14                      # 0xe
	.long	16                      # 0x10
	.size	init_ht.up_to_16, 36


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
