	.text
	.file	"contain.bc"
	.globl	sf_contain
	.p2align	4, 0x90
	.type	sf_contain,@function
sf_contain:                             # @sf_contain
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r13, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movslq	12(%r15), %rbx
	leaq	8(,%rbx,8), %rdi
	callq	malloc
	movq	%rax, %r14
	movslq	(%r15), %rax
	imulq	%rbx, %rax
	testl	%eax, %eax
	jle	.LBB0_1
# BB#2:                                 # %.lr.ph.i.preheader
	movq	24(%r15), %rbx
	leaq	(%rbx,%rax,4), %r13
	movq	%r14, %r12
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rbx), %eax
	movl	%eax, (%rbx)
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	set_ord
	shll	$16, %eax
	orl	%eax, (%rbx)
	movq	%rbx, (%r12)
	addq	$8, %r12
	movslq	(%r15), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r13, %rbx
	jb	.LBB0_3
# BB#4:                                 # %._crit_edge.loopexit.i
	movl	12(%r15), %ebx
	jmp	.LBB0_5
.LBB0_1:
	movq	%r14, %r12
.LBB0_5:                                # %sf_sort.exit
	movq	$0, (%r12)
	movslq	%ebx, %rsi
	movl	$8, %edx
	movl	$descend, %ecx
	movq	%r14, %rdi
	callq	qsort
	cmpq	$0, (%r14)
	je	.LBB0_12
# BB#6:                                 # %.outer.i.preheader
	movq	%r14, %rbx
	movq	%r14, %r12
	jmp	.LBB0_7
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_7 Depth=1
	movq	-8(%rbx), %rax
	movq	%rax, (%r12)
	addq	$8, %r12
.LBB0_7:                                # %.outer.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_8 Depth 2
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB0_8:                                #   Parent Loop BB0_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$0, 8(%rsi)
	je	.LBB0_11
# BB#9:                                 #   in Loop: Header=BB0_8 Depth=2
	leaq	8(%rsi), %rbx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	descend
	testl	%eax, %eax
	movq	%rbx, %rsi
	je	.LBB0_8
	jmp	.LBB0_10
.LBB0_11:                               # %rm_equal.exit
	movq	(%rsi), %rax
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.LBB0_12
# BB#21:                                # %.lr.ph.i12.preheader
	movl	$-1, %r9d
	movq	%r14, %r8
	movq	%r14, %r13
                                        # implicit-def: %R10
	.p2align	4, 0x90
.LBB0_24:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_25 Depth 2
                                        #       Child Loop BB0_27 Depth 3
	movl	%r9d, %ecx
	addq	$8, %r8
	movl	(%rax), %edi
	movl	%edi, %r9d
	shrl	$16, %r9d
	cmpl	%ecx, %r9d
	cmovneq	%r13, %r10
	andl	$1023, %edi             # imm = 0x3FF
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB0_25:                               #   Parent Loop BB0_24 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_27 Depth 3
	cmpq	%r10, %rcx
	je	.LBB0_22
# BB#26:                                #   in Loop: Header=BB0_25 Depth=2
	movq	(%rcx), %rbx
	addq	$8, %rcx
	movq	%rdi, %rsi
	.p2align	4, 0x90
.LBB0_27:                               #   Parent Loop BB0_24 Depth=1
                                        #     Parent Loop BB0_25 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rbx,%rsi,4), %edx
	notl	%edx
	testl	(%rax,%rsi,4), %edx
	jne	.LBB0_29
# BB#28:                                #   in Loop: Header=BB0_27 Depth=3
	leaq	-1(%rsi), %rdx
	cmpq	$1, %rsi
	movq	%rdx, %rsi
	jg	.LBB0_27
	jmp	.LBB0_30
	.p2align	4, 0x90
.LBB0_29:                               # %._crit_edge
                                        #   in Loop: Header=BB0_25 Depth=2
	movl	%esi, %edx
.LBB0_30:                               #   in Loop: Header=BB0_25 Depth=2
	testl	%edx, %edx
	jne	.LBB0_25
	jmp	.LBB0_23
	.p2align	4, 0x90
.LBB0_22:                               # %.outer.i13
                                        #   in Loop: Header=BB0_24 Depth=1
	movq	%rax, (%r13)
	addq	$8, %r13
.LBB0_23:                               # %.loopexit.i
                                        #   in Loop: Header=BB0_24 Depth=1
	movq	(%r8), %rax
	testq	%rax, %rax
	jne	.LBB0_24
	jmp	.LBB0_13
.LBB0_12:
	movq	%r14, %r13
.LBB0_13:                               # %rm_contain.exit
	movq	$0, (%r13)
	subq	%r14, %r13
	shrq	$3, %r13
	movl	4(%r15), %esi
	xorl	%eax, %eax
	movl	%r13d, %edi
	callq	sf_new
	movq	%rax, %r12
	movl	%r13d, 12(%r12)
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.LBB0_43
# BB#14:                                # %.lr.ph.i14
	movq	24(%r12), %rcx
	movq	%r14, %r11
	.p2align	4, 0x90
.LBB0_15:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_32 Depth 2
                                        #     Child Loop BB0_36 Depth 2
                                        #     Child Loop BB0_40 Depth 2
	movl	(%rax), %esi
	andl	$1023, %esi             # imm = 0x3FF
	leaq	1(%rsi), %r10
	cmpq	$8, %r10
	jb	.LBB0_39
# BB#16:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_15 Depth=1
	movq	%r10, %r8
	andq	$2040, %r8              # imm = 0x7F8
	je	.LBB0_39
# BB#17:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_15 Depth=1
	leaq	4(%rax,%rsi,4), %rdx
	cmpq	%rdx, %rcx
	jae	.LBB0_19
# BB#18:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_15 Depth=1
	leaq	4(%rcx,%rsi,4), %rdx
	cmpq	%rdx, %rax
	jb	.LBB0_39
.LBB0_19:                               # %vector.body.preheader
                                        #   in Loop: Header=BB0_15 Depth=1
	leaq	-8(%r8), %r9
	movl	%r9d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB0_20
# BB#31:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB0_15 Depth=1
	leaq	-12(%rcx,%rsi,4), %r13
	leaq	-12(%rax,%rsi,4), %rdx
	negq	%rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_32:                               # %vector.body.prol
                                        #   Parent Loop BB0_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdx,%rbx,4), %xmm0
	movups	-16(%rdx,%rbx,4), %xmm1
	movups	%xmm0, (%r13,%rbx,4)
	movups	%xmm1, -16(%r13,%rbx,4)
	addq	$-8, %rbx
	incq	%rdi
	jne	.LBB0_32
# BB#33:                                # %vector.body.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB0_15 Depth=1
	negq	%rbx
	cmpq	$24, %r9
	jae	.LBB0_35
	jmp	.LBB0_37
.LBB0_20:                               #   in Loop: Header=BB0_15 Depth=1
	xorl	%ebx, %ebx
	cmpq	$24, %r9
	jb	.LBB0_37
.LBB0_35:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB0_15 Depth=1
	movq	%r10, %r9
	andq	$-8, %r9
	negq	%r9
	negq	%rbx
	leaq	-12(%rcx,%rsi,4), %rdx
	leaq	-12(%rax,%rsi,4), %rdi
	.p2align	4, 0x90
.LBB0_36:                               # %vector.body
                                        #   Parent Loop BB0_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rbx,4), %xmm0
	movups	-16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%rdx,%rbx,4)
	movups	%xmm1, -16(%rdx,%rbx,4)
	movups	-32(%rdi,%rbx,4), %xmm0
	movups	-48(%rdi,%rbx,4), %xmm1
	movups	%xmm0, -32(%rdx,%rbx,4)
	movups	%xmm1, -48(%rdx,%rbx,4)
	movups	-64(%rdi,%rbx,4), %xmm0
	movups	-80(%rdi,%rbx,4), %xmm1
	movups	%xmm0, -64(%rdx,%rbx,4)
	movups	%xmm1, -80(%rdx,%rbx,4)
	movups	-96(%rdi,%rbx,4), %xmm0
	movups	-112(%rdi,%rbx,4), %xmm1
	movups	%xmm0, -96(%rdx,%rbx,4)
	movups	%xmm1, -112(%rdx,%rbx,4)
	addq	$-32, %rbx
	cmpq	%rbx, %r9
	jne	.LBB0_36
.LBB0_37:                               # %middle.block
                                        #   in Loop: Header=BB0_15 Depth=1
	cmpq	%r8, %r10
	je	.LBB0_41
# BB#38:                                #   in Loop: Header=BB0_15 Depth=1
	subq	%r8, %rsi
	.p2align	4, 0x90
.LBB0_39:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_15 Depth=1
	incq	%rsi
	.p2align	4, 0x90
.LBB0_40:                               # %scalar.ph
                                        #   Parent Loop BB0_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rax,%rsi,4), %edx
	movl	%edx, -4(%rcx,%rsi,4)
	decq	%rsi
	jg	.LBB0_40
.LBB0_41:                               # %.loopexit
                                        #   in Loop: Header=BB0_15 Depth=1
	movslq	(%r12), %rax
	leaq	(%rcx,%rax,4), %rcx
	movq	8(%r11), %rax
	addq	$8, %r11
	testq	%rax, %rax
	jne	.LBB0_15
# BB#42:                                # %._crit_edge.i
	testq	%r14, %r14
	je	.LBB0_44
.LBB0_43:                               # %._crit_edge.thread.i
	movq	%r14, %rdi
	callq	free
.LBB0_44:                               # %sf_unlist.exit
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sf_free
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	sf_contain, .Lfunc_end0-sf_contain
	.cfi_endproc

	.globl	sf_rev_contain
	.p2align	4, 0x90
	.type	sf_rev_contain,@function
sf_rev_contain:                         # @sf_rev_contain
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 48
.Lcfi15:
	.cfi_offset %rbx, -48
.Lcfi16:
	.cfi_offset %r12, -40
.Lcfi17:
	.cfi_offset %r13, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movslq	12(%r15), %rbx
	leaq	8(,%rbx,8), %rdi
	callq	malloc
	movq	%rax, %r14
	movslq	(%r15), %rax
	imulq	%rbx, %rax
	testl	%eax, %eax
	jle	.LBB1_1
# BB#2:                                 # %.lr.ph.i.preheader
	movq	24(%r15), %rbx
	leaq	(%rbx,%rax,4), %r13
	movq	%r14, %r12
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rbx), %eax
	movl	%eax, (%rbx)
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	set_ord
	shll	$16, %eax
	orl	%eax, (%rbx)
	movq	%rbx, (%r12)
	addq	$8, %r12
	movslq	(%r15), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r13, %rbx
	jb	.LBB1_3
# BB#4:                                 # %._crit_edge.loopexit.i
	movl	12(%r15), %ebx
	jmp	.LBB1_5
.LBB1_1:
	movq	%r14, %r12
.LBB1_5:                                # %sf_sort.exit
	movq	$0, (%r12)
	movslq	%ebx, %rsi
	movl	$8, %edx
	movl	$ascend, %ecx
	movq	%r14, %rdi
	callq	qsort
	cmpq	$0, (%r14)
	je	.LBB1_12
# BB#6:                                 # %.outer.i.preheader
	movq	%r14, %rbx
	movq	%r14, %r12
	jmp	.LBB1_7
	.p2align	4, 0x90
.LBB1_10:                               #   in Loop: Header=BB1_7 Depth=1
	movq	-8(%rbx), %rax
	movq	%rax, (%r12)
	addq	$8, %r12
.LBB1_7:                                # %.outer.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_8 Depth 2
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB1_8:                                #   Parent Loop BB1_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$0, 8(%rsi)
	je	.LBB1_11
# BB#9:                                 #   in Loop: Header=BB1_8 Depth=2
	leaq	8(%rsi), %rbx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	ascend
	testl	%eax, %eax
	movq	%rbx, %rsi
	je	.LBB1_8
	jmp	.LBB1_10
.LBB1_11:                               # %rm_equal.exit
	movq	(%rsi), %rax
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.LBB1_12
# BB#21:                                # %.lr.ph.i12.preheader
	movl	$-1, %r9d
	movq	%r14, %r8
	movq	%r14, %r13
                                        # implicit-def: %RSI
	.p2align	4, 0x90
.LBB1_24:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_25 Depth 2
                                        #       Child Loop BB1_27 Depth 3
	movl	%r9d, %ecx
	addq	$8, %r8
	movl	(%rax), %r9d
	shrl	$16, %r9d
	cmpl	%ecx, %r9d
	cmovneq	%r13, %rsi
	movq	%r14, %rdi
	.p2align	4, 0x90
.LBB1_25:                               #   Parent Loop BB1_24 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_27 Depth 3
	cmpq	%rsi, %rdi
	je	.LBB1_22
# BB#26:                                #   in Loop: Header=BB1_25 Depth=2
	movq	(%rdi), %rcx
	addq	$8, %rdi
	movl	(%rcx), %ebx
	andl	$1023, %ebx             # imm = 0x3FF
	.p2align	4, 0x90
.LBB1_27:                               #   Parent Loop BB1_24 Depth=1
                                        #     Parent Loop BB1_25 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rax,%rbx,4), %edx
	notl	%edx
	testl	(%rcx,%rbx,4), %edx
	jne	.LBB1_29
# BB#28:                                #   in Loop: Header=BB1_27 Depth=3
	leaq	-1(%rbx), %rdx
	cmpq	$1, %rbx
	movq	%rdx, %rbx
	jg	.LBB1_27
	jmp	.LBB1_30
	.p2align	4, 0x90
.LBB1_29:                               # %._crit_edge
                                        #   in Loop: Header=BB1_25 Depth=2
	movl	%ebx, %edx
.LBB1_30:                               #   in Loop: Header=BB1_25 Depth=2
	testl	%edx, %edx
	jne	.LBB1_25
	jmp	.LBB1_23
	.p2align	4, 0x90
.LBB1_22:                               # %.outer.i13
                                        #   in Loop: Header=BB1_24 Depth=1
	movq	%rax, (%r13)
	addq	$8, %r13
.LBB1_23:                               # %.loopexit.i
                                        #   in Loop: Header=BB1_24 Depth=1
	movq	(%r8), %rax
	testq	%rax, %rax
	jne	.LBB1_24
	jmp	.LBB1_13
.LBB1_12:
	movq	%r14, %r13
.LBB1_13:                               # %rm_rev_contain.exit
	movq	$0, (%r13)
	subq	%r14, %r13
	shrq	$3, %r13
	movl	4(%r15), %esi
	xorl	%eax, %eax
	movl	%r13d, %edi
	callq	sf_new
	movq	%rax, %r12
	movl	%r13d, 12(%r12)
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.LBB1_43
# BB#14:                                # %.lr.ph.i14
	movq	24(%r12), %rcx
	movq	%r14, %r11
	.p2align	4, 0x90
.LBB1_15:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_32 Depth 2
                                        #     Child Loop BB1_36 Depth 2
                                        #     Child Loop BB1_40 Depth 2
	movl	(%rax), %esi
	andl	$1023, %esi             # imm = 0x3FF
	leaq	1(%rsi), %r10
	cmpq	$8, %r10
	jb	.LBB1_39
# BB#16:                                # %min.iters.checked
                                        #   in Loop: Header=BB1_15 Depth=1
	movq	%r10, %r8
	andq	$2040, %r8              # imm = 0x7F8
	je	.LBB1_39
# BB#17:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_15 Depth=1
	leaq	4(%rax,%rsi,4), %rdx
	cmpq	%rdx, %rcx
	jae	.LBB1_19
# BB#18:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_15 Depth=1
	leaq	4(%rcx,%rsi,4), %rdx
	cmpq	%rdx, %rax
	jb	.LBB1_39
.LBB1_19:                               # %vector.body.preheader
                                        #   in Loop: Header=BB1_15 Depth=1
	leaq	-8(%r8), %r9
	movl	%r9d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB1_20
# BB#31:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB1_15 Depth=1
	leaq	-12(%rcx,%rsi,4), %r13
	leaq	-12(%rax,%rsi,4), %rdx
	negq	%rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_32:                               # %vector.body.prol
                                        #   Parent Loop BB1_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdx,%rbx,4), %xmm0
	movups	-16(%rdx,%rbx,4), %xmm1
	movups	%xmm0, (%r13,%rbx,4)
	movups	%xmm1, -16(%r13,%rbx,4)
	addq	$-8, %rbx
	incq	%rdi
	jne	.LBB1_32
# BB#33:                                # %vector.body.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB1_15 Depth=1
	negq	%rbx
	cmpq	$24, %r9
	jae	.LBB1_35
	jmp	.LBB1_37
.LBB1_20:                               #   in Loop: Header=BB1_15 Depth=1
	xorl	%ebx, %ebx
	cmpq	$24, %r9
	jb	.LBB1_37
.LBB1_35:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB1_15 Depth=1
	movq	%r10, %r9
	andq	$-8, %r9
	negq	%r9
	negq	%rbx
	leaq	-12(%rcx,%rsi,4), %rdx
	leaq	-12(%rax,%rsi,4), %rdi
	.p2align	4, 0x90
.LBB1_36:                               # %vector.body
                                        #   Parent Loop BB1_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rbx,4), %xmm0
	movups	-16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%rdx,%rbx,4)
	movups	%xmm1, -16(%rdx,%rbx,4)
	movups	-32(%rdi,%rbx,4), %xmm0
	movups	-48(%rdi,%rbx,4), %xmm1
	movups	%xmm0, -32(%rdx,%rbx,4)
	movups	%xmm1, -48(%rdx,%rbx,4)
	movups	-64(%rdi,%rbx,4), %xmm0
	movups	-80(%rdi,%rbx,4), %xmm1
	movups	%xmm0, -64(%rdx,%rbx,4)
	movups	%xmm1, -80(%rdx,%rbx,4)
	movups	-96(%rdi,%rbx,4), %xmm0
	movups	-112(%rdi,%rbx,4), %xmm1
	movups	%xmm0, -96(%rdx,%rbx,4)
	movups	%xmm1, -112(%rdx,%rbx,4)
	addq	$-32, %rbx
	cmpq	%rbx, %r9
	jne	.LBB1_36
.LBB1_37:                               # %middle.block
                                        #   in Loop: Header=BB1_15 Depth=1
	cmpq	%r8, %r10
	je	.LBB1_41
# BB#38:                                #   in Loop: Header=BB1_15 Depth=1
	subq	%r8, %rsi
	.p2align	4, 0x90
.LBB1_39:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB1_15 Depth=1
	incq	%rsi
	.p2align	4, 0x90
.LBB1_40:                               # %scalar.ph
                                        #   Parent Loop BB1_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rax,%rsi,4), %edx
	movl	%edx, -4(%rcx,%rsi,4)
	decq	%rsi
	jg	.LBB1_40
.LBB1_41:                               # %.loopexit
                                        #   in Loop: Header=BB1_15 Depth=1
	movslq	(%r12), %rax
	leaq	(%rcx,%rax,4), %rcx
	movq	8(%r11), %rax
	addq	$8, %r11
	testq	%rax, %rax
	jne	.LBB1_15
# BB#42:                                # %._crit_edge.i
	testq	%r14, %r14
	je	.LBB1_44
.LBB1_43:                               # %._crit_edge.thread.i
	movq	%r14, %rdi
	callq	free
.LBB1_44:                               # %sf_unlist.exit
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sf_free
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	sf_rev_contain, .Lfunc_end1-sf_rev_contain
	.cfi_endproc

	.globl	sf_ind_contain
	.p2align	4, 0x90
	.type	sf_ind_contain,@function
sf_ind_contain:                         # @sf_ind_contain
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 64
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movslq	12(%r12), %rbx
	leaq	8(,%rbx,8), %rdi
	callq	malloc
	movq	%rax, %r15
	movslq	(%r12), %rax
	imulq	%rbx, %rax
	testl	%eax, %eax
	jle	.LBB2_1
# BB#2:                                 # %.lr.ph.i.preheader
	movq	24(%r12), %rbx
	leaq	(%rbx,%rax,4), %rbp
	movq	%r15, %r13
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rbx), %eax
	movl	%eax, (%rbx)
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	set_ord
	shll	$16, %eax
	orl	%eax, (%rbx)
	movq	%rbx, (%r13)
	addq	$8, %r13
	movslq	(%r12), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%rbp, %rbx
	jb	.LBB2_3
# BB#4:                                 # %._crit_edge.loopexit.i
	movl	12(%r12), %ebx
	jmp	.LBB2_5
.LBB2_1:
	movq	%r15, %r13
.LBB2_5:                                # %sf_sort.exit
	movq	$0, (%r13)
	movslq	%ebx, %rsi
	movl	$8, %edx
	movl	$descend, %ecx
	movq	%r15, %rdi
	callq	qsort
	cmpq	$0, (%r15)
	je	.LBB2_22
# BB#6:                                 # %.outer.i.preheader
	movq	%r15, %rbx
	movq	%r15, %r13
	jmp	.LBB2_7
	.p2align	4, 0x90
.LBB2_10:                               #   in Loop: Header=BB2_7 Depth=1
	movq	-8(%rbx), %rax
	movq	%rax, (%r13)
	addq	$8, %r13
.LBB2_7:                                # %.outer.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_8 Depth 2
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB2_8:                                #   Parent Loop BB2_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$0, 8(%rsi)
	je	.LBB2_11
# BB#9:                                 #   in Loop: Header=BB2_8 Depth=2
	leaq	8(%rsi), %rbx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	descend
	testl	%eax, %eax
	movq	%rbx, %rsi
	je	.LBB2_8
	jmp	.LBB2_10
.LBB2_11:                               # %rm_equal.exit
	movq	(%rsi), %rax
	movq	%rax, (%r13)
	movq	$0, 8(%r13)
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.LBB2_22
# BB#12:                                # %.lr.ph.i14.preheader
	movl	$-1, %r9d
	movq	%r15, %r8
	movq	%r15, %rsi
                                        # implicit-def: %R10
	.p2align	4, 0x90
.LBB2_15:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_16 Depth 2
                                        #       Child Loop BB2_18 Depth 3
	movl	%r9d, %ecx
	addq	$8, %r8
	movl	(%rax), %ebx
	movl	%ebx, %r9d
	shrl	$16, %r9d
	cmpl	%ecx, %r9d
	cmovneq	%rsi, %r10
	andl	$1023, %ebx             # imm = 0x3FF
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB2_16:                               #   Parent Loop BB2_15 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_18 Depth 3
	cmpq	%r10, %rcx
	je	.LBB2_13
# BB#17:                                #   in Loop: Header=BB2_16 Depth=2
	movq	(%rcx), %rbp
	addq	$8, %rcx
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB2_18:                               #   Parent Loop BB2_15 Depth=1
                                        #     Parent Loop BB2_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rbp,%rdi,4), %edx
	notl	%edx
	testl	(%rax,%rdi,4), %edx
	jne	.LBB2_20
# BB#19:                                #   in Loop: Header=BB2_18 Depth=3
	leaq	-1(%rdi), %rdx
	cmpq	$1, %rdi
	movq	%rdx, %rdi
	jg	.LBB2_18
	jmp	.LBB2_21
	.p2align	4, 0x90
.LBB2_20:                               # %._crit_edge
                                        #   in Loop: Header=BB2_16 Depth=2
	movl	%edi, %edx
.LBB2_21:                               #   in Loop: Header=BB2_16 Depth=2
	testl	%edx, %edx
	jne	.LBB2_16
	jmp	.LBB2_14
	.p2align	4, 0x90
.LBB2_13:                               # %.outer.i15
                                        #   in Loop: Header=BB2_15 Depth=1
	movq	%rax, (%rsi)
	addq	$8, %rsi
.LBB2_14:                               # %.loopexit.i
                                        #   in Loop: Header=BB2_15 Depth=1
	movq	(%r8), %rax
	testq	%rax, %rax
	jne	.LBB2_15
	jmp	.LBB2_23
.LBB2_22:                               # %rm_equal.exit.thread
	movq	%r15, %rsi
.LBB2_23:                               # %rm_contain.exit
	movq	$0, (%rsi)
	subq	%r15, %rsi
	shrq	$3, %rsi
	movl	4(%r12), %edx
	movq	24(%r12), %r8
	movq	%r15, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r14, %rcx
	callq	sf_ind_unlist
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sf_free
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	sf_ind_contain, .Lfunc_end2-sf_ind_contain
	.cfi_endproc

	.globl	sf_dupl
	.p2align	4, 0x90
	.type	sf_dupl,@function
sf_dupl:                                # @sf_dupl
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 48
.Lcfi38:
	.cfi_offset %rbx, -48
.Lcfi39:
	.cfi_offset %r12, -40
.Lcfi40:
	.cfi_offset %r13, -32
.Lcfi41:
	.cfi_offset %r14, -24
.Lcfi42:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movslq	12(%r15), %rbx
	leaq	8(,%rbx,8), %rdi
	callq	malloc
	movq	%rax, %r14
	movslq	(%r15), %rax
	imulq	%rbx, %rax
	testl	%eax, %eax
	jle	.LBB3_1
# BB#2:                                 # %.lr.ph.i.preheader
	movq	24(%r15), %rbx
	leaq	(%rbx,%rax,4), %r13
	movq	%r14, %r12
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rbx), %eax
	movl	%eax, (%rbx)
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	set_ord
	shll	$16, %eax
	orl	%eax, (%rbx)
	movq	%rbx, (%r12)
	addq	$8, %r12
	movslq	(%r15), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r13, %rbx
	jb	.LBB3_3
# BB#4:                                 # %._crit_edge.loopexit.i
	movl	12(%r15), %ebx
	jmp	.LBB3_5
.LBB3_1:
	movq	%r14, %r12
.LBB3_5:                                # %sf_sort.exit
	movq	$0, (%r12)
	movslq	%ebx, %rsi
	movl	$8, %edx
	movl	$descend, %ecx
	movq	%r14, %rdi
	callq	qsort
	cmpq	$0, (%r14)
	je	.LBB3_6
# BB#7:                                 # %.outer.i.preheader
	movq	%r14, %rbx
	movq	%r14, %r12
	jmp	.LBB3_8
	.p2align	4, 0x90
.LBB3_11:                               #   in Loop: Header=BB3_8 Depth=1
	movq	-8(%rbx), %rax
	movq	%rax, (%r12)
	addq	$8, %r12
.LBB3_8:                                # %.outer.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_9 Depth 2
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB3_9:                                #   Parent Loop BB3_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$0, 8(%rsi)
	je	.LBB3_12
# BB#10:                                #   in Loop: Header=BB3_9 Depth=2
	leaq	8(%rsi), %rbx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	descend
	testl	%eax, %eax
	movq	%rbx, %rsi
	je	.LBB3_9
	jmp	.LBB3_11
.LBB3_12:
	movq	(%rsi), %rax
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	leaq	8(%r12), %rbx
	jmp	.LBB3_13
.LBB3_6:
	movq	%r14, %rbx
.LBB3_13:                               # %rm_equal.exit
	subq	%r14, %rbx
	shrq	$3, %rbx
	movl	4(%r15), %esi
	xorl	%eax, %eax
	movl	%ebx, %edi
	callq	sf_new
	movq	%rax, %r12
	movl	%ebx, 12(%r12)
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.LBB3_33
# BB#14:                                # %.lr.ph.i11
	movq	24(%r12), %rcx
	movq	%r14, %r11
	.p2align	4, 0x90
.LBB3_15:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_22 Depth 2
                                        #     Child Loop BB3_26 Depth 2
                                        #     Child Loop BB3_30 Depth 2
	movl	(%rax), %esi
	andl	$1023, %esi             # imm = 0x3FF
	leaq	1(%rsi), %r10
	cmpq	$8, %r10
	jb	.LBB3_29
# BB#16:                                # %min.iters.checked
                                        #   in Loop: Header=BB3_15 Depth=1
	movq	%r10, %r8
	andq	$2040, %r8              # imm = 0x7F8
	je	.LBB3_29
# BB#17:                                # %vector.memcheck
                                        #   in Loop: Header=BB3_15 Depth=1
	leaq	4(%rax,%rsi,4), %rdx
	cmpq	%rdx, %rcx
	jae	.LBB3_19
# BB#18:                                # %vector.memcheck
                                        #   in Loop: Header=BB3_15 Depth=1
	leaq	4(%rcx,%rsi,4), %rdx
	cmpq	%rdx, %rax
	jb	.LBB3_29
.LBB3_19:                               # %vector.body.preheader
                                        #   in Loop: Header=BB3_15 Depth=1
	leaq	-8(%r8), %r9
	movl	%r9d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB3_20
# BB#21:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB3_15 Depth=1
	leaq	-12(%rcx,%rsi,4), %r13
	leaq	-12(%rax,%rsi,4), %rdx
	negq	%rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_22:                               # %vector.body.prol
                                        #   Parent Loop BB3_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdx,%rbx,4), %xmm0
	movups	-16(%rdx,%rbx,4), %xmm1
	movups	%xmm0, (%r13,%rbx,4)
	movups	%xmm1, -16(%r13,%rbx,4)
	addq	$-8, %rbx
	incq	%rdi
	jne	.LBB3_22
# BB#23:                                # %vector.body.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB3_15 Depth=1
	negq	%rbx
	cmpq	$24, %r9
	jae	.LBB3_25
	jmp	.LBB3_27
.LBB3_20:                               #   in Loop: Header=BB3_15 Depth=1
	xorl	%ebx, %ebx
	cmpq	$24, %r9
	jb	.LBB3_27
.LBB3_25:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB3_15 Depth=1
	movq	%r10, %r9
	andq	$-8, %r9
	negq	%r9
	negq	%rbx
	leaq	-12(%rcx,%rsi,4), %rdx
	leaq	-12(%rax,%rsi,4), %rdi
	.p2align	4, 0x90
.LBB3_26:                               # %vector.body
                                        #   Parent Loop BB3_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rbx,4), %xmm0
	movups	-16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%rdx,%rbx,4)
	movups	%xmm1, -16(%rdx,%rbx,4)
	movups	-32(%rdi,%rbx,4), %xmm0
	movups	-48(%rdi,%rbx,4), %xmm1
	movups	%xmm0, -32(%rdx,%rbx,4)
	movups	%xmm1, -48(%rdx,%rbx,4)
	movups	-64(%rdi,%rbx,4), %xmm0
	movups	-80(%rdi,%rbx,4), %xmm1
	movups	%xmm0, -64(%rdx,%rbx,4)
	movups	%xmm1, -80(%rdx,%rbx,4)
	movups	-96(%rdi,%rbx,4), %xmm0
	movups	-112(%rdi,%rbx,4), %xmm1
	movups	%xmm0, -96(%rdx,%rbx,4)
	movups	%xmm1, -112(%rdx,%rbx,4)
	addq	$-32, %rbx
	cmpq	%rbx, %r9
	jne	.LBB3_26
.LBB3_27:                               # %middle.block
                                        #   in Loop: Header=BB3_15 Depth=1
	cmpq	%r8, %r10
	je	.LBB3_31
# BB#28:                                #   in Loop: Header=BB3_15 Depth=1
	subq	%r8, %rsi
	.p2align	4, 0x90
.LBB3_29:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB3_15 Depth=1
	incq	%rsi
	.p2align	4, 0x90
.LBB3_30:                               # %scalar.ph
                                        #   Parent Loop BB3_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rax,%rsi,4), %edx
	movl	%edx, -4(%rcx,%rsi,4)
	decq	%rsi
	jg	.LBB3_30
.LBB3_31:                               # %.loopexit
                                        #   in Loop: Header=BB3_15 Depth=1
	movslq	(%r12), %rax
	leaq	(%rcx,%rax,4), %rcx
	movq	8(%r11), %rax
	addq	$8, %r11
	testq	%rax, %rax
	jne	.LBB3_15
# BB#32:                                # %._crit_edge.i
	testq	%r14, %r14
	je	.LBB3_34
.LBB3_33:                               # %._crit_edge.thread.i
	movq	%r14, %rdi
	callq	free
.LBB3_34:                               # %sf_unlist.exit
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sf_free
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	sf_dupl, .Lfunc_end3-sf_dupl
	.cfi_endproc

	.globl	sf_union
	.p2align	4, 0x90
	.type	sf_union,@function
sf_union:                               # @sf_union
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi46:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi49:
	.cfi_def_cfa_offset 96
.Lcfi50:
	.cfi_offset %rbx, -56
.Lcfi51:
	.cfi_offset %r12, -48
.Lcfi52:
	.cfi_offset %r13, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movslq	12(%r13), %r12
	leaq	8(,%r12,8), %rdi
	callq	malloc
	movq	%rax, %r15
	movslq	(%r13), %rax
	movq	%rax, %rdx
	imulq	%r12, %rdx
	testl	%edx, %edx
	jle	.LBB4_1
# BB#2:                                 # %.lr.ph.i
	movq	24(%r13), %rcx
	leaq	(%rcx,%rdx,4), %rdx
	shlq	$2, %rax
	movq	%r15, %rsi
	.p2align	4, 0x90
.LBB4_3:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, (%rsi)
	addq	$8, %rsi
	addq	%rax, %rcx
	cmpq	%rdx, %rcx
	jb	.LBB4_3
	jmp	.LBB4_4
.LBB4_1:
	movq	%r15, %rsi
.LBB4_4:                                # %sf_list.exit
	movq	$0, (%rsi)
	movslq	12(%rbx), %rbp
	leaq	8(,%rbp,8), %rdi
	callq	malloc
	movq	%rax, %r14
	movslq	(%rbx), %rax
	movq	%rbp, %rdx
	imulq	%rax, %rdx
	testl	%edx, %edx
	jle	.LBB4_5
# BB#6:                                 # %.lr.ph.i31
	movq	24(%rbx), %rcx
	leaq	(%rcx,%rdx,4), %rsi
	shlq	$2, %rax
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB4_7:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, (%rdx)
	addq	$8, %rdx
	addq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.LBB4_7
	jmp	.LBB4_8
.LBB4_5:
	movq	%r14, %rdx
.LBB4_8:                                # %sf_list.exit35
	movq	$0, (%rdx)
	cmpl	%ebp, %r12d
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	%r13, 16(%rsp)          # 8-byte Spill
	cmovgq	%r13, %rbx
	movslq	12(%rbx), %rax
	leaq	8(,%rax,8), %rdi
	callq	malloc
	movq	%r14, %rbp
	movq	%r15, %r13
	movq	%r15, %r12
	movq	%r14, %rbx
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB4_9
.LBB4_27:                               #   in Loop: Header=BB4_9 Depth=1
	movq	(%r13), %rax
	addq	$8, %r13
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, (%rcx)
	addq	$8, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	addq	$8, %rbp
	.p2align	4, 0x90
.LBB4_9:                                # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	testq	%rax, %rax
	je	.LBB4_14
# BB#10:                                #   in Loop: Header=BB4_9 Depth=1
	cmpq	$0, (%rbp)
	je	.LBB4_11
# BB#23:                                #   in Loop: Header=BB4_9 Depth=1
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	descend
	cmpl	$1, %eax
	je	.LBB4_28
# BB#24:                                #   in Loop: Header=BB4_9 Depth=1
	testl	%eax, %eax
	je	.LBB4_27
# BB#25:                                #   in Loop: Header=BB4_9 Depth=1
	cmpl	$-1, %eax
	jne	.LBB4_9
# BB#26:                                #   in Loop: Header=BB4_9 Depth=1
	movq	(%r13), %rax
	addq	$8, %r13
	movq	%rax, (%r12)
	addq	$8, %r12
	jmp	.LBB4_9
.LBB4_28:                               #   in Loop: Header=BB4_9 Depth=1
	movq	(%rbp), %rax
	addq	$8, %rbp
	movq	%rax, (%rbx)
	addq	$8, %rbx
	jmp	.LBB4_9
.LBB4_11:                               # %.critedge.i.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_12:                               # %.critedge.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, (%r12,%rcx)
	movq	8(%r13,%rcx), %rax
	addq	$8, %rcx
	testq	%rax, %rax
	jne	.LBB4_12
# BB#13:                                # %.preheader.i.loopexit
	addq	%rcx, %r12
.LBB4_14:                               # %.preheader.i
	movq	16(%rsp), %r13          # 8-byte Reload
	movq	(%rbp), %rax
	testq	%rax, %rax
	je	.LBB4_18
# BB#15:                                # %.lr.ph.i36.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_16:                               # %.lr.ph.i36
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, (%rbx,%rcx)
	movq	8(%rbp,%rcx), %rax
	addq	$8, %rcx
	testq	%rax, %rax
	jne	.LBB4_16
# BB#17:                                # %rm2_equal.exit.loopexit
	addq	%rcx, %rbx
.LBB4_18:                               # %rm2_equal.exit
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	$0, (%rax)
	movq	$0, (%rbx)
	movq	$0, (%r12)
	subq	32(%rsp), %rax          # 8-byte Folded Reload
	shrq	$3, %rax
	movq	%rax, %r10
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.LBB4_19
# BB#29:                                # %.preheader.lr.ph.i.preheader
	movq	%r15, %r8
	movq	%r15, %rcx
	movq	24(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_32:                               # %.preheader.i37
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_33 Depth 2
                                        #       Child Loop BB4_36 Depth 3
	addq	$8, %r8
	movq	%r14, %rsi
	.p2align	4, 0x90
.LBB4_33:                               #   Parent Loop BB4_32 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_36 Depth 3
	movq	(%rsi), %rdi
	testq	%rdi, %rdi
	je	.LBB4_30
# BB#34:                                #   in Loop: Header=BB4_33 Depth=2
	movzwl	2(%rdi), %ebx
	movl	(%rax), %ebp
	movl	%ebp, %edx
	shrl	$16, %edx
	cmpl	%edx, %ebx
	jbe	.LBB4_30
# BB#35:                                #   in Loop: Header=BB4_33 Depth=2
	addq	$8, %rsi
	andl	$1023, %ebp             # imm = 0x3FF
	.p2align	4, 0x90
.LBB4_36:                               #   Parent Loop BB4_32 Depth=1
                                        #     Parent Loop BB4_33 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdi,%rbp,4), %edx
	notl	%edx
	testl	(%rax,%rbp,4), %edx
	jne	.LBB4_38
# BB#37:                                #   in Loop: Header=BB4_36 Depth=3
	leaq	-1(%rbp), %rbx
	cmpq	$1, %rbp
	movq	%rbx, %rbp
	jg	.LBB4_36
	jmp	.LBB4_39
	.p2align	4, 0x90
.LBB4_38:                               # %._crit_edge274
                                        #   in Loop: Header=BB4_33 Depth=2
	movl	%ebp, %ebx
.LBB4_39:                               #   in Loop: Header=BB4_33 Depth=2
	testl	%ebx, %ebx
	jne	.LBB4_33
	jmp	.LBB4_31
	.p2align	4, 0x90
.LBB4_30:                               # %.critedge.i38
                                        #   in Loop: Header=BB4_32 Depth=1
	movq	%rax, (%rcx)
	addq	$8, %rcx
.LBB4_31:                               # %.loopexit.i
                                        #   in Loop: Header=BB4_32 Depth=1
	movq	(%r8), %rax
	testq	%rax, %rax
	jne	.LBB4_32
	jmp	.LBB4_20
.LBB4_19:
	movq	%r15, %rcx
	movq	24(%rsp), %r12          # 8-byte Reload
.LBB4_20:                               # %rm2_contain.exit
	movq	$0, (%rcx)
	subq	%r15, %rcx
	shrq	$3, %rcx
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	je	.LBB4_21
# BB#40:                                # %.preheader.lr.ph.i41.preheader
	movq	%r14, %r8
	movq	%r14, %r9
	.p2align	4, 0x90
.LBB4_43:                               # %.preheader.i44
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_44 Depth 2
                                        #       Child Loop BB4_47 Depth 3
	addq	$8, %r8
	movq	%r15, %rdi
	.p2align	4, 0x90
.LBB4_44:                               #   Parent Loop BB4_43 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_47 Depth 3
	movq	(%rdi), %rbp
	testq	%rbp, %rbp
	je	.LBB4_41
# BB#45:                                #   in Loop: Header=BB4_44 Depth=2
	movzwl	2(%rbp), %esi
	movl	(%rdx), %ebx
	movl	%ebx, %eax
	shrl	$16, %eax
	cmpl	%eax, %esi
	jbe	.LBB4_41
# BB#46:                                #   in Loop: Header=BB4_44 Depth=2
	addq	$8, %rdi
	andl	$1023, %ebx             # imm = 0x3FF
	.p2align	4, 0x90
.LBB4_47:                               #   Parent Loop BB4_43 Depth=1
                                        #     Parent Loop BB4_44 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rbp,%rbx,4), %eax
	notl	%eax
	testl	(%rdx,%rbx,4), %eax
	jne	.LBB4_49
# BB#48:                                #   in Loop: Header=BB4_47 Depth=3
	leaq	-1(%rbx), %rsi
	cmpq	$1, %rbx
	movq	%rsi, %rbx
	jg	.LBB4_47
	jmp	.LBB4_50
	.p2align	4, 0x90
.LBB4_49:                               # %._crit_edge
                                        #   in Loop: Header=BB4_44 Depth=2
	movl	%ebx, %esi
.LBB4_50:                               #   in Loop: Header=BB4_44 Depth=2
	testl	%esi, %esi
	jne	.LBB4_44
	jmp	.LBB4_42
	.p2align	4, 0x90
.LBB4_41:                               # %.critedge.i50
                                        #   in Loop: Header=BB4_43 Depth=1
	movq	%rdx, (%r9)
	addq	$8, %r9
.LBB4_42:                               # %.loopexit.i42
                                        #   in Loop: Header=BB4_43 Depth=1
	movq	(%r8), %rdx
	testq	%rdx, %rdx
	jne	.LBB4_43
	jmp	.LBB4_22
.LBB4_21:
	movq	%r14, %r9
.LBB4_22:                               # %rm2_contain.exit52
	movq	$0, (%r9)
	subq	%r14, %r9
	shrq	$3, %r9
	addl	%r10d, %ecx
	addl	%r9d, %ecx
	movl	4(%r13), %r8d
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	sf_merge
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	sf_free
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sf_free
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	sf_union, .Lfunc_end4-sf_union
	.cfi_endproc

	.globl	dist_merge
	.p2align	4, 0x90
	.type	dist_merge,@function
dist_merge:                             # @dist_merge
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi60:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi62:
	.cfi_def_cfa_offset 64
.Lcfi63:
	.cfi_offset %rbx, -56
.Lcfi64:
	.cfi_offset %r12, -48
.Lcfi65:
	.cfi_offset %r13, -40
.Lcfi66:
	.cfi_offset %r14, -32
.Lcfi67:
	.cfi_offset %r15, -24
.Lcfi68:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	cube+80(%rip), %rax
	movq	(%rax), %rdi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	set_copy
	movslq	12(%r12), %rbx
	leaq	8(,%rbx,8), %rdi
	callq	malloc
	movq	%rax, %r15
	movslq	(%r12), %rax
	imulq	%rbx, %rax
	testl	%eax, %eax
	jle	.LBB5_1
# BB#2:                                 # %.lr.ph.i.preheader
	movq	24(%r12), %rbx
	leaq	(%rbx,%rax,4), %r14
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rbx), %eax
	movl	%eax, (%rbx)
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	set_ord
	shll	$16, %eax
	orl	%eax, (%rbx)
	movq	%rbx, (%rbp)
	addq	$8, %rbp
	movslq	(%r12), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r14, %rbx
	jb	.LBB5_3
# BB#4:                                 # %._crit_edge.loopexit.i
	movl	12(%r12), %ebx
	jmp	.LBB5_5
.LBB5_1:
	movq	%r15, %rbp
.LBB5_5:                                # %sf_sort.exit
	movq	%r12, (%rsp)            # 8-byte Spill
	movq	$0, (%rbp)
	movslq	%ebx, %rsi
	movl	$8, %edx
	movl	$d1_order, %ecx
	movq	%r15, %rdi
	callq	qsort
	cmpq	$0, (%r15)
	je	.LBB5_14
# BB#6:                                 # %.preheader.i
	cmpq	$0, 8(%r15)
	je	.LBB5_7
# BB#8:                                 # %.lr.ph.i12.preheader
	leaq	8(%r15), %rbx
	movl	$1, %ebp
	movq	%r15, %r12
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_9:                                # %.lr.ph.i12
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	d1_order
	movq	(%r12), %rdi
	testl	%eax, %eax
	je	.LBB5_10
# BB#11:                                #   in Loop: Header=BB5_9 Depth=1
	movslq	%r13d, %rax
	incl	%r13d
	movq	%rdi, (%r15,%rax,8)
	movl	%ebp, %r14d
	jmp	.LBB5_12
	.p2align	4, 0x90
.LBB5_10:                               #   in Loop: Header=BB5_9 Depth=1
	movq	(%rbx), %rdx
	xorl	%eax, %eax
	movq	%rdi, %rsi
	callq	set_or
.LBB5_12:                               #   in Loop: Header=BB5_9 Depth=1
	movslq	%r14d, %rax
	leaq	(%r15,%rax,8), %r12
	incl	%ebp
	cmpq	$0, 8(%rbx)
	leaq	8(%rbx), %rbx
	jne	.LBB5_9
	jmp	.LBB5_13
.LBB5_7:
	xorl	%r13d, %r13d
	movq	%r15, %r12
.LBB5_13:                               # %._crit_edge.i
	movq	(%r12), %rax
	movslq	%r13d, %rcx
	incl	%r13d
	movq	%rax, (%r15,%rcx,8)
.LBB5_14:                               # %d1_rm_equal.exit
	movslq	%r13d, %r12
	movq	$0, (%r15,%r12,8)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	4(%rax), %esi
	xorl	%eax, %eax
	movl	%r12d, %edi
	callq	sf_new
	movq	%rax, %r13
	movl	%r12d, 12(%r13)
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.LBB5_34
# BB#15:                                # %.lr.ph.i13
	movq	24(%r13), %rcx
	movq	%r15, %r11
	.p2align	4, 0x90
.LBB5_16:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_23 Depth 2
                                        #     Child Loop BB5_27 Depth 2
                                        #     Child Loop BB5_31 Depth 2
	movl	(%rax), %esi
	andl	$1023, %esi             # imm = 0x3FF
	leaq	1(%rsi), %r10
	cmpq	$8, %r10
	jb	.LBB5_30
# BB#17:                                # %min.iters.checked
                                        #   in Loop: Header=BB5_16 Depth=1
	movq	%r10, %r9
	andq	$2040, %r9              # imm = 0x7F8
	je	.LBB5_30
# BB#18:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_16 Depth=1
	leaq	4(%rax,%rsi,4), %rdx
	cmpq	%rdx, %rcx
	jae	.LBB5_20
# BB#19:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_16 Depth=1
	leaq	4(%rcx,%rsi,4), %rdx
	cmpq	%rdx, %rax
	jb	.LBB5_30
.LBB5_20:                               # %vector.body.preheader
                                        #   in Loop: Header=BB5_16 Depth=1
	leaq	-8(%r9), %r8
	movl	%r8d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB5_21
# BB#22:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB5_16 Depth=1
	leaq	-12(%rcx,%rsi,4), %rdx
	leaq	-12(%rax,%rsi,4), %rbp
	negq	%rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_23:                               # %vector.body.prol
                                        #   Parent Loop BB5_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbp,%rbx,4), %xmm0
	movups	-16(%rbp,%rbx,4), %xmm1
	movups	%xmm0, (%rdx,%rbx,4)
	movups	%xmm1, -16(%rdx,%rbx,4)
	addq	$-8, %rbx
	incq	%rdi
	jne	.LBB5_23
# BB#24:                                # %vector.body.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB5_16 Depth=1
	negq	%rbx
	cmpq	$24, %r8
	jae	.LBB5_26
	jmp	.LBB5_28
.LBB5_21:                               #   in Loop: Header=BB5_16 Depth=1
	xorl	%ebx, %ebx
	cmpq	$24, %r8
	jb	.LBB5_28
.LBB5_26:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB5_16 Depth=1
	movq	%r10, %rdi
	andq	$-8, %rdi
	negq	%rdi
	negq	%rbx
	leaq	-12(%rcx,%rsi,4), %rbp
	leaq	-12(%rax,%rsi,4), %rdx
	.p2align	4, 0x90
.LBB5_27:                               # %vector.body
                                        #   Parent Loop BB5_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdx,%rbx,4), %xmm0
	movups	-16(%rdx,%rbx,4), %xmm1
	movups	%xmm0, (%rbp,%rbx,4)
	movups	%xmm1, -16(%rbp,%rbx,4)
	movups	-32(%rdx,%rbx,4), %xmm0
	movups	-48(%rdx,%rbx,4), %xmm1
	movups	%xmm0, -32(%rbp,%rbx,4)
	movups	%xmm1, -48(%rbp,%rbx,4)
	movups	-64(%rdx,%rbx,4), %xmm0
	movups	-80(%rdx,%rbx,4), %xmm1
	movups	%xmm0, -64(%rbp,%rbx,4)
	movups	%xmm1, -80(%rbp,%rbx,4)
	movups	-96(%rdx,%rbx,4), %xmm0
	movups	-112(%rdx,%rbx,4), %xmm1
	movups	%xmm0, -96(%rbp,%rbx,4)
	movups	%xmm1, -112(%rbp,%rbx,4)
	addq	$-32, %rbx
	cmpq	%rbx, %rdi
	jne	.LBB5_27
.LBB5_28:                               # %middle.block
                                        #   in Loop: Header=BB5_16 Depth=1
	cmpq	%r9, %r10
	je	.LBB5_32
# BB#29:                                #   in Loop: Header=BB5_16 Depth=1
	subq	%r9, %rsi
	.p2align	4, 0x90
.LBB5_30:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB5_16 Depth=1
	incq	%rsi
	.p2align	4, 0x90
.LBB5_31:                               # %scalar.ph
                                        #   Parent Loop BB5_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rax,%rsi,4), %edx
	movl	%edx, -4(%rcx,%rsi,4)
	decq	%rsi
	jg	.LBB5_31
.LBB5_32:                               # %.loopexit
                                        #   in Loop: Header=BB5_16 Depth=1
	movslq	(%r13), %rax
	leaq	(%rcx,%rax,4), %rcx
	movq	8(%r11), %rax
	addq	$8, %r11
	testq	%rax, %rax
	jne	.LBB5_16
# BB#33:                                # %._crit_edge.i16
	testq	%r15, %r15
	je	.LBB5_35
.LBB5_34:                               # %._crit_edge.thread.i
	movq	%r15, %rdi
	callq	free
.LBB5_35:                               # %sf_unlist.exit
	xorl	%eax, %eax
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	sf_free
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	dist_merge, .Lfunc_end5-dist_merge
	.cfi_endproc

	.globl	d1merge
	.p2align	4, 0x90
	.type	d1merge,@function
d1merge:                                # @d1merge
	.cfi_startproc
# BB#0:
	movq	cube+72(%rip), %rax
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rsi
	jmp	dist_merge              # TAILCALL
.Lfunc_end6:
	.size	d1merge, .Lfunc_end6-d1merge
	.cfi_endproc

	.globl	d1_rm_equal
	.p2align	4, 0x90
	.type	d1_rm_equal,@function
d1_rm_equal:                            # @d1_rm_equal
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi72:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi73:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi75:
	.cfi_def_cfa_offset 64
.Lcfi76:
	.cfi_offset %rbx, -56
.Lcfi77:
	.cfi_offset %r12, -48
.Lcfi78:
	.cfi_offset %r13, -40
.Lcfi79:
	.cfi_offset %r14, -32
.Lcfi80:
	.cfi_offset %r15, -24
.Lcfi81:
	.cfi_offset %rbp, -16
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdi, %r15
	cmpq	$0, (%r15)
	je	.LBB7_1
# BB#2:                                 # %.preheader
	cmpq	$0, 8(%r15)
	je	.LBB7_3
# BB#4:                                 # %.lr.ph
	leaq	8(%r15), %rbx
	movl	$1, %ebp
	movq	%r15, %r12
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB7_5:                                # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	*(%rsp)                 # 8-byte Folded Reload
	movq	(%r12), %rdi
	testl	%eax, %eax
	je	.LBB7_6
# BB#7:                                 #   in Loop: Header=BB7_5 Depth=1
	movslq	%r13d, %rax
	incl	%r13d
	movq	%rdi, (%r15,%rax,8)
	movl	%ebp, %r14d
	jmp	.LBB7_8
	.p2align	4, 0x90
.LBB7_6:                                #   in Loop: Header=BB7_5 Depth=1
	movq	(%rbx), %rdx
	xorl	%eax, %eax
	movq	%rdi, %rsi
	callq	set_or
.LBB7_8:                                #   in Loop: Header=BB7_5 Depth=1
	movslq	%r14d, %rax
	leaq	(%r15,%rax,8), %r12
	incl	%ebp
	cmpq	$0, 8(%rbx)
	leaq	8(%rbx), %rbx
	jne	.LBB7_5
	jmp	.LBB7_9
.LBB7_1:
	xorl	%r13d, %r13d
	jmp	.LBB7_10
.LBB7_3:
	xorl	%r13d, %r13d
	movq	%r15, %r12
.LBB7_9:                                # %._crit_edge
	movq	(%r12), %rax
	movslq	%r13d, %rcx
	incl	%r13d
	movq	%rax, (%r15,%rcx,8)
.LBB7_10:
	movslq	%r13d, %rax
	movq	$0, (%r15,%rax,8)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	d1_rm_equal, .Lfunc_end7-d1_rm_equal
	.cfi_endproc

	.globl	rm_equal
	.p2align	4, 0x90
	.type	rm_equal,@function
rm_equal:                               # @rm_equal
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi84:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi86:
	.cfi_def_cfa_offset 48
.Lcfi87:
	.cfi_offset %rbx, -40
.Lcfi88:
	.cfi_offset %r12, -32
.Lcfi89:
	.cfi_offset %r14, -24
.Lcfi90:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	cmpq	$0, (%r14)
	movq	%r14, %rax
	je	.LBB8_7
# BB#1:                                 # %.preheader
	movq	%r14, %rbx
	movq	%r14, %r12
	jmp	.LBB8_2
	.p2align	4, 0x90
.LBB8_5:                                #   in Loop: Header=BB8_2 Depth=1
	movq	-8(%rbx), %rax
	movq	%rax, (%r12)
	addq	$8, %r12
.LBB8_2:                                # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_3 Depth 2
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB8_3:                                #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$0, 8(%rsi)
	je	.LBB8_6
# BB#4:                                 #   in Loop: Header=BB8_3 Depth=2
	leaq	8(%rsi), %rbx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	*%r15
	testl	%eax, %eax
	movq	%rbx, %rsi
	je	.LBB8_3
	jmp	.LBB8_5
.LBB8_6:
	movq	(%rsi), %rax
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	leaq	8(%r12), %rax
.LBB8_7:
	subq	%r14, %rax
	shrq	$3, %rax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	rm_equal, .Lfunc_end8-rm_equal
	.cfi_endproc

	.globl	rm_contain
	.p2align	4, 0x90
	.type	rm_contain,@function
rm_contain:                             # @rm_contain
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 24
.Lcfi93:
	.cfi_offset %rbx, -24
.Lcfi94:
	.cfi_offset %r14, -16
	movq	(%rdi), %rcx
	testq	%rcx, %rcx
	movq	%rdi, %rax
	je	.LBB9_11
# BB#1:                                 # %.lr.ph.preheader
	movl	$-1, %r9d
	movq	%rdi, %r8
	movq	%rdi, %rax
                                        # implicit-def: %R10
	.p2align	4, 0x90
.LBB9_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_5 Depth 2
                                        #       Child Loop BB9_7 Depth 3
	movl	%r9d, %edx
	addq	$8, %r8
	movl	(%rcx), %r11d
	movl	%r11d, %r9d
	shrl	$16, %r9d
	cmpl	%edx, %r9d
	cmovneq	%rax, %r10
	andl	$1023, %r11d            # imm = 0x3FF
	movq	%rdi, %r14
	.p2align	4, 0x90
.LBB9_5:                                #   Parent Loop BB9_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_7 Depth 3
	cmpq	%r10, %r14
	je	.LBB9_2
# BB#6:                                 #   in Loop: Header=BB9_5 Depth=2
	movq	(%r14), %rdx
	addq	$8, %r14
	movq	%r11, %rbx
	.p2align	4, 0x90
.LBB9_7:                                #   Parent Loop BB9_4 Depth=1
                                        #     Parent Loop BB9_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdx,%rbx,4), %esi
	notl	%esi
	testl	(%rcx,%rbx,4), %esi
	jne	.LBB9_9
# BB#8:                                 #   in Loop: Header=BB9_7 Depth=3
	leaq	-1(%rbx), %rsi
	cmpq	$1, %rbx
	movq	%rsi, %rbx
	jg	.LBB9_7
	jmp	.LBB9_10
	.p2align	4, 0x90
.LBB9_9:                                # %._crit_edge
                                        #   in Loop: Header=BB9_5 Depth=2
	movl	%ebx, %esi
.LBB9_10:                               #   in Loop: Header=BB9_5 Depth=2
	testl	%esi, %esi
	jne	.LBB9_5
	jmp	.LBB9_3
	.p2align	4, 0x90
.LBB9_2:                                # %.outer
                                        #   in Loop: Header=BB9_4 Depth=1
	movq	%rcx, (%rax)
	addq	$8, %rax
.LBB9_3:                                # %.loopexit
                                        #   in Loop: Header=BB9_4 Depth=1
	movq	(%r8), %rcx
	testq	%rcx, %rcx
	jne	.LBB9_4
.LBB9_11:                               # %.outer._crit_edge
	movq	$0, (%rax)
	subq	%rdi, %rax
	shrq	$3, %rax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	rm_contain, .Lfunc_end9-rm_contain
	.cfi_endproc

	.globl	rm_rev_contain
	.p2align	4, 0x90
	.type	rm_rev_contain,@function
rm_rev_contain:                         # @rm_rev_contain
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi95:
	.cfi_def_cfa_offset 16
.Lcfi96:
	.cfi_offset %rbx, -16
	movq	(%rdi), %rcx
	testq	%rcx, %rcx
	movq	%rdi, %rax
	je	.LBB10_11
# BB#1:                                 # %.lr.ph.preheader
	movl	$-1, %r9d
	movq	%rdi, %r8
	movq	%rdi, %rax
                                        # implicit-def: %R10
	.p2align	4, 0x90
.LBB10_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_5 Depth 2
                                        #       Child Loop BB10_7 Depth 3
	movl	%r9d, %edx
	addq	$8, %r8
	movl	(%rcx), %r9d
	shrl	$16, %r9d
	cmpl	%edx, %r9d
	cmovneq	%rax, %r10
	movq	%rdi, %r11
	.p2align	4, 0x90
.LBB10_5:                               #   Parent Loop BB10_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_7 Depth 3
	cmpq	%r10, %r11
	je	.LBB10_2
# BB#6:                                 #   in Loop: Header=BB10_5 Depth=2
	movq	(%r11), %rdx
	addq	$8, %r11
	movl	(%rdx), %esi
	andl	$1023, %esi             # imm = 0x3FF
	.p2align	4, 0x90
.LBB10_7:                               #   Parent Loop BB10_4 Depth=1
                                        #     Parent Loop BB10_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rcx,%rsi,4), %ebx
	notl	%ebx
	testl	(%rdx,%rsi,4), %ebx
	jne	.LBB10_9
# BB#8:                                 #   in Loop: Header=BB10_7 Depth=3
	leaq	-1(%rsi), %rbx
	cmpq	$1, %rsi
	movq	%rbx, %rsi
	jg	.LBB10_7
	jmp	.LBB10_10
	.p2align	4, 0x90
.LBB10_9:                               # %._crit_edge
                                        #   in Loop: Header=BB10_5 Depth=2
	movl	%esi, %ebx
.LBB10_10:                              #   in Loop: Header=BB10_5 Depth=2
	testl	%ebx, %ebx
	jne	.LBB10_5
	jmp	.LBB10_3
	.p2align	4, 0x90
.LBB10_2:                               # %.outer
                                        #   in Loop: Header=BB10_4 Depth=1
	movq	%rcx, (%rax)
	addq	$8, %rax
.LBB10_3:                               # %.loopexit
                                        #   in Loop: Header=BB10_4 Depth=1
	movq	(%r8), %rcx
	testq	%rcx, %rcx
	jne	.LBB10_4
.LBB10_11:                              # %.outer._crit_edge
	movq	$0, (%rax)
	subq	%rdi, %rax
	shrq	$3, %rax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	retq
.Lfunc_end10:
	.size	rm_rev_contain, .Lfunc_end10-rm_rev_contain
	.cfi_endproc

	.globl	rm2_equal
	.p2align	4, 0x90
	.type	rm2_equal,@function
rm2_equal:                              # @rm2_equal
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi97:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi98:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi100:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi101:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi103:
	.cfi_def_cfa_offset 64
.Lcfi104:
	.cfi_offset %rbx, -56
.Lcfi105:
	.cfi_offset %r12, -48
.Lcfi106:
	.cfi_offset %r13, -40
.Lcfi107:
	.cfi_offset %r14, -32
.Lcfi108:
	.cfi_offset %r15, -24
.Lcfi109:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	%rbx, %rbp
	movq	%r12, %r14
	movq	%r15, (%rsp)            # 8-byte Spill
	jmp	.LBB11_1
.LBB11_15:                              #   in Loop: Header=BB11_1 Depth=1
	movq	(%rbx), %rax
	addq	$8, %rbx
	movq	%rax, (%r15)
	addq	$8, %r15
	addq	$8, %r12
	.p2align	4, 0x90
.LBB11_1:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.LBB11_6
# BB#2:                                 #   in Loop: Header=BB11_1 Depth=1
	cmpq	$0, (%r12)
	je	.LBB11_3
# BB#11:                                #   in Loop: Header=BB11_1 Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	*%r13
	cmpl	$1, %eax
	je	.LBB11_16
# BB#12:                                #   in Loop: Header=BB11_1 Depth=1
	testl	%eax, %eax
	je	.LBB11_15
# BB#13:                                #   in Loop: Header=BB11_1 Depth=1
	cmpl	$-1, %eax
	jne	.LBB11_1
# BB#14:                                #   in Loop: Header=BB11_1 Depth=1
	movq	(%rbx), %rax
	addq	$8, %rbx
	movq	%rax, (%rbp)
	addq	$8, %rbp
	jmp	.LBB11_1
.LBB11_16:                              #   in Loop: Header=BB11_1 Depth=1
	movq	(%r12), %rax
	addq	$8, %r12
	movq	%rax, (%r14)
	addq	$8, %r14
	jmp	.LBB11_1
.LBB11_3:                               # %.critedge.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_4:                               # %.critedge
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, (%rbp,%rcx)
	movq	8(%rbx,%rcx), %rax
	addq	$8, %rcx
	testq	%rax, %rax
	jne	.LBB11_4
# BB#5:                                 # %.preheader.loopexit
	addq	%rcx, %rbp
.LBB11_6:                               # %.preheader
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.LBB11_10
# BB#7:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_8:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, (%r14,%rcx)
	movq	8(%r12,%rcx), %rax
	addq	$8, %rcx
	testq	%rax, %rax
	jne	.LBB11_8
# BB#9:                                 # %._crit_edge.loopexit
	addq	%rcx, %r14
.LBB11_10:                              # %._crit_edge
	movq	(%rsp), %rax            # 8-byte Reload
	movq	$0, (%r15)
	movq	$0, (%r14)
	movq	$0, (%rbp)
	subq	%rax, %r15
	shrq	$3, %r15
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	rm2_equal, .Lfunc_end11-rm2_equal
	.cfi_endproc

	.globl	rm2_contain
	.p2align	4, 0x90
	.type	rm2_contain,@function
rm2_contain:                            # @rm2_contain
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi110:
	.cfi_def_cfa_offset 16
.Lcfi111:
	.cfi_offset %rbx, -16
	movq	(%rdi), %r10
	testq	%r10, %r10
	movq	%rdi, %rax
	je	.LBB12_12
# BB#1:                                 # %.preheader.lr.ph.preheader
	movq	%rdi, %r8
	movq	%rdi, %rax
	.p2align	4, 0x90
.LBB12_4:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_5 Depth 2
                                        #       Child Loop BB12_8 Depth 3
	addq	$8, %r8
	movq	%rsi, %r9
	.p2align	4, 0x90
.LBB12_5:                               #   Parent Loop BB12_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_8 Depth 3
	movq	(%r9), %r11
	testq	%r11, %r11
	je	.LBB12_2
# BB#6:                                 #   in Loop: Header=BB12_5 Depth=2
	movzwl	2(%r11), %edx
	movl	(%r10), %ecx
	movl	%ecx, %ebx
	shrl	$16, %ebx
	cmpl	%ebx, %edx
	jbe	.LBB12_2
# BB#7:                                 #   in Loop: Header=BB12_5 Depth=2
	addq	$8, %r9
	andl	$1023, %ecx             # imm = 0x3FF
	.p2align	4, 0x90
.LBB12_8:                               #   Parent Loop BB12_4 Depth=1
                                        #     Parent Loop BB12_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r11,%rcx,4), %edx
	notl	%edx
	testl	(%r10,%rcx,4), %edx
	jne	.LBB12_10
# BB#9:                                 #   in Loop: Header=BB12_8 Depth=3
	leaq	-1(%rcx), %rdx
	cmpq	$1, %rcx
	movq	%rdx, %rcx
	jg	.LBB12_8
	jmp	.LBB12_11
	.p2align	4, 0x90
.LBB12_10:                              # %._crit_edge
                                        #   in Loop: Header=BB12_5 Depth=2
	movl	%ecx, %edx
.LBB12_11:                              #   in Loop: Header=BB12_5 Depth=2
	testl	%edx, %edx
	jne	.LBB12_5
	jmp	.LBB12_3
	.p2align	4, 0x90
.LBB12_2:                               # %.critedge
                                        #   in Loop: Header=BB12_4 Depth=1
	movq	%r10, (%rax)
	addq	$8, %rax
.LBB12_3:                               # %.loopexit
                                        #   in Loop: Header=BB12_4 Depth=1
	movq	(%r8), %r10
	testq	%r10, %r10
	jne	.LBB12_4
.LBB12_12:                              # %.outer._crit_edge
	movq	$0, (%rax)
	subq	%rdi, %rax
	shrq	$3, %rax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	retq
.Lfunc_end12:
	.size	rm2_contain, .Lfunc_end12-rm2_contain
	.cfi_endproc

	.globl	sf_sort
	.p2align	4, 0x90
	.type	sf_sort,@function
sf_sort:                                # @sf_sort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi112:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi113:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi114:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi115:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi116:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi117:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi118:
	.cfi_def_cfa_offset 64
.Lcfi119:
	.cfi_offset %rbx, -56
.Lcfi120:
	.cfi_offset %r12, -48
.Lcfi121:
	.cfi_offset %r13, -40
.Lcfi122:
	.cfi_offset %r14, -32
.Lcfi123:
	.cfi_offset %r15, -24
.Lcfi124:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movslq	12(%r12), %rbx
	leaq	8(,%rbx,8), %rdi
	callq	malloc
	movq	%rax, %r15
	movslq	(%r12), %rax
	imulq	%rbx, %rax
	testl	%eax, %eax
	jle	.LBB13_1
# BB#2:                                 # %.lr.ph.preheader
	movq	24(%r12), %rbx
	leaq	(%rbx,%rax,4), %rbp
	movq	%r15, %r13
	.p2align	4, 0x90
.LBB13_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rbx), %eax
	movl	%eax, (%rbx)
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	set_ord
	shll	$16, %eax
	orl	%eax, (%rbx)
	movq	%rbx, (%r13)
	addq	$8, %r13
	movslq	(%r12), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%rbp, %rbx
	jb	.LBB13_3
# BB#4:                                 # %._crit_edge.loopexit
	movl	12(%r12), %ebx
	jmp	.LBB13_5
.LBB13_1:
	movq	%r15, %r13
.LBB13_5:                               # %._crit_edge
	movq	$0, (%r13)
	movslq	%ebx, %rsi
	movl	$8, %edx
	movq	%r15, %rdi
	movq	%r14, %rcx
	callq	qsort
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	sf_sort, .Lfunc_end13-sf_sort
	.cfi_endproc

	.globl	sf_list
	.p2align	4, 0x90
	.type	sf_list,@function
sf_list:                                # @sf_list
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi125:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi127:
	.cfi_def_cfa_offset 32
.Lcfi128:
	.cfi_offset %rbx, -24
.Lcfi129:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movslq	12(%r14), %rbx
	leaq	8(,%rbx,8), %rdi
	callq	malloc
	movslq	(%r14), %rcx
	imulq	%rcx, %rbx
	testl	%ebx, %ebx
	jle	.LBB14_1
# BB#2:                                 # %.lr.ph
	movq	24(%r14), %rdx
	leaq	(%rdx,%rbx,4), %rsi
	shlq	$2, %rcx
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB14_3:                               # =>This Inner Loop Header: Depth=1
	movq	%rdx, (%rdi)
	addq	$8, %rdi
	addq	%rcx, %rdx
	cmpq	%rsi, %rdx
	jb	.LBB14_3
	jmp	.LBB14_4
.LBB14_1:
	movq	%rax, %rdi
.LBB14_4:                               # %._crit_edge
	movq	$0, (%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end14:
	.size	sf_list, .Lfunc_end14-sf_list
	.cfi_endproc

	.globl	sf_unlist
	.p2align	4, 0x90
	.type	sf_unlist,@function
sf_unlist:                              # @sf_unlist
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi130:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi131:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi134:
	.cfi_def_cfa_offset 48
.Lcfi135:
	.cfi_offset %rbx, -40
.Lcfi136:
	.cfi_offset %r14, -32
.Lcfi137:
	.cfi_offset %r15, -24
.Lcfi138:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	xorl	%eax, %eax
	movl	%ebp, %edi
	movl	%edx, %esi
	callq	sf_new
	movq	%rax, %r15
	movl	%ebp, 12(%r15)
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.LBB15_20
# BB#1:                                 # %.lr.ph
	movq	24(%r15), %rcx
	movq	%r14, %r11
	.p2align	4, 0x90
.LBB15_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_9 Depth 2
                                        #     Child Loop BB15_13 Depth 2
                                        #     Child Loop BB15_17 Depth 2
	movl	(%rax), %esi
	andl	$1023, %esi             # imm = 0x3FF
	leaq	1(%rsi), %r10
	cmpq	$8, %r10
	jb	.LBB15_16
# BB#3:                                 # %min.iters.checked
                                        #   in Loop: Header=BB15_2 Depth=1
	movq	%r10, %r9
	andq	$2040, %r9              # imm = 0x7F8
	je	.LBB15_16
# BB#4:                                 # %vector.memcheck
                                        #   in Loop: Header=BB15_2 Depth=1
	leaq	4(%rax,%rsi,4), %rdx
	cmpq	%rdx, %rcx
	jae	.LBB15_6
# BB#5:                                 # %vector.memcheck
                                        #   in Loop: Header=BB15_2 Depth=1
	leaq	4(%rcx,%rsi,4), %rdx
	cmpq	%rdx, %rax
	jb	.LBB15_16
.LBB15_6:                               # %vector.body.preheader
                                        #   in Loop: Header=BB15_2 Depth=1
	leaq	-8(%r9), %r8
	movl	%r8d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB15_7
# BB#8:                                 # %vector.body.prol.preheader
                                        #   in Loop: Header=BB15_2 Depth=1
	leaq	-12(%rcx,%rsi,4), %rdx
	leaq	-12(%rax,%rsi,4), %rbp
	negq	%rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB15_9:                               # %vector.body.prol
                                        #   Parent Loop BB15_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbp,%rbx,4), %xmm0
	movups	-16(%rbp,%rbx,4), %xmm1
	movups	%xmm0, (%rdx,%rbx,4)
	movups	%xmm1, -16(%rdx,%rbx,4)
	addq	$-8, %rbx
	incq	%rdi
	jne	.LBB15_9
# BB#10:                                # %vector.body.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB15_2 Depth=1
	negq	%rbx
	cmpq	$24, %r8
	jae	.LBB15_12
	jmp	.LBB15_14
.LBB15_7:                               #   in Loop: Header=BB15_2 Depth=1
	xorl	%ebx, %ebx
	cmpq	$24, %r8
	jb	.LBB15_14
.LBB15_12:                              # %vector.body.preheader.new
                                        #   in Loop: Header=BB15_2 Depth=1
	movq	%r10, %rdi
	andq	$-8, %rdi
	negq	%rdi
	negq	%rbx
	leaq	-12(%rcx,%rsi,4), %rbp
	leaq	-12(%rax,%rsi,4), %rdx
	.p2align	4, 0x90
.LBB15_13:                              # %vector.body
                                        #   Parent Loop BB15_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdx,%rbx,4), %xmm0
	movups	-16(%rdx,%rbx,4), %xmm1
	movups	%xmm0, (%rbp,%rbx,4)
	movups	%xmm1, -16(%rbp,%rbx,4)
	movups	-32(%rdx,%rbx,4), %xmm0
	movups	-48(%rdx,%rbx,4), %xmm1
	movups	%xmm0, -32(%rbp,%rbx,4)
	movups	%xmm1, -48(%rbp,%rbx,4)
	movups	-64(%rdx,%rbx,4), %xmm0
	movups	-80(%rdx,%rbx,4), %xmm1
	movups	%xmm0, -64(%rbp,%rbx,4)
	movups	%xmm1, -80(%rbp,%rbx,4)
	movups	-96(%rdx,%rbx,4), %xmm0
	movups	-112(%rdx,%rbx,4), %xmm1
	movups	%xmm0, -96(%rbp,%rbx,4)
	movups	%xmm1, -112(%rbp,%rbx,4)
	addq	$-32, %rbx
	cmpq	%rbx, %rdi
	jne	.LBB15_13
.LBB15_14:                              # %middle.block
                                        #   in Loop: Header=BB15_2 Depth=1
	cmpq	%r9, %r10
	je	.LBB15_18
# BB#15:                                #   in Loop: Header=BB15_2 Depth=1
	subq	%r9, %rsi
	.p2align	4, 0x90
.LBB15_16:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB15_2 Depth=1
	incq	%rsi
	.p2align	4, 0x90
.LBB15_17:                              # %scalar.ph
                                        #   Parent Loop BB15_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rax,%rsi,4), %edx
	movl	%edx, -4(%rcx,%rsi,4)
	decq	%rsi
	jg	.LBB15_17
.LBB15_18:                              # %.loopexit
                                        #   in Loop: Header=BB15_2 Depth=1
	movslq	(%r15), %rax
	leaq	(%rcx,%rax,4), %rcx
	movq	8(%r11), %rax
	addq	$8, %r11
	testq	%rax, %rax
	jne	.LBB15_2
# BB#19:                                # %._crit_edge
	testq	%r14, %r14
	je	.LBB15_21
.LBB15_20:                              # %._crit_edge.thread
	movq	%r14, %rdi
	callq	free
.LBB15_21:
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	sf_unlist, .Lfunc_end15-sf_unlist
	.cfi_endproc

	.globl	sf_ind_unlist
	.p2align	4, 0x90
	.type	sf_ind_unlist,@function
sf_ind_unlist:                          # @sf_ind_unlist
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi139:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi140:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi141:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi142:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi143:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi144:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi145:
	.cfi_def_cfa_offset 80
.Lcfi146:
	.cfi_offset %rbx, -56
.Lcfi147:
	.cfi_offset %r12, -48
.Lcfi148:
	.cfi_offset %r13, -40
.Lcfi149:
	.cfi_offset %r14, -32
.Lcfi150:
	.cfi_offset %r15, -24
.Lcfi151:
	.cfi_offset %rbp, -16
	movq	%r8, %r13
	movq	%rcx, %rbp
	movl	%esi, %ebx
	movq	%rdi, %r15
	xorl	%eax, %eax
	movl	%ebx, %edi
	movl	%edx, %esi
	callq	sf_new
	movq	%rax, %r14
	movl	%ebx, 12(%r14)
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movslq	%ebx, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %r12
	movq	%r15, (%rsp)            # 8-byte Spill
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.LBB16_8
# BB#1:                                 # %.lr.ph53
	movq	24(%r14), %rcx
	xorl	%r11d, %r11d
	movq	(%rsp), %r15            # 8-byte Reload
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB16_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_16 Depth 2
                                        #     Child Loop BB16_20 Depth 2
                                        #     Child Loop BB16_24 Depth 2
	movl	(%rax), %edx
	andl	$1023, %edx             # imm = 0x3FF
	leaq	1(%rdx), %r10
	cmpq	$8, %r10
	jb	.LBB16_23
# BB#3:                                 # %min.iters.checked
                                        #   in Loop: Header=BB16_2 Depth=1
	movq	%r10, %r8
	andq	$2040, %r8              # imm = 0x7F8
	je	.LBB16_23
# BB#4:                                 # %vector.memcheck
                                        #   in Loop: Header=BB16_2 Depth=1
	leaq	4(%rax,%rdx,4), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB16_6
# BB#5:                                 # %vector.memcheck
                                        #   in Loop: Header=BB16_2 Depth=1
	leaq	4(%rcx,%rdx,4), %rsi
	cmpq	%rsi, %rax
	jb	.LBB16_23
.LBB16_6:                               # %vector.body.preheader
                                        #   in Loop: Header=BB16_2 Depth=1
	leaq	-8(%r8), %r9
	movl	%r9d, %ebx
	shrl	$3, %ebx
	incl	%ebx
	andq	$3, %rbx
	je	.LBB16_7
# BB#15:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB16_2 Depth=1
	leaq	-12(%rcx,%rdx,4), %rsi
	leaq	-12(%rax,%rdx,4), %rdi
	negq	%rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB16_16:                              # %vector.body.prol
                                        #   Parent Loop BB16_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rbp,4), %xmm0
	movups	-16(%rdi,%rbp,4), %xmm1
	movups	%xmm0, (%rsi,%rbp,4)
	movups	%xmm1, -16(%rsi,%rbp,4)
	addq	$-8, %rbp
	incq	%rbx
	jne	.LBB16_16
# BB#17:                                # %vector.body.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB16_2 Depth=1
	negq	%rbp
	cmpq	$24, %r9
	jae	.LBB16_19
	jmp	.LBB16_21
.LBB16_7:                               #   in Loop: Header=BB16_2 Depth=1
	xorl	%ebp, %ebp
	cmpq	$24, %r9
	jb	.LBB16_21
.LBB16_19:                              # %vector.body.preheader.new
                                        #   in Loop: Header=BB16_2 Depth=1
	movq	%r10, %rbx
	andq	$-8, %rbx
	negq	%rbx
	negq	%rbp
	leaq	-12(%rcx,%rdx,4), %rsi
	leaq	-12(%rax,%rdx,4), %rdi
	.p2align	4, 0x90
.LBB16_20:                              # %vector.body
                                        #   Parent Loop BB16_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rbp,4), %xmm0
	movups	-16(%rdi,%rbp,4), %xmm1
	movups	%xmm0, (%rsi,%rbp,4)
	movups	%xmm1, -16(%rsi,%rbp,4)
	movups	-32(%rdi,%rbp,4), %xmm0
	movups	-48(%rdi,%rbp,4), %xmm1
	movups	%xmm0, -32(%rsi,%rbp,4)
	movups	%xmm1, -48(%rsi,%rbp,4)
	movups	-64(%rdi,%rbp,4), %xmm0
	movups	-80(%rdi,%rbp,4), %xmm1
	movups	%xmm0, -64(%rsi,%rbp,4)
	movups	%xmm1, -80(%rsi,%rbp,4)
	movups	-96(%rdi,%rbp,4), %xmm0
	movups	-112(%rdi,%rbp,4), %xmm1
	movups	%xmm0, -96(%rsi,%rbp,4)
	movups	%xmm1, -112(%rsi,%rbp,4)
	addq	$-32, %rbp
	cmpq	%rbp, %rbx
	jne	.LBB16_20
.LBB16_21:                              # %middle.block
                                        #   in Loop: Header=BB16_2 Depth=1
	cmpq	%r8, %r10
	movq	16(%rsp), %rbp          # 8-byte Reload
	je	.LBB16_25
# BB#22:                                #   in Loop: Header=BB16_2 Depth=1
	subq	%r8, %rdx
	.p2align	4, 0x90
.LBB16_23:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB16_2 Depth=1
	incq	%rdx
	.p2align	4, 0x90
.LBB16_24:                              # %scalar.ph
                                        #   Parent Loop BB16_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rax,%rdx,4), %esi
	movl	%esi, -4(%rcx,%rdx,4)
	decq	%rdx
	jg	.LBB16_24
.LBB16_25:                              # %.loopexit
                                        #   in Loop: Header=BB16_2 Depth=1
	subq	%r13, %rax
	sarq	$2, %rax
	movslq	(%r14), %rsi
	cqto
	idivq	%rsi
	movl	(%rbp,%rax,4), %eax
	movl	%eax, (%r12,%r11,4)
	leaq	(%rcx,%rsi,4), %rcx
	incq	%r11
	movq	8(%r15), %rax
	leaq	8(%r15), %r15
	testq	%rax, %rax
	jne	.LBB16_2
.LBB16_8:                               # %.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB16_10
# BB#9:                                 # %.lr.ph.preheader
	decl	%eax
	leaq	4(,%rax,4), %rdx
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	memcpy
.LBB16_10:                              # %._crit_edge
	testq	%r12, %r12
	je	.LBB16_12
# BB#11:
	movq	%r12, %rdi
	callq	free
.LBB16_12:
	movq	(%rsp), %rdi            # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB16_14
# BB#13:
	callq	free
.LBB16_14:
	movq	%r14, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	sf_ind_unlist, .Lfunc_end16-sf_ind_unlist
	.cfi_endproc

	.globl	sf_merge
	.p2align	4, 0x90
	.type	sf_merge,@function
sf_merge:                               # @sf_merge
	.cfi_startproc
# BB#0:                                 # %.lr.ph86
	pushq	%rbp
.Lcfi152:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi153:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi154:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi155:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi156:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi157:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi158:
	.cfi_def_cfa_offset 112
.Lcfi159:
	.cfi_offset %rbx, -56
.Lcfi160:
	.cfi_offset %r12, -48
.Lcfi161:
	.cfi_offset %r13, -40
.Lcfi162:
	.cfi_offset %r14, -32
.Lcfi163:
	.cfi_offset %r15, -24
.Lcfi164:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r12
	xorl	%eax, %eax
	movl	%ebp, %edi
	movl	%r8d, %esi
	callq	sf_new
	movq	%rax, %r13
	movl	%ebp, 12(%r13)
	movq	24(%r13), %rbp
	movq	%r12, (%rsp)
	movq	%r15, 8(%rsp)
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movq	%rbx, 16(%rsp)
	movq	$-1, %rbx
	movq	%r12, %r14
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movq	%r15, %rax
	movq	%r12, 32(%rsp)          # 8-byte Spill
	jmp	.LBB17_1
	.p2align	4, 0x90
.LBB17_4:                               # %._crit_edge
                                        #   in Loop: Header=BB17_1 Depth=1
	movq	24(%rsp,%rbx,8), %rax
	incq	%rbx
.LBB17_1:                               # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rdi
	movq	(%rax), %rsi
	xorl	%eax, %eax
	callq	desc1
	testl	%eax, %eax
	jle	.LBB17_3
# BB#2:                                 #   in Loop: Header=BB17_1 Depth=1
	movq	16(%rsp,%rbx,8), %r12
	movq	%r14, 16(%rsp,%rbx,8)
	movq	%r12, (%rsp)
	movq	%r12, %r14
.LBB17_3:                               #   in Loop: Header=BB17_1 Depth=1
	testq	%rbx, %rbx
	jne	.LBB17_4
# BB#32:
	movq	8(%rsp), %rbx
	movq	16(%rsp), %r14
	movq	(%rbx), %rdi
	movq	(%r14), %rsi
	xorl	%eax, %eax
	callq	desc1
	testl	%eax, %eax
	jle	.LBB17_34
# BB#33:
	movq	%rbx, 16(%rsp)
	movq	%r14, 8(%rsp)
.LBB17_34:                              # %.loopexit.1
	movq	(%rsp), %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.LBB17_25
# BB#35:                                # %.lr.ph.lr.ph
	movq	8(%rsp), %r12
	movq	16(%rsp), %r15
.LBB17_6:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_7 Depth 2
                                        #       Child Loop BB17_14 Depth 3
                                        #       Child Loop BB17_18 Depth 3
                                        #       Child Loop BB17_22 Depth 3
	movq	%r14, %rcx
	movq	%r12, %r14
	movq	%r15, %r12
	movq	%rcx, %r15
	.p2align	4, 0x90
.LBB17_7:                               #   Parent Loop BB17_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_14 Depth 3
                                        #       Child Loop BB17_18 Depth 3
                                        #       Child Loop BB17_22 Depth 3
	movl	(%rax), %ecx
	andl	$1023, %ecx             # imm = 0x3FF
	leaq	1(%rcx), %r10
	cmpq	$8, %r10
	jb	.LBB17_21
# BB#8:                                 # %min.iters.checked
                                        #   in Loop: Header=BB17_7 Depth=2
	movq	%r10, %r9
	andq	$2040, %r9              # imm = 0x7F8
	je	.LBB17_21
# BB#9:                                 # %vector.memcheck
                                        #   in Loop: Header=BB17_7 Depth=2
	leaq	4(%rax,%rcx,4), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB17_11
# BB#10:                                # %vector.memcheck
                                        #   in Loop: Header=BB17_7 Depth=2
	leaq	4(%rbp,%rcx,4), %rdx
	cmpq	%rdx, %rax
	jb	.LBB17_21
.LBB17_11:                              # %vector.body.preheader
                                        #   in Loop: Header=BB17_7 Depth=2
	leaq	-8(%r9), %r8
	movl	%r8d, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB17_12
# BB#13:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB17_7 Depth=2
	leaq	-12(%rbp,%rcx,4), %rsi
	leaq	-12(%rax,%rcx,4), %rbx
	negq	%rdx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB17_14:                              # %vector.body.prol
                                        #   Parent Loop BB17_6 Depth=1
                                        #     Parent Loop BB17_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	(%rbx,%rdi,4), %xmm0
	movups	-16(%rbx,%rdi,4), %xmm1
	movups	%xmm0, (%rsi,%rdi,4)
	movups	%xmm1, -16(%rsi,%rdi,4)
	addq	$-8, %rdi
	incq	%rdx
	jne	.LBB17_14
# BB#15:                                # %vector.body.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB17_7 Depth=2
	negq	%rdi
	cmpq	$24, %r8
	jae	.LBB17_17
	jmp	.LBB17_19
.LBB17_12:                              #   in Loop: Header=BB17_7 Depth=2
	xorl	%edi, %edi
	cmpq	$24, %r8
	jb	.LBB17_19
.LBB17_17:                              # %vector.body.preheader.new
                                        #   in Loop: Header=BB17_7 Depth=2
	movq	%r10, %rdx
	andq	$-8, %rdx
	negq	%rdx
	negq	%rdi
	leaq	-12(%rbp,%rcx,4), %rsi
	leaq	-12(%rax,%rcx,4), %rbx
	.p2align	4, 0x90
.LBB17_18:                              # %vector.body
                                        #   Parent Loop BB17_6 Depth=1
                                        #     Parent Loop BB17_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	(%rbx,%rdi,4), %xmm0
	movups	-16(%rbx,%rdi,4), %xmm1
	movups	%xmm0, (%rsi,%rdi,4)
	movups	%xmm1, -16(%rsi,%rdi,4)
	movups	-32(%rbx,%rdi,4), %xmm0
	movups	-48(%rbx,%rdi,4), %xmm1
	movups	%xmm0, -32(%rsi,%rdi,4)
	movups	%xmm1, -48(%rsi,%rdi,4)
	movups	-64(%rbx,%rdi,4), %xmm0
	movups	-80(%rbx,%rdi,4), %xmm1
	movups	%xmm0, -64(%rsi,%rdi,4)
	movups	%xmm1, -80(%rsi,%rdi,4)
	movups	-96(%rbx,%rdi,4), %xmm0
	movups	-112(%rbx,%rdi,4), %xmm1
	movups	%xmm0, -96(%rsi,%rdi,4)
	movups	%xmm1, -112(%rsi,%rdi,4)
	addq	$-32, %rdi
	cmpq	%rdi, %rdx
	jne	.LBB17_18
.LBB17_19:                              # %middle.block
                                        #   in Loop: Header=BB17_7 Depth=2
	cmpq	%r9, %r10
	je	.LBB17_23
# BB#20:                                #   in Loop: Header=BB17_7 Depth=2
	subq	%r9, %rcx
	.p2align	4, 0x90
.LBB17_21:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB17_7 Depth=2
	incq	%rcx
	.p2align	4, 0x90
.LBB17_22:                              # %scalar.ph
                                        #   Parent Loop BB17_6 Depth=1
                                        #     Parent Loop BB17_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rax,%rcx,4), %edx
	movl	%edx, -4(%rbp,%rcx,4)
	decq	%rcx
	jg	.LBB17_22
.LBB17_23:                              # %.loopexit
                                        #   in Loop: Header=BB17_7 Depth=2
	movslq	(%r13), %rax
	leaq	(%rbp,%rax,4), %rbp
	movq	8(%r15), %rdi
	addq	$8, %r15
	movq	(%r12), %rsi
	xorl	%eax, %eax
	callq	desc1
	testl	%eax, %eax
	jg	.LBB17_5
# BB#24:                                #   in Loop: Header=BB17_7 Depth=2
	movq	(%r15), %rdi
	movq	(%r14), %rsi
	xorl	%eax, %eax
	callq	desc1
	testl	%eax, %eax
	movq	%r15, %rcx
	cmovgq	%r14, %rcx
	cmovgq	%r15, %r14
	movq	(%rcx), %rax
	testq	%rax, %rax
	movq	%rcx, %r15
	jne	.LBB17_7
	jmp	.LBB17_25
	.p2align	4, 0x90
.LBB17_5:                               # %.outer.loopexit
                                        #   in Loop: Header=BB17_6 Depth=1
	movq	(%r14), %rax
	testq	%rax, %rax
	jne	.LBB17_6
.LBB17_25:                              # %.outer._crit_edge
	movq	32(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB17_27
# BB#26:
	callq	free
.LBB17_27:
	movq	40(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB17_29
# BB#28:
	callq	free
.LBB17_29:
	movq	48(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB17_31
# BB#30:
	callq	free
.LBB17_31:
	movq	%r13, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	sf_merge, .Lfunc_end17-sf_merge
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
