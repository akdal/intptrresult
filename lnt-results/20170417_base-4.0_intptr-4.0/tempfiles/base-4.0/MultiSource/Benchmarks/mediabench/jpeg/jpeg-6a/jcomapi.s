	.text
	.file	"jcomapi.bc"
	.globl	jpeg_abort
	.p2align	4, 0x90
	.type	jpeg_abort,@function
jpeg_abort:                             # @jpeg_abort
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movl	$1, %esi
	callq	*72(%rax)
	cmpl	$0, 24(%rbx)
	movl	$200, %eax
	movl	$100, %ecx
	cmovnel	%eax, %ecx
	movl	%ecx, 28(%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	jpeg_abort, .Lfunc_end0-jpeg_abort
	.cfi_endproc

	.globl	jpeg_destroy
	.p2align	4, 0x90
	.type	jpeg_destroy,@function
jpeg_destroy:                           # @jpeg_destroy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.LBB1_2
# BB#1:
	movq	%rbx, %rdi
	callq	*80(%rax)
.LBB1_2:
	movq	$0, 8(%rbx)
	movl	$0, 28(%rbx)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	jpeg_destroy, .Lfunc_end1-jpeg_destroy
	.cfi_endproc

	.globl	jpeg_alloc_quant_table
	.p2align	4, 0x90
	.type	jpeg_alloc_quant_table,@function
jpeg_alloc_quant_table:                 # @jpeg_alloc_quant_table
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 16
	movq	8(%rdi), %rax
	xorl	%esi, %esi
	movl	$132, %edx
	callq	*(%rax)
	movl	$0, 128(%rax)
	popq	%rcx
	retq
.Lfunc_end2:
	.size	jpeg_alloc_quant_table, .Lfunc_end2-jpeg_alloc_quant_table
	.cfi_endproc

	.globl	jpeg_alloc_huff_table
	.p2align	4, 0x90
	.type	jpeg_alloc_huff_table,@function
jpeg_alloc_huff_table:                  # @jpeg_alloc_huff_table
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	movq	8(%rdi), %rax
	xorl	%esi, %esi
	movl	$280, %edx              # imm = 0x118
	callq	*(%rax)
	movl	$0, 276(%rax)
	popq	%rcx
	retq
.Lfunc_end3:
	.size	jpeg_alloc_huff_table, .Lfunc_end3-jpeg_alloc_huff_table
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
