	.text
	.file	"mbuffer.bc"
	.globl	dump_dpb
	.p2align	4, 0x90
	.type	dump_dpb,@function
dump_dpb:                               # @dump_dpb
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	dump_dpb, .Lfunc_end0-dump_dpb
	.cfi_endproc

	.globl	getDpbSize
	.p2align	4, 0x90
	.type	getDpbSize,@function
getDpbSize:                             # @getDpbSize
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	active_sps(%rip), %rcx
	movl	1140(%rcx), %eax
	movl	1144(%rcx), %edx
	incl	%edx
	cmpl	$1, 1148(%rcx)
	movl	$1, %ebx
	adcl	$0, %ebx
	leal	(%rax,%rax,2), %eax
	shll	$7, %eax
	addl	$384, %eax              # imm = 0x180
	imull	%edx, %ebx
	imull	%eax, %ebx
	movl	24(%rcx), %edx
	addl	$-9, %edx
	cmpl	$42, %edx
	ja	.LBB1_16
# BB#1:
	movl	$152064, %eax           # imm = 0x25200
	jmpq	*.LJTI1_0(,%rdx,8)
.LBB1_5:
	movl	$912384, %eax           # imm = 0xDEC00
	jmp	.LBB1_17
.LBB1_7:
	movl	$3110400, %eax          # imm = 0x2F7600
	jmp	.LBB1_17
.LBB1_10:
	movl	$12582912, %eax         # imm = 0xC00000
	jmp	.LBB1_17
.LBB1_2:
	cmpl	$99, 4(%rcx)
	ja	.LBB1_17
# BB#3:
	cmpl	$0, 20(%rcx)
	jne	.LBB1_17
# BB#4:
	movl	$345600, %eax           # imm = 0x54600
	jmp	.LBB1_17
.LBB1_16:
	movl	$.L.str, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	xorl	%eax, %eax
	jmp	.LBB1_17
.LBB1_6:
	movl	$1824768, %eax          # imm = 0x1BD800
	jmp	.LBB1_17
.LBB1_8:
	movl	$6912000, %eax          # imm = 0x697800
	jmp	.LBB1_17
.LBB1_9:
	movl	$7864320, %eax          # imm = 0x780000
	jmp	.LBB1_17
.LBB1_11:
	movl	4(%rcx), %ecx
	addl	$-100, %ecx
	cmpl	$44, %ecx
	ja	.LBB1_13
# BB#12:
	movl	$13369344, %eax         # imm = 0xCC0000
	movabsq	$17592190239745, %rdx   # imm = 0x100000400401
	btq	%rcx, %rdx
	jb	.LBB1_17
.LBB1_13:
	movl	$12582912, %eax         # imm = 0xC00000
	jmp	.LBB1_17
.LBB1_14:
	movl	$42393600, %eax         # imm = 0x286E000
	jmp	.LBB1_17
.LBB1_15:
	movl	$70778880, %eax         # imm = 0x4380000
.LBB1_17:
	cltd
	idivl	%ebx
	cmpl	$17, %eax
	movl	$16, %ecx
	cmovgel	%ecx, %eax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	getDpbSize, .Lfunc_end1-getDpbSize
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_17
	.quad	.LBB1_17
	.quad	.LBB1_2
	.quad	.LBB1_5
	.quad	.LBB1_5
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_5
	.quad	.LBB1_6
	.quad	.LBB1_7
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_7
	.quad	.LBB1_8
	.quad	.LBB1_9
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_10
	.quad	.LBB1_10
	.quad	.LBB1_11
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_14
	.quad	.LBB1_15

	.text
	.globl	check_num_ref
	.p2align	4, 0x90
	.type	check_num_ref,@function
check_num_ref:                          # @check_num_ref
	.cfi_startproc
# BB#0:
	movl	dpb+32(%rip), %eax
	addl	dpb+36(%rip), %eax
	movq	img(%rip), %rcx
	movl	28(%rcx), %ecx
	testl	%ecx, %ecx
	movl	$1, %edx
	cmovgl	%ecx, %edx
	cmpl	%edx, %eax
	jle	.LBB2_1
# BB#2:
	movl	$.L.str.1, %edi
	movl	$500, %esi              # imm = 0x1F4
	jmp	error                   # TAILCALL
.LBB2_1:
	retq
.Lfunc_end2:
	.size	check_num_ref, .Lfunc_end2-check_num_ref
	.cfi_endproc

	.globl	init_dpb
	.p2align	4, 0x90
	.type	init_dpb,@function
init_dpb:                               # @init_dpb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -32
.Lcfi6:
	.cfi_offset %r14, -24
.Lcfi7:
	.cfi_offset %rbp, -16
	cmpl	$0, dpb+48(%rip)
	je	.LBB3_2
# BB#1:
	callq	free_dpb
.LBB3_2:
	movq	active_sps(%rip), %rcx
	movl	1140(%rcx), %eax
	movl	1144(%rcx), %edx
	incl	%edx
	cmpl	$1, 1148(%rcx)
	movl	$1, %ebx
	adcl	$0, %ebx
	leal	(%rax,%rax,2), %eax
	shll	$7, %eax
	addl	$384, %eax              # imm = 0x180
	imull	%edx, %ebx
	imull	%eax, %ebx
	movl	24(%rcx), %edx
	addl	$-9, %edx
	cmpl	$42, %edx
	ja	.LBB3_18
# BB#3:
	movl	$152064, %eax           # imm = 0x25200
	jmpq	*.LJTI3_0(,%rdx,8)
.LBB3_7:
	movl	$912384, %eax           # imm = 0xDEC00
	jmp	.LBB3_19
.LBB3_9:
	movl	$3110400, %eax          # imm = 0x2F7600
	jmp	.LBB3_19
.LBB3_12:
	movl	$12582912, %eax         # imm = 0xC00000
	jmp	.LBB3_19
.LBB3_4:
	cmpl	$99, 4(%rcx)
	ja	.LBB3_19
# BB#5:
	cmpl	$0, 20(%rcx)
	jne	.LBB3_19
# BB#6:
	movl	$345600, %eax           # imm = 0x54600
	jmp	.LBB3_19
.LBB3_18:
	movl	$.L.str, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	xorl	%eax, %eax
	jmp	.LBB3_19
.LBB3_8:
	movl	$1824768, %eax          # imm = 0x1BD800
	jmp	.LBB3_19
.LBB3_10:
	movl	$6912000, %eax          # imm = 0x697800
	jmp	.LBB3_19
.LBB3_11:
	movl	$7864320, %eax          # imm = 0x780000
	jmp	.LBB3_19
.LBB3_13:
	movl	4(%rcx), %ecx
	addl	$-100, %ecx
	cmpl	$44, %ecx
	ja	.LBB3_15
# BB#14:
	movl	$13369344, %eax         # imm = 0xCC0000
	movabsq	$17592190239745, %rdx   # imm = 0x100000400401
	btq	%rcx, %rdx
	jb	.LBB3_19
.LBB3_15:
	movl	$12582912, %eax         # imm = 0xC00000
	jmp	.LBB3_19
.LBB3_16:
	movl	$42393600, %eax         # imm = 0x286E000
	jmp	.LBB3_19
.LBB3_17:
	movl	$70778880, %eax         # imm = 0x4380000
.LBB3_19:                               # %getDpbSize.exit
	cltd
	idivl	%ebx
	cmpl	$17, %eax
	movl	$16, %r14d
	cmovll	%eax, %r14d
	movl	%r14d, dpb+24(%rip)
	movq	input(%rip), %rax
	cmpl	32(%rax), %r14d
	jae	.LBB3_21
# BB#20:
	movl	$.L.str.2, %edi
	movl	$1000, %esi             # imm = 0x3E8
	callq	error
	movl	dpb+24(%rip), %r14d
.LBB3_21:
	movl	$0, dpb+28(%rip)
	movq	$0, dpb+56(%rip)
	movq	$0, dpb+32(%rip)
	movl	%r14d, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, dpb(%rip)
	testq	%rax, %rax
	jne	.LBB3_23
# BB#22:
	movl	$.L.str.3, %edi
	callq	no_mem_exit
	movl	dpb+24(%rip), %r14d
.LBB3_23:
	movl	%r14d, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, dpb+8(%rip)
	testq	%rax, %rax
	jne	.LBB3_25
# BB#24:
	movl	$.L.str.4, %edi
	callq	no_mem_exit
	movl	dpb+24(%rip), %r14d
.LBB3_25:
	movl	%r14d, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, dpb+16(%rip)
	testq	%rax, %rax
	jne	.LBB3_27
# BB#26:
	movl	$.L.str.5, %edi
	callq	no_mem_exit
	movl	dpb+24(%rip), %r14d
.LBB3_27:                               # %.preheader21
	testl	%r14d, %r14d
	je	.LBB3_32
# BB#28:                                # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_29:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %edi
	movl	$64, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB3_31
# BB#30:                                #   in Loop: Header=BB3_29 Depth=1
	movl	$.L.str.7, %edi
	callq	no_mem_exit
	movl	dpb+24(%rip), %r14d
.LBB3_31:                               # %alloc_frame_store.exit
                                        #   in Loop: Header=BB3_29 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movups	%xmm0, 40(%rbx)
	movq	$0, 56(%rbx)
	movq	dpb(%rip), %rax
	movl	%ebp, %ecx
	movq	%rbx, (%rax,%rcx,8)
	movq	dpb+8(%rip), %rax
	movq	$0, (%rax,%rcx,8)
	movq	dpb+16(%rip), %rax
	movq	$0, (%rax,%rcx,8)
	incl	%ebp
	cmpl	%r14d, %ebp
	jb	.LBB3_29
.LBB3_32:                               # %.preheader20.preheader
	movl	$33, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, listX(%rip)
	testq	%rax, %rax
	jne	.LBB3_34
# BB#33:
	movl	$.L.str.6, %edi
	callq	no_mem_exit
.LBB3_34:                               # %.preheader20.129
	movl	$33, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, listX+8(%rip)
	testq	%rax, %rax
	jne	.LBB3_36
# BB#35:
	movl	$.L.str.6, %edi
	callq	no_mem_exit
.LBB3_36:                               # %.preheader20.230
	movl	$33, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, listX+16(%rip)
	testq	%rax, %rax
	jne	.LBB3_38
# BB#37:
	movl	$.L.str.6, %edi
	callq	no_mem_exit
.LBB3_38:                               # %.preheader20.331
	movl	$33, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, listX+24(%rip)
	testq	%rax, %rax
	jne	.LBB3_40
# BB#39:
	movl	$.L.str.6, %edi
	callq	no_mem_exit
.LBB3_40:                               # %.preheader20.432
	movl	$33, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, listX+32(%rip)
	testq	%rax, %rax
	jne	.LBB3_42
# BB#41:
	movl	$.L.str.6, %edi
	callq	no_mem_exit
.LBB3_42:                               # %.preheader20.533
	movl	$33, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, listX+40(%rip)
	testq	%rax, %rax
	jne	.LBB3_43
# BB#46:
	movl	$.L.str.6, %edi
	callq	no_mem_exit
.LBB3_43:                               # %.preheader.preheader
	movq	$-24, %rax
	.p2align	4, 0x90
.LBB3_44:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, (%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 8(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 16(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 24(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 32(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 40(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 48(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 56(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 64(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 72(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 80(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 88(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 96(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 104(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 112(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 120(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 128(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 136(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 144(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 152(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 160(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 168(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 176(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 184(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 192(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 200(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 208(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 216(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 224(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 232(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 240(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 248(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 256(%rcx)
	movl	$0, listXsize+24(%rax)
	addq	$4, %rax
	jne	.LBB3_44
# BB#45:
	movl	$-2147483648, dpb+40(%rip) # imm = 0x80000000
	movq	img(%rip), %rax
	movl	$0, 15428(%rax)
	movl	$1, dpb+48(%rip)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	init_dpb, .Lfunc_end3-init_dpb
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_19
	.quad	.LBB3_19
	.quad	.LBB3_4
	.quad	.LBB3_7
	.quad	.LBB3_7
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_7
	.quad	.LBB3_8
	.quad	.LBB3_9
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_9
	.quad	.LBB3_10
	.quad	.LBB3_11
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_13
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_18
	.quad	.LBB3_16
	.quad	.LBB3_17

	.text
	.globl	free_dpb
	.p2align	4, 0x90
	.type	free_dpb,@function
free_dpb:                               # @free_dpb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	dpb(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_13
# BB#1:                                 # %.preheader
	movl	dpb+24(%rip), %eax
	testl	%eax, %eax
	je	.LBB4_12
# BB#2:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %ecx
	movq	(%rdi,%rcx,8), %rbx
	testq	%rbx, %rbx
	je	.LBB4_11
# BB#4:                                 #   in Loop: Header=BB4_3 Depth=1
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_6
# BB#5:                                 #   in Loop: Header=BB4_3 Depth=1
	callq	free_storable_picture
	movq	$0, 40(%rbx)
.LBB4_6:                                #   in Loop: Header=BB4_3 Depth=1
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_8
# BB#7:                                 #   in Loop: Header=BB4_3 Depth=1
	callq	free_storable_picture
	movq	$0, 48(%rbx)
.LBB4_8:                                #   in Loop: Header=BB4_3 Depth=1
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_10
# BB#9:                                 #   in Loop: Header=BB4_3 Depth=1
	callq	free_storable_picture
.LBB4_10:                               #   in Loop: Header=BB4_3 Depth=1
	movq	%rbx, %rdi
	callq	free
	movl	dpb+24(%rip), %eax
	movq	dpb(%rip), %rdi
.LBB4_11:                               # %free_frame_store.exit
                                        #   in Loop: Header=BB4_3 Depth=1
	incl	%ebp
	cmpl	%eax, %ebp
	jb	.LBB4_3
.LBB4_12:                               # %._crit_edge
	callq	free
	movq	$0, dpb(%rip)
.LBB4_13:
	movq	dpb+8(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_15
# BB#14:
	callq	free
.LBB4_15:
	movq	dpb+16(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_17
# BB#16:
	callq	free
.LBB4_17:
	movl	$-2147483648, dpb+40(%rip) # imm = 0x80000000
	movq	listX(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_19
# BB#18:
	callq	free
	movq	$0, listX(%rip)
.LBB4_19:
	movq	listX+8(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_21
# BB#20:
	callq	free
	movq	$0, listX+8(%rip)
.LBB4_21:
	movq	listX+16(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_23
# BB#22:
	callq	free
	movq	$0, listX+16(%rip)
.LBB4_23:
	movq	listX+24(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_25
# BB#24:
	callq	free
	movq	$0, listX+24(%rip)
.LBB4_25:
	movq	listX+32(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_27
# BB#26:
	callq	free
	movq	$0, listX+32(%rip)
.LBB4_27:
	movq	listX+40(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_29
# BB#28:
	callq	free
	movq	$0, listX+40(%rip)
.LBB4_29:
	movl	$0, dpb+48(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end4:
	.size	free_dpb, .Lfunc_end4-free_dpb
	.cfi_endproc

	.globl	alloc_frame_store
	.p2align	4, 0x90
	.type	alloc_frame_store,@function
alloc_frame_store:                      # @alloc_frame_store
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movl	$1, %edi
	movl	$64, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB5_2
# BB#1:
	movl	$.L.str.7, %edi
	callq	no_mem_exit
.LBB5_2:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movups	%xmm0, 40(%rbx)
	movq	$0, 56(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	alloc_frame_store, .Lfunc_end5-alloc_frame_store
	.cfi_endproc

	.globl	free_frame_store
	.p2align	4, 0x90
	.type	free_frame_store,@function
free_frame_store:                       # @free_frame_store
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 16
.Lcfi16:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB6_8
# BB#1:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_3
# BB#2:
	callq	free_storable_picture
	movq	$0, 40(%rbx)
.LBB6_3:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_5
# BB#4:
	callq	free_storable_picture
	movq	$0, 48(%rbx)
.LBB6_5:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_7
# BB#6:
	callq	free_storable_picture
.LBB6_7:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB6_8:
	popq	%rbx
	retq
.Lfunc_end6:
	.size	free_frame_store, .Lfunc_end6-free_frame_store
	.cfi_endproc

	.globl	alloc_storable_picture
	.p2align	4, 0x90
	.type	alloc_storable_picture,@function
alloc_storable_picture:                 # @alloc_storable_picture
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi20:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 80
.Lcfi24:
	.cfi_offset %rbx, -56
.Lcfi25:
	.cfi_offset %r12, -48
.Lcfi26:
	.cfi_offset %r13, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movl	%r8d, %r14d
	movl	%ecx, %r15d
	movl	%edx, %r12d
	movl	%esi, %ebp
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movl	$1, %edi
	movl	$6592, %esi             # imm = 0x19C0
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB7_2
# BB#1:
	movl	$.L.str.8, %edi
	callq	no_mem_exit
.LBB7_2:
	movq	$0, 6448(%rbx)
	leaq	6440(%rbx), %rdi
	xorps	%xmm0, %xmm0
	movups	%xmm0, 6464(%rbx)
	movl	%r12d, %esi
	movl	%ebp, %edx
	callq	get_mem2Dpel
	movq	img(%rip), %rax
	cmpl	$0, 15536(%rax)
	je	.LBB7_4
# BB#3:
	leaq	6472(%rbx), %rdi
	movl	$2, %esi
	movl	%r14d, %edx
	movl	%r15d, %ecx
	callq	get_mem3Dpel
	movq	img(%rip), %rax
.LBB7_4:
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movl	15348(%rax), %edi
	movl	$4, %esi
	callq	calloc
	movq	%rax, 6480(%rbx)
	testq	%rax, %rax
	jne	.LBB7_6
# BB#5:
	movl	$.L.str.9, %edi
	callq	no_mem_exit
.LBB7_6:
	leaq	6488(%rbx), %rdi
	movl	%r12d, %r13d
	sarl	$31, %r13d
	shrl	$30, %r13d
	addl	%r12d, %r13d
	sarl	$2, %r13d
	movl	%ebp, %r14d
	sarl	$31, %r14d
	shrl	$30, %r14d
	addl	%ebp, %r14d
	sarl	$2, %r14d
	movl	$2, %esi
	movl	%r13d, %edx
	movl	%r14d, %ecx
	callq	get_mem3D
	leaq	6496(%rbx), %rdi
	movl	$6, %esi
	movl	%r13d, %edx
	movl	%r14d, %ecx
	callq	get_mem3Dint64
	leaq	6504(%rbx), %rdi
	movl	$6, %esi
	movl	%r13d, %edx
	movl	%r14d, %ecx
	callq	get_mem3Dint64
	leaq	6512(%rbx), %rdi
	movl	$2, %esi
	movl	$2, %r8d
	movl	%r13d, %edx
	movl	%r14d, %ecx
	callq	get_mem4Dshort
	leaq	6520(%rbx), %rdi
	movl	%r13d, %esi
	movl	%r14d, %edx
	callq	get_mem2D
	movq	%rbx, %rdi
	addq	$6528, %rdi             # imm = 0x1980
	movl	%r13d, %esi
	movl	%r14d, %edx
	callq	get_mem2D
	xorps	%xmm0, %xmm0
	movups	%xmm0, 6376(%rbx)
	movups	%xmm0, 6360(%rbx)
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rbx)
	movl	%ebp, 6392(%rbx)
	movl	%r12d, 6396(%rbx)
	addl	$23, %ebp
	movl	%ebp, 6408(%rbx)
	addl	$23, %r12d
	movl	%r12d, 6412(%rbx)
	movl	%r15d, 6400(%rbx)
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	%edx, 6404(%rbx)
	movl	img_pad_size_uv_x(%rip), %eax
	leal	-1(%r15,%rax,2), %eax
	movq	img(%rip), %rcx
	subl	15544(%rcx), %eax
	movl	%eax, 6416(%rbx)
	movl	img_pad_size_uv_y(%rip), %eax
	leal	-1(%rdx,%rax,2), %eax
	subl	15548(%rcx), %eax
	movl	%eax, 6420(%rbx)
	movl	$0, 6428(%rbx)
	movl	$0, 6432(%rbx)
	movups	%xmm0, 6536(%rbx)
	movq	$0, 6552(%rbx)
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	alloc_storable_picture, .Lfunc_end7-alloc_storable_picture
	.cfi_endproc

	.globl	free_storable_picture
	.p2align	4, 0x90
	.type	free_storable_picture,@function
free_storable_picture:                  # @free_storable_picture
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 16
.Lcfi31:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB8_32
# BB#1:
	movq	6488(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_3
# BB#2:
	movl	$2, %esi
	callq	free_mem3D
	movq	$0, 6488(%rbx)
.LBB8_3:
	movq	6496(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_5
# BB#4:
	movl	$6, %esi
	callq	free_mem3Dint64
	movq	$0, 6496(%rbx)
.LBB8_5:
	movq	6504(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_7
# BB#6:
	movl	$6, %esi
	callq	free_mem3Dint64
	movq	$0, 6504(%rbx)
.LBB8_7:
	movq	6512(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_9
# BB#8:
	movl	6396(%rbx), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$30, %edx
	addl	%eax, %edx
	sarl	$2, %edx
	movl	$2, %esi
	callq	free_mem4Dshort
	movq	$0, 6512(%rbx)
.LBB8_9:
	movq	6520(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_11
# BB#10:
	callq	free_mem2D
	movq	$0, 6520(%rbx)
.LBB8_11:
	movq	6528(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_13
# BB#12:
	callq	free_mem2D
	movq	$0, 6528(%rbx)
.LBB8_13:
	movq	6440(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_15
# BB#14:
	callq	free_mem2Dpel
	movq	$0, 6440(%rbx)
.LBB8_15:
	movq	6448(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_17
# BB#16:
	movl	$4, %esi
	movl	$4, %edx
	callq	free_mem4Dpel
	movq	$0, 6448(%rbx)
.LBB8_17:
	movq	6464(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_27
# BB#18:
	movq	img(%rip), %rax
	movl	15536(%rax), %eax
	testl	%eax, %eax
	je	.LBB8_27
# BB#19:
	movq	input(%rip), %rcx
	cmpl	$0, 5772(%rcx)
	je	.LBB8_27
# BB#20:
	movl	$2, %esi
	cmpl	$2, %eax
	je	.LBB8_24
# BB#21:
	cmpl	$1, %eax
	jne	.LBB8_25
# BB#22:
	movl	$8, %edx
	movl	$8, %ecx
	jmp	.LBB8_26
.LBB8_32:
	popq	%rbx
	retq
.LBB8_24:
	movl	$4, %edx
	movl	$8, %ecx
	jmp	.LBB8_26
.LBB8_25:
	movl	$4, %edx
	movl	$4, %ecx
.LBB8_26:
	callq	free_mem5Dpel
	movq	$0, 6464(%rbx)
.LBB8_27:
	movq	6472(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_29
# BB#28:
	movl	$2, %esi
	callq	free_mem3Dpel
	movq	$0, 6472(%rbx)
.LBB8_29:
	movq	6480(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_31
# BB#30:
	callq	free
.LBB8_31:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end8:
	.size	free_storable_picture, .Lfunc_end8-free_storable_picture
	.cfi_endproc

	.globl	is_short_ref
	.p2align	4, 0x90
	.type	is_short_ref,@function
is_short_ref:                           # @is_short_ref
	.cfi_startproc
# BB#0:
	cmpl	$0, 6380(%rdi)
	je	.LBB9_1
# BB#2:
	cmpl	$0, 6376(%rdi)
	sete	%al
	movzbl	%al, %eax
	retq
.LBB9_1:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.Lfunc_end9:
	.size	is_short_ref, .Lfunc_end9-is_short_ref
	.cfi_endproc

	.globl	is_long_ref
	.p2align	4, 0x90
	.type	is_long_ref,@function
is_long_ref:                            # @is_long_ref
	.cfi_startproc
# BB#0:
	cmpl	$0, 6380(%rdi)
	je	.LBB10_1
# BB#2:
	cmpl	$0, 6376(%rdi)
	setne	%al
	movzbl	%al, %eax
	retq
.LBB10_1:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.Lfunc_end10:
	.size	is_long_ref, .Lfunc_end10-is_long_ref
	.cfi_endproc

	.globl	init_lists
	.p2align	4, 0x90
	.type	init_lists,@function
init_lists:                             # @init_lists
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi38:
	.cfi_def_cfa_offset 80
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movl	%esi, %r12d
	movl	log2_max_frame_num_minus4(%rip), %ecx
	addl	$4, %ecx
	movl	$1, %r9d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r9d
	testl	%r12d, %r12d
	je	.LBB11_1
# BB#23:
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	cmpl	$1, %r12d
	sete	%r10b
	setne	%r11b
	movl	dpb+32(%rip), %r8d
	testq	%r8, %r8
	je	.LBB11_31
# BB#24:                                # %.lr.ph380
	movq	dpb+8(%rip), %r15
	movq	img(%rip), %r14
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB11_25:                              # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rax,8), %rdx
	movl	4(%rdx), %esi
	testl	%esi, %esi
	je	.LBB11_30
# BB#26:                                #   in Loop: Header=BB11_25 Depth=1
	movl	20(%rdx), %ecx
	cmpl	15332(%r14), %ecx
	movl	$0, %ebx
	cmoval	%r9d, %ebx
	subl	%ebx, %ecx
	movl	%ecx, 24(%rdx)
	testb	$1, %sil
	je	.LBB11_28
# BB#27:                                #   in Loop: Header=BB11_25 Depth=1
	leal	(%r10,%rcx,2), %ebx
	movq	48(%rdx), %rbp
	movl	%ebx, 6364(%rbp)
.LBB11_28:                              #   in Loop: Header=BB11_25 Depth=1
	testb	$2, %sil
	je	.LBB11_30
# BB#29:                                #   in Loop: Header=BB11_25 Depth=1
	leal	(%r11,%rcx,2), %ecx
	movq	56(%rdx), %rdx
	movl	%ecx, 6364(%rdx)
.LBB11_30:                              #   in Loop: Header=BB11_25 Depth=1
	incq	%rax
	cmpq	%r8, %rax
	jb	.LBB11_25
.LBB11_31:                              # %.preheader294
	movl	dpb+36(%rip), %r9d
	testq	%r9, %r9
	je	.LBB11_38
# BB#32:                                # %.lr.ph377
	movq	dpb+16(%rip), %rdx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_33:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rbp,8), %rcx
	movl	8(%rcx), %esi
	testb	$1, %sil
	je	.LBB11_35
# BB#34:                                #   in Loop: Header=BB11_33 Depth=1
	movq	48(%rcx), %rbx
	movl	6372(%rbx), %eax
	leal	(%r10,%rax,2), %eax
	movl	%eax, 6368(%rbx)
.LBB11_35:                              #   in Loop: Header=BB11_33 Depth=1
	testb	$2, %sil
	je	.LBB11_37
# BB#36:                                #   in Loop: Header=BB11_33 Depth=1
	movq	56(%rcx), %rax
	movl	6372(%rax), %ecx
	leal	(%r11,%rcx,2), %ecx
	movl	%ecx, 6368(%rax)
.LBB11_37:                              #   in Loop: Header=BB11_33 Depth=1
	incq	%rbp
	cmpq	%r9, %rbp
	jb	.LBB11_33
	jmp	.LBB11_38
.LBB11_1:                               # %.preheader293
	movl	dpb+32(%rip), %r8d
	testq	%r8, %r8
	je	.LBB11_8
# BB#2:                                 # %.lr.ph375
	movq	dpb+8(%rip), %rax
	movq	img(%rip), %r10
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB11_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rsi,8), %rdx
	cmpl	$3, (%rdx)
	jne	.LBB11_7
# BB#4:                                 #   in Loop: Header=BB11_3 Depth=1
	movq	40(%rdx), %rbp
	cmpl	$0, 6380(%rbp)
	je	.LBB11_7
# BB#5:                                 #   in Loop: Header=BB11_3 Depth=1
	cmpl	$0, 6376(%rbp)
	jne	.LBB11_7
# BB#6:                                 #   in Loop: Header=BB11_3 Depth=1
	movl	20(%rdx), %ebx
	cmpl	15332(%r10), %ebx
	movl	$0, %ecx
	cmoval	%r9d, %ecx
	subl	%ecx, %ebx
	movl	%ebx, 24(%rdx)
	movl	%ebx, 6364(%rbp)
	.p2align	4, 0x90
.LBB11_7:                               #   in Loop: Header=BB11_3 Depth=1
	incq	%rsi
	cmpq	%r8, %rsi
	jb	.LBB11_3
.LBB11_8:                               # %.preheader291
	movl	dpb+36(%rip), %ecx
	testq	%rcx, %rcx
	je	.LBB11_38
# BB#9:                                 # %.lr.ph373
	movq	dpb+16(%rip), %rdx
	testb	$1, %cl
	jne	.LBB11_11
# BB#10:
	xorl	%esi, %esi
	cmpl	$1, %ecx
	jne	.LBB11_16
	jmp	.LBB11_38
.LBB11_11:
	movq	(%rdx), %rax
	cmpl	$3, (%rax)
	jne	.LBB11_14
# BB#12:
	movq	40(%rax), %rax
	cmpl	$0, 6376(%rax)
	je	.LBB11_14
# BB#13:
	movl	6372(%rax), %esi
	movl	%esi, 6368(%rax)
.LBB11_14:                              # %.prol.loopexit514
	movl	$1, %esi
	cmpl	$1, %ecx
	je	.LBB11_38
	.p2align	4, 0x90
.LBB11_16:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rsi,8), %rax
	cmpl	$3, (%rax)
	jne	.LBB11_19
# BB#17:                                #   in Loop: Header=BB11_16 Depth=1
	movq	40(%rax), %rax
	cmpl	$0, 6376(%rax)
	je	.LBB11_19
# BB#18:                                #   in Loop: Header=BB11_16 Depth=1
	movl	6372(%rax), %ebp
	movl	%ebp, 6368(%rax)
.LBB11_19:                              #   in Loop: Header=BB11_16 Depth=1
	movq	8(%rdx,%rsi,8), %rax
	cmpl	$3, (%rax)
	jne	.LBB11_22
# BB#20:                                #   in Loop: Header=BB11_16 Depth=1
	movq	40(%rax), %rax
	cmpl	$0, 6376(%rax)
	je	.LBB11_22
# BB#21:                                #   in Loop: Header=BB11_16 Depth=1
	movl	6372(%rax), %ebp
	movl	%ebp, 6368(%rax)
.LBB11_22:                              #   in Loop: Header=BB11_16 Depth=1
	addq	$2, %rsi
	cmpq	%rcx, %rsi
	jb	.LBB11_16
.LBB11_38:                              # %.loopexit292
	movabsq	$17179869184, %r13      # imm = 0x400000000
	cmpl	$4, %edi
	ja	.LBB11_87
# BB#39:                                # %.loopexit292
	movl	%edi, %eax
	jmpq	*.LJTI11_0(,%rax,8)
.LBB11_41:
	testl	%r12d, %r12d
	je	.LBB11_42
# BB#57:
	movl	dpb+24(%rip), %ebp
	movl	$8, %esi
	movq	%rbp, %rdi
	callq	calloc
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB11_59
# BB#58:
	movl	$.L.str.10, %edi
	callq	no_mem_exit
	movl	dpb+24(%rip), %ebp
.LBB11_59:
	movl	%ebp, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB11_61
# BB#60:
	movl	$.L.str.11, %edi
	callq	no_mem_exit
.LBB11_61:                              # %.preheader290
	movl	dpb+32(%rip), %eax
	xorl	%ebx, %ebx
	testq	%rax, %rax
	movl	$0, %edx
	je	.LBB11_74
# BB#62:                                # %.lr.ph369
	movq	dpb+8(%rip), %rcx
	testb	$1, %al
	jne	.LBB11_64
# BB#63:
	xorl	%esi, %esi
	xorl	%edx, %edx
	cmpl	$1, %eax
	jne	.LBB11_69
	jmp	.LBB11_74
.LBB11_40:
	movq	$0, listXsize(%rip)
	jmp	.LBB11_189
.LBB11_87:
	testl	%r12d, %r12d
	je	.LBB11_88
# BB#128:
	movl	dpb+24(%rip), %ebx
	movl	$8, %esi
	movq	%rbx, %rdi
	callq	calloc
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.LBB11_130
# BB#129:
	movl	$.L.str.10, %edi
	callq	no_mem_exit
	movl	dpb+24(%rip), %ebx
.LBB11_130:
	movl	%ebx, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB11_132
# BB#131:
	movl	$.L.str.12, %edi
	callq	no_mem_exit
	movl	dpb+24(%rip), %ebx
.LBB11_132:
	movl	%ebx, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	jne	.LBB11_134
# BB#133:
	movl	$.L.str.11, %edi
	callq	no_mem_exit
.LBB11_134:
	movl	%r12d, 4(%rsp)          # 4-byte Spill
	movabsq	$4294967296, %rax       # imm = 0x100000000
	movq	%rax, listXsize(%rip)
	movl	dpb+32(%rip), %eax
	testq	%rax, %rax
	je	.LBB11_135
# BB#136:                               # %.lr.ph347
	xorl	%ecx, %ecx
	movq	dpb+8(%rip), %rdx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB11_137:                             # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rcx,8), %rsi
	cmpl	$0, (%rsi)
	je	.LBB11_140
# BB#138:                               #   in Loop: Header=BB11_137 Depth=1
	movq	img(%rip), %rdi
	movl	15328(%rdi), %edi
	cmpl	36(%rsi), %edi
	jl	.LBB11_140
# BB#139:                               #   in Loop: Header=BB11_137 Depth=1
	movslq	%r12d, %rdi
	incl	%r12d
	movq	%rsi, (%r13,%rdi,8)
.LBB11_140:                             #   in Loop: Header=BB11_137 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB11_137
	jmp	.LBB11_141
.LBB11_42:                              # %.preheader289
	testl	%r8d, %r8d
	je	.LBB11_43
# BB#44:                                # %.lr.ph358
	movl	%r8d, %eax
	xorl	%ecx, %ecx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_45:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+8(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	cmpl	$3, (%rdx)
	jne	.LBB11_49
# BB#46:                                #   in Loop: Header=BB11_45 Depth=1
	movq	40(%rdx), %rdx
	cmpl	$0, 6380(%rdx)
	je	.LBB11_49
# BB#47:                                #   in Loop: Header=BB11_45 Depth=1
	cmpl	$0, 6376(%rdx)
	jne	.LBB11_49
# BB#48:                                #   in Loop: Header=BB11_45 Depth=1
	movq	listX(%rip), %rsi
	movslq	%ebp, %rdi
	incl	%ebp
	movq	%rdx, (%rsi,%rdi,8)
	.p2align	4, 0x90
.LBB11_49:                              #   in Loop: Header=BB11_45 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB11_45
	jmp	.LBB11_50
.LBB11_64:
	movq	(%rcx), %rdx
	cmpl	$0, 4(%rdx)
	je	.LBB11_65
# BB#66:
	movq	%rdx, (%r15)
	movl	$1, %edx
	jmp	.LBB11_67
.LBB11_88:                              # %.preheader287
	testl	%r8d, %r8d
	je	.LBB11_89
# BB#90:                                # %.lr.ph323
	movl	%r8d, %eax
	xorl	%ecx, %ecx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB11_91:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+8(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	cmpl	$3, (%rdx)
	jne	.LBB11_96
# BB#92:                                #   in Loop: Header=BB11_91 Depth=1
	movq	40(%rdx), %rdx
	cmpl	$0, 6380(%rdx)
	je	.LBB11_96
# BB#93:                                #   in Loop: Header=BB11_91 Depth=1
	cmpl	$0, 6376(%rdx)
	jne	.LBB11_96
# BB#94:                                #   in Loop: Header=BB11_91 Depth=1
	movq	img(%rip), %rsi
	movl	15324(%rsi), %esi
	cmpl	4(%rdx), %esi
	jle	.LBB11_96
# BB#95:                                #   in Loop: Header=BB11_91 Depth=1
	movq	listX(%rip), %rsi
	movslq	%r15d, %rdi
	incl	%r15d
	movq	%rdx, (%rsi,%rdi,8)
	.p2align	4, 0x90
.LBB11_96:                              #   in Loop: Header=BB11_91 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB11_91
	jmp	.LBB11_97
.LBB11_43:
	xorl	%ebp, %ebp
.LBB11_50:                              # %._crit_edge359
	movq	listX(%rip), %rdi
	movslq	%ebp, %rbx
	movl	$8, %edx
	movl	$compare_pic_by_pic_num_desc, %ecx
	movq	%rbx, %rsi
	callq	qsort
	movl	%ebx, listXsize(%rip)
	movl	dpb+36(%rip), %eax
	testq	%rax, %rax
	je	.LBB11_56
# BB#51:                                # %.lr.ph353
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_52:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+16(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	cmpl	$3, (%rdx)
	jne	.LBB11_55
# BB#53:                                #   in Loop: Header=BB11_52 Depth=1
	movq	40(%rdx), %rdx
	cmpl	$0, 6376(%rdx)
	je	.LBB11_55
# BB#54:                                #   in Loop: Header=BB11_52 Depth=1
	movq	listX(%rip), %rsi
	movslq	%ebp, %rdi
	incl	%ebp
	movq	%rdx, (%rsi,%rdi,8)
.LBB11_55:                              #   in Loop: Header=BB11_52 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB11_52
.LBB11_56:                              # %._crit_edge354
	movq	listX(%rip), %rax
	leaq	(%rax,%rbx,8), %rdi
	movl	%ebp, %eax
	subl	%ebx, %eax
	movslq	%eax, %rsi
	movl	$8, %edx
	movl	$compare_pic_by_lt_pic_num_asc, %ecx
	callq	qsort
	movl	%ebp, listXsize(%rip)
	jmp	.LBB11_86
.LBB11_135:
	xorl	%r12d, %r12d
.LBB11_141:                             # %._crit_edge348
	movslq	%r12d, %rbx
	movl	$8, %edx
	movl	$compare_fs_by_poc_desc, %ecx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	qsort
	movl	dpb+32(%rip), %eax
	testq	%rax, %rax
	movl	%r12d, %ebp
	je	.LBB11_147
# BB#142:                               # %.lr.ph341
	movq	dpb+8(%rip), %rcx
	xorl	%edx, %edx
	movl	%r12d, %ebp
	.p2align	4, 0x90
.LBB11_143:                             # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	cmpl	$0, (%rsi)
	je	.LBB11_146
# BB#144:                               #   in Loop: Header=BB11_143 Depth=1
	movq	img(%rip), %rdi
	movl	15328(%rdi), %edi
	cmpl	36(%rsi), %edi
	jge	.LBB11_146
# BB#145:                               #   in Loop: Header=BB11_143 Depth=1
	movslq	%ebp, %rdi
	incl	%ebp
	movq	%rsi, (%r13,%rdi,8)
.LBB11_146:                             #   in Loop: Header=BB11_143 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jb	.LBB11_143
.LBB11_147:                             # %._crit_edge342
	leaq	(%r13,%rbx,8), %rdi
	movl	%ebp, %eax
	subl	%ebx, %eax
	movslq	%eax, %r15
	movl	$8, %edx
	movl	$compare_fs_by_poc_asc, %ecx
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	%r15, %rsi
	callq	qsort
	testl	%ebx, %ebx
	jle	.LBB11_149
# BB#148:                               # %.lr.ph337.preheader
	leaq	(%r14,%r15,8), %rdi
	leal	-1(%r12), %eax
	leaq	8(,%rax,8), %rdx
	movq	%r13, %rsi
	callq	memcpy
.LBB11_149:                             # %.preheader288
	cmpl	%r12d, %ebp
	movq	%r14, %r15
	jle	.LBB11_151
# BB#150:                               # %.lr.ph333.preheader
	leal	-1(%rbp), %eax
	subl	%r12d, %eax
	leaq	8(,%rax,8), %rdx
	movq	%r15, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	memcpy
.LBB11_151:                             # %._crit_edge334
	movq	$0, listXsize(%rip)
	movq	listX(%rip), %rcx
	xorl	%ebx, %ebx
	movl	$listXsize, %r8d
	xorl	%r9d, %r9d
	movl	4(%rsp), %r14d          # 4-byte Reload
	movl	%r14d, %edi
	movq	%r13, %rsi
	movl	%ebp, %edx
	callq	gen_pic_list_from_frame_list
	movq	listX+8(%rip), %rcx
	movl	$listXsize+4, %r8d
	xorl	%r9d, %r9d
	movl	%r14d, %edi
	movq	%r15, %rsi
	movl	%ebp, %edx
	callq	gen_pic_list_from_frame_list
	movl	dpb+36(%rip), %eax
	testq	%rax, %rax
	movq	16(%rsp), %r12          # 8-byte Reload
	je	.LBB11_162
# BB#152:                               # %.lr.ph329
	movq	dpb+16(%rip), %rcx
	cmpl	$4, %eax
	jb	.LBB11_153
# BB#154:                               # %min.iters.checked459
	movl	%eax, %r8d
	andl	$3, %r8d
	movq	%rax, %rbx
	subq	%r8, %rbx
	je	.LBB11_153
# BB#155:                               # %vector.memcheck472
	leaq	(%rcx,%rax,8), %rsi
	cmpq	%rsi, %r12
	jae	.LBB11_157
# BB#156:                               # %vector.memcheck472
	leaq	(%r12,%rax,8), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB11_157
.LBB11_153:
	xorl	%ebx, %ebx
	xorl	%edi, %edi
.LBB11_160:                             # %scalar.ph457.preheader
	movslq	%edi, %rdx
	leaq	(%r12,%rdx,8), %rdx
	movq	%rbx, %rsi
	movl	%edi, %ebx
	.p2align	4, 0x90
.LBB11_161:                             # %scalar.ph457
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rdi
	incl	%ebx
	movq	%rdi, (%rdx)
	incq	%rsi
	addq	$8, %rdx
	cmpq	%rax, %rsi
	jb	.LBB11_161
.LBB11_162:                             # %._crit_edge330
	movslq	%ebx, %rbx
	movl	$8, %edx
	movl	$compare_fs_by_lt_pic_idx_asc, %ecx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	qsort
	movq	listX(%rip), %rcx
	movl	$listXsize, %r8d
	movl	$1, %r9d
	movl	%r14d, %edi
	movq	%r12, %rsi
	movl	%ebx, %edx
	callq	gen_pic_list_from_frame_list
	movq	listX+8(%rip), %rcx
	movl	$listXsize+4, %r8d
	movl	$1, %r9d
	movl	%r14d, %edi
	movq	%r12, %rsi
	movl	%ebx, %edx
	callq	gen_pic_list_from_frame_list
	movq	%r13, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	movl	listXsize+4(%rip), %r15d
	jmp	.LBB11_163
.LBB11_65:
	xorl	%edx, %edx
.LBB11_67:                              # %.prol.loopexit510
	movl	$1, %esi
	cmpl	$1, %eax
	je	.LBB11_74
	.p2align	4, 0x90
.LBB11_69:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rdi
	cmpl	$0, 4(%rdi)
	je	.LBB11_71
# BB#70:                                #   in Loop: Header=BB11_69 Depth=1
	movslq	%edx, %rbp
	incl	%edx
	movq	%rdi, (%r15,%rbp,8)
.LBB11_71:                              #   in Loop: Header=BB11_69 Depth=1
	movq	8(%rcx,%rsi,8), %rdi
	cmpl	$0, 4(%rdi)
	je	.LBB11_73
# BB#72:                                #   in Loop: Header=BB11_69 Depth=1
	movslq	%edx, %rbp
	incl	%edx
	movq	%rdi, (%r15,%rbp,8)
.LBB11_73:                              #   in Loop: Header=BB11_69 Depth=1
	addq	$2, %rsi
	cmpq	%rax, %rsi
	jb	.LBB11_69
.LBB11_74:                              # %._crit_edge370
	movslq	%edx, %rbp
	movl	$8, %edx
	movl	$compare_fs_by_frame_num_desc, %ecx
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	qsort
	movl	$0, listXsize(%rip)
	movq	listX(%rip), %rcx
	movl	$listXsize, %r8d
	xorl	%r9d, %r9d
	movl	%r12d, %edi
	movq	%r15, %rsi
	movl	%ebp, %edx
	callq	gen_pic_list_from_frame_list
	movl	dpb+36(%rip), %eax
	testq	%rax, %rax
	je	.LBB11_85
# BB#75:                                # %.lr.ph364
	movq	dpb+16(%rip), %rcx
	cmpl	$4, %eax
	jb	.LBB11_76
# BB#77:                                # %min.iters.checked
	movl	%eax, %r8d
	andl	$3, %r8d
	movq	%rax, %rbx
	subq	%r8, %rbx
	je	.LBB11_76
# BB#78:                                # %vector.memcheck
	leaq	(%rcx,%rax,8), %rsi
	cmpq	%rsi, %r14
	jae	.LBB11_80
# BB#79:                                # %vector.memcheck
	leaq	(%r14,%rax,8), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB11_80
.LBB11_76:
	xorl	%ebx, %ebx
	xorl	%edi, %edi
.LBB11_83:                              # %scalar.ph.preheader
	movslq	%edi, %rdx
	leaq	(%r14,%rdx,8), %rdx
	movq	%rbx, %rsi
	movl	%edi, %ebx
	.p2align	4, 0x90
.LBB11_84:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rdi
	incl	%ebx
	movq	%rdi, (%rdx)
	incq	%rsi
	addq	$8, %rdx
	cmpq	%rax, %rsi
	jb	.LBB11_84
.LBB11_85:                              # %._crit_edge365
	movslq	%ebx, %rbp
	movl	$8, %edx
	movl	$compare_fs_by_lt_pic_idx_asc, %ecx
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	qsort
	movq	listX(%rip), %rcx
	movl	$listXsize, %r8d
	movl	$1, %r9d
	movl	%r12d, %edi
	movq	%r14, %rsi
	movl	%ebp, %edx
	callq	gen_pic_list_from_frame_list
	movq	%r15, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
.LBB11_86:
	movl	$0, listXsize+4(%rip)
	xorl	%r15d, %r15d
.LBB11_163:
	movl	listXsize(%rip), %r9d
	cmpl	$2, %r9d
	jl	.LBB11_175
# BB#164:
	cmpl	%r15d, %r9d
	jne	.LBB11_175
# BB#165:                               # %.preheader
	testl	%r15d, %r15d
	jle	.LBB11_174
# BB#166:                               # %.lr.ph301
	movq	listX(%rip), %rcx
	movq	listX+8(%rip), %rdx
	movslq	%r15d, %r10
	leaq	-1(%r10), %r8
	movq	%r10, %rsi
	xorl	%edi, %edi
	andq	$3, %rsi
	je	.LBB11_167
# BB#168:                               # %.prol.preheader
	movl	$1, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_169:                             # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdi,8), %rax
	cmpq	(%rdx,%rdi,8), %rax
	cmovnel	%ebx, %ebp
	incq	%rdi
	cmpq	%rdi, %rsi
	jne	.LBB11_169
	jmp	.LBB11_170
.LBB11_167:
	xorl	%ebp, %ebp
.LBB11_170:                             # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB11_173
# BB#171:                               # %.lr.ph301.new
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB11_172:                             # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdi,8), %rax
	cmpq	(%rdx,%rdi,8), %rax
	movq	8(%rcx,%rdi,8), %rax
	cmovnel	%ebx, %ebp
	cmpq	8(%rdx,%rdi,8), %rax
	movq	16(%rcx,%rdi,8), %rax
	cmovnel	%ebx, %ebp
	cmpq	16(%rdx,%rdi,8), %rax
	movq	24(%rcx,%rdi,8), %rax
	cmovnel	%ebx, %ebp
	cmpq	24(%rdx,%rdi,8), %rax
	cmovnel	%ebx, %ebp
	addq	$4, %rdi
	cmpq	%r10, %rdi
	jl	.LBB11_172
.LBB11_173:                             # %._crit_edge302
	testl	%ebp, %ebp
	jne	.LBB11_175
.LBB11_174:                             # %._crit_edge302.thread
	movq	listX+8(%rip), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	%rdx, (%rax)
	movq	listX+8(%rip), %rax
	movq	%rcx, 8(%rax)
.LBB11_175:
	movq	img(%rip), %rax
	movl	14456(%rax), %edx
	cmpl	%edx, %r9d
	cmovlel	%r9d, %edx
	movl	%edx, listXsize(%rip)
	movl	14460(%rax), %eax
	cmpl	%eax, %r15d
	cmovlel	%r15d, %eax
	movl	%eax, listXsize+4(%rip)
	cmpl	$32, %edx
	ja	.LBB11_182
# BB#176:                               # %.lr.ph298.preheader
	movl	%edx, %ecx
	movl	$33, %esi
	subl	%edx, %esi
	movl	$32, %edx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB11_179
# BB#177:                               # %.lr.ph298.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB11_178:                             # %.lr.ph298.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	listX(%rip), %rdi
	movq	$0, (%rdi,%rcx,8)
	incq	%rcx
	incq	%rsi
	jne	.LBB11_178
.LBB11_179:                             # %.lr.ph298.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB11_182
# BB#180:                               # %.lr.ph298.preheader.new
	addq	$-33, %rcx
	.p2align	4, 0x90
.LBB11_181:                             # %.lr.ph298
                                        # =>This Inner Loop Header: Depth=1
	movq	listX(%rip), %rdx
	movq	$0, 264(%rdx,%rcx,8)
	movq	listX(%rip), %rdx
	movq	$0, 272(%rdx,%rcx,8)
	movq	listX(%rip), %rdx
	movq	$0, 280(%rdx,%rcx,8)
	movq	listX(%rip), %rdx
	movq	$0, 288(%rdx,%rcx,8)
	movq	listX(%rip), %rdx
	movq	$0, 296(%rdx,%rcx,8)
	movq	listX(%rip), %rdx
	movq	$0, 304(%rdx,%rcx,8)
	movq	listX(%rip), %rdx
	movq	$0, 312(%rdx,%rcx,8)
	movq	listX(%rip), %rdx
	movq	$0, 320(%rdx,%rcx,8)
	addq	$8, %rcx
	jne	.LBB11_181
.LBB11_182:                             # %._crit_edge
	cmpl	$32, %eax
	ja	.LBB11_189
# BB#183:                               # %.lr.ph.preheader
	movl	%eax, %ecx
	movl	$33, %edx
	subl	%eax, %edx
	movl	$32, %eax
	subq	%rcx, %rax
	andq	$7, %rdx
	je	.LBB11_186
# BB#184:                               # %.lr.ph.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB11_185:                             # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	listX+8(%rip), %rsi
	movq	$0, (%rsi,%rcx,8)
	incq	%rcx
	incq	%rdx
	jne	.LBB11_185
.LBB11_186:                             # %.lr.ph.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB11_189
# BB#187:                               # %.lr.ph.preheader.new
	addq	$-33, %rcx
	.p2align	4, 0x90
.LBB11_188:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	listX+8(%rip), %rax
	movq	$0, 264(%rax,%rcx,8)
	movq	listX+8(%rip), %rax
	movq	$0, 272(%rax,%rcx,8)
	movq	listX+8(%rip), %rax
	movq	$0, 280(%rax,%rcx,8)
	movq	listX+8(%rip), %rax
	movq	$0, 288(%rax,%rcx,8)
	movq	listX+8(%rip), %rax
	movq	$0, 296(%rax,%rcx,8)
	movq	listX+8(%rip), %rax
	movq	$0, 304(%rax,%rcx,8)
	movq	listX+8(%rip), %rax
	movq	$0, 312(%rax,%rcx,8)
	movq	listX+8(%rip), %rax
	movq	$0, 320(%rax,%rcx,8)
	addq	$8, %rcx
	jne	.LBB11_188
.LBB11_189:                             # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_89:
	xorl	%r15d, %r15d
.LBB11_97:                              # %._crit_edge324
	movq	listX(%rip), %rdi
	movslq	%r15d, %rbx
	movl	$8, %edx
	movl	$compare_pic_by_poc_desc, %ecx
	movq	%rbx, %rsi
	callq	qsort
	movl	dpb+32(%rip), %eax
	testq	%rax, %rax
	movl	%r15d, %r14d
	je	.LBB11_105
# BB#98:                                # %.lr.ph318
	xorl	%ecx, %ecx
	movl	%r15d, %r14d
	.p2align	4, 0x90
.LBB11_99:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+8(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	cmpl	$3, (%rdx)
	jne	.LBB11_104
# BB#100:                               #   in Loop: Header=BB11_99 Depth=1
	movq	40(%rdx), %rdx
	cmpl	$0, 6380(%rdx)
	je	.LBB11_104
# BB#101:                               #   in Loop: Header=BB11_99 Depth=1
	cmpl	$0, 6376(%rdx)
	jne	.LBB11_104
# BB#102:                               #   in Loop: Header=BB11_99 Depth=1
	movq	img(%rip), %rsi
	movl	15324(%rsi), %esi
	cmpl	4(%rdx), %esi
	jge	.LBB11_104
# BB#103:                               #   in Loop: Header=BB11_99 Depth=1
	movq	listX(%rip), %rsi
	movslq	%r14d, %rdi
	incl	%r14d
	movq	%rdx, (%rsi,%rdi,8)
	.p2align	4, 0x90
.LBB11_104:                             #   in Loop: Header=BB11_99 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB11_99
.LBB11_105:                             # %._crit_edge319
	movq	listX(%rip), %rax
	leaq	(%rax,%rbx,8), %rdi
	movl	%r14d, %eax
	subl	%ebx, %eax
	movslq	%eax, %rbp
	movl	$8, %edx
	movl	$compare_pic_by_poc_asc, %ecx
	movq	%rbp, %rsi
	callq	qsort
	testl	%ebx, %ebx
	jle	.LBB11_113
# BB#106:                               # %.lr.ph314.preheader
	movl	%r15d, %r9d
	leaq	-1(%r9), %r8
	movq	%r9, %rsi
	andq	$3, %rsi
	je	.LBB11_107
# BB#108:                               # %.lr.ph314.prol.preheader
	leaq	(,%rbp,8), %rdi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_109:                             # %.lr.ph314.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	listX(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	listX+8(%rip), %rax
	addq	%rdi, %rax
	movq	%rdx, (%rax,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB11_109
	jmp	.LBB11_110
.LBB11_80:                              # %vector.body.preheader
	leaq	16(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB11_81:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movq	%rdx, %rdi
	sarq	$29, %rdi
	movups	%xmm0, (%r14,%rdi)
	movups	%xmm1, 16(%r14,%rdi)
	addq	%r13, %rdx
	addq	$32, %rsi
	addq	$-4, %rbp
	jne	.LBB11_81
# BB#82:                                # %middle.block
	testl	%r8d, %r8d
	movl	%ebx, %edi
	jne	.LBB11_83
	jmp	.LBB11_85
.LBB11_107:
	xorl	%ecx, %ecx
.LBB11_110:                             # %.lr.ph314.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB11_113
# BB#111:                               # %.lr.ph314.preheader.new
	shlq	$3, %rbp
	.p2align	4, 0x90
.LBB11_112:                             # %.lr.ph314
                                        # =>This Inner Loop Header: Depth=1
	movq	listX(%rip), %rax
	movq	(%rax,%rcx,8), %rax
	movq	listX+8(%rip), %rdx
	addq	%rbp, %rdx
	movq	%rax, (%rdx,%rcx,8)
	movq	listX(%rip), %rax
	movq	8(%rax,%rcx,8), %rax
	movq	listX+8(%rip), %rdx
	addq	%rbp, %rdx
	movq	%rax, 8(%rdx,%rcx,8)
	movq	listX(%rip), %rax
	movq	16(%rax,%rcx,8), %rax
	movq	listX+8(%rip), %rdx
	addq	%rbp, %rdx
	movq	%rax, 16(%rdx,%rcx,8)
	movq	listX(%rip), %rax
	movq	24(%rax,%rcx,8), %rax
	movq	listX+8(%rip), %rdx
	addq	%rbp, %rdx
	movq	%rax, 24(%rdx,%rcx,8)
	addq	$4, %rcx
	cmpq	%rcx, %r9
	jne	.LBB11_112
.LBB11_113:                             # %.preheader286
	movl	%r14d, %edx
	subl	%r15d, %edx
	jle	.LBB11_121
# BB#114:                               # %.lr.ph310.preheader
	movslq	%r14d, %rax
	leaq	-1(%rax), %r8
	subq	%rbx, %r8
	andq	$3, %rdx
	movq	%rbx, %rcx
	je	.LBB11_118
# BB#115:                               # %.lr.ph310.prol.preheader
	leaq	(,%rbx,8), %rdi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_116:                             # %.lr.ph310.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	listX(%rip), %rbp
	addq	%rdi, %rbp
	movq	(%rbp,%rcx,8), %rbp
	movq	listX+8(%rip), %rsi
	movq	%rbp, (%rsi,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rdx
	jne	.LBB11_116
# BB#117:                               # %.lr.ph310.prol.loopexit.unr-lcssa
	addq	%rbx, %rcx
.LBB11_118:                             # %.lr.ph310.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB11_121
# BB#119:                               # %.lr.ph310.preheader.new
	shlq	$3, %rbx
	negq	%rbx
	.p2align	4, 0x90
.LBB11_120:                             # %.lr.ph310
                                        # =>This Inner Loop Header: Depth=1
	movq	listX(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	listX+8(%rip), %rsi
	addq	%rbx, %rsi
	movq	%rdx, (%rsi,%rcx,8)
	movq	listX(%rip), %rdx
	movq	8(%rdx,%rcx,8), %rdx
	movq	listX+8(%rip), %rsi
	addq	%rbx, %rsi
	movq	%rdx, 8(%rsi,%rcx,8)
	movq	listX(%rip), %rdx
	movq	16(%rdx,%rcx,8), %rdx
	movq	listX+8(%rip), %rsi
	addq	%rbx, %rsi
	movq	%rdx, 16(%rsi,%rcx,8)
	movq	listX(%rip), %rdx
	movq	24(%rdx,%rcx,8), %rdx
	movq	listX+8(%rip), %rsi
	addq	%rbx, %rsi
	movq	%rdx, 24(%rsi,%rcx,8)
	addq	$4, %rcx
	cmpq	%rcx, %rax
	jne	.LBB11_120
.LBB11_121:                             # %._crit_edge311
	movl	%r14d, listXsize+4(%rip)
	movl	%r14d, listXsize(%rip)
	movl	dpb+36(%rip), %eax
	testq	%rax, %rax
	movl	%r14d, %r15d
	je	.LBB11_127
# BB#122:                               # %.lr.ph306
	xorl	%ecx, %ecx
	movl	%r14d, %r15d
	.p2align	4, 0x90
.LBB11_123:                             # =>This Inner Loop Header: Depth=1
	movq	dpb+16(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	cmpl	$3, (%rdx)
	jne	.LBB11_126
# BB#124:                               #   in Loop: Header=BB11_123 Depth=1
	movq	40(%rdx), %rdx
	cmpl	$0, 6376(%rdx)
	je	.LBB11_126
# BB#125:                               #   in Loop: Header=BB11_123 Depth=1
	movq	listX(%rip), %rsi
	movslq	%r15d, %rdi
	movq	%rdx, (%rsi,%rdi,8)
	movq	dpb+16(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	40(%rdx), %rdx
	movq	listX+8(%rip), %rsi
	leal	1(%rdi), %r15d
	movq	%rdx, (%rsi,%rdi,8)
.LBB11_126:                             #   in Loop: Header=BB11_123 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB11_123
.LBB11_127:                             # %._crit_edge307
	movq	listX(%rip), %rax
	movslq	%r14d, %rcx
	leaq	(%rax,%rcx,8), %rdi
	movl	%r15d, %eax
	subl	%r14d, %eax
	movslq	%eax, %rsi
	movl	$8, %edx
	movl	$compare_pic_by_lt_pic_num_asc, %ecx
	callq	qsort
	movslq	listXsize(%rip), %rax
	leaq	(,%rax,8), %rdi
	addq	listX+8(%rip), %rdi
	movslq	%r15d, %rbx
	movq	%rbx, %rsi
	subq	%rax, %rsi
	movl	$8, %edx
	movl	$compare_pic_by_lt_pic_num_asc, %ecx
	callq	qsort
	movl	%ebx, listXsize+4(%rip)
	movl	%ebx, listXsize(%rip)
	jmp	.LBB11_163
.LBB11_157:                             # %vector.body455.preheader
	leaq	16(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rbp
	movabsq	$17179869184, %r9       # imm = 0x400000000
	.p2align	4, 0x90
.LBB11_158:                             # %vector.body455
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movq	%rdx, %rdi
	sarq	$29, %rdi
	movups	%xmm0, (%r12,%rdi)
	movups	%xmm1, 16(%r12,%rdi)
	addq	%r9, %rdx
	addq	$32, %rsi
	addq	$-4, %rbp
	jne	.LBB11_158
# BB#159:                               # %middle.block456
	testl	%r8d, %r8d
	movl	%ebx, %edi
	jne	.LBB11_160
	jmp	.LBB11_162
.Lfunc_end11:
	.size	init_lists, .Lfunc_end11-init_lists
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI11_0:
	.quad	.LBB11_41
	.quad	.LBB11_87
	.quad	.LBB11_40
	.quad	.LBB11_41
	.quad	.LBB11_40

	.text
	.p2align	4, 0x90
	.type	compare_pic_by_pic_num_desc,@function
compare_pic_by_pic_num_desc:            # @compare_pic_by_pic_num_desc
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	6364(%rax), %eax
	movq	(%rsi), %rcx
	xorl	%edx, %edx
	cmpl	6364(%rcx), %eax
	movl	$-1, %ecx
	cmovlel	%edx, %ecx
	movl	$1, %eax
	cmovgel	%ecx, %eax
	retq
.Lfunc_end12:
	.size	compare_pic_by_pic_num_desc, .Lfunc_end12-compare_pic_by_pic_num_desc
	.cfi_endproc

	.p2align	4, 0x90
	.type	compare_pic_by_lt_pic_num_asc,@function
compare_pic_by_lt_pic_num_asc:          # @compare_pic_by_lt_pic_num_asc
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	6368(%rax), %eax
	movq	(%rsi), %rcx
	xorl	%edx, %edx
	cmpl	6368(%rcx), %eax
	setg	%dl
	movl	$-1, %eax
	cmovgel	%edx, %eax
	retq
.Lfunc_end13:
	.size	compare_pic_by_lt_pic_num_asc, .Lfunc_end13-compare_pic_by_lt_pic_num_asc
	.cfi_endproc

	.p2align	4, 0x90
	.type	compare_fs_by_frame_num_desc,@function
compare_fs_by_frame_num_desc:           # @compare_fs_by_frame_num_desc
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	24(%rax), %eax
	movq	(%rsi), %rcx
	xorl	%edx, %edx
	cmpl	24(%rcx), %eax
	movl	$-1, %ecx
	cmovlel	%edx, %ecx
	movl	$1, %eax
	cmovgel	%ecx, %eax
	retq
.Lfunc_end14:
	.size	compare_fs_by_frame_num_desc, .Lfunc_end14-compare_fs_by_frame_num_desc
	.cfi_endproc

	.p2align	4, 0x90
	.type	gen_pic_list_from_frame_list,@function
gen_pic_list_from_frame_list:           # @gen_pic_list_from_frame_list
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi51:
	.cfi_def_cfa_offset 96
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %rbx
	testl	%r9d, %r9d
	movl	$is_short_ref, %eax
	movl	$is_long_ref, %r13d
	cmoveq	%rax, %r13
	movl	%edx, %eax
	xorl	%r12d, %r12d
	movl	%edi, 36(%rsp)          # 4-byte Spill
	cmpl	$1, %edi
	movl	$0, %r14d
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jne	.LBB15_17
# BB#1:                                 # %.preheader84
	testl	%eax, %eax
	jle	.LBB15_35
# BB#2:                                 # %.critedge.preheader.preheader
	movslq	%eax, %r15
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	movq	24(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB15_3:                               # %.critedge.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_5 Depth 2
                                        #     Child Loop BB15_11 Depth 2
	cmpl	%eax, %r12d
	jge	.LBB15_9
# BB#4:                                 # %.lr.ph102.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	movslq	%r12d, %r12
	.p2align	4, 0x90
.LBB15_5:                               # %.lr.ph102
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%r12,8), %rax
	testb	$1, (%rax)
	je	.LBB15_36
# BB#6:                                 #   in Loop: Header=BB15_5 Depth=2
	movq	48(%rax), %rdi
	callq	*%r13
	testl	%eax, %eax
	jne	.LBB15_7
.LBB15_36:                              # %.critedge
                                        #   in Loop: Header=BB15_5 Depth=2
	incq	%r12
	cmpq	%r15, %r12
	jl	.LBB15_5
	jmp	.LBB15_8
.LBB15_7:                               #   in Loop: Header=BB15_3 Depth=1
	movq	(%rbx,%r12,8), %rax
	movq	48(%rax), %rax
	movslq	(%rbp), %rcx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%rax, (%rdx,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, (%rbp)
	leal	1(%r12), %r12d
.LBB15_8:                               # %.preheader82
                                        #   in Loop: Header=BB15_3 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
.LBB15_9:                               # %.preheader82
                                        #   in Loop: Header=BB15_3 Depth=1
	cmpl	%eax, %r14d
	jge	.LBB15_15
# BB#10:                                # %.lr.ph105.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	movslq	%r14d, %r14
	.p2align	4, 0x90
.LBB15_11:                              # %.lr.ph105
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%r14,8), %rax
	testb	$2, (%rax)
	je	.LBB15_37
# BB#12:                                #   in Loop: Header=BB15_11 Depth=2
	movq	56(%rax), %rdi
	callq	*%r13
	testl	%eax, %eax
	jne	.LBB15_13
.LBB15_37:                              #   in Loop: Header=BB15_11 Depth=2
	incq	%r14
	cmpq	%r15, %r14
	jl	.LBB15_11
	jmp	.LBB15_14
.LBB15_13:                              #   in Loop: Header=BB15_3 Depth=1
	movq	(%rbx,%r14,8), %rax
	movq	56(%rax), %rax
	movslq	(%rbp), %rcx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%rax, (%rdx,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, (%rbp)
	leal	1(%r14), %r14d
.LBB15_14:                              # %.backedge86
                                        #   in Loop: Header=BB15_3 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
.LBB15_15:                              # %.backedge86
                                        #   in Loop: Header=BB15_3 Depth=1
	cmpl	%eax, %r12d
	jl	.LBB15_3
# BB#16:                                # %.backedge86
                                        #   in Loop: Header=BB15_3 Depth=1
	cmpl	%eax, %r14d
	jl	.LBB15_3
.LBB15_17:                              # %.loopexit85
	cmpl	$2, 36(%rsp)            # 4-byte Folded Reload
	jne	.LBB15_35
# BB#18:                                # %.preheader80
	cmpl	%eax, %r12d
	jl	.LBB15_20
# BB#19:                                # %.preheader80
	cmpl	%eax, %r14d
	jge	.LBB15_35
.LBB15_20:                              # %.critedge1.preheader.preheader
	movslq	%eax, %rbp
	.p2align	4, 0x90
.LBB15_21:                              # %.critedge1.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_23 Depth 2
                                        #     Child Loop BB15_29 Depth 2
	cmpl	%eax, %r14d
	jge	.LBB15_27
# BB#22:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB15_21 Depth=1
	movslq	%r14d, %r14
	.p2align	4, 0x90
.LBB15_23:                              # %.lr.ph
                                        #   Parent Loop BB15_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%r14,8), %rax
	testb	$2, (%rax)
	je	.LBB15_38
# BB#24:                                #   in Loop: Header=BB15_23 Depth=2
	movq	56(%rax), %rdi
	callq	*%r13
	testl	%eax, %eax
	jne	.LBB15_25
.LBB15_38:                              # %.critedge1
                                        #   in Loop: Header=BB15_23 Depth=2
	incq	%r14
	cmpq	%rbp, %r14
	jl	.LBB15_23
	jmp	.LBB15_26
.LBB15_25:                              #   in Loop: Header=BB15_21 Depth=1
	movq	(%rbx,%r14,8), %rax
	movq	56(%rax), %rax
	movq	24(%rsp), %rdx          # 8-byte Reload
	movslq	(%rdx), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%rax, (%rsi,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, (%rdx)
	leal	1(%r14), %r14d
.LBB15_26:                              # %.preheader
                                        #   in Loop: Header=BB15_21 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
.LBB15_27:                              # %.preheader
                                        #   in Loop: Header=BB15_21 Depth=1
	cmpl	%eax, %r12d
	jge	.LBB15_33
# BB#28:                                # %.lr.ph96.preheader
                                        #   in Loop: Header=BB15_21 Depth=1
	movslq	%r12d, %r12
	.p2align	4, 0x90
.LBB15_29:                              # %.lr.ph96
                                        #   Parent Loop BB15_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%r12,8), %rax
	testb	$1, (%rax)
	je	.LBB15_39
# BB#30:                                #   in Loop: Header=BB15_29 Depth=2
	movq	48(%rax), %rdi
	callq	*%r13
	testl	%eax, %eax
	jne	.LBB15_31
.LBB15_39:                              #   in Loop: Header=BB15_29 Depth=2
	incq	%r12
	cmpq	%rbp, %r12
	jl	.LBB15_29
	jmp	.LBB15_32
.LBB15_31:                              #   in Loop: Header=BB15_21 Depth=1
	movq	(%rbx,%r12,8), %rax
	movq	48(%rax), %rax
	movq	24(%rsp), %rdx          # 8-byte Reload
	movslq	(%rdx), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%rax, (%rsi,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, (%rdx)
	leal	1(%r12), %r12d
.LBB15_32:                              # %.backedge
                                        #   in Loop: Header=BB15_21 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
.LBB15_33:                              # %.backedge
                                        #   in Loop: Header=BB15_21 Depth=1
	cmpl	%eax, %r12d
	jl	.LBB15_21
# BB#34:                                # %.backedge
                                        #   in Loop: Header=BB15_21 Depth=1
	cmpl	%eax, %r14d
	jl	.LBB15_21
.LBB15_35:                              # %.loopexit81
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	gen_pic_list_from_frame_list, .Lfunc_end15-gen_pic_list_from_frame_list
	.cfi_endproc

	.p2align	4, 0x90
	.type	compare_fs_by_lt_pic_idx_asc,@function
compare_fs_by_lt_pic_idx_asc:           # @compare_fs_by_lt_pic_idx_asc
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	28(%rax), %eax
	movq	(%rsi), %rcx
	xorl	%edx, %edx
	cmpl	28(%rcx), %eax
	setg	%dl
	movl	$-1, %eax
	cmovgel	%edx, %eax
	retq
.Lfunc_end16:
	.size	compare_fs_by_lt_pic_idx_asc, .Lfunc_end16-compare_fs_by_lt_pic_idx_asc
	.cfi_endproc

	.p2align	4, 0x90
	.type	compare_pic_by_poc_desc,@function
compare_pic_by_poc_desc:                # @compare_pic_by_poc_desc
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	4(%rax), %eax
	movq	(%rsi), %rcx
	xorl	%edx, %edx
	cmpl	4(%rcx), %eax
	movl	$-1, %ecx
	cmovlel	%edx, %ecx
	movl	$1, %eax
	cmovgel	%ecx, %eax
	retq
.Lfunc_end17:
	.size	compare_pic_by_poc_desc, .Lfunc_end17-compare_pic_by_poc_desc
	.cfi_endproc

	.p2align	4, 0x90
	.type	compare_pic_by_poc_asc,@function
compare_pic_by_poc_asc:                 # @compare_pic_by_poc_asc
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	4(%rax), %eax
	movq	(%rsi), %rcx
	xorl	%edx, %edx
	cmpl	4(%rcx), %eax
	setg	%dl
	movl	$-1, %eax
	cmovgel	%edx, %eax
	retq
.Lfunc_end18:
	.size	compare_pic_by_poc_asc, .Lfunc_end18-compare_pic_by_poc_asc
	.cfi_endproc

	.p2align	4, 0x90
	.type	compare_fs_by_poc_desc,@function
compare_fs_by_poc_desc:                 # @compare_fs_by_poc_desc
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	36(%rax), %eax
	movq	(%rsi), %rcx
	xorl	%edx, %edx
	cmpl	36(%rcx), %eax
	movl	$-1, %ecx
	cmovlel	%edx, %ecx
	movl	$1, %eax
	cmovgel	%ecx, %eax
	retq
.Lfunc_end19:
	.size	compare_fs_by_poc_desc, .Lfunc_end19-compare_fs_by_poc_desc
	.cfi_endproc

	.p2align	4, 0x90
	.type	compare_fs_by_poc_asc,@function
compare_fs_by_poc_asc:                  # @compare_fs_by_poc_asc
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	36(%rax), %eax
	movq	(%rsi), %rcx
	xorl	%edx, %edx
	cmpl	36(%rcx), %eax
	setg	%dl
	movl	$-1, %eax
	cmovgel	%edx, %eax
	retq
.Lfunc_end20:
	.size	compare_fs_by_poc_asc, .Lfunc_end20-compare_fs_by_poc_asc
	.cfi_endproc

	.globl	init_mbaff_lists
	.p2align	4, 0x90
	.type	init_mbaff_lists,@function
init_mbaff_lists:                       # @init_mbaff_lists
	.cfi_startproc
# BB#0:
	movq	$-16, %rax
	.p2align	4, 0x90
.LBB21_1:                               # %.preheader30
                                        # =>This Inner Loop Header: Depth=1
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, (%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 8(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 16(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 24(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 32(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 40(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 48(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 56(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 64(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 72(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 80(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 88(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 96(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 104(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 112(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 120(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 128(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 136(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 144(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 152(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 160(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 168(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 176(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 184(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 192(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 200(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 208(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 216(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 224(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 232(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 240(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 248(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 256(%rcx)
	movl	$0, listXsize+24(%rax)
	addq	$4, %rax
	jne	.LBB21_1
# BB#2:                                 # %.preheader
	movslq	listXsize(%rip), %rax
	testq	%rax, %rax
	jle	.LBB21_5
# BB#3:                                 # %.lr.ph35
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB21_4:                               # =>This Inner Loop Header: Depth=1
	movq	listX(%rip), %rsi
	movq	(%rsi,%rcx), %rsi
	movq	6536(%rsi), %rsi
	movq	listX+16(%rip), %rdi
	movq	%rsi, (%rdi,%rcx,2)
	movq	listX(%rip), %rsi
	movq	(%rsi,%rcx), %rsi
	movq	6544(%rsi), %rsi
	movq	listX+16(%rip), %rdi
	movq	%rsi, 8(%rdi,%rcx,2)
	movq	listX(%rip), %rsi
	movq	(%rsi,%rcx), %rsi
	movq	6544(%rsi), %rsi
	movq	listX+32(%rip), %rdi
	movq	%rsi, (%rdi,%rcx,2)
	movq	listX(%rip), %rsi
	movq	(%rsi,%rcx), %rsi
	movq	6536(%rsi), %rsi
	movq	listX+32(%rip), %rdi
	movq	%rsi, 8(%rdi,%rcx,2)
	incq	%rdx
	addq	$8, %rcx
	cmpq	%rax, %rdx
	jl	.LBB21_4
.LBB21_5:                               # %._crit_edge36
	addl	%eax, %eax
	movl	%eax, listXsize+16(%rip)
	movl	%eax, listXsize+8(%rip)
	movslq	listXsize+4(%rip), %rax
	testq	%rax, %rax
	jle	.LBB21_8
# BB#6:                                 # %.lr.ph
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB21_7:                               # =>This Inner Loop Header: Depth=1
	movq	listX+8(%rip), %rsi
	movq	(%rsi,%rcx), %rsi
	movq	6536(%rsi), %rsi
	movq	listX+24(%rip), %rdi
	movq	%rsi, (%rdi,%rcx,2)
	movq	listX+8(%rip), %rsi
	movq	(%rsi,%rcx), %rsi
	movq	6544(%rsi), %rsi
	movq	listX+24(%rip), %rdi
	movq	%rsi, 8(%rdi,%rcx,2)
	movq	listX+8(%rip), %rsi
	movq	(%rsi,%rcx), %rsi
	movq	6544(%rsi), %rsi
	movq	listX+40(%rip), %rdi
	movq	%rsi, (%rdi,%rcx,2)
	movq	listX+8(%rip), %rsi
	movq	(%rsi,%rcx), %rsi
	movq	6536(%rsi), %rsi
	movq	listX+40(%rip), %rdi
	movq	%rsi, 8(%rdi,%rcx,2)
	incq	%rdx
	addq	$8, %rcx
	cmpq	%rax, %rdx
	jl	.LBB21_7
.LBB21_8:                               # %._crit_edge
	addl	%eax, %eax
	movl	%eax, listXsize+20(%rip)
	movl	%eax, listXsize+12(%rip)
	retq
.Lfunc_end21:
	.size	init_mbaff_lists, .Lfunc_end21-init_mbaff_lists
	.cfi_endproc

	.globl	reorder_ref_pic_list
	.p2align	4, 0x90
	.type	reorder_ref_pic_list,@function
reorder_ref_pic_list:                   # @reorder_ref_pic_list
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi62:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi64:
	.cfi_def_cfa_offset 128
.Lcfi65:
	.cfi_offset %rbx, -56
.Lcfi66:
	.cfi_offset %r12, -48
.Lcfi67:
	.cfi_offset %r13, -40
.Lcfi68:
	.cfi_offset %r14, -32
.Lcfi69:
	.cfi_offset %r15, -24
.Lcfi70:
	.cfi_offset %rbp, -16
	movq	%r9, 56(%rsp)           # 8-byte Spill
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movq	%rcx, %r9
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rdi, %rbp
	movl	log2_max_frame_num_minus4(%rip), %ecx
	addl	$4, %ecx
	movl	$1, %r11d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r11d
	movq	img(%rip), %rax
	cmpl	$0, 24(%rax)
	movl	15332(%rax), %r15d
	je	.LBB22_2
# BB#1:
	addl	%r11d, %r11d
	leal	1(%r15,%r15), %r15d
.LBB22_2:
	movl	(%r9), %eax
	leal	1(%rdx), %ecx
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	cmpl	$3, %eax
	je	.LBB22_61
# BB#3:                                 # %.lr.ph
	movslq	20(%rsp), %r12          # 4-byte Folded Reload
	movslq	%edx, %r14
	xorl	%r13d, %r13d
	movq	%r9, %rbx
	movl	%r15d, %ecx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movq	%r12, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB22_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_38 Depth 2
                                        #     Child Loop BB22_46 Depth 2
                                        #     Child Loop BB22_53 Depth 2
                                        #     Child Loop BB22_55 Depth 2
                                        #     Child Loop BB22_13 Depth 2
                                        #     Child Loop BB22_21 Depth 2
                                        #     Child Loop BB22_28 Depth 2
                                        #     Child Loop BB22_30 Depth 2
	cmpl	$4, %eax
	jl	.LBB22_6
# BB#5:                                 #   in Loop: Header=BB22_4 Depth=1
	movl	$.L.str.13, %edi
	movl	$500, %esi              # imm = 0x1F4
	movq	%r13, 64(%rsp)          # 8-byte Spill
	movq	%r9, %r13
	movq	%rdx, %r12
	movl	%r11d, %r15d
	callq	error
	movl	%r15d, %r11d
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	%r12, %rdx
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%r13, %r9
	movq	64(%rsp), %r13          # 8-byte Reload
	movl	(%rbx), %eax
.LBB22_6:                               #   in Loop: Header=BB22_4 Depth=1
	cmpl	$1, %eax
	jg	.LBB22_35
# BB#7:                                 #   in Loop: Header=BB22_4 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%r13,4), %ecx
	incl	%ecx
	testl	%eax, %eax
	je	.LBB22_8
# BB#9:                                 #   in Loop: Header=BB22_4 Depth=1
	addl	16(%rsp), %ecx          # 4-byte Folded Reload
	cmpl	%r11d, %ecx
	movl	%r11d, %eax
	movl	$0, %esi
	cmovll	%esi, %eax
	subl	%eax, %ecx
	movl	%ecx, %esi
	jmp	.LBB22_10
	.p2align	4, 0x90
.LBB22_35:                              #   in Loop: Header=BB22_4 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r13,4), %r8d
	movl	dpb+36(%rip), %ecx
	testq	%rcx, %rcx
	je	.LBB22_50
# BB#36:                                # %.lr.ph.i.i55
                                        #   in Loop: Header=BB22_4 Depth=1
	movq	img(%rip), %rax
	cmpl	$0, 24(%rax)
	movq	dpb+16(%rip), %r10
	je	.LBB22_45
# BB#37:                                # %.lr.ph.split.i.i60.preheader
                                        #   in Loop: Header=BB22_4 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB22_38:                              # %.lr.ph.split.i.i60
                                        #   Parent Loop BB22_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r10,%rdi,8), %rbx
	movl	4(%rbx), %eax
	testb	$1, %al
	je	.LBB22_41
# BB#39:                                #   in Loop: Header=BB22_38 Depth=2
	movq	48(%rbx), %rsi
	cmpl	$0, 6376(%rsi)
	je	.LBB22_41
# BB#40:                                #   in Loop: Header=BB22_38 Depth=2
	cmpl	%r8d, 6368(%rsi)
	je	.LBB22_51
.LBB22_41:                              #   in Loop: Header=BB22_38 Depth=2
	testb	$2, %al
	je	.LBB22_44
# BB#42:                                #   in Loop: Header=BB22_38 Depth=2
	movq	56(%rbx), %rsi
	cmpl	$0, 6376(%rsi)
	je	.LBB22_44
# BB#43:                                #   in Loop: Header=BB22_38 Depth=2
	cmpl	%r8d, 6368(%rsi)
	je	.LBB22_51
.LBB22_44:                              #   in Loop: Header=BB22_38 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jb	.LBB22_38
	jmp	.LBB22_50
.LBB22_8:                               #   in Loop: Header=BB22_4 Depth=1
	movl	16(%rsp), %esi          # 4-byte Reload
	subl	%ecx, %esi
	movl	%esi, %eax
	sarl	$31, %eax
	andl	%r11d, %eax
	addl	%eax, %esi
.LBB22_10:                              #   in Loop: Header=BB22_4 Depth=1
	cmpl	%r15d, %esi
	movl	$0, %eax
	cmovgl	%r11d, %eax
	movl	%esi, %r8d
	subl	%eax, %r8d
	movl	dpb+32(%rip), %ecx
	testq	%rcx, %rcx
	movl	%esi, 16(%rsp)          # 4-byte Spill
	je	.LBB22_25
# BB#11:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB22_4 Depth=1
	movq	img(%rip), %rax
	cmpl	$0, 24(%rax)
	movq	dpb+8(%rip), %r10
	je	.LBB22_20
# BB#12:                                # %.lr.ph.split.i.i.preheader
                                        #   in Loop: Header=BB22_4 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB22_13:                              # %.lr.ph.split.i.i
                                        #   Parent Loop BB22_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r10,%rdi,8), %rbx
	movl	4(%rbx), %eax
	testb	$1, %al
	je	.LBB22_16
# BB#14:                                #   in Loop: Header=BB22_13 Depth=2
	movq	48(%rbx), %rsi
	cmpl	$0, 6376(%rsi)
	jne	.LBB22_16
# BB#15:                                #   in Loop: Header=BB22_13 Depth=2
	cmpl	%r8d, 6364(%rsi)
	je	.LBB22_26
	.p2align	4, 0x90
.LBB22_16:                              #   in Loop: Header=BB22_13 Depth=2
	testb	$2, %al
	je	.LBB22_19
# BB#17:                                #   in Loop: Header=BB22_13 Depth=2
	movq	56(%rbx), %rsi
	cmpl	$0, 6376(%rsi)
	jne	.LBB22_19
# BB#18:                                #   in Loop: Header=BB22_13 Depth=2
	cmpl	%r8d, 6364(%rsi)
	je	.LBB22_26
	.p2align	4, 0x90
.LBB22_19:                              #   in Loop: Header=BB22_13 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jb	.LBB22_13
	jmp	.LBB22_25
.LBB22_45:                              # %.lr.ph.split.us.i.i57.preheader
                                        #   in Loop: Header=BB22_4 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB22_46:                              # %.lr.ph.split.us.i.i57
                                        #   Parent Loop BB22_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r10,%rdi,8), %rax
	cmpl	$3, 4(%rax)
	jne	.LBB22_49
# BB#47:                                #   in Loop: Header=BB22_46 Depth=2
	movq	40(%rax), %rsi
	cmpl	$0, 6376(%rsi)
	je	.LBB22_49
# BB#48:                                #   in Loop: Header=BB22_46 Depth=2
	cmpl	%r8d, 6368(%rsi)
	je	.LBB22_51
.LBB22_49:                              #   in Loop: Header=BB22_46 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jb	.LBB22_46
.LBB22_50:                              #   in Loop: Header=BB22_4 Depth=1
	xorl	%esi, %esi
.LBB22_51:                              # %get_long_term_pic.exit.i
                                        #   in Loop: Header=BB22_4 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
	cmpl	%edx, %eax
	movslq	%eax, %rcx
	movq	%r12, %rax
	jle	.LBB22_53
# BB#52:                                # %._crit_edge38.i.thread
                                        #   in Loop: Header=BB22_4 Depth=1
	leal	1(%rcx), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	%rsi, (%rbp,%rcx,8)
	jmp	.LBB22_60
	.p2align	4, 0x90
.LBB22_53:                              # %.lr.ph37.i
                                        #   Parent Loop BB22_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rbp,%rax,8), %rdi
	movq	%rdi, (%rbp,%rax,8)
	decq	%rax
	cmpq	%rcx, %rax
	jg	.LBB22_53
# BB#54:                                # %.lr.ph.preheader.i65
                                        #   in Loop: Header=BB22_4 Depth=1
	leal	1(%rcx), %ebx
	movq	%rsi, (%rbp,%rcx,8)
	movl	%ebx, %edi
	.p2align	4, 0x90
.LBB22_55:                              # %.lr.ph.i67
                                        #   Parent Loop BB22_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp,%rcx,8), %rax
	cmpl	$0, 6376(%rax)
	je	.LBB22_57
# BB#56:                                #   in Loop: Header=BB22_55 Depth=2
	cmpl	%r8d, 6368(%rax)
	je	.LBB22_58
.LBB22_57:                              #   in Loop: Header=BB22_55 Depth=2
	movslq	%edi, %rsi
	incl	%edi
	movq	%rax, (%rbp,%rsi,8)
.LBB22_58:                              #   in Loop: Header=BB22_55 Depth=2
	incq	%rcx
	cmpq	%r14, %rcx
	jle	.LBB22_55
	jmp	.LBB22_59
.LBB22_20:                              # %.lr.ph.split.us.i.i.preheader
                                        #   in Loop: Header=BB22_4 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB22_21:                              # %.lr.ph.split.us.i.i
                                        #   Parent Loop BB22_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r10,%rdi,8), %rax
	cmpl	$3, 4(%rax)
	jne	.LBB22_24
# BB#22:                                #   in Loop: Header=BB22_21 Depth=2
	movq	40(%rax), %rsi
	cmpl	$0, 6376(%rsi)
	jne	.LBB22_24
# BB#23:                                #   in Loop: Header=BB22_21 Depth=2
	cmpl	%r8d, 6364(%rsi)
	je	.LBB22_26
	.p2align	4, 0x90
.LBB22_24:                              #   in Loop: Header=BB22_21 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jb	.LBB22_21
.LBB22_25:                              #   in Loop: Header=BB22_4 Depth=1
	xorl	%esi, %esi
.LBB22_26:                              # %get_short_term_pic.exit.i
                                        #   in Loop: Header=BB22_4 Depth=1
	movl	12(%rsp), %ebx          # 4-byte Reload
	cmpl	%edx, %ebx
	movslq	%ebx, %rcx
	movq	%r12, %rax
	jle	.LBB22_28
# BB#27:                                # %._crit_edge40.i.thread
                                        #   in Loop: Header=BB22_4 Depth=1
	incl	%ebx
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	movq	%rsi, (%rbp,%rcx,8)
	jmp	.LBB22_60
	.p2align	4, 0x90
.LBB22_28:                              # %.lr.ph39.i
                                        #   Parent Loop BB22_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rbp,%rax,8), %rdi
	movq	%rdi, (%rbp,%rax,8)
	decq	%rax
	cmpq	%rcx, %rax
	jg	.LBB22_28
# BB#29:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB22_4 Depth=1
	incl	%ebx
	movq	%rsi, (%rbp,%rcx,8)
	movl	%ebx, %edi
	.p2align	4, 0x90
.LBB22_30:                              # %.lr.ph.i
                                        #   Parent Loop BB22_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp,%rcx,8), %rax
	testq	%rax, %rax
	je	.LBB22_34
# BB#31:                                #   in Loop: Header=BB22_30 Depth=2
	cmpl	$0, 6376(%rax)
	jne	.LBB22_33
# BB#32:                                #   in Loop: Header=BB22_30 Depth=2
	cmpl	%r8d, 6364(%rax)
	je	.LBB22_34
.LBB22_33:                              #   in Loop: Header=BB22_30 Depth=2
	movslq	%edi, %rsi
	incl	%edi
	movq	%rax, (%rbp,%rsi,8)
.LBB22_34:                              #   in Loop: Header=BB22_30 Depth=2
	incq	%rcx
	cmpq	%r14, %rcx
	jle	.LBB22_30
.LBB22_59:                              #   in Loop: Header=BB22_4 Depth=1
	movl	%ebx, 12(%rsp)          # 4-byte Spill
.LBB22_60:                              # %reorder_short_term.exit
                                        #   in Loop: Header=BB22_4 Depth=1
	leaq	4(%r9,%r13,4), %rbx
	movl	4(%r9,%r13,4), %eax
	incq	%r13
	cmpl	$3, %eax
	jne	.LBB22_4
.LBB22_61:                              # %._crit_edge
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	20(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, (%rax)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	reorder_ref_pic_list, .Lfunc_end22-reorder_ref_pic_list
	.cfi_endproc

	.globl	update_ref_list
	.p2align	4, 0x90
	.type	update_ref_list,@function
update_ref_list:                        # @update_ref_list
	.cfi_startproc
# BB#0:
	movl	dpb+28(%rip), %ecx
	testq	%rcx, %rcx
	je	.LBB23_1
# BB#2:                                 # %.lr.ph17
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB23_3:                               # =>This Inner Loop Header: Depth=1
	movq	dpb(%rip), %rax
	movq	(%rax,%rdx,8), %rsi
	movl	(%rsi), %edi
	cmpl	$3, %edi
	jne	.LBB23_6
# BB#4:                                 #   in Loop: Header=BB23_3 Depth=1
	movq	40(%rsi), %rax
	cmpl	$0, 6380(%rax)
	je	.LBB23_7
# BB#5:                                 #   in Loop: Header=BB23_3 Depth=1
	cmpl	$0, 6376(%rax)
	jne	.LBB23_7
	jmp	.LBB23_14
	.p2align	4, 0x90
.LBB23_6:                               #   in Loop: Header=BB23_3 Depth=1
	testb	$1, %dil
	je	.LBB23_10
.LBB23_7:                               # %.thread.i
                                        #   in Loop: Header=BB23_3 Depth=1
	movq	48(%rsi), %rax
	testq	%rax, %rax
	je	.LBB23_10
# BB#8:                                 #   in Loop: Header=BB23_3 Depth=1
	cmpl	$0, 6380(%rax)
	je	.LBB23_10
# BB#9:                                 #   in Loop: Header=BB23_3 Depth=1
	cmpl	$0, 6376(%rax)
	je	.LBB23_14
	.p2align	4, 0x90
.LBB23_10:                              #   in Loop: Header=BB23_3 Depth=1
	testb	$2, %dil
	je	.LBB23_15
# BB#11:                                #   in Loop: Header=BB23_3 Depth=1
	movq	56(%rsi), %rax
	testq	%rax, %rax
	je	.LBB23_15
# BB#12:                                #   in Loop: Header=BB23_3 Depth=1
	cmpl	$0, 6380(%rax)
	je	.LBB23_15
# BB#13:                                #   in Loop: Header=BB23_3 Depth=1
	cmpl	$0, 6376(%rax)
	jne	.LBB23_15
.LBB23_14:                              #   in Loop: Header=BB23_3 Depth=1
	movq	dpb+8(%rip), %rax
	movl	%r8d, %edi
	incl	%r8d
	movq	%rsi, (%rax,%rdi,8)
.LBB23_15:                              # %is_short_term_reference.exit
                                        #   in Loop: Header=BB23_3 Depth=1
	incq	%rdx
	cmpq	%rcx, %rdx
	jb	.LBB23_3
	jmp	.LBB23_16
.LBB23_1:
	xorl	%r8d, %r8d
.LBB23_16:                              # %._crit_edge18
	movl	%r8d, dpb+32(%rip)
	movl	dpb+24(%rip), %ecx
	cmpl	%ecx, %r8d
	jae	.LBB23_22
# BB#17:                                # %.lr.ph
	movl	%r8d, %edx
	movl	%ecx, %esi
	subl	%r8d, %esi
	leaq	-1(%rcx), %rax
	subq	%rdx, %rax
	andq	$7, %rsi
	je	.LBB23_20
# BB#18:                                # %.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB23_19:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+8(%rip), %rdi
	movq	$0, (%rdi,%rdx,8)
	incq	%rdx
	incq	%rsi
	jne	.LBB23_19
.LBB23_20:                              # %.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB23_22
	.p2align	4, 0x90
.LBB23_21:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+8(%rip), %rax
	movq	$0, (%rax,%rdx,8)
	movq	dpb+8(%rip), %rax
	movq	$0, 8(%rax,%rdx,8)
	movq	dpb+8(%rip), %rax
	movq	$0, 16(%rax,%rdx,8)
	movq	dpb+8(%rip), %rax
	movq	$0, 24(%rax,%rdx,8)
	movq	dpb+8(%rip), %rax
	movq	$0, 32(%rax,%rdx,8)
	movq	dpb+8(%rip), %rax
	movq	$0, 40(%rax,%rdx,8)
	movq	dpb+8(%rip), %rax
	movq	$0, 48(%rax,%rdx,8)
	movq	dpb+8(%rip), %rax
	movq	$0, 56(%rax,%rdx,8)
	addq	$8, %rdx
	cmpq	%rcx, %rdx
	jb	.LBB23_21
.LBB23_22:                              # %._crit_edge
	retq
.Lfunc_end23:
	.size	update_ref_list, .Lfunc_end23-update_ref_list
	.cfi_endproc

	.globl	update_ltref_list
	.p2align	4, 0x90
	.type	update_ltref_list,@function
update_ltref_list:                      # @update_ltref_list
	.cfi_startproc
# BB#0:
	movl	dpb+28(%rip), %r8d
	testq	%r8, %r8
	je	.LBB24_1
# BB#2:                                 # %.lr.ph17
	xorl	%edx, %edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB24_3:                               # =>This Inner Loop Header: Depth=1
	movq	dpb(%rip), %rcx
	movq	(%rcx,%rdx,8), %rsi
	movl	(%rsi), %edi
	cmpl	$3, %edi
	jne	.LBB24_6
# BB#4:                                 #   in Loop: Header=BB24_3 Depth=1
	movq	40(%rsi), %rcx
	cmpl	$0, 6380(%rcx)
	je	.LBB24_7
# BB#5:                                 #   in Loop: Header=BB24_3 Depth=1
	cmpl	$0, 6376(%rcx)
	jne	.LBB24_14
	jmp	.LBB24_7
	.p2align	4, 0x90
.LBB24_6:                               #   in Loop: Header=BB24_3 Depth=1
	testb	$1, %dil
	je	.LBB24_10
.LBB24_7:                               # %.thread.i
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	48(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB24_10
# BB#8:                                 #   in Loop: Header=BB24_3 Depth=1
	cmpl	$0, 6380(%rcx)
	je	.LBB24_10
# BB#9:                                 #   in Loop: Header=BB24_3 Depth=1
	cmpl	$0, 6376(%rcx)
	jne	.LBB24_14
	.p2align	4, 0x90
.LBB24_10:                              #   in Loop: Header=BB24_3 Depth=1
	testb	$2, %dil
	je	.LBB24_15
# BB#11:                                #   in Loop: Header=BB24_3 Depth=1
	movq	56(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB24_15
# BB#12:                                #   in Loop: Header=BB24_3 Depth=1
	cmpl	$0, 6380(%rcx)
	je	.LBB24_15
# BB#13:                                #   in Loop: Header=BB24_3 Depth=1
	cmpl	$0, 6376(%rcx)
	je	.LBB24_15
	.p2align	4, 0x90
.LBB24_14:                              #   in Loop: Header=BB24_3 Depth=1
	movq	dpb+16(%rip), %rcx
	movl	%eax, %edi
	incl	%eax
	movq	%rsi, (%rcx,%rdi,8)
.LBB24_15:                              # %is_long_term_reference.exit
                                        #   in Loop: Header=BB24_3 Depth=1
	incq	%rdx
	cmpq	%r8, %rdx
	jb	.LBB24_3
	jmp	.LBB24_16
.LBB24_1:
	xorl	%eax, %eax
.LBB24_16:                              # %._crit_edge18
	movl	%eax, dpb+36(%rip)
	movl	dpb+24(%rip), %ecx
	cmpl	%ecx, %eax
	jae	.LBB24_22
# BB#17:                                # %.lr.ph
	movl	%eax, %edx
	movl	%ecx, %esi
	subl	%eax, %esi
	leaq	-1(%rcx), %rax
	subq	%rdx, %rax
	andq	$7, %rsi
	je	.LBB24_20
# BB#18:                                # %.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB24_19:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+16(%rip), %rdi
	movq	$0, (%rdi,%rdx,8)
	incq	%rdx
	incq	%rsi
	jne	.LBB24_19
.LBB24_20:                              # %.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB24_22
	.p2align	4, 0x90
.LBB24_21:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+16(%rip), %rax
	movq	$0, (%rax,%rdx,8)
	movq	dpb+16(%rip), %rax
	movq	$0, 8(%rax,%rdx,8)
	movq	dpb+16(%rip), %rax
	movq	$0, 16(%rax,%rdx,8)
	movq	dpb+16(%rip), %rax
	movq	$0, 24(%rax,%rdx,8)
	movq	dpb+16(%rip), %rax
	movq	$0, 32(%rax,%rdx,8)
	movq	dpb+16(%rip), %rax
	movq	$0, 40(%rax,%rdx,8)
	movq	dpb+16(%rip), %rax
	movq	$0, 48(%rax,%rdx,8)
	movq	dpb+16(%rip), %rax
	movq	$0, 56(%rax,%rdx,8)
	addq	$8, %rdx
	cmpq	%rcx, %rdx
	jb	.LBB24_21
.LBB24_22:                              # %._crit_edge
	retq
.Lfunc_end24:
	.size	update_ltref_list, .Lfunc_end24-update_ltref_list
	.cfi_endproc

	.globl	mm_update_max_long_term_frame_idx
	.p2align	4, 0x90
	.type	mm_update_max_long_term_frame_idx,@function
mm_update_max_long_term_frame_idx:      # @mm_update_max_long_term_frame_idx
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	leal	-1(%rdi), %eax
	movl	%eax, dpb+44(%rip)
	movl	dpb+36(%rip), %r8d
	testq	%r8, %r8
	je	.LBB25_16
# BB#1:                                 # %.lr.ph
	movq	dpb+16(%rip), %r9
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB25_2:                               # =>This Inner Loop Header: Depth=1
	movq	(%r9,%rdx,8), %rsi
	cmpl	%edi, 28(%rsi)
	jl	.LBB25_15
# BB#3:                                 #   in Loop: Header=BB25_2 Depth=1
	movl	(%rsi), %eax
	testb	$1, %al
	je	.LBB25_6
# BB#4:                                 #   in Loop: Header=BB25_2 Depth=1
	movq	48(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB25_6
# BB#5:                                 #   in Loop: Header=BB25_2 Depth=1
	movq	$0, 6376(%rcx)
.LBB25_6:                               #   in Loop: Header=BB25_2 Depth=1
	testb	$2, %al
	je	.LBB25_9
# BB#7:                                 #   in Loop: Header=BB25_2 Depth=1
	movq	56(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB25_9
# BB#8:                                 #   in Loop: Header=BB25_2 Depth=1
	movq	$0, 6376(%rcx)
.LBB25_9:                               # %thread-pre-split.i
                                        #   in Loop: Header=BB25_2 Depth=1
	cmpl	$3, %eax
	jne	.LBB25_14
# BB#10:                                #   in Loop: Header=BB25_2 Depth=1
	movq	48(%rsi), %rax
	testq	%rax, %rax
	je	.LBB25_13
# BB#11:                                #   in Loop: Header=BB25_2 Depth=1
	movq	56(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB25_13
# BB#12:                                #   in Loop: Header=BB25_2 Depth=1
	movl	$0, 6380(%rax)
	movl	$0, 6376(%rax)
	movq	$0, 6376(%rcx)
.LBB25_13:                              #   in Loop: Header=BB25_2 Depth=1
	movq	40(%rsi), %rax
	movl	$0, 6380(%rax)
	movl	$0, 6376(%rax)
.LBB25_14:                              # %unmark_for_long_term_reference.exit
                                        #   in Loop: Header=BB25_2 Depth=1
	movq	$0, 4(%rsi)
.LBB25_15:                              #   in Loop: Header=BB25_2 Depth=1
	incq	%rdx
	cmpq	%r8, %rdx
	jb	.LBB25_2
.LBB25_16:                              # %._crit_edge
	retq
.Lfunc_end25:
	.size	mm_update_max_long_term_frame_idx, .Lfunc_end25-mm_update_max_long_term_frame_idx
	.cfi_endproc

	.globl	store_picture_in_dpb
	.p2align	4, 0x90
	.type	store_picture_in_dpb,@function
store_picture_in_dpb:                   # @store_picture_in_dpb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi74:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi75:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi77:
	.cfi_def_cfa_offset 64
.Lcfi78:
	.cfi_offset %rbx, -56
.Lcfi79:
	.cfi_offset %r12, -48
.Lcfi80:
	.cfi_offset %r13, -40
.Lcfi81:
	.cfi_offset %r14, -32
.Lcfi82:
	.cfi_offset %r15, -24
.Lcfi83:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	img(%rip), %rax
	movl	15360(%rax), %ecx
	xorl	%edx, %edx
	testl	%ecx, %ecx
	setne	%dl
	movl	%edx, 6380(%r14)
	movl	$0, 15428(%rax)
	xorl	%edx, %edx
	cmpl	$2, 24(%rax)
	sete	%dl
	movl	%edx, 15424(%rax)
	movq	14208(%rax), %rdx
	cmpl	$0, 4(%rdx)
	je	.LBB26_26
# BB#1:
	cmpl	$0, 15368(%rax)
	je	.LBB26_30
# BB#2:                                 # %.preheader14.i
	cmpl	$0, dpb+28(%rip)
	je	.LBB26_15
# BB#3:                                 # %.lr.ph19.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB26_4:                               # %.lr.ph19.i
                                        # =>This Inner Loop Header: Depth=1
	movq	dpb(%rip), %rcx
	movl	%eax, %ebp
	movq	(%rcx,%rbp,8), %rbx
	testq	%rbx, %rbx
	je	.LBB26_12
# BB#5:                                 #   in Loop: Header=BB26_4 Depth=1
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_7
# BB#6:                                 #   in Loop: Header=BB26_4 Depth=1
	callq	free_storable_picture
	movq	$0, 40(%rbx)
.LBB26_7:                               #   in Loop: Header=BB26_4 Depth=1
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_9
# BB#8:                                 #   in Loop: Header=BB26_4 Depth=1
	callq	free_storable_picture
	movq	$0, 48(%rbx)
.LBB26_9:                               #   in Loop: Header=BB26_4 Depth=1
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_11
# BB#10:                                #   in Loop: Header=BB26_4 Depth=1
	callq	free_storable_picture
.LBB26_11:                              #   in Loop: Header=BB26_4 Depth=1
	movq	%rbx, %rdi
	callq	free
.LBB26_12:                              # %free_frame_store.exit.i
                                        #   in Loop: Header=BB26_4 Depth=1
	movl	$1, %edi
	movl	$64, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB26_14
# BB#13:                                #   in Loop: Header=BB26_4 Depth=1
	movl	$.L.str.7, %edi
	callq	no_mem_exit
.LBB26_14:                              # %alloc_frame_store.exit.i
                                        #   in Loop: Header=BB26_4 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movups	%xmm0, 40(%rbx)
	movq	$0, 56(%rbx)
	movq	dpb(%rip), %rax
	movq	%rbx, (%rax,%rbp,8)
	leal	1(%rbp), %eax
	cmpl	dpb+28(%rip), %eax
	jb	.LBB26_4
.LBB26_15:                              # %.preheader13.i
	movl	dpb+32(%rip), %eax
	testq	%rax, %rax
	je	.LBB26_20
# BB#16:                                # %.lr.ph17.i
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$7, %rsi
	je	.LBB26_18
	.p2align	4, 0x90
.LBB26_17:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+8(%rip), %rdi
	movq	$0, (%rdi,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB26_17
.LBB26_18:                              # %.prol.loopexit321
	cmpq	$7, %rdx
	jb	.LBB26_20
	.p2align	4, 0x90
.LBB26_19:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+8(%rip), %rdx
	movq	$0, (%rdx,%rcx,8)
	movq	dpb+8(%rip), %rdx
	movq	$0, 8(%rdx,%rcx,8)
	movq	dpb+8(%rip), %rdx
	movq	$0, 16(%rdx,%rcx,8)
	movq	dpb+8(%rip), %rdx
	movq	$0, 24(%rdx,%rcx,8)
	movq	dpb+8(%rip), %rdx
	movq	$0, 32(%rdx,%rcx,8)
	movq	dpb+8(%rip), %rdx
	movq	$0, 40(%rdx,%rcx,8)
	movq	dpb+8(%rip), %rdx
	movq	$0, 48(%rdx,%rcx,8)
	movq	dpb+8(%rip), %rdx
	movq	$0, 56(%rdx,%rcx,8)
	addq	$8, %rcx
	cmpq	%rcx, %rax
	jne	.LBB26_19
.LBB26_20:                              # %.preheader.i
	movl	dpb+36(%rip), %eax
	testq	%rax, %rax
	je	.LBB26_25
# BB#21:                                # %.lr.ph.i
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$7, %rsi
	je	.LBB26_23
	.p2align	4, 0x90
.LBB26_22:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+16(%rip), %rdi
	movq	$0, (%rdi,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB26_22
.LBB26_23:                              # %.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB26_25
	.p2align	4, 0x90
.LBB26_24:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+16(%rip), %rdx
	movq	$0, (%rdx,%rcx,8)
	movq	dpb+16(%rip), %rdx
	movq	$0, 8(%rdx,%rcx,8)
	movq	dpb+16(%rip), %rdx
	movq	$0, 16(%rdx,%rcx,8)
	movq	dpb+16(%rip), %rdx
	movq	$0, 24(%rdx,%rcx,8)
	movq	dpb+16(%rip), %rdx
	movq	$0, 32(%rdx,%rcx,8)
	movq	dpb+16(%rip), %rdx
	movq	$0, 40(%rdx,%rcx,8)
	movq	dpb+16(%rip), %rdx
	movq	$0, 48(%rdx,%rcx,8)
	movq	dpb+16(%rip), %rdx
	movq	$0, 56(%rdx,%rcx,8)
	addq	$8, %rcx
	cmpq	%rcx, %rax
	jne	.LBB26_24
.LBB26_25:                              # %._crit_edge.i
	movl	$0, dpb+28(%rip)
	jmp	.LBB26_31
.LBB26_26:
	testl	%ecx, %ecx
	je	.LBB26_35
# BB#27:
	cmpl	$0, 15364(%rax)
	je	.LBB26_35
# BB#28:
	movl	$0, 15428(%rax)
	movq	15376(%rax), %r13
	testq	%r13, %r13
	je	.LBB26_35
# BB#29:                                # %.lr.ph.i24
	movl	$1, %r12d
	jmp	.LBB26_87
.LBB26_30:
	callq	flush_dpb
.LBB26_31:
	movq	$0, dpb+56(%rip)
	callq	update_ref_list
	callq	update_ltref_list
	movl	$-2147483648, dpb+40(%rip) # imm = 0x80000000
	movq	img(%rip), %rax
	cmpl	$0, 15372(%rax)
	je	.LBB26_33
# BB#32:
	movl	$0, dpb+44(%rip)
	movl	$0, 6372(%r14)
	movl	$1, %eax
	jmp	.LBB26_34
.LBB26_33:
	movl	$-1, dpb+44(%rip)
	xorl	%eax, %eax
.LBB26_34:                              # %adaptive_memory_management.exit
	movl	%eax, 6376(%r14)
.LBB26_35:                              # %adaptive_memory_management.exit
	movl	(%r14), %eax
	cmpl	$1, %eax
	je	.LBB26_37
# BB#36:
	cmpl	$2, %eax
	jne	.LBB26_45
.LBB26_37:
	movq	dpb+56(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB26_45
# BB#38:
	movl	20(%rdi), %ecx
	cmpl	6364(%r14), %ecx
	jne	.LBB26_45
# BB#39:
	movl	(%rdi), %ecx
	cmpl	$1, %eax
	jne	.LBB26_41
# BB#40:
	cmpl	$2, %ecx
	je	.LBB26_42
	jmp	.LBB26_45
.LBB26_41:
	cmpl	$1, %ecx
	jne	.LBB26_45
.LBB26_42:
	cmpl	$0, 6380(%r14)
	movl	12(%rdi), %eax
	je	.LBB26_44
# BB#43:
	testl	%eax, %eax
	jne	.LBB26_69
	jmp	.LBB26_45
.LBB26_44:                              # %.thread
	testl	%eax, %eax
	je	.LBB26_69
.LBB26_45:                              # %thread-pre-split
	movq	img(%rip), %rax
	movq	14208(%rax), %rcx
	cmpl	$0, 4(%rcx)
	je	.LBB26_60
.LBB26_46:
	movl	dpb+28(%rip), %eax
	movl	dpb+24(%rip), %r8d
	cmpl	%r8d, %eax
	jne	.LBB26_262
# BB#47:
	testl	%eax, %eax
	je	.LBB26_261
# BB#48:                                # %.lr.ph.i29
	movq	dpb(%rip), %rcx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB26_49:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdi,8), %rdx
	cmpl	$0, 32(%rdx)
	je	.LBB26_59
# BB#50:                                #   in Loop: Header=BB26_49 Depth=1
	cmpl	$0, 4(%rdx)
	jne	.LBB26_59
# BB#51:                                #   in Loop: Header=BB26_49 Depth=1
	movl	(%rdx), %esi
	cmpl	$3, %esi
	jne	.LBB26_53
# BB#52:                                #   in Loop: Header=BB26_49 Depth=1
	movq	40(%rdx), %rbp
	cmpl	$0, 6380(%rbp)
	jne	.LBB26_59
	jmp	.LBB26_54
.LBB26_53:                              #   in Loop: Header=BB26_49 Depth=1
	testb	$1, %sil
	je	.LBB26_56
.LBB26_54:                              # %.thread.i.i
                                        #   in Loop: Header=BB26_49 Depth=1
	movq	48(%rdx), %rbp
	testq	%rbp, %rbp
	je	.LBB26_56
# BB#55:                                #   in Loop: Header=BB26_49 Depth=1
	cmpl	$0, 6380(%rbp)
	jne	.LBB26_59
.LBB26_56:                              #   in Loop: Header=BB26_49 Depth=1
	testb	$2, %sil
	je	.LBB26_68
# BB#57:                                #   in Loop: Header=BB26_49 Depth=1
	movq	56(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB26_68
# BB#58:                                #   in Loop: Header=BB26_49 Depth=1
	cmpl	$0, 6380(%rdx)
	je	.LBB26_68
	.p2align	4, 0x90
.LBB26_59:                              # %is_used_for_reference.exit.thread.i
                                        #   in Loop: Header=BB26_49 Depth=1
	incq	%rdi
	cmpq	%rax, %rdi
	jb	.LBB26_49
	jmp	.LBB26_261
.LBB26_60:
	cmpl	$0, 6380(%r14)
	je	.LBB26_46
# BB#61:
	cmpl	$0, 15364(%rax)
	jne	.LBB26_46
# BB#62:
	movq	active_sps(%rip), %rax
	movl	1132(%rax), %eax
	subl	dpb+36(%rip), %eax
	cmpl	%eax, dpb+32(%rip)
	jne	.LBB26_256
# BB#63:
	movl	dpb+28(%rip), %eax
	testl	%eax, %eax
	je	.LBB26_256
# BB#64:                                # %.lr.ph.i26
	movq	dpb(%rip), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB26_65:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rdi
	cmpl	$0, 4(%rdi)
	je	.LBB26_67
# BB#66:                                #   in Loop: Header=BB26_65 Depth=1
	cmpl	$0, 8(%rdi)
	je	.LBB26_255
.LBB26_67:                              #   in Loop: Header=BB26_65 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jb	.LBB26_65
	jmp	.LBB26_256
.LBB26_68:                              # %is_used_for_reference.exit.i
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	remove_frame_from_dpb
	jmp	.LBB26_261
.LBB26_69:
	movq	%r14, %rsi
	callq	insert_picture_in_dpb
	callq	update_ref_list
	callq	update_ltref_list
	movq	$0, dpb+56(%rip)
	jmp	.LBB26_287
.LBB26_246:                             # %.us-lcssa33.i.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movl	$0, 6380(%rbx)
	movl	$0, 6376(%rbx)
	movl	%ecx, 4(%rdi)
	andl	$1, %ebp
	movl	%ebp, 8(%rdi)
	cmpl	$3, (%rdi)
	jne	.LBB26_248
.LBB26_247:                             #   in Loop: Header=BB26_87 Depth=1
	movq	40(%rdi), %rax
	movl	$0, 6380(%rax)
	movq	dpb+16(%rip), %rax
	movq	(%rax,%rsi,8), %rax
	movq	40(%rax), %rax
	movl	$0, 6376(%rax)
	jmp	.LBB26_248
.LBB26_70:                              #   in Loop: Header=BB26_87 Depth=1
	movl	%r15d, 6372(%rdi)
	movl	%r15d, 28(%rax)
	movl	%r15d, 6368(%rdi)
	movl	$1, 6376(%rdi)
	movq	48(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB26_73
# BB#71:                                #   in Loop: Header=BB26_87 Depth=1
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB26_73
# BB#72:                                #   in Loop: Header=BB26_87 Depth=1
	movl	%r15d, 6372(%rdx)
	movl	%r15d, 6372(%rcx)
	movl	%r15d, 6368(%rcx)
	movl	%r15d, 6368(%rdx)
	movl	$1, 6376(%rdx)
	movl	$1, 6376(%rcx)
.LBB26_73:                              #   in Loop: Header=BB26_87 Depth=1
	movl	$3, 8(%rax)
	jmp	.LBB26_234
.LBB26_74:                              # %.us-lcssa10.us.i.i
                                        #   in Loop: Header=BB26_87 Depth=1
	callq	unmark_for_reference
	callq	update_ref_list
	jmp	.LBB26_249
.LBB26_75:                              #   in Loop: Header=BB26_87 Depth=1
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB26_81
# BB#76:                                #   in Loop: Header=BB26_87 Depth=1
	cmpl	%r15d, 28(%rax)
	je	.LBB26_82
# BB#77:                                #   in Loop: Header=BB26_87 Depth=1
	movl	$.Lstr.2, %edi
	callq	puts
	movq	dpb+8(%rip), %rax
	movq	(%rax,%r12,8), %rax
	movq	48(%rax), %rsi
	movl	8(%rax), %ecx
	jmp	.LBB26_82
.LBB26_78:                              #   in Loop: Header=BB26_87 Depth=1
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB26_83
# BB#79:                                #   in Loop: Header=BB26_87 Depth=1
	cmpl	%r15d, 28(%rax)
	je	.LBB26_84
# BB#80:                                #   in Loop: Header=BB26_87 Depth=1
	movl	$.Lstr.2, %edi
	callq	puts
	movq	dpb+8(%rip), %rax
	movq	(%rax,%r12,8), %rax
	movq	56(%rax), %rsi
	movl	8(%rax), %ecx
	jmp	.LBB26_84
.LBB26_81:                              #   in Loop: Header=BB26_87 Depth=1
	xorl	%ecx, %ecx
.LBB26_82:                              #   in Loop: Header=BB26_87 Depth=1
	movl	%r15d, 6372(%rsi)
	movl	%r15d, 28(%rax)
	leal	(%rbx,%r15,2), %edx
	movl	%edx, 6368(%rsi)
	movl	$1, 6376(%rsi)
	orl	$1, %ecx
	jmp	.LBB26_85
.LBB26_83:                              #   in Loop: Header=BB26_87 Depth=1
	xorl	%ecx, %ecx
.LBB26_84:                              #   in Loop: Header=BB26_87 Depth=1
	movl	%r15d, 6372(%rsi)
	movl	%r15d, 28(%rax)
	leal	(%rbx,%r15,2), %edx
	movl	%edx, 6368(%rsi)
	movl	$1, 6376(%rsi)
	orl	$2, %ecx
.LBB26_85:                              #   in Loop: Header=BB26_87 Depth=1
	movl	%ecx, 8(%rax)
	cmpl	$3, %ecx
	movl	$1, %r12d
	jne	.LBB26_234
# BB#86:                                #   in Loop: Header=BB26_87 Depth=1
	movq	40(%rax), %rax
	movl	$1, 6376(%rax)
	movl	%r15d, 6368(%rax)
	movl	%r15d, 6372(%rax)
	jmp	.LBB26_234
	.p2align	4, 0x90
.LBB26_87:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_204 Depth 2
                                        #     Child Loop BB26_159 Depth 2
                                        #     Child Loop BB26_162 Depth 2
                                        #     Child Loop BB26_142 Depth 2
                                        #     Child Loop BB26_131 Depth 2
                                        #     Child Loop BB26_226 Depth 2
                                        #     Child Loop BB26_181 Depth 2
                                        #     Child Loop BB26_197 Depth 2
                                        #     Child Loop BB26_105 Depth 2
                                        #     Child Loop BB26_112 Depth 2
                                        #     Child Loop BB26_95 Depth 2
                                        #     Child Loop BB26_219 Depth 2
	movl	(%r13), %ecx
	cmpq	$6, %rcx
	ja	.LBB26_91
# BB#88:                                #   in Loop: Header=BB26_87 Depth=1
	jmpq	*.LJTI26_0(,%rcx,8)
.LBB26_89:                              #   in Loop: Header=BB26_87 Depth=1
	cmpq	$0, 24(%r13)
	je	.LBB26_249
# BB#90:                                #   in Loop: Header=BB26_87 Depth=1
	movl	$.L.str.22, %edi
	jmp	.LBB26_238
.LBB26_91:                              #   in Loop: Header=BB26_87 Depth=1
	movl	$.L.str.23, %edi
	jmp	.LBB26_238
.LBB26_92:                              #   in Loop: Header=BB26_87 Depth=1
	movl	(%r14), %edx
	movl	6360(%r14), %eax
	testl	%edx, %edx
	leal	1(%rax,%rax), %r8d
	cmovel	%eax, %r8d
	movl	dpb+32(%rip), %ecx
	testq	%rcx, %rcx
	je	.LBB26_245
# BB#93:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movl	4(%r13), %eax
	notl	%eax
	addl	%eax, %r8d
	testl	%edx, %edx
	movq	dpb+8(%rip), %rdx
	je	.LBB26_218
# BB#94:                                # %.lr.ph.split.i.i.preheader
                                        #   in Loop: Header=BB26_87 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB26_95:                              # %.lr.ph.split.i.i
                                        #   Parent Loop BB26_87 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rdi,8), %rsi
	movl	4(%rsi), %ebp
	movl	%ebp, %ebx
	andl	$1, %ebx
	je	.LBB26_98
# BB#96:                                #   in Loop: Header=BB26_95 Depth=2
	testb	$1, 8(%rsi)
	jne	.LBB26_98
# BB#97:                                #   in Loop: Header=BB26_95 Depth=2
	movq	48(%rsi), %rax
	cmpl	%r8d, 6364(%rax)
	je	.LBB26_241
.LBB26_98:                              #   in Loop: Header=BB26_95 Depth=2
	testb	$2, %bpl
	je	.LBB26_101
# BB#99:                                #   in Loop: Header=BB26_95 Depth=2
	testb	$2, 8(%rsi)
	jne	.LBB26_101
# BB#100:                               #   in Loop: Header=BB26_95 Depth=2
	movq	56(%rsi), %rax
	cmpl	%r8d, 6364(%rax)
	je	.LBB26_243
.LBB26_101:                             #   in Loop: Header=BB26_95 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jb	.LBB26_95
	jmp	.LBB26_245
.LBB26_102:                             #   in Loop: Header=BB26_87 Depth=1
	movl	dpb+36(%rip), %eax
	testq	%rax, %rax
	je	.LBB26_248
# BB#103:                               # %.lr.ph.i34.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movl	8(%r13), %r8d
	cmpl	$0, (%r14)
	movq	dpb+16(%rip), %rdx
	je	.LBB26_112
# BB#104:                               # %.lr.ph.split.i38.i.preheader
                                        #   in Loop: Header=BB26_87 Depth=1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB26_105:                             # %.lr.ph.split.i38.i
                                        #   Parent Loop BB26_87 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rsi,8), %rdi
	movl	4(%rdi), %ebp
	movl	%ebp, %ecx
	andl	$1, %ecx
	je	.LBB26_108
# BB#106:                               #   in Loop: Header=BB26_105 Depth=2
	movl	8(%rdi), %r9d
	testb	$1, %r9b
	je	.LBB26_108
# BB#107:                               #   in Loop: Header=BB26_105 Depth=2
	movq	48(%rdi), %rbx
	cmpl	%r8d, 6368(%rbx)
	je	.LBB26_242
.LBB26_108:                             #   in Loop: Header=BB26_105 Depth=2
	testb	$2, %bpl
	je	.LBB26_111
# BB#109:                               #   in Loop: Header=BB26_105 Depth=2
	movl	8(%rdi), %ebp
	testb	$2, %bpl
	je	.LBB26_111
# BB#110:                               #   in Loop: Header=BB26_105 Depth=2
	movq	56(%rdi), %rbx
	cmpl	%r8d, 6368(%rbx)
	je	.LBB26_246
.LBB26_111:                             #   in Loop: Header=BB26_105 Depth=2
	incq	%rsi
	cmpq	%rax, %rsi
	jb	.LBB26_105
	jmp	.LBB26_248
	.p2align	4, 0x90
.LBB26_112:                             # %.lr.ph.split.us.i36.i
                                        #   Parent Loop BB26_87 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rsi
	cmpl	$3, 4(%rsi)
	jne	.LBB26_127
# BB#113:                               #   in Loop: Header=BB26_112 Depth=2
	cmpl	$3, 8(%rsi)
	jne	.LBB26_127
# BB#114:                               #   in Loop: Header=BB26_112 Depth=2
	movq	40(%rsi), %rdi
	cmpl	%r8d, 6368(%rdi)
	jne	.LBB26_127
# BB#115:                               #   in Loop: Header=BB26_112 Depth=2
	movl	(%rsi), %ebp
	testb	$1, %bpl
	je	.LBB26_118
# BB#116:                               #   in Loop: Header=BB26_112 Depth=2
	movq	48(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB26_118
# BB#117:                               #   in Loop: Header=BB26_112 Depth=2
	movq	$0, 6376(%rcx)
.LBB26_118:                             #   in Loop: Header=BB26_112 Depth=2
	testb	$2, %bpl
	je	.LBB26_121
# BB#119:                               #   in Loop: Header=BB26_112 Depth=2
	movq	56(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB26_121
# BB#120:                               #   in Loop: Header=BB26_112 Depth=2
	movq	$0, 6376(%rcx)
.LBB26_121:                             # %thread-pre-split.i.us.i.i
                                        #   in Loop: Header=BB26_112 Depth=2
	cmpl	$3, %ebp
	jne	.LBB26_126
# BB#122:                               #   in Loop: Header=BB26_112 Depth=2
	movq	48(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB26_125
# BB#123:                               #   in Loop: Header=BB26_112 Depth=2
	movq	56(%rsi), %rbp
	testq	%rbp, %rbp
	je	.LBB26_125
# BB#124:                               #   in Loop: Header=BB26_112 Depth=2
	movl	$0, 6380(%rcx)
	movl	$0, 6376(%rcx)
	movq	$0, 6376(%rbp)
.LBB26_125:                             #   in Loop: Header=BB26_112 Depth=2
	movq	$0, 6376(%rdi)
.LBB26_126:                             # %unmark_for_long_term_reference.exit.us.i.i
                                        #   in Loop: Header=BB26_112 Depth=2
	movq	$0, 4(%rsi)
	.p2align	4, 0x90
.LBB26_127:                             #   in Loop: Header=BB26_112 Depth=2
	addq	$8, %rdx
	decq	%rax
	jne	.LBB26_112
	jmp	.LBB26_248
.LBB26_128:                             #   in Loop: Header=BB26_87 Depth=1
	movl	4(%r13), %ebp
	movl	12(%r13), %r15d
	movl	6360(%r14), %eax
	movl	(%r14), %ecx
	cmpl	$0, %ecx
	leal	1(%rax,%rax), %edx
	cmovel	%eax, %edx
	notl	%ebp
	addl	%edx, %ebp
	cmpl	$0, %ecx
	je	.LBB26_179
# BB#129:                               # %.preheader.i.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movl	dpb+32(%rip), %eax
	testq	%rax, %rax
	je	.LBB26_136
# BB#130:                               # %.lr.ph.i42.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movq	dpb+8(%rip), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB26_131:                             #   Parent Loop BB26_87 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rdx,8), %rsi
	movl	4(%rsi), %edi
	testb	$1, %dil
	je	.LBB26_133
# BB#132:                               #   in Loop: Header=BB26_131 Depth=2
	movq	48(%rsi), %rbx
	cmpl	%ebp, 6364(%rbx)
	je	.LBB26_239
.LBB26_133:                             #   in Loop: Header=BB26_131 Depth=2
	testb	$2, %dil
	je	.LBB26_135
# BB#134:                               #   in Loop: Header=BB26_131 Depth=2
	movq	56(%rsi), %rsi
	cmpl	%ebp, 6364(%rsi)
	je	.LBB26_240
.LBB26_135:                             #   in Loop: Header=BB26_131 Depth=2
	incq	%rdx
	cmpq	%rax, %rdx
	jb	.LBB26_131
.LBB26_136:                             # %._crit_edge.i.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movl	$.L.str.24, %edi
	movl	$200, %esi
	callq	error
	xorl	%edi, %edi
.LBB26_137:                             # %unmark_long_term_frame_for_reference_by_frame_idx.exit.i.i
                                        #   in Loop: Header=BB26_87 Depth=1
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	%r15d, %esi
	movl	%ebp, %r8d
	callq	unmark_long_term_field_for_reference_by_frame_idx
	movl	(%r14), %eax
	cmpl	$1, %eax
	je	.LBB26_223
# BB#138:                               # %unmark_long_term_frame_for_reference_by_frame_idx.exit.i.i
                                        #   in Loop: Header=BB26_87 Depth=1
	testl	%eax, %eax
	je	.LBB26_195
# BB#139:                               #   in Loop: Header=BB26_87 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB26_224
.LBB26_140:                             #   in Loop: Header=BB26_87 Depth=1
	movl	16(%r13), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, dpb+44(%rip)
	movl	dpb+36(%rip), %ecx
	testq	%rcx, %rcx
	je	.LBB26_248
# BB#141:                               # %.lr.ph.i45.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movq	dpb+16(%rip), %rdx
	.p2align	4, 0x90
.LBB26_142:                             #   Parent Loop BB26_87 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rsi
	cmpl	%eax, 28(%rsi)
	jl	.LBB26_155
# BB#143:                               #   in Loop: Header=BB26_142 Depth=2
	movl	(%rsi), %edi
	testb	$1, %dil
	je	.LBB26_146
# BB#144:                               #   in Loop: Header=BB26_142 Depth=2
	movq	48(%rsi), %rbp
	testq	%rbp, %rbp
	je	.LBB26_146
# BB#145:                               #   in Loop: Header=BB26_142 Depth=2
	movq	$0, 6376(%rbp)
.LBB26_146:                             #   in Loop: Header=BB26_142 Depth=2
	testb	$2, %dil
	je	.LBB26_149
# BB#147:                               #   in Loop: Header=BB26_142 Depth=2
	movq	56(%rsi), %rbp
	testq	%rbp, %rbp
	je	.LBB26_149
# BB#148:                               #   in Loop: Header=BB26_142 Depth=2
	movq	$0, 6376(%rbp)
.LBB26_149:                             # %thread-pre-split.i.i.i
                                        #   in Loop: Header=BB26_142 Depth=2
	cmpl	$3, %edi
	jne	.LBB26_154
# BB#150:                               #   in Loop: Header=BB26_142 Depth=2
	movq	48(%rsi), %rdi
	testq	%rdi, %rdi
	je	.LBB26_153
# BB#151:                               #   in Loop: Header=BB26_142 Depth=2
	movq	56(%rsi), %rbp
	testq	%rbp, %rbp
	je	.LBB26_153
# BB#152:                               #   in Loop: Header=BB26_142 Depth=2
	movl	$0, 6380(%rdi)
	movl	$0, 6376(%rdi)
	movq	$0, 6376(%rbp)
.LBB26_153:                             #   in Loop: Header=BB26_142 Depth=2
	movq	40(%rsi), %rdi
	movl	$0, 6380(%rdi)
	movl	$0, 6376(%rdi)
.LBB26_154:                             # %unmark_for_long_term_reference.exit.i.i
                                        #   in Loop: Header=BB26_142 Depth=2
	movq	$0, 4(%rsi)
.LBB26_155:                             #   in Loop: Header=BB26_142 Depth=2
	addq	$8, %rdx
	decq	%rcx
	jne	.LBB26_142
	jmp	.LBB26_248
.LBB26_157:                             #   in Loop: Header=BB26_87 Depth=1
	cmpl	$0, dpb+32(%rip)
	je	.LBB26_160
# BB#158:                               # %.lr.ph.i49.i.preheader
                                        #   in Loop: Header=BB26_87 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB26_159:                             # %.lr.ph.i49.i
                                        #   Parent Loop BB26_87 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	dpb+8(%rip), %rax
	movl	%ebx, %ecx
	movq	(%rax,%rcx,8), %rdi
	callq	unmark_for_reference
	incl	%ebx
	cmpl	dpb+32(%rip), %ebx
	jb	.LBB26_159
.LBB26_160:                             # %mm_unmark_all_short_term_for_reference.exit.i
                                        #   in Loop: Header=BB26_87 Depth=1
	callq	update_ref_list
	movl	$-1, dpb+44(%rip)
	movl	dpb+36(%rip), %eax
	testq	%rax, %rax
	je	.LBB26_176
# BB#161:                               # %.lr.ph.i.i51.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movq	dpb+16(%rip), %rcx
	.p2align	4, 0x90
.LBB26_162:                             #   Parent Loop BB26_87 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	cmpl	$0, 28(%rdx)
	js	.LBB26_175
# BB#163:                               #   in Loop: Header=BB26_162 Depth=2
	movl	(%rdx), %esi
	testb	$1, %sil
	je	.LBB26_166
# BB#164:                               #   in Loop: Header=BB26_162 Depth=2
	movq	48(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_166
# BB#165:                               #   in Loop: Header=BB26_162 Depth=2
	movq	$0, 6376(%rdi)
.LBB26_166:                             #   in Loop: Header=BB26_162 Depth=2
	testb	$2, %sil
	je	.LBB26_169
# BB#167:                               #   in Loop: Header=BB26_162 Depth=2
	movq	56(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_169
# BB#168:                               #   in Loop: Header=BB26_162 Depth=2
	movq	$0, 6376(%rdi)
.LBB26_169:                             # %thread-pre-split.i.i.i53.i
                                        #   in Loop: Header=BB26_162 Depth=2
	cmpl	$3, %esi
	jne	.LBB26_174
# BB#170:                               #   in Loop: Header=BB26_162 Depth=2
	movq	48(%rdx), %rsi
	testq	%rsi, %rsi
	je	.LBB26_173
# BB#171:                               #   in Loop: Header=BB26_162 Depth=2
	movq	56(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_173
# BB#172:                               #   in Loop: Header=BB26_162 Depth=2
	movl	$0, 6380(%rsi)
	movl	$0, 6376(%rsi)
	movq	$0, 6376(%rdi)
.LBB26_173:                             #   in Loop: Header=BB26_162 Depth=2
	movq	40(%rdx), %rsi
	movl	$0, 6380(%rsi)
	movl	$0, 6376(%rsi)
.LBB26_174:                             # %unmark_for_long_term_reference.exit.i.i54.i
                                        #   in Loop: Header=BB26_162 Depth=2
	movq	$0, 4(%rdx)
.LBB26_175:                             #   in Loop: Header=BB26_162 Depth=2
	addq	$8, %rcx
	decq	%rax
	jne	.LBB26_162
.LBB26_176:                             # %mm_unmark_all_long_term_for_reference.exit.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movq	img(%rip), %rax
	movl	$1, 15428(%rax)
	jmp	.LBB26_249
.LBB26_177:                             #   in Loop: Header=BB26_87 Depth=1
	movl	12(%r13), %ebp
	movl	(%r14), %edi
	testl	%edi, %edi
	je	.LBB26_202
# BB#178:                               #   in Loop: Header=BB26_87 Depth=1
	movl	6364(%r14), %ecx
	movl	$1, %edx
	xorl	%r8d, %r8d
	movl	%ebp, %esi
	callq	unmark_long_term_field_for_reference_by_frame_idx
	movl	dpb+36(%rip), %r8d
	movq	img(%rip), %rax
	jmp	.LBB26_236
.LBB26_179:                             #   in Loop: Header=BB26_87 Depth=1
	movl	dpb+36(%rip), %eax
	testq	%rax, %rax
	je	.LBB26_195
# BB#180:                               # %.lr.ph.i27.i.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movq	dpb+16(%rip), %rcx
	.p2align	4, 0x90
.LBB26_181:                             #   Parent Loop BB26_87 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	cmpl	%r15d, 28(%rdx)
	jne	.LBB26_194
# BB#182:                               #   in Loop: Header=BB26_181 Depth=2
	movl	(%rdx), %esi
	testb	$1, %sil
	je	.LBB26_185
# BB#183:                               #   in Loop: Header=BB26_181 Depth=2
	movq	48(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_185
# BB#184:                               #   in Loop: Header=BB26_181 Depth=2
	movq	$0, 6376(%rdi)
.LBB26_185:                             #   in Loop: Header=BB26_181 Depth=2
	testb	$2, %sil
	je	.LBB26_188
# BB#186:                               #   in Loop: Header=BB26_181 Depth=2
	movq	56(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_188
# BB#187:                               #   in Loop: Header=BB26_181 Depth=2
	movq	$0, 6376(%rdi)
.LBB26_188:                             # %thread-pre-split.i.i.i.i
                                        #   in Loop: Header=BB26_181 Depth=2
	cmpl	$3, %esi
	jne	.LBB26_193
# BB#189:                               #   in Loop: Header=BB26_181 Depth=2
	movq	48(%rdx), %rsi
	testq	%rsi, %rsi
	je	.LBB26_192
# BB#190:                               #   in Loop: Header=BB26_181 Depth=2
	movq	56(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_192
# BB#191:                               #   in Loop: Header=BB26_181 Depth=2
	movl	$0, 6380(%rsi)
	movl	$0, 6376(%rsi)
	movq	$0, 6376(%rdi)
.LBB26_192:                             #   in Loop: Header=BB26_181 Depth=2
	movq	40(%rdx), %rsi
	movl	$0, 6380(%rsi)
	movl	$0, 6376(%rsi)
.LBB26_193:                             # %unmark_for_long_term_reference.exit.i.i.i
                                        #   in Loop: Header=BB26_181 Depth=2
	movq	$0, 4(%rdx)
.LBB26_194:                             #   in Loop: Header=BB26_181 Depth=2
	addq	$8, %rcx
	decq	%rax
	jne	.LBB26_181
.LBB26_195:                             # %.preheader.i.i.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movl	dpb+32(%rip), %ecx
	testq	%rcx, %rcx
	je	.LBB26_201
# BB#196:                               # %.lr.ph9.i.i.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movq	dpb+8(%rip), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB26_197:                             #   Parent Loop BB26_87 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rsi,8), %rax
	cmpl	$3, 4(%rax)
	jne	.LBB26_200
# BB#198:                               #   in Loop: Header=BB26_197 Depth=2
	movq	40(%rax), %rdi
	cmpl	$0, 6376(%rdi)
	jne	.LBB26_200
# BB#199:                               #   in Loop: Header=BB26_197 Depth=2
	cmpl	%ebp, 6364(%rdi)
	je	.LBB26_70
	.p2align	4, 0x90
.LBB26_200:                             #   in Loop: Header=BB26_197 Depth=2
	incq	%rsi
	cmpq	%rcx, %rsi
	jb	.LBB26_197
.LBB26_201:                             # %._crit_edge10.i.i.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movl	$.Lstr.3, %edi
	callq	puts
	jmp	.LBB26_234
.LBB26_202:                             #   in Loop: Header=BB26_87 Depth=1
	movl	dpb+36(%rip), %r8d
	testq	%r8, %r8
	je	.LBB26_235
# BB#203:                               # %.lr.ph.i.i57.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movq	dpb+16(%rip), %rdx
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB26_204:                             #   Parent Loop BB26_87 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rdi
	cmpl	%ebp, 28(%rdi)
	jne	.LBB26_217
# BB#205:                               #   in Loop: Header=BB26_204 Depth=2
	movl	(%rdi), %ecx
	testb	$1, %cl
	je	.LBB26_208
# BB#206:                               #   in Loop: Header=BB26_204 Depth=2
	movq	48(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB26_208
# BB#207:                               #   in Loop: Header=BB26_204 Depth=2
	movq	$0, 6376(%rbx)
.LBB26_208:                             #   in Loop: Header=BB26_204 Depth=2
	testb	$2, %cl
	je	.LBB26_211
# BB#209:                               #   in Loop: Header=BB26_204 Depth=2
	movq	56(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB26_211
# BB#210:                               #   in Loop: Header=BB26_204 Depth=2
	movq	$0, 6376(%rbx)
.LBB26_211:                             # %thread-pre-split.i.i.i59.i
                                        #   in Loop: Header=BB26_204 Depth=2
	cmpl	$3, %ecx
	jne	.LBB26_216
# BB#212:                               #   in Loop: Header=BB26_204 Depth=2
	movq	48(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB26_215
# BB#213:                               #   in Loop: Header=BB26_204 Depth=2
	movq	56(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB26_215
# BB#214:                               #   in Loop: Header=BB26_204 Depth=2
	movl	$0, 6380(%rcx)
	movl	$0, 6376(%rcx)
	movq	$0, 6376(%rbx)
.LBB26_215:                             #   in Loop: Header=BB26_204 Depth=2
	movq	40(%rdi), %rcx
	movl	$0, 6380(%rcx)
	movl	$0, 6376(%rcx)
.LBB26_216:                             # %unmark_for_long_term_reference.exit.i.i60.i
                                        #   in Loop: Header=BB26_204 Depth=2
	movq	$0, 4(%rdi)
.LBB26_217:                             #   in Loop: Header=BB26_204 Depth=2
	addq	$8, %rdx
	decq	%rsi
	jne	.LBB26_204
	jmp	.LBB26_236
.LBB26_218:                             # %.lr.ph.split.us.i.i.preheader
                                        #   in Loop: Header=BB26_87 Depth=1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB26_219:                             # %.lr.ph.split.us.i.i
                                        #   Parent Loop BB26_87 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rsi,8), %rdi
	cmpl	$3, 4(%rdi)
	jne	.LBB26_222
# BB#220:                               #   in Loop: Header=BB26_219 Depth=2
	cmpl	$0, 8(%rdi)
	jne	.LBB26_222
# BB#221:                               #   in Loop: Header=BB26_219 Depth=2
	movq	40(%rdi), %rax
	cmpl	%r8d, 6364(%rax)
	je	.LBB26_74
	.p2align	4, 0x90
.LBB26_222:                             #   in Loop: Header=BB26_219 Depth=2
	incq	%rsi
	cmpq	%rcx, %rsi
	jb	.LBB26_219
	jmp	.LBB26_245
.LBB26_223:                             #   in Loop: Header=BB26_87 Depth=1
	movl	$1, %ebx
.LBB26_224:                             #   in Loop: Header=BB26_87 Depth=1
	movl	dpb+32(%rip), %ecx
	testq	%rcx, %rcx
	je	.LBB26_233
# BB#225:                               # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movq	dpb+8(%rip), %rdx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB26_226:                             #   Parent Loop BB26_87 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%r12,8), %rax
	movl	4(%rax), %edi
	testb	$1, %dil
	je	.LBB26_229
# BB#227:                               #   in Loop: Header=BB26_226 Depth=2
	movq	48(%rax), %rsi
	cmpl	$0, 6376(%rsi)
	jne	.LBB26_229
# BB#228:                               #   in Loop: Header=BB26_226 Depth=2
	cmpl	%ebp, 6364(%rsi)
	je	.LBB26_75
	.p2align	4, 0x90
.LBB26_229:                             #   in Loop: Header=BB26_226 Depth=2
	testb	$2, %dil
	je	.LBB26_232
# BB#230:                               #   in Loop: Header=BB26_226 Depth=2
	movq	56(%rax), %rsi
	cmpl	$0, 6376(%rsi)
	jne	.LBB26_232
# BB#231:                               #   in Loop: Header=BB26_226 Depth=2
	cmpl	%ebp, 6364(%rsi)
	je	.LBB26_78
	.p2align	4, 0x90
.LBB26_232:                             #   in Loop: Header=BB26_226 Depth=2
	incq	%r12
	cmpq	%rcx, %r12
	jb	.LBB26_226
.LBB26_233:                             # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movl	$.Lstr, %edi
	callq	puts
	movl	$1, %r12d
.LBB26_234:                             # %mm_assign_long_term_frame_idx.exit.i
                                        #   in Loop: Header=BB26_87 Depth=1
	callq	update_ref_list
.LBB26_248:                             # %mm_unmark_long_term_for_reference.exit.i
                                        #   in Loop: Header=BB26_87 Depth=1
	callq	update_ltref_list
	jmp	.LBB26_249
.LBB26_235:                             #   in Loop: Header=BB26_87 Depth=1
	xorl	%r8d, %r8d
.LBB26_236:                             # %mm_mark_current_picture_long_term.exit.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movl	$1, 6376(%r14)
	movl	%ebp, 6372(%r14)
	addl	dpb+32(%rip), %r8d
	movl	28(%rax), %eax
	testl	%eax, %eax
	cmovlel	%r12d, %eax
	cmpl	%eax, %r8d
	jle	.LBB26_249
# BB#237:                               #   in Loop: Header=BB26_87 Depth=1
	movl	$.L.str.1, %edi
.LBB26_238:                             # %check_num_ref.exit.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	jmp	.LBB26_249
.LBB26_239:                             #   in Loop: Header=BB26_87 Depth=1
	movl	$1, %edi
	jmp	.LBB26_137
.LBB26_240:                             #   in Loop: Header=BB26_87 Depth=1
	movl	$2, %edi
	jmp	.LBB26_137
.LBB26_241:                             # %.us-lcssa.i.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movl	$0, 6380(%rax)
	andl	$2, %ebp
	movl	%ebp, 4(%rsi)
	cmpl	$3, (%rsi)
	je	.LBB26_244
	jmp	.LBB26_245
.LBB26_242:                             # %.us-lcssa.i39.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movl	$0, 6380(%rbx)
	movl	$0, 6376(%rbx)
	andl	$2, %ebp
	movl	%ebp, 4(%rdi)
	andl	$2, %r9d
	movl	%r9d, 8(%rdi)
	cmpl	$3, (%rdi)
	je	.LBB26_247
	jmp	.LBB26_248
.LBB26_243:                             # %.us-lcssa9.i.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movl	$0, 6380(%rax)
	movl	%ebx, 4(%rsi)
	cmpl	$3, (%rsi)
	jne	.LBB26_245
.LBB26_244:                             #   in Loop: Header=BB26_87 Depth=1
	movq	40(%rsi), %rax
	movl	$0, 6380(%rax)
.LBB26_245:                             # %mm_unmark_short_term_for_reference.exit.i
                                        #   in Loop: Header=BB26_87 Depth=1
	callq	update_ref_list
	.p2align	4, 0x90
.LBB26_249:                             # %check_num_ref.exit.i
                                        #   in Loop: Header=BB26_87 Depth=1
	movq	24(%r13), %rax
	movq	img(%rip), %rcx
	movq	%rax, 15376(%rcx)
	movq	%r13, %rdi
	callq	free
	movq	img(%rip), %rax
	movq	15376(%rax), %r13
	testq	%r13, %r13
	jne	.LBB26_87
# BB#250:                               # %._crit_edge.i25
	cmpl	$0, 15428(%rax)
	je	.LBB26_35
# BB#251:
	movq	$0, 6360(%r14)
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	je	.LBB26_257
# BB#252:
	cmpl	$1, %ecx
	je	.LBB26_258
# BB#253:
	cmpl	$2, %ecx
	jne	.LBB26_259
# BB#254:
	movl	$0, 15320(%rax)
	movl	$0, 12(%r14)
	movl	$0, 4(%r14)
	xorl	%ecx, %ecx
	jmp	.LBB26_260
.LBB26_255:
	callq	unmark_for_reference
	callq	update_ref_list
.LBB26_256:                             # %sliding_window_memory_management.exit
	movl	$0, 6376(%r14)
	jmp	.LBB26_46
.LBB26_257:
	movl	4(%r14), %edx
	movl	8(%r14), %esi
	subl	%edx, %esi
	movl	%esi, 8(%r14)
	movl	12(%r14), %ecx
	subl	%edx, %ecx
	movl	%ecx, 12(%r14)
	movl	%esi, 15316(%rax)
	movl	%ecx, 15320(%rax)
	cmpl	%ecx, %esi
	cmovlel	%esi, %ecx
	movl	%ecx, 4(%r14)
	movl	%ecx, 15324(%rax)
	jmp	.LBB26_260
.LBB26_258:
	movl	$0, 15316(%rax)
	movq	$0, 4(%r14)
	xorl	%ecx, %ecx
	jmp	.LBB26_260
.LBB26_259:                             # %._crit_edge165.i
	movl	4(%r14), %ecx
.LBB26_260:
	movl	%ecx, 15328(%rax)
	callq	flush_dpb
	jmp	.LBB26_35
.LBB26_261:                             # %remove_unused_frame_from_dpb.exitthread-pre-split
	movl	dpb+28(%rip), %eax
	movl	dpb+24(%rip), %r8d
.LBB26_262:                             # %remove_unused_frame_from_dpb.exit
	movl	6380(%r14), %ecx
	cmpl	%r8d, %eax
	jne	.LBB26_265
# BB#263:
	testl	%ecx, %ecx
	je	.LBB26_275
.LBB26_264:
	callq	output_one_frame_from_dpb
	jmp	.LBB26_261
.LBB26_265:
	testl	%ecx, %ecx
	je	.LBB26_273
# BB#266:
	cmpl	$0, 6376(%r14)
	jne	.LBB26_273
# BB#267:
	movl	dpb+32(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB26_273
# BB#268:                               # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB26_269:                             # =>This Inner Loop Header: Depth=1
	movq	dpb+8(%rip), %rax
	movl	%ebx, %edx
	movq	(%rax,%rdx,8), %rax
	movl	20(%rax), %eax
	cmpl	6360(%r14), %eax
	jne	.LBB26_271
# BB#270:                               #   in Loop: Header=BB26_269 Depth=1
	movl	$.L.str.14, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movl	dpb+32(%rip), %ecx
.LBB26_271:                             #   in Loop: Header=BB26_269 Depth=1
	incl	%ebx
	cmpl	%ecx, %ebx
	jb	.LBB26_269
# BB#272:                               # %.loopexit.loopexit
	movl	dpb+28(%rip), %eax
.LBB26_273:                             # %.loopexit
	movq	dpb(%rip), %rcx
	movl	%eax, %eax
	movq	(%rcx,%rax,8), %rdi
	movq	%r14, %rsi
	callq	insert_picture_in_dpb
	cmpl	$0, (%r14)
	je	.LBB26_284
# BB#274:
	movq	dpb(%rip), %rcx
	movl	dpb+28(%rip), %eax
	movq	(%rcx,%rax,8), %rcx
	jmp	.LBB26_285
.LBB26_275:
	testl	%r8d, %r8d
	jne	.LBB26_277
# BB#276:
	movl	$.L.str.28, %edi
	movl	$150, %esi
	callq	error
	movl	dpb+28(%rip), %r8d
	testl	%r8d, %r8d
	je	.LBB26_283
.LBB26_277:                             # %.lr.ph.i32
	xorl	%esi, %esi
	movl	$-1, %edx
	movl	$2147483647, %eax       # imm = 0x7FFFFFFF
	movq	dpb(%rip), %rdi
	movl	%r8d, %ecx
	.p2align	4, 0x90
.LBB26_278:                             # %._crit_edge12.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%esi, %ebp
	movq	(%rdi,%rbp,8), %rbp
	movl	36(%rbp), %ebx
	cmpl	%ebx, %eax
	jle	.LBB26_280
# BB#279:                               #   in Loop: Header=BB26_278 Depth=1
	cmpl	$0, 32(%rbp)
	cmovel	%ebx, %eax
	cmovel	%esi, %edx
	cmovel	%r8d, %ecx
.LBB26_280:                             #   in Loop: Header=BB26_278 Depth=1
	incl	%esi
	cmpl	%ecx, %esi
	jb	.LBB26_278
# BB#281:                               # %get_smallest_poc.exit
	cmpl	$-1, %edx
	je	.LBB26_283
# BB#282:
	cmpl	%eax, 4(%r14)
	jge	.LBB26_264
.LBB26_283:                             # %get_smallest_poc.exit.thread
	movl	p_dec(%rip), %esi
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	direct_output           # TAILCALL
.LBB26_284:                             # %.loopexit._crit_edge
	movl	dpb+28(%rip), %eax
	xorl	%ecx, %ecx
.LBB26_285:
	movq	%rcx, dpb+56(%rip)
	incl	%eax
	movl	%eax, dpb+28(%rip)
	callq	update_ref_list
	callq	update_ltref_list
	movl	dpb+32(%rip), %eax
	addl	dpb+36(%rip), %eax
	movq	img(%rip), %rcx
	movl	28(%rcx), %ecx
	testl	%ecx, %ecx
	movl	$1, %edx
	cmovgl	%ecx, %edx
	cmpl	%edx, %eax
	jle	.LBB26_287
# BB#286:
	movl	$.L.str.1, %edi
	movl	$500, %esi              # imm = 0x1F4
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	error                   # TAILCALL
.LBB26_287:                             # %check_num_ref.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	store_picture_in_dpb, .Lfunc_end26-store_picture_in_dpb
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI26_0:
	.quad	.LBB26_89
	.quad	.LBB26_92
	.quad	.LBB26_102
	.quad	.LBB26_128
	.quad	.LBB26_140
	.quad	.LBB26_157
	.quad	.LBB26_177

	.text
	.p2align	4, 0x90
	.type	insert_picture_in_dpb,@function
insert_picture_in_dpb:                  # @insert_picture_in_dpb
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 32
.Lcfi87:
	.cfi_offset %rbx, -32
.Lcfi88:
	.cfi_offset %r14, -24
.Lcfi89:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	cmpl	$0, 6380(%r15)
	je	.LBB27_2
# BB#1:
	movq	%r15, %rdi
	callq	UnifiedOneForthPix
.LBB27_2:
	movl	(%r15), %eax
	cmpl	$2, %eax
	je	.LBB27_27
# BB#3:
	cmpl	$1, %eax
	je	.LBB27_9
# BB#4:
	testl	%eax, %eax
	jne	.LBB27_44
# BB#5:
	movq	%r15, 40(%r14)
	movl	$3, (%r14)
	cmpl	$0, 6380(%r15)
	je	.LBB27_8
# BB#6:
	movl	$3, 4(%r14)
	movl	$3, 12(%r14)
	cmpl	$0, 6376(%r15)
	je	.LBB27_8
# BB#7:
	movl	$3, 8(%r14)
	movl	6372(%r15), %eax
	movl	%eax, 28(%r14)
.LBB27_8:
	movq	%r14, %rdi
	callq	dpb_split_field
	jmp	.LBB27_44
.LBB27_9:
	movq	%r15, 48(%r14)
	movl	(%r14), %eax
	orl	$1, %eax
	movl	%eax, (%r14)
	cmpl	$0, 6380(%r15)
	je	.LBB27_12
# BB#10:
	orb	$1, 4(%r14)
	orb	$1, 12(%r14)
	cmpl	$0, 6376(%r15)
	je	.LBB27_12
# BB#11:
	orb	$1, 8(%r14)
	movl	6372(%r15), %ecx
	movl	%ecx, 28(%r14)
.LBB27_12:                              # %thread-pre-split
	cmpl	$3, %eax
	je	.LBB27_13
# BB#14:
	movl	4(%r15), %eax
	movl	%eax, 36(%r14)
	movl	6392(%r15), %ecx
	cmpl	$4, %ecx
	jl	.LBB27_44
# BB#15:                                # %.preheader.lr.ph.i
	movl	6396(%r15), %edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB27_16:                              # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_18 Depth 2
	cmpl	$4, %edx
	jl	.LBB27_26
# BB#17:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB27_16 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB27_18:                              # %.lr.ph.i
                                        #   Parent Loop BB27_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	6488(%r15), %rdx
	movq	(%rdx), %rsi
	movq	8(%rdx), %rdx
	movq	(%rsi,%rcx,8), %rsi
	movsbq	(%rsi,%rax), %rsi
	testq	%rsi, %rsi
	movq	(%rdx,%rcx,8), %rdx
	movsbq	(%rdx,%rax), %rdx
	js	.LBB27_19
# BB#20:                                #   in Loop: Header=BB27_18 Depth=2
	movq	24(%r15,%rsi,8), %rdi
	jmp	.LBB27_21
	.p2align	4, 0x90
.LBB27_19:                              #   in Loop: Header=BB27_18 Depth=2
	xorl	%edi, %edi
.LBB27_21:                              #   in Loop: Header=BB27_18 Depth=2
	movq	6504(%r15), %rsi
	movq	(%rsi), %rbx
	movq	(%rbx,%rcx,8), %rbx
	movq	%rdi, (%rbx,%rax,8)
	testb	%dl, %dl
	js	.LBB27_22
# BB#23:                                #   in Loop: Header=BB27_18 Depth=2
	movq	288(%r15,%rdx,8), %rdx
	jmp	.LBB27_24
	.p2align	4, 0x90
.LBB27_22:                              #   in Loop: Header=BB27_18 Depth=2
	xorl	%edx, %edx
.LBB27_24:                              #   in Loop: Header=BB27_18 Depth=2
	movq	8(%rsi), %rsi
	movq	(%rsi,%rcx,8), %rsi
	movq	%rdx, (%rsi,%rax,8)
	movq	6528(%r15), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movb	$1, (%rdx,%rax)
	incq	%rcx
	movl	6396(%r15), %edx
	movl	%edx, %esi
	sarl	$31, %esi
	shrl	$30, %esi
	addl	%edx, %esi
	sarl	$2, %esi
	movslq	%esi, %rsi
	cmpq	%rsi, %rcx
	jl	.LBB27_18
# BB#25:                                # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB27_16 Depth=1
	movl	6392(%r15), %ecx
.LBB27_26:                              # %._crit_edge.i
                                        #   in Loop: Header=BB27_16 Depth=1
	incq	%rax
	movl	%ecx, %esi
	sarl	$31, %esi
	shrl	$30, %esi
	addl	%ecx, %esi
	sarl	$2, %esi
	movslq	%esi, %rsi
	cmpq	%rsi, %rax
	jl	.LBB27_16
	jmp	.LBB27_44
.LBB27_27:
	movq	%r15, 56(%r14)
	movl	(%r14), %eax
	orl	$2, %eax
	movl	%eax, (%r14)
	cmpl	$0, 6380(%r15)
	je	.LBB27_30
# BB#28:
	orb	$2, 4(%r14)
	orb	$2, 12(%r14)
	cmpl	$0, 6376(%r15)
	je	.LBB27_30
# BB#29:
	orb	$2, 8(%r14)
	movl	6372(%r15), %ecx
	movl	%ecx, 28(%r14)
.LBB27_30:                              # %thread-pre-split60
	cmpl	$3, %eax
	jne	.LBB27_31
.LBB27_13:
	movq	%r14, %rdi
	callq	dpb_combine_field
.LBB27_44:                              # %gen_field_ref_ids.exit
	movl	6364(%r15), %eax
	movl	%eax, 20(%r14)
	movl	6384(%r15), %eax
	movl	%eax, 32(%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB27_31:
	movl	4(%r15), %eax
	movl	%eax, 36(%r14)
	movl	6392(%r15), %ecx
	cmpl	$4, %ecx
	jl	.LBB27_44
# BB#32:                                # %.preheader.lr.ph.i49
	movl	6396(%r15), %edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB27_33:                              # %.preheader.i51
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_35 Depth 2
	cmpl	$4, %edx
	jl	.LBB27_43
# BB#34:                                # %.lr.ph.i53.preheader
                                        #   in Loop: Header=BB27_33 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB27_35:                              # %.lr.ph.i53
                                        #   Parent Loop BB27_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	6488(%r15), %rdx
	movq	(%rdx), %rsi
	movq	8(%rdx), %rdx
	movq	(%rsi,%rcx,8), %rsi
	movsbq	(%rsi,%rax), %rsi
	testq	%rsi, %rsi
	movq	(%rdx,%rcx,8), %rdx
	movsbq	(%rdx,%rax), %rdx
	js	.LBB27_36
# BB#37:                                #   in Loop: Header=BB27_35 Depth=2
	movq	24(%r15,%rsi,8), %rdi
	jmp	.LBB27_38
	.p2align	4, 0x90
.LBB27_36:                              #   in Loop: Header=BB27_35 Depth=2
	xorl	%edi, %edi
.LBB27_38:                              #   in Loop: Header=BB27_35 Depth=2
	movq	6504(%r15), %rsi
	movq	(%rsi), %rbx
	movq	(%rbx,%rcx,8), %rbx
	movq	%rdi, (%rbx,%rax,8)
	testb	%dl, %dl
	js	.LBB27_39
# BB#40:                                #   in Loop: Header=BB27_35 Depth=2
	movq	288(%r15,%rdx,8), %rdx
	jmp	.LBB27_41
	.p2align	4, 0x90
.LBB27_39:                              #   in Loop: Header=BB27_35 Depth=2
	xorl	%edx, %edx
.LBB27_41:                              #   in Loop: Header=BB27_35 Depth=2
	movq	8(%rsi), %rsi
	movq	(%rsi,%rcx,8), %rsi
	movq	%rdx, (%rsi,%rax,8)
	movq	6528(%r15), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movb	$1, (%rdx,%rax)
	incq	%rcx
	movl	6396(%r15), %edx
	movl	%edx, %esi
	sarl	$31, %esi
	shrl	$30, %esi
	addl	%edx, %esi
	sarl	$2, %esi
	movslq	%esi, %rsi
	cmpq	%rsi, %rcx
	jl	.LBB27_35
# BB#42:                                # %._crit_edge.loopexit.i56
                                        #   in Loop: Header=BB27_33 Depth=1
	movl	6392(%r15), %ecx
.LBB27_43:                              # %._crit_edge.i58
                                        #   in Loop: Header=BB27_33 Depth=1
	incq	%rax
	movl	%ecx, %esi
	sarl	$31, %esi
	shrl	$30, %esi
	addl	%ecx, %esi
	sarl	$2, %esi
	movslq	%esi, %rsi
	cmpq	%rsi, %rax
	jl	.LBB27_33
	jmp	.LBB27_44
.Lfunc_end27:
	.size	insert_picture_in_dpb, .Lfunc_end27-insert_picture_in_dpb
	.cfi_endproc

	.p2align	4, 0x90
	.type	output_one_frame_from_dpb,@function
output_one_frame_from_dpb:              # @output_one_frame_from_dpb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi92:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi94:
	.cfi_def_cfa_offset 48
.Lcfi95:
	.cfi_offset %rbx, -40
.Lcfi96:
	.cfi_offset %r14, -32
.Lcfi97:
	.cfi_offset %r15, -24
.Lcfi98:
	.cfi_offset %rbp, -16
	movl	dpb+28(%rip), %eax
	testl	%eax, %eax
	jne	.LBB28_4
# BB#1:
	movl	$.L.str.30, %edi
	movl	$150, %esi
	callq	error
	movl	dpb+28(%rip), %eax
	testl	%eax, %eax
	jne	.LBB28_4
# BB#2:                                 # %.thread
	movl	$.L.str.28, %edi
	movl	$150, %esi
	callq	error
	movl	dpb+28(%rip), %eax
	testl	%eax, %eax
	je	.LBB28_3
.LBB28_4:                               # %.lr.ph.i
	xorl	%edx, %edx
	movl	$-1, %r15d
	movl	$2147483647, %ebp       # imm = 0x7FFFFFFF
	movq	dpb(%rip), %rcx
	movl	%eax, %esi
	.p2align	4, 0x90
.LBB28_5:                               # %._crit_edge12.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %edi
	movq	(%rcx,%rdi,8), %rbx
	movl	36(%rbx), %edi
	cmpl	%edi, %ebp
	jle	.LBB28_7
# BB#6:                                 #   in Loop: Header=BB28_5 Depth=1
	cmpl	$0, 32(%rbx)
	cmovel	%edi, %ebp
	cmovel	%edx, %r15d
	cmovel	%eax, %esi
.LBB28_7:                               #   in Loop: Header=BB28_5 Depth=1
	incl	%edx
	cmpl	%esi, %edx
	jb	.LBB28_5
# BB#8:                                 # %get_smallest_poc.exit
	cmpl	$-1, %r15d
	jne	.LBB28_10
.LBB28_9:                               # %get_smallest_poc.exit.thread
	movl	$.L.str.31, %edi
	movl	$150, %esi
	callq	error
	movl	$-1, %r15d
	movq	dpb(%rip), %rcx
.LBB28_10:
	movslq	%r15d, %r14
	movq	(%rcx,%r14,8), %rdi
	movl	p_dec(%rip), %esi
	callq	write_stored_frame
	movq	input(%rip), %rax
	cmpl	$0, 5084(%rax)
	jne	.LBB28_13
# BB#11:
	cmpl	%ebp, dpb+40(%rip)
	jl	.LBB28_13
# BB#12:
	movl	$.L.str.32, %edi
	movl	$150, %esi
	callq	error
.LBB28_13:
	movl	%ebp, dpb+40(%rip)
	movq	dpb(%rip), %rax
	movq	(%rax,%r14,8), %rax
	cmpl	$0, 4(%rax)
	je	.LBB28_14
.LBB28_22:                              # %is_used_for_reference.exit.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB28_14:
	movl	(%rax), %ecx
	cmpl	$3, %ecx
	jne	.LBB28_16
# BB#15:
	movq	40(%rax), %rdx
	cmpl	$0, 6380(%rdx)
	jne	.LBB28_22
	jmp	.LBB28_17
.LBB28_16:
	testb	$1, %cl
	je	.LBB28_19
.LBB28_17:                              # %.thread.i
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB28_19
# BB#18:
	cmpl	$0, 6380(%rdx)
	jne	.LBB28_22
.LBB28_19:
	testb	$2, %cl
	je	.LBB28_23
# BB#20:
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.LBB28_23
# BB#21:
	cmpl	$0, 6380(%rax)
	jne	.LBB28_22
.LBB28_23:                              # %is_used_for_reference.exit
	movl	%r15d, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	remove_frame_from_dpb   # TAILCALL
.LBB28_3:
	movl	$2147483647, %ebp       # imm = 0x7FFFFFFF
	jmp	.LBB28_9
.Lfunc_end28:
	.size	output_one_frame_from_dpb, .Lfunc_end28-output_one_frame_from_dpb
	.cfi_endproc

	.globl	replace_top_pic_with_frame
	.p2align	4, 0x90
	.type	replace_top_pic_with_frame,@function
replace_top_pic_with_frame:             # @replace_top_pic_with_frame
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi101:
	.cfi_def_cfa_offset 32
.Lcfi102:
	.cfi_offset %rbx, -24
.Lcfi103:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	img(%rip), %rax
	xorl	%ecx, %ecx
	cmpl	$0, 15360(%rax)
	setne	%cl
	movl	%ecx, 6380(%r14)
	je	.LBB29_2
# BB#1:
	movq	%r14, %rdi
	callq	UnifiedOneForthPix
.LBB29_2:                               # %.preheader
	movl	dpb+28(%rip), %eax
	testq	%rax, %rax
	je	.LBB29_7
# BB#3:                                 # %.lr.ph
	movq	dpb(%rip), %rcx
	movq	img(%rip), %rdx
	movl	15332(%rdx), %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB29_4:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rbx
	cmpl	%edx, 20(%rbx)
	jne	.LBB29_6
# BB#5:                                 #   in Loop: Header=BB29_4 Depth=1
	cmpl	$1, (%rbx)
	je	.LBB29_8
.LBB29_6:                               #   in Loop: Header=BB29_4 Depth=1
	incq	%rsi
	cmpq	%rax, %rsi
	jb	.LBB29_4
.LBB29_7:                               # %._crit_edge
	movl	p_dec(%rip), %esi
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	direct_output_paff      # TAILCALL
.LBB29_8:
	movq	48(%rbx), %rdi
	callq	free_storable_picture
	movq	$0, 48(%rbx)
	movq	%r14, 40(%rbx)
	movl	$3, (%rbx)
	cmpl	$0, 6380(%r14)
	je	.LBB29_11
# BB#9:
	movl	$3, 4(%rbx)
	cmpl	$0, 6376(%r14)
	je	.LBB29_11
# BB#10:
	movl	$3, 8(%rbx)
.LBB29_11:
	movq	%rbx, %rdi
	callq	dpb_split_field
	callq	update_ref_list
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	update_ltref_list       # TAILCALL
.Lfunc_end29:
	.size	replace_top_pic_with_frame, .Lfunc_end29-replace_top_pic_with_frame
	.cfi_endproc

	.globl	dpb_split_field
	.p2align	4, 0x90
	.type	dpb_split_field,@function
dpb_split_field:                        # @dpb_split_field
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi106:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi107:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi108:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi109:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi110:
	.cfi_def_cfa_offset 96
.Lcfi111:
	.cfi_offset %rbx, -56
.Lcfi112:
	.cfi_offset %r12, -48
.Lcfi113:
	.cfi_offset %r13, -40
.Lcfi114:
	.cfi_offset %r14, -32
.Lcfi115:
	.cfi_offset %r15, -24
.Lcfi116:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	40(%r12), %r10
	movl	6392(%r10), %esi
	movl	%esi, %eax
	sarl	$4, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	4(%r10), %eax
	movl	%eax, 36(%r12)
	movq	active_sps(%rip), %rax
	cmpl	$0, 1148(%rax)
	je	.LBB30_1
# BB#14:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 6536(%r10)
	jmp	.LBB30_15
.LBB30_1:
	movl	6396(%r10), %eax
	movl	6400(%r10), %ecx
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%eax, %edx
	sarl	%edx
	movl	6404(%r10), %eax
	movl	%eax, %r8d
	shrl	$31, %r8d
	addl	%eax, %r8d
	sarl	%r8d
	movl	$1, %edi
	callq	alloc_storable_picture
	movq	%rax, 48(%r12)
	movq	40(%r12), %rax
	movl	6392(%rax), %esi
	movl	6396(%rax), %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	movl	6400(%rax), %ecx
	movl	6404(%rax), %eax
	movl	%eax, %r8d
	shrl	$31, %r8d
	addl	%eax, %r8d
	sarl	%r8d
	movl	$2, %edi
	callq	alloc_storable_picture
	movq	%rax, 56(%r12)
	movq	40(%r12), %rax
	cmpl	$2, 6396(%rax)
	jl	.LBB30_4
# BB#2:                                 # %.lr.ph510.preheader
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB30_3:                               # %.lr.ph510
                                        # =>This Inner Loop Header: Depth=1
	movq	48(%r12), %rcx
	movq	6440(%rcx), %rcx
	movq	(%rcx,%rbx), %rdi
	movq	6440(%rax), %rcx
	movq	(%rcx,%rbx,2), %rsi
	movslq	6392(%rax), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	incq	%rbp
	movq	40(%r12), %rax
	movl	6396(%rax), %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	movslq	%edx, %rcx
	addq	$8, %rbx
	cmpq	%rcx, %rbp
	jl	.LBB30_3
.LBB30_4:                               # %.preheader482
	cmpl	$2, 6404(%rax)
	jl	.LBB30_7
# BB#5:                                 # %.lr.ph507.preheader
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB30_6:                               # %.lr.ph507
                                        # =>This Inner Loop Header: Depth=1
	movq	48(%r12), %rcx
	movq	6472(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbx), %rdi
	movq	6472(%rax), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbx,2), %rsi
	movslq	6400(%rax), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	movq	40(%r12), %rax
	movq	48(%r12), %rcx
	movq	6472(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbx), %rdi
	movq	6472(%rax), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbx,2), %rsi
	movslq	6400(%rax), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	incq	%rbp
	movq	40(%r12), %rax
	movl	6404(%rax), %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	movslq	%edx, %rcx
	addq	$8, %rbx
	cmpq	%rcx, %rbp
	jl	.LBB30_6
.LBB30_7:                               # %.preheader481
	cmpl	$2, 6396(%rax)
	jl	.LBB30_10
# BB#8:                                 # %.lr.ph505.preheader
	movl	$1, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB30_9:                               # %.lr.ph505
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%r12), %rcx
	movq	6440(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rdi
	movq	6440(%rax), %rcx
	movslq	%ebx, %rbx
	movq	(%rcx,%rbx,8), %rsi
	movslq	6392(%rax), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	incq	%rbp
	movq	40(%r12), %rax
	movl	6396(%rax), %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	movslq	%edx, %rcx
	addl	$2, %ebx
	cmpq	%rcx, %rbp
	jl	.LBB30_9
.LBB30_10:                              # %.preheader480
	cmpl	$2, 6404(%rax)
	jl	.LBB30_13
# BB#11:                                # %.lr.ph502.preheader
	movl	$1, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB30_12:                              # %.lr.ph502
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%r12), %rcx
	movq	6472(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rdi
	movq	6472(%rax), %rcx
	movq	(%rcx), %rcx
	movslq	%ebx, %rbx
	movq	(%rcx,%rbx,8), %rsi
	movslq	6400(%rax), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	movq	40(%r12), %rax
	movq	56(%r12), %rcx
	movq	6472(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rdi
	movq	6472(%rax), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rsi
	movslq	6400(%rax), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	incq	%rbp
	movq	40(%r12), %rax
	movl	6404(%rax), %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	movslq	%edx, %rcx
	addl	$2, %ebx
	cmpq	%rcx, %rbp
	jl	.LBB30_12
.LBB30_13:                              # %._crit_edge503
	movq	48(%r12), %rdi
	callq	UnifiedOneForthPix
	movq	56(%r12), %rdi
	callq	UnifiedOneForthPix
	movq	40(%r12), %rsi
	movq	48(%r12), %rdi
	movl	8(%rsi), %eax
	movl	%eax, 4(%rdi)
	movl	12(%rsi), %ecx
	movq	56(%r12), %rdx
	movl	%ecx, 4(%rdx)
	movl	16(%rsi), %ebp
	movl	%ebp, 16(%rdi)
	movl	%ecx, 12(%rdx)
	movl	%ecx, 12(%rdi)
	movl	%eax, 8(%rdx)
	movl	%eax, 8(%rdi)
	movl	16(%rsi), %eax
	movl	%eax, 16(%rdx)
	movl	6380(%rsi), %eax
	movl	%eax, 6380(%rdx)
	movl	%eax, 6380(%rdi)
	movl	6376(%rsi), %eax
	movl	%eax, 6376(%rdx)
	movl	%eax, 6376(%rdi)
	movl	6372(%rsi), %eax
	movl	%eax, 6372(%rdx)
	movl	%eax, 6372(%rdi)
	movl	%eax, 28(%r12)
	movl	$1, 6428(%rdx)
	movl	$1, 6428(%rdi)
	movl	6432(%rsi), %eax
	movl	%eax, 6432(%rdx)
	movl	%eax, 6432(%rdi)
	movq	%rdi, 6536(%rsi)
	movq	%rdx, 6544(%rsi)
	movq	%rdx, 6544(%rdi)
	movq	%rsi, 6552(%rdi)
	movq	%rdi, 6536(%rdx)
	movq	%rsi, 6552(%rdx)
	movl	6560(%rsi), %eax
	movl	%eax, 6560(%rdx)
	movl	%eax, 6560(%rdi)
	addq	$288, %rdi              # imm = 0x120
	addq	$816, %rsi              # imm = 0x330
	movslq	listXsize+4(%rip), %r14
	shlq	$4, %r14
	movq	%r14, %rdx
	callq	memcpy
	movl	$288, %edi              # imm = 0x120
	addq	56(%r12), %rdi
	movl	$1344, %esi             # imm = 0x540
	addq	40(%r12), %rsi
	movq	%r14, %rdx
	callq	memcpy
	movq	48(%r12), %rdi
	addq	$24, %rdi
	movl	$552, %esi              # imm = 0x228
	addq	40(%r12), %rsi
	movslq	listXsize(%rip), %r14
	shlq	$4, %r14
	movq	%r14, %rdx
	callq	memcpy
	movq	56(%r12), %rdi
	addq	$24, %rdi
	movl	$1080, %esi             # imm = 0x438
	addq	40(%r12), %rsi
	movq	%r14, %rdx
	callq	memcpy
	movq	40(%r12), %r10
.LBB30_15:                              # %.preheader479
	movl	12(%rsp), %eax          # 4-byte Reload
	addl	%eax, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	6396(%r10), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	cmpl	$4, %eax
	jl	.LBB30_39
# BB#16:                                # %.lr.ph499.preheader
	movl	6392(%r10), %ecx
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	addl	%ecx, %eax
	sarl	$2, %eax
	movslq	%eax, %r11
	movl	16(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%ecx, %eax
	sarl	$2, %eax
	cltq
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB30_17:                              # %.lr.ph499
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_19 Depth 2
	cmpl	$4, 32(%rsp)            # 4-byte Folded Reload
	jl	.LBB30_38
# BB#18:                                # %.lr.ph496
                                        #   in Loop: Header=BB30_17 Depth=1
	movl	%ebp, %eax
	sarl	$31, %eax
	movl	%eax, %ecx
	shrl	$30, %ecx
	addl	%ebp, %ecx
	movl	%ecx, %r14d
	sarl	$2, %r14d
	shrl	$29, %eax
	addl	%ebp, %eax
	sarl	$3, %eax
	imull	12(%rsp), %eax          # 4-byte Folded Reload
	shrl	$31, %ecx
	addl	%r14d, %ecx
	andl	$-2, %ecx
	subl	%ecx, %r14d
	addl	%eax, %r14d
	movl	6432(%r10), %r13d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB30_19:                              #   Parent Loop BB30_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%r13d, %r13d
	je	.LBB30_33
# BB#20:                                #   in Loop: Header=BB30_19 Depth=2
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%ecx, %eax
	sarl	$2, %eax
	leal	(%r14,%rax,2), %ebx
	movq	6480(%r10), %rax
	movslq	%ebx, %rdx
	cmpb	$0, (%rax,%rdx)
	je	.LBB30_33
# BB#21:                                #   in Loop: Header=BB30_19 Depth=2
	addl	%ebx, %ebx
	andl	$2, %ebx
	leal	2(%rbx), %r9d
	movq	6488(%r10), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	movq	(%rdx,%rbp,8), %rdx
	movsbq	(%rdx,%rcx), %r15
	testq	%r15, %r15
	movq	(%rax,%rbp,8), %rax
	movsbq	(%rax,%rcx), %rdx
	js	.LBB30_22
# BB#23:                                #   in Loop: Header=BB30_19 Depth=2
	imulq	$264, %r9, %rax         # imm = 0x108
	addq	%r10, %rax
	movq	24(%rax,%r15,8), %r8
	jmp	.LBB30_24
	.p2align	4, 0x90
.LBB30_33:                              #   in Loop: Header=BB30_19 Depth=2
	movq	6488(%r10), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	movq	(%rdx,%rbp,8), %rdx
	movsbq	(%rdx,%rcx), %rsi
	testq	%rsi, %rsi
	movq	(%rax,%rbp,8), %rax
	movsbq	(%rax,%rcx), %rdi
	movq	$-1, %rdx
	movq	$-1, %rbx
	js	.LBB30_35
# BB#34:                                #   in Loop: Header=BB30_19 Depth=2
	movq	24(%r10,%rsi,8), %rbx
.LBB30_35:                              #   in Loop: Header=BB30_19 Depth=2
	movq	6504(%r10), %rsi
	movq	(%rsi), %rax
	movq	(%rax,%rbp,8), %rax
	movq	%rbx, (%rax,%rcx,8)
	testb	%dil, %dil
	js	.LBB30_37
# BB#36:                                #   in Loop: Header=BB30_19 Depth=2
	movq	288(%r10,%rdi,8), %rdx
	jmp	.LBB30_37
.LBB30_22:                              #   in Loop: Header=BB30_19 Depth=2
	xorl	%r8d, %r8d
.LBB30_24:                              # %._crit_edge538
                                        #   in Loop: Header=BB30_19 Depth=2
	movq	6504(%r10), %rsi
	movq	(%rsi,%r9,8), %rax
	movq	(%rax,%rbp,8), %rax
	movq	%r8, (%rax,%rcx,8)
	addl	$3, %ebx
	testb	%dl, %dl
	js	.LBB30_25
# BB#26:                                #   in Loop: Header=BB30_19 Depth=2
	imulq	$264, %rbx, %rax        # imm = 0x108
	addq	%r10, %rax
	movq	24(%rax,%rdx,8), %rax
	jmp	.LBB30_27
.LBB30_25:                              #   in Loop: Header=BB30_19 Depth=2
	xorl	%eax, %eax
.LBB30_27:                              # %._crit_edge537
                                        #   in Loop: Header=BB30_19 Depth=2
	movq	(%rsi,%rbx,8), %rdi
	movq	(%rdi,%rbp,8), %rdi
	movq	%rax, (%rdi,%rcx,8)
	testb	%r15b, %r15b
	js	.LBB30_28
# BB#29:                                #   in Loop: Header=BB30_19 Depth=2
	imulq	$264, %r9, %rax         # imm = 0x108
	addq	%r10, %rax
	movq	1608(%rax,%r15,8), %rax
	jmp	.LBB30_30
.LBB30_28:                              #   in Loop: Header=BB30_19 Depth=2
	xorl	%eax, %eax
.LBB30_30:                              #   in Loop: Header=BB30_19 Depth=2
	movq	(%rsi), %rdi
	movq	(%rdi,%rbp,8), %rdi
	movq	%rax, (%rdi,%rcx,8)
	testb	%dl, %dl
	js	.LBB30_31
# BB#32:                                #   in Loop: Header=BB30_19 Depth=2
	imulq	$264, %rbx, %rax        # imm = 0x108
	addq	%r10, %rax
	movq	1608(%rax,%rdx,8), %rdx
	jmp	.LBB30_37
.LBB30_31:                              #   in Loop: Header=BB30_19 Depth=2
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB30_37:                              #   in Loop: Header=BB30_19 Depth=2
	movq	8(%rsi), %rax
	movq	(%rax,%rbp,8), %rax
	movq	%rdx, (%rax,%rcx,8)
	incq	%rcx
	cmpq	%r11, %rcx
	jl	.LBB30_19
.LBB30_38:                              # %._crit_edge497
                                        #   in Loop: Header=BB30_17 Depth=1
	incq	%rbp
	cmpq	24(%rsp), %rbp          # 8-byte Folded Reload
	jl	.LBB30_17
.LBB30_39:                              # %._crit_edge500
	movq	active_sps(%rip), %rax
	cmpl	$0, 1148(%rax)
	je	.LBB30_42
# BB#40:
	movl	16(%rsp), %ecx          # 4-byte Reload
.LBB30_41:                              # %.loopexit478.thread545
	movq	6528(%r10), %rax
	movq	(%rax), %rdi
	movl	6392(%r10), %eax
	sarl	$4, %eax
	imull	%ecx, %eax
	movslq	%eax, %rdx
	xorl	%esi, %esi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	memset                  # TAILCALL
.LBB30_42:
	cmpl	$0, 6432(%r10)
	movl	16(%rsp), %ecx          # 4-byte Reload
	je	.LBB30_52
# BB#43:                                # %.preheader477
	cmpl	$8, %ecx
	jl	.LBB30_73
# BB#44:                                # %.lr.ph492
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB30_45:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_47 Depth 2
	cmpl	$4, 6392(%r10)
	jl	.LBB30_50
# BB#46:                                # %.lr.ph489
                                        #   in Loop: Header=BB30_45 Depth=1
	movl	%r13d, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%r13d, %eax
	movl	%eax, %esi
	sarl	$2, %esi
	andl	$-4, %eax
	movl	%r13d, %ecx
	subl	%eax, %ecx
	leal	(%rcx,%rsi,8), %eax
	movl	%r13d, %edi
	shrl	$31, %edi
	addl	%r13d, %edi
	movl	%edi, %r11d
	sarl	%r11d
	leal	4(%rax), %ebp
	imull	12(%rsp), %esi          # 4-byte Folded Reload
	shrl	$31, %edi
	addl	%r11d, %edi
	andl	$-2, %edi
	subl	%edi, %r11d
	addl	%esi, %r11d
	leal	(%r13,%r13), %esi
	leal	1(%r13,%r13), %edi
	movslq	%edi, %r8
	movslq	%esi, %r9
	movslq	%ebp, %r14
	movslq	%eax, %r15
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB30_47:                              #   Parent Loop BB30_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%edi, %eax
	sarl	$2, %eax
	leal	(%r11,%rax,2), %eax
	movq	6480(%r10), %rbx
	cltq
	cmpb	$0, (%rbx,%rax)
	je	.LBB30_49
# BB#48:                                #   in Loop: Header=BB30_47 Depth=2
	movq	48(%r12), %rax
	movq	6528(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movb	$1, (%rax,%rdi)
	movq	56(%r12), %rax
	movq	6528(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movb	$1, (%rax,%rdi)
	movq	40(%r12), %rax
	movq	6528(%rax), %rax
	movq	(%rax,%r8,8), %rax
	movb	$1, (%rax,%rdi)
	movq	40(%r12), %rax
	movq	6528(%rax), %rax
	movq	(%rax,%r9,8), %rax
	movb	$1, (%rax,%rdi)
	movq	40(%r12), %rax
	movq	56(%r12), %r10
	movq	6512(%rax), %rcx
	movq	(%rcx), %rbp
	movq	(%rbp,%r14,8), %rbp
	movq	(%rbp,%rdi,8), %rbp
	movzwl	(%rbp), %esi
	movq	6512(%r10), %rdx
	movq	(%rdx), %rbx
	movq	(%rbx,%r13,8), %rbx
	movq	(%rbx,%rdi,8), %rbx
	movw	%si, (%rbx)
	movzwl	2(%rbp), %esi
	movw	%si, 2(%rbx)
	movq	8(%rcx), %rcx
	movq	(%rcx,%r14,8), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movzwl	(%rcx), %esi
	movq	8(%rdx), %rdx
	movq	(%rdx,%r13,8), %rdx
	movq	(%rdx,%rdi,8), %rdx
	movw	%si, (%rdx)
	movzwl	2(%rcx), %ecx
	movw	%cx, 2(%rdx)
	movq	6488(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movzbl	(%rax,%rdi), %eax
	movq	6488(%r10), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movb	%al, (%rcx,%rdi)
	movq	40(%r12), %rax
	movq	56(%r12), %rcx
	movq	6488(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movzbl	(%rax,%rdi), %eax
	movq	6488(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movb	%al, (%rcx,%rdi)
	movq	40(%r12), %r10
	movq	56(%r12), %rcx
	movq	6504(%r10), %rdx
	movq	32(%rdx), %rsi
	movq	(%rsi,%r14,8), %rsi
	movq	(%rsi,%rdi,8), %rsi
	movq	6504(%rcx), %rcx
	movq	(%rcx), %rbp
	movq	(%rbp,%r13,8), %rbp
	movq	%rsi, (%rbp,%rdi,8)
	movq	40(%rdx), %rdx
	movq	(%rdx,%r14,8), %rdx
	movq	(%rdx,%rdi,8), %rdx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	%rdx, (%rcx,%rdi,8)
	movq	6512(%r10), %rcx
	movq	(%rcx), %rdx
	movq	(%rdx,%r15,8), %rdx
	movq	(%rdx,%rdi,8), %rdx
	movzwl	(%rdx), %esi
	movq	48(%r12), %rbp
	movq	6512(%rbp), %rbx
	movq	(%rbx), %rax
	movq	(%rax,%r13,8), %rax
	movq	(%rax,%rdi,8), %rax
	movw	%si, (%rax)
	movzwl	2(%rdx), %edx
	movw	%dx, 2(%rax)
	movq	8(%rcx), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax,%rdi,8), %rax
	movzwl	(%rax), %ecx
	movq	8(%rbx), %rdx
	movq	(%rdx,%r13,8), %rdx
	movq	(%rdx,%rdi,8), %rdx
	movw	%cx, (%rdx)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rdx)
	movq	6488(%r10), %rax
	movq	(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movzbl	(%rax,%rdi), %eax
	movq	6488(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movb	%al, (%rcx,%rdi)
	movq	40(%r12), %rax
	movq	48(%r12), %rcx
	movq	6488(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movzbl	(%rax,%rdi), %eax
	movq	6488(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movb	%al, (%rcx,%rdi)
	movq	40(%r12), %r10
	movq	48(%r12), %rax
	movq	6504(%r10), %rcx
	movq	16(%rcx), %rdx
	movq	(%rdx,%r15,8), %rdx
	movq	(%rdx,%rdi,8), %rdx
	movq	6504(%rax), %rax
	movq	(%rax), %rsi
	movq	(%rsi,%r13,8), %rsi
	movq	%rdx, (%rsi,%rdi,8)
	movq	24(%rcx), %rcx
	movq	(%rcx,%r15,8), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movq	8(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	%rcx, (%rax,%rdi,8)
.LBB30_49:                              #   in Loop: Header=BB30_47 Depth=2
	incq	%rdi
	movl	6392(%r10), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%eax, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rax
	cmpq	%rax, %rdi
	jl	.LBB30_47
.LBB30_50:                              # %._crit_edge490
                                        #   in Loop: Header=BB30_45 Depth=1
	incq	%r13
	movl	6396(%r10), %ecx
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$29, %eax
	addl	%ecx, %eax
	sarl	$3, %eax
	cltq
	cmpq	%rax, %r13
	jl	.LBB30_45
# BB#51:                                # %.loopexit478
	movq	active_sps(%rip), %rax
	cmpl	$0, 1148(%rax)
	jne	.LBB30_41
.LBB30_52:                              # %.preheader
	cmpl	$8, %ecx
	jl	.LBB30_73
# BB#53:                                # %.lr.ph486
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB30_54:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_56 Depth 2
	cmpl	$4, 6392(%r10)
	jl	.LBB30_72
# BB#55:                                # %.lr.ph
                                        #   in Loop: Header=BB30_54 Depth=1
	movl	%r15d, %edx
	shrl	$31, %edx
	addl	%r15d, %edx
	movl	%edx, %esi
	sarl	%esi
	movl	%r15d, %eax
	orl	$1, %eax
	movl	%r15d, %ecx
	andl	$2147483646, %ecx       # imm = 0x7FFFFFFE
	testb	$2, %r15b
	cmovnel	%eax, %ecx
	addl	%ecx, %ecx
	leal	(%r15,%r15), %eax
	cltq
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leal	1(%r15,%r15), %eax
	cltq
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movslq	%ecx, %r11
	movl	%r15d, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%r15d, %eax
	sarl	$2, %eax
	imull	12(%rsp), %eax          # 4-byte Folded Reload
	shrl	$31, %edx
	addl	%esi, %edx
	andl	$-2, %edx
	subl	%edx, %esi
	addl	%eax, %esi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB30_56:                              #   Parent Loop BB30_54 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rax
	orq	$1, %rax
	movl	%edx, %ebx
	andl	$-2, %ebx
	testb	$2, %dl
	cmovneq	%rax, %rbx
	cmpl	$0, 6432(%r10)
	je	.LBB30_58
# BB#57:                                #   in Loop: Header=BB30_56 Depth=2
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%edx, %eax
	sarl	$2, %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rax,2), %eax
	movq	6480(%r10), %rcx
	cltq
	movzbl	(%rcx,%rax), %ecx
	testb	%cl, %cl
	je	.LBB30_58
# BB#70:                                #   in Loop: Header=BB30_56 Depth=2
	movq	6528(%r10), %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	(%rax,%rsi,8), %rax
	movb	%cl, (%rax,%rdx)
	movq	40(%r12), %rax
	movq	6528(%rax), %rax
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	(%rax,%rsi,8), %rax
	movb	%cl, (%rax,%rdx)
	jmp	.LBB30_71
	.p2align	4, 0x90
.LBB30_58:                              #   in Loop: Header=BB30_56 Depth=2
	movq	6528(%r10), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movb	$0, (%rax,%rdx)
	movq	40(%r12), %rax
	movq	6528(%rax), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movb	$0, (%rax,%rdx)
	movq	56(%r12), %rax
	movq	6528(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movb	$0, (%rax,%rdx)
	movq	48(%r12), %rax
	movq	6528(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movb	$0, (%rax,%rdx)
	movq	40(%r12), %r14
	movq	56(%r12), %r13
	movq	6512(%r14), %rcx
	movq	(%rcx), %rax
	movq	(%rax,%r11,8), %rbp
	movslq	%ebx, %r10
	movq	(%rbp,%r10,8), %rbp
	movzwl	(%rbp), %ebx
	movq	6512(%r13), %rax
	movq	(%rax), %rsi
	movq	(%rsi,%r15,8), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movw	%bx, (%rsi)
	movq	48(%r12), %r8
	movq	6512(%r8), %r8
	movq	(%r8), %r9
	movq	(%r9,%r15,8), %rdi
	movq	(%rdi,%rdx,8), %rdi
	movw	%bx, (%rdi)
	movzwl	2(%rbp), %ebp
	movw	%bp, 2(%rsi)
	movw	%bp, 2(%rdi)
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%r10,8), %rcx
	movzwl	(%rcx), %esi
	movq	8(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax,%rdx,8), %rax
	movw	%si, (%rax)
	movq	8(%r8), %rdi
	movq	(%rdi,%r15,8), %rdi
	movq	(%rdi,%rdx,8), %rdi
	movw	%si, (%rdi)
	movzwl	2(%rcx), %ecx
	movw	%cx, 2(%rax)
	movw	%cx, 2(%rdi)
	movq	6488(%r14), %rax
	movq	(%rax), %rax
	movq	(%rax,%r11,8), %rax
	movsbq	(%rax,%r10), %rdi
	cmpq	$-1, %rdi
	movq	6488(%r13), %rax
	movq	(%rax), %rax
	movq	(%rax,%r15,8), %rcx
	je	.LBB30_59
# BB#60:                                #   in Loop: Header=BB30_56 Depth=2
	movb	%dil, (%rcx,%rdx)
	movq	48(%r12), %rax
	movq	6488(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movb	%dil, (%rax,%rdx)
	testb	%dil, %dil
	js	.LBB30_62
# BB#61:                                #   in Loop: Header=BB30_56 Depth=2
	movq	40(%r12), %rax
	movq	48(%r12), %rcx
	movq	3192(%rax,%rdi,8), %rsi
	movq	6504(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r15,8), %rcx
	movq	%rsi, (%rcx,%rdx,8)
	movq	4776(%rax,%rdi,8), %rcx
	jmp	.LBB30_63
	.p2align	4, 0x90
.LBB30_59:                              #   in Loop: Header=BB30_56 Depth=2
	movb	$-1, (%rcx,%rdx)
	movq	48(%r12), %rax
	movq	6488(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movb	$-1, (%rax,%rdx)
	movq	56(%r12), %rsi
	jmp	.LBB30_64
.LBB30_62:                              # %.critedge
                                        #   in Loop: Header=BB30_56 Depth=2
	movq	48(%r12), %rax
	movq	6504(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	$0, (%rax,%rdx,8)
	xorl	%ecx, %ecx
.LBB30_63:                              #   in Loop: Header=BB30_56 Depth=2
	movq	56(%r12), %rsi
	movq	6504(%rsi), %rax
	movq	(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	%rcx, (%rax,%rdx,8)
.LBB30_64:                              #   in Loop: Header=BB30_56 Depth=2
	movq	40(%r12), %rax
	movq	6488(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r11,8), %rax
	movsbq	(%rax,%r10), %rax
	cmpq	$-1, %rax
	movq	6488(%rsi), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r15,8), %rcx
	je	.LBB30_65
# BB#66:                                #   in Loop: Header=BB30_56 Depth=2
	movb	%al, (%rcx,%rdx)
	movq	48(%r12), %rcx
	movq	6488(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r15,8), %rcx
	movb	%al, (%rcx,%rdx)
	testb	%al, %al
	js	.LBB30_68
# BB#67:                                #   in Loop: Header=BB30_56 Depth=2
	movq	40(%r12), %rcx
	movq	48(%r12), %rsi
	movq	3456(%rcx,%rax,8), %rdi
	movq	6504(%rsi), %rsi
	movq	8(%rsi), %rsi
	movq	(%rsi,%r15,8), %rsi
	movq	%rdi, (%rsi,%rdx,8)
	movq	5040(%rcx,%rax,8), %rax
	jmp	.LBB30_69
	.p2align	4, 0x90
.LBB30_65:                              #   in Loop: Header=BB30_56 Depth=2
	movb	$-1, (%rcx,%rdx)
	movq	48(%r12), %rax
	movq	6488(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movb	$-1, (%rax,%rdx)
	jmp	.LBB30_71
.LBB30_68:                              # %.critedge476
                                        #   in Loop: Header=BB30_56 Depth=2
	movq	48(%r12), %rax
	movq	6504(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	$0, (%rax,%rdx,8)
	xorl	%eax, %eax
.LBB30_69:                              #   in Loop: Header=BB30_56 Depth=2
	movq	56(%r12), %rcx
	movq	6504(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r15,8), %rcx
	movq	%rax, (%rcx,%rdx,8)
.LBB30_71:                              #   in Loop: Header=BB30_56 Depth=2
	incq	%rdx
	movq	40(%r12), %r10
	movl	6392(%r10), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%eax, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rax
	cmpq	%rax, %rdx
	jl	.LBB30_56
.LBB30_72:                              # %._crit_edge
                                        #   in Loop: Header=BB30_54 Depth=1
	incq	%r15
	movl	6396(%r10), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%eax, %ecx
	sarl	$3, %ecx
	movslq	%ecx, %rax
	cmpq	%rax, %r15
	jl	.LBB30_54
.LBB30_73:                              # %.loopexit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end30:
	.size	dpb_split_field, .Lfunc_end30-dpb_split_field
	.cfi_endproc

	.globl	flush_dpb
	.p2align	4, 0x90
	.type	flush_dpb,@function
flush_dpb:                              # @flush_dpb
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi117:
	.cfi_def_cfa_offset 16
.Lcfi118:
	.cfi_offset %rbx, -16
	cmpl	$0, dpb+28(%rip)
	je	.LBB31_20
# BB#1:                                 # %.lr.ph7.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB31_2:                               # %.lr.ph7
                                        # =>This Inner Loop Header: Depth=1
	movq	dpb(%rip), %rax
	movl	%ebx, %ecx
	movq	(%rax,%rcx,8), %rdi
	callq	unmark_for_reference
	incl	%ebx
	movl	dpb+28(%rip), %eax
	cmpl	%eax, %ebx
	jb	.LBB31_2
# BB#3:                                 # %.preheader
	testl	%eax, %eax
	je	.LBB31_4
.LBB31_6:                               # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_7 Depth 2
	movq	dpb(%rip), %r8
	movl	%eax, %edx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB31_7:                               #   Parent Loop BB31_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r8,%rdi,8), %rsi
	cmpl	$0, 32(%rsi)
	je	.LBB31_17
# BB#8:                                 #   in Loop: Header=BB31_7 Depth=2
	cmpl	$0, 4(%rsi)
	jne	.LBB31_17
# BB#9:                                 #   in Loop: Header=BB31_7 Depth=2
	movl	(%rsi), %ecx
	cmpl	$3, %ecx
	jne	.LBB31_11
# BB#10:                                #   in Loop: Header=BB31_7 Depth=2
	movq	40(%rsi), %rbx
	cmpl	$0, 6380(%rbx)
	jne	.LBB31_17
	jmp	.LBB31_12
.LBB31_11:                              #   in Loop: Header=BB31_7 Depth=2
	testb	$1, %cl
	je	.LBB31_14
.LBB31_12:                              # %.thread.i.i
                                        #   in Loop: Header=BB31_7 Depth=2
	movq	48(%rsi), %rbx
	testq	%rbx, %rbx
	je	.LBB31_14
# BB#13:                                #   in Loop: Header=BB31_7 Depth=2
	cmpl	$0, 6380(%rbx)
	jne	.LBB31_17
.LBB31_14:                              #   in Loop: Header=BB31_7 Depth=2
	testb	$2, %cl
	je	.LBB31_5
# BB#15:                                #   in Loop: Header=BB31_7 Depth=2
	movq	56(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB31_5
# BB#16:                                #   in Loop: Header=BB31_7 Depth=2
	cmpl	$0, 6380(%rcx)
	je	.LBB31_5
	.p2align	4, 0x90
.LBB31_17:                              # %is_used_for_reference.exit.thread.i
                                        #   in Loop: Header=BB31_7 Depth=2
	incq	%rdi
	cmpq	%rdx, %rdi
	jb	.LBB31_7
	jmp	.LBB31_18
.LBB31_5:                               # %thread-pre-split
                                        #   in Loop: Header=BB31_6 Depth=1
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	remove_frame_from_dpb
	movl	dpb+28(%rip), %eax
	testl	%eax, %eax
	jne	.LBB31_6
	jmp	.LBB31_20
.LBB31_4:
	xorl	%eax, %eax
.LBB31_18:                              # %remove_unused_frame_from_dpb.exit.thread.preheader
	testl	%eax, %eax
	je	.LBB31_20
	.p2align	4, 0x90
.LBB31_19:                              # %remove_unused_frame_from_dpb.exit.thread
                                        # =>This Inner Loop Header: Depth=1
	callq	output_one_frame_from_dpb
	cmpl	$0, dpb+28(%rip)
	jne	.LBB31_19
.LBB31_20:                              # %remove_unused_frame_from_dpb.exit.thread._crit_edge
	movl	$-2147483648, dpb+40(%rip) # imm = 0x80000000
	popq	%rbx
	retq
.Lfunc_end31:
	.size	flush_dpb, .Lfunc_end31-flush_dpb
	.cfi_endproc

	.p2align	4, 0x90
	.type	unmark_for_reference,@function
unmark_for_reference:                   # @unmark_for_reference
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi119:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi120:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi121:
	.cfi_def_cfa_offset 32
.Lcfi122:
	.cfi_offset %rbx, -24
.Lcfi123:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	(%rbx), %eax
	testb	$1, %al
	je	.LBB32_3
# BB#1:
	movq	48(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB32_3
# BB#2:
	movl	$0, 6380(%rcx)
.LBB32_3:
	testb	$2, %al
	je	.LBB32_6
# BB#4:
	movq	56(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB32_6
# BB#5:
	movl	$0, 6380(%rcx)
.LBB32_6:                               # %thread-pre-split
	cmpl	$3, %eax
	jne	.LBB32_7
# BB#8:
	movq	48(%rbx), %rax
	testq	%rax, %rax
	je	.LBB32_11
# BB#9:
	movq	56(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB32_11
# BB#10:
	movl	$0, 6380(%rax)
	movl	$0, 6380(%rcx)
.LBB32_11:
	leaq	40(%rbx), %r14
	movq	40(%rbx), %rax
	movl	$0, 6380(%rax)
	jmp	.LBB32_12
.LBB32_7:                               # %._crit_edge
	leaq	40(%rbx), %r14
	movq	40(%rbx), %rax
.LBB32_12:
	movl	$0, 4(%rbx)
	testq	%rax, %rax
	je	.LBB32_19
# BB#13:
	movq	6448(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB32_15
# BB#14:
	movl	$4, %esi
	movl	$4, %edx
	callq	free_mem4Dpel
	movq	(%r14), %rax
	movq	$0, 6448(%rax)
.LBB32_15:
	movq	6496(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB32_17
# BB#16:
	movl	$6, %esi
	callq	free_mem3Dint64
	movq	(%r14), %rax
	movq	$0, 6496(%rax)
.LBB32_17:
	movq	6504(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB32_19
# BB#18:
	movl	$6, %esi
	callq	free_mem3Dint64
	movq	(%r14), %rax
	movq	$0, 6504(%rax)
.LBB32_19:
	movq	48(%rbx), %rax
	testq	%rax, %rax
	je	.LBB32_26
# BB#20:
	movq	6448(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB32_22
# BB#21:
	movl	$4, %esi
	movl	$4, %edx
	callq	free_mem4Dpel
	movq	48(%rbx), %rax
	movq	$0, 6448(%rax)
.LBB32_22:
	movq	6496(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB32_24
# BB#23:
	movl	$6, %esi
	callq	free_mem3Dint64
	movq	48(%rbx), %rax
	movq	$0, 6496(%rax)
.LBB32_24:
	movq	6504(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB32_26
# BB#25:
	movl	$6, %esi
	callq	free_mem3Dint64
	movq	48(%rbx), %rax
	movq	$0, 6504(%rax)
.LBB32_26:
	movq	56(%rbx), %rax
	testq	%rax, %rax
	je	.LBB32_33
# BB#27:
	movq	6448(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB32_29
# BB#28:
	movl	$4, %esi
	movl	$4, %edx
	callq	free_mem4Dpel
	movq	56(%rbx), %rax
	movq	$0, 6448(%rax)
.LBB32_29:
	movq	6496(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB32_31
# BB#30:
	movl	$6, %esi
	callq	free_mem3Dint64
	movq	56(%rbx), %rax
	movq	$0, 6496(%rax)
.LBB32_31:
	movq	6504(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB32_33
# BB#32:
	movl	$6, %esi
	callq	free_mem3Dint64
	movq	56(%rbx), %rax
	movq	$0, 6504(%rax)
.LBB32_33:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end32:
	.size	unmark_for_reference, .Lfunc_end32-unmark_for_reference
	.cfi_endproc

	.globl	gen_field_ref_ids
	.p2align	4, 0x90
	.type	gen_field_ref_ids,@function
gen_field_ref_ids:                      # @gen_field_ref_ids
	.cfi_startproc
# BB#0:
	movl	6392(%rdi), %ecx
	cmpl	$4, %ecx
	jl	.LBB33_13
# BB#1:                                 # %.preheader.lr.ph
	movl	6396(%rdi), %edx
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB33_2:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_4 Depth 2
	cmpl	$4, %edx
	jl	.LBB33_12
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB33_2 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB33_4:                               # %.lr.ph
                                        #   Parent Loop BB33_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	6488(%rdi), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %rsi
	movq	(%rdx,%rcx,8), %rax
	movsbq	(%rax,%r8), %rax
	testq	%rax, %rax
	movq	(%rsi,%rcx,8), %rdx
	movsbq	(%rdx,%r8), %rdx
	js	.LBB33_5
# BB#6:                                 #   in Loop: Header=BB33_4 Depth=2
	movq	24(%rdi,%rax,8), %r9
	jmp	.LBB33_7
	.p2align	4, 0x90
.LBB33_5:                               #   in Loop: Header=BB33_4 Depth=2
	xorl	%r9d, %r9d
.LBB33_7:                               #   in Loop: Header=BB33_4 Depth=2
	movq	6504(%rdi), %rsi
	movq	(%rsi), %rax
	movq	(%rax,%rcx,8), %rax
	movq	%r9, (%rax,%r8,8)
	testb	%dl, %dl
	js	.LBB33_8
# BB#9:                                 #   in Loop: Header=BB33_4 Depth=2
	movq	288(%rdi,%rdx,8), %rax
	jmp	.LBB33_10
	.p2align	4, 0x90
.LBB33_8:                               #   in Loop: Header=BB33_4 Depth=2
	xorl	%eax, %eax
.LBB33_10:                              #   in Loop: Header=BB33_4 Depth=2
	movq	8(%rsi), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	%rax, (%rdx,%r8,8)
	movq	6528(%rdi), %rax
	movq	(%rax,%rcx,8), %rax
	movb	$1, (%rax,%r8)
	incq	%rcx
	movl	6396(%rdi), %edx
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%edx, %eax
	sarl	$2, %eax
	cltq
	cmpq	%rax, %rcx
	jl	.LBB33_4
# BB#11:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB33_2 Depth=1
	movl	6392(%rdi), %ecx
.LBB33_12:                              # %._crit_edge
                                        #   in Loop: Header=BB33_2 Depth=1
	incq	%r8
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%ecx, %eax
	sarl	$2, %eax
	cltq
	cmpq	%rax, %r8
	jl	.LBB33_2
.LBB33_13:                              # %._crit_edge36
	retq
.Lfunc_end33:
	.size	gen_field_ref_ids, .Lfunc_end33-gen_field_ref_ids
	.cfi_endproc

	.globl	dpb_combine_field_yuv
	.p2align	4, 0x90
	.type	dpb_combine_field_yuv,@function
dpb_combine_field_yuv:                  # @dpb_combine_field_yuv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi124:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi125:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 32
.Lcfi127:
	.cfi_offset %rbx, -32
.Lcfi128:
	.cfi_offset %r14, -24
.Lcfi129:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	48(%r14), %rax
	movl	6392(%rax), %esi
	movl	6396(%rax), %edx
	addl	%edx, %edx
	movl	6400(%rax), %ecx
	movl	6404(%rax), %r8d
	addl	%r8d, %r8d
	xorl	%edi, %edi
	callq	alloc_storable_picture
	movq	%rax, 40(%r14)
	movq	48(%r14), %rcx
	cmpl	$0, 6396(%rcx)
	jle	.LBB34_3
# BB#1:                                 # %.lr.ph87
	xorl	%ebx, %ebx
	movl	$1, %r15d
	jmp	.LBB34_2
	.p2align	4, 0x90
.LBB34_16:                              # %._crit_edge91
                                        #   in Loop: Header=BB34_2 Depth=1
	movq	40(%r14), %rax
	addq	$8, %rbx
	incq	%r15
.LBB34_2:                               # =>This Inner Loop Header: Depth=1
	movq	6440(%rax), %rax
	movq	(%rax,%rbx,2), %rdi
	movq	6440(%rcx), %rax
	movq	(%rax,%rbx), %rsi
	movslq	6392(%rcx), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	movq	40(%r14), %rax
	movq	56(%r14), %rcx
	movq	6440(%rax), %rax
	movq	8(%rax,%rbx,2), %rdi
	movq	6440(%rcx), %rax
	movq	(%rax,%rbx), %rsi
	movslq	6392(%rcx), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	movq	48(%r14), %rcx
	movslq	6396(%rcx), %rax
	cmpq	%rax, %r15
	jl	.LBB34_16
.LBB34_3:                               # %.preheader
	cmpl	$0, 6404(%rcx)
	jle	.LBB34_6
# BB#4:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB34_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	40(%r14), %rax
	movq	6472(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%rbx,2), %rdi
	movq	6472(%rcx), %rax
	movq	(%rax), %rax
	movq	(%rax,%rbx), %rsi
	movslq	6400(%rcx), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	movq	40(%r14), %rax
	movq	56(%r14), %rcx
	movq	6472(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax,%rbx,2), %rdi
	movq	6472(%rcx), %rax
	movq	(%rax), %rax
	movq	(%rax,%rbx), %rsi
	movslq	6400(%rcx), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	movq	40(%r14), %rax
	movq	48(%r14), %rcx
	movq	6472(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbx,2), %rdi
	movq	6472(%rcx), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbx), %rsi
	movslq	6400(%rcx), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	movq	40(%r14), %rax
	movq	56(%r14), %rcx
	movq	6472(%rax), %rax
	movq	8(%rax), %rax
	movq	8(%rax,%rbx,2), %rdi
	movq	6472(%rcx), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbx), %rsi
	movslq	6400(%rcx), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	incq	%r15
	movq	48(%r14), %rcx
	movslq	6404(%rcx), %rax
	addq	$8, %rbx
	cmpq	%rax, %r15
	jl	.LBB34_5
.LBB34_6:                               # %._crit_edge
	movl	4(%rcx), %esi
	movq	40(%r14), %rax
	movq	56(%r14), %rdx
	movl	4(%rdx), %edi
	cmpl	%edi, %esi
	cmovlel	%esi, %edi
	movl	%edi, 16(%rax)
	movl	%edi, 4(%rax)
	movl	%edi, 36(%r14)
	movl	%edi, 16(%rcx)
	movl	%edi, 16(%rdx)
	movl	4(%rcx), %esi
	movl	%esi, 8(%rax)
	movl	%esi, 8(%rdx)
	movl	4(%rdx), %esi
	movl	%esi, 12(%rax)
	movl	%esi, 12(%rcx)
	cmpl	$0, 6380(%rcx)
	je	.LBB34_7
# BB#8:
	cmpl	$0, 6380(%rdx)
	setne	%sil
	jmp	.LBB34_9
.LBB34_7:
	xorl	%esi, %esi
.LBB34_9:
	movzbl	%sil, %esi
	movl	%esi, 6380(%rax)
	cmpl	$0, 6376(%rcx)
	je	.LBB34_10
# BB#11:
	xorl	%esi, %esi
	cmpl	$0, 6376(%rdx)
	setne	%sil
	movl	%esi, 6376(%rax)
	je	.LBB34_13
# BB#12:
	movl	28(%r14), %esi
	movl	%esi, 6372(%rax)
	jmp	.LBB34_13
.LBB34_10:                              # %.thread
	movl	$0, 6376(%rax)
.LBB34_13:
	movq	%rcx, 6536(%rax)
	movq	%rdx, 6544(%rax)
	movl	$0, 6428(%rax)
	movl	6560(%rcx), %esi
	movl	%esi, 6560(%rax)
	movl	6568(%rcx), %esi
	movl	%esi, 6568(%rax)
	testl	%esi, %esi
	je	.LBB34_15
# BB#14:
	movups	6572(%rcx), %xmm0
	movups	%xmm0, 6572(%rax)
.LBB34_15:
	movq	%rax, 6552(%rdx)
	movq	%rax, 6552(%rcx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end34:
	.size	dpb_combine_field_yuv, .Lfunc_end34-dpb_combine_field_yuv
	.cfi_endproc

	.globl	dpb_combine_field
	.p2align	4, 0x90
	.type	dpb_combine_field,@function
dpb_combine_field:                      # @dpb_combine_field
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi130:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi131:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi132:
	.cfi_def_cfa_offset 32
.Lcfi133:
	.cfi_offset %rbx, -32
.Lcfi134:
	.cfi_offset %r14, -24
.Lcfi135:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	callq	dpb_combine_field_yuv
	movq	40(%r14), %rdi
	callq	UnifiedOneForthPix
	movl	listXsize+4(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB35_3
# BB#1:                                 # %.lr.ph179
	movq	40(%r14), %r8
	leal	1(%rcx), %edx
	shrl	$31, %edx
	leal	1(%rcx,%rdx), %ecx
	sarl	%ecx
	movslq	%ecx, %r9
	movl	$288, %edx              # imm = 0x120
	movq	48(%r14), %rsi
	addq	%rdx, %rsi
	addq	56(%r14), %rdx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB35_2:                               # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rax
	movq	%rax, %rbx
	shrq	$63, %rbx
	addq	%rax, %rbx
	andq	$-2, %rbx
	movq	(%rdx), %rcx
	movq	%rcx, %rax
	shrq	$63, %rax
	addq	%rcx, %rax
	andq	$-2, %rax
	cmpq	%rax, %rbx
	cmovleq	%rbx, %rax
	movq	%rax, 288(%r8,%rdi,8)
	incq	%rdi
	addq	$16, %rsi
	addq	$16, %rdx
	cmpq	%r9, %rdi
	jl	.LBB35_2
.LBB35_3:                               # %.preheader170
	movl	listXsize(%rip), %edx
	movq	48(%r14), %rdi
	testl	%edx, %edx
	jle	.LBB35_6
# BB#4:                                 # %.lr.ph176
	movq	40(%r14), %r8
	movq	56(%r14), %r10
	leal	1(%rdx), %esi
	shrl	$31, %esi
	leal	1(%rdx,%rsi), %edx
	sarl	%edx
	movslq	%edx, %r9
	leaq	24(%rdi), %rsi
	addq	$24, %r10
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB35_5:                               # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	andq	$-2, %rax
	movq	(%r10), %rcx
	movq	%rcx, %rdx
	shrq	$63, %rdx
	addq	%rcx, %rdx
	andq	$-2, %rdx
	cmpq	%rdx, %rax
	cmovleq	%rax, %rdx
	movq	%rdx, 24(%r8,%rbx,8)
	incq	%rbx
	addq	$16, %rsi
	addq	$16, %r10
	cmpq	%r9, %rbx
	jl	.LBB35_5
.LBB35_6:                               # %.preheader
	cmpl	$4, 6396(%rdi)
	jl	.LBB35_34
# BB#7:                                 # %.lr.ph173
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB35_8:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB35_10 Depth 2
	cmpl	$4, 6392(%rdi)
	jl	.LBB35_33
# BB#9:                                 # %.lr.ph
                                        #   in Loop: Header=BB35_8 Depth=1
	movl	%r10d, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%r10d, %eax
	movl	%eax, %ecx
	andl	$2147483644, %ecx       # imm = 0x7FFFFFFC
	andl	$-4, %eax
	movl	%r10d, %edx
	subl	%eax, %edx
	leal	(%rdx,%rcx,2), %eax
	leal	4(%rax), %ecx
	movslq	%ecx, %r8
	movslq	%eax, %r9
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB35_10:                              #   Parent Loop BB35_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	40(%r14), %rax
	movq	6528(%rax), %rax
	movq	(%rax,%r8,8), %rax
	movb	$1, (%rax,%rsi)
	movq	40(%r14), %rax
	movq	6528(%rax), %rax
	movq	(%rax,%r9,8), %rax
	movb	$1, (%rax,%rsi)
	movq	40(%r14), %r11
	movq	48(%r14), %r15
	movq	6512(%r15), %rdx
	movq	(%rdx), %rdi
	movq	(%rdi,%r10,8), %rdi
	movq	(%rdi,%rsi,8), %rdi
	movzwl	(%rdi), %ebx
	movq	6512(%r11), %rax
	movq	(%rax), %rcx
	movq	(%rcx,%r9,8), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movw	%bx, (%rcx)
	movzwl	2(%rdi), %edi
	movw	%di, 2(%rcx)
	movq	8(%rdx), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movzwl	(%rcx), %edx
	movq	8(%rax), %rax
	movq	(%rax,%r9,8), %rax
	movq	(%rax,%rsi,8), %rax
	movw	%dx, (%rax)
	movzwl	2(%rcx), %ecx
	movw	%cx, 2(%rax)
	movq	6488(%r15), %rax
	movq	(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movsbq	(%rax,%rsi), %rcx
	testq	%rcx, %rcx
	movq	6488(%r11), %rax
	movq	(%rax), %rax
	movq	(%rax,%r9,8), %rax
	movb	%cl, (%rax,%rsi)
	movq	40(%r14), %rax
	movq	48(%r14), %rdx
	movq	6488(%rdx), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%r10,8), %rdx
	movsbq	(%rdx,%rsi), %rdi
	movq	6488(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r9,8), %rax
	movb	%dil, (%rax,%rsi)
	movq	48(%r14), %rdx
	js	.LBB35_11
# BB#12:                                #   in Loop: Header=BB35_10 Depth=2
	movq	24(%rdx,%rcx,8), %r11
	jmp	.LBB35_13
	.p2align	4, 0x90
.LBB35_11:                              #   in Loop: Header=BB35_10 Depth=2
	xorl	%r11d, %r11d
.LBB35_13:                              # %._crit_edge186
                                        #   in Loop: Header=BB35_10 Depth=2
	movq	6504(%rdx), %rbx
	movq	(%rbx), %rax
	movq	(%rax,%r10,8), %rax
	movq	%r11, (%rax,%rsi,8)
	testb	%dil, %dil
	js	.LBB35_14
# BB#15:                                #   in Loop: Header=BB35_10 Depth=2
	movq	288(%rdx,%rdi,8), %rax
	jmp	.LBB35_16
	.p2align	4, 0x90
.LBB35_14:                              #   in Loop: Header=BB35_10 Depth=2
	xorl	%eax, %eax
.LBB35_16:                              #   in Loop: Header=BB35_10 Depth=2
	movq	8(%rbx), %rbx
	movq	(%rbx,%r10,8), %rbx
	movq	%rax, (%rbx,%rsi,8)
	testb	%cl, %cl
	js	.LBB35_17
# BB#18:                                #   in Loop: Header=BB35_10 Depth=2
	movq	1608(%rdx,%rcx,8), %rax
	jmp	.LBB35_19
	.p2align	4, 0x90
.LBB35_17:                              #   in Loop: Header=BB35_10 Depth=2
	xorl	%eax, %eax
.LBB35_19:                              #   in Loop: Header=BB35_10 Depth=2
	movq	40(%r14), %r11
	movq	6504(%r11), %rcx
	movq	(%rcx), %rbx
	movq	(%rbx,%r9,8), %rbx
	movq	%rax, (%rbx,%rsi,8)
	testb	%dil, %dil
	js	.LBB35_20
# BB#21:                                #   in Loop: Header=BB35_10 Depth=2
	movq	1872(%rdx,%rdi,8), %rax
	jmp	.LBB35_22
	.p2align	4, 0x90
.LBB35_20:                              #   in Loop: Header=BB35_10 Depth=2
	xorl	%eax, %eax
.LBB35_22:                              #   in Loop: Header=BB35_10 Depth=2
	movq	8(%rcx), %rcx
	movq	(%rcx,%r9,8), %rcx
	movq	%rax, (%rcx,%rsi,8)
	movq	56(%r14), %r15
	movq	6512(%r15), %rcx
	movq	(%rcx), %rdx
	movq	(%rdx,%r10,8), %rdx
	movq	(%rdx,%rsi,8), %rdx
	movzwl	(%rdx), %edi
	movq	6512(%r11), %rbx
	movq	(%rbx), %rax
	movq	(%rax,%r8,8), %rax
	movq	(%rax,%rsi,8), %rax
	movw	%di, (%rax)
	movzwl	2(%rdx), %edx
	movw	%dx, 2(%rax)
	movq	8(%rcx), %rax
	movq	(%rax,%r10,8), %rax
	movq	(%rax,%rsi,8), %rax
	movzwl	(%rax), %ecx
	movq	8(%rbx), %rdx
	movq	(%rdx,%r8,8), %rdx
	movq	(%rdx,%rsi,8), %rdx
	movw	%cx, (%rdx)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rdx)
	movq	6488(%r15), %rax
	movq	(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movsbq	(%rax,%rsi), %rcx
	testq	%rcx, %rcx
	movq	6488(%r11), %rax
	movq	(%rax), %rax
	movq	(%rax,%r8,8), %rax
	movb	%cl, (%rax,%rsi)
	movq	40(%r14), %rax
	movq	56(%r14), %rdx
	movq	6488(%rdx), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%r10,8), %rdx
	movsbq	(%rdx,%rsi), %rdi
	movq	6488(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r8,8), %rax
	movb	%dil, (%rax,%rsi)
	movq	56(%r14), %rdx
	js	.LBB35_23
# BB#24:                                #   in Loop: Header=BB35_10 Depth=2
	movq	24(%rdx,%rcx,8), %r11
	jmp	.LBB35_25
	.p2align	4, 0x90
.LBB35_23:                              #   in Loop: Header=BB35_10 Depth=2
	xorl	%r11d, %r11d
.LBB35_25:                              # %._crit_edge188
                                        #   in Loop: Header=BB35_10 Depth=2
	movq	6504(%rdx), %rbx
	movq	(%rbx), %rax
	movq	(%rax,%r10,8), %rax
	movq	%r11, (%rax,%rsi,8)
	testb	%dil, %dil
	js	.LBB35_26
# BB#27:                                #   in Loop: Header=BB35_10 Depth=2
	movq	288(%rdx,%rdi,8), %rax
	jmp	.LBB35_28
	.p2align	4, 0x90
.LBB35_26:                              #   in Loop: Header=BB35_10 Depth=2
	xorl	%eax, %eax
.LBB35_28:                              #   in Loop: Header=BB35_10 Depth=2
	movq	8(%rbx), %rbx
	movq	(%rbx,%r10,8), %rbx
	movq	%rax, (%rbx,%rsi,8)
	movq	$-1, %r11
	testb	%cl, %cl
	movq	$-1, %rax
	js	.LBB35_30
# BB#29:                                #   in Loop: Header=BB35_10 Depth=2
	movq	1608(%rdx,%rcx,8), %rax
.LBB35_30:                              #   in Loop: Header=BB35_10 Depth=2
	movq	40(%r14), %rcx
	movq	6504(%rcx), %rcx
	movq	(%rcx), %rbx
	movq	(%rbx,%r8,8), %rbx
	movq	%rax, (%rbx,%rsi,8)
	testb	%dil, %dil
	js	.LBB35_32
# BB#31:                                #   in Loop: Header=BB35_10 Depth=2
	movq	1872(%rdx,%rdi,8), %r11
.LBB35_32:                              #   in Loop: Header=BB35_10 Depth=2
	movq	8(%rcx), %rax
	movq	(%rax,%r8,8), %rax
	movq	%r11, (%rax,%rsi,8)
	movq	48(%r14), %rax
	movq	6528(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movb	$1, (%rax,%rsi)
	movq	56(%r14), %rax
	movq	6528(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movb	$1, (%rax,%rsi)
	incq	%rsi
	movq	48(%r14), %rdi
	movl	6392(%rdi), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%eax, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rax
	cmpq	%rax, %rsi
	jl	.LBB35_10
.LBB35_33:                              # %._crit_edge
                                        #   in Loop: Header=BB35_8 Depth=1
	incq	%r10
	movl	6396(%rdi), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%eax, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rax
	cmpq	%rax, %r10
	jl	.LBB35_8
.LBB35_34:                              # %._crit_edge174
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end35:
	.size	dpb_combine_field, .Lfunc_end35-dpb_combine_field
	.cfi_endproc

	.globl	alloc_ref_pic_list_reordering_buffer
	.p2align	4, 0x90
	.type	alloc_ref_pic_list_reordering_buffer,@function
alloc_ref_pic_list_reordering_buffer:   # @alloc_ref_pic_list_reordering_buffer
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi136:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi137:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi138:
	.cfi_def_cfa_offset 32
.Lcfi139:
	.cfi_offset %rbx, -32
.Lcfi140:
	.cfi_offset %r14, -24
.Lcfi141:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	img(%rip), %rax
	movl	20(%rax), %ecx
	cmpl	$2, %ecx
	je	.LBB36_8
# BB#1:
	cmpl	$4, %ecx
	jne	.LBB36_2
.LBB36_8:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 56(%r15)
	movq	$0, 72(%r15)
	jmp	.LBB36_9
.LBB36_2:
	movslq	14456(%rax), %r14
	incq	%r14
	movl	$4, %esi
	movq	%r14, %rdi
	callq	calloc
	movq	%rax, 56(%r15)
	testq	%rax, %rax
	jne	.LBB36_4
# BB#3:
	movl	$.L.str.15, %edi
	callq	no_mem_exit
.LBB36_4:
	movl	$4, %esi
	movq	%r14, %rdi
	callq	calloc
	movq	%rax, 64(%r15)
	testq	%rax, %rax
	jne	.LBB36_6
# BB#5:
	movl	$.L.str.16, %edi
	callq	no_mem_exit
.LBB36_6:
	movl	$4, %esi
	movq	%r14, %rdi
	callq	calloc
	movq	%rax, 72(%r15)
	testq	%rax, %rax
	jne	.LBB36_9
# BB#7:
	movl	$.L.str.17, %edi
	callq	no_mem_exit
.LBB36_9:
	movq	img(%rip), %rax
	cmpl	$1, 20(%rax)
	jne	.LBB36_16
# BB#10:
	movslq	14460(%rax), %rbx
	incq	%rbx
	movl	$4, %esi
	movq	%rbx, %rdi
	callq	calloc
	movq	%rax, 88(%r15)
	testq	%rax, %rax
	jne	.LBB36_12
# BB#11:
	movl	$.L.str.18, %edi
	callq	no_mem_exit
.LBB36_12:
	movl	$4, %esi
	movq	%rbx, %rdi
	callq	calloc
	movq	%rax, 96(%r15)
	testq	%rax, %rax
	jne	.LBB36_14
# BB#13:
	movl	$.L.str.19, %edi
	callq	no_mem_exit
.LBB36_14:
	movl	$4, %esi
	movq	%rbx, %rdi
	callq	calloc
	movq	%rax, 104(%r15)
	testq	%rax, %rax
	jne	.LBB36_17
# BB#15:
	movl	$.L.str.20, %edi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	no_mem_exit             # TAILCALL
.LBB36_16:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 88(%r15)
	movq	$0, 104(%r15)
.LBB36_17:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end36:
	.size	alloc_ref_pic_list_reordering_buffer, .Lfunc_end36-alloc_ref_pic_list_reordering_buffer
	.cfi_endproc

	.globl	free_ref_pic_list_reordering_buffer
	.p2align	4, 0x90
	.type	free_ref_pic_list_reordering_buffer,@function
free_ref_pic_list_reordering_buffer:    # @free_ref_pic_list_reordering_buffer
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi142:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi143:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi144:
	.cfi_def_cfa_offset 32
.Lcfi145:
	.cfi_offset %rbx, -24
.Lcfi146:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB37_2
# BB#1:
	callq	free
.LBB37_2:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB37_4
# BB#3:
	callq	free
.LBB37_4:
	leaq	56(%rbx), %r14
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB37_6
# BB#5:
	callq	free
.LBB37_6:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movq	$0, 16(%r14)
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB37_8
# BB#7:
	callq	free
.LBB37_8:
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB37_10
# BB#9:
	callq	free
.LBB37_10:
	movq	104(%rbx), %rdi
	addq	$88, %rbx
	testq	%rdi, %rdi
	je	.LBB37_12
# BB#11:
	callq	free
.LBB37_12:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movq	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end37:
	.size	free_ref_pic_list_reordering_buffer, .Lfunc_end37-free_ref_pic_list_reordering_buffer
	.cfi_endproc

	.globl	fill_frame_num_gap
	.p2align	4, 0x90
	.type	fill_frame_num_gap,@function
fill_frame_num_gap:                     # @fill_frame_num_gap
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi147:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi148:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi149:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi150:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi151:
	.cfi_def_cfa_offset 48
.Lcfi152:
	.cfi_offset %rbx, -48
.Lcfi153:
	.cfi_offset %r12, -40
.Lcfi154:
	.cfi_offset %r14, -32
.Lcfi155:
	.cfi_offset %r15, -24
.Lcfi156:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	log2_max_frame_num_minus4(%rip), %ecx
	addl	$4, %ecx
	movl	$1, %r14d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r14d
	movl	15360(%rbx), %r15d
	movl	$1, 15360(%rbx)
	movl	15332(%rbx), %r12d
	movl	15432(%rbx), %eax
	incl	%eax
	jmp	.LBB38_2
	.p2align	4, 0x90
.LBB38_1:                               #   in Loop: Header=BB38_2 Depth=1
	movl	68(%rbx), %edx
	movl	52(%rbx), %esi
	movl	64(%rbx), %ecx
	movl	80(%rbx), %r8d
	xorl	%edi, %edi
	callq	alloc_storable_picture
	movl	$1, 6428(%rax)
	movl	%ebp, 6364(%rax)
	movl	$1, 6388(%rax)
	movl	$1, 6384(%rax)
	movl	$0, 15364(%rbx)
	movq	%rax, %rdi
	callq	store_picture_in_dpb
	incl	%ebp
	movl	%ebp, %eax
.LBB38_2:                               # =>This Inner Loop Header: Depth=1
	cltd
	idivl	%r14d
	movl	%edx, %ebp
	cmpl	%ebp, %r12d
	jne	.LBB38_1
# BB#3:                                 # %._crit_edge
	movl	%r15d, 15360(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end38:
	.size	fill_frame_num_gap, .Lfunc_end38-fill_frame_num_gap
	.cfi_endproc

	.globl	alloc_colocated
	.p2align	4, 0x90
	.type	alloc_colocated,@function
alloc_colocated:                        # @alloc_colocated
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi157:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi158:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi159:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi160:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi161:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi162:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi163:
	.cfi_def_cfa_offset 64
.Lcfi164:
	.cfi_offset %rbx, -56
.Lcfi165:
	.cfi_offset %r12, -48
.Lcfi166:
	.cfi_offset %r13, -40
.Lcfi167:
	.cfi_offset %r14, -32
.Lcfi168:
	.cfi_offset %r15, -24
.Lcfi169:
	.cfi_offset %rbp, -16
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movl	%esi, %r14d
	movl	%edi, %r13d
	movl	$1, %edi
	movl	$4880, %esi             # imm = 0x1310
	callq	calloc
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.LBB39_2
# BB#1:
	movl	$.L.str.21, %edi
	callq	no_mem_exit
.LBB39_2:
	movl	%r13d, 4(%r12)
	movl	%r14d, 8(%r12)
	leaq	1600(%r12), %rdi
	movl	%r14d, %r15d
	sarl	$31, %r15d
	movl	%r15d, %ebx
	shrl	$30, %ebx
	addl	%r14d, %ebx
	sarl	$2, %ebx
	movl	%r13d, %ebp
	sarl	$31, %ebp
	shrl	$30, %ebp
	addl	%r13d, %ebp
	sarl	$2, %ebp
	movl	$2, %esi
	movl	%ebx, %edx
	movl	%ebp, %ecx
	callq	get_mem3D
	leaq	1608(%r12), %rdi
	movl	$2, %esi
	movl	%ebx, %edx
	movl	%ebp, %ecx
	callq	get_mem3Dint64
	leaq	1616(%r12), %rdi
	movl	$2, %esi
	movl	$2, %r8d
	movl	%ebx, %edx
	movl	%ebp, %ecx
	callq	get_mem4Dshort
	leaq	1624(%r12), %rdi
	movl	%ebx, %esi
	movl	%ebp, %edx
	callq	get_mem2D
	leaq	4872(%r12), %rdi
	movl	%ebx, %esi
	movl	%ebp, %edx
	callq	get_mem2D
	movl	4(%rsp), %eax           # 4-byte Reload
	testl	%eax, %eax
	movl	%eax, %ebx
	je	.LBB39_4
# BB#3:
	leaq	3216(%r12), %rdi
	shrl	$29, %r15d
	addl	%r15d, %r14d
	sarl	$3, %r14d
	movl	$2, %esi
	movl	%r14d, %edx
	movl	%ebp, %ecx
	callq	get_mem3D
	leaq	3224(%r12), %rdi
	movl	$2, %esi
	movl	%r14d, %edx
	movl	%ebp, %ecx
	callq	get_mem3Dint64
	leaq	3232(%r12), %rdi
	movl	$2, %esi
	movl	$2, %r8d
	movl	%r14d, %edx
	movl	%ebp, %ecx
	callq	get_mem4Dshort
	leaq	3240(%r12), %rdi
	movl	%r14d, %esi
	movl	%ebp, %edx
	callq	get_mem2D
	leaq	4832(%r12), %rdi
	movl	$2, %esi
	movl	%r14d, %edx
	movl	%ebp, %ecx
	callq	get_mem3D
	leaq	4840(%r12), %rdi
	movl	$2, %esi
	movl	%r14d, %edx
	movl	%ebp, %ecx
	callq	get_mem3Dint64
	leaq	4848(%r12), %rdi
	movl	$2, %esi
	movl	$2, %r8d
	movl	%r14d, %edx
	movl	%ebp, %ecx
	callq	get_mem4Dshort
	movq	%r12, %rdi
	addq	$4856, %rdi             # imm = 0x12F8
	movl	%r14d, %esi
	movl	%ebp, %edx
	callq	get_mem2D
.LBB39_4:
	movl	%ebx, (%r12)
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end39:
	.size	alloc_colocated, .Lfunc_end39-alloc_colocated
	.cfi_endproc

	.globl	free_colocated
	.p2align	4, 0x90
	.type	free_colocated,@function
free_colocated:                         # @free_colocated
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi170:
	.cfi_def_cfa_offset 16
.Lcfi171:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB40_11
# BB#1:
	movq	1600(%rbx), %rdi
	movl	$2, %esi
	callq	free_mem3D
	movq	1608(%rbx), %rdi
	movl	$2, %esi
	callq	free_mem3Dint64
	movq	1616(%rbx), %rdi
	movl	8(%rbx), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$30, %edx
	addl	%eax, %edx
	sarl	$2, %edx
	movl	$2, %esi
	callq	free_mem4Dshort
	movq	1624(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB40_3
# BB#2:
	callq	free_mem2D
	movq	$0, 1624(%rbx)
.LBB40_3:
	movq	4872(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB40_5
# BB#4:
	callq	free_mem2D
	movq	$0, 4872(%rbx)
.LBB40_5:
	cmpl	$0, (%rbx)
	je	.LBB40_10
# BB#6:
	movq	3216(%rbx), %rdi
	movl	$2, %esi
	callq	free_mem3D
	movq	3224(%rbx), %rdi
	movl	$2, %esi
	callq	free_mem3Dint64
	movq	3232(%rbx), %rdi
	movl	8(%rbx), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%eax, %edx
	sarl	$3, %edx
	movl	$2, %esi
	callq	free_mem4Dshort
	movq	3240(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB40_8
# BB#7:
	callq	free_mem2D
	movq	$0, 3240(%rbx)
.LBB40_8:
	movq	4832(%rbx), %rdi
	movl	$2, %esi
	callq	free_mem3D
	movq	4840(%rbx), %rdi
	movl	$2, %esi
	callq	free_mem3Dint64
	movq	4848(%rbx), %rdi
	movl	8(%rbx), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%eax, %edx
	sarl	$3, %edx
	movl	$2, %esi
	callq	free_mem4Dshort
	movq	4856(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB40_10
# BB#9:
	callq	free_mem2D
.LBB40_10:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB40_11:
	popq	%rbx
	retq
.Lfunc_end40:
	.size	free_colocated, .Lfunc_end40-free_colocated
	.cfi_endproc

	.globl	compute_colocated
	.p2align	4, 0x90
	.type	compute_colocated,@function
compute_colocated:                      # @compute_colocated
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi172:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi173:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi174:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi175:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi176:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi177:
	.cfi_def_cfa_offset 56
.Lcfi178:
	.cfi_offset %rbx, -56
.Lcfi179:
	.cfi_offset %r12, -48
.Lcfi180:
	.cfi_offset %r13, -40
.Lcfi181:
	.cfi_offset %r14, -32
.Lcfi182:
	.cfi_offset %r15, -24
.Lcfi183:
	.cfi_offset %rbp, -16
	movq	8(%rsi), %rax
	movq	(%rax), %r14
	movq	img(%rip), %rax
	cmpl	$0, 15268(%rax)
	movq	%rsi, -56(%rsp)         # 8-byte Spill
	je	.LBB41_2
# BB#1:
	movq	24(%rsi), %rcx
	movq	40(%rsi), %rdx
	movq	(%rcx), %r15
	movq	(%rdx), %r12
	jmp	.LBB41_8
.LBB41_2:
	movl	24(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB41_7
# BB#3:
	cmpl	(%r14), %ecx
	je	.LBB41_7
# BB#4:
	cmpl	$0, 6428(%r14)
	je	.LBB41_7
# BB#5:
	cmpl	$1, %ecx
	jne	.LBB41_172
# BB#6:
	movq	6536(%r14), %r12
	jmp	.LBB41_173
.LBB41_7:
	movq	%r14, %r12
	movq	%r14, %r15
.LBB41_8:
	movq	active_sps(%rip), %rcx
	cmpl	$0, 1148(%rcx)
	je	.LBB41_10
# BB#9:
	cmpl	$0, 1156(%rcx)
	je	.LBB41_21
.LBB41_10:                              # %.preheader895
	movl	6396(%r14), %edx
	cmpl	$4, %edx
	jl	.LBB41_21
# BB#11:                                # %.lr.ph937
	leaq	6512(%r14), %rax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	movq	%r15, %rax
	leaq	6488(%r14), %r15
	leaq	6512(%rax), %rcx
	movq	%rcx, -24(%rsp)         # 8-byte Spill
	movq	%rax, -72(%rsp)         # 8-byte Spill
	leaq	6488(%rax), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	leaq	6512(%r12), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	movq	%r12, -64(%rsp)         # 8-byte Spill
	leaq	6488(%r12), %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	movl	6392(%r14), %ecx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB41_12:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB41_14 Depth 2
	cmpl	$4, %ecx
	jl	.LBB41_19
# BB#13:                                # %.lr.ph934
                                        #   in Loop: Header=BB41_12 Depth=1
	movl	%r12d, %eax
	shrl	$31, %eax
	addl	%r12d, %eax
	sarl	%eax
	movl	%r12d, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%r12d, %ecx
	sarl	$3, %ecx
	leal	(%rax,%rcx,4), %ecx
	cltq
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	leal	4(%rcx), %eax
	movl	%eax, -76(%rsp)         # 4-byte Spill
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB41_14:                              #   Parent Loop BB41_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	img(%rip), %rax
	cmpl	$0, 15268(%rax)
	movq	-88(%rsp), %rax         # 8-byte Reload
	movq	%r12, %rbp
	movq	%r15, %rcx
	movq	%r12, %r13
	movq	%r14, %r11
	je	.LBB41_17
# BB#15:                                #   in Loop: Header=BB41_14 Depth=2
	movq	6528(%r14), %rax
	movq	(%rax,%r12,8), %rax
	cmpb	$0, (%rax,%r10)
	movq	-88(%rsp), %rax         # 8-byte Reload
	movq	%r12, %rbp
	movq	%r15, %rcx
	movq	%r12, %r13
	movq	%r14, %r11
	je	.LBB41_17
# BB#16:                                #   in Loop: Header=BB41_14 Depth=2
	movq	enc_picture(%rip), %rax
	movl	4(%rax), %eax
	movl	%eax, %ecx
	movq	-64(%rsp), %r11         # 8-byte Reload
	subl	4(%r11), %ecx
	movl	%ecx, %esi
	negl	%esi
	cmovll	%ecx, %esi
	movq	-72(%rsp), %rbp         # 8-byte Reload
	subl	4(%rbp), %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%ecx, %esi
	movq	-40(%rsp), %rax         # 8-byte Reload
	cmovgq	-24(%rsp), %rax         # 8-byte Folded Reload
	movq	-48(%rsp), %rcx         # 8-byte Reload
	cmovgq	-32(%rsp), %rcx         # 8-byte Folded Reload
	movl	-76(%rsp), %esi         # 4-byte Reload
	cmovgl	-8(%rsp), %esi          # 4-byte Folded Reload
	movslq	%esi, %r13
	cmovgq	%rbp, %r11
	movq	-16(%rsp), %rbp         # 8-byte Reload
.LBB41_17:                              #   in Loop: Header=BB41_14 Depth=2
	movq	(%rax), %rax
	movq	(%rax), %rdx
	movq	(%rdx,%rbp,8), %rdx
	movq	(%rdx,%r10,8), %rbx
	movzwl	(%rbx), %ebx
	movq	1616(%rdi), %r8
	movq	(%r8), %r9
	movq	(%r9,%r12,8), %rsi
	movq	(%rsi,%r10,8), %rsi
	movw	%bx, (%rsi)
	movq	(%rdx,%r10,8), %rdx
	movzwl	2(%rdx), %edx
	movw	%dx, 2(%rsi)
	movq	8(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax,%r10,8), %rax
	movzwl	(%rax), %edx
	movq	8(%r8), %rsi
	movq	(%rsi,%r12,8), %rsi
	movq	(%rsi,%r10,8), %rsi
	movw	%dx, (%rsi)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rsi)
	movq	(%rcx), %rax
	movq	(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movzbl	(%rax,%r10), %eax
	movq	1600(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r12,8), %rdx
	movb	%al, (%rdx,%r10)
	movq	(%rcx), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movzbl	(%rax,%r10), %eax
	movq	1600(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r12,8), %rcx
	movb	%al, (%rcx,%r10)
	movq	6504(%r14), %rax
	movq	(%rax), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	1608(%rdi), %rdx
	movq	(%rdx), %rsi
	movq	(%rsi,%r12,8), %rsi
	movq	%rcx, (%rsi,%r10,8)
	movq	8(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	(%rax,%r10,8), %rax
	movq	8(%rdx), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	%rax, (%rcx,%r10,8)
	movzbl	6376(%r11), %eax
	movb	%al, 4864(%rdi)
	incq	%r10
	movl	6392(%r14), %ecx
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%ecx, %eax
	sarl	$2, %eax
	cltq
	cmpq	%rax, %r10
	jl	.LBB41_14
# BB#18:                                # %._crit_edge935.loopexit
                                        #   in Loop: Header=BB41_12 Depth=1
	movl	6396(%r14), %edx
.LBB41_19:                              # %._crit_edge935
                                        #   in Loop: Header=BB41_12 Depth=1
	incq	%r12
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%edx, %eax
	sarl	$2, %eax
	cltq
	cmpq	%rax, %r12
	jl	.LBB41_12
# BB#20:                                # %.loopexit896.loopexit
	movq	img(%rip), %rax
	movq	-64(%rsp), %r12         # 8-byte Reload
	movq	-72(%rsp), %r15         # 8-byte Reload
.LBB41_21:                              # %.loopexit896
	cmpl	$0, 24(%rax)
	jne	.LBB41_23
# BB#22:
	cmpl	$0, 15268(%rax)
	je	.LBB41_81
.LBB41_23:                              # %.preheader893
	movl	6396(%r14), %eax
	cmpl	$8, %eax
	jl	.LBB41_81
# BB#24:                                # %.lr.ph930
	movl	6392(%r14), %esi
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB41_25:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB41_27 Depth 2
	cmpl	$4, %esi
	jl	.LBB41_80
# BB#26:                                # %.lr.ph927
                                        #   in Loop: Header=BB41_25 Depth=1
	movl	%r8d, %eax
	orl	$1, %eax
	movl	%r8d, %edx
	andl	$-2, %edx
	testb	$2, %r8b
	cmovneq	%rax, %rdx
	movslq	%edx, %r11
	leal	(%r8,%r8), %eax
	movslq	%eax, %r10
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB41_27:                              #   Parent Loop BB41_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %edx
	orl	$1, %edx
	movl	%eax, %esi
	andl	$-2, %esi
	testb	$2, %al
	cmovnel	%edx, %esi
	movq	img(%rip), %rdx
	cmpl	$0, 15268(%rdx)
	je	.LBB41_34
# BB#28:                                #   in Loop: Header=BB41_27 Depth=2
	movq	6512(%r12), %rcx
	movq	(%rcx), %rdx
	movq	(%rdx,%r11,8), %rdx
	movslq	%esi, %r9
	movq	(%rdx,%r9,8), %rdx
	movzwl	(%rdx), %esi
	movq	4848(%rdi), %rbp
	movq	(%rbp), %rbx
	movq	(%rbx,%r8,8), %rbx
	movq	(%rbx,%rax,8), %rbx
	movw	%si, (%rbx)
	movzwl	2(%rdx), %edx
	movw	%dx, 2(%rbx)
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movzwl	(%rcx), %edx
	movq	8(%rbp), %rsi
	movq	(%rsi,%r8,8), %rsi
	movq	(%rsi,%rax,8), %rsi
	movw	%dx, (%rsi)
	movzwl	2(%rcx), %ecx
	movw	%cx, 2(%rsi)
	movq	6488(%r12), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movzbl	(%rcx,%r9), %ecx
	movq	4832(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r8,8), %rdx
	movb	%cl, (%rdx,%rax)
	movq	6488(%r12), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movzbl	(%rcx,%r9), %ecx
	movq	4832(%rdi), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%r8,8), %rdx
	movb	%cl, (%rdx,%rax)
	movq	6504(%r12), %rcx
	movq	(%rcx), %rdx
	movq	(%rdx,%r11,8), %rdx
	movq	(%rdx,%r9,8), %rdx
	movq	4840(%rdi), %rsi
	movq	(%rsi), %rbx
	movq	(%rbx,%r8,8), %rbx
	movq	%rdx, (%rbx,%rax,8)
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movq	8(%rsi), %rdx
	movq	(%rdx,%r8,8), %rdx
	movq	%rcx, (%rdx,%rax,8)
	movq	img(%rip), %rcx
	cmpl	$1, 14452(%rcx)
	jne	.LBB41_59
# BB#29:                                #   in Loop: Header=BB41_27 Depth=2
	movq	4832(%rdi), %rsi
	movq	(%rsi), %rcx
	movq	(%rcx,%r8,8), %rcx
	movzbl	(%rcx,%rax), %edx
	cmpl	$0, 6376(%r12)
	jne	.LBB41_36
# BB#30:                                #   in Loop: Header=BB41_27 Depth=2
	testb	%dl, %dl
	jne	.LBB41_36
# BB#31:                                #   in Loop: Header=BB41_27 Depth=2
	movq	4848(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	(%rcx,%rax,8), %rsi
	movswl	(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$1, %edx
	ja	.LBB41_57
# BB#32:                                #   in Loop: Header=BB41_27 Depth=2
	movswl	2(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	movb	$1, %sil
	cmpl	$2, %edx
	jb	.LBB41_58
	jmp	.LBB41_57
	.p2align	4, 0x90
.LBB41_34:                              #   in Loop: Header=BB41_27 Depth=2
	movq	6512(%r14), %rdx
	movq	(%rdx), %rbp
	movq	(%rbp,%r11,8), %rbp
	movslq	%esi, %rsi
	movq	(%rbp,%rsi,8), %rbp
	movzwl	(%rbp), %r9d
	movq	1616(%rdi), %rcx
	movq	(%rcx), %rbx
	movq	(%rbx,%r8,8), %rbx
	movq	(%rbx,%rax,8), %rbx
	movw	%r9w, (%rbx)
	movzwl	2(%rbp), %ebp
	movw	%bp, 2(%rbx)
	movq	8(%rdx), %rdx
	movq	(%rdx,%r11,8), %rdx
	movq	(%rdx,%rsi,8), %rdx
	movzwl	(%rdx), %ebp
	movq	8(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	(%rcx,%rax,8), %rcx
	movw	%bp, (%rcx)
	movzwl	2(%rdx), %edx
	movw	%dx, 2(%rcx)
	movq	6488(%r14), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movzbl	(%rcx,%rsi), %edx
	movq	1600(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r8,8), %rbx
	cmpb	$-1, %dl
	je	.LBB41_38
# BB#35:                                #   in Loop: Header=BB41_27 Depth=2
	movb	%dl, (%rbx,%rax)
	movq	6504(%r14), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%rsi,8), %rbp
	jmp	.LBB41_39
.LBB41_36:                              # %._crit_edge1203
                                        #   in Loop: Header=BB41_27 Depth=2
	cmpb	$-1, %dl
	jne	.LBB41_57
# BB#49:                                #   in Loop: Header=BB41_27 Depth=2
	movq	8(%rsi), %rcx
	movq	(%rcx,%r8,8), %rcx
	cmpb	$0, (%rcx,%rax)
	jne	.LBB41_57
# BB#53:                                #   in Loop: Header=BB41_27 Depth=2
	movq	4848(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	(%rcx,%rax,8), %rdx
	movswl	(%rdx), %ecx
	movl	%ecx, %esi
	negl	%esi
	cmovll	%ecx, %esi
	cmpl	$1, %esi
	ja	.LBB41_57
# BB#54:                                #   in Loop: Header=BB41_27 Depth=2
	movswl	2(%rdx), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$2, %edx
	setb	%sil
	jmp	.LBB41_58
	.p2align	4, 0x90
.LBB41_57:                              #   in Loop: Header=BB41_27 Depth=2
	xorl	%esi, %esi
.LBB41_58:                              #   in Loop: Header=BB41_27 Depth=2
	xorb	$1, %sil
	movq	4856(%rdi), %rcx
	movq	(%rcx,%r8,8), %rcx
	movb	%sil, (%rcx,%rax)
.LBB41_59:                              #   in Loop: Header=BB41_27 Depth=2
	movq	6512(%r15), %rcx
	movq	(%rcx), %rdx
	movq	(%rdx,%r11,8), %rdx
	movq	(%rdx,%r9,8), %rdx
	movzwl	(%rdx), %esi
	movq	3232(%rdi), %rbx
	movq	(%rbx), %rbp
	movq	(%rbp,%r8,8), %rbp
	movq	(%rbp,%rax,8), %rbp
	movw	%si, (%rbp)
	movzwl	2(%rdx), %edx
	movw	%dx, 2(%rbp)
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movzwl	(%rcx), %edx
	movq	8(%rbx), %rsi
	movq	(%rsi,%r8,8), %rsi
	movq	(%rsi,%rax,8), %rsi
	movw	%dx, (%rsi)
	movzwl	2(%rcx), %ecx
	movw	%cx, 2(%rsi)
	movq	6488(%r15), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movzbl	(%rcx,%r9), %ecx
	movq	3216(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r8,8), %rdx
	movb	%cl, (%rdx,%rax)
	movq	6488(%r15), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movzbl	(%rcx,%r9), %ecx
	movq	3216(%rdi), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%r8,8), %rdx
	movb	%cl, (%rdx,%rax)
	movq	6504(%r15), %rcx
	movq	(%rcx), %rdx
	movq	(%rdx,%r11,8), %rdx
	movq	(%rdx,%r9,8), %rdx
	movq	3224(%rdi), %rsi
	movq	(%rsi), %rbx
	movq	(%rbx,%r8,8), %rbx
	movq	%rdx, (%rbx,%rax,8)
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movq	8(%rsi), %rdx
	movq	(%rdx,%r8,8), %rdx
	movq	%rcx, (%rdx,%rax,8)
	movq	img(%rip), %rcx
	movl	14452(%rcx), %edx
	cmpl	$1, %edx
	jne	.LBB41_73
# BB#60:                                #   in Loop: Header=BB41_27 Depth=2
	movq	3216(%rdi), %rsi
	movq	(%rsi), %rcx
	movq	(%rcx,%r8,8), %rcx
	movzbl	(%rcx,%rax), %edx
	cmpl	$0, 6376(%r15)
	jne	.LBB41_65
# BB#61:                                #   in Loop: Header=BB41_27 Depth=2
	testb	%dl, %dl
	jne	.LBB41_65
# BB#62:                                #   in Loop: Header=BB41_27 Depth=2
	movq	3232(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	(%rcx,%rax,8), %rsi
	movswl	(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$1, %edx
	ja	.LBB41_71
# BB#63:                                #   in Loop: Header=BB41_27 Depth=2
	movswl	2(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	movb	$1, %sil
	cmpl	$2, %edx
	jb	.LBB41_72
	jmp	.LBB41_71
.LBB41_65:                              # %._crit_edge1209
                                        #   in Loop: Header=BB41_27 Depth=2
	cmpb	$-1, %dl
	jne	.LBB41_71
# BB#67:                                #   in Loop: Header=BB41_27 Depth=2
	movq	8(%rsi), %rcx
	movq	(%rcx,%r8,8), %rcx
	cmpb	$0, (%rcx,%rax)
	jne	.LBB41_71
# BB#69:                                #   in Loop: Header=BB41_27 Depth=2
	movq	3232(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	(%rcx,%rax,8), %rdx
	movswl	(%rdx), %ecx
	movl	%ecx, %esi
	negl	%esi
	cmovll	%ecx, %esi
	cmpl	$1, %esi
	ja	.LBB41_71
# BB#70:                                #   in Loop: Header=BB41_27 Depth=2
	movswl	2(%rdx), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$2, %edx
	setb	%sil
	jmp	.LBB41_72
	.p2align	4, 0x90
.LBB41_71:                              #   in Loop: Header=BB41_27 Depth=2
	xorl	%esi, %esi
.LBB41_72:                              #   in Loop: Header=BB41_27 Depth=2
	xorb	$1, %sil
	movq	3240(%rdi), %rcx
	movq	(%rcx,%r8,8), %rcx
	movb	%sil, (%rcx,%rax)
	movq	img(%rip), %rcx
	movl	14452(%rcx), %edx
.LBB41_73:                              #   in Loop: Header=BB41_27 Depth=2
	testl	%edx, %edx
	jne	.LBB41_78
# BB#74:                                #   in Loop: Header=BB41_27 Depth=2
	movq	6528(%r14), %rcx
	movq	(%rcx,%r10,8), %rcx
	cmpb	$0, (%rcx,%rax)
	jne	.LBB41_78
# BB#75:                                #   in Loop: Header=BB41_27 Depth=2
	movq	3232(%rdi), %rcx
	movq	(%rcx), %rdx
	movq	(%rdx,%r8,8), %rdx
	movq	(%rdx,%rax,8), %rdx
	movzwl	2(%rdx), %ebp
	movl	%ebp, %esi
	shrl	$15, %esi
	addl	%ebp, %esi
	sarw	%si
	movw	%si, 2(%rdx)
	movq	8(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	(%rcx,%rax,8), %rcx
	movzwl	2(%rcx), %edx
	movl	%edx, %esi
	shrl	$15, %esi
	addl	%edx, %esi
	sarw	%si
	movw	%si, 2(%rcx)
	movq	4848(%rdi), %rcx
	movq	(%rcx), %rdx
	movq	(%rdx,%r8,8), %rdx
	movq	(%rdx,%rax,8), %rdx
	movzwl	2(%rdx), %ebp
	movl	%ebp, %esi
	shrl	$15, %esi
	addl	%ebp, %esi
	sarw	%si
	movw	%si, 2(%rdx)
	movq	8(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	(%rcx,%rax,8), %rcx
	movzwl	2(%rcx), %edx
	movl	%edx, %esi
	shrl	$15, %esi
	addl	%edx, %esi
	sarw	%si
	movw	%si, 2(%rcx)
	jmp	.LBB41_78
.LBB41_38:                              #   in Loop: Header=BB41_27 Depth=2
	movb	$-1, (%rbx,%rax)
	movq	$-1, %rbp
.LBB41_39:                              #   in Loop: Header=BB41_27 Depth=2
	movq	1608(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	%rbp, (%rcx,%rax,8)
	movq	6488(%r14), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movzbl	(%rcx,%rsi), %edx
	movq	1600(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r8,8), %rbx
	cmpb	$-1, %dl
	je	.LBB41_41
# BB#40:                                #   in Loop: Header=BB41_27 Depth=2
	movb	%dl, (%rbx,%rax)
	movq	6504(%r14), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%rsi,8), %rsi
	jmp	.LBB41_42
.LBB41_41:                              #   in Loop: Header=BB41_27 Depth=2
	movb	$-1, (%rbx,%rax)
	movq	$-1, %rsi
.LBB41_42:                              #   in Loop: Header=BB41_27 Depth=2
	movq	1608(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	%rsi, (%rcx,%rax,8)
	movzbl	6376(%r14), %ebx
	movb	%bl, 4864(%rdi)
	movq	img(%rip), %rcx
	cmpl	$1, 14452(%rcx)
	jne	.LBB41_78
# BB#43:                                #   in Loop: Header=BB41_27 Depth=2
	movq	1600(%rdi), %rsi
	movq	(%rsi), %rcx
	movq	(%rcx,%r8,8), %rcx
	movzbl	(%rcx,%rax), %edx
	orb	%dl, %bl
	je	.LBB41_46
# BB#44:                                # %._crit_edge1219
                                        #   in Loop: Header=BB41_27 Depth=2
	cmpb	$-1, %dl
	jne	.LBB41_76
# BB#51:                                #   in Loop: Header=BB41_27 Depth=2
	movq	8(%rsi), %rcx
	movq	(%rcx,%r8,8), %rcx
	cmpb	$0, (%rcx,%rax)
	jne	.LBB41_76
# BB#55:                                #   in Loop: Header=BB41_27 Depth=2
	movq	1616(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	(%rcx,%rax,8), %rsi
	movswl	(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$1, %edx
	ja	.LBB41_76
# BB#56:                                #   in Loop: Header=BB41_27 Depth=2
	movswl	2(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$2, %edx
	setb	%sil
	jmp	.LBB41_77
.LBB41_46:                              #   in Loop: Header=BB41_27 Depth=2
	movq	1616(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	(%rcx,%rax,8), %rsi
	movswl	(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$1, %edx
	ja	.LBB41_76
# BB#47:                                #   in Loop: Header=BB41_27 Depth=2
	movswl	2(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	movb	$1, %sil
	cmpl	$2, %edx
	jb	.LBB41_77
.LBB41_76:                              #   in Loop: Header=BB41_27 Depth=2
	xorl	%esi, %esi
.LBB41_77:                              #   in Loop: Header=BB41_27 Depth=2
	xorb	$1, %sil
	movq	1624(%rdi), %rcx
	movq	(%rcx,%r8,8), %rcx
	movb	%sil, (%rcx,%rax)
	.p2align	4, 0x90
.LBB41_78:                              #   in Loop: Header=BB41_27 Depth=2
	incq	%rax
	movl	6392(%r14), %esi
	movl	%esi, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%esi, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %rax
	jl	.LBB41_27
# BB#79:                                # %._crit_edge928.loopexit
                                        #   in Loop: Header=BB41_25 Depth=1
	movl	6396(%r14), %eax
.LBB41_80:                              # %._crit_edge928
                                        #   in Loop: Header=BB41_25 Depth=1
	incq	%r8
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%eax, %ecx
	sarl	$3, %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %r8
	jl	.LBB41_25
.LBB41_81:                              # %.loopexit894
	movq	active_sps(%rip), %rcx
	movl	1148(%rcx), %edx
	testl	%edx, %edx
	je	.LBB41_83
# BB#82:
	cmpl	$0, 1156(%rcx)
	je	.LBB41_94
.LBB41_83:
	movq	img(%rip), %rax
	cmpl	$0, 24(%rax)
	jne	.LBB41_93
# BB#84:                                # %.preheader891
	movl	6396(%r14), %eax
	sarl	$2, %eax
	testl	%eax, %eax
	jle	.LBB41_93
# BB#85:                                # %.lr.ph924
	leaq	6544(%r14), %r9
	leaq	6536(%r14), %r8
	movl	6392(%r14), %eax
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB41_86:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB41_88 Depth 2
	movl	%eax, %ecx
	sarl	$2, %ecx
	testl	%ecx, %ecx
	jle	.LBB41_91
# BB#87:                                # %.lr.ph921
                                        #   in Loop: Header=BB41_86 Depth=1
	movl	%r12d, %ecx
	sarl	%ecx
	movl	%r12d, %esi
	sarl	$3, %esi
	leal	(%rcx,%rsi,4), %r11d
	leal	4(%r11), %r10d
	movslq	%ecx, %r15
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB41_88:                              #   Parent Loop BB41_86 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	6528(%r14), %rbp
	movq	(%rbp,%r12,8), %rbp
	cmpb	$0, (%rbp,%rcx)
	je	.LBB41_90
# BB#89:                                #   in Loop: Header=BB41_88 Depth=2
	movq	enc_picture(%rip), %rax
	movl	4(%rax), %edx
	movq	6536(%r14), %rbp
	movq	6544(%r14), %rsi
	movl	%edx, %ebx
	subl	4(%rsi), %ebx
	movl	%ebx, %eax
	negl	%eax
	cmovll	%ebx, %eax
	subl	4(%rbp), %edx
	movl	%edx, %ebx
	negl	%ebx
	cmovll	%edx, %ebx
	cmpl	%ebx, %eax
	cmovgq	%rbp, %rsi
	movq	%r9, %r13
	cmovgq	%r8, %r13
	movq	6512(%rsi), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r15,8), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movzwl	(%rdx), %edx
	movq	1616(%rdi), %rsi
	movq	(%rsi), %rbx
	movq	(%rbx,%r12,8), %rbx
	movq	(%rbx,%rcx,8), %rbx
	movw	%dx, (%rbx)
	movq	(%r13), %rdx
	movq	6512(%rdx), %rbp
	movq	(%rbp), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax,%rcx,8), %rax
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rbx)
	movl	%r10d, %eax
	cmovgl	%r11d, %eax
	movslq	%eax, %rbx
	movq	8(%rbp), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax,%rcx,8), %rax
	movzwl	(%rax), %ebp
	movq	8(%rsi), %rsi
	movq	(%rsi,%r12,8), %rsi
	movq	(%rsi,%rcx,8), %rsi
	movw	%bp, (%rsi)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rsi)
	movq	6488(%rdx), %rax
	movq	(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movzbl	(%rax,%rcx), %eax
	movq	1600(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r12,8), %rdx
	movb	%al, (%rdx,%rcx)
	movq	(%r13), %rax
	movq	6488(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movzbl	(%rax,%rcx), %eax
	movq	1600(%rdi), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%r12,8), %rdx
	movb	%al, (%rdx,%rcx)
	movq	6504(%r14), %rax
	movq	(%rax), %rdx
	movq	(%rdx,%rbx,8), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	1608(%rdi), %rsi
	movq	(%rsi), %rbp
	movq	(%rbp,%r12,8), %rbp
	movq	%rdx, (%rbp,%rcx,8)
	movq	8(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax,%rcx,8), %rax
	movq	8(%rsi), %rdx
	movq	(%rdx,%r12,8), %rdx
	movq	%rax, (%rdx,%rcx,8)
	movq	(%r13), %rax
	movzbl	6376(%rax), %eax
	movb	%al, 4864(%rdi)
	movl	6392(%r14), %eax
.LBB41_90:                              #   in Loop: Header=BB41_88 Depth=2
	incq	%rcx
	movl	%eax, %edx
	sarl	$2, %edx
	movslq	%edx, %rdx
	cmpq	%rdx, %rcx
	jl	.LBB41_88
.LBB41_91:                              # %._crit_edge922
                                        #   in Loop: Header=BB41_86 Depth=1
	incq	%r12
	movl	6396(%r14), %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %r12
	jl	.LBB41_86
# BB#92:                                # %.loopexit892.loopexit
	movq	active_sps(%rip), %rcx
	movl	1148(%rcx), %edx
.LBB41_93:                              # %.loopexit892
	movb	6376(%r14), %bl
	leaq	4864(%rdi), %r8
	movb	%bl, 4864(%rdi)
	testl	%edx, %edx
	jne	.LBB41_95
	jmp	.LBB41_96
.LBB41_94:                              # %.loopexit892.thread
	movb	6376(%r14), %dl
	leaq	4864(%rdi), %r8
	movb	%dl, 4864(%rdi)
.LBB41_95:
	cmpl	$0, 1156(%rcx)
	je	.LBB41_116
.LBB41_96:                              # %.preheader886
	movl	6396(%r14), %edx
	movl	%edx, %ecx
	sarl	$2, %ecx
	testl	%ecx, %ecx
	jle	.LBB41_136
# BB#97:                                # %.lr.ph914
	movl	6392(%r14), %ebx
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB41_98:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB41_100 Depth 2
	movl	%ebx, %esi
	sarl	$2, %esi
	testl	%esi, %esi
	jle	.LBB41_115
# BB#99:                                # %.lr.ph911
                                        #   in Loop: Header=BB41_98 Depth=1
	movl	%r10d, %edx
	orl	$1, %edx
	movl	%r10d, %esi
	andl	$-2, %esi
	testb	$2, %r10b
	cmovneq	%rdx, %rsi
	movslq	%esi, %r9
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB41_100:                             #   Parent Loop BB41_98 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %rsi
	orq	$1, %rsi
	movl	%eax, %ebp
	andl	$-2, %ebp
	testb	$2, %al
	cmovneq	%rsi, %rbp
	movq	1616(%rdi), %rbx
	movq	(%rbx), %rdx
	movq	(%rdx,%r9,8), %rcx
	movslq	%ebp, %rsi
	movq	(%rcx,%rsi,8), %rcx
	movzwl	(%rcx), %ebp
	movq	(%rdx,%r10,8), %rdx
	movq	(%rdx,%rax,8), %rdx
	movw	%bp, (%rdx)
	movzwl	2(%rcx), %ecx
	movw	%cx, 2(%rdx)
	movq	8(%rbx), %rcx
	movq	(%rcx,%r9,8), %rdx
	movq	(%rdx,%rsi,8), %rdx
	movzwl	(%rdx), %ebp
	movq	(%rcx,%r10,8), %rcx
	movq	(%rcx,%rax,8), %rcx
	movw	%bp, (%rcx)
	movzwl	2(%rdx), %edx
	movw	%dx, 2(%rcx)
	movq	1600(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r9,8), %rdx
	movzbl	(%rdx,%rsi), %edx
	movq	(%rcx,%r10,8), %rcx
	movb	%dl, (%rcx,%rax)
	movq	1600(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r9,8), %rdx
	movzbl	(%rdx,%rsi), %edx
	movq	(%rcx,%r10,8), %rcx
	movb	%dl, (%rcx,%rax)
	movq	1608(%rdi), %rcx
	movq	(%rcx), %rdx
	movq	(%rdx,%r9,8), %rbp
	movq	(%rbp,%rsi,8), %rbp
	movq	(%rdx,%r10,8), %rdx
	movq	%rbp, (%rdx,%rax,8)
	movq	8(%rcx), %rcx
	movq	(%rcx,%r9,8), %rdx
	movq	(%rdx,%rsi,8), %rdx
	movq	(%rcx,%r10,8), %rcx
	movq	%rdx, (%rcx,%rax,8)
	movq	img(%rip), %rcx
	cmpl	$1, 14452(%rcx)
	jne	.LBB41_113
# BB#101:                               #   in Loop: Header=BB41_100 Depth=2
	movq	1600(%rdi), %rsi
	movq	(%rsi), %rcx
	movq	(%rcx,%r10,8), %rcx
	movzbl	(%rcx,%rax), %ebx
	movzbl	(%r8), %ecx
	orb	%bl, %cl
	je	.LBB41_104
# BB#102:                               # %._crit_edge1242
                                        #   in Loop: Header=BB41_100 Depth=2
	cmpb	$-1, %bl
	jne	.LBB41_111
# BB#107:                               #   in Loop: Header=BB41_100 Depth=2
	movq	8(%rsi), %rcx
	movq	(%rcx,%r10,8), %rcx
	cmpb	$0, (%rcx,%rax)
	jne	.LBB41_111
# BB#109:                               #   in Loop: Header=BB41_100 Depth=2
	movq	1616(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	(%rcx,%rax,8), %rsi
	movswl	(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$1, %edx
	ja	.LBB41_111
# BB#110:                               #   in Loop: Header=BB41_100 Depth=2
	movswl	2(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$2, %edx
	setb	%sil
	jmp	.LBB41_112
.LBB41_104:                             #   in Loop: Header=BB41_100 Depth=2
	movq	1616(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	(%rcx,%rax,8), %rsi
	movswl	(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$1, %edx
	ja	.LBB41_111
# BB#105:                               #   in Loop: Header=BB41_100 Depth=2
	movswl	2(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	movb	$1, %sil
	cmpl	$2, %edx
	jb	.LBB41_112
	.p2align	4, 0x90
.LBB41_111:                             #   in Loop: Header=BB41_100 Depth=2
	xorl	%esi, %esi
.LBB41_112:                             #   in Loop: Header=BB41_100 Depth=2
	xorb	$1, %sil
	movq	1624(%rdi), %rcx
	movq	(%rcx,%r10,8), %rcx
	movb	%sil, (%rcx,%rax)
.LBB41_113:                             #   in Loop: Header=BB41_100 Depth=2
	incq	%rax
	movl	6392(%r14), %ebx
	movl	%ebx, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %rax
	jl	.LBB41_100
# BB#114:                               # %._crit_edge912.loopexit
                                        #   in Loop: Header=BB41_98 Depth=1
	movl	6396(%r14), %edx
.LBB41_115:                             # %._crit_edge912
                                        #   in Loop: Header=BB41_98 Depth=1
	incq	%r10
	movl	%edx, %eax
	sarl	$2, %eax
	cltq
	cmpq	%rax, %r10
	jl	.LBB41_98
	jmp	.LBB41_136
.LBB41_116:                             # %.preheader889
	movl	6396(%r14), %edx
	cmpl	$4, %edx
	jl	.LBB41_136
# BB#117:                               # %.preheader888.lr.ph
	movl	6392(%r14), %esi
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB41_118:                             # %.preheader888
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB41_120 Depth 2
	cmpl	$4, %esi
	jl	.LBB41_135
# BB#119:                               # %.lr.ph916.preheader
                                        #   in Loop: Header=BB41_118 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB41_120:                             # %.lr.ph916
                                        #   Parent Loop BB41_118 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	6512(%r14), %rdx
	movq	(%rdx), %rsi
	movq	(%rsi,%r8,8), %rsi
	movq	(%rsi,%rcx,8), %rsi
	movzwl	(%rsi), %ebp
	movq	1616(%rdi), %rbx
	movq	(%rbx), %rax
	movq	(%rax,%r8,8), %rax
	movq	(%rax,%rcx,8), %rax
	movw	%bp, (%rax)
	movzwl	2(%rsi), %esi
	movw	%si, 2(%rax)
	movq	8(%rdx), %rax
	movq	(%rax,%r8,8), %rax
	movq	(%rax,%rcx,8), %rax
	movzwl	(%rax), %edx
	movq	8(%rbx), %rsi
	movq	(%rsi,%r8,8), %rsi
	movq	(%rsi,%rcx,8), %rsi
	movw	%dx, (%rsi)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rsi)
	movq	6488(%r14), %rax
	movq	(%rax), %rax
	movq	(%rax,%r8,8), %rax
	movzbl	(%rax,%rcx), %eax
	movq	1600(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r8,8), %rdx
	movb	%al, (%rdx,%rcx)
	movq	6488(%r14), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r8,8), %rax
	movzbl	(%rax,%rcx), %eax
	movq	1600(%rdi), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%r8,8), %rdx
	movb	%al, (%rdx,%rcx)
	movq	6504(%r14), %rax
	movq	(%rax), %rdx
	movq	(%rdx,%r8,8), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	1608(%rdi), %rsi
	movq	(%rsi), %rbp
	movq	(%rbp,%r8,8), %rbp
	movq	%rdx, (%rbp,%rcx,8)
	movq	8(%rax), %rax
	movq	(%rax,%r8,8), %rax
	movq	(%rax,%rcx,8), %rax
	movq	8(%rsi), %rdx
	movq	(%rdx,%r8,8), %rdx
	movq	%rax, (%rdx,%rcx,8)
	movq	img(%rip), %rax
	cmpl	$1, 14452(%rax)
	jne	.LBB41_133
# BB#121:                               #   in Loop: Header=BB41_120 Depth=2
	movq	1600(%rdi), %rdx
	movq	(%rdx), %rax
	movq	(%rax,%r8,8), %rax
	movzbl	(%rax,%rcx), %ebx
	movzbl	4864(%rdi), %eax
	orb	%bl, %al
	je	.LBB41_124
# BB#122:                               # %._crit_edge1233
                                        #   in Loop: Header=BB41_120 Depth=2
	cmpb	$-1, %bl
	jne	.LBB41_131
# BB#127:                               #   in Loop: Header=BB41_120 Depth=2
	movq	8(%rdx), %rax
	movq	(%rax,%r8,8), %rax
	cmpb	$0, (%rax,%rcx)
	jne	.LBB41_131
# BB#129:                               #   in Loop: Header=BB41_120 Depth=2
	movq	1616(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r8,8), %rax
	movq	(%rax,%rcx,8), %rdx
	movswl	(%rdx), %eax
	movl	%eax, %esi
	negl	%esi
	cmovll	%eax, %esi
	cmpl	$1, %esi
	ja	.LBB41_131
# BB#130:                               #   in Loop: Header=BB41_120 Depth=2
	movswl	2(%rdx), %eax
	movl	%eax, %edx
	negl	%edx
	cmovll	%eax, %edx
	cmpl	$2, %edx
	setb	%dl
	jmp	.LBB41_132
.LBB41_124:                             #   in Loop: Header=BB41_120 Depth=2
	movq	1616(%rdi), %rax
	movq	(%rax), %rax
	movq	(%rax,%r8,8), %rax
	movq	(%rax,%rcx,8), %rdx
	movswl	(%rdx), %eax
	movl	%eax, %esi
	negl	%esi
	cmovll	%eax, %esi
	cmpl	$1, %esi
	ja	.LBB41_131
# BB#125:                               #   in Loop: Header=BB41_120 Depth=2
	movswl	2(%rdx), %eax
	movl	%eax, %esi
	negl	%esi
	cmovll	%eax, %esi
	movb	$1, %dl
	cmpl	$2, %esi
	jb	.LBB41_132
	.p2align	4, 0x90
.LBB41_131:                             #   in Loop: Header=BB41_120 Depth=2
	xorl	%edx, %edx
.LBB41_132:                             #   in Loop: Header=BB41_120 Depth=2
	xorb	$1, %dl
	movq	1624(%rdi), %rax
	movq	(%rax,%r8,8), %rax
	movb	%dl, (%rax,%rcx)
.LBB41_133:                             #   in Loop: Header=BB41_120 Depth=2
	incq	%rcx
	movl	6392(%r14), %esi
	movl	%esi, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%esi, %eax
	sarl	$2, %eax
	cltq
	cmpq	%rax, %rcx
	jl	.LBB41_120
# BB#134:                               # %._crit_edge917.loopexit
                                        #   in Loop: Header=BB41_118 Depth=1
	movl	6396(%r14), %edx
.LBB41_135:                             # %._crit_edge917
                                        #   in Loop: Header=BB41_118 Depth=1
	incq	%r8
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%edx, %eax
	sarl	$2, %eax
	cltq
	cmpq	%rax, %r8
	jl	.LBB41_118
.LBB41_136:                             # %.loopexit887
	movq	img(%rip), %r10
	cmpl	$0, 14452(%r10)
	jne	.LBB41_171
# BB#137:                               # %.preheader885
	cmpl	$4, %edx
	jl	.LBB41_150
# BB#138:                               # %.preheader884.lr.ph
	movl	6392(%r14), %r8d
	movl	%r8d, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%r8d, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %r11
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%edx, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %r9
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB41_139:                             # %.preheader884
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB41_141 Depth 2
	cmpl	$4, %r8d
	jl	.LBB41_149
# BB#140:                               # %.lr.ph906
                                        #   in Loop: Header=BB41_139 Depth=1
	movl	15268(%r10), %ebx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB41_141:                             #   Parent Loop BB41_139 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%ebx, %ebx
	je	.LBB41_144
.LBB41_142:                             #   in Loop: Header=BB41_141 Depth=2
	movq	6528(%r14), %rdx
	movq	(%rdx,%rbp,8), %rdx
	cmpb	$0, (%rdx,%rcx)
	je	.LBB41_145
# BB#143:                               #   in Loop: Header=BB41_141 Depth=2
	movq	1616(%rdi), %rdx
	movq	(%rdx), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax,%rcx,8), %rax
	shlw	2(%rax)
	movq	8(%rdx), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax,%rcx,8), %rax
	shlw	2(%rax)
	jmp	.LBB41_148
	.p2align	4, 0x90
.LBB41_144:                             #   in Loop: Header=BB41_141 Depth=2
	cmpl	$0, 24(%r10)
	je	.LBB41_142
.LBB41_145:                             # %.thread1264
                                        #   in Loop: Header=BB41_141 Depth=2
	cmpl	$0, 24(%r10)
	je	.LBB41_148
# BB#146:                               #   in Loop: Header=BB41_141 Depth=2
	movq	6528(%r14), %rax
	movq	(%rax,%rbp,8), %rax
	cmpb	$0, (%rax,%rcx)
	jne	.LBB41_148
# BB#147:                               #   in Loop: Header=BB41_141 Depth=2
	movq	1616(%rdi), %r15
	movq	(%r15), %rdx
	movq	(%rdx,%rbp,8), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movzwl	2(%rdx), %eax
	movl	%eax, %esi
	shrl	$15, %esi
	addl	%eax, %esi
	sarw	%si
	movw	%si, 2(%rdx)
	movq	8(%r15), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax,%rcx,8), %rax
	movzwl	2(%rax), %edx
	movl	%edx, %esi
	shrl	$15, %esi
	addl	%edx, %esi
	sarw	%si
	movw	%si, 2(%rax)
	.p2align	4, 0x90
.LBB41_148:                             #   in Loop: Header=BB41_141 Depth=2
	incq	%rcx
	cmpq	%r11, %rcx
	jl	.LBB41_141
.LBB41_149:                             # %._crit_edge907
                                        #   in Loop: Header=BB41_139 Depth=1
	incq	%rbp
	cmpq	%r9, %rbp
	jl	.LBB41_139
.LBB41_150:                             # %.preheader883
	movl	15268(%r10), %eax
	leal	2(,%rax,4), %eax
	testl	%eax, %eax
	jle	.LBB41_171
# BB#151:                               # %.preheader.lr.ph
	movq	enc_picture(%rip), %r10
	movq	img(%rip), %rax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	leaq	14472(%rax), %r9
	xorl	%edi, %edi
	movl	$-128, %r12d
	movl	$127, %r11d
	movl	$-1024, %r14d           # imm = 0xFC00
	movl	$1023, %r15d            # imm = 0x3FF
	.p2align	4, 0x90
.LBB41_152:                             # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB41_166 Depth 2
                                        #     Child Loop BB41_156 Depth 2
                                        #     Child Loop BB41_161 Depth 2
	cmpl	$0, listXsize(,%rdi,4)
	jle	.LBB41_170
# BB#153:                               # %.lr.ph
                                        #   in Loop: Header=BB41_152 Depth=1
	movq	-56(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rdi,8), %r13
	movq	%rdi, %rax
	orq	$1, %rax
	movq	(%rcx,%rax,8), %rax
	movq	(%rax), %rbp
	testl	%edi, %edi
	je	.LBB41_160
# BB#154:                               # %.lr.ph
                                        #   in Loop: Header=BB41_152 Depth=1
	cmpl	$2, %edi
	jne	.LBB41_165
# BB#155:                               # %.lr.ph.split.split.us.preheader
                                        #   in Loop: Header=BB41_152 Depth=1
	movq	%r9, %rsi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB41_156:                             # %.lr.ph.split.split.us
                                        #   Parent Loop BB41_152 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	8(%r10), %ebx
	movq	(%r13,%rcx,8), %rax
	movl	4(%rax), %eax
	subl	%eax, %ebx
	cmpl	$-129, %ebx
	cmovlel	%r12d, %ebx
	movl	4(%rbp), %r8d
	subl	%eax, %r8d
	cmpl	$-129, %r8d
	cmovlel	%r12d, %r8d
	cmpl	$128, %r8d
	cmovgel	%r11d, %r8d
	testl	%r8d, %r8d
	je	.LBB41_158
# BB#157:                               #   in Loop: Header=BB41_156 Depth=2
	cmpl	$128, %ebx
	cmovgel	%r11d, %ebx
	movl	%r8d, %edx
	shrl	$31, %edx
	addl	%r8d, %edx
	sarl	%edx
	movl	%edx, %eax
	negl	%eax
	cmpl	$-1, %r8d
	cmovgel	%edx, %eax
	addl	$16384, %eax            # imm = 0x4000
	cltd
	idivl	%r8d
	imull	%ebx, %eax
	addl	$32, %eax
	sarl	$6, %eax
	cmpl	$-1025, %eax            # imm = 0xFBFF
	cmovlel	%r14d, %eax
	cmpl	$1024, %eax             # imm = 0x400
	cmovgel	%r15d, %eax
	jmp	.LBB41_159
	.p2align	4, 0x90
.LBB41_158:                             #   in Loop: Header=BB41_156 Depth=2
	movl	$9999, %eax             # imm = 0x270F
.LBB41_159:                             #   in Loop: Header=BB41_156 Depth=2
	movl	%eax, (%rsi)
	incq	%rcx
	movslq	listXsize(,%rdi,4), %rax
	addq	$4, %rsi
	cmpq	%rax, %rcx
	jl	.LBB41_156
	jmp	.LBB41_170
	.p2align	4, 0x90
.LBB41_160:                             # %.lr.ph.split.us.preheader
                                        #   in Loop: Header=BB41_152 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB41_161:                             # %.lr.ph.split.us
                                        #   Parent Loop BB41_152 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%r10), %esi
	movq	(%r13,%rcx,8), %rax
	movl	4(%rax), %eax
	subl	%eax, %esi
	cmpl	$-129, %esi
	cmovlel	%r12d, %esi
	movl	4(%rbp), %ebx
	subl	%eax, %ebx
	cmpl	$-129, %ebx
	cmovlel	%r12d, %ebx
	cmpl	$128, %ebx
	cmovgel	%r11d, %ebx
	testl	%ebx, %ebx
	je	.LBB41_163
# BB#162:                               #   in Loop: Header=BB41_161 Depth=2
	cmpl	$128, %esi
	cmovgel	%r11d, %esi
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
	movl	%edx, %eax
	negl	%eax
	cmpl	$-1, %ebx
	cmovgel	%edx, %eax
	addl	$16384, %eax            # imm = 0x4000
	cltd
	idivl	%ebx
	imull	%esi, %eax
	addl	$32, %eax
	sarl	$6, %eax
	cmpl	$-1025, %eax            # imm = 0xFBFF
	cmovlel	%r14d, %eax
	cmpl	$1024, %eax             # imm = 0x400
	cmovgel	%r15d, %eax
	jmp	.LBB41_164
	.p2align	4, 0x90
.LBB41_163:                             #   in Loop: Header=BB41_161 Depth=2
	movl	$9999, %eax             # imm = 0x270F
.LBB41_164:                             #   in Loop: Header=BB41_161 Depth=2
	movl	%eax, (%r9,%rcx,4)
	incq	%rcx
	movslq	listXsize(,%rdi,4), %rax
	cmpq	%rax, %rcx
	jl	.LBB41_161
	jmp	.LBB41_170
	.p2align	4, 0x90
.LBB41_165:                             # %.lr.ph.split.split.preheader
                                        #   in Loop: Header=BB41_152 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB41_166:                             # %.lr.ph.split.split
                                        #   Parent Loop BB41_152 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	12(%r10), %esi
	movq	(%r13,%rcx,8), %rax
	movl	4(%rax), %eax
	subl	%eax, %esi
	cmpl	$-129, %esi
	cmovlel	%r12d, %esi
	movl	4(%rbp), %ebx
	subl	%eax, %ebx
	cmpl	$-129, %ebx
	cmovlel	%r12d, %ebx
	cmpl	$128, %ebx
	cmovgel	%r11d, %ebx
	testl	%ebx, %ebx
	je	.LBB41_168
# BB#167:                               #   in Loop: Header=BB41_166 Depth=2
	cmpl	$128, %esi
	cmovgel	%r11d, %esi
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
	movl	%edx, %eax
	negl	%eax
	cmpl	$-1, %ebx
	cmovgel	%edx, %eax
	addl	$16384, %eax            # imm = 0x4000
	cltd
	idivl	%ebx
	imull	%esi, %eax
	addl	$32, %eax
	sarl	$6, %eax
	cmpl	$-1025, %eax            # imm = 0xFBFF
	cmovlel	%r14d, %eax
	cmpl	$1024, %eax             # imm = 0x400
	cmovgel	%r15d, %eax
	jmp	.LBB41_169
	.p2align	4, 0x90
.LBB41_168:                             #   in Loop: Header=BB41_166 Depth=2
	movl	$9999, %eax             # imm = 0x270F
.LBB41_169:                             #   in Loop: Header=BB41_166 Depth=2
	movl	%eax, (%r9,%rcx,4)
	incq	%rcx
	movslq	listXsize(,%rdi,4), %rax
	cmpq	%rax, %rcx
	jl	.LBB41_166
.LBB41_170:                             # %._crit_edge
                                        #   in Loop: Header=BB41_152 Depth=1
	addq	$2, %rdi
	movq	-88(%rsp), %rax         # 8-byte Reload
	movl	15268(%rax), %eax
	leal	2(,%rax,4), %eax
	cltq
	addq	$256, %r9               # imm = 0x100
	cmpq	%rax, %rdi
	jl	.LBB41_152
.LBB41_171:                             # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB41_172:
	movq	6544(%r14), %r12
.LBB41_173:
	movq	%r12, %r15
	movq	%r12, %r14
	jmp	.LBB41_8
.Lfunc_end41:
	.size	compute_colocated, .Lfunc_end41-compute_colocated
	.cfi_endproc

	.p2align	4, 0x90
	.type	unmark_long_term_field_for_reference_by_frame_idx,@function
unmark_long_term_field_for_reference_by_frame_idx: # @unmark_long_term_field_for_reference_by_frame_idx
	.cfi_startproc
# BB#0:
	movl	%ecx, %r9d
	testl	%r8d, %r8d
	jns	.LBB42_2
# BB#1:
	movl	log2_max_frame_num_minus4(%rip), %ecx
	addl	$4, %ecx
	movl	$2, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	addl	%eax, %r8d
.LBB42_2:
	pushq	%rbp
.Lcfi184:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi185:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi186:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi187:
	.cfi_def_cfa_offset 40
.Lcfi188:
	.cfi_offset %rbx, -40
.Lcfi189:
	.cfi_offset %r14, -32
.Lcfi190:
	.cfi_offset %r15, -24
.Lcfi191:
	.cfi_offset %rbp, -16
	movl	dpb+36(%rip), %r15d
	testq	%r15, %r15
	je	.LBB42_46
# BB#3:                                 # %.lr.ph
	movq	dpb+16(%rip), %r11
	movl	%r8d, %r10d
	shrl	$31, %r10d
	addl	%r8d, %r10d
	sarl	%r10d
	movq	dpb+56(%rip), %r8
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB42_4:                               # =>This Inner Loop Header: Depth=1
	movq	(%r11,%rax,8), %rbx
	cmpl	%esi, 28(%rbx)
	jne	.LBB42_45
# BB#5:                                 #   in Loop: Header=BB42_4 Depth=1
	cmpl	$2, %edi
	je	.LBB42_30
# BB#6:                                 #   in Loop: Header=BB42_4 Depth=1
	cmpl	$1, %edi
	jne	.LBB42_45
# BB#7:                                 #   in Loop: Header=BB42_4 Depth=1
	movl	8(%rbx), %ecx
	cmpl	$1, %ecx
	je	.LBB42_14
# BB#8:                                 #   in Loop: Header=BB42_4 Depth=1
	cmpl	$3, %ecx
	je	.LBB42_14
# BB#9:                                 #   in Loop: Header=BB42_4 Depth=1
	testl	%edx, %edx
	je	.LBB42_13
# BB#10:                                #   in Loop: Header=BB42_4 Depth=1
	testq	%r8, %r8
	je	.LBB42_14
# BB#11:                                #   in Loop: Header=BB42_4 Depth=1
	cmpq	%rbx, %r8
	jne	.LBB42_14
# BB#12:                                #   in Loop: Header=BB42_4 Depth=1
	cmpl	%r9d, 20(%r8)
	jne	.LBB42_14
	jmp	.LBB42_45
.LBB42_30:                              #   in Loop: Header=BB42_4 Depth=1
	movl	8(%rbx), %ecx
	cmpl	$2, %ecx
	je	.LBB42_33
# BB#31:                                #   in Loop: Header=BB42_4 Depth=1
	cmpl	$3, %ecx
	je	.LBB42_33
	jmp	.LBB42_26
.LBB42_13:                              #   in Loop: Header=BB42_4 Depth=1
	cmpl	%r10d, 20(%rbx)
	je	.LBB42_45
.LBB42_14:                              #   in Loop: Header=BB42_4 Depth=1
	movl	(%rbx), %ebp
	testb	$1, %bpl
	je	.LBB42_17
# BB#15:                                #   in Loop: Header=BB42_4 Depth=1
	movq	48(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB42_17
# BB#16:                                #   in Loop: Header=BB42_4 Depth=1
	movq	$0, 6376(%rcx)
.LBB42_17:                              #   in Loop: Header=BB42_4 Depth=1
	testb	$2, %bpl
	je	.LBB42_20
# BB#18:                                #   in Loop: Header=BB42_4 Depth=1
	movq	56(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB42_20
# BB#19:                                #   in Loop: Header=BB42_4 Depth=1
	movq	$0, 6376(%rcx)
.LBB42_20:                              # %thread-pre-split.i45
                                        #   in Loop: Header=BB42_4 Depth=1
	cmpl	$3, %ebp
	jne	.LBB42_25
# BB#21:                                #   in Loop: Header=BB42_4 Depth=1
	movq	48(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB42_24
# BB#22:                                #   in Loop: Header=BB42_4 Depth=1
	movq	56(%rbx), %r14
	testq	%r14, %r14
	je	.LBB42_24
# BB#23:                                #   in Loop: Header=BB42_4 Depth=1
	movl	$0, 6380(%rbp)
	movl	$0, 6376(%rbp)
	movq	$0, 6376(%r14)
.LBB42_24:                              #   in Loop: Header=BB42_4 Depth=1
	movq	40(%rbx), %rcx
	movl	$0, 6380(%rcx)
	movl	$0, 6376(%rcx)
.LBB42_25:                              # %unmark_for_long_term_reference.exit52
                                        #   in Loop: Header=BB42_4 Depth=1
	cmpl	$2, %edi
	movq	$0, 4(%rbx)
	jne	.LBB42_45
.LBB42_26:                              # %.thread55
                                        #   in Loop: Header=BB42_4 Depth=1
	testl	%edx, %edx
	je	.LBB42_32
# BB#27:                                #   in Loop: Header=BB42_4 Depth=1
	testq	%r8, %r8
	je	.LBB42_33
# BB#28:                                #   in Loop: Header=BB42_4 Depth=1
	cmpq	%rbx, %r8
	jne	.LBB42_33
# BB#29:                                #   in Loop: Header=BB42_4 Depth=1
	cmpl	%r9d, 20(%r8)
	jne	.LBB42_33
	jmp	.LBB42_45
.LBB42_32:                              #   in Loop: Header=BB42_4 Depth=1
	cmpl	%r10d, 20(%rbx)
	je	.LBB42_45
	.p2align	4, 0x90
.LBB42_33:                              #   in Loop: Header=BB42_4 Depth=1
	movl	(%rbx), %ebp
	testb	$1, %bpl
	je	.LBB42_36
# BB#34:                                #   in Loop: Header=BB42_4 Depth=1
	movq	48(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB42_36
# BB#35:                                #   in Loop: Header=BB42_4 Depth=1
	movq	$0, 6376(%rcx)
.LBB42_36:                              #   in Loop: Header=BB42_4 Depth=1
	testb	$2, %bpl
	je	.LBB42_39
# BB#37:                                #   in Loop: Header=BB42_4 Depth=1
	movq	56(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB42_39
# BB#38:                                #   in Loop: Header=BB42_4 Depth=1
	movq	$0, 6376(%rcx)
.LBB42_39:                              # %thread-pre-split.i35
                                        #   in Loop: Header=BB42_4 Depth=1
	cmpl	$3, %ebp
	jne	.LBB42_44
# BB#40:                                #   in Loop: Header=BB42_4 Depth=1
	movq	48(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB42_43
# BB#41:                                #   in Loop: Header=BB42_4 Depth=1
	movq	56(%rbx), %r14
	testq	%r14, %r14
	je	.LBB42_43
# BB#42:                                #   in Loop: Header=BB42_4 Depth=1
	movl	$0, 6380(%rbp)
	movl	$0, 6376(%rbp)
	movq	$0, 6376(%r14)
.LBB42_43:                              #   in Loop: Header=BB42_4 Depth=1
	movq	40(%rbx), %rcx
	movl	$0, 6380(%rcx)
	movl	$0, 6376(%rcx)
.LBB42_44:                              # %unmark_for_long_term_reference.exit36
                                        #   in Loop: Header=BB42_4 Depth=1
	movq	$0, 4(%rbx)
.LBB42_45:                              # %.thread
                                        #   in Loop: Header=BB42_4 Depth=1
	incq	%rax
	cmpq	%r15, %rax
	jb	.LBB42_4
.LBB42_46:                              # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end42:
	.size	unmark_long_term_field_for_reference_by_frame_idx, .Lfunc_end42-unmark_long_term_field_for_reference_by_frame_idx
	.cfi_endproc

	.p2align	4, 0x90
	.type	remove_frame_from_dpb,@function
remove_frame_from_dpb:                  # @remove_frame_from_dpb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi192:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi193:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi194:
	.cfi_def_cfa_offset 32
.Lcfi195:
	.cfi_offset %rbx, -32
.Lcfi196:
	.cfi_offset %r14, -24
.Lcfi197:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	movq	dpb(%rip), %rax
	movslq	%r14d, %rbx
	movq	(%rax,%rbx,8), %rbp
	movl	(%rbp), %eax
	cmpq	$3, %rax
	ja	.LBB43_3
# BB#1:
	jmpq	*.LJTI43_0(,%rax,8)
.LBB43_2:
	movq	48(%rbp), %rdi
	callq	free_storable_picture
	movq	$0, 48(%rbp)
	jmp	.LBB43_7
.LBB43_3:
	movl	$.L.str.29, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	jmp	.LBB43_7
.LBB43_4:
	movq	56(%rbp), %rdi
	callq	free_storable_picture
	jmp	.LBB43_6
.LBB43_5:
	movq	40(%rbp), %rdi
	callq	free_storable_picture
	movq	48(%rbp), %rdi
	callq	free_storable_picture
	movq	56(%rbp), %rdi
	callq	free_storable_picture
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%rbp)
.LBB43_6:
	movq	$0, 56(%rbp)
.LBB43_7:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	movq	dpb(%rip), %rdx
	movq	(%rdx,%rbx,8), %r8
	movl	dpb+28(%rip), %ecx
	decl	%ecx
	movl	%ecx, %ebp
	subl	%ebx, %ebp
	jbe	.LBB43_13
# BB#8:                                 # %.lr.ph
	movl	%r14d, %edi
	movl	%ecx, %esi
	leaq	-1(%rsi), %rbx
	subq	%rdi, %rbx
	andq	$3, %rbp
	je	.LBB43_11
# BB#9:                                 # %.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB43_10:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rdx,%rdi,8), %rax
	movq	%rax, (%rdx,%rdi,8)
	incq	%rdi
	movq	dpb(%rip), %rdx
	incq	%rbp
	jne	.LBB43_10
.LBB43_11:                              # %.prol.loopexit
	cmpq	$3, %rbx
	jb	.LBB43_14
	.p2align	4, 0x90
.LBB43_12:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rdx,%rdi,8), %rax
	movq	%rax, (%rdx,%rdi,8)
	movq	dpb(%rip), %rax
	movq	16(%rax,%rdi,8), %rdx
	movq	%rdx, 8(%rax,%rdi,8)
	movq	dpb(%rip), %rax
	movq	24(%rax,%rdi,8), %rdx
	movq	%rdx, 16(%rax,%rdi,8)
	movq	dpb(%rip), %rax
	movq	32(%rax,%rdi,8), %rdx
	movq	%rdx, 24(%rax,%rdi,8)
	leaq	4(%rdi), %rdi
	movq	dpb(%rip), %rdx
	cmpq	%rsi, %rdi
	jb	.LBB43_12
	jmp	.LBB43_14
.LBB43_13:                              # %.._crit_edge_crit_edge
	movl	%ecx, %esi
.LBB43_14:                              # %._crit_edge
	movq	%r8, (%rdx,%rsi,8)
	movl	%ecx, dpb+28(%rip)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end43:
	.size	remove_frame_from_dpb, .Lfunc_end43-remove_frame_from_dpb
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI43_0:
	.quad	.LBB43_7
	.quad	.LBB43_2
	.quad	.LBB43_4
	.quad	.LBB43_5

	.type	Co_located,@object      # @Co_located
	.bss
	.globl	Co_located
	.p2align	3
Co_located:
	.quad	0
	.size	Co_located, 8

	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"undefined level"
	.size	.L.str, 16

	.type	dpb,@object             # @dpb
	.comm	dpb,64,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Max. number of reference frames exceeded. Invalid stream."
	.size	.L.str.1, 58

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"DPB size at specified level is smaller than the specified number of reference frames. This is not allowed.\n"
	.size	.L.str.2, 108

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"init_dpb: dpb->fs"
	.size	.L.str.3, 18

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"init_dpb: dpb->fs_ref"
	.size	.L.str.4, 22

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"init_dpb: dpb->fs_ltref"
	.size	.L.str.5, 24

	.type	listX,@object           # @listX
	.comm	listX,48,16
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"init_dpb: listX[i]"
	.size	.L.str.6, 19

	.type	listXsize,@object       # @listXsize
	.comm	listXsize,24,16
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"alloc_frame_store: f"
	.size	.L.str.7, 21

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"alloc_storable_picture: s"
	.size	.L.str.8, 26

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"alloc_storable_picture: s->mb_field"
	.size	.L.str.9, 36

	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"init_lists: fs_list0"
	.size	.L.str.10, 21

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"init_lists: fs_listlt"
	.size	.L.str.11, 22

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"init_lists: fs_list1"
	.size	.L.str.12, 21

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Invalid remapping_of_pic_nums_idc command"
	.size	.L.str.13, 42

	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"duplicate frame_num im short-term reference picture buffer"
	.size	.L.str.14, 59

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"alloc_ref_pic_list_reordering_buffer: remapping_of_pic_nums_idc_l0"
	.size	.L.str.15, 67

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"alloc_ref_pic_list_reordering_buffer: abs_diff_pic_num_minus1_l0"
	.size	.L.str.16, 65

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"alloc_ref_pic_list_reordering_buffer: long_term_pic_idx_l0"
	.size	.L.str.17, 59

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"alloc_ref_pic_list_reordering_buffer: remapping_of_pic_nums_idc_l1"
	.size	.L.str.18, 67

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"alloc_ref_pic_list_reordering_buffer: abs_diff_pic_num_minus1_l1"
	.size	.L.str.19, 65

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"alloc_ref_pic_list_reordering_buffer: long_term_pic_idx_l1"
	.size	.L.str.20, 59

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"alloc_colocated: s"
	.size	.L.str.21, 19

	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"memory_management_control_operation = 0 not last operation in buffer"
	.size	.L.str.22, 69

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"invalid memory_management_control_operation in buffer"
	.size	.L.str.23, 54

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"field for long term marking not found"
	.size	.L.str.24, 38

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"Cannot determine smallest POC, DPB empty."
	.size	.L.str.28, 42

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"invalid frame store type"
	.size	.L.str.29, 25

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"Cannot output frame, DPB empty."
	.size	.L.str.30, 32

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"no frames for output available"
	.size	.L.str.31, 31

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"output POC must be in ascending order"
	.size	.L.str.32, 38

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Warning: reference field for long term marking not found"
	.size	.Lstr, 57

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"Warning: assigning long_term_frame_idx different from other field"
	.size	.Lstr.2, 66

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"Warning: reference frame for long term marking not found"
	.size	.Lstr.3, 57


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
