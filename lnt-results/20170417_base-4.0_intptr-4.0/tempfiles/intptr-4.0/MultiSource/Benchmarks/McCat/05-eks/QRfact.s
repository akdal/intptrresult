	.text
	.file	"QRfact.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
.LCPI0_1:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_2:
	.quad	4607182418800017408     # double 1
	.text
	.globl	Givens
	.p2align	4, 0x90
	.type	Givens,@function
Givens:                                 # @Givens
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 48
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movapd	%xmm0, %xmm3
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm1
	jne	.LBB0_2
	jp	.LBB0_2
# BB#1:
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, (%r14)
	movq	$0, (%rbx)
	jmp	.LBB0_9
.LBB0_2:
	movapd	.LCPI0_0(%rip), %xmm0   # xmm0 = [nan,nan]
	movapd	%xmm1, %xmm2
	andpd	%xmm0, %xmm2
	andpd	%xmm3, %xmm0
	ucomisd	%xmm0, %xmm2
	jbe	.LBB0_6
# BB#3:
	xorpd	.LCPI0_1(%rip), %xmm3
	divsd	%xmm1, %xmm3
	movapd	%xmm3, %xmm1
	mulsd	%xmm1, %xmm1
	addsd	.LCPI0_2(%rip), %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_5
# BB#4:                                 # %call.sqrt
	movapd	%xmm1, %xmm0
	movapd	%xmm3, (%rsp)           # 16-byte Spill
	callq	sqrt
	movapd	(%rsp), %xmm3           # 16-byte Reload
.LBB0_5:                                # %.split
	movsd	.LCPI0_2(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, (%rbx)
	mulsd	%xmm1, %xmm3
	movsd	%xmm3, (%r14)
	jmp	.LBB0_9
.LBB0_6:
	xorpd	.LCPI0_1(%rip), %xmm1
	divsd	%xmm3, %xmm1
	movapd	%xmm1, %xmm2
	mulsd	%xmm2, %xmm2
	addsd	.LCPI0_2(%rip), %xmm2
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm2, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_8
# BB#7:                                 # %call.sqrt23
	movapd	%xmm2, %xmm0
	movapd	%xmm1, (%rsp)           # 16-byte Spill
	callq	sqrt
	movapd	(%rsp), %xmm1           # 16-byte Reload
.LBB0_8:                                # %.split22
	movsd	.LCPI0_2(%rip), %xmm2   # xmm2 = mem[0],zero
	divsd	%xmm0, %xmm2
	movsd	%xmm2, (%r14)
	mulsd	%xmm2, %xmm1
	movsd	%xmm1, (%rbx)
.LBB0_9:
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	Givens, .Lfunc_end0-Givens
	.cfi_endproc

	.globl	sign
	.p2align	4, 0x90
	.type	sign,@function
sign:                                   # @sign
	.cfi_startproc
# BB#0:
	xorps	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	movl	$-1, %ecx
	movl	$1, %eax
	cmoval	%ecx, %eax
	retq
.Lfunc_end1:
	.size	sign, .Lfunc_end1-sign
	.cfi_endproc

	.globl	ApplyRGivens
	.p2align	4, 0x90
	.type	ApplyRGivens,@function
ApplyRGivens:                           # @ApplyRGivens
	.cfi_startproc
# BB#0:
	movslq	%esi, %rax
	movslq	%edx, %rcx
	movl	$1, %edx
	jmp	.LBB2_1
	.p2align	4, 0x90
.LBB2_3:                                #   in Loop: Header=BB2_1 Depth=1
	movq	(%rdi,%rdx,8), %rsi
	movsd	(%rsi,%rax,8), %xmm2    # xmm2 = mem[0],zero
	movsd	(%rsi,%rcx,8), %xmm3    # xmm3 = mem[0],zero
	movapd	%xmm2, %xmm4
	mulsd	%xmm1, %xmm4
	movapd	%xmm3, %xmm5
	mulsd	%xmm0, %xmm5
	subsd	%xmm5, %xmm4
	movsd	%xmm4, (%rsi,%rax,8)
	mulsd	%xmm0, %xmm2
	mulsd	%xmm1, %xmm3
	addsd	%xmm2, %xmm3
	movsd	%xmm3, (%rsi,%rcx,8)
	addq	$2, %rdx
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movq	-8(%rdi,%rdx,8), %rsi
	movsd	(%rsi,%rax,8), %xmm2    # xmm2 = mem[0],zero
	movsd	(%rsi,%rcx,8), %xmm3    # xmm3 = mem[0],zero
	movapd	%xmm2, %xmm4
	mulsd	%xmm1, %xmm4
	movapd	%xmm3, %xmm5
	mulsd	%xmm0, %xmm5
	subsd	%xmm5, %xmm4
	movsd	%xmm4, (%rsi,%rax,8)
	mulsd	%xmm0, %xmm2
	mulsd	%xmm1, %xmm3
	addsd	%xmm2, %xmm3
	movsd	%xmm3, (%rsi,%rcx,8)
	cmpq	$51, %rdx
	jne	.LBB2_3
# BB#2:
	retq
.Lfunc_end2:
	.size	ApplyRGivens, .Lfunc_end2-ApplyRGivens
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
.LCPI3_3:
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
.LCPI3_4:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_1:
	.quad	4457293557087583675     # double 1.0E-10
.LCPI3_2:
	.quad	4602678819172646912     # double 0.5
.LCPI3_5:
	.quad	4607182418800017408     # double 1
	.text
	.globl	QRiterate
	.p2align	4, 0x90
	.type	QRiterate,@function
QRiterate:                              # @QRiterate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi11:
	.cfi_def_cfa_offset 288
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movapd	.LCPI3_0(%rip), %xmm8   # xmm8 = [nan,nan]
	movsd	.LCPI3_1(%rip), %xmm9   # xmm9 = mem[0],zero
	xorpd	%xmm10, %xmm10
	movsd	.LCPI3_5(%rip), %xmm11  # xmm11 = mem[0],zero
	xorpd	%xmm12, %xmm12
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	jmp	.LBB3_1
	.p2align	4, 0x90
.LBB3_9:                                # %.critedge180.thread.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	movabsq	$-4294967296, %rsi      # imm = 0xFFFFFFFF00000000
	movslq	%ecx, %rbp
	movq	%rbp, %rax
	shlq	$32, %rax
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movl	%ecx, %edx
	movq	%rbp, %r8
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB3_10:                               # %.critedge180.thread
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rbx
	movq	%rax, %rbp
	movl	%edx, %r10d
	testq	%rbx, %rbx
	jle	.LBB3_12
# BB#11:                                #   in Loop: Header=BB3_10 Depth=2
	leaq	-1(%rbx), %rcx
	movq	-8(%rdi,%rbx,8), %rax
	movsd	(%rax,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	leaq	(%rbp,%rsi), %rax
	leal	-1(%r10), %edx
	ucomisd	%xmm10, %xmm0
	jne	.LBB3_10
	jp	.LBB3_10
.LBB3_12:                               # %.critedge
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	-8(%rdi,%r8,8), %rax
	movsd	-8(%rax,%r8,8), %xmm3   # xmm3 = mem[0],zero
	movq	(%rdi,%r8,8), %rax
	movsd	(%rax,%r8,8), %xmm2     # xmm2 = mem[0],zero
	subsd	%xmm2, %xmm3
	mulsd	.LCPI3_2(%rip), %xmm3
	xorl	%ecx, %ecx
	ucomisd	%xmm3, %xmm10
	seta	%cl
	movsd	-8(%rax,%r8,8), %xmm4   # xmm4 = mem[0],zero
	mulsd	%xmm4, %xmm4
	movsd	.LCPI3_3(,%rcx,8), %xmm5 # xmm5 = mem[0],zero
	movapd	%xmm3, %xmm1
	mulsd	%xmm1, %xmm1
	addsd	%xmm4, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movq	%r10, 32(%rsp)          # 8-byte Spill
	jnp	.LBB3_14
# BB#13:                                # %call.sqrt
                                        #   in Loop: Header=BB3_1 Depth=1
	movapd	%xmm1, %xmm0
	movsd	%xmm2, 16(%rsp)         # 8-byte Spill
	movsd	%xmm3, 88(%rsp)         # 8-byte Spill
	movsd	%xmm4, 80(%rsp)         # 8-byte Spill
	movsd	%xmm5, 72(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	72(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	movsd	80(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movsd	88(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	16(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movq	32(%rsp), %r10          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	xorpd	%xmm12, %xmm12
	movsd	.LCPI3_5(%rip), %xmm11  # xmm11 = mem[0],zero
	xorpd	%xmm10, %xmm10
	movsd	.LCPI3_1(%rip), %xmm9   # xmm9 = mem[0],zero
	movapd	.LCPI3_0(%rip), %xmm8   # xmm8 = [nan,nan]
	movq	24(%rsp), %rdi          # 8-byte Reload
.LBB3_14:                               # %.critedge.split
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	104(%rsp), %rax         # 8-byte Reload
	leal	-1(%rax), %eax
	movl	%eax, 88(%rsp)          # 4-byte Spill
	mulsd	%xmm5, %xmm0
	addsd	%xmm0, %xmm3
	divsd	%xmm3, %xmm4
	subsd	%xmm4, %xmm2
	movq	%rbp, %r13
	sarq	$32, %r13
	sarq	$29, %rbp
	movq	(%rdi,%rbp), %rax
	movsd	(%rax,%rbp), %xmm13     # xmm13 = mem[0],zero
	subsd	%xmm2, %xmm13
	movq	%rbx, %rax
	incq	%rax
	movq	%r13, 80(%rsp)          # 8-byte Spill
	movl	%ebx, %r9d
	movq	%rbx, 136(%rsp)         # 8-byte Spill
	jmp	.LBB3_15
	.p2align	4, 0x90
.LBB3_16:                               # %.sink.split.split.preheader
                                        #   in Loop: Header=BB3_15 Depth=2
	movapd	%xmm13, %xmm15
	movapd	.LCPI3_4(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm0, %xmm15
	divsd	%xmm14, %xmm15
	movapd	%xmm15, 208(%rsp)       # 16-byte Spill
	mulsd	%xmm15, %xmm15
	addsd	%xmm11, %xmm15
	movapd	%xmm14, %xmm1
	xorpd	%xmm0, %xmm1
	divsd	%xmm13, %xmm1
	movapd	%xmm1, 192(%rsp)        # 16-byte Spill
	mulsd	%xmm1, %xmm1
	addsd	%xmm11, %xmm1
	movapd	%xmm1, 112(%rsp)        # 16-byte Spill
	xorl	%r14d, %r14d
	movl	%esi, %ebp
	movapd	%xmm13, 176(%rsp)       # 16-byte Spill
	movapd	%xmm14, 160(%rsp)       # 16-byte Spill
	movapd	%xmm15, 144(%rsp)       # 16-byte Spill
	.p2align	4, 0x90
.LBB3_17:                               # %.sink.split.split
                                        #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_15 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_65 Depth 4
                                        #         Child Loop BB3_54 Depth 4
                                        #         Child Loop BB3_72 Depth 4
                                        #         Child Loop BB3_75 Depth 4
	movq	%rdx, %r13
	cmpl	%r10d, %ebp
	movl	%r10d, %eax
	cmovgel	%ebp, %eax
	cltq
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leal	(%rsi,%r14), %eax
	movq	136(%rsp), %rbx         # 8-byte Reload
	cmpl	%ebx, %eax
	cmovll	%ebx, %eax
	movslq	%eax, %r9
	cmpl	%ebp, %ebx
	movl	%ebp, %eax
	cmovgel	%ebx, %eax
	movslq	%eax, %r12
	cmpq	%r8, %r13
	jge	.LBB3_1
# BB#18:                                #   in Loop: Header=BB3_17 Depth=3
	movapd	%xmm14, %xmm0
	andpd	%xmm8, %xmm0
	movapd	%xmm13, %xmm1
	andpd	%xmm8, %xmm1
	ucomisd	%xmm1, %xmm0
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movl	%ebp, 40(%rsp)          # 4-byte Spill
	jbe	.LBB3_48
# BB#19:                                #   in Loop: Header=BB3_17 Depth=3
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm15, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB3_21
# BB#20:                                # %call.sqrt440
                                        #   in Loop: Header=BB3_17 Depth=3
	movapd	%xmm15, %xmm0
	movq	%r9, 8(%rsp)            # 8-byte Spill
	callq	sqrt
	movq	8(%rsp), %r9            # 8-byte Reload
	movapd	144(%rsp), %xmm15       # 16-byte Reload
	movapd	160(%rsp), %xmm14       # 16-byte Reload
	movapd	176(%rsp), %xmm13       # 16-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	xorpd	%xmm12, %xmm12
	movsd	.LCPI3_5(%rip), %xmm11  # xmm11 = mem[0],zero
	xorpd	%xmm10, %xmm10
	movsd	.LCPI3_1(%rip), %xmm9   # xmm9 = mem[0],zero
	movapd	.LCPI3_0(%rip), %xmm8   # xmm8 = [nan,nan]
	movq	24(%rsp), %rdi          # 8-byte Reload
.LBB3_21:                               # %.split
                                        #   in Loop: Header=BB3_17 Depth=3
	movapd	%xmm11, %xmm2
	divsd	%xmm0, %xmm2
	movapd	208(%rsp), %xmm1        # 16-byte Reload
	mulsd	%xmm2, %xmm1
	jmp	.LBB3_51
	.p2align	4, 0x90
.LBB3_48:                               #   in Loop: Header=BB3_17 Depth=3
	movapd	112(%rsp), %xmm0        # 16-byte Reload
	sqrtsd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB3_50
# BB#49:                                # %call.sqrt442
                                        #   in Loop: Header=BB3_17 Depth=3
	movapd	112(%rsp), %xmm0        # 16-byte Reload
	movq	%r9, 8(%rsp)            # 8-byte Spill
	callq	sqrt
	movq	8(%rsp), %r9            # 8-byte Reload
	movapd	144(%rsp), %xmm15       # 16-byte Reload
	movapd	160(%rsp), %xmm14       # 16-byte Reload
	movapd	176(%rsp), %xmm13       # 16-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	xorpd	%xmm12, %xmm12
	movsd	.LCPI3_5(%rip), %xmm11  # xmm11 = mem[0],zero
	xorpd	%xmm10, %xmm10
	movsd	.LCPI3_1(%rip), %xmm9   # xmm9 = mem[0],zero
	movapd	.LCPI3_0(%rip), %xmm8   # xmm8 = [nan,nan]
	movq	24(%rsp), %rdi          # 8-byte Reload
.LBB3_50:                               # %.split441
                                        #   in Loop: Header=BB3_17 Depth=3
	movapd	%xmm11, %xmm1
	divsd	%xmm0, %xmm1
	movapd	192(%rsp), %xmm2        # 16-byte Reload
	mulsd	%xmm1, %xmm2
.LBB3_51:                               # %Givens.exit
                                        #   in Loop: Header=BB3_17 Depth=3
	leaq	-1(%r13), %r14
	cmpq	80(%rsp), %r14          # 8-byte Folded Reload
	cmovlel	%ebx, %r14d
	leaq	2(%r13), %rax
	cmpq	%rax, %r8
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	%eax, %r11d
	cmovll	104(%rsp), %r11d        # 4-byte Folded Reload
	cmpl	%r11d, %r14d
	jg	.LBB3_56
# BB#52:                                # %.lr.ph
                                        #   in Loop: Header=BB3_17 Depth=3
	movq	(%rdi,%r13,8), %rax
	movq	8(%rdi,%r13,8), %rdx
	movslq	%r11d, %rcx
	cmpq	%r9, %rcx
	movq	%r9, %r8
	cmovgeq	%rcx, %r8
	leaq	1(%r8), %r10
	subq	%r9, %r10
	cmpq	$1, %r10
	movq	%r12, %rsi
	jbe	.LBB3_53
# BB#57:                                # %min.iters.checked286
                                        #   in Loop: Header=BB3_17 Depth=3
	movq	%r10, %rbx
	andq	$-2, %rbx
	movq	%r12, %rsi
	je	.LBB3_53
# BB#58:                                # %vector.memcheck307
                                        #   in Loop: Header=BB3_17 Depth=3
	movq	%r9, 8(%rsp)            # 8-byte Spill
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	(%rax,%rsi,8), %r9
	cmpq	8(%rsp), %rcx           # 8-byte Folded Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	cmovgeq	%rcx, %rsi
	leaq	8(%rdx,%rsi,8), %rbp
	cmpq	%rbp, %r9
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	jae	.LBB3_60
# BB#59:                                # %vector.memcheck307
                                        #   in Loop: Header=BB3_17 Depth=3
	leaq	8(%rax,%rsi,8), %rsi
	leaq	(%rdx,%r9,8), %rbp
	cmpq	%rsi, %rbp
	movq	%r12, %rsi
	jb	.LBB3_53
.LBB3_60:                               # %vector.ph308
                                        #   in Loop: Header=BB3_17 Depth=3
	movapd	%xmm1, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movapd	%xmm2, %xmm3
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	leaq	-2(%rbx), %rsi
	movq	%rsi, %rbp
	shrq	%rbp
	btl	$1, %esi
	jb	.LBB3_61
# BB#62:                                # %vector.body278.prol
                                        #   in Loop: Header=BB3_17 Depth=3
	movupd	(%rax,%r12,8), %xmm4
	movupd	(%rdx,%r12,8), %xmm5
	movaps	%xmm0, %xmm6
	mulpd	%xmm4, %xmm6
	movaps	%xmm3, %xmm7
	mulpd	%xmm5, %xmm7
	subpd	%xmm7, %xmm6
	movupd	%xmm6, (%rax,%r12,8)
	mulpd	%xmm3, %xmm4
	mulpd	%xmm0, %xmm5
	addpd	%xmm4, %xmm5
	movupd	%xmm5, (%rdx,%r12,8)
	movl	$2, %r9d
	testq	%rbp, %rbp
	jne	.LBB3_64
	jmp	.LBB3_66
.LBB3_61:                               #   in Loop: Header=BB3_17 Depth=3
	xorl	%r9d, %r9d
	testq	%rbp, %rbp
	je	.LBB3_66
.LBB3_64:                               # %vector.ph308.new
                                        #   in Loop: Header=BB3_17 Depth=3
	movq	96(%rsp), %rsi          # 8-byte Reload
	cmpq	%rsi, %rcx
	movq	%rsi, %rbp
	cmovgeq	%rcx, %rbp
	incq	%rbp
	subq	%rsi, %rbp
	andq	$-2, %rbp
	subq	%r9, %rbp
	addq	%rsi, %r9
	leaq	16(%rdx,%r9,8), %rsi
	leaq	16(%rax,%r9,8), %r9
	.p2align	4, 0x90
.LBB3_65:                               # %vector.body278
                                        #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_15 Depth=2
                                        #       Parent Loop BB3_17 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movupd	-16(%r9), %xmm4
	movupd	-16(%rsi), %xmm5
	movapd	%xmm0, %xmm6
	mulpd	%xmm4, %xmm6
	movapd	%xmm3, %xmm7
	mulpd	%xmm5, %xmm7
	subpd	%xmm7, %xmm6
	movupd	%xmm6, -16(%r9)
	mulpd	%xmm3, %xmm4
	mulpd	%xmm0, %xmm5
	addpd	%xmm4, %xmm5
	movupd	%xmm5, -16(%rsi)
	movupd	(%r9), %xmm4
	movupd	(%rsi), %xmm5
	movapd	%xmm0, %xmm6
	mulpd	%xmm4, %xmm6
	movapd	%xmm3, %xmm7
	mulpd	%xmm5, %xmm7
	subpd	%xmm7, %xmm6
	movupd	%xmm6, (%r9)
	mulpd	%xmm3, %xmm4
	mulpd	%xmm0, %xmm5
	addpd	%xmm4, %xmm5
	movupd	%xmm5, (%rsi)
	addq	$32, %rsi
	addq	$32, %r9
	addq	$-4, %rbp
	jne	.LBB3_65
.LBB3_66:                               # %middle.block279
                                        #   in Loop: Header=BB3_17 Depth=3
	cmpq	%rbx, %r10
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %r9            # 8-byte Reload
	je	.LBB3_55
# BB#67:                                #   in Loop: Header=BB3_17 Depth=3
	addq	%r12, %rbx
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB3_53:                               # %scalar.ph280.preheader
                                        #   in Loop: Header=BB3_17 Depth=3
	decq	%rsi
	.p2align	4, 0x90
.LBB3_54:                               # %scalar.ph280
                                        #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_15 Depth=2
                                        #       Parent Loop BB3_17 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	8(%rax,%rsi,8), %xmm0   # xmm0 = mem[0],zero
	movsd	8(%rdx,%rsi,8), %xmm3   # xmm3 = mem[0],zero
	movapd	%xmm1, %xmm4
	mulsd	%xmm0, %xmm4
	movapd	%xmm2, %xmm5
	mulsd	%xmm3, %xmm5
	subsd	%xmm5, %xmm4
	movsd	%xmm4, 8(%rax,%rsi,8)
	mulsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm3
	addsd	%xmm0, %xmm3
	movsd	%xmm3, 8(%rdx,%rsi,8)
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB3_54
.LBB3_55:                               # %.preheader
                                        #   in Loop: Header=BB3_17 Depth=3
	cmpl	%r11d, %r14d
	jle	.LBB3_68
.LBB3_56:                               # %.preheader.._crit_edge_crit_edge
                                        #   in Loop: Header=BB3_17 Depth=3
	leaq	1(%r13), %rdx
	movq	32(%rsp), %r10          # 8-byte Reload
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	movl	40(%rsp), %ebp          # 4-byte Reload
	jmp	.LBB3_74
	.p2align	4, 0x90
.LBB3_68:                               # %.lr.ph207
                                        #   in Loop: Header=BB3_17 Depth=3
	testb	$1, %r10b
	je	.LBB3_70
# BB#69:                                #   in Loop: Header=BB3_17 Depth=3
	movq	(%rdi,%r12,8), %rax
	movsd	(%rax,%r13,8), %xmm0    # xmm0 = mem[0],zero
	movsd	8(%rax,%r13,8), %xmm3   # xmm3 = mem[0],zero
	movapd	%xmm1, %xmm4
	mulsd	%xmm0, %xmm4
	movapd	%xmm2, %xmm5
	mulsd	%xmm3, %xmm5
	subsd	%xmm5, %xmm4
	movsd	%xmm4, (%rax,%r13,8)
	mulsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm3
	addsd	%xmm0, %xmm3
	movsd	%xmm3, 8(%rax,%r13,8)
	incq	%r12
.LBB3_70:                               # %.prol.loopexit
                                        #   in Loop: Header=BB3_17 Depth=3
	movq	32(%rsp), %r10          # 8-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	movl	40(%rsp), %ebp          # 4-byte Reload
	leaq	1(%r13), %rdx
	cmpq	%r9, %r8
	je	.LBB3_73
# BB#71:                                # %.lr.ph207.new
                                        #   in Loop: Header=BB3_17 Depth=3
	decq	%r12
	.p2align	4, 0x90
.LBB3_72:                               #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_15 Depth=2
                                        #       Parent Loop BB3_17 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%rdi,%r12,8), %rax
	movsd	(%rax,%r13,8), %xmm0    # xmm0 = mem[0],zero
	movsd	8(%rax,%r13,8), %xmm3   # xmm3 = mem[0],zero
	movapd	%xmm1, %xmm4
	mulsd	%xmm0, %xmm4
	movapd	%xmm2, %xmm5
	mulsd	%xmm3, %xmm5
	subsd	%xmm5, %xmm4
	movsd	%xmm4, (%rax,%r13,8)
	mulsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm3
	addsd	%xmm0, %xmm3
	movsd	%xmm3, 8(%rax,%r13,8)
	movq	16(%rdi,%r12,8), %rax
	movsd	(%rax,%r13,8), %xmm0    # xmm0 = mem[0],zero
	movsd	8(%rax,%r13,8), %xmm3   # xmm3 = mem[0],zero
	movapd	%xmm1, %xmm4
	mulsd	%xmm0, %xmm4
	movapd	%xmm2, %xmm5
	mulsd	%xmm3, %xmm5
	subsd	%xmm5, %xmm4
	movsd	%xmm4, (%rax,%r13,8)
	mulsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm3
	addsd	%xmm0, %xmm3
	movsd	%xmm3, 8(%rax,%r13,8)
	addq	$2, %r12
	cmpq	%rcx, %r12
	jl	.LBB3_72
.LBB3_73:                               #   in Loop: Header=BB3_17 Depth=3
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
.LBB3_74:                               # %._crit_edge
                                        #   in Loop: Header=BB3_17 Depth=3
	movl	$1, %eax
	jmp	.LBB3_75
	.p2align	4, 0x90
.LBB3_79:                               #   in Loop: Header=BB3_75 Depth=4
	movq	(%r15,%rax,8), %rcx
	movsd	(%rcx,%r13,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rcx,%rdx,8), %xmm3    # xmm3 = mem[0],zero
	movapd	%xmm1, %xmm4
	mulsd	%xmm0, %xmm4
	movapd	%xmm2, %xmm5
	mulsd	%xmm3, %xmm5
	subsd	%xmm5, %xmm4
	movsd	%xmm4, (%rcx,%r13,8)
	mulsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm3
	addsd	%xmm0, %xmm3
	movsd	%xmm3, (%rcx,%rdx,8)
	addq	$2, %rax
.LBB3_75:                               #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_15 Depth=2
                                        #       Parent Loop BB3_17 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	-8(%r15,%rax,8), %rcx
	movsd	(%rcx,%r13,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rcx,%rdx,8), %xmm3    # xmm3 = mem[0],zero
	movapd	%xmm1, %xmm4
	mulsd	%xmm0, %xmm4
	movapd	%xmm2, %xmm5
	mulsd	%xmm3, %xmm5
	subsd	%xmm5, %xmm4
	movsd	%xmm4, (%rcx,%r13,8)
	mulsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm3
	addsd	%xmm0, %xmm3
	movsd	%xmm3, (%rcx,%rdx,8)
	cmpq	$51, %rax
	jne	.LBB3_79
# BB#76:                                # %ApplyRGivens.exit
                                        #   in Loop: Header=BB3_17 Depth=3
	incl	%ebp
	incl	%r14d
	cmpl	88(%rsp), %r8d          # 4-byte Folded Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	jge	.LBB3_17
.LBB3_77:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB3_15 Depth=2
	movq	(%rdi,%rdx,8), %rax
	movsd	(%rax,%r13,8), %xmm13   # xmm13 = mem[0],zero
	movl	%edx, %r9d
	movq	72(%rsp), %rax          # 8-byte Reload
.LBB3_15:                               # %.sink.split
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_17 Depth 3
                                        #         Child Loop BB3_65 Depth 4
                                        #         Child Loop BB3_54 Depth 4
                                        #         Child Loop BB3_72 Depth 4
                                        #         Child Loop BB3_75 Depth 4
                                        #       Child Loop BB3_23 Depth 3
                                        #         Child Loop BB3_45 Depth 4
                                        #         Child Loop BB3_27 Depth 4
                                        #         Child Loop BB3_33 Depth 4
                                        #         Child Loop BB3_35 Depth 4
	cltq
	movq	(%rdi,%rax,8), %rax
	movsd	(%rax,%r13,8), %xmm14   # xmm14 = mem[0],zero
	leal	-1(%r9), %esi
	movslq	%r9d, %rdx
	ucomisd	%xmm10, %xmm14
	movq	%r9, 64(%rsp)           # 8-byte Spill
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	jne	.LBB3_16
	jp	.LBB3_16
# BB#22:                                # %.sink.split.split.us.preheader
                                        #   in Loop: Header=BB3_15 Depth=2
	leal	-1(%r9), %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%ebx, %ebx
	movl	%esi, %r12d
	.p2align	4, 0x90
.LBB3_23:                               # %.sink.split.split.us
                                        #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_15 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_45 Depth 4
                                        #         Child Loop BB3_27 Depth 4
                                        #         Child Loop BB3_33 Depth 4
                                        #         Child Loop BB3_35 Depth 4
	movq	%rdx, %r13
	cmpl	%r10d, %r12d
	movl	%r10d, %eax
	cmovgel	%r12d, %eax
	movslq	%eax, %r11
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	(%rax,%rbx), %eax
	movq	136(%rsp), %rdx         # 8-byte Reload
	cmpl	%edx, %eax
	cmovll	%edx, %eax
	movslq	%eax, %rbp
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	leal	(%rsi,%rbx), %eax
	movq	%rdx, %rbx
	cmpl	%ebx, %eax
	cmovll	%ebx, %eax
	movslq	%eax, %rdx
	cmpl	%r12d, %ebx
	movl	%r12d, %eax
	cmovgel	%ebx, %eax
	movslq	%eax, %r14
	cmpq	%r8, %r13
	jge	.LBB3_1
# BB#24:                                # %Givens.exit.us
                                        #   in Loop: Header=BB3_23 Depth=3
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	leaq	-1(%r13), %rax
	cmpq	80(%rsp), %rax          # 8-byte Folded Reload
	cmovlel	%ebx, %eax
	movq	%r8, %rbp
	leaq	2(%r13), %rcx
	cmpq	%rcx, %rbp
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	%ecx, %ebx
	cmovll	104(%rsp), %ebx         # 4-byte Folded Reload
	cmpl	%ebx, %eax
	jg	.LBB3_34
# BB#25:                                # %.lr.ph.us
                                        #   in Loop: Header=BB3_23 Depth=3
	movq	(%rdi,%r13,8), %r8
	movq	8(%rdi,%r13,8), %r9
	movslq	%ebx, %rcx
	cmpq	%rdx, %rcx
	movq	%rdx, %rsi
	cmovgeq	%rcx, %rsi
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	incq	%rsi
	subq	%rdx, %rsi
	cmpq	$1, %rsi
	movq	%r14, %rdi
	jbe	.LBB3_26
# BB#37:                                # %min.iters.checked
                                        #   in Loop: Header=BB3_23 Depth=3
	movq	%rsi, %rbp
	andq	$-2, %rbp
	movq	%r14, %rdi
	je	.LBB3_26
# BB#38:                                # %vector.memcheck
                                        #   in Loop: Header=BB3_23 Depth=3
	leaq	(%r8,%rdx,8), %r10
	cmpq	%rdx, %rcx
	movq	%rdx, %rdi
	cmovgeq	%rcx, %rdi
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	leaq	8(%r9,%rdi,8), %rbp
	cmpq	%rbp, %r10
	movq	112(%rsp), %rbp         # 8-byte Reload
	movq	32(%rsp), %r10          # 8-byte Reload
	jae	.LBB3_40
# BB#39:                                # %vector.memcheck
                                        #   in Loop: Header=BB3_23 Depth=3
	leaq	8(%r8,%rdi,8), %rdi
	leaq	(%r9,%rdx,8), %rdx
	cmpq	%rdi, %rdx
	movq	%r14, %rdi
	jb	.LBB3_26
.LBB3_40:                               # %vector.body.preheader
                                        #   in Loop: Header=BB3_23 Depth=3
	leaq	-2(%rbp), %rdi
	movq	%rdi, %rdx
	shrq	%rdx
	btl	$1, %edi
	jb	.LBB3_41
# BB#42:                                # %vector.body.prol
                                        #   in Loop: Header=BB3_23 Depth=3
	movupd	(%r8,%r14,8), %xmm0
	movupd	(%r9,%r14,8), %xmm1
	movapd	%xmm0, %xmm2
	mulpd	%xmm12, %xmm0
	addpd	%xmm1, %xmm0
	mulpd	%xmm12, %xmm1
	subpd	%xmm1, %xmm2
	movupd	%xmm2, (%r8,%r14,8)
	movupd	%xmm0, (%r9,%r14,8)
	movl	$2, %edi
	testq	%rdx, %rdx
	jne	.LBB3_44
	jmp	.LBB3_46
.LBB3_41:                               #   in Loop: Header=BB3_23 Depth=3
	xorl	%edi, %edi
	testq	%rdx, %rdx
	je	.LBB3_46
.LBB3_44:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB3_23 Depth=3
	cmpq	%r11, %rcx
	movq	%r11, %r10
	cmovgeq	%rcx, %r10
	incq	%r10
	subq	%r11, %r10
	andq	$-2, %r10
	subq	%rdi, %r10
	addq	%r11, %rdi
	leaq	16(%r9,%rdi,8), %rdx
	leaq	16(%r8,%rdi,8), %rbp
	.p2align	4, 0x90
.LBB3_45:                               # %vector.body
                                        #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_15 Depth=2
                                        #       Parent Loop BB3_23 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movupd	-16(%rbp), %xmm0
	movupd	-16(%rdx), %xmm1
	movapd	%xmm0, %xmm2
	mulpd	%xmm12, %xmm0
	addpd	%xmm1, %xmm0
	mulpd	%xmm12, %xmm1
	subpd	%xmm1, %xmm2
	movupd	%xmm2, -16(%rbp)
	movupd	%xmm0, -16(%rdx)
	movupd	(%rbp), %xmm0
	movupd	(%rdx), %xmm1
	movapd	%xmm0, %xmm2
	mulpd	%xmm12, %xmm0
	addpd	%xmm1, %xmm0
	mulpd	%xmm12, %xmm1
	subpd	%xmm1, %xmm2
	movupd	%xmm2, (%rbp)
	movupd	%xmm0, (%rdx)
	addq	$32, %rdx
	addq	$32, %rbp
	addq	$-4, %r10
	jne	.LBB3_45
.LBB3_46:                               # %middle.block
                                        #   in Loop: Header=BB3_23 Depth=3
	movq	112(%rsp), %rdx         # 8-byte Reload
	cmpq	%rdx, %rsi
	movq	32(%rsp), %r10          # 8-byte Reload
	je	.LBB3_28
# BB#47:                                #   in Loop: Header=BB3_23 Depth=3
	addq	%r14, %rdx
	movq	%rdx, %rdi
	.p2align	4, 0x90
.LBB3_26:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB3_23 Depth=3
	decq	%rdi
	.p2align	4, 0x90
.LBB3_27:                               # %scalar.ph
                                        #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_15 Depth=2
                                        #       Parent Loop BB3_23 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	8(%r8,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	movsd	8(%r9,%rdi,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	mulsd	%xmm10, %xmm0
	addsd	%xmm1, %xmm0
	mulsd	%xmm10, %xmm1
	subsd	%xmm1, %xmm2
	movsd	%xmm2, 8(%r8,%rdi,8)
	movsd	%xmm0, 8(%r9,%rdi,8)
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB3_27
.LBB3_28:                               # %.preheader.us
                                        #   in Loop: Header=BB3_23 Depth=3
	movq	%rsi, %rdx
	cmpl	%ebx, %eax
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	64(%rsp), %r9           # 8-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
	jg	.LBB3_34
# BB#29:                                # %.lr.ph207.us
                                        #   in Loop: Header=BB3_23 Depth=3
	testb	$1, %dl
	je	.LBB3_31
# BB#30:                                #   in Loop: Header=BB3_23 Depth=3
	movq	(%rdi,%r14,8), %rax
	movupd	(%rax,%r13,8), %xmm0
	movapd	%xmm0, %xmm1
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	mulpd	%xmm12, %xmm1
	movapd	%xmm0, %xmm2
	subpd	%xmm1, %xmm2
	addpd	%xmm0, %xmm1
	movsd	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1]
	movupd	%xmm1, (%rax,%r13,8)
	incq	%r14
.LBB3_31:                               # %.prol.loopexit336
                                        #   in Loop: Header=BB3_23 Depth=3
	movq	96(%rsp), %rax          # 8-byte Reload
	cmpq	40(%rsp), %rax          # 8-byte Folded Reload
	je	.LBB3_34
# BB#32:                                # %.lr.ph207.us.new
                                        #   in Loop: Header=BB3_23 Depth=3
	decq	%r14
	.p2align	4, 0x90
.LBB3_33:                               #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_15 Depth=2
                                        #       Parent Loop BB3_23 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%rdi,%r14,8), %rax
	movupd	(%rax,%r13,8), %xmm0
	movapd	%xmm0, %xmm1
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	mulpd	%xmm12, %xmm1
	movapd	%xmm0, %xmm2
	subpd	%xmm1, %xmm2
	addpd	%xmm0, %xmm1
	movsd	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1]
	movupd	%xmm1, (%rax,%r13,8)
	movq	16(%rdi,%r14,8), %rax
	movupd	(%rax,%r13,8), %xmm0
	movapd	%xmm0, %xmm1
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	mulpd	%xmm12, %xmm1
	movapd	%xmm0, %xmm2
	subpd	%xmm1, %xmm2
	addpd	%xmm0, %xmm1
	movsd	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1]
	movupd	%xmm1, (%rax,%r13,8)
	addq	$2, %r14
	cmpq	%rcx, %r14
	jl	.LBB3_33
	.p2align	4, 0x90
.LBB3_34:                               # %._crit_edge.us
                                        #   in Loop: Header=BB3_23 Depth=3
	leaq	1(%r13), %rdx
	movl	$1, %eax
	movq	16(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB3_35
	.p2align	4, 0x90
.LBB3_80:                               #   in Loop: Header=BB3_35 Depth=4
	movq	(%r15,%rax,8), %rcx
	movsd	(%rcx,%r13,8), %xmm0    # xmm0 = mem[0],zero
	movsd	8(%rcx,%r13,8), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	mulsd	%xmm10, %xmm0
	addsd	%xmm1, %xmm0
	mulsd	%xmm10, %xmm1
	subsd	%xmm1, %xmm2
	movsd	%xmm2, (%rcx,%r13,8)
	movsd	%xmm0, 8(%rcx,%r13,8)
	addq	$2, %rax
.LBB3_35:                               #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_15 Depth=2
                                        #       Parent Loop BB3_23 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	-8(%r15,%rax,8), %rcx
	movsd	(%rcx,%r13,8), %xmm0    # xmm0 = mem[0],zero
	movsd	8(%rcx,%r13,8), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	mulsd	%xmm10, %xmm0
	addsd	%xmm1, %xmm0
	mulsd	%xmm10, %xmm1
	subsd	%xmm1, %xmm2
	movsd	%xmm2, (%rcx,%r13,8)
	movsd	%xmm0, 8(%rcx,%r13,8)
	cmpq	$51, %rax
	jne	.LBB3_80
# BB#36:                                # %ApplyRGivens.exit.us
                                        #   in Loop: Header=BB3_23 Depth=3
	incl	%r12d
	incl	%ebx
	cmpl	88(%rsp), %r9d          # 4-byte Folded Reload
	jge	.LBB3_23
	jmp	.LBB3_77
	.p2align	4, 0x90
.LBB3_1:                                # %.loopexit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_2 Depth 2
                                        #     Child Loop BB3_5 Depth 2
                                        #     Child Loop BB3_10 Depth 2
                                        #     Child Loop BB3_15 Depth 2
                                        #       Child Loop BB3_17 Depth 3
                                        #         Child Loop BB3_65 Depth 4
                                        #         Child Loop BB3_54 Depth 4
                                        #         Child Loop BB3_72 Depth 4
                                        #         Child Loop BB3_75 Depth 4
                                        #       Child Loop BB3_23 Depth 3
                                        #         Child Loop BB3_45 Depth 4
                                        #         Child Loop BB3_27 Depth 4
                                        #         Child Loop BB3_33 Depth 4
                                        #         Child Loop BB3_35 Depth 4
	movq	(%rdi), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_2:                                #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%rcx), %rdx
	movq	8(%rdi,%rcx,8), %rsi
	movsd	(%rsi,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	andpd	%xmm8, %xmm0
	movsd	(%rax,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	andpd	%xmm8, %xmm1
	movsd	8(%rsi,%rcx,8), %xmm2   # xmm2 = mem[0],zero
	andpd	%xmm8, %xmm2
	addsd	%xmm1, %xmm2
	mulsd	%xmm9, %xmm2
	ucomisd	%xmm0, %xmm2
	jbe	.LBB3_3
# BB#81:                                #   in Loop: Header=BB3_2 Depth=2
	movq	$0, 8(%rax,%rcx,8)
	movq	$0, (%rsi,%rcx,8)
.LBB3_3:                                # %.backedge
                                        #   in Loop: Header=BB3_2 Depth=2
	cmpq	$50, %rdx
	movq	%rsi, %rax
	movq	%rdx, %rcx
	jne	.LBB3_2
# BB#4:                                 # %.preheader198.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_5:                                # %.preheader198
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	50(%rax), %rcx
	testq	%rcx, %rcx
	jle	.LBB3_8
# BB#6:                                 #   in Loop: Header=BB3_5 Depth=2
	movq	392(%rdi,%rax,8), %rcx
	movsd	400(%rcx,%rax,8), %xmm0 # xmm0 = mem[0],zero
	decq	%rax
	ucomisd	%xmm10, %xmm0
	jne	.LBB3_7
	jnp	.LBB3_5
.LBB3_7:                                # %.critedge180.thread.preheader.loopexit
                                        #   in Loop: Header=BB3_1 Depth=1
	addl	$51, %eax
	movl	%eax, %ecx
	jmp	.LBB3_9
	.p2align	4, 0x90
.LBB3_8:                                # %.critedge180
                                        #   in Loop: Header=BB3_1 Depth=1
	cmpl	$-50, %eax
	jne	.LBB3_9
# BB#78:                                # %.critedge.thread
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	QRiterate, .Lfunc_end3-QRiterate
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
