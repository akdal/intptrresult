	.text
	.file	"rawcaudio.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	xorl	%edi, %edi
	movl	$sbuf, %esi
	movl	$2000, %edx             # imm = 0x7D0
	callq	read
	testl	%eax, %eax
	js	.LBB0_4
# BB#1:
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testl	%eax, %eax
	je	.LBB0_5
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	shrl	%eax
	movl	$sbuf, %edi
	movl	$abuf, %esi
	movl	$state, %ecx
	movl	%eax, %edx
	callq	adpcm_coder
	shrl	$2, %ebx
	movl	$1, %edi
	movl	$abuf, %esi
	movq	%rbx, %rdx
	callq	write
	xorl	%edi, %edi
	movl	$sbuf, %esi
	movl	$2000, %edx             # imm = 0x7D0
	callq	read
	testl	%eax, %eax
	movq	%rax, %rbx
	jns	.LBB0_2
.LBB0_4:                                # %._crit_edge
	movl	$.L.str, %edi
	callq	perror
	movl	$1, %edi
	callq	exit
.LBB0_5:
	movq	stderr(%rip), %rdi
	movswl	state(%rip), %edx
	movsbl	state+2(%rip), %ecx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	fprintf
	xorl	%edi, %edi
	callq	exit
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	sbuf,@object            # @sbuf
	.comm	sbuf,2000,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"input file"
	.size	.L.str, 11

	.type	abuf,@object            # @abuf
	.comm	abuf,500,16
	.type	state,@object           # @state
	.comm	state,4,2
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Final valprev=%d, index=%d\n"
	.size	.L.str.1, 28


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
