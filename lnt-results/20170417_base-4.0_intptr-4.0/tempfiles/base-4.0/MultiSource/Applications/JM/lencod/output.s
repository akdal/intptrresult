	.text
	.file	"output.bc"
	.globl	testEndian
	.p2align	4, 0x90
	.type	testEndian,@function
testEndian:                             # @testEndian
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	testEndian, .Lfunc_end0-testEndian
	.cfi_endproc

	.globl	img2buf
	.p2align	4, 0x90
	.type	img2buf,@function
img2buf:                                # @img2buf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 224
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movl	%ecx, %r14d
	movl	%edx, %r15d
	movq	%rsi, %rbx
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	240(%rsp), %r12d
	movl	232(%rsp), %edi
	movl	224(%rsp), %eax
	movl	%r15d, %ebp
	movq	%r9, %r13
	subl	%r9d, %ebp
	subl	%eax, %ebp
	cmpl	$3, %r8d
	movq	%r8, 40(%rsp)           # 8-byte Spill
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	jb	.LBB1_2
# BB#1:
	movl	%r14d, %eax
	subl	%edi, %eax
	subl	%r12d, %eax
	movl	%ebp, %ecx
	imull	%r8d, %ecx
	imull	%eax, %ecx
	movslq	%ecx, %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	memset
	movl	224(%rsp), %eax
	movl	232(%rsp), %edi
	movl	$2, %r8d
	jmp	.LBB1_3
.LBB1_2:
	movslq	%r8d, %r8
.LBB1_3:
	subl	%r12d, %r14d
	cmpl	%edi, %r14d
	movq	%r13, %rbx
	jle	.LBB1_13
# BB#4:                                 # %.preheader.lr.ph
	subl	%eax, %r15d
	movl	%r15d, %eax
	subl	%ebx, %eax
	jle	.LBB1_13
# BB#5:                                 # %.preheader.us.preheader
	movslq	%ebx, %rsi
	movslq	%edi, %rdi
	movslq	%r15d, %rdx
	movslq	%r14d, %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	leaq	-1(%rdx), %rcx
	subq	%rsi, %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	andl	$3, %edx
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	leaq	(%rsi,%rsi), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	imull	%ebp, %ecx
	movl	%ecx, 56(%rsp)          # 4-byte Spill
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	negq	%rdx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	negl	%ebx
	leal	(,%rax,4), %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movl	%ebp, 60(%rsp)          # 4-byte Spill
	movq	128(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_6:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_8 Depth 2
                                        #     Child Loop BB1_11 Depth 2
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	cmpq	$0, 120(%rsp)           # 8-byte Folded Reload
	movq	64(%rsp), %r12          # 8-byte Reload
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rdi, %r8
	movq	%r8, 32(%rsp)           # 8-byte Spill
	je	.LBB1_9
# BB#7:                                 # %.prol.preheader
                                        #   in Loop: Header=BB1_6 Depth=1
	movq	88(%rsp), %r14          # 8-byte Reload
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebx
	movq	96(%rsp), %rbp          # 8-byte Reload
	movq	64(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_8:                                #   Parent Loop BB1_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebx, %rbx
	leaq	(%r13,%rbx), %rdi
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%r8,8), %rsi
	addq	%rbp, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	incq	%r12
	addq	$2, %rbp
	addl	%r15d, %ebx
	incq	%r14
	jne	.LBB1_8
.LBB1_9:                                # %.prol.loopexit
                                        #   in Loop: Header=BB1_6 Depth=1
	cmpq	$3, 104(%rsp)           # 8-byte Folded Reload
	movl	52(%rsp), %ebx          # 4-byte Reload
	jb	.LBB1_12
# BB#10:                                # %.preheader.us.new
                                        #   in Loop: Header=BB1_6 Depth=1
	movq	80(%rsp), %r14          # 8-byte Reload
	subq	%r12, %r14
	leaq	6(%r12,%r12), %r15
	movq	72(%rsp), %rdx          # 8-byte Reload
	leal	3(%rdx,%r12), %edi
	movq	40(%rsp), %rax          # 8-byte Reload
	imull	%eax, %edi
	movq	%rdi, 160(%rsp)         # 8-byte Spill
	leal	2(%rdx,%r12), %edi
	imull	%eax, %edi
	movq	%rdi, 152(%rsp)         # 8-byte Spill
	movl	%r12d, %edi
	addl	%edx, %edi
	imull	%eax, %edi
	movq	%rdi, 144(%rsp)         # 8-byte Spill
	leal	1(%rdx,%r12), %edx
	imull	%eax, %edx
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB1_11:                               #   Parent Loop BB1_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	144(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r12), %eax
	movslq	%eax, %rdi
	addq	%r13, %rdi
	movq	(%rsi,%r8,8), %rax
	leaq	-6(%rax,%r15), %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movq	136(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r12), %eax
	movslq	%eax, %rdi
	addq	%r13, %rdi
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	leaq	-4(%rax,%r15), %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movq	152(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r12), %eax
	movslq	%eax, %rdi
	addq	%r13, %rdi
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	leaq	-2(%rax,%r15), %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movq	160(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r12), %eax
	movslq	%eax, %rdi
	addq	%r13, %rdi
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rax,8), %rsi
	addq	%r15, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	addq	$8, %r15
	addl	%ebx, %r12d
	addq	$-4, %r14
	jne	.LBB1_11
.LBB1_12:                               # %._crit_edge.us
                                        #   in Loop: Header=BB1_6 Depth=1
	incq	%r8
	movl	20(%rsp), %eax          # 4-byte Reload
	addl	56(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	60(%rsp), %ebp          # 4-byte Reload
	movq	72(%rsp), %rbx          # 8-byte Reload
	addl	%ebp, %ebx
	cmpq	112(%rsp), %r8          # 8-byte Folded Reload
	movq	%r8, %rdi
	jne	.LBB1_6
.LBB1_13:                               # %._crit_edge125
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	img2buf, .Lfunc_end1-img2buf
	.cfi_endproc

	.globl	write_picture
	.p2align	4, 0x90
	.type	write_picture,@function
write_picture:                          # @write_picture
	.cfi_startproc
# BB#0:
	jmp	write_out_picture       # TAILCALL
.Lfunc_end2:
	.size	write_picture, .Lfunc_end2-write_picture
	.cfi_endproc

	.globl	write_out_picture
	.p2align	4, 0x90
	.type	write_out_picture,@function
write_out_picture:                      # @write_out_picture
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 272
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movq	img(%rip), %rax
	movl	15440(%rax), %eax
	movq	input(%rip), %rcx
	cmpl	$0, 5272(%rcx)
	je	.LBB3_1
# BB#2:
	cmpl	$3, 64(%rcx)
	sete	%bpl
	cmpl	$-1, %r14d
	jne	.LBB3_4
	jmp	.LBB3_70
.LBB3_1:
	xorl	%ebp, %ebp
	cmpl	$-1, %r14d
	je	.LBB3_70
.LBB3_4:
	movl	6388(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.LBB3_70
# BB#5:
	movl	%eax, %r13d
	sarl	$31, %r13d
	shrl	$29, %r13d
	addl	%eax, %r13d
	sarl	$3, %r13d
	cmpl	$0, 6568(%rbx)
	je	.LBB3_6
# BB#7:
	movslq	6560(%rbx), %rax
	movl	.Lwrite_out_picture.SubWidthC(,%rax,4), %r12d
	movl	6572(%rbx), %r15d
	imull	%r12d, %r15d
	imull	6576(%rbx), %r12d
	movl	$2, %ecx
	subl	6564(%rbx), %ecx
	imull	.Lwrite_out_picture.SubHeightC(,%rax,4), %ecx
	movl	6580(%rbx), %eax
	imull	%ecx, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	imull	6584(%rbx), %ecx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	jmp	.LBB3_8
.LBB3_70:
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_6:
	xorl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
.LBB3_8:
	movl	6392(%rbx), %eax
	imull	%r13d, %eax
	imull	6396(%rbx), %eax
	movslq	%eax, %rdi
	callq	malloc
	movq	%rax, 112(%rsp)         # 8-byte Spill
	testq	%rax, %rax
	jne	.LBB3_10
# BB#9:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB3_10:
	movl	%ebp, %edx
	testb	%dl, %dl
	movl	%r14d, 124(%rsp)        # 4-byte Spill
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	%rbx, 208(%rsp)         # 8-byte Spill
	movq	72(%rsp), %r14          # 8-byte Reload
	movl	%edx, 204(%rsp)         # 4-byte Spill
	je	.LBB3_27
# BB#11:
	movslq	6572(%rbx), %r8
	movl	6576(%rbx), %esi
	movslq	6564(%rbx), %rax
	movl	$2, %ecx
	subq	%rax, %rcx
	movslq	6580(%rbx), %r14
	imulq	%rcx, %r14
	movl	6584(%rbx), %ebp
	imull	%ecx, %ebp
	movq	6472(%rbx), %rax
	movq	8(%rax), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	6400(%rbx), %eax
	movl	6404(%rbx), %ebx
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movl	%eax, %edi
	movq	%r8, 56(%rsp)           # 8-byte Spill
	subl	%r8d, %edi
	movl	%esi, 72(%rsp)          # 4-byte Spill
	subl	%esi, %edi
	cmpl	$3, %r13d
	movl	%edi, 96(%rsp)          # 4-byte Spill
	jb	.LBB3_13
# BB#12:
	leal	(%rbp,%r14), %eax
	movl	%ebx, %ecx
	subl	%eax, %ecx
	movl	%edi, %eax
	imull	%r13d, %eax
	imull	%ecx, %eax
	movslq	%eax, %rdx
	xorl	%esi, %esi
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	memset
	movl	96(%rsp), %edi          # 4-byte Reload
	movl	$2, %r13d
	jmp	.LBB3_14
.LBB3_13:
	movslq	%r13d, %r13
.LBB3_14:
	movl	%ebx, 128(%rsp)         # 4-byte Spill
	movl	%ebx, %eax
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	subl	%ebp, %eax
	movq	%r14, 104(%rsp)         # 8-byte Spill
	cmpl	%r14d, %eax
	jle	.LBB3_24
# BB#15:                                # %.preheader.lr.ph.i
	movl	40(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %edx
	subl	72(%rsp), %edx          # 4-byte Folded Reload
	movq	%rdx, %rcx
	cmpl	56(%rsp), %edx          # 4-byte Folded Reload
	jle	.LBB3_24
# BB#16:                                # %.preheader.us.preheader.i
	movq	%rcx, %rdx
	movslq	%edx, %rsi
	cltq
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movq	56(%rsp), %rcx          # 8-byte Reload
	subl	%ecx, %edx
	movq	%rsi, 152(%rsp)         # 8-byte Spill
	leaq	-1(%rsi), %rax
	subq	%rcx, %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	andl	$3, %edx
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, %esi
	imull	%edi, %esi
	movl	%esi, 168(%rsp)         # 4-byte Spill
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	negq	%rdx
	movq	%rdx, 160(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	negl	%ecx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	leal	(,%rax,4), %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movq	104(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB3_17:                               # %.preheader.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_19 Depth 2
                                        #     Child Loop BB3_22 Depth 2
	cmpq	$0, 136(%rsp)           # 8-byte Folded Reload
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	112(%rsp), %rsi         # 8-byte Reload
	je	.LBB3_20
# BB#18:                                # %.prol.preheader207
                                        #   in Loop: Header=BB3_17 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rcx,%rax,2), %rbx
	movq	160(%rsp), %r15         # 8-byte Reload
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, %ebp
	movq	%rax, %r14
	.p2align	4, 0x90
.LBB3_19:                               #   Parent Loop BB3_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movslq	%ebp, %rbp
	leaq	(%rsi,%rbp), %rdi
	movq	%rsi, %r14
	movq	%rbx, %rsi
	movq	%r13, %rdx
	callq	memcpy
	movq	%r14, %rsi
	movq	24(%rsp), %r14          # 8-byte Reload
	incq	%r14
	addq	$2, %rbx
	addl	%r12d, %ebp
	incq	%r15
	jne	.LBB3_19
.LBB3_20:                               # %.prol.loopexit208
                                        #   in Loop: Header=BB3_17 Depth=1
	cmpq	$3, 176(%rsp)           # 8-byte Folded Reload
	movq	%rsi, %rbp
	movq	%r13, %r12
	jb	.LBB3_23
# BB#21:                                # %.preheader.us.i.new
                                        #   in Loop: Header=BB3_17 Depth=1
	movq	152(%rsp), %r15         # 8-byte Reload
	subq	%r14, %r15
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	6(%rax,%r14,2), %rbx
	movq	64(%rsp), %rcx          # 8-byte Reload
	leal	3(%rcx,%r14), %esi
	movq	16(%rsp), %rax          # 8-byte Reload
	imull	%eax, %esi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	leal	2(%rcx,%r14), %esi
	imull	%eax, %esi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leal	1(%rcx,%r14), %esi
	imull	%eax, %esi
	movq	%rsi, 192(%rsp)         # 8-byte Spill
	addl	%ecx, %r14d
	imull	%eax, %r14d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB3_22:                               #   Parent Loop BB3_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r14,%r13), %eax
	movslq	%eax, %rdi
	addq	%rbp, %rdi
	leaq	-6(%rbx), %rsi
	movq	%r12, %rdx
	callq	memcpy
	movq	192(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r13), %eax
	movslq	%eax, %rdi
	addq	%rbp, %rdi
	leaq	-4(%rbx), %rsi
	movq	%r12, %rdx
	callq	memcpy
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	(%rax,%r13), %eax
	movslq	%eax, %rdi
	addq	%rbp, %rdi
	leaq	-2(%rbx), %rsi
	movq	%r12, %rdx
	callq	memcpy
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r13), %eax
	movslq	%eax, %rdi
	addq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	callq	memcpy
	movl	36(%rsp), %edx          # 4-byte Reload
	addq	$8, %rbx
	addl	%edx, %r13d
	addq	$-4, %r15
	jne	.LBB3_22
.LBB3_23:                               # %._crit_edge.us.i
                                        #   in Loop: Header=BB3_17 Depth=1
	movq	80(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	movl	4(%rsp), %eax           # 4-byte Reload
	addl	168(%rsp), %eax         # 4-byte Folded Reload
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
	addl	96(%rsp), %eax          # 4-byte Folded Reload
	movq	%rax, 64(%rsp)          # 8-byte Spill
	cmpq	184(%rsp), %rcx         # 8-byte Folded Reload
	movq	%r12, %r13
	jne	.LBB3_17
.LBB3_24:                               # %img2buf.exit
	movq	88(%rsp), %rax          # 8-byte Reload
	addl	104(%rsp), %eax         # 4-byte Folded Reload
	movl	128(%rsp), %edx         # 4-byte Reload
	subl	%eax, %edx
	movl	72(%rsp), %eax          # 4-byte Reload
	addl	56(%rsp), %eax          # 4-byte Folded Reload
	movl	40(%rsp), %ecx          # 4-byte Reload
	subl	%eax, %ecx
	movq	16(%rsp), %r13          # 8-byte Reload
	imull	%r13d, %edx
	imull	%ecx, %edx
	movslq	%edx, %rdx
	movl	124(%rsp), %edi         # 4-byte Reload
	movq	112(%rsp), %rsi         # 8-byte Reload
	callq	write
	movq	208(%rsp), %rbx         # 8-byte Reload
	cmpl	$0, 6568(%rbx)
	je	.LBB3_25
# BB#26:
	movslq	6560(%rbx), %rax
	movl	.Lwrite_out_picture.SubWidthC(,%rax,4), %r12d
	movl	6572(%rbx), %r15d
	imull	%r12d, %r15d
	imull	6576(%rbx), %r12d
	movl	$2, %r14d
	subl	6564(%rbx), %r14d
	imull	.Lwrite_out_picture.SubHeightC(,%rax,4), %r14d
	movl	6580(%rbx), %eax
	imull	%r14d, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	imull	6584(%rbx), %r14d
	jmp	.LBB3_27
.LBB3_25:
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
.LBB3_27:
	movl	%r15d, 104(%rsp)        # 4-byte Spill
	movq	6440(%rbx), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	6392(%rbx), %eax
	movl	6396(%rbx), %ecx
	movl	%eax, 88(%rsp)          # 4-byte Spill
	movl	%eax, %ebp
	subl	%r15d, %ebp
	movl	%r12d, 128(%rsp)        # 4-byte Spill
	subl	%r12d, %ebp
	cmpl	$3, %r13d
	movl	%ecx, 52(%rsp)          # 4-byte Spill
	movl	%ebp, 96(%rsp)          # 4-byte Spill
	jb	.LBB3_29
# BB#28:
	movq	40(%rsp), %r15          # 8-byte Reload
	leal	(%r15,%r14), %eax
	subl	%eax, %ecx
	movl	%ebp, %eax
	imull	%r13d, %eax
	imull	%ecx, %eax
	movslq	%eax, %rdx
	xorl	%esi, %esi
	movq	112(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdi
	callq	memset
	movl	96(%rsp), %ebp          # 4-byte Reload
	movl	52(%rsp), %ecx          # 4-byte Reload
	movl	$2, %edx
	jmp	.LBB3_30
.LBB3_29:
	movslq	%r13d, %rdx
	movq	112(%rsp), %rbx         # 8-byte Reload
	movq	40(%rsp), %r15          # 8-byte Reload
.LBB3_30:
	movl	%ecx, %eax
	movq	%r14, 72(%rsp)          # 8-byte Spill
	subl	%r14d, %eax
	cmpl	%r15d, %eax
	jle	.LBB3_40
# BB#31:                                # %.preheader.lr.ph.i166
	movl	88(%rsp), %ecx          # 4-byte Reload
	subl	128(%rsp), %ecx         # 4-byte Folded Reload
	movl	%ecx, %edi
	subl	104(%rsp), %edi         # 4-byte Folded Reload
	jle	.LBB3_40
# BB#32:                                # %.preheader.us.preheader.i169
	movl	104(%rsp), %r8d         # 4-byte Reload
	movslq	%r8d, %r9
	movslq	40(%rsp), %rsi          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	cltq
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	leaq	-1(%rcx), %rax
	movq	%r9, 56(%rsp)           # 8-byte Spill
	subq	%r9, %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	andl	$3, %edi
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	imull	%ebp, %ecx
	movl	%ecx, 168(%rsp)         # 4-byte Spill
	movq	%rdi, 136(%rsp)         # 8-byte Spill
	negq	%rdi
	movq	%rdi, 160(%rsp)         # 8-byte Spill
	movl	%r8d, %ecx
	negl	%ecx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	leal	(,%rax,4), %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB3_33:                               # %.preheader.us.i171
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_35 Depth 2
                                        #     Child Loop BB3_38 Depth 2
	cmpq	$0, 136(%rsp)           # 8-byte Folded Reload
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	(%rax,%rsi,8), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	%rbx, %rsi
	je	.LBB3_36
# BB#34:                                # %.prol.preheader202
                                        #   in Loop: Header=BB3_33 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rcx,%rax,2), %rbx
	movq	160(%rsp), %r13         # 8-byte Reload
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, %ebp
	movq	%rax, %r15
	.p2align	4, 0x90
.LBB3_35:                               #   Parent Loop BB3_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movslq	%ebp, %rbp
	leaq	(%rsi,%rbp), %rdi
	movq	%rsi, %r15
	movq	%rbx, %rsi
	movq	%rdx, %r14
	callq	memcpy
	movq	%r15, %rsi
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	%r14, %rdx
	incq	%r15
	addq	$2, %rbx
	addl	%r12d, %ebp
	incq	%r13
	jne	.LBB3_35
.LBB3_36:                               # %.prol.loopexit203
                                        #   in Loop: Header=BB3_33 Depth=1
	cmpq	$3, 176(%rsp)           # 8-byte Folded Reload
	movq	%rdx, %rbp
	movq	%rsi, %r12
	jb	.LBB3_39
# BB#37:                                # %.preheader.us.i171.new
                                        #   in Loop: Header=BB3_33 Depth=1
	movq	152(%rsp), %r13         # 8-byte Reload
	subq	%r15, %r13
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	6(%rax,%r15,2), %rbx
	movq	64(%rsp), %rcx          # 8-byte Reload
	leal	3(%rcx,%r15), %esi
	movq	16(%rsp), %rax          # 8-byte Reload
	imull	%eax, %esi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	leal	2(%rcx,%r15), %esi
	imull	%eax, %esi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leal	1(%rcx,%r15), %esi
	imull	%eax, %esi
	movq	%rsi, 192(%rsp)         # 8-byte Spill
	addl	%ecx, %r15d
	imull	%eax, %r15d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB3_38:                               #   Parent Loop BB3_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r15,%r14), %eax
	movslq	%eax, %rdi
	addq	%r12, %rdi
	leaq	-6(%rbx), %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	192(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14), %eax
	movslq	%eax, %rdi
	addq	%r12, %rdi
	leaq	-4(%rbx), %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	(%rax,%r14), %eax
	movslq	%eax, %rdi
	addq	%r12, %rdi
	leaq	-2(%rbx), %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r14), %eax
	movslq	%eax, %rdi
	addq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movl	36(%rsp), %edx          # 4-byte Reload
	addq	$8, %rbx
	addl	%edx, %r14d
	addq	$-4, %r13
	jne	.LBB3_38
.LBB3_39:                               # %._crit_edge.us.i177
                                        #   in Loop: Header=BB3_33 Depth=1
	movq	80(%rsp), %rsi          # 8-byte Reload
	incq	%rsi
	movl	4(%rsp), %eax           # 4-byte Reload
	addl	168(%rsp), %eax         # 4-byte Folded Reload
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
	addl	96(%rsp), %eax          # 4-byte Folded Reload
	movq	%rax, 64(%rsp)          # 8-byte Spill
	cmpq	184(%rsp), %rsi         # 8-byte Folded Reload
	movq	%r12, %rbx
	movq	%rbp, %rdx
	jne	.LBB3_33
.LBB3_40:                               # %img2buf.exit178
	movq	40(%rsp), %rax          # 8-byte Reload
	addl	72(%rsp), %eax          # 4-byte Folded Reload
	movl	52(%rsp), %edx          # 4-byte Reload
	subl	%eax, %edx
	movl	104(%rsp), %eax         # 4-byte Reload
	addl	128(%rsp), %eax         # 4-byte Folded Reload
	movl	88(%rsp), %ecx          # 4-byte Reload
	subl	%eax, %ecx
	movq	16(%rsp), %r14          # 8-byte Reload
	imull	%r14d, %edx
	imull	%ecx, %edx
	movslq	%edx, %rdx
	movl	124(%rsp), %edi         # 4-byte Reload
	movq	%rbx, %rsi
	callq	write
	movq	208(%rsp), %rdx         # 8-byte Reload
	cmpl	$0, 6560(%rdx)
	movq	%rbx, %rbp
	je	.LBB3_69
# BB#41:
	movslq	6572(%rdx), %rbx
	movl	6576(%rdx), %esi
	movslq	6564(%rdx), %rax
	movl	$2, %ecx
	subq	%rax, %rcx
	movslq	6580(%rdx), %r15
	imulq	%rcx, %r15
	movl	6584(%rdx), %r12d
	imull	%ecx, %r12d
	movq	6472(%rdx), %rax
	movq	(%rax), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	6400(%rdx), %eax
	movl	6404(%rdx), %r13d
	movl	%eax, 88(%rsp)          # 4-byte Spill
	movl	%eax, %edx
	subl	%ebx, %edx
	subl	%esi, %edx
	cmpl	$3, %r14d
	movl	%edx, 96(%rsp)          # 4-byte Spill
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	jb	.LBB3_43
# BB#42:
	leal	(%r12,%r15), %eax
	movl	%r13d, %ecx
	subl	%eax, %ecx
	movl	%edx, %eax
	imull	%r14d, %eax
	imull	%ecx, %eax
	movslq	%eax, %rdx
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	memset
	movl	96(%rsp), %edx          # 4-byte Reload
	movl	$2, %eax
	jmp	.LBB3_44
.LBB3_43:
	movslq	%r14d, %rax
.LBB3_44:
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	%r13d, 52(%rsp)         # 4-byte Spill
	movl	%r13d, %eax
	movq	%r12, 128(%rsp)         # 8-byte Spill
	subl	%r12d, %eax
	movq	%r15, 40(%rsp)          # 8-byte Spill
	cmpl	%r15d, %eax
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	jle	.LBB3_54
# BB#45:                                # %.preheader.lr.ph.i151
	movl	88(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %esi
	subl	104(%rsp), %esi         # 4-byte Folded Reload
	movq	%rsi, %rcx
	cmpl	%ebx, %esi
	jle	.LBB3_54
# BB#46:                                # %.preheader.us.preheader.i154
	movslq	%ecx, %rsi
	cltq
	movq	%rax, 176(%rsp)         # 8-byte Spill
	subl	%ebx, %ecx
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	leaq	-1(%rsi), %rax
	subq	%rbx, %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	andl	$3, %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, %esi
	imull	%edx, %esi
	movl	%esi, 160(%rsp)         # 4-byte Spill
	movq	%rcx, 184(%rsp)         # 8-byte Spill
	negq	%rcx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movl	%ebx, %ecx
	negl	%ecx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	leal	(,%rax,4), %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movq	40(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_47:                               # %.preheader.us.i156
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_49 Depth 2
                                        #     Child Loop BB3_52 Depth 2
	cmpq	$0, 184(%rsp)           # 8-byte Folded Reload
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbx, %r14
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	112(%rsp), %r13         # 8-byte Reload
	movq	136(%rsp), %r15         # 8-byte Reload
	movq	%r15, %rdx
	je	.LBB3_50
# BB#48:                                # %.prol.preheader197
                                        #   in Loop: Header=BB3_47 Depth=1
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rcx,%rsi,2), %rbx
	movq	152(%rsp), %r12         # 8-byte Reload
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, %ebp
	movq	%rsi, %r14
	.p2align	4, 0x90
.LBB3_49:                               #   Parent Loop BB3_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movslq	%ebp, %rbp
	leaq	(%r13,%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rax, %r15
	movq	%r13, %r14
	movq	%rdx, %r13
	callq	memcpy
	movq	%r13, %rdx
	movq	%r14, %r13
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	%r15, %rax
	incq	%r14
	addq	$2, %rbx
	addl	%eax, %ebp
	incq	%r12
	jne	.LBB3_49
.LBB3_50:                               # %.prol.loopexit198
                                        #   in Loop: Header=BB3_47 Depth=1
	cmpq	$3, 168(%rsp)           # 8-byte Folded Reload
	movq	%rdx, %r12
	jb	.LBB3_53
# BB#51:                                # %.preheader.us.i156.new
                                        #   in Loop: Header=BB3_47 Depth=1
	movq	72(%rsp), %rbp          # 8-byte Reload
	subq	%r14, %rbp
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	6(%rax,%r14,2), %rbx
	movq	80(%rsp), %rcx          # 8-byte Reload
	leal	3(%rcx,%r14), %esi
	movq	16(%rsp), %rax          # 8-byte Reload
	imull	%eax, %esi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	leal	2(%rcx,%r14), %esi
	imull	%eax, %esi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leal	1(%rcx,%r14), %esi
	imull	%eax, %esi
	movq	%rsi, 192(%rsp)         # 8-byte Spill
	addl	%ecx, %r14d
	imull	%eax, %r14d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB3_52:                               #   Parent Loop BB3_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r14,%r15), %eax
	movslq	%eax, %rdi
	addq	%r13, %rdi
	leaq	-6(%rbx), %rsi
	movq	%r12, %rdx
	callq	memcpy
	movq	192(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r15), %eax
	movslq	%eax, %rdi
	addq	%r13, %rdi
	leaq	-4(%rbx), %rsi
	movq	%r12, %rdx
	callq	memcpy
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	(%rax,%r15), %eax
	movslq	%eax, %rdi
	addq	%r13, %rdi
	leaq	-2(%rbx), %rsi
	movq	%r12, %rdx
	callq	memcpy
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r15), %eax
	movslq	%eax, %rdi
	addq	%r13, %rdi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	callq	memcpy
	movl	36(%rsp), %edx          # 4-byte Reload
	addq	$8, %rbx
	addl	%edx, %r15d
	addq	$-4, %rbp
	jne	.LBB3_52
.LBB3_53:                               # %._crit_edge.us.i162
                                        #   in Loop: Header=BB3_47 Depth=1
	movq	56(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	movl	4(%rsp), %eax           # 4-byte Reload
	addl	160(%rsp), %eax         # 4-byte Folded Reload
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	80(%rsp), %rax          # 8-byte Reload
	addl	96(%rsp), %eax          # 4-byte Folded Reload
	movq	%rax, 80(%rsp)          # 8-byte Spill
	cmpq	176(%rsp), %rcx         # 8-byte Folded Reload
	movq	64(%rsp), %rbx          # 8-byte Reload
	jne	.LBB3_47
.LBB3_54:                               # %img2buf.exit163
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	128(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%rax), %r12d
	movl	52(%rsp), %ecx          # 4-byte Reload
	subl	%r12d, %ecx
	movq	104(%rsp), %r15         # 8-byte Reload
	leal	(%r15,%rbx), %r13d
	movl	88(%rsp), %eax          # 4-byte Reload
	subl	%r13d, %eax
	movq	16(%rsp), %r14          # 8-byte Reload
	imull	%r14d, %ecx
	imull	%eax, %ecx
	movslq	%ecx, %rdx
	movl	124(%rsp), %edi         # 4-byte Reload
	movq	112(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rsi
	callq	write
	cmpb	$0, 204(%rsp)           # 1-byte Folded Reload
	movq	208(%rsp), %rcx         # 8-byte Reload
	jne	.LBB3_69
# BB#55:
	movq	%rbp, %rdi
	movl	%r13d, 52(%rsp)         # 4-byte Spill
	movl	%r12d, 88(%rsp)         # 4-byte Spill
	movq	6472(%rcx), %rax
	movq	8(%rax), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	6400(%rcx), %eax
	movl	6404(%rcx), %r12d
	movl	%eax, 208(%rsp)         # 4-byte Spill
	movl	%eax, %r13d
	subl	%ebx, %r13d
	subl	%r15d, %r13d
	cmpl	$3, %r14d
	jb	.LBB3_57
# BB#56:
	movl	%r12d, %eax
	subl	88(%rsp), %eax          # 4-byte Folded Reload
	movl	%r13d, %ecx
	imull	%r14d, %ecx
	imull	%eax, %ecx
	movslq	%ecx, %rdx
	xorl	%esi, %esi
	callq	memset
	movl	$2, %eax
	jmp	.LBB3_58
.LBB3_57:
	movslq	%r14d, %rax
.LBB3_58:
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	128(%rsp), %rcx         # 8-byte Reload
	movl	%r12d, 204(%rsp)        # 4-byte Spill
	movl	%r12d, %eax
	subl	%ecx, %eax
	movq	40(%rsp), %rsi          # 8-byte Reload
	cmpl	%esi, %eax
	jle	.LBB3_68
# BB#59:                                # %.preheader.lr.ph.i136
	movl	208(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, %edx
	subl	%r15d, %edx
	movq	%rdx, %rcx
	cmpl	%ebx, %edx
	jle	.LBB3_68
# BB#60:                                # %.preheader.us.preheader.i139
	movslq	%esi, %rdx
	movslq	%ecx, %rsi
	cltq
	movq	%rax, 176(%rsp)         # 8-byte Spill
	subl	%ebx, %ecx
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	leaq	-1(%rsi), %rax
	subq	%rbx, %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	andl	$3, %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, %esi
	imull	%r13d, %esi
	movl	%esi, 160(%rsp)         # 4-byte Spill
	movq	%rcx, 184(%rsp)         # 8-byte Spill
	negq	%rcx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movl	%ebx, %ecx
	negl	%ecx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	leal	(,%rax,4), %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movl	%r13d, 136(%rsp)        # 4-byte Spill
	.p2align	4, 0x90
.LBB3_61:                               # %.preheader.us.i141
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_63 Depth 2
                                        #     Child Loop BB3_66 Depth 2
	cmpq	$0, 184(%rsp)           # 8-byte Folded Reload
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	(%rax,%rdx,8), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbx, %r14
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	112(%rsp), %r13         # 8-byte Reload
	movq	144(%rsp), %r15         # 8-byte Reload
	movq	%r15, %rdx
	je	.LBB3_64
# BB#62:                                # %.prol.preheader
                                        #   in Loop: Header=BB3_61 Depth=1
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rcx,%rsi,2), %rbx
	movq	152(%rsp), %r12         # 8-byte Reload
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, %ebp
	movq	%rsi, %r14
	.p2align	4, 0x90
.LBB3_63:                               #   Parent Loop BB3_61 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movslq	%ebp, %rbp
	leaq	(%r13,%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rax, %r15
	movq	%r13, %r14
	movq	%rdx, %r13
	callq	memcpy
	movq	%r13, %rdx
	movq	%r14, %r13
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	%r15, %rax
	incq	%r14
	addq	$2, %rbx
	addl	%eax, %ebp
	incq	%r12
	jne	.LBB3_63
.LBB3_64:                               # %.prol.loopexit
                                        #   in Loop: Header=BB3_61 Depth=1
	cmpq	$3, 168(%rsp)           # 8-byte Folded Reload
	movq	%rdx, %r12
	jb	.LBB3_67
# BB#65:                                # %.preheader.us.i141.new
                                        #   in Loop: Header=BB3_61 Depth=1
	movq	72(%rsp), %rbp          # 8-byte Reload
	subq	%r14, %rbp
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	6(%rax,%r14,2), %rbx
	movq	80(%rsp), %rcx          # 8-byte Reload
	leal	3(%rcx,%r14), %esi
	movq	16(%rsp), %rax          # 8-byte Reload
	imull	%eax, %esi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	leal	2(%rcx,%r14), %esi
	imull	%eax, %esi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leal	1(%rcx,%r14), %esi
	imull	%eax, %esi
	movq	%rsi, 192(%rsp)         # 8-byte Spill
	addl	%ecx, %r14d
	imull	%eax, %r14d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB3_66:                               #   Parent Loop BB3_61 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r14,%r15), %eax
	movslq	%eax, %rdi
	addq	%r13, %rdi
	leaq	-6(%rbx), %rsi
	movq	%r12, %rdx
	callq	memcpy
	movq	192(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r15), %eax
	movslq	%eax, %rdi
	addq	%r13, %rdi
	leaq	-4(%rbx), %rsi
	movq	%r12, %rdx
	callq	memcpy
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	(%rax,%r15), %eax
	movslq	%eax, %rdi
	addq	%r13, %rdi
	leaq	-2(%rbx), %rsi
	movq	%r12, %rdx
	callq	memcpy
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r15), %eax
	movslq	%eax, %rdi
	addq	%r13, %rdi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	callq	memcpy
	movl	36(%rsp), %edx          # 4-byte Reload
	addq	$8, %rbx
	addl	%edx, %r15d
	addq	$-4, %rbp
	jne	.LBB3_66
.LBB3_67:                               # %._crit_edge.us.i147
                                        #   in Loop: Header=BB3_61 Depth=1
	movq	56(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	movl	4(%rsp), %eax           # 4-byte Reload
	addl	160(%rsp), %eax         # 4-byte Folded Reload
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	136(%rsp), %r13d        # 4-byte Reload
	movq	80(%rsp), %rax          # 8-byte Reload
	addl	%r13d, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	cmpq	176(%rsp), %rdx         # 8-byte Folded Reload
	movq	64(%rsp), %rbx          # 8-byte Reload
	jne	.LBB3_61
.LBB3_68:                               # %img2buf.exit148
	movl	204(%rsp), %ecx         # 4-byte Reload
	subl	88(%rsp), %ecx          # 4-byte Folded Reload
	movl	208(%rsp), %eax         # 4-byte Reload
	subl	52(%rsp), %eax          # 4-byte Folded Reload
	imull	16(%rsp), %ecx          # 4-byte Folded Reload
	imull	%eax, %ecx
	movslq	%ecx, %rdx
	movl	124(%rsp), %edi         # 4-byte Reload
	movq	112(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rsi
	callq	write
.LBB3_69:
	movq	%rbp, %rdi
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end3:
	.size	write_out_picture, .Lfunc_end3-write_out_picture
	.cfi_endproc

	.globl	init_out_buffer
	.p2align	4, 0x90
	.type	init_out_buffer,@function
init_out_buffer:                        # @init_out_buffer
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 16
	callq	alloc_frame_store
	movq	%rax, out_buffer(%rip)
	popq	%rax
	retq
.Lfunc_end4:
	.size	init_out_buffer, .Lfunc_end4-init_out_buffer
	.cfi_endproc

	.globl	uninit_out_buffer
	.p2align	4, 0x90
	.type	uninit_out_buffer,@function
uninit_out_buffer:                      # @uninit_out_buffer
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 16
	movq	out_buffer(%rip), %rdi
	callq	free_frame_store
	movq	$0, out_buffer(%rip)
	popq	%rax
	retq
.Lfunc_end5:
	.size	uninit_out_buffer, .Lfunc_end5-uninit_out_buffer
	.cfi_endproc

	.globl	clear_picture
	.p2align	4, 0x90
	.type	clear_picture,@function
clear_picture:                          # @clear_picture
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 32
.Lcfi31:
	.cfi_offset %rbx, -24
.Lcfi32:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	cmpl	$0, 6396(%r14)
	jle	.LBB6_3
# BB#1:                                 # %.lr.ph24
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	movq	6440(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	img(%rip), %rax
	movl	15512(%rax), %esi
	movslq	6392(%r14), %rdx
	addq	%rdx, %rdx
	callq	memset
	incq	%rbx
	movslq	6396(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB6_2
.LBB6_3:                                # %.preheader18
	cmpl	$0, 6404(%r14)
	jle	.LBB6_9
# BB#4:                                 # %.lr.ph21
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_5:                                # =>This Inner Loop Header: Depth=1
	movq	6472(%r14), %rax
	movq	(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	img(%rip), %rax
	movl	15516(%rax), %esi
	movslq	6400(%r14), %rdx
	addq	%rdx, %rdx
	callq	memset
	incq	%rbx
	movslq	6404(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB6_5
# BB#6:                                 # %.preheader
	testl	%eax, %eax
	jle	.LBB6_9
# BB#7:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_8:                                # =>This Inner Loop Header: Depth=1
	movq	6472(%r14), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	img(%rip), %rax
	movl	15516(%rax), %esi
	movslq	6400(%r14), %rdx
	addq	%rdx, %rdx
	callq	memset
	incq	%rbx
	movslq	6404(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB6_8
.LBB6_9:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	clear_picture, .Lfunc_end6-clear_picture
	.cfi_endproc

	.globl	write_unpaired_field
	.p2align	4, 0x90
	.type	write_unpaired_field,@function
write_unpaired_field:                   # @write_unpaired_field
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 48
.Lcfi38:
	.cfi_offset %rbx, -48
.Lcfi39:
	.cfi_offset %r12, -40
.Lcfi40:
	.cfi_offset %r14, -32
.Lcfi41:
	.cfi_offset %r15, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r12
	movl	(%r12), %eax
	testb	$1, %al
	je	.LBB7_11
# BB#1:
	movq	48(%r12), %rbx
	movl	6392(%rbx), %esi
	movl	6396(%rbx), %edx
	movl	6400(%rbx), %ecx
	movl	6404(%rbx), %r8d
	movl	$2, %edi
	callq	alloc_storable_picture
	movq	%rax, %rbp
	movq	%rbp, 56(%r12)
	movl	6560(%rbx), %eax
	movl	%eax, 6560(%rbp)
	cmpl	$0, 6396(%rbp)
	jle	.LBB7_4
# BB#2:                                 # %.lr.ph24.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_3:                                # =>This Inner Loop Header: Depth=1
	movq	6440(%rbp), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	img(%rip), %rax
	movl	15512(%rax), %esi
	movslq	6392(%rbp), %rdx
	addq	%rdx, %rdx
	callq	memset
	incq	%rbx
	movslq	6396(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB7_3
.LBB7_4:                                # %.preheader18.i
	cmpl	$0, 6404(%rbp)
	jle	.LBB7_10
# BB#5:                                 # %.lr.ph21.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_6:                                # =>This Inner Loop Header: Depth=1
	movq	6472(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	img(%rip), %rax
	movl	15516(%rax), %esi
	movslq	6400(%rbp), %rdx
	addq	%rdx, %rdx
	callq	memset
	incq	%rbx
	movslq	6404(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB7_6
# BB#7:                                 # %.preheader.i
	testl	%eax, %eax
	jle	.LBB7_10
# BB#8:                                 # %.lr.ph.i.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_9:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	6472(%rbp), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	img(%rip), %rax
	movl	15516(%rax), %esi
	movslq	6400(%rbp), %rdx
	addq	%rdx, %rdx
	callq	memset
	incq	%rbx
	movslq	6404(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB7_9
.LBB7_10:                               # %clear_picture.exit
	movq	%r12, %rdi
	callq	dpb_combine_field_yuv
	movq	40(%r12), %rdi
	movl	%r14d, %esi
	callq	write_out_picture
	movl	(%r12), %eax
.LBB7_11:
	testb	$2, %al
	je	.LBB7_33
# BB#12:
	movq	56(%r12), %r15
	movl	6392(%r15), %esi
	movl	6396(%r15), %edx
	movl	6400(%r15), %ecx
	movl	6404(%r15), %r8d
	movl	$1, %edi
	callq	alloc_storable_picture
	movq	%rax, %rbp
	movq	%rbp, 48(%r12)
	cmpl	$0, 6396(%rbp)
	jle	.LBB7_15
# BB#13:                                # %.lr.ph24.i39
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_14:                               # =>This Inner Loop Header: Depth=1
	movq	6440(%rbp), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	img(%rip), %rax
	movl	15512(%rax), %esi
	movslq	6392(%rbp), %rdx
	addq	%rdx, %rdx
	callq	memset
	incq	%rbx
	movslq	6396(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB7_14
.LBB7_15:                               # %.preheader18.i40
	cmpl	$0, 6404(%rbp)
	jle	.LBB7_21
# BB#16:                                # %.lr.ph21.i41
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_17:                               # =>This Inner Loop Header: Depth=1
	movq	6472(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	img(%rip), %rax
	movl	15516(%rax), %esi
	movslq	6400(%rbp), %rdx
	addq	%rdx, %rdx
	callq	memset
	incq	%rbx
	movslq	6404(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB7_17
# BB#18:                                # %.preheader.i44
	testl	%eax, %eax
	jle	.LBB7_21
# BB#19:                                # %.lr.ph.i45.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_20:                               # %.lr.ph.i45
                                        # =>This Inner Loop Header: Depth=1
	movq	6472(%rbp), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	img(%rip), %rax
	movl	15516(%rax), %esi
	movslq	6400(%rbp), %rdx
	addq	%rdx, %rdx
	callq	memset
	incq	%rbx
	movslq	6404(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB7_20
.LBB7_21:                               # %clear_picture.exit50
	movl	6560(%r15), %eax
	movq	48(%r12), %rbp
	movl	%eax, 6560(%rbp)
	cmpl	$0, 6396(%rbp)
	jle	.LBB7_24
# BB#22:                                # %.lr.ph24.i51
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_23:                               # =>This Inner Loop Header: Depth=1
	movq	6440(%rbp), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	img(%rip), %rax
	movl	15512(%rax), %esi
	movslq	6392(%rbp), %rdx
	addq	%rdx, %rdx
	callq	memset
	incq	%rbx
	movslq	6396(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB7_23
.LBB7_24:                               # %.preheader18.i52
	cmpl	$0, 6404(%rbp)
	jle	.LBB7_30
# BB#25:                                # %.lr.ph21.i53
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_26:                               # =>This Inner Loop Header: Depth=1
	movq	6472(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	img(%rip), %rax
	movl	15516(%rax), %esi
	movslq	6400(%rbp), %rdx
	addq	%rdx, %rdx
	callq	memset
	incq	%rbx
	movslq	6404(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB7_26
# BB#27:                                # %.preheader.i56
	testl	%eax, %eax
	jle	.LBB7_30
# BB#28:                                # %.lr.ph.i57.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_29:                               # %.lr.ph.i57
                                        # =>This Inner Loop Header: Depth=1
	movq	6472(%rbp), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	img(%rip), %rax
	movl	15516(%rax), %esi
	movslq	6400(%rbp), %rdx
	addq	%rdx, %rdx
	callq	memset
	incq	%rbx
	movslq	6404(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB7_29
.LBB7_30:                               # %clear_picture.exit62
	movq	48(%r12), %rax
	movq	56(%r12), %rcx
	movl	6568(%rcx), %edx
	movl	%edx, 6568(%rax)
	testl	%edx, %edx
	je	.LBB7_32
# BB#31:
	movups	6572(%rcx), %xmm0
	movups	%xmm0, 6572(%rax)
.LBB7_32:
	movq	%r12, %rdi
	callq	dpb_combine_field_yuv
	movq	40(%r12), %rdi
	movl	%r14d, %esi
	callq	write_out_picture
.LBB7_33:
	movl	$3, (%r12)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	write_unpaired_field, .Lfunc_end7-write_unpaired_field
	.cfi_endproc

	.globl	flush_direct_output
	.p2align	4, 0x90
	.type	flush_direct_output,@function
flush_direct_output:                    # @flush_direct_output
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 16
	movl	%edi, %eax
	movq	out_buffer(%rip), %rdi
	movl	%eax, %esi
	callq	write_unpaired_field
	movq	out_buffer(%rip), %rax
	movq	40(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 40(%rax)
	movq	48(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 48(%rax)
	movq	56(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 56(%rax)
	movl	$0, (%rax)
	popq	%rax
	retq
.Lfunc_end8:
	.size	flush_direct_output, .Lfunc_end8-flush_direct_output
	.cfi_endproc

	.globl	write_stored_frame
	.p2align	4, 0x90
	.type	write_stored_frame,@function
write_stored_frame:                     # @write_stored_frame
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	out_buffer(%rip), %rdi
	callq	write_unpaired_field
	movq	out_buffer(%rip), %rax
	movq	40(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 40(%rax)
	movq	48(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 48(%rax)
	movq	56(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 56(%rax)
	movl	$0, (%rax)
	cmpl	$2, (%rbx)
	jg	.LBB9_2
# BB#1:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	write_unpaired_field
	jmp	.LBB9_3
.LBB9_2:
	movq	40(%rbx), %rdi
	movl	%ebp, %esi
	callq	write_out_picture
.LBB9_3:
	movl	$1, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end9:
	.size	write_stored_frame, .Lfunc_end9-write_stored_frame
	.cfi_endproc

	.globl	direct_output
	.p2align	4, 0x90
	.type	direct_output,@function
direct_output:                          # @direct_output
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 32
.Lcfi52:
	.cfi_offset %rbx, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	(%rbx), %eax
	cmpl	$1, %eax
	je	.LBB10_3
# BB#1:
	testl	%eax, %eax
	jne	.LBB10_6
# BB#2:
	movq	out_buffer(%rip), %rdi
	movl	%ebp, %esi
	callq	write_unpaired_field
	movq	out_buffer(%rip), %rax
	movq	40(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 40(%rax)
	movq	48(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 48(%rax)
	movq	56(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 56(%rax)
	movl	$0, (%rax)
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	write_out_picture
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	free_storable_picture   # TAILCALL
.LBB10_3:
	movq	out_buffer(%rip), %rdi
	movl	(%rdi), %ecx
	movl	$1, %eax
	testb	$1, %cl
	je	.LBB10_5
# BB#4:
	movl	%ebp, %esi
	callq	write_unpaired_field
	movq	out_buffer(%rip), %rax
	movq	40(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 40(%rax)
	movq	48(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 48(%rax)
	movq	56(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rdi
	movq	$0, 56(%rdi)
	movl	$0, (%rdi)
	movl	(%rbx), %eax
	xorl	%ecx, %ecx
.LBB10_5:
	movq	%rbx, 48(%rdi)
	orl	$1, %ecx
	movl	%ecx, (%rdi)
.LBB10_6:
	movq	out_buffer(%rip), %rdi
	movl	(%rdi), %ecx
	cmpl	$2, %eax
	jne	.LBB10_10
# BB#7:
	testb	$2, %cl
	je	.LBB10_9
# BB#8:
	movl	%ebp, %esi
	callq	write_unpaired_field
	movq	out_buffer(%rip), %rax
	movq	40(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 40(%rax)
	movq	48(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 48(%rax)
	movq	56(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rdi
	movq	$0, 56(%rdi)
	movl	$0, (%rdi)
	xorl	%ecx, %ecx
.LBB10_9:
	movq	%rbx, 56(%rdi)
	orl	$2, %ecx
	movl	%ecx, (%rdi)
.LBB10_10:                              # %._crit_edge
	cmpl	$3, %ecx
	jne	.LBB10_12
# BB#11:
	callq	dpb_combine_field_yuv
	movq	out_buffer(%rip), %rax
	movq	40(%rax), %rdi
	movl	%ebp, %esi
	callq	write_out_picture
	movq	out_buffer(%rip), %rax
	movq	40(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 40(%rax)
	movq	48(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 48(%rax)
	movq	56(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 56(%rax)
	movl	$0, (%rax)
.LBB10_12:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end10:
	.size	direct_output, .Lfunc_end10-direct_output
	.cfi_endproc

	.globl	direct_output_paff
	.p2align	4, 0x90
	.type	direct_output_paff,@function
direct_output_paff:                     # @direct_output_paff
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi56:
	.cfi_def_cfa_offset 32
.Lcfi57:
	.cfi_offset %rbx, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	$.Lstr, %edi
	callq	puts
	movq	out_buffer(%rip), %rax
	movq	40(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 40(%rax)
	movq	48(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 48(%rax)
	movq	56(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 56(%rax)
	movl	$0, (%rax)
	movq	%rbx, %rdi
	movl	%ebp, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	direct_output           # TAILCALL
.Lfunc_end11:
	.size	direct_output_paff, .Lfunc_end11-direct_output_paff
	.cfi_endproc

	.type	.Lwrite_out_picture.SubWidthC,@object # @write_out_picture.SubWidthC
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.Lwrite_out_picture.SubWidthC:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.size	.Lwrite_out_picture.SubWidthC, 16

	.type	.Lwrite_out_picture.SubHeightC,@object # @write_out_picture.SubHeightC
	.p2align	4
.Lwrite_out_picture.SubHeightC:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.size	.Lwrite_out_picture.SubHeightC, 16

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"write_out_picture: buf"
	.size	.L.str.1, 23

	.type	out_buffer,@object      # @out_buffer
	.comm	out_buffer,8,8
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Warning!!! Frame can't fit in DPB. Displayed out of sequence."
	.size	.Lstr, 62


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
