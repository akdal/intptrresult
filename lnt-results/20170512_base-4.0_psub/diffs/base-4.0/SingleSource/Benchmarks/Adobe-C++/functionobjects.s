	.text
	.file	"functionobjects.bc"
	.globl	_Z13record_resultdPKc
	.p2align	4, 0x90
	.type	_Z13record_resultdPKc,@function
_Z13record_resultdPKc:                  # @_Z13record_resultdPKc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	results(%rip), %rax
	testq	%rax, %rax
	je	.LBB0_1
# BB#2:
	movl	current_test(%rip), %ecx
	movl	allocated_results(%rip), %edx
	cmpl	%edx, %ecx
	jge	.LBB0_3
	jmp	.LBB0_5
.LBB0_1:                                # %._crit_edge
	movl	allocated_results(%rip), %edx
.LBB0_3:
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movslq	%edx, %rcx
	movq	%rcx, %rsi
	shlq	$4, %rsi
	addl	$10, %ecx
	movl	%ecx, allocated_results(%rip)
	addq	$160, %rsi
	movq	%rax, %rdi
	callq	realloc
	movq	%rax, results(%rip)
	testq	%rax, %rax
	je	.LBB0_6
# BB#4:                                 # %._crit_edge2
	movl	current_test(%rip), %ecx
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
.LBB0_5:
	movslq	%ecx, %rcx
	movq	%rcx, %rdx
	shlq	$4, %rdx
	movsd	%xmm0, (%rax,%rdx)
	movq	%rbx, 8(%rax,%rdx)
	incl	%ecx
	movl	%ecx, current_test(%rip)
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB0_6:
	movl	allocated_results(%rip), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$-1, %edi
	callq	exit
.Lfunc_end0:
	.size	_Z13record_resultdPKc, .Lfunc_end0-_Z13record_resultdPKc
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4696837146684686336     # double 1.0E+6
	.text
	.globl	_Z9summarizePKciiii
	.p2align	4, 0x90
	.type	_Z9summarizePKciiii,@function
_Z9summarizePKciiii:                    # @_Z9summarizePKciiii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi6:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi7:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi9:
	.cfi_def_cfa_offset 80
.Lcfi10:
	.cfi_offset %rbx, -56
.Lcfi11:
	.cfi_offset %r12, -48
.Lcfi12:
	.cfi_offset %r13, -40
.Lcfi13:
	.cfi_offset %r14, -32
.Lcfi14:
	.cfi_offset %r15, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	movl	%edx, %r12d
	movl	%esi, %r13d
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movslq	current_test(%rip), %rbx
	testq	%rbx, %rbx
	jle	.LBB1_1
# BB#2:                                 # %.lr.ph63
	movq	results(%rip), %r14
	addq	$8, %r14
	movl	$12, %ebp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rdi
	callq	strlen
	cmpl	%ebp, %eax
	cmovgel	%eax, %ebp
	incq	%r15
	addq	$16, %r14
	cmpq	%rbx, %r15
	jl	.LBB1_3
	jmp	.LBB1_4
.LBB1_1:
	movl	$12, %ebp
.LBB1_4:                                # %._crit_edge64
	leal	-12(%rbp), %esi
	movl	$.L.str.1, %edi
	movl	$.L.str.2, %edx
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.3, %edi
	movl	$.L.str.2, %edx
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
	xorpd	%xmm0, %xmm0
	cmpl	$0, current_test(%rip)
	jle	.LBB1_5
# BB#20:                                # %.lr.ph59
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r13d, %xmm0
	cvtsi2sdl	%r12d, %xmm1
	mulsd	%xmm0, %xmm1
	divsd	.LCPI1_0(%rip), %xmm1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	movl	%ebp, %r12d
	xorl	%ebx, %ebx
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB1_21:                               # =>This Inner Loop Header: Depth=1
	movq	results(%rip), %r14
	movq	(%r14,%rbp), %r13
	movq	%r13, %rdi
	callq	strlen
	movl	%r12d, %edx
	subl	%eax, %edx
	movsd	-8(%r14,%rbp), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movapd	%xmm0, %xmm2
	divsd	(%r14), %xmm2
	movl	$.L.str.4, %edi
	movl	$.L.str.5, %ecx
	movb	$3, %al
	movq	%r13, %r8
	movl	%ebx, %esi
	callq	printf
	incq	%rbx
	movslq	current_test(%rip), %rax
	addq	$16, %rbp
	cmpq	%rax, %rbx
	jl	.LBB1_21
# BB#6:                                 # %.preheader48
	testl	%eax, %eax
	movq	16(%rsp), %r14          # 8-byte Reload
	xorpd	%xmm0, %xmm0
	jle	.LBB1_14
# BB#7:                                 # %.lr.ph54
	movq	results(%rip), %rbp
	leaq	-1(%rax), %rsi
	movq	%rax, %rdi
	andq	$3, %rdi
	je	.LBB1_8
# BB#9:                                 # %.prol.preheader
	xorpd	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB1_10:                               # =>This Inner Loop Header: Depth=1
	addsd	(%rdx), %xmm0
	incq	%rcx
	addq	$16, %rdx
	cmpq	%rcx, %rdi
	jne	.LBB1_10
	jmp	.LBB1_11
.LBB1_5:
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB1_14
.LBB1_8:
	xorl	%ecx, %ecx
	xorpd	%xmm0, %xmm0
.LBB1_11:                               # %.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB1_14
# BB#12:                                # %.lr.ph54.new
	movq	%rcx, %rdx
	shlq	$4, %rdx
	leaq	48(%rbp,%rdx), %rdx
	.p2align	4, 0x90
.LBB1_13:                               # =>This Inner Loop Header: Depth=1
	addsd	-48(%rdx), %xmm0
	addsd	-32(%rdx), %xmm0
	addsd	-16(%rdx), %xmm0
	addsd	(%rdx), %xmm0
	addq	$4, %rcx
	addq	$64, %rdx
	cmpq	%rax, %rcx
	jl	.LBB1_13
.LBB1_14:                               # %._crit_edge55
	movl	$.L.str.6, %edi
	movb	$1, %al
	movq	%r14, %rsi
	callq	printf
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB1_19
# BB#15:                                # %._crit_edge55
	cmpl	$2, current_test(%rip)
	jl	.LBB1_19
# BB#16:                                # %.lr.ph.preheader
	xorpd	%xmm1, %xmm1
	movl	$1, %ebx
	movl	$16, %ebp
	.p2align	4, 0x90
.LBB1_17:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	movq	results(%rip), %rax
	movsd	(%rax,%rbp), %xmm0      # xmm0 = mem[0],zero
	divsd	(%rax), %xmm0
	callq	log
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	incq	%rbx
	movslq	current_test(%rip), %rax
	addq	$16, %rbp
	cmpq	%rax, %rbx
	jl	.LBB1_17
# BB#18:                                # %._crit_edge
	decl	%eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	callq	exp
	movl	$.L.str.7, %edi
	movb	$1, %al
	movq	%r14, %rsi
	callq	printf
.LBB1_19:
	movl	$0, current_test(%rip)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_Z9summarizePKciiii, .Lfunc_end1-_Z9summarizePKciiii
	.cfi_endproc

	.globl	_Z17summarize_simplefP8_IO_FILEPKc
	.p2align	4, 0x90
	.type	_Z17summarize_simplefP8_IO_FILEPKc,@function
_Z17summarize_simplefP8_IO_FILEPKc:     # @_Z17summarize_simplefP8_IO_FILEPKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 64
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movslq	current_test(%rip), %r12
	testq	%r12, %r12
	jle	.LBB2_1
# BB#2:                                 # %.lr.ph42
	movq	results(%rip), %rbp
	addq	$8, %rbp
	movl	$12, %r13d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	callq	strlen
	cmpl	%r13d, %eax
	cmovgel	%eax, %r13d
	incq	%rbx
	addq	$16, %rbp
	cmpq	%r12, %rbx
	jl	.LBB2_3
	jmp	.LBB2_4
.LBB2_1:
	movl	$12, %r13d
.LBB2_4:                                # %._crit_edge43
	leal	-12(%r13), %edx
	movl	$.L.str.8, %esi
	movl	$.L.str.2, %ecx
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	movl	$.L.str.9, %esi
	movl	$.L.str.2, %ecx
	xorl	%eax, %eax
	movq	%r15, %rdi
	movl	%r13d, %edx
	callq	fprintf
	xorpd	%xmm0, %xmm0
	cmpl	$0, current_test(%rip)
	jle	.LBB2_15
# BB#5:                                 # %.lr.ph38
	movq	%r14, (%rsp)            # 8-byte Spill
	movl	%r13d, %r13d
	xorl	%ebx, %ebx
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB2_6:                                # =>This Inner Loop Header: Depth=1
	movq	results(%rip), %r14
	movq	(%r14,%rbp), %r12
	movq	%r12, %rdi
	callq	strlen
	movl	%r13d, %ecx
	subl	%eax, %ecx
	movsd	-8(%r14,%rbp), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str.10, %esi
	movl	$.L.str.5, %r8d
	movb	$1, %al
	movq	%r15, %rdi
	movq	%r12, %r9
	movl	%ebx, %edx
	callq	fprintf
	incq	%rbx
	movslq	current_test(%rip), %rax
	addq	$16, %rbp
	cmpq	%rax, %rbx
	jl	.LBB2_6
# BB#7:                                 # %.preheader
	testl	%eax, %eax
	movq	(%rsp), %r14            # 8-byte Reload
	xorpd	%xmm0, %xmm0
	jle	.LBB2_15
# BB#8:                                 # %.lr.ph
	movq	results(%rip), %rbp
	leaq	-1(%rax), %rsi
	movq	%rax, %rdi
	andq	$3, %rdi
	je	.LBB2_9
# BB#10:                                # %.prol.preheader
	xorpd	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB2_11:                               # =>This Inner Loop Header: Depth=1
	addsd	(%rdx), %xmm0
	incq	%rcx
	addq	$16, %rdx
	cmpq	%rcx, %rdi
	jne	.LBB2_11
	jmp	.LBB2_12
.LBB2_9:
	xorl	%ecx, %ecx
	xorpd	%xmm0, %xmm0
.LBB2_12:                               # %.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB2_15
# BB#13:                                # %.lr.ph.new
	movq	%rcx, %rdx
	shlq	$4, %rdx
	leaq	48(%rbp,%rdx), %rdx
	.p2align	4, 0x90
.LBB2_14:                               # =>This Inner Loop Header: Depth=1
	addsd	-48(%rdx), %xmm0
	addsd	-32(%rdx), %xmm0
	addsd	-16(%rdx), %xmm0
	addsd	(%rdx), %xmm0
	addq	$4, %rcx
	addq	$64, %rdx
	cmpq	%rax, %rcx
	jl	.LBB2_14
.LBB2_15:                               # %._crit_edge
	movl	$.L.str.6, %esi
	movb	$1, %al
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	fprintf
	movl	$0, current_test(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_Z17summarize_simplefP8_IO_FILEPKc, .Lfunc_end2-_Z17summarize_simplefP8_IO_FILEPKc
	.cfi_endproc

	.globl	_Z11start_timerv
	.p2align	4, 0x90
	.type	_Z11start_timerv,@function
_Z11start_timerv:                       # @_Z11start_timerv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 16
	callq	clock
	movq	%rax, start_time(%rip)
	popq	%rax
	retq
.Lfunc_end3:
	.size	_Z11start_timerv, .Lfunc_end3-_Z11start_timerv
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4696837146684686336     # double 1.0E+6
	.text
	.globl	_Z5timerv
	.p2align	4, 0x90
	.type	_Z5timerv,@function
_Z5timerv:                              # @_Z5timerv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 16
	callq	clock
	movq	%rax, end_time(%rip)
	subq	start_time(%rip), %rax
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI4_0(%rip), %xmm0
	popq	%rax
	retq
.Lfunc_end4:
	.size	_Z5timerv, .Lfunc_end4-_Z5timerv
	.cfi_endproc

	.globl	_Z19less_than_function1PKvS0_
	.p2align	4, 0x90
	.type	_Z19less_than_function1PKvS0_,@function
_Z19less_than_function1PKvS0_:          # @_Z19less_than_function1PKvS0_
	.cfi_startproc
# BB#0:
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	xorl	%ecx, %ecx
	ucomisd	%xmm1, %xmm0
	seta	%cl
	ucomisd	%xmm0, %xmm1
	movl	$-1, %eax
	cmovbel	%ecx, %eax
	retq
.Lfunc_end5:
	.size	_Z19less_than_function1PKvS0_, .Lfunc_end5-_Z19less_than_function1PKvS0_
	.cfi_endproc

	.globl	_Z19less_than_function2dd
	.p2align	4, 0x90
	.type	_Z19less_than_function2dd,@function
_Z19less_than_function2dd:              # @_Z19less_than_function2dd
	.cfi_startproc
# BB#0:
	ucomisd	%xmm0, %xmm1
	seta	%al
	retq
.Lfunc_end6:
	.size	_Z19less_than_function2dd, .Lfunc_end6-_Z19less_than_function2dd
	.cfi_endproc

	.globl	_ZNK17less_than_functorclERKdS1_
	.p2align	4, 0x90
	.type	_ZNK17less_than_functorclERKdS1_,@function
_ZNK17less_than_functorclERKdS1_:       # @_ZNK17less_than_functorclERKdS1_
	.cfi_startproc
# BB#0:
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	ucomisd	(%rsi), %xmm0
	seta	%al
	retq
.Lfunc_end7:
	.size	_ZNK17less_than_functorclERKdS1_, .Lfunc_end7-_ZNK17less_than_functorclERKdS1_
	.cfi_endproc

	.globl	_Z18quicksort_functionPdS_PFbddE
	.p2align	4, 0x90
	.type	_Z18quicksort_functionPdS_PFbddE,@function
_Z18quicksort_functionPdS_PFbddE:       # @_Z18quicksort_functionPdS_PFbddE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi36:
	.cfi_def_cfa_offset 64
.Lcfi37:
	.cfi_offset %rbx, -48
.Lcfi38:
	.cfi_offset %r12, -40
.Lcfi39:
	.cfi_offset %r13, -32
.Lcfi40:
	.cfi_offset %r14, -24
.Lcfi41:
	.cfi_offset %r15, -16
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	%r14, %rax
	subq	%r15, %rax
	cmpq	$9, %rax
	jl	.LBB8_9
# BB#1:
	movsd	(%r15), %xmm2           # xmm2 = mem[0],zero
	movq	%r14, %r13
	movq	%r15, %rbx
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	jmp	.LBB8_2
.LBB8_7:                                #   in Loop: Header=BB8_2 Depth=1
	movq	(%r13), %rax
	movq	(%rbx), %rcx
	movq	%rcx, (%r13)
	movq	%rax, (%rbx)
	.p2align	4, 0x90
.LBB8_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_5 Depth 2
	movsd	-8(%r13), %xmm1         # xmm1 = mem[0],zero
	addq	$-8, %r13
	movapd	%xmm2, %xmm0
	callq	*%r12
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	testb	%al, %al
	jne	.LBB8_2
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	cmpq	%r13, %rbx
	jae	.LBB8_8
# BB#4:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	addq	$-8, %rbx
	.p2align	4, 0x90
.LBB8_5:                                # %.preheader
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	addq	$8, %rbx
	movapd	%xmm2, %xmm1
	callq	*%r12
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	testb	%al, %al
	jne	.LBB8_5
# BB#6:                                 #   in Loop: Header=BB8_2 Depth=1
	cmpq	%r13, %rbx
	jb	.LBB8_7
.LBB8_8:
	addq	$8, %r13
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	callq	_Z9quicksortIPdPFbddEEvT_S3_T0_
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	_Z9quicksortIPdPFbddEEvT_S3_T0_ # TAILCALL
.LBB8_9:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	_Z18quicksort_functionPdS_PFbddE, .Lfunc_end8-_Z18quicksort_functionPdS_PFbddE
	.cfi_endproc

	.section	.text._Z9quicksortIPdPFbddEEvT_S3_T0_,"axG",@progbits,_Z9quicksortIPdPFbddEEvT_S3_T0_,comdat
	.weak	_Z9quicksortIPdPFbddEEvT_S3_T0_
	.p2align	4, 0x90
	.type	_Z9quicksortIPdPFbddEEvT_S3_T0_,@function
_Z9quicksortIPdPFbddEEvT_S3_T0_:        # @_Z9quicksortIPdPFbddEEvT_S3_T0_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi47:
	.cfi_def_cfa_offset 64
.Lcfi48:
	.cfi_offset %rbx, -48
.Lcfi49:
	.cfi_offset %r12, -40
.Lcfi50:
	.cfi_offset %r13, -32
.Lcfi51:
	.cfi_offset %r14, -24
.Lcfi52:
	.cfi_offset %r15, -16
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	%r14, %rax
	subq	%r15, %rax
	cmpq	$9, %rax
	jl	.LBB9_9
	.p2align	4, 0x90
.LBB9_1:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_2 Depth 2
                                        #       Child Loop BB9_5 Depth 3
	movsd	(%r15), %xmm2           # xmm2 = mem[0],zero
	movq	%r14, %r13
	movq	%r15, %rbx
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	jmp	.LBB9_2
.LBB9_7:                                #   in Loop: Header=BB9_2 Depth=2
	movq	(%r13), %rax
	movq	(%rbx), %rcx
	movq	%rcx, (%r13)
	movq	%rax, (%rbx)
	.p2align	4, 0x90
.LBB9_2:                                #   Parent Loop BB9_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_5 Depth 3
	movsd	-8(%r13), %xmm1         # xmm1 = mem[0],zero
	addq	$-8, %r13
	movapd	%xmm2, %xmm0
	callq	*%r12
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	testb	%al, %al
	jne	.LBB9_2
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=2
	cmpq	%r13, %rbx
	jae	.LBB9_8
# BB#4:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB9_2 Depth=2
	addq	$-8, %rbx
	.p2align	4, 0x90
.LBB9_5:                                # %.preheader
                                        #   Parent Loop BB9_1 Depth=1
                                        #     Parent Loop BB9_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	addq	$8, %rbx
	movapd	%xmm2, %xmm1
	callq	*%r12
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	testb	%al, %al
	jne	.LBB9_5
# BB#6:                                 #   in Loop: Header=BB9_2 Depth=2
	cmpq	%r13, %rbx
	jb	.LBB9_7
.LBB9_8:                                # %tailrecurse
                                        #   in Loop: Header=BB9_1 Depth=1
	addq	$8, %r13
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	callq	_Z9quicksortIPdPFbddEEvT_S3_T0_
	movq	%r14, %rax
	subq	%r13, %rax
	cmpq	$8, %rax
	movq	%r13, %r15
	jg	.LBB9_1
.LBB9_9:                                # %tailrecurse._crit_edge
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	_Z9quicksortIPdPFbddEEvT_S3_T0_, .Lfunc_end9-_Z9quicksortIPdPFbddEEvT_S3_T0_
	.cfi_endproc

	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi59:
	.cfi_def_cfa_offset 160
.Lcfi60:
	.cfi_offset %rbx, -56
.Lcfi61:
	.cfi_offset %r12, -48
.Lcfi62:
	.cfi_offset %r13, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %ebp
	movl	$10000, %ebx            # imm = 0x2710
	movl	$300, %eax              # imm = 0x12C
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpl	$2, %ebp
	jl	.LBB10_3
# BB#1:
	movq	8(%r14), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpl	$2, %ebp
	je	.LBB10_3
# BB#2:
	movq	16(%r14), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rbx
.LBB10_3:                               # %.thread
	leal	123(%rbx), %edi
	callq	srand
	movslq	%ebx, %r13
	movl	$8, %ecx
	movq	%r13, %rax
	mulq	%rcx
	movq	$-1, %r14
	cmovnoq	%rax, %r14
	movq	%r14, %rdi
	callq	_Znam
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testl	%r13d, %r13d
	jle	.LBB10_6
# BB#4:                                 # %.lr.ph294.preheader
	movl	%ebx, %ebx
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB10_5:                               # %.lr.ph294
                                        # =>This Inner Loop Header: Depth=1
	callq	rand
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, (%rbp)
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB10_5
.LBB10_6:                               # %._crit_edge295
	movq	%r14, %rdi
	callq	_Znam
	movq	%rax, %rbx
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB10_266
# BB#7:                                 # %.lr.ph292
	leaq	(,%r13,8), %r12
	leaq	8(%rbx), %r14
	leaq	-8(,%r13,8), %r15
	xorl	%ebp, %ebp
	testq	%r12, %r12
	je	.LBB10_13
	.p2align	4, 0x90
.LBB10_8:                               # %.lr.ph292.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_9 Depth 2
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	memcpy
	movl	$8, %edx
	movl	$_Z19less_than_function1PKvS0_, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	qsort
	movq	%r15, %rax
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB10_9:                               #   Parent Loop BB10_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_12
# BB#10:                                #   in Loop: Header=BB10_9 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_9
# BB#11:                                #   in Loop: Header=BB10_8 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_12:                              # %_Z13verify_sortedIPdEvT_S1_.exit195
                                        #   in Loop: Header=BB10_8 Depth=1
	incl	%ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jne	.LBB10_8
	jmp	.LBB10_18
	.p2align	4, 0x90
.LBB10_13:                              # %.lr.ph292.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_14 Depth 2
	movl	$8, %edx
	movl	$_Z19less_than_function1PKvS0_, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	qsort
	movq	%r15, %rax
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB10_14:                              #   Parent Loop BB10_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_17
# BB#15:                                #   in Loop: Header=BB10_14 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_14
# BB#16:                                #   in Loop: Header=BB10_13 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_17:                              # %_Z13verify_sortedIPdEvT_S1_.exit195.us
                                        #   in Loop: Header=BB10_13 Depth=1
	incl	%ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jne	.LBB10_13
.LBB10_18:                              # %.preheader264
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB10_266
# BB#19:                                # %.lr.ph289
	testq	%r12, %r12
	leaq	(%rbx,%r13,8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r13, 40(%rsp)          # 8-byte Spill
	je	.LBB10_29
# BB#20:                                # %.lr.ph289.split.preheader
	movq	%r13, %rax
	leaq	8(%rbx), %r13
	leaq	-8(,%rax,8), %r15
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB10_21:                              # %.lr.ph289.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_23 Depth 2
                                        #       Child Loop BB10_26 Depth 3
                                        #     Child Loop BB10_71 Depth 2
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	memcpy
	cmpq	$9, %r12
	jl	.LBB10_70
# BB#22:                                #   in Loop: Header=BB10_21 Depth=1
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm2
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rbx, %rax
	jmp	.LBB10_23
.LBB10_28:                              #   in Loop: Header=BB10_23 Depth=2
	movq	(%rax), %rcx
	movq	%rcx, (%rbp)
	movsd	%xmm1, (%rax)
	movapd	%xmm1, %xmm2
	.p2align	4, 0x90
.LBB10_23:                              #   Parent Loop BB10_21 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_26 Depth 3
	movsd	-8(%rbp), %xmm1         # xmm1 = mem[0],zero
	addq	$-8, %rbp
	ucomisd	%xmm0, %xmm1
	ja	.LBB10_23
# BB#24:                                #   in Loop: Header=BB10_23 Depth=2
	cmpq	%rbp, %rax
	jae	.LBB10_69
# BB#25:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB10_23 Depth=2
	ucomisd	%xmm2, %xmm0
	jbe	.LBB10_27
	.p2align	4, 0x90
.LBB10_26:                              # %.preheader.i..preheader.i_crit_edge
                                        #   Parent Loop BB10_21 Depth=1
                                        #     Parent Loop BB10_23 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	ucomisd	8(%rax), %xmm0
	leaq	8(%rax), %rax
	ja	.LBB10_26
.LBB10_27:                              # %.preheader.i._crit_edge
                                        #   in Loop: Header=BB10_23 Depth=2
	cmpq	%rbp, %rax
	jb	.LBB10_28
.LBB10_69:                              #   in Loop: Header=BB10_21 Depth=1
	addq	$8, %rbp
	movl	$_Z19less_than_function2dd, %edx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	_Z9quicksortIPdPFbddEEvT_S3_T0_
	movl	$_Z19less_than_function2dd, %edx
	movq	%rbp, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	_Z9quicksortIPdPFbddEEvT_S3_T0_
.LBB10_70:                              # %_Z18quicksort_functionPdS_PFbddE.exit.preheader
                                        #   in Loop: Header=BB10_21 Depth=1
	movq	%r15, %rax
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB10_71:                              # %_Z18quicksort_functionPdS_PFbddE.exit
                                        #   Parent Loop BB10_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_74
# BB#72:                                #   in Loop: Header=BB10_71 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_71
# BB#73:                                #   in Loop: Header=BB10_21 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_74:                              # %_Z13verify_sortedIPdEvT_S1_.exit199
                                        #   in Loop: Header=BB10_21 Depth=1
	incl	%r14d
	cmpl	8(%rsp), %r14d          # 4-byte Folded Reload
	jne	.LBB10_21
	jmp	.LBB10_43
.LBB10_29:                              # %.lr.ph289.split.us.preheader
	leaq	8(%rbx), %r14
	leaq	-8(,%r13,8), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB10_30:                              # %.lr.ph289.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_32 Depth 2
                                        #       Child Loop BB10_35 Depth 3
                                        #     Child Loop BB10_39 Depth 2
	cmpq	$9, %r12
	jl	.LBB10_38
# BB#31:                                #   in Loop: Header=BB10_30 Depth=1
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm2
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	%rbx, %rax
	jmp	.LBB10_32
.LBB10_51:                              #   in Loop: Header=BB10_32 Depth=2
	movq	(%rax), %rcx
	movq	%rcx, (%r13)
	movsd	%xmm1, (%rax)
	movapd	%xmm1, %xmm2
	.p2align	4, 0x90
.LBB10_32:                              #   Parent Loop BB10_30 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_35 Depth 3
	movsd	-8(%r13), %xmm1         # xmm1 = mem[0],zero
	addq	$-8, %r13
	ucomisd	%xmm0, %xmm1
	ja	.LBB10_32
# BB#33:                                #   in Loop: Header=BB10_32 Depth=2
	cmpq	%r13, %rax
	jae	.LBB10_37
# BB#34:                                # %.preheader.i.us.preheader
                                        #   in Loop: Header=BB10_32 Depth=2
	ucomisd	%xmm2, %xmm0
	jbe	.LBB10_36
	.p2align	4, 0x90
.LBB10_35:                              # %.preheader.i.us..preheader.i.us_crit_edge
                                        #   Parent Loop BB10_30 Depth=1
                                        #     Parent Loop BB10_32 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	ucomisd	8(%rax), %xmm0
	leaq	8(%rax), %rax
	ja	.LBB10_35
.LBB10_36:                              # %.preheader.i.us._crit_edge
                                        #   in Loop: Header=BB10_32 Depth=2
	cmpq	%r13, %rax
	jb	.LBB10_51
.LBB10_37:                              #   in Loop: Header=BB10_30 Depth=1
	addq	$8, %r13
	movl	$_Z19less_than_function2dd, %edx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	_Z9quicksortIPdPFbddEEvT_S3_T0_
	movl	$_Z19less_than_function2dd, %edx
	movq	%r13, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	_Z9quicksortIPdPFbddEEvT_S3_T0_
.LBB10_38:                              # %_Z18quicksort_functionPdS_PFbddE.exit.us.preheader
                                        #   in Loop: Header=BB10_30 Depth=1
	movq	%r15, %rax
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB10_39:                              # %_Z18quicksort_functionPdS_PFbddE.exit.us
                                        #   Parent Loop BB10_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_42
# BB#40:                                #   in Loop: Header=BB10_39 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_39
# BB#41:                                #   in Loop: Header=BB10_30 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_42:                              # %_Z13verify_sortedIPdEvT_S1_.exit199.us
                                        #   in Loop: Header=BB10_30 Depth=1
	incl	%ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jne	.LBB10_30
.LBB10_43:                              # %.preheader263
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movq	40(%rsp), %rax          # 8-byte Reload
	jle	.LBB10_266
# BB#44:                                # %.lr.ph287
	testq	%r12, %r12
	leaq	(%rbx,%rax,8), %r14
	leaq	8(%rbx), %r15
	leaq	-8(,%rax,8), %r13
	je	.LBB10_52
# BB#45:                                # %.lr.ph287.split.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB10_46:                              # %.lr.ph287.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_47 Depth 2
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	memcpy
	movl	$_Z19less_than_function2dd, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_Z9quicksortIPdPFbddEEvT_S3_T0_
	movq	%r13, %rax
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB10_47:                              #   Parent Loop BB10_46 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_50
# BB#48:                                #   in Loop: Header=BB10_47 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_47
# BB#49:                                #   in Loop: Header=BB10_46 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_50:                              # %_Z13verify_sortedIPdEvT_S1_.exit203
                                        #   in Loop: Header=BB10_46 Depth=1
	incl	%ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jne	.LBB10_46
	jmp	.LBB10_58
.LBB10_52:                              # %.lr.ph287.split.us.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB10_53:                              # %.lr.ph287.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_54 Depth 2
	movl	$_Z19less_than_function2dd, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_Z9quicksortIPdPFbddEEvT_S3_T0_
	movq	%r13, %rax
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB10_54:                              #   Parent Loop BB10_53 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_57
# BB#55:                                #   in Loop: Header=BB10_54 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_54
# BB#56:                                #   in Loop: Header=BB10_53 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_57:                              # %_Z13verify_sortedIPdEvT_S1_.exit203.us
                                        #   in Loop: Header=BB10_53 Depth=1
	incl	%ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jne	.LBB10_53
.LBB10_58:                              # %.preheader262
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movq	40(%rsp), %rax          # 8-byte Reload
	jle	.LBB10_266
# BB#59:                                # %.lr.ph285
	testq	%r12, %r12
	je	.LBB10_75
# BB#60:                                # %.lr.ph285.split.preheader
	leaq	8(%rbx), %r15
	leaq	-8(,%rax,8), %rbp
	xorl	%r13d, %r13d
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB10_61:                              # %.lr.ph285.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_63 Depth 2
                                        #       Child Loop BB10_66 Depth 3
                                        #     Child Loop BB10_110 Depth 2
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	memcpy
	cmpq	$9, %r12
	jl	.LBB10_109
# BB#62:                                #   in Loop: Header=BB10_61 Depth=1
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm2
	movq	%r14, %rbp
	movq	%rbx, %rax
	jmp	.LBB10_63
.LBB10_68:                              #   in Loop: Header=BB10_63 Depth=2
	movq	(%rax), %rcx
	movq	%rcx, (%rbp)
	movsd	%xmm1, (%rax)
	movapd	%xmm1, %xmm2
	.p2align	4, 0x90
.LBB10_63:                              #   Parent Loop BB10_61 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_66 Depth 3
	movsd	-8(%rbp), %xmm1         # xmm1 = mem[0],zero
	addq	$-8, %rbp
	ucomisd	%xmm0, %xmm1
	ja	.LBB10_63
# BB#64:                                #   in Loop: Header=BB10_63 Depth=2
	cmpq	%rbp, %rax
	jae	.LBB10_108
# BB#65:                                # %.preheader.i207.preheader
                                        #   in Loop: Header=BB10_63 Depth=2
	ucomisd	%xmm2, %xmm0
	jbe	.LBB10_67
	.p2align	4, 0x90
.LBB10_66:                              # %.preheader..preheader_crit_edge.i
                                        #   Parent Loop BB10_61 Depth=1
                                        #     Parent Loop BB10_63 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	ucomisd	8(%rax), %xmm0
	leaq	8(%rax), %rax
	ja	.LBB10_66
.LBB10_67:                              # %.preheader.i207._crit_edge
                                        #   in Loop: Header=BB10_63 Depth=2
	cmpq	%rbp, %rax
	jb	.LBB10_68
.LBB10_108:                             #   in Loop: Header=BB10_61 Depth=1
	addq	$8, %rbp
	movl	$_Z19less_than_function2dd, %edx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	_Z9quicksortIPdPFbddEEvT_S3_T0_
	movl	$_Z19less_than_function2dd, %edx
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	_Z9quicksortIPdPFbddEEvT_S3_T0_
	movq	32(%rsp), %rbp          # 8-byte Reload
.LBB10_109:                             # %_Z9quicksortIPdXadL_Z19less_than_function2ddEEEvT_S1_.exit.preheader
                                        #   in Loop: Header=BB10_61 Depth=1
	movq	%rbp, %rax
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB10_110:                             # %_Z9quicksortIPdXadL_Z19less_than_function2ddEEEvT_S1_.exit
                                        #   Parent Loop BB10_61 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_113
# BB#111:                               #   in Loop: Header=BB10_110 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_110
# BB#112:                               #   in Loop: Header=BB10_61 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_113:                             # %_Z13verify_sortedIPdEvT_S1_.exit210
                                        #   in Loop: Header=BB10_61 Depth=1
	incl	%r13d
	cmpl	8(%rsp), %r13d          # 4-byte Folded Reload
	jne	.LBB10_61
	jmp	.LBB10_88
.LBB10_75:                              # %.lr.ph285.split.us.preheader
	leaq	8(%rbx), %r13
	leaq	-8(,%rax,8), %rbp
	xorl	%r15d, %r15d
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB10_76:                              # %.lr.ph285.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_78 Depth 2
                                        #       Child Loop BB10_107 Depth 3
                                        #     Child Loop BB10_84 Depth 2
	cmpq	$9, %r12
	jl	.LBB10_83
# BB#77:                                #   in Loop: Header=BB10_76 Depth=1
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm2
	movq	%r14, %rbp
	movq	%rbx, %rax
	jmp	.LBB10_78
.LBB10_106:                             #   in Loop: Header=BB10_78 Depth=2
	movq	(%rax), %rcx
	movq	%rcx, (%rbp)
	movsd	%xmm1, (%rax)
	movapd	%xmm1, %xmm2
	.p2align	4, 0x90
.LBB10_78:                              #   Parent Loop BB10_76 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_107 Depth 3
	movsd	-8(%rbp), %xmm1         # xmm1 = mem[0],zero
	addq	$-8, %rbp
	ucomisd	%xmm0, %xmm1
	ja	.LBB10_78
# BB#79:                                #   in Loop: Header=BB10_78 Depth=2
	cmpq	%rbp, %rax
	jae	.LBB10_82
# BB#80:                                # %.preheader.i207.preheader.us
                                        #   in Loop: Header=BB10_78 Depth=2
	ucomisd	%xmm2, %xmm0
	jbe	.LBB10_81
	.p2align	4, 0x90
.LBB10_107:                             # %.preheader..preheader_crit_edge.i.us
                                        #   Parent Loop BB10_76 Depth=1
                                        #     Parent Loop BB10_78 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	ucomisd	8(%rax), %xmm0
	leaq	8(%rax), %rax
	ja	.LBB10_107
.LBB10_81:                              # %.preheader.i207._crit_edge.us
                                        #   in Loop: Header=BB10_78 Depth=2
	cmpq	%rbp, %rax
	jb	.LBB10_106
.LBB10_82:                              #   in Loop: Header=BB10_76 Depth=1
	addq	$8, %rbp
	movl	$_Z19less_than_function2dd, %edx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	_Z9quicksortIPdPFbddEEvT_S3_T0_
	movl	$_Z19less_than_function2dd, %edx
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	_Z9quicksortIPdPFbddEEvT_S3_T0_
	movq	32(%rsp), %rbp          # 8-byte Reload
.LBB10_83:                              # %_Z9quicksortIPdXadL_Z19less_than_function2ddEEEvT_S1_.exit.us.preheader
                                        #   in Loop: Header=BB10_76 Depth=1
	movq	%rbp, %rax
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB10_84:                              # %_Z9quicksortIPdXadL_Z19less_than_function2ddEEEvT_S1_.exit.us
                                        #   Parent Loop BB10_76 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_87
# BB#85:                                #   in Loop: Header=BB10_84 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_84
# BB#86:                                #   in Loop: Header=BB10_76 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_87:                              # %_Z13verify_sortedIPdEvT_S1_.exit210.us
                                        #   in Loop: Header=BB10_76 Depth=1
	incl	%r15d
	cmpl	8(%rsp), %r15d          # 4-byte Folded Reload
	jne	.LBB10_76
.LBB10_88:                              # %.preheader261
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	jle	.LBB10_266
# BB#89:                                # %.lr.ph283
	leaq	(%rbx,%rcx,8), %r15
	movq	%r12, %rax
	sarq	$3, %rax
	bsrq	%rax, %rax
	xorq	$63, %rax
	addq	%rax, %rax
	xorq	$126, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rbx, %rax
	subq	$-128, %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leaq	8(%rbx), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	-136(,%rcx,8), %rax
	shrq	$3, %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	136(%rbx), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	56(%rsp), %r13
	xorl	%r14d, %r14d
	movq	%r15, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB10_90:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_150 Depth 2
                                        #       Child Loop BB10_156 Depth 3
                                        #     Child Loop BB10_131 Depth 2
                                        #       Child Loop BB10_146 Depth 3
                                        #     Child Loop BB10_137 Depth 2
                                        #     Child Loop BB10_140 Depth 2
                                        #       Child Loop BB10_141 Depth 3
                                        #       Child Loop BB10_143 Depth 3
                                        #     Child Loop BB10_94 Depth 2
	testq	%r12, %r12
	je	.LBB10_92
# BB#91:                                #   in Loop: Header=BB10_90 Depth=1
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	memcpy
.LBB10_92:                              # %_ZSt4copyIPdS0_ET0_T_S2_S1_.exit211
                                        #   in Loop: Header=BB10_90 Depth=1
	cmpq	%r15, %rbx
	je	.LBB10_93
# BB#129:                               #   in Loop: Header=BB10_90 Depth=1
	movl	$_Z19less_than_function2dd, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_T1_
	cmpq	$129, %r12
	jl	.LBB10_148
# BB#130:                               # %.preheader306.preheader
                                        #   in Loop: Header=BB10_90 Depth=1
	movl	$1, %ebp
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB10_131:                             # %.preheader306
                                        #   Parent Loop BB10_90 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_146 Depth 3
	leaq	(%rbx,%rbp,8), %r15
	movsd	(%rbx,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB10_145
# BB#132:                               # %_ZSt13copy_backwardIPdS0_ET0_T_S2_S1_.exit.i.i
                                        #   in Loop: Header=BB10_131 Depth=2
	leaq	(,%rbp,8), %rdx
	subq	%rdx, %rdi
	addq	$16, %rdi
	movq	%rbx, %rsi
	movsd	%xmm1, 32(%rsp)         # 8-byte Spill
	callq	memmove
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, (%rbx)
	jmp	.LBB10_133
	.p2align	4, 0x90
.LBB10_145:                             #   in Loop: Header=BB10_131 Depth=2
	movq	%r15, %rcx
	movq	%r13, %rax
	.p2align	4, 0x90
.LBB10_146:                             # %._crit_edge348
                                        #   Parent Loop BB10_90 Depth=1
                                        #     Parent Loop BB10_131 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	%xmm1, (%rax)
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	movq	%rcx, %rax
	leaq	-8(%rcx), %rcx
	movsd	56(%rsp), %xmm0         # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB10_146
# BB#147:                               # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_comp_iterIPFbddEEEEvT_T0_.exit.i.i
                                        #   in Loop: Header=BB10_131 Depth=2
	movsd	%xmm0, 8(%rcx)
.LBB10_133:                             # %.backedge.i.i
                                        #   in Loop: Header=BB10_131 Depth=2
	incq	%rbp
	cmpq	$16, %rbp
	movq	%r15, %rdi
	jne	.LBB10_131
# BB#134:                               # %_ZSt16__insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_.exit.i
                                        #   in Loop: Header=BB10_90 Depth=1
	movq	24(%rsp), %r15          # 8-byte Reload
	cmpq	%r15, 80(%rsp)          # 8-byte Folded Reload
	je	.LBB10_93
# BB#135:                               # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB10_90 Depth=1
	cmpq	$0, 88(%rsp)            # 8-byte Folded Reload
	movq	80(%rsp), %rax          # 8-byte Reload
	jne	.LBB10_139
# BB#136:                               # %.lr.ph.i.i.prol
                                        #   in Loop: Header=BB10_90 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB10_137:                             #   Parent Loop BB10_90 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rax), %rdx
	movq	%rdx, (%rcx)
	movsd	-16(%rax), %xmm1        # xmm1 = mem[0],zero
	leaq	-8(%rax), %rax
	movsd	56(%rsp), %xmm0         # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	movq	%rax, %rcx
	ja	.LBB10_137
# BB#138:                               # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_comp_iterIPFbddEEEEvT_T0_.exit.i15.i.prol
                                        #   in Loop: Header=BB10_90 Depth=1
	movsd	%xmm0, (%rax)
	movq	72(%rsp), %rax          # 8-byte Reload
.LBB10_139:                             # %.lr.ph.i.i.prol.loopexit
                                        #   in Loop: Header=BB10_90 Depth=1
	cmpq	$0, 96(%rsp)            # 8-byte Folded Reload
	je	.LBB10_93
	.p2align	4, 0x90
.LBB10_140:                             # %.lr.ph.i.i
                                        #   Parent Loop BB10_90 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_141 Depth 3
                                        #       Child Loop BB10_143 Depth 3
	movq	%rax, %rdx
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB10_141:                             #   Parent Loop BB10_90 Depth=1
                                        #     Parent Loop BB10_140 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rdx), %rsi
	movq	%rsi, (%rcx)
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	movq	%rdx, %rcx
	leaq	-8(%rdx), %rdx
	movsd	56(%rsp), %xmm0         # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB10_141
# BB#142:                               # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_comp_iterIPFbddEEEEvT_T0_.exit.i15.i
                                        #   in Loop: Header=BB10_140 Depth=2
	movsd	%xmm0, 8(%rdx)
	leaq	8(%rax), %rcx
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB10_143:                             #   Parent Loop BB10_90 Depth=1
                                        #     Parent Loop BB10_140 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %rsi
	movq	%rsi, (%rdx)
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	movq	%rcx, %rdx
	leaq	-8(%rcx), %rcx
	movsd	56(%rsp), %xmm0         # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB10_143
# BB#144:                               # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_comp_iterIPFbddEEEEvT_T0_.exit.i15.i.1
                                        #   in Loop: Header=BB10_140 Depth=2
	movsd	%xmm0, 8(%rcx)
	addq	$16, %rax
	cmpq	%r15, %rax
	jne	.LBB10_140
	jmp	.LBB10_93
	.p2align	4, 0x90
.LBB10_148:                             # %.preheader.i.i
                                        #   in Loop: Header=BB10_90 Depth=1
	cmpq	%r15, 64(%rsp)          # 8-byte Folded Reload
	je	.LBB10_93
# BB#149:                               # %.lr.ph.i20.i.preheader
                                        #   in Loop: Header=BB10_90 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB10_150:                             # %.lr.ph.i20.i
                                        #   Parent Loop BB10_90 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_156 Depth 3
	movq	%rax, %rbp
	movsd	(%rbp), %xmm1           # xmm1 = mem[0],zero
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB10_155
# BB#151:                               #   in Loop: Header=BB10_150 Depth=2
	movq	%rbp, %rdx
	subq	%rbx, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	je	.LBB10_153
# BB#152:                               #   in Loop: Header=BB10_150 Depth=2
	shlq	$3, %rax
	subq	%rax, %rdi
	addq	$16, %rdi
	movq	%rbx, %rsi
	movsd	%xmm1, 32(%rsp)         # 8-byte Spill
	callq	memmove
	movsd	32(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
.LBB10_153:                             # %_ZSt13copy_backwardIPdS0_ET0_T_S2_S1_.exit.i25.i
                                        #   in Loop: Header=BB10_150 Depth=2
	movsd	%xmm1, (%rbx)
	jmp	.LBB10_154
	.p2align	4, 0x90
.LBB10_155:                             #   in Loop: Header=BB10_150 Depth=2
	movq	%rbp, %rcx
	movq	%r13, %rax
	.p2align	4, 0x90
.LBB10_156:                             # %._crit_edge346
                                        #   Parent Loop BB10_90 Depth=1
                                        #     Parent Loop BB10_150 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	%xmm1, (%rax)
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	movq	%rcx, %rax
	leaq	-8(%rcx), %rcx
	movsd	56(%rsp), %xmm0         # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB10_156
# BB#157:                               # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_comp_iterIPFbddEEEEvT_T0_.exit.i32.i
                                        #   in Loop: Header=BB10_150 Depth=2
	movsd	%xmm0, 8(%rcx)
.LBB10_154:                             # %.backedge.i27.i
                                        #   in Loop: Header=BB10_150 Depth=2
	leaq	8(%rbp), %rax
	cmpq	%r15, %rax
	movq	%rbp, %rdi
	jne	.LBB10_150
	.p2align	4, 0x90
.LBB10_93:                              # %_ZSt4sortIPdPFbddEEvT_S3_T0_.exit.preheader
                                        #   in Loop: Header=BB10_90 Depth=1
	movl	$8, %eax
	.p2align	4, 0x90
.LBB10_94:                              # %_ZSt4sortIPdPFbddEEvT_S3_T0_.exit
                                        #   Parent Loop BB10_90 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r12
	je	.LBB10_97
# BB#95:                                #   in Loop: Header=BB10_94 Depth=2
	movsd	-8(%rbx,%rax), %xmm0    # xmm0 = mem[0],zero
	ucomisd	(%rbx,%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB10_94
# BB#96:                                #   in Loop: Header=BB10_90 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_97:                              # %_Z13verify_sortedIPdEvT_S1_.exit214
                                        #   in Loop: Header=BB10_90 Depth=1
	incl	%r14d
	cmpl	8(%rsp), %r14d          # 4-byte Folded Reload
	jne	.LBB10_90
# BB#98:                                # %.preheader259
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movq	40(%rsp), %r13          # 8-byte Reload
	jle	.LBB10_266
# BB#99:                                # %.lr.ph281
	testq	%r12, %r12
	je	.LBB10_114
# BB#100:                               # %.lr.ph281.split.preheader
	leaq	-8(,%r13,8), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB10_101:                             # %.lr.ph281.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_102 Depth 2
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	memcpy
	movq	%rbx, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	_Z9quicksortIPd17less_than_functorEvT_S2_T0_
	movq	%r14, %rax
	movq	64(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB10_102:                             #   Parent Loop BB10_101 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_105
# BB#103:                               #   in Loop: Header=BB10_102 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_102
# BB#104:                               #   in Loop: Header=BB10_101 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_105:                             # %_Z13verify_sortedIPdEvT_S1_.exit222
                                        #   in Loop: Header=BB10_101 Depth=1
	incl	%ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jne	.LBB10_101
	jmp	.LBB10_120
.LBB10_114:                             # %.lr.ph281.split.us.preheader
	leaq	8(%rbx), %r14
	leaq	-8(,%r13,8), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB10_115:                             # %.lr.ph281.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_116 Depth 2
	movq	%rbx, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	_Z9quicksortIPd17less_than_functorEvT_S2_T0_
	movq	%r15, %rax
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB10_116:                             #   Parent Loop BB10_115 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_119
# BB#117:                               #   in Loop: Header=BB10_116 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_116
# BB#118:                               #   in Loop: Header=BB10_115 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_119:                             # %_Z13verify_sortedIPdEvT_S1_.exit222.us
                                        #   in Loop: Header=BB10_115 Depth=1
	incl	%ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jne	.LBB10_115
.LBB10_120:                             # %.preheader258
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	jle	.LBB10_266
# BB#121:                               # %.lr.ph279
	xorl	%ebp, %ebp
	testq	%r12, %r12
	je	.LBB10_158
	.p2align	4, 0x90
.LBB10_122:                             # %.lr.ph279.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_123 Depth 2
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_T1_
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_
	movl	$8, %eax
	.p2align	4, 0x90
.LBB10_123:                             # %_ZSt4sortIPd17less_than_functorEvT_S2_T0_.exit
                                        #   Parent Loop BB10_122 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r12
	je	.LBB10_126
# BB#124:                               #   in Loop: Header=BB10_123 Depth=2
	movsd	-8(%rbx,%rax), %xmm0    # xmm0 = mem[0],zero
	ucomisd	(%rbx,%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB10_123
# BB#125:                               #   in Loop: Header=BB10_122 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_126:                             # %_Z13verify_sortedIPdEvT_S1_.exit225
                                        #   in Loop: Header=BB10_122 Depth=1
	testq	%r12, %r12
	je	.LBB10_128
# BB#127:                               #   in Loop: Header=BB10_122 Depth=1
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	memcpy
.LBB10_128:                             # %_ZSt4copyIPdS0_ET0_T_S2_S1_.exit226
                                        #   in Loop: Header=BB10_122 Depth=1
	incl	%ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jne	.LBB10_122
	jmp	.LBB10_165
	.p2align	4, 0x90
.LBB10_158:                             # %.lr.ph279.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_159 Depth 2
	movl	$8, %eax
	.p2align	4, 0x90
.LBB10_159:                             # %_ZSt4sortIPd17less_than_functorEvT_S2_T0_.exit.us
                                        #   Parent Loop BB10_158 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_162
# BB#160:                               #   in Loop: Header=BB10_159 Depth=2
	movsd	-8(%rbx,%rax), %xmm0    # xmm0 = mem[0],zero
	ucomisd	(%rbx,%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB10_159
# BB#161:                               #   in Loop: Header=BB10_158 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_162:                             # %_Z13verify_sortedIPdEvT_S1_.exit225.us
                                        #   in Loop: Header=BB10_158 Depth=1
	testq	%r12, %r12
	je	.LBB10_164
# BB#163:                               #   in Loop: Header=BB10_158 Depth=1
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	memcpy
.LBB10_164:                             # %_ZSt4copyIPdS0_ET0_T_S2_S1_.exit226.us
                                        #   in Loop: Header=BB10_158 Depth=1
	incl	%ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jne	.LBB10_158
.LBB10_165:                             # %.preheader257
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB10_266
# BB#166:                               # %.lr.ph277
	leaq	8(%rbx), %r14
	testq	%r12, %r12
	je	.LBB10_173
# BB#167:                               # %.lr.ph277.split.preheader
	leaq	-8(,%r13,8), %r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB10_168:                             # %.lr.ph277.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_169 Depth 2
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	memcpy
	movq	%rbx, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	_Z9quicksortIPd24inline_less_than_functorEvT_S2_T0_
	movq	%r13, %rax
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB10_169:                             #   Parent Loop BB10_168 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_172
# BB#170:                               #   in Loop: Header=BB10_169 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_169
# BB#171:                               #   in Loop: Header=BB10_168 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_172:                             # %_Z13verify_sortedIPdEvT_S1_.exit230
                                        #   in Loop: Header=BB10_168 Depth=1
	incl	%ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jne	.LBB10_168
	jmp	.LBB10_179
.LBB10_173:                             # %.lr.ph277.split.us.preheader
	leaq	-8(,%r13,8), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB10_174:                             # %.lr.ph277.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_175 Depth 2
	movq	%rbx, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	_Z9quicksortIPd24inline_less_than_functorEvT_S2_T0_
	movq	%r15, %rax
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB10_175:                             #   Parent Loop BB10_174 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_178
# BB#176:                               #   in Loop: Header=BB10_175 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_175
# BB#177:                               #   in Loop: Header=BB10_174 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_178:                             # %_Z13verify_sortedIPdEvT_S1_.exit230.us
                                        #   in Loop: Header=BB10_174 Depth=1
	incl	%ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jne	.LBB10_174
.LBB10_179:                             # %.preheader256
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	jle	.LBB10_266
# BB#180:                               # %.lr.ph275
	leaq	8(%rbx), %r14
	testq	%r12, %r12
	je	.LBB10_189
# BB#181:                               # %.lr.ph275.split.preheader
	leaq	-8(,%rax,8), %rbp
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB10_182:                             # %.lr.ph275.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_185 Depth 2
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	memcpy
	cmpq	%r15, %rbx
	je	.LBB10_184
# BB#183:                               #   in Loop: Header=BB10_182 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_T1_
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_
.LBB10_184:                             # %_ZSt4sortIPd24inline_less_than_functorEvT_S2_T0_.exit.preheader
                                        #   in Loop: Header=BB10_182 Depth=1
	movq	%rbp, %rax
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB10_185:                             # %_ZSt4sortIPd24inline_less_than_functorEvT_S2_T0_.exit
                                        #   Parent Loop BB10_182 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_188
# BB#186:                               #   in Loop: Header=BB10_185 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_185
# BB#187:                               #   in Loop: Header=BB10_182 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_188:                             # %_Z13verify_sortedIPdEvT_S1_.exit238
                                        #   in Loop: Header=BB10_182 Depth=1
	incl	%r13d
	cmpl	8(%rsp), %r13d          # 4-byte Folded Reload
	jne	.LBB10_182
	jmp	.LBB10_197
.LBB10_189:                             # %.lr.ph275.split.us.preheader
	leaq	-8(,%rax,8), %r13
	xorl	%ebp, %ebp
.LBB10_190:                             # %.lr.ph275.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_193 Depth 2
	cmpq	%r15, %rbx
	je	.LBB10_192
# BB#191:                               #   in Loop: Header=BB10_190 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_T1_
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_
.LBB10_192:                             # %_ZSt4sortIPd24inline_less_than_functorEvT_S2_T0_.exit.us.preheader
                                        #   in Loop: Header=BB10_190 Depth=1
	movq	%r13, %rax
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB10_193:                             # %_ZSt4sortIPd24inline_less_than_functorEvT_S2_T0_.exit.us
                                        #   Parent Loop BB10_190 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_196
# BB#194:                               #   in Loop: Header=BB10_193 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_193
# BB#195:                               #   in Loop: Header=BB10_190 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_196:                             # %_Z13verify_sortedIPdEvT_S1_.exit238.us
                                        #   in Loop: Header=BB10_190 Depth=1
	incl	%ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jne	.LBB10_190
.LBB10_197:                             # %.preheader255
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movq	40(%rsp), %rax          # 8-byte Reload
	jle	.LBB10_266
# BB#198:                               # %.lr.ph273
	leaq	8(%rbx), %r14
	testq	%r12, %r12
	je	.LBB10_205
# BB#199:                               # %.lr.ph273.split.preheader
	leaq	-8(,%rax,8), %r13
	xorl	%ebp, %ebp
.LBB10_200:                             # %.lr.ph273.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_201 Depth 2
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	memcpy
	movq	%rbx, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	_Z9quicksortIPdSt4lessIdEEvT_S3_T0_
	movq	%r13, %rax
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB10_201:                             #   Parent Loop BB10_200 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_204
# BB#202:                               #   in Loop: Header=BB10_201 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_201
# BB#203:                               #   in Loop: Header=BB10_200 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_204:                             # %_Z13verify_sortedIPdEvT_S1_.exit242
                                        #   in Loop: Header=BB10_200 Depth=1
	incl	%ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jne	.LBB10_200
	jmp	.LBB10_211
.LBB10_205:                             # %.lr.ph273.split.us.preheader
	leaq	-8(,%rax,8), %r15
	xorl	%ebp, %ebp
.LBB10_206:                             # %.lr.ph273.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_207 Depth 2
	movq	%rbx, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	_Z9quicksortIPdSt4lessIdEEvT_S3_T0_
	movq	%r15, %rax
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB10_207:                             #   Parent Loop BB10_206 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_210
# BB#208:                               #   in Loop: Header=BB10_207 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_207
# BB#209:                               #   in Loop: Header=BB10_206 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_210:                             # %_Z13verify_sortedIPdEvT_S1_.exit242.us
                                        #   in Loop: Header=BB10_206 Depth=1
	incl	%ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jne	.LBB10_206
.LBB10_211:                             # %.preheader254
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	jle	.LBB10_266
# BB#212:                               # %.lr.ph271
	leaq	8(%rbx), %r14
	testq	%r12, %r12
	je	.LBB10_221
# BB#213:                               # %.lr.ph271.split.preheader
	leaq	-8(,%rax,8), %rbp
	xorl	%r13d, %r13d
.LBB10_214:                             # %.lr.ph271.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_217 Depth 2
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	memcpy
	cmpq	%r15, %rbx
	je	.LBB10_216
# BB#215:                               #   in Loop: Header=BB10_214 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_T1_
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_
.LBB10_216:                             # %_ZSt4sortIPdSt4lessIdEEvT_S3_T0_.exit.preheader
                                        #   in Loop: Header=BB10_214 Depth=1
	movq	%rbp, %rax
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB10_217:                             # %_ZSt4sortIPdSt4lessIdEEvT_S3_T0_.exit
                                        #   Parent Loop BB10_214 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_220
# BB#218:                               #   in Loop: Header=BB10_217 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_217
# BB#219:                               #   in Loop: Header=BB10_214 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_220:                             # %_Z13verify_sortedIPdEvT_S1_.exit235
                                        #   in Loop: Header=BB10_214 Depth=1
	incl	%r13d
	cmpl	8(%rsp), %r13d          # 4-byte Folded Reload
	jne	.LBB10_214
	jmp	.LBB10_229
.LBB10_221:                             # %.lr.ph271.split.us.preheader
	leaq	-8(,%rax,8), %r13
	xorl	%ebp, %ebp
.LBB10_222:                             # %.lr.ph271.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_225 Depth 2
	cmpq	%r15, %rbx
	je	.LBB10_224
# BB#223:                               #   in Loop: Header=BB10_222 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_T1_
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_
.LBB10_224:                             # %_ZSt4sortIPdSt4lessIdEEvT_S3_T0_.exit.us.preheader
                                        #   in Loop: Header=BB10_222 Depth=1
	movq	%r13, %rax
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB10_225:                             # %_ZSt4sortIPdSt4lessIdEEvT_S3_T0_.exit.us
                                        #   Parent Loop BB10_222 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_228
# BB#226:                               #   in Loop: Header=BB10_225 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_225
# BB#227:                               #   in Loop: Header=BB10_222 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_228:                             # %_Z13verify_sortedIPdEvT_S1_.exit235.us
                                        #   in Loop: Header=BB10_222 Depth=1
	incl	%ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jne	.LBB10_222
.LBB10_229:                             # %.preheader253
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movq	40(%rsp), %rax          # 8-byte Reload
	jle	.LBB10_266
# BB#230:                               # %.lr.ph269
	leaq	8(%rbx), %r14
	testq	%r12, %r12
	je	.LBB10_237
# BB#231:                               # %.lr.ph269.split.preheader
	leaq	-8(,%rax,8), %r13
	xorl	%ebp, %ebp
.LBB10_232:                             # %.lr.ph269.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_233 Depth 2
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	memcpy
	movq	%rbx, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	_Z9quicksortIPdEvT_S1_
	movq	%r13, %rax
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB10_233:                             #   Parent Loop BB10_232 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_236
# BB#234:                               #   in Loop: Header=BB10_233 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_233
# BB#235:                               #   in Loop: Header=BB10_232 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_236:                             # %_Z13verify_sortedIPdEvT_S1_.exit219
                                        #   in Loop: Header=BB10_232 Depth=1
	incl	%ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jne	.LBB10_232
	jmp	.LBB10_243
.LBB10_237:                             # %.lr.ph269.split.us.preheader
	leaq	-8(,%rax,8), %r15
	xorl	%ebp, %ebp
.LBB10_238:                             # %.lr.ph269.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_239 Depth 2
	movq	%rbx, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	_Z9quicksortIPdEvT_S1_
	movq	%r15, %rax
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB10_239:                             #   Parent Loop BB10_238 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_242
# BB#240:                               #   in Loop: Header=BB10_239 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_239
# BB#241:                               #   in Loop: Header=BB10_238 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_242:                             # %_Z13verify_sortedIPdEvT_S1_.exit219.us
                                        #   in Loop: Header=BB10_238 Depth=1
	incl	%ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jne	.LBB10_238
.LBB10_243:                             # %.preheader
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	jle	.LBB10_266
# BB#244:                               # %.lr.ph
	testq	%r12, %r12
	je	.LBB10_253
# BB#245:                               # %.lr.ph.split.preheader
	leaq	8(%rbx), %r14
	leaq	-8(,%rax,8), %rbp
	xorl	%r13d, %r13d
.LBB10_246:                             # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_249 Depth 2
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	memcpy
	cmpq	%r15, %rbx
	je	.LBB10_248
# BB#247:                               #   in Loop: Header=BB10_246 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_
.LBB10_248:                             # %_ZSt4sortIPdEvT_S1_.exit.preheader
                                        #   in Loop: Header=BB10_246 Depth=1
	movq	%rbp, %rax
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB10_249:                             # %_ZSt4sortIPdEvT_S1_.exit
                                        #   Parent Loop BB10_246 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_252
# BB#250:                               #   in Loop: Header=BB10_249 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_249
# BB#251:                               #   in Loop: Header=BB10_246 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_252:                             # %_Z13verify_sortedIPdEvT_S1_.exit
                                        #   in Loop: Header=BB10_246 Depth=1
	incl	%r13d
	cmpl	8(%rsp), %r13d          # 4-byte Folded Reload
	jne	.LBB10_246
	jmp	.LBB10_266
.LBB10_253:                             # %.lr.ph.split.us
	cmpq	%r15, %rbx
	je	.LBB10_260
# BB#254:                               # %.lr.ph.split.us.split.preheader
	movq	%rbx, %r12
	addq	$8, %r12
	leaq	-8(,%rax,8), %r14
	xorl	%ebp, %ebp
.LBB10_255:                             # %.lr.ph.split.us.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_256 Depth 2
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_
	movq	%r14, %rax
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB10_256:                             # %_ZSt4sortIPdEvT_S1_.exit.us
                                        #   Parent Loop BB10_255 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_259
# BB#257:                               #   in Loop: Header=BB10_256 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rax
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_256
# BB#258:                               #   in Loop: Header=BB10_255 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_259:                             # %_Z13verify_sortedIPdEvT_S1_.exit.us
                                        #   in Loop: Header=BB10_255 Depth=1
	incl	%ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jne	.LBB10_255
	jmp	.LBB10_266
.LBB10_260:                             # %.lr.ph.split.us.split.us.preheader
	xorl	%ebp, %ebp
.LBB10_261:                             # %.lr.ph.split.us.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_262 Depth 2
	movl	$8, %eax
.LBB10_262:                             # %_ZSt4sortIPdEvT_S1_.exit.us.us
                                        #   Parent Loop BB10_261 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB10_265
# BB#263:                               #   in Loop: Header=BB10_262 Depth=2
	movsd	-8(%rbx,%rax), %xmm0    # xmm0 = mem[0],zero
	ucomisd	(%rbx,%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB10_262
# BB#264:                               #   in Loop: Header=BB10_261 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_265:                             # %_Z13verify_sortedIPdEvT_S1_.exit.us.us
                                        #   in Loop: Header=BB10_261 Depth=1
	incl	%ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jne	.LBB10_261
.LBB10_266:                             # %._crit_edge
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
	xorl	%eax, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	main, .Lfunc_end10-main
	.cfi_endproc

	.section	.text._Z9quicksortIPd17less_than_functorEvT_S2_T0_,"axG",@progbits,_Z9quicksortIPd17less_than_functorEvT_S2_T0_,comdat
	.weak	_Z9quicksortIPd17less_than_functorEvT_S2_T0_
	.p2align	4, 0x90
	.type	_Z9quicksortIPd17less_than_functorEvT_S2_T0_,@function
_Z9quicksortIPd17less_than_functorEvT_S2_T0_: # @_Z9quicksortIPd17less_than_functorEvT_S2_T0_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi68:
	.cfi_def_cfa_offset 32
.Lcfi69:
	.cfi_offset %rbx, -24
.Lcfi70:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%r14, %rax
	subq	%rdi, %rax
	cmpq	$9, %rax
	jl	.LBB11_10
# BB#1:
	movq	(%rdi), %rax
	movq	%rsp, %rcx
	movq	%r14, %rbx
	movq	%rdi, %rdx
	jmp	.LBB11_2
	.p2align	4, 0x90
.LBB11_8:                               #   in Loop: Header=BB11_2 Depth=1
	movsd	%xmm1, (%rbx)
	movq	%rcx, %rdx
.LBB11_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_3 Depth 2
                                        #     Child Loop BB11_6 Depth 2
	movq	%rax, (%rcx)
	movsd	(%rsp), %xmm0           # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB11_3:                               #   Parent Loop BB11_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rbx), %xmm1         # xmm1 = mem[0],zero
	addq	$-8, %rbx
	ucomisd	%xmm0, %xmm1
	ja	.LBB11_3
# BB#4:                                 #   in Loop: Header=BB11_2 Depth=1
	cmpq	%rbx, %rdx
	jae	.LBB11_9
# BB#5:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB11_2 Depth=1
	movd	%xmm1, %rax
	addq	$-8, %rdx
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB11_6:                               # %.preheader
                                        #   Parent Loop BB11_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %xmm1          # xmm1 = mem[0],zero
	addq	$8, %rcx
	ucomisd	%xmm1, %xmm0
	ja	.LBB11_6
# BB#7:                                 #   in Loop: Header=BB11_2 Depth=1
	cmpq	%rbx, %rcx
	jb	.LBB11_8
.LBB11_9:
	addq	$8, %rbx
	movq	%rbx, %rsi
	callq	_Z9quicksortIPd17less_than_functorEvT_S2_T0_
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_Z9quicksortIPd17less_than_functorEvT_S2_T0_
.LBB11_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end11:
	.size	_Z9quicksortIPd17less_than_functorEvT_S2_T0_, .Lfunc_end11-_Z9quicksortIPd17less_than_functorEvT_S2_T0_
	.cfi_endproc

	.section	.text._Z9quicksortIPd24inline_less_than_functorEvT_S2_T0_,"axG",@progbits,_Z9quicksortIPd24inline_less_than_functorEvT_S2_T0_,comdat
	.weak	_Z9quicksortIPd24inline_less_than_functorEvT_S2_T0_
	.p2align	4, 0x90
	.type	_Z9quicksortIPd24inline_less_than_functorEvT_S2_T0_,@function
_Z9quicksortIPd24inline_less_than_functorEvT_S2_T0_: # @_Z9quicksortIPd24inline_less_than_functorEvT_S2_T0_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi73:
	.cfi_def_cfa_offset 32
.Lcfi74:
	.cfi_offset %rbx, -24
.Lcfi75:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%r14, %rax
	subq	%rdi, %rax
	cmpq	$9, %rax
	jl	.LBB12_10
# BB#1:
	movq	(%rdi), %rax
	movq	%rsp, %rcx
	movq	%r14, %rbx
	movq	%rdi, %rdx
	jmp	.LBB12_2
	.p2align	4, 0x90
.LBB12_8:                               #   in Loop: Header=BB12_2 Depth=1
	movsd	%xmm1, (%rbx)
	movq	%rcx, %rdx
.LBB12_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_3 Depth 2
                                        #     Child Loop BB12_6 Depth 2
	movq	%rax, (%rcx)
	movsd	(%rsp), %xmm0           # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB12_3:                               #   Parent Loop BB12_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rbx), %xmm1         # xmm1 = mem[0],zero
	addq	$-8, %rbx
	ucomisd	%xmm0, %xmm1
	ja	.LBB12_3
# BB#4:                                 #   in Loop: Header=BB12_2 Depth=1
	cmpq	%rbx, %rdx
	jae	.LBB12_9
# BB#5:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB12_2 Depth=1
	movd	%xmm1, %rax
	addq	$-8, %rdx
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB12_6:                               # %.preheader
                                        #   Parent Loop BB12_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %xmm1          # xmm1 = mem[0],zero
	addq	$8, %rcx
	ucomisd	%xmm1, %xmm0
	ja	.LBB12_6
# BB#7:                                 #   in Loop: Header=BB12_2 Depth=1
	cmpq	%rbx, %rcx
	jb	.LBB12_8
.LBB12_9:
	addq	$8, %rbx
	movq	%rbx, %rsi
	callq	_Z9quicksortIPd24inline_less_than_functorEvT_S2_T0_
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_Z9quicksortIPd24inline_less_than_functorEvT_S2_T0_
.LBB12_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end12:
	.size	_Z9quicksortIPd24inline_less_than_functorEvT_S2_T0_, .Lfunc_end12-_Z9quicksortIPd24inline_less_than_functorEvT_S2_T0_
	.cfi_endproc

	.section	.text._Z9quicksortIPdSt4lessIdEEvT_S3_T0_,"axG",@progbits,_Z9quicksortIPdSt4lessIdEEvT_S3_T0_,comdat
	.weak	_Z9quicksortIPdSt4lessIdEEvT_S3_T0_
	.p2align	4, 0x90
	.type	_Z9quicksortIPdSt4lessIdEEvT_S3_T0_,@function
_Z9quicksortIPdSt4lessIdEEvT_S3_T0_:    # @_Z9quicksortIPdSt4lessIdEEvT_S3_T0_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi78:
	.cfi_def_cfa_offset 32
.Lcfi79:
	.cfi_offset %rbx, -24
.Lcfi80:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%r14, %rax
	subq	%rdi, %rax
	cmpq	$9, %rax
	jl	.LBB13_10
# BB#1:
	movq	(%rdi), %rax
	movq	%rsp, %rcx
	movq	%r14, %rbx
	movq	%rdi, %rdx
	jmp	.LBB13_2
	.p2align	4, 0x90
.LBB13_8:                               #   in Loop: Header=BB13_2 Depth=1
	movsd	%xmm1, (%rbx)
	movq	%rcx, %rdx
.LBB13_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_3 Depth 2
                                        #     Child Loop BB13_6 Depth 2
	movq	%rax, (%rcx)
	movsd	(%rsp), %xmm0           # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB13_3:                               #   Parent Loop BB13_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rbx), %xmm1         # xmm1 = mem[0],zero
	addq	$-8, %rbx
	ucomisd	%xmm0, %xmm1
	ja	.LBB13_3
# BB#4:                                 #   in Loop: Header=BB13_2 Depth=1
	cmpq	%rbx, %rdx
	jae	.LBB13_9
# BB#5:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB13_2 Depth=1
	movd	%xmm1, %rax
	addq	$-8, %rdx
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB13_6:                               # %.preheader
                                        #   Parent Loop BB13_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %xmm1          # xmm1 = mem[0],zero
	addq	$8, %rcx
	ucomisd	%xmm1, %xmm0
	ja	.LBB13_6
# BB#7:                                 #   in Loop: Header=BB13_2 Depth=1
	cmpq	%rbx, %rcx
	jb	.LBB13_8
.LBB13_9:
	addq	$8, %rbx
	movq	%rbx, %rsi
	callq	_Z9quicksortIPdSt4lessIdEEvT_S3_T0_
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_Z9quicksortIPdSt4lessIdEEvT_S3_T0_
.LBB13_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end13:
	.size	_Z9quicksortIPdSt4lessIdEEvT_S3_T0_, .Lfunc_end13-_Z9quicksortIPdSt4lessIdEEvT_S3_T0_
	.cfi_endproc

	.section	.text._Z9quicksortIPdEvT_S1_,"axG",@progbits,_Z9quicksortIPdEvT_S1_,comdat
	.weak	_Z9quicksortIPdEvT_S1_
	.p2align	4, 0x90
	.type	_Z9quicksortIPdEvT_S1_,@function
_Z9quicksortIPdEvT_S1_:                 # @_Z9quicksortIPdEvT_S1_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi83:
	.cfi_def_cfa_offset 32
.Lcfi84:
	.cfi_offset %rbx, -24
.Lcfi85:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%r14, %rax
	subq	%rdi, %rax
	cmpq	$9, %rax
	jl	.LBB14_5
	.p2align	4, 0x90
.LBB14_1:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_2 Depth 2
                                        #       Child Loop BB14_7 Depth 3
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	movq	%r14, %rbx
	movq	%rdi, %rax
	jmp	.LBB14_2
.LBB14_9:                               #   in Loop: Header=BB14_2 Depth=2
	movsd	%xmm1, (%rbx)
	movsd	%xmm2, (%rax)
	movapd	%xmm2, %xmm1
	.p2align	4, 0x90
.LBB14_2:                               #   Parent Loop BB14_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB14_7 Depth 3
	movsd	-8(%rbx), %xmm2         # xmm2 = mem[0],zero
	addq	$-8, %rbx
	ucomisd	%xmm0, %xmm2
	ja	.LBB14_2
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=2
	cmpq	%rbx, %rax
	jb	.LBB14_7
	jmp	.LBB14_4
	.p2align	4, 0x90
.LBB14_6:                               # %.preheader..preheader_crit_edge
                                        #   in Loop: Header=BB14_7 Depth=3
	movsd	8(%rax), %xmm1          # xmm1 = mem[0],zero
	addq	$8, %rax
.LBB14_7:                               # %.preheader..preheader_crit_edge
                                        #   Parent Loop BB14_1 Depth=1
                                        #     Parent Loop BB14_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	ucomisd	%xmm1, %xmm0
	ja	.LBB14_6
# BB#8:                                 # %.preheader._crit_edge
                                        #   in Loop: Header=BB14_2 Depth=2
	cmpq	%rbx, %rax
	jb	.LBB14_9
.LBB14_4:                               # %tailrecurse
                                        #   in Loop: Header=BB14_1 Depth=1
	addq	$8, %rbx
	movq	%rbx, %rsi
	callq	_Z9quicksortIPdEvT_S1_
	movq	%r14, %rax
	subq	%rbx, %rax
	cmpq	$8, %rax
	movq	%rbx, %rdi
	jg	.LBB14_1
.LBB14_5:                               # %tailrecurse._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end14:
	.size	_Z9quicksortIPdEvT_S1_, .Lfunc_end14-_Z9quicksortIPdEvT_S1_
	.cfi_endproc

	.section	.text._ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_T1_,"axG",@progbits,_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_T1_,comdat
	.weak	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_T1_
	.p2align	4, 0x90
	.type	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_T1_,@function
_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_T1_: # @_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_T1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi88:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi89:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 48
.Lcfi91:
	.cfi_offset %rbx, -48
.Lcfi92:
	.cfi_offset %r12, -40
.Lcfi93:
	.cfi_offset %r13, -32
.Lcfi94:
	.cfi_offset %r14, -24
.Lcfi95:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	%rsi, %r12
	movq	%rdi, %r15
	movq	%r12, %rax
	subq	%r15, %rax
	cmpq	$129, %rax
	jl	.LBB15_4
# BB#1:                                 # %.lr.ph.preheader
	decq	%rbx
	.p2align	4, 0x90
.LBB15_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	cmpq	$-1, %rbx
	je	.LBB15_5
# BB#3:                                 #   in Loop: Header=BB15_2 Depth=1
	callq	_ZSt27__unguarded_partition_pivotIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEET_S7_S7_T0_
	movq	%rax, %r13
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	%r14, %rcx
	callq	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_T1_
	movq	%r13, %rax
	subq	%r15, %rax
	decq	%rbx
	cmpq	$128, %rax
	movq	%r13, %r12
	jg	.LBB15_2
.LBB15_4:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB15_5:
	callq	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	_ZSt11__sort_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_ # TAILCALL
.Lfunc_end15:
	.size	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_T1_, .Lfunc_end15-_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_T1_
	.cfi_endproc

	.section	.text._ZSt27__unguarded_partition_pivotIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEET_S7_S7_T0_,"axG",@progbits,_ZSt27__unguarded_partition_pivotIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEET_S7_S7_T0_,comdat
	.weak	_ZSt27__unguarded_partition_pivotIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEET_S7_S7_T0_
	.p2align	4, 0x90
	.type	_ZSt27__unguarded_partition_pivotIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEET_S7_S7_T0_,@function
_ZSt27__unguarded_partition_pivotIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEET_S7_S7_T0_: # @_ZSt27__unguarded_partition_pivotIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEET_S7_S7_T0_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi98:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi99:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 48
.Lcfi101:
	.cfi_offset %rbx, -48
.Lcfi102:
	.cfi_offset %r12, -40
.Lcfi103:
	.cfi_offset %r13, -32
.Lcfi104:
	.cfi_offset %r14, -24
.Lcfi105:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %r15
	movq	%r12, %rbx
	subq	%r15, %rbx
	movq	%rbx, %rax
	sarq	$3, %rax
	shrq	$63, %rbx
	addq	%rax, %rbx
	sarq	%rbx
	leaq	8(%r15), %r13
	movsd	8(%r15), %xmm0          # xmm0 = mem[0],zero
	movsd	(%r15,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	callq	*%r14
	testb	%al, %al
	je	.LBB16_6
# BB#1:
	movsd	(%r15,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	-8(%r12), %xmm1         # xmm1 = mem[0],zero
	callq	*%r14
	testb	%al, %al
	je	.LBB16_3
# BB#2:
	movq	(%r15), %rax
	movq	(%r15,%rbx,8), %rcx
	movq	%rcx, (%r15)
	movq	%rax, (%r15,%rbx,8)
	jmp	.LBB16_10
.LBB16_6:
	movsd	(%r13), %xmm0           # xmm0 = mem[0],zero
	movsd	-8(%r12), %xmm1         # xmm1 = mem[0],zero
	callq	*%r14
	testb	%al, %al
	je	.LBB16_8
# BB#7:
	movq	(%r15), %rax
	movq	8(%r15), %rcx
	movq	%rcx, (%r15)
	movq	%rax, 8(%r15)
	jmp	.LBB16_10
.LBB16_3:
	movsd	8(%r15), %xmm0          # xmm0 = mem[0],zero
	movsd	-8(%r12), %xmm1         # xmm1 = mem[0],zero
	callq	*%r14
	movq	(%r15), %rcx
	testb	%al, %al
	jne	.LBB16_4
# BB#5:
	movq	8(%r15), %rax
	movq	%rax, (%r15)
	movq	%rcx, 8(%r15)
	jmp	.LBB16_10
.LBB16_8:
	movsd	(%r15,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	-8(%r12), %xmm1         # xmm1 = mem[0],zero
	callq	*%r14
	movq	(%r15), %rcx
	testb	%al, %al
	je	.LBB16_9
.LBB16_4:
	movq	-8(%r12), %rax
	movq	%rax, (%r15)
	movq	%rcx, -8(%r12)
	jmp	.LBB16_10
.LBB16_9:
	movq	(%r15,%rbx,8), %rax
	movq	%rax, (%r15)
	movq	%rcx, (%r15,%rbx,8)
	jmp	.LBB16_10
	.p2align	4, 0x90
.LBB16_15:                              #   in Loop: Header=BB16_10 Depth=1
	movq	(%rbx), %rax
	movq	(%r12), %rcx
	movq	%rcx, (%rbx)
	movq	%rax, (%r12)
.LBB16_10:                              # %_ZSt22__move_median_to_firstIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_S7_S7_T0_.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_11 Depth 2
                                        #     Child Loop BB16_13 Depth 2
	addq	$-8, %r13
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB16_11:                              #   Parent Loop BB16_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	addq	$8, %rbx
	movsd	(%r15), %xmm1           # xmm1 = mem[0],zero
	callq	*%r14
	testb	%al, %al
	jne	.LBB16_11
# BB#12:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB16_10 Depth=1
	leaq	8(%rbx), %r13
	.p2align	4, 0x90
.LBB16_13:                              # %.preheader.i
                                        #   Parent Loop BB16_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	movsd	-8(%r12), %xmm1         # xmm1 = mem[0],zero
	addq	$-8, %r12
	callq	*%r14
	testb	%al, %al
	jne	.LBB16_13
# BB#14:                                #   in Loop: Header=BB16_10 Depth=1
	cmpq	%r12, %rbx
	jb	.LBB16_15
# BB#16:                                # %_ZSt21__unguarded_partitionIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEET_S7_S7_S7_T0_.exit
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end16:
	.size	_ZSt27__unguarded_partition_pivotIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEET_S7_S7_T0_, .Lfunc_end16-_ZSt27__unguarded_partition_pivotIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEET_S7_S7_T0_
	.cfi_endproc

	.section	.text._ZSt11__sort_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_,"axG",@progbits,_ZSt11__sort_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_,comdat
	.weak	_ZSt11__sort_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_
	.p2align	4, 0x90
	.type	_ZSt11__sort_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_,@function
_ZSt11__sort_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_: # @_ZSt11__sort_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi106:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi107:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi108:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi109:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi110:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi112:
	.cfi_def_cfa_offset 96
.Lcfi113:
	.cfi_offset %rbx, -56
.Lcfi114:
	.cfi_offset %r12, -48
.Lcfi115:
	.cfi_offset %r13, -40
.Lcfi116:
	.cfi_offset %r14, -32
.Lcfi117:
	.cfi_offset %r15, -24
.Lcfi118:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	%r12, %rax
	subq	%r13, %rax
	cmpq	$9, %rax
	jl	.LBB17_14
	.p2align	4, 0x90
.LBB17_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_3 Depth 2
                                        #     Child Loop BB17_9 Depth 2
	movsd	-8(%r12), %xmm0         # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movq	(%r13), %rax
	movq	%rax, -8(%r12)
	leaq	-8(%r12), %r12
	movq	%r12, %rcx
	subq	%r13, %rcx
	movq	%rcx, %rdx
	sarq	$3, %rdx
	leaq	-1(%rdx), %rax
	cmpq	$2, %rax
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	jl	.LBB17_4
# BB#2:                                 # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB17_1 Depth=1
	shrq	$63, %rax
	leaq	-1(%rdx,%rax), %r14
	sarq	%r14
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB17_3:                               # %.lr.ph.i.i
                                        #   Parent Loop BB17_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r12,%r12), %rax
	leaq	2(%r12,%r12), %rbp
	leaq	1(%r12,%r12), %rbx
	movsd	16(%r13,%rax,8), %xmm0  # xmm0 = mem[0],zero
	movsd	8(%r13,%rax,8), %xmm1   # xmm1 = mem[0],zero
	callq	*%r15
	testb	%al, %al
	cmoveq	%rbp, %rbx
	movq	(%r13,%rbx,8), %rax
	movq	%rax, (%r13,%r12,8)
	cmpq	%r14, %rbx
	movq	%rbx, %r12
	jl	.LBB17_3
	jmp	.LBB17_5
	.p2align	4, 0x90
.LBB17_4:                               #   in Loop: Header=BB17_1 Depth=1
	xorl	%ebx, %ebx
.LBB17_5:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB17_1 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	testb	$1, %cl
	jne	.LBB17_8
# BB#6:                                 #   in Loop: Header=BB17_1 Depth=1
	leaq	-2(%rcx), %rax
	shrq	$63, %rax
	leaq	-2(%rcx,%rax), %rax
	sarq	%rax
	cmpq	%rax, %rbx
	jne	.LBB17_8
# BB#7:                                 #   in Loop: Header=BB17_1 Depth=1
	leaq	(%rbx,%rbx), %rax
	movq	8(%r13,%rax,8), %rax
	movq	%rax, (%r13,%rbx,8)
	leaq	1(%rbx,%rbx), %rbx
.LBB17_8:                               #   in Loop: Header=BB17_1 Depth=1
	movq	8(%rsp), %r12           # 8-byte Reload
	testq	%rbx, %rbx
	jle	.LBB17_12
	.p2align	4, 0x90
.LBB17_9:                               # %.lr.ph.i.i.i
                                        #   Parent Loop BB17_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rbx), %r14
	movq	%r14, %rax
	shrq	$63, %rax
	leaq	-1(%rbx,%rax), %rbp
	sarq	%rbp
	movsd	(%r13,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	callq	*%r15
	testb	%al, %al
	je	.LBB17_12
# BB#10:                                #   in Loop: Header=BB17_9 Depth=2
	movq	(%r13,%rbp,8), %rax
	movq	%rax, (%r13,%rbx,8)
	cmpq	$1, %r14
	movq	%rbp, %rbx
	jg	.LBB17_9
	jmp	.LBB17_13
	.p2align	4, 0x90
.LBB17_12:                              #   in Loop: Header=BB17_1 Depth=1
	movq	%rbx, %rbp
.LBB17_13:                              # %_ZSt10__pop_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_S7_T0_.exit
                                        #   in Loop: Header=BB17_1 Depth=1
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, (%r13,%rbp,8)
	cmpq	$8, 32(%rsp)            # 8-byte Folded Reload
	jg	.LBB17_1
.LBB17_14:                              # %._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	_ZSt11__sort_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_, .Lfunc_end17-_ZSt11__sort_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_
	.cfi_endproc

	.section	.text._ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_,"axG",@progbits,_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_,comdat
	.weak	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_
	.p2align	4, 0x90
	.type	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_,@function
_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_: # @_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi119:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi120:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi121:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi122:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi123:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi125:
	.cfi_def_cfa_offset 96
.Lcfi126:
	.cfi_offset %rbx, -56
.Lcfi127:
	.cfi_offset %r12, -48
.Lcfi128:
	.cfi_offset %r13, -40
.Lcfi129:
	.cfi_offset %r14, -32
.Lcfi130:
	.cfi_offset %r15, -24
.Lcfi131:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rdi, %r15
	subq	%r15, %rsi
	cmpq	$16, %rsi
	jl	.LBB18_24
# BB#1:
	sarq	$3, %rsi
	leaq	-2(%rsi), %rax
	shrq	$63, %rax
	leaq	-2(%rsi,%rax), %r12
	sarq	%r12
	leaq	-1(%rsi), %rax
	shrq	$63, %rax
	leaq	-1(%rsi,%rax), %r13
	sarq	%r13
	testb	$1, %sil
	movq	%r13, 8(%rsp)           # 8-byte Spill
	jne	.LBB18_14
# BB#2:                                 # %.split.us.preheader
	leaq	1(%r12,%r12), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r12, %rbp
	movq	%r12, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB18_3:                               # %.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_5 Depth 2
                                        #     Child Loop BB18_9 Depth 2
	movsd	(%r15,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	cmpq	%rbp, %r13
	movq	%rbp, %rax
	movq	%r13, %rbp
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rax, %r13
	jle	.LBB18_6
# BB#4:                                 # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB18_3 Depth=1
	movq	24(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB18_5:                               # %.lr.ph.i.us
                                        #   Parent Loop BB18_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rbx,%rbx), %rax
	leaq	2(%rbx,%rbx), %r12
	leaq	1(%rbx,%rbx), %r13
	movsd	16(%r15,%rax,8), %xmm0  # xmm0 = mem[0],zero
	movsd	8(%r15,%rax,8), %xmm1   # xmm1 = mem[0],zero
	callq	*%r14
	testb	%al, %al
	cmoveq	%r12, %r13
	movq	(%r15,%r13,8), %rax
	movq	%rax, (%r15,%rbx,8)
	cmpq	%rbp, %r13
	movq	%r13, %rbx
	jl	.LBB18_5
.LBB18_6:                               # %._crit_edge.i.us
                                        #   in Loop: Header=BB18_3 Depth=1
	movq	16(%rsp), %r12          # 8-byte Reload
	cmpq	%r12, %r13
	jne	.LBB18_8
# BB#7:                                 #   in Loop: Header=BB18_3 Depth=1
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	(%r15,%r13,8), %rax
	movq	%rax, (%r15,%r12,8)
.LBB18_8:                               #   in Loop: Header=BB18_3 Depth=1
	movq	24(%rsp), %rbp          # 8-byte Reload
	cmpq	%rbp, %r13
	jle	.LBB18_12
	.p2align	4, 0x90
.LBB18_9:                               # %.lr.ph.i.i.us
                                        #   Parent Loop BB18_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%r13), %rax
	shrq	$63, %rax
	leaq	-1(%r13,%rax), %rbx
	sarq	%rbx
	movsd	(%r15,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	callq	*%r14
	testb	%al, %al
	je	.LBB18_12
# BB#10:                                #   in Loop: Header=BB18_9 Depth=2
	movq	(%r15,%rbx,8), %rax
	movq	%rax, (%r15,%r13,8)
	cmpq	%rbp, %rbx
	movq	%rbx, %r13
	jg	.LBB18_9
	jmp	.LBB18_13
	.p2align	4, 0x90
.LBB18_12:                              #   in Loop: Header=BB18_3 Depth=1
	movq	%r13, %rbx
.LBB18_13:                              # %_ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_T0_S8_T1_T2_.exit.us
                                        #   in Loop: Header=BB18_3 Depth=1
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, (%r15,%rbx,8)
	testq	%rbp, %rbp
	leaq	-1(%rbp), %rbp
	movq	8(%rsp), %r13           # 8-byte Reload
	jne	.LBB18_3
	jmp	.LBB18_24
	.p2align	4, 0x90
.LBB18_14:                              # %.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_16 Depth 2
                                        #     Child Loop BB18_19 Depth 2
	movsd	(%r15,%r12,8), %xmm0    # xmm0 = mem[0],zero
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	cmpq	%r12, %r13
	movq	%r12, %rbx
	jle	.LBB18_23
# BB#15:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB18_14 Depth=1
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB18_16:                              # %.lr.ph.i
                                        #   Parent Loop BB18_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, %rbx
	leaq	(%rbx,%rbx), %rax
	leaq	2(%rbx,%rbx), %r12
	leaq	1(%rbx,%rbx), %rbp
	movsd	16(%r15,%rax,8), %xmm0  # xmm0 = mem[0],zero
	movsd	8(%r15,%rax,8), %xmm1   # xmm1 = mem[0],zero
	callq	*%r14
	testb	%al, %al
	cmoveq	%r12, %rbp
	movq	(%r15,%rbp,8), %rax
	movq	%rax, (%r15,%rbx,8)
	cmpq	%r13, %rbp
	jl	.LBB18_16
# BB#17:                                # %._crit_edge.i
                                        #   in Loop: Header=BB18_14 Depth=1
	movq	16(%rsp), %r12          # 8-byte Reload
	cmpq	%r12, %rbp
	jle	.LBB18_21
# BB#18:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB18_14 Depth=1
	movq	8(%rsp), %r13           # 8-byte Reload
	.p2align	4, 0x90
.LBB18_19:                              # %.lr.ph.i.i
                                        #   Parent Loop BB18_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rbp), %rax
	shrq	$63, %rax
	leaq	-1(%rbp,%rax), %rbx
	sarq	%rbx
	movsd	(%r15,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	callq	*%r14
	testb	%al, %al
	je	.LBB18_22
# BB#20:                                #   in Loop: Header=BB18_19 Depth=2
	movq	(%r15,%rbx,8), %rax
	movq	%rax, (%r15,%rbp,8)
	cmpq	%r12, %rbx
	movq	%rbx, %rbp
	jg	.LBB18_19
	jmp	.LBB18_23
	.p2align	4, 0x90
.LBB18_21:                              #   in Loop: Header=BB18_14 Depth=1
	movq	%rbp, %rbx
	movq	8(%rsp), %r13           # 8-byte Reload
	jmp	.LBB18_23
.LBB18_22:                              #   in Loop: Header=BB18_14 Depth=1
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB18_23:                              # %_ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_T0_S8_T1_T2_.exit
                                        #   in Loop: Header=BB18_14 Depth=1
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, (%r15,%rbx,8)
	testq	%r12, %r12
	leaq	-1(%r12), %r12
	jne	.LBB18_14
.LBB18_24:                              # %.loopexit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_, .Lfunc_end18-_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_
	.cfi_endproc

	.section	.text._ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_T1_,"axG",@progbits,_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_T1_,comdat
	.weak	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_T1_
	.p2align	4, 0x90
	.type	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_T1_,@function
_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_T1_: # @_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_T1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi132:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi133:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi134:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi135:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi136:
	.cfi_def_cfa_offset 48
.Lcfi137:
	.cfi_offset %rbx, -48
.Lcfi138:
	.cfi_offset %r12, -40
.Lcfi139:
	.cfi_offset %r13, -32
.Lcfi140:
	.cfi_offset %r14, -24
.Lcfi141:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	%r14, %rax
	subq	%r12, %rax
	cmpq	$129, %rax
	jl	.LBB19_31
# BB#1:                                 # %.lr.ph
	leaq	8(%r12), %r13
	.p2align	4, 0x90
.LBB19_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_11 Depth 2
                                        #       Child Loop BB19_12 Depth 3
                                        #       Child Loop BB19_14 Depth 3
	testq	%r15, %r15
	je	.LBB19_17
# BB#3:                                 #   in Loop: Header=BB19_2 Depth=1
	shrq	$4, %rax
	leaq	(%r12,%rax,8), %rdx
	leaq	-8(%r14), %rcx
	movsd	8(%r12), %xmm1          # xmm1 = mem[0],zero
	movsd	(%r12,%rax,8), %xmm2    # xmm2 = mem[0],zero
	movsd	-8(%r14), %xmm0         # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm2
	jbe	.LBB19_6
# BB#4:                                 #   in Loop: Header=BB19_2 Depth=1
	ucomisd	%xmm2, %xmm0
	movq	%rdx, %rax
	movapd	%xmm2, %xmm3
	ja	.LBB19_9
# BB#5:                                 #   in Loop: Header=BB19_2 Depth=1
	ucomisd	%xmm1, %xmm0
	cmovbeq	%r13, %rcx
	maxsd	%xmm1, %xmm0
	movq	%rcx, %rax
	jmp	.LBB19_8
	.p2align	4, 0x90
.LBB19_6:                               #   in Loop: Header=BB19_2 Depth=1
	ucomisd	%xmm1, %xmm0
	movq	%r13, %rax
	movapd	%xmm1, %xmm3
	ja	.LBB19_9
# BB#7:                                 #   in Loop: Header=BB19_2 Depth=1
	ucomisd	%xmm2, %xmm0
	cmovaq	%rcx, %rdx
	maxsd	%xmm2, %xmm0
	movq	%rdx, %rax
.LBB19_8:                               # %_ZSt22__move_median_to_firstIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_S6_S6_T0_.exit.i
                                        #   in Loop: Header=BB19_2 Depth=1
	movapd	%xmm0, %xmm3
.LBB19_9:                               # %_ZSt22__move_median_to_firstIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_S6_S6_T0_.exit.i
                                        #   in Loop: Header=BB19_2 Depth=1
	decq	%r15
	movq	(%r12), %rcx
	movsd	%xmm3, (%r12)
	movq	%rcx, (%rax)
	movq	%r14, %rax
	movq	%r13, %rcx
	jmp	.LBB19_11
	.p2align	4, 0x90
.LBB19_10:                              #   in Loop: Header=BB19_11 Depth=2
	movsd	%xmm2, (%rbx)
	movsd	%xmm0, (%rax)
.LBB19_11:                              #   Parent Loop BB19_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB19_12 Depth 3
                                        #       Child Loop BB19_14 Depth 3
	movsd	(%r12), %xmm1           # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB19_12:                              #   Parent Loop BB19_2 Depth=1
                                        #     Parent Loop BB19_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rcx
	ucomisd	%xmm0, %xmm1
	ja	.LBB19_12
# BB#13:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB19_11 Depth=2
	leaq	-8(%rcx), %rbx
	.p2align	4, 0x90
.LBB19_14:                              # %.preheader.i.i
                                        #   Parent Loop BB19_2 Depth=1
                                        #     Parent Loop BB19_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rax), %xmm2         # xmm2 = mem[0],zero
	addq	$-8, %rax
	ucomisd	%xmm1, %xmm2
	ja	.LBB19_14
# BB#15:                                #   in Loop: Header=BB19_11 Depth=2
	cmpq	%rax, %rbx
	jb	.LBB19_10
# BB#16:                                # %_ZSt27__unguarded_partition_pivotIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEET_S6_S6_T0_.exit
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_T1_
	movq	%rbx, %rax
	subq	%r12, %rax
	cmpq	$128, %rax
	movq	%rbx, %r14
	jg	.LBB19_2
	jmp	.LBB19_31
.LBB19_17:                              # %.lr.ph.i8.i
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_
	.p2align	4, 0x90
.LBB19_18:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_20 Depth 2
                                        #     Child Loop BB19_26 Depth 2
	movsd	-8(%r14), %xmm0         # xmm0 = mem[0],zero
	movq	(%r12), %rax
	movq	%rax, -8(%r14)
	leaq	-8(%r14), %r14
	movq	%r14, %r8
	subq	%r12, %r8
	movq	%r8, %rdx
	sarq	$3, %rdx
	leaq	-1(%rdx), %rcx
	cmpq	$2, %rcx
	jl	.LBB19_21
# BB#19:                                # %.lr.ph.i.i.i9.i.preheader
                                        #   in Loop: Header=BB19_18 Depth=1
	shrq	$63, %rcx
	leaq	-1(%rdx,%rcx), %rsi
	sarq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB19_20:                              # %.lr.ph.i.i.i9.i
                                        #   Parent Loop BB19_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rdi,%rdi), %rbx
	leaq	2(%rdi,%rdi), %rax
	leaq	1(%rdi,%rdi), %rcx
	movsd	8(%r12,%rbx,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	16(%r12,%rbx,8), %xmm1
	cmovbeq	%rax, %rcx
	movq	(%r12,%rcx,8), %rax
	movq	%rax, (%r12,%rdi,8)
	cmpq	%rsi, %rcx
	movq	%rcx, %rdi
	jl	.LBB19_20
	jmp	.LBB19_22
	.p2align	4, 0x90
.LBB19_21:                              #   in Loop: Header=BB19_18 Depth=1
	xorl	%ecx, %ecx
.LBB19_22:                              # %._crit_edge.i.i.i.i
                                        #   in Loop: Header=BB19_18 Depth=1
	testb	$1, %dl
	jne	.LBB19_25
# BB#23:                                #   in Loop: Header=BB19_18 Depth=1
	leaq	-2(%rdx), %rax
	shrq	$63, %rax
	leaq	-2(%rdx,%rax), %rax
	sarq	%rax
	cmpq	%rax, %rcx
	jne	.LBB19_25
# BB#24:                                #   in Loop: Header=BB19_18 Depth=1
	leaq	(%rcx,%rcx), %rax
	movq	8(%r12,%rax,8), %rax
	movq	%rax, (%r12,%rcx,8)
	leaq	1(%rcx,%rcx), %rcx
.LBB19_25:                              #   in Loop: Header=BB19_18 Depth=1
	testq	%rcx, %rcx
	jle	.LBB19_29
	.p2align	4, 0x90
.LBB19_26:                              # %.lr.ph.i.i.i.i.i
                                        #   Parent Loop BB19_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rcx), %rsi
	movq	%rsi, %rax
	shrq	$63, %rax
	leaq	-1(%rcx,%rax), %rdx
	sarq	%rdx
	movsd	(%r12,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB19_29
# BB#27:                                #   in Loop: Header=BB19_26 Depth=2
	movsd	%xmm1, (%r12,%rcx,8)
	cmpq	$1, %rsi
	movq	%rdx, %rcx
	jg	.LBB19_26
	jmp	.LBB19_30
	.p2align	4, 0x90
.LBB19_29:                              #   in Loop: Header=BB19_18 Depth=1
	movq	%rcx, %rdx
.LBB19_30:                              # %_ZSt10__pop_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_S6_T0_.exit.i.i
                                        #   in Loop: Header=BB19_18 Depth=1
	movsd	%xmm0, (%r12,%rdx,8)
	cmpq	$8, %r8
	jg	.LBB19_18
.LBB19_31:                              # %_ZSt14__partial_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_S6_T0_.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end19:
	.size	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_T1_, .Lfunc_end19-_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_T1_
	.cfi_endproc

	.section	.text._ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_,"axG",@progbits,_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_,comdat
	.weak	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_
	.p2align	4, 0x90
	.type	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_,@function
_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_: # @_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi142:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi143:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi144:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi145:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi146:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi147:
	.cfi_def_cfa_offset 64
.Lcfi148:
	.cfi_offset %rbx, -48
.Lcfi149:
	.cfi_offset %r12, -40
.Lcfi150:
	.cfi_offset %r13, -32
.Lcfi151:
	.cfi_offset %r14, -24
.Lcfi152:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	%r14, %rax
	subq	%r12, %rax
	cmpq	$129, %rax
	jl	.LBB20_20
# BB#1:
	movl	$1, %ebx
	movq	%rsp, %r15
	movq	%r12, %rdi
	.p2align	4, 0x90
.LBB20_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_18 Depth 2
	leaq	(%r12,%rbx,8), %r13
	movsd	(%r12,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	movq	(%r12), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB20_17
# BB#3:                                 # %_ZSt13copy_backwardIPdS0_ET0_T_S2_S1_.exit.i
                                        #   in Loop: Header=BB20_2 Depth=1
	leaq	(,%rbx,8), %rdx
	subq	%rdx, %rdi
	addq	$16, %rdi
	movq	%r12, %rsi
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	callq	memmove
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, (%r12)
	jmp	.LBB20_4
	.p2align	4, 0x90
.LBB20_17:                              #   in Loop: Header=BB20_2 Depth=1
	movq	%r13, %rcx
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB20_18:                              # %._crit_edge.i
                                        #   Parent Loop BB20_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	%xmm1, (%rax)
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	movq	%rcx, %rax
	leaq	-8(%rcx), %rcx
	movq	(%rsp), %rdx
	movd	%rdx, %xmm0
	ucomisd	%xmm0, %xmm1
	ja	.LBB20_18
# BB#19:                                # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_comp_iterI17less_than_functorEEEvT_T0_.exit.i
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	%rdx, 8(%rcx)
.LBB20_4:                               # %.backedge.i
                                        #   in Loop: Header=BB20_2 Depth=1
	incq	%rbx
	cmpq	$16, %rbx
	movq	%r13, %rdi
	jne	.LBB20_2
# BB#5:                                 # %_ZSt16__insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_.exit
	movq	%r12, %rax
	subq	$-128, %rax
	cmpq	%r14, %rax
	je	.LBB20_28
# BB#6:                                 # %.lr.ph.i33
	leaq	-136(%r14), %rdx
	subq	%r12, %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB20_10
# BB#7:
	movq	%rsp, %rax
	leaq	136(%r12), %rdx
	.p2align	4, 0x90
.LBB20_8:                               # =>This Inner Loop Header: Depth=1
	movq	-8(%rdx), %rsi
	movq	%rsi, (%rax)
	movq	-16(%rdx), %xmm0        # xmm0 = mem[0],zero
	leaq	-8(%rdx), %rdx
	movq	(%rsp), %rsi
	movd	%rsi, %xmm1
	ucomisd	%xmm1, %xmm0
	movq	%rdx, %rax
	ja	.LBB20_8
# BB#9:                                 # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_comp_iterI17less_than_functorEEEvT_T0_.exit.i39.prol
	movq	%rsi, (%rdx)
	addq	$136, %r12
	movq	%r12, %rax
.LBB20_10:                              # %.prol.loopexit
	testq	%rcx, %rcx
	je	.LBB20_28
# BB#11:                                # %.lr.ph.i33.new
	movq	%rsp, %rcx
	.p2align	4, 0x90
.LBB20_12:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_13 Depth 2
                                        #     Child Loop BB20_15 Depth 2
	movq	%rax, %rsi
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB20_13:                              #   Parent Loop BB20_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rdi
	movq	%rdi, (%rdx)
	movq	-8(%rsi), %xmm0         # xmm0 = mem[0],zero
	movq	%rsi, %rdx
	leaq	-8(%rsi), %rsi
	movq	(%rsp), %rdi
	movd	%rdi, %xmm1
	ucomisd	%xmm1, %xmm0
	ja	.LBB20_13
# BB#14:                                # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_comp_iterI17less_than_functorEEEvT_T0_.exit.i39
                                        #   in Loop: Header=BB20_12 Depth=1
	movq	%rdi, 8(%rsi)
	leaq	8(%rax), %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB20_15:                              #   Parent Loop BB20_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rdi
	movq	%rdi, (%rsi)
	movq	-8(%rdx), %xmm0         # xmm0 = mem[0],zero
	movq	%rdx, %rsi
	leaq	-8(%rdx), %rdx
	movq	(%rsp), %rdi
	movd	%rdi, %xmm1
	ucomisd	%xmm1, %xmm0
	ja	.LBB20_15
# BB#16:                                # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_comp_iterI17less_than_functorEEEvT_T0_.exit.i39.1
                                        #   in Loop: Header=BB20_12 Depth=1
	movq	%rdi, 8(%rdx)
	addq	$16, %rax
	cmpq	%r14, %rax
	jne	.LBB20_12
	jmp	.LBB20_28
.LBB20_20:
	cmpq	%r14, %r12
	je	.LBB20_28
# BB#21:                                # %.preheader.i
	leaq	8(%r12), %rax
	cmpq	%r14, %rax
	je	.LBB20_28
# BB#22:                                # %.lr.ph.i
	movq	%rsp, %r15
	movq	%r12, %rdi
	.p2align	4, 0x90
.LBB20_23:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_30 Depth 2
	movq	%rax, %rbx
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	movq	(%r12), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB20_29
# BB#24:                                #   in Loop: Header=BB20_23 Depth=1
	movq	%rbx, %rdx
	subq	%r12, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	je	.LBB20_26
# BB#25:                                #   in Loop: Header=BB20_23 Depth=1
	shlq	$3, %rax
	subq	%rax, %rdi
	addq	$16, %rdi
	movq	%r12, %rsi
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	callq	memmove
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
.LBB20_26:                              # %_ZSt13copy_backwardIPdS0_ET0_T_S2_S1_.exit.i20
                                        #   in Loop: Header=BB20_23 Depth=1
	movsd	%xmm1, (%r12)
	jmp	.LBB20_27
	.p2align	4, 0x90
.LBB20_29:                              #   in Loop: Header=BB20_23 Depth=1
	movq	%rbx, %rcx
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB20_30:                              # %._crit_edge.i28
                                        #   Parent Loop BB20_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	%xmm1, (%rax)
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	movq	%rcx, %rax
	leaq	-8(%rcx), %rcx
	movq	(%rsp), %rdx
	movd	%rdx, %xmm0
	ucomisd	%xmm0, %xmm1
	ja	.LBB20_30
# BB#31:                                # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_comp_iterI17less_than_functorEEEvT_T0_.exit.i29
                                        #   in Loop: Header=BB20_23 Depth=1
	movq	%rdx, 8(%rcx)
.LBB20_27:                              # %.backedge.i22
                                        #   in Loop: Header=BB20_23 Depth=1
	leaq	8(%rbx), %rax
	cmpq	%r14, %rax
	movq	%rbx, %rdi
	jne	.LBB20_23
.LBB20_28:                              # %_ZSt26__unguarded_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_.exit
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end20:
	.size	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_, .Lfunc_end20-_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_
	.cfi_endproc

	.section	.text._ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_,"axG",@progbits,_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_,comdat
	.weak	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_
	.p2align	4, 0x90
	.type	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_,@function
_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_: # @_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_
	.cfi_startproc
# BB#0:
	subq	%rdi, %rsi
	cmpq	$16, %rsi
	jl	.LBB21_23
# BB#1:
	sarq	$3, %rsi
	leaq	-2(%rsi), %rax
	shrq	$63, %rax
	leaq	-2(%rsi,%rax), %r9
	sarq	%r9
	leaq	-1(%rsi), %rax
	shrq	$63, %rax
	leaq	-1(%rsi,%rax), %r11
	sarq	%r11
	testb	$1, %sil
	jne	.LBB21_14
# BB#2:                                 # %.split.us.preheader
	leaq	1(%r9,%r9), %r8
	movq	%r9, %r10
	.p2align	4, 0x90
.LBB21_3:                               # %.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_5 Depth 2
                                        #     Child Loop BB21_9 Depth 2
	movsd	(%rdi,%r10,8), %xmm0    # xmm0 = mem[0],zero
	cmpq	%r10, %r11
	movq	%r10, %rdx
	jle	.LBB21_6
# BB#4:                                 # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB21_3 Depth=1
	movq	%r10, %rax
	.p2align	4, 0x90
.LBB21_5:                               # %.lr.ph.i.us
                                        #   Parent Loop BB21_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rax,%rax), %rsi
	leaq	2(%rax,%rax), %rcx
	leaq	1(%rax,%rax), %rdx
	movsd	8(%rdi,%rsi,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	16(%rdi,%rsi,8), %xmm1
	cmovbeq	%rcx, %rdx
	movq	(%rdi,%rdx,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	cmpq	%r11, %rdx
	movq	%rdx, %rax
	jl	.LBB21_5
.LBB21_6:                               # %._crit_edge.i.us
                                        #   in Loop: Header=BB21_3 Depth=1
	cmpq	%r9, %rdx
	jne	.LBB21_8
# BB#7:                                 #   in Loop: Header=BB21_3 Depth=1
	movq	(%rdi,%r8,8), %rax
	movq	%rax, (%rdi,%r9,8)
	movq	%r8, %rdx
.LBB21_8:                               #   in Loop: Header=BB21_3 Depth=1
	cmpq	%r10, %rdx
	jle	.LBB21_12
	.p2align	4, 0x90
.LBB21_9:                               # %.lr.ph.i.i.us
                                        #   Parent Loop BB21_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rdx), %rax
	shrq	$63, %rax
	leaq	-1(%rdx,%rax), %rax
	sarq	%rax
	movsd	(%rdi,%rax,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB21_12
# BB#10:                                #   in Loop: Header=BB21_9 Depth=2
	movsd	%xmm1, (%rdi,%rdx,8)
	cmpq	%r10, %rax
	movq	%rax, %rdx
	jg	.LBB21_9
	jmp	.LBB21_13
	.p2align	4, 0x90
.LBB21_12:                              #   in Loop: Header=BB21_3 Depth=1
	movq	%rdx, %rax
.LBB21_13:                              # %_ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_T0_S7_T1_T2_.exit.us
                                        #   in Loop: Header=BB21_3 Depth=1
	movsd	%xmm0, (%rdi,%rax,8)
	testq	%r10, %r10
	leaq	-1(%r10), %r10
	jne	.LBB21_3
	jmp	.LBB21_23
	.p2align	4, 0x90
.LBB21_14:                              # %.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_16 Depth 2
                                        #     Child Loop BB21_18 Depth 2
	movsd	(%rdi,%r9,8), %xmm0     # xmm0 = mem[0],zero
	cmpq	%r9, %r11
	movq	%r9, %rsi
	jle	.LBB21_22
# BB#15:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB21_14 Depth=1
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB21_16:                              # %.lr.ph.i
                                        #   Parent Loop BB21_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rax
	leaq	(%rax,%rax), %rcx
	leaq	2(%rax,%rax), %rsi
	leaq	1(%rax,%rax), %rdx
	movsd	8(%rdi,%rcx,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	16(%rdi,%rcx,8), %xmm1
	cmovbeq	%rsi, %rdx
	movq	(%rdi,%rdx,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	cmpq	%r11, %rdx
	jl	.LBB21_16
# BB#17:                                # %._crit_edge.i
                                        #   in Loop: Header=BB21_14 Depth=1
	cmpq	%r9, %rdx
	jle	.LBB21_21
	.p2align	4, 0x90
.LBB21_18:                              # %.lr.ph.i.i
                                        #   Parent Loop BB21_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rdx), %rax
	shrq	$63, %rax
	leaq	-1(%rdx,%rax), %rsi
	sarq	%rsi
	movsd	(%rdi,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB21_21
# BB#19:                                #   in Loop: Header=BB21_18 Depth=2
	movsd	%xmm1, (%rdi,%rdx,8)
	cmpq	%r9, %rsi
	movq	%rsi, %rdx
	jg	.LBB21_18
	jmp	.LBB21_22
	.p2align	4, 0x90
.LBB21_21:                              #   in Loop: Header=BB21_14 Depth=1
	movq	%rdx, %rsi
.LBB21_22:                              # %_ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_T0_S7_T1_T2_.exit
                                        #   in Loop: Header=BB21_14 Depth=1
	movsd	%xmm0, (%rdi,%rsi,8)
	testq	%r9, %r9
	leaq	-1(%r9), %r9
	jne	.LBB21_14
.LBB21_23:                              # %.loopexit
	retq
.Lfunc_end21:
	.size	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_, .Lfunc_end21-_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterI17less_than_functorEEEvT_S6_T0_
	.cfi_endproc

	.section	.text._ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_T1_,"axG",@progbits,_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_T1_,comdat
	.weak	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_T1_
	.p2align	4, 0x90
	.type	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_T1_,@function
_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_T1_: # @_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_T1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi153:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi154:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi155:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi156:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi157:
	.cfi_def_cfa_offset 48
.Lcfi158:
	.cfi_offset %rbx, -48
.Lcfi159:
	.cfi_offset %r12, -40
.Lcfi160:
	.cfi_offset %r13, -32
.Lcfi161:
	.cfi_offset %r14, -24
.Lcfi162:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	%r14, %rax
	subq	%r12, %rax
	cmpq	$129, %rax
	jl	.LBB22_31
# BB#1:                                 # %.lr.ph
	leaq	8(%r12), %r13
	.p2align	4, 0x90
.LBB22_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_11 Depth 2
                                        #       Child Loop BB22_12 Depth 3
                                        #       Child Loop BB22_14 Depth 3
	testq	%r15, %r15
	je	.LBB22_17
# BB#3:                                 #   in Loop: Header=BB22_2 Depth=1
	shrq	$4, %rax
	leaq	(%r12,%rax,8), %rdx
	leaq	-8(%r14), %rcx
	movsd	8(%r12), %xmm1          # xmm1 = mem[0],zero
	movsd	(%r12,%rax,8), %xmm2    # xmm2 = mem[0],zero
	movsd	-8(%r14), %xmm0         # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm2
	jbe	.LBB22_6
# BB#4:                                 #   in Loop: Header=BB22_2 Depth=1
	ucomisd	%xmm2, %xmm0
	movq	%rdx, %rax
	movapd	%xmm2, %xmm3
	ja	.LBB22_9
# BB#5:                                 #   in Loop: Header=BB22_2 Depth=1
	ucomisd	%xmm1, %xmm0
	cmovbeq	%r13, %rcx
	maxsd	%xmm1, %xmm0
	movq	%rcx, %rax
	jmp	.LBB22_8
	.p2align	4, 0x90
.LBB22_6:                               #   in Loop: Header=BB22_2 Depth=1
	ucomisd	%xmm1, %xmm0
	movq	%r13, %rax
	movapd	%xmm1, %xmm3
	ja	.LBB22_9
# BB#7:                                 #   in Loop: Header=BB22_2 Depth=1
	ucomisd	%xmm2, %xmm0
	cmovaq	%rcx, %rdx
	maxsd	%xmm2, %xmm0
	movq	%rdx, %rax
.LBB22_8:                               # %_ZSt22__move_median_to_firstIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_S6_S6_T0_.exit.i
                                        #   in Loop: Header=BB22_2 Depth=1
	movapd	%xmm0, %xmm3
.LBB22_9:                               # %_ZSt22__move_median_to_firstIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_S6_S6_T0_.exit.i
                                        #   in Loop: Header=BB22_2 Depth=1
	decq	%r15
	movq	(%r12), %rcx
	movsd	%xmm3, (%r12)
	movq	%rcx, (%rax)
	movq	%r14, %rax
	movq	%r13, %rcx
	jmp	.LBB22_11
	.p2align	4, 0x90
.LBB22_10:                              #   in Loop: Header=BB22_11 Depth=2
	movsd	%xmm2, (%rbx)
	movsd	%xmm0, (%rax)
.LBB22_11:                              #   Parent Loop BB22_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB22_12 Depth 3
                                        #       Child Loop BB22_14 Depth 3
	movsd	(%r12), %xmm1           # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB22_12:                              #   Parent Loop BB22_2 Depth=1
                                        #     Parent Loop BB22_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rcx
	ucomisd	%xmm0, %xmm1
	ja	.LBB22_12
# BB#13:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB22_11 Depth=2
	leaq	-8(%rcx), %rbx
	.p2align	4, 0x90
.LBB22_14:                              # %.preheader.i.i
                                        #   Parent Loop BB22_2 Depth=1
                                        #     Parent Loop BB22_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rax), %xmm2         # xmm2 = mem[0],zero
	addq	$-8, %rax
	ucomisd	%xmm1, %xmm2
	ja	.LBB22_14
# BB#15:                                #   in Loop: Header=BB22_11 Depth=2
	cmpq	%rax, %rbx
	jb	.LBB22_10
# BB#16:                                # %_ZSt27__unguarded_partition_pivotIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEET_S6_S6_T0_.exit
                                        #   in Loop: Header=BB22_2 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_T1_
	movq	%rbx, %rax
	subq	%r12, %rax
	cmpq	$128, %rax
	movq	%rbx, %r14
	jg	.LBB22_2
	jmp	.LBB22_31
.LBB22_17:                              # %.lr.ph.i8.i
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_
	.p2align	4, 0x90
.LBB22_18:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_20 Depth 2
                                        #     Child Loop BB22_26 Depth 2
	movsd	-8(%r14), %xmm0         # xmm0 = mem[0],zero
	movq	(%r12), %rax
	movq	%rax, -8(%r14)
	leaq	-8(%r14), %r14
	movq	%r14, %r8
	subq	%r12, %r8
	movq	%r8, %rdx
	sarq	$3, %rdx
	leaq	-1(%rdx), %rcx
	cmpq	$2, %rcx
	jl	.LBB22_21
# BB#19:                                # %.lr.ph.i.i.i9.i.preheader
                                        #   in Loop: Header=BB22_18 Depth=1
	shrq	$63, %rcx
	leaq	-1(%rdx,%rcx), %rsi
	sarq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB22_20:                              # %.lr.ph.i.i.i9.i
                                        #   Parent Loop BB22_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rdi,%rdi), %rbx
	leaq	2(%rdi,%rdi), %rax
	leaq	1(%rdi,%rdi), %rcx
	movsd	8(%r12,%rbx,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	16(%r12,%rbx,8), %xmm1
	cmovbeq	%rax, %rcx
	movq	(%r12,%rcx,8), %rax
	movq	%rax, (%r12,%rdi,8)
	cmpq	%rsi, %rcx
	movq	%rcx, %rdi
	jl	.LBB22_20
	jmp	.LBB22_22
	.p2align	4, 0x90
.LBB22_21:                              #   in Loop: Header=BB22_18 Depth=1
	xorl	%ecx, %ecx
.LBB22_22:                              # %._crit_edge.i.i.i.i
                                        #   in Loop: Header=BB22_18 Depth=1
	testb	$1, %dl
	jne	.LBB22_25
# BB#23:                                #   in Loop: Header=BB22_18 Depth=1
	leaq	-2(%rdx), %rax
	shrq	$63, %rax
	leaq	-2(%rdx,%rax), %rax
	sarq	%rax
	cmpq	%rax, %rcx
	jne	.LBB22_25
# BB#24:                                #   in Loop: Header=BB22_18 Depth=1
	leaq	(%rcx,%rcx), %rax
	movq	8(%r12,%rax,8), %rax
	movq	%rax, (%r12,%rcx,8)
	leaq	1(%rcx,%rcx), %rcx
.LBB22_25:                              #   in Loop: Header=BB22_18 Depth=1
	testq	%rcx, %rcx
	jle	.LBB22_29
	.p2align	4, 0x90
.LBB22_26:                              # %.lr.ph.i.i.i.i.i
                                        #   Parent Loop BB22_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rcx), %rsi
	movq	%rsi, %rax
	shrq	$63, %rax
	leaq	-1(%rcx,%rax), %rdx
	sarq	%rdx
	movsd	(%r12,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB22_29
# BB#27:                                #   in Loop: Header=BB22_26 Depth=2
	movsd	%xmm1, (%r12,%rcx,8)
	cmpq	$1, %rsi
	movq	%rdx, %rcx
	jg	.LBB22_26
	jmp	.LBB22_30
	.p2align	4, 0x90
.LBB22_29:                              #   in Loop: Header=BB22_18 Depth=1
	movq	%rcx, %rdx
.LBB22_30:                              # %_ZSt10__pop_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_S6_T0_.exit.i.i
                                        #   in Loop: Header=BB22_18 Depth=1
	movsd	%xmm0, (%r12,%rdx,8)
	cmpq	$8, %r8
	jg	.LBB22_18
.LBB22_31:                              # %_ZSt14__partial_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_S6_T0_.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end22:
	.size	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_T1_, .Lfunc_end22-_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_T1_
	.cfi_endproc

	.section	.text._ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_,"axG",@progbits,_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_,comdat
	.weak	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_
	.p2align	4, 0x90
	.type	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_,@function
_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_: # @_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi163:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi164:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi165:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi166:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi167:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi168:
	.cfi_def_cfa_offset 64
.Lcfi169:
	.cfi_offset %rbx, -48
.Lcfi170:
	.cfi_offset %r12, -40
.Lcfi171:
	.cfi_offset %r13, -32
.Lcfi172:
	.cfi_offset %r14, -24
.Lcfi173:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	%r14, %rax
	subq	%r12, %rax
	cmpq	$129, %rax
	jl	.LBB23_20
# BB#1:
	movl	$1, %ebx
	movq	%rsp, %r15
	movq	%r12, %rdi
	.p2align	4, 0x90
.LBB23_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_18 Depth 2
	leaq	(%r12,%rbx,8), %r13
	movsd	(%r12,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	movq	(%r12), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB23_17
# BB#3:                                 # %_ZSt13copy_backwardIPdS0_ET0_T_S2_S1_.exit.i
                                        #   in Loop: Header=BB23_2 Depth=1
	leaq	(,%rbx,8), %rdx
	subq	%rdx, %rdi
	addq	$16, %rdi
	movq	%r12, %rsi
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	callq	memmove
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, (%r12)
	jmp	.LBB23_4
	.p2align	4, 0x90
.LBB23_17:                              #   in Loop: Header=BB23_2 Depth=1
	movq	%r13, %rcx
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB23_18:                              # %._crit_edge.i
                                        #   Parent Loop BB23_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	%xmm1, (%rax)
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	movq	%rcx, %rax
	leaq	-8(%rcx), %rcx
	movq	(%rsp), %rdx
	movd	%rdx, %xmm0
	ucomisd	%xmm0, %xmm1
	ja	.LBB23_18
# BB#19:                                # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_comp_iterI24inline_less_than_functorEEEvT_T0_.exit.i
                                        #   in Loop: Header=BB23_2 Depth=1
	movq	%rdx, 8(%rcx)
.LBB23_4:                               # %.backedge.i
                                        #   in Loop: Header=BB23_2 Depth=1
	incq	%rbx
	cmpq	$16, %rbx
	movq	%r13, %rdi
	jne	.LBB23_2
# BB#5:                                 # %_ZSt16__insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_.exit
	movq	%r12, %rax
	subq	$-128, %rax
	cmpq	%r14, %rax
	je	.LBB23_28
# BB#6:                                 # %.lr.ph.i33
	leaq	-136(%r14), %rdx
	subq	%r12, %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB23_10
# BB#7:
	movq	%rsp, %rax
	leaq	136(%r12), %rdx
	.p2align	4, 0x90
.LBB23_8:                               # =>This Inner Loop Header: Depth=1
	movq	-8(%rdx), %rsi
	movq	%rsi, (%rax)
	movq	-16(%rdx), %xmm0        # xmm0 = mem[0],zero
	leaq	-8(%rdx), %rdx
	movq	(%rsp), %rsi
	movd	%rsi, %xmm1
	ucomisd	%xmm1, %xmm0
	movq	%rdx, %rax
	ja	.LBB23_8
# BB#9:                                 # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_comp_iterI24inline_less_than_functorEEEvT_T0_.exit.i39.prol
	movq	%rsi, (%rdx)
	addq	$136, %r12
	movq	%r12, %rax
.LBB23_10:                              # %.prol.loopexit
	testq	%rcx, %rcx
	je	.LBB23_28
# BB#11:                                # %.lr.ph.i33.new
	movq	%rsp, %rcx
	.p2align	4, 0x90
.LBB23_12:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_13 Depth 2
                                        #     Child Loop BB23_15 Depth 2
	movq	%rax, %rsi
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB23_13:                              #   Parent Loop BB23_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rdi
	movq	%rdi, (%rdx)
	movq	-8(%rsi), %xmm0         # xmm0 = mem[0],zero
	movq	%rsi, %rdx
	leaq	-8(%rsi), %rsi
	movq	(%rsp), %rdi
	movd	%rdi, %xmm1
	ucomisd	%xmm1, %xmm0
	ja	.LBB23_13
# BB#14:                                # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_comp_iterI24inline_less_than_functorEEEvT_T0_.exit.i39
                                        #   in Loop: Header=BB23_12 Depth=1
	movq	%rdi, 8(%rsi)
	leaq	8(%rax), %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB23_15:                              #   Parent Loop BB23_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rdi
	movq	%rdi, (%rsi)
	movq	-8(%rdx), %xmm0         # xmm0 = mem[0],zero
	movq	%rdx, %rsi
	leaq	-8(%rdx), %rdx
	movq	(%rsp), %rdi
	movd	%rdi, %xmm1
	ucomisd	%xmm1, %xmm0
	ja	.LBB23_15
# BB#16:                                # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_comp_iterI24inline_less_than_functorEEEvT_T0_.exit.i39.1
                                        #   in Loop: Header=BB23_12 Depth=1
	movq	%rdi, 8(%rdx)
	addq	$16, %rax
	cmpq	%r14, %rax
	jne	.LBB23_12
	jmp	.LBB23_28
.LBB23_20:
	cmpq	%r14, %r12
	je	.LBB23_28
# BB#21:                                # %.preheader.i
	leaq	8(%r12), %rax
	cmpq	%r14, %rax
	je	.LBB23_28
# BB#22:                                # %.lr.ph.i
	movq	%rsp, %r15
	movq	%r12, %rdi
	.p2align	4, 0x90
.LBB23_23:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_30 Depth 2
	movq	%rax, %rbx
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	movq	(%r12), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB23_29
# BB#24:                                #   in Loop: Header=BB23_23 Depth=1
	movq	%rbx, %rdx
	subq	%r12, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	je	.LBB23_26
# BB#25:                                #   in Loop: Header=BB23_23 Depth=1
	shlq	$3, %rax
	subq	%rax, %rdi
	addq	$16, %rdi
	movq	%r12, %rsi
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	callq	memmove
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
.LBB23_26:                              # %_ZSt13copy_backwardIPdS0_ET0_T_S2_S1_.exit.i20
                                        #   in Loop: Header=BB23_23 Depth=1
	movsd	%xmm1, (%r12)
	jmp	.LBB23_27
	.p2align	4, 0x90
.LBB23_29:                              #   in Loop: Header=BB23_23 Depth=1
	movq	%rbx, %rcx
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB23_30:                              # %._crit_edge.i28
                                        #   Parent Loop BB23_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	%xmm1, (%rax)
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	movq	%rcx, %rax
	leaq	-8(%rcx), %rcx
	movq	(%rsp), %rdx
	movd	%rdx, %xmm0
	ucomisd	%xmm0, %xmm1
	ja	.LBB23_30
# BB#31:                                # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_comp_iterI24inline_less_than_functorEEEvT_T0_.exit.i29
                                        #   in Loop: Header=BB23_23 Depth=1
	movq	%rdx, 8(%rcx)
.LBB23_27:                              # %.backedge.i22
                                        #   in Loop: Header=BB23_23 Depth=1
	leaq	8(%rbx), %rax
	cmpq	%r14, %rax
	movq	%rbx, %rdi
	jne	.LBB23_23
.LBB23_28:                              # %_ZSt26__unguarded_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_.exit
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end23:
	.size	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_, .Lfunc_end23-_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_
	.cfi_endproc

	.section	.text._ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_,"axG",@progbits,_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_,comdat
	.weak	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_
	.p2align	4, 0x90
	.type	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_,@function
_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_: # @_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_
	.cfi_startproc
# BB#0:
	subq	%rdi, %rsi
	cmpq	$16, %rsi
	jl	.LBB24_23
# BB#1:
	sarq	$3, %rsi
	leaq	-2(%rsi), %rax
	shrq	$63, %rax
	leaq	-2(%rsi,%rax), %r9
	sarq	%r9
	leaq	-1(%rsi), %rax
	shrq	$63, %rax
	leaq	-1(%rsi,%rax), %r11
	sarq	%r11
	testb	$1, %sil
	jne	.LBB24_14
# BB#2:                                 # %.split.us.preheader
	leaq	1(%r9,%r9), %r8
	movq	%r9, %r10
	.p2align	4, 0x90
.LBB24_3:                               # %.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_5 Depth 2
                                        #     Child Loop BB24_9 Depth 2
	movsd	(%rdi,%r10,8), %xmm0    # xmm0 = mem[0],zero
	cmpq	%r10, %r11
	movq	%r10, %rdx
	jle	.LBB24_6
# BB#4:                                 # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	%r10, %rax
	.p2align	4, 0x90
.LBB24_5:                               # %.lr.ph.i.us
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rax,%rax), %rsi
	leaq	2(%rax,%rax), %rcx
	leaq	1(%rax,%rax), %rdx
	movsd	8(%rdi,%rsi,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	16(%rdi,%rsi,8), %xmm1
	cmovbeq	%rcx, %rdx
	movq	(%rdi,%rdx,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	cmpq	%r11, %rdx
	movq	%rdx, %rax
	jl	.LBB24_5
.LBB24_6:                               # %._crit_edge.i.us
                                        #   in Loop: Header=BB24_3 Depth=1
	cmpq	%r9, %rdx
	jne	.LBB24_8
# BB#7:                                 #   in Loop: Header=BB24_3 Depth=1
	movq	(%rdi,%r8,8), %rax
	movq	%rax, (%rdi,%r9,8)
	movq	%r8, %rdx
.LBB24_8:                               #   in Loop: Header=BB24_3 Depth=1
	cmpq	%r10, %rdx
	jle	.LBB24_12
	.p2align	4, 0x90
.LBB24_9:                               # %.lr.ph.i.i.us
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rdx), %rax
	shrq	$63, %rax
	leaq	-1(%rdx,%rax), %rax
	sarq	%rax
	movsd	(%rdi,%rax,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB24_12
# BB#10:                                #   in Loop: Header=BB24_9 Depth=2
	movsd	%xmm1, (%rdi,%rdx,8)
	cmpq	%r10, %rax
	movq	%rax, %rdx
	jg	.LBB24_9
	jmp	.LBB24_13
	.p2align	4, 0x90
.LBB24_12:                              #   in Loop: Header=BB24_3 Depth=1
	movq	%rdx, %rax
.LBB24_13:                              # %_ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_T0_S7_T1_T2_.exit.us
                                        #   in Loop: Header=BB24_3 Depth=1
	movsd	%xmm0, (%rdi,%rax,8)
	testq	%r10, %r10
	leaq	-1(%r10), %r10
	jne	.LBB24_3
	jmp	.LBB24_23
	.p2align	4, 0x90
.LBB24_14:                              # %.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_16 Depth 2
                                        #     Child Loop BB24_18 Depth 2
	movsd	(%rdi,%r9,8), %xmm0     # xmm0 = mem[0],zero
	cmpq	%r9, %r11
	movq	%r9, %rsi
	jle	.LBB24_22
# BB#15:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB24_14 Depth=1
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB24_16:                              # %.lr.ph.i
                                        #   Parent Loop BB24_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rax
	leaq	(%rax,%rax), %rcx
	leaq	2(%rax,%rax), %rsi
	leaq	1(%rax,%rax), %rdx
	movsd	8(%rdi,%rcx,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	16(%rdi,%rcx,8), %xmm1
	cmovbeq	%rsi, %rdx
	movq	(%rdi,%rdx,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	cmpq	%r11, %rdx
	jl	.LBB24_16
# BB#17:                                # %._crit_edge.i
                                        #   in Loop: Header=BB24_14 Depth=1
	cmpq	%r9, %rdx
	jle	.LBB24_21
	.p2align	4, 0x90
.LBB24_18:                              # %.lr.ph.i.i
                                        #   Parent Loop BB24_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rdx), %rax
	shrq	$63, %rax
	leaq	-1(%rdx,%rax), %rsi
	sarq	%rsi
	movsd	(%rdi,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB24_21
# BB#19:                                #   in Loop: Header=BB24_18 Depth=2
	movsd	%xmm1, (%rdi,%rdx,8)
	cmpq	%r9, %rsi
	movq	%rsi, %rdx
	jg	.LBB24_18
	jmp	.LBB24_22
	.p2align	4, 0x90
.LBB24_21:                              #   in Loop: Header=BB24_14 Depth=1
	movq	%rdx, %rsi
.LBB24_22:                              # %_ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_T0_S7_T1_T2_.exit
                                        #   in Loop: Header=BB24_14 Depth=1
	movsd	%xmm0, (%rdi,%rsi,8)
	testq	%r9, %r9
	leaq	-1(%r9), %r9
	jne	.LBB24_14
.LBB24_23:                              # %.loopexit
	retq
.Lfunc_end24:
	.size	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_, .Lfunc_end24-_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterI24inline_less_than_functorEEEvT_S6_T0_
	.cfi_endproc

	.section	.text._ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_T1_,"axG",@progbits,_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_T1_,comdat
	.weak	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_T1_
	.p2align	4, 0x90
	.type	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_T1_,@function
_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_T1_: # @_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_T1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi174:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi175:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi176:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi177:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi178:
	.cfi_def_cfa_offset 48
.Lcfi179:
	.cfi_offset %rbx, -48
.Lcfi180:
	.cfi_offset %r12, -40
.Lcfi181:
	.cfi_offset %r13, -32
.Lcfi182:
	.cfi_offset %r14, -24
.Lcfi183:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	%r14, %rax
	subq	%r12, %rax
	cmpq	$129, %rax
	jl	.LBB25_31
# BB#1:                                 # %.lr.ph
	leaq	8(%r12), %r13
	.p2align	4, 0x90
.LBB25_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_11 Depth 2
                                        #       Child Loop BB25_12 Depth 3
                                        #       Child Loop BB25_14 Depth 3
	testq	%r15, %r15
	je	.LBB25_17
# BB#3:                                 #   in Loop: Header=BB25_2 Depth=1
	shrq	$4, %rax
	leaq	(%r12,%rax,8), %rdx
	leaq	-8(%r14), %rcx
	movsd	8(%r12), %xmm1          # xmm1 = mem[0],zero
	movsd	(%r12,%rax,8), %xmm2    # xmm2 = mem[0],zero
	movsd	-8(%r14), %xmm0         # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm2
	jbe	.LBB25_6
# BB#4:                                 #   in Loop: Header=BB25_2 Depth=1
	ucomisd	%xmm2, %xmm0
	movq	%rdx, %rax
	movapd	%xmm2, %xmm3
	ja	.LBB25_9
# BB#5:                                 #   in Loop: Header=BB25_2 Depth=1
	ucomisd	%xmm1, %xmm0
	cmovbeq	%r13, %rcx
	maxsd	%xmm1, %xmm0
	movq	%rcx, %rax
	jmp	.LBB25_8
	.p2align	4, 0x90
.LBB25_6:                               #   in Loop: Header=BB25_2 Depth=1
	ucomisd	%xmm1, %xmm0
	movq	%r13, %rax
	movapd	%xmm1, %xmm3
	ja	.LBB25_9
# BB#7:                                 #   in Loop: Header=BB25_2 Depth=1
	ucomisd	%xmm2, %xmm0
	cmovaq	%rcx, %rdx
	maxsd	%xmm2, %xmm0
	movq	%rdx, %rax
.LBB25_8:                               # %_ZSt22__move_median_to_firstIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_S7_S7_T0_.exit.i
                                        #   in Loop: Header=BB25_2 Depth=1
	movapd	%xmm0, %xmm3
.LBB25_9:                               # %_ZSt22__move_median_to_firstIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_S7_S7_T0_.exit.i
                                        #   in Loop: Header=BB25_2 Depth=1
	decq	%r15
	movq	(%r12), %rcx
	movsd	%xmm3, (%r12)
	movq	%rcx, (%rax)
	movq	%r14, %rax
	movq	%r13, %rcx
	jmp	.LBB25_11
	.p2align	4, 0x90
.LBB25_10:                              #   in Loop: Header=BB25_11 Depth=2
	movsd	%xmm2, (%rbx)
	movsd	%xmm0, (%rax)
.LBB25_11:                              #   Parent Loop BB25_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB25_12 Depth 3
                                        #       Child Loop BB25_14 Depth 3
	movsd	(%r12), %xmm1           # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB25_12:                              #   Parent Loop BB25_2 Depth=1
                                        #     Parent Loop BB25_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rcx
	ucomisd	%xmm0, %xmm1
	ja	.LBB25_12
# BB#13:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB25_11 Depth=2
	leaq	-8(%rcx), %rbx
	.p2align	4, 0x90
.LBB25_14:                              # %.preheader.i.i
                                        #   Parent Loop BB25_2 Depth=1
                                        #     Parent Loop BB25_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rax), %xmm2         # xmm2 = mem[0],zero
	addq	$-8, %rax
	ucomisd	%xmm1, %xmm2
	ja	.LBB25_14
# BB#15:                                #   in Loop: Header=BB25_11 Depth=2
	cmpq	%rax, %rbx
	jb	.LBB25_10
# BB#16:                                # %_ZSt27__unguarded_partition_pivotIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEET_S7_S7_T0_.exit
                                        #   in Loop: Header=BB25_2 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_T1_
	movq	%rbx, %rax
	subq	%r12, %rax
	cmpq	$128, %rax
	movq	%rbx, %r14
	jg	.LBB25_2
	jmp	.LBB25_31
.LBB25_17:                              # %.lr.ph.i8.i
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_
	.p2align	4, 0x90
.LBB25_18:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_20 Depth 2
                                        #     Child Loop BB25_26 Depth 2
	movsd	-8(%r14), %xmm0         # xmm0 = mem[0],zero
	movq	(%r12), %rax
	movq	%rax, -8(%r14)
	leaq	-8(%r14), %r14
	movq	%r14, %r8
	subq	%r12, %r8
	movq	%r8, %rdx
	sarq	$3, %rdx
	leaq	-1(%rdx), %rcx
	cmpq	$2, %rcx
	jl	.LBB25_21
# BB#19:                                # %.lr.ph.i.i.i9.i.preheader
                                        #   in Loop: Header=BB25_18 Depth=1
	shrq	$63, %rcx
	leaq	-1(%rdx,%rcx), %rsi
	sarq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB25_20:                              # %.lr.ph.i.i.i9.i
                                        #   Parent Loop BB25_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rdi,%rdi), %rbx
	leaq	2(%rdi,%rdi), %rax
	leaq	1(%rdi,%rdi), %rcx
	movsd	8(%r12,%rbx,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	16(%r12,%rbx,8), %xmm1
	cmovbeq	%rax, %rcx
	movq	(%r12,%rcx,8), %rax
	movq	%rax, (%r12,%rdi,8)
	cmpq	%rsi, %rcx
	movq	%rcx, %rdi
	jl	.LBB25_20
	jmp	.LBB25_22
	.p2align	4, 0x90
.LBB25_21:                              #   in Loop: Header=BB25_18 Depth=1
	xorl	%ecx, %ecx
.LBB25_22:                              # %._crit_edge.i.i.i.i
                                        #   in Loop: Header=BB25_18 Depth=1
	testb	$1, %dl
	jne	.LBB25_25
# BB#23:                                #   in Loop: Header=BB25_18 Depth=1
	leaq	-2(%rdx), %rax
	shrq	$63, %rax
	leaq	-2(%rdx,%rax), %rax
	sarq	%rax
	cmpq	%rax, %rcx
	jne	.LBB25_25
# BB#24:                                #   in Loop: Header=BB25_18 Depth=1
	leaq	(%rcx,%rcx), %rax
	movq	8(%r12,%rax,8), %rax
	movq	%rax, (%r12,%rcx,8)
	leaq	1(%rcx,%rcx), %rcx
.LBB25_25:                              #   in Loop: Header=BB25_18 Depth=1
	testq	%rcx, %rcx
	jle	.LBB25_29
	.p2align	4, 0x90
.LBB25_26:                              # %.lr.ph.i.i.i.i.i
                                        #   Parent Loop BB25_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rcx), %rsi
	movq	%rsi, %rax
	shrq	$63, %rax
	leaq	-1(%rcx,%rax), %rdx
	sarq	%rdx
	movsd	(%r12,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB25_29
# BB#27:                                #   in Loop: Header=BB25_26 Depth=2
	movsd	%xmm1, (%r12,%rcx,8)
	cmpq	$1, %rsi
	movq	%rdx, %rcx
	jg	.LBB25_26
	jmp	.LBB25_30
	.p2align	4, 0x90
.LBB25_29:                              #   in Loop: Header=BB25_18 Depth=1
	movq	%rcx, %rdx
.LBB25_30:                              # %_ZSt10__pop_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_S7_T0_.exit.i.i
                                        #   in Loop: Header=BB25_18 Depth=1
	movsd	%xmm0, (%r12,%rdx,8)
	cmpq	$8, %r8
	jg	.LBB25_18
.LBB25_31:                              # %_ZSt14__partial_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_S7_T0_.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end25:
	.size	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_T1_, .Lfunc_end25-_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_T1_
	.cfi_endproc

	.section	.text._ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_,"axG",@progbits,_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_,comdat
	.weak	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_
	.p2align	4, 0x90
	.type	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_,@function
_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_: # @_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi184:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi185:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi186:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi187:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi188:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi189:
	.cfi_def_cfa_offset 64
.Lcfi190:
	.cfi_offset %rbx, -48
.Lcfi191:
	.cfi_offset %r12, -40
.Lcfi192:
	.cfi_offset %r13, -32
.Lcfi193:
	.cfi_offset %r14, -24
.Lcfi194:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	%r14, %rax
	subq	%r12, %rax
	cmpq	$129, %rax
	jl	.LBB26_20
# BB#1:
	movl	$1, %ebx
	movq	%rsp, %r15
	movq	%r12, %rdi
	.p2align	4, 0x90
.LBB26_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_18 Depth 2
	leaq	(%r12,%rbx,8), %r13
	movsd	(%r12,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	movq	(%r12), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB26_17
# BB#3:                                 # %_ZSt13copy_backwardIPdS0_ET0_T_S2_S1_.exit.i
                                        #   in Loop: Header=BB26_2 Depth=1
	leaq	(,%rbx,8), %rdx
	subq	%rdx, %rdi
	addq	$16, %rdi
	movq	%r12, %rsi
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	callq	memmove
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, (%r12)
	jmp	.LBB26_4
	.p2align	4, 0x90
.LBB26_17:                              #   in Loop: Header=BB26_2 Depth=1
	movq	%r13, %rcx
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB26_18:                              # %._crit_edge.i
                                        #   Parent Loop BB26_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	%xmm1, (%rax)
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	movq	%rcx, %rax
	leaq	-8(%rcx), %rcx
	movq	(%rsp), %rdx
	movd	%rdx, %xmm0
	ucomisd	%xmm0, %xmm1
	ja	.LBB26_18
# BB#19:                                # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_comp_iterISt4lessIdEEEEvT_T0_.exit.i
                                        #   in Loop: Header=BB26_2 Depth=1
	movq	%rdx, 8(%rcx)
.LBB26_4:                               # %.backedge.i
                                        #   in Loop: Header=BB26_2 Depth=1
	incq	%rbx
	cmpq	$16, %rbx
	movq	%r13, %rdi
	jne	.LBB26_2
# BB#5:                                 # %_ZSt16__insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_.exit
	movq	%r12, %rax
	subq	$-128, %rax
	cmpq	%r14, %rax
	je	.LBB26_28
# BB#6:                                 # %.lr.ph.i33
	leaq	-136(%r14), %rdx
	subq	%r12, %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB26_10
# BB#7:
	movq	%rsp, %rax
	leaq	136(%r12), %rdx
	.p2align	4, 0x90
.LBB26_8:                               # =>This Inner Loop Header: Depth=1
	movq	-8(%rdx), %rsi
	movq	%rsi, (%rax)
	movq	-16(%rdx), %xmm0        # xmm0 = mem[0],zero
	leaq	-8(%rdx), %rdx
	movq	(%rsp), %rsi
	movd	%rsi, %xmm1
	ucomisd	%xmm1, %xmm0
	movq	%rdx, %rax
	ja	.LBB26_8
# BB#9:                                 # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_comp_iterISt4lessIdEEEEvT_T0_.exit.i39.prol
	movq	%rsi, (%rdx)
	addq	$136, %r12
	movq	%r12, %rax
.LBB26_10:                              # %.prol.loopexit
	testq	%rcx, %rcx
	je	.LBB26_28
# BB#11:                                # %.lr.ph.i33.new
	movq	%rsp, %rcx
	.p2align	4, 0x90
.LBB26_12:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_13 Depth 2
                                        #     Child Loop BB26_15 Depth 2
	movq	%rax, %rsi
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB26_13:                              #   Parent Loop BB26_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rdi
	movq	%rdi, (%rdx)
	movq	-8(%rsi), %xmm0         # xmm0 = mem[0],zero
	movq	%rsi, %rdx
	leaq	-8(%rsi), %rsi
	movq	(%rsp), %rdi
	movd	%rdi, %xmm1
	ucomisd	%xmm1, %xmm0
	ja	.LBB26_13
# BB#14:                                # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_comp_iterISt4lessIdEEEEvT_T0_.exit.i39
                                        #   in Loop: Header=BB26_12 Depth=1
	movq	%rdi, 8(%rsi)
	leaq	8(%rax), %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB26_15:                              #   Parent Loop BB26_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rdi
	movq	%rdi, (%rsi)
	movq	-8(%rdx), %xmm0         # xmm0 = mem[0],zero
	movq	%rdx, %rsi
	leaq	-8(%rdx), %rdx
	movq	(%rsp), %rdi
	movd	%rdi, %xmm1
	ucomisd	%xmm1, %xmm0
	ja	.LBB26_15
# BB#16:                                # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_comp_iterISt4lessIdEEEEvT_T0_.exit.i39.1
                                        #   in Loop: Header=BB26_12 Depth=1
	movq	%rdi, 8(%rdx)
	addq	$16, %rax
	cmpq	%r14, %rax
	jne	.LBB26_12
	jmp	.LBB26_28
.LBB26_20:
	cmpq	%r14, %r12
	je	.LBB26_28
# BB#21:                                # %.preheader.i
	leaq	8(%r12), %rax
	cmpq	%r14, %rax
	je	.LBB26_28
# BB#22:                                # %.lr.ph.i
	movq	%rsp, %r15
	movq	%r12, %rdi
	.p2align	4, 0x90
.LBB26_23:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_30 Depth 2
	movq	%rax, %rbx
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	movq	(%r12), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB26_29
# BB#24:                                #   in Loop: Header=BB26_23 Depth=1
	movq	%rbx, %rdx
	subq	%r12, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	je	.LBB26_26
# BB#25:                                #   in Loop: Header=BB26_23 Depth=1
	shlq	$3, %rax
	subq	%rax, %rdi
	addq	$16, %rdi
	movq	%r12, %rsi
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	callq	memmove
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
.LBB26_26:                              # %_ZSt13copy_backwardIPdS0_ET0_T_S2_S1_.exit.i20
                                        #   in Loop: Header=BB26_23 Depth=1
	movsd	%xmm1, (%r12)
	jmp	.LBB26_27
	.p2align	4, 0x90
.LBB26_29:                              #   in Loop: Header=BB26_23 Depth=1
	movq	%rbx, %rcx
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB26_30:                              # %._crit_edge.i28
                                        #   Parent Loop BB26_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	%xmm1, (%rax)
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	movq	%rcx, %rax
	leaq	-8(%rcx), %rcx
	movq	(%rsp), %rdx
	movd	%rdx, %xmm0
	ucomisd	%xmm0, %xmm1
	ja	.LBB26_30
# BB#31:                                # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_comp_iterISt4lessIdEEEEvT_T0_.exit.i29
                                        #   in Loop: Header=BB26_23 Depth=1
	movq	%rdx, 8(%rcx)
.LBB26_27:                              # %.backedge.i22
                                        #   in Loop: Header=BB26_23 Depth=1
	leaq	8(%rbx), %rax
	cmpq	%r14, %rax
	movq	%rbx, %rdi
	jne	.LBB26_23
.LBB26_28:                              # %_ZSt26__unguarded_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_.exit
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end26:
	.size	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_, .Lfunc_end26-_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_
	.cfi_endproc

	.section	.text._ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_,"axG",@progbits,_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_,comdat
	.weak	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_
	.p2align	4, 0x90
	.type	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_,@function
_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_: # @_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_
	.cfi_startproc
# BB#0:
	subq	%rdi, %rsi
	cmpq	$16, %rsi
	jl	.LBB27_23
# BB#1:
	sarq	$3, %rsi
	leaq	-2(%rsi), %rax
	shrq	$63, %rax
	leaq	-2(%rsi,%rax), %r9
	sarq	%r9
	leaq	-1(%rsi), %rax
	shrq	$63, %rax
	leaq	-1(%rsi,%rax), %r11
	sarq	%r11
	testb	$1, %sil
	jne	.LBB27_14
# BB#2:                                 # %.split.us.preheader
	leaq	1(%r9,%r9), %r8
	movq	%r9, %r10
	.p2align	4, 0x90
.LBB27_3:                               # %.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_5 Depth 2
                                        #     Child Loop BB27_9 Depth 2
	movsd	(%rdi,%r10,8), %xmm0    # xmm0 = mem[0],zero
	cmpq	%r10, %r11
	movq	%r10, %rdx
	jle	.LBB27_6
# BB#4:                                 # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB27_3 Depth=1
	movq	%r10, %rax
	.p2align	4, 0x90
.LBB27_5:                               # %.lr.ph.i.us
                                        #   Parent Loop BB27_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rax,%rax), %rsi
	leaq	2(%rax,%rax), %rcx
	leaq	1(%rax,%rax), %rdx
	movsd	8(%rdi,%rsi,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	16(%rdi,%rsi,8), %xmm1
	cmovbeq	%rcx, %rdx
	movq	(%rdi,%rdx,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	cmpq	%r11, %rdx
	movq	%rdx, %rax
	jl	.LBB27_5
.LBB27_6:                               # %._crit_edge.i.us
                                        #   in Loop: Header=BB27_3 Depth=1
	cmpq	%r9, %rdx
	jne	.LBB27_8
# BB#7:                                 #   in Loop: Header=BB27_3 Depth=1
	movq	(%rdi,%r8,8), %rax
	movq	%rax, (%rdi,%r9,8)
	movq	%r8, %rdx
.LBB27_8:                               #   in Loop: Header=BB27_3 Depth=1
	cmpq	%r10, %rdx
	jle	.LBB27_12
	.p2align	4, 0x90
.LBB27_9:                               # %.lr.ph.i.i.us
                                        #   Parent Loop BB27_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rdx), %rax
	shrq	$63, %rax
	leaq	-1(%rdx,%rax), %rax
	sarq	%rax
	movsd	(%rdi,%rax,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB27_12
# BB#10:                                #   in Loop: Header=BB27_9 Depth=2
	movsd	%xmm1, (%rdi,%rdx,8)
	cmpq	%r10, %rax
	movq	%rax, %rdx
	jg	.LBB27_9
	jmp	.LBB27_13
	.p2align	4, 0x90
.LBB27_12:                              #   in Loop: Header=BB27_3 Depth=1
	movq	%rdx, %rax
.LBB27_13:                              # %_ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_T0_S8_T1_T2_.exit.us
                                        #   in Loop: Header=BB27_3 Depth=1
	movsd	%xmm0, (%rdi,%rax,8)
	testq	%r10, %r10
	leaq	-1(%r10), %r10
	jne	.LBB27_3
	jmp	.LBB27_23
	.p2align	4, 0x90
.LBB27_14:                              # %.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_16 Depth 2
                                        #     Child Loop BB27_18 Depth 2
	movsd	(%rdi,%r9,8), %xmm0     # xmm0 = mem[0],zero
	cmpq	%r9, %r11
	movq	%r9, %rsi
	jle	.LBB27_22
# BB#15:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB27_14 Depth=1
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB27_16:                              # %.lr.ph.i
                                        #   Parent Loop BB27_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rax
	leaq	(%rax,%rax), %rcx
	leaq	2(%rax,%rax), %rsi
	leaq	1(%rax,%rax), %rdx
	movsd	8(%rdi,%rcx,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	16(%rdi,%rcx,8), %xmm1
	cmovbeq	%rsi, %rdx
	movq	(%rdi,%rdx,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	cmpq	%r11, %rdx
	jl	.LBB27_16
# BB#17:                                # %._crit_edge.i
                                        #   in Loop: Header=BB27_14 Depth=1
	cmpq	%r9, %rdx
	jle	.LBB27_21
	.p2align	4, 0x90
.LBB27_18:                              # %.lr.ph.i.i
                                        #   Parent Loop BB27_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rdx), %rax
	shrq	$63, %rax
	leaq	-1(%rdx,%rax), %rsi
	sarq	%rsi
	movsd	(%rdi,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB27_21
# BB#19:                                #   in Loop: Header=BB27_18 Depth=2
	movsd	%xmm1, (%rdi,%rdx,8)
	cmpq	%r9, %rsi
	movq	%rsi, %rdx
	jg	.LBB27_18
	jmp	.LBB27_22
	.p2align	4, 0x90
.LBB27_21:                              #   in Loop: Header=BB27_14 Depth=1
	movq	%rdx, %rsi
.LBB27_22:                              # %_ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_T0_S8_T1_T2_.exit
                                        #   in Loop: Header=BB27_14 Depth=1
	movsd	%xmm0, (%rdi,%rsi,8)
	testq	%r9, %r9
	leaq	-1(%r9), %r9
	jne	.LBB27_14
.LBB27_23:                              # %.loopexit
	retq
.Lfunc_end27:
	.size	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_, .Lfunc_end27-_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_comp_iterISt4lessIdEEEEvT_S7_T0_
	.cfi_endproc

	.section	.text._ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_,"axG",@progbits,_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_,comdat
	.weak	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_
	.p2align	4, 0x90
	.type	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_,@function
_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_: # @_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi195:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi196:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi197:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi198:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi199:
	.cfi_def_cfa_offset 48
.Lcfi200:
	.cfi_offset %rbx, -48
.Lcfi201:
	.cfi_offset %r12, -40
.Lcfi202:
	.cfi_offset %r13, -32
.Lcfi203:
	.cfi_offset %r14, -24
.Lcfi204:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	%r14, %rax
	subq	%r12, %rax
	cmpq	$129, %rax
	jl	.LBB28_31
# BB#1:                                 # %.lr.ph
	leaq	8(%r12), %r13
	.p2align	4, 0x90
.LBB28_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB28_11 Depth 2
                                        #       Child Loop BB28_12 Depth 3
                                        #       Child Loop BB28_14 Depth 3
	testq	%r15, %r15
	je	.LBB28_17
# BB#3:                                 #   in Loop: Header=BB28_2 Depth=1
	shrq	$4, %rax
	leaq	(%r12,%rax,8), %rdx
	leaq	-8(%r14), %rcx
	movsd	8(%r12), %xmm1          # xmm1 = mem[0],zero
	movsd	(%r12,%rax,8), %xmm2    # xmm2 = mem[0],zero
	movsd	-8(%r14), %xmm0         # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm2
	jbe	.LBB28_6
# BB#4:                                 #   in Loop: Header=BB28_2 Depth=1
	ucomisd	%xmm2, %xmm0
	movq	%rdx, %rax
	movapd	%xmm2, %xmm3
	ja	.LBB28_9
# BB#5:                                 #   in Loop: Header=BB28_2 Depth=1
	ucomisd	%xmm1, %xmm0
	cmovbeq	%r13, %rcx
	maxsd	%xmm1, %xmm0
	movq	%rcx, %rax
	jmp	.LBB28_8
	.p2align	4, 0x90
.LBB28_6:                               #   in Loop: Header=BB28_2 Depth=1
	ucomisd	%xmm1, %xmm0
	movq	%r13, %rax
	movapd	%xmm1, %xmm3
	ja	.LBB28_9
# BB#7:                                 #   in Loop: Header=BB28_2 Depth=1
	ucomisd	%xmm2, %xmm0
	cmovaq	%rcx, %rdx
	maxsd	%xmm2, %xmm0
	movq	%rdx, %rax
.LBB28_8:                               # %_ZSt22__move_median_to_firstIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_S4_S4_T0_.exit.i
                                        #   in Loop: Header=BB28_2 Depth=1
	movapd	%xmm0, %xmm3
.LBB28_9:                               # %_ZSt22__move_median_to_firstIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_S4_S4_T0_.exit.i
                                        #   in Loop: Header=BB28_2 Depth=1
	decq	%r15
	movq	(%r12), %rcx
	movsd	%xmm3, (%r12)
	movq	%rcx, (%rax)
	movq	%r14, %rax
	movq	%r13, %rcx
	jmp	.LBB28_11
	.p2align	4, 0x90
.LBB28_10:                              #   in Loop: Header=BB28_11 Depth=2
	movsd	%xmm2, (%rbx)
	movsd	%xmm0, (%rax)
.LBB28_11:                              #   Parent Loop BB28_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB28_12 Depth 3
                                        #       Child Loop BB28_14 Depth 3
	movsd	(%r12), %xmm1           # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB28_12:                              #   Parent Loop BB28_2 Depth=1
                                        #     Parent Loop BB28_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rcx
	ucomisd	%xmm0, %xmm1
	ja	.LBB28_12
# BB#13:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB28_11 Depth=2
	leaq	-8(%rcx), %rbx
	.p2align	4, 0x90
.LBB28_14:                              # %.preheader.i.i
                                        #   Parent Loop BB28_2 Depth=1
                                        #     Parent Loop BB28_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rax), %xmm2         # xmm2 = mem[0],zero
	addq	$-8, %rax
	ucomisd	%xmm1, %xmm2
	ja	.LBB28_14
# BB#15:                                #   in Loop: Header=BB28_11 Depth=2
	cmpq	%rax, %rbx
	jb	.LBB28_10
# BB#16:                                # %_ZSt27__unguarded_partition_pivotIPdN9__gnu_cxx5__ops15_Iter_less_iterEET_S4_S4_T0_.exit
                                        #   in Loop: Header=BB28_2 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_
	movq	%rbx, %rax
	subq	%r12, %rax
	cmpq	$128, %rax
	movq	%rbx, %r14
	jg	.LBB28_2
	jmp	.LBB28_31
.LBB28_17:                              # %.lr.ph.i5.i
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_
	.p2align	4, 0x90
.LBB28_18:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB28_20 Depth 2
                                        #     Child Loop BB28_26 Depth 2
	movsd	-8(%r14), %xmm0         # xmm0 = mem[0],zero
	movq	(%r12), %rax
	movq	%rax, -8(%r14)
	leaq	-8(%r14), %r14
	movq	%r14, %r8
	subq	%r12, %r8
	movq	%r8, %rdx
	sarq	$3, %rdx
	leaq	-1(%rdx), %rcx
	cmpq	$2, %rcx
	jl	.LBB28_21
# BB#19:                                # %.lr.ph.i.i.i6.i.preheader
                                        #   in Loop: Header=BB28_18 Depth=1
	shrq	$63, %rcx
	leaq	-1(%rdx,%rcx), %rsi
	sarq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB28_20:                              # %.lr.ph.i.i.i6.i
                                        #   Parent Loop BB28_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rdi,%rdi), %rbx
	leaq	2(%rdi,%rdi), %rax
	leaq	1(%rdi,%rdi), %rcx
	movsd	8(%r12,%rbx,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	16(%r12,%rbx,8), %xmm1
	cmovbeq	%rax, %rcx
	movq	(%r12,%rcx,8), %rax
	movq	%rax, (%r12,%rdi,8)
	cmpq	%rsi, %rcx
	movq	%rcx, %rdi
	jl	.LBB28_20
	jmp	.LBB28_22
	.p2align	4, 0x90
.LBB28_21:                              #   in Loop: Header=BB28_18 Depth=1
	xorl	%ecx, %ecx
.LBB28_22:                              # %._crit_edge.i.i.i.i
                                        #   in Loop: Header=BB28_18 Depth=1
	testb	$1, %dl
	jne	.LBB28_25
# BB#23:                                #   in Loop: Header=BB28_18 Depth=1
	leaq	-2(%rdx), %rax
	shrq	$63, %rax
	leaq	-2(%rdx,%rax), %rax
	sarq	%rax
	cmpq	%rax, %rcx
	jne	.LBB28_25
# BB#24:                                #   in Loop: Header=BB28_18 Depth=1
	leaq	(%rcx,%rcx), %rax
	movq	8(%r12,%rax,8), %rax
	movq	%rax, (%r12,%rcx,8)
	leaq	1(%rcx,%rcx), %rcx
.LBB28_25:                              #   in Loop: Header=BB28_18 Depth=1
	testq	%rcx, %rcx
	jle	.LBB28_29
	.p2align	4, 0x90
.LBB28_26:                              # %.lr.ph.i.i.i.i.i
                                        #   Parent Loop BB28_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rcx), %rsi
	movq	%rsi, %rax
	shrq	$63, %rax
	leaq	-1(%rcx,%rax), %rdx
	sarq	%rdx
	movsd	(%r12,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB28_29
# BB#27:                                #   in Loop: Header=BB28_26 Depth=2
	movsd	%xmm1, (%r12,%rcx,8)
	cmpq	$1, %rsi
	movq	%rdx, %rcx
	jg	.LBB28_26
	jmp	.LBB28_30
	.p2align	4, 0x90
.LBB28_29:                              #   in Loop: Header=BB28_18 Depth=1
	movq	%rcx, %rdx
.LBB28_30:                              # %_ZSt10__pop_heapIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_S4_T0_.exit.i.i
                                        #   in Loop: Header=BB28_18 Depth=1
	movsd	%xmm0, (%r12,%rdx,8)
	cmpq	$8, %r8
	jg	.LBB28_18
.LBB28_31:                              # %_ZSt14__partial_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_S4_T0_.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end28:
	.size	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_, .Lfunc_end28-_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_
	.cfi_endproc

	.section	.text._ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_,"axG",@progbits,_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_,comdat
	.weak	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_
	.p2align	4, 0x90
	.type	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_,@function
_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_: # @_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi205:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi206:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi207:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi208:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi209:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi210:
	.cfi_def_cfa_offset 64
.Lcfi211:
	.cfi_offset %rbx, -48
.Lcfi212:
	.cfi_offset %r12, -40
.Lcfi213:
	.cfi_offset %r13, -32
.Lcfi214:
	.cfi_offset %r14, -24
.Lcfi215:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	%r14, %rax
	subq	%r12, %rax
	cmpq	$129, %rax
	jl	.LBB29_20
# BB#1:
	movl	$1, %ebx
	movq	%rsp, %r15
	movq	%r12, %rdi
	.p2align	4, 0x90
.LBB29_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB29_18 Depth 2
	leaq	(%r12,%rbx,8), %r13
	movsd	(%r12,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	movq	(%r12), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB29_17
# BB#3:                                 # %_ZSt13copy_backwardIPdS0_ET0_T_S2_S1_.exit.i
                                        #   in Loop: Header=BB29_2 Depth=1
	leaq	(,%rbx,8), %rdx
	subq	%rdx, %rdi
	addq	$16, %rdi
	movq	%r12, %rsi
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	callq	memmove
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, (%r12)
	jmp	.LBB29_4
	.p2align	4, 0x90
.LBB29_17:                              #   in Loop: Header=BB29_2 Depth=1
	movq	%r13, %rcx
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB29_18:                              # %._crit_edge.i
                                        #   Parent Loop BB29_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	%xmm1, (%rax)
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	movq	%rcx, %rax
	leaq	-8(%rcx), %rcx
	movq	(%rsp), %rdx
	movd	%rdx, %xmm0
	ucomisd	%xmm0, %xmm1
	ja	.LBB29_18
# BB#19:                                # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_less_iterEEvT_T0_.exit.i
                                        #   in Loop: Header=BB29_2 Depth=1
	movq	%rdx, 8(%rcx)
.LBB29_4:                               # %.backedge.i
                                        #   in Loop: Header=BB29_2 Depth=1
	incq	%rbx
	cmpq	$16, %rbx
	movq	%r13, %rdi
	jne	.LBB29_2
# BB#5:                                 # %_ZSt16__insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_.exit
	movq	%r12, %rax
	subq	$-128, %rax
	cmpq	%r14, %rax
	je	.LBB29_28
# BB#6:                                 # %.lr.ph.i28
	leaq	-136(%r14), %rdx
	subq	%r12, %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB29_10
# BB#7:
	movq	%rsp, %rax
	leaq	136(%r12), %rdx
	.p2align	4, 0x90
.LBB29_8:                               # =>This Inner Loop Header: Depth=1
	movq	-8(%rdx), %rsi
	movq	%rsi, (%rax)
	movq	-16(%rdx), %xmm0        # xmm0 = mem[0],zero
	leaq	-8(%rdx), %rdx
	movq	(%rsp), %rsi
	movd	%rsi, %xmm1
	ucomisd	%xmm1, %xmm0
	movq	%rdx, %rax
	ja	.LBB29_8
# BB#9:                                 # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_less_iterEEvT_T0_.exit.i34.prol
	movq	%rsi, (%rdx)
	addq	$136, %r12
	movq	%r12, %rax
.LBB29_10:                              # %.prol.loopexit
	testq	%rcx, %rcx
	je	.LBB29_28
# BB#11:                                # %.lr.ph.i28.new
	movq	%rsp, %rcx
	.p2align	4, 0x90
.LBB29_12:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB29_13 Depth 2
                                        #     Child Loop BB29_15 Depth 2
	movq	%rax, %rsi
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB29_13:                              #   Parent Loop BB29_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rdi
	movq	%rdi, (%rdx)
	movq	-8(%rsi), %xmm0         # xmm0 = mem[0],zero
	movq	%rsi, %rdx
	leaq	-8(%rsi), %rsi
	movq	(%rsp), %rdi
	movd	%rdi, %xmm1
	ucomisd	%xmm1, %xmm0
	ja	.LBB29_13
# BB#14:                                # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_less_iterEEvT_T0_.exit.i34
                                        #   in Loop: Header=BB29_12 Depth=1
	movq	%rdi, 8(%rsi)
	leaq	8(%rax), %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB29_15:                              #   Parent Loop BB29_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rdi
	movq	%rdi, (%rsi)
	movq	-8(%rdx), %xmm0         # xmm0 = mem[0],zero
	movq	%rdx, %rsi
	leaq	-8(%rdx), %rdx
	movq	(%rsp), %rdi
	movd	%rdi, %xmm1
	ucomisd	%xmm1, %xmm0
	ja	.LBB29_15
# BB#16:                                # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_less_iterEEvT_T0_.exit.i34.1
                                        #   in Loop: Header=BB29_12 Depth=1
	movq	%rdi, 8(%rdx)
	addq	$16, %rax
	cmpq	%r14, %rax
	jne	.LBB29_12
	jmp	.LBB29_28
.LBB29_20:
	cmpq	%r14, %r12
	je	.LBB29_28
# BB#21:                                # %.preheader.i
	leaq	8(%r12), %rax
	cmpq	%r14, %rax
	je	.LBB29_28
# BB#22:                                # %.lr.ph.i
	movq	%rsp, %r15
	movq	%r12, %rdi
	.p2align	4, 0x90
.LBB29_23:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB29_30 Depth 2
	movq	%rax, %rbx
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	movq	(%r12), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB29_29
# BB#24:                                #   in Loop: Header=BB29_23 Depth=1
	movq	%rbx, %rdx
	subq	%r12, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	je	.LBB29_26
# BB#25:                                #   in Loop: Header=BB29_23 Depth=1
	shlq	$3, %rax
	subq	%rax, %rdi
	addq	$16, %rdi
	movq	%r12, %rsi
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	callq	memmove
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
.LBB29_26:                              # %_ZSt13copy_backwardIPdS0_ET0_T_S2_S1_.exit.i15
                                        #   in Loop: Header=BB29_23 Depth=1
	movsd	%xmm1, (%r12)
	jmp	.LBB29_27
	.p2align	4, 0x90
.LBB29_29:                              #   in Loop: Header=BB29_23 Depth=1
	movq	%rbx, %rcx
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB29_30:                              # %._crit_edge.i23
                                        #   Parent Loop BB29_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	%xmm1, (%rax)
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	movq	%rcx, %rax
	leaq	-8(%rcx), %rcx
	movq	(%rsp), %rdx
	movd	%rdx, %xmm0
	ucomisd	%xmm0, %xmm1
	ja	.LBB29_30
# BB#31:                                # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_less_iterEEvT_T0_.exit.i24
                                        #   in Loop: Header=BB29_23 Depth=1
	movq	%rdx, 8(%rcx)
.LBB29_27:                              # %.backedge.i17
                                        #   in Loop: Header=BB29_23 Depth=1
	leaq	8(%rbx), %rax
	cmpq	%r14, %rax
	movq	%rbx, %rdi
	jne	.LBB29_23
.LBB29_28:                              # %_ZSt26__unguarded_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_.exit
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end29:
	.size	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_, .Lfunc_end29-_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_
	.cfi_endproc

	.section	.text._ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_,"axG",@progbits,_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_,comdat
	.weak	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_
	.p2align	4, 0x90
	.type	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_,@function
_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_: # @_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_
	.cfi_startproc
# BB#0:
	subq	%rdi, %rsi
	cmpq	$16, %rsi
	jl	.LBB30_23
# BB#1:
	sarq	$3, %rsi
	leaq	-2(%rsi), %rax
	shrq	$63, %rax
	leaq	-2(%rsi,%rax), %r9
	sarq	%r9
	leaq	-1(%rsi), %rax
	shrq	$63, %rax
	leaq	-1(%rsi,%rax), %r11
	sarq	%r11
	testb	$1, %sil
	jne	.LBB30_14
# BB#2:                                 # %.split.us.preheader
	leaq	1(%r9,%r9), %r8
	movq	%r9, %r10
	.p2align	4, 0x90
.LBB30_3:                               # %.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_5 Depth 2
                                        #     Child Loop BB30_9 Depth 2
	movsd	(%rdi,%r10,8), %xmm0    # xmm0 = mem[0],zero
	cmpq	%r10, %r11
	movq	%r10, %rdx
	jle	.LBB30_6
# BB#4:                                 # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB30_3 Depth=1
	movq	%r10, %rax
	.p2align	4, 0x90
.LBB30_5:                               # %.lr.ph.i.us
                                        #   Parent Loop BB30_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rax,%rax), %rsi
	leaq	2(%rax,%rax), %rcx
	leaq	1(%rax,%rax), %rdx
	movsd	8(%rdi,%rsi,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	16(%rdi,%rsi,8), %xmm1
	cmovbeq	%rcx, %rdx
	movq	(%rdi,%rdx,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	cmpq	%r11, %rdx
	movq	%rdx, %rax
	jl	.LBB30_5
.LBB30_6:                               # %._crit_edge.i.us
                                        #   in Loop: Header=BB30_3 Depth=1
	cmpq	%r9, %rdx
	jne	.LBB30_8
# BB#7:                                 #   in Loop: Header=BB30_3 Depth=1
	movq	(%rdi,%r8,8), %rax
	movq	%rax, (%rdi,%r9,8)
	movq	%r8, %rdx
.LBB30_8:                               #   in Loop: Header=BB30_3 Depth=1
	cmpq	%r10, %rdx
	jle	.LBB30_12
	.p2align	4, 0x90
.LBB30_9:                               # %.lr.ph.i.i.us
                                        #   Parent Loop BB30_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rdx), %rax
	shrq	$63, %rax
	leaq	-1(%rdx,%rax), %rax
	sarq	%rax
	movsd	(%rdi,%rax,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB30_12
# BB#10:                                #   in Loop: Header=BB30_9 Depth=2
	movsd	%xmm1, (%rdi,%rdx,8)
	cmpq	%r10, %rax
	movq	%rax, %rdx
	jg	.LBB30_9
	jmp	.LBB30_13
	.p2align	4, 0x90
.LBB30_12:                              #   in Loop: Header=BB30_3 Depth=1
	movq	%rdx, %rax
.LBB30_13:                              # %_ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_.exit.us
                                        #   in Loop: Header=BB30_3 Depth=1
	movsd	%xmm0, (%rdi,%rax,8)
	testq	%r10, %r10
	leaq	-1(%r10), %r10
	jne	.LBB30_3
	jmp	.LBB30_23
	.p2align	4, 0x90
.LBB30_14:                              # %.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_16 Depth 2
                                        #     Child Loop BB30_18 Depth 2
	movsd	(%rdi,%r9,8), %xmm0     # xmm0 = mem[0],zero
	cmpq	%r9, %r11
	movq	%r9, %rsi
	jle	.LBB30_22
# BB#15:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB30_14 Depth=1
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB30_16:                              # %.lr.ph.i
                                        #   Parent Loop BB30_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rax
	leaq	(%rax,%rax), %rcx
	leaq	2(%rax,%rax), %rsi
	leaq	1(%rax,%rax), %rdx
	movsd	8(%rdi,%rcx,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	16(%rdi,%rcx,8), %xmm1
	cmovbeq	%rsi, %rdx
	movq	(%rdi,%rdx,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	cmpq	%r11, %rdx
	jl	.LBB30_16
# BB#17:                                # %._crit_edge.i
                                        #   in Loop: Header=BB30_14 Depth=1
	cmpq	%r9, %rdx
	jle	.LBB30_21
	.p2align	4, 0x90
.LBB30_18:                              # %.lr.ph.i.i
                                        #   Parent Loop BB30_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rdx), %rax
	shrq	$63, %rax
	leaq	-1(%rdx,%rax), %rsi
	sarq	%rsi
	movsd	(%rdi,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB30_21
# BB#19:                                #   in Loop: Header=BB30_18 Depth=2
	movsd	%xmm1, (%rdi,%rdx,8)
	cmpq	%r9, %rsi
	movq	%rsi, %rdx
	jg	.LBB30_18
	jmp	.LBB30_22
	.p2align	4, 0x90
.LBB30_21:                              #   in Loop: Header=BB30_14 Depth=1
	movq	%rdx, %rsi
.LBB30_22:                              # %_ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_.exit
                                        #   in Loop: Header=BB30_14 Depth=1
	movsd	%xmm0, (%rdi,%rsi,8)
	testq	%r9, %r9
	leaq	-1(%r9), %r9
	jne	.LBB30_14
.LBB30_23:                              # %.loopexit
	retq
.Lfunc_end30:
	.size	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_, .Lfunc_end30-_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_
	.cfi_endproc

	.type	results,@object         # @results
	.bss
	.globl	results
	.p2align	3
results:
	.quad	0
	.size	results, 8

	.type	current_test,@object    # @current_test
	.globl	current_test
	.p2align	2
current_test:
	.long	0                       # 0x0
	.size	current_test, 4

	.type	allocated_results,@object # @allocated_results
	.globl	allocated_results
	.p2align	2
allocated_results:
	.long	0                       # 0x0
	.size	allocated_results, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Could not allocate %d results\n"
	.size	.L.str, 31

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\ntest %*s description   absolute   operations   ratio with\n"
	.size	.L.str.1, 60

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" "
	.size	.L.str.2, 2

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"number %*s time       per second   test0\n\n"
	.size	.L.str.3, 43

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%2i %*s\"%s\"  %5.2f sec   %5.2f M     %.2f\n"
	.size	.L.str.4, 43

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.zero	1
	.size	.L.str.5, 1

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\nTotal absolute time for %s: %.2f sec\n"
	.size	.L.str.6, 39

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\n%s Penalty: %.2f\n\n"
	.size	.L.str.7, 20

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"\ntest %*s description   absolute\n"
	.size	.L.str.8, 34

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"number %*s time\n\n"
	.size	.L.str.9, 18

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%2i %*s\"%s\"  %5.2f sec\n"
	.size	.L.str.10, 24

	.type	start_time,@object      # @start_time
	.bss
	.globl	start_time
	.p2align	3
start_time:
	.quad	0                       # 0x0
	.size	start_time, 8

	.type	end_time,@object        # @end_time
	.globl	end_time
	.p2align	3
end_time:
	.quad	0                       # 0x0
	.size	end_time, 8

	.type	.L.str.11,@object       # @.str.11
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.11:
	.asciz	"test %i failed\n"
	.size	.L.str.11, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
