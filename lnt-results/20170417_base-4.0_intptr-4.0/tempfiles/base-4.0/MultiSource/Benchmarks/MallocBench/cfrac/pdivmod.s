	.text
	.file	"pdivmod.bc"
	.globl	pdivmod
	.p2align	4, 0x90
	.type	pdivmod,@function
pdivmod:                                # @pdivmod
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 192
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbp
	movq	%rdx, %r13
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	%rbx, 32(%rsp)
	movq	%r12, 56(%rsp)
	movzwl	4(%r12), %r15d
	testq	%rbx, %rbx
	je	.LBB0_2
# BB#1:
	incw	(%rbx)
.LBB0_2:
	incw	(%r12)
	movw	4(%rbx), %ax
	cmpw	%r15w, %ax
	jae	.LBB0_19
# BB#3:
	movq	$0, 8(%rsp)
	movq	$0, 16(%rsp)
	movq	pzero(%rip), %rsi
	leaq	8(%rsp), %rdi
	callq	psetq
	movq	32(%rsp), %rsi
	leaq	16(%rsp), %rdi
	callq	psetq
.LBB0_4:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_7
# BB#5:
	decw	(%rdi)
	jne	.LBB0_7
# BB#6:
	xorl	%eax, %eax
	callq	pfree
.LBB0_7:
	movq	56(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_10
# BB#8:
	decw	(%rax)
	jne	.LBB0_10
# BB#9:
	movq	56(%rsp), %rdi
	xorl	%eax, %eax
	callq	pfree
.LBB0_10:
	cmpq	$-1, %rbp
	je	.LBB0_26
# BB#11:
	testq	%r13, %r13
	je	.LBB0_34
# BB#12:
	cmpq	$-1, %r13
	jne	.LBB0_33
# BB#13:
	testq	%rbp, %rbp
	je	.LBB0_15
# BB#14:
	movq	16(%rsp), %rsi
	movq	%rbp, %rdi
	callq	psetq
.LBB0_15:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_18
# BB#16:
	decw	(%rdi)
	jne	.LBB0_18
# BB#17:
	xorl	%eax, %eax
	callq	pfree
.LBB0_18:
	movq	8(%rsp), %rdi
	jmp	.LBB0_32
.LBB0_19:
	movq	%rbp, 128(%rsp)         # 8-byte Spill
	subl	%r15d, %eax
	movl	%eax, 68(%rsp)          # 4-byte Spill
	movzwl	%ax, %ecx
	movq	%rcx, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leal	1(%rcx), %edi
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	callq	palloc
	movq	%rax, %rbp
	movq	%rbp, 8(%rsp)
	testq	%rbp, %rbp
	je	.LBB0_52
# BB#20:
	movb	6(%rbx), %al
	cmpb	6(%r12), %al
	setne	6(%rbp)
	xorl	%eax, %eax
	movl	%r15d, %edi
	callq	palloc
	movq	%rax, %r8
	movq	%r8, 16(%rsp)
	testq	%r8, %r8
	je	.LBB0_42
# BB#21:
	movq	%r13, 112(%rsp)         # 8-byte Spill
	leaq	8(%r12,%r15,2), %rcx
	movb	6(%rbx), %al
	movb	%al, 6(%r8)
	movq	24(%rsp), %r14          # 8-byte Reload
	leaq	10(%rbp,%r14,2), %rax
	movzwl	-2(%rcx), %r13d
	cmpl	$1, %r15d
	jne	.LBB0_45
# BB#22:
	testw	%r13w, %r13w
	je	.LBB0_53
# BB#23:
	leaq	(%r14,%r14), %rsi
	leaq	(%rbx,%r15,2), %rcx
	addq	$8, %rbx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_24:                               # =>This Inner Loop Header: Depth=1
	shll	$16, %edx
	movzwl	6(%rsi,%rcx), %eax
	orl	%edx, %eax
	xorl	%edx, %edx
	divl	%r13d
	movw	%ax, 8(%rsi,%rbp)
	addq	$-2, %rbp
	leaq	6(%rcx,%rsi), %rax
	addq	$-2, %rcx
	cmpq	%rbx, %rax
	ja	.LBB0_24
# BB#25:
	movw	%dx, 6(%r8,%r15,2)
	leaq	10(%rbp,%r14,2), %rbx
	jmp	.LBB0_90
.LBB0_26:
	testq	%r13, %r13
	je	.LBB0_28
# BB#27:
	movq	8(%rsp), %rsi
	movq	%r13, %rdi
	callq	psetq
.LBB0_28:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_31
# BB#29:
	decw	(%rdi)
	jne	.LBB0_31
# BB#30:
	xorl	%eax, %eax
	callq	pfree
.LBB0_31:
	movq	16(%rsp), %rdi
.LBB0_32:
	callq	presult
	movq	%rax, %r14
	jmp	.LBB0_52
.LBB0_33:
	movq	8(%rsp), %rsi
	movq	%r13, %rdi
	callq	psetq
.LBB0_34:
	testq	%rbp, %rbp
	je	.LBB0_36
# BB#35:
	movq	16(%rsp), %rsi
	movq	%rbp, %rdi
	callq	psetq
.LBB0_36:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_39
# BB#37:
	decw	(%rdi)
	jne	.LBB0_39
# BB#38:
	xorl	%eax, %eax
	callq	pfree
.LBB0_39:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_44
# BB#40:
	decw	(%rdi)
	je	.LBB0_49
# BB#41:
	xorl	%r14d, %r14d
	jmp	.LBB0_52
.LBB0_42:
	decw	(%rbp)
	je	.LBB0_50
.LBB0_44:
	xorl	%r14d, %r14d
	jmp	.LBB0_52
.LBB0_45:
	movq	%rcx, %rbx
	movq	%rax, 104(%rsp)         # 8-byte Spill
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movl	%r15d, %edi
	callq	palloc
	movq	%rax, %r8
	testq	%r8, %r8
	je	.LBB0_52
# BB#46:
	incl	%r13d
	xorl	%r15d, %r15d
	movl	$65536, %eax            # imm = 0x10000
	xorl	%edx, %edx
	divl	%r13d
	leaq	8(%r8), %rcx
	movzwl	%ax, %r13d
	leaq	10(%r12), %rsi
	movq	%rbx, %rbp
	cmpq	%rsi, %rbp
	cmovaq	%rbp, %rsi
	movq	$-9, %rdx
	subq	%r12, %rdx
	leaq	8(%r12), %rax
	addq	%rsi, %rdx
	movl	%edx, %esi
	shrl	%esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_54
# BB#47:                                # %.prol.preheader305
	negq	%rsi
	xorl	%r15d, %r15d
	movq	24(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_48:                               # =>This Inner Loop Header: Depth=1
	movzwl	(%rax), %edi
	addq	$2, %rax
	imull	%r13d, %edi
	addl	%r15d, %edi
	movl	%edi, %r15d
	shrl	$16, %r15d
	movw	%di, (%rcx)
	addq	$2, %rcx
	incq	%rsi
	jne	.LBB0_48
	jmp	.LBB0_55
.LBB0_49:
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	jmp	.LBB0_51
.LBB0_50:
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	movq	%rbp, %rdi
.LBB0_51:
	callq	pfree
.LBB0_52:
	movq	%r14, %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_53:
	movl	$4, %edi
	movl	$.L.str, %esi
	movq	%rax, %rbx
	movl	$.L.str.1, %edx
	callq	errorp
	movq	%rax, %rdi
	callq	pnew
	movq	%rax, 8(%rsp)
	jmp	.LBB0_90
.LBB0_54:
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB0_55:                               # %.prol.loopexit306
	cmpq	$6, %rdx
	jb	.LBB0_57
	.p2align	4, 0x90
.LBB0_56:                               # =>This Inner Loop Header: Depth=1
	movzwl	(%rax), %edx
	imull	%r13d, %edx
	addl	%r15d, %edx
	movw	%dx, (%rcx)
	shrl	$16, %edx
	movzwl	2(%rax), %esi
	imull	%r13d, %esi
	addl	%edx, %esi
	movw	%si, 2(%rcx)
	shrl	$16, %esi
	movzwl	4(%rax), %edx
	imull	%r13d, %edx
	addl	%esi, %edx
	movw	%dx, 4(%rcx)
	shrl	$16, %edx
	movzwl	6(%rax), %r15d
	imull	%r13d, %r15d
	addl	%edx, %r15d
	movw	%r15w, 6(%rcx)
	shrl	$16, %r15d
	addq	$8, %rax
	addq	$8, %rcx
	cmpq	%rbp, %rax
	jb	.LBB0_56
.LBB0_57:
	leaq	56(%rsp), %rdi
	movq	%r8, %rsi
	movq	%r8, %rbx
	callq	psetq
	movq	%rbx, %rdi
	decw	(%rdi)
	jne	.LBB0_59
# BB#58:
	xorl	%eax, %eax
	callq	pfree
.LBB0_59:
	movq	40(%rsp), %r12          # 8-byte Reload
	leal	1(%r12,%r14), %edi
	movq	%r14, %rbx
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	callq	palloc
	testq	%rax, %rax
	je	.LBB0_52
# BB#60:
	movq	32(%rsp), %rsi
	leaq	8(%rsi), %rcx
	leaq	8(%rax), %r10
	leaq	(%r12,%rbx), %rdx
	leaq	8(%rsi,%rdx,2), %rdx
	leaq	10(%rsi), %rbp
	cmpq	%rbp, %rdx
	cmovaq	%rdx, %rbp
	movq	$-9, %rdi
	subq	%rsi, %rdi
	addq	%rbp, %rdi
	movq	%rdi, %rdx
	shrq	%rdx
	leal	1(%rdx), %ebp
	andq	$3, %rbp
	movq	%rbx, %r8
	je	.LBB0_63
# BB#61:                                # %.prol.preheader290
	negq	%rbp
	.p2align	4, 0x90
.LBB0_62:                               # =>This Inner Loop Header: Depth=1
	movzwl	(%rcx), %ebx
	addq	$2, %rcx
	imull	%r13d, %ebx
	addl	%r15d, %ebx
	movl	%ebx, %r15d
	shrl	$16, %r15d
	movw	%bx, (%r10)
	addq	$2, %r10
	incq	%rbp
	jne	.LBB0_62
.LBB0_63:                               # %.prol.loopexit291
	cmpq	$6, %rdi
	jb	.LBB0_66
# BB#64:                                # %.new292
	leaq	8(%rsi,%r8,2), %rsi
	leaq	(%rsi,%r12,2), %rsi
	.p2align	4, 0x90
.LBB0_65:                               # =>This Inner Loop Header: Depth=1
	movzwl	(%rcx), %edi
	imull	%r13d, %edi
	addl	%r15d, %edi
	movw	%di, (%r10)
	shrl	$16, %edi
	movzwl	2(%rcx), %ebp
	imull	%r13d, %ebp
	addl	%edi, %ebp
	movw	%bp, 2(%r10)
	shrl	$16, %ebp
	movzwl	4(%rcx), %edi
	imull	%r13d, %edi
	addl	%ebp, %edi
	movw	%di, 4(%r10)
	shrl	$16, %edi
	movzwl	6(%rcx), %r15d
	imull	%r13d, %r15d
	addl	%edi, %r15d
	movw	%r15w, 6(%r10)
	shrl	$16, %r15d
	addq	$8, %rcx
	addq	$8, %r10
	cmpq	%rsi, %rcx
	jb	.LBB0_65
.LBB0_66:
	movw	%r15w, 10(%rax,%rdx,2)
	leaq	32(%rsp), %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	callq	psetq
	movq	%rbx, %rdi
	decw	(%rdi)
	jne	.LBB0_68
# BB#67:
	xorl	%eax, %eax
	callq	pfree
.LBB0_68:
	movq	32(%rsp), %rcx
	leaq	8(%rcx), %r14
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leaq	10(%rcx,%rax,2), %r9
	movq	56(%rsp), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	%r10, %rax
	negq	%rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	$1, %eax
	subq	%r10, %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	leaq	10(%rsi), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_69:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_71 Depth 2
                                        #     Child Loop BB0_77 Depth 2
                                        #     Child Loop BB0_84 Depth 2
	leaq	-2(%r9,%r10,2), %r12
	movzwl	-2(%r9,%r10,2), %ecx
	movzwl	6(%rsi,%r10,2), %r8d
	movw	$-1, %ax
	cmpw	%r8w, %cx
	je	.LBB0_74
# BB#70:                                #   in Loop: Header=BB0_69 Depth=1
	movq	%rdx, %r11
	shll	$16, %ecx
	movzwl	-2(%r12), %eax
	orl	%ecx, %eax
	xorl	%edx, %edx
	divl	%r8d
	movzwl	4(%rsi,%r10,2), %r10d
	movzwl	-4(%r12), %esi
	addq	$-4, %r12
	movl	%edx, %ebp
	shll	$16, %ebp
	movl	%r8d, %ecx
	shll	$16, %ecx
	.p2align	4, 0x90
.LBB0_71:                               #   Parent Loop BB0_69 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	%ax, %ebx
	imull	%r10d, %ebx
	movl	%esi, %edi
	orl	%ebp, %edi
	cmpl	%edi, %ebx
	jbe	.LBB0_73
# BB#72:                                #   in Loop: Header=BB0_71 Depth=2
	decl	%eax
	addl	%r8d, %edx
	addl	%ecx, %ebp
	cmpl	$65536, %edx            # imm = 0x10000
	jb	.LBB0_71
.LBB0_73:                               #   in Loop: Header=BB0_69 Depth=1
	addq	$4, %r12
	movq	%r11, %rdx
.LBB0_74:                               #   in Loop: Header=BB0_69 Depth=1
	leaq	-2(%r9), %rdi
	movzwl	%ax, %esi
	cmpq	%r9, %r12
	movq	%r9, %rcx
	cmovaq	%r12, %rcx
	subq	%r9, %rcx
	incq	%rcx
	movq	%rcx, %r15
	shrq	%r15
	btl	$1, %ecx
	jb	.LBB0_76
# BB#75:                                #   in Loop: Header=BB0_69 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	movzwl	(%rcx), %ecx
	imull	%esi, %ecx
	movl	%ecx, %r10d
	shrl	$16, %r10d
	movzwl	(%rdi), %r11d
	orl	$-65536, %ecx           # imm = 0xFFFF0000
	subl	%ecx, %r11d
	movw	%r11w, (%rdi)
	shrl	$16, %r11d
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	%r9, %rdi
	testq	%r15, %r15
	jne	.LBB0_77
	jmp	.LBB0_78
	.p2align	4, 0x90
.LBB0_76:                               #   in Loop: Header=BB0_69 Depth=1
	xorl	%r10d, %r10d
	movl	$1, %r11d
	movq	48(%rsp), %r8           # 8-byte Reload
	testq	%r15, %r15
	je	.LBB0_78
	.p2align	4, 0x90
.LBB0_77:                               #   Parent Loop BB0_69 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%r8), %ecx
	imull	%esi, %ecx
	addl	%r10d, %ecx
	movl	%ecx, %ebp
	notl	%ebp
	shrl	$16, %ecx
	movzwl	%bp, %ebp
	movzwl	(%rdi), %ebx
	addl	%r11d, %ebx
	addl	%ebp, %ebx
	movw	%bx, (%rdi)
	shrl	$16, %ebx
	movzwl	2(%r8), %r10d
	imull	%esi, %r10d
	addl	%ecx, %r10d
	movl	%r10d, %ecx
	notl	%ecx
	shrl	$16, %r10d
	movzwl	%cx, %ecx
	movzwl	2(%rdi), %r11d
	addl	%ebx, %r11d
	addl	%ecx, %r11d
	movw	%r11w, 2(%rdi)
	shrl	$16, %r11d
	addq	$4, %rdi
	addq	$4, %r8
	cmpq	%r12, %rdi
	jb	.LBB0_77
.LBB0_78:                               #   in Loop: Header=BB0_69 Depth=1
	leaq	(%r9,%r15,2), %rsi
	xorl	$65535, %r10d           # imm = 0xFFFF
	movzwl	(%r9,%r15,2), %ecx
	addl	%r11d, %r10d
	addl	%ecx, %r10d
	movw	%r10w, (%r9,%r15,2)
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rsi,%rcx,2), %rcx
	testl	$196608, %r10d          # imm = 0x30000
	je	.LBB0_80
# BB#79:                                #   in Loop: Header=BB0_69 Depth=1
	movq	40(%rsp), %r10          # 8-byte Reload
	jmp	.LBB0_86
	.p2align	4, 0x90
.LBB0_80:                               #   in Loop: Header=BB0_69 Depth=1
	movq	120(%rsp), %rdi         # 8-byte Reload
	leaq	(%r9,%rdi,2), %r8
	negq	%r9
	leaq	(%r8,%r15,2), %rdi
	cmpq	%rsi, %rdi
	cmovaq	%rdi, %rsi
	movq	40(%rsp), %r10          # 8-byte Reload
	leaq	(%rsi,%r10,2), %rsi
	leaq	(%r15,%r15), %rdi
	subq	%rdi, %r9
	leaq	-1(%r9,%rsi), %rdi
	movq	%rdi, %rsi
	shrq	%rsi
	xorl	%ebp, %ebp
	btl	$1, %edi
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	%rcx, %rdi
	jb	.LBB0_82
# BB#81:                                #   in Loop: Header=BB0_69 Depth=1
	movzwl	(%rcx), %edi
	movq	48(%rsp), %rbp          # 8-byte Reload
	movzwl	(%rbp), %ebp
	addl	%edi, %ebp
	movw	%bp, (%rcx)
	shrl	$16, %ebp
	leaq	2(%rcx), %rdi
	movq	72(%rsp), %rbx          # 8-byte Reload
.LBB0_82:                               # %.prol.loopexit277
                                        #   in Loop: Header=BB0_69 Depth=1
	movq	%rdx, %r9
	addq	%rsi, %r15
	testq	%rsi, %rsi
	je	.LBB0_85
# BB#83:                                # %.new278
                                        #   in Loop: Header=BB0_69 Depth=1
	leaq	(%rcx,%r10,2), %rcx
	.p2align	4, 0x90
.LBB0_84:                               #   Parent Loop BB0_69 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rdi), %esi
	movzwl	(%rbx), %edx
	addl	%ebp, %esi
	addl	%edx, %esi
	movw	%si, (%rdi)
	shrl	$16, %esi
	movzwl	2(%rdi), %ebp
	movzwl	2(%rbx), %edx
	addl	%esi, %ebp
	addl	%edx, %ebp
	movw	%bp, 2(%rdi)
	shrl	$16, %ebp
	addq	$4, %rdi
	addq	$4, %rbx
	cmpq	%rcx, %rdi
	jb	.LBB0_84
.LBB0_85:                               #   in Loop: Header=BB0_69 Depth=1
	leaq	(%r8,%r15,2), %rcx
	movzwl	(%r8,%r15,2), %edx
	addl	%ebp, %edx
	movw	%dx, (%r8,%r15,2)
	movq	88(%rsp), %rdx          # 8-byte Reload
	leaq	(%rcx,%rdx,2), %rcx
	decl	%eax
	movq	%r9, %rdx
.LBB0_86:                               #   in Loop: Header=BB0_69 Depth=1
	movq	80(%rsp), %rsi          # 8-byte Reload
	movw	%ax, -2(%rdx)
	addq	$-2, %rdx
	cmpq	%r14, %rcx
	movq	%rcx, %r9
	ja	.LBB0_69
# BB#87:
	movq	%rdx, %rbx
	movq	16(%rsp), %rdi
	leaq	(%r10,%r10), %rcx
	xorl	%edx, %edx
	movq	96(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_88:                               # =>This Inner Loop Header: Depth=1
	shll	$16, %edx
	movzwl	6(%rsi,%rcx), %eax
	orl	%edx, %eax
	xorl	%edx, %edx
	divl	%r13d
	movw	%ax, 6(%rdi,%rcx)
	leaq	6(%rsi,%rcx), %rax
	addq	$-2, %rcx
	cmpq	%r14, %rax
	ja	.LBB0_88
# BB#89:
	xorl	%eax, %eax
	callq	pnorm
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB0_90:
	cmpw	$0, 68(%rsp)            # 2-byte Folded Reload
	movq	128(%rsp), %rbp         # 8-byte Reload
	movq	112(%rsp), %r13         # 8-byte Reload
	je	.LBB0_93
# BB#91:
	cmpw	$0, (%rbx,%r14,2)
	jne	.LBB0_93
# BB#92:
	movq	8(%rsp), %rax
	decw	4(%rax)
.LBB0_93:
	movq	8(%rsp), %rax
	movzwl	4(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB0_4
# BB#94:
	cmpw	$0, (%rbx)
	jne	.LBB0_4
# BB#95:
	movb	$0, 6(%rax)
	jmp	.LBB0_4
.Lfunc_end0:
	.size	pdivmod, .Lfunc_end0-pdivmod
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"pdivmod"
	.size	.L.str, 8

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"divide by zero"
	.size	.L.str.1, 15


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
