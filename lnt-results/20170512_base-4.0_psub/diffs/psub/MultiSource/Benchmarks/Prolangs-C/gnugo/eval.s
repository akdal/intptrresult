	.text
	.file	"eval.bc"
	.globl	eval
	.p2align	4, 0x90
	.type	eval,@function
eval:                                   # @eval
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	%edi, %r12d
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_1:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_2 Depth 2
	movq	%r15, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_2:                                #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	p(%rbp), %eax
	cmpl	%r12d, %eax
	jne	.LBB0_4
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=2
	movl	$0, lib(%rip)
	movl	%r14d, %edi
	movl	%ebx, %esi
	movl	%r12d, %edx
	callq	countlib
	movzbl	lib(%rip), %eax
	movb	%al, l(%rbp)
.LBB0_4:                                #   in Loop: Header=BB0_2 Depth=2
	incq	%rbx
	incq	%rbp
	cmpq	$19, %rbx
	jne	.LBB0_2
# BB#5:                                 #   in Loop: Header=BB0_1 Depth=1
	incq	%r14
	addq	$19, %r15
	cmpq	$19, %r14
	jne	.LBB0_1
# BB#6:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	eval, .Lfunc_end0-eval
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
