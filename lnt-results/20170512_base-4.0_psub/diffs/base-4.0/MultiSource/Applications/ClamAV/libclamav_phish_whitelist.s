	.text
	.file	"libclamav_phish_whitelist.bc"
	.globl	whitelist_match
	.p2align	4, 0x90
	.type	whitelist_match,@function
whitelist_match:                        # @whitelist_match
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 64
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbp
	xorl	%ebx, %ebx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_2
# BB#1:
	movl	$1, (%rsp)
	leaq	8(%rsp), %r9
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r15, %rdx
	movl	%r14d, %r8d
	callq	regex_list_match
	movl	%eax, %ebx
.LBB0_2:
	movl	%ebx, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	whitelist_match, .Lfunc_end0-whitelist_match
	.cfi_endproc

	.globl	init_whitelist
	.p2align	4, 0x90
	.type	init_whitelist,@function
init_whitelist:                         # @init_whitelist
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 16
.Lcfi12:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB1_1
# BB#2:
	movl	$96, %edi
	callq	cli_malloc
	movq	%rax, 56(%rbx)
	testq	%rax, %rax
	je	.LBB1_3
# BB#5:
	movq	%rax, %rdi
	popq	%rbx
	jmp	init_regex_list         # TAILCALL
.LBB1_1:
	movl	$-111, %eax
	popq	%rbx
	retq
.LBB1_3:
	movl	$-114, %eax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	init_whitelist, .Lfunc_end1-init_whitelist
	.cfi_endproc

	.globl	is_whitelist_ok
	.p2align	4, 0x90
	.type	is_whitelist_ok,@function
is_whitelist_ok:                        # @is_whitelist_ok
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB2_2
# BB#1:
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB2_2
# BB#3:
	jmp	is_regex_ok             # TAILCALL
.LBB2_2:
	movl	$1, %eax
	retq
.Lfunc_end2:
	.size	is_whitelist_ok, .Lfunc_end2-is_whitelist_ok
	.cfi_endproc

	.globl	whitelist_cleanup
	.p2align	4, 0x90
	.type	whitelist_cleanup,@function
whitelist_cleanup:                      # @whitelist_cleanup
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB3_2
# BB#1:
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB3_2
# BB#3:
	jmp	regex_list_cleanup      # TAILCALL
.LBB3_2:
	retq
.Lfunc_end3:
	.size	whitelist_cleanup, .Lfunc_end3-whitelist_cleanup
	.cfi_endproc

	.globl	whitelist_done
	.p2align	4, 0x90
	.type	whitelist_done,@function
whitelist_done:                         # @whitelist_done
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB4_3
# BB#1:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_3
# BB#2:
	callq	regex_list_done
	movq	56(%rbx), %rdi
	callq	free
	movq	$0, 56(%rbx)
.LBB4_3:
	popq	%rbx
	retq
.Lfunc_end4:
	.size	whitelist_done, .Lfunc_end4-whitelist_done
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Phishing: looking up in whitelist: %s:%s; host-only:%d\n"
	.size	.L.str, 56


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
