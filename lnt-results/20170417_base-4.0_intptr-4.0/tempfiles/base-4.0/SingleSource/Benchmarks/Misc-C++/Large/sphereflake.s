	.text
	.file	"sphereflake.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4607182418800017408     # double 1
.LCPI0_1:
	.quad	4608589793683570688     # double 1.3125
.LCPI0_2:
	.quad	4602678819172646912     # double 0.5
.LCPI0_3:
	.quad	4427486594234968593     # double 9.9999999999999998E-13
.LCPI0_4:
	.quad	-4795885442619807215    # double -9.9999999999999998E-13
.LCPI0_5:
	.quad	4598175219545276416     # double 0.25
.LCPI0_6:
	.quad	-4620693217682128896    # double -0.5
.LCPI0_8:
	.quad	-4575660153439431339    # double -511.83333333333331
.LCPI0_9:
	.quad	-4575652823361912832    # double -512.5
.LCPI0_11:
	.quad	-4575666017501446144    # double -511.5
.LCPI0_12:
	.quad	4652209618980700160     # double 1023
.LCPI0_13:
	.quad	4652218415073722368     # double 1024
.LCPI0_14:
	.quad	9218868437227405312     # double +Inf
.LCPI0_15:
	.quad	-9223372036854775808    # double -0
.LCPI0_17:
	.quad	4634204016564240384     # double 64
.LCPI0_18:
	.quad	-4616189618054758400    # double -1
.LCPI0_19:
	.quad	0                       # double 0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_7:
	.quad	-4575652823361912832    # double -512.5
	.quad	-4575655755392920235    # double -512.16666666666663
.LCPI0_10:
	.quad	-4575655755392920235    # double -512.16666666666663
	.quad	-4575666017501446144    # double -511.5
.LCPI0_16:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
.LCPI0_20:
	.zero	16
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$248, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 304
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	cmpl	$2, %edi
	jne	.LBB0_1
# BB#2:                                 # %.critedge.preheader
	movq	8(%rsi), %rdi
	movl	$10, %ebx
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	cmpl	$1, %eax
	movl	$2, %r14d
	movl	$2, %ecx
	cmovgl	%eax, %ecx
	decl	%ecx
	cmpl	$2, %ecx
	jge	.LBB0_3
	jmp	.LBB0_6
.LBB0_1:
	movl	$6, %eax
	movl	$5, %ecx
.LBB0_3:                                # %.critedge.preheader41
	movl	$9, %esi
	.p2align	4, 0x90
.LBB0_4:                                # %.critedge
                                        # =>This Inner Loop Header: Depth=1
	movl	%esi, %edx
	leal	9(%rdx,%rdx,8), %esi
	decl	%ecx
	cmpl	$1, %ecx
	jg	.LBB0_4
# BB#5:                                 # %.critedge._crit_edge.loopexit
	leal	(%rdx,%rdx,8), %ebx
	addl	$10, %ebx
	movl	%eax, %r14d
.LBB0_6:                                # %.critedge._crit_edge
	movslq	%ebx, %rbp
	movl	$72, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	leaq	(%rbp,%rbp,8), %rcx
	leaq	(%rax,%rcx,8), %rcx
	movq	%rax, _ZL4pool(%rip)
	movq	%rcx, _ZL3end(%rip)
	movsd	.LCPI0_0(%rip), %xmm2   # xmm2 = mem[0],zero
	xorpd	%xmm0, %xmm0
	movl	$-101, %ecx
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI0_4(%rip), %xmm4   # xmm4 = mem[0],zero
	.p2align	4, 0x90
.LBB0_7:                                # %.preheader.i.i
                                        # =>This Inner Loop Header: Depth=1
	incl	%ecx
	je	.LBB0_10
# BB#8:                                 #   in Loop: Header=BB0_7 Depth=1
	movapd	%xmm1, %xmm0
	divsd	%xmm2, %xmm0
	addsd	%xmm2, %xmm0
	mulsd	.LCPI0_2(%rip), %xmm0
	movapd	%xmm0, %xmm3
	subsd	%xmm2, %xmm3
	ucomisd	.LCPI0_3(%rip), %xmm3
	movapd	%xmm0, %xmm2
	ja	.LBB0_7
# BB#9:                                 #   in Loop: Header=BB0_7 Depth=1
	ucomisd	%xmm3, %xmm4
	movapd	%xmm0, %xmm2
	ja	.LBB0_7
.LBB0_10:                               # %_ZNK3v_t4normEv.exit
	movsd	.LCPI0_0(%rip), %xmm6   # xmm6 = mem[0],zero
	movapd	%xmm6, %xmm4
	divsd	%xmm0, %xmm4
	movsd	.LCPI0_5(%rip), %xmm3   # xmm3 = mem[0],zero
	mulsd	%xmm4, %xmm3
	movsd	.LCPI0_6(%rip), %xmm5   # xmm5 = mem[0],zero
	mulsd	%xmm4, %xmm5
	xorpd	%xmm0, %xmm0
	xorpd	%xmm1, %xmm1
	xorpd	%xmm2, %xmm2
	movq	%rax, %rdi
	movl	%r14d, %esi
	movl	%ebx, %edx
	callq	_ZL6createP6node_tii3v_tS1_d
	movl	$_ZSt4cout, %edi
	movl	$.L.str, %esi
	movl	$3, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$1024, %r15d            # imm = 0x400
	movl	$_ZSt4cout, %edi
	movl	$1024, %esi             # imm = 0x400
	callq	_ZNSolsEi
	movq	%rax, %rbx
	movl	$.L.str.2, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$1024, %esi             # imm = 0x400
	movq	%rbx, %rdi
	callq	_ZNSolsEi
	movl	$.L.str.3, %esi
	movl	$5, %edx
	movq	%rax, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 64(%rsp)
	movabsq	$-4606619468846596096, %rax # imm = 0xC012000000000000
	movq	%rax, 80(%rsp)
	movaps	.LCPI0_7(%rip), %xmm0   # xmm0 = [-5.125000e+02,-5.121667e+02]
	movaps	%xmm0, 144(%rsp)
	movq	.LCPI0_8(%rip), %xmm0   # xmm0 = mem[0],zero
	movdqa	%xmm0, %xmm1
	pslldq	$8, %xmm1               # xmm1 = zero,zero,zero,zero,zero,zero,zero,zero,xmm1[0,1,2,3,4,5,6,7]
	movdqa	%xmm1, 160(%rsp)
	movsd	.LCPI0_9(%rip), %xmm1   # xmm1 = mem[0],zero
	movaps	%xmm1, 176(%rsp)
	movaps	.LCPI0_10(%rip), %xmm1  # xmm1 = [-5.121667e+02,-5.115000e+02]
	movaps	%xmm1, 192(%rsp)
	movq	.LCPI0_11(%rip), %xmm1  # xmm1 = mem[0],zero
	pslldq	$8, %xmm1               # xmm1 = zero,zero,zero,zero,zero,zero,zero,zero,xmm1[0,1,2,3,4,5,6,7]
	movdqa	%xmm1, 208(%rsp)
	movdqa	%xmm0, 224(%rsp)
	movsd	.LCPI0_12(%rip), %xmm0  # xmm0 = mem[0],zero
	movapd	%xmm0, (%rsp)           # 16-byte Spill
	movabsq	$9218868437227405312, %r13 # imm = 0x7FF0000000000000
	leaq	64(%rsp), %r14
	leaq	16(%rsp), %rbx
	.p2align	4, 0x90
.LBB0_11:                               # %.preheader38.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_12 Depth 2
                                        #       Child Loop BB0_13 Depth 3
                                        #         Child Loop BB0_15 Depth 4
                                        #         Child Loop BB0_28 Depth 4
                                        #           Child Loop BB0_31 Depth 5
                                        #           Child Loop BB0_43 Depth 5
	movl	$1024, %r12d            # imm = 0x400
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_12:                               # %.preheader.i
                                        #   Parent Loop BB0_11 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_13 Depth 3
                                        #         Child Loop BB0_15 Depth 4
                                        #         Child Loop BB0_28 Depth 4
                                        #           Child Loop BB0_31 Depth 5
                                        #           Child Loop BB0_43 Depth 5
	movapd	%xmm0, 112(%rsp)        # 16-byte Spill
	unpcklpd	(%rsp), %xmm0   # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0]
	movapd	%xmm0, 128(%rsp)        # 16-byte Spill
	xorpd	%xmm0, %xmm0
	xorl	%ebp, %ebp
	movsd	.LCPI0_2(%rip), %xmm13  # xmm13 = mem[0],zero
	movsd	.LCPI0_3(%rip), %xmm14  # xmm14 = mem[0],zero
	movsd	.LCPI0_4(%rip), %xmm15  # xmm15 = mem[0],zero
	jmp	.LBB0_13
.LBB0_49:                               #   in Loop: Header=BB0_13 Depth=3
	movsd	.LCPI0_14(%rip), %xmm0  # xmm0 = mem[0],zero
	jmp	.LBB0_50
	.p2align	4, 0x90
.LBB0_13:                               #   Parent Loop BB0_11 Depth=1
                                        #     Parent Loop BB0_12 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_15 Depth 4
                                        #         Child Loop BB0_28 Depth 4
                                        #           Child Loop BB0_31 Depth 5
                                        #           Child Loop BB0_43 Depth 5
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	leaq	(%rbp,%rbp,2), %rax
	movupd	144(%rsp,%rax,8), %xmm0
	addpd	128(%rsp), %xmm0        # 16-byte Folded Reload
	movsd	160(%rsp,%rax,8), %xmm1 # xmm1 = mem[0],zero
	addsd	.LCPI0_13(%rip), %xmm1
	movapd	%xmm0, %xmm2
	mulsd	%xmm2, %xmm2
	movapd	%xmm0, %xmm3
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	mulsd	%xmm3, %xmm3
	addsd	%xmm2, %xmm3
	movapd	%xmm1, %xmm2
	mulsd	%xmm2, %xmm2
	addsd	%xmm3, %xmm2
	movsd	.LCPI0_14(%rip), %xmm3  # xmm3 = mem[0],zero
	ucomisd	%xmm3, %xmm2
	jae	.LBB0_18
# BB#14:                                # %.preheader.i.i.i.preheader
                                        #   in Loop: Header=BB0_13 Depth=3
	xorpd	%xmm3, %xmm3
	movl	$-101, %eax
	movsd	.LCPI0_0(%rip), %xmm4   # xmm4 = mem[0],zero
	.p2align	4, 0x90
.LBB0_15:                               # %.preheader.i.i.i
                                        #   Parent Loop BB0_11 Depth=1
                                        #     Parent Loop BB0_12 Depth=2
                                        #       Parent Loop BB0_13 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%eax
	je	.LBB0_18
# BB#16:                                #   in Loop: Header=BB0_15 Depth=4
	movapd	%xmm2, %xmm3
	divsd	%xmm4, %xmm3
	addsd	%xmm4, %xmm3
	mulsd	%xmm13, %xmm3
	movapd	%xmm3, %xmm5
	subsd	%xmm4, %xmm5
	ucomisd	%xmm14, %xmm5
	movapd	%xmm3, %xmm4
	ja	.LBB0_15
# BB#17:                                #   in Loop: Header=BB0_15 Depth=4
	ucomisd	%xmm5, %xmm15
	movapd	%xmm3, %xmm4
	ja	.LBB0_15
	.p2align	4, 0x90
.LBB0_18:                               # %_ZNK3v_t4normEv.exit.i
                                        #   in Loop: Header=BB0_13 Depth=3
	movsd	.LCPI0_0(%rip), %xmm2   # xmm2 = mem[0],zero
	divsd	%xmm3, %xmm2
	mulsd	%xmm2, %xmm1
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm2, %xmm0
	movupd	%xmm0, 88(%rsp)
	movsd	%xmm1, 104(%rsp)
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 16(%rsp)
	movq	$0, 32(%rsp)
	movq	%r13, 40(%rsp)
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN6node_t9intersectILb0EEEvRK5ray_tR5hit_t
	movsd	40(%rsp), %xmm7         # xmm7 = mem[0],zero
	xorpd	%xmm1, %xmm1
	ucomisd	.LCPI0_14(%rip), %xmm7
	jb	.LBB0_25
# BB#19:                                #   in Loop: Header=BB0_13 Depth=3
	movsd	.LCPI0_2(%rip), %xmm13  # xmm13 = mem[0],zero
	movsd	.LCPI0_3(%rip), %xmm14  # xmm14 = mem[0],zero
	movsd	.LCPI0_4(%rip), %xmm15  # xmm15 = mem[0],zero
	jmp	.LBB0_51
	.p2align	4, 0x90
.LBB0_25:                               #   in Loop: Header=BB0_13 Depth=3
	movsd	16(%rsp), %xmm12        # xmm12 = mem[0],zero
	movsd	24(%rsp), %xmm2         # xmm2 = mem[0],zero
	movsd	_ZL5light(%rip), %xmm9  # xmm9 = mem[0],zero
	movapd	%xmm12, %xmm3
	mulsd	%xmm9, %xmm3
	movsd	_ZL5light+8(%rip), %xmm10 # xmm10 = mem[0],zero
	movapd	%xmm2, %xmm4
	mulsd	%xmm10, %xmm4
	addsd	%xmm3, %xmm4
	movsd	32(%rsp), %xmm5         # xmm5 = mem[0],zero
	movsd	_ZL5light+16(%rip), %xmm11 # xmm11 = mem[0],zero
	movapd	%xmm5, %xmm8
	mulsd	%xmm11, %xmm8
	addsd	%xmm4, %xmm8
	movsd	.LCPI0_15(%rip), %xmm0  # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm8
	movsd	.LCPI0_2(%rip), %xmm13  # xmm13 = mem[0],zero
	movsd	.LCPI0_3(%rip), %xmm14  # xmm14 = mem[0],zero
	movsd	.LCPI0_4(%rip), %xmm15  # xmm15 = mem[0],zero
	jae	.LBB0_51
# BB#26:                                #   in Loop: Header=BB0_13 Depth=3
	movsd	.LCPI0_15(%rip), %xmm1  # xmm1 = mem[0],zero
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	xorpd	%xmm1, %xmm8
	movq	_ZL4pool(%rip), %rax
	movq	_ZL3end(%rip), %rcx
	cmpq	%rcx, %rax
	movsd	.LCPI0_14(%rip), %xmm0  # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm3
	jae	.LBB0_50
# BB#27:                                # %.lr.ph.i.i.i.preheader
                                        #   in Loop: Header=BB0_13 Depth=3
	movsd	88(%rsp), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm7, %xmm1
	movsd	96(%rsp), %xmm3         # xmm3 = mem[0],zero
	mulsd	%xmm7, %xmm3
	mulsd	104(%rsp), %xmm7
	addsd	64(%rsp), %xmm1
	addsd	72(%rsp), %xmm3
	addsd	80(%rsp), %xmm7
	mulsd	%xmm14, %xmm12
	mulsd	%xmm14, %xmm2
	mulsd	%xmm14, %xmm5
	addsd	%xmm1, %xmm12
	addsd	%xmm3, %xmm2
	addsd	%xmm7, %xmm5
	movapd	.LCPI0_16(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm0, %xmm9
	xorpd	%xmm0, %xmm10
	xorpd	%xmm0, %xmm11
	jmp	.LBB0_28
	.p2align	4, 0x90
.LBB0_29:                               #   in Loop: Header=BB0_28 Depth=4
	movsd	.LCPI0_14(%rip), %xmm0  # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	movapd	%xmm0, %xmm3
	jae	.LBB0_34
# BB#30:                                # %.preheader.i.i4.i.i.preheader
                                        #   in Loop: Header=BB0_28 Depth=4
	xorpd	%xmm3, %xmm3
	movl	$-101, %edx
	movsd	.LCPI0_0(%rip), %xmm4   # xmm4 = mem[0],zero
	.p2align	4, 0x90
.LBB0_31:                               # %.preheader.i.i4.i.i
                                        #   Parent Loop BB0_11 Depth=1
                                        #     Parent Loop BB0_12 Depth=2
                                        #       Parent Loop BB0_13 Depth=3
                                        #         Parent Loop BB0_28 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	incl	%edx
	je	.LBB0_34
# BB#32:                                #   in Loop: Header=BB0_31 Depth=5
	movapd	%xmm1, %xmm3
	divsd	%xmm4, %xmm3
	addsd	%xmm4, %xmm3
	mulsd	%xmm13, %xmm3
	movapd	%xmm3, %xmm6
	subsd	%xmm4, %xmm6
	ucomisd	%xmm14, %xmm6
	movapd	%xmm3, %xmm4
	ja	.LBB0_31
# BB#33:                                #   in Loop: Header=BB0_31 Depth=5
	ucomisd	%xmm6, %xmm15
	movapd	%xmm3, %xmm4
	ja	.LBB0_31
	.p2align	4, 0x90
.LBB0_34:                               # %_ZL8LLVMsqrtd.exit.i8.i.i
                                        #   in Loop: Header=BB0_28 Depth=4
	movapd	%xmm7, %xmm1
	addsd	%xmm3, %xmm1
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm1, %xmm0
	ja	.LBB0_36
# BB#35:                                # %_ZL8LLVMsqrtd.exit.i8.i.i
                                        #   in Loop: Header=BB0_28 Depth=4
	subsd	%xmm3, %xmm7
	xorpd	%xmm0, %xmm0
	cmpltsd	%xmm7, %xmm0
	andpd	%xmm0, %xmm7
	andnpd	%xmm1, %xmm0
	orpd	%xmm7, %xmm0
	ucomisd	.LCPI0_14(%rip), %xmm0
	jae	.LBB0_36
# BB#39:                                #   in Loop: Header=BB0_28 Depth=4
	movsd	32(%rax), %xmm0         # xmm0 = mem[0],zero
	movsd	40(%rax), %xmm1         # xmm1 = mem[0],zero
	subsd	%xmm12, %xmm0
	subsd	%xmm2, %xmm1
	movsd	48(%rax), %xmm3         # xmm3 = mem[0],zero
	subsd	%xmm5, %xmm3
	movapd	%xmm0, %xmm4
	mulsd	%xmm9, %xmm4
	movapd	%xmm1, %xmm6
	mulsd	%xmm10, %xmm6
	addsd	%xmm4, %xmm6
	movapd	%xmm3, %xmm7
	mulsd	%xmm11, %xmm7
	addsd	%xmm6, %xmm7
	movapd	%xmm7, %xmm4
	mulsd	%xmm4, %xmm4
	mulsd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	mulsd	%xmm3, %xmm3
	addsd	%xmm1, %xmm3
	subsd	%xmm3, %xmm4
	movsd	56(%rax), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	addsd	%xmm4, %xmm1
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm1, %xmm0
	jbe	.LBB0_41
# BB#40:                                #   in Loop: Header=BB0_28 Depth=4
	movl	$1, %edx
	jmp	.LBB0_37
.LBB0_41:                               #   in Loop: Header=BB0_28 Depth=4
	movsd	.LCPI0_14(%rip), %xmm0  # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	movapd	%xmm0, %xmm3
	jae	.LBB0_46
# BB#42:                                # %.preheader.i.i.i.i.preheader
                                        #   in Loop: Header=BB0_28 Depth=4
	xorpd	%xmm3, %xmm3
	movl	$-101, %edx
	movsd	.LCPI0_0(%rip), %xmm4   # xmm4 = mem[0],zero
	.p2align	4, 0x90
.LBB0_43:                               # %.preheader.i.i.i.i
                                        #   Parent Loop BB0_11 Depth=1
                                        #     Parent Loop BB0_12 Depth=2
                                        #       Parent Loop BB0_13 Depth=3
                                        #         Parent Loop BB0_28 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	incl	%edx
	je	.LBB0_46
# BB#44:                                #   in Loop: Header=BB0_43 Depth=5
	movapd	%xmm1, %xmm3
	divsd	%xmm4, %xmm3
	addsd	%xmm4, %xmm3
	mulsd	%xmm13, %xmm3
	movapd	%xmm3, %xmm6
	subsd	%xmm4, %xmm6
	ucomisd	%xmm14, %xmm6
	movapd	%xmm3, %xmm4
	ja	.LBB0_43
# BB#45:                                #   in Loop: Header=BB0_43 Depth=5
	ucomisd	%xmm6, %xmm15
	movapd	%xmm3, %xmm4
	ja	.LBB0_43
.LBB0_46:                               # %_ZL8LLVMsqrtd.exit.i.i.i
                                        #   in Loop: Header=BB0_28 Depth=4
	movapd	%xmm7, %xmm1
	addsd	%xmm3, %xmm1
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm1, %xmm0
	ja	.LBB0_47
# BB#48:                                # %_ZL8LLVMsqrtd.exit.i.i.i
                                        #   in Loop: Header=BB0_28 Depth=4
	subsd	%xmm3, %xmm7
	xorpd	%xmm3, %xmm3
	cmpltsd	%xmm7, %xmm3
	andpd	%xmm3, %xmm7
	andnpd	%xmm1, %xmm3
	orpd	%xmm7, %xmm3
	movsd	.LCPI0_14(%rip), %xmm0  # xmm0 = mem[0],zero
	ucomisd	%xmm3, %xmm0
	movl	$1, %edx
	jbe	.LBB0_37
	jmp	.LBB0_49
.LBB0_47:                               #   in Loop: Header=BB0_28 Depth=4
	movl	$1, %edx
	jmp	.LBB0_37
	.p2align	4, 0x90
.LBB0_28:                               # %.lr.ph.i.i.i
                                        #   Parent Loop BB0_11 Depth=1
                                        #     Parent Loop BB0_12 Depth=2
                                        #       Parent Loop BB0_13 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_31 Depth 5
                                        #           Child Loop BB0_43 Depth 5
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	subsd	%xmm12, %xmm1
	subsd	%xmm2, %xmm0
	movsd	16(%rax), %xmm4         # xmm4 = mem[0],zero
	subsd	%xmm5, %xmm4
	movapd	%xmm1, %xmm6
	mulsd	%xmm9, %xmm6
	movapd	%xmm0, %xmm3
	mulsd	%xmm10, %xmm3
	addsd	%xmm6, %xmm3
	movapd	%xmm4, %xmm7
	mulsd	%xmm11, %xmm7
	addsd	%xmm3, %xmm7
	movapd	%xmm7, %xmm3
	mulsd	%xmm3, %xmm3
	mulsd	%xmm1, %xmm1
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	mulsd	%xmm4, %xmm4
	addsd	%xmm0, %xmm4
	subsd	%xmm4, %xmm3
	movsd	24(%rax), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	addsd	%xmm3, %xmm1
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm1, %xmm0
	jbe	.LBB0_29
.LBB0_36:                               # %_ZNK8sphere_t9intersectERK5ray_t.exit10.thread.i.i
                                        #   in Loop: Header=BB0_28 Depth=4
	movq	64(%rax), %rdx
.LBB0_37:                               # %.backedge.i.i.i
                                        #   in Loop: Header=BB0_28 Depth=4
	leaq	(%rdx,%rdx,8), %rdx
	leaq	(%rax,%rdx,8), %rax
	cmpq	%rcx, %rax
	jb	.LBB0_28
# BB#38:                                #   in Loop: Header=BB0_13 Depth=3
	movsd	.LCPI0_14(%rip), %xmm0  # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm3
.LBB0_50:                               # %_ZN6node_t9intersectILb1EEEvRK5ray_tR5hit_t.exit.i.i
                                        #   in Loop: Header=BB0_13 Depth=3
	movapd	%xmm0, %xmm1
	cmplesd	%xmm3, %xmm1
	andpd	%xmm8, %xmm1
	.p2align	4, 0x90
.LBB0_51:                               # %_ZL9ray_tracePK6node_tRK5ray_t.exit.i
                                        #   in Loop: Header=BB0_13 Depth=3
	movsd	56(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm1, %xmm0
	incq	%rbp
	cmpq	$4, %rbp
	jne	.LBB0_13
# BB#23:                                #   in Loop: Header=BB0_12 Depth=2
	mulsd	.LCPI0_17(%rip), %xmm0
	cvttsd2si	%xmm0, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSolsEi
	movl	$.L.str.2, %esi
	movl	$1, %edx
	movq	%rax, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movapd	112(%rsp), %xmm0        # 16-byte Reload
	addsd	.LCPI0_0(%rip), %xmm0
	decl	%r12d
	jne	.LBB0_12
# BB#24:                                #   in Loop: Header=BB0_11 Depth=1
	movapd	(%rsp), %xmm0           # 16-byte Reload
	addsd	.LCPI0_18(%rip), %xmm0
	movapd	%xmm0, (%rsp)           # 16-byte Spill
	decl	%r15d
	jne	.LBB0_11
# BB#20:
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_54
# BB#21:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit.i
	cmpb	$0, 56(%rbx)
	je	.LBB0_52
# BB#22:
	movb	67(%rbx), %al
	jmp	.LBB0_53
.LBB0_52:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB0_53:                               # %_ZL10trace_rgssii.exit
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	xorl	%eax, %eax
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_54:
	callq	_ZSt16__throw_bad_castv
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4613937818241073152     # double 3
.LCPI1_1:
	.quad	4596373779694328218     # double 0.20000000000000001
.LCPI1_2:
	.quad	4618760256179214148     # double 6.2831853070000001
.LCPI1_3:
	.quad	-4604611780675561660    # double -6.2831853070000001
.LCPI1_4:
	.quad	4616991696745479712     # double 4.7123889840000004
.LCPI1_5:
	.quad	4614256656557473151     # double 3.1415926559999998
.LCPI1_6:
	.quad	4607182418800017408     # double 1
.LCPI1_7:
	.quad	4609753056925599056     # double 1.570796327
.LCPI1_8:
	.quad	-4609115380297302657    # double -3.1415926559999998
.LCPI1_9:
	.quad	-4616189618054758400    # double -1
.LCPI1_10:
	.quad	-4604930618986332160    # double -6
.LCPI1_11:
	.quad	4638144666238189568     # double 120
.LCPI1_12:
	.quad	9218868437227405312     # double +Inf
.LCPI1_13:
	.quad	4602678819172646912     # double 0.5
.LCPI1_14:
	.quad	4427486594234968593     # double 9.9999999999999998E-13
.LCPI1_15:
	.quad	-4795885442619807215    # double -9.9999999999999998E-13
.LCPI1_16:
	.quad	4607394977673999205     # double 1.0471975511965976
.LCPI1_17:
	.quad	-4623412205601733497    # double -0.3490658503988659
.LCPI1_18:
	.quad	4603579539098121011     # double 0.59999999999999998
.LCPI1_19:
	.quad	4611898577301369701     # double 2.0943951023931953
	.text
	.p2align	4, 0x90
	.type	_ZL6createP6node_tii3v_tS1_d,@function
_ZL6createP6node_tii3v_tS1_d:           # @_ZL6createP6node_tii3v_tS1_d
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	subq	$152, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 192
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movsd	%xmm3, 24(%rsp)
	movsd	%xmm4, 32(%rsp)
	movsd	%xmm5, 40(%rsp)
	movapd	%xmm6, %xmm3
	addsd	%xmm3, %xmm3
	cmpl	$1, %r15d
	movslq	%edx, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	movsd	%xmm0, (%rbx)
	movsd	%xmm1, 8(%rbx)
	movsd	%xmm2, 16(%rbx)
	movsd	%xmm3, 24(%rbx)
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	movsd	%xmm0, 32(%rbx)
	movsd	%xmm1, 64(%rsp)         # 8-byte Spill
	movsd	%xmm1, 40(%rbx)
	movsd	%xmm2, 72(%rsp)         # 8-byte Spill
	movsd	%xmm2, 48(%rbx)
	movsd	%xmm6, 56(%rbx)
	movq	%rcx, 64(%rbx)
	addq	$72, %rbx
	cmpl	$2, %r15d
	jl	.LBB1_63
# BB#1:
	addl	$-9, %edx
	movslq	%edx, %rax
	imulq	$954437177, %rax, %rax  # imm = 0x38E38E39
	movq	%rax, %rcx
	shrq	$63, %rcx
	sarq	$33, %rax
	addl	%ecx, %eax
	testl	%eax, %eax
	movl	$1, %r14d
	cmovgl	%eax, %r14d
	leaq	80(%rsp), %rdi
	leaq	24(%rsp), %rsi
	movsd	%xmm6, 8(%rsp)          # 8-byte Spill
	callq	_ZN7basis_tC2ERK3v_t
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm0
	divsd	.LCPI1_0(%rip), %xmm0
	decl	%r15d
	movsd	%xmm0, 48(%rsp)         # 8-byte Spill
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	xorpd	%xmm0, %xmm0
	xorl	%ebp, %ebp
	movsd	.LCPI1_2(%rip), %xmm7   # xmm7 = mem[0],zero
	movsd	.LCPI1_3(%rip), %xmm14  # xmm14 = mem[0],zero
	movsd	.LCPI1_4(%rip), %xmm9   # xmm9 = mem[0],zero
	movsd	.LCPI1_9(%rip), %xmm12  # xmm12 = mem[0],zero
	xorpd	%xmm10, %xmm10
	.p2align	4, 0x90
.LBB1_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_64 Depth 2
                                        #     Child Loop BB1_4 Depth 2
                                        #     Child Loop BB1_15 Depth 2
                                        #     Child Loop BB1_17 Depth 2
                                        #     Child Loop BB1_28 Depth 2
	movsd	24(%rsp), %xmm8         # xmm8 = mem[0],zero
	movsd	.LCPI1_1(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	%xmm2, %xmm3
	mulsd	%xmm3, %xmm8
	movsd	32(%rsp), %xmm11        # xmm11 = mem[0],zero
	mulsd	%xmm3, %xmm11
	movsd	40(%rsp), %xmm2         # xmm2 = mem[0],zero
	mulsd	%xmm3, %xmm2
	ucomisd	%xmm10, %xmm0
	movapd	%xmm10, %xmm3
	jbe	.LBB1_3
	.p2align	4, 0x90
.LBB1_64:                               # %.lr.ph31.i74
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	%xmm7, %xmm3
	ucomisd	%xmm3, %xmm0
	ja	.LBB1_64
.LBB1_3:                                # %.preheader.i72
                                        #   in Loop: Header=BB1_2 Depth=1
	ucomisd	%xmm7, %xmm3
	movsd	.LCPI1_6(%rip), %xmm13  # xmm13 = mem[0],zero
	jbe	.LBB1_5
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph.i76
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	%xmm14, %xmm3
	ucomisd	%xmm7, %xmm3
	ja	.LBB1_4
.LBB1_5:                                # %._crit_edge.i78
                                        #   in Loop: Header=BB1_2 Depth=1
	ucomisd	%xmm9, %xmm3
	jbe	.LBB1_7
# BB#6:                                 #   in Loop: Header=BB1_2 Depth=1
	movapd	%xmm7, %xmm5
	subsd	%xmm3, %xmm5
	movapd	%xmm12, %xmm4
.LBB1_12:                               # %_ZL7LLVMsind.exit84
                                        #   in Loop: Header=BB1_2 Depth=1
	movapd	%xmm5, %xmm3
	jmp	.LBB1_13
	.p2align	4, 0x90
.LBB1_7:                                #   in Loop: Header=BB1_2 Depth=1
	ucomisd	.LCPI1_5(%rip), %xmm3
	jbe	.LBB1_9
# BB#8:                                 #   in Loop: Header=BB1_2 Depth=1
	addsd	.LCPI1_8(%rip), %xmm3
	movapd	%xmm12, %xmm4
	jmp	.LBB1_13
	.p2align	4, 0x90
.LBB1_9:                                #   in Loop: Header=BB1_2 Depth=1
	ucomisd	.LCPI1_7(%rip), %xmm3
	jbe	.LBB1_10
# BB#11:                                #   in Loop: Header=BB1_2 Depth=1
	movsd	.LCPI1_5(%rip), %xmm5   # xmm5 = mem[0],zero
	subsd	%xmm3, %xmm5
	movapd	%xmm13, %xmm4
	jmp	.LBB1_12
.LBB1_10:                               #   in Loop: Header=BB1_2 Depth=1
	movapd	%xmm13, %xmm4
	.p2align	4, 0x90
.LBB1_13:                               # %_ZL7LLVMsind.exit84
                                        #   in Loop: Header=BB1_2 Depth=1
	movapd	%xmm3, %xmm5
	mulsd	%xmm5, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm3, %xmm6
	mulsd	%xmm5, %xmm6
	mulsd	%xmm3, %xmm6
	divsd	.LCPI1_10(%rip), %xmm5
	divsd	.LCPI1_11(%rip), %xmm6
	addsd	%xmm3, %xmm5
	addsd	%xmm6, %xmm5
	mulsd	%xmm4, %xmm5
	movapd	%xmm13, %xmm3
	minsd	%xmm5, %xmm3
	movapd	%xmm12, %xmm5
	maxsd	%xmm3, %xmm5
	movsd	104(%rsp), %xmm3        # xmm3 = mem[0],zero
	mulsd	%xmm5, %xmm3
	movsd	112(%rsp), %xmm4        # xmm4 = mem[0],zero
	mulsd	%xmm5, %xmm4
	mulsd	120(%rsp), %xmm5
	movapd	%xmm10, %xmm6
	addsd	.LCPI1_7(%rip), %xmm6
	ucomisd	%xmm6, %xmm0
	jbe	.LBB1_17
	.p2align	4, 0x90
.LBB1_15:                               # %.lr.ph31.i.i61
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	%xmm7, %xmm6
	ucomisd	%xmm6, %xmm0
	ja	.LBB1_15
	jmp	.LBB1_17
	.p2align	4, 0x90
.LBB1_16:                               # %.lr.ph.i.i63
                                        #   in Loop: Header=BB1_17 Depth=2
	addsd	%xmm14, %xmm6
.LBB1_17:                               # %.lr.ph.i.i63
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	ucomisd	%xmm7, %xmm6
	ja	.LBB1_16
# BB#18:                                # %._crit_edge.i.i65
                                        #   in Loop: Header=BB1_2 Depth=1
	subsd	%xmm8, %xmm3
	subsd	%xmm11, %xmm4
	subsd	%xmm2, %xmm5
	ucomisd	%xmm9, %xmm6
	jbe	.LBB1_20
# BB#19:                                #   in Loop: Header=BB1_2 Depth=1
	movapd	%xmm7, %xmm1
	subsd	%xmm6, %xmm1
	movapd	%xmm12, %xmm0
.LBB1_25:                               # %_ZL7LLVMcosd.exit70
                                        #   in Loop: Header=BB1_2 Depth=1
	movapd	%xmm1, %xmm6
	jmp	.LBB1_26
	.p2align	4, 0x90
.LBB1_20:                               #   in Loop: Header=BB1_2 Depth=1
	ucomisd	.LCPI1_5(%rip), %xmm6
	jbe	.LBB1_22
# BB#21:                                #   in Loop: Header=BB1_2 Depth=1
	addsd	.LCPI1_8(%rip), %xmm6
	movapd	%xmm12, %xmm0
	jmp	.LBB1_26
	.p2align	4, 0x90
.LBB1_22:                               #   in Loop: Header=BB1_2 Depth=1
	ucomisd	.LCPI1_7(%rip), %xmm6
	jbe	.LBB1_23
# BB#24:                                #   in Loop: Header=BB1_2 Depth=1
	movsd	.LCPI1_5(%rip), %xmm1   # xmm1 = mem[0],zero
	subsd	%xmm6, %xmm1
	movapd	%xmm13, %xmm0
	jmp	.LBB1_25
.LBB1_23:                               #   in Loop: Header=BB1_2 Depth=1
	movapd	%xmm13, %xmm0
	.p2align	4, 0x90
.LBB1_26:                               # %_ZL7LLVMcosd.exit70
                                        #   in Loop: Header=BB1_2 Depth=1
	movsd	.LCPI1_13(%rip), %xmm7  # xmm7 = mem[0],zero
	movapd	%xmm7, %xmm8
	movsd	.LCPI1_14(%rip), %xmm7  # xmm7 = mem[0],zero
	movsd	.LCPI1_15(%rip), %xmm9  # xmm9 = mem[0],zero
	movsd	%xmm10, 16(%rsp)        # 8-byte Spill
	movapd	%xmm6, %xmm1
	mulsd	%xmm1, %xmm1
	mulsd	%xmm6, %xmm1
	movapd	%xmm6, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm6, %xmm2
	divsd	.LCPI1_10(%rip), %xmm1
	divsd	.LCPI1_11(%rip), %xmm2
	addsd	%xmm6, %xmm1
	addsd	%xmm2, %xmm1
	mulsd	%xmm0, %xmm1
	movapd	%xmm13, %xmm0
	minsd	%xmm1, %xmm0
	movapd	%xmm12, %xmm1
	maxsd	%xmm0, %xmm1
	movsd	128(%rsp), %xmm0        # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	136(%rsp), %xmm2        # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	mulsd	144(%rsp), %xmm1
	addsd	%xmm0, %xmm3
	addsd	%xmm2, %xmm4
	addsd	%xmm1, %xmm5
	movapd	%xmm3, %xmm0
	mulsd	%xmm0, %xmm0
	movapd	%xmm4, %xmm1
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm5, %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	movsd	.LCPI1_12(%rip), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jae	.LBB1_31
# BB#27:                                # %.preheader.i.i54.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	xorpd	%xmm1, %xmm1
	movl	$-101, %eax
	movapd	%xmm13, %xmm2
	.p2align	4, 0x90
.LBB1_28:                               # %.preheader.i.i54
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%eax
	je	.LBB1_31
# BB#29:                                #   in Loop: Header=BB1_28 Depth=2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	addsd	%xmm2, %xmm1
	mulsd	%xmm8, %xmm1
	movapd	%xmm1, %xmm6
	subsd	%xmm2, %xmm6
	ucomisd	%xmm7, %xmm6
	movapd	%xmm1, %xmm2
	ja	.LBB1_28
# BB#30:                                #   in Loop: Header=BB1_28 Depth=2
	ucomisd	%xmm6, %xmm9
	movapd	%xmm1, %xmm2
	ja	.LBB1_28
	.p2align	4, 0x90
.LBB1_31:                               # %_ZNK3v_t4normEv.exit57
                                        #   in Loop: Header=BB1_2 Depth=1
	movapd	%xmm13, %xmm0
	divsd	%xmm1, %xmm0
	mulsd	%xmm0, %xmm3
	mulsd	%xmm0, %xmm4
	mulsd	%xmm0, %xmm5
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movapd	%xmm2, %xmm0
	mulsd	%xmm3, %xmm0
	movapd	%xmm2, %xmm1
	mulsd	%xmm4, %xmm1
	mulsd	%xmm5, %xmm2
	addsd	56(%rsp), %xmm0         # 8-byte Folded Reload
	addsd	64(%rsp), %xmm1         # 8-byte Folded Reload
	addsd	72(%rsp), %xmm2         # 8-byte Folded Reload
	movq	%rbx, %rdi
	movl	%r15d, %esi
	movl	%r14d, %edx
	movsd	48(%rsp), %xmm6         # 8-byte Reload
                                        # xmm6 = mem[0],zero
	callq	_ZL6createP6node_tii3v_tS1_d
	movq	%rax, %rbx
	movsd	16(%rsp), %xmm10        # 8-byte Reload
                                        # xmm10 = mem[0],zero
	addsd	.LCPI1_16(%rip), %xmm10
	incl	%ebp
	cmpl	$6, %ebp
	movsd	.LCPI1_2(%rip), %xmm7   # xmm7 = mem[0],zero
	movsd	.LCPI1_3(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm14
	movsd	.LCPI1_4(%rip), %xmm9   # xmm9 = mem[0],zero
	xorpd	%xmm0, %xmm0
	movsd	.LCPI1_9(%rip), %xmm12  # xmm12 = mem[0],zero
	jne	.LBB1_2
# BB#32:
	addsd	.LCPI1_17(%rip), %xmm10
	xorl	%ebp, %ebp
	xorpd	%xmm1, %xmm1
	movsd	.LCPI1_9(%rip), %xmm11  # xmm11 = mem[0],zero
	.p2align	4, 0x90
.LBB1_33:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_65 Depth 2
                                        #     Child Loop BB1_35 Depth 2
                                        #     Child Loop BB1_46 Depth 2
                                        #     Child Loop BB1_48 Depth 2
                                        #     Child Loop BB1_59 Depth 2
	movsd	24(%rsp), %xmm3         # xmm3 = mem[0],zero
	movsd	.LCPI1_18(%rip), %xmm0  # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm3
	movsd	32(%rsp), %xmm4         # xmm4 = mem[0],zero
	mulsd	%xmm0, %xmm4
	movsd	40(%rsp), %xmm5         # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	ucomisd	%xmm10, %xmm1
	movapd	%xmm10, %xmm0
	jbe	.LBB1_34
	.p2align	4, 0x90
.LBB1_65:                               # %.lr.ph31.i
                                        #   Parent Loop BB1_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	%xmm7, %xmm0
	ucomisd	%xmm0, %xmm1
	ja	.LBB1_65
.LBB1_34:                               # %.preheader.i
                                        #   in Loop: Header=BB1_33 Depth=1
	ucomisd	%xmm7, %xmm0
	movsd	.LCPI1_6(%rip), %xmm12  # xmm12 = mem[0],zero
	jbe	.LBB1_36
	.p2align	4, 0x90
.LBB1_35:                               # %.lr.ph.i
                                        #   Parent Loop BB1_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	%xmm14, %xmm0
	ucomisd	%xmm7, %xmm0
	ja	.LBB1_35
.LBB1_36:                               # %._crit_edge.i
                                        #   in Loop: Header=BB1_33 Depth=1
	ucomisd	%xmm9, %xmm0
	jbe	.LBB1_38
# BB#37:                                #   in Loop: Header=BB1_33 Depth=1
	movapd	%xmm7, %xmm2
	subsd	%xmm0, %xmm2
	movapd	%xmm11, %xmm8
.LBB1_43:                               # %_ZL7LLVMsind.exit
                                        #   in Loop: Header=BB1_33 Depth=1
	movapd	%xmm2, %xmm0
	jmp	.LBB1_44
	.p2align	4, 0x90
.LBB1_38:                               #   in Loop: Header=BB1_33 Depth=1
	ucomisd	.LCPI1_5(%rip), %xmm0
	jbe	.LBB1_40
# BB#39:                                #   in Loop: Header=BB1_33 Depth=1
	addsd	.LCPI1_8(%rip), %xmm0
	movapd	%xmm11, %xmm8
	jmp	.LBB1_44
	.p2align	4, 0x90
.LBB1_40:                               #   in Loop: Header=BB1_33 Depth=1
	ucomisd	.LCPI1_7(%rip), %xmm0
	jbe	.LBB1_41
# BB#42:                                #   in Loop: Header=BB1_33 Depth=1
	movsd	.LCPI1_5(%rip), %xmm2   # xmm2 = mem[0],zero
	subsd	%xmm0, %xmm2
	movapd	%xmm12, %xmm8
	jmp	.LBB1_43
.LBB1_41:                               #   in Loop: Header=BB1_33 Depth=1
	movapd	%xmm12, %xmm8
	.p2align	4, 0x90
.LBB1_44:                               # %_ZL7LLVMsind.exit
                                        #   in Loop: Header=BB1_33 Depth=1
	movapd	%xmm0, %xmm2
	mulsd	%xmm2, %xmm2
	mulsd	%xmm0, %xmm2
	movapd	%xmm0, %xmm6
	mulsd	%xmm2, %xmm6
	mulsd	%xmm0, %xmm6
	divsd	.LCPI1_10(%rip), %xmm2
	divsd	.LCPI1_11(%rip), %xmm6
	addsd	%xmm0, %xmm2
	addsd	%xmm6, %xmm2
	mulsd	%xmm8, %xmm2
	movapd	%xmm12, %xmm0
	minsd	%xmm2, %xmm0
	movapd	%xmm11, %xmm2
	maxsd	%xmm0, %xmm2
	movsd	104(%rsp), %xmm8        # xmm8 = mem[0],zero
	mulsd	%xmm2, %xmm8
	movsd	112(%rsp), %xmm6        # xmm6 = mem[0],zero
	mulsd	%xmm2, %xmm6
	mulsd	120(%rsp), %xmm2
	movapd	%xmm10, %xmm0
	addsd	.LCPI1_7(%rip), %xmm0
	ucomisd	%xmm0, %xmm1
	jbe	.LBB1_48
	.p2align	4, 0x90
.LBB1_46:                               # %.lr.ph31.i.i
                                        #   Parent Loop BB1_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	%xmm7, %xmm0
	ucomisd	%xmm0, %xmm1
	ja	.LBB1_46
	jmp	.LBB1_48
	.p2align	4, 0x90
.LBB1_47:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB1_48 Depth=2
	addsd	%xmm14, %xmm0
.LBB1_48:                               # %.lr.ph.i.i
                                        #   Parent Loop BB1_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	ucomisd	%xmm7, %xmm0
	ja	.LBB1_47
# BB#49:                                # %._crit_edge.i.i
                                        #   in Loop: Header=BB1_33 Depth=1
	addsd	%xmm8, %xmm3
	addsd	%xmm6, %xmm4
	addsd	%xmm2, %xmm5
	ucomisd	%xmm9, %xmm0
	jbe	.LBB1_51
# BB#50:                                #   in Loop: Header=BB1_33 Depth=1
	movapd	%xmm7, %xmm2
	subsd	%xmm0, %xmm2
	movapd	%xmm11, %xmm1
.LBB1_56:                               # %_ZL7LLVMcosd.exit
                                        #   in Loop: Header=BB1_33 Depth=1
	movapd	%xmm2, %xmm0
	jmp	.LBB1_57
	.p2align	4, 0x90
.LBB1_51:                               #   in Loop: Header=BB1_33 Depth=1
	ucomisd	.LCPI1_5(%rip), %xmm0
	jbe	.LBB1_53
# BB#52:                                #   in Loop: Header=BB1_33 Depth=1
	addsd	.LCPI1_8(%rip), %xmm0
	movapd	%xmm11, %xmm1
	jmp	.LBB1_57
	.p2align	4, 0x90
.LBB1_53:                               #   in Loop: Header=BB1_33 Depth=1
	ucomisd	.LCPI1_7(%rip), %xmm0
	jbe	.LBB1_54
# BB#55:                                #   in Loop: Header=BB1_33 Depth=1
	movsd	.LCPI1_5(%rip), %xmm2   # xmm2 = mem[0],zero
	subsd	%xmm0, %xmm2
	movapd	%xmm12, %xmm1
	jmp	.LBB1_56
.LBB1_54:                               #   in Loop: Header=BB1_33 Depth=1
	movapd	%xmm12, %xmm1
	.p2align	4, 0x90
.LBB1_57:                               # %_ZL7LLVMcosd.exit
                                        #   in Loop: Header=BB1_33 Depth=1
	movsd	.LCPI1_13(%rip), %xmm7  # xmm7 = mem[0],zero
	movapd	%xmm7, %xmm8
	movsd	.LCPI1_14(%rip), %xmm7  # xmm7 = mem[0],zero
	movsd	.LCPI1_15(%rip), %xmm9  # xmm9 = mem[0],zero
	movsd	%xmm10, 16(%rsp)        # 8-byte Spill
	movapd	%xmm0, %xmm2
	mulsd	%xmm2, %xmm2
	mulsd	%xmm0, %xmm2
	movapd	%xmm0, %xmm6
	mulsd	%xmm2, %xmm6
	mulsd	%xmm0, %xmm6
	divsd	.LCPI1_10(%rip), %xmm2
	divsd	.LCPI1_11(%rip), %xmm6
	addsd	%xmm0, %xmm2
	addsd	%xmm6, %xmm2
	mulsd	%xmm1, %xmm2
	movapd	%xmm12, %xmm0
	minsd	%xmm2, %xmm0
	movapd	%xmm11, %xmm1
	maxsd	%xmm0, %xmm1
	movsd	128(%rsp), %xmm0        # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	136(%rsp), %xmm2        # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	mulsd	144(%rsp), %xmm1
	addsd	%xmm0, %xmm3
	addsd	%xmm2, %xmm4
	addsd	%xmm1, %xmm5
	movapd	%xmm3, %xmm0
	mulsd	%xmm0, %xmm0
	movapd	%xmm4, %xmm1
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm5, %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	movsd	.LCPI1_12(%rip), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jae	.LBB1_62
# BB#58:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB1_33 Depth=1
	xorpd	%xmm1, %xmm1
	movl	$-101, %eax
	movapd	%xmm12, %xmm2
	.p2align	4, 0x90
.LBB1_59:                               # %.preheader.i.i
                                        #   Parent Loop BB1_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%eax
	je	.LBB1_62
# BB#60:                                #   in Loop: Header=BB1_59 Depth=2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	addsd	%xmm2, %xmm1
	mulsd	%xmm8, %xmm1
	movapd	%xmm1, %xmm6
	subsd	%xmm2, %xmm6
	ucomisd	%xmm7, %xmm6
	movapd	%xmm1, %xmm2
	ja	.LBB1_59
# BB#61:                                #   in Loop: Header=BB1_59 Depth=2
	ucomisd	%xmm6, %xmm9
	movapd	%xmm1, %xmm2
	ja	.LBB1_59
	.p2align	4, 0x90
.LBB1_62:                               # %_ZNK3v_t4normEv.exit
                                        #   in Loop: Header=BB1_33 Depth=1
	movapd	%xmm12, %xmm0
	divsd	%xmm1, %xmm0
	mulsd	%xmm0, %xmm3
	mulsd	%xmm0, %xmm4
	mulsd	%xmm0, %xmm5
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movapd	%xmm2, %xmm0
	mulsd	%xmm3, %xmm0
	movapd	%xmm2, %xmm1
	mulsd	%xmm4, %xmm1
	mulsd	%xmm5, %xmm2
	addsd	56(%rsp), %xmm0         # 8-byte Folded Reload
	addsd	64(%rsp), %xmm1         # 8-byte Folded Reload
	addsd	72(%rsp), %xmm2         # 8-byte Folded Reload
	movq	%rbx, %rdi
	movl	%r15d, %esi
	movl	%r14d, %edx
	movsd	48(%rsp), %xmm6         # 8-byte Reload
                                        # xmm6 = mem[0],zero
	callq	_ZL6createP6node_tii3v_tS1_d
	movq	%rax, %rbx
	movsd	16(%rsp), %xmm10        # 8-byte Reload
                                        # xmm10 = mem[0],zero
	addsd	.LCPI1_19(%rip), %xmm10
	incl	%ebp
	cmpl	$3, %ebp
	movsd	.LCPI1_2(%rip), %xmm7   # xmm7 = mem[0],zero
	movsd	.LCPI1_3(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm14
	movsd	.LCPI1_4(%rip), %xmm9   # xmm9 = mem[0],zero
	xorpd	%xmm1, %xmm1
	movsd	.LCPI1_9(%rip), %xmm11  # xmm11 = mem[0],zero
	jne	.LBB1_33
.LBB1_63:
	movq	%rbx, %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZL6createP6node_tii3v_tS1_d, .Lfunc_end1-_ZL6createP6node_tii3v_tS1_d
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	9218868437227405312     # double +Inf
.LCPI2_1:
	.quad	4607182418800017408     # double 1
.LCPI2_2:
	.quad	4602678819172646912     # double 0.5
.LCPI2_3:
	.quad	4427486594234968593     # double 9.9999999999999998E-13
.LCPI2_4:
	.quad	-4795885442619807215    # double -9.9999999999999998E-13
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_5:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.section	.text._ZN7basis_tC2ERK3v_t,"axG",@progbits,_ZN7basis_tC2ERK3v_t,comdat
	.weak	_ZN7basis_tC2ERK3v_t
	.p2align	4, 0x90
	.type	_ZN7basis_tC2ERK3v_t,@function
_ZN7basis_tC2ERK3v_t:                   # @_ZN7basis_tC2ERK3v_t
	.cfi_startproc
# BB#0:
	movupd	(%rsi), %xmm0
	movapd	%xmm0, %xmm1
	mulsd	%xmm1, %xmm1
	movapd	%xmm0, %xmm3
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	mulsd	%xmm3, %xmm3
	addsd	%xmm1, %xmm3
	movsd	16(%rsi), %xmm1         # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	mulsd	%xmm2, %xmm2
	addsd	%xmm3, %xmm2
	movsd	.LCPI2_0(%rip), %xmm3   # xmm3 = mem[0],zero
	ucomisd	%xmm3, %xmm2
	jae	.LBB2_5
# BB#1:                                 # %.preheader.i.i.preheader
	movsd	.LCPI2_1(%rip), %xmm7   # xmm7 = mem[0],zero
	xorpd	%xmm3, %xmm3
	movl	$-101, %eax
	movsd	.LCPI2_2(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	.LCPI2_3(%rip), %xmm5   # xmm5 = mem[0],zero
	movsd	.LCPI2_4(%rip), %xmm8   # xmm8 = mem[0],zero
	.p2align	4, 0x90
.LBB2_2:                                # %.preheader.i.i
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	je	.LBB2_5
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	movapd	%xmm2, %xmm3
	divsd	%xmm7, %xmm3
	addsd	%xmm7, %xmm3
	mulsd	%xmm4, %xmm3
	movapd	%xmm3, %xmm6
	subsd	%xmm7, %xmm6
	ucomisd	%xmm5, %xmm6
	movapd	%xmm3, %xmm7
	ja	.LBB2_2
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	ucomisd	%xmm6, %xmm8
	movapd	%xmm3, %xmm7
	ja	.LBB2_2
.LBB2_5:                                # %_ZNK3v_t4normEv.exit
	movsd	.LCPI2_1(%rip), %xmm4   # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm2
	divsd	%xmm3, %xmm2
	mulsd	%xmm2, %xmm1
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm2, %xmm0
	movapd	%xmm0, %xmm2
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	movapd	%xmm1, %xmm3
	mulsd	%xmm3, %xmm3
	ucomisd	%xmm4, %xmm3
	jne	.LBB2_6
	jnp	.LBB2_11
.LBB2_6:                                # %_ZNK3v_t4normEv.exit
	movapd	%xmm0, %xmm4
	mulsd	%xmm4, %xmm4
	ucomisd	.LCPI2_1(%rip), %xmm4
	jne	.LBB2_7
	jnp	.LBB2_11
.LBB2_7:                                # %_ZNK3v_t4normEv.exit
	movaps	%xmm2, %xmm5
	mulsd	%xmm5, %xmm5
	ucomisd	.LCPI2_1(%rip), %xmm5
	jne	.LBB2_8
	jnp	.LBB2_11
.LBB2_8:
	leaq	32(%rdi), %rax
	movupd	%xmm0, 24(%rdi)
	leaq	40(%rdi), %rcx
	movsd	%xmm1, 40(%rdi)
	ucomisd	%xmm4, %xmm5
	jbe	.LBB2_12
# BB#9:
	ucomisd	%xmm3, %xmm5
	jbe	.LBB2_13
# BB#10:
	movaps	.LCPI2_5(%rip), %xmm3   # xmm3 = [-0.000000e+00,-0.000000e+00]
	xorps	%xmm2, %xmm3
	movlps	%xmm3, (%rax)
	movapd	%xmm0, %xmm4
	unpcklpd	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0]
	jmp	.LBB2_15
.LBB2_11:
	movsd	%xmm1, 24(%rdi)
	leaq	32(%rdi), %rax
	leaq	40(%rdi), %rcx
	movupd	%xmm0, 32(%rdi)
	movapd	%xmm1, %xmm4
	unpcklpd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0]
	movaps	%xmm2, %xmm3
	jmp	.LBB2_16
.LBB2_12:
	ucomisd	%xmm4, %xmm3
	jbe	.LBB2_14
.LBB2_13:
	movapd	.LCPI2_5(%rip), %xmm3   # xmm3 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm1, %xmm3
	movlpd	%xmm3, (%rcx)
	movapd	%xmm0, %xmm4
	jmp	.LBB2_16
.LBB2_14:
	movapd	.LCPI2_5(%rip), %xmm3   # xmm3 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm0, %xmm3
	movlpd	%xmm3, 24(%rdi)
	movapd	%xmm0, %xmm4
	movsd	%xmm3, %xmm4            # xmm4 = xmm3[0],xmm4[1]
.LBB2_15:
	movapd	%xmm1, %xmm3
.LBB2_16:
	movupd	%xmm0, (%rdi)
	movsd	%xmm1, 16(%rdi)
	movaps	%xmm2, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm6
	movhlps	%xmm6, %xmm6            # xmm6 = xmm6[1,1]
	mulsd	%xmm1, %xmm6
	subsd	%xmm6, %xmm5
	movapd	%xmm1, %xmm6
	unpcklpd	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0]
	mulpd	%xmm4, %xmm6
	unpcklpd	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0]
	mulpd	%xmm0, %xmm3
	subpd	%xmm3, %xmm6
	movsd	%xmm5, 48(%rdi)
	movupd	%xmm6, 56(%rdi)
	movapd	%xmm6, %xmm3
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	movaps	%xmm3, %xmm4
	mulsd	%xmm2, %xmm4
	movapd	%xmm1, %xmm7
	mulsd	%xmm6, %xmm7
	subsd	%xmm7, %xmm4
	mulsd	%xmm5, %xmm1
	mulsd	%xmm0, %xmm3
	subsd	%xmm3, %xmm1
	mulsd	%xmm0, %xmm6
	mulsd	%xmm2, %xmm5
	subsd	%xmm5, %xmm6
	movsd	%xmm4, 24(%rdi)
	movsd	%xmm1, (%rax)
	movsd	%xmm6, (%rcx)
	retq
.Lfunc_end2:
	.size	_ZN7basis_tC2ERK3v_t, .Lfunc_end2-_ZN7basis_tC2ERK3v_t
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	9218868437227405312     # double +Inf
.LCPI3_1:
	.quad	4607182418800017408     # double 1
.LCPI3_2:
	.quad	4602678819172646912     # double 0.5
.LCPI3_3:
	.quad	4427486594234968593     # double 9.9999999999999998E-13
.LCPI3_4:
	.quad	-4795885442619807215    # double -9.9999999999999998E-13
.LCPI3_5:
	.quad	0                       # double 0
	.section	.text._ZN6node_t9intersectILb0EEEvRK5ray_tR5hit_t,"axG",@progbits,_ZN6node_t9intersectILb0EEEvRK5ray_tR5hit_t,comdat
	.weak	_ZN6node_t9intersectILb0EEEvRK5ray_tR5hit_t
	.p2align	4, 0x90
	.type	_ZN6node_t9intersectILb0EEEvRK5ray_tR5hit_t,@function
_ZN6node_t9intersectILb0EEEvRK5ray_tR5hit_t: # @_ZN6node_t9intersectILb0EEEvRK5ray_tR5hit_t
	.cfi_startproc
# BB#0:
	movq	_ZL4pool(%rip), %rax
	movq	_ZL3end(%rip), %rcx
	cmpq	%rcx, %rax
	jae	.LBB3_22
# BB#1:                                 # %.lr.ph
	movsd	.LCPI3_0(%rip), %xmm5   # xmm5 = mem[0],zero
	xorpd	%xmm11, %xmm11
	movsd	.LCPI3_2(%rip), %xmm13  # xmm13 = mem[0],zero
	movsd	.LCPI3_3(%rip), %xmm14  # xmm14 = mem[0],zero
	movsd	.LCPI3_4(%rip), %xmm15  # xmm15 = mem[0],zero
	.p2align	4, 0x90
.LBB3_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_5 Depth 2
                                        #     Child Loop BB3_15 Depth 2
	movupd	(%rdi), %xmm9
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rax), %xmm2          # xmm2 = mem[0],zero
	subsd	%xmm9, %xmm0
	movapd	%xmm9, %xmm6
	movhlps	%xmm6, %xmm6            # xmm6 = xmm6[1,1]
	subsd	%xmm6, %xmm2
	movsd	16(%rax), %xmm4         # xmm4 = mem[0],zero
	movsd	16(%rdi), %xmm8         # xmm8 = mem[0],zero
	subsd	%xmm8, %xmm4
	movupd	24(%rdi), %xmm12
	movapd	%xmm0, %xmm3
	mulsd	%xmm12, %xmm3
	movapd	%xmm12, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	movapd	%xmm2, %xmm7
	mulsd	%xmm1, %xmm7
	addsd	%xmm3, %xmm7
	movsd	40(%rdi), %xmm10        # xmm10 = mem[0],zero
	movapd	%xmm4, %xmm3
	mulsd	%xmm10, %xmm3
	addsd	%xmm7, %xmm3
	movapd	%xmm3, %xmm7
	mulsd	%xmm7, %xmm7
	mulsd	%xmm0, %xmm0
	mulsd	%xmm2, %xmm2
	addsd	%xmm0, %xmm2
	mulsd	%xmm4, %xmm4
	addsd	%xmm2, %xmm4
	subsd	%xmm4, %xmm7
	movsd	24(%rax), %xmm4         # xmm4 = mem[0],zero
	mulsd	%xmm4, %xmm4
	addsd	%xmm7, %xmm4
	ucomisd	%xmm4, %xmm11
	movapd	%xmm5, %xmm0
	ja	.LBB3_9
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	ucomisd	%xmm5, %xmm4
	movapd	%xmm5, %xmm2
	jae	.LBB3_8
# BB#4:                                 # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	xorpd	%xmm2, %xmm2
	movl	$-101, %edx
	movsd	.LCPI3_1(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB3_5:                                # %.preheader.i.i
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%edx
	je	.LBB3_8
# BB#6:                                 #   in Loop: Header=BB3_5 Depth=2
	movapd	%xmm4, %xmm2
	divsd	%xmm0, %xmm2
	addsd	%xmm0, %xmm2
	mulsd	%xmm13, %xmm2
	movapd	%xmm2, %xmm7
	subsd	%xmm0, %xmm7
	ucomisd	%xmm14, %xmm7
	movapd	%xmm2, %xmm0
	ja	.LBB3_5
# BB#7:                                 #   in Loop: Header=BB3_5 Depth=2
	ucomisd	%xmm7, %xmm15
	movapd	%xmm2, %xmm0
	ja	.LBB3_5
	.p2align	4, 0x90
.LBB3_8:                                # %_ZL8LLVMsqrtd.exit.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movapd	%xmm3, %xmm0
	addsd	%xmm2, %xmm0
	subsd	%xmm2, %xmm3
	xorpd	%xmm2, %xmm2
	cmpltsd	%xmm3, %xmm2
	andpd	%xmm2, %xmm3
	andnpd	%xmm0, %xmm2
	orpd	%xmm3, %xmm2
	cmpltsd	%xmm11, %xmm0
	movapd	%xmm0, %xmm3
	andnpd	%xmm2, %xmm3
	andpd	%xmm5, %xmm0
	orpd	%xmm3, %xmm0
.LBB3_9:                                # %_ZNK8sphere_t9intersectERK5ray_t.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	movsd	24(%rsi), %xmm4         # xmm4 = mem[0],zero
	ucomisd	%xmm4, %xmm0
	jae	.LBB3_10
# BB#11:                                #   in Loop: Header=BB3_2 Depth=1
	movupd	32(%rax), %xmm3
	movapd	%xmm3, %xmm0
	subsd	%xmm9, %xmm0
	movapd	%xmm3, %xmm2
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	subsd	%xmm6, %xmm2
	movsd	48(%rax), %xmm5         # xmm5 = mem[0],zero
	movapd	%xmm5, %xmm7
	subsd	%xmm8, %xmm7
	movapd	%xmm0, %xmm6
	mulsd	%xmm12, %xmm6
	mulsd	%xmm2, %xmm1
	addsd	%xmm6, %xmm1
	movapd	%xmm7, %xmm11
	mulsd	%xmm10, %xmm11
	addsd	%xmm1, %xmm11
	movapd	%xmm11, %xmm6
	mulsd	%xmm6, %xmm6
	mulsd	%xmm0, %xmm0
	mulsd	%xmm2, %xmm2
	addsd	%xmm0, %xmm2
	mulsd	%xmm7, %xmm7
	addsd	%xmm2, %xmm7
	subsd	%xmm7, %xmm6
	movsd	56(%rax), %xmm0         # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	mulsd	%xmm1, %xmm1
	addsd	%xmm6, %xmm1
	xorpd	%xmm2, %xmm2
	ucomisd	%xmm1, %xmm2
	jbe	.LBB3_13
# BB#12:                                #   in Loop: Header=BB3_2 Depth=1
	movl	$1, %edx
	movsd	.LCPI3_0(%rip), %xmm5   # xmm5 = mem[0],zero
	xorpd	%xmm11, %xmm11
	jmp	.LBB3_21
	.p2align	4, 0x90
.LBB3_10:                               #   in Loop: Header=BB3_2 Depth=1
	movq	64(%rax), %rdx
	jmp	.LBB3_21
	.p2align	4, 0x90
.LBB3_13:                               #   in Loop: Header=BB3_2 Depth=1
	movsd	%xmm0, -16(%rsp)        # 8-byte Spill
	movsd	%xmm5, -8(%rsp)         # 8-byte Spill
	movsd	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	movapd	%xmm0, %xmm2
	jae	.LBB3_18
# BB#14:                                # %.preheader.i.i24.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	xorpd	%xmm2, %xmm2
	movl	$-101, %edx
	movsd	.LCPI3_1(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB3_15:                               # %.preheader.i.i24
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%edx
	je	.LBB3_18
# BB#16:                                #   in Loop: Header=BB3_15 Depth=2
	movapd	%xmm1, %xmm2
	divsd	%xmm0, %xmm2
	addsd	%xmm0, %xmm2
	mulsd	%xmm13, %xmm2
	movapd	%xmm2, %xmm7
	subsd	%xmm0, %xmm7
	ucomisd	%xmm14, %xmm7
	movapd	%xmm2, %xmm0
	ja	.LBB3_15
# BB#17:                                #   in Loop: Header=BB3_15 Depth=2
	ucomisd	%xmm7, %xmm15
	movapd	%xmm2, %xmm0
	ja	.LBB3_15
	.p2align	4, 0x90
.LBB3_18:                               # %_ZNK8sphere_t9intersectERK5ray_t.exit30
                                        #   in Loop: Header=BB3_2 Depth=1
	movapd	%xmm11, %xmm1
	addsd	%xmm2, %xmm1
	subsd	%xmm2, %xmm11
	xorpd	%xmm0, %xmm0
	cmpltsd	%xmm11, %xmm0
	andpd	%xmm0, %xmm11
	andnpd	%xmm1, %xmm0
	orpd	%xmm11, %xmm0
	xorpd	%xmm11, %xmm11
	cmpltsd	%xmm11, %xmm1
	movapd	%xmm1, %xmm2
	andnpd	%xmm0, %xmm2
	movsd	.LCPI3_0(%rip), %xmm5   # xmm5 = mem[0],zero
	andpd	%xmm5, %xmm1
	orpd	%xmm2, %xmm1
	ucomisd	%xmm1, %xmm4
	movsd	-8(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	jbe	.LBB3_20
# BB#19:                                #   in Loop: Header=BB3_2 Depth=1
	movsd	%xmm1, 24(%rsi)
	movapd	%xmm1, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	%xmm12, %xmm0
	mulsd	%xmm1, %xmm10
	addpd	%xmm9, %xmm0
	addsd	%xmm8, %xmm10
	subpd	%xmm3, %xmm0
	subsd	%xmm2, %xmm10
	movsd	.LCPI3_1(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	-16(%rsp), %xmm1        # 8-byte Folded Reload
	mulsd	%xmm1, %xmm10
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm0, %xmm1
	movupd	%xmm1, (%rsi)
	movsd	%xmm10, 16(%rsi)
.LBB3_20:                               # %.backedge
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	$1, %edx
.LBB3_21:                               # %.backedge
                                        #   in Loop: Header=BB3_2 Depth=1
	leaq	(%rdx,%rdx,8), %rdx
	leaq	(%rax,%rdx,8), %rax
	cmpq	%rcx, %rax
	jb	.LBB3_2
.LBB3_22:                               # %._crit_edge
	retq
.Lfunc_end3:
	.size	_ZN6node_t9intersectILb0EEEvRK5ray_tR5hit_t, .Lfunc_end3-_ZN6node_t9intersectILb0EEEvRK5ray_tR5hit_t
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4607182418800017408     # double 1
.LCPI4_1:
	.quad	4609355405620223673     # double 1.4825000000000002
.LCPI4_2:
	.quad	4602678819172646912     # double 0.5
.LCPI4_3:
	.quad	4427486594234968593     # double 9.9999999999999998E-13
.LCPI4_4:
	.quad	-4795885442619807215    # double -9.9999999999999998E-13
.LCPI4_6:
	.quad	4606281698874543309     # double 0.90000000000000002
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_5:
	.quad	-4620693217682128896    # double -0.5
	.quad	-4619342137793917747    # double -0.65000000000000002
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_sphereflake.ii,@function
_GLOBAL__sub_I_sphereflake.ii:          # @_GLOBAL__sub_I_sphereflake.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movsd	.LCPI4_0(%rip), %xmm5   # xmm5 = mem[0],zero
	xorpd	%xmm0, %xmm0
	movl	$-101, %eax
	movsd	.LCPI4_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI4_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI4_3(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI4_4(%rip), %xmm4   # xmm4 = mem[0],zero
	.p2align	4, 0x90
.LBB4_1:                                # %.preheader.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	je	.LBB4_4
# BB#2:                                 #   in Loop: Header=BB4_1 Depth=1
	movapd	%xmm1, %xmm0
	divsd	%xmm5, %xmm0
	addsd	%xmm5, %xmm0
	mulsd	%xmm2, %xmm0
	movapd	%xmm0, %xmm6
	subsd	%xmm5, %xmm6
	ucomisd	%xmm3, %xmm6
	movapd	%xmm0, %xmm5
	ja	.LBB4_1
# BB#3:                                 #   in Loop: Header=BB4_1 Depth=1
	ucomisd	%xmm6, %xmm4
	movapd	%xmm0, %xmm5
	ja	.LBB4_1
.LBB4_4:                                # %__cxx_global_var_init.1.exit
	movsd	.LCPI4_0(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	.LCPI4_5(%rip), %xmm0
	mulsd	.LCPI4_6(%rip), %xmm1
	movapd	%xmm0, _ZL5light(%rip)
	movsd	%xmm1, _ZL5light+16(%rip)
	popq	%rax
	retq
.Lfunc_end4:
	.size	_GLOBAL__sub_I_sphereflake.ii, .Lfunc_end4-_GLOBAL__sub_I_sphereflake.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	_ZL5light,@object       # @_ZL5light
	.local	_ZL5light
	.comm	_ZL5light,24,16
	.type	_ZL4pool,@object        # @_ZL4pool
	.local	_ZL4pool
	.comm	_ZL4pool,8,8
	.type	_ZL3end,@object         # @_ZL3end
	.local	_ZL3end
	.comm	_ZL3end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"P2\n"
	.size	.L.str, 4

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" "
	.size	.L.str.2, 2

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n256\n"
	.size	.L.str.3, 6

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_sphereflake.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
