	.text
	.file	"UpdateCallbackConsole.bc"
	.section	.text._ZN8NWindows16NSynchronization16CCriticalSectionD2Ev,"axG",@progbits,_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev,comdat
	.weak	_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev
	.p2align	4, 0x90
	.type	_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev,@function
_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev: # @_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev
	.cfi_startproc
# BB#0:
	jmp	pthread_mutex_destroy   # TAILCALL
.Lfunc_end0:
	.size	_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev, .Lfunc_end0-_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev
	.cfi_endproc

	.text
	.globl	_ZN22CUpdateCallbackConsole10OpenResultEPKwi
	.p2align	4, 0x90
	.type	_ZN22CUpdateCallbackConsole10OpenResultEPKwi,@function
_ZN22CUpdateCallbackConsole10OpenResultEPKwi: # @_ZN22CUpdateCallbackConsole10OpenResultEPKwi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	64(%rbx), %rdi
	movl	$_Z4endlR13CStdOutStream, %esi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
	testl	%ebp, %ebp
	je	.LBB1_2
# BB#1:
	movq	64(%rbx), %rdi
	movl	$.L.str, %esi
	callq	_ZN13CStdOutStreamlsEPKc
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	_ZN13CStdOutStreamlsEPKw
	movl	$.L.str.1, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.LBB1_2:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN22CUpdateCallbackConsole10OpenResultEPKwi, .Lfunc_end1-_ZN22CUpdateCallbackConsole10OpenResultEPKwi
	.cfi_endproc

	.globl	_ZN22CUpdateCallbackConsole13StartScanningEv
	.p2align	4, 0x90
	.type	_ZN22CUpdateCallbackConsole13StartScanningEv,@function
_ZN22CUpdateCallbackConsole13StartScanningEv: # @_ZN22CUpdateCallbackConsole13StartScanningEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 16
	movq	64(%rdi), %rdi
	movl	$.L.str.7, %esi
	callq	_ZN13CStdOutStreamlsEPKc
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end2:
	.size	_ZN22CUpdateCallbackConsole13StartScanningEv, .Lfunc_end2-_ZN22CUpdateCallbackConsole13StartScanningEv
	.cfi_endproc

	.globl	_ZN22CUpdateCallbackConsole12ScanProgressEyyPKw
	.p2align	4, 0x90
	.type	_ZN22CUpdateCallbackConsole12ScanProgressEyyPKw,@function
_ZN22CUpdateCallbackConsole12ScanProgressEyyPKw: # @_ZN22CUpdateCallbackConsole12ScanProgressEyyPKw
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	jmpq	*24(%rax)               # TAILCALL
.Lfunc_end3:
	.size	_ZN22CUpdateCallbackConsole12ScanProgressEyyPKw, .Lfunc_end3-_ZN22CUpdateCallbackConsole12ScanProgressEyyPKw
	.cfi_endproc

	.globl	_ZN22CUpdateCallbackConsole15CanNotFindErrorEPKwj
	.p2align	4, 0x90
	.type	_ZN22CUpdateCallbackConsole15CanNotFindErrorEPKwj,@function
_ZN22CUpdateCallbackConsole15CanNotFindErrorEPKwj: # @_ZN22CUpdateCallbackConsole15CanNotFindErrorEPKwj
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 96
.Lcfi14:
	.cfi_offset %rbx, -56
.Lcfi15:
	.cfi_offset %r12, -48
.Lcfi16:
	.cfi_offset %r13, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movl	%edx, 28(%rsp)          # 4-byte Spill
	movq	%rsi, %rbp
	movq	%rdi, %r15
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%rbp,%r14,4)
	leaq	1(%r14), %r14
	jne	.LBB4_1
# BB#2:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leaq	-1(%r14), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movslq	%r14d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %r13
	cmovnoq	%rax, %r13
	movq	%r13, %rdi
	callq	_Znam
	movq	%rax, %rbx
	movl	$0, (%rbx)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_3:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB4_3
# BB#4:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
.Ltmp0:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r12
.Ltmp1:
# BB#5:                                 # %.noexc
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r12)
.Ltmp2:
	movq	%r13, %rdi
	callq	_Znam
.Ltmp3:
# BB#6:                                 # %.noexc.i
	movq	%rax, (%r12)
	movl	$0, (%rax)
	movl	%r14d, 12(%r12)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_7:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB4_7
# BB#8:
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%eax, 8(%r12)
	leaq	168(%r15), %rdi
.Ltmp5:
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp6:
# BB#9:                                 # %_ZN11CStringBaseIwED2Ev.exit
	movq	184(%r15), %rax
	movslq	180(%r15), %rcx
	movq	%r12, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 180(%r15)
	movq	%rbx, %rdi
	callq	_ZdaPv
	leaq	200(%r15), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	216(%r15), %rax
	movslq	212(%r15), %rcx
	movl	28(%rsp), %r14d         # 4-byte Reload
	movl	%r14d, (%rax,%rcx,4)
	incl	212(%r15)
	cmpb	$0, 58(%r15)
	je	.LBB4_11
# BB#10:                                # %_ZN11CStringBaseIwED2Ev.exit._crit_edge
	addq	$8, %r15
	jmp	.LBB4_12
.LBB4_11:
	movq	64(%r15), %rdi
	movl	$_Z4endlR13CStdOutStream, %esi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
	leaq	8(%r15), %rbx
	movq	%rbx, %rdi
	callq	_ZN15CPercentPrinter12PrintNewLineEv
	movb	$1, 58(%r15)
	movq	%rbx, %r15
.LBB4_12:
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	_ZN15CPercentPrinter11PrintStringEPKw
	movl	$.L.str.2, %esi
	movq	%r15, %rdi
	callq	_ZN15CPercentPrinter11PrintStringEPKc
	movq	$0, 16(%rsp)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, 8(%rsp)
	movl	$0, (%rax)
	movl	$4, 20(%rsp)
.Ltmp8:
	leaq	8(%rsp), %rsi
	movl	%r14d, %edi
	callq	_ZN8NWindows6NError15MyFormatMessageEjR11CStringBaseIwE
.Ltmp9:
# BB#13:                                # %_ZN8NWindows6NError16MyFormatMessageWEj.exit
	movq	8(%rsp), %rsi
.Ltmp11:
	movq	%r15, %rdi
	callq	_ZN15CPercentPrinter11PrintStringEPKw
.Ltmp12:
# BB#14:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_16
# BB#15:
	callq	_ZdaPv
.LBB4_16:                               # %_ZN11CStringBaseIwED2Ev.exit9
	movq	%r15, %rdi
	callq	_ZN15CPercentPrinter12PrintNewLineEv
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_17:
.Ltmp13:
	jmp	.LBB4_19
.LBB4_18:
.Ltmp10:
.LBB4_19:
	movq	%rax, %rbp
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB4_23
	jmp	.LBB4_24
.LBB4_20:
.Ltmp4:
	movq	%rax, %rbp
	movq	%r12, %rdi
	callq	_ZdlPv
	jmp	.LBB4_22
.LBB4_21:
.Ltmp7:
	movq	%rax, %rbp
.LBB4_22:
	movq	%rbx, %rdi
.LBB4_23:
	callq	_ZdaPv
.LBB4_24:                               # %unwind_resume
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN22CUpdateCallbackConsole15CanNotFindErrorEPKwj, .Lfunc_end4-_ZN22CUpdateCallbackConsole15CanNotFindErrorEPKwj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\352\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp2-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp2           #   Call between .Ltmp2 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp8-.Ltmp6           #   Call between .Ltmp6 and .Ltmp8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin0   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Lfunc_end4-.Ltmp12     #   Call between .Ltmp12 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN22CUpdateCallbackConsole14FinishScanningEv
	.p2align	4, 0x90
	.type	_ZN22CUpdateCallbackConsole14FinishScanningEv,@function
_ZN22CUpdateCallbackConsole14FinishScanningEv: # @_ZN22CUpdateCallbackConsole14FinishScanningEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 16
	movq	64(%rdi), %rdi
	movl	$_Z4endlR13CStdOutStream, %esi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end5:
	.size	_ZN22CUpdateCallbackConsole14FinishScanningEv, .Lfunc_end5-_ZN22CUpdateCallbackConsole14FinishScanningEv
	.cfi_endproc

	.globl	_ZN22CUpdateCallbackConsole12StartArchiveEPKwb
	.p2align	4, 0x90
	.type	_ZN22CUpdateCallbackConsole12StartArchiveEPKwb,@function
_ZN22CUpdateCallbackConsole12StartArchiveEPKwb: # @_ZN22CUpdateCallbackConsole12StartArchiveEPKwb
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -24
.Lcfi25:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	64(%rbx), %rdi
	testb	%dl, %dl
	je	.LBB6_2
# BB#1:
	movl	$.L.str.8, %esi
	jmp	.LBB6_3
.LBB6_2:
	movl	$.L.str.9, %esi
.LBB6_3:
	callq	_ZN13CStdOutStreamlsEPKc
	testq	%r14, %r14
	movq	64(%rbx), %rdi
	je	.LBB6_5
# BB#4:
	movq	%r14, %rsi
	callq	_ZN13CStdOutStreamlsEPKw
	jmp	.LBB6_6
.LBB6_5:
	movl	$.L.str.3, %esi
	callq	_ZN13CStdOutStreamlsEPKc
.LBB6_6:
	movq	64(%rbx), %rdi
	movl	$_Z4endlR13CStdOutStream, %esi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	_ZN22CUpdateCallbackConsole12StartArchiveEPKwb, .Lfunc_end6-_ZN22CUpdateCallbackConsole12StartArchiveEPKwb
	.cfi_endproc

	.globl	_ZN22CUpdateCallbackConsole13FinishArchiveEv
	.p2align	4, 0x90
	.type	_ZN22CUpdateCallbackConsole13FinishArchiveEv,@function
_ZN22CUpdateCallbackConsole13FinishArchiveEv: # @_ZN22CUpdateCallbackConsole13FinishArchiveEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 16
	movq	64(%rdi), %rdi
	movl	$_Z4endlR13CStdOutStream, %esi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end7:
	.size	_ZN22CUpdateCallbackConsole13FinishArchiveEv, .Lfunc_end7-_ZN22CUpdateCallbackConsole13FinishArchiveEv
	.cfi_endproc

	.globl	_ZN22CUpdateCallbackConsole10CheckBreakEv
	.p2align	4, 0x90
	.type	_ZN22CUpdateCallbackConsole10CheckBreakEv,@function
_ZN22CUpdateCallbackConsole10CheckBreakEv: # @_ZN22CUpdateCallbackConsole10CheckBreakEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 16
	callq	_ZN13NConsoleClose15TestBreakSignalEv
	xorl	%ecx, %ecx
	testb	%al, %al
	movl	$-2147467260, %eax      # imm = 0x80004004
	cmovel	%ecx, %eax
	popq	%rcx
	retq
.Lfunc_end8:
	.size	_ZN22CUpdateCallbackConsole10CheckBreakEv, .Lfunc_end8-_ZN22CUpdateCallbackConsole10CheckBreakEv
	.cfi_endproc

	.globl	_ZN22CUpdateCallbackConsole8FinilizeEv
	.p2align	4, 0x90
	.type	_ZN22CUpdateCallbackConsole8FinilizeEv,@function
_ZN22CUpdateCallbackConsole8FinilizeEv: # @_ZN22CUpdateCallbackConsole8FinilizeEv
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 16
.Lcfi29:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$_ZL17g_CriticalSection, %edi
	callq	pthread_mutex_lock
	cmpb	$0, 56(%rbx)
	je	.LBB9_8
# BB#1:
	cmpb	$0, 72(%rbx)
	je	.LBB9_3
# BB#2:
	leaq	8(%rbx), %rdi
.Ltmp14:
	callq	_ZN15CPercentPrinter10ClosePrintEv
.Ltmp15:
.LBB9_3:
	cmpb	$0, 73(%rbx)
	jne	.LBB9_7
# BB#4:
	cmpb	$0, 57(%rbx)
	je	.LBB9_7
# BB#5:
	leaq	8(%rbx), %rdi
.Ltmp16:
	callq	_ZN15CPercentPrinter12PrintNewLineEv
.Ltmp17:
# BB#6:
	movb	$0, 57(%rbx)
.LBB9_7:
	movb	$0, 56(%rbx)
.LBB9_8:
	movl	$_ZL17g_CriticalSection, %edi
	callq	pthread_mutex_unlock
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB9_9:
.Ltmp18:
	movq	%rax, %rbx
	movl	$_ZL17g_CriticalSection, %edi
	callq	pthread_mutex_unlock
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end9:
	.size	_ZN22CUpdateCallbackConsole8FinilizeEv, .Lfunc_end9-_ZN22CUpdateCallbackConsole8FinilizeEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp17-.Ltmp14         #   Call between .Ltmp14 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin1   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end9-.Ltmp17     #   Call between .Ltmp17 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN22CUpdateCallbackConsole11SetNumFilesEy
	.p2align	4, 0x90
	.type	_ZN22CUpdateCallbackConsole11SetNumFilesEy,@function
_ZN22CUpdateCallbackConsole11SetNumFilesEy: # @_ZN22CUpdateCallbackConsole11SetNumFilesEy
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end10:
	.size	_ZN22CUpdateCallbackConsole11SetNumFilesEy, .Lfunc_end10-_ZN22CUpdateCallbackConsole11SetNumFilesEy
	.cfi_endproc

	.globl	_ZN22CUpdateCallbackConsole8SetTotalEy
	.p2align	4, 0x90
	.type	_ZN22CUpdateCallbackConsole8SetTotalEy,@function
_ZN22CUpdateCallbackConsole8SetTotalEy: # @_ZN22CUpdateCallbackConsole8SetTotalEy
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -24
.Lcfi34:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$_ZL17g_CriticalSection, %edi
	callq	pthread_mutex_lock
	cmpb	$0, 72(%rbx)
	je	.LBB11_2
# BB#1:
	movq	%r14, 32(%rbx)
	movq	$0, 16(%rbx)
.LBB11_2:
	movl	$_ZL17g_CriticalSection, %edi
	callq	pthread_mutex_unlock
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end11:
	.size	_ZN22CUpdateCallbackConsole8SetTotalEy, .Lfunc_end11-_ZN22CUpdateCallbackConsole8SetTotalEy
	.cfi_endproc

	.globl	_ZN22CUpdateCallbackConsole12SetCompletedEPKy
	.p2align	4, 0x90
	.type	_ZN22CUpdateCallbackConsole12SetCompletedEPKy,@function
_ZN22CUpdateCallbackConsole12SetCompletedEPKy: # @_ZN22CUpdateCallbackConsole12SetCompletedEPKy
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	$_ZL17g_CriticalSection, %edi
	callq	pthread_mutex_lock
	testq	%rbx, %rbx
	je	.LBB12_4
# BB#1:
	cmpb	$0, 72(%r14)
	je	.LBB12_4
# BB#2:
	leaq	8(%r14), %rdi
	movq	(%rbx), %rax
	movq	%rax, 24(%r14)
.Ltmp19:
	callq	_ZN15CPercentPrinter10PrintRatioEv
.Ltmp20:
# BB#3:
	movb	$1, 56(%r14)
.LBB12_4:
.Ltmp21:
	callq	_ZN13NConsoleClose15TestBreakSignalEv
.Ltmp22:
# BB#5:
	xorl	%ecx, %ecx
	testb	%al, %al
	movl	$-2147467260, %ebx      # imm = 0x80004004
	cmovel	%ecx, %ebx
	movl	$_ZL17g_CriticalSection, %edi
	callq	pthread_mutex_unlock
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB12_6:
.Ltmp23:
	movq	%rax, %rbx
	movl	$_ZL17g_CriticalSection, %edi
	callq	pthread_mutex_unlock
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZN22CUpdateCallbackConsole12SetCompletedEPKy, .Lfunc_end12-_ZN22CUpdateCallbackConsole12SetCompletedEPKy
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp19-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp22-.Ltmp19         #   Call between .Ltmp19 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin2   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end12-.Ltmp22    #   Call between .Ltmp22 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN22CUpdateCallbackConsole12SetRatioInfoEPKyS1_
	.p2align	4, 0x90
	.type	_ZN22CUpdateCallbackConsole12SetRatioInfoEPKyS1_,@function
_ZN22CUpdateCallbackConsole12SetRatioInfoEPKyS1_: # @_ZN22CUpdateCallbackConsole12SetRatioInfoEPKyS1_
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi40:
	.cfi_def_cfa_offset 16
	callq	_ZN13NConsoleClose15TestBreakSignalEv
	xorl	%ecx, %ecx
	testb	%al, %al
	movl	$-2147467260, %eax      # imm = 0x80004004
	cmovel	%ecx, %eax
	popq	%rcx
	retq
.Lfunc_end13:
	.size	_ZN22CUpdateCallbackConsole12SetRatioInfoEPKyS1_, .Lfunc_end13-_ZN22CUpdateCallbackConsole12SetRatioInfoEPKyS1_
	.cfi_endproc

	.globl	_ZN22CUpdateCallbackConsole9GetStreamEPKwb
	.p2align	4, 0x90
	.type	_ZN22CUpdateCallbackConsole9GetStreamEPKwb,@function
_ZN22CUpdateCallbackConsole9GetStreamEPKwb: # @_ZN22CUpdateCallbackConsole9GetStreamEPKwb
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi45:
	.cfi_def_cfa_offset 48
.Lcfi46:
	.cfi_offset %rbx, -40
.Lcfi47:
	.cfi_offset %r14, -32
.Lcfi48:
	.cfi_offset %r15, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$_ZL17g_CriticalSection, %edi
	callq	pthread_mutex_lock
	cmpb	$0, 73(%rbx)
	jne	.LBB14_7
# BB#1:
	leaq	8(%rbx), %r14
	testb	%bpl, %bpl
	je	.LBB14_3
# BB#2:
.Ltmp26:
	movl	$.L.str.4, %esi
	movq	%r14, %rdi
	callq	_ZN15CPercentPrinter11PrintStringEPKc
.Ltmp27:
	jmp	.LBB14_4
.LBB14_3:
.Ltmp24:
	movl	$.L.str.5, %esi
	movq	%r14, %rdi
	callq	_ZN15CPercentPrinter11PrintStringEPKc
.Ltmp25:
.LBB14_4:
	cmpl	$0, (%r15)
	movl	$.L.str.10, %esi
	cmovneq	%r15, %rsi
.Ltmp28:
	movq	%r14, %rdi
	callq	_ZN15CPercentPrinter11PrintStringEPKw
.Ltmp29:
# BB#5:
	cmpb	$0, 72(%rbx)
	je	.LBB14_7
# BB#6:
.Ltmp30:
	movq	%r14, %rdi
	callq	_ZN15CPercentPrinter12RePrintRatioEv
.Ltmp31:
.LBB14_7:
	movl	$_ZL17g_CriticalSection, %edi
	callq	pthread_mutex_unlock
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_8:
.Ltmp32:
	movq	%rax, %rbx
	movl	$_ZL17g_CriticalSection, %edi
	callq	pthread_mutex_unlock
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end14:
	.size	_ZN22CUpdateCallbackConsole9GetStreamEPKwb, .Lfunc_end14-_ZN22CUpdateCallbackConsole9GetStreamEPKwb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp26-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp31-.Ltmp26         #   Call between .Ltmp26 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin3   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Lfunc_end14-.Ltmp31    #   Call between .Ltmp31 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN22CUpdateCallbackConsole13OpenFileErrorEPKwj
	.p2align	4, 0x90
	.type	_ZN22CUpdateCallbackConsole13OpenFileErrorEPKwj,@function
_ZN22CUpdateCallbackConsole13OpenFileErrorEPKwj: # @_ZN22CUpdateCallbackConsole13OpenFileErrorEPKwj
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi54:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi56:
	.cfi_def_cfa_offset 80
.Lcfi57:
	.cfi_offset %rbx, -56
.Lcfi58:
	.cfi_offset %r12, -48
.Lcfi59:
	.cfi_offset %r13, -40
.Lcfi60:
	.cfi_offset %r14, -32
.Lcfi61:
	.cfi_offset %r15, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, %r13
	movq	%rdi, %r15
	movl	$_ZL17g_CriticalSection, %edi
	callq	pthread_mutex_lock
	leaq	136(%r15), %rdi
.Ltmp33:
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp34:
# BB#1:
	movq	152(%r15), %rax
	movslq	148(%r15), %rcx
	movl	%ebx, 20(%rsp)          # 4-byte Spill
	movl	%ebx, (%rax,%rcx,4)
	incl	148(%r15)
	movl	$-1, %ebx
	movq	%r13, %rax
	.p2align	4, 0x90
.LBB15_2:                               # =>This Inner Loop Header: Depth=1
	incl	%ebx
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB15_2
# BB#3:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%rbx), %r14d
	movslq	%r14d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %r12
	cmovnoq	%rax, %r12
.Ltmp35:
	movq	%r12, %rdi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp36:
# BB#4:                                 # %.noexc
	movl	$0, (%rbp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB15_5:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB15_5
# BB#6:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
.Ltmp38:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp39:
# BB#7:                                 # %.noexc7
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
.Ltmp40:
	movq	%r12, %rdi
	callq	_Znam
.Ltmp41:
# BB#8:                                 # %.noexc.i
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%r14d, 12(%r13)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB15_9:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB15_9
# BB#10:
	movl	%ebx, 8(%r13)
	leaq	104(%r15), %rdi
.Ltmp43:
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp44:
# BB#11:                                # %_ZN11CStringBaseIwED2Ev.exit
	movq	120(%r15), %rax
	movslq	116(%r15), %rcx
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 116(%r15)
	movq	%rbp, %rdi
	callq	_ZdaPv
	addq	$8, %r15
.Ltmp46:
	movq	%r15, %rdi
	callq	_ZN15CPercentPrinter10ClosePrintEv
.Ltmp47:
	movl	20(%rsp), %ebx          # 4-byte Reload
# BB#12:
.Ltmp48:
	movq	%r15, %rdi
	callq	_ZN15CPercentPrinter12PrintNewLineEv
.Ltmp49:
# BB#13:
.Ltmp50:
	movl	$.L.str.6, %esi
	movq	%r15, %rdi
	callq	_ZN15CPercentPrinter11PrintStringEPKc
.Ltmp51:
# BB#14:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
.Ltmp52:
	movl	$16, %edi
	callq	_Znam
.Ltmp53:
# BB#15:                                # %.noexc9
	movq	%rax, (%rsp)
	movl	$0, (%rax)
	movl	$4, 12(%rsp)
.Ltmp55:
	movq	%rsp, %rsi
	movl	%ebx, %edi
	callq	_ZN8NWindows6NError15MyFormatMessageEjR11CStringBaseIwE
.Ltmp56:
# BB#16:                                # %_ZN8NWindows6NError16MyFormatMessageWEj.exit
	movq	(%rsp), %rsi
.Ltmp58:
	movq	%r15, %rdi
	callq	_ZN15CPercentPrinter11PrintStringEPKw
.Ltmp59:
# BB#17:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_19
# BB#18:
	callq	_ZdaPv
.LBB15_19:                              # %_ZN11CStringBaseIwED2Ev.exit12
	movl	$_ZL17g_CriticalSection, %edi
	callq	pthread_mutex_unlock
	movl	$1, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_20:
.Ltmp60:
	jmp	.LBB15_22
.LBB15_21:
.Ltmp57:
.LBB15_22:
	movq	%rax, %r14
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB15_27
	jmp	.LBB15_30
.LBB15_23:
.Ltmp42:
	movq	%rax, %r14
	movq	%r13, %rdi
	callq	_ZdlPv
	jmp	.LBB15_26
.LBB15_24:
.Ltmp37:
	jmp	.LBB15_29
.LBB15_25:
.Ltmp45:
	movq	%rax, %r14
.LBB15_26:
	movq	%rbp, %rdi
.LBB15_27:
	callq	_ZdaPv
	jmp	.LBB15_30
.LBB15_28:
.Ltmp54:
.LBB15_29:
	movq	%rax, %r14
.LBB15_30:
	movl	$_ZL17g_CriticalSection, %edi
	callq	pthread_mutex_unlock
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end15:
	.size	_ZN22CUpdateCallbackConsole13OpenFileErrorEPKwj, .Lfunc_end15-_ZN22CUpdateCallbackConsole13OpenFileErrorEPKwj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\367\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Ltmp33-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp54-.Lfunc_begin4   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin4   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp45-.Lfunc_begin4   #     jumps to .Ltmp45
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin4   #     jumps to .Ltmp42
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin4   #     jumps to .Ltmp45
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin4   # >> Call Site 6 <<
	.long	.Ltmp53-.Ltmp46         #   Call between .Ltmp46 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin4   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin4   # >> Call Site 7 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin4   #     jumps to .Ltmp57
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin4   # >> Call Site 8 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin4   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin4   # >> Call Site 9 <<
	.long	.Lfunc_end15-.Ltmp59    #   Call between .Ltmp59 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN22CUpdateCallbackConsole18SetOperationResultEi
	.p2align	4, 0x90
	.type	_ZN22CUpdateCallbackConsole18SetOperationResultEi,@function
_ZN22CUpdateCallbackConsole18SetOperationResultEi: # @_ZN22CUpdateCallbackConsole18SetOperationResultEi
	.cfi_startproc
# BB#0:
	movw	$257, 56(%rdi)          # imm = 0x101
	xorl	%eax, %eax
	retq
.Lfunc_end16:
	.size	_ZN22CUpdateCallbackConsole18SetOperationResultEi, .Lfunc_end16-_ZN22CUpdateCallbackConsole18SetOperationResultEi
	.cfi_endproc

	.globl	_ZN22CUpdateCallbackConsole22CryptoGetTextPassword2EPiPPw
	.p2align	4, 0x90
	.type	_ZN22CUpdateCallbackConsole22CryptoGetTextPassword2EPiPPw,@function
_ZN22CUpdateCallbackConsole22CryptoGetTextPassword2EPiPPw: # @_ZN22CUpdateCallbackConsole22CryptoGetTextPassword2EPiPPw
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi66:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi67:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi69:
	.cfi_def_cfa_offset 80
.Lcfi70:
	.cfi_offset %rbx, -56
.Lcfi71:
	.cfi_offset %r12, -48
.Lcfi72:
	.cfi_offset %r13, -40
.Lcfi73:
	.cfi_offset %r14, -32
.Lcfi74:
	.cfi_offset %r15, -24
.Lcfi75:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	movq	$0, (%r14)
	movl	$1, %eax
	cmpb	$0, 74(%r12)
	jne	.LBB17_17
# BB#1:
	cmpb	$0, 96(%r12)
	je	.LBB17_2
# BB#3:
	movq	64(%r12), %rsi
	leaq	8(%rsp), %rbx
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	_Z11GetPasswordP13CStdOutStreamb
	leaq	80(%r12), %rax
	cmpq	%rax, %rbx
	je	.LBB17_4
# BB#5:
	movl	$0, 88(%r12)
	movq	80(%r12), %rbx
	movl	$0, (%rbx)
	movslq	16(%rsp), %r13
	incq	%r13
	movl	92(%r12), %ebp
	cmpl	%ebp, %r13d
	je	.LBB17_11
# BB#6:
	movl	$4, %ecx
	movq	%r13, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp61:
	callq	_Znam
.Ltmp62:
# BB#7:                                 # %.noexc
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB17_10
# BB#8:                                 # %.noexc
	testl	%ebp, %ebp
	jle	.LBB17_10
# BB#9:                                 # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	%rbx, %rax
	movslq	88(%r12), %rcx
.LBB17_10:                              # %._crit_edge16.i.i
	movq	%rax, 80(%r12)
	movl	$0, (%rax,%rcx,4)
	movl	%r13d, 92(%r12)
	movq	%rax, %rbx
.LBB17_11:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB17_12:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB17_12
# BB#13:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	16(%rsp), %eax
	movl	%eax, 88(%r12)
	testq	%rdi, %rdi
	jne	.LBB17_15
	jmp	.LBB17_16
.LBB17_2:
	xorl	%eax, %eax
	jmp	.LBB17_17
.LBB17_4:                               # %._ZN11CStringBaseIwEaSERKS0_.exit_crit_edge
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_16
.LBB17_15:
	callq	_ZdaPv
.LBB17_16:                              # %_ZN11CStringBaseIwED2Ev.exit
	movb	$1, 74(%r12)
	movl	$1, %eax
.LBB17_17:
	movl	%eax, (%r15)
	movq	80(%r12), %rdi
	callq	SysAllocString
	movq	%rax, (%r14)
	xorl	%ecx, %ecx
	testq	%rax, %rax
	movl	$-2147024882, %eax      # imm = 0x8007000E
	cmovnel	%ecx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_18:
.Ltmp63:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_20
# BB#19:
	callq	_ZdaPv
.LBB17_20:                              # %_ZN11CStringBaseIwED2Ev.exit5
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end17:
	.size	_ZN22CUpdateCallbackConsole22CryptoGetTextPassword2EPiPPw, .Lfunc_end17-_ZN22CUpdateCallbackConsole22CryptoGetTextPassword2EPiPPw
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp61-.Lfunc_begin5   #   Call between .Lfunc_begin5 and .Ltmp61
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp61-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin5   #     jumps to .Ltmp63
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Lfunc_end17-.Ltmp62    #   Call between .Ltmp62 and .Lfunc_end17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN22CUpdateCallbackConsole21CryptoGetTextPasswordEPPw
	.p2align	4, 0x90
	.type	_ZN22CUpdateCallbackConsole21CryptoGetTextPasswordEPPw,@function
_ZN22CUpdateCallbackConsole21CryptoGetTextPasswordEPPw: # @_ZN22CUpdateCallbackConsole21CryptoGetTextPasswordEPPw
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi78:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi79:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi80:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi82:
	.cfi_def_cfa_offset 80
.Lcfi83:
	.cfi_offset %rbx, -56
.Lcfi84:
	.cfi_offset %r12, -48
.Lcfi85:
	.cfi_offset %r13, -40
.Lcfi86:
	.cfi_offset %r14, -32
.Lcfi87:
	.cfi_offset %r15, -24
.Lcfi88:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	$0, (%r14)
	cmpb	$0, 74(%r15)
	jne	.LBB18_15
# BB#1:
	movq	64(%r15), %rsi
	leaq	8(%rsp), %rbx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	_Z11GetPasswordP13CStdOutStreamb
	leaq	80(%r15), %rax
	cmpq	%rax, %rbx
	je	.LBB18_2
# BB#3:
	movl	$0, 88(%r15)
	movq	80(%r15), %rbx
	movl	$0, (%rbx)
	movslq	16(%rsp), %r12
	incq	%r12
	movl	92(%r15), %ebp
	cmpl	%ebp, %r12d
	je	.LBB18_9
# BB#4:
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp64:
	callq	_Znam
	movq	%rax, %r13
.Ltmp65:
# BB#5:                                 # %.noexc
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB18_8
# BB#6:                                 # %.noexc
	testl	%ebp, %ebp
	jle	.LBB18_8
# BB#7:                                 # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	88(%r15), %rax
.LBB18_8:                               # %._crit_edge16.i.i
	movq	%r13, 80(%r15)
	movl	$0, (%r13,%rax,4)
	movl	%r12d, 92(%r15)
	movq	%r13, %rbx
.LBB18_9:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB18_10:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB18_10
# BB#11:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	16(%rsp), %eax
	movl	%eax, 88(%r15)
	testq	%rdi, %rdi
	jne	.LBB18_13
	jmp	.LBB18_14
.LBB18_2:                               # %._ZN11CStringBaseIwEaSERKS0_.exit_crit_edge
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_14
.LBB18_13:
	callq	_ZdaPv
.LBB18_14:                              # %_ZN11CStringBaseIwED2Ev.exit
	movb	$1, 74(%r15)
.LBB18_15:
	movq	80(%r15), %rdi
	callq	SysAllocString
	movq	%rax, (%r14)
	xorl	%ecx, %ecx
	testq	%rax, %rax
	movl	$-2147024882, %eax      # imm = 0x8007000E
	cmovnel	%ecx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB18_16:
.Ltmp66:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_18
# BB#17:
	callq	_ZdaPv
.LBB18_18:                              # %_ZN11CStringBaseIwED2Ev.exit4
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end18:
	.size	_ZN22CUpdateCallbackConsole21CryptoGetTextPasswordEPPw, .Lfunc_end18-_ZN22CUpdateCallbackConsole21CryptoGetTextPasswordEPPw
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp64-.Lfunc_begin6   #   Call between .Lfunc_begin6 and .Ltmp64
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp64-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Ltmp65-.Ltmp64         #   Call between .Ltmp64 and .Ltmp65
	.long	.Ltmp66-.Lfunc_begin6   #     jumps to .Ltmp66
	.byte	0                       #   On action: cleanup
	.long	.Ltmp65-.Lfunc_begin6   # >> Call Site 3 <<
	.long	.Lfunc_end18-.Ltmp65    #   Call between .Ltmp65 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_UpdateCallbackConsole.ii,@function
_GLOBAL__sub_I_UpdateCallbackConsole.ii: # @_GLOBAL__sub_I_UpdateCallbackConsole.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi89:
	.cfi_def_cfa_offset 16
	movl	$_ZL17g_CriticalSection, %edi
	callq	CriticalSection_Init
	movl	$_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev, %edi
	movl	$_ZL17g_CriticalSection, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end19:
	.size	_GLOBAL__sub_I_UpdateCallbackConsole.ii, .Lfunc_end19-_GLOBAL__sub_I_UpdateCallbackConsole.ii
	.cfi_endproc

	.type	_ZL17g_CriticalSection,@object # @_ZL17g_CriticalSection
	.local	_ZL17g_CriticalSection
	.comm	_ZL17g_CriticalSection,40,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Error: "
	.size	.L.str, 8

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	" is not supported archive"
	.size	.L.str.1, 26

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	":  WARNING: "
	.size	.L.str.2, 13

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"StdOut"
	.size	.L.str.3, 7

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Anti item    "
	.size	.L.str.4, 14

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Compressing  "
	.size	.L.str.5, 14

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"WARNING: "
	.size	.L.str.6, 10

	.type	_ZTV22CUpdateCallbackConsole,@object # @_ZTV22CUpdateCallbackConsole
	.section	.rodata,"a",@progbits
	.globl	_ZTV22CUpdateCallbackConsole
	.p2align	3
_ZTV22CUpdateCallbackConsole:
	.quad	0
	.quad	_ZTI22CUpdateCallbackConsole
	.quad	_ZN22CUpdateCallbackConsole8SetTotalEy
	.quad	_ZN22CUpdateCallbackConsole12SetCompletedEPKy
	.quad	_ZN22CUpdateCallbackConsole12SetRatioInfoEPKyS1_
	.quad	_ZN22CUpdateCallbackConsole10CheckBreakEv
	.quad	_ZN22CUpdateCallbackConsole8FinilizeEv
	.quad	_ZN22CUpdateCallbackConsole11SetNumFilesEy
	.quad	_ZN22CUpdateCallbackConsole9GetStreamEPKwb
	.quad	_ZN22CUpdateCallbackConsole13OpenFileErrorEPKwj
	.quad	_ZN22CUpdateCallbackConsole18SetOperationResultEi
	.quad	_ZN22CUpdateCallbackConsole22CryptoGetTextPassword2EPiPPw
	.quad	_ZN22CUpdateCallbackConsole21CryptoGetTextPasswordEPPw
	.quad	_ZN22CUpdateCallbackConsole10OpenResultEPKwi
	.quad	_ZN22CUpdateCallbackConsole13StartScanningEv
	.quad	_ZN22CUpdateCallbackConsole12ScanProgressEyyPKw
	.quad	_ZN22CUpdateCallbackConsole15CanNotFindErrorEPKwj
	.quad	_ZN22CUpdateCallbackConsole14FinishScanningEv
	.quad	_ZN22CUpdateCallbackConsole12StartArchiveEPKwb
	.quad	_ZN22CUpdateCallbackConsole13FinishArchiveEv
	.size	_ZTV22CUpdateCallbackConsole, 160

	.type	_ZTS22CUpdateCallbackConsole,@object # @_ZTS22CUpdateCallbackConsole
	.globl	_ZTS22CUpdateCallbackConsole
	.p2align	4
_ZTS22CUpdateCallbackConsole:
	.asciz	"22CUpdateCallbackConsole"
	.size	_ZTS22CUpdateCallbackConsole, 25

	.type	_ZTS18IUpdateCallbackUI2,@object # @_ZTS18IUpdateCallbackUI2
	.section	.rodata._ZTS18IUpdateCallbackUI2,"aG",@progbits,_ZTS18IUpdateCallbackUI2,comdat
	.weak	_ZTS18IUpdateCallbackUI2
	.p2align	4
_ZTS18IUpdateCallbackUI2:
	.asciz	"18IUpdateCallbackUI2"
	.size	_ZTS18IUpdateCallbackUI2, 21

	.type	_ZTS17IUpdateCallbackUI,@object # @_ZTS17IUpdateCallbackUI
	.section	.rodata._ZTS17IUpdateCallbackUI,"aG",@progbits,_ZTS17IUpdateCallbackUI,comdat
	.weak	_ZTS17IUpdateCallbackUI
	.p2align	4
_ZTS17IUpdateCallbackUI:
	.asciz	"17IUpdateCallbackUI"
	.size	_ZTS17IUpdateCallbackUI, 20

	.type	_ZTI17IUpdateCallbackUI,@object # @_ZTI17IUpdateCallbackUI
	.section	.rodata._ZTI17IUpdateCallbackUI,"aG",@progbits,_ZTI17IUpdateCallbackUI,comdat
	.weak	_ZTI17IUpdateCallbackUI
	.p2align	3
_ZTI17IUpdateCallbackUI:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS17IUpdateCallbackUI
	.size	_ZTI17IUpdateCallbackUI, 16

	.type	_ZTI18IUpdateCallbackUI2,@object # @_ZTI18IUpdateCallbackUI2
	.section	.rodata._ZTI18IUpdateCallbackUI2,"aG",@progbits,_ZTI18IUpdateCallbackUI2,comdat
	.weak	_ZTI18IUpdateCallbackUI2
	.p2align	4
_ZTI18IUpdateCallbackUI2:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18IUpdateCallbackUI2
	.quad	_ZTI17IUpdateCallbackUI
	.size	_ZTI18IUpdateCallbackUI2, 24

	.type	_ZTI22CUpdateCallbackConsole,@object # @_ZTI22CUpdateCallbackConsole
	.section	.rodata,"a",@progbits
	.globl	_ZTI22CUpdateCallbackConsole
	.p2align	4
_ZTI22CUpdateCallbackConsole:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS22CUpdateCallbackConsole
	.quad	_ZTI18IUpdateCallbackUI2
	.size	_ZTI22CUpdateCallbackConsole, 24

	.type	.L.str.7,@object        # @.str.7
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.7:
	.asciz	"Scanning"
	.size	.L.str.7, 9

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Updating archive "
	.size	.L.str.8, 18

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Creating archive "
	.size	.L.str.9, 18

	.type	.L.str.10,@object       # @.str.10
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.10:
	.long	91                      # 0x5b
	.long	67                      # 0x43
	.long	111                     # 0x6f
	.long	110                     # 0x6e
	.long	116                     # 0x74
	.long	101                     # 0x65
	.long	110                     # 0x6e
	.long	116                     # 0x74
	.long	93                      # 0x5d
	.long	0                       # 0x0
	.size	.L.str.10, 40

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_UpdateCallbackConsole.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
