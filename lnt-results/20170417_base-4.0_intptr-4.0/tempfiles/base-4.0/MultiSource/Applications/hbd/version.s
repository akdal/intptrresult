	.text
	.file	"version.bc"
	.globl	_ZN12ClassVersion4readEP9Classfile
	.p2align	4, 0x90
	.type	_ZN12ClassVersion4readEP9Classfile,@function
_ZN12ClassVersion4readEP9Classfile:     # @_ZN12ClassVersion4readEP9Classfile
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	(%r15), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movw	%ax, (%r14)
	movq	(%r15), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movw	%ax, 2(%r14)
	movzwl	%ax, %eax
	cmpl	$45, %eax
	jne	.LBB0_3
# BB#1:
	movzwl	(%r14), %edx
	cmpl	$3, %edx
	jne	.LBB0_4
# BB#2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_3:
	movl	$5, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_Z10fatalerroriz        # TAILCALL
.LBB0_4:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fprintf                 # TAILCALL
.Lfunc_end0:
	.size	_ZN12ClassVersion4readEP9Classfile, .Lfunc_end0-_ZN12ClassVersion4readEP9Classfile
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Warning: Class Version 45.%d. (Program designed for ver 45.3)\n"
	.size	.L.str, 63


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
