	.text
	.file	"strcat.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	$10000000, %r14d        # imm = 0x989680
	cmpl	$2, %edi
	jne	.LBB0_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r14
.LBB0_2:
	movl	$1, %edi
	movl	$32, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_5
# BB#3:                                 # %.preheader
	testl	%r14d, %r14d
	jle	.LBB0_4
# BB#7:                                 # %.lr.ph.preheader
	movl	$32, %r12d
	xorl	%ebp, %ebp
	movq	%rbx, %r15
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	%r12d, %rax
	addq	%r15, %rax
	subq	%rbx, %rax
	cmpq	$6, %rax
	jg	.LBB0_12
# BB#9:                                 #   in Loop: Header=BB0_8 Depth=1
	addl	%r12d, %r12d
	movslq	%r12d, %rsi
	movq	%r15, %rdi
	callq	realloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_10
# BB#11:                                #   in Loop: Header=BB0_8 Depth=1
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbx
	addq	%r15, %rbx
.LBB0_12:                               #   in Loop: Header=BB0_8 Depth=1
	movq	%rbx, %rdi
	callq	strlen
	movb	$0, 6(%rbx,%rax)
	movw	$2671, 4(%rbx,%rax)     # imm = 0xA6F
	movl	$1819043176, (%rbx,%rax) # imm = 0x6C6C6568
	addq	$6, %rbx
	incl	%ebp
	cmpl	%r14d, %ebp
	jl	.LBB0_8
	jmp	.LBB0_13
.LBB0_4:
	movq	%rbx, %r15
.LBB0_13:                               # %._crit_edge
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rcx
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	movq	%r15, %rdi
	callq	free
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_10:
	movl	$.L.str.1, %edi
.LBB0_6:
	callq	perror
	movl	$1, %edi
	callq	exit
.LBB0_5:
	movl	$.L.str, %edi
	jmp	.LBB0_6
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"calloc strbuf"
	.size	.L.str, 14

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"realloc strbuf"
	.size	.L.str.1, 15

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"hello\n"
	.size	.L.str.2, 7

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%d\n"
	.size	.L.str.3, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
