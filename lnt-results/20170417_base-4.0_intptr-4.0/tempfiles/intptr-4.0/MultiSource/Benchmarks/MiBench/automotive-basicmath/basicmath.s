	.text
	.file	"basicmath.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4607182418800017408     # double 1
.LCPI0_1:
	.quad	-4601271444289093632    # double -10.5
.LCPI0_2:
	.quad	4629700416936869888     # double 32
.LCPI0_3:
	.quad	-4594234569871327232    # double -30
.LCPI0_4:
	.quad	-4606619468846596096    # double -4.5
.LCPI0_5:
	.quad	4625478292286210048     # double 17
.LCPI0_6:
	.quad	-4608308318706860032    # double -3.5
.LCPI0_7:
	.quad	4626885667169763328     # double 22
.LCPI0_8:
	.quad	-4593953094894616576    # double -31
.LCPI0_9:
	.quad	-4599470004438145434    # double -13.699999999999999
.LCPI0_10:
	.quad	-4593249407452839936    # double -35
.LCPI0_11:
	.quad	4613937818241073152     # double 3
.LCPI0_12:
	.quad	4623136420479977390     # double 12.34
.LCPI0_13:
	.quad	4617315517961601024     # double 5
.LCPI0_14:
	.quad	4622945017495814144     # double 12
.LCPI0_15:
	.quad	-4602678819172646912    # double -8
.LCPI0_16:
	.quad	-4588894285875684311    # double -67.890000000000001
.LCPI0_17:
	.quad	4618441417868443648     # double 6
.LCPI0_18:
	.quad	-4596036009722275430    # double -23.600000000000001
.LCPI0_19:
	.quad	4631530004285489152     # double 45
.LCPI0_20:
	.quad	4621070394150921175     # double 8.6699999999999999
.LCPI0_21:
	.quad	4620130267728707584     # double 7.5
.LCPI0_22:
	.quad	4629981891913580544     # double 34
.LCPI0_23:
	.quad	-4600427019358961664    # double -12
.LCPI0_24:
	.quad	-4613037098315599053    # double -1.7
.LCPI0_25:
	.quad	4617653287933653811     # double 5.2999999999999998
.LCPI0_26:
	.quad	4625196817309499392     # double 16
.LCPI0_27:
	.quad	4621819117588971520     # double 10
.LCPI0_28:
	.quad	-4616189618054758400    # double -1
.LCPI0_29:
	.quad	-4621575923209093513    # double -0.45100000000000001
.LCPI0_30:
	.quad	-4606056518893174784    # double -5
.LCPI0_31:
	.quad	4603669611090668421     # double 0.60999999999999999
.LCPI0_32:
	.quad	4624633867356078080     # double 15
.LCPI0_33:
	.quad	-4625196817309499392    # double -0.25
.LCPI0_34:
	.quad	4614256656552045848     # double 3.1415926535897931
.LCPI0_35:
	.quad	4640537203540230144     # double 180
.LCPI0_36:
	.quad	4562254508917369340     # double 0.001
.LCPI0_37:
	.quad	4645040803167600640     # double 360
.LCPI0_38:
	.quad	4558169792339680569     # double 5.4541539124822798E-4
.LCPI0_39:
	.quad	4618760257305316251     # double 6.2831863071795864
.LCPI0_40:
	.quad	0                       # double 0
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$72, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 112
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	$.Lstr, %edi
	callq	puts
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI0_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI0_3(%rip), %xmm3   # xmm3 = mem[0],zero
	leaq	4(%rsp), %rdi
	leaq	16(%rsp), %rsi
	callq	SolveCubic
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, 4(%rsp)
	jle	.LBB0_3
# BB#1:                                 # %.lr.ph161.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph161
                                        # =>This Inner Loop Header: Depth=1
	movsd	16(%rsp,%rbx,8), %xmm0  # xmm0 = mem[0],zero
	movl	$.L.str.2, %edi
	movb	$1, %al
	callq	printf
	incq	%rbx
	movslq	4(%rsp), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_2
.LBB0_3:                                # %._crit_edge162
	movl	$10, %edi
	callq	putchar
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI0_4(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI0_5(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI0_3(%rip), %xmm3   # xmm3 = mem[0],zero
	leaq	4(%rsp), %rdi
	leaq	16(%rsp), %rsi
	callq	SolveCubic
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, 4(%rsp)
	jle	.LBB0_6
# BB#4:                                 # %.lr.ph157.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph157
                                        # =>This Inner Loop Header: Depth=1
	movsd	16(%rsp,%rbx,8), %xmm0  # xmm0 = mem[0],zero
	movl	$.L.str.2, %edi
	movb	$1, %al
	callq	printf
	incq	%rbx
	movslq	4(%rsp), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_5
.LBB0_6:                                # %._crit_edge158
	movl	$10, %edi
	callq	putchar
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI0_6(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI0_7(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI0_8(%rip), %xmm3   # xmm3 = mem[0],zero
	leaq	4(%rsp), %rdi
	leaq	16(%rsp), %rsi
	callq	SolveCubic
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, 4(%rsp)
	jle	.LBB0_9
# BB#7:                                 # %.lr.ph153.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph153
                                        # =>This Inner Loop Header: Depth=1
	movsd	16(%rsp,%rbx,8), %xmm0  # xmm0 = mem[0],zero
	movl	$.L.str.2, %edi
	movb	$1, %al
	callq	printf
	incq	%rbx
	movslq	4(%rsp), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_8
.LBB0_9:                                # %._crit_edge154
	movl	$10, %edi
	callq	putchar
	movsd	.LCPI0_9(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI0_10(%rip), %xmm3  # xmm3 = mem[0],zero
	leaq	4(%rsp), %rdi
	leaq	16(%rsp), %rsi
	movaps	%xmm0, %xmm2
	callq	SolveCubic
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, 4(%rsp)
	jle	.LBB0_12
# BB#10:                                # %.lr.ph149.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_11:                               # %.lr.ph149
                                        # =>This Inner Loop Header: Depth=1
	movsd	16(%rsp,%rbx,8), %xmm0  # xmm0 = mem[0],zero
	movl	$.L.str.2, %edi
	movb	$1, %al
	callq	printf
	incq	%rbx
	movslq	4(%rsp), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_11
.LBB0_12:                               # %._crit_edge150
	movl	$10, %edi
	callq	putchar
	movsd	.LCPI0_11(%rip), %xmm0  # xmm0 = mem[0],zero
	movsd	.LCPI0_12(%rip), %xmm1  # xmm1 = mem[0],zero
	movsd	.LCPI0_13(%rip), %xmm2  # xmm2 = mem[0],zero
	movsd	.LCPI0_14(%rip), %xmm3  # xmm3 = mem[0],zero
	leaq	4(%rsp), %rdi
	leaq	16(%rsp), %rsi
	callq	SolveCubic
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, 4(%rsp)
	jle	.LBB0_15
# BB#13:                                # %.lr.ph145.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_14:                               # %.lr.ph145
                                        # =>This Inner Loop Header: Depth=1
	movsd	16(%rsp,%rbx,8), %xmm0  # xmm0 = mem[0],zero
	movl	$.L.str.2, %edi
	movb	$1, %al
	callq	printf
	incq	%rbx
	movslq	4(%rsp), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_14
.LBB0_15:                               # %._crit_edge146
	movl	$10, %edi
	callq	putchar
	movsd	.LCPI0_15(%rip), %xmm0  # xmm0 = mem[0],zero
	movsd	.LCPI0_16(%rip), %xmm1  # xmm1 = mem[0],zero
	movsd	.LCPI0_17(%rip), %xmm2  # xmm2 = mem[0],zero
	movsd	.LCPI0_18(%rip), %xmm3  # xmm3 = mem[0],zero
	leaq	4(%rsp), %rdi
	leaq	16(%rsp), %rsi
	callq	SolveCubic
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, 4(%rsp)
	jle	.LBB0_18
# BB#16:                                # %.lr.ph141.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_17:                               # %.lr.ph141
                                        # =>This Inner Loop Header: Depth=1
	movsd	16(%rsp,%rbx,8), %xmm0  # xmm0 = mem[0],zero
	movl	$.L.str.2, %edi
	movb	$1, %al
	callq	printf
	incq	%rbx
	movslq	4(%rsp), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_17
.LBB0_18:                               # %._crit_edge142
	movl	$10, %edi
	callq	putchar
	movsd	.LCPI0_19(%rip), %xmm0  # xmm0 = mem[0],zero
	movsd	.LCPI0_20(%rip), %xmm1  # xmm1 = mem[0],zero
	movsd	.LCPI0_21(%rip), %xmm2  # xmm2 = mem[0],zero
	movsd	.LCPI0_22(%rip), %xmm3  # xmm3 = mem[0],zero
	leaq	4(%rsp), %rdi
	leaq	16(%rsp), %rsi
	callq	SolveCubic
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, 4(%rsp)
	jle	.LBB0_21
# BB#19:                                # %.lr.ph137.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_20:                               # %.lr.ph137
                                        # =>This Inner Loop Header: Depth=1
	movsd	16(%rsp,%rbx,8), %xmm0  # xmm0 = mem[0],zero
	movl	$.L.str.2, %edi
	movb	$1, %al
	callq	printf
	incq	%rbx
	movslq	4(%rsp), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_20
.LBB0_21:                               # %._crit_edge138
	movl	$10, %edi
	callq	putchar
	movsd	.LCPI0_23(%rip), %xmm0  # xmm0 = mem[0],zero
	movsd	.LCPI0_24(%rip), %xmm1  # xmm1 = mem[0],zero
	movsd	.LCPI0_25(%rip), %xmm2  # xmm2 = mem[0],zero
	movsd	.LCPI0_26(%rip), %xmm3  # xmm3 = mem[0],zero
	leaq	4(%rsp), %rdi
	leaq	16(%rsp), %rsi
	callq	SolveCubic
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, 4(%rsp)
	jle	.LBB0_24
# BB#22:                                # %.lr.ph133.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_23:                               # %.lr.ph133
                                        # =>This Inner Loop Header: Depth=1
	movsd	16(%rsp,%rbx,8), %xmm0  # xmm0 = mem[0],zero
	movl	$.L.str.2, %edi
	movb	$1, %al
	callq	printf
	incq	%rbx
	movslq	4(%rsp), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_23
.LBB0_24:                               # %._crit_edge134
	movl	$10, %edi
	callq	putchar
	movl	$1, %r15d
	leaq	4(%rsp), %r14
	leaq	16(%rsp), %rbx
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	%xmm0, 48(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_25:                               # %.preheader121
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_26 Depth 2
                                        #       Child Loop BB0_27 Depth 3
                                        #         Child Loop BB0_28 Depth 4
                                        #           Child Loop BB0_30 Depth 5
	movsd	.LCPI0_27(%rip), %xmm0  # xmm0 = mem[0],zero
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_26:                               # %.preheader120
                                        #   Parent Loop BB0_25 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_27 Depth 3
                                        #         Child Loop BB0_28 Depth 4
                                        #           Child Loop BB0_30 Depth 5
	movsd	.LCPI0_13(%rip), %xmm0  # xmm0 = mem[0],zero
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_27:                               # %.preheader
                                        #   Parent Loop BB0_25 Depth=1
                                        #     Parent Loop BB0_26 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_28 Depth 4
                                        #           Child Loop BB0_30 Depth 5
	movsd	.LCPI0_28(%rip), %xmm0  # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm3
	.p2align	4, 0x90
.LBB0_28:                               #   Parent Loop BB0_25 Depth=1
                                        #     Parent Loop BB0_26 Depth=2
                                        #       Parent Loop BB0_27 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_30 Depth 5
	movsd	48(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	56(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	40(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	%xmm3, 8(%rsp)          # 8-byte Spill
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	SolveCubic
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, 4(%rsp)
	jle	.LBB0_31
# BB#29:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_28 Depth=4
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_30:                               # %.lr.ph
                                        #   Parent Loop BB0_25 Depth=1
                                        #     Parent Loop BB0_26 Depth=2
                                        #       Parent Loop BB0_27 Depth=3
                                        #         Parent Loop BB0_28 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movsd	16(%rsp,%rbp,8), %xmm0  # xmm0 = mem[0],zero
	movl	$.L.str.2, %edi
	movb	$1, %al
	callq	printf
	incq	%rbp
	movslq	4(%rsp), %rax
	cmpq	%rax, %rbp
	jl	.LBB0_30
.LBB0_31:                               # %._crit_edge
                                        #   in Loop: Header=BB0_28 Depth=4
	movl	$10, %edi
	callq	putchar
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
	addsd	.LCPI0_29(%rip), %xmm3
	ucomisd	.LCPI0_30(%rip), %xmm3
	ja	.LBB0_28
# BB#32:                                #   in Loop: Header=BB0_27 Depth=3
	movsd	40(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	.LCPI0_31(%rip), %xmm1
	movsd	.LCPI0_32(%rip), %xmm0  # xmm0 = mem[0],zero
	movsd	%xmm1, 40(%rsp)         # 8-byte Spill
	ucomisd	%xmm1, %xmm0
	ja	.LBB0_27
# BB#33:                                #   in Loop: Header=BB0_26 Depth=2
	movsd	56(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	.LCPI0_33(%rip), %xmm0
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	ucomisd	.LCPI0_40, %xmm0
	ja	.LBB0_26
# BB#34:                                #   in Loop: Header=BB0_25 Depth=1
	movsd	48(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	.LCPI0_0(%rip), %xmm0
	movsd	%xmm0, 48(%rsp)         # 8-byte Spill
	incl	%r15d
	cmpl	$10, %r15d
	jne	.LBB0_25
# BB#35:
	movl	$.Lstr.1, %edi
	callq	puts
	leaq	64(%rsp), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_36:                               # =>This Inner Loop Header: Depth=1
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	usqrt
	movl	64(%rsp), %edx
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
	addl	$2, %ebp
	cmpl	$100000, %ebp           # imm = 0x186A0
	jl	.LBB0_36
# BB#37:
	movl	$10, %edi
	callq	putchar
	movl	$1072497001, %ebx       # imm = 0x3FED0169
	leaq	64(%rsp), %r14
	.p2align	4, 0x90
.LBB0_38:                               # =>This Inner Loop Header: Depth=1
	movl	%ebx, %edi
	movq	%r14, %rsi
	callq	usqrt
	movl	64(%rsp), %edx
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	incl	%ebx
	cmpl	$1072513385, %ebx       # imm = 0x3FED4169
	jne	.LBB0_38
# BB#39:
	movl	$.Lstr.2, %edi
	callq	puts
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_40:                               # =>This Inner Loop Header: Depth=1
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	atan
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	mulsd	.LCPI0_34(%rip), %xmm1
	divsd	.LCPI0_35(%rip), %xmm1
	movl	$.L.str.8, %edi
	movb	$2, %al
	callq	printf
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	.LCPI0_36(%rip), %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movsd	.LCPI0_37(%rip), %xmm0  # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jae	.LBB0_40
# BB#41:
	movl	$10, %edi
	callq	putchar
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	atan
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_42:                               # =>This Inner Loop Header: Depth=1
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	mulsd	.LCPI0_35(%rip), %xmm0
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	atan
	movsd	40(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	.LCPI0_34(%rip), %xmm1
	movl	$.L.str.10, %edi
	movb	$2, %al
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	printf
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	atan
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	.LCPI0_38(%rip), %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	atan
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	.LCPI0_39(%rip), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jae	.LBB0_42
# BB#43:
	xorl	%eax, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"Solutions:"
	.size	.L.str.1, 11

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" %f"
	.size	.L.str.2, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"sqrt(%3d) = %2d\n"
	.size	.L.str.5, 17

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"sqrt(%lX) = %X\n"
	.size	.L.str.6, 16

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"%3.0f degrees = %.12f radians\n"
	.size	.L.str.8, 31

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%.12f radians = %3.0f degrees\n"
	.size	.L.str.10, 31

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"********* CUBIC FUNCTIONS ***********"
	.size	.Lstr, 38

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"********* INTEGER SQR ROOTS ***********"
	.size	.Lstr.1, 40

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"********* ANGLE CONVERSION ***********"
	.size	.Lstr.2, 39


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
