	.text
	.file	"trace.bc"
	.globl	ltrace_fcn_name
	.p2align	4, 0x90
	.type	ltrace_fcn_name,@function
ltrace_fcn_name:                        # @ltrace_fcn_name
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB0_1
# BB#2:
	movzwl	2(%rdi), %eax
	cmpl	$1, %eax
	jne	.LBB0_3
# BB#4:
	movq	8(%rdi), %rax
	cmpq	sym_begin(%rip), %rax
	je	.LBB0_6
# BB#5:
	xorl	%eax, %eax
	retq
.LBB0_1:
	xorl	%eax, %eax
	retq
.LBB0_3:
	xorl	%eax, %eax
	retq
.LBB0_6:
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.LBB0_7
# BB#8:
	movzwl	2(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB0_9
# BB#10:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.LBB0_11
# BB#12:
	movzwl	2(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB0_13
# BB#14:
	movq	8(%rax), %rcx
	cmpq	sym_quote(%rip), %rcx
	je	.LBB0_16
# BB#15:
	xorl	%eax, %eax
	retq
.LBB0_7:
	xorl	%eax, %eax
	retq
.LBB0_9:
	xorl	%eax, %eax
	retq
.LBB0_11:
	xorl	%eax, %eax
	retq
.LBB0_13:
	xorl	%eax, %eax
	retq
.LBB0_16:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.LBB0_17
# BB#18:
	movzwl	2(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB0_19
# BB#20:
	movq	8(%rax), %rax
	retq
.LBB0_17:
	xorl	%eax, %eax
	retq
.LBB0_19:
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	ltrace_fcn_name, .Lfunc_end0-ltrace_fcn_name
	.cfi_endproc

	.globl	ltrace_1
	.p2align	4, 0x90
	.type	ltrace_1,@function
ltrace_1:                               # @ltrace_1
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r13, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	callq	leval
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB1_1
# BB#4:
	movswq	2(%rbx), %rax
	cmpq	$11, %rax
	jne	.LBB1_2
# BB#5:
	movq	16(%rbx), %r14
	movq	%r14, %rdi
	callq	cdr
	testq	%rax, %rax
	je	.LBB1_16
# BB#6:
	movzwl	2(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB1_16
# BB#7:
	movq	8(%rax), %rcx
	cmpq	sym_begin(%rip), %rcx
	je	.LBB1_8
.LBB1_16:                               # %ltrace_fcn_name.exit.thread
	movq	sym_begin(%rip), %r12
	movq	sym_quote(%rip), %r13
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	cons
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	cons
	movq	%rax, %r15
	movq	%r14, %rdi
	callq	cdr
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	cons
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	cons
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	cons
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	setcdr
.LBB1_17:
	movzwl	tc_closure_traced(%rip), %eax
	movw	%ax, 2(%rbx)
	jmp	.LBB1_18
.LBB1_1:
	xorl	%eax, %eax
.LBB1_2:                                # %.critedge16
	cmpq	tc_closure_traced(%rip), %rax
	je	.LBB1_18
# BB#3:
	movl	$.L.str, %edi
	movq	%rbx, %rsi
	callq	err
.LBB1_18:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB1_8:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.LBB1_16
# BB#9:
	movzwl	2(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB1_16
# BB#10:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.LBB1_16
# BB#11:
	movzwl	2(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB1_16
# BB#12:
	movq	8(%rax), %rcx
	cmpq	sym_quote(%rip), %rcx
	jne	.LBB1_16
# BB#13:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.LBB1_16
# BB#14:
	movzwl	2(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB1_16
# BB#15:                                # %ltrace_fcn_name.exit
	cmpq	$0, 8(%rax)
	jne	.LBB1_17
	jmp	.LBB1_16
.Lfunc_end1:
	.size	ltrace_1, .Lfunc_end1-ltrace_1
	.cfi_endproc

	.globl	ltrace
	.p2align	4, 0x90
	.type	ltrace,@function
ltrace:                                 # @ltrace
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB2_3
	.p2align	4, 0x90
.LBB2_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	car
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	ltrace_1
	movq	%rbx, %rdi
	callq	cdr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB2_1
.LBB2_3:                                # %._crit_edge
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	ltrace, .Lfunc_end2-ltrace
	.cfi_endproc

	.globl	luntrace_1
	.p2align	4, 0x90
	.type	luntrace_1,@function
luntrace_1:                             # @luntrace_1
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB3_1
# BB#2:
	movswq	2(%rax), %rcx
	cmpq	$11, %rcx
	jne	.LBB3_3
	jmp	.LBB3_6
.LBB3_1:
	xorl	%ecx, %ecx
.LBB3_3:                                # %.critedge6
	cmpq	tc_closure_traced(%rip), %rcx
	jne	.LBB3_5
# BB#4:
	movw	$11, 2(%rax)
	xorl	%eax, %eax
	retq
.LBB3_5:
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 16
	movl	$.L.str.1, %edi
	movq	%rax, %rsi
	callq	err
	addq	$8, %rsp
.LBB3_6:
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	luntrace_1, .Lfunc_end3-luntrace_1
	.cfi_endproc

	.globl	luntrace
	.p2align	4, 0x90
	.type	luntrace,@function
luntrace:                               # @luntrace
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 16
.Lcfi17:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB4_2
	jmp	.LBB4_9
	.p2align	4, 0x90
.LBB4_8:                                # %luntrace_1.exit
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	%rbx, %rdi
	callq	cdr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB4_9
.LBB4_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	car
	testq	%rax, %rax
	je	.LBB4_3
# BB#4:                                 #   in Loop: Header=BB4_2 Depth=1
	movswq	2(%rax), %rcx
	cmpq	$11, %rcx
	je	.LBB4_8
	jmp	.LBB4_5
	.p2align	4, 0x90
.LBB4_3:                                #   in Loop: Header=BB4_2 Depth=1
	xorl	%ecx, %ecx
.LBB4_5:                                # %.critedge6.i
                                        #   in Loop: Header=BB4_2 Depth=1
	cmpq	tc_closure_traced(%rip), %rcx
	jne	.LBB4_7
# BB#6:                                 #   in Loop: Header=BB4_2 Depth=1
	movw	$11, 2(%rax)
	jmp	.LBB4_8
	.p2align	4, 0x90
.LBB4_7:                                #   in Loop: Header=BB4_2 Depth=1
	movl	$.L.str.1, %edi
	movq	%rax, %rsi
	callq	err
	jmp	.LBB4_8
.LBB4_9:                                # %._crit_edge
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end4:
	.size	luntrace, .Lfunc_end4-luntrace
	.cfi_endproc

	.globl	ct_prin1
	.p2align	4, 0x90
	.type	ct_prin1,@function
ct_prin1:                               # @ct_prin1
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	$.L.str.2, %esi
	movq	%rbx, %rdi
	callq	gput_st
	movq	16(%r14), %rdi
	callq	car
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	lprin1g
	movl	$.L.str.3, %esi
	movq	%rbx, %rdi
	callq	gput_st
	movq	16(%r14), %rdi
	callq	cdr
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	lprin1g
	movl	$.L.str.4, %esi
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	gput_st                 # TAILCALL
.Lfunc_end5:
	.size	ct_prin1, .Lfunc_end5-ct_prin1
	.cfi_endproc

	.globl	ct_eval
	.p2align	4, 0x90
	.type	ct_eval,@function
ct_eval:                                # @ct_eval
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 48
.Lcfi28:
	.cfi_offset %rbx, -48
.Lcfi29:
	.cfi_offset %r12, -40
.Lcfi30:
	.cfi_offset %r13, -32
.Lcfi31:
	.cfi_offset %r14, -24
.Lcfi32:
	.cfi_offset %r15, -16
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %r13
	movq	16(%r13), %rdi
	callq	cdr
	testq	%rax, %rax
	je	.LBB6_5
# BB#1:
	movzwl	2(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB6_5
# BB#2:
	movq	8(%rax), %rcx
	cmpq	sym_begin(%rip), %rcx
	je	.LBB6_10
.LBB6_5:
	xorl	%r15d, %r15d
.LBB6_6:                                # %ltrace_fcn_name.exit
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	movq	(%r12), %rsi
	callq	leval_args
	movq	%rax, %r12
	movq	stdout(%rip), %rdi
	movl	$.L.str.5, %esi
	callq	fput_st
	movq	stdout(%rip), %rsi
	movq	%r15, %rdi
	callq	lprin1f
	testq	%r12, %r12
	movq	stdout(%rip), %rdi
	je	.LBB6_9
# BB#7:                                 # %.lr.ph.preheader
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB6_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.3, %esi
	callq	fput_st
	movq	%rbx, %rdi
	callq	car
	movq	stdout(%rip), %rsi
	movq	%rax, %rdi
	callq	lprin1f
	movq	%rbx, %rdi
	callq	cdr
	movq	%rax, %rbx
	movq	stdout(%rip), %rdi
	testq	%rbx, %rbx
	jne	.LBB6_8
.LBB6_9:                                # %._crit_edge
	movl	$.L.str.6, %esi
	callq	fput_st
	movq	16(%r13), %rdi
	callq	car
	movq	8(%r13), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	extend_env
	movq	%rax, %rbx
	movq	16(%r13), %rdi
	callq	cdr
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	leval
	movq	%rax, %rbx
	movq	stdout(%rip), %rdi
	movl	$.L.str.7, %esi
	callq	fput_st
	movq	stdout(%rip), %rsi
	movq	%r15, %rdi
	callq	lprin1f
	movq	stdout(%rip), %rdi
	movl	$.L.str.3, %esi
	callq	fput_st
	movq	stdout(%rip), %rsi
	movq	%rbx, %rdi
	callq	lprin1f
	movq	stdout(%rip), %rdi
	movl	$.L.str.6, %esi
	callq	fput_st
	movq	%rbx, (%r14)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB6_10:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.LBB6_5
# BB#11:
	movzwl	2(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB6_5
# BB#12:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.LBB6_5
# BB#13:
	movzwl	2(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB6_5
# BB#14:
	movq	8(%rax), %rcx
	cmpq	sym_quote(%rip), %rcx
	jne	.LBB6_5
# BB#20:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.LBB6_5
# BB#21:
	movzwl	2(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB6_5
# BB#22:
	movq	8(%rax), %r15
	jmp	.LBB6_6
.Lfunc_end6:
	.size	ct_eval, .Lfunc_end6-ct_eval
	.cfi_endproc

	.globl	init_trace
	.p2align	4, 0x90
	.type	init_trace,@function
init_trace:                             # @init_trace
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -16
	callq	allocate_user_tc
	movq	%rax, tc_closure_traced(%rip)
	leaq	8(%rsp), %r9
	xorl	%esi, %esi
	movl	$ct_gc_mark, %edx
	movl	$ct_gc_scan, %ecx
	xorl	%r8d, %r8d
	movq	%rax, %rdi
	callq	set_gc_hooks
	movl	$sym_traced, %edi
	movl	$.L.str.8, %esi
	callq	gc_protect_sym
	movq	sym_traced(%rip), %rdi
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	setvar
	movl	$sym_begin, %edi
	movl	$.L.str.9, %esi
	callq	gc_protect_sym
	movl	$sym_quote, %edi
	movl	$.L.str.10, %esi
	callq	gc_protect_sym
	movq	tc_closure_traced(%rip), %rdi
	movl	$ct_prin1, %esi
	callq	set_print_hooks
	movq	tc_closure_traced(%rip), %rdi
	movl	$ct_eval, %esi
	callq	set_eval_hooks
	movl	$.L.str.11, %edi
	movl	$ltrace, %esi
	callq	init_fsubr
	movl	$.L.str.12, %edi
	movl	$luntrace, %esi
	callq	init_lsubr
	movl	$.L.str.13, %edi
	callq	cintern
	movq	%rax, %rbx
	movl	$.L.str.14, %edi
	callq	cintern
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setvar
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end7:
	.size	init_trace, .Lfunc_end7-init_trace
	.cfi_endproc

	.p2align	4, 0x90
	.type	ct_gc_mark,@function
ct_gc_mark:                             # @ct_gc_mark
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 16
.Lcfi37:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	callq	gc_mark
	movq	8(%rbx), %rax
	popq	%rbx
	retq
.Lfunc_end8:
	.size	ct_gc_mark, .Lfunc_end8-ct_gc_mark
	.cfi_endproc

	.p2align	4, 0x90
	.type	ct_gc_scan,@function
ct_gc_scan:                             # @ct_gc_scan
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 16
.Lcfi39:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	callq	gc_relocate
	movq	%rax, 8(%rbx)
	movq	16(%rbx), %rdi
	callq	gc_relocate
	movq	%rax, 16(%rbx)
	popq	%rbx
	retq
.Lfunc_end9:
	.size	ct_gc_scan, .Lfunc_end9-ct_gc_scan
	.cfi_endproc

	.type	sym_begin,@object       # @sym_begin
	.local	sym_begin
	.comm	sym_begin,8,8
	.type	sym_quote,@object       # @sym_quote
	.local	sym_quote
	.comm	sym_quote,8,8
	.type	tc_closure_traced,@object # @tc_closure_traced
	.local	tc_closure_traced
	.comm	tc_closure_traced,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"not a closure, cannot trace"
	.size	.L.str, 28

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"not a closure, cannot untrace"
	.size	.L.str.1, 30

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"#<CLOSURE(TRACED) "
	.size	.L.str.2, 19

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	" "
	.size	.L.str.3, 2

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	">"
	.size	.L.str.4, 2

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"->"
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\n"
	.size	.L.str.6, 2

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"<-"
	.size	.L.str.7, 3

	.type	sym_traced,@object      # @sym_traced
	.local	sym_traced
	.comm	sym_traced,8,8
	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"*traced*"
	.size	.L.str.8, 9

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"begin"
	.size	.L.str.9, 6

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"quote"
	.size	.L.str.10, 6

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"trace"
	.size	.L.str.11, 6

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"untrace"
	.size	.L.str.12, 8

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"*trace-version*"
	.size	.L.str.13, 16

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"$Id: trace.c 9206 2003-10-17 18:48:45Z gaeke $"
	.size	.L.str.14, 47


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
