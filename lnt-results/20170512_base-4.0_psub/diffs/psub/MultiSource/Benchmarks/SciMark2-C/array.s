	.text
	.file	"array.bc"
	.globl	new_Array2D_double
	.p2align	4, 0x90
	.type	new_Array2D_double,@function
new_Array2D_double:                     # @new_Array2D_double
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movl	%edi, %ebp
	movslq	%ebp, %r15
	leaq	(,%r15,8), %rdi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_8
# BB#1:                                 # %.preheader24
	testl	%ebp, %ebp
	jle	.LBB0_9
# BB#2:                                 # %.lr.ph29
	movslq	%ebx, %r12
	shlq	$3, %r12
	movq	%r14, %rbp
	addq	$-8, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, 8(%rbp)
	testq	%rax, %rax
	je	.LBB0_4
# BB#6:                                 #   in Loop: Header=BB0_3 Depth=1
	incq	%rbx
	addq	$8, %rbp
	cmpq	%r15, %rbx
	jl	.LBB0_3
	jmp	.LBB0_9
.LBB0_4:                                # %.preheader
	cmpl	$1, %ebx
	jg	.LBB0_7
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	callq	free
	addq	$-8, %rbp
	jmp	.LBB0_5
.LBB0_7:                                # %._crit_edge
	movq	%r14, %rdi
	callq	free
.LBB0_8:                                # %.critedge
	xorl	%r14d, %r14d
.LBB0_9:                                # %.critedge
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	new_Array2D_double, .Lfunc_end0-new_Array2D_double
	.cfi_endproc

	.globl	Array2D_double_delete
	.p2align	4, 0x90
	.type	Array2D_double_delete,@function
Array2D_double_delete:                  # @Array2D_double_delete
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	testq	%r14, %r14
	je	.LBB1_5
# BB#1:                                 # %.preheader
	testl	%edi, %edi
	jle	.LBB1_4
# BB#2:                                 # %.lr.ph.preheader
	movl	%edi, %r15d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	callq	free
	addq	$8, %rbx
	decq	%r15
	jne	.LBB1_3
.LBB1_4:                                # %._crit_edge
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.LBB1_5:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	Array2D_double_delete, .Lfunc_end1-Array2D_double_delete
	.cfi_endproc

	.globl	Array2D_double_copy
	.p2align	4, 0x90
	.type	Array2D_double_copy,@function
Array2D_double_copy:                    # @Array2D_double_copy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 48
.Lcfi21:
	.cfi_offset %rbx, -48
.Lcfi22:
	.cfi_offset %r12, -40
.Lcfi23:
	.cfi_offset %r13, -32
.Lcfi24:
	.cfi_offset %r14, -24
.Lcfi25:
	.cfi_offset %r15, -16
	testl	%edi, %edi
	jle	.LBB2_16
# BB#1:                                 # %.lr.ph49
	movl	%esi, %r8d
	andl	$3, %r8d
	je	.LBB2_9
# BB#2:                                 # %.lr.ph49.split.us.preheader
	movslq	%esi, %r15
	movl	%r8d, %eax
	movl	%edi, %r14d
	leaq	-1(%r15), %r9
	subq	%rax, %r9
	shrq	$2, %r9
	movl	%r9d, %r10d
	andl	$1, %r10d
	leaq	4(%r8), %r11
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_19 Depth 2
	movq	(%rdx,%r12,8), %r13
	movq	(%rcx,%r12,8), %rax
	movq	(%rax), %rdi
	movq	%rdi, (%r13)
	cmpl	$1, %r8d
	je	.LBB2_4
# BB#17:                                #   in Loop: Header=BB2_3 Depth=1
	cmpl	$2, %r8d
	movq	8(%rax), %rdi
	movq	%rdi, 8(%r13)
	je	.LBB2_4
# BB#18:                                #   in Loop: Header=BB2_3 Depth=1
	movq	16(%rax), %rdi
	movq	%rdi, 16(%r13)
.LBB2_4:                                # %..preheader_crit_edge.us
                                        #   in Loop: Header=BB2_3 Depth=1
	cmpl	%esi, %r8d
	jge	.LBB2_8
# BB#5:                                 # %.lr.ph46.us.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	testq	%r10, %r10
	movq	%r8, %rbx
	jne	.LBB2_7
# BB#6:                                 # %.lr.ph46.us.prol
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	(%rax,%r8,8), %rdi
	movq	%rdi, (%r13,%r8,8)
	movq	8(%rax,%r8,8), %rdi
	movq	%rdi, 8(%r13,%r8,8)
	movq	16(%rax,%r8,8), %rdi
	movq	%rdi, 16(%r13,%r8,8)
	movq	24(%rax,%r8,8), %rdi
	movq	%rdi, 24(%r13,%r8,8)
	movq	%r11, %rbx
.LBB2_7:                                # %.lr.ph46.us.prol.loopexit
                                        #   in Loop: Header=BB2_3 Depth=1
	testq	%r9, %r9
	je	.LBB2_8
	.p2align	4, 0x90
.LBB2_19:                               # %.lr.ph46.us
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rbx,8), %rdi
	movq	%rdi, (%r13,%rbx,8)
	movq	8(%rax,%rbx,8), %rdi
	movq	%rdi, 8(%r13,%rbx,8)
	movq	16(%rax,%rbx,8), %rdi
	movq	%rdi, 16(%r13,%rbx,8)
	movq	24(%rax,%rbx,8), %rdi
	movq	%rdi, 24(%r13,%rbx,8)
	movq	32(%rax,%rbx,8), %rdi
	movq	%rdi, 32(%r13,%rbx,8)
	movq	40(%rax,%rbx,8), %rdi
	movq	%rdi, 40(%r13,%rbx,8)
	movq	48(%rax,%rbx,8), %rdi
	movq	%rdi, 48(%r13,%rbx,8)
	movq	56(%rax,%rbx,8), %rdi
	movq	%rdi, 56(%r13,%rbx,8)
	addq	$8, %rbx
	cmpq	%r15, %rbx
	jl	.LBB2_19
.LBB2_8:                                # %._crit_edge.us
                                        #   in Loop: Header=BB2_3 Depth=1
	incq	%r12
	cmpq	%r14, %r12
	jne	.LBB2_3
	jmp	.LBB2_16
.LBB2_9:                                # %.lr.ph49.split
	cmpl	%esi, %r8d
	jge	.LBB2_16
# BB#10:                                # %.lr.ph49.split.split.us.preheader
	movslq	%esi, %r12
	movl	%edi, %r10d
	leaq	-1(%r12), %r14
	subq	%r8, %r14
	shrq	$2, %r14
	movl	%r14d, %r11d
	andl	$1, %r11d
	leaq	4(%r8), %r9
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB2_11:                               # %.lr.ph49.split.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_14 Depth 2
	movq	(%rdx,%r15,8), %rbx
	movq	(%rcx,%r15,8), %rdi
	testq	%r11, %r11
	movq	%r8, %rsi
	jne	.LBB2_13
# BB#12:                                #   in Loop: Header=BB2_11 Depth=1
	movq	(%rdi,%r8,8), %rsi
	movq	%rsi, (%rbx,%r8,8)
	movq	8(%rdi,%r8,8), %rsi
	movq	%rsi, 8(%rbx,%r8,8)
	movq	16(%rdi,%r8,8), %rsi
	movq	%rsi, 16(%rbx,%r8,8)
	movq	24(%rdi,%r8,8), %rsi
	movq	%rsi, 24(%rbx,%r8,8)
	movq	%r9, %rsi
.LBB2_13:                               # %.prol.loopexit
                                        #   in Loop: Header=BB2_11 Depth=1
	testq	%r14, %r14
	je	.LBB2_15
	.p2align	4, 0x90
.LBB2_14:                               #   Parent Loop BB2_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi,%rsi,8), %rax
	movq	%rax, (%rbx,%rsi,8)
	movq	8(%rdi,%rsi,8), %rax
	movq	%rax, 8(%rbx,%rsi,8)
	movq	16(%rdi,%rsi,8), %rax
	movq	%rax, 16(%rbx,%rsi,8)
	movq	24(%rdi,%rsi,8), %rax
	movq	%rax, 24(%rbx,%rsi,8)
	movq	32(%rdi,%rsi,8), %rax
	movq	%rax, 32(%rbx,%rsi,8)
	movq	40(%rdi,%rsi,8), %rax
	movq	%rax, 40(%rbx,%rsi,8)
	movq	48(%rdi,%rsi,8), %rax
	movq	%rax, 48(%rbx,%rsi,8)
	movq	56(%rdi,%rsi,8), %rax
	movq	%rax, 56(%rbx,%rsi,8)
	addq	$8, %rsi
	cmpq	%r12, %rsi
	jl	.LBB2_14
.LBB2_15:                               # %._crit_edge.us58
                                        #   in Loop: Header=BB2_11 Depth=1
	incq	%r15
	cmpq	%r10, %r15
	jne	.LBB2_11
.LBB2_16:                               # %._crit_edge50
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	Array2D_double_copy, .Lfunc_end2-Array2D_double_copy
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
