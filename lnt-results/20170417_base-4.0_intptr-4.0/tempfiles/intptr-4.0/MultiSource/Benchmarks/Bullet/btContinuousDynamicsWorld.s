	.text
	.file	"btContinuousDynamicsWorld.bc"
	.globl	_ZN25btContinuousDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
	.p2align	4, 0x90
	.type	_ZN25btContinuousDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration,@function
_ZN25btContinuousDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration: # @_ZN25btContinuousDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN23btDiscreteDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
	movq	$_ZTV25btContinuousDynamicsWorld+16, (%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN25btContinuousDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration, .Lfunc_end0-_ZN25btContinuousDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
	.cfi_endproc

	.globl	_ZN25btContinuousDynamicsWorldD2Ev
	.p2align	4, 0x90
	.type	_ZN25btContinuousDynamicsWorldD2Ev,@function
_ZN25btContinuousDynamicsWorldD2Ev:     # @_ZN25btContinuousDynamicsWorldD2Ev
	.cfi_startproc
# BB#0:
	jmp	_ZN23btDiscreteDynamicsWorldD2Ev # TAILCALL
.Lfunc_end1:
	.size	_ZN25btContinuousDynamicsWorldD2Ev, .Lfunc_end1-_ZN25btContinuousDynamicsWorldD2Ev
	.cfi_endproc

	.globl	_ZN25btContinuousDynamicsWorldD0Ev
	.p2align	4, 0x90
	.type	_ZN25btContinuousDynamicsWorldD0Ev,@function
_ZN25btContinuousDynamicsWorldD0Ev:     # @_ZN25btContinuousDynamicsWorldD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp0:
	callq	_ZN23btDiscreteDynamicsWorldD2Ev
.Ltmp1:
# BB#1:                                 # %_ZN25btContinuousDynamicsWorldD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB2_2:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN25btContinuousDynamicsWorldD0Ev, .Lfunc_end2-_ZN25btContinuousDynamicsWorldD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end2-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN25btContinuousDynamicsWorld28internalSingleStepSimulationEf
	.p2align	4, 0x90
	.type	_ZN25btContinuousDynamicsWorld28internalSingleStepSimulationEf,@function
_ZN25btContinuousDynamicsWorld28internalSingleStepSimulationEf: # @_ZN25btContinuousDynamicsWorld28internalSingleStepSimulationEf
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	callq	_ZN23btDiscreteDynamicsWorld14startProfilingEf
	movq	144(%rbx), %rax
	testq	%rax, %rax
	je	.LBB3_2
# BB#1:
	movq	%rbx, %rdi
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	*%rax
.LBB3_2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*16(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	*240(%rax)
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 48(%rbx)
	movl	$0, 52(%rbx)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	%rax, 72(%rbx)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*56(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*256(%rax)
	leaq	160(%rbx), %rsi
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 172(%rbx)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*264(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	*320(%rax)
	movss	60(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	ucomiss	%xmm1, %xmm0
	jbe	.LBB3_4
# BB#3:
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	callq	printf
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
.LBB3_4:
	movq	(%rbx), %rax
	mulss	8(%rsp), %xmm1          # 4-byte Folded Reload
	movq	%rbx, %rdi
	movaps	%xmm1, %xmm0
	callq	*248(%rax)
	movq	%rbx, %rdi
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN23btDiscreteDynamicsWorld13updateActionsEf
	movq	%rbx, %rdi
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN23btDiscreteDynamicsWorld21updateActivationStateEf
	movq	136(%rbx), %rax
	testq	%rax, %rax
	je	.LBB3_5
# BB#6:
	movq	%rbx, %rdi
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$16, %rsp
	popq	%rbx
	jmpq	*%rax                   # TAILCALL
.LBB3_5:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end3:
	.size	_ZN25btContinuousDynamicsWorld28internalSingleStepSimulationEf, .Lfunc_end3-_ZN25btContinuousDynamicsWorld28internalSingleStepSimulationEf
	.cfi_endproc

	.globl	_ZN25btContinuousDynamicsWorld22calculateTimeOfImpactsEf
	.p2align	4, 0x90
	.type	_ZN25btContinuousDynamicsWorld22calculateTimeOfImpactsEf,@function
_ZN25btContinuousDynamicsWorld22calculateTimeOfImpactsEf: # @_ZN25btContinuousDynamicsWorld22calculateTimeOfImpactsEf
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 48
.Lcfi15:
	.cfi_offset %rbx, -40
.Lcfi16:
	.cfi_offset %r12, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movq	%rdi, %rbx
	callq	_ZN25btContinuousDynamicsWorld19updateTemporalAabbsEf
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 48(%rbx)
	movl	$1065353216, 60(%rbx)   # imm = 0x3F800000
	movabsq	$8589934592, %rax       # imm = 0x200000000
	movq	%rax, 52(%rbx)
	movq	40(%rbx), %r14
	testq	%r14, %r14
	je	.LBB4_2
# BB#1:
	leaq	48(%rbx), %r15
	movq	(%r14), %rax
	movq	64(%rax), %r12
	movq	112(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*64(%rax)
	movq	40(%rbx), %rcx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%r15, %rdx
	callq	*%r12
.LBB4_2:
	movl	$1, 56(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	_ZN25btContinuousDynamicsWorld22calculateTimeOfImpactsEf, .Lfunc_end4-_ZN25btContinuousDynamicsWorld22calculateTimeOfImpactsEf
	.cfi_endproc

	.globl	_ZN25btContinuousDynamicsWorld19updateTemporalAabbsEf
	.p2align	4, 0x90
	.type	_ZN25btContinuousDynamicsWorld19updateTemporalAabbsEf,@function
_ZN25btContinuousDynamicsWorld19updateTemporalAabbsEf: # @_ZN25btContinuousDynamicsWorld19updateTemporalAabbsEf
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 48
	subq	$48, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 96
.Lcfi25:
	.cfi_offset %rbx, -48
.Lcfi26:
	.cfi_offset %r12, -40
.Lcfi27:
	.cfi_offset %r13, -32
.Lcfi28:
	.cfi_offset %r14, -24
.Lcfi29:
	.cfi_offset %r15, -16
	movss	%xmm0, 44(%rsp)         # 4-byte Spill
	movq	%rdi, %r12
	movl	12(%r12), %eax
	testl	%eax, %eax
	jle	.LBB5_15
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	leaq	24(%rsp), %r14
	leaq	8(%rsp), %r15
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	movq	24(%r12), %rcx
	movq	(%rcx,%rbx,8), %r13
	testq	%r13, %r13
	je	.LBB5_14
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	cmpl	$2, 256(%r13)
	jne	.LBB5_14
# BB#4:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	200(%r13), %rdi
	movq	(%rdi), %rax
	leaq	8(%r13), %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	*16(%rax)
	xorps	%xmm4, %xmm4
	movl	8(%rsp), %edx
	movl	12(%rsp), %ecx
	movl	16(%rsp), %r8d
	movl	24(%rsp), %esi
	movl	28(%rsp), %edi
	movl	32(%rsp), %eax
	movss	328(%r13), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	44(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	movss	332(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	movss	336(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	ucomiss	%xmm4, %xmm2
	jbe	.LBB5_6
# BB#5:                                 #   in Loop: Header=BB5_2 Depth=1
	movd	%edx, %xmm3
	addss	%xmm2, %xmm3
	movd	%xmm3, %edx
	ucomiss	%xmm4, %xmm1
	ja	.LBB5_8
	jmp	.LBB5_9
	.p2align	4, 0x90
.LBB5_6:                                #   in Loop: Header=BB5_2 Depth=1
	movd	%esi, %xmm3
	addss	%xmm2, %xmm3
	movd	%xmm3, %esi
	ucomiss	%xmm4, %xmm1
	jbe	.LBB5_9
.LBB5_8:                                #   in Loop: Header=BB5_2 Depth=1
	movd	%ecx, %xmm2
	addss	%xmm1, %xmm2
	movd	%xmm2, %ecx
	ucomiss	%xmm4, %xmm0
	ja	.LBB5_11
	jmp	.LBB5_12
	.p2align	4, 0x90
.LBB5_9:                                #   in Loop: Header=BB5_2 Depth=1
	movd	%edi, %xmm2
	addss	%xmm1, %xmm2
	movd	%xmm2, %edi
	ucomiss	%xmm4, %xmm0
	jbe	.LBB5_12
.LBB5_11:                               #   in Loop: Header=BB5_2 Depth=1
	movd	%r8d, %xmm1
	addss	%xmm0, %xmm1
	movd	%xmm1, %r8d
	jmp	.LBB5_13
	.p2align	4, 0x90
.LBB5_12:                               #   in Loop: Header=BB5_2 Depth=1
	movd	%eax, %xmm1
	addss	%xmm0, %xmm1
	movd	%xmm1, %eax
.LBB5_13:                               #   in Loop: Header=BB5_2 Depth=1
	movl	%esi, 24(%rsp)
	movl	%edi, 28(%rsp)
	movl	%eax, 32(%rsp)
	movl	$0, 36(%rsp)
	movl	$0, 20(%rsp)
	movd	%edx, %xmm0
	addss	%xmm4, %xmm0
	movss	%xmm0, 8(%rsp)
	movd	%ecx, %xmm0
	addss	%xmm4, %xmm0
	movss	%xmm0, 12(%rsp)
	movd	%r8d, %xmm0
	addss	%xmm4, %xmm0
	movss	%xmm0, 16(%rsp)
	movq	40(%r12), %r8
	movq	112(%r12), %rdi
	movq	(%rdi), %rax
	movq	192(%r13), %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	*32(%rax)
	movl	12(%r12), %eax
.LBB5_14:                               #   in Loop: Header=BB5_2 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB5_2
.LBB5_15:                               # %._crit_edge
	movq	40(%r12), %rsi
	movq	112(%r12), %rdi
	movq	(%rdi), %rax
	callq	*56(%rax)
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	_ZN25btContinuousDynamicsWorld19updateTemporalAabbsEf, .Lfunc_end5-_ZN25btContinuousDynamicsWorld19updateTemporalAabbsEf
	.cfi_endproc

	.section	.text._ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw,"axG",@progbits,_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw,comdat
	.weak	_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw,@function
_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw: # @_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw
	.cfi_startproc
# BB#0:
	movq	%rsi, 120(%rdi)
	retq
.Lfunc_end6:
	.size	_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw, .Lfunc_end6-_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw
	.cfi_endproc

	.section	.text._ZN16btCollisionWorld14getDebugDrawerEv,"axG",@progbits,_ZN16btCollisionWorld14getDebugDrawerEv,comdat
	.weak	_ZN16btCollisionWorld14getDebugDrawerEv
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld14getDebugDrawerEv,@function
_ZN16btCollisionWorld14getDebugDrawerEv: # @_ZN16btCollisionWorld14getDebugDrawerEv
	.cfi_startproc
# BB#0:
	movq	120(%rdi), %rax
	retq
.Lfunc_end7:
	.size	_ZN16btCollisionWorld14getDebugDrawerEv, .Lfunc_end7-_ZN16btCollisionWorld14getDebugDrawerEv
	.cfi_endproc

	.section	.text._ZNK25btContinuousDynamicsWorld12getWorldTypeEv,"axG",@progbits,_ZNK25btContinuousDynamicsWorld12getWorldTypeEv,comdat
	.weak	_ZNK25btContinuousDynamicsWorld12getWorldTypeEv
	.p2align	4, 0x90
	.type	_ZNK25btContinuousDynamicsWorld12getWorldTypeEv,@function
_ZNK25btContinuousDynamicsWorld12getWorldTypeEv: # @_ZNK25btContinuousDynamicsWorld12getWorldTypeEv
	.cfi_startproc
# BB#0:
	movl	$3, %eax
	retq
.Lfunc_end8:
	.size	_ZNK25btContinuousDynamicsWorld12getWorldTypeEv, .Lfunc_end8-_ZNK25btContinuousDynamicsWorld12getWorldTypeEv
	.cfi_endproc

	.section	.text._ZN23btDiscreteDynamicsWorld11setNumTasksEi,"axG",@progbits,_ZN23btDiscreteDynamicsWorld11setNumTasksEi,comdat
	.weak	_ZN23btDiscreteDynamicsWorld11setNumTasksEi
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld11setNumTasksEi,@function
_ZN23btDiscreteDynamicsWorld11setNumTasksEi: # @_ZN23btDiscreteDynamicsWorld11setNumTasksEi
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end9:
	.size	_ZN23btDiscreteDynamicsWorld11setNumTasksEi, .Lfunc_end9-_ZN23btDiscreteDynamicsWorld11setNumTasksEi
	.cfi_endproc

	.section	.text._ZN23btDiscreteDynamicsWorld14updateVehiclesEf,"axG",@progbits,_ZN23btDiscreteDynamicsWorld14updateVehiclesEf,comdat
	.weak	_ZN23btDiscreteDynamicsWorld14updateVehiclesEf
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld14updateVehiclesEf,@function
_ZN23btDiscreteDynamicsWorld14updateVehiclesEf: # @_ZN23btDiscreteDynamicsWorld14updateVehiclesEf
	.cfi_startproc
# BB#0:
	jmp	_ZN23btDiscreteDynamicsWorld13updateActionsEf # TAILCALL
.Lfunc_end10:
	.size	_ZN23btDiscreteDynamicsWorld14updateVehiclesEf, .Lfunc_end10-_ZN23btDiscreteDynamicsWorld14updateVehiclesEf
	.cfi_endproc

	.type	_ZTV25btContinuousDynamicsWorld,@object # @_ZTV25btContinuousDynamicsWorld
	.section	.rodata,"a",@progbits
	.globl	_ZTV25btContinuousDynamicsWorld
	.p2align	3
_ZTV25btContinuousDynamicsWorld:
	.quad	0
	.quad	_ZTI25btContinuousDynamicsWorld
	.quad	_ZN25btContinuousDynamicsWorldD2Ev
	.quad	_ZN25btContinuousDynamicsWorldD0Ev
	.quad	_ZN16btCollisionWorld11updateAabbsEv
	.quad	_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw
	.quad	_ZN16btCollisionWorld14getDebugDrawerEv
	.quad	_ZN23btDiscreteDynamicsWorld18addCollisionObjectEP17btCollisionObjectss
	.quad	_ZN23btDiscreteDynamicsWorld21removeCollisionObjectEP17btCollisionObject
	.quad	_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv
	.quad	_ZN23btDiscreteDynamicsWorld14stepSimulationEfif
	.quad	_ZN23btDiscreteDynamicsWorld14debugDrawWorldEv
	.quad	_ZN23btDiscreteDynamicsWorld13addConstraintEP17btTypedConstraintb
	.quad	_ZN23btDiscreteDynamicsWorld16removeConstraintEP17btTypedConstraint
	.quad	_ZN23btDiscreteDynamicsWorld9addActionEP17btActionInterface
	.quad	_ZN23btDiscreteDynamicsWorld12removeActionEP17btActionInterface
	.quad	_ZN23btDiscreteDynamicsWorld10setGravityERK9btVector3
	.quad	_ZNK23btDiscreteDynamicsWorld10getGravityEv
	.quad	_ZN23btDiscreteDynamicsWorld23synchronizeMotionStatesEv
	.quad	_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBody
	.quad	_ZN23btDiscreteDynamicsWorld15removeRigidBodyEP11btRigidBody
	.quad	_ZN23btDiscreteDynamicsWorld19setConstraintSolverEP18btConstraintSolver
	.quad	_ZN23btDiscreteDynamicsWorld19getConstraintSolverEv
	.quad	_ZNK23btDiscreteDynamicsWorld17getNumConstraintsEv
	.quad	_ZN23btDiscreteDynamicsWorld13getConstraintEi
	.quad	_ZNK23btDiscreteDynamicsWorld13getConstraintEi
	.quad	_ZNK25btContinuousDynamicsWorld12getWorldTypeEv
	.quad	_ZN23btDiscreteDynamicsWorld11clearForcesEv
	.quad	_ZN23btDiscreteDynamicsWorld10addVehicleEP17btActionInterface
	.quad	_ZN23btDiscreteDynamicsWorld13removeVehicleEP17btActionInterface
	.quad	_ZN23btDiscreteDynamicsWorld12addCharacterEP17btActionInterface
	.quad	_ZN23btDiscreteDynamicsWorld15removeCharacterEP17btActionInterface
	.quad	_ZN23btDiscreteDynamicsWorld25predictUnconstraintMotionEf
	.quad	_ZN23btDiscreteDynamicsWorld19integrateTransformsEf
	.quad	_ZN23btDiscreteDynamicsWorld26calculateSimulationIslandsEv
	.quad	_ZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfo
	.quad	_ZN25btContinuousDynamicsWorld28internalSingleStepSimulationEf
	.quad	_ZN23btDiscreteDynamicsWorld18saveKinematicStateEf
	.quad	_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBodyss
	.quad	_ZN23btDiscreteDynamicsWorld12applyGravityEv
	.quad	_ZN23btDiscreteDynamicsWorld11setNumTasksEi
	.quad	_ZN23btDiscreteDynamicsWorld14updateVehiclesEf
	.quad	_ZN25btContinuousDynamicsWorld22calculateTimeOfImpactsEf
	.size	_ZTV25btContinuousDynamicsWorld, 344

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"toi = %f\n"
	.size	.L.str, 10

	.type	_ZTS25btContinuousDynamicsWorld,@object # @_ZTS25btContinuousDynamicsWorld
	.section	.rodata,"a",@progbits
	.globl	_ZTS25btContinuousDynamicsWorld
	.p2align	4
_ZTS25btContinuousDynamicsWorld:
	.asciz	"25btContinuousDynamicsWorld"
	.size	_ZTS25btContinuousDynamicsWorld, 28

	.type	_ZTI25btContinuousDynamicsWorld,@object # @_ZTI25btContinuousDynamicsWorld
	.globl	_ZTI25btContinuousDynamicsWorld
	.p2align	4
_ZTI25btContinuousDynamicsWorld:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS25btContinuousDynamicsWorld
	.quad	_ZTI23btDiscreteDynamicsWorld
	.size	_ZTI25btContinuousDynamicsWorld, 24


	.globl	_ZN25btContinuousDynamicsWorldC1EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
	.type	_ZN25btContinuousDynamicsWorldC1EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration,@function
_ZN25btContinuousDynamicsWorldC1EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration = _ZN25btContinuousDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
	.globl	_ZN25btContinuousDynamicsWorldD1Ev
	.type	_ZN25btContinuousDynamicsWorldD1Ev,@function
_ZN25btContinuousDynamicsWorldD1Ev = _ZN25btContinuousDynamicsWorldD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
