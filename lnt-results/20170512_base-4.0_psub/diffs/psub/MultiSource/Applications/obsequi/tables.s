	.text
	.file	"tables.bc"
	.globl	init_static_tables
	.p2align	4, 0x90
	.type	init_static_tables,@function
init_static_tables:                     # @init_static_tables
	.cfi_startproc
# BB#0:
	movl	$0, countbits16(%rip)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	leal	1(%rax), %ecx
	movl	%ecx, %edx
	andl	$1, %edx
	sarl	%ecx
	movslq	%ecx, %rcx
	addl	countbits16(,%rcx,4), %edx
	movl	%edx, countbits16+4(,%rax,4)
	leal	2(%rax), %ecx
	movl	%ecx, %edx
	andl	$1, %edx
	sarl	%ecx
	movslq	%ecx, %rcx
	addl	countbits16(,%rcx,4), %edx
	movl	%edx, countbits16+8(,%rax,4)
	leaq	3(%rax), %rcx
	movl	%ecx, %edx
	andl	$1, %edx
	movl	%ecx, %esi
	sarl	%esi
	movslq	%esi, %rsi
	addl	countbits16(,%rsi,4), %edx
	movl	%edx, countbits16+12(,%rax,4)
	cmpq	$65535, %rcx            # imm = 0xFFFF
	movq	%rcx, %rax
	jne	.LBB0_1
# BB#2:                                 # %init_countbits.exit.preheader
	movl	$1, %eax
	.p2align	4, 0x90
.LBB0_3:                                # %init_countbits.exit
                                        # =>This Inner Loop Header: Depth=1
	xorl	%ecx, %ecx
	testb	$1, %al
	jne	.LBB0_21
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	movl	$1, %ecx
	testb	$2, %al
	jne	.LBB0_21
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=1
	movl	$2, %ecx
	testb	$4, %al
	jne	.LBB0_21
# BB#6:                                 #   in Loop: Header=BB0_3 Depth=1
	movl	$3, %ecx
	testb	$8, %al
	jne	.LBB0_21
# BB#7:                                 #   in Loop: Header=BB0_3 Depth=1
	movl	$4, %ecx
	testb	$16, %al
	jne	.LBB0_21
# BB#8:                                 #   in Loop: Header=BB0_3 Depth=1
	movl	$5, %ecx
	testb	$32, %al
	jne	.LBB0_21
# BB#9:                                 #   in Loop: Header=BB0_3 Depth=1
	movl	$6, %ecx
	testb	$64, %al
	jne	.LBB0_21
# BB#10:                                #   in Loop: Header=BB0_3 Depth=1
	testb	%al, %al
	js	.LBB0_11
# BB#12:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$8, %ecx
	testb	$1, %ah
	jne	.LBB0_21
# BB#13:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$9, %ecx
	testb	$2, %ah
	jne	.LBB0_21
# BB#14:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$10, %ecx
	testb	$4, %ah
	jne	.LBB0_21
# BB#15:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$11, %ecx
	testb	$8, %ah
	jne	.LBB0_21
# BB#16:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$12, %ecx
	testb	$16, %ah
	jne	.LBB0_21
# BB#17:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$13, %ecx
	testb	$32, %ah
	jne	.LBB0_21
# BB#18:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$14, %ecx
	testb	$64, %ah
	jne	.LBB0_21
# BB#19:                                #   in Loop: Header=BB0_3 Depth=1
	testw	%ax, %ax
	jns	.LBB0_22
# BB#20:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$15, %ecx
	jmp	.LBB0_21
.LBB0_11:                               #   in Loop: Header=BB0_3 Depth=1
	movl	$7, %ecx
	.p2align	4, 0x90
.LBB0_21:                               # %.sink.split.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%ecx, lastbit16(,%rax,4)
.LBB0_22:                               #   in Loop: Header=BB0_3 Depth=1
	incq	%rax
	cmpq	$65536, %rax            # imm = 0x10000
	jne	.LBB0_3
# BB#23:                                # %init_lastbit.exit
	movl	$100, lastbit16(%rip)
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_24:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_26 Depth 2
	testq	%rax, %rax
	movl	$0, %edx
	je	.LBB0_27
# BB#25:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB0_24 Depth=1
	xorl	%edx, %edx
	movl	%eax, %esi
	.p2align	4, 0x90
.LBB0_26:                               # %.lr.ph.i
                                        #   Parent Loop BB0_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %ecx
	negl	%ecx
	andl	%esi, %ecx
	leal	(%rcx,%rcx), %edi
	orl	%ecx, %edi
	notl	%edi
	incl	%edx
	andl	%edi, %esi
	jne	.LBB0_26
.LBB0_27:                               # %._crit_edge.i
                                        #   in Loop: Header=BB0_24 Depth=1
	movl	%edx, %esi
	orl	$-268435456, %esi       # imm = 0xF0000000
	testw	%cx, %cx
	cmovnsl	%edx, %esi
	movl	%esi, move_table16(,%rax,4)
	incq	%rax
	cmpq	$65536, %rax            # imm = 0x10000
	jne	.LBB0_24
# BB#28:                                # %init_movetable.exit
	retq
.Lfunc_end0:
	.size	init_static_tables, .Lfunc_end0-init_static_tables
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
.LCPI1_1:
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.text
	.globl	init_less_static_tables
	.p2align	4, 0x90
	.type	init_less_static_tables,@function
init_less_static_tables:                # @init_less_static_tables
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
.Lcfi6:
	.cfi_offset %rbx, -56
.Lcfi7:
	.cfi_offset %r12, -48
.Lcfi8:
	.cfi_offset %r13, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movslq	g_board_size(%rip), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	movslq	g_board_size+4(%rip), %r15
	movl	%r15d, %r12d
	xorl	%eax, %eax
	movl	$g_keyinfo+49208, %ecx
	.p2align	4, 0x90
.LBB1_1:                                # %.preheader69
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_2 Depth 2
	movl	$32, %edx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB1_2:                                # %.preheader68
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	$-1, -49208(%rsi)
	movl	$0, -49200(%rsi)
	movl	$-1, -52(%rsi)
	movl	$-1, -56(%rsi)
	movl	$0, -48(%rsi)
	movl	$-1, -49156(%rsi)
	movl	$-1, -49160(%rsi)
	movl	$0, -49152(%rsi)
	movl	$-1, -4(%rsi)
	movl	$-1, -8(%rsi)
	movl	$0, (%rsi)
	addq	$96, %rsi
	addq	$-2, %rdx
	jne	.LBB1_2
# BB#3:                                 #   in Loop: Header=BB1_1 Depth=1
	incq	%rax
	addq	$1536, %rcx             # imm = 0x600
	cmpq	$32, %rax
	jne	.LBB1_1
# BB#4:                                 # %.preheader67
	cmpl	$0, -40(%rsp)           # 4-byte Folded Reload
	jle	.LBB1_22
# BB#5:                                 # %.preheader67
	testl	%r12d, %r12d
	jle	.LBB1_22
# BB#6:                                 # %.preheader66.us.preheader
	movl	-40(%rsp), %eax         # 4-byte Reload
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movl	%r12d, %eax
	andl	$1, %eax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movl	$g_keyinfo+1584, %eax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	xorl	%r13d, %r13d
	movl	$g_keyinfo+50740, %r10d
	movl	$g_keyinfo+1588, %r11d
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_7:                                # %.preheader66.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_16 Depth 2
                                        #     Child Loop BB1_28 Depth 2
	movq	%rax, %rcx
	imulq	%r15, %rcx
	incq	%rax
	cmpq	-40(%rsp), %rax         # 8-byte Folded Reload
	jge	.LBB1_8
# BB#27:                                # %.lr.ph.split.us.us.preheader
                                        #   in Loop: Header=BB1_7 Depth=1
	movq	%rax, %r14
	movq	%r11, %rax
	movq	%r10, %rdx
	movq	%r13, %rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_28:                               # %.lr.ph.split.us.us
                                        #   Parent Loop BB1_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%rbx), %rdi
	cmpq	%r15, %rdi
	jge	.LBB1_29
# BB#30:                                #   in Loop: Header=BB1_28 Depth=2
	addq	%rcx, %rbx
	movl	%esi, -4(%rax)
	leal	1(%rsi), %ebp
	movl	%ebp, (%rax)
	jmp	.LBB1_31
	.p2align	4, 0x90
.LBB1_29:                               # %.lr.ph.split.us.us..backedge.us.us_crit_edge
                                        #   in Loop: Header=BB1_28 Depth=2
	movl	%esi, %ebx
.LBB1_31:                               # %.backedge.us.us
                                        #   in Loop: Header=BB1_28 Depth=2
	movl	%ebx, -4(%rdx)
	leal	(%r12,%rsi), %ebp
	movl	%ebp, (%rdx)
	incq	%rsi
	addq	$1536, %rdx             # imm = 0x600
	addq	$48, %rax
	cmpq	%rdi, %r12
	movq	%rdi, %rbx
	jne	.LBB1_28
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_8:                                # %.lr.ph..lr.ph.split_crit_edge.us.preheader
                                        #   in Loop: Header=BB1_7 Depth=1
	cmpq	$0, -24(%rsp)           # 8-byte Folded Reload
	jne	.LBB1_10
# BB#9:                                 #   in Loop: Header=BB1_7 Depth=1
	movq	%rax, %r14
	xorl	%ecx, %ecx
	cmpl	$1, %r12d
	jne	.LBB1_15
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_10:                               # %.lr.ph..lr.ph.split_crit_edge.us.prol
                                        #   in Loop: Header=BB1_7 Depth=1
	cmpl	$2, %r15d
	jl	.LBB1_11
# BB#12:                                #   in Loop: Header=BB1_7 Depth=1
	movq	%rax, %r14
	shlq	$9, %rax
	movl	%ecx, g_keyinfo+48(%rax,%rax,2)
	incl	%ecx
	movl	%ecx, g_keyinfo+52(%rax,%rax,2)
	jmp	.LBB1_13
.LBB1_11:                               #   in Loop: Header=BB1_7 Depth=1
	movq	%rax, %r14
.LBB1_13:                               # %.lr.ph..lr.ph.split_crit_edge.us.prol.loopexit
                                        #   in Loop: Header=BB1_7 Depth=1
	movl	$1, %ecx
	cmpl	$1, %r12d
	je	.LBB1_21
.LBB1_15:                               # %.lr.ph..lr.ph.split_crit_edge.us.preheader.new
                                        #   in Loop: Header=BB1_7 Depth=1
	movq	%r12, %rax
	subq	%rcx, %rax
	leaq	(%rcx,%r9), %rdx
	leaq	(%rcx,%rcx,2), %rsi
	shlq	$4, %rsi
	addq	-32(%rsp), %rsi         # 8-byte Folded Reload
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB1_16:                               # %.lr.ph..lr.ph.split_crit_edge.us
                                        #   Parent Loop BB1_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%rcx,%r8), %rdi
	cmpq	%r15, %rdi
	jge	.LBB1_18
# BB#17:                                #   in Loop: Header=BB1_16 Depth=2
	leal	(%rdx,%r8), %edi
	movl	%edi, (%rsi)
	leal	1(%rdx,%r8), %edi
	movl	%edi, 4(%rsi)
.LBB1_18:                               # %.backedge.us76
                                        #   in Loop: Header=BB1_16 Depth=2
	leaq	2(%rcx,%r8), %rdi
	cmpq	%r15, %rdi
	jge	.LBB1_20
# BB#19:                                #   in Loop: Header=BB1_16 Depth=2
	leal	1(%rdx,%r8), %edi
	movl	%edi, 48(%rsi)
	leal	2(%rdx,%r8), %edi
	movl	%edi, 52(%rsi)
.LBB1_20:                               # %.backedge.us76.1
                                        #   in Loop: Header=BB1_16 Depth=2
	addq	$2, %r8
	addq	$96, %rsi
	cmpq	%r8, %rax
	jne	.LBB1_16
.LBB1_21:                               # %._crit_edge.us
                                        #   in Loop: Header=BB1_7 Depth=1
	addq	%r15, %r9
	addq	$1536, -32(%rsp)        # 8-byte Folded Spill
                                        # imm = 0x600
	addq	%r12, %r13
	addq	$48, %r10
	addq	$1536, %r11             # imm = 0x600
	movq	%r14, %rax
	cmpq	-16(%rsp), %rax         # 8-byte Folded Reload
	jne	.LBB1_7
.LBB1_22:                               # %.preheader65
	movl	$g_keyinfo, %r13d
	movq	-40(%rsp), %rax         # 8-byte Reload
	decl	%eax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [4294967295,4294967295,0,4294967295]
	movaps	.LCPI1_1(%rip), %xmm1   # xmm1 = [4294967295,0,4294967295,4294967295]
	.p2align	4, 0x90
.LBB1_23:                               # %.preheader64
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_24 Depth 2
                                        #       Child Loop BB1_25 Depth 3
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movq	%r13, -24(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_24:                               # %.preheader
                                        #   Parent Loop BB1_23 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_25 Depth 3
	movq	%rcx, -16(%rsp)         # 8-byte Spill
	movl	$2, %r14d
	movq	%r13, -32(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_25:                               #   Parent Loop BB1_23 Depth=1
                                        #     Parent Loop BB1_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r13), %esi
	cmpl	$-1, %esi
	je	.LBB1_26
# BB#32:                                #   in Loop: Header=BB1_25 Depth=3
	movl	%esi, %eax
	cltd
	idivl	%r12d
	movl	%eax, %ecx
	movl	%esi, %eax
	cltd
	idivl	%r15d
	movl	%edx, %r9d
	movl	4(%r13), %edi
	movl	%edi, %eax
	cltd
	idivl	%r12d
	movl	%eax, %r8d
	movl	%edi, %eax
	cltd
	idivl	%r15d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	-40(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rsi
	subl	%ecx, %eax
	movslq	%ecx, %rbx
	movl	%ecx, %r10d
	imull	%r12d, %r10d
	movl	%esi, %ebp
	subl	%r8d, %ebp
	movslq	%r8d, %rsi
	movl	%r8d, %edi
	imull	%r15d, %edi
	movl	%r15d, %ecx
	subl	%edx, %ecx
	leal	-1(%rcx,%rdi), %edi
	imull	%r15d, %ebp
	leal	-1(%rcx,%rbp), %r8d
	leal	(%rbp,%rdx), %r11d
	shlq	$7, %rsi
	movslq	%edx, %rcx
	movl	g_zobrist+132(%rsi,%rcx,4), %edx
	movl	%r12d, %ecx
	subl	%r9d, %ecx
	leal	-1(%rcx,%r10), %esi
	movl	%esi, 12(%r13)
	movl	%edi, 16(%r13)
	imull	%r12d, %eax
	leal	-1(%rcx,%rax), %r10d
	leal	(%rax,%r9), %ecx
	movl	%ecx, 24(%r13)
	movl	%r11d, 28(%r13)
	shlq	$7, %rbx
	movslq	%r9d, %rax
	xorl	g_zobrist+132(%rbx,%rax,4), %edx
	movl	%edx, 8(%r13)
	movl	%esi, %eax
	cltd
	idivl	%r12d
	movl	%eax, %r9d
	movl	%esi, %eax
	cltd
	idivl	%r15d
	movl	%edx, %esi
	movl	%edi, %eax
	cltd
	idivl	%r12d
	movl	%eax, %ebx
	movl	%edi, %eax
	cltd
	idivl	%r15d
	movslq	%ebx, %rax
	shlq	$7, %rax
	movslq	%edx, %rdx
	movl	g_zobrist+132(%rax,%rdx,4), %eax
	movslq	%r9d, %rdx
	shlq	$7, %rdx
	movslq	%esi, %rsi
	xorl	g_zobrist+132(%rdx,%rsi,4), %eax
	movl	%eax, 20(%r13)
	movl	%ecx, %eax
	cltd
	idivl	%r12d
	movl	%eax, %esi
	movl	%ecx, %eax
	cltd
	idivl	%r15d
	movslq	%esi, %rsi
	shlq	$7, %rsi
	movslq	%edx, %rdi
	movl	%r11d, %eax
	cltd
	idivl	%r12d
	movl	%eax, %ecx
	movl	%r11d, %eax
	cltd
	idivl	%r15d
	movslq	%ecx, %rax
	shlq	$7, %rax
	movslq	%edx, %rcx
	movl	g_zobrist+132(%rax,%rcx,4), %eax
	xorl	g_zobrist+132(%rsi,%rdi,4), %eax
	movl	%eax, 32(%r13)
	movl	%r10d, %eax
	cltd
	idivl	%r12d
	movl	%eax, %ecx
	movl	%r10d, %eax
	cltd
	idivl	%r15d
	movl	%edx, %esi
	movl	%r8d, %eax
	cltd
	idivl	%r12d
	movl	%eax, %edi
	movl	%r8d, %eax
	cltd
	idivl	%r15d
	movslq	%edi, %rax
	shlq	$7, %rax
	movslq	%edx, %rdx
	movl	g_zobrist+132(%rax,%rdx,4), %eax
	movslq	%ecx, %rcx
	shlq	$7, %rcx
	movslq	%esi, %rdx
	xorl	g_zobrist+132(%rcx,%rdx,4), %eax
	jmp	.LBB1_33
	.p2align	4, 0x90
.LBB1_26:                               #   in Loop: Header=BB1_25 Depth=3
	movaps	%xmm0, (%r13)
	movups	%xmm1, 16(%r13)
	movl	$0, 32(%r13)
	xorl	%eax, %eax
	movl	$-1, %r8d
	movl	$-1, %r10d
.LBB1_33:                               # %fill_in_key_entry.exit
                                        #   in Loop: Header=BB1_25 Depth=3
	movl	%r8d, 40(%r13)
	movl	%r10d, 36(%r13)
	movl	%eax, 44(%r13)
	addq	$49152, %r13            # imm = 0xC000
	decq	%r14
	jne	.LBB1_25
# BB#34:                                #   in Loop: Header=BB1_24 Depth=2
	movq	-16(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	movq	-32(%rsp), %r13         # 8-byte Reload
	addq	$48, %r13
	cmpq	$32, %rcx
	jne	.LBB1_24
# BB#35:                                #   in Loop: Header=BB1_23 Depth=1
	movq	-8(%rsp), %rax          # 8-byte Reload
	incq	%rax
	movq	-24(%rsp), %r13         # 8-byte Reload
	addq	$1536, %r13             # imm = 0x600
	cmpq	$32, %rax
	jne	.LBB1_23
# BB#36:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	init_less_static_tables, .Lfunc_end1-init_less_static_tables
	.cfi_endproc

	.type	g_keyinfo,@object       # @g_keyinfo
	.comm	g_keyinfo,98304,16
	.type	countbits16,@object     # @countbits16
	.comm	countbits16,262144,16
	.type	lastbit16,@object       # @lastbit16
	.comm	lastbit16,262144,16
	.type	move_table16,@object    # @move_table16
	.comm	move_table16,262144,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
