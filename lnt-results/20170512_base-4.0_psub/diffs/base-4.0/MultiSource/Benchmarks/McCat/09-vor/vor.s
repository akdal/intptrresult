	.text
	.file	"vor.bc"
	.globl	clean_up
	.p2align	4, 0x90
	.type	clean_up,@function
clean_up:                               # @clean_up
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movq	K(%rip), %rdi
	callq	free
	movq	E(%rip), %rdi
	callq	free
	movl	$1, Kcount(%rip)
	movl	$1, Ecount(%rip)
	popq	%rax
	retq
.Lfunc_end0:
	.size	clean_up, .Lfunc_end0-clean_up
	.cfi_endproc

	.globl	getpoint
	.p2align	4, 0x90
	.type	getpoint,@function
getpoint:                               # @getpoint
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	shlq	$32, %rsi
	movl	%edi, %eax
	orq	%rax, %rsi
	movl	$Splaytree, %edi
	jmp	insert                  # TAILCALL
.Lfunc_end1:
	.size	getpoint, .Lfunc_end1-getpoint
	.cfi_endproc

	.globl	get_file
	.p2align	4, 0x90
	.type	get_file,@function
get_file:                               # @get_file
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -32
.Lcfi6:
	.cfi_offset %r14, -24
.Lcfi7:
	.cfi_offset %rbp, -16
	callq	init
	movq	%rax, Splaytree(%rip)
	movq	stdin(%rip), %rdi
	leaq	4(%rsp), %rdx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	callq	fscanf
	cmpl	$0, 4(%rsp)
	jle	.LBB2_3
# BB#1:                                 # %.lr.ph.preheader
	leaq	12(%rsp), %r14
	leaq	8(%rsp), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	stdin(%rip), %rdi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r14, %rdx
	movq	%rbx, %rcx
	callq	fscanf
	movl	8(%rsp), %eax
	shlq	$32, %rax
	movl	12(%rsp), %esi
	orq	%rax, %rsi
	movl	$Splaytree, %edi
	callq	insert
	incl	%ebp
	cmpl	4(%rsp), %ebp
	jl	.LBB2_2
.LBB2_3:                                # %._crit_edge
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	get_file, .Lfunc_end2-get_file
	.cfi_endproc

	.globl	add_point
	.p2align	4, 0x90
	.type	add_point,@function
add_point:                              # @add_point
	.cfi_startproc
# BB#0:
	movq	K(%rip), %rax
	movslq	Kcount(%rip), %rcx
	leal	1(%rcx), %edx
	shlq	$2, %rcx
	leaq	(%rcx,%rcx,4), %rcx
	movq	%rdi, (%rax,%rcx)
	movq	K(%rip), %rax
	movl	$0, 8(%rax,%rcx)
	movl	$0, 12(%rax,%rcx)
	movl	$0, 16(%rax,%rcx)
	movl	%edx, Kcount(%rip)
	retq
.Lfunc_end3:
	.size	add_point, .Lfunc_end3-add_point
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	-4556648864387432448    # double -1.0E+4
.LCPI4_1:
	.quad	4666723172467343360     # double 1.0E+4
	.text
	.globl	compute_v
	.p2align	4, 0x90
	.type	compute_v,@function
compute_v:                              # @compute_v
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 64
.Lcfi15:
	.cfi_offset %rbx, -56
.Lcfi16:
	.cfi_offset %r12, -48
.Lcfi17:
	.cfi_offset %r13, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movl	4(%rdi), %ebp
	movl	8(%rdi), %r13d
	callq	next
	movl	4(%rax), %r15d
	movl	8(%rax), %r14d
	movq	%r13, %r12
	shlq	$32, %r12
	orq	%rbp, %r12
	movq	%r14, %rbx
	shlq	$32, %rbx
	orq	%r15, %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	vector
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rbx
	callq	midpoint
	movq	%rbx, %rdi
	callq	calculate_c
	cmpl	%r15d, %ebp
	jge	.LBB4_9
# BB#1:
	cmpl	%r14d, %r13d
	jge	.LBB4_9
# BB#2:
	movsd	.LCPI4_1(%rip), %xmm1   # xmm1 = mem[0],zero
.LBB4_3:
	movq	%rbx, %rdi
	movabsq	$4294967296, %rsi       # imm = 0x100000000
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	callq	intersect
	movsd	.LCPI4_0(%rip), %xmm2   # xmm2 = mem[0],zero
	ucomisd	%xmm0, %xmm2
	ja	.LBB4_5
# BB#4:
	ucomisd	.LCPI4_1(%rip), %xmm0
	jbe	.LBB4_8
.LBB4_5:
	movsd	.LCPI4_0(%rip), %xmm1   # xmm1 = mem[0],zero
	jmp	.LBB4_6
.LBB4_9:
	cmpl	%r15d, %ebp
	jge	.LBB4_13
# BB#10:
	cmpl	%r14d, %r13d
	jne	.LBB4_13
# BB#11:
	movsd	.LCPI4_1(%rip), %xmm1   # xmm1 = mem[0],zero
.LBB4_12:
	movq	%rbx, %rdi
	movabsq	$4294967296, %rsi       # imm = 0x100000000
	jmp	.LBB4_7
.LBB4_13:
	cmpl	%r15d, %ebp
	jge	.LBB4_19
# BB#14:
	cmpl	%r14d, %r13d
	jle	.LBB4_19
# BB#15:
	movsd	.LCPI4_1(%rip), %xmm1   # xmm1 = mem[0],zero
.LBB4_16:
	movq	%rbx, %rdi
	movabsq	$4294967296, %rsi       # imm = 0x100000000
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	callq	intersect
	movsd	.LCPI4_0(%rip), %xmm2   # xmm2 = mem[0],zero
	ucomisd	%xmm0, %xmm2
	ja	.LBB4_18
# BB#17:
	ucomisd	.LCPI4_1(%rip), %xmm0
	jbe	.LBB4_8
.LBB4_18:
	movsd	.LCPI4_1(%rip), %xmm1   # xmm1 = mem[0],zero
.LBB4_6:
	movl	$1, %esi
	movq	%rbx, %rdi
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
.LBB4_7:
	callq	intersect
.LBB4_8:
	cvttsd2si	%xmm0, %eax
	cvttsd2si	%xmm1, %ecx
	shlq	$32, %rcx
	orq	%rcx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_19:
	cmpl	%r15d, %ebp
	jne	.LBB4_23
# BB#20:
	cmpl	%r14d, %r13d
	jge	.LBB4_23
# BB#21:
	movsd	.LCPI4_0(%rip), %xmm1   # xmm1 = mem[0],zero
	jmp	.LBB4_22
.LBB4_23:
	cmpl	%r15d, %ebp
	jne	.LBB4_26
# BB#24:
	cmpl	%r14d, %r13d
	jle	.LBB4_26
# BB#25:
	movsd	.LCPI4_1(%rip), %xmm1   # xmm1 = mem[0],zero
.LBB4_22:
	movl	$1, %esi
	movq	%rbx, %rdi
	jmp	.LBB4_7
.LBB4_26:
	cmpl	%r15d, %ebp
	jle	.LBB4_29
# BB#27:
	cmpl	%r14d, %r13d
	jge	.LBB4_29
# BB#28:
	movsd	.LCPI4_0(%rip), %xmm1   # xmm1 = mem[0],zero
	jmp	.LBB4_3
.LBB4_29:
	cmpl	%r15d, %ebp
	jle	.LBB4_32
# BB#30:
	cmpl	%r14d, %r13d
	jne	.LBB4_32
# BB#31:
	movsd	.LCPI4_0(%rip), %xmm1   # xmm1 = mem[0],zero
	jmp	.LBB4_12
.LBB4_32:
	cmpl	%r15d, %ebp
	jle	.LBB4_35
# BB#33:
	cmpl	%r14d, %r13d
	jle	.LBB4_35
# BB#34:
	movsd	.LCPI4_0(%rip), %xmm1   # xmm1 = mem[0],zero
	jmp	.LBB4_16
.LBB4_35:
	movl	$.Lstr, %edi
	callq	puts
	movl	$1, %edi
	callq	exit
.Lfunc_end4:
	.size	compute_v, .Lfunc_end4-compute_v
	.cfi_endproc

	.globl	add_infinit_points_to_K
	.p2align	4, 0x90
	.type	add_infinit_points_to_K,@function
add_infinit_points_to_K:                # @add_infinit_points_to_K
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -24
.Lcfi25:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movslq	CHno(%rip), %rax
	leaq	(%rax,%rax), %rbx
	leaq	-1(%rax,%rax), %rdi
	movl	$20, %esi
	callq	calloc
	movq	%rax, K(%rip)
	addl	$-2, %ebx
	movslq	%ebx, %rdi
	movl	$32, %esi
	callq	calloc
	movq	%rax, E(%rip)
	callq	CHinit
	movq	%rax, CHSplaytree(%rip)
	movq	16(%r14), %rbx
	movq	%r14, %rdi
	callq	compute_v
	movq	K(%rip), %rcx
	movslq	Kcount(%rip), %rdx
	movq	%rdx, %rsi
	shlq	$2, %rsi
	leaq	(%rsi,%rsi,4), %rsi
	movq	%rax, (%rcx,%rsi)
	movq	K(%rip), %rax
	movl	$0, 8(%rax,%rsi)
	movl	$0, 12(%rax,%rsi)
	movl	$0, 16(%rax,%rsi)
	leal	1(%rdx), %eax
	movl	%eax, Kcount(%rip)
	movl	%edx, 12(%r14)
	movl	$CHSplaytree, %edi
	movq	%r14, %rsi
	callq	CHinsert
	cmpq	%r14, %rbx
	je	.LBB5_3
	.p2align	4, 0x90
.LBB5_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	compute_v
	movq	K(%rip), %rcx
	movslq	Kcount(%rip), %rdx
	movq	%rdx, %rsi
	shlq	$2, %rsi
	leaq	(%rsi,%rsi,4), %rsi
	movq	%rax, (%rcx,%rsi)
	movq	K(%rip), %rax
	movl	$0, 8(%rax,%rsi)
	movl	$0, 12(%rax,%rsi)
	movl	$0, 16(%rax,%rsi)
	leal	1(%rdx), %eax
	movl	%eax, Kcount(%rip)
	movl	%edx, 12(%rbx)
	movl	$CHSplaytree, %edi
	movq	%rbx, %rsi
	callq	CHinsert
	movq	16(%rbx), %rbx
	cmpq	%r14, %rbx
	jne	.LBB5_1
.LBB5_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	add_infinit_points_to_K, .Lfunc_end5-add_infinit_points_to_K
	.cfi_endproc

	.globl	add_edge
	.p2align	4, 0x90
	.type	add_edge,@function
add_edge:                               # @add_edge
	.cfi_startproc
# BB#0:
	movq	E(%rip), %r8
	movslq	Ecount(%rip), %rax
	shlq	$5, %rax
	movl	%edi, (%r8,%rax)
	movl	Ecount(%rip), %eax
	movslq	%eax, %r9
	shlq	$5, %r9
	movl	%esi, 4(%r8,%r9)
	movq	K(%rip), %r10
	movslq	%edi, %rcx
	leaq	(%rcx,%rcx,4), %rcx
	movslq	8(%r10,%rcx,4), %rdx
	testq	%rdx, %rdx
	je	.LBB6_1
# BB#2:
	cmpl	$0, 12(%r10,%rcx,4)
	je	.LBB6_3
# BB#6:
	leaq	16(%r10,%rcx,4), %rcx
	jmp	.LBB6_7
.LBB6_1:
	leaq	8(%r10,%rcx,4), %rcx
	movl	%eax, (%rcx)
	movl	%eax, 8(%r8,%r9)
	leaq	16(%r8,%r9), %rcx
	jmp	.LBB6_7
.LBB6_3:
	leaq	12(%r10,%rcx,4), %rcx
	movl	%eax, (%rcx)
	movl	%edx, 8(%r8,%r9)
	movl	%edx, 16(%r8,%r9)
	shlq	$5, %rdx
	cmpl	%edi, (%r8,%rdx)
	jne	.LBB6_5
# BB#4:
	movl	%eax, 8(%r8,%rdx)
	leaq	16(%r8,%rdx), %rcx
	jmp	.LBB6_7
.LBB6_5:
	movl	%eax, 12(%r8,%rdx)
	leaq	20(%r8,%rdx), %rcx
.LBB6_7:
	movl	%eax, (%rcx)
	movslq	%esi, %rcx
	leaq	(%rcx,%rcx,4), %rcx
	movslq	8(%r10,%rcx,4), %rdx
	testq	%rdx, %rdx
	je	.LBB6_8
# BB#9:
	cmpl	$0, 12(%r10,%rcx,4)
	je	.LBB6_10
# BB#13:
	leaq	16(%r10,%rcx,4), %rcx
	jmp	.LBB6_14
.LBB6_8:
	leaq	8(%r10,%rcx,4), %rcx
	movl	%eax, (%rcx)
	movl	%eax, 8(%r8,%r9)
	leaq	16(%r8,%r9), %rcx
	jmp	.LBB6_14
.LBB6_10:
	leaq	12(%r10,%rcx,4), %rcx
	movl	%eax, (%rcx)
	movl	%edx, 8(%r8,%r9)
	movl	%edx, 16(%r8,%r9)
	shlq	$5, %rdx
	cmpl	%esi, (%r8,%rdx)
	jne	.LBB6_12
# BB#11:
	movl	%eax, 8(%r8,%rdx)
	leaq	16(%r8,%rdx), %rcx
	jmp	.LBB6_14
.LBB6_12:
	movl	%eax, 12(%r8,%rdx)
	leaq	20(%r8,%rdx), %rcx
.LBB6_14:
	movl	%eax, (%rcx)
	incl	%eax
	movl	%eax, Ecount(%rip)
	retq
.Lfunc_end6:
	.size	add_edge, .Lfunc_end6-add_edge
	.cfi_endproc

	.globl	maximize_radius_and_angle
	.p2align	4, 0x90
	.type	maximize_radius_and_angle,@function
maximize_radius_and_angle:              # @maximize_radius_and_angle
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 32
	subq	$80, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 112
.Lcfi30:
	.cfi_offset %rbx, -32
.Lcfi31:
	.cfi_offset %r14, -24
.Lcfi32:
	.cfi_offset %r15, -16
	movl	$CHSplaytree, %edi
	callq	CHdelete_max
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	before
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	next
	movq	%rax, %r15
	movq	%rbx, %rdi
	callq	before
	movq	4(%rax), %rdi
	movq	4(%rbx), %rsi
	movq	4(%r14), %rdx
	callq	centre
	movq	4(%rbx), %rdi
	callq	radius2
	movsd	%xmm0, 56(%rsp)
	movq	%rbx, %rdi
	callq	before
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	angle
	movsd	%xmm0, 64(%rsp)
	movl	(%rbx), %eax
	movl	%eax, 72(%rsp)
	movq	72(%rsp), %rax
	movq	%rax, 16(%rsp)
	movupd	56(%rsp), %xmm0
	movupd	%xmm0, (%rsp)
	movl	$CHSplaytree, %edi
	callq	CHdelete
	movq	%r15, %rdi
	callq	next
	movq	4(%r14), %rdi
	movq	4(%r15), %rsi
	movq	4(%rax), %rdx
	callq	centre
	movq	4(%r15), %rdi
	callq	radius2
	movsd	%xmm0, 32(%rsp)
	movq	%r15, %rdi
	callq	next
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	angle
	movsd	%xmm0, 40(%rsp)
	movl	(%r15), %eax
	movl	%eax, 48(%rsp)
	movq	48(%rsp), %rax
	movq	%rax, 16(%rsp)
	movups	32(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	movl	$CHSplaytree, %edi
	callq	CHdelete
	movq	%r14, %rax
	addq	$80, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	maximize_radius_and_angle, .Lfunc_end7-maximize_radius_and_angle
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI8_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	draw_sec
	.p2align	4, 0x90
	.type	draw_sec,@function
draw_sec:                               # @draw_sec
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi38:
	.cfi_def_cfa_offset 80
.Lcfi39:
	.cfi_offset %rbx, -48
.Lcfi40:
	.cfi_offset %r12, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	before
	movq	4(%rax), %rdi
	movq	4(%r14), %rsi
	callq	length2
	movl	%eax, %ebx
	movq	%r14, %rdi
	callq	next
	movq	4(%r14), %rdi
	movq	4(%rax), %rsi
	callq	length2
	cmpl	%eax, %ebx
	jle	.LBB8_3
# BB#1:
	movq	%r14, %rdi
	callq	before
	movq	4(%rax), %rdi
	movq	4(%r14), %rsi
	callq	length2
	movl	%eax, %ebp
	movq	%r14, %rdi
	callq	before
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	next
	movq	4(%rbx), %rdi
	movq	4(%rax), %rsi
	callq	length2
	cmpl	%eax, %ebp
	jle	.LBB8_3
# BB#2:
	movq	%r14, %rdi
	callq	next
	jmp	.LBB8_6
.LBB8_3:
	movq	%r14, %rdi
	callq	next
	movq	4(%r14), %rdi
	movq	4(%rax), %rsi
	callq	length2
	movl	%eax, %ebx
	movq	%r14, %rdi
	callq	before
	movq	%rax, %rbp
	movq	%r14, %rdi
	callq	next
	movq	4(%rbp), %rdi
	movq	4(%rax), %rsi
	callq	length2
	cmpl	%eax, %ebx
	movq	%r14, %rbx
	jle	.LBB8_7
# BB#4:
	movq	%r14, %rdi
	callq	next
	movq	4(%r14), %rdi
	movq	4(%rax), %rsi
	callq	length2
	movl	%eax, %ebx
	movq	%r14, %rdi
	callq	before
	movq	4(%r14), %rdi
	movq	4(%rax), %rsi
	callq	length2
	cmpl	%eax, %ebx
	movq	%r14, %rbx
	jle	.LBB8_7
# BB#5:
	movq	%r14, %rdi
	callq	before
.LBB8_6:
	movq	%rax, %rbx
.LBB8_7:
	movq	%rbx, %rdi
	callq	before
	movq	%rax, %r15
	movq	%rbx, %rdi
	callq	next
	movq	%rax, %r12
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	callq	angle
	xorpd	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	jbe	.LBB8_11
# BB#8:
	movq	4(%r15), %rdi
	movq	4(%r12), %rsi
	callq	midpoint
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movq	4(%r15), %rdi
	movq	4(%r12), %rsi
	callq	midpoint
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movq	4(%r15), %rdi
	movq	4(%r12), %rsi
	callq	length2
	cvtsi2sdl	%eax, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB8_10
# BB#9:                                 # %call.sqrt
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB8_10:                               # %.split
	mulsd	.LCPI8_0(%rip), %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	jmp	.LBB8_14
.LBB8_11:
	movq	4(%r15), %rdi
	movq	4(%rbx), %rsi
	movq	4(%r12), %rdx
	callq	centre
	movq	4(%r14), %rdi
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	callq	radius2
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jp	.LBB8_13
# BB#12:
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	jmp	.LBB8_14
.LBB8_13:                               # %call.sqrt52
	callq	sqrt
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
.LBB8_14:
	cvttsd2si	24(%rsp), %esi  # 8-byte Folded Reload
	cvttsd2si	16(%rsp), %edx  # 8-byte Folded Reload
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.4, %edi
	movb	$1, %al
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	printf                  # TAILCALL
.Lfunc_end8:
	.size	draw_sec, .Lfunc_end8-draw_sec
	.cfi_endproc

	.globl	alg2
	.p2align	4, 0x90
	.type	alg2,@function
alg2:                                   # @alg2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -32
.Lcfi48:
	.cfi_offset %r14, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movq	S(%rip), %rdi
	callq	add_infinit_points_to_K
	cmpl	$3, CHno(%rip)
	jl	.LBB9_5
	.p2align	4, 0x90
.LBB9_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	callq	maximize_radius_and_angle
	movq	%rax, %r14
	cmpl	$3, CHno(%rip)
	jne	.LBB9_3
# BB#2:                                 #   in Loop: Header=BB9_1 Depth=1
	movq	%r14, %rdi
	callq	draw_sec
.LBB9_3:                                #   in Loop: Header=BB9_1 Depth=1
	movq	%r14, %rdi
	callq	before
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	next
	movq	4(%rbx), %rdi
	movq	4(%r14), %rsi
	movq	4(%rax), %rdx
	callq	centre
	cvttsd2si	%xmm0, %eax
	cvttsd2si	%xmm1, %ecx
	shlq	$32, %rcx
	orq	%rcx, %rax
	movq	K(%rip), %rcx
	movslq	Kcount(%rip), %rdi
	movq	%rdi, %rdx
	shlq	$2, %rdx
	leaq	(%rdx,%rdx,4), %rdx
	movq	%rax, (%rcx,%rdx)
	movq	K(%rip), %rax
	movl	$0, 8(%rax,%rdx)
	movl	$0, 12(%rax,%rdx)
	movl	$0, 16(%rax,%rdx)
	leal	1(%rdi), %eax
	movl	%eax, Kcount(%rip)
	movl	12(%r14), %esi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	add_edge
	movl	Kcount(%rip), %edi
	decl	%edi
	movl	12(%rbx), %esi
	callq	add_edge
	movl	Kcount(%rip), %eax
	decl	%eax
	movl	%eax, 12(%rbx)
	movq	%r14, %rdi
	callq	next
	movq	%rax, 16(%rbx)
	movq	%rbx, %rdi
	callq	next
	movq	%rbx, 24(%rax)
	movq	%rbx, S(%rip)
	decl	CHno(%rip)
	movq	%r14, %rdi
	callq	next
	movl	$CHSplaytree, %edi
	movq	%rax, %rsi
	callq	CHinsert
	movq	%r14, %rdi
	callq	before
	movl	$CHSplaytree, %edi
	movq	%rax, %rsi
	callq	CHinsert
	cmpl	$2, CHno(%rip)
	jg	.LBB9_1
# BB#4:                                 # %._crit_edge
	movl	12(%rbx), %ebp
	movq	%rbx, %rdi
	callq	next
	movl	12(%rax), %esi
	movl	%ebp, %edi
	jmp	.LBB9_6
.LBB9_5:
	movl	$2, CHno(%rip)
	movq	S(%rip), %rdi
	movl	12(%rdi), %ebx
	callq	next
	movl	12(%rax), %esi
	movl	%ebx, %edi
.LBB9_6:
	callq	add_edge
	movq	S(%rip), %rdi
	callq	free
	movq	CHSplaytree(%rip), %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	CHfree_tree             # TAILCALL
.Lfunc_end9:
	.size	alg2, .Lfunc_end9-alg2
	.cfi_endproc

	.globl	construct_vor
	.p2align	4, 0x90
	.type	construct_vor,@function
construct_vor:                          # @construct_vor
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 16
	xorl	%eax, %eax
	callq	construct_ch
	movq	%rax, S(%rip)
	movq	%rax, %rdi
	callq	number_points
	callq	alg2
	movq	K(%rip), %rdi
	callq	free
	movq	E(%rip), %rdi
	callq	free
	movl	$1, Kcount(%rip)
	movl	$1, Ecount(%rip)
	popq	%rax
	retq
.Lfunc_end10:
	.size	construct_vor, .Lfunc_end10-construct_vor
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi54:
	.cfi_def_cfa_offset 48
.Lcfi55:
	.cfi_offset %rbx, -32
.Lcfi56:
	.cfi_offset %r14, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	callq	init
	movq	%rax, Splaytree(%rip)
	movq	stdin(%rip), %rdi
	leaq	4(%rsp), %rdx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	callq	fscanf
	cmpl	$0, 4(%rsp)
	jle	.LBB11_3
# BB#1:                                 # %.lr.ph.i.preheader
	leaq	12(%rsp), %r14
	leaq	8(%rsp), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	stdin(%rip), %rdi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r14, %rdx
	movq	%rbx, %rcx
	callq	fscanf
	movl	8(%rsp), %eax
	shlq	$32, %rax
	movl	12(%rsp), %esi
	orq	%rax, %rsi
	movl	$Splaytree, %edi
	callq	insert
	incl	%ebp
	cmpl	4(%rsp), %ebp
	jl	.LBB11_2
.LBB11_3:                               # %get_file.exit
	xorl	%eax, %eax
	callq	construct_ch
	movq	%rax, S(%rip)
	movq	%rax, %rdi
	callq	number_points
	callq	alg2
	movq	K(%rip), %rdi
	callq	free
	movq	E(%rip), %rdi
	callq	free
	movl	$1, Kcount(%rip)
	movl	$1, Ecount(%rip)
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end11:
	.size	main, .Lfunc_end11-main
	.cfi_endproc

	.type	CHno,@object            # @CHno
	.bss
	.globl	CHno
	.p2align	2
CHno:
	.long	0                       # 0x0
	.size	CHno, 4

	.type	Kcount,@object          # @Kcount
	.data
	.globl	Kcount
	.p2align	2
Kcount:
	.long	1                       # 0x1
	.size	Kcount, 4

	.type	Ecount,@object          # @Ecount
	.globl	Ecount
	.p2align	2
Ecount:
	.long	1                       # 0x1
	.size	Ecount, 4

	.type	K,@object               # @K
	.comm	K,8,8
	.type	E,@object               # @E
	.comm	E,8,8
	.type	Splaytree,@object       # @Splaytree
	.comm	Splaytree,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d"
	.size	.L.str, 3

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%d %d\n"
	.size	.L.str.1, 7

	.type	CHSplaytree,@object     # @CHSplaytree
	.comm	CHSplaytree,8,8
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"The center is (%d,%d)\n"
	.size	.L.str.3, 23

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"The radius is %9.2f\n"
	.size	.L.str.4, 21

	.type	S,@object               # @S
	.comm	S,8,8
	.type	default_radius,@object  # @default_radius
	.comm	default_radius,4,4
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Error: Can't intersect"
	.size	.Lstr, 23


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
