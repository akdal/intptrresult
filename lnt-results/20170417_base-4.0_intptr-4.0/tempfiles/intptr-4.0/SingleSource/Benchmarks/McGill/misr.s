	.text
	.file	"misr.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4598175219545276416     # double 0.25
.LCPI0_1:
	.quad	4681608360884174848     # double 1.0E+5
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$136, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 176
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movl	$10, reg_len(%rip)
	cmpl	$6, %ebp
	jle	.LBB0_3
# BB#1:
	movq	48(%rbx), %rsi
	leaq	32(%rsp), %rdi
	callq	strcpy
	cmpl	$7, %ebp
	je	.LBB0_4
# BB#19:
	movq	56(%rbx), %rdi
	leaq	10(%rsp), %rdx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	callq	sscanf
	cmpl	$8, %ebp
	je	.LBB0_5
# BB#20:
	movq	64(%rbx), %rdi
	leaq	12(%rsp), %rdx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	callq	sscanf
	cmpl	$10, %ebp
	jl	.LBB0_6
# BB#21:
	movq	72(%rbx), %rdi
	leaq	14(%rsp), %rdx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	callq	sscanf
	jmp	.LBB0_7
.LBB0_3:                                # %.thread52
	movabsq	$3472328296227680304, %rax # imm = 0x3030303030303030
	movq	%rax, 33(%rsp)
	movb	$48, 41(%rsp)
	movb	$49, 32(%rsp)
	movb	$0, 42(%rsp)
.LBB0_4:                                # %.thread47
	movw	$1, 10(%rsp)
.LBB0_5:                                # %.thread47
	movw	$0, 12(%rsp)
.LBB0_6:
	movw	$0, 14(%rsp)
.LBB0_7:
	movslq	reg_len(%rip), %rbx
	cmpq	$101, %rbx
	jl	.LBB0_9
# BB#8:
	movl	$.L.str.1, %edi
	movl	$100, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$2, %ebx
	jmp	.LBB0_23
.LBB0_9:
	leaq	32(%rsp), %rdi
	callq	strlen
	cmpq	%rbx, %rax
	jne	.LBB0_22
# BB#10:
	leaq	10(%rsp), %rdi
	callq	seed48
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	reg_len(%rip), %ebx
	testl	%ebx, %ebx
	js	.LBB0_13
# BB#11:                                # %.lr.ph.i.preheader
	incl	%ebx
	leaq	16(%rsp), %rbp
	.p2align	4, 0x90
.LBB0_12:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$16, %edi
	callq	malloc
	movl	$1, (%rax)
	movl	$1, 4(%rax)
	movq	$0, 8(%rax)
	movq	%rax, 8(%rbp)
	decl	%ebx
	movq	%rax, %rbp
	jne	.LBB0_12
.LBB0_13:                               # %create_link_list.exit.preheader
	xorl	%ebx, %ebx
	leaq	16(%rsp), %r15
	leaq	32(%rsp), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_14:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_16 Depth 2
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_17
# BB#15:                                # %.lr.ph.i46.preheader
                                        #   in Loop: Header=BB0_14 Depth=1
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB0_16:                               # %.lr.ph.i46
                                        #   Parent Loop BB0_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	movl	%edx, 4(%rcx)
	movq	%rax, %rcx
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.LBB0_16
.LBB0_17:                               # %init.exit
                                        #   in Loop: Header=BB0_14 Depth=1
	movl	$10, %edi
	movq	%r15, %rsi
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	%r14, %rdx
	callq	simulate
	addl	%eax, %ebp
	incl	%ebx
	cmpl	$100000, %ebx           # imm = 0x186A0
	jne	.LBB0_14
# BB#18:
	xorl	%ebx, %ebx
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	printf
	movl	reg_len(%rip), %esi
	movzwl	10(%rsp), %r9d
	movzwl	12(%rsp), %r10d
	movzwl	14(%rsp), %r11d
	movl	$100000, %eax           # imm = 0x186A0
	subl	%ebp, %eax
	cvtsi2sdl	%eax, %xmm1
	divsd	.LCPI0_1(%rip), %xmm1
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	leaq	32(%rsp), %r8
	movl	$.L.str.5, %edi
	movl	$10, %edx
	movl	$100000, %ecx           # imm = 0x186A0
	movb	$2, %al
	pushq	%r11
.Lcfi9:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi10:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$16, %rsp
.Lcfi11:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB0_23
.LBB0_22:
	movl	$.Lstr, %edi
	callq	puts
	movl	$4, %ebx
.LBB0_23:
	movl	%ebx, %eax
	addq	$136, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.globl	create_link_list
	.p2align	4, 0x90
	.type	create_link_list,@function
create_link_list:                       # @create_link_list
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movl	reg_len(%rip), %r14d
	testl	%r14d, %r14d
	js	.LBB1_3
# BB#1:                                 # %.lr.ph.preheader
	movl	$-1, %ebp
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$16, %edi
	callq	malloc
	movl	$1, (%rax)
	movl	$1, 4(%rax)
	movq	$0, 8(%rax)
	movq	%rax, 8(%rbx)
	incl	%ebp
	cmpl	%r14d, %ebp
	movq	%rax, %rbx
	jl	.LBB1_2
.LBB1_3:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	create_link_list, .Lfunc_end1-create_link_list
	.cfi_endproc

	.globl	init
	.p2align	4, 0x90
	.type	init,@function
init:                                   # @init
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.LBB2_3
	.p2align	4, 0x90
.LBB2_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi), %ecx
	movl	%ecx, 4(%rdi)
	movq	%rax, %rdi
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.LBB2_1
.LBB2_3:                                # %._crit_edge
	retq
.Lfunc_end2:
	.size	init, .Lfunc_end2-init
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4652007308841189376     # double 1000
.LCPI3_1:
	.quad	4666723172467343360     # double 1.0E+4
	.text
	.globl	simulate
	.p2align	4, 0x90
	.type	simulate,@function
simulate:                               # @simulate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 160
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movq	%rsi, %r13
	movl	reg_len(%rip), %ecx
	movl	%edi, 44(%rsp)          # 4-byte Spill
	testl	%edi, %edi
	jle	.LBB3_16
# BB#1:                                 # %.preheader114.lr.ph
	leal	-1(%rcx), %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	cltq
	imulq	$-2078209981, %rax, %rax # imm = 0x84210843
	shrq	$32, %rax
	leal	-1(%rax,%rcx), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	sarl	$4, %eax
	addl	%edx, %eax
	movl	%eax, %edx
	shll	$5, %edx
	subl	%eax, %edx
	movslq	%edx, %rsi
	negl	%edx
	leal	-1(%rcx,%rdx), %ecx
	cltq
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	movl	%ecx, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	addq	%r12, %rsi
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%r13, 72(%rsp)          # 8-byte Spill
	movq	%r12, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_2:                                # %.preheader114
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_4 Depth 2
                                        #       Child Loop BB3_5 Depth 3
                                        #     Child Loop BB3_10 Depth 2
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, (%rsp)           # 16-byte Spill
	callq	lrand48
	movq	%rax, %rbp
	cmpl	$31, 40(%rsp)           # 4-byte Folded Reload
	jl	.LBB3_9
# BB#3:                                 # %.preheader112.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, (%rsp)           # 16-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_4:                                # %.preheader112
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_5 Depth 3
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	movl	$31, %r15d
	movq	%r12, %rbx
	movq	%r13, %r14
	.p2align	4, 0x90
.LBB3_5:                                #   Parent Loop BB3_2 Depth=1
                                        #     Parent Loop BB3_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$49, (%rbx)
	jne	.LBB3_7
# BB#6:                                 #   in Loop: Header=BB3_5 Depth=3
	movq	(%r14), %xmm0           # xmm0 = mem[0],zero
	pshufd	$212, %xmm0, %xmm0      # xmm0 = xmm0[0,1,1,3]
	movdqa	(%rsp), %xmm1           # 16-byte Reload
	paddq	%xmm0, %xmm1
	movdqa	%xmm1, (%rsp)           # 16-byte Spill
.LBB3_7:                                # %._crit_edge151
                                        #   in Loop: Header=BB3_5 Depth=3
	movq	8(%r14), %rax
	movl	(%rax), %eax
	addl	%ebp, %eax
	andl	$1, %eax
	movl	%eax, (%r14)
	callq	lrand48
	movq	%rax, %rcx
	movabsq	$2361183241434822607, %rdx # imm = 0x20C49BA5E353F7CF
	imulq	%rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$7, %rdx
	addq	%rax, %rdx
	imulq	$1000, %rdx, %rax       # imm = 0x3E8
	subq	%rax, %rcx
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rcx, %xmm0
	divsd	.LCPI3_0(%rip), %xmm0
	xorl	%eax, %eax
	movq	24(%rsp), %xmm1         # 8-byte Folded Reload
                                        # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	seta	%al
	xorl	%ebp, %eax
	movq	8(%r14), %r13
	addl	4(%r13), %eax
	andl	$1, %eax
	movl	%eax, 4(%r14)
	sarq	%rbp
	incq	%rbx
	decq	%r15
	movq	%r13, %r14
	jne	.LBB3_5
# BB#8:                                 #   in Loop: Header=BB3_4 Depth=2
	movq	96(%rsp), %rbx          # 8-byte Reload
	incq	%rbx
	callq	lrand48
	movq	%rax, %rbp
	addq	$31, %r12
	cmpq	88(%rsp), %rbx          # 8-byte Folded Reload
	jl	.LBB3_4
.LBB3_9:                                # %.preheader113
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	jle	.LBB3_13
	.p2align	4, 0x90
.LBB3_10:                               # %.lr.ph136
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$49, (%rbx)
	jne	.LBB3_12
# BB#11:                                #   in Loop: Header=BB3_10 Depth=2
	movq	(%r13), %xmm0           # xmm0 = mem[0],zero
	pshufd	$212, %xmm0, %xmm0      # xmm0 = xmm0[0,1,1,3]
	movdqa	(%rsp), %xmm1           # 16-byte Reload
	paddq	%xmm0, %xmm1
	movdqa	%xmm1, (%rsp)           # 16-byte Spill
.LBB3_12:                               # %.lr.ph136._crit_edge
                                        #   in Loop: Header=BB3_10 Depth=2
	movq	8(%r13), %rax
	movl	(%rax), %eax
	addl	%ebp, %eax
	andl	$1, %eax
	movl	%eax, (%r13)
	callq	lrand48
	movq	%rax, %rcx
	movabsq	$2361183241434822607, %rdx # imm = 0x20C49BA5E353F7CF
	imulq	%rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$7, %rdx
	addq	%rax, %rdx
	imulq	$1000, %rdx, %rax       # imm = 0x3E8
	subq	%rax, %rcx
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rcx, %xmm0
	divsd	.LCPI3_0(%rip), %xmm0
	xorl	%eax, %eax
	movq	24(%rsp), %xmm1         # 8-byte Folded Reload
                                        # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	seta	%al
	xorl	%ebp, %eax
	movq	8(%r13), %rcx
	addl	4(%rcx), %eax
	andl	$1, %eax
	movl	%eax, 4(%r13)
	sarq	%rbp
	incq	%rbx
	decq	%r12
	movq	%rcx, %r13
	jne	.LBB3_10
.LBB3_13:                               # %._crit_edge137
                                        #   in Loop: Header=BB3_2 Depth=1
	callq	lrand48
	movq	%rax, %rbp
	movslq	reg_len(%rip), %rax
	movq	64(%rsp), %r12          # 8-byte Reload
	cmpb	$49, -1(%r12,%rax)
	jne	.LBB3_14
# BB#26:                                #   in Loop: Header=BB3_2 Depth=1
	movq	(%r13), %xmm0           # xmm0 = mem[0],zero
	pshufd	$212, %xmm0, %xmm0      # xmm0 = xmm0[0,1,1,3]
	movdqa	(%rsp), %xmm1           # 16-byte Reload
	paddq	%xmm0, %xmm1
	movdqa	%xmm1, %xmm0
	jmp	.LBB3_27
	.p2align	4, 0x90
.LBB3_14:                               #   in Loop: Header=BB3_2 Depth=1
	movdqa	(%rsp), %xmm0           # 16-byte Reload
.LBB3_27:                               # %._crit_edge137._crit_edge
                                        #   in Loop: Header=BB3_2 Depth=1
	movdqa	%xmm0, (%rsp)           # 16-byte Spill
	movd	%xmm0, %eax
	addl	%ebp, %eax
	andl	$1, %eax
	movl	%eax, (%r13)
	callq	lrand48
	movq	%rax, %rcx
	movabsq	$3777893186295716171, %rdx # imm = 0x346DC5D63886594B
	imulq	%rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$11, %rdx
	addq	%rax, %rdx
	imulq	$10000, %rdx, %rax      # imm = 0x2710
	subq	%rax, %rcx
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rcx, %xmm0
	divsd	.LCPI3_1(%rip), %xmm0
	xorl	%eax, %eax
	movsd	24(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	seta	%al
	xorl	%ebp, %eax
	pshufd	$78, (%rsp), %xmm0      # 16-byte Folded Reload
                                        # xmm0 = mem[2,3,0,1]
	movd	%xmm0, %ecx
	addl	%eax, %ecx
	andl	$1, %ecx
	movl	%ecx, 4(%r13)
	movq	80(%rsp), %rax          # 8-byte Reload
	incl	%eax
	cmpl	44(%rsp), %eax          # 4-byte Folded Reload
	movq	72(%rsp), %r13          # 8-byte Reload
	jne	.LBB3_2
# BB#15:                                # %.preheader.loopexit
	movl	reg_len(%rip), %ecx
.LBB3_16:                               # %.preheader
	testl	%ecx, %ecx
	jle	.LBB3_17
# BB#18:                                # %.lr.ph.preheader
	leal	-1(%rcx), %esi
	movl	%ecx, %edi
	xorl	%edx, %edx
	andl	$3, %edi
	je	.LBB3_19
# BB#20:                                # %.lr.ph.prol.preheader
	movl	$1, %ebp
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_21:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r13), %ebx
	cmpl	4(%r13), %ebx
	cmovnel	%ebp, %eax
	movq	8(%r13), %r13
	incl	%edx
	cmpl	%edx, %edi
	jne	.LBB3_21
	jmp	.LBB3_22
.LBB3_17:
	xorl	%eax, %eax
	jmp	.LBB3_25
.LBB3_19:
	xorl	%eax, %eax
.LBB3_22:                               # %.lr.ph.prol.loopexit
	cmpl	$3, %esi
	jb	.LBB3_25
# BB#23:
	movl	$1, %esi
	.p2align	4, 0x90
.LBB3_24:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r13), %edi
	cmpl	4(%r13), %edi
	movq	8(%r13), %rdi
	movl	(%rdi), %ebp
	cmovnel	%esi, %eax
	cmpl	4(%rdi), %ebp
	movq	8(%rdi), %rdi
	movl	(%rdi), %ebp
	cmovnel	%esi, %eax
	cmpl	4(%rdi), %ebp
	movq	8(%rdi), %rdi
	movl	(%rdi), %ebp
	cmovnel	%esi, %eax
	cmpl	4(%rdi), %ebp
	cmovnel	%esi, %eax
	movq	8(%rdi), %r13
	addl	$4, %edx
	cmpl	%ecx, %edx
	jl	.LBB3_24
.LBB3_25:                               # %._crit_edge
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	simulate, .Lfunc_end3-simulate
	.cfi_endproc

	.globl	kill_list
	.p2align	4, 0x90
	.type	kill_list,@function
kill_list:                              # @kill_list
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 16
.Lcfi32:
	.cfi_offset %rbx, -16
	testq	%rdi, %rdi
	je	.LBB4_2
	.p2align	4, 0x90
.LBB4_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB4_1
.LBB4_2:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end4:
	.size	kill_list, .Lfunc_end4-kill_list
	.cfi_endproc

	.type	reg_len,@object         # @reg_len
	.comm	reg_len,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%hu"
	.size	.L.str, 4

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Register too long; Max. = %d\n"
	.size	.L.str.1, 30

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"reg_len\t#_vect\tprob      #_tms\tstruct\tseed1\tseed2\tseed3\tProb same output\n "
	.size	.L.str.4, 75

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%d\t%d\t%.3e %d\t%s\t%d\t%d\t%d\t%.8e\n"
	.size	.L.str.5, 32

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Structure does not match Register length:"
	.size	.Lstr, 42


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
