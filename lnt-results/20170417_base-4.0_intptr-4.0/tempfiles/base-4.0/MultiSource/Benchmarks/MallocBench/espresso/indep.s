	.text
	.file	"indep.bc"
	.globl	sm_maximal_independent_set
	.p2align	4, 0x90
	.type	sm_maximal_independent_set,@function
sm_maximal_independent_set:             # @sm_maximal_independent_set
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	xorl	%eax, %eax
	callq	solution_alloc
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%eax, %eax
	callq	sm_alloc
	movq	%rax, %r12
	movq	32(%rbx), %r13
	testq	%r13, %r13
	je	.LBB0_18
	.p2align	4, 0x90
.LBB0_12:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_14 Depth 2
                                        #       Child Loop BB0_16 Depth 3
                                        #     Child Loop BB0_2 Depth 2
                                        #       Child Loop BB0_4 Depth 3
	movq	16(%r13), %rbp
	testq	%rbp, %rbp
	je	.LBB0_11
# BB#13:                                # %.lr.ph61.preheader.i
                                        #   in Loop: Header=BB0_12 Depth=1
	movq	16(%rbx), %rax
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB0_14:                               # %.lr.ph61.i
                                        #   Parent Loop BB0_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_16 Depth 3
	movslq	4(%rcx), %rdx
	movq	(%rax,%rdx,8), %rdx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB0_17
# BB#15:                                # %.lr.ph.i
                                        #   in Loop: Header=BB0_14 Depth=2
	movq	(%rbx), %rsi
	.p2align	4, 0x90
.LBB0_16:                               #   Parent Loop BB0_12 Depth=1
                                        #     Parent Loop BB0_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	(%rdx), %rdi
	movq	(%rsi,%rdi,8), %rdi
	movl	$0, 8(%rdi)
	movq	8(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_16
.LBB0_17:                               # %._crit_edge.i
                                        #   in Loop: Header=BB0_14 Depth=2
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_14
	jmp	.LBB0_2
	.p2align	4, 0x90
.LBB0_10:                               # %._crit_edge66.i
                                        #   in Loop: Header=BB0_2 Depth=2
	movq	24(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB0_11
# BB#1:                                 # %._crit_edge66.i._crit_edge
                                        #   in Loop: Header=BB0_2 Depth=2
	movq	16(%rbx), %rax
.LBB0_2:                                #   Parent Loop BB0_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_4 Depth 3
	movslq	4(%rbp), %rcx
	movq	(%rax,%rcx,8), %rax
	movq	16(%rax), %r14
	testq	%r14, %r14
	je	.LBB0_10
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph65.i
                                        #   Parent Loop BB0_12 Depth=1
                                        #     Parent Loop BB0_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	(%r14), %rax
	testq	%rax, %rax
	js	.LBB0_8
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=3
	cmpl	8(%rbx), %eax
	jge	.LBB0_8
# BB#6:                                 #   in Loop: Header=BB0_4 Depth=3
	movq	(%rbx), %rcx
	movq	(%rcx,%rax,8), %rax
	cmpl	$0, 8(%rax)
	jne	.LBB0_3
	jmp	.LBB0_9
	.p2align	4, 0x90
.LBB0_8:                                #   in Loop: Header=BB0_4 Depth=3
	xorl	%eax, %eax
	cmpl	$0, 8(%rax)
	jne	.LBB0_3
.LBB0_9:                                #   in Loop: Header=BB0_4 Depth=3
	movl	$1, 8(%rax)
	movl	(%r13), %esi
	movl	(%rax), %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sm_insert
.LBB0_3:                                #   in Loop: Header=BB0_4 Depth=3
	movq	8(%r14), %r14
	testq	%r14, %r14
	jne	.LBB0_4
	jmp	.LBB0_10
	.p2align	4, 0x90
.LBB0_11:                               # %._crit_edge70.i
                                        #   in Loop: Header=BB0_12 Depth=1
	movq	32(%r13), %r13
	testq	%r13, %r13
	jne	.LBB0_12
.LBB0_18:                               # %build_intersection_matrix.exit.preheader
	cmpl	$0, 48(%r12)
	jle	.LBB0_33
# BB#19:                                # %.lr.ph73
	testq	%r15, %r15
	je	.LBB0_28
	.p2align	4, 0x90
.LBB0_20:                               # %.lr.ph73.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_21 Depth 2
                                        #     Child Loop BB0_24 Depth 2
                                        #     Child Loop BB0_26 Depth 2
	movq	32(%r12), %r13
	movq	32(%r13), %rax
	testq	%rax, %rax
	je	.LBB0_22
	.p2align	4, 0x90
.LBB0_21:                               # %.lr.ph
                                        #   Parent Loop BB0_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rax), %ecx
	cmpl	4(%r13), %ecx
	cmovlq	%rax, %r13
	movq	32(%rax), %rax
	testq	%rax, %rax
	jne	.LBB0_21
.LBB0_22:                               # %._crit_edge
                                        #   in Loop: Header=BB0_20 Depth=1
	movslq	(%r13), %rsi
	movq	(%rbx), %rax
	movq	(%rax,%rsi,8), %rax
	movq	16(%rax), %rcx
	movslq	4(%rcx), %rax
	movl	(%r15,%rax,4), %eax
	jmp	.LBB0_24
	.p2align	4, 0x90
.LBB0_23:                               # %.lr.ph66
                                        #   in Loop: Header=BB0_24 Depth=2
	movslq	4(%rcx), %rdx
	movl	(%r15,%rdx,4), %edx
	cmpl	%eax, %edx
	cmovlel	%edx, %eax
.LBB0_24:                               # %.lr.ph66
                                        #   Parent Loop BB0_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_23
# BB#25:                                # %.loopexit
                                        #   in Loop: Header=BB0_20 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	addl	%eax, 8(%rcx)
	movq	(%rcx), %rdi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	sm_row_insert
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	sm_row_dup
	movq	%rax, %r13
	movq	16(%r13), %rbp
	testq	%rbp, %rbp
	je	.LBB0_27
	.p2align	4, 0x90
.LBB0_26:                               # %.lr.ph71
                                        #   Parent Loop BB0_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rbp), %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sm_delrow
	movl	4(%rbp), %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sm_delcol
	movq	24(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB0_26
.LBB0_27:                               # %build_intersection_matrix.exit
                                        #   in Loop: Header=BB0_20 Depth=1
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	sm_row_free
	cmpl	$0, 48(%r12)
	jg	.LBB0_20
	jmp	.LBB0_33
	.p2align	4, 0x90
.LBB0_28:                               # %.lr.ph73.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_29 Depth 2
                                        #     Child Loop BB0_31 Depth 2
	movq	32(%r12), %rbx
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB0_30
	.p2align	4, 0x90
.LBB0_29:                               # %.lr.ph.us
                                        #   Parent Loop BB0_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rax), %ecx
	cmpl	4(%rbx), %ecx
	cmovlq	%rax, %rbx
	movq	32(%rax), %rax
	testq	%rax, %rax
	jne	.LBB0_29
.LBB0_30:                               # %._crit_edge.us
                                        #   in Loop: Header=BB0_28 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	incl	8(%rax)
	movq	(%rax), %rdi
	movl	(%rbx), %esi
	xorl	%eax, %eax
	callq	sm_row_insert
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sm_row_dup
	movq	%rax, %rbx
	movq	16(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB0_32
	.p2align	4, 0x90
.LBB0_31:                               # %.lr.ph71.us
                                        #   Parent Loop BB0_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rbp), %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sm_delrow
	movl	4(%rbp), %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sm_delcol
	movq	24(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB0_31
.LBB0_32:                               # %build_intersection_matrix.exit.us
                                        #   in Loop: Header=BB0_28 Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sm_row_free
	cmpl	$0, 48(%r12)
	jg	.LBB0_28
.LBB0_33:                               # %build_intersection_matrix.exit._crit_edge
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sm_free
	movq	(%rsp), %rax            # 8-byte Reload
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	sm_maximal_independent_set, .Lfunc_end0-sm_maximal_independent_set
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
