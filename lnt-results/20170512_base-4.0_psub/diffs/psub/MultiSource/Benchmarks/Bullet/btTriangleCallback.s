	.text
	.file	"btTriangleCallback.bc"
	.globl	_ZN18btTriangleCallbackD2Ev
	.p2align	4, 0x90
	.type	_ZN18btTriangleCallbackD2Ev,@function
_ZN18btTriangleCallbackD2Ev:            # @_ZN18btTriangleCallbackD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	_ZN18btTriangleCallbackD2Ev, .Lfunc_end0-_ZN18btTriangleCallbackD2Ev
	.cfi_endproc

	.globl	_ZN18btTriangleCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZN18btTriangleCallbackD0Ev,@function
_ZN18btTriangleCallbackD0Ev:            # @_ZN18btTriangleCallbackD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end1:
	.size	_ZN18btTriangleCallbackD0Ev, .Lfunc_end1-_ZN18btTriangleCallbackD0Ev
	.cfi_endproc

	.globl	_ZN31btInternalTriangleIndexCallbackD2Ev
	.p2align	4, 0x90
	.type	_ZN31btInternalTriangleIndexCallbackD2Ev,@function
_ZN31btInternalTriangleIndexCallbackD2Ev: # @_ZN31btInternalTriangleIndexCallbackD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end2:
	.size	_ZN31btInternalTriangleIndexCallbackD2Ev, .Lfunc_end2-_ZN31btInternalTriangleIndexCallbackD2Ev
	.cfi_endproc

	.globl	_ZN31btInternalTriangleIndexCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZN31btInternalTriangleIndexCallbackD0Ev,@function
_ZN31btInternalTriangleIndexCallbackD0Ev: # @_ZN31btInternalTriangleIndexCallbackD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end3:
	.size	_ZN31btInternalTriangleIndexCallbackD0Ev, .Lfunc_end3-_ZN31btInternalTriangleIndexCallbackD0Ev
	.cfi_endproc

	.type	_ZTV18btTriangleCallback,@object # @_ZTV18btTriangleCallback
	.section	.rodata,"a",@progbits
	.globl	_ZTV18btTriangleCallback
	.p2align	3
_ZTV18btTriangleCallback:
	.quad	0
	.quad	_ZTI18btTriangleCallback
	.quad	_ZN18btTriangleCallbackD2Ev
	.quad	_ZN18btTriangleCallbackD0Ev
	.quad	__cxa_pure_virtual
	.size	_ZTV18btTriangleCallback, 40

	.type	_ZTS18btTriangleCallback,@object # @_ZTS18btTriangleCallback
	.globl	_ZTS18btTriangleCallback
	.p2align	4
_ZTS18btTriangleCallback:
	.asciz	"18btTriangleCallback"
	.size	_ZTS18btTriangleCallback, 21

	.type	_ZTI18btTriangleCallback,@object # @_ZTI18btTriangleCallback
	.globl	_ZTI18btTriangleCallback
	.p2align	3
_ZTI18btTriangleCallback:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS18btTriangleCallback
	.size	_ZTI18btTriangleCallback, 16

	.type	_ZTV31btInternalTriangleIndexCallback,@object # @_ZTV31btInternalTriangleIndexCallback
	.globl	_ZTV31btInternalTriangleIndexCallback
	.p2align	3
_ZTV31btInternalTriangleIndexCallback:
	.quad	0
	.quad	_ZTI31btInternalTriangleIndexCallback
	.quad	_ZN31btInternalTriangleIndexCallbackD2Ev
	.quad	_ZN31btInternalTriangleIndexCallbackD0Ev
	.quad	__cxa_pure_virtual
	.size	_ZTV31btInternalTriangleIndexCallback, 40

	.type	_ZTS31btInternalTriangleIndexCallback,@object # @_ZTS31btInternalTriangleIndexCallback
	.globl	_ZTS31btInternalTriangleIndexCallback
	.p2align	4
_ZTS31btInternalTriangleIndexCallback:
	.asciz	"31btInternalTriangleIndexCallback"
	.size	_ZTS31btInternalTriangleIndexCallback, 34

	.type	_ZTI31btInternalTriangleIndexCallback,@object # @_ZTI31btInternalTriangleIndexCallback
	.globl	_ZTI31btInternalTriangleIndexCallback
	.p2align	3
_ZTI31btInternalTriangleIndexCallback:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS31btInternalTriangleIndexCallback
	.size	_ZTI31btInternalTriangleIndexCallback, 16


	.globl	_ZN18btTriangleCallbackD1Ev
	.type	_ZN18btTriangleCallbackD1Ev,@function
_ZN18btTriangleCallbackD1Ev = _ZN18btTriangleCallbackD2Ev
	.globl	_ZN31btInternalTriangleIndexCallbackD1Ev
	.type	_ZN31btInternalTriangleIndexCallbackD1Ev,@function
_ZN31btInternalTriangleIndexCallbackD1Ev = _ZN31btInternalTriangleIndexCallbackD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
