	.text
	.file	"matrix_dec.bc"
	.globl	unmix16
	.p2align	4, 0x90
	.type	unmix16,@function
unmix16:                                # @unmix16
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
.Lcfi4:
	.cfi_offset %rbx, -40
.Lcfi5:
	.cfi_offset %r14, -32
.Lcfi6:
	.cfi_offset %r15, -24
.Lcfi7:
	.cfi_offset %rbp, -16
	movl	%r9d, %r10d
	movl	40(%rsp), %r9d
	testl	%r9d, %r9d
	je	.LBB0_8
# BB#1:                                 # %.preheader41
	testl	%r8d, %r8d
	jle	.LBB0_16
# BB#2:                                 # %.lr.ph47
	movl	%ecx, %r11d
	movl	%r8d, %r15d
	testb	$1, %r15b
	jne	.LBB0_4
# BB#3:
	xorl	%ecx, %ecx
	cmpl	$1, %r8d
	jne	.LBB0_6
	jmp	.LBB0_16
.LBB0_8:                                # %.preheader
	testl	%r8d, %r8d
	jle	.LBB0_16
# BB#9:                                 # %.lr.ph
	movl	%ecx, %eax
	movl	%r8d, %r9d
	leaq	-1(%r9), %r8
	movq	%r9, %r10
	andq	$3, %r10
	je	.LBB0_10
# BB#11:                                # %.prol.preheader
	leaq	(%rax,%rax), %r11
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_12:                               # =>This Inner Loop Header: Depth=1
	movzwl	(%rdi,%rcx,4), %ebx
	movw	%bx, (%rdx)
	movzwl	(%rsi,%rcx,4), %ebx
	movw	%bx, 2(%rdx)
	incq	%rcx
	addq	%r11, %rdx
	cmpq	%rcx, %r10
	jne	.LBB0_12
	jmp	.LBB0_13
.LBB0_4:
	movl	(%rsi), %r14d
	movl	(%rdi), %ebp
	addl	%r14d, %ebp
	movl	%r14d, %ebx
	imull	%r9d, %ebx
	movl	%r10d, %ecx
	sarl	%cl, %ebx
	subl	%ebx, %ebp
	movw	%bp, (%rdx)
	subl	%r14d, %ebp
	movw	%bp, 2(%rdx)
	leaq	(%rdx,%r11,2), %rdx
	movl	$1, %ecx
	cmpl	$1, %r8d
	je	.LBB0_16
.LBB0_6:                                # %.lr.ph47.new
	subq	%rcx, %r15
	leaq	4(%rdi,%rcx,4), %rdi
	leaq	4(%rsi,%rcx,4), %rsi
	addq	%r11, %r11
	.p2align	4, 0x90
.LBB0_7:                                # =>This Inner Loop Header: Depth=1
	movl	-4(%rsi), %eax
	movl	-4(%rdi), %ebx
	addl	%eax, %ebx
	movl	%eax, %ebp
	imull	%r9d, %ebp
	movl	%r10d, %ecx
	sarl	%cl, %ebp
	subl	%ebp, %ebx
	movw	%bx, (%rdx)
	subl	%eax, %ebx
	movw	%bx, 2(%rdx)
	movl	(%rsi), %ebx
	movl	(%rdi), %ebp
	addl	%ebx, %ebp
	movl	%ebx, %eax
	imull	%r9d, %eax
	sarl	%cl, %eax
	subl	%eax, %ebp
	movw	%bp, (%rdx,%r11)
	subl	%ebx, %ebp
	movw	%bp, 2(%rdx,%r11)
	leaq	(%rdx,%r11), %rdx
	addq	$8, %rdi
	addq	$8, %rsi
	addq	%r11, %rdx
	addq	$-2, %r15
	jne	.LBB0_7
	jmp	.LBB0_16
.LBB0_10:
	xorl	%ecx, %ecx
.LBB0_13:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB0_16
# BB#14:                                # %.lr.ph.new
	subq	%rcx, %r9
	leaq	12(%rsi,%rcx,4), %rsi
	leaq	12(%rdi,%rcx,4), %rdi
	addq	%rax, %rax
	.p2align	4, 0x90
.LBB0_15:                               # =>This Inner Loop Header: Depth=1
	movzwl	-12(%rdi), %ecx
	movw	%cx, (%rdx)
	movzwl	-12(%rsi), %ecx
	movw	%cx, 2(%rdx)
	movzwl	-8(%rdi), %ecx
	movw	%cx, (%rdx,%rax)
	movzwl	-8(%rsi), %ecx
	movw	%cx, 2(%rdx,%rax)
	leaq	(%rdx,%rax), %rcx
	movzwl	-4(%rdi), %edx
	movw	%dx, (%rax,%rcx)
	movzwl	-4(%rsi), %edx
	movw	%dx, 2(%rax,%rcx)
	leaq	(%rcx,%rax), %rcx
	movzwl	(%rdi), %edx
	movw	%dx, (%rax,%rcx)
	movzwl	(%rsi), %edx
	movw	%dx, 2(%rax,%rcx)
	leaq	(%rcx,%rax), %rdx
	addq	$16, %rsi
	addq	$16, %rdi
	addq	%rax, %rdx
	addq	$-4, %r9
	jne	.LBB0_15
.LBB0_16:                               # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	unmix16, .Lfunc_end0-unmix16
	.cfi_endproc

	.globl	unmix20
	.p2align	4, 0x90
	.type	unmix20,@function
unmix20:                                # @unmix20
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 24
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movl	%r9d, %r10d
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	24(%rsp), %r9d
	testl	%r9d, %r9d
	je	.LBB1_4
# BB#1:                                 # %.preheader68
	testl	%r8d, %r8d
	jle	.LBB1_7
# BB#2:                                 # %.lr.ph74
	leal	-3(%rcx,%rcx,2), %r11d
	movl	%r8d, %r8d
	addq	$5, %rdx
	addq	$3, %r11
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %eax
	movl	(%rdi), %ebx
	addl	%eax, %ebx
	movl	%eax, %ebp
	imull	%r9d, %ebp
	movl	%r10d, %ecx
	sarl	%cl, %ebp
	subl	%ebp, %ebx
	movl	%ebx, %ecx
	subl	%eax, %ecx
	movl	%ebx, %eax
	shrl	$12, %eax
	movb	%al, -3(%rdx)
	movl	%ebx, %eax
	shrl	$4, %eax
	movb	%al, -4(%rdx)
	shlb	$4, %bl
	movb	%bl, -5(%rdx)
	movl	%ecx, %eax
	shrl	$12, %eax
	movb	%al, (%rdx)
	movl	%ecx, %eax
	shrl	$4, %eax
	movb	%al, -1(%rdx)
	shlb	$4, %cl
	movb	%cl, -2(%rdx)
	addq	$4, %rdi
	addq	$4, %rsi
	addq	%r11, %rdx
	decq	%r8
	jne	.LBB1_3
	jmp	.LBB1_7
.LBB1_4:                                # %.preheader
	testl	%r8d, %r8d
	jle	.LBB1_7
# BB#5:                                 # %.lr.ph
	leal	-3(%rcx,%rcx,2), %r9d
	movl	%r8d, %ecx
	addq	$5, %rdx
	addq	$3, %r9
	.p2align	4, 0x90
.LBB1_6:                                # =>This Inner Loop Header: Depth=1
	movl	(%rdi), %eax
	movl	%eax, %ebp
	shrl	$12, %ebp
	movb	%bpl, -3(%rdx)
	movl	%eax, %ebx
	shrl	$4, %ebx
	movb	%bl, -4(%rdx)
	shlb	$4, %al
	movb	%al, -5(%rdx)
	movl	(%rsi), %ebp
	movl	%ebp, %eax
	shrl	$12, %eax
	movb	%al, (%rdx)
	movl	%ebp, %eax
	shrl	$4, %eax
	movb	%al, -1(%rdx)
	shlb	$4, %bpl
	movb	%bpl, -2(%rdx)
	addq	$4, %rdi
	addq	$4, %rsi
	addq	%r9, %rdx
	decq	%rcx
	jne	.LBB1_6
.LBB1_7:                                # %.loopexit
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	unmix20, .Lfunc_end1-unmix20
	.cfi_endproc

	.globl	unmix24
	.p2align	4, 0x90
	.type	unmix24,@function
unmix24:                                # @unmix24
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 48
.Lcfi17:
	.cfi_offset %rbx, -48
.Lcfi18:
	.cfi_offset %r12, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	64(%rsp), %ebx
	movq	56(%rsp), %r15
	movl	48(%rsp), %r11d
	leal	(,%rbx,8), %r10d
	testl	%r11d, %r11d
	je	.LBB2_8
# BB#1:
	testl	%ebx, %ebx
	je	.LBB2_2
# BB#5:                                 # %.preheader145
	testl	%r8d, %r8d
	jle	.LBB2_19
# BB#6:                                 # %.lr.ph158
	leal	-3(%rcx,%rcx,2), %r14d
	movl	%r8d, %r8d
	addq	$2, %r15
	addq	$5, %rdx
	addq	$3, %r14
	.p2align	4, 0x90
.LBB2_7:                                # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %r12d
	movl	(%rdi), %ebp
	addl	%r12d, %ebp
	movl	%r12d, %ebx
	imull	%r11d, %ebx
	movl	%r9d, %ecx
	sarl	%cl, %ebx
	subl	%ebx, %ebp
	movl	%ebp, %eax
	subl	%r12d, %eax
	movl	%r10d, %ecx
	shll	%cl, %ebp
	movzwl	-2(%r15), %ebx
	orl	%ebp, %ebx
	shll	%cl, %eax
	movzwl	(%r15), %ecx
	orl	%eax, %ecx
	shrl	$16, %ebp
	movb	%bpl, -3(%rdx)
	movb	%bh, -4(%rdx)  # NOREX
	movb	%bl, -5(%rdx)
	shrl	$16, %eax
	movb	%al, (%rdx)
	movb	%ch, -1(%rdx)  # NOREX
	movb	%cl, -2(%rdx)
	addq	$4, %rdi
	addq	$4, %rsi
	addq	$4, %r15
	addq	%r14, %rdx
	decq	%r8
	jne	.LBB2_7
	jmp	.LBB2_19
.LBB2_8:
	testl	%ebx, %ebx
	je	.LBB2_9
# BB#16:                                # %.preheader
	testl	%r8d, %r8d
	jle	.LBB2_19
# BB#17:                                # %.lr.ph
	leal	-3(%rcx,%rcx,2), %r9d
	movl	%r8d, %r8d
	addq	$2, %r15
	addq	$5, %rdx
	addq	$3, %r9
	.p2align	4, 0x90
.LBB2_18:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi), %eax
	movl	(%rsi), %ebp
	movl	%r10d, %ecx
	shll	%cl, %eax
	movzwl	-2(%r15), %ebx
	orl	%eax, %ebx
	shll	%cl, %ebp
	movzwl	(%r15), %ecx
	orl	%ebp, %ecx
	shrl	$16, %eax
	movb	%al, -3(%rdx)
	movb	%bh, -4(%rdx)  # NOREX
	movb	%bl, -5(%rdx)
	shrl	$16, %ebp
	movb	%bpl, (%rdx)
	movb	%ch, -1(%rdx)  # NOREX
	movb	%cl, -2(%rdx)
	addq	$4, %rdi
	addq	$4, %rsi
	addq	$4, %r15
	addq	%r9, %rdx
	decq	%r8
	jne	.LBB2_18
	jmp	.LBB2_19
.LBB2_2:                                # %.preheader147
	testl	%r8d, %r8d
	jle	.LBB2_19
# BB#3:                                 # %.lr.ph161
	leal	-3(%rcx,%rcx,2), %r10d
	movl	%r8d, %r8d
	addq	$5, %rdx
	addq	$3, %r10
	.p2align	4, 0x90
.LBB2_4:                                # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %ebx
	movl	(%rdi), %eax
	addl	%ebx, %eax
	movl	%ebx, %ebp
	imull	%r11d, %ebp
	movl	%r9d, %ecx
	sarl	%cl, %ebp
	subl	%ebp, %eax
	movl	%eax, %ecx
	shrl	$16, %ecx
	movb	%cl, -3(%rdx)
	movb	%ah, -4(%rdx)  # NOREX
	movb	%al, -5(%rdx)
	subl	%ebx, %eax
	movl	%eax, %ecx
	shrl	$16, %ecx
	movb	%cl, (%rdx)
	movb	%ah, -1(%rdx)  # NOREX
	movb	%al, -2(%rdx)
	addq	$4, %rdi
	addq	$4, %rsi
	addq	%r10, %rdx
	decq	%r8
	jne	.LBB2_4
	jmp	.LBB2_19
.LBB2_9:                                # %.preheader143
	testl	%r8d, %r8d
	jle	.LBB2_19
# BB#10:                                # %.lr.ph154
	leal	-3(%rcx,%rcx,2), %ebp
	movl	%r8d, %ecx
	testb	$1, %cl
	jne	.LBB2_12
# BB#11:
	xorl	%ebx, %ebx
	cmpl	$1, %r8d
	jne	.LBB2_14
	jmp	.LBB2_19
.LBB2_12:
	movl	(%rdi), %eax
	movl	%eax, %ebx
	shrl	$16, %ebx
	movb	%bl, 2(%rdx)
	movb	%ah, 1(%rdx)  # NOREX
	movb	%al, (%rdx)
	movl	(%rsi), %eax
	movl	%eax, %ebx
	shrl	$16, %ebx
	movb	%bl, 5(%rdx)
	movb	%ah, 4(%rdx)  # NOREX
	movb	%al, 3(%rdx)
	leaq	3(%rdx,%rbp), %rdx
	movl	$1, %ebx
	cmpl	$1, %r8d
	je	.LBB2_19
.LBB2_14:                               # %.lr.ph154.new
	subq	%rbx, %rcx
	leaq	4(%rdi,%rbx,4), %rdi
	leaq	4(%rsi,%rbx,4), %rsi
	addq	$5, %rdx
	leaq	6(%rbp,%rbp), %r8
	.p2align	4, 0x90
.LBB2_15:                               # =>This Inner Loop Header: Depth=1
	movl	-4(%rdi), %eax
	movl	%eax, %ebx
	shrl	$16, %ebx
	movb	%bl, -3(%rdx)
	movb	%ah, -4(%rdx)  # NOREX
	movb	%al, -5(%rdx)
	movl	-4(%rsi), %ebx
	movl	%ebx, %eax
	shrl	$16, %eax
	movb	%al, (%rdx)
	movb	%bh, -1(%rdx)  # NOREX
	movb	%bl, -2(%rdx)
	movl	(%rdi), %ebx
	movl	%ebx, %eax
	shrl	$16, %eax
	movb	%al, (%rbp,%rdx)
	movb	%bh, -1(%rbp,%rdx)  # NOREX
	movb	%bl, -2(%rbp,%rdx)
	movl	(%rsi), %ebx
	movl	%ebx, %eax
	shrl	$16, %eax
	movb	%al, 3(%rbp,%rdx)
	movb	%bh, 2(%rbp,%rdx)  # NOREX
	movb	%bl, 1(%rbp,%rdx)
	addq	$8, %rdi
	addq	$8, %rsi
	addq	%r8, %rdx
	addq	$-2, %rcx
	jne	.LBB2_15
.LBB2_19:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	unmix24, .Lfunc_end2-unmix24
	.cfi_endproc

	.globl	unmix32
	.p2align	4, 0x90
	.type	unmix32,@function
unmix32:                                # @unmix32
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 40
.Lcfi26:
	.cfi_offset %rbx, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movl	56(%rsp), %ebx
	movq	48(%rsp), %r15
	movl	40(%rsp), %r11d
	leal	(,%rbx,8), %r10d
	testl	%r11d, %r11d
	je	.LBB3_4
# BB#1:                                 # %.preheader81
	testl	%r8d, %r8d
	jle	.LBB3_16
# BB#2:                                 # %.lr.ph92
	movl	%ecx, %r14d
	movl	%r8d, %r8d
	addq	$2, %r15
	addq	$4, %rdx
	shlq	$2, %r14
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %eax
	movl	(%rdi), %ebx
	addl	%eax, %ebx
	movl	%eax, %ebp
	imull	%r11d, %ebp
	movl	%r9d, %ecx
	sarl	%cl, %ebp
	subl	%ebp, %ebx
	movl	%ebx, %ebp
	subl	%eax, %ebp
	movl	%r10d, %ecx
	shll	%cl, %ebx
	movzwl	-2(%r15), %eax
	orl	%ebx, %eax
	movl	%eax, -4(%rdx)
	shll	%cl, %ebp
	movzwl	(%r15), %eax
	orl	%ebp, %eax
	movl	%eax, (%rdx)
	addq	$4, %r15
	addq	$4, %rdi
	addq	$4, %rsi
	addq	%r14, %rdx
	decq	%r8
	jne	.LBB3_3
	jmp	.LBB3_16
.LBB3_4:
	testl	%ebx, %ebx
	je	.LBB3_8
# BB#5:                                 # %.preheader79
	testl	%r8d, %r8d
	jle	.LBB3_16
# BB#6:                                 # %.lr.ph88
	movl	%ecx, %r9d
	movl	%r8d, %ebx
	addq	$2, %r15
	addq	$4, %rdx
	shlq	$2, %r9
	.p2align	4, 0x90
.LBB3_7:                                # =>This Inner Loop Header: Depth=1
	movl	(%rdi), %eax
	movl	%r10d, %ecx
	shll	%cl, %eax
	movzwl	-2(%r15), %ecx
	orl	%eax, %ecx
	movl	%ecx, -4(%rdx)
	movl	(%rsi), %eax
	movl	%r10d, %ecx
	shll	%cl, %eax
	movzwl	(%r15), %ecx
	orl	%eax, %ecx
	movl	%ecx, (%rdx)
	addq	$4, %r15
	addq	$4, %rdi
	addq	$4, %rsi
	addq	%r9, %rdx
	decq	%rbx
	jne	.LBB3_7
	jmp	.LBB3_16
.LBB3_8:                                # %.preheader
	testl	%r8d, %r8d
	jle	.LBB3_16
# BB#9:                                 # %.lr.ph
	movl	%ecx, %eax
	movl	%r8d, %ecx
	leaq	-1(%rcx), %r8
	movq	%rcx, %r9
	andq	$3, %r9
	je	.LBB3_10
# BB#11:                                # %.prol.preheader
	leaq	(,%rax,4), %r10
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_12:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rbx,4), %ebp
	movl	%ebp, (%rdx)
	movl	(%rsi,%rbx,4), %ebp
	movl	%ebp, 4(%rdx)
	incq	%rbx
	addq	%r10, %rdx
	cmpq	%rbx, %r9
	jne	.LBB3_12
	jmp	.LBB3_13
.LBB3_10:
	xorl	%ebx, %ebx
.LBB3_13:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB3_16
# BB#14:                                # %.lr.ph.new
	subq	%rbx, %rcx
	leaq	12(%rsi,%rbx,4), %rsi
	leaq	12(%rdi,%rbx,4), %rdi
	shlq	$2, %rax
	.p2align	4, 0x90
.LBB3_15:                               # =>This Inner Loop Header: Depth=1
	movl	-12(%rdi), %ebp
	movl	%ebp, (%rdx)
	movl	-12(%rsi), %ebp
	movl	%ebp, 4(%rdx)
	movl	-8(%rdi), %ebp
	movl	%ebp, (%rdx,%rax)
	movl	-8(%rsi), %ebp
	movl	%ebp, 4(%rdx,%rax)
	leaq	(%rdx,%rax), %rdx
	movl	-4(%rdi), %ebp
	movl	%ebp, (%rax,%rdx)
	movl	-4(%rsi), %ebp
	movl	%ebp, 4(%rax,%rdx)
	leaq	(%rdx,%rax), %rdx
	movl	(%rdi), %ebp
	movl	%ebp, (%rax,%rdx)
	movl	(%rsi), %ebp
	movl	%ebp, 4(%rax,%rdx)
	leaq	(%rdx,%rax), %rdx
	addq	$16, %rsi
	addq	$16, %rdi
	addq	%rax, %rdx
	addq	$-4, %rcx
	jne	.LBB3_15
.LBB3_16:                               # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	unmix32, .Lfunc_end3-unmix32
	.cfi_endproc

	.globl	copyPredictorTo24
	.p2align	4, 0x90
	.type	copyPredictorTo24,@function
copyPredictorTo24:                      # @copyPredictorTo24
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 24
.Lcfi32:
	.cfi_offset %rbx, -24
.Lcfi33:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	testl	%ecx, %ecx
	jle	.LBB4_7
# BB#1:                                 # %.lr.ph
	leal	(%rdx,%rdx,2), %ebp
	movl	%ecx, %edx
	testb	$1, %dl
	jne	.LBB4_3
# BB#2:
	xorl	%r8d, %r8d
	cmpl	$1, %ecx
	jne	.LBB4_5
	jmp	.LBB4_7
.LBB4_3:
	movl	(%rdi), %eax
	movl	%eax, %ebx
	shrl	$16, %ebx
	movb	%bl, 2(%rsi)
	movb	%ah, 1(%rsi)  # NOREX
	movb	%al, (%rsi)
	addq	%rbp, %rsi
	movl	$1, %r8d
	cmpl	$1, %ecx
	je	.LBB4_7
.LBB4_5:                                # %.lr.ph.new
	subq	%r8, %rdx
	leaq	4(%rdi,%r8,4), %rcx
	addq	$2, %rsi
	leaq	(%rbp,%rbp), %rdi
	.p2align	4, 0x90
.LBB4_6:                                # =>This Inner Loop Header: Depth=1
	movl	-4(%rcx), %eax
	movl	%eax, %ebx
	shrl	$16, %ebx
	movb	%bl, (%rsi)
	movb	%ah, -1(%rsi)  # NOREX
	movb	%al, -2(%rsi)
	movl	(%rcx), %ebx
	movl	%ebx, %eax
	shrl	$16, %eax
	movb	%al, (%rbp,%rsi)
	movb	%bh, -1(%rbp,%rsi)  # NOREX
	movb	%bl, -2(%rbp,%rsi)
	addq	$8, %rcx
	addq	%rdi, %rsi
	addq	$-2, %rdx
	jne	.LBB4_6
.LBB4_7:                                # %._crit_edge
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end4:
	.size	copyPredictorTo24, .Lfunc_end4-copyPredictorTo24
	.cfi_endproc

	.globl	copyPredictorTo24Shift
	.p2align	4, 0x90
	.type	copyPredictorTo24Shift,@function
copyPredictorTo24Shift:                 # @copyPredictorTo24Shift
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 24
.Lcfi36:
	.cfi_offset %rbx, -24
.Lcfi37:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	testl	%r8d, %r8d
	jle	.LBB5_7
# BB#1:                                 # %.lr.ph
	shll	$3, %r9d
	leal	(%rcx,%rcx,2), %ebp
	movl	%r8d, %eax
	testb	$1, %al
	jne	.LBB5_3
# BB#2:
	xorl	%ecx, %ecx
	cmpl	$1, %r8d
	jne	.LBB5_5
	jmp	.LBB5_7
.LBB5_3:
	movl	(%rdi), %r10d
	movl	%r9d, %ecx
	shll	%cl, %r10d
	movzwl	(%rsi), %ecx
	orl	%r10d, %ecx
	shrl	$16, %r10d
	movb	%r10b, 2(%rdx)
	movb	%ch, 1(%rdx)  # NOREX
	movb	%cl, (%rdx)
	addq	%rbp, %rdx
	movl	$1, %ecx
	cmpl	$1, %r8d
	je	.LBB5_7
.LBB5_5:                                # %.lr.ph.new
	subq	%rcx, %rax
	leaq	4(%rdi,%rcx,4), %rdi
	leaq	2(%rsi,%rcx,2), %rsi
	addq	$2, %rdx
	leaq	(%rbp,%rbp), %r8
	.p2align	4, 0x90
.LBB5_6:                                # =>This Inner Loop Header: Depth=1
	movl	-4(%rdi), %ebx
	movl	%r9d, %ecx
	shll	%cl, %ebx
	movzwl	-2(%rsi), %ecx
	orl	%ebx, %ecx
	shrl	$16, %ebx
	movb	%bl, (%rdx)
	movb	%ch, -1(%rdx)  # NOREX
	movb	%cl, -2(%rdx)
	movl	(%rdi), %ebx
	movl	%r9d, %ecx
	shll	%cl, %ebx
	movzwl	(%rsi), %ecx
	orl	%ebx, %ecx
	shrl	$16, %ebx
	movb	%bl, (%rbp,%rdx)
	movb	%ch, -1(%rbp,%rdx)  # NOREX
	movb	%cl, -2(%rbp,%rdx)
	addq	$8, %rdi
	addq	$4, %rsi
	addq	%r8, %rdx
	addq	$-2, %rax
	jne	.LBB5_6
.LBB5_7:                                # %._crit_edge
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end5:
	.size	copyPredictorTo24Shift, .Lfunc_end5-copyPredictorTo24Shift
	.cfi_endproc

	.globl	copyPredictorTo20
	.p2align	4, 0x90
	.type	copyPredictorTo20,@function
copyPredictorTo20:                      # @copyPredictorTo20
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	testl	%ecx, %ecx
	jle	.LBB6_7
# BB#1:                                 # %.lr.ph
	leal	(%rdx,%rdx,2), %r9d
	movl	%ecx, %edx
	testb	$1, %dl
	jne	.LBB6_3
# BB#2:
	xorl	%r8d, %r8d
	cmpl	$1, %ecx
	jne	.LBB6_5
	jmp	.LBB6_7
.LBB6_3:
	movl	(%rdi), %r8d
	movl	%r8d, %eax
	shrl	$12, %eax
	movb	%al, 2(%rsi)
	movl	%r8d, %eax
	shrl	$4, %eax
	movb	%al, 1(%rsi)
	shlb	$4, %r8b
	movb	%r8b, (%rsi)
	addq	%r9, %rsi
	movl	$1, %r8d
	cmpl	$1, %ecx
	je	.LBB6_7
.LBB6_5:                                # %.lr.ph.new
	subq	%r8, %rdx
	leaq	4(%rdi,%r8,4), %rcx
	addq	$2, %rsi
	leaq	(%r9,%r9), %r8
	.p2align	4, 0x90
.LBB6_6:                                # =>This Inner Loop Header: Depth=1
	movl	-4(%rcx), %edi
	movl	%edi, %eax
	shrl	$12, %eax
	movb	%al, (%rsi)
	movl	%edi, %eax
	shrl	$4, %eax
	movb	%al, -1(%rsi)
	shlb	$4, %dil
	movb	%dil, -2(%rsi)
	movl	(%rcx), %eax
	movl	%eax, %edi
	shrl	$12, %edi
	movb	%dil, (%r9,%rsi)
	movl	%eax, %edi
	shrl	$4, %edi
	movb	%dil, -1(%r9,%rsi)
	shlb	$4, %al
	movb	%al, -2(%r9,%rsi)
	addq	$8, %rcx
	addq	%r8, %rsi
	addq	$-2, %rdx
	jne	.LBB6_6
.LBB6_7:                                # %._crit_edge
	retq
.Lfunc_end6:
	.size	copyPredictorTo20, .Lfunc_end6-copyPredictorTo20
	.cfi_endproc

	.globl	copyPredictorTo32
	.p2align	4, 0x90
	.type	copyPredictorTo32,@function
copyPredictorTo32:                      # @copyPredictorTo32
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 24
.Lcfi40:
	.cfi_offset %rbx, -24
.Lcfi41:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	testl	%ecx, %ecx
	jle	.LBB7_6
# BB#1:                                 # %.lr.ph.preheader
	movl	%ecx, %r10d
	leaq	-1(%r10), %r8
	movq	%r10, %r9
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	andq	$3, %r9
	je	.LBB7_3
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax,4), %r11d
	movslq	%ecx, %rcx
	movl	%r11d, (%rsi,%rcx,4)
	incq	%rax
	addl	%edx, %ecx
	cmpq	%rax, %r9
	jne	.LBB7_2
.LBB7_3:                                # %.lr.ph.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB7_6
# BB#4:                                 # %.lr.ph.preheader.new
	subq	%rax, %r10
	leaq	12(%rdi,%rax,4), %rdi
	leal	(%rdx,%rdx,2), %r8d
	leal	(%rdx,%rdx), %r9d
	.p2align	4, 0x90
.LBB7_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rdi), %r11d
	movslq	%ecx, %rax
	movl	%r11d, (%rsi,%rax,4)
	leal	(%rax,%rdx), %ecx
	movl	-8(%rdi), %r11d
	movslq	%ecx, %rcx
	movl	%r11d, (%rsi,%rcx,4)
	addl	%edx, %ecx
	leal	(%r9,%rax), %r11d
	movl	-4(%rdi), %ebp
	movslq	%r11d, %rbx
	movl	%ebp, (%rsi,%rbx,4)
	addl	%edx, %ecx
	addl	%r8d, %eax
	movl	(%rdi), %ebp
	cltq
	movl	%ebp, (%rsi,%rax,4)
	addl	%edx, %ecx
	addq	$16, %rdi
	addq	$-4, %r10
	jne	.LBB7_5
.LBB7_6:                                # %._crit_edge
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end7:
	.size	copyPredictorTo32, .Lfunc_end7-copyPredictorTo32
	.cfi_endproc

	.globl	copyPredictorTo32Shift
	.p2align	4, 0x90
	.type	copyPredictorTo32Shift,@function
copyPredictorTo32Shift:                 # @copyPredictorTo32Shift
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 16
.Lcfi43:
	.cfi_offset %rbx, -16
	movl	%r9d, %r10d
	testl	%r8d, %r8d
	jle	.LBB8_7
# BB#1:                                 # %.lr.ph
	shll	$3, %r10d
	movl	%ecx, %r9d
	movl	%r8d, %ebx
	testb	$1, %bl
	jne	.LBB8_3
# BB#2:
	xorl	%ecx, %ecx
	cmpl	$1, %r8d
	jne	.LBB8_5
	jmp	.LBB8_7
.LBB8_3:
	movl	(%rdi), %r11d
	movl	%r10d, %ecx
	shll	%cl, %r11d
	movzwl	(%rsi), %ecx
	orl	%r11d, %ecx
	movl	%ecx, (%rdx)
	leaq	(%rdx,%r9,4), %rdx
	movl	$1, %ecx
	cmpl	$1, %r8d
	je	.LBB8_7
.LBB8_5:                                # %.lr.ph.new
	subq	%rcx, %rbx
	leaq	4(%rdi,%rcx,4), %rdi
	leaq	2(%rsi,%rcx,2), %rsi
	leaq	(,%r9,8), %r8
	.p2align	4, 0x90
.LBB8_6:                                # =>This Inner Loop Header: Depth=1
	movl	-4(%rdi), %eax
	movl	%r10d, %ecx
	shll	%cl, %eax
	movzwl	-2(%rsi), %ecx
	orl	%eax, %ecx
	movl	%ecx, (%rdx)
	movl	(%rdi), %eax
	movl	%r10d, %ecx
	shll	%cl, %eax
	movzwl	(%rsi), %ecx
	orl	%eax, %ecx
	movl	%ecx, (%rdx,%r9,4)
	addq	$8, %rdi
	addq	$4, %rsi
	addq	%r8, %rdx
	addq	$-2, %rbx
	jne	.LBB8_6
.LBB8_7:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end8:
	.size	copyPredictorTo32Shift, .Lfunc_end8-copyPredictorTo32Shift
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
