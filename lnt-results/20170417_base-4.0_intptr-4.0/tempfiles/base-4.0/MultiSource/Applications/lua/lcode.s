	.text
	.file	"lcode.bc"
	.hidden	luaK_nil
	.globl	luaK_nil
	.p2align	4, 0x90
	.type	luaK_nil,@function
luaK_nil:                               # @luaK_nil
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movslq	48(%rdi), %rax
	cmpl	52(%rdi), %eax
	jle	.LBB0_3
# BB#1:
	testl	%eax, %eax
	je	.LBB0_2
# BB#4:
	movq	(%rdi), %rcx
	movq	24(%rcx), %r8
	movl	-4(%r8,%rax,4), %r10d
	movl	%r10d, %ecx
	andl	$63, %ecx
	cmpl	$3, %ecx
	jne	.LBB0_3
# BB#5:
	movl	%r10d, %ecx
	shrl	$6, %ecx
	movzbl	%cl, %ecx
	cmpl	%esi, %ecx
	jg	.LBB0_3
# BB#6:
	movl	%r10d, %r9d
	shrl	$23, %r9d
	leal	1(%r9), %ecx
	cmpl	%esi, %ecx
	jl	.LBB0_3
# BB#7:
	leal	-1(%rsi,%rdx), %edx
	cmpl	%r9d, %edx
	jle	.LBB0_9
# BB#8:                                 # %.critedge
	andl	$8388607, %r10d         # imm = 0x7FFFFF
	shll	$23, %edx
	orl	%r10d, %edx
	movl	%edx, -4(%r8,%rax,4)
	retq
.LBB0_2:
	movzbl	74(%rdi), %eax
	cmpl	%esi, %eax
	jle	.LBB0_9
.LBB0_3:
	addl	%esi, %edx
	shll	$6, %esi
	shll	$23, %edx
	addl	$-8388608, %edx         # imm = 0xFF800000
	orl	%esi, %edx
	orl	$3, %edx
	movq	24(%rdi), %rax
	movl	8(%rax), %eax
	movl	%edx, %esi
	movl	%eax, %edx
	jmp	luaK_code               # TAILCALL
.LBB0_9:                                # %.critedge31
	retq
.Lfunc_end0:
	.size	luaK_nil, .Lfunc_end0-luaK_nil
	.cfi_endproc

	.hidden	luaK_codeABC
	.globl	luaK_codeABC
	.p2align	4, 0x90
	.type	luaK_codeABC,@function
luaK_codeABC:                           # @luaK_codeABC
	.cfi_startproc
# BB#0:
	shll	$6, %edx
	orl	%edx, %esi
	shll	$23, %ecx
	shll	$14, %r8d
	orl	%ecx, %r8d
	orl	%r8d, %esi
	movq	24(%rdi), %rax
	movl	8(%rax), %edx
	jmp	luaK_code               # TAILCALL
.Lfunc_end1:
	.size	luaK_codeABC, .Lfunc_end1-luaK_codeABC
	.cfi_endproc

	.hidden	luaK_jump
	.globl	luaK_jump
	.p2align	4, 0x90
	.type	luaK_jump,@function
luaK_jump:                              # @luaK_jump
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movl	56(%r12), %r15d
	movl	$-1, 56(%r12)
	movq	24(%r12), %rax
	movl	8(%rax), %edx
	movl	$2147450902, %esi       # imm = 0x7FFF8016
	callq	luaK_code
	movl	%eax, %r14d
	cmpl	$-1, %r15d
	je	.LBB2_8
# BB#1:
	cmpl	$-1, %r14d
	je	.LBB2_2
# BB#3:                                 # %.preheader.i
	movq	(%r12), %rax
	movq	24(%rax), %rbp
	movl	%r14d, %edx
	.p2align	4, 0x90
.LBB2_4:                                # =>This Inner Loop Header: Depth=1
	movl	%edx, %ecx
	movslq	%ecx, %rbx
	movl	(%rbp,%rbx,4), %eax
	movl	%eax, %edx
	shrl	$14, %edx
	leal	-131071(%rdx), %esi
	cmpl	$-1, %esi
	leal	-131070(%rcx,%rdx), %edx
	cmovel	%esi, %edx
	cmpl	$-1, %edx
	jne	.LBB2_4
# BB#5:
	notl	%ecx
	addl	%ecx, %r15d
	movl	%r15d, %ecx
	negl	%ecx
	cmovll	%r15d, %ecx
	cmpl	$131072, %ecx           # imm = 0x20000
	jl	.LBB2_7
# BB#6:
	movq	24(%r12), %rdi
	movl	$.L.str.1, %esi
	callq	luaX_syntaxerror
	movl	(%rbp,%rbx,4), %eax
.LBB2_7:                                # %fixjump.exit.i
	andl	$16383, %eax            # imm = 0x3FFF
	shll	$14, %r15d
	leal	2147467264(%rax,%r15), %eax
	movl	%eax, (%rbp,%rbx,4)
	jmp	.LBB2_8
.LBB2_2:
	movl	%r15d, %r14d
.LBB2_8:                                # %luaK_concat.exit
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	luaK_jump, .Lfunc_end2-luaK_jump
	.cfi_endproc

	.hidden	luaK_codeABx
	.globl	luaK_codeABx
	.p2align	4, 0x90
	.type	luaK_codeABx,@function
luaK_codeABx:                           # @luaK_codeABx
	.cfi_startproc
# BB#0:
	shll	$6, %edx
	orl	%edx, %esi
	shll	$14, %ecx
	orl	%ecx, %esi
	movq	24(%rdi), %rax
	movl	8(%rax), %edx
	jmp	luaK_code               # TAILCALL
.Lfunc_end3:
	.size	luaK_codeABx, .Lfunc_end3-luaK_codeABx
	.cfi_endproc

	.hidden	luaK_concat
	.globl	luaK_concat
	.p2align	4, 0x90
	.type	luaK_concat,@function
luaK_concat:                            # @luaK_concat
	.cfi_startproc
# BB#0:
	cmpl	$-1, %edx
	je	.LBB4_7
# BB#1:
	movl	(%rsi), %ecx
	cmpl	$-1, %ecx
	je	.LBB4_8
# BB#2:                                 # %.preheader
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	movq	(%rdi), %rax
	movq	24(%rax), %r14
	.p2align	4, 0x90
.LBB4_3:                                # =>This Inner Loop Header: Depth=1
	movl	%ecx, %ebx
	movslq	%ebx, %r15
	movl	(%r14,%r15,4), %eax
	movl	%eax, %ecx
	shrl	$14, %ecx
	leal	-131071(%rcx), %esi
	cmpl	$-1, %esi
	leal	-131070(%rbx,%rcx), %ecx
	cmovel	%esi, %ecx
	cmpl	$-1, %ecx
	jne	.LBB4_3
# BB#4:
	notl	%ebx
	addl	%edx, %ebx
	movl	%ebx, %ecx
	negl	%ecx
	cmovll	%ebx, %ecx
	cmpl	$131072, %ecx           # imm = 0x20000
	jl	.LBB4_6
# BB#5:
	movq	24(%rdi), %rdi
	movl	$.L.str.1, %esi
	callq	luaX_syntaxerror
	movl	(%r14,%r15,4), %eax
.LBB4_6:                                # %fixjump.exit
	andl	$16383, %eax            # imm = 0x3FFF
	shll	$14, %ebx
	leal	2147467264(%rax,%rbx), %eax
	movl	%eax, (%r14,%r15,4)
	popq	%rbx
	popq	%r14
	popq	%r15
.LBB4_7:
	retq
.LBB4_8:
	movl	%edx, (%rsi)
	retq
.Lfunc_end4:
	.size	luaK_concat, .Lfunc_end4-luaK_concat
	.cfi_endproc

	.hidden	luaK_ret
	.globl	luaK_ret
	.p2align	4, 0x90
	.type	luaK_ret,@function
luaK_ret:                               # @luaK_ret
	.cfi_startproc
# BB#0:
	shll	$6, %esi
	shll	$23, %edx
	addl	$8388608, %edx          # imm = 0x800000
	orl	%edx, %esi
	orl	$30, %esi
	movq	24(%rdi), %rax
	movl	8(%rax), %edx
	jmp	luaK_code               # TAILCALL
.Lfunc_end5:
	.size	luaK_ret, .Lfunc_end5-luaK_ret
	.cfi_endproc

	.hidden	luaK_getlabel
	.globl	luaK_getlabel
	.p2align	4, 0x90
	.type	luaK_getlabel,@function
luaK_getlabel:                          # @luaK_getlabel
	.cfi_startproc
# BB#0:
	movl	48(%rdi), %eax
	movl	%eax, 52(%rdi)
	retq
.Lfunc_end6:
	.size	luaK_getlabel, .Lfunc_end6-luaK_getlabel
	.cfi_endproc

	.hidden	luaK_patchlist
	.globl	luaK_patchlist
	.p2align	4, 0x90
	.type	luaK_patchlist,@function
luaK_patchlist:                         # @luaK_patchlist
	.cfi_startproc
# BB#0:
	cmpl	%edx, 48(%rdi)
	jne	.LBB7_8
# BB#1:
	movl	%edx, 52(%rdi)
	cmpl	$-1, %esi
	je	.LBB7_10
# BB#2:
	movl	56(%rdi), %ecx
	cmpl	$-1, %ecx
	je	.LBB7_9
# BB#3:                                 # %.preheader.i.i
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %r15, -16
	movq	(%rdi), %rax
	movq	24(%rax), %r14
	.p2align	4, 0x90
.LBB7_4:                                # =>This Inner Loop Header: Depth=1
	movl	%ecx, %ebx
	movslq	%ebx, %r15
	movl	(%r14,%r15,4), %eax
	movl	%eax, %ecx
	shrl	$14, %ecx
	leal	-131071(%rcx), %edx
	cmpl	$-1, %edx
	leal	-131070(%rbx,%rcx), %ecx
	cmovel	%edx, %ecx
	cmpl	$-1, %ecx
	jne	.LBB7_4
# BB#5:
	notl	%ebx
	addl	%esi, %ebx
	movl	%ebx, %ecx
	negl	%ecx
	cmovll	%ebx, %ecx
	cmpl	$131072, %ecx           # imm = 0x20000
	jl	.LBB7_7
# BB#6:
	movq	24(%rdi), %rdi
	movl	$.L.str.1, %esi
	callq	luaX_syntaxerror
	movl	(%r14,%r15,4), %eax
.LBB7_7:                                # %fixjump.exit.i.i
	andl	$16383, %eax            # imm = 0x3FFF
	shll	$14, %ebx
	leal	2147467264(%rax,%rbx), %eax
	movl	%eax, (%r14,%r15,4)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB7_8:
	movl	$255, %ecx
	movl	%edx, %r8d
	jmp	patchlistaux            # TAILCALL
.LBB7_9:
	movl	%esi, 56(%rdi)
.LBB7_10:                               # %luaK_patchtohere.exit
	retq
.Lfunc_end7:
	.size	luaK_patchlist, .Lfunc_end7-luaK_patchlist
	.cfi_endproc

	.hidden	luaK_patchtohere
	.globl	luaK_patchtohere
	.p2align	4, 0x90
	.type	luaK_patchtohere,@function
luaK_patchtohere:                       # @luaK_patchtohere
	.cfi_startproc
# BB#0:
	movl	48(%rdi), %eax
	movl	%eax, 52(%rdi)
	cmpl	$-1, %esi
	je	.LBB8_7
# BB#1:
	movl	56(%rdi), %ecx
	cmpl	$-1, %ecx
	je	.LBB8_8
# BB#2:                                 # %.preheader.i
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -32
.Lcfi26:
	.cfi_offset %r14, -24
.Lcfi27:
	.cfi_offset %r15, -16
	movq	(%rdi), %rax
	movq	24(%rax), %r14
	.p2align	4, 0x90
.LBB8_3:                                # =>This Inner Loop Header: Depth=1
	movl	%ecx, %ebx
	movslq	%ebx, %r15
	movl	(%r14,%r15,4), %eax
	movl	%eax, %ecx
	shrl	$14, %ecx
	leal	-131071(%rcx), %edx
	cmpl	$-1, %edx
	leal	-131070(%rbx,%rcx), %ecx
	cmovel	%edx, %ecx
	cmpl	$-1, %ecx
	jne	.LBB8_3
# BB#4:
	notl	%ebx
	addl	%esi, %ebx
	movl	%ebx, %ecx
	negl	%ecx
	cmovll	%ebx, %ecx
	cmpl	$131072, %ecx           # imm = 0x20000
	jl	.LBB8_6
# BB#5:
	movq	24(%rdi), %rdi
	movl	$.L.str.1, %esi
	callq	luaX_syntaxerror
	movl	(%r14,%r15,4), %eax
.LBB8_6:                                # %fixjump.exit.i
	andl	$16383, %eax            # imm = 0x3FFF
	shll	$14, %ebx
	leal	2147467264(%rax,%rbx), %eax
	movl	%eax, (%r14,%r15,4)
	popq	%rbx
	popq	%r14
	popq	%r15
.LBB8_7:                                # %luaK_concat.exit
	retq
.LBB8_8:
	movl	%esi, 56(%rdi)
	retq
.Lfunc_end8:
	.size	luaK_patchtohere, .Lfunc_end8-luaK_patchtohere
	.cfi_endproc

	.p2align	4, 0x90
	.type	patchlistaux,@function
patchlistaux:                           # @patchlistaux
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 80
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movl	%r8d, 16(%rsp)          # 4-byte Spill
	movl	%ecx, %r12d
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movl	%esi, %ebp
	movq	%rdi, %r15
	cmpl	$-1, %ebp
	je	.LBB9_27
# BB#1:                                 # %.lr.ph
	cmpl	$255, %r12d
	jne	.LBB9_2
# BB#6:                                 # %.lr.ph.split.us.preheader
	movl	$16383, %r14d           # imm = 0x3FFF
	.p2align	4, 0x90
.LBB9_7:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %ebx
	movq	(%r15), %rax
	movq	24(%rax), %rax
	movslq	%ebx, %rcx
	leaq	(%rax,%rcx,4), %r12
	movl	(%rax,%rcx,4), %eax
	movl	%eax, %edx
	shrl	$14, %edx
	leal	-131071(%rdx), %esi
	cmpl	$-1, %esi
	leal	-131070(%rbx,%rdx), %ebp
	cmovel	%esi, %ebp
	testl	%ecx, %ecx
	jle	.LBB9_10
# BB#8:                                 #   in Loop: Header=BB9_7 Depth=1
	movl	-4(%r12), %edx
	movl	%edx, %ecx
	andl	$63, %ecx
	cmpb	$0, luaP_opmodes(%rcx)
	js	.LBB9_9
.LBB9_10:                               #   in Loop: Header=BB9_7 Depth=1
	movq	%r12, %rcx
	jmp	.LBB9_11
	.p2align	4, 0x90
.LBB9_9:                                #   in Loop: Header=BB9_7 Depth=1
	leaq	-4(%r12), %rcx
	movl	%edx, %eax
.LBB9_11:                               # %getjumpcontrol.exit.i.us
                                        #   in Loop: Header=BB9_7 Depth=1
	movl	%eax, %edx
	andl	$63, %edx
	cmpl	$27, %edx
	jne	.LBB9_12
# BB#13:                                #   in Loop: Header=BB9_7 Depth=1
	movl	%eax, %edx
	shrl	$17, %edx
	andl	$32704, %edx            # imm = 0x7FC0
	andl	$8372224, %eax          # imm = 0x7FC000
	orl	%edx, %eax
	orl	$26, %eax
	movl	%eax, (%rcx)
	notl	%ebx
	addl	12(%rsp), %ebx          # 4-byte Folded Reload
	jmp	.LBB9_14
	.p2align	4, 0x90
.LBB9_12:                               #   in Loop: Header=BB9_7 Depth=1
	notl	%ebx
	addl	16(%rsp), %ebx          # 4-byte Folded Reload
.LBB9_14:                               #   in Loop: Header=BB9_7 Depth=1
	movl	%ebx, %eax
	negl	%eax
	cmovll	%ebx, %eax
	cmpl	$131072, %eax           # imm = 0x20000
	jl	.LBB9_16
# BB#15:                                #   in Loop: Header=BB9_7 Depth=1
	movq	24(%r15), %rdi
	movl	$.L.str.1, %esi
	callq	luaX_syntaxerror
.LBB9_16:                               # %.backedge.us
                                        #   in Loop: Header=BB9_7 Depth=1
	movl	(%r12), %eax
	andl	%r14d, %eax
	shll	$14, %ebx
	leal	2147467264(%rax,%rbx), %eax
	movl	%eax, (%r12)
	cmpl	$-1, %ebp
	jne	.LBB9_7
	jmp	.LBB9_27
.LBB9_2:
	movl	%r12d, %ebx
	shll	$6, %ebx
	movl	%ebx, %eax
	andl	$16320, %eax            # imm = 0x3FC0
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	$16383, %edi            # imm = 0x3FFF
	.p2align	4, 0x90
.LBB9_3:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %r13d
	movq	(%r15), %rax
	movq	24(%rax), %rax
	movslq	%r13d, %rcx
	leaq	(%rax,%rcx,4), %r14
	movl	(%rax,%rcx,4), %eax
	movl	%eax, %edx
	shrl	$14, %edx
	leal	-131071(%rdx), %esi
	cmpl	$-1, %esi
	leal	-131070(%r13,%rdx), %ebp
	cmovel	%esi, %ebp
	testl	%ecx, %ecx
	jle	.LBB9_17
# BB#4:                                 #   in Loop: Header=BB9_3 Depth=1
	movl	-4(%r14), %edx
	movl	%edx, %ecx
	andl	$63, %ecx
	cmpb	$0, luaP_opmodes(%rcx)
	js	.LBB9_5
.LBB9_17:                               #   in Loop: Header=BB9_3 Depth=1
	movq	%r14, %rcx
	jmp	.LBB9_18
	.p2align	4, 0x90
.LBB9_5:                                #   in Loop: Header=BB9_3 Depth=1
	leaq	-4(%r14), %rcx
	movl	%edx, %eax
.LBB9_18:                               # %getjumpcontrol.exit.i
                                        #   in Loop: Header=BB9_3 Depth=1
	movl	%eax, %edx
	andl	$63, %edx
	cmpl	$27, %edx
	jne	.LBB9_23
# BB#19:                                #   in Loop: Header=BB9_3 Depth=1
	movl	%eax, %edx
	shrl	$23, %edx
	cmpl	%r12d, %edx
	jne	.LBB9_20
# BB#21:                                #   in Loop: Header=BB9_3 Depth=1
	andl	$8372224, %eax          # imm = 0x7FC000
	orl	%ebx, %eax
	orl	$26, %eax
	jmp	.LBB9_22
	.p2align	4, 0x90
.LBB9_23:                               #   in Loop: Header=BB9_3 Depth=1
	notl	%r13d
	addl	16(%rsp), %r13d         # 4-byte Folded Reload
	jmp	.LBB9_24
	.p2align	4, 0x90
.LBB9_20:                               #   in Loop: Header=BB9_3 Depth=1
	andl	$-16321, %eax           # imm = 0xC03F
	orl	20(%rsp), %eax          # 4-byte Folded Reload
.LBB9_22:                               #   in Loop: Header=BB9_3 Depth=1
	movl	%eax, (%rcx)
	notl	%r13d
	addl	12(%rsp), %r13d         # 4-byte Folded Reload
.LBB9_24:                               #   in Loop: Header=BB9_3 Depth=1
	movl	%r13d, %eax
	negl	%eax
	cmovll	%r13d, %eax
	cmpl	$131072, %eax           # imm = 0x20000
	jl	.LBB9_26
# BB#25:                                #   in Loop: Header=BB9_3 Depth=1
	movq	24(%r15), %rdi
	movl	$.L.str.1, %esi
	callq	luaX_syntaxerror
	movl	$16383, %edi            # imm = 0x3FFF
.LBB9_26:                               # %.backedge
                                        #   in Loop: Header=BB9_3 Depth=1
	movl	(%r14), %eax
	andl	%edi, %eax
	shll	$14, %r13d
	leal	2147467264(%rax,%r13), %eax
	movl	%eax, (%r14)
	cmpl	$-1, %ebp
	jne	.LBB9_3
.LBB9_27:                               # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	patchlistaux, .Lfunc_end9-patchlistaux
	.cfi_endproc

	.hidden	luaK_checkstack
	.globl	luaK_checkstack
	.p2align	4, 0x90
	.type	luaK_checkstack,@function
luaK_checkstack:                        # @luaK_checkstack
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 32
.Lcfi44:
	.cfi_offset %rbx, -24
.Lcfi45:
	.cfi_offset %r14, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	addl	60(%r14), %ebx
	movq	(%r14), %rax
	movzbl	115(%rax), %ecx
	cmpl	%ecx, %ebx
	jle	.LBB10_4
# BB#1:
	cmpl	$250, %ebx
	jl	.LBB10_3
# BB#2:
	movq	24(%r14), %rdi
	movl	$.L.str, %esi
	callq	luaX_syntaxerror
	movq	(%r14), %rax
.LBB10_3:
	movb	%bl, 115(%rax)
.LBB10_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	luaK_checkstack, .Lfunc_end10-luaK_checkstack
	.cfi_endproc

	.hidden	luaK_reserveregs
	.globl	luaK_reserveregs
	.p2align	4, 0x90
	.type	luaK_reserveregs,@function
luaK_reserveregs:                       # @luaK_reserveregs
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 32
.Lcfi49:
	.cfi_offset %rbx, -32
.Lcfi50:
	.cfi_offset %r14, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movl	60(%rbx), %eax
	leal	(%rax,%r14), %ebp
	movq	(%rbx), %rcx
	movzbl	115(%rcx), %edx
	cmpl	%edx, %ebp
	jle	.LBB11_4
# BB#1:
	cmpl	$250, %ebp
	jl	.LBB11_3
# BB#2:
	movq	24(%rbx), %rdi
	movl	$.L.str, %esi
	callq	luaX_syntaxerror
	movq	(%rbx), %rcx
	movl	60(%rbx), %eax
.LBB11_3:
	movb	%bpl, 115(%rcx)
.LBB11_4:                               # %luaK_checkstack.exit
	addl	%r14d, %eax
	movl	%eax, 60(%rbx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end11:
	.size	luaK_reserveregs, .Lfunc_end11-luaK_reserveregs
	.cfi_endproc

	.hidden	luaK_stringK
	.globl	luaK_stringK
	.p2align	4, 0x90
	.type	luaK_stringK,@function
luaK_stringK:                           # @luaK_stringK
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi52:
	.cfi_def_cfa_offset 32
	movq	%rsi, 8(%rsp)
	movl	$4, 16(%rsp)
	leaq	8(%rsp), %rsi
	movq	%rsi, %rdx
	callq	addk
	addq	$24, %rsp
	retq
.Lfunc_end12:
	.size	luaK_stringK, .Lfunc_end12-luaK_stringK
	.cfi_endproc

	.p2align	4, 0x90
	.type	addk,@function
addk:                                   # @addk
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 48
.Lcfi58:
	.cfi_offset %rbx, -48
.Lcfi59:
	.cfi_offset %r12, -40
.Lcfi60:
	.cfi_offset %r14, -32
.Lcfi61:
	.cfi_offset %r15, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movq	8(%rbx), %rsi
	movq	32(%rbx), %r14
	movq	%r14, %rdi
	movq	%rax, %rdx
	callq	luaH_set
	cmpl	$3, 8(%rax)
	jne	.LBB13_2
# BB#1:
	cvttsd2si	(%rax), %eax
	jmp	.LBB13_17
.LBB13_2:
	movq	(%rbx), %r12
	movl	76(%r12), %ebp
	movl	64(%rbx), %ecx
	cvtsi2sdl	%ecx, %xmm0
	movsd	%xmm0, (%rax)
	movl	$3, 8(%rax)
	cmpl	%ebp, %ecx
	jge	.LBB13_4
# BB#3:                                 # %..preheader_crit_edge
	movq	16(%r12), %rax
	movl	%ebp, %esi
	cmpl	%esi, %ebp
	jl	.LBB13_6
	jmp	.LBB13_12
.LBB13_4:
	leaq	76(%r12), %rdx
	movq	16(%r12), %rsi
	movl	$16, %ecx
	movl	$262143, %r8d           # imm = 0x3FFFF
	movl	$.L.str.2, %r9d
	movq	%r14, %rdi
	callq	luaM_growaux_
	movq	%rax, 16(%r12)
	movl	76(%r12), %esi
	cmpl	%esi, %ebp
	jge	.LBB13_12
.LBB13_6:                               # %.lr.ph
	movslq	%ebp, %rdi
	movslq	%esi, %rdx
	subl	%ebp, %esi
	leaq	-1(%rdx), %r8
	subq	%rdi, %r8
	andq	$7, %rsi
	je	.LBB13_9
# BB#7:                                 # %.prol.preheader
	movq	%rdi, %rcx
	shlq	$4, %rcx
	leaq	8(%rax,%rcx), %rbp
	negq	%rsi
	.p2align	4, 0x90
.LBB13_8:                               # =>This Inner Loop Header: Depth=1
	incq	%rdi
	movl	$0, (%rbp)
	addq	$16, %rbp
	incq	%rsi
	jne	.LBB13_8
.LBB13_9:                               # %.prol.loopexit
	cmpq	$7, %r8
	jb	.LBB13_12
# BB#10:                                # %.lr.ph.new
	movq	%rdi, %rcx
	shlq	$4, %rcx
	leaq	120(%rax,%rcx), %rsi
	.p2align	4, 0x90
.LBB13_11:                              # =>This Inner Loop Header: Depth=1
	movl	$0, -112(%rsi)
	movl	$0, -96(%rsi)
	movl	$0, -80(%rsi)
	movl	$0, -64(%rsi)
	movl	$0, -48(%rsi)
	movl	$0, -32(%rsi)
	movl	$0, -16(%rsi)
	addq	$8, %rdi
	movl	$0, (%rsi)
	subq	$-128, %rsi
	cmpq	%rdx, %rdi
	jl	.LBB13_11
.LBB13_12:                              # %._crit_edge
	movslq	64(%rbx), %rcx
	shlq	$4, %rcx
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%rcx)
	movl	8(%r15), %edx
	movl	%edx, 8(%rax,%rcx)
	cmpl	$4, 8(%r15)
	jl	.LBB13_16
# BB#13:
	movq	(%r15), %rdx
	testb	$3, 9(%rdx)
	je	.LBB13_16
# BB#14:
	testb	$4, 9(%r12)
	je	.LBB13_16
# BB#15:
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	luaC_barrierf
.LBB13_16:
	movl	64(%rbx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 64(%rbx)
.LBB13_17:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	addk, .Lfunc_end13-addk
	.cfi_endproc

	.hidden	luaK_numberK
	.globl	luaK_numberK
	.p2align	4, 0x90
	.type	luaK_numberK,@function
luaK_numberK:                           # @luaK_numberK
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi63:
	.cfi_def_cfa_offset 32
	movsd	%xmm0, 8(%rsp)
	movl	$3, 16(%rsp)
	leaq	8(%rsp), %rsi
	movq	%rsi, %rdx
	callq	addk
	addq	$24, %rsp
	retq
.Lfunc_end14:
	.size	luaK_numberK, .Lfunc_end14-luaK_numberK
	.cfi_endproc

	.hidden	luaK_setreturns
	.globl	luaK_setreturns
	.p2align	4, 0x90
	.type	luaK_setreturns,@function
luaK_setreturns:                        # @luaK_setreturns
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi66:
	.cfi_def_cfa_offset 32
.Lcfi67:
	.cfi_offset %rbx, -24
.Lcfi68:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, %rbx
	movl	(%rsi), %eax
	cmpl	$14, %eax
	je	.LBB15_3
# BB#1:
	cmpl	$13, %eax
	jne	.LBB15_8
# BB#2:
	movq	(%rbx), %rax
	movq	24(%rax), %rax
	movslq	8(%rsi), %rcx
	movl	$-8372225, %esi         # imm = 0xFF803FFF
	andl	(%rax,%rcx,4), %esi
	shll	$14, %edx
	addl	$16384, %edx            # imm = 0x4000
	andl	$8372224, %edx          # imm = 0x7FC000
	orl	%esi, %edx
	movl	%edx, (%rax,%rcx,4)
	jmp	.LBB15_8
.LBB15_3:
	movq	(%rbx), %rax
	movq	24(%rax), %rcx
	movslq	8(%rsi), %rdi
	movl	$8388607, %ebp          # imm = 0x7FFFFF
	andl	(%rcx,%rdi,4), %ebp
	shll	$23, %edx
	leal	8388608(%rbp,%rdx), %edx
	movl	%edx, (%rcx,%rdi,4)
	movslq	8(%rsi), %rdx
	movl	$-16321, %esi           # imm = 0xC03F
	andl	(%rcx,%rdx,4), %esi
	movl	60(%rbx), %edi
	shll	$6, %edi
	andl	$16320, %edi            # imm = 0x3FC0
	orl	%esi, %edi
	movl	%edi, (%rcx,%rdx,4)
	movl	60(%rbx), %ecx
	movzbl	115(%rax), %edx
	cmpl	%edx, %ecx
	jl	.LBB15_7
# BB#4:
	leal	1(%rcx), %ebp
	cmpl	$249, %ecx
	jl	.LBB15_6
# BB#5:
	movq	24(%rbx), %rdi
	movl	$.L.str, %esi
	callq	luaX_syntaxerror
	movq	(%rbx), %rax
	movl	60(%rbx), %ecx
.LBB15_6:
	movb	%bpl, 115(%rax)
.LBB15_7:                               # %luaK_reserveregs.exit
	incl	%ecx
	movl	%ecx, 60(%rbx)
.LBB15_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end15:
	.size	luaK_setreturns, .Lfunc_end15-luaK_setreturns
	.cfi_endproc

	.hidden	luaK_setoneret
	.globl	luaK_setoneret
	.p2align	4, 0x90
	.type	luaK_setoneret,@function
luaK_setoneret:                         # @luaK_setoneret
	.cfi_startproc
# BB#0:
	movl	(%rsi), %eax
	cmpl	$14, %eax
	je	.LBB16_3
# BB#1:
	cmpl	$13, %eax
	jne	.LBB16_4
# BB#2:
	movl	$12, (%rsi)
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	movslq	8(%rsi), %rcx
	movl	(%rax,%rcx,4), %eax
	shrl	$6, %eax
	movzbl	%al, %eax
	movl	%eax, 8(%rsi)
	retq
.LBB16_3:
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	movslq	8(%rsi), %rcx
	movl	$8388607, %edx          # imm = 0x7FFFFF
	andl	(%rax,%rcx,4), %edx
	orl	$16777216, %edx         # imm = 0x1000000
	movl	%edx, (%rax,%rcx,4)
	movl	$11, (%rsi)
.LBB16_4:
	retq
.Lfunc_end16:
	.size	luaK_setoneret, .Lfunc_end16-luaK_setoneret
	.cfi_endproc

	.hidden	luaK_dischargevars
	.globl	luaK_dischargevars
	.p2align	4, 0x90
	.type	luaK_dischargevars,@function
luaK_dischargevars:                     # @luaK_dischargevars
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 16
.Lcfi70:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movl	(%rbx), %eax
	addl	$-6, %eax
	cmpl	$8, %eax
	ja	.LBB17_17
# BB#1:
	jmpq	*.LJTI17_0(,%rax,8)
.LBB17_2:
	movl	$12, (%rbx)
	popq	%rbx
	retq
.LBB17_3:
	movl	8(%rbx), %esi
	shll	$23, %esi
	orl	$4, %esi
	jmp	.LBB17_4
.LBB17_6:
	movl	8(%rbx), %esi
	shll	$14, %esi
	orl	$5, %esi
.LBB17_4:                               # %luaK_setoneret.exit
	movq	24(%rdi), %rax
	movl	8(%rax), %edx
	jmp	.LBB17_5
.LBB17_7:
	movl	12(%rbx), %eax
	testb	$1, %ah
	jne	.LBB17_10
# BB#8:
	movzbl	74(%rdi), %ecx
	cmpl	%eax, %ecx
	jg	.LBB17_10
# BB#9:
	decl	60(%rdi)
.LBB17_10:                              # %freereg.exit
	movl	8(%rbx), %ecx
	testb	$1, %ch
	jne	.LBB17_13
# BB#11:
	movzbl	74(%rdi), %edx
	cmpl	%ecx, %edx
	jg	.LBB17_13
# BB#12:
	decl	60(%rdi)
.LBB17_13:                              # %freereg.exit21
	shll	$23, %ecx
	shll	$14, %eax
	orl	%ecx, %eax
	orl	$6, %eax
	movq	24(%rdi), %rcx
	movl	8(%rcx), %edx
	movl	%eax, %esi
.LBB17_5:                               # %luaK_setoneret.exit
	callq	luaK_code
	movl	%eax, 8(%rbx)
	jmp	.LBB17_16
.LBB17_15:
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	movslq	8(%rbx), %rcx
	movl	$8388607, %edx          # imm = 0x7FFFFF
	andl	(%rax,%rcx,4), %edx
	orl	$16777216, %edx         # imm = 0x1000000
	movl	%edx, (%rax,%rcx,4)
.LBB17_16:                              # %luaK_setoneret.exit
	movl	$11, (%rbx)
.LBB17_17:                              # %luaK_setoneret.exit
	popq	%rbx
	retq
.LBB17_14:
	movl	$12, (%rbx)
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	movslq	8(%rbx), %rcx
	movl	(%rax,%rcx,4), %eax
	shrl	$6, %eax
	movzbl	%al, %eax
	movl	%eax, 8(%rbx)
	popq	%rbx
	retq
.Lfunc_end17:
	.size	luaK_dischargevars, .Lfunc_end17-luaK_dischargevars
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI17_0:
	.quad	.LBB17_2
	.quad	.LBB17_3
	.quad	.LBB17_6
	.quad	.LBB17_7
	.quad	.LBB17_17
	.quad	.LBB17_17
	.quad	.LBB17_17
	.quad	.LBB17_14
	.quad	.LBB17_15

	.text
	.hidden	luaK_exp2nextreg
	.globl	luaK_exp2nextreg
	.p2align	4, 0x90
	.type	luaK_exp2nextreg,@function
luaK_exp2nextreg:                       # @luaK_exp2nextreg
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 32
.Lcfi74:
	.cfi_offset %rbx, -32
.Lcfi75:
	.cfi_offset %r14, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	luaK_dischargevars
	cmpl	$12, (%r14)
	jne	.LBB18_4
# BB#1:
	movl	8(%r14), %eax
	testb	$1, %ah
	jne	.LBB18_4
# BB#2:
	movzbl	74(%rbx), %ecx
	cmpl	%eax, %ecx
	jg	.LBB18_4
# BB#3:
	decl	60(%rbx)
.LBB18_4:                               # %freeexp.exit
	movl	60(%rbx), %edx
	movq	(%rbx), %rax
	movzbl	115(%rax), %ecx
	cmpl	%ecx, %edx
	jl	.LBB18_8
# BB#5:
	leal	1(%rdx), %ebp
	cmpl	$249, %edx
	jl	.LBB18_7
# BB#6:
	movq	24(%rbx), %rdi
	movl	$.L.str, %esi
	callq	luaX_syntaxerror
	movq	(%rbx), %rax
	movl	60(%rbx), %edx
.LBB18_7:
	movb	%bpl, 115(%rax)
.LBB18_8:                               # %luaK_reserveregs.exit
	leal	1(%rdx), %eax
	movl	%eax, 60(%rbx)
	movq	%rbx, %rdi
	movq	%r14, %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	exp2reg                 # TAILCALL
.Lfunc_end18:
	.size	luaK_exp2nextreg, .Lfunc_end18-luaK_exp2nextreg
	.cfi_endproc

	.p2align	4, 0x90
	.type	exp2reg,@function
exp2reg:                                # @exp2reg
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi80:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi81:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi83:
	.cfi_def_cfa_offset 80
.Lcfi84:
	.cfi_offset %rbx, -56
.Lcfi85:
	.cfi_offset %r12, -48
.Lcfi86:
	.cfi_offset %r13, -40
.Lcfi87:
	.cfi_offset %r14, -32
.Lcfi88:
	.cfi_offset %r15, -24
.Lcfi89:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r15
	movq	%rdi, %r12
	callq	discharge2reg
	cmpl	$10, (%r15)
	jne	.LBB19_9
# BB#1:
	movl	8(%r15), %r13d
	cmpl	$-1, %r13d
	je	.LBB19_9
# BB#2:
	movl	16(%r15), %edx
	cmpl	$-1, %edx
	je	.LBB19_8
# BB#3:                                 # %.preheader.i
	movq	(%r12), %rax
	movq	24(%rax), %rbx
	.p2align	4, 0x90
.LBB19_4:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, %ecx
	movslq	%ecx, %rbp
	movl	(%rbx,%rbp,4), %eax
	movl	%eax, %edx
	shrl	$14, %edx
	leal	-131071(%rdx), %esi
	cmpl	$-1, %esi
	leal	-131070(%rcx,%rdx), %edx
	cmovel	%esi, %edx
	cmpl	$-1, %edx
	jne	.LBB19_4
# BB#5:
	notl	%ecx
	addl	%ecx, %r13d
	movl	%r13d, %ecx
	negl	%ecx
	cmovll	%r13d, %ecx
	cmpl	$131072, %ecx           # imm = 0x20000
	jl	.LBB19_7
# BB#6:
	movq	24(%r12), %rdi
	movl	$.L.str.1, %esi
	callq	luaX_syntaxerror
	movl	(%rbx,%rbp,4), %eax
.LBB19_7:                               # %fixjump.exit.i
	andl	$16383, %eax            # imm = 0x3FFF
	shll	$14, %r13d
	leal	2147467264(%rax,%r13), %eax
	movl	%eax, (%rbx,%rbp,4)
	jmp	.LBB19_9
.LBB19_8:
	movl	%r13d, 16(%r15)
.LBB19_9:                               # %luaK_concat.exit
	movl	16(%r15), %edx
	movl	20(%r15), %eax
	cmpl	%eax, %edx
	je	.LBB19_49
# BB#10:
	cmpl	$-1, %edx
	je	.LBB19_17
# BB#11:                                # %.lr.ph.i
	movq	(%r12), %rcx
	movq	24(%rcx), %rcx
	.p2align	4, 0x90
.LBB19_12:                              # =>This Inner Loop Header: Depth=1
	movslq	%edx, %rdx
	testl	%edx, %edx
	jle	.LBB19_14
# BB#13:                                #   in Loop: Header=BB19_12 Depth=1
	movl	-4(%rcx,%rdx,4), %esi
	movl	%esi, %edi
	andl	$63, %edi
	cmpb	$0, luaP_opmodes(%rdi)
	js	.LBB19_15
.LBB19_14:                              #   in Loop: Header=BB19_12 Depth=1
	movl	(%rcx,%rdx,4), %esi
.LBB19_15:                              # %getjumpcontrol.exit.i
                                        #   in Loop: Header=BB19_12 Depth=1
	andl	$63, %esi
	cmpl	$27, %esi
	jne	.LBB19_25
# BB#16:                                # %select.unfold.i
                                        #   in Loop: Header=BB19_12 Depth=1
	movl	(%rcx,%rdx,4), %esi
	shrl	$14, %esi
	leal	-131071(%rsi), %edi
	cmpl	$-1, %edi
	leal	-131070(%rdx,%rsi), %edx
	cmovel	%edi, %edx
	cmpl	$-1, %edx
	jne	.LBB19_12
.LBB19_17:                              # %.loopexit
	movl	$-1, %ebx
	cmpl	$-1, %eax
	je	.LBB19_34
# BB#18:                                # %.lr.ph.i49
	movq	(%r12), %rcx
	movq	24(%rcx), %rcx
	.p2align	4, 0x90
.LBB19_19:                              # =>This Inner Loop Header: Depth=1
	cltq
	testl	%eax, %eax
	jle	.LBB19_21
# BB#20:                                #   in Loop: Header=BB19_19 Depth=1
	movl	-4(%rcx,%rax,4), %edx
	movl	%edx, %esi
	andl	$63, %esi
	cmpb	$0, luaP_opmodes(%rsi)
	js	.LBB19_22
.LBB19_21:                              #   in Loop: Header=BB19_19 Depth=1
	movl	(%rcx,%rax,4), %edx
.LBB19_22:                              # %getjumpcontrol.exit.i52
                                        #   in Loop: Header=BB19_19 Depth=1
	andl	$63, %edx
	cmpl	$27, %edx
	jne	.LBB19_25
# BB#23:                                # %select.unfold.i54
                                        #   in Loop: Header=BB19_19 Depth=1
	movl	(%rcx,%rax,4), %edx
	shrl	$14, %edx
	leal	-131071(%rdx), %esi
	cmpl	$-1, %esi
	leal	-131070(%rax,%rdx), %eax
	cmovel	%esi, %eax
	cmpl	$-1, %eax
	jne	.LBB19_19
# BB#24:
	movl	$-1, %r8d
	jmp	.LBB19_48
.LBB19_25:                              # %need_value.exit
	cmpl	$10, (%r15)
	movl	%r14d, 4(%rsp)          # 4-byte Spill
	jne	.LBB19_27
# BB#26:                                # %need_value.exit.luaK_jump.exit_crit_edge
	leaq	24(%r12), %r13
	leaq	56(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$-1, %r14d
	jmp	.LBB19_37
.LBB19_27:
	leaq	56(%r12), %rbx
	movl	56(%r12), %ebp
	movl	$-1, 56(%r12)
	leaq	24(%r12), %r13
	movq	24(%r12), %rax
	movl	8(%rax), %edx
	movl	$2147450902, %esi       # imm = 0x7FFF8016
	movq	%r12, %rdi
	callq	luaK_code
	movq	%rbp, %rdi
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$-1, %edi
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	je	.LBB19_35
# BB#28:
	cmpl	$-1, %eax
	je	.LBB19_36
# BB#29:                                # %.preheader.i.i
	movq	%r13, %rbx
	movq	(%r12), %rcx
	movq	24(%rcx), %rbp
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%eax, %edx
	.p2align	4, 0x90
.LBB19_30:                              # =>This Inner Loop Header: Depth=1
	movl	%edx, %ecx
	movslq	%ecx, %r13
	movl	(%rbp,%r13,4), %eax
	movl	%eax, %edx
	shrl	$14, %edx
	leal	-131071(%rdx), %esi
	cmpl	$-1, %esi
	leal	-131070(%rcx,%rdx), %edx
	cmovel	%esi, %edx
	cmpl	$-1, %edx
	jne	.LBB19_30
# BB#31:
	notl	%ecx
	addl	%ecx, %edi
	movl	%edi, %ecx
	negl	%ecx
	cmovll	%edi, %ecx
	cmpl	$131072, %ecx           # imm = 0x20000
	jl	.LBB19_33
# BB#32:
	movq	%rdi, %r14
	movq	(%rbx), %rdi
	movl	$.L.str.1, %esi
	callq	luaX_syntaxerror
	movq	%r14, %rdi
	movl	(%rbp,%r13,4), %eax
.LBB19_33:                              # %fixjump.exit.i.i
	andl	$16383, %eax            # imm = 0x3FFF
	shll	$14, %edi
	leal	2147467264(%rax,%rdi), %eax
	movl	%eax, (%rbp,%r13,4)
	movq	%rbx, %r13
	movq	8(%rsp), %r14           # 8-byte Reload
	jmp	.LBB19_37
.LBB19_34:
	movl	$-1, %r8d
	jmp	.LBB19_48
.LBB19_35:
	movq	%rax, %r14
	jmp	.LBB19_37
.LBB19_36:
	movl	%edi, %r14d
.LBB19_37:                              # %luaK_jump.exit
	movl	48(%r12), %eax
	movl	%eax, 52(%r12)
	movl	4(%rsp), %ebp           # 4-byte Reload
	shll	$6, %ebp
	movl	%ebp, %esi
	orl	$16386, %esi            # imm = 0x4002
	movq	(%r13), %rax
	movl	8(%rax), %edx
	movq	%r12, %rdi
	callq	luaK_code
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	48(%r12), %eax
	movl	%eax, 52(%r12)
	orl	$8388610, %ebp          # imm = 0x800002
	movq	(%r13), %rax
	movl	8(%rax), %edx
	movq	%r12, %rdi
	movl	%ebp, %esi
	callq	luaK_code
	movl	%eax, %ebx
	movl	48(%r12), %eax
	movl	%eax, 52(%r12)
	cmpl	$-1, %r14d
	je	.LBB19_46
# BB#38:
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %edx
	cmpl	$-1, %edx
	je	.LBB19_45
# BB#39:                                # %.preheader.i.i63
	movl	%ebx, 16(%rsp)          # 4-byte Spill
	movq	%r13, %r8
	movq	(%r12), %rax
	movq	24(%rax), %rbp
	.p2align	4, 0x90
.LBB19_40:                              # =>This Inner Loop Header: Depth=1
	movl	%edx, %ecx
	movslq	%ecx, %rbx
	movl	(%rbp,%rbx,4), %eax
	movl	%eax, %edx
	shrl	$14, %edx
	leal	-131071(%rdx), %esi
	cmpl	$-1, %esi
	leal	-131070(%rcx,%rdx), %edx
	cmovel	%esi, %edx
	cmpl	$-1, %edx
	jne	.LBB19_40
# BB#41:
	notl	%ecx
	movq	%r14, %r13
	addl	%ecx, %r13d
	movl	%r13d, %ecx
	negl	%ecx
	cmovll	%r13d, %ecx
	cmpl	$131072, %ecx           # imm = 0x20000
	movl	4(%rsp), %r14d          # 4-byte Reload
	jl	.LBB19_43
# BB#42:
	movq	(%r8), %rdi
	movl	$.L.str.1, %esi
	callq	luaX_syntaxerror
	movl	(%rbp,%rbx,4), %eax
.LBB19_43:                              # %fixjump.exit.i.i70
	andl	$16383, %eax            # imm = 0x3FFF
	shll	$14, %r13d
	leal	2147467264(%rax,%r13), %eax
	movl	%eax, (%rbp,%rbx,4)
	movl	16(%rsp), %ebx          # 4-byte Reload
	jmp	.LBB19_47
.LBB19_45:
	movl	%r14d, (%rax)
.LBB19_46:                              # %luaK_patchtohere.exit
	movl	4(%rsp), %r14d          # 4-byte Reload
.LBB19_47:                              # %luaK_patchtohere.exit
	movl	8(%rsp), %r8d           # 4-byte Reload
.LBB19_48:                              # %luaK_patchtohere.exit
	movl	48(%r12), %ebp
	movl	%ebp, 52(%r12)
	movl	20(%r15), %esi
	movq	%r12, %rdi
	movl	%ebp, %edx
	movl	%r14d, %ecx
	callq	patchlistaux
	movl	16(%r15), %esi
	movq	%r12, %rdi
	movl	%ebp, %edx
	movl	%r14d, %ecx
	movl	%ebx, %r8d
	callq	patchlistaux
.LBB19_49:
	movl	$-1, 16(%r15)
	movl	$-1, 20(%r15)
	movl	%r14d, 8(%r15)
	movl	$12, (%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	exp2reg, .Lfunc_end19-exp2reg
	.cfi_endproc

	.hidden	luaK_exp2anyreg
	.globl	luaK_exp2anyreg
	.p2align	4, 0x90
	.type	luaK_exp2anyreg,@function
luaK_exp2anyreg:                        # @luaK_exp2anyreg
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 32
.Lcfi93:
	.cfi_offset %rbx, -32
.Lcfi94:
	.cfi_offset %r14, -24
.Lcfi95:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	luaK_dischargevars
	cmpl	$12, (%rbx)
	jne	.LBB20_4
# BB#1:
	movl	8(%rbx), %eax
	movl	16(%rbx), %ecx
	cmpl	20(%rbx), %ecx
	je	.LBB20_14
# BB#2:
	movzbl	74(%r14), %ecx
	cmpl	%ecx, %eax
	jge	.LBB20_3
.LBB20_4:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	luaK_dischargevars
	cmpl	$12, (%rbx)
	jne	.LBB20_8
# BB#5:
	movl	8(%rbx), %eax
	testb	$1, %ah
	jne	.LBB20_8
# BB#6:
	movzbl	74(%r14), %ecx
	cmpl	%eax, %ecx
	jg	.LBB20_8
# BB#7:
	decl	60(%r14)
.LBB20_8:                               # %freeexp.exit.i
	movl	60(%r14), %edx
	movq	(%r14), %rax
	movzbl	115(%rax), %ecx
	cmpl	%ecx, %edx
	jl	.LBB20_12
# BB#9:
	leal	1(%rdx), %ebp
	cmpl	$249, %edx
	jl	.LBB20_11
# BB#10:
	movq	24(%r14), %rdi
	movl	$.L.str, %esi
	callq	luaX_syntaxerror
	movq	(%r14), %rax
	movl	60(%r14), %edx
.LBB20_11:
	movb	%bpl, 115(%rax)
.LBB20_12:                              # %luaK_exp2nextreg.exit
	leal	1(%rdx), %eax
	movl	%eax, 60(%r14)
	movq	%r14, %rdi
	movq	%rbx, %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	exp2reg
	addq	$8, %rbx
.LBB20_13:                              # %.sink.split
	movl	(%rbx), %eax
.LBB20_14:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB20_3:
	leaq	8(%rbx), %rbp
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movl	%eax, %edx
	callq	exp2reg
	movq	%rbp, %rbx
	jmp	.LBB20_13
.Lfunc_end20:
	.size	luaK_exp2anyreg, .Lfunc_end20-luaK_exp2anyreg
	.cfi_endproc

	.hidden	luaK_exp2val
	.globl	luaK_exp2val
	.p2align	4, 0x90
	.type	luaK_exp2val,@function
luaK_exp2val:                           # @luaK_exp2val
	.cfi_startproc
# BB#0:
	movl	16(%rsi), %eax
	cmpl	20(%rsi), %eax
	jne	.LBB21_1
# BB#2:
	jmp	luaK_dischargevars      # TAILCALL
.LBB21_1:
	jmp	luaK_exp2anyreg         # TAILCALL
.Lfunc_end21:
	.size	luaK_exp2val, .Lfunc_end21-luaK_exp2val
	.cfi_endproc

	.hidden	luaK_exp2RK
	.globl	luaK_exp2RK
	.p2align	4, 0x90
	.type	luaK_exp2RK,@function
luaK_exp2RK:                            # @luaK_exp2RK
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi98:
	.cfi_def_cfa_offset 64
.Lcfi99:
	.cfi_offset %rbx, -24
.Lcfi100:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	16(%rbx), %eax
	cmpl	20(%rbx), %eax
	jne	.LBB22_1
# BB#2:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	luaK_dischargevars
	jmp	.LBB22_3
.LBB22_1:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	luaK_exp2anyreg
.LBB22_3:                               # %luaK_exp2val.exit
	movl	(%rbx), %eax
	leal	-1(%rax), %ecx
	cmpl	$3, %ecx
	jb	.LBB22_6
# BB#4:                                 # %luaK_exp2val.exit
	cmpl	$4, %eax
	je	.LBB22_14
# BB#5:                                 # %luaK_exp2val.exit
	cmpl	$5, %eax
	jne	.LBB22_16
.LBB22_6:
	cmpl	$255, 64(%r14)
	jg	.LBB22_16
# BB#7:
	cmpl	$5, %eax
	je	.LBB22_10
# BB#8:
	cmpl	$1, %eax
	jne	.LBB22_11
# BB#9:
	movl	$0, 32(%rsp)
	movq	8(%r14), %rax
	movq	%rax, 8(%rsp)
	movl	$5, 16(%rsp)
	leaq	8(%rsp), %rsi
	leaq	24(%rsp), %rdx
	movq	%r14, %rdi
	jmp	.LBB22_13
.LBB22_14:
	movl	8(%rbx), %eax
	cmpl	$255, %eax
	jle	.LBB22_15
.LBB22_16:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	jmp	luaK_exp2anyreg         # TAILCALL
.LBB22_10:
	movq	8(%rbx), %rax
	movq	%rax, 8(%rsp)
	movl	$3, 16(%rsp)
	jmp	.LBB22_12
.LBB22_11:
	xorl	%ecx, %ecx
	cmpl	$2, %eax
	sete	%cl
	movl	%ecx, 8(%rsp)
	movl	$1, 16(%rsp)
.LBB22_12:
	leaq	8(%rsp), %rsi
	movq	%r14, %rdi
	movq	%rsi, %rdx
.LBB22_13:
	callq	addk
	movl	%eax, 8(%rbx)
	movl	$4, (%rbx)
.LBB22_15:
	orl	$256, %eax              # imm = 0x100
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end22:
	.size	luaK_exp2RK, .Lfunc_end22-luaK_exp2RK
	.cfi_endproc

	.hidden	luaK_storevar
	.globl	luaK_storevar
	.p2align	4, 0x90
	.type	luaK_storevar,@function
luaK_storevar:                          # @luaK_storevar
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 32
.Lcfi104:
	.cfi_offset %rbx, -32
.Lcfi105:
	.cfi_offset %r14, -24
.Lcfi106:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	(%rbx), %eax
	addl	$-6, %eax
	cmpl	$3, %eax
	ja	.LBB23_11
# BB#1:
	jmpq	*.LJTI23_0(,%rax,8)
.LBB23_2:
	cmpl	$12, (%r15)
	jne	.LBB23_6
# BB#3:
	movl	8(%r15), %eax
	testb	$1, %ah
	jne	.LBB23_6
# BB#4:
	movzbl	74(%r14), %ecx
	cmpl	%eax, %ecx
	jg	.LBB23_6
# BB#5:
	decl	60(%r14)
.LBB23_6:                               # %freeexp.exit
	movl	8(%rbx), %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	exp2reg                 # TAILCALL
.LBB23_8:
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	luaK_exp2anyreg
	movl	8(%rbx), %esi
	shll	$6, %eax
	shll	$14, %esi
	orl	%eax, %esi
	orl	$7, %esi
	jmp	.LBB23_10
.LBB23_9:
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	luaK_exp2RK
	movl	8(%rbx), %esi
	movl	12(%rbx), %ecx
	shll	$6, %esi
	shll	$23, %ecx
	shll	$14, %eax
	orl	%eax, %esi
	orl	%ecx, %esi
	orl	$9, %esi
	jmp	.LBB23_10
.LBB23_7:
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	luaK_exp2anyreg
	movl	8(%rbx), %esi
	shll	$6, %eax
	shll	$23, %esi
	orl	%eax, %esi
	orl	$8, %esi
.LBB23_10:
	movq	24(%r14), %rax
	movl	8(%rax), %edx
	movq	%r14, %rdi
	callq	luaK_code
.LBB23_11:
	cmpl	$12, (%r15)
	jne	.LBB23_15
# BB#12:
	movl	8(%r15), %eax
	testb	$1, %ah
	jne	.LBB23_15
# BB#13:
	movzbl	74(%r14), %ecx
	cmpl	%eax, %ecx
	jg	.LBB23_15
# BB#14:
	decl	60(%r14)
.LBB23_15:                              # %freeexp.exit28
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end23:
	.size	luaK_storevar, .Lfunc_end23-luaK_storevar
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI23_0:
	.quad	.LBB23_2
	.quad	.LBB23_7
	.quad	.LBB23_8
	.quad	.LBB23_9

	.text
	.hidden	luaK_self
	.globl	luaK_self
	.p2align	4, 0x90
	.type	luaK_self,@function
luaK_self:                              # @luaK_self
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi107:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi108:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi109:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi110:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 48
.Lcfi112:
	.cfi_offset %rbx, -48
.Lcfi113:
	.cfi_offset %r12, -40
.Lcfi114:
	.cfi_offset %r14, -32
.Lcfi115:
	.cfi_offset %r15, -24
.Lcfi116:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	luaK_exp2anyreg
	cmpl	$12, (%r14)
	jne	.LBB24_4
# BB#1:
	movl	8(%r14), %eax
	testb	$1, %ah
	jne	.LBB24_4
# BB#2:
	movzbl	74(%rbx), %ecx
	cmpl	%eax, %ecx
	jg	.LBB24_4
# BB#3:
	decl	60(%rbx)
.LBB24_4:                               # %freeexp.exit
	movl	60(%rbx), %r12d
	leal	2(%r12), %ebp
	movq	(%rbx), %rax
	movzbl	115(%rax), %ecx
	cmpl	%ecx, %ebp
	movl	%r12d, %ecx
	jle	.LBB24_8
# BB#5:
	cmpl	$250, %ebp
	movl	%r12d, %ecx
	jl	.LBB24_7
# BB#6:
	movq	24(%rbx), %rdi
	movl	$.L.str, %esi
	callq	luaX_syntaxerror
	movq	(%rbx), %rax
	movl	60(%rbx), %ecx
.LBB24_7:
	movb	%bpl, 115(%rax)
.LBB24_8:                               # %luaK_reserveregs.exit
	addl	$2, %ecx
	movl	%ecx, 60(%rbx)
	movl	8(%r14), %ebp
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	luaK_exp2RK
	movl	%r12d, %ecx
	shll	$6, %ecx
	shll	$23, %ebp
	shll	$14, %eax
	orl	%ecx, %ebp
	orl	%eax, %ebp
	orl	$11, %ebp
	movq	24(%rbx), %rax
	movl	8(%rax), %edx
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	luaK_code
	cmpl	$12, (%r15)
	jne	.LBB24_12
# BB#9:
	movl	8(%r15), %eax
	testb	$1, %ah
	jne	.LBB24_12
# BB#10:
	movzbl	74(%rbx), %ecx
	cmpl	%eax, %ecx
	jg	.LBB24_12
# BB#11:
	decl	60(%rbx)
.LBB24_12:                              # %freeexp.exit16
	movl	%r12d, 8(%r14)
	movl	$12, (%r14)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	luaK_self, .Lfunc_end24-luaK_self
	.cfi_endproc

	.hidden	luaK_goiftrue
	.globl	luaK_goiftrue
	.p2align	4, 0x90
	.type	luaK_goiftrue,@function
luaK_goiftrue:                          # @luaK_goiftrue
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi117:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi118:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi119:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi120:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi121:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi122:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi123:
	.cfi_def_cfa_offset 64
.Lcfi124:
	.cfi_offset %rbx, -56
.Lcfi125:
	.cfi_offset %r12, -48
.Lcfi126:
	.cfi_offset %r13, -40
.Lcfi127:
	.cfi_offset %r14, -32
.Lcfi128:
	.cfi_offset %r15, -24
.Lcfi129:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	callq	luaK_dischargevars
	movl	(%r14), %eax
	addl	$-2, %eax
	cmpl	$8, %eax
	ja	.LBB25_14
# BB#1:
	jmpq	*.LJTI25_0(,%rax,8)
.LBB25_2:
	movl	56(%r13), %r12d
	movl	$-1, 56(%r13)
	movq	24(%r13), %rax
	movl	8(%rax), %edx
	movl	$2147450902, %esi       # imm = 0x7FFF8016
	movq	%r13, %rdi
	callq	luaK_code
	movl	%eax, %r15d
	cmpl	$-1, %r12d
	je	.LBB25_15
# BB#3:
	cmpl	$-1, %r15d
	je	.LBB25_16
# BB#4:                                 # %.preheader.i.i
	movq	(%r13), %rax
	movq	24(%rax), %rbx
	movl	%r15d, %edx
	.p2align	4, 0x90
.LBB25_5:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, %ecx
	movslq	%ecx, %rbp
	movl	(%rbx,%rbp,4), %eax
	movl	%eax, %edx
	shrl	$14, %edx
	leal	-131071(%rdx), %esi
	cmpl	$-1, %esi
	leal	-131070(%rcx,%rdx), %edx
	cmovel	%esi, %edx
	cmpl	$-1, %edx
	jne	.LBB25_5
# BB#6:
	notl	%ecx
	addl	%ecx, %r12d
	movl	%r12d, %ecx
	negl	%ecx
	cmovll	%r12d, %ecx
	cmpl	$131072, %ecx           # imm = 0x20000
	jl	.LBB25_8
# BB#7:
	movq	24(%r13), %rdi
	movl	$.L.str.1, %esi
	callq	luaX_syntaxerror
	movl	(%rbx,%rbp,4), %eax
.LBB25_8:                               # %fixjump.exit.i.i
	andl	$16383, %eax            # imm = 0x3FFF
	shll	$14, %r12d
	leal	2147467264(%rax,%r12), %eax
	movl	%eax, (%rbx,%rbp,4)
	jmp	.LBB25_15
.LBB25_14:
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	jumponcond
	movl	%eax, %r15d
	jmp	.LBB25_15
.LBB25_9:
	movq	(%r13), %rcx
	movslq	8(%r14), %rdx
	leaq	(,%rdx,4), %rax
	addq	24(%rcx), %rax
	testq	%rdx, %rdx
	jle	.LBB25_12
# BB#10:
	movl	-4(%rax), %ecx
	movl	%ecx, %edx
	andl	$63, %edx
	cmpb	$0, luaP_opmodes(%rdx)
	js	.LBB25_11
.LBB25_12:
	movl	(%rax), %ecx
	jmp	.LBB25_13
.LBB25_11:
	addq	$-4, %rax
.LBB25_13:                              # %invertjump.exit
	movl	%ecx, %edx
	andl	$-16321, %edx           # imm = 0xC03F
	xorl	%esi, %esi
	testw	$16320, %cx             # imm = 0x3FC0
	sete	%sil
	shll	$6, %esi
	orl	%edx, %esi
	movl	%esi, (%rax)
	movl	8(%r14), %r15d
.LBB25_15:                              # %luaK_jump.exit
	cmpl	$-1, %r15d
	movl	%r15d, %r12d
	je	.LBB25_22
.LBB25_16:                              # %luaK_jump.exit.thread31
	movl	20(%r14), %edx
	cmpl	$-1, %edx
	je	.LBB25_30
# BB#17:                                # %.preheader.i
	movq	(%r13), %rax
	movq	24(%rax), %rbp
	.p2align	4, 0x90
.LBB25_18:                              # =>This Inner Loop Header: Depth=1
	movl	%edx, %ecx
	movslq	%ecx, %rbx
	movl	(%rbp,%rbx,4), %eax
	movl	%eax, %edx
	shrl	$14, %edx
	leal	-131071(%rdx), %esi
	cmpl	$-1, %esi
	leal	-131070(%rcx,%rdx), %edx
	cmovel	%esi, %edx
	cmpl	$-1, %edx
	jne	.LBB25_18
# BB#19:
	notl	%ecx
	addl	%ecx, %r12d
	movl	%r12d, %ecx
	negl	%ecx
	cmovll	%r12d, %ecx
	cmpl	$131072, %ecx           # imm = 0x20000
	jl	.LBB25_21
# BB#20:
	movq	24(%r13), %rdi
	movl	$.L.str.1, %esi
	callq	luaX_syntaxerror
	movl	(%rbp,%rbx,4), %eax
.LBB25_21:                              # %fixjump.exit.i
	andl	$16383, %eax            # imm = 0x3FFF
	shll	$14, %r12d
	leal	2147467264(%rax,%r12), %eax
	movl	%eax, (%rbp,%rbx,4)
	jmp	.LBB25_22
.LBB25_30:
	movl	%r12d, 20(%r14)
.LBB25_22:                              # %luaK_concat.exit
	movl	16(%r14), %r15d
	movl	48(%r13), %eax
	movl	%eax, 52(%r13)
	cmpl	$-1, %r15d
	je	.LBB25_29
# BB#23:
	movl	56(%r13), %edx
	cmpl	$-1, %edx
	je	.LBB25_31
# BB#24:                                # %.preheader.i.i23
	movq	(%r13), %rax
	movq	24(%rax), %rbx
	.p2align	4, 0x90
.LBB25_25:                              # =>This Inner Loop Header: Depth=1
	movl	%edx, %ecx
	movslq	%ecx, %rbp
	movl	(%rbx,%rbp,4), %eax
	movl	%eax, %edx
	shrl	$14, %edx
	leal	-131071(%rdx), %esi
	cmpl	$-1, %esi
	leal	-131070(%rcx,%rdx), %edx
	cmovel	%esi, %edx
	cmpl	$-1, %edx
	jne	.LBB25_25
# BB#26:
	notl	%ecx
	addl	%ecx, %r15d
	movl	%r15d, %ecx
	negl	%ecx
	cmovll	%r15d, %ecx
	cmpl	$131072, %ecx           # imm = 0x20000
	jl	.LBB25_28
# BB#27:
	movq	24(%r13), %rdi
	movl	$.L.str.1, %esi
	callq	luaX_syntaxerror
	movl	(%rbx,%rbp,4), %eax
.LBB25_28:                              # %fixjump.exit.i.i30
	andl	$16383, %eax            # imm = 0x3FFF
	shll	$14, %r15d
	leal	2147467264(%rax,%r15), %eax
	movl	%eax, (%rbx,%rbp,4)
	jmp	.LBB25_29
.LBB25_31:
	movl	%r15d, 56(%r13)
.LBB25_29:                              # %luaK_patchtohere.exit
	movl	$-1, 16(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	luaK_goiftrue, .Lfunc_end25-luaK_goiftrue
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI25_0:
	.quad	.LBB25_22
	.quad	.LBB25_2
	.quad	.LBB25_22
	.quad	.LBB25_22
	.quad	.LBB25_14
	.quad	.LBB25_14
	.quad	.LBB25_14
	.quad	.LBB25_14
	.quad	.LBB25_9

	.text
	.p2align	4, 0x90
	.type	jumponcond,@function
jumponcond:                             # @jumponcond
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi130:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi131:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi134:
	.cfi_def_cfa_offset 48
.Lcfi135:
	.cfi_offset %rbx, -40
.Lcfi136:
	.cfi_offset %r14, -32
.Lcfi137:
	.cfi_offset %r15, -24
.Lcfi138:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movl	(%rbp), %eax
	cmpl	$12, %eax
	je	.LBB26_10
# BB#1:
	cmpl	$11, %eax
	jne	.LBB26_4
# BB#2:
	movq	(%rbx), %rax
	movq	24(%rax), %rax
	movslq	8(%rbp), %rcx
	movl	(%rax,%rcx,4), %edx
	movl	%edx, %eax
	andl	$63, %eax
	cmpl	$19, %eax
	jne	.LBB26_4
# BB#3:
	decl	48(%rbx)
	shrl	$23, %edx
	xorl	%r8d, %r8d
	testl	%r14d, %r14d
	sete	%r8b
	movl	$26, %esi
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	jmp	.LBB26_14
.LBB26_4:                               # %.thread.thread
	movl	60(%rbx), %edx
	movq	(%rbx), %rax
	movzbl	115(%rax), %ecx
	cmpl	%ecx, %edx
	jl	.LBB26_8
# BB#5:
	leal	1(%rdx), %r15d
	cmpl	$249, %edx
	jl	.LBB26_7
# BB#6:
	movq	24(%rbx), %rdi
	movl	$.L.str, %esi
	callq	luaX_syntaxerror
	movq	(%rbx), %rax
	movl	60(%rbx), %edx
.LBB26_7:
	movb	%r15b, 115(%rax)
.LBB26_8:                               # %discharge2anyreg.exit
	leal	1(%rdx), %eax
	movl	%eax, 60(%rbx)
	movq	%rbx, %rdi
	movq	%rbp, %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	discharge2reg
	cmpl	$12, (%rbp)
	jne	.LBB26_9
.LBB26_10:                              # %discharge2anyreg.exit.thread
	movl	8(%rbp), %ecx
	testb	$1, %ch
	jne	.LBB26_13
# BB#11:
	movzbl	74(%rbx), %eax
	cmpl	%ecx, %eax
	jg	.LBB26_13
# BB#12:
	decl	60(%rbx)
	jmp	.LBB26_13
.LBB26_9:                               # %discharge2anyreg.exit.freeexp.exit_crit_edge
	movl	8(%rbp), %ecx
.LBB26_13:                              # %freeexp.exit
	movl	$27, %esi
	movl	$255, %edx
	movq	%rbx, %rdi
	movl	%r14d, %r8d
.LBB26_14:                              # %freeexp.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	condjump                # TAILCALL
.Lfunc_end26:
	.size	jumponcond, .Lfunc_end26-jumponcond
	.cfi_endproc

	.hidden	luaK_indexed
	.globl	luaK_indexed
	.p2align	4, 0x90
	.type	luaK_indexed,@function
luaK_indexed:                           # @luaK_indexed
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi139:
	.cfi_def_cfa_offset 16
.Lcfi140:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	callq	luaK_exp2RK
	movl	%eax, 12(%rbx)
	movl	$9, (%rbx)
	popq	%rbx
	retq
.Lfunc_end27:
	.size	luaK_indexed, .Lfunc_end27-luaK_indexed
	.cfi_endproc

	.hidden	luaK_prefix
	.globl	luaK_prefix
	.p2align	4, 0x90
	.type	luaK_prefix,@function
luaK_prefix:                            # @luaK_prefix
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi141:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi142:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi143:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi144:
	.cfi_def_cfa_offset 64
.Lcfi145:
	.cfi_offset %rbx, -32
.Lcfi146:
	.cfi_offset %r14, -24
.Lcfi147:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rdi, %r14
	movl	$-1, 28(%rsp)
	movl	$-1, 24(%rsp)
	movl	$5, 8(%rsp)
	movq	$0, 16(%rsp)
	cmpl	$2, %esi
	je	.LBB28_46
# BB#1:
	cmpl	$1, %esi
	je	.LBB28_8
# BB#2:
	testl	%esi, %esi
	jne	.LBB28_48
# BB#3:
	cmpl	$5, (%rbx)
	jne	.LBB28_6
# BB#4:
	cmpl	$-1, 16(%rbx)
	jne	.LBB28_6
# BB#5:                                 # %isnumeral.exit
	cmpl	$-1, 20(%rbx)
	je	.LBB28_7
.LBB28_6:                               # %isnumeral.exit.thread
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	luaK_exp2anyreg
.LBB28_7:
	leaq	8(%rsp), %rcx
	movl	$18, %esi
	jmp	.LBB28_47
.LBB28_46:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	luaK_exp2anyreg
	leaq	8(%rsp), %rcx
	movl	$20, %esi
.LBB28_47:                              # %codenot.exit
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	codearith
	jmp	.LBB28_48
.LBB28_8:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	luaK_dischargevars
	movl	(%rbx), %eax
	decl	%eax
	cmpl	$11, %eax
	ja	.LBB28_27
# BB#9:
	jmpq	*.LJTI28_0(,%rax,8)
.LBB28_11:
	movl	$3, (%rbx)
	jmp	.LBB28_27
.LBB28_10:
	movl	$2, (%rbx)
	jmp	.LBB28_27
.LBB28_12:
	movq	(%r14), %rcx
	movslq	8(%rbx), %rdx
	leaq	(,%rdx,4), %rax
	addq	24(%rcx), %rax
	testq	%rdx, %rdx
	jle	.LBB28_15
# BB#13:
	movl	-4(%rax), %ecx
	movl	%ecx, %edx
	andl	$63, %edx
	cmpb	$0, luaP_opmodes(%rdx)
	js	.LBB28_14
.LBB28_15:
	movl	(%rax), %ecx
	jmp	.LBB28_16
.LBB28_17:
	movl	60(%r14), %edx
	movq	(%r14), %rax
	movzbl	115(%rax), %ecx
	cmpl	%ecx, %edx
	jl	.LBB28_21
# BB#18:
	leal	1(%rdx), %ebp
	cmpl	$249, %edx
	jl	.LBB28_20
# BB#19:
	movq	24(%r14), %rdi
	movl	$.L.str, %esi
	callq	luaX_syntaxerror
	movq	(%r14), %rax
	movl	60(%r14), %edx
.LBB28_20:
	movb	%bpl, 115(%rax)
.LBB28_21:                              # %discharge2anyreg.exit.i
	leal	1(%rdx), %eax
	movl	%eax, 60(%r14)
	movq	%r14, %rdi
	movq	%rbx, %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	discharge2reg
	cmpl	$12, (%rbx)
	jne	.LBB28_22
.LBB28_23:                              # %discharge2anyreg.exit.thread.i
	movl	8(%rbx), %eax
	testb	$1, %ah
	jne	.LBB28_26
# BB#24:
	movzbl	74(%r14), %ecx
	cmpl	%eax, %ecx
	jg	.LBB28_26
# BB#25:
	decl	60(%r14)
	jmp	.LBB28_26
.LBB28_22:                              # %discharge2anyreg.exit.freeexp.exit_crit_edge.i
	movl	8(%rbx), %eax
.LBB28_26:                              # %freeexp.exit.i
	shll	$23, %eax
	orl	$19, %eax
	movq	24(%r14), %rcx
	movl	8(%rcx), %edx
	movq	%r14, %rdi
	movl	%eax, %esi
	callq	luaK_code
	movl	%eax, 8(%rbx)
	movl	$11, (%rbx)
	jmp	.LBB28_27
.LBB28_14:
	addq	$-4, %rax
.LBB28_16:                              # %invertjump.exit.i
	movl	%ecx, %edx
	andl	$-16321, %edx           # imm = 0xC03F
	xorl	%esi, %esi
	testw	$16320, %cx             # imm = 0x3FC0
	sete	%sil
	shll	$6, %esi
	orl	%edx, %esi
	movl	%esi, (%rax)
.LBB28_27:
	movl	16(%rbx), %ecx
	movl	20(%rbx), %eax
	movl	%ecx, 20(%rbx)
	movl	%eax, 16(%rbx)
	cmpl	$-1, %ecx
	je	.LBB28_37
# BB#28:                                # %.lr.ph.i28.i
	movq	(%r14), %rax
	movq	24(%rax), %rax
	.p2align	4, 0x90
.LBB28_29:                              # =>This Inner Loop Header: Depth=1
	movslq	%ecx, %rsi
	leaq	(%rax,%rsi,4), %rdx
	testl	%esi, %esi
	jle	.LBB28_32
# BB#30:                                #   in Loop: Header=BB28_29 Depth=1
	movl	-4(%rdx), %esi
	movl	%esi, %edi
	andl	$63, %edi
	cmpb	$0, luaP_opmodes(%rdi)
	js	.LBB28_31
.LBB28_32:                              #   in Loop: Header=BB28_29 Depth=1
	movl	(%rdx), %esi
	movq	%rdx, %rdi
	jmp	.LBB28_33
	.p2align	4, 0x90
.LBB28_31:                              #   in Loop: Header=BB28_29 Depth=1
	leaq	-4(%rdx), %rdi
.LBB28_33:                              # %getjumpcontrol.exit.i.i32.i
                                        #   in Loop: Header=BB28_29 Depth=1
	movl	%esi, %ebp
	andl	$63, %ebp
	cmpl	$27, %ebp
	jne	.LBB28_35
# BB#34:                                #   in Loop: Header=BB28_29 Depth=1
	movl	%esi, %ebp
	shrl	$17, %ebp
	andl	$32704, %ebp            # imm = 0x7FC0
	andl	$8372224, %esi          # imm = 0x7FC000
	orl	%ebp, %esi
	orl	$26, %esi
	movl	%esi, (%rdi)
.LBB28_35:                              # %patchtestreg.exit.i34.i
                                        #   in Loop: Header=BB28_29 Depth=1
	movl	(%rdx), %edx
	shrl	$14, %edx
	leal	-131071(%rdx), %esi
	cmpl	$-1, %esi
	leal	-131070(%rcx,%rdx), %ecx
	cmovel	%esi, %ecx
	cmpl	$-1, %ecx
	jne	.LBB28_29
# BB#36:                                # %removevalues.exit35thread-pre-split.i
	movl	16(%rbx), %eax
.LBB28_37:                              # %removevalues.exit35.i
	cmpl	$-1, %eax
	je	.LBB28_48
# BB#38:                                # %.lr.ph.i.i
	movq	(%r14), %rcx
	movq	24(%rcx), %rcx
	.p2align	4, 0x90
.LBB28_39:                              # =>This Inner Loop Header: Depth=1
	movslq	%eax, %rsi
	leaq	(%rcx,%rsi,4), %rdx
	testl	%esi, %esi
	jle	.LBB28_42
# BB#40:                                #   in Loop: Header=BB28_39 Depth=1
	movl	-4(%rdx), %esi
	movl	%esi, %edi
	andl	$63, %edi
	cmpb	$0, luaP_opmodes(%rdi)
	js	.LBB28_41
.LBB28_42:                              #   in Loop: Header=BB28_39 Depth=1
	movl	(%rdx), %esi
	movq	%rdx, %rdi
	jmp	.LBB28_43
	.p2align	4, 0x90
.LBB28_41:                              #   in Loop: Header=BB28_39 Depth=1
	leaq	-4(%rdx), %rdi
.LBB28_43:                              # %getjumpcontrol.exit.i.i.i
                                        #   in Loop: Header=BB28_39 Depth=1
	movl	%esi, %ebp
	andl	$63, %ebp
	cmpl	$27, %ebp
	jne	.LBB28_45
# BB#44:                                #   in Loop: Header=BB28_39 Depth=1
	movl	%esi, %ebp
	shrl	$17, %ebp
	andl	$32704, %ebp            # imm = 0x7FC0
	andl	$8372224, %esi          # imm = 0x7FC000
	orl	%ebp, %esi
	orl	$26, %esi
	movl	%esi, (%rdi)
.LBB28_45:                              # %patchtestreg.exit.i.i
                                        #   in Loop: Header=BB28_39 Depth=1
	movl	(%rdx), %edx
	shrl	$14, %edx
	leal	-131071(%rdx), %esi
	cmpl	$-1, %esi
	leal	-131070(%rax,%rdx), %eax
	cmovel	%esi, %eax
	cmpl	$-1, %eax
	jne	.LBB28_39
.LBB28_48:                              # %codenot.exit
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end28:
	.size	luaK_prefix, .Lfunc_end28-luaK_prefix
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI28_0:
	.quad	.LBB28_10
	.quad	.LBB28_11
	.quad	.LBB28_10
	.quad	.LBB28_11
	.quad	.LBB28_11
	.quad	.LBB28_27
	.quad	.LBB28_27
	.quad	.LBB28_27
	.quad	.LBB28_27
	.quad	.LBB28_12
	.quad	.LBB28_17
	.quad	.LBB28_23

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI29_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.p2align	4, 0x90
	.type	codearith,@function
codearith:                              # @codearith
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi148:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi149:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi150:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi151:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi152:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi153:
	.cfi_def_cfa_offset 80
.Lcfi154:
	.cfi_offset %rbx, -48
.Lcfi155:
	.cfi_offset %r12, -40
.Lcfi156:
	.cfi_offset %r14, -32
.Lcfi157:
	.cfi_offset %r15, -24
.Lcfi158:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, %rbx
	movl	%esi, %r15d
	movq	%rdi, %r14
	cmpl	$5, (%rbx)
	jne	.LBB29_20
# BB#1:
	cmpl	$-1, 16(%rbx)
	jne	.LBB29_20
# BB#2:                                 # %isnumeral.exit.i
	cmpl	$-1, 20(%rbx)
	jne	.LBB29_20
# BB#3:
	cmpl	$5, (%r12)
	jne	.LBB29_20
# BB#4:
	cmpl	$-1, 16(%r12)
	jne	.LBB29_20
# BB#5:                                 # %isnumeral.exit32.i
	cmpl	$-1, 20(%r12)
	je	.LBB29_6
.LBB29_20:
	xorl	%ebp, %ebp
	cmpl	$18, %r15d
	je	.LBB29_23
# BB#21:
	cmpl	$20, %r15d
	je	.LBB29_23
.LBB29_22:                              # %.thread
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	luaK_exp2RK
	movl	%eax, %ebp
.LBB29_23:                              # %.thread30
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	luaK_exp2RK
	cmpl	%ebp, %eax
	jle	.LBB29_30
# BB#24:
	cmpl	$12, (%rbx)
	jne	.LBB29_28
# BB#25:
	movl	8(%rbx), %ecx
	testb	$1, %ch
	jne	.LBB29_28
# BB#26:
	movzbl	74(%r14), %edx
	cmpl	%ecx, %edx
	jg	.LBB29_28
# BB#27:
	decl	60(%r14)
.LBB29_28:                              # %freeexp.exit
	cmpl	$12, (%r12)
	jne	.LBB29_39
# BB#29:
	movl	8(%r12), %ecx
	testb	$1, %ch
	je	.LBB29_37
	jmp	.LBB29_39
.LBB29_30:
	cmpl	$12, (%r12)
	jne	.LBB29_34
# BB#31:
	movl	8(%r12), %ecx
	testb	$1, %ch
	jne	.LBB29_34
# BB#32:
	movzbl	74(%r14), %edx
	cmpl	%ecx, %edx
	jg	.LBB29_34
# BB#33:
	decl	60(%r14)
.LBB29_34:                              # %freeexp.exit28
	cmpl	$12, (%rbx)
	jne	.LBB29_39
# BB#35:
	movl	8(%rbx), %ecx
	testb	$1, %ch
	jne	.LBB29_39
.LBB29_37:
	movzbl	74(%r14), %edx
	cmpl	%ecx, %edx
	jg	.LBB29_39
# BB#38:
	decl	60(%r14)
.LBB29_39:                              # %freeexp.exit27
	shll	$23, %eax
	shll	$14, %ebp
	orl	%r15d, %ebp
	orl	%eax, %ebp
	movq	24(%r14), %rax
	movl	8(%rax), %edx
	movq	%r14, %rdi
	movl	%ebp, %esi
	callq	luaK_code
	movl	%eax, 8(%rbx)
	movl	$11, (%rbx)
.LBB29_40:
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB29_6:
	xorpd	%xmm0, %xmm0
	leal	-12(%r15), %eax
	cmpl	$8, %eax
	ja	.LBB29_19
# BB#7:
	movsd	8(%rbx), %xmm2          # xmm2 = mem[0],zero
	movsd	8(%r12), %xmm1          # xmm1 = mem[0],zero
	xorl	%ebp, %ebp
	jmpq	*.LJTI29_0(,%rax,8)
.LBB29_8:
	addsd	%xmm1, %xmm2
	jmp	.LBB29_17
.LBB29_9:
	subsd	%xmm1, %xmm2
	jmp	.LBB29_17
.LBB29_10:
	mulsd	%xmm1, %xmm2
	jmp	.LBB29_17
.LBB29_11:
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm1
	jne	.LBB29_12
	jnp	.LBB29_22
.LBB29_12:
	divsd	%xmm1, %xmm2
	jmp	.LBB29_17
.LBB29_13:
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm1
	jne	.LBB29_14
	jnp	.LBB29_22
.LBB29_14:
	movapd	%xmm2, %xmm0
	divsd	%xmm1, %xmm0
	movapd	%xmm2, 16(%rsp)         # 16-byte Spill
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	callq	floor
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movapd	16(%rsp), %xmm1         # 16-byte Reload
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB29_19
	jmp	.LBB29_20
.LBB29_15:
	movapd	%xmm2, %xmm0
	callq	pow
	ucomisd	%xmm0, %xmm0
	jnp	.LBB29_19
	jmp	.LBB29_20
.LBB29_16:
	xorpd	.LCPI29_0(%rip), %xmm2
.LBB29_17:
	movapd	%xmm2, %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.LBB29_20
.LBB29_19:                              # %constfolding.exit
	movsd	%xmm0, 8(%rbx)
	jmp	.LBB29_40
.Lfunc_end29:
	.size	codearith, .Lfunc_end29-codearith
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI29_0:
	.quad	.LBB29_8
	.quad	.LBB29_9
	.quad	.LBB29_10
	.quad	.LBB29_11
	.quad	.LBB29_13
	.quad	.LBB29_15
	.quad	.LBB29_16
	.quad	.LBB29_19
	.quad	.LBB29_23

	.text
	.hidden	luaK_infix
	.globl	luaK_infix
	.p2align	4, 0x90
	.type	luaK_infix,@function
luaK_infix:                             # @luaK_infix
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi159:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi160:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi161:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi162:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi163:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi164:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi165:
	.cfi_def_cfa_offset 64
.Lcfi166:
	.cfi_offset %rbx, -56
.Lcfi167:
	.cfi_offset %r12, -48
.Lcfi168:
	.cfi_offset %r13, -40
.Lcfi169:
	.cfi_offset %r14, -32
.Lcfi170:
	.cfi_offset %r15, -24
.Lcfi171:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rdi, %r14
	cmpl	$14, %esi
	ja	.LBB30_5
# BB#1:
	movl	%esi, %eax
	jmpq	*.LJTI30_0(,%rax,8)
.LBB30_2:
	cmpl	$5, (%r12)
	jne	.LBB30_5
# BB#3:
	cmpl	$-1, 16(%r12)
	jne	.LBB30_5
# BB#4:                                 # %isnumeral.exit
	cmpl	$-1, 20(%r12)
	je	.LBB30_45
.LBB30_5:
	movq	%r14, %rdi
	movq	%r12, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	luaK_exp2RK             # TAILCALL
.LBB30_6:
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	luaK_dischargevars
	cmpl	$12, (%r12)
	jne	.LBB30_10
# BB#7:
	movl	8(%r12), %eax
	testb	$1, %ah
	jne	.LBB30_10
# BB#8:
	movzbl	74(%r14), %ecx
	cmpl	%eax, %ecx
	jg	.LBB30_10
# BB#9:
	decl	60(%r14)
.LBB30_10:                              # %freeexp.exit.i
	movl	60(%r14), %edx
	movq	(%r14), %rax
	movzbl	115(%rax), %ecx
	cmpl	%ecx, %edx
	jl	.LBB30_14
# BB#11:
	leal	1(%rdx), %ebx
	cmpl	$249, %edx
	jl	.LBB30_13
# BB#12:
	movq	24(%r14), %rdi
	movl	$.L.str, %esi
	callq	luaX_syntaxerror
	movq	(%r14), %rax
	movl	60(%r14), %edx
.LBB30_13:
	movb	%bl, 115(%rax)
.LBB30_14:                              # %luaK_exp2nextreg.exit
	leal	1(%rdx), %eax
	movl	%eax, 60(%r14)
	movq	%r14, %rdi
	movq	%r12, %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	exp2reg                 # TAILCALL
.LBB30_15:
	movq	%r14, %rdi
	movq	%r12, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	luaK_goiftrue           # TAILCALL
.LBB30_16:
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	luaK_dischargevars
	movl	(%r12), %eax
	decl	%eax
	cmpl	$9, %eax
	ja	.LBB30_25
# BB#17:
	jmpq	*.LJTI30_1(,%rax,8)
.LBB30_18:
	movl	56(%r14), %r15d
	movl	$-1, 56(%r14)
	movq	24(%r14), %rax
	movl	8(%rax), %edx
	movl	$2147450902, %esi       # imm = 0x7FFF8016
	movq	%r14, %rdi
	callq	luaK_code
	movl	%eax, %ebx
	cmpl	$-1, %r15d
	je	.LBB30_27
# BB#19:
	cmpl	$-1, %ebx
	je	.LBB30_28
# BB#20:                                # %.preheader.i.i.i
	movq	(%r14), %rax
	movq	24(%rax), %r13
	movl	%ebx, %edx
	.p2align	4, 0x90
.LBB30_21:                              # =>This Inner Loop Header: Depth=1
	movl	%edx, %ecx
	movslq	%ecx, %rbp
	movl	(%r13,%rbp,4), %eax
	movl	%eax, %edx
	shrl	$14, %edx
	leal	-131071(%rdx), %esi
	cmpl	$-1, %esi
	leal	-131070(%rcx,%rdx), %edx
	cmovel	%esi, %edx
	cmpl	$-1, %edx
	jne	.LBB30_21
# BB#22:
	notl	%ecx
	addl	%ecx, %r15d
	movl	%r15d, %ecx
	negl	%ecx
	cmovll	%r15d, %ecx
	cmpl	$131072, %ecx           # imm = 0x20000
	jl	.LBB30_24
# BB#23:
	movq	24(%r14), %rdi
	movl	$.L.str.1, %esi
	callq	luaX_syntaxerror
	movl	(%r13,%rbp,4), %eax
.LBB30_24:                              # %fixjump.exit.i.i.i
	andl	$16383, %eax            # imm = 0x3FFF
	shll	$14, %r15d
	leal	2147467264(%rax,%r15), %eax
	movl	%eax, (%r13,%rbp,4)
	jmp	.LBB30_27
.LBB30_25:
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	jumponcond
	movl	%eax, %ebx
	jmp	.LBB30_27
.LBB30_26:
	movl	8(%r12), %ebx
.LBB30_27:                              # %luaK_jump.exit.i
	cmpl	$-1, %ebx
	movl	%ebx, %r15d
	je	.LBB30_35
.LBB30_28:                              # %luaK_jump.exit.thread27.i
	movl	16(%r12), %edx
	cmpl	$-1, %edx
	je	.LBB30_34
# BB#29:                                # %.preheader.i.i
	movq	(%r14), %rax
	movq	24(%rax), %rbp
	.p2align	4, 0x90
.LBB30_30:                              # =>This Inner Loop Header: Depth=1
	movl	%edx, %ecx
	movslq	%ecx, %rbx
	movl	(%rbp,%rbx,4), %eax
	movl	%eax, %edx
	shrl	$14, %edx
	leal	-131071(%rdx), %esi
	cmpl	$-1, %esi
	leal	-131070(%rcx,%rdx), %edx
	cmovel	%esi, %edx
	cmpl	$-1, %edx
	jne	.LBB30_30
# BB#31:
	notl	%ecx
	addl	%ecx, %r15d
	movl	%r15d, %ecx
	negl	%ecx
	cmovll	%r15d, %ecx
	cmpl	$131072, %ecx           # imm = 0x20000
	jl	.LBB30_33
# BB#32:
	movq	24(%r14), %rdi
	movl	$.L.str.1, %esi
	callq	luaX_syntaxerror
	movl	(%rbp,%rbx,4), %eax
.LBB30_33:                              # %fixjump.exit.i.i
	andl	$16383, %eax            # imm = 0x3FFF
	shll	$14, %r15d
	leal	2147467264(%rax,%r15), %eax
	movl	%eax, (%rbp,%rbx,4)
	jmp	.LBB30_35
.LBB30_34:
	movl	%r15d, 16(%r12)
.LBB30_35:                              # %luaK_concat.exit.i
	movl	20(%r12), %r15d
	movl	48(%r14), %eax
	movl	%eax, 52(%r14)
	cmpl	$-1, %r15d
	je	.LBB30_44
# BB#36:
	movl	56(%r14), %edx
	cmpl	$-1, %edx
	je	.LBB30_43
# BB#37:                                # %.preheader.i.i19.i
	movq	(%r14), %rax
	movq	24(%rax), %rbp
	.p2align	4, 0x90
.LBB30_38:                              # =>This Inner Loop Header: Depth=1
	movl	%edx, %ecx
	movslq	%ecx, %rbx
	movl	(%rbp,%rbx,4), %eax
	movl	%eax, %edx
	shrl	$14, %edx
	leal	-131071(%rdx), %esi
	cmpl	$-1, %esi
	leal	-131070(%rcx,%rdx), %edx
	cmovel	%esi, %edx
	cmpl	$-1, %edx
	jne	.LBB30_38
# BB#39:
	notl	%ecx
	addl	%ecx, %r15d
	movl	%r15d, %ecx
	negl	%ecx
	cmovll	%r15d, %ecx
	cmpl	$131072, %ecx           # imm = 0x20000
	jl	.LBB30_41
# BB#40:
	movq	24(%r14), %rdi
	movl	$.L.str.1, %esi
	callq	luaX_syntaxerror
	movl	(%rbp,%rbx,4), %eax
.LBB30_41:                              # %fixjump.exit.i.i26.i
	andl	$16383, %eax            # imm = 0x3FFF
	shll	$14, %r15d
	leal	2147467264(%rax,%r15), %eax
	movl	%eax, (%rbp,%rbx,4)
	jmp	.LBB30_44
.LBB30_43:
	movl	%r15d, 56(%r14)
.LBB30_44:
	movl	$-1, 20(%r12)
.LBB30_45:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end30:
	.size	luaK_infix, .Lfunc_end30-luaK_infix
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI30_0:
	.quad	.LBB30_2
	.quad	.LBB30_2
	.quad	.LBB30_2
	.quad	.LBB30_2
	.quad	.LBB30_2
	.quad	.LBB30_2
	.quad	.LBB30_6
	.quad	.LBB30_5
	.quad	.LBB30_5
	.quad	.LBB30_5
	.quad	.LBB30_5
	.quad	.LBB30_5
	.quad	.LBB30_5
	.quad	.LBB30_15
	.quad	.LBB30_16
.LJTI30_1:
	.quad	.LBB30_35
	.quad	.LBB30_18
	.quad	.LBB30_35
	.quad	.LBB30_25
	.quad	.LBB30_25
	.quad	.LBB30_25
	.quad	.LBB30_25
	.quad	.LBB30_25
	.quad	.LBB30_25
	.quad	.LBB30_26

	.text
	.hidden	luaK_posfix
	.globl	luaK_posfix
	.p2align	4, 0x90
	.type	luaK_posfix,@function
luaK_posfix:                            # @luaK_posfix
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi172:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi173:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi174:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi175:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi176:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi177:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi178:
	.cfi_def_cfa_offset 64
.Lcfi179:
	.cfi_offset %rbx, -56
.Lcfi180:
	.cfi_offset %r12, -48
.Lcfi181:
	.cfi_offset %r13, -40
.Lcfi182:
	.cfi_offset %r14, -32
.Lcfi183:
	.cfi_offset %r15, -24
.Lcfi184:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %r14
	movq	%rdi, %r15
	cmpl	$14, %esi
	ja	.LBB31_98
# BB#1:
	movl	%esi, %eax
	jmpq	*.LJTI31_0(,%rax,8)
.LBB31_33:
	movl	$12, %esi
	jmp	.LBB31_34
.LBB31_35:
	movl	$13, %esi
	jmp	.LBB31_34
.LBB31_36:
	movl	$14, %esi
	jmp	.LBB31_34
.LBB31_37:
	movl	$15, %esi
	jmp	.LBB31_34
.LBB31_38:
	movl	$16, %esi
	jmp	.LBB31_34
.LBB31_39:
	movl	$17, %esi
	jmp	.LBB31_34
.LBB31_14:
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB31_15
# BB#16:
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	luaK_dischargevars
	cmpl	$11, (%r13)
	je	.LBB31_18
	jmp	.LBB31_24
.LBB31_51:
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	luaK_exp2RK
	movl	%eax, %ebp
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	luaK_exp2RK
	cmpl	$12, (%r13)
	jne	.LBB31_55
# BB#52:
	movl	8(%r13), %ecx
	testb	$1, %ch
	jne	.LBB31_55
# BB#53:
	movzbl	74(%r15), %edx
	cmpl	%ecx, %edx
	jg	.LBB31_55
# BB#54:
	decl	60(%r15)
.LBB31_55:                              # %freeexp.exit.i84
	cmpl	$12, (%r14)
	jne	.LBB31_59
# BB#56:
	movl	8(%r14), %ecx
	testb	$1, %ch
	jne	.LBB31_59
# BB#57:
	movzbl	74(%r15), %edx
	cmpl	%ecx, %edx
	jg	.LBB31_59
# BB#58:
	decl	60(%r15)
.LBB31_59:                              # %codecomp.exit85
	movl	$23, %esi
	xorl	%edx, %edx
	jmp	.LBB31_50
.LBB31_40:
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	luaK_exp2RK
	movl	%eax, %ebp
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	luaK_exp2RK
	cmpl	$12, (%r13)
	jne	.LBB31_44
# BB#41:
	movl	8(%r13), %ecx
	testb	$1, %ch
	jne	.LBB31_44
# BB#42:
	movzbl	74(%r15), %edx
	cmpl	%ecx, %edx
	jg	.LBB31_44
# BB#43:
	decl	60(%r15)
.LBB31_44:                              # %freeexp.exit.i83
	cmpl	$12, (%r14)
	jne	.LBB31_48
# BB#45:
	movl	8(%r14), %ecx
	testb	$1, %ch
	jne	.LBB31_48
# BB#46:
	movzbl	74(%r15), %edx
	cmpl	%ecx, %edx
	jg	.LBB31_48
# BB#47:
	decl	60(%r15)
.LBB31_48:                              # %codecomp.exit
	movl	$23, %esi
	jmp	.LBB31_49
.LBB31_60:
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	luaK_exp2RK
	movl	%eax, %ebp
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	luaK_exp2RK
	cmpl	$12, (%r13)
	jne	.LBB31_64
# BB#61:
	movl	8(%r13), %ecx
	testb	$1, %ch
	jne	.LBB31_64
# BB#62:
	movzbl	74(%r15), %edx
	cmpl	%ecx, %edx
	jg	.LBB31_64
# BB#63:
	decl	60(%r15)
.LBB31_64:                              # %freeexp.exit.i86
	cmpl	$12, (%r14)
	jne	.LBB31_68
# BB#65:
	movl	8(%r14), %ecx
	testb	$1, %ch
	jne	.LBB31_68
# BB#66:
	movzbl	74(%r15), %edx
	cmpl	%ecx, %edx
	jg	.LBB31_68
# BB#67:
	decl	60(%r15)
.LBB31_68:                              # %codecomp.exit87
	movl	$24, %esi
	jmp	.LBB31_49
.LBB31_69:
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	luaK_exp2RK
	movl	%eax, %ebp
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	luaK_exp2RK
	cmpl	$12, (%r13)
	jne	.LBB31_73
# BB#70:
	movl	8(%r13), %ecx
	testb	$1, %ch
	jne	.LBB31_73
# BB#71:
	movzbl	74(%r15), %edx
	cmpl	%ecx, %edx
	jg	.LBB31_73
# BB#72:
	decl	60(%r15)
.LBB31_73:                              # %freeexp.exit.i88
	cmpl	$12, (%r14)
	jne	.LBB31_77
# BB#74:
	movl	8(%r14), %ecx
	testb	$1, %ch
	jne	.LBB31_77
# BB#75:
	movzbl	74(%r15), %edx
	cmpl	%ecx, %edx
	jg	.LBB31_77
# BB#76:
	decl	60(%r15)
.LBB31_77:                              # %codecomp.exit89
	movl	$25, %esi
.LBB31_49:
	movl	$1, %edx
.LBB31_50:
	movq	%r15, %rdi
	movl	%ebp, %ecx
	movl	%eax, %r8d
	jmp	.LBB31_97
.LBB31_78:
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	luaK_exp2RK
	movl	%eax, %ebp
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	luaK_exp2RK
	cmpl	$12, (%r13)
	jne	.LBB31_82
# BB#79:
	movl	8(%r13), %ecx
	testb	$1, %ch
	jne	.LBB31_82
# BB#80:
	movzbl	74(%r15), %edx
	cmpl	%ecx, %edx
	jg	.LBB31_82
# BB#81:
	decl	60(%r15)
.LBB31_82:                              # %freeexp.exit.i90
	cmpl	$12, (%r14)
	jne	.LBB31_86
# BB#83:
	movl	8(%r14), %ecx
	testb	$1, %ch
	jne	.LBB31_86
# BB#84:
	movzbl	74(%r15), %edx
	cmpl	%ecx, %edx
	jg	.LBB31_86
# BB#85:
	decl	60(%r15)
.LBB31_86:                              # %codecomp.exit91
	movl	$24, %esi
	jmp	.LBB31_96
.LBB31_87:
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	luaK_exp2RK
	movl	%eax, %ebp
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	luaK_exp2RK
	cmpl	$12, (%r13)
	jne	.LBB31_91
# BB#88:
	movl	8(%r13), %ecx
	testb	$1, %ch
	jne	.LBB31_91
# BB#89:
	movzbl	74(%r15), %edx
	cmpl	%ecx, %edx
	jg	.LBB31_91
# BB#90:
	decl	60(%r15)
.LBB31_91:                              # %freeexp.exit.i92
	cmpl	$12, (%r14)
	jne	.LBB31_95
# BB#92:
	movl	8(%r14), %ecx
	testb	$1, %ch
	jne	.LBB31_95
# BB#93:
	movzbl	74(%r15), %edx
	cmpl	%ecx, %edx
	jg	.LBB31_95
# BB#94:
	decl	60(%r15)
.LBB31_95:                              # %codecomp.exit93
	movl	$25, %esi
.LBB31_96:
	movl	$1, %edx
	movq	%r15, %rdi
	movl	%eax, %ecx
	movl	%ebp, %r8d
.LBB31_97:
	callq	condjump
	movl	%eax, 8(%r14)
	movl	$10, (%r14)
	jmp	.LBB31_98
.LBB31_2:
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	luaK_dischargevars
	movl	20(%r14), %r12d
	cmpl	$-1, %r12d
	je	.LBB31_9
# BB#3:
	movl	20(%r13), %edx
	cmpl	$-1, %edx
	je	.LBB31_99
# BB#4:                                 # %.preheader.i
	movq	(%r15), %rax
	movq	24(%rax), %rbp
	.p2align	4, 0x90
.LBB31_5:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, %ecx
	movslq	%ecx, %rbx
	movl	(%rbp,%rbx,4), %eax
	movl	%eax, %edx
	shrl	$14, %edx
	leal	-131071(%rdx), %esi
	cmpl	$-1, %esi
	leal	-131070(%rcx,%rdx), %edx
	cmovel	%esi, %edx
	cmpl	$-1, %edx
	jne	.LBB31_5
	jmp	.LBB31_6
.LBB31_10:
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	luaK_dischargevars
	movl	16(%r14), %r12d
	cmpl	$-1, %r12d
	je	.LBB31_9
# BB#11:
	movl	16(%r13), %edx
	cmpl	$-1, %edx
	je	.LBB31_100
# BB#12:                                # %.preheader.i74
	movq	(%r15), %rax
	movq	24(%rax), %rbp
	.p2align	4, 0x90
.LBB31_13:                              # =>This Inner Loop Header: Depth=1
	movl	%edx, %ecx
	movslq	%ecx, %rbx
	movl	(%rbp,%rbx,4), %eax
	movl	%eax, %edx
	shrl	$14, %edx
	leal	-131071(%rdx), %esi
	cmpl	$-1, %esi
	leal	-131070(%rcx,%rdx), %edx
	cmovel	%esi, %edx
	cmpl	$-1, %edx
	jne	.LBB31_13
.LBB31_6:
	notl	%ecx
	addl	%ecx, %r12d
	movl	%r12d, %ecx
	negl	%ecx
	cmovll	%r12d, %ecx
	cmpl	$131072, %ecx           # imm = 0x20000
	jl	.LBB31_8
# BB#7:
	movq	24(%r15), %rdi
	movl	$.L.str.1, %esi
	callq	luaX_syntaxerror
	movl	(%rbp,%rbx,4), %eax
.LBB31_8:                               # %fixjump.exit.i
	andl	$16383, %eax            # imm = 0x3FFF
	shll	$14, %r12d
	leal	2147467264(%rax,%r12), %eax
	movl	%eax, (%rbp,%rbx,4)
	jmp	.LBB31_9
.LBB31_15:
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	luaK_exp2anyreg
	cmpl	$11, (%r13)
	jne	.LBB31_24
.LBB31_18:
	movq	(%r15), %rax
	movq	24(%rax), %rax
	movslq	8(%r13), %rcx
	movl	(%rax,%rcx,4), %edx
	movl	%edx, %esi
	andl	$63, %esi
	cmpl	$21, %esi
	jne	.LBB31_24
# BB#19:
	movl	8(%r14), %esi
	cmpl	$12, (%r14)
	jne	.LBB31_23
# BB#20:
	movl	%esi, %edi
	andl	$256, %edi              # imm = 0x100
	jne	.LBB31_23
# BB#21:
	movzbl	74(%r15), %edi
	cmpl	%esi, %edi
	jg	.LBB31_23
# BB#22:
	decl	60(%r15)
	movl	(%rax,%rcx,4), %edx
.LBB31_23:                              # %freeexp.exit
	andl	$8388607, %edx          # imm = 0x7FFFFF
	shll	$23, %esi
	orl	%edx, %esi
	movl	%esi, (%rax,%rcx,4)
	movl	$11, (%r14)
	movl	8(%r13), %eax
	movl	%eax, 8(%r14)
	jmp	.LBB31_98
.LBB31_24:
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	luaK_dischargevars
	cmpl	$12, (%r13)
	jne	.LBB31_28
# BB#25:
	movl	8(%r13), %eax
	testb	$1, %ah
	jne	.LBB31_28
# BB#26:
	movzbl	74(%r15), %ecx
	cmpl	%eax, %ecx
	jg	.LBB31_28
# BB#27:
	decl	60(%r15)
.LBB31_28:                              # %freeexp.exit.i
	movl	60(%r15), %edx
	movq	(%r15), %rax
	movzbl	115(%rax), %ecx
	cmpl	%ecx, %edx
	jl	.LBB31_32
# BB#29:
	leal	1(%rdx), %ebx
	cmpl	$249, %edx
	jl	.LBB31_31
# BB#30:
	movq	24(%r15), %rdi
	movl	$.L.str, %esi
	callq	luaX_syntaxerror
	movq	(%r15), %rax
	movl	60(%r15), %edx
.LBB31_31:
	movb	%bl, 115(%rax)
.LBB31_32:                              # %luaK_exp2nextreg.exit
	leal	1(%rdx), %eax
	movl	%eax, 60(%r15)
	movq	%r15, %rdi
	movq	%r13, %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	exp2reg
	movl	$21, %esi
.LBB31_34:
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r13, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	codearith               # TAILCALL
.LBB31_99:
	movl	%r12d, 20(%r13)
	jmp	.LBB31_9
.LBB31_100:
	movl	%r12d, 16(%r13)
.LBB31_9:                               # %luaK_concat.exit
	movq	16(%r13), %rax
	movq	%rax, 16(%r14)
	movups	(%r13), %xmm0
	movups	%xmm0, (%r14)
.LBB31_98:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end31:
	.size	luaK_posfix, .Lfunc_end31-luaK_posfix
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI31_0:
	.quad	.LBB31_33
	.quad	.LBB31_35
	.quad	.LBB31_36
	.quad	.LBB31_37
	.quad	.LBB31_38
	.quad	.LBB31_39
	.quad	.LBB31_14
	.quad	.LBB31_51
	.quad	.LBB31_40
	.quad	.LBB31_60
	.quad	.LBB31_69
	.quad	.LBB31_78
	.quad	.LBB31_87
	.quad	.LBB31_2
	.quad	.LBB31_10

	.text
	.hidden	luaK_fixline
	.globl	luaK_fixline
	.p2align	4, 0x90
	.type	luaK_fixline,@function
luaK_fixline:                           # @luaK_fixline
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	movslq	48(%rdi), %rcx
	movl	%esi, -4(%rax,%rcx,4)
	retq
.Lfunc_end32:
	.size	luaK_fixline, .Lfunc_end32-luaK_fixline
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaK_code,@function
luaK_code:                              # @luaK_code
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi185:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi186:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi187:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi188:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi189:
	.cfi_def_cfa_offset 48
.Lcfi190:
	.cfi_offset %rbx, -40
.Lcfi191:
	.cfi_offset %r14, -32
.Lcfi192:
	.cfi_offset %r15, -24
.Lcfi193:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movq	(%rbx), %rbp
	movl	56(%rbx), %esi
	movl	48(%rbx), %edx
	movl	$255, %ecx
	movl	%edx, %r8d
	callq	patchlistaux
	movl	$-1, 56(%rbx)
	movl	48(%rbx), %ecx
	cmpl	80(%rbp), %ecx
	jge	.LBB33_2
# BB#1:                                 # %._crit_edge
	movq	24(%rbp), %rax
	jmp	.LBB33_3
.LBB33_2:
	leaq	80(%rbp), %rdx
	movq	32(%rbx), %rdi
	movq	24(%rbp), %rsi
	movl	$4, %ecx
	movl	$2147483645, %r8d       # imm = 0x7FFFFFFD
	movl	$.L.str.3, %r9d
	callq	luaM_growaux_
	movq	%rax, 24(%rbp)
	movl	48(%rbx), %ecx
.LBB33_3:
	movslq	%ecx, %rcx
	movl	%r15d, (%rax,%rcx,4)
	movl	48(%rbx), %ecx
	cmpl	84(%rbp), %ecx
	jge	.LBB33_5
# BB#4:                                 # %._crit_edge22
	movq	40(%rbp), %rax
	jmp	.LBB33_6
.LBB33_5:
	leaq	84(%rbp), %rdx
	movq	32(%rbx), %rdi
	movq	40(%rbp), %rsi
	movl	$4, %ecx
	movl	$2147483645, %r8d       # imm = 0x7FFFFFFD
	movl	$.L.str.3, %r9d
	callq	luaM_growaux_
	movq	%rax, 40(%rbp)
	movl	48(%rbx), %ecx
.LBB33_6:
	movslq	%ecx, %rcx
	movl	%r14d, (%rax,%rcx,4)
	movl	48(%rbx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 48(%rbx)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end33:
	.size	luaK_code, .Lfunc_end33-luaK_code
	.cfi_endproc

	.hidden	luaK_setlist
	.globl	luaK_setlist
	.p2align	4, 0x90
	.type	luaK_setlist,@function
luaK_setlist:                           # @luaK_setlist
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi194:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi195:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi196:
	.cfi_def_cfa_offset 32
.Lcfi197:
	.cfi_offset %rbx, -32
.Lcfi198:
	.cfi_offset %r14, -24
.Lcfi199:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	decl	%edx
	movslq	%edx, %rax
	imulq	$1374389535, %rax, %rax # imm = 0x51EB851F
	movq	%rax, %rdx
	sarq	$36, %rdx
	shrq	$63, %rax
	leal	1(%rdx,%rax), %ebp
	movl	%r14d, %eax
	shll	$6, %eax
	movl	%ecx, %edx
	shll	$23, %edx
	xorl	%esi, %esi
	cmpl	$-1, %ecx
	cmovnel	%edx, %esi
	orl	%eax, %esi
	orl	$34, %esi
	cmpl	$511, %ebp              # imm = 0x1FF
	jg	.LBB34_2
# BB#1:
	shll	$14, %ebp
	orl	%ebp, %esi
	movq	24(%rbx), %rax
	movl	8(%rax), %edx
	movq	%rbx, %rdi
	jmp	.LBB34_3
.LBB34_2:
	movq	24(%rbx), %rax
	movl	8(%rax), %edx
	movq	%rbx, %rdi
	callq	luaK_code
	movq	24(%rbx), %rax
	movl	8(%rax), %edx
	movq	%rbx, %rdi
	movl	%ebp, %esi
.LBB34_3:
	callq	luaK_code
	incl	%r14d
	movl	%r14d, 60(%rbx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end34:
	.size	luaK_setlist, .Lfunc_end34-luaK_setlist
	.cfi_endproc

	.p2align	4, 0x90
	.type	discharge2reg,@function
discharge2reg:                          # @discharge2reg
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi200:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi201:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi202:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi203:
	.cfi_def_cfa_offset 48
.Lcfi204:
	.cfi_offset %rbx, -32
.Lcfi205:
	.cfi_offset %r14, -24
.Lcfi206:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	callq	luaK_dischargevars
	movl	(%rbx), %eax
	leal	-1(%rax), %ecx
	cmpl	$11, %ecx
	ja	.LBB35_20
# BB#1:
	jmpq	*.LJTI35_0(,%rcx,8)
.LBB35_11:
	xorl	%esi, %esi
	cmpl	$2, %eax
	sete	%sil
	movl	%r14d, %eax
	shll	$6, %eax
	shll	$23, %esi
	orl	%eax, %esi
	orl	$2, %esi
	jmp	.LBB35_18
.LBB35_2:
	movslq	48(%rbp), %rax
	cmpl	52(%rbp), %eax
	jle	.LBB35_5
# BB#3:
	testl	%eax, %eax
	je	.LBB35_4
# BB#6:
	movq	(%rbp), %rcx
	movq	24(%rcx), %rcx
	movl	-4(%rcx,%rax,4), %edx
	movl	%edx, %esi
	andl	$63, %esi
	cmpl	$3, %esi
	jne	.LBB35_5
# BB#7:
	movl	%edx, %esi
	shrl	$6, %esi
	movzbl	%sil, %esi
	cmpl	%r14d, %esi
	jg	.LBB35_5
# BB#8:
	movl	%edx, %esi
	shrl	$23, %esi
	leal	1(%rsi), %edi
	cmpl	%r14d, %edi
	jl	.LBB35_5
# BB#9:
	cmpl	%r14d, %esi
	jge	.LBB35_19
# BB#10:                                # %.critedge.i
	andl	$8388607, %edx          # imm = 0x7FFFFF
	movl	%r14d, %esi
	shll	$23, %esi
	orl	%edx, %esi
	movl	%esi, -4(%rcx,%rax,4)
	jmp	.LBB35_19
.LBB35_12:
	movl	8(%rbx), %esi
	movl	%r14d, %eax
	shll	$6, %eax
	shll	$14, %esi
	jmp	.LBB35_13
.LBB35_14:
	movq	8(%rbx), %rax
	movq	%rax, (%rsp)
	movl	$3, 8(%rsp)
	movq	%rsp, %rsi
	movq	%rbp, %rdi
	movq	%rsi, %rdx
	callq	addk
	movl	%r14d, %esi
	shll	$6, %esi
	shll	$14, %eax
.LBB35_13:                              # %luaK_nil.exit
	orl	%eax, %esi
	orl	$1, %esi
	jmp	.LBB35_18
.LBB35_15:
	movq	(%rbp), %rax
	movq	24(%rax), %rax
	movslq	8(%rbx), %rcx
	movl	$-16321, %edx           # imm = 0xC03F
	andl	(%rax,%rcx,4), %edx
	movl	%r14d, %esi
	shll	$6, %esi
	andl	$16320, %esi            # imm = 0x3FC0
	orl	%edx, %esi
	movl	%esi, (%rax,%rcx,4)
	jmp	.LBB35_19
.LBB35_16:
	movl	8(%rbx), %esi
	cmpl	%r14d, %esi
	je	.LBB35_19
# BB#17:
	movl	%r14d, %eax
	shll	$6, %eax
	shll	$23, %esi
	orl	%eax, %esi
	jmp	.LBB35_18
.LBB35_4:
	movzbl	74(%rbp), %eax
	cmpl	%r14d, %eax
	jle	.LBB35_19
.LBB35_5:
	movl	%r14d, %eax
	shll	$6, %eax
	movl	%r14d, %esi
	shll	$23, %esi
	orl	%eax, %esi
	orl	$3, %esi
.LBB35_18:                              # %luaK_nil.exit
	movq	24(%rbp), %rax
	movl	8(%rax), %edx
	movq	%rbp, %rdi
	callq	luaK_code
.LBB35_19:                              # %luaK_nil.exit
	movl	%r14d, 8(%rbx)
	movl	$12, (%rbx)
.LBB35_20:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end35:
	.size	discharge2reg, .Lfunc_end35-discharge2reg
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI35_0:
	.quad	.LBB35_2
	.quad	.LBB35_11
	.quad	.LBB35_11
	.quad	.LBB35_12
	.quad	.LBB35_14
	.quad	.LBB35_20
	.quad	.LBB35_20
	.quad	.LBB35_20
	.quad	.LBB35_20
	.quad	.LBB35_20
	.quad	.LBB35_15
	.quad	.LBB35_16

	.text
	.p2align	4, 0x90
	.type	condjump,@function
condjump:                               # @condjump
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi207:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi208:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi209:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi210:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi211:
	.cfi_def_cfa_offset 48
.Lcfi212:
	.cfi_offset %rbx, -48
.Lcfi213:
	.cfi_offset %r12, -40
.Lcfi214:
	.cfi_offset %r14, -32
.Lcfi215:
	.cfi_offset %r15, -24
.Lcfi216:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	shll	$6, %edx
	orl	%edx, %esi
	shll	$23, %ecx
	shll	$14, %r8d
	orl	%ecx, %r8d
	orl	%r8d, %esi
	movq	24(%r12), %rax
	movl	8(%rax), %edx
	callq	luaK_code
	movl	56(%r12), %r15d
	movl	$-1, 56(%r12)
	movq	24(%r12), %rax
	movl	8(%rax), %edx
	movl	$2147450902, %esi       # imm = 0x7FFF8016
	movq	%r12, %rdi
	callq	luaK_code
	movl	%eax, %r14d
	cmpl	$-1, %r15d
	je	.LBB36_8
# BB#1:
	cmpl	$-1, %r14d
	je	.LBB36_2
# BB#3:                                 # %.preheader.i.i
	movq	(%r12), %rax
	movq	24(%rax), %rbp
	movl	%r14d, %edx
	.p2align	4, 0x90
.LBB36_4:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, %ecx
	movslq	%ecx, %rbx
	movl	(%rbp,%rbx,4), %eax
	movl	%eax, %edx
	shrl	$14, %edx
	leal	-131071(%rdx), %esi
	cmpl	$-1, %esi
	leal	-131070(%rcx,%rdx), %edx
	cmovel	%esi, %edx
	cmpl	$-1, %edx
	jne	.LBB36_4
# BB#5:
	notl	%ecx
	addl	%ecx, %r15d
	movl	%r15d, %ecx
	negl	%ecx
	cmovll	%r15d, %ecx
	cmpl	$131072, %ecx           # imm = 0x20000
	jl	.LBB36_7
# BB#6:
	movq	24(%r12), %rdi
	movl	$.L.str.1, %esi
	callq	luaX_syntaxerror
	movl	(%rbp,%rbx,4), %eax
.LBB36_7:                               # %fixjump.exit.i.i
	andl	$16383, %eax            # imm = 0x3FFF
	shll	$14, %r15d
	leal	2147467264(%rax,%r15), %eax
	movl	%eax, (%rbp,%rbx,4)
	jmp	.LBB36_8
.LBB36_2:
	movl	%r15d, %r14d
.LBB36_8:                               # %luaK_jump.exit
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end36:
	.size	condjump, .Lfunc_end36-condjump
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"function or expression too complex"
	.size	.L.str, 35

	.hidden	luaP_opmodes
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"control structure too long"
	.size	.L.str.1, 27

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"constant table overflow"
	.size	.L.str.2, 24

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"code size overflow"
	.size	.L.str.3, 19

	.hidden	luaX_syntaxerror
	.hidden	luaH_set
	.hidden	luaM_growaux_
	.hidden	luaC_barrierf

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
