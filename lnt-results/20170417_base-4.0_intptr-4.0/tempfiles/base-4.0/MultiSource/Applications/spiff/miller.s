	.text
	.file	"miller.bc"
	.globl	G_do_miller
	.p2align	4, 0x90
	.type	G_do_miller,@function
G_do_miller:                            # @G_do_miller
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movl	%esi, %r13d
	movl	%edi, %r15d
	leal	(%r13,%r15), %r14d
	movslq	%r14d, %rbp
	leaq	8(,%rbp,8), %rdi
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	callq	_Z_myalloc
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	4(,%rbp,4), %rdi
	xorl	%eax, %eax
	callq	_Z_myalloc
	movq	%rax, %r12
	testl	%r15d, %r15d
	jle	.LBB0_6
# BB#1:
	testl	%r13d, %r13d
	jle	.LBB0_6
# BB#2:                                 # %.lr.ph158.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph158
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movl	%ebx, %edi
	movl	%ebx, %esi
	movl	36(%rsp), %edx          # 4-byte Reload
	callq	X_com
	testl	%eax, %eax
	jne	.LBB0_6
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	incl	%ebx
	cmpl	%r15d, %ebx
	jge	.LBB0_6
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=1
	cmpl	%r13d, %ebx
	jl	.LBB0_3
.LBB0_6:                                # %.critedge
	movl	%r14d, %ecx
	shrl	$31, %ecx
	addl	%r14d, %ecx
	sarl	%ecx
	movslq	%ecx, %r14
	movl	%ebx, (%r12,%r14,4)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	$0, (%rax,%r14,8)
	leal	1(%r14), %eax
	decl	%r14d
	cmpl	%r15d, %ebx
	movl	%r14d, %ebp
	cmovel	%eax, %ebp
	cmpl	%r13d, %ebx
	cmovnel	%eax, %r14d
	cmpl	%r14d, %ebp
	jle	.LBB0_8
# BB#7:
	xorl	%ebx, %ebx
	jmp	.LBB0_34
.LBB0_8:                                # %.preheader135
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_33
# BB#9:                                 # %.preheader.preheader
	movl	$1, %eax
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
.LBB0_10:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_12 Depth 2
                                        #       Child Loop BB0_23 Depth 3
	cmpl	%r14d, %ebp
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jg	.LBB0_30
# BB#11:                                # %.lr.ph148
                                        #   in Loop: Header=BB0_10 Depth=1
	movl	%ecx, %edx
	subl	%eax, %edx
	movl	%edx, 44(%rsp)          # 4-byte Spill
	leal	(%rax,%rcx), %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movslq	%ebp, %rbx
	jmp	.LBB0_12
.LBB0_18:                               #   in Loop: Header=BB0_12 Depth=2
	leaq	1(%rbx), %rcx
	jmp	.LBB0_14
	.p2align	4, 0x90
.LBB0_12:                               #   Parent Loop BB0_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_23 Depth 3
	movl	$24, %edi
	xorl	%eax, %eax
	callq	_Z_myalloc
	cmpl	44(%rsp), %ebx          # 4-byte Folded Reload
	movq	%r14, 64(%rsp)          # 8-byte Spill
	jne	.LBB0_15
# BB#13:                                # %._crit_edge164
                                        #   in Loop: Header=BB0_12 Depth=2
	leaq	1(%rbx), %rcx
	movl	4(%r12,%rbx,4), %edx
.LBB0_14:                               #   in Loop: Header=BB0_12 Depth=2
	movq	16(%rsp), %rdi          # 8-byte Reload
	incl	%edx
	movl	$2, %esi
	movl	%edx, %r14d
	movq	24(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB0_21
	.p2align	4, 0x90
.LBB0_15:                               #   in Loop: Header=BB0_12 Depth=2
	cmpl	40(%rsp), %ebx          # 4-byte Folded Reload
	jne	.LBB0_17
# BB#16:                                # %._crit_edge163
                                        #   in Loop: Header=BB0_12 Depth=2
	leaq	-1(%rbx), %rcx
	movl	-4(%r12,%rbx,4), %r14d
	jmp	.LBB0_20
	.p2align	4, 0x90
.LBB0_17:                               #   in Loop: Header=BB0_12 Depth=2
	movl	4(%r12,%rbx,4), %edx
	movl	-4(%r12,%rbx,4), %r14d
	cmpl	%r14d, %edx
	jge	.LBB0_18
# BB#19:                                #   in Loop: Header=BB0_12 Depth=2
	leaq	-1(%rbx), %rcx
.LBB0_20:                               #   in Loop: Header=BB0_12 Depth=2
	movl	$1, %esi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
.LBB0_21:                               #   in Loop: Header=BB0_12 Depth=2
	movq	(%rdx,%rcx,8), %rcx
	movq	%rcx, (%rax)
	movl	%esi, 8(%rax)
	movl	%r14d, 12(%rax)
	movl	%ebx, %r12d
	subl	%edi, %r12d
	addl	%r14d, %r12d
	movl	%r12d, 16(%rax)
	movq	%rax, (%rdx,%rbx,8)
	cmpl	%r15d, %r14d
	jl	.LBB0_23
	jmp	.LBB0_26
	.p2align	4, 0x90
.LBB0_25:                               #   in Loop: Header=BB0_23 Depth=3
	incl	%r14d
	incl	%r12d
	cmpl	%r15d, %r14d
	jge	.LBB0_26
.LBB0_23:                               #   Parent Loop BB0_10 Depth=1
                                        #     Parent Loop BB0_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%r13d, %r12d
	jge	.LBB0_26
# BB#24:                                # %.lr.ph
                                        #   in Loop: Header=BB0_23 Depth=3
	xorl	%eax, %eax
	movl	%r14d, %edi
	movl	%r12d, %esi
	movl	36(%rsp), %edx          # 4-byte Reload
	callq	X_com
	testl	%eax, %eax
	je	.LBB0_25
.LBB0_26:                               # %.critedge131
                                        #   in Loop: Header=BB0_12 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	%r14d, (%rax,%rbx,4)
	cmpl	%r15d, %r14d
	jne	.LBB0_29
# BB#27:                                # %.critedge131
                                        #   in Loop: Header=BB0_12 Depth=2
	cmpl	%r13d, %r12d
	je	.LBB0_28
.LBB0_29:                               #   in Loop: Header=BB0_12 Depth=2
	leaq	2(%rbx), %rax
	cmpl	%r15d, %r14d
	cmovel	%eax, %ebp
	addl	$-2, %ebx
	cmpl	%r13d, %r12d
	movq	64(%rsp), %r14          # 8-byte Reload
	cmovel	%ebx, %r14d
	movslq	%r14d, %rcx
	cmpq	%rcx, %rax
	movq	%rax, %rbx
	movq	48(%rsp), %r12          # 8-byte Reload
	jle	.LBB0_12
.LBB0_30:                               # %._crit_edge
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	56(%rsp), %rbx          # 8-byte Reload
	movslq	%ebx, %rax
	imulq	$1374389535, %rax, %rax # imm = 0x51EB851F
	movq	%rax, %rcx
	shrq	$63, %rcx
	sarq	$37, %rax
	addl	%ecx, %eax
	imull	$100, %eax, %eax
	cmpl	%eax, %ebx
	jne	.LBB0_32
# BB#31:                                #   in Loop: Header=BB0_10 Depth=1
	movl	$Z_err_buf, %edi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	sprintf
	movl	$Z_err_buf, %edi
	xorl	%eax, %eax
	callq	Z_chatter
.LBB0_32:                               #   in Loop: Header=BB0_10 Depth=1
	decl	%ebp
	incl	%r14d
	cmpl	12(%rsp), %ebx          # 4-byte Folded Reload
	leal	1(%rbx), %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	16(%rsp), %rcx          # 8-byte Reload
	jl	.LBB0_10
.LBB0_33:                               # %._crit_edge154
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	movl	12(%rsp), %edi          # 4-byte Reload
	callq	Z_exceed
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	Z_fatal
	jmp	.LBB0_34
.LBB0_28:
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rbx
.LBB0_34:
	movq	%rbx, %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	G_do_miller, .Lfunc_end0-G_do_miller
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"found %d differences\n"
	.size	.L.str, 22

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"fell off end of do_miller\n"
	.size	.L.str.1, 27


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
