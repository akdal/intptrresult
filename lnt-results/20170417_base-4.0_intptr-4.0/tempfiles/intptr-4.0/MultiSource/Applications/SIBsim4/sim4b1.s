	.text
	.file	"sim4b1.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4594572339843380020     # double 0.15000000000000002
.LCPI0_1:
	.quad	4607182418800017408     # double 1
.LCPI0_2:
	.quad	4596373779694328218     # double 0.20000000000000001
.LCPI0_3:
	.quad	4608083138725491507     # double 1.2
.LCPI0_4:
	.quad	4652007308841189376     # double 1000
.LCPI0_6:
	.quad	4602678819172646912     # double 0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_5:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	SIM4
	.p2align	4, 0x90
	.type	SIM4,@function
SIM4:                                   # @SIM4
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$536, %rsp              # imm = 0x218
.Lcfi6:
	.cfi_def_cfa_offset 592
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r15
	cmpl	$0, 16(%r15)
	je	.LBB0_621
# BB#1:
	cmpl	$0, 4148(%r12)
	je	.LBB0_621
# BB#2:
	movabsq	$21474836480, %rax      # imm = 0x500000000
	movq	%rax, 464(%rsp)
	movl	$40, %edi
	movq	%rdx, 440(%rsp)         # 8-byte Spill
	callq	xmalloc
	movq	%rax, 456(%rsp)
	movq	16(%r12), %rsi
	movl	4148(%r12), %edx
	movl	options+40(%rip), %r9d
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	leaq	464(%rsp), %rax
	movl	$1, %ecx
	movl	$1, %r8d
	movq	%r15, %rdi
	pushq	$0
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	456(%rsp)               # 8-byte Folded Reload
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	exon_cores
	movq	472(%rsp), %rax         # 8-byte Reload
	addq	$32, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -32
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 64(%rsp)
	cmpl	$0, 8(%rax)
	je	.LBB0_620
# BB#3:                                 # %.lr.ph663
	xorl	%ecx, %ecx
	movq	%r12, 128(%rsp)         # 8-byte Spill
	movq	%r15, 144(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_10 Depth 2
                                        #     Child Loop BB0_16 Depth 2
                                        #     Child Loop BB0_19 Depth 2
                                        #     Child Loop BB0_25 Depth 2
                                        #       Child Loop BB0_27 Depth 3
                                        #     Child Loop BB0_47 Depth 2
                                        #     Child Loop BB0_52 Depth 2
                                        #     Child Loop BB0_63 Depth 2
                                        #     Child Loop BB0_67 Depth 2
                                        #     Child Loop BB0_73 Depth 2
                                        #     Child Loop BB0_77 Depth 2
                                        #     Child Loop BB0_117 Depth 2
                                        #       Child Loop BB0_80 Depth 3
                                        #         Child Loop BB0_88 Depth 4
                                        #       Child Loop BB0_96 Depth 3
                                        #       Child Loop BB0_108 Depth 3
                                        #       Child Loop BB0_112 Depth 3
                                        #       Child Loop BB0_116 Depth 3
                                        #     Child Loop BB0_132 Depth 2
                                        #     Child Loop BB0_145 Depth 2
                                        #     Child Loop BB0_153 Depth 2
                                        #     Child Loop BB0_157 Depth 2
                                        #     Child Loop BB0_163 Depth 2
                                        #       Child Loop BB0_165 Depth 3
                                        #     Child Loop BB0_185 Depth 2
                                        #     Child Loop BB0_192 Depth 2
                                        #     Child Loop BB0_250 Depth 2
                                        #       Child Loop BB0_205 Depth 3
                                        #         Child Loop BB0_217 Depth 4
                                        #       Child Loop BB0_229 Depth 3
                                        #       Child Loop BB0_241 Depth 3
                                        #       Child Loop BB0_245 Depth 3
                                        #       Child Loop BB0_249 Depth 3
                                        #     Child Loop BB0_266 Depth 2
                                        #     Child Loop BB0_278 Depth 2
                                        #       Child Loop BB0_284 Depth 3
                                        #       Child Loop BB0_295 Depth 3
                                        #       Child Loop BB0_299 Depth 3
                                        #       Child Loop BB0_309 Depth 3
                                        #       Child Loop BB0_312 Depth 3
                                        #       Child Loop BB0_314 Depth 3
                                        #         Child Loop BB0_317 Depth 4
                                        #           Child Loop BB0_326 Depth 5
                                        #         Child Loop BB0_331 Depth 4
                                        #         Child Loop BB0_338 Depth 4
                                        #         Child Loop BB0_342 Depth 4
                                        #         Child Loop BB0_350 Depth 4
                                        #           Child Loop BB0_362 Depth 5
                                        #         Child Loop BB0_367 Depth 4
                                        #         Child Loop BB0_373 Depth 4
                                        #         Child Loop BB0_377 Depth 4
                                        #       Child Loop BB0_428 Depth 3
                                        #       Child Loop BB0_433 Depth 3
                                        #       Child Loop BB0_410 Depth 3
                                        #       Child Loop BB0_415 Depth 3
                                        #       Child Loop BB0_420 Depth 3
                                        #     Child Loop BB0_442 Depth 2
                                        #       Child Loop BB0_452 Depth 3
                                        #       Child Loop BB0_444 Depth 3
                                        #     Child Loop BB0_462 Depth 2
                                        #     Child Loop BB0_474 Depth 2
                                        #     Child Loop BB0_480 Depth 2
                                        #     Child Loop BB0_485 Depth 2
                                        #     Child Loop BB0_491 Depth 2
                                        #       Child Loop BB0_494 Depth 3
                                        #       Child Loop BB0_500 Depth 3
                                        #     Child Loop BB0_525 Depth 2
                                        #       Child Loop BB0_513 Depth 3
                                        #     Child Loop BB0_534 Depth 2
                                        #       Child Loop BB0_542 Depth 3
                                        #     Child Loop BB0_560 Depth 2
                                        #       Child Loop BB0_591 Depth 3
                                        #         Child Loop BB0_598 Depth 4
                                        #         Child Loop BB0_604 Depth 4
                                        #     Child Loop BB0_617 Depth 2
	movq	(%rax), %rax
	movq	%rcx, 528(%rsp)         # 8-byte Spill
	movl	%ecx, %ecx
	movq	(%rax,%rcx,8), %r14
	movq	$0, 40(%r14)
	movq	8(%r15), %rsi
	movq	16(%r12), %rdx
	movq	%r14, %rdi
	callq	kill_polyA
	movl	16(%r14), %r10d
	testl	%r10d, %r10d
	je	.LBB0_619
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=1
	leaq	8(%r14), %rbp
	cmpl	$0, 44(%r14)
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	jne	.LBB0_140
# BB#6:                                 #   in Loop: Header=BB0_4 Depth=1
	movq	(%rbp), %rax
	movq	(%rax), %r13
	movl	4(%r13), %edi
	cmpq	$2, %rdi
	jb	.LBB0_140
# BB#7:                                 #   in Loop: Header=BB0_4 Depth=1
	leal	-1(%rdi), %eax
	cmpl	$61, %eax
	jb	.LBB0_24
# BB#8:                                 #   in Loop: Header=BB0_4 Depth=1
	movl	(%r13), %ecx
	decl	%ecx
	cmpl	24(%r14), %ecx
	jbe	.LBB0_24
# BB#9:                                 #   in Loop: Header=BB0_4 Depth=1
	movl	36(%r15), %ecx
	cmpl	$10, %ecx
	movl	$10, %edx
	cmovael	%edx, %ecx
	movq	16(%r12), %rdx
	movl	%ecx, 324(%rsp)
	movq	%rdx, 296(%rsp)
	movl	%eax, 304(%rsp)
	leal	-2(%rcx,%rcx), %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	decl	%eax
	movl	%eax, 320(%rsp)
	shlq	$2, %rdi
	callq	xmalloc
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rax, 312(%rsp)
	movl	$524288, %edi           # imm = 0x80000
	movl	$8, %esi
	callq	xcalloc
	movq	%rax, %rbx
	movq	%rbx, 288(%rsp)
	movq	%r14, %rbp
	leaq	288(%rsp), %r14
	movq	%r14, %rdi
	callq	bld_table
	movl	24(%rbp), %esi
	movl	%esi, %edx
	leal	1(%rsi), %r8d
	addq	8(%r15), %rsi
	notl	%edx
	movq	%r13, 160(%rsp)         # 8-byte Spill
	addl	(%r13), %edx
	movl	options+24(%rip), %r9d
	subq	$8, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	movl	$1, %ecx
	movq	%r14, %rdi
	leaq	72(%rsp), %rax
	pushq	%rax
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	leaq	480(%rsp), %rax
	pushq	%rax
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	callq	exon_cores
	addq	$32, %rsp
.Lcfi22:
	.cfi_adjust_cfa_offset -32
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_10:                               #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rbp,8), %rdi
	movl	$free, %esi
	callq	tdestroy
	incq	%rbp
	cmpq	$524288, %rbp           # imm = 0x80000
	jne	.LBB0_10
# BB#11:                                # %free_hash_env.exit
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	%rbx, %rdi
	callq	free
	movl	72(%rsp), %eax
	testq	%rax, %rax
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	160(%rsp), %r13         # 8-byte Reload
	je	.LBB0_23
# BB#12:                                # %.lr.ph.i369
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	64(%rsp), %rcx
	testb	$1, %al
	jne	.LBB0_14
# BB#13:                                #   in Loop: Header=BB0_4 Depth=1
	xorl	%esi, %esi
	cmpl	$1, %eax
	jne	.LBB0_15
	jmp	.LBB0_17
.LBB0_14:                               #   in Loop: Header=BB0_4 Depth=1
	movq	(%rcx), %rdx
	movl	(%rdx), %esi
	movl	4(%rdx), %edi
	movl	%edi, (%rdx)
	movl	%esi, 4(%rdx)
	movl	8(%rdx), %esi
	movl	12(%rdx), %edi
	movl	%edi, 8(%rdx)
	movl	%esi, 12(%rdx)
	movl	$1, %esi
	cmpl	$1, %eax
	je	.LBB0_17
.LBB0_15:                               # %.lr.ph.i369.new
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, %rdx
	subq	%rsi, %rdx
	leaq	8(%rcx,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB0_16:                               #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rsi), %rdi
	movl	(%rdi), %ebp
	movl	4(%rdi), %ebx
	movl	%ebx, (%rdi)
	movl	%ebp, 4(%rdi)
	movl	8(%rdi), %ebp
	movl	12(%rdi), %ebx
	movl	%ebx, 8(%rdi)
	movl	%ebp, 12(%rdi)
	movq	(%rsi), %rdi
	movl	(%rdi), %ebp
	movl	4(%rdi), %ebx
	movl	%ebx, (%rdi)
	movl	%ebp, 4(%rdi)
	movl	8(%rdi), %ebp
	movl	12(%rdi), %ebx
	movl	%ebx, 8(%rdi)
	movl	%ebp, 12(%rdi)
	addq	$16, %rsi
	addq	$-2, %rdx
	jne	.LBB0_16
.LBB0_17:                               # %swap_seqs.exit
                                        #   in Loop: Header=BB0_4 Depth=1
	decl	%eax
	movq	(%rcx,%rax,8), %rax
	movl	16(%r15), %edi
	movl	8(%rax), %edx
	cmpl	%edi, %edx
	jae	.LBB0_22
# BB#18:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	8(%r15), %r8
	movq	16(%r12), %r9
	movl	4148(%r12), %ebp
	movl	12(%rax), %ebx
	.p2align	4, 0x90
.LBB0_19:                               # %.lr.ph.i372
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ebp, %ebx
	jae	.LBB0_22
# BB#20:                                #   in Loop: Header=BB0_19 Depth=2
	movl	%edx, %esi
	movzbl	(%r8,%rsi), %ecx
	movl	%ebx, %esi
	cmpb	(%r9,%rsi), %cl
	jne	.LBB0_22
# BB#21:                                #   in Loop: Header=BB0_19 Depth=2
	incl	%edx
	movl	%edx, 8(%rax)
	incl	%ebx
	movl	%ebx, 12(%rax)
	cmpl	%edi, %edx
	jb	.LBB0_19
.LBB0_22:                               # %grow_exon_right.exit
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	36(%r15), %ecx
	xorl	%edx, %edx
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rdi
	leaq	64(%rsp), %rsi
	callq	merge
	movl	$0, 72(%rsp)
	movq	(%rbp), %rax
	movq	(%rax), %r13
.LBB0_23:                               # %.preheader588
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	16(%r14), %r10d
	testl	%r10d, %r10d
	je	.LBB0_50
.LBB0_24:                               # %.lr.ph
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	(%rbp), %r9
	movq	16(%r12), %r8
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_25:                               #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_27 Depth 3
	movl	%r15d, %eax
	movq	(%r9,%rax,8), %rax
	movl	4(%rax), %r14d
	movl	12(%rax), %r11d
	leal	-1(%r14), %edx
	cmpl	%r11d, %edx
	movl	$0, %edi
	movl	$0, %r12d
	movl	$0, %esi
	movl	$0, %ebp
	movl	$0, %eax
	jae	.LBB0_35
# BB#26:                                # %.lr.ph.preheader.i373
                                        #   in Loop: Header=BB0_25 Depth=2
	movl	%edx, %edx
	addq	%r8, %rdx
	leal	1(%r11), %ebx
	subl	%r14d, %ebx
	xorl	%eax, %eax
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	xorl	%r12d, %r12d
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_27:                               # %.lr.ph.i376
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_25 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rdx), %ecx
	addb	$-65, %cl
	cmpb	$19, %cl
	ja	.LBB0_30
# BB#28:                                # %.lr.ph.i376
                                        #   in Loop: Header=BB0_27 Depth=3
	movzbl	%cl, %ecx
	jmpq	*.LJTI0_0(,%rcx,8)
.LBB0_29:                               #   in Loop: Header=BB0_27 Depth=3
	incl	%edi
	jmp	.LBB0_34
	.p2align	4, 0x90
.LBB0_30:                               #   in Loop: Header=BB0_27 Depth=3
	incl	%eax
	jmp	.LBB0_34
	.p2align	4, 0x90
.LBB0_31:                               #   in Loop: Header=BB0_27 Depth=3
	incl	%r12d
	jmp	.LBB0_34
	.p2align	4, 0x90
.LBB0_32:                               #   in Loop: Header=BB0_27 Depth=3
	incl	%esi
	jmp	.LBB0_34
	.p2align	4, 0x90
.LBB0_33:                               #   in Loop: Header=BB0_27 Depth=3
	incl	%ebp
	.p2align	4, 0x90
.LBB0_34:                               #   in Loop: Header=BB0_27 Depth=3
	incq	%rdx
	decl	%ebx
	jne	.LBB0_27
.LBB0_35:                               # %._crit_edge.i
                                        #   in Loop: Header=BB0_25 Depth=2
	movl	$1, %ebx
	subl	%r14d, %ebx
	addl	%r11d, %ebx
	subl	%eax, %ebx
	leal	(%rdi,%rdi), %eax
	leal	(%rax,%rax,4), %eax
	xorl	%edx, %edx
	divl	%ebx
	cmpl	$29, %ebx
	ja	.LBB0_40
# BB#36:                                #   in Loop: Header=BB0_25 Depth=2
	cmpl	$6, %eax
	ja	.LBB0_44
# BB#37:                                #   in Loop: Header=BB0_25 Depth=2
	addl	%edi, %esi
	addl	%esi, %esi
	leal	(%rsi,%rsi,4), %eax
	xorl	%edx, %edx
	divl	%ebx
	cmpl	$7, %eax
	ja	.LBB0_44
# BB#38:                                #   in Loop: Header=BB0_25 Depth=2
	leal	(%rbp,%rbp), %eax
	leal	(%rax,%rax,4), %eax
	xorl	%edx, %edx
	divl	%ebx
	cmpl	$6, %eax
	ja	.LBB0_44
# BB#39:                                #   in Loop: Header=BB0_25 Depth=2
	addl	%r12d, %ebp
	addl	%ebp, %ebp
	leal	(%rbp,%rbp,4), %eax
	xorl	%edx, %edx
	divl	%ebx
	cmpl	$7, %eax
	ja	.LBB0_44
	jmp	.LBB0_45
	.p2align	4, 0x90
.LBB0_40:                               #   in Loop: Header=BB0_25 Depth=2
	cmpl	$7, %eax
	ja	.LBB0_44
# BB#41:                                #   in Loop: Header=BB0_25 Depth=2
	addl	%edi, %esi
	imull	$100, %esi, %eax
	xorl	%edx, %edx
	divl	%ebx
	cmpl	$94, %eax
	ja	.LBB0_44
# BB#42:                                #   in Loop: Header=BB0_25 Depth=2
	leal	(%rbp,%rbp), %eax
	leal	(%rax,%rax,4), %eax
	xorl	%edx, %edx
	divl	%ebx
	cmpl	$7, %eax
	ja	.LBB0_44
# BB#43:                                #   in Loop: Header=BB0_25 Depth=2
	addl	%r12d, %ebp
	imull	$100, %ebp, %eax
	xorl	%edx, %edx
	divl	%ebx
	cmpl	$95, %eax
	jb	.LBB0_45
	.p2align	4, 0x90
.LBB0_44:                               #   in Loop: Header=BB0_25 Depth=2
	incl	%r15d
	cmpl	%r10d, %r15d
	jb	.LBB0_25
.LBB0_45:                               # %.critedge
                                        #   in Loop: Header=BB0_4 Depth=1
	testl	%r15d, %r15d
	movq	128(%rsp), %r12         # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB0_50
# BB#46:                                # %.lr.ph640.preheader
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	%r15d, %ebx
	movq	%rbp, %r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_47:                               # %.lr.ph640
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rax
	movq	(%rax,%rbp,8), %rdi
	callq	free
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB0_47
# BB#48:                                # %._crit_edge
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	16(%r14), %edx
	movq	8(%r14), %rdi
	leaq	(%rdi,%rbx,8), %rsi
	subl	%r15d, %edx
	shlq	$3, %rdx
	callq	memmove
	movl	16(%r14), %eax
	subl	%r15d, %eax
	movl	%eax, 16(%r14)
	movq	144(%rsp), %r15         # 8-byte Reload
	movq	%r13, %rbp
	je	.LBB0_619
# BB#49:                                # %.thread
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	(%rbp), %rax
	movq	(%rax), %r13
.LBB0_50:                               # %.critedge.thread
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	4(%r13), %r10d
	movl	%r10d, %r15d
	decl	%r15d
	movq	144(%rsp), %rdx         # 8-byte Reload
	je	.LBB0_140
# BB#51:                                #   in Loop: Header=BB0_4 Depth=1
	cmpl	$250, %r15d
	movl	$250, %ecx
	cmovbl	%r15d, %ecx
	leal	(,%rcx,4), %esi
	movl	(%r13), %edi
	leal	-1(%rdi), %r9d
	cmpl	%edi, %esi
	cmovgel	%r9d, %esi
	movq	16(%r12), %r12
	movq	8(%rdx), %rbx
	movq	%rsi, 176(%rsp)         # 8-byte Spill
	movslq	%esi, %rax
	movl	%r15d, %r8d
	subl	%ecx, %r8d
	movl	%r9d, %r11d
	subl	%eax, %r11d
	movl	36(%rdx), %edx
	movl	%edx, 104(%rsp)         # 4-byte Spill
	movl	%ecx, %r14d
	incl	%r14d
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	leaq	-2(%rbx,%rdi), %rsi
	leaq	-2(%r12,%r10), %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_52:                               #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, %rdx
	leaq	(%rax,%rdx), %rbp
	testq	%rbp, %rbp
	jle	.LBB0_55
# BB#53:                                #   in Loop: Header=BB0_52 Depth=2
	leaq	(%rcx,%rdx), %rbp
	testq	%rbp, %rbp
	jle	.LBB0_55
# BB#54:                                #   in Loop: Header=BB0_52 Depth=2
	movzbl	(%rdi,%rdx), %ebx
	leaq	-1(%rdx), %rbp
	cmpb	(%rsi,%rdx), %bl
	je	.LBB0_52
.LBB0_55:                               # %.critedge.i
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movq	176(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	addl	%edx, %esi
	je	.LBB0_60
# BB#56:                                # %.critedge.i
                                        #   in Loop: Header=BB0_4 Depth=1
	leal	1(%rcx), %esi
	leaq	-1(%rsi,%rdx), %rax
	testl	%eax, %eax
	je	.LBB0_60
# BB#57:                                #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, %r15
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movq	%r10, 248(%rsp)         # 8-byte Spill
	movl	%r11d, 384(%rsp)        # 4-byte Spill
	movl	%r8d, 376(%rsp)         # 4-byte Spill
	movq	%r13, 160(%rsp)         # 8-byte Spill
	movq	176(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rcx), %r12d
	movq	%rcx, 224(%rsp)         # 8-byte Spill
	leal	1(%rax,%rcx), %ebp
	movslq	%ebp, %rbx
	shlq	$2, %rbx
	movq	%rbx, %rdi
	callq	xmalloc
	movq	%rax, %r13
	movq	%rbx, %rdi
	callq	xmalloc
	movq	%rax, 48(%rsp)          # 8-byte Spill
	testl	%r12d, %r12d
	js	.LBB0_68
# BB#58:                                # %.lr.ph286.preheader.i
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	%ebp, %eax
	cmpl	$8, %ebp
	jb	.LBB0_65
# BB#61:                                # %min.iters.checked974
                                        #   in Loop: Header=BB0_4 Depth=1
	andl	$7, %ebp
	movq	%rax, %rcx
	subq	%rbp, %rcx
	je	.LBB0_65
# BB#62:                                # %vector.ph978
                                        #   in Loop: Header=BB0_4 Depth=1
	movd	%r14d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%r13), %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB0_63:                               # %vector.body970
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm0, (%rdx)
	addq	$32, %rdx
	addq	$-8, %rsi
	jne	.LBB0_63
# BB#64:                                # %middle.block971
                                        #   in Loop: Header=BB0_4 Depth=1
	testl	%ebp, %ebp
	jne	.LBB0_66
	jmp	.LBB0_68
.LBB0_60:                               #   in Loop: Header=BB0_4 Depth=1
	addl	%edx, %r15d
	addl	%edx, %r9d
	movl	%r15d, %edx
	xorl	%r15d, %r15d
	movl	%r9d, %ecx
	movq	128(%rsp), %r12         # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB0_138
.LBB0_65:                               #   in Loop: Header=BB0_4 Depth=1
	xorl	%ecx, %ecx
.LBB0_66:                               # %.lr.ph286.i.preheader
                                        #   in Loop: Header=BB0_4 Depth=1
	subq	%rcx, %rax
	leaq	(%r13,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB0_67:                               # %.lr.ph286.i
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r14d, (%rcx)
	addq	$4, %rcx
	decq	%rax
	jne	.LBB0_67
.LBB0_68:                               # %._crit_edge287.i
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	200(%rsp), %rax         # 8-byte Reload
	movl	%r15d, (%r13,%rax,4)
	leal	(,%r14,4), %ebp
	movq	%rbp, %rdi
	callq	xmalloc
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	callq	xmalloc
	movq	224(%rsp), %r11         # 8-byte Reload
	testl	%r11d, %r11d
	movq	128(%rsp), %r12         # 8-byte Reload
	movq	248(%rsp), %rdi         # 8-byte Reload
	jle	.LBB0_78
# BB#69:                                # %.lr.ph283.preheader.i
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpl	$8, %r11d
	jb	.LBB0_75
# BB#71:                                # %min.iters.checked953
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	%r11d, %ecx
	andl	$7, %ecx
	movq	%r11, %rbp
	subq	%rcx, %rbp
	movq	%r11, %rdx
	subq	%rcx, %rdx
	je	.LBB0_75
# BB#72:                                # %vector.ph957
                                        #   in Loop: Header=BB0_4 Depth=1
	incq	%rbp
	movd	%r14d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movq	88(%rsp), %rsi          # 8-byte Reload
	leaq	20(%rsi), %rsi
	.p2align	4, 0x90
.LBB0_73:                               # %vector.body949
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm0, (%rsi)
	addq	$32, %rsi
	addq	$-8, %rdx
	jne	.LBB0_73
# BB#74:                                # %middle.block950
                                        #   in Loop: Header=BB0_4 Depth=1
	testl	%ecx, %ecx
	jne	.LBB0_76
	jmp	.LBB0_78
.LBB0_75:                               #   in Loop: Header=BB0_4 Depth=1
	movl	$1, %ebp
.LBB0_76:                               # %.lr.ph283.i.preheader
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	%r14d, %ecx
	subq	%rbp, %rcx
	movq	88(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rbp,4), %rdx
	.p2align	4, 0x90
.LBB0_77:                               # %.lr.ph283.i
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r14d, (%rdx)
	addq	$4, %rdx
	decq	%rcx
	jne	.LBB0_77
.LBB0_78:                               # %._crit_edge.i380
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	200(%rsp), %rdx         # 8-byte Reload
	movl	(%r13,%rdx,4), %ecx
	movq	88(%rsp), %rsi          # 8-byte Reload
	movl	%ecx, (%rsi)
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movl	%edx, (%rax)
	movl	104(%rsp), %eax         # 4-byte Reload
	movq	%r13, %rsi
	movl	%eax, %r13d
	shrl	$31, %r13d
	addl	%eax, %r13d
	sarl	%r13d
	addl	%eax, %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	leal	-1(%rdx), %r10d
	movl	%r10d, %eax
	subl	%r11d, %eax
	movl	%eax, 192(%rsp)         # 4-byte Spill
	leaq	1(%rdx), %rbp
	leaq	4(%rsi), %rax
	movq	%rax, 328(%rsp)         # 8-byte Spill
	movq	48(%rsp), %r9           # 8-byte Reload
	leaq	4(%r9), %rax
	movq	%rax, 360(%rsp)         # 8-byte Spill
	movl	%edi, %ecx
	negl	%ecx
	cmpl	$-251, %ecx
	movl	$-251, %eax
	cmovbel	%eax, %ecx
	movq	24(%rsp), %rbx          # 8-byte Reload
	leal	-1(%rbx,%rcx), %eax
	movl	%eax, 344(%rsp)         # 4-byte Spill
	movl	%edx, %eax
	negl	%eax
	movq	%rcx, 392(%rsp)         # 8-byte Spill
	subl	%ecx, %eax
	movl	%eax, 336(%rsp)         # 4-byte Spill
	subq	%rdx, %rbx
	movq	96(%rsp), %rax          # 8-byte Reload
	leaq	-2(%rax,%rbx), %rcx
	movq	%rdi, %rax
	subq	%r11, %rax
	movq	40(%rsp), %rdi          # 8-byte Reload
	leaq	-2(%rdi,%rax), %r8
	leaq	16(%rsi), %rax
	movq	%rax, 352(%rsp)         # 8-byte Spill
	leaq	16(%r9), %rax
	movq	%rax, 432(%rsp)         # 8-byte Spill
	leaq	112(%rsi), %rax
	movq	%rax, 488(%rsp)         # 8-byte Spill
	leaq	112(%r9), %rax
	movq	%rax, 480(%rsp)         # 8-byte Spill
	movq	%rbp, %rax
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	movl	$0, %edi
	movq	%rdx, %r9
	movl	$1, %r15d
	movl	%r10d, %ebx
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movq	88(%rsp), %rsi          # 8-byte Reload
	movl	%r13d, 240(%rsp)        # 4-byte Spill
	movq	%r10, 216(%rsp)         # 8-byte Spill
	movq	%rbp, 232(%rsp)         # 8-byte Spill
	jmp	.LBB0_117
	.p2align	4, 0x90
.LBB0_79:                               # %.lr.ph.i381
                                        #   in Loop: Header=BB0_117 Depth=2
	movq	168(%rsp), %rax         # 8-byte Reload
	leal	-7(%rax), %edx
	subl	%edi, %edx
	shrl	$3, %edx
	incl	%edx
	movq	272(%rsp), %rsi         # 8-byte Reload
	incq	%rsi
	subq	%r9, %rsi
	incq	%rax
	andl	$3, %edx
	movq	%rsi, 272(%rsp)         # 8-byte Spill
	leaq	-8(%rsi), %rsi
	subq	%rdi, %rax
	movq	%rsi, 520(%rsp)         # 8-byte Spill
	shrq	$3, %rsi
	andq	$-8, %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	352(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rdi,4), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	432(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rdi,4), %rax
	movq	%rax, 496(%rsp)         # 8-byte Spill
	negq	%rdx
	movq	%rdx, 504(%rsp)         # 8-byte Spill
	movq	%rdi, %r9
	leaq	-1(%r9), %r10
	incq	%rsi
	movq	%rsi, 512(%rsp)         # 8-byte Spill
	movq	56(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rbx,4), %rax
	movq	%rax, 424(%rsp)         # 8-byte Spill
	movq	328(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbp,4), %rax
	movq	%rax, 408(%rsp)         # 8-byte Spill
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbx,4), %rax
	movq	%rax, 400(%rsp)         # 8-byte Spill
	movq	360(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbp,4), %rax
	movq	%rax, 416(%rsp)         # 8-byte Spill
	movq	112(%rsp), %rax         # 8-byte Reload
	movl	%eax, %esi
	notl	%esi
	movq	176(%rsp), %rax         # 8-byte Reload
	addl	%eax, %esi
	movl	%esi, 16(%rsp)          # 4-byte Spill
	leal	(%r15,%rax), %eax
	movl	%eax, 152(%rsp)         # 4-byte Spill
	movl	336(%rsp), %ebp         # 4-byte Reload
	movl	344(%rsp), %esi         # 4-byte Reload
	movl	192(%rsp), %eax         # 4-byte Reload
	movl	%eax, %r11d
	movq	%r15, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_80:                               #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_117 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_88 Depth 4
	cmpl	16(%rsp), %r9d          # 4-byte Folded Reload
	movl	%esi, 96(%rsp)          # 4-byte Spill
	jne	.LBB0_82
# BB#81:                                #   in Loop: Header=BB0_80 Depth=3
	movl	4(%rdi,%r9,4), %r12d
	jmp	.LBB0_87
	.p2align	4, 0x90
.LBB0_82:                               #   in Loop: Header=BB0_80 Depth=3
	cmpl	152(%rsp), %r9d         # 4-byte Folded Reload
	jne	.LBB0_84
# BB#83:                                #   in Loop: Header=BB0_80 Depth=3
	movl	-4(%rdi,%r9,4), %r12d
	decl	%r12d
	jmp	.LBB0_87
	.p2align	4, 0x90
.LBB0_84:                               #   in Loop: Header=BB0_80 Depth=3
	movl	(%rdi,%r9,4), %eax
	leal	-1(%rax), %r12d
	movl	4(%rdi,%r9,4), %esi
	cmpl	%esi, %r12d
	movl	-4(%rdi,%r9,4), %ebx
	jg	.LBB0_86
# BB#85:                                #   in Loop: Header=BB0_80 Depth=3
	cmpl	%ebx, %eax
	jle	.LBB0_87
.LBB0_86:                               # %._crit_edge333.i
                                        #   in Loop: Header=BB0_80 Depth=3
	leal	-1(%rbx), %r12d
	cmpl	%esi, %r12d
	cmovgl	%esi, %r12d
	cmpl	%eax, %ebx
	cmovgl	%esi, %r12d
	.p2align	4, 0x90
.LBB0_87:                               #   in Loop: Header=BB0_80 Depth=3
	movslq	%r12d, %rax
	leal	(%rax,%r11), %esi
	movslq	%esi, %r15
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_88:                               #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_117 Depth=2
                                        #       Parent Loop BB0_80 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%r13, %r14
	leaq	(%r15,%r14), %rbx
	testq	%rbx, %rbx
	jle	.LBB0_91
# BB#89:                                #   in Loop: Header=BB0_88 Depth=4
	leaq	(%rax,%r14), %rdx
	testq	%rdx, %rdx
	jle	.LBB0_91
# BB#90:                                #   in Loop: Header=BB0_88 Depth=4
	movzbl	(%r8,%rdx), %edx
	leaq	-1(%r14), %r13
	cmpb	(%rcx,%rbx), %dl
	je	.LBB0_88
.LBB0_91:                               # %.critedge258.i
                                        #   in Loop: Header=BB0_80 Depth=3
	movl	%r12d, %eax
	addq	%r14, %rax
	movq	48(%rsp), %r13          # 8-byte Reload
	movl	%eax, (%r13,%r9,4)
	addl	%r14d, %esi
	leal	(%r12,%r14), %edx
	orl	%esi, %edx
	je	.LBB0_135
# BB#92:                                #   in Loop: Header=BB0_80 Depth=3
	testl	%eax, %eax
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	40(%rsp), %r15          # 8-byte Reload
	je	.LBB0_136
# BB#93:                                #   in Loop: Header=BB0_80 Depth=3
	movl	%ebp, %eax
	subl	%r12d, %eax
	cmpl	%r14d, %eax
	je	.LBB0_137
# BB#94:                                # %.critedge5.i
                                        #   in Loop: Header=BB0_80 Depth=3
	incl	%r11d
	movl	96(%rsp), %esi          # 4-byte Reload
	incl	%esi
	decl	%ebp
	cmpq	24(%rsp), %r9           # 8-byte Folded Reload
	leaq	1(%r9), %r9
	movq	128(%rsp), %r12         # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	136(%rsp), %rbx         # 8-byte Reload
	jle	.LBB0_80
# BB#95:                                # %.critedge5._crit_edge.i
                                        #   in Loop: Header=BB0_117 Depth=2
	movq	200(%rsp), %rsi         # 8-byte Reload
	movl	(%rdi,%rsi,4), %eax
	movq	88(%rsp), %rbp          # 8-byte Reload
	movl	%eax, (%rbp,%r15,4)
	movq	280(%rsp), %rdx         # 8-byte Reload
	movl	%esi, (%rdx,%r15,4)
	movl	184(%rsp), %eax         # 4-byte Reload
	movq	224(%rsp), %r11         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_96:                               # %.lr.ph270.i
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_117 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	4(%r13,%r10,4), %esi
	cmpl	(%rbp,%r15,4), %esi
	jge	.LBB0_98
# BB#97:                                #   in Loop: Header=BB0_96 Depth=3
	movl	%esi, (%rbp,%r15,4)
	movl	%eax, (%rdx,%r15,4)
.LBB0_98:                               #   in Loop: Header=BB0_96 Depth=3
	incl	%eax
	incq	%r10
	movq	24(%rsp), %rsi          # 8-byte Reload
	cmpq	%rsi, %r10
	jle	.LBB0_96
# BB#99:                                # %.lr.ph272.i.preheader
                                        #   in Loop: Header=BB0_117 Depth=2
	movq	272(%rsp), %rax         # 8-byte Reload
	cmpq	$8, %rax
	jae	.LBB0_101
# BB#100:                               #   in Loop: Header=BB0_117 Depth=2
	movq	208(%rsp), %rdx         # 8-byte Reload
	movl	184(%rsp), %ebx         # 4-byte Reload
	jmp	.LBB0_115
.LBB0_101:                              # %min.iters.checked919
                                        #   in Loop: Header=BB0_117 Depth=2
	movq	%rax, %r9
	andq	$-8, %r9
	movq	208(%rsp), %rdx         # 8-byte Reload
	je	.LBB0_105
# BB#102:                               # %vector.memcheck935
                                        #   in Loop: Header=BB0_117 Depth=2
	movq	416(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, 424(%rsp)         # 8-byte Folded Reload
	jae	.LBB0_106
# BB#103:                               # %vector.memcheck935
                                        #   in Loop: Header=BB0_117 Depth=2
	movq	400(%rsp), %rax         # 8-byte Reload
	cmpq	408(%rsp), %rax         # 8-byte Folded Reload
	jae	.LBB0_106
.LBB0_105:                              #   in Loop: Header=BB0_117 Depth=2
	movl	184(%rsp), %ebx         # 4-byte Reload
.LBB0_115:                              # %.lr.ph272.i.preheader1004
                                        #   in Loop: Header=BB0_117 Depth=2
	decq	%rdx
	.p2align	4, 0x90
.LBB0_116:                              # %.lr.ph272.i
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_117 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	4(%r13,%rdx,4), %eax
	movl	%eax, 4(%rdi,%rdx,4)
	incq	%rdx
	cmpq	%rsi, %rdx
	jle	.LBB0_116
	jmp	.LBB0_130
.LBB0_106:                              # %vector.body914.preheader
                                        #   in Loop: Header=BB0_117 Depth=2
	testb	$3, 512(%rsp)           # 1-byte Folded Reload
	je	.LBB0_109
# BB#107:                               # %vector.body914.prol.preheader
                                        #   in Loop: Header=BB0_117 Depth=2
	xorl	%ebp, %ebp
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	504(%rsp), %rsi         # 8-byte Reload
	movq	496(%rsp), %r10         # 8-byte Reload
.LBB0_108:                              # %vector.body914.prol
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_117 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	-16(%r10,%rbp,4), %xmm0
	movupd	(%r10,%rbp,4), %xmm1
	movdqu	%xmm0, -16(%rbx,%rbp,4)
	movupd	%xmm1, (%rbx,%rbp,4)
	addq	$8, %rbp
	incq	%rsi
	jne	.LBB0_108
	jmp	.LBB0_110
.LBB0_109:                              #   in Loop: Header=BB0_117 Depth=2
	xorl	%ebp, %ebp
	movq	168(%rsp), %rax         # 8-byte Reload
.LBB0_110:                              # %vector.body914.prol.loopexit
                                        #   in Loop: Header=BB0_117 Depth=2
	cmpq	$24, 520(%rsp)          # 8-byte Folded Reload
	jb	.LBB0_113
# BB#111:                               # %vector.body914.preheader.new
                                        #   in Loop: Header=BB0_117 Depth=2
	subq	%rbp, %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	addq	%rdx, %rbp
	movq	488(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbp,4), %rsi
	movq	480(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbp,4), %rbp
	movq	168(%rsp), %rax         # 8-byte Reload
.LBB0_112:                              # %vector.body914
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_117 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rbp), %xmm0
	movupd	(%rbp), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movupd	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-32, %rax
	jne	.LBB0_112
.LBB0_113:                              # %middle.block915
                                        #   in Loop: Header=BB0_117 Depth=2
	cmpq	%r9, 272(%rsp)          # 8-byte Folded Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	184(%rsp), %ebx         # 4-byte Reload
	je	.LBB0_130
# BB#114:                               #   in Loop: Header=BB0_117 Depth=2
	addq	%r9, %rdx
	jmp	.LBB0_115
	.p2align	4, 0x90
.LBB0_117:                              #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_80 Depth 3
                                        #         Child Loop BB0_88 Depth 4
                                        #       Child Loop BB0_96 Depth 3
                                        #       Child Loop BB0_108 Depth 3
                                        #       Child Loop BB0_112 Depth 3
                                        #       Child Loop BB0_116 Depth 3
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movl	%ebx, 184(%rsp)         # 4-byte Spill
	movslq	%ebx, %rdx
	cmpq	%rdx, %rax
	movq	%rdx, 208(%rsp)         # 8-byte Spill
	movq	%rax, 264(%rsp)         # 8-byte Spill
	cmovgeq	%rax, %rdx
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	leaq	(%rbp,%rdi), %rbp
	movl	%r10d, %eax
	subl	%edi, %eax
	movslq	%eax, %r9
	cmpq	%r9, %rbp
	movq	%r9, %rax
	cmovgeq	%rbp, %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movl	%r10d, %eax
	subl	%edi, %eax
	movslq	%eax, %rbx
	cmpq	%rbx, %rbp
	cmovlq	%rbx, %rbp
	movl	(%rsi,%rdi,4), %esi
	movl	%r11d, %eax
	subl	%esi, %eax
	movl	$2, %esi
	cmpl	%eax, %r13d
	jge	.LBB0_121
# BB#118:                               #   in Loop: Header=BB0_117 Depth=2
	cmpl	%eax, 104(%rsp)         # 4-byte Folded Reload
	jle	.LBB0_120
# BB#119:                               #   in Loop: Header=BB0_117 Depth=2
	movl	options+28(%rip), %esi
	jmp	.LBB0_121
.LBB0_120:                              #   in Loop: Header=BB0_117 Depth=2
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI0_0(%rip), %xmm0
	addsd	.LCPI0_1(%rip), %xmm0
	cvttsd2si	%xmm0, %esi
.LBB0_121:                              # %good_ratio.exit.i
                                        #   in Loop: Header=BB0_117 Depth=2
	movslq	%esi, %rax
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	cmpq	%rax, %rdi
	movq	208(%rsp), %rdi         # 8-byte Reload
	jle	.LBB0_128
# BB#122:                               #   in Loop: Header=BB0_117 Depth=2
	movq	112(%rsp), %rsi         # 8-byte Reload
	testq	%rsi, %rsi
	jle	.LBB0_131
# BB#123:                               #   in Loop: Header=BB0_117 Depth=2
	leaq	-1(%rsi), %rdx
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	-4(%rax,%rsi,4), %esi
	movq	224(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%esi, %eax
	movl	$2, %esi
	cmpl	%eax, %r13d
	movq	208(%rsp), %rdi         # 8-byte Reload
	jge	.LBB0_127
# BB#124:                               #   in Loop: Header=BB0_117 Depth=2
	cmpl	%eax, 104(%rsp)         # 4-byte Folded Reload
	jle	.LBB0_126
# BB#125:                               #   in Loop: Header=BB0_117 Depth=2
	movl	options+28(%rip), %esi
	jmp	.LBB0_127
.LBB0_126:                              #   in Loop: Header=BB0_117 Depth=2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI0_0(%rip), %xmm0
	addsd	.LCPI0_1(%rip), %xmm0
	cvttsd2si	%xmm0, %esi
.LBB0_127:                              # %good_ratio.exit260.i
                                        #   in Loop: Header=BB0_117 Depth=2
	movslq	%esi, %rax
	cmpq	%rax, %rdx
	jg	.LBB0_131
.LBB0_128:                              # %.critedge5.preheader.i
                                        #   in Loop: Header=BB0_117 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	1(%rax), %rsi
	movq	120(%rsp), %rax         # 8-byte Reload
	decq	%rax
	cmpq	%rsi, %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	%rsi, 256(%rsp)         # 8-byte Spill
	jle	.LBB0_79
# BB#129:                               # %.critedge5._crit_edge.thread.i
                                        #   in Loop: Header=BB0_117 Depth=2
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	(%rdi,%rax,4), %eax
	movq	88(%rsp), %rbp          # 8-byte Reload
	movl	%eax, (%rbp,%r15,4)
	movq	176(%rsp), %rax         # 8-byte Reload
	movq	280(%rsp), %rdx         # 8-byte Reload
	movl	%eax, (%rdx,%r15,4)
	movl	184(%rsp), %ebx         # 4-byte Reload
	movq	224(%rsp), %r11         # 8-byte Reload
.LBB0_130:                              # %.loopexit.i
                                        #   in Loop: Header=BB0_117 Depth=2
	decl	192(%rsp)               # 4-byte Folded Spill
	decl	%ebx
	movq	112(%rsp), %rdi         # 8-byte Reload
	incq	%rdi
	decl	344(%rsp)               # 4-byte Folded Spill
	incl	336(%rsp)               # 4-byte Folded Spill
	movq	264(%rsp), %rax         # 8-byte Reload
	incq	%rax
	cmpq	%r11, %r15
	leaq	1(%r15), %r15
	movq	%rbp, %rsi
	movq	232(%rsp), %rbp         # 8-byte Reload
	movq	256(%rsp), %r9          # 8-byte Reload
	movq	216(%rsp), %r10         # 8-byte Reload
	movl	240(%rsp), %r13d        # 4-byte Reload
	jle	.LBB0_117
.LBB0_131:                              # %.critedge4.preheader.i
                                        #   in Loop: Header=BB0_4 Depth=1
	movslq	%r15d, %rbx
	movq	%rbx, %rdx
	shlq	$32, %rdx
	movabsq	$-4294967296, %rdi      # imm = 0xFFFFFFFF00000000
	movq	88(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_132:                              # %.critedge4.i
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rcx
	movq	%rdx, %rax
	testq	%rcx, %rcx
	jle	.LBB0_134
# BB#133:                               #   in Loop: Header=BB0_132 Depth=2
	leaq	-1(%rcx), %rbx
	movl	-4(%rbp,%rcx,4), %esi
	subl	(%rbp,%rcx,4), %esi
	leaq	(%rax,%rdi), %rdx
	cmpl	$3, %esi
	jl	.LBB0_132
.LBB0_134:                              # %.critedge11.i
                                        #   in Loop: Header=BB0_4 Depth=1
	sarq	$30, %rax
	movl	(%rbp,%rax), %edx
	movl	376(%rsp), %r13d        # 4-byte Reload
	addl	%edx, %r13d
	movq	%rbp, %rdi
	movl	384(%rsp), %ebp         # 4-byte Reload
	subl	224(%rsp), %ebp         # 4-byte Folded Reload
	addl	%edx, %ebp
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	280(%rsp), %rbx         # 8-byte Reload
	addl	(%rbx,%rax), %ebp
	callq	free
	movq	%rbx, %rdi
	movq	40(%rsp), %r15          # 8-byte Reload
	callq	free
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	%ebp, %ecx
	movl	%r13d, %edx
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	160(%rsp), %r13         # 8-byte Reload
	jmp	.LBB0_138
.LBB0_135:                              #   in Loop: Header=BB0_4 Depth=1
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%r13, %rdi
	callq	free
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	280(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	248(%rsp), %rax         # 8-byte Reload
	addl	392(%rsp), %eax         # 4-byte Folded Reload
	addl	%r12d, %eax
	addl	%r14d, %eax
	movl	96(%rsp), %ecx          # 4-byte Reload
	addl	%r12d, %ecx
	addl	%r14d, %ecx
	movl	%eax, %edx
	movq	128(%rsp), %r12         # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	160(%rsp), %r13         # 8-byte Reload
	movq	40(%rsp), %r15          # 8-byte Reload
	jmp	.LBB0_138
.LBB0_136:                              #   in Loop: Header=BB0_4 Depth=1
	callq	free
	movq	%r13, %rdi
	callq	free
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	280(%rsp), %rdi         # 8-byte Reload
	callq	free
	addl	96(%rsp), %r12d         # 4-byte Folded Reload
	addl	%r12d, %r14d
	movl	%r14d, %ecx
	movq	128(%rsp), %r12         # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	160(%rsp), %r13         # 8-byte Reload
	movl	376(%rsp), %edx         # 4-byte Reload
	jmp	.LBB0_138
.LBB0_137:                              #   in Loop: Header=BB0_4 Depth=1
	callq	free
	movq	%r13, %rdi
	callq	free
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	280(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	248(%rsp), %rax         # 8-byte Reload
	addl	392(%rsp), %eax         # 4-byte Folded Reload
	addl	%r12d, %eax
	addl	%eax, %r14d
	movl	%r14d, %edx
	movq	128(%rsp), %r12         # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	160(%rsp), %r13         # 8-byte Reload
	movl	384(%rsp), %ecx         # 4-byte Reload
.LBB0_138:                              # %extend_bw.exit
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	%edx, %eax
	notl	%eax
	addl	4(%r13), %eax
	imull	options+56(%rip), %eax
	imull	options+48(%rip), %r15d
	addl	%eax, %r15d
	js	.LBB0_140
# BB#139:                               #   in Loop: Header=BB0_4 Depth=1
	incl	%edx
	movl	%edx, 4(%r13)
	incl	%ecx
	movl	%ecx, (%r13)
	.p2align	4, 0x90
.LBB0_140:                              # %.thread537
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpl	$0, 40(%r14)
	jne	.LBB0_276
# BB#141:                               #   in Loop: Header=BB0_4 Depth=1
	movq	8(%r14), %rax
	movl	16(%r14), %r10d
	leal	-1(%r10), %ecx
	movq	(%rax,%rcx,8), %r11
	movl	12(%r11), %eax
	movl	4148(%r12), %edi
	cmpl	%eax, %edi
	jbe	.LBB0_276
# BB#142:                               #   in Loop: Header=BB0_4 Depth=1
	subl	%eax, %edi
	cmpl	$61, %edi
	jb	.LBB0_161
# BB#143:                               #   in Loop: Header=BB0_4 Depth=1
	movl	28(%r14), %ecx
	addl	24(%r14), %ecx
	cmpl	%ecx, 8(%r11)
	movq	%r14, %rbp
	movq	144(%rsp), %rsi         # 8-byte Reload
	jae	.LBB0_149
# BB#144:                               #   in Loop: Header=BB0_4 Depth=1
	movl	36(%rsi), %ecx
	cmpl	$10, %ecx
	movl	$10, %edx
	cmovael	%edx, %ecx
	addq	16(%r12), %rax
	movl	%ecx, 324(%rsp)
	movq	%rax, 296(%rsp)
	movl	%edi, 304(%rsp)
	leal	-2(%rcx,%rcx), %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	decl	%eax
	movl	%eax, 320(%rsp)
	incl	%edi
	shlq	$2, %rdi
	movq	%r11, %r14
	callq	xmalloc
	movq	%rax, %r13
	movq	%r13, 312(%rsp)
	movl	$524288, %edi           # imm = 0x80000
	movl	$8, %esi
	callq	xcalloc
	movq	%rax, %rbx
	movq	%rbx, 288(%rsp)
	leaq	288(%rsp), %r15
	movq	%r15, %rdi
	callq	bld_table
	movl	8(%r14), %esi
	movq	%r14, 104(%rsp)         # 8-byte Spill
	movl	12(%r14), %ecx
	movq	144(%rsp), %r14         # 8-byte Reload
	movl	24(%rbp), %edx
	subl	%esi, %edx
	leal	1(%rsi), %r8d
	addq	8(%r14), %rsi
	addl	28(%rbp), %edx
	incl	%ecx
	movl	options+24(%rip), %r9d
	subq	$8, %rsp
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	movq	%r15, %rdi
	leaq	72(%rsp), %rax
	pushq	%rax
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	leaq	480(%rsp), %rax
	pushq	%rax
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	callq	exon_cores
	addq	$32, %rsp
.Lcfi27:
	.cfi_adjust_cfa_offset -32
	movq	%r13, %rdi
	callq	free
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_145:                              #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rbp,8), %rdi
	movl	$free, %esi
	callq	tdestroy
	incq	%rbp
	cmpq	$524288, %rbp           # imm = 0x80000
	jne	.LBB0_145
# BB#146:                               # %free_hash_env.exit388
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	%rbx, %rdi
	callq	free
	movl	72(%rsp), %eax
	testq	%rax, %rax
	je	.LBB0_150
# BB#147:                               # %.lr.ph.i389
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	64(%rsp), %rcx
	testb	$1, %al
	jne	.LBB0_151
# BB#148:                               #   in Loop: Header=BB0_4 Depth=1
	xorl	%edx, %edx
	cmpl	$1, %eax
	jne	.LBB0_152
	jmp	.LBB0_154
.LBB0_149:                              #   in Loop: Header=BB0_4 Depth=1
	movq	%rbp, %r14
	movq	8(%rsp), %rbp           # 8-byte Reload
	testl	%r10d, %r10d
	jne	.LBB0_162
	jmp	.LBB0_188
.LBB0_150:                              # %free_hash_env.exit388._crit_edge
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	32(%rsp), %r14          # 8-byte Reload
	movl	16(%r14), %r10d
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	104(%rsp), %r11         # 8-byte Reload
	testl	%r10d, %r10d
	jne	.LBB0_162
	jmp	.LBB0_188
.LBB0_151:                              #   in Loop: Header=BB0_4 Depth=1
	movq	(%rcx), %rdx
	movl	(%rdx), %esi
	movl	4(%rdx), %edi
	movl	%edi, (%rdx)
	movl	%esi, 4(%rdx)
	movl	8(%rdx), %esi
	movl	12(%rdx), %edi
	movl	%edi, 8(%rdx)
	movl	%esi, 12(%rdx)
	movl	$1, %edx
	cmpl	$1, %eax
	je	.LBB0_154
.LBB0_152:                              # %.lr.ph.i389.new
                                        #   in Loop: Header=BB0_4 Depth=1
	subq	%rdx, %rax
	leaq	8(%rcx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB0_153:                              #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rdx), %rsi
	movl	(%rsi), %edi
	movl	4(%rsi), %ebp
	movl	%ebp, (%rsi)
	movl	%edi, 4(%rsi)
	movl	8(%rsi), %edi
	movl	12(%rsi), %ebp
	movl	%ebp, 8(%rsi)
	movl	%edi, 12(%rsi)
	movq	(%rdx), %rsi
	movl	(%rsi), %edi
	movl	4(%rsi), %ebp
	movl	%ebp, (%rsi)
	movl	%edi, 4(%rsi)
	movl	8(%rsi), %edi
	movl	12(%rsi), %ebp
	movl	%ebp, 8(%rsi)
	movl	%edi, 12(%rsi)
	addq	$16, %rdx
	addq	$-2, %rax
	jne	.LBB0_153
.LBB0_154:                              # %swap_seqs.exit393
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	(%rcx), %rax
	movq	8(%r14), %r8
	movl	(%rax), %edx
	leaq	-2(%r8,%rdx), %rcx
	cmpq	%r8, %rcx
	jb	.LBB0_160
# BB#155:                               # %swap_seqs.exit393
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	16(%r12), %r10
	movl	4(%rax), %edi
	leaq	-2(%r10,%rdi), %rcx
	cmpq	%r10, %rcx
	jb	.LBB0_160
# BB#156:                               # %.lr.ph.i394.preheader
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	%edx, %ecx
	addq	%r8, %rdx
	movl	%edi, %ebp
	addq	%r10, %rdi
	leal	-1(%rbp), %r9d
	leal	-1(%rcx), %ebp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_157:                              # %.lr.ph.i394
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-2(%rdx,%rcx), %ebx
	cmpb	-2(%rdi,%rcx), %bl
	jne	.LBB0_160
# BB#158:                               #   in Loop: Header=BB0_157 Depth=2
	leaq	-3(%rdi,%rcx), %rbx
	leal	(%rbp,%rcx), %esi
	movl	%esi, (%rax)
	leal	(%r9,%rcx), %esi
	movl	%esi, 4(%rax)
	cmpq	%r10, %rbx
	jb	.LBB0_160
# BB#159:                               #   in Loop: Header=BB0_157 Depth=2
	leaq	-3(%rdx,%rcx), %rsi
	decq	%rcx
	cmpq	%r8, %rsi
	jae	.LBB0_157
.LBB0_160:                              # %grow_exon_left.exit
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	16(%rax), %edx
	movl	36(%r14), %ecx
	movq	%rax, %r14
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rdi
	leaq	64(%rsp), %rsi
	callq	merge
	movl	$0, 72(%rsp)
	movq	8(%r14), %rax
	movl	16(%r14), %r10d
	leal	-1(%r10), %ecx
	movq	(%rax,%rcx,8), %r11
.LBB0_161:                              # %.preheader586
                                        #   in Loop: Header=BB0_4 Depth=1
	testl	%r10d, %r10d
	je	.LBB0_188
.LBB0_162:                              # %.lr.ph642
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	%r11, %r13
	movq	(%rbp), %r9
	movq	16(%r12), %r8
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_163:                              #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_165 Depth 3
	movl	%r15d, %eax
	notl	%eax
	addl	%r10d, %eax
	movq	(%r9,%rax,8), %rax
	movl	4(%rax), %r14d
	movl	12(%rax), %r11d
	leal	-1(%r14), %edx
	cmpl	%r11d, %edx
	movl	$0, %edi
	movl	$0, %r12d
	movl	$0, %esi
	movl	$0, %ebp
	movl	$0, %eax
	jae	.LBB0_173
# BB#164:                               # %.lr.ph.preheader.i397
                                        #   in Loop: Header=BB0_163 Depth=2
	movl	%edx, %edx
	addq	%r8, %rdx
	leal	1(%r11), %ebx
	subl	%r14d, %ebx
	xorl	%eax, %eax
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	xorl	%r12d, %r12d
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_165:                              # %.lr.ph.i405
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_163 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rdx), %ecx
	addb	$-65, %cl
	cmpb	$19, %cl
	ja	.LBB0_168
# BB#166:                               # %.lr.ph.i405
                                        #   in Loop: Header=BB0_165 Depth=3
	movzbl	%cl, %ecx
	jmpq	*.LJTI0_1(,%rcx,8)
.LBB0_167:                              #   in Loop: Header=BB0_165 Depth=3
	incl	%edi
	jmp	.LBB0_172
	.p2align	4, 0x90
.LBB0_168:                              #   in Loop: Header=BB0_165 Depth=3
	incl	%eax
	jmp	.LBB0_172
	.p2align	4, 0x90
.LBB0_169:                              #   in Loop: Header=BB0_165 Depth=3
	incl	%r12d
	jmp	.LBB0_172
	.p2align	4, 0x90
.LBB0_170:                              #   in Loop: Header=BB0_165 Depth=3
	incl	%esi
	jmp	.LBB0_172
	.p2align	4, 0x90
.LBB0_171:                              #   in Loop: Header=BB0_165 Depth=3
	incl	%ebp
	.p2align	4, 0x90
.LBB0_172:                              #   in Loop: Header=BB0_165 Depth=3
	incq	%rdx
	decl	%ebx
	jne	.LBB0_165
.LBB0_173:                              # %._crit_edge.i418
                                        #   in Loop: Header=BB0_163 Depth=2
	movl	$1, %ebx
	subl	%r14d, %ebx
	addl	%r11d, %ebx
	subl	%eax, %ebx
	leal	(%rdi,%rdi), %eax
	leal	(%rax,%rax,4), %eax
	xorl	%edx, %edx
	divl	%ebx
	cmpl	$29, %ebx
	ja	.LBB0_178
# BB#174:                               #   in Loop: Header=BB0_163 Depth=2
	cmpl	$6, %eax
	movq	32(%rsp), %r14          # 8-byte Reload
	ja	.LBB0_182
# BB#175:                               #   in Loop: Header=BB0_163 Depth=2
	addl	%edi, %esi
	addl	%esi, %esi
	leal	(%rsi,%rsi,4), %eax
	xorl	%edx, %edx
	divl	%ebx
	cmpl	$7, %eax
	ja	.LBB0_182
# BB#176:                               #   in Loop: Header=BB0_163 Depth=2
	leal	(%rbp,%rbp), %eax
	leal	(%rax,%rax,4), %eax
	xorl	%edx, %edx
	divl	%ebx
	cmpl	$6, %eax
	ja	.LBB0_182
# BB#177:                               #   in Loop: Header=BB0_163 Depth=2
	addl	%r12d, %ebp
	addl	%ebp, %ebp
	leal	(%rbp,%rbp,4), %eax
	xorl	%edx, %edx
	divl	%ebx
	cmpl	$7, %eax
	ja	.LBB0_182
	jmp	.LBB0_183
	.p2align	4, 0x90
.LBB0_178:                              #   in Loop: Header=BB0_163 Depth=2
	cmpl	$7, %eax
	movq	32(%rsp), %r14          # 8-byte Reload
	ja	.LBB0_182
# BB#179:                               #   in Loop: Header=BB0_163 Depth=2
	addl	%edi, %esi
	imull	$100, %esi, %eax
	xorl	%edx, %edx
	divl	%ebx
	cmpl	$94, %eax
	ja	.LBB0_182
# BB#180:                               #   in Loop: Header=BB0_163 Depth=2
	leal	(%rbp,%rbp), %eax
	leal	(%rax,%rax,4), %eax
	xorl	%edx, %edx
	divl	%ebx
	cmpl	$7, %eax
	ja	.LBB0_182
# BB#181:                               #   in Loop: Header=BB0_163 Depth=2
	addl	%r12d, %ebp
	imull	$100, %ebp, %eax
	xorl	%edx, %edx
	divl	%ebx
	cmpl	$95, %eax
	jb	.LBB0_183
	.p2align	4, 0x90
.LBB0_182:                              #   in Loop: Header=BB0_163 Depth=2
	incl	%r15d
	cmpl	%r15d, %r10d
	ja	.LBB0_163
.LBB0_183:                              # %.critedge362
                                        #   in Loop: Header=BB0_4 Depth=1
	testl	%r15d, %r15d
	movq	128(%rsp), %r12         # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%r13, %r11
	je	.LBB0_188
# BB#184:                               #   in Loop: Header=BB0_4 Depth=1
	movl	%r10d, %ebx
	subl	%r15d, %ebx
	cmpl	%r10d, %ebx
	jae	.LBB0_186
	.p2align	4, 0x90
.LBB0_185:                              # %.lr.ph650
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r14), %rax
	movl	%ebx, %ecx
	movq	(%rax,%rcx,8), %rdi
	callq	free
	incl	%ebx
	movl	16(%r14), %r10d
	cmpl	%r10d, %ebx
	jb	.LBB0_185
.LBB0_186:                              # %._crit_edge651
                                        #   in Loop: Header=BB0_4 Depth=1
	subl	%r15d, %r10d
	movl	%r10d, 16(%r14)
	movq	144(%rsp), %r15         # 8-byte Reload
	je	.LBB0_619
# BB#187:                               # %.thread539
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	(%rbp), %rax
	decl	%r10d
	movq	(%rax,%r10,8), %r11
.LBB0_188:                              # %.critedge362.thread
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	4148(%r12), %r13d
	movl	12(%r11), %r9d
	movl	%r13d, %r15d
	subl	%r9d, %r15d
	je	.LBB0_276
# BB#189:                               #   in Loop: Header=BB0_4 Depth=1
	cmpl	$250, %r15d
	movl	$250, %eax
	cmovael	%eax, %r15d
	movq	16(%r12), %rsi
	addq	%r9, %rsi
	movl	8(%r11), %r8d
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	8(%rcx), %rdi
	addq	%r8, %rdi
	leal	(,%r15,4), %eax
	movl	16(%rcx), %r10d
	subl	%r8d, %r10d
	cmpl	%r10d, %eax
	cmovlel	%eax, %r10d
	movl	36(%rcx), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	xorl	%ebx, %ebx
	testl	%r15d, %r15d
	jle	.LBB0_195
# BB#190:                               #   in Loop: Header=BB0_4 Depth=1
	testl	%r10d, %r10d
	jle	.LBB0_195
# BB#191:                               # %.lr.ph302.preheader.i
                                        #   in Loop: Header=BB0_4 Depth=1
	movslq	%r10d, %rax
	movl	%r15d, %ecx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_192:                              # %.lr.ph302.i
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rsi,%rbx), %edx
	cmpb	(%rdi,%rbx), %dl
	jne	.LBB0_195
# BB#193:                               #   in Loop: Header=BB0_192 Depth=2
	incq	%rbx
	cmpq	%rcx, %rbx
	jge	.LBB0_195
# BB#194:                               #   in Loop: Header=BB0_192 Depth=2
	cmpq	%rax, %rbx
	jl	.LBB0_192
.LBB0_195:                              # %.critedge.i422
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpl	%r15d, %ebx
	jne	.LBB0_197
# BB#196:                               #   in Loop: Header=BB0_4 Depth=1
	addl	%r15d, %r8d
	xorl	%r13d, %r13d
	jmp	.LBB0_274
.LBB0_197:                              #   in Loop: Header=BB0_4 Depth=1
	cmpl	%r10d, %ebx
	jne	.LBB0_199
# BB#198:                               #   in Loop: Header=BB0_4 Depth=1
	addl	%r10d, %r8d
	xorl	%r13d, %r13d
	movl	%r10d, %r15d
	jmp	.LBB0_274
.LBB0_199:                              #   in Loop: Header=BB0_4 Depth=1
	movq	%rdi, 272(%rsp)         # 8-byte Spill
	movq	%rsi, 200(%rsp)         # 8-byte Spill
	movq	%r8, 232(%rsp)          # 8-byte Spill
	movq	%r9, 256(%rsp)          # 8-byte Spill
	movq	%r11, 104(%rsp)         # 8-byte Spill
	leal	1(%r15), %r14d
	leal	(%r10,%r15), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movq	%r10, 136(%rsp)         # 8-byte Spill
	leal	1(%r10,%r15), %eax
	movslq	%eax, %rbp
	shlq	$2, %rbp
	movq	%rbp, %rdi
	callq	xmalloc
	movq	%rax, %r12
	movq	%rbp, %rdi
	callq	xmalloc
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	24(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	js	.LBB0_201
# BB#200:                               # %.lr.ph296.preheader.i
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	%eax, %eax
	leaq	4(,%rax,4), %rdx
	movl	$255, %esi
	movq	%r12, %rdi
	callq	memset
.LBB0_201:                              # %._crit_edge297.i
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	%r15d, %eax
	movq	%r12, 208(%rsp)         # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%ebx, (%r12,%rax,4)
	movl	%r14d, %eax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	shll	$2, %r14d
	movq	%r14, %rdi
	callq	xmalloc
	movq	%rax, %rbp
	movq	%r14, %rdi
	callq	xmalloc
	movq	%rax, %rbx
	testl	%r15d, %r15d
	jle	.LBB0_203
# BB#202:                               # %._crit_edge293.thread.i
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	%rbp, %rdi
	addq	$4, %rdi
	leal	-1(%r15), %eax
	leaq	4(,%rax,4), %rdx
	movl	$255, %esi
	callq	memset
.LBB0_203:                              # %.lr.ph285.i
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	208(%rsp), %r10         # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	(%r10,%rdi,4), %eax
	movl	%eax, (%rbp)
	movq	%rbx, 264(%rsp)         # 8-byte Spill
	movl	%r15d, (%rbx)
	movl	16(%rsp), %r11d         # 4-byte Reload
	movl	%r11d, %edx
	shrl	$31, %edx
	addl	%r11d, %edx
	sarl	%edx
	addl	%r11d, %r11d
	movslq	136(%rsp), %rax         # 4-byte Folded Reload
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	256(%rsp), %rax         # 8-byte Reload
	leal	-1(%rax), %eax
	subl	%r13d, %eax
	cmpl	$-251, %eax
	movl	$-251, %ecx
	cmovbel	%ecx, %eax
	movl	$-2, %esi
	subl	%eax, %esi
	leaq	4(%r10), %rax
	movq	%rax, 336(%rsp)         # 8-byte Spill
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	4(%rcx), %rax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	leaq	16(%r10), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	leaq	16(%rcx), %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	leaq	112(%r10), %rax
	movq	%rax, 408(%rsp)         # 8-byte Spill
	leaq	112(%rcx), %rax
	movq	%rax, 400(%rsp)         # 8-byte Spill
	movl	$-1, 224(%rsp)          # 4-byte Folded Spill
	movl	%esi, %ebx
	movq	%rdi, 176(%rsp)         # 8-byte Spill
	movq	160(%rsp), %r13         # 8-byte Reload
	movq	%r13, %rcx
	movl	%ebx, %r13d
	movq	%rcx, %rbx
	xorl	%edi, %edi
	movl	$1, %eax
	movq	128(%rsp), %r12         # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	%rbp, %r9
	movl	%edx, 192(%rsp)         # 4-byte Spill
	movl	%r11d, 16(%rsp)         # 4-byte Spill
	movq	%r9, 216(%rsp)          # 8-byte Spill
	movq	%rsi, 344(%rsp)         # 8-byte Spill
	jmp	.LBB0_250
.LBB0_204:                              # %.lr.ph275.i
                                        #   in Loop: Header=BB0_250 Depth=2
	movq	152(%rsp), %r11         # 8-byte Reload
	leal	-7(%r11), %edi
	movq	184(%rsp), %rsi         # 8-byte Reload
	subl	%esi, %edi
	shrl	$3, %edi
	incl	%edi
	incq	%rbx
	subq	%r8, %rbx
	incq	%r11
	andl	$3, %edi
	movq	%rbx, 168(%rsp)         # 8-byte Spill
	leaq	-8(%rbx), %rcx
	subq	%rsi, %r11
	movq	%rcx, 376(%rsp)         # 8-byte Spill
	movq	%rcx, %rbx
	shrq	$3, %rbx
	andq	$-8, %r11
	movq	%r11, 152(%rsp)         # 8-byte Spill
	movq	248(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi,4), %rcx
	movq	%rcx, 424(%rsp)         # 8-byte Spill
	movq	240(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi,4), %rcx
	movq	%rcx, 416(%rsp)         # 8-byte Spill
	negq	%rdi
	movq	%rdi, 384(%rsp)         # 8-byte Spill
	leaq	-1(%rsi), %r11
	incq	%rbx
	movq	%rbx, 392(%rsp)         # 8-byte Spill
	leaq	(%r10,%r13,4), %rcx
	movq	%rcx, 328(%rsp)         # 8-byte Spill
	movq	336(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,4), %rcx
	movq	%rcx, 352(%rsp)         # 8-byte Spill
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r13,4), %rcx
	movq	%rcx, 432(%rsp)         # 8-byte Spill
	movq	280(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,4), %rax
	movq	%rax, 360(%rsp)         # 8-byte Spill
	movslq	%edx, %r8
	movq	112(%rsp), %rax         # 8-byte Reload
	movl	%eax, %ebp
	notl	%ebp
	movq	24(%rsp), %rcx          # 8-byte Reload
	addl	%ecx, %ebp
	movq	48(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%rcx), %eax
	movl	%eax, 88(%rsp)          # 4-byte Spill
	movq	%r8, %rax
	movl	224(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, %r13d
	movq	136(%rsp), %rbx         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_205:                              #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_250 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_217 Depth 4
	cmpl	%ebp, %eax
	jne	.LBB0_207
# BB#206:                               #   in Loop: Header=BB0_205 Depth=3
	movl	4(%r10,%rax,4), %r14d
	incl	%r14d
	jmp	.LBB0_213
	.p2align	4, 0x90
.LBB0_207:                              #   in Loop: Header=BB0_205 Depth=3
	cmpl	88(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB0_209
# BB#208:                               #   in Loop: Header=BB0_205 Depth=3
	movl	-4(%r10,%rax,4), %r14d
	jmp	.LBB0_213
	.p2align	4, 0x90
.LBB0_209:                              #   in Loop: Header=BB0_205 Depth=3
	movl	(%r10,%rax,4), %esi
	movl	4(%r10,%rax,4), %edx
	cmpl	%edx, %esi
	jge	.LBB0_211
# BB#210:                               # %._crit_edge349.i
                                        #   in Loop: Header=BB0_205 Depth=3
	movl	-4(%r10,%rax,4), %edi
	jmp	.LBB0_212
.LBB0_211:                              #   in Loop: Header=BB0_205 Depth=3
	leal	1(%rsi), %r14d
	movl	-4(%r10,%rax,4), %edi
	cmpl	%edi, %r14d
	jge	.LBB0_213
.LBB0_212:                              #   in Loop: Header=BB0_205 Depth=3
	leal	1(%rdx), %r14d
	cmpl	%edi, %r14d
	cmovll	%edi, %r14d
	cmpl	%esi, %edx
	cmovll	%edi, %r14d
	.p2align	4, 0x90
.LBB0_213:                              #   in Loop: Header=BB0_205 Depth=3
	movl	%eax, %r12d
	subl	%r15d, %r12d
	addl	%r14d, %r12d
	testl	%r14d, %r14d
	js	.LBB0_223
# BB#214:                               # %.preheader.i431
                                        #   in Loop: Header=BB0_205 Depth=3
	cmpl	%r15d, %r14d
	jge	.LBB0_223
# BB#215:                               # %.preheader.i431
                                        #   in Loop: Header=BB0_205 Depth=3
	cmpl	%ebx, %r12d
	jge	.LBB0_223
# BB#216:                               # %.lr.ph.preheader.i432
                                        #   in Loop: Header=BB0_205 Depth=3
	leal	(%r14,%r13), %r12d
	movslq	%r14d, %rdi
	movq	200(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi), %rsi
	incq	%rdi
	movslq	%r12d, %r10
	movq	272(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r10), %r9
	incq	%r10
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_217:                              # %.lr.ph.i435
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_250 Depth=2
                                        #       Parent Loop BB0_205 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rsi,%rdx), %ecx
	cmpb	(%r9,%rdx), %cl
	jne	.LBB0_221
# BB#218:                               #   in Loop: Header=BB0_217 Depth=4
	leaq	(%rdi,%rdx), %rbx
	leaq	1(%rdx), %rcx
	cmpq	24(%rsp), %rbx          # 8-byte Folded Reload
	jge	.LBB0_220
# BB#219:                               #   in Loop: Header=BB0_217 Depth=4
	addq	%r10, %rdx
	cmpq	96(%rsp), %rdx          # 8-byte Folded Reload
	movq	%rcx, %rdx
	jl	.LBB0_217
.LBB0_220:                              # %.critedge5.i438.loopexitsplit
                                        #   in Loop: Header=BB0_205 Depth=3
	addl	%ecx, %r14d
	addl	%ecx, %r12d
	jmp	.LBB0_222
.LBB0_221:                              # %.lr.ph.i435..critedge5.i438.loopexit_crit_edge
                                        #   in Loop: Header=BB0_205 Depth=3
	addl	%edx, %r14d
	addl	%edx, %r12d
.LBB0_222:                              # %.critedge5.i438
                                        #   in Loop: Header=BB0_205 Depth=3
	movq	136(%rsp), %rbx         # 8-byte Reload
	movq	208(%rsp), %r10         # 8-byte Reload
.LBB0_223:                              # %.critedge5.i438
                                        #   in Loop: Header=BB0_205 Depth=3
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	%r14d, (%rcx,%rax,4)
	cmpl	%r15d, %r14d
	jne	.LBB0_225
# BB#224:                               # %.critedge5.i438
                                        #   in Loop: Header=BB0_205 Depth=3
	cmpl	%ebx, %r12d
	je	.LBB0_269
.LBB0_225:                              #   in Loop: Header=BB0_205 Depth=3
	cmpl	%r15d, %r14d
	je	.LBB0_270
# BB#226:                               #   in Loop: Header=BB0_205 Depth=3
	cmpl	%ebx, %r12d
	je	.LBB0_271
# BB#227:                               # %.critedge3.i
                                        #   in Loop: Header=BB0_205 Depth=3
	incl	%r13d
	cmpq	56(%rsp), %rax          # 8-byte Folded Reload
	leaq	1(%rax), %rax
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	jl	.LBB0_205
# BB#228:                               # %.critedge3._crit_edge.i
                                        #   in Loop: Header=BB0_250 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	(%r10,%rax,4), %eax
	movq	216(%rsp), %r9          # 8-byte Reload
	movl	%eax, (%r9,%rsi,4)
	movq	264(%rsp), %rdx         # 8-byte Reload
	movl	%r15d, (%rdx,%rsi,4)
	movl	120(%rsp), %eax         # 4-byte Reload
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_229:                              # %.lr.ph277.i
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_250 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	4(%rdi,%r11,4), %ecx
	cmpl	(%r9,%rsi,4), %ecx
	jle	.LBB0_231
# BB#230:                               #   in Loop: Header=BB0_229 Depth=3
	movl	%ecx, (%r9,%rsi,4)
	movl	%eax, (%rdx,%rsi,4)
.LBB0_231:                              #   in Loop: Header=BB0_229 Depth=3
	incl	%eax
	incq	%r11
	cmpq	%rbx, %r11
	jl	.LBB0_229
# BB#232:                               # %.lr.ph279.i.preheader
                                        #   in Loop: Header=BB0_250 Depth=2
	movq	168(%rsp), %rax         # 8-byte Reload
	cmpq	$8, %rax
	movq	128(%rsp), %r12         # 8-byte Reload
	movl	120(%rsp), %r13d        # 4-byte Reload
	jae	.LBB0_234
# BB#233:                               #   in Loop: Header=BB0_250 Depth=2
	movl	16(%rsp), %r11d         # 4-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	jmp	.LBB0_248
.LBB0_234:                              # %min.iters.checked888
                                        #   in Loop: Header=BB0_250 Depth=2
	andq	$-8, %rax
	movl	16(%rsp), %r11d         # 4-byte Reload
	je	.LBB0_238
# BB#235:                               # %vector.memcheck
                                        #   in Loop: Header=BB0_250 Depth=2
	movq	360(%rsp), %rdx         # 8-byte Reload
	cmpq	%rdx, 328(%rsp)         # 8-byte Folded Reload
	jae	.LBB0_239
# BB#236:                               # %vector.memcheck
                                        #   in Loop: Header=BB0_250 Depth=2
	movq	432(%rsp), %rdx         # 8-byte Reload
	cmpq	352(%rsp), %rdx         # 8-byte Folded Reload
	jae	.LBB0_239
.LBB0_238:                              #   in Loop: Header=BB0_250 Depth=2
	movq	160(%rsp), %rcx         # 8-byte Reload
.LBB0_248:                              # %.lr.ph279.i.preheader1003
                                        #   in Loop: Header=BB0_250 Depth=2
	decq	%r8
	.p2align	4, 0x90
.LBB0_249:                              # %.lr.ph279.i
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_250 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	4(%rdi,%r8,4), %eax
	movl	%eax, 4(%r10,%r8,4)
	incq	%r8
	cmpq	%rbx, %r8
	jl	.LBB0_249
	jmp	.LBB0_263
.LBB0_239:                              # %vector.body884.preheader
                                        #   in Loop: Header=BB0_250 Depth=2
	testb	$3, 392(%rsp)           # 1-byte Folded Reload
	je	.LBB0_242
# BB#240:                               # %vector.body884.prol.preheader
                                        #   in Loop: Header=BB0_250 Depth=2
	xorl	%edx, %edx
	movq	152(%rsp), %rsi         # 8-byte Reload
	movq	384(%rsp), %rcx         # 8-byte Reload
	movq	376(%rsp), %rbp         # 8-byte Reload
	movq	424(%rsp), %r11         # 8-byte Reload
	movq	416(%rsp), %rdi         # 8-byte Reload
.LBB0_241:                              # %vector.body884.prol
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_250 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	-16(%rdi,%rdx,4), %xmm0
	movupd	(%rdi,%rdx,4), %xmm1
	movdqu	%xmm0, -16(%r11,%rdx,4)
	movupd	%xmm1, (%r11,%rdx,4)
	addq	$8, %rdx
	incq	%rcx
	jne	.LBB0_241
	jmp	.LBB0_243
.LBB0_242:                              #   in Loop: Header=BB0_250 Depth=2
	xorl	%edx, %edx
	movq	152(%rsp), %rsi         # 8-byte Reload
	movq	376(%rsp), %rbp         # 8-byte Reload
.LBB0_243:                              # %vector.body884.prol.loopexit
                                        #   in Loop: Header=BB0_250 Depth=2
	cmpq	$24, %rbp
	jb	.LBB0_246
# BB#244:                               # %vector.body884.preheader.new
                                        #   in Loop: Header=BB0_250 Depth=2
	subq	%rdx, %rsi
	movq	184(%rsp), %rdi         # 8-byte Reload
	addq	%rdx, %rdi
	movq	408(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,4), %rdx
	movq	400(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,4), %rdi
.LBB0_245:                              # %vector.body884
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_250 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movdqu	-16(%rdi), %xmm0
	movupd	(%rdi), %xmm1
	movdqu	%xmm0, -16(%rdx)
	movupd	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rdi
	addq	$-32, %rsi
	jne	.LBB0_245
.LBB0_246:                              # %middle.block885
                                        #   in Loop: Header=BB0_250 Depth=2
	cmpq	%rax, 168(%rsp)         # 8-byte Folded Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	16(%rsp), %r11d         # 4-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	je	.LBB0_263
# BB#247:                               #   in Loop: Header=BB0_250 Depth=2
	addq	%rax, %r8
	jmp	.LBB0_248
	.p2align	4, 0x90
.LBB0_250:                              #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_205 Depth 3
                                        #         Child Loop BB0_217 Depth 4
                                        #       Child Loop BB0_229 Depth 3
                                        #       Child Loop BB0_241 Depth 3
                                        #       Child Loop BB0_245 Depth 3
                                        #       Child Loop BB0_249 Depth 3
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	%r13d, 120(%rsp)        # 4-byte Spill
	movslq	%r13d, %rax
	cmpq	%rax, %rbx
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	cmovgeq	%rbx, %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rdi), %rax
	movl	%esi, %ecx
	subl	%edi, %ecx
	movslq	%ecx, %r8
	cmpq	%r8, %rax
	movq	%r8, %rbx
	cmovgeq	%rax, %rbx
	movl	%esi, %ecx
	subl	%edi, %ecx
	movslq	%ecx, %r13
	cmpq	%r13, %rax
	cmovlq	%r13, %rax
	movl	(%r9,%rdi,4), %esi
	movl	$2, %ecx
	cmpl	%esi, %edx
	jge	.LBB0_254
# BB#251:                               #   in Loop: Header=BB0_250 Depth=2
	cmpl	%esi, %r11d
	jle	.LBB0_253
# BB#252:                               #   in Loop: Header=BB0_250 Depth=2
	movl	options+28(%rip), %ecx
	jmp	.LBB0_254
.LBB0_253:                              #   in Loop: Header=BB0_250 Depth=2
	cvtsi2sdl	%esi, %xmm0
	mulsd	.LCPI0_0(%rip), %xmm0
	addsd	.LCPI0_1(%rip), %xmm0
	cvttsd2si	%xmm0, %ecx
.LBB0_254:                              # %good_ratio.exit.i425
                                        #   in Loop: Header=BB0_250 Depth=2
	movslq	%ecx, %rcx
	cmpq	%rcx, %rdi
	jle	.LBB0_261
# BB#255:                               #   in Loop: Header=BB0_250 Depth=2
	testq	%rdi, %rdi
	jle	.LBB0_265
# BB#256:                               #   in Loop: Header=BB0_250 Depth=2
	movq	%rbx, 168(%rsp)         # 8-byte Spill
	movq	%rdi, %rcx
	leaq	-1(%rcx), %rdi
	movq	%rcx, %rbx
	movl	-4(%r9,%rcx,4), %esi
	movl	$2, %ecx
	cmpl	%esi, %edx
	jge	.LBB0_260
# BB#257:                               #   in Loop: Header=BB0_250 Depth=2
	cmpl	%esi, %r11d
	jle	.LBB0_259
# BB#258:                               #   in Loop: Header=BB0_250 Depth=2
	movl	options+28(%rip), %ecx
	jmp	.LBB0_260
.LBB0_259:                              #   in Loop: Header=BB0_250 Depth=2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm0
	mulsd	.LCPI0_0(%rip), %xmm0
	addsd	.LCPI0_1(%rip), %xmm0
	cvttsd2si	%xmm0, %ecx
.LBB0_260:                              # %good_ratio.exit258.i
                                        #   in Loop: Header=BB0_250 Depth=2
	movslq	%ecx, %rcx
	cmpq	%rcx, %rdi
	movq	%rbx, %rdi
	movq	168(%rsp), %rbx         # 8-byte Reload
	jg	.LBB0_265
.LBB0_261:                              # %.critedge3.preheader.i
                                        #   in Loop: Header=BB0_250 Depth=2
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	movq	176(%rsp), %rdx         # 8-byte Reload
	decq	%rdx
	cmpq	56(%rsp), %rdx          # 8-byte Folded Reload
	movq	%rdx, 176(%rsp)         # 8-byte Spill
	jle	.LBB0_204
# BB#262:                               # %.critedge3._crit_edge.thread.i
                                        #   in Loop: Header=BB0_250 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	(%r10,%rax,4), %eax
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	%eax, (%r9,%rsi,4)
	movq	264(%rsp), %rax         # 8-byte Reload
	movl	%r15d, (%rax,%rsi,4)
	movl	120(%rsp), %r13d        # 4-byte Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
.LBB0_263:                              # %._crit_edge.i442
                                        #   in Loop: Header=BB0_250 Depth=2
	incq	%rbx
	leaq	1(%rsi), %rax
	decl	224(%rsp)               # 4-byte Folded Spill
	movq	112(%rsp), %rdi         # 8-byte Reload
	incq	%rdi
	decl	%r13d
	cmpq	24(%rsp), %rsi          # 8-byte Folded Reload
	movl	192(%rsp), %edx         # 4-byte Reload
	movq	344(%rsp), %rsi         # 8-byte Reload
	jle	.LBB0_250
# BB#264:                               #   in Loop: Header=BB0_4 Depth=1
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	movq	%rax, 48(%rsp)          # 8-byte Spill
.LBB0_265:                              # %.critedge2.preheader.i
                                        #   in Loop: Header=BB0_4 Depth=1
	movslq	48(%rsp), %rcx          # 4-byte Folded Reload
	movq	%rcx, %rdx
	shlq	$32, %rdx
	movabsq	$-4294967296, %rdi      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB0_266:                              # %.critedge2.i
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %r13
	movq	%rdx, %rax
	testq	%r13, %r13
	jle	.LBB0_268
# BB#267:                               #   in Loop: Header=BB0_266 Depth=2
	movl	(%r9,%r13,4), %esi
	leaq	-1(%r13), %rcx
	subl	-4(%r9,%r13,4), %esi
	leaq	(%rax,%rdi), %rdx
	cmpl	$3, %esi
	jl	.LBB0_266
.LBB0_268:                              # %.critedge7.i
                                        #   in Loop: Header=BB0_4 Depth=1
	sarq	$30, %rax
	movl	(%r9,%rax), %ebx
	movq	232(%rsp), %rcx         # 8-byte Reload
	subl	%r15d, %ecx
	addl	%ebx, %ecx
	movq	264(%rsp), %rbp         # 8-byte Reload
	addl	(%rbp,%rax), %ecx
	movq	%r9, %rdi
	movq	%rcx, %r15
	callq	free
	movq	%rbp, %rdi
	callq	free
	movq	208(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%r15, %r8
	movl	%ebx, %r15d
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	104(%rsp), %r11         # 8-byte Reload
	movq	256(%rsp), %r9          # 8-byte Reload
	jmp	.LBB0_274
.LBB0_269:                              #   in Loop: Header=BB0_4 Depth=1
	movq	%r10, %rdi
	callq	free
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	216(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	264(%rsp), %rdi         # 8-byte Reload
	callq	free
	addl	232(%rsp), %ebx         # 4-byte Folded Reload
	jmp	.LBB0_272
.LBB0_270:                              #   in Loop: Header=BB0_4 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%r10, %rbx
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	216(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	264(%rsp), %rdi         # 8-byte Reload
	callq	free
	addl	232(%rsp), %r12d        # 4-byte Folded Reload
	movl	%r12d, %r8d
	jmp	.LBB0_273
.LBB0_271:                              #   in Loop: Header=BB0_4 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%r10, %rbp
	callq	free
	movq	%rbp, %rdi
	callq	free
	movq	216(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	264(%rsp), %rdi         # 8-byte Reload
	callq	free
	addl	232(%rsp), %ebx         # 4-byte Folded Reload
	movl	%r14d, %r15d
.LBB0_272:                              # %extend_fw.exit
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	%ebx, %r8d
.LBB0_273:                              # %extend_fw.exit
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	128(%rsp), %r12         # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	104(%rsp), %r11         # 8-byte Reload
	movq	256(%rsp), %r9          # 8-byte Reload
	movq	48(%rsp), %r13          # 8-byte Reload
.LBB0_274:                              # %extend_fw.exit
                                        #   in Loop: Header=BB0_4 Depth=1
	addl	%r15d, %r9d
	movl	%r9d, %eax
	subl	12(%r11), %eax
	imull	options+56(%rip), %eax
	imull	options+48(%rip), %r13d
	addl	%eax, %r13d
	js	.LBB0_276
# BB#275:                               #   in Loop: Header=BB0_4 Depth=1
	movl	%r9d, 12(%r11)
	movl	%r8d, 8(%r11)
	.p2align	4, 0x90
.LBB0_276:                              # %.thread542
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	16(%r14), %r11d
	cmpl	$2, %r11d
	movq	144(%rsp), %r13         # 8-byte Reload
	jb	.LBB0_440
# BB#277:                               # %.lr.ph654
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	$1, %esi
	.p2align	4, 0x90
.LBB0_278:                              #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_284 Depth 3
                                        #       Child Loop BB0_295 Depth 3
                                        #       Child Loop BB0_299 Depth 3
                                        #       Child Loop BB0_309 Depth 3
                                        #       Child Loop BB0_312 Depth 3
                                        #       Child Loop BB0_314 Depth 3
                                        #         Child Loop BB0_317 Depth 4
                                        #           Child Loop BB0_326 Depth 5
                                        #         Child Loop BB0_331 Depth 4
                                        #         Child Loop BB0_338 Depth 4
                                        #         Child Loop BB0_342 Depth 4
                                        #         Child Loop BB0_350 Depth 4
                                        #           Child Loop BB0_362 Depth 5
                                        #         Child Loop BB0_367 Depth 4
                                        #         Child Loop BB0_373 Depth 4
                                        #         Child Loop BB0_377 Depth 4
                                        #       Child Loop BB0_428 Depth 3
                                        #       Child Loop BB0_433 Depth 3
                                        #       Child Loop BB0_410 Depth 3
                                        #       Child Loop BB0_415 Depth 3
                                        #       Child Loop BB0_420 Depth 3
	movq	%rbp, %rax
	movq	(%rbp), %rcx
	leal	-1(%rsi), %edi
	movq	(%rcx,%rdi,8), %rbp
	movl	%esi, %edx
	movq	(%rcx,%rdx,8), %r8
	movl	4(%r8), %ebx
	movl	12(%rbp), %r9d
	movl	%ebx, %r15d
	subl	%r9d, %r15d
	leal	-1(%r15), %ecx
	movl	%ecx, 112(%rsp)         # 4-byte Spill
	testl	%ecx, %ecx
	jle	.LBB0_289
# BB#279:                               #   in Loop: Header=BB0_278 Depth=2
	movl	(%r8), %edx
	leal	-1(%rdx), %r10d
	movl	8(%rbp), %ecx
	cmpl	%ecx, %r10d
	jbe	.LBB0_289
# BB#280:                               #   in Loop: Header=BB0_278 Depth=2
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	movq	%rdi, 280(%rsp)         # 8-byte Spill
	movq	%rsi, 264(%rsp)         # 8-byte Spill
	movl	112(%rsp), %esi         # 4-byte Reload
	cmpl	$500, %esi              # imm = 0x1F4
	jg	.LBB0_409
# BB#281:                               #   in Loop: Header=BB0_278 Depth=2
	subl	192(%rsp), %edx         # 4-byte Folded Reload
	movq	%rdx, %rax
	leal	-1(%rax), %edx
	xorl	%r12d, %r12d
	cmpl	$999999, %edx           # imm = 0xF423F
	ja	.LBB0_407
# BB#282:                               #   in Loop: Header=BB0_278 Depth=2
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	36(%r13), %eax
	movl	%esi, %r14d
	cvtsi2sdq	%r14, %xmm0
	movapd	%xmm0, %xmm1
	mulsd	.LCPI0_2(%rip), %xmm1
	addsd	.LCPI0_1(%rip), %xmm1
	cvttsd2si	%xmm1, %rcx
	cmpl	%eax, %ecx
	cmovbel	%eax, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%edx, %r11d
	subl	%esi, %r11d
	movq	128(%rsp), %rcx         # 8-byte Reload
	js	.LBB0_290
# BB#283:                               #   in Loop: Header=BB0_278 Depth=2
	movl	%ebx, 328(%rsp)         # 4-byte Spill
	movq	16(%rcx), %rdi
	leaq	(%rdi,%r9), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movq	8(%r13), %rbx
	movq	192(%rsp), %rax         # 8-byte Reload
	leaq	(%rbx,%rax), %rcx
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	movslq	%esi, %rcx
	movq	%rdx, %rsi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movslq	%edx, %rdx
	leaq	(%rdx,%rax), %rax
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	leaq	-1(%rbx,%rax), %rsi
	leaq	(%rcx,%r9), %rax
	movq	%rdi, 152(%rsp)         # 8-byte Spill
	leaq	-1(%rdi,%rax), %rdi
	xorl	%eax, %eax
	movq	%r9, 216(%rsp)          # 8-byte Spill
	movq	%rbp, 232(%rsp)         # 8-byte Spill
	movq	%r8, 240(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_284:                              #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_278 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	(%rdx,%rax), %rbx
	testq	%rbx, %rbx
	jle	.LBB0_292
# BB#285:                               #   in Loop: Header=BB0_284 Depth=3
	leaq	(%rcx,%rax), %rbx
	testq	%rbx, %rbx
	jle	.LBB0_292
# BB#286:                               #   in Loop: Header=BB0_284 Depth=3
	movzbl	(%rdi,%rax), %ebx
	cmpb	(%rsi,%rax), %bl
	leaq	-1(%rax), %rax
	je	.LBB0_284
# BB#287:                               # %.critedge.thread.loopexit.i
                                        #   in Loop: Header=BB0_278 Depth=2
	movl	%r11d, 352(%rsp)        # 4-byte Spill
	movl	%r10d, 360(%rsp)        # 4-byte Spill
	addl	%r15d, %eax
	movl	%eax, %r14d
	movq	88(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB0_294
	.p2align	4, 0x90
.LBB0_289:                              #   in Loop: Header=BB0_278 Depth=2
	movq	%rax, %rbp
	jmp	.LBB0_439
.LBB0_290:                              #   in Loop: Header=BB0_278 Depth=2
	movl	%eax, %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	movl	%edx, %eax
	cvtsi2sdq	%rax, %xmm3
	movapd	%xmm3, %xmm2
	mulsd	.LCPI0_3(%rip), %xmm2
	minsd	%xmm1, %xmm2
	ucomisd	%xmm0, %xmm2
	jae	.LBB0_302
# BB#291:                               #   in Loop: Header=BB0_278 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movl	%eax, %r12d
	jmp	.LBB0_407
.LBB0_292:                              # %.critedge.i449
                                        #   in Loop: Header=BB0_278 Depth=2
	addq	%rax, %r14
	testl	%r14d, %r14d
	movq	88(%rsp), %rcx          # 8-byte Reload
	je	.LBB0_385
# BB#293:                               # %.critedge.i449..critedge.thread.i_crit_edge
                                        #   in Loop: Header=BB0_278 Depth=2
	movl	%r11d, 352(%rsp)        # 4-byte Spill
	movl	%r10d, 360(%rsp)        # 4-byte Spill
.LBB0_294:                              # %.critedge.thread.i
                                        #   in Loop: Header=BB0_278 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	(%rbp,%rax), %ebx
	leal	(%rbp,%rcx), %r12d
	shlq	$2, %r12
	movq	%r12, %rdi
	callq	xmalloc
	movq	%rax, %r13
	movq	%r12, %rdi
	callq	xmalloc
	xorl	%edx, %edx
	movq	192(%rsp), %rsi         # 8-byte Reload
	movq	%rsi, %r8
	.p2align	4, 0x90
.LBB0_295:                              #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_278 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%edx, %ecx
	movl	%r15d, (%r13,%rcx,4)
	movl	%r15d, (%rax,%rcx,4)
	incl	%edx
	cmpl	%ebx, %edx
	jbe	.LBB0_295
# BB#296:                               #   in Loop: Header=BB0_278 Depth=2
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movslq	%ebp, %r9
	movl	%r14d, (%r13,%r9,4)
	xorl	%ebp, %ebp
	movl	112(%rsp), %ecx         # 4-byte Reload
	testl	%ecx, %ecx
	je	.LBB0_304
# BB#297:                               #   in Loop: Header=BB0_278 Depth=2
	movq	24(%rsp), %rdi          # 8-byte Reload
	testl	%edi, %edi
	movq	216(%rsp), %rdx         # 8-byte Reload
	je	.LBB0_305
# BB#298:                               # %.lr.ph598.i.preheader
                                        #   in Loop: Header=BB0_278 Depth=2
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_299:                              # %.lr.ph598.i
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_278 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	176(%rsp), %rax         # 8-byte Reload
	movzbl	(%rax,%rbp), %eax
	movq	208(%rsp), %rsi         # 8-byte Reload
	cmpb	(%rsi,%rbp), %al
	jne	.LBB0_305
# BB#300:                               #   in Loop: Header=BB0_299 Depth=3
	incq	%rbp
	cmpl	%edi, %ebp
	jae	.LBB0_305
# BB#301:                               #   in Loop: Header=BB0_299 Depth=3
	cmpl	%ecx, %ebp
	jb	.LBB0_299
	jmp	.LBB0_305
.LBB0_302:                              #   in Loop: Header=BB0_278 Depth=2
	movsd	%xmm3, 24(%rsp)         # 8-byte Spill
	movq	%r13, %r15
	movq	%r8, 240(%rsp)          # 8-byte Spill
	movq	192(%rsp), %r14         # 8-byte Reload
	incl	%r14d
	incl	%r9d
	decl	%ebx
	movl	$32, %edi
	movq	%r9, %r12
	movl	%r10d, %r13d
	callq	xmalloc
	movl	%ebx, %ecx
	movq	%rax, %rbx
	movl	%r14d, (%rbx)
	movl	%r12d, 4(%rbx)
	movl	%r13d, 8(%rbx)
	movl	%ecx, 12(%rbx)
	movl	72(%rsp), %ecx
	movl	76(%rsp), %esi
	cmpl	%ecx, %esi
	jbe	.LBB0_393
# BB#303:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB0_278 Depth=2
	movq	64(%rsp), %rax
	jmp	.LBB0_394
.LBB0_304:                              #   in Loop: Header=BB0_278 Depth=2
	movq	216(%rsp), %rdx         # 8-byte Reload
.LBB0_305:                              # %.critedge2.i450
                                        #   in Loop: Header=BB0_278 Depth=2
	cmpl	%ecx, %ebp
	jne	.LBB0_308
# BB#306:                               #   in Loop: Header=BB0_278 Depth=2
	leal	1(%r8), %r14d
	incl	%edx
	addl	%ecx, %r8d
	movl	328(%rsp), %ebx         # 4-byte Reload
	decl	%ebx
	movl	$32, %edi
	movq	%r8, %r12
	movq	%rdx, %r15
	callq	xmalloc
	movq	%rax, %rbp
	movl	%r14d, (%rbp)
	movl	%r15d, 4(%rbp)
	movl	%r12d, 8(%rbp)
	movl	%ebx, 12(%rbp)
	movl	72(%rsp), %ecx
	movl	76(%rsp), %esi
	cmpl	%ecx, %esi
	jbe	.LBB0_383
# BB#307:                               # %._crit_edge.i523.i
                                        #   in Loop: Header=BB0_278 Depth=2
	movq	64(%rsp), %rax
	jmp	.LBB0_384
.LBB0_308:                              #   in Loop: Header=BB0_278 Depth=2
	movq	%r9, 200(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %ecx
	movl	%ecx, 168(%rsp)         # 4-byte Spill
	leal	1(%rax), %eax
	movl	%eax, 120(%rsp)         # 4-byte Spill
	movq	%r12, %rdi
	callq	xmalloc
	movq	%r12, %rdi
	movq	%rax, %r14
	callq	xmalloc
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_309:                              #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_278 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%edx, %ecx
	movl	$-1, (%r14,%rcx,4)
	movl	$-1, (%rax,%rcx,4)
	incl	%edx
	cmpl	%ebx, %edx
	jbe	.LBB0_309
# BB#310:                               #   in Loop: Header=BB0_278 Depth=2
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	200(%rsp), %rax         # 8-byte Reload
	movl	%ebp, (%r14,%rax,4)
	movl	120(%rsp), %ebx         # 4-byte Reload
	shlq	$2, %rbx
	movq	%rbx, %rdi
	callq	xmalloc
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	xmalloc
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movq	%rbx, %rdi
	callq	xmalloc
	movq	%rax, %r12
	movq	%rbx, %rdi
	callq	xmalloc
	movq	272(%rsp), %rbx         # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	testl	%r11d, %r11d
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	%r14, 56(%rsp)          # 8-byte Spill
	movq	%rbp, 256(%rsp)         # 8-byte Spill
	movq	%r12, 160(%rsp)         # 8-byte Spill
	movq	%rax, 136(%rsp)         # 8-byte Spill
	je	.LBB0_387
# BB#311:                               # %.lr.ph592.i.preheader
                                        #   in Loop: Header=BB0_278 Depth=2
	movl	$1, %edi
	movl	112(%rsp), %esi         # 4-byte Reload
	.p2align	4, 0x90
.LBB0_312:                              # %.lr.ph592.i
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_278 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%edi, %ecx
	movl	%r15d, (%rbx,%rcx,4)
	movl	$-1, (%rbp,%rcx,4)
	incl	%edi
	cmpl	%r11d, %edi
	jbe	.LBB0_312
# BB#313:                               # %.preheader540.lr.ph.i
                                        #   in Loop: Header=BB0_278 Depth=2
	movq	200(%rsp), %rdx         # 8-byte Reload
	movl	(%r13,%rdx,4), %ecx
	movl	%ecx, (%rbx)
	movl	%r11d, (%rax)
	movl	(%r14,%rdx,4), %eax
	movl	%eax, (%rbp)
	movl	%r11d, (%r12)
	movl	$1, %edx
	movl	$1, %eax
	movq	%r15, 40(%rsp)          # 8-byte Spill
	subl	%r15d, %eax
	movq	88(%rsp), %rcx          # 8-byte Reload
	leal	-2(%rax,%rcx), %ecx
	movslq	168(%rsp), %rbx         # 4-byte Folded Reload
	movslq	120(%rsp), %r15         # 4-byte Folded Reload
	movq	192(%rsp), %rax         # 8-byte Reload
	movq	104(%rsp), %rdi         # 8-byte Reload
	leaq	-1(%rdi,%rax), %r14
	movq	216(%rsp), %rax         # 8-byte Reload
	movq	152(%rsp), %rdi         # 8-byte Reload
	leaq	-1(%rdi,%rax), %rdi
	movq	%rbx, %rax
	leaq	-1(%rax), %rbx
	movq	%rbx, 168(%rsp)         # 8-byte Spill
	movl	$-1, 344(%rsp)          # 4-byte Folded Spill
	movl	$0, 336(%rsp)           # 4-byte Folded Spill
	movl	$-1, 120(%rsp)          # 4-byte Folded Spill
	movl	$-1, 184(%rsp)          # 4-byte Folded Spill
	movl	%r11d, %r9d
	movq	96(%rsp), %r10          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_314:                              # %.preheader540.i
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_278 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_317 Depth 4
                                        #           Child Loop BB0_326 Depth 5
                                        #         Child Loop BB0_331 Depth 4
                                        #         Child Loop BB0_338 Depth 4
                                        #         Child Loop BB0_342 Depth 4
                                        #         Child Loop BB0_350 Depth 4
                                        #           Child Loop BB0_362 Depth 5
                                        #         Child Loop BB0_367 Depth 4
                                        #         Child Loop BB0_373 Depth 4
                                        #         Child Loop BB0_377 Depth 4
	cmpq	%r15, %rax
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	%ecx, 224(%rsp)         # 4-byte Spill
	jle	.LBB0_316
# BB#315:                               # %._crit_edge.thread.i
                                        #   in Loop: Header=BB0_314 Depth=3
	movq	248(%rsp), %rax         # 8-byte Reload
	movq	200(%rsp), %rcx         # 8-byte Reload
	movl	(%rax,%rcx,4), %ecx
	movl	%edx, %r8d
	movq	272(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%r8,4), %rax
	movl	%ecx, (%rbx,%r8,4)
	movq	136(%rsp), %rcx         # 8-byte Reload
	movl	%r11d, (%rcx,%r8,4)
	jmp	.LBB0_335
	.p2align	4, 0x90
.LBB0_316:                              # %.lr.ph.i451
                                        #   in Loop: Header=BB0_314 Depth=3
	movl	%r11d, %r10d
	subl	%edx, %r10d
	leal	(%rdx,%r11), %r8d
	movq	%rax, %rbp
	movl	%ecx, %esi
	movq	248(%rsp), %r12         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_317:                              #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_278 Depth=2
                                        #       Parent Loop BB0_314 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_326 Depth 5
	cmpl	%r10d, %ebp
	jne	.LBB0_319
# BB#318:                               #   in Loop: Header=BB0_317 Depth=4
	movl	4(%r13,%rbp,4), %edx
	jmp	.LBB0_325
	.p2align	4, 0x90
.LBB0_319:                              #   in Loop: Header=BB0_317 Depth=4
	cmpl	%r8d, %ebp
	jne	.LBB0_321
# BB#320:                               #   in Loop: Header=BB0_317 Depth=4
	movl	-4(%r13,%rbp,4), %edx
	decl	%edx
	jmp	.LBB0_325
	.p2align	4, 0x90
.LBB0_321:                              #   in Loop: Header=BB0_317 Depth=4
	movl	(%r13,%rbp,4), %r11d
	movl	4(%r13,%rbp,4), %ecx
	cmpl	%ecx, %r11d
	jle	.LBB0_323
# BB#322:                               # %._crit_edge659.i
                                        #   in Loop: Header=BB0_317 Depth=4
	movl	-4(%r13,%rbp,4), %eax
	jmp	.LBB0_324
.LBB0_323:                              #   in Loop: Header=BB0_317 Depth=4
	leal	-1(%r11), %edx
	movl	-4(%r13,%rbp,4), %eax
	cmpl	%eax, %edx
	jle	.LBB0_325
.LBB0_324:                              #   in Loop: Header=BB0_317 Depth=4
	leal	-1(%rax), %edx
	cmpl	%r11d, %eax
	cmovgel	%ecx, %edx
	cmpl	%ecx, %eax
	cmovgel	%ecx, %edx
	.p2align	4, 0x90
.LBB0_325:                              #   in Loop: Header=BB0_317 Depth=4
	movslq	%edx, %rdx
	leal	(%rdx,%rsi), %eax
	cltq
	.p2align	4, 0x90
.LBB0_326:                              #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_278 Depth=2
                                        #       Parent Loop BB0_314 Depth=3
                                        #         Parent Loop BB0_317 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	%rdx, %rcx
	testq	%rax, %rax
	jle	.LBB0_329
# BB#327:                               #   in Loop: Header=BB0_326 Depth=5
	testq	%rcx, %rcx
	jle	.LBB0_329
# BB#328:                               #   in Loop: Header=BB0_326 Depth=5
	leaq	-1(%rcx), %rdx
	movzbl	(%rdi,%rcx), %ebx
	cmpb	(%r14,%rax), %bl
	leaq	-1(%rax), %rax
	je	.LBB0_326
.LBB0_329:                              # %.critedge511.i
                                        #   in Loop: Header=BB0_317 Depth=4
	movl	%ecx, (%r12,%rbp,4)
	incl	%esi
	cmpq	%r15, %rbp
	leaq	1(%rbp), %rbp
	jl	.LBB0_317
# BB#330:                               # %._crit_edge.i455
                                        #   in Loop: Header=BB0_314 Depth=3
	movq	200(%rsp), %rax         # 8-byte Reload
	movl	(%r12,%rax,4), %ecx
	movl	152(%rsp), %r8d         # 4-byte Reload
	movq	272(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r8,4), %rax
	movl	%ecx, (%rdx,%r8,4)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	136(%rsp), %rdx         # 8-byte Reload
	movl	%ecx, (%rdx,%r8,4)
	movq	168(%rsp), %rcx         # 8-byte Reload
	movq	104(%rsp), %rdx         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	96(%rsp), %r10          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_331:                              # %.lr.ph546.i
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_278 Depth=2
                                        #       Parent Loop BB0_314 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	4(%r12,%rcx,4), %esi
	movl	%esi, 4(%r13,%rcx,4)
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	%esi, 4(%r12,%rcx,4)
	movl	4(%r13,%rcx,4), %ebp
	cmpl	(%rax), %ebp
	jge	.LBB0_333
# BB#332:                               #   in Loop: Header=BB0_331 Depth=4
	movl	%ebp, (%rax)
	movq	136(%rsp), %rsi         # 8-byte Reload
	movl	%edx, (%rsi,%r8,4)
.LBB0_333:                              #   in Loop: Header=BB0_331 Depth=4
	incl	%edx
	incq	%rcx
	cmpq	%r15, %rcx
	jl	.LBB0_331
# BB#334:                               #   in Loop: Header=BB0_314 Depth=3
	movl	112(%rsp), %esi         # 4-byte Reload
	movq	256(%rsp), %rbp         # 8-byte Reload
	movq	152(%rsp), %rdx         # 8-byte Reload
.LBB0_335:                              # %.preheader539.i
                                        #   in Loop: Header=BB0_314 Depth=3
	testl	%edx, %edx
	movq	160(%rsp), %rbx         # 8-byte Reload
	je	.LBB0_345
# BB#336:                               # %.lr.ph548.i
                                        #   in Loop: Header=BB0_314 Depth=3
	movl	(%rax), %eax
	cmpl	$0, 120(%rsp)           # 4-byte Folded Reload
	js	.LBB0_341
# BB#337:                               # %.lr.ph548.split.i.preheader
                                        #   in Loop: Header=BB0_314 Depth=3
	movq	152(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, %r11d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_338:                              # %.lr.ph548.split.i
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_278 Depth=2
                                        #       Parent Loop BB0_314 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpl	(%rbp,%rcx,4), %eax
	jg	.LBB0_340
# BB#339:                               #   in Loop: Header=BB0_338 Depth=4
	cmpl	%r11d, %r9d
	ja	.LBB0_346
.LBB0_340:                              #   in Loop: Header=BB0_338 Depth=4
	incq	%rcx
	incl	%r11d
	cmpq	%r8, %rcx
	jb	.LBB0_338
	jmp	.LBB0_345
	.p2align	4, 0x90
.LBB0_341:                              # %.lr.ph548.split.us.i.preheader
                                        #   in Loop: Header=BB0_314 Depth=3
	movq	152(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, %r11d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_342:                              # %.lr.ph548.split.us.i
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_278 Depth=2
                                        #       Parent Loop BB0_314 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpl	(%rbp,%rcx,4), %eax
	jg	.LBB0_344
# BB#343:                               #   in Loop: Header=BB0_342 Depth=4
	cmpl	%r11d, %r9d
	jae	.LBB0_346
.LBB0_344:                              #   in Loop: Header=BB0_342 Depth=4
	incq	%rcx
	incl	%r11d
	cmpq	%r8, %rcx
	jb	.LBB0_342
	.p2align	4, 0x90
.LBB0_345:                              #   in Loop: Header=BB0_314 Depth=3
	movl	%r9d, %r11d
	jmp	.LBB0_347
	.p2align	4, 0x90
.LBB0_346:                              # %.._crit_edge549.i.loopexit997_crit_edge
                                        #   in Loop: Header=BB0_314 Depth=3
	movq	152(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 184(%rsp)         # 4-byte Spill
	movl	%ecx, %eax
	movl	%eax, 120(%rsp)         # 4-byte Spill
.LBB0_347:                              # %._crit_edge549.i
                                        #   in Loop: Header=BB0_314 Depth=3
	movq	104(%rsp), %rax         # 8-byte Reload
	cmpq	%r15, %rax
	movq	%rax, %rdx
	jle	.LBB0_349
# BB#348:                               # %._crit_edge570.thread.i
                                        #   in Loop: Header=BB0_314 Depth=3
	movq	200(%rsp), %rax         # 8-byte Reload
	movl	(%r10,%rax,4), %ecx
	leaq	(%rbp,%r8,4), %rax
	movl	%ecx, (%rbp,%r8,4)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, (%rbx,%r8,4)
	jmp	.LBB0_371
	.p2align	4, 0x90
.LBB0_349:                              # %.lr.ph569.i
                                        #   in Loop: Header=BB0_314 Depth=3
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %r12d
	movq	152(%rsp), %rax         # 8-byte Reload
	subl	%eax, %r12d
	leal	(%rax,%rcx), %eax
	movl	%eax, 88(%rsp)          # 4-byte Spill
	movl	336(%rsp), %eax         # 4-byte Reload
	movl	%eax, %ecx
	movq	%rdx, %rbp
	movl	344(%rsp), %eax         # 4-byte Reload
	movl	%eax, %ebx
	.p2align	4, 0x90
.LBB0_350:                              #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_278 Depth=2
                                        #       Parent Loop BB0_314 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_362 Depth 5
	cmpl	%r12d, %ebp
	jne	.LBB0_352
# BB#351:                               #   in Loop: Header=BB0_350 Depth=4
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	4(%rax,%rbp,4), %eax
	incl	%eax
	testl	%eax, %eax
	jns	.LBB0_359
	jmp	.LBB0_365
	.p2align	4, 0x90
.LBB0_352:                              #   in Loop: Header=BB0_350 Depth=4
	cmpl	88(%rsp), %ebp          # 4-byte Folded Reload
	jne	.LBB0_354
# BB#353:                               #   in Loop: Header=BB0_350 Depth=4
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	-4(%rax,%rbp,4), %eax
	testl	%eax, %eax
	jns	.LBB0_359
	jmp	.LBB0_365
	.p2align	4, 0x90
.LBB0_354:                              #   in Loop: Header=BB0_350 Depth=4
	movq	56(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx,%rbp,4), %r9d
	movl	4(%rdx,%rbp,4), %r10d
	cmpl	%r10d, %r9d
	jge	.LBB0_356
# BB#355:                               # %._crit_edge660.i
                                        #   in Loop: Header=BB0_350 Depth=4
	movl	-4(%rdx,%rbp,4), %edx
	jmp	.LBB0_357
.LBB0_356:                              #   in Loop: Header=BB0_350 Depth=4
	leal	1(%r9), %eax
	movl	-4(%rdx,%rbp,4), %edx
	cmpl	%edx, %eax
	jge	.LBB0_358
.LBB0_357:                              #   in Loop: Header=BB0_350 Depth=4
	leal	1(%r10), %eax
	cmpl	%edx, %eax
	cmovll	%edx, %eax
	cmpl	%r9d, %r10d
	cmovll	%edx, %eax
	.p2align	4, 0x90
.LBB0_358:                              #   in Loop: Header=BB0_350 Depth=4
	testl	%eax, %eax
	js	.LBB0_365
.LBB0_359:                              # %.preheader.i457
                                        #   in Loop: Header=BB0_350 Depth=4
	cmpl	%esi, %eax
	jae	.LBB0_365
# BB#360:                               # %.preheader.i457
                                        #   in Loop: Header=BB0_350 Depth=4
	movl	%ebp, %edx
	subl	16(%rsp), %edx          # 4-byte Folded Reload
	addl	%eax, %edx
	cmpl	24(%rsp), %edx          # 4-byte Folded Reload
	jae	.LBB0_365
# BB#361:                               # %.lr.ph563.preheader.i
                                        #   in Loop: Header=BB0_350 Depth=4
	leal	(%rax,%rbx), %edx
	movslq	%eax, %r10
	addq	176(%rsp), %r10         # 8-byte Folded Reload
	leal	(%rax,%rcx), %r13d
	movslq	%edx, %r9
	addq	208(%rsp), %r9          # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB0_362:                              # %.lr.ph563.i
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_278 Depth=2
                                        #       Parent Loop BB0_314 Depth=3
                                        #         Parent Loop BB0_350 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzbl	(%r10), %edx
	cmpb	(%r9), %dl
	jne	.LBB0_365
# BB#363:                               #   in Loop: Header=BB0_362 Depth=5
	incl	%eax
	cmpl	%esi, %eax
	jae	.LBB0_365
# BB#364:                               #   in Loop: Header=BB0_362 Depth=5
	incq	%r10
	incq	%r9
	cmpl	24(%rsp), %r13d         # 4-byte Folded Reload
	leal	1(%r13), %edx
	movl	%edx, %r13d
	jb	.LBB0_362
	.p2align	4, 0x90
.LBB0_365:                              # %.critedge8.i
                                        #   in Loop: Header=BB0_350 Depth=4
	movq	96(%rsp), %r10          # 8-byte Reload
	movl	%eax, (%r10,%rbp,4)
	incl	%ebx
	incl	%ecx
	cmpq	%r15, %rbp
	leaq	1(%rbp), %rbp
	movq	48(%rsp), %r13          # 8-byte Reload
	jl	.LBB0_350
# BB#366:                               # %._crit_edge570.i
                                        #   in Loop: Header=BB0_314 Depth=3
	movq	200(%rsp), %rax         # 8-byte Reload
	movl	(%r10,%rax,4), %ecx
	movq	256(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r8,4), %rax
	movl	%ecx, (%rdx,%r8,4)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	160(%rsp), %r9          # 8-byte Reload
	movl	%ecx, (%r9,%r8,4)
	movq	168(%rsp), %rcx         # 8-byte Reload
	movq	104(%rsp), %rdx         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	56(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_367:                              # %.lr.ph573.i
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_278 Depth=2
                                        #       Parent Loop BB0_314 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	4(%r10,%rcx,4), %esi
	movl	%esi, 4(%rbx,%rcx,4)
	movl	$-1, 4(%r10,%rcx,4)
	movl	4(%rbx,%rcx,4), %ebp
	cmpl	(%rax), %ebp
	jle	.LBB0_369
# BB#368:                               #   in Loop: Header=BB0_367 Depth=4
	movl	%ebp, (%rax)
	movl	%edx, (%r9,%r8,4)
.LBB0_369:                              #   in Loop: Header=BB0_367 Depth=4
	incl	%edx
	incq	%rcx
	cmpq	%r15, %rcx
	jl	.LBB0_367
# BB#370:                               #   in Loop: Header=BB0_314 Depth=3
	movl	112(%rsp), %esi         # 4-byte Reload
	movq	256(%rsp), %rbp         # 8-byte Reload
.LBB0_371:                              # %.preheader538.i
                                        #   in Loop: Header=BB0_314 Depth=3
	movl	(%rax), %eax
	cmpl	$0, 120(%rsp)           # 4-byte Folded Reload
	movq	272(%rsp), %rbx         # 8-byte Reload
	movq	152(%rsp), %rdx         # 8-byte Reload
	js	.LBB0_376
# BB#372:                               # %.preheader538.split.i.preheader
                                        #   in Loop: Header=BB0_314 Depth=3
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_373:                              # %.preheader538.split.i
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_278 Depth=2
                                        #       Parent Loop BB0_314 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%r12d, %ecx
	cmpl	%eax, (%rbx,%rcx,4)
	jg	.LBB0_375
# BB#374:                               #   in Loop: Header=BB0_373 Depth=4
	leal	(%rdx,%r12), %r9d
	cmpl	%r9d, %r11d
	ja	.LBB0_381
.LBB0_375:                              #   in Loop: Header=BB0_373 Depth=4
	incl	%r12d
	cmpl	%edx, %r12d
	jbe	.LBB0_373
	jmp	.LBB0_380
	.p2align	4, 0x90
.LBB0_376:                              # %.preheader538.split.us.i.preheader
                                        #   in Loop: Header=BB0_314 Depth=3
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_377:                              # %.preheader538.split.us.i
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_278 Depth=2
                                        #       Parent Loop BB0_314 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%r12d, %ecx
	cmpl	%eax, (%rbx,%rcx,4)
	jg	.LBB0_379
# BB#378:                               #   in Loop: Header=BB0_377 Depth=4
	leal	(%rdx,%r12), %r9d
	cmpl	%r9d, %r11d
	jae	.LBB0_381
.LBB0_379:                              #   in Loop: Header=BB0_377 Depth=4
	incl	%r12d
	cmpl	%edx, %r12d
	jbe	.LBB0_377
.LBB0_380:                              #   in Loop: Header=BB0_314 Depth=3
	movq	%rbp, %r8
	movl	%r11d, %r9d
	movl	184(%rsp), %eax         # 4-byte Reload
	movl	%eax, %r12d
	jmp	.LBB0_382
	.p2align	4, 0x90
.LBB0_381:                              # %..us-lcssa.us.i.loopexit993_crit_edge
                                        #   in Loop: Header=BB0_314 Depth=3
	movq	%rbp, %r8
	movl	%edx, %eax
	movl	%eax, 120(%rsp)         # 4-byte Spill
.LBB0_382:                              # %.us-lcssa.us.i
                                        #   in Loop: Header=BB0_314 Depth=3
	movl	224(%rsp), %ecx         # 4-byte Reload
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	decq	%rax
	incq	%r15
	incl	%edx
	decl	%ecx
	decl	344(%rsp)               # 4-byte Folded Spill
	decq	168(%rsp)               # 8-byte Folded Spill
	decl	336(%rsp)               # 4-byte Folded Spill
	cmpl	%r9d, %edx
	movl	%r12d, %ebp
	movl	%ebp, 184(%rsp)         # 4-byte Spill
	movq	%r8, %rbp
	jbe	.LBB0_314
	jmp	.LBB0_388
.LBB0_383:                              #   in Loop: Header=BB0_278 Depth=2
	addl	$5, %esi
	movl	%esi, 76(%rsp)
	movq	64(%rsp), %rdi
	shlq	$3, %rsi
	callq	xrealloc
	movq	%rax, 64(%rsp)
	movl	72(%rsp), %ecx
.LBB0_384:                              # %add_col_elt.exit525.i
                                        #   in Loop: Header=BB0_278 Depth=2
	movq	248(%rsp), %rbx         # 8-byte Reload
	leal	1(%rcx), %edx
	movl	%edx, 72(%rsp)
	movl	%ecx, %ecx
	movq	%rbp, (%rax,%rcx,8)
	movq	%r13, %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	xorl	%r12d, %r12d
	jmp	.LBB0_405
.LBB0_385:                              #   in Loop: Header=BB0_278 Depth=2
	movq	%r15, %rax
	movl	$2, %ebp
	subl	%eax, %ebp
	addl	192(%rsp), %ebp         # 4-byte Folded Reload
	addl	24(%rsp), %ebp          # 4-byte Folded Reload
	incl	%r9d
	movl	328(%rsp), %ebx         # 4-byte Reload
	decl	%ebx
	movl	$32, %edi
	movq	%r9, %r15
	movl	%r10d, %r14d
	callq	xmalloc
	movq	%rax, %r13
	movl	%ebp, (%r13)
	movl	%r15d, 4(%r13)
	movl	%r14d, 8(%r13)
	movl	%ebx, 12(%r13)
	movl	72(%rsp), %ecx
	movl	76(%rsp), %esi
	cmpl	%ecx, %esi
	jbe	.LBB0_395
# BB#386:                               # %._crit_edge.i518.i
                                        #   in Loop: Header=BB0_278 Depth=2
	movq	64(%rsp), %rax
	jmp	.LBB0_396
.LBB0_387:                              # %._crit_edge593.thread.i
                                        #   in Loop: Header=BB0_278 Depth=2
	movq	200(%rsp), %rdx         # 8-byte Reload
	movl	(%r13,%rdx,4), %ecx
	movl	%ecx, (%rbx)
	movl	$0, (%rax)
	movl	(%r14,%rdx,4), %eax
	movl	%eax, (%rbp)
	movl	$0, (%r12)
	movl	$-1, %r12d
	movl	$1, %edx
	movl	$-1, 120(%rsp)          # 4-byte Folded Spill
	movl	112(%rsp), %esi         # 4-byte Reload
	movq	96(%rsp), %r10          # 8-byte Reload
.LBB0_388:                              # %._crit_edge586.i
                                        #   in Loop: Header=BB0_278 Depth=2
	cmpl	%edx, %r11d
	jae	.LBB0_390
# BB#389:                               #   in Loop: Header=BB0_278 Depth=2
	movq	%rdx, %r12
	movq	%r13, %rdi
	movq	%r10, %r15
	movq	%rbx, %r14
	callq	free
	movq	248(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	136(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	%rbp, %rdi
	callq	free
	movq	160(%rsp), %rdi         # 8-byte Reload
	callq	free
	movl	112(%rsp), %esi         # 4-byte Reload
	movq	144(%rsp), %r13         # 8-byte Reload
	movq	232(%rsp), %rbp         # 8-byte Reload
	movq	240(%rsp), %r8          # 8-byte Reload
	jmp	.LBB0_407
.LBB0_390:                              #   in Loop: Header=BB0_278 Depth=2
	movslq	%r12d, %rcx
	movl	(%rbx,%rcx,4), %ebx
	movl	%esi, %edx
	subl	%ebx, %edx
	movslq	120(%rsp), %rax         # 4-byte Folded Reload
	movl	(%rbp,%rax,4), %esi
	cmpl	%esi, %edx
	cmovll	%esi, %ebx
	movq	136(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx,%rcx,4), %edi
	testl	%ebx, %ebx
	movq	216(%rsp), %rsi         # 8-byte Reload
	movq	192(%rsp), %rbp         # 8-byte Reload
	je	.LBB0_399
# BB#391:                               #   in Loop: Header=BB0_278 Depth=2
	movl	%edi, 24(%rsp)          # 4-byte Spill
	leal	1(%rbp), %r15d
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill>
	subl	16(%rsp), %ebp          # 4-byte Folded Reload
	addl	%ebx, %ebp
	movq	160(%rsp), %rcx         # 8-byte Reload
	addl	(%rcx,%rax,4), %ebp
	leal	(%rbx,%rsi), %r13d
	leal	1(%rsi), %r14d
	movl	$32, %edi
	callq	xmalloc
	movq	%rax, %r8
	movl	%r15d, (%r8)
	movl	%r14d, 4(%r8)
	movl	%ebp, 8(%r8)
	movl	%r13d, 12(%r8)
	movl	72(%rsp), %ecx
	movl	76(%rsp), %esi
	cmpl	%ecx, %esi
	jbe	.LBB0_397
# BB#392:                               # %._crit_edge.i528.i
                                        #   in Loop: Header=BB0_278 Depth=2
	movq	64(%rsp), %rax
	jmp	.LBB0_398
.LBB0_393:                              #   in Loop: Header=BB0_278 Depth=2
	addl	$5, %esi
	movl	%esi, 76(%rsp)
	movq	64(%rsp), %rdi
	shlq	$3, %rsi
	callq	xrealloc
	movq	%rax, 64(%rsp)
	movl	72(%rsp), %ecx
.LBB0_394:                              # %add_col_elt.exit.i
                                        #   in Loop: Header=BB0_278 Depth=2
	movl	112(%rsp), %esi         # 4-byte Reload
	movq	240(%rsp), %r8          # 8-byte Reload
	movq	%r15, %r13
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	leal	1(%rcx), %edx
	movl	%edx, 72(%rsp)
	movl	%ecx, %ecx
	movq	%rbx, (%rax,%rcx,8)
	movl	$1, %eax
	subl	88(%rsp), %eax          # 4-byte Folded Reload
	addl	%esi, %eax
	mulsd	.LCPI0_2(%rip), %xmm0
	addsd	.LCPI0_1(%rip), %xmm0
	cvttsd2si	%xmm0, %r12
	addl	%eax, %r12d
	jmp	.LBB0_407
.LBB0_395:                              #   in Loop: Header=BB0_278 Depth=2
	addl	$5, %esi
	movl	%esi, 76(%rsp)
	movq	64(%rsp), %rdi
	shlq	$3, %rsi
	callq	xrealloc
	movq	%rax, 64(%rsp)
	movl	72(%rsp), %ecx
.LBB0_396:                              # %add_col_elt.exit520.i
                                        #   in Loop: Header=BB0_278 Depth=2
	movl	112(%rsp), %esi         # 4-byte Reload
	movq	232(%rsp), %rbp         # 8-byte Reload
	movq	240(%rsp), %r8          # 8-byte Reload
	leal	1(%rcx), %edx
	movl	%edx, 72(%rsp)
	movl	%ecx, %ecx
	movq	%r13, (%rax,%rcx,8)
	jmp	.LBB0_406
.LBB0_397:                              #   in Loop: Header=BB0_278 Depth=2
	addl	$5, %esi
	movl	%esi, 76(%rsp)
	movq	64(%rsp), %rdi
	shlq	$3, %rsi
	movq	%r8, %rbp
	callq	xrealloc
	movq	%rbp, %r8
	movq	%rax, 64(%rsp)
	movl	72(%rsp), %ecx
.LBB0_398:                              # %add_col_elt.exit530.i
                                        #   in Loop: Header=BB0_278 Depth=2
	movq	216(%rsp), %rsi         # 8-byte Reload
	movq	192(%rsp), %rbp         # 8-byte Reload
	movq	48(%rsp), %r13          # 8-byte Reload
	movl	24(%rsp), %edi          # 4-byte Reload
	leal	1(%rcx), %edx
	movl	%edx, 72(%rsp)
	movl	%ecx, %ecx
	movq	%r8, (%rax,%rcx,8)
.LBB0_399:                              #   in Loop: Header=BB0_278 Depth=2
	cmpl	112(%rsp), %ebx         # 4-byte Folded Reload
	jae	.LBB0_404
# BB#400:                               #   in Loop: Header=BB0_278 Depth=2
	incl	%ebp
	subl	16(%rsp), %ebp          # 4-byte Folded Reload
	addl	352(%rsp), %ebp         # 4-byte Folded Reload
	addl	%edi, %ebp
	addl	%ebx, %ebp
	leal	1(%rsi,%rbx), %r14d
	movl	328(%rsp), %ebx         # 4-byte Reload
	decl	%ebx
	movl	$32, %edi
	callq	xmalloc
	movq	%rax, %r15
	movl	%ebp, (%r15)
	movl	%r14d, 4(%r15)
	movl	360(%rsp), %eax         # 4-byte Reload
	movl	%eax, 8(%r15)
	movl	%ebx, 12(%r15)
	movl	72(%rsp), %ecx
	movl	76(%rsp), %esi
	cmpl	%ecx, %esi
	jbe	.LBB0_402
# BB#401:                               # %._crit_edge.i533.i
                                        #   in Loop: Header=BB0_278 Depth=2
	movq	64(%rsp), %rax
	jmp	.LBB0_403
.LBB0_402:                              #   in Loop: Header=BB0_278 Depth=2
	addl	$5, %esi
	movl	%esi, 76(%rsp)
	movq	64(%rsp), %rdi
	shlq	$3, %rsi
	callq	xrealloc
	movq	%rax, 64(%rsp)
	movl	72(%rsp), %ecx
.LBB0_403:                              # %add_col_elt.exit535.i
                                        #   in Loop: Header=BB0_278 Depth=2
	leal	1(%rcx), %edx
	movl	%edx, 72(%rsp)
	movl	%ecx, %ecx
	movq	%r15, (%rax,%rcx,8)
.LBB0_404:                              #   in Loop: Header=BB0_278 Depth=2
	movq	%r13, %rdi
	callq	free
	movq	248(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	272(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	136(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	256(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	160(%rsp), %rdi         # 8-byte Reload
	callq	free
	movl	120(%rsp), %eax         # 4-byte Reload
	addl	%r12d, %eax
	movl	%eax, %r12d
.LBB0_405:                              # %greedy.exit
                                        #   in Loop: Header=BB0_278 Depth=2
	movl	112(%rsp), %esi         # 4-byte Reload
	movq	232(%rsp), %rbp         # 8-byte Reload
	movq	240(%rsp), %r8          # 8-byte Reload
.LBB0_406:                              # %greedy.exit
                                        #   in Loop: Header=BB0_278 Depth=2
	movq	144(%rsp), %r13         # 8-byte Reload
.LBB0_407:                              # %greedy.exit
                                        #   in Loop: Header=BB0_278 Depth=2
	movl	72(%rsp), %r9d
	testl	%r9d, %r9d
	je	.LBB0_409
# BB#408:                               #   in Loop: Header=BB0_278 Depth=2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	movl	36(%r13), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rcx, %xmm1
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%esi, %xmm2
	mulsd	.LCPI0_2(%rip), %xmm2
	addsd	.LCPI0_1(%rip), %xmm2
	maxsd	%xmm1, %xmm2
	ucomisd	%xmm0, %xmm2
	jae	.LBB0_425
	.p2align	4, 0x90
.LBB0_409:                              # %.thread545
                                        #   in Loop: Header=BB0_278 Depth=2
	movl	36(%r13), %eax
	cmpl	$8, %eax
	movl	$8, %ecx
	cmovael	%ecx, %eax
	movl	8(%rbp), %ecx
	movl	(%r8), %edi
	subl	%ecx, %edi
	movq	%r13, %r15
	addq	8(%r13), %rcx
	leal	-1(%rdi), %edx
	movl	%eax, 324(%rsp)
	movq	%rcx, 296(%rsp)
	movl	%edx, 304(%rsp)
	leal	-2(%rax,%rax), %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	decl	%eax
	movl	%eax, 320(%rsp)
	shlq	$2, %rdi
	movl	%esi, %r13d
	callq	xmalloc
	movq	%rax, %r12
	movq	%r12, 312(%rsp)
	movl	$524288, %edi           # imm = 0x80000
	movl	$8, %esi
	callq	xcalloc
	movq	%rax, %rbx
	movq	%rbx, 288(%rsp)
	leaq	288(%rsp), %r14
	movq	%r14, %rdi
	callq	bld_table
	movl	8(%rbp), %ecx
	movl	12(%rbp), %esi
	leal	1(%rsi), %r8d
	movq	128(%rsp), %rax         # 8-byte Reload
	addq	16(%rax), %rsi
	incl	%ecx
	movl	options+24(%rip), %r9d
	subq	$8, %rsp
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	movq	%r14, %rdi
	movl	%r13d, %edx
	leaq	72(%rsp), %rax
	pushq	%rax
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	leaq	480(%rsp), %rax
	pushq	%rax
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	callq	exon_cores
	addq	$32, %rsp
.Lcfi32:
	.cfi_adjust_cfa_offset -32
	movq	%r12, %rdi
	callq	free
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_410:                              #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_278 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbx,%rbp,8), %rdi
	movl	$free, %esi
	callq	tdestroy
	incq	%rbp
	cmpq	$524288, %rbp           # imm = 0x80000
	jne	.LBB0_410
# BB#411:                               # %free_hash_env.exit474
                                        #   in Loop: Header=BB0_278 Depth=2
	movq	%rbx, %rdi
	callq	free
	movl	72(%rsp), %r9d
	testl	%r9d, %r9d
	je	.LBB0_424
# BB#412:                               #   in Loop: Header=BB0_278 Depth=2
	movq	64(%rsp), %r8
	movq	(%r8), %rdi
	movq	%r15, %r13
	movq	8(%r13), %r11
	movq	128(%rsp), %r12         # 8-byte Reload
	movq	16(%r12), %r10
	movl	(%rdi), %ebp
	leaq	-2(%r11,%rbp), %rcx
	cmpq	%r11, %rcx
	jb	.LBB0_418
# BB#413:                               #   in Loop: Header=BB0_278 Depth=2
	movl	4(%rdi), %ebx
	leaq	-2(%r10,%rbx), %rcx
	cmpq	%r10, %rcx
	jb	.LBB0_418
# BB#414:                               # %.lr.ph.i478.preheader
                                        #   in Loop: Header=BB0_278 Depth=2
	movl	%ebp, %ecx
	addq	%r11, %rbp
	movl	%ebx, %edx
	addq	%r10, %rbx
	leal	-1(%rdx), %r14d
	leal	-1(%rcx), %edx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_415:                              # %.lr.ph.i478
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_278 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	-2(%rbp,%rcx), %eax
	cmpb	-2(%rbx,%rcx), %al
	jne	.LBB0_418
# BB#416:                               #   in Loop: Header=BB0_415 Depth=3
	leaq	-3(%rbx,%rcx), %rax
	leal	(%rdx,%rcx), %esi
	movl	%esi, (%rdi)
	leal	(%r14,%rcx), %esi
	movl	%esi, 4(%rdi)
	cmpq	%r10, %rax
	jb	.LBB0_418
# BB#417:                               #   in Loop: Header=BB0_415 Depth=3
	leaq	-3(%rbp,%rcx), %rax
	decq	%rcx
	cmpq	%r11, %rax
	jae	.LBB0_415
.LBB0_418:                              # %grow_exon_left.exit481
                                        #   in Loop: Header=BB0_278 Depth=2
	decl	%r9d
	movq	(%r8,%r9,8), %rdx
	movl	16(%r13), %esi
	movl	8(%rdx), %edi
	cmpl	%esi, %edi
	jae	.LBB0_423
# BB#419:                               # %.lr.ph.preheader.i483
                                        #   in Loop: Header=BB0_278 Depth=2
	movl	4148(%r12), %ebp
	movl	12(%rdx), %ecx
	.p2align	4, 0x90
.LBB0_420:                              # %.lr.ph.i484
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_278 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%ebp, %ecx
	jae	.LBB0_423
# BB#421:                               #   in Loop: Header=BB0_420 Depth=3
	movl	%edi, %eax
	movzbl	(%r11,%rax), %eax
	movl	%ecx, %ebx
	cmpb	(%r10,%rbx), %al
	jne	.LBB0_423
# BB#422:                               #   in Loop: Header=BB0_420 Depth=3
	incl	%edi
	movl	%edi, 8(%rdx)
	incl	%ecx
	movl	%ecx, 12(%rdx)
	cmpl	%esi, %edi
	jb	.LBB0_420
.LBB0_423:                              # %grow_exon_right.exit486
                                        #   in Loop: Header=BB0_278 Depth=2
	movl	36(%r13), %ecx
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rdi
	leaq	64(%rsp), %rsi
	movq	264(%rsp), %rdx         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	jmp	.LBB0_437
.LBB0_424:                              #   in Loop: Header=BB0_278 Depth=2
	movq	264(%rsp), %rax         # 8-byte Reload
	movl	%eax, %esi
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	128(%rsp), %r12         # 8-byte Reload
	movq	%r15, %r13
	jmp	.LBB0_438
.LBB0_425:                              #   in Loop: Header=BB0_278 Depth=2
	movq	64(%rsp), %r8
	movq	(%r8), %rbp
	movq	8(%r13), %r11
	movq	128(%rsp), %r12         # 8-byte Reload
	movq	16(%r12), %r10
	movl	(%rbp), %ebx
	leaq	-2(%r11,%rbx), %rax
	cmpq	%r11, %rax
	jb	.LBB0_431
# BB#426:                               #   in Loop: Header=BB0_278 Depth=2
	movl	4(%rbp), %edi
	leaq	-2(%r10,%rdi), %rax
	cmpq	%r10, %rax
	jb	.LBB0_431
# BB#427:                               # %.lr.ph.i462.preheader
                                        #   in Loop: Header=BB0_278 Depth=2
	movl	%ebx, %eax
	addq	%r11, %rbx
	movl	%edi, %edx
	addq	%r10, %rdi
	leal	-1(%rdx), %r14d
	leal	-1(%rax), %r15d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_428:                              # %.lr.ph.i462
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_278 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	-2(%rbx,%rax), %edx
	cmpb	-2(%rdi,%rax), %dl
	jne	.LBB0_431
# BB#429:                               #   in Loop: Header=BB0_428 Depth=3
	leaq	-3(%rdi,%rax), %rdx
	leal	(%r15,%rax), %esi
	movl	%esi, (%rbp)
	leal	(%r14,%rax), %esi
	movl	%esi, 4(%rbp)
	cmpq	%r10, %rdx
	jb	.LBB0_431
# BB#430:                               #   in Loop: Header=BB0_428 Depth=3
	leaq	-3(%rbx,%rax), %rdx
	decq	%rax
	cmpq	%r11, %rdx
	jae	.LBB0_428
.LBB0_431:                              # %grow_exon_left.exit465
                                        #   in Loop: Header=BB0_278 Depth=2
	decl	%r9d
	movq	(%r8,%r9,8), %rsi
	movq	144(%rsp), %r13         # 8-byte Reload
	movl	16(%r13), %edi
	movl	8(%rsi), %ebp
	cmpl	%edi, %ebp
	jae	.LBB0_436
# BB#432:                               # %.lr.ph.preheader.i467
                                        #   in Loop: Header=BB0_278 Depth=2
	movl	4148(%r12), %r8d
	movl	12(%rsi), %eax
	.p2align	4, 0x90
.LBB0_433:                              # %.lr.ph.i468
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_278 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%r8d, %eax
	jae	.LBB0_436
# BB#434:                               #   in Loop: Header=BB0_433 Depth=3
	movl	%ebp, %ebx
	movzbl	(%r11,%rbx), %ebx
	movl	%eax, %edx
	cmpb	(%r10,%rdx), %bl
	jne	.LBB0_436
# BB#435:                               #   in Loop: Header=BB0_433 Depth=3
	incl	%ebp
	movl	%ebp, 8(%rsi)
	incl	%eax
	movl	%eax, 12(%rsi)
	cmpl	%edi, %ebp
	jb	.LBB0_433
.LBB0_436:                              # %.loopexit
                                        #   in Loop: Header=BB0_278 Depth=2
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rdi
	leaq	64(%rsp), %rsi
	movq	264(%rsp), %rdx         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
.LBB0_437:                              # %.sink.split
                                        #   in Loop: Header=BB0_278 Depth=2
	callq	merge
	movq	32(%rsp), %r14          # 8-byte Reload
	movl	$0, 72(%rsp)
	movq	280(%rsp), %rsi         # 8-byte Reload
.LBB0_438:                              #   in Loop: Header=BB0_278 Depth=2
	movl	16(%r14), %r11d
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
.LBB0_439:                              #   in Loop: Header=BB0_278 Depth=2
	incl	%esi
	cmpl	%r11d, %esi
	jb	.LBB0_278
.LBB0_440:                              # %.loopexit585
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	8(%r13), %rsi
	movq	16(%r12), %rdx
	movq	%r14, %rdi
	callq	kill_polyA
	movl	16(%r14), %ecx
	cmpl	$2, %ecx
	jb	.LBB0_472
# BB#441:                               # %.lr.ph92.lr.ph.i
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	36(%r13), %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	$1, %r12d
.LBB0_442:                              # %.lr.ph92.i
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_452 Depth 3
                                        #       Child Loop BB0_444 Depth 3
	leal	-1(%r12), %r13d
	movl	%r12d, %r15d
	movl	$1, %eax
	subl	%r12d, %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	cmpl	$1, %r12d
	jbe	.LBB0_452
# BB#443:                               # %.lr.ph92.split.us.i.preheader
                                        #   in Loop: Header=BB0_442 Depth=2
	addl	$-2, %r12d
	movq	%r13, 96(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_444:                              # %.lr.ph92.split.us.i
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_442 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbp), %rax
	movq	(%rax,%r13,8), %rbx
	movq	(%rax,%r15,8), %rbp
	movl	4(%rbp), %r13d
	subl	4(%rbx), %r13d
	cmpl	options+36(%rip), %r13d
	ja	.LBB0_459
# BB#445:                               #   in Loop: Header=BB0_444 Depth=3
	decl	%ecx
	movq	32(%rsp), %r14          # 8-byte Reload
	movl	%ecx, 16(%r14)
	movl	12(%rbx), %eax
	cmpl	12(%rbp), %eax
	jbe	.LBB0_448
# BB#446:                               #   in Loop: Header=BB0_444 Depth=3
	movq	%rbp, %rdi
	callq	free
	movq	8(%r14), %rax
	leaq	(%rax,%r15,8), %rdi
	leaq	8(%rax,%r15,8), %rsi
	movl	16(%r14), %edx
	subl	%r15d, %edx
	shlq	$3, %rdx
	callq	memmove
	movl	16(%r14), %ecx
	cmpl	%ecx, %r15d
	jae	.LBB0_450
# BB#447:                               #   in Loop: Header=BB0_444 Depth=3
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	(%rbp), %rax
	movq	(%rax,%r15,8), %rax
	addl	%r13d, 8(%rbx)
	addl	%r13d, 12(%rbx)
	subl	%r13d, (%rax)
	addq	$4, %rax
	jmp	.LBB0_449
	.p2align	4, 0x90
.LBB0_448:                              #   in Loop: Header=BB0_444 Depth=3
	movq	%rbx, %rdi
	callq	free
	movq	8(%r14), %rax
	leaq	(%rax,%r15,8), %rsi
	leaq	-8(%rax,%r15,8), %rdi
	movl	16(%r14), %edx
	addl	24(%rsp), %edx          # 4-byte Folded Reload
	shlq	$3, %rdx
	callq	memmove
	movq	8(%r14), %rax
	movq	(%rax,%r12,8), %rax
	addl	%r13d, 8(%rax)
	addl	%r13d, 12(%rax)
	subl	%r13d, (%rbp)
	leaq	4(%rbp), %rax
	movl	16(%r14), %ecx
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB0_449:                              # %.backedge.us.sink.split.i
                                        #   in Loop: Header=BB0_444 Depth=3
	subl	%r13d, (%rax)
	jmp	.LBB0_451
	.p2align	4, 0x90
.LBB0_450:                              #   in Loop: Header=BB0_444 Depth=3
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB0_451:                              # %.backedge.us.i
                                        #   in Loop: Header=BB0_444 Depth=3
	movq	96(%rsp), %r13          # 8-byte Reload
	cmpl	%ecx, %r15d
	jb	.LBB0_444
	jmp	.LBB0_460
	.p2align	4, 0x90
.LBB0_452:                              # %.lr.ph92.split.i
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_442 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbp), %rax
	movq	(%rax,%r13,8), %rbx
	movq	(%rax,%r15,8), %rdi
	movl	4(%rdi), %ebp
	subl	4(%rbx), %ebp
	cmpl	options+36(%rip), %ebp
	ja	.LBB0_459
# BB#453:                               #   in Loop: Header=BB0_452 Depth=3
	decl	%ecx
	movq	32(%rsp), %r14          # 8-byte Reload
	movl	%ecx, 16(%r14)
	movl	12(%rbx), %eax
	cmpl	12(%rdi), %eax
	jbe	.LBB0_456
# BB#454:                               #   in Loop: Header=BB0_452 Depth=3
	callq	free
	movq	8(%r14), %rax
	leaq	(%rax,%r15,8), %rdi
	leaq	8(%rax,%r15,8), %rsi
	movl	16(%r14), %edx
	subl	%r15d, %edx
	shlq	$3, %rdx
	callq	memmove
	movl	16(%r14), %ecx
	cmpl	%ecx, %r15d
	jae	.LBB0_457
# BB#455:                               #   in Loop: Header=BB0_452 Depth=3
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rdx), %rax
	movq	(%rax,%r15,8), %rax
	addl	%ebp, 8(%rbx)
	addl	%ebp, 12(%rbx)
	subl	%ebp, (%rax)
	subl	%ebp, 4(%rax)
	movq	%rdx, %rbp
	jmp	.LBB0_458
	.p2align	4, 0x90
.LBB0_456:                              #   in Loop: Header=BB0_452 Depth=3
	movq	%rbx, %rdi
	callq	free
	movq	8(%r14), %rax
	leaq	(%rax,%r15,8), %rsi
	leaq	-8(%rax,%r15,8), %rdi
	movl	16(%r14), %edx
	addl	24(%rsp), %edx          # 4-byte Folded Reload
	shlq	$3, %rdx
	callq	memmove
	movl	16(%r14), %ecx
.LBB0_457:                              #   in Loop: Header=BB0_452 Depth=3
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB0_458:                              # %.backedge.i
                                        #   in Loop: Header=BB0_452 Depth=3
	cmpl	%ecx, %r15d
	jb	.LBB0_452
	jmp	.LBB0_460
	.p2align	4, 0x90
.LBB0_459:                              #   in Loop: Header=BB0_442 Depth=2
	movq	8(%rsp), %rbp           # 8-byte Reload
	incl	%r15d
	cmpl	%ecx, %r15d
	movl	%r15d, %r12d
	jb	.LBB0_442
	.p2align	4, 0x90
.LBB0_460:                              # %.preheader.i487
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpl	$2, %ecx
	jb	.LBB0_471
# BB#461:                               # %.lr.ph.i488
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	$1, %r15d
	movq	128(%rsp), %r12         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_462:                              #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rax
	leal	-1(%r15), %ebx
	movq	(%rax,%rbx,8), %rsi
	movl	%r15d, %r14d
	movq	(%rax,%r14,8), %rdi
	movl	(%rdi), %eax
	movl	8(%rsi), %r10d
	leal	31(%r10), %ebp
	cmpl	%ebp, %eax
	jae	.LBB0_464
# BB#463:                               #   in Loop: Header=BB0_462 Depth=2
	movl	4(%rdi), %ebp
	leaq	12(%rsi), %r8
	movl	12(%rsi), %r9d
	movq	56(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%r9), %edx
	cmpl	%edx, %ebp
	ja	.LBB0_465
	jmp	.LBB0_469
	.p2align	4, 0x90
.LBB0_464:                              # %._crit_edge103.i
                                        #   in Loop: Header=BB0_462 Depth=2
	leaq	12(%rsi), %r8
	movl	12(%rsi), %r9d
	movl	4(%rdi), %ebp
.LBB0_465:                              #   in Loop: Header=BB0_462 Depth=2
	subl	%r10d, %eax
	jbe	.LBB0_468
# BB#466:                               #   in Loop: Header=BB0_462 Depth=2
	subl	%r9d, %ebp
	jbe	.LBB0_468
# BB#467:                               # %about_same_gap_p.exit.i
                                        #   in Loop: Header=BB0_462 Depth=2
	decl	%eax
	decl	%ebp
	cmpl	%eax, %ebp
	movl	%ebp, %edx
	cmoval	%eax, %edx
	cmovbel	%eax, %ebp
	movl	%ebp, %eax
	subl	%edx, %eax
	imull	$100, %eax, %eax
	xorl	%edx, %edx
	divl	%ebp
	cmpl	options+32(%rip), %eax
	jbe	.LBB0_469
.LBB0_468:                              #   in Loop: Header=BB0_462 Depth=2
	movl	%r15d, %ebx
	movq	32(%rsp), %r14          # 8-byte Reload
	jmp	.LBB0_470
	.p2align	4, 0x90
.LBB0_469:                              #   in Loop: Header=BB0_462 Depth=2
	movl	8(%rdi), %eax
	movl	%eax, 8(%rsi)
	movl	12(%rdi), %eax
	movl	%eax, (%r8)
	callq	free
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	16(%rcx), %edx
	decl	%edx
	movl	%edx, 16(%rcx)
	movq	8(%rcx), %rax
	leaq	(%rax,%r14,8), %rdi
	leaq	8(%rax,%r14,8), %rsi
	movq	%rcx, %r14
	subl	%r15d, %edx
	shlq	$3, %rdx
	callq	memmove
	movl	16(%r14), %ecx
.LBB0_470:                              # %about_same_gap_p.exit.thread.i
                                        #   in Loop: Header=BB0_462 Depth=2
	movq	8(%rsp), %rbp           # 8-byte Reload
	incl	%ebx
	cmpl	%ecx, %ebx
	movl	%ebx, %r15d
	jb	.LBB0_462
	jmp	.LBB0_472
.LBB0_471:                              #   in Loop: Header=BB0_4 Depth=1
	movq	128(%rsp), %r12         # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
.LBB0_472:                              # %compact_exons.exit
                                        #   in Loop: Header=BB0_4 Depth=1
	testl	%ecx, %ecx
	movq	144(%rsp), %r15         # 8-byte Reload
	je	.LBB0_482
# BB#473:                               # %.lr.ph656.preheader
                                        #   in Loop: Header=BB0_4 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_474:                              # %.lr.ph656
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rax
	movl	%ebx, %edx
	movq	(%rax,%rdx,8), %rdi
	movl	12(%rdi), %eax
	incl	%eax
	subl	4(%rdi), %eax
	cmpl	36(%r15), %eax
	jae	.LBB0_476
# BB#475:                               # %.thread547
                                        #   in Loop: Header=BB0_474 Depth=2
	callq	free
	incl	%ebx
	movl	16(%r14), %ecx
	cmpl	%ecx, %ebx
	jb	.LBB0_474
	jmp	.LBB0_477
.LBB0_476:                              # %._crit_edge657
                                        #   in Loop: Header=BB0_4 Depth=1
	testl	%ebx, %ebx
	je	.LBB0_478
.LBB0_477:                              # %thread-pre-split
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	8(%r14), %rdi
	movl	%ebx, %eax
	leaq	(%rdi,%rax,8), %rsi
	subl	%ebx, %ecx
	shlq	$3, %rcx
	movq	%rcx, %rdx
	callq	memmove
	movl	16(%r14), %ecx
	subl	%ebx, %ecx
	movl	%ecx, 16(%r14)
	je	.LBB0_482
.LBB0_478:                              # %thread-pre-split.thread
                                        #   in Loop: Header=BB0_4 Depth=1
	decl	%ecx
	js	.LBB0_482
# BB#479:                               # %.preheader
                                        #   in Loop: Header=BB0_4 Depth=1
	movslq	%ecx, %rbx
	incq	%rbx
	.p2align	4, 0x90
.LBB0_480:                              #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rax
	movq	-8(%rax,%rbx,8), %rdi
	movl	12(%rdi), %eax
	incl	%eax
	subl	4(%rdi), %eax
	cmpl	36(%r15), %eax
	jae	.LBB0_482
# BB#481:                               #   in Loop: Header=BB0_480 Depth=2
	callq	free
	movl	16(%r14), %eax
	decl	%eax
	movl	%eax, 16(%r14)
	decq	%rbx
	jg	.LBB0_480
	jmp	.LBB0_483
	.p2align	4, 0x90
.LBB0_482:                              # %thread-pre-split551
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	16(%r14), %eax
.LBB0_483:                              # %.loopexit581
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	8(%r15), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	16(%r12), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leaq	32(%r14), %r13
	cmpl	$1, %eax
	jbe	.LBB0_506
# BB#484:                               # %.lr.ph313.i
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB0_485:                              #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rax
	leal	-1(%rbx), %ecx
	movq	(%rax,%rcx,8), %rbp
	movl	%ebx, %ecx
	movq	(%rax,%rcx,8), %rax
	movl	$-16777216, 28(%rbp)    # imm = 0xFF000000
	movl	4(%rax), %edx
	movl	12(%rbp), %ecx
	subl	%ecx, %edx
	cmpl	$1, %edx
	jne	.LBB0_488
# BB#486:                               #   in Loop: Header=BB0_485 Depth=2
	movl	8(%rbp), %edx
	movl	%edx, 288(%rsp)
	movl	%ecx, 292(%rsp)
	movl	(%rax), %eax
	movl	%eax, 296(%rsp)
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	leaq	288(%rsp), %rdx
	callq	perfect_spl_p
	testl	%eax, %eax
	je	.LBB0_488
# BB#487:                               #   in Loop: Header=BB0_485 Depth=2
	movl	312(%rsp), %eax
	addl	%eax, (%r13)
	andl	$3, %eax
	shlq	$32, %rax
	movq	24(%rbp), %rcx
	movabsq	$72057581153026047, %rdx # imm = 0xFFFFFCFFFFFFFF
	andq	%rdx, %rcx
	movl	300(%rsp), %edx
	shlq	$56, %rdx
	movabsq	$12884901888, %rsi      # imm = 0x300000000
	andq	%rsi, %rax
	orq	%rcx, %rax
	orq	%rdx, %rax
	movq	%rax, 24(%rbp)
	movl	options+44(%rip), %ecx
	leal	4(%rcx,%rcx), %ecx
	andl	$4194302, %ecx          # imm = 0x3FFFFE
	shlq	$34, %rcx
	movabsq	$-72057576858058753, %rdx # imm = 0xFF000003FFFFFFFF
	andq	%rdx, %rax
	orq	%rcx, %rax
	movq	%rax, 24(%rbp)
.LBB0_488:                              #   in Loop: Header=BB0_485 Depth=2
	incl	%ebx
	movl	16(%r14), %eax
	cmpl	%eax, %ebx
	movq	8(%rsp), %rbp           # 8-byte Reload
	jb	.LBB0_485
# BB#489:                               # %.preheader260.i
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpl	$2, %eax
	jb	.LBB0_506
# BB#490:                               # %.lr.ph309.i.preheader
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	$1, %ebx
	movq	%r13, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_491:                              # %.lr.ph309.i
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_494 Depth 3
                                        #       Child Loop BB0_500 Depth 3
	movq	(%rbp), %rcx
	leal	-1(%rbx), %edx
	movq	(%rcx,%rdx,8), %r12
	movl	%ebx, %edx
	movq	(%rcx,%rdx,8), %rcx
	movl	12(%r12), %r14d
	subl	4(%rcx), %r14d
	jb	.LBB0_504
# BB#492:                               #   in Loop: Header=BB0_491 Depth=2
	leal	2(%r14), %ebp
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	imulq	$28, %rbp, %rdi
	callq	xmalloc
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	%rax, 40(%rsp)          # 8-byte Spill
	testl	%ebp, %ebp
	je	.LBB0_503
# BB#493:                               # %.lr.ph303.i
                                        #   in Loop: Header=BB0_491 Depth=2
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	notl	%r14d
	xorl	%r15d, %r15d
	movq	40(%rsp), %rbx          # 8-byte Reload
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_494:                              #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_491 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%r14,%r15), %eax
	movl	8(%r12), %ecx
	addl	%eax, %ecx
	movl	%ecx, (%rbx)
	addl	12(%r12), %eax
	movl	%eax, 4(%rbx)
	movl	(%rdi), %eax
	addl	%r15d, %eax
	movl	%eax, 8(%rbx)
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	callq	perfect_spl_p
	testl	%eax, %eax
	je	.LBB0_496
# BB#495:                               #   in Loop: Header=BB0_494 Depth=3
	incl	%r13d
	jmp	.LBB0_497
	.p2align	4, 0x90
.LBB0_496:                              #   in Loop: Header=BB0_494 Depth=3
	movl	$0, 24(%rbx)
.LBB0_497:                              #   in Loop: Header=BB0_494 Depth=3
	movq	48(%rsp), %rdi          # 8-byte Reload
	incq	%r15
	addq	$28, %rbx
	cmpq	%r15, %rbp
	jne	.LBB0_494
# BB#498:                               # %._crit_edge304.i
                                        #   in Loop: Header=BB0_491 Depth=2
	cmpl	$1, %r13d
	movq	144(%rsp), %r15         # 8-byte Reload
	movq	56(%rsp), %r13          # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	jne	.LBB0_503
# BB#499:                               # %.lr.ph307.i
                                        #   in Loop: Header=BB0_491 Depth=2
	movq	40(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_500:                              #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_491 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	24(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB0_502
# BB#501:                               #   in Loop: Header=BB0_500 Depth=3
	addl	%ecx, (%r13)
	andl	$3, %ecx
	shlq	$32, %rcx
	movq	24(%r12), %rdx
	movabsq	$-12884901889, %rsi     # imm = 0xFFFFFFFCFFFFFFFF
	andq	%rsi, %rdx
	orq	%rcx, %rdx
	movq	%rdx, 24(%r12)
	movl	12(%rax), %ecx
	shlq	$56, %rcx
	movabsq	$72057594037927935, %rsi # imm = 0xFFFFFFFFFFFFFF
	andq	%rsi, %rdx
	orq	%rcx, %rdx
	movq	%rdx, 24(%r12)
	movl	options+44(%rip), %ecx
	leal	4(%rcx,%rcx), %ecx
	andl	$4194302, %ecx          # imm = 0x3FFFFE
	shlq	$34, %rcx
	movabsq	$-72057576858058753, %rsi # imm = 0xFF000003FFFFFFFF
	andq	%rsi, %rdx
	orq	%rcx, %rdx
	movq	%rdx, 24(%r12)
	movl	(%rax), %ecx
	movl	%ecx, 8(%r12)
	movl	4(%rax), %ecx
	movl	%ecx, 12(%r12)
	incl	%ecx
	movl	%ecx, 4(%rdi)
	movl	8(%rax), %ecx
	movl	%ecx, (%rdi)
.LBB0_502:                              #   in Loop: Header=BB0_500 Depth=3
	addq	$28, %rax
	decq	%rbp
	jne	.LBB0_500
.LBB0_503:                              # %.loopexit.i498
                                        #   in Loop: Header=BB0_491 Depth=2
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	16(%rax), %eax
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB0_504:                              #   in Loop: Header=BB0_491 Depth=2
	incl	%ebx
	cmpl	%eax, %ebx
	jb	.LBB0_491
# BB#505:                               #   in Loop: Header=BB0_4 Depth=1
	movq	128(%rsp), %r12         # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
.LBB0_506:                              # %._crit_edge310.i
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpl	$0, (%r13)
	movq	%r13, 56(%rsp)          # 8-byte Spill
	jne	.LBB0_532
# BB#507:                               # %.preheader258.i
                                        #   in Loop: Header=BB0_4 Depth=1
	xorl	%ebx, %ebx
	cmpl	$2, %eax
	jb	.LBB0_530
# BB#508:                               # %.lr.ph296.i
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	$0, 136(%rsp)           # 4-byte Folded Spill
	movl	$1, %r12d
	xorl	%ebx, %ebx
                                        # implicit-def: %ECX
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	jmp	.LBB0_525
.LBB0_509:                              #   in Loop: Header=BB0_525 Depth=2
	js	.LBB0_522
# BB#510:                               #   in Loop: Header=BB0_525 Depth=2
	movl	%ebx, 88(%rsp)          # 4-byte Spill
	movl	%ecx, %ebx
	subl	%esi, %ebx
	cmpl	$-2, %ebx
	je	.LBB0_523
# BB#511:                               # %.lr.ph288.i
                                        #   in Loop: Header=BB0_525 Depth=2
	notl	%ebx
	movl	$-1, 40(%rsp)           # 4-byte Folded Spill
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	jmp	.LBB0_513
	.p2align	4, 0x90
.LBB0_512:                              # %splice_score_compare.exit.thread._crit_edge.i
                                        #   in Loop: Header=BB0_513 Depth=3
	incl	%r14d
	movl	12(%rbp), %ecx
.LBB0_513:                              #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_525 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rbx,%r14), %eax
	addl	8(%rbp), %eax
	movl	%eax, 288(%rsp)
	addl	%ebx, %ecx
	addl	%r14d, %ecx
	movl	%ecx, 292(%rsp)
	movl	(%r15), %eax
	addl	%r14d, %eax
	movl	%eax, 296(%rsp)
	xorl	%ecx, %ecx
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	leaq	288(%rsp), %rdx
	callq	compute_max_score
	movl	304(%rsp), %eax
	cmpl	%r13d, %eax
	jb	.LBB0_520
# BB#514:                               #   in Loop: Header=BB0_513 Depth=3
	movl	308(%rsp), %ecx
	jbe	.LBB0_517
# BB#515:                               # %.splice_score_compare.exit.thread251.i_crit_edge
                                        #   in Loop: Header=BB0_513 Depth=3
	movl	300(%rsp), %edx
.LBB0_516:                              # %splice_score_compare.exit.thread251.i
                                        #   in Loop: Header=BB0_513 Depth=3
	movl	312(%rsp), %esi
	movl	%esi, 16(%rsp)          # 4-byte Spill
	movl	%ecx, 48(%rsp)          # 4-byte Spill
	movl	%eax, %r13d
	movl	%edx, 40(%rsp)          # 4-byte Spill
	jmp	.LBB0_520
	.p2align	4, 0x90
.LBB0_517:                              #   in Loop: Header=BB0_513 Depth=3
	cmpl	48(%rsp), %ecx          # 4-byte Folded Reload
	jb	.LBB0_520
# BB#518:                               #   in Loop: Header=BB0_513 Depth=3
	movl	300(%rsp), %edx
	ja	.LBB0_516
# BB#519:                               #   in Loop: Header=BB0_513 Depth=3
	cmpl	40(%rsp), %edx          # 4-byte Folded Reload
	jb	.LBB0_516
	.p2align	4, 0x90
.LBB0_520:                              # %splice_score_compare.exit.thread.i
                                        #   in Loop: Header=BB0_513 Depth=3
	movl	%ebx, %eax
	addl	%r14d, %eax
	jne	.LBB0_512
# BB#521:                               # %._crit_edge289.loopexit.i
                                        #   in Loop: Header=BB0_525 Depth=2
	movq	32(%rsp), %r14          # 8-byte Reload
	movl	16(%r14), %eax
	jmp	.LBB0_524
.LBB0_522:                              #   in Loop: Header=BB0_525 Depth=2
	shrq	$34, %rdx
	andl	$4194303, %edx          # imm = 0x3FFFFF
	addl	%ebx, %edx
	movl	%edx, %ebx
	jmp	.LBB0_528
.LBB0_523:                              #   in Loop: Header=BB0_525 Depth=2
	xorl	%r13d, %r13d
.LBB0_524:                              # %._crit_edge289.i
                                        #   in Loop: Header=BB0_525 Depth=2
	movl	16(%rsp), %edx          # 4-byte Reload
	testl	%edx, %edx
	movl	$0, %ecx
	cmovgl	%r13d, %ecx
	addl	%ecx, 136(%rsp)         # 4-byte Folded Spill
	movl	%edx, %ecx
	sarl	$31, %ecx
	andl	%r13d, %ecx
	movl	88(%rsp), %ebx          # 4-byte Reload
	addl	%ecx, %ebx
	movq	144(%rsp), %r15         # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	56(%rsp), %r13          # 8-byte Reload
	jmp	.LBB0_529
	.p2align	4, 0x90
.LBB0_525:                              #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_513 Depth 3
	movq	(%rbp), %rcx
	leal	-1(%r12), %edx
	movq	(%rcx,%rdx,8), %rbp
	movl	%r12d, %edx
	movq	(%rcx,%rdx,8), %r15
	movl	12(%rbp), %ecx
	leal	1(%rcx), %edx
	movl	4(%r15), %esi
	cmpl	%esi, %edx
	jb	.LBB0_528
# BB#526:                               #   in Loop: Header=BB0_525 Depth=2
	movq	24(%rbp), %rdx
	movq	%rdx, %rdi
	shlq	$30, %rdi
	sarq	$62, %rdi
	testl	%edi, %edi
	jle	.LBB0_509
# BB#527:                               #   in Loop: Header=BB0_525 Depth=2
	shrq	$34, %rdx
	andl	$4194303, %edx          # imm = 0x3FFFFF
	addl	136(%rsp), %edx         # 4-byte Folded Reload
	movl	%edx, %ecx
	movl	%ecx, 136(%rsp)         # 4-byte Spill
.LBB0_528:                              #   in Loop: Header=BB0_525 Depth=2
	movq	144(%rsp), %r15         # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB0_529:                              #   in Loop: Header=BB0_525 Depth=2
	incl	%r12d
	cmpl	%eax, %r12d
	jb	.LBB0_525
	jmp	.LBB0_531
.LBB0_530:                              #   in Loop: Header=BB0_4 Depth=1
	movl	$0, 136(%rsp)           # 4-byte Folded Spill
.LBB0_531:                              # %._crit_edge297.i502
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpl	%ebx, 136(%rsp)         # 4-byte Folded Reload
	movl	$-1, %ecx
	movl	$1, %edx
	cmovael	%edx, %ecx
	movl	%ecx, (%r13)
	movq	128(%rsp), %r12         # 8-byte Reload
.LBB0_532:                              # %.preheader.i503
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpl	$2, %eax
	jb	.LBB0_554
# BB#533:                               # %.lr.ph280.i
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	$1, %ebx
                                        # implicit-def: %ECX
	movl	%ecx, 16(%rsp)          # 4-byte Spill
                                        # implicit-def: %ECX
	movq	%rcx, 136(%rsp)         # 8-byte Spill
                                        # implicit-def: %ECX
	movl	%ecx, 88(%rsp)          # 4-byte Spill
                                        # implicit-def: %ECX
	movl	%ecx, 176(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB0_534:                              #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_542 Depth 3
	movq	(%rbp), %rcx
	leal	-1(%rbx), %edx
	movq	(%rcx,%rdx,8), %r13
	movl	%ebx, %edx
	movq	(%rcx,%rdx,8), %r14
	movq	24(%r13), %rcx
	movq	%rcx, %rdx
	sarq	$56, %rdx
	testl	%edx, %edx
	js	.LBB0_537
# BB#535:                               #   in Loop: Header=BB0_534 Depth=2
	movq	%rcx, %rdx
	shlq	$30, %rdx
	sarq	$62, %rdx
	movq	56(%rsp), %rsi          # 8-byte Reload
	imull	(%rsi), %edx
	testl	%edx, %edx
	jle	.LBB0_537
# BB#536:                               #   in Loop: Header=BB0_534 Depth=2
	movq	32(%rsp), %r14          # 8-byte Reload
	jmp	.LBB0_553
	.p2align	4, 0x90
.LBB0_537:                              #   in Loop: Header=BB0_534 Depth=2
	movl	12(%r13), %edx
	leal	1(%rdx), %edi
	movl	4(%r14), %esi
	cmpl	%esi, %edi
	jae	.LBB0_539
# BB#538:                               #   in Loop: Header=BB0_534 Depth=2
	movq	32(%rsp), %r14          # 8-byte Reload
	jmp	.LBB0_553
	.p2align	4, 0x90
.LBB0_539:                              #   in Loop: Header=BB0_534 Depth=2
	movl	%edx, %r12d
	subl	%esi, %r12d
	cmpl	$-2, %r12d
	je	.LBB0_551
# BB#540:                               # %.lr.ph.i505
                                        #   in Loop: Header=BB0_534 Depth=2
	notl	%r12d
	movl	$-1, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	jmp	.LBB0_542
	.p2align	4, 0x90
.LBB0_541:                              # %splice_score_compare.exit216.thread._crit_edge.i
                                        #   in Loop: Header=BB0_542 Depth=3
	incl	%ebp
	movl	12(%r13), %edx
.LBB0_542:                              #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_534 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%r12,%rbp), %eax
	addl	8(%r13), %eax
	movl	%eax, 288(%rsp)
	addl	%r12d, %edx
	addl	%ebp, %edx
	movl	%edx, 292(%rsp)
	movl	(%r14), %eax
	addl	%ebp, %eax
	movl	%eax, 296(%rsp)
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %ecx
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	leaq	288(%rsp), %rdx
	callq	compute_max_score
	movl	304(%rsp), %eax
	cmpl	%r15d, %eax
	jb	.LBB0_549
# BB#543:                               #   in Loop: Header=BB0_542 Depth=3
	movl	308(%rsp), %ecx
	jbe	.LBB0_546
# BB#544:                               # %.splice_score_compare.exit216.thread253.i_crit_edge
                                        #   in Loop: Header=BB0_542 Depth=3
	movl	300(%rsp), %edx
.LBB0_545:                              # %splice_score_compare.exit216.thread253.i
                                        #   in Loop: Header=BB0_542 Depth=3
	movl	288(%rsp), %esi
	movl	%esi, 16(%rsp)          # 4-byte Spill
	movl	292(%rsp), %esi
	movq	%rsi, 136(%rsp)         # 8-byte Spill
	movl	296(%rsp), %esi
	movl	%esi, 88(%rsp)          # 4-byte Spill
	movl	312(%rsp), %esi
	movl	%esi, 176(%rsp)         # 4-byte Spill
	movl	%ecx, 48(%rsp)          # 4-byte Spill
	movl	%eax, %r15d
	movl	%edx, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	jmp	.LBB0_549
	.p2align	4, 0x90
.LBB0_546:                              #   in Loop: Header=BB0_542 Depth=3
	cmpl	48(%rsp), %ecx          # 4-byte Folded Reload
	jb	.LBB0_549
# BB#547:                               #   in Loop: Header=BB0_542 Depth=3
	movl	300(%rsp), %edx
	ja	.LBB0_545
# BB#548:                               #   in Loop: Header=BB0_542 Depth=3
	cmpl	40(%rsp), %edx          # 4-byte Folded Reload
	jb	.LBB0_545
	.p2align	4, 0x90
.LBB0_549:                              # %splice_score_compare.exit216.thread.i
                                        #   in Loop: Header=BB0_542 Depth=3
	movl	%r12d, %eax
	addl	%ebp, %eax
	jne	.LBB0_541
# BB#550:                               # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB0_534 Depth=2
	movl	24(%r13), %ecx
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB0_552
.LBB0_551:                              #   in Loop: Header=BB0_534 Depth=2
	movl	$-1, %esi
	xorl	%r15d, %r15d
.LBB0_552:                              # %._crit_edge.i507
                                        #   in Loop: Header=BB0_534 Depth=2
	movl	176(%rsp), %eax         # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	andl	$3, %eax
	shlq	$32, %rax
	movl	%ecx, %ecx
	shlq	$56, %rsi
	movabsq	$12884901888, %rdx      # imm = 0x300000000
	andq	%rdx, %rax
	andl	$4194303, %r15d         # imm = 0x3FFFFF
	shlq	$34, %r15
	orq	%rcx, %rax
	orq	%rsi, %rax
	orq	%r15, %rax
	movq	%rax, 24(%r13)
	movl	16(%rsp), %eax          # 4-byte Reload
	movl	%eax, 8(%r13)
	movq	136(%rsp), %rax         # 8-byte Reload
	movl	%eax, 12(%r13)
	leal	1(%rax), %eax
	movl	%eax, 4(%r14)
	movl	88(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%r14)
	movq	32(%rsp), %r14          # 8-byte Reload
	movl	16(%r14), %eax
	movq	128(%rsp), %r12         # 8-byte Reload
	movq	144(%rsp), %r15         # 8-byte Reload
.LBB0_553:                              #   in Loop: Header=BB0_534 Depth=2
	incl	%ebx
	cmpl	%eax, %ebx
	jb	.LBB0_534
.LBB0_554:                              # %slide_intron.exit
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	8(%r15), %rsi
	movq	16(%r12), %rbp
	movl	16(%r15), %edx
	movl	4148(%r12), %edi
	movq	$0, (%r14)
	leal	1(%rdx), %eax
	movl	%eax, 288(%rsp)
	leal	1(%rdi), %eax
	movl	%eax, 292(%rsp)
	movl	$0, 296(%rsp)
	movl	$0, 300(%rsp)
	movl	16(%r14), %ecx
	decl	%ecx
	js	.LBB0_608
# BB#555:                               # %.lr.ph287.i
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	%rbp, 176(%rsp)         # 8-byte Spill
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	movslq	%ecx, %rsi
	xorl	%ecx, %ecx
	leaq	288(%rsp), %rbp
	movq	%rbp, %r11
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	%rdx, 104(%rsp)         # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%edx, 112(%rsp)         # 4-byte Spill
	movq	%rdi, 152(%rsp)         # 8-byte Spill
	movl	%edi, %edx
	movl	%edx, 120(%rsp)         # 4-byte Spill
	xorl	%r12d, %r12d
	xorl	%r10d, %r10d
	xorl	%r14d, %r14d
	jmp	.LBB0_560
	.p2align	4, 0x90
.LBB0_556:                              #   in Loop: Header=BB0_560 Depth=2
	movq	144(%rsp), %r15         # 8-byte Reload
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	96(%rsp), %rbx          # 8-byte Reload
	movl	208(%rsp), %esi         # 4-byte Reload
.LBB0_557:                              # %._crit_edge.i517
                                        #   in Loop: Header=BB0_560 Depth=2
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %ecx
	movl	$2, %eax
	subl	%esi, %eax
	subl	136(%rsp), %eax         # 4-byte Folded Reload
	addl	%r12d, %eax
	addl	%ecx, %eax
	addl	12(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	mulsd	.LCPI0_6(%rip), %xmm0
	cvttsd2si	%xmm0, %esi
	movl	%esi, %eax
	subl	%r10d, %eax
	imull	$100, %eax, %eax
	cltd
	idivl	%esi
	movl	%eax, 16(%rbx)
	movq	368(%rsp), %rax
	movq	%rdi, (%rax)
	movq	448(%rsp), %r10
	movq	48(%rsp), %rsi          # 8-byte Reload
	decq	%rsi
	testl	%esi, %esi
	jns	.LBB0_559
	jmp	.LBB0_609
.LBB0_558:                              #   in Loop: Header=BB0_560 Depth=2
	movq	%r10, 40(%rsp)          # 8-byte Spill
	jmp	.LBB0_569
	.p2align	4, 0x90
.LBB0_559:                              # %._crit_edge._crit_edge.i
                                        #   in Loop: Header=BB0_560 Depth=2
	movl	4(%rbx), %eax
	movq	%rbx, %r11
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	%edx, %r12d
.LBB0_560:                              #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_591 Depth 3
                                        #         Child Loop BB0_598 Depth 4
                                        #         Child Loop BB0_604 Depth 4
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	(%r13), %rdx
	movq	(%rdx,%rsi,8), %rbp
	movl	12(%rbp), %r9d
	subl	%r9d, %eax
	cmpl	$1, %eax
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	movq	%r11, 24(%rsp)          # 8-byte Spill
	jne	.LBB0_564
# BB#561:                               #   in Loop: Header=BB0_560 Depth=2
	movl	(%r11), %ebx
	leaq	8(%rbp), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	8(%rbp), %r8d
	subl	%r8d, %ebx
	decl	%ebx
	movl	%r12d, 16(%rsp)         # 4-byte Spill
	je	.LBB0_566
# BB#562:                               #   in Loop: Header=BB0_560 Depth=2
	testl	%ecx, %ecx
	je	.LBB0_558
# BB#563:                               #   in Loop: Header=BB0_560 Depth=2
	movq	%r10, %r15
	movl	$16, %edi
	callq	xmalloc
	movq	96(%rsp), %rbp          # 8-byte Reload
	movb	$1, 12(%rax)
	movl	%ebx, 8(%rax)
	movq	%r15, (%rax)
	movl	8(%rbp), %r8d
	movl	12(%rbp), %r9d
	movq	%rax, 40(%rsp)          # 8-byte Spill
	jmp	.LBB0_570
	.p2align	4, 0x90
.LBB0_564:                              #   in Loop: Header=BB0_560 Depth=2
	testl	%ecx, %ecx
	je	.LBB0_567
# BB#565:                               #   in Loop: Header=BB0_560 Depth=2
	movq	%r10, %r15
	movl	$40, %edi
	movq	%r11, %rbx
	callq	xmalloc
	movq	96(%rsp), %rbp          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rcx
	movq	%rcx, (%rax)
	movq	%rax, (%rdx)
	movq	%r15, 8(%rax)
	movl	(%rbx), %ecx
	movl	%ecx, 16(%rax)
	movl	4(%rbx), %edx
	movl	%edx, 20(%rax)
	movl	112(%rsp), %esi         # 4-byte Reload
	incl	%esi
	subl	%ecx, %esi
	movl	%esi, 24(%rax)
	movl	120(%rsp), %ecx         # 4-byte Reload
	incl	%ecx
	subl	%edx, %ecx
	movl	%ecx, 28(%rax)
	movl	%r12d, 32(%rax)
	movl	12(%rbp), %r9d
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	jmp	.LBB0_568
.LBB0_566:                              #   in Loop: Header=BB0_560 Depth=2
	movq	%r10, 40(%rsp)          # 8-byte Spill
	jmp	.LBB0_570
.LBB0_567:                              #   in Loop: Header=BB0_560 Depth=2
	movl	%r12d, 16(%rsp)         # 4-byte Spill
	movq	%r10, 40(%rsp)          # 8-byte Spill
.LBB0_568:                              #   in Loop: Header=BB0_560 Depth=2
	leaq	8(%rbp), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	8(%rbp), %r8d
	movl	%r9d, %eax
	movl	%eax, 120(%rsp)         # 4-byte Spill
.LBB0_569:                              # %.critedge.i511
                                        #   in Loop: Header=BB0_560 Depth=2
	movl	%r8d, 112(%rsp)         # 4-byte Spill
.LBB0_570:                              # %.critedge.i511
                                        #   in Loop: Header=BB0_560 Depth=2
	movl	(%rbp), %edx
	movl	4(%rbp), %ecx
	decl	%edx
	leal	1(%r9), %eax
	subl	%ecx, %eax
	decl	%ecx
	cvtsi2sdq	%rax, %xmm0
	mulsd	.LCPI0_2(%rip), %xmm0
	maxsd	.LCPI0_4(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	subq	$8, %rsp
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	movq	96(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdi
	movq	184(%rsp), %r13         # 8-byte Reload
	movq	%r13, %rsi
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	%rax
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	movq	%rbp, %r15
	callq	align_get_dist
	addq	$16, %rsp
.Lcfi35:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %r10d
	testl	%r10d, %r10d
	leaq	368(%rsp), %rbx
	movq	%rbx, %r11
	leaq	448(%rsp), %rbp
	js	.LBB0_607
# BB#571:                               #   in Loop: Header=BB0_560 Depth=2
	movl	(%r15), %edx
	movl	4(%r15), %ecx
	decl	%edx
	decl	%ecx
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r8d
	movl	12(%r15), %r9d
	subq	$8, %rsp
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rdi
	movq	%r13, %rsi
	pushq	160(%rsp)               # 8-byte Folded Reload
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)               # 8-byte Folded Reload
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	movq	%r10, %rbx
	movq	%r11, %r15
	callq	align_path
	movq	%r15, %rsi
	movq	%rbx, %r15
	addq	$48, %rsp
.Lcfi42:
	.cfi_adjust_cfa_offset -48
	cmpq	$0, 368(%rsp)
	je	.LBB0_607
# BB#572:                               #   in Loop: Header=BB0_560 Depth=2
	movq	%rbp, %rdi
	leaq	472(%rsp), %rdx
	callq	Condense_both_Ends
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 8(%rax)
	je	.LBB0_574
# BB#573:                               #   in Loop: Header=BB0_560 Depth=2
	pxor	%xmm8, %xmm8
	movdqa	.LCPI0_5(%rip), %xmm9   # xmm9 = [1,1,1,1]
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	%r15, %rbp
	movq	%rax, %r15
	jmp	.LBB0_580
	.p2align	4, 0x90
.LBB0_574:                              #   in Loop: Header=BB0_560 Depth=2
	movq	368(%rsp), %rdi
	cmpb	$1, 12(%rdi)
	pxor	%xmm8, %xmm8
	movdqa	.LCPI0_5(%rip), %xmm9   # xmm9 = [1,1,1,1]
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	%r15, %rbp
	jne	.LBB0_579
# BB#575:                               #   in Loop: Header=BB0_560 Depth=2
	movl	8(%rdi), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	subl	%eax, (%rcx)
	movq	40(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	je	.LBB0_578
# BB#576:                               #   in Loop: Header=BB0_560 Depth=2
	cmpb	$1, 12(%rcx)
	jne	.LBB0_578
# BB#577:                               #   in Loop: Header=BB0_560 Depth=2
	addl	%eax, 8(%rcx)
.LBB0_578:                              #   in Loop: Header=BB0_560 Depth=2
	subl	%eax, %ebp
	subl	%eax, 112(%rsp)         # 4-byte Folded Spill
	callq	free
	movq	472(%rsp), %rax
	movq	$0, (%rax)
	movq	472(%rsp), %rax
	movq	%rax, 368(%rsp)
	pxor	%xmm8, %xmm8
	movdqa	.LCPI0_5(%rip), %xmm9   # xmm9 = [1,1,1,1]
	movq	96(%rsp), %rbx          # 8-byte Reload
.LBB0_579:                              #   in Loop: Header=BB0_560 Depth=2
	movq	144(%rsp), %r15         # 8-byte Reload
.LBB0_580:                              #   in Loop: Header=BB0_560 Depth=2
	movq	448(%rsp), %rdi
	testq	%r12, %r12
	jne	.LBB0_586
# BB#581:                               #   in Loop: Header=BB0_560 Depth=2
	testq	%rdi, %rdi
	movl	16(%rsp), %r12d         # 4-byte Reload
	je	.LBB0_587
# BB#582:                               #   in Loop: Header=BB0_560 Depth=2
	cmpb	$1, 12(%rdi)
	jne	.LBB0_589
# BB#583:                               #   in Loop: Header=BB0_560 Depth=2
	movl	8(%rdi), %eax
	addl	%eax, (%rbx)
	movq	(%rdi), %rbx
	cmpq	%rdi, 368(%rsp)
	jne	.LBB0_585
# BB#584:                               #   in Loop: Header=BB0_560 Depth=2
	movq	%rbx, 368(%rsp)
.LBB0_585:                              #   in Loop: Header=BB0_560 Depth=2
	subl	%eax, %ebp
	callq	free
	movq	%rbx, 448(%rsp)
	movq	%rbx, %rdi
	pxor	%xmm8, %xmm8
	movdqa	.LCPI0_5(%rip), %xmm9   # xmm9 = [1,1,1,1]
	movq	96(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB0_587
	.p2align	4, 0x90
.LBB0_586:                              #   in Loop: Header=BB0_560 Depth=2
	movl	16(%rsp), %r12d         # 4-byte Reload
.LBB0_587:                              #   in Loop: Header=BB0_560 Depth=2
	addl	%r12d, %ebp
	movl	(%rbx), %edx
	movl	4(%rbx), %esi
	testq	%rdi, %rdi
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	jne	.LBB0_590
# BB#588:                               #   in Loop: Header=BB0_560 Depth=2
	movl	%edx, 136(%rsp)         # 4-byte Spill
	xorl	%r10d, %r10d
	xorl	%r12d, %r12d
	movq	40(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_557
	.p2align	4, 0x90
.LBB0_589:                              # %.thread309.i
                                        #   in Loop: Header=BB0_560 Depth=2
	addl	%r12d, %ebp
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movl	(%rbx), %edx
	movl	4(%rbx), %esi
.LBB0_590:                              # %.lr.ph274.preheader.i
                                        #   in Loop: Header=BB0_560 Depth=2
	movl	%esi, 208(%rsp)         # 4-byte Spill
	movl	%esi, %eax
	movq	176(%rsp), %rcx         # 8-byte Reload
	leaq	-1(%rcx,%rax), %r11
	movl	%edx, 136(%rsp)         # 4-byte Spill
	movl	%edx, %eax
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	-1(%rcx,%rax), %r15
	xorl	%r12d, %r12d
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB0_591:                              # %.lr.ph274.i
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_560 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_598 Depth 4
                                        #         Child Loop BB0_604 Depth 4
	movsbl	12(%rdi), %edx
	cmpl	$1, %edx
	je	.LBB0_600
# BB#592:                               # %.lr.ph274.i
                                        #   in Loop: Header=BB0_591 Depth=3
	cmpl	$2, %edx
	je	.LBB0_601
# BB#593:                               # %.lr.ph274.i
                                        #   in Loop: Header=BB0_591 Depth=3
	cmpl	$3, %edx
	jne	.LBB0_606
# BB#594:                               # %.preheader.i513
                                        #   in Loop: Header=BB0_591 Depth=3
	movl	8(%rdi), %ebx
	testl	%ebx, %ebx
	jle	.LBB0_606
# BB#595:                               # %.lr.ph.i514.preheader
                                        #   in Loop: Header=BB0_591 Depth=3
	leal	-1(%rbx), %eax
	leaq	1(%r11), %r8
	leaq	1(%rax), %r13
	cmpq	$7, %r13
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jbe	.LBB0_602
# BB#596:                               # %min.iters.checked
                                        #   in Loop: Header=BB0_591 Depth=3
	movq	%r13, %rsi
	movabsq	$8589934584, %rax       # imm = 0x1FFFFFFF8
	andq	%rax, %rsi
	movq	%r13, %r9
	andq	%rax, %r9
	je	.LBB0_602
# BB#597:                               # %vector.ph
                                        #   in Loop: Header=BB0_591 Depth=3
	leaq	(%r11,%rsi), %rbp
	addq	%r15, %rsi
	movd	%r14d, %xmm2
	movd	%r10d, %xmm0
	leaq	4(%r15), %rax
	addq	$4, %r11
	xorpd	%xmm1, %xmm1
	movq	%r9, %rdx
	xorpd	%xmm3, %xmm3
	.p2align	4, 0x90
.LBB0_598:                              # %vector.body
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_560 Depth=2
                                        #       Parent Loop BB0_591 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movd	-4(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3],xmm4[4],xmm8[4],xmm4[5],xmm8[5],xmm4[6],xmm8[6],xmm4[7],xmm8[7]
	punpcklwd	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3]
	movd	(%rax), %xmm5           # xmm5 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1],xmm5[2],xmm8[2],xmm5[3],xmm8[3],xmm5[4],xmm8[4],xmm5[5],xmm8[5],xmm5[6],xmm8[6],xmm5[7],xmm8[7]
	punpcklwd	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1],xmm5[2],xmm8[2],xmm5[3],xmm8[3]
	movd	-4(%r11), %xmm6         # xmm6 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1],xmm6[2],xmm8[2],xmm6[3],xmm8[3],xmm6[4],xmm8[4],xmm6[5],xmm8[5],xmm6[6],xmm8[6],xmm6[7],xmm8[7]
	punpcklwd	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1],xmm6[2],xmm8[2],xmm6[3],xmm8[3]
	movd	(%r11), %xmm7           # xmm7 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm7    # xmm7 = xmm7[0],xmm8[0],xmm7[1],xmm8[1],xmm7[2],xmm8[2],xmm7[3],xmm8[3],xmm7[4],xmm8[4],xmm7[5],xmm8[5],xmm7[6],xmm8[6],xmm7[7],xmm8[7]
	punpcklwd	%xmm8, %xmm7    # xmm7 = xmm7[0],xmm8[0],xmm7[1],xmm8[1],xmm7[2],xmm8[2],xmm7[3],xmm8[3]
	pcmpeqd	%xmm4, %xmm6
	movdqa	%xmm6, %xmm4
	psrld	$31, %xmm4
	pcmpeqd	%xmm5, %xmm7
	movdqa	%xmm7, %xmm5
	psrld	$31, %xmm5
	paddd	%xmm4, %xmm2
	paddd	%xmm5, %xmm3
	pandn	%xmm9, %xmm6
	pandn	%xmm9, %xmm7
	paddd	%xmm6, %xmm0
	paddd	%xmm7, %xmm1
	addq	$8, %rax
	addq	$8, %r11
	addq	$-8, %rdx
	jne	.LBB0_598
# BB#599:                               # %middle.block
                                        #   in Loop: Header=BB0_591 Depth=3
	paddd	%xmm2, %xmm3
	pshufd	$78, %xmm3, %xmm2       # xmm2 = xmm3[2,3,0,1]
	paddd	%xmm3, %xmm2
	pshufd	$229, %xmm2, %xmm3      # xmm3 = xmm2[1,1,2,3]
	paddd	%xmm2, %xmm3
	movd	%xmm3, %r14d
	paddd	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %r10d
	cmpq	%r9, %r13
	jne	.LBB0_603
	jmp	.LBB0_605
	.p2align	4, 0x90
.LBB0_600:                              #   in Loop: Header=BB0_591 Depth=3
	movslq	8(%rdi), %rdx
	addl	%edx, %r12d
	addl	%edx, %r10d
	addq	%rdx, %r15
	jmp	.LBB0_606
	.p2align	4, 0x90
.LBB0_601:                              #   in Loop: Header=BB0_591 Depth=3
	movslq	8(%rdi), %rdx
	addl	%edx, %r12d
	addl	%edx, %r10d
	addq	%rdx, %r11
	jmp	.LBB0_606
.LBB0_602:                              #   in Loop: Header=BB0_591 Depth=3
	xorl	%r9d, %r9d
	movq	%r11, %rbp
	movq	%r15, %rsi
.LBB0_603:                              # %.lr.ph.i514.preheader992
                                        #   in Loop: Header=BB0_591 Depth=3
	subl	%r9d, %ebx
	.p2align	4, 0x90
.LBB0_604:                              # %.lr.ph.i514
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_560 Depth=2
                                        #       Parent Loop BB0_591 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rsi), %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	cmpb	(%rbp), %dl
	sete	%cl
	setne	%al
	addl	%ecx, %r14d
	addl	%eax, %r10d
	incq	%rsi
	incq	%rbp
	decl	%ebx
	jne	.LBB0_604
.LBB0_605:                              # %.loopexit.i516.loopexit
                                        #   in Loop: Header=BB0_591 Depth=3
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	1(%r15,%rax), %r15
	addq	%rax, %r8
	movq	%r8, %r11
	movq	8(%rsp), %r13           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_606:                              # %.loopexit.i516
                                        #   in Loop: Header=BB0_591 Depth=3
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_591
	jmp	.LBB0_556
	.p2align	4, 0x90
.LBB0_607:                              # %pluri_align.exit.thread
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	128(%rsp), %r12         # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	144(%rsp), %r15         # 8-byte Reload
	jmp	.LBB0_616
	.p2align	4, 0x90
.LBB0_608:                              #   in Loop: Header=BB0_4 Depth=1
	xorl	%r14d, %r14d
	xorl	%r10d, %r10d
	xorl	%ecx, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%edi, %ecx
	movl	%ecx, 120(%rsp)         # 4-byte Spill
	leaq	288(%rsp), %rcx
	movq	%rcx, %rbx
	decl	%eax
	jne	.LBB0_610
	jmp	.LBB0_612
.LBB0_609:                              # %._crit_edge288.loopexit.i
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	4(%rbx), %eax
	movl	112(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, %edx
	movq	128(%rsp), %r12         # 8-byte Reload
	movq	152(%rsp), %rdi         # 8-byte Reload
	decl	%eax
	je	.LBB0_612
.LBB0_610:                              # %._crit_edge288.i
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpl	%edi, %eax
	je	.LBB0_612
# BB#611:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%r12, %r13
	movq	%r15, %r12
	movq	%r10, %r15
	movl	$40, %edi
	movq	%rdx, %rbp
	callq	xmalloc
	movq	%r15, %rsi
	movq	%r12, %r15
	movq	%r13, %r12
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rcx
	movq	%rcx, (%rax)
	movq	%rax, (%rdx)
	movl	(%rbx), %ecx
	movl	%ecx, 16(%rax)
	movl	4(%rbx), %edx
	movl	%edx, 20(%rax)
	incl	%ebp
	subl	%ecx, %ebp
	movl	%ebp, 24(%rax)
	movl	120(%rsp), %ecx         # 4-byte Reload
	incl	%ecx
	subl	%edx, %ecx
	jmp	.LBB0_614
.LBB0_612:                              #   in Loop: Header=BB0_4 Depth=1
	cmpl	%edi, %eax
	je	.LBB0_615
# BB#613:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%r12, %r13
	movq	%r15, %r12
	movq	%r10, %r15
	movl	$40, %edi
	movq	%rdx, %rbp
	callq	xmalloc
	movq	%r15, %rsi
	movq	%r12, %r15
	movq	%r13, %r12
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rcx
	movq	%rcx, (%rax)
	movq	%rax, (%rdx)
	movl	(%rbx), %ecx
	movl	%ecx, 16(%rax)
	movl	$1, 20(%rax)
	incl	%ebp
	subl	%ecx, %ebp
	movl	%ebp, 24(%rax)
	movl	120(%rsp), %ecx         # 4-byte Reload
.LBB0_614:                              # %.sink.split.i
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	%ecx, 28(%rax)
	movq	%rsi, 8(%rax)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 32(%rax)
.LBB0_615:                              # %pluri_align.exit
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%r14d, 36(%rax)
	movq	%rax, %r14
	cmpl	$0, options+20(%rip)
	jne	.LBB0_619
.LBB0_616:                              #   in Loop: Header=BB0_4 Depth=1
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB0_618
	.p2align	4, 0x90
.LBB0_617:                              # %.lr.ph.i
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rbp
	movq	8(%rbx), %rdi
	callq	Free_script
	movq	%rbx, %rdi
	callq	free
	testq	%rbp, %rbp
	movq	%rbp, %rbx
	jne	.LBB0_617
.LBB0_618:                              # %free_align.exit
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	$0, (%r14)
.LBB0_619:                              #   in Loop: Header=BB0_4 Depth=1
	movq	528(%rsp), %rcx         # 8-byte Reload
	incl	%ecx
	movq	440(%rsp), %rax         # 8-byte Reload
	cmpl	8(%rax), %ecx
	jb	.LBB0_4
.LBB0_620:                              # %._crit_edge664
	movq	456(%rsp), %rdi
	callq	free
	movq	64(%rsp), %rdi
	callq	free
.LBB0_621:
	addq	$536, %rsp              # imm = 0x218
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	SIM4, .Lfunc_end0-SIM4
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_29
	.quad	.LBB0_30
	.quad	.LBB0_31
	.quad	.LBB0_30
	.quad	.LBB0_30
	.quad	.LBB0_30
	.quad	.LBB0_32
	.quad	.LBB0_30
	.quad	.LBB0_30
	.quad	.LBB0_30
	.quad	.LBB0_30
	.quad	.LBB0_30
	.quad	.LBB0_30
	.quad	.LBB0_30
	.quad	.LBB0_30
	.quad	.LBB0_30
	.quad	.LBB0_30
	.quad	.LBB0_30
	.quad	.LBB0_30
	.quad	.LBB0_33
.LJTI0_1:
	.quad	.LBB0_167
	.quad	.LBB0_168
	.quad	.LBB0_169
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_170
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_171

	.text
	.globl	init_col
	.p2align	4, 0x90
	.type	init_col,@function
init_col:                               # @init_col
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 16
.Lcfi44:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	%esi, 12(%rbx)
	movl	$0, 8(%rbx)
	testl	%esi, %esi
	je	.LBB1_1
# BB#2:
	movl	%esi, %edi
	shlq	$3, %rdi
	callq	xmalloc
	jmp	.LBB1_3
.LBB1_1:
	xorl	%eax, %eax
.LBB1_3:
	movq	%rax, (%rbx)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	init_col, .Lfunc_end1-init_col
	.cfi_endproc

	.p2align	4, 0x90
	.type	exon_cores,@function
exon_cores:                             # @exon_cores
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi51:
	.cfi_def_cfa_offset 256
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movl	%r9d, 108(%rsp)         # 4-byte Spill
	movl	%r8d, 72(%rsp)          # 4-byte Spill
	movl	%ecx, 76(%rsp)          # 4-byte Spill
	movl	%edx, %ebx
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rdi, %r12
	movq	256(%rsp), %r14
	cmpq	$0, 272(%rsp)
	setne	15(%rsp)                # 1-byte Folded Spill
	movl	16(%r12), %eax
	leal	1(%rbx,%rax), %edi
	movl	$4, %esi
	callq	xcalloc
	movq	%rbx, %rcx
	movq	%rax, 112(%rsp)         # 8-byte Spill
	testl	%ecx, %ecx
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	je	.LBB2_15
# BB#1:                                 # %.lr.ph97.i
	movl	16(%r12), %eax
	movq	112(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,4), %rbx
	movl	%ecx, %edx
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rdx, 184(%rsp)         # 8-byte Spill
	leaq	(%rax,%rdx), %rdx
	movq	%rdx, 176(%rsp)         # 8-byte Spill
	xorl	%ebp, %ebp
	movq	%rax, %rdx
	movq	%rbx, 120(%rsp)         # 8-byte Spill
	jmp	.LBB2_3
	.p2align	4, 0x90
.LBB2_14:                               # %.critedge.preheader._crit_edge.i
                                        #   in Loop: Header=BB2_3 Depth=1
	cmpl	%ecx, %ebp
	jb	.LBB2_3
	jmp	.LBB2_15
	.p2align	4, 0x90
.LBB2_2:                                # %.critedge66.i.loopexit398
                                        #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%rdx,%rcx), %rdx
	leal	1(%rbp,%rcx), %ebp
	movq	40(%rsp), %rcx          # 8-byte Reload
.LBB2_3:                                # %.critedge66.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_6 Depth 2
                                        #     Child Loop BB2_11 Depth 2
                                        #       Child Loop BB2_22 Depth 3
                                        #         Child Loop BB2_26 Depth 4
                                        #         Child Loop BB2_31 Depth 4
	movl	$0, 80(%rsp)
	cmpl	%ecx, %ebp
	jae	.LBB2_9
# BB#4:                                 # %.critedge66.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	36(%r12), %r9d
	cmpl	$2, %r9d
	jb	.LBB2_9
# BB#5:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	%ebp, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_6:                                # %.lr.ph.i
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdx,%rcx), %edi
	movl	encoding(,%rdi,4), %edi
	cmpl	$3, %edi
	ja	.LBB2_2
# BB#7:                                 # %.critedge65.i
                                        #   in Loop: Header=BB2_6 Depth=2
	leaq	1(%r8,%rcx), %rax
	leal	(%rdi,%rsi,4), %esi
	movl	%esi, 80(%rsp)
	leaq	1(%rcx), %rdi
	cmpl	40(%rsp), %eax          # 4-byte Folded Reload
	jae	.LBB2_8
# BB#133:                               # %.critedge65.i
                                        #   in Loop: Header=BB2_6 Depth=2
	addl	$2, %ecx
	cmpl	%r9d, %ecx
	movq	%rdi, %rcx
	jb	.LBB2_6
.LBB2_8:                                # %.critedge.preheader.i.loopexit
                                        #   in Loop: Header=BB2_3 Depth=1
	addq	%rdi, %rdx
	addl	%edi, %ebp
	movq	40(%rsp), %rcx          # 8-byte Reload
.LBB2_9:                                # %.critedge.preheader.i
                                        #   in Loop: Header=BB2_3 Depth=1
	cmpl	%ecx, %ebp
	jae	.LBB2_14
# BB#10:                                # %.lr.ph90.i.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	leal	1(%rbp), %eax
	.p2align	4, 0x90
.LBB2_11:                               # %.lr.ph90.i
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_22 Depth 3
                                        #         Child Loop BB2_26 Depth 4
                                        #         Child Loop BB2_31 Depth 4
	movl	%eax, 104(%rsp)         # 4-byte Spill
	cltq
	addq	32(%rsp), %rax          # 8-byte Folded Reload
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movzbl	(%rdx), %eax
	incq	%rdx
	movl	encoding(,%rax,4), %eax
	incl	%ebp
	cmpl	$3, %eax
	ja	.LBB2_3
# BB#12:                                #   in Loop: Header=BB2_11 Depth=2
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	movl	32(%r12), %ecx
	andl	80(%rsp), %ecx
	leal	(%rax,%rcx,4), %eax
	movl	%eax, 80(%rsp)
	movq	(%r12), %rcx
	andl	$524287, %eax           # imm = 0x7FFFF
	leaq	(%rcx,%rax,8), %rsi
	movl	$hash_node_compare, %edx
	leaq	80(%rsp), %rdi
	callq	tfind
	testq	%rax, %rax
	je	.LBB2_13
# BB#20:                                #   in Loop: Header=BB2_11 Depth=2
	movq	(%rax), %rax
	movl	4(%rax), %eax
	testl	%eax, %eax
	js	.LBB2_13
# BB#21:                                # %.lr.ph87.i
                                        #   in Loop: Header=BB2_11 Depth=2
	movslq	%ebp, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rdx, 192(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rdx), %rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movq	%rbp, 128(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_22:                               #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_11 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_26 Depth 4
                                        #         Child Loop BB2_31 Depth 4
	movl	%ebp, %ecx
	subl	%eax, %ecx
	movslq	%ecx, %r11
	cmpl	%eax, (%rbx,%r11,4)
	jle	.LBB2_24
# BB#23:                                # %.extend_hit.exit_crit_edge.i
                                        #   in Loop: Header=BB2_22 Depth=3
	movslq	%eax, %rsi
	jmp	.LBB2_41
	.p2align	4, 0x90
.LBB2_24:                               #   in Loop: Header=BB2_22 Depth=3
	movq	8(%r12), %rcx
	movslq	%eax, %r9
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leaq	(%rcx,%r9), %r15
	xorl	%r14d, %r14d
	movq	192(%rsp), %rax         # 8-byte Reload
	cmpq	184(%rsp), %rax         # 8-byte Folded Reload
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movl	$0, %r13d
	movq	176(%rsp), %rax         # 8-byte Reload
	jge	.LBB2_29
# BB#25:                                # %.lr.ph126.i.i
                                        #   in Loop: Header=BB2_22 Depth=3
	movl	16(%r12), %ecx
	addq	16(%rsp), %rcx          # 8-byte Folded Reload
	movl	options+64(%rip), %r10d
	movl	options+56(%rip), %r8d
	xorl	%edi, %edi
	movl	options+48(%rip), %ebp
	xorl	%r13d, %r13d
	movq	136(%rsp), %rbx         # 8-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rsi
	.p2align	4, 0x90
.LBB2_26:                               #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_11 Depth=2
                                        #       Parent Loop BB2_22 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%r13d, %edx
	subl	%r10d, %edx
	cmpl	%edx, %edi
	jl	.LBB2_29
# BB#27:                                #   in Loop: Header=BB2_26 Depth=4
	cmpq	%rcx, %rsi
	jae	.LBB2_29
# BB#28:                                #   in Loop: Header=BB2_26 Depth=4
	movzbl	(%rbx), %edx
	incq	%rbx
	cmpb	(%rsi), %dl
	leaq	1(%rsi), %rsi
	movl	%ebp, %edx
	cmovel	%r8d, %edx
	addl	%edx, %edi
	cmpl	%r13d, %edi
	cmovgq	%rsi, %r15
	cmovgel	%edi, %r13d
	cmpq	%rax, %rbx
	jb	.LBB2_26
.LBB2_29:                               # %.critedge.i.i
                                        #   in Loop: Header=BB2_22 Depth=3
	movl	36(%r12), %eax
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %r12
	subq	%rax, %r12
	movq	136(%rsp), %rbp         # 8-byte Reload
	subq	%rax, %rbp
	cmpq	32(%rsp), %rbp          # 8-byte Folded Reload
	movq	%r9, 96(%rsp)           # 8-byte Spill
	movq	%r11, 88(%rsp)          # 8-byte Spill
	movq	%rax, 144(%rsp)         # 8-byte Spill
	jbe	.LBB2_34
# BB#30:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB2_22 Depth=3
	movq	%rax, %rsi
	negq	%rsi
	movl	options+64(%rip), %r11d
	movl	options+56(%rip), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movl	options+48(%rip), %r10d
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi), %rcx
	leaq	(%rdx,%rsi), %rdx
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB2_31:                               #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_11 Depth=2
                                        #       Parent Loop BB2_22 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%r14d, %eax
	subl	%r11d, %eax
	cmpl	%eax, %r8d
	jl	.LBB2_34
# BB#32:                                #   in Loop: Header=BB2_31 Depth=4
	leaq	(%rdx,%rdi), %rax
	cmpq	16(%rsp), %rax          # 8-byte Folded Reload
	jbe	.LBB2_34
# BB#33:                                #   in Loop: Header=BB2_31 Depth=4
	leaq	(%rsi,%rdi), %rax
	movq	64(%rsp), %rbx          # 8-byte Reload
	movzbl	-1(%rbx,%rax), %r9d
	movq	24(%rsp), %rbx          # 8-byte Reload
	cmpb	-1(%rbx,%rax), %r9b
	movl	%r10d, %eax
	cmovel	48(%rsp), %eax          # 4-byte Folded Reload
	addl	%eax, %r8d
	leaq	-1(%rdx,%rdi), %rax
	cmpl	%r14d, %r8d
	cmovgq	%rax, %r12
	leaq	-1(%rcx,%rdi), %rax
	cmovgel	%r8d, %r14d
	cmovgq	%rax, %rbp
	decq	%rdi
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB2_31
.LBB2_34:                               # %.critedge2.i.i
                                        #   in Loop: Header=BB2_22 Depth=3
	movq	144(%rsp), %rcx         # 8-byte Reload
	addl	%ecx, %r13d
	addl	%r14d, %r13d
	cmpl	108(%rsp), %r13d        # 4-byte Folded Reload
	jge	.LBB2_36
# BB#35:                                # %.critedge2._crit_edge.i.i
                                        #   in Loop: Header=BB2_22 Depth=3
	movq	256(%rsp), %r14
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	120(%rsp), %rbx         # 8-byte Reload
	movq	128(%rsp), %rbp         # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	jmp	.LBB2_40
	.p2align	4, 0x90
.LBB2_36:                               #   in Loop: Header=BB2_22 Depth=3
	movl	%r12d, %eax
	movq	16(%rsp), %r14          # 8-byte Reload
	subl	%r14d, %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	subl	32(%rsp), %ebp          # 4-byte Folded Reload
	notl	%r14d
	addl	%r15d, %r14d
	leal	(%r15,%rbp), %eax
	notl	%r12d
	addl	%eax, %r12d
	movl	$32, %edi
	callq	xmalloc
	movq	%rax, %rbx
	movl	24(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rbx)
	movl	%ebp, 4(%rbx)
	movl	%r14d, 8(%rbx)
	movl	%r12d, 12(%rbx)
	movq	256(%rsp), %r14
	movl	8(%r14), %ecx
	movl	12(%r14), %esi
	cmpl	%ecx, %esi
	jbe	.LBB2_38
# BB#37:                                # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB2_22 Depth=3
	movq	(%r14), %rax
	jmp	.LBB2_39
.LBB2_38:                               #   in Loop: Header=BB2_22 Depth=3
	addl	$5, %esi
	movl	%esi, 12(%r14)
	movq	(%r14), %rdi
	shlq	$3, %rsi
	callq	xrealloc
	movq	%rax, (%r14)
	movl	8(%r14), %ecx
.LBB2_39:                               # %add_col_elt.exit.i.i
                                        #   in Loop: Header=BB2_22 Depth=3
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	88(%rsp), %rdi          # 8-byte Reload
	leal	1(%rcx), %edx
	movl	%edx, 8(%r14)
	movl	%ecx, %ecx
	movq	%rbx, (%rax,%rcx,8)
	movq	(%r14), %rax
	movq	(%rax,%rcx,8), %rax
	movl	%r13d, 16(%rax)
	movq	8(%r12), %rax
	movl	36(%r12), %ecx
	movq	120(%rsp), %rbx         # 8-byte Reload
	movq	128(%rsp), %rbp         # 8-byte Reload
.LBB2_40:                               #   in Loop: Header=BB2_22 Depth=3
	subl	%eax, %r15d
	addl	%ecx, %r15d
	movl	%r15d, (%rbx,%rdi,4)
.LBB2_41:                               # %extend_hit.exit.i
                                        #   in Loop: Header=BB2_22 Depth=3
	movq	24(%r12), %rax
	movl	(%rax,%rsi,4), %eax
	testl	%eax, %eax
	jns	.LBB2_22
.LBB2_13:                               # %.critedge.backedge.i
                                        #   in Loop: Header=BB2_11 Depth=2
	movl	104(%rsp), %eax         # 4-byte Reload
	incl	%eax
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmpl	%ecx, %ebp
	movq	168(%rsp), %rdx         # 8-byte Reload
	jb	.LBB2_11
	jmp	.LBB2_14
.LBB2_15:                               # %search.exit
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	(%r14), %rdi
	movl	8(%r14), %esi
	movl	$8, %edx
	movl	$msp_rna_compare, %ecx
	callq	qsort
	movl	8(%r14), %eax
	testl	%eax, %eax
	movl	$0, %edx
	je	.LBB2_52
# BB#16:                                # %.lr.ph47.i.preheader
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	movl	%eax, %edx
.LBB2_17:                               # %.lr.ph47.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_18 Depth 2
                                        #       Child Loop BB2_44 Depth 3
                                        #       Child Loop BB2_49 Depth 3
	movl	16(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r15d
	incl	%eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB2_18:                               #   Parent Loop BB2_17 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_44 Depth 3
                                        #       Child Loop BB2_49 Depth 3
	movq	(%r14), %rax
	movq	(%rax,%r15,8), %rsi
	movl	12(%rsi), %ecx
	movl	%ecx, %edi
	subl	4(%rsi), %edi
	cmpl	$50, %edi
	jae	.LBB2_19
# BB#42:                                #   in Loop: Header=BB2_18 Depth=2
	movl	16(%rsp), %esi          # 4-byte Reload
	cmpl	%edx, %esi
	movl	%esi, %ebx
	jae	.LBB2_46
# BB#43:                                # %.lr.ph.i211.preheader
                                        #   in Loop: Header=BB2_18 Depth=2
	addl	$5, %ecx
	movl	16(%rsp), %ebx          # 4-byte Reload
	.p2align	4, 0x90
.LBB2_44:                               # %.lr.ph.i211
                                        #   Parent Loop BB2_17 Depth=1
                                        #     Parent Loop BB2_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ebx, %esi
	movq	(%rax,%rsi,8), %rsi
	cmpl	%ecx, 12(%rsi)
	ja	.LBB2_46
# BB#45:                                #   in Loop: Header=BB2_44 Depth=3
	incl	%ebx
	cmpl	%edx, %ebx
	jb	.LBB2_44
.LBB2_46:                               # %.critedge.i
                                        #   in Loop: Header=BB2_18 Depth=2
	movl	%ebx, %r13d
	subl	%r15d, %r13d
	cmpl	$20, %r13d
	jb	.LBB2_19
# BB#47:                                # %.preheader.i
                                        #   in Loop: Header=BB2_18 Depth=2
	movl	%ebx, %ebp
	cmpl	%r15d, %ebx
	jbe	.LBB2_51
# BB#48:                                # %.lr.ph45.i.preheader
                                        #   in Loop: Header=BB2_18 Depth=2
	movq	%r15, %r12
	.p2align	4, 0x90
.LBB2_49:                               # %.lr.ph45.i
                                        #   Parent Loop BB2_17 Depth=1
                                        #     Parent Loop BB2_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax,%r12,8), %rdi
	callq	free
	incq	%r12
	movq	(%r14), %rax
	cmpq	%r12, %rbp
	jne	.LBB2_49
# BB#50:                                # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB2_18 Depth=2
	movl	8(%r14), %edx
	movq	56(%rsp), %r12          # 8-byte Reload
.LBB2_51:                               # %._crit_edge.i
                                        #   in Loop: Header=BB2_18 Depth=2
	leaq	(%rax,%r15,8), %rdi
	leaq	(%rax,%rbp,8), %rsi
	subl	%ebx, %edx
	shlq	$3, %rdx
	callq	memmove
	movl	8(%r14), %edx
	subl	%r13d, %edx
	movl	%edx, 8(%r14)
	cmpl	%edx, %r15d
	jb	.LBB2_18
	jmp	.LBB2_52
	.p2align	4, 0x90
.LBB2_19:                               # %.outer.backedge.i
                                        #   in Loop: Header=BB2_17 Depth=1
	cmpl	%edx, 16(%rsp)          # 4-byte Folded Reload
	jb	.LBB2_17
.LBB2_52:                               # %trim_small_repeated_msps.exit
	movq	(%r14), %rdi
	movl	%edx, %esi
	movl	$8, %edx
	movl	$msp_compare, %ecx
	callq	qsort
	movl	8(%r14), %eax
	testl	%eax, %eax
	movl	$0, %esi
	je	.LBB2_69
# BB#53:                                # %.lr.ph100.i.preheader
	movl	$1, %ebp
	movl	%eax, %esi
	xorl	%eax, %eax
	jmp	.LBB2_55
	.p2align	4, 0x90
.LBB2_66:                               #   in Loop: Header=BB2_56 Depth=2
	movl	16(%r9), %esi
	movl	16(%rdi), %ecx
	addl	%esi, %ecx
	incl	%eax
	subl	%eax, %ecx
	movl	$0, %eax
	cmovbl	%eax, %ecx
	cmpl	%edx, %r10d
	movq	%r9, %rax
	cmovaq	%rdi, %rax
	movl	(%rax), %eax
	movl	%eax, (%r9)
	cmpl	%ebx, 4(%r9)
	movq	%r9, %rax
	cmovaq	%rdi, %rax
	movl	4(%rax), %eax
	movl	%eax, 4(%r9)
	movl	8(%r9), %eax
	cmpl	8(%rdi), %eax
	movq	%r9, %rax
	cmovbq	%rdi, %rax
	movl	8(%rax), %eax
	movl	%eax, 8(%r9)
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpl	12(%rdi), %eax
	movq	%r9, %rax
	cmovbq	%rdi, %rax
	movl	12(%rax), %eax
	movl	%eax, 12(%r9)
	cmpl	%esi, %ecx
	jbe	.LBB2_68
# BB#67:                                #   in Loop: Header=BB2_56 Depth=2
	movl	%ecx, 16(%r9)
.LBB2_68:                               # %.thread.i
                                        #   in Loop: Header=BB2_56 Depth=2
	decl	%r8d
	movq	256(%rsp), %r14
	movl	%r8d, 8(%r14)
	callq	free
	movq	(%r14), %rdi
	subq	%r12, %rdi
	movl	8(%r14), %edx
	subl	%ebp, %edx
	shlq	$3, %rdx
	leaq	8(%rdi), %rsi
	callq	memmove
	movl	8(%r14), %esi
	movq	48(%rsp), %rdi          # 8-byte Reload
	cmpl	%esi, %edi
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	24(%rsp), %eax          # 4-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	jb	.LBB2_56
	jmp	.LBB2_69
	.p2align	4, 0x90
.LBB2_54:                               # %.lr.ph100.i.backedge
                                        #   in Loop: Header=BB2_55 Depth=1
	movq	16(%rsp), %rbp          # 8-byte Reload
	incl	%ebp
	movq	256(%rsp), %r14
	movq	56(%rsp), %r12          # 8-byte Reload
	movl	24(%rsp), %eax          # 4-byte Reload
.LBB2_55:                               # %.lr.ph100.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_56 Depth 2
                                        #       Child Loop BB2_58 Depth 3
	movl	%ebp, %ebp
	leaq	(,%rbp,8), %rdx
	negq	%rdx
	movl	%eax, %edi
	incl	%eax
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	%rdi, 48(%rsp)          # 8-byte Spill
.LBB2_56:                               #   Parent Loop BB2_55 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_58 Depth 3
	cmpl	%esi, %eax
	jae	.LBB2_69
# BB#57:                                # %.lr.ph.i213
                                        #   in Loop: Header=BB2_56 Depth=2
	movq	(%r14), %r11
	movq	(%r11,%rdi,8), %r9
	movl	(%r9), %r10d
	movl	12(%r9), %eax
	leal	1(%rax), %r15d
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leal	2(%rax), %r14d
	movq	%rdx, %r12
	.p2align	4, 0x90
.LBB2_58:                               #   Parent Loop BB2_55 Depth=1
                                        #     Parent Loop BB2_56 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r11,%rbp,8), %rdi
	movl	(%rdi), %edx
	movl	4(%rdi), %ebx
	movl	%r14d, %eax
	subl	%ebx, %eax
	cmpl	%r15d, %ebx
	movl	$0, %ecx
	cmoval	%ecx, %eax
	cmpl	%r10d, %edx
	jbe	.LBB2_60
# BB#59:                                #   in Loop: Header=BB2_58 Depth=3
	movl	8(%r9), %r13d
	leal	1(%r13), %r8d
	cmpl	%r8d, %edx
	jbe	.LBB2_62
.LBB2_60:                               # %.thr_comm.i
                                        #   in Loop: Header=BB2_58 Depth=3
	testl	%eax, %eax
	jne	.LBB2_64
# BB#61:                                #   in Loop: Header=BB2_58 Depth=3
	xorl	%r13d, %r13d
	jmp	.LBB2_63
	.p2align	4, 0x90
.LBB2_62:                               #   in Loop: Header=BB2_58 Depth=3
	subl	%edx, %r13d
	testl	%eax, %eax
	sete	%r8b
	addl	$2, %r13d
	sete	%cl
	xorb	%r8b, %cl
	jne	.LBB2_64
.LBB2_63:                               #   in Loop: Header=BB2_58 Depth=3
	movl	%esi, %r8d
	movl	%eax, %esi
	subl	%r13d, %esi
	movl	%esi, %ecx
	negl	%ecx
	cmovll	%esi, %ecx
	movl	%r8d, %esi
	cmpl	$11, %ecx
	jl	.LBB2_65
.LBB2_64:                               #   in Loop: Header=BB2_58 Depth=3
	incq	%rbp
	addq	$-8, %r12
	cmpl	%esi, %ebp
	jb	.LBB2_58
	jmp	.LBB2_54
	.p2align	4, 0x90
.LBB2_65:                               #   in Loop: Header=BB2_56 Depth=2
	testl	%eax, %eax
	jne	.LBB2_66
	jmp	.LBB2_54
.LBB2_69:                               # %combine_msps.exit
	cmpq	$0, 272(%rsp)
	je	.LBB2_70
# BB#120:
	movl	%esi, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	link_msps
	movl	%eax, 24(%rsp)          # 4-byte Spill
	xorl	%ecx, %ecx
	movb	15(%rsp), %al           # 1-byte Reload
	movq	272(%rsp), %rbp
	jmp	.LBB2_121
.LBB2_70:                               # %.preheader
	xorl	%edx, %edx
	testl	%esi, %esi
	je	.LBB2_71
# BB#72:                                # %.lr.ph284
	movq	(%r14), %rcx
	movl	%esi, %r8d
	testb	$1, %r8b
	jne	.LBB2_74
# BB#73:
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.LBB2_75
.LBB2_71:
	movl	%esi, %r9d
	xorl	%eax, %eax
	movq	40(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB2_77
.LBB2_74:
	movq	(%rcx), %rax
	movl	4(%rax), %edi
	movl	12(%rax), %eax
	movl	%esi, %ebp
	movq	40(%rsp), %rsi          # 8-byte Reload
	cmpl	%esi, %edi
	cmovbl	%edi, %esi
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movl	%ebp, %esi
	movl	$1, %edi
.LBB2_75:                               # %.prol.loopexit432
	movl	%esi, %r9d
	cmpl	$1, %esi
	movq	40(%rsp), %rsi          # 8-byte Reload
	je	.LBB2_77
	.p2align	4, 0x90
.LBB2_76:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdi,8), %rbp
	movl	4(%rbp), %ebx
	movl	12(%rbp), %ebp
	cmpl	%esi, %ebx
	cmovbl	%ebx, %esi
	cmpl	%eax, %ebp
	cmoval	%ebp, %eax
	movq	8(%rcx,%rdi,8), %rbp
	movl	4(%rbp), %ebx
	movl	12(%rbp), %ebp
	cmpl	%esi, %ebx
	cmovbl	%ebx, %esi
	cmpl	%eax, %ebp
	cmoval	%ebp, %eax
	addq	$2, %rdi
	cmpq	%r8, %rdi
	jb	.LBB2_76
.LBB2_77:                               # %._crit_edge285
	leal	1(%rax), %ecx
	subl	%esi, %ecx
	shrl	$2, %ecx
	cmpl	%ecx, %eax
	movl	$0, %edi
	cmoval	%ecx, %edi
	testl	%r9d, %r9d
	je	.LBB2_80
# BB#78:                                # %.lr.ph278.preheader
	addl	%esi, %ecx
	subl	%edi, %eax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_79:                               # %.lr.ph278
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rdx
	movl	%esi, %edi
	movq	(%rdx,%rdi,8), %rdx
	cmpl	%ecx, 4(%rdx)
	sbbq	%rdi, %rdi
	movq	24(%rdx), %rbp
	andl	$2, %edi
	cmpl	12(%rdx), %eax
	sbbq	%rbx, %rbx
	andl	$1, %ebx
	andq	$-4, %rbp
	orq	%rdi, %rbp
	orq	%rbx, %rbp
	movq	%rbp, 24(%rdx)
	incl	%esi
	movl	8(%r14), %edx
	cmpl	%edx, %esi
	jb	.LBB2_79
.LBB2_80:                               # %._crit_edge279
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	link_msps
	movl	%eax, 24(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	js	.LBB2_132
# BB#81:
	movq	(%r14), %rdi
	movslq	24(%rsp), %rcx          # 4-byte Folded Reload
	movq	(%rdi,%rcx,8), %rcx
	movl	20(%rcx), %r15d
	movl	options+72(%rip), %ecx
	cmpl	$50, %ecx
	jb	.LBB2_83
# BB#82:
	imull	%r15d, %ecx
	imulq	$1374389535, %rcx, %r15 # imm = 0x51EB851F
	shrq	$37, %r15
	cmpl	$2, 8(%r14)
	jae	.LBB2_86
	jmp	.LBB2_85
.LBB2_83:
	movl	%r15d, %ecx
	shrl	$2, %ecx
	subl	%ecx, %r15d
	cmpl	$2, 8(%r14)
	jb	.LBB2_85
.LBB2_86:                               # %.lr.ph270
	movd	72(%rsp), %xmm0         # 4-byte Folded Reload
                                        # xmm0 = mem[0],zero,zero,zero
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movd	76(%rsp), %xmm1         # 4-byte Folded Reload
                                        # xmm1 = mem[0],zero,zero,zero
	pshufd	$80, %xmm1, %xmm1       # xmm1 = xmm1[0,0,1,1]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	xorl	%esi, %esi
	movl	$1, %r14d
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	xorl	%ecx, %ecx
	movdqa	%xmm1, 144(%rsp)        # 16-byte Spill
	.p2align	4, 0x90
.LBB2_87:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_109 Depth 2
                                        #     Child Loop BB2_111 Depth 2
	leal	-1(%r14), %edx
	movq	(%rdi,%rdx,8), %rdx
	movl	%r14d, %eax
	movq	(%rdi,%rax,8), %r13
	movq	24(%rdx), %rax
	movq	%rax, %rdx
	andq	$1, %rdx
	je	.LBB2_89
# BB#88:                                #   in Loop: Header=BB2_87 Depth=1
	testb	$1, 24(%r13)
	je	.LBB2_95
.LBB2_89:                               #   in Loop: Header=BB2_87 Depth=1
	testb	$2, %al
	jne	.LBB2_91
# BB#90:                                #   in Loop: Header=BB2_87 Depth=1
	testb	$2, 24(%r13)
	jne	.LBB2_95
.LBB2_91:                               #   in Loop: Header=BB2_87 Depth=1
	testq	%rdx, %rdx
	jne	.LBB2_93
# BB#92:                                #   in Loop: Header=BB2_87 Depth=1
	movq	256(%rsp), %rdx
	jmp	.LBB2_113
	.p2align	4, 0x90
.LBB2_93:                               #   in Loop: Header=BB2_87 Depth=1
	testb	$2, 24(%r13)
	jne	.LBB2_95
# BB#94:                                #   in Loop: Header=BB2_87 Depth=1
	movq	256(%rsp), %rdx
	jmp	.LBB2_113
	.p2align	4, 0x90
.LBB2_95:                               #   in Loop: Header=BB2_87 Depth=1
	movq	256(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r14d, %edx
	callq	link_msps
	movq	(%rbx), %rdx
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movslq	%eax, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	(%rdx,%rcx,8), %rax
	movl	20(%rax), %ebp
	movl	8(%rbx), %edx
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	link_msps
	movq	(%rbx), %rdi
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movslq	%eax, %rcx
	movq	(%rdi,%rcx,8), %rcx
	movl	20(%rcx), %edx
	movl	options+72(%rip), %ecx
	cmpl	%r15d, %edx
	jb	.LBB2_98
# BB#96:                                #   in Loop: Header=BB2_87 Depth=1
	cmpl	%r15d, %ebp
	jb	.LBB2_98
# BB#97:                                #   in Loop: Header=BB2_87 Depth=1
	cmpl	$49, %ecx
	ja	.LBB2_101
.LBB2_98:                               #   in Loop: Header=BB2_87 Depth=1
	cmpl	%r15d, %edx
	sbbb	%sil, %sil
	cmpl	%r15d, %ebp
	sbbb	%dl, %dl
	cmpl	$49, %ecx
	movl	$1, %ecx
	ja	.LBB2_99
# BB#100:                               #   in Loop: Header=BB2_87 Depth=1
	andb	%sil, %dl
	andb	$1, %dl
	jne	.LBB2_99
.LBB2_101:                              #   in Loop: Header=BB2_87 Depth=1
	movq	%r15, %rbx
	movl	$1, %edi
	movl	$48, %esi
	callq	xcalloc
	movq	%rax, %r15
	movq	264(%rsp), %rbp
	movl	8(%rbp), %ecx
	movl	12(%rbp), %esi
	cmpl	%ecx, %esi
	jbe	.LBB2_103
# BB#102:                               # %._crit_edge.i216
                                        #   in Loop: Header=BB2_87 Depth=1
	movq	(%rbp), %rax
	jmp	.LBB2_104
	.p2align	4, 0x90
.LBB2_103:                              #   in Loop: Header=BB2_87 Depth=1
	addl	$5, %esi
	movl	%esi, 12(%rbp)
	movq	(%rbp), %rdi
	shlq	$3, %rsi
	callq	xrealloc
	movq	%rax, (%rbp)
	movl	8(%rbp), %ecx
.LBB2_104:                              # %add_col_elt.exit
                                        #   in Loop: Header=BB2_87 Depth=1
	leal	1(%rcx), %edx
	movl	%edx, 8(%rbp)
	movl	%ecx, %ecx
	movq	%r15, (%rax,%rcx,8)
	movq	(%rbp), %rax
	movq	(%rax,%rcx,8), %r12
	movl	48(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 24(%r12)
	movl	(%r13), %eax
	subl	%ecx, %eax
	movl	%eax, 28(%r12)
	leaq	8(%r12), %r15
	movl	%r14d, %eax
	subl	16(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 20(%r12)
	movl	$0, 16(%r12)
	je	.LBB2_105
# BB#106:                               #   in Loop: Header=BB2_87 Depth=1
	movl	%eax, %edi
	shlq	$3, %rdi
	callq	xmalloc
	jmp	.LBB2_107
.LBB2_105:                              #   in Loop: Header=BB2_87 Depth=1
	xorl	%eax, %eax
.LBB2_107:                              # %init_col.exit
                                        #   in Loop: Header=BB2_87 Depth=1
	movq	%rax, 8(%r12)
	movq	256(%rsp), %rax
	movq	(%rax), %rdi
	xorl	%ecx, %ecx
	movl	64(%rsp), %esi          # 4-byte Reload
	movq	%r15, %rdx
	callq	msp2exons
	movl	16(%r12), %eax
	testq	%rax, %rax
	movq	56(%rsp), %r12          # 8-byte Reload
	movdqa	144(%rsp), %xmm1        # 16-byte Reload
	je	.LBB2_112
# BB#108:                               # %.lr.ph261
                                        #   in Loop: Header=BB2_87 Depth=1
	movq	(%r15), %rcx
	leaq	-1(%rax), %rsi
	movq	%rax, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB2_110
	.p2align	4, 0x90
.LBB2_109:                              #   Parent Loop BB2_87 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rdx,8), %rbp
	movdqu	(%rbp), %xmm0
	paddd	%xmm1, %xmm0
	movdqu	%xmm0, (%rbp)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB2_109
.LBB2_110:                              # %.prol.loopexit427
                                        #   in Loop: Header=BB2_87 Depth=1
	cmpq	$3, %rsi
	jb	.LBB2_112
	.p2align	4, 0x90
.LBB2_111:                              #   Parent Loop BB2_87 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rdx,8), %rsi
	movdqu	(%rsi), %xmm0
	paddd	%xmm1, %xmm0
	movdqu	%xmm0, (%rsi)
	movq	8(%rcx,%rdx,8), %rsi
	movdqu	(%rsi), %xmm0
	paddd	%xmm1, %xmm0
	movdqu	%xmm0, (%rsi)
	movq	16(%rcx,%rdx,8), %rsi
	movdqu	(%rsi), %xmm0
	paddd	%xmm1, %xmm0
	movdqu	%xmm0, (%rsi)
	movq	24(%rcx,%rdx,8), %rsi
	movdqu	(%rsi), %xmm0
	paddd	%xmm1, %xmm0
	movdqu	%xmm0, (%rsi)
	addq	$4, %rdx
	cmpq	%rax, %rdx
	jb	.LBB2_111
.LBB2_112:                              # %._crit_edge262
                                        #   in Loop: Header=BB2_87 Depth=1
	movq	256(%rsp), %rdx
	movq	(%rdx), %rdi
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rdi,%rax,8), %rcx
	movl	8(%rcx), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	xorl	%ecx, %ecx
	movl	%r14d, %esi
	movq	%rbx, %r15
	jmp	.LBB2_113
.LBB2_99:                               #   in Loop: Header=BB2_87 Depth=1
	movq	256(%rsp), %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_113:                              #   in Loop: Header=BB2_87 Depth=1
	incl	%r14d
	movl	8(%rdx), %edx
	cmpl	%edx, %r14d
	jb	.LBB2_87
# BB#114:                               # %._crit_edge271
	testl	%ecx, %ecx
	je	.LBB2_116
# BB#115:
	movq	256(%rsp), %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	link_msps
	movl	%eax, 24(%rsp)          # 4-byte Spill
	jmp	.LBB2_116
.LBB2_85:
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
.LBB2_116:                              # %._crit_edge271.thread
	movl	$1, %edi
	movl	$48, %esi
	callq	xcalloc
	movq	%rax, %r14
	movq	264(%rsp), %rbp
	movl	8(%rbp), %ecx
	movl	12(%rbp), %esi
	cmpl	%ecx, %esi
	jbe	.LBB2_118
# BB#117:                               # %._crit_edge.i219
	movq	(%rbp), %rax
	jmp	.LBB2_119
.LBB2_118:
	addl	$5, %esi
	movl	%esi, 12(%rbp)
	movq	(%rbp), %rdi
	shlq	$3, %rsi
	callq	xrealloc
	movq	%rax, (%rbp)
	movl	8(%rbp), %ecx
.LBB2_119:                              # %.thread
	leal	1(%rcx), %edx
	movl	%edx, 8(%rbp)
	movl	%ecx, %ecx
	movq	%r14, (%rax,%rcx,8)
	movq	(%rbp), %rax
	movq	(%rax,%rcx,8), %rbp
	movl	48(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 24(%rbp)
	movl	16(%r12), %eax
	subl	%ecx, %eax
	movl	%eax, 28(%rbp)
	addq	$8, %rbp
	movq	256(%rsp), %r14
	xorl	%ecx, %ecx
	movb	15(%rsp), %al           # 1-byte Reload
.LBB2_121:
	movb	%al, %cl
	cmpl	$0, 12(%rbp)
	jne	.LBB2_126
# BB#122:
	movl	8(%r14), %edi
	testq	%rdi, %rdi
	movl	%edi, 12(%rbp)
	movl	$0, 8(%rbp)
	je	.LBB2_123
# BB#124:
	shlq	$3, %rdi
	movl	%ecx, %ebx
	callq	xmalloc
	movl	%ebx, %ecx
	jmp	.LBB2_125
.LBB2_123:
	xorl	%eax, %eax
.LBB2_125:                              # %init_col.exit223
	movq	%rax, (%rbp)
.LBB2_126:
	movq	(%r14), %rdi
	movl	24(%rsp), %esi          # 4-byte Reload
	movq	%rbp, %rdx
	callq	msp2exons
	movl	8(%rbp), %eax
	testq	%rax, %rax
	je	.LBB2_131
# BB#127:                               # %.lr.ph
	movq	(%rbp), %rcx
	movd	72(%rsp), %xmm0         # 4-byte Folded Reload
                                        # xmm0 = mem[0],zero,zero,zero
	pshufd	$80, %xmm0, %xmm1       # xmm1 = xmm0[0,0,1,1]
	movd	76(%rsp), %xmm0         # 4-byte Folded Reload
                                        # xmm0 = mem[0],zero,zero,zero
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	leaq	-1(%rax), %rsi
	movq	%rax, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB2_129
	.p2align	4, 0x90
.LBB2_128:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rbp
	movdqu	(%rbp), %xmm1
	paddd	%xmm0, %xmm1
	movdqu	%xmm1, (%rbp)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB2_128
.LBB2_129:                              # %.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB2_131
	.p2align	4, 0x90
.LBB2_130:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	movdqu	(%rsi), %xmm1
	paddd	%xmm0, %xmm1
	movdqu	%xmm1, (%rsi)
	movq	8(%rcx,%rdx,8), %rsi
	movdqu	(%rsi), %xmm1
	paddd	%xmm0, %xmm1
	movdqu	%xmm1, (%rsi)
	movq	16(%rcx,%rdx,8), %rsi
	movdqu	(%rsi), %xmm1
	paddd	%xmm0, %xmm1
	movdqu	%xmm1, (%rsi)
	movq	24(%rcx,%rdx,8), %rsi
	movdqu	(%rsi), %xmm1
	paddd	%xmm0, %xmm1
	movdqu	%xmm1, (%rsi)
	addq	$4, %rdx
	cmpq	%rax, %rdx
	jb	.LBB2_130
.LBB2_131:                              # %._crit_edge
	movl	$0, 8(%r14)
.LBB2_132:
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	exon_cores, .Lfunc_end2-exon_cores
	.cfi_endproc

	.p2align	4, 0x90
	.type	kill_polyA,@function
kill_polyA:                             # @kill_polyA
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi62:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi64:
	.cfi_def_cfa_offset 80
.Lcfi65:
	.cfi_offset %rbx, -56
.Lcfi66:
	.cfi_offset %r12, -48
.Lcfi67:
	.cfi_offset %r13, -40
.Lcfi68:
	.cfi_offset %r14, -32
.Lcfi69:
	.cfi_offset %r15, -24
.Lcfi70:
	.cfi_offset %rbp, -16
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movl	16(%rdi), %r9d
	testl	%r9d, %r9d
	je	.LBB3_92
# BB#1:                                 # %.lr.ph253
	movq	%rdx, %r12
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	8(%rdi), %r8
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB3_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_4 Depth 2
	movl	%r13d, %eax
	movq	(%r8,%rax,8), %rax
	movl	4(%rax), %r11d
	movl	12(%rax), %r10d
	leal	-1(%r11), %edx
	cmpl	%r10d, %edx
	movl	$0, %ecx
	movl	$0, %r15d
	movl	$0, %esi
	movl	$0, %edi
	movl	$0, %eax
	jae	.LBB3_12
# BB#3:                                 # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	%edx, %edx
	addq	%r12, %rdx
	leal	1(%r10), %ebp
	subl	%r11d, %ebp
	xorl	%eax, %eax
	xorl	%edi, %edi
	xorl	%esi, %esi
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_4:                                # %.lr.ph.i
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdx), %r14d
	addb	$-65, %r14b
	cmpb	$19, %r14b
	ja	.LBB3_10
# BB#5:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB3_4 Depth=2
	movzbl	%r14b, %ebx
	jmpq	*.LJTI3_0(,%rbx,8)
.LBB3_6:                                #   in Loop: Header=BB3_4 Depth=2
	incl	%ecx
	jmp	.LBB3_11
	.p2align	4, 0x90
.LBB3_10:                               #   in Loop: Header=BB3_4 Depth=2
	incl	%eax
	jmp	.LBB3_11
	.p2align	4, 0x90
.LBB3_7:                                #   in Loop: Header=BB3_4 Depth=2
	incl	%r15d
	jmp	.LBB3_11
	.p2align	4, 0x90
.LBB3_8:                                #   in Loop: Header=BB3_4 Depth=2
	incl	%esi
	jmp	.LBB3_11
	.p2align	4, 0x90
.LBB3_9:                                #   in Loop: Header=BB3_4 Depth=2
	incl	%edi
	.p2align	4, 0x90
.LBB3_11:                               #   in Loop: Header=BB3_4 Depth=2
	incq	%rdx
	decl	%ebp
	jne	.LBB3_4
.LBB3_12:                               # %._crit_edge.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	$1, %ebp
	subl	%r11d, %ebp
	addl	%r10d, %ebp
	subl	%eax, %ebp
	leal	(%rcx,%rcx), %eax
	leal	(%rax,%rax,4), %eax
	xorl	%edx, %edx
	divl	%ebp
	cmpl	$29, %ebp
	ja	.LBB3_17
# BB#13:                                #   in Loop: Header=BB3_2 Depth=1
	cmpl	$6, %eax
	ja	.LBB3_21
# BB#14:                                #   in Loop: Header=BB3_2 Depth=1
	addl	%ecx, %esi
	addl	%esi, %esi
	leal	(%rsi,%rsi,4), %eax
	xorl	%edx, %edx
	divl	%ebp
	cmpl	$7, %eax
	ja	.LBB3_21
# BB#15:                                #   in Loop: Header=BB3_2 Depth=1
	leal	(%rdi,%rdi), %eax
	leal	(%rax,%rax,4), %eax
	xorl	%edx, %edx
	divl	%ebp
	cmpl	$6, %eax
	ja	.LBB3_21
# BB#16:                                #   in Loop: Header=BB3_2 Depth=1
	addl	%r15d, %edi
	addl	%edi, %edi
	leal	(%rdi,%rdi,4), %eax
	xorl	%edx, %edx
	divl	%ebp
	cmpl	$7, %eax
	ja	.LBB3_21
	jmp	.LBB3_22
	.p2align	4, 0x90
.LBB3_17:                               #   in Loop: Header=BB3_2 Depth=1
	cmpl	$7, %eax
	ja	.LBB3_21
# BB#18:                                #   in Loop: Header=BB3_2 Depth=1
	addl	%ecx, %esi
	imull	$100, %esi, %eax
	xorl	%edx, %edx
	divl	%ebp
	cmpl	$94, %eax
	ja	.LBB3_21
# BB#19:                                #   in Loop: Header=BB3_2 Depth=1
	leal	(%rdi,%rdi), %eax
	leal	(%rax,%rax,4), %eax
	xorl	%edx, %edx
	divl	%ebp
	cmpl	$7, %eax
	ja	.LBB3_21
# BB#20:                                #   in Loop: Header=BB3_2 Depth=1
	addl	%r15d, %edi
	imull	$100, %edi, %eax
	xorl	%edx, %edx
	divl	%ebp
	cmpl	$95, %eax
	jb	.LBB3_22
	.p2align	4, 0x90
.LBB3_21:                               #   in Loop: Header=BB3_2 Depth=1
	incl	%r13d
	cmpl	%r9d, %r13d
	jb	.LBB3_2
.LBB3_22:                               # %.critedge158
	testl	%r13d, %r13d
	movq	%r12, %r15
	movq	16(%rsp), %r12          # 8-byte Reload
	je	.LBB3_26
# BB#23:                                # %.lr.ph249
	movl	%r13d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_24:                               # =>This Inner Loop Header: Depth=1
	movq	8(%r12), %rax
	movq	(%rax,%rbp,8), %rdi
	callq	free
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB3_24
# BB#25:                                # %._crit_edge250
	movl	16(%r12), %edx
	movq	8(%r12), %rdi
	leaq	(%rdi,%rbx,8), %rsi
	subl	%r13d, %edx
	shlq	$3, %rdx
	callq	memmove
	movl	16(%r12), %r9d
	subl	%r13d, %r9d
	movl	%r9d, 16(%r12)
.LBB3_26:                               # %.preheader
	testl	%r9d, %r9d
	je	.LBB3_92
# BB#27:                                # %.lr.ph242
	movq	8(%r12), %r8
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB3_28:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_30 Depth 2
	movl	%r13d, %eax
	notl	%eax
	addl	%r9d, %eax
	movq	(%r8,%rax,8), %rax
	movl	4(%rax), %r11d
	movl	12(%rax), %r10d
	leal	-1(%r11), %edx
	cmpl	%r10d, %edx
	movl	$0, %ecx
	movl	$0, %r14d
	movl	$0, %esi
	movl	$0, %edi
	movl	$0, %eax
	jae	.LBB3_38
# BB#29:                                # %.lr.ph.preheader.i169
                                        #   in Loop: Header=BB3_28 Depth=1
	movl	%edx, %edx
	addq	%r15, %rdx
	leal	1(%r10), %ebp
	subl	%r11d, %ebp
	xorl	%eax, %eax
	xorl	%edi, %edi
	xorl	%esi, %esi
	xorl	%r14d, %r14d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_30:                               # %.lr.ph.i177
                                        #   Parent Loop BB3_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdx), %ebx
	addb	$-65, %bl
	cmpb	$19, %bl
	ja	.LBB3_36
# BB#31:                                # %.lr.ph.i177
                                        #   in Loop: Header=BB3_30 Depth=2
	movzbl	%bl, %ebx
	jmpq	*.LJTI3_1(,%rbx,8)
.LBB3_32:                               #   in Loop: Header=BB3_30 Depth=2
	incl	%ecx
	jmp	.LBB3_37
	.p2align	4, 0x90
.LBB3_36:                               #   in Loop: Header=BB3_30 Depth=2
	incl	%eax
	jmp	.LBB3_37
	.p2align	4, 0x90
.LBB3_33:                               #   in Loop: Header=BB3_30 Depth=2
	incl	%r14d
	jmp	.LBB3_37
	.p2align	4, 0x90
.LBB3_34:                               #   in Loop: Header=BB3_30 Depth=2
	incl	%esi
	jmp	.LBB3_37
	.p2align	4, 0x90
.LBB3_35:                               #   in Loop: Header=BB3_30 Depth=2
	incl	%edi
	.p2align	4, 0x90
.LBB3_37:                               #   in Loop: Header=BB3_30 Depth=2
	incq	%rdx
	decl	%ebp
	jne	.LBB3_30
.LBB3_38:                               # %._crit_edge.i190
                                        #   in Loop: Header=BB3_28 Depth=1
	movl	$1, %ebp
	subl	%r11d, %ebp
	addl	%r10d, %ebp
	subl	%eax, %ebp
	leal	(%rcx,%rcx), %eax
	leal	(%rax,%rax,4), %eax
	xorl	%edx, %edx
	divl	%ebp
	cmpl	$29, %ebp
	ja	.LBB3_43
# BB#39:                                #   in Loop: Header=BB3_28 Depth=1
	cmpl	$6, %eax
	ja	.LBB3_47
# BB#40:                                #   in Loop: Header=BB3_28 Depth=1
	addl	%ecx, %esi
	addl	%esi, %esi
	leal	(%rsi,%rsi,4), %eax
	xorl	%edx, %edx
	divl	%ebp
	cmpl	$7, %eax
	ja	.LBB3_47
# BB#41:                                #   in Loop: Header=BB3_28 Depth=1
	leal	(%rdi,%rdi), %eax
	leal	(%rax,%rax,4), %eax
	xorl	%edx, %edx
	divl	%ebp
	cmpl	$6, %eax
	ja	.LBB3_47
# BB#42:                                #   in Loop: Header=BB3_28 Depth=1
	addl	%r14d, %edi
	addl	%edi, %edi
	leal	(%rdi,%rdi,4), %eax
	xorl	%edx, %edx
	divl	%ebp
	cmpl	$7, %eax
	ja	.LBB3_47
	jmp	.LBB3_48
	.p2align	4, 0x90
.LBB3_43:                               #   in Loop: Header=BB3_28 Depth=1
	cmpl	$7, %eax
	ja	.LBB3_47
# BB#44:                                #   in Loop: Header=BB3_28 Depth=1
	addl	%ecx, %esi
	imull	$100, %esi, %eax
	xorl	%edx, %edx
	divl	%ebp
	cmpl	$94, %eax
	ja	.LBB3_47
# BB#45:                                #   in Loop: Header=BB3_28 Depth=1
	leal	(%rdi,%rdi), %eax
	leal	(%rax,%rax,4), %eax
	xorl	%edx, %edx
	divl	%ebp
	cmpl	$7, %eax
	ja	.LBB3_47
# BB#46:                                #   in Loop: Header=BB3_28 Depth=1
	addl	%r14d, %edi
	imull	$100, %edi, %eax
	xorl	%edx, %edx
	divl	%ebp
	cmpl	$95, %eax
	jb	.LBB3_48
	.p2align	4, 0x90
.LBB3_47:                               #   in Loop: Header=BB3_28 Depth=1
	incl	%r13d
	cmpl	%r13d, %r9d
	ja	.LBB3_28
.LBB3_48:                               # %.critedge159
	testl	%r13d, %r13d
	je	.LBB3_53
# BB#49:
	movl	%r9d, %ebx
	subl	%r13d, %ebx
	cmpl	%r9d, %ebx
	jae	.LBB3_52
	.p2align	4, 0x90
.LBB3_50:                               # =>This Inner Loop Header: Depth=1
	movq	8(%r12), %rax
	movl	%ebx, %ecx
	movq	(%rax,%rcx,8), %rdi
	callq	free
	incl	%ebx
	movl	16(%r12), %r9d
	cmpl	%r9d, %ebx
	jb	.LBB3_50
.LBB3_52:                               # %._crit_edge239
	subl	%r13d, %r9d
	movl	%r9d, 16(%r12)
.LBB3_53:                               # %thread-pre-split
	testl	%r9d, %r9d
	je	.LBB3_92
# BB#54:
	movq	8(%r12), %rax
	leal	-1(%r9), %ecx
	movq	(%rax,%rcx,8), %r8
	movl	12(%r8), %esi
	movb	(%r15,%rsi), %al
	testb	%al, %al
	je	.LBB3_55
# BB#56:                                # %.lr.ph232.preheader
	addq	%r15, %rsi
	xorl	%ecx, %ecx
	movl	$1, %edx
	xorl	%ebx, %ebx
                                        # implicit-def: %R10D
	xorl	%ebp, %ebp
                                        # implicit-def: %R11D
	.p2align	4, 0x90
.LBB3_57:                               # %.lr.ph232
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$78, %al
	je	.LBB3_61
# BB#58:                                # %.lr.ph232
                                        #   in Loop: Header=BB3_57 Depth=1
	cmpb	$65, %al
	jne	.LBB3_60
# BB#59:                                #   in Loop: Header=BB3_57 Depth=1
	incl	%ebx
	cmpl	%ecx, %ebp
	leal	1(%rcx), %eax
	cmovlel	%ebx, %r11d
	cmovlel	%eax, %ebp
	cmovlel	%edx, %r10d
	movl	%eax, %ecx
	jmp	.LBB3_61
	.p2align	4, 0x90
.LBB3_60:                               #   in Loop: Header=BB3_57 Depth=1
	addl	$-2, %ecx
.LBB3_61:                               #   in Loop: Header=BB3_57 Depth=1
	movl	%ebp, %eax
	subl	%ecx, %eax
	cmpl	$9, %eax
	jg	.LBB3_63
# BB#62:                                #   in Loop: Header=BB3_57 Depth=1
	movzbl	(%rsi,%rdx), %eax
	incq	%rdx
	testb	%al, %al
	jne	.LBB3_57
.LBB3_63:                               # %.critedge
	cmpl	$8, %r11d
	jb	.LBB3_73
# BB#64:                                # %.critedge
	testl	%ebp, %ebp
	jle	.LBB3_73
# BB#65:
	leal	(%r11,%r11), %eax
	leal	(%rax,%rax,4), %eax
	xorl	%edx, %edx
	divl	%r10d
	cmpl	$8, %eax
	jb	.LBB3_73
# BB#66:
	testl	%r10d, %r10d
	movq	8(%rsp), %rcx           # 8-byte Reload
	je	.LBB3_73
# BB#67:
	movl	8(%r8), %eax
	movb	(%rcx,%rax), %bl
	testb	%bl, %bl
	je	.LBB3_73
# BB#68:                                # %.lr.ph220.preheader
	leaq	1(%rcx,%rax), %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_69:                               # %.lr.ph220
                                        # =>This Inner Loop Header: Depth=1
	movl	%esi, %ebp
	leaq	1(%rdx), %rcx
	xorl	%esi, %esi
	cmpb	$65, %bl
	sete	%sil
	addl	%ebp, %esi
	cmpl	%r10d, %ecx
	jae	.LBB3_71
# BB#70:                                # %.lr.ph220
                                        #   in Loop: Header=BB3_69 Depth=1
	movzbl	(%rax,%rdx), %ebx
	testb	%bl, %bl
	movq	%rcx, %rdx
	jne	.LBB3_69
.LBB3_71:
	addl	%esi, %esi
	leal	(%rsi,%rsi,4), %eax
	xorl	%edx, %edx
	divl	%ecx
	cmpl	$7, %eax
	ja	.LBB3_73
# BB#72:
	testl	%r9d, %r9d
	movl	$1, 40(%r12)
	jne	.LBB3_74
	jmp	.LBB3_92
.LBB3_73:
	testl	%r9d, %r9d
	jne	.LBB3_74
	jmp	.LBB3_92
.LBB3_55:
                                        # implicit-def: %R10D
                                        # implicit-def: %R11D
.LBB3_74:                               # %.thread269
	movq	8(%r12), %rax
	movq	(%rax), %r8
	movl	4(%r8), %eax
	leaq	-2(%r15,%rax), %rcx
	cmpq	%r15, %rcx
	jb	.LBB3_92
# BB#75:                                # %.lr.ph210.preheader
	leaq	-3(%r15,%rax), %rax
	xorl	%ecx, %ecx
	movl	$1, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_76:                               # %.lr.ph210
                                        # =>This Inner Loop Header: Depth=1
	movzbl	1(%rax), %ebx
	cmpb	$78, %bl
	je	.LBB3_80
# BB#77:                                # %.lr.ph210
                                        #   in Loop: Header=BB3_76 Depth=1
	cmpb	$84, %bl
	jne	.LBB3_79
# BB#78:                                #   in Loop: Header=BB3_76 Depth=1
	incl	%esi
	cmpl	%ecx, %edi
	leal	1(%rcx), %ecx
	cmovlel	%esi, %r11d
	cmovlel	%ecx, %edi
	cmovlel	%edx, %r10d
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	cmpq	%r15, %rax
	jae	.LBB3_81
	jmp	.LBB3_82
	.p2align	4, 0x90
.LBB3_79:                               #   in Loop: Header=BB3_76 Depth=1
	addl	$-2, %ecx
.LBB3_80:                               #   in Loop: Header=BB3_76 Depth=1
	cmpq	%r15, %rax
	jb	.LBB3_82
.LBB3_81:                               #   in Loop: Header=BB3_76 Depth=1
	movl	%edi, %ebp
	subl	%ecx, %ebp
	decq	%rax
	incl	%edx
	cmpl	$10, %ebp
	jl	.LBB3_76
.LBB3_82:                               # %.critedge1
	cmpl	$8, %r11d
	jb	.LBB3_92
# BB#83:                                # %.critedge1
	testl	%edi, %edi
	jle	.LBB3_92
# BB#84:
	addl	%r11d, %r11d
	leal	(%r11,%r11,4), %eax
	xorl	%edx, %edx
	divl	%r10d
	cmpl	$8, %eax
	jb	.LBB3_92
# BB#85:
	testl	%r10d, %r10d
	je	.LBB3_92
# BB#86:
	movl	(%r8), %eax
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	-2(%rdx,%rax), %rcx
	cmpq	%rdx, %rcx
	jb	.LBB3_92
# BB#87:                                # %.lr.ph.preheader
	movq	8(%rsp), %rdi           # 8-byte Reload
	leaq	-3(%rdi,%rax), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_88:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %esi
	incl	%ecx
	xorl	%edx, %edx
	cmpb	$84, 1(%rax)
	sete	%dl
	addl	%esi, %edx
	cmpl	%r10d, %ecx
	jae	.LBB3_90
# BB#89:                                # %.lr.ph
                                        #   in Loop: Header=BB3_88 Depth=1
	cmpq	%rdi, %rax
	leaq	-1(%rax), %rax
	jae	.LBB3_88
.LBB3_90:
	addl	%edx, %edx
	leal	(%rdx,%rdx,4), %eax
	xorl	%edx, %edx
	divl	%ecx
	cmpl	$7, %eax
	ja	.LBB3_92
# BB#91:
	movl	$1, 44(%r12)
.LBB3_92:                               # %thread-pre-split.thread
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	kill_polyA, .Lfunc_end3-kill_polyA
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_6
	.quad	.LBB3_10
	.quad	.LBB3_7
	.quad	.LBB3_10
	.quad	.LBB3_10
	.quad	.LBB3_10
	.quad	.LBB3_8
	.quad	.LBB3_10
	.quad	.LBB3_10
	.quad	.LBB3_10
	.quad	.LBB3_10
	.quad	.LBB3_10
	.quad	.LBB3_10
	.quad	.LBB3_10
	.quad	.LBB3_10
	.quad	.LBB3_10
	.quad	.LBB3_10
	.quad	.LBB3_10
	.quad	.LBB3_10
	.quad	.LBB3_9
.LJTI3_1:
	.quad	.LBB3_32
	.quad	.LBB3_36
	.quad	.LBB3_33
	.quad	.LBB3_36
	.quad	.LBB3_36
	.quad	.LBB3_36
	.quad	.LBB3_34
	.quad	.LBB3_36
	.quad	.LBB3_36
	.quad	.LBB3_36
	.quad	.LBB3_36
	.quad	.LBB3_36
	.quad	.LBB3_36
	.quad	.LBB3_36
	.quad	.LBB3_36
	.quad	.LBB3_36
	.quad	.LBB3_36
	.quad	.LBB3_36
	.quad	.LBB3_36
	.quad	.LBB3_35

	.text
	.globl	init_hash_env
	.p2align	4, 0x90
	.type	init_hash_env,@function
init_hash_env:                          # @init_hash_env
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 16
.Lcfi72:
	.cfi_offset %rbx, -16
	movl	%ecx, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %rbx
	movl	%esi, 36(%rbx)
	movq	%rdx, 8(%rbx)
	movl	%eax, 16(%rbx)
	leal	-2(%rsi,%rsi), %ecx
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	decl	%edx
	movl	%edx, 32(%rbx)
	leal	1(%rax), %edi
	shlq	$2, %rdi
	callq	xmalloc
	movq	%rax, 24(%rbx)
	movl	$524288, %edi           # imm = 0x80000
	movl	$8, %esi
	callq	xcalloc
	movq	%rax, (%rbx)
	popq	%rbx
	retq
.Lfunc_end4:
	.size	init_hash_env, .Lfunc_end4-init_hash_env
	.cfi_endproc

	.globl	bld_table
	.p2align	4, 0x90
	.type	bld_table,@function
bld_table:                              # @bld_table
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi76:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi77:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi79:
	.cfi_def_cfa_offset 64
.Lcfi80:
	.cfi_offset %rbx, -56
.Lcfi81:
	.cfi_offset %r12, -48
.Lcfi82:
	.cfi_offset %r13, -40
.Lcfi83:
	.cfi_offset %r14, -32
.Lcfi84:
	.cfi_offset %r15, -24
.Lcfi85:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	16(%r14), %eax
	testl	%eax, %eax
	je	.LBB5_3
# BB#1:                                 # %.preheader.lr.ph
	movq	8(%r14), %r13
	xorl	%r12d, %r12d
	jmp	.LBB5_5
.LBB5_10:                               # %.critedge.preheader.loopexit121
                                        #   in Loop: Header=BB5_5 Depth=1
	leal	1(%rsi,%rdx), %r12d
	leaq	1(%rcx,%rdx), %r13
	xorl	%ebp, %ebp
	cmpl	%eax, %r12d
	jb	.LBB5_12
	jmp	.LBB5_2
	.p2align	4, 0x90
.LBB5_9:                                # %.lr.ph..critedge.preheader.loopexit_crit_edge
                                        #   in Loop: Header=BB5_5 Depth=1
	leal	(%rdx,%rsi), %r12d
	addq	%rdx, %rcx
	movq	%rcx, %r13
	cmpl	%eax, %r12d
	jb	.LBB5_12
	jmp	.LBB5_2
	.p2align	4, 0x90
.LBB5_18:                               # %.thread
                                        #   in Loop: Header=BB5_5 Depth=1
	incq	%r13
	movq	24(%r14), %rcx
	movl	%r12d, %edx
	movl	%eax, (%rcx,%rdx,4)
	movl	%r12d, 4(%rbx)
	movl	16(%r14), %eax
.LBB5_11:                               # %.critedge.preheader
                                        #   in Loop: Header=BB5_5 Depth=1
	cmpl	%eax, %r12d
	jae	.LBB5_2
.LBB5_12:                               # %.lr.ph71
                                        #   in Loop: Header=BB5_5 Depth=1
	movzbl	(%r13), %ecx
	movl	encoding(,%rcx,4), %ecx
	incl	%r12d
	cmpl	$3, %ecx
	ja	.LBB5_4
# BB#13:                                #   in Loop: Header=BB5_5 Depth=1
	andl	32(%r14), %ebp
	leal	(%rcx,%rbp,4), %ebp
	movl	$8, %edi
	callq	xmalloc
	movq	%rax, %rbx
	movl	%ebp, (%rbx)
	movq	(%r14), %rax
	movl	%ebp, %ecx
	andl	$524287, %ecx           # imm = 0x7FFFF
	leaq	(%rax,%rcx,8), %rsi
	movl	$hash_node_compare, %edx
	movq	%rbx, %rdi
	callq	tsearch
	movq	%rax, %r15
	cmpq	%rbx, (%r15)
	je	.LBB5_14
# BB#17:                                #   in Loop: Header=BB5_5 Depth=1
	movq	%rbx, %rdi
	callq	free
	movq	(%r15), %rbx
	movl	4(%rbx), %eax
	jmp	.LBB5_18
	.p2align	4, 0x90
.LBB5_14:                               #   in Loop: Header=BB5_5 Depth=1
	movl	$-1, %eax
	jmp	.LBB5_18
	.p2align	4, 0x90
.LBB5_4:                                # %.backedge.outer.loopexit
                                        #   in Loop: Header=BB5_5 Depth=1
	incq	%r13
	jmp	.LBB5_5
	.p2align	4, 0x90
.LBB5_2:                                # %.loopexit
                                        #   in Loop: Header=BB5_5 Depth=1
	cmpl	%eax, %r12d
	jae	.LBB5_3
.LBB5_5:                                # %.backedge.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_7 Depth 2
                                        #       Child Loop BB5_8 Depth 3
	movl	36(%r14), %edi
	xorl	%ebp, %ebp
	cmpl	$2, %edi
	jb	.LBB5_11
.LBB5_7:                                # %.lr.ph.preheader
                                        #   Parent Loop BB5_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_8 Depth 3
	movq	%r13, %rcx
	movl	%r12d, %esi
	xorl	%edx, %edx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_8:                                # %.lr.ph
                                        #   Parent Loop BB5_5 Depth=1
                                        #     Parent Loop BB5_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rsi,%rdx), %ebx
	cmpl	%eax, %ebx
	jae	.LBB5_9
# BB#15:                                #   in Loop: Header=BB5_8 Depth=3
	movzbl	(%rcx,%rdx), %ebx
	movl	encoding(,%rbx,4), %ebx
	cmpl	$3, %ebx
	ja	.LBB5_6
# BB#16:                                #   in Loop: Header=BB5_8 Depth=3
	leal	(%rbx,%rbp,4), %ebp
	incq	%rdx
	leal	1(%rdx), %ebx
	cmpl	%edi, %ebx
	jb	.LBB5_8
	jmp	.LBB5_9
	.p2align	4, 0x90
.LBB5_6:                                # %.backedge.loopexit
                                        #   in Loop: Header=BB5_7 Depth=2
	leal	1(%rsi,%rdx), %r12d
	leaq	1(%rcx,%rdx), %r13
	cmpl	$2, %edi
	jae	.LBB5_7
	jmp	.LBB5_10
.LBB5_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	bld_table, .Lfunc_end5-bld_table
	.cfi_endproc

	.globl	free_hash_env
	.p2align	4, 0x90
	.type	free_hash_env,@function
free_hash_env:                          # @free_hash_env
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi88:
	.cfi_def_cfa_offset 32
.Lcfi89:
	.cfi_offset %rbx, -24
.Lcfi90:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	24(%r14), %rdi
	callq	free
	movq	(%r14), %rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %rdi
	movl	$free, %esi
	callq	tdestroy
	incq	%rbx
	movq	(%r14), %rdi
	cmpq	$524288, %rbx           # imm = 0x80000
	jne	.LBB6_1
# BB#2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.Lfunc_end6:
	.size	free_hash_env, .Lfunc_end6-free_hash_env
	.cfi_endproc

	.p2align	4, 0x90
	.type	merge,@function
merge:                                  # @merge
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi94:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi95:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi97:
	.cfi_def_cfa_offset 64
.Lcfi98:
	.cfi_offset %rbx, -56
.Lcfi99:
	.cfi_offset %r12, -48
.Lcfi100:
	.cfi_offset %r13, -40
.Lcfi101:
	.cfi_offset %r14, -32
.Lcfi102:
	.cfi_offset %r15, -24
.Lcfi103:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r14d
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movl	8(%rbx), %r12d
	testl	%r12d, %r12d
	je	.LBB7_17
# BB#1:
	movl	8(%r15), %edx
	leal	(%rdx,%r12), %eax
	addl	%r14d, %r12d
	cmpl	12(%r15), %eax
	jbe	.LBB7_3
# BB#2:
	movq	%rcx, %rbp
	movl	%eax, 12(%r15)
	movq	(%r15), %rdi
	movl	%eax, %esi
	shlq	$3, %rsi
	callq	xrealloc
	movq	%rax, (%r15)
	movl	8(%r15), %edx
	jmp	.LBB7_4
.LBB7_3:                                # %._crit_edge
	movq	%rcx, %rbp
	movq	(%r15), %rax
.LBB7_4:
	movl	%r12d, %ecx
	leaq	(%rax,%rcx,8), %rdi
	movl	%r14d, %r13d
	leaq	(%rax,%r13,8), %rsi
	shlq	$3, %r13
	subl	%r14d, %edx
	shlq	$3, %rdx
	callq	memmove
	addq	(%r15), %r13
	movq	(%rbx), %rsi
	movl	8(%rbx), %edx
	shlq	$3, %rdx
	movq	%r13, %rdi
	callq	memcpy
	movl	8(%r15), %eax
	addl	8(%rbx), %eax
	movl	%eax, 8(%r15)
	cmpl	%eax, %r12d
	adcl	$0, %r12d
	cmpl	$1, %r14d
	adcl	$0, %r14d
	cmpl	%r12d, %r14d
	movq	%rbp, %r13
	jae	.LBB7_17
# BB#5:                                 # %.lr.ph
	incl	%r13d
	jmp	.LBB7_8
.LBB7_7:                                #   in Loop: Header=BB7_8 Depth=1
	movl	%r14d, %ebx
	jmp	.LBB7_16
	.p2align	4, 0x90
.LBB7_8:                                # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	leal	-1(%r14), %ebx
	movq	(%rax,%rbx,8), %rdi
	movl	%r14d, %ebp
	movq	(%rax,%rbp,8), %rax
	movl	4(%rax), %esi
	movl	4(%rdi), %edx
	cmpl	%edx, %esi
	jbe	.LBB7_14
# BB#9:                                 #   in Loop: Header=BB7_8 Depth=1
	movl	12(%rdi), %r11d
	movl	12(%rax), %r8d
	cmpl	%r8d, %r11d
	jae	.LBB7_13
# BB#10:                                #   in Loop: Header=BB7_8 Depth=1
	movl	(%rax), %r10d
	movl	8(%rdi), %r9d
	leal	31(%r9), %ecx
	cmpl	%ecx, %r10d
	jae	.LBB7_7
# BB#11:                                #   in Loop: Header=BB7_8 Depth=1
	leal	(%r13,%r11), %ecx
	cmpl	%ecx, %esi
	ja	.LBB7_7
# BB#12:                                #   in Loop: Header=BB7_8 Depth=1
	cmpl	%r10d, (%rdi)
	movq	%rdi, %rcx
	cmovaq	%rax, %rcx
	movl	(%rcx), %ecx
	movl	%ecx, (%rdi)
	cmpl	%esi, %edx
	movq	%rdi, %rcx
	cmovaq	%rax, %rcx
	movl	4(%rcx), %ecx
	movl	%ecx, 4(%rdi)
	cmpl	%r9d, 8(%rax)
	movq	%rax, %rcx
	cmovbq	%rdi, %rcx
	movl	8(%rcx), %ecx
	movl	%ecx, 8(%rdi)
	cmpl	%r11d, %r8d
	movq	%rax, %rcx
	cmovbq	%rdi, %rcx
	movl	12(%rcx), %ecx
	movl	%ecx, 12(%rdi)
.LBB7_13:                               #   in Loop: Header=BB7_8 Depth=1
	movq	%rax, %rdi
	callq	free
	movl	8(%r15), %edx
	decl	%edx
	movl	%edx, 8(%r15)
	movq	(%r15), %rax
	leaq	(%rax,%rbp,8), %rdi
	leaq	8(%rax,%rbp,8), %rsi
	subl	%r14d, %edx
	shlq	$3, %rdx
	callq	memmove
	jmp	.LBB7_15
	.p2align	4, 0x90
.LBB7_14:                               #   in Loop: Header=BB7_8 Depth=1
	callq	free
	movq	(%r15), %rax
	leaq	(%rax,%rbp,8), %rsi
	leaq	-8(%rax,%rbp,8), %rdi
	movl	8(%r15), %edx
	subl	%r14d, %edx
	shlq	$3, %rdx
	callq	memmove
	decl	8(%r15)
.LBB7_15:                               #   in Loop: Header=BB7_8 Depth=1
	decl	%r12d
.LBB7_16:                               #   in Loop: Header=BB7_8 Depth=1
	incl	%ebx
	cmpl	%r12d, %ebx
	movl	%ebx, %r14d
	jb	.LBB7_8
.LBB7_17:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	merge, .Lfunc_end7-merge
	.cfi_endproc

	.globl	free_align
	.p2align	4, 0x90
	.type	free_align,@function
free_align:                             # @free_align
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi106:
	.cfi_def_cfa_offset 32
.Lcfi107:
	.cfi_offset %rbx, -24
.Lcfi108:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB8_2
	.p2align	4, 0x90
.LBB8_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %r14
	movq	8(%rbx), %rdi
	callq	Free_script
	movq	%rbx, %rdi
	callq	free
	testq	%r14, %r14
	movq	%r14, %rbx
	jne	.LBB8_1
.LBB8_2:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	free_align, .Lfunc_end8-free_align
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_0:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.text
	.globl	init_encoding
	.p2align	4, 0x90
	.type	init_encoding,@function
init_encoding:                          # @init_encoding
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	movaps	.LCPI9_0(%rip), %xmm0   # xmm0 = [4,4,4,4]
	movaps	%xmm0, encoding(%rip)
	movaps	%xmm0, encoding+16(%rip)
	movaps	%xmm0, encoding+32(%rip)
	movaps	%xmm0, encoding+48(%rip)
	movaps	%xmm0, encoding+64(%rip)
	movaps	%xmm0, encoding+80(%rip)
	movaps	%xmm0, encoding+96(%rip)
	movaps	%xmm0, encoding+112(%rip)
	movaps	%xmm0, encoding+128(%rip)
	movaps	%xmm0, encoding+144(%rip)
	movaps	%xmm0, encoding+160(%rip)
	movaps	%xmm0, encoding+176(%rip)
	movaps	%xmm0, encoding+192(%rip)
	movaps	%xmm0, encoding+208(%rip)
	movaps	%xmm0, encoding+224(%rip)
	movaps	%xmm0, encoding+240(%rip)
	movaps	%xmm0, encoding+256(%rip)
	movaps	%xmm0, encoding+272(%rip)
	movaps	%xmm0, encoding+288(%rip)
	movaps	%xmm0, encoding+304(%rip)
	movaps	%xmm0, encoding+320(%rip)
	movaps	%xmm0, encoding+336(%rip)
	movaps	%xmm0, encoding+352(%rip)
	movaps	%xmm0, encoding+368(%rip)
	movaps	%xmm0, encoding+384(%rip)
	movaps	%xmm0, encoding+400(%rip)
	movaps	%xmm0, encoding+416(%rip)
	movaps	%xmm0, encoding+432(%rip)
	movaps	%xmm0, encoding+448(%rip)
	movaps	%xmm0, encoding+464(%rip)
	movaps	%xmm0, encoding+480(%rip)
	movaps	%xmm0, encoding+496(%rip)
	movl	$0, encoding+260(%rip)
	movl	$1, encoding+268(%rip)
	movl	$2, encoding+284(%rip)
	movl	$3, encoding+336(%rip)
	retq
.Lfunc_end9:
	.size	init_encoding, .Lfunc_end9-init_encoding
	.cfi_endproc

	.globl	print_exons
	.p2align	4, 0x90
	.type	print_exons,@function
print_exons:                            # @print_exons
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi111:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi112:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 48
.Lcfi114:
	.cfi_offset %rbx, -48
.Lcfi115:
	.cfi_offset %r12, -40
.Lcfi116:
	.cfi_offset %r13, -32
.Lcfi117:
	.cfi_offset %r14, -24
.Lcfi118:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movl	8(%r12), %ecx
	decl	%ecx
	movq	(%r12), %rax
	je	.LBB10_1
# BB#2:                                 # %.lr.ph
	testl	%esi, %esi
	movl	$.L.str.2, %edx
	movl	$.L.str.3, %r15d
	cmovgq	%rdx, %r15
	movl	%ecx, %r14d
	je	.LBB10_6
# BB#3:                                 # %.lr.ph.split.preheader
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB10_4:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax,%r13,8), %rdi
	movq	24(%rdi), %rbx
	movq	%rbx, %rax
	sarq	$56, %rax
	movl	options+68(%rip), %edx
	movl	(%rdi), %esi
	addl	%edx, %esi
	addl	8(%rdi), %edx
	movl	4(%rdi), %ecx
	movl	12(%rdi), %r8d
	movl	16(%rdi), %r9d
	testl	%eax, %eax
	js	.LBB10_5
# BB#8:                                 #   in Loop: Header=BB10_4 Depth=1
	movq	options+8(%rip), %rdi
	leaq	(%rdi,%rax,8), %r10
	leaq	2(%rdi,%rax,8), %r11
	shrq	$34, %rbx
	andl	$4194303, %ebx          # imm = 0x3FFFFF
	movl	$.L.str.1, %edi
	movl	$0, %eax
	pushq	%rbx
.Lcfi119:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi120:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi121:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi122:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$32, %rsp
.Lcfi123:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB10_9
	.p2align	4, 0x90
.LBB10_5:                               #   in Loop: Header=BB10_4 Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_9:                               #   in Loop: Header=BB10_4 Depth=1
	incq	%r13
	movq	(%r12), %rax
	cmpq	%r13, %r14
	jne	.LBB10_4
	jmp	.LBB10_10
.LBB10_1:
	xorl	%r14d, %r14d
	jmp	.LBB10_10
.LBB10_6:                               # %.lr.ph.split.us.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB10_7:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rbx,8), %rax
	movl	options+68(%rip), %edx
	movl	(%rax), %esi
	addl	%edx, %esi
	addl	8(%rax), %edx
	movl	4(%rax), %ecx
	movl	12(%rax), %r8d
	movl	16(%rax), %r9d
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	movq	(%r12), %rax
	cmpq	%rbx, %r14
	jne	.LBB10_7
.LBB10_10:                              # %._crit_edge
	movq	(%rax,%r14,8), %rax
	movl	options+68(%rip), %edx
	movl	(%rax), %esi
	addl	%edx, %esi
	addl	8(%rax), %edx
	movl	4(%rax), %ecx
	movl	12(%rax), %r8d
	movl	16(%rax), %r9d
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	printf                  # TAILCALL
.Lfunc_end10:
	.size	print_exons, .Lfunc_end10-print_exons
	.cfi_endproc

	.p2align	4, 0x90
	.type	msp_rna_compare,@function
msp_rna_compare:                        # @msp_rna_compare
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rcx
	movq	(%rsi), %rdx
	movl	4(%rdx), %esi
	movl	$1, %eax
	cmpl	%esi, 4(%rcx)
	ja	.LBB11_4
# BB#1:
	movl	$-1, %eax
	jb	.LBB11_4
# BB#2:
	movl	12(%rdx), %edx
	cmpl	%edx, 12(%rcx)
	ja	.LBB11_4
# BB#3:
	sbbl	%eax, %eax
	andl	$1, %eax
.LBB11_4:
	retq
.Lfunc_end11:
	.size	msp_rna_compare, .Lfunc_end11-msp_rna_compare
	.cfi_endproc

	.p2align	4, 0x90
	.type	msp_compare,@function
msp_compare:                            # @msp_compare
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rcx
	movq	(%rsi), %rdx
	movl	(%rdx), %esi
	movl	$1, %eax
	cmpl	%esi, (%rcx)
	ja	.LBB12_4
# BB#1:
	movl	$-1, %eax
	jb	.LBB12_4
# BB#2:
	movl	4(%rdx), %eax
	cmpl	%eax, 4(%rcx)
	movl	$1, %eax
	ja	.LBB12_4
# BB#3:
	sbbl	%eax, %eax
.LBB12_4:
	retq
.Lfunc_end12:
	.size	msp_compare, .Lfunc_end12-msp_compare
	.cfi_endproc

	.p2align	4, 0x90
	.type	link_msps,@function
link_msps:                              # @link_msps
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi124:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi125:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi126:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi127:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi128:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi129:
	.cfi_def_cfa_offset 56
.Lcfi130:
	.cfi_offset %rbx, -56
.Lcfi131:
	.cfi_offset %r12, -48
.Lcfi132:
	.cfi_offset %r13, -40
.Lcfi133:
	.cfi_offset %r14, -32
.Lcfi134:
	.cfi_offset %r15, -24
.Lcfi135:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	$-1, %eax
	movl	%edx, %ecx
	subl	%esi, %ecx
	jbe	.LBB13_29
# BB#1:                                 # %.lr.ph69
	movl	$4294967292, %eax       # imm = 0xFFFFFFFC
	movl	%esi, %r10d
	leal	-1(%rdx), %ebp
	testb	$1, %cl
	movq	%r10, %rcx
	je	.LBB13_3
# BB#2:
	movq	(%rdi), %rcx
	movq	(%rcx,%r10,8), %rcx
	movl	$0, 20(%rcx)
	orq	%rax, 24(%rcx)
	leaq	1(%r10), %rcx
.LBB13_3:                               # %.prol.loopexit
	cmpl	%esi, %ebp
	je	.LBB13_4
	.p2align	4, 0x90
.LBB13_5:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rsi
	movq	(%rsi,%rcx,8), %rsi
	movl	$0, 20(%rsi)
	orq	%rax, 24(%rsi)
	movq	(%rdi), %rsi
	movq	8(%rsi,%rcx,8), %rsi
	movl	$0, 20(%rsi)
	orq	%rax, 24(%rsi)
	addq	$2, %rcx
	cmpl	%edx, %ecx
	jb	.LBB13_5
.LBB13_4:                               # %.lr.ph67
	movl	%edx, %ebx
	addl	$-2, %edx
	xorl	%r11d, %r11d
	xorl	%eax, %eax
	movq	%rbx, -16(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB13_7:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_9 Depth 2
	movl	%eax, %ecx
	movl	%r11d, %ebp
	movq	(%rdi), %rsi
	movq	(%rsi,%r10,8), %r15
	movl	20(%r15), %r11d
	addl	16(%r15), %r11d
	movl	%r11d, 20(%r15)
	cmpl	%ebp, %r11d
	movl	%r10d, %eax
	cmovbel	%ecx, %eax
	cmovbel	%ebp, %r11d
	leaq	1(%r10), %rcx
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	cmpq	%rbx, %rcx
	jae	.LBB13_6
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB13_7 Depth=1
	movq	%r10, %r14
	andq	$1073741823, %r14       # imm = 0x3FFFFFFF
	shlq	$2, %r14
	jmp	.LBB13_9
	.p2align	4, 0x90
.LBB13_28:                              # %lies_after_p.exit.thread._crit_edge
                                        #   in Loop: Header=BB13_9 Depth=2
	movq	(%rdi), %rsi
	incq	%r10
.LBB13_9:                               #   Parent Loop BB13_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rsi,%r10,8), %rbx
	movl	(%rbx), %esi
	movl	4(%rbx), %r13d
	movl	8(%r15), %ecx
	movl	12(%r15), %r8d
	movl	%ecx, %ebp
	subl	%esi, %ebp
	jae	.LBB13_17
# BB#10:                                #   in Loop: Header=BB13_9 Depth=2
	movl	%r8d, %ebp
	subl	%r13d, %ebp
	jae	.LBB13_11
.LBB13_24:                              # %lies_after_p.exit
                                        #   in Loop: Header=BB13_9 Depth=2
	movl	20(%r15), %r12d
	cmpl	20(%rbx), %r12d
	jb	.LBB13_27
# BB#25:                                #   in Loop: Header=BB13_9 Depth=2
	subl	(%r15), %esi
	movl	%esi, %ecx
	negl	%ecx
	cmovll	%esi, %ecx
	sarl	$15, %ecx
	subl	4(%r15), %r13d
	movl	%r13d, %esi
	negl	%esi
	cmovll	%r13d, %esi
	sarl	$15, %esi
	addl	%ecx, %esi
	subl	%esi, %r12d
	jbe	.LBB13_27
# BB#26:                                #   in Loop: Header=BB13_9 Depth=2
	movl	%r12d, 20(%rbx)
	movq	24(%rbx), %rcx
	movabsq	$-4294967293, %rsi      # imm = 0xFFFFFFFF00000003
	andq	%rsi, %rcx
	orq	%r14, %rcx
	movq	%rcx, 24(%rbx)
	cmpl	%r10d, %edx
	jne	.LBB13_28
	jmp	.LBB13_6
	.p2align	4, 0x90
.LBB13_17:                              #   in Loop: Header=BB13_9 Depth=2
	cmpl	%r13d, %r8d
	jae	.LBB13_27
# BB#18:                                #   in Loop: Header=BB13_9 Depth=2
	movl	%esi, %r12d
	subl	(%r15), %r12d
	jb	.LBB13_27
# BB#19:                                #   in Loop: Header=BB13_9 Depth=2
	movl	8(%rbx), %r8d
	subl	%ecx, %r8d
	jb	.LBB13_27
# BB#20:                                #   in Loop: Header=BB13_9 Depth=2
	cmpl	%ebp, %r12d
	jbe	.LBB13_27
# BB#21:                                #   in Loop: Header=BB13_9 Depth=2
	cmpl	%ebp, %r8d
	jbe	.LBB13_27
# BB#22:                                #   in Loop: Header=BB13_9 Depth=2
	movl	options+40(%rip), %ecx
	cmpl	%ecx, %r12d
	jbe	.LBB13_27
# BB#23:                                #   in Loop: Header=BB13_9 Depth=2
	cmpl	%ecx, %r8d
	ja	.LBB13_24
	jmp	.LBB13_27
	.p2align	4, 0x90
.LBB13_11:                              #   in Loop: Header=BB13_9 Depth=2
	movl	%r13d, %r12d
	subl	4(%r15), %r12d
	jb	.LBB13_27
# BB#12:                                #   in Loop: Header=BB13_9 Depth=2
	movl	12(%rbx), %r9d
	subl	%r8d, %r9d
	jae	.LBB13_13
	.p2align	4, 0x90
.LBB13_27:                              # %lies_after_p.exit.thread
                                        #   in Loop: Header=BB13_9 Depth=2
	cmpl	%r10d, %edx
	jne	.LBB13_28
	jmp	.LBB13_6
.LBB13_13:                              #   in Loop: Header=BB13_9 Depth=2
	cmpl	%ebp, %r12d
	jbe	.LBB13_27
# BB#14:                                #   in Loop: Header=BB13_9 Depth=2
	cmpl	%ebp, %r9d
	jbe	.LBB13_27
# BB#15:                                #   in Loop: Header=BB13_9 Depth=2
	movl	options+40(%rip), %ecx
	cmpl	%ecx, %r12d
	jbe	.LBB13_27
# BB#16:                                #   in Loop: Header=BB13_9 Depth=2
	cmpl	%ecx, %r9d
	ja	.LBB13_24
	jmp	.LBB13_27
	.p2align	4, 0x90
.LBB13_6:                               # %.loopexit
                                        #   in Loop: Header=BB13_7 Depth=1
	movq	-8(%rsp), %r10          # 8-byte Reload
	movq	-16(%rsp), %rbx         # 8-byte Reload
	cmpq	%rbx, %r10
	jne	.LBB13_7
.LBB13_29:                              # %.loopexit61
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	link_msps, .Lfunc_end13-link_msps
	.cfi_endproc

	.p2align	4, 0x90
	.type	msp2exons,@function
msp2exons:                              # @msp2exons
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi136:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi137:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi138:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi139:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi140:
	.cfi_def_cfa_offset 48
.Lcfi141:
	.cfi_offset %rbx, -40
.Lcfi142:
	.cfi_offset %r12, -32
.Lcfi143:
	.cfi_offset %r14, -24
.Lcfi144:
	.cfi_offset %r15, -16
	movq	%rdx, %r12
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r14
	testl	%esi, %esi
	js	.LBB14_16
# BB#1:                                 # %.lr.ph71
	testl	%ecx, %ecx
	je	.LBB14_5
# BB#2:                                 # %.lr.ph71.split.preheader
	movl	8(%r12), %ecx
	.p2align	4, 0x90
.LBB14_3:                               # %.lr.ph71.split
                                        # =>This Inner Loop Header: Depth=1
	movslq	%esi, %rax
	movq	(%r14,%rax,8), %rbx
	movl	12(%r12), %esi
	cmpl	%ecx, %esi
	jbe	.LBB14_14
# BB#4:                                 # %._crit_edge.i
                                        #   in Loop: Header=BB14_3 Depth=1
	movq	(%r12), %rax
	jmp	.LBB14_15
	.p2align	4, 0x90
.LBB14_14:                              #   in Loop: Header=BB14_3 Depth=1
	addl	$5, %esi
	movl	%esi, 12(%r12)
	movq	(%r12), %rdi
	shlq	$3, %rsi
	callq	xrealloc
	movq	%rax, (%r12)
	movl	8(%r12), %ecx
.LBB14_15:                              # %add_col_elt.exit
                                        #   in Loop: Header=BB14_3 Depth=1
	movl	%ecx, %edx
	leal	1(%rcx), %ecx
	movl	%ecx, 8(%r12)
	movq	%rbx, (%rax,%rdx,8)
	movslq	24(%rbx), %rsi
	shrq	$2, %rsi
	testl	%esi, %esi
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	jns	.LBB14_3
	jmp	.LBB14_16
	.p2align	4, 0x90
.LBB14_5:                               # %.lr.ph71.split.us
                                        # =>This Inner Loop Header: Depth=1
	movslq	%esi, %rax
	movq	(%r14,%rax,8), %r15
	movl	8(%r12), %ecx
	testl	%ecx, %ecx
	je	.LBB14_9
# BB#6:                                 #   in Loop: Header=BB14_5 Depth=1
	movq	(%r12), %rax
	leal	-1(%rcx), %edx
	movq	(%rax,%rdx,8), %rax
	movl	(%rax), %edx
	movl	8(%r15), %edi
	leal	30(%rdi), %esi
	cmpl	%esi, %edx
	jae	.LBB14_9
# BB#7:                                 #   in Loop: Header=BB14_5 Depth=1
	movl	4(%rax), %r8d
	movl	12(%r15), %ebx
	leal	-1(%rbx), %esi
	cmpl	%esi, %r8d
	jbe	.LBB14_9
# BB#8:                                 #   in Loop: Header=BB14_5 Depth=1
	cmpl	%edi, 8(%rax)
	movq	%rax, %rcx
	cmovbq	%r15, %rcx
	movl	8(%rcx), %ecx
	movl	%ecx, 8(%rax)
	cmpl	%ebx, 12(%rax)
	movq	%rax, %rcx
	cmovbq	%r15, %rcx
	movl	12(%rcx), %ecx
	movl	%ecx, 12(%rax)
	cmpl	(%r15), %edx
	movq	%rax, %rcx
	cmovaq	%r15, %rcx
	movl	(%rcx), %ecx
	movl	%ecx, (%rax)
	cmpl	4(%r15), %r8d
	movq	%rax, %rcx
	cmovaq	%r15, %rcx
	movl	4(%rcx), %ecx
	movl	%ecx, 4(%rax)
	movq	24(%r15), %rbx
	movq	%r15, %rdi
	callq	free
	jmp	.LBB14_13
	.p2align	4, 0x90
.LBB14_9:                               # %.thread.us
                                        #   in Loop: Header=BB14_5 Depth=1
	movl	12(%r12), %esi
	cmpl	%ecx, %esi
	jbe	.LBB14_10
# BB#11:                                # %._crit_edge.i.us
                                        #   in Loop: Header=BB14_5 Depth=1
	movq	(%r12), %rax
	jmp	.LBB14_12
	.p2align	4, 0x90
.LBB14_10:                              #   in Loop: Header=BB14_5 Depth=1
	addl	$5, %esi
	movl	%esi, 12(%r12)
	movq	(%r12), %rdi
	shlq	$3, %rsi
	callq	xrealloc
	movq	%rax, (%r12)
	movl	8(%r12), %ecx
.LBB14_12:                              # %add_col_elt.exit.us
                                        #   in Loop: Header=BB14_5 Depth=1
	leal	1(%rcx), %edx
	movl	%edx, 8(%r12)
	movl	%ecx, %ecx
	movq	%r15, (%rax,%rcx,8)
	movq	24(%r15), %rbx
.LBB14_13:                              # %.backedge.us
                                        #   in Loop: Header=BB14_5 Depth=1
	movslq	%ebx, %rsi
	shrq	$2, %rsi
	testl	%esi, %esi
	jns	.LBB14_5
.LBB14_16:                              # %._crit_edge
	movl	8(%r12), %eax
	cmpl	$2, %eax
	jb	.LBB14_20
# BB#17:                                # %.preheader
	decl	%eax
	je	.LBB14_20
# BB#18:                                # %.lr.ph
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB14_19:                              # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rdx
	movl	%ecx, %esi
	movq	(%rdx,%rsi,8), %r8
	movl	%eax, %ebx
	movq	(%rdx,%rbx,8), %rdi
	movq	%rdi, (%rdx,%rsi,8)
	movq	(%r12), %rdx
	movq	%r8, (%rdx,%rbx,8)
	incl	%ecx
	decl	%eax
	cmpl	%ecx, %eax
	ja	.LBB14_19
.LBB14_20:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	msp2exons, .Lfunc_end14-msp2exons
	.cfi_endproc

	.p2align	4, 0x90
	.type	hash_node_compare,@function
hash_node_compare:                      # @hash_node_compare
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	xorl	%ecx, %ecx
	cmpl	(%rsi), %eax
	seta	%cl
	movl	$-1, %eax
	cmovael	%ecx, %eax
	retq
.Lfunc_end15:
	.size	hash_node_compare, .Lfunc_end15-hash_node_compare
	.cfi_endproc

	.p2align	4, 0x90
	.type	perfect_spl_p,@function
perfect_spl_p:                          # @perfect_spl_p
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi145:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi146:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi147:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi148:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi149:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi150:
	.cfi_def_cfa_offset 56
.Lcfi151:
	.cfi_offset %rbx, -56
.Lcfi152:
	.cfi_offset %r12, -48
.Lcfi153:
	.cfi_offset %r13, -40
.Lcfi154:
	.cfi_offset %r14, -32
.Lcfi155:
	.cfi_offset %r15, -24
.Lcfi156:
	.cfi_offset %rbp, -16
	movl	(%rdx), %r10d
	movl	4(%rdx), %ebp
	addq	%rdi, %r10
	movl	options+44(%rip), %r8d
	movq	%r10, %rax
	subq	%r8, %rax
	leaq	(%rsi,%rbp), %r14
	movq	%r14, %rcx
	subq	%r8, %rcx
	movl	$0, -20(%rsp)
	movl	$0, -28(%rsp)
	movb	(%rax), %r12b
	movb	(%rcx), %r11b
	xorl	%ecx, %ecx
	cmpb	%r11b, %r12b
	sete	%cl
	cmpq	$2, %r8
	movl	%ecx, -24(%rsp)
	jb	.LBB16_4
# BB#1:                                 # %.lr.ph.i.preheader
	movq	%rbp, -16(%rsp)         # 8-byte Spill
	movq	%rdx, -8(%rsp)          # 8-byte Spill
	movl	$1, %r9d
	subq	%r8, %r9
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB16_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%r9), %r15d
	xorl	%ebp, %ebp
	cmpb	%r15b, %r12b
	sete	%bpl
	addl	%eax, %ebp
	cmpl	%ecx, %ebp
	cmovll	%ecx, %ebp
	movl	%ebp, -28(%rsp)
	movzbl	(%r10,%r9), %r12d
	xorl	%eax, %eax
	cmpb	%r11b, %r12b
	sete	%al
	addl	%r13d, %eax
	cmpl	%ecx, %eax
	cmovll	%ecx, %eax
	movl	%eax, -20(%rsp)
	xorl	%ebx, %ebx
	cmpl	%eax, %ebp
	setl	%bl
	movl	-28(%rsp,%rbx,8), %ebx
	xorl	%edx, %edx
	cmpb	%r15b, %r12b
	sete	%dl
	addl	%ecx, %edx
	leal	-1(%rbx), %ecx
	cmpl	%edx, %ebx
	cmovlel	%edx, %ecx
	incq	%r9
	movl	%r15d, %r11d
	movl	%eax, %r13d
	movl	%ebp, %eax
	jne	.LBB16_2
# BB#3:                                 # %._crit_edge.i
	movl	%ecx, -24(%rsp)
	movq	-8(%rsp), %rdx          # 8-byte Reload
	movq	-16(%rsp), %rbp         # 8-byte Reload
.LBB16_4:                               # %SWscore.exit
	xorl	%eax, %eax
	cmpl	%r8d, %ecx
	jb	.LBB16_19
# BB#5:
	movl	8(%rdx), %ecx
	leaq	(%rdi,%rcx), %rbx
	movl	$0, -20(%rsp)
	movl	$0, -28(%rsp)
	movb	-1(%rdi,%rcx), %r12b
	movb	(%r14), %r15b
	xorl	%ecx, %ecx
	cmpb	%r15b, %r12b
	sete	%cl
	movl	%ecx, -24(%rsp)
	cmpl	$2, %r8d
	jb	.LBB16_9
# BB#6:                                 # %.lr.ph.i34.preheader
	movq	%rdx, -8(%rsp)          # 8-byte Spill
	leaq	-1(%r8), %r14
	leaq	1(%rsi,%rbp), %r11
	xorl	%r13d, %r13d
	movq	%rbx, -16(%rsp)         # 8-byte Spill
	movq	%rbx, %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB16_7:                               # %.lr.ph.i34
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r11), %r9d
	xorl	%ebx, %ebx
	cmpb	%r9b, %r12b
	sete	%bl
	addl	%ebp, %ebx
	cmpl	%ecx, %ebx
	cmovll	%ecx, %ebx
	movl	%ebx, -28(%rsp)
	movzbl	(%rdi), %r12d
	xorl	%ebp, %ebp
	cmpb	%r15b, %r12b
	sete	%bpl
	addl	%r13d, %ebp
	cmpl	%ecx, %ebp
	cmovll	%ecx, %ebp
	movl	%ebp, -20(%rsp)
	xorl	%edx, %edx
	cmpl	%ebp, %ebx
	setl	%dl
	movl	-28(%rsp,%rdx,8), %edx
	xorl	%esi, %esi
	cmpb	%r9b, %r12b
	sete	%sil
	addl	%ecx, %esi
	leal	-1(%rdx), %ecx
	cmpl	%esi, %edx
	cmovlel	%esi, %ecx
	incq	%rdi
	incq	%r11
	decq	%r14
	movl	%r9d, %r15d
	movl	%ebp, %r13d
	movl	%ebx, %ebp
	jne	.LBB16_7
# BB#8:                                 # %._crit_edge.i35
	movl	%ecx, -24(%rsp)
	movq	-8(%rsp), %rdx          # 8-byte Reload
	movq	-16(%rsp), %rbx         # 8-byte Reload
.LBB16_9:                               # %SWscore.exit36
	cmpl	%r8d, %ecx
	jb	.LBB16_19
# BB#10:
	movzwl	(%r10), %ecx
	movw	%cx, -28(%rsp)
	movzwl	-3(%rbx), %ecx
	movw	%cx, -26(%rsp)
	movl	options+16(%rip), %ecx
	testq	%rcx, %rcx
	je	.LBB16_19
# BB#11:                                # %.lr.ph
	movq	options+8(%rip), %rsi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB16_12:                              # =>This Inner Loop Header: Depth=1
	movl	-28(%rsp), %edi
	cmpl	(%rsi), %edi
	je	.LBB16_16
# BB#13:                                #   in Loop: Header=BB16_12 Depth=1
	movl	-28(%rsp), %edi
	cmpl	4(%rsi), %edi
	je	.LBB16_17
# BB#14:                                #   in Loop: Header=BB16_12 Depth=1
	incq	%rax
	addq	$8, %rsi
	cmpq	%rcx, %rax
	jb	.LBB16_12
# BB#15:
	xorl	%eax, %eax
	jmp	.LBB16_19
.LBB16_16:
	movl	%eax, 12(%rdx)
	movl	$1, 24(%rdx)
	jmp	.LBB16_18
.LBB16_17:
	movl	%eax, 12(%rdx)
	movl	$-1, 24(%rdx)
.LBB16_18:                              # %.loopexit
	movl	$1, %eax
.LBB16_19:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	perfect_spl_p, .Lfunc_end16-perfect_spl_p
	.cfi_endproc

	.p2align	4, 0x90
	.type	compute_max_score,@function
compute_max_score:                      # @compute_max_score
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi157:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi158:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi159:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi160:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi161:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi162:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi163:
	.cfi_def_cfa_offset 112
.Lcfi164:
	.cfi_offset %rbx, -56
.Lcfi165:
	.cfi_offset %r12, -48
.Lcfi166:
	.cfi_offset %r13, -40
.Lcfi167:
	.cfi_offset %r14, -32
.Lcfi168:
	.cfi_offset %r15, -24
.Lcfi169:
	.cfi_offset %rbp, -16
	movl	%ecx, %r13d
	movq	%rdx, %r12
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rdi, %r14
	movl	(%r12), %ebp
	movl	4(%r12), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	8(%r12), %ebx
	movl	options+44(%rip), %eax
	leal	4(%rax,%rax), %edi
	callq	xmalloc
	movq	%rax, %r15
	movl	$0, 20(%r12)
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 12(%r12)
	cmpl	$0, options+16(%rip)
	je	.LBB17_9
# BB#1:                                 # %.lr.ph
	movl	%ebp, %edx
	movq	%r14, %rcx
	movq	24(%rsp), %rax          # 8-byte Reload
	testl	%r13d, %r13d
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	js	.LBB17_2
# BB#4:                                 # %.lr.ph.split.us
	je	.LBB17_7
# BB#5:                                 # %.lr.ph.split.us.split.preheader
	xorl	%ebp, %ebp
	movq	%rax, %r13
	movl	%edx, %r14d
	.p2align	4, 0x90
.LBB17_6:                               # %.lr.ph.split.us.split
                                        # =>This Inner Loop Header: Depth=1
	movq	options+8(%rip), %rax
	movl	%ebp, %ecx
	leaq	(%rax,%rcx,8), %rax
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r13, %rsi
	movq	%r12, %rdx
	movl	%ebp, %ecx
	movl	%r14d, %r8d
	movl	4(%rsp), %r9d           # 4-byte Reload
	pushq	$1
.Lcfi170:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi171:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi172:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi173:
	.cfi_adjust_cfa_offset 8
	callq	compute_max_score_1
	addq	$32, %rsp
.Lcfi174:
	.cfi_adjust_cfa_offset -32
	incl	%ebp
	cmpl	options+16(%rip), %ebp
	jb	.LBB17_6
	jmp	.LBB17_9
.LBB17_2:                               # %.lr.ph.split.preheader
	xorl	%ebp, %ebp
	movq	%rax, %r13
	movl	%edx, %r14d
	.p2align	4, 0x90
.LBB17_3:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	options+8(%rip), %rax
	movl	%ebp, %ecx
	leaq	4(%rax,%rcx,8), %rax
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r13, %rsi
	movq	%r12, %rdx
	movl	%ebp, %ecx
	movl	%r14d, %r8d
	movl	4(%rsp), %r9d           # 4-byte Reload
	pushq	$-1
.Lcfi175:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi176:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi177:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi178:
	.cfi_adjust_cfa_offset 8
	callq	compute_max_score_1
	addq	$32, %rsp
.Lcfi179:
	.cfi_adjust_cfa_offset -32
	incl	%ebp
	cmpl	options+16(%rip), %ebp
	jb	.LBB17_3
	jmp	.LBB17_9
.LBB17_7:                               # %.lr.ph.split.us.split.us.preheader
	xorl	%r14d, %r14d
	movl	%edx, 20(%rsp)          # 4-byte Spill
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r13           # 8-byte Reload
	movl	20(%rsp), %ebp          # 4-byte Reload
	.p2align	4, 0x90
.LBB17_8:                               # %.lr.ph.split.us.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	options+8(%rip), %rax
	movl	%r14d, %ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leaq	(%rax,%rcx,8), %rax
	movq	%r13, %rdi
	movl	4(%rsp), %r15d          # 4-byte Reload
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	%r14d, %ecx
	movl	%ebp, %r8d
	movl	%r15d, %r9d
	pushq	$1
.Lcfi180:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi181:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi182:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi183:
	.cfi_adjust_cfa_offset 8
	callq	compute_max_score_1
	addq	$32, %rsp
.Lcfi184:
	.cfi_adjust_cfa_offset -32
	movq	options+8(%rip), %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	4(%rax,%rcx,8), %rax
	movq	%r13, %rdi
	movq	%r12, %rsi
	movl	%r15d, %r9d
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdx
	movl	%r14d, %ecx
	movl	%ebp, %r8d
	pushq	$-1
.Lcfi185:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi186:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi187:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi188:
	.cfi_adjust_cfa_offset 8
	callq	compute_max_score_1
	addq	$32, %rsp
.Lcfi189:
	.cfi_adjust_cfa_offset -32
	incl	%r14d
	cmpl	options+16(%rip), %r14d
	jb	.LBB17_8
.LBB17_9:                               # %._crit_edge
	movq	%r15, %rdi
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end17:
	.size	compute_max_score, .Lfunc_end17-compute_max_score
	.cfi_endproc

	.p2align	4, 0x90
	.type	compute_max_score_1,@function
compute_max_score_1:                    # @compute_max_score_1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi190:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi191:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi192:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi193:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi194:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi195:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi196:
	.cfi_def_cfa_offset 336
.Lcfi197:
	.cfi_offset %rbx, -56
.Lcfi198:
	.cfi_offset %r12, -48
.Lcfi199:
	.cfi_offset %r13, -40
.Lcfi200:
	.cfi_offset %r14, -32
.Lcfi201:
	.cfi_offset %r15, -24
.Lcfi202:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
	movq	%r9, 144(%rsp)          # 8-byte Spill
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%r8, 160(%rsp)          # 8-byte Spill
	movl	%ecx, 76(%rsp)          # 4-byte Spill
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	%rsi, 152(%rsp)         # 8-byte Spill
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	352(%rsp), %rcx
	movq	344(%rsp), %rdx
	movl	options+44(%rip), %eax
	movl	(%rcx), %ecx
	movl	%ecx, (%rdx,%rax)
	movl	options+36(%rip), %ecx
	movl	%ecx, %eax
	negl	%eax
	cmpl	%eax, %ecx
	jl	.LBB18_22
# BB#1:                                 # %.lr.ph
	movl	336(%rsp), %esi
	movl	144(%rsp), %ecx         # 4-byte Reload
	addq	%rcx, 152(%rsp)         # 8-byte Folded Spill
	movl	160(%rsp), %r14d        # 4-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r14), %rcx
	movq	%rcx, 232(%rsp)         # 8-byte Spill
	leal	-3(%rsi), %ecx
	movq	%rcx, 224(%rsp)         # 8-byte Spill
	leal	-2(%rsi), %ecx
	movq	%rcx, 216(%rsp)         # 8-byte Spill
	movl	%esi, %ecx
	leaq	-3(%rdx,%rcx), %rsi
	movq	%rsi, 208(%rsp)         # 8-byte Spill
	movslq	%eax, %r15
	movl	options+44(%rip), %edi
	addq	%r15, %r14
	addq	%rdx, %r14
	movq	344(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 264(%rsp)         # 8-byte Spill
	addq	%r15, %rcx
	leaq	-3(%rdx,%rcx), %rcx
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	leaq	3(%rax), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
                                        # implicit-def: %R12D
                                        # implicit-def: %EAX
	movl	%eax, 20(%rsp)          # 4-byte Spill
	jmp	.LBB18_2
.LBB18_18:                              #   in Loop: Header=BB18_2 Depth=1
	ja	.LBB18_20
# BB#19:                                #   in Loop: Header=BB18_2 Depth=1
	movl	76(%rsp), %eax          # 4-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	cmpl	%eax, 12(%rcx)
	ja	.LBB18_20
	jmp	.LBB18_21
.LBB18_28:                              #   in Loop: Header=BB18_2 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 40(%rsp)          # 4-byte Spill
	jmp	.LBB18_31
	.p2align	4, 0x90
.LBB18_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_4 Depth 2
                                        #       Child Loop BB18_5 Depth 3
                                        #       Child Loop BB18_11 Depth 3
	movl	%edi, %edx
	movq	152(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rsi
	subq	%rdx, %rsi
	addq	%r15, %rsi
	movq	344(%rsp), %r13
	movq	%r13, %rdi
	callq	memcpy
	movl	options+44(%rip), %edx
	leaq	4(%r13,%rdx), %rdi
	leaq	(%rbx,%r15), %rsi
	callq	memcpy
	movq	160(%rsp), %rax         # 8-byte Reload
	leal	(%r15,%rax), %ebp
	movq	352(%rsp), %rax
	movq	%rax, %rcx
	movb	(%rcx), %bl
	movb	1(%rcx), %r10b
	movl	options+44(%rip), %r9d
	movq	%r9, %rax
	shrq	%rax
	movq	232(%rsp), %rdi         # 8-byte Reload
	subq	%r9, %rdi
	addq	%r15, %rdi
	movq	224(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%r15), %edx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	movq	216(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%r15), %edx
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movq	208(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r15), %rdx
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	movl	336(%rsp), %edx
	leal	(%r15,%rdx), %edx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	cmpq	$2147483646, %rax       # imm = 0x7FFFFFFE
	movb	(%r13), %sil
	movb	2(%rcx), %al
	movb	%al, 17(%rsp)           # 1-byte Spill
	movb	3(%rcx), %al
	movb	%al, 18(%rsp)           # 1-byte Spill
	movb	2(%r13,%r9), %al
	movb	%al, 19(%rsp)           # 1-byte Spill
	movb	%sil, 15(%rsp)          # 1-byte Spill
	movq	%r14, 256(%rsp)         # 8-byte Spill
	movq	%r15, 248(%rsp)         # 8-byte Spill
	movq	%r9, 240(%rsp)          # 8-byte Spill
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movb	%r10b, 16(%rsp)         # 1-byte Spill
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movb	%bl, 14(%rsp)           # 1-byte Spill
	ja	.LBB18_23
# BB#3:                                 # %.split.us.preheader
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	200(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r9), %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movq	%r9, %rdx
	negq	%rdx
	leal	2(%r9), %eax
	decq	%rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	addq	%r14, %rdx
	xorl	%r15d, %r15d
	movl	$-1, %r9d
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	%rax, 184(%rsp)         # 8-byte Spill
	xorl	%r10d, %r10d
	movl	$0, 24(%rsp)            # 4-byte Folded Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB18_4:                               # %.split.us
                                        #   Parent Loop BB18_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB18_5 Depth 3
                                        #       Child Loop BB18_11 Depth 3
	movl	%r13d, 140(%rsp)        # 4-byte Spill
	movl	%r12d, %eax
	movl	%eax, 128(%rsp)         # 4-byte Spill
	movl	%r10d, 132(%rsp)        # 4-byte Spill
	movl	%r15d, 136(%rsp)        # 4-byte Spill
	leal	(%rbp,%r9), %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movb	(%rcx,%rax), %r13b
	xorl	%r8d, %r8d
	cmpb	%bl, %r13b
	sete	%r8b
	leal	1(%rbp,%r9), %eax
	movb	(%rcx,%rax), %al
	movb	%al, 31(%rsp)           # 1-byte Spill
	incl	%r8d
	movslq	%r9d, %r11
	movl	$0, 60(%rsp)
	movl	$0, 52(%rsp)
	movb	(%rdi,%r11), %cl
	xorl	%r10d, %r10d
	cmpb	%sil, %cl
	sete	%r10b
	movl	%r10d, 56(%rsp)
	movq	264(%rsp), %rbx         # 8-byte Reload
	movq	%rdx, 176(%rsp)         # 8-byte Spill
	movq	192(%rsp), %r14         # 8-byte Reload
	movl	%esi, %r15d
	xorl	%eax, %eax
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB18_5:                               # %.lr.ph.i.us
                                        #   Parent Loop BB18_2 Depth=1
                                        #     Parent Loop BB18_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rbx), %r12d
	xorl	%ebp, %ebp
	cmpb	%r12b, %cl
	sete	%bpl
	addl	%edi, %ebp
	cmpl	%r10d, %ebp
	cmovll	%r10d, %ebp
	movl	%ebp, 52(%rsp)
	movzbl	(%rdx), %ecx
	xorl	%edi, %edi
	cmpb	%r15b, %cl
	sete	%dil
	addl	%eax, %edi
	cmpl	%r10d, %edi
	cmovll	%r10d, %edi
	movl	%edi, 60(%rsp)
	xorl	%eax, %eax
	cmpl	%edi, %ebp
	setl	%al
	movl	52(%rsp,%rax,8), %eax
	xorl	%esi, %esi
	cmpb	%r12b, %cl
	sete	%sil
	addl	%r10d, %esi
	leal	-1(%rax), %r10d
	cmpl	%esi, %eax
	cmovlel	%esi, %r10d
	incq	%rdx
	incq	%rbx
	decq	%r14
	movl	%r12d, %r15d
	movl	%edi, %eax
	movl	%ebp, %edi
	jne	.LBB18_5
# BB#6:                                 # %._crit_edge.i.us
                                        #   in Loop: Header=BB18_4 Depth=2
	xorl	%r15d, %r15d
	cmpb	14(%rsp), %r13b         # 1-byte Folded Reload
	sete	%r15b
	movb	31(%rsp), %al           # 1-byte Reload
	cmpb	16(%rsp), %al           # 1-byte Folded Reload
	cmovel	%r8d, %r15d
	movl	132(%rsp), %edx         # 4-byte Reload
	cmpl	%edx, %r10d
	movl	136(%rsp), %ecx         # 4-byte Reload
	jb	.LBB18_9
# BB#7:                                 #   in Loop: Header=BB18_4 Depth=2
	cmpl	%ecx, %r15d
	ja	.LBB18_10
# BB#8:                                 #   in Loop: Header=BB18_4 Depth=2
	cmpl	%edx, %r10d
	ja	.LBB18_10
.LBB18_9:                               # %splice_score_compare.exit83.thread.us
                                        #   in Loop: Header=BB18_4 Depth=2
	movl	128(%rsp), %eax         # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%edx, %r10d
	movl	%ecx, %r15d
.LBB18_10:                              # %.lr.ph.i78.us.preheader
                                        #   in Loop: Header=BB18_4 Depth=2
	movq	104(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r11), %eax
	xorl	%ecx, %ecx
	movq	32(%rsp), %rdx          # 8-byte Reload
	movb	17(%rsp), %bl           # 1-byte Reload
	cmpb	%bl, (%rdx,%rax)
	sete	%cl
	movq	112(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r11), %eax
	leal	1(%rcx), %r8d
	movb	18(%rsp), %bl           # 1-byte Reload
	cmpb	%bl, (%rdx,%rax)
	cmovnel	%ecx, %r8d
	movl	$0, 60(%rsp)
	movl	$0, 52(%rsp)
	movq	120(%rsp), %rax         # 8-byte Reload
	movb	(%rax,%r11), %r12b
	xorl	%ecx, %ecx
	movb	19(%rsp), %al           # 1-byte Reload
	cmpb	%al, %r12b
	sete	%cl
	movl	%ecx, 56(%rsp)
	xorl	%r13d, %r13d
	movq	272(%rsp), %rbp         # 8-byte Reload
	movq	184(%rsp), %rdx         # 8-byte Reload
	movq	192(%rsp), %r14         # 8-byte Reload
	movl	%eax, %ebx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB18_11:                              # %.lr.ph.i78.us
                                        #   Parent Loop BB18_2 Depth=1
                                        #     Parent Loop BB18_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rbp), %r11d
	xorl	%edi, %edi
	cmpb	%r11b, %r12b
	sete	%dil
	addl	%eax, %edi
	cmpl	%ecx, %edi
	cmovll	%ecx, %edi
	movl	%edi, 52(%rsp)
	movzbl	(%rdx), %r12d
	xorl	%eax, %eax
	cmpb	%bl, %r12b
	sete	%al
	addl	%r13d, %eax
	cmpl	%ecx, %eax
	cmovll	%ecx, %eax
	movl	%eax, 60(%rsp)
	xorl	%ebx, %ebx
	cmpl	%eax, %edi
	setl	%bl
	movl	52(%rsp,%rbx,8), %ebx
	xorl	%esi, %esi
	cmpb	%r11b, %r12b
	sete	%sil
	addl	%ecx, %esi
	leal	-1(%rbx), %ecx
	cmpl	%esi, %ebx
	cmovlel	%esi, %ecx
	incq	%rdx
	incq	%rbp
	decq	%r14
	movl	%r11d, %ebx
	movl	%eax, %r13d
	movl	%edi, %eax
	jne	.LBB18_11
# BB#12:                                # %SWscore.exit80.us
                                        #   in Loop: Header=BB18_4 Depth=2
	movl	140(%rsp), %r13d        # 4-byte Reload
	cmpl	%r13d, %ecx
	jb	.LBB18_14
# BB#13:                                #   in Loop: Header=BB18_4 Depth=2
	cmpl	%ecx, %r13d
	setb	%al
	movl	24(%rsp), %edi          # 4-byte Reload
	cmpl	%r8d, %edi
	setb	%dl
	movq	88(%rsp), %rsi          # 8-byte Reload
	leal	(%rsi,%r9), %esi
	orb	%al, %dl
	cmovnel	%ecx, %r13d
	movl	20(%rsp), %eax          # 4-byte Reload
	cmovnel	%esi, %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	cmovnel	%r8d, %edi
	movl	%edi, 24(%rsp)          # 4-byte Spill
.LBB18_14:                              # %splice_score_compare.exit70.thread.us
                                        #   in Loop: Header=BB18_4 Depth=2
	movq	40(%rsp), %r12          # 8-byte Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	96(%rsp), %rdi          # 8-byte Reload
	movb	14(%rsp), %bl           # 1-byte Reload
	movb	15(%rsp), %sil          # 1-byte Reload
	movq	176(%rsp), %rdx         # 8-byte Reload
	incl	%r9d
	incq	%rdx
	incq	184(%rsp)               # 8-byte Folded Spill
	cmpl	$2, %r9d
	jne	.LBB18_4
	jmp	.LBB18_15
	.p2align	4, 0x90
.LBB18_23:                              #   in Loop: Header=BB18_2 Depth=1
	leal	-1(%rbp), %r8d
	xorl	%edx, %edx
	movq	32(%rsp), %rsi          # 8-byte Reload
	cmpb	%bl, (%rsi,%r8)
	sete	%dl
	movl	%ebp, %eax
	leal	1(%rdx), %ecx
	cmpb	%r10b, (%rsi,%rax)
	movb	15(%rsp), %bl           # 1-byte Reload
	cmovnel	%edx, %ecx
	movb	-1(%rdi), %dl
	xorl	%r11d, %r11d
	cmpb	%bl, %dl
	sete	%r11b
	testl	%ecx, %ecx
	jne	.LBB18_26
# BB#24:                                #   in Loop: Header=BB18_2 Depth=1
	cmpb	%bl, %dl
	je	.LBB18_26
# BB#25:                                # %splice_score_compare.exit83.thread
                                        #   in Loop: Header=BB18_2 Depth=1
	xorl	%r11d, %r11d
	movl	%r12d, %r8d
	xorl	%ecx, %ecx
.LBB18_26:                              # %splice_score_compare.exit70.thread
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	104(%rsp), %rdx         # 8-byte Reload
	leal	-1(%rdx), %edx
	xorl	%edi, %edi
	movq	32(%rsp), %rsi          # 8-byte Reload
	movb	17(%rsp), %bl           # 1-byte Reload
	cmpb	%bl, (%rsi,%rdx)
	sete	%dil
	movq	112(%rsp), %rdx         # 8-byte Reload
	leal	-1(%rdx), %edx
	leal	1(%rdi), %r10d
	movb	18(%rsp), %bl           # 1-byte Reload
	cmpb	%bl, (%rsi,%rdx)
	cmovnel	%edi, %r10d
	xorl	%r13d, %r13d
	movq	120(%rsp), %rdx         # 8-byte Reload
	movb	19(%rsp), %bl           # 1-byte Reload
	cmpb	%bl, -1(%rdx)
	sete	%r13b
	testl	%r10d, %r10d
	setne	%dl
	movq	88(%rsp), %rdi          # 8-byte Reload
	leal	-1(%rdi), %edi
	movl	%r13d, %ebx
	orb	%dl, %bl
	movl	20(%rsp), %edx          # 4-byte Reload
	cmovnel	%edi, %edx
	movl	%edx, 20(%rsp)          # 4-byte Spill
	movl	$0, %edx
	cmovel	%edx, %r10d
	xorl	%edx, %edx
	movb	14(%rsp), %bl           # 1-byte Reload
	cmpb	%bl, (%rsi,%rax)
	sete	%dl
	leal	1(%rbp), %r12d
	leal	1(%rdx), %edi
	movb	16(%rsp), %al           # 1-byte Reload
	cmpb	%al, (%rsi,%r12)
	cmovnel	%edx, %edi
	xorl	%edx, %edx
	movq	96(%rsp), %rax          # 8-byte Reload
	movb	15(%rsp), %bl           # 1-byte Reload
	cmpb	%bl, (%rax)
	sete	%dl
	cmpl	%r11d, %edx
	jb	.LBB18_30
# BB#27:                                #   in Loop: Header=BB18_2 Depth=1
	cmpl	%ecx, %edi
	ja	.LBB18_28
# BB#29:                                #   in Loop: Header=BB18_2 Depth=1
	cmpl	%r11d, %edx
	movq	64(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 40(%rsp)          # 4-byte Spill
	ja	.LBB18_31
.LBB18_30:                              # %splice_score_compare.exit83.thread.1
                                        #   in Loop: Header=BB18_2 Depth=1
	movl	%r8d, %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movl	%r11d, %edx
	movl	%ecx, %edi
.LBB18_31:                              # %SWscore.exit80.1
                                        #   in Loop: Header=BB18_2 Depth=1
	movl	104(%rsp), %ecx         # 4-byte Reload
	xorl	%esi, %esi
	movq	32(%rsp), %rbx          # 8-byte Reload
	movb	17(%rsp), %al           # 1-byte Reload
	cmpb	%al, (%rbx,%rcx)
	sete	%sil
	movl	112(%rsp), %ebp         # 4-byte Reload
	leal	1(%rsi), %ecx
	movb	18(%rsp), %al           # 1-byte Reload
	cmpb	%al, (%rbx,%rbp)
	cmovnel	%esi, %ecx
	xorl	%esi, %esi
	movq	120(%rsp), %rbp         # 8-byte Reload
	movb	19(%rsp), %bl           # 1-byte Reload
	cmpb	%bl, (%rbp)
	sete	%sil
	cmpl	%r13d, %esi
	jb	.LBB18_33
# BB#32:                                #   in Loop: Header=BB18_2 Depth=1
	cmpl	%esi, %r13d
	setb	%r8b
	cmpl	%ecx, %r10d
	setb	%bl
	orb	%r8b, %bl
	cmovnel	%esi, %r13d
	movl	20(%rsp), %esi          # 4-byte Reload
	cmovnel	88(%rsp), %esi          # 4-byte Folded Reload
	movl	%esi, 20(%rsp)          # 4-byte Spill
	cmovnel	%ecx, %r10d
.LBB18_33:                              # %splice_score_compare.exit70.thread.1
                                        #   in Loop: Header=BB18_2 Depth=1
	movl	%r10d, 24(%rsp)         # 4-byte Spill
	movb	16(%rsp), %bl           # 1-byte Reload
	movb	14(%rsp), %al           # 1-byte Reload
	xorl	%ecx, %ecx
	movq	32(%rsp), %rsi          # 8-byte Reload
	cmpb	%al, (%rsi,%r12)
	sete	%cl
	movq	64(%rsp), %rax          # 8-byte Reload
	addl	$2, %eax
	leal	1(%rcx), %r15d
	cmpb	%bl, (%rsi,%rax)
	cmovnel	%ecx, %r15d
	xorl	%r10d, %r10d
	movq	96(%rsp), %rax          # 8-byte Reload
	movb	15(%rsp), %cl           # 1-byte Reload
	cmpb	%cl, 1(%rax)
	sete	%r10b
	cmpl	%edx, %r10d
	jb	.LBB18_36
# BB#34:                                #   in Loop: Header=BB18_2 Depth=1
	cmpl	%edi, %r15d
	ja	.LBB18_37
# BB#35:                                #   in Loop: Header=BB18_2 Depth=1
	cmpl	%edx, %r10d
	ja	.LBB18_37
.LBB18_36:                              # %splice_score_compare.exit83.thread.2
                                        #   in Loop: Header=BB18_2 Depth=1
	movl	40(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r12d
	movl	%edx, %r10d
	movl	%edi, %r15d
.LBB18_37:                              # %SWscore.exit80.2
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	104(%rsp), %rax         # 8-byte Reload
	incl	%eax
	xorl	%ecx, %ecx
	movq	32(%rsp), %rdx          # 8-byte Reload
	movb	17(%rsp), %bl           # 1-byte Reload
	cmpb	%bl, (%rdx,%rax)
	sete	%cl
	movq	112(%rsp), %rsi         # 8-byte Reload
	incl	%esi
	leal	1(%rcx), %eax
	movb	18(%rsp), %bl           # 1-byte Reload
	cmpb	%bl, (%rdx,%rsi)
	cmovnel	%ecx, %eax
	xorl	%ecx, %ecx
	movq	120(%rsp), %rdx         # 8-byte Reload
	movb	19(%rsp), %bl           # 1-byte Reload
	cmpb	%bl, 1(%rdx)
	sete	%cl
	cmpl	%r13d, %ecx
	jb	.LBB18_15
# BB#38:                                #   in Loop: Header=BB18_2 Depth=1
	cmpl	%ecx, %r13d
	setb	%dl
	movl	24(%rsp), %edi          # 4-byte Reload
	cmpl	%eax, %edi
	setb	%bl
	movq	88(%rsp), %rsi          # 8-byte Reload
	incl	%esi
	orb	%dl, %bl
	cmovnel	%ecx, %r13d
	movl	20(%rsp), %ecx          # 4-byte Reload
	cmovnel	%esi, %ecx
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	cmovnel	%eax, %edi
	movl	%edi, 24(%rsp)          # 4-byte Spill
.LBB18_15:                              # %.us-lcssa.us
                                        #   in Loop: Header=BB18_2 Depth=1
	addl	%r10d, %r13d
	movl	24(%rsp), %edx          # 4-byte Reload
	addl	%r15d, %edx
	movq	80(%rsp), %rax          # 8-byte Reload
	cmpl	16(%rax), %r13d
	movq	248(%rsp), %r15         # 8-byte Reload
	jb	.LBB18_21
# BB#16:                                #   in Loop: Header=BB18_2 Depth=1
	jbe	.LBB18_17
.LBB18_20:                              # %splice_score_compare.exit.thread126
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	80(%rsp), %rcx          # 8-byte Reload
	movl	%r13d, 16(%rcx)
	movl	%edx, 20(%rcx)
	movl	%r12d, (%rcx)
	movq	144(%rsp), %rax         # 8-byte Reload
	leal	(%r15,%rax), %eax
	movl	%eax, 4(%rcx)
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, 8(%rcx)
	movl	76(%rsp), %eax          # 4-byte Reload
	movl	%eax, 12(%rcx)
	movl	360(%rsp), %eax
	movl	%eax, 24(%rcx)
	jmp	.LBB18_21
	.p2align	4, 0x90
.LBB18_17:                              #   in Loop: Header=BB18_2 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	cmpl	20(%rax), %edx
	jae	.LBB18_18
	.p2align	4, 0x90
.LBB18_21:                              # %splice_score_compare.exit.thread
                                        #   in Loop: Header=BB18_2 Depth=1
	movslq	options+36(%rip), %rax
	movq	256(%rsp), %r14         # 8-byte Reload
	incq	%r14
	incq	168(%rsp)               # 8-byte Folded Spill
	cmpq	%rax, %r15
	leaq	1(%r15), %r15
	movq	240(%rsp), %rdi         # 8-byte Reload
	jl	.LBB18_2
.LBB18_22:                              # %._crit_edge
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	compute_max_score_1, .Lfunc_end18-compute_max_score_1
	.cfi_endproc

	.type	encoding,@object        # @encoding
	.comm	encoding,512,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%u-%u  (%u-%u)   %u%% ==\n"
	.size	.L.str, 26

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%u-%u  (%u-%u)   %u%% %s (%.2s/%.2s) %u\n"
	.size	.L.str.1, 41

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"->"
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"<-"
	.size	.L.str.3, 3

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%u-%u  (%u-%u)   %u%%\n"
	.size	.L.str.4, 23


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
