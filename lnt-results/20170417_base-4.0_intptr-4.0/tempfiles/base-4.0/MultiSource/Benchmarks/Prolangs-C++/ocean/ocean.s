	.text
	.file	"ocean.bc"
	.globl	_ZN4Cell9getCellAtE10Coordinate
	.p2align	4, 0x90
	.type	_ZN4Cell9getCellAtE10Coordinate,@function
_ZN4Cell9getCellAtE10Coordinate:        # @_ZN4Cell9getCellAtE10Coordinate
	.cfi_startproc
# BB#0:
	movl	(%rsi), %eax
	movl	4(%rsi), %ecx
	imulq	$8000, %rcx, %rcx       # imm = 0x1F40
	movq	cells(%rcx,%rax,8), %rax
	retq
.Lfunc_end0:
	.size	_ZN4Cell9getCellAtE10Coordinate, .Lfunc_end0-_ZN4Cell9getCellAtE10Coordinate
	.cfi_endproc

	.globl	_ZN4Cell12assignCellAtE10CoordinatePS_
	.p2align	4, 0x90
	.type	_ZN4Cell12assignCellAtE10CoordinatePS_,@function
_ZN4Cell12assignCellAtE10CoordinatePS_: # @_ZN4Cell12assignCellAtE10CoordinatePS_
	.cfi_startproc
# BB#0:
	movl	(%rsi), %eax
	movl	4(%rsi), %ecx
	imulq	$8000, %rcx, %rcx       # imm = 0x1F40
	movq	%rdx, cells(%rcx,%rax,8)
	retq
.Lfunc_end1:
	.size	_ZN4Cell12assignCellAtE10CoordinatePS_, .Lfunc_end1-_ZN4Cell12assignCellAtE10CoordinatePS_
	.cfi_endproc

	.globl	_ZN4Cell20getNeighborWithImageEc
	.p2align	4, 0x90
	.type	_ZN4Cell20getNeighborWithImageEc,@function
_ZN4Cell20getNeighborWithImageEc:       # @_ZN4Cell20getNeighborWithImageEc
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	movl	4(%rax), %r8d
	testq	%r8, %r8
	movl	%r8d, %ecx
	jne	.LBB2_2
# BB#1:
	movq	Ocean1(%rip), %rcx
	movl	(%rcx), %ecx
.LBB2_2:                                # %_ZN4Cell5northEv.exit
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 48
.Lcfi2:
	.cfi_offset %rbx, -16
	decl	%ecx
	movl	(%rax), %r9d
	imulq	$8000, %rcx, %rax       # imm = 0x1F40
	movq	cells(%rax,%r9,8), %rax
	xorl	%ebx, %ebx
	cmpb	%sil, 16(%rax)
	jne	.LBB2_6
# BB#3:
	testl	%r8d, %r8d
	movl	%r8d, %eax
	jne	.LBB2_5
# BB#4:
	movq	Ocean1(%rip), %rax
	movl	(%rax), %eax
.LBB2_5:                                # %_ZN4Cell5northEv.exit17
	decl	%eax
	imulq	$8000, %rax, %rax       # imm = 0x1F40
	movq	cells(%rax,%r9,8), %rax
	movq	%rax, (%rsp)
	movl	$1, %ebx
.LBB2_6:
	leal	1(%r8), %eax
	movq	Ocean1(%rip), %rcx
	xorl	%edx, %edx
	divl	(%rcx)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	imulq	$8000, %rdx, %rax       # imm = 0x1F40
	movq	cells(%rax,%r9,8), %rax
	cmpb	%sil, 16(%rax)
	jne	.LBB2_8
# BB#7:
	movq	%rax, (%rsp,%rbx,8)
	leal	1(%rbx), %eax
	movl	%eax, %ebx
.LBB2_8:
	leal	1(%r9), %eax
	movl	4(%rcx), %ecx
	xorl	%edx, %edx
	divl	%ecx
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	imulq	$8000, %r8, %rax        # imm = 0x1F40
	movq	cells(%rax,%rdx,8), %rdx
	cmpb	%sil, 16(%rdx)
	jne	.LBB2_10
# BB#9:
	movl	%ebx, %r8d
	incl	%ebx
	movq	%rdx, (%rsp,%r8,8)
.LBB2_10:                               # %_ZN4Cell4westEv.exit15
	testl	%r9d, %r9d
	movl	%r9d, %edx
	cmovel	%ecx, %edx
	decl	%edx
	movq	cells(%rax,%rdx,8), %rdx
	cmpb	%sil, 16(%rdx)
	jne	.LBB2_12
# BB#11:                                # %_ZN4Cell4westEv.exit
	testl	%r9d, %r9d
	cmovnel	%r9d, %ecx
	decl	%ecx
	movq	cells(%rax,%rcx,8), %rax
	movl	%ebx, %ecx
	incl	%ebx
	movq	%rax, (%rsp,%rcx,8)
.LBB2_12:
	testl	%ebx, %ebx
	je	.LBB2_14
# BB#13:
	decl	%ebx
	callq	random
	movslq	%ebx, %rcx
	imulq	%rax, %rcx
	movabsq	$-9223372032559808509, %rdx # imm = 0x8000000100000003
	movq	%rcx, %rax
	imulq	%rdx
	addq	%rcx, %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$30, %rdx
	addl	%eax, %edx
	movq	(%rsp,%rdx,8), %rdi
.LBB2_14:
	movq	%rdi, %rax
	addq	$32, %rsp
	popq	%rbx
	retq
.Lfunc_end2:
	.size	_ZN4Cell20getNeighborWithImageEc, .Lfunc_end2-_ZN4Cell20getNeighborWithImageEc
	.cfi_endproc

	.globl	_ZN4Cell5northEv
	.p2align	4, 0x90
	.type	_ZN4Cell5northEv,@function
_ZN4Cell5northEv:                       # @_ZN4Cell5northEv
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	movl	4(%rax), %ecx
	testl	%ecx, %ecx
	jne	.LBB3_2
# BB#1:
	movq	Ocean1(%rip), %rcx
	movl	(%rcx), %ecx
.LBB3_2:
	decl	%ecx
	movl	(%rax), %eax
	imulq	$8000, %rcx, %rcx       # imm = 0x1F40
	movq	cells(%rcx,%rax,8), %rax
	retq
.Lfunc_end3:
	.size	_ZN4Cell5northEv, .Lfunc_end3-_ZN4Cell5northEv
	.cfi_endproc

	.globl	_ZN4Cell5southEv
	.p2align	4, 0x90
	.type	_ZN4Cell5southEv,@function
_ZN4Cell5southEv:                       # @_ZN4Cell5southEv
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	movl	(%rax), %ecx
	movl	4(%rax), %eax
	incl	%eax
	movq	Ocean1(%rip), %rsi
	xorl	%edx, %edx
	divl	(%rsi)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	imulq	$8000, %rdx, %rax       # imm = 0x1F40
	movq	cells(%rax,%rcx,8), %rax
	retq
.Lfunc_end4:
	.size	_ZN4Cell5southEv, .Lfunc_end4-_ZN4Cell5southEv
	.cfi_endproc

	.globl	_ZN4Cell4eastEv
	.p2align	4, 0x90
	.type	_ZN4Cell4eastEv,@function
_ZN4Cell4eastEv:                        # @_ZN4Cell4eastEv
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rcx
	movl	(%rcx), %eax
	movl	4(%rcx), %ecx
	incl	%eax
	movq	Ocean1(%rip), %rsi
	xorl	%edx, %edx
	divl	4(%rsi)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	imulq	$8000, %rcx, %rax       # imm = 0x1F40
	movq	cells(%rax,%rdx,8), %rax
	retq
.Lfunc_end5:
	.size	_ZN4Cell4eastEv, .Lfunc_end5-_ZN4Cell4eastEv
	.cfi_endproc

	.globl	_ZN4Cell4westEv
	.p2align	4, 0x90
	.type	_ZN4Cell4westEv,@function
_ZN4Cell4westEv:                        # @_ZN4Cell4westEv
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rcx
	movl	(%rcx), %eax
	testl	%eax, %eax
	jne	.LBB6_2
# BB#1:
	movq	Ocean1(%rip), %rax
	movl	4(%rax), %eax
.LBB6_2:
	decl	%eax
	movl	4(%rcx), %ecx
	imulq	$8000, %rcx, %rcx       # imm = 0x1F40
	movq	cells(%rcx,%rax,8), %rax
	retq
.Lfunc_end6:
	.size	_ZN4Cell4westEv, .Lfunc_end6-_ZN4Cell4westEv
	.cfi_endproc

	.globl	_ZN6Random14nextIntBetweenEii
	.p2align	4, 0x90
	.type	_ZN6Random14nextIntBetweenEii,@function
_ZN6Random14nextIntBetweenEii:          # @_ZN6Random14nextIntBetweenEii
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 16
.Lcfi4:
	.cfi_offset %rbx, -16
	movl	%edx, %ebx
	callq	random
	movslq	%ebx, %rcx
	imulq	%rax, %rcx
	movabsq	$-9223372032559808509, %rdx # imm = 0x8000000100000003
	movq	%rcx, %rax
	imulq	%rdx
	addq	%rcx, %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$30, %rdx
	leal	(%rdx,%rax), %eax
	popq	%rbx
	retq
.Lfunc_end7:
	.size	_ZN6Random14nextIntBetweenEii, .Lfunc_end7-_ZN6Random14nextIntBetweenEii
	.cfi_endproc

	.globl	_ZN4Cell21getEmptyNeighborCoordEv
	.p2align	4, 0x90
	.type	_ZN4Cell21getEmptyNeighborCoordEv,@function
_ZN4Cell21getEmptyNeighborCoordEv:      # @_ZN4Cell21getEmptyNeighborCoordEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
.Lcfi6:
	.cfi_offset %rbx, -16
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movl	$45, %esi
	movq	%rax, %rdi
	callq	_ZN4Cell20getNeighborWithImageEc
	movq	8(%rax), %rax
	movl	(%rax), %ecx
	movl	%ecx, (%rbx)
	movl	4(%rax), %eax
	movl	%eax, 4(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end8:
	.size	_ZN4Cell21getEmptyNeighborCoordEv, .Lfunc_end8-_ZN4Cell21getEmptyNeighborCoordEv
	.cfi_endproc

	.globl	_ZN4Cell20getPreyNeighborCoordEv
	.p2align	4, 0x90
	.type	_ZN4Cell20getPreyNeighborCoordEv,@function
_ZN4Cell20getPreyNeighborCoordEv:       # @_ZN4Cell20getPreyNeighborCoordEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 16
.Lcfi8:
	.cfi_offset %rbx, -16
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movl	$102, %esi
	movq	%rax, %rdi
	callq	_ZN4Cell20getNeighborWithImageEc
	movq	8(%rax), %rax
	movl	(%rax), %ecx
	movl	%ecx, (%rbx)
	movl	4(%rax), %eax
	movl	%eax, 4(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end9:
	.size	_ZN4Cell20getPreyNeighborCoordEv, .Lfunc_end9-_ZN4Cell20getPreyNeighborCoordEv
	.cfi_endproc

	.globl	_ZN4Cell9reproduceE10Coordinate
	.p2align	4, 0x90
	.type	_ZN4Cell9reproduceE10Coordinate,@function
_ZN4Cell9reproduceE10Coordinate:        # @_ZN4Cell9reproduceE10Coordinate
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -24
.Lcfi13:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	$_ZTV4Cell+16, (%rbx)
.Ltmp0:
	movl	$8, %edi
	callq	_Znwm
.Ltmp1:
# BB#1:
	movl	(%r14), %ecx
	movl	%ecx, (%rax)
	movl	4(%r14), %ecx
	movl	%ecx, 4(%rax)
	movq	%rax, 8(%rbx)
	movb	$45, 16(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB10_2:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZN4Cell9reproduceE10Coordinate, .Lfunc_end10-_ZN4Cell9reproduceE10Coordinate
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end10-.Ltmp1     #   Call between .Ltmp1 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN4Cell7displayEv
	.p2align	4, 0x90
	.type	_ZN4Cell7displayEv,@function
_ZN4Cell7displayEv:                     # @_ZN4Cell7displayEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end11:
	.size	_ZN4Cell7displayEv, .Lfunc_end11-_ZN4Cell7displayEv
	.cfi_endproc

	.globl	_ZN4Prey8moveFromE10CoordinateS0_
	.p2align	4, 0x90
	.type	_ZN4Prey8moveFromE10CoordinateS0_,@function
_ZN4Prey8moveFromE10CoordinateS0_:      # @_ZN4Prey8moveFromE10CoordinateS0_
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 64
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	20(%r15), %ebp
	decl	%ebp
	movl	%ebp, 20(%r15)
	movl	(%rbx), %r12d
	cmpl	(%r14), %r12d
	je	.LBB12_10
# BB#1:                                 # %_ZN10CoordinateneERKS_.exit
	movl	4(%rbx), %r13d
	cmpl	4(%r14), %r13d
	je	.LBB12_10
# BB#2:
	imulq	$8000, %r13, %rax       # imm = 0x1F40
	movq	cells(%rax,%r12,8), %rdi
	testq	%rdi, %rdi
	je	.LBB12_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movl	(%rbx), %r12d
	movl	4(%rbx), %r13d
	movl	20(%r15), %ebp
.LBB12_4:
	movl	$8, %edi
	callq	_Znwm
	movl	%r12d, (%rax)
	movl	%r13d, 4(%rax)
	movq	%rax, 8(%r15)
	movl	%r13d, %eax
	movl	%r12d, %ecx
	imulq	$8000, %rax, %rax       # imm = 0x1F40
	movq	%r15, cells(%rax,%rcx,8)
	testl	%ebp, %ebp
	jle	.LBB12_5
# BB#7:
	movl	(%r14), %r15d
	movl	4(%r14), %ebp
.Ltmp3:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp4:
# BB#8:
	movq	$_ZTV4Cell+16, (%rbx)
.Ltmp6:
	movl	$8, %edi
	callq	_Znwm
.Ltmp7:
# BB#9:
	movl	%r15d, (%rax)
	movl	%ebp, 4(%rax)
	movq	%rax, 8(%rbx)
	movb	$45, 16(%rbx)
	imulq	$8000, %rbp, %rax       # imm = 0x1F40
	movq	%rbx, cells(%rax,%r15,8)
	jmp	.LBB12_10
.LBB12_5:
	movl	$6, 20(%r15)
	movl	(%r14), %ebx
	movl	4(%r14), %ebp
	movq	(%r15), %rax
	movq	(%rax), %rax
	movl	%ebx, (%rsp)
	movl	%ebp, 4(%rsp)
.Ltmp9:
	movq	%rsp, %rsi
	movq	%r15, %rdi
	callq	*%rax
.Ltmp10:
# BB#6:
	imulq	$8000, %rbp, %rcx       # imm = 0x1F40
	movq	%rax, cells(%rcx,%rbx,8)
.LBB12_10:                              # %_ZN10CoordinateneERKS_.exit.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_13:
.Ltmp11:
	jmp	.LBB12_14
.LBB12_12:
.Ltmp8:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB12_11:
.Ltmp5:
.LBB12_14:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZN4Prey8moveFromE10CoordinateS0_, .Lfunc_end12-_ZN4Prey8moveFromE10CoordinateS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp3-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 4 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end12-.Ltmp10    #   Call between .Ltmp10 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN4Prey9reproduceE10Coordinate
	.p2align	4, 0x90
	.type	_ZN4Prey9reproduceE10Coordinate,@function
_ZN4Prey9reproduceE10Coordinate:        # @_ZN4Prey9reproduceE10Coordinate
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 32
.Lcfi30:
	.cfi_offset %rbx, -24
.Lcfi31:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	$_ZTV4Cell+16, (%rbx)
.Ltmp12:
	movl	$8, %edi
	callq	_Znwm
.Ltmp13:
# BB#1:
	movl	(%r14), %ecx
	movl	%ecx, (%rax)
	movl	4(%r14), %ecx
	movl	%ecx, 4(%rax)
	movq	%rax, 8(%rbx)
	movq	$_ZTV4Prey+16, (%rbx)
	movl	$6, 20(%rbx)
	movb	$102, 16(%rbx)
	movq	Ocean1(%rip), %rax
	incl	8(%rax)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB13_2:
.Ltmp14:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end13:
	.size	_ZN4Prey9reproduceE10Coordinate, .Lfunc_end13-_ZN4Prey9reproduceE10Coordinate
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp12-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin2   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Lfunc_end13-.Ltmp13    #   Call between .Ltmp13 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8Predator7processEv
	.p2align	4, 0x90
	.type	_ZN8Predator7processEv,@function
_ZN8Predator7processEv:                 # @_ZN8Predator7processEv
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 80
.Lcfi38:
	.cfi_offset %rbx, -48
.Lcfi39:
	.cfi_offset %r12, -40
.Lcfi40:
	.cfi_offset %r13, -32
.Lcfi41:
	.cfi_offset %r14, -24
.Lcfi42:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	decl	24(%rbx)
	je	.LBB14_1
# BB#6:
	movl	$102, %esi
	movq	%rbx, %rdi
	callq	_ZN4Cell20getNeighborWithImageEc
	movq	8(%rax), %rdx
	movl	(%rdx), %eax
	movq	8(%rbx), %rsi
	movl	(%rsi), %ecx
	cmpl	%ecx, %eax
	je	.LBB14_10
# BB#7:                                 # %_ZN10CoordinateneERKS_.exit
	movl	4(%rdx), %edx
	movl	4(%rsi), %esi
	cmpl	%esi, %edx
	jne	.LBB14_8
.LBB14_10:                              # %_ZN10CoordinateneERKS_.exit.thread
	movl	$45, %esi
	movq	%rbx, %rdi
	callq	_ZN4Cell20getNeighborWithImageEc
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rbx), %rcx
	movl	(%rcx), %edx
	movl	%edx, 16(%rsp)
	movl	4(%rcx), %ecx
	movl	%ecx, 20(%rsp)
	movq	%rax, 24(%rsp)
.Ltmp18:
	leaq	16(%rsp), %rsi
	leaq	24(%rsp), %rdx
	movq	%rbx, %rdi
	callq	_ZN4Prey8moveFromE10CoordinateS0_
.Ltmp19:
	jmp	.LBB14_11
.LBB14_1:
	movq	8(%rbx), %r13
	movl	(%r13), %r15d
	movl	4(%r13), %r12d
.Ltmp20:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp21:
# BB#2:
	movq	$_ZTV4Cell+16, (%r14)
.Ltmp23:
	movl	$8, %edi
	callq	_Znwm
.Ltmp24:
# BB#3:
	movl	(%r13), %ecx
	movl	%ecx, (%rax)
	movl	4(%r13), %ecx
	movl	%ecx, 4(%rax)
	movq	%rax, 8(%r14)
	movb	$45, 16(%r14)
	imulq	$8000, %r12, %rax       # imm = 0x1F40
	movq	%r14, cells(%rax,%r15,8)
	movq	Ocean1(%rip), %rax
	decl	12(%rax)
	movq	(%rbx), %rax
.Ltmp26:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp27:
	jmp	.LBB14_11
.LBB14_8:
	movq	Ocean1(%rip), %rdi
	decl	8(%rdi)
	movl	$6, 24(%rbx)
	movl	%ecx, 8(%rsp)
	movl	%esi, 12(%rsp)
	movl	%eax, (%rsp)
	movl	%edx, 4(%rsp)
.Ltmp15:
	leaq	8(%rsp), %rsi
	movq	%rsp, %rdx
	movq	%rbx, %rdi
	callq	_ZN4Prey8moveFromE10CoordinateS0_
.Ltmp16:
.LBB14_11:
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB14_9:
.Ltmp17:
	jmp	.LBB14_13
.LBB14_5:
.Ltmp25:
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB14_4:
.Ltmp22:
	jmp	.LBB14_13
.LBB14_12:
.Ltmp28:
.LBB14_13:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end14:
	.size	_ZN8Predator7processEv, .Lfunc_end14-_ZN8Predator7processEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\320"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp18-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp28-.Lfunc_begin3   #     jumps to .Ltmp28
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin3   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin3   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin3   #     jumps to .Ltmp28
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin3   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Lfunc_end14-.Ltmp16    #   Call between .Ltmp16 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN4Prey7processEv,"axG",@progbits,_ZN4Prey7processEv,comdat
	.weak	_ZN4Prey7processEv
	.p2align	4, 0x90
	.type	_ZN4Prey7processEv,@function
_ZN4Prey7processEv:                     # @_ZN4Prey7processEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 32
.Lcfi45:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$45, %esi
	callq	_ZN4Cell20getNeighborWithImageEc
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rbx), %rcx
	movl	(%rcx), %edx
	movl	%edx, (%rsp)
	movl	4(%rcx), %ecx
	movl	%ecx, 4(%rsp)
	movq	%rax, 8(%rsp)
	movq	%rsp, %rsi
	leaq	8(%rsp), %rdx
	movq	%rbx, %rdi
	callq	_ZN4Prey8moveFromE10CoordinateS0_
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end15:
	.size	_ZN4Prey7processEv, .Lfunc_end15-_ZN4Prey7processEv
	.cfi_endproc

	.text
	.globl	_ZN8Predator9reproduceE10Coordinate
	.p2align	4, 0x90
	.type	_ZN8Predator9reproduceE10Coordinate,@function
_ZN8Predator9reproduceE10Coordinate:    # @_ZN8Predator9reproduceE10Coordinate
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 32
.Lcfi49:
	.cfi_offset %rbx, -32
.Lcfi50:
	.cfi_offset %r14, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	(%r14), %ebp
	movl	4(%r14), %r14d
	movq	$_ZTV4Cell+16, (%rbx)
.Ltmp29:
	movl	$8, %edi
	callq	_Znwm
.Ltmp30:
# BB#1:
	movl	%ebp, (%rax)
	movl	%r14d, 4(%rax)
	movq	%rax, 8(%rbx)
	movl	$6, 20(%rbx)
	movq	$_ZTV8Predator+16, (%rbx)
	movl	$6, 24(%rbx)
	movb	$83, 16(%rbx)
	movq	Ocean1(%rip), %rax
	incl	12(%rax)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB16_2:
.Ltmp31:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end16:
	.size	_ZN8Predator9reproduceE10Coordinate, .Lfunc_end16-_ZN8Predator9reproduceE10Coordinate
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp29-.Lfunc_begin4   #   Call between .Lfunc_begin4 and .Ltmp29
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp30-.Ltmp29         #   Call between .Ltmp29 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin4   #     jumps to .Ltmp31
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Lfunc_end16-.Ltmp30    #   Call between .Ltmp30 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI17_0:
	.long	805306368               # float 4.65661287E-10
	.text
	.globl	_ZN6Random8randRealEv
	.p2align	4, 0x90
	.type	_ZN6Random8randRealEv,@function
_ZN6Random8randRealEv:                  # @_ZN6Random8randRealEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 16
	callq	random
	cvtsi2ssq	%rax, %xmm0
	mulss	.LCPI17_0(%rip), %xmm0
	popq	%rax
	retq
.Lfunc_end17:
	.size	_ZN6Random8randRealEv, .Lfunc_end17-_ZN6Random8randRealEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI18_0:
	.long	500                     # 0x1f4
	.long	1000                    # 0x3e8
	.long	1000                    # 0x3e8
	.long	200                     # 0xc8
	.text
	.globl	_ZN5Ocean10initializeEv
	.p2align	4, 0x90
	.type	_ZN5Ocean10initializeEv,@function
_ZN5Ocean10initializeEv:                # @_ZN5Ocean10initializeEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 16
.Lcfi54:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$8, %edi
	callq	_Znwm
	movq	%rax, 24(%rbx)
	movl	$3797, (%rax)           # imm = 0xED5
	movl	$2117, 4(%rax)          # imm = 0x845
	movl	$750, 16(%rbx)          # imm = 0x2EE
	movaps	.LCPI18_0(%rip), %xmm0  # xmm0 = [500,1000,1000,200]
	movups	%xmm0, (%rbx)
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZN5Ocean9initCellsEv   # TAILCALL
.Lfunc_end18:
	.size	_ZN5Ocean10initializeEv, .Lfunc_end18-_ZN5Ocean10initializeEv
	.cfi_endproc

	.globl	_ZN5Ocean9initCellsEv
	.p2align	4, 0x90
	.type	_ZN5Ocean9initCellsEv,@function
_ZN5Ocean9initCellsEv:                  # @_ZN5Ocean9initCellsEv
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi58:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi59:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi61:
	.cfi_def_cfa_offset 80
.Lcfi62:
	.cfi_offset %rbx, -56
.Lcfi63:
	.cfi_offset %r12, -48
.Lcfi64:
	.cfi_offset %r13, -40
.Lcfi65:
	.cfi_offset %r14, -32
.Lcfi66:
	.cfi_offset %r15, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	(%rdi), %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB19_7
# BB#1:                                 # %.preheader.lr.ph.i
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	4(%rax), %r12d
	testq	%r12, %r12
	je	.LBB19_7
# BB#2:                                 # %.preheader.us.i.preheader
	movl	$cells, %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB19_3:                               # %.preheader.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_4 Depth 2
	movq	%r14, %r13
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB19_4:                               #   Parent Loop BB19_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	$_ZTV4Cell+16, (%rbx)
.Ltmp32:
	movl	$8, %edi
	callq	_Znwm
.Ltmp33:
# BB#5:                                 #   in Loop: Header=BB19_4 Depth=2
	movl	%r15d, (%rax)
	movl	%ebp, 4(%rax)
	movq	%rax, 8(%rbx)
	movb	$45, 16(%rbx)
	movq	%rbx, (%r13)
	incq	%r15
	addq	$8, %r13
	cmpq	%r12, %r15
	jb	.LBB19_4
# BB#6:                                 # %._crit_edge.us.i
                                        #   in Loop: Header=BB19_3 Depth=1
	incq	%rbp
	addq	$8000, %r14             # imm = 0x1F40
	cmpq	16(%rsp), %rbp          # 8-byte Folded Reload
	jb	.LBB19_3
.LBB19_7:                               # %_ZN5Ocean13addEmptyCellsEv.exit
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rdi
	callq	_ZN5Ocean12addObstaclesEv
	movq	%rbx, %rdi
	callq	_ZN5Ocean12addPredatorsEv
	movq	%rbx, %rdi
	callq	_ZN5Ocean7addPreyEv
	movq	%rbx, Ocean1(%rip)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB19_8:                               # %.us-lcssa.us.i
.Ltmp34:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end19:
	.size	_ZN5Ocean9initCellsEv, .Lfunc_end19-_ZN5Ocean9initCellsEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table19:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp32-.Lfunc_begin5   #   Call between .Lfunc_begin5 and .Ltmp32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin5   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Lfunc_end19-.Ltmp33    #   Call between .Ltmp33 and .Lfunc_end19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN5Ocean13addEmptyCellsEv
	.p2align	4, 0x90
	.type	_ZN5Ocean13addEmptyCellsEv,@function
_ZN5Ocean13addEmptyCellsEv:             # @_ZN5Ocean13addEmptyCellsEv
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi71:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi74:
	.cfi_def_cfa_offset 64
.Lcfi75:
	.cfi_offset %rbx, -56
.Lcfi76:
	.cfi_offset %r12, -48
.Lcfi77:
	.cfi_offset %r13, -40
.Lcfi78:
	.cfi_offset %r14, -32
.Lcfi79:
	.cfi_offset %r15, -24
.Lcfi80:
	.cfi_offset %rbp, -16
	movl	(%rdi), %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	testl	%eax, %eax
	je	.LBB20_11
# BB#1:                                 # %.preheader.lr.ph
	movl	4(%rdi), %r12d
	testl	%r12d, %r12d
	je	.LBB20_7
# BB#2:                                 # %.preheader.us.preheader
	movl	$cells, %r15d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB20_3:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_4 Depth 2
	movq	%r15, %rbp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB20_4:                               #   Parent Loop BB20_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	$_ZTV4Cell+16, (%rbx)
.Ltmp35:
	movl	$8, %edi
	callq	_Znwm
.Ltmp36:
# BB#5:                                 #   in Loop: Header=BB20_4 Depth=2
	movl	%r14d, (%rax)
	movl	%r13d, 4(%rax)
	movq	%rax, 8(%rbx)
	movb	$45, 16(%rbx)
	movq	%rbx, (%rbp)
	incl	%r14d
	addq	$8, %rbp
	cmpl	%r12d, %r14d
	jb	.LBB20_4
# BB#6:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB20_3 Depth=1
	incl	%r13d
	addq	$8000, %r15             # imm = 0x1F40
	cmpl	(%rsp), %r13d           # 4-byte Folded Reload
	jb	.LBB20_3
	jmp	.LBB20_11
.LBB20_7:                               # %.preheader.preheader
	movq	(%rsp), %rax            # 8-byte Reload
	leal	-1(%rax), %ecx
	movl	%eax, %edx
	xorl	%eax, %eax
	andl	$7, %edx
	je	.LBB20_9
	.p2align	4, 0x90
.LBB20_8:                               # %.preheader.prol
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	cmpl	%eax, %edx
	jne	.LBB20_8
.LBB20_9:                               # %.preheader.prol.loopexit
	cmpl	$7, %ecx
	jb	.LBB20_11
	.p2align	4, 0x90
.LBB20_10:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	addl	$8, %eax
	cmpl	(%rsp), %eax            # 4-byte Folded Reload
	jb	.LBB20_10
.LBB20_11:                              # %._crit_edge24
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB20_12:                              # %.us-lcssa.us
.Ltmp37:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end20:
	.size	_ZN5Ocean13addEmptyCellsEv, .Lfunc_end20-_ZN5Ocean13addEmptyCellsEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table20:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp35-.Lfunc_begin6   #   Call between .Lfunc_begin6 and .Ltmp35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin6   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin6   # >> Call Site 3 <<
	.long	.Lfunc_end20-.Ltmp36    #   Call between .Ltmp36 and .Lfunc_end20
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN5Ocean12addObstaclesEv
	.p2align	4, 0x90
	.type	_ZN5Ocean12addObstaclesEv,@function
_ZN5Ocean12addObstaclesEv:              # @_ZN5Ocean12addObstaclesEv
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%rbp
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi84:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi85:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi87:
	.cfi_def_cfa_offset 64
.Lcfi88:
	.cfi_offset %rbx, -56
.Lcfi89:
	.cfi_offset %r12, -48
.Lcfi90:
	.cfi_offset %r13, -40
.Lcfi91:
	.cfi_offset %r14, -32
.Lcfi92:
	.cfi_offset %r15, -24
.Lcfi93:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpl	$0, 16(%r14)
	je	.LBB21_8
# BB#1:                                 # %.lr.ph
	xorl	%r15d, %r15d
	movabsq	$-9223372032559808509, %r12 # imm = 0x8000000100000003
	.p2align	4, 0x90
.LBB21_2:                               # =>This Inner Loop Header: Depth=1
	movl	4(%r14), %ebx
	decl	%ebx
	callq	random
	movslq	%ebx, %rcx
	imulq	%rax, %rcx
	movq	%rcx, %rax
	imulq	%r12
	movq	%rdx, %rbx
	addq	%rcx, %rbx
	movq	%rbx, %rax
	shrq	$63, %rax
	shrq	$30, %rbx
	addl	%eax, %ebx
	movl	(%r14), %ebp
	decl	%ebp
	callq	random
	movslq	%ebp, %rcx
	imulq	%rax, %rcx
	movq	%rcx, %rax
	imulq	%r12
	addq	%rcx, %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$30, %rdx
	addl	%eax, %edx
	imulq	$8000, %rdx, %rax       # imm = 0x1F40
	movq	cells(%rax,%rbx,8), %rdi
	cmpb	$45, 16(%rdi)
	jne	.LBB21_2
# BB#3:                                 #   in Loop: Header=BB21_2 Depth=1
	movq	8(%rdi), %rax
	movl	(%rax), %ebp
	movl	4(%rax), %r13d
	testq	%rdi, %rdi
	je	.LBB21_5
# BB#4:                                 #   in Loop: Header=BB21_2 Depth=1
	movq	(%rdi), %rax
.Ltmp38:
	callq	*16(%rax)
.Ltmp39:
.LBB21_5:                               # %_ZN5Ocean17getEmptyCellCoordEv.exit
                                        #   in Loop: Header=BB21_2 Depth=1
.Ltmp41:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp42:
# BB#6:                                 #   in Loop: Header=BB21_2 Depth=1
	movq	$_ZTV4Cell+16, (%rbx)
.Ltmp44:
	movl	$8, %edi
	callq	_Znwm
.Ltmp45:
# BB#7:                                 #   in Loop: Header=BB21_2 Depth=1
	movl	%ebp, (%rax)
	movl	%r13d, 4(%rax)
	movq	%rax, 8(%rbx)
	movq	$_ZTV8Obstacle+16, (%rbx)
	movb	$35, 16(%rbx)
	imulq	$8000, %r13, %rax       # imm = 0x1F40
	movq	%rbx, cells(%rax,%rbp,8)
	incl	%r15d
	cmpl	16(%r14), %r15d
	jb	.LBB21_2
.LBB21_8:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB21_9:
.Ltmp40:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB21_11:
.Ltmp46:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB21_10:
.Ltmp43:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end21:
	.size	_ZN5Ocean12addObstaclesEv, .Lfunc_end21-_ZN5Ocean12addObstaclesEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp38-.Lfunc_begin7   # >> Call Site 1 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin7   #     jumps to .Ltmp40
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin7   # >> Call Site 2 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin7   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin7   # >> Call Site 3 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin7   #     jumps to .Ltmp46
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin7   # >> Call Site 4 <<
	.long	.Lfunc_end21-.Ltmp45    #   Call between .Ltmp45 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN5Ocean12addPredatorsEv
	.p2align	4, 0x90
	.type	_ZN5Ocean12addPredatorsEv,@function
_ZN5Ocean12addPredatorsEv:              # @_ZN5Ocean12addPredatorsEv
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%rbp
.Lcfi94:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi95:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi96:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi97:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi98:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi100:
	.cfi_def_cfa_offset 64
.Lcfi101:
	.cfi_offset %rbx, -56
.Lcfi102:
	.cfi_offset %r12, -48
.Lcfi103:
	.cfi_offset %r13, -40
.Lcfi104:
	.cfi_offset %r14, -32
.Lcfi105:
	.cfi_offset %r15, -24
.Lcfi106:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpl	$0, 12(%r14)
	je	.LBB22_8
# BB#1:                                 # %.lr.ph
	xorl	%r15d, %r15d
	movabsq	$-9223372032559808509, %r12 # imm = 0x8000000100000003
	.p2align	4, 0x90
.LBB22_2:                               # =>This Inner Loop Header: Depth=1
	movl	4(%r14), %ebx
	decl	%ebx
	callq	random
	movslq	%ebx, %rcx
	imulq	%rax, %rcx
	movq	%rcx, %rax
	imulq	%r12
	movq	%rdx, %rbx
	addq	%rcx, %rbx
	movq	%rbx, %rax
	shrq	$63, %rax
	shrq	$30, %rbx
	addl	%eax, %ebx
	movl	(%r14), %ebp
	decl	%ebp
	callq	random
	movslq	%ebp, %rcx
	imulq	%rax, %rcx
	movq	%rcx, %rax
	imulq	%r12
	addq	%rcx, %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$30, %rdx
	addl	%eax, %edx
	imulq	$8000, %rdx, %rax       # imm = 0x1F40
	movq	cells(%rax,%rbx,8), %rdi
	cmpb	$45, 16(%rdi)
	jne	.LBB22_2
# BB#3:                                 #   in Loop: Header=BB22_2 Depth=1
	movq	8(%rdi), %rax
	movl	(%rax), %ebp
	movl	4(%rax), %r13d
	testq	%rdi, %rdi
	je	.LBB22_5
# BB#4:                                 #   in Loop: Header=BB22_2 Depth=1
	movq	(%rdi), %rax
.Ltmp47:
	callq	*16(%rax)
.Ltmp48:
.LBB22_5:                               # %_ZN5Ocean17getEmptyCellCoordEv.exit
                                        #   in Loop: Header=BB22_2 Depth=1
.Ltmp50:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp51:
# BB#6:                                 #   in Loop: Header=BB22_2 Depth=1
	movq	$_ZTV4Cell+16, (%rbx)
.Ltmp53:
	movl	$8, %edi
	callq	_Znwm
.Ltmp54:
# BB#7:                                 #   in Loop: Header=BB22_2 Depth=1
	movl	%ebp, (%rax)
	movl	%r13d, 4(%rax)
	movq	%rax, 8(%rbx)
	movl	$6, 20(%rbx)
	movq	$_ZTV8Predator+16, (%rbx)
	movl	$6, 24(%rbx)
	movb	$83, 16(%rbx)
	imulq	$8000, %r13, %rax       # imm = 0x1F40
	movq	%rbx, cells(%rax,%rbp,8)
	incl	%r15d
	cmpl	12(%r14), %r15d
	jb	.LBB22_2
.LBB22_8:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB22_9:
.Ltmp49:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB22_11:
.Ltmp55:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB22_10:
.Ltmp52:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end22:
	.size	_ZN5Ocean12addPredatorsEv, .Lfunc_end22-_ZN5Ocean12addPredatorsEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp47-.Lfunc_begin8   # >> Call Site 1 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin8   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin8   # >> Call Site 2 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin8   #     jumps to .Ltmp52
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin8   # >> Call Site 3 <<
	.long	.Ltmp54-.Ltmp53         #   Call between .Ltmp53 and .Ltmp54
	.long	.Ltmp55-.Lfunc_begin8   #     jumps to .Ltmp55
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin8   # >> Call Site 4 <<
	.long	.Lfunc_end22-.Ltmp54    #   Call between .Ltmp54 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN5Ocean7addPreyEv
	.p2align	4, 0x90
	.type	_ZN5Ocean7addPreyEv,@function
_ZN5Ocean7addPreyEv:                    # @_ZN5Ocean7addPreyEv
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%rbp
.Lcfi107:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi108:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi109:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi110:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi111:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi112:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi113:
	.cfi_def_cfa_offset 64
.Lcfi114:
	.cfi_offset %rbx, -56
.Lcfi115:
	.cfi_offset %r12, -48
.Lcfi116:
	.cfi_offset %r13, -40
.Lcfi117:
	.cfi_offset %r14, -32
.Lcfi118:
	.cfi_offset %r15, -24
.Lcfi119:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpl	$0, 12(%r14)
	je	.LBB23_8
# BB#1:                                 # %.lr.ph
	xorl	%r15d, %r15d
	movabsq	$-9223372032559808509, %r12 # imm = 0x8000000100000003
	.p2align	4, 0x90
.LBB23_2:                               # =>This Inner Loop Header: Depth=1
	movl	4(%r14), %ebx
	decl	%ebx
	callq	random
	movslq	%ebx, %rcx
	imulq	%rax, %rcx
	movq	%rcx, %rax
	imulq	%r12
	movq	%rdx, %rbx
	addq	%rcx, %rbx
	movq	%rbx, %rax
	shrq	$63, %rax
	shrq	$30, %rbx
	addl	%eax, %ebx
	movl	(%r14), %ebp
	decl	%ebp
	callq	random
	movslq	%ebp, %rcx
	imulq	%rax, %rcx
	movq	%rcx, %rax
	imulq	%r12
	addq	%rcx, %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$30, %rdx
	addl	%eax, %edx
	imulq	$8000, %rdx, %rax       # imm = 0x1F40
	movq	cells(%rax,%rbx,8), %rdi
	cmpb	$45, 16(%rdi)
	jne	.LBB23_2
# BB#3:                                 #   in Loop: Header=BB23_2 Depth=1
	movq	8(%rdi), %rax
	movl	(%rax), %ebp
	movl	4(%rax), %r13d
	testq	%rdi, %rdi
	je	.LBB23_5
# BB#4:                                 #   in Loop: Header=BB23_2 Depth=1
	movq	(%rdi), %rax
.Ltmp56:
	callq	*16(%rax)
.Ltmp57:
.LBB23_5:                               # %_ZN5Ocean17getEmptyCellCoordEv.exit
                                        #   in Loop: Header=BB23_2 Depth=1
.Ltmp59:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp60:
# BB#6:                                 #   in Loop: Header=BB23_2 Depth=1
	movq	$_ZTV4Cell+16, (%rbx)
.Ltmp62:
	movl	$8, %edi
	callq	_Znwm
.Ltmp63:
# BB#7:                                 #   in Loop: Header=BB23_2 Depth=1
	movl	%ebp, (%rax)
	movl	%r13d, 4(%rax)
	movq	%rax, 8(%rbx)
	movq	$_ZTV4Prey+16, (%rbx)
	movl	$6, 20(%rbx)
	movb	$102, 16(%rbx)
	imulq	$8000, %r13, %rax       # imm = 0x1F40
	movq	%rbx, cells(%rax,%rbp,8)
	incl	%r15d
	cmpl	12(%r14), %r15d
	jb	.LBB23_2
.LBB23_8:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB23_9:
.Ltmp58:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB23_11:
.Ltmp64:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB23_10:
.Ltmp61:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end23:
	.size	_ZN5Ocean7addPreyEv, .Lfunc_end23-_ZN5Ocean7addPreyEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp56-.Lfunc_begin9   # >> Call Site 1 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin9   #     jumps to .Ltmp58
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin9   # >> Call Site 2 <<
	.long	.Ltmp60-.Ltmp59         #   Call between .Ltmp59 and .Ltmp60
	.long	.Ltmp61-.Lfunc_begin9   #     jumps to .Ltmp61
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin9   # >> Call Site 3 <<
	.long	.Ltmp63-.Ltmp62         #   Call between .Ltmp62 and .Ltmp63
	.long	.Ltmp64-.Lfunc_begin9   #     jumps to .Ltmp64
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin9   # >> Call Site 4 <<
	.long	.Lfunc_end23-.Ltmp63    #   Call between .Ltmp63 and .Lfunc_end23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN5Ocean12displayStatsEi
	.p2align	4, 0x90
	.type	_ZN5Ocean12displayStatsEi,@function
_ZN5Ocean12displayStatsEi:              # @_ZN5Ocean12displayStatsEi
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end24:
	.size	_ZN5Ocean12displayStatsEi, .Lfunc_end24-_ZN5Ocean12displayStatsEi
	.cfi_endproc

	.globl	_ZN5Ocean13displayBorderEv
	.p2align	4, 0x90
	.type	_ZN5Ocean13displayBorderEv,@function
_ZN5Ocean13displayBorderEv:             # @_ZN5Ocean13displayBorderEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end25:
	.size	_ZN5Ocean13displayBorderEv, .Lfunc_end25-_ZN5Ocean13displayBorderEv
	.cfi_endproc

	.globl	_ZN5Ocean17getEmptyCellCoordEv
	.p2align	4, 0x90
	.type	_ZN5Ocean17getEmptyCellCoordEv,@function
_ZN5Ocean17getEmptyCellCoordEv:         # @_ZN5Ocean17getEmptyCellCoordEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi120:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi121:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi123:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 48
.Lcfi125:
	.cfi_offset %rbx, -48
.Lcfi126:
	.cfi_offset %r12, -40
.Lcfi127:
	.cfi_offset %r14, -32
.Lcfi128:
	.cfi_offset %r15, -24
.Lcfi129:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movabsq	$-9223372032559808509, %r12 # imm = 0x8000000100000003
	.p2align	4, 0x90
.LBB26_1:                               # =>This Inner Loop Header: Depth=1
	movl	4(%r15), %ebx
	decl	%ebx
	callq	random
	movslq	%ebx, %rcx
	imulq	%rax, %rcx
	movq	%rcx, %rax
	imulq	%r12
	movq	%rdx, %rbx
	addq	%rcx, %rbx
	movq	%rbx, %rax
	shrq	$63, %rax
	shrq	$30, %rbx
	addl	%eax, %ebx
	movl	(%r15), %ebp
	decl	%ebp
	callq	random
	movslq	%ebp, %rcx
	imulq	%rax, %rcx
	movq	%rcx, %rax
	imulq	%r12
	addq	%rcx, %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$30, %rdx
	addl	%eax, %edx
	imulq	$8000, %rdx, %rax       # imm = 0x1F40
	movq	cells(%rax,%rbx,8), %rdi
	cmpb	$45, 16(%rdi)
	jne	.LBB26_1
# BB#2:
	movq	8(%rdi), %rax
	movl	(%rax), %ecx
	movl	%ecx, (%r14)
	movl	4(%rax), %eax
	movl	%eax, 4(%r14)
	testq	%rdi, %rdi
	je	.LBB26_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB26_4:
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	_ZN5Ocean17getEmptyCellCoordEv, .Lfunc_end26-_ZN5Ocean17getEmptyCellCoordEv
	.cfi_endproc

	.globl	_ZN5Ocean12displayCellsEv
	.p2align	4, 0x90
	.type	_ZN5Ocean12displayCellsEv,@function
_ZN5Ocean12displayCellsEv:              # @_ZN5Ocean12displayCellsEv
	.cfi_startproc
# BB#0:                                 # %._crit_edge
	retq
.Lfunc_end27:
	.size	_ZN5Ocean12displayCellsEv, .Lfunc_end27-_ZN5Ocean12displayCellsEv
	.cfi_endproc

	.globl	_ZN5Ocean3runEv
	.p2align	4, 0x90
	.type	_ZN5Ocean3runEv,@function
_ZN5Ocean3runEv:                        # @_ZN5Ocean3runEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi130:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi131:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi133:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi134:
	.cfi_def_cfa_offset 48
.Lcfi135:
	.cfi_offset %rbx, -48
.Lcfi136:
	.cfi_offset %r12, -40
.Lcfi137:
	.cfi_offset %r14, -32
.Lcfi138:
	.cfi_offset %r15, -24
.Lcfi139:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB28_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB28_5 Depth 2
                                        #       Child Loop BB28_7 Depth 3
	cmpl	$0, 12(%r12)
	je	.LBB28_10
# BB#2:                                 #   in Loop: Header=BB28_1 Depth=1
	cmpl	$0, 8(%r12)
	je	.LBB28_10
# BB#3:                                 # %.preheader20
                                        #   in Loop: Header=BB28_1 Depth=1
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.LBB28_10
# BB#4:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB28_1 Depth=1
	movl	4(%r12), %ecx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB28_5:                               # %.preheader
                                        #   Parent Loop BB28_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB28_7 Depth 3
	testl	%ecx, %ecx
	movl	$0, %ecx
	je	.LBB28_9
# BB#6:                                 # %.lr.ph
                                        #   in Loop: Header=BB28_5 Depth=2
	movl	%r15d, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB28_7:                               #   Parent Loop BB28_1 Depth=1
                                        #     Parent Loop BB28_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ebx, %eax
	imulq	$8000, %rbp, %rcx       # imm = 0x1F40
	movq	cells(%rcx,%rax,8), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	incl	%ebx
	movl	4(%r12), %ecx
	cmpl	%ecx, %ebx
	jb	.LBB28_7
# BB#8:                                 # %._crit_edge.loopexit
                                        #   in Loop: Header=BB28_5 Depth=2
	movl	(%r12), %eax
.LBB28_9:                               # %._crit_edge
                                        #   in Loop: Header=BB28_5 Depth=2
	incl	%r15d
	cmpl	%eax, %r15d
	jb	.LBB28_5
.LBB28_10:                              # %.loopexit
                                        #   in Loop: Header=BB28_1 Depth=1
	incl	%r14d
	cmpl	$1000, %r14d            # imm = 0x3E8
	jne	.LBB28_1
# BB#11:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end28:
	.size	_ZN5Ocean3runEv, .Lfunc_end28-_ZN5Ocean3runEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI29_0:
	.long	500                     # 0x1f4
	.long	1000                    # 0x3e8
	.long	1000                    # 0x3e8
	.long	200                     # 0xc8
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi140:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi141:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi142:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi143:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi144:
	.cfi_def_cfa_offset 48
.Lcfi145:
	.cfi_offset %rbx, -48
.Lcfi146:
	.cfi_offset %r12, -40
.Lcfi147:
	.cfi_offset %r14, -32
.Lcfi148:
	.cfi_offset %r15, -24
.Lcfi149:
	.cfi_offset %rbp, -16
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %r12
	movl	$8, %edi
	callq	_Znwm
	movq	%rax, 24(%r12)
	movl	$3797, (%rax)           # imm = 0xED5
	movl	$2117, 4(%rax)          # imm = 0x845
	movl	$750, 16(%r12)          # imm = 0x2EE
	movaps	.LCPI29_0(%rip), %xmm0  # xmm0 = [500,1000,1000,200]
	movups	%xmm0, (%r12)
	movq	%r12, %rdi
	callq	_ZN5Ocean9initCellsEv
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB29_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB29_5 Depth 2
                                        #       Child Loop BB29_7 Depth 3
	cmpl	$0, 12(%r12)
	je	.LBB29_10
# BB#2:                                 #   in Loop: Header=BB29_1 Depth=1
	cmpl	$0, 8(%r12)
	je	.LBB29_10
# BB#3:                                 # %.preheader20.i
                                        #   in Loop: Header=BB29_1 Depth=1
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.LBB29_10
# BB#4:                                 # %.preheader.preheader.i
                                        #   in Loop: Header=BB29_1 Depth=1
	movl	4(%r12), %ecx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB29_5:                               # %.preheader.i
                                        #   Parent Loop BB29_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB29_7 Depth 3
	testl	%ecx, %ecx
	movl	$0, %ecx
	je	.LBB29_9
# BB#6:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB29_5 Depth=2
	movl	%r15d, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB29_7:                               #   Parent Loop BB29_1 Depth=1
                                        #     Parent Loop BB29_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ebx, %eax
	imulq	$8000, %rbp, %rcx       # imm = 0x1F40
	movq	cells(%rcx,%rax,8), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	incl	%ebx
	movl	4(%r12), %ecx
	cmpl	%ecx, %ebx
	jb	.LBB29_7
# BB#8:                                 # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB29_5 Depth=2
	movl	(%r12), %eax
.LBB29_9:                               # %._crit_edge.i
                                        #   in Loop: Header=BB29_5 Depth=2
	incl	%r15d
	cmpl	%eax, %r15d
	jb	.LBB29_5
.LBB29_10:                              # %.loopexit.i
                                        #   in Loop: Header=BB29_1 Depth=1
	incl	%r14d
	cmpl	$1000, %r14d            # imm = 0x3E8
	jne	.LBB29_1
# BB#11:                                # %_ZN5Ocean3runEv.exit
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end29:
	.size	main, .Lfunc_end29-main
	.cfi_endproc

	.section	.text._ZN4CellD0Ev,"axG",@progbits,_ZN4CellD0Ev,comdat
	.weak	_ZN4CellD0Ev
	.p2align	4, 0x90
	.type	_ZN4CellD0Ev,@function
_ZN4CellD0Ev:                           # @_ZN4CellD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi150:
	.cfi_def_cfa_offset 16
.Lcfi151:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV4Cell+16, (%rbx)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB30_2
# BB#1:
	callq	_ZdlPv
.LBB30_2:                               # %_ZN4CellD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end30:
	.size	_ZN4CellD0Ev, .Lfunc_end30-_ZN4CellD0Ev
	.cfi_endproc

	.section	.text._ZN4Cell7processEv,"axG",@progbits,_ZN4Cell7processEv,comdat
	.weak	_ZN4Cell7processEv
	.p2align	4, 0x90
	.type	_ZN4Cell7processEv,@function
_ZN4Cell7processEv:                     # @_ZN4Cell7processEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end31:
	.size	_ZN4Cell7processEv, .Lfunc_end31-_ZN4Cell7processEv
	.cfi_endproc

	.section	.text._ZN4PreyD0Ev,"axG",@progbits,_ZN4PreyD0Ev,comdat
	.weak	_ZN4PreyD0Ev
	.p2align	4, 0x90
	.type	_ZN4PreyD0Ev,@function
_ZN4PreyD0Ev:                           # @_ZN4PreyD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi152:
	.cfi_def_cfa_offset 16
.Lcfi153:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV4Cell+16, (%rbx)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_2
# BB#1:
	callq	_ZdlPv
.LBB32_2:                               # %_ZN4CellD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end32:
	.size	_ZN4PreyD0Ev, .Lfunc_end32-_ZN4PreyD0Ev
	.cfi_endproc

	.section	.text._ZN8PredatorD0Ev,"axG",@progbits,_ZN8PredatorD0Ev,comdat
	.weak	_ZN8PredatorD0Ev
	.p2align	4, 0x90
	.type	_ZN8PredatorD0Ev,@function
_ZN8PredatorD0Ev:                       # @_ZN8PredatorD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi154:
	.cfi_def_cfa_offset 16
.Lcfi155:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV4Cell+16, (%rbx)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB33_2
# BB#1:
	callq	_ZdlPv
.LBB33_2:                               # %_ZN4CellD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end33:
	.size	_ZN8PredatorD0Ev, .Lfunc_end33-_ZN8PredatorD0Ev
	.cfi_endproc

	.section	.text._ZN4CellD2Ev,"axG",@progbits,_ZN4CellD2Ev,comdat
	.weak	_ZN4CellD2Ev
	.p2align	4, 0x90
	.type	_ZN4CellD2Ev,@function
_ZN4CellD2Ev:                           # @_ZN4CellD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV4Cell+16, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB34_1
# BB#2:
	jmp	_ZdlPv                  # TAILCALL
.LBB34_1:
	retq
.Lfunc_end34:
	.size	_ZN4CellD2Ev, .Lfunc_end34-_ZN4CellD2Ev
	.cfi_endproc

	.section	.text._ZN8ObstacleD0Ev,"axG",@progbits,_ZN8ObstacleD0Ev,comdat
	.weak	_ZN8ObstacleD0Ev
	.p2align	4, 0x90
	.type	_ZN8ObstacleD0Ev,@function
_ZN8ObstacleD0Ev:                       # @_ZN8ObstacleD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi156:
	.cfi_def_cfa_offset 16
.Lcfi157:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV4Cell+16, (%rbx)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB35_2
# BB#1:
	callq	_ZdlPv
.LBB35_2:                               # %_ZN4CellD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end35:
	.size	_ZN8ObstacleD0Ev, .Lfunc_end35-_ZN8ObstacleD0Ev
	.cfi_endproc

	.type	cells,@object           # @cells
	.bss
	.globl	cells
	.p2align	4
cells:
	.zero	4000000
	.size	cells, 4000000

	.type	Ocean1,@object          # @Ocean1
	.globl	Ocean1
	.p2align	3
Ocean1:
	.quad	0
	.size	Ocean1, 8

	.type	_ZTV4Cell,@object       # @_ZTV4Cell
	.section	.rodata,"a",@progbits
	.globl	_ZTV4Cell
	.p2align	3
_ZTV4Cell:
	.quad	0
	.quad	_ZTI4Cell
	.quad	_ZN4Cell9reproduceE10Coordinate
	.quad	_ZN4CellD2Ev
	.quad	_ZN4CellD0Ev
	.quad	_ZN4Cell7processEv
	.size	_ZTV4Cell, 48

	.type	_ZTS4Cell,@object       # @_ZTS4Cell
	.globl	_ZTS4Cell
_ZTS4Cell:
	.asciz	"4Cell"
	.size	_ZTS4Cell, 6

	.type	_ZTI4Cell,@object       # @_ZTI4Cell
	.globl	_ZTI4Cell
	.p2align	3
_ZTI4Cell:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS4Cell
	.size	_ZTI4Cell, 16

	.type	_ZTV4Prey,@object       # @_ZTV4Prey
	.globl	_ZTV4Prey
	.p2align	3
_ZTV4Prey:
	.quad	0
	.quad	_ZTI4Prey
	.quad	_ZN4Prey9reproduceE10Coordinate
	.quad	_ZN4CellD2Ev
	.quad	_ZN4PreyD0Ev
	.quad	_ZN4Prey7processEv
	.size	_ZTV4Prey, 48

	.type	_ZTS4Prey,@object       # @_ZTS4Prey
	.globl	_ZTS4Prey
_ZTS4Prey:
	.asciz	"4Prey"
	.size	_ZTS4Prey, 6

	.type	_ZTI4Prey,@object       # @_ZTI4Prey
	.globl	_ZTI4Prey
	.p2align	4
_ZTI4Prey:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS4Prey
	.quad	_ZTI4Cell
	.size	_ZTI4Prey, 24

	.type	_ZTV8Predator,@object   # @_ZTV8Predator
	.globl	_ZTV8Predator
	.p2align	3
_ZTV8Predator:
	.quad	0
	.quad	_ZTI8Predator
	.quad	_ZN8Predator9reproduceE10Coordinate
	.quad	_ZN4CellD2Ev
	.quad	_ZN8PredatorD0Ev
	.quad	_ZN8Predator7processEv
	.size	_ZTV8Predator, 48

	.type	_ZTS8Predator,@object   # @_ZTS8Predator
	.globl	_ZTS8Predator
_ZTS8Predator:
	.asciz	"8Predator"
	.size	_ZTS8Predator, 10

	.type	_ZTI8Predator,@object   # @_ZTI8Predator
	.globl	_ZTI8Predator
	.p2align	4
_ZTI8Predator:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS8Predator
	.quad	_ZTI4Prey
	.size	_ZTI8Predator, 24

	.type	_ZTV8Obstacle,@object   # @_ZTV8Obstacle
	.section	.rodata._ZTV8Obstacle,"aG",@progbits,_ZTV8Obstacle,comdat
	.weak	_ZTV8Obstacle
	.p2align	3
_ZTV8Obstacle:
	.quad	0
	.quad	_ZTI8Obstacle
	.quad	_ZN4Cell9reproduceE10Coordinate
	.quad	_ZN4CellD2Ev
	.quad	_ZN8ObstacleD0Ev
	.quad	_ZN4Cell7processEv
	.size	_ZTV8Obstacle, 48

	.type	_ZTS8Obstacle,@object   # @_ZTS8Obstacle
	.section	.rodata._ZTS8Obstacle,"aG",@progbits,_ZTS8Obstacle,comdat
	.weak	_ZTS8Obstacle
_ZTS8Obstacle:
	.asciz	"8Obstacle"
	.size	_ZTS8Obstacle, 10

	.type	_ZTI8Obstacle,@object   # @_ZTI8Obstacle
	.section	.rodata._ZTI8Obstacle,"aG",@progbits,_ZTI8Obstacle,comdat
	.weak	_ZTI8Obstacle
	.p2align	4
_ZTI8Obstacle:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS8Obstacle
	.quad	_ZTI4Cell
	.size	_ZTI8Obstacle, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
