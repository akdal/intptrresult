	.text
	.file	"libclamav_aspack.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	3140                    # 0xc44
	.quad	3252                    # 0xcb4
.LCPI0_1:
	.quad	3508                    # 0xdb4
	.quad	3540                    # 0xdd4
.LCPI0_2:
	.quad	3796                    # 0xed4
	.quad	3872                    # 0xf20
.LCPI0_3:
	.zero	16
	.text
	.globl	unaspack212
	.p2align	4, 0x90
	.type	unaspack212,@function
unaspack212:                            # @unaspack212
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$2088, %rsp             # imm = 0x828
.Lcfi6:
	.cfi_def_cfa_offset 2144
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movl	%r8d, %r15d
	movl	%ecx, 92(%rsp)          # 4-byte Spill
	movq	%rdx, %rbp
	movl	%esi, %r13d
	movq	%rdi, %rbx
	movl	$6144, %edi             # imm = 0x1800
	movl	$1, %esi
	callq	cli_calloc
	testq	%rax, %rax
	je	.LBB0_1
# BB#3:
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	movl	%r14d, 88(%rsp)         # 4-byte Spill
	movl	%r15d, %r14d
	leaq	(%rbx,%r14), %rcx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	leaq	1404(%rbx,%r14), %rbp
	movq	%rax, 1192(%rsp)
	leaq	2884(%rax), %rcx
	movq	%rcx, 1200(%rsp)
	movl	$721, 1208(%rsp)        # imm = 0x2D1
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqa	.LCPI0_0(%rip), %xmm1   # xmm1 = [3140,3252]
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 1216(%rsp)
	movl	$28, 1232(%rsp)
	movdqa	.LCPI0_1(%rip), %xmm1   # xmm1 = [3508,3540]
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 1240(%rsp)
	movl	$8, 1256(%rsp)
	paddq	.LCPI0_2(%rip), %xmm0
	movdqu	%xmm0, 1264(%rsp)
	movl	$19, 1280(%rsp)
	addq	$4128, %rax             # imm = 0x1020
	movq	%rax, 1304(%rsp)
	movl	$65536, 172(%rsp)       # imm = 0x10000
	xorl	%eax, %eax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_4:                                # =>This Inner Loop Header: Depth=1
	movl	%edx, 956(%rsp,%rax,4)
	leal	1806(%r14,%rax), %ecx
	movzbl	(%rbx,%rcx), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	addl	%edx, %esi
	movl	%esi, 960(%rsp,%rax,4)
	leal	1807(%r14,%rax), %ecx
	movzbl	(%rbx,%rcx), %ecx
	movl	$1, %edx
	shll	%cl, %edx
	addl	%esi, %edx
	addq	$2, %rax
	cmpq	$58, %rax
	jne	.LBB0_4
# BB#5:
	leaq	176(%rsp), %rdi
	movl	%r13d, %eax
	addq	%rbx, %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	leal	1750(%r15), %eax
	addq	%rbx, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	addl	$328, %r15d             # imm = 0x148
	movq	%r15, 152(%rsp)         # 8-byte Spill
	xorl	%esi, %esi
	movl	$777, %edx              # imm = 0x309
	callq	memset
	movl	$1, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	168(%rsp), %r12
	xorl	%edi, %edi
	movl	%r13d, 12(%rsp)         # 4-byte Spill
.LBB0_6:                                # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_7 Depth 2
                                        #       Child Loop BB0_23 Depth 3
                                        #         Child Loop BB0_47 Depth 4
                                        #         Child Loop BB0_53 Depth 4
                                        #         Child Loop BB0_74 Depth 4
                                        #         Child Loop BB0_78 Depth 4
                                        #     Child Loop BB0_35 Depth 2
	cmpl	$8, %r13d
	sbbb	%cl, %cl
	cmpq	%rbx, %rbp
	sbbb	%al, %al
	orb	%cl, %al
	andb	$1, %al
.LBB0_7:                                #   Parent Loop BB0_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_23 Depth 3
                                        #         Child Loop BB0_47 Depth 4
                                        #         Child Loop BB0_53 Depth 4
                                        #         Child Loop BB0_74 Depth 4
                                        #         Child Loop BB0_78 Depth 4
	testb	%al, %al
	jne	.LBB0_28
# BB#8:                                 #   in Loop: Header=BB0_7 Depth=2
	leaq	8(%rbp), %rcx
	cmpq	128(%rsp), %rcx         # 8-byte Folded Reload
	ja	.LBB0_28
# BB#9:                                 #   in Loop: Header=BB0_7 Depth=2
	cmpq	%rbx, %rcx
	jbe	.LBB0_28
# BB#10:                                #   in Loop: Header=BB0_7 Depth=2
	movl	(%rbp), %esi
	movq	%rsi, %rdx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	testq	%rsi, %rsi
	je	.LBB0_11
# BB#14:                                #   in Loop: Header=BB0_7 Depth=2
	movb	%al, 11(%rsp)           # 1-byte Spill
	movq	%rdi, 136(%rsp)         # 8-byte Spill
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movl	4(%rbp), %r15d
	leal	-1(%r15), %eax
	cmpl	%r13d, %eax
	jae	.LBB0_20
# BB#15:                                #   in Loop: Header=BB0_7 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%rbx,%rax), %rcx
	leaq	(%rcx,%r15), %rax
	cmpq	128(%rsp), %rax         # 8-byte Folded Reload
	ja	.LBB0_20
# BB#16:                                #   in Loop: Header=BB0_7 Depth=2
	cmpq	%rbx, %rax
	jbe	.LBB0_20
# BB#17:                                #   in Loop: Header=BB0_7 Depth=2
	leal	270(%r15), %ebp
	movl	$1, %esi
	movq	%rbp, %rdi
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	callq	cli_calloc
	movq	64(%rsp), %rsi          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB0_20
# BB#18:                                #   in Loop: Header=BB0_7 Depth=2
	movq	%rax, 1288(%rsp)
	addq	%rax, %rbp
	movq	%rbp, 1296(%rsp)
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	movq	%r15, %rdx
	callq	memcpy
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	48(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	callq	cli_dbgmsg
	xorl	%esi, %esi
	movl	$768, %edx              # imm = 0x300
	leaq	1312(%rsp), %rdi
	callq	memset
	movq	1304(%rsp), %rdi
	xorl	%esi, %esi
	movl	$757, %edx              # imm = 0x2F5
	callq	memset
	movl	$32, 168(%rsp)
	movq	%r12, %rdi
	callq	build_decrypt_dictionaries
	testl	%eax, %eax
	je	.LBB0_19
# BB#22:                                # %.outer.outer.i.i..outer.outer.i.i.split_crit_edge.preheader
                                        #   in Loop: Header=BB0_7 Depth=2
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 96(%rsp)
	xorl	%ebp, %ebp
	movl	%r15d, %eax
	movl	%eax, 60(%rsp)          # 4-byte Spill
	movl	$0, 44(%rsp)            # 4-byte Folded Spill
	jmp	.LBB0_23
.LBB0_39:                               # %.us-lcssa102.us.i.i
                                        #   in Loop: Header=BB0_23 Depth=3
	addl	$-256, %r13d
	cmpl	$463, %r13d             # imm = 0x1CF
	ja	.LBB0_27
# BB#40:                                #   in Loop: Header=BB0_23 Depth=3
	movl	%r13d, %eax
	andl	$7, %eax
	leal	2(%rax), %r10d
	cmpl	$7, %eax
	jne	.LBB0_41
# BB#42:                                #   in Loop: Header=BB0_23 Depth=3
	movq	%r10, 32(%rsp)          # 8-byte Spill
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	28(%rsp), %rdx
	callq	getdec
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$85, %eax
	ja	.LBB0_27
# BB#43:                                #   in Loop: Header=BB0_23 Depth=3
	movl	28(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.LBB0_27
# BB#44:                                #   in Loop: Header=BB0_23 Depth=3
	movl	%ebp, %r9d
	leal	28(%rax), %ecx
	movq	72(%rsp), %rdx          # 8-byte Reload
	movzbl	(%rdx,%rcx), %r8d
	movl	168(%rsp), %edx
	cmpl	$7, %edx
	jbe	.LBB0_45
# BB#46:                                # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB0_23 Depth=3
	movq	1288(%rsp), %rcx
	movq	1296(%rsp), %rsi
	movq	32(%rsp), %r10          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_47:                               #   Parent Loop BB0_6 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        #       Parent Loop BB0_23 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	%rsi, %rcx
	jae	.LBB0_27
# BB#48:                                #   in Loop: Header=BB0_47 Depth=4
	movl	172(%rsp), %ebp
	shll	$8, %ebp
	movzbl	(%rcx), %edi
	orl	%ebp, %edi
	movl	%edi, 172(%rsp)
	incq	%rcx
	movq	%rcx, 1288(%rsp)
	addl	$-8, %edx
	movl	%edx, 168(%rsp)
	cmpl	$7, %edx
	ja	.LBB0_47
	jmp	.LBB0_49
.LBB0_41:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB0_23 Depth=3
	movl	168(%rsp), %edx
	movq	72(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB0_50
.LBB0_45:                               # %.readstream.exit_crit_edge.i.i
                                        #   in Loop: Header=BB0_23 Depth=3
	movl	172(%rsp), %edi
	movq	32(%rsp), %r10          # 8-byte Reload
.LBB0_49:                               # %readstream.exit.i.i
                                        #   in Loop: Header=BB0_23 Depth=3
	movl	%eax, %eax
	movq	72(%rsp), %rsi          # 8-byte Reload
	movzbl	(%rsi,%rax), %eax
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edi
	andl	$16777215, %edi         # imm = 0xFFFFFF
	movl	$24, %ecx
	subl	%r8d, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edi
	addl	%r10d, %eax
	addl	%edi, %eax
	addl	%r8d, %edx
	movl	%edx, 168(%rsp)
	movl	%eax, %r10d
	movl	%r9d, %ebp
.LBB0_50:                               #   in Loop: Header=BB0_23 Depth=3
	shrl	$3, %r13d
	movl	%r13d, %ecx
	movzbl	56(%rsi,%rcx), %r8d
	cmpl	$3, %r8d
	setb	%al
	cmpl	$0, 2080(%rsp)
	movl	956(%rsp,%rcx,4), %r11d
	sete	%cl
	orb	%al, %cl
	cmpl	$8, %edx
	jb	.LBB0_51
# BB#52:                                # %.lr.ph.i96.i.i
                                        #   in Loop: Header=BB0_23 Depth=3
	movl	%ebp, %r9d
	movq	1288(%rsp), %rsi
	movq	1296(%rsp), %rdi
	movl	12(%rsp), %r13d         # 4-byte Reload
	.p2align	4, 0x90
.LBB0_53:                               #   Parent Loop BB0_6 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        #       Parent Loop BB0_23 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	%rdi, %rsi
	jae	.LBB0_54
# BB#55:                                #   in Loop: Header=BB0_53 Depth=4
	movl	172(%rsp), %eax
	shll	$8, %eax
	movzbl	(%rsi), %ebp
	orl	%eax, %ebp
	movl	%ebp, 172(%rsp)
	incq	%rsi
	movq	%rsi, 1288(%rsp)
	addl	$-8, %edx
	movl	%edx, 168(%rsp)
	cmpl	$7, %edx
	ja	.LBB0_53
# BB#56:                                #   in Loop: Header=BB0_23 Depth=3
	xorl	%esi, %esi
	jmp	.LBB0_57
.LBB0_51:                               #   in Loop: Header=BB0_23 Depth=3
	xorl	%esi, %esi
	movl	12(%rsp), %r13d         # 4-byte Reload
	testb	%cl, %cl
	jne	.LBB0_59
	jmp	.LBB0_61
.LBB0_54:                               #   in Loop: Header=BB0_23 Depth=3
	movb	$1, %sil
.LBB0_57:                               # %readstream.exit98.i.i
                                        #   in Loop: Header=BB0_23 Depth=3
	movl	%r9d, %ebp
	testb	%cl, %cl
	je	.LBB0_61
.LBB0_59:                               #   in Loop: Header=BB0_23 Depth=3
	testb	%sil, %sil
	jne	.LBB0_27
# BB#60:                                #   in Loop: Header=BB0_23 Depth=3
	movl	172(%rsp), %esi
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	andl	$16777215, %esi         # imm = 0xFFFFFF
	movl	$24, %ecx
	subl	%r8d, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	addl	%r11d, %esi
	addl	%r8d, %edx
	movl	%edx, 168(%rsp)
	cmpl	$2, %esi
	jbe	.LBB0_65
	jmp	.LBB0_67
.LBB0_61:                               #   in Loop: Header=BB0_23 Depth=3
	testb	%sil, %sil
	jne	.LBB0_27
# BB#62:                                #   in Loop: Header=BB0_23 Depth=3
	movl	%r11d, 84(%rsp)         # 4-byte Spill
	movq	%r10, 32(%rsp)          # 8-byte Spill
	movl	172(%rsp), %r13d
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r13d
	andl	$16777215, %r13d        # imm = 0xFFFFFF
	movl	$27, %ecx
	subl	%r8d, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r13d
	leal	-3(%r8,%rdx), %eax
	movl	%eax, 168(%rsp)
	movl	$2, %esi
	movq	%r12, %rdi
	leaq	28(%rsp), %rdx
	callq	getdec
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$0, 28(%rsp)
	jne	.LBB0_27
# BB#63:                                #   in Loop: Header=BB0_23 Depth=3
	addl	84(%rsp), %eax          # 4-byte Folded Reload
	leal	(%rax,%r13,8), %esi
	movl	12(%rsp), %r13d         # 4-byte Reload
	movq	32(%rsp), %r10          # 8-byte Reload
	cmpl	$2, %esi
	ja	.LBB0_67
.LBB0_65:                               #   in Loop: Header=BB0_23 Depth=3
	movl	%esi, %ecx
	movl	96(%rsp,%rcx,4), %r8d
	testl	%esi, %esi
	je	.LBB0_69
# BB#66:                                #   in Loop: Header=BB0_23 Depth=3
	movl	44(%rsp), %eax          # 4-byte Reload
	movl	%eax, 96(%rsp,%rcx,4)
	jmp	.LBB0_68
.LBB0_67:                               #   in Loop: Header=BB0_23 Depth=3
	movl	100(%rsp), %eax
	movl	%eax, 104(%rsp)
	movl	44(%rsp), %eax          # 4-byte Reload
	movl	%eax, 100(%rsp)
	addl	$-3, %esi
	movl	%esi, %r8d
.LBB0_68:                               # %.sink.split.i.i
                                        #   in Loop: Header=BB0_23 Depth=3
	movl	%r8d, 96(%rsp)
	movl	%r8d, %eax
	movl	%eax, 44(%rsp)          # 4-byte Spill
.LBB0_69:                               #   in Loop: Header=BB0_23 Depth=3
	cmpl	60(%rsp), %r10d         # 4-byte Folded Reload
	ja	.LBB0_27
# BB#70:                                #   in Loop: Header=BB0_23 Depth=3
	cmpl	%ebp, %r8d
	jae	.LBB0_27
# BB#71:                                # %.preheader.i.i
                                        #   in Loop: Header=BB0_23 Depth=3
	testl	%r10d, %r10d
	je	.LBB0_23
# BB#72:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB0_23 Depth=3
	notl	%r8d
	leal	-1(%r10), %esi
	movl	%r10d, %edi
	andl	$3, %edi
	movl	%ebp, %r11d
	movl	%ebp, %ecx
	movq	%r10, 32(%rsp)          # 8-byte Spill
                                        # kill: %R10D<def> %R10D<kill> %R10<kill>
	movq	64(%rsp), %rdx          # 8-byte Reload
	je	.LBB0_75
# BB#73:                                # %.prol.preheader
                                        #   in Loop: Header=BB0_23 Depth=3
	negl	%edi
	movl	%r11d, %ecx
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r10d
.LBB0_74:                               #   Parent Loop BB0_6 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        #       Parent Loop BB0_23 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	decl	%r10d
	leal	(%r8,%rcx), %ebp
	movzbl	(%rdx,%rbp), %eax
	movl	%ecx, %ebp
	movb	%al, (%rdx,%rbp)
	incl	%ecx
	incl	%edi
	jne	.LBB0_74
.LBB0_75:                               # %.prol.loopexit
                                        #   in Loop: Header=BB0_23 Depth=3
	cmpl	$3, %esi
	jb	.LBB0_76
# BB#77:                                # %.lr.ph.i.i.new
                                        #   in Loop: Header=BB0_23 Depth=3
	leal	1(%rcx,%r8), %r9d
	addl	%ecx, %r8d
	xorl	%edi, %edi
	movq	64(%rsp), %rdx          # 8-byte Reload
.LBB0_78:                               #   Parent Loop BB0_6 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        #       Parent Loop BB0_23 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leal	(%rcx,%rdi), %esi
	leal	(%r8,%rdi), %ebp
	movzbl	(%rdx,%rbp), %eax
	movb	%al, (%rdx,%rsi)
	leal	1(%rcx,%rdi), %ebp
	leal	(%r9,%rdi), %esi
	movzbl	(%rdx,%rsi), %eax
	movb	%al, (%rdx,%rbp)
	leal	2(%rcx,%rdi), %ebp
	leal	1(%r9,%rdi), %esi
	movzbl	(%rdx,%rsi), %eax
	movb	%al, (%rdx,%rbp)
	leal	3(%rcx,%rdi), %ebp
	leal	2(%r9,%rdi), %esi
	movzbl	(%rdx,%rsi), %eax
	movb	%al, (%rdx,%rbp)
	addl	$4, %edi
	cmpl	%edi, %r10d
	jne	.LBB0_78
.LBB0_76:                               #   in Loop: Header=BB0_23 Depth=3
	movl	%r11d, %ebp
	movq	32(%rsp), %rax          # 8-byte Reload
	jmp	.LBB0_30
	.p2align	4, 0x90
.LBB0_23:                               # %.outer.split.us.i.i
                                        #   Parent Loop BB0_6 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_47 Depth 4
                                        #         Child Loop BB0_53 Depth 4
                                        #         Child Loop BB0_74 Depth 4
                                        #         Child Loop BB0_78 Depth 4
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	28(%rsp), %rdx
	callq	getdec
	movl	%eax, %r13d
	cmpl	$0, 28(%rsp)
	jne	.LBB0_27
# BB#24:                                #   in Loop: Header=BB0_23 Depth=3
	cmpl	$256, %r13d             # imm = 0x100
	jb	.LBB0_29
# BB#25:                                #   in Loop: Header=BB0_23 Depth=3
	cmpl	$720, %r13d             # imm = 0x2D0
	jb	.LBB0_39
# BB#26:                                #   in Loop: Header=BB0_23 Depth=3
	movq	%r12, %rdi
	callq	build_decrypt_dictionaries
	testl	%eax, %eax
	jne	.LBB0_23
	jmp	.LBB0_27
.LBB0_29:                               # %.us-lcssa.us.i.i
                                        #   in Loop: Header=BB0_23 Depth=3
	movl	%ebp, %eax
	movq	64(%rsp), %rcx          # 8-byte Reload
	movb	%r13b, (%rcx,%rax)
	movl	$1, %eax
	movl	12(%rsp), %r13d         # 4-byte Reload
.LBB0_30:                               # %.outer.outer.backedge.i.i
                                        #   in Loop: Header=BB0_23 Depth=3
	addl	%eax, %ebp
	movl	%r15d, %eax
	subl	%ebp, %eax
	movl	%eax, 60(%rsp)          # 4-byte Spill
	ja	.LBB0_23
# BB#31:                                # %decomp_block.exit.split
                                        #   in Loop: Header=BB0_7 Depth=2
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	136(%rsp), %rdi         # 8-byte Reload
	testl	%edi, %edi
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, %rbp
	movb	11(%rsp), %al           # 1-byte Reload
	jne	.LBB0_7
# BB#32:                                # %decomp_block.exit.split
                                        #   in Loop: Header=BB0_7 Depth=2
	cmpl	$8, %r15d
	movq	%rcx, %rbp
	jb	.LBB0_7
# BB#33:                                # %.preheader
                                        #   in Loop: Header=BB0_6 Depth=1
	xorl	%edi, %edi
	addl	$-6, %r15d
	movq	%rcx, %rbp
	je	.LBB0_6
# BB#34:                                # %.lr.ph285.preheader
                                        #   in Loop: Header=BB0_6 Depth=1
	xorl	%edi, %edi
.LBB0_35:                               # %.lr.ph285
                                        #   Parent Loop BB0_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	leal	(%rdi,%rax), %eax
	movzbl	(%rbx,%rax), %ecx
	andb	$-2, %cl
	cmpb	$-24, %cl
	jne	.LBB0_38
# BB#36:                                #   in Loop: Header=BB0_35 Depth=2
	incl	%eax
	movzbl	(%rbx,%rax), %ecx
	movq	152(%rsp), %rdx         # 8-byte Reload
	cmpb	(%rbx,%rdx), %cl
	jne	.LBB0_38
# BB#37:                                #   in Loop: Header=BB0_35 Depth=2
	movl	(%rbx,%rax), %ecx
	shrl	$8, %ecx
	subl	%edi, %ecx
	movl	%ecx, (%rbx,%rax)
	addl	$4, %edi
.LBB0_38:                               #   in Loop: Header=BB0_35 Depth=2
	incl	%edi
	cmpl	%r15d, %edi
	jb	.LBB0_35
	jmp	.LBB0_6
.LBB0_27:                               # %.critedge.critedge174
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
.LBB0_28:                               # %.critedge
	movq	1192(%rsp), %rdi
	callq	free
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	je	.LBB0_12
.LBB0_21:
	xorl	%ebx, %ebx
	movl	$.L.str.2, %edi
	jmp	.LBB0_2
.LBB0_1:
	xorl	%ebx, %ebx
	movl	$.L.str, %edi
.LBB0_2:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB0_95
.LBB0_11:                               # %.critedge.thread166
	movq	1192(%rsp), %rdi
	callq	free
.LBB0_12:
	movl	92(%rsp), %esi          # 4-byte Reload
	movzwl	%si, %eax
	cmpl	$3, %eax
	jb	.LBB0_13
# BB#79:
	leal	-2(%rax), %ecx
	movslq	%ecx, %rdx
	leaq	(%rdx,%rdx,8), %rdx
	movq	120(%rsp), %r12         # 8-byte Reload
	cmpl	%r14d, (%r12,%rdx,4)
	jne	.LBB0_81
# BB#80:
	leal	-1(%rax), %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx,8), %rdx
	cmpl	$0, 12(%r12,%rdx,4)
	cmovew	%cx, %ax
	movw	%ax, %si
	jmp	.LBB0_81
.LBB0_13:
	movq	120(%rsp), %r12         # 8-byte Reload
.LBB0_81:
	movzwl	%si, %r14d
	leaq	(,%r14,4), %rax
	leaq	(%rax,%rax,8), %rbp
	movq	%rbp, %rdi
	movl	%esi, %r13d
	callq	cli_malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_82
# BB#83:
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	testw	%r13w, %r13w
	je	.LBB0_90
# BB#84:                                # %.lr.ph.preheader
	testb	$1, %r14b
	jne	.LBB0_86
# BB#85:
	xorl	%ecx, %ecx
	cmpl	$1, %r14d
	jne	.LBB0_88
	jmp	.LBB0_90
.LBB0_82:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	2144(%rsp), %edi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%rbx, %rsi
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	cli_writen
	jmp	.LBB0_94
.LBB0_86:                               # %.lr.ph.prol
	movl	(%r15), %eax
	movl	%eax, 8(%r15)
	movl	4(%r15), %eax
	movl	%eax, 12(%r15)
	movl	$1, %ecx
	cmpl	$1, %r14d
	je	.LBB0_90
.LBB0_88:                               # %.lr.ph.preheader.new
	movq	%r14, %rax
	subq	%rcx, %rax
	leaq	(%rcx,%rcx,8), %rcx
	leaq	48(%r15,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB0_89:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-48(%rcx), %edx
	movl	%edx, -40(%rcx)
	movl	-44(%rcx), %edx
	movl	%edx, -36(%rcx)
	movl	-12(%rcx), %edx
	movl	%edx, -4(%rcx)
	movl	-8(%rcx), %edx
	movl	%edx, (%rcx)
	addq	$72, %rcx
	addq	$-2, %rax
	jne	.LBB0_89
.LBB0_90:                               # %._crit_edge
	movq	160(%rsp), %rax         # 8-byte Reload
	movl	923(%rax), %r8d
	movl	$0, %r9d
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movl	%r14d, %edx
	movl	88(%rsp), %ecx          # 4-byte Reload
	movl	2144(%rsp), %r14d
	pushq	%r14
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	cli_rebuildpe
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	movl	12(%rsp), %ebp          # 4-byte Reload
	je	.LBB0_91
# BB#92:
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB0_93
.LBB0_91:
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%r14d, %edi
	movq	%rbx, %rsi
	movl	%ebp, %edx
	callq	cli_writen
.LBB0_93:
	movq	%r15, %rdi
	callq	free
.LBB0_94:
	movl	$1, %ebx
.LBB0_95:
	movl	%ebx, %eax
	addq	$2088, %rsp             # imm = 0x828
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_19:                               # %decomp_block.exit.thread
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
.LBB0_20:                               # %.critedge.thread
	movq	1192(%rsp), %rdi
	callq	free
	jmp	.LBB0_21
.Lfunc_end0:
	.size	unaspack212, .Lfunc_end0-unaspack212
	.cfi_endproc

	.p2align	4, 0x90
	.type	build_decrypt_dictionaries,@function
build_decrypt_dictionaries:             # @build_decrypt_dictionaries
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 80
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	(%rbx), %eax
	cmpl	$7, %eax
	jbe	.LBB1_1
# BB#2:                                 # %.lr.ph.i.i
	movq	1120(%rbx), %rcx
	movq	1128(%rbx), %rsi
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	cmpq	%rsi, %rcx
	jae	.LBB1_65
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=1
	movl	4(%rbx), %edi
	shll	$8, %edi
	movzbl	(%rcx), %edx
	orl	%edi, %edx
	movl	%edx, 4(%rbx)
	incq	%rcx
	movq	%rcx, 1120(%rbx)
	addl	$-8, %eax
	movl	%eax, (%rbx)
	cmpl	$7, %eax
	ja	.LBB1_3
	jmp	.LBB1_5
.LBB1_1:                                # %..loopexit_crit_edge.i
	movl	4(%rbx), %edx
.LBB1_5:                                # %getbits.exit
	movl	$0, 12(%rsp)
	movl	$8, %ecx
	subl	%eax, %ecx
	incl	%eax
	movl	%eax, (%rbx)
	movl	$8388608, %eax          # imm = 0x800000
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	testl	%edx, %eax
	jne	.LBB1_7
# BB#6:                                 # %.thread123
	movq	1136(%rbx), %rdi
	xorl	%esi, %esi
	movl	$757, %edx              # imm = 0x2F5
	callq	memset
.LBB1_7:                                # %.preheader96
	xorl	%eax, %eax
.LBB1_9:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_12 Depth 2
	movl	(%rbx), %edx
	cmpl	$7, %edx
	jbe	.LBB1_10
# BB#11:                                # %.lr.ph.i.i64
                                        #   in Loop: Header=BB1_9 Depth=1
	movq	1120(%rbx), %rcx
	movq	1128(%rbx), %rdi
	.p2align	4, 0x90
.LBB1_12:                               #   Parent Loop BB1_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rdi, %rcx
	jae	.LBB1_56
# BB#13:                                #   in Loop: Header=BB1_12 Depth=2
	movl	4(%rbx), %ebp
	shll	$8, %ebp
	movzbl	(%rcx), %esi
	orl	%ebp, %esi
	movl	%esi, 4(%rbx)
	incq	%rcx
	movq	%rcx, 1120(%rbx)
	addl	$-8, %edx
	movl	%edx, (%rbx)
	cmpl	$7, %edx
	ja	.LBB1_12
	jmp	.LBB1_8
	.p2align	4, 0x90
.LBB1_10:                               # %..loopexit.i67_crit_edge
                                        #   in Loop: Header=BB1_9 Depth=1
	movl	4(%rbx), %esi
.LBB1_8:                                # %.loopexit126
                                        #   in Loop: Header=BB1_9 Depth=1
	movl	$0, 12(%rsp)
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	shrl	$20, %esi
	andb	$15, %sil
	addl	$4, %edx
	movl	%edx, (%rbx)
	movb	%sil, 8(%rbx,%rax)
	incq	%rax
	cmpq	$18, %rax
	jbe	.LBB1_9
# BB#14:
	leaq	8(%rbx), %rsi
	movl	$3, %edx
	movq	%rbx, %rdi
	callq	build_decrypt_array
	testb	%al, %al
	je	.LBB1_57
# BB#15:                                # %.preheader94
	leaq	28(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	leaq	12(%rsp), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_16:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_25 Depth 2
                                        #     Child Loop BB1_29 Depth 2
                                        #     Child Loop BB1_35 Depth 2
	movl	$3, %esi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	getdec
	cmpl	$0, 12(%rsp)
	jne	.LBB1_58
# BB#17:                                #   in Loop: Header=BB1_16 Depth=1
	cmpl	$16, %eax
	jb	.LBB1_47
# BB#18:                                #   in Loop: Header=BB1_16 Depth=1
	je	.LBB1_32
# BB#19:                                #   in Loop: Header=BB1_16 Depth=1
	cmpl	$17, %eax
	jne	.LBB1_20
# BB#21:                                #   in Loop: Header=BB1_16 Depth=1
	movl	$3, %eax
	movl	$3, %r13d
	jmp	.LBB1_22
.LBB1_47:                               #   in Loop: Header=BB1_16 Depth=1
	movq	1136(%rbx), %rcx
	movl	%ebp, %edx
	movzbl	(%rcx,%rdx), %ecx
	addl	%eax, %ecx
	andb	$15, %cl
	incl	%ebp
	movb	%cl, 27(%rbx,%rbp)
	cmpl	$757, %ebp              # imm = 0x2F5
	jb	.LBB1_16
	jmp	.LBB1_49
.LBB1_32:                               #   in Loop: Header=BB1_16 Depth=1
	movl	(%rbx), %edx
	cmpl	$7, %edx
	jbe	.LBB1_33
# BB#34:                                # %.lr.ph.i.i84
                                        #   in Loop: Header=BB1_16 Depth=1
	movq	1120(%rbx), %rcx
	movq	1128(%rbx), %rsi
	.p2align	4, 0x90
.LBB1_35:                               #   Parent Loop BB1_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rsi, %rcx
	jae	.LBB1_66
# BB#36:                                #   in Loop: Header=BB1_35 Depth=2
	movl	4(%rbx), %edi
	shll	$8, %edi
	movzbl	(%rcx), %eax
	orl	%edi, %eax
	movl	%eax, 4(%rbx)
	incq	%rcx
	movq	%rcx, 1120(%rbx)
	addl	$-8, %edx
	movl	%edx, (%rbx)
	cmpl	$7, %edx
	ja	.LBB1_35
	jmp	.LBB1_37
.LBB1_20:                               #   in Loop: Header=BB1_16 Depth=1
	movl	$11, %r13d
	movl	$7, %eax
.LBB1_22:                               #   in Loop: Header=BB1_16 Depth=1
	movl	(%rbx), %edx
	cmpl	$7, %edx
	jbe	.LBB1_23
# BB#24:                                # %.lr.ph.i.i74
                                        #   in Loop: Header=BB1_16 Depth=1
	movq	1120(%rbx), %rcx
	movq	1128(%rbx), %rsi
	.p2align	4, 0x90
.LBB1_25:                               #   Parent Loop BB1_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rsi, %rcx
	jae	.LBB1_66
# BB#26:                                #   in Loop: Header=BB1_25 Depth=2
	movl	4(%rbx), %edi
	shll	$8, %edi
	movzbl	(%rcx), %r14d
	orl	%edi, %r14d
	movl	%r14d, 4(%rbx)
	incq	%rcx
	movq	%rcx, 1120(%rbx)
	addl	$-8, %edx
	movl	%edx, (%rbx)
	cmpl	$7, %edx
	ja	.LBB1_25
	jmp	.LBB1_27
.LBB1_23:                               # %.getbits.exit79_crit_edge
                                        #   in Loop: Header=BB1_16 Depth=1
	movl	4(%rbx), %r14d
.LBB1_27:                               # %.preheader
                                        #   in Loop: Header=BB1_16 Depth=1
	movl	$0, 12(%rsp)
	leal	(%rdx,%rax), %ecx
	movl	%ecx, (%rbx)
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r14d
	andl	$16777215, %r14d        # imm = 0xFFFFFF
	movl	$24, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r14d
	cmpl	$756, %ebp              # imm = 0x2F4
	ja	.LBB1_49
# BB#28:                                # %.lr.ph105.preheader
                                        #   in Loop: Header=BB1_16 Depth=1
	movl	%ebp, %ebp
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbp), %rdi
	leal	-1(%r13,%r14), %edx
	notq	%rdx
	leaq	-757(%rbp), %rax
	cmpq	%rdx, %rax
	cmovaeq	%rax, %rdx
	negq	%rdx
	xorl	%esi, %esi
	callq	memset
	incq	%rbp
	movl	$1, %eax
	subl	%r13d, %eax
	subl	%r14d, %eax
	.p2align	4, 0x90
.LBB1_29:                               # %.lr.ph105
                                        #   Parent Loop BB1_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$756, %rbp              # imm = 0x2F4
	leaq	1(%rbp), %rbp
	ja	.LBB1_31
# BB#30:                                # %.lr.ph105
                                        #   in Loop: Header=BB1_29 Depth=2
	testl	%eax, %eax
	leal	1(%rax), %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	jne	.LBB1_29
.LBB1_31:                               # %.backedge.loopexit
                                        #   in Loop: Header=BB1_16 Depth=1
	decl	%ebp
	cmpl	$757, %ebp              # imm = 0x2F5
	jb	.LBB1_16
	jmp	.LBB1_49
.LBB1_33:                               # %.getbits.exit89_crit_edge
                                        #   in Loop: Header=BB1_16 Depth=1
	movl	4(%rbx), %eax
.LBB1_37:                               # %.preheader92
                                        #   in Loop: Header=BB1_16 Depth=1
	movl	$0, 12(%rsp)
	leal	2(%rdx), %ecx
	movl	%ecx, (%rbx)
	cmpl	$756, %ebp              # imm = 0x2F4
	ja	.LBB1_49
# BB#38:                                # %.lr.ph
                                        #   in Loop: Header=BB1_16 Depth=1
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	movl	%ebp, %ecx
	movb	27(%rbx,%rcx), %dl
	leaq	1(%rcx), %rbp
	movb	%dl, 28(%rbx,%rcx)
	cmpq	$756, %rbp              # imm = 0x2F4
	ja	.LBB1_48
# BB#39:                                # %.lr.ph.1
                                        #   in Loop: Header=BB1_16 Depth=1
	leaq	2(%rcx), %rbp
	movb	%dl, 29(%rbx,%rcx)
	cmpq	$756, %rbp              # imm = 0x2F4
	ja	.LBB1_48
# BB#40:                                # %.lr.ph.2
                                        #   in Loop: Header=BB1_16 Depth=1
	leaq	3(%rcx), %rbp
	movb	%dl, 30(%rbx,%rcx)
	cmpq	$756, %rbp              # imm = 0x2F4
	ja	.LBB1_48
# BB#41:                                # %.lr.ph.2
                                        #   in Loop: Header=BB1_16 Depth=1
	shrl	$22, %eax
	andl	$3, %eax
	je	.LBB1_48
# BB#42:                                # %.lr.ph.3
                                        #   in Loop: Header=BB1_16 Depth=1
	leaq	4(%rcx), %rbp
	movb	%dl, 31(%rbx,%rcx)
	cmpq	$756, %rbp              # imm = 0x2F4
	ja	.LBB1_48
# BB#43:                                # %.lr.ph.3
                                        #   in Loop: Header=BB1_16 Depth=1
	cmpl	$1, %eax
	je	.LBB1_48
# BB#44:                                # %.lr.ph.4
                                        #   in Loop: Header=BB1_16 Depth=1
	leaq	5(%rcx), %rbp
	movb	%dl, 32(%rbx,%rcx)
	cmpq	$756, %rbp              # imm = 0x2F4
	ja	.LBB1_48
# BB#45:                                # %.lr.ph.4
                                        #   in Loop: Header=BB1_16 Depth=1
	cmpl	$2, %eax
	je	.LBB1_48
# BB#46:                                # %.lr.ph.5
                                        #   in Loop: Header=BB1_16 Depth=1
	movb	%dl, 33(%rbx,%rcx)
	addq	$6, %rcx
	movq	%rcx, %rbp
.LBB1_48:                               # %.backedge
                                        #   in Loop: Header=BB1_16 Depth=1
	cmpl	$757, %ebp              # imm = 0x2F5
	jb	.LBB1_16
.LBB1_49:                               # %.backedge.thread
	xorl	%r12d, %r12d
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	build_decrypt_array
	testb	%al, %al
	je	.LBB1_58
# BB#50:
	leaq	749(%rbx), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	build_decrypt_array
	testb	%al, %al
	je	.LBB1_58
# BB#51:
	leaq	777(%rbx), %rsi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	build_decrypt_array
	testb	%al, %al
	je	.LBB1_58
# BB#52:
	movl	$0, 1912(%rbx)
	cmpb	$3, 777(%rbx)
	jne	.LBB1_54
# BB#53:
	cmpb	$3, 778(%rbx)
	jne	.LBB1_54
# BB#59:
	cmpb	$3, 779(%rbx)
	jne	.LBB1_54
# BB#60:
	cmpb	$3, 780(%rbx)
	jne	.LBB1_54
# BB#61:
	cmpb	$3, 781(%rbx)
	jne	.LBB1_54
# BB#62:
	cmpb	$3, 782(%rbx)
	jne	.LBB1_54
# BB#63:
	cmpb	$3, 783(%rbx)
	jne	.LBB1_54
# BB#64:
	cmpb	$3, 784(%rbx)
	je	.LBB1_55
.LBB1_54:
	movl	$1, 1912(%rbx)
.LBB1_55:                               # %.loopexit
	movq	1136(%rbx), %rdi
	movl	$757, %edx              # imm = 0x2F5
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	memcpy
	movl	$1, %r12d
	jmp	.LBB1_58
.LBB1_56:                               # %.loopexit95.loopexit110
	movl	$1, 12(%rsp)
	movb	$0, 8(%rbx,%rax)
.LBB1_57:                               # %.loopexit95
	xorl	%r12d, %r12d
	jmp	.LBB1_58
.LBB1_65:
	movl	$1, 12(%rsp)
	movq	1136(%rbx), %rdi
	xorl	%r12d, %r12d
	xorl	%esi, %esi
	movl	$757, %edx              # imm = 0x2F5
	callq	memset
.LBB1_58:                               # %.loopexit95
	movl	%r12d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_66:                               # %getbits.exit89.thread
	movl	$1, 12(%rsp)
	jmp	.LBB1_58
.Lfunc_end1:
	.size	build_decrypt_dictionaries, .Lfunc_end1-build_decrypt_dictionaries
	.cfi_endproc

	.p2align	4, 0x90
	.type	build_decrypt_array,@function
build_decrypt_array:                    # @build_decrypt_array
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 240
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rsi, %r13
	leaq	(%rdx,%rdx,2), %rsi
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 160(%rsp)
	movaps	%xmm0, 144(%rsp)
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm0, 112(%rsp)
	movq	$0, 176(%rsp)
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movq	$0, 96(%rsp)
	movl	1040(%rdi,%rsi,8), %eax
	testl	%eax, %eax
	je	.LBB2_4
# BB#1:                                 # %.lr.ph125.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph125
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r13,%rcx), %edx
	cmpq	$17, %rdx
	ja	.LBB2_23
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	incl	112(%rsp,%rdx,4)
	incq	%rcx
	cmpl	%eax, %ecx
	jb	.LBB2_2
.LBB2_4:                                # %._crit_edge
	movq	%rsi, %rax
	shlq	$5, %rax
	leaq	1144(%rdi,%rax), %rbx
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	leaq	1040(%rdi,%rsi,8), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$0, (%rbx)
	movl	$0, 384(%rbx)
	movl	$23, %ebp
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_5:                                # =>This Inner Loop Header: Depth=1
	movl	%r12d, %eax
	movl	116(%rsp,%r14,4), %r12d
	movl	%ebp, %ecx
	shll	%cl, %r12d
	addl	%eax, %r12d
	cmpl	$16777216, %r12d        # imm = 0x1000000
	ja	.LBB2_23
# BB#6:                                 #   in Loop: Header=BB2_5 Depth=1
	movl	%r12d, 4(%rbx,%r14,4)
	movl	384(%rbx,%r14,4), %eax
	addl	112(%rsp,%r14,4), %eax
	movl	%eax, 36(%rsp,%r14,4)
	movl	%eax, 388(%rbx,%r14,4)
	cmpl	$16, %ebp
	jb	.LBB2_11
# BB#7:                                 #   in Loop: Header=BB2_5 Depth=1
	movl	%r12d, %r15d
	shrl	$16, %r15d
	movl	%r15d, %ecx
	subl	%edx, %ecx
	je	.LBB2_12
# BB#8:                                 #   in Loop: Header=BB2_5 Depth=1
	xorl	%eax, %eax
	cmpl	$256, %ecx              # imm = 0x100
	ja	.LBB2_24
# BB#9:                                 #   in Loop: Header=BB2_5 Depth=1
	testl	%r15d, %r15d
	je	.LBB2_24
# BB#10:                                #   in Loop: Header=BB2_5 Depth=1
	movl	%edx, %edi
	movq	8(%rsp), %rax           # 8-byte Reload
	addq	-8(%rax), %rdi
	movl	%ecx, %edx
	leal	1(%r14), %esi
	callq	memset
	jmp	.LBB2_12
	.p2align	4, 0x90
.LBB2_11:                               #   in Loop: Header=BB2_5 Depth=1
	movl	%edx, %r15d
.LBB2_12:                               #   in Loop: Header=BB2_5 Depth=1
	incq	%r14
	decl	%ebp
	cmpl	$8, %ebp
	movl	%r15d, %edx
	ja	.LBB2_5
# BB#13:
	cmpl	$16777216, %r12d        # imm = 0x1000000
	jne	.LBB2_23
# BB#14:                                # %.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %esi
	movb	$1, %al
	testl	%esi, %esi
	je	.LBB2_24
# BB#15:                                # %.lr.ph
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	1024(%rcx,%rdx,8), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_16:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, %edi
	movzbl	(%r13,%rdi), %ebp
	testq	%rbp, %rbp
	je	.LBB2_20
# BB#17:                                #   in Loop: Header=BB2_16 Depth=1
	cmpb	$17, %bpl
	ja	.LBB2_23
# BB#18:                                #   in Loop: Header=BB2_16 Depth=1
	movl	32(%rsp,%rbp,4), %ebp
	cmpl	%esi, %ebp
	jae	.LBB2_23
# BB#19:                                #   in Loop: Header=BB2_16 Depth=1
	movq	(%rcx), %rsi
	movl	%edx, (%rsi,%rbp,4)
	movzbl	(%r13,%rdi), %esi
	incl	32(%rsp,%rsi,4)
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	(%rsi), %esi
.LBB2_20:                               #   in Loop: Header=BB2_16 Depth=1
	incl	%edx
	cmpl	%esi, %edx
	jb	.LBB2_16
	jmp	.LBB2_24
.LBB2_23:
	xorl	%eax, %eax
.LBB2_24:                               # %.critedge
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	build_decrypt_array, .Lfunc_end2-build_decrypt_array
	.cfi_endproc

	.p2align	4, 0x90
	.type	getdec,@function
getdec:                                 # @getdec
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	$1, (%rdx)
	movl	(%rdi), %r9d
	cmpl	$7, %r9d
	jbe	.LBB3_1
# BB#2:                                 # %.lr.ph.i
	movq	1120(%rdi), %rax
	movq	1128(%rdi), %r10
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	cmpq	%r10, %rax
	jae	.LBB3_4
# BB#5:                                 #   in Loop: Header=BB3_3 Depth=1
	movl	4(%rdi), %ecx
	shll	$8, %ecx
	movzbl	(%rax), %r8d
	orl	%ecx, %r8d
	movl	%r8d, 4(%rdi)
	incq	%rax
	movq	%rax, 1120(%rdi)
	addl	$-8, %r9d
	movl	%r9d, (%rdi)
	cmpl	$7, %r9d
	ja	.LBB3_3
	jmp	.LBB3_6
.LBB3_1:                                # %..loopexit_crit_edge
	movl	4(%rdi), %r8d
.LBB3_6:                                # %.loopexit
	movl	$8, %ecx
	subl	%r9d, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r8d
	andl	$16776704, %r8d         # imm = 0xFFFE00
	leaq	(%rsi,%rsi,2), %r10
	movq	%r10, %rsi
	shlq	$5, %rsi
	cmpl	1176(%rdi,%rsi), %r8d
	jae	.LBB3_8
# BB#7:
	movl	%r8d, %eax
	shrl	$16, %eax
	movq	1032(%rdi,%r10,8), %rcx
	movb	(%rcx,%rax), %r11b
	movl	%r11d, %ecx
	decb	%cl
	xorl	%eax, %eax
	cmpb	$22, %cl
	jbe	.LBB3_14
	jmp	.LBB3_16
.LBB3_8:
	cmpl	1184(%rdi,%rsi), %r8d
	jae	.LBB3_10
# BB#9:
	cmpl	1180(%rdi,%rsi), %r8d
	setae	%r11b
	addb	$9, %r11b
	jmp	.LBB3_14
.LBB3_4:
	xorl	%eax, %eax
	retq
.LBB3_10:
	movb	$11, %r11b
	cmpl	1188(%rdi,%rsi), %r8d
	jb	.LBB3_14
# BB#11:
	movb	$12, %r11b
	cmpl	1192(%rdi,%rsi), %r8d
	jb	.LBB3_14
# BB#12:
	movb	$13, %r11b
	cmpl	1196(%rdi,%rsi), %r8d
	jb	.LBB3_14
# BB#13:
	cmpl	1200(%rdi,%rsi), %r8d
	setae	%r11b
	orb	$14, %r11b
.LBB3_14:
	movzbl	%r11b, %eax
	addl	%eax, %r9d
	movl	%r9d, (%rdi)
	addq	%rdi, %rsi
	subl	1140(%rsi,%rax,4), %r8d
	movl	$24, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r8d
	addl	1528(%rsi,%rax,4), %r8d
	xorl	%eax, %eax
	cmpl	1040(%rdi,%r10,8), %r8d
	jae	.LBB3_16
# BB#15:
	movq	1024(%rdi,%r10,8), %rax
	movl	%r8d, %ecx
	movl	(%rax,%rcx,4), %eax
	movl	$0, (%rdx)
.LBB3_16:                               # %readstream.exit
	retq
.Lfunc_end3:
	.size	getdec, .Lfunc_end3-getdec
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Aspack: Unable to allocate dictionary\n"
	.size	.L.str, 39

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Aspack: unpacking block rva:%x - sz:%x\n"
	.size	.L.str.1, 40

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Aspack: unpacking failure\n"
	.size	.L.str.2, 27

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Aspack: OOM - rebuild failed\n"
	.size	.L.str.3, 30

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Aspack: rebuild failed\n"
	.size	.L.str.4, 24

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Aspack: successfully rebuilt\n"
	.size	.L.str.5, 30


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
