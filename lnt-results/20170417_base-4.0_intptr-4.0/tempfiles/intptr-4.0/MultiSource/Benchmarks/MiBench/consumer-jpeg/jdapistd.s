	.text
	.file	"jdapistd.bc"
	.globl	jpeg_start_decompress
	.p2align	4, 0x90
	.type	jpeg_start_decompress,@function
jpeg_start_decompress:                  # @jpeg_start_decompress
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	28(%r15), %eax
	cmpl	$202, %eax
	je	.LBB0_4
# BB#1:
	cmpl	$203, %eax
	je	.LBB0_7
# BB#2:
	cmpl	$204, %eax
	jne	.LBB0_18
# BB#3:                                 # %.thread37
	leaq	528(%r15), %r12
	jmp	.LBB0_21
.LBB0_4:
	movq	%r15, %rdi
	callq	jinit_master_decompress
	cmpl	$0, 80(%r15)
	je	.LBB0_6
# BB#5:
	movl	$207, 28(%r15)
	movl	$1, %ebx
	jmp	.LBB0_30
.LBB0_18:
	movq	(%r15), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%r15, %rdi
	callq	*(%rcx)
	jmp	.LBB0_19
.LBB0_6:                                # %.thread
	movl	$203, 28(%r15)
.LBB0_7:
	movq	560(%r15), %rax
	cmpl	$0, 32(%rax)
	je	.LBB0_17
# BB#8:                                 # %.thread33.preheader
	movq	16(%r15), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_10
	jmp	.LBB0_11
	.p2align	4, 0x90
.LBB0_16:                               #   in Loop: Header=BB0_11 Depth=1
	movl	400(%r15), %edx
	addq	%rax, %rdx
	movq	%rdx, 16(%rcx)
.LBB0_9:                                # %.thread33
                                        #   in Loop: Header=BB0_11 Depth=1
	testq	%rcx, %rcx
	je	.LBB0_11
.LBB0_10:
	movq	%r15, %rdi
	callq	*(%rcx)
.LBB0_11:                               # =>This Inner Loop Header: Depth=1
	movq	560(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	testl	%eax, %eax
	je	.LBB0_29
# BB#12:                                #   in Loop: Header=BB0_11 Depth=1
	cmpl	$2, %eax
	je	.LBB0_17
# BB#13:                                #   in Loop: Header=BB0_11 Depth=1
	movq	16(%r15), %rcx
	orl	$2, %eax
	cmpl	$3, %eax
	jne	.LBB0_9
# BB#14:                                #   in Loop: Header=BB0_11 Depth=1
	testq	%rcx, %rcx
	je	.LBB0_9
# BB#15:                                #   in Loop: Header=BB0_11 Depth=1
	movq	8(%rcx), %rdx
	incq	%rdx
	movq	%rdx, 8(%rcx)
	movq	16(%rcx), %rax
	cmpq	%rax, %rdx
	jl	.LBB0_9
	jmp	.LBB0_16
.LBB0_17:                               # %.loopexit
	movl	164(%r15), %eax
	movl	%eax, 172(%r15)
.LBB0_19:
	leaq	528(%r15), %r12
	cmpl	$204, 28(%r15)
	je	.LBB0_21
# BB#20:
	movq	528(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	movl	$0, 160(%r15)
	movl	$204, 28(%r15)
.LBB0_21:                               # %.preheader31.i
	movq	(%r12), %rax
	cmpl	$0, 16(%rax)
	je	.LBB0_28
# BB#22:                                # %.preheader.lr.ph.i
	leaq	160(%r15), %r14
	movl	160(%r15), %ebp
	.p2align	4, 0x90
.LBB0_23:                               # =>This Inner Loop Header: Depth=1
	movl	132(%r15), %eax
	cmpl	%eax, %ebp
	jae	.LBB0_27
# BB#24:                                #   in Loop: Header=BB0_23 Depth=1
	movq	16(%r15), %rcx
	testq	%rcx, %rcx
	je	.LBB0_26
# BB#25:                                #   in Loop: Header=BB0_23 Depth=1
	movl	%ebp, %edx
	movq	%rdx, 8(%rcx)
	movq	%rax, 16(%rcx)
	movq	%r15, %rdi
	callq	*(%rcx)
	movl	160(%r15), %ebp
.LBB0_26:                               #   in Loop: Header=BB0_23 Depth=1
	movq	536(%r15), %rax
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	*8(%rax)
	movl	160(%r15), %eax
	cmpl	%ebp, %eax
	movl	%eax, %ebp
	jne	.LBB0_23
	jmp	.LBB0_30
.LBB0_27:                               #   in Loop: Header=BB0_23 Depth=1
	movq	(%r12), %rax
	movq	%r15, %rdi
	callq	*8(%rax)
	movq	(%r12), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	movl	$0, 160(%r15)
	movq	(%r12), %rax
	xorl	%ebp, %ebp
	cmpl	$0, 16(%rax)
	jne	.LBB0_23
.LBB0_28:                               # %._crit_edge.i
	cmpl	$1, 84(%r15)
	movl	$205, %eax
	sbbl	$-1, %eax
	movl	%eax, 28(%r15)
	movl	$1, %ebx
	jmp	.LBB0_30
.LBB0_29:                               # %output_pass_setup.exit.loopexit42
	xorl	%ebx, %ebx
.LBB0_30:                               # %output_pass_setup.exit
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	jpeg_start_decompress, .Lfunc_end0-jpeg_start_decompress
	.cfi_endproc

	.globl	jpeg_read_scanlines
	.p2align	4, 0x90
	.type	jpeg_read_scanlines,@function
jpeg_read_scanlines:                    # @jpeg_read_scanlines
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -32
.Lcfi15:
	.cfi_offset %r14, -24
.Lcfi16:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movl	28(%rbx), %eax
	cmpl	$205, %eax
	je	.LBB1_2
# BB#1:
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB1_2:
	movl	132(%rbx), %eax
	movl	160(%rbx), %ecx
	cmpl	%eax, %ecx
	jae	.LBB1_3
# BB#4:
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.LBB1_6
# BB#5:
	movq	%rcx, 8(%rdx)
	movq	%rax, 16(%rdx)
	movq	%rbx, %rdi
	callq	*(%rdx)
.LBB1_6:
	movl	$0, 12(%rsp)
	movq	536(%rbx), %rax
	leaq	12(%rsp), %rdx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movl	%r14d, %ecx
	callq	*8(%rax)
	movl	12(%rsp), %eax
	addl	%eax, 160(%rbx)
	jmp	.LBB1_7
.LBB1_3:
	movq	(%rbx), %rax
	movl	$119, 40(%rax)
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB1_7:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	jpeg_read_scanlines, .Lfunc_end1-jpeg_read_scanlines
	.cfi_endproc

	.globl	jpeg_read_raw_data
	.p2align	4, 0x90
	.type	jpeg_read_raw_data,@function
jpeg_read_raw_data:                     # @jpeg_read_raw_data
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 48
.Lcfi22:
	.cfi_offset %rbx, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	28(%rbx), %eax
	cmpl	$206, %eax
	je	.LBB2_2
# BB#1:
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB2_2:
	movl	132(%rbx), %eax
	movl	160(%rbx), %ecx
	cmpl	%eax, %ecx
	jae	.LBB2_9
# BB#3:
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.LBB2_5
# BB#4:
	movq	%rcx, 8(%rdx)
	movq	%rax, 16(%rdx)
	movq	%rbx, %rdi
	callq	*(%rdx)
.LBB2_5:
	movl	396(%rbx), %ebp
	imull	392(%rbx), %ebp
	cmpl	%r15d, %ebp
	jbe	.LBB2_7
# BB#6:
	movq	(%rbx), %rax
	movl	$21, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB2_7:
	movq	544(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*24(%rax)
	testl	%eax, %eax
	je	.LBB2_10
# BB#8:
	addl	%ebp, 160(%rbx)
	jmp	.LBB2_11
.LBB2_9:
	movq	(%rbx), %rax
	movl	$119, 40(%rax)
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	*8(%rax)
.LBB2_10:
	xorl	%ebp, %ebp
.LBB2_11:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	jpeg_read_raw_data, .Lfunc_end2-jpeg_read_raw_data
	.cfi_endproc

	.globl	jpeg_start_output
	.p2align	4, 0x90
	.type	jpeg_start_output,@function
jpeg_start_output:                      # @jpeg_start_output
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 48
.Lcfi31:
	.cfi_offset %rbx, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r15
	movl	28(%r15), %eax
	cmpl	$204, %eax
	je	.LBB3_3
# BB#1:
	cmpl	$207, %eax
	je	.LBB3_3
# BB#2:
	movq	(%r15), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%r15, %rdi
	callq	*(%rcx)
.LBB3_3:
	testl	%ebp, %ebp
	movl	$1, %eax
	cmovgl	%ebp, %eax
	movq	560(%r15), %rcx
	cmpl	$0, 36(%rcx)
	je	.LBB3_5
# BB#4:
	movl	164(%r15), %ecx
	cmpl	%ecx, %eax
	cmovgl	%ecx, %eax
.LBB3_5:
	movl	%eax, 172(%r15)
	cmpl	$204, 28(%r15)
	je	.LBB3_7
# BB#6:
	movq	528(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	movl	$0, 160(%r15)
	movl	$204, 28(%r15)
.LBB3_7:                                # %.preheader31.i
	movq	528(%r15), %rax
	cmpl	$0, 16(%rax)
	je	.LBB3_14
# BB#8:                                 # %.preheader.lr.ph.i
	leaq	160(%r15), %r14
	movl	160(%r15), %ebx
	.p2align	4, 0x90
.LBB3_9:                                # =>This Inner Loop Header: Depth=1
	movl	132(%r15), %eax
	cmpl	%eax, %ebx
	jae	.LBB3_13
# BB#10:                                #   in Loop: Header=BB3_9 Depth=1
	movq	16(%r15), %rcx
	testq	%rcx, %rcx
	je	.LBB3_12
# BB#11:                                #   in Loop: Header=BB3_9 Depth=1
	movl	%ebx, %edx
	movq	%rdx, 8(%rcx)
	movq	%rax, 16(%rcx)
	movq	%r15, %rdi
	callq	*(%rcx)
	movl	160(%r15), %ebx
.LBB3_12:                               #   in Loop: Header=BB3_9 Depth=1
	movq	536(%r15), %rax
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	*8(%rax)
	movl	160(%r15), %eax
	cmpl	%ebx, %eax
	movl	%eax, %ebx
	jne	.LBB3_9
	jmp	.LBB3_15
.LBB3_13:                               #   in Loop: Header=BB3_9 Depth=1
	movq	528(%r15), %rax
	movq	%r15, %rdi
	callq	*8(%rax)
	movq	528(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	movl	$0, 160(%r15)
	movq	528(%r15), %rax
	xorl	%ebx, %ebx
	cmpl	$0, 16(%rax)
	jne	.LBB3_9
.LBB3_14:                               # %._crit_edge.i
	cmpl	$1, 84(%r15)
	movl	$205, %eax
	sbbl	$-1, %eax
	movl	%eax, 28(%r15)
	movl	$1, %ebp
.LBB3_15:                               # %output_pass_setup.exit
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	jpeg_start_output, .Lfunc_end3-jpeg_start_output
	.cfi_endproc

	.globl	jpeg_finish_output
	.p2align	4, 0x90
	.type	jpeg_finish_output,@function
jpeg_finish_output:                     # @jpeg_finish_output
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 16
.Lcfi36:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	28(%rbx), %eax
	leal	-205(%rax), %ecx
	cmpl	$1, %ecx
	ja	.LBB4_3
# BB#1:
	cmpl	$0, 80(%rbx)
	je	.LBB4_4
# BB#2:
	movq	528(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	movl	$208, 28(%rbx)
	jmp	.LBB4_5
.LBB4_3:                                # %thread-pre-split
	cmpl	$208, %eax
	je	.LBB4_5
.LBB4_4:                                # %thread-pre-split.thread
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
	.p2align	4, 0x90
.LBB4_5:                                # =>This Inner Loop Header: Depth=1
	movl	164(%rbx), %eax
	cmpl	172(%rbx), %eax
	jg	.LBB4_9
# BB#6:                                 #   in Loop: Header=BB4_5 Depth=1
	movq	560(%rbx), %rax
	cmpl	$0, 36(%rax)
	jne	.LBB4_9
# BB#7:                                 #   in Loop: Header=BB4_5 Depth=1
	movq	%rbx, %rdi
	callq	*(%rax)
	testl	%eax, %eax
	jne	.LBB4_5
# BB#8:
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB4_9:                                # %.critedge
	movl	$207, 28(%rbx)
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end4:
	.size	jpeg_finish_output, .Lfunc_end4-jpeg_finish_output
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
