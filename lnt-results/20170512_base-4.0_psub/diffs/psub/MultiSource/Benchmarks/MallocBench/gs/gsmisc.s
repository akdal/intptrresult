	.text
	.file	"gsmisc.bc"
	.globl	gs_malloc
	.p2align	4, 0x90
	.type	gs_malloc,@function
gs_malloc:                              # @gs_malloc
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	imull	%esi, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_1
.LBB0_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_1:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$42, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	fprintf
	jmp	.LBB0_2
.Lfunc_end0:
	.size	gs_malloc, .Lfunc_end0-gs_malloc
	.cfi_endproc

	.globl	gs_free
	.p2align	4, 0x90
	.type	gs_free,@function
gs_free:                                # @gs_free
	.cfi_startproc
# BB#0:
	jmp	free                    # TAILCALL
.Lfunc_end1:
	.size	gs_free, .Lfunc_end1-gs_free
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s(%d): "
	.size	.L.str, 9

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Benchmarks/MallocBench/gs/gsmisc.c"
	.size	.L.str.1, 85

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%s: malloc failed\n"
	.size	.L.str.2, 19


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
