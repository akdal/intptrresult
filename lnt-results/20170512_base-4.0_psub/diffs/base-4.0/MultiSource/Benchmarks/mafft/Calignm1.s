	.text
	.file	"Calignm1.bc"
	.globl	tracking
	.p2align	4, 0x90
	.type	tracking,@function
tracking:                               # @tracking
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 176
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r8d, %r14d
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	%rdx, %rbp
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	(%r13), %rdi
	callq	strlen
	movq	%rax, %r15
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	callq	strlen
	movq	%r15, %r9
	movq	%rax, %r15
	cmpl	$-1, %r14d
	jl	.LBB0_6
# BB#1:                                 # %.lr.ph138
	movl	%r15d, %eax
	addl	%r9d, %eax
	movslq	%eax, %rcx
	leal	2(%r14), %edx
	leaq	-1(%rdx), %r8
	movq	%rdx, %rdi
	xorl	%esi, %esi
	andq	$3, %rdi
	je	.LBB0_3
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rsi,8), %rbp
	leaq	(%rbp,%rcx), %rax
	movq	%rax, (%rbx,%rsi,8)
	movb	$0, (%rbp,%rcx)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB0_2
.LBB0_3:                                # %.prol.loopexit187
	cmpq	$3, %r8
	jb	.LBB0_6
# BB#4:                                 # %.lr.ph138.new
	subq	%rsi, %rdx
	leaq	24(%rbx,%rsi,8), %rax
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	movq	-24(%rax), %rsi
	leaq	(%rsi,%rcx), %rdi
	movq	%rdi, -24(%rax)
	movb	$0, (%rsi,%rcx)
	movq	-16(%rax), %rsi
	leaq	(%rsi,%rcx), %rdi
	movq	%rdi, -16(%rax)
	movb	$0, (%rsi,%rcx)
	movq	-8(%rax), %rsi
	leaq	(%rsi,%rcx), %rdi
	movq	%rdi, -8(%rax)
	movb	$0, (%rsi,%rcx)
	movq	(%rax), %rsi
	leaq	(%rsi,%rcx), %rdi
	movq	%rdi, (%rax)
	movb	$0, (%rsi,%rcx)
	addq	$32, %rax
	addq	$-4, %rdx
	jne	.LBB0_5
.LBB0_6:                                # %.preheader112
	movl	%r15d, %eax
	movq	%r9, 24(%rsp)           # 8-byte Spill
	addl	%r9d, %eax
	movl	%eax, 84(%rsp)          # 4-byte Spill
	movq	32(%rsp), %r10          # 8-byte Reload
	js	.LBB0_65
# BB#7:                                 # %.lr.ph133
	leal	1(%r14), %eax
	movslq	%eax, %r11
	movl	%r11d, %esi
	leaq	-1(%rsi), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rsi, 64(%rsp)          # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	andl	$3, %esi
	leaq	24(%rbx), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	24(%r13), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	-1(%r10), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%r14, 104(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_8:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_53 Depth 2
                                        #       Child Loop BB0_54 Depth 3
                                        #       Child Loop BB0_57 Depth 3
                                        #     Child Loop BB0_20 Depth 2
                                        #     Child Loop BB0_24 Depth 2
                                        #     Child Loop BB0_30 Depth 2
                                        #       Child Loop BB0_31 Depth 3
                                        #       Child Loop BB0_34 Depth 3
                                        #     Child Loop BB0_46 Depth 2
                                        #     Child Loop BB0_50 Depth 2
                                        #     Child Loop BB0_60 Depth 2
                                        #     Child Loop BB0_63 Depth 2
	movl	%r15d, %edx
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, %edi
	movslq	%edi, %r15
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r15,8), %rax
	movl	%edx, 20(%rsp)          # 4-byte Spill
	movslq	%edx, %r8
	movl	(%rax,%r8,4), %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testl	%eax, %eax
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	js	.LBB0_9
# BB#10:                                #   in Loop: Header=BB0_8 Depth=1
	je	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_8 Depth=1
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	%edi, %edx
	subl	8(%rsp), %edx           # 4-byte Folded Reload
	jmp	.LBB0_13
	.p2align	4, 0x90
.LBB0_9:                                #   in Loop: Header=BB0_8 Depth=1
	leal	-1(%rdi), %edx
	jmp	.LBB0_14
	.p2align	4, 0x90
.LBB0_12:                               #   in Loop: Header=BB0_8 Depth=1
	movq	48(%rsp), %rdi          # 8-byte Reload
	leal	-1(%rdi), %edx
.LBB0_13:                               #   in Loop: Header=BB0_8 Depth=1
	movl	$-1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB0_14:                               #   in Loop: Header=BB0_8 Depth=1
	movl	%edi, %ecx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	subl	%edx, %ecx
	cmpl	$2, %ecx
	jl	.LBB0_15
# BB#16:                                # %.preheader109.lr.ph
                                        #   in Loop: Header=BB0_8 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	-1(%rdi,%rax), %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	testl	%r14d, %r14d
	js	.LBB0_17
# BB#52:                                # %.preheader109.preheader
                                        #   in Loop: Header=BB0_8 Depth=1
	movl	%ecx, %r10d
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB0_53:                               # %.preheader109
                                        #   Parent Loop BB0_8 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_54 Depth 3
                                        #       Child Loop BB0_57 Depth 3
	movq	%r15, %rbp
	subq	%r14, %rbp
	xorl	%ecx, %ecx
	testq	%rsi, %rsi
	je	.LBB0_55
	.p2align	4, 0x90
.LBB0_54:                               #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_53 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r13,%rcx,8), %rax
	movzbl	(%rax,%rbp), %eax
	movq	(%rbx,%rcx,8), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, (%rbx,%rcx,8)
	movb	%al, -1(%rdx)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB0_54
.LBB0_55:                               # %.prol.loopexit
                                        #   in Loop: Header=BB0_53 Depth=2
	cmpq	$3, 72(%rsp)            # 8-byte Folded Reload
	jb	.LBB0_58
# BB#56:                                # %.preheader109.new
                                        #   in Loop: Header=BB0_53 Depth=2
	movq	64(%rsp), %r9           # 8-byte Reload
	subq	%rcx, %r9
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	88(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,8), %r12
	.p2align	4, 0x90
.LBB0_57:                               #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_53 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%r12), %rcx
	movzbl	(%rcx,%rbp), %ecx
	movq	-24(%rax), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, -24(%rax)
	movb	%cl, -1(%rdx)
	movq	-16(%r12), %rcx
	movzbl	(%rcx,%rbp), %ecx
	movq	-16(%rax), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, -16(%rax)
	movb	%cl, -1(%rdx)
	movq	-8(%r12), %rcx
	movzbl	(%rcx,%rbp), %ecx
	movq	-8(%rax), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, -8(%rax)
	movb	%cl, -1(%rdx)
	movq	(%r12), %rcx
	movzbl	(%rcx,%rbp), %ecx
	movq	(%rax), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, (%rax)
	movb	%cl, -1(%rdx)
	addq	$32, %rax
	addq	$32, %r12
	addq	$-4, %r9
	jne	.LBB0_57
.LBB0_58:                               # %._crit_edge
                                        #   in Loop: Header=BB0_53 Depth=2
	movq	(%rbx,%r11,8), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rbx,%r11,8)
	movb	$45, -1(%rax)
	incq	%r14
	cmpq	%r10, %r14
	jne	.LBB0_53
# BB#26:                                # %.preheader111.loopexit141
                                        #   in Loop: Header=BB0_8 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	subl	24(%rsp), %ecx          # 4-byte Folded Reload
	movq	104(%rsp), %r14         # 8-byte Reload
	movq	32(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %r15           # 8-byte Reload
	jmp	.LBB0_27
	.p2align	4, 0x90
.LBB0_15:                               #   in Loop: Header=BB0_8 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB0_27
.LBB0_17:                               # %.preheader109.us.preheader
                                        #   in Loop: Header=BB0_8 Depth=1
	leal	3(%rdi), %r10d
	movq	24(%rsp), %rax          # 8-byte Reload
	subl	%eax, %r10d
	leal	-2(%rdi), %r9d
	subl	%eax, %r9d
	andl	$3, %r10d
	je	.LBB0_18
# BB#19:                                # %.preheader109.us.prol.preheader
                                        #   in Loop: Header=BB0_8 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_20:                               # %.preheader109.us.prol
                                        #   Parent Loop BB0_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%r11,8), %rax
	leaq	-1(%rax), %rbp
	movq	%rbp, (%rbx,%r11,8)
	movb	$45, -1(%rax)
	incl	%edi
	cmpl	%edi, %r10d
	jne	.LBB0_20
# BB#21:                                # %.preheader109.us.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB0_8 Depth=1
	incl	%edi
	jmp	.LBB0_22
.LBB0_18:                               #   in Loop: Header=BB0_8 Depth=1
	movl	$1, %edi
.LBB0_22:                               # %.preheader109.us.prol.loopexit
                                        #   in Loop: Header=BB0_8 Depth=1
	movq	32(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %r15           # 8-byte Reload
	cmpl	$3, %r9d
	jb	.LBB0_25
# BB#23:                                # %.preheader109.us.preheader.new
                                        #   in Loop: Header=BB0_8 Depth=1
	subl	%edi, %ecx
	.p2align	4, 0x90
.LBB0_24:                               # %.preheader109.us
                                        #   Parent Loop BB0_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%r11,8), %rax
	leaq	-1(%rax), %rdi
	movq	%rdi, (%rbx,%r11,8)
	movb	$45, -1(%rax)
	movq	(%rbx,%r11,8), %rax
	leaq	-1(%rax), %rdi
	movq	%rdi, (%rbx,%r11,8)
	movb	$45, -1(%rax)
	movq	(%rbx,%r11,8), %rax
	leaq	-1(%rax), %rdi
	movq	%rdi, (%rbx,%r11,8)
	movb	$45, -1(%rax)
	movq	(%rbx,%r11,8), %rax
	leaq	-1(%rax), %rdi
	movq	%rdi, (%rbx,%r11,8)
	movb	$45, -1(%rax)
	addl	$-4, %ecx
	jne	.LBB0_24
.LBB0_25:                               # %.preheader111.loopexit
                                        #   in Loop: Header=BB0_8 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	subl	24(%rsp), %ecx          # 4-byte Folded Reload
	.p2align	4, 0x90
.LBB0_27:                               # %.preheader111
                                        #   in Loop: Header=BB0_8 Depth=1
	movl	%r15d, %eax
	negl	%eax
	cmpl	$2, %eax
	jl	.LBB0_38
# BB#28:                                # %.preheader.lr.ph
                                        #   in Loop: Header=BB0_8 Depth=1
	decl	%ecx
	testl	%r14d, %r14d
	movl	%eax, %r15d
	js	.LBB0_43
# BB#29:                                # %.preheader.preheader
                                        #   in Loop: Header=BB0_8 Depth=1
	movq	%rcx, %r9
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB0_30:                               # %.preheader
                                        #   Parent Loop BB0_8 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_31 Depth 3
                                        #       Child Loop BB0_34 Depth 3
	xorl	%edi, %edi
	testq	%rsi, %rsi
	je	.LBB0_32
	.p2align	4, 0x90
.LBB0_31:                               #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbx,%rdi,8), %rax
	leaq	-1(%rax), %rdx
	movq	%rdx, (%rbx,%rdi,8)
	movb	$45, -1(%rax)
	incq	%rdi
	cmpq	%rdi, %rsi
	jne	.LBB0_31
.LBB0_32:                               # %.prol.loopexit174
                                        #   in Loop: Header=BB0_30 Depth=2
	cmpq	$3, 72(%rsp)            # 8-byte Folded Reload
	jb	.LBB0_35
# BB#33:                                # %.preheader.new
                                        #   in Loop: Header=BB0_30 Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	subq	%rdi, %rax
	movq	56(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB0_34:                               #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rdi), %rdx
	leaq	-1(%rdx), %rbp
	movq	%rbp, -24(%rdi)
	movb	$45, -1(%rdx)
	movq	-16(%rdi), %rdx
	leaq	-1(%rdx), %rbp
	movq	%rbp, -16(%rdi)
	movb	$45, -1(%rdx)
	movq	-8(%rdi), %rdx
	leaq	-1(%rdx), %rbp
	movq	%rbp, -8(%rdi)
	movb	$45, -1(%rdx)
	movq	(%rdi), %rdx
	leaq	-1(%rdx), %rbp
	movq	%rbp, (%rdi)
	movb	$45, -1(%rdx)
	addq	$32, %rdi
	addq	$-4, %rax
	jne	.LBB0_34
.LBB0_35:                               # %._crit_edge118
                                        #   in Loop: Header=BB0_30 Depth=2
	movq	(%rbx,%r11,8), %rax
	leaq	-1(%rax), %rdx
	movq	%rdx, (%rbx,%r11,8)
	movq	%r8, %rdx
	subq	%rcx, %rdx
	movb	(%r10,%rdx), %dl
	movb	%dl, -1(%rax)
	incq	%rcx
	cmpq	%r15, %rcx
	jne	.LBB0_30
# BB#36:                                # %._crit_edge121.loopexit140
                                        #   in Loop: Header=BB0_8 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	%r9, %rcx
	jmp	.LBB0_37
.LBB0_43:                               # %.preheader.us.preheader
                                        #   in Loop: Header=BB0_8 Depth=1
	leal	3(%r15), %r12d
	leaq	-2(%r15), %r9
	movq	%rcx, %rdx
	andq	$3, %r12
	je	.LBB0_44
# BB#45:                                # %.preheader.us.prol.preheader
                                        #   in Loop: Header=BB0_8 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r8), %r10
	negq	%r12
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_46:                               # %.preheader.us.prol
                                        #   Parent Loop BB0_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%r11,8), %rax
	leaq	-1(%rax), %rdi
	movq	%rdi, (%rbx,%r11,8)
	movzbl	(%r10,%rbp), %ecx
	movb	%cl, -1(%rax)
	decq	%rbp
	cmpq	%rbp, %r12
	jne	.LBB0_46
# BB#47:                                # %.preheader.us.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB0_8 Depth=1
	movl	$1, %ecx
	subq	%rbp, %rcx
	movq	32(%rsp), %r10          # 8-byte Reload
	cmpq	$3, %r9
	jae	.LBB0_49
	jmp	.LBB0_51
.LBB0_44:                               #   in Loop: Header=BB0_8 Depth=1
	movl	$1, %ecx
	cmpq	$3, %r9
	jb	.LBB0_51
.LBB0_49:                               # %.preheader.us.preheader.new
                                        #   in Loop: Header=BB0_8 Depth=1
	subq	%rcx, %r15
	subq	%rcx, %r8
	addq	%r10, %r8
	.p2align	4, 0x90
.LBB0_50:                               # %.preheader.us
                                        #   Parent Loop BB0_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%r11,8), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rbx,%r11,8)
	movzbl	(%r8), %ecx
	movb	%cl, -1(%rax)
	movq	(%rbx,%r11,8), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rbx,%r11,8)
	movzbl	-1(%r8), %ecx
	movb	%cl, -1(%rax)
	movq	(%rbx,%r11,8), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rbx,%r11,8)
	movzbl	-2(%r8), %ecx
	movb	%cl, -1(%rax)
	movq	(%rbx,%r11,8), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rbx,%r11,8)
	movzbl	-3(%r8), %ecx
	movb	%cl, -1(%rax)
	addq	$-4, %r8
	addq	$-4, %r15
	jne	.LBB0_50
.LBB0_51:                               # %._crit_edge121.loopexit
                                        #   in Loop: Header=BB0_8 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	%rdx, %rcx
.LBB0_37:                               # %._crit_edge121
                                        #   in Loop: Header=BB0_8 Depth=1
	subl	%r15d, %ecx
.LBB0_38:                               # %._crit_edge121
                                        #   in Loop: Header=BB0_8 Depth=1
	movl	20(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	jle	.LBB0_65
# BB#39:                                # %._crit_edge121
                                        #   in Loop: Header=BB0_8 Depth=1
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_65
# BB#40:                                # %.preheader110
                                        #   in Loop: Header=BB0_8 Depth=1
	movq	%rcx, %r12
	addl	%eax, %r15d
	testl	%r14d, %r14d
	js	.LBB0_64
# BB#41:                                # %.lr.ph124
                                        #   in Loop: Header=BB0_8 Depth=1
	testq	%rsi, %rsi
	movslq	24(%rsp), %r8           # 4-byte Folded Reload
	je	.LBB0_42
# BB#59:                                # %.prol.preheader181
                                        #   in Loop: Header=BB0_8 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_60:                               #   Parent Loop BB0_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13,%rbp,8), %rax
	movzbl	(%rax,%r8), %eax
	movq	(%rbx,%rbp,8), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, (%rbx,%rbp,8)
	movb	%al, -1(%rdx)
	incq	%rbp
	cmpq	%rbp, %rsi
	jne	.LBB0_60
	jmp	.LBB0_61
	.p2align	4, 0x90
.LBB0_42:                               #   in Loop: Header=BB0_8 Depth=1
	xorl	%ebp, %ebp
.LBB0_61:                               # %.prol.loopexit182
                                        #   in Loop: Header=BB0_8 Depth=1
	cmpq	$3, 72(%rsp)            # 8-byte Folded Reload
	jb	.LBB0_64
# BB#62:                                # %.lr.ph124.new
                                        #   in Loop: Header=BB0_8 Depth=1
	movq	64(%rsp), %rdi          # 8-byte Reload
	subq	%rbp, %rdi
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbp,8), %rax
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rbp,8), %rbp
	.p2align	4, 0x90
.LBB0_63:                               #   Parent Loop BB0_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rbp), %rdx
	movzbl	(%rdx,%r8), %r9d
	movq	-24(%rax), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -24(%rax)
	movb	%r9b, -1(%rcx)
	movq	-16(%rbp), %rcx
	movzbl	(%rcx,%r8), %r9d
	movq	-16(%rax), %rdx
	leaq	-1(%rdx), %rcx
	movq	%rcx, -16(%rax)
	movb	%r9b, -1(%rdx)
	movq	-8(%rbp), %rcx
	movzbl	(%rcx,%r8), %r9d
	movq	-8(%rax), %rdx
	leaq	-1(%rdx), %rcx
	movq	%rcx, -8(%rax)
	movb	%r9b, -1(%rdx)
	movq	(%rbp), %rcx
	movzbl	(%rcx,%r8), %r9d
	movq	(%rax), %rdx
	leaq	-1(%rdx), %rcx
	movq	%rcx, (%rax)
	movb	%r9b, -1(%rdx)
	addq	$32, %rax
	addq	$32, %rbp
	addq	$-4, %rdi
	jne	.LBB0_63
.LBB0_64:                               # %._crit_edge125
                                        #   in Loop: Header=BB0_8 Depth=1
	movslq	%r15d, %rax
	movb	(%r10,%rax), %al
	movq	(%rbx,%r11,8), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%rbx,%r11,8)
	movb	%al, -1(%rcx)
	cmpl	84(%rsp), %r12d         # 4-byte Folded Reload
	leal	1(%r12), %eax
	movl	%eax, %ecx
	jl	.LBB0_8
.LBB0_65:                               # %._crit_edge134
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	tracking, .Lfunc_end0-tracking
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4607632778762754458     # double 1.1000000000000001
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_1:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI1_2:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI1_3:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.text
	.globl	Calignm1
	.p2align	4, 0x90
	.type	Calignm1,@function
Calignm1:                               # @Calignm1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi19:
	.cfi_def_cfa_offset 368
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%r8d, %r14d
	movq	%rdx, %r12
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	testl	%r14d, %r14d
	js	.LBB1_4
# BB#1:                                 # %.lr.ph483.preheader
	leal	1(%r14), %eax
	leaq	-1(%rax), %rdi
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB1_5
# BB#2:                                 # %.lr.ph483.prol.preheader
	xorps	%xmm0, %xmm0
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph483.prol
                                        # =>This Inner Loop Header: Depth=1
	cvtss2sd	%xmm0, %xmm0
	addsd	(%rcx,%rdx,8), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	incq	%rdx
	cmpq	%rdx, %rsi
	jne	.LBB1_3
	jmp	.LBB1_6
.LBB1_4:
	xorpd	%xmm0, %xmm0
	jmp	.LBB1_9
.LBB1_5:
	xorl	%edx, %edx
	xorpd	%xmm0, %xmm0
.LBB1_6:                                # %.lr.ph483.prol.loopexit
	cmpq	$3, %rdi
	jb	.LBB1_9
# BB#7:                                 # %.lr.ph483.preheader.new
	subq	%rdx, %rax
	leaq	24(%rcx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB1_8:                                # %.lr.ph483
                                        # =>This Inner Loop Header: Depth=1
	cvtss2sd	%xmm0, %xmm0
	addsd	-24(%rdx), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	addsd	-16(%rdx), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	addsd	-8(%rdx), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	addsd	(%rdx), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	addq	$32, %rdx
	addq	$-4, %rax
	jne	.LBB1_8
.LBB1_9:                                # %._crit_edge484
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movss	%xmm0, 44(%rsp)         # 4-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	callq	strlen
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	Calignm1.orlgth(%rip), %r15d
	movl	Calignm1.orlgth1(%rip), %r13d
	testl	%r13d, %r13d
	jle	.LBB1_29
# BB#10:                                # %._crit_edge484
	testl	%r15d, %r15d
	jle	.LBB1_29
# BB#11:                                # %._crit_edge484
	movl	njob(%rip), %eax
	testl	%eax, %eax
	js	.LBB1_29
# BB#12:                                # %.lr.ph479
	cltq
	movq	Calignm1.mseq(%rip), %rcx
	movq	Calignm1.nseq(%rip), %rdx
	leaq	1(%rax), %r8
	cmpq	$4, %r8
	jae	.LBB1_14
# BB#13:
	xorl	%esi, %esi
	jmp	.LBB1_27
.LBB1_14:                               # %min.iters.checked
	movq	%r8, %rsi
	andq	$-4, %rsi
	je	.LBB1_18
# BB#15:                                # %vector.memcheck
	leaq	8(%rcx,%rax,8), %rdi
	cmpq	%rdi, %rdx
	jae	.LBB1_19
# BB#16:                                # %vector.memcheck
	leaq	8(%rdx,%rax,8), %rdi
	cmpq	%rdi, %rcx
	jae	.LBB1_19
.LBB1_18:
	xorl	%esi, %esi
.LBB1_27:                               # %scalar.ph.preheader
	decq	%rsi
	.p2align	4, 0x90
.LBB1_28:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rcx,%rsi,8), %rdi
	movq	%rdi, 8(%rdx,%rsi,8)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB1_28
.LBB1_29:                               # %.loopexit402
	cmpl	%r15d, 8(%rsp)          # 4-byte Folded Reload
	movq	%r14, 56(%rsp)          # 8-byte Spill
	movq	%r12, 48(%rsp)          # 8-byte Spill
	jg	.LBB1_31
# BB#30:                                # %.loopexit402
	cmpl	%r13d, 24(%rsp)         # 4-byte Folded Reload
	jle	.LBB1_54
.LBB1_31:
	testl	%r15d, %r15d
	jle	.LBB1_34
# BB#32:
	testl	%r13d, %r13d
	movq	8(%rsp), %rbx           # 8-byte Reload
	jle	.LBB1_35
# BB#33:
	movq	Calignm1.v(%rip), %rdi
	callq	FreeFloatMtx
	movq	Calignm1.g(%rip), %rdi
	callq	FreeFloatCub
	movq	Calignm1.gl(%rip), %rdi
	callq	FreeFloatTri
	movq	Calignm1.gs(%rip), %rdi
	callq	FreeFloatTri
	movq	Calignm1.m(%rip), %rdi
	callq	FreeFloatVec
	movq	Calignm1.mp(%rip), %rdi
	callq	FreeIntVec
	movq	Calignm1.mseq(%rip), %rdi
	callq	FreeCharMtx
	movq	Calignm1.gvsa(%rip), %rdi
	callq	FreeFloatVec
	movq	Calignm1.scmx(%rip), %rdi
	callq	FreeFloatMtx
	movl	Calignm1.orlgth(%rip), %r15d
	movl	Calignm1.orlgth1(%rip), %r13d
	jmp	.LBB1_35
.LBB1_34:
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB1_35:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	movsd	.LCPI1_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r15d, %eax
	cmovgel	%eax, %r15d
	movq	24(%rsp), %rax          # 8-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r13d, %eax
	cmovgel	%eax, %r13d
	cmpl	%r13d, %r15d
	cmovll	%r13d, %r15d
	leal	2(%r15), %ebx
	leal	2(%r13), %esi
	movl	%ebx, %edi
	callq	AllocateFloatMtx
	movq	%rax, Calignm1.v(%rip)
	movl	$3, %esi
	movl	$3, %edx
	movl	%ebx, %edi
	callq	AllocateFloatCub
	movq	%rax, Calignm1.g(%rip)
	leal	3(%r15), %r12d
	movl	%r12d, %edi
	callq	AllocateFloatTri
	movq	%rax, Calignm1.gl(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatTri
	movq	%rax, Calignm1.gs(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, Calignm1.m(%rip)
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, Calignm1.mp(%rip)
	movl	njob(%rip), %edi
	incl	%edi
	movl	$1, %esi
	callq	AllocateCharMtx
	movq	%rax, Calignm1.mseq(%rip)
	movl	njob(%rip), %edi
	incl	%edi
	leal	(%r15,%r13), %esi
	callq	AllocateCharMtx
	movq	%rax, Calignm1.nseq(%rip)
	movslq	njob(%rip), %rcx
	testq	%rcx, %rcx
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	js	.LBB1_53
# BB#36:                                # %.lr.ph476
	movq	Calignm1.mseq(%rip), %rdx
	leaq	1(%rcx), %r8
	cmpq	$4, %r8
	jae	.LBB1_38
# BB#37:
	xorl	%esi, %esi
	jmp	.LBB1_51
.LBB1_38:                               # %min.iters.checked660
	movq	%r8, %rsi
	andq	$-4, %rsi
	je	.LBB1_42
# BB#39:                                # %vector.memcheck673
	leaq	8(%rax,%rcx,8), %rdi
	cmpq	%rdi, %rdx
	jae	.LBB1_43
# BB#40:                                # %vector.memcheck673
	leaq	8(%rdx,%rcx,8), %rdi
	cmpq	%rdi, %rax
	jae	.LBB1_43
.LBB1_42:
	xorl	%esi, %esi
.LBB1_51:                               # %scalar.ph658.preheader
	decq	%rsi
	.p2align	4, 0x90
.LBB1_52:                               # %scalar.ph658
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax,%rsi,8), %rdi
	movq	%rdi, 8(%rdx,%rsi,8)
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB1_52
.LBB1_53:                               # %._crit_edge477
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, Calignm1.gvsa(%rip)
	movl	$26, %edi
	movl	%ebx, %esi
	callq	AllocateFloatMtx
	movq	%rax, Calignm1.scmx(%rip)
	movl	%r15d, Calignm1.orlgth(%rip)
	movl	%r13d, Calignm1.orlgth1(%rip)
.LBB1_54:
	movl	commonAlloc1(%rip), %eax
	cmpl	%eax, %r15d
	movl	commonAlloc2(%rip), %ecx
	jg	.LBB1_57
# BB#55:
	cmpl	%ecx, %r13d
	jg	.LBB1_57
# BB#56:                                # %._crit_edge619
	movq	commonIP(%rip), %rax
	jmp	.LBB1_61
.LBB1_57:                               # %._crit_edge613
	testl	%eax, %eax
	je	.LBB1_60
# BB#58:                                # %._crit_edge613
	testl	%ecx, %ecx
	je	.LBB1_60
# BB#59:
	movq	commonIP(%rip), %rdi
	callq	FreeIntMtx
	movl	commonAlloc1(%rip), %eax
	movl	Calignm1.orlgth(%rip), %r15d
	movl	commonAlloc2(%rip), %ecx
	movl	Calignm1.orlgth1(%rip), %r13d
.LBB1_60:
	cmpl	%r15d, %eax
	cmovgel	%eax, %r15d
	cmpl	%r13d, %ecx
	cmovgel	%ecx, %r13d
	leal	10(%r15), %edi
	leal	10(%r13), %esi
	callq	AllocateIntMtx
	movq	%rax, commonIP(%rip)
	movl	%r15d, commonAlloc1(%rip)
	movl	%r13d, commonAlloc2(%rip)
.LBB1_61:
	movq	%rax, Calignm1.ijp(%rip)
	movq	Calignm1.scmx(%rip), %rcx
	movl	%r14d, %edi
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	scmx_calc
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movq	24(%rsp), %r13          # 8-byte Reload
	jle	.LBB1_79
# BB#62:                                # %.lr.ph473
	movq	Calignm1.scmx(%rip), %rax
	testl	%r13d, %r13d
	movq	Calignm1.gvsa(%rip), %r9
	jle	.LBB1_73
# BB#63:                                # %.lr.ph473.split.us.preheader
	movq	Calignm1.v(%rip), %r15
	movl	%r13d, %r11d
	movl	8(%rsp), %r8d           # 4-byte Reload
	movl	%r13d, %r10d
	andl	$1, %r10d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_64:                               # %.lr.ph473.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_65 Depth 2
                                        #       Child Loop BB1_66 Depth 3
                                        #     Child Loop BB1_71 Depth 2
	movl	$n_dis, %esi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_65:                               #   Parent Loop BB1_64 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_66 Depth 3
	movl	$0, 208(%rsp,%rcx,4)
	xorpd	%xmm0, %xmm0
	movl	$1, %ebp
	movq	%rsi, %rdi
	.p2align	4, 0x90
.LBB1_66:                               #   Parent Loop BB1_64 Depth=1
                                        #     Parent Loop BB1_65 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorps	%xmm1, %xmm1
	cvtsi2ssl	(%rdi), %xmm1
	movq	-8(%rax,%rbp,8), %rdx
	mulss	(%rdx,%rbx,4), %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	104(%rdi), %xmm0
	movq	(%rax,%rbp,8), %rdx
	mulss	(%rdx,%rbx,4), %xmm0
	addss	%xmm1, %xmm0
	addq	$208, %rdi
	addq	$2, %rbp
	cmpq	$27, %rbp
	jne	.LBB1_66
# BB#67:                                #   in Loop: Header=BB1_65 Depth=2
	movss	%xmm0, 208(%rsp,%rcx,4)
	incq	%rcx
	addq	$4, %rsi
	cmpq	$26, %rcx
	jne	.LBB1_65
# BB#68:                                # %.lr.ph469.us
                                        #   in Loop: Header=BB1_64 Depth=1
	testq	%r10, %r10
	jne	.LBB1_70
# BB#69:                                #   in Loop: Header=BB1_64 Depth=1
	xorl	%ecx, %ecx
	cmpq	$1, %r11
	jne	.LBB1_71
	jmp	.LBB1_72
	.p2align	4, 0x90
.LBB1_70:                               #   in Loop: Header=BB1_64 Depth=1
	movsbq	(%r12), %rcx
	movslq	amino_n(,%rcx,4), %rcx
	movl	208(%rsp,%rcx,4), %ecx
	movq	(%r15,%rbx,8), %rdx
	movl	%ecx, (%rdx)
	movl	$1, %ecx
	cmpq	$1, %r11
	je	.LBB1_72
	.p2align	4, 0x90
.LBB1_71:                               #   Parent Loop BB1_64 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	(%r12,%rcx), %rsi
	movslq	amino_n(,%rsi,4), %rsi
	movl	208(%rsp,%rsi,4), %esi
	movq	(%r15,%rbx,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movsbq	1(%r12,%rcx), %rsi
	movslq	amino_n(,%rsi,4), %rsi
	movl	208(%rsp,%rsi,4), %esi
	movq	(%r15,%rbx,8), %rdi
	movl	%esi, 4(%rdi,%rcx,4)
	addq	$2, %rcx
	cmpq	%rcx, %r11
	jne	.LBB1_71
.LBB1_72:                               # %._crit_edge470.us
                                        #   in Loop: Header=BB1_64 Depth=1
	movl	304(%rsp), %ecx
	movl	%ecx, (%r9,%rbx,4)
	incq	%rbx
	cmpq	%r8, %rbx
	jne	.LBB1_64
	jmp	.LBB1_79
.LBB1_73:                               # %.lr.ph473.split.preheader
	movl	8(%rsp), %r8d           # 4-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_74:                               # %.lr.ph473.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_75 Depth 2
                                        #       Child Loop BB1_76 Depth 3
	movl	$n_dis, %ebx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_75:                               #   Parent Loop BB1_74 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_76 Depth 3
	movl	$0, 208(%rsp,%rdi,4)
	xorpd	%xmm0, %xmm0
	movl	$1, %ebp
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB1_76:                               #   Parent Loop BB1_74 Depth=1
                                        #     Parent Loop BB1_75 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorps	%xmm1, %xmm1
	cvtsi2ssl	(%rcx), %xmm1
	movq	-8(%rax,%rbp,8), %rdx
	mulss	(%rdx,%rsi,4), %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	104(%rcx), %xmm0
	movq	(%rax,%rbp,8), %rdx
	mulss	(%rdx,%rsi,4), %xmm0
	addss	%xmm1, %xmm0
	addq	$208, %rcx
	addq	$2, %rbp
	cmpq	$27, %rbp
	jne	.LBB1_76
# BB#77:                                #   in Loop: Header=BB1_75 Depth=2
	movss	%xmm0, 208(%rsp,%rdi,4)
	incq	%rdi
	addq	$4, %rbx
	cmpq	$26, %rdi
	jne	.LBB1_75
# BB#78:                                # %.preheader400
                                        #   in Loop: Header=BB1_74 Depth=1
	movl	304(%rsp), %ecx
	movl	%ecx, (%r9,%rsi,4)
	incq	%rsi
	cmpq	%r8, %rsi
	jne	.LBB1_74
.LBB1_79:                               # %.preheader399
	movq	8(%rsp), %r15           # 8-byte Reload
	testl	%r15d, %r15d
	js	.LBB1_85
# BB#80:                                # %.lr.ph464
	movq	Calignm1.v(%rip), %rdx
	movslq	%r13d, %rax
	leaq	1(%r15), %rbp
	movl	%ebp, %ecx
	leaq	-1(%rcx), %rsi
	xorl	%edi, %edi
	andq	$7, %rbp
	je	.LBB1_82
	.p2align	4, 0x90
.LBB1_81:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rdi,8), %rbx
	movl	$0, (%rbx,%rax,4)
	incq	%rdi
	cmpq	%rdi, %rbp
	jne	.LBB1_81
.LBB1_82:                               # %.prol.loopexit769
	cmpq	$7, %rsi
	jb	.LBB1_85
# BB#83:                                # %.lr.ph464.new
	subq	%rdi, %rcx
	leaq	56(%rdx,%rdi,8), %rdx
	.p2align	4, 0x90
.LBB1_84:                               # =>This Inner Loop Header: Depth=1
	movq	-56(%rdx), %rsi
	movl	$0, (%rsi,%rax,4)
	movq	-48(%rdx), %rsi
	movl	$0, (%rsi,%rax,4)
	movq	-40(%rdx), %rsi
	movl	$0, (%rsi,%rax,4)
	movq	-32(%rdx), %rsi
	movl	$0, (%rsi,%rax,4)
	movq	-24(%rdx), %rsi
	movl	$0, (%rsi,%rax,4)
	movq	-16(%rdx), %rsi
	movl	$0, (%rsi,%rax,4)
	movq	-8(%rdx), %rsi
	movl	$0, (%rsi,%rax,4)
	movq	(%rdx), %rsi
	movl	$0, (%rsi,%rax,4)
	addq	$64, %rdx
	addq	$-8, %rcx
	jne	.LBB1_84
.LBB1_85:                               # %.preheader398
	testl	%r13d, %r13d
	js	.LBB1_87
# BB#86:                                # %.lr.ph461
	movq	Calignm1.v(%rip), %rax
	movq	%r15, %rcx
	shlq	$32, %rcx
	movslq	%r15d, %rbx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	sarq	$29, %rcx
	movq	(%rax,%rcx), %rdi
	movl	%r13d, %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
	jmp	.LBB1_88
.LBB1_87:                               # %.preheader398.._crit_edge462_crit_edge
	movq	%r15, %rax
	shlq	$32, %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movslq	%r15d, %rbx
.LBB1_88:                               # %._crit_edge462
	movss	44(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	testl	%r15d, %r15d
	movq	Calignm1.gvsa(%rip), %rdi
	movl	$0, (%rdi,%rbx,4)
	js	.LBB1_94
# BB#89:                                # %.preheader397.lr.ph
	movq	Calignm1.g(%rip), %rcx
	leaq	1(%r15), %rdx
	movl	%edx, %eax
	testb	$1, %dl
	jne	.LBB1_91
# BB#90:
	xorl	%edx, %edx
	cmpq	$1, %rax
	jne	.LBB1_92
	jmp	.LBB1_94
.LBB1_91:                               # %.preheader397.prol
	movq	(%rcx), %rdx
	movq	(%rdx), %rsi
	movl	$0, 8(%rsi)
	movq	$0, (%rsi)
	movq	8(%rdx), %rsi
	movl	$0, 8(%rsi)
	movq	$0, (%rsi)
	movq	16(%rdx), %rdx
	movl	$0, 8(%rdx)
	movq	$0, (%rdx)
	movl	$1, %edx
	cmpq	$1, %rax
	je	.LBB1_94
.LBB1_92:                               # %.preheader397.lr.ph.new
	subq	%rdx, %rax
	leaq	8(%rcx,%rdx,8), %rcx
	.p2align	4, 0x90
.LBB1_93:                               # %.preheader397
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rcx), %rdx
	movq	(%rdx), %rsi
	movl	$0, 8(%rsi)
	movq	$0, (%rsi)
	movq	8(%rdx), %rsi
	movl	$0, 8(%rsi)
	movq	$0, (%rsi)
	movq	16(%rdx), %rdx
	movl	$0, 8(%rdx)
	movq	$0, (%rdx)
	movq	(%rcx), %rdx
	movq	(%rdx), %rsi
	movl	$0, 8(%rsi)
	movq	$0, (%rsi)
	movq	8(%rdx), %rsi
	movl	$0, 8(%rsi)
	movq	$0, (%rsi)
	movq	16(%rdx), %rdx
	movl	$0, 8(%rdx)
	movq	$0, (%rdx)
	addq	$16, %rcx
	addq	$-2, %rax
	jne	.LBB1_93
.LBB1_94:                               # %.preheader395
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	testl	%r14d, %r14d
	js	.LBB1_100
# BB#95:                                # %.lr.ph456
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	js	.LBB1_100
# BB#96:                                # %.lr.ph456.split.preheader
	movq	Calignm1.g(%rip), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, %ecx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	leal	1(%rax), %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	56(%rsp), %rax          # 8-byte Reload
	leal	1(%rax), %r11d
	xorps	%xmm0, %xmm0
	cvtsi2ssl	penalty(%rip), %xmm0
	xorl	%r13d, %r13d
	xorpd	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB1_97:                               # %.lr.ph456.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_98 Depth 2
	movq	16(%rsp), %rax          # 8-byte Reload
	movsd	(%rax,%r13,8), %xmm2    # xmm2 = mem[0],zero
	cvtsd2ss	%xmm2, %xmm2
	movaps	%xmm2, %xmm3
	mulss	%xmm1, %xmm3
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r13,8), %r14
	mulss	%xmm0, %xmm3
	movq	64(%rsp), %r10          # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	movq	80(%rsp), %rax          # 8-byte Reload
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_98:                               #   Parent Loop BB1_97 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$45, (%r14)
	setne	%r15b
	sete	%dl
	testq	%r9, %r9
	sete	%r12b
	setne	%cl
	andb	%dl, %cl
	movl	%ebx, %edi
	movzbl	%cl, %esi
	xorl	$1, %edi
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%edi, %xmm4
	negl	%edi
	movl	%esi, %ecx
	andl	%edi, %ecx
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%ecx, %xmm5
	mulss	%xmm2, %xmm5
	movq	(%rax), %rcx
	mulss	%xmm0, %xmm5
	movq	(%rcx), %rdx
	addss	(%rdx), %xmm5
	movss	%xmm5, (%rdx)
	movq	8(%rcx), %r8
	movss	(%r8), %xmm5            # xmm5 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm5
	movss	%xmm5, (%r8)
	movq	16(%rcx), %rcx
	orb	%r15b, %r12b
	movzbl	%r12b, %ebp
	negl	%ebx
	movss	(%rcx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	andl	%ebp, %ebx
	andl	%ebp, %edi
	addl	%ebx, %edi
	addss	%xmm3, %xmm5
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%edi, %xmm6
	mulss	%xmm2, %xmm6
	mulss	%xmm0, %xmm6
	movss	%xmm5, (%rcx)
	addss	4(%rdx), %xmm6
	movss	%xmm6, 4(%rdx)
	movss	4(%r8), %xmm5           # xmm5 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm5
	movss	%xmm5, 4(%r8)
	mulss	%xmm2, %xmm4
	mulss	%xmm0, %xmm4
	addss	8(%rdx), %xmm4
	movss	%xmm4, 8(%rdx)
	movss	8(%rcx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm4
	incq	%r14
	addq	$8, %rax
	decq	%r9
	decq	%r10
	movl	%esi, %ebx
	movss	%xmm4, 8(%rcx)
	jne	.LBB1_98
# BB#99:                                # %._crit_edge454
                                        #   in Loop: Header=BB1_97 Depth=1
	incq	%r13
	cmpq	%r11, %r13
	jne	.LBB1_97
.LBB1_100:                              # %.preheader394
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	2(%rax), %r14d
	cmpl	$-1, %eax
	jl	.LBB1_121
# BB#101:                               # %.preheader393.lr.ph
	movq	Calignm1.gl(%rip), %r9
	movq	Calignm1.gs(%rip), %r11
	movslq	%r14d, %r15
	movq	$-6, %r12
	movl	$2, %r13d
	xorl	%r10d, %r10d
	xorpd	%xmm0, %xmm0
	jmp	.LBB1_110
.LBB1_102:                              # %vector.body685.preheader
                                        #   in Loop: Header=BB1_110 Depth=1
	addq	$-6, %rbx
	movq	%rbx, %rdi
	shrq	$3, %rdi
	incq	%rdi
	testb	$3, %dil
	je	.LBB1_105
# BB#103:                               # %vector.body685.prol.preheader
                                        #   in Loop: Header=BB1_110 Depth=1
	movl	%r12d, %edi
	shrl	$3, %edi
	incl	%edi
	andl	$3, %edi
	negq	%rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_104:                              # %vector.body685.prol
                                        #   Parent Loop BB1_110 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	%xmm0, (%rcx,%rbp,4)
	movupd	%xmm0, 16(%rcx,%rbp,4)
	movupd	%xmm0, (%rdx,%rbp,4)
	movupd	%xmm0, 16(%rdx,%rbp,4)
	addq	$8, %rbp
	incq	%rdi
	jne	.LBB1_104
	jmp	.LBB1_106
.LBB1_105:                              #   in Loop: Header=BB1_110 Depth=1
	xorl	%ebp, %ebp
.LBB1_106:                              # %vector.body685.prol.loopexit
                                        #   in Loop: Header=BB1_110 Depth=1
	cmpq	$24, %rbx
	jb	.LBB1_109
# BB#107:                               # %vector.body685.preheader.new
                                        #   in Loop: Header=BB1_110 Depth=1
	movq	%r13, %rbx
	andq	$-8, %rbx
	subq	%rbp, %rbx
	leaq	112(%rdx,%rbp,4), %rdi
	leaq	112(%rcx,%rbp,4), %rbp
	.p2align	4, 0x90
.LBB1_108:                              # %vector.body685
                                        #   Parent Loop BB1_110 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	%xmm0, -112(%rbp)
	movupd	%xmm0, -96(%rbp)
	movupd	%xmm0, -112(%rdi)
	movupd	%xmm0, -96(%rdi)
	movupd	%xmm0, -80(%rbp)
	movupd	%xmm0, -64(%rbp)
	movupd	%xmm0, -80(%rdi)
	movupd	%xmm0, -64(%rdi)
	movupd	%xmm0, -48(%rbp)
	movupd	%xmm0, -32(%rbp)
	movupd	%xmm0, -48(%rdi)
	movupd	%xmm0, -32(%rdi)
	movupd	%xmm0, -16(%rbp)
	movupd	%xmm0, (%rbp)
	movupd	%xmm0, -16(%rdi)
	movupd	%xmm0, (%rdi)
	subq	$-128, %rdi
	subq	$-128, %rbp
	addq	$-32, %rbx
	jne	.LBB1_108
.LBB1_109:                              # %middle.block686
                                        #   in Loop: Header=BB1_110 Depth=1
	cmpq	%rax, %rsi
	jne	.LBB1_115
	jmp	.LBB1_120
	.p2align	4, 0x90
.LBB1_110:                              # %.lr.ph447
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_104 Depth 2
                                        #     Child Loop BB1_108 Depth 2
                                        #     Child Loop BB1_117 Depth 2
                                        #     Child Loop BB1_119 Depth 2
	movq	%r10, %rbx
	leaq	2(%rbx), %rsi
	leaq	1(%rbx), %r10
	movq	(%r9,%rbx,8), %rcx
	movq	(%r11,%rbx,8), %rdx
	cmpq	$7, %rsi
	jbe	.LBB1_114
# BB#111:                               # %min.iters.checked689
                                        #   in Loop: Header=BB1_110 Depth=1
	movq	%rsi, %rax
	andq	$-8, %rax
	je	.LBB1_114
# BB#112:                               # %vector.memcheck702
                                        #   in Loop: Header=BB1_110 Depth=1
	leaq	(%rdx,%rsi,4), %rdi
	cmpq	%rdi, %rcx
	jae	.LBB1_102
# BB#113:                               # %vector.memcheck702
                                        #   in Loop: Header=BB1_110 Depth=1
	leaq	(%rcx,%rsi,4), %rdi
	cmpq	%rdi, %rdx
	jae	.LBB1_102
	.p2align	4, 0x90
.LBB1_114:                              #   in Loop: Header=BB1_110 Depth=1
	xorl	%eax, %eax
.LBB1_115:                              # %scalar.ph687.preheader
                                        #   in Loop: Header=BB1_110 Depth=1
	movl	%esi, %edi
	subl	%eax, %edi
	movq	%r10, %rbx
	subq	%rax, %rbx
	testb	$7, %dil
	je	.LBB1_118
# BB#116:                               # %scalar.ph687.prol.preheader
                                        #   in Loop: Header=BB1_110 Depth=1
	movl	%r13d, %edi
	subl	%eax, %edi
	andl	$7, %edi
	negq	%rdi
	.p2align	4, 0x90
.LBB1_117:                              # %scalar.ph687.prol
                                        #   Parent Loop BB1_110 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$0, (%rcx,%rax,4)
	movl	$0, (%rdx,%rax,4)
	incq	%rax
	incq	%rdi
	jne	.LBB1_117
.LBB1_118:                              # %scalar.ph687.prol.loopexit
                                        #   in Loop: Header=BB1_110 Depth=1
	cmpq	$7, %rbx
	jb	.LBB1_120
	.p2align	4, 0x90
.LBB1_119:                              # %scalar.ph687
                                        #   Parent Loop BB1_110 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$0, (%rcx,%rax,4)
	movl	$0, (%rdx,%rax,4)
	movl	$0, 4(%rcx,%rax,4)
	movl	$0, 4(%rdx,%rax,4)
	movl	$0, 8(%rcx,%rax,4)
	movl	$0, 8(%rdx,%rax,4)
	movl	$0, 12(%rcx,%rax,4)
	movl	$0, 12(%rdx,%rax,4)
	movl	$0, 16(%rcx,%rax,4)
	movl	$0, 16(%rdx,%rax,4)
	movl	$0, 20(%rcx,%rax,4)
	movl	$0, 20(%rdx,%rax,4)
	movl	$0, 24(%rcx,%rax,4)
	movl	$0, 24(%rdx,%rax,4)
	movl	$0, 28(%rcx,%rax,4)
	movl	$0, 28(%rdx,%rax,4)
	addq	$8, %rax
	cmpq	%rax, %rsi
	jne	.LBB1_119
.LBB1_120:                              # %.loopexit
                                        #   in Loop: Header=BB1_110 Depth=1
	incq	%r13
	incq	%r12
	cmpq	%r15, %r10
	jl	.LBB1_110
.LBB1_121:                              # %.preheader392
	movq	56(%rsp), %r13          # 8-byte Reload
	testl	%r13d, %r13d
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	16(%rsp), %r15          # 8-byte Reload
	js	.LBB1_131
# BB#122:                               # %.lr.ph445
	testl	%r12d, %r12d
	js	.LBB1_146
# BB#123:                               # %.lr.ph445.split.preheader
	movq	Calignm1.gs(%rip), %rax
	movq	Calignm1.gl(%rip), %r10
	leal	1(%r12), %edx
	leal	1(%r13), %r9d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB1_124:                              # %.lr.ph445.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_125 Depth 2
	movsd	(%r15,%r11,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%r11,8), %rbp
	xorl	%esi, %esi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_125:                              #   Parent Loop BB1_124 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$45, (%rbp,%rsi)
	jne	.LBB1_128
# BB#126:                               #   in Loop: Header=BB1_125 Depth=2
	movslq	%ebx, %rdi
	incl	%ebx
	cvtsi2ssl	penalty(%rip), %xmm1
	mulss	%xmm0, %xmm1
	movq	(%rax,%rsi,8), %rcx
	movss	4(%rcx,%rdi,4), %xmm2   # xmm2 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm2
	movss	%xmm2, 4(%rcx,%rdi,4)
	leaq	1(%rsi), %rcx
	cmpb	$45, 1(%rbp,%rsi)
	je	.LBB1_129
# BB#127:                               #   in Loop: Header=BB1_125 Depth=2
	incq	%rdi
	movq	(%r10,%rsi,8), %rsi
	addss	(%rsi,%rdi,4), %xmm1
	movss	%xmm1, (%rsi,%rdi,4)
	jmp	.LBB1_129
	.p2align	4, 0x90
.LBB1_128:                              # %._crit_edge632
                                        #   in Loop: Header=BB1_125 Depth=2
	incq	%rsi
	xorl	%ebx, %ebx
	movq	%rsi, %rcx
.LBB1_129:                              #   in Loop: Header=BB1_125 Depth=2
	cmpq	%rdx, %rcx
	movq	%rcx, %rsi
	jne	.LBB1_125
# BB#130:                               # %._crit_edge443
                                        #   in Loop: Header=BB1_124 Depth=1
	incq	%r11
	cmpq	%r9, %r11
	jne	.LBB1_124
.LBB1_131:                              # %.preheader391
	testl	%r12d, %r12d
	js	.LBB1_146
# BB#132:                               # %.preheader390.lr.ph
	movq	Calignm1.gl(%rip), %r9
	movq	Calignm1.gs(%rip), %r11
	leal	1(%r12), %ecx
	movl	$1, %edx
	xorl	%eax, %eax
	movq	48(%rsp), %r10          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_133:                              # %.preheader390
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_136 Depth 2
                                        #     Child Loop BB1_140 Depth 2
                                        #     Child Loop BB1_143 Depth 2
	testq	%rax, %rax
	jle	.LBB1_144
# BB#134:                               # %.lr.ph433
                                        #   in Loop: Header=BB1_133 Depth=1
	leaq	-1(%rax), %rbx
	movq	(%r11,%rax,8), %rdi
	movss	4(%rdi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	testb	$3, %al
	je	.LBB1_138
# BB#135:                               # %.prol.preheader755
                                        #   in Loop: Header=BB1_133 Depth=1
	movl	%eax, %esi
	andl	$3, %esi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_136:                              #   Parent Loop BB1_133 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addss	8(%rdi,%rbp,4), %xmm0
	movss	%xmm0, 8(%rdi,%rbp,4)
	incq	%rbp
	cmpq	%rbp, %rsi
	jne	.LBB1_136
# BB#137:                               # %.prol.loopexit756.unr-lcssa
                                        #   in Loop: Header=BB1_133 Depth=1
	incq	%rbp
	cmpq	$3, %rbx
	jae	.LBB1_139
	jmp	.LBB1_141
	.p2align	4, 0x90
.LBB1_138:                              #   in Loop: Header=BB1_133 Depth=1
	movl	$1, %ebp
	cmpq	$3, %rbx
	jb	.LBB1_141
.LBB1_139:                              # %.lr.ph433.new
                                        #   in Loop: Header=BB1_133 Depth=1
	leaq	1(%rax), %rbx
	.p2align	4, 0x90
.LBB1_140:                              #   Parent Loop BB1_133 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addss	4(%rdi,%rbp,4), %xmm0
	movss	%xmm0, 4(%rdi,%rbp,4)
	addss	8(%rdi,%rbp,4), %xmm0
	movss	%xmm0, 8(%rdi,%rbp,4)
	addss	12(%rdi,%rbp,4), %xmm0
	movss	%xmm0, 12(%rdi,%rbp,4)
	addss	16(%rdi,%rbp,4), %xmm0
	movss	%xmm0, 16(%rdi,%rbp,4)
	leaq	4(%rbp), %rbp
	cmpq	%rbp, %rbx
	jne	.LBB1_140
.LBB1_141:                              # %.preheader389
                                        #   in Loop: Header=BB1_133 Depth=1
	testq	%rax, %rax
	jle	.LBB1_144
# BB#142:                               # %.lr.ph435
                                        #   in Loop: Header=BB1_133 Depth=1
	movq	(%r9,%rax,8), %rsi
	movq	%rdx, %rdi
	.p2align	4, 0x90
.LBB1_143:                              #   Parent Loop BB1_133 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rsi,%rdi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rsi,%rdi,4), %xmm0
	movss	%xmm0, -4(%rsi,%rdi,4)
	decq	%rdi
	cmpq	$1, %rdi
	jg	.LBB1_143
.LBB1_144:                              # %._crit_edge436
                                        #   in Loop: Header=BB1_133 Depth=1
	incq	%rax
	incq	%rdx
	cmpq	%rcx, %rax
	jne	.LBB1_133
# BB#145:
	movq	%r11, %r9
	jmp	.LBB1_147
.LBB1_146:                              # %.preheader391.._crit_edge438_crit_edge
	movq	Calignm1.gs(%rip), %r9
	movq	48(%rsp), %r10          # 8-byte Reload
.LBB1_147:                              # %._crit_edge438
	movq	Calignm1.v(%rip), %r11
	movq	%r11, Calignm1.w(%rip)
	movq	Calignm1.g(%rip), %r15
	movq	(%r15), %rax
	movq	(%rax), %rax
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	(%r11), %rcx
	addss	(%rcx), %xmm0
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movss	%xmm0, (%rcx)
	movss	4(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movq	8(%r15), %rcx
	movq	8(%rcx), %rcx
	addss	(%rcx), %xmm0
	movq	8(%r9), %rdx
	addss	8(%rdx), %xmm0
	movq	128(%rsp), %rbx         # 8-byte Reload
	addss	(%rbx), %xmm0
	movq	8(%r11), %rdx
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	testl	%r12d, %r12d
	jle	.LBB1_150
# BB#148:                               # %.lr.ph430
	movq	Calignm1.gl(%rip), %rdx
	movslq	%r14d, %rsi
	xorps	%xmm0, %xmm0
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_149:                              # =>This Inner Loop Header: Depth=1
	movss	4(%rcx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movq	(%rdx,%rdi,8), %rcx
	addss	4(%rcx,%rdi,4), %xmm1
	addss	4(%rbx,%rdi,4), %xmm1
	addss	%xmm1, %xmm0
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	addss	(%rbx), %xmm1
	addss	%xmm0, %xmm1
	movq	16(%r15,%rdi,8), %rcx
	movq	8(%rcx), %rcx
	addss	(%rcx), %xmm1
	movq	16(%r9,%rdi,8), %rbp
	addss	12(%rbp,%rdi,4), %xmm1
	movq	16(%r11,%rdi,8), %rbp
	addss	(%rbp), %xmm1
	movss	%xmm1, (%rbp)
	leaq	1(%rdi), %rbp
	addq	$3, %rdi
	cmpq	%rsi, %rdi
	movq	%rbp, %rdi
	jl	.LBB1_149
.LBB1_150:                              # %._crit_edge431
	xorps	%xmm0, %xmm0
	cvtsi2ssl	penalty(%rip), %xmm0
	mulss	%xmm7, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	n_dis+2496(%rip), %xmm1
	mulss	%xmm7, %xmm1
	movaps	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	movq	120(%rsp), %rdi         # 8-byte Reload
	addss	4(%rdi), %xmm2
	movss	%xmm2, 4(%rdi)
	cmpl	$2, %r8d
	movq	%r11, %rbp
	movq	%r15, %r14
	jl	.LBB1_158
# BB#151:                               # %.lr.ph426
	leal	1(%r8), %eax
	leal	3(%r8), %esi
	leaq	-3(%rax), %rcx
	xorps	%xmm2, %xmm2
	andq	$3, %rsi
	je	.LBB1_155
# BB#152:                               # %.prol.preheader750
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_153:                              # =>This Inner Loop Header: Depth=1
	addss	%xmm1, %xmm2
	movaps	%xmm0, %xmm3
	addss	%xmm2, %xmm3
	addss	8(%rdi,%rdx,4), %xmm3
	movss	%xmm3, 8(%rdi,%rdx,4)
	incq	%rdx
	cmpq	%rdx, %rsi
	jne	.LBB1_153
# BB#154:                               # %.prol.loopexit751.unr-lcssa
	addq	$2, %rdx
	cmpq	$3, %rcx
	jae	.LBB1_156
	jmp	.LBB1_158
.LBB1_155:
	movl	$2, %edx
	cmpq	$3, %rcx
	jb	.LBB1_158
.LBB1_156:                              # %.lr.ph426.new
	subq	%rdx, %rax
	leaq	12(%rdi,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB1_157:                              # =>This Inner Loop Header: Depth=1
	addss	%xmm1, %xmm2
	movaps	%xmm0, %xmm3
	addss	%xmm2, %xmm3
	addss	-12(%rcx), %xmm3
	movss	%xmm3, -12(%rcx)
	addss	%xmm1, %xmm2
	movaps	%xmm0, %xmm3
	addss	%xmm2, %xmm3
	addss	-8(%rcx), %xmm3
	movss	%xmm3, -8(%rcx)
	addss	%xmm1, %xmm2
	movaps	%xmm0, %xmm3
	addss	%xmm2, %xmm3
	addss	-4(%rcx), %xmm3
	movss	%xmm3, -4(%rcx)
	addss	%xmm1, %xmm2
	movaps	%xmm0, %xmm3
	addss	%xmm2, %xmm3
	addss	(%rcx), %xmm3
	movss	%xmm3, (%rcx)
	addq	$16, %rcx
	addq	$-4, %rax
	jne	.LBB1_157
.LBB1_158:                              # %.preheader388
	movq	%r9, 152(%rsp)          # 8-byte Spill
	testl	%r8d, %r8d
	js	.LBB1_167
# BB#159:                               # %.lr.ph422
	movq	Calignm1.m(%rip), %r9
	movq	Calignm1.mp(%rip), %rdx
	leaq	1(%r8), %rcx
	movl	%ecx, %eax
	cmpq	$7, %rax
	jbe	.LBB1_164
# BB#160:                               # %min.iters.checked716
	andl	$7, %ecx
	movq	%rax, %rdi
	subq	%rcx, %rdi
	je	.LBB1_164
# BB#161:                               # %vector.body712.preheader
	movq	%rbp, %r11
	leaq	16(%r9), %rbp
	leaq	16(%rdx), %rbx
	xorps	%xmm0, %xmm0
	movq	%rdi, %rsi
	.p2align	4, 0x90
.LBB1_162:                              # %vector.body712
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -16(%rbp)
	movups	%xmm0, (%rbp)
	movups	%xmm0, -16(%rbx)
	movups	%xmm0, (%rbx)
	addq	$32, %rbp
	addq	$32, %rbx
	addq	$-8, %rsi
	jne	.LBB1_162
# BB#163:                               # %middle.block713
	testq	%rcx, %rcx
	movq	%r11, %rbp
	jne	.LBB1_165
	jmp	.LBB1_167
.LBB1_164:
	xorl	%edi, %edi
.LBB1_165:                              # %scalar.ph714.preheader
	leaq	(%r9,%rdi,4), %rcx
	leaq	(%rdx,%rdi,4), %rdx
	subq	%rdi, %rax
	.p2align	4, 0x90
.LBB1_166:                              # %scalar.ph714
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, (%rcx)
	movl	$0, (%rdx)
	addq	$4, %rcx
	addq	$4, %rdx
	decq	%rax
	jne	.LBB1_166
.LBB1_167:                              # %.preheader387
	testl	%r12d, %r12d
	jle	.LBB1_189
# BB#168:                               # %.lr.ph413
	movq	Calignm1.gl(%rip), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	%r12, %rax
	movq	Calignm1.mp(%rip), %rdx
	movq	Calignm1.m(%rip), %r10
	movq	Calignm1.ijp(%rip), %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	leal	1(%r8), %r13d
	leal	1(%rax), %eax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movl	$1, %edi
	movq	%r14, 192(%rsp)         # 8-byte Spill
	movq	%rdx, 184(%rsp)         # 8-byte Spill
	movq	%r10, 176(%rsp)         # 8-byte Spill
	movq	%rbp, %r12
	.p2align	4, 0x90
.LBB1_169:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_171 Depth 2
	testl	%r8d, %r8d
	jle	.LBB1_186
# BB#170:                               # %.lr.ph410
                                        #   in Loop: Header=BB1_169 Depth=1
	movq	-8(%r12,%rdi,8), %r15
	leal	-2(%rdi), %r11d
	movq	(%r14,%rdi,8), %rax
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	16(%rax), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rdi,8), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	(%r12,%rdi,8), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	xorps	%xmm0, %xmm0
	xorl	%r9d, %r9d
	movl	$-1, %esi
	movl	$4, %ebp
	movl	$1, %r8d
	movl	%r11d, 92(%rsp)         # 4-byte Spill
	movq	%rbx, 136(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_171:                              #   Parent Loop BB1_169 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$2, %r8
	jl	.LBB1_174
# BB#172:                               #   in Loop: Header=BB1_171 Depth=2
	movss	-8(%r15,%r8,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	8(%rbx), %xmm1
	xorps	%xmm2, %xmm2
	cvtsi2ssl	n_dis+2496(%rip), %xmm2
	mulss	%xmm7, %xmm2
	addss	%xmm2, %xmm1
	movq	-8(%r14,%rdi,8), %rax
	movq	16(%rax), %rax
	addss	8(%rax), %xmm2
	addss	%xmm2, %xmm0
	ucomiss	%xmm0, %xmm1
	jbe	.LBB1_175
# BB#173:                               # %..sink.split_crit_edge
                                        #   in Loop: Header=BB1_171 Depth=2
	leaq	-2(%r8), %r9
	movaps	%xmm1, %xmm0
	cmpq	$2, %rdi
	jge	.LBB1_176
	jmp	.LBB1_178
	.p2align	4, 0x90
.LBB1_174:                              #   in Loop: Header=BB1_171 Depth=2
	movss	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	8(%rbx), %xmm0
	xorl	%r9d, %r9d
.LBB1_175:                              #   in Loop: Header=BB1_171 Depth=2
	cmpq	$2, %rdi
	jl	.LBB1_178
.LBB1_176:                              #   in Loop: Header=BB1_171 Depth=2
	movq	%r12, %rax
	movq	%rax, %rbx
	movq	-16(%rbx,%rdi,8), %rbx
	leaq	-1(%r8), %rax
	movss	-4(%rbx,%r8,4), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movq	-8(%r14,%rdi,8), %rbx
	movq	(%rbx), %rcx
	movq	8(%rbx), %rbx
	addss	4(%rcx), %xmm2
	movq	128(%rsp), %rcx         # 8-byte Reload
	movss	-4(%rcx,%rdi,4), %xmm3  # xmm3 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm2
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movq	168(%rsp), %rcx         # 8-byte Reload
	movq	-16(%rcx,%rdi,8), %rcx
	movl	%r11d, %ebx
	subl	(%rdx,%r8,4), %ebx
	movslq	%ebx, %rbx
	addss	(%rcx,%rbx,4), %xmm1
	addss	%xmm3, %xmm1
	addss	(%r10,%r8,4), %xmm1
	movss	%xmm1, (%r10,%r8,4)
	ucomiss	%xmm1, %xmm2
	jbe	.LBB1_180
# BB#177:                               #   in Loop: Header=BB1_171 Depth=2
	movl	%r11d, %r14d
	leaq	(%rdx,%r8,4), %r11
	leaq	(%r10,%r8,4), %r10
	movq	136(%rsp), %rbx         # 8-byte Reload
	jmp	.LBB1_179
	.p2align	4, 0x90
.LBB1_178:                              #   in Loop: Header=BB1_171 Depth=2
	leaq	-1(%r8), %rax
	movq	120(%rsp), %rcx         # 8-byte Reload
	movss	-4(%rcx,%r8,4), %xmm2   # xmm2 = mem[0],zero,zero,zero
	addss	4(%rbx), %xmm2
	leaq	(%r10,%rbp), %r10
	leaq	(%rdx,%rbp), %r11
	xorl	%r14d, %r14d
.LBB1_179:                              # %.sink.split6
                                        #   in Loop: Header=BB1_171 Depth=2
	movss	%xmm2, (%r10)
	movl	%r14d, (%r11)
	movaps	%xmm2, %xmm1
	movq	192(%rsp), %r14         # 8-byte Reload
	movq	184(%rsp), %rdx         # 8-byte Reload
	movq	176(%rsp), %r10         # 8-byte Reload
	movl	92(%rsp), %r11d         # 4-byte Reload
	jmp	.LBB1_181
	.p2align	4, 0x90
.LBB1_180:                              #   in Loop: Header=BB1_171 Depth=2
	movq	136(%rsp), %rbx         # 8-byte Reload
.LBB1_181:                              #   in Loop: Header=BB1_171 Depth=2
	movss	(%r15,%rax,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	addss	(%rbx), %xmm2
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rax,%r8,4)
	movq	72(%rsp), %rax          # 8-byte Reload
	movss	(%rax), %xmm3           # xmm3 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm3
	ucomiss	%xmm2, %xmm3
	jbe	.LBB1_183
# BB#182:                               #   in Loop: Header=BB1_171 Depth=2
	leal	(%r9,%rsi), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx,%r8,4)
	movaps	%xmm3, %xmm2
.LBB1_183:                              #   in Loop: Header=BB1_171 Depth=2
	movq	80(%rsp), %rax          # 8-byte Reload
	addss	(%rax), %xmm1
	movslq	(%rdx,%r8,4), %rcx
	movslq	%edi, %rax
	subq	%rcx, %rax
	movq	64(%rsp), %rcx          # 8-byte Reload
	addss	(%rcx,%rax,4), %xmm1
	ucomiss	%xmm2, %xmm1
	jbe	.LBB1_185
# BB#184:                               #   in Loop: Header=BB1_171 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx,%r8,4)
	movaps	%xmm1, %xmm2
.LBB1_185:                              #   in Loop: Header=BB1_171 Depth=2
	movq	200(%rsp), %rax         # 8-byte Reload
	addss	(%rax,%r8,4), %xmm2
	movss	%xmm2, (%rax,%r8,4)
	incq	%r8
	addq	$4, %rbp
	decl	%esi
	cmpq	%r8, %r13
	jne	.LBB1_171
	jmp	.LBB1_187
	.p2align	4, 0x90
.LBB1_186:                              #   in Loop: Header=BB1_169 Depth=1
	xorps	%xmm0, %xmm0
	xorl	%r9d, %r9d
.LBB1_187:                              # %._crit_edge411
                                        #   in Loop: Header=BB1_169 Depth=1
	incq	%rdi
	cmpq	160(%rsp), %rdi         # 8-byte Folded Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	jne	.LBB1_169
# BB#188:                               # %._crit_edge414
	movq	%r12, %rbp
	movss	%xmm0, Calignm1.mi(%rip)
	movl	%r9d, Calignm1.mpi(%rip)
	movq	56(%rsp), %r13          # 8-byte Reload
	movq	48(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %r12           # 8-byte Reload
.LBB1_189:
	cmpl	$0, cnst(%rip)
	je	.LBB1_191
# BB#190:
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	movq	96(%rsp), %rcx          # 8-byte Reload
	addq	%rax, %rcx
	sarq	$29, %rcx
	movq	(%rbp,%rcx), %rcx
	movq	%r8, %rdx
	shlq	$32, %rdx
	addq	%rdx, %rax
	sarq	$30, %rax
	movss	(%rcx,%rax), %xmm0      # xmm0 = mem[0],zero,zero,zero
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	(%r14,%rcx,8), %rax
	movq	(%rax), %rax
	addss	(%rax), %xmm0
	leaq	(%rbp,%rcx,8), %r14
	movq	(%rbp,%rcx,8), %rax
	movslq	%r8d, %r15
	sarq	$30, %rdx
	movss	%xmm0, (%rax,%rdx)
	movq	Calignm1.ijp(%rip), %rax
	movq	(%rax,%rcx,8), %rax
	movl	$0, (%rax,%rdx)
	jmp	.LBB1_192
.LBB1_191:                              # %._crit_edge633
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	(%rbp,%rax,8), %r14
	movslq	%r8d, %r15
.LBB1_192:
	testl	%r12d, %r12d
	movq	(%r14), %rax
	movl	(%rax,%r15,4), %eax
	movq	112(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx)
	js	.LBB1_198
# BB#193:                               # %.lr.ph407
	movq	Calignm1.ijp(%rip), %rcx
	incq	%r12
	movl	%r12d, %eax
	leaq	-1(%rax), %rsi
	xorl	%edx, %edx
	andq	$7, %r12
	je	.LBB1_195
	.p2align	4, 0x90
.LBB1_194:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rdi
	incq	%rdx
	movl	%edx, (%rdi)
	cmpq	%rdx, %r12
	jne	.LBB1_194
.LBB1_195:                              # %.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB1_198
# BB#196:                               # %.lr.ph407.new
	negq	%rax
	leaq	4(%rdx,%rax), %rax
	leaq	56(%rcx,%rdx,8), %rcx
	leaq	4(%rdx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_197:                              # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx,%rsi,8), %rdi
	leal	(%rdx,%rsi), %ebp
	leal	-3(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-48(%rcx,%rsi,8), %rdi
	leal	-2(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-40(%rcx,%rsi,8), %rdi
	leal	-1(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-32(%rcx,%rsi,8), %rdi
	movl	%ebp, (%rdi)
	movq	-24(%rcx,%rsi,8), %rdi
	leal	1(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-16(%rcx,%rsi,8), %rdi
	leal	2(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-8(%rcx,%rsi,8), %rdi
	leal	3(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	(%rcx,%rsi,8), %rdi
	leal	4(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	leaq	8(%rax,%rsi), %rdi
	addq	$8, %rsi
	cmpq	$4, %rdi
	jne	.LBB1_197
.LBB1_198:                              # %.preheader
	testl	%r8d, %r8d
	movq	Calignm1.ijp(%rip), %rcx
	js	.LBB1_207
# BB#199:                               # %.lr.ph
	movq	(%rcx), %rdx
	incq	%r8
	movl	%r8d, %eax
	cmpq	$7, %rax
	jbe	.LBB1_204
# BB#200:                               # %min.iters.checked733
	andl	$7, %r8d
	movq	%rax, %rbp
	subq	%r8, %rbp
	je	.LBB1_204
# BB#201:                               # %vector.body729.preheader
	leaq	16(%rdx), %rdi
	movdqa	.LCPI1_1(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI1_2(%rip), %xmm1   # xmm1 = [4,4,4,4]
	pcmpeqd	%xmm2, %xmm2
	movdqa	.LCPI1_3(%rip), %xmm3   # xmm3 = [8,8,8,8]
	movq	%rbp, %rsi
	.p2align	4, 0x90
.LBB1_202:                              # %vector.body729
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm4
	paddd	%xmm1, %xmm4
	movdqa	%xmm0, %xmm5
	pxor	%xmm2, %xmm5
	pxor	%xmm2, %xmm4
	movdqu	%xmm5, -16(%rdi)
	movdqu	%xmm4, (%rdi)
	paddd	%xmm3, %xmm0
	addq	$32, %rdi
	addq	$-8, %rsi
	jne	.LBB1_202
# BB#203:                               # %middle.block730
	testq	%r8, %r8
	jne	.LBB1_205
	jmp	.LBB1_207
.LBB1_204:
	xorl	%ebp, %ebp
.LBB1_205:                              # %scalar.ph731.preheader
	movl	%ebp, %esi
	notl	%esi
	leaq	(%rdx,%rbp,4), %rdx
	subq	%rbp, %rax
	.p2align	4, 0x90
.LBB1_206:                              # %scalar.ph731
                                        # =>This Inner Loop Header: Depth=1
	movl	%esi, (%rdx)
	decl	%esi
	addq	$4, %rdx
	decq	%rax
	jne	.LBB1_206
.LBB1_207:                              # %._crit_edge
	movq	Calignm1.nseq(%rip), %rbp
	movq	%rbp, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%r10, %rdx
	movl	%r13d, %r8d
	callq	tracking
	movq	(%r14), %rax
	movl	(%rax,%r15,4), %eax
	movq	112(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx)
	movq	%rbp, %rax
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_19:                               # %vector.body.preheader
	leaq	-4(%rsi), %rbp
	movl	%ebp, %edi
	shrl	$2, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB1_22
# BB#20:                                # %vector.body.prol.preheader
	negq	%rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_21:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rcx,%rbx,8), %xmm0
	movupd	16(%rcx,%rbx,8), %xmm1
	movupd	%xmm0, (%rdx,%rbx,8)
	movupd	%xmm1, 16(%rdx,%rbx,8)
	addq	$4, %rbx
	incq	%rdi
	jne	.LBB1_21
	jmp	.LBB1_23
.LBB1_43:                               # %vector.body656.preheader
	movl	%ebx, %r9d
	leaq	-4(%rsi), %rbp
	movl	%ebp, %edi
	shrl	$2, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB1_46
# BB#44:                                # %vector.body656.prol.preheader
	negq	%rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_45:                               # %vector.body656.prol
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rax,%rbx,8), %xmm0
	movupd	16(%rax,%rbx,8), %xmm1
	movupd	%xmm0, (%rdx,%rbx,8)
	movupd	%xmm1, 16(%rdx,%rbx,8)
	addq	$4, %rbx
	incq	%rdi
	jne	.LBB1_45
	jmp	.LBB1_47
.LBB1_22:
	xorl	%ebx, %ebx
.LBB1_23:                               # %vector.body.prol.loopexit
	cmpq	$12, %rbp
	jb	.LBB1_26
# BB#24:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rbx, %rbp
	leaq	112(%rdx,%rbx,8), %rdi
	leaq	112(%rcx,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB1_25:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rdi)
	movups	%xmm1, -96(%rdi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rdi)
	movups	%xmm1, -64(%rdi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rdi)
	movups	%xmm1, -32(%rdi)
	movupd	-16(%rbx), %xmm0
	movupd	(%rbx), %xmm1
	movupd	%xmm0, -16(%rdi)
	movupd	%xmm1, (%rdi)
	subq	$-128, %rdi
	subq	$-128, %rbx
	addq	$-16, %rbp
	jne	.LBB1_25
.LBB1_26:                               # %middle.block
	cmpq	%rsi, %r8
	je	.LBB1_29
	jmp	.LBB1_27
.LBB1_46:
	xorl	%ebx, %ebx
.LBB1_47:                               # %vector.body656.prol.loopexit
	cmpq	$12, %rbp
	jb	.LBB1_50
# BB#48:                                # %vector.body656.preheader.new
	movq	%rsi, %rbp
	subq	%rbx, %rbp
	leaq	112(%rdx,%rbx,8), %rdi
	leaq	112(%rax,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB1_49:                               # %vector.body656
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rdi)
	movups	%xmm1, -96(%rdi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rdi)
	movups	%xmm1, -64(%rdi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rdi)
	movups	%xmm1, -32(%rdi)
	movupd	-16(%rbx), %xmm0
	movupd	(%rbx), %xmm1
	movupd	%xmm0, -16(%rdi)
	movupd	%xmm1, (%rdi)
	subq	$-128, %rdi
	subq	$-128, %rbx
	addq	$-16, %rbp
	jne	.LBB1_49
.LBB1_50:                               # %middle.block657
	cmpq	%rsi, %r8
	movl	%r9d, %ebx
	je	.LBB1_53
	jmp	.LBB1_51
.Lfunc_end1:
	.size	Calignm1, .Lfunc_end1-Calignm1
	.cfi_endproc

	.globl	Cscore_m_1
	.p2align	4, 0x90
	.type	Cscore_m_1,@function
Cscore_m_1:                             # @Cscore_m_1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 176
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movl	%edx, %ebp
	movl	%esi, %ebx
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	(%rdi), %rdi
	callq	strlen
	movq	%rax, %r13
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, 40(%rsp)          # 8-byte Spill
	testl	%r13d, %r13d
	jle	.LBB2_1
# BB#2:                                 # %.preheader.lr.ph
	movslq	%ebp, %rcx
	movq	%rcx, %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%ecx, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	%ebx, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	%r13d, %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	xorpd	%xmm5, %xmm5
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%ebx, 68(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB2_3:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_7 Depth 2
                                        #     Child Loop BB2_11 Depth 2
	testl	%ebx, %ebx
	jle	.LBB2_4
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB2_3 Depth=1
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %r13
	xorpd	%xmm0, %xmm0
	jle	.LBB2_6
# BB#10:                                # %.lr.ph.split.us.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	88(%rsp), %r8           # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %r11          # 8-byte Reload
	movq	96(%rsp), %r10          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_11:                               # %.lr.ph.split.us
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%r10, %r10
	je	.LBB2_17
# BB#12:                                #   in Loop: Header=BB2_11 Depth=2
	movsd	(%r13), %xmm1           # xmm1 = mem[0],zero
	movq	(%r11), %r9
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %r12
	movq	16(%rsp), %rax          # 8-byte Reload
	movzbl	-1(%r12,%rax), %ebx
	xorl	%r15d, %r15d
	cmpb	$45, -1(%r9,%rax)
	movb	$1, %dl
	movb	$1, %al
	movl	$0, %ecx
	movl	$0, %esi
	jne	.LBB2_14
# BB#13:                                #   in Loop: Header=BB2_11 Depth=2
	movl	(%rdi), %esi
	incl	%esi
	movl	$1, %ecx
	xorl	%eax, %eax
.LBB2_14:                               #   in Loop: Header=BB2_11 Depth=2
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movl	%esi, (%rdi)
	cmpb	$45, %bl
	movl	$0, %esi
	jne	.LBB2_16
# BB#15:                                #   in Loop: Header=BB2_11 Depth=2
	movl	(%rbp), %esi
	incl	%esi
	movl	$1, %r15d
	xorl	%edx, %edx
.LBB2_16:                               #   in Loop: Header=BB2_11 Depth=2
	movl	%edx, 76(%rsp)          # 4-byte Spill
	movl	%esi, (%rbp)
	xorl	%edx, %edx
	xorl	%eax, %eax
	cmpl	%esi, (%rdi)
	setge	%dl
	movl	%edx, 80(%rsp)          # 4-byte Spill
	setle	%al
	movl	%eax, 84(%rsp)          # 4-byte Spill
	movq	16(%rsp), %rbx          # 8-byte Reload
	movl	%r15d, %eax
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movsbq	(%r9,%rbx), %rdx
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	cmpq	$45, %rdx
	sete	%r14b
	setne	%r15b
	movl	%eax, %r9d
	negl	%r9d
	movl	32(%rsp), %eax          # 4-byte Reload
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	andb	%r14b, %al
	movb	%al, 15(%rsp)           # 1-byte Spill
	movl	$0, %esi
	cmovel	%esi, %r9d
	movsbq	(%r12,%rbx), %rax
	xorl	%r12d, %r12d
	xorl	%esi, %esi
	cmpq	$45, %rax
	sete	%r12b
	setne	%sil
	shlq	$9, %rdx
	cvtsi2sdl	amino_dis(%rdx,%rax,4), %xmm2
	movl	%r15d, %ebx
	negl	%ecx
	andl	%ecx, %r15d
	andl	%r14d, %ecx
	andl	%esi, %r9d
	negl	%ecx
	movl	72(%rsp), %r14d         # 4-byte Reload
	andl	%r14d, %ecx
	negl	%ecx
	andl	%esi, %ecx
	movl	%esi, %eax
	movzbl	76(%rsp), %esi          # 1-byte Folded Reload
	andb	%sil, %al
	andb	15(%rsp), %al           # 1-byte Folded Reload
	andb	32(%rsp), %bl           # 1-byte Folded Reload
	movzbl	%al, %eax
	movl	%r12d, %edx
	andb	%sil, %dl
	andb	%bl, %dl
	movzbl	%dl, %edx
	addl	%eax, %edx
	addl	%edx, %r9d
	negl	%r15d
	andl	%r15d, %esi
	negl	%esi
	andl	%r12d, %esi
	addl	%r9d, %esi
	andl	%r14d, %r15d
	negl	%r15d
	andl	%r12d, %r15d
	negl	%r15d
	andl	80(%rsp), %r15d         # 4-byte Folded Reload
	addl	%esi, %r15d
	negl	%ecx
	andl	84(%rsp), %ecx          # 4-byte Folded Reload
	addl	%r15d, %ecx
	cvtsi2sdl	%ecx, %xmm3
	cvtsi2sdl	penalty(%rip), %xmm4
	mulsd	%xmm3, %xmm4
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	unpcklpd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0]
	mulpd	%xmm1, %xmm4
	addpd	%xmm4, %xmm0
.LBB2_17:                               #   in Loop: Header=BB2_11 Depth=2
	decq	%r10
	addq	$8, %r13
	addq	$8, %r11
	addq	$4, %rdi
	addq	$4, %rbp
	decq	%r8
	jne	.LBB2_11
	jmp	.LBB2_18
	.p2align	4, 0x90
.LBB2_4:                                #   in Loop: Header=BB2_3 Depth=1
	xorpd	%xmm0, %xmm0
	jmp	.LBB2_18
	.p2align	4, 0x90
.LBB2_6:                                # %.lr.ph.split.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	88(%rsp), %r11          # 8-byte Reload
	movq	96(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph.split
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rcx, %rcx
	je	.LBB2_9
# BB#8:                                 # %.thread136
                                        #   in Loop: Header=BB2_7 Depth=2
	movsd	(%r13), %xmm1           # xmm1 = mem[0],zero
	movl	$0, (%rdi)
	movl	$0, (%rsi)
	movq	(%rbp), %rdx
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	(%rax,%rbx,8), %rbx
	movq	16(%rsp), %rax          # 8-byte Reload
	movsbq	(%rbx,%rax), %r9
	cmpq	$45, %r9
	sete	%r8b
	setne	%r10b
	movsbq	(%rdx,%rax), %rdx
	cmpq	$45, %rdx
	sete	%bl
	setne	%al
	andb	%r10b, %bl
	movzbl	%bl, %ebx
	andb	%r8b, %al
	movzbl	%al, %eax
	addl	%ebx, %eax
	cvtsi2sdl	%eax, %xmm2
	cvtsi2sdl	penalty(%rip), %xmm3
	mulsd	%xmm2, %xmm3
	shlq	$9, %rdx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	amino_dis(%rdx,%r9,4), %xmm2
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	unpcklpd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	mulpd	%xmm1, %xmm3
	addpd	%xmm3, %xmm0
.LBB2_9:                                #   in Loop: Header=BB2_7 Depth=2
	addq	$8, %r13
	addq	$8, %rbp
	addq	$4, %rdi
	addq	$4, %rsi
	decq	%rcx
	decq	%r11
	jne	.LBB2_7
.LBB2_18:                               # %._crit_edge
                                        #   in Loop: Header=BB2_3 Depth=1
	movapd	%xmm0, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	addsd	%xmm1, %xmm5
	addsd	%xmm0, %xmm5
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpq	112(%rsp), %rcx         # 8-byte Folded Reload
	movl	68(%rsp), %ebx          # 4-byte Reload
	jne	.LBB2_3
	jmp	.LBB2_19
.LBB2_1:
	xorpd	%xmm5, %xmm5
.LBB2_19:                               # %._crit_edge146
	movsd	%xmm5, 32(%rsp)         # 8-byte Spill
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	Cscore_m_1, .Lfunc_end2-Cscore_m_1
	.cfi_endproc

	.type	Calignm1.mi,@object     # @Calignm1.mi
	.local	Calignm1.mi
	.comm	Calignm1.mi,4,4
	.type	Calignm1.m,@object      # @Calignm1.m
	.local	Calignm1.m
	.comm	Calignm1.m,8,8
	.type	Calignm1.mpi,@object    # @Calignm1.mpi
	.local	Calignm1.mpi
	.comm	Calignm1.mpi,4,4
	.type	Calignm1.mp,@object     # @Calignm1.mp
	.local	Calignm1.mp
	.comm	Calignm1.mp,8,8
	.type	Calignm1.g,@object      # @Calignm1.g
	.local	Calignm1.g
	.comm	Calignm1.g,8,8
	.type	Calignm1.ijp,@object    # @Calignm1.ijp
	.local	Calignm1.ijp
	.comm	Calignm1.ijp,8,8
	.type	Calignm1.v,@object      # @Calignm1.v
	.local	Calignm1.v
	.comm	Calignm1.v,8,8
	.type	Calignm1.w,@object      # @Calignm1.w
	.local	Calignm1.w
	.comm	Calignm1.w,8,8
	.type	Calignm1.gvsa,@object   # @Calignm1.gvsa
	.local	Calignm1.gvsa
	.comm	Calignm1.gvsa,8,8
	.type	Calignm1.mseq,@object   # @Calignm1.mseq
	.local	Calignm1.mseq
	.comm	Calignm1.mseq,8,8
	.type	Calignm1.nseq,@object   # @Calignm1.nseq
	.local	Calignm1.nseq
	.comm	Calignm1.nseq,8,8
	.type	Calignm1.scmx,@object   # @Calignm1.scmx
	.local	Calignm1.scmx
	.comm	Calignm1.scmx,8,8
	.type	Calignm1.orlgth,@object # @Calignm1.orlgth
	.local	Calignm1.orlgth
	.comm	Calignm1.orlgth,4,4
	.type	Calignm1.orlgth1,@object # @Calignm1.orlgth1
	.local	Calignm1.orlgth1
	.comm	Calignm1.orlgth1,4,4
	.type	Calignm1.gl,@object     # @Calignm1.gl
	.local	Calignm1.gl
	.comm	Calignm1.gl,8,8
	.type	Calignm1.gs,@object     # @Calignm1.gs
	.local	Calignm1.gs
	.comm	Calignm1.gs,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
