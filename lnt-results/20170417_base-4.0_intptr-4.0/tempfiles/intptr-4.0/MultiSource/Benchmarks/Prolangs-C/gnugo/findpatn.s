	.text
	.file	"findpatn.bc"
	.globl	findpatn
	.p2align	4, 0x90
	.type	findpatn,@function
findpatn:                               # @findpatn
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %rbp
	cmpl	$0, opn+16(%rip)
	je	.LBB0_6
# BB#1:
	movl	$0, opn+16(%rip)
	movl	findpatn.mtype(%rip), %ecx
	movl	$findpatn.cnd, %edx
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	opening
	testl	%eax, %eax
	je	.LBB0_3
# BB#2:
	movl	$1, opn+16(%rip)
.LBB0_3:
	movslq	(%rbp), %rax
	movslq	(%r14), %rcx
	imulq	$19, %rax, %rax
	cmpb	$0, p(%rax,%rcx)
	je	.LBB0_4
# BB#5:
	movl	$0, opn+16(%rip)
.LBB0_6:
	cmpl	$0, opn(%rip)
	je	.LBB0_10
# BB#7:
	movl	$0, opn(%rip)
	xorl	%edi, %edi
	xorl	%esi, %esi
	movl	$5, %edx
	movl	$5, %ecx
	callq	openregion
	testl	%eax, %eax
	je	.LBB0_10
# BB#8:
	movl	$0, findpatn.cnd(%rip)
	movl	$0, findpatn.mtype(%rip)
	movl	$findpatn.cnd, %edx
	xorl	%ecx, %ecx
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	opening
	movl	findpatn.mtype(%rip), %ecx
	movl	$findpatn.cnd, %edx
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	opening
	testl	%eax, %eax
	je	.LBB0_4
# BB#9:
	movl	$1, opn+16(%rip)
	jmp	.LBB0_4
.LBB0_10:
	cmpl	$0, opn+4(%rip)
	je	.LBB0_15
# BB#11:
	movl	$0, opn+4(%rip)
	movl	$13, %edi
	xorl	%esi, %esi
	movl	$18, %edx
	movl	$5, %ecx
	callq	openregion
	testl	%eax, %eax
	je	.LBB0_15
# BB#12:
	movl	$0, findpatn.cnd(%rip)
	movl	$1, findpatn.mtype(%rip)
	movl	$1, %ebx
	movl	$findpatn.cnd, %edx
	movl	$1, %ecx
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	opening
	movl	findpatn.mtype(%rip), %ecx
	movl	$findpatn.cnd, %edx
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	opening
	testl	%eax, %eax
	je	.LBB0_14
# BB#13:
	movl	$1, opn+16(%rip)
.LBB0_14:
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$80, (%rax)
	jmp	.LBB0_45
.LBB0_15:
	cmpl	$0, opn+8(%rip)
	je	.LBB0_19
# BB#16:
	movl	$0, opn+8(%rip)
	xorl	%edi, %edi
	movl	$13, %esi
	movl	$5, %edx
	movl	$18, %ecx
	callq	openregion
	testl	%eax, %eax
	je	.LBB0_19
# BB#17:
	movl	$0, findpatn.cnd(%rip)
	movl	$2, findpatn.mtype(%rip)
	movl	$findpatn.cnd, %edx
	movl	$2, %ecx
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	opening
	movl	findpatn.mtype(%rip), %ecx
	movl	$findpatn.cnd, %edx
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	opening
	testl	%eax, %eax
	je	.LBB0_4
# BB#18:
	movl	$1, opn+16(%rip)
	jmp	.LBB0_4
.LBB0_19:
	cmpl	$0, opn+12(%rip)
	je	.LBB0_23
# BB#20:
	movl	$0, opn+12(%rip)
	movl	$13, %edi
	movl	$13, %esi
	movl	$18, %edx
	movl	$18, %ecx
	callq	openregion
	testl	%eax, %eax
	je	.LBB0_23
# BB#21:
	movl	$0, findpatn.cnd(%rip)
	movl	$3, findpatn.mtype(%rip)
	movl	$findpatn.cnd, %edx
	movl	$3, %ecx
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	opening
	movl	findpatn.mtype(%rip), %ecx
	movl	$findpatn.cnd, %edx
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	opening
	testl	%eax, %eax
	je	.LBB0_4
# BB#22:
	movl	$1, opn+16(%rip)
	jmp	.LBB0_4
.LBB0_23:
	cmpl	$0, opn+20(%rip)
	je	.LBB0_27
# BB#24:
	movl	$0, opn+20(%rip)
	xorl	%edi, %edi
	movl	$6, %esi
	movl	$4, %edx
	movl	$11, %ecx
	callq	openregion
	testl	%eax, %eax
	je	.LBB0_27
# BB#25:
	movl	$3, (%rbp)
	movl	$9, (%r14)
	jmp	.LBB0_4
.LBB0_27:
	cmpl	$0, opn+24(%rip)
	je	.LBB0_30
# BB#28:
	movl	$0, opn+24(%rip)
	movl	$18, %edi
	movl	$6, %esi
	movl	$14, %edx
	movl	$11, %ecx
	callq	openregion
	testl	%eax, %eax
	je	.LBB0_30
# BB#29:
	movl	$15, (%rbp)
	movl	$9, (%r14)
	jmp	.LBB0_4
.LBB0_30:
	cmpl	$0, opn+28(%rip)
	je	.LBB0_33
# BB#31:
	movl	$0, opn+28(%rip)
	movl	$6, %edi
	xorl	%esi, %esi
	movl	$11, %edx
	movl	$4, %ecx
	callq	openregion
	testl	%eax, %eax
	je	.LBB0_33
# BB#32:
	movl	$9, (%rbp)
	movl	$3, (%r14)
	jmp	.LBB0_4
.LBB0_33:
	cmpl	$0, opn+32(%rip)
	je	.LBB0_36
# BB#34:
	movl	$0, opn+32(%rip)
	movl	$6, %edi
	movl	$18, %esi
	movl	$11, %edx
	movl	$14, %ecx
	callq	openregion
	testl	%eax, %eax
	je	.LBB0_36
# BB#35:
	movl	$9, (%rbp)
	movl	$15, (%r14)
.LBB0_4:
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$80, (%rax)
	movl	$1, %ebx
.LBB0_45:
	movl	%ebx, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_36:
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movl	$-1, (%rbp)
	movq	%r14, 48(%rsp)          # 8-byte Spill
	movl	$-1, (%r14)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$-1, (%rax)
	movl	$p, %r12d
	xorl	%ebx, %ebx
	leaq	28(%rsp), %r13
	leaq	24(%rsp), %rbp
	leaq	20(%rsp), %r14
	.p2align	4, 0x90
.LBB0_37:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_38 Depth 2
	movq	%r12, 32(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_38:                               #   Parent Loop BB0_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r12), %eax
	cmpl	mymove(%rip), %eax
	jne	.LBB0_42
# BB#39:                                #   in Loop: Header=BB0_38 Depth=2
	movl	%ebx, %edi
	movl	%r15d, %esi
	movq	%r13, %rdx
	movq	%rbp, %rcx
	movq	%r14, %r8
	callq	matchpat
	testl	%eax, %eax
	je	.LBB0_42
# BB#40:                                #   in Loop: Header=BB0_38 Depth=2
	movl	20(%rsp), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	(%rcx), %eax
	jle	.LBB0_42
# BB#41:                                #   in Loop: Header=BB0_38 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%eax, (%rcx)
	movl	28(%rsp), %eax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movl	24(%rsp), %eax
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	.p2align	4, 0x90
.LBB0_42:                               #   in Loop: Header=BB0_38 Depth=2
	incq	%r15
	incq	%r12
	cmpq	$19, %r15
	jne	.LBB0_38
# BB#43:                                #   in Loop: Header=BB0_37 Depth=1
	incq	%rbx
	movq	32(%rsp), %r12          # 8-byte Reload
	addq	$19, %r12
	cmpq	$19, %rbx
	jne	.LBB0_37
# BB#44:
	xorl	%ebx, %ebx
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, (%rax)
	setg	%bl
	jmp	.LBB0_45
.Lfunc_end0:
	.size	findpatn, .Lfunc_end0-findpatn
	.cfi_endproc

	.type	findpatn.cnd,@object    # @findpatn.cnd
	.local	findpatn.cnd
	.comm	findpatn.cnd,4,4
	.type	findpatn.mtype,@object  # @findpatn.mtype
	.local	findpatn.mtype
	.comm	findpatn.mtype,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
