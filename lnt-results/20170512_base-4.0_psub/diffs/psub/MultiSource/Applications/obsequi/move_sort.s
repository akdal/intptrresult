	.text
	.file	"move_sort.bc"
	.globl	sort_moves
	.p2align	4, 0x90
	.type	sort_moves,@function
sort_moves:                             # @sort_moves
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$394248, %rsp           # imm = 0x60408
.Lcfi6:
	.cfi_def_cfa_offset 394304
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %r13d
	movq	%rdi, %r15
	xorl	%r12d, %r12d
	cmpl	%r14d, %r13d
	jge	.LBB0_6
# BB#1:                                 # %.preheader61.preheader
	movslq	%r13d, %rbx
	leaq	1024(%rsp), %rbp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_2:                                # %.preheader61
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
	testl	%r12d, %r12d
	movl	$0, %eax
	jle	.LBB0_19
# BB#3:                                 # %.lr.ph73
                                        #   in Loop: Header=BB0_2 Depth=1
	leaq	(%rbx,%rbx,2), %rcx
	movl	8(%r15,%rcx,4), %esi
	movslq	%r12d, %rdi
	movq	%rbp, %rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_4:                                #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	(%rsp,%rax,4), %esi
	je	.LBB0_5
# BB#18:                                #   in Loop: Header=BB0_4 Depth=2
	incq	%rax
	addq	$3072, %rdx             # imm = 0xC00
	cmpq	%rdi, %rax
	jl	.LBB0_4
	jmp	.LBB0_19
	.p2align	4, 0x90
.LBB0_5:                                #   in Loop: Header=BB0_2 Depth=1
	movslq	512(%rsp,%rax,4), %rsi
	leal	1(%rsi), %edi
	movl	%edi, 512(%rsp,%rax,4)
	leaq	(%rsi,%rsi,2), %rsi
	movl	8(%r15,%rcx,4), %edi
	movl	%edi, 8(%rdx,%rsi,4)
	movq	(%r15,%rcx,4), %rcx
	movq	%rcx, (%rdx,%rsi,4)
.LBB0_19:                               # %.loopexit
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpl	%r12d, %eax
	jne	.LBB0_23
# BB#20:                                #   in Loop: Header=BB0_2 Depth=1
	cmpl	$128, %r12d
	jne	.LBB0_22
# BB#21:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str, %edi
	movl	$34, %esi
	movl	$1, %edx
	movl	$.L.str.1, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
.LBB0_22:                               #   in Loop: Header=BB0_2 Depth=1
	movslq	%r12d, %rax
	leaq	(%rax,%rax,2), %rcx
	shlq	$10, %rcx
	leaq	(%rbx,%rbx,2), %rdx
	movl	8(%r15,%rdx,4), %esi
	movl	%esi, 1032(%rsp,%rcx)
	movq	(%r15,%rdx,4), %rsi
	movq	%rsi, 1024(%rsp,%rcx)
	movl	8(%r15,%rdx,4), %ecx
	movl	%ecx, (%rsp,%rax,4)
	movl	$1, 512(%rsp,%rax,4)
	incl	%r12d
.LBB0_23:                               #   in Loop: Header=BB0_2 Depth=1
	incq	%rbx
	cmpl	%r14d, %ebx
	jne	.LBB0_2
.LBB0_6:                                # %.preheader60
	cmpl	%r14d, %r13d
	je	.LBB0_29
# BB#7:                                 # %.lr.ph71
	cmpl	$2, %r12d
	jl	.LBB0_24
# BB#8:                                 # %.lr.ph71.split.us.preheader
	movl	%r12d, %r10d
	addl	$3, %r12d
	leaq	-2(%r10), %r8
	andl	$3, %r12d
	movq	%r12, %rdx
	negq	%rdx
	.p2align	4, 0x90
.LBB0_9:                                # %.lr.ph71.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_12 Depth 2
                                        #     Child Loop BB0_14 Depth 2
                                        #     Child Loop BB0_16 Depth 2
	testq	%r12, %r12
	movl	(%rsp), %esi
	je	.LBB0_10
# BB#11:                                # %.prol.preheader
                                        #   in Loop: Header=BB0_9 Depth=1
	xorl	%ebx, %ebx
	movl	$1, %edi
	.p2align	4, 0x90
.LBB0_12:                               #   Parent Loop BB0_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsp,%rdi,4), %ecx
	cmpl	%esi, %ecx
	cmovgel	%ecx, %esi
	cmovgl	%edi, %ebx
	leaq	1(%rdx,%rdi), %rcx
	incq	%rdi
	cmpq	$1, %rcx
	jne	.LBB0_12
	jmp	.LBB0_13
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_9 Depth=1
	movl	$1, %edi
	xorl	%ebx, %ebx
.LBB0_13:                               # %.prol.loopexit
                                        #   in Loop: Header=BB0_9 Depth=1
	cmpq	$3, %r8
	jb	.LBB0_15
	.p2align	4, 0x90
.LBB0_14:                               #   Parent Loop BB0_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsp,%rdi,4), %ecx
	cmpl	%esi, %ecx
	cmovgel	%ecx, %esi
	cmovgl	%edi, %ebx
	movl	4(%rsp,%rdi,4), %ecx
	leal	1(%rdi), %ebp
	cmpl	%esi, %ecx
	cmovgel	%ecx, %esi
	cmovlel	%ebx, %ebp
	movl	8(%rsp,%rdi,4), %ecx
	leal	2(%rdi), %eax
	cmpl	%esi, %ecx
	cmovgel	%ecx, %esi
	cmovlel	%ebp, %eax
	movl	12(%rsp,%rdi,4), %ecx
	leal	3(%rdi), %ebx
	cmpl	%esi, %ecx
	cmovgel	%ecx, %esi
	cmovlel	%eax, %ebx
	addq	$4, %rdi
	cmpq	%r10, %rdi
	jne	.LBB0_14
.LBB0_15:                               # %..preheader_crit_edge.us
                                        #   in Loop: Header=BB0_9 Depth=1
	movslq	%ebx, %r9
	movslq	512(%rsp,%r9,4), %rdi
	movslq	%r13d, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%r15,%rax,4), %rbx
	leaq	(%r9,%r9,2), %rax
	shlq	$10, %rax
	leaq	1024(%rsp,%rax), %rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_16:                               #   Parent Loop BB0_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rcx
	movl	8(%rbp), %esi
	movl	%esi, 8(%rbx)
	movq	(%rbp), %rsi
	movq	%rsi, (%rbx)
	addq	$12, %rbx
	addq	$12, %rbp
	cmpq	%rdi, %rcx
	jl	.LBB0_16
# BB#17:                                #   in Loop: Header=BB0_9 Depth=1
	movl	%r14d, %esi
	subl	%r13d, %esi
	addl	%ecx, %r13d
	movl	$-5000, (%rsp,%r9,4)    # imm = 0xEC78
	cmpl	%ecx, %esi
	jne	.LBB0_9
	jmp	.LBB0_29
.LBB0_24:                               # %.lr.ph71.split.preheader
	movslq	512(%rsp), %rax
	leaq	1024(%rsp), %rcx
	.p2align	4, 0x90
.LBB0_25:                               # %.lr.ph71.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_26 Depth 2
	movslq	%r13d, %rdx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%r15,%rdx,4), %rsi
	movq	%rcx, %rdi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_26:                               #   Parent Loop BB0_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rdx
	movl	8(%rdi), %ebp
	movl	%ebp, 8(%rsi)
	movq	(%rdi), %rbp
	movq	%rbp, (%rsi)
	addq	$12, %rsi
	addq	$12, %rdi
	cmpq	%rax, %rdx
	jl	.LBB0_26
# BB#27:                                #   in Loop: Header=BB0_25 Depth=1
	movl	%r14d, %esi
	subl	%r13d, %esi
	addl	%edx, %r13d
	cmpl	%edx, %esi
	jne	.LBB0_25
# BB#28:                                # %._crit_edge.loopexit97
	movl	$-5000, (%rsp)          # imm = 0xEC78
.LBB0_29:                               # %._crit_edge
	addq	$394248, %rsp           # imm = 0x60408
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	sort_moves, .Lfunc_end0-sort_moves
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/obsequi/move_sort.c"
	.size	.L.str, 83

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Not enough buckets.\n"
	.size	.L.str.1, 21


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
