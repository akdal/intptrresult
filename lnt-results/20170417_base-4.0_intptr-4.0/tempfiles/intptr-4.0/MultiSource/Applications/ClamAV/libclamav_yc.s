	.text
	.file	"libclamav_yc.bc"
	.globl	yc_decrypt
	.p2align	4, 0x90
	.type	yc_decrypt,@function
yc_decrypt:                             # @yc_decrypt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, 16(%rsp)          # 4-byte Spill
	movl	%ecx, %r15d
	movq	%rdx, %rbp
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movq	%rdi, %rbx
	movl	%r15d, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	(%rax,%rax,8), %r14
	movl	%r8d, %r12d
	movzwl	20(%rbx,%r12), %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	8(%rbp,%r14,4), %r13d
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	cli_dbgmsg
	leaq	147(%rbx,%r13), %rdi
	leaq	198(%rbx,%r13), %rsi
	movl	$2967, %edx             # imm = 0xB97
	callq	yc_poly_emulator
	movl	$1, %ecx
	testl	%eax, %eax
	jne	.LBB0_19
# BB#1:
	movq	8(%rsp), %rcx           # 8-byte Reload
	addq	%rbx, %r12
	addq	%rbx, %r13
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movl	4(%rsp), %eax           # 4-byte Reload
	subl	32(%rbp,%r14,4), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	testl	%r15d, %r15d
	movl	%r15d, %ebp
	movq	%r12, %rsi
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %r8
	je	.LBB0_16
# BB#2:                                 # %.lr.ph
	leaq	24(%rcx,%rsi), %rdx
	leaq	1111(%r8), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax), %r13
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%r13)
	je	.LBB0_15
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	movl	%r12d, %eax
	andl	$-8, %eax
	movl	(%rdx,%rax), %eax
	movzwl	%ax, %ecx
	cmpl	$17273, %ecx            # imm = 0x4379
	je	.LBB0_15
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=1
	cmpl	$1936487470, %eax       # imm = 0x736C742E
	je	.LBB0_15
# BB#6:                                 #   in Loop: Header=BB0_3 Depth=1
	cmpl	$1633970478, %eax       # imm = 0x6164692E
	je	.LBB0_15
# BB#7:                                 #   in Loop: Header=BB0_3 Depth=1
	cmpl	$1633972782, %eax       # imm = 0x6164722E
	je	.LBB0_15
# BB#8:                                 #   in Loop: Header=BB0_3 Depth=1
	cmpl	$1633969454, %eax       # imm = 0x6164652E
	je	.LBB0_15
# BB#9:                                 #   in Loop: Header=BB0_3 Depth=1
	cmpl	$1818587694, %eax       # imm = 0x6C65722E
	je	.LBB0_15
# BB#10:                                #   in Loop: Header=BB0_3 Depth=1
	cmpl	$1869374834, %eax       # imm = 0x6F6C6572
	je	.LBB0_15
# BB#11:                                #   in Loop: Header=BB0_3 Depth=1
	cmpl	$1920168494, %eax       # imm = 0x7273722E
	je	.LBB0_15
# BB#12:                                #   in Loop: Header=BB0_3 Depth=1
	cmpl	$1668445042, %eax       # imm = 0x63727372
	je	.LBB0_15
# BB#13:                                #   in Loop: Header=BB0_3 Depth=1
	movl	4(%r13), %eax
	testl	%eax, %eax
	je	.LBB0_15
# BB#14:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	movq	%r8, %rbp
	movq	%rdx, %r14
	callq	cli_dbgmsg
	movl	(%r13), %esi
	movl	24(%r13), %edx
	addq	%rbx, %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	yc_poly_emulator
	movq	%r14, %rdx
	movq	%rbp, %r8
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	40(%rsp), %r14          # 8-byte Reload
	movl	20(%rsp), %ebp          # 4-byte Reload
	testl	%eax, %eax
	jne	.LBB0_18
.LBB0_15:                               # %.thread
                                        #   in Loop: Header=BB0_3 Depth=1
	incq	%r15
	addq	$40, %r12
	addq	$36, %r13
	cmpq	%rdi, %r15
	jb	.LBB0_3
.LBB0_16:                               # %._crit_edge
	movw	%bp, 6(%rsi)
	movq	$0, 128(%rsi)
	movl	2575(%r8), %eax
	movl	%eax, 40(%rsi)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	4(%rax,%r14,4), %eax
	subl	%eax, 80(%rsi)
	movl	16(%rsp), %edi          # 4-byte Reload
	movq	%rbx, %rsi
	movl	4(%rsp), %edx           # 4-byte Reload
	callq	cli_writen
	xorl	%ecx, %ecx
	cmpl	$-1, %eax
	jne	.LBB0_19
# BB#17:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_18:                               # %.loopexit
	movl	$1, %ecx
.LBB0_19:                               # %.loopexit
	movl	%ecx, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	yc_decrypt, .Lfunc_end0-yc_decrypt
	.cfi_endproc

	.p2align	4, 0x90
	.type	yc_poly_emulator,@function
yc_poly_emulator:                       # @yc_poly_emulator
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	testl	%edx, %edx
	je	.LBB1_25
# BB#1:                                 # %.lr.ph.preheader
	movl	%edx, %r9d
	xorl	%r10d, %r10d
.LBB1_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_3 Depth 2
	movb	(%rsi,%r10), %bpl
	movl	%edx, %r11d
	andb	$7, %r11b
	movzbl	%r11b, %eax
	movl	$8, %r8d
	subl	%eax, %r8d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_3:                                #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %eax
	movsbl	(%rdi,%rax), %ecx
	leal	64(%rcx), %eax
	cmpl	$116, %eax
	ja	.LBB1_7
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=2
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_5:                                #   in Loop: Header=BB1_3 Depth=2
	leal	1(%rbx), %eax
	addl	$2, %ebx
	movzbl	%bpl, %ebp
	movzbl	(%rdi,%rbx), %ecx
	movl	%ecx, %r14d
	andl	$7, %r14d
	andl	$7, %ecx
	cmpb	$-64, (%rdi,%rax)
	jne	.LBB1_18
# BB#6:                                 #   in Loop: Header=BB1_3 Depth=2
	movl	%ebp, %eax
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %eax
	movl	$8, %ecx
	subl	%r14d, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	jmp	.LBB1_21
.LBB1_7:                                #   in Loop: Header=BB1_3 Depth=2
	cmpl	$-112, %ecx
	je	.LBB1_23
	jmp	.LBB1_26
.LBB1_8:                                #   in Loop: Header=BB1_3 Depth=2
	leal	1(%rbx), %eax
	addl	$2, %ebx
	movzbl	%bpl, %ebp
	cmpb	$-56, (%rdi,%rax)
	jne	.LBB1_20
# BB#9:                                 #   in Loop: Header=BB1_3 Depth=2
	movl	%ebp, %eax
	movl	%r11d, %ecx
	shrl	%cl, %eax
	movl	%r8d, %ecx
	jmp	.LBB1_19
.LBB1_10:                               #   in Loop: Header=BB1_3 Depth=2
	leal	1(%rbx), %eax
	movsbl	(%rdi,%rax), %eax
	leal	1(%rax,%rbx), %ebx
	jmp	.LBB1_23
.LBB1_11:                               #   in Loop: Header=BB1_3 Depth=2
	decb	%bpl
	incl	%ebx
	jmp	.LBB1_23
.LBB1_12:                               #   in Loop: Header=BB1_3 Depth=2
	addb	%dl, %bpl
	incl	%ebx
	jmp	.LBB1_23
.LBB1_13:                               #   in Loop: Header=BB1_3 Depth=2
	incl	%ebx
	addb	(%rdi,%rbx), %bpl
	jmp	.LBB1_23
.LBB1_14:                               #   in Loop: Header=BB1_3 Depth=2
	subb	%dl, %bpl
	incl	%ebx
	jmp	.LBB1_23
.LBB1_15:                               #   in Loop: Header=BB1_3 Depth=2
	incl	%ebx
	subb	(%rdi,%rbx), %bpl
	jmp	.LBB1_23
.LBB1_16:                               #   in Loop: Header=BB1_3 Depth=2
	xorb	%dl, %bpl
	incl	%ebx
	jmp	.LBB1_23
.LBB1_17:                               #   in Loop: Header=BB1_3 Depth=2
	incl	%ebx
	xorb	(%rdi,%rbx), %bpl
	jmp	.LBB1_23
.LBB1_18:                               #   in Loop: Header=BB1_3 Depth=2
	movl	%ebp, %eax
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shrl	%cl, %eax
	movl	$8, %ecx
	subl	%r14d, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
.LBB1_19:                               #   in Loop: Header=BB1_3 Depth=2
	shll	%cl, %ebp
	jmp	.LBB1_22
.LBB1_20:                               #   in Loop: Header=BB1_3 Depth=2
	movl	%ebp, %eax
	movl	%r11d, %ecx
	shll	%cl, %eax
	movl	%r8d, %ecx
.LBB1_21:                               #   in Loop: Header=BB1_3 Depth=2
	shrl	%cl, %ebp
.LBB1_22:                               #   in Loop: Header=BB1_3 Depth=2
	orl	%eax, %ebp
	.p2align	4, 0x90
.LBB1_23:                               #   in Loop: Header=BB1_3 Depth=2
	incl	%ebx
	cmpl	$48, %ebx
	jb	.LBB1_3
# BB#24:                                #   in Loop: Header=BB1_2 Depth=1
	decb	%dl
	movb	%bpl, (%rsi,%r10)
	incq	%r10
	cmpq	%r9, %r10
	jb	.LBB1_2
.LBB1_25:
	xorl	%eax, %eax
	jmp	.LBB1_27
.LBB1_26:
	movzbl	%cl, %esi
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, %eax
.LBB1_27:                               # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	yc_poly_emulator, .Lfunc_end1-yc_poly_emulator
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_5
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_8
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_10
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_23
	.quad	.LBB1_23
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_11
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_12
	.quad	.LBB1_26
	.quad	.LBB1_13
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_14
	.quad	.LBB1_26
	.quad	.LBB1_15
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_16
	.quad	.LBB1_26
	.quad	.LBB1_17

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"yC: decrypting decryptor on sect %d\n"
	.size	.L.str, 37

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"yC: decrypting sect%d\n"
	.size	.L.str.1, 23

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"yC: Cannot write unpacked file\n"
	.size	.L.str.2, 32

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"yC: Unhandled opcode %x\n"
	.size	.L.str.3, 25


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
