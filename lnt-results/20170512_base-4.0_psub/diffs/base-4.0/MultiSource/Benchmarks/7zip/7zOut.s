	.text
	.file	"7zOut.bc"
	.globl	_ZN8NArchive3N7z11COutArchive11WriteDirectEPKvj
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive11WriteDirectEPKvj,@function
_ZN8NArchive3N7z11COutArchive11WriteDirectEPKvj: # @_ZN8NArchive3N7z11COutArchive11WriteDirectEPKvj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	xorl	%r14d, %r14d
	testl	%edx, %edx
	je	.LBB0_7
# BB#1:                                 # %.lr.ph.i
	movq	120(%rdi), %r12
	movl	%edx, %ebp
	movl	$4294967295, %r13d      # imm = 0xFFFFFFFF
	leaq	4(%rsp), %r15
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	cmpq	%r13, %rbp
	movl	$-1, %edx
	cmovbl	%ebp, %edx
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rcx
	callq	*40(%rax)
	testl	%eax, %eax
	jne	.LBB0_6
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	4(%rsp), %eax
	testq	%rax, %rax
	je	.LBB0_4
# BB#5:                                 # %.thread.i
                                        #   in Loop: Header=BB0_2 Depth=1
	addq	%rax, %rbx
	subq	%rax, %rbp
	jne	.LBB0_2
	jmp	.LBB0_7
.LBB0_6:
	movl	%eax, %r14d
	jmp	.LBB0_7
.LBB0_4:                                # %.thread21.i
	movl	$-2147467259, %r14d     # imm = 0x80004005
.LBB0_7:                                # %_ZL10WriteBytesP20ISequentialOutStreamPKvm.exit
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN8NArchive3N7z11COutArchive11WriteDirectEPKvj, .Lfunc_end0-_ZN8NArchive3N7z11COutArchive11WriteDirectEPKvj
	.cfi_endproc

	.globl	_ZN8NArchive3N7z11COutArchive14WriteSignatureEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive14WriteSignatureEv,@function
_ZN8NArchive3N7z11COutArchive14WriteSignatureEv: # @_ZN8NArchive3N7z11COutArchive14WriteSignatureEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi18:
	.cfi_def_cfa_offset 64
.Lcfi19:
	.cfi_offset %rbx, -48
.Lcfi20:
	.cfi_offset %r12, -40
.Lcfi21:
	.cfi_offset %r13, -32
.Lcfi22:
	.cfi_offset %r14, -24
.Lcfi23:
	.cfi_offset %r15, -16
	leaq	4(%rsp), %r12
	movzwl	_ZN8NArchive3N7z10kSignatureE+4(%rip), %eax
	movw	%ax, 8(%rsp)
	movl	_ZN8NArchive3N7z10kSignatureE(%rip), %eax
	movl	%eax, 4(%rsp)
	movw	$768, 10(%rsp)          # imm = 0x300
	movq	120(%rdi), %r14
	movl	$8, %ebx
	movl	$4294967295, %r13d      # imm = 0xFFFFFFFF
	leaq	12(%rsp), %r15
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	cmpq	%r13, %rbx
	movl	$-1, %edx
	cmovbl	%ebx, %edx
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%r15, %rcx
	callq	*40(%rax)
	testl	%eax, %eax
	jne	.LBB1_6
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	movl	12(%rsp), %eax
	testq	%rax, %rax
	je	.LBB1_3
# BB#4:                                 # %.thread.i.i
                                        #   in Loop: Header=BB1_1 Depth=1
	addq	%rax, %r12
	subq	%rax, %rbx
	jne	.LBB1_1
# BB#5:
	xorl	%eax, %eax
	jmp	.LBB1_6
.LBB1_3:                                # %.thread21.i.i
	movl	$-2147467259, %eax      # imm = 0x80004005
.LBB1_6:                                # %_ZN8NArchive3N7z11COutArchive11WriteDirectEPKvj.exit
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	_ZN8NArchive3N7z11COutArchive14WriteSignatureEv, .Lfunc_end1-_ZN8NArchive3N7z11COutArchive14WriteSignatureEv
	.cfi_endproc

	.globl	_ZN8NArchive3N7z11COutArchive16WriteStartHeaderERKNS0_12CStartHeaderE
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive16WriteStartHeaderERKNS0_12CStartHeaderE,@function
_ZN8NArchive3N7z11COutArchive16WriteStartHeaderERKNS0_12CStartHeaderE: # @_ZN8NArchive3N7z11COutArchive16WriteStartHeaderERKNS0_12CStartHeaderE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 80
.Lcfi30:
	.cfi_offset %rbx, -48
.Lcfi31:
	.cfi_offset %r12, -40
.Lcfi32:
	.cfi_offset %r13, -32
.Lcfi33:
	.cfi_offset %r14, -24
.Lcfi34:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	%rsp, %r12
	leaq	4(%rsp), %rdi
	movq	(%rsi), %rax
	movb	%al, 4(%rsp)
	movb	%ah, 5(%rsp)  # NOREX
	movq	%rax, %rcx
	shrq	$16, %rcx
	movb	%cl, 6(%rsp)
	movq	%rax, %rcx
	shrq	$24, %rcx
	movb	%cl, 7(%rsp)
	movq	%rax, %rcx
	shrq	$32, %rcx
	movb	%cl, 8(%rsp)
	movq	%rax, %rcx
	shrq	$40, %rcx
	movb	%cl, 9(%rsp)
	movq	%rax, %rcx
	shrq	$48, %rcx
	movb	%cl, 10(%rsp)
	shrq	$56, %rax
	movb	%al, 11(%rsp)
	movq	8(%rsi), %rax
	movb	%al, 12(%rsp)
	movb	%ah, 13(%rsp)  # NOREX
	movq	%rax, %rcx
	shrq	$16, %rcx
	movb	%cl, 14(%rsp)
	movq	%rax, %rcx
	shrq	$24, %rcx
	movb	%cl, 15(%rsp)
	movq	%rax, %rcx
	shrq	$32, %rcx
	movb	%cl, 16(%rsp)
	movq	%rax, %rcx
	shrq	$40, %rcx
	movb	%cl, 17(%rsp)
	movq	%rax, %rcx
	shrq	$48, %rcx
	movb	%cl, 18(%rsp)
	shrq	$56, %rax
	movb	%al, 19(%rsp)
	movl	16(%rsi), %eax
	movb	%al, 20(%rsp)
	movb	%ah, 21(%rsp)  # NOREX
	movl	%eax, %ecx
	shrl	$16, %ecx
	movb	%cl, 22(%rsp)
	shrl	$24, %eax
	movb	%al, 23(%rsp)
	movl	$20, %esi
	callq	CrcCalc
	movb	%al, (%rsp)
	movb	%ah, 1(%rsp)  # NOREX
	movl	%eax, %ecx
	shrl	$16, %ecx
	movb	%cl, 2(%rsp)
	shrl	$24, %eax
	movb	%al, 3(%rsp)
	movq	120(%r14), %r14
	movl	$24, %ebx
	movl	$4294967295, %r13d      # imm = 0xFFFFFFFF
	leaq	28(%rsp), %r15
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	cmpq	%r13, %rbx
	movl	$-1, %edx
	cmovbl	%ebx, %edx
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%r15, %rcx
	callq	*40(%rax)
	testl	%eax, %eax
	jne	.LBB2_6
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
	movl	28(%rsp), %eax
	testq	%rax, %rax
	je	.LBB2_3
# BB#4:                                 # %.thread.i.i
                                        #   in Loop: Header=BB2_1 Depth=1
	addq	%rax, %r12
	subq	%rax, %rbx
	jne	.LBB2_1
# BB#5:
	xorl	%eax, %eax
	jmp	.LBB2_6
.LBB2_3:                                # %.thread21.i.i
	movl	$-2147467259, %eax      # imm = 0x80004005
.LBB2_6:                                # %_ZN8NArchive3N7z11COutArchive11WriteDirectEPKvj.exit
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	_ZN8NArchive3N7z11COutArchive16WriteStartHeaderERKNS0_12CStartHeaderE, .Lfunc_end2-_ZN8NArchive3N7z11COutArchive16WriteStartHeaderERKNS0_12CStartHeaderE
	.cfi_endproc

	.globl	_ZN8NArchive3N7z11COutArchive6CreateEP20ISequentialOutStreamb
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive6CreateEP20ISequentialOutStreamb,@function
_ZN8NArchive3N7z11COutArchive6CreateEP20ISequentialOutStreamb: # @_ZN8NArchive3N7z11COutArchive6CreateEP20ISequentialOutStreamb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi41:
	.cfi_def_cfa_offset 80
.Lcfi42:
	.cfi_offset %rbx, -56
.Lcfi43:
	.cfi_offset %r12, -48
.Lcfi44:
	.cfi_offset %r13, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	120(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB3_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 120(%r14)
.LBB3_2:                                # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit.i
	leaq	112(%r14), %rbp
	movq	112(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB3_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, (%rbp)
.LBB3_4:                                # %_ZN8NArchive3N7z11COutArchive5CloseEv.exit
	testq	%rbx, %rbx
	je	.LBB3_6
# BB#5:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
.LBB3_6:
	movq	120(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB3_8
# BB#7:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB3_8:                                # %_ZN9CMyComPtrI20ISequentialOutStreamEaSEPS0_.exit
	movq	%rbx, 120(%r14)
	testb	%r15b, %r15b
	je	.LBB3_11
# BB#9:
	cmpq	$0, (%rbp)
	jne	.LBB3_12
	jmp	.LBB3_18
.LBB3_11:
	movq	(%rbx), %rax
	movl	$IID_IOutStream, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	*(%rax)
	cmpq	$0, (%rbp)
	je	.LBB3_17
.LBB3_12:                               # %.thread
	leaq	12(%rsp), %rbx
	movzwl	_ZN8NArchive3N7z10kSignatureE+4(%rip), %eax
	movw	%ax, 16(%rsp)
	movl	_ZN8NArchive3N7z10kSignatureE(%rip), %eax
	movl	%eax, 12(%rsp)
	movw	$768, 18(%rsp)          # imm = 0x300
	movq	120(%r14), %r12
	movl	$8, %ebp
	movl	$4294967295, %r13d      # imm = 0xFFFFFFFF
	leaq	20(%rsp), %r15
	.p2align	4, 0x90
.LBB3_13:                               # =>This Inner Loop Header: Depth=1
	cmpq	%r13, %rbp
	movl	$-1, %edx
	cmovbl	%ebp, %edx
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rcx
	callq	*40(%rax)
	testl	%eax, %eax
	jne	.LBB3_19
# BB#14:                                #   in Loop: Header=BB3_13 Depth=1
	movl	20(%rsp), %eax
	testq	%rax, %rax
	je	.LBB3_18
# BB#15:                                # %.thread.i.i.i
                                        #   in Loop: Header=BB3_13 Depth=1
	addq	%rax, %rbx
	subq	%rax, %rbp
	jne	.LBB3_13
# BB#16:
	movq	112(%r14), %rdi
	movq	(%rdi), %rax
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	*48(%rax)
	jmp	.LBB3_19
.LBB3_18:                               # %.thread21.i.i.i
	movl	$-2147467259, %eax      # imm = 0x80004005
	jmp	.LBB3_19
.LBB3_17:
	movl	$-2147467263, %eax      # imm = 0x80004001
.LBB3_19:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZN8NArchive3N7z11COutArchive6CreateEP20ISequentialOutStreamb, .Lfunc_end3-_ZN8NArchive3N7z11COutArchive6CreateEP20ISequentialOutStreamb
	.cfi_endproc

	.globl	_ZN8NArchive3N7z11COutArchive5CloseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive5CloseEv,@function
_ZN8NArchive3N7z11COutArchive5CloseEv:  # @_ZN8NArchive3N7z11COutArchive5CloseEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 16
.Lcfi49:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 120(%rbx)
.LBB4_2:                                # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 112(%rbx)
.LBB4_4:                                # %_ZN9CMyComPtrI10IOutStreamE7ReleaseEv.exit
	popq	%rbx
	retq
.Lfunc_end4:
	.size	_ZN8NArchive3N7z11COutArchive5CloseEv, .Lfunc_end4-_ZN8NArchive3N7z11COutArchive5CloseEv
	.cfi_endproc

	.globl	_ZN8NArchive3N7z11COutArchive23SkipPrefixArchiveHeaderEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive23SkipPrefixArchiveHeaderEv,@function
_ZN8NArchive3N7z11COutArchive23SkipPrefixArchiveHeaderEv: # @_ZN8NArchive3N7z11COutArchive23SkipPrefixArchiveHeaderEv
	.cfi_startproc
# BB#0:
	movq	112(%rdi), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	movl	$24, %esi
	movl	$1, %edx
	xorl	%ecx, %ecx
	jmpq	*%rax                   # TAILCALL
.Lfunc_end5:
	.size	_ZN8NArchive3N7z11COutArchive23SkipPrefixArchiveHeaderEv, .Lfunc_end5-_ZN8NArchive3N7z11COutArchive23SkipPrefixArchiveHeaderEv
	.cfi_endproc

	.globl	_ZNK8NArchive3N7z11COutArchive6GetPosEv
	.p2align	4, 0x90
	.type	_ZNK8NArchive3N7z11COutArchive6GetPosEv,@function
_ZNK8NArchive3N7z11COutArchive6GetPosEv: # @_ZNK8NArchive3N7z11COutArchive6GetPosEv
	.cfi_startproc
# BB#0:
	cmpb	$0, 8(%rdi)
	je	.LBB6_2
# BB#1:
	movq	16(%rdi), %rax
	retq
.LBB6_2:
	cmpb	$0, 9(%rdi)
	je	.LBB6_3
# BB#5:
	addq	$32, %rdi
	jmp	_ZNK10COutBuffer16GetProcessedSizeEv # TAILCALL
.LBB6_3:
	movq	104(%rdi), %rax
	retq
.Lfunc_end6:
	.size	_ZNK8NArchive3N7z11COutArchive6GetPosEv, .Lfunc_end6-_ZNK8NArchive3N7z11COutArchive6GetPosEv
	.cfi_endproc

	.globl	_ZN8NArchive3N7z11COutArchive10WriteBytesEPKvm
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive10WriteBytesEPKvm,@function
_ZN8NArchive3N7z11COutArchive10WriteBytesEPKvm: # @_ZN8NArchive3N7z11COutArchive10WriteBytesEPKvm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi54:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi56:
	.cfi_def_cfa_offset 64
.Lcfi57:
	.cfi_offset %rbx, -56
.Lcfi58:
	.cfi_offset %r12, -48
.Lcfi59:
	.cfi_offset %r13, -40
.Lcfi60:
	.cfi_offset %r14, -32
.Lcfi61:
	.cfi_offset %r15, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r13
	cmpb	$0, 8(%r13)
	je	.LBB7_2
# BB#1:
	addq	%r14, 16(%r13)
	jmp	.LBB7_11
.LBB7_2:
	cmpb	$0, 9(%r13)
	je	.LBB7_9
# BB#3:
	testq	%r14, %r14
	je	.LBB7_8
# BB#4:                                 # %.lr.ph.i
	leaq	32(%r13), %r12
	movq	%r15, %rbx
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB7_5:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %eax
	movq	32(%r13), %rcx
	movl	40(%r13), %edx
	leal	1(%rdx), %esi
	movl	%esi, 40(%r13)
	movb	%al, (%rcx,%rdx)
	movl	40(%r13), %eax
	cmpl	44(%r13), %eax
	jne	.LBB7_7
# BB#6:                                 #   in Loop: Header=BB7_5 Depth=1
	movq	%r12, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB7_7:                                # %_ZN10COutBuffer9WriteByteEh.exit.i
                                        #   in Loop: Header=BB7_5 Depth=1
	incq	%rbx
	decq	%rbp
	jne	.LBB7_5
.LBB7_8:                                # %_ZN10COutBuffer10WriteBytesEPKvm.exit
	movl	24(%r13), %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	CrcUpdate
	movl	%eax, 24(%r13)
	jmp	.LBB7_11
.LBB7_9:
	movq	96(%r13), %rax
	movq	104(%r13), %rdi
	subq	%rdi, %rax
	cmpq	%r14, %rax
	jb	.LBB7_12
# BB#10:                                # %_ZN8NArchive3N7z15CWriteBufferLoc10WriteBytesEPKvm.exit
	addq	88(%r13), %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	memcpy
	addq	%r14, 104(%r13)
.LBB7_11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_12:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$1, (%rax)
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end7:
	.size	_ZN8NArchive3N7z11COutArchive10WriteBytesEPKvm, .Lfunc_end7-_ZN8NArchive3N7z11COutArchive10WriteBytesEPKvm
	.cfi_endproc

	.globl	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive9WriteByteEh,@function
_ZN8NArchive3N7z11COutArchive9WriteByteEh: # @_ZN8NArchive3N7z11COutArchive9WriteByteEh
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi65:
	.cfi_def_cfa_offset 32
.Lcfi66:
	.cfi_offset %rbx, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	cmpb	$0, 8(%rbx)
	je	.LBB8_2
# BB#1:
	incq	16(%rbx)
	jmp	.LBB8_8
.LBB8_2:
	cmpb	$0, 9(%rbx)
	je	.LBB8_6
# BB#3:
	movq	32(%rbx), %rax
	movl	40(%rbx), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 40(%rbx)
	movb	%bpl, (%rax,%rcx)
	movl	40(%rbx), %eax
	cmpl	44(%rbx), %eax
	jne	.LBB8_5
# BB#4:
	leaq	32(%rbx), %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB8_5:                                # %_ZN10COutBuffer9WriteByteEh.exit
	movl	24(%rbx), %eax
	movzbl	%bpl, %ecx
	movzbl	%al, %edx
	xorl	%ecx, %edx
	shrl	$8, %eax
	xorl	g_CrcTable(,%rdx,4), %eax
	movl	%eax, 24(%rbx)
	jmp	.LBB8_8
.LBB8_6:
	movq	104(%rbx), %rax
	cmpq	%rax, 96(%rbx)
	je	.LBB8_9
# BB#7:                                 # %_ZN8NArchive3N7z15CWriteBufferLoc9WriteByteEh.exit
	movq	88(%rbx), %rcx
	leaq	1(%rax), %rdx
	movq	%rdx, 104(%rbx)
	movb	%bpl, (%rcx,%rax)
.LBB8_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB8_9:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$1, (%rax)
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end8:
	.size	_ZN8NArchive3N7z11COutArchive9WriteByteEh, .Lfunc_end8-_ZN8NArchive3N7z11COutArchive9WriteByteEh
	.cfi_endproc

	.globl	_ZN8NArchive3N7z11COutArchive11WriteUInt32Ej
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive11WriteUInt32Ej,@function
_ZN8NArchive3N7z11COutArchive11WriteUInt32Ej: # @_ZN8NArchive3N7z11COutArchive11WriteUInt32Ej
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi70:
	.cfi_def_cfa_offset 32
.Lcfi71:
	.cfi_offset %rbx, -24
.Lcfi72:
	.cfi_offset %r14, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	movzbl	%bl, %esi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movzbl	%bh, %esi  # NOREX
	movq	%r14, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	%ebx, %eax
	shrl	$16, %eax
	movzbl	%al, %esi
	movq	%r14, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	shrl	$24, %ebx
	movq	%r14, %rdi
	movl	%ebx, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN8NArchive3N7z11COutArchive9WriteByteEh # TAILCALL
.Lfunc_end9:
	.size	_ZN8NArchive3N7z11COutArchive11WriteUInt32Ej, .Lfunc_end9-_ZN8NArchive3N7z11COutArchive11WriteUInt32Ej
	.cfi_endproc

	.globl	_ZN8NArchive3N7z11COutArchive11WriteUInt64Ey
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive11WriteUInt64Ey,@function
_ZN8NArchive3N7z11COutArchive11WriteUInt64Ey: # @_ZN8NArchive3N7z11COutArchive11WriteUInt64Ey
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi75:
	.cfi_def_cfa_offset 32
.Lcfi76:
	.cfi_offset %rbx, -24
.Lcfi77:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movzbl	%bl, %esi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movzbl	%bh, %esi  # NOREX
	movq	%r14, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	%ebx, %eax
	shrl	$16, %eax
	movzbl	%al, %esi
	movq	%r14, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	%ebx, %esi
	shrl	$24, %esi
	movq	%r14, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movq	%rbx, %rax
	shrq	$32, %rax
	movzbl	%al, %esi
	movq	%r14, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movq	%rbx, %rax
	shrq	$40, %rax
	movzbl	%al, %esi
	movq	%r14, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movq	%rbx, %rax
	shrq	$48, %rax
	movzbl	%al, %esi
	movq	%r14, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	shrq	$56, %rbx
	movq	%r14, %rdi
	movl	%ebx, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN8NArchive3N7z11COutArchive9WriteByteEh # TAILCALL
.Lfunc_end10:
	.size	_ZN8NArchive3N7z11COutArchive11WriteUInt64Ey, .Lfunc_end10-_ZN8NArchive3N7z11COutArchive11WriteUInt64Ey
	.cfi_endproc

	.globl	_ZN8NArchive3N7z11COutArchive11WriteNumberEy
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive11WriteNumberEy,@function
_ZN8NArchive3N7z11COutArchive11WriteNumberEy: # @_ZN8NArchive3N7z11COutArchive11WriteNumberEy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 32
.Lcfi81:
	.cfi_offset %rbx, -32
.Lcfi82:
	.cfi_offset %r14, -24
.Lcfi83:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	xorl	%ebp, %ebp
	cmpq	$128, %rbx
	movl	$0, %eax
	jb	.LBB11_3
# BB#1:
	cmpq	$16383, %rbx            # imm = 0x3FFF
	ja	.LBB11_7
# BB#2:
	movb	$-128, %al
	movl	$1, %ebp
	jmp	.LBB11_3
.LBB11_7:
	cmpq	$2097152, %rbx          # imm = 0x200000
	jae	.LBB11_9
# BB#8:
	movb	$-64, %al
	movl	$2, %ebp
	jmp	.LBB11_3
.LBB11_9:
	cmpq	$268435456, %rbx        # imm = 0x10000000
	jae	.LBB11_11
# BB#10:
	movb	$-32, %al
	movl	$3, %ebp
	jmp	.LBB11_3
.LBB11_11:
	movq	%rbx, %rax
	shrq	$35, %rax
	jne	.LBB11_13
# BB#12:
	movb	$-16, %al
	movl	$4, %ebp
	jmp	.LBB11_3
.LBB11_13:
	movq	%rbx, %rax
	shrq	$42, %rax
	jne	.LBB11_15
# BB#14:
	movb	$-8, %al
	movl	$5, %ebp
	jmp	.LBB11_3
.LBB11_15:
	movq	%rbx, %rax
	shrq	$49, %rax
	jne	.LBB11_17
# BB#16:
	movb	$-4, %al
	movl	$6, %ebp
	jmp	.LBB11_3
.LBB11_17:
	movq	%rbx, %rax
	shrq	$56, %rax
	jne	.LBB11_19
# BB#18:
	movb	$-2, %al
	movl	$7, %ebp
.LBB11_3:                               # %.loopexit
	movl	%ebp, %ecx
	shlb	$3, %cl
	movq	%rbx, %rdx
	shrq	%cl, %rdx
	orb	%al, %dl
	movzbl	%dl, %esi
	movq	%r14, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	testl	%ebp, %ebp
	je	.LBB11_6
.LBB11_4:                               # %.lr.ph.preheader
	incl	%ebp
	.p2align	4, 0x90
.LBB11_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%bl, %esi
	movq	%r14, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	shrq	$8, %rbx
	decl	%ebp
	cmpl	$1, %ebp
	jg	.LBB11_5
.LBB11_6:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB11_19:                              # %.loopexit.thread
	movl	$255, %esi
	movq	%r14, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	$8, %ebp
	jmp	.LBB11_4
.Lfunc_end11:
	.size	_ZN8NArchive3N7z11COutArchive11WriteNumberEy, .Lfunc_end11-_ZN8NArchive3N7z11COutArchive11WriteNumberEy
	.cfi_endproc

	.globl	_ZN8NArchive3N7z11COutArchive11WriteFolderERKNS0_7CFolderE
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive11WriteFolderERKNS0_7CFolderE,@function
_ZN8NArchive3N7z11COutArchive11WriteFolderERKNS0_7CFolderE: # @_ZN8NArchive3N7z11COutArchive11WriteFolderERKNS0_7CFolderE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi87:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi88:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi90:
	.cfi_def_cfa_offset 96
.Lcfi91:
	.cfi_offset %rbx, -56
.Lcfi92:
	.cfi_offset %r12, -48
.Lcfi93:
	.cfi_offset %r13, -40
.Lcfi94:
	.cfi_offset %r14, -32
.Lcfi95:
	.cfi_offset %r15, -24
.Lcfi96:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r12
	movslq	12(%r13), %rbp
	xorl	%ebx, %ebx
	cmpq	$128, %rbp
	movl	$0, %eax
	jb	.LBB12_3
# BB#1:
	cmpl	$16383, %ebp            # imm = 0x3FFF
	ja	.LBB12_10
# BB#2:
	movb	$-128, %al
	movl	$1, %ebx
	jmp	.LBB12_3
.LBB12_10:
	cmpl	$2097152, %ebp          # imm = 0x200000
	jae	.LBB12_12
# BB#11:
	movb	$-64, %al
	movl	$2, %ebx
	jmp	.LBB12_3
.LBB12_12:
	cmpl	$268435456, %ebp        # imm = 0x10000000
	jae	.LBB12_14
# BB#13:
	movb	$-32, %al
	movl	$3, %ebx
	jmp	.LBB12_3
.LBB12_14:
	testl	%ebp, %ebp
	js	.LBB12_16
# BB#15:
	movb	$-16, %al
	movl	$4, %ebx
.LBB12_3:                               # %.loopexit.i68
	movl	%ebx, %ecx
	shlb	$3, %cl
	movq	%rbp, %rdx
	shrq	%cl, %rdx
	orb	%al, %dl
	movzbl	%dl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	testl	%ebx, %ebx
	je	.LBB12_6
.LBB12_4:                               # %.lr.ph.i71.preheader
	incl	%ebx
	.p2align	4, 0x90
.LBB12_5:                               # %.lr.ph.i71
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%bpl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	shrq	$8, %rbp
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB12_5
.LBB12_6:                               # %_ZN8NArchive3N7z11COutArchive11WriteNumberEy.exit72.preheader
	cmpl	$0, 12(%r13)
	jle	.LBB12_17
# BB#7:                                 # %.lr.ph130
	xorl	%r14d, %r14d
	movq	%r13, 16(%rsp)          # 8-byte Spill
	jmp	.LBB12_8
.LBB12_93:                              # %.loopexit.thread.i100
                                        #   in Loop: Header=BB12_8 Depth=1
	movl	$255, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	$8, %ebp
	jmp	.LBB12_67
	.p2align	4, 0x90
.LBB12_8:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_45 Depth 2
                                        #     Child Loop BB12_55 Depth 2
                                        #     Child Loop BB12_61 Depth 2
                                        #     Child Loop BB12_68 Depth 2
	movq	16(%r13), %rax
	movq	(%rax,%r14,8), %r15
	movq	(%r15), %rax
	movq	16(%r15), %rsi
	cmpq	$256, %rax              # imm = 0x100
	jae	.LBB12_42
# BB#9:                                 #   in Loop: Header=BB12_8 Depth=1
	movl	$1, %ecx
	movl	$1, %r13d
	jmp	.LBB12_44
	.p2align	4, 0x90
.LBB12_42:                              #   in Loop: Header=BB12_8 Depth=1
	cmpq	$65535, %rax            # imm = 0xFFFF
	ja	.LBB12_109
# BB#43:                                #   in Loop: Header=BB12_8 Depth=1
	movl	$2, %ecx
	movl	$2, %r13d
	jmp	.LBB12_44
	.p2align	4, 0x90
.LBB12_109:                             #   in Loop: Header=BB12_8 Depth=1
	cmpq	$16777216, %rax         # imm = 0x1000000
	jae	.LBB12_111
# BB#110:                               #   in Loop: Header=BB12_8 Depth=1
	movl	$3, %ecx
	movl	$3, %r13d
	jmp	.LBB12_44
.LBB12_111:                             #   in Loop: Header=BB12_8 Depth=1
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB12_113
# BB#112:                               #   in Loop: Header=BB12_8 Depth=1
	movl	$4, %ecx
	movl	$4, %r13d
	jmp	.LBB12_44
.LBB12_113:                             #   in Loop: Header=BB12_8 Depth=1
	movq	%rax, %rcx
	shrq	$40, %rcx
	jne	.LBB12_115
# BB#114:                               #   in Loop: Header=BB12_8 Depth=1
	movl	$5, %ecx
	movl	$5, %r13d
	jmp	.LBB12_44
.LBB12_115:                             #   in Loop: Header=BB12_8 Depth=1
	movq	%rax, %rcx
	shrq	$48, %rcx
	jne	.LBB12_117
# BB#116:                               #   in Loop: Header=BB12_8 Depth=1
	movl	$6, %ecx
	movl	$6, %r13d
	jmp	.LBB12_44
.LBB12_117:                             #   in Loop: Header=BB12_8 Depth=1
	xorl	%r13d, %r13d
	movabsq	$72057594037927936, %rcx # imm = 0x100000000000000
	cmpq	%rcx, %rax
	setae	%r13b
	leal	7(%r13), %ecx
	addq	$7, %r13
	.p2align	4, 0x90
.LBB12_44:                              # %.lr.ph127.preheader
                                        #   in Loop: Header=BB12_8 Depth=1
	movl	%ecx, %edx
	incq	%rdx
	.p2align	4, 0x90
.LBB12_45:                              # %.lr.ph127
                                        #   Parent Loop BB12_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	%al, 23(%rsp,%rdx)
	shrq	$8, %rax
	decq	%rdx
	cmpq	$1, %rdx
	jg	.LBB12_45
# BB#46:                                # %._crit_edge128
                                        #   in Loop: Header=BB12_8 Depth=1
	cmpl	$1, 32(%r15)
	jne	.LBB12_47
# BB#48:                                #   in Loop: Header=BB12_8 Depth=1
	cmpl	$1, 36(%r15)
	sete	%bl
	jmp	.LBB12_49
	.p2align	4, 0x90
.LBB12_47:                              #   in Loop: Header=BB12_8 Depth=1
	xorl	%ebx, %ebx
.LBB12_49:                              # %_ZNK8NArchive3N7z10CCoderInfo13IsSimpleCoderEv.exit
                                        #   in Loop: Header=BB12_8 Depth=1
	testb	%bl, %bl
	sete	%al
	shlb	$4, %al
	movzbl	%al, %eax
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	testq	%rsi, %rsi
	setne	%dl
	shlb	$5, %dl
	movzbl	%dl, %edx
	orl	%edx, %ecx
	orl	%eax, %ecx
	movzbl	%cl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movq	%r12, %rdi
	leaq	25(%rsp), %rsi
	movq	%r13, %rdx
	callq	_ZN8NArchive3N7z11COutArchive10WriteBytesEPKvm
	testb	%bl, %bl
	movq	16(%rsp), %r13          # 8-byte Reload
	jne	.LBB12_62
# BB#50:                                #   in Loop: Header=BB12_8 Depth=1
	movl	32(%r15), %ebp
	xorl	%ebx, %ebx
	cmpq	$128, %rbp
	movl	$0, %eax
	jb	.LBB12_53
# BB#51:                                #   in Loop: Header=BB12_8 Depth=1
	cmpl	$16383, %ebp            # imm = 0x3FFF
	ja	.LBB12_71
# BB#52:                                #   in Loop: Header=BB12_8 Depth=1
	movb	$-128, %al
	movl	$1, %ebx
	jmp	.LBB12_53
.LBB12_71:                              #   in Loop: Header=BB12_8 Depth=1
	cmpl	$2097152, %ebp          # imm = 0x200000
	jae	.LBB12_73
# BB#72:                                #   in Loop: Header=BB12_8 Depth=1
	movb	$-64, %al
	movl	$2, %ebx
	jmp	.LBB12_53
.LBB12_73:                              #   in Loop: Header=BB12_8 Depth=1
	xorl	%ebx, %ebx
	cmpl	$268435455, %ebp        # imm = 0xFFFFFFF
	seta	%bl
	cmpl	$268435456, %ebp        # imm = 0x10000000
	movb	$-32, %al
	jb	.LBB12_75
# BB#74:                                #   in Loop: Header=BB12_8 Depth=1
	movb	$-16, %al
.LBB12_75:                              #   in Loop: Header=BB12_8 Depth=1
	addl	$3, %ebx
	.p2align	4, 0x90
.LBB12_53:                              # %.loopexit.i82
                                        #   in Loop: Header=BB12_8 Depth=1
	movl	%ebx, %ecx
	shlb	$3, %cl
	movq	%rbp, %rdx
	shrq	%cl, %rdx
	orb	%al, %dl
	movzbl	%dl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	testl	%ebx, %ebx
	je	.LBB12_56
# BB#54:                                # %.lr.ph.i85.preheader
                                        #   in Loop: Header=BB12_8 Depth=1
	incl	%ebx
	.p2align	4, 0x90
.LBB12_55:                              # %.lr.ph.i85
                                        #   Parent Loop BB12_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%bpl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	shrq	$8, %rbp
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB12_55
.LBB12_56:                              # %_ZN8NArchive3N7z11COutArchive11WriteNumberEy.exit86
                                        #   in Loop: Header=BB12_8 Depth=1
	movl	36(%r15), %ebp
	xorl	%ebx, %ebx
	cmpq	$128, %rbp
	movl	$0, %eax
	jb	.LBB12_59
# BB#57:                                #   in Loop: Header=BB12_8 Depth=1
	cmpl	$16383, %ebp            # imm = 0x3FFF
	ja	.LBB12_76
# BB#58:                                #   in Loop: Header=BB12_8 Depth=1
	movb	$-128, %al
	movl	$1, %ebx
	jmp	.LBB12_59
.LBB12_76:                              #   in Loop: Header=BB12_8 Depth=1
	cmpl	$2097152, %ebp          # imm = 0x200000
	jae	.LBB12_78
# BB#77:                                #   in Loop: Header=BB12_8 Depth=1
	movb	$-64, %al
	movl	$2, %ebx
	jmp	.LBB12_59
.LBB12_78:                              #   in Loop: Header=BB12_8 Depth=1
	xorl	%ebx, %ebx
	cmpl	$268435455, %ebp        # imm = 0xFFFFFFF
	seta	%bl
	cmpl	$268435456, %ebp        # imm = 0x10000000
	movb	$-32, %al
	jb	.LBB12_80
# BB#79:                                #   in Loop: Header=BB12_8 Depth=1
	movb	$-16, %al
.LBB12_80:                              #   in Loop: Header=BB12_8 Depth=1
	addl	$3, %ebx
	.p2align	4, 0x90
.LBB12_59:                              # %.loopexit.i89
                                        #   in Loop: Header=BB12_8 Depth=1
	movl	%ebx, %ecx
	shlb	$3, %cl
	movq	%rbp, %rdx
	shrq	%cl, %rdx
	orb	%al, %dl
	movzbl	%dl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	testl	%ebx, %ebx
	je	.LBB12_62
# BB#60:                                # %.lr.ph.i92.preheader
                                        #   in Loop: Header=BB12_8 Depth=1
	incl	%ebx
	.p2align	4, 0x90
.LBB12_61:                              # %.lr.ph.i92
                                        #   Parent Loop BB12_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%bpl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	shrq	$8, %rbp
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB12_61
.LBB12_62:                              # %_ZN8NArchive3N7z11COutArchive11WriteNumberEy.exit93
                                        #   in Loop: Header=BB12_8 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	testq	%rdx, %rdx
	je	.LBB12_70
# BB#63:                                # %.critedge
                                        #   in Loop: Header=BB12_8 Depth=1
	xorl	%ebp, %ebp
	cmpq	$128, %rdx
	movl	$0, %eax
	jb	.LBB12_66
# BB#64:                                #   in Loop: Header=BB12_8 Depth=1
	cmpq	$16383, %rdx            # imm = 0x3FFF
	ja	.LBB12_81
# BB#65:                                #   in Loop: Header=BB12_8 Depth=1
	movb	$-128, %al
	movl	$1, %ebp
	jmp	.LBB12_66
.LBB12_81:                              #   in Loop: Header=BB12_8 Depth=1
	cmpq	$2097152, %rdx          # imm = 0x200000
	jae	.LBB12_83
# BB#82:                                #   in Loop: Header=BB12_8 Depth=1
	movb	$-64, %al
	movl	$2, %ebp
	jmp	.LBB12_66
.LBB12_83:                              #   in Loop: Header=BB12_8 Depth=1
	cmpq	$268435456, %rdx        # imm = 0x10000000
	jae	.LBB12_85
# BB#84:                                #   in Loop: Header=BB12_8 Depth=1
	movb	$-32, %al
	movl	$3, %ebp
	jmp	.LBB12_66
.LBB12_85:                              #   in Loop: Header=BB12_8 Depth=1
	movq	%rdx, %rax
	shrq	$35, %rax
	jne	.LBB12_87
# BB#86:                                #   in Loop: Header=BB12_8 Depth=1
	movb	$-16, %al
	movl	$4, %ebp
	jmp	.LBB12_66
.LBB12_87:                              #   in Loop: Header=BB12_8 Depth=1
	movq	%rdx, %rax
	shrq	$42, %rax
	jne	.LBB12_89
# BB#88:                                #   in Loop: Header=BB12_8 Depth=1
	movb	$-8, %al
	movl	$5, %ebp
	jmp	.LBB12_66
.LBB12_89:                              #   in Loop: Header=BB12_8 Depth=1
	movq	%rdx, %rax
	shrq	$49, %rax
	jne	.LBB12_91
# BB#90:                                #   in Loop: Header=BB12_8 Depth=1
	movb	$-4, %al
	movl	$6, %ebp
	jmp	.LBB12_66
.LBB12_91:                              #   in Loop: Header=BB12_8 Depth=1
	movabsq	$72057594037927936, %rax # imm = 0x100000000000000
	cmpq	%rax, %rdx
	jae	.LBB12_93
# BB#92:                                #   in Loop: Header=BB12_8 Depth=1
	movb	$-2, %al
	movl	$7, %ebp
	.p2align	4, 0x90
.LBB12_66:                              # %.loopexit.i96
                                        #   in Loop: Header=BB12_8 Depth=1
	movl	%ebp, %ecx
	shlb	$3, %cl
	shrq	%cl, %rdx
	orb	%al, %dl
	movzbl	%dl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	testl	%ebp, %ebp
	je	.LBB12_69
.LBB12_67:                              # %.lr.ph.i99.preheader
                                        #   in Loop: Header=BB12_8 Depth=1
	incl	%ebp
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB12_68:                              # %.lr.ph.i99
                                        #   Parent Loop BB12_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%bl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	shrq	$8, %rbx
	decl	%ebp
	cmpl	$1, %ebp
	jg	.LBB12_68
.LBB12_69:                              # %_ZN8NArchive3N7z11COutArchive11WriteNumberEy.exit101
                                        #   in Loop: Header=BB12_8 Depth=1
	movq	24(%r15), %rsi
	movq	%r12, %rdi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	_ZN8NArchive3N7z11COutArchive10WriteBytesEPKvm
.LBB12_70:                              # %_ZN8NArchive3N7z11COutArchive11WriteNumberEy.exit72
                                        #   in Loop: Header=BB12_8 Depth=1
	incq	%r14
	movslq	12(%r13), %rax
	cmpq	%rax, %r14
	jl	.LBB12_8
.LBB12_17:                              # %.preheader118
	cmpl	$0, 44(%r13)
	jle	.LBB12_32
# BB#18:                                # %.lr.ph121
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB12_19:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_24 Depth 2
                                        #     Child Loop BB12_30 Depth 2
	movq	48(%r13), %r15
	movl	(%r15,%r14,8), %ebp
	cmpq	$128, %rbp
	movl	$0, %ebx
	movl	$0, %eax
	jb	.LBB12_22
# BB#20:                                #   in Loop: Header=BB12_19 Depth=1
	cmpl	$16383, %ebp            # imm = 0x3FFF
	ja	.LBB12_94
# BB#21:                                #   in Loop: Header=BB12_19 Depth=1
	movb	$-128, %al
	movl	$1, %ebx
	jmp	.LBB12_22
	.p2align	4, 0x90
.LBB12_94:                              #   in Loop: Header=BB12_19 Depth=1
	cmpl	$2097152, %ebp          # imm = 0x200000
	jae	.LBB12_96
# BB#95:                                #   in Loop: Header=BB12_19 Depth=1
	movb	$-64, %al
	movl	$2, %ebx
	jmp	.LBB12_22
.LBB12_96:                              #   in Loop: Header=BB12_19 Depth=1
	xorl	%ebx, %ebx
	cmpl	$268435455, %ebp        # imm = 0xFFFFFFF
	seta	%bl
	cmpl	$268435456, %ebp        # imm = 0x10000000
	movb	$-32, %al
	jb	.LBB12_98
# BB#97:                                #   in Loop: Header=BB12_19 Depth=1
	movb	$-16, %al
.LBB12_98:                              #   in Loop: Header=BB12_19 Depth=1
	addl	$3, %ebx
	.p2align	4, 0x90
.LBB12_22:                              # %.loopexit.i104
                                        #   in Loop: Header=BB12_19 Depth=1
	movl	%ebx, %ecx
	shlb	$3, %cl
	movq	%rbp, %rdx
	shrq	%cl, %rdx
	orb	%al, %dl
	movzbl	%dl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	testl	%ebx, %ebx
	je	.LBB12_25
# BB#23:                                # %.lr.ph.i107.preheader
                                        #   in Loop: Header=BB12_19 Depth=1
	incl	%ebx
	.p2align	4, 0x90
.LBB12_24:                              # %.lr.ph.i107
                                        #   Parent Loop BB12_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%bpl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	shrq	$8, %rbp
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB12_24
.LBB12_25:                              # %_ZN8NArchive3N7z11COutArchive11WriteNumberEy.exit108
                                        #   in Loop: Header=BB12_19 Depth=1
	movl	4(%r15,%r14,8), %ebp
	xorl	%ebx, %ebx
	cmpq	$128, %rbp
	movl	$0, %eax
	jb	.LBB12_28
# BB#26:                                #   in Loop: Header=BB12_19 Depth=1
	cmpl	$16383, %ebp            # imm = 0x3FFF
	ja	.LBB12_99
# BB#27:                                #   in Loop: Header=BB12_19 Depth=1
	movb	$-128, %al
	movl	$1, %ebx
	jmp	.LBB12_28
	.p2align	4, 0x90
.LBB12_99:                              #   in Loop: Header=BB12_19 Depth=1
	cmpl	$2097152, %ebp          # imm = 0x200000
	jae	.LBB12_101
# BB#100:                               #   in Loop: Header=BB12_19 Depth=1
	movb	$-64, %al
	movl	$2, %ebx
	jmp	.LBB12_28
.LBB12_101:                             #   in Loop: Header=BB12_19 Depth=1
	xorl	%ebx, %ebx
	cmpl	$268435455, %ebp        # imm = 0xFFFFFFF
	seta	%bl
	cmpl	$268435456, %ebp        # imm = 0x10000000
	movb	$-32, %al
	jb	.LBB12_103
# BB#102:                               #   in Loop: Header=BB12_19 Depth=1
	movb	$-16, %al
.LBB12_103:                             #   in Loop: Header=BB12_19 Depth=1
	addl	$3, %ebx
	.p2align	4, 0x90
.LBB12_28:                              # %.loopexit.i75
                                        #   in Loop: Header=BB12_19 Depth=1
	movl	%ebx, %ecx
	shlb	$3, %cl
	movq	%rbp, %rdx
	shrq	%cl, %rdx
	orb	%al, %dl
	movzbl	%dl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	testl	%ebx, %ebx
	je	.LBB12_31
# BB#29:                                # %.lr.ph.i78.preheader
                                        #   in Loop: Header=BB12_19 Depth=1
	incl	%ebx
	.p2align	4, 0x90
.LBB12_30:                              # %.lr.ph.i78
                                        #   Parent Loop BB12_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%bpl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	shrq	$8, %rbp
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB12_30
.LBB12_31:                              # %_ZN8NArchive3N7z11COutArchive11WriteNumberEy.exit79
                                        #   in Loop: Header=BB12_19 Depth=1
	incq	%r14
	movslq	44(%r13), %rax
	cmpq	%rax, %r14
	jl	.LBB12_19
.LBB12_32:                              # %._crit_edge
	cmpl	$2, 76(%r13)
	jl	.LBB12_41
# BB#33:                                # %.lr.ph
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB12_34:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_39 Depth 2
	movq	80(%r13), %rax
	movl	(%rax,%r14,4), %ebp
	cmpq	$128, %rbp
	movl	$0, %ebx
	movl	$0, %eax
	jb	.LBB12_37
# BB#35:                                #   in Loop: Header=BB12_34 Depth=1
	cmpl	$16383, %ebp            # imm = 0x3FFF
	ja	.LBB12_104
# BB#36:                                #   in Loop: Header=BB12_34 Depth=1
	movb	$-128, %al
	movl	$1, %ebx
	jmp	.LBB12_37
	.p2align	4, 0x90
.LBB12_104:                             #   in Loop: Header=BB12_34 Depth=1
	cmpl	$2097152, %ebp          # imm = 0x200000
	jae	.LBB12_106
# BB#105:                               #   in Loop: Header=BB12_34 Depth=1
	movb	$-64, %al
	movl	$2, %ebx
	jmp	.LBB12_37
.LBB12_106:                             #   in Loop: Header=BB12_34 Depth=1
	xorl	%ebx, %ebx
	cmpl	$268435455, %ebp        # imm = 0xFFFFFFF
	seta	%bl
	cmpl	$268435456, %ebp        # imm = 0x10000000
	movb	$-32, %al
	jb	.LBB12_108
# BB#107:                               #   in Loop: Header=BB12_34 Depth=1
	movb	$-16, %al
.LBB12_108:                             #   in Loop: Header=BB12_34 Depth=1
	addl	$3, %ebx
	.p2align	4, 0x90
.LBB12_37:                              # %.loopexit.i
                                        #   in Loop: Header=BB12_34 Depth=1
	movl	%ebx, %ecx
	shlb	$3, %cl
	movq	%rbp, %rdx
	shrq	%cl, %rdx
	orb	%al, %dl
	movzbl	%dl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	testl	%ebx, %ebx
	je	.LBB12_40
# BB#38:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB12_34 Depth=1
	incl	%ebx
	.p2align	4, 0x90
.LBB12_39:                              # %.lr.ph.i
                                        #   Parent Loop BB12_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%bpl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	shrq	$8, %rbp
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB12_39
.LBB12_40:                              # %_ZN8NArchive3N7z11COutArchive11WriteNumberEy.exit
                                        #   in Loop: Header=BB12_34 Depth=1
	incq	%r14
	movslq	76(%r13), %rax
	cmpq	%rax, %r14
	jl	.LBB12_34
.LBB12_41:                              # %.loopexit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_16:                              # %.loopexit.thread.i
	movl	$255, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	$8, %ebx
	jmp	.LBB12_4
.Lfunc_end12:
	.size	_ZN8NArchive3N7z11COutArchive11WriteFolderERKNS0_7CFolderE, .Lfunc_end12-_ZN8NArchive3N7z11COutArchive11WriteFolderERKNS0_7CFolderE
	.cfi_endproc

	.globl	_ZN8NArchive3N7z11COutArchive15WriteBoolVectorERK13CRecordVectorIbE
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive15WriteBoolVectorERK13CRecordVectorIbE,@function
_ZN8NArchive3N7z11COutArchive15WriteBoolVectorERK13CRecordVectorIbE: # @_ZN8NArchive3N7z11COutArchive15WriteBoolVectorERK13CRecordVectorIbE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 32
.Lcfi100:
	.cfi_offset %rbx, -32
.Lcfi101:
	.cfi_offset %r14, -24
.Lcfi102:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	12(%r15), %esi
	testl	%esi, %esi
	jle	.LBB13_8
# BB#1:                                 # %.lr.ph
	movb	$-128, %al
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB13_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rcx
	movzbl	(%rcx,%rbx), %ecx
	testb	%cl, %cl
	je	.LBB13_4
# BB#3:                                 #   in Loop: Header=BB13_2 Depth=1
	movl	%eax, %ecx
.LBB13_4:                               #   in Loop: Header=BB13_2 Depth=1
	orb	%cl, %dl
	shrb	%al
	jne	.LBB13_6
# BB#5:                                 #   in Loop: Header=BB13_2 Depth=1
	movzbl	%dl, %esi
	movq	%r14, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	12(%r15), %esi
	movb	$-128, %al
	xorl	%edx, %edx
.LBB13_6:                               #   in Loop: Header=BB13_2 Depth=1
	incq	%rbx
	movslq	%esi, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB13_2
# BB#7:                                 # %._crit_edge
	cmpb	$-128, %al
	jne	.LBB13_9
.LBB13_8:                               # %._crit_edge.thread
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB13_9:
	movzbl	%dl, %esi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN8NArchive3N7z11COutArchive9WriteByteEh # TAILCALL
.Lfunc_end13:
	.size	_ZN8NArchive3N7z11COutArchive15WriteBoolVectorERK13CRecordVectorIbE, .Lfunc_end13-_ZN8NArchive3N7z11COutArchive15WriteBoolVectorERK13CRecordVectorIbE
	.cfi_endproc

	.globl	_ZN8NArchive3N7z11COutArchive16WriteHashDigestsERK13CRecordVectorIbERKS2_IjE
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive16WriteHashDigestsERK13CRecordVectorIbERKS2_IjE,@function
_ZN8NArchive3N7z11COutArchive16WriteHashDigestsERK13CRecordVectorIbERKS2_IjE: # @_ZN8NArchive3N7z11COutArchive16WriteHashDigestsERK13CRecordVectorIbERKS2_IjE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi103:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi104:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi106:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi107:
	.cfi_def_cfa_offset 48
.Lcfi108:
	.cfi_offset %rbx, -48
.Lcfi109:
	.cfi_offset %r12, -40
.Lcfi110:
	.cfi_offset %r14, -32
.Lcfi111:
	.cfi_offset %r15, -24
.Lcfi112:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	movslq	12(%r15), %rax
	testq	%rax, %rax
	jle	.LBB14_30
# BB#1:                                 # %.lr.ph25
	movq	16(%r15), %rcx
	cmpl	$8, %eax
	jb	.LBB14_2
# BB#3:                                 # %min.iters.checked
	movq	%rax, %rdx
	andq	$-8, %rdx
	je	.LBB14_2
# BB#4:                                 # %vector.body.preheader
	leaq	-8(%rdx), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	btl	$3, %edi
	jb	.LBB14_5
# BB#6:                                 # %vector.body.prol
	movd	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movd	4(%rcx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	pxor	%xmm2, %xmm2
	punpcklbw	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3],xmm0[4],xmm2[4],xmm0[5],xmm2[5],xmm0[6],xmm2[6],xmm0[7],xmm2[7]
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	punpcklbw	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3],xmm1[4],xmm2[4],xmm1[5],xmm2[5],xmm1[6],xmm2[6],xmm1[7],xmm2[7]
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	movl	$8, %edi
	testq	%rsi, %rsi
	jne	.LBB14_8
	jmp	.LBB14_10
.LBB14_2:
	xorl	%edx, %edx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB14_11:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx,%rdx), %esi
	addl	%esi, %ebx
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB14_11
.LBB14_12:                              # %._crit_edge
	testl	%ebx, %ebx
	je	.LBB14_30
# BB#13:
	movl	$10, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	cmpl	12(%r15), %ebx
	jne	.LBB14_15
# BB#14:
	movl	$1, %esi
	jmp	.LBB14_24
.LBB14_15:
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	12(%r15), %esi
	testl	%esi, %esi
	jle	.LBB14_25
# BB#16:                                # %.lr.ph.i
	movb	$-128, %al
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB14_17:                              # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rcx
	movzbl	(%rcx,%rbx), %ecx
	testb	%cl, %cl
	je	.LBB14_19
# BB#18:                                #   in Loop: Header=BB14_17 Depth=1
	movl	%eax, %ecx
.LBB14_19:                              #   in Loop: Header=BB14_17 Depth=1
	orb	%cl, %dl
	shrb	%al
	jne	.LBB14_21
# BB#20:                                #   in Loop: Header=BB14_17 Depth=1
	movzbl	%dl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	12(%r15), %esi
	movb	$-128, %al
	xorl	%edx, %edx
.LBB14_21:                              #   in Loop: Header=BB14_17 Depth=1
	incq	%rbx
	movslq	%esi, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB14_17
# BB#22:                                # %._crit_edge.i
	cmpb	$-128, %al
	je	.LBB14_25
# BB#23:
	movzbl	%dl, %esi
.LBB14_24:                              # %_ZN8NArchive3N7z11COutArchive15WriteBoolVectorERK13CRecordVectorIbE.exit.preheader
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.LBB14_25:                              # %_ZN8NArchive3N7z11COutArchive15WriteBoolVectorERK13CRecordVectorIbE.exit.preheader
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.LBB14_30
# BB#26:                                # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB14_27:                              # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rcx
	cmpb	$0, (%rcx,%rbx)
	je	.LBB14_29
# BB#28:                                #   in Loop: Header=BB14_27 Depth=1
	movq	16(%r14), %rax
	movl	(%rax,%rbx,4), %eax
	movzbl	%al, %esi
	movl	%eax, %ebp
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	%ebp, %eax
	movzbl	%ah, %esi  # NOREX
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	%ebp, %eax
	shrl	$16, %eax
	movzbl	%al, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	%ebp, %esi
	shrl	$24, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	12(%r14), %eax
.LBB14_29:                              # %_ZN8NArchive3N7z11COutArchive15WriteBoolVectorERK13CRecordVectorIbE.exit
                                        #   in Loop: Header=BB14_27 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB14_27
.LBB14_30:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_5:
	pxor	%xmm0, %xmm0
	xorl	%edi, %edi
	pxor	%xmm1, %xmm1
	testq	%rsi, %rsi
	je	.LBB14_10
.LBB14_8:                               # %vector.body.preheader.new
	movq	%rdx, %rsi
	subq	%rdi, %rsi
	leaq	12(%rcx,%rdi), %rdi
	pxor	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB14_9:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	-12(%rdi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movd	-8(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1],xmm3[2],xmm2[2],xmm3[3],xmm2[3],xmm3[4],xmm2[4],xmm3[5],xmm2[5],xmm3[6],xmm2[6],xmm3[7],xmm2[7]
	punpcklwd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1],xmm3[2],xmm2[2],xmm3[3],xmm2[3]
	punpcklbw	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3],xmm4[4],xmm2[4],xmm4[5],xmm2[5],xmm4[6],xmm2[6],xmm4[7],xmm2[7]
	punpcklwd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3]
	paddd	%xmm0, %xmm3
	paddd	%xmm1, %xmm4
	movd	-4(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movd	(%rdi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3],xmm0[4],xmm2[4],xmm0[5],xmm2[5],xmm0[6],xmm2[6],xmm0[7],xmm2[7]
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	punpcklbw	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3],xmm1[4],xmm2[4],xmm1[5],xmm2[5],xmm1[6],xmm2[6],xmm1[7],xmm2[7]
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	paddd	%xmm3, %xmm0
	paddd	%xmm4, %xmm1
	addq	$16, %rdi
	addq	$-16, %rsi
	jne	.LBB14_9
.LBB14_10:                              # %middle.block
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %ebx
	cmpq	%rdx, %rax
	jne	.LBB14_11
	jmp	.LBB14_12
.Lfunc_end14:
	.size	_ZN8NArchive3N7z11COutArchive16WriteHashDigestsERK13CRecordVectorIbERKS2_IjE, .Lfunc_end14-_ZN8NArchive3N7z11COutArchive16WriteHashDigestsERK13CRecordVectorIbERKS2_IjE
	.cfi_endproc

	.globl	_ZN8NArchive3N7z11COutArchive13WritePackInfoEyRK13CRecordVectorIyERKS2_IbERKS2_IjE
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive13WritePackInfoEyRK13CRecordVectorIyERKS2_IbERKS2_IjE,@function
_ZN8NArchive3N7z11COutArchive13WritePackInfoEyRK13CRecordVectorIyERKS2_IbERKS2_IjE: # @_ZN8NArchive3N7z11COutArchive13WritePackInfoEyRK13CRecordVectorIyERKS2_IbERKS2_IjE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi113:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi114:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi116:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi117:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi118:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi119:
	.cfi_def_cfa_offset 80
.Lcfi120:
	.cfi_offset %rbx, -56
.Lcfi121:
	.cfi_offset %r12, -48
.Lcfi122:
	.cfi_offset %r13, -40
.Lcfi123:
	.cfi_offset %r14, -32
.Lcfi124:
	.cfi_offset %r15, -24
.Lcfi125:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %r15
	cmpl	$0, 12(%r12)
	je	.LBB15_56
# BB#1:
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movabsq	$34359738368, %r14      # imm = 0x800000000
	movl	$6, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	xorl	%ebp, %ebp
	cmpq	$128, %r13
	movl	$0, %eax
	jb	.LBB15_4
# BB#2:
	cmpq	$16383, %r13            # imm = 0x3FFF
	ja	.LBB15_23
# BB#3:
	movb	$-128, %al
	movl	$1, %ebp
	jmp	.LBB15_4
.LBB15_56:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_23:
	cmpq	$2097152, %r13          # imm = 0x200000
	jae	.LBB15_25
# BB#24:
	movb	$-64, %al
	movl	$2, %ebp
	jmp	.LBB15_4
.LBB15_25:
	cmpq	$268435456, %r13        # imm = 0x10000000
	jae	.LBB15_27
# BB#26:
	movb	$-32, %al
	movl	$3, %ebp
	jmp	.LBB15_4
.LBB15_27:
	cmpq	%r14, %r13
	jae	.LBB15_29
# BB#28:
	movb	$-16, %al
	movl	$4, %ebp
	jmp	.LBB15_4
.LBB15_29:
	movabsq	$4398046511104, %rax    # imm = 0x40000000000
	cmpq	%rax, %r13
	jae	.LBB15_31
# BB#30:
	movb	$-8, %al
	movl	$5, %ebp
	jmp	.LBB15_4
.LBB15_31:
	movabsq	$562949953421312, %rax  # imm = 0x2000000000000
	cmpq	%rax, %r13
	jae	.LBB15_33
# BB#32:
	movb	$-4, %al
	movl	$6, %ebp
	jmp	.LBB15_4
.LBB15_33:
	movabsq	$72057594037927936, %rax # imm = 0x100000000000000
	cmpq	%rax, %r13
	jae	.LBB15_35
# BB#34:
	movb	$-2, %al
	movl	$7, %ebp
.LBB15_4:                               # %.loopexit.i13
	movl	%ebp, %ecx
	shlb	$3, %cl
	movq	%r13, %rdx
	shrq	%cl, %rdx
	orb	%al, %dl
	movzbl	%dl, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	testl	%ebp, %ebp
	je	.LBB15_7
.LBB15_5:                               # %.lr.ph.i16.preheader
	incl	%ebp
	.p2align	4, 0x90
.LBB15_6:                               # %.lr.ph.i16
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%r13b, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	shrq	$8, %r13
	decl	%ebp
	cmpl	$1, %ebp
	jg	.LBB15_6
.LBB15_7:                               # %_ZN8NArchive3N7z11COutArchive11WriteNumberEy.exit18
	movslq	12(%r12), %rbp
	xorl	%ebx, %ebx
	cmpq	$128, %rbp
	movl	$0, %eax
	jb	.LBB15_10
# BB#8:
	cmpl	$16383, %ebp            # imm = 0x3FFF
	ja	.LBB15_36
# BB#9:
	movb	$-128, %al
	movl	$1, %ebx
	jmp	.LBB15_10
.LBB15_36:
	cmpl	$2097152, %ebp          # imm = 0x200000
	jae	.LBB15_38
# BB#37:
	movb	$-64, %al
	movl	$2, %ebx
	jmp	.LBB15_10
.LBB15_38:
	cmpl	$268435456, %ebp        # imm = 0x10000000
	jae	.LBB15_40
# BB#39:
	movb	$-32, %al
	movl	$3, %ebx
	jmp	.LBB15_10
.LBB15_40:
	testl	%ebp, %ebp
	js	.LBB15_42
# BB#41:
	movb	$-16, %al
	movl	$4, %ebx
.LBB15_10:                              # %.loopexit.i21
	movl	%ebx, %ecx
	shlb	$3, %cl
	movq	%rbp, %rdx
	shrq	%cl, %rdx
	orb	%al, %dl
	movzbl	%dl, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	testl	%ebx, %ebx
	je	.LBB15_13
.LBB15_11:                              # %.lr.ph.i24.preheader
	incl	%ebx
	.p2align	4, 0x90
.LBB15_12:                              # %.lr.ph.i24
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%bpl, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	shrq	$8, %rbp
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB15_12
.LBB15_13:                              # %_ZN8NArchive3N7z11COutArchive11WriteNumberEy.exit26
	movl	$9, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	cmpl	$0, 12(%r12)
	movq	%r14, %rbx
	jle	.LBB15_22
# BB#14:                                # %.lr.ph
	xorl	%r13d, %r13d
	jmp	.LBB15_15
.LBB15_55:                              # %.loopexit.thread.i
                                        #   in Loop: Header=BB15_15 Depth=1
	movl	$255, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	$8, %r14d
	jmp	.LBB15_19
	.p2align	4, 0x90
.LBB15_15:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_20 Depth 2
	movq	16(%r12), %rax
	movq	(%rax,%r13,8), %rbp
	cmpq	$128, %rbp
	movl	$0, %r14d
	movl	$0, %eax
	jb	.LBB15_18
# BB#16:                                #   in Loop: Header=BB15_15 Depth=1
	cmpq	$16383, %rbp            # imm = 0x3FFF
	ja	.LBB15_43
# BB#17:                                #   in Loop: Header=BB15_15 Depth=1
	movb	$-128, %al
	movl	$1, %r14d
	jmp	.LBB15_18
	.p2align	4, 0x90
.LBB15_43:                              #   in Loop: Header=BB15_15 Depth=1
	cmpq	$2097152, %rbp          # imm = 0x200000
	jae	.LBB15_45
# BB#44:                                #   in Loop: Header=BB15_15 Depth=1
	movb	$-64, %al
	movl	$2, %r14d
	jmp	.LBB15_18
.LBB15_45:                              #   in Loop: Header=BB15_15 Depth=1
	cmpq	$268435456, %rbp        # imm = 0x10000000
	jae	.LBB15_47
# BB#46:                                #   in Loop: Header=BB15_15 Depth=1
	movb	$-32, %al
	movl	$3, %r14d
	jmp	.LBB15_18
.LBB15_47:                              #   in Loop: Header=BB15_15 Depth=1
	cmpq	%rbx, %rbp
	jae	.LBB15_49
# BB#48:                                #   in Loop: Header=BB15_15 Depth=1
	movb	$-16, %al
	movl	$4, %r14d
	jmp	.LBB15_18
.LBB15_49:                              #   in Loop: Header=BB15_15 Depth=1
	movabsq	$4398046511104, %rax    # imm = 0x40000000000
	cmpq	%rax, %rbp
	jae	.LBB15_51
# BB#50:                                #   in Loop: Header=BB15_15 Depth=1
	movb	$-8, %al
	movl	$5, %r14d
	jmp	.LBB15_18
.LBB15_51:                              #   in Loop: Header=BB15_15 Depth=1
	movabsq	$562949953421312, %rax  # imm = 0x2000000000000
	cmpq	%rax, %rbp
	jae	.LBB15_53
# BB#52:                                #   in Loop: Header=BB15_15 Depth=1
	movb	$-4, %al
	movl	$6, %r14d
	jmp	.LBB15_18
.LBB15_53:                              #   in Loop: Header=BB15_15 Depth=1
	movabsq	$72057594037927936, %rax # imm = 0x100000000000000
	cmpq	%rax, %rbp
	jae	.LBB15_55
# BB#54:                                #   in Loop: Header=BB15_15 Depth=1
	movb	$-2, %al
	movl	$7, %r14d
	.p2align	4, 0x90
.LBB15_18:                              # %.loopexit.i
                                        #   in Loop: Header=BB15_15 Depth=1
	movl	%r14d, %ecx
	shlb	$3, %cl
	movq	%rbp, %rdx
	shrq	%cl, %rdx
	orb	%al, %dl
	movzbl	%dl, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	testl	%r14d, %r14d
	je	.LBB15_21
.LBB15_19:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB15_15 Depth=1
	incl	%r14d
	.p2align	4, 0x90
.LBB15_20:                              # %.lr.ph.i
                                        #   Parent Loop BB15_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%bpl, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	shrq	$8, %rbp
	decl	%r14d
	cmpl	$1, %r14d
	jg	.LBB15_20
.LBB15_21:                              # %_ZN8NArchive3N7z11COutArchive11WriteNumberEy.exit
                                        #   in Loop: Header=BB15_15 Depth=1
	incq	%r13
	movslq	12(%r12), %rax
	cmpq	%rax, %r13
	jl	.LBB15_15
.LBB15_22:                              # %._crit_edge
	movq	%r15, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	_ZN8NArchive3N7z11COutArchive16WriteHashDigestsERK13CRecordVectorIbERKS2_IjE
	xorl	%esi, %esi
	movq	%r15, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN8NArchive3N7z11COutArchive9WriteByteEh # TAILCALL
.LBB15_42:                              # %.loopexit.thread.i25
	movl	$255, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	$8, %ebx
	jmp	.LBB15_11
.LBB15_35:                              # %.loopexit.thread.i17
	movl	$255, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	$8, %ebp
	jmp	.LBB15_5
.Lfunc_end15:
	.size	_ZN8NArchive3N7z11COutArchive13WritePackInfoEyRK13CRecordVectorIyERKS2_IbERKS2_IjE, .Lfunc_end15-_ZN8NArchive3N7z11COutArchive13WritePackInfoEyRK13CRecordVectorIyERKS2_IbERKS2_IjE
	.cfi_endproc

	.globl	_ZN8NArchive3N7z11COutArchive15WriteUnpackInfoERK13CObjectVectorINS0_7CFolderEE
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive15WriteUnpackInfoERK13CObjectVectorINS0_7CFolderEE,@function
_ZN8NArchive3N7z11COutArchive15WriteUnpackInfoERK13CObjectVectorINS0_7CFolderEE: # @_ZN8NArchive3N7z11COutArchive15WriteUnpackInfoERK13CObjectVectorINS0_7CFolderEE
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi126:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi127:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi128:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi129:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi130:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi131:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi132:
	.cfi_def_cfa_offset 128
.Lcfi133:
	.cfi_offset %rbx, -56
.Lcfi134:
	.cfi_offset %r12, -48
.Lcfi135:
	.cfi_offset %r13, -40
.Lcfi136:
	.cfi_offset %r14, -32
.Lcfi137:
	.cfi_offset %r15, -24
.Lcfi138:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbx
	cmpl	$0, 12(%r13)
	je	.LBB16_32
# BB#1:
	movl	$7, %esi
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	$11, %esi
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movslq	12(%r13), %r14
	xorl	%ebp, %ebp
	cmpq	$128, %r14
	movl	$0, %eax
	jb	.LBB16_4
# BB#2:
	cmpl	$16383, %r14d           # imm = 0x3FFF
	ja	.LBB16_33
# BB#3:
	movb	$-128, %al
	movl	$1, %ebp
	jmp	.LBB16_4
.LBB16_33:
	cmpl	$2097152, %r14d         # imm = 0x200000
	jae	.LBB16_35
# BB#34:
	movb	$-64, %al
	movl	$2, %ebp
	jmp	.LBB16_4
.LBB16_35:
	cmpl	$268435456, %r14d       # imm = 0x10000000
	jae	.LBB16_37
# BB#36:
	movb	$-32, %al
	movl	$3, %ebp
	jmp	.LBB16_4
.LBB16_37:
	testl	%r14d, %r14d
	js	.LBB16_39
# BB#38:
	movb	$-16, %al
	movl	$4, %ebp
.LBB16_4:                               # %.loopexit.i
	movl	%ebp, %ecx
	shlb	$3, %cl
	movq	%r14, %rdx
	shrq	%cl, %rdx
	orb	%al, %dl
	movzbl	%dl, %esi
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	testl	%ebp, %ebp
	je	.LBB16_7
.LBB16_5:                               # %.lr.ph.i.preheader
	incl	%ebp
	.p2align	4, 0x90
.LBB16_6:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%r14b, %esi
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	shrq	$8, %r14
	decl	%ebp
	cmpl	$1, %ebp
	jg	.LBB16_6
.LBB16_7:                               # %_ZN8NArchive3N7z11COutArchive11WriteNumberEy.exit
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	cmpl	$0, 12(%r13)
	jle	.LBB16_10
# BB#8:                                 # %.lr.ph62
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB16_9:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r13), %rax
	movq	(%rax,%rbp,8), %rsi
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z11COutArchive11WriteFolderERKNS0_7CFolderE
	incq	%rbp
	movslq	12(%r13), %rax
	cmpq	%rax, %rbp
	jl	.LBB16_9
.LBB16_10:                              # %._crit_edge63
	movl	$12, %esi
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	12(%r13), %eax
	testl	%eax, %eax
	movq	%r13, (%rsp)            # 8-byte Spill
	jle	.LBB16_23
# BB#11:                                # %.lr.ph59
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB16_12:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_14 Depth 2
                                        #       Child Loop BB16_19 Depth 3
	movq	16(%r13), %rcx
	movq	(%rcx,%r15,8), %r12
	cmpl	$0, 108(%r12)
	jle	.LBB16_22
# BB#13:                                # %.lr.ph55
                                        #   in Loop: Header=BB16_12 Depth=1
	xorl	%r13d, %r13d
	jmp	.LBB16_14
.LBB16_52:                              # %.loopexit.thread.i50
                                        #   in Loop: Header=BB16_14 Depth=2
	movl	$255, %esi
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	$8, %r14d
	jmp	.LBB16_18
	.p2align	4, 0x90
.LBB16_14:                              #   Parent Loop BB16_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB16_19 Depth 3
	movq	112(%r12), %rax
	movq	(%rax,%r13,8), %rbp
	xorl	%r14d, %r14d
	cmpq	$128, %rbp
	movl	$0, %eax
	jb	.LBB16_17
# BB#15:                                #   in Loop: Header=BB16_14 Depth=2
	cmpq	$16383, %rbp            # imm = 0x3FFF
	ja	.LBB16_40
# BB#16:                                #   in Loop: Header=BB16_14 Depth=2
	movb	$-128, %al
	movl	$1, %r14d
	jmp	.LBB16_17
	.p2align	4, 0x90
.LBB16_40:                              #   in Loop: Header=BB16_14 Depth=2
	cmpq	$2097152, %rbp          # imm = 0x200000
	jae	.LBB16_42
# BB#41:                                #   in Loop: Header=BB16_14 Depth=2
	movb	$-64, %al
	movl	$2, %r14d
	jmp	.LBB16_17
.LBB16_42:                              #   in Loop: Header=BB16_14 Depth=2
	cmpq	$268435456, %rbp        # imm = 0x10000000
	jae	.LBB16_44
# BB#43:                                #   in Loop: Header=BB16_14 Depth=2
	movb	$-32, %al
	movl	$3, %r14d
	jmp	.LBB16_17
.LBB16_44:                              #   in Loop: Header=BB16_14 Depth=2
	movq	%rbp, %rax
	shrq	$35, %rax
	jne	.LBB16_46
# BB#45:                                #   in Loop: Header=BB16_14 Depth=2
	movb	$-16, %al
	movl	$4, %r14d
	jmp	.LBB16_17
.LBB16_46:                              #   in Loop: Header=BB16_14 Depth=2
	movq	%rbp, %rax
	shrq	$42, %rax
	jne	.LBB16_48
# BB#47:                                #   in Loop: Header=BB16_14 Depth=2
	movb	$-8, %al
	movl	$5, %r14d
	jmp	.LBB16_17
.LBB16_48:                              #   in Loop: Header=BB16_14 Depth=2
	movq	%rbp, %rax
	shrq	$49, %rax
	jne	.LBB16_50
# BB#49:                                #   in Loop: Header=BB16_14 Depth=2
	movb	$-4, %al
	movl	$6, %r14d
	jmp	.LBB16_17
.LBB16_50:                              #   in Loop: Header=BB16_14 Depth=2
	movq	%rbp, %rax
	shrq	$56, %rax
	jne	.LBB16_52
# BB#51:                                #   in Loop: Header=BB16_14 Depth=2
	movb	$-2, %al
	movl	$7, %r14d
	.p2align	4, 0x90
.LBB16_17:                              # %.loopexit.i46
                                        #   in Loop: Header=BB16_14 Depth=2
	movl	%r14d, %ecx
	shlb	$3, %cl
	movq	%rbp, %rdx
	shrq	%cl, %rdx
	orb	%al, %dl
	movzbl	%dl, %esi
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	testl	%r14d, %r14d
	je	.LBB16_20
.LBB16_18:                              # %.lr.ph.i49.preheader
                                        #   in Loop: Header=BB16_14 Depth=2
	incl	%r14d
	.p2align	4, 0x90
.LBB16_19:                              # %.lr.ph.i49
                                        #   Parent Loop BB16_12 Depth=1
                                        #     Parent Loop BB16_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	%bpl, %esi
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	shrq	$8, %rbp
	decl	%r14d
	cmpl	$1, %r14d
	jg	.LBB16_19
.LBB16_20:                              # %_ZN8NArchive3N7z11COutArchive11WriteNumberEy.exit51
                                        #   in Loop: Header=BB16_14 Depth=2
	incq	%r13
	movslq	108(%r12), %rax
	cmpq	%rax, %r13
	jl	.LBB16_14
# BB#21:                                # %._crit_edge56.loopexit
                                        #   in Loop: Header=BB16_12 Depth=1
	movq	(%rsp), %r13            # 8-byte Reload
	movl	12(%r13), %eax
.LBB16_22:                              # %._crit_edge56
                                        #   in Loop: Header=BB16_12 Depth=1
	incq	%r15
	movslq	%eax, %rcx
	cmpq	%rcx, %r15
	jl	.LBB16_12
.LBB16_23:                              # %._crit_edge60
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rsp)
	movq	$1, 32(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 8(%rsp)
	movups	%xmm0, 48(%rsp)
	movq	$4, 64(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 40(%rsp)
	testl	%eax, %eax
	jle	.LBB16_28
# BB#24:                                # %.lr.ph
	xorl	%r14d, %r14d
	leaq	8(%rsp), %r12
	leaq	40(%rsp), %r15
	.p2align	4, 0x90
.LBB16_25:                              # =>This Inner Loop Header: Depth=1
	movq	16(%r13), %rax
	movq	(%rax,%r14,8), %rbp
	movzbl	132(%rbp), %r13d
.Ltmp0:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp1:
# BB#26:                                #   in Loop: Header=BB16_25 Depth=1
	movq	24(%rsp), %rax
	movslq	20(%rsp), %rcx
	movb	%r13b, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 20(%rsp)
	movl	128(%rbp), %ebp
.Ltmp2:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp3:
# BB#27:                                #   in Loop: Header=BB16_25 Depth=1
	movq	56(%rsp), %rax
	movslq	52(%rsp), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	52(%rsp)
	incq	%r14
	movq	(%rsp), %r13            # 8-byte Reload
	movslq	12(%r13), %rax
	cmpq	%rax, %r14
	jl	.LBB16_25
.LBB16_28:                              # %._crit_edge
.Ltmp5:
	leaq	8(%rsp), %rsi
	leaq	40(%rsp), %r15
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	_ZN8NArchive3N7z11COutArchive16WriteHashDigestsERK13CRecordVectorIbERKS2_IjE
.Ltmp6:
# BB#29:
.Ltmp7:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp8:
# BB#30:
.Ltmp12:
	leaq	40(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp13:
# BB#31:
	leaq	8(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.LBB16_32:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB16_39:                              # %.loopexit.thread.i
	movl	$255, %esi
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	$8, %ebp
	jmp	.LBB16_5
.LBB16_54:
.Ltmp14:
	movq	%rax, %rbx
	jmp	.LBB16_57
.LBB16_55:
.Ltmp9:
	jmp	.LBB16_56
.LBB16_53:
.Ltmp4:
.LBB16_56:
	movq	%rax, %rbx
.Ltmp10:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp11:
.LBB16_57:
.Ltmp15:
	leaq	8(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp16:
# BB#58:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB16_59:
.Ltmp17:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end16:
	.size	_ZN8NArchive3N7z11COutArchive15WriteUnpackInfoERK13CObjectVectorINS0_7CFolderEE, .Lfunc_end16-_ZN8NArchive3N7z11COutArchive15WriteUnpackInfoERK13CObjectVectorINS0_7CFolderEE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp8-.Ltmp5           #   Call between .Ltmp5 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp10-.Ltmp13         #   Call between .Ltmp13 and .Ltmp10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp16-.Ltmp10         #   Call between .Ltmp10 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Lfunc_end16-.Ltmp16    #   Call between .Ltmp16 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end17:
	.size	__clang_call_terminate, .Lfunc_end17-__clang_call_terminate

	.text
	.globl	_ZN8NArchive3N7z11COutArchive19WriteSubStreamsInfoERK13CObjectVectorINS0_7CFolderEERK13CRecordVectorIjERKS7_IyERKS7_IbESA_
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive19WriteSubStreamsInfoERK13CObjectVectorINS0_7CFolderEERK13CRecordVectorIjERKS7_IyERKS7_IbESA_,@function
_ZN8NArchive3N7z11COutArchive19WriteSubStreamsInfoERK13CObjectVectorINS0_7CFolderEERK13CRecordVectorIjERKS7_IyERKS7_IbESA_: # @_ZN8NArchive3N7z11COutArchive19WriteSubStreamsInfoERK13CObjectVectorINS0_7CFolderEERK13CRecordVectorIjERKS7_IyERKS7_IbESA_
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi139:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi140:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi141:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi142:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi143:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi144:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi145:
	.cfi_def_cfa_offset 176
.Lcfi146:
	.cfi_offset %rbx, -56
.Lcfi147:
	.cfi_offset %r12, -48
.Lcfi148:
	.cfi_offset %r13, -40
.Lcfi149:
	.cfi_offset %r14, -32
.Lcfi150:
	.cfi_offset %r15, -24
.Lcfi151:
	.cfi_offset %rbp, -16
	movq	%r9, 104(%rsp)          # 8-byte Spill
	movq	%r8, 112(%rsp)          # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, %r15
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdi, %r12
	movl	$8, %esi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movslq	12(%r15), %rcx
	testq	%rcx, %rcx
	movq	%r15, 16(%rsp)          # 8-byte Spill
	jle	.LBB18_19
# BB#1:                                 # %.lr.ph110
	movq	16(%r15), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB18_2:                               # =>This Inner Loop Header: Depth=1
	cmpl	$1, (%rax,%rdx,4)
	jne	.LBB18_28
# BB#3:                                 #   in Loop: Header=BB18_2 Depth=1
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB18_2
	jmp	.LBB18_4
.LBB18_28:
	movl	$13, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	cmpl	$0, 12(%r15)
	jle	.LBB18_19
# BB#29:                                # %.lr.ph107.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB18_30:                              # %.lr.ph107
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_35 Depth 2
	movq	16(%r15), %rax
	movl	(%rax,%r14,4), %ebp
	cmpq	$128, %rbp
	movl	$0, %ebx
	movl	$0, %eax
	jb	.LBB18_33
# BB#31:                                #   in Loop: Header=BB18_30 Depth=1
	cmpl	$16383, %ebp            # imm = 0x3FFF
	ja	.LBB18_37
# BB#32:                                #   in Loop: Header=BB18_30 Depth=1
	movb	$-128, %al
	movl	$1, %ebx
	jmp	.LBB18_33
	.p2align	4, 0x90
.LBB18_37:                              #   in Loop: Header=BB18_30 Depth=1
	cmpl	$2097152, %ebp          # imm = 0x200000
	jae	.LBB18_39
# BB#38:                                #   in Loop: Header=BB18_30 Depth=1
	movb	$-64, %al
	movl	$2, %ebx
	jmp	.LBB18_33
.LBB18_39:                              #   in Loop: Header=BB18_30 Depth=1
	xorl	%ebx, %ebx
	cmpl	$268435455, %ebp        # imm = 0xFFFFFFF
	seta	%bl
	cmpl	$268435456, %ebp        # imm = 0x10000000
	movb	$-32, %al
	jb	.LBB18_41
# BB#40:                                #   in Loop: Header=BB18_30 Depth=1
	movb	$-16, %al
.LBB18_41:                              #   in Loop: Header=BB18_30 Depth=1
	addl	$3, %ebx
	.p2align	4, 0x90
.LBB18_33:                              # %.loopexit.i
                                        #   in Loop: Header=BB18_30 Depth=1
	movl	%ebx, %ecx
	shlb	$3, %cl
	movq	%rbp, %rdx
	shrq	%cl, %rdx
	orb	%al, %dl
	movzbl	%dl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	testl	%ebx, %ebx
	je	.LBB18_36
# BB#34:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB18_30 Depth=1
	incl	%ebx
	.p2align	4, 0x90
.LBB18_35:                              # %.lr.ph.i
                                        #   Parent Loop BB18_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%bpl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	shrq	$8, %rbp
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB18_35
.LBB18_36:                              # %_ZN8NArchive3N7z11COutArchive11WriteNumberEy.exit
                                        #   in Loop: Header=BB18_30 Depth=1
	incq	%r14
	movslq	12(%r15), %rcx
	cmpq	%rcx, %r14
	jl	.LBB18_30
.LBB18_4:                               # %.preheader83
	testl	%ecx, %ecx
	jle	.LBB18_19
# BB#5:                                 # %.preheader82.lr.ph
	movq	16(%r15), %rdx
	movb	$1, %al
	xorl	%ebp, %ebp
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB18_6:                               # %.preheader82
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_8 Depth 2
                                        #       Child Loop BB18_44 Depth 3
	movl	(%rdx,%rbp,4), %esi
	testl	%esi, %esi
	je	.LBB18_18
# BB#7:                                 # %.lr.ph97.preheader
                                        #   in Loop: Header=BB18_6 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB18_8
.LBB18_42:                              # %.loopexit.thread.i
                                        #   in Loop: Header=BB18_8 Depth=2
	movl	$255, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	$8, %r15d
	jmp	.LBB18_43
	.p2align	4, 0x90
.LBB18_8:                               # %.lr.ph97
                                        #   Parent Loop BB18_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB18_44 Depth 3
	incl	%ebx
	cmpl	%esi, %ebx
	je	.LBB18_16
# BB#9:                                 #   in Loop: Header=BB18_8 Depth=2
	testb	$1, %al
	je	.LBB18_11
# BB#10:                                #   in Loop: Header=BB18_8 Depth=2
	movl	$9, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.LBB18_11:                              #   in Loop: Header=BB18_8 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	16(%rax), %rax
	movslq	%r13d, %rcx
	movq	(%rax,%rcx,8), %r14
	xorl	%r15d, %r15d
	cmpq	$128, %r14
	movl	$0, %eax
	jb	.LBB18_14
# BB#12:                                #   in Loop: Header=BB18_8 Depth=2
	cmpq	$16383, %r14            # imm = 0x3FFF
	ja	.LBB18_45
# BB#13:                                #   in Loop: Header=BB18_8 Depth=2
	movb	$-128, %al
	movl	$1, %r15d
	jmp	.LBB18_14
.LBB18_45:                              #   in Loop: Header=BB18_8 Depth=2
	cmpq	$2097152, %r14          # imm = 0x200000
	jae	.LBB18_47
# BB#46:                                #   in Loop: Header=BB18_8 Depth=2
	movb	$-64, %al
	movl	$2, %r15d
	jmp	.LBB18_14
.LBB18_47:                              #   in Loop: Header=BB18_8 Depth=2
	cmpq	$268435456, %r14        # imm = 0x10000000
	jae	.LBB18_49
# BB#48:                                #   in Loop: Header=BB18_8 Depth=2
	movb	$-32, %al
	movl	$3, %r15d
	jmp	.LBB18_14
.LBB18_49:                              #   in Loop: Header=BB18_8 Depth=2
	movq	%r14, %rax
	shrq	$35, %rax
	jne	.LBB18_51
# BB#50:                                #   in Loop: Header=BB18_8 Depth=2
	movb	$-16, %al
	movl	$4, %r15d
	jmp	.LBB18_14
.LBB18_51:                              #   in Loop: Header=BB18_8 Depth=2
	movq	%r14, %rax
	shrq	$42, %rax
	jne	.LBB18_53
# BB#52:                                #   in Loop: Header=BB18_8 Depth=2
	movb	$-8, %al
	movl	$5, %r15d
	jmp	.LBB18_14
.LBB18_53:                              #   in Loop: Header=BB18_8 Depth=2
	movq	%r14, %rax
	shrq	$49, %rax
	jne	.LBB18_55
# BB#54:                                #   in Loop: Header=BB18_8 Depth=2
	movb	$-4, %al
	movl	$6, %r15d
	jmp	.LBB18_14
.LBB18_55:                              #   in Loop: Header=BB18_8 Depth=2
	movq	%r14, %rax
	shrq	$56, %rax
	jne	.LBB18_42
# BB#56:                                #   in Loop: Header=BB18_8 Depth=2
	movb	$-2, %al
	movl	$7, %r15d
	.p2align	4, 0x90
.LBB18_14:                              # %.loopexit.i76
                                        #   in Loop: Header=BB18_8 Depth=2
	movl	%r15d, %ecx
	shlb	$3, %cl
	movq	%r14, %rdx
	shrq	%cl, %rdx
	orb	%al, %dl
	movzbl	%dl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	testl	%r15d, %r15d
	je	.LBB18_15
.LBB18_43:                              # %.lr.ph.i79.preheader
                                        #   in Loop: Header=BB18_8 Depth=2
	incl	%r15d
	.p2align	4, 0x90
.LBB18_44:                              # %.lr.ph.i79
                                        #   Parent Loop BB18_6 Depth=1
                                        #     Parent Loop BB18_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	%r14b, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	shrq	$8, %r14
	decl	%r15d
	cmpl	$1, %r15d
	jg	.LBB18_44
.LBB18_15:                              #   in Loop: Header=BB18_8 Depth=2
	xorl	%eax, %eax
	movq	16(%rsp), %r15          # 8-byte Reload
.LBB18_16:                              # %_ZN8NArchive3N7z11COutArchive11WriteNumberEy.exit80
                                        #   in Loop: Header=BB18_8 Depth=2
	incl	%r13d
	movq	16(%r15), %rdx
	movl	(%rdx,%rbp,4), %esi
	cmpl	%esi, %ebx
	jb	.LBB18_8
# BB#17:                                # %._crit_edge98.loopexit
                                        #   in Loop: Header=BB18_6 Depth=1
	movl	12(%r15), %ecx
.LBB18_18:                              # %._crit_edge98
                                        #   in Loop: Header=BB18_6 Depth=1
	incq	%rbp
	movslq	%ecx, %rsi
	cmpq	%rsi, %rbp
	jl	.LBB18_6
.LBB18_19:                              # %._crit_edge104
	xorps	%xmm0, %xmm0
	movups	%xmm0, 80(%rsp)
	movq	$1, 96(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 72(%rsp)
	movups	%xmm0, 48(%rsp)
	movq	$4, 64(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 40(%rsp)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	12(%rax), %eax
	testl	%eax, %eax
	jle	.LBB18_60
# BB#20:                                # %.lr.ph92
	xorl	%esi, %esi
	leaq	40(%rsp), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB18_21:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_24 Depth 2
	movq	16(%r15), %rcx
	movl	(%rcx,%rsi,4), %ebx
	cmpl	$1, %ebx
	jne	.LBB18_22
# BB#57:                                #   in Loop: Header=BB18_21 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	16(%rcx), %rcx
	movq	(%rcx,%rsi,8), %rcx
	cmpb	$0, 132(%rcx)
	je	.LBB18_23
# BB#58:                                #   in Loop: Header=BB18_21 Depth=1
	incl	%edi
	jmp	.LBB18_59
	.p2align	4, 0x90
.LBB18_22:                              # %.preheader
                                        #   in Loop: Header=BB18_21 Depth=1
	testl	%ebx, %ebx
	jle	.LBB18_59
.LBB18_23:                              # %.lr.ph.preheader
                                        #   in Loop: Header=BB18_21 Depth=1
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movl	%edi, 28(%rsp)          # 4-byte Spill
	movslq	%edi, %r13
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB18_24:                              # %.lr.ph
                                        #   Parent Loop BB18_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	16(%rax), %rax
	movzbl	(%rax,%r13), %r14d
.Ltmp18:
	leaq	72(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp19:
# BB#25:                                #   in Loop: Header=BB18_24 Depth=2
	movq	88(%rsp), %rax
	movslq	84(%rsp), %rcx
	movb	%r14b, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 84(%rsp)
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	16(%rax), %rax
	movl	(%rax,%r13,4), %ebp
.Ltmp20:
	leaq	40(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp21:
# BB#26:                                #   in Loop: Header=BB18_24 Depth=2
	movq	56(%rsp), %rax
	movslq	52(%rsp), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	52(%rsp)
	incl	%r15d
	incq	%r13
	cmpl	%ebx, %r15d
	jl	.LBB18_24
# BB#27:                                # %.loopexit.loopexit
                                        #   in Loop: Header=BB18_21 Depth=1
	movl	28(%rsp), %edi          # 4-byte Reload
	addl	%r15d, %edi
	movq	(%rsp), %rax            # 8-byte Reload
	movl	12(%rax), %eax
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB18_59:                              # %.loopexit
                                        #   in Loop: Header=BB18_21 Depth=1
	incq	%rsi
	movslq	%eax, %rcx
	cmpq	%rcx, %rsi
	jl	.LBB18_21
.LBB18_60:                              # %._crit_edge
.Ltmp23:
	leaq	72(%rsp), %rsi
	leaq	40(%rsp), %rdx
	movq	%r12, %rdi
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	callq	_ZN8NArchive3N7z11COutArchive16WriteHashDigestsERK13CRecordVectorIbERKS2_IjE
.Ltmp24:
# BB#61:
.Ltmp25:
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp26:
# BB#62:
.Ltmp30:
	leaq	40(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp31:
# BB#63:
	leaq	72(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB18_67:
.Ltmp32:
	movq	%rax, %rbx
	jmp	.LBB18_68
.LBB18_65:
.Ltmp27:
	jmp	.LBB18_66
.LBB18_64:
.Ltmp22:
.LBB18_66:
	movq	%rax, %rbx
.Ltmp28:
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp29:
.LBB18_68:
.Ltmp33:
	leaq	72(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp34:
# BB#69:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB18_70:
.Ltmp35:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end18:
	.size	_ZN8NArchive3N7z11COutArchive19WriteSubStreamsInfoERK13CObjectVectorINS0_7CFolderEERK13CRecordVectorIjERKS7_IyERKS7_IbESA_, .Lfunc_end18-_ZN8NArchive3N7z11COutArchive19WriteSubStreamsInfoERK13CObjectVectorINS0_7CFolderEERK13CRecordVectorIjERKS7_IyERKS7_IbESA_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp18-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp21-.Ltmp18         #   Call between .Ltmp18 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin1   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp26-.Ltmp23         #   Call between .Ltmp23 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin1   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin1   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp28-.Ltmp31         #   Call between .Ltmp31 and .Ltmp28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp34-.Ltmp28         #   Call between .Ltmp28 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin1   #     jumps to .Ltmp35
	.byte	1                       #   On action: 1
	.long	.Ltmp34-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Lfunc_end18-.Ltmp34    #   Call between .Ltmp34 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3N7z11COutArchive9SkipAlignEjj
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive9SkipAlignEjj,@function
_ZN8NArchive3N7z11COutArchive9SkipAlignEjj: # @_ZN8NArchive3N7z11COutArchive9SkipAlignEjj
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end19:
	.size	_ZN8NArchive3N7z11COutArchive9SkipAlignEjj, .Lfunc_end19-_ZN8NArchive3N7z11COutArchive9SkipAlignEjj
	.cfi_endproc

	.globl	_ZN8NArchive3N7z11COutArchive22WriteAlignedBoolHeaderERK13CRecordVectorIbEihj
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive22WriteAlignedBoolHeaderERK13CRecordVectorIbEihj,@function
_ZN8NArchive3N7z11COutArchive22WriteAlignedBoolHeaderERK13CRecordVectorIbEihj: # @_ZN8NArchive3N7z11COutArchive22WriteAlignedBoolHeaderERK13CRecordVectorIbEihj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi152:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi153:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi154:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi155:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi156:
	.cfi_def_cfa_offset 48
.Lcfi157:
	.cfi_offset %rbx, -48
.Lcfi158:
	.cfi_offset %r12, -40
.Lcfi159:
	.cfi_offset %r14, -32
.Lcfi160:
	.cfi_offset %r15, -24
.Lcfi161:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %r14
	movq	%rdi, %r12
	movl	12(%r14), %eax
	leal	7(%rax), %edx
	shrl	$3, %edx
	xorl	%ebx, %ebx
	cmpl	%r15d, %eax
	movl	$0, %eax
	cmovneq	%rdx, %rax
	movslq	%r15d, %rdx
	movl	%r8d, %esi
	imulq	%rdx, %rsi
	leaq	2(%rsi,%rax), %rbp
	movl	%ecx, %esi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	cmpq	$128, %rbp
	movl	$0, %eax
	jb	.LBB20_3
# BB#1:
	cmpq	$16383, %rbp            # imm = 0x3FFF
	ja	.LBB20_8
# BB#2:
	movb	$-128, %al
	movl	$1, %ebx
	jmp	.LBB20_3
.LBB20_8:
	cmpq	$2097152, %rbp          # imm = 0x200000
	jae	.LBB20_10
# BB#9:
	movb	$-64, %al
	movl	$2, %ebx
	jmp	.LBB20_3
.LBB20_10:
	cmpq	$268435456, %rbp        # imm = 0x10000000
	jae	.LBB20_12
# BB#11:
	movb	$-32, %al
	movl	$3, %ebx
	jmp	.LBB20_3
.LBB20_12:
	movq	%rbp, %rax
	shrq	$35, %rax
	jne	.LBB20_14
# BB#13:
	movb	$-16, %al
	movl	$4, %ebx
	jmp	.LBB20_3
.LBB20_14:
	movq	%rbp, %rax
	shrq	$42, %rax
	jne	.LBB20_16
# BB#15:
	movb	$-8, %al
	movl	$5, %ebx
	jmp	.LBB20_3
.LBB20_16:
	movq	%rbp, %rax
	shrq	$49, %rax
	jne	.LBB20_18
# BB#17:
	movb	$-4, %al
	movl	$6, %ebx
	jmp	.LBB20_3
.LBB20_18:
	movq	%rbp, %rax
	shrq	$56, %rax
	jne	.LBB20_20
# BB#19:
	movb	$-2, %al
	movl	$7, %ebx
.LBB20_3:                               # %.loopexit.i
	movl	%ebx, %ecx
	shlb	$3, %cl
	movq	%rbp, %rdx
	shrq	%cl, %rdx
	orb	%al, %dl
	movzbl	%dl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	testl	%ebx, %ebx
	je	.LBB20_6
.LBB20_4:                               # %.lr.ph.i17.preheader
	incl	%ebx
	.p2align	4, 0x90
.LBB20_5:                               # %.lr.ph.i17
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%bpl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	shrq	$8, %rbp
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB20_5
.LBB20_6:                               # %_ZN8NArchive3N7z11COutArchive11WriteNumberEy.exit
	cmpl	%r15d, 12(%r14)
	jne	.LBB20_21
# BB#7:
	movl	$1, %esi
	jmp	.LBB20_30
.LBB20_21:
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	12(%r14), %esi
	testl	%esi, %esi
	jle	.LBB20_31
# BB#22:                                # %.lr.ph.i
	movb	$-128, %al
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB20_23:                              # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rcx
	movzbl	(%rcx,%rbx), %ecx
	testb	%cl, %cl
	je	.LBB20_25
# BB#24:                                #   in Loop: Header=BB20_23 Depth=1
	movl	%eax, %ecx
.LBB20_25:                              #   in Loop: Header=BB20_23 Depth=1
	orb	%cl, %dl
	shrb	%al
	jne	.LBB20_27
# BB#26:                                #   in Loop: Header=BB20_23 Depth=1
	movzbl	%dl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	12(%r14), %esi
	movb	$-128, %al
	xorl	%edx, %edx
.LBB20_27:                              #   in Loop: Header=BB20_23 Depth=1
	incq	%rbx
	movslq	%esi, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB20_23
# BB#28:                                # %._crit_edge.i
	cmpb	$-128, %al
	je	.LBB20_31
# BB#29:
	movzbl	%dl, %esi
.LBB20_30:                              # %_ZN8NArchive3N7z11COutArchive15WriteBoolVectorERK13CRecordVectorIbE.exit
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.LBB20_31:                              # %_ZN8NArchive3N7z11COutArchive15WriteBoolVectorERK13CRecordVectorIbE.exit
	xorl	%esi, %esi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN8NArchive3N7z11COutArchive9WriteByteEh # TAILCALL
.LBB20_20:                              # %.loopexit.thread.i
	movl	$255, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	$8, %ebx
	jmp	.LBB20_4
.Lfunc_end20:
	.size	_ZN8NArchive3N7z11COutArchive22WriteAlignedBoolHeaderERK13CRecordVectorIbEihj, .Lfunc_end20-_ZN8NArchive3N7z11COutArchive22WriteAlignedBoolHeaderERK13CRecordVectorIbEihj
	.cfi_endproc

	.globl	_ZN8NArchive3N7z11COutArchive20WriteUInt64DefVectorERKNS0_16CUInt64DefVectorEh
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive20WriteUInt64DefVectorERKNS0_16CUInt64DefVectorEh,@function
_ZN8NArchive3N7z11COutArchive20WriteUInt64DefVectorERKNS0_16CUInt64DefVectorEh: # @_ZN8NArchive3N7z11COutArchive20WriteUInt64DefVectorERKNS0_16CUInt64DefVectorEh
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi162:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi163:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi164:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi166:
	.cfi_def_cfa_offset 48
.Lcfi167:
	.cfi_offset %rbx, -40
.Lcfi168:
	.cfi_offset %r12, -32
.Lcfi169:
	.cfi_offset %r14, -24
.Lcfi170:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movslq	44(%r14), %rcx
	testq	%rcx, %rcx
	jle	.LBB21_18
# BB#1:                                 # %.lr.ph26
	movq	48(%r14), %rsi
	cmpl	$8, %ecx
	jb	.LBB21_2
# BB#3:                                 # %min.iters.checked
	movq	%rcx, %rdi
	andq	$-8, %rdi
	je	.LBB21_2
# BB#4:                                 # %vector.body.preheader
	leaq	-8(%rdi), %rax
	movq	%rax, %rbx
	shrq	$3, %rbx
	btl	$3, %eax
	jb	.LBB21_5
# BB#6:                                 # %vector.body.prol
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movd	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	pxor	%xmm2, %xmm2
	punpcklbw	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3],xmm0[4],xmm2[4],xmm0[5],xmm2[5],xmm0[6],xmm2[6],xmm0[7],xmm2[7]
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	punpcklbw	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3],xmm1[4],xmm2[4],xmm1[5],xmm2[5],xmm1[6],xmm2[6],xmm1[7],xmm2[7]
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	movl	$8, %r8d
	testq	%rbx, %rbx
	jne	.LBB21_8
	jmp	.LBB21_10
.LBB21_2:
	xorl	%edi, %edi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB21_11:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi,%rdi), %ebx
	addl	%ebx, %eax
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB21_11
.LBB21_12:                              # %._crit_edge
	testl	%eax, %eax
	je	.LBB21_18
# BB#13:
	leaq	32(%r14), %rsi
	movzbl	%dl, %ecx
	movl	$8, %r8d
	movq	%r15, %rdi
	movl	%eax, %edx
	callq	_ZN8NArchive3N7z11COutArchive22WriteAlignedBoolHeaderERK13CRecordVectorIbEihj
	movl	44(%r14), %eax
	testl	%eax, %eax
	jle	.LBB21_18
# BB#14:                                # %.lr.ph
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB21_15:                              # =>This Inner Loop Header: Depth=1
	movq	48(%r14), %rcx
	cmpb	$0, (%rcx,%r12)
	je	.LBB21_17
# BB#16:                                #   in Loop: Header=BB21_15 Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%r12,8), %rbx
	movzbl	%bl, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movzbl	%bh, %esi  # NOREX
	movq	%r15, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	%ebx, %eax
	shrl	$16, %eax
	movzbl	%al, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	%ebx, %esi
	shrl	$24, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movq	%rbx, %rax
	shrq	$32, %rax
	movzbl	%al, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movq	%rbx, %rax
	shrq	$40, %rax
	movzbl	%al, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movq	%rbx, %rax
	shrq	$48, %rax
	movzbl	%al, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	shrq	$56, %rbx
	movq	%r15, %rdi
	movl	%ebx, %esi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	44(%r14), %eax
.LBB21_17:                              #   in Loop: Header=BB21_15 Depth=1
	incq	%r12
	movslq	%eax, %rcx
	cmpq	%rcx, %r12
	jl	.LBB21_15
.LBB21_18:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB21_5:
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	pxor	%xmm1, %xmm1
	testq	%rbx, %rbx
	je	.LBB21_10
.LBB21_8:                               # %vector.body.preheader.new
	movq	%rdi, %rax
	subq	%r8, %rax
	leaq	12(%rsi,%r8), %rbx
	pxor	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB21_9:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	-12(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movd	-8(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1],xmm3[2],xmm2[2],xmm3[3],xmm2[3],xmm3[4],xmm2[4],xmm3[5],xmm2[5],xmm3[6],xmm2[6],xmm3[7],xmm2[7]
	punpcklwd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1],xmm3[2],xmm2[2],xmm3[3],xmm2[3]
	punpcklbw	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3],xmm4[4],xmm2[4],xmm4[5],xmm2[5],xmm4[6],xmm2[6],xmm4[7],xmm2[7]
	punpcklwd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3]
	paddd	%xmm0, %xmm3
	paddd	%xmm1, %xmm4
	movd	-4(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movd	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3],xmm0[4],xmm2[4],xmm0[5],xmm2[5],xmm0[6],xmm2[6],xmm0[7],xmm2[7]
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	punpcklbw	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3],xmm1[4],xmm2[4],xmm1[5],xmm2[5],xmm1[6],xmm2[6],xmm1[7],xmm2[7]
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	paddd	%xmm3, %xmm0
	paddd	%xmm4, %xmm1
	addq	$16, %rbx
	addq	$-16, %rax
	jne	.LBB21_9
.LBB21_10:                              # %middle.block
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	cmpq	%rdi, %rcx
	jne	.LBB21_11
	jmp	.LBB21_12
.Lfunc_end21:
	.size	_ZN8NArchive3N7z11COutArchive20WriteUInt64DefVectorERKNS0_16CUInt64DefVectorEh, .Lfunc_end21-_ZN8NArchive3N7z11COutArchive20WriteUInt64DefVectorERKNS0_16CUInt64DefVectorEh
	.cfi_endproc

	.globl	_ZN8NArchive3N7z11COutArchive12EncodeStreamERNS0_8CEncoderERK7CBufferIhER13CRecordVectorIyER13CObjectVectorINS0_7CFolderEE
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive12EncodeStreamERNS0_8CEncoderERK7CBufferIhER13CRecordVectorIyER13CObjectVectorINS0_7CFolderEE,@function
_ZN8NArchive3N7z11COutArchive12EncodeStreamERNS0_8CEncoderERK7CBufferIhER13CRecordVectorIyER13CObjectVectorINS0_7CFolderEE: # @_ZN8NArchive3N7z11COutArchive12EncodeStreamERNS0_8CEncoderERK7CBufferIhER13CRecordVectorIyER13CObjectVectorINS0_7CFolderEE
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi171:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi172:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi173:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi174:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi175:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi176:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi177:
	.cfi_def_cfa_offset 208
.Lcfi178:
	.cfi_offset %rbx, -56
.Lcfi179:
	.cfi_offset %r12, -48
.Lcfi180:
	.cfi_offset %r13, -40
.Lcfi181:
	.cfi_offset %r14, -32
.Lcfi182:
	.cfi_offset %r15, -24
.Lcfi183:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rcx, %r15
	movq	%rdx, %rbp
	movq	%rsi, %r12
	movq	%rdi, %r13
.Lcfi184:
	.cfi_escape 0x2e, 0x00
	movl	$48, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	$0, 8(%rbx)
	movq	$_ZTV12CBufInStream+16, (%rbx)
	movq	$0, 40(%rbx)
.Lcfi185:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	*_ZTV12CBufInStream+24(%rip)
	movq	8(%rbp), %rsi
	movq	16(%rbp), %rdi
	movq	%rdi, 16(%rbx)
	movq	%rsi, 32(%rbx)
	movq	$0, 24(%rbx)
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.LBB22_3
# BB#1:
	movq	(%rax), %rcx
.Ltmp36:
.Lcfi186:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	*16(%rcx)
.Ltmp37:
# BB#2:                                 # %._crit_edge
	movq	8(%rbp), %rsi
	movq	16(%rbp), %rdi
.LBB22_3:
	movq	$0, 40(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rsp)
	movq	$8, 40(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, 16(%rsp)
	movups	%xmm0, 56(%rsp)
	movq	$8, 72(%rsp)
	movq	$_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE+16, 48(%rsp)
	movups	%xmm0, 88(%rsp)
	movq	$4, 104(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 80(%rsp)
	movups	%xmm0, 120(%rsp)
	movq	$8, 136(%rsp)
	movq	$_ZTV13CRecordVectorIyE+16, 112(%rsp)
	movb	$1, 148(%rsp)
.Ltmp39:
.Lcfi187:
	.cfi_escape 0x2e, 0x00
	callq	CrcCalc
.Ltmp40:
# BB#4:
	movl	%eax, 144(%rsp)
	movq	8(%rbp), %rax
	movq	%rax, 8(%rsp)
	movq	120(%r13), %r9
.Ltmp42:
.Lcfi188:
	.cfi_escape 0x2e, 0x10
	leaq	8(%rsp), %rcx
	leaq	16(%rsp), %r8
	movl	$0, %edx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	pushq	$0
.Lcfi189:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi190:
	.cfi_adjust_cfa_offset 8
	callq	_ZN8NArchive3N7z8CEncoder6EncodeEP19ISequentialInStreamPKyS5_RNS0_7CFolderEP20ISequentialOutStreamR13CRecordVectorIyEP21ICompressProgressInfo
	addq	$16, %rsp
.Lcfi191:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %ebp
.Ltmp43:
# BB#5:
	testl	%ebp, %ebp
	jne	.LBB22_10
# BB#6:
.Ltmp44:
.Lcfi192:
	.cfi_escape 0x2e, 0x00
	movl	$136, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp45:
# BB#7:                                 # %.noexc29
.Ltmp46:
.Lcfi193:
	.cfi_escape 0x2e, 0x00
	leaq	16(%rsp), %rsi
	movq	%rbp, %rdi
	callq	_ZN8NArchive3N7z7CFolderC2ERKS1_
.Ltmp47:
# BB#8:
.Ltmp49:
.Lcfi194:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp50:
# BB#9:                                 # %_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE3AddERKS2_.exit
	movq	16(%r14), %rax
	movslq	12(%r14), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r14)
	xorl	%ebp, %ebp
.LBB22_10:
.Ltmp54:
.Lcfi195:
	.cfi_escape 0x2e, 0x00
	leaq	16(%rsp), %rdi
	callq	_ZN8NArchive3N7z7CFolderD2Ev
.Ltmp55:
# BB#11:                                # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit28
	movq	(%rbx), %rax
.Lcfi196:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	*16(%rax)
	movl	%ebp, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB22_14:
.Ltmp48:
	movq	%rax, %r14
.Lcfi197:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB22_18
.LBB22_12:
.Ltmp38:
	movq	%rax, %r14
	jmp	.LBB22_19
.LBB22_15:
.Ltmp56:
	movq	%rax, %r14
	jmp	.LBB22_19
.LBB22_16:
.Ltmp41:
	jmp	.LBB22_17
.LBB22_13:
.Ltmp51:
.LBB22_17:
	movq	%rax, %r14
.LBB22_18:
.Ltmp52:
.Lcfi198:
	.cfi_escape 0x2e, 0x00
	leaq	16(%rsp), %rdi
	callq	_ZN8NArchive3N7z7CFolderD2Ev
.Ltmp53:
.LBB22_19:
	movq	(%rbx), %rax
.Ltmp57:
.Lcfi199:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp58:
# BB#20:                                # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
.Lcfi200:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB22_21:
.Ltmp59:
.Lcfi201:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end22:
	.size	_ZN8NArchive3N7z11COutArchive12EncodeStreamERNS0_8CEncoderERK7CBufferIhER13CRecordVectorIyER13CObjectVectorINS0_7CFolderEE, .Lfunc_end22-_ZN8NArchive3N7z11COutArchive12EncodeStreamERNS0_8CEncoderERK7CBufferIhER13CRecordVectorIyER13CObjectVectorINS0_7CFolderEE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp36-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp37-.Ltmp36         #   Call between .Ltmp36 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin2   #     jumps to .Ltmp38
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin2   #     jumps to .Ltmp41
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp45-.Ltmp42         #   Call between .Ltmp42 and .Ltmp45
	.long	.Ltmp51-.Lfunc_begin2   #     jumps to .Ltmp51
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin2   #     jumps to .Ltmp48
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin2   #     jumps to .Ltmp51
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin2   #     jumps to .Ltmp56
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp52-.Ltmp55         #   Call between .Ltmp55 and .Ltmp52
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Ltmp58-.Ltmp52         #   Call between .Ltmp52 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin2   #     jumps to .Ltmp59
	.byte	1                       #   On action: 1
	.long	.Ltmp58-.Lfunc_begin2   # >> Call Site 10 <<
	.long	.Lfunc_end22-.Ltmp58    #   Call between .Ltmp58 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z7CFolderD2Ev,"axG",@progbits,_ZN8NArchive3N7z7CFolderD2Ev,comdat
	.weak	_ZN8NArchive3N7z7CFolderD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z7CFolderD2Ev,@function
_ZN8NArchive3N7z7CFolderD2Ev:           # @_ZN8NArchive3N7z7CFolderD2Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi202:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi203:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi204:
	.cfi_def_cfa_offset 32
.Lcfi205:
	.cfi_offset %rbx, -24
.Lcfi206:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	96(%rbx), %rdi
.Ltmp60:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp61:
# BB#1:
	leaq	64(%rbx), %rdi
.Ltmp65:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp66:
# BB#2:
	leaq	32(%rbx), %rdi
.Ltmp70:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp71:
# BB#3:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp82:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp83:
# BB#4:                                 # %_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB23_5:
.Ltmp84:
	movq	%rax, %r14
.Ltmp85:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp86:
	jmp	.LBB23_6
.LBB23_7:
.Ltmp87:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB23_9:
.Ltmp72:
	movq	%rax, %r14
	jmp	.LBB23_12
.LBB23_10:
.Ltmp67:
	movq	%rax, %r14
	jmp	.LBB23_11
.LBB23_8:
.Ltmp62:
	movq	%rax, %r14
	leaq	64(%rbx), %rdi
.Ltmp63:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp64:
.LBB23_11:
	leaq	32(%rbx), %rdi
.Ltmp68:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp69:
.LBB23_12:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp73:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp74:
# BB#13:
.Ltmp79:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp80:
.LBB23_6:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB23_14:
.Ltmp75:
	movq	%rax, %r14
.Ltmp76:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp77:
	jmp	.LBB23_17
.LBB23_15:
.Ltmp78:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB23_16:
.Ltmp81:
	movq	%rax, %r14
.LBB23_17:                              # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end23:
	.size	_ZN8NArchive3N7z7CFolderD2Ev, .Lfunc_end23-_ZN8NArchive3N7z7CFolderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Ltmp60-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp61-.Ltmp60         #   Call between .Ltmp60 and .Ltmp61
	.long	.Ltmp62-.Lfunc_begin3   #     jumps to .Ltmp62
	.byte	0                       #   On action: cleanup
	.long	.Ltmp65-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp66-.Ltmp65         #   Call between .Ltmp65 and .Ltmp66
	.long	.Ltmp67-.Lfunc_begin3   #     jumps to .Ltmp67
	.byte	0                       #   On action: cleanup
	.long	.Ltmp70-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp71-.Ltmp70         #   Call between .Ltmp70 and .Ltmp71
	.long	.Ltmp72-.Lfunc_begin3   #     jumps to .Ltmp72
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin3   #     jumps to .Ltmp84
	.byte	0                       #   On action: cleanup
	.long	.Ltmp83-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp85-.Ltmp83         #   Call between .Ltmp83 and .Ltmp85
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin3   #     jumps to .Ltmp87
	.byte	1                       #   On action: 1
	.long	.Ltmp63-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp69-.Ltmp63         #   Call between .Ltmp63 and .Ltmp69
	.long	.Ltmp81-.Lfunc_begin3   #     jumps to .Ltmp81
	.byte	1                       #   On action: 1
	.long	.Ltmp73-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Ltmp74-.Ltmp73         #   Call between .Ltmp73 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin3   #     jumps to .Ltmp75
	.byte	1                       #   On action: 1
	.long	.Ltmp79-.Lfunc_begin3   # >> Call Site 9 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin3   #     jumps to .Ltmp81
	.byte	1                       #   On action: 1
	.long	.Ltmp80-.Lfunc_begin3   # >> Call Site 10 <<
	.long	.Ltmp76-.Ltmp80         #   Call between .Ltmp80 and .Ltmp76
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp76-.Lfunc_begin3   # >> Call Site 11 <<
	.long	.Ltmp77-.Ltmp76         #   Call between .Ltmp76 and .Ltmp77
	.long	.Ltmp78-.Lfunc_begin3   #     jumps to .Ltmp78
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3N7z11COutArchive11WriteHeaderERKNS0_16CArchiveDatabaseERKNS0_14CHeaderOptionsERy
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive11WriteHeaderERKNS0_16CArchiveDatabaseERKNS0_14CHeaderOptionsERy,@function
_ZN8NArchive3N7z11COutArchive11WriteHeaderERKNS0_16CArchiveDatabaseERKNS0_14CHeaderOptionsERy: # @_ZN8NArchive3N7z11COutArchive11WriteHeaderERKNS0_16CArchiveDatabaseERKNS0_14CHeaderOptionsERy
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi207:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi208:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi209:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi210:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi211:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi212:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi213:
	.cfi_def_cfa_offset 320
.Lcfi214:
	.cfi_offset %rbx, -56
.Lcfi215:
	.cfi_offset %r12, -48
.Lcfi216:
	.cfi_offset %r13, -40
.Lcfi217:
	.cfi_offset %r14, -32
.Lcfi218:
	.cfi_offset %r15, -24
.Lcfi219:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r13
	movq	%rdi, %r12
	movslq	12(%r13), %rax
	testq	%rax, %rax
	jle	.LBB24_8
# BB#1:                                 # %.lr.ph343
	movq	16(%r13), %rdx
	cmpl	$4, %eax
	jb	.LBB24_6
# BB#2:                                 # %min.iters.checked
	movq	%rax, %rsi
	andq	$-4, %rsi
	je	.LBB24_6
# BB#3:                                 # %vector.body.preheader
	leaq	-4(%rsi), %rbx
	movl	%ebx, %edi
	shrl	$2, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB24_9
# BB#4:                                 # %vector.body.prol.preheader
	negq	%rdi
	pxor	%xmm0, %xmm0
	xorl	%ebp, %ebp
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB24_5:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%rdx,%rbp,8), %xmm2
	movdqu	16(%rdx,%rbp,8), %xmm3
	paddq	%xmm2, %xmm0
	paddq	%xmm3, %xmm1
	addq	$4, %rbp
	incq	%rdi
	jne	.LBB24_5
	jmp	.LBB24_10
.LBB24_6:
	xorl	%esi, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB24_7:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	addq	(%rdx,%rsi,8), %rdi
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB24_7
	jmp	.LBB24_14
.LBB24_8:
	xorl	%edi, %edi
	jmp	.LBB24_14
.LBB24_9:
	xorl	%ebp, %ebp
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
.LBB24_10:                              # %vector.body.prol.loopexit
	cmpq	$12, %rbx
	jb	.LBB24_13
# BB#11:                                # %vector.body.preheader.new
	movq	%rsi, %rdi
	subq	%rbp, %rdi
	leaq	112(%rdx,%rbp,8), %rbp
	.p2align	4, 0x90
.LBB24_12:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-112(%rbp), %xmm2
	movdqu	-96(%rbp), %xmm3
	movdqu	-80(%rbp), %xmm4
	movdqu	-64(%rbp), %xmm5
	paddq	%xmm0, %xmm2
	paddq	%xmm1, %xmm3
	movdqu	-48(%rbp), %xmm6
	movdqu	-32(%rbp), %xmm7
	paddq	%xmm4, %xmm6
	paddq	%xmm2, %xmm6
	paddq	%xmm5, %xmm7
	paddq	%xmm3, %xmm7
	movdqu	-16(%rbp), %xmm0
	movdqu	(%rbp), %xmm1
	paddq	%xmm6, %xmm0
	paddq	%xmm7, %xmm1
	subq	$-128, %rbp
	addq	$-16, %rdi
	jne	.LBB24_12
.LBB24_13:                              # %middle.block
	paddq	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddq	%xmm0, %xmm1
	movd	%xmm1, %rdi
	cmpq	%rsi, %rax
	jne	.LBB24_7
.LBB24_14:                              # %._crit_edge344
	movq	%rdi, (%rcx)
	movl	$1, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	cmpl	$0, 108(%r13)
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	%r14, 256(%rsp)         # 8-byte Spill
	jle	.LBB24_28
# BB#15:
	leaq	96(%r13), %rbx
	movl	$4, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	leaq	32(%r13), %rcx
	leaq	64(%r13), %r8
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%r13, %rdx
	callq	_ZN8NArchive3N7z11COutArchive13WritePackInfoEyRK13CRecordVectorIyERKS2_IbERKS2_IjE
	movq	%r12, %rdi
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %rsi
	callq	_ZN8NArchive3N7z11COutArchive15WriteUnpackInfoERK13CObjectVectorINS0_7CFolderEE
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 200(%rsp)
	movq	$8, 216(%rsp)
	movq	$_ZTV13CRecordVectorIyE+16, 192(%rsp)
	movdqu	%xmm0, 168(%rsp)
	movq	$1, 184(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 160(%rsp)
	movdqu	%xmm0, 232(%rsp)
	movq	$4, 248(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 224(%rsp)
	leaq	172(%r13), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	172(%r13), %ecx
	testl	%ecx, %ecx
	jle	.LBB24_23
# BB#16:                                # %.lr.ph338
	xorl	%ebx, %ebx
	leaq	224(%rsp), %r15
	.p2align	4, 0x90
.LBB24_17:                              # =>This Inner Loop Header: Depth=1
	movq	176(%r13), %rax
	movq	(%rax,%rbx,8), %r14
	cmpb	$0, 32(%r14)
	je	.LBB24_22
# BB#18:                                #   in Loop: Header=BB24_17 Depth=1
	movq	(%r14), %r13
.Ltmp88:
	leaq	192(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp89:
# BB#19:                                #   in Loop: Header=BB24_17 Depth=1
	movq	208(%rsp), %rax
	movslq	204(%rsp), %rcx
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 204(%rsp)
	movzbl	34(%r14), %r13d
.Ltmp90:
	leaq	160(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp91:
# BB#20:                                #   in Loop: Header=BB24_17 Depth=1
	movq	176(%rsp), %rax
	movslq	172(%rsp), %rcx
	movb	%r13b, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 172(%rsp)
	movl	12(%r14), %ebp
.Ltmp92:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp93:
# BB#21:                                # %_ZN13CRecordVectorIjE3AddEj.exit
                                        #   in Loop: Header=BB24_17 Depth=1
	movq	240(%rsp), %rax
	movslq	236(%rsp), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	236(%rsp)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %ecx
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB24_22:                              #   in Loop: Header=BB24_17 Depth=1
	incq	%rbx
	movslq	%ecx, %rax
	cmpq	%rax, %rbx
	jl	.LBB24_17
.LBB24_23:                              # %._crit_edge339
	movq	%r13, %rdx
	subq	$-128, %rdx
.Ltmp95:
	leaq	192(%rsp), %rcx
	leaq	160(%rsp), %r8
	leaq	224(%rsp), %r15
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %r9
	callq	_ZN8NArchive3N7z11COutArchive19WriteSubStreamsInfoERK13CObjectVectorINS0_7CFolderEERK13CRecordVectorIjERKS7_IyERKS7_IbESA_
.Ltmp96:
# BB#24:
.Ltmp97:
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp98:
# BB#25:
.Ltmp102:
	leaq	224(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp103:
# BB#26:
.Ltmp107:
	leaq	160(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp108:
# BB#27:
	leaq	192(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	movq	8(%rsp), %rbx           # 8-byte Reload
	cmpl	$0, (%rbx)
	jne	.LBB24_29
	jmp	.LBB24_194
.LBB24_28:                              # %._crit_edge344._crit_edge
	leaq	172(%r13), %rbx
	cmpl	$0, (%rbx)
	je	.LBB24_194
.LBB24_29:
	movl	$5, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movslq	(%rbx), %rbp
	xorl	%ebx, %ebx
	cmpq	$128, %rbp
	movl	$0, %eax
	jb	.LBB24_38
# BB#30:
	cmpl	$16383, %ebp            # imm = 0x3FFF
	ja	.LBB24_32
# BB#31:
	movb	$-128, %al
	movl	$1, %ebx
	jmp	.LBB24_38
.LBB24_32:
	cmpl	$2097152, %ebp          # imm = 0x200000
	jae	.LBB24_34
# BB#33:
	movb	$-64, %al
	movl	$2, %ebx
	jmp	.LBB24_38
.LBB24_34:
	cmpl	$268435456, %ebp        # imm = 0x10000000
	jae	.LBB24_36
# BB#35:
	movb	$-32, %al
	movl	$3, %ebx
	jmp	.LBB24_38
.LBB24_36:
	testl	%ebp, %ebp
	js	.LBB24_94
# BB#37:
	movb	$-16, %al
	movl	$4, %ebx
.LBB24_38:                              # %.loopexit.i
	movl	%ebx, %ecx
	shlb	$3, %cl
	movq	%rbp, %rdx
	shrq	%cl, %rdx
	orb	%al, %dl
	movzbl	%dl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	testl	%ebx, %ebx
	je	.LBB24_41
.LBB24_39:                              # %.lr.ph.i.preheader
	incl	%ebx
	.p2align	4, 0x90
.LBB24_40:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%bpl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	shrq	$8, %rbp
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB24_40
.LBB24_41:                              # %_ZN8NArchive3N7z11COutArchive11WriteNumberEy.exit
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 40(%rsp)
	movq	$1, 56(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 32(%rsp)
	movq	8(%rsp), %r14           # 8-byte Reload
	movl	(%r14), %esi
.Ltmp112:
	leaq	32(%rsp), %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp113:
# BB#42:                                # %.preheader297
	cmpl	$0, (%r14)
	jle	.LBB24_140
# BB#43:                                # %.lr.ph333
	xorl	%ebx, %ebx
	leaq	32(%rsp), %rbp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB24_44:                              # =>This Inner Loop Header: Depth=1
	movq	176(%r13), %rax
	movq	(%rax,%rbx,8), %rax
	cmpb	$0, 32(%rax)
	je	.LBB24_47
# BB#45:                                #   in Loop: Header=BB24_44 Depth=1
.Ltmp115:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp116:
# BB#46:                                # %_ZN13CRecordVectorIbE3AddEb.exit199
                                        #   in Loop: Header=BB24_44 Depth=1
	movq	48(%rsp), %rax
	movslq	44(%rsp), %rcx
	movb	$0, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 44(%rsp)
	jmp	.LBB24_49
	.p2align	4, 0x90
.LBB24_47:                              #   in Loop: Header=BB24_44 Depth=1
.Ltmp117:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp118:
# BB#48:                                #   in Loop: Header=BB24_44 Depth=1
	movq	48(%rsp), %rax
	movslq	44(%rsp), %rcx
	movb	$1, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 44(%rsp)
	incl	%r15d
.LBB24_49:                              #   in Loop: Header=BB24_44 Depth=1
	incq	%rbx
	movslq	(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB24_44
# BB#50:                                # %._crit_edge334
	testl	%r15d, %r15d
	jle	.LBB24_140
# BB#51:
.Ltmp120:
	movl	$14, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp121:
# BB#52:
	movl	44(%rsp), %ecx
	addl	$7, %ecx
	movl	%ecx, %ebp
	shrl	$3, %ebp
	xorl	%ebx, %ebx
	cmpl	$1024, %ecx             # imm = 0x400
	movl	$0, %eax
	jb	.LBB24_60
# BB#53:
	cmpl	$131071, %ecx           # imm = 0x1FFFF
	ja	.LBB24_55
# BB#54:
	movb	$-128, %al
	movl	$1, %ebx
	jmp	.LBB24_60
.LBB24_55:
	cmpl	$16777216, %ecx         # imm = 0x1000000
	jae	.LBB24_57
# BB#56:
	movb	$-64, %al
	movl	$2, %ebx
	jmp	.LBB24_60
.LBB24_57:
	movl	%ecx, %ebx
	shrl	$31, %ebx
	testl	%ecx, %ecx
	movb	$-32, %al
	jns	.LBB24_59
# BB#58:
	movb	$-16, %al
.LBB24_59:
	addl	$3, %ebx
.LBB24_60:                              # %.loopexit.i204
	movl	%ebx, %ecx
	shlb	$3, %cl
	movq	%rbp, %rdx
	shrq	%cl, %rdx
	orb	%al, %dl
	movzbl	%dl, %esi
.Ltmp122:
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp123:
# BB#61:                                # %.noexc208
	testl	%ebx, %ebx
	je	.LBB24_65
# BB#62:                                # %.lr.ph.i207.preheader
	incl	%ebx
	.p2align	4, 0x90
.LBB24_63:                              # %.lr.ph.i207
                                        # =>This Inner Loop Header: Depth=1
.Ltmp124:
	movzbl	%bpl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp125:
# BB#64:                                # %.noexc209
                                        #   in Loop: Header=BB24_63 Depth=1
	shrq	$8, %rbp
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB24_63
.LBB24_65:                              # %_ZN8NArchive3N7z11COutArchive11WriteNumberEy.exit210
	movl	44(%rsp), %esi
	testl	%esi, %esi
	jle	.LBB24_75
# BB#66:                                # %.lr.ph.i211
	movb	$-128, %al
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB24_67:                              # =>This Inner Loop Header: Depth=1
	movq	48(%rsp), %rdx
	movzbl	(%rdx,%rbx), %edx
	testb	%dl, %dl
	je	.LBB24_69
# BB#68:                                #   in Loop: Header=BB24_67 Depth=1
	movl	%eax, %edx
.LBB24_69:                              #   in Loop: Header=BB24_67 Depth=1
	orb	%dl, %cl
	shrb	%al
	jne	.LBB24_72
# BB#70:                                #   in Loop: Header=BB24_67 Depth=1
.Ltmp127:
	movzbl	%cl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp128:
# BB#71:                                # %.noexc212
                                        #   in Loop: Header=BB24_67 Depth=1
	movb	$-128, %al
	movl	44(%rsp), %esi
	xorl	%ecx, %ecx
.LBB24_72:                              #   in Loop: Header=BB24_67 Depth=1
	incq	%rbx
	movslq	%esi, %rdx
	cmpq	%rdx, %rbx
	jl	.LBB24_67
# BB#73:                                # %._crit_edge.i
	cmpb	$-128, %al
	je	.LBB24_75
# BB#74:
.Ltmp130:
	movzbl	%cl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp131:
.LBB24_75:                              # %_ZN8NArchive3N7z11COutArchive15WriteBoolVectorERK13CRecordVectorIbE.exit
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 104(%rsp)
	movq	$1, 120(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 96(%rsp)
	movdqu	%xmm0, 72(%rsp)
	movq	$1, 88(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 64(%rsp)
.Ltmp133:
	leaq	96(%rsp), %rdi
	movl	%r15d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp134:
# BB#76:
.Ltmp135:
	leaq	64(%rsp), %rdi
	movl	%r15d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp136:
# BB#77:                                # %.preheader289
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.LBB24_138
# BB#78:                                # %.lr.ph327
	xorl	%r15d, %r15d
	leaq	64(%rsp), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB24_79:                              # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	176(%rcx), %rcx
	movq	(%rcx,%r15,8), %rbx
	cmpb	$0, 32(%rbx)
	jne	.LBB24_86
# BB#80:                                #   in Loop: Header=BB24_79 Depth=1
	movzbl	33(%rbx), %ebp
.Ltmp138:
	leaq	96(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp139:
# BB#81:                                #   in Loop: Header=BB24_79 Depth=1
	xorb	$1, %bpl
	movq	112(%rsp), %rax
	movslq	108(%rsp), %rcx
	movb	%bpl, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 108(%rsp)
	movzbl	33(%rbx), %ebx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movslq	460(%rcx), %rax
	cmpq	%r15, %rax
	jle	.LBB24_83
# BB#82:                                #   in Loop: Header=BB24_79 Depth=1
	movq	464(%rcx), %rax
	cmpb	$0, (%rax,%r15)
	setne	%bpl
	jmp	.LBB24_84
.LBB24_83:                              #   in Loop: Header=BB24_79 Depth=1
	xorl	%ebp, %ebp
.LBB24_84:                              # %_ZNK8NArchive3N7z16CArchiveDatabase10IsItemAntiEi.exit
                                        #   in Loop: Header=BB24_79 Depth=1
.Ltmp141:
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp142:
# BB#85:                                #   in Loop: Header=BB24_79 Depth=1
	xorl	$1, %ebx
	addl	%ebx, %r14d
	movq	80(%rsp), %rax
	movslq	76(%rsp), %rcx
	movb	%bpl, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 76(%rsp)
	movzbl	%bpl, %eax
	addl	%eax, %r13d
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %eax
.LBB24_86:                              #   in Loop: Header=BB24_79 Depth=1
	incq	%r15
	movslq	%eax, %rcx
	cmpq	%rcx, %r15
	jl	.LBB24_79
# BB#87:                                # %._crit_edge328
	testl	%r14d, %r14d
	movq	8(%rsp), %r14           # 8-byte Reload
	je	.LBB24_113
# BB#88:
.Ltmp144:
	movl	$15, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp145:
# BB#89:
	movl	108(%rsp), %ecx
	addl	$7, %ecx
	movl	%ecx, %ebp
	shrl	$3, %ebp
	xorl	%ebx, %ebx
	cmpl	$1024, %ecx             # imm = 0x400
	movl	$0, %eax
	jb	.LBB24_98
# BB#90:
	cmpl	$131071, %ecx           # imm = 0x1FFFF
	ja	.LBB24_92
# BB#91:
	movb	$-128, %al
	movl	$1, %ebx
	jmp	.LBB24_98
.LBB24_92:
	cmpl	$16777216, %ecx         # imm = 0x1000000
	jae	.LBB24_95
# BB#93:
	movb	$-64, %al
	movl	$2, %ebx
	jmp	.LBB24_98
.LBB24_94:                              # %.loopexit.thread.i
	movl	$255, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	$8, %ebx
	jmp	.LBB24_39
.LBB24_95:
	movl	%ecx, %ebx
	shrl	$31, %ebx
	testl	%ecx, %ecx
	movb	$-32, %al
	jns	.LBB24_97
# BB#96:
	movb	$-16, %al
.LBB24_97:
	addl	$3, %ebx
.LBB24_98:                              # %.loopexit.i229
	movl	%ebx, %ecx
	shlb	$3, %cl
	movq	%rbp, %rdx
	shrq	%cl, %rdx
	orb	%al, %dl
	movzbl	%dl, %esi
.Ltmp146:
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp147:
# BB#99:                                # %.noexc234
	testl	%ebx, %ebx
	je	.LBB24_103
# BB#100:                               # %.lr.ph.i232.preheader
	incl	%ebx
	.p2align	4, 0x90
.LBB24_101:                             # %.lr.ph.i232
                                        # =>This Inner Loop Header: Depth=1
.Ltmp148:
	movzbl	%bpl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp149:
# BB#102:                               # %.noexc235
                                        #   in Loop: Header=BB24_101 Depth=1
	shrq	$8, %rbp
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB24_101
.LBB24_103:                             # %_ZN8NArchive3N7z11COutArchive11WriteNumberEy.exit236
	movl	108(%rsp), %esi
	testl	%esi, %esi
	jle	.LBB24_113
# BB#104:                               # %.lr.ph.i237
	movb	$-128, %al
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB24_105:                             # =>This Inner Loop Header: Depth=1
	movq	112(%rsp), %rdx
	movzbl	(%rdx,%rbx), %edx
	testb	%dl, %dl
	je	.LBB24_107
# BB#106:                               #   in Loop: Header=BB24_105 Depth=1
	movl	%eax, %edx
.LBB24_107:                             #   in Loop: Header=BB24_105 Depth=1
	orb	%dl, %cl
	shrb	%al
	jne	.LBB24_110
# BB#108:                               #   in Loop: Header=BB24_105 Depth=1
.Ltmp151:
	movzbl	%cl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp152:
# BB#109:                               # %.noexc247
                                        #   in Loop: Header=BB24_105 Depth=1
	movb	$-128, %al
	movl	108(%rsp), %esi
	xorl	%ecx, %ecx
.LBB24_110:                             #   in Loop: Header=BB24_105 Depth=1
	incq	%rbx
	movslq	%esi, %rdx
	cmpq	%rdx, %rbx
	jl	.LBB24_105
# BB#111:                               # %._crit_edge.i238
	cmpb	$-128, %al
	je	.LBB24_113
# BB#112:
.Ltmp154:
	movzbl	%cl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp155:
.LBB24_113:                             # %_ZN8NArchive3N7z11COutArchive15WriteBoolVectorERK13CRecordVectorIbE.exit249
	testl	%r13d, %r13d
	je	.LBB24_138
# BB#114:
.Ltmp156:
	movl	$16, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp157:
# BB#115:
	movl	76(%rsp), %ecx
	addl	$7, %ecx
	movl	%ecx, %ebp
	shrl	$3, %ebp
	xorl	%ebx, %ebx
	cmpl	$1024, %ecx             # imm = 0x400
	movl	$0, %eax
	jb	.LBB24_123
# BB#116:
	cmpl	$131071, %ecx           # imm = 0x1FFFF
	ja	.LBB24_118
# BB#117:
	movb	$-128, %al
	movl	$1, %ebx
	jmp	.LBB24_123
.LBB24_118:
	cmpl	$16777216, %ecx         # imm = 0x1000000
	jae	.LBB24_120
# BB#119:
	movb	$-64, %al
	movl	$2, %ebx
	jmp	.LBB24_123
.LBB24_120:
	movl	%ecx, %ebx
	shrl	$31, %ebx
	testl	%ecx, %ecx
	movb	$-32, %al
	jns	.LBB24_122
# BB#121:
	movb	$-16, %al
.LBB24_122:
	addl	$3, %ebx
.LBB24_123:                             # %.loopexit.i252
	movl	%ebx, %ecx
	shlb	$3, %cl
	movq	%rbp, %rdx
	shrq	%cl, %rdx
	orb	%al, %dl
	movzbl	%dl, %esi
.Ltmp158:
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp159:
# BB#124:                               # %.noexc257
	testl	%ebx, %ebx
	je	.LBB24_128
# BB#125:                               # %.lr.ph.i255.preheader
	incl	%ebx
	.p2align	4, 0x90
.LBB24_126:                             # %.lr.ph.i255
                                        # =>This Inner Loop Header: Depth=1
.Ltmp160:
	movzbl	%bpl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp161:
# BB#127:                               # %.noexc258
                                        #   in Loop: Header=BB24_126 Depth=1
	shrq	$8, %rbp
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB24_126
.LBB24_128:                             # %_ZN8NArchive3N7z11COutArchive11WriteNumberEy.exit259
	movl	76(%rsp), %esi
	testl	%esi, %esi
	jle	.LBB24_138
# BB#129:                               # %.lr.ph.i260
	movb	$-128, %al
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB24_130:                             # =>This Inner Loop Header: Depth=1
	movq	80(%rsp), %rdx
	movzbl	(%rdx,%rbx), %edx
	testb	%dl, %dl
	je	.LBB24_132
# BB#131:                               #   in Loop: Header=BB24_130 Depth=1
	movl	%eax, %edx
.LBB24_132:                             #   in Loop: Header=BB24_130 Depth=1
	orb	%dl, %cl
	shrb	%al
	jne	.LBB24_135
# BB#133:                               #   in Loop: Header=BB24_130 Depth=1
.Ltmp163:
	movzbl	%cl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp164:
# BB#134:                               # %.noexc270
                                        #   in Loop: Header=BB24_130 Depth=1
	movb	$-128, %al
	movl	76(%rsp), %esi
	xorl	%ecx, %ecx
.LBB24_135:                             #   in Loop: Header=BB24_130 Depth=1
	incq	%rbx
	movslq	%esi, %rdx
	cmpq	%rdx, %rbx
	jl	.LBB24_130
# BB#136:                               # %._crit_edge.i261
	cmpb	$-128, %al
	je	.LBB24_138
# BB#137:
.Ltmp166:
	movzbl	%cl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp167:
.LBB24_138:                             # %_ZN8NArchive3N7z11COutArchive15WriteBoolVectorERK13CRecordVectorIbE.exit272
.Ltmp171:
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp172:
# BB#139:
.Ltmp176:
	leaq	96(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp177:
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB24_140:                             # %._crit_edge334.thread
	leaq	32(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	movslq	(%r14), %rax
	testq	%rax, %rax
	jle	.LBB24_171
# BB#141:                               # %.lr.ph320
	movq	176(%r13), %rcx
	testb	$1, %al
	jne	.LBB24_143
# BB#142:
	xorl	%esi, %esi
	xorl	%ebp, %ebp
	xorl	%edx, %edx
	cmpl	$1, %eax
	jne	.LBB24_144
	jmp	.LBB24_145
.LBB24_143:
	movq	(%rcx), %rdx
	movl	24(%rdx), %esi
	xorl	%edx, %edx
	testl	%esi, %esi
	setne	%dl
	leal	2(%rsi,%rsi), %esi
	movslq	%esi, %rbp
	movl	$1, %esi
	cmpl	$1, %eax
	je	.LBB24_145
	.p2align	4, 0x90
.LBB24_144:                             # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rdi
	movl	24(%rdi), %edi
	cmpl	$1, %edi
	sbbl	$-1, %edx
	leal	2(%rdi,%rdi), %edi
	movslq	%edi, %rdi
	addq	%rbp, %rdi
	movq	8(%rcx,%rsi,8), %rbp
	movl	24(%rbp), %ebp
	cmpl	$1, %ebp
	sbbl	$-1, %edx
	leal	2(%rbp,%rbp), %ebp
	movslq	%ebp, %rbp
	addq	%rdi, %rbp
	addq	$2, %rsi
	cmpq	%rax, %rsi
	jl	.LBB24_144
.LBB24_145:                             # %._crit_edge321
	testl	%edx, %edx
	jle	.LBB24_171
# BB#146:
	orq	$1, %rbp
	movl	$17, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	xorl	%ebx, %ebx
	cmpq	$128, %rbp
	movl	$0, %eax
	jb	.LBB24_161
# BB#147:
	cmpq	$16383, %rbp            # imm = 0x3FFF
	ja	.LBB24_149
# BB#148:
	movb	$-128, %al
	movl	$1, %ebx
	jmp	.LBB24_161
.LBB24_149:
	cmpq	$2097152, %rbp          # imm = 0x200000
	jae	.LBB24_151
# BB#150:
	movb	$-64, %al
	movl	$2, %ebx
	jmp	.LBB24_161
.LBB24_151:
	cmpq	$268435456, %rbp        # imm = 0x10000000
	jae	.LBB24_153
# BB#152:
	movb	$-32, %al
	movl	$3, %ebx
	jmp	.LBB24_161
.LBB24_153:
	movq	%rbp, %rax
	shrq	$35, %rax
	jne	.LBB24_155
# BB#154:
	movb	$-16, %al
	movl	$4, %ebx
	jmp	.LBB24_161
.LBB24_155:
	movq	%rbp, %rax
	shrq	$42, %rax
	jne	.LBB24_157
# BB#156:
	movb	$-8, %al
	movl	$5, %ebx
	jmp	.LBB24_161
.LBB24_157:
	movq	%rbp, %rax
	shrq	$49, %rax
	jne	.LBB24_159
# BB#158:
	movb	$-4, %al
	movl	$6, %ebx
	jmp	.LBB24_161
.LBB24_159:
	movq	%rbp, %rax
	shrq	$56, %rax
	jne	.LBB24_195
# BB#160:
	movb	$-2, %al
	movl	$7, %ebx
.LBB24_161:                             # %.loopexit.i220
	movl	%ebx, %ecx
	shlb	$3, %cl
	movq	%rbp, %rdx
	shrq	%cl, %rdx
	orb	%al, %dl
	movzbl	%dl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	testl	%ebx, %ebx
	movq	8(%rsp), %r14           # 8-byte Reload
	je	.LBB24_164
.LBB24_162:                             # %.lr.ph.i223.preheader
	incl	%ebx
	.p2align	4, 0x90
.LBB24_163:                             # %.lr.ph.i223
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%bpl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	shrq	$8, %rbp
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB24_163
.LBB24_164:                             # %_ZN8NArchive3N7z11COutArchive11WriteNumberEy.exit226
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.LBB24_171
# BB#165:                               # %.lr.ph315
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB24_166:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_168 Depth 2
	movq	176(%r13), %rcx
	movq	(%rcx,%r14,8), %r15
	cmpl	$0, 24(%r15)
	js	.LBB24_170
# BB#167:                               # %.lr.ph312
                                        #   in Loop: Header=BB24_166 Depth=1
	movq	$-1, %rbx
	.p2align	4, 0x90
.LBB24_168:                             #   Parent Loop BB24_166 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r15), %rax
	movl	4(%rax,%rbx,4), %eax
	movzbl	%al, %esi
	movl	%eax, %ebp
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	%ebp, %eax
	movzbl	%ah, %esi  # NOREX
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movslq	24(%r15), %rax
	incq	%rbx
	cmpq	%rax, %rbx
	jl	.LBB24_168
# BB#169:                               # %._crit_edge313.loopexit
                                        #   in Loop: Header=BB24_166 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %eax
.LBB24_170:                             # %._crit_edge313
                                        #   in Loop: Header=BB24_166 Depth=1
	incq	%r14
	movslq	%eax, %rcx
	cmpq	%rcx, %r14
	jl	.LBB24_166
.LBB24_171:                             # %.loopexit279
	movq	256(%rsp), %rbx         # 8-byte Reload
	cmpb	$0, 1(%rbx)
	je	.LBB24_173
# BB#172:
	leaq	192(%r13), %rsi
	movl	$18, %edx
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive20WriteUInt64DefVectorERKNS0_16CUInt64DefVectorEh
.LBB24_173:
	cmpb	$0, 2(%rbx)
	je	.LBB24_175
# BB#174:
	leaq	256(%r13), %rsi
	movl	$19, %edx
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive20WriteUInt64DefVectorERKNS0_16CUInt64DefVectorEh
.LBB24_175:
	cmpb	$0, 3(%rbx)
	movq	8(%rsp), %rbx           # 8-byte Reload
	je	.LBB24_177
# BB#176:
	leaq	320(%r13), %rsi
	movl	$20, %edx
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive20WriteUInt64DefVectorERKNS0_16CUInt64DefVectorEh
.LBB24_177:
	leaq	384(%r13), %rsi
	movl	$24, %edx
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive20WriteUInt64DefVectorERKNS0_16CUInt64DefVectorEh
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 136(%rsp)
	movq	$1, 152(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 128(%rsp)
	movl	172(%r13), %esi
.Ltmp181:
	leaq	128(%rsp), %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp182:
# BB#178:                               # %.preheader278
	cmpl	$0, (%rbx)
	jle	.LBB24_193
# BB#179:                               # %.lr.ph309
	xorl	%ebx, %ebx
	leaq	128(%rsp), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB24_180:                             # =>This Inner Loop Header: Depth=1
	movq	176(%r13), %rax
	movq	(%rax,%rbx,8), %rax
	movzbl	35(%rax), %r14d
.Ltmp184:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp185:
# BB#181:                               #   in Loop: Header=BB24_180 Depth=1
	movzbl	%r14b, %eax
	movq	144(%rsp), %rcx
	movslq	140(%rsp), %rdx
	movb	%al, (%rcx,%rdx)
	leal	1(%rdx), %ecx
	movl	%ecx, 140(%rsp)
	addl	%eax, %ebp
	incq	%rbx
	movq	8(%rsp), %r14           # 8-byte Reload
	movslq	(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB24_180
# BB#182:                               # %._crit_edge
	testl	%ebp, %ebp
	jle	.LBB24_193
# BB#183:
.Ltmp187:
	leaq	128(%rsp), %rsi
	movl	$21, %ecx
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	%ebp, %edx
	callq	_ZN8NArchive3N7z11COutArchive22WriteAlignedBoolHeaderERK13CRecordVectorIbEihj
.Ltmp188:
# BB#184:                               # %.preheader
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jle	.LBB24_193
# BB#185:                               # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB24_186:                             # =>This Inner Loop Header: Depth=1
	movq	176(%r13), %rax
	movq	(%rax,%rbp,8), %rax
	cmpb	$0, 35(%rax)
	je	.LBB24_192
# BB#187:                               #   in Loop: Header=BB24_186 Depth=1
	movl	8(%rax), %ebx
.Ltmp190:
	movzbl	%bl, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp191:
# BB#188:                               # %.noexc
                                        #   in Loop: Header=BB24_186 Depth=1
.Ltmp192:
	movzbl	%bh, %esi  # NOREX
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp193:
# BB#189:                               # %.noexc190
                                        #   in Loop: Header=BB24_186 Depth=1
	movl	%ebx, %eax
	shrl	$16, %eax
.Ltmp194:
	movzbl	%al, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp195:
# BB#190:                               # %.noexc191
                                        #   in Loop: Header=BB24_186 Depth=1
	shrl	$24, %ebx
.Ltmp196:
	movq	%r12, %rdi
	movl	%ebx, %esi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp197:
# BB#191:                               # %.noexc191._ZN8NArchive3N7z11COutArchive11WriteUInt32Ej.exit_crit_edge
                                        #   in Loop: Header=BB24_186 Depth=1
	movl	(%r14), %ecx
.LBB24_192:                             # %_ZN8NArchive3N7z11COutArchive11WriteUInt32Ej.exit
                                        #   in Loop: Header=BB24_186 Depth=1
	incq	%rbp
	movslq	%ecx, %rax
	cmpq	%rax, %rbp
	jl	.LBB24_186
.LBB24_193:                             # %.loopexit
	leaq	128(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.LBB24_194:
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB24_195:                             # %.loopexit.thread.i225
	movl	$255, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
	movl	$8, %ebx
	movq	8(%rsp), %r14           # 8-byte Reload
	jmp	.LBB24_162
.LBB24_196:
.Ltmp189:
	jmp	.LBB24_220
.LBB24_197:
.Ltmp178:
	jmp	.LBB24_222
.LBB24_198:
.Ltmp173:
	movq	%rax, %rbx
	jmp	.LBB24_216
.LBB24_199:                             # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp
.Ltmp168:
	jmp	.LBB24_214
.LBB24_200:
.Ltmp137:
	movq	%rax, %rbx
	leaq	64(%rsp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB24_215
.LBB24_201:                             # %.loopexit.split-lp291.loopexit.split-lp.loopexit.split-lp
.Ltmp132:
	jmp	.LBB24_222
.LBB24_202:
.Ltmp183:
	jmp	.LBB24_220
.LBB24_203:
.Ltmp114:
	jmp	.LBB24_222
.LBB24_204:
.Ltmp109:
	movq	%rax, %rbx
	jmp	.LBB24_227
.LBB24_205:
.Ltmp104:
	movq	%rax, %rbx
	jmp	.LBB24_226
.LBB24_206:                             # %.loopexit280
.Ltmp165:
	jmp	.LBB24_214
.LBB24_207:                             # %.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp153:
	jmp	.LBB24_214
.LBB24_208:
.Ltmp99:
	jmp	.LBB24_225
.LBB24_209:
.Ltmp140:
	jmp	.LBB24_214
.LBB24_210:
.Ltmp143:
	jmp	.LBB24_214
.LBB24_211:                             # %.loopexit290
.Ltmp129:
	jmp	.LBB24_222
.LBB24_212:                             # %.loopexit.split-lp.loopexit
.Ltmp162:
	jmp	.LBB24_214
.LBB24_213:                             # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp150:
.LBB24_214:                             # %.loopexit.split-lp
	movq	%rax, %rbx
.LBB24_215:                             # %.loopexit.split-lp
.Ltmp169:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp170:
.LBB24_216:
.Ltmp174:
	leaq	96(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp175:
	jmp	.LBB24_223
.LBB24_217:                             # %.loopexit.split-lp291.loopexit
.Ltmp126:
	jmp	.LBB24_222
.LBB24_218:
.Ltmp198:
	jmp	.LBB24_220
.LBB24_219:
.Ltmp186:
.LBB24_220:
	movq	%rax, %rbx
.Ltmp199:
	leaq	128(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp200:
	jmp	.LBB24_228
.LBB24_221:                             # %.loopexit.split-lp291.loopexit.split-lp.loopexit
.Ltmp119:
.LBB24_222:
	movq	%rax, %rbx
.LBB24_223:
.Ltmp179:
	leaq	32(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp180:
	jmp	.LBB24_228
.LBB24_224:
.Ltmp94:
.LBB24_225:
	movq	%rax, %rbx
.Ltmp100:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp101:
.LBB24_226:
.Ltmp105:
	leaq	160(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp106:
.LBB24_227:
.Ltmp110:
	leaq	192(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp111:
.LBB24_228:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB24_229:
.Ltmp201:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end24:
	.size	_ZN8NArchive3N7z11COutArchive11WriteHeaderERKNS0_16CArchiveDatabaseERKNS0_14CHeaderOptionsERy, .Lfunc_end24-_ZN8NArchive3N7z11COutArchive11WriteHeaderERKNS0_16CArchiveDatabaseERKNS0_14CHeaderOptionsERy
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table24:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\203"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\272\003"              # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp88-.Lfunc_begin4   #   Call between .Lfunc_begin4 and .Ltmp88
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp88-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp93-.Ltmp88         #   Call between .Ltmp88 and .Ltmp93
	.long	.Ltmp94-.Lfunc_begin4   #     jumps to .Ltmp94
	.byte	0                       #   On action: cleanup
	.long	.Ltmp95-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp98-.Ltmp95         #   Call between .Ltmp95 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin4   #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp102-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Ltmp103-.Ltmp102       #   Call between .Ltmp102 and .Ltmp103
	.long	.Ltmp104-.Lfunc_begin4  #     jumps to .Ltmp104
	.byte	0                       #   On action: cleanup
	.long	.Ltmp107-.Lfunc_begin4  # >> Call Site 5 <<
	.long	.Ltmp108-.Ltmp107       #   Call between .Ltmp107 and .Ltmp108
	.long	.Ltmp109-.Lfunc_begin4  #     jumps to .Ltmp109
	.byte	0                       #   On action: cleanup
	.long	.Ltmp108-.Lfunc_begin4  # >> Call Site 6 <<
	.long	.Ltmp112-.Ltmp108       #   Call between .Ltmp108 and .Ltmp112
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp112-.Lfunc_begin4  # >> Call Site 7 <<
	.long	.Ltmp113-.Ltmp112       #   Call between .Ltmp112 and .Ltmp113
	.long	.Ltmp114-.Lfunc_begin4  #     jumps to .Ltmp114
	.byte	0                       #   On action: cleanup
	.long	.Ltmp115-.Lfunc_begin4  # >> Call Site 8 <<
	.long	.Ltmp118-.Ltmp115       #   Call between .Ltmp115 and .Ltmp118
	.long	.Ltmp119-.Lfunc_begin4  #     jumps to .Ltmp119
	.byte	0                       #   On action: cleanup
	.long	.Ltmp120-.Lfunc_begin4  # >> Call Site 9 <<
	.long	.Ltmp123-.Ltmp120       #   Call between .Ltmp120 and .Ltmp123
	.long	.Ltmp132-.Lfunc_begin4  #     jumps to .Ltmp132
	.byte	0                       #   On action: cleanup
	.long	.Ltmp124-.Lfunc_begin4  # >> Call Site 10 <<
	.long	.Ltmp125-.Ltmp124       #   Call between .Ltmp124 and .Ltmp125
	.long	.Ltmp126-.Lfunc_begin4  #     jumps to .Ltmp126
	.byte	0                       #   On action: cleanup
	.long	.Ltmp127-.Lfunc_begin4  # >> Call Site 11 <<
	.long	.Ltmp128-.Ltmp127       #   Call between .Ltmp127 and .Ltmp128
	.long	.Ltmp129-.Lfunc_begin4  #     jumps to .Ltmp129
	.byte	0                       #   On action: cleanup
	.long	.Ltmp130-.Lfunc_begin4  # >> Call Site 12 <<
	.long	.Ltmp131-.Ltmp130       #   Call between .Ltmp130 and .Ltmp131
	.long	.Ltmp132-.Lfunc_begin4  #     jumps to .Ltmp132
	.byte	0                       #   On action: cleanup
	.long	.Ltmp133-.Lfunc_begin4  # >> Call Site 13 <<
	.long	.Ltmp136-.Ltmp133       #   Call between .Ltmp133 and .Ltmp136
	.long	.Ltmp137-.Lfunc_begin4  #     jumps to .Ltmp137
	.byte	0                       #   On action: cleanup
	.long	.Ltmp138-.Lfunc_begin4  # >> Call Site 14 <<
	.long	.Ltmp139-.Ltmp138       #   Call between .Ltmp138 and .Ltmp139
	.long	.Ltmp140-.Lfunc_begin4  #     jumps to .Ltmp140
	.byte	0                       #   On action: cleanup
	.long	.Ltmp141-.Lfunc_begin4  # >> Call Site 15 <<
	.long	.Ltmp142-.Ltmp141       #   Call between .Ltmp141 and .Ltmp142
	.long	.Ltmp143-.Lfunc_begin4  #     jumps to .Ltmp143
	.byte	0                       #   On action: cleanup
	.long	.Ltmp144-.Lfunc_begin4  # >> Call Site 16 <<
	.long	.Ltmp145-.Ltmp144       #   Call between .Ltmp144 and .Ltmp145
	.long	.Ltmp168-.Lfunc_begin4  #     jumps to .Ltmp168
	.byte	0                       #   On action: cleanup
	.long	.Ltmp145-.Lfunc_begin4  # >> Call Site 17 <<
	.long	.Ltmp146-.Ltmp145       #   Call between .Ltmp145 and .Ltmp146
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp146-.Lfunc_begin4  # >> Call Site 18 <<
	.long	.Ltmp147-.Ltmp146       #   Call between .Ltmp146 and .Ltmp147
	.long	.Ltmp168-.Lfunc_begin4  #     jumps to .Ltmp168
	.byte	0                       #   On action: cleanup
	.long	.Ltmp148-.Lfunc_begin4  # >> Call Site 19 <<
	.long	.Ltmp149-.Ltmp148       #   Call between .Ltmp148 and .Ltmp149
	.long	.Ltmp150-.Lfunc_begin4  #     jumps to .Ltmp150
	.byte	0                       #   On action: cleanup
	.long	.Ltmp151-.Lfunc_begin4  # >> Call Site 20 <<
	.long	.Ltmp152-.Ltmp151       #   Call between .Ltmp151 and .Ltmp152
	.long	.Ltmp153-.Lfunc_begin4  #     jumps to .Ltmp153
	.byte	0                       #   On action: cleanup
	.long	.Ltmp154-.Lfunc_begin4  # >> Call Site 21 <<
	.long	.Ltmp159-.Ltmp154       #   Call between .Ltmp154 and .Ltmp159
	.long	.Ltmp168-.Lfunc_begin4  #     jumps to .Ltmp168
	.byte	0                       #   On action: cleanup
	.long	.Ltmp160-.Lfunc_begin4  # >> Call Site 22 <<
	.long	.Ltmp161-.Ltmp160       #   Call between .Ltmp160 and .Ltmp161
	.long	.Ltmp162-.Lfunc_begin4  #     jumps to .Ltmp162
	.byte	0                       #   On action: cleanup
	.long	.Ltmp163-.Lfunc_begin4  # >> Call Site 23 <<
	.long	.Ltmp164-.Ltmp163       #   Call between .Ltmp163 and .Ltmp164
	.long	.Ltmp165-.Lfunc_begin4  #     jumps to .Ltmp165
	.byte	0                       #   On action: cleanup
	.long	.Ltmp166-.Lfunc_begin4  # >> Call Site 24 <<
	.long	.Ltmp167-.Ltmp166       #   Call between .Ltmp166 and .Ltmp167
	.long	.Ltmp168-.Lfunc_begin4  #     jumps to .Ltmp168
	.byte	0                       #   On action: cleanup
	.long	.Ltmp171-.Lfunc_begin4  # >> Call Site 25 <<
	.long	.Ltmp172-.Ltmp171       #   Call between .Ltmp171 and .Ltmp172
	.long	.Ltmp173-.Lfunc_begin4  #     jumps to .Ltmp173
	.byte	0                       #   On action: cleanup
	.long	.Ltmp176-.Lfunc_begin4  # >> Call Site 26 <<
	.long	.Ltmp177-.Ltmp176       #   Call between .Ltmp176 and .Ltmp177
	.long	.Ltmp178-.Lfunc_begin4  #     jumps to .Ltmp178
	.byte	0                       #   On action: cleanup
	.long	.Ltmp177-.Lfunc_begin4  # >> Call Site 27 <<
	.long	.Ltmp181-.Ltmp177       #   Call between .Ltmp177 and .Ltmp181
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp181-.Lfunc_begin4  # >> Call Site 28 <<
	.long	.Ltmp182-.Ltmp181       #   Call between .Ltmp181 and .Ltmp182
	.long	.Ltmp183-.Lfunc_begin4  #     jumps to .Ltmp183
	.byte	0                       #   On action: cleanup
	.long	.Ltmp184-.Lfunc_begin4  # >> Call Site 29 <<
	.long	.Ltmp185-.Ltmp184       #   Call between .Ltmp184 and .Ltmp185
	.long	.Ltmp186-.Lfunc_begin4  #     jumps to .Ltmp186
	.byte	0                       #   On action: cleanup
	.long	.Ltmp187-.Lfunc_begin4  # >> Call Site 30 <<
	.long	.Ltmp188-.Ltmp187       #   Call between .Ltmp187 and .Ltmp188
	.long	.Ltmp189-.Lfunc_begin4  #     jumps to .Ltmp189
	.byte	0                       #   On action: cleanup
	.long	.Ltmp190-.Lfunc_begin4  # >> Call Site 31 <<
	.long	.Ltmp197-.Ltmp190       #   Call between .Ltmp190 and .Ltmp197
	.long	.Ltmp198-.Lfunc_begin4  #     jumps to .Ltmp198
	.byte	0                       #   On action: cleanup
	.long	.Ltmp197-.Lfunc_begin4  # >> Call Site 32 <<
	.long	.Ltmp169-.Ltmp197       #   Call between .Ltmp197 and .Ltmp169
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp169-.Lfunc_begin4  # >> Call Site 33 <<
	.long	.Ltmp111-.Ltmp169       #   Call between .Ltmp169 and .Ltmp111
	.long	.Ltmp201-.Lfunc_begin4  #     jumps to .Ltmp201
	.byte	1                       #   On action: 1
	.long	.Ltmp111-.Lfunc_begin4  # >> Call Site 34 <<
	.long	.Lfunc_end24-.Ltmp111   #   Call between .Ltmp111 and .Lfunc_end24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3N7z11COutArchive13WriteDatabaseERKNS0_16CArchiveDatabaseEPKNS0_22CCompressionMethodModeERKNS0_14CHeaderOptionsE
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchive13WriteDatabaseERKNS0_16CArchiveDatabaseEPKNS0_22CCompressionMethodModeERKNS0_14CHeaderOptionsE,@function
_ZN8NArchive3N7z11COutArchive13WriteDatabaseERKNS0_16CArchiveDatabaseEPKNS0_22CCompressionMethodModeERKNS0_14CHeaderOptionsE: # @_ZN8NArchive3N7z11COutArchive13WriteDatabaseERKNS0_16CArchiveDatabaseEPKNS0_22CCompressionMethodModeERKNS0_14CHeaderOptionsE
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi220:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi221:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi222:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi223:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi224:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi225:
	.cfi_def_cfa_offset 56
	subq	$712, %rsp              # imm = 0x2C8
.Lcfi226:
	.cfi_def_cfa_offset 768
.Lcfi227:
	.cfi_offset %rbx, -56
.Lcfi228:
	.cfi_offset %r12, -48
.Lcfi229:
	.cfi_offset %r13, -40
.Lcfi230:
	.cfi_offset %r14, -32
.Lcfi231:
	.cfi_offset %r15, -24
.Lcfi232:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r12
	movl	172(%r14), %eax
	movl	236(%r14), %ecx
	cmpl	%eax, %ecx
	movl	$-2147467259, %ebp      # imm = 0x80004005
	je	.LBB25_2
# BB#1:
	testl	%ecx, %ecx
	jne	.LBB25_73
.LBB25_2:
	movl	300(%r14), %ecx
	cmpl	%eax, %ecx
	je	.LBB25_4
# BB#3:
	testl	%ecx, %ecx
	jne	.LBB25_73
.LBB25_4:
	movl	364(%r14), %ecx
	cmpl	%eax, %ecx
	je	.LBB25_6
# BB#5:
	testl	%ecx, %ecx
	jne	.LBB25_73
.LBB25_6:
	movl	428(%r14), %ecx
	cmpl	%eax, %ecx
	je	.LBB25_8
# BB#7:
	testl	%ecx, %ecx
	jne	.LBB25_73
.LBB25_8:                               # %_ZNK8NArchive3N7z16CArchiveDatabase13CheckNumFilesEv.exit
	movl	460(%r14), %ecx
	cmpl	%ecx, %eax
	je	.LBB25_10
# BB#9:                                 # %_ZNK8NArchive3N7z16CArchiveDatabase13CheckNumFilesEv.exit
	testl	%ecx, %ecx
	jne	.LBB25_73
.LBB25_10:
	cmpl	$0, 12(%r14)
	jne	.LBB25_15
# BB#11:
	cmpl	$0, 44(%r14)
	jne	.LBB25_15
# BB#12:
	cmpl	$0, 76(%r14)
	jne	.LBB25_15
# BB#13:
	cmpl	$0, 108(%r14)
	jne	.LBB25_15
# BB#14:
	orl	140(%r14), %eax
	je	.LBB25_74
.LBB25_15:                              # %_ZNK8NArchive3N7z16CArchiveDatabase7IsEmptyEv.exit.thread
	testq	%rbx, %rbx
	je	.LBB25_21
# BB#16:
	movb	68(%rbx), %al
	cmpl	$0, 12(%rbx)
	je	.LBB25_19
# BB#17:                                # %_ZNK8NArchive3N7z22CCompressionMethodMode7IsEmptyEv.exit.thread
	testb	%al, %al
	jne	.LBB25_20
# BB#18:
	cmpb	$0, (%r13)
	jne	.LBB25_20
	jmp	.LBB25_22
.LBB25_19:                              # %_ZNK8NArchive3N7z22CCompressionMethodMode7IsEmptyEv.exit
	testb	%al, %al
	je	.LBB25_21
.LBB25_20:                              # %_ZNK8NArchive3N7z22CCompressionMethodMode7IsEmptyEv.exit.thread.thread
	movb	$1, %bpl
	jmp	.LBB25_23
.LBB25_21:
	xorl	%ebx, %ebx
.LBB25_22:
	xorl	%ebp, %ebp
.LBB25_23:                              # %.thread
	leaq	32(%r12), %r15
	movq	120(%r12), %rsi
	movq	%r15, %rdi
	callq	_ZN10COutBuffer9SetStreamEP20ISequentialOutStream
	movq	%r15, %rdi
	callq	_ZN10COutBuffer4InitEv
	movl	$-1, 24(%r12)
	movb	%bpl, 8(%r12)
	movb	$1, 9(%r12)
	movq	$0, 16(%r12)
	movq	%rsp, %rcx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	callq	_ZN8NArchive3N7z11COutArchive11WriteHeaderERKNS0_16CArchiveDatabaseERKNS0_14CHeaderOptionsERy
	testb	%bpl, %bpl
	je	.LBB25_69
# BB#24:
	movq	%r15, 112(%rsp)         # 8-byte Spill
	movq	$_ZTV7CBufferIhE+16, 56(%rsp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 64(%rsp)
	movq	16(%r12), %rbp
	xorl	%r15d, %r15d
	testq	%rbp, %rbp
	je	.LBB25_27
# BB#25:
.Ltmp202:
	movq	%rbp, %rdi
	callq	_Znam
.Ltmp203:
# BB#26:
	movq	%rax, 72(%rsp)
	movq	%rbp, 64(%rsp)
	movq	%rax, %r15
	jmp	.LBB25_28
.LBB25_27:
	xorl	%ebp, %ebp
.LBB25_28:                              # %_ZN7CBufferIhE11SetCapacityEm.exit
	movq	%r15, 88(%r12)
	movq	%rbp, 96(%r12)
	movq	$0, 104(%r12)
	movw	$0, 8(%r12)
.Ltmp204:
	movq	%rsp, %rcx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	callq	_ZN8NArchive3N7z11COutArchive11WriteHeaderERKNS0_16CArchiveDatabaseERKNS0_14CHeaderOptionsERy
.Ltmp205:
# BB#29:
	movq	16(%r12), %rax
	cmpq	104(%r12), %rax
	jne	.LBB25_57
# BB#30:
.Ltmp206:
	leaq	184(%rsp), %r14
	movq	%r14, %rdi
	callq	_ZN8NArchive3N7z22CCompressionMethodModeC2Ev
.Ltmp207:
# BB#31:
	movq	%r15, 8(%rsp)           # 8-byte Spill
	movb	68(%rbx), %al
	movb	%al, 252(%rsp)
	cmpq	%r14, %rbx
	je	.LBB25_41
# BB#32:
	movl	$0, 264(%rsp)
	movq	256(%rsp), %rbp
	movl	$0, (%rbp)
	movslq	80(%rbx), %r15
	incq	%r15
	movl	268(%rsp), %eax
	cmpl	%eax, %r15d
	je	.LBB25_38
# BB#33:
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp209:
	callq	_Znam
.Ltmp210:
# BB#34:                                # %.noexc90
	xorl	%ecx, %ecx
	testq	%rbp, %rbp
	je	.LBB25_37
# BB#35:                                # %.noexc90
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	jle	.LBB25_37
# BB#36:                                # %._crit_edge.thread.i.i
	movq	%rbp, %rdi
	movq	%rax, %rbp
	callq	_ZdaPv
	movq	%rbp, %rax
	movslq	264(%rsp), %rcx
.LBB25_37:                              # %._crit_edge16.i.i
	movq	%rax, 256(%rsp)
	movl	$0, (%rax,%rcx,4)
	movl	%r15d, 268(%rsp)
	movq	%rax, %rbp
.LBB25_38:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	72(%rbx), %rax
.LBB25_39:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbp)
	addq	$4, %rbp
	testl	%ecx, %ecx
	jne	.LBB25_39
# BB#40:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	80(%rbx), %eax
	movl	%eax, 264(%rsp)
.LBB25_41:                              # %_ZN11CStringBaseIwEaSERKS0_.exit
	cmpb	$0, (%r13)
	cmovneq	%rbx, %r14
.Ltmp211:
	leaq	272(%rsp), %rdi
	movq	%r14, %rsi
	callq	_ZN8NArchive3N7z8CEncoderC1ERKNS0_22CCompressionMethodModeE
.Ltmp212:
# BB#42:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 88(%rsp)
	movq	$8, 104(%rsp)
	movq	$_ZTV13CRecordVectorIyE+16, 80(%rsp)
	movdqu	%xmm0, 32(%rsp)
	movq	$8, 48(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE+16, 24(%rsp)
.Ltmp214:
	leaq	272(%rsp), %rsi
	leaq	56(%rsp), %rdx
	leaq	80(%rsp), %rcx
	leaq	24(%rsp), %r8
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive12EncodeStreamERNS0_8CEncoderERK7CBufferIhER13CRecordVectorIyER13CObjectVectorINS0_7CFolderEE
	movl	%eax, %ebp
.Ltmp215:
# BB#43:
	movl	$1, %r14d
	testl	%ebp, %ebp
	jne	.LBB25_61
# BB#44:
	movb	$1, 9(%r12)
	cmpl	$0, 36(%rsp)
	je	.LBB25_80
# BB#45:
.Ltmp216:
	movl	$23, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp217:
# BB#46:                                # %_ZN8NArchive3N7z11COutArchive7WriteIDEy.exit
	movq	(%rsp), %rbx
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 160(%rsp)
	movq	$1, 176(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 152(%rsp)
	movdqu	%xmm0, 128(%rsp)
	movq	$4, 144(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 120(%rsp)
.Ltmp218:
	leaq	80(%rsp), %rdx
	leaq	152(%rsp), %rcx
	leaq	120(%rsp), %r8
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NArchive3N7z11COutArchive13WritePackInfoEyRK13CRecordVectorIyERKS2_IbERKS2_IjE
.Ltmp219:
# BB#47:
.Ltmp223:
	leaq	120(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp224:
# BB#48:
.Ltmp228:
	leaq	152(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp229:
# BB#49:
.Ltmp231:
	leaq	24(%rsp), %rsi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive15WriteUnpackInfoERK13CObjectVectorINS0_7CFolderEE
.Ltmp232:
# BB#50:
.Ltmp233:
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive9WriteByteEh
.Ltmp234:
# BB#51:                                # %.preheader
	movslq	92(%rsp), %rax
	testq	%rax, %rax
	jle	.LBB25_61
# BB#52:                                # %.lr.ph
	movq	96(%rsp), %rcx
	cmpl	$3, %eax
	jbe	.LBB25_58
# BB#53:                                # %min.iters.checked
	movq	%rax, %rsi
	andq	$-4, %rsi
	je	.LBB25_58
# BB#54:                                # %vector.ph
	movd	%rbx, %xmm0
	leaq	-4(%rsi), %rbx
	movl	%ebx, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB25_75
# BB#55:                                # %vector.body.prol.preheader
	negq	%rdx
	pxor	%xmm1, %xmm1
	xorl	%edi, %edi
.LBB25_56:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%rcx,%rdi,8), %xmm2
	movdqu	16(%rcx,%rdi,8), %xmm3
	paddq	%xmm2, %xmm0
	paddq	%xmm3, %xmm1
	addq	$4, %rdi
	incq	%rdx
	jne	.LBB25_56
	jmp	.LBB25_76
.LBB25_57:
	movl	$-2147467259, %ebp      # imm = 0x80004005
	movl	$1, %r14d
	jmp	.LBB25_66
.LBB25_58:
	xorl	%esi, %esi
.LBB25_59:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	addq	(%rcx,%rsi,8), %rbx
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB25_59
.LBB25_60:                              # %..loopexit_crit_edge
	movq	%rbx, (%rsp)
	xorl	%r14d, %r14d
.LBB25_61:                              # %.loopexit
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE+16, 24(%rsp)
.Ltmp235:
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp236:
# BB#62:
.Ltmp241:
	leaq	24(%rsp), %rdi
	movq	8(%rsp), %r15           # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp242:
# BB#63:                                # %_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev.exit96
.Ltmp244:
	leaq	80(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp245:
# BB#64:
.Ltmp247:
	leaq	272(%rsp), %rdi
	callq	_ZN8NArchive3N7z8CEncoderD1Ev
.Ltmp248:
# BB#65:
.Ltmp250:
	leaq	184(%rsp), %rdi
	callq	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
.Ltmp251:
.LBB25_66:
	movq	$_ZTV7CBufferIhE+16, 56(%rsp)
	testq	%r15, %r15
	je	.LBB25_68
# BB#67:
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB25_68:                              # %_ZN7CBufferIhED2Ev.exit92
	testl	%r14d, %r14d
	movq	112(%rsp), %r15         # 8-byte Reload
	jne	.LBB25_73
.LBB25_69:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer5FlushEv
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB25_73
# BB#70:                                # %.thread98
	movl	24(%r12), %ebx
	notl	%ebx
	movq	%r15, %rdi
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
	movq	(%rsp), %rbp
.LBB25_71:
	movq	%rax, 280(%rsp)
	movl	%ebx, 288(%rsp)
	movq	%rbp, 272(%rsp)
	movq	(%r12), %rsi
	movq	112(%r12), %rdi
	movq	(%rdi), %rax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB25_73
# BB#72:
	leaq	272(%rsp), %rsi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z11COutArchive16WriteStartHeaderERKNS0_12CStartHeaderE
	movl	%eax, %ebp
.LBB25_73:                              # %_ZNK8NArchive3N7z16CArchiveDatabase13CheckNumFilesEv.exit.thread
	movl	%ebp, %eax
	addq	$712, %rsp              # imm = 0x2C8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB25_74:
	movq	$0, (%rsp)
	xorl	%ebp, %ebp
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	CrcCalc
	movl	%eax, %ebx
	xorl	%eax, %eax
	jmp	.LBB25_71
.LBB25_75:
	pxor	%xmm1, %xmm1
	xorl	%edi, %edi
.LBB25_76:                              # %vector.body.prol.loopexit
	cmpq	$12, %rbx
	jb	.LBB25_79
# BB#77:                                # %vector.ph.new
	movq	%rsi, %rdx
	subq	%rdi, %rdx
	leaq	112(%rcx,%rdi,8), %rdi
.LBB25_78:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-112(%rdi), %xmm2
	movdqu	-96(%rdi), %xmm3
	movdqu	-80(%rdi), %xmm4
	movdqu	-64(%rdi), %xmm5
	paddq	%xmm0, %xmm2
	paddq	%xmm1, %xmm3
	movdqu	-48(%rdi), %xmm6
	movdqu	-32(%rdi), %xmm7
	paddq	%xmm4, %xmm6
	paddq	%xmm2, %xmm6
	paddq	%xmm5, %xmm7
	paddq	%xmm3, %xmm7
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	paddq	%xmm6, %xmm0
	paddq	%xmm7, %xmm1
	subq	$-128, %rdi
	addq	$-16, %rdx
	jne	.LBB25_78
.LBB25_79:                              # %middle.block
	paddq	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddq	%xmm0, %xmm1
	movd	%xmm1, %rbx
	cmpq	%rsi, %rax
	jne	.LBB25_59
	jmp	.LBB25_60
.LBB25_80:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$1, (%rax)
.Ltmp253:
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp254:
# BB#81:
.LBB25_82:
.Ltmp230:
	jmp	.LBB25_95
.LBB25_83:
.Ltmp225:
	movq	%rax, %rbx
	jmp	.LBB25_85
.LBB25_84:
.Ltmp220:
	movq	%rax, %rbx
.Ltmp221:
	leaq	120(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp222:
.LBB25_85:
.Ltmp226:
	leaq	152(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp227:
	jmp	.LBB25_96
.LBB25_86:
.Ltmp252:
	jmp	.LBB25_106
.LBB25_87:
.Ltmp249:
	jmp	.LBB25_93
.LBB25_88:
.Ltmp246:
	movq	%rax, %rbx
	jmp	.LBB25_99
.LBB25_89:
.Ltmp243:
	movq	%rax, %rbx
	jmp	.LBB25_98
.LBB25_90:
.Ltmp237:
	movq	%rax, %rbx
.Ltmp238:
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp239:
	jmp	.LBB25_98
.LBB25_91:
.Ltmp240:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB25_92:
.Ltmp213:
.LBB25_93:
	movq	%rax, %rbx
	jmp	.LBB25_100
.LBB25_94:
.Ltmp255:
.LBB25_95:
	movq	%rax, %rbx
.LBB25_96:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE+16, 24(%rsp)
.Ltmp256:
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp257:
# BB#97:
.Ltmp262:
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp263:
.LBB25_98:
.Ltmp264:
	leaq	80(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp265:
.LBB25_99:
.Ltmp266:
	leaq	272(%rsp), %rdi
	callq	_ZN8NArchive3N7z8CEncoderD1Ev
.Ltmp267:
.LBB25_100:
.Ltmp268:
	leaq	184(%rsp), %rdi
	callq	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
.Ltmp269:
	jmp	.LBB25_107
.LBB25_101:
.Ltmp258:
	movq	%rax, %rbx
.Ltmp259:
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp260:
	jmp	.LBB25_104
.LBB25_102:
.Ltmp261:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB25_103:
.Ltmp270:
	movq	%rax, %rbx
.LBB25_104:                             # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB25_105:
.Ltmp208:
	movq	%r15, 8(%rsp)           # 8-byte Spill
.LBB25_106:
	movq	%rax, %rbx
.LBB25_107:
	movq	$_ZTV7CBufferIhE+16, 56(%rsp)
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB25_109
# BB#108:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZdaPv
.LBB25_109:                             # %_ZN7CBufferIhED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end25:
	.size	_ZN8NArchive3N7z11COutArchive13WriteDatabaseERKNS0_16CArchiveDatabaseEPKNS0_22CCompressionMethodModeERKNS0_14CHeaderOptionsE, .Lfunc_end25-_ZN8NArchive3N7z11COutArchive13WriteDatabaseERKNS0_16CArchiveDatabaseEPKNS0_22CCompressionMethodModeERKNS0_14CHeaderOptionsE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table25:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\232\202\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\221\002"              # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp202-.Lfunc_begin5  #   Call between .Lfunc_begin5 and .Ltmp202
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp202-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp207-.Ltmp202       #   Call between .Ltmp202 and .Ltmp207
	.long	.Ltmp208-.Lfunc_begin5  #     jumps to .Ltmp208
	.byte	0                       #   On action: cleanup
	.long	.Ltmp209-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp212-.Ltmp209       #   Call between .Ltmp209 and .Ltmp212
	.long	.Ltmp213-.Lfunc_begin5  #     jumps to .Ltmp213
	.byte	0                       #   On action: cleanup
	.long	.Ltmp214-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp217-.Ltmp214       #   Call between .Ltmp214 and .Ltmp217
	.long	.Ltmp255-.Lfunc_begin5  #     jumps to .Ltmp255
	.byte	0                       #   On action: cleanup
	.long	.Ltmp218-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp219-.Ltmp218       #   Call between .Ltmp218 and .Ltmp219
	.long	.Ltmp220-.Lfunc_begin5  #     jumps to .Ltmp220
	.byte	0                       #   On action: cleanup
	.long	.Ltmp223-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp224-.Ltmp223       #   Call between .Ltmp223 and .Ltmp224
	.long	.Ltmp225-.Lfunc_begin5  #     jumps to .Ltmp225
	.byte	0                       #   On action: cleanup
	.long	.Ltmp228-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Ltmp229-.Ltmp228       #   Call between .Ltmp228 and .Ltmp229
	.long	.Ltmp230-.Lfunc_begin5  #     jumps to .Ltmp230
	.byte	0                       #   On action: cleanup
	.long	.Ltmp231-.Lfunc_begin5  # >> Call Site 8 <<
	.long	.Ltmp234-.Ltmp231       #   Call between .Ltmp231 and .Ltmp234
	.long	.Ltmp255-.Lfunc_begin5  #     jumps to .Ltmp255
	.byte	0                       #   On action: cleanup
	.long	.Ltmp235-.Lfunc_begin5  # >> Call Site 9 <<
	.long	.Ltmp236-.Ltmp235       #   Call between .Ltmp235 and .Ltmp236
	.long	.Ltmp237-.Lfunc_begin5  #     jumps to .Ltmp237
	.byte	0                       #   On action: cleanup
	.long	.Ltmp241-.Lfunc_begin5  # >> Call Site 10 <<
	.long	.Ltmp242-.Ltmp241       #   Call between .Ltmp241 and .Ltmp242
	.long	.Ltmp243-.Lfunc_begin5  #     jumps to .Ltmp243
	.byte	0                       #   On action: cleanup
	.long	.Ltmp244-.Lfunc_begin5  # >> Call Site 11 <<
	.long	.Ltmp245-.Ltmp244       #   Call between .Ltmp244 and .Ltmp245
	.long	.Ltmp246-.Lfunc_begin5  #     jumps to .Ltmp246
	.byte	0                       #   On action: cleanup
	.long	.Ltmp247-.Lfunc_begin5  # >> Call Site 12 <<
	.long	.Ltmp248-.Ltmp247       #   Call between .Ltmp247 and .Ltmp248
	.long	.Ltmp249-.Lfunc_begin5  #     jumps to .Ltmp249
	.byte	0                       #   On action: cleanup
	.long	.Ltmp250-.Lfunc_begin5  # >> Call Site 13 <<
	.long	.Ltmp251-.Ltmp250       #   Call between .Ltmp250 and .Ltmp251
	.long	.Ltmp252-.Lfunc_begin5  #     jumps to .Ltmp252
	.byte	0                       #   On action: cleanup
	.long	.Ltmp251-.Lfunc_begin5  # >> Call Site 14 <<
	.long	.Ltmp253-.Ltmp251       #   Call between .Ltmp251 and .Ltmp253
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp253-.Lfunc_begin5  # >> Call Site 15 <<
	.long	.Ltmp254-.Ltmp253       #   Call between .Ltmp253 and .Ltmp254
	.long	.Ltmp255-.Lfunc_begin5  #     jumps to .Ltmp255
	.byte	0                       #   On action: cleanup
	.long	.Ltmp221-.Lfunc_begin5  # >> Call Site 16 <<
	.long	.Ltmp227-.Ltmp221       #   Call between .Ltmp221 and .Ltmp227
	.long	.Ltmp270-.Lfunc_begin5  #     jumps to .Ltmp270
	.byte	1                       #   On action: 1
	.long	.Ltmp238-.Lfunc_begin5  # >> Call Site 17 <<
	.long	.Ltmp239-.Ltmp238       #   Call between .Ltmp238 and .Ltmp239
	.long	.Ltmp240-.Lfunc_begin5  #     jumps to .Ltmp240
	.byte	1                       #   On action: 1
	.long	.Ltmp256-.Lfunc_begin5  # >> Call Site 18 <<
	.long	.Ltmp257-.Ltmp256       #   Call between .Ltmp256 and .Ltmp257
	.long	.Ltmp258-.Lfunc_begin5  #     jumps to .Ltmp258
	.byte	1                       #   On action: 1
	.long	.Ltmp262-.Lfunc_begin5  # >> Call Site 19 <<
	.long	.Ltmp269-.Ltmp262       #   Call between .Ltmp262 and .Ltmp269
	.long	.Ltmp270-.Lfunc_begin5  #     jumps to .Ltmp270
	.byte	1                       #   On action: 1
	.long	.Ltmp259-.Lfunc_begin5  # >> Call Site 20 <<
	.long	.Ltmp260-.Ltmp259       #   Call between .Ltmp259 and .Ltmp260
	.long	.Ltmp261-.Lfunc_begin5  #     jumps to .Ltmp261
	.byte	1                       #   On action: 1
	.long	.Ltmp260-.Lfunc_begin5  # >> Call Site 21 <<
	.long	.Lfunc_end25-.Ltmp260   #   Call between .Ltmp260 and .Lfunc_end25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z22CCompressionMethodModeC2Ev,"axG",@progbits,_ZN8NArchive3N7z22CCompressionMethodModeC2Ev,comdat
	.weak	_ZN8NArchive3N7z22CCompressionMethodModeC2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z22CCompressionMethodModeC2Ev,@function
_ZN8NArchive3N7z22CCompressionMethodModeC2Ev: # @_ZN8NArchive3N7z22CCompressionMethodModeC2Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r15
.Lcfi233:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi234:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi235:
	.cfi_def_cfa_offset 32
.Lcfi236:
	.cfi_offset %rbx, -32
.Lcfi237:
	.cfi_offset %r14, -24
.Lcfi238:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	movq	$8, 24(%rbx)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
	leaq	32(%rbx), %r15
	movups	%xmm0, 40(%rbx)
	movq	$16, 56(%rbx)
	movq	$_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE+16, 32(%rbx)
	movl	$1, 64(%rbx)
	movb	$0, 68(%rbx)
	movups	%xmm0, 72(%rbx)
.Ltmp271:
	movl	$16, %edi
	callq	_Znam
.Ltmp272:
# BB#1:
	movq	%rax, 72(%rbx)
	movl	$0, (%rax)
	movl	$4, 84(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB26_2:
.Ltmp273:
	movq	%rax, %r14
.Ltmp274:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp275:
# BB#3:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
.Ltmp276:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp277:
# BB#4:
.Ltmp282:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp283:
# BB#5:                                 # %_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB26_6:
.Ltmp278:
	movq	%rax, %r14
.Ltmp279:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp280:
	jmp	.LBB26_9
.LBB26_7:
.Ltmp281:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB26_8:
.Ltmp284:
	movq	%rax, %r14
.LBB26_9:                               # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end26:
	.size	_ZN8NArchive3N7z22CCompressionMethodModeC2Ev, .Lfunc_end26-_ZN8NArchive3N7z22CCompressionMethodModeC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp271-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp272-.Ltmp271       #   Call between .Ltmp271 and .Ltmp272
	.long	.Ltmp273-.Lfunc_begin6  #     jumps to .Ltmp273
	.byte	0                       #   On action: cleanup
	.long	.Ltmp274-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp275-.Ltmp274       #   Call between .Ltmp274 and .Ltmp275
	.long	.Ltmp284-.Lfunc_begin6  #     jumps to .Ltmp284
	.byte	1                       #   On action: 1
	.long	.Ltmp276-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp277-.Ltmp276       #   Call between .Ltmp276 and .Ltmp277
	.long	.Ltmp278-.Lfunc_begin6  #     jumps to .Ltmp278
	.byte	1                       #   On action: 1
	.long	.Ltmp282-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp283-.Ltmp282       #   Call between .Ltmp282 and .Ltmp283
	.long	.Ltmp284-.Lfunc_begin6  #     jumps to .Ltmp284
	.byte	1                       #   On action: 1
	.long	.Ltmp283-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp279-.Ltmp283       #   Call between .Ltmp283 and .Ltmp279
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp279-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp280-.Ltmp279       #   Call between .Ltmp279 and .Ltmp280
	.long	.Ltmp281-.Lfunc_begin6  #     jumps to .Ltmp281
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev: # @_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi239:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi240:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi241:
	.cfi_def_cfa_offset 32
.Lcfi242:
	.cfi_offset %rbx, -24
.Lcfi243:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE+16, (%rbx)
.Ltmp285:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp286:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB27_2:
.Ltmp287:
	movq	%rax, %r14
.Ltmp288:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp289:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB27_4:
.Ltmp290:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end27:
	.size	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev, .Lfunc_end27-_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table27:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp285-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp286-.Ltmp285       #   Call between .Ltmp285 and .Ltmp286
	.long	.Ltmp287-.Lfunc_begin7  #     jumps to .Ltmp287
	.byte	0                       #   On action: cleanup
	.long	.Ltmp286-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp288-.Ltmp286       #   Call between .Ltmp286 and .Ltmp288
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp288-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp289-.Ltmp288       #   Call between .Ltmp288 and .Ltmp289
	.long	.Ltmp290-.Lfunc_begin7  #     jumps to .Ltmp290
	.byte	1                       #   On action: 1
	.long	.Ltmp289-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Lfunc_end27-.Ltmp289   #   Call between .Ltmp289 and .Lfunc_end27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z22CCompressionMethodModeD2Ev,"axG",@progbits,_ZN8NArchive3N7z22CCompressionMethodModeD2Ev,comdat
	.weak	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev,@function
_ZN8NArchive3N7z22CCompressionMethodModeD2Ev: # @_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi244:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi245:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi246:
	.cfi_def_cfa_offset 32
.Lcfi247:
	.cfi_offset %rbx, -24
.Lcfi248:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB28_2
# BB#1:
	callq	_ZdaPv
.LBB28_2:                               # %_ZN11CStringBaseIwED2Ev.exit
	leaq	32(%rbx), %rdi
.Ltmp291:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp292:
# BB#3:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
.Ltmp303:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp304:
# BB#4:                                 # %_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB28_5:
.Ltmp305:
	movq	%rax, %r14
.Ltmp306:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp307:
	jmp	.LBB28_6
.LBB28_7:
.Ltmp308:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB28_8:
.Ltmp293:
	movq	%rax, %r14
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
.Ltmp294:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp295:
# BB#9:
.Ltmp300:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp301:
.LBB28_6:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB28_12:
.Ltmp302:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB28_10:
.Ltmp296:
	movq	%rax, %r14
.Ltmp297:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp298:
# BB#13:                                # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB28_11:
.Ltmp299:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end28:
	.size	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev, .Lfunc_end28-_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table28:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp291-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp292-.Ltmp291       #   Call between .Ltmp291 and .Ltmp292
	.long	.Ltmp293-.Lfunc_begin8  #     jumps to .Ltmp293
	.byte	0                       #   On action: cleanup
	.long	.Ltmp303-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp304-.Ltmp303       #   Call between .Ltmp303 and .Ltmp304
	.long	.Ltmp305-.Lfunc_begin8  #     jumps to .Ltmp305
	.byte	0                       #   On action: cleanup
	.long	.Ltmp304-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp306-.Ltmp304       #   Call between .Ltmp304 and .Ltmp306
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp306-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Ltmp307-.Ltmp306       #   Call between .Ltmp306 and .Ltmp307
	.long	.Ltmp308-.Lfunc_begin8  #     jumps to .Ltmp308
	.byte	1                       #   On action: 1
	.long	.Ltmp294-.Lfunc_begin8  # >> Call Site 5 <<
	.long	.Ltmp295-.Ltmp294       #   Call between .Ltmp294 and .Ltmp295
	.long	.Ltmp296-.Lfunc_begin8  #     jumps to .Ltmp296
	.byte	1                       #   On action: 1
	.long	.Ltmp300-.Lfunc_begin8  # >> Call Site 6 <<
	.long	.Ltmp301-.Ltmp300       #   Call between .Ltmp300 and .Ltmp301
	.long	.Ltmp302-.Lfunc_begin8  #     jumps to .Ltmp302
	.byte	1                       #   On action: 1
	.long	.Ltmp301-.Lfunc_begin8  # >> Call Site 7 <<
	.long	.Ltmp297-.Ltmp301       #   Call between .Ltmp301 and .Ltmp297
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp297-.Lfunc_begin8  # >> Call Site 8 <<
	.long	.Ltmp298-.Ltmp297       #   Call between .Ltmp297 and .Ltmp298
	.long	.Ltmp299-.Lfunc_begin8  #     jumps to .Ltmp299
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN7CBufferIhED2Ev,"axG",@progbits,_ZN7CBufferIhED2Ev,comdat
	.weak	_ZN7CBufferIhED2Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED2Ev,@function
_ZN7CBufferIhED2Ev:                     # @_ZN7CBufferIhED2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV7CBufferIhE+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB29_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB29_1:
	retq
.Lfunc_end29:
	.size	_ZN7CBufferIhED2Ev, .Lfunc_end29-_ZN7CBufferIhED2Ev
	.cfi_endproc

	.text
	.globl	_ZNK8NArchive3N7z16CArchiveDatabase7GetFileEiRNS0_9CFileItemERNS0_10CFileItem2E
	.p2align	4, 0x90
	.type	_ZNK8NArchive3N7z16CArchiveDatabase7GetFileEiRNS0_9CFileItemERNS0_10CFileItem2E,@function
_ZNK8NArchive3N7z16CArchiveDatabase7GetFileEiRNS0_9CFileItemERNS0_10CFileItem2E: # @_ZNK8NArchive3N7z16CArchiveDatabase7GetFileEiRNS0_9CFileItemERNS0_10CFileItem2E
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi249:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi250:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi251:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi252:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi253:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi254:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi255:
	.cfi_def_cfa_offset 80
.Lcfi256:
	.cfi_offset %rbx, -56
.Lcfi257:
	.cfi_offset %r12, -48
.Lcfi258:
	.cfi_offset %r13, -40
.Lcfi259:
	.cfi_offset %r14, -32
.Lcfi260:
	.cfi_offset %r15, -24
.Lcfi261:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbp
	movq	%rdi, %r12
	movq	176(%r12), %rax
	movslq	%esi, %rdx
	movq	(%rax,%rdx,8), %r15
	movups	(%r15), %xmm0
	movups	%xmm0, (%rbp)
	cmpq	%rbp, %r15
	je	.LBB30_10
# BB#1:
	movl	$0, 24(%rbp)
	movq	16(%rbp), %rbx
	movl	$0, (%rbx)
	movslq	24(%r15), %r13
	incq	%r13
	movl	28(%rbp), %eax
	cmpl	%eax, %r13d
	je	.LBB30_7
# BB#2:
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	$4, %ecx
	movq	%r13, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB30_3
# BB#4:
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	4(%rsp), %esi           # 4-byte Reload
	jle	.LBB30_6
# BB#5:                                 # %._crit_edge.thread.i.i.i
	movq	%rbx, %rdi
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	%rbx, %rax
	movl	4(%rsp), %esi           # 4-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	movslq	24(%rbp), %rcx
	jmp	.LBB30_6
.LBB30_3:
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	4(%rsp), %esi           # 4-byte Reload
.LBB30_6:                               # %._crit_edge16.i.i.i
	movq	%rax, 16(%rbp)
	movl	$0, (%rax,%rcx,4)
	movl	%r13d, 28(%rbp)
	movq	%rax, %rbx
.LBB30_7:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
	movq	16(%r15), %rax
	.p2align	4, 0x90
.LBB30_8:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB30_8
# BB#9:                                 # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i.i
	movl	24(%r15), %eax
	movl	%eax, 24(%rbp)
.LBB30_10:                              # %_ZN8NArchive3N7z9CFileItemaSERKS1_.exit
	movl	32(%r15), %eax
	movl	%eax, 32(%rbp)
	cmpl	%esi, 236(%r12)
	jle	.LBB30_11
# BB#12:
	movq	240(%r12), %rax
	cmpb	$0, (%rax,%rdx)
	je	.LBB30_11
# BB#13:
	movq	208(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movb	$1, %cl
	jmp	.LBB30_14
.LBB30_11:
	xorl	%eax, %eax
	xorl	%ecx, %ecx
.LBB30_14:                              # %_ZNK8NArchive3N7z16CUInt64DefVector7GetItemEiRy.exit24
	movq	%rax, (%r14)
	movb	%cl, 32(%r14)
	cmpl	%esi, 300(%r12)
	jle	.LBB30_15
# BB#16:
	movq	304(%r12), %rax
	cmpb	$0, (%rax,%rdx)
	je	.LBB30_15
# BB#17:
	movq	272(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movb	$1, %cl
	jmp	.LBB30_18
.LBB30_15:
	xorl	%eax, %eax
	xorl	%ecx, %ecx
.LBB30_18:                              # %_ZNK8NArchive3N7z16CUInt64DefVector7GetItemEiRy.exit21
	movq	%rax, 8(%r14)
	movb	%cl, 33(%r14)
	cmpl	%esi, 364(%r12)
	jle	.LBB30_19
# BB#20:
	movq	368(%r12), %rax
	cmpb	$0, (%rax,%rdx)
	je	.LBB30_19
# BB#21:
	movq	336(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movb	$1, %cl
	jmp	.LBB30_22
.LBB30_19:
	xorl	%eax, %eax
	xorl	%ecx, %ecx
.LBB30_22:                              # %_ZNK8NArchive3N7z16CUInt64DefVector7GetItemEiRy.exit18
	movq	%rax, 16(%r14)
	movb	%cl, 34(%r14)
	cmpl	%esi, 428(%r12)
	jle	.LBB30_23
# BB#24:
	movq	432(%r12), %rax
	cmpb	$0, (%rax,%rdx)
	je	.LBB30_23
# BB#25:
	movq	400(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movb	$1, %cl
	jmp	.LBB30_26
.LBB30_23:
	xorl	%eax, %eax
	xorl	%ecx, %ecx
.LBB30_26:                              # %_ZNK8NArchive3N7z16CUInt64DefVector7GetItemEiRy.exit
	movq	%rax, 24(%r14)
	movb	%cl, 35(%r14)
	cmpl	%esi, 460(%r12)
	jle	.LBB30_27
# BB#28:
	movq	464(%r12), %rax
	cmpb	$0, (%rax,%rdx)
	setne	%al
	jmp	.LBB30_29
.LBB30_27:
	xorl	%eax, %eax
.LBB30_29:                              # %_ZNK8NArchive3N7z16CArchiveDatabase10IsItemAntiEi.exit
	movb	%al, 36(%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end30:
	.size	_ZNK8NArchive3N7z16CArchiveDatabase7GetFileEiRNS0_9CFileItemERNS0_10CFileItem2E, .Lfunc_end30-_ZNK8NArchive3N7z16CArchiveDatabase7GetFileEiRNS0_9CFileItemERNS0_10CFileItem2E
	.cfi_endproc

	.globl	_ZN8NArchive3N7z16CArchiveDatabase7AddFileERKNS0_9CFileItemERKNS0_10CFileItem2E
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z16CArchiveDatabase7AddFileERKNS0_9CFileItemERKNS0_10CFileItem2E,@function
_ZN8NArchive3N7z16CArchiveDatabase7AddFileERKNS0_9CFileItemERKNS0_10CFileItem2E: # @_ZN8NArchive3N7z16CArchiveDatabase7AddFileERKNS0_9CFileItemERKNS0_10CFileItem2E
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi262:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi263:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi264:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi265:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi266:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi267:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi268:
	.cfi_def_cfa_offset 64
.Lcfi269:
	.cfi_offset %rbx, -56
.Lcfi270:
	.cfi_offset %r12, -48
.Lcfi271:
	.cfi_offset %r13, -40
.Lcfi272:
	.cfi_offset %r14, -32
.Lcfi273:
	.cfi_offset %r15, -24
.Lcfi274:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rdi, %rbx
	movslq	172(%rbx), %r13
	movb	32(%r15), %r14b
	movq	(%r15), %rbp
	cmpl	%r13d, 236(%rbx)
	movq	%rsi, (%rsp)            # 8-byte Spill
	jle	.LBB31_2
# BB#1:                                 # %.._crit_edge9_crit_edge.i
	movq	240(%rbx), %rax
	jmp	.LBB31_4
.LBB31_2:                               # %.lr.ph8.i
	leaq	224(%rbx), %r12
	.p2align	4, 0x90
.LBB31_3:                               # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	240(%rbx), %rax
	movslq	236(%rbx), %rcx
	movb	$0, (%rax,%rcx)
	leal	1(%rcx), %edx
	movl	%edx, 236(%rbx)
	cmpl	%r13d, %ecx
	jl	.LBB31_3
.LBB31_4:                               # %._crit_edge9.i
	testb	%r14b, %r14b
	movb	%r14b, (%rax,%r13)
	je	.LBB31_10
# BB#5:                                 # %.preheader.i
	cmpl	%r13d, 204(%rbx)
	jle	.LBB31_7
# BB#6:                                 # %.preheader.._crit_edge_crit_edge.i
	movq	208(%rbx), %rax
	jmp	.LBB31_9
.LBB31_7:                               # %.lr.ph.i
	leaq	192(%rbx), %r12
	.p2align	4, 0x90
.LBB31_8:                               # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	208(%rbx), %rax
	movslq	204(%rbx), %rcx
	movq	$0, (%rax,%rcx,8)
	leal	1(%rcx), %edx
	movl	%edx, 204(%rbx)
	cmpl	%r13d, %ecx
	jl	.LBB31_8
.LBB31_9:                               # %._crit_edge.i
	movq	%rbp, (%rax,%r13,8)
.LBB31_10:                              # %_ZN8NArchive3N7z16CUInt64DefVector7SetItemEiby.exit
	movb	33(%r15), %r14b
	movq	8(%r15), %rbp
	cmpl	%r13d, 300(%rbx)
	jle	.LBB31_12
# BB#11:                                # %.._crit_edge9_crit_edge.i18
	movq	304(%rbx), %rax
	jmp	.LBB31_14
.LBB31_12:                              # %.lr.ph8.i19
	leaq	288(%rbx), %r12
	.p2align	4, 0x90
.LBB31_13:                              # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	304(%rbx), %rax
	movslq	300(%rbx), %rcx
	movb	$0, (%rax,%rcx)
	leal	1(%rcx), %edx
	movl	%edx, 300(%rbx)
	cmpl	%r13d, %ecx
	jl	.LBB31_13
.LBB31_14:                              # %._crit_edge9.i20
	testb	%r14b, %r14b
	movb	%r14b, (%rax,%r13)
	je	.LBB31_20
# BB#15:                                # %.preheader.i21
	cmpl	%r13d, 268(%rbx)
	jle	.LBB31_17
# BB#16:                                # %.preheader.._crit_edge_crit_edge.i25
	movq	272(%rbx), %rax
	jmp	.LBB31_19
.LBB31_17:                              # %.lr.ph.i26
	leaq	256(%rbx), %r12
	.p2align	4, 0x90
.LBB31_18:                              # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	272(%rbx), %rax
	movslq	268(%rbx), %rcx
	movq	$0, (%rax,%rcx,8)
	leal	1(%rcx), %edx
	movl	%edx, 268(%rbx)
	cmpl	%r13d, %ecx
	jl	.LBB31_18
.LBB31_19:                              # %._crit_edge.i27
	movq	%rbp, (%rax,%r13,8)
.LBB31_20:                              # %_ZN8NArchive3N7z16CUInt64DefVector7SetItemEiby.exit28
	movb	34(%r15), %r14b
	movq	16(%r15), %rbp
	cmpl	%r13d, 364(%rbx)
	jle	.LBB31_22
# BB#21:                                # %.._crit_edge9_crit_edge.i31
	movq	368(%rbx), %rax
	jmp	.LBB31_24
.LBB31_22:                              # %.lr.ph8.i32
	leaq	352(%rbx), %r12
	.p2align	4, 0x90
.LBB31_23:                              # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	368(%rbx), %rax
	movslq	364(%rbx), %rcx
	movb	$0, (%rax,%rcx)
	leal	1(%rcx), %edx
	movl	%edx, 364(%rbx)
	cmpl	%r13d, %ecx
	jl	.LBB31_23
.LBB31_24:                              # %._crit_edge9.i33
	testb	%r14b, %r14b
	movb	%r14b, (%rax,%r13)
	je	.LBB31_30
# BB#25:                                # %.preheader.i34
	cmpl	%r13d, 332(%rbx)
	jle	.LBB31_27
# BB#26:                                # %.preheader.._crit_edge_crit_edge.i38
	movq	336(%rbx), %rax
	jmp	.LBB31_29
.LBB31_27:                              # %.lr.ph.i39
	leaq	320(%rbx), %r12
	.p2align	4, 0x90
.LBB31_28:                              # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	336(%rbx), %rax
	movslq	332(%rbx), %rcx
	movq	$0, (%rax,%rcx,8)
	leal	1(%rcx), %edx
	movl	%edx, 332(%rbx)
	cmpl	%r13d, %ecx
	jl	.LBB31_28
.LBB31_29:                              # %._crit_edge.i40
	movq	%rbp, (%rax,%r13,8)
.LBB31_30:                              # %_ZN8NArchive3N7z16CUInt64DefVector7SetItemEiby.exit41
	movb	35(%r15), %r14b
	movq	24(%r15), %rbp
	cmpl	%r13d, 428(%rbx)
	jle	.LBB31_32
# BB#31:                                # %.._crit_edge9_crit_edge.i44
	movq	432(%rbx), %rax
	jmp	.LBB31_34
.LBB31_32:                              # %.lr.ph8.i45
	leaq	416(%rbx), %r12
	.p2align	4, 0x90
.LBB31_33:                              # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	432(%rbx), %rax
	movslq	428(%rbx), %rcx
	movb	$0, (%rax,%rcx)
	leal	1(%rcx), %edx
	movl	%edx, 428(%rbx)
	cmpl	%r13d, %ecx
	jl	.LBB31_33
.LBB31_34:                              # %._crit_edge9.i46
	testb	%r14b, %r14b
	movb	%r14b, (%rax,%r13)
	je	.LBB31_40
# BB#35:                                # %.preheader.i47
	cmpl	%r13d, 396(%rbx)
	jle	.LBB31_37
# BB#36:                                # %.preheader.._crit_edge_crit_edge.i51
	movq	400(%rbx), %rax
	jmp	.LBB31_39
.LBB31_37:                              # %.lr.ph.i52
	leaq	384(%rbx), %r12
	.p2align	4, 0x90
.LBB31_38:                              # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	400(%rbx), %rax
	movslq	396(%rbx), %rcx
	movq	$0, (%rax,%rcx,8)
	leal	1(%rcx), %edx
	movl	%edx, 396(%rbx)
	cmpl	%r13d, %ecx
	jl	.LBB31_38
.LBB31_39:                              # %._crit_edge.i53
	movq	%rbp, (%rax,%r13,8)
.LBB31_40:                              # %_ZN8NArchive3N7z16CUInt64DefVector7SetItemEiby.exit54
	leaq	160(%rbx), %r12
	movb	36(%r15), %bpl
	cmpl	%r13d, 460(%rbx)
	jle	.LBB31_42
# BB#41:                                # %.._crit_edge_crit_edge.i
	movq	464(%rbx), %rax
	jmp	.LBB31_44
.LBB31_42:                              # %.lr.ph.i57
	leaq	448(%rbx), %r15
	.p2align	4, 0x90
.LBB31_43:                              # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	464(%rbx), %rax
	movslq	460(%rbx), %rcx
	movb	$0, (%rax,%rcx)
	leal	1(%rcx), %edx
	movl	%edx, 460(%rbx)
	cmpl	%r13d, %ecx
	jl	.LBB31_43
.LBB31_44:                              # %_ZN8NArchive3N7z16CArchiveDatabase11SetItemAntiEib.exit
	movb	%bpl, (%rax,%r13)
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE3AddERKS2_ # TAILCALL
.Lfunc_end31:
	.size	_ZN8NArchive3N7z16CArchiveDatabase7AddFileERKNS0_9CFileItemERKNS0_10CFileItem2E, .Lfunc_end31-_ZN8NArchive3N7z16CArchiveDatabase7AddFileERKNS0_9CFileItemERKNS0_10CFileItem2E
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE3AddERKS2_,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE3AddERKS2_,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE3AddERKS2_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE3AddERKS2_,@function
_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE3AddERKS2_: # @_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE3AddERKS2_
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r15
.Lcfi275:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi276:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi277:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi278:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi279:
	.cfi_def_cfa_offset 48
.Lcfi280:
	.cfi_offset %rbx, -48
.Lcfi281:
	.cfi_offset %r12, -40
.Lcfi282:
	.cfi_offset %r13, -32
.Lcfi283:
	.cfi_offset %r14, -24
.Lcfi284:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movups	(%r15), %xmm0
	movups	%xmm0, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movslq	24(%r15), %r13
	leaq	1(%r13), %r12
	testl	%r12d, %r12d
	je	.LBB32_1
# BB#2:                                 # %._crit_edge16.i.i.i
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp309:
	callq	_Znam
.Ltmp310:
# BB#3:                                 # %.noexc
	movq	%rax, 16(%rbx)
	movl	$0, (%rax)
	movl	%r12d, 28(%rbx)
	jmp	.LBB32_4
.LBB32_1:
	xorl	%eax, %eax
.LBB32_4:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
	movq	16(%r15), %rcx
	.p2align	4, 0x90
.LBB32_5:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB32_5
# BB#6:
	movl	%r13d, 24(%rbx)
	movl	32(%r15), %eax
	movl	%eax, 32(%rbx)
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r14), %rcx
	movslq	12(%r14), %rax
	movq	%rbx, (%rcx,%rax,8)
	leal	1(%rax), %ecx
	movl	%ecx, 12(%r14)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB32_7:
.Ltmp311:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end32:
	.size	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE3AddERKS2_, .Lfunc_end32-_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE3AddERKS2_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin9-.Lfunc_begin9 # >> Call Site 1 <<
	.long	.Ltmp309-.Lfunc_begin9  #   Call between .Lfunc_begin9 and .Ltmp309
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp309-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp310-.Ltmp309       #   Call between .Ltmp309 and .Ltmp310
	.long	.Ltmp311-.Lfunc_begin9  #     jumps to .Ltmp311
	.byte	0                       #   On action: cleanup
	.long	.Ltmp310-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Lfunc_end32-.Ltmp310   #   Call between .Ltmp310 and .Lfunc_end32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev: # @_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi285:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi286:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi287:
	.cfi_def_cfa_offset 32
.Lcfi288:
	.cfi_offset %rbx, -24
.Lcfi289:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp312:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp313:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB33_2:
.Ltmp314:
	movq	%rax, %r14
.Ltmp315:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp316:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB33_4:
.Ltmp317:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end33:
	.size	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev, .Lfunc_end33-_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table33:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp312-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp313-.Ltmp312       #   Call between .Ltmp312 and .Ltmp313
	.long	.Ltmp314-.Lfunc_begin10 #     jumps to .Ltmp314
	.byte	0                       #   On action: cleanup
	.long	.Ltmp313-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp315-.Ltmp313       #   Call between .Ltmp313 and .Ltmp315
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp315-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp316-.Ltmp315       #   Call between .Ltmp315 and .Ltmp316
	.long	.Ltmp317-.Lfunc_begin10 #     jumps to .Ltmp317
	.byte	1                       #   On action: 1
	.long	.Ltmp316-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Lfunc_end33-.Ltmp316   #   Call between .Ltmp316 and .Lfunc_end33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev: # @_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi290:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi291:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi292:
	.cfi_def_cfa_offset 32
.Lcfi293:
	.cfi_offset %rbx, -24
.Lcfi294:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp318:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp319:
# BB#1:
.Ltmp324:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp325:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB34_5:
.Ltmp326:
	movq	%rax, %r14
	jmp	.LBB34_6
.LBB34_3:
.Ltmp320:
	movq	%rax, %r14
.Ltmp321:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp322:
.LBB34_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB34_4:
.Ltmp323:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end34:
	.size	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev, .Lfunc_end34-_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table34:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp318-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp319-.Ltmp318       #   Call between .Ltmp318 and .Ltmp319
	.long	.Ltmp320-.Lfunc_begin11 #     jumps to .Ltmp320
	.byte	0                       #   On action: cleanup
	.long	.Ltmp324-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp325-.Ltmp324       #   Call between .Ltmp324 and .Ltmp325
	.long	.Ltmp326-.Lfunc_begin11 #     jumps to .Ltmp326
	.byte	0                       #   On action: cleanup
	.long	.Ltmp321-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp322-.Ltmp321       #   Call between .Ltmp321 and .Ltmp322
	.long	.Ltmp323-.Lfunc_begin11 #     jumps to .Ltmp323
	.byte	1                       #   On action: 1
	.long	.Ltmp322-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Lfunc_end34-.Ltmp322   #   Call between .Ltmp322 and .Lfunc_end34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi295:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi296:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi297:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi298:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi299:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi300:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi301:
	.cfi_def_cfa_offset 64
.Lcfi302:
	.cfi_offset %rbx, -56
.Lcfi303:
	.cfi_offset %r12, -48
.Lcfi304:
	.cfi_offset %r13, -40
.Lcfi305:
	.cfi_offset %r14, -32
.Lcfi306:
	.cfi_offset %r15, -24
.Lcfi307:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB35_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB35_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB35_6
# BB#3:                                 #   in Loop: Header=BB35_2 Depth=1
	movq	$_ZTV7CBufferIhE+16, 8(%rbp)
	movq	24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB35_5
# BB#4:                                 #   in Loop: Header=BB35_2 Depth=1
	callq	_ZdaPv
.LBB35_5:                               # %_ZN8NArchive3N7z10CCoderInfoD2Ev.exit
                                        #   in Loop: Header=BB35_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB35_6:                               #   in Loop: Header=BB35_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB35_2
.LBB35_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end35:
	.size	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii, .Lfunc_end35-_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev,"axG",@progbits,_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev,comdat
	.weak	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev,@function
_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev: # @_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r14
.Lcfi308:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi309:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi310:
	.cfi_def_cfa_offset 32
.Lcfi311:
	.cfi_offset %rbx, -24
.Lcfi312:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp327:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp328:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB36_2:
.Ltmp329:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end36:
	.size	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev, .Lfunc_end36-_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table36:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp327-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp328-.Ltmp327       #   Call between .Ltmp327 and .Ltmp328
	.long	.Ltmp329-.Lfunc_begin12 #     jumps to .Ltmp329
	.byte	0                       #   On action: cleanup
	.long	.Ltmp328-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Lfunc_end36-.Ltmp328   #   Call between .Ltmp328 and .Lfunc_end36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev: # @_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r14
.Lcfi313:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi314:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi315:
	.cfi_def_cfa_offset 32
.Lcfi316:
	.cfi_offset %rbx, -24
.Lcfi317:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
.Ltmp330:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp331:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB37_2:
.Ltmp332:
	movq	%rax, %r14
.Ltmp333:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp334:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB37_4:
.Ltmp335:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end37:
	.size	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev, .Lfunc_end37-_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table37:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp330-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp331-.Ltmp330       #   Call between .Ltmp330 and .Ltmp331
	.long	.Ltmp332-.Lfunc_begin13 #     jumps to .Ltmp332
	.byte	0                       #   On action: cleanup
	.long	.Ltmp331-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp333-.Ltmp331       #   Call between .Ltmp331 and .Ltmp333
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp333-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Ltmp334-.Ltmp333       #   Call between .Ltmp333 and .Ltmp334
	.long	.Ltmp335-.Lfunc_begin13 #     jumps to .Ltmp335
	.byte	1                       #   On action: 1
	.long	.Ltmp334-.Lfunc_begin13 # >> Call Site 4 <<
	.long	.Lfunc_end37-.Ltmp334   #   Call between .Ltmp334 and .Lfunc_end37
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev: # @_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi318:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi319:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi320:
	.cfi_def_cfa_offset 32
.Lcfi321:
	.cfi_offset %rbx, -24
.Lcfi322:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
.Ltmp336:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp337:
# BB#1:
.Ltmp342:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp343:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB38_5:
.Ltmp344:
	movq	%rax, %r14
	jmp	.LBB38_6
.LBB38_3:
.Ltmp338:
	movq	%rax, %r14
.Ltmp339:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp340:
.LBB38_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB38_4:
.Ltmp341:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end38:
	.size	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev, .Lfunc_end38-_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table38:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp336-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp337-.Ltmp336       #   Call between .Ltmp336 and .Ltmp337
	.long	.Ltmp338-.Lfunc_begin14 #     jumps to .Ltmp338
	.byte	0                       #   On action: cleanup
	.long	.Ltmp342-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp343-.Ltmp342       #   Call between .Ltmp342 and .Ltmp343
	.long	.Ltmp344-.Lfunc_begin14 #     jumps to .Ltmp344
	.byte	0                       #   On action: cleanup
	.long	.Ltmp339-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp340-.Ltmp339       #   Call between .Ltmp339 and .Ltmp340
	.long	.Ltmp341-.Lfunc_begin14 #     jumps to .Ltmp341
	.byte	1                       #   On action: 1
	.long	.Ltmp340-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Lfunc_end38-.Ltmp340   #   Call between .Ltmp340 and .Lfunc_end38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi323:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi324:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi325:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi326:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi327:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi328:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi329:
	.cfi_def_cfa_offset 80
.Lcfi330:
	.cfi_offset %rbx, -56
.Lcfi331:
	.cfi_offset %r12, -48
.Lcfi332:
	.cfi_offset %r13, -40
.Lcfi333:
	.cfi_offset %r14, -32
.Lcfi334:
	.cfi_offset %r15, -24
.Lcfi335:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %edi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	subl	%esi, %edi
	cmpl	%ecx, %eax
	cmovlel	%edx, %edi
	movl	%edi, 12(%rsp)          # 4-byte Spill
	testl	%edi, %edi
	jle	.LBB39_7
# BB#1:                                 # %.lr.ph
	movslq	16(%rsp), %r13          # 4-byte Folded Reload
	movslq	12(%rsp), %r14          # 4-byte Folded Reload
	shlq	$3, %r13
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB39_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%r13, %rax
	movq	(%rax,%r15,8), %rbp
	testq	%rbp, %rbp
	je	.LBB39_6
# BB#3:                                 #   in Loop: Header=BB39_2 Depth=1
	leaq	8(%rbp), %rbx
	movq	$_ZTV13CObjectVectorI5CPropE+16, 8(%rbp)
.Ltmp345:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp346:
# BB#4:                                 # %_ZN13CObjectVectorI5CPropED2Ev.exit.i
                                        #   in Loop: Header=BB39_2 Depth=1
.Ltmp351:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp352:
# BB#5:                                 # %_ZN7CMethodD2Ev.exit
                                        #   in Loop: Header=BB39_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB39_6:                               #   in Loop: Header=BB39_2 Depth=1
	incq	%r15
	cmpq	%r14, %r15
	jl	.LBB39_2
.LBB39_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	12(%rsp), %edx          # 4-byte Reload
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB39_8:
.Ltmp347:
	movq	%rax, %r14
.Ltmp348:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp349:
	jmp	.LBB39_11
.LBB39_9:
.Ltmp350:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB39_10:
.Ltmp353:
	movq	%rax, %r14
.LBB39_11:                              # %.body
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end39:
	.size	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii, .Lfunc_end39-_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table39:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp345-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp346-.Ltmp345       #   Call between .Ltmp345 and .Ltmp346
	.long	.Ltmp347-.Lfunc_begin15 #     jumps to .Ltmp347
	.byte	0                       #   On action: cleanup
	.long	.Ltmp351-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Ltmp352-.Ltmp351       #   Call between .Ltmp351 and .Ltmp352
	.long	.Ltmp353-.Lfunc_begin15 #     jumps to .Ltmp353
	.byte	0                       #   On action: cleanup
	.long	.Ltmp352-.Lfunc_begin15 # >> Call Site 3 <<
	.long	.Ltmp348-.Ltmp352       #   Call between .Ltmp352 and .Ltmp348
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp348-.Lfunc_begin15 # >> Call Site 4 <<
	.long	.Ltmp349-.Ltmp348       #   Call between .Ltmp348 and .Ltmp349
	.long	.Ltmp350-.Lfunc_begin15 #     jumps to .Ltmp350
	.byte	1                       #   On action: 1
	.long	.Ltmp349-.Lfunc_begin15 # >> Call Site 5 <<
	.long	.Lfunc_end39-.Ltmp349   #   Call between .Ltmp349 and .Lfunc_end39
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropED2Ev,"axG",@progbits,_ZN13CObjectVectorI5CPropED2Ev,comdat
	.weak	_ZN13CObjectVectorI5CPropED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropED2Ev,@function
_ZN13CObjectVectorI5CPropED2Ev:         # @_ZN13CObjectVectorI5CPropED2Ev
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%r14
.Lcfi336:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi337:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi338:
	.cfi_def_cfa_offset 32
.Lcfi339:
	.cfi_offset %rbx, -24
.Lcfi340:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rbx)
.Ltmp354:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp355:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB40_2:
.Ltmp356:
	movq	%rax, %r14
.Ltmp357:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp358:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB40_4:
.Ltmp359:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end40:
	.size	_ZN13CObjectVectorI5CPropED2Ev, .Lfunc_end40-_ZN13CObjectVectorI5CPropED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table40:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp354-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp355-.Ltmp354       #   Call between .Ltmp354 and .Ltmp355
	.long	.Ltmp356-.Lfunc_begin16 #     jumps to .Ltmp356
	.byte	0                       #   On action: cleanup
	.long	.Ltmp355-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Ltmp357-.Ltmp355       #   Call between .Ltmp355 and .Ltmp357
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp357-.Lfunc_begin16 # >> Call Site 3 <<
	.long	.Ltmp358-.Ltmp357       #   Call between .Ltmp357 and .Ltmp358
	.long	.Ltmp359-.Lfunc_begin16 #     jumps to .Ltmp359
	.byte	1                       #   On action: 1
	.long	.Ltmp358-.Lfunc_begin16 # >> Call Site 4 <<
	.long	.Lfunc_end40-.Ltmp358   #   Call between .Ltmp358 and .Lfunc_end40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropED0Ev,"axG",@progbits,_ZN13CObjectVectorI5CPropED0Ev,comdat
	.weak	_ZN13CObjectVectorI5CPropED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropED0Ev,@function
_ZN13CObjectVectorI5CPropED0Ev:         # @_ZN13CObjectVectorI5CPropED0Ev
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%r14
.Lcfi341:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi342:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi343:
	.cfi_def_cfa_offset 32
.Lcfi344:
	.cfi_offset %rbx, -24
.Lcfi345:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rbx)
.Ltmp360:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp361:
# BB#1:
.Ltmp366:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp367:
# BB#2:                                 # %_ZN13CObjectVectorI5CPropED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB41_5:
.Ltmp368:
	movq	%rax, %r14
	jmp	.LBB41_6
.LBB41_3:
.Ltmp362:
	movq	%rax, %r14
.Ltmp363:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp364:
.LBB41_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB41_4:
.Ltmp365:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end41:
	.size	_ZN13CObjectVectorI5CPropED0Ev, .Lfunc_end41-_ZN13CObjectVectorI5CPropED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table41:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp360-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp361-.Ltmp360       #   Call between .Ltmp360 and .Ltmp361
	.long	.Ltmp362-.Lfunc_begin17 #     jumps to .Ltmp362
	.byte	0                       #   On action: cleanup
	.long	.Ltmp366-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Ltmp367-.Ltmp366       #   Call between .Ltmp366 and .Ltmp367
	.long	.Ltmp368-.Lfunc_begin17 #     jumps to .Ltmp368
	.byte	0                       #   On action: cleanup
	.long	.Ltmp363-.Lfunc_begin17 # >> Call Site 3 <<
	.long	.Ltmp364-.Ltmp363       #   Call between .Ltmp363 and .Ltmp364
	.long	.Ltmp365-.Lfunc_begin17 #     jumps to .Ltmp365
	.byte	1                       #   On action: 1
	.long	.Ltmp364-.Lfunc_begin17 # >> Call Site 4 <<
	.long	.Lfunc_end41-.Ltmp364   #   Call between .Ltmp364 and .Lfunc_end41
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI5CPropE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI5CPropE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropE6DeleteEii,@function
_ZN13CObjectVectorI5CPropE6DeleteEii:   # @_ZN13CObjectVectorI5CPropE6DeleteEii
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi346:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi347:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi348:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi349:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi350:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi351:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi352:
	.cfi_def_cfa_offset 64
.Lcfi353:
	.cfi_offset %rbx, -56
.Lcfi354:
	.cfi_offset %r12, -48
.Lcfi355:
	.cfi_offset %r13, -40
.Lcfi356:
	.cfi_offset %r14, -32
.Lcfi357:
	.cfi_offset %r15, -24
.Lcfi358:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB42_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB42_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB42_5
# BB#3:                                 #   in Loop: Header=BB42_2 Depth=1
	leaq	8(%rbp), %rdi
.Ltmp369:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp370:
# BB#4:                                 # %_ZN5CPropD2Ev.exit
                                        #   in Loop: Header=BB42_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB42_5:                               #   in Loop: Header=BB42_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB42_2
.LBB42_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB42_7:
.Ltmp371:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end42:
	.size	_ZN13CObjectVectorI5CPropE6DeleteEii, .Lfunc_end42-_ZN13CObjectVectorI5CPropE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table42:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp369-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp370-.Ltmp369       #   Call between .Ltmp369 and .Ltmp370
	.long	.Ltmp371-.Lfunc_begin18 #     jumps to .Ltmp371
	.byte	0                       #   On action: cleanup
	.long	.Ltmp370-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Lfunc_end42-.Ltmp370   #   Call between .Ltmp370 and .Lfunc_end42
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev,"axG",@progbits,_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev,comdat
	.weak	_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev,@function
_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev: # @_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev
.Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception19
# BB#0:
	pushq	%r14
.Lcfi359:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi360:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi361:
	.cfi_def_cfa_offset 32
.Lcfi362:
	.cfi_offset %rbx, -24
.Lcfi363:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp372:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp373:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB43_2:
.Ltmp374:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end43:
	.size	_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev, .Lfunc_end43-_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table43:
.Lexception19:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp372-.Lfunc_begin19 # >> Call Site 1 <<
	.long	.Ltmp373-.Ltmp372       #   Call between .Ltmp372 and .Ltmp373
	.long	.Ltmp374-.Lfunc_begin19 #     jumps to .Ltmp374
	.byte	0                       #   On action: cleanup
	.long	.Ltmp373-.Lfunc_begin19 # >> Call Site 2 <<
	.long	.Lfunc_end43-.Ltmp373   #   Call between .Ltmp373 and .Lfunc_end43
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN7CBufferIhED0Ev,"axG",@progbits,_ZN7CBufferIhED0Ev,comdat
	.weak	_ZN7CBufferIhED0Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED0Ev,@function
_ZN7CBufferIhED0Ev:                     # @_ZN7CBufferIhED0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi364:
	.cfi_def_cfa_offset 16
.Lcfi365:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV7CBufferIhE+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB44_2
# BB#1:
	callq	_ZdaPv
.LBB44_2:                               # %_ZN7CBufferIhED2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end44:
	.size	_ZN7CBufferIhED0Ev, .Lfunc_end44-_ZN7CBufferIhED0Ev
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIjED0Ev,"axG",@progbits,_ZN13CRecordVectorIjED0Ev,comdat
	.weak	_ZN13CRecordVectorIjED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIjED0Ev,@function
_ZN13CRecordVectorIjED0Ev:              # @_ZN13CRecordVectorIjED0Ev
.Lfunc_begin20:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception20
# BB#0:
	pushq	%r14
.Lcfi366:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi367:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi368:
	.cfi_def_cfa_offset 32
.Lcfi369:
	.cfi_offset %rbx, -24
.Lcfi370:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp375:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp376:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB45_2:
.Ltmp377:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end45:
	.size	_ZN13CRecordVectorIjED0Ev, .Lfunc_end45-_ZN13CRecordVectorIjED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table45:
.Lexception20:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp375-.Lfunc_begin20 # >> Call Site 1 <<
	.long	.Ltmp376-.Ltmp375       #   Call between .Ltmp375 and .Ltmp376
	.long	.Ltmp377-.Lfunc_begin20 #     jumps to .Ltmp377
	.byte	0                       #   On action: cleanup
	.long	.Ltmp376-.Lfunc_begin20 # >> Call Site 2 <<
	.long	.Lfunc_end45-.Ltmp376   #   Call between .Ltmp376 and .Lfunc_end45
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIyED0Ev,"axG",@progbits,_ZN13CRecordVectorIyED0Ev,comdat
	.weak	_ZN13CRecordVectorIyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyED0Ev,@function
_ZN13CRecordVectorIyED0Ev:              # @_ZN13CRecordVectorIyED0Ev
.Lfunc_begin21:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception21
# BB#0:
	pushq	%r14
.Lcfi371:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi372:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi373:
	.cfi_def_cfa_offset 32
.Lcfi374:
	.cfi_offset %rbx, -24
.Lcfi375:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp378:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp379:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB46_2:
.Ltmp380:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end46:
	.size	_ZN13CRecordVectorIyED0Ev, .Lfunc_end46-_ZN13CRecordVectorIyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table46:
.Lexception21:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp378-.Lfunc_begin21 # >> Call Site 1 <<
	.long	.Ltmp379-.Ltmp378       #   Call between .Ltmp378 and .Ltmp379
	.long	.Ltmp380-.Lfunc_begin21 #     jumps to .Ltmp380
	.byte	0                       #   On action: cleanup
	.long	.Ltmp379-.Lfunc_begin21 # >> Call Site 2 <<
	.long	.Lfunc_end46-.Ltmp379   #   Call between .Ltmp379 and .Lfunc_end46
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIbED0Ev,"axG",@progbits,_ZN13CRecordVectorIbED0Ev,comdat
	.weak	_ZN13CRecordVectorIbED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIbED0Ev,@function
_ZN13CRecordVectorIbED0Ev:              # @_ZN13CRecordVectorIbED0Ev
.Lfunc_begin22:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception22
# BB#0:
	pushq	%r14
.Lcfi376:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi377:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi378:
	.cfi_def_cfa_offset 32
.Lcfi379:
	.cfi_offset %rbx, -24
.Lcfi380:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp381:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp382:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB47_2:
.Ltmp383:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end47:
	.size	_ZN13CRecordVectorIbED0Ev, .Lfunc_end47-_ZN13CRecordVectorIbED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table47:
.Lexception22:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp381-.Lfunc_begin22 # >> Call Site 1 <<
	.long	.Ltmp382-.Ltmp381       #   Call between .Ltmp381 and .Ltmp382
	.long	.Ltmp383-.Lfunc_begin22 #     jumps to .Ltmp383
	.byte	0                       #   On action: cleanup
	.long	.Ltmp382-.Lfunc_begin22 # >> Call Site 2 <<
	.long	.Lfunc_end47-.Ltmp382   #   Call between .Ltmp382 and .Lfunc_end47
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NArchive3N7z7CFolderC2ERKS1_,"axG",@progbits,_ZN8NArchive3N7z7CFolderC2ERKS1_,comdat
	.weak	_ZN8NArchive3N7z7CFolderC2ERKS1_
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z7CFolderC2ERKS1_,@function
_ZN8NArchive3N7z7CFolderC2ERKS1_:       # @_ZN8NArchive3N7z7CFolderC2ERKS1_
.Lfunc_begin23:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception23
# BB#0:
	pushq	%r15
.Lcfi381:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi382:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi383:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi384:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi385:
	.cfi_def_cfa_offset 48
.Lcfi386:
	.cfi_offset %rbx, -48
.Lcfi387:
	.cfi_offset %r12, -40
.Lcfi388:
	.cfi_offset %r13, -32
.Lcfi389:
	.cfi_offset %r14, -24
.Lcfi390:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r12
	callq	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_
	leaq	32(%r12), %r14
	leaq	32(%rbx), %rsi
.Ltmp384:
	movq	%r14, %rdi
	callq	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_
.Ltmp385:
# BB#1:
	leaq	64(%r12), %r13
	leaq	64(%rbx), %rsi
.Ltmp387:
	movq	%r13, %rdi
	callq	_ZN13CRecordVectorIjEC2ERKS0_
.Ltmp388:
# BB#2:
	leaq	96(%r12), %rdi
	leaq	96(%rbx), %rsi
.Ltmp390:
	callq	_ZN13CRecordVectorIyEC2ERKS0_
.Ltmp391:
# BB#3:
	movb	132(%rbx), %al
	movb	%al, 132(%r12)
	movl	128(%rbx), %eax
	movl	%eax, 128(%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB48_6:
.Ltmp392:
	movq	%rax, %r15
.Ltmp393:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp394:
	jmp	.LBB48_7
.LBB48_5:
.Ltmp389:
	movq	%rax, %r15
.LBB48_7:
.Ltmp395:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp396:
	jmp	.LBB48_8
.LBB48_4:
.Ltmp386:
	movq	%rax, %r15
.LBB48_8:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%r12)
.Ltmp397:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp398:
# BB#9:
.Ltmp403:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp404:
# BB#10:                                # %_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev.exit
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB48_11:
.Ltmp399:
	movq	%rax, %rbx
.Ltmp400:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp401:
	jmp	.LBB48_14
.LBB48_12:
.Ltmp402:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB48_13:
.Ltmp405:
	movq	%rax, %rbx
.LBB48_14:                              # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end48:
	.size	_ZN8NArchive3N7z7CFolderC2ERKS1_, .Lfunc_end48-_ZN8NArchive3N7z7CFolderC2ERKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table48:
.Lexception23:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	125                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Lfunc_begin23-.Lfunc_begin23 # >> Call Site 1 <<
	.long	.Ltmp384-.Lfunc_begin23 #   Call between .Lfunc_begin23 and .Ltmp384
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp384-.Lfunc_begin23 # >> Call Site 2 <<
	.long	.Ltmp385-.Ltmp384       #   Call between .Ltmp384 and .Ltmp385
	.long	.Ltmp386-.Lfunc_begin23 #     jumps to .Ltmp386
	.byte	0                       #   On action: cleanup
	.long	.Ltmp387-.Lfunc_begin23 # >> Call Site 3 <<
	.long	.Ltmp388-.Ltmp387       #   Call between .Ltmp387 and .Ltmp388
	.long	.Ltmp389-.Lfunc_begin23 #     jumps to .Ltmp389
	.byte	0                       #   On action: cleanup
	.long	.Ltmp390-.Lfunc_begin23 # >> Call Site 4 <<
	.long	.Ltmp391-.Ltmp390       #   Call between .Ltmp390 and .Ltmp391
	.long	.Ltmp392-.Lfunc_begin23 #     jumps to .Ltmp392
	.byte	0                       #   On action: cleanup
	.long	.Ltmp393-.Lfunc_begin23 # >> Call Site 5 <<
	.long	.Ltmp396-.Ltmp393       #   Call between .Ltmp393 and .Ltmp396
	.long	.Ltmp405-.Lfunc_begin23 #     jumps to .Ltmp405
	.byte	1                       #   On action: 1
	.long	.Ltmp397-.Lfunc_begin23 # >> Call Site 6 <<
	.long	.Ltmp398-.Ltmp397       #   Call between .Ltmp397 and .Ltmp398
	.long	.Ltmp399-.Lfunc_begin23 #     jumps to .Ltmp399
	.byte	1                       #   On action: 1
	.long	.Ltmp403-.Lfunc_begin23 # >> Call Site 7 <<
	.long	.Ltmp404-.Ltmp403       #   Call between .Ltmp403 and .Ltmp404
	.long	.Ltmp405-.Lfunc_begin23 #     jumps to .Ltmp405
	.byte	1                       #   On action: 1
	.long	.Ltmp404-.Lfunc_begin23 # >> Call Site 8 <<
	.long	.Ltmp400-.Ltmp404       #   Call between .Ltmp404 and .Ltmp400
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp400-.Lfunc_begin23 # >> Call Site 9 <<
	.long	.Ltmp401-.Ltmp400       #   Call between .Ltmp400 and .Ltmp401
	.long	.Ltmp402-.Lfunc_begin23 #     jumps to .Ltmp402
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI49_0:
	.zero	16
	.section	.text._ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_,@function
_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_: # @_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_
.Lfunc_begin24:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception24
# BB#0:
	pushq	%rbp
.Lcfi391:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi392:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi393:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi394:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi395:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi396:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi397:
	.cfi_def_cfa_offset 80
.Lcfi398:
	.cfi_offset %rbx, -56
.Lcfi399:
	.cfi_offset %r12, -48
.Lcfi400:
	.cfi_offset %r13, -40
.Lcfi401:
	.cfi_offset %r14, -32
.Lcfi402:
	.cfi_offset %r15, -24
.Lcfi403:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbp)
	movq	$8, 24(%rbp)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbp)
.Ltmp406:
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp407:
# BB#1:                                 # %.noexc
	movl	12(%rbx), %r15d
	movl	12(%rbp), %esi
	addl	%r15d, %esi
.Ltmp408:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp409:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB49_10
# BB#3:                                 # %.lr.ph.i.i
	xorl	%r13d, %r13d
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB49_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rax
	movq	(%rax,%r13,8), %r14
.Ltmp411:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp412:
# BB#5:                                 # %.noexc5
                                        #   in Loop: Header=BB49_4 Depth=1
	movq	(%r14), %rax
	movq	%rax, (%rbx)
	movq	$_ZTV7CBufferIhE+16, 8(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movq	16(%r14), %r12
	testq	%r12, %r12
	je	.LBB49_8
# BB#6:                                 # %_ZN7CBufferIhE11SetCapacityEm.exit.i.i.i.i
                                        #   in Loop: Header=BB49_4 Depth=1
.Ltmp413:
	movq	%r12, %rdi
	callq	_Znam
.Ltmp414:
# BB#7:                                 # %.noexc.i
                                        #   in Loop: Header=BB49_4 Depth=1
	movq	%rax, 24(%rbx)
	movq	%r12, 16(%rbx)
	movq	24(%r14), %rsi
	movq	%rax, %rdi
	movq	%r12, %rdx
	callq	memmove
.LBB49_8:                               #   in Loop: Header=BB49_4 Depth=1
	movq	32(%r14), %rax
	movq	%rax, 32(%rbx)
.Ltmp416:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp417:
# BB#9:                                 # %.noexc4
                                        #   in Loop: Header=BB49_4 Depth=1
	movq	16(%rbp), %rax
	movslq	12(%rbp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rbp)
	incq	%r13
	cmpq	%r13, %r15
	movq	16(%rsp), %rbx          # 8-byte Reload
	jne	.LBB49_4
.LBB49_10:                              # %_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEaSERKS3_.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB49_12:                              # %.loopexit.split-lp
.Ltmp410:
	jmp	.LBB49_13
.LBB49_17:
.Ltmp415:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB49_14
.LBB49_11:                              # %.loopexit
.Ltmp418:
.LBB49_13:                              # %.body
	movq	%rax, %r14
.LBB49_14:                              # %.body
.Ltmp419:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp420:
# BB#15:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB49_16:
.Ltmp421:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end49:
	.size	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_, .Lfunc_end49-_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table49:
.Lexception24:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp406-.Lfunc_begin24 # >> Call Site 1 <<
	.long	.Ltmp409-.Ltmp406       #   Call between .Ltmp406 and .Ltmp409
	.long	.Ltmp410-.Lfunc_begin24 #     jumps to .Ltmp410
	.byte	0                       #   On action: cleanup
	.long	.Ltmp411-.Lfunc_begin24 # >> Call Site 2 <<
	.long	.Ltmp412-.Ltmp411       #   Call between .Ltmp411 and .Ltmp412
	.long	.Ltmp418-.Lfunc_begin24 #     jumps to .Ltmp418
	.byte	0                       #   On action: cleanup
	.long	.Ltmp413-.Lfunc_begin24 # >> Call Site 3 <<
	.long	.Ltmp414-.Ltmp413       #   Call between .Ltmp413 and .Ltmp414
	.long	.Ltmp415-.Lfunc_begin24 #     jumps to .Ltmp415
	.byte	0                       #   On action: cleanup
	.long	.Ltmp414-.Lfunc_begin24 # >> Call Site 4 <<
	.long	.Ltmp416-.Ltmp414       #   Call between .Ltmp414 and .Ltmp416
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp416-.Lfunc_begin24 # >> Call Site 5 <<
	.long	.Ltmp417-.Ltmp416       #   Call between .Ltmp416 and .Ltmp417
	.long	.Ltmp418-.Lfunc_begin24 #     jumps to .Ltmp418
	.byte	0                       #   On action: cleanup
	.long	.Ltmp419-.Lfunc_begin24 # >> Call Site 6 <<
	.long	.Ltmp420-.Ltmp419       #   Call between .Ltmp419 and .Ltmp420
	.long	.Ltmp421-.Lfunc_begin24 #     jumps to .Ltmp421
	.byte	1                       #   On action: 1
	.long	.Ltmp420-.Lfunc_begin24 # >> Call Site 7 <<
	.long	.Lfunc_end49-.Ltmp420   #   Call between .Ltmp420 and .Lfunc_end49
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_,"axG",@progbits,_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_,comdat
	.weak	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_,@function
_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_: # @_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_
.Lfunc_begin25:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception25
# BB#0:
	pushq	%r15
.Lcfi404:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi405:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi406:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi407:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi408:
	.cfi_def_cfa_offset 48
.Lcfi409:
	.cfi_offset %rbx, -48
.Lcfi410:
	.cfi_offset %r12, -40
.Lcfi411:
	.cfi_offset %r13, -32
.Lcfi412:
	.cfi_offset %r14, -24
.Lcfi413:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$8, 24(%r12)
	movq	$_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE+16, (%r12)
.Ltmp422:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp423:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp424:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp425:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB50_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB50_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %r13
.Ltmp427:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp428:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB50_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%r13, (%rax,%rcx,8)
	incl	12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB50_4
.LBB50_6:                               # %_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEaSERKS3_.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB50_8:                               # %.loopexit.split-lp
.Ltmp426:
	jmp	.LBB50_9
.LBB50_7:                               # %.loopexit
.Ltmp429:
.LBB50_9:
	movq	%rax, %r14
.Ltmp430:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp431:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB50_11:
.Ltmp432:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end50:
	.size	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_, .Lfunc_end50-_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table50:
.Lexception25:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp422-.Lfunc_begin25 # >> Call Site 1 <<
	.long	.Ltmp425-.Ltmp422       #   Call between .Ltmp422 and .Ltmp425
	.long	.Ltmp426-.Lfunc_begin25 #     jumps to .Ltmp426
	.byte	0                       #   On action: cleanup
	.long	.Ltmp427-.Lfunc_begin25 # >> Call Site 2 <<
	.long	.Ltmp428-.Ltmp427       #   Call between .Ltmp427 and .Ltmp428
	.long	.Ltmp429-.Lfunc_begin25 #     jumps to .Ltmp429
	.byte	0                       #   On action: cleanup
	.long	.Ltmp430-.Lfunc_begin25 # >> Call Site 3 <<
	.long	.Ltmp431-.Ltmp430       #   Call between .Ltmp430 and .Ltmp431
	.long	.Ltmp432-.Lfunc_begin25 #     jumps to .Ltmp432
	.byte	1                       #   On action: 1
	.long	.Ltmp431-.Lfunc_begin25 # >> Call Site 4 <<
	.long	.Lfunc_end50-.Ltmp431   #   Call between .Ltmp431 and .Lfunc_end50
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIjEC2ERKS0_,"axG",@progbits,_ZN13CRecordVectorIjEC2ERKS0_,comdat
	.weak	_ZN13CRecordVectorIjEC2ERKS0_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIjEC2ERKS0_,@function
_ZN13CRecordVectorIjEC2ERKS0_:          # @_ZN13CRecordVectorIjEC2ERKS0_
.Lfunc_begin26:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception26
# BB#0:
	pushq	%rbp
.Lcfi414:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi415:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi416:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi417:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi418:
	.cfi_def_cfa_offset 48
.Lcfi419:
	.cfi_offset %rbx, -48
.Lcfi420:
	.cfi_offset %r12, -40
.Lcfi421:
	.cfi_offset %r14, -32
.Lcfi422:
	.cfi_offset %r15, -24
.Lcfi423:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$4, 24(%r12)
	movq	$_ZTV13CRecordVectorIjE+16, (%r12)
.Ltmp433:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp434:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp435:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp436:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB51_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB51_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movl	(%rax,%rbx,4), %ebp
.Ltmp438:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp439:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB51_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB51_4
.LBB51_6:                               # %_ZN13CRecordVectorIjEaSERKS0_.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB51_8:                               # %.loopexit.split-lp
.Ltmp437:
	jmp	.LBB51_9
.LBB51_7:                               # %.loopexit
.Ltmp440:
.LBB51_9:
	movq	%rax, %r14
.Ltmp441:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp442:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB51_11:
.Ltmp443:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end51:
	.size	_ZN13CRecordVectorIjEC2ERKS0_, .Lfunc_end51-_ZN13CRecordVectorIjEC2ERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table51:
.Lexception26:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp433-.Lfunc_begin26 # >> Call Site 1 <<
	.long	.Ltmp436-.Ltmp433       #   Call between .Ltmp433 and .Ltmp436
	.long	.Ltmp437-.Lfunc_begin26 #     jumps to .Ltmp437
	.byte	0                       #   On action: cleanup
	.long	.Ltmp438-.Lfunc_begin26 # >> Call Site 2 <<
	.long	.Ltmp439-.Ltmp438       #   Call between .Ltmp438 and .Ltmp439
	.long	.Ltmp440-.Lfunc_begin26 #     jumps to .Ltmp440
	.byte	0                       #   On action: cleanup
	.long	.Ltmp441-.Lfunc_begin26 # >> Call Site 3 <<
	.long	.Ltmp442-.Ltmp441       #   Call between .Ltmp441 and .Ltmp442
	.long	.Ltmp443-.Lfunc_begin26 #     jumps to .Ltmp443
	.byte	1                       #   On action: 1
	.long	.Ltmp442-.Lfunc_begin26 # >> Call Site 4 <<
	.long	.Lfunc_end51-.Ltmp442   #   Call between .Ltmp442 and .Lfunc_end51
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIyEC2ERKS0_,"axG",@progbits,_ZN13CRecordVectorIyEC2ERKS0_,comdat
	.weak	_ZN13CRecordVectorIyEC2ERKS0_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyEC2ERKS0_,@function
_ZN13CRecordVectorIyEC2ERKS0_:          # @_ZN13CRecordVectorIyEC2ERKS0_
.Lfunc_begin27:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception27
# BB#0:
	pushq	%r15
.Lcfi424:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi425:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi426:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi427:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi428:
	.cfi_def_cfa_offset 48
.Lcfi429:
	.cfi_offset %rbx, -48
.Lcfi430:
	.cfi_offset %r12, -40
.Lcfi431:
	.cfi_offset %r13, -32
.Lcfi432:
	.cfi_offset %r14, -24
.Lcfi433:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$8, 24(%r12)
	movq	$_ZTV13CRecordVectorIyE+16, (%r12)
.Ltmp444:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp445:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp446:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp447:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB52_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB52_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %r13
.Ltmp449:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp450:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB52_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB52_4
.LBB52_6:                               # %_ZN13CRecordVectorIyEaSERKS0_.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB52_8:                               # %.loopexit.split-lp
.Ltmp448:
	jmp	.LBB52_9
.LBB52_7:                               # %.loopexit
.Ltmp451:
.LBB52_9:
	movq	%rax, %r14
.Ltmp452:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp453:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB52_11:
.Ltmp454:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end52:
	.size	_ZN13CRecordVectorIyEC2ERKS0_, .Lfunc_end52-_ZN13CRecordVectorIyEC2ERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table52:
.Lexception27:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp444-.Lfunc_begin27 # >> Call Site 1 <<
	.long	.Ltmp447-.Ltmp444       #   Call between .Ltmp444 and .Ltmp447
	.long	.Ltmp448-.Lfunc_begin27 #     jumps to .Ltmp448
	.byte	0                       #   On action: cleanup
	.long	.Ltmp449-.Lfunc_begin27 # >> Call Site 2 <<
	.long	.Ltmp450-.Ltmp449       #   Call between .Ltmp449 and .Ltmp450
	.long	.Ltmp451-.Lfunc_begin27 #     jumps to .Ltmp451
	.byte	0                       #   On action: cleanup
	.long	.Ltmp452-.Lfunc_begin27 # >> Call Site 3 <<
	.long	.Ltmp453-.Ltmp452       #   Call between .Ltmp452 and .Ltmp453
	.long	.Ltmp454-.Lfunc_begin27 #     jumps to .Ltmp454
	.byte	1                       #   On action: 1
	.long	.Ltmp453-.Lfunc_begin27 # >> Call Site 4 <<
	.long	.Lfunc_end52-.Ltmp453   #   Call between .Ltmp453 and .Lfunc_end52
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev: # @_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev
.Lfunc_begin28:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception28
# BB#0:
	pushq	%r14
.Lcfi434:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi435:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi436:
	.cfi_def_cfa_offset 32
.Lcfi437:
	.cfi_offset %rbx, -24
.Lcfi438:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE+16, (%rbx)
.Ltmp455:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp456:
# BB#1:
.Ltmp461:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp462:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB53_5:
.Ltmp463:
	movq	%rax, %r14
	jmp	.LBB53_6
.LBB53_3:
.Ltmp457:
	movq	%rax, %r14
.Ltmp458:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp459:
.LBB53_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB53_4:
.Ltmp460:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end53:
	.size	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev, .Lfunc_end53-_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table53:
.Lexception28:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp455-.Lfunc_begin28 # >> Call Site 1 <<
	.long	.Ltmp456-.Ltmp455       #   Call between .Ltmp455 and .Ltmp456
	.long	.Ltmp457-.Lfunc_begin28 #     jumps to .Ltmp457
	.byte	0                       #   On action: cleanup
	.long	.Ltmp461-.Lfunc_begin28 # >> Call Site 2 <<
	.long	.Ltmp462-.Ltmp461       #   Call between .Ltmp461 and .Ltmp462
	.long	.Ltmp463-.Lfunc_begin28 #     jumps to .Ltmp463
	.byte	0                       #   On action: cleanup
	.long	.Ltmp458-.Lfunc_begin28 # >> Call Site 3 <<
	.long	.Ltmp459-.Ltmp458       #   Call between .Ltmp458 and .Ltmp459
	.long	.Ltmp460-.Lfunc_begin28 #     jumps to .Ltmp460
	.byte	1                       #   On action: 1
	.long	.Ltmp459-.Lfunc_begin28 # >> Call Site 4 <<
	.long	.Lfunc_end53-.Ltmp459   #   Call between .Ltmp459 and .Lfunc_end53
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii
.Lfunc_begin29:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception29
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi439:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi440:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi441:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi442:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi443:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi444:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi445:
	.cfi_def_cfa_offset 64
.Lcfi446:
	.cfi_offset %rbx, -56
.Lcfi447:
	.cfi_offset %r12, -48
.Lcfi448:
	.cfi_offset %r13, -40
.Lcfi449:
	.cfi_offset %r14, -32
.Lcfi450:
	.cfi_offset %r15, -24
.Lcfi451:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB54_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB54_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB54_5
# BB#3:                                 #   in Loop: Header=BB54_2 Depth=1
.Ltmp464:
	movq	%rbp, %rdi
	callq	_ZN8NArchive3N7z7CFolderD2Ev
.Ltmp465:
# BB#4:                                 #   in Loop: Header=BB54_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB54_5:                               #   in Loop: Header=BB54_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB54_2
.LBB54_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB54_7:
.Ltmp466:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end54:
	.size	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii, .Lfunc_end54-_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table54:
.Lexception29:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp464-.Lfunc_begin29 # >> Call Site 1 <<
	.long	.Ltmp465-.Ltmp464       #   Call between .Ltmp464 and .Ltmp465
	.long	.Ltmp466-.Lfunc_begin29 #     jumps to .Ltmp466
	.byte	0                       #   On action: cleanup
	.long	.Ltmp465-.Lfunc_begin29 # >> Call Site 2 <<
	.long	.Lfunc_end54-.Ltmp465   #   Call between .Ltmp465 and .Lfunc_end54
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,@object # @_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.quad	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,@object # @_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE:
	.asciz	"13CObjectVectorIN8NArchive3N7z10CCoderInfoEE"
	.size	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE, 45

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,@object # @_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE, 24

	.type	_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE,@object # @_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.section	.rodata._ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE,"aG",@progbits,_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE,comdat
	.weak	_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.p2align	3
_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE:
	.quad	0
	.quad	_ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE, 40

	.type	_ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE,@object # @_ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.section	.rodata._ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE,"aG",@progbits,_ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE,comdat
	.weak	_ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.p2align	4
_ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE:
	.asciz	"13CRecordVectorIN8NArchive3N7z9CBindPairEE"
	.size	_ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE, 43

	.type	_ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE,@object # @_ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.section	.rodata._ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE,"aG",@progbits,_ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE,comdat
	.weak	_ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.p2align	4
_ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE, 24

	.type	_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE,@object # @_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.quad	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE,@object # @_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE:
	.asciz	"13CObjectVectorIN8NArchive3N7z11CMethodFullEE"
	.size	_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE, 46

	.type	_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE,@object # @_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE, 24

	.type	_ZTV13CObjectVectorI5CPropE,@object # @_ZTV13CObjectVectorI5CPropE
	.section	.rodata._ZTV13CObjectVectorI5CPropE,"aG",@progbits,_ZTV13CObjectVectorI5CPropE,comdat
	.weak	_ZTV13CObjectVectorI5CPropE
	.p2align	3
_ZTV13CObjectVectorI5CPropE:
	.quad	0
	.quad	_ZTI13CObjectVectorI5CPropE
	.quad	_ZN13CObjectVectorI5CPropED2Ev
	.quad	_ZN13CObjectVectorI5CPropED0Ev
	.quad	_ZN13CObjectVectorI5CPropE6DeleteEii
	.size	_ZTV13CObjectVectorI5CPropE, 40

	.type	_ZTS13CObjectVectorI5CPropE,@object # @_ZTS13CObjectVectorI5CPropE
	.section	.rodata._ZTS13CObjectVectorI5CPropE,"aG",@progbits,_ZTS13CObjectVectorI5CPropE,comdat
	.weak	_ZTS13CObjectVectorI5CPropE
	.p2align	4
_ZTS13CObjectVectorI5CPropE:
	.asciz	"13CObjectVectorI5CPropE"
	.size	_ZTS13CObjectVectorI5CPropE, 24

	.type	_ZTI13CObjectVectorI5CPropE,@object # @_ZTI13CObjectVectorI5CPropE
	.section	.rodata._ZTI13CObjectVectorI5CPropE,"aG",@progbits,_ZTI13CObjectVectorI5CPropE,comdat
	.weak	_ZTI13CObjectVectorI5CPropE
	.p2align	4
_ZTI13CObjectVectorI5CPropE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI5CPropE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI5CPropE, 24

	.type	_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE,@object # @_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE
	.section	.rodata._ZTV13CRecordVectorIN8NArchive3N7z5CBindEE,"aG",@progbits,_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE,comdat
	.weak	_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE
	.p2align	3
_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE:
	.quad	0
	.quad	_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE, 40

	.type	_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE,@object # @_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE
	.section	.rodata._ZTS13CRecordVectorIN8NArchive3N7z5CBindEE,"aG",@progbits,_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE,comdat
	.weak	_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE
	.p2align	4
_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE:
	.asciz	"13CRecordVectorIN8NArchive3N7z5CBindEE"
	.size	_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE, 39

	.type	_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE,@object # @_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE
	.section	.rodata._ZTI13CRecordVectorIN8NArchive3N7z5CBindEE,"aG",@progbits,_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE,comdat
	.weak	_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE
	.p2align	4
_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE, 24

	.type	_ZTV7CBufferIhE,@object # @_ZTV7CBufferIhE
	.section	.rodata._ZTV7CBufferIhE,"aG",@progbits,_ZTV7CBufferIhE,comdat
	.weak	_ZTV7CBufferIhE
	.p2align	3
_ZTV7CBufferIhE:
	.quad	0
	.quad	_ZTI7CBufferIhE
	.quad	_ZN7CBufferIhED2Ev
	.quad	_ZN7CBufferIhED0Ev
	.size	_ZTV7CBufferIhE, 32

	.type	_ZTS7CBufferIhE,@object # @_ZTS7CBufferIhE
	.section	.rodata._ZTS7CBufferIhE,"aG",@progbits,_ZTS7CBufferIhE,comdat
	.weak	_ZTS7CBufferIhE
_ZTS7CBufferIhE:
	.asciz	"7CBufferIhE"
	.size	_ZTS7CBufferIhE, 12

	.type	_ZTI7CBufferIhE,@object # @_ZTI7CBufferIhE
	.section	.rodata._ZTI7CBufferIhE,"aG",@progbits,_ZTI7CBufferIhE,comdat
	.weak	_ZTI7CBufferIhE
	.p2align	3
_ZTI7CBufferIhE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS7CBufferIhE
	.size	_ZTI7CBufferIhE, 16

	.type	_ZTV13CRecordVectorIjE,@object # @_ZTV13CRecordVectorIjE
	.section	.rodata._ZTV13CRecordVectorIjE,"aG",@progbits,_ZTV13CRecordVectorIjE,comdat
	.weak	_ZTV13CRecordVectorIjE
	.p2align	3
_ZTV13CRecordVectorIjE:
	.quad	0
	.quad	_ZTI13CRecordVectorIjE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIjED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIjE, 40

	.type	_ZTS13CRecordVectorIjE,@object # @_ZTS13CRecordVectorIjE
	.section	.rodata._ZTS13CRecordVectorIjE,"aG",@progbits,_ZTS13CRecordVectorIjE,comdat
	.weak	_ZTS13CRecordVectorIjE
	.p2align	4
_ZTS13CRecordVectorIjE:
	.asciz	"13CRecordVectorIjE"
	.size	_ZTS13CRecordVectorIjE, 19

	.type	_ZTI13CRecordVectorIjE,@object # @_ZTI13CRecordVectorIjE
	.section	.rodata._ZTI13CRecordVectorIjE,"aG",@progbits,_ZTI13CRecordVectorIjE,comdat
	.weak	_ZTI13CRecordVectorIjE
	.p2align	4
_ZTI13CRecordVectorIjE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIjE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIjE, 24

	.type	_ZTV13CRecordVectorIyE,@object # @_ZTV13CRecordVectorIyE
	.section	.rodata._ZTV13CRecordVectorIyE,"aG",@progbits,_ZTV13CRecordVectorIyE,comdat
	.weak	_ZTV13CRecordVectorIyE
	.p2align	3
_ZTV13CRecordVectorIyE:
	.quad	0
	.quad	_ZTI13CRecordVectorIyE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIyED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIyE, 40

	.type	_ZTS13CRecordVectorIyE,@object # @_ZTS13CRecordVectorIyE
	.section	.rodata._ZTS13CRecordVectorIyE,"aG",@progbits,_ZTS13CRecordVectorIyE,comdat
	.weak	_ZTS13CRecordVectorIyE
	.p2align	4
_ZTS13CRecordVectorIyE:
	.asciz	"13CRecordVectorIyE"
	.size	_ZTS13CRecordVectorIyE, 19

	.type	_ZTI13CRecordVectorIyE,@object # @_ZTI13CRecordVectorIyE
	.section	.rodata._ZTI13CRecordVectorIyE,"aG",@progbits,_ZTI13CRecordVectorIyE,comdat
	.weak	_ZTI13CRecordVectorIyE
	.p2align	4
_ZTI13CRecordVectorIyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIyE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIyE, 24

	.type	_ZTV13CRecordVectorIbE,@object # @_ZTV13CRecordVectorIbE
	.section	.rodata._ZTV13CRecordVectorIbE,"aG",@progbits,_ZTV13CRecordVectorIbE,comdat
	.weak	_ZTV13CRecordVectorIbE
	.p2align	3
_ZTV13CRecordVectorIbE:
	.quad	0
	.quad	_ZTI13CRecordVectorIbE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIbED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIbE, 40

	.type	_ZTS13CRecordVectorIbE,@object # @_ZTS13CRecordVectorIbE
	.section	.rodata._ZTS13CRecordVectorIbE,"aG",@progbits,_ZTS13CRecordVectorIbE,comdat
	.weak	_ZTS13CRecordVectorIbE
	.p2align	4
_ZTS13CRecordVectorIbE:
	.asciz	"13CRecordVectorIbE"
	.size	_ZTS13CRecordVectorIbE, 19

	.type	_ZTI13CRecordVectorIbE,@object # @_ZTI13CRecordVectorIbE
	.section	.rodata._ZTI13CRecordVectorIbE,"aG",@progbits,_ZTI13CRecordVectorIbE,comdat
	.weak	_ZTI13CRecordVectorIbE
	.p2align	4
_ZTI13CRecordVectorIbE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIbE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIbE, 24

	.type	_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE,@object # @_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE
	.quad	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE,@object # @_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE:
	.asciz	"13CObjectVectorIN8NArchive3N7z7CFolderEE"
	.size	_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE, 41

	.type	_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE,@object # @_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
