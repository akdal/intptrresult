	.text
	.file	"btContinuousConvexCollision.bc"
	.globl	_ZN27btContinuousConvexCollisionC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.p2align	4, 0x90
	.type	_ZN27btContinuousConvexCollisionC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver,@function
_ZN27btContinuousConvexCollisionC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver: # @_ZN27btContinuousConvexCollisionC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.cfi_startproc
# BB#0:
	movq	$_ZTV27btContinuousConvexCollision+16, (%rdi)
	movq	%rcx, 8(%rdi)
	movq	%r8, 16(%rdi)
	movq	%rsi, 24(%rdi)
	movq	%rdx, 32(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN27btContinuousConvexCollisionC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver, .Lfunc_end0-_ZN27btContinuousConvexCollisionC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	981668463               # float 0.00100000005
.LCPI1_1:
	.long	1045220557              # float 0.200000003
.LCPI1_2:
	.long	872415232               # float 1.1920929E-7
.LCPI1_3:
	.long	1065353216              # float 1
.LCPI1_4:
	.long	0                       # float 0
	.text
	.globl	_ZN27btContinuousConvexCollision16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE
	.p2align	4, 0x90
	.type	_ZN27btContinuousConvexCollision16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE,@function
_ZN27btContinuousConvexCollision16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE: # @_ZN27btContinuousConvexCollision16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$808, %rsp              # imm = 0x328
.Lcfi6:
	.cfi_def_cfa_offset 864
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movq	%r8, %r14
	movq	%rcx, %r15
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	movq	%rdi, %r13
	movq	8(%r13), %rdi
	callq	_ZN22btVoronoiSimplexSolver5resetEv
	movsd	48(%rbx), %xmm0         # xmm0 = mem[0],zero
	movsd	48(%rbp), %xmm1         # xmm1 = mem[0],zero
	subps	%xmm1, %xmm0
	movss	56(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	56(%rbp), %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm2, 168(%rsp)
	leaq	288(%rsp), %rdx
	leaq	488(%rsp), %rcx
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
	movss	488(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movsd	288(%rsp), %xmm0        # xmm0 = mem[0],zero
	movaps	%xmm2, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm0, %xmm1
	movaps	%xmm1, 128(%rsp)        # 16-byte Spill
	mulss	296(%rsp), %xmm2
	movaps	%xmm2, 80(%rsp)         # 16-byte Spill
	movaps	%xmm1, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm1, 472(%rsp)
	movlps	%xmm0, 480(%rsp)
	movsd	48(%r14), %xmm1         # xmm1 = mem[0],zero
	movsd	48(%r15), %xmm0         # xmm0 = mem[0],zero
	subps	%xmm0, %xmm1
	movss	56(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	56(%r15), %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, 12(%rsp)         # 4-byte Spill
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movaps	%xmm1, 144(%rsp)        # 16-byte Spill
	movlps	%xmm1, 456(%rsp)
	movlps	%xmm0, 464(%rsp)
	leaq	288(%rsp), %rdx
	leaq	488(%rsp), %rcx
	movq	%r15, 112(%rsp)         # 8-byte Spill
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
	movss	488(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movsd	288(%rsp), %xmm0        # xmm0 = mem[0],zero
	movaps	%xmm1, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm0, %xmm2
	mulss	296(%rsp), %xmm1
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movlps	%xmm2, 440(%rsp)
	movaps	%xmm2, 48(%rsp)         # 16-byte Spill
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movaps	%xmm2, 64(%rsp)         # 16-byte Spill
	movlps	%xmm0, 448(%rsp)
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movq	32(%r13), %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	movaps	80(%rsp), %xmm1         # 16-byte Reload
	movaps	%xmm0, %xmm3
	movaps	128(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm2, %xmm2
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB1_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
.LBB1_2:                                # %.split
	movss	8(%rsp), %xmm4          # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	mulss	%xmm0, %xmm0
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB1_4
# BB#3:                                 # %call.sqrt588
	movaps	%xmm1, %xmm0
	movss	%xmm4, 8(%rsp)          # 4-byte Spill
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm4          # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
.LBB1_4:                                # %.split.split
	mulss	%xmm0, %xmm3
	addss	%xmm3, %xmm4
	movsd	160(%rsp), %xmm0        # xmm0 = mem[0],zero
	movaps	144(%rsp), %xmm1        # 16-byte Reload
	subps	%xmm0, %xmm1
	movaps	%xmm1, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	subss	168(%rsp), %xmm3
	movaps	%xmm1, 144(%rsp)        # 16-byte Spill
	movaps	%xmm1, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm2, 32(%rsp)         # 16-byte Spill
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	movss	%xmm4, 8(%rsp)          # 4-byte Spill
	jnp	.LBB1_6
# BB#5:                                 # %call.sqrt589
	movaps	%xmm1, %xmm0
	callq	sqrtf
	movss	8(%rsp), %xmm4          # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
.LBB1_6:                                # %.split.split.split
	addss	%xmm4, %xmm0
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB1_8
	jp	.LBB1_8
# BB#7:
	xorl	%ebp, %ebp
	jmp	.LBB1_43
.LBB1_8:
	leaq	584(%rsp), %rdi
	callq	_ZN21btConvexInternalShapeC2Ev
	movq	$_ZTV13btSphereShape+16, 584(%rsp)
	movl	$8, 592(%rsp)
	movl	$0, 624(%rsp)
	movl	$0, 640(%rsp)
	movq	$_ZTV16btPointCollector+16, 240(%rsp)
	movl	$1566444395, 280(%rsp)  # imm = 0x5D5E0B6B
	movb	$0, 284(%rsp)
	movq	24(%r13), %rbp
	movq	32(%r13), %rbx
	movl	8(%rbp), %r14d
	movl	8(%rbx), %r15d
	movq	(%rbp), %rax
.Ltmp0:
	movq	%rbp, %rdi
	callq	*88(%rax)
	movss	%xmm0, 48(%rsp)         # 4-byte Spill
.Ltmp1:
# BB#9:
	movq	32(%r13), %rdi
	movq	(%rdi), %rax
.Ltmp2:
	callq	*88(%rax)
	movaps	%xmm0, %xmm1
.Ltmp3:
# BB#10:
	movq	8(%r13), %r9
	movq	16(%r13), %rax
.Ltmp4:
	movq	%rax, (%rsp)
	leaq	488(%rsp), %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	movl	%r14d, %ecx
	movl	%r15d, %r8d
	movss	48(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_iiffP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
.Ltmp5:
# BB#11:
	movl	$1566444395, 416(%rsp)  # imm = 0x5D5E0B6B
	movq	$0, 424(%rsp)
	movq	120(%rsp), %rax         # 8-byte Reload
	movups	(%rax), %xmm0
	movaps	%xmm0, 288(%rsp)
	movups	16(%rax), %xmm0
	movaps	%xmm0, 304(%rsp)
	movups	32(%rax), %xmm0
	movaps	%xmm0, 320(%rsp)
	movups	48(%rax), %xmm0
	movaps	%xmm0, 336(%rsp)
	movq	112(%rsp), %rax         # 8-byte Reload
	movups	(%rax), %xmm0
	movaps	%xmm0, 352(%rsp)
	movups	16(%rax), %xmm0
	movaps	%xmm0, 368(%rsp)
	movups	32(%rax), %xmm0
	movaps	%xmm0, 384(%rsp)
	movups	48(%rax), %xmm0
	movaps	%xmm0, 400(%rsp)
.Ltmp7:
	leaq	488(%rsp), %rdi
	leaq	288(%rsp), %rsi
	leaq	240(%rsp), %rdx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	callq	_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb
.Ltmp8:
# BB#12:
	movb	284(%rsp), %al
	movups	264(%rsp), %xmm0
	movaps	%xmm0, 224(%rsp)
	testb	%al, %al
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	je	.LBB1_41
# BB#13:
	movss	280(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 48(%rsp)         # 4-byte Spill
	movq	248(%rsp), %xmm0        # xmm0 = mem[0],zero
	pshufd	$212, %xmm0, %xmm0      # xmm0 = xmm0[0,1,1,3]
	movl	256(%rsp), %eax
	movss	260(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 80(%rsp)         # 4-byte Spill
	movdqa	%xmm0, 16(%rsp)         # 16-byte Spill
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	movaps	144(%rsp), %xmm1        # 16-byte Reload
	mulss	%xmm0, %xmm1
	pshufd	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	32(%rsp), %xmm0         # 16-byte Folded Reload
	addss	%xmm1, %xmm0
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movd	%eax, %xmm1
	mulss	12(%rsp), %xmm1         # 4-byte Folded Reload
	addss	%xmm0, %xmm1
	movss	%xmm1, 128(%rsp)        # 4-byte Spill
	leaq	304(%rsp), %r15
	leaq	152(%r12), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	$-1, %r14d
	xorps	%xmm3, %xmm3
                                        # implicit-def: %BL
	.p2align	4, 0x90
.LBB1_14:                               # =>This Inner Loop Header: Depth=1
	movaps	%xmm3, %xmm4
	movss	48(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	ucomiss	.LCPI1_0(%rip), %xmm0
	jbe	.LBB1_37
# BB#15:                                #   in Loop: Header=BB1_14 Depth=1
	movq	176(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB1_18
# BB#16:                                #   in Loop: Header=BB1_14 Depth=1
	movss	%xmm4, 32(%rsp)         # 4-byte Spill
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	movabsq	$4575657222473777152, %rcx # imm = 0x3F8000003F800000
	movq	%rcx, 288(%rsp)
	movq	$1065353216, 296(%rsp)  # imm = 0x3F800000
.Ltmp10:
	leaq	224(%rsp), %rsi
	movss	.LCPI1_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	leaq	288(%rsp), %rdx
	callq	*%rax
.Ltmp11:
# BB#17:                                #   in Loop: Header=BB1_14 Depth=1
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
.LBB1_18:                               #   in Loop: Header=BB1_14 Depth=1
	incl	%r14d
	cmpl	$63, %r14d
	jg	.LBB1_41
# BB#19:                                #   in Loop: Header=BB1_14 Depth=1
	pshufd	$232, 16(%rsp), %xmm0   # 16-byte Folded Reload
                                        # xmm0 = mem[0,2,2,3]
	mulps	144(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	%xmm0, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	addss	%xmm0, %xmm1
	movd	64(%rsp), %xmm3         # 4-byte Folded Reload
	mulss	12(%rsp), %xmm3         # 4-byte Folded Reload
	addss	%xmm1, %xmm3
	movaps	%xmm2, %xmm0
	movss	%xmm3, 128(%rsp)        # 4-byte Spill
	addss	%xmm3, %xmm0
	movss	.LCPI1_2(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB1_41
# BB#20:                                #   in Loop: Header=BB1_14 Depth=1
	movss	48(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	addss	%xmm4, %xmm1
	xorl	%ebp, %ebp
	ucomiss	%xmm1, %xmm4
	jae	.LBB1_42
# BB#21:                                #   in Loop: Header=BB1_14 Depth=1
	xorps	%xmm0, %xmm0
	ucomiss	%xmm1, %xmm0
	ja	.LBB1_42
# BB#22:                                #   in Loop: Header=BB1_14 Depth=1
	ucomiss	.LCPI1_3(%rip), %xmm1
	ja	.LBB1_42
# BB#23:                                #   in Loop: Header=BB1_14 Depth=1
.Ltmp13:
	movq	120(%rsp), %rdi         # 8-byte Reload
	leaq	160(%rsp), %rsi
	leaq	472(%rsp), %rdx
	movss	%xmm1, 32(%rsp)         # 4-byte Spill
	movaps	%xmm1, %xmm0
	leaq	488(%rsp), %rcx
	callq	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
.Ltmp14:
	leaq	712(%rsp), %rbp
# BB#24:                                #   in Loop: Header=BB1_14 Depth=1
.Ltmp15:
	movq	112(%rsp), %rdi         # 8-byte Reload
	leaq	456(%rsp), %rsi
	leaq	440(%rsp), %rdx
	movss	32(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	leaq	648(%rsp), %rcx
	callq	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
.Ltmp16:
# BB#25:                                #   in Loop: Header=BB1_14 Depth=1
	movq	176(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB1_27
# BB#26:                                #   in Loop: Header=BB1_14 Depth=1
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	movq	$1065353216, 288(%rsp)  # imm = 0x3F800000
	movq	$0, 296(%rsp)
.Ltmp17:
	leaq	536(%rsp), %rsi
	movss	.LCPI1_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	leaq	288(%rsp), %rdx
	callq	*%rax
.Ltmp18:
.LBB1_27:                               #   in Loop: Header=BB1_14 Depth=1
	movq	(%r12), %rax
.Ltmp20:
	movq	%r12, %rdi
	movss	32(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	*(%rax)
.Ltmp21:
# BB#28:                                #   in Loop: Header=BB1_14 Depth=1
	movq	$_ZTV16btPointCollector+16, 176(%rsp)
	movl	$1566444395, 216(%rsp)  # imm = 0x5D5E0B6B
	movb	$0, 220(%rsp)
	movq	24(%r13), %rsi
	movq	32(%r13), %rdx
	movq	8(%r13), %rcx
	movq	16(%r13), %r8
.Ltmp23:
	movq	%rbp, %rdi
	callq	_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
.Ltmp24:
# BB#29:                                #   in Loop: Header=BB1_14 Depth=1
	movl	$1566444395, 416(%rsp)  # imm = 0x5D5E0B6B
	movq	$0, 424(%rsp)
	movups	488(%rsp), %xmm0
	movaps	%xmm0, 288(%rsp)
	leaq	504(%rsp), %rax
	movups	(%rax), %xmm0
	movups	%xmm0, (%r15)
	movups	16(%rax), %xmm0
	movups	%xmm0, 16(%r15)
	leaq	536(%rsp), %rax
	movups	(%rax), %xmm0
	movups	%xmm0, 32(%r15)
	movups	648(%rsp), %xmm0
	movups	%xmm0, 48(%r15)
	leaq	664(%rsp), %rax
	movups	(%rax), %xmm0
	movups	%xmm0, 64(%r15)
	movups	16(%rax), %xmm0
	movups	%xmm0, 80(%r15)
	movups	32(%rax), %xmm0
	movups	%xmm0, 96(%r15)
.Ltmp26:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rbp, %rdi
	leaq	288(%rsp), %rsi
	leaq	176(%rsp), %rdx
	callq	_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb
.Ltmp27:
# BB#30:                                #   in Loop: Header=BB1_14 Depth=1
	cmpb	$0, 220(%rsp)
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movl	%ebx, %esi
	movss	32(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	je	.LBB1_33
# BB#31:                                #   in Loop: Header=BB1_14 Depth=1
	movss	216(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jbe	.LBB1_34
# BB#32:                                #   in Loop: Header=BB1_14 Depth=1
	movss	%xmm3, 168(%r12)
	movq	184(%rsp), %xmm0        # xmm0 = mem[0],zero
	pshufd	$212, %xmm0, %xmm0      # xmm0 = xmm0[0,1,1,3]
	movl	192(%rsp), %eax
	movss	196(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movd	%xmm0, 136(%r12)
	movdqa	%xmm0, 16(%rsp)         # 16-byte Spill
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, 140(%r12)
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movl	%eax, 144(%r12)
	movss	%xmm1, 80(%rsp)         # 4-byte Spill
	movss	%xmm1, 148(%r12)
	leaq	200(%rsp), %rax
	movups	(%rax), %xmm0
	movq	104(%rsp), %rax         # 8-byte Reload
	movups	%xmm0, (%rax)
	movb	$1, %sil
	xorl	%ecx, %ecx
	jmp	.LBB1_35
	.p2align	4, 0x90
.LBB1_33:                               #   in Loop: Header=BB1_14 Depth=1
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	jmp	.LBB1_35
	.p2align	4, 0x90
.LBB1_34:                               #   in Loop: Header=BB1_14 Depth=1
	leaq	200(%rsp), %rax
	movups	(%rax), %xmm1
	movaps	%xmm1, 224(%rsp)
	movq	184(%rsp), %xmm1        # xmm1 = mem[0],zero
	pshufd	$212, %xmm1, %xmm1      # xmm1 = xmm1[0,1,1,3]
	movdqa	%xmm1, 16(%rsp)         # 16-byte Spill
	movl	192(%rsp), %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movb	$1, %cl
	movss	196(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 80(%rsp)         # 4-byte Spill
	movss	%xmm0, 48(%rsp)         # 4-byte Spill
.LBB1_35:                               #   in Loop: Header=BB1_14 Depth=1
	testb	%cl, %cl
	movl	%esi, %ebx
	movb	%sil, %bpl
	jne	.LBB1_14
	jmp	.LBB1_42
.LBB1_37:
	addss	128(%rsp), %xmm2        # 4-byte Folded Reload
	movss	184(%r12), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	jae	.LBB1_41
# BB#38:
	movss	%xmm4, 168(%r12)
	movdqa	16(%rsp), %xmm0         # 16-byte Reload
	movd	%xmm0, 136(%r12)
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, 140(%r12)
	movl	64(%rsp), %eax          # 4-byte Reload
	movl	%eax, 144(%r12)
	movss	80(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 148(%r12)
	movaps	224(%rsp), %xmm0
	movq	104(%rsp), %rax         # 8-byte Reload
	movups	%xmm0, (%rax)
	movb	$1, %bpl
	jmp	.LBB1_42
.LBB1_41:
	xorl	%ebp, %ebp
.LBB1_42:                               # %.thread
	leaq	584(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.LBB1_43:
	andb	$1, %bpl
	movl	%ebp, %eax
	addq	$808, %rsp              # imm = 0x328
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_44:
.Ltmp9:
	jmp	.LBB1_51
.LBB1_45:
.Ltmp19:
	jmp	.LBB1_51
.LBB1_46:
.Ltmp12:
	jmp	.LBB1_51
.LBB1_47:
.Ltmp6:
	jmp	.LBB1_51
.LBB1_48:
.Ltmp28:
	jmp	.LBB1_51
.LBB1_49:
.Ltmp25:
	jmp	.LBB1_51
.LBB1_50:
.Ltmp22:
.LBB1_51:
	movq	%rax, %rbx
.Ltmp29:
	leaq	584(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp30:
# BB#52:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB1_53:
.Ltmp31:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end1:
	.size	_ZN27btContinuousConvexCollision16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE, .Lfunc_end1-_ZN27btContinuousConvexCollision16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\245\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\234\001"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp5-.Ltmp0           #   Call between .Ltmp0 and .Ltmp5
	.long	.Ltmp6-.Lfunc_begin0    #     jumps to .Ltmp6
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp8-.Ltmp7           #   Call between .Ltmp7 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp11-.Ltmp10         #   Call between .Ltmp10 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp16-.Ltmp13         #   Call between .Ltmp13 and .Ltmp16
	.long	.Ltmp22-.Lfunc_begin0   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin0   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin0   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin0   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin0   #     jumps to .Ltmp28
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp29-.Ltmp27         #   Call between .Ltmp27 and .Ltmp29
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp30-.Ltmp29         #   Call between .Ltmp29 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin0   #     jumps to .Ltmp31
	.byte	1                       #   On action: 1
	.long	.Ltmp30-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Lfunc_end1-.Ltmp30     #   Call between .Ltmp30 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	1061752795              # float 0.785398185
.LCPI3_1:
	.long	981668463               # float 0.00100000005
.LCPI3_2:
	.long	1056964608              # float 0.5
.LCPI3_3:
	.long	3165301419              # float -0.020833334
.LCPI3_4:
	.long	1065353216              # float 1
.LCPI3_5:
	.long	1073741824              # float 2
	.section	.text._ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_,"axG",@progbits,_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_,comdat
	.weak	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
	.p2align	4, 0x90
	.type	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_,@function
_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_: # @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
	subq	$64, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 96
.Lcfi17:
	.cfi_offset %rbx, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %r15, -16
	movq	%rcx, %r15
	movaps	%xmm0, %xmm3
	movq	%rdx, %rbx
	movq	%rdi, %r14
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	movaps	%xmm3, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm0, %xmm1
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	movsd	48(%r14), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	addss	56(%r14), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, 48(%r15)
	movlps	%xmm1, 56(%r15)
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm2, %xmm2
	sqrtss	%xmm0, %xmm2
	ucomiss	%xmm2, %xmm2
	jnp	.LBB3_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	movaps	%xmm0, %xmm2
.LBB3_2:                                # %.split
	movaps	%xmm2, %xmm0
	mulss	%xmm3, %xmm0
	ucomiss	.LCPI3_0(%rip), %xmm0
	jbe	.LBB3_4
# BB#3:                                 # %select.true.sink
	movss	.LCPI3_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	divss	%xmm3, %xmm2
.LBB3_4:                                # %select.end
	movss	.LCPI3_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	jbe	.LBB3_6
# BB#5:
	movss	.LCPI3_2(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	movaps	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm3, %xmm0
	mulss	.LCPI3_3(%rip), %xmm0
	mulss	%xmm2, %xmm0
	mulss	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	jmp	.LBB3_7
.LBB3_6:
	movss	.LCPI3_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	mulss	%xmm3, %xmm0
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	movss	%xmm2, 8(%rsp)          # 4-byte Spill
	callq	sinf
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	divss	%xmm2, %xmm0
.LBB3_7:
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 8(%rsp)          # 4-byte Spill
	mulss	8(%rbx), %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	mulss	%xmm3, %xmm2
	mulss	.LCPI3_2(%rip), %xmm2
	movaps	%xmm2, %xmm0
	callq	cosf
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	leaq	48(%rsp), %rsi
	movq	%r14, %rdi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movsd	48(%rsp), %xmm12        # xmm12 = mem[0],zero
	movq	56(%rsp), %xmm9         # xmm9 = mem[0],zero
	movss	16(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm3
	mulss	%xmm12, %xmm3
	pshufd	$229, %xmm9, %xmm8      # xmm8 = xmm9[1,1,2,3]
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm4
	mulss	%xmm8, %xmm4
	addss	%xmm3, %xmm4
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm7
	mulss	%xmm9, %xmm7
	addss	%xmm4, %xmm7
	pshufd	$229, %xmm12, %xmm11    # xmm11 = xmm12[1,1,2,3]
	movaps	32(%rsp), %xmm3         # 16-byte Reload
	movaps	%xmm3, %xmm4
	mulss	%xmm11, %xmm4
	subss	%xmm4, %xmm7
	movaps	%xmm1, %xmm4
	movaps	%xmm1, %xmm13
	mulss	%xmm11, %xmm4
	movaps	%xmm2, %xmm5
	mulss	%xmm8, %xmm5
	addss	%xmm4, %xmm5
	movaps	%xmm3, %xmm6
	mulss	%xmm12, %xmm6
	addss	%xmm5, %xmm6
	movaps	%xmm0, %xmm4
	mulss	%xmm9, %xmm4
	subss	%xmm4, %xmm6
	movaps	%xmm13, %xmm4
	mulss	%xmm9, %xmm4
	movaps	%xmm3, %xmm5
	movaps	%xmm3, %xmm1
	mulss	%xmm8, %xmm5
	addss	%xmm4, %xmm5
	movaps	%xmm0, %xmm10
	mulss	%xmm11, %xmm10
	addss	%xmm5, %xmm10
	movaps	%xmm2, %xmm4
	mulss	%xmm12, %xmm4
	subss	%xmm4, %xmm10
	movaps	%xmm13, %xmm3
	mulss	%xmm8, %xmm3
	movaps	%xmm0, %xmm4
	mulss	%xmm12, %xmm4
	subss	%xmm4, %xmm3
	mulss	%xmm11, %xmm2
	subss	%xmm2, %xmm3
	mulss	%xmm9, %xmm1
	subss	%xmm1, %xmm3
	movaps	%xmm7, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm10, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB3_9
# BB#8:                                 # %call.sqrt113
	movaps	%xmm1, %xmm0
	movss	%xmm7, 8(%rsp)          # 4-byte Spill
	movss	%xmm6, 12(%rsp)         # 4-byte Spill
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	movss	%xmm10, 32(%rsp)        # 4-byte Spill
	callq	sqrtf
	movss	32(%rsp), %xmm10        # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm7          # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
.LBB3_9:                                # %.split112
	movss	.LCPI3_4(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm2
	divss	%xmm0, %xmm2
	mulss	%xmm2, %xmm7
	mulss	%xmm2, %xmm6
	mulss	%xmm2, %xmm10
	mulss	%xmm3, %xmm2
	movaps	%xmm7, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm6, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm10, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm0, %xmm3
	movss	.LCPI3_5(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	divss	%xmm3, %xmm4
	movaps	%xmm7, %xmm0
	mulss	%xmm4, %xmm0
	movaps	%xmm6, %xmm3
	mulss	%xmm4, %xmm3
	mulss	%xmm10, %xmm4
	movaps	%xmm2, %xmm9
	mulss	%xmm0, %xmm9
	movaps	%xmm2, %xmm11
	mulss	%xmm3, %xmm11
	mulss	%xmm4, %xmm2
	mulss	%xmm7, %xmm0
	movaps	%xmm7, %xmm5
	mulss	%xmm3, %xmm5
	mulss	%xmm4, %xmm7
	mulss	%xmm6, %xmm3
	mulss	%xmm4, %xmm6
	mulss	%xmm10, %xmm4
	movaps	%xmm3, %xmm1
	addss	%xmm4, %xmm1
	movaps	%xmm8, %xmm10
	subss	%xmm1, %xmm10
	movaps	%xmm5, %xmm12
	subss	%xmm2, %xmm12
	movaps	%xmm7, %xmm1
	addss	%xmm11, %xmm1
	addss	%xmm2, %xmm5
	addss	%xmm0, %xmm4
	movaps	%xmm8, %xmm2
	subss	%xmm4, %xmm2
	movaps	%xmm6, %xmm4
	subss	%xmm9, %xmm4
	subss	%xmm11, %xmm7
	addss	%xmm9, %xmm6
	addss	%xmm0, %xmm3
	subss	%xmm3, %xmm8
	movss	%xmm10, (%r15)
	movss	%xmm12, 4(%r15)
	movss	%xmm1, 8(%r15)
	movl	$0, 12(%r15)
	movss	%xmm5, 16(%r15)
	movss	%xmm2, 20(%r15)
	movss	%xmm4, 24(%r15)
	movl	$0, 28(%r15)
	movss	%xmm7, 32(%r15)
	movss	%xmm6, 36(%r15)
	movss	%xmm8, 40(%r15)
	movl	$0, 44(%r15)
	addq	$64, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_, .Lfunc_end3-_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
	.cfi_endproc

	.section	.text._ZN27btContinuousConvexCollisionD0Ev,"axG",@progbits,_ZN27btContinuousConvexCollisionD0Ev,comdat
	.weak	_ZN27btContinuousConvexCollisionD0Ev
	.p2align	4, 0x90
	.type	_ZN27btContinuousConvexCollisionD0Ev,@function
_ZN27btContinuousConvexCollisionD0Ev:   # @_ZN27btContinuousConvexCollisionD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 32
.Lcfi23:
	.cfi_offset %rbx, -24
.Lcfi24:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp32:
	callq	_ZN12btConvexCastD2Ev
.Ltmp33:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB4_2:
.Ltmp34:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN27btContinuousConvexCollisionD0Ev, .Lfunc_end4-_ZN27btContinuousConvexCollisionD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp32-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin1   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end4-.Ltmp33     #   Call between .Ltmp33 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	1065353216              # float 1
.LCPI5_1:
	.long	679477248               # float 1.42108547E-14
	.section	.text._ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf,"axG",@progbits,_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf,comdat
	.weak	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
	.p2align	4, 0x90
	.type	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf,@function
_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf: # @_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 24
	subq	$88, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 112
.Lcfi28:
	.cfi_offset %rbx, -24
.Lcfi29:
	.cfi_offset %r14, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movss	20(%rdi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	40(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm0
	mulss	%xmm4, %xmm0
	movss	24(%rdi), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movss	36(%rdi), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm1
	mulss	%xmm14, %xmm1
	subss	%xmm1, %xmm0
	movaps	%xmm0, %xmm3
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
	movss	32(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm5
	mulss	%xmm1, %xmm5
	movss	16(%rdi), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm2
	mulss	%xmm11, %xmm2
	subss	%xmm2, %xmm5
	movaps	%xmm14, %xmm15
	mulss	%xmm11, %xmm15
	movaps	%xmm8, %xmm2
	mulss	%xmm1, %xmm2
	subss	%xmm2, %xmm15
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movss	4(%rdi), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	mulss	%xmm0, %xmm2
	movss	%xmm2, 36(%rsp)         # 4-byte Spill
	movss	8(%rdi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm2
	mulss	%xmm6, %xmm2
	movaps	%xmm14, %xmm7
	mulss	%xmm6, %xmm7
	movaps	%xmm4, %xmm3
	mulss	%xmm13, %xmm3
	movaps	%xmm12, %xmm10
	mulss	%xmm13, %xmm10
	movaps	%xmm8, %xmm0
	mulss	%xmm6, %xmm0
	movaps	%xmm1, %xmm9
	mulss	%xmm6, %xmm9
	mulss	%xmm11, %xmm6
	mulss	%xmm13, %xmm1
	mulss	%xmm13, %xmm11
	mulss	%xmm5, %xmm13
	addss	36(%rsp), %xmm13        # 4-byte Folded Reload
	addss	%xmm13, %xmm2
	movss	.LCPI5_0(%rip), %xmm13  # xmm13 = mem[0],zero,zero,zero
	divss	%xmm2, %xmm13
	subss	%xmm3, %xmm7
	subss	%xmm0, %xmm10
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm3
	mulss	%xmm13, %xmm7
	mulss	%xmm13, %xmm10
	mulss	%xmm13, %xmm5
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	subss	%xmm9, %xmm4
	mulss	%xmm13, %xmm4
	mulss	%xmm0, %xmm12
	subss	%xmm12, %xmm6
	mulss	%xmm13, %xmm6
	mulss	%xmm13, %xmm15
	mulss	%xmm0, %xmm14
	subss	%xmm14, %xmm1
	mulss	%xmm13, %xmm1
	mulss	%xmm0, %xmm8
	subss	%xmm11, %xmm8
	mulss	%xmm13, %xmm8
	movss	(%rsi), %xmm12          # xmm12 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm9
	mulss	%xmm3, %xmm9
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
	movaps	%xmm13, %xmm2
	mulss	%xmm5, %xmm2
	addss	%xmm9, %xmm2
	movss	8(%rsi), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm0
	mulss	%xmm15, %xmm0
	addss	%xmm2, %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movaps	%xmm12, %xmm11
	mulss	%xmm7, %xmm11
	movaps	%xmm13, %xmm2
	mulss	%xmm4, %xmm2
	addss	%xmm11, %xmm2
	movaps	%xmm9, %xmm11
	mulss	%xmm1, %xmm11
	addss	%xmm2, %xmm11
	mulss	%xmm10, %xmm12
	mulss	%xmm6, %xmm13
	addss	%xmm12, %xmm13
	mulss	%xmm8, %xmm9
	addss	%xmm13, %xmm9
	movss	16(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm12
	mulss	%xmm2, %xmm12
	movss	20(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm13
	mulss	%xmm0, %xmm13
	addss	%xmm12, %xmm13
	movss	24(%rsi), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm12
	mulss	%xmm14, %xmm12
	addss	%xmm13, %xmm12
	movaps	%xmm7, %xmm13
	mulss	%xmm2, %xmm13
	movaps	%xmm4, %xmm3
	mulss	%xmm0, %xmm3
	addss	%xmm13, %xmm3
	movaps	%xmm1, %xmm13
	mulss	%xmm14, %xmm13
	addss	%xmm3, %xmm13
	mulss	%xmm10, %xmm2
	mulss	%xmm6, %xmm0
	addss	%xmm2, %xmm0
	mulss	%xmm8, %xmm14
	addss	%xmm0, %xmm14
	movss	32(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movss	36(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	movss	40(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm15
	addss	%xmm5, %xmm15
	mulss	%xmm0, %xmm7
	mulss	%xmm2, %xmm4
	addss	%xmm7, %xmm4
	mulss	%xmm3, %xmm1
	addss	%xmm4, %xmm1
	mulss	%xmm0, %xmm10
	mulss	%xmm2, %xmm6
	addss	%xmm10, %xmm6
	mulss	%xmm3, %xmm8
	addss	%xmm6, %xmm8
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 40(%rsp)
	movss	%xmm11, 44(%rsp)
	movss	%xmm9, 48(%rsp)
	movl	$0, 52(%rsp)
	movss	%xmm12, 56(%rsp)
	movss	%xmm13, 60(%rsp)
	movss	%xmm14, 64(%rsp)
	movl	$0, 68(%rsp)
	movss	%xmm15, 72(%rsp)
	movss	%xmm1, 76(%rsp)
	movss	%xmm8, 80(%rsp)
	movl	$0, 84(%rsp)
	leaq	40(%rsp), %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	24(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movss	28(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB5_2
# BB#1:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB5_2:                                # %.split
	movss	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	mulps	16(%rsp), %xmm0
	movaps	%xmm0, 16(%rsp)
	shufps	$231, %xmm0, %xmm0      # xmm0 = xmm0[3,1,2,3]
	callq	acosf
	addss	%xmm0, %xmm0
	movss	%xmm0, (%r14)
	movl	16(%rsp), %eax
	movl	20(%rsp), %ecx
	movl	24(%rsp), %edx
	movl	%eax, (%rbx)
	movl	%ecx, 4(%rbx)
	movl	%edx, 8(%rbx)
	movl	$0, 12(%rbx)
	movd	%eax, %xmm0
	mulss	%xmm0, %xmm0
	movd	%ecx, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movd	%edx, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movss	.LCPI5_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB5_4
# BB#3:
	movq	$1065353216, (%rbx)     # imm = 0x3F800000
	movq	$0, 8(%rbx)
	jmp	.LBB5_7
.LBB5_4:
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB5_6
# BB#5:                                 # %call.sqrt52
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB5_6:                                # %.split51
	movss	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	divss	%xmm1, %xmm2
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movss	%xmm0, (%rbx)
	movss	4(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movss	%xmm0, 4(%rbx)
	mulss	8(%rbx), %xmm2
	movss	%xmm2, 8(%rbx)
.LBB5_7:
	addq	$88, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf, .Lfunc_end5-_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_0:
	.long	1065353216              # float 1
.LCPI6_1:
	.long	1056964608              # float 0.5
	.section	.text._ZNK11btMatrix3x311getRotationER12btQuaternion,"axG",@progbits,_ZNK11btMatrix3x311getRotationER12btQuaternion,comdat
	.weak	_ZNK11btMatrix3x311getRotationER12btQuaternion
	.p2align	4, 0x90
	.type	_ZNK11btMatrix3x311getRotationER12btQuaternion,@function
_ZNK11btMatrix3x311getRotationER12btQuaternion: # @_ZNK11btMatrix3x311getRotationER12btQuaternion
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi36:
	.cfi_def_cfa_offset 80
.Lcfi37:
	.cfi_offset %rbx, -56
.Lcfi38:
	.cfi_offset %r12, -48
.Lcfi39:
	.cfi_offset %r13, -40
.Lcfi40:
	.cfi_offset %r14, -32
.Lcfi41:
	.cfi_offset %r15, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	20(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	addss	%xmm2, %xmm1
	movss	40(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm1
	xorps	%xmm4, %xmm4
	ucomiss	%xmm4, %xmm1
	jbe	.LBB6_4
# BB#1:
	addss	.LCPI6_0(%rip), %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB6_3
# BB#2:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movq	%rsi, %rbp
	callq	sqrtf
	movq	%rbp, %rsi
.LBB6_3:                                # %.split
	movss	.LCPI6_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	32(%rbx), %xmm2
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movaps	%xmm1, %xmm3
	divss	%xmm0, %xmm3
	movss	36(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	24(%rbx), %xmm1
	movss	16(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	4(%rbx), %xmm4
	movaps	%xmm3, %xmm5
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	unpcklps	%xmm3, %xmm3    # xmm3 = xmm3[0,0,1,1]
	unpcklps	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	unpcklps	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	mulps	%xmm3, %xmm1
	movaps	%xmm1, (%rsp)
	jmp	.LBB6_7
.LBB6_4:
	xorl	%eax, %eax
	ucomiss	%xmm0, %xmm2
	seta	%al
	maxss	%xmm0, %xmm2
	ucomiss	%xmm2, %xmm3
	movl	$2, %r15d
	cmovbel	%eax, %r15d
	leal	1(%r15), %eax
	movl	$2863311531, %ecx       # imm = 0xAAAAAAAB
	imulq	%rcx, %rax
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	negl	%eax
	leal	1(%r15,%rax), %edx
	movl	%r15d, %eax
	addl	$2, %eax
	imulq	%rcx, %rax
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	negl	%eax
	leal	2(%r15,%rax), %r14d
	movq	%r15, %rbp
	shlq	$4, %rbp
	addq	%rbx, %rbp
	movss	(%rbp,%r15,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	%rdx, %r12
	shlq	$4, %r12
	addq	%rbx, %r12
	subss	(%r12,%rdx,4), %xmm0
	movq	%r14, %r13
	shlq	$4, %r13
	addq	%rbx, %r13
	subss	(%r13,%r14,4), %xmm0
	addss	.LCPI6_0(%rip), %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB6_6
# BB#5:                                 # %call.sqrt72
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %rbx
	callq	sqrtf
	movq	%rbx, %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movaps	%xmm0, %xmm1
.LBB6_6:                                # %.split71
	movss	.LCPI6_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	mulss	%xmm0, %xmm2
	movss	%xmm2, (%rsp,%r15,4)
	divss	%xmm1, %xmm0
	movss	(%r13,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	subss	(%r12,%r14,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, 12(%rsp)
	movss	(%r12,%r15,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	(%rbp,%rdx,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsp,%rdx,4)
	movss	(%r13,%r15,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	(%rbp,%r14,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsp,%r14,4)
	movaps	(%rsp), %xmm1
.LBB6_7:
	movups	%xmm1, (%rsi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZNK11btMatrix3x311getRotationER12btQuaternion, .Lfunc_end6-_ZNK11btMatrix3x311getRotationER12btQuaternion
	.cfi_endproc

	.section	.text._ZN16btPointCollectorD0Ev,"axG",@progbits,_ZN16btPointCollectorD0Ev,comdat
	.weak	_ZN16btPointCollectorD0Ev
	.p2align	4, 0x90
	.type	_ZN16btPointCollectorD0Ev,@function
_ZN16btPointCollectorD0Ev:              # @_ZN16btPointCollectorD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end7:
	.size	_ZN16btPointCollectorD0Ev, .Lfunc_end7-_ZN16btPointCollectorD0Ev
	.cfi_endproc

	.section	.text._ZN16btPointCollector20setShapeIdentifiersAEii,"axG",@progbits,_ZN16btPointCollector20setShapeIdentifiersAEii,comdat
	.weak	_ZN16btPointCollector20setShapeIdentifiersAEii
	.p2align	4, 0x90
	.type	_ZN16btPointCollector20setShapeIdentifiersAEii,@function
_ZN16btPointCollector20setShapeIdentifiersAEii: # @_ZN16btPointCollector20setShapeIdentifiersAEii
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end8:
	.size	_ZN16btPointCollector20setShapeIdentifiersAEii, .Lfunc_end8-_ZN16btPointCollector20setShapeIdentifiersAEii
	.cfi_endproc

	.section	.text._ZN16btPointCollector20setShapeIdentifiersBEii,"axG",@progbits,_ZN16btPointCollector20setShapeIdentifiersBEii,comdat
	.weak	_ZN16btPointCollector20setShapeIdentifiersBEii
	.p2align	4, 0x90
	.type	_ZN16btPointCollector20setShapeIdentifiersBEii,@function
_ZN16btPointCollector20setShapeIdentifiersBEii: # @_ZN16btPointCollector20setShapeIdentifiersBEii
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end9:
	.size	_ZN16btPointCollector20setShapeIdentifiersBEii, .Lfunc_end9-_ZN16btPointCollector20setShapeIdentifiersBEii
	.cfi_endproc

	.section	.text._ZN16btPointCollector15addContactPointERK9btVector3S2_f,"axG",@progbits,_ZN16btPointCollector15addContactPointERK9btVector3S2_f,comdat
	.weak	_ZN16btPointCollector15addContactPointERK9btVector3S2_f
	.p2align	4, 0x90
	.type	_ZN16btPointCollector15addContactPointERK9btVector3S2_f,@function
_ZN16btPointCollector15addContactPointERK9btVector3S2_f: # @_ZN16btPointCollector15addContactPointERK9btVector3S2_f
	.cfi_startproc
# BB#0:
	movss	40(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB10_2
# BB#1:
	movb	$1, 44(%rdi)
	movups	(%rsi), %xmm1
	movups	%xmm1, 8(%rdi)
	movups	(%rdx), %xmm1
	movups	%xmm1, 24(%rdi)
	movss	%xmm0, 40(%rdi)
.LBB10_2:
	retq
.Lfunc_end10:
	.size	_ZN16btPointCollector15addContactPointERK9btVector3S2_f, .Lfunc_end10-_ZN16btPointCollector15addContactPointERK9btVector3S2_f
	.cfi_endproc

	.section	.text._ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev,"axG",@progbits,_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev,comdat
	.weak	_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev
	.p2align	4, 0x90
	.type	_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev,@function
_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev: # @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end11:
	.size	_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev, .Lfunc_end11-_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev
	.cfi_endproc

	.type	_ZTV27btContinuousConvexCollision,@object # @_ZTV27btContinuousConvexCollision
	.section	.rodata,"a",@progbits
	.globl	_ZTV27btContinuousConvexCollision
	.p2align	3
_ZTV27btContinuousConvexCollision:
	.quad	0
	.quad	_ZTI27btContinuousConvexCollision
	.quad	_ZN12btConvexCastD2Ev
	.quad	_ZN27btContinuousConvexCollisionD0Ev
	.quad	_ZN27btContinuousConvexCollision16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE
	.size	_ZTV27btContinuousConvexCollision, 40

	.type	_ZTS27btContinuousConvexCollision,@object # @_ZTS27btContinuousConvexCollision
	.globl	_ZTS27btContinuousConvexCollision
	.p2align	4
_ZTS27btContinuousConvexCollision:
	.asciz	"27btContinuousConvexCollision"
	.size	_ZTS27btContinuousConvexCollision, 30

	.type	_ZTI27btContinuousConvexCollision,@object # @_ZTI27btContinuousConvexCollision
	.globl	_ZTI27btContinuousConvexCollision
	.p2align	4
_ZTI27btContinuousConvexCollision:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS27btContinuousConvexCollision
	.quad	_ZTI12btConvexCast
	.size	_ZTI27btContinuousConvexCollision, 24

	.type	_ZTV16btPointCollector,@object # @_ZTV16btPointCollector
	.section	.rodata._ZTV16btPointCollector,"aG",@progbits,_ZTV16btPointCollector,comdat
	.weak	_ZTV16btPointCollector
	.p2align	3
_ZTV16btPointCollector:
	.quad	0
	.quad	_ZTI16btPointCollector
	.quad	_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev
	.quad	_ZN16btPointCollectorD0Ev
	.quad	_ZN16btPointCollector20setShapeIdentifiersAEii
	.quad	_ZN16btPointCollector20setShapeIdentifiersBEii
	.quad	_ZN16btPointCollector15addContactPointERK9btVector3S2_f
	.size	_ZTV16btPointCollector, 56

	.type	_ZTS16btPointCollector,@object # @_ZTS16btPointCollector
	.section	.rodata._ZTS16btPointCollector,"aG",@progbits,_ZTS16btPointCollector,comdat
	.weak	_ZTS16btPointCollector
	.p2align	4
_ZTS16btPointCollector:
	.asciz	"16btPointCollector"
	.size	_ZTS16btPointCollector, 19

	.type	_ZTSN36btDiscreteCollisionDetectorInterface6ResultE,@object # @_ZTSN36btDiscreteCollisionDetectorInterface6ResultE
	.section	.rodata._ZTSN36btDiscreteCollisionDetectorInterface6ResultE,"aG",@progbits,_ZTSN36btDiscreteCollisionDetectorInterface6ResultE,comdat
	.weak	_ZTSN36btDiscreteCollisionDetectorInterface6ResultE
	.p2align	4
_ZTSN36btDiscreteCollisionDetectorInterface6ResultE:
	.asciz	"N36btDiscreteCollisionDetectorInterface6ResultE"
	.size	_ZTSN36btDiscreteCollisionDetectorInterface6ResultE, 48

	.type	_ZTIN36btDiscreteCollisionDetectorInterface6ResultE,@object # @_ZTIN36btDiscreteCollisionDetectorInterface6ResultE
	.section	.rodata._ZTIN36btDiscreteCollisionDetectorInterface6ResultE,"aG",@progbits,_ZTIN36btDiscreteCollisionDetectorInterface6ResultE,comdat
	.weak	_ZTIN36btDiscreteCollisionDetectorInterface6ResultE
	.p2align	3
_ZTIN36btDiscreteCollisionDetectorInterface6ResultE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN36btDiscreteCollisionDetectorInterface6ResultE
	.size	_ZTIN36btDiscreteCollisionDetectorInterface6ResultE, 16

	.type	_ZTI16btPointCollector,@object # @_ZTI16btPointCollector
	.section	.rodata._ZTI16btPointCollector,"aG",@progbits,_ZTI16btPointCollector,comdat
	.weak	_ZTI16btPointCollector
	.p2align	4
_ZTI16btPointCollector:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS16btPointCollector
	.quad	_ZTIN36btDiscreteCollisionDetectorInterface6ResultE
	.size	_ZTI16btPointCollector, 24


	.globl	_ZN27btContinuousConvexCollisionC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.type	_ZN27btContinuousConvexCollisionC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver,@function
_ZN27btContinuousConvexCollisionC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver = _ZN27btContinuousConvexCollisionC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
