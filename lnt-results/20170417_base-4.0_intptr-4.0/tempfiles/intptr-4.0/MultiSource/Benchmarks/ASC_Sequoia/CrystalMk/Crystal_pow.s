	.text
	.file	"Crystal_pow.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4596373779694328218     # double 0.20000000000000001
.LCPI0_1:
	.quad	4606281698874543309     # double 0.90000000000000002
.LCPI0_2:
	.quad	4622945017495814144     # double 12
.LCPI0_3:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
.LCPI0_4:
	.quad	4602678819172646912     # double 0.5
.LCPI0_5:
	.quad	4576918229304087675     # double 0.01
.LCPI0_6:
	.quad	4608083138725491507     # double 1.2
	.text
	.globl	Crystal_pow
	.p2align	4, 0x90
	.type	Crystal_pow,@function
Crystal_pow:                            # @Crystal_pow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$200, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 240
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %ebp
	testl	%ebp, %ebp
	jle	.LBB0_14
# BB#1:                                 # %.lr.ph31.preheader
	movl	%ebp, %r15d
	testb	$1, %r15b
	jne	.LBB0_3
# BB#2:
	xorl	%eax, %eax
	cmpl	$1, %ebp
	jne	.LBB0_5
	jmp	.LBB0_7
.LBB0_3:                                # %.lr.ph31.prol
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, 96(%rsp)
	movq	$0, (%rsp)
	movl	$1, %eax
	cmpl	$1, %ebp
	je	.LBB0_7
.LBB0_5:
	movabsq	$4607182418800017408, %rcx # imm = 0x3FF0000000000000
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI0_2(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph31
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, 96(%rsp,%rax,8)
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	mulsd	%xmm0, %xmm3
	mulsd	%xmm1, %xmm3
	divsd	%xmm2, %xmm3
	movsd	%xmm3, (%rsp,%rax,8)
	leal	1(%rax), %edx
	movq	%rcx, 104(%rsp,%rax,8)
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%edx, %xmm3
	mulsd	%xmm0, %xmm3
	mulsd	%xmm1, %xmm3
	divsd	%xmm2, %xmm3
	movsd	%xmm3, 8(%rsp,%rax,8)
	addq	$2, %rax
	cmpq	%r15, %rax
	jne	.LBB0_6
.LBB0_7:                                # %.preheader
	testl	%ebp, %ebp
	jle	.LBB0_14
# BB#8:                                 # %.lr.ph.preheader
	testb	$1, %r15b
	jne	.LBB0_10
# BB#9:
	xorl	%eax, %eax
	cmpl	$1, %ebp
	jne	.LBB0_12
	jmp	.LBB0_14
.LBB0_10:                               # %.lr.ph.prol
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	mulsd	96(%rsp), %xmm0
	addsd	.LCPI0_3(%rip), %xmm0
	mulsd	.LCPI0_4(%rip), %xmm0
	movsd	.LCPI0_5(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	mulsd	.LCPI0_6(%rip), %xmm0
	movsd	%xmm0, (%rsp)
	movl	$1, %eax
	cmpl	$1, %ebp
	je	.LBB0_14
.LBB0_12:                               # %.lr.ph.preheader.new
	subq	%rax, %r15
	leaq	8(%r14,%rax,8), %r14
	leaq	104(%rsp,%rax,8), %rbx
	leaq	8(%rsp,%rax,8), %rbp
	.p2align	4, 0x90
.LBB0_13:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	-8(%r14), %xmm0         # xmm0 = mem[0],zero
	mulsd	-8(%rbx), %xmm0
	movsd	.LCPI0_3(%rip), %xmm1   # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm0
	movsd	.LCPI0_4(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	.LCPI0_5(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	movsd	.LCPI0_6(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, -8(%rbp)
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%rbx), %xmm0
	addsd	.LCPI0_3(%rip), %xmm0
	mulsd	.LCPI0_4(%rip), %xmm0
	movsd	.LCPI0_5(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	mulsd	.LCPI0_6(%rip), %xmm0
	movsd	%xmm0, (%rbp)
	addq	$16, %r14
	addq	$16, %rbx
	addq	$16, %rbp
	addq	$-2, %r15
	jne	.LBB0_13
.LBB0_14:                               # %._crit_edge
	movsd	24(%rsp), %xmm0         # xmm0 = mem[0],zero
	addq	$200, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	Crystal_pow, .Lfunc_end0-Crystal_pow
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
