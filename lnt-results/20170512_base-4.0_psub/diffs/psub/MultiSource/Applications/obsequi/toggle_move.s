	.text
	.file	"toggle_move.bc"
	.globl	update_safe
	.p2align	4, 0x90
	.type	update_safe,@function
update_safe:                            # @update_safe
	.cfi_startproc
# BB#0:
	movslq	%edi, %rax
	leaq	(%rax,%rax,2), %r8
	shlq	$7, %rax
	movslq	%esi, %rdx
	movl	g_board+4(%rax,%rdx,4), %esi
	andl	g_board-4(%rax,%rdx,4), %esi
	movl	g_board(%rax,%rdx,4), %eax
	leal	(%rax,%rax), %edi
	orl	%eax, %edi
	notl	%edi
	leal	(%rsi,%rsi), %eax
	andl	%esi, %eax
	andl	%edi, %eax
	movzwl	%ax, %esi
	movl	move_table16(,%rsi,4), %esi
	shrl	$16, %eax
	movl	move_table16(,%rax,4), %eax
	addl	%esi, %eax
	cmpl	$268435456, %esi        # imm = 0x10000000
	sbbb	%cl, %cl
	movzbl	%cl, %ecx
	orl	$65534, %ecx            # imm = 0xFFFE
	andl	%eax, %ecx
	leaq	(%rdx,%rdx,2), %rax
	movq	%r8, %rdx
	shlq	$7, %rdx
	movl	%ecx, %esi
	subl	g_info(%rdx,%rax,4), %esi
	addl	%esi, g_info_totals(,%r8,4)
	movl	%ecx, g_info(%rdx,%rax,4)
	retq
.Lfunc_end0:
	.size	update_safe, .Lfunc_end0-update_safe
	.cfi_endproc

	.globl	update_real
	.p2align	4, 0x90
	.type	update_real,@function
update_real:                            # @update_real
	.cfi_startproc
# BB#0:
	movslq	%edi, %rcx
	movslq	%esi, %rax
	leaq	(%rcx,%rcx,2), %rdx
	shlq	$7, %rcx
	movl	g_board(%rcx,%rax,4), %ecx
	leal	(%rcx,%rcx), %esi
	orl	%ecx, %esi
	notl	%esi
	movzwl	%si, %ecx
	movl	move_table16(,%rcx,4), %ecx
	shrl	$16, %esi
	movl	move_table16(,%rsi,4), %esi
	addl	%ecx, %esi
	cmpl	$268435456, %ecx        # imm = 0x10000000
	sbbb	%cl, %cl
	movzbl	%cl, %ecx
	orl	$65534, %ecx            # imm = 0xFFFE
	andl	%esi, %ecx
	leaq	(%rax,%rax,2), %rax
	movq	%rdx, %rsi
	shlq	$7, %rsi
	movl	%ecx, %edi
	subl	g_info+4(%rsi,%rax,4), %edi
	addl	%edi, g_info_totals+4(,%rdx,4)
	movl	%ecx, g_info+4(%rsi,%rax,4)
	retq
.Lfunc_end1:
	.size	update_real, .Lfunc_end1-update_real
	.cfi_endproc

	.globl	toggle_move
	.p2align	4, 0x90
	.type	toggle_move,@function
toggle_move:                            # @toggle_move
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
.Lcfi6:
	.cfi_offset %rbx, -56
.Lcfi7:
	.cfi_offset %r12, -48
.Lcfi8:
	.cfi_offset %r13, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movabsq	$-8589934592, %r9       # imm = 0xFFFFFFFE00000000
	movq	%rdi, %r13
	shrq	$32, %r13
	movl	%edx, %r12d
	andl	$1, %r12d
	xorl	$1, %edx
	movl	$3, %ebp
	movl	%r13d, %ecx
	shll	%cl, %ebp
	movq	%rdi, %r8
	shlq	$32, %r8
	movslq	%edi, %rbx
	movq	%r8, %rcx
	sarq	$30, %rcx
	movq	%r12, %rax
	shlq	$7, %rax
	xorl	%ebp, g_board(%rax,%rcx)
	leaq	g_board(%rax,%rcx), %r10
	movl	$1, %esi
	movl	%edi, %ecx
	shll	%cl, %esi
	movslq	%edx, %rdx
	sarq	$32, %rdi
	movq	%rdx, %r15
	shlq	$7, %r15
	xorl	%esi, g_board(%r15,%rdi,4)
	xorl	g_board+4(%r15,%rdi,4), %esi
	movq	%rsi, -16(%rsp)         # 8-byte Spill
	movl	%esi, g_board+4(%r15,%rdi,4)
	movq	%rbx, -40(%rsp)         # 8-byte Spill
	movl	%ebx, %ecx
	decl	%ecx
	movabsq	$4294967296, %r11       # imm = 0x100000000
	movq	%rdi, -8(%rsp)          # 8-byte Spill
	movq	%rdx, -24(%rsp)         # 8-byte Spill
	je	.LBB2_1
# BB#2:
	leaq	(%r8,%r9), %rbp
	sarq	$30, %rbp
	movl	(%r10), %r9d
	movl	g_board(%rax,%rbp), %ebx
	andl	%r9d, %ebx
	movslq	%ecx, %rsi
	movl	g_board(%rax,%rsi,4), %ebp
	leal	(%rbp,%rbp), %ecx
	orl	%ebp, %ecx
	notl	%ecx
	leal	(%rbx,%rbx), %edx
	andl	%ebx, %edx
	andl	%ecx, %edx
	movzwl	%dx, %ecx
	movl	move_table16(,%rcx,4), %ecx
	shrl	$16, %edx
	movl	move_table16(,%rdx,4), %edx
	addl	%ecx, %edx
	cmpl	$268435456, %ecx        # imm = 0x10000000
	sbbb	%cl, %cl
	movzbl	%cl, %ecx
	orl	$65534, %ecx            # imm = 0xFFFE
	andl	%edx, %ecx
	leaq	(%rsi,%rsi,2), %rdx
	leaq	(%r12,%r12,2), %rbx
	movq	%rbx, %rsi
	shlq	$7, %rsi
	movl	%ecx, %r10d
	subl	g_info(%rsi,%rdx,4), %r10d
	leaq	g_info_totals(,%rbx,4), %rdi
	movq	%rdi, -56(%rsp)         # 8-byte Spill
	addl	g_info_totals(,%rbx,4), %r10d
	movl	%r10d, g_info_totals(,%rbx,4)
	movl	%ecx, g_info(%rsi,%rdx,4)
	jmp	.LBB2_3
.LBB2_1:                                # %._crit_edge
	movl	g_board(%rax), %ebp
	movl	(%r10), %r9d
	leaq	(%r12,%r12,2), %rcx
	leaq	g_info_totals(,%rcx,4), %rdx
	movq	%rdx, -56(%rsp)         # 8-byte Spill
	movl	g_info_totals(,%rcx,4), %r10d
.LBB2_3:
	leaq	(%r8,%r11), %rcx
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	sarq	$30, %rcx
	movl	g_board(%rax,%rcx), %r14d
	andl	%r14d, %ebp
	leal	(%r9,%r9), %ecx
	orl	%r9d, %ecx
	notl	%ecx
	leal	(%rbp,%rbp), %edx
	andl	%ebp, %edx
	andl	%ecx, %edx
	movzwl	%dx, %esi
	movl	move_table16(,%rsi,4), %esi
	shrl	$16, %edx
	movl	move_table16(,%rdx,4), %edx
	addl	%esi, %edx
	cmpl	$268435456, %esi        # imm = 0x10000000
	sbbb	%bl, %bl
	movzbl	%bl, %esi
	orl	$65534, %esi            # imm = 0xFFFE
	andl	%edx, %esi
	movq	-40(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rdi,2), %rdx
	leaq	(%r12,%r12,2), %rbp
	movq	%rbp, %rbx
	movq	%rbx, -32(%rsp)         # 8-byte Spill
	shlq	$7, %rbp
	movl	%esi, %r11d
	subl	g_info(%rbp,%rdx,4), %r11d
	addl	%r10d, %r11d
	cmpl	g_board_size(,%r12,4), %edi
	movq	%rdx, %r12
	movl	%esi, g_info(%rbp,%r12,4)
	movabsq	$8589934592, %r10       # imm = 0x200000000
	movq	-56(%rsp), %rdx         # 8-byte Reload
	movl	%r11d, (%rdx)
	je	.LBB2_5
# BB#4:
	movq	-48(%rsp), %rdi         # 8-byte Reload
	sarq	$32, %rdi
	addq	%r10, %r8
	sarq	$30, %r8
	andl	g_board(%rax,%r8), %r9d
	leal	(%r14,%r14), %eax
	orl	%r14d, %eax
	notl	%eax
	leal	(%r9,%r9), %esi
	andl	%r9d, %eax
	andl	%esi, %eax
	movzwl	%ax, %esi
	movl	move_table16(,%rsi,4), %esi
	shrl	$16, %eax
	movl	move_table16(,%rax,4), %eax
	addl	%esi, %eax
	cmpl	$268435456, %esi        # imm = 0x10000000
	sbbb	%bl, %bl
	movzbl	%bl, %esi
	orl	$65534, %esi            # imm = 0xFFFE
	andl	%eax, %esi
	leaq	(%rdi,%rdi,2), %rax
	movl	%esi, %edi
	subl	g_info(%rbp,%rax,4), %edi
	addl	%r11d, %edi
	movl	%edi, (%rdx)
	movl	%esi, g_info(%rbp,%rax,4)
.LBB2_5:
	movq	-8(%rsp), %r11          # 8-byte Reload
	leaq	g_board(%r15,%r11,4), %r9
	leaq	1(%r11), %r8
	movl	%r13d, %esi
	decl	%esi
	movq	-24(%rsp), %r14         # 8-byte Reload
	je	.LBB2_7
# BB#6:
	movq	%r13, %rdi
	shlq	$32, %rdi
	movabsq	$-8589934592, %rax      # imm = 0xFFFFFFFE00000000
	addq	%rax, %rdi
	sarq	$30, %rdi
	movl	(%r9), %edx
	andl	g_board(%r15,%rdi), %edx
	movslq	%esi, %rsi
	movl	g_board(%r15,%rsi,4), %edi
	leal	(%rdi,%rdi), %ebx
	orl	%edi, %ebx
	notl	%ebx
	leal	(%rdx,%rdx), %edi
	andl	%edx, %edi
	andl	%ebx, %edi
	movzwl	%di, %edx
	movl	move_table16(,%rdx,4), %edx
	shrl	$16, %edi
	movl	move_table16(,%rdi,4), %edi
	addl	%edx, %edi
	cmpl	$268435456, %edx        # imm = 0x10000000
	sbbb	%dl, %dl
	movzbl	%dl, %eax
	orl	$65534, %eax            # imm = 0xFFFE
	andl	%edi, %eax
	leaq	(%rsi,%rsi,2), %rsi
	leaq	(%r14,%r14,2), %rbx
	movq	%rbx, %rdi
	shlq	$7, %rdi
	movl	%eax, %edx
	subl	g_info(%rdi,%rsi,4), %edx
	addl	%edx, g_info_totals(,%rbx,4)
	movl	%eax, g_info(%rdi,%rsi,4)
.LBB2_7:
	cmpl	g_board_size(,%r14,4), %r8d
	je	.LBB2_9
# BB#8:
	shlq	$32, %r13
	movabsq	$4294967296, %rax       # imm = 0x100000000
	movq	%rax, %rdx
	addq	%r13, %rdx
	sarq	$30, %rdx
	movabsq	$12884901888, %rax      # imm = 0x300000000
	addq	%r13, %rax
	sarq	$30, %rax
	movl	g_board(%r15,%rax), %eax
	andl	g_board(%r15,%rdx), %eax
	addq	%r10, %r13
	movq	%r13, %rsi
	sarq	$32, %rsi
	sarq	$30, %r13
	movl	g_board(%r15,%r13), %edx
	leal	(%rdx,%rdx), %edi
	orl	%edx, %edi
	notl	%edi
	leal	(%rax,%rax), %edx
	andl	%eax, %edx
	andl	%edi, %edx
	movzwl	%dx, %eax
	movl	move_table16(,%rax,4), %eax
	shrl	$16, %edx
	movl	move_table16(,%rdx,4), %edx
	addl	%eax, %edx
	cmpl	$268435456, %eax        # imm = 0x10000000
	sbbb	%al, %al
	movzbl	%al, %ebx
	orl	$65534, %ebx            # imm = 0xFFFE
	andl	%edx, %ebx
	leaq	(%rsi,%rsi,2), %rdx
	leaq	(%r14,%r14,2), %rsi
	movq	%rsi, %rdi
	shlq	$7, %rdi
	movl	%ebx, %eax
	subl	g_info(%rdi,%rdx,4), %eax
	addl	%eax, g_info_totals(,%rsi,4)
	movl	%ebx, g_info(%rdi,%rdx,4)
.LBB2_9:
	movzwl	%cx, %eax
	movl	move_table16(,%rax,4), %eax
	shrl	$16, %ecx
	movl	move_table16(,%rcx,4), %ecx
	addl	%eax, %ecx
	cmpl	$268435456, %eax        # imm = 0x10000000
	sbbb	%al, %al
	movzbl	%al, %eax
	orl	$65534, %eax            # imm = 0xFFFE
	andl	%ecx, %eax
	movl	%eax, %ecx
	subl	g_info+4(%rbp,%r12,4), %ecx
	movq	-32(%rsp), %rdx         # 8-byte Reload
	addl	%ecx, g_info_totals+4(,%rdx,4)
	movl	%eax, g_info+4(%rbp,%r12,4)
	movl	(%r9), %eax
	leal	(%rax,%rax), %ecx
	orl	%eax, %ecx
	notl	%ecx
	movzwl	%cx, %eax
	movl	move_table16(,%rax,4), %eax
	shrl	$16, %ecx
	movl	move_table16(,%rcx,4), %ecx
	addl	%eax, %ecx
	cmpl	$268435456, %eax        # imm = 0x10000000
	sbbb	%al, %al
	movzbl	%al, %esi
	orl	$65534, %esi            # imm = 0xFFFE
	andl	%ecx, %esi
	leaq	(%r11,%r11,2), %rdi
	leaq	(%r14,%r14,2), %rax
	movq	%rax, %rcx
	shlq	$7, %rcx
	movl	%esi, %edx
	subl	g_info+4(%rcx,%rdi,4), %edx
	addl	g_info_totals+4(,%rax,4), %edx
	movl	%esi, g_info+4(%rcx,%rdi,4)
	movq	-16(%rsp), %rdi         # 8-byte Reload
	leal	(%rdi,%rdi), %esi
	orl	%edi, %esi
	notl	%esi
	movzwl	%si, %edi
	movl	move_table16(,%rdi,4), %edi
	shrl	$16, %esi
	movl	move_table16(,%rsi,4), %esi
	addl	%edi, %esi
	cmpl	$268435456, %edi        # imm = 0x10000000
	sbbb	%bl, %bl
	movzbl	%bl, %ebp
	orl	$65534, %ebp            # imm = 0xFFFE
	andl	%esi, %ebp
	leaq	(%r8,%r8,2), %rsi
	movl	%ebp, %edi
	subl	g_info+4(%rcx,%rsi,4), %edi
	addl	%edx, %edi
	movl	%edi, g_info_totals+4(,%rax,4)
	movl	%ebp, g_info+4(%rcx,%rsi,4)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	toggle_move, .Lfunc_end2-toggle_move
	.cfi_endproc

	.globl	score_and_get_first
	.p2align	4, 0x90
	.type	score_and_get_first,@function
score_and_get_first:                    # @score_and_get_first
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi18:
	.cfi_def_cfa_offset 96
.Lcfi19:
	.cfi_offset %rbx, -56
.Lcfi20:
	.cfi_offset %r12, -48
.Lcfi21:
	.cfi_offset %r13, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbp
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movl	%esi, %ebx
	movq	%rdi, %r13
	cmpl	$-1, %ebp
	je	.LBB3_6
# BB#1:                                 # %.preheader64
	testl	%ebx, %ebx
	jle	.LBB3_12
# BB#2:                                 # %.lr.ph73
	movq	%rbp, %rax
	shrq	$32, %rax
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	movl	%ebx, %r14d
	movq	%rax, %rbx
	movl	$-1, %r12d
	xorl	%r15d, %r15d
	movq	%r13, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	cmpl	(%r13), %ebp
	jne	.LBB3_9
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	cmpl	4(%r13), %ebx
	jne	.LBB3_9
# BB#5:                                 # %._crit_edge104
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	$450000, %eax           # imm = 0x6DDD0
	movl	%r15d, %r12d
	jmp	.LBB3_10
	.p2align	4, 0x90
.LBB3_9:                                #   in Loop: Header=BB3_3 Depth=1
	movq	(%r13), %rdi
	movl	12(%rsp), %esi          # 4-byte Reload
	callq	score_move
.LBB3_10:                               #   in Loop: Header=BB3_3 Depth=1
	movl	%eax, 8(%r13)
	incq	%r15
	addq	$12, %r13
	cmpq	%r15, %r14
	jne	.LBB3_3
	jmp	.LBB3_11
.LBB3_6:                                # %.preheader
	testl	%ebx, %ebx
	jle	.LBB3_12
# BB#7:                                 # %.lr.ph70.preheader
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	movl	%ebx, %r14d
	movq	%r13, 16(%rsp)          # 8-byte Spill
	leaq	8(%r13), %rbp
	movl	$-1, %r12d
	movl	$-50000, %r15d          # imm = 0xFFFF3CB0
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_8:                                # %.lr.ph70
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rbp), %rdi
	movl	12(%rsp), %esi          # 4-byte Reload
	callq	score_move
	movl	%eax, (%rbp)
	cmpl	%r15d, %eax
	cmovgl	%ebx, %r12d
	cmovgel	%eax, %r15d
	incq	%rbx
	addq	$12, %rbp
	cmpq	%rbx, %r14
	jne	.LBB3_8
.LBB3_11:                               # %.loopexit
	cmpl	$-1, %r12d
	movq	16(%rsp), %r13          # 8-byte Reload
	movl	8(%rsp), %ebx           # 4-byte Reload
	jne	.LBB3_13
.LBB3_12:                               # %.loopexit.thread
	movl	$.L.str, %edi
	movl	$180, %esi
	movl	$1, %edx
	movl	$.L.str.1, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
	movl	$-1, %r12d
.LBB3_13:
	cmpl	$2, %ebx
	jl	.LBB3_18
# BB#14:
	movslq	%r12d, %rax
	leaq	(%rax,%rax,2), %rcx
	movl	8(%r13,%rcx,4), %edx
	movl	%edx, 32(%rsp)
	movq	(%r13,%rcx,4), %rdx
	movq	%rdx, 24(%rsp)
	testl	%eax, %eax
	jle	.LBB3_17
# BB#15:                                # %.lr.ph.preheader
	incq	%rax
	leaq	(%r13,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB3_16:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rcx), %edx
	movl	%edx, 8(%rcx)
	movq	-12(%rcx), %rdx
	movq	%rdx, (%rcx)
	leaq	-12(%rcx), %rcx
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB3_16
.LBB3_17:                               # %._crit_edge
	movl	32(%rsp), %eax
	movl	%eax, 8(%r13)
	movq	24(%rsp), %rax
	movq	%rax, (%r13)
.LBB3_18:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	score_and_get_first, .Lfunc_end3-score_and_get_first
	.cfi_endproc

	.p2align	4, 0x90
	.type	score_move,@function
score_move:                             # @score_move
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
.Lcfi31:
	.cfi_offset %rbx, -56
.Lcfi32:
	.cfi_offset %r12, -48
.Lcfi33:
	.cfi_offset %r13, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%rdi, %r9
	movq	%r9, %r8
	shrq	$32, %r8
	movl	%esi, %ebx
	andl	$1, %ebx
	movl	%esi, -68(%rsp)         # 4-byte Spill
	movl	%esi, %eax
	movl	$3, %edx
	movl	%r8d, %ecx
	shll	%cl, %edx
	movq	%r9, %r15
	shlq	$32, %r15
	movq	%r15, %rsi
	sarq	$30, %rsi
	movq	%rbx, %r13
	shlq	$7, %r13
	movl	%edx, -72(%rsp)         # 4-byte Spill
	xorl	%edx, g_board(%r13,%rsi)
	xorl	$1, %eax
	movl	$1, %edx
	movl	%r9d, %ecx
	shll	%cl, %edx
	movslq	%eax, %rdi
	movq	%r9, %rcx
	sarq	$32, %rcx
	movq	%rdi, %r11
	shlq	$7, %r11
	xorl	%edx, g_board(%r11,%rcx,4)
	movl	g_board+4(%r11,%rcx,4), %ebp
	movl	%edx, -76(%rsp)         # 4-byte Spill
	xorl	%edx, %ebp
	movl	%ebp, g_board+4(%r11,%rcx,4)
	movq	%rsi, -40(%rsp)         # 8-byte Spill
	movl	g_board(%r13,%rsi), %r10d
	leal	(%r10,%r10), %eax
	orl	%r10d, %eax
	notl	%eax
	movzwl	%ax, %edx
	movl	move_table16(,%rdx,4), %edx
	movl	%eax, %esi
	shrl	$16, %esi
	movl	move_table16(,%rsi,4), %esi
	addl	%edx, %esi
	cmpl	$268435456, %edx        # imm = 0x10000000
	sbbb	%dl, %dl
	movzbl	%dl, %r14d
	orl	$65534, %r14d           # imm = 0xFFFE
	andl	%esi, %r14d
	movq	%rbx, -8(%rsp)          # 8-byte Spill
	leaq	(%rbx,%rbx,2), %r12
	shlq	$7, %r12
	movl	g_board(%r11,%rcx,4), %esi
	leal	(%rsi,%rsi), %edx
	movq	%rsi, -24(%rsp)         # 8-byte Spill
	orl	%esi, %edx
	notl	%edx
	movzwl	%dx, %esi
	movl	move_table16(,%rsi,4), %esi
	shrl	$16, %edx
	movl	move_table16(,%rdx,4), %edx
	addl	%esi, %edx
	cmpl	$268435456, %esi        # imm = 0x10000000
	sbbb	%bl, %bl
	movzbl	%bl, %esi
	orl	$65534, %esi            # imm = 0xFFFE
	andl	%edx, %esi
	leal	(%rbp,%rbp), %edx
	orl	%ebp, %edx
	movq	%rdi, -48(%rsp)         # 8-byte Spill
	leaq	(%rdi,%rdi,2), %rdi
	shlq	$7, %rdi
	notl	%edx
	movzwl	%dx, %ebp
	movl	move_table16(,%rbp,4), %ebp
	shrl	$16, %edx
	movl	move_table16(,%rdx,4), %edx
	addl	%ebp, %edx
	cmpl	$268435456, %ebp        # imm = 0x10000000
	sbbb	%bl, %bl
	movzbl	%bl, %ebp
	orl	$65534, %ebp            # imm = 0xFFFE
	andl	%edx, %ebp
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rcx,2), %rbx
	movl	g_info+16(%rdi,%rbx,4), %edx
	subl	%ebp, %edx
	movslq	%r9d, %rcx
	leaq	(%rcx,%rcx,2), %rbp
	movq	%rbp, -16(%rsp)         # 8-byte Spill
	subl	g_info+4(%r12,%rbp,4), %r14d
	addl	g_info+4(%rdi,%rbx,4), %r14d
	subl	%esi, %r14d
	addl	%edx, %r14d
	movq	%rcx, -56(%rsp)         # 8-byte Spill
	movl	%ecx, %esi
	decl	%esi
	movabsq	$-8589934592, %rcx      # imm = 0xFFFFFFFE00000000
	movq	%r10, -64(%rsp)         # 8-byte Spill
	je	.LBB4_1
# BB#2:
	leaq	(%r15,%rcx), %rdx
	sarq	$30, %rdx
	movl	g_board(%r13,%rdx), %edx
	andl	%r10d, %edx
	movslq	%esi, %rsi
	movl	g_board(%r13,%rsi,4), %ebp
	leal	(%rbp,%rbp), %ebx
	orl	%ebp, %ebx
	notl	%ebx
	leal	(%rdx,%rdx), %ecx
	andl	%edx, %ecx
	andl	%ebx, %ecx
	movzwl	%cx, %edx
	movl	move_table16(,%rdx,4), %edx
	shrl	$16, %ecx
	movl	move_table16(,%rcx,4), %ecx
	addl	%edx, %ecx
	cmpl	$268435456, %edx        # imm = 0x10000000
	sbbb	%dl, %dl
	movzbl	%dl, %edx
	orl	$65534, %edx            # imm = 0xFFFE
	andl	%ecx, %edx
	leaq	(%rsi,%rsi,2), %rcx
	subl	g_info(%r12,%rcx,4), %r14d
	addl	%edx, %r14d
	jmp	.LBB4_3
.LBB4_1:                                # %._crit_edge
	movl	g_board(%r13), %ebp
.LBB4_3:
	movq	%r12, %rsi
	movabsq	$8589934592, %rdx       # imm = 0x200000000
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	movq	%rcx, %rbx
	leaq	(%r15,%rcx), %r10
	movq	%r10, %rcx
	sarq	$30, %rcx
	movl	g_board(%r13,%rcx), %r12d
	andl	%r12d, %ebp
	leal	(%rbp,%rbp), %ecx
	andl	%ebp, %ecx
	andl	%eax, %ecx
	movzwl	%cx, %eax
	movl	move_table16(,%rax,4), %eax
	shrl	$16, %ecx
	movl	move_table16(,%rcx,4), %ecx
	addl	%eax, %ecx
	cmpl	$268435456, %eax        # imm = 0x10000000
	sbbb	%al, %al
	movzbl	%al, %eax
	orl	$65534, %eax            # imm = 0xFFFE
	andl	%ecx, %eax
	movq	-16(%rsp), %rcx         # 8-byte Reload
	subl	g_info(%rsi,%rcx,4), %eax
	addl	%r14d, %eax
	movq	-8(%rsp), %rcx          # 8-byte Reload
	cmpl	g_board_size(,%rcx,4), %r9d
	je	.LBB4_5
# BB#4:
	sarq	$32, %r10
	addq	%rdx, %r15
	sarq	$30, %r15
	movl	g_board(%r13,%r15), %ecx
	andl	-64(%rsp), %ecx         # 4-byte Folded Reload
	leal	(%r12,%r12), %edx
	orl	%r12d, %edx
	notl	%edx
	movq	%rsi, %rbp
	leal	(%rcx,%rcx), %esi
	andl	%ecx, %esi
	andl	%edx, %esi
	movzwl	%si, %ecx
	movl	move_table16(,%rcx,4), %ecx
	shrl	$16, %esi
	movl	move_table16(,%rsi,4), %edx
	addl	%ecx, %edx
	cmpl	$268435456, %ecx        # imm = 0x10000000
	sbbb	%cl, %cl
	movzbl	%cl, %ecx
	orl	$65534, %ecx            # imm = 0xFFFE
	andl	%edx, %ecx
	leaq	(%r10,%r10,2), %rdx
	subl	g_info(%rbp,%rdx,4), %eax
	addl	%ecx, %eax
.LBB4_5:
	movq	-32(%rsp), %r9          # 8-byte Reload
	movq	-40(%rsp), %r10         # 8-byte Reload
	movq	-48(%rsp), %r14         # 8-byte Reload
	movq	%rbx, %rbp
	leal	1(%r8), %r12d
	movl	%r8d, %edx
	decl	%edx
	je	.LBB4_6
# BB#7:
	movq	%r8, %rcx
	shlq	$32, %rcx
	movabsq	$-8589934592, %rsi      # imm = 0xFFFFFFFE00000000
	addq	%rsi, %rcx
	sarq	$30, %rcx
	movq	-24(%rsp), %rbx         # 8-byte Reload
	andl	g_board(%r11,%rcx), %ebx
	movslq	%edx, %rdx
	movl	g_board(%r11,%rdx,4), %ecx
	leal	(%rcx,%rcx), %esi
	orl	%ecx, %esi
	notl	%esi
	leal	(%rbx,%rbx), %ecx
	andl	%ebx, %ecx
	andl	%esi, %ecx
	movzwl	%cx, %esi
	movl	move_table16(,%rsi,4), %esi
	shrl	$16, %ecx
	movl	move_table16(,%rcx,4), %ecx
	addl	%esi, %ecx
	cmpl	$268435456, %esi        # imm = 0x10000000
	sbbb	%bl, %bl
	movzbl	%bl, %esi
	orl	$65534, %esi            # imm = 0xFFFE
	andl	%ecx, %esi
	leaq	(%rdx,%rdx,2), %rcx
	movq	%rdi, %r15
	addl	g_info(%rdi,%rcx,4), %eax
	subl	%esi, %eax
	jmp	.LBB4_8
.LBB4_6:
	movq	%rdi, %r15
.LBB4_8:
	movq	-64(%rsp), %rdi         # 8-byte Reload
	movabsq	$8589934592, %rdx       # imm = 0x200000000
	leaq	g_board(%r13,%r10), %r10
	leaq	g_board(%r11,%r9,4), %rsi
	cmpl	g_board_size(,%r14,4), %r12d
	je	.LBB4_10
# BB#9:
	shlq	$32, %r8
	addq	%r8, %rbp
	sarq	$30, %rbp
	movabsq	$12884901888, %rcx      # imm = 0x300000000
	addq	%r8, %rcx
	sarq	$30, %rcx
	movl	g_board(%r11,%rcx), %ecx
	andl	g_board(%r11,%rbp), %ecx
	addq	%rdx, %r8
	movq	%r8, %rdx
	sarq	$32, %rdx
	sarq	$30, %r8
	movl	g_board(%r11,%r8), %ebp
	leal	(%rbp,%rbp), %ebx
	orl	%ebp, %ebx
	notl	%ebx
	leal	(%rcx,%rcx), %ebp
	andl	%ecx, %ebp
	andl	%ebx, %ebp
	movzwl	%bp, %ecx
	movl	move_table16(,%rcx,4), %ecx
	shrl	$16, %ebp
	movl	move_table16(,%rbp,4), %ebp
	addl	%ecx, %ebp
	cmpl	$268435456, %ecx        # imm = 0x10000000
	sbbb	%cl, %cl
	movzbl	%cl, %ecx
	orl	$65534, %ecx            # imm = 0xFFFE
	andl	%ebp, %ecx
	leaq	(%rdx,%rdx,2), %rdx
	addl	g_info(%r15,%rdx,4), %eax
	subl	%ecx, %eax
.LBB4_10:
	movl	-68(%rsp), %ecx         # 4-byte Reload
	movl	-72(%rsp), %edx         # 4-byte Reload
	movl	-76(%rsp), %ebp         # 4-byte Reload
	movq	-56(%rsp), %rbx         # 8-byte Reload
	xorl	%edx, %edi
	movl	%edi, (%r10)
	xorl	%ebp, (%rsi)
	xorl	%ebp, 4(%rsi)
	shll	$7, %eax
	movslq	%ecx, %rcx
	shlq	$7, %rbx
	shlq	$12, %rcx
	addq	%rbx, %rcx
	addl	g_first_move(%rcx,%r9,4), %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	score_move, .Lfunc_end4-score_move
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/obsequi/toggle_move.c"
	.size	.L.str, 85

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"No maximum\n"
	.size	.L.str.1, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
