	.text
	.file	"cvrmisc.bc"
	.globl	cover_cost
	.p2align	4, 0x90
	.type	cover_cost,@function
cover_cost:                             # @cover_cost
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	xorl	%eax, %eax
	callq	cube1list
	movq	%rax, %r15
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	massive_count
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB0_2
# BB#1:                                 # %.thread
	callq	free
	jmp	.LBB0_3
.LBB0_2:
	testq	%r15, %r15
	je	.LBB0_4
.LBB0_3:
	movq	%r15, %rdi
	callq	free
.LBB0_4:
	movslq	12(%r14), %r9
	movl	%r9d, (%r13)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 4(%r13)
	movl	$0, 20(%r13)
	movslq	cube+8(%rip), %r10
	xorl	%ecx, %ecx
	testq	%r10, %r10
	movl	$0, %eax
	jle	.LBB0_8
# BB#5:                                 # %.lr.ph64
	leaq	4(%r13), %rdx
	movq	cdata+8(%rip), %rsi
	leaq	-1(%r10), %r8
	movq	%r10, %rbx
	xorl	%eax, %eax
	xorl	%edi, %edi
	andq	$3, %rbx
	je	.LBB0_7
	.p2align	4, 0x90
.LBB0_6:                                # =>This Inner Loop Header: Depth=1
	addl	(%rsi,%rdi,4), %eax
	movl	%eax, (%rdx)
	incq	%rdi
	cmpq	%rdi, %rbx
	jne	.LBB0_6
.LBB0_7:                                # %.prol.loopexit75
	cmpq	$3, %r8
	jb	.LBB0_8
	.p2align	4, 0x90
.LBB0_26:                               # =>This Inner Loop Header: Depth=1
	addl	(%rsi,%rdi,4), %eax
	movl	%eax, (%rdx)
	addl	4(%rsi,%rdi,4), %eax
	movl	%eax, (%rdx)
	addl	8(%rsi,%rdi,4), %eax
	movl	%eax, (%rdx)
	addl	12(%rsi,%rdi,4), %eax
	movl	%eax, (%rdx)
	addq	$4, %rdi
	cmpq	%r10, %rdi
	jl	.LBB0_26
.LBB0_8:                                # %.preheader
	movslq	cube+4(%rip), %r8
	movq	%r8, %r15
	decq	%r15
	cmpl	%r15d, %r10d
	jge	.LBB0_20
# BB#9:                                 # %.lr.ph59
	movq	cube+112(%rip), %rdx
	movq	cdata+8(%rip), %rsi
	movq	cube+32(%rip), %r11
	movl	%r15d, %ecx
	subl	%r10d, %ecx
	leaq	-1(%r15), %r12
	testb	$1, %cl
	jne	.LBB0_11
# BB#10:
	xorl	%ecx, %ecx
	movq	%r10, %rdi
	cmpq	%r10, %r12
	jne	.LBB0_15
	jmp	.LBB0_20
.LBB0_11:
	cmpl	$0, (%rdx,%r10,4)
	movl	(%rsi,%r10,4), %ecx
	je	.LBB0_13
# BB#12:
	movl	(%r11,%r10,4), %edi
	imull	%r9d, %edi
	subl	%ecx, %edi
	movl	%edi, %ecx
.LBB0_13:
	movl	%ecx, 12(%r13)
	leaq	1(%r10), %rdi
	cmpq	%r10, %r12
	je	.LBB0_20
	.p2align	4, 0x90
.LBB0_15:                               # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%rdx,%rdi,4)
	movl	(%rsi,%rdi,4), %ebx
	je	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_15 Depth=1
	movl	(%r11,%rdi,4), %ebp
	imull	%r9d, %ebp
	subl	%ebx, %ebp
	movl	%ebp, %ebx
.LBB0_17:                               #   in Loop: Header=BB0_15 Depth=1
	addl	%ebx, %ecx
	movl	%ecx, 12(%r13)
	cmpl	$0, 4(%rdx,%rdi,4)
	movl	4(%rsi,%rdi,4), %ebx
	je	.LBB0_19
# BB#18:                                #   in Loop: Header=BB0_15 Depth=1
	movl	4(%r11,%rdi,4), %ebp
	imull	%r9d, %ebp
	subl	%ebx, %ebp
	movl	%ebp, %ebx
.LBB0_19:                               #   in Loop: Header=BB0_15 Depth=1
	addl	%ebx, %ecx
	movl	%ecx, 12(%r13)
	addq	$2, %rdi
	cmpq	%r15, %rdi
	jl	.LBB0_15
.LBB0_20:                               # %._crit_edge60
	xorl	%r11d, %r11d
	cmpl	%r8d, %r10d
	je	.LBB0_22
# BB#21:
	movq	cube+32(%rip), %rdx
	movslq	%r15d, %rsi
	movl	(%rdx,%rsi,4), %r11d
	imull	%r9d, %r11d
	movq	cdata+8(%rip), %rdx
	subl	(%rdx,%rsi,4), %r11d
	movl	%r11d, 8(%r13)
.LBB0_22:
	movslq	(%r14), %rsi
	imulq	%rsi, %r9
	testl	%r9d, %r9d
	jle	.LBB0_25
# BB#23:                                # %.lr.ph.preheader
	movq	24(%r14), %rdi
	leaq	(%rdi,%r9,4), %rbx
	shlq	$2, %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_24:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi), %ebp
	shrl	$15, %ebp
	andl	$1, %ebp
	addl	%ebp, %edx
	movl	%edx, 20(%r13)
	addq	%rsi, %rdi
	cmpq	%rbx, %rdi
	jb	.LBB0_24
.LBB0_25:                               # %._crit_edge
	addl	%eax, %r11d
	addl	%ecx, %r11d
	movl	%r11d, 16(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	cover_cost, .Lfunc_end0-cover_cost
	.cfi_endproc

	.globl	fmt_cost
	.p2align	4, 0x90
	.type	fmt_cost,@function
fmt_cost:                               # @fmt_cost
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movl	cube+4(%rip), %eax
	decl	%eax
	movl	(%rdi), %edx
	movl	4(%rdi), %ecx
	movl	8(%rdi), %r9d
	cmpl	%eax, cube+8(%rip)
	jne	.LBB1_2
# BB#1:
	movl	16(%rdi), %r10d
	movl	$fmt_cost.s, %edi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%r9d, %r8d
	movl	%r10d, %r9d
	jmp	.LBB1_3
.LBB1_2:
	movl	12(%rdi), %r8d
	movl	$fmt_cost.s, %edi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
.LBB1_3:
	callq	sprintf
	movl	$fmt_cost.s, %eax
	popq	%rcx
	retq
.Lfunc_end1:
	.size	fmt_cost, .Lfunc_end1-fmt_cost
	.cfi_endproc

	.globl	print_cost
	.p2align	4, 0x90
	.type	print_cost,@function
print_cost:                             # @print_cost
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 32
	movq	%rsp, %rsi
	callq	cover_cost
	movl	cube+4(%rip), %eax
	decl	%eax
	movl	(%rsp), %edx
	movl	4(%rsp), %ecx
	movl	8(%rsp), %r9d
	cmpl	%eax, cube+8(%rip)
	jne	.LBB2_2
# BB#1:
	movl	16(%rsp), %r10d
	movl	$fmt_cost.s, %edi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%r9d, %r8d
	movl	%r10d, %r9d
	jmp	.LBB2_3
.LBB2_2:
	movl	12(%rsp), %r8d
	movl	$fmt_cost.s, %edi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
.LBB2_3:                                # %fmt_cost.exit
	callq	sprintf
	movl	$fmt_cost.s, %eax
	addq	$24, %rsp
	retq
.Lfunc_end2:
	.size	print_cost, .Lfunc_end2-print_cost
	.cfi_endproc

	.globl	copy_cost
	.p2align	4, 0x90
	.type	copy_cost,@function
copy_cost:                              # @copy_cost
	.cfi_startproc
# BB#0:
	movups	(%rdi), %xmm0
	movups	%xmm0, (%rsi)
	movl	16(%rdi), %eax
	movl	%eax, 16(%rsi)
	movl	20(%rdi), %eax
	movl	%eax, 20(%rsi)
	retq
.Lfunc_end3:
	.size	copy_cost, .Lfunc_end3-copy_cost
	.cfi_endproc

	.globl	size_stamp
	.p2align	4, 0x90
	.type	size_stamp,@function
size_stamp:                             # @size_stamp
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rsp, %rsi
	callq	cover_cost
	movl	cube+4(%rip), %eax
	decl	%eax
	movl	(%rsp), %edx
	movl	4(%rsp), %ecx
	movl	8(%rsp), %r9d
	cmpl	%eax, cube+8(%rip)
	jne	.LBB4_2
# BB#1:
	movl	16(%rsp), %ebx
	movl	$fmt_cost.s, %edi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%r9d, %r8d
	movl	%ebx, %r9d
	jmp	.LBB4_3
.LBB4_2:
	movl	12(%rsp), %r8d
	movl	$fmt_cost.s, %edi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
.LBB4_3:                                # %print_cost.exit
	callq	sprintf
	movl	$.L.str.2, %edi
	movl	$fmt_cost.s, %edx
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	printf
	movq	stdout(%rip), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	jmp	fflush                  # TAILCALL
.Lfunc_end4:
	.size	size_stamp, .Lfunc_end4-size_stamp
	.cfi_endproc

	.globl	print_trace
	.p2align	4, 0x90
	.type	print_trace,@function
print_trace:                            # @print_trace
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 64
.Lcfi24:
	.cfi_offset %rbx, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	callq	util_print_time
	movq	%rax, %r15
	leaq	8(%rsp), %rsi
	movq	%rbx, %rdi
	callq	cover_cost
	movl	cube+4(%rip), %eax
	decl	%eax
	movl	8(%rsp), %edx
	movl	12(%rsp), %ecx
	movl	16(%rsp), %r9d
	cmpl	%eax, cube+8(%rip)
	jne	.LBB5_2
# BB#1:
	movl	24(%rsp), %ebx
	movl	$fmt_cost.s, %edi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%r9d, %r8d
	movl	%ebx, %r9d
	jmp	.LBB5_3
.LBB5_2:
	movl	20(%rsp), %r8d
	movl	$fmt_cost.s, %edi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
.LBB5_3:                                # %print_cost.exit
	callq	sprintf
	movl	$.L.str.3, %edi
	movl	$fmt_cost.s, %ecx
	xorl	%eax, %eax
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	printf
	movq	stdout(%rip), %rdi
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	fflush                  # TAILCALL
.Lfunc_end5:
	.size	print_trace, .Lfunc_end5-print_trace
	.cfi_endproc

	.globl	totals
	.p2align	4, 0x90
	.type	totals,@function
totals:                                 # @totals
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 48
.Lcfi32:
	.cfi_offset %rbx, -48
.Lcfi33:
	.cfi_offset %r12, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, %r14
	movl	%esi, %r15d
	movq	%rdi, %rbp
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbx
	subq	%rbp, %rbx
	movslq	%r15d, %rbp
	addq	%rbx, total_time(,%rbp,8)
	incl	total_calls(,%rbp,4)
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	cover_cost
	cmpl	$0, trace(%rip)
	je	.LBB6_5
# BB#1:
	movq	total_name(,%rbp,8), %r14
	movq	%rbx, %rdi
	callq	util_print_time
	movq	%rax, %r15
	movl	cube+4(%rip), %eax
	decl	%eax
	movl	(%r12), %edx
	movl	4(%r12), %ecx
	movl	8(%r12), %r9d
	cmpl	%eax, cube+8(%rip)
	jne	.LBB6_3
# BB#2:
	movl	16(%r12), %ebp
	movl	$fmt_cost.s, %edi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%r9d, %r8d
	movl	%ebp, %r9d
	jmp	.LBB6_4
.LBB6_5:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_3:
	movl	12(%r12), %r8d
	movl	$fmt_cost.s, %edi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
.LBB6_4:                                # %fmt_cost.exit
	callq	sprintf
	movl	$.L.str.3, %edi
	movl	$fmt_cost.s, %ecx
	xorl	%eax, %eax
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	printf
	movq	stdout(%rip), %rdi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fflush                  # TAILCALL
.Lfunc_end6:
	.size	totals, .Lfunc_end6-totals
	.cfi_endproc

	.globl	fatal
	.p2align	4, 0x90
	.type	fatal,@function
fatal:                                  # @fatal
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rcx
	movq	stderr(%rip), %rdi
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end7:
	.size	fatal, .Lfunc_end7-fatal
	.cfi_endproc

	.type	fmt_cost.s,@object      # @fmt_cost.s
	.local	fmt_cost.s
	.comm	fmt_cost.s,200,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"c=%d in=%d out=%d tot=%d"
	.size	.L.str, 25

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"c=%d in=%d mv=%d out=%d"
	.size	.L.str.1, 24

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"# %s\tCost is %s\n"
	.size	.L.str.2, 17

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"# %s\tTime was %s, cost is %s\n"
	.size	.L.str.3, 30

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"espresso: %s\n"
	.size	.L.str.4, 14


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
