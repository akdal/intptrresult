	.text
	.file	"ArchiveOpenCallback.bc"
	.globl	_ZN16COpenCallbackImp8SetTotalEPKyS1_
	.p2align	4, 0x90
	.type	_ZN16COpenCallbackImp8SetTotalEPKyS1_,@function
_ZN16COpenCallbackImp8SetTotalEPKyS1_:  # @_ZN16COpenCallbackImp8SetTotalEPKyS1_
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rax
	movq	176(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_4
# BB#1:
	movq	(%rdi), %rax
.Ltmp0:
	callq	*40(%rax)
.Ltmp1:
	jmp	.LBB0_9
.LBB0_4:
	movq	168(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_5
# BB#6:
	movq	(%rdi), %rax
.Ltmp2:
	callq	*8(%rax)
.Ltmp3:
	jmp	.LBB0_9
.LBB0_5:
	xorl	%eax, %eax
.LBB0_9:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_2:
.Ltmp4:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %r14
	cmpl	$2, %ebx
	je	.LBB0_3
# BB#8:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB0_9
.LBB0_3:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%r14, (%rax)
.Ltmp5:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp6:
# BB#10:
.LBB0_7:
.Ltmp7:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_ZN16COpenCallbackImp8SetTotalEPKyS1_, .Lfunc_end0-_ZN16COpenCallbackImp8SetTotalEPKyS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\302\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	3                       #   On action: 2
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp5-.Ltmp3           #   Call between .Ltmp3 and .Ltmp5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp6      #   Call between .Ltmp6 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN16COpenCallbackImp12SetCompletedEPKyS1_
	.p2align	4, 0x90
	.type	_ZN16COpenCallbackImp12SetCompletedEPKyS1_,@function
_ZN16COpenCallbackImp12SetCompletedEPKyS1_: # @_ZN16COpenCallbackImp12SetCompletedEPKyS1_
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rdi, %rax
	movq	176(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#1:
	movq	(%rdi), %rax
.Ltmp8:
	callq	*48(%rax)
.Ltmp9:
	jmp	.LBB1_9
.LBB1_4:
	movq	168(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB1_5
# BB#6:
	movq	(%rdi), %rax
.Ltmp10:
	callq	*16(%rax)
.Ltmp11:
	jmp	.LBB1_9
.LBB1_5:
	xorl	%eax, %eax
.LBB1_9:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_2:
.Ltmp12:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %r14
	cmpl	$2, %ebx
	je	.LBB1_3
# BB#8:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB1_9
.LBB1_3:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%r14, (%rax)
.Ltmp13:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp14:
# BB#10:
.LBB1_7:
.Ltmp15:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_ZN16COpenCallbackImp12SetCompletedEPKyS1_, .Lfunc_end1-_ZN16COpenCallbackImp12SetCompletedEPKyS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\302\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp8-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp11-.Ltmp8          #   Call between .Ltmp8 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin1   #     jumps to .Ltmp12
	.byte	3                       #   On action: 2
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp13-.Ltmp11         #   Call between .Ltmp11 and .Ltmp13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin1   #     jumps to .Ltmp15
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end1-.Ltmp14     #   Call between .Ltmp14 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN16COpenCallbackImp11GetPropertyEjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN16COpenCallbackImp11GetPropertyEjP14tagPROPVARIANT,@function
_ZN16COpenCallbackImp11GetPropertyEjP14tagPROPVARIANT: # @_ZN16COpenCallbackImp11GetPropertyEjP14tagPROPVARIANT
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 48
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %r14, -16
	movq	%rdx, %rbx
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %rax
	movl	$0, 8(%rsp)
	cmpb	$0, 112(%rax)
	je	.LBB2_4
# BB#1:
	cmpl	$4, %esi
	jne	.LBB2_13
# BB#2:
	movq	120(%rax), %rsi
.Ltmp16:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp17:
.LBB2_13:
.Ltmp32:
	leaq	8(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp33:
# BB#14:
.Ltmp38:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp39:
# BB#15:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	xorl	%eax, %eax
.LBB2_21:
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB2_4:
	addl	$-4, %esi
	cmpl	$8, %esi
	ja	.LBB2_13
# BB#5:
	jmpq	*.LJTI2_0(,%rsi,8)
.LBB2_6:
	movq	96(%rax), %rsi
.Ltmp30:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp31:
	jmp	.LBB2_13
.LBB2_11:
	addq	$72, %rax
.Ltmp20:
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME
.Ltmp21:
	jmp	.LBB2_13
.LBB2_7:
	movl	88(%rax), %esi
	andl	$16, %esi
	shrl	$4, %esi
.Ltmp28:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEb
.Ltmp29:
	jmp	.LBB2_13
.LBB2_12:
	addq	$80, %rax
.Ltmp18:
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME
.Ltmp19:
	jmp	.LBB2_13
.LBB2_8:
	movq	56(%rax), %rsi
.Ltmp26:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp27:
	jmp	.LBB2_13
.LBB2_9:
	movl	88(%rax), %esi
.Ltmp24:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp25:
	jmp	.LBB2_13
.LBB2_10:
	addq	$64, %rax
.Ltmp22:
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME
.Ltmp23:
	jmp	.LBB2_13
.LBB2_16:
.Ltmp40:
	movq	%rdx, %r14
	movq	%rax, %rbx
	jmp	.LBB2_17
.LBB2_3:
.Ltmp34:
	movq	%rdx, %r14
	movq	%rax, %rbx
.Ltmp35:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp36:
.LBB2_17:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit12
	movq	%rbx, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r14d
	je	.LBB2_18
# BB#20:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB2_21
.LBB2_18:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp41:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp42:
# BB#23:
.LBB2_19:
.Ltmp43:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB2_22:
.Ltmp37:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN16COpenCallbackImp11GetPropertyEjP14tagPROPVARIANT, .Lfunc_end2-_ZN16COpenCallbackImp11GetPropertyEjP14tagPROPVARIANT
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_6
	.quad	.LBB2_13
	.quad	.LBB2_7
	.quad	.LBB2_8
	.quad	.LBB2_13
	.quad	.LBB2_9
	.quad	.LBB2_10
	.quad	.LBB2_11
	.quad	.LBB2_12
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	105                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp33-.Ltmp16         #   Call between .Ltmp16 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin2   #     jumps to .Ltmp34
	.byte	3                       #   On action: 2
	.long	.Ltmp38-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin2   #     jumps to .Ltmp40
	.byte	3                       #   On action: 2
	.long	.Ltmp30-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp23-.Ltmp30         #   Call between .Ltmp30 and .Ltmp23
	.long	.Ltmp34-.Lfunc_begin2   #     jumps to .Ltmp34
	.byte	3                       #   On action: 2
	.long	.Ltmp35-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin2   #     jumps to .Ltmp37
	.byte	1                       #   On action: 1
	.long	.Ltmp36-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp41-.Ltmp36         #   Call between .Ltmp36 and .Ltmp41
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin2   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Lfunc_end2-.Ltmp42     #   Call between .Ltmp42 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end3:
	.size	__clang_call_terminate, .Lfunc_end3-__clang_call_terminate

	.text
	.globl	_ZThn8_N16COpenCallbackImp11GetPropertyEjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZThn8_N16COpenCallbackImp11GetPropertyEjP14tagPROPVARIANT,@function
_ZThn8_N16COpenCallbackImp11GetPropertyEjP14tagPROPVARIANT: # @_ZThn8_N16COpenCallbackImp11GetPropertyEjP14tagPROPVARIANT
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN16COpenCallbackImp11GetPropertyEjP14tagPROPVARIANT # TAILCALL
.Lfunc_end4:
	.size	_ZThn8_N16COpenCallbackImp11GetPropertyEjP14tagPROPVARIANT, .Lfunc_end4-_ZThn8_N16COpenCallbackImp11GetPropertyEjP14tagPROPVARIANT
	.cfi_endproc

	.globl	_ZN16COpenCallbackImp8FindNameERK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN16COpenCallbackImp8FindNameERK11CStringBaseIwE,@function
_ZN16COpenCallbackImp8FindNameERK11CStringBaseIwE: # @_ZN16COpenCallbackImp8FindNameERK11CStringBaseIwE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 48
.Lcfi20:
	.cfi_offset %rbx, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movl	$-1, %r14d
	cmpl	$0, 148(%rbp)
	jle	.LBB5_5
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	movq	152(%rbp), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%r15), %rdi
	movq	(%rax), %rsi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB5_4
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	incq	%rbx
	movslq	148(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB5_2
	jmp	.LBB5_5
.LBB5_4:                                # %.._crit_edge.loopexit_crit_edge
	movl	%ebx, %r14d
.LBB5_5:                                # %._crit_edge
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN16COpenCallbackImp8FindNameERK11CStringBaseIwE, .Lfunc_end5-_ZN16COpenCallbackImp8FindNameERK11CStringBaseIwE
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.zero	16
	.text
	.globl	_ZN16COpenCallbackImp9GetStreamEPKwPP9IInStream
	.p2align	4, 0x90
	.type	_ZN16COpenCallbackImp9GetStreamEPKwPP9IInStream,@function
_ZN16COpenCallbackImp9GetStreamEPKwPP9IInStream: # @_ZN16COpenCallbackImp9GetStreamEPKwPP9IInStream
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 80
.Lcfi31:
	.cfi_offset %rbx, -56
.Lcfi32:
	.cfi_offset %r12, -48
.Lcfi33:
	.cfi_offset %r13, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	$1, %ebx
	cmpb	$0, 112(%r13)
	je	.LBB6_1
.LBB6_58:
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_1:
	movq	168(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB6_4
# BB#2:
	movq	(%rdi), %rax
.Ltmp44:
	callq	*(%rax)
	movl	%eax, %ebx
.Ltmp45:
# BB#3:
	testl	%ebx, %ebx
	jne	.LBB6_58
.LBB6_4:
	movq	$0, (%r12)
	leaq	40(%r13), %rsi
.Ltmp46:
	leaq	8(%rsp), %rdi
	movq	%r14, %rdx
	callq	_ZplIwE11CStringBaseIT_ERKS2_PKS1_
.Ltmp47:
# BB#5:
	leaq	56(%r13), %rdi
	movq	8(%rsp), %rsi
.Ltmp49:
	callq	_ZN8NWindows5NFile5NFind10CFileInfoW4FindEPKw
.Ltmp50:
# BB#6:
	movl	$1, %ebx
	testb	%al, %al
	je	.LBB6_54
# BB#7:
	testb	$16, 88(%r13)
	jne	.LBB6_54
# BB#8:
.Ltmp51:
	movl	$1144, %edi             # imm = 0x478
	callq	_Znwm
	movq	%rax, %r15
.Ltmp52:
# BB#9:
	movl	$0, 16(%r15)
	movl	$_ZTV13CInFileStream+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTV13CInFileStream+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r15)
	movq	$_ZTVN8NWindows5NFile3NIO9CFileBaseE+16, 24(%r15)
	movl	$-1, 32(%r15)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 40(%r15)
.Ltmp54:
	movl	$4, %edi
	callq	_Znam
.Ltmp55:
# BB#10:                                # %.noexc
	movq	%r15, %rbx
	addq	$24, %rbx
	movq	%rax, 40(%r15)
	movb	$0, (%rax)
	movl	$4, 52(%r15)
	movq	$_ZTVN8NWindows5NFile3NIO7CInFileE+16, 24(%r15)
	movb	$0, 20(%r15)
	movl	$_ZTV16CInFileStreamVol+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTV16CInFileStreamVol+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r15)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 1112(%r15)
.Ltmp57:
	movl	$16, %edi
	callq	_Znam
.Ltmp58:
# BB#11:                                # %_ZN9CMyComPtrI9IInStreamEC2EPS0_.exit
	movq	%rax, 1112(%r15)
	movl	$0, (%rax)
	movl	$4, 1124(%r15)
	movq	$0, 1136(%r15)
	movl	$1, 16(%r15)
	movq	8(%rsp), %rsi
.Ltmp63:
	movq	%r15, %rdi
	callq	_ZN13CInFileStream4OpenEPKw
.Ltmp64:
# BB#12:
	testb	%al, %al
	je	.LBB6_53
# BB#13:
	movq	%r15, (%r12)
	movl	$0, 1120(%r15)
	movq	1112(%r15), %rbx
	movl	$0, (%rbx)
	xorl	%ebp, %ebp
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB6_14:                               # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB6_14
# BB#15:                                # %_Z11MyStringLenIwEiPKT_.exit.i
	movl	1124(%r15), %r12d
	cmpl	%ebp, %r12d
	je	.LBB6_21
# BB#16:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp72:
	callq	_Znam
.Ltmp73:
# BB#17:                                # %.noexc38
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB6_20
# BB#18:                                # %.noexc38
	testl	%r12d, %r12d
	jle	.LBB6_20
# BB#19:                                # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	%rbx, %rax
	movslq	1120(%r15), %rcx
.LBB6_20:                               # %._crit_edge16.i.i
	movq	%rax, 1112(%r15)
	movl	$0, (%rax,%rcx,4)
	movl	%ebp, 1124(%r15)
	movq	%rax, %rbx
.LBB6_21:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.preheader
	decl	%ebp
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB6_22:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB6_22
# BB#23:
	movl	%ebp, 1120(%r15)
	movq	%r13, 1128(%r15)
	testq	%r13, %r13
	je	.LBB6_25
# BB#24:
	movq	(%r13), %rax
.Ltmp74:
	movq	%r13, %rdi
	callq	*8(%rax)
.Ltmp75:
.LBB6_25:                               # %.noexc39
	movq	1136(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB6_27
# BB#26:
	movq	(%rdi), %rax
.Ltmp76:
	callq	*16(%rax)
.Ltmp77:
.LBB6_27:
	movq	%r13, 1136(%r15)
	movl	$-1, %ebp
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB6_28:                               # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB6_28
# BB#29:                                # %_Z11MyStringLenIwEiPKT_.exit.i41
	leal	1(%rbp), %r15d
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %r12
	cmovnoq	%rax, %r12
.Ltmp79:
	movq	%r12, %rdi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp80:
# BB#30:                                # %.noexc45
	movl	%r15d, 4(%rsp)          # 4-byte Spill
	movl	$0, (%rbx)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_31:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i44
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB6_31
# BB#32:                                # %_ZN11CStringBaseIwEC2EPKw.exit
.Ltmp82:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp83:
# BB#33:                                # %.noexc46
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%r15)
.Ltmp84:
	movq	%r12, %rdi
	callq	_Znam
.Ltmp85:
# BB#34:                                # %.noexc.i
	movq	%rax, (%r15)
	movl	$0, (%rax)
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, 12(%r15)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_35:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB6_35
# BB#36:
	movl	%ebp, 8(%r15)
	leaq	136(%r13), %rdi
.Ltmp87:
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp88:
# BB#37:                                # %.thread
	movq	152(%r13), %rax
	movslq	148(%r13), %rcx
	movq	%r15, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 148(%r13)
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	56(%r13), %rax
	addq	%rax, 184(%r13)
	xorl	%ebx, %ebx
	jmp	.LBB6_54
.LBB6_53:
	callq	__errno_location
	movl	(%rax), %ebx
	movq	(%r15), %rax
.Ltmp69:
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp70:
.LBB6_54:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_58
# BB#55:
	callq	_ZdaPv
	jmp	.LBB6_58
.LBB6_46:
.Ltmp86:
	movq	%rdx, %r14
	movq	%rax, %r12
	movq	%r15, %rdi
	callq	_ZdlPv
	jmp	.LBB6_49
.LBB6_47:
.Ltmp81:
	jmp	.LBB6_39
.LBB6_50:
.Ltmp71:
	jmp	.LBB6_39
.LBB6_51:                               # %.thread74
.Ltmp78:
	jmp	.LBB6_39
.LBB6_48:
.Ltmp89:
	movq	%rdx, %r14
	movq	%rax, %r12
.LBB6_49:                               # %_ZN11CStringBaseIwED2Ev.exit51
	movq	%rbx, %rdi
	callq	_ZdaPv
	jmp	.LBB6_44
.LBB6_52:
.Ltmp65:
	movq	%rdx, %r14
	movq	%rax, %r12
	movq	(%r15), %rax
.Ltmp66:
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp67:
	jmp	.LBB6_44
.LBB6_59:
.Ltmp68:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_40:
.Ltmp59:
	movq	%rdx, %r14
	movq	%rax, %r12
	movl	$_ZTV13CInFileStream+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTV13CInFileStream+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r15)
.Ltmp60:
	movq	%rbx, %rdi
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp61:
	jmp	.LBB6_43
.LBB6_41:
.Ltmp62:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_42:
.Ltmp56:
	movq	%rdx, %r14
	movq	%rax, %r12
.LBB6_43:                               # %.body
	movq	%r15, %rdi
	callq	_ZdlPv
	jmp	.LBB6_44
.LBB6_38:
.Ltmp53:
.LBB6_39:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit54
	movq	%rdx, %r14
	movq	%rax, %r12
.LBB6_44:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit54
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_62
# BB#45:
	callq	_ZdaPv
	jmp	.LBB6_62
.LBB6_61:
.Ltmp48:
	movq	%rdx, %r14
	movq	%rax, %r12
.LBB6_62:
	movq	%r12, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r14d
	je	.LBB6_63
# BB#57:
	callq	__cxa_end_catch
	movl	$-2147024882, %ebx      # imm = 0x8007000E
	jmp	.LBB6_58
.LBB6_63:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp90:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp91:
# BB#60:
.LBB6_56:
.Ltmp92:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZN16COpenCallbackImp9GetStreamEPKwPP9IInStream, .Lfunc_end6-_ZN16COpenCallbackImp9GetStreamEPKwPP9IInStream
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\337\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\320\001"              # Call site table length
	.long	.Ltmp44-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp47-.Ltmp44         #   Call between .Ltmp44 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin3   #     jumps to .Ltmp48
	.byte	3                       #   On action: 2
	.long	.Ltmp49-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp52-.Ltmp49         #   Call between .Ltmp49 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin3   #     jumps to .Ltmp53
	.byte	3                       #   On action: 2
	.long	.Ltmp54-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin3   #     jumps to .Ltmp56
	.byte	3                       #   On action: 2
	.long	.Ltmp57-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin3   #     jumps to .Ltmp59
	.byte	3                       #   On action: 2
	.long	.Ltmp63-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp64-.Ltmp63         #   Call between .Ltmp63 and .Ltmp64
	.long	.Ltmp65-.Lfunc_begin3   #     jumps to .Ltmp65
	.byte	3                       #   On action: 2
	.long	.Ltmp72-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp77-.Ltmp72         #   Call between .Ltmp72 and .Ltmp77
	.long	.Ltmp78-.Lfunc_begin3   #     jumps to .Ltmp78
	.byte	3                       #   On action: 2
	.long	.Ltmp79-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin3   #     jumps to .Ltmp81
	.byte	3                       #   On action: 2
	.long	.Ltmp82-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp89-.Lfunc_begin3   #     jumps to .Ltmp89
	.byte	3                       #   On action: 2
	.long	.Ltmp84-.Lfunc_begin3   # >> Call Site 9 <<
	.long	.Ltmp85-.Ltmp84         #   Call between .Ltmp84 and .Ltmp85
	.long	.Ltmp86-.Lfunc_begin3   #     jumps to .Ltmp86
	.byte	3                       #   On action: 2
	.long	.Ltmp87-.Lfunc_begin3   # >> Call Site 10 <<
	.long	.Ltmp88-.Ltmp87         #   Call between .Ltmp87 and .Ltmp88
	.long	.Ltmp89-.Lfunc_begin3   #     jumps to .Ltmp89
	.byte	3                       #   On action: 2
	.long	.Ltmp69-.Lfunc_begin3   # >> Call Site 11 <<
	.long	.Ltmp70-.Ltmp69         #   Call between .Ltmp69 and .Ltmp70
	.long	.Ltmp71-.Lfunc_begin3   #     jumps to .Ltmp71
	.byte	3                       #   On action: 2
	.long	.Ltmp66-.Lfunc_begin3   # >> Call Site 12 <<
	.long	.Ltmp67-.Ltmp66         #   Call between .Ltmp66 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin3   #     jumps to .Ltmp68
	.byte	1                       #   On action: 1
	.long	.Ltmp60-.Lfunc_begin3   # >> Call Site 13 <<
	.long	.Ltmp61-.Ltmp60         #   Call between .Ltmp60 and .Ltmp61
	.long	.Ltmp62-.Lfunc_begin3   #     jumps to .Ltmp62
	.byte	1                       #   On action: 1
	.long	.Ltmp61-.Lfunc_begin3   # >> Call Site 14 <<
	.long	.Ltmp90-.Ltmp61         #   Call between .Ltmp61 and .Ltmp90
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp90-.Lfunc_begin3   # >> Call Site 15 <<
	.long	.Ltmp91-.Ltmp90         #   Call between .Ltmp90 and .Ltmp91
	.long	.Ltmp92-.Lfunc_begin3   #     jumps to .Ltmp92
	.byte	0                       #   On action: cleanup
	.long	.Ltmp91-.Lfunc_begin3   # >> Call Site 16 <<
	.long	.Lfunc_end6-.Ltmp91     #   Call between .Ltmp91 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZplIwE11CStringBaseIT_ERKS2_PKS1_,"axG",@progbits,_ZplIwE11CStringBaseIT_ERKS2_PKS1_,comdat
	.weak	_ZplIwE11CStringBaseIT_ERKS2_PKS1_
	.p2align	4, 0x90
	.type	_ZplIwE11CStringBaseIT_ERKS2_PKS1_,@function
_ZplIwE11CStringBaseIT_ERKS2_PKS1_:     # @_ZplIwE11CStringBaseIT_ERKS2_PKS1_
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 48
.Lcfi42:
	.cfi_offset %rbx, -48
.Lcfi43:
	.cfi_offset %r12, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movslq	8(%r15), %r12
	leaq	1(%r12), %rbp
	testl	%ebp, %ebp
	je	.LBB7_1
# BB#2:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	%ebp, 12(%r14)
	jmp	.LBB7_3
.LBB7_1:
	xorl	%eax, %eax
.LBB7_3:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB7_4:                                # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB7_4
# BB#5:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit
	movl	%r12d, 8(%r14)
	movl	$-1, %ebp
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB7_6:                                # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB7_6
# BB#7:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
.Ltmp93:
	movq	%r14, %rdi
	movl	%ebp, %esi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp94:
# BB#8:                                 # %.noexc
	movslq	8(%r14), %rax
	movq	%rax, %rcx
	shlq	$2, %rcx
	addq	(%r14), %rcx
	.p2align	4, 0x90
.LBB7_9:                                # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %edx
	addq	$4, %rbx
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB7_9
# BB#10:
	addl	%eax, %ebp
	movl	%ebp, 8(%r14)
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_11:
.Ltmp95:
	movq	%rax, %rbx
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB7_13
# BB#12:
	callq	_ZdaPv
.LBB7_13:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZplIwE11CStringBaseIT_ERKS2_PKS1_, .Lfunc_end7-_ZplIwE11CStringBaseIT_ERKS2_PKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp93-.Lfunc_begin4   #   Call between .Lfunc_begin4 and .Ltmp93
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp93-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp94-.Ltmp93         #   Call between .Ltmp93 and .Ltmp94
	.long	.Ltmp95-.Lfunc_begin4   #     jumps to .Ltmp95
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Lfunc_end7-.Ltmp94     #   Call between .Ltmp94 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn8_N16COpenCallbackImp9GetStreamEPKwPP9IInStream
	.p2align	4, 0x90
	.type	_ZThn8_N16COpenCallbackImp9GetStreamEPKwPP9IInStream,@function
_ZThn8_N16COpenCallbackImp9GetStreamEPKwPP9IInStream: # @_ZThn8_N16COpenCallbackImp9GetStreamEPKwPP9IInStream
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN16COpenCallbackImp9GetStreamEPKwPP9IInStream # TAILCALL
.Lfunc_end8:
	.size	_ZThn8_N16COpenCallbackImp9GetStreamEPKwPP9IInStream, .Lfunc_end8-_ZThn8_N16COpenCallbackImp9GetStreamEPKwPP9IInStream
	.cfi_endproc

	.globl	_ZN16COpenCallbackImp21CryptoGetTextPasswordEPPw
	.p2align	4, 0x90
	.type	_ZN16COpenCallbackImp21CryptoGetTextPasswordEPPw,@function
_ZN16COpenCallbackImp21CryptoGetTextPasswordEPPw: # @_ZN16COpenCallbackImp21CryptoGetTextPasswordEPPw
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi49:
	.cfi_def_cfa_offset 32
.Lcfi50:
	.cfi_offset %rbx, -24
.Lcfi51:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	176(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_13
# BB#1:
	movq	$0, (%rsp)
	movq	(%rdi), %rax
.Ltmp96:
	movq	%rsp, %rdx
	movl	$IID_ICryptoGetTextPassword, %esi
	callq	*(%rax)
.Ltmp97:
# BB#2:                                 # %_ZNK9CMyComPtrI20IArchiveOpenCallbackE14QueryInterfaceI22ICryptoGetTextPasswordEEiRK4GUIDPPT_.exit
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_13
# BB#3:
	movq	(%rdi), %rax
.Ltmp98:
	movq	%r14, %rsi
	callq	*40(%rax)
	movl	%eax, %ebx
.Ltmp99:
# BB#4:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_18
# BB#5:
	movq	(%rdi), %rax
.Ltmp104:
	callq	*16(%rax)
.Ltmp105:
	jmp	.LBB9_18
.LBB9_13:
	movq	168(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_14
# BB#15:
	movq	(%rdi), %rax
.Ltmp107:
	movq	%r14, %rsi
	callq	*24(%rax)
	movl	%eax, %ebx
.Ltmp108:
	jmp	.LBB9_18
.LBB9_14:
	movl	$-2147467263, %ebx      # imm = 0x80004001
.LBB9_18:
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB9_9:
.Ltmp106:
	jmp	.LBB9_10
.LBB9_6:
.Ltmp109:
.LBB9_10:
	movq	%rdx, %r14
	movq	%rax, %rbx
	jmp	.LBB9_11
.LBB9_7:
.Ltmp100:
	movq	%rdx, %r14
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_11
# BB#8:
	movq	(%rdi), %rax
.Ltmp101:
	callq	*16(%rax)
.Ltmp102:
.LBB9_11:
	movq	%rbx, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r14d
	je	.LBB9_12
# BB#17:
	callq	__cxa_end_catch
	movl	$-2147024882, %ebx      # imm = 0x8007000E
	jmp	.LBB9_18
.LBB9_12:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp110:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp111:
# BB#20:
.LBB9_16:
.Ltmp112:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB9_19:
.Ltmp103:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN16COpenCallbackImp21CryptoGetTextPasswordEPPw, .Lfunc_end9-_ZN16COpenCallbackImp21CryptoGetTextPasswordEPPw
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	105                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp96-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp99-.Ltmp96         #   Call between .Ltmp96 and .Ltmp99
	.long	.Ltmp100-.Lfunc_begin5  #     jumps to .Ltmp100
	.byte	3                       #   On action: 2
	.long	.Ltmp104-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp105-.Ltmp104       #   Call between .Ltmp104 and .Ltmp105
	.long	.Ltmp106-.Lfunc_begin5  #     jumps to .Ltmp106
	.byte	3                       #   On action: 2
	.long	.Ltmp107-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp108-.Ltmp107       #   Call between .Ltmp107 and .Ltmp108
	.long	.Ltmp109-.Lfunc_begin5  #     jumps to .Ltmp109
	.byte	3                       #   On action: 2
	.long	.Ltmp101-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp102-.Ltmp101       #   Call between .Ltmp101 and .Ltmp102
	.long	.Ltmp103-.Lfunc_begin5  #     jumps to .Ltmp103
	.byte	1                       #   On action: 1
	.long	.Ltmp102-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp110-.Ltmp102       #   Call between .Ltmp102 and .Ltmp110
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp110-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp111-.Ltmp110       #   Call between .Ltmp110 and .Ltmp111
	.long	.Ltmp112-.Lfunc_begin5  #     jumps to .Ltmp112
	.byte	0                       #   On action: cleanup
	.long	.Ltmp111-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Lfunc_end9-.Ltmp111    #   Call between .Ltmp111 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn24_N16COpenCallbackImp21CryptoGetTextPasswordEPPw
	.p2align	4, 0x90
	.type	_ZThn24_N16COpenCallbackImp21CryptoGetTextPasswordEPPw,@function
_ZThn24_N16COpenCallbackImp21CryptoGetTextPasswordEPPw: # @_ZThn24_N16COpenCallbackImp21CryptoGetTextPasswordEPPw
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN16COpenCallbackImp21CryptoGetTextPasswordEPPw # TAILCALL
.Lfunc_end10:
	.size	_ZThn24_N16COpenCallbackImp21CryptoGetTextPasswordEPPw, .Lfunc_end10-_ZThn24_N16COpenCallbackImp21CryptoGetTextPasswordEPPw
	.cfi_endproc

	.section	.text._ZN16COpenCallbackImp14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN16COpenCallbackImp14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN16COpenCallbackImp14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN16COpenCallbackImp14QueryInterfaceERK4GUIDPPv,@function
_ZN16COpenCallbackImp14QueryInterfaceERK4GUIDPPv: # @_ZN16COpenCallbackImp14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB11_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB11_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB11_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB11_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB11_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB11_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB11_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB11_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB11_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB11_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB11_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB11_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB11_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB11_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB11_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB11_16
.LBB11_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_IArchiveOpenVolumeCallback(%rip), %cl
	jne	.LBB11_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_IArchiveOpenVolumeCallback+1(%rip), %al
	jne	.LBB11_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_IArchiveOpenVolumeCallback+2(%rip), %al
	jne	.LBB11_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_IArchiveOpenVolumeCallback+3(%rip), %al
	jne	.LBB11_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_IArchiveOpenVolumeCallback+4(%rip), %al
	jne	.LBB11_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_IArchiveOpenVolumeCallback+5(%rip), %al
	jne	.LBB11_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_IArchiveOpenVolumeCallback+6(%rip), %al
	jne	.LBB11_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_IArchiveOpenVolumeCallback+7(%rip), %al
	jne	.LBB11_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_IArchiveOpenVolumeCallback+8(%rip), %al
	jne	.LBB11_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_IArchiveOpenVolumeCallback+9(%rip), %al
	jne	.LBB11_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_IArchiveOpenVolumeCallback+10(%rip), %al
	jne	.LBB11_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_IArchiveOpenVolumeCallback+11(%rip), %al
	jne	.LBB11_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_IArchiveOpenVolumeCallback+12(%rip), %al
	jne	.LBB11_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_IArchiveOpenVolumeCallback+13(%rip), %al
	jne	.LBB11_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_IArchiveOpenVolumeCallback+14(%rip), %al
	jne	.LBB11_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit10
	movb	15(%rsi), %al
	cmpb	IID_IArchiveOpenVolumeCallback+15(%rip), %al
	jne	.LBB11_33
.LBB11_16:
	leaq	8(%rdi), %rax
	jmp	.LBB11_67
.LBB11_33:                              # %_ZeqRK4GUIDS1_.exit10.thread
	cmpb	IID_ICryptoGetTextPassword(%rip), %cl
	jne	.LBB11_50
# BB#34:
	movb	1(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+1(%rip), %al
	jne	.LBB11_50
# BB#35:
	movb	2(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+2(%rip), %al
	jne	.LBB11_50
# BB#36:
	movb	3(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+3(%rip), %al
	jne	.LBB11_50
# BB#37:
	movb	4(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+4(%rip), %al
	jne	.LBB11_50
# BB#38:
	movb	5(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+5(%rip), %al
	jne	.LBB11_50
# BB#39:
	movb	6(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+6(%rip), %al
	jne	.LBB11_50
# BB#40:
	movb	7(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+7(%rip), %al
	jne	.LBB11_50
# BB#41:
	movb	8(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+8(%rip), %al
	jne	.LBB11_50
# BB#42:
	movb	9(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+9(%rip), %al
	jne	.LBB11_50
# BB#43:
	movb	10(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+10(%rip), %al
	jne	.LBB11_50
# BB#44:
	movb	11(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+11(%rip), %al
	jne	.LBB11_50
# BB#45:
	movb	12(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+12(%rip), %al
	jne	.LBB11_50
# BB#46:
	movb	13(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+13(%rip), %al
	jne	.LBB11_50
# BB#47:
	movb	14(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+14(%rip), %al
	jne	.LBB11_50
# BB#48:                                # %_ZeqRK4GUIDS1_.exit14
	movb	15(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+15(%rip), %al
	jne	.LBB11_50
# BB#49:
	leaq	24(%rdi), %rax
	jmp	.LBB11_67
.LBB11_50:                              # %_ZeqRK4GUIDS1_.exit14.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IArchiveOpenSetSubArchiveName(%rip), %cl
	jne	.LBB11_68
# BB#51:
	movb	1(%rsi), %cl
	cmpb	IID_IArchiveOpenSetSubArchiveName+1(%rip), %cl
	jne	.LBB11_68
# BB#52:
	movb	2(%rsi), %cl
	cmpb	IID_IArchiveOpenSetSubArchiveName+2(%rip), %cl
	jne	.LBB11_68
# BB#53:
	movb	3(%rsi), %cl
	cmpb	IID_IArchiveOpenSetSubArchiveName+3(%rip), %cl
	jne	.LBB11_68
# BB#54:
	movb	4(%rsi), %cl
	cmpb	IID_IArchiveOpenSetSubArchiveName+4(%rip), %cl
	jne	.LBB11_68
# BB#55:
	movb	5(%rsi), %cl
	cmpb	IID_IArchiveOpenSetSubArchiveName+5(%rip), %cl
	jne	.LBB11_68
# BB#56:
	movb	6(%rsi), %cl
	cmpb	IID_IArchiveOpenSetSubArchiveName+6(%rip), %cl
	jne	.LBB11_68
# BB#57:
	movb	7(%rsi), %cl
	cmpb	IID_IArchiveOpenSetSubArchiveName+7(%rip), %cl
	jne	.LBB11_68
# BB#58:
	movb	8(%rsi), %cl
	cmpb	IID_IArchiveOpenSetSubArchiveName+8(%rip), %cl
	jne	.LBB11_68
# BB#59:
	movb	9(%rsi), %cl
	cmpb	IID_IArchiveOpenSetSubArchiveName+9(%rip), %cl
	jne	.LBB11_68
# BB#60:
	movb	10(%rsi), %cl
	cmpb	IID_IArchiveOpenSetSubArchiveName+10(%rip), %cl
	jne	.LBB11_68
# BB#61:
	movb	11(%rsi), %cl
	cmpb	IID_IArchiveOpenSetSubArchiveName+11(%rip), %cl
	jne	.LBB11_68
# BB#62:
	movb	12(%rsi), %cl
	cmpb	IID_IArchiveOpenSetSubArchiveName+12(%rip), %cl
	jne	.LBB11_68
# BB#63:
	movb	13(%rsi), %cl
	cmpb	IID_IArchiveOpenSetSubArchiveName+13(%rip), %cl
	jne	.LBB11_68
# BB#64:
	movb	14(%rsi), %cl
	cmpb	IID_IArchiveOpenSetSubArchiveName+14(%rip), %cl
	jne	.LBB11_68
# BB#65:                                # %_ZeqRK4GUIDS1_.exit12
	movb	15(%rsi), %cl
	cmpb	IID_IArchiveOpenSetSubArchiveName+15(%rip), %cl
	jne	.LBB11_68
# BB#66:
	leaq	16(%rdi), %rax
.LBB11_67:                              # %_ZeqRK4GUIDS1_.exit12.thread
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB11_68:                              # %_ZeqRK4GUIDS1_.exit12.thread
	popq	%rcx
	retq
.Lfunc_end11:
	.size	_ZN16COpenCallbackImp14QueryInterfaceERK4GUIDPPv, .Lfunc_end11-_ZN16COpenCallbackImp14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN16COpenCallbackImp6AddRefEv,"axG",@progbits,_ZN16COpenCallbackImp6AddRefEv,comdat
	.weak	_ZN16COpenCallbackImp6AddRefEv
	.p2align	4, 0x90
	.type	_ZN16COpenCallbackImp6AddRefEv,@function
_ZN16COpenCallbackImp6AddRefEv:         # @_ZN16COpenCallbackImp6AddRefEv
	.cfi_startproc
# BB#0:
	movl	32(%rdi), %eax
	incl	%eax
	movl	%eax, 32(%rdi)
	retq
.Lfunc_end12:
	.size	_ZN16COpenCallbackImp6AddRefEv, .Lfunc_end12-_ZN16COpenCallbackImp6AddRefEv
	.cfi_endproc

	.section	.text._ZN16COpenCallbackImp7ReleaseEv,"axG",@progbits,_ZN16COpenCallbackImp7ReleaseEv,comdat
	.weak	_ZN16COpenCallbackImp7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN16COpenCallbackImp7ReleaseEv,@function
_ZN16COpenCallbackImp7ReleaseEv:        # @_ZN16COpenCallbackImp7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi53:
	.cfi_def_cfa_offset 16
	movl	32(%rdi), %eax
	decl	%eax
	movl	%eax, 32(%rdi)
	jne	.LBB13_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB13_2:
	popq	%rcx
	retq
.Lfunc_end13:
	.size	_ZN16COpenCallbackImp7ReleaseEv, .Lfunc_end13-_ZN16COpenCallbackImp7ReleaseEv
	.cfi_endproc

	.section	.text._ZN16COpenCallbackImpD2Ev,"axG",@progbits,_ZN16COpenCallbackImpD2Ev,comdat
	.weak	_ZN16COpenCallbackImpD2Ev
	.p2align	4, 0x90
	.type	_ZN16COpenCallbackImpD2Ev,@function
_ZN16COpenCallbackImpD2Ev:              # @_ZN16COpenCallbackImpD2Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 32
.Lcfi57:
	.cfi_offset %rbx, -32
.Lcfi58:
	.cfi_offset %r14, -24
.Lcfi59:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movl	$_ZTV16COpenCallbackImp+120, %eax
	movd	%rax, %xmm0
	movl	$_ZTV16COpenCallbackImp+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r15)
	movl	$_ZTV16COpenCallbackImp+256, %eax
	movd	%rax, %xmm0
	movl	$_ZTV16COpenCallbackImp+192, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%r15)
	movq	176(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB14_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp113:
	callq	*16(%rax)
.Ltmp114:
.LBB14_2:                               # %_ZN9CMyComPtrI20IArchiveOpenCallbackED2Ev.exit
	leaq	136(%r15), %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 136(%r15)
.Ltmp125:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp126:
# BB#3:
.Ltmp131:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp132:
# BB#4:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	120(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB14_6
# BB#5:
	callq	_ZdaPv
.LBB14_6:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	96(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB14_8
# BB#7:
	callq	_ZdaPv
.LBB14_8:                               # %_ZN8NWindows5NFile5NFind10CFileInfoWD2Ev.exit
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB14_12
# BB#9:
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZdaPv                  # TAILCALL
.LBB14_12:                              # %_ZN11CStringBaseIwED2Ev.exit8
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB14_13:
.Ltmp115:
	movq	%rax, %r14
	leaq	136(%r15), %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 136(%r15)
.Ltmp116:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp117:
# BB#14:
.Ltmp122:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp123:
	jmp	.LBB14_18
.LBB14_25:
.Ltmp124:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB14_15:
.Ltmp118:
	movq	%rax, %r14
.Ltmp119:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp120:
# BB#26:                                # %.body10
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB14_16:
.Ltmp121:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB14_17:
.Ltmp133:
	movq	%rax, %r14
	jmp	.LBB14_18
.LBB14_10:
.Ltmp127:
	movq	%rax, %r14
.Ltmp128:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp129:
.LBB14_18:                              # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit12
	movq	120(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB14_20
# BB#19:
	callq	_ZdaPv
.LBB14_20:                              # %_ZN11CStringBaseIwED2Ev.exit13
	movq	96(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB14_22
# BB#21:
	callq	_ZdaPv
.LBB14_22:                              # %_ZN8NWindows5NFile5NFind10CFileInfoWD2Ev.exit14
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB14_24
# BB#23:
	callq	_ZdaPv
.LBB14_24:                              # %_ZN11CStringBaseIwED2Ev.exit15
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB14_11:
.Ltmp130:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end14:
	.size	_ZN16COpenCallbackImpD2Ev, .Lfunc_end14-_ZN16COpenCallbackImpD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp113-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp114-.Ltmp113       #   Call between .Ltmp113 and .Ltmp114
	.long	.Ltmp115-.Lfunc_begin6  #     jumps to .Ltmp115
	.byte	0                       #   On action: cleanup
	.long	.Ltmp125-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp126-.Ltmp125       #   Call between .Ltmp125 and .Ltmp126
	.long	.Ltmp127-.Lfunc_begin6  #     jumps to .Ltmp127
	.byte	0                       #   On action: cleanup
	.long	.Ltmp131-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp132-.Ltmp131       #   Call between .Ltmp131 and .Ltmp132
	.long	.Ltmp133-.Lfunc_begin6  #     jumps to .Ltmp133
	.byte	0                       #   On action: cleanup
	.long	.Ltmp116-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp117-.Ltmp116       #   Call between .Ltmp116 and .Ltmp117
	.long	.Ltmp118-.Lfunc_begin6  #     jumps to .Ltmp118
	.byte	1                       #   On action: 1
	.long	.Ltmp122-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp123-.Ltmp122       #   Call between .Ltmp122 and .Ltmp123
	.long	.Ltmp124-.Lfunc_begin6  #     jumps to .Ltmp124
	.byte	1                       #   On action: 1
	.long	.Ltmp119-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp120-.Ltmp119       #   Call between .Ltmp119 and .Ltmp120
	.long	.Ltmp121-.Lfunc_begin6  #     jumps to .Ltmp121
	.byte	1                       #   On action: 1
	.long	.Ltmp128-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Ltmp129-.Ltmp128       #   Call between .Ltmp128 and .Ltmp129
	.long	.Ltmp130-.Lfunc_begin6  #     jumps to .Ltmp130
	.byte	1                       #   On action: 1
	.long	.Ltmp129-.Lfunc_begin6  # >> Call Site 8 <<
	.long	.Lfunc_end14-.Ltmp129   #   Call between .Ltmp129 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN16COpenCallbackImpD0Ev,"axG",@progbits,_ZN16COpenCallbackImpD0Ev,comdat
	.weak	_ZN16COpenCallbackImpD0Ev
	.p2align	4, 0x90
	.type	_ZN16COpenCallbackImpD0Ev,@function
_ZN16COpenCallbackImpD0Ev:              # @_ZN16COpenCallbackImpD0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi62:
	.cfi_def_cfa_offset 32
.Lcfi63:
	.cfi_offset %rbx, -24
.Lcfi64:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp134:
	callq	_ZN16COpenCallbackImpD2Ev
.Ltmp135:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB15_2:
.Ltmp136:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end15:
	.size	_ZN16COpenCallbackImpD0Ev, .Lfunc_end15-_ZN16COpenCallbackImpD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp134-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp135-.Ltmp134       #   Call between .Ltmp134 and .Ltmp135
	.long	.Ltmp136-.Lfunc_begin7  #     jumps to .Ltmp136
	.byte	0                       #   On action: cleanup
	.long	.Ltmp135-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Lfunc_end15-.Ltmp135   #   Call between .Ltmp135 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN16COpenCallbackImp17SetSubArchiveNameEPKw,"axG",@progbits,_ZN16COpenCallbackImp17SetSubArchiveNameEPKw,comdat
	.weak	_ZN16COpenCallbackImp17SetSubArchiveNameEPKw
	.p2align	4, 0x90
	.type	_ZN16COpenCallbackImp17SetSubArchiveNameEPKw,@function
_ZN16COpenCallbackImp17SetSubArchiveNameEPKw: # @_ZN16COpenCallbackImp17SetSubArchiveNameEPKw
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi69:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi71:
	.cfi_def_cfa_offset 64
.Lcfi72:
	.cfi_offset %rbx, -56
.Lcfi73:
	.cfi_offset %r12, -48
.Lcfi74:
	.cfi_offset %r13, -40
.Lcfi75:
	.cfi_offset %r14, -32
.Lcfi76:
	.cfi_offset %r15, -24
.Lcfi77:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movb	$1, 112(%r14)
	movl	$0, 128(%r14)
	movq	120(%r14), %rbx
	movl	$0, (%rbx)
	xorl	%ebp, %ebp
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB16_1:                               # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB16_1
# BB#2:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	movl	132(%r14), %r13d
	cmpl	%ebp, %r13d
	je	.LBB16_7
# BB#3:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB16_6
# BB#4:
	testl	%r13d, %r13d
	jle	.LBB16_6
# BB#5:                                 # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	128(%r14), %rax
.LBB16_6:                               # %._crit_edge16.i.i
	movq	%r12, 120(%r14)
	movl	$0, (%r12,%rax,4)
	movl	%ebp, 132(%r14)
	movq	%r12, %rbx
.LBB16_7:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.preheader
	decl	%ebp
	.p2align	4, 0x90
.LBB16_8:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15), %eax
	addq	$4, %r15
	movl	%eax, (%rbx)
	addq	$4, %rbx
	testl	%eax, %eax
	jne	.LBB16_8
# BB#9:                                 # %_ZN11CStringBaseIwEaSEPKw.exit
	movl	%ebp, 128(%r14)
	movq	$0, 184(%r14)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	_ZN16COpenCallbackImp17SetSubArchiveNameEPKw, .Lfunc_end16-_ZN16COpenCallbackImp17SetSubArchiveNameEPKw
	.cfi_endproc

	.section	.text._ZThn8_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN16COpenCallbackImp14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end17:
	.size	_ZThn8_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv, .Lfunc_end17-_ZThn8_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N16COpenCallbackImp6AddRefEv,"axG",@progbits,_ZThn8_N16COpenCallbackImp6AddRefEv,comdat
	.weak	_ZThn8_N16COpenCallbackImp6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N16COpenCallbackImp6AddRefEv,@function
_ZThn8_N16COpenCallbackImp6AddRefEv:    # @_ZThn8_N16COpenCallbackImp6AddRefEv
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	incl	%eax
	movl	%eax, 24(%rdi)
	retq
.Lfunc_end18:
	.size	_ZThn8_N16COpenCallbackImp6AddRefEv, .Lfunc_end18-_ZThn8_N16COpenCallbackImp6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N16COpenCallbackImp7ReleaseEv,"axG",@progbits,_ZThn8_N16COpenCallbackImp7ReleaseEv,comdat
	.weak	_ZThn8_N16COpenCallbackImp7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N16COpenCallbackImp7ReleaseEv,@function
_ZThn8_N16COpenCallbackImp7ReleaseEv:   # @_ZThn8_N16COpenCallbackImp7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi78:
	.cfi_def_cfa_offset 16
	movl	24(%rdi), %eax
	decl	%eax
	movl	%eax, 24(%rdi)
	jne	.LBB19_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB19_2:                               # %_ZN16COpenCallbackImp7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end19:
	.size	_ZThn8_N16COpenCallbackImp7ReleaseEv, .Lfunc_end19-_ZThn8_N16COpenCallbackImp7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N16COpenCallbackImpD1Ev,"axG",@progbits,_ZThn8_N16COpenCallbackImpD1Ev,comdat
	.weak	_ZThn8_N16COpenCallbackImpD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N16COpenCallbackImpD1Ev,@function
_ZThn8_N16COpenCallbackImpD1Ev:         # @_ZThn8_N16COpenCallbackImpD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN16COpenCallbackImpD2Ev # TAILCALL
.Lfunc_end20:
	.size	_ZThn8_N16COpenCallbackImpD1Ev, .Lfunc_end20-_ZThn8_N16COpenCallbackImpD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N16COpenCallbackImpD0Ev,"axG",@progbits,_ZThn8_N16COpenCallbackImpD0Ev,comdat
	.weak	_ZThn8_N16COpenCallbackImpD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N16COpenCallbackImpD0Ev,@function
_ZThn8_N16COpenCallbackImpD0Ev:         # @_ZThn8_N16COpenCallbackImpD0Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi81:
	.cfi_def_cfa_offset 32
.Lcfi82:
	.cfi_offset %rbx, -24
.Lcfi83:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp137:
	movq	%rbx, %rdi
	callq	_ZN16COpenCallbackImpD2Ev
.Ltmp138:
# BB#1:                                 # %_ZN16COpenCallbackImpD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB21_2:
.Ltmp139:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end21:
	.size	_ZThn8_N16COpenCallbackImpD0Ev, .Lfunc_end21-_ZThn8_N16COpenCallbackImpD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp137-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp138-.Ltmp137       #   Call between .Ltmp137 and .Ltmp138
	.long	.Ltmp139-.Lfunc_begin8  #     jumps to .Ltmp139
	.byte	0                       #   On action: cleanup
	.long	.Ltmp138-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Lfunc_end21-.Ltmp138   #   Call between .Ltmp138 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn16_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN16COpenCallbackImp14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end22:
	.size	_ZThn16_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv, .Lfunc_end22-_ZThn16_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N16COpenCallbackImp6AddRefEv,"axG",@progbits,_ZThn16_N16COpenCallbackImp6AddRefEv,comdat
	.weak	_ZThn16_N16COpenCallbackImp6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N16COpenCallbackImp6AddRefEv,@function
_ZThn16_N16COpenCallbackImp6AddRefEv:   # @_ZThn16_N16COpenCallbackImp6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end23:
	.size	_ZThn16_N16COpenCallbackImp6AddRefEv, .Lfunc_end23-_ZThn16_N16COpenCallbackImp6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N16COpenCallbackImp7ReleaseEv,"axG",@progbits,_ZThn16_N16COpenCallbackImp7ReleaseEv,comdat
	.weak	_ZThn16_N16COpenCallbackImp7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N16COpenCallbackImp7ReleaseEv,@function
_ZThn16_N16COpenCallbackImp7ReleaseEv:  # @_ZThn16_N16COpenCallbackImp7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi84:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB24_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB24_2:                               # %_ZN16COpenCallbackImp7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end24:
	.size	_ZThn16_N16COpenCallbackImp7ReleaseEv, .Lfunc_end24-_ZThn16_N16COpenCallbackImp7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N16COpenCallbackImpD1Ev,"axG",@progbits,_ZThn16_N16COpenCallbackImpD1Ev,comdat
	.weak	_ZThn16_N16COpenCallbackImpD1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N16COpenCallbackImpD1Ev,@function
_ZThn16_N16COpenCallbackImpD1Ev:        # @_ZThn16_N16COpenCallbackImpD1Ev
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN16COpenCallbackImpD2Ev # TAILCALL
.Lfunc_end25:
	.size	_ZThn16_N16COpenCallbackImpD1Ev, .Lfunc_end25-_ZThn16_N16COpenCallbackImpD1Ev
	.cfi_endproc

	.section	.text._ZThn16_N16COpenCallbackImpD0Ev,"axG",@progbits,_ZThn16_N16COpenCallbackImpD0Ev,comdat
	.weak	_ZThn16_N16COpenCallbackImpD0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N16COpenCallbackImpD0Ev,@function
_ZThn16_N16COpenCallbackImpD0Ev:        # @_ZThn16_N16COpenCallbackImpD0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi87:
	.cfi_def_cfa_offset 32
.Lcfi88:
	.cfi_offset %rbx, -24
.Lcfi89:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-16, %rbx
.Ltmp140:
	movq	%rbx, %rdi
	callq	_ZN16COpenCallbackImpD2Ev
.Ltmp141:
# BB#1:                                 # %_ZN16COpenCallbackImpD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB26_2:
.Ltmp142:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end26:
	.size	_ZThn16_N16COpenCallbackImpD0Ev, .Lfunc_end26-_ZThn16_N16COpenCallbackImpD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp140-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp141-.Ltmp140       #   Call between .Ltmp140 and .Ltmp141
	.long	.Ltmp142-.Lfunc_begin9  #     jumps to .Ltmp142
	.byte	0                       #   On action: cleanup
	.long	.Ltmp141-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Lfunc_end26-.Ltmp141   #   Call between .Ltmp141 and .Lfunc_end26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn16_N16COpenCallbackImp17SetSubArchiveNameEPKw,"axG",@progbits,_ZThn16_N16COpenCallbackImp17SetSubArchiveNameEPKw,comdat
	.weak	_ZThn16_N16COpenCallbackImp17SetSubArchiveNameEPKw
	.p2align	4, 0x90
	.type	_ZThn16_N16COpenCallbackImp17SetSubArchiveNameEPKw,@function
_ZThn16_N16COpenCallbackImp17SetSubArchiveNameEPKw: # @_ZThn16_N16COpenCallbackImp17SetSubArchiveNameEPKw
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi92:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi93:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi94:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi95:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi96:
	.cfi_def_cfa_offset 64
.Lcfi97:
	.cfi_offset %rbx, -56
.Lcfi98:
	.cfi_offset %r12, -48
.Lcfi99:
	.cfi_offset %r13, -40
.Lcfi100:
	.cfi_offset %r14, -32
.Lcfi101:
	.cfi_offset %r15, -24
.Lcfi102:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movb	$1, 96(%r14)
	movl	$0, 112(%r14)
	movq	104(%r14), %rbx
	movl	$0, (%rbx)
	xorl	%ebp, %ebp
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB27_1:                               # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB27_1
# BB#2:                                 # %_Z11MyStringLenIwEiPKT_.exit.i.i
	movl	116(%r14), %r13d
	cmpl	%ebp, %r13d
	je	.LBB27_7
# BB#3:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB27_6
# BB#4:
	testl	%r13d, %r13d
	jle	.LBB27_6
# BB#5:                                 # %._crit_edge.thread.i.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	112(%r14), %rax
.LBB27_6:                               # %._crit_edge16.i.i.i
	movq	%r12, 104(%r14)
	movl	$0, (%r12,%rax,4)
	movl	%ebp, 116(%r14)
	movq	%r12, %rbx
.LBB27_7:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i.preheader
	decl	%ebp
	.p2align	4, 0x90
.LBB27_8:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15), %eax
	addq	$4, %r15
	movl	%eax, (%rbx)
	addq	$4, %rbx
	testl	%eax, %eax
	jne	.LBB27_8
# BB#9:                                 # %_ZN16COpenCallbackImp17SetSubArchiveNameEPKw.exit
	movl	%ebp, 112(%r14)
	movq	$0, 168(%r14)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end27:
	.size	_ZThn16_N16COpenCallbackImp17SetSubArchiveNameEPKw, .Lfunc_end27-_ZThn16_N16COpenCallbackImp17SetSubArchiveNameEPKw
	.cfi_endproc

	.section	.text._ZThn24_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn24_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn24_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn24_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv,@function
_ZThn24_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv: # @_ZThn24_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN16COpenCallbackImp14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end28:
	.size	_ZThn24_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv, .Lfunc_end28-_ZThn24_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn24_N16COpenCallbackImp6AddRefEv,"axG",@progbits,_ZThn24_N16COpenCallbackImp6AddRefEv,comdat
	.weak	_ZThn24_N16COpenCallbackImp6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn24_N16COpenCallbackImp6AddRefEv,@function
_ZThn24_N16COpenCallbackImp6AddRefEv:   # @_ZThn24_N16COpenCallbackImp6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end29:
	.size	_ZThn24_N16COpenCallbackImp6AddRefEv, .Lfunc_end29-_ZThn24_N16COpenCallbackImp6AddRefEv
	.cfi_endproc

	.section	.text._ZThn24_N16COpenCallbackImp7ReleaseEv,"axG",@progbits,_ZThn24_N16COpenCallbackImp7ReleaseEv,comdat
	.weak	_ZThn24_N16COpenCallbackImp7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn24_N16COpenCallbackImp7ReleaseEv,@function
_ZThn24_N16COpenCallbackImp7ReleaseEv:  # @_ZThn24_N16COpenCallbackImp7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi103:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB30_2
# BB#1:
	addq	$-24, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB30_2:                               # %_ZN16COpenCallbackImp7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end30:
	.size	_ZThn24_N16COpenCallbackImp7ReleaseEv, .Lfunc_end30-_ZThn24_N16COpenCallbackImp7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn24_N16COpenCallbackImpD1Ev,"axG",@progbits,_ZThn24_N16COpenCallbackImpD1Ev,comdat
	.weak	_ZThn24_N16COpenCallbackImpD1Ev
	.p2align	4, 0x90
	.type	_ZThn24_N16COpenCallbackImpD1Ev,@function
_ZThn24_N16COpenCallbackImpD1Ev:        # @_ZThn24_N16COpenCallbackImpD1Ev
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN16COpenCallbackImpD2Ev # TAILCALL
.Lfunc_end31:
	.size	_ZThn24_N16COpenCallbackImpD1Ev, .Lfunc_end31-_ZThn24_N16COpenCallbackImpD1Ev
	.cfi_endproc

	.section	.text._ZThn24_N16COpenCallbackImpD0Ev,"axG",@progbits,_ZThn24_N16COpenCallbackImpD0Ev,comdat
	.weak	_ZThn24_N16COpenCallbackImpD0Ev
	.p2align	4, 0x90
	.type	_ZThn24_N16COpenCallbackImpD0Ev,@function
_ZThn24_N16COpenCallbackImpD0Ev:        # @_ZThn24_N16COpenCallbackImpD0Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi106:
	.cfi_def_cfa_offset 32
.Lcfi107:
	.cfi_offset %rbx, -24
.Lcfi108:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-24, %rbx
.Ltmp143:
	movq	%rbx, %rdi
	callq	_ZN16COpenCallbackImpD2Ev
.Ltmp144:
# BB#1:                                 # %_ZN16COpenCallbackImpD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB32_2:
.Ltmp145:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end32:
	.size	_ZThn24_N16COpenCallbackImpD0Ev, .Lfunc_end32-_ZThn24_N16COpenCallbackImpD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp143-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp144-.Ltmp143       #   Call between .Ltmp143 and .Ltmp144
	.long	.Ltmp145-.Lfunc_begin10 #     jumps to .Ltmp145
	.byte	0                       #   On action: cleanup
	.long	.Ltmp144-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Lfunc_end32-.Ltmp144   #   Call between .Ltmp144 and .Lfunc_end32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CInFileStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN13CInFileStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN13CInFileStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN13CInFileStream14QueryInterfaceERK4GUIDPPv,@function
_ZN13CInFileStream14QueryInterfaceERK4GUIDPPv: # @_ZN13CInFileStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 32
.Lcfi112:
	.cfi_offset %rbx, -32
.Lcfi113:
	.cfi_offset %r14, -24
.Lcfi114:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB33_1
# BB#2:
	movl	$IID_IInStream, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB33_3
.LBB33_1:
	movq	%rbx, (%r14)
.LBB33_6:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB33_7:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB33_3:
	movl	$IID_IStreamGetSize, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB33_4
# BB#5:
	leaq	8(%rbx), %rax
	movq	%rax, (%r14)
	jmp	.LBB33_6
.LBB33_4:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB33_7
.Lfunc_end33:
	.size	_ZN13CInFileStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end33-_ZN13CInFileStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN13CInFileStream6AddRefEv,"axG",@progbits,_ZN13CInFileStream6AddRefEv,comdat
	.weak	_ZN13CInFileStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN13CInFileStream6AddRefEv,@function
_ZN13CInFileStream6AddRefEv:            # @_ZN13CInFileStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end34:
	.size	_ZN13CInFileStream6AddRefEv, .Lfunc_end34-_ZN13CInFileStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN13CInFileStream7ReleaseEv,"axG",@progbits,_ZN13CInFileStream7ReleaseEv,comdat
	.weak	_ZN13CInFileStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN13CInFileStream7ReleaseEv,@function
_ZN13CInFileStream7ReleaseEv:           # @_ZN13CInFileStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi115:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB35_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB35_2:
	popq	%rcx
	retq
.Lfunc_end35:
	.size	_ZN13CInFileStream7ReleaseEv, .Lfunc_end35-_ZN13CInFileStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN16CInFileStreamVolD2Ev,"axG",@progbits,_ZN16CInFileStreamVolD2Ev,comdat
	.weak	_ZN16CInFileStreamVolD2Ev
	.p2align	4, 0x90
	.type	_ZN16CInFileStreamVolD2Ev,@function
_ZN16CInFileStreamVolD2Ev:              # @_ZN16CInFileStreamVolD2Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%rbp
.Lcfi116:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi117:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi118:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi119:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi120:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi121:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi122:
	.cfi_def_cfa_offset 80
.Lcfi123:
	.cfi_offset %rbx, -56
.Lcfi124:
	.cfi_offset %r12, -48
.Lcfi125:
	.cfi_offset %r13, -40
.Lcfi126:
	.cfi_offset %r14, -32
.Lcfi127:
	.cfi_offset %r15, -24
.Lcfi128:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	$_ZTV16CInFileStreamVol+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTV16CInFileStreamVol+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r14)
	cmpq	$0, 1136(%r14)
	je	.LBB36_17
# BB#1:
	movq	1128(%r14), %rbp
	cmpl	$0, 148(%rbp)
	jle	.LBB36_15
# BB#2:                                 # %.lr.ph.i
	leaq	1112(%r14), %r13
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB36_3:                               # =>This Inner Loop Header: Depth=1
	movq	152(%rbp), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%r13), %rdi
	movq	(%rax), %rsi
.Ltmp146:
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp147:
# BB#4:                                 # %.noexc
                                        #   in Loop: Header=BB36_3 Depth=1
	testl	%eax, %eax
	je	.LBB36_6
# BB#5:                                 #   in Loop: Header=BB36_3 Depth=1
	incq	%rbx
	movslq	148(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB36_3
	jmp	.LBB36_15
.LBB36_6:                               # %_ZN16COpenCallbackImp8FindNameERK11CStringBaseIwE.exit
	testl	%ebx, %ebx
	js	.LBB36_15
# BB#7:
	movq	1128(%r14), %rcx
	leaq	136(%rcx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	148(%rcx), %eax
	subl	%ebx, %eax
	movl	$1, %ecx
	cmovlel	%eax, %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	testl	%ecx, %ecx
	jle	.LBB36_14
# BB#8:                                 # %.lr.ph.i9
	movslq	4(%rsp), %r12           # 4-byte Folded Reload
	movslq	%ebx, %r15
	shlq	$3, %r15
	.p2align	4, 0x90
.LBB36_9:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	152(%rax), %rax
	movq	(%rax,%r15), %rbp
	testq	%rbp, %rbp
	je	.LBB36_13
# BB#10:                                #   in Loop: Header=BB36_9 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB36_12
# BB#11:                                #   in Loop: Header=BB36_9 Depth=1
	callq	_ZdaPv
.LBB36_12:                              # %_ZN11CStringBaseIwED2Ev.exit.i
                                        #   in Loop: Header=BB36_9 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB36_13:                              #   in Loop: Header=BB36_9 Depth=1
	addq	$8, %r15
	decq	%r12
	jne	.LBB36_9
.LBB36_14:                              # %._crit_edge.i
.Ltmp149:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%ebx, %esi
	movl	4(%rsp), %edx           # 4-byte Reload
	callq	_ZN17CBaseRecordVector6DeleteEii
.Ltmp150:
.LBB36_15:                              # %_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii.exit
	movq	1136(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB36_17
# BB#16:
	movq	(%rdi), %rax
.Ltmp154:
	callq	*16(%rax)
.Ltmp155:
.LBB36_17:                              # %_ZN9CMyComPtrI20IArchiveOpenCallbackED2Ev.exit
	movq	1112(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB36_19
# BB#18:
	callq	_ZdaPv
.LBB36_19:                              # %_ZN11CStringBaseIwED2Ev.exit
	movl	$_ZTV13CInFileStream+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTV13CInFileStream+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r14)
	addq	$24, %r14
	movq	%r14, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev # TAILCALL
.LBB36_29:
.Ltmp151:
	jmp	.LBB36_22
.LBB36_20:
.Ltmp156:
	movq	%rax, %rbx
	leaq	1112(%r14), %r13
	jmp	.LBB36_24
.LBB36_21:
.Ltmp148:
.LBB36_22:
	movq	%rax, %rbx
	movq	1136(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB36_24
# BB#23:
	movq	(%rdi), %rax
.Ltmp152:
	callq	*16(%rax)
.Ltmp153:
.LBB36_24:                              # %_ZN9CMyComPtrI20IArchiveOpenCallbackED2Ev.exit15
	movq	(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB36_26
# BB#25:
	callq	_ZdaPv
.LBB36_26:                              # %_ZN11CStringBaseIwED2Ev.exit16
	movl	$_ZTV13CInFileStream+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTV13CInFileStream+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r14)
	addq	$24, %r14
.Ltmp157:
	movq	%r14, %rdi
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp158:
# BB#27:                                # %_ZN13CInFileStreamD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB36_28:
.Ltmp159:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end36:
	.size	_ZN16CInFileStreamVolD2Ev, .Lfunc_end36-_ZN16CInFileStreamVolD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table36:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp146-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp147-.Ltmp146       #   Call between .Ltmp146 and .Ltmp147
	.long	.Ltmp148-.Lfunc_begin11 #     jumps to .Ltmp148
	.byte	0                       #   On action: cleanup
	.long	.Ltmp149-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp150-.Ltmp149       #   Call between .Ltmp149 and .Ltmp150
	.long	.Ltmp151-.Lfunc_begin11 #     jumps to .Ltmp151
	.byte	0                       #   On action: cleanup
	.long	.Ltmp154-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp155-.Ltmp154       #   Call between .Ltmp154 and .Ltmp155
	.long	.Ltmp156-.Lfunc_begin11 #     jumps to .Ltmp156
	.byte	0                       #   On action: cleanup
	.long	.Ltmp155-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Ltmp152-.Ltmp155       #   Call between .Ltmp155 and .Ltmp152
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp152-.Lfunc_begin11 # >> Call Site 5 <<
	.long	.Ltmp158-.Ltmp152       #   Call between .Ltmp152 and .Ltmp158
	.long	.Ltmp159-.Lfunc_begin11 #     jumps to .Ltmp159
	.byte	1                       #   On action: 1
	.long	.Ltmp158-.Lfunc_begin11 # >> Call Site 6 <<
	.long	.Lfunc_end36-.Ltmp158   #   Call between .Ltmp158 and .Lfunc_end36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN16CInFileStreamVolD0Ev,"axG",@progbits,_ZN16CInFileStreamVolD0Ev,comdat
	.weak	_ZN16CInFileStreamVolD0Ev
	.p2align	4, 0x90
	.type	_ZN16CInFileStreamVolD0Ev,@function
_ZN16CInFileStreamVolD0Ev:              # @_ZN16CInFileStreamVolD0Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r14
.Lcfi129:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi130:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi131:
	.cfi_def_cfa_offset 32
.Lcfi132:
	.cfi_offset %rbx, -24
.Lcfi133:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp160:
	callq	_ZN16CInFileStreamVolD2Ev
.Ltmp161:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB37_2:
.Ltmp162:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end37:
	.size	_ZN16CInFileStreamVolD0Ev, .Lfunc_end37-_ZN16CInFileStreamVolD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table37:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp160-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp161-.Ltmp160       #   Call between .Ltmp160 and .Ltmp161
	.long	.Ltmp162-.Lfunc_begin12 #     jumps to .Ltmp162
	.byte	0                       #   On action: cleanup
	.long	.Ltmp161-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Lfunc_end37-.Ltmp161   #   Call between .Ltmp161 and .Lfunc_end37
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N13CInFileStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N13CInFileStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N13CInFileStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N13CInFileStream14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N13CInFileStream14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N13CInFileStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi134:
	.cfi_def_cfa_offset 16
	addq	$-8, %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB38_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB38_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB38_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB38_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB38_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB38_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB38_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB38_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB38_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB38_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB38_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB38_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB38_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB38_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB38_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB38_16
.LBB38_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_IInStream(%rip), %cl
	jne	.LBB38_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_IInStream+1(%rip), %al
	jne	.LBB38_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_IInStream+2(%rip), %al
	jne	.LBB38_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_IInStream+3(%rip), %al
	jne	.LBB38_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_IInStream+4(%rip), %al
	jne	.LBB38_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_IInStream+5(%rip), %al
	jne	.LBB38_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_IInStream+6(%rip), %al
	jne	.LBB38_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_IInStream+7(%rip), %al
	jne	.LBB38_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_IInStream+8(%rip), %al
	jne	.LBB38_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_IInStream+9(%rip), %al
	jne	.LBB38_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_IInStream+10(%rip), %al
	jne	.LBB38_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_IInStream+11(%rip), %al
	jne	.LBB38_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_IInStream+12(%rip), %al
	jne	.LBB38_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_IInStream+13(%rip), %al
	jne	.LBB38_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_IInStream+14(%rip), %al
	jne	.LBB38_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit6
	movb	15(%rsi), %al
	cmpb	IID_IInStream+15(%rip), %al
	jne	.LBB38_33
.LBB38_16:
	movq	%rdi, (%rdx)
	jmp	.LBB38_50
.LBB38_33:                              # %_ZeqRK4GUIDS1_.exit6.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IStreamGetSize(%rip), %cl
	jne	.LBB38_51
# BB#34:
	movb	1(%rsi), %cl
	cmpb	IID_IStreamGetSize+1(%rip), %cl
	jne	.LBB38_51
# BB#35:
	movb	2(%rsi), %cl
	cmpb	IID_IStreamGetSize+2(%rip), %cl
	jne	.LBB38_51
# BB#36:
	movb	3(%rsi), %cl
	cmpb	IID_IStreamGetSize+3(%rip), %cl
	jne	.LBB38_51
# BB#37:
	movb	4(%rsi), %cl
	cmpb	IID_IStreamGetSize+4(%rip), %cl
	jne	.LBB38_51
# BB#38:
	movb	5(%rsi), %cl
	cmpb	IID_IStreamGetSize+5(%rip), %cl
	jne	.LBB38_51
# BB#39:
	movb	6(%rsi), %cl
	cmpb	IID_IStreamGetSize+6(%rip), %cl
	jne	.LBB38_51
# BB#40:
	movb	7(%rsi), %cl
	cmpb	IID_IStreamGetSize+7(%rip), %cl
	jne	.LBB38_51
# BB#41:
	movb	8(%rsi), %cl
	cmpb	IID_IStreamGetSize+8(%rip), %cl
	jne	.LBB38_51
# BB#42:
	movb	9(%rsi), %cl
	cmpb	IID_IStreamGetSize+9(%rip), %cl
	jne	.LBB38_51
# BB#43:
	movb	10(%rsi), %cl
	cmpb	IID_IStreamGetSize+10(%rip), %cl
	jne	.LBB38_51
# BB#44:
	movb	11(%rsi), %cl
	cmpb	IID_IStreamGetSize+11(%rip), %cl
	jne	.LBB38_51
# BB#45:
	movb	12(%rsi), %cl
	cmpb	IID_IStreamGetSize+12(%rip), %cl
	jne	.LBB38_51
# BB#46:
	movb	13(%rsi), %cl
	cmpb	IID_IStreamGetSize+13(%rip), %cl
	jne	.LBB38_51
# BB#47:
	movb	14(%rsi), %cl
	cmpb	IID_IStreamGetSize+14(%rip), %cl
	jne	.LBB38_51
# BB#48:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_IStreamGetSize+15(%rip), %cl
	jne	.LBB38_51
# BB#49:
	leaq	8(%rdi), %rax
	movq	%rax, (%rdx)
.LBB38_50:                              # %_ZN13CInFileStream14QueryInterfaceERK4GUIDPPv.exit
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB38_51:                              # %_ZN13CInFileStream14QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end38:
	.size	_ZThn8_N13CInFileStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end38-_ZThn8_N13CInFileStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N13CInFileStream6AddRefEv,"axG",@progbits,_ZThn8_N13CInFileStream6AddRefEv,comdat
	.weak	_ZThn8_N13CInFileStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N13CInFileStream6AddRefEv,@function
_ZThn8_N13CInFileStream6AddRefEv:       # @_ZThn8_N13CInFileStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end39:
	.size	_ZThn8_N13CInFileStream6AddRefEv, .Lfunc_end39-_ZThn8_N13CInFileStream6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N13CInFileStream7ReleaseEv,"axG",@progbits,_ZThn8_N13CInFileStream7ReleaseEv,comdat
	.weak	_ZThn8_N13CInFileStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N13CInFileStream7ReleaseEv,@function
_ZThn8_N13CInFileStream7ReleaseEv:      # @_ZThn8_N13CInFileStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi135:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB40_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB40_2:                               # %_ZN13CInFileStream7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end40:
	.size	_ZThn8_N13CInFileStream7ReleaseEv, .Lfunc_end40-_ZThn8_N13CInFileStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N16CInFileStreamVolD1Ev,"axG",@progbits,_ZThn8_N16CInFileStreamVolD1Ev,comdat
	.weak	_ZThn8_N16CInFileStreamVolD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N16CInFileStreamVolD1Ev,@function
_ZThn8_N16CInFileStreamVolD1Ev:         # @_ZThn8_N16CInFileStreamVolD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN16CInFileStreamVolD2Ev # TAILCALL
.Lfunc_end41:
	.size	_ZThn8_N16CInFileStreamVolD1Ev, .Lfunc_end41-_ZThn8_N16CInFileStreamVolD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N16CInFileStreamVolD0Ev,"axG",@progbits,_ZThn8_N16CInFileStreamVolD0Ev,comdat
	.weak	_ZThn8_N16CInFileStreamVolD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N16CInFileStreamVolD0Ev,@function
_ZThn8_N16CInFileStreamVolD0Ev:         # @_ZThn8_N16CInFileStreamVolD0Ev
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r14
.Lcfi136:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi137:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi138:
	.cfi_def_cfa_offset 32
.Lcfi139:
	.cfi_offset %rbx, -24
.Lcfi140:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp163:
	movq	%rbx, %rdi
	callq	_ZN16CInFileStreamVolD2Ev
.Ltmp164:
# BB#1:                                 # %_ZN16CInFileStreamVolD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB42_2:
.Ltmp165:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end42:
	.size	_ZThn8_N16CInFileStreamVolD0Ev, .Lfunc_end42-_ZThn8_N16CInFileStreamVolD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table42:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp163-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp164-.Ltmp163       #   Call between .Ltmp163 and .Ltmp164
	.long	.Ltmp165-.Lfunc_begin13 #     jumps to .Ltmp165
	.byte	0                       #   On action: cleanup
	.long	.Ltmp164-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Lfunc_end42-.Ltmp164   #   Call between .Ltmp164 and .Lfunc_end42
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NWindows5NFile3NIO7CInFileD0Ev,"axG",@progbits,_ZN8NWindows5NFile3NIO7CInFileD0Ev,comdat
	.weak	_ZN8NWindows5NFile3NIO7CInFileD0Ev
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO7CInFileD0Ev,@function
_ZN8NWindows5NFile3NIO7CInFileD0Ev:     # @_ZN8NWindows5NFile3NIO7CInFileD0Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi141:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi142:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi143:
	.cfi_def_cfa_offset 32
.Lcfi144:
	.cfi_offset %rbx, -24
.Lcfi145:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp166:
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp167:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB43_2:
.Ltmp168:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end43:
	.size	_ZN8NWindows5NFile3NIO7CInFileD0Ev, .Lfunc_end43-_ZN8NWindows5NFile3NIO7CInFileD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table43:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp166-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp167-.Ltmp166       #   Call between .Ltmp166 and .Ltmp167
	.long	.Ltmp168-.Lfunc_begin14 #     jumps to .Ltmp168
	.byte	0                       #   On action: cleanup
	.long	.Ltmp167-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Lfunc_end43-.Ltmp167   #   Call between .Ltmp167 and .Lfunc_end43
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZeqRK4GUIDS1_,"axG",@progbits,_ZeqRK4GUIDS1_,comdat
	.weak	_ZeqRK4GUIDS1_
	.p2align	4, 0x90
	.type	_ZeqRK4GUIDS1_,@function
_ZeqRK4GUIDS1_:                         # @_ZeqRK4GUIDS1_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	cmpb	(%rsi), %al
	jne	.LBB44_16
# BB#1:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	jne	.LBB44_16
# BB#2:
	movb	2(%rdi), %al
	cmpb	2(%rsi), %al
	jne	.LBB44_16
# BB#3:
	movb	3(%rdi), %al
	cmpb	3(%rsi), %al
	jne	.LBB44_16
# BB#4:
	movb	4(%rdi), %al
	cmpb	4(%rsi), %al
	jne	.LBB44_16
# BB#5:
	movb	5(%rdi), %al
	cmpb	5(%rsi), %al
	jne	.LBB44_16
# BB#6:
	movb	6(%rdi), %al
	cmpb	6(%rsi), %al
	jne	.LBB44_16
# BB#7:
	movb	7(%rdi), %al
	cmpb	7(%rsi), %al
	jne	.LBB44_16
# BB#8:
	movb	8(%rdi), %al
	cmpb	8(%rsi), %al
	jne	.LBB44_16
# BB#9:
	movb	9(%rdi), %al
	cmpb	9(%rsi), %al
	jne	.LBB44_16
# BB#10:
	movb	10(%rdi), %al
	cmpb	10(%rsi), %al
	jne	.LBB44_16
# BB#11:
	movb	11(%rdi), %al
	cmpb	11(%rsi), %al
	jne	.LBB44_16
# BB#12:
	movb	12(%rdi), %al
	cmpb	12(%rsi), %al
	jne	.LBB44_16
# BB#13:
	movb	13(%rdi), %al
	cmpb	13(%rsi), %al
	jne	.LBB44_16
# BB#14:
	movb	14(%rdi), %al
	cmpb	14(%rsi), %al
	jne	.LBB44_16
# BB#15:
	movb	15(%rdi), %cl
	xorl	%eax, %eax
	cmpb	15(%rsi), %cl
	sete	%al
	retq
.LBB44_16:
	xorl	%eax, %eax
	retq
.Lfunc_end44:
	.size	_ZeqRK4GUIDS1_, .Lfunc_end44-_ZeqRK4GUIDS1_
	.cfi_endproc

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,@function
_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii: # @_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi146:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi147:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi148:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi149:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi150:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi151:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi152:
	.cfi_def_cfa_offset 64
.Lcfi153:
	.cfi_offset %rbx, -56
.Lcfi154:
	.cfi_offset %r12, -48
.Lcfi155:
	.cfi_offset %r13, -40
.Lcfi156:
	.cfi_offset %r14, -32
.Lcfi157:
	.cfi_offset %r15, -24
.Lcfi158:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB45_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB45_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB45_6
# BB#3:                                 #   in Loop: Header=BB45_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB45_5
# BB#4:                                 #   in Loop: Header=BB45_2 Depth=1
	callq	_ZdaPv
.LBB45_5:                               # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB45_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB45_6:                               #   in Loop: Header=BB45_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB45_2
.LBB45_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end45:
	.size	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii, .Lfunc_end45-_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED2Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED2Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED2Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED2Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED2Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi159:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi160:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi161:
	.cfi_def_cfa_offset 32
.Lcfi162:
	.cfi_offset %rbx, -24
.Lcfi163:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp169:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp170:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB46_2:
.Ltmp171:
	movq	%rax, %r14
.Ltmp172:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp173:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB46_4:
.Ltmp174:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end46:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED2Ev, .Lfunc_end46-_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table46:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp169-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp170-.Ltmp169       #   Call between .Ltmp169 and .Ltmp170
	.long	.Ltmp171-.Lfunc_begin15 #     jumps to .Ltmp171
	.byte	0                       #   On action: cleanup
	.long	.Ltmp170-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Ltmp172-.Ltmp170       #   Call between .Ltmp170 and .Ltmp172
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp172-.Lfunc_begin15 # >> Call Site 3 <<
	.long	.Ltmp173-.Ltmp172       #   Call between .Ltmp172 and .Ltmp173
	.long	.Ltmp174-.Lfunc_begin15 #     jumps to .Ltmp174
	.byte	1                       #   On action: 1
	.long	.Ltmp173-.Lfunc_begin15 # >> Call Site 4 <<
	.long	.Lfunc_end46-.Ltmp173   #   Call between .Ltmp173 and .Lfunc_end46
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED0Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED0Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED0Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED0Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED0Ev
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%r14
.Lcfi164:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi166:
	.cfi_def_cfa_offset 32
.Lcfi167:
	.cfi_offset %rbx, -24
.Lcfi168:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp175:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp176:
# BB#1:
.Ltmp181:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp182:
# BB#2:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB47_5:
.Ltmp183:
	movq	%rax, %r14
	jmp	.LBB47_6
.LBB47_3:
.Ltmp177:
	movq	%rax, %r14
.Ltmp178:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp179:
.LBB47_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB47_4:
.Ltmp180:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end47:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED0Ev, .Lfunc_end47-_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table47:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp175-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp176-.Ltmp175       #   Call between .Ltmp175 and .Ltmp176
	.long	.Ltmp177-.Lfunc_begin16 #     jumps to .Ltmp177
	.byte	0                       #   On action: cleanup
	.long	.Ltmp181-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Ltmp182-.Ltmp181       #   Call between .Ltmp181 and .Ltmp182
	.long	.Ltmp183-.Lfunc_begin16 #     jumps to .Ltmp183
	.byte	0                       #   On action: cleanup
	.long	.Ltmp178-.Lfunc_begin16 # >> Call Site 3 <<
	.long	.Ltmp179-.Ltmp178       #   Call between .Ltmp178 and .Ltmp179
	.long	.Ltmp180-.Lfunc_begin16 #     jumps to .Ltmp180
	.byte	1                       #   On action: 1
	.long	.Ltmp179-.Lfunc_begin16 # >> Call Site 4 <<
	.long	.Lfunc_end47-.Ltmp179   #   Call between .Ltmp179 and .Lfunc_end47
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN11CStringBaseIwE10GrowLengthEi,"axG",@progbits,_ZN11CStringBaseIwE10GrowLengthEi,comdat
	.weak	_ZN11CStringBaseIwE10GrowLengthEi
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwE10GrowLengthEi,@function
_ZN11CStringBaseIwE10GrowLengthEi:      # @_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi169:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi170:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi171:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi172:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi173:
	.cfi_def_cfa_offset 48
.Lcfi174:
	.cfi_offset %rbx, -48
.Lcfi175:
	.cfi_offset %r12, -40
.Lcfi176:
	.cfi_offset %r14, -32
.Lcfi177:
	.cfi_offset %r15, -24
.Lcfi178:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	8(%r14), %ebp
	movl	12(%r14), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	%esi, %eax
	jg	.LBB48_28
# BB#1:
	decl	%eax
	cmpl	$8, %ebx
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebx
	jl	.LBB48_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %ecx
	shrl	$31, %ecx
	addl	%ebx, %ecx
	sarl	%ecx
.LBB48_3:                               # %select.end
	movl	%esi, %edx
	subl	%eax, %edx
	addl	%ecx, %eax
	cmpl	%esi, %eax
	cmovgel	%ecx, %edx
	leal	1(%rbx,%rdx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB48_28
# BB#4:
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB48_27
# BB#5:                                 # %.preheader.i
	movq	(%r14), %rdi
	testl	%ebp, %ebp
	jle	.LBB48_25
# BB#6:                                 # %.lr.ph.i
	movslq	%ebp, %rax
	cmpl	$7, %ebp
	jbe	.LBB48_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB48_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r12
	jae	.LBB48_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB48_17
.LBB48_7:
	xorl	%ecx, %ecx
.LBB48_8:                               # %scalar.ph.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB48_11
# BB#9:                                 # %scalar.ph.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB48_10:                              # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %esi
	movl	%esi, (%r12,%rcx,4)
	incq	%rcx
	incq	%rbp
	jne	.LBB48_10
.LBB48_11:                              # %scalar.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB48_26
# BB#12:                                # %scalar.ph.preheader.new
	subq	%rcx, %rax
	leaq	28(%r12,%rcx,4), %rdx
	leaq	28(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB48_13:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB48_13
	jmp	.LBB48_26
.LBB48_25:                              # %._crit_edge.i
	testq	%rdi, %rdi
	je	.LBB48_27
.LBB48_26:                              # %._crit_edge.thread.i
	callq	_ZdaPv
	movl	8(%r14), %ebp
.LBB48_27:                              # %._crit_edge16.i
	movq	%r12, (%r14)
	movslq	%ebp, %rax
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%r14)
.LBB48_28:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB48_17:                              # %vector.body.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB48_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB48_20:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r12,%rbx,4)
	movups	%xmm1, 16(%r12,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB48_20
	jmp	.LBB48_21
.LBB48_18:
	xorl	%ebx, %ebx
.LBB48_21:                              # %vector.body.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB48_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx,4), %rsi
	leaq	112(%rdi,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB48_23:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-32, %rdx
	jne	.LBB48_23
.LBB48_24:                              # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB48_8
	jmp	.LBB48_26
.Lfunc_end48:
	.size	_ZN11CStringBaseIwE10GrowLengthEi, .Lfunc_end48-_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_endproc

	.type	_ZTV16COpenCallbackImp,@object # @_ZTV16COpenCallbackImp
	.section	.rodata,"a",@progbits
	.globl	_ZTV16COpenCallbackImp
	.p2align	3
_ZTV16COpenCallbackImp:
	.quad	0
	.quad	_ZTI16COpenCallbackImp
	.quad	_ZN16COpenCallbackImp14QueryInterfaceERK4GUIDPPv
	.quad	_ZN16COpenCallbackImp6AddRefEv
	.quad	_ZN16COpenCallbackImp7ReleaseEv
	.quad	_ZN16COpenCallbackImpD2Ev
	.quad	_ZN16COpenCallbackImpD0Ev
	.quad	_ZN16COpenCallbackImp8SetTotalEPKyS1_
	.quad	_ZN16COpenCallbackImp12SetCompletedEPKyS1_
	.quad	_ZN16COpenCallbackImp11GetPropertyEjP14tagPROPVARIANT
	.quad	_ZN16COpenCallbackImp9GetStreamEPKwPP9IInStream
	.quad	_ZN16COpenCallbackImp21CryptoGetTextPasswordEPPw
	.quad	_ZN16COpenCallbackImp17SetSubArchiveNameEPKw
	.quad	-8
	.quad	_ZTI16COpenCallbackImp
	.quad	_ZThn8_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N16COpenCallbackImp6AddRefEv
	.quad	_ZThn8_N16COpenCallbackImp7ReleaseEv
	.quad	_ZThn8_N16COpenCallbackImpD1Ev
	.quad	_ZThn8_N16COpenCallbackImpD0Ev
	.quad	_ZThn8_N16COpenCallbackImp11GetPropertyEjP14tagPROPVARIANT
	.quad	_ZThn8_N16COpenCallbackImp9GetStreamEPKwPP9IInStream
	.quad	-16
	.quad	_ZTI16COpenCallbackImp
	.quad	_ZThn16_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N16COpenCallbackImp6AddRefEv
	.quad	_ZThn16_N16COpenCallbackImp7ReleaseEv
	.quad	_ZThn16_N16COpenCallbackImpD1Ev
	.quad	_ZThn16_N16COpenCallbackImpD0Ev
	.quad	_ZThn16_N16COpenCallbackImp17SetSubArchiveNameEPKw
	.quad	-24
	.quad	_ZTI16COpenCallbackImp
	.quad	_ZThn24_N16COpenCallbackImp14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn24_N16COpenCallbackImp6AddRefEv
	.quad	_ZThn24_N16COpenCallbackImp7ReleaseEv
	.quad	_ZThn24_N16COpenCallbackImpD1Ev
	.quad	_ZThn24_N16COpenCallbackImpD0Ev
	.quad	_ZThn24_N16COpenCallbackImp21CryptoGetTextPasswordEPPw
	.size	_ZTV16COpenCallbackImp, 304

	.type	_ZTS16COpenCallbackImp,@object # @_ZTS16COpenCallbackImp
	.globl	_ZTS16COpenCallbackImp
	.p2align	4
_ZTS16COpenCallbackImp:
	.asciz	"16COpenCallbackImp"
	.size	_ZTS16COpenCallbackImp, 19

	.type	_ZTS20IArchiveOpenCallback,@object # @_ZTS20IArchiveOpenCallback
	.section	.rodata._ZTS20IArchiveOpenCallback,"aG",@progbits,_ZTS20IArchiveOpenCallback,comdat
	.weak	_ZTS20IArchiveOpenCallback
	.p2align	4
_ZTS20IArchiveOpenCallback:
	.asciz	"20IArchiveOpenCallback"
	.size	_ZTS20IArchiveOpenCallback, 23

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI20IArchiveOpenCallback,@object # @_ZTI20IArchiveOpenCallback
	.section	.rodata._ZTI20IArchiveOpenCallback,"aG",@progbits,_ZTI20IArchiveOpenCallback,comdat
	.weak	_ZTI20IArchiveOpenCallback
	.p2align	4
_ZTI20IArchiveOpenCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20IArchiveOpenCallback
	.quad	_ZTI8IUnknown
	.size	_ZTI20IArchiveOpenCallback, 24

	.type	_ZTS26IArchiveOpenVolumeCallback,@object # @_ZTS26IArchiveOpenVolumeCallback
	.section	.rodata._ZTS26IArchiveOpenVolumeCallback,"aG",@progbits,_ZTS26IArchiveOpenVolumeCallback,comdat
	.weak	_ZTS26IArchiveOpenVolumeCallback
	.p2align	4
_ZTS26IArchiveOpenVolumeCallback:
	.asciz	"26IArchiveOpenVolumeCallback"
	.size	_ZTS26IArchiveOpenVolumeCallback, 29

	.type	_ZTI26IArchiveOpenVolumeCallback,@object # @_ZTI26IArchiveOpenVolumeCallback
	.section	.rodata._ZTI26IArchiveOpenVolumeCallback,"aG",@progbits,_ZTI26IArchiveOpenVolumeCallback,comdat
	.weak	_ZTI26IArchiveOpenVolumeCallback
	.p2align	4
_ZTI26IArchiveOpenVolumeCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS26IArchiveOpenVolumeCallback
	.quad	_ZTI8IUnknown
	.size	_ZTI26IArchiveOpenVolumeCallback, 24

	.type	_ZTS29IArchiveOpenSetSubArchiveName,@object # @_ZTS29IArchiveOpenSetSubArchiveName
	.section	.rodata._ZTS29IArchiveOpenSetSubArchiveName,"aG",@progbits,_ZTS29IArchiveOpenSetSubArchiveName,comdat
	.weak	_ZTS29IArchiveOpenSetSubArchiveName
	.p2align	4
_ZTS29IArchiveOpenSetSubArchiveName:
	.asciz	"29IArchiveOpenSetSubArchiveName"
	.size	_ZTS29IArchiveOpenSetSubArchiveName, 32

	.type	_ZTI29IArchiveOpenSetSubArchiveName,@object # @_ZTI29IArchiveOpenSetSubArchiveName
	.section	.rodata._ZTI29IArchiveOpenSetSubArchiveName,"aG",@progbits,_ZTI29IArchiveOpenSetSubArchiveName,comdat
	.weak	_ZTI29IArchiveOpenSetSubArchiveName
	.p2align	4
_ZTI29IArchiveOpenSetSubArchiveName:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS29IArchiveOpenSetSubArchiveName
	.quad	_ZTI8IUnknown
	.size	_ZTI29IArchiveOpenSetSubArchiveName, 24

	.type	_ZTS22ICryptoGetTextPassword,@object # @_ZTS22ICryptoGetTextPassword
	.section	.rodata._ZTS22ICryptoGetTextPassword,"aG",@progbits,_ZTS22ICryptoGetTextPassword,comdat
	.weak	_ZTS22ICryptoGetTextPassword
	.p2align	4
_ZTS22ICryptoGetTextPassword:
	.asciz	"22ICryptoGetTextPassword"
	.size	_ZTS22ICryptoGetTextPassword, 25

	.type	_ZTI22ICryptoGetTextPassword,@object # @_ZTI22ICryptoGetTextPassword
	.section	.rodata._ZTI22ICryptoGetTextPassword,"aG",@progbits,_ZTI22ICryptoGetTextPassword,comdat
	.weak	_ZTI22ICryptoGetTextPassword
	.p2align	4
_ZTI22ICryptoGetTextPassword:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS22ICryptoGetTextPassword
	.quad	_ZTI8IUnknown
	.size	_ZTI22ICryptoGetTextPassword, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI16COpenCallbackImp,@object # @_ZTI16COpenCallbackImp
	.section	.rodata,"a",@progbits
	.globl	_ZTI16COpenCallbackImp
	.p2align	4
_ZTI16COpenCallbackImp:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS16COpenCallbackImp
	.long	1                       # 0x1
	.long	5                       # 0x5
	.quad	_ZTI20IArchiveOpenCallback
	.quad	2                       # 0x2
	.quad	_ZTI26IArchiveOpenVolumeCallback
	.quad	2050                    # 0x802
	.quad	_ZTI29IArchiveOpenSetSubArchiveName
	.quad	4098                    # 0x1002
	.quad	_ZTI22ICryptoGetTextPassword
	.quad	6146                    # 0x1802
	.quad	_ZTI13CMyUnknownImp
	.quad	8194                    # 0x2002
	.size	_ZTI16COpenCallbackImp, 104

	.type	_ZTV16CInFileStreamVol,@object # @_ZTV16CInFileStreamVol
	.section	.rodata._ZTV16CInFileStreamVol,"aG",@progbits,_ZTV16CInFileStreamVol,comdat
	.weak	_ZTV16CInFileStreamVol
	.p2align	3
_ZTV16CInFileStreamVol:
	.quad	0
	.quad	_ZTI16CInFileStreamVol
	.quad	_ZN13CInFileStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN13CInFileStream6AddRefEv
	.quad	_ZN13CInFileStream7ReleaseEv
	.quad	_ZN16CInFileStreamVolD2Ev
	.quad	_ZN16CInFileStreamVolD0Ev
	.quad	_ZN13CInFileStream4ReadEPvjPj
	.quad	_ZN13CInFileStream4SeekExjPy
	.quad	_ZN13CInFileStream7GetSizeEPy
	.quad	-8
	.quad	_ZTI16CInFileStreamVol
	.quad	_ZThn8_N13CInFileStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N13CInFileStream6AddRefEv
	.quad	_ZThn8_N13CInFileStream7ReleaseEv
	.quad	_ZThn8_N16CInFileStreamVolD1Ev
	.quad	_ZThn8_N16CInFileStreamVolD0Ev
	.quad	_ZThn8_N13CInFileStream7GetSizeEPy
	.size	_ZTV16CInFileStreamVol, 144

	.type	_ZTS16CInFileStreamVol,@object # @_ZTS16CInFileStreamVol
	.section	.rodata._ZTS16CInFileStreamVol,"aG",@progbits,_ZTS16CInFileStreamVol,comdat
	.weak	_ZTS16CInFileStreamVol
	.p2align	4
_ZTS16CInFileStreamVol:
	.asciz	"16CInFileStreamVol"
	.size	_ZTS16CInFileStreamVol, 19

	.type	_ZTI16CInFileStreamVol,@object # @_ZTI16CInFileStreamVol
	.section	.rodata._ZTI16CInFileStreamVol,"aG",@progbits,_ZTI16CInFileStreamVol,comdat
	.weak	_ZTI16CInFileStreamVol
	.p2align	4
_ZTI16CInFileStreamVol:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS16CInFileStreamVol
	.quad	_ZTI13CInFileStream
	.size	_ZTI16CInFileStreamVol, 24

	.type	_ZTVN8NWindows5NFile3NIO7CInFileE,@object # @_ZTVN8NWindows5NFile3NIO7CInFileE
	.section	.rodata._ZTVN8NWindows5NFile3NIO7CInFileE,"aG",@progbits,_ZTVN8NWindows5NFile3NIO7CInFileE,comdat
	.weak	_ZTVN8NWindows5NFile3NIO7CInFileE
	.p2align	3
_ZTVN8NWindows5NFile3NIO7CInFileE:
	.quad	0
	.quad	_ZTIN8NWindows5NFile3NIO7CInFileE
	.quad	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
	.quad	_ZN8NWindows5NFile3NIO7CInFileD0Ev
	.quad	_ZN8NWindows5NFile3NIO9CFileBase5CloseEv
	.size	_ZTVN8NWindows5NFile3NIO7CInFileE, 40

	.type	_ZTSN8NWindows5NFile3NIO7CInFileE,@object # @_ZTSN8NWindows5NFile3NIO7CInFileE
	.section	.rodata._ZTSN8NWindows5NFile3NIO7CInFileE,"aG",@progbits,_ZTSN8NWindows5NFile3NIO7CInFileE,comdat
	.weak	_ZTSN8NWindows5NFile3NIO7CInFileE
	.p2align	4
_ZTSN8NWindows5NFile3NIO7CInFileE:
	.asciz	"N8NWindows5NFile3NIO7CInFileE"
	.size	_ZTSN8NWindows5NFile3NIO7CInFileE, 30

	.type	_ZTIN8NWindows5NFile3NIO7CInFileE,@object # @_ZTIN8NWindows5NFile3NIO7CInFileE
	.section	.rodata._ZTIN8NWindows5NFile3NIO7CInFileE,"aG",@progbits,_ZTIN8NWindows5NFile3NIO7CInFileE,comdat
	.weak	_ZTIN8NWindows5NFile3NIO7CInFileE
	.p2align	4
_ZTIN8NWindows5NFile3NIO7CInFileE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows5NFile3NIO7CInFileE
	.quad	_ZTIN8NWindows5NFile3NIO9CFileBaseE
	.size	_ZTIN8NWindows5NFile3NIO7CInFileE, 24

	.type	_ZTV13CObjectVectorI11CStringBaseIwEE,@object # @_ZTV13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTV13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTV13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTV13CObjectVectorI11CStringBaseIwEE
	.p2align	3
_ZTV13CObjectVectorI11CStringBaseIwEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI11CStringBaseIwEE
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.size	_ZTV13CObjectVectorI11CStringBaseIwEE, 40

	.type	_ZTS13CObjectVectorI11CStringBaseIwEE,@object # @_ZTS13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTS13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTS13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTS13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTS13CObjectVectorI11CStringBaseIwEE:
	.asciz	"13CObjectVectorI11CStringBaseIwEE"
	.size	_ZTS13CObjectVectorI11CStringBaseIwEE, 34

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorI11CStringBaseIwEE,@object # @_ZTI13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTI13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTI13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTI13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTI13CObjectVectorI11CStringBaseIwEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI11CStringBaseIwEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI11CStringBaseIwEE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
