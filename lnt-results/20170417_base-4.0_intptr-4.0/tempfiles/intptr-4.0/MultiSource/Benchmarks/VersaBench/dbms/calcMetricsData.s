	.text
	.file	"calcMetricsData.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	-4039728866278637763    # double -3.4028234699999998E+38
	.text
	.globl	calcMetricsData
	.p2align	4, 0x90
	.type	calcMetricsData,@function
calcMetricsData:                        # @calcMetricsData
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	getTime
	subq	(%rbx), %rax
	movq	%rax, (%rbx)
	movq	32(%rbx), %rax
	testq	%rax, %rax
	jle	.LBB0_4
# BB#1:
	movsd	40(%rbx), %xmm2         # xmm2 = mem[0],zero
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm2, %xmm0
	divsd	%xmm1, %xmm0
	movsd	%xmm0, 72(%rbx)
	movsd	48(%rbx), %xmm0         # xmm0 = mem[0],zero
	mulsd	%xmm2, %xmm2
	divsd	%xmm1, %xmm2
	subsd	%xmm2, %xmm0
	xorpd	%xmm2, %xmm2
	ucomisd	%xmm0, %xmm2
	jbe	.LBB0_2
.LBB0_4:
	movabsq	$-4039728866278637763, %rax # imm = 0xC7EFFFFFE091FF3D
	movq	%rax, 72(%rbx)
	movsd	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero
	jmp	.LBB0_5
.LBB0_2:
	divsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB0_5
# BB#3:                                 # %call.sqrt
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB0_5:
	movsd	%xmm1, 80(%rbx)
	movq	96(%rbx), %rax
	testq	%rax, %rax
	jle	.LBB0_9
# BB#6:
	movsd	104(%rbx), %xmm2        # xmm2 = mem[0],zero
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm2, %xmm0
	divsd	%xmm1, %xmm0
	movsd	%xmm0, 136(%rbx)
	movsd	112(%rbx), %xmm0        # xmm0 = mem[0],zero
	mulsd	%xmm2, %xmm2
	divsd	%xmm1, %xmm2
	subsd	%xmm2, %xmm0
	xorpd	%xmm2, %xmm2
	ucomisd	%xmm0, %xmm2
	jbe	.LBB0_7
.LBB0_9:
	movabsq	$-4039728866278637763, %rax # imm = 0xC7EFFFFFE091FF3D
	movq	%rax, 136(%rbx)
	movsd	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero
	jmp	.LBB0_10
.LBB0_7:
	divsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB0_10
# BB#8:                                 # %call.sqrt72
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB0_10:
	movsd	%xmm1, 144(%rbx)
	movq	160(%rbx), %rax
	testq	%rax, %rax
	jle	.LBB0_14
# BB#11:
	movsd	168(%rbx), %xmm2        # xmm2 = mem[0],zero
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm2, %xmm0
	divsd	%xmm1, %xmm0
	movsd	%xmm0, 200(%rbx)
	movsd	176(%rbx), %xmm0        # xmm0 = mem[0],zero
	mulsd	%xmm2, %xmm2
	divsd	%xmm1, %xmm2
	subsd	%xmm2, %xmm0
	xorpd	%xmm2, %xmm2
	ucomisd	%xmm0, %xmm2
	jbe	.LBB0_12
.LBB0_14:
	movabsq	$-4039728866278637763, %rax # imm = 0xC7EFFFFFE091FF3D
	movq	%rax, 200(%rbx)
	movsd	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero
	jmp	.LBB0_15
.LBB0_12:
	divsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB0_15
# BB#13:                                # %call.sqrt74
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB0_15:
	movsd	%xmm1, 208(%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	calcMetricsData, .Lfunc_end0-calcMetricsData
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
