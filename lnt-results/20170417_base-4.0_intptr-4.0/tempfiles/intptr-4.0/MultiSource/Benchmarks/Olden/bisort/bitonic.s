	.text
	.file	"bitonic.bc"
	.globl	InOrder
	.p2align	4, 0x90
	.type	InOrder,@function
InOrder:                                # @InOrder
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB0_4
	.p2align	4, 0x90
.LBB0_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movq	16(%rbx), %r14
	callq	InOrder
	movzbl	InOrder.counter(%rip), %eax
	movl	%eax, %ecx
	incb	%cl
	movb	%cl, InOrder.counter(%rip)
	testb	%al, %al
	jne	.LBB0_3
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	movl	(%rbx), %esi
	movl	$.L.str, %edi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	printf
.LBB0_3:                                # %tailrecurse.backedge
                                        #   in Loop: Header=BB0_1 Depth=1
	testq	%r14, %r14
	movq	%r14, %rbx
	jne	.LBB0_1
.LBB0_4:                                # %tailrecurse._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	InOrder, .Lfunc_end0-InOrder
	.cfi_endproc

	.globl	mult
	.p2align	4, 0x90
	.type	mult,@function
mult:                                   # @mult
	.cfi_startproc
# BB#0:
	movslq	%edi, %rcx
	imulq	$1759218605, %rcx, %rdx # imm = 0x68DB8BAD
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$44, %rdx
	addl	%eax, %edx
	imull	$10000, %edx, %eax      # imm = 0x2710
	subl	%eax, %ecx
	movslq	%esi, %rax
	imulq	$1759218605, %rax, %rsi # imm = 0x68DB8BAD
	movq	%rsi, %rdi
	shrq	$63, %rdi
	sarq	$44, %rsi
	addl	%edi, %esi
	imull	$10000, %esi, %edi      # imm = 0x2710
	subl	%edi, %eax
	imull	%ecx, %esi
	imull	%eax, %edx
	addl	%esi, %edx
	movslq	%edx, %rdx
	imulq	$1759218605, %rdx, %rsi # imm = 0x68DB8BAD
	movq	%rsi, %rdi
	shrq	$63, %rdi
	sarq	$44, %rsi
	addl	%edi, %esi
	imull	$10000, %esi, %esi      # imm = 0x2710
	subl	%esi, %edx
	imull	$10000, %edx, %edx      # imm = 0x2710
	imull	%ecx, %eax
	addl	%edx, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end1:
	.size	mult, .Lfunc_end1-mult
	.cfi_endproc

	.globl	skiprand
	.p2align	4, 0x90
	.type	skiprand,@function
skiprand:                               # @skiprand
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	je	.LBB2_2
	.p2align	4, 0x90
.LBB2_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	%edi, %rax
	imulq	$1759218605, %rax, %rcx # imm = 0x68DB8BAD
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$44, %rcx
	addl	%edx, %ecx
	imull	$10000, %ecx, %edx      # imm = 0x2710
	subl	%edx, %eax
	imull	$3141, %eax, %edx       # imm = 0xC45
	imull	$5821, %ecx, %ecx       # imm = 0x16BD
	addl	%edx, %ecx
	movslq	%ecx, %rcx
	imulq	$1759218605, %rcx, %rdx # imm = 0x68DB8BAD
	movq	%rdx, %rdi
	shrq	$63, %rdi
	sarq	$44, %rdx
	addl	%edi, %edx
	imull	$10000, %edx, %edx      # imm = 0x2710
	subl	%edx, %ecx
	imull	$10000, %ecx, %ecx      # imm = 0x2710
	imull	$5821, %eax, %eax       # imm = 0x16BD
	leal	1(%rax,%rcx), %edi
	decl	%esi
	jne	.LBB2_1
.LBB2_2:                                # %._crit_edge
	movl	%edi, %eax
	retq
.Lfunc_end2:
	.size	skiprand, .Lfunc_end2-skiprand
	.cfi_endproc

	.globl	random
	.p2align	4, 0x90
	.type	random,@function
random:                                 # @random
	.cfi_startproc
# BB#0:
	movslq	%edi, %rax
	imulq	$1759218605, %rax, %rcx # imm = 0x68DB8BAD
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$44, %rcx
	addl	%edx, %ecx
	imull	$10000, %ecx, %edx      # imm = 0x2710
	subl	%edx, %eax
	imull	$3141, %eax, %edx       # imm = 0xC45
	imull	$5821, %ecx, %ecx       # imm = 0x16BD
	addl	%edx, %ecx
	movslq	%ecx, %rcx
	imulq	$1759218605, %rcx, %rdx # imm = 0x68DB8BAD
	movq	%rdx, %rsi
	shrq	$63, %rsi
	sarq	$44, %rdx
	addl	%esi, %edx
	imull	$10000, %edx, %edx      # imm = 0x2710
	subl	%edx, %ecx
	imull	$10000, %ecx, %ecx      # imm = 0x2710
	imull	$5821, %eax, %eax       # imm = 0x16BD
	leal	1(%rax,%rcx), %eax
	retq
.Lfunc_end3:
	.size	random, .Lfunc_end3-random
	.cfi_endproc

	.globl	RandTree
	.p2align	4, 0x90
	.type	RandTree,@function
RandTree:                               # @RandTree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 64
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movl	%ecx, %r13d
	movl	%edi, %ebp
	incl	foo(%rip)
	cmpl	$2, %ebp
	jl	.LBB4_7
# BB#1:
	movl	NDim(%rip), %ecx
	cmpl	%r13d, %ecx
	movl	%edx, %eax
	jle	.LBB4_3
# BB#2:
	movl	%r13d, %eax
	notl	%eax
	addl	%eax, %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	addl	%edx, %eax
.LBB4_3:
	movl	%eax, (%rsp)            # 4-byte Spill
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movslq	%esi, %rax
	imulq	$1759218605, %rax, %rcx # imm = 0x68DB8BAD
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$44, %rcx
	addl	%edx, %ecx
	imull	$10000, %ecx, %edx      # imm = 0x2710
	subl	%edx, %eax
	imull	$3141, %eax, %edx       # imm = 0xC45
	imull	$5821, %ecx, %ecx       # imm = 0x16BD
	addl	%edx, %ecx
	movslq	%ecx, %rcx
	imulq	$1759218605, %rcx, %rdx # imm = 0x68DB8BAD
	movq	%rdx, %rsi
	shrq	$63, %rsi
	sarq	$44, %rdx
	addl	%esi, %edx
	imull	$10000, %edx, %edx      # imm = 0x2710
	subl	%edx, %ecx
	imull	$10000, %ecx, %ecx      # imm = 0x2710
	imull	$5821, %eax, %eax       # imm = 0x16BD
	leal	1(%rax,%rcx), %r14d
	movslq	%r14d, %r15
	imulq	$1374389535, %r15, %rax # imm = 0x51EB851F
	movq	%rax, %rcx
	shrq	$63, %rcx
	sarq	$37, %rax
	addl	%ecx, %eax
	imull	$100, %eax, %eax
	movl	%r15d, %ebx
	subl	%eax, %ebx
	movl	$24, %edi
	callq	malloc
	movq	%rax, %r12
	movl	%ebx, (%r12)
	movl	%ebp, %ebx
	shrl	%ebx
	incl	%r13d
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movl	%ebx, %edi
	movl	%r15d, %esi
	movl	(%rsp), %edx            # 4-byte Reload
	movl	%r13d, %ecx
	callq	RandTree
	movq	%rax, %r15
	cmpl	$-1, %ebp
	je	.LBB4_6
# BB#4:                                 # %.lr.ph.i.preheader
	notl	%ebp
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movslq	%r14d, %rax
	imulq	$1759218605, %rax, %rcx # imm = 0x68DB8BAD
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$44, %rcx
	addl	%edx, %ecx
	imull	$10000, %ecx, %edx      # imm = 0x2710
	subl	%edx, %eax
	imull	$3141, %eax, %edx       # imm = 0xC45
	imull	$5821, %ecx, %ecx       # imm = 0x16BD
	addl	%edx, %ecx
	movslq	%ecx, %rcx
	imulq	$1759218605, %rcx, %rdx # imm = 0x68DB8BAD
	movq	%rdx, %rsi
	shrq	$63, %rsi
	sarq	$44, %rdx
	addl	%esi, %edx
	imull	$10000, %edx, %edx      # imm = 0x2710
	subl	%edx, %ecx
	imull	$10000, %ecx, %ecx      # imm = 0x2710
	imull	$5821, %eax, %eax       # imm = 0x16BD
	leal	1(%rax,%rcx), %r14d
	incl	%ebp
	jne	.LBB4_5
.LBB4_6:                                # %skiprand.exit
	movl	%ebx, %edi
	movl	%r14d, %esi
	movl	4(%rsp), %edx           # 4-byte Reload
	movl	%r13d, %ecx
	callq	RandTree
	movq	%r15, 8(%r12)
	movq	%rax, 16(%r12)
	movq	%r12, %rax
	jmp	.LBB4_8
.LBB4_7:
	xorl	%eax, %eax
.LBB4_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	RandTree, .Lfunc_end4-RandTree
	.cfi_endproc

	.globl	SwapValue
	.p2align	4, 0x90
	.type	SwapValue,@function
SwapValue:                              # @SwapValue
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	movl	(%rsi), %ecx
	movl	%eax, (%rsi)
	movl	%ecx, (%rdi)
	retq
.Lfunc_end5:
	.size	SwapValue, .Lfunc_end5-SwapValue
	.cfi_endproc

	.globl	SwapValLeft
	.p2align	4, 0x90
	.type	SwapValLeft,@function
SwapValLeft:                            # @SwapValLeft
	.cfi_startproc
# BB#0:
	movl	%r8d, (%rsi)
	movq	%rdx, 8(%rsi)
	movq	%rcx, 8(%rdi)
	movl	%r9d, (%rdi)
	retq
.Lfunc_end6:
	.size	SwapValLeft, .Lfunc_end6-SwapValLeft
	.cfi_endproc

	.globl	SwapValRight
	.p2align	4, 0x90
	.type	SwapValRight,@function
SwapValRight:                           # @SwapValRight
	.cfi_startproc
# BB#0:
	movl	%r8d, (%rsi)
	movq	%rdx, 16(%rsi)
	movq	%rcx, 16(%rdi)
	movl	%r9d, (%rdi)
	retq
.Lfunc_end7:
	.size	SwapValRight, .Lfunc_end7-SwapValRight
	.cfi_endproc

	.globl	Bimerge
	.p2align	4, 0x90
	.type	Bimerge,@function
Bimerge:                                # @Bimerge
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 48
.Lcfi23:
	.cfi_offset %rbx, -48
.Lcfi24:
	.cfi_offset %r12, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movq	%rdi, %r14
	jmp	.LBB8_1
	.p2align	4, 0x90
.LBB8_10:                               # %.lr.ph.split
                                        #   Parent Loop BB8_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ebp
	movq	8(%rdi), %rdx
	movl	(%rax), %esi
	movq	8(%rax), %rcx
	xorl	%ebx, %ebx
	cmpl	%esi, %ebp
	setg	%bl
	cmpl	%r12d, %ebx
	je	.LBB8_12
# BB#11:                                #   in Loop: Header=BB8_10 Depth=2
	movq	16(%rax), %rbx
	movq	16(%rdi), %r8
	movl	%ebp, (%rax)
	movq	%rdx, 8(%rax)
	movq	%rcx, 8(%rdi)
	movl	%esi, (%rdi)
	movq	%rbx, %rcx
	movq	%r8, %rdx
.LBB8_12:                               # %.backedge
                                        #   in Loop: Header=BB8_10 Depth=2
	testq	%rdx, %rdx
	movq	%rdx, %rdi
	movq	%rcx, %rax
	jne	.LBB8_10
# BB#13:                                # %._crit_edge.loopexit84
                                        #   in Loop: Header=BB8_1 Depth=1
	movq	8(%r14), %rdi
.LBB8_14:                               # %._crit_edge
                                        #   in Loop: Header=BB8_1 Depth=1
	testq	%rdi, %rdi
	je	.LBB8_16
# BB#15:                                #   in Loop: Header=BB8_1 Depth=1
	movq	16(%r14), %rbx
	movl	(%r14), %esi
	movl	%r12d, %edx
	callq	Bimerge
	movl	%eax, (%r14)
	movq	%rbx, %r14
	movl	%r15d, %esi
.LBB8_1:                                # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_10 Depth 2
                                        #     Child Loop BB8_7 Depth 2
	movl	(%r14), %r15d
	movq	8(%r14), %rdi
	movq	16(%r14), %rax
	xorl	%ecx, %ecx
	cmpl	%esi, %r15d
	setg	%cl
	cmpl	%r12d, %ecx
	je	.LBB8_2
# BB#3:                                 #   in Loop: Header=BB8_1 Depth=1
	movl	%esi, (%r14)
	testq	%rdi, %rdi
	jne	.LBB8_5
	jmp	.LBB8_16
	.p2align	4, 0x90
.LBB8_2:                                #   in Loop: Header=BB8_1 Depth=1
	movl	%esi, %r15d
	testq	%rdi, %rdi
	je	.LBB8_16
.LBB8_5:                                # %.lr.ph
                                        #   in Loop: Header=BB8_1 Depth=1
	cmpl	%r12d, %ecx
	je	.LBB8_10
# BB#6:                                 # %.lr.ph.split.us.preheader
                                        #   in Loop: Header=BB8_1 Depth=1
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB8_7:                                # %.lr.ph.split.us
                                        #   Parent Loop BB8_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %ebx
	movq	16(%rcx), %rsi
	movl	(%rax), %ebp
	movq	16(%rax), %r8
	xorl	%edx, %edx
	cmpl	%ebp, %ebx
	setg	%dl
	cmpl	%r12d, %edx
	je	.LBB8_9
# BB#8:                                 #   in Loop: Header=BB8_7 Depth=2
	movq	8(%rax), %rdx
	movq	8(%rcx), %r9
	movl	%ebx, (%rax)
	movq	%rsi, 16(%rax)
	movq	%r8, 16(%rcx)
	movl	%ebp, (%rcx)
	movq	%rdx, %r8
	movq	%r9, %rsi
.LBB8_9:                                # %.backedge.us
                                        #   in Loop: Header=BB8_7 Depth=2
	testq	%rsi, %rsi
	movq	%rsi, %rcx
	movq	%r8, %rax
	jne	.LBB8_7
	jmp	.LBB8_14
.LBB8_16:                               # %._crit_edge.thread
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	Bimerge, .Lfunc_end8-Bimerge
	.cfi_endproc

	.globl	Bisort
	.p2align	4, 0x90
	.type	Bisort,@function
Bisort:                                 # @Bisort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 48
.Lcfi33:
	.cfi_offset %rbx, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_1
# BB#3:
	movq	16(%rbx), %r15
	movl	(%rbx), %esi
	movl	%ebp, %edx
	callq	Bisort
	movl	%eax, (%rbx)
	xorl	%edx, %edx
	testl	%ebp, %ebp
	sete	%dl
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	Bisort
	movq	%rbx, %rdi
	movl	%eax, %esi
	movl	%ebp, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	Bimerge                 # TAILCALL
.LBB9_1:
	movl	(%rbx), %eax
	xorl	%ecx, %ecx
	cmpl	%r14d, %eax
	setg	%cl
	cmpl	%ebp, %ecx
	jne	.LBB9_4
# BB#2:
	movl	%r14d, %eax
	jmp	.LBB9_5
.LBB9_4:
	movl	%r14d, (%rbx)
.LBB9_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	Bisort, .Lfunc_end9-Bisort
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 32
.Lcfi40:
	.cfi_offset %rbx, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	callq	dealwithargs
	movl	%eax, %ebx
	movl	NDim(%rip), %edx
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movl	$12345768, %esi         # imm = 0xBC61A8
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	%ebx, %edi
	callq	RandTree
	movq	%rax, %rbx
	cmpl	$0, flag(%rip)
	je	.LBB10_2
# BB#1:
	movq	%rbx, %rdi
	callq	InOrder
	movl	$.L.str.2, %edi
	movl	$8, %esi
	xorl	%eax, %eax
	callq	printf
.LBB10_2:
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$8, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	Bisort
	movl	%eax, %ebp
	cmpl	$0, flag(%rip)
	je	.LBB10_4
# BB#3:
	movl	$.Lstr.4, %edi
	callq	puts
	movq	%rbx, %rdi
	callq	InOrder
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
.LBB10_4:
	movl	$1, %edx
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	Bisort
	movl	%eax, %ebp
	cmpl	$0, flag(%rip)
	je	.LBB10_6
# BB#5:
	movl	$.Lstr.4, %edi
	callq	puts
	movq	%rbx, %rdi
	callq	InOrder
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
.LBB10_6:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end10:
	.size	main, .Lfunc_end10-main
	.cfi_endproc

	.type	flag,@object            # @flag
	.bss
	.globl	flag
	.p2align	2
flag:
	.long	0                       # 0x0
	.size	flag, 4

	.type	foo,@object             # @foo
	.globl	foo
	.p2align	2
foo:
	.long	0                       # 0x0
	.size	foo, 4

	.type	InOrder.counter,@object # @InOrder.counter
	.local	InOrder.counter
	.comm	InOrder.counter,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d @ 0x%x\n"
	.size	.L.str, 11

	.type	NDim,@object            # @NDim
	.comm	NDim,4,4
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Bisort with %d size of dim %d\n"
	.size	.L.str.1, 31

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%d\n"
	.size	.L.str.2, 4

	.type	NumNodes,@object        # @NumNodes
	.comm	NumNodes,4,4
	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.1:
	.asciz	"BEGINNING BITONIC SORT ALGORITHM HERE"
	.size	.Lstr.1, 38

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"**************************************"
	.size	.Lstr.2, 39

	.type	.Lstr.4,@object         # @str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.4:
	.asciz	"Sorted Tree:"
	.size	.Lstr.4, 13


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
