	.text
	.file	"display.bc"
	.globl	check_hash_code_sanity
	.p2align	4, 0x90
	.type	check_hash_code_sanity,@function
check_hash_code_sanity:                 # @check_hash_code_sanity
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	g_board_size(%rip), %r14d
	movl	g_board_size+4(%rip), %ebp
	testl	%r14d, %r14d
	jle	.LBB0_49
# BB#1:                                 # %.preheader.lr.ph
	movl	%r14d, %eax
	decl	%eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	movq	%r14, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_5 Depth 2
                                        #       Child Loop BB0_9 Depth 3
                                        #       Child Loop BB0_14 Depth 3
                                        #       Child Loop BB0_19 Depth 3
                                        #       Child Loop BB0_24 Depth 3
                                        #       Child Loop BB0_31 Depth 3
                                        #       Child Loop BB0_36 Depth 3
                                        #       Child Loop BB0_41 Depth 3
                                        #       Child Loop BB0_46 Depth 3
	movq	%rax, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	incq	%rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	testl	%ebp, %ebp
	jle	.LBB0_2
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	24(%rsp), %r13          # 8-byte Reload
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	imull	%ebp, %r13d
	addl	36(%rsp), %r12d         # 4-byte Folded Reload
	imull	%ebp, %r12d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_5:                                #   Parent Loop BB0_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_9 Depth 3
                                        #       Child Loop BB0_14 Depth 3
                                        #       Child Loop BB0_19 Depth 3
                                        #       Child Loop BB0_24 Depth 3
                                        #       Child Loop BB0_31 Depth 3
                                        #       Child Loop BB0_36 Depth 3
                                        #       Child Loop BB0_41 Depth 3
                                        #       Child Loop BB0_46 Depth 3
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	g_board+4(,%rax,4), %eax
	leal	1(%r14), %r15d
	leal	(%r14,%r13), %esi
	btl	%r15d, %eax
	jae	.LBB0_28
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=2
	movl	g_norm_hashkey+16(%rip), %eax
	movl	%eax, 16(%rsp)
	movups	g_norm_hashkey(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movl	%esi, %eax
	sarl	$31, %eax
	shrl	$27, %eax
	addl	%esi, %eax
	sarl	$5, %eax
	cltq
	movl	(%rsp,%rax,4), %eax
	btl	%esi, %eax
	jb	.LBB0_11
# BB#7:                                 #   in Loop: Header=BB0_5 Depth=2
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$-1, g_board_size(%rip)
	jl	.LBB0_10
# BB#8:                                 # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB0_5 Depth=2
	movq	$-1, %rbx
	.p2align	4, 0x90
.LBB0_9:                                # %.lr.ph.i.i
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	g_board+4(,%rbx,4), %esi
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	printf
	movslq	g_board_size(%rip), %rax
	incq	%rbx
	cmpq	%rax, %rbx
	jle	.LBB0_9
.LBB0_10:                               # %print_bitboard.exit.i
                                        #   in Loop: Header=BB0_5 Depth=2
	movl	(%rsp), %esi
	movl	4(%rsp), %edx
	movl	8(%rsp), %ecx
	movl	12(%rsp), %r8d
	movl	16(%rsp), %r9d
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.1, %edi
	movl	$23, %esi
	movl	$1, %edx
	movl	$.L.str.28, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
.LBB0_11:                               # %check_hashkey_bit_set.exit
                                        #   in Loop: Header=BB0_5 Depth=2
	movl	%ebp, %ebx
	subl	%r14d, %ebx
	leal	-1(%rbx,%r13), %esi
	movl	g_flipV_hashkey+16(%rip), %eax
	movl	%eax, 16(%rsp)
	movups	g_flipV_hashkey(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movl	%esi, %eax
	sarl	$31, %eax
	shrl	$27, %eax
	addl	%esi, %eax
	sarl	$5, %eax
	cltq
	movl	(%rsp,%rax,4), %eax
	btl	%esi, %eax
	jb	.LBB0_16
# BB#12:                                #   in Loop: Header=BB0_5 Depth=2
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$-1, g_board_size(%rip)
	jl	.LBB0_15
# BB#13:                                # %.lr.ph.i.i55.preheader
                                        #   in Loop: Header=BB0_5 Depth=2
	movq	$-1, %rbp
	.p2align	4, 0x90
.LBB0_14:                               # %.lr.ph.i.i55
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	g_board+4(,%rbp,4), %esi
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	printf
	movslq	g_board_size(%rip), %rax
	incq	%rbp
	cmpq	%rax, %rbp
	jle	.LBB0_14
.LBB0_15:                               # %print_bitboard.exit.i66
                                        #   in Loop: Header=BB0_5 Depth=2
	movl	(%rsp), %esi
	movl	4(%rsp), %edx
	movl	8(%rsp), %ecx
	movl	12(%rsp), %r8d
	movl	16(%rsp), %r9d
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.1, %edi
	movl	$23, %esi
	movl	$1, %edx
	movl	$.L.str.28, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
	movl	20(%rsp), %ebp          # 4-byte Reload
.LBB0_16:                               # %check_hashkey_bit_set.exit67
                                        #   in Loop: Header=BB0_5 Depth=2
	decl	%ebx
	addl	%r12d, %r14d
	movl	g_flipH_hashkey+16(%rip), %eax
	movl	%eax, 16(%rsp)
	movups	g_flipH_hashkey(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movl	%r14d, %eax
	sarl	$31, %eax
	shrl	$27, %eax
	addl	%r14d, %eax
	sarl	$5, %eax
	cltq
	movl	(%rsp,%rax,4), %eax
	btl	%r14d, %eax
	jb	.LBB0_21
# BB#17:                                #   in Loop: Header=BB0_5 Depth=2
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
	cmpl	$-1, g_board_size(%rip)
	jl	.LBB0_20
# BB#18:                                # %.lr.ph.i.i70.preheader
                                        #   in Loop: Header=BB0_5 Depth=2
	movq	$-1, %rbp
	.p2align	4, 0x90
.LBB0_19:                               # %.lr.ph.i.i70
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	g_board+4(,%rbp,4), %esi
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	printf
	movslq	g_board_size(%rip), %rax
	incq	%rbp
	cmpq	%rax, %rbp
	jle	.LBB0_19
.LBB0_20:                               # %print_bitboard.exit.i81
                                        #   in Loop: Header=BB0_5 Depth=2
	movl	(%rsp), %esi
	movl	4(%rsp), %edx
	movl	8(%rsp), %ecx
	movl	12(%rsp), %r8d
	movl	16(%rsp), %r9d
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.1, %edi
	movl	$23, %esi
	movl	$1, %edx
	movl	$.L.str.28, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
	movl	20(%rsp), %ebp          # 4-byte Reload
.LBB0_21:                               # %check_hashkey_bit_set.exit82
                                        #   in Loop: Header=BB0_5 Depth=2
	addl	%r12d, %ebx
	movl	g_flipVH_hashkey+16(%rip), %eax
	movl	%eax, 16(%rsp)
	movups	g_flipVH_hashkey(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movl	%ebx, %eax
	sarl	$31, %eax
	shrl	$27, %eax
	addl	%ebx, %eax
	sarl	$5, %eax
	cltq
	movl	(%rsp,%rax,4), %eax
	btl	%ebx, %eax
	jb	.LBB0_27
# BB#22:                                #   in Loop: Header=BB0_5 Depth=2
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	cmpl	$-1, g_board_size(%rip)
	jl	.LBB0_25
# BB#23:                                # %.lr.ph.i.i85.preheader
                                        #   in Loop: Header=BB0_5 Depth=2
	movq	$-1, %rbx
	.p2align	4, 0x90
.LBB0_24:                               # %.lr.ph.i.i85
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	g_board+4(,%rbx,4), %esi
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	printf
	movslq	g_board_size(%rip), %rax
	incq	%rbx
	cmpq	%rax, %rbx
	jle	.LBB0_24
.LBB0_25:                               # %print_bitboard.exit.i96
                                        #   in Loop: Header=BB0_5 Depth=2
	movl	(%rsp), %esi
	movl	4(%rsp), %edx
	movl	8(%rsp), %ecx
	movl	12(%rsp), %r8d
	movl	16(%rsp), %r9d
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.1, %edi
	movl	$23, %esi
	jmp	.LBB0_26
	.p2align	4, 0x90
.LBB0_28:                               #   in Loop: Header=BB0_5 Depth=2
	movl	g_norm_hashkey+16(%rip), %eax
	movl	%eax, 16(%rsp)
	movups	g_norm_hashkey(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movl	%esi, %eax
	sarl	$31, %eax
	shrl	$27, %eax
	addl	%esi, %eax
	sarl	$5, %eax
	cltq
	movl	(%rsp,%rax,4), %eax
	btl	%esi, %eax
	jae	.LBB0_33
# BB#29:                                #   in Loop: Header=BB0_5 Depth=2
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$-1, g_board_size(%rip)
	jl	.LBB0_32
# BB#30:                                # %.lr.ph.i.i100.preheader
                                        #   in Loop: Header=BB0_5 Depth=2
	movq	$-1, %rbx
	.p2align	4, 0x90
.LBB0_31:                               # %.lr.ph.i.i100
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	g_board+4(,%rbx,4), %esi
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	printf
	movslq	g_board_size(%rip), %rax
	incq	%rbx
	cmpq	%rax, %rbx
	jle	.LBB0_31
.LBB0_32:                               # %print_bitboard.exit.i111
                                        #   in Loop: Header=BB0_5 Depth=2
	movl	(%rsp), %esi
	movl	4(%rsp), %edx
	movl	8(%rsp), %ecx
	movl	12(%rsp), %r8d
	movl	16(%rsp), %r9d
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.1, %edi
	movl	$38, %esi
	movl	$1, %edx
	movl	$.L.str.28, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
.LBB0_33:                               # %check_hashkey_bit_not_set.exit
                                        #   in Loop: Header=BB0_5 Depth=2
	movl	%ebp, %ebx
	subl	%r14d, %ebx
	leal	-1(%rbx,%r13), %esi
	movl	g_flipV_hashkey+16(%rip), %eax
	movl	%eax, 16(%rsp)
	movups	g_flipV_hashkey(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movl	%esi, %eax
	sarl	$31, %eax
	shrl	$27, %eax
	addl	%esi, %eax
	sarl	$5, %eax
	cltq
	movl	(%rsp,%rax,4), %eax
	btl	%esi, %eax
	jae	.LBB0_38
# BB#34:                                #   in Loop: Header=BB0_5 Depth=2
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$-1, g_board_size(%rip)
	jl	.LBB0_37
# BB#35:                                # %.lr.ph.i.i114.preheader
                                        #   in Loop: Header=BB0_5 Depth=2
	movq	$-1, %rbp
	.p2align	4, 0x90
.LBB0_36:                               # %.lr.ph.i.i114
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	g_board+4(,%rbp,4), %esi
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	printf
	movslq	g_board_size(%rip), %rax
	incq	%rbp
	cmpq	%rax, %rbp
	jle	.LBB0_36
.LBB0_37:                               # %print_bitboard.exit.i125
                                        #   in Loop: Header=BB0_5 Depth=2
	movl	(%rsp), %esi
	movl	4(%rsp), %edx
	movl	8(%rsp), %ecx
	movl	12(%rsp), %r8d
	movl	16(%rsp), %r9d
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.1, %edi
	movl	$38, %esi
	movl	$1, %edx
	movl	$.L.str.28, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
	movl	20(%rsp), %ebp          # 4-byte Reload
.LBB0_38:                               # %check_hashkey_bit_not_set.exit126
                                        #   in Loop: Header=BB0_5 Depth=2
	decl	%ebx
	addl	%r12d, %r14d
	movl	g_flipH_hashkey+16(%rip), %eax
	movl	%eax, 16(%rsp)
	movups	g_flipH_hashkey(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movl	%r14d, %eax
	sarl	$31, %eax
	shrl	$27, %eax
	addl	%r14d, %eax
	sarl	$5, %eax
	cltq
	movl	(%rsp,%rax,4), %eax
	btl	%r14d, %eax
	jae	.LBB0_43
# BB#39:                                #   in Loop: Header=BB0_5 Depth=2
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
	cmpl	$-1, g_board_size(%rip)
	jl	.LBB0_42
# BB#40:                                # %.lr.ph.i.i129.preheader
                                        #   in Loop: Header=BB0_5 Depth=2
	movq	$-1, %rbp
	.p2align	4, 0x90
.LBB0_41:                               # %.lr.ph.i.i129
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	g_board+4(,%rbp,4), %esi
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	printf
	movslq	g_board_size(%rip), %rax
	incq	%rbp
	cmpq	%rax, %rbp
	jle	.LBB0_41
.LBB0_42:                               # %print_bitboard.exit.i140
                                        #   in Loop: Header=BB0_5 Depth=2
	movl	(%rsp), %esi
	movl	4(%rsp), %edx
	movl	8(%rsp), %ecx
	movl	12(%rsp), %r8d
	movl	16(%rsp), %r9d
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.1, %edi
	movl	$38, %esi
	movl	$1, %edx
	movl	$.L.str.28, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
	movl	20(%rsp), %ebp          # 4-byte Reload
.LBB0_43:                               # %check_hashkey_bit_not_set.exit141
                                        #   in Loop: Header=BB0_5 Depth=2
	addl	%r12d, %ebx
	movl	g_flipVH_hashkey+16(%rip), %eax
	movl	%eax, 16(%rsp)
	movups	g_flipVH_hashkey(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movl	%ebx, %eax
	sarl	$31, %eax
	shrl	$27, %eax
	addl	%ebx, %eax
	sarl	$5, %eax
	cltq
	movl	(%rsp,%rax,4), %eax
	btl	%ebx, %eax
	jae	.LBB0_27
# BB#44:                                #   in Loop: Header=BB0_5 Depth=2
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	cmpl	$-1, g_board_size(%rip)
	jl	.LBB0_47
# BB#45:                                # %.lr.ph.i.i144.preheader
                                        #   in Loop: Header=BB0_5 Depth=2
	movq	$-1, %rbx
	.p2align	4, 0x90
.LBB0_46:                               # %.lr.ph.i.i144
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	g_board+4(,%rbx,4), %esi
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	printf
	movslq	g_board_size(%rip), %rax
	incq	%rbx
	cmpq	%rax, %rbx
	jle	.LBB0_46
.LBB0_47:                               # %print_bitboard.exit.i155
                                        #   in Loop: Header=BB0_5 Depth=2
	movl	(%rsp), %esi
	movl	4(%rsp), %edx
	movl	8(%rsp), %ecx
	movl	12(%rsp), %r8d
	movl	16(%rsp), %r9d
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.1, %edi
	movl	$38, %esi
.LBB0_26:                               # %.backedge
                                        #   in Loop: Header=BB0_5 Depth=2
	movl	$1, %edx
	movl	$.L.str.28, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
.LBB0_27:                               # %.backedge
                                        #   in Loop: Header=BB0_5 Depth=2
	cmpl	%ebp, %r15d
	movl	%r15d, %r14d
	jne	.LBB0_5
.LBB0_2:                                # %.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	notl	%ecx
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	cmpq	%r14, %rax
	movl	%ecx, %r12d
	jne	.LBB0_3
# BB#48:                                # %._crit_edge.loopexit
	movl	g_board_size(%rip), %r14d
	movl	g_board_size+4(%rip), %ebp
.LBB0_49:                               # %._crit_edge
	movl	g_norm_hashkey+16(%rip), %eax
	movl	%eax, 16(%rsp)
	movups	g_norm_hashkey(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movl	16(%rsp), %eax
	testl	%r14d, %r14d
	jle	.LBB0_57
# BB#50:                                # %._crit_edge
	testl	%ebp, %ebp
	jle	.LBB0_57
# BB#51:                                # %.preheader.us.preheader.i
	movl	%r14d, %r8d
	movl	%ebp, %r9d
	xorl	%esi, %esi
	movl	$g_zobrist+132, %ecx
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB0_52:                               # %.preheader.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_53 Depth 2
	movl	%ebp, %r11d
	incq	%r10
	movq	%rcx, %rdi
	movq	%r9, %rdx
	movl	%esi, %ebp
	.p2align	4, 0x90
.LBB0_53:                               #   Parent Loop BB0_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %ebx
	sarl	$31, %ebx
	shrl	$27, %ebx
	addl	%ebp, %ebx
	sarl	$5, %ebx
	movslq	%ebx, %rbx
	movl	(%rsp,%rbx,4), %ebx
	btl	%ebp, %ebx
	jae	.LBB0_55
# BB#54:                                #   in Loop: Header=BB0_53 Depth=2
	xorl	(%rdi), %eax
.LBB0_55:                               # %._crit_edge.i
                                        #   in Loop: Header=BB0_53 Depth=2
	incl	%ebp
	addq	$4, %rdi
	decq	%rdx
	jne	.LBB0_53
# BB#56:                                # %._crit_edge.us.i
                                        #   in Loop: Header=BB0_52 Depth=1
	movl	%r11d, %ebp
	addl	%ebp, %esi
	subq	$-128, %rcx
	cmpq	%r8, %r10
	jne	.LBB0_52
.LBB0_57:                               # %._crit_edge31.i
	testl	%eax, %eax
	je	.LBB0_59
# BB#58:
	movl	$.L.str.1, %edi
	movl	$59, %esi
	movl	$1, %edx
	movl	$.L.str.29, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
	movl	g_board_size(%rip), %r14d
	movl	g_board_size+4(%rip), %ebp
.LBB0_59:                               # %check_hashkey_code.exit
	movl	g_flipV_hashkey+16(%rip), %eax
	movl	%eax, 16(%rsp)
	movups	g_flipV_hashkey(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movl	16(%rsp), %eax
	testl	%r14d, %r14d
	jle	.LBB0_67
# BB#60:                                # %check_hashkey_code.exit
	testl	%ebp, %ebp
	jle	.LBB0_67
# BB#61:                                # %.preheader.us.preheader.i160
	movl	%r14d, %r8d
	movl	%ebp, %r9d
	xorl	%esi, %esi
	movl	$g_zobrist+132, %ecx
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB0_62:                               # %.preheader.us.i164
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_63 Depth 2
	movl	%ebp, %r11d
	incq	%r10
	movq	%rcx, %rdi
	movq	%r9, %rdx
	movl	%esi, %ebp
	.p2align	4, 0x90
.LBB0_63:                               #   Parent Loop BB0_62 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %ebx
	sarl	$31, %ebx
	shrl	$27, %ebx
	addl	%ebp, %ebx
	sarl	$5, %ebx
	movslq	%ebx, %rbx
	movl	(%rsp,%rbx,4), %ebx
	btl	%ebp, %ebx
	jae	.LBB0_65
# BB#64:                                #   in Loop: Header=BB0_63 Depth=2
	xorl	(%rdi), %eax
.LBB0_65:                               # %._crit_edge.i170
                                        #   in Loop: Header=BB0_63 Depth=2
	incl	%ebp
	addq	$4, %rdi
	decq	%rdx
	jne	.LBB0_63
# BB#66:                                # %._crit_edge.us.i172
                                        #   in Loop: Header=BB0_62 Depth=1
	movl	%r11d, %ebp
	addl	%ebp, %esi
	subq	$-128, %rcx
	cmpq	%r8, %r10
	jne	.LBB0_62
.LBB0_67:                               # %._crit_edge31.i174
	testl	%eax, %eax
	je	.LBB0_69
# BB#68:
	movl	$.L.str.1, %edi
	movl	$59, %esi
	movl	$1, %edx
	movl	$.L.str.29, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
	movl	g_board_size(%rip), %r14d
	movl	g_board_size+4(%rip), %ebp
.LBB0_69:                               # %check_hashkey_code.exit175
	movl	g_flipH_hashkey+16(%rip), %eax
	movl	%eax, 16(%rsp)
	movups	g_flipH_hashkey(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movl	16(%rsp), %eax
	testl	%r14d, %r14d
	jle	.LBB0_77
# BB#70:                                # %check_hashkey_code.exit175
	testl	%ebp, %ebp
	jle	.LBB0_77
# BB#71:                                # %.preheader.us.preheader.i179
	movl	%r14d, %r8d
	movl	%ebp, %r9d
	xorl	%esi, %esi
	movl	$g_zobrist+132, %ecx
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB0_72:                               # %.preheader.us.i183
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_73 Depth 2
	movl	%ebp, %r11d
	incq	%r10
	movq	%rcx, %rdi
	movq	%r9, %rdx
	movl	%esi, %ebp
	.p2align	4, 0x90
.LBB0_73:                               #   Parent Loop BB0_72 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %ebx
	sarl	$31, %ebx
	shrl	$27, %ebx
	addl	%ebp, %ebx
	sarl	$5, %ebx
	movslq	%ebx, %rbx
	movl	(%rsp,%rbx,4), %ebx
	btl	%ebp, %ebx
	jae	.LBB0_75
# BB#74:                                #   in Loop: Header=BB0_73 Depth=2
	xorl	(%rdi), %eax
.LBB0_75:                               # %._crit_edge.i189
                                        #   in Loop: Header=BB0_73 Depth=2
	incl	%ebp
	addq	$4, %rdi
	decq	%rdx
	jne	.LBB0_73
# BB#76:                                # %._crit_edge.us.i191
                                        #   in Loop: Header=BB0_72 Depth=1
	movl	%r11d, %ebp
	addl	%ebp, %esi
	subq	$-128, %rcx
	cmpq	%r8, %r10
	jne	.LBB0_72
.LBB0_77:                               # %._crit_edge31.i193
	testl	%eax, %eax
	je	.LBB0_79
# BB#78:
	movl	$.L.str.1, %edi
	movl	$59, %esi
	movl	$1, %edx
	movl	$.L.str.29, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
	movl	g_board_size(%rip), %r14d
	movl	g_board_size+4(%rip), %ebp
.LBB0_79:                               # %check_hashkey_code.exit194
	movl	g_flipVH_hashkey+16(%rip), %eax
	movl	%eax, 16(%rsp)
	movups	g_flipVH_hashkey(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movl	16(%rsp), %eax
	testl	%r14d, %r14d
	jle	.LBB0_87
# BB#80:                                # %check_hashkey_code.exit194
	testl	%ebp, %ebp
	jle	.LBB0_87
# BB#81:                                # %.preheader.us.preheader.i198
	movl	%r14d, %r8d
	movl	%ebp, %r9d
	xorl	%esi, %esi
	movl	$g_zobrist+132, %ecx
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB0_82:                               # %.preheader.us.i202
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_83 Depth 2
	movl	%ebp, %r11d
	incq	%r10
	movq	%rcx, %rdi
	movq	%r9, %rdx
	movl	%esi, %ebp
	.p2align	4, 0x90
.LBB0_83:                               #   Parent Loop BB0_82 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %ebx
	sarl	$31, %ebx
	shrl	$27, %ebx
	addl	%ebp, %ebx
	sarl	$5, %ebx
	movslq	%ebx, %rbx
	movl	(%rsp,%rbx,4), %ebx
	btl	%ebp, %ebx
	jae	.LBB0_85
# BB#84:                                #   in Loop: Header=BB0_83 Depth=2
	xorl	(%rdi), %eax
.LBB0_85:                               # %._crit_edge.i208
                                        #   in Loop: Header=BB0_83 Depth=2
	incl	%ebp
	addq	$4, %rdi
	decq	%rdx
	jne	.LBB0_83
# BB#86:                                # %._crit_edge.us.i210
                                        #   in Loop: Header=BB0_82 Depth=1
	movl	%r11d, %ebp
	addl	%ebp, %esi
	subq	$-128, %rcx
	cmpq	%r8, %r10
	jne	.LBB0_82
.LBB0_87:                               # %._crit_edge31.i212
	testl	%eax, %eax
	je	.LBB0_89
# BB#88:
	movl	$.L.str.1, %edi
	movl	$59, %esi
	movl	$1, %edx
	movl	$.L.str.29, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
.LBB0_89:                               # %check_hashkey_code.exit213
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	check_hash_code_sanity, .Lfunc_end0-check_hash_code_sanity
	.cfi_endproc

	.globl	check_board_sanity
	.p2align	4, 0x90
	.type	check_board_sanity,@function
check_board_sanity:                     # @check_board_sanity
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	cmpl	$-1, g_board_size(%rip)
	jl	.LBB1_29
# BB#1:                                 # %.preheader.preheader
	movl	g_board_size+4(%rip), %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_4 Depth 2
                                        #       Child Loop BB1_14 Depth 3
                                        #       Child Loop BB1_8 Depth 3
                                        #         Child Loop BB1_9 Depth 4
                                        #       Child Loop BB1_25 Depth 3
                                        #       Child Loop BB1_19 Depth 3
                                        #         Child Loop BB1_20 Depth 4
	cmpl	$-1, %ebp
	jl	.LBB1_28
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	$1, %esi
	movl	%ebx, %ecx
	shll	%cl, %esi
	xorl	%r12d, %r12d
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movl	%esi, 12(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB1_4:                                #   Parent Loop BB1_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_14 Depth 3
                                        #       Child Loop BB1_8 Depth 3
                                        #         Child Loop BB1_9 Depth 4
                                        #       Child Loop BB1_25 Depth 3
                                        #       Child Loop BB1_19 Depth 3
                                        #         Child Loop BB1_20 Depth 4
	xorl	%eax, %eax
	testl	g_board+128(,%r12,4), %esi
	setne	%al
	movl	g_board(,%rbx,4), %ecx
	leal	1(%rax), %edx
	btl	%r12d, %ecx
	cmovael	%eax, %edx
	cmpl	$1, %edx
	jne	.LBB1_27
# BB#5:                                 #   in Loop: Header=BB1_4 Depth=2
	movl	g_board_size(%rip), %r14d
	testl	%ebp, %ebp
	jle	.LBB1_16
# BB#6:                                 # %.preheader.lr.ph.i
                                        #   in Loop: Header=BB1_4 Depth=2
	testl	%r14d, %r14d
	jle	.LBB1_14
# BB#7:                                 # %.preheader.us.preheader.i
                                        #   in Loop: Header=BB1_4 Depth=2
	movl	%ebp, %r13d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_8:                                # %.preheader.us.i
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_9 Depth 4
	leaq	1(%rbp), %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_9:                                #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        #       Parent Loop BB1_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	g_board+132(,%rbp,4), %eax
	incl	%ebx
	btl	%ebx, %eax
	jae	.LBB1_11
# BB#10:                                #   in Loop: Header=BB1_9 Depth=4
	movl	$.L.str.5, %edi
	jmp	.LBB1_12
	.p2align	4, 0x90
.LBB1_11:                               #   in Loop: Header=BB1_9 Depth=4
	movl	$.L.str.6, %edi
.LBB1_12:                               # %.backedge.us.i
                                        #   in Loop: Header=BB1_9 Depth=4
	xorl	%eax, %eax
	callq	printf
	cmpl	%ebx, %r14d
	jne	.LBB1_9
# BB#13:                                # %._crit_edge.us.i
                                        #   in Loop: Header=BB1_8 Depth=3
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	cmpq	%r13, %r15
	movq	%r15, %rbp
	jne	.LBB1_8
	jmp	.LBB1_15
	.p2align	4, 0x90
.LBB1_14:                               # %.preheader.i
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	decl	%ebp
	jne	.LBB1_14
.LBB1_15:                               # %print_board.exitthread-pre-split
                                        #   in Loop: Header=BB1_4 Depth=2
	movl	g_board_size(%rip), %r14d
	movl	g_board_size+4(%rip), %ebp
.LBB1_16:                               # %print_board.exit
                                        #   in Loop: Header=BB1_4 Depth=2
	testl	%r14d, %r14d
	jle	.LBB1_26
# BB#17:                                # %.preheader.lr.ph.i21
                                        #   in Loop: Header=BB1_4 Depth=2
	testl	%ebp, %ebp
	jle	.LBB1_25
# BB#18:                                # %.preheader.us.preheader.i23
                                        #   in Loop: Header=BB1_4 Depth=2
	movl	%r14d, %r13d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_19:                               # %.preheader.us.i26
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_20 Depth 4
	leaq	1(%rbx), %r15
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB1_20:                               #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        #       Parent Loop BB1_19 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	g_board+4(,%rbx,4), %eax
	incl	%r14d
	btl	%r14d, %eax
	jae	.LBB1_22
# BB#21:                                #   in Loop: Header=BB1_20 Depth=4
	movl	$.L.str.5, %edi
	jmp	.LBB1_23
	.p2align	4, 0x90
.LBB1_22:                               #   in Loop: Header=BB1_20 Depth=4
	movl	$.L.str.6, %edi
.LBB1_23:                               # %.backedge.us.i29
                                        #   in Loop: Header=BB1_20 Depth=4
	xorl	%eax, %eax
	callq	printf
	cmpl	%r14d, %ebp
	jne	.LBB1_20
# BB#24:                                # %._crit_edge.us.i31
                                        #   in Loop: Header=BB1_19 Depth=3
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	cmpq	%r13, %r15
	movq	%r15, %rbx
	jne	.LBB1_19
	jmp	.LBB1_26
	.p2align	4, 0x90
.LBB1_25:                               # %.preheader.i34
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	decl	%r14d
	jne	.LBB1_25
.LBB1_26:                               # %print_board.exit35
                                        #   in Loop: Header=BB1_4 Depth=2
	movl	$.L.str, %edi
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	16(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %esi
	movl	%r12d, %edx
	callq	printf
	movl	$.L.str.1, %edi
	movl	$136, %esi
	movl	$1, %edx
	movl	$.L.str.2, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
	movl	g_board_size+4(%rip), %ebp
	movl	12(%rsp), %esi          # 4-byte Reload
.LBB1_27:                               #   in Loop: Header=BB1_4 Depth=2
	movslq	%ebp, %rax
	cmpq	%rax, %r12
	leaq	1(%r12), %r12
	jle	.LBB1_4
.LBB1_28:                               # %._crit_edge
                                        #   in Loop: Header=BB1_2 Depth=1
	movslq	g_board_size(%rip), %rax
	cmpq	%rax, %rbx
	leaq	1(%rbx), %rbx
	jle	.LBB1_2
.LBB1_29:                               # %._crit_edge40
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	check_board_sanity, .Lfunc_end1-check_board_sanity
	.cfi_endproc

	.globl	print_board
	.p2align	4, 0x90
	.type	print_board,@function
print_board:                            # @print_board
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 64
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	andl	$1, %r14d
	movl	g_board_size(,%r14,4), %r15d
	testl	%r15d, %r15d
	jle	.LBB2_10
# BB#1:                                 # %.preheader.lr.ph
	movl	%r14d, %eax
	xorl	$1, %eax
	movl	g_board_size(,%rax,4), %r13d
	testl	%r13d, %r13d
	jle	.LBB2_9
# BB#2:                                 # %.preheader.us.preheader
	shlq	$7, %r14
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_3:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_4 Depth 2
	leaq	g_board+4(%r14,%r12,4), %rbx
	incq	%r12
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_4:                                #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %eax
	incl	%ebp
	btl	%ebp, %eax
	jae	.LBB2_6
# BB#5:                                 #   in Loop: Header=BB2_4 Depth=2
	movl	$.L.str.5, %edi
	jmp	.LBB2_7
	.p2align	4, 0x90
.LBB2_6:                                #   in Loop: Header=BB2_4 Depth=2
	movl	$.L.str.6, %edi
.LBB2_7:                                # %.backedge.us
                                        #   in Loop: Header=BB2_4 Depth=2
	xorl	%eax, %eax
	callq	printf
	cmpl	%ebp, %r13d
	jne	.LBB2_4
# BB#8:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	cmpq	%r15, %r12
	jne	.LBB2_3
	jmp	.LBB2_10
	.p2align	4, 0x90
.LBB2_9:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	decl	%r15d
	jne	.LBB2_9
.LBB2_10:                               # %._crit_edge24
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	print_board, .Lfunc_end2-print_board
	.cfi_endproc

	.globl	print_hashentry
	.p2align	4, 0x90
	.type	print_hashentry,@function
print_hashentry:                        # @print_hashentry
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi45:
	.cfi_def_cfa_offset 64
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	g_trans_table(%rip), %rax
	movslq	%edi, %rsi
	leaq	(%rsi,%rsi,2), %rcx
	movl	(%rax,%rcx,8), %edx
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movl	4(%rax,%rcx,8), %edx
	movl	%edx, (%rsp)            # 4-byte Spill
	movl	8(%rax,%rcx,8), %r12d
	movl	12(%rax,%rcx,8), %r13d
	movl	16(%rax,%rcx,8), %r15d
	movzwl	21(%rax,%rcx,8), %edx
	movzbl	23(%rax,%rcx,8), %ebp
	shll	$16, %ebp
	orl	%edx, %ebp
	movzbl	20(%rax,%rcx,8), %r14d
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	movl	%ebp, %r10d
	andl	$127, %r10d
	movl	%ebp, %r11d
	shrl	$7, %r11d
	andl	$1, %r11d
	movl	%ebp, %ebx
	shll	$10, %ebx
	sarl	$18, %ebx
	shrl	$22, %ebp
	subq	$8, %rsp
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.4, %edi
	movl	$0, %eax
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	8(%rsp), %edx           # 4-byte Reload
	movl	%r12d, %ecx
	movl	%r13d, %r8d
	movl	%r15d, %r9d
	pushq	%r14
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$56, %rsp
.Lcfi58:
	.cfi_adjust_cfa_offset -48
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	print_hashentry, .Lfunc_end3-print_hashentry
	.cfi_endproc

	.globl	print_board_info
	.p2align	4, 0x90
	.type	print_board_info,@function
print_board_info:                       # @print_board_info
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 48
	subq	$2576, %rsp             # imm = 0xA10
.Lcfi64:
	.cfi_def_cfa_offset 2624
.Lcfi65:
	.cfi_offset %rbx, -48
.Lcfi66:
	.cfi_offset %r12, -40
.Lcfi67:
	.cfi_offset %r13, -32
.Lcfi68:
	.cfi_offset %r14, -24
.Lcfi69:
	.cfi_offset %r15, -16
	movb	$0, 15(%rsp)
	movl	g_board_size(%rip), %edx
	movl	g_board_size+4(%rip), %ebx
	cmpl	%ebx, %edx
	cmovgel	%edx, %ebx
	leaq	96(%rsp), %r15
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sprintf
	leaq	176(%rsp), %rdi
	movl	g_board_size+4(%rip), %edx
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	15(%rsp), %r14
	movl	$.L.str.10, %edi
	movl	$.L.str.11, %edx
	movl	$.L.str.12, %ecx
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	printf
	movl	$.L.str.13, %edi
	movl	$.L.str.14, %edx
	movl	$.L.str.15, %ecx
	movl	$.L.str.14, %r8d
	movl	$.L.str.15, %r9d
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	printf
	testl	%ebx, %ebx
	jle	.LBB4_3
# BB#1:                                 # %.lr.ph.preheader
	movslq	%ebx, %r12
	movl	$g_info+400, %ebx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %edx
	movl	-4(%rbx), %ecx
	movl	-388(%rbx), %r9d
	movl	-384(%rbx), %r8d
	cmpq	$2, %r13
	leaq	1(%r13), %r13
	movq	%r14, %rax
	cmovlq	%r15, %rax
	movq	%rax, (%rsp)
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	printf
	addq	$12, %rbx
	addq	$80, %r15
	cmpq	%r12, %r13
	jl	.LBB4_2
.LBB4_3:                                # %._crit_edge
	movl	g_info_totals+16(%rip), %esi
	movl	g_info_totals+12(%rip), %edx
	movl	g_info_totals+4(%rip), %ecx
	movl	g_info_totals(%rip), %r8d
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	printf
	addq	$2576, %rsp             # imm = 0xA10
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	print_board_info, .Lfunc_end4-print_board_info
	.cfi_endproc

	.globl	print_bitboard
	.p2align	4, 0x90
	.type	print_bitboard,@function
print_bitboard:                         # @print_bitboard
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 32
.Lcfi73:
	.cfi_offset %rbx, -32
.Lcfi74:
	.cfi_offset %r14, -24
.Lcfi75:
	.cfi_offset %r15, -16
	movl	%edi, %r15d
	andl	$1, %r15d
	cmpl	$-1, g_board_size(,%r15,4)
	jl	.LBB5_3
# BB#1:                                 # %.lr.ph.preheader
	movq	%r15, %rax
	shlq	$7, %rax
	leaq	g_board(%rax), %r14
	movq	$-1, %rbx
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	4(%r14,%rbx,4), %esi
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	printf
	movslq	g_board_size(,%r15,4), %rax
	incq	%rbx
	cmpq	%rax, %rbx
	jle	.LBB5_2
.LBB5_3:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	print_bitboard, .Lfunc_end5-print_bitboard
	.cfi_endproc

	.globl	print_hashkey
	.p2align	4, 0x90
	.type	print_hashkey,@function
print_hashkey:                          # @print_hashkey
	.cfi_startproc
# BB#0:
	movl	8(%rsp), %esi
	movl	12(%rsp), %edx
	movl	16(%rsp), %ecx
	movl	20(%rsp), %r8d
	movl	24(%rsp), %r9d
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	jmp	printf                  # TAILCALL
.Lfunc_end6:
	.size	print_hashkey, .Lfunc_end6-print_hashkey
	.cfi_endproc

	.globl	print_u64bit
	.p2align	4, 0x90
	.type	print_u64bit,@function
print_u64bit:                           # @print_u64bit
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi79:
	.cfi_def_cfa_offset 80
.Lcfi80:
	.cfi_offset %rbx, -32
.Lcfi81:
	.cfi_offset %r14, -24
.Lcfi82:
	.cfi_offset %r15, -16
	xorl	%r14d, %r14d
	movabsq	$2361183241434822607, %rcx # imm = 0x20C49BA5E353F7CF
	.p2align	4, 0x90
.LBB7_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rax
	shrq	$3, %rax
	mulq	%rcx
	shrq	$4, %rdx
	imull	$1000, %edx, %eax       # imm = 0x3E8
	movl	%edi, %ebx
	subl	%eax, %ebx
	movl	%ebx, (%rsp,%r14,4)
	incq	%r14
	cmpq	$999, %rdi              # imm = 0x3E7
	movq	%rdx, %rdi
	ja	.LBB7_1
# BB#2:
	leaq	-1(%r14), %r15
	cmpl	$10, %r15d
	jl	.LBB7_4
# BB#3:                                 # %.thread
	movl	$.L.str.1, %edi
	movl	$255, %esi
	movl	$1, %edx
	movl	$.L.str.20, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	jmp	.LBB7_5
.LBB7_4:
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	cmpl	$1, %r14d
	je	.LBB7_7
.LBB7_5:                                # %.lr.ph.preheader
	movslq	%r15d, %rax
	leaq	-4(%rsp,%rax,4), %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rbx,4), %esi
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	callq	printf
	leal	(%r14,%rbx), %eax
	decq	%rbx
	cmpl	$2, %eax
	jne	.LBB7_6
.LBB7_7:                                # %._crit_edge
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	print_u64bit, .Lfunc_end7-print_u64bit
	.cfi_endproc

	.globl	print_keyinfo
	.p2align	4, 0x90
	.type	print_keyinfo,@function
print_keyinfo:                          # @print_keyinfo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 32
.Lcfi86:
	.cfi_offset %rbx, -32
.Lcfi87:
	.cfi_offset %r14, -24
.Lcfi88:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movl	%esi, %r14d
	movq	%rdi, %rbx
	cmpl	$-1, %ebx
	jne	.LBB8_2
# BB#1:
	movl	$.L.str.1, %edi
	movl	$266, %esi              # imm = 0x10A
	movl	$1, %edx
	movl	$.L.str.23, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
.LBB8_2:
	movq	%rbx, %rdx
	shrq	$32, %rdx
	testl	%ebp, %ebp
	je	.LBB8_4
# BB#3:
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r14d, %ecx
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	printf                  # TAILCALL
.LBB8_4:
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	printf                  # TAILCALL
.Lfunc_end8:
	.size	print_keyinfo, .Lfunc_end8-print_keyinfo
	.cfi_endproc

	.globl	print_keyinfo_table
	.p2align	4, 0x90
	.type	print_keyinfo_table,@function
print_keyinfo_table:                    # @print_keyinfo_table
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi92:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi93:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi95:
	.cfi_def_cfa_offset 64
.Lcfi96:
	.cfi_offset %rbx, -56
.Lcfi97:
	.cfi_offset %r12, -48
.Lcfi98:
	.cfi_offset %r13, -40
.Lcfi99:
	.cfi_offset %r14, -32
.Lcfi100:
	.cfi_offset %r15, -24
.Lcfi101:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movslq	%edi, %rax
	leaq	(%rax,%rax,2), %rax
	shlq	$14, %rax
	leaq	g_keyinfo(%rax), %r13
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB9_1:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_2 Depth 2
	movq	%r13, (%rsp)            # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_2:                                #   Parent Loop BB9_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-1, (%r13)
	je	.LBB9_24
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=2
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	movl	%ebx, %edx
	callq	printf
	movq	(%r13), %rbp
	movl	8(%r13), %r12d
	cmpl	$-1, %ebp
	jne	.LBB9_5
# BB#4:                                 #   in Loop: Header=BB9_2 Depth=2
	movl	$.L.str.1, %edi
	movl	$266, %esi              # imm = 0x10A
	movl	$1, %edx
	movl	$.L.str.23, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
.LBB9_5:                                #   in Loop: Header=BB9_2 Depth=2
	movq	%rbp, %rdx
	shrq	$32, %rdx
	testl	%r15d, %r15d
	je	.LBB9_7
# BB#6:                                 #   in Loop: Header=BB9_2 Depth=2
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r12d, %ecx
	callq	printf
	jmp	.LBB9_8
	.p2align	4, 0x90
.LBB9_7:                                #   in Loop: Header=BB9_2 Depth=2
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	printf
.LBB9_8:                                # %print_keyinfo.exit
                                        #   in Loop: Header=BB9_2 Depth=2
	movq	12(%r13), %rbp
	movl	20(%r13), %r12d
	cmpl	$-1, %ebp
	jne	.LBB9_10
# BB#9:                                 #   in Loop: Header=BB9_2 Depth=2
	movl	$.L.str.1, %edi
	movl	$266, %esi              # imm = 0x10A
	movl	$1, %edx
	movl	$.L.str.23, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
.LBB9_10:                               #   in Loop: Header=BB9_2 Depth=2
	movq	%rbp, %rdx
	shrq	$32, %rdx
	testl	%r15d, %r15d
	je	.LBB9_12
# BB#11:                                #   in Loop: Header=BB9_2 Depth=2
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r12d, %ecx
	callq	printf
	jmp	.LBB9_13
	.p2align	4, 0x90
.LBB9_12:                               #   in Loop: Header=BB9_2 Depth=2
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	printf
.LBB9_13:                               # %print_keyinfo.exit42
                                        #   in Loop: Header=BB9_2 Depth=2
	movq	24(%r13), %rbp
	movl	32(%r13), %r12d
	cmpl	$-1, %ebp
	jne	.LBB9_15
# BB#14:                                #   in Loop: Header=BB9_2 Depth=2
	movl	$.L.str.1, %edi
	movl	$266, %esi              # imm = 0x10A
	movl	$1, %edx
	movl	$.L.str.23, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
.LBB9_15:                               #   in Loop: Header=BB9_2 Depth=2
	movq	%rbp, %rdx
	shrq	$32, %rdx
	testl	%r15d, %r15d
	je	.LBB9_17
# BB#16:                                #   in Loop: Header=BB9_2 Depth=2
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r12d, %ecx
	callq	printf
	jmp	.LBB9_18
	.p2align	4, 0x90
.LBB9_17:                               #   in Loop: Header=BB9_2 Depth=2
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	printf
.LBB9_18:                               # %print_keyinfo.exit46
                                        #   in Loop: Header=BB9_2 Depth=2
	movq	36(%r13), %rbp
	movl	44(%r13), %r12d
	cmpl	$-1, %ebp
	jne	.LBB9_20
# BB#19:                                #   in Loop: Header=BB9_2 Depth=2
	movl	$.L.str.1, %edi
	movl	$266, %esi              # imm = 0x10A
	movl	$1, %edx
	movl	$.L.str.23, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
.LBB9_20:                               #   in Loop: Header=BB9_2 Depth=2
	movq	%rbp, %rdx
	shrq	$32, %rdx
	testl	%r15d, %r15d
	je	.LBB9_22
# BB#21:                                #   in Loop: Header=BB9_2 Depth=2
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r12d, %ecx
	callq	printf
	jmp	.LBB9_23
	.p2align	4, 0x90
.LBB9_22:                               #   in Loop: Header=BB9_2 Depth=2
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	printf
.LBB9_23:                               # %print_keyinfo.exit50
                                        #   in Loop: Header=BB9_2 Depth=2
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
.LBB9_24:                               #   in Loop: Header=BB9_2 Depth=2
	incq	%rbx
	addq	$48, %r13
	cmpq	$32, %rbx
	jne	.LBB9_2
# BB#25:                                #   in Loop: Header=BB9_1 Depth=1
	incq	%r14
	movq	(%rsp), %r13            # 8-byte Reload
	addq	$1536, %r13             # imm = 0x600
	cmpq	$32, %r14
	jne	.LBB9_1
# BB#26:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	print_keyinfo_table, .Lfunc_end9-print_keyinfo_table
	.cfi_endproc

	.globl	print_external
	.p2align	4, 0x90
	.type	print_external,@function
print_external:                         # @print_external
	.cfi_startproc
# BB#0:
	movl	$1, %edi
	movl	$1, %esi
	jmp	print_keyinfo_table     # TAILCALL
.Lfunc_end10:
	.size	print_external, .Lfunc_end10-print_external
	.cfi_endproc

	.globl	print_current_state
	.p2align	4, 0x90
	.type	print_current_state,@function
print_current_state:                    # @print_current_state
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi102:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi103:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi105:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 48
.Lcfi107:
	.cfi_offset %rbx, -48
.Lcfi108:
	.cfi_offset %r12, -40
.Lcfi109:
	.cfi_offset %r14, -32
.Lcfi110:
	.cfi_offset %r15, -24
.Lcfi111:
	.cfi_offset %rbp, -16
	movl	g_board_size(%rip), %r14d
	testl	%r14d, %r14d
	jle	.LBB11_10
# BB#1:                                 # %.preheader.lr.ph.i
	movl	g_board_size+4(%rip), %r12d
	testl	%r12d, %r12d
	jle	.LBB11_9
# BB#2:                                 # %.preheader.us.preheader.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB11_3:                               # %.preheader.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_4 Depth 2
	leaq	1(%rbx), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_4:                               #   Parent Loop BB11_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	g_board+4(,%rbx,4), %eax
	incl	%ebp
	btl	%ebp, %eax
	jae	.LBB11_6
# BB#5:                                 #   in Loop: Header=BB11_4 Depth=2
	movl	$.L.str.5, %edi
	jmp	.LBB11_7
	.p2align	4, 0x90
.LBB11_6:                               #   in Loop: Header=BB11_4 Depth=2
	movl	$.L.str.6, %edi
.LBB11_7:                               # %.backedge.us.i
                                        #   in Loop: Header=BB11_4 Depth=2
	xorl	%eax, %eax
	callq	printf
	cmpl	%ebp, %r12d
	jne	.LBB11_4
# BB#8:                                 # %._crit_edge.us.i
                                        #   in Loop: Header=BB11_3 Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	cmpq	%r14, %r15
	movq	%r15, %rbx
	jne	.LBB11_3
	jmp	.LBB11_10
	.p2align	4, 0x90
.LBB11_9:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	decl	%r14d
	jne	.LBB11_9
.LBB11_10:                              # %print_board.exit
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	callq	print_board_info
	movl	$0, g_print(%rip)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	print_current_state, .Lfunc_end11-print_current_state
	.cfi_endproc

	.globl	current_search_state
	.p2align	4, 0x90
	.type	current_search_state,@function
current_search_state:                   # @current_search_state
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi112:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi113:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi114:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi116:
	.cfi_def_cfa_offset 48
.Lcfi117:
	.cfi_offset %rbx, -40
.Lcfi118:
	.cfi_offset %r12, -32
.Lcfi119:
	.cfi_offset %r14, -24
.Lcfi120:
	.cfi_offset %r15, -16
	movl	$1, g_print(%rip)
	movq	current_search_state.str(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB12_2
# BB#1:
	callq	free
.LBB12_2:
	movq	g_num_nodes(%rip), %rdi
	callq	u64bit_to_string
	movq	%rax, %rdx
	movl	g_move_number+4(%rip), %ecx
	movl	g_move_number+8(%rip), %r8d
	movl	g_move_number+12(%rip), %r9d
	movl	g_move_number+16(%rip), %r10d
	movl	g_move_number+20(%rip), %r11d
	movl	g_move_number+24(%rip), %r14d
	movl	g_move_number+28(%rip), %r15d
	movl	g_move_number+32(%rip), %r12d
	movl	g_move_number+36(%rip), %ebx
	movl	$current_search_state.str, %edi
	movl	$.L.str.27, %esi
	movl	$0, %eax
	pushq	%rbx
.Lcfi121:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi122:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi123:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi124:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi125:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi126:
	.cfi_adjust_cfa_offset 8
	callq	asprintf
	addq	$48, %rsp
.Lcfi127:
	.cfi_adjust_cfa_offset -48
	movq	current_search_state.str(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	current_search_state, .Lfunc_end12-current_search_state
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d %d - %d.\n"
	.size	.L.str, 13

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/obsequi/display.c"
	.size	.L.str.1, 81

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Board is inconsistent.\n"
	.size	.L.str.2, 24

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Hash entry: %d.\n"
	.size	.L.str.3, 17

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	" Key:%8X:%8X:%8X:%8X, n:%u, d:%d, w:%d, v:%d, t:%d, int:%d.\n"
	.size	.L.str.4, 61

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	" #"
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	" 0"
	.size	.L.str.6, 3

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Number of rows    = %d"
	.size	.L.str.8, 23

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Number of columns = %d"
	.size	.L.str.9, 23

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%7s %15s %15s\n"
	.size	.L.str.10, 15

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Vertical"
	.size	.L.str.11, 9

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Horizontal"
	.size	.L.str.12, 11

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"%7s %7s %7s %7s %7s\n"
	.size	.L.str.13, 21

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Real"
	.size	.L.str.14, 5

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Safe"
	.size	.L.str.15, 5

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"%6d) %7d %7d %7d %7d  %s\n"
	.size	.L.str.16, 26

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Totals: %7d %7d %7d %7d\n"
	.size	.L.str.17, 25

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"%X\n"
	.size	.L.str.18, 4

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Key: %8X:%8X:%8X:%8X, Code: %8X.\n"
	.size	.L.str.19, 34

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Too large???\n"
	.size	.L.str.20, 14

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"%d"
	.size	.L.str.21, 3

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	",%3d"
	.size	.L.str.22, 5

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"bit1_index equal to -1"
	.size	.L.str.23, 23

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"%3d:%3d %8X "
	.size	.L.str.24, 13

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"%3d:%3d "
	.size	.L.str.25, 9

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"(%2d,%2d)>>  "
	.size	.L.str.26, 14

	.type	current_search_state.str,@object # @current_search_state.str
	.local	current_search_state.str
	.comm	current_search_state.str,8,8
	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"Nodes: %s.\n%d %d %d %d %d %d %d %d %d."
	.size	.L.str.27, 39

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"HashKey Incorrect.\n"
	.size	.L.str.28, 20

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"Invalid hash code.\n"
	.size	.L.str.29, 20


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
