	.text
	.file	"interp.bc"
	.globl	interp_init
	.p2align	4, 0x90
	.type	interp_init,@function
interp_init:                            # @interp_init
	.cfi_startproc
# BB#0:                                 # %.lr.ph
	movq	$ostack+160, osbot(%rip)
	movl	$ostack, %eax
	movq	$ostack+144, osp(%rip)
	movq	$ostack+8144, ostop(%rip)
	movl	$ostack+160, %ecx
	movl	$ostack, %edx
	notl	%edx
	addl	%ecx, %edx
	shrl	$4, %edx
	incl	%edx
	testb	$7, %dl
	je	.LBB0_3
# BB#1:                                 # %.prol.preheader
	movl	$ostack+160, %edx
	movl	$ostack, %eax
	movl	$ostack, %ecx
	notl	%ecx
	addl	%edx, %ecx
	shrl	$4, %ecx
	incl	%ecx
	andl	$7, %ecx
	negq	%rcx
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movw	$0, (%rax)
	movw	$-4, 8(%rax)
	addq	$16, %rax
	incq	%rcx
	jne	.LBB0_2
.LBB0_3:                                # %.prol.loopexit
	movl	$ostack, %ecx
	notq	%rcx
	leaq	ostack+160(%rcx), %rcx
	shrq	$4, %rcx
	cmpq	$7, %rcx
	jb	.LBB0_6
# BB#4:
	movl	$ostack+160, %ecx
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	movw	$0, (%rax)
	movw	$-4, 8(%rax)
	movw	$0, 16(%rax)
	movw	$-4, 24(%rax)
	movw	$0, 32(%rax)
	movw	$-4, 40(%rax)
	movw	$0, 48(%rax)
	movw	$-4, 56(%rax)
	movw	$0, 64(%rax)
	movw	$-4, 72(%rax)
	movw	$0, 80(%rax)
	movw	$-4, 88(%rax)
	movw	$0, 96(%rax)
	movw	$-4, 104(%rax)
	movw	$0, 112(%rax)
	movw	$-4, 120(%rax)
	subq	$-128, %rax
	cmpq	%rcx, %rax
	jb	.LBB0_5
.LBB0_6:                                # %.preheader
	movl	$ostack+176, %eax
	movd	%rax, %xmm0
	movl	$ostack+160, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, osp_nargs+8(%rip)
	movl	$ostack+208, %eax
	movd	%rax, %xmm0
	movl	$ostack+192, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, osp_nargs+24(%rip)
	movq	$ostack+224, osp_nargs+40(%rip)
	movq	$estack-16, esp(%rip)
	movq	$estack+2384, estop(%rip)
	movslq	%edi, %rax
	shlq	$4, %rax
	leaq	dstack-16(%rax), %rax
	movq	%rax, dsp(%rip)
	movq	$dstack+304, dstop(%rip)
	retq
.Lfunc_end0:
	.size	interp_init, .Lfunc_end0-interp_init
	.cfi_endproc

	.globl	interp_fix_op
	.p2align	4, 0x90
	.type	interp_fix_op,@function
interp_fix_op:                          # @interp_fix_op
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	cmpq	special_ops+48(%rip), %rax
	je	.LBB1_1
# BB#4:
	cmpq	special_ops+40(%rip), %rax
	je	.LBB1_5
# BB#6:
	cmpq	special_ops+32(%rip), %rax
	je	.LBB1_7
# BB#8:
	cmpq	special_ops+24(%rip), %rax
	je	.LBB1_9
# BB#10:
	cmpq	special_ops+16(%rip), %rax
	je	.LBB1_11
# BB#12:
	cmpq	special_ops+8(%rip), %rax
	je	.LBB1_13
# BB#14:
	cmpq	special_ops(%rip), %rax
	jne	.LBB1_3
# BB#15:
	movw	$65, %ax
	jmp	.LBB1_2
.LBB1_1:
	movw	$89, %ax
	jmp	.LBB1_2
.LBB1_5:
	movw	$85, %ax
	jmp	.LBB1_2
.LBB1_7:
	movw	$81, %ax
	jmp	.LBB1_2
.LBB1_9:
	movw	$77, %ax
	jmp	.LBB1_2
.LBB1_11:
	movw	$73, %ax
	jmp	.LBB1_2
.LBB1_13:
	movw	$69, %ax
.LBB1_2:                                # %.critedge
	movw	%ax, 8(%rdi)
.LBB1_3:                                # %.critedge11
	retq
.Lfunc_end1:
	.size	interp_fix_op, .Lfunc_end1-interp_fix_op
	.cfi_endproc

	.globl	interpret
	.p2align	4, 0x90
	.type	interpret,@function
interpret:                              # @interpret
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, 8(%rsp)
	movq	esp(%rip), %rax
	leaq	16(%rax), %rcx
	movq	%rcx, esp(%rip)
	movq	$interp_exit, 16(%rax)
	movw	$37, 24(%rax)
	callq	interp
	movl	%eax, %ebx
	cmpl	$-100, %ebx
	jne	.LBB2_1
.LBB2_13:
	xorl	%ebx, %ebx
.LBB2_16:                               # %._crit_edge
	movl	%ebx, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_1:                                # %.lr.ph
	testl	%ebp, %ebp
	je	.LBB2_14
# BB#2:
	leaq	16(%rsp), %r14
	leaq	24(%rsp), %r15
	leaq	8(%rsp), %r12
	leaq	32(%rsp), %r13
	movl	$5243393, %ebp          # imm = 0x500201
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	osbot(%rip), %rax
	addq	$-16, %rax
	cmpq	%rax, osp(%rip)
	jae	.LBB2_5
# BB#4:                                 #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, osp(%rip)
.LBB2_5:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$dstack, %edi
	movl	$dstack, %esi
	movl	$name_errordict, %edx
	movq	%r14, %rcx
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB2_16
# BB#6:                                 #   in Loop: Header=BB2_3 Depth=1
	movq	16(%rsp), %rdi
	movl	$name_ErrorNames, %edx
	movq	%rdi, %rsi
	movq	%r15, %rcx
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB2_16
# BB#7:                                 #   in Loop: Header=BB2_3 Depth=1
	leal	25(%rbx), %eax
	cmpl	$22, %eax
	ja	.LBB2_9
# BB#8:                                 #   in Loop: Header=BB2_3 Depth=1
	btl	%eax, %ebp
	jb	.LBB2_16
.LBB2_9:                                #   in Loop: Header=BB2_3 Depth=1
	cmpl	$-2, %ebx
	jg	.LBB2_16
# BB#10:                                #   in Loop: Header=BB2_3 Depth=1
	movl	%ebx, %ecx
	negl	%ecx
	movq	24(%rsp), %rax
	movzwl	10(%rax), %edx
	cmpl	%ecx, %edx
	jl	.LBB2_16
# BB#11:                                #   in Loop: Header=BB2_3 Depth=1
	movq	16(%rsp), %rdi
	movl	%ebx, %ecx
	notl	%ecx
	movslq	%ecx, %rdx
	shlq	$4, %rdx
	addq	(%rax), %rdx
	movq	%rdi, %rsi
	movq	%r12, %rcx
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB2_16
# BB#12:                                #   in Loop: Header=BB2_3 Depth=1
	movq	8(%rsp), %rax
	movups	(%rax), %xmm0
	movaps	%xmm0, 32(%rsp)
	movq	%r13, 8(%rsp)
	movq	osp(%rip), %rax
	leaq	16(%rax), %rcx
	movq	%rcx, osp(%rip)
	movups	error_object(%rip), %xmm0
	movups	%xmm0, 16(%rax)
	movq	%r13, %rdi
	callq	interp
	movl	%eax, %ebx
	cmpl	$-100, %ebx
	jne	.LBB2_3
	jmp	.LBB2_13
.LBB2_14:                               # %.lr.ph.split.us
	movq	osbot(%rip), %rax
	addq	$-16, %rax
	cmpq	%rax, osp(%rip)
	jae	.LBB2_16
# BB#15:
	movq	%rax, osp(%rip)
	jmp	.LBB2_16
.Lfunc_end2:
	.size	interpret, .Lfunc_end2-interpret
	.cfi_endproc

	.globl	interp_exit
	.p2align	4, 0x90
	.type	interp_exit,@function
interp_exit:                            # @interp_exit
	.cfi_startproc
# BB#0:
	movl	$-100, %eax
	retq
.Lfunc_end3:
	.size	interp_exit, .Lfunc_end3-interp_exit
	.cfi_endproc

	.globl	interp
	.p2align	4, 0x90
	.type	interp,@function
interp:                                 # @interp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 192
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	esp(%rip), %rbp
	cmpq	estop(%rip), %rbp
	jae	.LBB4_1
# BB#3:
	movq	osp(%rip), %r12
	movups	(%r14), %xmm0
	movups	%xmm0, 16(%rbp)
	addq	$16, %rbp
	movq	%rsp, %r15
	xorl	%r13d, %r13d
	jmp	.LBB4_93
.LBB4_34:                               #   in Loop: Header=BB4_93 Depth=1
	leaq	-16(%rbx), %rbp
	xorl	%eax, %eax
	jmp	.LBB4_6
.LBB4_97:                               #   in Loop: Header=BB4_93 Depth=1
	movq	%rbx, %rcx
.LBB4_5:                                # %.thread.loopexit
                                        #   in Loop: Header=BB4_93 Depth=1
	movq	(%rbx), %rbx
	decl	%eax
	movq	%rcx, %rbp
	jmp	.LBB4_6
.LBB4_35:                               #   in Loop: Header=BB4_6 Depth=2
	movups	(%rcx), %xmm0
	movups	%xmm0, 16(%rbp)
	addq	$16, %rbp
	.p2align	4, 0x90
.LBB4_6:                                # %.thread.preheader
                                        #   Parent Loop BB4_93 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_7 Depth 3
	movq	%rbp, %rdx
	jmp	.LBB4_7
	.p2align	4, 0x90
.LBB4_15:                               #   in Loop: Header=BB4_7 Depth=3
	addq	$16, %r14
	movq	%rbp, %rdx
	movq	%r14, %rbx
.LBB4_7:                                # %.thread
                                        #   Parent Loop BB4_93 Depth=1
                                        #     Parent Loop BB4_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rbx, %r14
	movq	%rdx, %rbp
	movl	%eax, %r13d
	movzbl	8(%r14), %eax
	cmpq	$89, %rax
	ja	.LBB4_90
# BB#8:                                 # %.thread
                                        #   in Loop: Header=BB4_7 Depth=3
	jmpq	*.LJTI4_0(,%rax,8)
.LBB4_76:                               #   in Loop: Header=BB4_7 Depth=3
	xorl	%eax, %eax
	movq	%r14, %rdi
	leaq	16(%rsp), %rsi
	callq	file_check_read
	testl	%eax, %eax
	js	.LBB4_11
# BB#77:                                #   in Loop: Header=BB4_7 Depth=3
	movq	%r12, osp(%rip)
	movq	16(%rsp), %rdi
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%r15, %rdx
	callq	scan_token
	testl	%eax, %eax
	jne	.LBB4_78
# BB#80:                                #   in Loop: Header=BB4_7 Depth=3
	testl	%r13d, %r13d
	jle	.LBB4_82
# BB#81:                                #   in Loop: Header=BB4_7 Depth=3
	leaq	16(%r14), %rax
	movq	%rax, (%rbp)
	movw	%r13w, 10(%rbp)
.LBB4_82:                               #   in Loop: Header=BB4_7 Depth=3
	cmpq	estop(%rip), %rbp
	jae	.LBB4_24
# BB#83:                                # %.thread299
                                        #   in Loop: Header=BB4_7 Depth=3
	movups	(%r14), %xmm0
	movups	%xmm0, 16(%rbp)
	addq	$16, %rbp
	xorl	%eax, %eax
	movq	%rbp, %rdx
	movq	%r15, %rbx
	jmp	.LBB4_7
.LBB4_47:                               #   in Loop: Header=BB4_7 Depth=3
	movq	(%r14), %rax
	movq	24(%rax), %rbx
	cmpq	$1, %rbx
	ja	.LBB4_50
# BB#48:                                #   in Loop: Header=BB4_7 Depth=3
	movq	dsp(%rip), %rsi
	movl	$dstack, %edi
	movq	%r14, %rdx
	leaq	16(%rsp), %rcx
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB4_100
# BB#49:                                # %.thread284
                                        #   in Loop: Header=BB4_7 Depth=3
	movq	16(%rsp), %rbx
.LBB4_50:                               #   in Loop: Header=BB4_7 Depth=3
	movzbl	8(%rbx), %eax
	cmpq	$54, %rax
	ja	.LBB4_70
# BB#51:                                #   in Loop: Header=BB4_7 Depth=3
	movabsq	$18036388743086100, %rcx # imm = 0x40140000100014
	btq	%rax, %rcx
	jae	.LBB4_52
# BB#67:                                #   in Loop: Header=BB4_7 Depth=3
	cmpq	ostop(%rip), %r12
	jae	.LBB4_68
# BB#72:                                #   in Loop: Header=BB4_7 Depth=3
	movups	(%rbx), %xmm0
	movups	%xmm0, 16(%r12)
	addq	$16, %r12
.LBB4_73:                               #   in Loop: Header=BB4_7 Depth=3
	leal	-1(%r13), %eax
	leaq	16(%r14), %rbx
	cmpl	$1, %r13d
	movq	%rbp, %rdx
	jg	.LBB4_7
	jmp	.LBB4_74
.LBB4_39:                               #   in Loop: Header=BB4_7 Depth=3
	movq	%rbp, esp(%rip)
	movq	%r12, osp(%rip)
	movq	%r12, %rdi
	callq	*(%r14)
	movq	osp(%rip), %r12
	testl	%eax, %eax
	je	.LBB4_14
	jmp	.LBB4_40
.LBB4_84:                               #   in Loop: Header=BB4_7 Depth=3
	movq	(%r14), %rsi
	movzwl	10(%r14), %edx
	leaq	16(%rsp), %rbx
	movq	%rbx, %rdi
	callq	sread_string
	movq	%r12, osp(%rip)
	movl	$1, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	scan_token
	testl	%eax, %eax
	jne	.LBB4_85
# BB#86:                                #   in Loop: Header=BB4_7 Depth=3
	testl	%r13d, %r13d
	jle	.LBB4_88
# BB#87:                                #   in Loop: Header=BB4_7 Depth=3
	leaq	16(%r14), %rax
	movq	%rax, (%rbp)
	movw	%r13w, 10(%rbp)
.LBB4_88:                               #   in Loop: Header=BB4_7 Depth=3
	cmpq	estop(%rip), %rbp
	jae	.LBB4_24
# BB#89:                                # %.thread302
                                        #   in Loop: Header=BB4_7 Depth=3
	leaq	16(%rbp), %rdx
	movzwl	8(%r14), %eax
	movw	%ax, 24(%rbp)
	movq	16(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 16(%rbp)
	movl	32(%rsp), %ecx
	addl	40(%rsp), %ecx
	subl	%eax, %ecx
	addl	$65535, %ecx            # imm = 0xFFFF
	movw	%cx, 26(%rbp)
	xorl	%eax, %eax
	movq	%r15, %rbx
	jmp	.LBB4_7
.LBB4_9:                                #   in Loop: Header=BB4_7 Depth=3
	movq	%r12, %rdi
	callq	zadd
	testl	%eax, %eax
	jns	.LBB4_13
	jmp	.LBB4_11
.LBB4_16:                               #   in Loop: Header=BB4_7 Depth=3
	cmpq	osp_nargs+8(%rip), %r12
	jb	.LBB4_17
# BB#18:                                #   in Loop: Header=BB4_7 Depth=3
	movups	(%r12), %xmm0
	movups	%xmm0, 16(%r12)
	leaq	16(%r12), %r12
	jmp	.LBB4_14
.LBB4_19:                               #   in Loop: Header=BB4_7 Depth=3
	cmpq	osp_nargs+16(%rip), %r12
	jb	.LBB4_17
# BB#20:                                #   in Loop: Header=BB4_7 Depth=3
	movups	(%r12), %xmm0
	movaps	%xmm0, (%rsp)
	movups	-16(%r12), %xmm0
	movups	%xmm0, (%r12)
	movaps	(%rsp), %xmm0
	movups	%xmm0, -16(%r12)
	jmp	.LBB4_14
.LBB4_21:                               #   in Loop: Header=BB4_7 Depth=3
	movzwl	-24(%r12), %eax
	andl	$252, %eax
	cmpl	$4, %eax
	jne	.LBB4_22
# BB#23:                                #   in Loop: Header=BB4_7 Depth=3
	cmpq	estop(%rip), %rbp
	jae	.LBB4_24
# BB#25:                                #   in Loop: Header=BB4_7 Depth=3
	testl	%r13d, %r13d
	jle	.LBB4_27
# BB#26:                                #   in Loop: Header=BB4_7 Depth=3
	addq	$16, %r14
	movq	%r14, (%rbp)
	movw	%r13w, 10(%rbp)
.LBB4_27:                               #   in Loop: Header=BB4_7 Depth=3
	xorl	%eax, %eax
	cmpw	$0, -32(%r12)
	sete	%al
	shlq	$4, %rax
	leaq	-16(%r12,%rax), %rcx
	movzbl	-8(%r12,%rax), %eax
	leaq	-48(%r12), %r12
	cmpb	$3, %al
	je	.LBB4_30
# BB#28:                                #   in Loop: Header=BB4_7 Depth=3
	cmpb	$43, %al
	jne	.LBB4_29
.LBB4_30:                               #   in Loop: Header=BB4_7 Depth=3
	movq	(%rcx), %rbx
	movzwl	10(%rcx), %edx
	leal	-1(%rdx), %eax
	cmpl	$1, %edx
	ja	.LBB4_35
# BB#31:                                #   in Loop: Header=BB4_7 Depth=3
	testw	%dx, %dx
	movq	%rbp, %rdx
	movq	%rbp, %rcx
	jne	.LBB4_7
	jmp	.LBB4_32
.LBB4_36:                               #   in Loop: Header=BB4_7 Depth=3
	movl	$3, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	obj_compare
	testl	%eax, %eax
	js	.LBB4_11
# BB#37:                                #   in Loop: Header=BB4_7 Depth=3
	movw	%ax, -16(%r12)
	movw	$4, -8(%r12)
	leaq	-16(%r12), %r12
	jmp	.LBB4_14
.LBB4_12:                               #   in Loop: Header=BB4_7 Depth=3
	cmpq	osp_nargs+8(%rip), %r12
	jae	.LBB4_13
	jmp	.LBB4_17
.LBB4_38:                               #   in Loop: Header=BB4_7 Depth=3
	movq	%r12, %rdi
	callq	zsub
	testl	%eax, %eax
	js	.LBB4_11
.LBB4_13:                               #   in Loop: Header=BB4_7 Depth=3
	addq	$-16, %r12
.LBB4_14:                               #   in Loop: Header=BB4_7 Depth=3
	leal	-1(%r13), %eax
	cmpl	$2, %r13d
	jge	.LBB4_15
	jmp	.LBB4_74
.LBB4_52:                               #   in Loop: Header=BB4_7 Depth=3
	movabsq	$8796093022216, %rcx    # imm = 0x80000000008
	btq	%rax, %rcx
	jae	.LBB4_53
# BB#59:                                #   in Loop: Header=BB4_7 Depth=3
	testl	%r13d, %r13d
	jle	.LBB4_61
# BB#60:                                #   in Loop: Header=BB4_7 Depth=3
	addq	$16, %r14
	movq	%r14, (%rbp)
	movw	%r13w, 10(%rbp)
.LBB4_61:                               #   in Loop: Header=BB4_7 Depth=3
	cmpq	estop(%rip), %rbp
	jae	.LBB4_62
# BB#63:                                #   in Loop: Header=BB4_7 Depth=3
	leaq	16(%rbp), %rcx
	movups	(%rbx), %xmm0
	movups	%xmm0, 16(%rbp)
	movq	(%rbx), %rsi
	movzwl	10(%rbx), %edi
	leal	-1(%rdi), %eax
	cmpl	$1, %edi
	movq	%rcx, %rdx
	movq	%rsi, %rbx
	ja	.LBB4_7
# BB#64:                                #   in Loop: Header=BB4_7 Depth=3
	testw	%di, %di
	movq	%rbp, %rdx
	movq	%rsi, %rbx
	jne	.LBB4_7
	jmp	.LBB4_32
.LBB4_53:                               #   in Loop: Header=BB4_7 Depth=3
	cmpq	$37, %rax
	jne	.LBB4_70
# BB#54:                                #   in Loop: Header=BB4_7 Depth=3
	movq	%rbp, esp(%rip)
	movq	%r12, osp(%rip)
	movq	%r12, %rdi
	callq	*(%rbx)
	movq	osp(%rip), %r12
	testl	%eax, %eax
	je	.LBB4_73
	jmp	.LBB4_55
.LBB4_70:                               #   in Loop: Header=BB4_7 Depth=3
	xorl	%eax, %eax
	testl	%r13d, %r13d
	movq	%rbp, %rdx
	jle	.LBB4_7
# BB#71:                                #   in Loop: Header=BB4_6 Depth=2
	addq	$16, %r14
	movq	%r14, (%rbp)
	movw	%r13w, 10(%rbp)
	jmp	.LBB4_6
.LBB4_29:                               #   in Loop: Header=BB4_6 Depth=2
	leaq	16(%rbp), %rbx
	movups	(%rcx), %xmm0
	movups	%xmm0, 16(%rbp)
	xorl	%eax, %eax
	jmp	.LBB4_6
.LBB4_40:                               #   in Loop: Header=BB4_93 Depth=1
	cmpl	$-20, %eax
	je	.LBB4_46
# BB#41:                                #   in Loop: Header=BB4_93 Depth=1
	cmpl	$1, %eax
	jne	.LBB4_11
# BB#42:                                #   in Loop: Header=BB4_93 Depth=1
	movq	esp(%rip), %rcx
	cmpq	%rbp, %rcx
	jbe	.LBB4_45
.LBB4_43:                               #   in Loop: Header=BB4_93 Depth=1
	testl	%r13d, %r13d
	jle	.LBB4_32
# BB#44:                                #   in Loop: Header=BB4_93 Depth=1
	addq	$16, %r14
	movq	%r14, (%rbp)
	movw	%r13w, 10(%rbp)
	movq	esp(%rip), %rcx
	jmp	.LBB4_32
.LBB4_85:                               #   in Loop: Header=BB4_93 Depth=1
	cmpl	$1, %eax
	jne	.LBB4_11
	jmp	.LBB4_93
.LBB4_78:                               #   in Loop: Header=BB4_93 Depth=1
	cmpl	$1, %eax
	jne	.LBB4_11
# BB#79:                                #   in Loop: Header=BB4_93 Depth=1
	movq	16(%rsp), %rsi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	file_close
	testl	%eax, %eax
	js	.LBB4_11
	.p2align	4, 0x90
.LBB4_93:                               # %.thread296
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_6 Depth 2
                                        #       Child Loop BB4_7 Depth 3
                                        #     Child Loop BB4_32 Depth 2
	leal	-1(%r13), %eax
	cmpl	$2, %r13d
	jl	.LBB4_74
# BB#94:                                #   in Loop: Header=BB4_93 Depth=1
	addq	$16, %r14
	movq	%r14, %rbx
	jmp	.LBB4_6
	.p2align	4, 0x90
.LBB4_74:                               # %.loopexit
                                        #   in Loop: Header=BB4_93 Depth=1
	testl	%eax, %eax
	je	.LBB4_95
# BB#75:                                #   in Loop: Header=BB4_93 Depth=1
	movq	%rbp, %rcx
	jmp	.LBB4_32
	.p2align	4, 0x90
.LBB4_95:                               #   in Loop: Header=BB4_93 Depth=1
	addq	$-16, %rbp
	addq	$16, %r14
	xorl	%eax, %eax
	movq	%r14, %rbx
	jmp	.LBB4_6
.LBB4_90:                               #   in Loop: Header=BB4_93 Depth=1
	cmpq	ostop(%rip), %r12
	jb	.LBB4_92
	jmp	.LBB4_91
.LBB4_45:                               #   in Loop: Header=BB4_93 Depth=1
	jne	.LBB4_32
	jmp	.LBB4_93
.LBB4_55:                               #   in Loop: Header=BB4_93 Depth=1
	cmpl	$-20, %eax
	je	.LBB4_65
# BB#56:                                #   in Loop: Header=BB4_93 Depth=1
	cmpl	$1, %eax
	jne	.LBB4_66
# BB#57:                                #   in Loop: Header=BB4_93 Depth=1
	movq	esp(%rip), %rcx
	cmpq	%rbp, %rcx
	ja	.LBB4_43
# BB#58:                                #   in Loop: Header=BB4_93 Depth=1
	jne	.LBB4_32
	jmp	.LBB4_93
.LBB4_92:                               #   in Loop: Header=BB4_93 Depth=1
	movups	(%r14), %xmm0
	movups	%xmm0, 16(%r12)
	addq	$16, %r12
	jmp	.LBB4_93
	.p2align	4, 0x90
.LBB4_32:                               # %.thread295
                                        #   Parent Loop BB4_93 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rbx
	movzbl	8(%rbx), %eax
	cmpb	$3, %al
	je	.LBB4_96
# BB#33:                                # %.thread295
                                        #   in Loop: Header=BB4_32 Depth=2
	cmpb	$43, %al
	jne	.LBB4_34
.LBB4_96:                               #   in Loop: Header=BB4_32 Depth=2
	movzwl	10(%rbx), %eax
	cmpl	$1, %eax
	ja	.LBB4_97
# BB#4:                                 #   in Loop: Header=BB4_32 Depth=2
	leaq	-16(%rbx), %rcx
	testw	%ax, %ax
	je	.LBB4_32
	jmp	.LBB4_5
.LBB4_98:
	movq	%rbp, esp(%rip)
	movups	(%r14), %xmm0
	movups	%xmm0, error_object(%rip)
	movl	$-7, %eax
	jmp	.LBB4_99
.LBB4_17:
	movq	%rbp, esp(%rip)
	movups	(%r14), %xmm0
	movups	%xmm0, error_object(%rip)
	movl	$-17, %eax
	jmp	.LBB4_99
.LBB4_24:
	movq	%rbp, esp(%rip)
.LBB4_1:
	movups	(%r14), %xmm0
.LBB4_2:                                # %.thread287
	movups	%xmm0, error_object(%rip)
	movl	$-5, %eax
.LBB4_99:                               # %.thread287
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_22:
	movq	%rbp, esp(%rip)
	movups	(%r14), %xmm0
	movups	%xmm0, error_object(%rip)
	movl	$-20, %eax
	jmp	.LBB4_99
.LBB4_68:
	movq	%rbp, esp(%rip)
	movups	(%rbx), %xmm0
.LBB4_69:                               # %.thread287
	movups	%xmm0, error_object(%rip)
	movl	$-16, %eax
	jmp	.LBB4_99
.LBB4_100:
	movq	%rbp, esp(%rip)
	movups	(%r14), %xmm0
	movups	%xmm0, error_object(%rip)
	movl	$-21, %eax
	jmp	.LBB4_99
.LBB4_46:
	movq	osbot(%rip), %rax
	movzwl	10(%r14), %ecx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rax
	cmpq	%rax, %r12
	sbbl	%eax, %eax
	andl	$1, %eax
	leal	-20(%rax,%rax,2), %eax
.LBB4_11:
	movq	%rbp, esp(%rip)
	movups	(%r14), %xmm0
	movups	%xmm0, error_object(%rip)
	jmp	.LBB4_99
.LBB4_62:
	movq	%rbp, esp(%rip)
	movups	(%rbx), %xmm0
	jmp	.LBB4_2
.LBB4_65:
	movq	osbot(%rip), %rax
	movzwl	10(%rbx), %ecx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rax
	cmpq	%rax, %r12
	sbbl	%eax, %eax
	andl	$1, %eax
	leal	-20(%rax,%rax,2), %eax
.LBB4_66:                               # %.loopexit308
	movq	%rbp, esp(%rip)
	movups	(%rbx), %xmm0
	movups	%xmm0, error_object(%rip)
	jmp	.LBB4_99
.LBB4_91:
	movq	%rbp, esp(%rip)
	movups	(%r14), %xmm0
	jmp	.LBB4_69
.Lfunc_end4:
	.size	interp, .Lfunc_end4-interp
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_98
	.quad	.LBB4_98
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_98
	.quad	.LBB4_98
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_98
	.quad	.LBB4_98
	.quad	.LBB4_90
	.quad	.LBB4_76
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_47
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_93
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_39
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_98
	.quad	.LBB4_98
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_98
	.quad	.LBB4_98
	.quad	.LBB4_90
	.quad	.LBB4_84
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_9
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_16
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_19
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_21
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_36
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_12
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_38

	.type	special_ops,@object     # @special_ops
	.data
	.globl	special_ops
	.p2align	4
special_ops:
	.quad	zadd
	.quad	zdup
	.quad	zexch
	.quad	zifelse
	.quad	zle
	.quad	zpop
	.quad	zsub
	.size	special_ops, 56

	.type	ostack,@object          # @ostack
	.comm	ostack,8320,16
	.type	osbot,@object           # @osbot
	.comm	osbot,8,8
	.type	osp,@object             # @osp
	.comm	osp,8,8
	.type	ostop,@object           # @ostop
	.comm	ostop,8,8
	.type	osp_nargs,@object       # @osp_nargs
	.comm	osp_nargs,48,16
	.type	estack,@object          # @estack
	.comm	estack,2400,16
	.type	esp,@object             # @esp
	.comm	esp,8,8
	.type	estop,@object           # @estop
	.comm	estop,8,8
	.type	dstack,@object          # @dstack
	.comm	dstack,320,16
	.type	dsp,@object             # @dsp
	.comm	dsp,8,8
	.type	dstop,@object           # @dstop
	.comm	dstop,8,8
	.type	error_object,@object    # @error_object
	.comm	error_object,16,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
