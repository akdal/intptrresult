	.text
	.file	"scan.bc"
	.globl	scan_buffer
	.p2align	4, 0x90
	.type	scan_buffer,@function
scan_buffer:                            # @scan_buffer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	(%rdi), %r10
	movl	24(%rdi), %r9d
	cmpb	$1, 90(%rsi)
	jne	.LBB0_10
# BB#1:
	movb	88(%rsi), %al
	xorl	%r8d, %r8d
	cmpb	$4, %al
	je	.LBB0_24
# BB#2:
	cmpb	$2, %al
	je	.LBB0_29
# BB#3:
	cmpb	$1, %al
	jne	.LBB0_35
# BB#4:
	movq	80(%rsi), %r11
	movzbl	(%r10), %eax
	incq	%r10
	movq	%rax, %rsi
	shrq	$6, %rsi
	movq	8(%r11,%rsi,8), %rsi
	movl	%eax, %ecx
	andl	$63, %ecx
	movb	(%rsi,%rcx), %bl
	testb	%bl, %bl
	je	.LBB0_61
# BB#5:                                 # %.lr.ph312.preheader
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph312
                                        # =>This Inner Loop Header: Depth=1
	decb	%bl
	movzbl	%bl, %ecx
	leaq	(%rcx,%rcx,4), %rbp
	cmpq	$0, (%r11,%rbp,8)
	je	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_6 Depth=1
	movq	%r10, (%rdi)
	movl	%r9d, 24(%rdi)
	movb	%bl, %r8b
.LBB0_8:                                #   in Loop: Header=BB0_6 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, %al
	sete	%cl
	cmoveq	%r10, %rsi
	addl	%ecx, %r9d
	movzbl	(%r10), %eax
	incq	%r10
	movq	%rax, %rcx
	shrq	$6, %rcx
	leaq	(%r11,%rbp,8), %rbx
	movq	8(%rbx,%rcx,8), %rcx
	movl	%eax, %ebx
	andl	$63, %ebx
	movzbl	(%rcx,%rbx), %ebx
	testb	%bl, %bl
	jne	.LBB0_6
# BB#9:                                 # %._crit_edge
	movzbl	%r8b, %eax
	jmp	.LBB0_66
.LBB0_10:
	movq	80(%rsi), %r8
	movzbl	(%r10), %r14d
	incq	%r10
	movq	%r14, %rbp
	shrq	$6, %rbp
	movq	8(%r8,%rbp,8), %rax
	movl	%r14d, %ecx
	andl	$63, %ecx
	movb	(%rax,%rcx), %al
	testb	%al, %al
	je	.LBB0_18
# BB#11:                                # %.lr.ph386
	movq	96(%rsi), %rbx
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	leaq	8(%rdx), %rbx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	movq	%rbx, (%rsp)            # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_12:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_15 Depth 2
	decb	%al
	movzbl	%al, %r11d
	leaq	(%r11,%r11,4), %r13
	cmpq	$0, (%r8,%r13,8)
	je	.LBB0_17
# BB#13:                                #   in Loop: Header=BB0_12 Depth=1
	movq	%r10, (%rdi)
	movl	%r9d, 24(%rdi)
	movq	%rsi, %r12
	movq	104(%rsi), %rsi
	shlq	$5, %r11
	addq	16(%rsp), %r11          # 8-byte Folded Reload
	movq	(%r11,%rbp,8), %rbp
	movzbl	(%rbp,%rcx), %ecx
	movq	(%rsi,%rcx,8), %rcx
	cmpq	$0, (%rcx)
	je	.LBB0_16
# BB#14:                                # %.lr.ph374.preheader
                                        #   in Loop: Header=BB0_12 Depth=1
	movslq	%ebx, %rsi
	leaq	(%rsi,%rsi,4), %rsi
	movq	8(%rsp), %rbp           # 8-byte Reload
	leaq	(%rbp,%rsi,8), %rbp
	addq	$8, %rcx
	.p2align	4, 0x90
.LBB0_15:                               # %.lr.ph374
                                        #   Parent Loop BB0_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi), %xmm0
	movups	16(%rdi), %xmm1
	movups	%xmm1, 16(%rbp)
	movups	%xmm0, (%rbp)
	movq	-8(%rcx), %rsi
	movq	%rsi, -8(%rbp)
	addq	$40, %rbp
	incl	%ebx
	cmpq	$0, (%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB0_15
.LBB0_16:                               #   in Loop: Header=BB0_12 Depth=1
                                        # kill: %AL<def> %AL<kill> %RAX<def>
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%r12, %rsi
.LBB0_17:                               # %.loopexit301
                                        #   in Loop: Header=BB0_12 Depth=1
	xorl	%eax, %eax
	cmpb	$10, %r14b
	sete	%al
	cmoveq	%r10, %r15
	addl	%eax, %r9d
	movzbl	(%r10), %r14d
	incq	%r10
	movq	%r14, %rbp
	shrq	$6, %rbp
	leaq	(%r8,%r13,8), %rax
	movq	8(%rax,%rbp,8), %rax
	movl	%r14d, %ecx
	andl	$63, %ecx
	movb	(%rax,%rcx), %al
	testb	%al, %al
	jne	.LBB0_12
	jmp	.LBB0_19
.LBB0_18:
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%r15d, %r15d
.LBB0_19:                               # %._crit_edge387
	subl	%r15d, %r10d
	cmpq	$1, %r15
	sbbl	%eax, %eax
	orl	%r10d, %eax
	movl	%eax, 20(%rdi)
	movzbl	(%rsp), %eax            # 1-byte Folded Reload
	leaq	(%rax,%rax,4), %rax
	movq	(%r8,%rax,8), %rax
	testq	%rax, %rax
	je	.LBB0_23
# BB#20:
	movl	%r9d, 24(%rdi)
	cmpq	$0, (%rax)
	je	.LBB0_36
# BB#21:                                # %.lr.ph369
	movq	%rsi, %rbp
	movslq	%ebx, %rcx
	leaq	(%rcx,%rcx,4), %rcx
	leaq	8(%rdx,%rcx,8), %rcx
	addq	$8, %rax
	.p2align	4, 0x90
.LBB0_22:                               # =>This Inner Loop Header: Depth=1
	movups	(%rdi), %xmm0
	movups	16(%rdi), %xmm1
	movups	%xmm1, 16(%rcx)
	movups	%xmm0, (%rcx)
	movq	-8(%rax), %rsi
	movq	%rsi, -8(%rcx)
	addq	$40, %rcx
	incl	%ebx
	cmpq	$0, (%rax)
	leaq	8(%rax), %rax
	jne	.LBB0_22
	jmp	.LBB0_37
.LBB0_23:
	movq	%rsi, %rbp
	testl	%ebx, %ebx
	jne	.LBB0_38
	jmp	.LBB0_51
.LBB0_24:
	movq	80(%rsi), %r11
	movzbl	(%r10), %eax
	incq	%r10
	movq	%rax, %rsi
	shrq	$6, %rsi
	movq	8(%r11,%rsi,8), %rsi
	movl	%eax, %ecx
	andl	$63, %ecx
	movl	(%rsi,%rcx,4), %ebp
	testl	%ebp, %ebp
	je	.LBB0_62
# BB#25:                                # %.lr.ph341.preheader
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_26:                               # %.lr.ph341
                                        # =>This Inner Loop Header: Depth=1
	decl	%ebp
	leaq	(%rbp,%rbp,4), %rbx
	cmpq	$0, (%r11,%rbx,8)
	je	.LBB0_28
# BB#27:                                #   in Loop: Header=BB0_26 Depth=1
	movq	%r10, (%rdi)
	movl	%r9d, 24(%rdi)
	movl	%ebp, %r8d
.LBB0_28:                               #   in Loop: Header=BB0_26 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, %al
	sete	%cl
	cmoveq	%r10, %rsi
	addl	%ecx, %r9d
	movzbl	(%r10), %eax
	incq	%r10
	movq	%rax, %rcx
	shrq	$6, %rcx
	leaq	(%r11,%rbx,8), %rbx
	movq	8(%rbx,%rcx,8), %rcx
	movl	%eax, %ebx
	andl	$63, %ebx
	movl	(%rcx,%rbx,4), %ebp
	testl	%ebp, %ebp
	jne	.LBB0_26
	jmp	.LBB0_63
.LBB0_29:
	movq	80(%rsi), %r11
	movzbl	(%r10), %eax
	incq	%r10
	movq	%rax, %rsi
	shrq	$6, %rsi
	movq	8(%r11,%rsi,8), %rsi
	movl	%eax, %ecx
	andl	$63, %ecx
	movw	(%rsi,%rcx,2), %bx
	testw	%bx, %bx
	je	.LBB0_64
# BB#30:                                # %.lr.ph326.preheader
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_31:                               # %.lr.ph326
                                        # =>This Inner Loop Header: Depth=1
	decl	%ebx
	movzwl	%bx, %ecx
	leaq	(%rcx,%rcx,4), %rbp
	cmpq	$0, (%r11,%rbp,8)
	je	.LBB0_33
# BB#32:                                #   in Loop: Header=BB0_31 Depth=1
	movq	%r10, (%rdi)
	movl	%r9d, 24(%rdi)
	movw	%bx, %r8w
.LBB0_33:                               #   in Loop: Header=BB0_31 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, %al
	sete	%cl
	cmoveq	%r10, %rsi
	addl	%ecx, %r9d
	movzbl	(%r10), %eax
	incq	%r10
	movq	%rax, %rcx
	shrq	$6, %rcx
	leaq	(%r11,%rbp,8), %rbx
	movq	8(%rbx,%rcx,8), %rcx
	movl	%eax, %ebx
	andl	$63, %ebx
	movw	(%rcx,%rbx,2), %bx
	testw	%bx, %bx
	jne	.LBB0_31
	jmp	.LBB0_65
.LBB0_35:
	xorl	%esi, %esi
	jmp	.LBB0_67
.LBB0_36:
	movq	%rsi, %rbp
.LBB0_37:                               # %.loopexit300
	testl	%ebx, %ebx
	je	.LBB0_51
.LBB0_38:
	cmpb	$2, 90(%rbp)
	jne	.LBB0_71
# BB#39:
	leal	-1(%rbx), %eax
	testl	%ebx, %ebx
	jle	.LBB0_52
# BB#40:                                # %.lr.ph359.preheader
	cltq
	leaq	(%rax,%rax,4), %rax
	movq	8(%rdx,%rax,8), %rcx
	movslq	%ebx, %rax
	leaq	(%rax,%rax,4), %rsi
	leaq	-32(%rdx,%rsi,8), %rdi
	xorl	%esi, %esi
	movl	$1, %r8d
	.p2align	4, 0x90
.LBB0_41:                               # %.lr.ph359
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rdi), %rbp
	cmpb	$1, 2(%rbp)
	cmovel	%r8d, %esi
	cmpq	%rcx, (%rdi)
	jb	.LBB0_43
# BB#42:                                #   in Loop: Header=BB0_41 Depth=1
	decq	%rax
	addq	$-40, %rdi
	testq	%rax, %rax
	jg	.LBB0_41
.LBB0_43:                               # %.lr.ph359.._crit_edge360_crit_edge
	decl	%eax
	testl	%esi, %esi
	je	.LBB0_52
# BB#44:                                # %.preheader298
	testl	%ebx, %ebx
	jle	.LBB0_72
# BB#45:                                # %.lr.ph354.preheader
	movl	%ebx, %r8d
	xorl	%esi, %esi
	movq	%rdx, %rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_46:                               # %.lr.ph354
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rcx, 8(%rdi)
	jne	.LBB0_50
# BB#47:                                #   in Loop: Header=BB0_46 Depth=1
	movl	%ebx, %ebp
	cmpq	%rbp, %rsi
	je	.LBB0_49
# BB#48:                                #   in Loop: Header=BB0_46 Depth=1
	movslq	%ebx, %rbp
	leaq	(%rbp,%rbp,4), %rbp
	movq	32(%rdi), %rax
	movq	%rax, 32(%rdx,%rbp,8)
	movups	(%rdi), %xmm0
	movups	16(%rdi), %xmm1
	movups	%xmm1, 16(%rdx,%rbp,8)
	movups	%xmm0, (%rdx,%rbp,8)
.LBB0_49:                               #   in Loop: Header=BB0_46 Depth=1
	incl	%ebx
.LBB0_50:                               #   in Loop: Header=BB0_46 Depth=1
	incq	%rsi
	addq	$40, %rdi
	cmpq	%rsi, %r8
	jne	.LBB0_46
	jmp	.LBB0_71
.LBB0_51:
	xorl	%ebx, %ebx
	jmp	.LBB0_71
.LBB0_52:                               # %.preheader
	testl	%eax, %eax
	js	.LBB0_59
# BB#53:                                # %.lr.ph349.preheader
	movslq	%eax, %rcx
	leaq	(%rcx,%rcx,4), %rsi
	incq	%rcx
	leaq	(%rdx,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB0_54:                               # %.lr.ph349
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rdi
	cmpb	$1, 2(%rdi)
	je	.LBB0_58
# BB#55:                                #   in Loop: Header=BB0_54 Depth=1
	leaq	-1(%rcx), %rdi
	movl	%eax, %ebp
	cmpq	%rbp, %rdi
	je	.LBB0_57
# BB#56:                                #   in Loop: Header=BB0_54 Depth=1
	movslq	%eax, %rdi
	leaq	(%rdi,%rdi,4), %rdi
	movq	32(%rsi), %rbp
	movq	%rbp, 32(%rdx,%rdi,8)
	movups	(%rsi), %xmm0
	movups	16(%rsi), %xmm1
	movups	%xmm1, 16(%rdx,%rdi,8)
	movups	%xmm0, (%rdx,%rdi,8)
.LBB0_57:                               #   in Loop: Header=BB0_54 Depth=1
	decl	%eax
.LBB0_58:                               #   in Loop: Header=BB0_54 Depth=1
	addq	$-40, %rsi
	decq	%rcx
	jg	.LBB0_54
.LBB0_59:                               # %._crit_edge350
	subl	%eax, %ebx
	decl	%ebx
	cmpl	$-1, %eax
	je	.LBB0_71
# BB#60:
	incl	%eax
	cltq
	leaq	(%rax,%rax,4), %rax
	leaq	(%rdx,%rax,8), %rsi
	movslq	%ebx, %rax
	shlq	$3, %rax
	leaq	(%rax,%rax,4), %rax
	movq	%rdx, %rdi
	movq	%rax, %rdx
	callq	memmove
	jmp	.LBB0_71
.LBB0_61:
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	movzbl	%r8b, %eax
	jmp	.LBB0_66
.LBB0_62:
	xorl	%esi, %esi
.LBB0_63:                               # %._crit_edge342
	movl	%r8d, %eax
	jmp	.LBB0_66
.LBB0_64:
	xorl	%esi, %esi
	xorl	%r8d, %r8d
.LBB0_65:                               # %.sink.split
	movzwl	%r8w, %eax
.LBB0_66:                               # %.sink.split
	leaq	(%rax,%rax,4), %rax
	leaq	(%r11,%rax,8), %rax
	movq	(%rax), %r8
.LBB0_67:
	subl	%esi, %r10d
	cmpq	$1, %rsi
	sbbl	%eax, %eax
	orl	%r10d, %eax
	movl	%eax, 20(%rdi)
	xorl	%ebx, %ebx
	testq	%r8, %r8
	je	.LBB0_71
# BB#68:
	movl	%r9d, 24(%rdi)
	cmpq	$0, (%r8)
	je	.LBB0_71
# BB#69:                                # %.lr.ph
	addq	$8, %rdx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_70:                               # =>This Inner Loop Header: Depth=1
	movups	(%rdi), %xmm0
	movups	16(%rdi), %xmm1
	movups	%xmm1, 16(%rdx)
	movups	%xmm0, (%rdx)
	movq	(%r8,%rbx,8), %rax
	movq	%rax, -8(%rdx)
	addq	$40, %rdx
	cmpq	$0, 8(%r8,%rbx,8)
	leaq	1(%rbx), %rbx
	jne	.LBB0_70
.LBB0_71:                               # %.loopexit
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_72:
	xorl	%ebx, %ebx
	jmp	.LBB0_71
.Lfunc_end0:
	.size	scan_buffer, .Lfunc_end0-scan_buffer
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
