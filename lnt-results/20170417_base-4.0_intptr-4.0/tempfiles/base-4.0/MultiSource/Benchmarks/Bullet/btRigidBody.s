	.text
	.file	"btRigidBody.bc"
	.globl	_ZN11btRigidBodyC2ERKNS_27btRigidBodyConstructionInfoE
	.p2align	4, 0x90
	.type	_ZN11btRigidBodyC2ERKNS_27btRigidBodyConstructionInfoE,@function
_ZN11btRigidBodyC2ERKNS_27btRigidBodyConstructionInfoE: # @_ZN11btRigidBodyC2ERKNS_27btRigidBodyConstructionInfoE
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	_ZN17btCollisionObjectC2Ev
	movq	$_ZTV11btRigidBody+16, (%rbx)
	movb	$1, 544(%rbx)
	movq	$0, 536(%rbx)
	movq	$0, 524(%rbx)
.Ltmp0:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN11btRigidBody14setupRigidBodyERKNS_27btRigidBodyConstructionInfoE
.Ltmp1:
# BB#1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_2:
.Ltmp2:
	movq	%rax, %r14
	movq	536(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_6
# BB#3:
	cmpb	$0, 544(%rbx)
	je	.LBB0_5
# BB#4:
.Ltmp3:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
.LBB0_5:                                # %.noexc
	movq	$0, 536(%rbx)
.LBB0_6:
	movb	$1, 544(%rbx)
	movq	$0, 536(%rbx)
	movq	$0, 524(%rbx)
.Ltmp5:
	movq	%rbx, %rdi
	callq	_ZN17btCollisionObjectD2Ev
.Ltmp6:
# BB#7:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_8:
.Ltmp7:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN11btRigidBodyC2ERKNS_27btRigidBodyConstructionInfoE, .Lfunc_end0-_ZN11btRigidBodyC2ERKNS_27btRigidBodyConstructionInfoE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp3           #   Call between .Ltmp3 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	1                       #   On action: 1
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp6      #   Call between .Ltmp6 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN11btRigidBody14setupRigidBodyERKNS_27btRigidBodyConstructionInfoE
	.p2align	4, 0x90
	.type	_ZN11btRigidBody14setupRigidBodyERKNS_27btRigidBodyConstructionInfoE,@function
_ZN11btRigidBody14setupRigidBodyERKNS_27btRigidBodyConstructionInfoE: # @_ZN11btRigidBody14setupRigidBodyERKNS_27btRigidBodyConstructionInfoE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 80
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$2, 256(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 344(%rbx)
	movups	%xmm0, 328(%rbx)
	movl	$1065353216, 364(%rbx)  # imm = 0x3F800000
	movl	$1065353216, 368(%rbx)  # imm = 0x3F800000
	movl	$1065353216, 372(%rbx)  # imm = 0x3F800000
	movl	$0, 376(%rbx)
	movabsq	$4575657222473777152, %rax # imm = 0x3F8000003F800000
	movq	%rax, 380(%rbx)
	movl	$1065353216, 388(%rbx)  # imm = 0x3F800000
	movups	%xmm0, 408(%rbx)
	movups	%xmm0, 392(%rbx)
	movl	$0, 424(%rbx)
	movups	%xmm0, 460(%rbx)
	movups	%xmm0, 444(%rbx)
	movl	$0, 476(%rbx)
	movl	$1056964608, 480(%rbx)  # imm = 0x3F000000
	movl	120(%r14), %eax
	movl	%eax, 504(%rbx)
	movl	124(%r14), %eax
	movl	%eax, 508(%rbx)
	movq	8(%r14), %rdi
	movq	%rdi, 512(%rbx)
	movq	$0, 552(%rbx)
	movb	128(%r14), %al
	movb	%al, 484(%rbx)
	movl	132(%r14), %eax
	movl	%eax, 488(%rbx)
	movl	136(%r14), %eax
	movl	%eax, 492(%rbx)
	movl	140(%r14), %eax
	movl	%eax, 496(%rbx)
	movl	144(%r14), %eax
	movl	%eax, 500(%rbx)
	testq	%rdi, %rdi
	je	.LBB1_2
# BB#1:
	movq	(%rdi), %rax
	leaq	8(%rbx), %r15
	movq	%r15, %rsi
	callq	*16(%rax)
	leaq	24(%rbx), %rax
	leaq	40(%rbx), %rcx
	leaq	56(%rbx), %rdx
	jmp	.LBB1_3
.LBB1_2:
	leaq	8(%rbx), %r15
	movups	16(%r14), %xmm0
	movups	%xmm0, 8(%rbx)
	leaq	24(%rbx), %rax
	movups	32(%r14), %xmm0
	movups	%xmm0, 24(%rbx)
	leaq	40(%rbx), %rcx
	movups	48(%r14), %xmm0
	movups	%xmm0, 40(%rbx)
	leaq	56(%rbx), %rdx
	movups	64(%r14), %xmm0
	movups	%xmm0, 56(%rbx)
.LBB1_3:
	movups	(%r15), %xmm0
	movups	%xmm0, 72(%rbx)
	movups	(%rax), %xmm0
	movups	%xmm0, 88(%rbx)
	movups	(%rcx), %xmm0
	movups	%xmm0, 104(%rbx)
	movups	(%rdx), %xmm0
	movups	%xmm0, 120(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 152(%rbx)
	movups	%xmm0, 136(%rbx)
	movl	112(%r14), %eax
	movl	%eax, 236(%rbx)
	movl	116(%r14), %eax
	movl	%eax, 240(%rbx)
	movq	(%rbx), %rax
	movq	80(%r14), %rsi
	movq	%rbx, %rdi
	callq	*24(%rax)
	movl	_ZL8uniqueId(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, _ZL8uniqueId(%rip)
	movl	%eax, 560(%rbx)
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movl	216(%rbx), %eax
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm1
	jne	.LBB1_5
	jp	.LBB1_5
# BB#4:
	orl	$1, %eax
	movl	%eax, 216(%rbx)
	jmp	.LBB1_6
.LBB1_5:
	andl	$-2, %eax
	movl	%eax, 216(%rbx)
	movss	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
.LBB1_6:                                # %_ZN11btRigidBody12setMassPropsEfRK9btVector3.exit
	movss	%xmm0, 360(%rbx)
	movss	88(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	92(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	xorl	%eax, %eax
	xorps	%xmm0, %xmm0
	ucomiss	%xmm2, %xmm0
	movss	.LCPI1_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	divss	%xmm2, %xmm4
	movd	%xmm4, %edx
	movl	$0, %ecx
	cmovnel	%edx, %ecx
	cmovpl	%edx, %ecx
	ucomiss	%xmm3, %xmm0
	movaps	%xmm1, %xmm2
	divss	%xmm3, %xmm2
	movd	%xmm2, %esi
	movl	$0, %edx
	cmovnel	%esi, %edx
	cmovpl	%esi, %edx
	movss	96(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	movaps	%xmm1, %xmm3
	divss	%xmm2, %xmm3
	movd	%xmm3, %esi
	cmovnel	%esi, %eax
	cmovpl	%esi, %eax
	movl	%ecx, 428(%rbx)
	movl	%edx, 432(%rbx)
	movl	%eax, 436(%rbx)
	movl	$0, 440(%rbx)
	movss	104(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	108(%r14), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	%xmm2, 44(%rsp)
	movss	%xmm3, 40(%rsp)
	movl	$0, 16(%rsp)
	movl	$1065353216, 12(%rsp)   # imm = 0x3F800000
	ucomiss	%xmm1, %xmm2
	leaq	12(%rsp), %r8
	leaq	44(%rsp), %rdi
	cmovaq	%r8, %rdi
	ucomiss	%xmm2, %xmm0
	leaq	16(%rsp), %rsi
	cmovaq	%rsi, %rdi
	movl	(%rdi), %edi
	movl	%edi, 476(%rbx)
	movl	$0, 16(%rsp)
	movl	$1065353216, 12(%rsp)   # imm = 0x3F800000
	ucomiss	%xmm1, %xmm3
	leaq	40(%rsp), %rdi
	cmovaq	%r8, %rdi
	ucomiss	%xmm3, %xmm0
	cmovaq	%rsi, %rdi
	movl	(%rdi), %esi
	movl	%esi, 480(%rbx)
	movd	%ecx, %xmm11
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	12(%rbx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm14
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	mulss	%xmm11, %xmm14
	movd	%edx, %xmm4
	movaps	%xmm9, %xmm15
	mulss	%xmm4, %xmm15
	movss	16(%rbx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movd	%eax, %xmm6
	movaps	%xmm13, %xmm10
	mulss	%xmm6, %xmm10
	movss	24(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 28(%rsp)         # 4-byte Spill
	movss	28(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	32(%rbx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movss	40(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	%xmm3, 24(%rsp)         # 4-byte Spill
	movaps	%xmm0, %xmm5
	mulss	%xmm14, %xmm5
	movaps	%xmm9, %xmm0
	mulss	%xmm15, %xmm0
	addss	%xmm5, %xmm0
	movaps	%xmm13, %xmm2
	mulss	%xmm10, %xmm2
	addss	%xmm0, %xmm2
	movss	%xmm2, 36(%rsp)         # 4-byte Spill
	movaps	%xmm14, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm15, %xmm5
	mulss	%xmm8, %xmm5
	addss	%xmm0, %xmm5
	movaps	%xmm10, %xmm0
	mulss	%xmm12, %xmm0
	addss	%xmm5, %xmm0
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	movss	44(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 20(%rsp)         # 4-byte Spill
	mulss	%xmm3, %xmm14
	mulss	%xmm0, %xmm15
	addss	%xmm14, %xmm15
	movss	48(%rbx), %xmm14        # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm10
	addss	%xmm15, %xmm10
	movaps	%xmm11, %xmm2
	mulss	%xmm1, %xmm2
	movaps	%xmm4, %xmm3
	mulss	%xmm8, %xmm3
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movaps	%xmm9, %xmm5
	mulss	%xmm3, %xmm5
	addss	%xmm0, %xmm5
	movaps	%xmm6, %xmm0
	mulss	%xmm12, %xmm0
	movaps	%xmm13, %xmm15
	mulss	%xmm0, %xmm15
	addss	%xmm5, %xmm15
	movaps	%xmm1, %xmm5
	mulss	%xmm2, %xmm5
	movaps	%xmm8, %xmm7
	mulss	%xmm3, %xmm7
	addss	%xmm5, %xmm7
	movaps	%xmm12, %xmm5
	mulss	%xmm0, %xmm5
	addss	%xmm7, %xmm5
	movss	24(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	movss	20(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	addss	%xmm2, %xmm3
	mulss	%xmm14, %xmm0
	addss	%xmm3, %xmm0
	mulss	%xmm7, %xmm11
	mulss	%xmm1, %xmm4
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm2
	mulss	%xmm4, %xmm9
	addss	%xmm2, %xmm9
	mulss	%xmm14, %xmm6
	mulss	%xmm6, %xmm13
	addss	%xmm9, %xmm13
	movss	28(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm2
	mulss	%xmm4, %xmm8
	addss	%xmm2, %xmm8
	mulss	%xmm6, %xmm12
	addss	%xmm8, %xmm12
	mulss	%xmm7, %xmm11
	mulss	%xmm1, %xmm4
	addss	%xmm11, %xmm4
	mulss	%xmm14, %xmm6
	addss	%xmm4, %xmm6
	movss	36(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 280(%rbx)
	movss	32(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 284(%rbx)
	movss	%xmm10, 288(%rbx)
	movl	$0, 292(%rbx)
	movss	%xmm15, 296(%rbx)
	movss	%xmm5, 300(%rbx)
	movss	%xmm0, 304(%rbx)
	movl	$0, 308(%rbx)
	movss	%xmm13, 312(%rbx)
	movss	%xmm12, 316(%rbx)
	movss	%xmm6, 320(%rbx)
	movl	$0, 324(%rbx)
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	_ZN11btRigidBody14setupRigidBodyERKNS_27btRigidBodyConstructionInfoE, .Lfunc_end1-_ZN11btRigidBody14setupRigidBodyERKNS_27btRigidBodyConstructionInfoE
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	0                       # float 0
	.long	0                       # float 0
	.long	1056964608              # float 0.5
	.long	0                       # float 0
.LCPI3_1:
	.long	1000593162              # float 0.00499999989
	.long	1008981770              # float 0.00999999977
	.long	1008981770              # float 0.00999999977
	.long	1008981770              # float 0.00999999977
	.text
	.globl	_ZN11btRigidBodyC2EfP13btMotionStateP16btCollisionShapeRK9btVector3
	.p2align	4, 0x90
	.type	_ZN11btRigidBodyC2EfP13btMotionStateP16btCollisionShapeRK9btVector3,@function
_ZN11btRigidBodyC2EfP13btMotionStateP16btCollisionShapeRK9btVector3: # @_ZN11btRigidBodyC2EfP13btMotionStateP16btCollisionShapeRK9btVector3
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 40
	subq	$168, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 208
.Lcfi17:
	.cfi_offset %rbx, -40
.Lcfi18:
	.cfi_offset %r12, -32
.Lcfi19:
	.cfi_offset %r14, -24
.Lcfi20:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	%rdi, %rbx
	callq	_ZN17btCollisionObjectC2Ev
	movq	$_ZTV11btRigidBody+16, (%rbx)
	movb	$1, 544(%rbx)
	movq	$0, 536(%rbx)
	movq	$0, 524(%rbx)
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)
	movq	%r12, 24(%rsp)
	movq	%r15, 96(%rsp)
	movups	(%r14), %xmm0
	movups	%xmm0, 104(%rsp)
	movaps	.LCPI3_0(%rip), %xmm0   # xmm0 = [0.000000e+00,0.000000e+00,5.000000e-01,0.000000e+00]
	movups	%xmm0, 120(%rsp)
	movl	$1061997773, 136(%rsp)  # imm = 0x3F4CCCCD
	movl	$1065353216, 140(%rsp)  # imm = 0x3F800000
	movb	$0, 144(%rsp)
	movaps	.LCPI3_1(%rip), %xmm0   # xmm0 = [5.000000e-03,1.000000e-02,1.000000e-02,1.000000e-02]
	movups	%xmm0, 148(%rsp)
	movl	$1065353216, 32(%rsp)   # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, 36(%rsp)
	movl	$1065353216, 52(%rsp)   # imm = 0x3F800000
	movups	%xmm0, 56(%rsp)
	movl	$1065353216, 72(%rsp)   # imm = 0x3F800000
	movups	%xmm0, 76(%rsp)
	movl	$0, 92(%rsp)
.Ltmp8:
	leaq	16(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_ZN11btRigidBody14setupRigidBodyERKNS_27btRigidBodyConstructionInfoE
.Ltmp9:
# BB#1:
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB3_2:
.Ltmp10:
	movq	%rax, %r14
	movq	536(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_6
# BB#3:
	cmpb	$0, 544(%rbx)
	je	.LBB3_5
# BB#4:
.Ltmp11:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp12:
.LBB3_5:                                # %.noexc
	movq	$0, 536(%rbx)
.LBB3_6:
	movb	$1, 544(%rbx)
	movq	$0, 536(%rbx)
	movq	$0, 524(%rbx)
.Ltmp13:
	movq	%rbx, %rdi
	callq	_ZN17btCollisionObjectD2Ev
.Ltmp14:
# BB#7:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_8:
.Ltmp15:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN11btRigidBodyC2EfP13btMotionStateP16btCollisionShapeRK9btVector3, .Lfunc_end3-_ZN11btRigidBodyC2EfP13btMotionStateP16btCollisionShapeRK9btVector3
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp8-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin1   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp14-.Ltmp11         #   Call between .Ltmp11 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin1   #     jumps to .Ltmp15
	.byte	1                       #   On action: 1
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp14     #   Call between .Ltmp14 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN11btRigidBody12setMassPropsEfRK9btVector3
	.p2align	4, 0x90
	.type	_ZN11btRigidBody12setMassPropsEfRK9btVector3,@function
_ZN11btRigidBody12setMassPropsEfRK9btVector3: # @_ZN11btRigidBody12setMassPropsEfRK9btVector3
	.cfi_startproc
# BB#0:
	movl	216(%rdi), %eax
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB4_2
	jp	.LBB4_2
# BB#1:
	orl	$1, %eax
	movl	%eax, 216(%rdi)
	jmp	.LBB4_3
.LBB4_2:
	andl	$-2, %eax
	movl	%eax, 216(%rdi)
	movss	.LCPI4_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
.LBB4_3:
	movss	%xmm1, 360(%rdi)
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	xorl	%eax, %eax
	xorps	%xmm2, %xmm2
	ucomiss	%xmm0, %xmm2
	movss	.LCPI4_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm4
	divss	%xmm0, %xmm4
	movd	%xmm4, %ecx
	movl	$0, %edx
	cmovnel	%ecx, %edx
	cmovpl	%ecx, %edx
	ucomiss	%xmm1, %xmm2
	movaps	%xmm3, %xmm0
	divss	%xmm1, %xmm0
	movd	%xmm0, %r8d
	movl	$0, %ecx
	cmovnel	%r8d, %ecx
	cmovpl	%r8d, %ecx
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm2
	divss	%xmm0, %xmm3
	movd	%xmm3, %esi
	cmovnel	%esi, %eax
	cmovpl	%esi, %eax
	movl	%edx, 428(%rdi)
	movl	%ecx, 432(%rdi)
	movl	%eax, 436(%rdi)
	movl	$0, 440(%rdi)
	retq
.Lfunc_end4:
	.size	_ZN11btRigidBody12setMassPropsEfRK9btVector3, .Lfunc_end4-_ZN11btRigidBody12setMassPropsEfRK9btVector3
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN11btRigidBody10setDampingEff
	.p2align	4, 0x90
	.type	_ZN11btRigidBody10setDampingEff,@function
_ZN11btRigidBody10setDampingEff:        # @_ZN11btRigidBody10setDampingEff
	.cfi_startproc
# BB#0:                                 # %_Z11GEN_clampedIfERKT_S2_S2_S2_.exit
	movss	%xmm0, -4(%rsp)
	movss	%xmm1, -8(%rsp)
	movl	$0, -12(%rsp)
	movl	$1065353216, -16(%rsp)  # imm = 0x3F800000
	movss	.LCPI5_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	leaq	-16(%rsp), %rax
	leaq	-4(%rsp), %rcx
	cmovaq	%rax, %rcx
	xorps	%xmm3, %xmm3
	ucomiss	%xmm0, %xmm3
	leaq	-12(%rsp), %rdx
	cmovaq	%rdx, %rcx
	movl	(%rcx), %ecx
	movl	%ecx, 476(%rdi)
	movl	$0, -12(%rsp)
	movl	$1065353216, -16(%rsp)  # imm = 0x3F800000
	ucomiss	%xmm2, %xmm1
	leaq	-8(%rsp), %rcx
	cmovaq	%rax, %rcx
	ucomiss	%xmm1, %xmm3
	cmovaq	%rdx, %rcx
	movl	(%rcx), %eax
	movl	%eax, 480(%rdi)
	retq
.Lfunc_end5:
	.size	_ZN11btRigidBody10setDampingEff, .Lfunc_end5-_ZN11btRigidBody10setDampingEff
	.cfi_endproc

	.globl	_ZN11btRigidBody19updateInertiaTensorEv
	.p2align	4, 0x90
	.type	_ZN11btRigidBody19updateInertiaTensorEv,@function
_ZN11btRigidBody19updateInertiaTensorEv: # @_ZN11btRigidBody19updateInertiaTensorEv
	.cfi_startproc
# BB#0:
	movss	428(%rdi), %xmm11       # xmm11 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	12(%rdi), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm14
	movaps	%xmm0, %xmm1
	movss	%xmm1, -24(%rsp)        # 4-byte Spill
	mulss	%xmm11, %xmm14
	movss	432(%rdi), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm0
	mulss	%xmm4, %xmm0
	movss	16(%rdi), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movss	436(%rdi), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm15
	mulss	%xmm6, %xmm15
	movss	24(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	%xmm3, -12(%rsp)        # 4-byte Spill
	movss	28(%rdi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	32(%rdi), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movss	40(%rdi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	%xmm8, -16(%rsp)        # 4-byte Spill
	movaps	%xmm14, %xmm5
	mulss	%xmm1, %xmm5
	movaps	%xmm0, %xmm2
	mulss	%xmm9, %xmm2
	addss	%xmm5, %xmm2
	movaps	%xmm15, %xmm1
	mulss	%xmm13, %xmm1
	addss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsp)         # 4-byte Spill
	movaps	%xmm14, %xmm2
	mulss	%xmm3, %xmm2
	movaps	%xmm0, %xmm5
	mulss	%xmm7, %xmm5
	addss	%xmm2, %xmm5
	movaps	%xmm15, %xmm1
	mulss	%xmm12, %xmm1
	addss	%xmm5, %xmm1
	movss	%xmm1, -8(%rsp)         # 4-byte Spill
	movss	44(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, -20(%rsp)        # 4-byte Spill
	mulss	%xmm8, %xmm14
	mulss	%xmm1, %xmm0
	addss	%xmm14, %xmm0
	movss	48(%rdi), %xmm14        # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm15
	addss	%xmm0, %xmm15
	movaps	%xmm11, %xmm8
	mulss	%xmm3, %xmm8
	movaps	%xmm4, %xmm1
	mulss	%xmm7, %xmm1
	movaps	%xmm8, %xmm0
	mulss	-24(%rsp), %xmm0        # 4-byte Folded Reload
	movaps	%xmm1, %xmm2
	mulss	%xmm9, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm6, %xmm0
	mulss	%xmm12, %xmm0
	movaps	%xmm0, %xmm10
	mulss	%xmm13, %xmm10
	addss	%xmm2, %xmm10
	movaps	%xmm8, %xmm2
	mulss	%xmm3, %xmm2
	movaps	%xmm1, %xmm3
	mulss	%xmm7, %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm0, %xmm5
	mulss	%xmm12, %xmm5
	addss	%xmm3, %xmm5
	movss	-16(%rsp), %xmm3        # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm8
	movss	-20(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	addss	%xmm8, %xmm1
	mulss	%xmm14, %xmm0
	addss	%xmm1, %xmm0
	mulss	%xmm3, %xmm11
	mulss	%xmm2, %xmm4
	movss	-24(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm1
	mulss	%xmm4, %xmm9
	addss	%xmm1, %xmm9
	mulss	%xmm14, %xmm6
	mulss	%xmm6, %xmm13
	addss	%xmm9, %xmm13
	movss	-12(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm1
	mulss	%xmm4, %xmm7
	addss	%xmm1, %xmm7
	mulss	%xmm6, %xmm12
	addss	%xmm7, %xmm12
	mulss	%xmm3, %xmm11
	mulss	%xmm2, %xmm4
	addss	%xmm11, %xmm4
	mulss	%xmm14, %xmm6
	addss	%xmm4, %xmm6
	movss	-4(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 280(%rdi)
	movss	-8(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 284(%rdi)
	movss	%xmm15, 288(%rdi)
	movl	$0, 292(%rdi)
	movss	%xmm10, 296(%rdi)
	movss	%xmm5, 300(%rdi)
	movss	%xmm0, 304(%rdi)
	movl	$0, 308(%rdi)
	movss	%xmm13, 312(%rdi)
	movss	%xmm12, 316(%rdi)
	movss	%xmm6, 320(%rdi)
	movl	$0, 324(%rdi)
	retq
.Lfunc_end6:
	.size	_ZN11btRigidBody19updateInertiaTensorEv, .Lfunc_end6-_ZN11btRigidBody19updateInertiaTensorEv
	.cfi_endproc

	.globl	_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform
	.p2align	4, 0x90
	.type	_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform,@function
_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform: # @_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform
	.cfi_startproc
# BB#0:
	movq	%rsi, %rax
	leaq	8(%rdi), %rcx
	leaq	328(%rdi), %rsi
	leaq	344(%rdi), %rdx
	movq	%rcx, %rdi
	movq	%rax, %rcx
	jmp	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_ # TAILCALL
.Lfunc_end7:
	.size	_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform, .Lfunc_end7-_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI8_0:
	.long	1061752795              # float 0.785398185
.LCPI8_1:
	.long	981668463               # float 0.00100000005
.LCPI8_2:
	.long	1056964608              # float 0.5
.LCPI8_3:
	.long	3165301419              # float -0.020833334
.LCPI8_4:
	.long	1065353216              # float 1
.LCPI8_5:
	.long	1073741824              # float 2
	.section	.text._ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_,"axG",@progbits,_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_,comdat
	.weak	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
	.p2align	4, 0x90
	.type	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_,@function
_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_: # @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 32
	subq	$64, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 96
.Lcfi25:
	.cfi_offset %rbx, -32
.Lcfi26:
	.cfi_offset %r14, -24
.Lcfi27:
	.cfi_offset %r15, -16
	movq	%rcx, %r15
	movaps	%xmm0, %xmm3
	movq	%rdx, %rbx
	movq	%rdi, %r14
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	movaps	%xmm3, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm0, %xmm1
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	movsd	48(%r14), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	addss	56(%r14), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, 48(%r15)
	movlps	%xmm1, 56(%r15)
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm2, %xmm2
	sqrtss	%xmm0, %xmm2
	ucomiss	%xmm2, %xmm2
	jnp	.LBB8_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	movaps	%xmm0, %xmm2
.LBB8_2:                                # %.split
	movaps	%xmm2, %xmm0
	mulss	%xmm3, %xmm0
	ucomiss	.LCPI8_0(%rip), %xmm0
	jbe	.LBB8_4
# BB#3:                                 # %select.true.sink
	movss	.LCPI8_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	divss	%xmm3, %xmm2
.LBB8_4:                                # %select.end
	movss	.LCPI8_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	jbe	.LBB8_6
# BB#5:
	movss	.LCPI8_2(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	movaps	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm3, %xmm0
	mulss	.LCPI8_3(%rip), %xmm0
	mulss	%xmm2, %xmm0
	mulss	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	jmp	.LBB8_7
.LBB8_6:
	movss	.LCPI8_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	mulss	%xmm3, %xmm0
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	movss	%xmm2, 8(%rsp)          # 4-byte Spill
	callq	sinf
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	divss	%xmm2, %xmm0
.LBB8_7:
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 8(%rsp)          # 4-byte Spill
	mulss	8(%rbx), %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	mulss	%xmm3, %xmm2
	mulss	.LCPI8_2(%rip), %xmm2
	movaps	%xmm2, %xmm0
	callq	cosf
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	leaq	48(%rsp), %rsi
	movq	%r14, %rdi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movsd	48(%rsp), %xmm12        # xmm12 = mem[0],zero
	movq	56(%rsp), %xmm9         # xmm9 = mem[0],zero
	movss	16(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm3
	mulss	%xmm12, %xmm3
	pshufd	$229, %xmm9, %xmm8      # xmm8 = xmm9[1,1,2,3]
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm4
	mulss	%xmm8, %xmm4
	addss	%xmm3, %xmm4
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm7
	mulss	%xmm9, %xmm7
	addss	%xmm4, %xmm7
	pshufd	$229, %xmm12, %xmm11    # xmm11 = xmm12[1,1,2,3]
	movaps	32(%rsp), %xmm3         # 16-byte Reload
	movaps	%xmm3, %xmm4
	mulss	%xmm11, %xmm4
	subss	%xmm4, %xmm7
	movaps	%xmm1, %xmm4
	movaps	%xmm1, %xmm13
	mulss	%xmm11, %xmm4
	movaps	%xmm2, %xmm5
	mulss	%xmm8, %xmm5
	addss	%xmm4, %xmm5
	movaps	%xmm3, %xmm6
	mulss	%xmm12, %xmm6
	addss	%xmm5, %xmm6
	movaps	%xmm0, %xmm4
	mulss	%xmm9, %xmm4
	subss	%xmm4, %xmm6
	movaps	%xmm13, %xmm4
	mulss	%xmm9, %xmm4
	movaps	%xmm3, %xmm5
	movaps	%xmm3, %xmm1
	mulss	%xmm8, %xmm5
	addss	%xmm4, %xmm5
	movaps	%xmm0, %xmm10
	mulss	%xmm11, %xmm10
	addss	%xmm5, %xmm10
	movaps	%xmm2, %xmm4
	mulss	%xmm12, %xmm4
	subss	%xmm4, %xmm10
	movaps	%xmm13, %xmm3
	mulss	%xmm8, %xmm3
	movaps	%xmm0, %xmm4
	mulss	%xmm12, %xmm4
	subss	%xmm4, %xmm3
	mulss	%xmm11, %xmm2
	subss	%xmm2, %xmm3
	mulss	%xmm9, %xmm1
	subss	%xmm1, %xmm3
	movaps	%xmm7, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm10, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB8_9
# BB#8:                                 # %call.sqrt113
	movaps	%xmm1, %xmm0
	movss	%xmm7, 8(%rsp)          # 4-byte Spill
	movss	%xmm6, 12(%rsp)         # 4-byte Spill
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	movss	%xmm10, 32(%rsp)        # 4-byte Spill
	callq	sqrtf
	movss	32(%rsp), %xmm10        # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm7          # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
.LBB8_9:                                # %.split112
	movss	.LCPI8_4(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm2
	divss	%xmm0, %xmm2
	mulss	%xmm2, %xmm7
	mulss	%xmm2, %xmm6
	mulss	%xmm2, %xmm10
	mulss	%xmm3, %xmm2
	movaps	%xmm7, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm6, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm10, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm0, %xmm3
	movss	.LCPI8_5(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	divss	%xmm3, %xmm4
	movaps	%xmm7, %xmm0
	mulss	%xmm4, %xmm0
	movaps	%xmm6, %xmm3
	mulss	%xmm4, %xmm3
	mulss	%xmm10, %xmm4
	movaps	%xmm2, %xmm9
	mulss	%xmm0, %xmm9
	movaps	%xmm2, %xmm11
	mulss	%xmm3, %xmm11
	mulss	%xmm4, %xmm2
	mulss	%xmm7, %xmm0
	movaps	%xmm7, %xmm5
	mulss	%xmm3, %xmm5
	mulss	%xmm4, %xmm7
	mulss	%xmm6, %xmm3
	mulss	%xmm4, %xmm6
	mulss	%xmm10, %xmm4
	movaps	%xmm3, %xmm1
	addss	%xmm4, %xmm1
	movaps	%xmm8, %xmm10
	subss	%xmm1, %xmm10
	movaps	%xmm5, %xmm12
	subss	%xmm2, %xmm12
	movaps	%xmm7, %xmm1
	addss	%xmm11, %xmm1
	addss	%xmm2, %xmm5
	addss	%xmm0, %xmm4
	movaps	%xmm8, %xmm2
	subss	%xmm4, %xmm2
	movaps	%xmm6, %xmm4
	subss	%xmm9, %xmm4
	subss	%xmm11, %xmm7
	addss	%xmm9, %xmm6
	addss	%xmm0, %xmm3
	subss	%xmm3, %xmm8
	movss	%xmm10, (%r15)
	movss	%xmm12, 4(%r15)
	movss	%xmm1, 8(%r15)
	movl	$0, 12(%r15)
	movss	%xmm5, 16(%r15)
	movss	%xmm2, 20(%r15)
	movss	%xmm4, 24(%r15)
	movl	$0, 28(%r15)
	movss	%xmm7, 32(%r15)
	movss	%xmm6, 36(%r15)
	movss	%xmm8, 40(%r15)
	movl	$0, 44(%r15)
	addq	$64, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_, .Lfunc_end8-_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI9_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN11btRigidBody18saveKinematicStateEf
	.p2align	4, 0x90
	.type	_ZN11btRigidBody18saveKinematicStateEf,@function
_ZN11btRigidBody18saveKinematicStateEf: # @_ZN11btRigidBody18saveKinematicStateEf
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 24
	subq	$72, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 96
.Lcfi31:
	.cfi_offset %rbx, -24
.Lcfi32:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB9_1
	jnp	.LBB9_5
.LBB9_1:
	movq	512(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_2
# BB#3:
	movq	(%rdi), %rax
	leaq	8(%rbx), %r14
	movq	%r14, %rsi
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	callq	*16(%rax)
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB9_4
.LBB9_2:                                # %._crit_edge
	leaq	8(%rbx), %r14
.LBB9_4:
	leaq	72(%rbx), %rdi
	movsd	56(%rbx), %xmm3         # xmm3 = mem[0],zero
	movsd	120(%rbx), %xmm1        # xmm1 = mem[0],zero
	subps	%xmm1, %xmm3
	movss	64(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	128(%rbx), %xmm1
	movss	.LCPI9_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm2
	movaps	%xmm2, 16(%rsp)         # 16-byte Spill
	movaps	%xmm2, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	mulps	%xmm0, %xmm3
	mulss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm3, 328(%rbx)
	movlps	%xmm2, 336(%rbx)
	leaq	32(%rsp), %rdx
	leaq	12(%rsp), %rcx
	movq	%r14, %rsi
	callq	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movsd	32(%rsp), %xmm1         # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm1, %xmm2
	mulss	40(%rsp), %xmm0
	mulps	48(%rsp), %xmm2         # 16-byte Folded Reload
	mulss	16(%rsp), %xmm0         # 16-byte Folded Reload
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, 344(%rbx)
	movlps	%xmm1, 352(%rbx)
	movups	328(%rbx), %xmm0
	movups	%xmm0, 136(%rbx)
	movups	344(%rbx), %xmm0
	movups	%xmm0, 152(%rbx)
	movups	(%r14), %xmm0
	movups	%xmm0, 72(%rbx)
	movups	24(%rbx), %xmm0
	movups	%xmm0, 88(%rbx)
	movups	40(%rbx), %xmm0
	movups	%xmm0, 104(%rbx)
	movups	56(%rbx), %xmm0
	movups	%xmm0, 120(%rbx)
.LBB9_5:
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	_ZN11btRigidBody18saveKinematicStateEf, .Lfunc_end9-_ZN11btRigidBody18saveKinematicStateEf
	.cfi_endproc

	.globl	_ZNK11btRigidBody7getAabbER9btVector3S1_
	.p2align	4, 0x90
	.type	_ZNK11btRigidBody7getAabbER9btVector3S1_,@function
_ZNK11btRigidBody7getAabbER9btVector3S1_: # @_ZNK11btRigidBody7getAabbER9btVector3S1_
	.cfi_startproc
# BB#0:
	movq	%rdx, %r8
	movq	%rsi, %rcx
	movq	200(%rdi), %rdx
	movq	(%rdx), %rsi
	movq	16(%rsi), %rax
	leaq	8(%rdi), %rsi
	movq	%rdx, %rdi
	movq	%rcx, %rdx
	movq	%r8, %rcx
	jmpq	*%rax                   # TAILCALL
.Lfunc_end10:
	.size	_ZNK11btRigidBody7getAabbER9btVector3S1_, .Lfunc_end10-_ZNK11btRigidBody7getAabbER9btVector3S1_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI11_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN11btRigidBody10setGravityERK9btVector3
	.p2align	4, 0x90
	.type	_ZN11btRigidBody10setGravityERK9btVector3,@function
_ZN11btRigidBody10setGravityERK9btVector3: # @_ZN11btRigidBody10setGravityERK9btVector3
	.cfi_startproc
# BB#0:
	movss	360(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB11_1
	jnp	.LBB11_2
.LBB11_1:
	movss	.LCPI11_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	movaps	%xmm1, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm0, %xmm2
	mulss	8(%rsi), %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movlps	%xmm2, 396(%rdi)
	movlps	%xmm0, 404(%rdi)
.LBB11_2:
	movups	(%rsi), %xmm0
	movups	%xmm0, 412(%rdi)
	retq
.Lfunc_end11:
	.size	_ZN11btRigidBody10setGravityERK9btVector3, .Lfunc_end11-_ZN11btRigidBody10setGravityERK9btVector3
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI12_0:
	.long	1065353216              # float 1
.LCPI12_1:
	.long	1000593162              # float 0.00499999989
	.text
	.globl	_ZN11btRigidBody12applyDampingEf
	.p2align	4, 0x90
	.type	_ZN11btRigidBody12applyDampingEf,@function
_ZN11btRigidBody12applyDampingEf:       # @_ZN11btRigidBody12applyDampingEf
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -16
	movaps	%xmm0, %xmm1
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movq	%rdi, %rbx
	movss	.LCPI12_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	subss	476(%rbx), %xmm0
	callq	powf
	movss	328(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 328(%rbx)
	movss	332(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 332(%rbx)
	mulss	336(%rbx), %xmm0
	movss	%xmm0, 336(%rbx)
	movss	.LCPI12_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	subss	480(%rbx), %xmm0
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	callq	powf
	movaps	%xmm0, %xmm1
	movss	344(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	movss	%xmm4, 344(%rbx)
	movss	348(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	movss	%xmm5, 348(%rbx)
	mulss	352(%rbx), %xmm1
	movss	%xmm1, 352(%rbx)
	cmpb	$0, 484(%rbx)
	je	.LBB12_21
# BB#1:
	movaps	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm5, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm1, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	movss	328(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	496(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm3
	jbe	.LBB12_2
# BB#3:
	movaps	%xmm2, %xmm6
	mulss	%xmm6, %xmm6
	movss	332(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	336(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm7
	mulss	%xmm7, %xmm7
	addss	%xmm6, %xmm7
	movaps	%xmm0, %xmm6
	mulss	%xmm6, %xmm6
	addss	%xmm7, %xmm6
	movss	492(%rbx), %xmm7        # xmm7 = mem[0],zero,zero,zero
	ucomiss	%xmm6, %xmm7
	jbe	.LBB12_5
# BB#4:
	movss	488(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm4
	movss	%xmm4, 344(%rbx)
	mulss	%xmm6, %xmm5
	movss	%xmm5, 348(%rbx)
	mulss	%xmm6, %xmm1
	movss	%xmm1, 352(%rbx)
	mulss	%xmm6, %xmm2
	movss	%xmm2, 328(%rbx)
	mulss	%xmm6, %xmm3
	movss	%xmm3, 332(%rbx)
	mulss	%xmm6, %xmm0
	movss	%xmm0, 336(%rbx)
	jmp	.LBB12_5
.LBB12_2:                               # %._crit_edge
	movss	332(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	336(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
.LBB12_5:
	mulss	%xmm2, %xmm2
	mulss	%xmm3, %xmm3
	addss	%xmm2, %xmm3
	mulss	%xmm0, %xmm0
	addss	%xmm3, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB12_7
# BB#6:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB12_7:                               # %.split
	movss	476(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jbe	.LBB12_13
# BB#8:
	ucomiss	.LCPI12_1(%rip), %xmm1
	jbe	.LBB12_12
# BB#9:
	movss	328(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	332(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	336(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB12_11
# BB#10:                                # %call.sqrt68
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB12_11:                              # %.split67
	movss	.LCPI12_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	328(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	332(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm3
	mulss	%xmm0, %xmm3
	movaps	%xmm0, %xmm4
	mulss	%xmm2, %xmm4
	movss	336(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm0
	movss	.LCPI12_1(%rip), %xmm6  # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm3
	mulss	%xmm6, %xmm4
	mulss	%xmm6, %xmm0
	subss	%xmm3, %xmm1
	movss	%xmm1, 328(%rbx)
	subss	%xmm4, %xmm2
	movss	%xmm2, 332(%rbx)
	subss	%xmm0, %xmm5
	movss	%xmm5, 336(%rbx)
	jmp	.LBB12_13
.LBB12_12:
	leaq	328(%rbx), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
.LBB12_13:
	movss	344(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	348(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movss	352(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB12_15
# BB#14:                                # %call.sqrt70
	movaps	%xmm1, %xmm0
	callq	sqrtf
.LBB12_15:                              # %.split69
	movss	480(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB12_21
# BB#16:
	ucomiss	.LCPI12_1(%rip), %xmm0
	jbe	.LBB12_20
# BB#17:
	movss	344(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	348(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	352(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB12_19
# BB#18:                                # %call.sqrt72
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB12_19:                              # %.split71
	movss	.LCPI12_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm6
	divss	%xmm1, %xmm6
	movss	344(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	348(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	mulss	%xmm6, %xmm2
	movaps	%xmm6, %xmm3
	mulss	%xmm1, %xmm3
	movss	352(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm6
	movss	.LCPI12_1(%rip), %xmm5  # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	mulss	%xmm5, %xmm3
	mulss	%xmm5, %xmm6
	subss	%xmm2, %xmm0
	movss	%xmm0, 344(%rbx)
	subss	%xmm3, %xmm1
	movss	%xmm1, 348(%rbx)
	subss	%xmm6, %xmm4
	movss	%xmm4, 352(%rbx)
	jmp	.LBB12_21
.LBB12_20:
	addq	$344, %rbx              # imm = 0x158
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
.LBB12_21:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end12:
	.size	_ZN11btRigidBody12applyDampingEf, .Lfunc_end12-_ZN11btRigidBody12applyDampingEf
	.cfi_endproc

	.globl	_ZN11btRigidBody12applyGravityEv
	.p2align	4, 0x90
	.type	_ZN11btRigidBody12applyGravityEv,@function
_ZN11btRigidBody12applyGravityEv:       # @_ZN11btRigidBody12applyGravityEv
	.cfi_startproc
# BB#0:
	testb	$3, 216(%rdi)
	jne	.LBB13_2
# BB#1:
	movss	396(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	400(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	380(%rdi), %xmm0
	mulss	384(%rdi), %xmm1
	movss	404(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	388(%rdi), %xmm2
	addss	444(%rdi), %xmm0
	movss	%xmm0, 444(%rdi)
	addss	448(%rdi), %xmm1
	movss	%xmm1, 448(%rdi)
	addss	452(%rdi), %xmm2
	movss	%xmm2, 452(%rdi)
.LBB13_2:
	retq
.Lfunc_end13:
	.size	_ZN11btRigidBody12applyGravityEv, .Lfunc_end13-_ZN11btRigidBody12applyGravityEv
	.cfi_endproc

	.globl	_ZN11btRigidBody18proceedToTransformERK11btTransform
	.p2align	4, 0x90
	.type	_ZN11btRigidBody18proceedToTransformERK11btTransform,@function
_ZN11btRigidBody18proceedToTransformERK11btTransform: # @_ZN11btRigidBody18proceedToTransformERK11btTransform
	.cfi_startproc
# BB#0:
	jmp	_ZN11btRigidBody24setCenterOfMassTransformERK11btTransform # TAILCALL
.Lfunc_end14:
	.size	_ZN11btRigidBody18proceedToTransformERK11btTransform, .Lfunc_end14-_ZN11btRigidBody18proceedToTransformERK11btTransform
	.cfi_endproc

	.globl	_ZN11btRigidBody24setCenterOfMassTransformERK11btTransform
	.p2align	4, 0x90
	.type	_ZN11btRigidBody24setCenterOfMassTransformERK11btTransform,@function
_ZN11btRigidBody24setCenterOfMassTransformERK11btTransform: # @_ZN11btRigidBody24setCenterOfMassTransformERK11btTransform
	.cfi_startproc
# BB#0:
	testb	$3, 216(%rdi)
	je	.LBB15_2
# BB#1:
	leaq	8(%rdi), %r8
	movups	8(%rdi), %xmm0
	movups	%xmm0, 72(%rdi)
	leaq	24(%rdi), %r9
	movups	24(%rdi), %xmm0
	movups	%xmm0, 88(%rdi)
	leaq	40(%rdi), %r11
	movups	40(%rdi), %xmm0
	movups	%xmm0, 104(%rdi)
	leaq	56(%rdi), %rdx
	movups	56(%rdi), %xmm0
	movups	%xmm0, 120(%rdi)
	leaq	32(%rsi), %r10
	leaq	16(%rsi), %rax
	movq	%rsi, %rcx
	addq	$48, %rcx
	jmp	.LBB15_3
.LBB15_2:
	movups	(%rsi), %xmm0
	movups	%xmm0, 72(%rdi)
	movups	16(%rsi), %xmm0
	movups	%xmm0, 88(%rdi)
	leaq	32(%rsi), %r10
	movups	32(%rsi), %xmm0
	movups	%xmm0, 104(%rdi)
	leaq	48(%rsi), %rcx
	movups	48(%rsi), %xmm0
	leaq	16(%rsi), %rax
	movups	%xmm0, 120(%rdi)
	leaq	8(%rdi), %r8
	leaq	24(%rdi), %r9
	leaq	40(%rdi), %r11
	leaq	56(%rdi), %rdx
.LBB15_3:
	movups	328(%rdi), %xmm0
	movups	%xmm0, 136(%rdi)
	movups	344(%rdi), %xmm0
	movups	%xmm0, 152(%rdi)
	movups	(%rsi), %xmm0
	movups	%xmm0, (%r8)
	movups	(%rax), %xmm0
	movups	%xmm0, (%r9)
	movups	(%r10), %xmm0
	movups	%xmm0, (%r11)
	movups	(%rcx), %xmm0
	movups	%xmm0, (%rdx)
	movss	428(%rdi), %xmm11       # xmm11 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	12(%rdi), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm14
	movss	%xmm0, -24(%rsp)        # 4-byte Spill
	mulss	%xmm11, %xmm14
	movss	432(%rdi), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm15
	mulss	%xmm4, %xmm15
	movss	16(%rdi), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movss	436(%rdi), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm10
	mulss	%xmm6, %xmm10
	movss	24(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, -12(%rsp)        # 4-byte Spill
	movss	28(%rdi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	32(%rdi), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movss	40(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	%xmm3, -16(%rsp)        # 4-byte Spill
	movaps	%xmm0, %xmm5
	mulss	%xmm14, %xmm5
	movaps	%xmm9, %xmm0
	mulss	%xmm15, %xmm0
	addss	%xmm5, %xmm0
	movaps	%xmm13, %xmm2
	mulss	%xmm10, %xmm2
	addss	%xmm0, %xmm2
	movss	%xmm2, -4(%rsp)         # 4-byte Spill
	movaps	%xmm14, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm15, %xmm5
	mulss	%xmm8, %xmm5
	addss	%xmm0, %xmm5
	movaps	%xmm10, %xmm0
	mulss	%xmm12, %xmm0
	addss	%xmm5, %xmm0
	movss	%xmm0, -8(%rsp)         # 4-byte Spill
	movss	44(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, -20(%rsp)        # 4-byte Spill
	mulss	%xmm3, %xmm14
	mulss	%xmm0, %xmm15
	addss	%xmm14, %xmm15
	movss	48(%rdi), %xmm14        # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm10
	addss	%xmm15, %xmm10
	movaps	%xmm11, %xmm2
	mulss	%xmm1, %xmm2
	movaps	%xmm4, %xmm3
	mulss	%xmm8, %xmm3
	movss	-24(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movaps	%xmm9, %xmm5
	mulss	%xmm3, %xmm5
	addss	%xmm0, %xmm5
	movaps	%xmm6, %xmm0
	mulss	%xmm12, %xmm0
	movaps	%xmm13, %xmm15
	mulss	%xmm0, %xmm15
	addss	%xmm5, %xmm15
	movaps	%xmm1, %xmm5
	mulss	%xmm2, %xmm5
	movaps	%xmm8, %xmm7
	mulss	%xmm3, %xmm7
	addss	%xmm5, %xmm7
	movaps	%xmm12, %xmm5
	mulss	%xmm0, %xmm5
	addss	%xmm7, %xmm5
	movss	-16(%rsp), %xmm7        # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	movss	-20(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	addss	%xmm2, %xmm3
	mulss	%xmm14, %xmm0
	addss	%xmm3, %xmm0
	mulss	%xmm7, %xmm11
	mulss	%xmm1, %xmm4
	movss	-24(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm2
	mulss	%xmm4, %xmm9
	addss	%xmm2, %xmm9
	mulss	%xmm14, %xmm6
	mulss	%xmm6, %xmm13
	addss	%xmm9, %xmm13
	movss	-12(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm2
	mulss	%xmm4, %xmm8
	addss	%xmm2, %xmm8
	mulss	%xmm6, %xmm12
	addss	%xmm8, %xmm12
	mulss	%xmm7, %xmm11
	mulss	%xmm1, %xmm4
	addss	%xmm11, %xmm4
	mulss	%xmm14, %xmm6
	addss	%xmm4, %xmm6
	movss	-4(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 280(%rdi)
	movss	-8(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 284(%rdi)
	movss	%xmm10, 288(%rdi)
	movl	$0, 292(%rdi)
	movss	%xmm15, 296(%rdi)
	movss	%xmm5, 300(%rdi)
	movss	%xmm0, 304(%rdi)
	movl	$0, 308(%rdi)
	movss	%xmm13, 312(%rdi)
	movss	%xmm12, 316(%rdi)
	movss	%xmm6, 320(%rdi)
	movl	$0, 324(%rdi)
	retq
.Lfunc_end15:
	.size	_ZN11btRigidBody24setCenterOfMassTransformERK11btTransform, .Lfunc_end15-_ZN11btRigidBody24setCenterOfMassTransformERK11btTransform
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI16_0:
	.long	1070141403              # float 1.57079637
	.text
	.globl	_ZN11btRigidBody19integrateVelocitiesEf
	.p2align	4, 0x90
	.type	_ZN11btRigidBody19integrateVelocitiesEf,@function
_ZN11btRigidBody19integrateVelocitiesEf: # @_ZN11btRigidBody19integrateVelocitiesEf
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -16
	movaps	%xmm0, %xmm6
	movq	%rdi, %rbx
	testb	$3, 216(%rbx)
	jne	.LBB16_5
# BB#1:
	movss	360(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm0
	movss	444(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	448(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	mulss	452(%rbx), %xmm0
	addss	328(%rbx), %xmm1
	movss	%xmm1, 328(%rbx)
	addss	332(%rbx), %xmm2
	movss	%xmm2, 332(%rbx)
	addss	336(%rbx), %xmm0
	movss	%xmm0, 336(%rbx)
	movss	460(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	464(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	280(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	movss	284(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	addss	%xmm0, %xmm2
	movss	468(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	288(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	%xmm2, %xmm1
	movss	296(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	movss	300(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm2, %xmm5
	movss	304(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	addss	%xmm5, %xmm2
	mulss	312(%rbx), %xmm4
	mulss	316(%rbx), %xmm3
	addss	%xmm4, %xmm3
	mulss	320(%rbx), %xmm0
	addss	%xmm3, %xmm0
	mulss	%xmm6, %xmm1
	mulss	%xmm6, %xmm2
	mulss	%xmm6, %xmm0
	addss	344(%rbx), %xmm1
	movss	%xmm1, 344(%rbx)
	addss	348(%rbx), %xmm2
	movss	%xmm2, 348(%rbx)
	addss	352(%rbx), %xmm0
	movss	%xmm0, 352(%rbx)
	mulss	%xmm1, %xmm1
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB16_3
# BB#2:                                 # %call.sqrt
	movss	%xmm6, 12(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	12(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB16_3:                               # %.split
	movaps	%xmm1, %xmm0
	mulss	%xmm6, %xmm0
	ucomiss	.LCPI16_0(%rip), %xmm0
	jbe	.LBB16_5
# BB#4:
	movss	.LCPI16_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm6, %xmm0
	divss	%xmm1, %xmm0
	movss	344(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 344(%rbx)
	movss	348(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 348(%rbx)
	mulss	352(%rbx), %xmm0
	movss	%xmm0, 352(%rbx)
.LBB16_5:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end16:
	.size	_ZN11btRigidBody19integrateVelocitiesEf, .Lfunc_end16-_ZN11btRigidBody19integrateVelocitiesEf
	.cfi_endproc

	.globl	_ZNK11btRigidBody14getOrientationEv
	.p2align	4, 0x90
	.type	_ZNK11btRigidBody14getOrientationEv,@function
_ZNK11btRigidBody14getOrientationEv:    # @_ZNK11btRigidBody14getOrientationEv
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 32
	addq	$8, %rdi
	leaq	8(%rsp), %rsi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movsd	8(%rsp), %xmm0          # xmm0 = mem[0],zero
	movsd	16(%rsp), %xmm1         # xmm1 = mem[0],zero
	addq	$24, %rsp
	retq
.Lfunc_end17:
	.size	_ZNK11btRigidBody14getOrientationEv, .Lfunc_end17-_ZNK11btRigidBody14getOrientationEv
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI18_0:
	.long	1065353216              # float 1
.LCPI18_1:
	.long	1056964608              # float 0.5
	.section	.text._ZNK11btMatrix3x311getRotationER12btQuaternion,"axG",@progbits,_ZNK11btMatrix3x311getRotationER12btQuaternion,comdat
	.weak	_ZNK11btMatrix3x311getRotationER12btQuaternion
	.p2align	4, 0x90
	.type	_ZNK11btMatrix3x311getRotationER12btQuaternion,@function
_ZNK11btMatrix3x311getRotationER12btQuaternion: # @_ZNK11btMatrix3x311getRotationER12btQuaternion
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 80
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	20(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	addss	%xmm2, %xmm1
	movss	40(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm1
	xorps	%xmm4, %xmm4
	ucomiss	%xmm4, %xmm1
	jbe	.LBB18_4
# BB#1:
	addss	.LCPI18_0(%rip), %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB18_3
# BB#2:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movq	%rsi, %rbp
	callq	sqrtf
	movq	%rbp, %rsi
.LBB18_3:                               # %.split
	movss	.LCPI18_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	32(%rbx), %xmm2
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movaps	%xmm1, %xmm3
	divss	%xmm0, %xmm3
	movss	36(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	24(%rbx), %xmm1
	movss	16(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	4(%rbx), %xmm4
	movaps	%xmm3, %xmm5
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	unpcklps	%xmm3, %xmm3    # xmm3 = xmm3[0,0,1,1]
	unpcklps	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	unpcklps	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	mulps	%xmm3, %xmm1
	movaps	%xmm1, (%rsp)
	jmp	.LBB18_7
.LBB18_4:
	xorl	%eax, %eax
	ucomiss	%xmm0, %xmm2
	seta	%al
	maxss	%xmm0, %xmm2
	ucomiss	%xmm2, %xmm3
	movl	$2, %r15d
	cmovbel	%eax, %r15d
	leal	1(%r15), %eax
	movl	$2863311531, %ecx       # imm = 0xAAAAAAAB
	imulq	%rcx, %rax
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	negl	%eax
	leal	1(%r15,%rax), %edx
	movl	%r15d, %eax
	addl	$2, %eax
	imulq	%rcx, %rax
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	negl	%eax
	leal	2(%r15,%rax), %r14d
	movq	%r15, %rbp
	shlq	$4, %rbp
	addq	%rbx, %rbp
	movss	(%rbp,%r15,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	%rdx, %r12
	shlq	$4, %r12
	addq	%rbx, %r12
	subss	(%r12,%rdx,4), %xmm0
	movq	%r14, %r13
	shlq	$4, %r13
	addq	%rbx, %r13
	subss	(%r13,%r14,4), %xmm0
	addss	.LCPI18_0(%rip), %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB18_6
# BB#5:                                 # %call.sqrt72
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %rbx
	callq	sqrtf
	movq	%rbx, %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movaps	%xmm0, %xmm1
.LBB18_6:                               # %.split71
	movss	.LCPI18_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	mulss	%xmm0, %xmm2
	movss	%xmm2, (%rsp,%r15,4)
	divss	%xmm1, %xmm0
	movss	(%r13,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	subss	(%r12,%r14,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, 12(%rsp)
	movss	(%r12,%r15,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	(%rbp,%rdx,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsp,%rdx,4)
	movss	(%r13,%r15,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	(%rbp,%r14,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsp,%r14,4)
	movaps	(%rsp), %xmm1
.LBB18_7:
	movups	%xmm1, (%rsi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	_ZNK11btMatrix3x311getRotationER12btQuaternion, .Lfunc_end18-_ZNK11btMatrix3x311getRotationER12btQuaternion
	.cfi_endproc

	.text
	.globl	_ZN11btRigidBody24checkCollideWithOverrideEP17btCollisionObject
	.p2align	4, 0x90
	.type	_ZN11btRigidBody24checkCollideWithOverrideEP17btCollisionObject,@function
_ZN11btRigidBody24checkCollideWithOverrideEP17btCollisionObject: # @_ZN11btRigidBody24checkCollideWithOverrideEP17btCollisionObject
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	movb	$1, %al
	je	.LBB19_8
# BB#1:
	cmpl	$2, 256(%rsi)
	jne	.LBB19_8
# BB#2:                                 # %.preheader
	movslq	524(%rdi), %rcx
	testq	%rcx, %rcx
	jle	.LBB19_8
# BB#3:                                 # %.lr.ph
	movq	536(%rdi), %r8
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB19_5:                               # =>This Inner Loop Header: Depth=1
	movq	(%r8,%rdi,8), %rdx
	cmpq	%rsi, 24(%rdx)
	je	.LBB19_7
# BB#6:                                 #   in Loop: Header=BB19_5 Depth=1
	cmpq	%rsi, 32(%rdx)
	je	.LBB19_7
# BB#4:                                 #   in Loop: Header=BB19_5 Depth=1
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB19_5
	jmp	.LBB19_8
.LBB19_7:
	xorl	%eax, %eax
.LBB19_8:                               # %.thread
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end19:
	.size	_ZN11btRigidBody24checkCollideWithOverrideEP17btCollisionObject, .Lfunc_end19-_ZN11btRigidBody24checkCollideWithOverrideEP17btCollisionObject
	.cfi_endproc

	.globl	_ZN11btRigidBody16addConstraintRefEP17btTypedConstraint
	.p2align	4, 0x90
	.type	_ZN11btRigidBody16addConstraintRefEP17btTypedConstraint,@function
_ZN11btRigidBody16addConstraintRefEP17btTypedConstraint: # @_ZN11btRigidBody16addConstraintRefEP17btTypedConstraint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi57:
	.cfi_def_cfa_offset 48
.Lcfi58:
	.cfi_offset %rbx, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movslq	524(%r15), %rax
	testq	%rax, %rax
	jle	.LBB20_5
# BB#1:                                 # %.lr.ph.i
	movl	%eax, %ecx
	movq	536(%r15), %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB20_2:                               # =>This Inner Loop Header: Depth=1
	cmpq	%r14, (%rsi,%rdx,8)
	je	.LBB20_4
# BB#3:                                 #   in Loop: Header=BB20_2 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB20_2
	jmp	.LBB20_5
.LBB20_4:                               # %_ZNK20btAlignedObjectArrayIP17btTypedConstraintE16findLinearSearchERKS1_.exit
	cmpl	%edx, %ecx
	jne	.LBB20_21
.LBB20_5:                               # %_ZNK20btAlignedObjectArrayIP17btTypedConstraintE16findLinearSearchERKS1_.exit.thread
	cmpl	528(%r15), %eax
	jne	.LBB20_20
# BB#6:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%ecx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB20_20
# BB#7:
	testl	%ebp, %ebp
	je	.LBB20_8
# BB#9:
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movl	524(%r15), %eax
	testl	%eax, %eax
	jg	.LBB20_11
	jmp	.LBB20_15
.LBB20_8:
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.LBB20_15
.LBB20_11:                              # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB20_13
	.p2align	4, 0x90
.LBB20_12:                              # =>This Inner Loop Header: Depth=1
	movq	536(%r15), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB20_12
.LBB20_13:                              # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB20_15
	.p2align	4, 0x90
.LBB20_14:                              # =>This Inner Loop Header: Depth=1
	movq	536(%r15), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	movq	536(%r15), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%rbx,%rdx,8)
	movq	536(%r15), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%rbx,%rdx,8)
	movq	536(%r15), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%rbx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB20_14
.LBB20_15:                              # %_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4copyEiiPS1_.exit.i.i
	movq	536(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB20_19
# BB#16:
	cmpb	$0, 544(%r15)
	je	.LBB20_18
# BB#17:
	callq	_Z21btAlignedFreeInternalPv
	movl	524(%r15), %eax
.LBB20_18:
	movq	$0, 536(%r15)
.LBB20_19:                              # %_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv.exit.i.i
	movb	$1, 544(%r15)
	movq	%rbx, 536(%r15)
	movl	%ebp, 528(%r15)
.LBB20_20:                              # %_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_.exit
	movq	536(%r15), %rcx
	cltq
	movq	%r14, (%rcx,%rax,8)
	incl	%eax
	movl	%eax, 524(%r15)
.LBB20_21:
	movb	$1, 272(%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	_ZN11btRigidBody16addConstraintRefEP17btTypedConstraint, .Lfunc_end20-_ZN11btRigidBody16addConstraintRefEP17btTypedConstraint
	.cfi_endproc

	.globl	_ZN11btRigidBody19removeConstraintRefEP17btTypedConstraint
	.p2align	4, 0x90
	.type	_ZN11btRigidBody19removeConstraintRefEP17btTypedConstraint,@function
_ZN11btRigidBody19removeConstraintRefEP17btTypedConstraint: # @_ZN11btRigidBody19removeConstraintRefEP17btTypedConstraint
	.cfi_startproc
# BB#0:
	movslq	524(%rdi), %rax
	testq	%rax, %rax
	jle	.LBB21_6
# BB#1:                                 # %.lr.ph.i.i
	movq	536(%rdi), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB21_2:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rsi, (%rcx,%rdx,8)
	je	.LBB21_4
# BB#3:                                 #   in Loop: Header=BB21_2 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB21_2
	jmp	.LBB21_6
.LBB21_4:                               # %_ZNK20btAlignedObjectArrayIP17btTypedConstraintE16findLinearSearchERKS1_.exit.i
	cmpl	%eax, %edx
	jge	.LBB21_6
# BB#5:
	movq	(%rcx,%rdx,8), %r8
	movq	-8(%rcx,%rax,8), %rsi
	movq	%rsi, (%rcx,%rdx,8)
	movq	536(%rdi), %rcx
	movq	%r8, -8(%rcx,%rax,8)
	decq	%rax
	movl	%eax, 524(%rdi)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
.LBB21_6:                               # %_ZN20btAlignedObjectArrayIP17btTypedConstraintE6removeERKS1_.exit
	testl	%eax, %eax
	setg	272(%rdi)
	retq
.Lfunc_end21:
	.size	_ZN11btRigidBody19removeConstraintRefEP17btTypedConstraint, .Lfunc_end21-_ZN11btRigidBody19removeConstraintRefEP17btTypedConstraint
	.cfi_endproc

	.section	.text._ZN11btRigidBodyD2Ev,"axG",@progbits,_ZN11btRigidBodyD2Ev,comdat
	.weak	_ZN11btRigidBodyD2Ev
	.p2align	4, 0x90
	.type	_ZN11btRigidBodyD2Ev,@function
_ZN11btRigidBodyD2Ev:                   # @_ZN11btRigidBodyD2Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 32
.Lcfi65:
	.cfi_offset %rbx, -24
.Lcfi66:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV11btRigidBody+16, (%rbx)
	movq	536(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_4
# BB#1:
	cmpb	$0, 544(%rbx)
	je	.LBB22_3
# BB#2:
.Ltmp16:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp17:
.LBB22_3:                               # %.noexc
	movq	$0, 536(%rbx)
.LBB22_4:
	movb	$1, 544(%rbx)
	movq	$0, 536(%rbx)
	movq	$0, 524(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17btCollisionObjectD2Ev # TAILCALL
.LBB22_5:
.Ltmp18:
	movq	%rax, %r14
.Ltmp19:
	movq	%rbx, %rdi
	callq	_ZN17btCollisionObjectD2Ev
.Ltmp20:
# BB#6:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB22_7:
.Ltmp21:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end22:
	.size	_ZN11btRigidBodyD2Ev, .Lfunc_end22-_ZN11btRigidBodyD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin2   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp19-.Ltmp17         #   Call between .Ltmp17 and .Ltmp19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin2   #     jumps to .Ltmp21
	.byte	1                       #   On action: 1
	.long	.Ltmp20-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end22-.Ltmp20    #   Call between .Ltmp20 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN11btRigidBodyD0Ev,"axG",@progbits,_ZN11btRigidBodyD0Ev,comdat
	.weak	_ZN11btRigidBodyD0Ev
	.p2align	4, 0x90
	.type	_ZN11btRigidBodyD0Ev,@function
_ZN11btRigidBodyD0Ev:                   # @_ZN11btRigidBodyD0Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi69:
	.cfi_def_cfa_offset 32
.Lcfi70:
	.cfi_offset %rbx, -24
.Lcfi71:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV11btRigidBody+16, (%rbx)
	movq	536(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_4
# BB#1:
	cmpb	$0, 544(%rbx)
	je	.LBB23_3
# BB#2:
.Ltmp22:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp23:
.LBB23_3:                               # %.noexc.i
	movq	$0, 536(%rbx)
.LBB23_4:
	movb	$1, 544(%rbx)
	movq	$0, 536(%rbx)
	movq	$0, 524(%rbx)
.Ltmp28:
	movq	%rbx, %rdi
	callq	_ZN17btCollisionObjectD2Ev
.Ltmp29:
# BB#5:                                 # %_ZN11btRigidBodyD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB23_6:
.Ltmp24:
	movq	%rax, %r14
.Ltmp25:
	movq	%rbx, %rdi
	callq	_ZN17btCollisionObjectD2Ev
.Ltmp26:
	jmp	.LBB23_9
.LBB23_7:
.Ltmp27:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB23_8:
.Ltmp30:
	movq	%rax, %r14
.LBB23_9:                               # %.body
.Ltmp31:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp32:
# BB#10:                                # %_ZN17btCollisionObjectdlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB23_11:
.Ltmp33:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end23:
	.size	_ZN11btRigidBodyD0Ev, .Lfunc_end23-_ZN11btRigidBodyD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp22-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin3   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin3   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp25-.Ltmp29         #   Call between .Ltmp29 and .Ltmp25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin3   #     jumps to .Ltmp27
	.byte	1                       #   On action: 1
	.long	.Ltmp31-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin3   #     jumps to .Ltmp33
	.byte	1                       #   On action: 1
	.long	.Ltmp32-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Lfunc_end23-.Ltmp32    #   Call between .Ltmp32 and .Lfunc_end23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape,"axG",@progbits,_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape,comdat
	.weak	_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape
	.p2align	4, 0x90
	.type	_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape,@function
_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape: # @_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape
	.cfi_startproc
# BB#0:
	movq	%rsi, 200(%rdi)
	movq	%rsi, 208(%rdi)
	retq
.Lfunc_end24:
	.size	_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape, .Lfunc_end24-_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI25_0:
	.long	1065353216              # float 1
.LCPI25_1:
	.long	679477248               # float 1.42108547E-14
	.section	.text._ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf,"axG",@progbits,_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf,comdat
	.weak	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
	.p2align	4, 0x90
	.type	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf,@function
_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf: # @_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 24
	subq	$88, %rsp
.Lcfi74:
	.cfi_def_cfa_offset 112
.Lcfi75:
	.cfi_offset %rbx, -24
.Lcfi76:
	.cfi_offset %r14, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movss	20(%rdi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	40(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm0
	mulss	%xmm4, %xmm0
	movss	24(%rdi), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movss	36(%rdi), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm1
	mulss	%xmm14, %xmm1
	subss	%xmm1, %xmm0
	movaps	%xmm0, %xmm3
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
	movss	32(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm5
	mulss	%xmm1, %xmm5
	movss	16(%rdi), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm2
	mulss	%xmm11, %xmm2
	subss	%xmm2, %xmm5
	movaps	%xmm14, %xmm15
	mulss	%xmm11, %xmm15
	movaps	%xmm8, %xmm2
	mulss	%xmm1, %xmm2
	subss	%xmm2, %xmm15
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movss	4(%rdi), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	mulss	%xmm0, %xmm2
	movss	%xmm2, 36(%rsp)         # 4-byte Spill
	movss	8(%rdi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm2
	mulss	%xmm6, %xmm2
	movaps	%xmm14, %xmm7
	mulss	%xmm6, %xmm7
	movaps	%xmm4, %xmm3
	mulss	%xmm13, %xmm3
	movaps	%xmm12, %xmm10
	mulss	%xmm13, %xmm10
	movaps	%xmm8, %xmm0
	mulss	%xmm6, %xmm0
	movaps	%xmm1, %xmm9
	mulss	%xmm6, %xmm9
	mulss	%xmm11, %xmm6
	mulss	%xmm13, %xmm1
	mulss	%xmm13, %xmm11
	mulss	%xmm5, %xmm13
	addss	36(%rsp), %xmm13        # 4-byte Folded Reload
	addss	%xmm13, %xmm2
	movss	.LCPI25_0(%rip), %xmm13 # xmm13 = mem[0],zero,zero,zero
	divss	%xmm2, %xmm13
	subss	%xmm3, %xmm7
	subss	%xmm0, %xmm10
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm3
	mulss	%xmm13, %xmm7
	mulss	%xmm13, %xmm10
	mulss	%xmm13, %xmm5
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	subss	%xmm9, %xmm4
	mulss	%xmm13, %xmm4
	mulss	%xmm0, %xmm12
	subss	%xmm12, %xmm6
	mulss	%xmm13, %xmm6
	mulss	%xmm13, %xmm15
	mulss	%xmm0, %xmm14
	subss	%xmm14, %xmm1
	mulss	%xmm13, %xmm1
	mulss	%xmm0, %xmm8
	subss	%xmm11, %xmm8
	mulss	%xmm13, %xmm8
	movss	(%rsi), %xmm12          # xmm12 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm9
	mulss	%xmm3, %xmm9
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
	movaps	%xmm13, %xmm2
	mulss	%xmm5, %xmm2
	addss	%xmm9, %xmm2
	movss	8(%rsi), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm0
	mulss	%xmm15, %xmm0
	addss	%xmm2, %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movaps	%xmm12, %xmm11
	mulss	%xmm7, %xmm11
	movaps	%xmm13, %xmm2
	mulss	%xmm4, %xmm2
	addss	%xmm11, %xmm2
	movaps	%xmm9, %xmm11
	mulss	%xmm1, %xmm11
	addss	%xmm2, %xmm11
	mulss	%xmm10, %xmm12
	mulss	%xmm6, %xmm13
	addss	%xmm12, %xmm13
	mulss	%xmm8, %xmm9
	addss	%xmm13, %xmm9
	movss	16(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm12
	mulss	%xmm2, %xmm12
	movss	20(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm13
	mulss	%xmm0, %xmm13
	addss	%xmm12, %xmm13
	movss	24(%rsi), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm12
	mulss	%xmm14, %xmm12
	addss	%xmm13, %xmm12
	movaps	%xmm7, %xmm13
	mulss	%xmm2, %xmm13
	movaps	%xmm4, %xmm3
	mulss	%xmm0, %xmm3
	addss	%xmm13, %xmm3
	movaps	%xmm1, %xmm13
	mulss	%xmm14, %xmm13
	addss	%xmm3, %xmm13
	mulss	%xmm10, %xmm2
	mulss	%xmm6, %xmm0
	addss	%xmm2, %xmm0
	mulss	%xmm8, %xmm14
	addss	%xmm0, %xmm14
	movss	32(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movss	36(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	movss	40(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm15
	addss	%xmm5, %xmm15
	mulss	%xmm0, %xmm7
	mulss	%xmm2, %xmm4
	addss	%xmm7, %xmm4
	mulss	%xmm3, %xmm1
	addss	%xmm4, %xmm1
	mulss	%xmm0, %xmm10
	mulss	%xmm2, %xmm6
	addss	%xmm10, %xmm6
	mulss	%xmm3, %xmm8
	addss	%xmm6, %xmm8
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 40(%rsp)
	movss	%xmm11, 44(%rsp)
	movss	%xmm9, 48(%rsp)
	movl	$0, 52(%rsp)
	movss	%xmm12, 56(%rsp)
	movss	%xmm13, 60(%rsp)
	movss	%xmm14, 64(%rsp)
	movl	$0, 68(%rsp)
	movss	%xmm15, 72(%rsp)
	movss	%xmm1, 76(%rsp)
	movss	%xmm8, 80(%rsp)
	movl	$0, 84(%rsp)
	leaq	40(%rsp), %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	24(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movss	28(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB25_2
# BB#1:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB25_2:                               # %.split
	movss	.LCPI25_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	mulps	16(%rsp), %xmm0
	movaps	%xmm0, 16(%rsp)
	shufps	$231, %xmm0, %xmm0      # xmm0 = xmm0[3,1,2,3]
	callq	acosf
	addss	%xmm0, %xmm0
	movss	%xmm0, (%r14)
	movl	16(%rsp), %eax
	movl	20(%rsp), %ecx
	movl	24(%rsp), %edx
	movl	%eax, (%rbx)
	movl	%ecx, 4(%rbx)
	movl	%edx, 8(%rbx)
	movl	$0, 12(%rbx)
	movd	%eax, %xmm0
	mulss	%xmm0, %xmm0
	movd	%ecx, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movd	%edx, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movss	.LCPI25_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB25_4
# BB#3:
	movq	$1065353216, (%rbx)     # imm = 0x3F800000
	movq	$0, 8(%rbx)
	jmp	.LBB25_7
.LBB25_4:
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB25_6
# BB#5:                                 # %call.sqrt52
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB25_6:                               # %.split51
	movss	.LCPI25_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	divss	%xmm1, %xmm2
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movss	%xmm0, (%rbx)
	movss	4(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movss	%xmm0, 4(%rbx)
	mulss	8(%rbx), %xmm2
	movss	%xmm2, 8(%rbx)
.LBB25_7:
	addq	$88, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end25:
	.size	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf, .Lfunc_end25-_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
	.cfi_endproc

	.type	gDeactivationTime,@object # @gDeactivationTime
	.data
	.globl	gDeactivationTime
	.p2align	2
gDeactivationTime:
	.long	1073741824              # float 2
	.size	gDeactivationTime, 4

	.type	gDisableDeactivation,@object # @gDisableDeactivation
	.bss
	.globl	gDisableDeactivation
gDisableDeactivation:
	.byte	0                       # 0x0
	.size	gDisableDeactivation, 1

	.type	_ZTV11btRigidBody,@object # @_ZTV11btRigidBody
	.section	.rodata,"a",@progbits
	.globl	_ZTV11btRigidBody
	.p2align	3
_ZTV11btRigidBody:
	.quad	0
	.quad	_ZTI11btRigidBody
	.quad	_ZN11btRigidBody24checkCollideWithOverrideEP17btCollisionObject
	.quad	_ZN11btRigidBodyD2Ev
	.quad	_ZN11btRigidBodyD0Ev
	.quad	_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape
	.size	_ZTV11btRigidBody, 48

	.type	_ZL8uniqueId,@object    # @_ZL8uniqueId
	.local	_ZL8uniqueId
	.comm	_ZL8uniqueId,4,4
	.type	_ZTS11btRigidBody,@object # @_ZTS11btRigidBody
	.globl	_ZTS11btRigidBody
_ZTS11btRigidBody:
	.asciz	"11btRigidBody"
	.size	_ZTS11btRigidBody, 14

	.type	_ZTI11btRigidBody,@object # @_ZTI11btRigidBody
	.globl	_ZTI11btRigidBody
	.p2align	4
_ZTI11btRigidBody:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS11btRigidBody
	.quad	_ZTI17btCollisionObject
	.size	_ZTI11btRigidBody, 24


	.globl	_ZN11btRigidBodyC1ERKNS_27btRigidBodyConstructionInfoE
	.type	_ZN11btRigidBodyC1ERKNS_27btRigidBodyConstructionInfoE,@function
_ZN11btRigidBodyC1ERKNS_27btRigidBodyConstructionInfoE = _ZN11btRigidBodyC2ERKNS_27btRigidBodyConstructionInfoE
	.globl	_ZN11btRigidBodyC1EfP13btMotionStateP16btCollisionShapeRK9btVector3
	.type	_ZN11btRigidBodyC1EfP13btMotionStateP16btCollisionShapeRK9btVector3,@function
_ZN11btRigidBodyC1EfP13btMotionStateP16btCollisionShapeRK9btVector3 = _ZN11btRigidBodyC2EfP13btMotionStateP16btCollisionShapeRK9btVector3
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
