	.text
	.file	"utils.bc"
	.globl	_fatal_error_aux
	.p2align	4, 0x90
	.type	_fatal_error_aux,@function
_fatal_error_aux:                       # @_fatal_error_aux
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$208, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 256
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movl	%edx, %ebx
	movl	%esi, %r14d
	movq	%rdi, %r15
	testb	%al, %al
	je	.LBB0_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB0_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	testl	%ebx, %ebx
	movl	$.L.str, %eax
	movl	$.L.str.1, %ebp
	cmoveq	%rax, %rbp
	cmpq	$0, _fatal_error_aux.err_file(%rip)
	jne	.LBB0_5
# BB#3:
	movl	$.L.str.2, %edi
	movl	$.L.str.3, %esi
	callq	fopen
	movq	%rax, _fatal_error_aux.err_file(%rip)
	testq	%rax, %rax
	je	.LBB0_4
.LBB0_5:                                # %.thread
	testl	%ebx, %ebx
	je	.LBB0_6
.LBB0_11:
	movq	stderr(%rip), %rdi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	fprintf
	movq	_fatal_error_aux.err_file(%rip), %rdi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	fprintf
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	256(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$32, (%rsp)
	movq	stderr(%rip), %rdi
	movq	%rsp, %rbp
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	vfprintf
	movq	_fatal_error_aux.err_file(%rip), %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	vfprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	%r15, %rdx
	movl	%r14d, %ecx
	callq	fprintf
	movq	_fatal_error_aux.err_file(%rip), %rdi
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	%r15, %rdx
	movl	%r14d, %ecx
	callq	fprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	_fatal_error_aux.err_file(%rip), %rdi
	callq	fflush
	testl	%ebx, %ebx
	jne	.LBB0_13
.LBB0_12:                               # %.critedge
	addq	$208, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_4:
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$30, %esi
	movl	$1, %edx
	callq	fwrite
	testl	%ebx, %ebx
	jne	.LBB0_11
.LBB0_6:
	movq	_fatal_error_aux.err_file(%rip), %rdi
	callq	ftell
	cmpl	$-1, %eax
	je	.LBB0_7
# BB#8:
	cmpl	$33556432, %eax         # imm = 0x20007D0
	jg	.LBB0_12
# BB#9:
	cmpl	$33554433, %eax         # imm = 0x2000001
	jl	.LBB0_11
# BB#10:
	movq	stderr(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$28, %esi
	movl	$1, %edx
	callq	fwrite
	movq	_fatal_error_aux.err_file(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$28, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB0_11
.LBB0_7:
	movl	$.L.str.5, %edi
	movl	$36, %esi
	movl	$1, %edx
	movl	$.L.str.6, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
	jmp	.LBB0_11
.LBB0_13:
	movl	%ebx, %edi
	callq	exit
.Lfunc_end0:
	.size	_fatal_error_aux, .Lfunc_end0-_fatal_error_aux
	.cfi_endproc

	.globl	Asprintf
	.p2align	4, 0x90
	.type	Asprintf,@function
Asprintf:                               # @Asprintf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 48
	subq	$208, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 256
.Lcfi17:
	.cfi_offset %rbx, -48
.Lcfi18:
	.cfi_offset %r12, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %r15d
	movq	%rsi, %rbx
	movq	%rdi, %r12
	testb	%al, %al
	je	.LBB1_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB1_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	256(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$32, (%rsp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.LBB1_4
# BB#3:                                 # %._crit_edge
	movq	(%r12), %rdi
	jmp	.LBB1_6
.LBB1_4:
	movl	$128, (%rbx)
	movl	$128, %edi
	callq	malloc
	movq	%rax, %rdi
	movl	$128, %eax
.LBB1_5:                                # %.sink.split
	movq	%rdi, (%r12)
.LBB1_6:
	movslq	%r15d, %rbp
	addq	%rbp, %rdi
	subl	%ebp, %eax
	movslq	%eax, %rsi
	movq	%rsp, %rcx
	movq	%r14, %rdx
	callq	vsnprintf
	movslq	(%rbx), %rsi
	movl	%esi, %ecx
	subl	%ebp, %ecx
	cmpl	%ecx, %eax
	jge	.LBB1_7
# BB#8:
	addq	$208, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_7:
	subq	$-128, %rsi
	movl	%esi, (%rbx)
	movq	(%r12), %rdi
	callq	realloc
	movq	%rax, %rdi
	movl	(%rbx), %eax
	jmp	.LBB1_5
.Lfunc_end1:
	.size	Asprintf, .Lfunc_end1-Asprintf
	.cfi_endproc

	.globl	u64bit_to_string
	.p2align	4, 0x90
	.type	u64bit_to_string,@function
u64bit_to_string:                       # @u64bit_to_string
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi26:
	.cfi_def_cfa_offset 80
.Lcfi27:
	.cfi_offset %rbx, -40
.Lcfi28:
	.cfi_offset %r12, -32
.Lcfi29:
	.cfi_offset %r14, -24
.Lcfi30:
	.cfi_offset %r15, -16
	xorl	%r14d, %r14d
	movabsq	$2361183241434822607, %rcx # imm = 0x20C49BA5E353F7CF
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rax
	shrq	$3, %rax
	mulq	%rcx
	movq	%rdx, %rax
	shrq	$4, %rax
	imull	$1000, %eax, %esi       # imm = 0x3E8
	movl	%edi, %edx
	subl	%esi, %edx
	movl	%edx, (%rsp,%r14,4)
	incq	%r14
	cmpq	$999, %rdi              # imm = 0x3E7
	movq	%rax, %rdi
	ja	.LBB2_1
# BB#2:
	movl	$u64bit_to_string.big_num, %edi
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	callq	sprintf
	movl	%eax, %ebx
	cmpl	$1, %r14d
	je	.LBB2_5
# BB#3:                                 # %.lr.ph.preheader
	leaq	-1(%r14), %rax
	cltq
	leaq	-4(%rsp,%rax,4), %r15
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ebx, %rbx
	leaq	u64bit_to_string.big_num(%rbx), %rdi
	movl	(%r15,%r12,4), %edx
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	callq	sprintf
	addl	%eax, %ebx
	leal	(%r14,%r12), %eax
	decq	%r12
	cmpl	$2, %eax
	jne	.LBB2_4
.LBB2_5:                                # %._crit_edge
	movl	$u64bit_to_string.big_num, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	u64bit_to_string, .Lfunc_end2-u64bit_to_string
	.cfi_endproc

	.globl	null_command
	.p2align	4, 0x90
	.type	null_command,@function
null_command:                           # @null_command
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end3:
	.size	null_command, .Lfunc_end3-null_command
	.cfi_endproc

	.type	_fatal_error_aux.err_file,@object # @_fatal_error_aux.err_file
	.local	_fatal_error_aux.err_file
	.comm	_fatal_error_aux.err_file,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"WARNING: "
	.size	.L.str, 10

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"ERROR: "
	.size	.L.str.1, 8

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	".fatal_error"
	.size	.L.str.2, 13

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"w"
	.size	.L.str.3, 2

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Couldn't open \".fatal_error\".\n"
	.size	.L.str.4, 31

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/obsequi/utils.c"
	.size	.L.str.5, 79

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"size == -1.\n"
	.size	.L.str.6, 13

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Log file getting too large.\n"
	.size	.L.str.7, 29

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"> File: %s, Line: %d.\n"
	.size	.L.str.8, 23

	.type	u64bit_to_string.big_num,@object # @u64bit_to_string.big_num
	.local	u64bit_to_string.big_num
	.comm	u64bit_to_string.big_num,80,16
	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%d"
	.size	.L.str.9, 3

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	",%03d"
	.size	.L.str.10, 6


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
