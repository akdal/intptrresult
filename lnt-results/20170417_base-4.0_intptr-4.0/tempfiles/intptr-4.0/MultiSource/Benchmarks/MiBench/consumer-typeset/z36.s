	.text
	.file	"z36.bc"
	.globl	ReadHyphTable
	.p2align	4, 0x90
	.type	ReadHyphTable,@function
ReadHyphTable:                          # @ReadHyphTable
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	testl	%ebp, %ebp
	jne	.LBB0_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_2:
	movl	%ebp, %ebx
	cmpq	$0, HyphTables(,%rbx,8)
	jne	.LBB0_4
# BB#3:
	cmpl	$0, TriedFile(,%rbx,4)
	je	.LBB0_5
.LBB0_4:                                # %._crit_edge
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_5:
	leaq	4(%rsp), %rsi
	movl	%ebp, %edi
	callq	TrieRead
	movq	%rax, HyphTables(,%rbx,8)
	movl	$1, TriedFile(,%rbx,4)
	movl	4(%rsp), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	ReadHyphTable, .Lfunc_end0-ReadHyphTable
	.cfi_endproc

	.p2align	4, 0x90
	.type	TrieRead,@function
TrieRead:                               # @TrieRead
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	subq	$2168, %rsp             # imm = 0x878
.Lcfi11:
	.cfi_def_cfa_offset 2224
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	callq	LanguageHyph
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB1_1
# BB#3:
	movb	32(%rbx), %al
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB1_5
# BB#4:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.7, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_5:
	leaq	64(%rbx), %r12
	addq	$32, %rbx
	movl	$.L.str.8, %esi
	movl	$8, %ecx
	movl	$6, %r8d
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	DefineFile
	cmpl	$0, InitializeAll(%rip)
	je	.LBB1_6
.LBB1_8:                                # %.thread
	movl	$.L.str.9, %esi
	movl	$7, %ecx
	movl	$6, %r8d
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	DefineFile
	movzwl	%ax, %ebp
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	OpenFile
	testq	%rax, %rax
	je	.LBB1_9
# BB#10:
	leaq	624(%rsp), %rdi
	movl	$512, %esi              # imm = 0x200
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%rax, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB1_13
# BB#11:
	leaq	624(%rsp), %rdi
	movl	$.L.str.11, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_14
# BB#12:
	leaq	624(%rsp), %rdi
	movl	$.L.str.12, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_14
.LBB1_13:
	movq	no_fpos(%rip), %r14
	movl	%ebp, %edi
	callq	FileName
	movl	%ebp, %r10d
	movq	%rax, %rbp
	movl	$36, %edi
	movl	$9, %esi
	movl	$.L.str.13, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbp, %r9
	movl	%r10d, %ebp
	callq	Error
.LBB1_14:
	leaq	624(%rsp), %r14
	movl	$.L.str.11, %esi
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_16
# BB#15:
	movl	$1, (%r15)
	xorl	%r14d, %r14d
                                        # implicit-def: %RAX
	jmp	.LBB1_123
.LBB1_1:
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	jmp	.LBB1_2
.LBB1_6:
	movzwl	%ax, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	OpenFile
	testq	%rax, %rax
	je	.LBB1_8
# BB#7:
	movl	$1, %ecx
                                        # implicit-def: %RAX
	movl	%ecx, (%r15)
	jmp	.LBB1_125
.LBB1_9:
	movq	no_fpos(%rip), %rbx
	movl	%ebp, %edi
	callq	FileName
	movq	%rax, %rbp
	xorl	%r14d, %r14d
	movl	$36, %edi
	movl	$8, %esi
	movl	$.L.str.10, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%rbp, %r9
	callq	Error
	movl	$0, (%r15)
                                        # implicit-def: %RAX
	jmp	.LBB1_123
.LBB1_16:
	movl	%ebp, 60(%rsp)          # 4-byte Spill
	movl	$273063, %edi           # imm = 0x42AA7
	callq	malloc
	testq	%rax, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jne	.LBB1_18
# BB#17:
	movq	no_fpos(%rip), %r8
	movl	$36, %edi
	movl	$4, %esi
	movl	$.L.str.25, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	16(%rsp), %rax          # 8-byte Reload
.LBB1_18:                               # %NewTrie.exit
	movabsq	$8589934560, %rbp       # imm = 0x1FFFFFFE0
	movabsq	$4300328830, %rcx       # imm = 0x10051CF7E
	movq	%rcx, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 248(%rax)
	movups	%xmm0, 232(%rax)
	movups	%xmm0, 216(%rax)
	movups	%xmm0, 200(%rax)
	movups	%xmm0, 184(%rax)
	movups	%xmm0, 168(%rax)
	movups	%xmm0, 152(%rax)
	movups	%xmm0, 136(%rax)
	movups	%xmm0, 120(%rax)
	movups	%xmm0, 104(%rax)
	movups	%xmm0, 88(%rax)
	movups	%xmm0, 72(%rax)
	movups	%xmm0, 56(%rax)
	movups	%xmm0, 40(%rax)
	movups	%xmm0, 24(%rax)
	movups	%xmm0, 8(%rax)
	leaq	296(%rax), %rcx
	movq	%rcx, 264(%rax)
	movl	$120000, 272(%rax)      # imm = 0x1D4C0
	movl	$0, 276(%rax)
	movq	%rax, %rcx
	addq	$240296, %rcx           # imm = 0x3AAA8
	movq	%rcx, 280(%rax)
	movl	$32767, 292(%rax)       # imm = 0x7FFF
	movl	$32767, 288(%rax)       # imm = 0x7FFF
	movl	$0, 68(%rsp)
	leaq	96(%rsp), %rdi
	movl	$512, %esi              # imm = 0x200
	movq	80(%rsp), %rdx          # 8-byte Reload
	callq	fgets
	testq	%rax, %rax
	movq	%r15, 32(%rsp)          # 8-byte Spill
	je	.LBB1_85
# BB#19:                                # %.lr.ph455.preheader
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	movl	$1, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	96(%rsp), %r15
	movabsq	$12884901888, %r12      # imm = 0x300000000
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB1_21:                               # %.lr.ph455
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_24 Depth 2
                                        #       Child Loop BB1_25 Depth 3
                                        #         Child Loop BB1_26 Depth 4
                                        #           Child Loop BB1_30 Depth 5
                                        #       Child Loop BB1_74 Depth 3
                                        #       Child Loop BB1_63 Depth 3
                                        #       Child Loop BB1_53 Depth 3
	movq	24(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r14, %rdx
	leaq	64(%rsp), %rcx
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB1_20
# BB#22:                                # %.lr.ph455
                                        #   in Loop: Header=BB1_21 Depth=1
	cmpb	$37, 624(%rsp)
	je	.LBB1_20
# BB#23:                                # %.lr.ph451.preheader
                                        #   in Loop: Header=BB1_21 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_24:                               # %.lr.ph451
                                        #   Parent Loop BB1_21 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_25 Depth 3
                                        #         Child Loop BB1_26 Depth 4
                                        #           Child Loop BB1_30 Depth 5
                                        #       Child Loop BB1_74 Depth 3
                                        #       Child Loop BB1_63 Depth 3
                                        #       Child Loop BB1_53 Depth 3
	movl	%eax, 92(%rsp)          # 4-byte Spill
	movslq	64(%rsp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%r14, 48(%rsp)          # 8-byte Spill
	jmp	.LBB1_25
	.p2align	4, 0x90
.LBB1_27:                               #   in Loop: Header=BB1_25 Depth=3
	testb	%al, %al
	je	.LBB1_35
	jmp	.LBB1_28
	.p2align	4, 0x90
.LBB1_34:                               #   in Loop: Header=BB1_25 Depth=3
	sarq	$29, %rbp
	movq	tex_codes(%rbp), %r15
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	strcpy
	movq	%r15, %rdi
	callq	strlen
	addq	%rax, %rbp
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	sarq	$29, %rbx
	movq	tex_codes(%rbx), %rdi
	callq	strlen
	leaq	1(%r14,%rax), %r14
	movabsq	$8589934560, %rbp       # imm = 0x1FFFFFFE0
	jmp	.LBB1_25
	.p2align	4, 0x90
.LBB1_28:                               #   in Loop: Header=BB1_25 Depth=3
	movq	48(%rsp), %rcx          # 8-byte Reload
	movb	%al, (%rcx)
	incq	%rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%r15, %r14
.LBB1_25:                               # %.outer.i
                                        #   Parent Loop BB1_21 Depth=1
                                        #     Parent Loop BB1_24 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_26 Depth 4
                                        #           Child Loop BB1_30 Depth 5
	leaq	1(%r14), %r15
	jmp	.LBB1_26
	.p2align	4, 0x90
.LBB1_33:                               #   in Loop: Header=BB1_26 Depth=4
	movq	(%r13), %rax
	cmpb	$0, (%rax)
	jne	.LBB1_34
.LBB1_32:                               # %.thread.i
                                        #   in Loop: Header=BB1_26 Depth=4
	movq	no_fpos(%rip), %r8
	subq	$8, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	movl	$36, %edi
	movl	$1, %esi
	movl	$.L.str.26, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %r9           # 8-byte Reload
	pushq	32(%rsp)                # 8-byte Folded Reload
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi20:
	.cfi_adjust_cfa_offset -16
	movabsq	$8589934560, %rbp       # imm = 0x1FFFFFFE0
.LBB1_26:                               #   Parent Loop BB1_21 Depth=1
                                        #     Parent Loop BB1_24 Depth=2
                                        #       Parent Loop BB1_25 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB1_30 Depth 5
	movb	(%r14), %al
	cmpb	$92, %al
	jne	.LBB1_27
# BB#29:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB1_26 Depth=4
	leaq	32(%rbp), %rbp
	movl	$tex_codes, %r13d
	movabsq	$4294967296, %rbx       # imm = 0x100000000
	.p2align	4, 0x90
.LBB1_30:                               # %.preheader.i
                                        #   Parent Loop BB1_21 Depth=1
                                        #     Parent Loop BB1_24 Depth=2
                                        #       Parent Loop BB1_25 Depth=3
                                        #         Parent Loop BB1_26 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	8(%r13), %rsi
	movq	%r15, %rdi
	callq	StringBeginsWith
	testl	%eax, %eax
	jne	.LBB1_33
# BB#31:                                #   in Loop: Header=BB1_30 Depth=5
	movq	24(%r13), %rax
	addq	$24, %r13
	addq	%r12, %rbx
	addq	%r12, %rbp
	cmpb	$0, (%rax)
	jne	.LBB1_30
	jmp	.LBB1_32
	.p2align	4, 0x90
.LBB1_35:                               # %DecodeEscapes.exit
                                        #   in Loop: Header=BB1_24 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	movb	$0, (%rax)
	movl	4(%rsp), %eax           # 4-byte Reload
	cmpl	$4, %eax
	ja	.LBB1_83
# BB#36:                                # %DecodeEscapes.exit
                                        #   in Loop: Header=BB1_24 Depth=2
	movl	%eax, %eax
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_39:                               #   in Loop: Header=BB1_24 Depth=2
	movl	$.L.str.15, %esi
	leaq	624(%rsp), %r14
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_40
# BB#42:                                #   in Loop: Header=BB1_24 Depth=2
	movq	no_fpos(%rip), %r15
	movl	60(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %rbx
	movl	$1, 4(%rsp)             # 4-byte Folded Spill
	movl	$36, %edi
	movl	$10, %esi
	movl	$.L.str.16, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	movq	%rbx, %r9
	callq	Error
	jmp	.LBB1_41
.LBB1_83:                               #   in Loop: Header=BB1_24 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.23, %r9d
	xorl	%eax, %eax
	callq	Error
	leaq	624(%rsp), %r14
	jmp	.LBB1_41
.LBB1_43:                               #   in Loop: Header=BB1_24 Depth=2
	movl	$.L.str.17, %esi
	leaq	624(%rsp), %r14
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_44
# BB#45:                                #   in Loop: Header=BB1_24 Depth=2
	movl	$.L.str.18, %esi
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_46
# BB#47:                                #   in Loop: Header=BB1_24 Depth=2
	movl	$.L.str.19, %esi
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_48
# BB#49:                                #   in Loop: Header=BB1_24 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	292(%rcx), %eax
	cmpl	288(%rcx), %eax
	je	.LBB1_51
# BB#50:                                #   in Loop: Header=BB1_24 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.163, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	16(%rsp), %rcx          # 8-byte Reload
.LBB1_51:                               # %.preheader.i419
                                        #   in Loop: Header=BB1_24 Depth=2
	movb	624(%rsp), %al
	testb	%al, %al
	leaq	96(%rsp), %r15
	movq	8(%rsp), %r13           # 8-byte Reload
	je	.LBB1_57
# BB#52:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB1_24 Depth=2
	leaq	625(%rsp), %rbx
	.p2align	4, 0x90
.LBB1_53:                               # %.lr.ph.i
                                        #   Parent Loop BB1_21 Depth=1
                                        #     Parent Loop BB1_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	%al, %r9d
	cmpb	$0, 8(%rcx,%r9)
	je	.LBB1_54
# BB#55:                                #   in Loop: Header=BB1_53 Depth=3
	movq	no_fpos(%rip), %r8
	movl	$36, %edi
	movl	$6, %esi
	movl	$.L.str.164, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	callq	Error
	movq	16(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB1_56
	.p2align	4, 0x90
.LBB1_54:                               #   in Loop: Header=BB1_53 Depth=3
	movzbl	4(%rcx), %eax
	movb	%al, 8(%rcx,%r9)
.LBB1_56:                               #   in Loop: Header=BB1_53 Depth=3
	movzbl	(%rbx), %eax
	incq	%rbx
	testb	%al, %al
	jne	.LBB1_53
.LBB1_57:                               # %AddClassToTrie.exit
                                        #   in Loop: Header=BB1_24 Depth=2
	incl	4(%rcx)
	movl	$1, 4(%rsp)             # 4-byte Folded Spill
	jmp	.LBB1_58
.LBB1_60:                               #   in Loop: Header=BB1_24 Depth=2
	movl	$.L.str.18, %esi
	leaq	624(%rsp), %r14
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_46
# BB#61:                                #   in Loop: Header=BB1_24 Depth=2
	movl	$.L.str.19, %esi
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_48
# BB#62:                                #   in Loop: Header=BB1_24 Depth=2
	movb	$46, 1648(%rsp)
	movb	$56, 1136(%rsp)
	movl	$56, %edx
	movl	$1, %eax
	movq	%r14, %rcx
	jmp	.LBB1_63
	.p2align	4, 0x90
.LBB1_66:                               #   in Loop: Header=BB1_63 Depth=3
	incq	%rcx
	movl	%esi, %edx
.LBB1_63:                               #   Parent Loop BB1_21 Depth=1
                                        #     Parent Loop BB1_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rcx), %ebx
	movl	$57, %esi
	cmpb	$45, %bl
	je	.LBB1_66
# BB#64:                                #   in Loop: Header=BB1_63 Depth=3
	testb	%bl, %bl
	je	.LBB1_67
# BB#65:                                #   in Loop: Header=BB1_63 Depth=3
	movslq	%eax, %rsi
	movb	%bl, 1648(%rsp,%rsi)
	incl	%eax
	movb	%dl, 1136(%rsp,%rsi)
	movl	$56, %esi
	jmp	.LBB1_66
.LBB1_70:                               #   in Loop: Header=BB1_24 Depth=2
	movl	$.L.str.18, %esi
	leaq	624(%rsp), %r14
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_46
# BB#71:                                #   in Loop: Header=BB1_24 Depth=2
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	leaq	68(%rsp), %rdx
	callq	sscanf
	movl	$3, 4(%rsp)             # 4-byte Folded Spill
	cmpl	$1, %eax
	leaq	96(%rsp), %r15
	movq	8(%rsp), %r13           # 8-byte Reload
	je	.LBB1_58
	jmp	.LBB1_72
.LBB1_37:                               # %.preheader
                                        #   in Loop: Header=BB1_24 Depth=2
	movb	624(%rsp), %al
	testb	%al, %al
	leaq	624(%rsp), %r14
	je	.LBB1_38
# BB#73:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_24 Depth=2
	xorl	%esi, %esi
	movl	$48, %ecx
	leaq	625(%rsp), %rdx
	leaq	96(%rsp), %r15
	movq	8(%rsp), %r13           # 8-byte Reload
	.p2align	4, 0x90
.LBB1_74:                               # %.lr.ph
                                        #   Parent Loop BB1_21 Depth=1
                                        #     Parent Loop BB1_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, %ebx
	addb	$-48, %bl
	cmpb	$10, %bl
	jae	.LBB1_76
# BB#75:                                #   in Loop: Header=BB1_74 Depth=3
	movzbl	%al, %ecx
	jmp	.LBB1_77
	.p2align	4, 0x90
.LBB1_76:                               #   in Loop: Header=BB1_74 Depth=3
	movslq	%esi, %rdi
	movb	%al, 1648(%rsp,%rdi)
	leal	1(%rdi), %esi
	movb	%cl, 1136(%rsp,%rdi)
	movl	$48, %ecx
.LBB1_77:                               #   in Loop: Header=BB1_74 Depth=3
	movzbl	(%rdx), %eax
	incq	%rdx
	testb	%al, %al
	jne	.LBB1_74
	jmp	.LBB1_78
.LBB1_46:                               #   in Loop: Header=BB1_24 Depth=2
	movl	$4, 4(%rsp)             # 4-byte Folded Spill
	jmp	.LBB1_41
.LBB1_67:                               #   in Loop: Header=BB1_24 Depth=2
	cltq
	movb	$46, 1648(%rsp,%rax)
	movb	%dl, 1136(%rsp,%rax)
	movb	$0, 1649(%rsp,%rax)
	movw	$56, 1137(%rsp,%rax)
	leaq	1648(%rsp), %rdi
	leaq	1136(%rsp), %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	TrieInsert
	movl	$2, 4(%rsp)             # 4-byte Folded Spill
	testl	%eax, %eax
	leaq	96(%rsp), %r15
	movq	8(%rsp), %r13           # 8-byte Reload
	jne	.LBB1_58
	jmp	.LBB1_68
.LBB1_48:                               #   in Loop: Header=BB1_24 Depth=2
	movl	$3, 4(%rsp)             # 4-byte Folded Spill
	jmp	.LBB1_41
.LBB1_40:                               #   in Loop: Header=BB1_24 Depth=2
	movl	$1, 4(%rsp)             # 4-byte Folded Spill
	jmp	.LBB1_41
.LBB1_44:                               #   in Loop: Header=BB1_24 Depth=2
	movl	$2, 4(%rsp)             # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB1_41:                               # %.backedge
                                        #   in Loop: Header=BB1_24 Depth=2
	leaq	96(%rsp), %r15
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB1_58:                               # %.backedge
                                        #   in Loop: Header=BB1_24 Depth=2
	movslq	92(%rsp), %rax          # 4-byte Folded Reload
	addq	%rax, %r13
	leaq	96(%rsp,%r13), %rdi
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movq	%r14, %rdx
	leaq	64(%rsp), %rcx
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB1_20
# BB#59:                                # %.backedge
                                        #   in Loop: Header=BB1_24 Depth=2
	cmpb	$37, 624(%rsp)
	movl	%r13d, %eax
	jne	.LBB1_24
	jmp	.LBB1_20
.LBB1_38:                               #   in Loop: Header=BB1_24 Depth=2
	movl	$48, %ecx
	xorl	%esi, %esi
	leaq	96(%rsp), %r15
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB1_78:                               # %._crit_edge
                                        #   in Loop: Header=BB1_24 Depth=2
	movslq	%esi, %rax
	movb	$0, 1648(%rsp,%rax)
	movb	%cl, 1136(%rsp,%rax)
	movb	$0, 1137(%rsp,%rax)
	movl	68(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB1_81
# BB#79:                                # %._crit_edge
                                        #   in Loop: Header=BB1_24 Depth=2
	cmpl	%ecx, %esi
	jle	.LBB1_81
# BB#80:                                #   in Loop: Header=BB1_24 Depth=2
	movl	$4, 4(%rsp)             # 4-byte Folded Spill
	jmp	.LBB1_58
.LBB1_81:                               #   in Loop: Header=BB1_24 Depth=2
	movl	$4, 4(%rsp)             # 4-byte Folded Spill
	leaq	1648(%rsp), %rdi
	leaq	1136(%rsp), %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	TrieInsert
	testl	%eax, %eax
	jne	.LBB1_58
	jmp	.LBB1_82
	.p2align	4, 0x90
.LBB1_20:                               # %.critedge.loopexit
                                        #   in Loop: Header=BB1_21 Depth=1
	movl	$512, %esi              # imm = 0x200
	movq	%r15, %rdi
	movq	80(%rsp), %rdx          # 8-byte Reload
	callq	fgets
	testq	%rax, %rax
	jne	.LBB1_21
# BB#84:                                # %.critedge._crit_edge
	cmpl	$4, 4(%rsp)             # 4-byte Folded Reload
	je	.LBB1_86
.LBB1_85:                               # %.critedge._crit_edge.thread
	movq	no_fpos(%rip), %r14
	movl	60(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %rbx
	movl	$36, %edi
	movl	$13, %esi
	movl	$.L.str.24, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbx, %r9
	callq	Error
.LBB1_86:
	movq	%rbp, %r14
	movq	80(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	movq	16(%rsp), %rax          # 8-byte Reload
	movslq	276(%rax), %rbx
	testq	%rbx, %rbx
	movl	%ebx, 272(%rax)
	movq	264(%rax), %r13
	jle	.LBB1_99
# BB#87:                                # %.lr.ph44.i.preheader
	testb	$1, %bl
	jne	.LBB1_89
# BB#88:
	xorl	%edx, %edx
	cmpl	$1, %ebx
	jne	.LBB1_93
	jmp	.LBB1_99
.LBB1_72:
	xorl	%r14d, %r14d
	movl	$36, %edi
	movl	$20, %esi
	movl	$.L.str.22, %edx
	movl	$2, %ecx
	movl	$0, %eax
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	40(%rsp), %r9           # 8-byte Reload
	pushq	24(%rsp)                # 8-byte Folded Reload
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.9
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi23:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB1_69
.LBB1_82:
	xorl	%r14d, %r14d
	movl	$36, %edi
	movl	$12, %esi
	movl	$.L.str.20, %edx
	movl	$2, %ecx
	movl	$0, %eax
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	40(%rsp), %r9           # 8-byte Reload
	pushq	24(%rsp)                # 8-byte Folded Reload
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.9
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi26:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB1_69
.LBB1_89:                               # %.lr.ph44.i.prol
	movzwl	(%r13), %ecx
	testw	%cx, %cx
	jns	.LBB1_91
# BB#90:
	addl	292(%rax), %ecx
	movw	%cx, (%r13)
.LBB1_91:                               # %.lr.ph44.i.prol.loopexit
	movl	$1, %edx
	cmpl	$1, %ebx
	je	.LBB1_99
.LBB1_93:                               # %.lr.ph44.i.preheader.new
	movq	%rbx, %rcx
	subq	%rdx, %rcx
	leaq	2(%r13,%rdx,2), %rdx
	.p2align	4, 0x90
.LBB1_94:                               # %.lr.ph44.i
                                        # =>This Inner Loop Header: Depth=1
	movzwl	-2(%rdx), %esi
	testw	%si, %si
	jns	.LBB1_96
# BB#95:                                #   in Loop: Header=BB1_94 Depth=1
	addl	292(%rax), %esi
	movw	%si, -2(%rdx)
.LBB1_96:                               # %.lr.ph44.i.1511
                                        #   in Loop: Header=BB1_94 Depth=1
	movzwl	(%rdx), %esi
	testw	%si, %si
	jns	.LBB1_98
# BB#97:                                #   in Loop: Header=BB1_94 Depth=1
	addl	292(%rax), %esi
	movw	%si, (%rdx)
.LBB1_98:                               #   in Loop: Header=BB1_94 Depth=1
	addq	$4, %rdx
	addq	$-2, %rcx
	jne	.LBB1_94
.LBB1_99:                               # %._crit_edge45.i
	movslq	292(%rax), %r10
	movl	288(%rax), %ecx
	movl	%ecx, %r8d
	subl	%r10d, %r8d
	jle	.LBB1_100
# BB#101:                               # %.lr.ph.preheader.i
	movq	280(%rax), %r11
	leaq	(%r11,%r10), %rdx
	leaq	(%r13,%rbx,2), %rsi
	leal	-1(%rcx), %r9d
	movl	%r9d, %r12d
	subl	%r10d, %r12d
	incq	%r12
	xorl	%edi, %edi
	cmpq	$32, %r12
	jb	.LBB1_114
# BB#102:                               # %min.iters.checked
	andq	%r12, %r14
	je	.LBB1_114
# BB#103:                               # %vector.memcheck
	leaq	(%r13,%rbx,2), %r15
	movl	%r9d, %ebp
	subl	%r10d, %ebp
	leaq	(%r10,%rbp), %rax
	leaq	1(%r11,%rax), %rax
	cmpq	%rax, %rsi
	jae	.LBB1_105
# BB#104:                               # %vector.memcheck
	leaq	1(%rbp,%r15), %rax
	cmpq	%rax, %rdx
	jb	.LBB1_114
.LBB1_105:                              # %vector.body.preheader
	movq	%r15, 24(%rsp)          # 8-byte Spill
	leaq	-32(%r14), %r15
	movl	%r15d, %edi
	shrl	$5, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB1_106
# BB#107:                               # %vector.body.prol.preheader
	leaq	16(%r13,%rbx,2), %rax
	leaq	16(%r11,%r10), %rbp
	negq	%rdi
	xorl	%ebx, %ebx
.LBB1_108:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbp,%rbx), %xmm0
	movups	(%rbp,%rbx), %xmm1
	movups	%xmm0, -16(%rax,%rbx)
	movups	%xmm1, (%rax,%rbx)
	addq	$32, %rbx
	incq	%rdi
	jne	.LBB1_108
	jmp	.LBB1_109
.LBB1_100:
	movq	32(%rsp), %r15          # 8-byte Reload
	jmp	.LBB1_122
.LBB1_68:
	xorl	%r14d, %r14d
	movl	$36, %edi
	movl	$11, %esi
	movl	$.L.str.20, %edx
	movl	$2, %ecx
	movl	$0, %eax
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	40(%rsp), %r9           # 8-byte Reload
	pushq	24(%rsp)                # 8-byte Folded Reload
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.9
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi29:
	.cfi_adjust_cfa_offset -16
.LBB1_69:
	movq	32(%rsp), %r15          # 8-byte Reload
	movl	$0, (%r15)
	movq	16(%rsp), %rax          # 8-byte Reload
	jmp	.LBB1_123
.LBB1_106:
	xorl	%ebx, %ebx
.LBB1_109:                              # %vector.body.prol.loopexit
	cmpq	$96, %r15
	jb	.LBB1_112
# BB#110:                               # %vector.body.preheader.new
	movq	%r14, %rax
	subq	%rbx, %rax
	movq	24(%rsp), %rdi          # 8-byte Reload
	leaq	112(%rbx,%rdi), %rdi
	addq	%r10, %rbx
	leaq	112(%r11,%rbx), %rbx
.LBB1_111:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rdi)
	movups	%xmm1, -96(%rdi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rdi)
	movups	%xmm1, -64(%rdi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rdi)
	movups	%xmm1, -32(%rdi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	subq	$-128, %rdi
	subq	$-128, %rbx
	addq	$-128, %rax
	jne	.LBB1_111
.LBB1_112:                              # %middle.block
	cmpq	%r14, %r12
	movq	32(%rsp), %r15          # 8-byte Reload
	je	.LBB1_121
# BB#113:
	addq	%r14, %rdx
	addq	%r14, %rsi
	movl	%r14d, %edi
.LBB1_114:                              # %.lr.ph.i424.preheader
	movl	%ecx, %eax
	subl	%edi, %eax
	subl	%r10d, %eax
	subl	%edi, %r9d
	subl	%r10d, %r9d
	andl	$7, %eax
	je	.LBB1_115
# BB#116:                               # %.lr.ph.i424.prol.preheader
	negl	%eax
	movq	32(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_117:                              # %.lr.ph.i424.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdx), %ebx
	incq	%rdx
	movb	%bl, (%rsi)
	incq	%rsi
	incl	%edi
	incl	%eax
	jne	.LBB1_117
	jmp	.LBB1_118
.LBB1_115:
	movq	32(%rsp), %r15          # 8-byte Reload
.LBB1_118:                              # %.lr.ph.i424.prol.loopexit
	cmpl	$7, %r9d
	jb	.LBB1_121
# BB#119:                               # %.lr.ph.i424.preheader.new
	subl	%edi, %ecx
	subl	%r10d, %ecx
	.p2align	4, 0x90
.LBB1_120:                              # %.lr.ph.i424
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdx), %eax
	movb	%al, (%rsi)
	movzbl	1(%rdx), %eax
	movb	%al, 1(%rsi)
	movzbl	2(%rdx), %eax
	movb	%al, 2(%rsi)
	movzbl	3(%rdx), %eax
	movb	%al, 3(%rsi)
	movzbl	4(%rdx), %eax
	movb	%al, 4(%rsi)
	movzbl	5(%rdx), %eax
	movb	%al, 5(%rsi)
	movzbl	6(%rdx), %eax
	movb	%al, 6(%rsi)
	movzbl	7(%rdx), %eax
	movb	%al, 7(%rsi)
	addq	$8, %rdx
	addq	$8, %rsi
	addl	$-8, %ecx
	jne	.LBB1_120
.LBB1_121:                              # %._crit_edge.loopexit.i
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	264(%rax), %r13
	movl	272(%rax), %ebx
.LBB1_122:                              # %CompressTrie.exit
	movslq	%ebx, %rcx
	leaq	(%r13,%rcx,2), %rcx
	movq	%rcx, 280(%rax)
	movl	$0, 292(%rax)
	movl	%r8d, 288(%rax)
	movb	$1, %r14b
.LBB1_123:
	movl	$1, %ecx
	testb	%r14b, %r14b
	je	.LBB1_124
.LBB1_2:                                # %.sink.split
	movl	%ecx, (%r15)
	jmp	.LBB1_125
.LBB1_124:
	xorl	%eax, %eax
.LBB1_125:
	addq	$2168, %rsp             # imm = 0x878
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	TrieRead, .Lfunc_end1-TrieRead
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_39
	.quad	.LBB1_43
	.quad	.LBB1_60
	.quad	.LBB1_70
	.quad	.LBB1_37

	.text
	.globl	Hyphenate
	.p2align	4, 0x90
	.type	Hyphenate,@function
Hyphenate:                              # @Hyphenate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 56
	subq	$6280, %rsp             # imm = 0x1888
.Lcfi36:
	.cfi_def_cfa_offset 6336
.Lcfi37:
	.cfi_offset %rbx, -56
.Lcfi38:
	.cfi_offset %r12, -48
.Lcfi39:
	.cfi_offset %r13, -40
.Lcfi40:
	.cfi_offset %r14, -32
.Lcfi41:
	.cfi_offset %r15, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	cmpb	$17, 32(%r12)
	je	.LBB2_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.3, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_2:                                # %.thread.preheader
	movq	8(%r12), %r13
	cmpq	%r12, %r13
	je	.LBB2_149
# BB#3:                                 # %.preheader393.lr.ph
	movabsq	$4294967296, %r10       # imm = 0x100000000
	movq	%r12, 48(%rsp)          # 8-byte Spill
	jmp	.LBB2_90
.LBB2_4:                                #   in Loop: Header=BB2_90 Depth=1
	xorl	%edx, %edx
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB2_5:                                # %.lr.ph.prol.loopexit
                                        #   in Loop: Header=BB2_90 Depth=1
	cmpq	$3, %r8
	jb	.LBB2_8
# BB#6:                                 # %.lr.ph.preheader.new
                                        #   in Loop: Header=BB2_90 Depth=1
	movl	%r13d, %eax
	subq	%rdx, %rax
	leaq	4228(%rsp), %rcx
	leaq	(%rcx,%rdx), %rcx
	addq	%r15, %rdx
	movslq	%r12d, %rsi
	leaq	67(%rsi,%rdx), %rdx
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph
                                        #   Parent Loop BB2_90 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-3(%rdx), %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movzbl	8(%rdi,%rsi), %ebx
	movb	%bl, -3(%rcx)
	movzbl	-2(%rdx), %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movzbl	8(%rdi,%rsi), %ebx
	movb	%bl, -2(%rcx)
	movzbl	-1(%rdx), %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movzbl	8(%rdi,%rsi), %ebx
	movb	%bl, -1(%rcx)
	movzbl	(%rdx), %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movzbl	8(%rdi,%rsi), %ebx
	movb	%bl, (%rcx)
	movq	8(%rsp), %rbx           # 8-byte Reload
	addq	$4, %rcx
	addq	$4, %rdx
	addq	$-4, %rax
	jne	.LBB2_7
.LBB2_8:                                # %._crit_edge
                                        #   in Loop: Header=BB2_90 Depth=1
	shlq	$32, %rbp
	movabsq	$4294967296, %rax       # imm = 0x100000000
	leaq	(%rbp,%rax), %rax
	sarq	$32, %rax
	movb	$1, 4224(%rsp,%rax)
	movb	$48, 2160(%rsp,%rax)
	movabsq	$8589934592, %rax       # imm = 0x200000000
	leaq	(%rbp,%rax), %rax
	sarq	$32, %rax
	movb	$0, 4224(%rsp,%rax)
	movb	$48, 2160(%rsp,%rax)
	movabsq	$12884901888, %rax      # imm = 0x300000000
	addq	%rax, %rbp
	sarq	$32, %rbp
	movb	$0, 2160(%rsp,%rbp)
	leaq	4224(%rsp), %rax
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB2_9:                                #   Parent Loop BB2_90 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_14 Depth 3
                                        #         Child Loop BB2_17 Depth 4
                                        #         Child Loop BB2_22 Depth 4
                                        #       Child Loop BB2_26 Depth 3
                                        #       Child Loop BB2_30 Depth 3
                                        #       Child Loop BB2_35 Depth 3
	movq	%rdx, %rax
	leaq	4224(%rsp), %rcx
	subq	%rcx, %rax
	leaq	2160(%rsp,%rax), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	264(%rbx), %rax
	xorl	%r13d, %r13d
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	%rdx, %rsi
	jmp	.LBB2_14
	.p2align	4, 0x90
.LBB2_10:                               # %.loopexit
                                        #   in Loop: Header=BB2_14 Depth=3
	movzbl	(%rsi), %ecx
	testl	%ecx, %ecx
	je	.LBB2_38
# BB#11:                                #   in Loop: Header=BB2_14 Depth=3
	movq	264(%rbx), %rax
	addl	%r13d, %ecx
	movslq	%ecx, %rcx
	movswl	(%rax,%rcx,2), %r13d
	testl	%r13d, %r13d
	je	.LBB2_38
# BB#12:                                #   in Loop: Header=BB2_14 Depth=3
	testw	%r13w, %r13w
	js	.LBB2_25
# BB#13:                                #   in Loop: Header=BB2_14 Depth=3
	shll	$2, %r13d
	incq	%rsi
.LBB2_14:                               #   Parent Loop BB2_90 Depth=1
                                        #     Parent Loop BB2_9 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_17 Depth 4
                                        #         Child Loop BB2_22 Depth 4
	movslq	%r13d, %rcx
	movswq	(%rax,%rcx,2), %rcx
	testq	%rcx, %rcx
	jns	.LBB2_10
# BB#15:                                #   in Loop: Header=BB2_14 Depth=3
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	280(%rbx), %rdx
	movq	%rdx, %rax
	subq	%rcx, %rax
	movb	(%rax), %al
	testb	%al, %al
	leaq	96(%rsp), %rbp
	je	.LBB2_20
# BB#16:                                # %.lr.ph418.preheader
                                        #   in Loop: Header=BB2_14 Depth=3
	negq	%rcx
	leaq	1(%rdx,%rcx), %r14
	leaq	96(%rsp), %rbp
	.p2align	4, 0x90
.LBB2_17:                               # %.lr.ph418
                                        #   Parent Loop BB2_90 Depth=1
                                        #     Parent Loop BB2_9 Depth=2
                                        #       Parent Loop BB2_14 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	%al, %ebx
	movl	%ebx, %r12d
	shrl	$4, %r12d
	andb	$15, %bl
	testl	%r12d, %r12d
	je	.LBB2_19
# BB#18:                                # %.lr.ph412.preheader
                                        #   in Loop: Header=BB2_17 Depth=4
	decl	%r12d
	leaq	1(%r12), %rdx
	movl	$48, %esi
	movq	%rbp, %rdi
	callq	memset
	leaq	1(%rbp,%r12), %rbp
.LBB2_19:                               # %._crit_edge413
                                        #   in Loop: Header=BB2_17 Depth=4
	orb	$48, %bl
	addb	$-2, %bl
	movb	%bl, (%rbp)
	incq	%rbp
	movzbl	(%r14), %eax
	incq	%r14
	testb	%al, %al
	jne	.LBB2_17
.LBB2_20:                               # %._crit_edge419
                                        #   in Loop: Header=BB2_14 Depth=3
	movb	$0, (%rbp)
	movb	96(%rsp), %al
	testb	%al, %al
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
	je	.LBB2_10
# BB#21:                                # %.lr.ph424.preheader
                                        #   in Loop: Header=BB2_14 Depth=3
	leaq	97(%rsp), %rcx
	movq	16(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_22:                               # %.lr.ph424
                                        #   Parent Loop BB2_90 Depth=1
                                        #     Parent Loop BB2_9 Depth=2
                                        #       Parent Loop BB2_14 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpb	(%rdx), %al
	jbe	.LBB2_24
# BB#23:                                #   in Loop: Header=BB2_22 Depth=4
	movb	%al, (%rdx)
.LBB2_24:                               #   in Loop: Header=BB2_22 Depth=4
	incq	%rdx
	movzbl	(%rcx), %eax
	incq	%rcx
	testb	%al, %al
	jne	.LBB2_22
	jmp	.LBB2_10
	.p2align	4, 0x90
.LBB2_25:                               #   in Loop: Header=BB2_9 Depth=2
	movslq	%r13d, %rcx
	movq	280(%rbx), %rax
	subq	%rcx, %rax
	leaq	2(%rax), %rbp
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB2_26:                               #   Parent Loop BB2_90 Depth=1
                                        #     Parent Loop BB2_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	-1(%rax,%rcx), %edx
	testb	%dl, %dl
	je	.LBB2_28
# BB#27:                                #   in Loop: Header=BB2_26 Depth=3
	incq	%rbp
	cmpb	%dl, (%rsi,%rcx)
	leaq	1(%rcx), %rcx
	je	.LBB2_26
	jmp	.LBB2_38
.LBB2_28:                               # %.preheader389
                                        #   in Loop: Header=BB2_9 Depth=2
	movb	(%rax,%rcx), %al
	testb	%al, %al
	leaq	96(%rsp), %r12
	je	.LBB2_33
# BB#29:                                # %.lr.ph434.preheader
                                        #   in Loop: Header=BB2_9 Depth=2
	leaq	96(%rsp), %r12
	.p2align	4, 0x90
.LBB2_30:                               # %.lr.ph434
                                        #   Parent Loop BB2_90 Depth=1
                                        #     Parent Loop BB2_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	%al, %ebx
	movl	%ebx, %r14d
	shrl	$4, %r14d
	andb	$15, %bl
	testl	%r14d, %r14d
	je	.LBB2_32
# BB#31:                                # %.lr.ph428.preheader
                                        #   in Loop: Header=BB2_30 Depth=3
	decl	%r14d
	leaq	1(%r14), %rdx
	movl	$48, %esi
	movq	%r12, %rdi
	callq	memset
	leaq	1(%r12,%r14), %r12
.LBB2_32:                               # %._crit_edge429
                                        #   in Loop: Header=BB2_30 Depth=3
	orb	$48, %bl
	addb	$-2, %bl
	movb	%bl, (%r12)
	incq	%r12
	movzbl	(%rbp), %eax
	incq	%rbp
	testb	%al, %al
	jne	.LBB2_30
.LBB2_33:                               # %._crit_edge435
                                        #   in Loop: Header=BB2_9 Depth=2
	movb	$0, (%r12)
	movb	96(%rsp), %al
	testb	%al, %al
	movq	8(%rsp), %rbx           # 8-byte Reload
	je	.LBB2_38
# BB#34:                                # %.lr.ph440.preheader
                                        #   in Loop: Header=BB2_9 Depth=2
	leaq	97(%rsp), %rcx
	.p2align	4, 0x90
.LBB2_35:                               # %.lr.ph440
                                        #   Parent Loop BB2_90 Depth=1
                                        #     Parent Loop BB2_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rsp), %rdx          # 8-byte Reload
	cmpb	(%rdx), %al
	jbe	.LBB2_37
# BB#36:                                #   in Loop: Header=BB2_35 Depth=3
	movb	%al, (%rdx)
.LBB2_37:                               #   in Loop: Header=BB2_35 Depth=3
	incq	%rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movzbl	(%rcx), %eax
	incq	%rcx
	testb	%al, %al
	jne	.LBB2_35
	.p2align	4, 0x90
.LBB2_38:                               # %.loopexit388
                                        #   in Loop: Header=BB2_9 Depth=2
	movq	72(%rsp), %rdx          # 8-byte Reload
	cmpb	$1, 2(%rdx)
	leaq	1(%rdx), %rdx
	jne	.LBB2_9
# BB#39:                                #   in Loop: Header=BB2_90 Depth=1
	movb	64(%r15), %r9b
	testb	%r9b, %r9b
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	24(%rsp), %r13          # 8-byte Reload
	movabsq	$4294967296, %r14       # imm = 0x100000000
	je	.LBB2_53
# BB#40:                                # %.lr.ph447.preheader
                                        #   in Loop: Header=BB2_90 Depth=1
	movq	finfo(%rip), %rax
	movl	40(%r15), %ecx
	andl	$4095, %ecx             # imm = 0xFFF
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	movq	8(%rax,%rcx), %r8
	leaq	65(%r15), %rdi
	movl	$2, %r10d
	movq	56(%rsp), %r11          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_41:                               # %.lr.ph447
                                        #   Parent Loop BB2_90 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_44 Depth 3
                                        #         Child Loop BB2_45 Depth 4
                                        #         Child Loop BB2_49 Depth 4
	movzbl	%r9b, %eax
	movzbl	(%r8,%rax), %eax
	cmpq	$2, %rax
	jb	.LBB2_52
# BB#42:                                #   in Loop: Header=BB2_41 Depth=2
	cmpb	%r9b, 256(%r8,%rax)
	jne	.LBB2_52
# BB#43:                                # %.preheader386.preheader
                                        #   in Loop: Header=BB2_41 Depth=2
	leaq	256(%r8,%rax), %rbp
	.p2align	4, 0x90
.LBB2_44:                               # %.preheader386
                                        #   Parent Loop BB2_90 Depth=1
                                        #     Parent Loop BB2_41 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_45 Depth 4
                                        #         Child Loop BB2_49 Depth 4
	leaq	1(%rbp), %rbx
	addq	$2, %rbp
	movq	%rbp, %rsi
	movq	%rdi, %rdx
	.p2align	4, 0x90
.LBB2_45:                               #   Parent Loop BB2_90 Depth=1
                                        #     Parent Loop BB2_41 Depth=2
                                        #       Parent Loop BB2_44 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rsi, %rbp
	movzbl	(%rbx), %ecx
	movzbl	1(%rbx), %eax
	cmpb	(%rdx), %cl
	jne	.LBB2_48
# BB#46:                                #   in Loop: Header=BB2_45 Depth=4
	testb	%cl, %cl
	je	.LBB2_48
# BB#47:                                #   in Loop: Header=BB2_45 Depth=4
	incq	%rbx
	incq	%rdx
	leaq	1(%rbp), %rsi
	testb	%al, %al
	jne	.LBB2_45
.LBB2_48:                               # %.critedge
                                        #   in Loop: Header=BB2_44 Depth=3
	testb	%al, %al
	je	.LBB2_51
	.p2align	4, 0x90
.LBB2_49:                               # %.preheader
                                        #   Parent Loop BB2_90 Depth=1
                                        #     Parent Loop BB2_41 Depth=2
                                        #       Parent Loop BB2_44 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpb	$0, (%rbp)
	leaq	1(%rbp), %rbp
	jne	.LBB2_49
# BB#50:                                #   in Loop: Header=BB2_44 Depth=3
	cmpb	%r9b, (%rbp)
	je	.LBB2_44
	jmp	.LBB2_52
.LBB2_51:                               #   in Loop: Header=BB2_41 Depth=2
	movb	$48, 2160(%rsp,%r10)
	.p2align	4, 0x90
.LBB2_52:                               # %.loopexit387
                                        #   in Loop: Header=BB2_41 Depth=2
	incq	%r10
	movb	1(%r11), %r9b
	incq	%r11
	incq	%rdi
	testb	%r9b, %r9b
	jne	.LBB2_41
.LBB2_53:                               # %._crit_edge448
                                        #   in Loop: Header=BB2_90 Depth=1
	movq	8(%r13), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movabsq	$-4294967296, %rdx      # imm = 0xFFFFFFFF00000000
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	44(%rsp), %ebx          # 4-byte Reload
	movq	%r14, %r10
	jmp	.LBB2_85
	.p2align	4, 0x90
.LBB2_54:                               #   in Loop: Header=BB2_85 Depth=2
	sarq	$32, %rbp
	addq	%rsi, %rbp
	movl	$11, %edi
	movq	%rbp, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	MakeWord
	movq	%rax, %r14
	movl	40(%r15), %ecx
	movl	$4095, %eax             # imm = 0xFFF
	andl	%eax, %ecx
	movl	40(%r14), %eax
	movl	$-4096, %edx            # imm = 0xF000
	andl	%edx, %eax
	orl	%ecx, %eax
	movl	%eax, 40(%r14)
	movl	40(%r15), %ecx
	movl	$4190208, %edx          # imm = 0x3FF000
	andl	%edx, %ecx
	andl	$-4190209, %eax         # imm = 0xFFC00FFF
	orl	%ecx, %eax
	movl	%eax, 40(%r14)
	movl	40(%r15), %ecx
	movl	$4194304, %edx          # imm = 0x400000
	andl	%edx, %ecx
	andl	$-4194305, %eax         # imm = 0xFFBFFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%r14)
	movl	40(%r15), %ecx
	movl	$528482304, %edx        # imm = 0x1F800000
	andl	%edx, %ecx
	andl	$-528482305, %eax       # imm = 0xE07FFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%r14)
	movl	40(%r15), %ecx
	movl	$-2147483648, %edx      # imm = 0x80000000
	andl	%edx, %ecx
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%r14)
	movl	40(%r15), %ecx
	movl	$1610612736, %edx       # imm = 0x60000000
	andl	%edx, %ecx
	andl	$-1610612737, %eax      # imm = 0x9FFFFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%r14)
	movq	%r14, %rdi
	callq	FontWordSize
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_56
# BB#55:                                #   in Loop: Header=BB2_85 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_57
.LBB2_56:                               #   in Loop: Header=BB2_85 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_57:                               #   in Loop: Header=BB2_85 Depth=2
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%r13), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_60
# BB#58:                                #   in Loop: Header=BB2_85 Depth=2
	testq	%rcx, %rcx
	je	.LBB2_60
# BB#59:                                #   in Loop: Header=BB2_85 Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_60:                               #   in Loop: Header=BB2_85 Depth=2
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB2_63
# BB#61:                                #   in Loop: Header=BB2_85 Depth=2
	testq	%rax, %rax
	je	.LBB2_63
# BB#62:                                #   in Loop: Header=BB2_85 Depth=2
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_63:                               #   in Loop: Header=BB2_85 Depth=2
	movzbl	zz_lengths+1(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r14
	testq	%r14, %r14
	je	.LBB2_65
# BB#64:                                #   in Loop: Header=BB2_85 Depth=2
	movq	%r14, zz_hold(%rip)
	movq	(%r14), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB2_66
.LBB2_65:                               #   in Loop: Header=BB2_85 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r14
	movq	%r14, zz_hold(%rip)
.LBB2_66:                               #   in Loop: Header=BB2_85 Depth=2
	movb	$1, 32(%r14)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%r14, 8(%r14)
	movq	%r14, (%r14)
	movw	$0, 41(%r14)
	movzwl	44(%r14), %eax
	andl	$127, %eax
	orl	$17920, %eax            # imm = 0x4600
	movw	%ax, 44(%r14)
	movw	$0, 46(%r14)
	movl	40(%r15), %eax
	movl	$1610612736, %ecx       # imm = 0x60000000
	andl	%ecx, %eax
	movl	40(%r14), %ecx
	movl	$-1610612737, %edx      # imm = 0x9FFFFFFF
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 40(%r14)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_68
# BB#67:                                #   in Loop: Header=BB2_85 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_69
.LBB2_68:                               #   in Loop: Header=BB2_85 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_69:                               #   in Loop: Header=BB2_85 Depth=2
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%r13), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_72
# BB#70:                                #   in Loop: Header=BB2_85 Depth=2
	testq	%rcx, %rcx
	je	.LBB2_72
# BB#71:                                #   in Loop: Header=BB2_85 Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_72:                               #   in Loop: Header=BB2_85 Depth=2
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB2_75
# BB#73:                                #   in Loop: Header=BB2_85 Depth=2
	testq	%rax, %rax
	je	.LBB2_75
# BB#74:                                #   in Loop: Header=BB2_85 Depth=2
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_75:                               #   in Loop: Header=BB2_85 Depth=2
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_77
# BB#76:                                #   in Loop: Header=BB2_85 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_78
.LBB2_77:                               #   in Loop: Header=BB2_85 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_78:                               #   in Loop: Header=BB2_85 Depth=2
	testq	%r14, %r14
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	je	.LBB2_81
# BB#79:                                #   in Loop: Header=BB2_85 Depth=2
	testq	%rax, %rax
	je	.LBB2_81
# BB#80:                                #   in Loop: Header=BB2_85 Depth=2
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_81:                               #   in Loop: Header=BB2_85 Depth=2
	movq	%rax, zz_res(%rip)
	movl	$11, %edi
	movl	$.L.str.6, %esi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	MakeWord
	movq	%rax, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_84
# BB#82:                                #   in Loop: Header=BB2_85 Depth=2
	movq	zz_res(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB2_84
# BB#83:                                #   in Loop: Header=BB2_85 Depth=2
	movq	16(%rax), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rcx), %rsi
	movq	%rsi, 16(%rax)
	movq	16(%rcx), %rsi
	movq	%rax, 24(%rsi)
	movq	%rdx, 16(%rcx)
	movq	%rcx, 24(%rdx)
.LBB2_84:                               #   in Loop: Header=BB2_85 Depth=2
	movb	$0, (%rbp)
	movl	$1, %eax
	movabsq	$4294967296, %r14       # imm = 0x100000000
	movq	%r14, %r10
	movabsq	$-4294967296, %rdx      # imm = 0xFFFFFFFF00000000
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %rdi          # 8-byte Reload
.LBB2_85:                               # %.outer
                                        #   Parent Loop BB2_90 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_86 Depth 3
	movslq	%ebx, %rcx
	movq	%rcx, %rbp
	shlq	$32, %rbp
	addq	%rdx, %rbp
	addq	%rdi, %rbp
	decq	%rcx
	.p2align	4, 0x90
.LBB2_86:                               #   Parent Loop BB2_90 Depth=1
                                        #     Parent Loop BB2_85 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	$3, %rcx
	jl	.LBB2_88
# BB#87:                                #   in Loop: Header=BB2_86 Depth=3
	addq	%rdx, %rbp
	decl	%ebx
	testb	$1, 2160(%rsp,%rcx)
	leaq	-1(%rcx), %rcx
	je	.LBB2_86
	jmp	.LBB2_54
.LBB2_88:                               #   in Loop: Header=BB2_90 Depth=1
	testl	%eax, %eax
	je	.LBB2_148
# BB#89:                                #   in Loop: Header=BB2_90 Depth=1
	movq	%r15, %rdi
	movq	%r10, %rbx
	callq	FontWordSize
	movq	%rbx, %r10
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r13
	jmp	.LBB2_148
	.p2align	4, 0x90
.LBB2_90:                               # %.preheader393
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_91 Depth 2
                                        #     Child Loop BB2_99 Depth 2
                                        #     Child Loop BB2_101 Depth 2
                                        #     Child Loop BB2_118 Depth 2
                                        #     Child Loop BB2_7 Depth 2
                                        #     Child Loop BB2_9 Depth 2
                                        #       Child Loop BB2_14 Depth 3
                                        #         Child Loop BB2_17 Depth 4
                                        #         Child Loop BB2_22 Depth 4
                                        #       Child Loop BB2_26 Depth 3
                                        #       Child Loop BB2_30 Depth 3
                                        #       Child Loop BB2_35 Depth 3
                                        #     Child Loop BB2_41 Depth 2
                                        #       Child Loop BB2_44 Depth 3
                                        #         Child Loop BB2_45 Depth 4
                                        #         Child Loop BB2_49 Depth 4
                                        #     Child Loop BB2_85 Depth 2
                                        #       Child Loop BB2_86 Depth 3
	movq	%r13, %r15
	.p2align	4, 0x90
.LBB2_91:                               #   Parent Loop BB2_90 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r15), %r15
	movzbl	32(%r15), %eax
	testb	%al, %al
	je	.LBB2_91
# BB#92:                                #   in Loop: Header=BB2_90 Depth=1
	movl	%eax, %ecx
	addb	$-11, %cl
	cmpb	$2, %cl
	jae	.LBB2_106
# BB#93:                                #   in Loop: Header=BB2_90 Depth=1
	cmpb	$0, 64(%r15)
	je	.LBB2_148
# BB#94:                                #   in Loop: Header=BB2_90 Depth=1
	movl	40(%r15), %ebp
	testl	%ebp, %ebp
	jns	.LBB2_148
# BB#95:                                #   in Loop: Header=BB2_90 Depth=1
	leaq	32(%r15), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	64(%r15), %r9
	shrl	$23, %ebp
	andl	$63, %ebp
	jne	.LBB2_97
# BB#96:                                #   in Loop: Header=BB2_90 Depth=1
	movl	$36, %edi
	movl	$19, %esi
	movl	$.L.str.4, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	%r9, %rbx
	movq	%r10, %r14
	callq	Error
	movq	%r14, %r10
	movq	%rbx, %r9
.LBB2_97:                               #   in Loop: Header=BB2_90 Depth=1
	movl	%ebp, %ebx
	movq	HyphTables(,%rbx,8), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testq	%rax, %rax
	je	.LBB2_109
.LBB2_98:                               #   in Loop: Header=BB2_90 Depth=1
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movl	$-1, %r14d
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	movq	8(%rsp), %rsi           # 8-byte Reload
	.p2align	4, 0x90
.LBB2_99:                               #   Parent Loop BB2_90 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %r12
	movq	%rax, %r8
	movzbl	(%r9,%r12), %edx
	leaq	1(%r12), %rcx
	incl	%r14d
	leaq	(%r8,%r10), %rax
	cmpb	$1, 8(%rsi,%rdx)
	je	.LBB2_99
# BB#100:                               # %.preheader392
                                        #   in Loop: Header=BB2_90 Depth=1
	movslq	%r12d, %rcx
	leaq	-2(%r10), %rax
	subq	%r12, %rax
	addq	%rcx, %rax
	leaq	64(%r15,%rcx), %rdx
	shlq	$32, %rcx
	movl	$-1, %ebp
	xorl	%edi, %edi
	movq	%rsi, %r11
	.p2align	4, 0x90
.LBB2_101:                              #   Parent Loop BB2_90 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, %r13d
	movzbl	(%rdx), %esi
	movzbl	8(%r11,%rsi), %ebx
	addq	%r10, %rcx
	incq	%rax
	incl	%ebp
	incq	%rdx
	leal	1(%r13), %edi
	cmpb	$1, %bl
	ja	.LBB2_101
# BB#102:                               #   in Loop: Header=BB2_90 Depth=1
	cmpb	$45, %sil
	jne	.LBB2_111
# BB#103:                               #   in Loop: Header=BB2_90 Depth=1
	sarq	$32, %rcx
	cmpb	$0, (%r9,%rcx)
	movq	48(%rsp), %r12          # 8-byte Reload
	je	.LBB2_115
# BB#104:                               #   in Loop: Header=BB2_90 Depth=1
	movq	%r10, %r14
	addq	%rcx, %r9
	movl	$11, %edi
	movq	%r9, %rbx
	movq	%r9, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	MakeWord
	movq	%rax, %rbp
	movl	40(%r15), %ecx
	movl	$4095, %eax             # imm = 0xFFF
	andl	%eax, %ecx
	movl	40(%rbp), %eax
	movl	$-4096, %edx            # imm = 0xF000
	andl	%edx, %eax
	orl	%ecx, %eax
	movl	%eax, 40(%rbp)
	movl	40(%r15), %ecx
	movl	$4190208, %edx          # imm = 0x3FF000
	andl	%edx, %ecx
	andl	$-4190209, %eax         # imm = 0xFFC00FFF
	orl	%ecx, %eax
	movl	%eax, 40(%rbp)
	movl	40(%r15), %ecx
	movl	$4194304, %edx          # imm = 0x400000
	andl	%edx, %ecx
	andl	$-4194305, %eax         # imm = 0xFFBFFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%rbp)
	movl	40(%r15), %ecx
	movl	$528482304, %edx        # imm = 0x1F800000
	andl	%edx, %ecx
	andl	$-528482305, %eax       # imm = 0xE07FFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%rbp)
	movl	40(%r15), %ecx
	movl	$-2147483648, %edx      # imm = 0x80000000
	andl	%edx, %ecx
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%rbp)
	movl	40(%r15), %ecx
	movl	$1610612736, %edx       # imm = 0x60000000
	andl	%edx, %ecx
	andl	$-1610612737, %eax      # imm = 0x9FFFFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%rbp)
	movq	%rbp, %rdi
	callq	FontWordSize
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_119
# BB#105:                               #   in Loop: Header=BB2_90 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_120
	.p2align	4, 0x90
.LBB2_106:                              #   in Loop: Header=BB2_90 Depth=1
	cmpb	$1, %al
	jne	.LBB2_148
# BB#107:                               #   in Loop: Header=BB2_90 Depth=1
	movzwl	44(%r15), %eax
	movl	%eax, %ecx
	andl	$57344, %ecx            # imm = 0xE000
	cmpl	$16384, %ecx            # imm = 0x4000
	jne	.LBB2_148
# BB#108:                               #   in Loop: Header=BB2_90 Depth=1
	andl	$65407, %eax            # imm = 0xFF7F
	movw	%ax, 44(%r15)
	jmp	.LBB2_148
.LBB2_109:                              #   in Loop: Header=BB2_90 Depth=1
	cmpl	$0, TriedFile(,%rbx,4)
	jne	.LBB2_148
# BB#110:                               #   in Loop: Header=BB2_90 Depth=1
	movl	%ebp, %edi
	leaq	92(%rsp), %rsi
	movq	%r9, %rbp
	movq	%r10, %r14
	callq	TrieRead
	movq	%r14, %r10
	movq	%rbp, %r9
	movq	%rax, HyphTables(,%rbx,8)
	movl	$1, TriedFile(,%rbx,4)
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testq	%rax, %rax
	jne	.LBB2_98
	jmp	.LBB2_148
.LBB2_111:                              #   in Loop: Header=BB2_90 Depth=1
	cmpl	$5, %ebp
	jl	.LBB2_114
# BB#112:                               #   in Loop: Header=BB2_90 Depth=1
	testb	%sil, %sil
	je	.LBB2_116
# BB#113:                               #   in Loop: Header=BB2_90 Depth=1
	testb	%bl, %bl
	jne	.LBB2_116
.LBB2_114:                              #   in Loop: Header=BB2_90 Depth=1
	movq	48(%rsp), %r12          # 8-byte Reload
.LBB2_115:                              #   in Loop: Header=BB2_90 Depth=1
	movq	24(%rsp), %r13          # 8-byte Reload
	jmp	.LBB2_148
.LBB2_116:                              # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_90 Depth=1
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movq	%r9, 56(%rsp)           # 8-byte Spill
	movb	$1, 4224(%rsp)
	movb	$48, 2160(%rsp)
	movl	%eax, %edx
	incq	%rdx
	movl	$48, %esi
	leaq	2161(%rsp), %rdi
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	%ebp, 44(%rsp)          # 4-byte Spill
	callq	memset
	movl	44(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ecx
	leaq	-1(%rcx), %r8
	testb	$3, %cl
	je	.LBB2_4
# BB#117:                               # %.lr.ph.prol.preheader
                                        #   in Loop: Header=BB2_90 Depth=1
	movslq	%r14d, %rcx
	leaq	64(%r15,%rcx), %rcx
	movl	%eax, %esi
	andl	$3, %esi
	xorl	%edx, %edx
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_118:                              # %.lr.ph.prol
                                        #   Parent Loop BB2_90 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx,%rdx), %edi
	movzbl	8(%rbx,%rdi), %eax
	movb	%al, 4225(%rsp,%rdx)
	incq	%rdx
	cmpq	%rdx, %rsi
	jne	.LBB2_118
	jmp	.LBB2_5
.LBB2_119:                              #   in Loop: Header=BB2_90 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_120:                              #   in Loop: Header=BB2_90 Depth=1
	movq	24(%rsp), %r13          # 8-byte Reload
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%r13), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_123
# BB#121:                               #   in Loop: Header=BB2_90 Depth=1
	testq	%rcx, %rcx
	je	.LBB2_123
# BB#122:                               #   in Loop: Header=BB2_90 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_123:                              #   in Loop: Header=BB2_90 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB2_126
# BB#124:                               #   in Loop: Header=BB2_90 Depth=1
	testq	%rax, %rax
	je	.LBB2_126
# BB#125:                               #   in Loop: Header=BB2_90 Depth=1
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_126:                              #   in Loop: Header=BB2_90 Depth=1
	movzbl	zz_lengths+1(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB2_128
# BB#127:                               #   in Loop: Header=BB2_90 Depth=1
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB2_129
.LBB2_128:                              #   in Loop: Header=BB2_90 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB2_129:                              #   in Loop: Header=BB2_90 Depth=1
	movb	$1, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movw	$0, 41(%rbp)
	movzwl	44(%rbp), %eax
	andl	$127, %eax
	orl	$17920, %eax            # imm = 0x4600
	movw	%ax, 44(%rbp)
	movw	$0, 46(%rbp)
	movl	40(%r15), %eax
	movl	$1610612736, %ecx       # imm = 0x60000000
	andl	%ecx, %eax
	movl	40(%rbp), %ecx
	movl	$-1610612737, %edx      # imm = 0x9FFFFFFF
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 40(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_131
# BB#130:                               #   in Loop: Header=BB2_90 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_132
.LBB2_131:                              #   in Loop: Header=BB2_90 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_132:                              #   in Loop: Header=BB2_90 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%r13), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_135
# BB#133:                               #   in Loop: Header=BB2_90 Depth=1
	testq	%rcx, %rcx
	je	.LBB2_135
# BB#134:                               #   in Loop: Header=BB2_90 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_135:                              #   in Loop: Header=BB2_90 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB2_138
# BB#136:                               #   in Loop: Header=BB2_90 Depth=1
	testq	%rax, %rax
	je	.LBB2_138
# BB#137:                               #   in Loop: Header=BB2_90 Depth=1
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_138:                              #   in Loop: Header=BB2_90 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_140
# BB#139:                               #   in Loop: Header=BB2_90 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_141
.LBB2_140:                              #   in Loop: Header=BB2_90 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_141:                              #   in Loop: Header=BB2_90 Depth=1
	testq	%rbp, %rbp
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB2_144
# BB#142:                               #   in Loop: Header=BB2_90 Depth=1
	testq	%rax, %rax
	je	.LBB2_144
# BB#143:                               #   in Loop: Header=BB2_90 Depth=1
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_144:                              #   in Loop: Header=BB2_90 Depth=1
	movq	%rax, zz_res(%rip)
	movl	$11, %edi
	movl	$.L.str.6, %esi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	MakeWord
	movq	%rax, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_147
# BB#145:                               #   in Loop: Header=BB2_90 Depth=1
	movq	zz_res(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB2_147
# BB#146:                               #   in Loop: Header=BB2_90 Depth=1
	movq	16(%rax), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rcx), %rsi
	movq	%rsi, 16(%rax)
	movq	16(%rcx), %rsi
	movq	%rax, 24(%rsi)
	movq	%rdx, 16(%rcx)
	movq	%rcx, 24(%rdx)
.LBB2_147:                              #   in Loop: Header=BB2_90 Depth=1
	movb	$0, (%rbx)
	movq	%r15, %rdi
	callq	FontWordSize
	movq	8(%r13), %r13
	movq	%r14, %r10
	.p2align	4, 0x90
.LBB2_148:                              # %.thread.backedge
                                        #   in Loop: Header=BB2_90 Depth=1
	movq	8(%r13), %r13
	cmpq	%r12, %r13
	jne	.LBB2_90
.LBB2_149:                              # %.thread._crit_edge
	movq	%r12, %rax
	addq	$6280, %rsp             # imm = 0x1888
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	Hyphenate, .Lfunc_end2-Hyphenate
	.cfi_endproc

	.p2align	4, 0x90
	.type	TrieInsert,@function
TrieInsert:                             # @TrieInsert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi46:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 56
	subq	$1048, %rsp             # imm = 0x418
.Lcfi49:
	.cfi_def_cfa_offset 1104
.Lcfi50:
	.cfi_offset %rbx, -56
.Lcfi51:
	.cfi_offset %r12, -48
.Lcfi52:
	.cfi_offset %r13, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movl	%r8d, %r15d
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	%rdi, %r14
	cmpl	$0, 276(%r13)
	jne	.LBB3_5
# BB#1:
	movl	4(%r13), %eax
	leal	-1(%rax), %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	leal	-1(%rax,%rcx), %eax
	andl	$-4, %eax
	addl	$4, %eax
	movl	%eax, 4(%r13)
	xorl	%ecx, %ecx
	cmpl	272(%r13), %eax
	jle	.LBB3_3
# BB#2:
	movq	no_fpos(%rip), %r8
	movl	$36, %edi
	movl	$5, %esi
	movl	$.L.str.167, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	callq	Error
	movl	4(%r13), %eax
	movl	276(%r13), %ecx
.LBB3_3:
	leal	(%rcx,%rax), %edx
	movl	%edx, 276(%r13)
	testl	%eax, %eax
	jle	.LBB3_5
# BB#4:                                 # %.lr.ph.i
	movq	264(%r13), %rax
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,2), %rdi
	leal	1(%rcx), %eax
	cmpl	%eax, %edx
	cmovgel	%edx, %eax
	notl	%ecx
	addl	%eax, %ecx
	leaq	2(%rcx,%rcx), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB3_5:                                # %NewTrieNode.exit
	leaq	16(%rsp), %rax
	leaq	-1(%rbx), %rcx
	jmp	.LBB3_6
	.p2align	4, 0x90
.LBB3_14:                               #   in Loop: Header=BB3_6 Depth=1
	incq	%rbx
.LBB3_6:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %edx
	cmpq	$48, %rdx
	je	.LBB3_12
# BB#7:                                 #   in Loop: Header=BB3_6 Depth=1
	testb	%dl, %dl
	jne	.LBB3_13
	jmp	.LBB3_8
	.p2align	4, 0x90
.LBB3_12:                               #   in Loop: Header=BB3_6 Depth=1
	leaq	-1(%rbx), %rsi
	subq	%rcx, %rsi
	cmpq	$15, %rsi
	jl	.LBB3_14
.LBB3_13:                               #   in Loop: Header=BB3_6 Depth=1
	movq	%rbx, %rsi
	subl	%ecx, %esi
	shll	$4, %esi
	addl	$240, %esi
	addl	$210, %edx
	orl	%esi, %edx
	movb	%dl, (%rax)
	incq	%rax
	movq	%rbx, %rcx
	jmp	.LBB3_14
.LBB3_8:
	movb	$0, (%rax)
	movb	(%r14), %al
	xorl	%ebx, %ebx
	testb	%al, %al
	jne	.LBB3_10
	jmp	.LBB3_17
	.p2align	4, 0x90
.LBB3_16:                               #   in Loop: Header=BB3_10 Depth=1
	movzbl	1(%r14,%rbx), %eax
	incq	%rbx
	testb	%al, %al
	je	.LBB3_17
.LBB3_10:                               # %.lr.ph123
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%al, %ebp
	movzbl	8(%r13,%rbp), %eax
	testb	%al, %al
	je	.LBB3_15
# BB#11:                                #   in Loop: Header=BB3_10 Depth=1
	movb	%al, 528(%rsp,%rbx)
	jmp	.LBB3_16
	.p2align	4, 0x90
.LBB3_15:                               #   in Loop: Header=BB3_10 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$36, %edi
	movl	$2, %esi
	movl	$.L.str.165, %edx
	movl	$1, %ecx
	movl	$0, %eax
	movq	%r12, %r9
	pushq	%rbp
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi58:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB3_16
.LBB3_17:                               # %._crit_edge124
	movb	$0, 528(%rsp,%rbx)
	movb	528(%rsp), %cl
	xorl	%ebx, %ebx
	testb	%cl, %cl
	movq	264(%r13), %rax
	je	.LBB3_24
# BB#18:                                # %.lr.ph
	leaq	528(%rsp), %rbx
	movabsq	$4294967296, %r8        # imm = 0x100000000
	movq	%r8, %r12
	xorl	%r15d, %r15d
	movq	%r14, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB3_19:                               # =>This Inner Loop Header: Depth=1
	movzbl	%cl, %ecx
	addl	%r15d, %ecx
	movslq	%ecx, %rcx
	movswl	(%rax,%rcx,2), %ebp
	testl	%ebp, %ebp
	je	.LBB3_20
# BB#28:                                #   in Loop: Header=BB3_19 Depth=1
	movslq	%ebp, %rcx
	testw	%cx, %cx
	jns	.LBB3_36
# BB#29:                                #   in Loop: Header=BB3_19 Depth=1
	movq	280(%r13), %rax
	subq	%rcx, %rax
	movzbl	(%rax), %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	292(%r13), %eax
	addl	%ebp, %eax
	jne	.LBB3_31
# BB#30:                                #   in Loop: Header=BB3_19 Depth=1
	movl	$1, %eax
	subl	%ebp, %eax
	movl	%eax, 292(%r13)
.LBB3_31:                               #   in Loop: Header=BB3_19 Depth=1
	movl	4(%r13), %eax
	movl	276(%r13), %r14d
	leal	(%rax,%r14), %ecx
	cmpl	272(%r13), %ecx
	jle	.LBB3_33
# BB#32:                                #   in Loop: Header=BB3_19 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$36, %edi
	movl	$5, %esi
	movl	$.L.str.167, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	callq	Error
	movabsq	$4294967296, %r8        # imm = 0x100000000
	movl	4(%r13), %eax
	movl	276(%r13), %r14d
.LBB3_33:                               #   in Loop: Header=BB3_19 Depth=1
	leal	(%r14,%rax), %ecx
	movl	%ecx, 276(%r13)
	testl	%eax, %eax
	jle	.LBB3_35
# BB#34:                                # %.lr.ph.i105
                                        #   in Loop: Header=BB3_19 Depth=1
	movq	264(%r13), %rax
	movslq	%r14d, %rdx
	leaq	(%rax,%rdx,2), %rdi
	leal	1(%rdx), %eax
	cmpl	%eax, %ecx
	cmovgel	%ecx, %eax
	notl	%edx
	addl	%eax, %edx
	leaq	2(%rdx,%rdx), %rdx
	xorl	%esi, %esi
	callq	memset
	movabsq	$4294967296, %r8        # imm = 0x100000000
.LBB3_35:                               # %NewTrieNode.exit106
                                        #   in Loop: Header=BB3_19 Depth=1
	movl	%r14d, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%r14d, %ecx
	movl	%ecx, %edx
	sarl	$2, %edx
	movq	264(%r13), %rax
	movzbl	(%rbx), %esi
	movslq	%r15d, %rdi
	addq	%rsi, %rdi
	movw	%dx, (%rax,%rdi,2)
	decl	%ebp
	andl	$-4, %ecx
	movslq	%ecx, %rcx
	addq	8(%rsp), %rcx           # 8-byte Folded Reload
	movw	%bp, (%rax,%rcx,2)
	movl	%edx, %ebp
	movq	(%rsp), %r14            # 8-byte Reload
.LBB3_36:                               #   in Loop: Header=BB3_19 Depth=1
	movl	%ebp, %r15d
	shll	$2, %r15d
	movzbl	1(%rbx), %ecx
	incq	%rbx
	addq	%r8, %r12
	testb	%cl, %cl
	jne	.LBB3_19
# BB#23:                                # %._crit_edge.loopexit
	movslq	%r15d, %rbx
.LBB3_24:                               # %._crit_edge
	cmpw	$0, (%rax,%rbx,2)
	je	.LBB3_26
# BB#25:
	movq	no_fpos(%rip), %r8
	movl	$36, %edi
	movl	$7, %esi
	movl	$.L.str.166, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r14, %r9
	callq	Error
	jmp	.LBB3_38
.LBB3_20:
	movl	292(%r13), %ebp
	leaq	16(%rsp), %rdi
	callq	strlen
	subq	%rax, %rbp
	addq	$65535, %rbp            # imm = 0xFFFF
	xorl	%r14d, %r14d
	testw	%bp, %bp
	js	.LBB3_39
# BB#21:
	movswl	%bp, %eax
	movl	%eax, 292(%r13)
	movswq	%bp, %rdi
	addq	280(%r13), %rdi
	leaq	16(%rsp), %rsi
	callq	strcpy
	sarq	$32, %r12
	leaq	528(%rsp,%r12), %r12
	movl	292(%r13), %ebp
	movq	%r12, %rdi
	callq	strlen
	subq	%rax, %rbp
	addq	$65535, %rbp            # imm = 0xFFFF
	testw	%bp, %bp
	js	.LBB3_39
# BB#22:
	movswl	%bp, %eax
	movl	%eax, 292(%r13)
	movswq	%bp, %rdi
	addq	280(%r13), %rdi
	movq	%r12, %rsi
	callq	strcpy
	negl	%ebp
	movq	264(%r13), %rax
	movzbl	(%rbx), %ecx
	movslq	%r15d, %rdx
	addq	%rcx, %rdx
	movw	%bp, (%rax,%rdx,2)
	jmp	.LBB3_38
.LBB3_26:
	movl	292(%r13), %ebp
	leaq	16(%rsp), %rdi
	callq	strlen
	subq	%rax, %rbp
	addq	$65535, %rbp            # imm = 0xFFFF
	testw	%bp, %bp
	js	.LBB3_27
# BB#37:
	movswl	%bp, %eax
	movl	%eax, 292(%r13)
	movswq	%bp, %rdi
	addq	280(%r13), %rdi
	leaq	16(%rsp), %rsi
	callq	strcpy
	negl	%ebp
	movq	264(%r13), %rax
	movw	%bp, (%rax,%rbx,2)
.LBB3_38:                               # %NewTrieString.exit.thread
	movl	$1, %r14d
.LBB3_39:                               # %NewTrieString.exit.thread
	movl	%r14d, %eax
	addq	$1048, %rsp             # imm = 0x418
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_27:
	xorl	%r14d, %r14d
	jmp	.LBB3_39
.Lfunc_end3:
	.size	TrieInsert, .Lfunc_end3-TrieInsert
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"assert failed in %s"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"ReadHyphTable: lnum <= 0!"
	.size	.L.str.1, 26

	.type	HyphTables,@object      # @HyphTables
	.local	HyphTables
	.comm	HyphTables,512,16
	.type	TriedFile,@object       # @TriedFile
	.local	TriedFile
	.comm	TriedFile,256,16
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"ReadHyphTable!"
	.size	.L.str.2, 15

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Hyphenate: type(x) != ACAT!"
	.size	.L.str.3, 28

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"no current language for word %s"
	.size	.L.str.4, 32

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"0ch"
	.size	.L.str.6, 4

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"TrieRead: fname!"
	.size	.L.str.7, 17

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	".lp"
	.size	.L.str.8, 4

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	".lh"
	.size	.L.str.9, 4

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"cannot open hyphenation file %s"
	.size	.L.str.10, 32

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Lout hyphenation information\n"
	.size	.L.str.11, 30

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Lout hyphenation placeholder\n"
	.size	.L.str.12, 30

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"header line of hyphenation file %s missing"
	.size	.L.str.13, 43

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"%s%n"
	.size	.L.str.14, 5

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Classes:"
	.size	.L.str.15, 9

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Classes heading of hyphenation file %s missing"
	.size	.L.str.16, 47

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Exceptions:"
	.size	.L.str.17, 12

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Patterns:"
	.size	.L.str.18, 10

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"LengthLimit:"
	.size	.L.str.19, 13

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"hyphenation file %s%s is too large (at line %d)"
	.size	.L.str.20, 48

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"%d"
	.size	.L.str.21, 3

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"bad LengthLimit in hyphenation file %s%s (line %d)"
	.size	.L.str.22, 51

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"TrieRead: state"
	.size	.L.str.23, 16

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"format error in hyphenation file %s"
	.size	.L.str.24, 36

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"run out of memory while constructing hyphenation table"
	.size	.L.str.25, 55

	.type	tex_codes,@object       # @tex_codes
	.section	.rodata,"a",@progbits
	.p2align	4
tex_codes:
	.quad	.L.str.27
	.quad	.L.str.28
	.quad	.L.str.29
	.quad	.L.str.30
	.quad	.L.str.31
	.quad	.L.str.32
	.quad	.L.str.33
	.quad	.L.str.34
	.quad	.L.str.35
	.quad	.L.str.36
	.quad	.L.str.37
	.quad	.L.str.38
	.quad	.L.str.39
	.quad	.L.str.40
	.quad	.L.str.41
	.quad	.L.str.42
	.quad	.L.str.43
	.quad	.L.str.44
	.quad	.L.str.45
	.quad	.L.str.46
	.quad	.L.str.47
	.quad	.L.str.48
	.quad	.L.str.49
	.quad	.L.str.50
	.quad	.L.str.51
	.quad	.L.str.52
	.quad	.L.str.53
	.quad	.L.str.54
	.quad	.L.str.55
	.quad	.L.str.56
	.quad	.L.str.57
	.quad	.L.str.58
	.quad	.L.str.59
	.quad	.L.str.60
	.quad	.L.str.61
	.quad	.L.str.62
	.quad	.L.str.63
	.quad	.L.str.64
	.quad	.L.str.65
	.quad	.L.str.66
	.quad	.L.str.67
	.quad	.L.str.68
	.quad	.L.str.69
	.quad	.L.str.70
	.quad	.L.str.71
	.quad	.L.str.72
	.quad	.L.str.73
	.quad	.L.str.74
	.quad	.L.str.75
	.quad	.L.str.76
	.quad	.L.str.77
	.quad	.L.str.78
	.quad	.L.str.79
	.quad	.L.str.80
	.quad	.L.str.81
	.quad	.L.str.82
	.quad	.L.str.83
	.quad	.L.str.84
	.quad	.L.str.85
	.quad	.L.str.86
	.quad	.L.str.87
	.quad	.L.str.88
	.quad	.L.str.89
	.quad	.L.str.90
	.quad	.L.str.91
	.quad	.L.str.92
	.quad	.L.str.93
	.quad	.L.str.94
	.quad	.L.str.95
	.quad	.L.str.96
	.quad	.L.str.97
	.quad	.L.str.98
	.quad	.L.str.99
	.quad	.L.str.100
	.quad	.L.str.101
	.quad	.L.str.102
	.quad	.L.str.103
	.quad	.L.str.104
	.quad	.L.str.105
	.quad	.L.str.106
	.quad	.L.str.107
	.quad	.L.str.108
	.quad	.L.str.109
	.quad	.L.str.110
	.quad	.L.str.111
	.quad	.L.str.112
	.quad	.L.str.113
	.quad	.L.str.114
	.quad	.L.str.115
	.quad	.L.str.116
	.quad	.L.str.117
	.quad	.L.str.118
	.quad	.L.str.119
	.quad	.L.str.120
	.quad	.L.str.121
	.quad	.L.str.122
	.quad	.L.str.123
	.quad	.L.str.124
	.quad	.L.str.125
	.quad	.L.str.126
	.quad	.L.str.127
	.quad	.L.str.128
	.quad	.L.str.129
	.quad	.L.str.130
	.quad	.L.str.131
	.quad	.L.str.132
	.quad	.L.str.133
	.quad	.L.str.134
	.quad	.L.str.135
	.quad	.L.str.136
	.quad	.L.str.137
	.quad	.L.str.138
	.quad	.L.str.139
	.quad	.L.str.140
	.quad	.L.str.141
	.quad	.L.str.142
	.quad	.L.str.143
	.quad	.L.str.144
	.quad	.L.str.145
	.quad	.L.str.146
	.quad	.L.str.147
	.quad	.L.str.148
	.quad	.L.str.149
	.quad	.L.str.150
	.quad	.L.str.151
	.quad	.L.str.152
	.quad	.L.str.153
	.quad	.L.str.154
	.quad	.L.str.155
	.quad	.L.str.156
	.quad	.L.str.157
	.quad	.L.str.158
	.quad	.L.str.159
	.quad	.L.str.160
	.quad	.L.str.161
	.quad	.L.str.162
	.quad	.L.str.162
	.quad	.L.str.162
	.size	tex_codes, 1104

	.type	.L.str.26,@object       # @.str.26
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.26:
	.asciz	"in hyphenation file %s, unknown escape sequence (line %d)"
	.size	.L.str.26, 58

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"Agrave"
	.size	.L.str.27, 7

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"`A"
	.size	.L.str.28, 3

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"\300"
	.size	.L.str.29, 2

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"Aacute"
	.size	.L.str.30, 7

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"'A"
	.size	.L.str.31, 3

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"\301"
	.size	.L.str.32, 2

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"Acircumflex"
	.size	.L.str.33, 12

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"^A"
	.size	.L.str.34, 3

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"\302"
	.size	.L.str.35, 2

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"Atilde"
	.size	.L.str.36, 7

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"~A"
	.size	.L.str.37, 3

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"\303"
	.size	.L.str.38, 2

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"Adieresis"
	.size	.L.str.39, 10

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"\"A"
	.size	.L.str.40, 3

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"\304"
	.size	.L.str.41, 2

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"agrave"
	.size	.L.str.42, 7

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"`a"
	.size	.L.str.43, 3

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"\340"
	.size	.L.str.44, 2

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"aacute"
	.size	.L.str.45, 7

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"'a"
	.size	.L.str.46, 3

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"\341"
	.size	.L.str.47, 2

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"acircumflex"
	.size	.L.str.48, 12

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"^a"
	.size	.L.str.49, 3

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"\342"
	.size	.L.str.50, 2

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"atilde"
	.size	.L.str.51, 7

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"~a"
	.size	.L.str.52, 3

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"\343"
	.size	.L.str.53, 2

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"adieresis"
	.size	.L.str.54, 10

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"\"a"
	.size	.L.str.55, 3

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"\344"
	.size	.L.str.56, 2

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"ccedilla"
	.size	.L.str.57, 9

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"cc"
	.size	.L.str.58, 3

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"\347"
	.size	.L.str.59, 2

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"Egrave"
	.size	.L.str.60, 7

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"`E"
	.size	.L.str.61, 3

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"\310"
	.size	.L.str.62, 2

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"Eacute"
	.size	.L.str.63, 7

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"'E"
	.size	.L.str.64, 3

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"\311"
	.size	.L.str.65, 2

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"Ecircumflex"
	.size	.L.str.66, 12

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"^E"
	.size	.L.str.67, 3

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"\312"
	.size	.L.str.68, 2

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"Edieresis"
	.size	.L.str.69, 10

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"\"E"
	.size	.L.str.70, 3

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"\313"
	.size	.L.str.71, 2

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"egrave"
	.size	.L.str.72, 7

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"`e"
	.size	.L.str.73, 3

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"\350"
	.size	.L.str.74, 2

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"eacute"
	.size	.L.str.75, 7

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"'e"
	.size	.L.str.76, 3

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"\351"
	.size	.L.str.77, 2

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"ecircumflex"
	.size	.L.str.78, 12

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"^e"
	.size	.L.str.79, 3

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"\352"
	.size	.L.str.80, 2

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"edieresis"
	.size	.L.str.81, 10

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"\"e"
	.size	.L.str.82, 3

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"\353"
	.size	.L.str.83, 2

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"Igrave"
	.size	.L.str.84, 7

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"`I"
	.size	.L.str.85, 3

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"\314"
	.size	.L.str.86, 2

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"Iacute"
	.size	.L.str.87, 7

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"'I"
	.size	.L.str.88, 3

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"\315"
	.size	.L.str.89, 2

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"Icircumflex"
	.size	.L.str.90, 12

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"^I"
	.size	.L.str.91, 3

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"\316"
	.size	.L.str.92, 2

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"Idieresis"
	.size	.L.str.93, 10

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"\"I"
	.size	.L.str.94, 3

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"\317"
	.size	.L.str.95, 2

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"igrave"
	.size	.L.str.96, 7

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"`\\i"
	.size	.L.str.97, 4

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"\354"
	.size	.L.str.98, 2

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"iacute"
	.size	.L.str.99, 7

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"'\\i"
	.size	.L.str.100, 4

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"\355"
	.size	.L.str.101, 2

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"icircumflex"
	.size	.L.str.102, 12

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"^\\i"
	.size	.L.str.103, 4

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"\356"
	.size	.L.str.104, 2

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"idieresis"
	.size	.L.str.105, 10

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"\"\\i"
	.size	.L.str.106, 4

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"\357"
	.size	.L.str.107, 2

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"Ograve"
	.size	.L.str.108, 7

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"`O"
	.size	.L.str.109, 3

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"\322"
	.size	.L.str.110, 2

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"Oacute"
	.size	.L.str.111, 7

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"'O"
	.size	.L.str.112, 3

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"\323"
	.size	.L.str.113, 2

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"Ocircumflex"
	.size	.L.str.114, 12

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"^O"
	.size	.L.str.115, 3

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"\324"
	.size	.L.str.116, 2

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"Otilde"
	.size	.L.str.117, 7

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"~O"
	.size	.L.str.118, 3

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"\325"
	.size	.L.str.119, 2

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"Odieresis"
	.size	.L.str.120, 10

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"\"O"
	.size	.L.str.121, 3

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"\326"
	.size	.L.str.122, 2

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"ograve"
	.size	.L.str.123, 7

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"`o"
	.size	.L.str.124, 3

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"\362"
	.size	.L.str.125, 2

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"oacute"
	.size	.L.str.126, 7

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"'o"
	.size	.L.str.127, 3

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"\363"
	.size	.L.str.128, 2

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"ocircumflex"
	.size	.L.str.129, 12

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"^o"
	.size	.L.str.130, 3

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"\364"
	.size	.L.str.131, 2

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"otilde"
	.size	.L.str.132, 7

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"~o"
	.size	.L.str.133, 3

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"\365"
	.size	.L.str.134, 2

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"odieresis"
	.size	.L.str.135, 10

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	"\"o"
	.size	.L.str.136, 3

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"\366"
	.size	.L.str.137, 2

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"Ugrave"
	.size	.L.str.138, 7

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"`U"
	.size	.L.str.139, 3

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"\331"
	.size	.L.str.140, 2

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"Uacute"
	.size	.L.str.141, 7

	.type	.L.str.142,@object      # @.str.142
.L.str.142:
	.asciz	"'U"
	.size	.L.str.142, 3

	.type	.L.str.143,@object      # @.str.143
.L.str.143:
	.asciz	"\332"
	.size	.L.str.143, 2

	.type	.L.str.144,@object      # @.str.144
.L.str.144:
	.asciz	"Ucircumflex"
	.size	.L.str.144, 12

	.type	.L.str.145,@object      # @.str.145
.L.str.145:
	.asciz	"^U"
	.size	.L.str.145, 3

	.type	.L.str.146,@object      # @.str.146
.L.str.146:
	.asciz	"\333"
	.size	.L.str.146, 2

	.type	.L.str.147,@object      # @.str.147
.L.str.147:
	.asciz	"Udieresis"
	.size	.L.str.147, 10

	.type	.L.str.148,@object      # @.str.148
.L.str.148:
	.asciz	"\"U"
	.size	.L.str.148, 3

	.type	.L.str.149,@object      # @.str.149
.L.str.149:
	.asciz	"\334"
	.size	.L.str.149, 2

	.type	.L.str.150,@object      # @.str.150
.L.str.150:
	.asciz	"ugrave"
	.size	.L.str.150, 7

	.type	.L.str.151,@object      # @.str.151
.L.str.151:
	.asciz	"`u"
	.size	.L.str.151, 3

	.type	.L.str.152,@object      # @.str.152
.L.str.152:
	.asciz	"\371"
	.size	.L.str.152, 2

	.type	.L.str.153,@object      # @.str.153
.L.str.153:
	.asciz	"uacute"
	.size	.L.str.153, 7

	.type	.L.str.154,@object      # @.str.154
.L.str.154:
	.asciz	"'u"
	.size	.L.str.154, 3

	.type	.L.str.155,@object      # @.str.155
.L.str.155:
	.asciz	"\372"
	.size	.L.str.155, 2

	.type	.L.str.156,@object      # @.str.156
.L.str.156:
	.asciz	"ucircumflex"
	.size	.L.str.156, 12

	.type	.L.str.157,@object      # @.str.157
.L.str.157:
	.asciz	"^u"
	.size	.L.str.157, 3

	.type	.L.str.158,@object      # @.str.158
.L.str.158:
	.asciz	"\373"
	.size	.L.str.158, 2

	.type	.L.str.159,@object      # @.str.159
.L.str.159:
	.asciz	"udieresis"
	.size	.L.str.159, 10

	.type	.L.str.160,@object      # @.str.160
.L.str.160:
	.asciz	"\"u"
	.size	.L.str.160, 3

	.type	.L.str.161,@object      # @.str.161
.L.str.161:
	.asciz	"\374"
	.size	.L.str.161, 2

	.type	.L.str.162,@object      # @.str.162
.L.str.162:
	.zero	1
	.size	.L.str.162, 1

	.type	.L.str.163,@object      # @.str.163
.L.str.163:
	.asciz	"AddClassToTrie: after insertion"
	.size	.L.str.163, 32

	.type	.L.str.164,@object      # @.str.164
.L.str.164:
	.asciz	"hyphenation class of %c may not be changed"
	.size	.L.str.164, 43

	.type	.L.str.165,@object      # @.str.165
.L.str.165:
	.asciz	"in hyphenation file %s, line %d: character (octal %o) is not in any class"
	.size	.L.str.165, 74

	.type	.L.str.166,@object      # @.str.166
.L.str.166:
	.asciz	"hyphenation string %s already inserted"
	.size	.L.str.166, 39

	.type	.L.str.167,@object      # @.str.167
.L.str.167:
	.asciz	"hyphenation trie node limit exceeded"
	.size	.L.str.167, 37


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
