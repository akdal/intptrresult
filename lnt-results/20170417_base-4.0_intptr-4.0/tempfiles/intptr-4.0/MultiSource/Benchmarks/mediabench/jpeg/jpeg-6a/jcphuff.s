	.text
	.file	"jcphuff.bc"
	.globl	jinit_phuff_encoder
	.p2align	4, 0x90
	.type	jinit_phuff_encoder,@function
jinit_phuff_encoder:                    # @jinit_phuff_encoder
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$184, %edx
	callq	*(%rax)
	movq	%rax, 488(%rbx)
	movq	$start_pass_phuff, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 120(%rax)
	movups	%xmm0, 152(%rax)
	movups	%xmm0, 136(%rax)
	movups	%xmm0, 168(%rax)
	movq	$0, 104(%rax)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	jinit_phuff_encoder, .Lfunc_end0-jinit_phuff_encoder
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_pass_phuff,@function
start_pass_phuff:                       # @start_pass_phuff
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 48
.Lcfi7:
	.cfi_offset %rbx, -48
.Lcfi8:
	.cfi_offset %r12, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r12
	movq	488(%r12), %r15
	movq	%r12, 64(%r15)
	movl	%r14d, 24(%r15)
	movl	404(%r12), %ebp
	cmpl	$0, 412(%r12)
	je	.LBB1_1
# BB#2:
	testl	%ebp, %ebp
	je	.LBB1_3
# BB#4:
	movq	$encode_mcu_AC_refine, 8(%r15)
	cmpq	$0, 104(%r15)
	jne	.LBB1_6
# BB#5:
	movq	8(%r12), %rax
	movl	$1, %esi
	movl	$1000, %edx             # imm = 0x3E8
	movq	%r12, %rdi
	callq	*(%rax)
	movq	%rax, 104(%r15)
	jmp	.LBB1_6
.LBB1_1:
	testl	%ebp, %ebp
	movl	$encode_mcu_DC_first, %eax
	movl	$encode_mcu_AC_first, %ecx
	cmoveq	%rax, %rcx
	movq	%rcx, 8(%r15)
	jmp	.LBB1_6
.LBB1_3:
	movq	$encode_mcu_DC_refine, 8(%r15)
.LBB1_6:
	testl	%r14d, %r14d
	movl	$finish_pass_gather_phuff, %eax
	movl	$finish_pass_phuff, %ecx
	cmovneq	%rax, %rcx
	movq	%rcx, 16(%r15)
	cmpl	$0, 316(%r12)
	jle	.LBB1_31
# BB#7:                                 # %.lr.ph
	testl	%ebp, %ebp
	je	.LBB1_17
# BB#8:                                 # %.lr.ph.split.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	320(%r12,%rbp,8), %rax
	movl	$0, 72(%r15,%rbp,4)
	movslq	24(%rax), %rbx
	cmpq	$3, %rbx
	movl	%ebx, 88(%r15)
	ja	.LBB1_12
# BB#10:                                #   in Loop: Header=BB1_9 Depth=1
	testl	%r14d, %r14d
	jne	.LBB1_13
# BB#11:                                #   in Loop: Header=BB1_9 Depth=1
	movq	152(%r12,%rbx,8), %rax
	testq	%rax, %rax
	jne	.LBB1_13
.LBB1_12:                               # %.lr.ph.split._crit_edge
                                        #   in Loop: Header=BB1_9 Depth=1
	movq	(%r12), %rax
	movl	$49, 40(%rax)
	movl	%ebx, 44(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
.LBB1_13:                               #   in Loop: Header=BB1_9 Depth=1
	testl	%r14d, %r14d
	je	.LBB1_29
# BB#14:                                #   in Loop: Header=BB1_9 Depth=1
	movq	152(%r15,%rbx,8), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_16
# BB#15:                                #   in Loop: Header=BB1_9 Depth=1
	movq	8(%r12), %rax
	movl	$1, %esi
	movl	$2056, %edx             # imm = 0x808
	movq	%r12, %rdi
	callq	*(%rax)
	movq	%rax, %rdi
	movq	%rdi, 152(%r15,%rbx,8)
.LBB1_16:                               #   in Loop: Header=BB1_9 Depth=1
	xorl	%esi, %esi
	movl	$2056, %edx             # imm = 0x808
	callq	memset
	jmp	.LBB1_30
	.p2align	4, 0x90
.LBB1_29:                               #   in Loop: Header=BB1_9 Depth=1
	movq	152(%r12,%rbx,8), %rsi
	leaq	120(%r15,%rbx,8), %rdx
	movq	%r12, %rdi
	callq	jpeg_make_c_derived_tbl
.LBB1_30:                               #   in Loop: Header=BB1_9 Depth=1
	incq	%rbp
	movslq	316(%r12), %rax
	cmpq	%rax, %rbp
	jl	.LBB1_9
	jmp	.LBB1_31
.LBB1_17:                               # %.lr.ph.split.us.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_18:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	320(%r12,%rbp,8), %rax
	movl	$0, 72(%r15,%rbp,4)
	cmpl	$0, 412(%r12)
	jne	.LBB1_28
# BB#19:                                #   in Loop: Header=BB1_18 Depth=1
	movslq	20(%rax), %rbx
	cmpq	$3, %rbx
	ja	.LBB1_22
# BB#20:                                #   in Loop: Header=BB1_18 Depth=1
	testl	%r14d, %r14d
	jne	.LBB1_23
# BB#21:                                #   in Loop: Header=BB1_18 Depth=1
	movq	120(%r12,%rbx,8), %rax
	testq	%rax, %rax
	jne	.LBB1_23
.LBB1_22:                               # %._crit_edge96
                                        #   in Loop: Header=BB1_18 Depth=1
	movq	(%r12), %rax
	movl	$49, 40(%rax)
	movl	%ebx, 44(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
.LBB1_23:                               #   in Loop: Header=BB1_18 Depth=1
	testl	%r14d, %r14d
	je	.LBB1_24
# BB#25:                                #   in Loop: Header=BB1_18 Depth=1
	movq	152(%r15,%rbx,8), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_27
# BB#26:                                #   in Loop: Header=BB1_18 Depth=1
	movq	8(%r12), %rax
	movl	$1, %esi
	movl	$2056, %edx             # imm = 0x808
	movq	%r12, %rdi
	callq	*(%rax)
	movq	%rax, %rdi
	movq	%rdi, 152(%r15,%rbx,8)
.LBB1_27:                               #   in Loop: Header=BB1_18 Depth=1
	xorl	%esi, %esi
	movl	$2056, %edx             # imm = 0x808
	callq	memset
	jmp	.LBB1_28
.LBB1_24:                               #   in Loop: Header=BB1_18 Depth=1
	movq	120(%r12,%rbx,8), %rsi
	leaq	120(%r15,%rbx,8), %rdx
	movq	%r12, %rdi
	callq	jpeg_make_c_derived_tbl
	.p2align	4, 0x90
.LBB1_28:                               #   in Loop: Header=BB1_18 Depth=1
	incq	%rbp
	movslq	316(%r12), %rax
	cmpq	%rax, %rbp
	jl	.LBB1_18
.LBB1_31:                               # %._crit_edge
	movl	$0, 92(%r15)
	movl	$0, 96(%r15)
	movq	$0, 48(%r15)
	movl	$0, 56(%r15)
	movl	272(%r12), %eax
	movl	%eax, 112(%r15)
	movl	$0, 116(%r15)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	start_pass_phuff, .Lfunc_end1-start_pass_phuff
	.cfi_endproc

	.p2align	4, 0x90
	.type	encode_mcu_DC_first,@function
encode_mcu_DC_first:                    # @encode_mcu_DC_first
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 64
.Lcfi19:
	.cfi_offset %rbx, -56
.Lcfi20:
	.cfi_offset %r12, -48
.Lcfi21:
	.cfi_offset %r13, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdi, %r14
	movq	488(%r14), %r13
	movl	416(%r14), %r12d
	movq	32(%r14), %rax
	movups	(%rax), %xmm0
	movups	%xmm0, 32(%r13)
	cmpl	$0, 272(%r14)
	je	.LBB2_3
# BB#1:
	cmpl	$0, 112(%r13)
	jne	.LBB2_3
# BB#2:
	movl	116(%r13), %esi
	movq	%r13, %rdi
	callq	emit_restart
.LBB2_3:                                # %.preheader
	cmpl	$0, 360(%r14)
	jle	.LBB2_14
# BB#4:                                 # %.lr.ph74
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB2_5:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_7 Depth 2
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax,%r15,8), %rcx
	movslq	364(%r14,%r15,4), %rdx
	movq	320(%r14,%rdx,8), %rax
	movswl	(%rcx), %esi
	movl	%r12d, %ecx
	sarl	%cl, %esi
	movl	%esi, %ebp
	subl	72(%r13,%rdx,4), %ebp
	movl	%ebp, %ecx
	sarl	$31, %ecx
	addl	%ecx, %ebp
	movl	%esi, 72(%r13,%rdx,4)
	xorl	%ebp, %ecx
	movl	$0, %ebx
	je	.LBB2_8
# BB#6:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_5 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph
                                        #   Parent Loop BB2_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebx
	sarl	%ecx
	jne	.LBB2_7
.LBB2_8:                                # %._crit_edge
                                        #   in Loop: Header=BB2_5 Depth=1
	cmpl	$0, 24(%r13)
	movslq	20(%rax), %rax
	je	.LBB2_10
# BB#9:                                 #   in Loop: Header=BB2_5 Depth=1
	movq	152(%r13,%rax,8), %rax
	movslq	%ebx, %rcx
	incq	(%rax,%rcx,8)
	testl	%ebx, %ebx
	jne	.LBB2_12
	jmp	.LBB2_13
	.p2align	4, 0x90
.LBB2_10:                               #   in Loop: Header=BB2_5 Depth=1
	movq	120(%r13,%rax,8), %rax
	movslq	%ebx, %rcx
	movl	(%rax,%rcx,4), %esi
	movsbl	1024(%rax,%rcx), %edx
	movq	%r13, %rdi
	callq	emit_bits
	testl	%ebx, %ebx
	je	.LBB2_13
.LBB2_12:                               #   in Loop: Header=BB2_5 Depth=1
	movq	%r13, %rdi
	movl	%ebp, %esi
	movl	%ebx, %edx
	callq	emit_bits
.LBB2_13:                               #   in Loop: Header=BB2_5 Depth=1
	incq	%r15
	movslq	360(%r14), %rax
	cmpq	%rax, %r15
	jl	.LBB2_5
.LBB2_14:                               # %._crit_edge75
	movq	32(%r14), %rax
	movups	32(%r13), %xmm0
	movups	%xmm0, (%rax)
	movl	272(%r14), %eax
	testl	%eax, %eax
	je	.LBB2_18
# BB#15:
	movl	112(%r13), %ecx
	testl	%ecx, %ecx
	jne	.LBB2_17
# BB#16:
	movl	%eax, 112(%r13)
	movl	116(%r13), %ecx
	incl	%ecx
	andl	$7, %ecx
	movl	%ecx, 116(%r13)
	movl	%eax, %ecx
.LBB2_17:
	decl	%ecx
	movl	%ecx, 112(%r13)
.LBB2_18:
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	encode_mcu_DC_first, .Lfunc_end2-encode_mcu_DC_first
	.cfi_endproc

	.p2align	4, 0x90
	.type	encode_mcu_AC_first,@function
encode_mcu_AC_first:                    # @encode_mcu_AC_first
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi31:
	.cfi_def_cfa_offset 96
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movq	488(%rbp), %rbx
	movslq	408(%rbp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	416(%rbp), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	32(%rbp), %rax
	movups	(%rax), %xmm0
	movups	%xmm0, 32(%rbx)
	cmpl	$0, 272(%rbp)
	je	.LBB3_3
# BB#1:
	cmpl	$0, 112(%rbx)
	jne	.LBB3_3
# BB#2:
	movl	116(%rbx), %esi
	movq	%rbx, %rdi
	callq	emit_restart
.LBB3_3:
	movslq	404(%rbp), %r14
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpl	%eax, %r14d
	jg	.LBB3_30
# BB#4:                                 # %.lr.ph85
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	(%r15), %rsi
	xorl	%r13d, %r13d
	movq	%rax, %rdx
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_5:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_15 Depth 2
                                        #     Child Loop BB3_19 Depth 2
	movslq	jpeg_natural_order(,%r14,4), %rax
	movswl	(%rsi,%rax,2), %r12d
	testl	%r12d, %r12d
	je	.LBB3_6
# BB#7:                                 #   in Loop: Header=BB3_5 Depth=1
	testw	%r12w, %r12w
	js	.LBB3_8
# BB#9:                                 #   in Loop: Header=BB3_5 Depth=1
	movl	12(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %r12d
	movl	%r12d, %r15d
	testl	%r12d, %r12d
	jne	.LBB3_12
	jmp	.LBB3_11
	.p2align	4, 0x90
.LBB3_6:                                #   in Loop: Header=BB3_5 Depth=1
	incl	%r13d
	jmp	.LBB3_26
	.p2align	4, 0x90
.LBB3_8:                                #   in Loop: Header=BB3_5 Depth=1
	negl	%r12d
	movl	12(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %r12d
	movl	%r12d, %r15d
	notl	%r15d
	testl	%r12d, %r12d
	je	.LBB3_11
.LBB3_12:                               #   in Loop: Header=BB3_5 Depth=1
	cmpl	$0, 92(%rbx)
	je	.LBB3_14
# BB#13:                                #   in Loop: Header=BB3_5 Depth=1
	movq	%rbx, %rdi
	callq	emit_eobrun
.LBB3_14:                               # %.preheader81
                                        #   in Loop: Header=BB3_5 Depth=1
	cmpl	$16, %r13d
	movl	%r13d, %ebp
	jl	.LBB3_18
	.p2align	4, 0x90
.LBB3_15:                               # %.lr.ph
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, 24(%rbx)
	movslq	88(%rbx), %rax
	je	.LBB3_22
# BB#16:                                #   in Loop: Header=BB3_15 Depth=2
	movq	152(%rbx,%rax,8), %rax
	incq	1920(%rax)
	jmp	.LBB3_23
	.p2align	4, 0x90
.LBB3_22:                               #   in Loop: Header=BB3_15 Depth=2
	movq	120(%rbx,%rax,8), %rax
	movl	960(%rax), %esi
	movsbl	1264(%rax), %edx
	movq	%rbx, %rdi
	callq	emit_bits
.LBB3_23:                               # %emit_symbol.exit
                                        #   in Loop: Header=BB3_15 Depth=2
	addl	$-16, %ebp
	cmpl	$15, %ebp
	jg	.LBB3_15
# BB#17:                                # %.preheader.loopexit
                                        #   in Loop: Header=BB3_5 Depth=1
	andl	$15, %r13d
.LBB3_18:                               # %.preheader
                                        #   in Loop: Header=BB3_5 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_19:                               #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	sarl	%r12d
	incl	%ebp
	testl	%r12d, %r12d
	jne	.LBB3_19
# BB#20:                                #   in Loop: Header=BB3_5 Depth=1
	shll	$4, %r13d
	addl	%ebp, %r13d
	cmpl	$0, 24(%rbx)
	movslq	88(%rbx), %rax
	je	.LBB3_24
# BB#21:                                #   in Loop: Header=BB3_5 Depth=1
	movq	152(%rbx,%rax,8), %rax
	movslq	%r13d, %rcx
	incq	(%rax,%rcx,8)
	jmp	.LBB3_25
	.p2align	4, 0x90
.LBB3_11:                               #   in Loop: Header=BB3_5 Depth=1
	incl	%r13d
	jmp	.LBB3_26
.LBB3_24:                               #   in Loop: Header=BB3_5 Depth=1
	movq	120(%rbx,%rax,8), %rax
	movslq	%r13d, %rcx
	movl	(%rax,%rcx,4), %esi
	movsbl	1024(%rax,%rcx), %edx
	movq	%rbx, %rdi
	callq	emit_bits
.LBB3_25:                               # %emit_symbol.exit80
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	%rbx, %rdi
	movl	%r15d, %esi
	movl	%ebp, %edx
	callq	emit_bits
	xorl	%r13d, %r13d
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
.LBB3_26:                               #   in Loop: Header=BB3_5 Depth=1
	cmpq	%rdx, %r14
	leaq	1(%r14), %r14
	jl	.LBB3_5
# BB#27:                                # %._crit_edge
	testl	%r13d, %r13d
	movq	24(%rsp), %rbp          # 8-byte Reload
	jle	.LBB3_30
# BB#28:
	movl	92(%rbx), %eax
	incl	%eax
	movl	%eax, 92(%rbx)
	cmpl	$32767, %eax            # imm = 0x7FFF
	jne	.LBB3_30
# BB#29:
	movq	%rbx, %rdi
	callq	emit_eobrun
.LBB3_30:                               # %._crit_edge.thread
	movq	32(%rbp), %rax
	movups	32(%rbx), %xmm0
	movups	%xmm0, (%rax)
	movl	272(%rbp), %eax
	testl	%eax, %eax
	je	.LBB3_34
# BB#31:
	movl	112(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.LBB3_33
# BB#32:
	movl	%eax, 112(%rbx)
	movl	116(%rbx), %ecx
	incl	%ecx
	andl	$7, %ecx
	movl	%ecx, 116(%rbx)
	movl	%eax, %ecx
.LBB3_33:
	decl	%ecx
	movl	%ecx, 112(%rbx)
.LBB3_34:
	movl	$1, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	encode_mcu_AC_first, .Lfunc_end3-encode_mcu_AC_first
	.cfi_endproc

	.p2align	4, 0x90
	.type	encode_mcu_DC_refine,@function
encode_mcu_DC_refine:                   # @encode_mcu_DC_refine
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 96
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	488(%rdi), %rbx
	movl	416(%rdi), %ebp
	movq	32(%rdi), %rax
	movups	(%rax), %xmm0
	movups	%xmm0, 32(%rbx)
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	cmpl	$0, 272(%rdi)
	je	.LBB4_3
# BB#1:
	cmpl	$0, 112(%rbx)
	jne	.LBB4_3
# BB#2:
	movl	116(%rbx), %esi
	movq	%rbx, %rdi
	callq	emit_restart
.LBB4_3:                                # %.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	360(%rax), %eax
	testl	%eax, %eax
	jle	.LBB4_21
# BB#4:                                 # %.lr.ph
	xorl	%edx, %edx
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movl	%ebp, 16(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB4_5:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_8 Depth 2
	cmpl	$0, 24(%rbx)
	jne	.LBB4_20
# BB#6:                                 #   in Loop: Header=BB4_5 Depth=1
	movl	56(%rbx), %r13d
	movq	(%r15,%rdx,8), %rcx
	movswl	(%rcx), %r14d
	movl	%ebp, %ecx
	shrl	%cl, %r14d
	andl	$1, %r14d
	movl	$23, %ecx
	subl	%r13d, %ecx
	incl	%r13d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %r14
	orq	48(%rbx), %r14
	cmpl	$8, %r13d
	jl	.LBB4_19
# BB#7:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB4_5 Depth=1
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movl	%r13d, 20(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB4_8:                                # %.lr.ph.i
                                        #   Parent Loop BB4_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %r15
	shrq	$16, %r15
	movq	32(%rbx), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 32(%rbx)
	movb	%r15b, (%rax)
	decq	40(%rbx)
	jne	.LBB4_12
# BB#9:                                 #   in Loop: Header=BB4_8 Depth=2
	movq	64(%rbx), %rdi
	movq	32(%rdi), %r12
	callq	*24(%r12)
	testl	%eax, %eax
	jne	.LBB4_11
# BB#10:                                #   in Loop: Header=BB4_8 Depth=2
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$22, 40(%rax)
	callq	*(%rax)
.LBB4_11:                               # %dump_buffer.exit.i
                                        #   in Loop: Header=BB4_8 Depth=2
	movups	(%r12), %xmm0
	movups	%xmm0, 32(%rbx)
.LBB4_12:                               #   in Loop: Header=BB4_8 Depth=2
	cmpb	$-1, %r15b
	jne	.LBB4_17
# BB#13:                                #   in Loop: Header=BB4_8 Depth=2
	movq	32(%rbx), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 32(%rbx)
	movb	$0, (%rax)
	decq	40(%rbx)
	jne	.LBB4_17
# BB#14:                                #   in Loop: Header=BB4_8 Depth=2
	movq	64(%rbx), %rdi
	movq	32(%rdi), %rbp
	callq	*24(%rbp)
	testl	%eax, %eax
	jne	.LBB4_16
# BB#15:                                #   in Loop: Header=BB4_8 Depth=2
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$22, 40(%rax)
	callq	*(%rax)
.LBB4_16:                               # %dump_buffer.exit
                                        #   in Loop: Header=BB4_8 Depth=2
	movups	(%rbp), %xmm0
	movups	%xmm0, 32(%rbx)
	.p2align	4, 0x90
.LBB4_17:                               #   in Loop: Header=BB4_8 Depth=2
	shlq	$8, %r14
	addl	$-8, %r13d
	cmpl	$7, %r13d
	jg	.LBB4_8
# BB#18:                                # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB4_5 Depth=1
	movl	20(%rsp), %r13d         # 4-byte Reload
	andl	$7, %r13d
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	360(%rax), %eax
	movq	24(%rsp), %r15          # 8-byte Reload
	movl	16(%rsp), %ebp          # 4-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
.LBB4_19:                               # %._crit_edge.i
                                        #   in Loop: Header=BB4_5 Depth=1
	movq	%r14, 48(%rbx)
	movl	%r13d, 56(%rbx)
.LBB4_20:                               # %emit_bits.exit
                                        #   in Loop: Header=BB4_5 Depth=1
	incq	%rdx
	movslq	%eax, %rcx
	cmpq	%rcx, %rdx
	jl	.LBB4_5
.LBB4_21:                               # %._crit_edge
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	32(%rcx), %rax
	movups	32(%rbx), %xmm0
	movups	%xmm0, (%rax)
	movl	272(%rcx), %eax
	testl	%eax, %eax
	je	.LBB4_25
# BB#22:
	movl	112(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.LBB4_24
# BB#23:
	movl	%eax, 112(%rbx)
	movl	116(%rbx), %ecx
	incl	%ecx
	andl	$7, %ecx
	movl	%ecx, 116(%rbx)
	movl	%eax, %ecx
.LBB4_24:
	decl	%ecx
	movl	%ecx, 112(%rbx)
.LBB4_25:
	movl	$1, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	encode_mcu_DC_refine, .Lfunc_end4-encode_mcu_DC_refine
	.cfi_endproc

	.p2align	4, 0x90
	.type	encode_mcu_AC_refine,@function
encode_mcu_AC_refine:                   # @encode_mcu_AC_refine
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 56
	subq	$344, %rsp              # imm = 0x158
.Lcfi57:
	.cfi_def_cfa_offset 400
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	488(%rdi), %rbx
	movslq	408(%rdi), %r13
	movl	416(%rdi), %r14d
	movq	32(%rdi), %rax
	movups	(%rax), %xmm0
	movups	%xmm0, 32(%rbx)
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	cmpl	$0, 272(%rdi)
	je	.LBB5_3
# BB#1:
	cmpl	$0, 112(%rbx)
	jne	.LBB5_3
# BB#2:
	movl	116(%rbx), %esi
	movq	%rbx, %rdi
	callq	emit_restart
.LBB5_3:
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	404(%rax), %r8d
	cmpl	%r13d, %r8d
	jle	.LBB5_5
# BB#4:                                 # %._crit_edge158.thread
	leaq	96(%rbx), %rcx
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	testl	%ebp, %ebp
	je	.LBB5_78
	jmp	.LBB5_79
.LBB5_5:                                # %.lr.ph157.preheader
	movq	(%r15), %r12
	movslq	%r8d, %r15
	movq	%r15, %rsi
	decq	%rsi
	xorl	%edx, %edx
	movl	%r8d, %edi
	.p2align	4, 0x90
.LBB5_6:                                # %.lr.ph157
                                        # =>This Inner Loop Header: Depth=1
	movslq	jpeg_natural_order+4(,%rsi,4), %rcx
	movswl	(%r12,%rcx,2), %ecx
	movl	%ecx, %eax
	negl	%eax
	cmovll	%ecx, %eax
	movl	%r14d, %ecx
	sarl	%cl, %eax
	movl	%eax, 84(%rsp,%rsi,4)
	cmpl	$1, %eax
	cmovel	%edi, %edx
	incq	%rsi
	incl	%edi
	cmpq	%r13, %rsi
	jl	.LBB5_6
# BB#7:                                 # %._crit_edge158
	leaq	96(%rbx), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	cmpl	%r13d, %r8d
	jle	.LBB5_9
# BB#8:
	xorl	%eax, %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	testl	%ebp, %ebp
	je	.LBB5_78
	jmp	.LBB5_79
.LBB5_9:                                # %.lr.ph151
	movl	96(%rbx), %r14d
	addq	104(%rbx), %r14
	movslq	%edx, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	xorl	%ebp, %ebp
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movq	%r12, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB5_10:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_13 Depth 2
                                        #       Child Loop BB5_21 Depth 3
                                        #       Child Loop BB5_33 Depth 3
                                        #     Child Loop BB5_43 Depth 2
                                        #     Child Loop BB5_61 Depth 2
                                        #     Child Loop BB5_73 Depth 2
	movl	80(%rsp,%r15,4), %eax
	testl	%eax, %eax
	je	.LBB5_87
# BB#11:                                # %.preheader
                                        #   in Loop: Header=BB5_10 Depth=1
	cmpq	72(%rsp), %r15          # 8-byte Folded Reload
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movl	%eax, 52(%rsp)          # 4-byte Spill
	jg	.LBB5_36
# BB#12:                                # %.preheader
                                        #   in Loop: Header=BB5_10 Depth=1
	cmpl	$16, 8(%rsp)            # 4-byte Folded Reload
	jl	.LBB5_36
	.p2align	4, 0x90
.LBB5_13:                               # %.lr.ph
                                        #   Parent Loop BB5_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_21 Depth 3
                                        #       Child Loop BB5_33 Depth 3
	movq	%rbx, %rdi
	callq	emit_eobrun
	cmpl	$0, 24(%rbx)
	movslq	88(%rbx), %rax
	je	.LBB5_15
# BB#14:                                # %emit_symbol.exit.thread
                                        #   in Loop: Header=BB5_13 Depth=2
	movq	152(%rbx,%rax,8), %rax
	incq	1920(%rax)
	movl	8(%rsp), %eax           # 4-byte Reload
	addl	$-16, %eax
	jmp	.LBB5_35
	.p2align	4, 0x90
.LBB5_15:                               # %emit_symbol.exit
                                        #   in Loop: Header=BB5_13 Depth=2
	movq	120(%rbx,%rax,8), %rax
	movl	960(%rax), %esi
	movsbl	1264(%rax), %edx
	movq	%rbx, %rdi
	callq	emit_bits
	addl	$-16, 8(%rsp)           # 4-byte Folded Spill
	testl	%ebp, %ebp
	je	.LBB5_34
# BB#16:                                # %emit_symbol.exit
                                        #   in Loop: Header=BB5_13 Depth=2
	movl	24(%rbx), %eax
	testl	%eax, %eax
	jne	.LBB5_34
# BB#17:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB5_13 Depth=2
	xorl	%eax, %eax
	testl	%eax, %eax
	jne	.LBB5_33
	jmp	.LBB5_19
	.p2align	4, 0x90
.LBB5_31:                               # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB5_13 Depth=2
	andl	$7, %r15d
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	24(%rsp), %r12          # 8-byte Reload
.LBB5_32:                               # %._crit_edge.i
                                        #   in Loop: Header=BB5_13 Depth=2
	movq	%r14, 48(%rbx)
	movl	%r15d, 56(%rbx)
	movq	40(%rsp), %r15          # 8-byte Reload
	movl	12(%rsp), %ebp          # 4-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB5_33:                               # %emit_bits.exit
                                        #   Parent Loop BB5_10 Depth=1
                                        #     Parent Loop BB5_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	decl	%ebp
	je	.LBB5_34
# BB#88:                                # %emit_bits.exit..preheader.i_crit_edge
                                        #   in Loop: Header=BB5_33 Depth=3
	incq	%r14
	movl	24(%rbx), %eax
	testl	%eax, %eax
	jne	.LBB5_33
.LBB5_19:                               #   in Loop: Header=BB5_13 Depth=2
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movl	56(%rbx), %r15d
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movzbl	(%r14), %r14d
	andl	$1, %r14d
	movl	$23, %ecx
	subl	%r15d, %ecx
	incl	%r15d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %r14
	orq	48(%rbx), %r14
	cmpl	$8, %r15d
	jl	.LBB5_32
# BB#20:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB5_13 Depth=2
	movl	%r15d, %r13d
	.p2align	4, 0x90
.LBB5_21:                               # %.lr.ph.i
                                        #   Parent Loop BB5_10 Depth=1
                                        #     Parent Loop BB5_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r14, %r12
	shrq	$16, %r12
	movq	32(%rbx), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 32(%rbx)
	movb	%r12b, (%rax)
	decq	40(%rbx)
	jne	.LBB5_25
# BB#22:                                #   in Loop: Header=BB5_21 Depth=3
	movq	64(%rbx), %rdi
	movq	32(%rdi), %rbp
	callq	*24(%rbp)
	testl	%eax, %eax
	jne	.LBB5_24
# BB#23:                                #   in Loop: Header=BB5_21 Depth=3
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$22, 40(%rax)
	callq	*(%rax)
.LBB5_24:                               # %dump_buffer.exit.i
                                        #   in Loop: Header=BB5_21 Depth=3
	movups	(%rbp), %xmm0
	movups	%xmm0, 32(%rbx)
.LBB5_25:                               #   in Loop: Header=BB5_21 Depth=3
	cmpb	$-1, %r12b
	jne	.LBB5_30
# BB#26:                                #   in Loop: Header=BB5_21 Depth=3
	movq	32(%rbx), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 32(%rbx)
	movb	$0, (%rax)
	decq	40(%rbx)
	jne	.LBB5_30
# BB#27:                                #   in Loop: Header=BB5_21 Depth=3
	movq	64(%rbx), %rdi
	movq	32(%rdi), %rbp
	callq	*24(%rbp)
	testl	%eax, %eax
	jne	.LBB5_29
# BB#28:                                #   in Loop: Header=BB5_21 Depth=3
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$22, 40(%rax)
	callq	*(%rax)
.LBB5_29:                               # %dump_buffer.exit
                                        #   in Loop: Header=BB5_21 Depth=3
	movups	(%rbp), %xmm0
	movups	%xmm0, 32(%rbx)
	.p2align	4, 0x90
.LBB5_30:                               #   in Loop: Header=BB5_21 Depth=3
	shlq	$8, %r14
	addl	$-8, %r13d
	cmpl	$7, %r13d
	jg	.LBB5_21
	jmp	.LBB5_31
	.p2align	4, 0x90
.LBB5_34:                               #   in Loop: Header=BB5_13 Depth=2
	movl	8(%rsp), %eax           # 4-byte Reload
.LBB5_35:                               # %emit_buffered_bits.exit
                                        #   in Loop: Header=BB5_13 Depth=2
	movq	104(%rbx), %r14
	xorl	%ebp, %ebp
	movl	%eax, 8(%rsp)           # 4-byte Spill
	cmpl	$15, %eax
	jg	.LBB5_13
.LBB5_36:                               # %.critedge
                                        #   in Loop: Header=BB5_10 Depth=1
	movl	52(%rsp), %eax          # 4-byte Reload
	cmpl	$2, %eax
	jl	.LBB5_38
# BB#37:                                #   in Loop: Header=BB5_10 Depth=1
	andb	$1, %al
	movl	%eax, %ecx
	movl	%ebp, %eax
	incl	%ebp
	movb	%cl, (%r14,%rax)
	jmp	.LBB5_75
	.p2align	4, 0x90
.LBB5_87:                               #   in Loop: Header=BB5_10 Depth=1
	incl	8(%rsp)                 # 4-byte Folded Spill
	jmp	.LBB5_75
	.p2align	4, 0x90
.LBB5_38:                               #   in Loop: Header=BB5_10 Depth=1
	movq	%rbx, %rdi
	callq	emit_eobrun
	movl	8(%rsp), %ecx           # 4-byte Reload
	shll	$4, %ecx
	orl	$1, %ecx
	cmpl	$0, 24(%rbx)
	movslq	88(%rbx), %rax
	je	.LBB5_40
# BB#39:                                # %emit_symbol.exit112.thread
                                        #   in Loop: Header=BB5_10 Depth=1
	movq	152(%rbx,%rax,8), %rax
	movslq	%ecx, %rcx
	incq	(%rax,%rcx,8)
	jmp	.LBB5_74
.LBB5_40:                               # %emit_symbol.exit112
                                        #   in Loop: Header=BB5_10 Depth=1
	movq	120(%rbx,%rax,8), %rax
	movslq	%ecx, %rcx
	movl	(%rax,%rcx,4), %esi
	movsbl	1024(%rax,%rcx), %edx
	movq	%rbx, %rdi
	callq	emit_bits
	cmpl	$0, 24(%rbx)
	jne	.LBB5_74
# BB#41:                                #   in Loop: Header=BB5_10 Depth=1
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movq	%r15, %rbp
	movl	56(%rbx), %esi
	movslq	jpeg_natural_order(,%rbp,4), %rax
	movq	%r12, %rdx
	movzwl	(%r12,%rax,2), %r12d
	shrl	$15, %r12d
	notl	%r12d
	andl	$1, %r12d
	movl	$23, %ecx
	subl	%esi, %ecx
	incl	%esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %r12
	movl	%esi, %ecx
	orq	48(%rbx), %r12
	xorl	%eax, %eax
	cmpl	$8, %ecx
	jl	.LBB5_54
# BB#42:                                # %.lr.ph.i113.preheader
                                        #   in Loop: Header=BB5_10 Depth=1
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movl	%ecx, %r13d
	.p2align	4, 0x90
.LBB5_43:                               # %.lr.ph.i113
                                        #   Parent Loop BB5_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %r15
	movq	%r12, %r14
	shrq	$16, %r14
	movq	32(%rbx), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 32(%rbx)
	movb	%r14b, (%rax)
	decq	40(%rbx)
	jne	.LBB5_47
# BB#44:                                #   in Loop: Header=BB5_43 Depth=2
	movq	64(%rbx), %rdi
	movq	32(%rdi), %rbp
	callq	*24(%rbp)
	testl	%eax, %eax
	jne	.LBB5_46
# BB#45:                                #   in Loop: Header=BB5_43 Depth=2
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$22, 40(%rax)
	callq	*(%rax)
.LBB5_46:                               # %dump_buffer.exit.i116
                                        #   in Loop: Header=BB5_43 Depth=2
	movups	(%rbp), %xmm0
	movups	%xmm0, 32(%rbx)
.LBB5_47:                               #   in Loop: Header=BB5_43 Depth=2
	cmpb	$-1, %r14b
	movq	%r15, %r14
	jne	.LBB5_52
# BB#48:                                #   in Loop: Header=BB5_43 Depth=2
	movq	32(%rbx), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 32(%rbx)
	movb	$0, (%rax)
	decq	40(%rbx)
	jne	.LBB5_52
# BB#49:                                #   in Loop: Header=BB5_43 Depth=2
	movq	64(%rbx), %rdi
	movq	32(%rdi), %rbp
	callq	*24(%rbp)
	testl	%eax, %eax
	jne	.LBB5_51
# BB#50:                                #   in Loop: Header=BB5_43 Depth=2
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$22, 40(%rax)
	callq	*(%rax)
.LBB5_51:                               # %dump_buffer.exit137
                                        #   in Loop: Header=BB5_43 Depth=2
	movups	(%rbp), %xmm0
	movups	%xmm0, 32(%rbx)
	.p2align	4, 0x90
.LBB5_52:                               #   in Loop: Header=BB5_43 Depth=2
	shlq	$8, %r12
	addl	$-8, %r13d
	cmpl	$7, %r13d
	jg	.LBB5_43
# BB#53:                                # %._crit_edge.loopexit.i117
                                        #   in Loop: Header=BB5_10 Depth=1
	movl	16(%rsp), %ecx          # 4-byte Reload
	andl	$7, %ecx
	movl	24(%rbx), %eax
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
.LBB5_54:                               # %emit_bits.exit121
                                        #   in Loop: Header=BB5_10 Depth=1
	movq	%r12, 48(%rbx)
	movl	%ecx, 56(%rbx)
	movl	12(%rsp), %ecx          # 4-byte Reload
	testl	%ecx, %ecx
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	%rdx, %r12
	je	.LBB5_74
# BB#55:                                # %emit_bits.exit121
                                        #   in Loop: Header=BB5_10 Depth=1
	testl	%eax, %eax
	jne	.LBB5_74
# BB#56:                                # %.preheader.i126.preheader
                                        #   in Loop: Header=BB5_10 Depth=1
	xorl	%eax, %eax
	testl	%eax, %eax
	jne	.LBB5_73
	jmp	.LBB5_59
	.p2align	4, 0x90
.LBB5_71:                               # %._crit_edge.loopexit.i132
                                        #   in Loop: Header=BB5_10 Depth=1
	andl	$7, %r15d
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB5_72:                               # %._crit_edge.i135
                                        #   in Loop: Header=BB5_10 Depth=1
	movq	%r12, 48(%rbx)
	movl	%r15d, 56(%rbx)
	movq	40(%rsp), %r15          # 8-byte Reload
	movl	12(%rsp), %ecx          # 4-byte Reload
	movq	%rax, %r12
.LBB5_73:                               # %emit_bits.exit136
                                        #   Parent Loop BB5_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%ecx
	je	.LBB5_74
# BB#57:                                # %.preheader.i126thread-pre-split
                                        #   in Loop: Header=BB5_73 Depth=2
	incq	%r14
	movl	24(%rbx), %eax
	testl	%eax, %eax
	jne	.LBB5_73
.LBB5_59:                               #   in Loop: Header=BB5_10 Depth=1
	movq	%r12, %rax
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	56(%rbx), %r15d
	movzbl	(%r14), %r12d
	andl	$1, %r12d
	movl	$23, %ecx
	subl	%r15d, %ecx
	incl	%r15d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %r12
	orq	48(%rbx), %r12
	cmpl	$8, %r15d
	jl	.LBB5_72
# BB#60:                                # %.lr.ph.i128.preheader
                                        #   in Loop: Header=BB5_10 Depth=1
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movl	%r15d, %r13d
	.p2align	4, 0x90
.LBB5_61:                               # %.lr.ph.i128
                                        #   Parent Loop BB5_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r12, %r14
	shrq	$16, %r14
	movq	32(%rbx), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 32(%rbx)
	movb	%r14b, (%rax)
	decq	40(%rbx)
	jne	.LBB5_65
# BB#62:                                #   in Loop: Header=BB5_61 Depth=2
	movq	64(%rbx), %rdi
	movq	32(%rdi), %rbp
	callq	*24(%rbp)
	testl	%eax, %eax
	jne	.LBB5_64
# BB#63:                                #   in Loop: Header=BB5_61 Depth=2
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$22, 40(%rax)
	callq	*(%rax)
.LBB5_64:                               # %dump_buffer.exit.i131
                                        #   in Loop: Header=BB5_61 Depth=2
	movups	(%rbp), %xmm0
	movups	%xmm0, 32(%rbx)
.LBB5_65:                               #   in Loop: Header=BB5_61 Depth=2
	cmpb	$-1, %r14b
	jne	.LBB5_70
# BB#66:                                #   in Loop: Header=BB5_61 Depth=2
	movq	32(%rbx), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 32(%rbx)
	movb	$0, (%rax)
	decq	40(%rbx)
	jne	.LBB5_70
# BB#67:                                #   in Loop: Header=BB5_61 Depth=2
	movq	64(%rbx), %rdi
	movq	32(%rdi), %rbp
	callq	*24(%rbp)
	testl	%eax, %eax
	jne	.LBB5_69
# BB#68:                                #   in Loop: Header=BB5_61 Depth=2
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$22, 40(%rax)
	callq	*(%rax)
.LBB5_69:                               # %dump_buffer.exit138
                                        #   in Loop: Header=BB5_61 Depth=2
	movups	(%rbp), %xmm0
	movups	%xmm0, 32(%rbx)
	.p2align	4, 0x90
.LBB5_70:                               #   in Loop: Header=BB5_61 Depth=2
	shlq	$8, %r12
	addl	$-8, %r13d
	cmpl	$7, %r13d
	jg	.LBB5_61
	jmp	.LBB5_71
	.p2align	4, 0x90
.LBB5_74:                               # %emit_buffered_bits.exit127
                                        #   in Loop: Header=BB5_10 Depth=1
	movq	104(%rbx), %r14
	xorl	%ebp, %ebp
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
.LBB5_75:                               #   in Loop: Header=BB5_10 Depth=1
	cmpq	%r13, %r15
	leaq	1(%r15), %r15
	jl	.LBB5_10
# BB#76:
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	8(%rsp), %eax           # 4-byte Reload
	testl	%ebp, %ebp
	jne	.LBB5_79
.LBB5_78:                               # %._crit_edge
	testl	%eax, %eax
	jle	.LBB5_82
.LBB5_79:
	movl	92(%rbx), %eax
	incl	%eax
	movl	%eax, 92(%rbx)
	addl	(%rcx), %ebp
	movl	%ebp, (%rcx)
	cmpl	$32767, %eax            # imm = 0x7FFF
	je	.LBB5_81
# BB#80:
	cmpl	$938, %ebp              # imm = 0x3AA
	jb	.LBB5_82
.LBB5_81:
	movq	%rbx, %rdi
	callq	emit_eobrun
.LBB5_82:
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	32(%rcx), %rax
	movups	32(%rbx), %xmm0
	movups	%xmm0, (%rax)
	movl	272(%rcx), %eax
	testl	%eax, %eax
	je	.LBB5_86
# BB#83:
	movl	112(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.LBB5_85
# BB#84:
	movl	%eax, 112(%rbx)
	movl	116(%rbx), %ecx
	incl	%ecx
	andl	$7, %ecx
	movl	%ecx, 116(%rbx)
	movl	%eax, %ecx
.LBB5_85:
	decl	%ecx
	movl	%ecx, 112(%rbx)
.LBB5_86:
	movl	$1, %eax
	addq	$344, %rsp              # imm = 0x158
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	encode_mcu_AC_refine, .Lfunc_end5-encode_mcu_AC_refine
	.cfi_endproc

	.p2align	4, 0x90
	.type	finish_pass_gather_phuff,@function
finish_pass_gather_phuff:               # @finish_pass_gather_phuff
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi68:
	.cfi_def_cfa_offset 64
.Lcfi69:
	.cfi_offset %rbx, -40
.Lcfi70:
	.cfi_offset %r12, -32
.Lcfi71:
	.cfi_offset %r14, -24
.Lcfi72:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	488(%r15), %r14
	movq	%r14, %rdi
	callq	emit_eobrun
	movl	404(%r15), %ecx
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	316(%r15), %eax
	testl	%eax, %eax
	jle	.LBB6_13
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	testl	%ecx, %ecx
	je	.LBB6_7
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	320(%r15,%rbx,8), %rcx
	movslq	24(%rcx), %r12
	cmpl	$0, (%rsp,%r12,4)
	jne	.LBB6_6
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	152(%r15,%r12,8), %rsi
	testq	%rsi, %rsi
	jne	.LBB6_5
# BB#4:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	%r15, %rdi
	callq	jpeg_alloc_huff_table
	movq	%rax, %rsi
	movq	%rsi, 152(%r15,%r12,8)
.LBB6_5:                                #   in Loop: Header=BB6_2 Depth=1
	movq	152(%r14,%r12,8), %rdx
	movq	%r15, %rdi
	callq	jpeg_gen_optimal_table
	movl	$1, (%rsp,%r12,4)
	movl	316(%r15), %eax
.LBB6_6:                                #   in Loop: Header=BB6_2 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB6_2
	jmp	.LBB6_13
	.p2align	4, 0x90
.LBB6_7:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, 412(%r15)
	jne	.LBB6_12
# BB#8:                                 #   in Loop: Header=BB6_7 Depth=1
	movq	320(%r15,%rbx,8), %rcx
	movslq	20(%rcx), %r12
	cmpl	$0, (%rsp,%r12,4)
	jne	.LBB6_12
# BB#9:                                 #   in Loop: Header=BB6_7 Depth=1
	movq	120(%r15,%r12,8), %rsi
	testq	%rsi, %rsi
	jne	.LBB6_11
# BB#10:                                #   in Loop: Header=BB6_7 Depth=1
	movq	%r15, %rdi
	callq	jpeg_alloc_huff_table
	movq	%rax, %rsi
	movq	%rsi, 120(%r15,%r12,8)
.LBB6_11:                               #   in Loop: Header=BB6_7 Depth=1
	movq	152(%r14,%r12,8), %rdx
	movq	%r15, %rdi
	callq	jpeg_gen_optimal_table
	movl	$1, (%rsp,%r12,4)
	movl	316(%r15), %eax
	.p2align	4, 0x90
.LBB6_12:                               #   in Loop: Header=BB6_7 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB6_7
.LBB6_13:                               # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	finish_pass_gather_phuff, .Lfunc_end6-finish_pass_gather_phuff
	.cfi_endproc

	.p2align	4, 0x90
	.type	finish_pass_phuff,@function
finish_pass_phuff:                      # @finish_pass_phuff
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi75:
	.cfi_def_cfa_offset 32
.Lcfi76:
	.cfi_offset %rbx, -24
.Lcfi77:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	32(%r14), %rax
	movq	488(%r14), %rbx
	movups	(%rax), %xmm0
	movups	%xmm0, 32(%rbx)
	movq	%rbx, %rdi
	callq	emit_eobrun
	movq	%rbx, %rdi
	callq	flush_bits
	movq	32(%r14), %rax
	movups	32(%rbx), %xmm0
	movups	%xmm0, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	finish_pass_phuff, .Lfunc_end7-finish_pass_phuff
	.cfi_endproc

	.p2align	4, 0x90
	.type	emit_restart,@function
emit_restart:                           # @emit_restart
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 32
.Lcfi81:
	.cfi_offset %rbx, -32
.Lcfi82:
	.cfi_offset %r14, -24
.Lcfi83:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	callq	emit_eobrun
	cmpl	$0, 24(%rbx)
	jne	.LBB8_10
# BB#1:
	movq	%rbx, %rdi
	callq	flush_bits
	movq	32(%rbx), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 32(%rbx)
	movb	$-1, (%rax)
	decq	40(%rbx)
	je	.LBB8_3
# BB#2:                                 # %._crit_edge
	movq	32(%rbx), %rax
	jmp	.LBB8_6
.LBB8_3:
	movq	64(%rbx), %rdi
	movq	32(%rdi), %r14
	callq	*24(%r14)
	testl	%eax, %eax
	jne	.LBB8_5
# BB#4:
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$22, 40(%rax)
	callq	*(%rax)
.LBB8_5:                                # %dump_buffer.exit
	movdqu	(%r14), %xmm0
	movdqu	%xmm0, 32(%rbx)
	movd	%xmm0, %rax
.LBB8_6:
	addl	$208, %ebp
	leaq	1(%rax), %rcx
	movq	%rcx, 32(%rbx)
	movb	%bpl, (%rax)
	decq	40(%rbx)
	jne	.LBB8_10
# BB#7:
	movq	64(%rbx), %rdi
	movq	32(%rdi), %rbp
	callq	*24(%rbp)
	testl	%eax, %eax
	jne	.LBB8_9
# BB#8:
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$22, 40(%rax)
	callq	*(%rax)
.LBB8_9:                                # %dump_buffer.exit18
	movdqu	(%rbp), %xmm0
	movdqu	%xmm0, 32(%rbx)
.LBB8_10:
	movq	64(%rbx), %rax
	cmpl	$0, 404(%rax)
	je	.LBB8_11
# BB#14:
	movq	$0, 92(%rbx)
	jmp	.LBB8_15
.LBB8_11:                               # %.preheader
	cmpl	$0, 316(%rax)
	jle	.LBB8_15
# BB#12:                                # %.lr.ph.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_13:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, 72(%rbx,%rax,4)
	incq	%rax
	movq	64(%rbx), %rcx
	movslq	316(%rcx), %rcx
	cmpq	%rcx, %rax
	jl	.LBB8_13
.LBB8_15:                               # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end8:
	.size	emit_restart, .Lfunc_end8-emit_restart
	.cfi_endproc

	.p2align	4, 0x90
	.type	emit_bits,@function
emit_bits:                              # @emit_bits
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi87:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi88:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi90:
	.cfi_def_cfa_offset 64
.Lcfi91:
	.cfi_offset %rbx, -56
.Lcfi92:
	.cfi_offset %r12, -48
.Lcfi93:
	.cfi_offset %r13, -40
.Lcfi94:
	.cfi_offset %r14, -32
.Lcfi95:
	.cfi_offset %r15, -24
.Lcfi96:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebp
	movq	%rdi, %r12
	movl	56(%r12), %r15d
	testl	%r14d, %r14d
	jne	.LBB9_2
# BB#1:
	movq	64(%r12), %rdi
	movq	(%rdi), %rax
	movl	$39, 40(%rax)
	callq	*(%rax)
.LBB9_2:
	cmpl	$0, 24(%r12)
	jne	.LBB9_15
# BB#3:
	movl	%ebp, %eax
	movl	$1, %r13d
	movl	%r14d, %ecx
	shlq	%cl, %r13
	decl	%r13d
	andq	%rax, %r13
	leal	(%r15,%r14), %ebx
	movl	$24, %ecx
	subl	%ebx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %r13
	orq	48(%r12), %r13
	cmpl	$8, %ebx
	jl	.LBB9_14
# BB#4:                                 # %.lr.ph
	addl	%r14d, %r15d
	.p2align	4, 0x90
.LBB9_5:                                # =>This Inner Loop Header: Depth=1
	movq	%r13, %rbp
	shrq	$16, %rbp
	movq	32(%r12), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 32(%r12)
	movb	%bpl, (%rax)
	decq	40(%r12)
	jne	.LBB9_9
# BB#6:                                 #   in Loop: Header=BB9_5 Depth=1
	movq	64(%r12), %rdi
	movq	32(%rdi), %r14
	callq	*24(%r14)
	testl	%eax, %eax
	jne	.LBB9_8
# BB#7:                                 #   in Loop: Header=BB9_5 Depth=1
	movq	64(%r12), %rdi
	movq	(%rdi), %rax
	movl	$22, 40(%rax)
	callq	*(%rax)
.LBB9_8:                                # %dump_buffer.exit
                                        #   in Loop: Header=BB9_5 Depth=1
	movups	(%r14), %xmm0
	movups	%xmm0, 32(%r12)
.LBB9_9:                                #   in Loop: Header=BB9_5 Depth=1
	cmpb	$-1, %bpl
	jne	.LBB9_12
# BB#10:                                #   in Loop: Header=BB9_5 Depth=1
	movq	32(%r12), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 32(%r12)
	movb	$0, (%rax)
	decq	40(%r12)
	jne	.LBB9_12
# BB#11:                                #   in Loop: Header=BB9_5 Depth=1
	movq	%r12, %rdi
	callq	dump_buffer
	.p2align	4, 0x90
.LBB9_12:                               #   in Loop: Header=BB9_5 Depth=1
	shlq	$8, %r13
	addl	$-8, %ebx
	cmpl	$7, %ebx
	jg	.LBB9_5
# BB#13:                                # %._crit_edge.loopexit
	andl	$7, %r15d
	movl	%r15d, %ebx
.LBB9_14:                               # %._crit_edge
	movq	%r13, 48(%r12)
	movl	%ebx, 56(%r12)
.LBB9_15:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	emit_bits, .Lfunc_end9-emit_bits
	.cfi_endproc

	.p2align	4, 0x90
	.type	emit_eobrun,@function
emit_eobrun:                            # @emit_eobrun
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi97:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi98:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi100:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi101:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi103:
	.cfi_def_cfa_offset 80
.Lcfi104:
	.cfi_offset %rbx, -56
.Lcfi105:
	.cfi_offset %r12, -48
.Lcfi106:
	.cfi_offset %r13, -40
.Lcfi107:
	.cfi_offset %r14, -32
.Lcfi108:
	.cfi_offset %r15, -24
.Lcfi109:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movl	92(%r13), %ecx
	testl	%ecx, %ecx
	je	.LBB10_29
# BB#1:                                 # %.preheader.preheader
	movl	$-1, %ebp
	movl	$-16, %eax
	.p2align	4, 0x90
.LBB10_2:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	sarl	%ecx
	incl	%ebp
	addl	$16, %eax
	testl	%ecx, %ecx
	jne	.LBB10_2
# BB#3:
	cmpl	$0, 24(%r13)
	movslq	88(%r13), %rcx
	je	.LBB10_5
# BB#4:
	movq	152(%r13,%rcx,8), %rcx
	cltq
	incq	(%rcx,%rax,8)
	testl	%ebp, %ebp
	jne	.LBB10_7
	jmp	.LBB10_8
.LBB10_5:
	movq	120(%r13,%rcx,8), %rcx
	cltq
	movl	(%rcx,%rax,4), %esi
	movsbl	1024(%rcx,%rax), %edx
	movq	%r13, %rdi
	callq	emit_bits
	testl	%ebp, %ebp
	je	.LBB10_8
.LBB10_7:
	movl	92(%r13), %esi
	movq	%r13, %rdi
	movl	%ebp, %edx
	callq	emit_bits
.LBB10_8:
	movl	$0, 92(%r13)
	movl	96(%r13), %r12d
	testl	%r12d, %r12d
	je	.LBB10_28
# BB#9:
	movl	24(%r13), %eax
	testl	%eax, %eax
	jne	.LBB10_28
# BB#10:                                # %.preheader.i.preheader
	movq	104(%r13), %rdx
	xorl	%eax, %eax
	testl	%eax, %eax
	jne	.LBB10_26
	jmp	.LBB10_12
	.p2align	4, 0x90
.LBB10_24:                              # %._crit_edge.loopexit.i
	movl	12(%rsp), %eax          # 4-byte Reload
	andl	$7, %eax
	movq	16(%rsp), %rdx          # 8-byte Reload
.LBB10_25:                              # %._crit_edge.i
	movq	%r14, 48(%r13)
	movl	%eax, 56(%r13)
.LBB10_26:                              # %emit_bits.exit
                                        # =>This Inner Loop Header: Depth=1
	decl	%r12d
	je	.LBB10_28
# BB#27:                                # %emit_bits.exit..preheader.i_crit_edge
                                        #   in Loop: Header=BB10_26 Depth=1
	incq	%rdx
	movl	24(%r13), %eax
	testl	%eax, %eax
	jne	.LBB10_26
.LBB10_12:
	movl	56(%r13), %eax
	movzbl	(%rdx), %r14d
	andl	$1, %r14d
	movl	$23, %ecx
	subl	%eax, %ecx
	incl	%eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %r14
	orq	48(%r13), %r14
	cmpl	$8, %eax
	jl	.LBB10_25
# BB#13:                                # %.lr.ph.i.preheader
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	%eax, %ebx
	.p2align	4, 0x90
.LBB10_14:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rbp
	shrq	$16, %rbp
	movq	32(%r13), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 32(%r13)
	movb	%bpl, (%rax)
	decq	40(%r13)
	jne	.LBB10_18
# BB#15:                                #   in Loop: Header=BB10_14 Depth=1
	movq	64(%r13), %rdi
	movq	32(%rdi), %r15
	callq	*24(%r15)
	testl	%eax, %eax
	jne	.LBB10_17
# BB#16:                                #   in Loop: Header=BB10_14 Depth=1
	movq	64(%r13), %rdi
	movq	(%rdi), %rax
	movl	$22, 40(%rax)
	callq	*(%rax)
.LBB10_17:                              # %dump_buffer.exit.i
                                        #   in Loop: Header=BB10_14 Depth=1
	movups	(%r15), %xmm0
	movups	%xmm0, 32(%r13)
.LBB10_18:                              #   in Loop: Header=BB10_14 Depth=1
	cmpb	$-1, %bpl
	jne	.LBB10_23
# BB#19:                                #   in Loop: Header=BB10_14 Depth=1
	movq	32(%r13), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 32(%r13)
	movb	$0, (%rax)
	decq	40(%r13)
	jne	.LBB10_23
# BB#20:                                #   in Loop: Header=BB10_14 Depth=1
	movq	64(%r13), %rdi
	movq	32(%rdi), %rbp
	callq	*24(%rbp)
	testl	%eax, %eax
	jne	.LBB10_22
# BB#21:                                #   in Loop: Header=BB10_14 Depth=1
	movq	64(%r13), %rdi
	movq	(%rdi), %rax
	movl	$22, 40(%rax)
	callq	*(%rax)
.LBB10_22:                              # %dump_buffer.exit
                                        #   in Loop: Header=BB10_14 Depth=1
	movups	(%rbp), %xmm0
	movups	%xmm0, 32(%r13)
	.p2align	4, 0x90
.LBB10_23:                              #   in Loop: Header=BB10_14 Depth=1
	shlq	$8, %r14
	addl	$-8, %ebx
	cmpl	$7, %ebx
	jg	.LBB10_14
	jmp	.LBB10_24
.LBB10_28:                              # %emit_buffered_bits.exit
	movl	$0, 96(%r13)
.LBB10_29:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	emit_eobrun, .Lfunc_end10-emit_eobrun
	.cfi_endproc

	.p2align	4, 0x90
	.type	flush_bits,@function
flush_bits:                             # @flush_bits
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi110:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi111:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi113:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi114:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi116:
	.cfi_def_cfa_offset 64
.Lcfi117:
	.cfi_offset %rbx, -56
.Lcfi118:
	.cfi_offset %r12, -48
.Lcfi119:
	.cfi_offset %r13, -40
.Lcfi120:
	.cfi_offset %r14, -32
.Lcfi121:
	.cfi_offset %r15, -24
.Lcfi122:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	cmpl	$0, 24(%r15)
	jne	.LBB11_15
# BB#1:
	movl	56(%r15), %r14d
	movl	$17, %ecx
	subl	%r14d, %ecx
	addl	$7, %r14d
	movl	$127, %r12d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %r12
	orq	48(%r15), %r12
	cmpl	$8, %r14d
	jl	.LBB11_14
# BB#2:                                 # %.lr.ph.i
	movl	%r14d, %ebx
	.p2align	4, 0x90
.LBB11_3:                               # =>This Inner Loop Header: Depth=1
	movq	%r12, %rbp
	shrq	$16, %rbp
	movq	32(%r15), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 32(%r15)
	movb	%bpl, (%rax)
	decq	40(%r15)
	jne	.LBB11_7
# BB#4:                                 #   in Loop: Header=BB11_3 Depth=1
	movq	64(%r15), %rdi
	movq	32(%rdi), %r13
	callq	*24(%r13)
	testl	%eax, %eax
	jne	.LBB11_6
# BB#5:                                 #   in Loop: Header=BB11_3 Depth=1
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	movl	$22, 40(%rax)
	callq	*(%rax)
.LBB11_6:                               # %dump_buffer.exit.i
                                        #   in Loop: Header=BB11_3 Depth=1
	movups	(%r13), %xmm0
	movups	%xmm0, 32(%r15)
.LBB11_7:                               #   in Loop: Header=BB11_3 Depth=1
	cmpb	$-1, %bpl
	jne	.LBB11_12
# BB#8:                                 #   in Loop: Header=BB11_3 Depth=1
	movq	32(%r15), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 32(%r15)
	movb	$0, (%rax)
	decq	40(%r15)
	jne	.LBB11_12
# BB#9:                                 #   in Loop: Header=BB11_3 Depth=1
	movq	64(%r15), %rdi
	movq	32(%rdi), %rbp
	callq	*24(%rbp)
	testl	%eax, %eax
	jne	.LBB11_11
# BB#10:                                #   in Loop: Header=BB11_3 Depth=1
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	movl	$22, 40(%rax)
	callq	*(%rax)
.LBB11_11:                              # %dump_buffer.exit
                                        #   in Loop: Header=BB11_3 Depth=1
	movups	(%rbp), %xmm0
	movups	%xmm0, 32(%r15)
	.p2align	4, 0x90
.LBB11_12:                              #   in Loop: Header=BB11_3 Depth=1
	shlq	$8, %r12
	addl	$-8, %ebx
	cmpl	$7, %ebx
	jg	.LBB11_3
# BB#13:                                # %._crit_edge.loopexit.i
	andl	$7, %r14d
.LBB11_14:                              # %._crit_edge.i
	movq	%r12, 48(%r15)
	movl	%r14d, 56(%r15)
.LBB11_15:                              # %emit_bits.exit
	movq	$0, 48(%r15)
	movl	$0, 56(%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	flush_bits, .Lfunc_end11-flush_bits
	.cfi_endproc

	.p2align	4, 0x90
	.type	dump_buffer,@function
dump_buffer:                            # @dump_buffer
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi123:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi125:
	.cfi_def_cfa_offset 32
.Lcfi126:
	.cfi_offset %rbx, -24
.Lcfi127:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	64(%rbx), %rdi
	movq	32(%rdi), %r14
	callq	*24(%r14)
	testl	%eax, %eax
	jne	.LBB12_2
# BB#1:
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$22, 40(%rax)
	callq	*(%rax)
.LBB12_2:
	movups	(%r14), %xmm0
	movups	%xmm0, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end12:
	.size	dump_buffer, .Lfunc_end12-dump_buffer
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
