	.text
	.file	"gethdr.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.text
	.globl	Get_Hdr
	.p2align	4, 0x90
	.type	Get_Hdr,@function
Get_Hdr:                                # @Get_Hdr
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movl	$default_intra_quantizer_matrix+64, %r15d
	movl	$default_intra_quantizer_matrix, %r14d
	movl	$base, %r12d
	jmp	.LBB0_1
	.p2align	4, 0x90
.LBB0_35:                               # %group_of_pictures_header.exit
                                        #   in Loop: Header=BB0_1 Depth=1
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, drop_flag(%rip)
	movl	$5, %edi
	callq	Get_Bits
	movl	%eax, hour(%rip)
	movl	$6, %edi
	callq	Get_Bits
	movl	%eax, minute(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	$6, %edi
	callq	Get_Bits
	movl	%eax, sec(%rip)
	movl	$6, %edi
	callq	Get_Bits
	movl	%eax, frame(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, closed_gop(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, broken_link(%rip)
	callq	extension_and_user_data
.LBB0_1:                                # %.backedge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_3 Depth 2
                                        #     Child Loop BB0_22 Depth 2
                                        #     Child Loop BB0_26 Depth 2
                                        #     Child Loop BB0_30 Depth 2
	movq	ld(%rip), %rax
	movl	2096(%rax), %edi
	andl	$7, %edi
	jmp	.LBB0_3
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph.i
                                        #   in Loop: Header=BB0_3 Depth=2
	movl	$8, %edi
.LBB0_3:                                # %.lr.ph.i
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	Flush_Buffer
	movl	$24, %edi
	callq	Show_Bits
	cmpl	$1, %eax
	jne	.LBB0_2
# BB#4:                                 # %next_start_code.exit
                                        #   in Loop: Header=BB0_1 Depth=1
	callq	Get_Bits32
	movl	%eax, %ecx
	cmpl	$438, %ecx              # imm = 0x1B6
	jg	.LBB0_16
# BB#5:                                 # %next_start_code.exit
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpl	$435, %ecx              # imm = 0x1B3
	jne	.LBB0_6
# BB#20:                                #   in Loop: Header=BB0_1 Depth=1
	movl	$12, %edi
	callq	Get_Bits
	movl	%eax, horizontal_size(%rip)
	movl	$12, %edi
	callq	Get_Bits
	movl	%eax, vertical_size(%rip)
	movl	$4, %edi
	callq	Get_Bits
	movl	%eax, aspect_ratio_information(%rip)
	movl	$4, %edi
	callq	Get_Bits
	movl	%eax, frame_rate_code(%rip)
	movl	$18, %edi
	callq	Get_Bits
	movl	%eax, bit_rate_value(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	$10, %edi
	callq	Get_Bits
	movl	%eax, vbv_buffer_size(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, constrained_parameters_flag(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movq	ld(%rip), %rcx
	movl	%eax, 3128(%rcx)
	testl	%eax, %eax
	je	.LBB0_23
# BB#21:                                # %.preheader26.i.preheader
                                        #   in Loop: Header=BB0_1 Depth=1
	movq	$-64, %rbx
	.p2align	4, 0x90
.LBB0_22:                               # %.preheader26.i
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$8, %edi
	callq	Get_Bits
	movq	ld(%rip), %rcx
	movzbl	scan+64(%rbx), %edx
	movl	%eax, 2104(%rcx,%rdx,4)
	incq	%rbx
	jne	.LBB0_22
	jmp	.LBB0_28
	.p2align	4, 0x90
.LBB0_16:                               # %next_start_code.exit
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpl	$440, %ecx              # imm = 0x1B8
	jne	.LBB0_17
# BB#33:                                #   in Loop: Header=BB0_1 Depth=1
	cmpq	%r12, ld(%rip)
	jne	.LBB0_35
# BB#34:                                #   in Loop: Header=BB0_1 Depth=1
	movl	True_Framenum_max(%rip), %eax
	incl	%eax
	movl	%eax, Temporal_Reference_Base(%rip)
	movb	$1, Temporal_Reference_GOP_Reset(%rip)
	jmp	.LBB0_35
.LBB0_6:                                # %next_start_code.exit
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpl	$256, %ecx              # imm = 0x100
	jne	.LBB0_18
	jmp	.LBB0_7
.LBB0_17:                               # %next_start_code.exit
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpl	$439, %ecx              # imm = 0x1B7
	je	.LBB0_47
.LBB0_18:                               #   in Loop: Header=BB0_1 Depth=1
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB0_1
# BB#19:                                #   in Loop: Header=BB0_1 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%ecx, %edx
	callq	fprintf
	jmp	.LBB0_1
.LBB0_23:                               # %vector.memcheck
                                        #   in Loop: Header=BB0_1 Depth=1
	leaq	2104(%rcx), %rax
	cmpq	%r15, %rax
	jae	.LBB0_27
# BB#24:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_1 Depth=1
	leaq	2360(%rcx), %rax
	cmpq	%r14, %rax
	jbe	.LBB0_27
# BB#25:                                # %.preheader25.i.preheader
                                        #   in Loop: Header=BB0_1 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_26:                               # %.preheader25.i
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	default_intra_quantizer_matrix(%rax), %edx
	movl	%edx, 2104(%rcx,%rax,4)
	movzbl	default_intra_quantizer_matrix+1(%rax), %edx
	movl	%edx, 2108(%rcx,%rax,4)
	movzbl	default_intra_quantizer_matrix+2(%rax), %edx
	movl	%edx, 2112(%rcx,%rax,4)
	movzbl	default_intra_quantizer_matrix+3(%rax), %edx
	movl	%edx, 2116(%rcx,%rax,4)
	addq	$4, %rax
	cmpq	$64, %rax
	jne	.LBB0_26
	jmp	.LBB0_28
.LBB0_27:                               # %vector.body12
                                        #   in Loop: Header=BB0_1 Depth=1
	movd	default_intra_quantizer_matrix(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	pxor	%xmm1, %xmm1
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 2104(%rcx)
	movd	default_intra_quantizer_matrix+4(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 2120(%rcx)
	movd	default_intra_quantizer_matrix+8(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 2136(%rcx)
	movd	default_intra_quantizer_matrix+12(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 2152(%rcx)
	movd	default_intra_quantizer_matrix+16(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 2168(%rcx)
	movd	default_intra_quantizer_matrix+20(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 2184(%rcx)
	movd	default_intra_quantizer_matrix+24(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 2200(%rcx)
	movd	default_intra_quantizer_matrix+28(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 2216(%rcx)
	movd	default_intra_quantizer_matrix+32(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 2232(%rcx)
	movd	default_intra_quantizer_matrix+36(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 2248(%rcx)
	movd	default_intra_quantizer_matrix+40(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 2264(%rcx)
	movd	default_intra_quantizer_matrix+44(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 2280(%rcx)
	movd	default_intra_quantizer_matrix+48(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 2296(%rcx)
	movd	default_intra_quantizer_matrix+52(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 2312(%rcx)
	movd	default_intra_quantizer_matrix+56(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 2328(%rcx)
	movd	default_intra_quantizer_matrix+60(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 2344(%rcx)
	.p2align	4, 0x90
.LBB0_28:                               # %.loopexit.i
                                        #   in Loop: Header=BB0_1 Depth=1
	movl	$1, %edi
	callq	Get_Bits
	movq	ld(%rip), %rcx
	movl	%eax, 3132(%rcx)
	testl	%eax, %eax
	je	.LBB0_31
# BB#29:                                # %.preheader23.i.preheader
                                        #   in Loop: Header=BB0_1 Depth=1
	movq	$-64, %rbx
	.p2align	4, 0x90
.LBB0_30:                               # %.preheader23.i
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$8, %edi
	callq	Get_Bits
	movq	ld(%rip), %rcx
	movzbl	scan+64(%rbx), %edx
	movl	%eax, 2360(%rcx,%rdx,4)
	incq	%rbx
	jne	.LBB0_30
	jmp	.LBB0_32
.LBB0_31:                               # %.preheader22.i
                                        #   in Loop: Header=BB0_1 Depth=1
	movdqa	.LCPI0_0(%rip), %xmm0   # xmm0 = [16,16,16,16]
	movdqu	%xmm0, 2360(%rcx)
	movdqu	%xmm0, 2376(%rcx)
	movdqu	%xmm0, 2392(%rcx)
	movdqu	%xmm0, 2408(%rcx)
	movdqu	%xmm0, 2424(%rcx)
	movdqu	%xmm0, 2440(%rcx)
	movdqu	%xmm0, 2456(%rcx)
	movdqu	%xmm0, 2472(%rcx)
	movdqu	%xmm0, 2488(%rcx)
	movdqu	%xmm0, 2504(%rcx)
	movdqu	%xmm0, 2520(%rcx)
	movdqu	%xmm0, 2536(%rcx)
	movdqu	%xmm0, 2552(%rcx)
	movdqu	%xmm0, 2568(%rcx)
	movdqu	%xmm0, 2584(%rcx)
	movdqu	%xmm0, 2600(%rcx)
	movq	ld(%rip), %rcx
.LBB0_32:                               # %vector.body
                                        #   in Loop: Header=BB0_1 Depth=1
	movups	2104(%rcx), %xmm0
	movups	%xmm0, 2616(%rcx)
	movups	2360(%rcx), %xmm0
	movups	%xmm0, 2872(%rcx)
	movups	2120(%rcx), %xmm0
	movups	%xmm0, 2632(%rcx)
	movups	2376(%rcx), %xmm0
	movups	%xmm0, 2888(%rcx)
	movups	2136(%rcx), %xmm0
	movups	%xmm0, 2648(%rcx)
	movups	2392(%rcx), %xmm0
	movups	%xmm0, 2904(%rcx)
	movups	2152(%rcx), %xmm0
	movups	%xmm0, 2664(%rcx)
	movups	2408(%rcx), %xmm0
	movups	%xmm0, 2920(%rcx)
	movups	2168(%rcx), %xmm0
	movups	%xmm0, 2680(%rcx)
	movups	2424(%rcx), %xmm0
	movups	%xmm0, 2936(%rcx)
	movups	2184(%rcx), %xmm0
	movups	%xmm0, 2696(%rcx)
	movups	2440(%rcx), %xmm0
	movups	%xmm0, 2952(%rcx)
	movups	2200(%rcx), %xmm0
	movups	%xmm0, 2712(%rcx)
	movups	2456(%rcx), %xmm0
	movups	%xmm0, 2968(%rcx)
	movups	2216(%rcx), %xmm0
	movups	%xmm0, 2728(%rcx)
	movups	2472(%rcx), %xmm0
	movups	%xmm0, 2984(%rcx)
	movups	2232(%rcx), %xmm0
	movups	%xmm0, 2744(%rcx)
	movups	2488(%rcx), %xmm0
	movups	%xmm0, 3000(%rcx)
	movups	2248(%rcx), %xmm0
	movups	%xmm0, 2760(%rcx)
	movups	2504(%rcx), %xmm0
	movups	%xmm0, 3016(%rcx)
	movups	2264(%rcx), %xmm0
	movups	%xmm0, 2776(%rcx)
	movups	2520(%rcx), %xmm0
	movups	%xmm0, 3032(%rcx)
	movups	2280(%rcx), %xmm0
	movups	%xmm0, 2792(%rcx)
	movups	2536(%rcx), %xmm0
	movups	%xmm0, 3048(%rcx)
	movups	2296(%rcx), %xmm0
	movups	%xmm0, 2808(%rcx)
	movups	2552(%rcx), %xmm0
	movups	%xmm0, 3064(%rcx)
	movups	2312(%rcx), %xmm0
	movups	%xmm0, 2824(%rcx)
	movups	2568(%rcx), %xmm0
	movups	%xmm0, 3080(%rcx)
	movups	2328(%rcx), %xmm0
	movups	%xmm0, 2840(%rcx)
	movups	2584(%rcx), %xmm0
	movups	%xmm0, 3096(%rcx)
	movups	2344(%rcx), %xmm0
	movups	%xmm0, 2856(%rcx)
	movups	2600(%rcx), %xmm0
	movups	%xmm0, 3112(%rcx)
	callq	extension_and_user_data
	jmp	.LBB0_1
.LBB0_7:
	movq	ld(%rip), %rax
	movl	$0, 3160(%rax)
	movl	$10, %edi
	callq	Get_Bits
	movl	%eax, temporal_reference(%rip)
	movl	$3, %edi
	callq	Get_Bits
	movl	%eax, picture_coding_type(%rip)
	movl	$16, %edi
	callq	Get_Bits
	movl	%eax, vbv_delay(%rip)
	movl	picture_coding_type(%rip), %eax
	movl	%eax, %ecx
	orl	$1, %ecx
	cmpl	$3, %ecx
	jne	.LBB0_9
# BB#8:
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, full_pel_forward_vector(%rip)
	movl	$3, %edi
	callq	Get_Bits
	movl	%eax, forward_f_code(%rip)
	movl	picture_coding_type(%rip), %eax
.LBB0_9:
	cmpl	$3, %eax
	jne	.LBB0_12
# BB#10:
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, full_pel_backward_vector(%rip)
	movl	$3, %edi
	callq	Get_Bits
	movl	%eax, backward_f_code(%rip)
	jmp	.LBB0_12
	.p2align	4, 0x90
.LBB0_11:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB0_12 Depth=1
	movl	$8, %edi
	callq	Flush_Buffer
.LBB0_12:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	callq	Get_Bits1
	testl	%eax, %eax
	jne	.LBB0_11
# BB#13:                                # %extra_bit_information.exit.i
	callq	extension_and_user_data
	movl	$base, %ecx
	movl	$1, %eax
	cmpq	%rcx, ld(%rip)
	jne	.LBB0_48
# BB#14:
	cmpl	$3, picture_coding_type(%rip)
	movl	temporal_reference(%rip), %ecx
	movl	Update_Temporal_Reference_Tacking_Data.temporal_reference_old(%rip), %edx
	jne	.LBB0_36
# BB#15:
	movl	%edx, %esi
	jmp	.LBB0_43
.LBB0_47:                               # %picture_header.exit.loopexit
	xorl	%eax, %eax
	jmp	.LBB0_48
.LBB0_36:
	cmpl	%edx, %ecx
	movl	%ecx, %esi
	je	.LBB0_43
# BB#37:
	cmpb	$1, Update_Temporal_Reference_Tacking_Data.temporal_reference_wrap(%rip)
	jne	.LBB0_39
# BB#38:
	addl	$1024, Temporal_Reference_Base(%rip) # imm = 0x400
	movb	$0, Update_Temporal_Reference_Tacking_Data.temporal_reference_wrap(%rip)
.LBB0_39:
	cmpl	%edx, %ecx
	jge	.LBB0_42
# BB#40:
	testb	$1, Temporal_Reference_GOP_Reset(%rip)
	jne	.LBB0_42
# BB#41:
	movb	$1, Update_Temporal_Reference_Tacking_Data.temporal_reference_wrap(%rip)
.LBB0_42:
	movl	%ecx, Update_Temporal_Reference_Tacking_Data.temporal_reference_old(%rip)
	movb	$0, Temporal_Reference_GOP_Reset(%rip)
	movl	%ecx, %esi
.LBB0_43:                               # %._crit_edge.i.i
	movl	Temporal_Reference_Base(%rip), %edx
	addl	%ecx, %edx
	movl	%edx, True_Framenum(%rip)
	cmpl	%esi, %ecx
	jg	.LBB0_46
# BB#44:                                # %._crit_edge.i.i
	movb	Update_Temporal_Reference_Tacking_Data.temporal_reference_wrap(%rip), %cl
	xorb	$1, %cl
	testb	$1, %cl
	jne	.LBB0_46
# BB#45:
	addl	$1024, %edx             # imm = 0x400
	movl	%edx, True_Framenum(%rip)
.LBB0_46:
	movl	True_Framenum_max(%rip), %ecx
	cmpl	%ecx, %edx
	cmovgel	%edx, %ecx
	movl	%ecx, True_Framenum_max(%rip)
.LBB0_48:                               # %picture_header.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	Get_Hdr, .Lfunc_end0-Get_Hdr
	.cfi_endproc

	.globl	next_start_code
	.p2align	4, 0x90
	.type	next_start_code,@function
next_start_code:                        # @next_start_code
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 16
	movq	ld(%rip), %rax
	movl	2096(%rax), %edi
	andl	$7, %edi
	jmp	.LBB1_2
	.p2align	4, 0x90
.LBB1_1:                                # %.lr.ph
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	$8, %edi
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	callq	Flush_Buffer
	movl	$24, %edi
	callq	Show_Bits
	cmpl	$1, %eax
	jne	.LBB1_1
# BB#3:                                 # %._crit_edge
	popq	%rax
	retq
.Lfunc_end1:
	.size	next_start_code, .Lfunc_end1-next_start_code
	.cfi_endproc

	.globl	slice_header
	.p2align	4, 0x90
	.type	slice_header,@function
slice_header:                           # @slice_header
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 16
.Lcfi11:
	.cfi_offset %rbx, -16
	movq	ld(%rip), %rax
	xorl	%ebx, %ebx
	cmpl	$0, 3144(%rax)
	je	.LBB2_3
# BB#1:
	cmpl	$2801, vertical_size(%rip) # imm = 0xAF1
	jl	.LBB2_3
# BB#2:
	movl	$3, %edi
	callq	Get_Bits
	movl	%eax, %ebx
	movq	ld(%rip), %rax
.LBB2_3:
	cmpl	$1, 3148(%rax)
	jne	.LBB2_5
# BB#4:
	movl	$7, %edi
	callq	Get_Bits
	movq	ld(%rip), %rcx
	movl	%eax, 3164(%rcx)
.LBB2_5:
	movl	$5, %edi
	callq	Get_Bits
	movq	ld(%rip), %rcx
	cmpl	$0, 3144(%rcx)
	je	.LBB2_9
# BB#6:
	cmpl	$0, 3152(%rcx)
	je	.LBB2_8
# BB#7:
	cltq
	movzbl	Non_Linear_quantizer_scale(%rax), %eax
	jmp	.LBB2_9
.LBB2_8:
	addl	%eax, %eax
.LBB2_9:
	movl	%eax, 3168(%rcx)
	movl	$1, %edi
	callq	Get_Bits
	testl	%eax, %eax
	je	.LBB2_13
# BB#10:
	movl	$1, %edi
	callq	Get_Bits
	movq	ld(%rip), %rcx
	movl	%eax, 3172(%rcx)
	movl	$1, %edi
	callq	Get_Bits
	movl	$6, %edi
	callq	Get_Bits
	jmp	.LBB2_12
	.p2align	4, 0x90
.LBB2_11:                               # %.lr.ph.i
                                        #   in Loop: Header=BB2_12 Depth=1
	movl	$8, %edi
	callq	Flush_Buffer
.LBB2_12:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	callq	Get_Bits1
	testl	%eax, %eax
	jne	.LBB2_11
	jmp	.LBB2_14
.LBB2_13:
	movq	ld(%rip), %rax
	movl	$0, 3172(%rax)
.LBB2_14:                               # %extra_bit_information.exit
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end2:
	.size	slice_header, .Lfunc_end2-slice_header
	.cfi_endproc

	.globl	marker_bit
	.p2align	4, 0x90
	.type	marker_bit,@function
marker_bit:                             # @marker_bit
	.cfi_startproc
# BB#0:
	movl	$1, %edi
	jmp	Get_Bits                # TAILCALL
.Lfunc_end3:
	.size	marker_bit, .Lfunc_end3-marker_bit
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4645744490609377280     # double 400
	.text
	.p2align	4, 0x90
	.type	extension_and_user_data,@function
extension_and_user_data:                # @extension_and_user_data
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 48
.Lcfi17:
	.cfi_offset %rbx, -48
.Lcfi18:
	.cfi_offset %r12, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	ld(%rip), %rax
	movl	2096(%rax), %edi
	andl	$7, %edi
	jmp	.LBB4_2
	.p2align	4, 0x90
.LBB4_1:                                # %.lr.ph.i
                                        #   in Loop: Header=BB4_2 Depth=1
	movl	$8, %edi
.LBB4_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	callq	Flush_Buffer
	movl	$24, %edi
	callq	Show_Bits
	cmpl	$1, %eax
	jne	.LBB4_1
# BB#3:                                 # %next_start_code.exit.preheader
	movl	$4095, %r12d            # imm = 0xFFF
	jmp	.LBB4_56
	.p2align	4, 0x90
.LBB4_4:                                #   in Loop: Header=BB4_56 Depth=1
	callq	Flush_Buffer32
	cmpl	$437, %ebx              # imm = 0x1B5
	jne	.LBB4_9
# BB#5:                                 #   in Loop: Header=BB4_56 Depth=1
	movl	$4, %edi
	callq	Get_Bits
	movl	%eax, %ecx
	leal	-1(%rcx), %eax
	cmpl	$9, %eax
	ja	.LBB4_41
# BB#6:                                 #   in Loop: Header=BB4_56 Depth=1
	jmpq	*.LJTI4_0(,%rax,8)
.LBB4_7:                                #   in Loop: Header=BB4_56 Depth=1
	movq	ld(%rip), %rax
	movl	$1, 3144(%rax)
	movl	$0, 3148(%rax)
	movl	$0, layer_id(%rip)
	movl	$8, %edi
	callq	Get_Bits
	movl	%eax, profile_and_level_indication(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, progressive_sequence(%rip)
	movl	$2, %edi
	callq	Get_Bits
	movl	%eax, chroma_format(%rip)
	movl	$2, %edi
	callq	Get_Bits
	movl	%eax, %ebp
	movl	$2, %edi
	callq	Get_Bits
	movl	%eax, %r14d
	movl	$12, %edi
	callq	Get_Bits
	movl	%eax, %ebx
	movl	$1, %edi
	callq	Get_Bits
	movl	$8, %edi
	callq	Get_Bits
	movl	%eax, %r15d
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, low_delay(%rip)
	movl	$2, %edi
	callq	Get_Bits
	movl	%eax, frame_rate_extension_n(%rip)
	movl	$5, %edi
	callq	Get_Bits
	movl	%eax, %ecx
	movl	%ecx, frame_rate_extension_d(%rip)
	movslq	frame_rate_code(%rip), %rsi
	movl	frame_rate_extension_n(%rip), %eax
	incl	%eax
	incl	%ecx
	cltd
	idivl	%ecx
	cvtsi2sdl	%eax, %xmm0
	mulsd	frame_rate_Table(,%rsi,8), %xmm0
	movsd	%xmm0, frame_rate(%rip)
	movl	profile_and_level_indication(%rip), %eax
	testb	%al, %al
	js	.LBB4_42
# BB#8:                                 #   in Loop: Header=BB4_56 Depth=1
	movl	%eax, %ecx
	sarl	$4, %ecx
	movl	%ecx, profile(%rip)
	andl	$15, %eax
	jmp	.LBB4_44
	.p2align	4, 0x90
.LBB4_9:                                #   in Loop: Header=BB4_56 Depth=1
	movq	ld(%rip), %rax
	movl	2096(%rax), %edi
	andl	$7, %edi
	jmp	.LBB4_11
	.p2align	4, 0x90
.LBB4_10:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB4_11 Depth=2
	movl	$8, %edi
.LBB4_11:                               # %.lr.ph.i.i
                                        #   Parent Loop BB4_56 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	Flush_Buffer
	movl	$24, %edi
	callq	Show_Bits
	cmpl	$1, %eax
	jne	.LBB4_10
	jmp	.LBB4_56
.LBB4_12:                               #   in Loop: Header=BB4_56 Depth=1
	movl	$3, %edi
	callq	Get_Bits
	movl	%eax, video_format(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, color_description(%rip)
	testl	%eax, %eax
	je	.LBB4_14
# BB#13:                                #   in Loop: Header=BB4_56 Depth=1
	movl	$8, %edi
	callq	Get_Bits
	movl	%eax, color_primaries(%rip)
	movl	$8, %edi
	callq	Get_Bits
	movl	%eax, transfer_characteristics(%rip)
	movl	$8, %edi
	callq	Get_Bits
	movl	%eax, matrix_coefficients(%rip)
.LBB4_14:                               # %sequence_display_extension.exit
                                        #   in Loop: Header=BB4_56 Depth=1
	movl	$14, %edi
	callq	Get_Bits
	movl	%eax, display_horizontal_size(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	$14, %edi
	callq	Get_Bits
	movl	%eax, display_vertical_size(%rip)
	jmp	.LBB4_53
.LBB4_15:                               #   in Loop: Header=BB4_56 Depth=1
	movl	$1, %edi
	callq	Get_Bits
	movq	ld(%rip), %rcx
	movl	%eax, 3128(%rcx)
	testl	%eax, %eax
	je	.LBB4_18
# BB#16:                                # %.preheader21.i.preheader
                                        #   in Loop: Header=BB4_56 Depth=1
	movq	$-64, %rbx
	.p2align	4, 0x90
.LBB4_17:                               # %.preheader21.i
                                        #   Parent Loop BB4_56 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$8, %edi
	callq	Get_Bits
	movq	ld(%rip), %rcx
	movzbl	scan+64(%rbx), %edx
	movl	%eax, 2104(%rcx,%rdx,4)
	movzbl	scan+64(%rbx), %edx
	movl	%eax, 2616(%rcx,%rdx,4)
	incq	%rbx
	jne	.LBB4_17
.LBB4_18:                               # %.loopexit22.i
                                        #   in Loop: Header=BB4_56 Depth=1
	movl	$1, %edi
	callq	Get_Bits
	movq	ld(%rip), %rcx
	movl	%eax, 3132(%rcx)
	testl	%eax, %eax
	je	.LBB4_21
# BB#19:                                # %.preheader19.i.preheader
                                        #   in Loop: Header=BB4_56 Depth=1
	movq	$-64, %rbx
	.p2align	4, 0x90
.LBB4_20:                               # %.preheader19.i
                                        #   Parent Loop BB4_56 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$8, %edi
	callq	Get_Bits
	movq	ld(%rip), %rcx
	movzbl	scan+64(%rbx), %edx
	movl	%eax, 2360(%rcx,%rdx,4)
	movzbl	scan+64(%rbx), %edx
	movl	%eax, 2872(%rcx,%rdx,4)
	incq	%rbx
	jne	.LBB4_20
.LBB4_21:                               # %.loopexit20.i
                                        #   in Loop: Header=BB4_56 Depth=1
	movl	$1, %edi
	callq	Get_Bits
	movq	ld(%rip), %rcx
	movl	%eax, 3136(%rcx)
	testl	%eax, %eax
	je	.LBB4_24
# BB#22:                                # %.preheader17.i.preheader
                                        #   in Loop: Header=BB4_56 Depth=1
	movq	$-64, %rbx
	.p2align	4, 0x90
.LBB4_23:                               # %.preheader17.i
                                        #   Parent Loop BB4_56 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$8, %edi
	callq	Get_Bits
	movq	ld(%rip), %rcx
	movzbl	scan+64(%rbx), %edx
	movl	%eax, 2616(%rcx,%rdx,4)
	incq	%rbx
	jne	.LBB4_23
.LBB4_24:                               # %.loopexit18.i
                                        #   in Loop: Header=BB4_56 Depth=1
	movl	$1, %edi
	callq	Get_Bits
	movq	ld(%rip), %rcx
	movl	%eax, 3140(%rcx)
	testl	%eax, %eax
	je	.LBB4_53
# BB#25:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB4_56 Depth=1
	movq	$-64, %rbx
	.p2align	4, 0x90
.LBB4_26:                               # %.preheader.i
                                        #   Parent Loop BB4_56 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$8, %edi
	callq	Get_Bits
	movq	ld(%rip), %rcx
	movzbl	scan+64(%rbx), %edx
	movl	%eax, 2872(%rcx,%rdx,4)
	incq	%rbx
	jne	.LBB4_26
	jmp	.LBB4_53
.LBB4_27:                               #   in Loop: Header=BB4_56 Depth=1
	movq	ld(%rip), %rax
	movl	2100(%rax), %ebx
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, copyright_flag(%rip)
	movl	$8, %edi
	callq	Get_Bits
	movl	%eax, copyright_identifier(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, original_or_copy(%rip)
	movl	$7, %edi
	callq	Get_Bits
	movl	$1, %edi
	callq	Get_Bits
	movl	$20, %edi
	callq	Get_Bits
	movl	%eax, copyright_number_1(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	$22, %edi
	callq	Get_Bits
	movl	%eax, copyright_number_2(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	$22, %edi
	callq	Get_Bits
	movl	%eax, copyright_number_3(%rip)
	cmpl	$0, Verbose_Flag(%rip)
	jle	.LBB4_53
# BB#28:                                #   in Loop: Header=BB4_56 Depth=1
	sarl	$3, %ebx
	addl	$-4, %ebx
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	cmpl	$2, Verbose_Flag(%rip)
	jl	.LBB4_53
# BB#29:                                #   in Loop: Header=BB4_56 Depth=1
	movl	copyright_flag(%rip), %esi
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	printf
	movl	copyright_identifier(%rip), %esi
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	printf
	movl	original_or_copy(%rip), %esi
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	printf
	movl	copyright_number_1(%rip), %esi
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	printf
	movl	copyright_number_2(%rip), %esi
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	printf
	movl	copyright_number_3(%rip), %esi
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB4_53
.LBB4_30:                               #   in Loop: Header=BB4_56 Depth=1
	movl	$2, %edi
	callq	Get_Bits
	incl	%eax
	movq	ld(%rip), %rcx
	movl	%eax, 3148(%rcx)
	movl	$4, %edi
	callq	Get_Bits
	movl	%eax, layer_id(%rip)
	movq	ld(%rip), %rax
	movl	3148(%rax), %eax
	cmpl	$2, %eax
	jne	.LBB4_32
# BB#31:                                #   in Loop: Header=BB4_56 Depth=1
	movl	$14, %edi
	callq	Get_Bits
	movl	%eax, lower_layer_prediction_horizontal_size(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	$14, %edi
	callq	Get_Bits
	movl	%eax, lower_layer_prediction_vertical_size(%rip)
	movl	$5, %edi
	callq	Get_Bits
	movl	%eax, horizontal_subsampling_factor_m(%rip)
	movl	$5, %edi
	callq	Get_Bits
	movl	%eax, horizontal_subsampling_factor_n(%rip)
	movl	$5, %edi
	callq	Get_Bits
	movl	%eax, vertical_subsampling_factor_m(%rip)
	movl	$5, %edi
	callq	Get_Bits
	movl	%eax, vertical_subsampling_factor_n(%rip)
	movq	ld(%rip), %rax
	movl	3148(%rax), %eax
.LBB4_32:                               #   in Loop: Header=BB4_56 Depth=1
	cmpl	$4, %eax
	jne	.LBB4_53
# BB#33:                                #   in Loop: Header=BB4_56 Depth=1
	movl	$.L.str.6, %edi
	callq	Error
	jmp	.LBB4_53
.LBB4_34:                               #   in Loop: Header=BB4_56 Depth=1
	cmpl	$0, progressive_sequence(%rip)
	je	.LBB4_46
# BB#35:                                #   in Loop: Header=BB4_56 Depth=1
	cmpl	$0, repeat_first_field(%rip)
	je	.LBB4_50
# BB#36:                                #   in Loop: Header=BB4_56 Depth=1
	xorl	%ebx, %ebx
	cmpl	$0, top_field_first(%rip)
	jmp	.LBB4_48
.LBB4_37:                               #   in Loop: Header=BB4_56 Depth=1
	movl	$4, %edi
	callq	Get_Bits
	movl	%eax, f_code(%rip)
	movl	$4, %edi
	callq	Get_Bits
	movl	%eax, f_code+4(%rip)
	movl	$4, %edi
	callq	Get_Bits
	movl	%eax, f_code+8(%rip)
	movl	$4, %edi
	callq	Get_Bits
	movl	%eax, f_code+12(%rip)
	movl	$2, %edi
	callq	Get_Bits
	movl	%eax, intra_dc_precision(%rip)
	movl	$2, %edi
	callq	Get_Bits
	movl	%eax, picture_structure(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, top_field_first(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, frame_pred_frame_dct(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, concealment_motion_vectors(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movq	ld(%rip), %rcx
	movl	%eax, 3152(%rcx)
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, intra_vlc_format(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movq	ld(%rip), %rcx
	movl	%eax, 3156(%rcx)
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, repeat_first_field(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, chroma_420_type(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, progressive_frame(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, composite_display_flag(%rip)
	testl	%eax, %eax
	je	.LBB4_53
# BB#38:                                #   in Loop: Header=BB4_56 Depth=1
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, v_axis(%rip)
	movl	$3, %edi
	callq	Get_Bits
	movl	%eax, field_sequence(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, sub_carrier(%rip)
	movl	$7, %edi
	callq	Get_Bits
	movl	%eax, burst_amplitude(%rip)
	movl	$8, %edi
	callq	Get_Bits
	movl	%eax, sub_carrier_phase(%rip)
	jmp	.LBB4_53
.LBB4_39:                               #   in Loop: Header=BB4_56 Depth=1
	movq	ld(%rip), %rax
	movl	$1, 3160(%rax)
	movl	$10, %edi
	callq	Get_Bits
	movl	%eax, lower_layer_temporal_reference(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	$15, %edi
	callq	Get_Bits
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-32768(%rax), %ecx
	cmpl	$16383, %eax            # imm = 0x3FFF
	cmovlel	%eax, %ecx
	movl	%ecx, lower_layer_horizontal_offset(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	$15, %edi
	callq	Get_Bits
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-32768(%rax), %ecx
	cmpl	$16383, %eax            # imm = 0x3FFF
	cmovlel	%eax, %ecx
	movl	%ecx, lower_layer_vertical_offset(%rip)
	movl	$2, %edi
	callq	Get_Bits
	movl	%eax, spatial_temporal_weight_code_table_index(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, lower_layer_progressive_frame(%rip)
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, lower_layer_deinterlaced_field_select(%rip)
	jmp	.LBB4_53
.LBB4_40:                               #   in Loop: Header=BB4_56 Depth=1
	movl	$.L.str.11, %edi
	callq	Error
	jmp	.LBB4_53
.LBB4_41:                               #   in Loop: Header=BB4_56 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movl	%ecx, %edx
	callq	fprintf
	jmp	.LBB4_53
.LBB4_42:                               #   in Loop: Header=BB4_56 Depth=1
	andl	$15, %eax
	cmpl	$5, %eax
	jne	.LBB4_45
# BB#43:                                #   in Loop: Header=BB4_56 Depth=1
	movl	$133, profile(%rip)
	movl	$8, %eax
.LBB4_44:                               # %.sink.split.i
                                        #   in Loop: Header=BB4_56 Depth=1
	movl	%eax, level(%rip)
.LBB4_45:                               # %sequence_extension.exit
                                        #   in Loop: Header=BB4_56 Depth=1
	shll	$12, %ebp
	movl	horizontal_size(%rip), %eax
	andl	%r12d, %eax
	orl	%ebp, %eax
	movl	%eax, horizontal_size(%rip)
	shll	$12, %r14d
	movl	vertical_size(%rip), %eax
	andl	%r12d, %eax
	orl	%r14d, %eax
	movl	%eax, vertical_size(%rip)
	shll	$18, %ebx
	addl	bit_rate_value(%rip), %ebx
	movl	%ebx, bit_rate_value(%rip)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	mulsd	.LCPI4_0(%rip), %xmm0
	movsd	%xmm0, bit_rate(%rip)
	shll	$10, %r15d
	addl	%r15d, vbv_buffer_size(%rip)
	jmp	.LBB4_53
.LBB4_46:                               #   in Loop: Header=BB4_56 Depth=1
	cmpl	$3, picture_structure(%rip)
	jne	.LBB4_50
# BB#47:                                #   in Loop: Header=BB4_56 Depth=1
	xorl	%ebx, %ebx
	cmpl	$0, repeat_first_field(%rip)
.LBB4_48:                               #   in Loop: Header=BB4_56 Depth=1
	setne	%bl
	orq	$2, %rbx
	jmp	.LBB4_51
.LBB4_50:                               #   in Loop: Header=BB4_56 Depth=1
	movl	$1, %ebx
.LBB4_51:                               #   in Loop: Header=BB4_56 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_52:                               #   Parent Loop BB4_56 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$16, %edi
	callq	Get_Bits
	movl	%eax, frame_center_horizontal_offset(,%rbp,4)
	movl	$1, %edi
	callq	Get_Bits
	movl	$16, %edi
	callq	Get_Bits
	movl	%eax, frame_center_vertical_offset(,%rbp,4)
	movl	$1, %edi
	callq	Get_Bits
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB4_52
	.p2align	4, 0x90
.LBB4_53:                               # %quant_matrix_extension.exit
                                        #   in Loop: Header=BB4_56 Depth=1
	movq	ld(%rip), %rax
	movl	2096(%rax), %edi
	andl	$7, %edi
	jmp	.LBB4_55
	.p2align	4, 0x90
.LBB4_54:                               # %.lr.ph.i10
                                        #   in Loop: Header=BB4_55 Depth=2
	movl	$8, %edi
.LBB4_55:                               # %.lr.ph.i10
                                        #   Parent Loop BB4_56 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	Flush_Buffer
	movl	$24, %edi
	callq	Show_Bits
	cmpl	$1, %eax
	jne	.LBB4_54
.LBB4_56:                               # %next_start_code.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_11 Depth 2
                                        #     Child Loop BB4_52 Depth 2
                                        #     Child Loop BB4_17 Depth 2
                                        #     Child Loop BB4_20 Depth 2
                                        #     Child Loop BB4_23 Depth 2
                                        #     Child Loop BB4_26 Depth 2
                                        #     Child Loop BB4_55 Depth 2
	movl	$32, %edi
	callq	Show_Bits
	movl	%eax, %ebx
	cmpl	$437, %ebx              # imm = 0x1B5
	je	.LBB4_4
# BB#57:                                # %next_start_code.exit
                                        #   in Loop: Header=BB4_56 Depth=1
	cmpl	$434, %ebx              # imm = 0x1B2
	je	.LBB4_4
# BB#58:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	extension_and_user_data, .Lfunc_end4-extension_and_user_data
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_7
	.quad	.LBB4_12
	.quad	.LBB4_15
	.quad	.LBB4_27
	.quad	.LBB4_30
	.quad	.LBB4_41
	.quad	.LBB4_34
	.quad	.LBB4_37
	.quad	.LBB4_39
	.quad	.LBB4_40

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Unexpected next_start_code %08x (ignored)\n"
	.size	.L.str, 43

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"reserved extension start code ID %d\n"
	.size	.L.str.2, 37

	.type	frame_rate_Table,@object # @frame_rate_Table
	.section	.rodata,"a",@progbits
	.p2align	4
frame_rate_Table:
	.quad	0                       # double 0
	.quad	4627160674689466646     # double 22.977022977022976
	.quad	4627448617123184640     # double 24
	.quad	4627730092099895296     # double 25
	.quad	4629129031169960744     # double 29.970029970029969
	.quad	4629137466983448576     # double 30
	.quad	4632233691727265792     # double 50
	.quad	4633632630797331240     # double 59.940059940059939
	.quad	4633641066610819072     # double 60
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.size	frame_rate_Table, 128

	.type	.L.str.6,@object        # @.str.6
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.6:
	.asciz	"temporal scalability not implemented\n"
	.size	.L.str.6, 38

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"temporal scalability not supported\n"
	.size	.L.str.11, 36

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"copyright_extension (byte %d)\n"
	.size	.L.str.15, 31

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"  copyright_flag =%d\n"
	.size	.L.str.16, 22

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"  copyright_identifier=%d\n"
	.size	.L.str.17, 27

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"  original_or_copy = %d (original=1, copy=0)\n"
	.size	.L.str.18, 46

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"  copyright_number_1=%d\n"
	.size	.L.str.19, 25

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"  copyright_number_2=%d\n"
	.size	.L.str.20, 25

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"  copyright_number_3=%d\n"
	.size	.L.str.21, 25

	.type	True_Framenum_max,@object # @True_Framenum_max
	.data
	.p2align	2
True_Framenum_max:
	.long	4294967295              # 0xffffffff
	.size	True_Framenum_max, 4

	.type	Temporal_Reference_Base,@object # @Temporal_Reference_Base
	.local	Temporal_Reference_Base
	.comm	Temporal_Reference_Base,4,4
	.type	Temporal_Reference_GOP_Reset,@object # @Temporal_Reference_GOP_Reset
	.local	Temporal_Reference_GOP_Reset
	.comm	Temporal_Reference_GOP_Reset,1,4
	.type	Update_Temporal_Reference_Tacking_Data.temporal_reference_wrap,@object # @Update_Temporal_Reference_Tacking_Data.temporal_reference_wrap
	.local	Update_Temporal_Reference_Tacking_Data.temporal_reference_wrap
	.comm	Update_Temporal_Reference_Tacking_Data.temporal_reference_wrap,1,4
	.type	Update_Temporal_Reference_Tacking_Data.temporal_reference_old,@object # @Update_Temporal_Reference_Tacking_Data.temporal_reference_old
	.local	Update_Temporal_Reference_Tacking_Data.temporal_reference_old
	.comm	Update_Temporal_Reference_Tacking_Data.temporal_reference_old,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
