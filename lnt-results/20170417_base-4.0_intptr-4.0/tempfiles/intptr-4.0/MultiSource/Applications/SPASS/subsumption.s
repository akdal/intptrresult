	.text
	.file	"subsumption.bc"
	.globl	subs_Init
	.p2align	4, 0x90
	.type	subs_Init,@function
subs_Init:                              # @subs_Init
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$0, stamp(%rip)
	movl	$multvec_j, %edi
	xorl	%esi, %esi
	movl	$400, %edx              # imm = 0x190
	callq	memset
	movl	$multvec_i, %edi
	xorl	%esi, %esi
	movl	$400, %edx              # imm = 0x190
	callq	memset
	popq	%rax
	retq
.Lfunc_end0:
	.size	subs_Init, .Lfunc_end0-subs_Init
	.cfi_endproc

	.globl	subs_STMulti
	.p2align	4, 0x90
	.type	subs_STMulti,@function
subs_STMulti:                           # @subs_STMulti
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 96
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movslq	68(%rdi), %rax
	movslq	72(%rdi), %rcx
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movslq	64(%rdi), %rdx
	addq	%rax, %rdx
	addq	%rcx, %rdx
	movl	68(%rsi), %ebp
	addl	64(%rsi), %ebp
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	addl	72(%rsi), %ebp
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	cmpl	%ebp, %edx
	jg	.LBB1_41
# BB#1:
	cmpl	$2, 24(%rsp)            # 4-byte Folded Reload
	jl	.LBB1_42
# BB#2:                                 # %.lr.ph.i
	movl	cont_BINDINGS(%rip), %eax
	movl	cont_STACKPOINTER(%rip), %ecx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_5 Depth 2
                                        #       Child Loop BB1_18 Depth 3
                                        #       Child Loop BB1_34 Depth 3
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	56(%rdx), %rdx
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	(%rdx,%rsi,8), %rdx
	movq	24(%rdx), %r12
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.LBB1_5
.LBB1_16:                               #   in Loop: Header=BB1_5 Depth=2
	movl	cont_BINDINGS(%rip), %edx
	testl	%edx, %edx
	jle	.LBB1_20
# BB#17:                                # %.lr.ph.i92.i.preheader
                                        #   in Loop: Header=BB1_5 Depth=2
	incl	%edx
	.p2align	4, 0x90
.LBB1_18:                               # %.lr.ph.i92.i
                                        #   Parent Loop BB1_4 Depth=1
                                        #     Parent Loop BB1_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rsi
	movq	%rsi, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rdx), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%edx
	cmpl	$1, %edx
	jg	.LBB1_18
# BB#19:                                # %cont_BackTrackAndStart.exit.loopexit.i
                                        #   in Loop: Header=BB1_5 Depth=2
	movl	(%r12), %ecx
.LBB1_20:                               # %cont_BackTrackAndStart.exit.i
                                        #   in Loop: Header=BB1_5 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	cmpl	%eax, %ecx
	movq	%r12, %rcx
	jne	.LBB1_22
# BB#21:                                #   in Loop: Header=BB1_5 Depth=2
	movq	16(%r12), %rcx
	movq	8(%rcx), %rcx
.LBB1_22:                               # %fol_Atom.exit88.i
                                        #   in Loop: Header=BB1_5 Depth=2
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rsi
	cmpl	%eax, (%rbx)
	movq	%rbx, %rax
	jne	.LBB1_24
# BB#23:                                #   in Loop: Header=BB1_5 Depth=2
	movq	16(%rbx), %rax
	movq	8(%rax), %rax
.LBB1_24:                               # %fol_Atom.exit80.i
                                        #   in Loop: Header=BB1_5 Depth=2
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	callq	unify_Match
	testl	%eax, %eax
	je	.LBB1_30
# BB#25:                                #   in Loop: Header=BB1_5 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	%eax, (%r12)
	movq	%r12, %rcx
	jne	.LBB1_27
# BB#26:                                #   in Loop: Header=BB1_5 Depth=2
	movq	16(%r12), %rcx
	movq	8(%rcx), %rcx
.LBB1_27:                               # %fol_Atom.exit72.i
                                        #   in Loop: Header=BB1_5 Depth=2
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rsi
	cmpl	%eax, (%rbx)
	jne	.LBB1_29
# BB#28:                                #   in Loop: Header=BB1_5 Depth=2
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
.LBB1_29:                               # %fol_Atom.exit.i
                                        #   in Loop: Header=BB1_5 Depth=2
	movq	16(%rbx), %rax
	movq	8(%rax), %rdx
	callq	unify_Match
	testl	%eax, %eax
	jne	.LBB1_6
.LBB1_30:                               #   in Loop: Header=BB1_5 Depth=2
	incl	%r14d
	xorps	%xmm0, %xmm0
	jmp	.LBB1_32
	.p2align	4, 0x90
.LBB1_5:                                #   Parent Loop BB1_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_18 Depth 3
                                        #       Child Loop BB1_34 Depth 3
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	56(%rdx), %rdx
	movslq	%r14d, %r15
	movq	(%rdx,%r15,8), %rdx
	movq	24(%rdx), %rbx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movslq	%ecx, %rcx
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	unify_Match
	testl	%eax, %eax
	je	.LBB1_7
.LBB1_6:                                #   in Loop: Header=BB1_5 Depth=2
	movl	$1, %r13d
	xorps	%xmm0, %xmm0
	jmp	.LBB1_32
	.p2align	4, 0x90
.LBB1_7:                                #   in Loop: Header=BB1_5 Depth=2
	movl	(%r12), %ecx
	cmpl	(%rbx), %ecx
	xorps	%xmm0, %xmm0
	jne	.LBB1_31
# BB#8:                                 #   in Loop: Header=BB1_5 Depth=2
	movl	fol_NOT(%rip), %eax
	cmpl	%eax, %ecx
	movl	%ecx, %edx
	jne	.LBB1_10
# BB#9:                                 #   in Loop: Header=BB1_5 Depth=2
	movq	16(%r12), %rdx
	movq	8(%rdx), %rdx
	movl	(%rdx), %edx
.LBB1_10:                               # %fol_Atom.exit109.i
                                        #   in Loop: Header=BB1_5 Depth=2
	cmpl	%edx, fol_EQUALITY(%rip)
	jne	.LBB1_31
# BB#11:                                #   in Loop: Header=BB1_5 Depth=2
	cmpl	%eax, %ecx
	movl	%ecx, %esi
	jne	.LBB1_13
# BB#12:                                #   in Loop: Header=BB1_5 Depth=2
	movq	16(%rbx), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %esi
.LBB1_13:                               # %fol_Atom.exit101.i
                                        #   in Loop: Header=BB1_5 Depth=2
	cmpl	%esi, %edx
	jne	.LBB1_31
# BB#14:                                #   in Loop: Header=BB1_5 Depth=2
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	56(%rdx), %rdx
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	(%rdx,%rsi,8), %rdx
	cmpl	$0, 8(%rdx)
	je	.LBB1_16
# BB#15:                                #   in Loop: Header=BB1_5 Depth=2
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	56(%rdx), %rdx
	movq	(%rdx,%r15,8), %rdx
	cmpl	$0, 8(%rdx)
	je	.LBB1_16
	.p2align	4, 0x90
.LBB1_31:                               #   in Loop: Header=BB1_5 Depth=2
	incl	%r14d
.LBB1_32:                               #   in Loop: Header=BB1_5 Depth=2
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB1_33
	.p2align	4, 0x90
.LBB1_34:                               # %.lr.ph.i.i
                                        #   Parent Loop BB1_4 Depth=1
                                        #     Parent Loop BB1_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB1_34
	jmp	.LBB1_35
	.p2align	4, 0x90
.LBB1_33:                               #   in Loop: Header=BB1_5 Depth=2
	movl	%ecx, %eax
.LBB1_35:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB1_5 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB1_36
# BB#37:                                #   in Loop: Header=BB1_5 Depth=2
	leaq	-1(%rdx), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdx,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB1_38
	.p2align	4, 0x90
.LBB1_36:                               #   in Loop: Header=BB1_5 Depth=2
	xorl	%ecx, %ecx
.LBB1_38:                               # %cont_BackTrack.exit.i
                                        #   in Loop: Header=BB1_5 Depth=2
	testl	%r13d, %r13d
	sete	%dl
	cmpl	%ebp, %r14d
	jge	.LBB1_40
# BB#39:                                # %cont_BackTrack.exit.i
                                        #   in Loop: Header=BB1_5 Depth=2
	testb	%dl, %dl
	jne	.LBB1_5
.LBB1_40:                               # %.critedge.i
                                        #   in Loop: Header=BB1_4 Depth=1
	testl	%r13d, %r13d
	je	.LBB1_41
# BB#3:                                 #   in Loop: Header=BB1_4 Depth=1
	movq	32(%rsp), %rsi          # 8-byte Reload
	incq	%rsi
	cmpq	24(%rsp), %rsi          # 8-byte Folded Reload
	jl	.LBB1_4
.LBB1_42:                               # %subs_TestlitsEq.exit
	movl	stamp(%rip), %eax
	incl	%eax
	movl	%eax, stamp(%rip)
	cmpl	$-1, %eax
	jne	.LBB1_44
# BB#43:                                # %.loopexit.loopexit
	movl	$1, stamp(%rip)
	movl	$multvec_j, %edi
	xorl	%esi, %esi
	movl	$400, %edx              # imm = 0x190
	callq	memset
	movl	$multvec_i, %edi
	xorl	%esi, %esi
	movl	$400, %edx              # imm = 0x190
	callq	memset
.LBB1_44:                               # %.loopexit
	xorl	%edi, %edi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	subs_STMultiIntern      # TAILCALL
.LBB1_41:                               # %subs_TestlitsEq.exit.thread
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	subs_STMulti, .Lfunc_end1-subs_STMulti
	.cfi_endproc

	.p2align	4, 0x90
	.type	subs_STMultiIntern,@function
subs_STMultiIntern:                     # @subs_STMultiIntern
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 80
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movslq	68(%r14), %rax
	movslq	72(%r14), %rcx
	movslq	64(%r14), %r12
	addq	%rax, %r12
	addq	%rcx, %r12
	testl	%r12d, %r12d
	jle	.LBB2_55
# BB#1:                                 # %.lr.ph
	movq	56(%rbx), %rax
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movslq	%edi, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %r13
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leal	1(%rcx), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB2_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_17 Depth 2
                                        #     Child Loop BB2_51 Depth 2
	movl	multvec_j(,%r15,4), %eax
	cmpl	stamp(%rip), %eax
	je	.LBB2_54
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%r15,8), %rax
	movq	24(%rax), %rbp
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	unify_Match
	testl	%eax, %eax
	je	.LBB2_15
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	movl	64(%rbx), %eax
	movl	72(%rbx), %ecx
	addl	68(%rbx), %eax
	leal	-1(%rcx,%rax), %eax
	cmpl	12(%rsp), %eax          # 4-byte Folded Reload
	je	.LBB2_5
# BB#10:                                #   in Loop: Header=BB2_2 Depth=1
	movl	stamp(%rip), %eax
	movl	%eax, multvec_j(,%r15,4)
	movl	8(%rsp), %edi           # 4-byte Reload
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	subs_STMultiIntern
	testl	%eax, %eax
	jne	.LBB2_11
# BB#14:                                #   in Loop: Header=BB2_2 Depth=1
	movl	$0, multvec_j(,%r15,4)
.LBB2_15:                               #   in Loop: Header=BB2_2 Depth=1
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	xorps	%xmm0, %xmm0
	jle	.LBB2_16
	.p2align	4, 0x90
.LBB2_17:                               # %.lr.ph.i118
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB2_17
	jmp	.LBB2_18
.LBB2_16:                               #   in Loop: Header=BB2_2 Depth=1
	movl	%ecx, %eax
.LBB2_18:                               # %._crit_edge.i119
                                        #   in Loop: Header=BB2_2 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB2_19
# BB#20:                                #   in Loop: Header=BB2_2 Depth=1
	leaq	-1(%rdx), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdx,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB2_21
.LBB2_19:                               #   in Loop: Header=BB2_2 Depth=1
	xorl	%ecx, %ecx
.LBB2_21:                               # %cont_BackTrack.exit120
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	(%r13), %esi
	cmpl	(%rbp), %esi
	jne	.LBB2_54
# BB#22:                                #   in Loop: Header=BB2_2 Depth=1
	movl	fol_NOT(%rip), %edx
	cmpl	%edx, %esi
	movl	%esi, %edi
	jne	.LBB2_24
# BB#23:                                #   in Loop: Header=BB2_2 Depth=1
	movq	16(%r13), %rdi
	movq	8(%rdi), %rdi
	movl	(%rdi), %edi
.LBB2_24:                               # %fol_Atom.exit114
                                        #   in Loop: Header=BB2_2 Depth=1
	cmpl	%edi, fol_EQUALITY(%rip)
	jne	.LBB2_54
# BB#25:                                #   in Loop: Header=BB2_2 Depth=1
	cmpl	%edx, %esi
	jne	.LBB2_27
# BB#26:                                #   in Loop: Header=BB2_2 Depth=1
	movq	16(%rbp), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %esi
.LBB2_27:                               # %fol_Atom.exit106
                                        #   in Loop: Header=BB2_2 Depth=1
	cmpl	%esi, %edi
	jne	.LBB2_54
# BB#28:                                #   in Loop: Header=BB2_2 Depth=1
	movq	56(%rbx), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rsi,%rdi,8), %rsi
	cmpl	$0, 8(%rsi)
	je	.LBB2_30
# BB#29:                                #   in Loop: Header=BB2_2 Depth=1
	movq	56(%r14), %rsi
	movq	(%rsi,%r15,8), %rsi
	cmpl	$0, 8(%rsi)
	jne	.LBB2_54
.LBB2_30:                               #   in Loop: Header=BB2_2 Depth=1
	leal	1(%rcx), %esi
	movl	%esi, cont_STACKPOINTER(%rip)
	movslq	%ecx, %rcx
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	cmpl	%edx, (%r13)
	movq	%r13, %rax
	jne	.LBB2_32
# BB#31:                                #   in Loop: Header=BB2_2 Depth=1
	movq	16(%r13), %rax
	movq	8(%rax), %rax
.LBB2_32:                               # %fol_Atom.exit98
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	16(%rax), %rax
	movq	8(%rax), %rsi
	cmpl	%edx, (%rbp)
	movq	%rbp, %rax
	jne	.LBB2_34
# BB#33:                                #   in Loop: Header=BB2_2 Depth=1
	movq	16(%rbp), %rax
	movq	8(%rax), %rax
.LBB2_34:                               # %fol_Atom.exit90
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	callq	unify_Match
	testl	%eax, %eax
	je	.LBB2_49
# BB#35:                                #   in Loop: Header=BB2_2 Depth=1
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	%eax, (%r13)
	movq	%r13, %rcx
	jne	.LBB2_37
# BB#36:                                #   in Loop: Header=BB2_2 Depth=1
	movq	16(%r13), %rcx
	movq	8(%rcx), %rcx
.LBB2_37:                               # %fol_Atom.exit82
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rsi
	cmpl	%eax, (%rbp)
	jne	.LBB2_39
# BB#38:                                #   in Loop: Header=BB2_2 Depth=1
	movq	16(%rbp), %rax
	movq	8(%rax), %rbp
.LBB2_39:                               # %fol_Atom.exit
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	16(%rbp), %rax
	movq	8(%rax), %rdx
	callq	unify_Match
	testl	%eax, %eax
	je	.LBB2_49
# BB#40:                                #   in Loop: Header=BB2_2 Depth=1
	movl	64(%rbx), %eax
	movl	72(%rbx), %ecx
	addl	68(%rbx), %eax
	leal	-1(%rcx,%rax), %eax
	cmpl	12(%rsp), %eax          # 4-byte Folded Reload
	je	.LBB2_41
# BB#44:                                #   in Loop: Header=BB2_2 Depth=1
	movl	stamp(%rip), %eax
	movl	%eax, multvec_j(,%r15,4)
	movl	8(%rsp), %edi           # 4-byte Reload
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	subs_STMultiIntern
	testl	%eax, %eax
	jne	.LBB2_45
# BB#48:                                #   in Loop: Header=BB2_2 Depth=1
	movl	$0, multvec_j(,%r15,4)
.LBB2_49:                               #   in Loop: Header=BB2_2 Depth=1
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jle	.LBB2_52
# BB#50:                                # %.lr.ph.i130.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	incl	%eax
	.p2align	4, 0x90
.LBB2_51:                               # %.lr.ph.i130
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB2_51
.LBB2_52:                               # %._crit_edge.i131
                                        #   in Loop: Header=BB2_2 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB2_54
# BB#53:                                #   in Loop: Header=BB2_2 Depth=1
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	.p2align	4, 0x90
.LBB2_54:                               # %cont_BackTrack.exit132
                                        #   in Loop: Header=BB2_2 Depth=1
	incq	%r15
	cmpq	%r12, %r15
	jl	.LBB2_2
.LBB2_55:
	xorl	%eax, %eax
.LBB2_56:                               # %cont_BackTrack.exit138
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_5:
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB2_8
# BB#6:                                 # %.lr.ph.i136.preheader
	incl	%eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph.i136
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB2_7
	jmp	.LBB2_8
.LBB2_11:
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB2_8
# BB#12:                                # %.lr.ph.i124.preheader
	incl	%eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB2_13:                               # %.lr.ph.i124
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB2_13
.LBB2_8:                                # %._crit_edge.i137
	movslq	cont_STACKPOINTER(%rip), %rcx
	testq	%rcx, %rcx
	movl	$1, %eax
	je	.LBB2_56
# BB#9:
	leaq	-1(%rcx), %rdx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rcx,4), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	jmp	.LBB2_56
.LBB2_41:
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB2_8
# BB#42:                                # %.lr.ph.i72.preheader
	incl	%eax
	xorps	%xmm0, %xmm0
.LBB2_43:                               # %.lr.ph.i72
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB2_43
	jmp	.LBB2_8
.LBB2_45:
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB2_8
# BB#46:                                # %.lr.ph.i.preheader
	incl	%eax
	xorps	%xmm0, %xmm0
.LBB2_47:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB2_47
	jmp	.LBB2_8
.Lfunc_end2:
	.size	subs_STMultiIntern, .Lfunc_end2-subs_STMultiIntern
	.cfi_endproc

	.globl	subs_STMultiExcept
	.p2align	4, 0x90
	.type	subs_STMultiExcept,@function
subs_STMultiExcept:                     # @subs_STMultiExcept
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 96
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movslq	68(%rdi), %rax
	movslq	72(%rdi), %rbp
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movslq	64(%rdi), %rbx
	addq	%rax, %rbx
	addq	%rbp, %rbx
	movl	68(%rsi), %eax
	addl	64(%rsi), %eax
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	addl	72(%rsi), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	cmpl	%eax, %ebx
	jg	.LBB3_55
# BB#1:
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	4(%rsi), %eax
	movq	56(%rsi), %rbp
	movslq	%edx, %r14
	movq	(%rbp,%r14,8), %rdx
	subl	4(%rdx), %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	4(%rsi), %edx
	movq	56(%rsi), %rsi
	movslq	%ecx, %r15
	movq	(%rsi,%r15,8), %rcx
	subl	4(%rcx), %edx
	cmpl	%edx, %eax
	jbe	.LBB3_2
.LBB3_55:                               # %subs_TestlitsEqExcept.exit
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_2:
	movl	stamp(%rip), %eax
	incl	%eax
	movl	%eax, stamp(%rip)
	cmpl	$-1, %eax
	jne	.LBB3_4
# BB#3:                                 # %.loopexit.loopexit
	movl	$1, stamp(%rip)
	movl	$multvec_j, %edi
	xorl	%esi, %esi
	movl	$400, %edx              # imm = 0x190
	callq	memset
	movl	$multvec_i, %edi
	xorl	%esi, %esi
	movl	$400, %edx              # imm = 0x190
	callq	memset
	movl	$1, %eax
.LBB3_4:                                # %.loopexit
	movl	%eax, multvec_i(,%r14,4)
	movl	%eax, multvec_j(,%r15,4)
	cmpl	$2, %ebx
	jl	.LBB3_54
# BB#5:
	movq	$-1, %rdx
	.p2align	4, 0x90
.LBB3_6:                                # =>This Inner Loop Header: Depth=1
	leaq	1(%rdx), %rcx
	cmpq	%rbx, %rcx
	jge	.LBB3_8
# BB#7:                                 #   in Loop: Header=BB3_6 Depth=1
	cmpl	%eax, multvec_i+4(,%rdx,4)
	movq	%rcx, %rdx
	je	.LBB3_6
.LBB3_8:                                # %.critedge.preheader.i
	cmpl	%ebx, %ecx
	jge	.LBB3_54
# BB#9:
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	jmp	.LBB3_12
	.p2align	4, 0x90
.LBB3_10:                               # %.critedge.loopexit.i
                                        #   in Loop: Header=BB3_12 Depth=1
	cmpl	%edx, %ecx
	jge	.LBB3_54
# BB#11:                                # %.critedge.loopexit.i..lr.ph.i_crit_edge
                                        #   in Loop: Header=BB3_12 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	56(%rdx), %rbp
.LBB3_12:                               # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_13 Depth 2
                                        #       Child Loop BB3_28 Depth 3
                                        #       Child Loop BB3_44 Depth 3
                                        #     Child Loop BB3_52 Depth 2
	movslq	%ecx, %r14
	movq	(%rbp,%r14,8), %rcx
	movq	24(%rcx), %r13
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	jmp	.LBB3_13
	.p2align	4, 0x90
.LBB3_49:                               # %cont_BackTrack.exit._crit_edge.i
                                        #   in Loop: Header=BB3_13 Depth=2
	movl	stamp(%rip), %eax
.LBB3_13:                               #   Parent Loop BB3_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_28 Depth 3
                                        #       Child Loop BB3_44 Depth 3
	movslq	%r15d, %rbp
	cmpl	%eax, multvec_j(,%rbp,4)
	jne	.LBB3_15
# BB#14:                                #   in Loop: Header=BB3_13 Depth=2
	incl	%r15d
	jmp	.LBB3_47
	.p2align	4, 0x90
.LBB3_15:                               #   in Loop: Header=BB3_13 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rbx
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	callq	unify_MatchBindings
	testl	%eax, %eax
	je	.LBB3_17
.LBB3_16:                               #   in Loop: Header=BB3_13 Depth=2
	movl	$1, %r12d
	xorps	%xmm0, %xmm0
	jmp	.LBB3_42
.LBB3_17:                               #   in Loop: Header=BB3_13 Depth=2
	movl	(%r13), %ecx
	cmpl	(%rbx), %ecx
	xorps	%xmm0, %xmm0
	jne	.LBB3_41
# BB#18:                                #   in Loop: Header=BB3_13 Depth=2
	movl	fol_NOT(%rip), %eax
	cmpl	%eax, %ecx
	movl	%ecx, %edx
	jne	.LBB3_20
# BB#19:                                #   in Loop: Header=BB3_13 Depth=2
	movq	16(%r13), %rdx
	movq	8(%rdx), %rdx
	movl	(%rdx), %edx
.LBB3_20:                               # %fol_Atom.exit125.i
                                        #   in Loop: Header=BB3_13 Depth=2
	cmpl	%edx, fol_EQUALITY(%rip)
	jne	.LBB3_41
# BB#21:                                #   in Loop: Header=BB3_13 Depth=2
	cmpl	%eax, %ecx
	movl	%ecx, %edi
	jne	.LBB3_23
# BB#22:                                #   in Loop: Header=BB3_13 Depth=2
	movq	16(%rbx), %rdi
	movq	8(%rdi), %rdi
	movl	(%rdi), %edi
.LBB3_23:                               # %fol_Atom.exit117.i
                                        #   in Loop: Header=BB3_13 Depth=2
	cmpl	%edi, %edx
	jne	.LBB3_41
# BB#24:                                #   in Loop: Header=BB3_13 Depth=2
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	56(%rdx), %rdx
	movq	(%rdx,%r14,8), %rdx
	cmpl	$0, 8(%rdx)
	je	.LBB3_26
# BB#25:                                #   in Loop: Header=BB3_13 Depth=2
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	56(%rdx), %rdx
	movq	(%rdx,%rbp,8), %rdx
	cmpl	$0, 8(%rdx)
	je	.LBB3_26
.LBB3_41:                               #   in Loop: Header=BB3_13 Depth=2
	incl	%r15d
.LBB3_42:                               #   in Loop: Header=BB3_13 Depth=2
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB3_45
# BB#43:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB3_13 Depth=2
	incl	%eax
	.p2align	4, 0x90
.LBB3_44:                               # %.lr.ph.i.i
                                        #   Parent Loop BB3_12 Depth=1
                                        #     Parent Loop BB3_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB3_44
.LBB3_45:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB3_13 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB3_47
# BB#46:                                #   in Loop: Header=BB3_13 Depth=2
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB3_47:                               # %cont_BackTrack.exit.i
                                        #   in Loop: Header=BB3_13 Depth=2
	testl	%r12d, %r12d
	sete	%al
	cmpl	28(%rsp), %r15d         # 4-byte Folded Reload
	jge	.LBB3_50
# BB#48:                                # %cont_BackTrack.exit.i
                                        #   in Loop: Header=BB3_13 Depth=2
	testb	%al, %al
	jne	.LBB3_49
	jmp	.LBB3_50
.LBB3_26:                               #   in Loop: Header=BB3_13 Depth=2
	movl	cont_BINDINGS(%rip), %edx
	testl	%edx, %edx
	jle	.LBB3_30
# BB#27:                                # %.lr.ph.i108.i.preheader
                                        #   in Loop: Header=BB3_13 Depth=2
	incl	%edx
	.p2align	4, 0x90
.LBB3_28:                               # %.lr.ph.i108.i
                                        #   Parent Loop BB3_12 Depth=1
                                        #     Parent Loop BB3_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdi
	movq	%rdi, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rdx), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%edx
	cmpl	$1, %edx
	jg	.LBB3_28
# BB#29:                                # %cont_BackTrackAndStart.exit.loopexit.i
                                        #   in Loop: Header=BB3_13 Depth=2
	movl	(%r13), %ecx
.LBB3_30:                               # %cont_BackTrackAndStart.exit.i
                                        #   in Loop: Header=BB3_13 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	cmpl	%eax, %ecx
	movq	%r13, %rcx
	jne	.LBB3_32
# BB#31:                                #   in Loop: Header=BB3_13 Depth=2
	movq	16(%r13), %rcx
	movq	8(%rcx), %rcx
.LBB3_32:                               # %fol_Atom.exit104.i
                                        #   in Loop: Header=BB3_13 Depth=2
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rsi
	cmpl	%eax, (%rbx)
	movq	%rbx, %rax
	jne	.LBB3_34
# BB#33:                                #   in Loop: Header=BB3_13 Depth=2
	movq	16(%rbx), %rax
	movq	8(%rax), %rax
.LBB3_34:                               # %fol_Atom.exit96.i
                                        #   in Loop: Header=BB3_13 Depth=2
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	callq	unify_MatchBindings
	testl	%eax, %eax
	je	.LBB3_40
# BB#35:                                #   in Loop: Header=BB3_13 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movl	fol_NOT(%rip), %eax
	movq	%r13, %rdx
	cmpl	%eax, (%rdx)
	movq	%rdx, %rcx
	jne	.LBB3_37
# BB#36:                                #   in Loop: Header=BB3_13 Depth=2
	movq	16(%rdx), %rcx
	movq	8(%rcx), %rcx
.LBB3_37:                               # %fol_Atom.exit88.i
                                        #   in Loop: Header=BB3_13 Depth=2
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rsi
	cmpl	%eax, (%rbx)
	jne	.LBB3_39
# BB#38:                                #   in Loop: Header=BB3_13 Depth=2
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
.LBB3_39:                               # %fol_Atom.exit.i
                                        #   in Loop: Header=BB3_13 Depth=2
	movq	16(%rbx), %rax
	movq	8(%rax), %rdx
	callq	unify_MatchBindings
	testl	%eax, %eax
	jne	.LBB3_16
.LBB3_40:                               #   in Loop: Header=BB3_13 Depth=2
	incl	%r15d
	xorps	%xmm0, %xmm0
	jmp	.LBB3_42
	.p2align	4, 0x90
.LBB3_50:                               # %.critedge1.i
                                        #   in Loop: Header=BB3_12 Depth=1
	testl	%r12d, %r12d
	movq	32(%rsp), %rdx          # 8-byte Reload
	je	.LBB3_55
# BB#51:                                # %.preheader.i
                                        #   in Loop: Header=BB3_12 Depth=1
	movl	stamp(%rip), %eax
	.p2align	4, 0x90
.LBB3_52:                               #   Parent Loop BB3_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%r14), %rcx
	cmpq	%rdx, %rcx
	jge	.LBB3_10
# BB#53:                                #   in Loop: Header=BB3_52 Depth=2
	cmpl	%eax, multvec_i+4(,%r14,4)
	movq	%rcx, %r14
	je	.LBB3_52
	jmp	.LBB3_10
.LBB3_54:                               # %subs_TestlitsEqExcept.exit.thread
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	subs_STMultiExceptIntern # TAILCALL
.Lfunc_end3:
	.size	subs_STMultiExcept, .Lfunc_end3-subs_STMultiExcept
	.cfi_endproc

	.p2align	4, 0x90
	.type	subs_STMultiExceptIntern,@function
subs_STMultiExceptIntern:               # @subs_STMultiExceptIntern
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 80
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	68(%r15), %eax
	addl	64(%r15), %eax
	movl	$1, %r12d
	addl	72(%r15), %eax
	jle	.LBB4_59
# BB#1:                                 # %.lr.ph162
	movslq	68(%rsi), %rax
	movslq	72(%rsi), %rcx
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movslq	64(%rsi), %r13
	addq	%rax, %r13
	addq	%rcx, %r13
	xorl	%ebx, %ebx
	movl	$-1, %ebp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movl	multvec_i(,%rbx,4), %eax
	cmpl	stamp(%rip), %eax
	je	.LBB4_8
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB4_5
# BB#4:                                 #   in Loop: Header=BB4_2 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB4_5:                                # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB4_2 Depth=1
	callq	term_NumberOfVarOccs
	testl	%ebp, %ebp
	js	.LBB4_6
# BB#7:                                 #   in Loop: Header=BB4_2 Depth=1
	cmpl	%r14d, %eax
	cmoval	%eax, %r14d
	cmoval	%ebx, %ebp
	jmp	.LBB4_8
.LBB4_6:                                # %clause_GetLiteralAtom.exit._crit_edge
                                        #   in Loop: Header=BB4_2 Depth=1
	movl	%eax, %r14d
	movl	%ebx, %ebp
	.p2align	4, 0x90
.LBB4_8:                                #   in Loop: Header=BB4_2 Depth=1
	incq	%rbx
	movslq	68(%r15), %rax
	movslq	72(%r15), %rcx
	movslq	64(%r15), %rdx
	addq	%rax, %rdx
	addq	%rcx, %rdx
	cmpq	%rdx, %rbx
	jl	.LBB4_2
# BB#9:                                 # %._crit_edge163
	testl	%ebp, %ebp
	js	.LBB4_59
# BB#10:
	movq	56(%r15), %rax
	movslq	%ebp, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %r12
	movl	stamp(%rip), %eax
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	%eax, multvec_i(,%rcx,4)
	testl	%r13d, %r13d
	movq	16(%rsp), %rbp          # 8-byte Reload
	jle	.LBB4_58
# BB#11:                                # %.lr.ph
	xorl	%r14d, %r14d
	cmpl	%eax, multvec_j(,%r14,4)
	je	.LBB4_56
	jmp	.LBB4_13
	.p2align	4, 0x90
.LBB4_57:                               # %cont_BackTrack.exit144._crit_edge
                                        #   in Loop: Header=BB4_56 Depth=1
	movl	stamp(%rip), %eax
	cmpl	%eax, multvec_j(,%r14,4)
	je	.LBB4_56
.LBB4_13:
	movq	56(%rbp), %rax
	movq	(%rax,%r14,8), %rax
	movq	24(%rax), %rbx
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	unify_MatchBindings
	testl	%eax, %eax
	je	.LBB4_21
# BB#14:
	movl	stamp(%rip), %eax
	movl	%eax, multvec_j(,%r14,4)
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	subs_STMultiExceptIntern
	testl	%eax, %eax
	jne	.LBB4_15
# BB#20:
	movl	$0, multvec_j(,%r14,4)
.LBB4_21:
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	xorps	%xmm0, %xmm0
	jle	.LBB4_22
	.p2align	4, 0x90
.LBB4_23:                               # %.lr.ph.i130
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB4_23
	jmp	.LBB4_24
	.p2align	4, 0x90
.LBB4_22:
	movl	%ecx, %eax
.LBB4_24:                               # %._crit_edge.i131
	movslq	cont_STACKPOINTER(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB4_25
# BB#26:
	leaq	-1(%rdx), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdx,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB4_27
	.p2align	4, 0x90
.LBB4_25:
	xorl	%ecx, %ecx
.LBB4_27:                               # %cont_BackTrack.exit132
	movl	(%r12), %esi
	cmpl	(%rbx), %esi
	jne	.LBB4_56
# BB#28:
	movl	fol_NOT(%rip), %edx
	cmpl	%edx, %esi
	movl	%esi, %edi
	jne	.LBB4_30
# BB#29:
	movq	16(%r12), %rdi
	movq	8(%rdi), %rdi
	movl	(%rdi), %edi
.LBB4_30:                               # %fol_Atom.exit126
	cmpl	%edi, fol_EQUALITY(%rip)
	jne	.LBB4_56
# BB#31:
	cmpl	%edx, %esi
	jne	.LBB4_33
# BB#32:
	movq	16(%rbx), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %esi
.LBB4_33:                               # %fol_Atom.exit118
	cmpl	%esi, %edi
	jne	.LBB4_56
# BB#34:
	movq	56(%r15), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rsi,%rdi,8), %rsi
	cmpl	$0, 8(%rsi)
	je	.LBB4_36
# BB#35:
	movq	56(%rbp), %rsi
	movq	(%rsi,%r14,8), %rsi
	cmpl	$0, 8(%rsi)
	jne	.LBB4_56
.LBB4_36:
	leal	1(%rcx), %esi
	movl	%esi, cont_STACKPOINTER(%rip)
	movslq	%ecx, %rcx
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	cmpl	%edx, (%r12)
	movq	%r12, %rax
	jne	.LBB4_38
# BB#37:
	movq	16(%r12), %rax
	movq	8(%rax), %rax
.LBB4_38:                               # %fol_Atom.exit110
	movq	16(%rax), %rax
	movq	8(%rax), %rsi
	cmpl	%edx, (%rbx)
	movq	%rbx, %rax
	jne	.LBB4_40
# BB#39:
	movq	16(%rbx), %rax
	movq	8(%rax), %rax
.LBB4_40:                               # %fol_Atom.exit102
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	callq	unify_MatchBindings
	testl	%eax, %eax
	je	.LBB4_51
# BB#41:
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	%eax, (%r12)
	movq	%r12, %rcx
	jne	.LBB4_43
# BB#42:
	movq	16(%r12), %rcx
	movq	8(%rcx), %rcx
.LBB4_43:                               # %fol_Atom.exit94
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rsi
	cmpl	%eax, (%rbx)
	jne	.LBB4_45
# BB#44:
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
.LBB4_45:                               # %fol_Atom.exit
	movq	16(%rbx), %rax
	movq	8(%rax), %rdx
	callq	unify_MatchBindings
	testl	%eax, %eax
	je	.LBB4_51
# BB#46:
	movl	stamp(%rip), %eax
	movl	%eax, multvec_j(,%r14,4)
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	subs_STMultiExceptIntern
	testl	%eax, %eax
	jne	.LBB4_47
# BB#50:
	movl	$0, multvec_j(,%r14,4)
.LBB4_51:
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jle	.LBB4_54
# BB#52:                                # %.lr.ph.i142.preheader
	incl	%eax
	.p2align	4, 0x90
.LBB4_53:                               # %.lr.ph.i142
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB4_53
.LBB4_54:                               # %._crit_edge.i143
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB4_56
# BB#55:
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	.p2align	4, 0x90
.LBB4_56:                               # %cont_BackTrack.exit144
                                        # =>This Inner Loop Header: Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB4_57
.LBB4_58:                               # %._crit_edge
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$0, multvec_i(,%rax,4)
	xorl	%r12d, %r12d
.LBB4_59:                               # %cont_BackTrack.exit138
	movl	%r12d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_15:
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB4_18
# BB#16:                                # %.lr.ph.i136.preheader
	incl	%eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB4_17:                               # %.lr.ph.i136
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB4_17
.LBB4_18:                               # %._crit_edge.i137
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	movl	$1, %r12d
	je	.LBB4_59
# BB#19:
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB4_59
.LBB4_47:
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB4_18
# BB#48:                                # %.lr.ph.i.preheader
	incl	%eax
	xorps	%xmm0, %xmm0
.LBB4_49:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB4_49
	jmp	.LBB4_18
.Lfunc_end4:
	.size	subs_STMultiExceptIntern, .Lfunc_end4-subs_STMultiExceptIntern
	.cfi_endproc

	.globl	subs_SubsumesBasic
	.p2align	4, 0x90
	.type	subs_SubsumesBasic,@function
subs_SubsumesBasic:                     # @subs_SubsumesBasic
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi59:
	.cfi_def_cfa_offset 80
.Lcfi60:
	.cfi_offset %rbx, -56
.Lcfi61:
	.cfi_offset %r12, -48
.Lcfi62:
	.cfi_offset %r13, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r12
	movq	%rdi, %rbp
	movl	64(%rbp), %r10d
	movl	68(%rbp), %edx
	addl	%r10d, %edx
	movl	72(%rbp), %r13d
	addl	%edx, %r13d
	movl	64(%r12), %r9d
	movl	68(%r12), %ebx
	addl	%r9d, %ebx
	movl	72(%r12), %r15d
	addl	%ebx, %r15d
	xorl	%eax, %eax
	testl	%r14d, %r14d
	movl	$0, %edi
	js	.LBB5_2
# BB#1:
	movq	56(%rbp), %rdi
	movslq	%r14d, %rsi
	movq	(%rdi,%rsi,8), %rsi
	movl	4(%rsi), %edi
.LBB5_2:
	testl	%ecx, %ecx
	js	.LBB5_4
# BB#3:
	movq	56(%r12), %rax
	movslq	%ecx, %rsi
	movq	(%rax,%rsi,8), %rax
	movl	4(%rax), %eax
.LBB5_4:
	cmpl	%r15d, %r13d
	jg	.LBB5_17
# BB#5:
	movl	4(%rbp), %esi
	subl	%edi, %esi
	movl	4(%r12), %edi
	subl	%eax, %edi
	cmpl	%edi, %esi
	jbe	.LBB5_6
.LBB5_17:
	xorl	%eax, %eax
	addq	$24, %rsp
.LBB5_18:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_6:
	movl	stamp(%rip), %eax
	incl	%eax
	movl	%eax, stamp(%rip)
	cmpl	$-1, %eax
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movl	%edx, 4(%rsp)           # 4-byte Spill
	jne	.LBB5_8
# BB#7:                                 # %.loopexit.loopexit
	movl	$1, stamp(%rip)
	movl	$multvec_j, %edi
	xorl	%esi, %esi
	movl	$400, %edx              # imm = 0x190
	movl	%r10d, 20(%rsp)         # 4-byte Spill
	movl	%r9d, 16(%rsp)          # 4-byte Spill
	movl	%ecx, %ebx
	callq	memset
	movl	$multvec_i, %edi
	xorl	%esi, %esi
	movl	$400, %edx              # imm = 0x190
	callq	memset
	movl	%ebx, %ecx
	movl	16(%rsp), %r9d          # 4-byte Reload
	movl	4(%rsp), %edx           # 4-byte Reload
	movl	20(%rsp), %r10d         # 4-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	$1, %eax
.LBB5_8:                                # %.loopexit
	testl	%r14d, %r14d
	js	.LBB5_10
# BB#9:
	movslq	%r14d, %rsi
	movl	%eax, multvec_i(,%rsi,4)
.LBB5_10:
	testl	%ecx, %ecx
	js	.LBB5_12
# BB#11:
	movslq	%ecx, %rcx
	movl	%eax, multvec_j(,%rcx,4)
.LBB5_12:
	cmpl	$2, %r13d
	jl	.LBB5_16
# BB#13:
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	movq	%rbp, %rdi
	movl	%r10d, %edx
	movq	%r12, %rcx
	movl	%r10d, %ebx
	movl	%r9d, %r14d
	callq	subs_PartnerTest
	movl	%r14d, %r8d
	movl	4(%rsp), %edx           # 4-byte Reload
	movl	%ebx, %esi
	movq	8(%rsp), %r9            # 8-byte Reload
	testl	%eax, %eax
	je	.LBB5_17
# BB#14:
	movq	%rbp, %rdi
	movq	%r12, %rcx
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	callq	subs_PartnerTest
	movl	4(%rsp), %esi           # 4-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	testl	%eax, %eax
	je	.LBB5_17
# BB#15:
	movq	%rbp, %rdi
	movl	%r13d, %edx
	movq	%r12, %rcx
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%r15d, %r9d
	callq	subs_PartnerTest
	movl	%r14d, %r9d
	movl	4(%rsp), %edx           # 4-byte Reload
	movl	%ebx, %r10d
	movq	8(%rsp), %rbx           # 8-byte Reload
	testl	%eax, %eax
	je	.LBB5_17
.LBB5_16:
	movq	%rbp, %rdi
	movl	%r10d, %esi
	movl	%r13d, %ecx
	movq	%r12, %r8
	pushq	%r15
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi67:
	.cfi_adjust_cfa_offset 8
	callq	subs_SubsumesInternBasic
	addq	$40, %rsp
.Lcfi68:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB5_18
.Lfunc_end5:
	.size	subs_SubsumesBasic, .Lfunc_end5-subs_SubsumesBasic
	.cfi_endproc

	.p2align	4, 0x90
	.type	subs_PartnerTest,@function
subs_PartnerTest:                       # @subs_PartnerTest
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi72:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi73:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi75:
	.cfi_def_cfa_offset 96
.Lcfi76:
	.cfi_offset %rbx, -56
.Lcfi77:
	.cfi_offset %r12, -48
.Lcfi78:
	.cfi_offset %r13, -40
.Lcfi79:
	.cfi_offset %r14, -32
.Lcfi80:
	.cfi_offset %r15, -24
.Lcfi81:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movl	%r8d, 8(%rsp)           # 4-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	cmpl	%edx, %esi
	je	.LBB6_52
# BB#1:                                 # %.preheader123
	movl	stamp(%rip), %ecx
	movslq	%esi, %rax
	movslq	%edx, %rsi
	decq	%rax
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	leaq	1(%rax), %rdx
	cmpq	%rsi, %rdx
	jge	.LBB6_4
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	cmpl	%ecx, multvec_i+4(,%rax,4)
	movq	%rdx, %rax
	je	.LBB6_2
.LBB6_4:
	cmpq	%rsi, %rdx
	jge	.LBB6_52
# BB#5:
	xorl	%eax, %eax
	cmpl	%r14d, 8(%rsp)          # 4-byte Folded Reload
	je	.LBB6_53
# BB#6:                                 # %.preheader122
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_7:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_8 Depth 2
                                        #       Child Loop BB6_23 Depth 3
                                        #       Child Loop BB6_40 Depth 3
                                        #     Child Loop BB6_49 Depth 2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movslq	%edx, %r15
	movq	(%rax,%r15,8), %rax
	movq	24(%rax), %r13
	xorl	%edi, %edi
	movl	8(%rsp), %r12d          # 4-byte Reload
	jmp	.LBB6_8
	.p2align	4, 0x90
.LBB6_45:                               # %cont_BackTrack.exit._crit_edge
                                        #   in Loop: Header=BB6_8 Depth=2
	movl	stamp(%rip), %ecx
.LBB6_8:                                #   Parent Loop BB6_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_23 Depth 3
                                        #       Child Loop BB6_40 Depth 3
	movslq	%r12d, %rbx
	cmpl	%ecx, multvec_j(,%rbx,4)
	jne	.LBB6_10
# BB#9:                                 #   in Loop: Header=BB6_8 Depth=2
	incl	%r12d
	jmp	.LBB6_43
	.p2align	4, 0x90
.LBB6_10:                               #   in Loop: Header=BB6_8 Depth=2
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rbp
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	unify_MatchBindings
	testl	%eax, %eax
	je	.LBB6_12
.LBB6_11:                               #   in Loop: Header=BB6_8 Depth=2
	movl	$1, %edi
	xorps	%xmm0, %xmm0
	jmp	.LBB6_38
.LBB6_12:                               #   in Loop: Header=BB6_8 Depth=2
	movl	(%r13), %ecx
	cmpl	(%rbp), %ecx
	xorps	%xmm0, %xmm0
	jne	.LBB6_36
# BB#13:                                #   in Loop: Header=BB6_8 Depth=2
	movl	fol_NOT(%rip), %eax
	cmpl	%eax, %ecx
	movl	%ecx, %edx
	jne	.LBB6_15
# BB#14:                                #   in Loop: Header=BB6_8 Depth=2
	movq	16(%r13), %rdx
	movq	8(%rdx), %rdx
	movl	(%rdx), %edx
.LBB6_15:                               # %fol_Atom.exit115
                                        #   in Loop: Header=BB6_8 Depth=2
	cmpl	%edx, fol_EQUALITY(%rip)
	jne	.LBB6_36
# BB#16:                                #   in Loop: Header=BB6_8 Depth=2
	cmpl	%eax, %ecx
	movl	%ecx, %edi
	jne	.LBB6_18
# BB#17:                                #   in Loop: Header=BB6_8 Depth=2
	movq	16(%rbp), %rdi
	movq	8(%rdi), %rdi
	movl	(%rdi), %edi
.LBB6_18:                               # %fol_Atom.exit107
                                        #   in Loop: Header=BB6_8 Depth=2
	cmpl	%edi, %edx
	jne	.LBB6_36
# BB#19:                                #   in Loop: Header=BB6_8 Depth=2
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	56(%rdx), %rdx
	movq	(%rdx,%r15,8), %rdx
	cmpl	$0, 8(%rdx)
	je	.LBB6_21
# BB#20:                                #   in Loop: Header=BB6_8 Depth=2
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	56(%rdx), %rdx
	movq	(%rdx,%rbx,8), %rdx
	cmpl	$0, 8(%rdx)
	je	.LBB6_21
.LBB6_36:                               #   in Loop: Header=BB6_8 Depth=2
	incl	%r12d
.LBB6_37:                               #   in Loop: Header=BB6_8 Depth=2
	movl	12(%rsp), %edi          # 4-byte Reload
.LBB6_38:                               #   in Loop: Header=BB6_8 Depth=2
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB6_41
# BB#39:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB6_8 Depth=2
	incl	%eax
	.p2align	4, 0x90
.LBB6_40:                               # %.lr.ph.i
                                        #   Parent Loop BB6_7 Depth=1
                                        #     Parent Loop BB6_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB6_40
.LBB6_41:                               # %._crit_edge.i
                                        #   in Loop: Header=BB6_8 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB6_43
# BB#42:                                #   in Loop: Header=BB6_8 Depth=2
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB6_43:                               # %cont_BackTrack.exit
                                        #   in Loop: Header=BB6_8 Depth=2
	testl	%edi, %edi
	sete	%al
	cmpl	%r14d, %r12d
	jge	.LBB6_46
# BB#44:                                # %cont_BackTrack.exit
                                        #   in Loop: Header=BB6_8 Depth=2
	testb	%al, %al
	jne	.LBB6_45
	jmp	.LBB6_46
.LBB6_21:                               #   in Loop: Header=BB6_8 Depth=2
	movl	cont_BINDINGS(%rip), %edx
	testl	%edx, %edx
	jle	.LBB6_25
# BB#22:                                # %.lr.ph.i98.preheader
                                        #   in Loop: Header=BB6_8 Depth=2
	incl	%edx
	.p2align	4, 0x90
.LBB6_23:                               # %.lr.ph.i98
                                        #   Parent Loop BB6_7 Depth=1
                                        #     Parent Loop BB6_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdi
	movq	%rdi, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rdx), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%edx
	cmpl	$1, %edx
	jg	.LBB6_23
# BB#24:                                # %cont_BackTrackAndStart.exit.loopexit
                                        #   in Loop: Header=BB6_8 Depth=2
	movl	(%r13), %ecx
.LBB6_25:                               # %cont_BackTrackAndStart.exit
                                        #   in Loop: Header=BB6_8 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	cmpl	%eax, %ecx
	movq	%r13, %rcx
	jne	.LBB6_27
# BB#26:                                #   in Loop: Header=BB6_8 Depth=2
	movq	16(%r13), %rcx
	movq	8(%rcx), %rcx
.LBB6_27:                               # %fol_Atom.exit94
                                        #   in Loop: Header=BB6_8 Depth=2
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rsi
	cmpl	%eax, (%rbp)
	movq	%rbp, %rax
	jne	.LBB6_29
# BB#28:                                #   in Loop: Header=BB6_8 Depth=2
	movq	16(%rbp), %rax
	movq	8(%rax), %rax
.LBB6_29:                               # %fol_Atom.exit86
                                        #   in Loop: Header=BB6_8 Depth=2
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	callq	unify_MatchBindings
	testl	%eax, %eax
	je	.LBB6_35
# BB#30:                                #   in Loop: Header=BB6_8 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movl	fol_NOT(%rip), %eax
	movq	%r13, %rdx
	cmpl	%eax, (%rdx)
	movq	%rdx, %rcx
	jne	.LBB6_32
# BB#31:                                #   in Loop: Header=BB6_8 Depth=2
	movq	16(%rdx), %rcx
	movq	8(%rcx), %rcx
.LBB6_32:                               # %fol_Atom.exit78
                                        #   in Loop: Header=BB6_8 Depth=2
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rsi
	cmpl	%eax, (%rbp)
	jne	.LBB6_34
# BB#33:                                #   in Loop: Header=BB6_8 Depth=2
	movq	16(%rbp), %rax
	movq	8(%rax), %rbp
.LBB6_34:                               # %fol_Atom.exit
                                        #   in Loop: Header=BB6_8 Depth=2
	movq	16(%rbp), %rax
	movq	8(%rax), %rdx
	callq	unify_MatchBindings
	testl	%eax, %eax
	jne	.LBB6_11
.LBB6_35:                               #   in Loop: Header=BB6_8 Depth=2
	incl	%r12d
	xorps	%xmm0, %xmm0
	jmp	.LBB6_37
	.p2align	4, 0x90
.LBB6_46:                               # %.critedge
                                        #   in Loop: Header=BB6_7 Depth=1
	testl	%edi, %edi
	je	.LBB6_47
# BB#48:                                # %.preheader
                                        #   in Loop: Header=BB6_7 Depth=1
	movl	stamp(%rip), %ecx
	movq	32(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_49:                               #   Parent Loop BB6_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%r15), %rdx
	cmpq	%rsi, %rdx
	jge	.LBB6_51
# BB#50:                                #   in Loop: Header=BB6_49 Depth=2
	cmpl	%ecx, multvec_i+4(,%r15,4)
	movq	%rdx, %r15
	je	.LBB6_49
.LBB6_51:                               # %.critedge1
                                        #   in Loop: Header=BB6_7 Depth=1
	cmpq	%rsi, %rdx
	jl	.LBB6_7
.LBB6_52:
	movl	$1, %eax
.LBB6_53:                               # %.loopexit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_47:
	xorl	%eax, %eax
	jmp	.LBB6_53
.Lfunc_end6:
	.size	subs_PartnerTest, .Lfunc_end6-subs_PartnerTest
	.cfi_endproc

	.p2align	4, 0x90
	.type	subs_SubsumesInternBasic,@function
subs_SubsumesInternBasic:               # @subs_SubsumesInternBasic
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi85:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi88:
	.cfi_def_cfa_offset 96
.Lcfi89:
	.cfi_offset %rbx, -56
.Lcfi90:
	.cfi_offset %r12, -48
.Lcfi91:
	.cfi_offset %r13, -40
.Lcfi92:
	.cfi_offset %r14, -32
.Lcfi93:
	.cfi_offset %r15, -24
.Lcfi94:
	.cfi_offset %rbp, -16
	movl	%r9d, 4(%rsp)           # 4-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movl	%edx, 8(%rsp)           # 4-byte Spill
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movq	%rdi, %r14
	movl	$1, %r12d
	testl	%ecx, %ecx
	jle	.LBB7_61
# BB#1:                                 # %.lr.ph175
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	movl	%ecx, %r15d
	xorl	%ebp, %ebp
	movl	$-1, %r13d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_2:                                # =>This Inner Loop Header: Depth=1
	movl	multvec_i(,%rbp,4), %eax
	cmpl	stamp(%rip), %eax
	je	.LBB7_8
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB7_5
# BB#4:                                 #   in Loop: Header=BB7_2 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB7_5:                                # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB7_2 Depth=1
	callq	term_NumberOfVarOccs
	testl	%r13d, %r13d
	js	.LBB7_6
# BB#7:                                 #   in Loop: Header=BB7_2 Depth=1
	cmpl	%ebx, %eax
	cmoval	%eax, %ebx
	cmoval	%ebp, %r13d
	jmp	.LBB7_8
.LBB7_6:                                # %clause_GetLiteralAtom.exit._crit_edge
                                        #   in Loop: Header=BB7_2 Depth=1
	movl	%eax, %ebx
	movl	%ebp, %r13d
	.p2align	4, 0x90
.LBB7_8:                                #   in Loop: Header=BB7_2 Depth=1
	incq	%rbp
	cmpq	%rbp, %r15
	jne	.LBB7_2
# BB#9:                                 # %._crit_edge176
	testl	%r13d, %r13d
	js	.LBB7_61
# BB#10:
	movq	56(%r14), %rax
	movslq	%r13d, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %r12
	movl	stamp(%rip), %eax
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	%eax, multvec_i(,%rcx,4)
	xorl	%edx, %edx
	cmpl	12(%rsp), %r13d         # 4-byte Folded Reload
	movl	4(%rsp), %ecx           # 4-byte Reload
	jl	.LBB7_12
# BB#11:
	cmpl	8(%rsp), %r13d          # 4-byte Folded Reload
	movl	104(%rsp), %ecx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	96(%rsp), %edx
	cmovll	%edx, %ecx
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	cmovll	4(%rsp), %edx           # 4-byte Folded Reload
.LBB7_12:                               # %.preheader
	cmpl	%ecx, %edx
	jge	.LBB7_60
# BB#13:                                # %.lr.ph
	movslq	%edx, %rbp
	movslq	%ecx, %rbx
	cmpl	%eax, multvec_j(,%rbp,4)
	je	.LBB7_58
	jmp	.LBB7_15
	.p2align	4, 0x90
.LBB7_59:                               # %cont_BackTrack.exit162._crit_edge
                                        #   in Loop: Header=BB7_58 Depth=1
	movl	stamp(%rip), %eax
	cmpl	%eax, multvec_j(,%rbp,4)
	je	.LBB7_58
.LBB7_15:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %r13
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r12, %rsi
	movq	%r13, %rdx
	callq	unify_MatchBindings
	testl	%eax, %eax
	je	.LBB7_23
# BB#16:
	movl	stamp(%rip), %eax
	movl	%eax, multvec_j(,%rbp,4)
	movq	%r14, %rdi
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	8(%rsp), %edx           # 4-byte Reload
	movl	%r15d, %ecx
	movq	16(%rsp), %r8           # 8-byte Reload
	movl	4(%rsp), %r9d           # 4-byte Reload
	movl	104(%rsp), %eax
	pushq	%rax
.Lcfi95:
	.cfi_adjust_cfa_offset 8
	movl	104(%rsp), %eax
	pushq	%rax
.Lcfi96:
	.cfi_adjust_cfa_offset 8
	callq	subs_SubsumesInternBasic
	addq	$16, %rsp
.Lcfi97:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB7_17
# BB#22:
	movl	$0, multvec_j(,%rbp,4)
.LBB7_23:
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	xorps	%xmm0, %xmm0
	jle	.LBB7_24
	.p2align	4, 0x90
.LBB7_25:                               # %.lr.ph.i148
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB7_25
	jmp	.LBB7_26
	.p2align	4, 0x90
.LBB7_24:
	movl	%ecx, %eax
.LBB7_26:                               # %._crit_edge.i149
	movslq	cont_STACKPOINTER(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB7_27
# BB#28:
	leaq	-1(%rdx), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdx,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB7_29
	.p2align	4, 0x90
.LBB7_27:
	xorl	%ecx, %ecx
.LBB7_29:                               # %cont_BackTrack.exit150
	movl	(%r12), %esi
	cmpl	(%r13), %esi
	jne	.LBB7_58
# BB#30:
	movl	fol_NOT(%rip), %edx
	cmpl	%edx, %esi
	movl	%esi, %edi
	jne	.LBB7_32
# BB#31:
	movq	16(%r12), %rdi
	movq	8(%rdi), %rdi
	movl	(%rdi), %edi
.LBB7_32:                               # %fol_Atom.exit144
	cmpl	%edi, fol_EQUALITY(%rip)
	jne	.LBB7_58
# BB#33:
	cmpl	%edx, %esi
	jne	.LBB7_35
# BB#34:
	movq	16(%r13), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %esi
.LBB7_35:                               # %fol_Atom.exit136
	cmpl	%esi, %edi
	jne	.LBB7_58
# BB#36:
	movq	56(%r14), %rsi
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rsi,%rdi,8), %rsi
	cmpl	$0, 8(%rsi)
	je	.LBB7_38
# BB#37:
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsi), %rsi
	movq	(%rsi,%rbp,8), %rsi
	cmpl	$0, 8(%rsi)
	jne	.LBB7_58
.LBB7_38:
	leal	1(%rcx), %esi
	movl	%esi, cont_STACKPOINTER(%rip)
	movslq	%ecx, %rcx
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	cmpl	%edx, (%r12)
	movq	%r12, %rax
	jne	.LBB7_40
# BB#39:
	movq	16(%r12), %rax
	movq	8(%rax), %rax
.LBB7_40:                               # %fol_Atom.exit128
	movq	16(%rax), %rax
	movq	8(%rax), %rsi
	cmpl	%edx, (%r13)
	movq	%r13, %rax
	jne	.LBB7_42
# BB#41:
	movq	16(%r13), %rax
	movq	8(%rax), %rax
.LBB7_42:                               # %fol_Atom.exit120
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	callq	unify_MatchBindings
	testl	%eax, %eax
	je	.LBB7_53
# BB#43:
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	%eax, (%r12)
	movq	%r12, %rcx
	jne	.LBB7_45
# BB#44:
	movq	16(%r12), %rcx
	movq	8(%rcx), %rcx
.LBB7_45:                               # %fol_Atom.exit112
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rsi
	cmpl	%eax, (%r13)
	jne	.LBB7_47
# BB#46:
	movq	16(%r13), %rax
	movq	8(%rax), %r13
.LBB7_47:                               # %fol_Atom.exit
	movq	16(%r13), %rax
	movq	8(%rax), %rdx
	callq	unify_MatchBindings
	testl	%eax, %eax
	je	.LBB7_53
# BB#48:
	movl	stamp(%rip), %eax
	movl	%eax, multvec_j(,%rbp,4)
	movq	%r14, %rdi
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	8(%rsp), %edx           # 4-byte Reload
	movl	28(%rsp), %ecx          # 4-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	movl	4(%rsp), %r9d           # 4-byte Reload
	movl	104(%rsp), %eax
	pushq	%rax
.Lcfi98:
	.cfi_adjust_cfa_offset 8
	movl	104(%rsp), %eax
	pushq	%rax
.Lcfi99:
	.cfi_adjust_cfa_offset 8
	callq	subs_SubsumesInternBasic
	addq	$16, %rsp
.Lcfi100:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB7_49
# BB#52:
	movl	$0, multvec_j(,%rbp,4)
.LBB7_53:
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jle	.LBB7_56
# BB#54:                                # %.lr.ph.i160.preheader
	incl	%eax
	.p2align	4, 0x90
.LBB7_55:                               # %.lr.ph.i160
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB7_55
.LBB7_56:                               # %._crit_edge.i161
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB7_58
# BB#57:
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	.p2align	4, 0x90
.LBB7_58:                               # %cont_BackTrack.exit162
                                        # =>This Inner Loop Header: Depth=1
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB7_59
.LBB7_60:                               # %._crit_edge
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	$0, multvec_i(,%rax,4)
	xorl	%r12d, %r12d
.LBB7_61:                               # %cont_BackTrack.exit156
	movl	%r12d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_17:
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB7_20
# BB#18:                                # %.lr.ph.i154.preheader
	incl	%eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB7_19:                               # %.lr.ph.i154
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB7_19
.LBB7_20:                               # %._crit_edge.i155
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	movl	$1, %r12d
	je	.LBB7_61
# BB#21:
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB7_61
.LBB7_49:
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB7_20
# BB#50:                                # %.lr.ph.i.preheader
	incl	%eax
	xorps	%xmm0, %xmm0
.LBB7_51:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB7_51
	jmp	.LBB7_20
.Lfunc_end7:
	.size	subs_SubsumesInternBasic, .Lfunc_end7-subs_SubsumesInternBasic
	.cfi_endproc

	.globl	subs_SubsumesWithSignature
	.p2align	4, 0x90
	.type	subs_SubsumesWithSignature,@function
subs_SubsumesWithSignature:             # @subs_SubsumesWithSignature
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi105:
	.cfi_def_cfa_offset 48
.Lcfi106:
	.cfi_offset %rbx, -40
.Lcfi107:
	.cfi_offset %r14, -32
.Lcfi108:
	.cfi_offset %r15, -24
.Lcfi109:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movl	%edx, %r14d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movl	68(%rbx), %eax
	addl	64(%rbx), %eax
	movl	68(%rbp), %ecx
	addl	64(%rbp), %ecx
	cmpl	%ecx, %eax
	jg	.LBB8_10
# BB#1:
	movl	72(%rbx), %esi
	movl	72(%rbp), %edx
	cmpl	%edx, %esi
	jg	.LBB8_10
# BB#2:
	addl	%esi, %eax
	addl	%edx, %ecx
	cmpl	%ecx, %eax
	jg	.LBB8_10
# BB#3:
	movl	stamp(%rip), %eax
	incl	%eax
	movl	%eax, stamp(%rip)
	cmpl	$-1, %eax
	jne	.LBB8_5
# BB#4:                                 # %.loopexit.loopexit
	movl	$1, stamp(%rip)
	movl	$multvec_j, %edi
	xorl	%esi, %esi
	movl	$400, %edx              # imm = 0x190
	callq	memset
	movl	$multvec_i, %edi
	xorl	%esi, %esi
	movl	$400, %edx              # imm = 0x190
	callq	memset
.LBB8_5:                                # %.loopexit
	movl	term_MARK(%rip), %eax
	cmpl	$-1, %eax
	jne	.LBB8_9
# BB#6:                                 # %.preheader.i.preheader
	movq	$-48000, %rax           # imm = 0xFFFF4480
	jmp	.LBB8_7
	.p2align	4, 0x90
.LBB8_12:                               # %.preheader.i.1
                                        #   in Loop: Header=BB8_7 Depth=1
	movq	$0, term_BIND+48016(%rax)
	movq	$0, term_BIND+48032(%rax)
	movq	$0, term_BIND+48048(%rax)
	movq	$0, term_BIND+48064(%rax)
	movq	$0, term_BIND+48080(%rax)
	movq	$0, term_BIND+48096(%rax)
	movq	$0, term_BIND+48112(%rax)
	subq	$-128, %rax
.LBB8_7:                                # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, term_BIND+48000(%rax)
	testq	%rax, %rax
	jne	.LBB8_12
# BB#8:
	movl	$1, term_MARK(%rip)
	movl	$1, %eax
.LBB8_9:                                # %term_NewMark.exit
	incl	%eax
	movl	%eax, term_MARK(%rip)
	xorl	%edi, %edi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	movl	%r14d, %ecx
	movq	%r15, %r8
	callq	subs_SubsumesInternWithSignature
	movl	%eax, %ebx
	movq	(%r15), %rdi
	movl	$symbol_IsVariable, %esi
	callq	list_DeleteElementIf
	movq	%rax, (%r15)
	movl	%ebx, %eax
	jmp	.LBB8_11
.LBB8_10:
	xorl	%eax, %eax
.LBB8_11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	subs_SubsumesWithSignature, .Lfunc_end8-subs_SubsumesWithSignature
	.cfi_endproc

	.p2align	4, 0x90
	.type	subs_SubsumesInternWithSignature,@function
subs_SubsumesInternWithSignature:       # @subs_SubsumesInternWithSignature
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi110:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi111:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi113:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi114:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi116:
	.cfi_def_cfa_offset 112
.Lcfi117:
	.cfi_offset %rbx, -56
.Lcfi118:
	.cfi_offset %r12, -48
.Lcfi119:
	.cfi_offset %r13, -40
.Lcfi120:
	.cfi_offset %r14, -32
.Lcfi121:
	.cfi_offset %r15, -24
.Lcfi122:
	.cfi_offset %rbp, -16
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movq	%rsi, %rbp
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movslq	68(%rdx), %rax
	movslq	72(%rdx), %rcx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movslq	64(%rdx), %r15
	addq	%rax, %r15
	addq	%rcx, %r15
	movq	56(%rbp), %rax
	movslq	%edi, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rbx
	movq	$0, 16(%rsp)
	testl	%r15d, %r15d
	jle	.LBB9_47
# BB#1:                                 # %.lr.ph157
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	leal	1(%rdi), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movl	symbol_TYPESTATBITS(%rip), %r14d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB9_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_9 Depth 2
                                        #     Child Loop BB9_14 Depth 2
                                        #     Child Loop BB9_38 Depth 2
                                        #     Child Loop BB9_43 Depth 2
	movl	multvec_j(,%r13,4), %eax
	cmpl	stamp(%rip), %eax
	je	.LBB9_45
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	24(%rax), %r12
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	%rbx, %rdi
	movq	%r12, %rsi
	leaq	16(%rsp), %rdx
	movl	12(%rsp), %ecx          # 4-byte Reload
	callq	fol_SignatureMatch
	testl	%eax, %eax
	je	.LBB9_7
# BB#4:                                 #   in Loop: Header=BB9_2 Depth=1
	movl	64(%rbp), %eax
	movl	72(%rbp), %ecx
	addl	68(%rbp), %eax
	leal	-1(%rcx,%rax), %eax
	cmpl	40(%rsp), %eax          # 4-byte Folded Reload
	je	.LBB9_48
# BB#5:                                 #   in Loop: Header=BB9_2 Depth=1
	movl	stamp(%rip), %eax
	movl	%eax, multvec_j(,%r13,4)
	movl	28(%rsp), %edi          # 4-byte Reload
	movq	%rbp, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	12(%rsp), %ecx          # 4-byte Reload
	leaq	16(%rsp), %r8
	callq	subs_SubsumesInternWithSignature
	testl	%eax, %eax
	jne	.LBB9_48
# BB#6:                                 #   in Loop: Header=BB9_2 Depth=1
	movl	$0, multvec_j(,%r13,4)
.LBB9_7:                                # %.preheader148
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	16(%rsp), %rax
	testq	%rax, %rax
	je	.LBB9_15
# BB#8:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB9_9:                                # %.lr.ph
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rdx), %rsi
	testl	%esi, %esi
	jle	.LBB9_11
# BB#10:                                #   in Loop: Header=BB9_9 Depth=2
	movslq	%esi, %rcx
	shlq	$4, %rcx
	movq	$0, term_BIND(%rcx)
	jmp	.LBB9_12
	.p2align	4, 0x90
.LBB9_11:                               #   in Loop: Header=BB9_9 Depth=2
	negl	%esi
	movl	%r14d, %ecx
	sarl	%cl, %esi
	movslq	%esi, %rcx
	movl	$0, symbol_CONTEXT(,%rcx,4)
.LBB9_12:                               #   in Loop: Header=BB9_9 Depth=2
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB9_9
# BB#13:                                # %._crit_edge
                                        #   in Loop: Header=BB9_2 Depth=1
	testq	%rax, %rax
	je	.LBB9_15
	.p2align	4, 0x90
.LBB9_14:                               # %.lr.ph.i138
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB9_14
.LBB9_15:                               # %list_Delete.exit139
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	$0, 16(%rsp)
	movl	(%rbx), %ecx
	movl	fol_NOT(%rip), %eax
	cmpl	%eax, %ecx
	movl	%ecx, %esi
	jne	.LBB9_17
# BB#16:                                #   in Loop: Header=BB9_2 Depth=1
	movq	16(%rbx), %rdx
	movq	8(%rdx), %rdx
	movl	(%rdx), %esi
.LBB9_17:                               # %fol_Atom.exit134
                                        #   in Loop: Header=BB9_2 Depth=1
	movl	(%r12), %edx
	cmpl	%eax, %edx
	movl	%edx, %edi
	jne	.LBB9_19
# BB#18:                                #   in Loop: Header=BB9_2 Depth=1
	movq	16(%r12), %rdi
	movq	8(%rdi), %rdi
	movl	(%rdi), %edi
.LBB9_19:                               # %fol_Atom.exit126
                                        #   in Loop: Header=BB9_2 Depth=1
	cmpl	%edi, %esi
	jne	.LBB9_45
# BB#20:                                #   in Loop: Header=BB9_2 Depth=1
	cmpl	%eax, %ecx
	movl	%ecx, %esi
	jne	.LBB9_22
# BB#21:                                #   in Loop: Header=BB9_2 Depth=1
	movq	16(%rbx), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %esi
.LBB9_22:                               # %fol_Atom.exit118
                                        #   in Loop: Header=BB9_2 Depth=1
	cmpl	%esi, fol_EQUALITY(%rip)
	jne	.LBB9_45
# BB#23:                                #   in Loop: Header=BB9_2 Depth=1
	cmpl	%eax, %ecx
	movq	%rbx, %rcx
	jne	.LBB9_25
# BB#24:                                #   in Loop: Header=BB9_2 Depth=1
	movq	16(%rbx), %rcx
	movq	8(%rcx), %rcx
.LBB9_25:                               # %fol_Atom.exit110
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rdi
	cmpl	%eax, %edx
	movq	%r12, %rax
	jne	.LBB9_27
# BB#26:                                #   in Loop: Header=BB9_2 Depth=1
	movq	16(%r12), %rax
	movq	8(%rax), %rax
.LBB9_27:                               # %fol_Atom.exit102
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	leaq	16(%rsp), %rdx
	movl	12(%rsp), %ecx          # 4-byte Reload
	callq	fol_SignatureMatch
	testl	%eax, %eax
	je	.LBB9_36
# BB#28:                                #   in Loop: Header=BB9_2 Depth=1
	movl	fol_NOT(%rip), %eax
	cmpl	%eax, (%rbx)
	movq	%rbx, %rcx
	jne	.LBB9_30
# BB#29:                                #   in Loop: Header=BB9_2 Depth=1
	movq	16(%rbx), %rcx
	movq	8(%rcx), %rcx
.LBB9_30:                               # %fol_Atom.exit94
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rdi
	cmpl	%eax, (%r12)
	jne	.LBB9_32
# BB#31:                                #   in Loop: Header=BB9_2 Depth=1
	movq	16(%r12), %rax
	movq	8(%rax), %r12
.LBB9_32:                               # %fol_Atom.exit
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	16(%r12), %rax
	movq	8(%rax), %rsi
	leaq	16(%rsp), %rdx
	movl	12(%rsp), %ecx          # 4-byte Reload
	callq	fol_SignatureMatch
	testl	%eax, %eax
	je	.LBB9_36
# BB#33:                                #   in Loop: Header=BB9_2 Depth=1
	movl	64(%rbp), %eax
	movl	72(%rbp), %ecx
	addl	68(%rbp), %eax
	leal	-1(%rcx,%rax), %eax
	cmpl	40(%rsp), %eax          # 4-byte Folded Reload
	je	.LBB9_48
# BB#34:                                #   in Loop: Header=BB9_2 Depth=1
	movl	stamp(%rip), %eax
	movl	%eax, multvec_j(,%r13,4)
	movl	28(%rsp), %edi          # 4-byte Reload
	movq	%rbp, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	12(%rsp), %ecx          # 4-byte Reload
	leaq	16(%rsp), %r8
	callq	subs_SubsumesInternWithSignature
	testl	%eax, %eax
	jne	.LBB9_48
# BB#35:                                #   in Loop: Header=BB9_2 Depth=1
	movl	$0, multvec_j(,%r13,4)
.LBB9_36:                               # %.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	16(%rsp), %rax
	testq	%rax, %rax
	je	.LBB9_44
# BB#37:                                # %.lr.ph153.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB9_38:                               # %.lr.ph153
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rdx), %rsi
	testl	%esi, %esi
	jle	.LBB9_40
# BB#39:                                #   in Loop: Header=BB9_38 Depth=2
	movslq	%esi, %rcx
	shlq	$4, %rcx
	movq	$0, term_BIND(%rcx)
	jmp	.LBB9_41
	.p2align	4, 0x90
.LBB9_40:                               #   in Loop: Header=BB9_38 Depth=2
	negl	%esi
	movl	%r14d, %ecx
	sarl	%cl, %esi
	movslq	%esi, %rcx
	movl	$0, symbol_CONTEXT(,%rcx,4)
.LBB9_41:                               #   in Loop: Header=BB9_38 Depth=2
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB9_38
# BB#42:                                # %._crit_edge154
                                        #   in Loop: Header=BB9_2 Depth=1
	testq	%rax, %rax
	je	.LBB9_44
	.p2align	4, 0x90
.LBB9_43:                               # %.lr.ph.i
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB9_43
.LBB9_44:                               # %list_Delete.exit
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	$0, 16(%rsp)
	.p2align	4, 0x90
.LBB9_45:                               #   in Loop: Header=BB9_2 Depth=1
	incq	%r13
	cmpq	%r15, %r13
	jl	.LBB9_2
.LBB9_47:
	xorl	%eax, %eax
	jmp	.LBB9_55
.LBB9_48:                               # %.sink.split
	movq	16(%rsp), %rax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rcx
	testq	%rax, %rax
	je	.LBB9_53
# BB#49:
	testq	%rcx, %rcx
	je	.LBB9_54
# BB#50:                                # %.preheader.i.preheader
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB9_51:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB9_51
# BB#52:
	movq	%rcx, (%rdx)
	jmp	.LBB9_54
.LBB9_53:
	movq	%rcx, %rax
.LBB9_54:                               # %list_Nconc.exit
	movq	%rax, (%rdi)
	movl	$1, %eax
.LBB9_55:                               # %.loopexit
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	subs_SubsumesInternWithSignature, .Lfunc_end9-subs_SubsumesInternWithSignature
	.cfi_endproc

	.p2align	4, 0x90
	.type	symbol_IsVariable,@function
symbol_IsVariable:                      # @symbol_IsVariable
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testl	%edi, %edi
	setg	%al
	retq
.Lfunc_end10:
	.size	symbol_IsVariable, .Lfunc_end10-symbol_IsVariable
	.cfi_endproc

	.globl	subs_Subsumes
	.p2align	4, 0x90
	.type	subs_Subsumes,@function
subs_Subsumes:                          # @subs_Subsumes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi123:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi124:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi125:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi126:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi127:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi128:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi129:
	.cfi_def_cfa_offset 64
.Lcfi130:
	.cfi_offset %rbx, -56
.Lcfi131:
	.cfi_offset %r12, -48
.Lcfi132:
	.cfi_offset %r13, -40
.Lcfi133:
	.cfi_offset %r14, -32
.Lcfi134:
	.cfi_offset %r15, -24
.Lcfi135:
	.cfi_offset %rbp, -16
	movl	%edx, %r8d
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movl	68(%rbp), %r14d
	addl	64(%rbp), %r14d
	movl	72(%rbp), %r15d
	addl	%r14d, %r15d
	movl	68(%rbx), %r12d
	addl	64(%rbx), %r12d
	movl	72(%rbx), %r13d
	addl	%r12d, %r13d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	movl	$0, %esi
	js	.LBB11_2
# BB#1:
	movq	56(%rbp), %rsi
	movslq	%r8d, %rdx
	movq	(%rsi,%rdx,8), %rdx
	movl	4(%rdx), %esi
.LBB11_2:
	testl	%ecx, %ecx
	js	.LBB11_4
# BB#3:
	movq	56(%rbx), %rax
	movslq	%ecx, %rdx
	movq	(%rax,%rdx,8), %rax
	movl	4(%rax), %eax
.LBB11_4:
	cmpl	%r13d, %r15d
	jg	.LBB11_15
# BB#5:
	movl	4(%rbp), %edx
	subl	%esi, %edx
	movl	4(%rbx), %esi
	subl	%eax, %esi
	cmpl	%esi, %edx
	jbe	.LBB11_6
.LBB11_15:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_6:
	movl	stamp(%rip), %eax
	incl	%eax
	movl	%eax, stamp(%rip)
	cmpl	$-1, %eax
	jne	.LBB11_8
# BB#7:                                 # %.loopexit.loopexit
	movl	$1, stamp(%rip)
	movl	$multvec_j, %edi
	xorl	%esi, %esi
	movl	$400, %edx              # imm = 0x190
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movl	%r8d, (%rsp)            # 4-byte Spill
	callq	memset
	movl	$multvec_i, %edi
	xorl	%esi, %esi
	movl	$400, %edx              # imm = 0x190
	callq	memset
	movl	(%rsp), %r8d            # 4-byte Reload
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	$1, %eax
.LBB11_8:                               # %.loopexit
	testl	%r8d, %r8d
	js	.LBB11_10
# BB#9:
	movslq	%r8d, %rdx
	movl	%eax, multvec_i(,%rdx,4)
.LBB11_10:
	testl	%ecx, %ecx
	js	.LBB11_12
# BB#11:
	movslq	%ecx, %rcx
	movl	%eax, multvec_j(,%rcx,4)
.LBB11_12:
	cmpl	$2, %r15d
	jl	.LBB11_16
# BB#13:
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	movq	%rbp, %rdi
	movl	%r14d, %edx
	movq	%rbx, %rcx
	movl	%r12d, %r9d
	callq	subs_PartnerTest
	testl	%eax, %eax
	je	.LBB11_15
# BB#14:
	movq	%rbp, %rdi
	movl	%r14d, %esi
	movl	%r15d, %edx
	movq	%rbx, %rcx
	movl	%r12d, %r8d
	movl	%r13d, %r9d
	callq	subs_PartnerTest
	testl	%eax, %eax
	je	.LBB11_15
.LBB11_16:
	movq	%rbp, %rdi
	movl	%r14d, %esi
	movl	%r15d, %edx
	movq	%rbx, %rcx
	movl	%r12d, %r8d
	movl	%r13d, %r9d
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	subs_SubsumesIntern     # TAILCALL
.Lfunc_end11:
	.size	subs_Subsumes, .Lfunc_end11-subs_Subsumes
	.cfi_endproc

	.p2align	4, 0x90
	.type	subs_SubsumesIntern,@function
subs_SubsumesIntern:                    # @subs_SubsumesIntern
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi136:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi137:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi138:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi139:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi140:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi141:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi142:
	.cfi_def_cfa_offset 96
.Lcfi143:
	.cfi_offset %rbx, -56
.Lcfi144:
	.cfi_offset %r12, -48
.Lcfi145:
	.cfi_offset %r13, -40
.Lcfi146:
	.cfi_offset %r14, -32
.Lcfi147:
	.cfi_offset %r15, -24
.Lcfi148:
	.cfi_offset %rbp, -16
	movl	%r9d, 12(%rsp)          # 4-byte Spill
	movl	%r8d, 8(%rsp)           # 4-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%edx, %r13d
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movq	%rdi, %r14
	testl	%r13d, %r13d
	jle	.LBB12_50
# BB#1:                                 # %.lr.ph167
	movl	%r13d, %r15d
	xorl	%ebp, %ebp
	movl	$-1, %ebx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB12_2:                               # =>This Inner Loop Header: Depth=1
	movl	multvec_i(,%rbp,4), %eax
	cmpl	stamp(%rip), %eax
	je	.LBB12_8
# BB#3:                                 #   in Loop: Header=BB12_2 Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB12_5
# BB#4:                                 #   in Loop: Header=BB12_2 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB12_5:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB12_2 Depth=1
	callq	term_NumberOfVarOccs
	testl	%ebx, %ebx
	js	.LBB12_7
# BB#6:                                 #   in Loop: Header=BB12_2 Depth=1
	cmpl	%r12d, %eax
	cmoval	%eax, %r12d
	cmoval	%ebp, %ebx
	jmp	.LBB12_8
.LBB12_7:                               # %clause_GetLiteralAtom.exit._crit_edge
                                        #   in Loop: Header=BB12_2 Depth=1
	movl	%eax, %r12d
	movl	%ebp, %ebx
	.p2align	4, 0x90
.LBB12_8:                               #   in Loop: Header=BB12_2 Depth=1
	incq	%rbp
	cmpq	%rbp, %r15
	jne	.LBB12_2
# BB#9:                                 # %._crit_edge168
	testl	%ebx, %ebx
	js	.LBB12_50
# BB#10:
	movl	%r13d, 28(%rsp)         # 4-byte Spill
	movq	56(%r14), %rax
	movslq	%ebx, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rbp
	movl	stamp(%rip), %eax
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	%eax, multvec_i(,%rcx,4)
	xorl	%esi, %esi
	cmpl	4(%rsp), %ebx           # 4-byte Folded Reload
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	8(%rsp), %edx           # 4-byte Reload
	cmovll	%edx, %ecx
	cmovll	%esi, %edx
	cmpl	%ecx, %edx
	jge	.LBB12_49
# BB#11:                                # %.lr.ph
	movslq	%edx, %r13
	movslq	%ecx, %rbx
	cmpl	%eax, multvec_j(,%r13,4)
	je	.LBB12_48
	.p2align	4, 0x90
.LBB12_13:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	24(%rax), %r12
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%rbp, %rsi
	movq	%r12, %rdx
	callq	unify_MatchBindings
	testl	%eax, %eax
	je	.LBB12_16
# BB#14:
	movl	stamp(%rip), %eax
	movl	%eax, multvec_j(,%r13,4)
	movq	%r14, %rdi
	movl	4(%rsp), %esi           # 4-byte Reload
	movl	%r15d, %edx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	8(%rsp), %r8d           # 4-byte Reload
	movl	12(%rsp), %r9d          # 4-byte Reload
	callq	subs_SubsumesIntern
	testl	%eax, %eax
	jne	.LBB12_52
# BB#15:
	movl	$0, multvec_j(,%r13,4)
.LBB12_16:
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	xorps	%xmm0, %xmm0
	jle	.LBB12_18
	.p2align	4, 0x90
.LBB12_17:                              # %.lr.ph.i139
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB12_17
	jmp	.LBB12_19
	.p2align	4, 0x90
.LBB12_18:
	movl	%ecx, %eax
.LBB12_19:                              # %._crit_edge.i140
	movslq	cont_STACKPOINTER(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB12_21
# BB#20:
	leaq	-1(%rdx), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdx,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB12_22
	.p2align	4, 0x90
.LBB12_21:
	xorl	%ecx, %ecx
.LBB12_22:                              # %cont_BackTrack.exit141
	movl	(%rbp), %esi
	cmpl	(%r12), %esi
	jne	.LBB12_48
# BB#23:
	movl	fol_NOT(%rip), %edx
	cmpl	%edx, %esi
	movl	%esi, %edi
	jne	.LBB12_25
# BB#24:
	movq	16(%rbp), %rdi
	movq	8(%rdi), %rdi
	movl	(%rdi), %edi
.LBB12_25:                              # %fol_Atom.exit135
	cmpl	%edi, fol_EQUALITY(%rip)
	jne	.LBB12_48
# BB#26:
	cmpl	%edx, %esi
	jne	.LBB12_28
# BB#27:
	movq	16(%r12), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %esi
.LBB12_28:                              # %fol_Atom.exit127
	cmpl	%esi, %edi
	jne	.LBB12_48
# BB#29:
	movq	56(%r14), %rsi
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rsi,%rdi,8), %rsi
	cmpl	$0, 8(%rsi)
	je	.LBB12_31
# BB#30:
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsi), %rsi
	movq	(%rsi,%r13,8), %rsi
	cmpl	$0, 8(%rsi)
	jne	.LBB12_48
.LBB12_31:
	leal	1(%rcx), %esi
	movl	%esi, cont_STACKPOINTER(%rip)
	movslq	%ecx, %rcx
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	cmpl	%edx, (%rbp)
	movq	%rbp, %rax
	jne	.LBB12_33
# BB#32:
	movq	16(%rbp), %rax
	movq	8(%rax), %rax
.LBB12_33:                              # %fol_Atom.exit119
	movq	16(%rax), %rax
	movq	8(%rax), %rsi
	cmpl	%edx, (%r12)
	movq	%r12, %rax
	jne	.LBB12_35
# BB#34:
	movq	16(%r12), %rax
	movq	8(%rax), %rax
.LBB12_35:                              # %fol_Atom.exit111
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	callq	unify_MatchBindings
	testl	%eax, %eax
	je	.LBB12_43
# BB#36:
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	%eax, (%rbp)
	movq	%rbp, %rcx
	jne	.LBB12_38
# BB#37:
	movq	16(%rbp), %rcx
	movq	8(%rcx), %rcx
.LBB12_38:                              # %fol_Atom.exit103
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rsi
	cmpl	%eax, (%r12)
	jne	.LBB12_40
# BB#39:
	movq	16(%r12), %rax
	movq	8(%rax), %r12
.LBB12_40:                              # %fol_Atom.exit
	movq	16(%r12), %rax
	movq	8(%rax), %rdx
	callq	unify_MatchBindings
	testl	%eax, %eax
	je	.LBB12_43
# BB#41:
	movl	stamp(%rip), %eax
	movl	%eax, multvec_j(,%r13,4)
	movq	%r14, %rdi
	movl	4(%rsp), %esi           # 4-byte Reload
	movl	28(%rsp), %edx          # 4-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	8(%rsp), %r8d           # 4-byte Reload
	movl	12(%rsp), %r9d          # 4-byte Reload
	callq	subs_SubsumesIntern
	testl	%eax, %eax
	jne	.LBB12_57
# BB#42:
	movl	$0, multvec_j(,%r13,4)
.LBB12_43:
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jle	.LBB12_46
# BB#44:                                # %.lr.ph.i151.preheader
	incl	%eax
	.p2align	4, 0x90
.LBB12_45:                              # %.lr.ph.i151
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB12_45
.LBB12_46:                              # %._crit_edge.i152
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB12_48
# BB#47:
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB12_48
	.p2align	4, 0x90
.LBB12_12:                              # %cont_BackTrack.exit153._crit_edge
                                        #   in Loop: Header=BB12_48 Depth=1
	movl	stamp(%rip), %eax
	cmpl	%eax, multvec_j(,%r13,4)
	jne	.LBB12_13
.LBB12_48:                              # %cont_BackTrack.exit153
                                        # =>This Inner Loop Header: Depth=1
	incq	%r13
	cmpq	%rbx, %r13
	jl	.LBB12_12
.LBB12_49:                              # %._crit_edge
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	$0, multvec_i(,%rax,4)
	xorl	%eax, %eax
	jmp	.LBB12_51
.LBB12_52:
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB12_55
# BB#53:                                # %.lr.ph.i145.preheader
	incl	%eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB12_54:                              # %.lr.ph.i145
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB12_54
.LBB12_55:                              # %._crit_edge.i146
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB12_50
# BB#56:
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB12_50:
	movl	$1, %eax
.LBB12_51:                              # %cont_BackTrack.exit147
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_57:
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB12_55
# BB#58:                                # %.lr.ph.i.preheader
	incl	%eax
	xorps	%xmm0, %xmm0
.LBB12_59:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB12_59
	jmp	.LBB12_55
.Lfunc_end12:
	.size	subs_SubsumesIntern, .Lfunc_end12-subs_SubsumesIntern
	.cfi_endproc

	.globl	subs_ST
	.p2align	4, 0x90
	.type	subs_ST,@function
subs_ST:                                # @subs_ST
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi149:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi150:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi151:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi152:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi153:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi154:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi155:
	.cfi_def_cfa_offset 64
.Lcfi156:
	.cfi_offset %rbx, -56
.Lcfi157:
	.cfi_offset %r12, -48
.Lcfi158:
	.cfi_offset %r13, -40
.Lcfi159:
	.cfi_offset %r14, -32
.Lcfi160:
	.cfi_offset %r15, -24
.Lcfi161:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r14
	movl	%esi, %ebx
	movl	%edi, %r12d
	leal	1(%r12), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movslq	%r12d, %r13
	movl	cont_BINDINGS(%rip), %eax
	movl	cont_STACKPOINTER(%rip), %ecx
	.p2align	4, 0x90
.LBB13_1:                               # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_3 Depth 2
                                        #       Child Loop BB13_7 Depth 3
                                        #     Child Loop BB13_19 Depth 2
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movslq	%ecx, %rcx
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movl	64(%r15), %ecx
	movl	68(%r15), %eax
	movl	72(%r15), %edx
	leal	(%rax,%rcx), %esi
	addl	%edx, %esi
	cmpl	%esi, %ebx
	jge	.LBB13_9
# BB#2:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB13_1 Depth=1
	movslq	%ebx, %rbx
	.p2align	4, 0x90
.LBB13_3:                               # %.lr.ph
                                        #   Parent Loop BB13_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB13_7 Depth 3
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	56(%r14), %rax
	movq	(%rax,%r13,8), %rax
	movq	24(%rax), %rsi
	movq	56(%r15), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdx
	callq	unify_Match
	testl	%eax, %eax
	jne	.LBB13_4
# BB#5:                                 #   in Loop: Header=BB13_3 Depth=2
	incq	%rbx
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jle	.LBB13_8
# BB#6:                                 # %.lr.ph.i42.preheader
                                        #   in Loop: Header=BB13_3 Depth=2
	incl	%eax
	.p2align	4, 0x90
.LBB13_7:                               # %.lr.ph.i42
                                        #   Parent Loop BB13_1 Depth=1
                                        #     Parent Loop BB13_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB13_7
.LBB13_8:                               # %cont_BackTrackAndStart.exit.backedge
                                        #   in Loop: Header=BB13_3 Depth=2
	movl	64(%r15), %ecx
	movl	68(%r15), %eax
	movl	72(%r15), %edx
	leal	(%rax,%rcx), %esi
	addl	%edx, %esi
	movslq	%esi, %rsi
	cmpq	%rsi, %rbx
	jl	.LBB13_3
	jmp	.LBB13_9
	.p2align	4, 0x90
.LBB13_4:                               # %.lr.ph..critedge.loopexit_crit_edge
                                        #   in Loop: Header=BB13_1 Depth=1
	movl	64(%r15), %ecx
	movl	68(%r15), %eax
	movl	72(%r15), %edx
.LBB13_9:                               # %.critedge
                                        #   in Loop: Header=BB13_1 Depth=1
	addl	%ecx, %eax
	addl	%edx, %eax
	cmpl	%eax, %ebx
	jge	.LBB13_10
# BB#15:                                #   in Loop: Header=BB13_1 Depth=1
	movl	64(%r14), %eax
	movl	72(%r14), %ecx
	addl	68(%r14), %eax
	leal	-1(%rcx,%rax), %eax
	movl	$1, %ebp
	cmpl	%r12d, %eax
	je	.LBB13_23
# BB#16:                                #   in Loop: Header=BB13_1 Depth=1
	xorl	%esi, %esi
	movl	4(%rsp), %edi           # 4-byte Reload
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	subs_ST
	xorps	%xmm0, %xmm0
	testl	%eax, %eax
	jne	.LBB13_23
# BB#17:                                #   in Loop: Header=BB13_1 Depth=1
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB13_18
	.p2align	4, 0x90
.LBB13_19:                              # %.lr.ph.i
                                        #   Parent Loop BB13_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB13_19
	jmp	.LBB13_20
	.p2align	4, 0x90
.LBB13_18:                              #   in Loop: Header=BB13_1 Depth=1
	movl	%ecx, %eax
.LBB13_20:                              # %._crit_edge.i
                                        #   in Loop: Header=BB13_1 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rdx
	xorl	%ebp, %ebp
	testq	%rdx, %rdx
	movl	$0, %ecx
	je	.LBB13_22
# BB#21:                                #   in Loop: Header=BB13_1 Depth=1
	leaq	-1(%rdx), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdx,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB13_22:                              # %cont_BackTrack.exit
                                        #   in Loop: Header=BB13_1 Depth=1
	incl	%ebx
	movl	68(%r15), %edx
	addl	64(%r15), %edx
	addl	72(%r15), %edx
	cmpl	%edx, %ebx
	jne	.LBB13_1
	jmp	.LBB13_23
.LBB13_10:
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB13_13
# BB#11:                                # %.lr.ph.i36.preheader
	incl	%eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB13_12:                              # %.lr.ph.i36
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB13_12
.LBB13_13:                              # %._crit_edge.i37
	movslq	cont_STACKPOINTER(%rip), %rax
	xorl	%ebp, %ebp
	testq	%rax, %rax
	je	.LBB13_23
# BB#14:
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB13_23:                              # %cont_BackTrack.exit38
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	subs_ST, .Lfunc_end13-subs_ST
	.cfi_endproc

	.globl	subs_Testlits
	.p2align	4, 0x90
	.type	subs_Testlits,@function
subs_Testlits:                          # @subs_Testlits
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi162:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi163:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi164:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi165:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi166:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi167:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi168:
	.cfi_def_cfa_offset 64
.Lcfi169:
	.cfi_offset %rbx, -56
.Lcfi170:
	.cfi_offset %r12, -48
.Lcfi171:
	.cfi_offset %r13, -40
.Lcfi172:
	.cfi_offset %r14, -32
.Lcfi173:
	.cfi_offset %r15, -24
.Lcfi174:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movl	68(%r15), %eax
	addl	64(%r15), %eax
	movl	$1, %r14d
	addl	72(%r15), %eax
	jle	.LBB14_14
# BB#1:                                 # %.lr.ph
	movl	cont_BINDINGS(%rip), %ecx
	movl	cont_STACKPOINTER(%rip), %esi
	xorl	%r12d, %r12d
.LBB14_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_3 Depth 2
                                        #       Child Loop BB14_6 Depth 3
	movq	56(%r15), %rax
	movq	(%rax,%r12,8), %rax
	movq	24(%rax), %r13
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB14_3:                               #   Parent Loop BB14_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB14_6 Depth 3
	movq	%rdx, %rbp
	leal	1(%rsi), %eax
	movl	%eax, cont_STACKPOINTER(%rip)
	movslq	%esi, %rax
	movl	%ecx, cont_STACK(,%rax,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	56(%rbx), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdx
	movq	%r13, %rsi
	callq	unify_Match
	leaq	1(%rbp), %rdx
	testl	%eax, %eax
	cmovel	%edx, %ebp
	movl	cont_BINDINGS(%rip), %esi
	testl	%esi, %esi
	jle	.LBB14_4
# BB#5:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB14_3 Depth=2
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB14_6:                               # %.lr.ph.i
                                        #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdi
	movq	%rdi, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-1(%rsi), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	cmpl	$1, %esi
	movl	%ecx, %esi
	jg	.LBB14_6
	jmp	.LBB14_7
	.p2align	4, 0x90
.LBB14_4:                               #   in Loop: Header=BB14_3 Depth=2
	movl	%esi, %ecx
.LBB14_7:                               # %._crit_edge.i
                                        #   in Loop: Header=BB14_3 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB14_8
# BB#9:                                 #   in Loop: Header=BB14_3 Depth=2
	leaq	-1(%rdi), %rsi
	movl	%esi, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdi,4), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	testl	%eax, %eax
	je	.LBB14_11
	jmp	.LBB14_13
	.p2align	4, 0x90
.LBB14_8:                               #   in Loop: Header=BB14_3 Depth=2
	xorl	%esi, %esi
	testl	%eax, %eax
	jne	.LBB14_13
.LBB14_11:                              #   in Loop: Header=BB14_3 Depth=2
	movl	68(%rbx), %eax
	addl	64(%rbx), %eax
	addl	72(%rbx), %eax
	cmpl	%eax, %ebp
	jl	.LBB14_3
	jmp	.LBB14_12
	.p2align	4, 0x90
.LBB14_13:                              # %.critedge22
                                        #   in Loop: Header=BB14_2 Depth=1
	incq	%r12
	movslq	68(%r15), %rax
	movslq	72(%r15), %rdx
	movslq	64(%r15), %rdi
	addq	%rax, %rdi
	addq	%rdx, %rdi
	cmpq	%rdi, %r12
	jl	.LBB14_2
	jmp	.LBB14_14
.LBB14_12:
	xorl	%r14d, %r14d
.LBB14_14:                              # %.critedge
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	subs_Testlits, .Lfunc_end14-subs_Testlits
	.cfi_endproc

	.globl	subs_IdcTestlits
	.p2align	4, 0x90
	.type	subs_IdcTestlits,@function
subs_IdcTestlits:                       # @subs_IdcTestlits
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi175:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi176:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi177:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi178:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi179:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi180:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi181:
	.cfi_def_cfa_offset 64
.Lcfi182:
	.cfi_offset %rbx, -56
.Lcfi183:
	.cfi_offset %r12, -48
.Lcfi184:
	.cfi_offset %r13, -40
.Lcfi185:
	.cfi_offset %r14, -32
.Lcfi186:
	.cfi_offset %r15, -24
.Lcfi187:
	.cfi_offset %rbp, -16
	movq	%rdx, %r13
	movq	%rsi, %r12
	movq	%rdi, %r15
	movl	64(%r15), %edi
	movl	68(%r15), %esi
	movl	72(%r15), %ebp
	leal	(%rsi,%rdi), %eax
	addl	%ebp, %eax
	jle	.LBB15_9
# BB#1:                                 # %.preheader.lr.ph.i
	movl	64(%r12), %eax
	movl	68(%r12), %ecx
	movl	72(%r12), %edx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB15_2:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_4 Depth 2
                                        #       Child Loop BB15_11 Depth 3
	leal	(%rcx,%rdx), %ebx
	addl	%eax, %ebx
	testl	%ebx, %ebx
	movl	$0, %ebp
	jle	.LBB15_16
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB15_2 Depth=1
	movl	cont_BINDINGS(%rip), %esi
	xorl	%ebp, %ebp
	movl	cont_STACKPOINTER(%rip), %edi
	.p2align	4, 0x90
.LBB15_4:                               #   Parent Loop BB15_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB15_11 Depth 3
	leal	1(%rdi), %eax
	movl	%eax, cont_STACKPOINTER(%rip)
	movslq	%edi, %rax
	movl	%esi, cont_STACK(,%rax,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	56(%r15), %rax
	movq	(%rax,%r14,8), %rax
	movq	24(%rax), %rsi
	movq	56(%r12), %rax
	movslq	%ebp, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rdx
	callq	unify_Match
	testl	%eax, %eax
	je	.LBB15_6
# BB#5:                                 #   in Loop: Header=BB15_4 Depth=2
	movl	64(%r12), %eax
	movl	72(%r12), %ecx
	addl	68(%r12), %eax
	leal	1(%rcx,%rax), %ebp
.LBB15_6:                               #   in Loop: Header=BB15_4 Depth=2
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jle	.LBB15_7
	.p2align	4, 0x90
.LBB15_11:                              # %.lr.ph.i.i
                                        #   Parent Loop BB15_2 Depth=1
                                        #     Parent Loop BB15_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-1(%rax), %esi
	movl	%esi, cont_BINDINGS(%rip)
	cmpl	$1, %eax
	movl	%esi, %eax
	jg	.LBB15_11
	jmp	.LBB15_12
	.p2align	4, 0x90
.LBB15_7:                               #   in Loop: Header=BB15_4 Depth=2
	movl	%eax, %esi
.LBB15_12:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB15_4 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB15_13
# BB#14:                                #   in Loop: Header=BB15_4 Depth=2
	leaq	-1(%rax), %rdi
	movl	%edi, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %esi
	movl	%esi, cont_BINDINGS(%rip)
	jmp	.LBB15_15
	.p2align	4, 0x90
.LBB15_13:                              #   in Loop: Header=BB15_4 Depth=2
	xorl	%edi, %edi
.LBB15_15:                              # %cont_BackTrack.exit.i
                                        #   in Loop: Header=BB15_4 Depth=2
	incl	%ebp
	movl	64(%r12), %eax
	movl	68(%r12), %ecx
	movl	72(%r12), %edx
	leal	(%rcx,%rax), %ebx
	addl	%edx, %ebx
	cmpl	%ebx, %ebp
	jl	.LBB15_4
.LBB15_16:                              # %._crit_edge.i
                                        #   in Loop: Header=BB15_2 Depth=1
	cmpl	%ebx, %ebp
	je	.LBB15_17
# BB#8:                                 #   in Loop: Header=BB15_2 Depth=1
	incq	%r14
	movl	64(%r15), %edi
	movl	68(%r15), %esi
	movl	72(%r15), %ebp
	leal	(%rsi,%rdi), %ebx
	addl	%ebp, %ebx
	movslq	%ebx, %rbx
	cmpq	%rbx, %r14
	jl	.LBB15_2
.LBB15_9:                               # %.preheader
	addl	%edi, %esi
	addl	%ebp, %esi
	jle	.LBB15_10
# BB#18:                                # %.lr.ph
	movq	%r13, (%rsp)            # 8-byte Spill
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB15_19:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_21 Depth 2
                                        #     Child Loop BB15_26 Depth 2
	movq	56(%r15), %rax
	movq	(%rax,%r14,8), %rax
	movq	24(%rax), %rdi
	callq	term_VariableSymbols
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB15_30
# BB#20:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB15_19 Depth=1
	movq	cont_LEFTCONTEXT(%rip), %rcx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB15_21:                              # %.lr.ph.i40
                                        #   Parent Loop BB15_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rsi), %rdx
	movslq	%edx, %rbp
	shlq	$5, %rbp
	cmpq	$0, 8(%rcx,%rbp)
	je	.LBB15_23
# BB#22:                                #   in Loop: Header=BB15_21 Depth=2
	movq	%rdi, %rdx
	jmp	.LBB15_24
	.p2align	4, 0x90
.LBB15_23:                              #   in Loop: Header=BB15_21 Depth=2
	movq	%rdx, 8(%rdi)
	movq	(%rdi), %rdx
	movq	%rdi, %rax
.LBB15_24:                              #   in Loop: Header=BB15_21 Depth=2
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	movq	%rdx, %rdi
	jne	.LBB15_21
# BB#25:                                # %._crit_edge.i42
                                        #   in Loop: Header=BB15_19 Depth=1
	testq	%rdx, %rdx
	je	.LBB15_29
	.p2align	4, 0x90
.LBB15_26:                              # %.lr.ph.i.i43
                                        #   Parent Loop BB15_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rcx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rdx, (%rsi)
	testq	%rcx, %rcx
	movq	%rcx, %rdx
	jne	.LBB15_26
# BB#27:                                # %list_Delete.exit.i
                                        #   in Loop: Header=BB15_19 Depth=1
	testq	%rax, %rax
	je	.LBB15_30
# BB#28:                                #   in Loop: Header=BB15_19 Depth=1
	movq	$0, (%rax)
.LBB15_29:                              # %subs_GetVariables.exit
                                        #   in Loop: Header=BB15_19 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r14, 8(%rbp)
	movq	%r12, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	movq	%rbp, %r12
	movq	%rax, %r13
.LBB15_30:                              # %subs_GetVariables.exit.thread
                                        #   in Loop: Header=BB15_19 Depth=1
	incq	%r14
	movslq	68(%r15), %rax
	movslq	72(%r15), %rcx
	movslq	64(%r15), %rdx
	addq	%rax, %rdx
	addq	%rcx, %rdx
	cmpq	%rdx, %r14
	jl	.LBB15_19
# BB#31:                                # %._crit_edge
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	litptr_Create
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rax, (%rcx)
	testq	%r13, %r13
	je	.LBB15_33
	.p2align	4, 0x90
.LBB15_32:                              # %.lr.ph.i36
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB15_32
.LBB15_33:                              # %list_Delete.exit38
	movl	$1, %eax
	testq	%r12, %r12
	je	.LBB15_35
	.p2align	4, 0x90
.LBB15_34:                              # %.lr.ph.i31
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r12)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%r12, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %r12
	jne	.LBB15_34
	jmp	.LBB15_35
.LBB15_17:
	xorl	%eax, %eax
	jmp	.LBB15_35
.LBB15_10:                              # %list_Delete.exit38.thread
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	litptr_Create
	movq	%rax, (%r13)
	movl	$1, %eax
.LBB15_35:                              # %subs_SubsumptionPossible.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	subs_IdcTestlits, .Lfunc_end15-subs_IdcTestlits
	.cfi_endproc

	.globl	subs_Idc
	.p2align	4, 0x90
	.type	subs_Idc,@function
subs_Idc:                               # @subs_Idc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi188:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi189:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi190:
	.cfi_def_cfa_offset 32
.Lcfi191:
	.cfi_offset %rbx, -24
.Lcfi192:
	.cfi_offset %rbp, -16
	movq	%rsi, %r8
	movl	vec_MAX(%rip), %ebp
	movl	68(%rdi), %ecx
	addl	64(%rdi), %ecx
	addl	72(%rdi), %ecx
	jle	.LBB16_3
# BB#1:                                 # %.lr.ph.preheader
	movslq	%ebp, %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB16_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, vec_VECTOR(,%rdx,8)
	incq	%rdx
	movl	%edx, vec_MAX(%rip)
	incq	%rcx
	movslq	68(%rdi), %rsi
	movslq	72(%rdi), %rax
	movslq	64(%rdi), %rbx
	addq	%rsi, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rcx
	jl	.LBB16_2
.LBB16_3:                               # %._crit_edge
	movl	%ebp, %esi
	movq	%r8, %rdx
	callq	subs_InternIdc
	movl	%ebp, vec_MAX(%rip)
	movq	cont_LASTBINDING(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB16_6
# BB#4:                                 # %.lr.ph.preheader.i
	movl	cont_BINDINGS(%rip), %edx
	decl	%edx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB16_5:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rsi
	movq	%rsi, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	movl	%edx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rcx
	decl	%edx
	testq	%rcx, %rcx
	jne	.LBB16_5
.LBB16_6:                               # %cont_Reset.exit
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end16:
	.size	subs_Idc, .Lfunc_end16-subs_Idc
	.cfi_endproc

	.p2align	4, 0x90
	.type	subs_InternIdc,@function
subs_InternIdc:                         # @subs_InternIdc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi193:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi194:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi195:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi196:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi197:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi198:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi199:
	.cfi_def_cfa_offset 80
.Lcfi200:
	.cfi_offset %rbx, -56
.Lcfi201:
	.cfi_offset %r12, -48
.Lcfi202:
	.cfi_offset %r13, -40
.Lcfi203:
	.cfi_offset %r14, -32
.Lcfi204:
	.cfi_offset %r15, -24
.Lcfi205:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movl	%esi, %r12d
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	vec_MAX(%rip), %esi
	cmpl	%r12d, %esi
	jle	.LBB17_9
# BB#1:                                 # %.preheader.lr.ph.i.i
	movl	64(%r15), %eax
	movl	68(%r15), %ecx
	movl	72(%r15), %edx
	movl	%r12d, %r14d
	.p2align	4, 0x90
.LBB17_2:                               # %.preheader.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_4 Depth 2
                                        #       Child Loop BB17_11 Depth 3
	leal	(%rcx,%rdx), %ebx
	addl	%eax, %ebx
	movl	$0, (%rsp)              # 4-byte Folded Spill
	testl	%ebx, %ebx
	movl	$0, %ebp
	jle	.LBB17_16
# BB#3:                                 # %.lr.ph.i.i
                                        #   in Loop: Header=BB17_2 Depth=1
	movl	%r14d, %r13d
	movl	cont_BINDINGS(%rip), %esi
	xorl	%ebp, %ebp
	movl	cont_STACKPOINTER(%rip), %edi
	.p2align	4, 0x90
.LBB17_4:                               #   Parent Loop BB17_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_11 Depth 3
	leal	1(%rdi), %eax
	movl	%eax, cont_STACKPOINTER(%rip)
	movslq	%edi, %rax
	movl	%esi, cont_STACK(,%rax,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movslq	vec_VECTOR(,%r13,8), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	56(%rcx), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rsi
	movq	56(%r15), %rax
	movslq	%ebp, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rdx
	callq	unify_Match
	testl	%eax, %eax
	je	.LBB17_6
# BB#5:                                 #   in Loop: Header=BB17_4 Depth=2
	movl	64(%r15), %eax
	movl	72(%r15), %ecx
	addl	68(%r15), %eax
	leal	1(%rcx,%rax), %ebp
.LBB17_6:                               #   in Loop: Header=BB17_4 Depth=2
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jle	.LBB17_7
	.p2align	4, 0x90
.LBB17_11:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB17_2 Depth=1
                                        #     Parent Loop BB17_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-1(%rax), %esi
	movl	%esi, cont_BINDINGS(%rip)
	cmpl	$1, %eax
	movl	%esi, %eax
	jg	.LBB17_11
	jmp	.LBB17_12
	.p2align	4, 0x90
.LBB17_7:                               #   in Loop: Header=BB17_4 Depth=2
	movl	%eax, %esi
.LBB17_12:                              # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB17_4 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB17_13
# BB#14:                                #   in Loop: Header=BB17_4 Depth=2
	leaq	-1(%rax), %rdi
	movl	%edi, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %esi
	movl	%esi, cont_BINDINGS(%rip)
	jmp	.LBB17_15
	.p2align	4, 0x90
.LBB17_13:                              #   in Loop: Header=BB17_4 Depth=2
	xorl	%edi, %edi
.LBB17_15:                              # %cont_BackTrack.exit.i.i
                                        #   in Loop: Header=BB17_4 Depth=2
	incl	%ebp
	movl	64(%r15), %eax
	movl	68(%r15), %ecx
	movl	72(%r15), %edx
	leal	(%rcx,%rax), %ebx
	addl	%edx, %ebx
	cmpl	%ebx, %ebp
	jl	.LBB17_4
.LBB17_16:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB17_2 Depth=1
	cmpl	%ebx, %ebp
	je	.LBB17_67
# BB#8:                                 #   in Loop: Header=BB17_2 Depth=1
	incl	%r14d
	movl	vec_MAX(%rip), %esi
	cmpl	%esi, %r14d
	jl	.LBB17_2
.LBB17_9:                               # %.preheader.i
	cmpl	%r12d, %esi
	jle	.LBB17_10
# BB#17:                                # %.lr.ph.i
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	56(%rax), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB17_18:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_20 Depth 2
                                        #     Child Loop BB17_25 Depth 2
	movl	%r12d, %r12d
	movslq	vec_VECTOR(,%r12,8), %rax
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rcx), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rdi
	callq	term_VariableSymbols
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB17_29
# BB#19:                                # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB17_18 Depth=1
	movq	cont_LEFTCONTEXT(%rip), %rcx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbp, %rsi
	.p2align	4, 0x90
.LBB17_20:                              # %.lr.ph.i39.i
                                        #   Parent Loop BB17_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rsi), %rdx
	movslq	%edx, %rbx
	shlq	$5, %rbx
	cmpq	$0, 8(%rcx,%rbx)
	je	.LBB17_22
# BB#21:                                #   in Loop: Header=BB17_20 Depth=2
	movq	%rdi, %rdx
	jmp	.LBB17_23
	.p2align	4, 0x90
.LBB17_22:                              #   in Loop: Header=BB17_20 Depth=2
	movq	%rdx, 8(%rdi)
	movq	(%rdi), %rdx
	movq	%rdi, %rax
.LBB17_23:                              #   in Loop: Header=BB17_20 Depth=2
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	movq	%rdx, %rdi
	jne	.LBB17_20
# BB#24:                                # %._crit_edge.i41.i
                                        #   in Loop: Header=BB17_18 Depth=1
	testq	%rdx, %rdx
	je	.LBB17_28
	.p2align	4, 0x90
.LBB17_25:                              # %.lr.ph.i.i42.i
                                        #   Parent Loop BB17_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rcx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rdx, (%rsi)
	testq	%rcx, %rcx
	movq	%rcx, %rdx
	jne	.LBB17_25
# BB#26:                                # %list_Delete.exit.i.i
                                        #   in Loop: Header=BB17_18 Depth=1
	testq	%rax, %rax
	je	.LBB17_29
# BB#27:                                #   in Loop: Header=BB17_18 Depth=1
	movq	$0, (%rax)
.LBB17_28:                              # %subs_GetVariables.exit.i
                                        #   in Loop: Header=BB17_18 Depth=1
	movq	vec_VECTOR(,%r12,8), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%rbx, 8(%r14)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r14)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r13, (%rax)
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%rax, %r13
.LBB17_29:                              # %subs_GetVariables.exit.thread.i
                                        #   in Loop: Header=BB17_18 Depth=1
	incl	%r12d
	cmpl	vec_MAX(%rip), %r12d
	jl	.LBB17_18
# BB#30:                                # %._crit_edge.i
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	movq	%r13, %rsi
	callq	litptr_Create
	testq	%r13, %r13
	je	.LBB17_32
	.p2align	4, 0x90
.LBB17_31:                              # %.lr.ph.i34.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rsi, %rsi
	movq	%rsi, %r13
	jne	.LBB17_31
.LBB17_32:                              # %list_Delete.exit36.i
	testq	%rbp, %rbp
	je	.LBB17_33
# BB#34:                                # %.lr.ph.i29.i.preheader
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rax, %r13
	.p2align	4, 0x90
.LBB17_35:                              # %.lr.ph.i29.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB17_35
	jmp	.LBB17_36
.LBB17_10:                              # %list_Delete.exit36.thread.i
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	litptr_Create
	movq	%rax, %r13
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	56(%rax), %rbx
	jmp	.LBB17_36
.LBB17_33:
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rax, %r13
.LBB17_36:                              # %.loopexit22
	movl	vec_MAX(%rip), %r14d
.LBB17_37:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_39 Depth 2
                                        #       Child Loop BB17_41 Depth 3
                                        #         Child Loop BB17_45 Depth 4
                                        #       Child Loop BB17_58 Depth 3
	movq	%r13, %rdi
	callq	subs_CompVec
	cmpl	%r14d, vec_MAX(%rip)
	je	.LBB17_65
# BB#38:                                #   in Loop: Header=BB17_37 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%r14d, %esi
	movq	%r15, %rdx
	callq	subs_SearchTop
	movslq	%eax, %r12
	movl	cont_BINDINGS(%rip), %eax
	xorl	%ebp, %ebp
	movl	cont_STACKPOINTER(%rip), %ecx
	.p2align	4, 0x90
.LBB17_39:                              #   Parent Loop BB17_37 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_41 Depth 3
                                        #         Child Loop BB17_45 Depth 4
                                        #       Child Loop BB17_58 Depth 3
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movslq	%ecx, %rcx
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movl	64(%r15), %ecx
	movl	68(%r15), %eax
	movl	72(%r15), %edx
	leal	(%rax,%rcx), %esi
	addl	%edx, %esi
	cmpl	%esi, %ebp
	jge	.LBB17_47
# BB#40:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB17_39 Depth=2
	movslq	%ebp, %rbp
	.p2align	4, 0x90
.LBB17_41:                              # %.lr.ph
                                        #   Parent Loop BB17_37 Depth=1
                                        #     Parent Loop BB17_39 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB17_45 Depth 4
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	(%rbx), %rax
	movq	(%rax,%r12,8), %rax
	movq	24(%rax), %rsi
	movq	56(%r15), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdx
	callq	unify_Match
	testl	%eax, %eax
	jne	.LBB17_42
# BB#43:                                #   in Loop: Header=BB17_41 Depth=3
	incq	%rbp
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jle	.LBB17_46
# BB#44:                                # %.lr.ph.i.i12.preheader
                                        #   in Loop: Header=BB17_41 Depth=3
	incl	%eax
	.p2align	4, 0x90
.LBB17_45:                              # %.lr.ph.i.i12
                                        #   Parent Loop BB17_37 Depth=1
                                        #     Parent Loop BB17_39 Depth=2
                                        #       Parent Loop BB17_41 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB17_45
.LBB17_46:                              # %cont_BackTrackAndStart.exit.i.backedge
                                        #   in Loop: Header=BB17_41 Depth=3
	movl	64(%r15), %ecx
	movl	68(%r15), %eax
	movl	72(%r15), %edx
	leal	(%rax,%rcx), %esi
	addl	%edx, %esi
	movslq	%esi, %rsi
	cmpq	%rsi, %rbp
	jl	.LBB17_41
	jmp	.LBB17_47
	.p2align	4, 0x90
.LBB17_42:                              # %.lr.ph..critedge.i.loopexit_crit_edge
                                        #   in Loop: Header=BB17_39 Depth=2
	movl	64(%r15), %ecx
	movl	68(%r15), %eax
	movl	72(%r15), %edx
.LBB17_47:                              # %.critedge.i
                                        #   in Loop: Header=BB17_39 Depth=2
	addl	%ecx, %eax
	addl	%edx, %eax
	cmpl	%eax, %ebp
	jge	.LBB17_48
# BB#53:                                #   in Loop: Header=BB17_39 Depth=2
	movl	%r14d, %eax
	subl	vec_MAX(%rip), %eax
	cmpl	$1, %eax
	je	.LBB17_64
# BB#54:                                #   in Loop: Header=BB17_39 Depth=2
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%r14d, %esi
	movq	%r15, %rdx
	callq	subs_InternIdc
	testl	%eax, %eax
	jne	.LBB17_64
# BB#55:                                #   in Loop: Header=BB17_39 Depth=2
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB17_56
# BB#57:                                # %.lr.ph.i41.i.preheader
                                        #   in Loop: Header=BB17_39 Depth=2
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB17_58:                              # %.lr.ph.i41.i
                                        #   Parent Loop BB17_37 Depth=1
                                        #     Parent Loop BB17_39 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB17_58
	jmp	.LBB17_59
	.p2align	4, 0x90
.LBB17_56:                              #   in Loop: Header=BB17_39 Depth=2
	movl	%ecx, %eax
.LBB17_59:                              # %._crit_edge.i42.i
                                        #   in Loop: Header=BB17_39 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB17_60
# BB#61:                                #   in Loop: Header=BB17_39 Depth=2
	leaq	-1(%rdx), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdx,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB17_62
	.p2align	4, 0x90
.LBB17_60:                              #   in Loop: Header=BB17_39 Depth=2
	xorl	%ecx, %ecx
.LBB17_62:                              # %cont_BackTrack.exit43.i
                                        #   in Loop: Header=BB17_39 Depth=2
	incl	%ebp
	movl	68(%r15), %edx
	addl	64(%r15), %edx
	addl	72(%r15), %edx
	cmpl	%edx, %ebp
	jl	.LBB17_39
	jmp	.LBB17_63
	.p2align	4, 0x90
.LBB17_64:                              # %subs_TcVec.exit
                                        #   in Loop: Header=BB17_37 Depth=1
	movl	%r14d, vec_MAX(%rip)
.LBB17_65:                              #   in Loop: Header=BB17_37 Depth=1
	movq	%r13, %rdi
	callq	litptr_AllUsed
	testl	%eax, %eax
	je	.LBB17_37
# BB#66:
	movq	%r13, %rdi
	callq	litptr_Delete
	movl	$1, (%rsp)              # 4-byte Folded Spill
	jmp	.LBB17_67
.LBB17_48:
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB17_51
# BB#49:                                # %.lr.ph.i37.i.preheader
	incl	%eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB17_50:                              # %.lr.ph.i37.i
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB17_50
.LBB17_51:                              # %._crit_edge.i.i13
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB17_63
# BB#52:
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB17_63:                              # %.loopexit
	movl	%r14d, vec_MAX(%rip)
	movq	%r13, %rdi
	callq	litptr_Delete
	movl	$0, (%rsp)              # 4-byte Folded Spill
.LBB17_67:                              # %subs_IdcVecTestlits.exit
	movl	(%rsp), %eax            # 4-byte Reload
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	subs_InternIdc, .Lfunc_end17-subs_InternIdc
	.cfi_endproc

	.globl	subs_IdcEq
	.p2align	4, 0x90
	.type	subs_IdcEq,@function
subs_IdcEq:                             # @subs_IdcEq
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi206:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi207:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi208:
	.cfi_def_cfa_offset 32
.Lcfi209:
	.cfi_offset %rbx, -24
.Lcfi210:
	.cfi_offset %rbp, -16
	movq	%rsi, %r8
	movl	vec_MAX(%rip), %ebp
	movl	68(%rdi), %ecx
	addl	64(%rdi), %ecx
	addl	72(%rdi), %ecx
	jle	.LBB18_3
# BB#1:                                 # %.lr.ph.preheader
	movslq	%ebp, %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB18_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, vec_VECTOR(,%rdx,8)
	incq	%rdx
	movl	%edx, vec_MAX(%rip)
	incq	%rcx
	movslq	68(%rdi), %rsi
	movslq	72(%rdi), %rax
	movslq	64(%rdi), %rbx
	addq	%rsi, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rcx
	jl	.LBB18_2
.LBB18_3:                               # %._crit_edge
	movl	%ebp, %esi
	movq	%r8, %rdx
	callq	subs_InternIdcEq
	movl	%ebp, vec_MAX(%rip)
	movq	cont_LASTBINDING(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB18_6
# BB#4:                                 # %.lr.ph.preheader.i
	movl	cont_BINDINGS(%rip), %edx
	decl	%edx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB18_5:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rsi
	movq	%rsi, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	movl	%edx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rcx
	decl	%edx
	testq	%rcx, %rcx
	jne	.LBB18_5
.LBB18_6:                               # %cont_Reset.exit
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end18:
	.size	subs_IdcEq, .Lfunc_end18-subs_IdcEq
	.cfi_endproc

	.p2align	4, 0x90
	.type	subs_InternIdcEq,@function
subs_InternIdcEq:                       # @subs_InternIdcEq
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi211:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi212:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi213:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi214:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi215:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi216:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi217:
	.cfi_def_cfa_offset 96
.Lcfi218:
	.cfi_offset %rbx, -56
.Lcfi219:
	.cfi_offset %r12, -48
.Lcfi220:
	.cfi_offset %r13, -40
.Lcfi221:
	.cfi_offset %r14, -32
.Lcfi222:
	.cfi_offset %r15, -24
.Lcfi223:
	.cfi_offset %rbp, -16
	movq	%rdx, %r13
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movl	vec_MAX(%rip), %eax
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	cmpl	%esi, %eax
	jle	.LBB19_48
# BB#1:                                 # %.lr.ph118.i.i
	movl	68(%r13), %r14d
	addl	64(%r13), %r14d
	addl	72(%r13), %r14d
	leal	1(%r14), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	.p2align	4, 0x90
.LBB19_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_4 Depth 2
                                        #       Child Loop BB19_7 Depth 3
                                        #       Child Loop BB19_9 Depth 3
                                        #       Child Loop BB19_40 Depth 3
	movl	%ecx, %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	testl	%r14d, %r14d
	movl	$0, %r12d
	jle	.LBB19_46
# BB#3:                                 # %.lr.ph.i.i
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movslq	vec_VECTOR(,%rax,8), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	56(%rcx), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rbp
	movl	cont_BINDINGS(%rip), %eax
	xorl	%r12d, %r12d
	movl	cont_STACKPOINTER(%rip), %ecx
	jmp	.LBB19_4
.LBB19_29:                              #   in Loop: Header=BB19_4 Depth=2
	leal	1(%rcx), %esi
	movl	%esi, cont_STACKPOINTER(%rip)
	movslq	%ecx, %rcx
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	cmpl	%edx, (%rbp)
	movq	%rbp, %rax
	jne	.LBB19_31
# BB#30:                                #   in Loop: Header=BB19_4 Depth=2
	movq	16(%rbp), %rax
	movq	8(%rax), %rax
.LBB19_31:                              # %fol_Atom.exit79.i.i
                                        #   in Loop: Header=BB19_4 Depth=2
	movq	16(%rax), %rax
	movq	8(%rax), %rsi
	cmpl	%edx, (%rbx)
	movq	%rbx, %rax
	movq	%r15, %r13
	jne	.LBB19_33
# BB#32:                                #   in Loop: Header=BB19_4 Depth=2
	movq	16(%rbx), %rax
	movq	8(%rax), %rax
.LBB19_33:                              # %fol_Atom.exit71.i.i
                                        #   in Loop: Header=BB19_4 Depth=2
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	callq	unify_Match
	testl	%eax, %eax
	je	.LBB19_39
# BB#34:                                #   in Loop: Header=BB19_4 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	%eax, (%rbp)
	movq	%rbp, %rcx
	jne	.LBB19_36
# BB#35:                                #   in Loop: Header=BB19_4 Depth=2
	movq	16(%rbp), %rcx
	movq	8(%rcx), %rcx
.LBB19_36:                              # %fol_Atom.exit63.i.i
                                        #   in Loop: Header=BB19_4 Depth=2
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rsi
	cmpl	%eax, (%rbx)
	jne	.LBB19_38
# BB#37:                                #   in Loop: Header=BB19_4 Depth=2
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
.LBB19_38:                              # %fol_Atom.exit.i.i
                                        #   in Loop: Header=BB19_4 Depth=2
	movq	16(%rbx), %rax
	movq	8(%rax), %rdx
	callq	unify_Match
	testl	%eax, %eax
	cmovnel	8(%rsp), %r12d          # 4-byte Folded Reload
.LBB19_39:                              #   in Loop: Header=BB19_4 Depth=2
	xorps	%xmm0, %xmm0
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB19_41
	.p2align	4, 0x90
.LBB19_40:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB19_2 Depth=1
                                        #     Parent Loop BB19_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB19_40
	jmp	.LBB19_42
.LBB19_41:                              #   in Loop: Header=BB19_4 Depth=2
	movl	%ecx, %eax
.LBB19_42:                              # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB19_4 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB19_44
# BB#43:                                #   in Loop: Header=BB19_4 Depth=2
	leaq	-1(%rdx), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdx,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB19_45
.LBB19_44:                              #   in Loop: Header=BB19_4 Depth=2
	xorl	%ecx, %ecx
	jmp	.LBB19_45
	.p2align	4, 0x90
.LBB19_4:                               #   Parent Loop BB19_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB19_7 Depth 3
                                        #       Child Loop BB19_9 Depth 3
                                        #       Child Loop BB19_40 Depth 3
	movq	%r13, %r15
	movq	56(%r13), %rdx
	movslq	%r12d, %r13
	movq	(%rdx,%r13,8), %rdx
	movq	24(%rdx), %rbx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movslq	%ecx, %rcx
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	unify_Match
	testl	%eax, %eax
	movl	cont_BINDINGS(%rip), %ecx
	je	.LBB19_8
# BB#5:                                 #   in Loop: Header=BB19_4 Depth=2
	testl	%ecx, %ecx
	xorps	%xmm0, %xmm0
	jle	.LBB19_10
# BB#6:                                 # %.lr.ph.i105.i.i.preheader
                                        #   in Loop: Header=BB19_4 Depth=2
	movq	%r15, %r13
	.p2align	4, 0x90
.LBB19_7:                               # %.lr.ph.i105.i.i
                                        #   Parent Loop BB19_2 Depth=1
                                        #     Parent Loop BB19_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB19_7
	jmp	.LBB19_11
	.p2align	4, 0x90
.LBB19_8:                               #   in Loop: Header=BB19_4 Depth=2
	testl	%ecx, %ecx
	xorps	%xmm0, %xmm0
	jle	.LBB19_14
	.p2align	4, 0x90
.LBB19_9:                               # %.lr.ph.i99.i.i
                                        #   Parent Loop BB19_2 Depth=1
                                        #     Parent Loop BB19_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB19_9
	jmp	.LBB19_15
	.p2align	4, 0x90
.LBB19_10:                              #   in Loop: Header=BB19_4 Depth=2
	movl	%ecx, %eax
	movq	%r15, %r13
.LBB19_11:                              # %._crit_edge.i106.i.i
                                        #   in Loop: Header=BB19_4 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB19_13
# BB#12:                                #   in Loop: Header=BB19_4 Depth=2
	leaq	-1(%rdx), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdx,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	movl	8(%rsp), %r12d          # 4-byte Reload
	jmp	.LBB19_45
	.p2align	4, 0x90
.LBB19_13:                              #   in Loop: Header=BB19_4 Depth=2
	xorl	%ecx, %ecx
	movl	8(%rsp), %r12d          # 4-byte Reload
	jmp	.LBB19_45
.LBB19_14:                              #   in Loop: Header=BB19_4 Depth=2
	movl	%ecx, %eax
.LBB19_15:                              # %._crit_edge.i100.i.i
                                        #   in Loop: Header=BB19_4 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB19_17
# BB#16:                                #   in Loop: Header=BB19_4 Depth=2
	leaq	-1(%rdx), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdx,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB19_18
.LBB19_17:                              #   in Loop: Header=BB19_4 Depth=2
	xorl	%ecx, %ecx
.LBB19_18:                              # %cont_BackTrack.exit101.i.i
                                        #   in Loop: Header=BB19_4 Depth=2
	movl	(%rbp), %esi
	cmpl	(%rbx), %esi
	jne	.LBB19_28
# BB#19:                                #   in Loop: Header=BB19_4 Depth=2
	movl	fol_NOT(%rip), %edx
	cmpl	%edx, %esi
	movl	%esi, %edi
	jne	.LBB19_21
# BB#20:                                #   in Loop: Header=BB19_4 Depth=2
	movq	16(%rbp), %rdi
	movq	8(%rdi), %rdi
	movl	(%rdi), %edi
.LBB19_21:                              # %fol_Atom.exit95.i.i
                                        #   in Loop: Header=BB19_4 Depth=2
	cmpl	%edi, fol_EQUALITY(%rip)
	jne	.LBB19_28
# BB#22:                                #   in Loop: Header=BB19_4 Depth=2
	cmpl	%edx, %esi
	jne	.LBB19_24
# BB#23:                                #   in Loop: Header=BB19_4 Depth=2
	movq	16(%rbx), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %esi
.LBB19_24:                              # %fol_Atom.exit87.i.i
                                        #   in Loop: Header=BB19_4 Depth=2
	cmpl	%esi, %edi
	jne	.LBB19_28
# BB#25:                                #   in Loop: Header=BB19_4 Depth=2
	movq	(%rsp), %rsi            # 8-byte Reload
	movslq	vec_VECTOR(,%rsi,8), %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	56(%rdi), %rdi
	movq	(%rdi,%rsi,8), %rsi
	cmpl	$0, 8(%rsi)
	je	.LBB19_29
# BB#26:                                #   in Loop: Header=BB19_4 Depth=2
	movq	56(%r15), %rsi
	movq	(%rsi,%r13,8), %rsi
	cmpl	$0, 8(%rsi)
	je	.LBB19_29
	.p2align	4, 0x90
.LBB19_28:                              #   in Loop: Header=BB19_4 Depth=2
	movq	%r15, %r13
.LBB19_45:                              # %cont_BackTrack.exit107.i.i
                                        #   in Loop: Header=BB19_4 Depth=2
	incl	%r12d
	cmpl	%r14d, %r12d
	jl	.LBB19_4
.LBB19_46:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB19_2 Depth=1
	cmpl	%r14d, %r12d
	je	.LBB19_121
# BB#47:                                #   in Loop: Header=BB19_2 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	incl	%ecx
	movl	vec_MAX(%rip), %eax
	cmpl	%eax, %ecx
	jl	.LBB19_2
.LBB19_48:                              # %.preheader.i
	movq	32(%rsp), %rbx          # 8-byte Reload
	cmpl	%ebx, %eax
	jle	.LBB19_66
# BB#49:                                # %.lr.ph.i
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	56(%rax), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB19_50:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_52 Depth 2
                                        #     Child Loop BB19_57 Depth 2
	movl	%ebx, %ebx
	movslq	vec_VECTOR(,%rbx,8), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rdi
	callq	term_VariableSymbols
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB19_61
# BB#51:                                # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB19_50 Depth=1
	movq	cont_LEFTCONTEXT(%rip), %rcx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r12, %rsi
	.p2align	4, 0x90
.LBB19_52:                              # %.lr.ph.i39.i
                                        #   Parent Loop BB19_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rsi), %rdx
	movslq	%edx, %rbp
	shlq	$5, %rbp
	cmpq	$0, 8(%rcx,%rbp)
	je	.LBB19_54
# BB#53:                                #   in Loop: Header=BB19_52 Depth=2
	movq	%rdi, %rdx
	jmp	.LBB19_55
	.p2align	4, 0x90
.LBB19_54:                              #   in Loop: Header=BB19_52 Depth=2
	movq	%rdx, 8(%rdi)
	movq	(%rdi), %rdx
	movq	%rdi, %rax
.LBB19_55:                              #   in Loop: Header=BB19_52 Depth=2
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	movq	%rdx, %rdi
	jne	.LBB19_52
# BB#56:                                # %._crit_edge.i41.i
                                        #   in Loop: Header=BB19_50 Depth=1
	testq	%rdx, %rdx
	je	.LBB19_60
	.p2align	4, 0x90
.LBB19_57:                              # %.lr.ph.i.i42.i
                                        #   Parent Loop BB19_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rcx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rdx, (%rsi)
	testq	%rcx, %rcx
	movq	%rcx, %rdx
	jne	.LBB19_57
# BB#58:                                # %list_Delete.exit.i.i
                                        #   in Loop: Header=BB19_50 Depth=1
	testq	%rax, %rax
	je	.LBB19_61
# BB#59:                                #   in Loop: Header=BB19_50 Depth=1
	movq	$0, (%rax)
.LBB19_60:                              # %subs_GetVariables.exit.i
                                        #   in Loop: Header=BB19_50 Depth=1
	movq	vec_VECTOR(,%rbx,8), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%rbp, 8(%r14)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, (%r14)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%r15, (%rax)
	movq	%r14, (%rsp)            # 8-byte Spill
	movq	%rax, %r15
.LBB19_61:                              # %subs_GetVariables.exit.thread.i
                                        #   in Loop: Header=BB19_50 Depth=1
	incl	%ebx
	cmpl	vec_MAX(%rip), %ebx
	jl	.LBB19_50
# BB#62:                                # %._crit_edge.i
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r15, %rsi
	callq	litptr_Create
	movq	%rax, %rbx
	testq	%r15, %r15
	je	.LBB19_64
	.p2align	4, 0x90
.LBB19_63:                              # %.lr.ph.i34.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB19_63
.LBB19_64:                              # %list_Delete.exit36.i
	movq	(%rsp), %rsi            # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB19_67
	.p2align	4, 0x90
.LBB19_65:                              # %.lr.ph.i29.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB19_65
	jmp	.LBB19_67
.LBB19_66:                              # %list_Delete.exit36.thread.i
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	litptr_Create
	movq	%rax, %rbx
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	56(%rax), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB19_67:                              # %.loopexit30
	movl	vec_MAX(%rip), %r12d
	movq	%rbx, (%rsp)            # 8-byte Spill
	jmp	.LBB19_68
	.p2align	4, 0x90
.LBB19_69:                              #   in Loop: Header=BB19_68 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%r12d, %esi
	movq	%r13, %rdx
	callq	subs_SearchTop
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx), %rcx
	movslq	%eax, %r14
	movq	(%rcx,%r14,8), %rax
	movq	24(%rax), %rbp
	movl	64(%r13), %ecx
	movl	68(%r13), %eax
	movl	72(%r13), %edx
	xorl	%r15d, %r15d
.LBB19_70:                              #   Parent Loop BB19_68 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB19_72 Depth 3
                                        #         Child Loop BB19_84 Depth 4
                                        #         Child Loop BB19_97 Depth 4
                                        #       Child Loop BB19_113 Depth 3
	addl	%ecx, %eax
	addl	%edx, %eax
	cmpl	%eax, %r15d
	jge	.LBB19_103
# BB#71:                                # %.lr.ph
                                        #   in Loop: Header=BB19_70 Depth=2
	movslq	%r15d, %r15
	movl	cont_BINDINGS(%rip), %eax
	movl	cont_STACKPOINTER(%rip), %ecx
	.p2align	4, 0x90
.LBB19_72:                              #   Parent Loop BB19_68 Depth=1
                                        #     Parent Loop BB19_70 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB19_84 Depth 4
                                        #         Child Loop BB19_97 Depth 4
	movq	56(%r13), %rdx
	movq	(%rdx,%r15,8), %rdx
	movq	24(%rdx), %rbx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movslq	%ecx, %rcx
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	unify_Match
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jne	.LBB19_108
# BB#73:                                #   in Loop: Header=BB19_72 Depth=3
	movl	(%rbp), %ecx
	cmpl	(%rbx), %ecx
	jne	.LBB19_96
# BB#74:                                #   in Loop: Header=BB19_72 Depth=3
	movl	fol_NOT(%rip), %eax
	cmpl	%eax, %ecx
	movl	%ecx, %edx
	jne	.LBB19_76
# BB#75:                                #   in Loop: Header=BB19_72 Depth=3
	movq	16(%rbp), %rdx
	movq	8(%rdx), %rdx
	movl	(%rdx), %edx
.LBB19_76:                              # %fol_Atom.exit.i
                                        #   in Loop: Header=BB19_72 Depth=3
	cmpl	%edx, fol_EQUALITY(%rip)
	jne	.LBB19_96
# BB#77:                                #   in Loop: Header=BB19_72 Depth=3
	cmpl	%eax, %ecx
	movl	%ecx, %esi
	jne	.LBB19_79
# BB#78:                                #   in Loop: Header=BB19_72 Depth=3
	movq	16(%rbx), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %esi
.LBB19_79:                              # %fol_Atom.exit78.i
                                        #   in Loop: Header=BB19_72 Depth=3
	cmpl	%esi, %edx
	jne	.LBB19_96
# BB#80:                                #   in Loop: Header=BB19_72 Depth=3
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rdx), %rdx
	movq	(%rdx,%r14,8), %rdx
	cmpl	$0, 8(%rdx)
	je	.LBB19_82
# BB#81:                                #   in Loop: Header=BB19_72 Depth=3
	movq	56(%r13), %rdx
	movq	(%rdx,%r15,8), %rdx
	cmpl	$0, 8(%rdx)
	jne	.LBB19_96
.LBB19_82:                              #   in Loop: Header=BB19_72 Depth=3
	movl	cont_BINDINGS(%rip), %edx
	testl	%edx, %edx
	jle	.LBB19_86
# BB#83:                                # %.lr.ph.i.i14.preheader
                                        #   in Loop: Header=BB19_72 Depth=3
	incl	%edx
	.p2align	4, 0x90
.LBB19_84:                              # %.lr.ph.i.i14
                                        #   Parent Loop BB19_68 Depth=1
                                        #     Parent Loop BB19_70 Depth=2
                                        #       Parent Loop BB19_72 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rsi
	movq	%rsi, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rdx), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%edx
	cmpl	$1, %edx
	jg	.LBB19_84
# BB#85:                                # %cont_BackTrackAndStart.exit.i.loopexit
                                        #   in Loop: Header=BB19_72 Depth=3
	movl	(%rbp), %ecx
.LBB19_86:                              # %cont_BackTrackAndStart.exit.i
                                        #   in Loop: Header=BB19_72 Depth=3
	movq	cont_LEFTCONTEXT(%rip), %rdi
	cmpl	%eax, %ecx
	movq	%rbp, %rcx
	jne	.LBB19_88
# BB#87:                                #   in Loop: Header=BB19_72 Depth=3
	movq	16(%rbp), %rcx
	movq	8(%rcx), %rcx
.LBB19_88:                              # %fol_Atom.exit86.i
                                        #   in Loop: Header=BB19_72 Depth=3
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rsi
	cmpl	%eax, (%rbx)
	movq	%rbx, %rax
	jne	.LBB19_90
# BB#89:                                #   in Loop: Header=BB19_72 Depth=3
	movq	16(%rbx), %rax
	movq	8(%rax), %rax
.LBB19_90:                              # %fol_Atom.exit94.i
                                        #   in Loop: Header=BB19_72 Depth=3
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	callq	unify_Match
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	je	.LBB19_96
# BB#91:                                #   in Loop: Header=BB19_72 Depth=3
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	%eax, (%rbp)
	movq	%rbp, %rcx
	jne	.LBB19_93
# BB#92:                                #   in Loop: Header=BB19_72 Depth=3
	movq	16(%rbp), %rcx
	movq	8(%rcx), %rcx
.LBB19_93:                              # %fol_Atom.exit102.i
                                        #   in Loop: Header=BB19_72 Depth=3
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rsi
	cmpl	%eax, (%rbx)
	jne	.LBB19_95
# BB#94:                                #   in Loop: Header=BB19_72 Depth=3
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
.LBB19_95:                              # %fol_Atom.exit110.i
                                        #   in Loop: Header=BB19_72 Depth=3
	movq	16(%rbx), %rax
	movq	8(%rax), %rdx
	callq	unify_Match
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jne	.LBB19_108
	.p2align	4, 0x90
.LBB19_96:                              # %.thread
                                        #   in Loop: Header=BB19_72 Depth=3
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB19_98
	.p2align	4, 0x90
.LBB19_97:                              # %.lr.ph.i114.i
                                        #   Parent Loop BB19_68 Depth=1
                                        #     Parent Loop BB19_70 Depth=2
                                        #       Parent Loop BB19_72 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB19_97
	jmp	.LBB19_99
	.p2align	4, 0x90
.LBB19_98:                              #   in Loop: Header=BB19_72 Depth=3
	movl	%ecx, %eax
.LBB19_99:                              # %._crit_edge.i.i15
                                        #   in Loop: Header=BB19_72 Depth=3
	incq	%r15
	movslq	cont_STACKPOINTER(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB19_101
# BB#100:                               #   in Loop: Header=BB19_72 Depth=3
	leaq	-1(%rdx), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdx,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB19_102
	.p2align	4, 0x90
.LBB19_101:                             #   in Loop: Header=BB19_72 Depth=3
	xorl	%ecx, %ecx
.LBB19_102:                             # %cont_BackTrack.exit.i.backedge
                                        #   in Loop: Header=BB19_72 Depth=3
	movslq	68(%r13), %rdx
	movslq	72(%r13), %rsi
	movslq	64(%r13), %rdi
	addq	%rdx, %rdi
	addq	%rsi, %rdi
	cmpq	%rdi, %r15
	jl	.LBB19_72
	jmp	.LBB19_103
	.p2align	4, 0x90
.LBB19_108:                             # %cont_BackTrack.exit.i.outer._crit_edge
                                        #   in Loop: Header=BB19_70 Depth=2
	movl	68(%r13), %eax
	addl	64(%r13), %eax
	addl	72(%r13), %eax
	cmpl	%eax, %r15d
	jge	.LBB19_103
# BB#109:                               #   in Loop: Header=BB19_70 Depth=2
	movl	vec_MAX(%rip), %eax
	subl	%r12d, %eax
	cmpl	$1, %eax
	je	.LBB19_118
# BB#110:                               #   in Loop: Header=BB19_70 Depth=2
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%r12d, %esi
	movq	%r13, %rdx
	callq	subs_InternIdcEq
	testl	%eax, %eax
	jne	.LBB19_118
# BB#111:                               #   in Loop: Header=BB19_70 Depth=2
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jle	.LBB19_114
# BB#112:                               # %.lr.ph.i124.i.preheader
                                        #   in Loop: Header=BB19_70 Depth=2
	incl	%eax
	.p2align	4, 0x90
.LBB19_113:                             # %.lr.ph.i124.i
                                        #   Parent Loop BB19_68 Depth=1
                                        #     Parent Loop BB19_70 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB19_113
.LBB19_114:                             # %._crit_edge.i125.i
                                        #   in Loop: Header=BB19_70 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB19_116
# BB#115:                               #   in Loop: Header=BB19_70 Depth=2
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB19_116:                             # %cont_BackTrack.exit126.i
                                        #   in Loop: Header=BB19_70 Depth=2
	incl	%r15d
	movl	64(%r13), %ecx
	movl	68(%r13), %eax
	movl	72(%r13), %edx
	leal	(%rax,%rcx), %esi
	addl	%edx, %esi
	cmpl	%esi, %r15d
	jl	.LBB19_70
	jmp	.LBB19_117
.LBB19_118:                             # %subs_TcVecEq.exit
                                        #   in Loop: Header=BB19_68 Depth=1
	movl	%r12d, vec_MAX(%rip)
	movq	(%rsp), %rbx            # 8-byte Reload
	jmp	.LBB19_119
	.p2align	4, 0x90
.LBB19_68:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_70 Depth 2
                                        #       Child Loop BB19_72 Depth 3
                                        #         Child Loop BB19_84 Depth 4
                                        #         Child Loop BB19_97 Depth 4
                                        #       Child Loop BB19_113 Depth 3
	movq	%rbx, %rdi
	callq	subs_CompVec
	cmpl	%r12d, vec_MAX(%rip)
	jne	.LBB19_69
.LBB19_119:                             #   in Loop: Header=BB19_68 Depth=1
	movq	%rbx, %rdi
	callq	litptr_AllUsed
	testl	%eax, %eax
	je	.LBB19_68
# BB#120:
	movq	%rbx, %rdi
	callq	litptr_Delete
	movl	$1, 20(%rsp)            # 4-byte Folded Spill
	jmp	.LBB19_121
.LBB19_103:                             # %cont_BackTrack.exit.i.outer._crit_edge.thread
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB19_106
# BB#104:                               # %.lr.ph.i118.i.preheader
	incl	%eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB19_105:                             # %.lr.ph.i118.i
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB19_105
.LBB19_106:                             # %._crit_edge.i119.i
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB19_117
# BB#107:
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB19_117:                             # %.loopexit
	movl	%r12d, vec_MAX(%rip)
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	litptr_Delete
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
.LBB19_121:                             # %subs_IdcVecTestlitsEq.exit
	movl	20(%rsp), %eax          # 4-byte Reload
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	subs_InternIdcEq, .Lfunc_end19-subs_InternIdcEq
	.cfi_endproc

	.globl	subs_IdcEqMatch
	.p2align	4, 0x90
	.type	subs_IdcEqMatch,@function
subs_IdcEqMatch:                        # @subs_IdcEqMatch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi224:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi225:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi226:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi227:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi228:
	.cfi_def_cfa_offset 48
.Lcfi229:
	.cfi_offset %rbx, -40
.Lcfi230:
	.cfi_offset %r14, -32
.Lcfi231:
	.cfi_offset %r15, -24
.Lcfi232:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	vec_MAX(%rip), %r15d
	movl	68(%rbx), %eax
	addl	64(%rbx), %eax
	addl	72(%rbx), %eax
	jle	.LBB20_3
# BB#1:                                 # %.lr.ph.preheader
	movslq	%r15d, %rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB20_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, vec_VECTOR(,%rcx,8)
	incq	%rcx
	movl	%ecx, vec_MAX(%rip)
	incq	%rax
	movslq	68(%rbx), %rsi
	movslq	72(%rbx), %rdi
	movslq	64(%rbx), %rbp
	addq	%rsi, %rbp
	addq	%rdi, %rbp
	cmpq	%rbp, %rax
	jl	.LBB20_2
.LBB20_3:                               # %._crit_edge
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%rdx, %rsi
	callq	unify_EstablishMatcher
	movq	%rbx, %rdi
	movl	%r15d, %esi
	movq	%r14, %rdx
	callq	subs_InternIdcEq
	movl	%r15d, vec_MAX(%rip)
	movq	cont_LASTBINDING(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB20_6
# BB#4:                                 # %.lr.ph.preheader.i
	movl	cont_BINDINGS(%rip), %edx
	decl	%edx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB20_5:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rsi
	movq	%rsi, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	movl	%edx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rcx
	decl	%edx
	testq	%rcx, %rcx
	jne	.LBB20_5
.LBB20_6:                               # %cont_Reset.exit
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	subs_IdcEqMatch, .Lfunc_end20-subs_IdcEqMatch
	.cfi_endproc

	.globl	subs_IdcRes
	.p2align	4, 0x90
	.type	subs_IdcRes,@function
subs_IdcRes:                            # @subs_IdcRes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi233:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi234:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi235:
	.cfi_def_cfa_offset 32
.Lcfi236:
	.cfi_offset %rbx, -24
.Lcfi237:
	.cfi_offset %rbp, -16
	movl	%edx, %r8d
	movl	%esi, %r9d
	movl	vec_MAX(%rip), %ebp
	movl	68(%rdi), %edx
	addl	64(%rdi), %edx
	addl	72(%rdi), %edx
	jle	.LBB21_3
# BB#1:                                 # %.lr.ph.preheader
	movslq	%ebp, %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB21_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, vec_VECTOR(,%rsi,8)
	incq	%rsi
	movl	%esi, vec_MAX(%rip)
	incq	%rdx
	movslq	68(%rdi), %rax
	movslq	72(%rdi), %rcx
	movslq	64(%rdi), %rbx
	addq	%rax, %rbx
	addq	%rcx, %rbx
	cmpq	%rbx, %rdx
	jl	.LBB21_2
.LBB21_3:                               # %._crit_edge
	movl	%ebp, %esi
	movl	%r9d, %edx
	movl	%r8d, %ecx
	callq	subs_InternIdcRes
	movl	%ebp, vec_MAX(%rip)
	movq	cont_LASTBINDING(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB21_6
# BB#4:                                 # %.lr.ph.preheader.i
	movl	cont_BINDINGS(%rip), %edx
	decl	%edx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB21_5:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rsi
	movq	%rsi, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	movl	%edx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rcx
	decl	%edx
	testq	%rcx, %rcx
	jne	.LBB21_5
.LBB21_6:                               # %cont_Reset.exit
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end21:
	.size	subs_IdcRes, .Lfunc_end21-subs_IdcRes
	.cfi_endproc

	.p2align	4, 0x90
	.type	subs_InternIdcRes,@function
subs_InternIdcRes:                      # @subs_InternIdcRes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi238:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi239:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi240:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi241:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi242:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi243:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi244:
	.cfi_def_cfa_offset 96
.Lcfi245:
	.cfi_offset %rbx, -56
.Lcfi246:
	.cfi_offset %r12, -48
.Lcfi247:
	.cfi_offset %r13, -40
.Lcfi248:
	.cfi_offset %r14, -32
.Lcfi249:
	.cfi_offset %r15, -24
.Lcfi250:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movl	%esi, %r15d
	movq	%rdi, %r13
	movl	vec_MAX(%rip), %esi
	cmpl	%r15d, %esi
	movl	%edx, 12(%rsp)          # 4-byte Spill
	jle	.LBB22_15
# BB#1:                                 # %.preheader.lr.ph.i.i
	cmpl	%r12d, %edx
	jge	.LBB22_14
# BB#2:                                 # %.preheader.us.preheader.i.i
	leal	1(%r12), %r14d
	movl	cont_BINDINGS(%rip), %eax
	movl	cont_STACKPOINTER(%rip), %ecx
	movl	%r15d, %ebp
	.p2align	4, 0x90
.LBB22_3:                               # %.preheader.us.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_4 Depth 2
                                        #       Child Loop BB22_6 Depth 3
	movl	%ebp, %ebp
	movl	12(%rsp), %ebx          # 4-byte Reload
	.p2align	4, 0x90
.LBB22_4:                               #   Parent Loop BB22_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB22_6 Depth 3
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movslq	%ecx, %rcx
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movslq	vec_VECTOR(,%rbp,8), %rax
	movq	56(%r13), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rsi
	movl	%ebx, %eax
	movslq	vec_VECTOR(,%rax,8), %rax
	movq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rdx
	callq	unify_Match
	testl	%eax, %eax
	cmovnel	%r14d, %ebx
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB22_7
# BB#5:                                 # %.lr.ph.i.us.i.i.preheader
                                        #   in Loop: Header=BB22_4 Depth=2
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB22_6:                               # %.lr.ph.i.us.i.i
                                        #   Parent Loop BB22_3 Depth=1
                                        #     Parent Loop BB22_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB22_6
	jmp	.LBB22_8
	.p2align	4, 0x90
.LBB22_7:                               #   in Loop: Header=BB22_4 Depth=2
	movl	%ecx, %eax
.LBB22_8:                               # %._crit_edge.i.us.i.i
                                        #   in Loop: Header=BB22_4 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB22_10
# BB#9:                                 #   in Loop: Header=BB22_4 Depth=2
	leaq	-1(%rdx), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdx,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB22_11
	.p2align	4, 0x90
.LBB22_10:                              #   in Loop: Header=BB22_4 Depth=2
	xorl	%ecx, %ecx
.LBB22_11:                              # %cont_BackTrack.exit.us.i.i
                                        #   in Loop: Header=BB22_4 Depth=2
	incl	%ebx
	cmpl	%r12d, %ebx
	jl	.LBB22_4
# BB#12:                                # %._crit_edge.us.i.i
                                        #   in Loop: Header=BB22_3 Depth=1
	cmpl	%r12d, %ebx
	je	.LBB22_79
# BB#13:                                #   in Loop: Header=BB22_3 Depth=1
	incl	%ebp
	movl	vec_MAX(%rip), %esi
	cmpl	%esi, %ebp
	jl	.LBB22_3
	jmp	.LBB22_15
.LBB22_14:                              # %.preheader.preheader.i.i
	xorl	%eax, %eax
	cmpl	%r12d, %edx
	je	.LBB22_80
.LBB22_15:                              # %.preheader.i
	cmpl	%r15d, %esi
	movq	%r13, 32(%rsp)          # 8-byte Spill
	jle	.LBB22_34
# BB#16:                                # %.lr.ph.i
	leaq	56(%r13), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB22_17:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_19 Depth 2
                                        #     Child Loop BB22_24 Depth 2
	movl	%r15d, %r15d
	movslq	vec_VECTOR(,%r15,8), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rdi
	callq	term_VariableSymbols
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB22_28
# BB#18:                                # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB22_17 Depth=1
	movq	cont_LEFTCONTEXT(%rip), %rcx
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r14, %rsi
	.p2align	4, 0x90
.LBB22_19:                              # %.lr.ph.i36.i
                                        #   Parent Loop BB22_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rsi), %rdx
	movslq	%edx, %rbx
	shlq	$5, %rbx
	cmpq	$0, 8(%rcx,%rbx)
	je	.LBB22_21
# BB#20:                                #   in Loop: Header=BB22_19 Depth=2
	movq	%rdi, %rdx
	jmp	.LBB22_22
	.p2align	4, 0x90
.LBB22_21:                              #   in Loop: Header=BB22_19 Depth=2
	movq	%rdx, 8(%rdi)
	movq	(%rdi), %rdx
	movq	%rdi, %rax
.LBB22_22:                              #   in Loop: Header=BB22_19 Depth=2
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	movq	%rdx, %rdi
	jne	.LBB22_19
# BB#23:                                # %._crit_edge.i.i
                                        #   in Loop: Header=BB22_17 Depth=1
	testq	%rdx, %rdx
	je	.LBB22_27
	.p2align	4, 0x90
.LBB22_24:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB22_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rcx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rdx, (%rsi)
	testq	%rcx, %rcx
	movq	%rcx, %rdx
	jne	.LBB22_24
# BB#25:                                # %list_Delete.exit.i.i
                                        #   in Loop: Header=BB22_17 Depth=1
	testq	%rax, %rax
	je	.LBB22_28
# BB#26:                                #   in Loop: Header=BB22_17 Depth=1
	movq	$0, (%rax)
.LBB22_27:                              # %subs_GetVariables.exit.i
                                        #   in Loop: Header=BB22_17 Depth=1
	movq	vec_VECTOR(,%r15,8), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	movq	%rbx, 8(%r13)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r13)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbp, (%rax)
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	%rax, %rbp
.LBB22_28:                              # %subs_GetVariables.exit.thread.i
                                        #   in Loop: Header=BB22_17 Depth=1
	incl	%r15d
	cmpl	vec_MAX(%rip), %r15d
	jl	.LBB22_17
# BB#29:                                # %._crit_edge.i
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	litptr_Create
	testq	%rbp, %rbp
	je	.LBB22_31
	.p2align	4, 0x90
.LBB22_30:                              # %.lr.ph.i33.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rsi, %rsi
	movq	%rsi, %rbp
	jne	.LBB22_30
.LBB22_31:                              # %list_Delete.exit34.i
	testq	%rbx, %rbx
	movl	12(%rsp), %r14d         # 4-byte Reload
	je	.LBB22_33
	.p2align	4, 0x90
.LBB22_32:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rsi, %rsi
	movq	%rsi, %rbx
	jne	.LBB22_32
.LBB22_33:
	movq	%rax, %rbx
	jmp	.LBB22_35
.LBB22_34:                              # %list_Delete.exit34.thread.i
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	litptr_Create
	movq	%rax, %rbx
	leaq	56(%r13), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	12(%rsp), %r14d         # 4-byte Reload
.LBB22_35:                              # %.loopexit41
	movl	vec_MAX(%rip), %r13d
	jmp	.LBB22_70
	.p2align	4, 0x90
.LBB22_36:                              #   in Loop: Header=BB22_70 Depth=1
	movq	vec_VECTOR(,%r13,8), %r15
	jle	.LBB22_53
# BB#37:                                # %.preheader.lr.ph.i.i11
                                        #   in Loop: Header=BB22_70 Depth=1
	cmpl	%r12d, %r14d
	jge	.LBB22_53
# BB#38:                                # %.preheader.us.preheader.i.i13
                                        #   in Loop: Header=BB22_70 Depth=1
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movl	cont_BINDINGS(%rip), %eax
	movl	cont_STACKPOINTER(%rip), %ecx
	movl	%r13d, %ebp
	.p2align	4, 0x90
.LBB22_39:                              # %.preheader.us.i.i15
                                        #   Parent Loop BB22_70 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB22_40 Depth 3
                                        #         Child Loop BB22_42 Depth 4
	movl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB22_40:                              #   Parent Loop BB22_70 Depth=1
                                        #     Parent Loop BB22_39 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB22_42 Depth 4
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movslq	%ecx, %rcx
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movslq	vec_VECTOR(,%rbp,8), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rsi
	movl	%r14d, %eax
	movslq	vec_VECTOR(,%rax,8), %rax
	movq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rdx
	callq	unify_Match
	cmpl	$1, %eax
	sbbl	$-1, %ebx
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB22_43
# BB#41:                                # %.lr.ph.i.us.i.i22.preheader
                                        #   in Loop: Header=BB22_40 Depth=3
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB22_42:                              # %.lr.ph.i.us.i.i22
                                        #   Parent Loop BB22_70 Depth=1
                                        #     Parent Loop BB22_39 Depth=2
                                        #       Parent Loop BB22_40 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB22_42
	jmp	.LBB22_44
	.p2align	4, 0x90
.LBB22_43:                              #   in Loop: Header=BB22_40 Depth=3
	movl	%ecx, %eax
.LBB22_44:                              # %._crit_edge.i.us.i.i23
                                        #   in Loop: Header=BB22_40 Depth=3
	movslq	cont_STACKPOINTER(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB22_46
# BB#45:                                #   in Loop: Header=BB22_40 Depth=3
	leaq	-1(%rdx), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdx,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB22_47
	.p2align	4, 0x90
.LBB22_46:                              #   in Loop: Header=BB22_40 Depth=3
	xorl	%ecx, %ecx
.LBB22_47:                              # %cont_BackTrack.exit.us.i.i24
                                        #   in Loop: Header=BB22_40 Depth=3
	incl	%r14d
	cmpl	%r12d, %r14d
	jge	.LBB22_49
# BB#48:                                # %cont_BackTrack.exit.us.i.i24
                                        #   in Loop: Header=BB22_40 Depth=3
	cmpl	$2, %ebx
	jl	.LBB22_40
.LBB22_49:                              # %._crit_edge.us.i.i25
                                        #   in Loop: Header=BB22_39 Depth=2
	orl	$1, %ebx
	cmpl	$1, %ebx
	je	.LBB22_51
# BB#50:                                #   in Loop: Header=BB22_39 Depth=2
	incl	%ebp
	cmpl	vec_MAX(%rip), %ebp
	movl	12(%rsp), %r14d         # 4-byte Reload
	jl	.LBB22_39
	jmp	.LBB22_52
.LBB22_51:                              # %.us-lcssa.us.loopexit.i.i
                                        #   in Loop: Header=BB22_70 Depth=1
	movq	vec_VECTOR(,%rbp,8), %r15
	movl	12(%rsp), %r14d         # 4-byte Reload
.LBB22_52:                              # %subs_SearchTopRes.exit.i
                                        #   in Loop: Header=BB22_70 Depth=1
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB22_53:                              # %subs_SearchTopRes.exit.i
                                        #   in Loop: Header=BB22_70 Depth=1
	movslq	%r15d, %r15
	movl	cont_BINDINGS(%rip), %eax
	movl	cont_STACKPOINTER(%rip), %ecx
	movl	%r14d, %ebp
.LBB22_54:                              #   Parent Loop BB22_70 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB22_55 Depth 3
                                        #         Child Loop BB22_57 Depth 4
                                        #       Child Loop BB22_63 Depth 3
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movslq	%ecx, %rcx
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	cmpl	%r12d, %ebp
	jge	.LBB22_76
	.p2align	4, 0x90
.LBB22_55:                              # %.lr.ph
                                        #   Parent Loop BB22_70 Depth=1
                                        #     Parent Loop BB22_54 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB22_57 Depth 4
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	(%rax,%r15,8), %rcx
	movq	24(%rcx), %rsi
	movl	%ebp, %ecx
	movslq	vec_VECTOR(,%rcx,8), %rcx
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rdx
	callq	unify_Match
	testl	%eax, %eax
	jne	.LBB22_59
# BB#56:                                #   in Loop: Header=BB22_55 Depth=3
	incl	%ebp
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jle	.LBB22_58
	.p2align	4, 0x90
.LBB22_57:                              # %.lr.ph.i.i31
                                        #   Parent Loop BB22_70 Depth=1
                                        #     Parent Loop BB22_54 Depth=2
                                        #       Parent Loop BB22_55 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	cmpl	$1, %eax
	leal	-1(%rax), %eax
	movl	%eax, cont_BINDINGS(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	jg	.LBB22_57
.LBB22_58:                              # %cont_BackTrackAndStart.exit.i.backedge
                                        #   in Loop: Header=BB22_55 Depth=3
	cmpl	%r12d, %ebp
	jl	.LBB22_55
	jmp	.LBB22_73
	.p2align	4, 0x90
.LBB22_59:                              #   in Loop: Header=BB22_54 Depth=2
	movl	%r13d, %eax
	subl	vec_MAX(%rip), %eax
	cmpl	$1, %eax
	je	.LBB22_69
# BB#60:                                #   in Loop: Header=BB22_54 Depth=2
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%r13d, %esi
	movl	%r14d, %edx
	movl	%r12d, %ecx
	callq	subs_InternIdcRes
	testl	%eax, %eax
	jne	.LBB22_69
# BB#61:                                #   in Loop: Header=BB22_54 Depth=2
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB22_64
# BB#62:                                # %.lr.ph.i35.i.preheader
                                        #   in Loop: Header=BB22_54 Depth=2
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB22_63:                              # %.lr.ph.i35.i
                                        #   Parent Loop BB22_70 Depth=1
                                        #     Parent Loop BB22_54 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB22_63
	jmp	.LBB22_65
.LBB22_64:                              #   in Loop: Header=BB22_54 Depth=2
	movl	%ecx, %eax
.LBB22_65:                              # %._crit_edge.i36.i
                                        #   in Loop: Header=BB22_54 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB22_67
# BB#66:                                #   in Loop: Header=BB22_54 Depth=2
	leaq	-1(%rdx), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdx,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB22_68
.LBB22_67:                              #   in Loop: Header=BB22_54 Depth=2
	xorl	%ecx, %ecx
.LBB22_68:                              # %cont_BackTrack.exit37.i
                                        #   in Loop: Header=BB22_54 Depth=2
	incl	%ebp
	cmpl	%r12d, %ebp
	jl	.LBB22_54
	jmp	.LBB22_78
.LBB22_69:                              # %subs_TcVecRes.exit
                                        #   in Loop: Header=BB22_70 Depth=1
	movl	%r13d, vec_MAX(%rip)
	jmp	.LBB22_71
	.p2align	4, 0x90
.LBB22_70:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_39 Depth 2
                                        #       Child Loop BB22_40 Depth 3
                                        #         Child Loop BB22_42 Depth 4
                                        #     Child Loop BB22_54 Depth 2
                                        #       Child Loop BB22_55 Depth 3
                                        #         Child Loop BB22_57 Depth 4
                                        #       Child Loop BB22_63 Depth 3
	movq	%rbx, %rdi
	callq	subs_CompVec
	cmpl	%r13d, vec_MAX(%rip)
	jne	.LBB22_36
.LBB22_71:                              #   in Loop: Header=BB22_70 Depth=1
	movq	%rbx, %rdi
	callq	litptr_AllUsed
	testl	%eax, %eax
	je	.LBB22_70
# BB#72:
	movq	%rbx, %rdi
	callq	litptr_Delete
	movl	$1, %eax
	jmp	.LBB22_80
.LBB22_73:                              # %._crit_edge
	testl	%eax, %eax
	jle	.LBB22_76
# BB#74:                                # %.lr.ph.i31.i.preheader
	incl	%eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB22_75:                              # %.lr.ph.i31.i
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB22_75
.LBB22_76:                              # %._crit_edge.i.i32
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB22_78
# BB#77:
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB22_78:                              # %.loopexit
	movl	%r13d, vec_MAX(%rip)
	movq	%rbx, %rdi
	callq	litptr_Delete
.LBB22_79:                              # %subs_IdcVecTestlitsRes.exit
	xorl	%eax, %eax
.LBB22_80:                              # %subs_IdcVecTestlitsRes.exit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	subs_InternIdcRes, .Lfunc_end22-subs_InternIdcRes
	.cfi_endproc

	.globl	subs_IdcEqMatchExcept
	.p2align	4, 0x90
	.type	subs_IdcEqMatchExcept,@function
subs_IdcEqMatchExcept:                  # @subs_IdcEqMatchExcept
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi251:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi252:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi253:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi254:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi255:
	.cfi_def_cfa_offset 48
.Lcfi256:
	.cfi_offset %rbx, -48
.Lcfi257:
	.cfi_offset %r12, -40
.Lcfi258:
	.cfi_offset %r14, -32
.Lcfi259:
	.cfi_offset %r15, -24
.Lcfi260:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %r15
	movq	%rdi, %rbp
	movl	64(%rbp), %ecx
	movl	68(%rbp), %edx
	movl	72(%rbp), %edi
	leal	(%rdx,%rcx), %ebx
	addl	%edi, %ebx
	movl	$1, %eax
	cmpl	$1, %ebx
	je	.LBB23_10
# BB#1:
	movl	vec_MAX(%rip), %r12d
	testl	%ebx, %ebx
	jle	.LBB23_6
# BB#2:                                 # %.lr.ph.preheader
	movl	%esi, %r9d
	xorl	%esi, %esi
	movl	%r12d, %ebx
	.p2align	4, 0x90
.LBB23_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rsi, %r9
	je	.LBB23_5
# BB#4:                                 #   in Loop: Header=BB23_3 Depth=1
	movslq	%ebx, %rcx
	leal	1(%rbx), %ebx
	movl	%ebx, vec_MAX(%rip)
	movq	%rsi, vec_VECTOR(,%rcx,8)
	movl	64(%rbp), %ecx
	movl	68(%rbp), %edx
	movl	72(%rbp), %edi
                                        # kill: %EBX<def> %EBX<kill> %RBX<def>
.LBB23_5:                               #   in Loop: Header=BB23_3 Depth=1
	incq	%rsi
	leal	(%rdx,%rcx), %eax
	addl	%edi, %eax
	cltq
	cmpq	%rax, %rsi
	jl	.LBB23_3
.LBB23_6:                               # %._crit_edge
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r8, %rsi
	callq	unify_EstablishMatcher
	movq	%rbp, %rdi
	movl	%r12d, %esi
	movq	%r15, %rdx
	movl	%r14d, %ecx
	callq	subs_InternIdcEqExcept
	movl	%r12d, vec_MAX(%rip)
	movq	cont_LASTBINDING(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB23_9
# BB#7:                                 # %.lr.ph.preheader.i
	movl	cont_BINDINGS(%rip), %edx
	decl	%edx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB23_8:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rsi
	movq	%rsi, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	movl	%edx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rcx
	decl	%edx
	testq	%rcx, %rcx
	jne	.LBB23_8
.LBB23_9:                               # %cont_Reset.exit
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
.LBB23_10:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	subs_IdcEqMatchExcept, .Lfunc_end23-subs_IdcEqMatchExcept
	.cfi_endproc

	.p2align	4, 0x90
	.type	subs_InternIdcEqExcept,@function
subs_InternIdcEqExcept:                 # @subs_InternIdcEqExcept
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi261:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi262:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi263:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi264:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi265:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi266:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi267:
	.cfi_def_cfa_offset 112
.Lcfi268:
	.cfi_offset %rbx, -56
.Lcfi269:
	.cfi_offset %r12, -48
.Lcfi270:
	.cfi_offset %r13, -40
.Lcfi271:
	.cfi_offset %r14, -32
.Lcfi272:
	.cfi_offset %r15, -24
.Lcfi273:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movq	%rdx, 24(%rsp)          # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movl	vec_MAX(%rip), %eax
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	cmpl	%esi, %eax
	jle	.LBB24_40
# BB#1:                                 # %.lr.ph112.i.i
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	68(%rax), %ebx
	addl	64(%rax), %ebx
	addl	72(%rax), %ebx
	leal	1(%rbx), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	.p2align	4, 0x90
.LBB24_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_4 Depth 2
                                        #       Child Loop BB24_19 Depth 3
                                        #       Child Loop BB24_34 Depth 3
	movl	%ecx, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	testl	%ebx, %ebx
	movl	$0, %eax
	jle	.LBB24_38
# BB#3:                                 # %.lr.ph.i.i
                                        #   in Loop: Header=BB24_2 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movslq	vec_VECTOR(,%rax,8), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	56(%rcx), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rbp
	xorl	%r13d, %r13d
	jmp	.LBB24_4
.LBB24_17:                              #   in Loop: Header=BB24_4 Depth=2
	movl	cont_BINDINGS(%rip), %edx
	testl	%edx, %edx
	jle	.LBB24_21
# BB#18:                                # %.lr.ph.i84.i.i.preheader
                                        #   in Loop: Header=BB24_4 Depth=2
	incl	%edx
	.p2align	4, 0x90
.LBB24_19:                              # %.lr.ph.i84.i.i
                                        #   Parent Loop BB24_2 Depth=1
                                        #     Parent Loop BB24_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rsi
	movq	%rsi, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rdx), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%edx
	cmpl	$1, %edx
	jg	.LBB24_19
# BB#20:                                # %cont_BackTrackAndStart.exit.loopexit.i.i
                                        #   in Loop: Header=BB24_4 Depth=2
	movl	(%rbp), %ecx
.LBB24_21:                              # %cont_BackTrackAndStart.exit.i.i
                                        #   in Loop: Header=BB24_4 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	cmpl	%eax, %ecx
	movq	%rbp, %rcx
	jne	.LBB24_23
# BB#22:                                #   in Loop: Header=BB24_4 Depth=2
	movq	16(%rbp), %rcx
	movq	8(%rcx), %rcx
.LBB24_23:                              # %fol_Atom.exit80.i.i
                                        #   in Loop: Header=BB24_4 Depth=2
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rsi
	cmpl	%eax, (%r15)
	movq	%r15, %rax
	jne	.LBB24_25
# BB#24:                                #   in Loop: Header=BB24_4 Depth=2
	movq	16(%r15), %rax
	movq	8(%rax), %rax
.LBB24_25:                              # %fol_Atom.exit72.i.i
                                        #   in Loop: Header=BB24_4 Depth=2
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	callq	unify_Match
	testl	%eax, %eax
	je	.LBB24_31
# BB#26:                                #   in Loop: Header=BB24_4 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	%eax, (%rbp)
	movq	%rbp, %rcx
	jne	.LBB24_28
# BB#27:                                #   in Loop: Header=BB24_4 Depth=2
	movq	16(%rbp), %rcx
	movq	8(%rcx), %rcx
.LBB24_28:                              # %fol_Atom.exit64.i.i
                                        #   in Loop: Header=BB24_4 Depth=2
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rsi
	cmpl	%eax, (%r15)
	jne	.LBB24_30
# BB#29:                                #   in Loop: Header=BB24_4 Depth=2
	movq	16(%r15), %rax
	movq	8(%rax), %r15
.LBB24_30:                              # %fol_Atom.exit.i.i
                                        #   in Loop: Header=BB24_4 Depth=2
	movq	16(%r15), %rax
	movq	8(%rax), %rdx
	callq	unify_Match
	testl	%eax, %eax
	cmovnel	32(%rsp), %r13d         # 4-byte Folded Reload
.LBB24_31:                              #   in Loop: Header=BB24_4 Depth=2
	movl	%r13d, %eax
	xorps	%xmm0, %xmm0
	jmp	.LBB24_32
	.p2align	4, 0x90
.LBB24_4:                               #   Parent Loop BB24_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB24_19 Depth 3
                                        #       Child Loop BB24_34 Depth 3
	cmpl	%r12d, %r13d
	movl	%r12d, %eax
	je	.LBB24_37
# BB#5:                                 #   in Loop: Header=BB24_4 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movslq	%r13d, %r14
	movq	(%rax,%r14,8), %rax
	movq	24(%rax), %r15
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	callq	unify_Match
	xorps	%xmm0, %xmm0
	testl	%eax, %eax
	movl	32(%rsp), %eax          # 4-byte Reload
	jne	.LBB24_32
# BB#6:                                 #   in Loop: Header=BB24_4 Depth=2
	movl	(%rbp), %ecx
	cmpl	(%r15), %ecx
	jne	.LBB24_16
# BB#7:                                 #   in Loop: Header=BB24_4 Depth=2
	movl	fol_NOT(%rip), %eax
	cmpl	%eax, %ecx
	movl	%ecx, %edx
	jne	.LBB24_9
# BB#8:                                 #   in Loop: Header=BB24_4 Depth=2
	movq	16(%rbp), %rdx
	movq	8(%rdx), %rdx
	movl	(%rdx), %edx
.LBB24_9:                               # %fol_Atom.exit101.i.i
                                        #   in Loop: Header=BB24_4 Depth=2
	cmpl	%edx, fol_EQUALITY(%rip)
	jne	.LBB24_16
# BB#10:                                #   in Loop: Header=BB24_4 Depth=2
	cmpl	%eax, %ecx
	movl	%ecx, %esi
	jne	.LBB24_12
# BB#11:                                #   in Loop: Header=BB24_4 Depth=2
	movq	16(%r15), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %esi
.LBB24_12:                              # %fol_Atom.exit93.i.i
                                        #   in Loop: Header=BB24_4 Depth=2
	cmpl	%esi, %edx
	jne	.LBB24_16
# BB#13:                                #   in Loop: Header=BB24_4 Depth=2
	movq	16(%rsp), %rdx          # 8-byte Reload
	movslq	vec_VECTOR(,%rdx,8), %rdx
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsi), %rsi
	movq	(%rsi,%rdx,8), %rdx
	cmpl	$0, 8(%rdx)
	je	.LBB24_17
# BB#14:                                #   in Loop: Header=BB24_4 Depth=2
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	56(%rdx), %rdx
	movq	(%rdx,%r14,8), %rdx
	cmpl	$0, 8(%rdx)
	je	.LBB24_17
.LBB24_16:                              #   in Loop: Header=BB24_4 Depth=2
	movl	%r13d, %eax
.LBB24_32:                              #   in Loop: Header=BB24_4 Depth=2
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB24_35
# BB#33:                                # %.lr.ph.i.i.i.preheader
                                        #   in Loop: Header=BB24_4 Depth=2
	incl	%ecx
	.p2align	4, 0x90
.LBB24_34:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB24_2 Depth=1
                                        #     Parent Loop BB24_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rdx
	movq	%rdx, cont_CURRENTBINDING(%rip)
	movq	24(%rdx), %rsi
	movq	%rsi, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rdx)
	movl	$0, 20(%rdx)
	movq	cont_CURRENTBINDING(%rip), %rdx
	movq	$0, 24(%rdx)
	leal	-2(%rcx), %edx
	movl	%edx, cont_BINDINGS(%rip)
	decl	%ecx
	cmpl	$1, %ecx
	jg	.LBB24_34
.LBB24_35:                              # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB24_4 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB24_37
# BB#36:                                #   in Loop: Header=BB24_4 Depth=2
	leaq	-1(%rcx), %rdx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rcx,4), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
.LBB24_37:                              # %cont_BackTrack.exit.i.i
                                        #   in Loop: Header=BB24_4 Depth=2
	incl	%eax
	cmpl	%ebx, %eax
	movl	%eax, %r13d
	jl	.LBB24_4
.LBB24_38:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB24_2 Depth=1
	cmpl	%ebx, %eax
	je	.LBB24_114
# BB#39:                                #   in Loop: Header=BB24_2 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	incl	%ecx
	movl	vec_MAX(%rip), %eax
	cmpl	%eax, %ecx
	jl	.LBB24_2
.LBB24_40:                              # %.preheader.i
	movq	48(%rsp), %r15          # 8-byte Reload
	cmpl	%r15d, %eax
	jle	.LBB24_59
# BB#41:                                # %.lr.ph.i
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	56(%rax), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB24_42:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_44 Depth 2
                                        #     Child Loop BB24_49 Depth 2
	movl	%r15d, %r15d
	movq	%r15, %r14
	movslq	vec_VECTOR(,%r15,8), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rdi
	callq	term_VariableSymbols
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB24_53
# BB#43:                                # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB24_42 Depth=1
	movq	cont_LEFTCONTEXT(%rip), %rcx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbp, %rsi
	.p2align	4, 0x90
.LBB24_44:                              # %.lr.ph.i39.i
                                        #   Parent Loop BB24_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rsi), %rdx
	movslq	%edx, %rbx
	shlq	$5, %rbx
	cmpq	$0, 8(%rcx,%rbx)
	je	.LBB24_46
# BB#45:                                #   in Loop: Header=BB24_44 Depth=2
	movq	%rdi, %rdx
	jmp	.LBB24_47
	.p2align	4, 0x90
.LBB24_46:                              #   in Loop: Header=BB24_44 Depth=2
	movq	%rdx, 8(%rdi)
	movq	(%rdi), %rdx
	movq	%rdi, %rax
.LBB24_47:                              #   in Loop: Header=BB24_44 Depth=2
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	movq	%rdx, %rdi
	jne	.LBB24_44
# BB#48:                                # %._crit_edge.i41.i
                                        #   in Loop: Header=BB24_42 Depth=1
	testq	%rdx, %rdx
	je	.LBB24_52
	.p2align	4, 0x90
.LBB24_49:                              # %.lr.ph.i.i42.i
                                        #   Parent Loop BB24_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rcx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rdx, (%rsi)
	testq	%rcx, %rcx
	movq	%rcx, %rdx
	jne	.LBB24_49
# BB#50:                                # %list_Delete.exit.i.i
                                        #   in Loop: Header=BB24_42 Depth=1
	testq	%rax, %rax
	je	.LBB24_53
# BB#51:                                #   in Loop: Header=BB24_42 Depth=1
	movq	$0, (%rax)
.LBB24_52:                              # %subs_GetVariables.exit.i
                                        #   in Loop: Header=BB24_42 Depth=1
	movq	%r14, %r15
	movq	vec_VECTOR(,%r15,8), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%rbx, 8(%r14)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r14)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r13, (%rax)
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%rax, %r13
	jmp	.LBB24_54
	.p2align	4, 0x90
.LBB24_53:                              #   in Loop: Header=BB24_42 Depth=1
	movq	%r14, %r15
.LBB24_54:                              # %subs_GetVariables.exit.thread.i
                                        #   in Loop: Header=BB24_42 Depth=1
	incl	%r15d
	cmpl	vec_MAX(%rip), %r15d
	jl	.LBB24_42
# BB#55:                                # %._crit_edge.i
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	callq	litptr_Create
	movq	%rax, %r15
	testq	%r13, %r13
	je	.LBB24_57
	.p2align	4, 0x90
.LBB24_56:                              # %.lr.ph.i35.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB24_56
.LBB24_57:                              # %list_Delete.exit37.i
	movq	16(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB24_60
	.p2align	4, 0x90
.LBB24_58:                              # %.lr.ph.i30.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB24_58
	jmp	.LBB24_60
.LBB24_59:                              # %list_Delete.exit37.thread.i
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	litptr_Create
	movq	%rax, %r15
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	56(%rax), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
.LBB24_60:                              # %.loopexit33
	movl	vec_MAX(%rip), %ebx
	leal	1(%r12), %r14d
	jmp	.LBB24_61
	.p2align	4, 0x90
.LBB24_62:                              #   in Loop: Header=BB24_61 Depth=1
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	movl	%ebx, %esi
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	callq	subs_SearchTop
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rcx
	cltq
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rbp
	movl	64(%rbx), %ecx
	movl	68(%rbx), %eax
	movl	72(%rbx), %edx
	xorl	%r15d, %r15d
.LBB24_63:                              #   Parent Loop BB24_61 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB24_64 Depth 3
                                        #         Child Loop BB24_77 Depth 4
                                        #         Child Loop BB24_91 Depth 4
                                        #       Child Loop BB24_106 Depth 3
	addl	%ecx, %eax
	addl	%edx, %eax
	cmpl	%eax, %r15d
	jge	.LBB24_96
	.p2align	4, 0x90
.LBB24_64:                              # %.lr.ph
                                        #   Parent Loop BB24_61 Depth=1
                                        #     Parent Loop BB24_63 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB24_77 Depth 4
                                        #         Child Loop BB24_91 Depth 4
	cmpl	%r12d, %r15d
	movl	%r14d, %eax
	je	.LBB24_95
# BB#65:                                #   in Loop: Header=BB24_64 Depth=3
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movslq	%r15d, %r13
	movq	(%rax,%r13,8), %rax
	movq	24(%rax), %rbx
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	unify_Match
	xorps	%xmm0, %xmm0
	testl	%eax, %eax
	jne	.LBB24_101
# BB#66:                                #   in Loop: Header=BB24_64 Depth=3
	movl	(%rbp), %ecx
	cmpl	(%rbx), %ecx
	jne	.LBB24_89
# BB#67:                                #   in Loop: Header=BB24_64 Depth=3
	movl	fol_NOT(%rip), %eax
	cmpl	%eax, %ecx
	movl	%ecx, %edx
	jne	.LBB24_69
# BB#68:                                #   in Loop: Header=BB24_64 Depth=3
	movq	16(%rbp), %rdx
	movq	8(%rdx), %rdx
	movl	(%rdx), %edx
.LBB24_69:                              # %fol_Atom.exit.i
                                        #   in Loop: Header=BB24_64 Depth=3
	cmpl	%edx, fol_EQUALITY(%rip)
	jne	.LBB24_89
# BB#70:                                #   in Loop: Header=BB24_64 Depth=3
	cmpl	%eax, %ecx
	movl	%ecx, %esi
	jne	.LBB24_72
# BB#71:                                #   in Loop: Header=BB24_64 Depth=3
	movq	16(%rbx), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %esi
.LBB24_72:                              # %fol_Atom.exit81.i
                                        #   in Loop: Header=BB24_64 Depth=3
	cmpl	%esi, %edx
	jne	.LBB24_89
# BB#73:                                #   in Loop: Header=BB24_64 Depth=3
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	(%rdx,%rsi,8), %rdx
	cmpl	$0, 8(%rdx)
	je	.LBB24_75
# BB#74:                                #   in Loop: Header=BB24_64 Depth=3
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	56(%rdx), %rdx
	movq	(%rdx,%r13,8), %rdx
	cmpl	$0, 8(%rdx)
	jne	.LBB24_89
.LBB24_75:                              #   in Loop: Header=BB24_64 Depth=3
	movl	cont_BINDINGS(%rip), %edx
	testl	%edx, %edx
	jle	.LBB24_79
# BB#76:                                # %.lr.ph.i.i17.preheader
                                        #   in Loop: Header=BB24_64 Depth=3
	incl	%edx
	.p2align	4, 0x90
.LBB24_77:                              # %.lr.ph.i.i17
                                        #   Parent Loop BB24_61 Depth=1
                                        #     Parent Loop BB24_63 Depth=2
                                        #       Parent Loop BB24_64 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rsi
	movq	%rsi, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rdx), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%edx
	cmpl	$1, %edx
	jg	.LBB24_77
# BB#78:                                # %cont_BackTrackAndStart.exit.i.loopexit
                                        #   in Loop: Header=BB24_64 Depth=3
	movl	(%rbp), %ecx
.LBB24_79:                              # %cont_BackTrackAndStart.exit.i
                                        #   in Loop: Header=BB24_64 Depth=3
	movq	cont_LEFTCONTEXT(%rip), %rdi
	cmpl	%eax, %ecx
	movq	%rbp, %rcx
	jne	.LBB24_81
# BB#80:                                #   in Loop: Header=BB24_64 Depth=3
	movq	16(%rbp), %rcx
	movq	8(%rcx), %rcx
.LBB24_81:                              # %fol_Atom.exit89.i
                                        #   in Loop: Header=BB24_64 Depth=3
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rsi
	cmpl	%eax, (%rbx)
	movq	%rbx, %rax
	jne	.LBB24_83
# BB#82:                                #   in Loop: Header=BB24_64 Depth=3
	movq	16(%rbx), %rax
	movq	8(%rax), %rax
.LBB24_83:                              # %fol_Atom.exit97.i
                                        #   in Loop: Header=BB24_64 Depth=3
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	callq	unify_Match
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	je	.LBB24_89
# BB#84:                                #   in Loop: Header=BB24_64 Depth=3
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	%eax, (%rbp)
	movq	%rbp, %rcx
	jne	.LBB24_86
# BB#85:                                #   in Loop: Header=BB24_64 Depth=3
	movq	16(%rbp), %rcx
	movq	8(%rcx), %rcx
.LBB24_86:                              # %fol_Atom.exit105.i
                                        #   in Loop: Header=BB24_64 Depth=3
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rsi
	cmpl	%eax, (%rbx)
	jne	.LBB24_88
# BB#87:                                #   in Loop: Header=BB24_64 Depth=3
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
.LBB24_88:                              # %fol_Atom.exit113.i
                                        #   in Loop: Header=BB24_64 Depth=3
	movq	16(%rbx), %rax
	movq	8(%rax), %rdx
	callq	unify_Match
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jne	.LBB24_101
	.p2align	4, 0x90
.LBB24_89:                              # %.thread
                                        #   in Loop: Header=BB24_64 Depth=3
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB24_92
# BB#90:                                # %.lr.ph.i117.i.preheader
                                        #   in Loop: Header=BB24_64 Depth=3
	incl	%eax
	.p2align	4, 0x90
.LBB24_91:                              # %.lr.ph.i117.i
                                        #   Parent Loop BB24_61 Depth=1
                                        #     Parent Loop BB24_63 Depth=2
                                        #       Parent Loop BB24_64 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB24_91
.LBB24_92:                              # %._crit_edge.i.i18
                                        #   in Loop: Header=BB24_64 Depth=3
	incl	%r15d
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB24_94
# BB#93:                                #   in Loop: Header=BB24_64 Depth=3
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB24_94:                              # %cont_BackTrack.exit.i.backedge
                                        #   in Loop: Header=BB24_64 Depth=3
	movl	%r15d, %eax
.LBB24_95:                              # %cont_BackTrack.exit.i.backedge
                                        #   in Loop: Header=BB24_64 Depth=3
	movq	24(%rsp), %rdx          # 8-byte Reload
	movl	68(%rdx), %ecx
	addl	64(%rdx), %ecx
	addl	72(%rdx), %ecx
	cmpl	%ecx, %eax
	movl	%eax, %r15d
	jl	.LBB24_64
	jmp	.LBB24_96
	.p2align	4, 0x90
.LBB24_101:                             # %cont_BackTrack.exit.i.outer._crit_edge
                                        #   in Loop: Header=BB24_63 Depth=2
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	68(%rcx), %eax
	addl	64(%rcx), %eax
	addl	72(%rcx), %eax
	cmpl	%eax, %r15d
	jge	.LBB24_96
# BB#102:                               #   in Loop: Header=BB24_63 Depth=2
	movl	vec_MAX(%rip), %eax
	movl	12(%rsp), %ebx          # 4-byte Reload
	subl	%ebx, %eax
	cmpl	$1, %eax
	je	.LBB24_111
# BB#103:                               #   in Loop: Header=BB24_63 Depth=2
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	%ebx, %esi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movl	%r12d, %ecx
	callq	subs_InternIdcEqExcept
	testl	%eax, %eax
	jne	.LBB24_111
# BB#104:                               #   in Loop: Header=BB24_63 Depth=2
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jle	.LBB24_107
# BB#105:                               # %.lr.ph.i127.i.preheader
                                        #   in Loop: Header=BB24_63 Depth=2
	incl	%eax
	.p2align	4, 0x90
.LBB24_106:                             # %.lr.ph.i127.i
                                        #   Parent Loop BB24_61 Depth=1
                                        #     Parent Loop BB24_63 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB24_106
.LBB24_107:                             # %._crit_edge.i128.i
                                        #   in Loop: Header=BB24_63 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB24_109
# BB#108:                               #   in Loop: Header=BB24_63 Depth=2
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB24_109:                             # %cont_BackTrack.exit129.i
                                        #   in Loop: Header=BB24_63 Depth=2
	incl	%r15d
	movq	24(%rsp), %rdx          # 8-byte Reload
	movl	64(%rdx), %ecx
	movl	68(%rdx), %eax
	movl	72(%rdx), %edx
	leal	(%rax,%rcx), %esi
	addl	%edx, %esi
	cmpl	%esi, %r15d
	jl	.LBB24_63
	jmp	.LBB24_110
.LBB24_111:                             # %subs_TcVecEqExcept.exit
                                        #   in Loop: Header=BB24_61 Depth=1
	movl	%ebx, vec_MAX(%rip)
	movq	48(%rsp), %r15          # 8-byte Reload
	jmp	.LBB24_112
	.p2align	4, 0x90
.LBB24_61:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_63 Depth 2
                                        #       Child Loop BB24_64 Depth 3
                                        #         Child Loop BB24_77 Depth 4
                                        #         Child Loop BB24_91 Depth 4
                                        #       Child Loop BB24_106 Depth 3
	movq	%r15, %rdi
	callq	subs_CompVec
	cmpl	%ebx, vec_MAX(%rip)
	jne	.LBB24_62
.LBB24_112:                             #   in Loop: Header=BB24_61 Depth=1
	movq	%r15, %rdi
	callq	litptr_AllUsed
	testl	%eax, %eax
	je	.LBB24_61
# BB#113:
	movq	%r15, %rdi
	callq	litptr_Delete
	movl	$1, 12(%rsp)            # 4-byte Folded Spill
	jmp	.LBB24_114
.LBB24_96:                              # %cont_BackTrack.exit.i.outer._crit_edge.thread
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB24_99
# BB#97:                                # %.lr.ph.i121.i.preheader
	incl	%eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB24_98:                              # %.lr.ph.i121.i
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB24_98
.LBB24_99:                              # %._crit_edge.i122.i
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB24_110
# BB#100:
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB24_110:                             # %.loopexit
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, vec_MAX(%rip)
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	litptr_Delete
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
.LBB24_114:                             # %subs_IdcVecTestlitsEqExcept.exit
	movl	12(%rsp), %eax          # 4-byte Reload
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	subs_InternIdcEqExcept, .Lfunc_end24-subs_InternIdcEqExcept
	.cfi_endproc

	.p2align	4, 0x90
	.type	subs_CompVec,@function
subs_CompVec:                           # @subs_CompVec
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi274:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi275:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi276:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi277:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi278:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi279:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi280:
	.cfi_def_cfa_offset 64
.Lcfi281:
	.cfi_offset %rbx, -56
.Lcfi282:
	.cfi_offset %r12, -48
.Lcfi283:
	.cfi_offset %r13, -40
.Lcfi284:
	.cfi_offset %r14, -32
.Lcfi285:
	.cfi_offset %r15, -24
.Lcfi286:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	8(%rbx), %r14d
	testl	%r14d, %r14d
	jle	.LBB25_16
# BB#1:                                 # %.lr.ph80
	leal	1(%r14), %r15d
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB25_2:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movslq	%eax, %rbp
	movq	(%rcx,%rbp,8), %rcx
	cmpl	$0, (%rcx)
	jne	.LBB25_4
# BB#3:                                 #   in Loop: Header=BB25_2 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r12, (%rax)
	movq	(%rbx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movslq	4(%rcx), %rcx
	movslq	vec_MAX(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, vec_MAX(%rip)
	movq	%rcx, vec_VECTOR(,%rdx,8)
	movq	(%rbx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movl	$1, (%rcx)
	movq	%rax, %r12
	movl	%r15d, %eax
.LBB25_4:                               #   in Loop: Header=BB25_2 Depth=1
	incl	%eax
	cmpl	%r14d, %eax
	jl	.LBB25_2
# BB#5:                                 # %._crit_edge81
	cmpl	%r14d, %eax
	je	.LBB25_16
# BB#6:                                 # %._crit_edge81
	testq	%r12, %r12
	je	.LBB25_16
# BB#7:                                 # %.lr.ph76.split.us.preheader
	movq	%r12, %rbp
	movq	%r12, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB25_8:                               # %.lr.ph76.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_9 Depth 2
	movslq	8(%r12), %r13
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB25_9:                               #   Parent Loop BB25_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rcx
	movq	(%rcx,%r15,8), %rax
	cmpl	$0, (%rax)
	jne	.LBB25_12
# BB#10:                                #   in Loop: Header=BB25_9 Depth=2
	movq	(%rcx,%r13,8), %rcx
	movq	8(%rcx), %rdi
	movq	8(%rax), %rsi
	callq	list_HasIntersection
	testl	%eax, %eax
	je	.LBB25_12
# BB#11:                                #   in Loop: Header=BB25_9 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, (%rbp)
	movq	(%rbx), %rcx
	movq	(%rcx,%r15,8), %rcx
	movslq	4(%rcx), %rcx
	movslq	vec_MAX(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, vec_MAX(%rip)
	movq	%rcx, vec_VECTOR(,%rdx,8)
	movq	(%rbx), %rcx
	movq	(%rcx,%r15,8), %rcx
	movl	$1, (%rcx)
	movq	%rax, %rbp
.LBB25_12:                              #   in Loop: Header=BB25_9 Depth=2
	incq	%r15
	cmpq	%r15, %r14
	jne	.LBB25_9
# BB#13:                                # %._crit_edge.us
                                        #   in Loop: Header=BB25_8 Depth=1
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB25_8
# BB#14:                                # %.lr.ph.i.preheader
	movq	(%rsp), %rsi            # 8-byte Reload
	.p2align	4, 0x90
.LBB25_15:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB25_15
.LBB25_16:                              # %list_Delete.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	subs_CompVec, .Lfunc_end25-subs_CompVec
	.cfi_endproc

	.p2align	4, 0x90
	.type	subs_SearchTop,@function
subs_SearchTop:                         # @subs_SearchTop
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi287:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi288:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi289:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi290:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi291:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi292:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi293:
	.cfi_def_cfa_offset 80
.Lcfi294:
	.cfi_offset %rbx, -56
.Lcfi295:
	.cfi_offset %r12, -48
.Lcfi296:
	.cfi_offset %r13, -40
.Lcfi297:
	.cfi_offset %r14, -32
.Lcfi298:
	.cfi_offset %r15, -24
.Lcfi299:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rdi, %r12
	movl	%esi, %eax
	movq	vec_VECTOR(,%rax,8), %rax
	cmpl	%esi, vec_MAX(%rip)
	jle	.LBB26_17
# BB#1:                                 # %.preheader.lr.ph
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	64(%r15), %edx
	movl	68(%r15), %ecx
	movl	72(%r15), %edi
	.p2align	4, 0x90
.LBB26_2:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_4 Depth 2
                                        #       Child Loop BB26_7 Depth 3
	addl	%edx, %ecx
	addl	%edi, %ecx
	jle	.LBB26_14
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB26_2 Depth=1
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movl	%esi, %r13d
	movl	cont_BINDINGS(%rip), %eax
	xorl	%ebp, %ebp
	movl	cont_STACKPOINTER(%rip), %esi
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB26_4:                               #   Parent Loop BB26_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB26_7 Depth 3
	leal	1(%rsi), %ecx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movslq	%esi, %rcx
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movslq	vec_VECTOR(,%r13,8), %rax
	movq	56(%r12), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rsi
	movq	56(%r15), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdx
	callq	unify_Match
	cmpl	$1, %eax
	sbbl	$-1, %r14d
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB26_5
# BB#6:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB26_4 Depth=2
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB26_7:                               # %.lr.ph.i
                                        #   Parent Loop BB26_2 Depth=1
                                        #     Parent Loop BB26_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB26_7
	jmp	.LBB26_8
	.p2align	4, 0x90
.LBB26_5:                               #   in Loop: Header=BB26_4 Depth=2
	movl	%ecx, %eax
.LBB26_8:                               # %._crit_edge.i
                                        #   in Loop: Header=BB26_4 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB26_9
# BB#10:                                #   in Loop: Header=BB26_4 Depth=2
	leaq	-1(%rcx), %rsi
	movl	%esi, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rcx,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB26_11
	.p2align	4, 0x90
.LBB26_9:                               #   in Loop: Header=BB26_4 Depth=2
	xorl	%esi, %esi
.LBB26_11:                              # %cont_BackTrack.exit
                                        #   in Loop: Header=BB26_4 Depth=2
	movl	64(%r15), %edx
	movl	68(%r15), %ecx
	movl	72(%r15), %edi
	cmpl	$1, %r14d
	jg	.LBB26_13
# BB#12:                                # %cont_BackTrack.exit
                                        #   in Loop: Header=BB26_4 Depth=2
	incq	%rbp
	leal	(%rcx,%rdx), %ebx
	addl	%edi, %ebx
	movslq	%ebx, %rbx
	cmpq	%rbx, %rbp
	jl	.LBB26_4
.LBB26_13:                              # %._crit_edge
                                        #   in Loop: Header=BB26_2 Depth=1
	orl	$1, %r14d
	cmpl	$1, %r14d
	movl	12(%rsp), %esi          # 4-byte Reload
	je	.LBB26_14
# BB#15:                                #   in Loop: Header=BB26_2 Depth=1
	incl	%esi
	cmpl	vec_MAX(%rip), %esi
	jl	.LBB26_2
# BB#16:
	movq	16(%rsp), %rax          # 8-byte Reload
	jmp	.LBB26_17
.LBB26_14:                              # %._crit_edge.thread
	movl	%esi, %eax
	movq	vec_VECTOR(,%rax,8), %rax
.LBB26_17:                              # %.loopexit
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	subs_SearchTop, .Lfunc_end26-subs_SearchTop
	.cfi_endproc

	.type	stamp,@object           # @stamp
	.local	stamp
	.comm	stamp,4,4
	.type	multvec_j,@object       # @multvec_j
	.local	multvec_j
	.comm	multvec_j,400,16
	.type	multvec_i,@object       # @multvec_i
	.local	multvec_i
	.comm	multvec_i,400,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
