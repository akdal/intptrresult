	.text
	.file	"gs.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movl	%edi, %r15d
	movq	$0, gs_lib_env_path(%rip)
	movl	$.L.str.1, %edi
	callq	getenv
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_2
# BB#1:
	movq	%rbx, %rdi
	callq	strlen
	leal	1(%rax), %edi
	movl	$1, %esi
	movl	$.L.str.1, %edx
	callq	gs_malloc
	movq	%rax, gs_lib_env_path(%rip)
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcpy
.LBB0_2:
	leal	2(%r15), %edi
	movl	$8, %esi
	movl	$.L.str.2, %edx
	callq	gs_malloc
	movq	%rax, gs_lib_paths(%rip)
	movl	$0, gs_lib_count(%rip)
	movq	gs_lib_env_path(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB0_4
# BB#3:
	movq	%rcx, (%rax)
	addq	$8, %rax
.LBB0_4:
	movq	gs_lib_default_path(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB0_6
# BB#5:
	movq	%rcx, (%rax)
	addq	$8, %rax
.LBB0_6:                                # %set_lib_paths.exit
	movq	$0, (%rax)
	movl	$0, quiet(%rip)
	movl	$1, user_errors(%rip)
	movl	$.L.str.3, %edx
	movl	$swproc, %ecx
	movl	$argproc, %r8d
	xorl	%eax, %eax
	movl	%r15d, %edi
	movq	%r14, %rsi
	callq	gs_main
	testl	%eax, %eax
	jne	.LBB0_8
# BB#7:
	callq	init2
.LBB0_8:
	xorl	%edi, %edi
	xorl	%eax, %eax
	callq	zflush
	xorl	%edi, %edi
	xorl	%eax, %eax
	callq	zflushpage
	xorl	%edi, %edi
	xorl	%eax, %eax
	callq	gs_exit
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.globl	swproc
	.p2align	4, 0x90
	.type	swproc,@function
swproc:                                 # @swproc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 40
	subq	$136, %rsp
.Lcfi10:
	.cfi_def_cfa_offset 176
.Lcfi11:
	.cfi_offset %rbx, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %ebx
	movl	$-1, %ebp
	leal	-68(%rbx), %eax
	cmpl	$51, %eax
	ja	.LBB1_36
# BB#1:
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_2:
	movl	$61, %esi
	movq	%r14, %rdi
	callq	strchr
	movq	%rax, %r15
	cmpl	$0, init1_done(%rip)
	jne	.LBB1_4
# BB#3:
	movl	memory_chunk_size(%rip), %edx
	movl	$gs_malloc, %edi
	movl	$gs_free, %esi
	callq	alloc_init
	xorl	%eax, %eax
	callq	name_init
	xorl	%eax, %eax
	callq	obj_init
	xorl	%eax, %eax
	callq	scan_init
	movl	$1, init1_done(%rip)
.LBB1_4:                                # %init1.exit35
	testq	%r15, %r15
	je	.LBB1_14
# BB#5:
	cmpq	%r14, %r15
	je	.LBB1_28
# BB#6:
	movq	%r15, %rax
	incq	%r15
	movb	$0, (%rax)
	orb	$32, %bl
	movq	%r15, %rdi
	cmpb	$100, %bl
	jne	.LBB1_32
# BB#7:
	callq	strlen
	leaq	16(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movl	%eax, %edx
	callq	sread_string
	movq	%rsp, %rdx
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	scan_token
	testl	%eax, %eax
	je	.LBB1_34
# BB#8:
	movl	$.L.str.6, %edi
	jmp	.LBB1_29
.LBB1_9:
	cmpl	$0, init1_done(%rip)
	jne	.LBB1_11
# BB#10:
	movl	memory_chunk_size(%rip), %edx
	movl	$gs_malloc, %edi
	movl	$gs_free, %esi
	callq	alloc_init
	xorl	%eax, %eax
	callq	name_init
	xorl	%eax, %eax
	callq	obj_init
	xorl	%eax, %eax
	callq	scan_init
	movl	$1, init1_done(%rip)
.LBB1_11:                               # %init1.exit36
	movq	%r14, %rdi
	callq	strlen
	leaq	16(%rsp), %rbp
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movl	%eax, %edx
	callq	sread_string
	movq	%rsp, %rdx
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	scan_token
	testl	%eax, %eax
	je	.LBB1_30
.LBB1_12:
	movl	$.Lstr, %edi
	callq	puts
	movl	$1, %edi
	xorl	%eax, %eax
	callq	gs_exit
.LBB1_13:
	cmpb	$119, %bl
	movl	$.L.str.12, %eax
	movl	$.L.str.13, %edi
	cmoveq	%rax, %rdi
	xorl	%ebp, %ebp
	movq	%rsp, %rsi
	jmp	.LBB1_27
.LBB1_14:
	movq	$0, (%rsp)
	movw	$32, 8(%rsp)
	jmp	.LBB1_34
.LBB1_15:
	movl	$0, user_errors(%rip)
	xorl	%ebp, %ebp
	jmp	.LBB1_36
.LBB1_16:
	movq	gs_lib_paths(%rip), %rax
	movslq	gs_lib_count(%rip), %rcx
	movq	%r14, (%rax,%rcx,8)
	leaq	1(%rcx), %rax
	movl	%eax, gs_lib_count(%rip)
	leaq	8(,%rcx,8), %rax
	addq	gs_lib_paths(%rip), %rax
	movq	gs_lib_env_path(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB1_18
# BB#17:
	movq	%rcx, (%rax)
	addq	$8, %rax
.LBB1_18:
	movq	gs_lib_default_path(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB1_20
# BB#19:
	movq	%rcx, (%rax)
	addq	$8, %rax
.LBB1_20:                               # %set_lib_paths.exit
	movq	$0, (%rax)
	xorl	%ebp, %ebp
	jmp	.LBB1_36
.LBB1_21:
	movl	$0, 16(%rsp)
	xorl	%ebp, %ebp
	leaq	16(%rsp), %rdx
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sscanf
	movl	16(%rsp), %eax
	leal	-1(%rax), %ecx
	cmpl	$63, %ecx
	jb	.LBB1_23
# BB#22:
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$1, %edi
	xorl	%eax, %eax
	callq	gs_exit
	movl	16(%rsp), %eax
.LBB1_23:
	shll	$10, %eax
	movl	%eax, memory_chunk_size(%rip)
	jmp	.LBB1_36
.LBB1_24:
	movl	$1, quiet(%rip)
	cmpl	$0, init1_done(%rip)
	jne	.LBB1_26
# BB#25:
	movl	memory_chunk_size(%rip), %edx
	movl	$gs_malloc, %edi
	movl	$gs_free, %esi
	callq	alloc_init
	xorl	%eax, %eax
	callq	name_init
	xorl	%eax, %eax
	callq	obj_init
	xorl	%eax, %eax
	callq	scan_init
	movl	$1, init1_done(%rip)
.LBB1_26:                               # %init1.exit
	movq	$0, 16(%rsp)
	movw	$32, 24(%rsp)
	xorl	%ebp, %ebp
	leaq	16(%rsp), %rsi
	movl	$.L.str.4, %edi
.LBB1_27:
	xorl	%eax, %eax
	jmp	.LBB1_35
.LBB1_28:
	movl	$.L.str.5, %edi
.LBB1_29:
	xorl	%eax, %eax
	callq	printf
	movl	$1, %edi
	xorl	%eax, %eax
	callq	gs_exit
	jmp	.LBB1_34
.LBB1_30:
	movb	8(%rsp), %al
	shrb	$2, %al
	cmpb	$5, %al
	je	.LBB1_13
# BB#31:
	cmpb	$11, %al
	je	.LBB1_13
	jmp	.LBB1_12
.LBB1_32:
	callq	strlen
	movq	%rax, %rbx
	movl	$1, %esi
	movl	$.L.str.7, %edx
	movl	%ebx, %edi
	callq	gs_malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB1_37
.LBB1_33:
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	strcpy
	movq	%rbp, (%rsp)
	movw	$566, 8(%rsp)           # imm = 0x236
	movw	%bx, 10(%rsp)
.LBB1_34:
	xorl	%ebp, %ebp
	movq	%rsp, %rsi
	xorl	%eax, %eax
	movq	%r14, %rdi
.LBB1_35:
	callq	initial_enter_name
.LBB1_36:
	movl	%ebp, %eax
	addq	$136, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_37:
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	movl	$.L.str.9, %edx
	movl	$153, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.10, %edi
	movl	$15, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	xorl	%eax, %eax
	callq	gs_exit
	jmp	.LBB1_33
.Lfunc_end1:
	.size	swproc, .Lfunc_end1-swproc
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_2
	.quad	.LBB1_15
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_16
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_21
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_2
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_2
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_9
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_24
	.quad	.LBB1_36
	.quad	.LBB1_2
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_9

	.text
	.globl	argproc
	.p2align	4, 0x90
	.type	argproc,@function
argproc:                                # @argproc
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	subq	$280, %rsp              # imm = 0x118
.Lcfi17:
	.cfi_def_cfa_offset 304
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	callq	init2
	movb	$0, 18(%rsp)
	movw	$10363, 16(%rsp)        # imm = 0x287B
	leaq	16(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	strcat
	movq	%rbx, %rdi
	callq	strlen
	movabsq	$28557020175366245, %rcx # imm = 0x65747563657865
	movq	%rcx, 21(%rsp,%rax)
	movabsq	$7311705584434508329, %rcx # imm = 0x6578657D6E757229
	movq	%rcx, 16(%rsp,%rax)
	movq	%rbx, (%rsp)
	movw	$567, 8(%rsp)           # imm = 0x237
	movq	%rbx, %rdi
	callq	strlen
	movw	%ax, 10(%rsp)
	movl	user_errors(%rip), %esi
	movq	%rsp, %rdi
	xorl	%eax, %eax
	callq	interpret
	movl	%eax, %ebx
	xorl	%edi, %edi
	xorl	%eax, %eax
	callq	zflushpage
	testl	%ebx, %ebx
	je	.LBB2_2
# BB#1:
	movq	osp(%rip), %rdi
	xorl	%eax, %eax
	callq	zflush
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movq	osbot(%rip), %rdi
	movq	osp(%rip), %rsi
	addq	$16, %rsi
	movl	$.L.str.23, %edx
	xorl	%eax, %eax
	callq	debug_dump_refs
	movq	esp(%rip), %rsi
	addq	$16, %rsi
	movl	$estack, %edi
	movl	$.L.str.24, %edx
	xorl	%eax, %eax
	callq	debug_dump_refs
	movl	$1, %edi
	xorl	%eax, %eax
	callq	gs_exit
.LBB2_2:
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	argproc, .Lfunc_end2-argproc
	.cfi_endproc

	.globl	debug_dump_stack
	.p2align	4, 0x90
	.type	debug_dump_stack,@function
debug_dump_stack:                       # @debug_dump_stack
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 16
.Lcfi21:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movq	osp(%rip), %rdi
	xorl	%eax, %eax
	callq	zflush
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movq	osbot(%rip), %rdi
	movq	osp(%rip), %rsi
	addq	$16, %rsi
	movl	$.L.str.23, %edx
	xorl	%eax, %eax
	callq	debug_dump_refs
	movq	esp(%rip), %rsi
	addq	$16, %rsi
	movl	$estack, %edi
	movl	$.L.str.24, %edx
	xorl	%eax, %eax
	popq	%rbx
	jmp	debug_dump_refs         # TAILCALL
.Lfunc_end3:
	.size	debug_dump_stack, .Lfunc_end3-debug_dump_stack
	.cfi_endproc

	.globl	interpret_string
	.p2align	4, 0x90
	.type	interpret_string,@function
interpret_string:                       # @interpret_string
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 32
	movq	%rdi, 8(%rsp)
	movw	$567, 16(%rsp)          # imm = 0x237
	callq	strlen
	movw	%ax, 18(%rsp)
	movl	user_errors(%rip), %esi
	leaq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	interpret
	addq	$24, %rsp
	retq
.Lfunc_end4:
	.size	interpret_string, .Lfunc_end4-interpret_string
	.cfi_endproc

	.globl	init1
	.p2align	4, 0x90
	.type	init1,@function
init1:                                  # @init1
	.cfi_startproc
# BB#0:
	cmpl	$0, init1_done(%rip)
	jne	.LBB5_2
# BB#1:
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 16
	movl	memory_chunk_size(%rip), %edx
	movl	$gs_malloc, %edi
	movl	$gs_free, %esi
	callq	alloc_init
	xorl	%eax, %eax
	callq	name_init
	xorl	%eax, %eax
	callq	obj_init
	xorl	%eax, %eax
	callq	scan_init
	movl	$1, init1_done(%rip)
	addq	$8, %rsp
.LBB5_2:
	retq
.Lfunc_end5:
	.size	init1, .Lfunc_end5-init1
	.cfi_endproc

	.globl	init2
	.p2align	4, 0x90
	.type	init2,@function
init2:                                  # @init2
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 16
	cmpl	$0, init1_done(%rip)
	jne	.LBB6_2
# BB#1:
	movl	memory_chunk_size(%rip), %edx
	movl	$gs_malloc, %edi
	movl	$gs_free, %esi
	callq	alloc_init
	xorl	%eax, %eax
	callq	name_init
	xorl	%eax, %eax
	callq	obj_init
	xorl	%eax, %eax
	callq	scan_init
	movl	$1, init1_done(%rip)
.LBB6_2:                                # %init1.exit
	cmpl	$0, init2_done(%rip)
	jne	.LBB6_4
# BB#3:
	xorl	%eax, %eax
	callq	gs_init
	xorl	%eax, %eax
	callq	zfile_init
	xorl	%eax, %eax
	callq	zfont_init
	xorl	%eax, %eax
	callq	zmath_init
	xorl	%eax, %eax
	callq	zmatrix_init
	movl	$1, %edi
	xorl	%eax, %eax
	callq	interp_init
	xorl	%eax, %eax
	callq	op_init
	movl	user_errors(%rip), %esi
	movl	$.L.str.18, %edi
	callq	run_file
	movl	$1, init2_done(%rip)
.LBB6_4:
	popq	%rax
	retq
.Lfunc_end6:
	.size	init2, .Lfunc_end6-init2
	.cfi_endproc

	.globl	run_file
	.p2align	4, 0x90
	.type	run_file,@function
run_file:                               # @run_file
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 48
.Lcfi28:
	.cfi_offset %rbx, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	callq	strlen
	movq	%rax, %rcx
	leaq	8(%rsp), %rdx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rsi
	callq	lib_file_open
	testl	%eax, %eax
	js	.LBB7_1
# BB#2:
	cmpl	$0, quiet(%rip)
	jne	.LBB7_4
.LBB7_3:
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB7_4:
	orb	$3, 16(%rsp)
	leaq	8(%rsp), %rdi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	interpret
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jns	.LBB7_6
# BB#5:
	movq	osp(%rip), %rdi
	xorl	%eax, %eax
	callq	zflush
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
	movq	osbot(%rip), %rdi
	movq	osp(%rip), %rsi
	addq	$16, %rsi
	movl	$.L.str.23, %edx
	xorl	%eax, %eax
	callq	debug_dump_refs
	movq	esp(%rip), %rsi
	addq	$16, %rsi
	movl	$estack, %edi
	movl	$.L.str.24, %edx
	xorl	%eax, %eax
	callq	debug_dump_refs
	movl	$1, %edi
	xorl	%eax, %eax
	callq	gs_exit
.LBB7_6:
	cmpl	$0, quiet(%rip)
	jne	.LBB7_8
# BB#7:
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB7_8:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB7_1:
	movq	stderr(%rip), %rdi
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	fprintf
	movl	$1, %edi
	xorl	%eax, %eax
	callq	gs_exit
	cmpl	$0, quiet(%rip)
	jne	.LBB7_4
	jmp	.LBB7_3
.Lfunc_end7:
	.size	run_file, .Lfunc_end7-run_file
	.cfi_endproc

	.globl	set_lib_paths
	.p2align	4, 0x90
	.type	set_lib_paths,@function
set_lib_paths:                          # @set_lib_paths
	.cfi_startproc
# BB#0:
	movslq	gs_lib_count(%rip), %rax
	shlq	$3, %rax
	addq	gs_lib_paths(%rip), %rax
	movq	gs_lib_env_path(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB8_2
# BB#1:
	movq	%rcx, (%rax)
	addq	$8, %rax
.LBB8_2:
	movq	gs_lib_default_path(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB8_4
# BB#3:
	movq	%rcx, (%rax)
	addq	$8, %rax
.LBB8_4:
	movq	$0, (%rax)
	retq
.Lfunc_end8:
	.size	set_lib_paths, .Lfunc_end8-set_lib_paths
	.cfi_endproc

	.type	memory_chunk_size,@object # @memory_chunk_size
	.data
	.globl	memory_chunk_size
	.p2align	2
memory_chunk_size:
	.long	20000                   # 0x4e20
	.size	memory_chunk_size, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Benchmarks/MallocBench/gs:/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Benchmarks/MallocBench/gs/fonts"
	.size	.L.str, 158

	.type	gs_lib_default_path,@object # @gs_lib_default_path
	.data
	.globl	gs_lib_default_path
	.p2align	3
gs_lib_default_path:
	.quad	.L.str
	.size	gs_lib_default_path, 8

	.type	gs_lib_env_path,@object # @gs_lib_env_path
	.comm	gs_lib_env_path,8,8
	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"GS_LIB"
	.size	.L.str.1, 7

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"-I array"
	.size	.L.str.2, 9

	.type	gs_lib_paths,@object    # @gs_lib_paths
	.comm	gs_lib_paths,8,8
	.type	gs_lib_count,@object    # @gs_lib_count
	.comm	gs_lib_count,4,4
	.type	quiet,@object           # @quiet
	.comm	quiet,4,4
	.type	user_errors,@object     # @user_errors
	.comm	user_errors,4,4
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"GS.MAP"
	.size	.L.str.3, 7

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"QUIET"
	.size	.L.str.4, 6

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Usage: -dname, -dname=token, -sname=string"
	.size	.L.str.5, 43

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"-dname= must be followed by a valid token"
	.size	.L.str.6, 42

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"-s"
	.size	.L.str.7, 3

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"%s(%d): "
	.size	.L.str.8, 9

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Benchmarks/MallocBench/gs/gs.c"
	.size	.L.str.9, 81

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Out of memory!\n"
	.size	.L.str.10, 16

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"DEVICEWIDTH"
	.size	.L.str.12, 12

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"DEVICEHEIGHT"
	.size	.L.str.13, 13

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"%d"
	.size	.L.str.14, 3

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"-M must be between 1 and 64"
	.size	.L.str.15, 28

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"{("
	.size	.L.str.16, 3

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	")run}execute"
	.size	.L.str.17, 13

	.type	init1_done,@object      # @init1_done
	.bss
	.globl	init1_done
	.p2align	2
init1_done:
	.long	0                       # 0x0
	.size	init1_done, 4

	.type	init2_done,@object      # @init2_done
	.globl	init2_done
	.p2align	2
init2_done:
	.long	0                       # 0x0
	.size	init2_done, 4

	.type	.L.str.18,@object       # @.str.18
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.18:
	.asciz	"ghost.ps"
	.size	.L.str.18, 9

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Can't find file %s (from command line)\n"
	.size	.L.str.19, 40

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Reading %s... "
	.size	.L.str.20, 15

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"%s read.\n"
	.size	.L.str.21, 10

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"\nInterp returns %d\n"
	.size	.L.str.22, 20

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"ostack"
	.size	.L.str.23, 7

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"estack"
	.size	.L.str.24, 7

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"-w and -h must be followed by a number"
	.size	.Lstr, 39


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
