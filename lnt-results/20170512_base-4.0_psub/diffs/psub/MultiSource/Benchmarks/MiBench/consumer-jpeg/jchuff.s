	.text
	.file	"jchuff.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	1                       # 0x1
	.quad	1                       # 0x1
	.text
	.globl	jpeg_make_c_derived_tbl
	.p2align	4, 0x90
	.type	jpeg_make_c_derived_tbl,@function
jpeg_make_c_derived_tbl:                # @jpeg_make_c_derived_tbl
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$1312, %rsp             # imm = 0x520
.Lcfi5:
	.cfi_def_cfa_offset 1360
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	(%rbx), %r15
	testq	%r15, %r15
	jne	.LBB0_2
# BB#1:
	movq	8(%rdi), %rax
	movl	$1, %esi
	movl	$1280, %edx             # imm = 0x500
	callq	*(%rax)
	movq	%rax, %r15
	movq	%r15, (%rbx)
.LBB0_2:
	xorl	%ebx, %ebx
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader50
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_8 Depth 2
                                        #     Child Loop BB0_12 Depth 2
	movzbl	(%r14,%r12), %ebp
	testl	%ebp, %ebp
	je	.LBB0_13
# BB#4:                                 # %.lr.ph63
                                        #   in Loop: Header=BB0_3 Depth=1
	movslq	%ebx, %rbx
	leaq	(%rsp,%rbx), %rdi
	cmpl	$1, %ebp
	movl	$1, %edx
	cmoval	%ebp, %edx
	decl	%edx
	incq	%rdx
	movl	%r12d, %esi
	callq	memset
	cmpb	$4, %bpl
	jb	.LBB0_10
# BB#6:                                 # %min.iters.checked
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%ebp, %ecx
	andl	$3, %ecx
	movl	%ebp, %eax
	subl	%ecx, %eax
	movl	%ebp, %edx
	subl	%ecx, %edx
	movdqa	.LCPI0_0(%rip), %xmm2   # xmm2 = [1,1]
	je	.LBB0_10
# BB#7:                                 # %vector.ph
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%ebp, %ecx
	andb	$3, %cl
	incl	%eax
	movd	%rbx, %xmm1
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_8:                                # %vector.body
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	paddq	%xmm2, %xmm1
	paddq	%xmm2, %xmm0
	addl	$-4, %edx
	jne	.LBB0_8
# BB#9:                                 # %middle.block
                                        #   in Loop: Header=BB0_3 Depth=1
	paddq	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddq	%xmm0, %xmm1
	movd	%xmm1, %rbx
	testb	%cl, %cl
	jne	.LBB0_11
	jmp	.LBB0_13
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_3 Depth=1
	movl	$1, %eax
.LBB0_11:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	decl	%eax
	.p2align	4, 0x90
.LBB0_12:                               # %scalar.ph
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rbx
	incl	%eax
	cmpl	%ebp, %eax
	jl	.LBB0_12
.LBB0_13:                               # %._crit_edge64
                                        #   in Loop: Header=BB0_3 Depth=1
	incq	%r12
	cmpq	$17, %r12
	jne	.LBB0_3
# BB#14:
	movslq	%ebx, %rax
	movb	$0, (%rsp,%rax)
	movsbl	(%rsp), %eax
	testl	%eax, %eax
	je	.LBB0_25
# BB#15:                                # %.preheader.preheader
	xorl	%ecx, %ecx
	movabsq	$4294967296, %r9        # imm = 0x100000000
	movl	%eax, %esi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_16:                               # %.preheader.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_19 Depth 2
                                        #     Child Loop BB0_21 Depth 2
                                        #     Child Loop BB0_23 Depth 2
	movsbl	%sil, %edi
	movl	%edi, %ebp
	subl	%eax, %ebp
	je	.LBB0_22
# BB#17:                                # %._crit_edge55.thread.preheader
                                        #   in Loop: Header=BB0_16 Depth=1
	leal	-1(%rdi), %r8d
	subl	%eax, %r8d
	andl	$7, %ebp
	je	.LBB0_20
# BB#18:                                # %._crit_edge55.thread.prol.preheader
                                        #   in Loop: Header=BB0_16 Depth=1
	negl	%ebp
	.p2align	4, 0x90
.LBB0_19:                               # %._crit_edge55.thread.prol
                                        #   Parent Loop BB0_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	%ecx, %ecx
	incl	%eax
	incl	%ebp
	jne	.LBB0_19
.LBB0_20:                               # %._crit_edge55.thread.prol.loopexit
                                        #   in Loop: Header=BB0_16 Depth=1
	cmpl	$7, %r8d
	jb	.LBB0_22
	.p2align	4, 0x90
.LBB0_21:                               # %._crit_edge55.thread
                                        #   Parent Loop BB0_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shll	$8, %ecx
	addl	$8, %eax
	cmpl	%eax, %edi
	jne	.LBB0_21
.LBB0_22:                               # %.lr.ph54.preheader
                                        #   in Loop: Header=BB0_16 Depth=1
	movl	%ecx, %esi
	movq	%rdx, %rdi
	shlq	$32, %rdi
	movslq	%edx, %rdx
	leal	(%rsi,%rsi), %ecx
	.p2align	4, 0x90
.LBB0_23:                               # %.lr.ph54
                                        #   Parent Loop BB0_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, 272(%rsp,%rdx,4)
	incl	%esi
	movsbl	1(%rsp,%rdx), %ebp
	incq	%rdx
	addq	%r9, %rdi
	addl	$2, %ecx
	cmpl	%eax, %ebp
	je	.LBB0_23
# BB#24:                                # %._crit_edge55
                                        #   in Loop: Header=BB0_16 Depth=1
	sarq	$32, %rdi
	movb	(%rsp,%rdi), %sil
	incl	%eax
	testb	%sil, %sil
	jne	.LBB0_16
.LBB0_25:                               # %._crit_edge60
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 1264(%r15)
	movdqu	%xmm0, 1248(%r15)
	movdqu	%xmm0, 1232(%r15)
	movdqu	%xmm0, 1216(%r15)
	movdqu	%xmm0, 1200(%r15)
	movdqu	%xmm0, 1184(%r15)
	movdqu	%xmm0, 1168(%r15)
	movdqu	%xmm0, 1152(%r15)
	movdqu	%xmm0, 1136(%r15)
	movdqu	%xmm0, 1120(%r15)
	movdqu	%xmm0, 1104(%r15)
	movdqu	%xmm0, 1088(%r15)
	movdqu	%xmm0, 1072(%r15)
	movdqu	%xmm0, 1056(%r15)
	movdqu	%xmm0, 1040(%r15)
	movdqu	%xmm0, 1024(%r15)
	testl	%ebx, %ebx
	jle	.LBB0_31
# BB#26:                                # %.lr.ph.preheader
	movl	%ebx, %eax
	testb	$1, %al
	jne	.LBB0_28
# BB#27:
	xorl	%esi, %esi
	cmpl	$1, %ebx
	jne	.LBB0_29
	jmp	.LBB0_31
.LBB0_28:                               # %.lr.ph.prol
	movl	272(%rsp), %ecx
	movzbl	17(%r14), %edx
	movl	%ecx, (%r15,%rdx,4)
	movb	(%rsp), %cl
	movzbl	17(%r14), %edx
	movb	%cl, 1024(%r15,%rdx)
	movl	$1, %esi
	cmpl	$1, %ebx
	je	.LBB0_31
.LBB0_29:                               # %.lr.ph.preheader.new
	subq	%rsi, %rax
	leaq	276(%rsp,%rsi,4), %rcx
	leaq	1(%rsp,%rsi), %rdx
	leaq	18(%r14,%rsi), %rsi
	.p2align	4, 0x90
.LBB0_30:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rcx), %edi
	movzbl	-1(%rsi), %ebp
	movl	%edi, (%r15,%rbp,4)
	movzbl	-1(%rdx), %ebx
	movzbl	-1(%rsi), %edi
	movb	%bl, 1024(%r15,%rdi)
	movl	(%rcx), %edi
	movzbl	(%rsi), %ebp
	movl	%edi, (%r15,%rbp,4)
	movzbl	(%rdx), %ebx
	movzbl	(%rsi), %edi
	movb	%bl, 1024(%r15,%rdi)
	addq	$8, %rcx
	addq	$2, %rdx
	addq	$2, %rsi
	addq	$-2, %rax
	jne	.LBB0_30
.LBB0_31:                               # %._crit_edge
	addq	$1312, %rsp             # imm = 0x520
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	jpeg_make_c_derived_tbl, .Lfunc_end0-jpeg_make_c_derived_tbl
	.cfi_endproc

	.globl	jpeg_gen_optimal_table
	.p2align	4, 0x90
	.type	jpeg_gen_optimal_table,@function
jpeg_gen_optimal_table:                 # @jpeg_gen_optimal_table
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 40
	subq	$2120, %rsp             # imm = 0x848
.Lcfi15:
	.cfi_def_cfa_offset 2160
.Lcfi16:
	.cfi_offset %rbx, -40
.Lcfi17:
	.cfi_offset %r12, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %r15, -16
	movq	%rdx, %r12
	movq	%rsi, %r15
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movb	$0, 32(%rsp)
	leaq	48(%rsp), %rdi
	xorl	%esi, %esi
	movl	$1028, %edx             # imm = 0x404
	callq	memset
	leaq	1088(%rsp), %rdi
	movl	$255, %esi
	movl	$1028, %edx             # imm = 0x404
	callq	memset
	movq	$1, 2048(%r12)
	jmp	.LBB1_1
	.p2align	4, 0x90
.LBB1_30:                               #   in Loop: Header=BB1_1 Depth=1
	movslq	%ecx, %rax
	movq	(%r12,%rax,8), %rdx
	movslq	%r8d, %rsi
	addq	%rdx, (%r12,%rsi,8)
	movq	$0, (%r12,%rax,8)
	.p2align	4, 0x90
.LBB1_31:                               #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%r9d, %rax
	incl	48(%rsp,%rax,4)
	movl	1088(%rsp,%rax,4), %r9d
	testl	%r9d, %r9d
	jns	.LBB1_31
# BB#32:                                #   in Loop: Header=BB1_1 Depth=1
	movl	%ecx, 1088(%rsp,%rax,4)
	.p2align	4, 0x90
.LBB1_33:                               #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ecx, %rax
	incl	48(%rsp,%rax,4)
	movl	1088(%rsp,%rax,4), %ecx
	testl	%ecx, %ecx
	jns	.LBB1_33
.LBB1_1:                                # %.loopexit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_2 Depth 2
                                        #     Child Loop BB1_4 Depth 2
                                        #     Child Loop BB1_31 Depth 2
                                        #     Child Loop BB1_33 Depth 2
	movl	$-1, %r8d
	xorl	%edx, %edx
	movl	$1000000000, %ecx       # imm = 0x3B9ACA00
	jmp	.LBB1_2
	.p2align	4, 0x90
.LBB1_34:                               #   in Loop: Header=BB1_2 Depth=2
	movq	8(%r12,%rdx,8), %rcx
	testq	%rcx, %rcx
	sete	%dl
	cmpq	%rsi, %rcx
	setg	%bl
	orb	%dl, %bl
	cmovel	%eax, %r8d
	cmovneq	%rsi, %rcx
	incq	%rax
	movq	%rax, %rdx
.LBB1_2:                                #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%rdx,8), %rsi
	testq	%rsi, %rsi
	sete	%al
	cmpq	%rcx, %rsi
	setg	%bl
	orb	%al, %bl
	cmovel	%edx, %r8d
	cmovneq	%rcx, %rsi
	leaq	1(%rdx), %rax
	cmpq	$257, %rax              # imm = 0x101
	jne	.LBB1_34
# BB#3:                                 # %.preheader114
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	%r8d, %r9d
	movl	$-1, %ecx
	xorl	%eax, %eax
	movl	$1000000000, %esi       # imm = 0x3B9ACA00
	jmp	.LBB1_4
	.p2align	4, 0x90
.LBB1_35:                               #   in Loop: Header=BB1_4 Depth=2
	movq	8(%r12,%rax,8), %rsi
	testq	%rsi, %rsi
	sete	%al
	cmpq	%rdi, %rsi
	setg	%dl
	orb	%al, %dl
	cmpq	%r9, %rbx
	sete	%al
	orb	%dl, %al
	cmovel	%ebx, %ecx
	cmovneq	%rdi, %rsi
	incq	%rbx
	movq	%rbx, %rax
.LBB1_4:                                #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%rax,8), %rdi
	testq	%rdi, %rdi
	sete	%bl
	cmpq	%rsi, %rdi
	setg	%dl
	orb	%bl, %dl
	cmpq	%rax, %r9
	sete	%bl
	orb	%dl, %bl
	cmovel	%eax, %ecx
	cmovneq	%rsi, %rdi
	leaq	1(%rax), %rbx
	cmpq	$257, %rbx              # imm = 0x101
	jne	.LBB1_35
# BB#5:                                 #   in Loop: Header=BB1_1 Depth=1
	testl	%ecx, %ecx
	jns	.LBB1_30
# BB#6:                                 # %.preheader113
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_7:                                # =>This Inner Loop Header: Depth=1
	movslq	48(%rsp,%rbx,4), %r12
	testq	%r12, %r12
	je	.LBB1_11
# BB#8:                                 #   in Loop: Header=BB1_7 Depth=1
	cmpl	$33, %r12d
	jl	.LBB1_10
# BB#9:                                 #   in Loop: Header=BB1_7 Depth=1
	movq	(%r14), %rax
	movl	$38, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB1_10:                               #   in Loop: Header=BB1_7 Depth=1
	incb	(%rsp,%r12)
.LBB1_11:                               #   in Loop: Header=BB1_7 Depth=1
	incq	%rbx
	cmpq	$257, %rbx              # imm = 0x101
	jne	.LBB1_7
# BB#12:                                # %.preheader111.preheader
	leaq	31(%rsp), %r9
	movabsq	$137438953472, %rdi     # imm = 0x2000000000
	movabsq	$-4294967296, %rdx      # imm = 0xFFFFFFFF00000000
	movl	$32, %esi
	.p2align	4, 0x90
.LBB1_13:                               # %.preheader111
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_15 Depth 2
                                        #       Child Loop BB1_16 Depth 3
	movb	(%rsp,%rsi), %bl
	leaq	-1(%rsi), %r8
	testb	%bl, %bl
	jne	.LBB1_15
	jmp	.LBB1_18
	.p2align	4, 0x90
.LBB1_17:                               #   in Loop: Header=BB1_15 Depth=2
	addb	$-2, %bl
	movb	%bl, (%rsp,%rsi)
	incb	-1(%rsp,%rsi)
	sarq	$32, %rcx
	addb	$2, (%rsp,%rcx)
	decb	(%rax)
	movb	(%rsp,%rsi), %bl
	testb	%bl, %bl
	je	.LBB1_18
.LBB1_15:                               #   Parent Loop BB1_13 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_16 Depth 3
	movq	%r9, %rax
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB1_16:                               #   Parent Loop BB1_13 Depth=1
                                        #     Parent Loop BB1_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addq	%rdx, %rcx
	cmpb	$0, -1(%rax)
	leaq	-1(%rax), %rax
	je	.LBB1_16
	jmp	.LBB1_17
	.p2align	4, 0x90
.LBB1_18:                               # %._crit_edge
                                        #   in Loop: Header=BB1_13 Depth=1
	addq	%rdx, %rdi
	decq	%r9
	cmpq	$16, %r8
	movq	%r8, %rsi
	jg	.LBB1_13
# BB#19:                                # %.preheader110.preheader
	leaq	17(%rsp), %rax
	.p2align	4, 0x90
.LBB1_20:                               # %.preheader110
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rax), %ecx
	decq	%rax
	testb	%cl, %cl
	je	.LBB1_20
# BB#21:
	decb	%cl
	movb	%cl, (%rax)
	movb	16(%rsp), %al
	movb	%al, 16(%r15)
	movaps	(%rsp), %xmm0
	movups	%xmm0, (%r15)
	xorl	%eax, %eax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB1_22:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_23 Depth 2
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_23:                               #   Parent Loop BB1_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ecx, 48(%rsp,%rdx,4)
	jne	.LBB1_25
# BB#24:                                #   in Loop: Header=BB1_23 Depth=2
	cltq
	movb	%dl, 17(%r15,%rax)
	incl	%eax
.LBB1_25:                               #   in Loop: Header=BB1_23 Depth=2
	cmpl	%ecx, 52(%rsp,%rdx,4)
	jne	.LBB1_27
# BB#26:                                #   in Loop: Header=BB1_23 Depth=2
	leal	1(%rdx), %esi
	cltq
	movb	%sil, 17(%r15,%rax)
	incl	%eax
.LBB1_27:                               #   in Loop: Header=BB1_23 Depth=2
	addq	$2, %rdx
	cmpq	$256, %rdx              # imm = 0x100
	jne	.LBB1_23
# BB#28:                                #   in Loop: Header=BB1_22 Depth=1
	incl	%ecx
	cmpl	$33, %ecx
	jne	.LBB1_22
# BB#29:
	movl	$0, 276(%r15)
	addq	$2120, %rsp             # imm = 0x848
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	jpeg_gen_optimal_table, .Lfunc_end1-jpeg_gen_optimal_table
	.cfi_endproc

	.globl	jinit_huff_encoder
	.p2align	4, 0x90
	.type	jinit_huff_encoder,@function
jinit_huff_encoder:                     # @jinit_huff_encoder
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 16
.Lcfi21:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$192, %edx
	callq	*(%rax)
	movq	%rax, 488(%rbx)
	movq	$start_pass_huff, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 128(%rax)
	movq	$0, 144(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 176(%rax)
	movq	$0, 152(%rax)
	popq	%rbx
	retq
.Lfunc_end2:
	.size	jinit_huff_encoder, .Lfunc_end2-jinit_huff_encoder
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_pass_huff,@function
start_pass_huff:                        # @start_pass_huff
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 64
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	488(%r15), %r14
	testl	%esi, %esi
	movl	$encode_mcu_gather, %eax
	movl	$encode_mcu_huff, %ecx
	cmovneq	%rax, %rcx
	movl	$finish_pass_gather, %eax
	movl	$finish_pass_huff, %edx
	cmovneq	%rax, %rdx
	movq	%rcx, 8(%r14)
	movq	%rdx, 16(%r14)
	cmpl	$0, 316(%r15)
	jle	.LBB3_18
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	testl	%esi, %esi
	je	.LBB3_2
	.p2align	4, 0x90
.LBB3_9:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	320(%r15,%rbx,8), %rax
	movslq	20(%rax), %r13
	cmpq	$4, %r13
	movl	24(%rax), %ebp
	jb	.LBB3_11
# BB#10:                                #   in Loop: Header=BB3_9 Depth=1
	movq	(%r15), %rax
	movl	$49, 40(%rax)
	movl	%r13d, 44(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB3_11:                               #   in Loop: Header=BB3_9 Depth=1
	cmpl	$4, %ebp
	jb	.LBB3_13
# BB#12:                                #   in Loop: Header=BB3_9 Depth=1
	movq	(%r15), %rax
	movl	$49, 40(%rax)
	movl	%ebp, 44(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB3_13:                               #   in Loop: Header=BB3_9 Depth=1
	movslq	%ebp, %r12
	movq	128(%r14,%r13,8), %rdi
	testq	%rdi, %rdi
	jne	.LBB3_15
# BB#14:                                #   in Loop: Header=BB3_9 Depth=1
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$2056, %edx             # imm = 0x808
	movq	%r15, %rdi
	callq	*(%rax)
	movq	%rax, %rdi
	movq	%rdi, 128(%r14,%r13,8)
.LBB3_15:                               #   in Loop: Header=BB3_9 Depth=1
	xorl	%esi, %esi
	movl	$2056, %edx             # imm = 0x808
	callq	memset
	movq	160(%r14,%r12,8), %rdi
	testq	%rdi, %rdi
	jne	.LBB3_17
# BB#16:                                #   in Loop: Header=BB3_9 Depth=1
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$2056, %edx             # imm = 0x808
	movq	%r15, %rdi
	callq	*(%rax)
	movq	%rax, %rdi
	movq	%rdi, 160(%r14,%r12,8)
.LBB3_17:                               #   in Loop: Header=BB3_9 Depth=1
	xorl	%esi, %esi
	movl	$2056, %edx             # imm = 0x808
	callq	memset
	movl	$0, 36(%r14,%rbx,4)
	incq	%rbx
	movslq	316(%r15), %rax
	cmpq	%rax, %rbx
	jl	.LBB3_9
	jmp	.LBB3_18
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	320(%r15,%rbx,8), %rax
	movslq	20(%rax), %r12
	cmpq	$3, %r12
	movl	24(%rax), %r13d
	ja	.LBB3_4
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	cmpq	$0, 120(%r15,%r12,8)
	jne	.LBB3_5
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	movq	(%r15), %rax
	movl	$49, 40(%rax)
	movl	%r12d, 44(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB3_5:                                #   in Loop: Header=BB3_2 Depth=1
	movslq	%r13d, %rbp
	cmpl	$3, %ebp
	ja	.LBB3_7
# BB#6:                                 #   in Loop: Header=BB3_2 Depth=1
	cmpq	$0, 152(%r15,%rbp,8)
	jne	.LBB3_8
.LBB3_7:                                # %._crit_edge94
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	(%r15), %rax
	movl	$49, 40(%rax)
	movl	%r13d, 44(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB3_8:                                #   in Loop: Header=BB3_2 Depth=1
	movq	120(%r15,%r12,8), %rsi
	leaq	64(%r14,%r12,8), %rdx
	movq	%r15, %rdi
	callq	jpeg_make_c_derived_tbl
	movq	152(%r15,%rbp,8), %rsi
	leaq	96(%r14,%rbp,8), %rdx
	movq	%r15, %rdi
	callq	jpeg_make_c_derived_tbl
	movl	$0, 36(%r14,%rbx,4)
	incq	%rbx
	movslq	316(%r15), %rax
	cmpq	%rax, %rbx
	jl	.LBB3_2
.LBB3_18:                               # %._crit_edge
	movq	$0, 24(%r14)
	movl	$0, 32(%r14)
	movl	272(%r15), %eax
	movl	%eax, 56(%r14)
	movl	$0, 60(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	start_pass_huff, .Lfunc_end3-start_pass_huff
	.cfi_endproc

	.p2align	4, 0x90
	.type	encode_mcu_gather,@function
encode_mcu_gather:                      # @encode_mcu_gather
	.cfi_startproc
# BB#0:
	movq	488(%rdi), %r8
	movl	272(%rdi), %eax
	testl	%eax, %eax
	je	.LBB4_8
# BB#1:
	movl	56(%r8), %ecx
	testl	%ecx, %ecx
	jne	.LBB4_7
# BB#2:                                 # %.preheader37
	cmpl	$0, 316(%rdi)
	jle	.LBB4_6
# BB#3:                                 # %.lr.ph40.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_4:                                # %.lr.ph40
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, 36(%r8,%rax,4)
	incq	%rax
	movslq	316(%rdi), %rcx
	cmpq	%rcx, %rax
	jl	.LBB4_4
# BB#5:                                 # %._crit_edge41.loopexit
	movl	272(%rdi), %eax
.LBB4_6:                                # %._crit_edge41
	movl	%eax, 56(%r8)
	movl	%eax, %ecx
.LBB4_7:
	decl	%ecx
	movl	%ecx, 56(%r8)
.LBB4_8:                                # %.preheader
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 40
.Lcfi39:
	.cfi_offset %rbx, -40
.Lcfi40:
	.cfi_offset %r14, -32
.Lcfi41:
	.cfi_offset %r15, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	cmpl	$0, 360(%rdi)
	jle	.LBB4_26
# BB#9:                                 # %.lr.ph.preheader
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB4_10:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_13 Depth 2
                                        #     Child Loop BB4_16 Depth 2
                                        #       Child Loop BB4_20 Depth 3
	movslq	364(%rdi,%r11,4), %r10
	movq	320(%rdi,%r10,8), %rax
	movq	(%rsi,%r11,8), %r14
	movslq	20(%rax), %rcx
	movq	128(%r8,%rcx,8), %rcx
	movslq	24(%rax), %rax
	movq	160(%r8,%rax,8), %r15
	movswl	(%r14), %r9d
	movl	%r9d, %ebx
	subl	36(%r8,%r10,4), %ebx
	movl	%ebx, %edx
	sarl	$31, %edx
	addl	%edx, %ebx
	xorl	%edx, %ebx
	je	.LBB4_11
# BB#12:                                # %.lr.ph45.i.preheader
                                        #   in Loop: Header=BB4_10 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_13:                               # %.lr.ph45.i
                                        #   Parent Loop BB4_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%edx
	sarl	%ebx
	jne	.LBB4_13
# BB#14:                                # %._crit_edge46.loopexit.i
                                        #   in Loop: Header=BB4_10 Depth=1
	movslq	%edx, %rdx
	jmp	.LBB4_15
	.p2align	4, 0x90
.LBB4_11:                               #   in Loop: Header=BB4_10 Depth=1
	xorl	%edx, %edx
.LBB4_15:                               # %._crit_edge46.i
                                        #   in Loop: Header=BB4_10 Depth=1
	incq	(%rcx,%rdx,8)
	xorl	%ecx, %ecx
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB4_16:                               #   Parent Loop BB4_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_20 Depth 3
	movslq	jpeg_natural_order(,%rbx,4), %rdx
	movswl	(%r14,%rdx,2), %edx
	testl	%edx, %edx
	je	.LBB4_27
# BB#17:                                # %.preheader.i
                                        #   in Loop: Header=BB4_16 Depth=2
	cmpl	$16, %ecx
	jl	.LBB4_19
# BB#18:                                # %.lr.ph.i
                                        #   in Loop: Header=BB4_16 Depth=2
	movq	1920(%r15), %rbp
	addl	$-16, %ecx
	movl	%ecx, %eax
	shrl	$4, %eax
	leaq	1(%rax,%rbp), %rbp
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	shll	$4, %eax
	subl	%eax, %ecx
	movq	%rbp, 1920(%r15)
.LBB4_19:                               #   in Loop: Header=BB4_16 Depth=2
	movl	%edx, %ebp
	negl	%ebp
	testw	%dx, %dx
	cmovnsl	%edx, %ebp
	shll	$4, %ecx
	.p2align	4, 0x90
.LBB4_20:                               #   Parent Loop BB4_10 Depth=1
                                        #     Parent Loop BB4_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	sarl	%ebp
	incl	%ecx
	testl	%ebp, %ebp
	jne	.LBB4_20
# BB#21:                                #   in Loop: Header=BB4_16 Depth=2
	movslq	%ecx, %rax
	incq	(%r15,%rax,8)
	xorl	%ecx, %ecx
	jmp	.LBB4_22
	.p2align	4, 0x90
.LBB4_27:                               #   in Loop: Header=BB4_16 Depth=2
	incl	%ecx
.LBB4_22:                               #   in Loop: Header=BB4_16 Depth=2
	incq	%rbx
	cmpq	$64, %rbx
	jne	.LBB4_16
# BB#23:                                #   in Loop: Header=BB4_10 Depth=1
	testl	%ecx, %ecx
	jle	.LBB4_25
# BB#24:                                #   in Loop: Header=BB4_10 Depth=1
	incq	(%r15)
.LBB4_25:                               # %htest_one_block.exit
                                        #   in Loop: Header=BB4_10 Depth=1
	movl	%r9d, 36(%r8,%r10,4)
	incq	%r11
	movslq	360(%rdi), %rax
	cmpq	%rax, %r11
	jl	.LBB4_10
.LBB4_26:                               # %._crit_edge
	movl	$1, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	encode_mcu_gather, .Lfunc_end4-encode_mcu_gather
	.cfi_endproc

	.p2align	4, 0x90
	.type	finish_pass_gather,@function
finish_pass_gather:                     # @finish_pass_gather
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi45:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi48:
	.cfi_def_cfa_offset 80
.Lcfi49:
	.cfi_offset %rbx, -48
.Lcfi50:
	.cfi_offset %r12, -40
.Lcfi51:
	.cfi_offset %r13, -32
.Lcfi52:
	.cfi_offset %r14, -24
.Lcfi53:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	488(%r15), %r14
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, (%rsp)
	cmpl	$0, 316(%r15)
	jle	.LBB5_11
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	movq	320(%r15,%rbx,8), %rax
	movslq	24(%rax), %r12
	movslq	20(%rax), %r13
	cmpl	$0, 16(%rsp,%r13,4)
	jne	.LBB5_6
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	120(%r15,%r13,8), %rsi
	testq	%rsi, %rsi
	jne	.LBB5_5
# BB#4:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	%r15, %rdi
	callq	jpeg_alloc_huff_table
	movq	%rax, %rsi
	movq	%rsi, 120(%r15,%r13,8)
.LBB5_5:                                #   in Loop: Header=BB5_2 Depth=1
	movq	128(%r14,%r13,8), %rdx
	movq	%r15, %rdi
	callq	jpeg_gen_optimal_table
	movl	$1, 16(%rsp,%r13,4)
.LBB5_6:                                #   in Loop: Header=BB5_2 Depth=1
	cmpl	$0, (%rsp,%r12,4)
	jne	.LBB5_10
# BB#7:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	152(%r15,%r12,8), %rsi
	testq	%rsi, %rsi
	jne	.LBB5_9
# BB#8:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	%r15, %rdi
	callq	jpeg_alloc_huff_table
	movq	%rax, %rsi
	movq	%rsi, 152(%r15,%r12,8)
.LBB5_9:                                #   in Loop: Header=BB5_2 Depth=1
	movq	160(%r14,%r12,8), %rdx
	movq	%r15, %rdi
	callq	jpeg_gen_optimal_table
	movl	$1, (%rsp,%r12,4)
.LBB5_10:                               #   in Loop: Header=BB5_2 Depth=1
	incq	%rbx
	movslq	316(%r15), %rax
	cmpq	%rax, %rbx
	jl	.LBB5_2
.LBB5_11:                               # %._crit_edge
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	finish_pass_gather, .Lfunc_end5-finish_pass_gather
	.cfi_endproc

	.p2align	4, 0x90
	.type	encode_mcu_huff,@function
encode_mcu_huff:                        # @encode_mcu_huff
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi60:
	.cfi_def_cfa_offset 192
.Lcfi61:
	.cfi_offset %rbx, -56
.Lcfi62:
	.cfi_offset %r12, -48
.Lcfi63:
	.cfi_offset %r13, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movq	488(%rdi), %r13
	movq	32(%rdi), %rcx
	movq	(%rcx), %rax
	movq	%rax, (%rsp)
	movq	8(%rcx), %r12
	movq	%r12, 8(%rsp)
	movdqu	24(%r13), %xmm0
	movups	40(%r13), %xmm1
	movaps	%xmm1, 32(%rsp)
	movdqa	%xmm0, 16(%rsp)
	movq	%rdi, 48(%rsp)
	cmpl	$0, 272(%rdi)
	je	.LBB6_2
# BB#1:
	cmpl	$0, 56(%r13)
	je	.LBB6_83
.LBB6_2:                                # %emit_restart.exit.preheader
	cmpl	$0, 360(%rdi)
	jle	.LBB6_107
.LBB6_3:                                # %.lr.ph
	xorl	%edx, %edx
	movq	%r13, 96(%rsp)          # 8-byte Spill
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	movq	%rsi, 72(%rsp)          # 8-byte Spill
.LBB6_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_6 Depth 2
                                        #     Child Loop BB6_10 Depth 2
                                        #     Child Loop BB6_20 Depth 2
                                        #     Child Loop BB6_30 Depth 2
                                        #       Child Loop BB6_32 Depth 3
                                        #         Child Loop BB6_35 Depth 4
                                        #       Child Loop BB6_45 Depth 3
                                        #       Child Loop BB6_49 Depth 3
                                        #       Child Loop BB6_58 Depth 3
                                        #     Child Loop BB6_73 Depth 2
	movslq	364(%rdi,%rdx,4), %rbp
	movq	320(%rdi,%rbp,8), %rcx
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movq	(%rsi,%rdx,8), %rdx
	movslq	20(%rcx), %rax
	movq	64(%r13,%rax,8), %rax
	movslq	24(%rcx), %rcx
	movq	96(%r13,%rcx,8), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movswl	(%rdx), %edx
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	subl	28(%rsp,%rbp,4), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	addl	%ecx, %edx
	movl	%edx, 60(%rsp)          # 4-byte Spill
	xorl	%edx, %ecx
	movl	$0, %ebx
	je	.LBB6_7
# BB#5:                                 # %.lr.ph147.i.preheader
                                        #   in Loop: Header=BB6_4 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_6:                                # %.lr.ph147.i
                                        #   Parent Loop BB6_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebx
	sarl	%ecx
	jne	.LBB6_6
.LBB6_7:                                # %._crit_edge148.i
                                        #   in Loop: Header=BB6_4 Depth=1
	movslq	%ebx, %rcx
	movsbl	1024(%rax,%rcx), %ebp
	testl	%ebp, %ebp
	movl	(%rax,%rcx,4), %r15d
	movl	24(%rsp), %r14d
	jne	.LBB6_9
# BB#8:                                 #   in Loop: Header=BB6_4 Depth=1
	movq	48(%rsp), %rdi
	movq	(%rdi), %rax
	movl	$39, 40(%rax)
	callq	*(%rax)
.LBB6_9:                                #   in Loop: Header=BB6_4 Depth=1
	movl	$1, %r13d
	movl	%ebp, %ecx
	shlq	%cl, %r13
	decl	%r13d
	andq	%r15, %r13
	addl	%ebp, %r14d
	movl	$24, %ecx
	subl	%r14d, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %r13
	orq	16(%rsp), %r13
	cmpl	$8, %r14d
	jl	.LBB6_18
	.p2align	4, 0x90
.LBB6_10:                               # %.lr.ph.i.i
                                        #   Parent Loop BB6_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rbp
	shrq	$16, %rbp
	movq	(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, (%rsp)
	movb	%bpl, (%rax)
	movq	8(%rsp), %r12
	decq	%r12
	movq	%r12, 8(%rsp)
	jne	.LBB6_13
# BB#11:                                #   in Loop: Header=BB6_10 Depth=2
	movq	48(%rsp), %rdi
	movq	32(%rdi), %r15
	callq	*24(%r15)
	testl	%eax, %eax
	je	.LBB6_97
# BB#12:                                # %dump_buffer.exit.i.i
                                        #   in Loop: Header=BB6_10 Depth=2
	movdqu	(%r15), %xmm0
	movdqa	%xmm0, (%rsp)
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %r12
.LBB6_13:                               #   in Loop: Header=BB6_10 Depth=2
	cmpb	$-1, %bpl
	jne	.LBB6_17
# BB#14:                                #   in Loop: Header=BB6_10 Depth=2
	movq	(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, (%rsp)
	movb	$0, (%rax)
	movq	8(%rsp), %r12
	decq	%r12
	movq	%r12, 8(%rsp)
	jne	.LBB6_17
# BB#15:                                #   in Loop: Header=BB6_10 Depth=2
	movq	48(%rsp), %rdi
	movq	32(%rdi), %rbp
	callq	*24(%rbp)
	testl	%eax, %eax
	je	.LBB6_113
# BB#16:                                # %dump_buffer.exit42.i.i
                                        #   in Loop: Header=BB6_10 Depth=2
	movdqu	(%rbp), %xmm0
	movdqa	%xmm0, (%rsp)
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %r12
	.p2align	4, 0x90
.LBB6_17:                               #   in Loop: Header=BB6_10 Depth=2
	shlq	$8, %r13
	addl	$-8, %r14d
	cmpl	$7, %r14d
	jg	.LBB6_10
.LBB6_18:                               # %.loopexit125.i
                                        #   in Loop: Header=BB6_4 Depth=1
	movq	%r13, 16(%rsp)
	movl	%r14d, 24(%rsp)
	testl	%ebx, %ebx
	je	.LBB6_29
# BB#19:                                #   in Loop: Header=BB6_4 Depth=1
	movl	60(%rsp), %eax          # 4-byte Reload
	movl	$1, %edx
	movl	%ebx, %ecx
	shlq	%cl, %rdx
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	leal	(%rcx,%rdx), %ebp
	andq	%rax, %rbp
	addl	%ebx, %r14d
	movl	$24, %ecx
	subl	%r14d, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rbp
	orq	%r13, %rbp
	cmpl	$8, %r14d
	jl	.LBB6_28
	.p2align	4, 0x90
.LBB6_20:                               # %.lr.ph.i67.i
                                        #   Parent Loop BB6_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, %rbx
	shrq	$16, %rbx
	movq	(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, (%rsp)
	movb	%bl, (%rax)
	movq	8(%rsp), %r12
	decq	%r12
	movq	%r12, 8(%rsp)
	jne	.LBB6_23
# BB#21:                                #   in Loop: Header=BB6_20 Depth=2
	movq	48(%rsp), %rdi
	movq	32(%rdi), %r15
	callq	*24(%r15)
	testl	%eax, %eax
	je	.LBB6_113
# BB#22:                                # %dump_buffer.exit.i70.i
                                        #   in Loop: Header=BB6_20 Depth=2
	movdqu	(%r15), %xmm0
	movdqa	%xmm0, (%rsp)
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %r12
.LBB6_23:                               #   in Loop: Header=BB6_20 Depth=2
	cmpb	$-1, %bl
	jne	.LBB6_27
# BB#24:                                #   in Loop: Header=BB6_20 Depth=2
	movq	(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, (%rsp)
	movb	$0, (%rax)
	movq	8(%rsp), %r12
	decq	%r12
	movq	%r12, 8(%rsp)
	jne	.LBB6_27
# BB#25:                                #   in Loop: Header=BB6_20 Depth=2
	movq	48(%rsp), %rdi
	movq	32(%rdi), %rbx
	callq	*24(%rbx)
	testl	%eax, %eax
	je	.LBB6_113
# BB#26:                                # %dump_buffer.exit42.i71.i
                                        #   in Loop: Header=BB6_20 Depth=2
	movdqu	(%rbx), %xmm0
	movdqa	%xmm0, (%rsp)
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %r12
	.p2align	4, 0x90
.LBB6_27:                               #   in Loop: Header=BB6_20 Depth=2
	shlq	$8, %rbp
	addl	$-8, %r14d
	cmpl	$7, %r14d
	jg	.LBB6_20
.LBB6_28:                               # %emit_bits.exit76.i
                                        #   in Loop: Header=BB6_4 Depth=1
	movq	%rbp, 16(%rsp)
	movl	%r14d, 24(%rsp)
	movq	%rbp, %r13
.LBB6_29:                               # %.preheader122.i
                                        #   in Loop: Header=BB6_4 Depth=1
	xorl	%edx, %edx
	movl	$1, %r15d
.LBB6_30:                               #   Parent Loop BB6_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_32 Depth 3
                                        #         Child Loop BB6_35 Depth 4
                                        #       Child Loop BB6_45 Depth 3
                                        #       Child Loop BB6_49 Depth 3
                                        #       Child Loop BB6_58 Depth 3
	movslq	jpeg_natural_order(,%r15,4), %rax
	movq	128(%rsp), %rcx         # 8-byte Reload
	movswl	(%rcx,%rax,2), %eax
	testl	%eax, %eax
	je	.LBB6_67
# BB#31:                                # %.preheader.i44
                                        #   in Loop: Header=BB6_30 Depth=2
	movl	%eax, 68(%rsp)          # 4-byte Spill
	movl	%edx, 60(%rsp)          # 4-byte Spill
	cmpl	$16, %edx
	jl	.LBB6_44
.LBB6_32:                               # %.lr.ph.i45
                                        #   Parent Loop BB6_4 Depth=1
                                        #     Parent Loop BB6_30 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB6_35 Depth 4
	movq	88(%rsp), %rax          # 8-byte Reload
	movsbl	1264(%rax), %ebx
	testl	%ebx, %ebx
	movl	960(%rax), %ebp
	jne	.LBB6_34
# BB#33:                                #   in Loop: Header=BB6_32 Depth=3
	movq	48(%rsp), %rdi
	movq	(%rdi), %rax
	movl	$39, 40(%rax)
	callq	*(%rax)
.LBB6_34:                               #   in Loop: Header=BB6_32 Depth=3
	movl	$1, %eax
	movl	%ebx, %ecx
	shlq	%cl, %rax
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	leal	(%rcx,%rax), %eax
	andq	%rbp, %rax
	addl	%ebx, %r14d
	movl	$24, %ecx
	subl	%r14d, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rax
	orq	%rax, %r13
	cmpl	$8, %r14d
	jl	.LBB6_43
	.p2align	4, 0x90
.LBB6_35:                               # %.lr.ph.i77.i
                                        #   Parent Loop BB6_4 Depth=1
                                        #     Parent Loop BB6_30 Depth=2
                                        #       Parent Loop BB6_32 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%r13, %rbx
	shrq	$16, %rbx
	movq	(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, (%rsp)
	movb	%bl, (%rax)
	movq	8(%rsp), %r12
	decq	%r12
	movq	%r12, 8(%rsp)
	jne	.LBB6_38
# BB#36:                                #   in Loop: Header=BB6_35 Depth=4
	movq	48(%rsp), %rdi
	movq	32(%rdi), %rbp
	callq	*24(%rbp)
	testl	%eax, %eax
	je	.LBB6_97
# BB#37:                                # %dump_buffer.exit.i80.i
                                        #   in Loop: Header=BB6_35 Depth=4
	movdqu	(%rbp), %xmm0
	movdqa	%xmm0, (%rsp)
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %r12
.LBB6_38:                               #   in Loop: Header=BB6_35 Depth=4
	cmpb	$-1, %bl
	jne	.LBB6_42
# BB#39:                                #   in Loop: Header=BB6_35 Depth=4
	movq	(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, (%rsp)
	movb	$0, (%rax)
	movq	8(%rsp), %r12
	decq	%r12
	movq	%r12, 8(%rsp)
	jne	.LBB6_42
# BB#40:                                #   in Loop: Header=BB6_35 Depth=4
	movq	48(%rsp), %rdi
	movq	32(%rdi), %rbx
	callq	*24(%rbx)
	testl	%eax, %eax
	je	.LBB6_97
# BB#41:                                # %dump_buffer.exit42.i81.i
                                        #   in Loop: Header=BB6_35 Depth=4
	movdqu	(%rbx), %xmm0
	movdqa	%xmm0, (%rsp)
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %r12
	.p2align	4, 0x90
.LBB6_42:                               #   in Loop: Header=BB6_35 Depth=4
	shlq	$8, %r13
	addl	$-8, %r14d
	cmpl	$7, %r14d
	jg	.LBB6_35
.LBB6_43:                               # %.loopexit.i47
                                        #   in Loop: Header=BB6_32 Depth=3
	movq	%r13, 16(%rsp)
	movl	%r14d, 24(%rsp)
	movl	60(%rsp), %eax          # 4-byte Reload
	addl	$-16, %eax
	movl	%eax, 60(%rsp)          # 4-byte Spill
	cmpl	$15, %eax
	jg	.LBB6_32
	.p2align	4, 0x90
.LBB6_44:                               # %._crit_edge.i
                                        #   in Loop: Header=BB6_30 Depth=2
	movq	%r15, 120(%rsp)         # 8-byte Spill
	movl	68(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %eax
	negl	%eax
	testw	%cx, %cx
	cmovnsl	%ecx, %eax
                                        # kill: %CX<def> %CX<kill> %ECX<kill>
	sarw	$15, %cx
	movw	%cx, 66(%rsp)           # 2-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_45:                               #   Parent Loop BB6_4 Depth=1
                                        #     Parent Loop BB6_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	sarl	%eax
	decq	%rbx
	testl	%eax, %eax
	jne	.LBB6_45
# BB#46:                                #   in Loop: Header=BB6_30 Depth=2
	movl	60(%rsp), %eax          # 4-byte Reload
	shll	$4, %eax
	subl	%ebx, %eax
	cltq
	movq	88(%rsp), %rcx          # 8-byte Reload
	movsbl	1024(%rcx,%rax), %ebp
	testl	%ebp, %ebp
	movl	(%rcx,%rax,4), %r15d
	jne	.LBB6_48
# BB#47:                                #   in Loop: Header=BB6_30 Depth=2
	movq	48(%rsp), %rdi
	movq	(%rdi), %rax
	movl	$39, 40(%rax)
	callq	*(%rax)
.LBB6_48:                               #   in Loop: Header=BB6_30 Depth=2
	movl	$1, %eax
	movl	%ebp, %ecx
	shlq	%cl, %rax
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	leal	(%rcx,%rax), %eax
	andq	%r15, %rax
	addl	%ebp, %r14d
	movl	$24, %ecx
	subl	%r14d, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rax
	orq	%rax, %r13
	cmpl	$8, %r14d
	jl	.LBB6_57
	.p2align	4, 0x90
.LBB6_49:                               # %.lr.ph.i87.i
                                        #   Parent Loop BB6_4 Depth=1
                                        #     Parent Loop BB6_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r13, %rbp
	shrq	$16, %rbp
	movq	(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, (%rsp)
	movb	%bpl, (%rax)
	movq	8(%rsp), %r12
	decq	%r12
	movq	%r12, 8(%rsp)
	jne	.LBB6_52
# BB#50:                                #   in Loop: Header=BB6_49 Depth=3
	movq	48(%rsp), %rdi
	movq	32(%rdi), %r15
	callq	*24(%r15)
	testl	%eax, %eax
	je	.LBB6_113
# BB#51:                                # %dump_buffer.exit.i90.i
                                        #   in Loop: Header=BB6_49 Depth=3
	movdqu	(%r15), %xmm0
	movdqa	%xmm0, (%rsp)
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %r12
.LBB6_52:                               #   in Loop: Header=BB6_49 Depth=3
	cmpb	$-1, %bpl
	jne	.LBB6_56
# BB#53:                                #   in Loop: Header=BB6_49 Depth=3
	movq	(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, (%rsp)
	movb	$0, (%rax)
	movq	8(%rsp), %r12
	decq	%r12
	movq	%r12, 8(%rsp)
	jne	.LBB6_56
# BB#54:                                #   in Loop: Header=BB6_49 Depth=3
	movq	48(%rsp), %rdi
	movq	32(%rdi), %rbp
	callq	*24(%rbp)
	testl	%eax, %eax
	je	.LBB6_113
# BB#55:                                # %dump_buffer.exit42.i91.i
                                        #   in Loop: Header=BB6_49 Depth=3
	movdqu	(%rbp), %xmm0
	movdqa	%xmm0, (%rsp)
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %r12
	.p2align	4, 0x90
.LBB6_56:                               #   in Loop: Header=BB6_49 Depth=3
	shlq	$8, %r13
	addl	$-8, %r14d
	cmpl	$7, %r14d
	jg	.LBB6_49
.LBB6_57:                               # %.loopexit121.i
                                        #   in Loop: Header=BB6_30 Depth=2
	movswl	66(%rsp), %ecx          # 2-byte Folded Reload
	addl	68(%rsp), %ecx          # 4-byte Folded Reload
	movl	%r14d, %eax
	movq	%r13, 16(%rsp)
	movl	%r14d, 24(%rsp)
	movl	%ecx, %edx
	movl	%ebx, %ecx
	negl	%ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rsi
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	leal	(%rcx,%rsi), %r15d
	andq	%rdx, %r15
	subq	%rbx, %rax
	subl	%r14d, %ebx
	addl	$24, %ebx
	movl	%ebx, %ecx
	shlq	%cl, %r15
	orq	%r13, %r15
	movl	%eax, %r14d
	cmpl	$7, %eax
	jle	.LBB6_66
	.p2align	4, 0x90
.LBB6_58:                               # %.lr.ph.i97.i
                                        #   Parent Loop BB6_4 Depth=1
                                        #     Parent Loop BB6_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r15, %rbx
	shrq	$16, %rbx
	movq	(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, (%rsp)
	movb	%bl, (%rax)
	movq	8(%rsp), %r12
	decq	%r12
	movq	%r12, 8(%rsp)
	jne	.LBB6_61
# BB#59:                                #   in Loop: Header=BB6_58 Depth=3
	movq	48(%rsp), %rdi
	movq	32(%rdi), %rbp
	callq	*24(%rbp)
	testl	%eax, %eax
	je	.LBB6_113
# BB#60:                                # %dump_buffer.exit.i100.i
                                        #   in Loop: Header=BB6_58 Depth=3
	movdqu	(%rbp), %xmm0
	movdqa	%xmm0, (%rsp)
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %r12
.LBB6_61:                               #   in Loop: Header=BB6_58 Depth=3
	cmpb	$-1, %bl
	jne	.LBB6_65
# BB#62:                                #   in Loop: Header=BB6_58 Depth=3
	movq	(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, (%rsp)
	movb	$0, (%rax)
	movq	8(%rsp), %r12
	decq	%r12
	movq	%r12, 8(%rsp)
	jne	.LBB6_65
# BB#63:                                #   in Loop: Header=BB6_58 Depth=3
	movq	48(%rsp), %rdi
	movq	32(%rdi), %rbx
	callq	*24(%rbx)
	testl	%eax, %eax
	je	.LBB6_113
# BB#64:                                # %dump_buffer.exit42.i101.i
                                        #   in Loop: Header=BB6_58 Depth=3
	movdqu	(%rbx), %xmm0
	movdqa	%xmm0, (%rsp)
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %r12
	.p2align	4, 0x90
.LBB6_65:                               #   in Loop: Header=BB6_58 Depth=3
	shlq	$8, %r15
	addl	$-8, %r14d
	cmpl	$7, %r14d
	jg	.LBB6_58
.LBB6_66:                               # %emit_bits.exit106.i
                                        #   in Loop: Header=BB6_30 Depth=2
	movq	%r15, 16(%rsp)
	movl	%r14d, 24(%rsp)
	xorl	%edx, %edx
	movq	%r15, %r13
	movq	120(%rsp), %r15         # 8-byte Reload
	jmp	.LBB6_68
	.p2align	4, 0x90
.LBB6_67:                               #   in Loop: Header=BB6_30 Depth=2
	incl	%edx
.LBB6_68:                               #   in Loop: Header=BB6_30 Depth=2
	incq	%r15
	cmpq	$64, %r15
	jl	.LBB6_30
# BB#69:                                #   in Loop: Header=BB6_4 Depth=1
	testl	%edx, %edx
	jle	.LBB6_82
# BB#70:                                #   in Loop: Header=BB6_4 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	movsbl	1024(%rax), %ebx
	testl	%ebx, %ebx
	movl	(%rax), %ebp
	jne	.LBB6_72
# BB#71:                                #   in Loop: Header=BB6_4 Depth=1
	movq	48(%rsp), %rdi
	movq	(%rdi), %rax
	movl	$39, 40(%rax)
	callq	*(%rax)
.LBB6_72:                               #   in Loop: Header=BB6_4 Depth=1
	movl	$1, %eax
	movl	%ebx, %ecx
	shlq	%cl, %rax
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	leal	(%rcx,%rax), %eax
	andq	%rbp, %rax
	addl	%ebx, %r14d
	movl	$24, %ecx
	subl	%r14d, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rax
	orq	%rax, %r13
	cmpl	$8, %r14d
	jl	.LBB6_81
.LBB6_73:                               # %.lr.ph.i107.i
                                        #   Parent Loop BB6_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rbx
	shrq	$16, %rbx
	movq	(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, (%rsp)
	movb	%bl, (%rax)
	movq	8(%rsp), %r12
	decq	%r12
	movq	%r12, 8(%rsp)
	jne	.LBB6_76
# BB#74:                                #   in Loop: Header=BB6_73 Depth=2
	movq	48(%rsp), %rdi
	movq	32(%rdi), %rbp
	callq	*24(%rbp)
	testl	%eax, %eax
	je	.LBB6_113
# BB#75:                                # %dump_buffer.exit.i110.i
                                        #   in Loop: Header=BB6_73 Depth=2
	movdqu	(%rbp), %xmm0
	movdqa	%xmm0, (%rsp)
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %r12
.LBB6_76:                               #   in Loop: Header=BB6_73 Depth=2
	cmpb	$-1, %bl
	jne	.LBB6_80
# BB#77:                                #   in Loop: Header=BB6_73 Depth=2
	movq	(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, (%rsp)
	movb	$0, (%rax)
	movq	8(%rsp), %r12
	decq	%r12
	movq	%r12, 8(%rsp)
	jne	.LBB6_80
# BB#78:                                #   in Loop: Header=BB6_73 Depth=2
	movq	48(%rsp), %rdi
	movq	32(%rdi), %rbx
	callq	*24(%rbx)
	testl	%eax, %eax
	je	.LBB6_113
# BB#79:                                # %dump_buffer.exit42.i111.i
                                        #   in Loop: Header=BB6_73 Depth=2
	movdqu	(%rbx), %xmm0
	movdqa	%xmm0, (%rsp)
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %r12
.LBB6_80:                               #   in Loop: Header=BB6_73 Depth=2
	shlq	$8, %r13
	addl	$-8, %r14d
	cmpl	$7, %r14d
	jg	.LBB6_73
.LBB6_81:                               # %emit_bits.exit116.i
                                        #   in Loop: Header=BB6_4 Depth=1
	movq	%r13, 16(%rsp)
	movl	%r14d, 24(%rsp)
.LBB6_82:                               # %encode_one_block.exit
                                        #   in Loop: Header=BB6_4 Depth=1
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
	movq	(%rsi,%rdx,8), %rax
	movswl	(%rax), %eax
	movq	104(%rsp), %rcx         # 8-byte Reload
	movl	%eax, 28(%rsp,%rcx,4)
	incq	%rdx
	movq	80(%rsp), %rdi          # 8-byte Reload
	movslq	360(%rdi), %rax
	cmpq	%rax, %rdx
	movq	96(%rsp), %r13          # 8-byte Reload
	jl	.LBB6_4
	jmp	.LBB6_107
.LBB6_97:
	xorl	%eax, %eax
	jmp	.LBB6_111
.LBB6_83:
	movl	60(%r13), %r14d
	movl	24(%rsp), %r15d
	testl	%r15d, %r15d
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	jle	.LBB6_95
# BB#84:                                # %.lr.ph.i.i.i
	movl	$17, %ecx
	subl	%r15d, %ecx
	movl	$127, %r12d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %r12
	orq	16(%rsp), %r12
	decl	%r15d
	jmp	.LBB6_86
	.p2align	4, 0x90
.LBB6_85:                               # %._crit_edge
                                        #   in Loop: Header=BB6_86 Depth=1
	shlq	$8, %r12
	movq	(%rsp), %rax
	addl	$-8, %r15d
.LBB6_86:                               # =>This Inner Loop Header: Depth=1
	movq	%r12, %rbx
	shrq	$16, %rbx
	leaq	1(%rax), %rcx
	movq	%rcx, (%rsp)
	movb	%bl, (%rax)
	decq	8(%rsp)
	jne	.LBB6_89
# BB#87:                                #   in Loop: Header=BB6_86 Depth=1
	movq	48(%rsp), %rdi
	movq	32(%rdi), %rbp
	callq	*24(%rbp)
	testl	%eax, %eax
	je	.LBB6_113
# BB#88:                                # %dump_buffer.exit.i.i.i
                                        #   in Loop: Header=BB6_86 Depth=1
	movdqu	(%rbp), %xmm0
	movdqa	%xmm0, (%rsp)
.LBB6_89:                               #   in Loop: Header=BB6_86 Depth=1
	cmpb	$-1, %bl
	jne	.LBB6_93
# BB#90:                                #   in Loop: Header=BB6_86 Depth=1
	movq	(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, (%rsp)
	movb	$0, (%rax)
	decq	8(%rsp)
	jne	.LBB6_93
# BB#91:                                #   in Loop: Header=BB6_86 Depth=1
	movq	48(%rsp), %rdi
	movq	32(%rdi), %rbx
	callq	*24(%rbx)
	testl	%eax, %eax
	je	.LBB6_113
# BB#92:                                # %dump_buffer.exit42.i.i.i
                                        #   in Loop: Header=BB6_86 Depth=1
	movdqu	(%rbx), %xmm0
	movdqa	%xmm0, (%rsp)
	.p2align	4, 0x90
.LBB6_93:                               #   in Loop: Header=BB6_86 Depth=1
	cmpl	$8, %r15d
	jge	.LBB6_85
# BB#94:                                # %.loopexit.i.loopexit
	movq	(%rsp), %rax
.LBB6_95:                               # %.loopexit.i
	movq	$0, 16(%rsp)
	movl	$0, 24(%rsp)
	leaq	1(%rax), %rcx
	movq	%rcx, (%rsp)
	movb	$-1, (%rax)
	decq	8(%rsp)
	je	.LBB6_98
# BB#96:                                # %.loopexit._crit_edge.i
	movq	(%rsp), %rax
	jmp	.LBB6_100
.LBB6_98:
	movq	48(%rsp), %rdi
	movq	32(%rdi), %rbx
	callq	*24(%rbx)
	testl	%eax, %eax
	je	.LBB6_113
# BB#99:                                # %dump_buffer.exit.i
	movdqu	(%rbx), %xmm0
	movdqa	%xmm0, (%rsp)
	movd	%xmm0, %rax
.LBB6_100:
	addl	$208, %r14d
	leaq	1(%rax), %rcx
	movq	%rcx, (%rsp)
	movb	%r14b, (%rax)
	movq	8(%rsp), %r12
	decq	%r12
	movq	%r12, 8(%rsp)
	movq	48(%rsp), %rbx
	jne	.LBB6_103
# BB#101:
	movq	32(%rbx), %rbp
	movq	%rbx, %rdi
	callq	*24(%rbp)
	testl	%eax, %eax
	je	.LBB6_113
# BB#102:                               # %dump_buffer.exit17.i
	movdqu	(%rbp), %xmm0
	movdqa	%xmm0, (%rsp)
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %r12
.LBB6_103:                              # %.preheader.i
	cmpl	$0, 316(%rbx)
	jle	.LBB6_106
# BB#104:                               # %.lr.ph.i.preheader
	xorl	%eax, %eax
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	72(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_105:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, 28(%rsp,%rax,4)
	incq	%rax
	movq	48(%rsp), %rcx
	movslq	316(%rcx), %rcx
	cmpq	%rcx, %rax
	jl	.LBB6_105
	jmp	.LBB6_2
.LBB6_113:
	xorl	%eax, %eax
	jmp	.LBB6_111
.LBB6_106:
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	72(%rsp), %rsi          # 8-byte Reload
	cmpl	$0, 360(%rdi)
	jg	.LBB6_3
.LBB6_107:                              # %emit_restart.exit._crit_edge
	leaq	16(%rsp), %rax
	leaq	24(%r13), %rcx
	movq	(%rsp), %rdx
	movq	32(%rdi), %rsi
	movq	%rdx, (%rsi)
	movq	%r12, 8(%rsi)
	movdqu	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movups	%xmm1, 16(%rcx)
	movdqu	%xmm0, (%rcx)
	movl	272(%rdi), %ecx
	movl	$1, %eax
	testl	%ecx, %ecx
	je	.LBB6_111
# BB#108:
	movl	56(%r13), %edx
	testl	%edx, %edx
	jne	.LBB6_110
# BB#109:
	movl	%ecx, 56(%r13)
	movl	60(%r13), %edx
	incl	%edx
	andl	$7, %edx
	movl	%edx, 60(%r13)
	movl	%ecx, %edx
.LBB6_110:
	decl	%edx
	movl	%edx, 56(%r13)
.LBB6_111:                              # %emit_restart.exit.thread
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	encode_mcu_huff, .Lfunc_end6-encode_mcu_huff
	.cfi_endproc

	.p2align	4, 0x90
	.type	finish_pass_huff,@function
finish_pass_huff:                       # @finish_pass_huff
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi70:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi71:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi73:
	.cfi_def_cfa_offset 112
.Lcfi74:
	.cfi_offset %rbx, -56
.Lcfi75:
	.cfi_offset %r12, -48
.Lcfi76:
	.cfi_offset %r13, -40
.Lcfi77:
	.cfi_offset %r14, -32
.Lcfi78:
	.cfi_offset %r15, -24
.Lcfi79:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	488(%rbx), %rdx
	movq	32(%rbx), %rax
	movq	(%rax), %r12
	movq	8(%rax), %rax
	movq	24(%rdx), %r8
	movl	32(%rdx), %edi
	leaq	36(%rdx), %rsi
	movl	52(%rdx), %ecx
	movl	%ecx, 48(%rsp)
	movups	36(%rdx), %xmm0
	movaps	%xmm0, 32(%rsp)
	testl	%edi, %edi
	jle	.LBB7_1
# BB#2:                                 # %.lr.ph.i.i
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	$17, %ecx
	subl	%edi, %ecx
	movl	$127, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rbp
	movq	%r8, 24(%rsp)           # 8-byte Spill
	orq	%r8, %rbp
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	leal	7(%rdi), %r13d
	.p2align	4, 0x90
.LBB7_3:                                # =>This Inner Loop Header: Depth=1
	movq	%rbp, %r15
	shrq	$16, %r15
	movb	%r15b, (%r12)
	incq	%r12
	decq	%rax
	jne	.LBB7_6
# BB#4:                                 #   in Loop: Header=BB7_3 Depth=1
	movq	32(%rbx), %r14
	movq	%rbx, %rdi
	callq	*24(%r14)
	testl	%eax, %eax
	je	.LBB7_10
# BB#5:                                 # %dump_buffer.exit.i.i
                                        #   in Loop: Header=BB7_3 Depth=1
	movq	(%r14), %r12
	movq	8(%r14), %rax
.LBB7_6:                                #   in Loop: Header=BB7_3 Depth=1
	cmpb	$-1, %r15b
	jne	.LBB7_12
# BB#7:                                 #   in Loop: Header=BB7_3 Depth=1
	movb	$0, (%r12)
	incq	%r12
	decq	%rax
	jne	.LBB7_12
# BB#8:                                 #   in Loop: Header=BB7_3 Depth=1
	movq	%rbx, %rdi
	movq	32(%rdi), %rbx
	movq	%rdi, %r14
	callq	*24(%rbx)
	testl	%eax, %eax
	je	.LBB7_9
# BB#11:                                # %dump_buffer.exit42.i.i
                                        #   in Loop: Header=BB7_3 Depth=1
	movq	(%rbx), %r12
	movq	8(%rbx), %rax
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB7_12:                               #   in Loop: Header=BB7_3 Depth=1
	shlq	$8, %rbp
	addl	$-8, %r13d
	cmpl	$7, %r13d
	jg	.LBB7_3
# BB#13:
	xorl	%edi, %edi
	xorl	%ebp, %ebp
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	jmp	.LBB7_14
.LBB7_1:
	xorl	%edi, %edi
	xorl	%ebp, %ebp
	jmp	.LBB7_14
.LBB7_9:
	movq	%r14, %rbx
.LBB7_10:
	movq	(%rbx), %rax
	movl	$22, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
	xorl	%eax, %eax
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB7_14:                               # %flush_bits.exit
	movq	32(%rbx), %rcx
	movq	%r12, (%rcx)
	movq	%rax, 8(%rcx)
	movq	%rdi, 24(%rdx)
	movl	%ebp, 32(%rdx)
	movl	48(%rsp), %eax
	movl	%eax, 16(%rsi)
	movaps	32(%rsp), %xmm0
	movups	%xmm0, (%rsi)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	finish_pass_huff, .Lfunc_end7-finish_pass_huff
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
